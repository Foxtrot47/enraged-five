// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Ambush
//		AUTHOR			:	Craig Vincent
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "commands_itemsets.sch"

USING "script_blips.sch"
USING "Locates_public.sch"
USING "select_mission_stage.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"
USING "chase_hint_cam.sch"
USING "mission_stat_public.sch"
USING "cutscene_public.sch"
USING "replay_public.sch"
USING "taxi_functions.sch"
USING "flow_mission_trigger_public.sch" //trigger stuff
USING "script_ped.sch"
USING "battlebuddy_public.sch"
USING "asset_management_public.sch"
USING "RC_helper_functions.sch"
USING "commands_recording.sch"


#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

//-----------------------------------------------------------------------------------------------------------
//		ENUMS
//-----------------------------------------------------------------------------------------------------------
ENUM MPF_MISSION_PED_FLAGS	
	mpf_mike,
	mpf_trev,
	mpf_frank,	
	MPF_NUM_OF_PEDS
ENDENUM
ENUM EPF_ENEMY_PED_FLAGS
	epf_c1_D,
	epf_c1_e2,
	epf_c1_e3,
	epf_c1_e4,
	
	epf_trgt_driver,
//	epf_trgt_shooter,
	
	epf_c2_D,
	epf_c2_e2,
	epf_c2_e3,
	epf_c2_e4,
	
	EPF_NUM_OF_ENEMIES
ENDENUM
ENUM MVF_MISSION_VEHICLE_FLAGS
	mvf_target,
	mvf_Convoy1,
	mvf_Convoy2,
	MVF_NUM_OF_VEH
ENDENUM
ENUM MSF_MISSION_STAGE_FLAGS
	msf_0_reached_ambush,
	msf_1_ambushing,
	msf_2_safety,
	msf_3_passed,
	
	MSF_NUM_OF_STAGES
ENDENUM
ENUM eOBJECT_FLAGS
	objCrate1,
	objCrate2,
	NUM_OF_OBJECTS
endenum
ENUM MFF_MISSION_FAIL_FLAGS
	mff_debug_fail,
	mff_default,	
	mff_got_away,
	mff_goods_destroyed,
	mff_goods_stuck,
	mff_left_truck,
	mff_player_dead,
	MFF_NUM_OF_FAILS
ENDENUM
Enum STAGE_SWITCHSTATE
	STAGESWITCH_IDLE,
	STAGESWITCH_REQUESTED,
	STAGESWITCH_EXITING,
	STAGESWITCH_ENTERING	
ENDENUM
ENUM BLOCK_STAGE
	DRIVING,
	BLOCKED,
	RETURNING
ENDENUM
ENUM ARMY_STAGE
	ONROUTE,
	AMBUSH,
	ARMYBASE
endenum
ENUM eAMBUSH_STAGE
	AMBUSH_REACT,
	AMBUSH_START_COMBAT,
	AMBUSH_NEAR_COMBAT,
	AMBUSH_FAR_COMBAT,
	AMBUSH_CHASE
endenum
ENUM ENUM_APPROACH	
	APPROACH_CHECK,
	APPROACH_FRONT,
	APPROACH_RIGHT,
	APPROACH_BEHIND,
	APPROACH_LEFT,
	APPROACH_TO_COMBAT
endenum
ENUM ENUM_GATE_STATE
	GATE_STATE_GATE_CLOSED = 0,
	GATE_STATE_GATE_CLOSING,
	GATE_STATE_GATE_OPEN,
	GATE_STATE_GATE_OPENING
ENDENUM

#IF IS_DEBUG_BUILD

	FUNC STRING GET_STRING_FROM_MISSION_STAGE(MSF_MISSION_STAGE_FLAGS eVal)
		SWITCH eVal
			CASE msf_0_reached_ambush	RETURN "msf_0_reached_ambush"
			CASE msf_1_ambushing		RETURN "msf_1_ambushing"
			CASE msf_2_safety			RETURN "msf_2_safety"
			CASE msf_3_passed			RETURN "msf_3_passed"
		ENDSWITCH
		RETURN "INVALID_INPUT"
	ENDFUNC
	
	FUNC STRING GET_STRING_FROM_GATE_STATE(ENUM_GATE_STATE eVal)
		SWITCH eVal
			CASE GATE_STATE_GATE_CLOSED		RETURN "GATE_STATE_GATE_CLOSED"	
			CASE GATE_STATE_GATE_CLOSING	RETURN "GATE_STATE_GATE_CLOSING"
			CASE GATE_STATE_GATE_OPEN		RETURN "GATE_STATE_GATE_OPEN"
			CASE GATE_STATE_GATE_OPENING	RETURN "GATE_STATE_GATE_OPENING"
		ENDSWITCH
		RETURN "INVALID_INPUT"
	ENDFUNC
	
	FUNC STRING GET_STRING_FROM_BLOCK_STAGE(BLOCK_STAGE eVal)
		SWITCH eVal
			CASE	DRIVING		RETURN "DRIVING"
			CASE	BLOCKED		RETURN "BLOCKED"
			CASE	RETURNING	RETURN "RETURNING"
		ENDSWITCH
		RETURN "INVALID_INPUT"
	ENDFUNC
	
	FUNC STRING GET_STRING_FROM_ARMY_STAGE(ARMY_STAGE eVal)
		SWITCH eVal
			CASE	ONROUTE		RETURN "ONROUTE"
			CASE	AMBUSH		RETURN "AMBUSH"
			CASE	ARMYBASE	RETURN "ARMYBASE"
		ENDSWITCH
		RETURN "INVALID_INPUT"
	ENDFUNC
	
	FUNC STRING GET_STRING_FROM_AMBUSH_STAGE(eAMBUSH_STAGE eVal)
		SWITCH eVal
			CASE	AMBUSH_REACT			RETURN "AMBUSH_REACT"	
			CASE	AMBUSH_START_COMBAT		RETURN "AMBUSH_START_COMBAT"
			CASE	AMBUSH_NEAR_COMBAT		RETURN "AMBUSH_NEAR_COMBAT"
			CASE	AMBUSH_FAR_COMBAT		RETURN "AMBUSH_FAR_COMBAT"
			CASE	AMBUSH_CHASE			RETURN "AMBUSH_CHASE"
		ENDSWITCH
		RETURN "INVALID_INPUT"
	ENDFUNC
	
	FUNC STRING GET_STRING_FROM_APPROACH_STAGE(ENUM_APPROACH eVal)
		SWITCH eVal
			CASE	APPROACH_CHECK		RETURN "APPROACH_CHECK"	
			CASE	APPROACH_FRONT		RETURN "APPROACH_FRONT"	
			CASE	APPROACH_RIGHT		RETURN "APPROACH_RIGHT"
			CASE	APPROACH_BEHIND		RETURN "APPROACH_BEHIND"
			CASE	APPROACH_LEFT		RETURN "APPROACH_LEFT"
			CASE	APPROACH_TO_COMBAT	RETURN "APPROACH_TO_COMBAT"
		ENDSWITCH
		RETURN "INVALID_INPUT"
	ENDFUNC
	
#ENDIF

//-----------------------------------------------------------------------------------------------------------
//		STRUCTS
//-----------------------------------------------------------------------------------------------------------
STRUCT VEHICLE_STRUCT
	VEHICLE_INDEX 		id
	BLIP_INDEX			blip
endstruct
Struct PEDS_STRUCT
	PED_INDEX			id	
	BLIP_INDEX			blip
ENDSTRUCT
struct ENEMY_STRUCT
	ped_index				id
	blip_index				blip
	AI_BLIP_STRUCT			blipstruct
	bool					bkilled
	int 					itargeted
	bool					bCharge		= false
	BOOL					bCombat 	= FALSE
endstruct
//-----------------------------------------------------------------------------------------------------------
//		CONSTANTS
//-----------------------------------------------------------------------------------------------------------
CONST_INT				STAGE_ENTRY				0
CONST_INT				STAGE_EXIT				-1 
CONST_FLOAT				fLOSE_DISTANCE  		400.0
CONST_FLOAT				RANGE_FOR_FAR_COMBAT 	120.0
Vector					vMethLab 				= <<1358.19678, 3618.23413, 33.89066>>

VECTOR 				VDEF_T_FRONT			=	<<0,5,-0.5>>
VECTOR 				VDEF_T_RIGHT_FRONT		=	<<4,3,-0.5>> 	
VECTOR				VDEF_T_RIGHT_BACK		=	<<4,-3,-0.5>> 	
VECTOR 				VDEF_T_BACK				=	<<0,-5,-0.5>>
VECTOR 				VDEF_T_LEFT_BACK		=	<<-4,-3,-0.5>>	
VECTOR 				VDEF_T_LEFT_FRONT		=	<<-4,3,-0.5>>	
CONST_FLOAT			F_T_ENDS					3.8
CONST_FLOAT			F_T_SIDES					5.0

VECTOR 				VDEF_C_FRONT			= 	<<0,2,-0.5>>
VECTOR 				VDEF_C_RIGHT			=	<<4, 0,-0.5>>
VECTOR 				VDEF_C_BACK				=	<<0,-2.2,-0.5>>
VECTOR				VDEF_C_LEFT				=	<<-4,0,-0.5>>
CONST_FLOAT			F_C_ENDS					2.8
CONST_FLOAT			F_C_SIDES				 	4.5

CONST_FLOAT 		CRASH_SPEED					7.0

CONST_FLOAT			F_CONVOY_MAX_CRUISE_SPEED	11.0
CONST_FLOAT			F_OPTIMUM_DISTANCE			40.0
CONST_FLOAT			F_CONVOY_STOP_DISTANCE		100.0	//the distance at which the convoy lead car will stop to allow other cars to catch up.

CHASE_HINT_CAM_STRUCT 	localChaseHintCam


//-----------------------------------------------------------------------------------------------------------
//		VARIABLES
//-----------------------------------------------------------------------------------------------------------

VEHICLE_STRUCT					Vehs[MVF_NUM_OF_VEH]	//holds all of the vehicles for tis level
PEDS_STRUCT						peds[MPF_NUM_OF_PEDS]	//holds all of the peds for this level
OBJECT_INDEX					objs[NUM_OF_OBJECTS]
ENEMY_STRUCT					enemies[EPF_NUM_OF_ENEMIES]			
structPedsForConversation 		convo_struct			//holds peds in the conversation
LOCATES_HEADER_DATA 			sLocatesData
VEHICLE_INDEX					vehPlayerCar

VEHICLE_INDEX					vehStats[MVF_NUM_OF_VEH]

BLOCK_STAGE 					blockstage
ARMY_STAGE						armystage 		= ONROUTE
eAMBUSH_STAGE 					eAmbushstage	= AMBUSH_REACT
ENUM_APPROACH 					eApproach		= APPROACH_CHECK	
ENUM_GATE_STATE eGateState

FLOAT fStartGateState
FLOAT fCurrentGateState
FLOAT fTargetGateState
FLOAT fTimeSinceSpotted

BOOL bCarImpounded
BOOL bOnBase

//INT iStickyBombTime[3]
BOOL bStickyBombStatPassed		= FALSE

SCENARIO_BLOCKING_INDEX 		sbiGarage

SEQUENCE_INDEX					seq						//used to create AI sequence
int 							i

//-----------audio-----------------
int 	DialogueTimer1
int 	DialogueTimer2
bool 	b_dialogue_played

//----- stage game variable's -----
WEAPON_TYPE wtEnemyWpn	= WEAPONTYPE_ASSAULTRIFLE

bool		bdisplay_msgkill = false
bool		bdisplay_msgexit = false
bool		bleftWarn 	= false

//-----------------AI---------------------
int 		iForceChatTimer
bool		bAmbush			= false
bool		bSafe			= false
bool		bPlay_YouAgain 	= false

WEAPON_TYPE wcurrent 
 
Vector		vAmbushLocation = <<0,0,0>>
float 		f_ClosingSpeed_Lead
float 		f_ClosingSpeed_Target 
float 		f_ClosingSpeed_Tail
INT			INT_PED_ADVANCE_DELAY = 30000
int 		iChargeTimer
//int			iBreakDriveTask
int			iWarnShotdelay
int			iEscortTimer
bool		bEscort = true

int				iBase 		= 0
int				iblockstage = 0
int 			iblockTimer
PED_INDEX 		cloestped
TEXT_LABEL_63 	sWaypoint = "Rural_prep_trigger1"
STRING 			sParkWaypoint = "rural_prep_park"
//---------------------------------

//------------------------------ RELATIONSHIPS --------------------------------------------------

rel_group_hash			rel_group_trev
REL_GROUP_HASH 			rel_group_enemies

//============================== streaming ============================== 
ASSET_MANAGEMENT_DATA	sAssetData
//----------------

//Stage Management and skip stuff
STAGE_SWITCHSTATE		stageswitch				//current switching status
INT						mission_stage			//current stage
INT						mission_substage		//current substage
INT						requestedStage			//the mission stage requested by a mission_stage switch
INT						iStageTimer				//timer used for debug
Bool					bDoSkip					//trigger the skip
INT						iSkipToStage			//the stage to skip to
#if IS_DEBUG_BUILD
	MissionStageMenuTextStruct zMenuNames[MSF_NUM_OF_STAGES]
	WIDGET_GROUP_ID widget_ambush
	WIDGET_GROUP_ID widget_parent
	WIDGET_GROUP_ID widget_debug
	TEXT_WIDGET_ID twBlock
	TEXT_WIDGET_ID twAmbush
	TEXT_WIDGET_ID twArmy
	TEXT_WIDGET_ID twApproach
	TEXT_WIDGET_ID twMissionStage
	INT iMaxWantedLevel
#endif

// ===========================================================================================================
//		Termination
// ===========================================================================================================
// -----------------------------------------------------------------------------------------------------------
//		Mission Helpers
// -----------------------------------------------------------------------------------------------------------
FUNC BOOL IsEntityAlive(ENTITY_INDEX mEntity)
  	If DOES_ENTITY_EXIST(mEntity)
		if IS_ENTITY_A_VEHICLE(mEntity)
			if IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(mEntity))
				return TRUE 
			endif
		elif IS_ENTITY_A_PED(mEntity)
			if not IS_PED_INJURED(GET_PED_INDEX_FROM_ENTITY_INDEX(mEntity))
	          	return TRUE 
	    	ENDIF	
		endif           
	ENDIF
      return FALSE
ENDFUNC
PROC Point_Gameplay_cam_at_coord(float targetHeading)	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(targetHeading - GET_ENTITY_HEADING(Player_ped_id()))
endproc
FUNC ped_index MIKE()
	return peds[mpf_mike].id
ENDFUNC
func ped_index TREV()
	return peds[mpf_trev].id
endfunc
func ped_index FRANK()
	return peds[mpf_frank].id
endfunc
PROC Prep_start_Cutscene(bool bplayercontrol,SET_PLAYER_CONTROL_FLAGS ControlFlag = 0)
	DISPLAY_RADAR(false)
	DISPLAY_HUD(false)
	SET_PLAYER_CONTROL(player_id(),bplayercontrol,ControlFlag)
endproc
PROC Prep_stop_Cutscene(bool bplayercontrol, SET_PLAYER_CONTROL_FLAGS ControlFlag = 0)
	DISPLAY_RADAR(true)
	DISPLAY_HUD(true)
	SET_PLAYER_CONTROL(player_id(),bplayercontrol,ControlFlag)
endproc
func bool Grab_mission_entites()
	// Grab the target
	IF NOT DOES_ENTITY_EXIST(vehs[mvf_target].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
			vehs[mvf_target].id = g_sTriggerSceneAssets.veh[0]
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(g_sTriggerSceneAssets.veh[0])
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehs[mvf_target].id, "PS_PREP_VEHICLES_GROUP")
			SET_VEHICLE_AS_RESTRICTED(vehs[mvf_target].id,0)
			RESET_ENTITY_ALPHA(vehs[mvf_target].id)
		ENDIF
	ENDIF
	// Grab the convoy 1 
	IF NOT DOES_ENTITY_EXIST(vehs[mvf_Convoy1].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[1], TRUE, TRUE)
			vehs[mvf_Convoy1].id = g_sTriggerSceneAssets.veh[1]
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(g_sTriggerSceneAssets.veh[1])
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehs[mvf_Convoy1].id, "PS_PREP_VEHICLES_GROUP")
			RESET_ENTITY_ALPHA(vehs[mvf_Convoy1].id)
		ENDIF
	ENDIF
	// Grab the convoy 2 
	IF NOT DOES_ENTITY_EXIST(vehs[mvf_Convoy2].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[2])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[2], TRUE, TRUE)
			vehs[mvf_Convoy2].id = g_sTriggerSceneAssets.veh[2]
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(g_sTriggerSceneAssets.veh[2])
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehs[mvf_Convoy2].id, "PS_PREP_VEHICLES_GROUP")
			RESET_ENTITY_ALPHA(vehs[mvf_Convoy2].id)
		ENDIF
	ENDIF
// -----------Grab the enemies----------
	IF NOT DOES_ENTITY_EXIST(enemies[epf_trgt_driver].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
			enemies[epf_trgt_driver].id = g_sTriggerSceneAssets.ped[0]
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemies[epf_trgt_driver].id)
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(enemies[epf_c1_D].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[2], TRUE, TRUE)
			enemies[epf_c1_D].id = g_sTriggerSceneAssets.ped[2]
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemies[epf_c1_D].id)
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(enemies[epf_c1_e2].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[3], TRUE, TRUE)
			enemies[epf_c1_e2].id = g_sTriggerSceneAssets.ped[3]
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemies[epf_c1_e2].id)
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(enemies[epf_c1_e3].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[4])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[4], TRUE, TRUE)
			enemies[epf_c1_e3].id = g_sTriggerSceneAssets.ped[4]
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemies[epf_c1_e3].id)
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(enemies[epf_c1_e4].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[5])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[5], TRUE, TRUE)
			enemies[epf_c1_e4].id = g_sTriggerSceneAssets.ped[5]
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemies[epf_c1_e4].id)
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(enemies[epf_c2_D].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[6])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[6], TRUE, TRUE)
			enemies[epf_c2_D].id = g_sTriggerSceneAssets.ped[6]
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemies[epf_c2_D].id)
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(enemies[epf_c2_e2].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[7])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[7], TRUE, TRUE)
			enemies[epf_c2_e2].id = g_sTriggerSceneAssets.ped[7]
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemies[epf_c2_e2].id)
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(enemies[epf_c2_e3].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[8])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[8], TRUE, TRUE)
			enemies[epf_c2_e3].id = g_sTriggerSceneAssets.ped[8]
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemies[epf_c2_e3].id)
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(enemies[epf_c2_e4].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[9])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[9], TRUE, TRUE)
			enemies[epf_c2_e4].id = g_sTriggerSceneAssets.ped[9]
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemies[epf_c2_e4].id)
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(objs[objCrate1])
		if does_entity_exist(g_sTriggerSceneAssets.object[0])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.object[0],true,true)
			objs[objCrate1] = g_sTriggerSceneAssets.object[0]			
		endif
	endif
	IF NOT DOES_ENTITY_EXIST(objs[objCrate2])
		if does_entity_exist(g_sTriggerSceneAssets.object[1])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.object[1],true,true)
			objs[objCrate2] = g_sTriggerSceneAssets.object[1]			
		endif
	endif
	
	int enemy
	for enemy = 0 to enum_to_int(EPF_NUM_OF_ENEMIES) -1
		if IsEntityAlive(enemies[enemy].id)
			SET_PED_RELATIONSHIP_GROUP_HASH(enemies[enemy].id, rel_group_enemies)
			RESET_ENTITY_ALPHA(enemies[enemy].id)
		ENDIF
	ENDFOR
	
	sWaypoint 				= g_RHP_string_WayPoint 
	g_RHP_string_WayPoint	= ""	
	TS_SET_BLIP_COORD(STATIC_BLIP_MISSION_RURAL_P1,<<0,0,2000>>)
	
	if DOES_ENTITY_EXIST(vehs[mvf_target].id)
	and DOES_ENTITY_EXIST(vehs[mvf_Convoy1].id)
	and DOES_ENTITY_EXIST(vehs[mvf_Convoy2].id)
	and DOES_ENTITY_EXIST(enemies[epf_trgt_driver].id )
	and DOES_ENTITY_EXIST(enemies[epf_c1_D].id )
	and DOES_ENTITY_EXIST(enemies[epf_c1_e2].id )
	and DOES_ENTITY_EXIST(enemies[epf_c1_e3].id )
	and DOES_ENTITY_EXIST(enemies[epf_c1_e4].id )
	and DOES_ENTITY_EXIST(enemies[epf_c2_D].id )
	and DOES_ENTITY_EXIST(enemies[epf_c2_e2].id )
	and DOES_ENTITY_EXIST(enemies[epf_c2_e3].id )
	and DOES_ENTITY_EXIST(enemies[epf_c2_e4].id )
	and DOES_ENTITY_EXIST(objs[objCrate1])
	and DOES_ENTITY_EXIST(objs[objCrate2])
		RETURN TRUE
	endif
	
RETURN FALSE
endfunc
proc AUDIO_manage_dialouge()
	int enemy
	for enemy = enum_to_int(epf_c1_D) to enum_to_int(epf_trgt_driver)
		if isEntityalive(enemies[enemy].id)
		and get_game_timer() - dialogueTimer1 > 10000
			if not is_ambient_speech_playing(enemies[enemy].id)				
				play_ped_ambient_speech_with_voice(enemies[enemy].id,"RHP_ACAA","ArmyPed",speech_params_force)
				dialogueTimer1 = get_game_Timer()
			endif				
		endif
	endfor
	for enemy = enum_to_int(epf_c2_D) to enum_to_int(epf_c2_e4)
		if isEntityalive(enemies[enemy].id)
		and get_game_timer() - dialogueTimer2 > 18000
			if not is_ambient_speech_playing(enemies[enemy].id)				
				play_ped_ambient_speech_with_voice(enemies[enemy].id,"RHP_ADAA","ArmyPed",speech_params_force)
				dialogueTimer2 = get_game_Timer()
			endif				
		endif
	endfor
endproc

PROC RUN_AUDIO_SCENES()

	IF mission_stage < ENUM_TO_INT(msf_1_ambushing)
		IF NOT IS_AUDIO_SCENE_ACTIVE("PS_PREP_INTERCEPT_CONVOY")
			MVF_MISSION_VEHICLE_FLAGS eVeh
			REPEAT MVF_NUM_OF_VEH eVeh
				IF DOES_ENTITY_EXIST(Vehs[eVeh].id)
					IF VDIST2(GET_ENTITY_COORDS(Vehs[eVeh].id, FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))	< 1000
						START_AUDIO_SCENE("PS_PREP_INTERCEPT_CONVOY")
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ELIF mission_stage = ENUM_TO_INT(msf_1_ambushing)
		IF NOT IS_AUDIO_SCENE_ACTIVE("PS_PREP_MILITARY_TRUCK_SHOOTOUT")
			START_AUDIO_SCENE("PS_PREP_MILITARY_TRUCK_SHOOTOUT")
		ENDIF
	ELIF mission_stage >= ENUM_TO_INT(msf_1_ambushing)
		IF IS_AUDIO_SCENE_ACTIVE("PS_PREP_INTERCEPT_CONVOY")
			STOP_AUDIO_SCENE("PS_PREP_INTERCEPT_CONVOY")
			MVF_MISSION_VEHICLE_FLAGS eVeh
			REPEAT MVF_NUM_OF_VEH eVeh
				IF DOES_ENTITY_EXIST(vehs[mvf_Convoy1].id)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehs[mvf_Convoy1].id)
				ENDIF
			ENDREPEAT
			INT iTemp
			FOR iTemp = 0 TO 3
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[iTemp])
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(g_sTriggerSceneAssets.veh[iTemp])
				ENDIF
			ENDFOR
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("PS_PREP_MILITARY_TRUCK_SHOOTOUT")
			STOP_AUDIO_SCENE("PS_PREP_MILITARY_TRUCK_SHOOTOUT")
		ENDIF
	ENDIF
	
ENDPROC

proc create_enemy_in_vehicle(EPF_ENEMY_PED_FLAGS enemy,VEHICLE_INDEX veh)
	
	MODEL_NAMES		model
	VEHICLE_SEAT 	vsSeat
	
	if veh = vehs[mvf_Convoy1].id
	or veh = vehs[mvf_Convoy2].id
		model = S_M_Y_MARINE_03
	elif veh = vehs[mvf_target].id
		model = S_M_M_MARINE_01
	endif
	
	if 	enemy = epf_c1_D or enemy = epf_c2_D or enemy = epf_trgt_driver
		vsSeat = VS_DRIVER	
		wtEnemyWpn = WEAPONTYPE_PISTOL
	elif enemy = epf_c1_e2	or 	enemy = epf_c2_e2
		vsSeat = VS_FRONT_RIGHT
		wtEnemyWpn = WEAPONTYPE_CARBINERIFLE
	elif enemy = epf_c1_e3	or 	enemy = epf_c2_e3
		vsSeat = VS_BACK_LEFT		
		wtEnemyWpn = WEAPONTYPE_SMG			
	elif enemy = epf_c1_e4 or enemy = epf_c2_e4
		vsSeat = VS_BACK_RIGHT	
		wtEnemyWpn = WEAPONTYPE_CARBINERIFLE	
	endif
	
	enemies[enemy].id = CREATE_PED_INSIDE_VEHICLE(veh,pedtype_mission,model,vsSeat)
	
	SET_PED_RELATIONSHIP_GROUP_HASH(enemies[enemy].id, rel_group_enemies)
	SET_PED_AS_ENEMY(enemies[enemy].id, TRUE)	
	GIVE_WEAPON_TO_PED(enemies[enemy].id, wtEnemyWpn, INFINITE_AMMO, TRUE)
	GIVE_WEAPON_TO_PED(enemies[enemy].id, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
	SET_PED_MONEY(enemies[enemy].id,0)		
	SET_PED_COMPONENT_VARIATION(enemies[enemy].id,PED_COMP_HAIR,1,0)
	SET_PED_ALERTNESS(enemies[enemy].id,AS_ALERT)
	//combat attributes		
	SET_PED_TARGET_LOSS_RESPONSE(enemies[enemy].id,TLR_NEVER_LOSE_TARGET)
	SET_PED_COMBAT_ATTRIBUTES(enemies[enemy].id, CA_USE_COVER,true)
	SET_PED_COMBAT_ATTRIBUTES(enemies[enemy].id, CA_USE_VEHICLE, false)
	SET_PED_COMBAT_MOVEMENT(enemies[enemy].id, CM_DEFENSIVE)
	SET_PED_CONFIG_FLAG(enemies[enemy].id,PCF_PreventAutoShuffleToDriversSeat,true)
	set_ped_config_flag(enemies[enemy].id,PCF_DontBlipCop,true)
	STOP_PED_SPEAKING(enemies[enemy].id,true)	
	SET_ENTITY_LOAD_COLLISION_FLAG(enemies[enemy].id,TRUE)		
	SET_PED_ACCURACY(enemies[enemy].id,10)
//	REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_BASE_BARRIER_IN, enemies[enemy].id)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(enemies[enemy].id,true)
	
	enemies[enemy].bkilled = false
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL tDebugName = "Enemy "
		tDebugName += enum_to_int(enemy) + 1
		SET_PED_NAME_DEBUG(enemies[enemy].id, tDebugName)
	#ENDIF
	
endproc

FLOAT fBlockTimers[MVF_NUM_OF_VEH]

proc ARMY_manage_convoy()
	vector vtrgt_FrontMax
	vector vtrgt_FrontMin
	vector vc1_FrontMax 
	vector vc1_FrontMin
	vector vc2_FrontMax
	vector vc2_FrontMin 
	
	
	if IsEntityAlive(vehs[mvf_target].id)
	and IsEntityAlive(vehs[mvf_Convoy1].id)
	and IsEntityAlive(vehs[mvf_Convoy2].id)
	and IsEntityAlive(enemies[epf_trgt_driver].id)
	and IsEntityAlive(enemies[epf_c1_D].id)
	and IsEntityAlive(enemies[epf_c2_D].id)
	
		vtrgt_FrontMax 	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Vehs[mvf_target].id,<<0,clamp(GET_DISTANCE_BETWEEN_ENTITIES(Vehs[mvf_target].id,Vehs[mvf_Convoy1].id),3,25),5.5>>)
		vtrgt_FrontMin 	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Vehs[mvf_target].id,<<0,0,-6>>)
		vc1_FrontMax 	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Vehs[mvf_Convoy1].id,<<0,20,5>>)
		vc1_FrontMin	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Vehs[mvf_Convoy1].id,<<0,0,-6>>)
		vc2_FrontMax	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Vehs[mvf_Convoy2].id,<<0,clamp(GET_DISTANCE_BETWEEN_ENTITIES(Vehs[mvf_Convoy2].id,Vehs[mvf_target].id),3,25),5>>)
		vc2_FrontMin	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Vehs[mvf_Convoy2].id,<<0,0,-6>>)	
		
		switch blockstage
			case DRIVING
			
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vc1_FrontMax,vc1_FrontMin,9)
				
					IF GET_ENTITY_SPEED(Vehs[mvf_Convoy1].id) < 1
					AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 1
						fBlockTimers[mvf_Convoy1] += GET_FRAME_TIME()
					ELSE
						IF fBlockTimers[mvf_Convoy1] > 0
							fBlockTimers[mvf_Convoy1] -= GET_FRAME_TIME()*10
						ENDIF
					ENDIF
					
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),vtrgt_FrontMax,vtrgt_FrontMin,9)
				
					IF GET_ENTITY_SPEED(Vehs[mvf_target].id) < 1
					AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 1
						fBlockTimers[mvf_target] += GET_FRAME_TIME()
					ELSE
						IF fBlockTimers[mvf_target] > 0
							fBlockTimers[mvf_target] -= GET_FRAME_TIME()*10
						ENDIF
					ENDIF
					
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),vc2_FrontMax,vc2_FrontMin,9)
				
					IF GET_ENTITY_SPEED(Vehs[mvf_Convoy2].id) < 1
					AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 1
						fBlockTimers[mvf_Convoy2] += GET_FRAME_TIME()
					ELSE
						IF fBlockTimers[mvf_Convoy2] > 0
							fBlockTimers[mvf_Convoy2] -= GET_FRAME_TIME()*10
						ENDIF
					ENDIF
					
				ENDIF
			
				if fBlockTimers[mvf_Convoy1] > 1.5
				
					cloestped = enemies[epf_c1_D].id
					
					CLEAR_PED_TASKS(enemies[epf_trgt_driver].id)
					CLEAR_PED_TASKS(enemies[epf_c1_D].id)
					CLEAR_PED_TASKS(enemies[epf_c2_D].id)
					BRING_VEHICLE_TO_HALT(Vehs[mvf_target].id,4,1)
					BRING_VEHICLE_TO_HALT(Vehs[mvf_Convoy1].id,4,1)
					BRING_VEHICLE_TO_HALT(Vehs[mvf_Convoy2].id,4,1)
					
					OPEN_SEQUENCE_TASK(seq)
						if IS_PED_IN_ANY_VEHICLE(enemies[epf_c1_D].id)
							TASK_LEAVE_ANY_VEHICLE(null, 0, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
						endif
						TASK_AIM_GUN_AT_COORD(null,GET_ENTITY_COORDS(player_ped_id()),1000,false,true)
						TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(null,player_ped_id(),player_ped_id(),PEDMOVE_WALK,false,3)
						TASK_AIM_GUN_AT_ENTITY(null,player_ped_id(),-1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(enemies[epf_c1_D].id,seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					blockstage = BLOCKED
					
				elif fBlockTimers[mvf_target] > 1.5
				
					cloestped = enemies[epf_trgt_driver].id					
					CLEAR_PED_TASKS(enemies[epf_trgt_driver].id)
					CLEAR_PED_TASKS(enemies[epf_c1_D].id)
					CLEAR_PED_TASKS(enemies[epf_c2_D].id)
					BRING_VEHICLE_TO_HALT(Vehs[mvf_target].id,4,1)
					BRING_VEHICLE_TO_HALT(Vehs[mvf_Convoy1].id,4,1)
					BRING_VEHICLE_TO_HALT(Vehs[mvf_Convoy2].id,4,1)
					
					OPEN_SEQUENCE_TASK(seq)
						if IS_PED_IN_ANY_VEHICLE(enemies[epf_trgt_driver].id)
							TASK_LEAVE_ANY_VEHICLE(null, 0, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
						endif
						TASK_AIM_GUN_AT_COORD(null,GET_ENTITY_COORDS(player_ped_id()),1000,false,true)
						TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(null,player_ped_id(),player_ped_id(),PEDMOVE_WALK,false,3)
						TASK_AIM_GUN_AT_ENTITY(null,player_ped_id(),-1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(enemies[epf_trgt_driver].id,seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					blockstage = BLOCKED
					
				elif fBlockTimers[mvf_Convoy2] > 1.5
				
					cloestped = enemies[epf_c2_D].id
					
					CLEAR_PED_TASKS(enemies[epf_trgt_driver].id)
					CLEAR_PED_TASKS(enemies[epf_c1_D].id)
					CLEAR_PED_TASKS(enemies[epf_c2_D].id)
					BRING_VEHICLE_TO_HALT(Vehs[mvf_target].id,4,1)
					BRING_VEHICLE_TO_HALT(Vehs[mvf_Convoy1].id,4,1)
					BRING_VEHICLE_TO_HALT(Vehs[mvf_Convoy2].id,4,1)
					
					OPEN_SEQUENCE_TASK(seq)
						if IS_PED_IN_ANY_VEHICLE(enemies[epf_c2_D].id)
							TASK_LEAVE_ANY_VEHICLE(null, 0, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
						endif
						TASK_AIM_GUN_AT_COORD(null,GET_ENTITY_COORDS(player_ped_id()),1000,false,true)
						TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(null,player_ped_id(),player_ped_id(),PEDMOVE_WALK,false,3)	
						TASK_AIM_GUN_AT_ENTITY(null,player_ped_id(),-1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(enemies[epf_c2_D].id,seq)
					CLEAR_SEQUENCE_TASK(seq)
					
					blockstage = BLOCKED
						
				else
				
					if IS_PED_IN_ANY_VEHICLE(enemies[epf_c2_D].id)
					and IS_PED_IN_ANY_VEHICLE(enemies[epf_c1_D].id)
					and IS_PED_IN_ANY_VEHICLE(enemies[epf_trgt_driver].id)

						if not IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_Convoy1].id)
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(enemies[epf_c1_D].id,vehs[mvf_Convoy1].id,sWaypoint,
							DRIVINGMODE_STOPFORCARS,0,EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN,-1,F_CONVOY_MAX_CRUISE_SPEED)
						endif
						if GET_SCRIPT_TASK_STATUS(enemies[epf_trgt_driver].id,SCRIPT_TASK_VEHICLE_MISSION)<> PERFORMING_TASK
							TASK_VEHICLE_ESCORT(enemies[epf_trgt_driver].id,vehs[mvf_target].id,vehs[mvf_Convoy1].id,VEHICLE_ESCORT_REAR,F_CONVOY_MAX_CRUISE_SPEED,DRIVINGMODE_STOPFORCARS, -1, 20, 0)
						endif
						if GET_SCRIPT_TASK_STATUS(enemies[epf_c2_D].id,SCRIPT_TASK_VEHICLE_MISSION)<> PERFORMING_TASK
							TASK_VEHICLE_ESCORT(enemies[epf_c2_D].id,vehs[mvf_Convoy2].id,vehs[mvf_target].id,VEHICLE_ESCORT_REAR,F_CONVOY_MAX_CRUISE_SPEED,DRIVINGMODE_STOPFORCARS, -1, 20, 0)
						endif				
						
						SET_VEHICLE_USE_MORE_RESTRICTIVE_SPAWN_CHECKS(vehs[mvf_target].id,true)
						SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vehs[mvf_target].id,true)
						SET_VEHICLE_WILL_TELL_OTHERS_TO_HURRY(vehs[mvf_target].id,true)
						SET_VEHICLE_USE_MORE_RESTRICTIVE_SPAWN_CHECKS(vehs[mvf_Convoy1].id,true)
						SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vehs[mvf_Convoy1].id,true)
						SET_VEHICLE_WILL_TELL_OTHERS_TO_HURRY(vehs[mvf_Convoy1].id,true)
						SET_VEHICLE_USE_MORE_RESTRICTIVE_SPAWN_CHECKS(vehs[mvf_Convoy2].id,true)
						SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vehs[mvf_Convoy2].id,true)
						SET_VEHICLE_WILL_TELL_OTHERS_TO_HURRY(vehs[mvf_Convoy2].id,true)
						
					endif
					iblockTimer = get_game_timer()
				endif
				
			break
			case BLOCKED
				switch iblockstage
					case 0
						if not IS_PED_IN_ANY_VEHICLE(cloestped)						
							ADD_PED_FOR_DIALOGUE(convo_struct,1,cloestped,"Armyped")
//							if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"RHPAUD","RHP_GET","RHP_GET_1",CONV_PRIORITY_MEDIUM)
								iblockTimer = get_game_timer()
								fBlockTimers[mvf_Convoy1] = 0
								fBlockTimers[mvf_Convoy2] = 0
								fBlockTimers[mvf_target] = 0
								iblockstage++
//							endif
						endif						
					break
					case 1
						if get_game_timer() - iblockTimer > 8000
						and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							if not IS_PED_IN_ANY_VEHICLE(cloestped)						
								ADD_PED_FOR_DIALOGUE(convo_struct,1,cloestped,"Armyped")
								if bPlay_YouAgain
//									if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"RHPAUD","RHP_GET","RHP_GET_5",CONV_PRIORITY_MEDIUM)
										bPlay_YouAgain = false
//									endif
								else
//									if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"RHPAUD","RHP_GET","RHP_GET_2",CONV_PRIORITY_MEDIUM)
										iblockTimer = get_game_timer()
										iblockstage++
//									endif
								endif
							endif
						endif
					break
					case 2
						if get_game_timer() - iblockTimer > 8000
						and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							if not IS_PED_IN_ANY_VEHICLE(cloestped)						
								ADD_PED_FOR_DIALOGUE(convo_struct,1,cloestped,"Armyped")
								if bPlay_YouAgain
//									if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"RHPAUD","RHP_GET","RHP_GET_5",CONV_PRIORITY_MEDIUM)
										bPlay_YouAgain = false
//									endif
								else
//									if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"RHPAUD","RHP_GET","RHP_GET_3",CONV_PRIORITY_MEDIUM)	
										iblockTimer = get_game_timer()
										iblockstage++
//									endif
								endif
							endif
						endif
					break
					case 3
						if get_game_timer() - iblockTimer > 8000
						and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()						
							if not IS_PED_IN_ANY_VEHICLE(cloestped)						
								ADD_PED_FOR_DIALOGUE(convo_struct,1,cloestped,"Armyped")
								if bPlay_YouAgain
//									if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"RHPAUD","RHP_GET","RHP_GET_5",CONV_PRIORITY_MEDIUM)
										bPlay_YouAgain = false
//									endif
								else
//									if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"RHPAUD","RHP_GET","RHP_GET_4",CONV_PRIORITY_MEDIUM)	
										OPEN_SEQUENCE_TASK(seq)											
											TASK_SHOOT_AT_COORD(null,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<0.5,0,-1>>),1000,FIRING_TYPE_1_THEN_AIM)
											TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(null,player_ped_id(),player_ped_id(),PEDMOVE_WALK,false,4)	
											TASK_AIM_GUN_AT_ENTITY(null,player_ped_id(),-1)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(cloestped,seq)
										CLEAR_SEQUENCE_TASK(seq)
										iblockTimer = get_game_timer()
										iWarnShotdelay = GET_GAME_TIMER()
										iblockstage++
//									endif
								endif
							endif
						endif
					break
					case 4						
					break								
				endswitch
				
				if not IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),vtrgt_FrontMax,vtrgt_FrontMin,9)	
				and not IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),vc1_FrontMax,vc1_FrontMin,9)
				and not IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),vc2_FrontMax,vc2_FrontMin,9)
					if GET_SCRIPT_TASK_STATUS(cloestped,script_task_aim_gun_at_entity) != PERFORMING_TASK
						TASK_AIM_GUN_AT_ENTITY(cloestped,PLAYER_PED_ID(),-1)
					endif
					if timera() > 3000
						if not IS_PED_IN_ANY_VEHICLE(cloestped)
							if cloestped = enemies[epf_trgt_driver].id
								TASK_ENTER_VEHICLE(cloestped,Vehs[mvf_target].id,DEFAULT_TIME_BEFORE_WARP,VS_DRIVER,PEDMOVEBLENDRATIO_RUN)
							elif cloestped = enemies[epf_c1_D].id
								TASK_ENTER_VEHICLE(cloestped,Vehs[mvf_Convoy1].id,DEFAULT_TIME_BEFORE_WARP,VS_DRIVER,PEDMOVEBLENDRATIO_RUN)
							elif cloestped = enemies[epf_c2_D].id
								TASK_ENTER_VEHICLE(cloestped,Vehs[mvf_Convoy2].id,DEFAULT_TIME_BEFORE_WARP,VS_DRIVER,PEDMOVEBLENDRATIO_RUN)
							endif	
						endif
						blockstage = RETURNING
					endif
				else
					if iblockstage = 4
						if get_game_timer() - iblockTimer > 9000
						and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()						
							bAmbush = true							
						endif
					endif
					SETTIMERA(0)					
				endif
				
			break
			case RETURNING
				if  IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),vtrgt_FrontMax,vtrgt_FrontMin,9)	
				or  IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),vc1_FrontMax,vc1_FrontMin,9)
				or  IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),vc2_FrontMax,vc2_FrontMin,9)	
					bPlay_YouAgain = false
					blockstage = DRIVING		
				else				
					if IS_PED_IN_ANY_VEHICLE(cloestped)							
						TASK_VEHICLE_ESCORT(enemies[epf_trgt_driver].id,vehs[mvf_target].id,vehs[mvf_Convoy1].id,VEHICLE_ESCORT_REAR,F_CONVOY_MAX_CRUISE_SPEED,DRIVINGMODE_STOPFORCARS, -1, 20, 0)
						TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(enemies[epf_c1_D].id,vehs[mvf_Convoy1].id,sWaypoint,
												DRIVINGMODE_STOPFORCARS,0,EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN,-1,F_CONVOY_MAX_CRUISE_SPEED)						
						TASK_VEHICLE_ESCORT(enemies[epf_c2_D].id,vehs[mvf_Convoy2].id,vehs[mvf_target].id,VEHICLE_ESCORT_REAR,F_CONVOY_MAX_CRUISE_SPEED,DRIVINGMODE_STOPFORCARS, -1, 20, 0)						
						bPlay_YouAgain = true
						blockstage = DRIVING			
					endif
				endif
			break
		endswitch
	endif
endproc
func vehicle_index return_closest_Mission_veh(PED_INDEX ped)
	
VEHICLE_INDEX closestVeh = vehs[mvf_Convoy1].id 
float distance = 9999
	if IsEntityAlive(vehs[mvf_Convoy1].id)
		if GET_DISTANCE_BETWEEN_ENTITIES(ped,vehs[mvf_Convoy1].id) < distance
			distance = GET_DISTANCE_BETWEEN_ENTITIES(ped,vehs[mvf_Convoy1].id)
			if DOES_VEHICLE_HAVE_FREE_SEAT(vehs[mvf_Convoy1].id)
				closestVeh = vehs[mvf_Convoy1].id
			else
				closestVeh = vehs[mvf_Convoy2].id
			endif		
		endif		
	endif
	if IsEntityAlive(vehs[mvf_Convoy2].id)
		if GET_DISTANCE_BETWEEN_ENTITIES(ped,vehs[mvf_Convoy2].id) < distance
			distance = GET_DISTANCE_BETWEEN_ENTITIES(ped,vehs[mvf_Convoy2].id)
			if DOES_VEHICLE_HAVE_FREE_SEAT(vehs[mvf_Convoy2].id)
				closestVeh = vehs[mvf_Convoy2].id
			else
				closestVeh = vehs[mvf_Convoy1].id
			endif		
		endif
	endif
	return closestVeh
endfunc
func eAMBUSH_STAGE GET_AMBUSH_STAGE()
	if IsEntityAlive(player_ped_id())
	and IsEntityAlive(vehs[mvf_target].id)			
		If GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehs[mvf_target].id,vAmbushLocation) > RANGE_FOR_FAR_COMBAT
			return AMBUSH_CHASE
		elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),vAmbushLocation) <= RANGE_FOR_FAR_COMBAT
			return AMBUSH_NEAR_COMBAT
		elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(),vAmbushLocation) > RANGE_FOR_FAR_COMBAT
		and not is_ped_in_vehicle(player_ped_id(),vehs[mvf_target].id)
			return AMBUSH_far_COMBAT
		endif
	endif
return AMBUSH_NEAR_COMBAT
endfunc 
FUNC ENUM_APPROACH GET_APPROACH_ANGLE(VEHICLE_INDEX veh = null)
VECTOR 	vPlayer
VECTOR 	vTruck
FLOAT 	ANGLE	

	if veh = null
		veh = vehs[mvf_target].id
	endif

	if IsEntityAlive(player_ped_id())
	and IsEntityAlive(veh)		
		vplayer 	= GET_ENTITY_COORDS(player_ped_id())
		vtruck		= GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(veh,vplayer)
		angle		= GET_ANGLE_BETWEEN_2D_VECTORS(vplayer.x,vplayer.y,vtruck.x,vtruck.y)				
		if vtruck.x < 0
			angle *= -1
		endif	
	endif

	if 	 (angle <=  45 	 and angle >= 0)
	or 	 (angle >= -45 	 and angle <= 0)
		return APPROACH_FRONT
	elif (angle <=  135  and angle >= 0)
		return APPROACH_RIGHT
	elif (angle >=  -135 and angle <= 0)
		return APPROACH_LEFT
	else
		return APPROACH_BEHIND
	endif
//*DEFAULT*	
return APPROACH_FRONT
endfunc
PROC SET_DENFENSIVE_AREA_BY_APPROACH(EPF_ENEMY_PED_FLAGS enemy)
VEHICLE_INDEX 	VEHCHOSEN
VECTOR 			PED_VDEF
FLOAT			PED_RADIUS

	if IsEntityAlive(enemies[enemy].id)	
		if 	enemy = epf_c1_D		
			if 	 GET_APPROACH_ANGLE() = APPROACH_FRONT							
				PED_VDEF	=	VDEF_C_RIGHT
				PED_RADIUS	=	F_C_SIDES				
				VEHCHOSEN	=	vehs[mvf_Convoy1].id	
			elif GET_APPROACH_ANGLE() = APPROACH_RIGHT
				PED_VDEF	=	VDEF_C_LEFT
				PED_RADIUS	=	F_C_SIDES
				VEHCHOSEN	=	vehs[mvf_Convoy1].id
			elif GET_APPROACH_ANGLE() = APPROACH_BEHIND
				PED_VDEF	=	VDEF_T_FRONT
				PED_RADIUS	=	F_T_ENDS
				VEHCHOSEN	=	vehs[mvf_target].id
			elif GET_APPROACH_ANGLE() = APPROACH_LEFT
				PED_VDEF	=	VDEF_C_RIGHT
				PED_RADIUS	=	F_C_SIDES
				VEHCHOSEN	=	vehs[mvf_Convoy2].id
			endif	
		elif 	enemy = epf_c1_e2
			if 	 GET_APPROACH_ANGLE() = APPROACH_FRONT
				PED_VDEF	= VDEF_C_BACK
				PED_RADIUS	= F_C_ENDS
				VEHCHOSEN	= vehs[mvf_Convoy1].id
			elif GET_APPROACH_ANGLE() = APPROACH_RIGHT
				PED_VDEF	=	VDEF_T_LEFT_BACK
				PED_RADIUS	=	F_T_SIDES
				VEHCHOSEN	=	vehs[mvf_target].id
			elif GET_APPROACH_ANGLE() = APPROACH_BEHIND
				PED_VDEF	=	VDEF_T_LEFT_BACK
				PED_RADIUS	=	F_T_SIDES
				VEHCHOSEN	=	vehs[mvf_target].id 
			elif GET_APPROACH_ANGLE() = APPROACH_LEFT
				PED_VDEF	= VDEF_C_RIGHT
				PED_RADIUS	= F_C_SIDES
				VEHCHOSEN	= vehs[mvf_Convoy1].id	
			endif
		elif 	enemy = epf_c1_e3
			if 	 GET_APPROACH_ANGLE() = APPROACH_FRONT
				PED_VDEF	=	VDEF_T_BACK
				PED_RADIUS	=	F_T_ENDS
				VEHCHOSEN	=	vehs[mvf_target].id
			elif GET_APPROACH_ANGLE() = APPROACH_RIGHT
				PED_VDEF	=	VDEF_C_LEFT
				PED_RADIUS	=	F_C_SIDES
				VEHCHOSEN	=	vehs[mvf_Convoy1].id
			elif GET_APPROACH_ANGLE() = APPROACH_BEHIND
				PED_VDEF	=	VDEF_C_BACK
				PED_RADIUS	=	F_C_ENDS
				VEHCHOSEN	=	vehs[mvf_Convoy1].id
			elif GET_APPROACH_ANGLE() = APPROACH_LEFT
				PED_VDEF	=	VDEF_C_BACK
				PED_RADIUS	=	F_C_ENDS
				VEHCHOSEN	=	vehs[mvf_Convoy1].id
			endif
		elif 	enemy = epf_c1_e4
			if 	 GET_APPROACH_ANGLE() = APPROACH_FRONT
				PED_VDEF	=	VDEF_T_LEFT_BACK
				PED_RADIUS	=	F_C_SIDES
				VEHCHOSEN	=	vehs[mvf_target].id
			elif GET_APPROACH_ANGLE() = APPROACH_RIGHT
				PED_VDEF	=	VDEF_C_BACK
				PED_RADIUS	=	F_C_ENDS
				VEHCHOSEN	=	vehs[mvf_Convoy1].id
			elif GET_APPROACH_ANGLE() = APPROACH_BEHIND
				PED_VDEF	=	VDEF_C_BACK
				PED_RADIUS	=	F_C_ENDS
				VEHCHOSEN	=	vehs[mvf_Convoy1].id
			elif GET_APPROACH_ANGLE() = APPROACH_LEFT
				PED_VDEF	=	VDEF_C_BACK
				PED_RADIUS	=	F_C_ENDS
				VEHCHOSEN	=	vehs[mvf_Convoy1].id
			endif
		elif 	enemy = epf_c2_D
			if 	 GET_APPROACH_ANGLE() = APPROACH_FRONT
				PED_VDEF	=	VDEF_C_BACK
				PED_RADIUS	=	F_C_ENDS
				VEHCHOSEN	=	vehs[mvf_Convoy2].id
			elif GET_APPROACH_ANGLE() = APPROACH_RIGHT
				PED_VDEF	= 	VDEF_C_LEFT
				PED_RADIUS	=	F_C_SIDES
				VEHCHOSEN	=	vehs[mvf_Convoy2].id
			elif GET_APPROACH_ANGLE() = APPROACH_BEHIND
				PED_VDEF	=	VDEF_C_FRONT
				PED_RADIUS	=	F_C_ENDS
				VEHCHOSEN	=	vehs[mvf_Convoy2].id
			elif GET_APPROACH_ANGLE() = APPROACH_LEFT
				PED_VDEF	= 	VDEF_T_RIGHT_FRONT
				PED_RADIUS	=	F_T_SIDES
				VEHCHOSEN	=	vehs[mvf_target].id
			endif
		elif 	enemy = epf_c2_e2
			if 	 GET_APPROACH_ANGLE() = APPROACH_FRONT
				PED_VDEF	=	VDEF_C_LEFT
				PED_RADIUS	=	F_C_SIDES
				VEHCHOSEN	=	vehs[mvf_Convoy2].id
			elif GET_APPROACH_ANGLE() = APPROACH_RIGHT
				PED_VDEF	= 	VDEF_T_LEFT_FRONT
				PED_RADIUS	=	F_T_SIDES
				VEHCHOSEN	=	vehs[mvf_target].id
			elif GET_APPROACH_ANGLE() = APPROACH_BEHIND
				PED_VDEF	=	VDEF_C_FRONT
				PED_RADIUS	=	F_C_ENDS
				VEHCHOSEN	=	vehs[mvf_Convoy2].id
			elif GET_APPROACH_ANGLE() = APPROACH_LEFT
				PED_VDEF	= 	VDEF_C_RIGHT
				PED_RADIUS	=	F_C_SIDES
				VEHCHOSEN	=	vehs[mvf_Convoy2].id
			endif
		elif 	enemy = epf_c2_e3
			if 	 GET_APPROACH_ANGLE() = APPROACH_FRONT
				PED_VDEF	=	VDEF_C_LEFT
				PED_RADIUS	=	F_C_SIDES
				VEHCHOSEN	=	vehs[mvf_Convoy2].id
			elif GET_APPROACH_ANGLE() = APPROACH_RIGHT
				PED_VDEF	= 	VDEF_C_LEFT
				PED_RADIUS	=	F_C_SIDES
				VEHCHOSEN	=	vehs[mvf_Convoy2].id
			elif GET_APPROACH_ANGLE() = APPROACH_BEHIND
				PED_VDEF	=	VDEF_C_RIGHT
				PED_RADIUS	=	F_C_SIDES
				VEHCHOSEN	=	vehs[mvf_Convoy2].id
			elif GET_APPROACH_ANGLE() = APPROACH_LEFT
				PED_VDEF	= 	VDEF_T_RIGHT_BACK
				PED_RADIUS	=	F_T_SIDES
				VEHCHOSEN	=	vehs[mvf_target].id
			endif
		elif 	enemy = epf_c2_e4
			if 	 GET_APPROACH_ANGLE() = APPROACH_FRONT
				PED_VDEF	=	VDEF_T_RIGHT_FRONT
				PED_RADIUS	=	F_T_SIDES
				VEHCHOSEN	=	vehs[mvf_target].id
			elif GET_APPROACH_ANGLE() = APPROACH_RIGHT
				PED_VDEF	= 	VDEF_T_LEFT_BACK
				PED_RADIUS	=	F_T_SIDES
				VEHCHOSEN	=	vehs[mvf_target].id
			elif GET_APPROACH_ANGLE() = APPROACH_BEHIND
				PED_VDEF	=	VDEF_C_RIGHT
				PED_RADIUS	=	F_C_SIDES
				VEHCHOSEN	=	vehs[mvf_Convoy2].id
			elif GET_APPROACH_ANGLE() = APPROACH_LEFT
				PED_VDEF	=	VDEF_C_RIGHT
				PED_RADIUS	=	F_C_SIDES
				VEHCHOSEN	=	vehs[mvf_Convoy2].id
			endif
		elif 	enemy = epf_trgt_driver
			if 	 GET_APPROACH_ANGLE() = APPROACH_FRONT
				PED_VDEF	=	VDEF_T_RIGHT_BACK
				PED_RADIUS	=	F_T_SIDES
				VEHCHOSEN	=	vehs[mvf_target].id
			elif GET_APPROACH_ANGLE() = APPROACH_RIGHT
				PED_VDEF	=	VDEF_T_LEFT_FRONT
				PED_RADIUS	=	F_T_SIDES
				VEHCHOSEN	=	vehs[mvf_target].id
			elif GET_APPROACH_ANGLE() = APPROACH_BEHIND
				PED_VDEF	=	VDEF_T_LEFT_FRONT
				PED_RADIUS	=	F_T_SIDES
				VEHCHOSEN	=	vehs[mvf_target].id
			elif GET_APPROACH_ANGLE() = APPROACH_LEFT
				PED_VDEF	=	VDEF_T_RIGHT_FRONT
				PED_RADIUS	=	F_T_SIDES
				VEHCHOSEN	=	vehs[mvf_target].id
			endif
		endif
		
		if not enemies[enemy].bCharge	
			//pick chosen else default 
			if IsEntityAlive(VEHCHOSEN)	
				SET_PED_SPHERE_DEFENSIVE_AREA(enemies[enemy].id,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(VEHCHOSEN,PED_VDEF),PED_RADIUS)
			elif IsEntityAlive(Vehs[mvf_target].id)	
				if 	 GET_APPROACH_ANGLE(Vehs[mvf_target].id) = APPROACH_FRONT
					SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(enemies[enemy].id,Vehs[mvf_target].id,VDEF_T_BACK,F_T_ENDS)
				elif GET_APPROACH_ANGLE(Vehs[mvf_target].id) = APPROACH_RIGHT
					SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(enemies[enemy].id,Vehs[mvf_target].id,<<-6,0,-0.5>>,6)
				elif GET_APPROACH_ANGLE(Vehs[mvf_target].id) = APPROACH_BEHIND
					SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(enemies[enemy].id,Vehs[mvf_target].id,VDEF_T_FRONT,F_T_ENDS)	
				elif GET_APPROACH_ANGLE(Vehs[mvf_target].id) = APPROACH_LEFT
					SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(enemies[enemy].id,Vehs[mvf_target].id,<<6,0,-0.5>>,6)	
				endif	
			endif
		endif
	endif
	
endproc

FUNC BOOL IS_BAD_EXPLOSION_IN_AREA(VECTOR vAreaMax, VECTOR vAreaMin, VECTOR vCentre, FLOAT fRadius = 0.0, BOOL bIsDoorBlownCheck = FALSE)

	BOOL bRet = FALSE
	
	IF NOT IS_VECTOR_ZERO(vAreaMax+vAreaMin)
		IF IS_EXPLOSION_IN_AREA(EXP_TAG_TANKSHELL, 			vAreaMin, vAreaMax)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_STICKYBOMB, 		vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_SMOKE_GRENADE, 	vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_ROCKET, 			vAreaMin, vAreaMax)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_PROPANE, 			vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_PLANE_ROCKET, 		vAreaMin, vAreaMax)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_PROGRAMMABLEAR, 	vAreaMin, vAreaMax)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_PETROL_PUMP, 		vAreaMin, vAreaMax)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_MOLOTOV, 			vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_HI_OCTANE,			vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADE, 			vAreaMin, vAreaMax)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADELAUNCHER, 	vAreaMin, vAreaMax)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_GAS_CANISTER, 		vAreaMin, vAreaMax)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_GAS_TANK, 			vAreaMin, vAreaMax)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_DIR_GAS_CANISTER, 	vAreaMin, vAreaMax)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_DIR_FLAME_EXPLODE, vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_DIR_FLAME, 		vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_CAR , 				vAreaMin, vAreaMax)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_BZGAS , 			vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR (IS_EXPLOSION_IN_AREA(EXP_TAG_BULLET , 			vAreaMin, vAreaMax) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_BOAT , 				vAreaMin, vAreaMax)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_BIKE , 				vAreaMin, vAreaMax)
		OR IS_EXPLOSION_IN_AREA(EXP_TAG_BARREL , 			vAreaMin, vAreaMax)
			bRet = TRUE
		ENDIF
	ELSE
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_TANKSHELL, 			vCentre, fRadius)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_STICKYBOMB, 			vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_SMOKE_GRENADE, 		vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_ROCKET, 				vCentre, fRadius)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_PROPANE, 			vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_PLANE_ROCKET, 		vCentre, fRadius)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_PROGRAMMABLEAR, 		vCentre, fRadius)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_PETROL_PUMP, 			vCentre, fRadius)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_MOLOTOV, 			vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_HI_OCTANE,			vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADE, 				vCentre, fRadius)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADELAUNCHER, 		vCentre, fRadius)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GAS_CANISTER, 		vCentre, fRadius)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GAS_TANK, 			vCentre, fRadius)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_GAS_CANISTER, 	vCentre, fRadius)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_FLAME_EXPLODE,	vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_FLAME, 			vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_CAR , 				vCentre, fRadius)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_BZGAS , 				vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR (IS_EXPLOSION_IN_SPHERE(EXP_TAG_BULLET , 			vCentre, fRadius) AND NOT bIsDoorBlownCheck)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_BOAT , 				vCentre, fRadius)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_BIKE , 				vCentre, fRadius)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_BARREL , 				vCentre, fRadius)
			bRet = TRUE										
		ENDIF
	ENDIF
	
	RETURN bRet
	
ENDFUNC

proc AMBUSH_CHECKS()
// ------------------------------------------------------------- AMBUSH CHECKS ----------------------------------------------------------------------------------		
INT enemy		
	//---------------------------- peds attacked ----------------------------	
	GET_CURRENT_PED_WEAPON(player_ped_id(),wcurrent)
		
	for enemy = 0 to enum_to_int(EPF_NUM_OF_ENEMIES) - 1
		if IsEntityAlive(enemies[enemy].id)
			if IS_PED_IN_COMBAT(enemies[enemy].id)	
			or IS_PED_IN_MELEE_COMBAT(enemies[enemy].id)
			or IS_PED_BEING_STUNNED(enemies[enemy].id)
			or IS_PED_BEING_STEALTH_KILLED(enemies[enemy].id)
			or IS_PED_BEING_JACKED(enemies[enemy].id)
			or HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(enemies[enemy].id,player_ped_id())	
				bAmbush = true
			endif
			if get_game_timer() - iWarnShotdelay > 800
				if HAS_PED_RECEIVED_EVENT(enemies[enemy].id,EVENT_SHOT_FIRED_BULLET_IMPACT)
				or HAS_PED_RECEIVED_EVENT(enemies[enemy].id,EVENT_SHOT_FIRED_WHIZZED_BY)
					bAmbush = true									
				endif	
			endif
			if HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(enemies[enemy].id,player_ped_id())
			and (IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),enemies[enemy].id)or IS_PLAYER_TARGETTING_ENTITY(player_id(),enemies[enemy].id))
				IF wcurrent != WEAPONTYPE_INVALID
				 	if GET_WEAPONTYPE_GROUP(wcurrent) != WEAPONGROUP_MELEE
				 	and wcurrent != WEAPONTYPE_UNARMED
						if get_game_timer() - enemies[enemy].itargeted > 800
							bAmbush = true
						endif
					endif
				ENDIF
			else
				enemies[enemy].itargeted = get_game_Timer()
			endif
		else
			bAmbush = true
		endif
	endfor
	
	//---------------------------- Front veh attacked ----------------------------	
	if IsEntityAlive(vehs[mvf_Convoy1].id)
	
		if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehs[mvf_Convoy1].id,player_ped_id(),false)				
		or HAS_VEHICLE_GOT_PROJECTILE_ATTACHED(player_ped_id(),vehs[mvf_Convoy1].id)
		or IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_Convoy1].id,true)			
		or IS_PED_ON_SPECIFIC_VEHICLE(player_ped_id(),vehs[mvf_Convoy1].id)
			bAmbush = true
		elif IS_PED_IN_ANY_VEHICLE(player_ped_id())
			if IS_PED_IN_MODEL(player_ped_id(),TOWTRUCK)
			or IS_PED_IN_MODEL(player_ped_id(),TOWTRUCK2)
				if IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(player_ped_id()),vehs[mvf_Convoy1].id)
					bAmbush = true
				endif
			endif
		endif
		
		IF IS_BAD_EXPLOSION_IN_AREA(<<0,0,0>>, <<0,0,0>>, GET_ENTITY_COORDS(vehs[mvf_Convoy1].id, FALSE), 30)
			bAmbush = true
		ENDIF
		
		if IS_PED_IN_ANY_VEHICLE(player_ped_id())
			if IS_ENTITY_TOUCHING_ENTITY(player_ped_id(),vehs[mvf_Convoy1].id)
			and f_ClosingSpeed_Lead >= CRASH_SPEED
				bAmbush = true			
			endif
			// Calculate closing velocity for this frame
			VECTOR vContactNormal_lead
			vContactNormal_lead = NORMALISE_VECTOR(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(player_ped_id()), FALSE) - GET_ENTITY_COORDS(vehs[mvf_Convoy1].id, FALSE))
			VECTOR vVelBMinusVelA_lead
			vVelBMinusVelA_lead = GET_ENTITY_VELOCITY(vehs[mvf_Convoy1].id) - GET_ENTITY_VELOCITY(GET_VEHICLE_PED_IS_IN(player_ped_id()))
			f_ClosingSpeed_Lead = DOT_PRODUCT(vVelBMinusVelA_lead, vContactNormal_lead)
		endif
	else
		bAmbush = true
	endif
	
	//---------------------------- tail veh attacked ----------------------------	
	if IsEntityAlive(vehs[mvf_Convoy2].id)										
		if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehs[mvf_Convoy2].id,player_ped_id(),false)		
		or HAS_VEHICLE_GOT_PROJECTILE_ATTACHED(player_ped_id(),vehs[mvf_Convoy2].id)
		or IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_Convoy2].id,true)
		or IS_PED_ON_SPECIFIC_VEHICLE(player_ped_id(),vehs[mvf_Convoy2].id)			
			bAmbush = true
		elif IS_PED_IN_ANY_VEHICLE(player_ped_id())
			if IS_PED_IN_MODEL(player_ped_id(),TOWTRUCK)
			or IS_PED_IN_MODEL(player_ped_id(),TOWTRUCK2)
				if IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(player_ped_id()),vehs[mvf_Convoy2].id)
					bAmbush = true
				endif
			endif
		endif
		
		IF IS_BAD_EXPLOSION_IN_AREA(<<0,0,0>>, <<0,0,0>>, GET_ENTITY_COORDS(vehs[mvf_Convoy2].id, FALSE), 30)
			bAmbush = true
		ENDIF
		
		if IS_PED_IN_ANY_VEHICLE(player_ped_id())
			if IS_ENTITY_TOUCHING_ENTITY(player_ped_id(),vehs[mvf_Convoy2].id)
			and f_ClosingSpeed_Tail >= CRASH_SPEED
				bAmbush = true			
			endif
			// Calculate closing velocity for this frame
			VECTOR vContactNormal_tail 
			vContactNormal_tail = NORMALISE_VECTOR(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(player_ped_id()), FALSE) - GET_ENTITY_COORDS(vehs[mvf_Convoy2].id, FALSE))
			VECTOR vVelBMinusVelA_tail 
			vVelBMinusVelA_tail = GET_ENTITY_VELOCITY(vehs[mvf_Convoy2].id) - GET_ENTITY_VELOCITY(GET_VEHICLE_PED_IS_IN(player_ped_id()))
			f_ClosingSpeed_Tail = DOT_PRODUCT(vVelBMinusVelA_tail, vContactNormal_tail)
		endif
	else
		bAmbush = true
	endif
	
	//---------------------------- transport veh attacked ----------------------------	
	if IsEntityAlive(vehs[mvf_target].id)
	
		if GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),player_ped_id()) < 45
		and GET_PLAYER_WANTED_LEVEL(player_id()) > 0
			bAmbush = true
		endif
		
		IF IS_BAD_EXPLOSION_IN_AREA(<<0,0,0>>, <<0,0,0>>, GET_ENTITY_COORDS(vehs[mvf_target].id, FALSE), 30)
			bAmbush = true
		ENDIF
		
		if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehs[mvf_target].id,player_ped_id(),false)	
		or HAS_VEHICLE_GOT_PROJECTILE_ATTACHED(player_ped_id(),vehs[mvf_target].id)
		or IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_target].id,true)
		or IS_PED_ON_SPECIFIC_VEHICLE(player_ped_id(),vehs[mvf_target].id)
			bAmbush = true
		elif IS_PED_IN_ANY_VEHICLE(player_ped_id())
			if IS_PED_IN_MODEL(player_ped_id(),TOWTRUCK)
			or IS_PED_IN_MODEL(player_ped_id(),TOWTRUCK2)
				if IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(player_ped_id()),vehs[mvf_target].id)
					bAmbush = true
				endif
			endif
		endif
		if IS_PED_IN_ANY_VEHICLE(player_ped_id())
			if IS_ENTITY_TOUCHING_ENTITY(player_ped_id(),vehs[mvf_target].id)
			and f_ClosingSpeed_Target >= CRASH_SPEED
				bAmbush = true			
			endif
			// Calculate closing velocity for this frame
			VECTOR vContactNormal_target 
			vContactNormal_target = NORMALISE_VECTOR(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(player_ped_id()), FALSE) - GET_ENTITY_COORDS(vehs[mvf_target].id, FALSE))
			VECTOR vVelBMinusVelA_target 
			vVelBMinusVelA_target = GET_ENTITY_VELOCITY(vehs[mvf_target].id) - GET_ENTITY_VELOCITY(GET_VEHICLE_PED_IS_IN(player_ped_id()))
			f_ClosingSpeed_Target = DOT_PRODUCT(vVelBMinusVelA_target, vContactNormal_target)
		endif
	 	IF IS_PLAYER_USING_A_TAXI()
			VEHICLE_INDEX taxiVehicle
			taxiVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(taxiVehicle)
				if IS_ENTITY_TOUCHING_ENTITY(taxiVehicle,vehs[mvf_target].id)
				//and f_ClosingSpeed_Target >= CRASH_SPEED
					bAmbush = true			
				endif
				// Calculate closing velocity for this frame
				VECTOR vContactNormal_target 
				vContactNormal_target = NORMALISE_VECTOR(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(player_ped_id()), FALSE) - GET_ENTITY_COORDS(vehs[mvf_target].id, FALSE))
				VECTOR vVelBMinusVelA_target 
				vVelBMinusVelA_target = GET_ENTITY_VELOCITY(vehs[mvf_target].id) - GET_ENTITY_VELOCITY(GET_VEHICLE_PED_IS_IN(player_ped_id()))
				f_ClosingSpeed_Target = DOT_PRODUCT(vVelBMinusVelA_target, vContactNormal_target)
			ENDIF
		ENDIF
	else
		bAmbush = true
	endif			
endproc
PROC ARMY_COMBAT()
	INT 	ENEMY
	switch armystage
		case ONROUTE
			if not bAmbush
				ARMY_manage_convoy()
				AMBUSH_CHECKS()
				
				if IsEntityAlive(enemies[epf_trgt_driver].id)
				and IsEntityAlive(enemies[epf_c1_D].id)
				and IsEntityAlive(enemies[epf_c2_D].id)
				and IsEntityAlive(vehs[mvf_Convoy1].id)
				and IsEntityAlive(vehs[mvf_Convoy2].id)
				and IsEntityAlive(vehs[mvf_target].id)
					
					if is_entity_in_angled_area(vehs[mvf_target].id,<<-1521.82935, 2725.00342, 16.64367>>,<<-1600.76294, 2794.87549, 21.45629>>,30)
					and is_ped_in_vehicle(enemies[epf_trgt_driver].id,vehs[mvf_target].id)	
						armystage = ARMYBASE
						iBase = 0
					endif
					
					// -------------------- WAYPOINT SPEED ----------------------
					
					//sets a sensible drive task speed to allow cars to catch up without stopping completely.
					
					FLOAT fDistToFurthestVehicle
					fDistToFurthestVehicle = VDIST(GET_ENTITY_COORDS(vehs[mvf_Convoy1].id, FALSE), GET_ENTITY_COORDS(vehs[mvf_target].id, FALSE))
					IF VDIST(GET_ENTITY_COORDS(vehs[mvf_Convoy1].id, FALSE), GET_ENTITY_COORDS(vehs[mvf_Convoy2].id, FALSE)) > fDistToFurthestVehicle 
						fDistToFurthestVehicle = VDIST(GET_ENTITY_COORDS(vehs[mvf_Convoy1].id, FALSE), GET_ENTITY_COORDS(vehs[mvf_Convoy2].id, FALSE))
					ENDIF
					
					FLOAT fSpeed
					FLOAT fYintercept
					fYintercept = F_CONVOY_MAX_CRUISE_SPEED + ((F_CONVOY_MAX_CRUISE_SPEED/(F_CONVOY_STOP_DISTANCE-F_OPTIMUM_DISTANCE))*F_OPTIMUM_DISTANCE)
					fSpeed = CLAMP(((-F_CONVOY_MAX_CRUISE_SPEED/(F_CONVOY_STOP_DISTANCE-F_OPTIMUM_DISTANCE))*fDistToFurthestVehicle) + fYintercept, 0, F_CONVOY_MAX_CRUISE_SPEED)
					IF IS_PED_SITTING_IN_VEHICLE(enemies[epf_c1_D].id, vehs[mvf_Convoy1].id)
						SET_DRIVE_TASK_CRUISE_SPEED(enemies[epf_c1_D].id, fSpeed)
					ENDIF
					
					#IF IS_DEBUG_BUILD
										
						DRAW_DEBUG_TEXT(GET_STRING_FROM_FLOAT(fSpeed), GET_ENTITY_COORDS(vehs[mvf_Convoy1].id, FALSE) + <<0,0,3.5>>)
						
						REPEAT COUNT_OF(Vehs) i
							IF DOES_ENTITY_EXIST(vehs[i].id)
								DRAW_DEBUG_LINE(GET_ENTITY_COORDS(vehs[i].id, FALSE), GET_ENTITY_COORDS(vehs[i].id, FALSE) + <<0,0,3>>, 0, 255, 0)
								DRAW_DEBUG_TEXT(GET_STRING_FROM_FLOAT(GET_ENTITY_SPEED(vehs[i].id)), GET_ENTITY_COORDS(vehs[i].id, FALSE) + <<0,0,3>>, 0, 255, 0)
							ENDIF
						ENDREPEAT
						
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
							SET_VEHICLE_FORWARD_SPEED(vehs[mvf_Convoy2].id, 0)
						ENDIF
						
						IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
							SET_VEHICLE_FORWARD_SPEED(vehs[mvf_target].id, 0)
						ENDIF
						
					#ENDIF
					
				endif		
			else		
				if IsEntityAlive(vehs[mvf_target].id)
					vAmbushLocation	= GET_ENTITY_COORDS(vehs[mvf_target].id, FALSE)
				endif
				eApproach 	 = APPROACH_CHECK
				eAmbushstage = AMBUSH_REACT
				armystage 	 = AMBUSH				
			endif
		break
//----------------------------------------------------------------- ARMY BASE -----------------------------------------------------------------			
		case ARMYBASE			
			if not bAmbush
				AMBUSH_CHECKS()
				if IsEntityAlive(enemies[epf_trgt_driver].id)
				and IsEntityAlive(enemies[epf_c1_D].id)
				and IsEntityAlive(enemies[epf_c2_D].id)
				and IsEntityAlive(vehs[mvf_Convoy1].id)
				and IsEntityAlive(vehs[mvf_Convoy2].id)
				and IsEntityAlive(vehs[mvf_target].id)
					switch iBase			
						case 0
							if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(enemies[epf_c1_D].id,SCRIPT_TASK_PERFORM_SEQUENCE)
								IF eGateState = GATE_STATE_GATE_OPEN
								OR (eGateState = GATE_STATE_GATE_OPENING AND fCurrentGateState > 0.8)
									SEQUENCE_INDEX seqTemp
									OPEN_SEQUENCE_TASK(seqTemp)
									TASK_VEHICLE_MISSION_COORS_TARGET(NULL,vehs[mvf_Convoy1].id,<<-1785.4601, 3169.6255, 31.9300>>,MISSION_GOTO,10,DRIVINGMODE_STOPFORCARS,3,3)
									TASK_VEHICLE_MISSION_COORS_TARGET(NULL,vehs[mvf_Convoy1].id,<<-1885.87463, 3006.15186, 31.81027>>,MISSION_GOTO,10,DRIVINGMODE_STOPFORCARS,-1,-1)
									CLOSE_SEQUENCE_TASK(seqTemp)
									TASK_PERFORM_SEQUENCE(enemies[epf_c1_D].id, seqTemp)
									CLEAR_SEQUENCE_TASK(seqTemp)
								ENDIF
							endif
							if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(enemies[epf_trgt_driver].id,SCRIPT_TASK_VEHICLE_MISSION)	
								TASK_VEHICLE_ESCORT(enemies[epf_trgt_driver].id,vehs[mvf_target].id,vehs[mvf_Convoy1].id,VEHICLE_ESCORT_REAR,F_CONVOY_MAX_CRUISE_SPEED,DRIVINGMODE_STOPFORCARS, -1, 20, 0)					
							endif
							if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(enemies[epf_c2_D].id,SCRIPT_TASK_VEHICLE_MISSION)	
								TASK_VEHICLE_ESCORT(enemies[epf_c2_D].id,vehs[mvf_Convoy2].id,vehs[mvf_target].id,VEHICLE_ESCORT_REAR,F_CONVOY_MAX_CRUISE_SPEED,DRIVINGMODE_STOPFORCARS, -1, 20, 0)
							endif
							if IS_ENTITY_AT_COORD(enemies[epf_c1_D].id,<<-1885.87463, 3006.15186, 31.81027>>,<<5,5,5>>)
								for enemy = 0 to enum_to_int(EPF_NUM_OF_ENEMIES)-1
									if IsEntityAlive(enemies[enemy].id)
										SET_PED_AS_COP(enemies[enemy].id)
										SET_PED_COMBAT_ATTRIBUTES(enemies[enemy].id,CA_LEAVE_VEHICLES,true)
										set_ped_combat_attributes(enemies[enemy].id, ca_use_vehicle,true)
										if enemy = enum_to_int(epf_c1_D)
										or enemy = enum_to_int(epf_c2_D)
										or enemy = enum_to_int(epf_trgt_driver)
											TASK_LEAVE_ANY_VEHICLE(enemies[enemy].id,200, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
										elif enemy = enum_to_int(epf_c1_e2)
										or enemy = enum_to_int(epf_c2_e2)
										or enemy = enum_to_int(epf_c2_e3)
											TASK_LEAVE_ANY_VEHICLE(enemies[enemy].id, 0, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
										else 
											TASK_LEAVE_ANY_VEHICLE(enemies[enemy].id,1000, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
										endif
										SAFE_RELEASE_PED(enemies[enemy].id,true,true,true)
									endif
								endfor
								iBase++
							endif
						break
						case 1
							
							
						break
					endswitch
				endif
			else		
				if IsEntityAlive(vehs[mvf_target].id)
					vAmbushLocation	= GET_ENTITY_COORDS(vehs[mvf_target].id)
				endif
				eApproach 	 = APPROACH_CHECK
				eAmbushstage = AMBUSH_REACT
				armystage 	 = AMBUSH				
			endif	
		break
//----------------------------------------------------------------- COMBAT -----------------------------------------------------------------		
		case AMBUSH	
			ped_index AmbPed
			GET_CLOSEST_PED(vAmbushLocation,50,true,false,ambPed)
			if IsEntityAlive(ambped)
				SET_PED_FLEE_ATTRIBUTES(ambPed,FA_USE_VEHICLE,false)
				SET_PED_FLEE_ATTRIBUTES(ambPed,FA_DISABLE_EXIT_VEHICLE,false)		
			endif
			
			switch eAmbushstage	
				case AMBUSH_REACT
					switch eApproach
						case APPROACH_CHECK
							eApproach = GET_APPROACH_ANGLE(vehs[mvf_target].id)
						break
						case APPROACH_FRONT
							Println("APPROACH_FRONT")
							// Leader
							if IsEntityAlive(enemies[epf_c1_D].id)
							and IsEntityAlive(vehs[mvf_Convoy1].id)
								if IS_PED_IN_VEHICLE(enemies[epf_c1_D].id,vehs[mvf_Convoy1].id)
									OPEN_SEQUENCE_TASK(seq)
										if GET_ENTITY_SPEED(enemies[epf_c1_D].id) > 5
											TASK_VEHICLE_TEMP_ACTION(null,vehs[mvf_Convoy1].id,TEMPACT_HANDBRAKETURNRIGHT,1000)										
										else
											TASK_VEHICLE_TEMP_ACTION(null,vehs[mvf_Convoy1].id,TEMPACT_REVERSE_RIGHT,1500)
										endif
										TASK_LEAVE_ANY_VEHICLE(null,720, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
//										TASK_COMBAT_PED(null,player_ped_id())
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(enemies[epf_c1_D].id,seq)
									CLEAR_SEQUENCE_TASK(seq)
								endif
							endif
							// target
							if IsEntityAlive(enemies[epf_trgt_driver].id)
							and IsEntityAlive(vehs[mvf_target].id)
								if IS_PED_IN_VEHICLE(enemies[epf_trgt_driver].id,vehs[mvf_target].id)
									OPEN_SEQUENCE_TASK(seq)
										if GET_ENTITY_SPEED(enemies[epf_trgt_driver].id) > 5
											TASK_VEHICLE_TEMP_ACTION(null,vehs[mvf_target].id,TEMPACT_HANDBRAKETURNLEFT,2500)										
										else
											TASK_VEHICLE_TEMP_ACTION(null,vehs[mvf_target].id,TEMPACT_REVERSE_LEFT,1500)
										endif
										TASK_LEAVE_ANY_VEHICLE(null,500, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
//										TASK_COMBAT_PED(null,player_ped_id())
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(enemies[epf_trgt_driver].id,seq)
									CLEAR_SEQUENCE_TASK(seq)
								endif	
							endif
							// Tail
							if IsEntityAlive(enemies[epf_c2_D].id)
							and IsEntityAlive(vehs[mvf_Convoy2].id)
							and IsEntityAlive(vehs[mvf_Convoy1].id)
								if IS_PED_IN_VEHICLE(enemies[epf_c2_D].id,vehs[mvf_Convoy2].id)
									OPEN_SEQUENCE_TASK(seq)
										IF VDIST2(GET_ENTITY_COORDS(vehs[mvf_Convoy2].id, FALSE), GET_ENTITY_COORDS(vehs[mvf_Convoy1].id, FALSE)) < 1000
											TASK_VEHICLE_DRIVE_TO_COORD(null,vehs[mvf_Convoy2].id,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehs[mvf_Convoy1].id,<<-3,0,0>>),10,DRIVINGSTYLE_ACCURATE,CRUSADER,DRIVINGMODE_AVOIDCARS,4,-1)
										ENDIF
										TASK_VEHICLE_TEMP_ACTION(null,vehs[mvf_Convoy2].id,TEMPACT_HANDBRAKETURNRIGHT,1000)
										TASK_LEAVE_ANY_VEHICLE(null, 0, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
//										TASK_COMBAT_PED(null,player_ped_id())
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(enemies[epf_c2_D].id,seq)
									CLEAR_SEQUENCE_TASK(seq)
								endif
							endif
//							iBreakDriveTask = get_Game_timer()
							eAmbushstage = AMBUSH_START_COMBAT
						break
						case APPROACH_RIGHT
							Println("APPROACH_RIGHT")
							// Leader 
							if IsEntityAlive(enemies[epf_c1_D].id)
							and IsEntityAlive(vehs[mvf_Convoy1].id)
								if IS_PED_IN_VEHICLE(enemies[epf_c1_D].id,vehs[mvf_Convoy1].id)
									OPEN_SEQUENCE_TASK(seq)										
											TASK_VEHICLE_TEMP_ACTION(null,vehs[mvf_Convoy1].id,TEMPACT_HANDBRAKETURNRIGHT,1500)
											TASK_LEAVE_ANY_VEHICLE(null,900, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
//											TASK_COMBAT_PED(null,player_ped_id())
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(enemies[epf_c1_D].id,seq)
									CLEAR_SEQUENCE_TASK(seq)	
								endif
							endif
							// target
							if IsEntityAlive(enemies[epf_trgt_driver].id)
							and IsEntityAlive(vehs[mvf_target].id)
								if IS_PED_IN_VEHICLE(enemies[epf_trgt_driver].id,vehs[mvf_target].id)
									OPEN_SEQUENCE_TASK(seq)
										TASK_VEHICLE_TEMP_ACTION(null,vehs[mvf_target].id,TEMPACT_HANDBRAKETURNRIGHT,1000)
										TASK_LEAVE_ANY_VEHICLE(null,100, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
//										TASK_COMBAT_PED(null,player_ped_id())
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(enemies[epf_trgt_driver].id,seq)
									CLEAR_SEQUENCE_TASK(seq)
								endif
							endif
							// Tail
							if IsEntityAlive(enemies[epf_c2_D].id)
							and IsEntityAlive(vehs[mvf_Convoy2].id)
							and IsEntityAlive(vehs[mvf_target].id)
								if IS_PED_IN_VEHICLE(enemies[epf_c2_D].id,vehs[mvf_Convoy2].id)
									OPEN_SEQUENCE_TASK(seq)
										IF VDIST2(GET_ENTITY_COORDS(vehs[mvf_Convoy2].id, FALSE), GET_ENTITY_COORDS(vehs[mvf_target].id, FALSE)) < 1000
											TASK_VEHICLE_DRIVE_TO_COORD(null,vehs[mvf_Convoy2].id,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehs[mvf_target].id,<<-2,1,0>>),10,DRIVINGSTYLE_ACCURATE,CRUSADER,DRIVINGMODE_AVOIDCARS,4,-1)
										ENDIF
										TASK_LEAVE_ANY_VEHICLE(null,120, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
//										TASK_COMBAT_PED(null,player_ped_id())
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(enemies[epf_c2_D].id,seq)
									CLEAR_SEQUENCE_TASK(seq)
								endif
							endif
//							iBreakDriveTask = get_Game_timer()
							eAmbushstage = AMBUSH_START_COMBAT
						break
						case APPROACH_BEHIND
							Println("APPROACH_BACK")
							// Leader
							if IsEntityAlive(enemies[epf_c1_D].id)
							and IsEntityAlive(vehs[mvf_Convoy1].id)
							and IsEntityAlive(vehs[mvf_Convoy2].id)
								if IS_PED_IN_VEHICLE(enemies[epf_c1_D].id,vehs[mvf_Convoy1].id)
									OPEN_SEQUENCE_TASK(seq)
										TASK_VEHICLE_TEMP_ACTION(null,vehs[mvf_Convoy1].id,TEMPACT_HANDBRAKESTRAIGHT,500)	
										IF VDIST2(GET_ENTITY_COORDS(vehs[mvf_Convoy1].id, FALSE), GET_ENTITY_COORDS(vehs[mvf_Convoy2].id, FALSE)) < 1000
											TASK_VEHICLE_DRIVE_TO_COORD(null,vehs[mvf_Convoy1].id,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehs[mvf_Convoy2].id,<<2,0,0>>),10,DRIVINGSTYLE_ACCURATE,CRUSADER,DRIVINGMODE_AVOIDCARS,4,-1)
										ENDIF
										TASK_VEHICLE_TEMP_ACTION(null,vehs[mvf_Convoy1].id,TEMPACT_HANDBRAKETURNRIGHT,1000)	
										TASK_LEAVE_ANY_VEHICLE(null,420, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
//										TASK_COMBAT_PED(null,player_ped_id())
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(enemies[epf_c1_D].id,seq)
									CLEAR_SEQUENCE_TASK(seq)
								endif
							endif
							// target
							if IsEntityAlive(enemies[epf_trgt_driver].id)
							and IsEntityAlive(vehs[mvf_target].id)
								if IS_PED_IN_VEHICLE(enemies[epf_trgt_driver].id,vehs[mvf_target].id)
									if GET_ENTITY_SPEED(enemies[epf_trgt_driver].id) > 5
										OPEN_SEQUENCE_TASK(seq)									
												TASK_VEHICLE_TEMP_ACTION(null,vehs[mvf_target].id,TEMPACT_HANDBRAKETURNRIGHT,1500)
												TASK_LEAVE_ANY_VEHICLE(null,380, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
//												TASK_COMBAT_PED(null,player_ped_id())
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(enemies[epf_trgt_driver].id,seq)
										CLEAR_SEQUENCE_TASK(seq)
									endif	
								endif
							endif
							// Tail
							if IsEntityAlive(enemies[epf_c2_D].id)
							and IsEntityAlive(vehs[mvf_Convoy2].id)
								if IS_PED_IN_VEHICLE(enemies[epf_c2_D].id,vehs[mvf_Convoy2].id)
									if GET_ENTITY_SPEED(enemies[epf_c2_D].id) > 5
										OPEN_SEQUENCE_TASK(seq)									
												TASK_VEHICLE_TEMP_ACTION(null,vehs[mvf_Convoy2].id,TEMPACT_HANDBRAKETURNLEFT,1500)	
												TASK_LEAVE_ANY_VEHICLE(null,250, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
//												TASK_COMBAT_PED(null,player_ped_id())
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(enemies[epf_c2_D].id,seq)
										CLEAR_SEQUENCE_TASK(seq)
									endif	
								endif
							endif
//							iBreakDriveTask = get_Game_timer()
							eAmbushstage = AMBUSH_START_COMBAT
						break	
						case APPROACH_LEFT
							Println("APPROACH_LEFT")
							// Leader
							if IsEntityAlive(enemies[epf_c1_D].id)
							and IsEntityAlive(vehs[mvf_Convoy1].id)
								if IS_PED_IN_VEHICLE(enemies[epf_c1_D].id,vehs[mvf_Convoy1].id)
									OPEN_SEQUENCE_TASK(seq)										
											TASK_VEHICLE_TEMP_ACTION(null,vehs[mvf_Convoy1].id,TEMPACT_HANDBRAKETURNLEFT,1500)
											TASK_LEAVE_ANY_VEHICLE(null,800, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
//											TASK_COMBAT_PED(null,player_ped_id())
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(enemies[epf_c1_D].id,seq)
									CLEAR_SEQUENCE_TASK(seq)
								endif
							endif
							// target
							if IsEntityAlive(enemies[epf_trgt_driver].id)
							and IsEntityAlive(vehs[mvf_target].id)
								if IS_PED_IN_VEHICLE(enemies[epf_trgt_driver].id,vehs[mvf_target].id)
									OPEN_SEQUENCE_TASK(seq)
										TASK_VEHICLE_TEMP_ACTION(null,vehs[mvf_target].id,TEMPACT_HANDBRAKETURNLEFT,1500)
										TASK_LEAVE_ANY_VEHICLE(null,50, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
//										TASK_COMBAT_PED(null,player_ped_id())
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(enemies[epf_trgt_driver].id,seq)
									CLEAR_SEQUENCE_TASK(seq)
								endif
							endif
							// Tail
							if IsEntityAlive(enemies[epf_c2_D].id)
							and IsEntityAlive(vehs[mvf_Convoy2].id)	
							and IsEntityAlive(vehs[mvf_target].id)
								if IS_PED_IN_VEHICLE(enemies[epf_c2_D].id,vehs[mvf_Convoy2].id)
									OPEN_SEQUENCE_TASK(seq)
										IF VDIST2(GET_ENTITY_COORDS(vehs[mvf_Convoy2].id, FALSE), GET_ENTITY_COORDS(vehs[mvf_target].id, FALSE)) < 1000
											TASK_VEHICLE_DRIVE_TO_COORD(null,vehs[mvf_Convoy2].id,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehs[mvf_target].id,<<-2,1,0>>),10,DRIVINGSTYLE_ACCURATE,CRUSADER,DRIVINGMODE_AVOIDCARS,4,-1)
										ENDIF
										TASK_LEAVE_ANY_VEHICLE(null,500, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
//										TASK_COMBAT_PED(null,player_ped_id())
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(enemies[epf_c2_D].id,seq)
									CLEAR_SEQUENCE_TASK(seq)
								endif
							endif	
//							iBreakDriveTask = get_Game_timer()
							eAmbushstage = AMBUSH_START_COMBAT
						break
					endswitch
				break
				case AMBUSH_START_COMBAT	
					for enemy = 0 to enum_to_int(EPF_NUM_OF_ENEMIES)-1
						if IsEntityAlive(enemies[enemy].id)
						and IsEntityAlive(player_ped_id())							
							//set general combat stuff that wont change														
							SET_PED_ACCURACY(enemies[enemy].id,10)
							SET_PED_SEEING_RANGE(enemies[enemy].id,580)
							SET_PED_COMBAT_ATTRIBUTES(enemies[enemy].id, CA_USE_COVER,true)
							SET_PED_COMBAT_ATTRIBUTES(enemies[enemy].id, CA_CAN_CHARGE,true)
							SET_PED_CONFIG_FLAG(enemies[enemy].id,PCF_PreventAutoShuffleToDriversSeat,false)
							SET_PED_COMBAT_MOVEMENT(enemies[enemy].id,CM_DEFENSIVE)
							SET_DENFENSIVE_AREA_BY_APPROACH(INT_TO_ENUM(EPF_ENEMY_PED_FLAGS ,enemy))
							//Combat task for non drivers
							if  enemy != enum_to_int(epf_c1_D)
							and	enemy != enum_to_int(epf_trgt_driver)
							and enemy != enum_to_int(epf_c2_D)
								int iPause	
								if  enemy = enum_to_int(epf_c2_e2) 
								 	ipause = 300
								elif enemy = enum_to_int(epf_c1_e2)
									ipause = 900
								elif enemy = enum_to_int(epf_c2_e3)
									ipause = 250
								elif enemy = enum_to_int(epf_c1_e3)	
									ipause = 350
								elif enemy = enum_to_int(epf_c2_e4)
									ipause = 500
								elif enemy = enum_to_int(epf_c1_e4)
									ipause = 650	
								endif
								OPEN_SEQUENCE_TASK(seq)
									if IS_PED_IN_ANY_VEHICLE(enemies[enemy].id)
										TASK_PAUSE(null,iPause)
									endif
									TASK_LEAVE_ANY_VEHICLE(null,ipause, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
//									TASK_COMBAT_PED(null,player_ped_id())
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(enemies[enemy].id,seq)
								CLEAR_SEQUENCE_TASK(seq)
								SET_PED_COMBAT_ATTRIBUTES(enemies[enemy].id,CA_LEAVE_VEHICLES,true)
							endif
						endif
					endfor					
					eAmbushstage = GET_AMBUSH_STAGE()	
				break
				case AMBUSH_NEAR_COMBAT	
					for enemy = 0 to enum_to_int(EPF_NUM_OF_ENEMIES)-1
						if IsEntityAlive(enemies[enemy].id)
						and IsEntityAlive(player_ped_id())
							SET_DENFENSIVE_AREA_BY_APPROACH(INT_TO_ENUM(EPF_ENEMY_PED_FLAGS ,enemy))							
						 	if IS_PED_IN_COVER(player_ped_id())
								INT_PED_ADVANCE_DELAY = 8000
							else
								INT_PED_ADVANCE_DELAY = 12000
							endif
							if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(enemies[enemy].id, FALSE),GET_ENTITY_COORDS(player_ped_id())) < 75	
								if not enemies[enemy].bCharge
								and GET_GAME_TIMER() - iChargeTimer > INT_PED_ADVANCE_DELAY
									SET_PED_COMBAT_ATTRIBUTES(enemies[enemy].id, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,true)	
									enemies[enemy].bCharge = true
									iChargeTimer = get_Game_timer()
								endif
							else
								SET_PED_COMBAT_MOVEMENT(enemies[enemy].id, CM_DEFENSIVE)
								SET_PED_COMBAT_ATTRIBUTES(enemies[enemy].id, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,false)	
								enemies[enemy].bCharge = false
							endif
							
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(enemies[enemy].id, SCRIPT_TASK_COMBAT)
								TASK_COMBAT_PED(enemies[enemy].id, PLAYER_PED_ID())
							ENDIF
							
//							IF NOT enemies[enemy].bCombat
//								IF GET_SCRIPT_TASK_STATUS(enemies[enemy].id,SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK	
//									IF get_Game_Timer() - iBreakDriveTask > 8000
//										CLEAR_PED_TASKS(enemies[enemy].id)
//									ENDIF
//								ELSE
//									TASK_COMBAT_PED(enemies[enemy].id,player_ped_id())
//									SET_PED_COMBAT_ATTRIBUTES(enemies[enemy].id,CA_LEAVE_VEHICLES,true)
//									enemies[enemy].bCombat = TRUE
//								ENDIF
//							ENDIF
						ENDIF
					ENDFOR
					eAmbushstage = GET_AMBUSH_STAGE()	
				BREAK
				CASE AMBUSH_FAR_COMBAT
					FOR enemy = 0 to enum_to_int(EPF_NUM_OF_ENEMIES)-1
						IF IsEntityAlive(enemies[enemy].id)
						AND IsEntityAlive(player_ped_id())							
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(enemies[enemy].id, FALSE),GET_ENTITY_COORDS(player_ped_id())) > RANGE_FOR_FAR_COMBAT
								SET_PED_COMBAT_MOVEMENT(enemies[enemy].id, CM_DEFENSIVE)
								SET_PED_COMBAT_ATTRIBUTES(enemies[enemy].id, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED,false)	
								enemies[enemy].bCharge = false
								SET_DENFENSIVE_AREA_BY_APPROACH(INT_TO_ENUM(EPF_ENEMY_PED_FLAGS ,enemy))
							ENDIF
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(enemies[enemy].id, SCRIPT_TASK_COMBAT)
								TASK_COMBAT_PED(enemies[enemy].id, PLAYER_PED_ID())
							ENDIF
//							IF NOT enemies[enemy].bCombat
//								IF GET_SCRIPT_TASK_STATUS(enemies[enemy].id,SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK	
//									IF GET_GAME_TIMER() - iBreakDriveTask > 8000
//										CLEAR_PED_TASKS(enemies[enemy].id)
//									ENDIF
//								ELSE
//									TASK_COMBAT_PED(enemies[enemy].id,player_ped_id())
//									SET_PED_COMBAT_ATTRIBUTES(enemies[enemy].id,CA_LEAVE_VEHICLES,true)
//									enemies[enemy].bCombat = TRUE
//								ENDIF
//							ENDIF
						ENDIF
					ENDFOR
					eAmbushstage = GET_AMBUSH_STAGE()	
				break
				case AMBUSH_CHASE
					BOOL bNearTheMethLab 
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vMethLab) < 30.0
						bNearTheMethLab = TRUE
					ENDIF
				
					for enemy = 0 to enum_to_int(EPF_NUM_OF_ENEMIES)-1
						if isentityalive(player_ped_id())
						and IsEntityAlive(enemies[enemy].id)	
						and IsEntityAlive(vehs[mvf_target].id)
							IF bNearTheMethLab
							AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(enemies[enemy].id, vMethLab) < 30.0

								set_ped_combat_attributes(enemies[enemy].id, ca_use_vehicle,false)
								SET_PED_COMBAT_ATTRIBUTES(enemies[enemy].id,CA_LEAVE_VEHICLES,true)
								REMOVE_PED_DEFENSIVE_AREA(enemies[enemy].id)
								SET_PED_COMBAT_MOVEMENT(enemies[enemy].id,CM_WILLADVANCE)
								
								if not IS_PED_IN_COMBAT(enemies[enemy].id)
									TASK_COMBAT_PED(enemies[enemy].id,player_ped_id())
								endif
						
							ELif is_ped_in_vehicle(player_ped_id(),vehs[mvf_target].id)
							or GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehs[mvf_target].id, FALSE),GET_ENTITY_COORDS(enemies[enemy].id, FALSE)) > (RANGE_FOR_FAR_COMBAT/2)							
								set_ped_combat_attributes(enemies[enemy].id, ca_use_vehicle,true)							
								if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id()),GET_ENTITY_COORDS(enemies[enemy].id, FALSE)) > (RANGE_FOR_FAR_COMBAT*2)
									if not IS_PED_IN_ANY_VEHICLE(enemies[enemy].id)
									and not IS_ENTITY_ON_SCREEN(enemies[enemy].id)
										
										VEHICLE_SEAT  Seat
										VEHICLE_INDEX veh
										veh = return_closest_Mission_veh(enemies[enemy].id)		
										if IsEntityAlive(veh)
											seat = VS_DRIVER
											if IS_VEHICLE_SEAT_FREE(veh,VS_DRIVER)
												Seat = VS_DRIVER
											elif IS_VEHICLE_SEAT_FREE(veh,VS_FRONT_RIGHT)
												Seat = VS_FRONT_RIGHT
											elif IS_VEHICLE_SEAT_FREE(veh,VS_BACK_LEFT)
												Seat = VS_BACK_LEFT
											elif IS_VEHICLE_SEAT_FREE(veh,VS_BACK_RIGHT)
												Seat = VS_BACK_RIGHT
											endif
											if DOES_VEHICLE_HAVE_FREE_SEAT(veh)									
												if GET_SCRIPT_TASK_STATUS(enemies[enemy].id, SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
													TASK_ENTER_VEHICLE(enemies[enemy].id,veh,1000,seat,PEDMOVEBLENDRATIO_RUN,ECF_WARP_PED)
												endif	
												SET_PED_COMBAT_ATTRIBUTES(enemies[enemy].id,CA_LEAVE_VEHICLES,false)	
											else
												SET_PED_AS_NO_LONGER_NEEDED(enemies[enemy].id)
											endif
										else
											SET_PED_AS_NO_LONGER_NEEDED(enemies[enemy].id)
										endif
									else										
										if not IS_PED_IN_COMBAT(enemies[enemy].id)
											TASK_COMBAT_PED(enemies[enemy].id,player_ped_id())
										endif
									endif
								else
									if IS_PED_SITTING_IN_ANY_VEHICLE(enemies[enemy].id)										
										if GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),enemies[enemy].id) < 35																			
											if GET_PED_VEHICLE_SEAT(enemies[enemy].id,GET_VEHICLE_PED_IS_IN(enemies[enemy].id)) = VS_DRIVER	
											 	if get_game_timer() - iEscortTimer > 40000
													if bEscort
														bEscort = false
														iEscortTimer = get_Game_timer()
													else														
														bEscort = true
														iEscortTimer = get_Game_timer()
													endif	
												endif												
												if bEscort
													Println("ESCORT SERVICE")
													if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(enemies[enemy].id,SCRIPT_TASK_VEHICLE_MISSION)
														TASK_VEHICLE_ESCORT(enemies[enemy].id,GET_VEHICLE_PED_IS_IN(enemies[enemy].id),vehs[mvf_target].id,VEHICLE_ESCORT_LEFT,F_CONVOY_MAX_CRUISE_SPEED,DRIVINGMODE_AVOIDCARS_RECKLESS, -1, 20, 0)
													endif
												else
													Println("cbmt SERVICE")
													if not IS_PED_IN_COMBAT(enemies[enemy].id)
														TASK_COMBAT_PED(enemies[enemy].id,player_ped_id())
													endif
												endif												
											else
												if not IS_PED_IN_COMBAT(enemies[enemy].id)
													TASK_COMBAT_PED(enemies[enemy].id,player_ped_id())
												endif
											endif
											
											//B* - 1950436
											IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 2 and GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),enemies[enemy].id) < 20
												SET_PED_COMBAT_ATTRIBUTES(enemies[enemy].id,CA_LEAVE_VEHICLES,TRUE)
											ELSE
												SET_PED_COMBAT_ATTRIBUTES(enemies[enemy].id,CA_LEAVE_VEHICLES,FALSE)
											ENDIF											
										else
											if not IS_PED_IN_COMBAT(enemies[enemy].id)
												TASK_COMBAT_PED(enemies[enemy].id,player_ped_id())
											endif
										endif
									else										
										if not IS_PED_IN_COMBAT(enemies[enemy].id)
											TASK_COMBAT_PED(enemies[enemy].id,player_ped_id())
										endif
									endif	
								endif

							elif not is_ped_in_vehicle(player_ped_id(),vehs[mvf_target].id)
							
								set_ped_combat_attributes(enemies[enemy].id, ca_use_vehicle,false)
								SET_PED_COMBAT_ATTRIBUTES(enemies[enemy].id,CA_LEAVE_VEHICLES,true)
								REMOVE_PED_DEFENSIVE_AREA(enemies[enemy].id)
								SET_PED_COMBAT_MOVEMENT(enemies[enemy].id,CM_WILLADVANCE)
							endif	
						endif
					endfor
					eAmbushstage = GET_AMBUSH_STAGE()
				break

			endswitch			
		break	
	endswitch	
endproc
FUNC BOOL DOES_BUDDY_NEED_TO_DRIVE_TRUCK(PED_INDEX hPed)

    IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()  
        IF IsEntityAlive(vehs[mvf_target].id)
        and not IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_target].id,<<1364.24829, 3620.51123, 33.89069>>,<<1350.89954, 3616.22485, 37.12358>>,6.5)                              
	       
			IF IsEntityAlive(PLAYER_PED_ID())
	       	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehs[mvf_target].id)
	            IF IS_PED_IN_VEHICLE(hPed, vehs[mvf_target].id)
	            AND GET_PED_IN_VEHICLE_SEAT(vehs[mvf_target].id, VS_DRIVER) = hPed
	                RETURN TRUE                        
	            ENDIF
	        ENDIF
			
        ENDIF
  	ENDIF
      
      RETURN FALSE
ENDFUNC
FUNC BOOL DOES_BUDDY_NEED_TO_FOLLOW_CONVOY(PED_INDEX hPed)

    IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()  
        IF IsEntityAlive(vehs[mvf_target].id)
        and armystage = ONROUTE                              
	       
			IF IsEntityAlive(PLAYER_PED_ID())
	       	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	            IF IS_PED_IN_VEHICLE(hPed, GET_VEHICLE_PED_IS_IN(player_ped_id()))
	            AND GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(player_ped_id()), VS_DRIVER) = hPed
	                RETURN TRUE                        
	            ENDIF
	        ENDIF
			
        ENDIF
  	ENDIF
      
      RETURN FALSE
ENDFUNC

BOOL bParking

PROC MONITOR_BATTLE_BUDDIES()
    // For each playable character...
    enumCharacterList eChar
    REPEAT MAX_BATTLE_BUDDIES eChar
            
        PED_INDEX hBuddy = GET_BATTLEBUDDY_PED(eChar)
        
        IF NOT IS_PED_INJURED(hBuddy)
            IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddy)              
    			// Does script need to take control of buddy and drive to dest?
                IF IS_BATTLEBUDDY_AVAILABLE(hBuddy, FALSE)
                    IF DOES_BUDDY_NEED_TO_DRIVE_TRUCK(hBuddy)
					or DOES_BUDDY_NEED_TO_FOLLOW_CONVOY(hBuddy)
                        IF OVERRIDE_BATTLEBUDDY(hBuddy, FALSE)
                            SET_ENTITY_AS_MISSION_ENTITY(hBuddy, TRUE, TRUE)
                            SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddy, TRUE)
                            CLEAR_PED_TASKS(hBuddy)
                        ENDIF
                    ENDIF
                ENDIF              
            ELSE
				// Does script still need to take control of buddy and drive to dest?
                IF DOES_BUDDY_NEED_TO_DRIVE_TRUCK(hBuddy)
                AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddy)
					REQUEST_WAYPOINT_RECORDING(sParkWaypoint)
					bParking = IS_ENTITY_IN_ANGLED_AREA(hBuddy, <<1365.772095,3625.582275,33.692444>>, <<1374.661743,3598.950684,36.894703>>, 34.000000)
					IF NOT bParking
						// Buddy drives truck to destination
		                IF  GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
		                AND GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
						//	                    TASK_VEHICLE_MISSION_COORS_TARGET(hBuddy, GET_VEHICLE_PED_IS_IN(hBuddy), vMethLab,
						//														  MISSION_GOTO, 30.0, DRIVINGMODE_AVOIDCARS, 2.0, -1)
							TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(hBuddy, GET_VEHICLE_PED_IS_IN(hBuddy),<<1376.77, 3605.09, 33.88>>,30,DRIVINGMODE_AVOIDCARS,2.0)
		                ENDIF
					ELSE
						IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_target].id)
							REQUEST_WAYPOINT_RECORDING(sParkWaypoint)
							IF GET_IS_WAYPOINT_RECORDING_LOADED(sParkWaypoint)
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(hBuddy, vehs[mvf_target].id, sParkWaypoint, DRIVINGMODE_AVOIDCARS, 0, EWAYPOINT_START_FROM_CLOSEST_POINT, -1, 10, FALSE, 2)
							ENDIF
						ENDIF
					ENDIF
				elif DOES_BUDDY_NEED_TO_FOLLOW_CONVOY(hBuddy)
				AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddy) 
					// Buddy drives truck behind convoy
					if IsEntityAlive(enemies[epf_c2_D].id)
		                IF  GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
		                AND GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
		                    TASK_VEHICLE_MISSION_PED_TARGET(hBuddy, GET_VEHICLE_PED_IS_IN(hBuddy), enemies[epf_c2_D].id,
															  MISSION_FOLLOW, 15.0, DRIVINGMODE_AVOIDCARS, 15.0, -1)
		                ENDIF 
					endif
                ELSE
                    RELEASE_BATTLEBUDDY(hBuddy)
                ENDIF              
            ENDIF
        ENDIF      
   ENDREPEAT
ENDPROC

PROC RESET_EVERYTHING()

//==================================== Destroy ====================================== 
//all peds are deleted
	for i = 0 TO Enum_to_int(MPF_NUM_OF_PEDS)-1
		if DOES_ENTITY_EXIST(peds[i].id)
		AND (NOT IS_PED_INJURED(peds[i].id))
			if IS_PED_IN_ANY_VEHICLE(peds[i].id)				
				SET_PED_COORDS_NO_GANG(peds[i].id,(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(peds[i].id))+<<0,-2,0>>))				
			endif		
			SAFE_DELETE_PED(peds[i].id)
		endif
	endfor	
	for i = 0 to enum_to_int(EPF_NUM_OF_ENEMIES) -1
		if DOES_ENTITY_EXIST(enemies[i].id)
			CLEANUP_AI_PED_BLIP(enemies[i].blipstruct)
			enemies[i].bCharge	= false
			if not IS_PED_INJURED(enemies[i].id)
				if IS_PED_IN_ANY_VEHICLE(enemies[i].id)				
					SET_PED_COORDS_NO_GANG(enemies[i].id,(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(enemies[i].id))+<<0,-2,0>>))				
				endif
				SAFE_DELETE_PED(enemies[i].id)
			ENDIF
		ENDIF
	ENDFOR
	
	for i = 0 to enum_to_int(NUM_OF_OBJECTS) -1
		if DOES_ENTITY_EXIST(objs[i])
			SAFE_DELETE_OBJECT(objs[i])
		endif
	endfor
	//all vehicles are deleted
	for i = 0 TO Enum_to_int(MVF_NUM_OF_VEH)-1
		if DOES_ENTITY_EXIST(vehs[i].id)	
			EMPTY_VEHICLE(vehs[i].id)
			SAFE_DELETE_VEHICLE(vehs[i].id)
		endif
	endfor	
	
	CLEAR_PRINTS()
	CLEAR_HELP()
	DESTROY_ALL_CAMS()
	
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
	
//====================================  Reset  ======================================
	
	//reset player control
	if IsEntityAlive(player_ped_id())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CLEAR_PLAYER_WANTED_LEVEL(player_id())
	endif
	//clear AI tasks
	if not IS_PED_INJURED(player_ped_id())	
		CLEAR_PED_TASKS(player_ped_id())
	endif	
		
	DISPLAY_RADAR(true)
	DISPLAY_HUD(true)	
	armystage 		= ONROUTE	
	eApproach 		= APPROACH_CHECK
	eAmbushstage	= AMBUSH_REACT
	blockstage		= DRIVING
	iblockstage 	= 0
	bAmbush 		= false
	bSafe			= false	
	
	sWaypoint 	= "Rural_prep_trigger1"
	
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Cleanup()

	PRINTSTRING("...Rural Bank Prep1 - Mission Cleanup")
	PRINTNL()
	
//======================================= Destroy ======================================		
	
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
	
	for i = 0 TO Enum_to_int(MPF_NUM_OF_PEDS)-1
		if IsEntityAlive(peds[i].id)
			if peds[i].id != player_ped_id()
				SET_PED_KEEP_TASK(peds[i].id,true)
				SAFE_RELEASE_PED(peds[i].id)
			endif
			if DOES_BLIP_EXIST(peds[i].blip)
				REMOVE_BLIP(peds[i].blip)
			endif
		endif
	endfor		
//-------------------- enemies-----------------------
	for i = 0 to ENUM_TO_INT(EPF_NUM_OF_ENEMIES) -1	
		if IsEntityAlive(enemies[i].id)	
			SAFE_RELEASE_PED(enemies[i].id)
			if DOES_BLIP_EXIST(enemies[i].blip)	
				REMOVE_BLIP(enemies[i].blip)
			endif
			CLEANUP_AI_PED_BLIP(enemies[i].blipstruct)
		endif
	endfor
//------------------- objects ---------------------
	for i = 0 to enum_to_int(NUM_OF_OBJECTS) -1
		if DOES_ENTITY_EXIST(objs[i])
			SAFE_RELEASE_OBJECT(objs[i])
		endif
	endfor
//---------------------vehs-----------------------
	for i = 0 TO Enum_to_int(MVF_NUM_OF_VEH)-1
		if IsEntityAlive(vehs[i].id)		 
			SAFE_RELEASE_VEHICLE(vehs[i].id)
		endif
		if DOES_BLIP_EXIST(vehs[i].blip)
			REMOVE_BLIP(vehs[i].blip)
		endif
	endfor	
	DISABLE_TAXI_HAILING(FALSE) //PD requested by bug #312601
	CLEAR_PRINTS()
//====================================  Reset  ======================================
		//reset player control
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
	
	RESET_ALL_BATTLEBUDDY_BEHAVIOUR_REQUESTS()
	
	if IsEntityAlive(player_ped_id())
		CLEAR_PED_TASKS(player_ped_id())
	endif
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbiGarage)
	
	SET_WANTED_LEVEL_MULTIPLIER(1)	//wanted level for mission reset to normal
	SET_MAX_WANTED_LEVEL(5)
	DISPLAY_RADAR(true)
	DISPLAY_HUD(true)	
	
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, true)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, true)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, true)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING,true)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER ,true)	
	RESET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN()
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1383.5099, 3596.5181, 33.8865>> - <<5,5, 1>>, <<1383.5099, 3596.5181, 33.8865>> + <<5,5,2>>, TRUE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(CRUSADER,false)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BARRACKS,false)
	armystage = ONROUTE
	bAmbush = false
	SET_POLICE_RADAR_BLIPS(true)	
	
	KILL_CHASE_HINT_CAM(localChaseHintCam)
	
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(widget_parent)
			DELETE_WIDGET_GROUP(widget_parent)
		ENDIF
		IF DOES_WIDGET_GROUP_EXIST(widget_ambush)
			DELETE_WIDGET_GROUP(widget_ambush)
		ENDIF
	#ENDIF
	
	SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(FALSE)
	
	TERMINATE_THIS_THREAD()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Passed()

	IF IS_VEHICLE_DRIVEABLE(vehs[mvf_target].id)
		DECOR_SET_BOOL(vehs[mvf_target].id, "IgnoredByQuickSave", TRUE)
	ENDIF
	
	PRINTSTRING("...Rural Prep 1 Mission Passed")
	PRINTNL()
	Mission_Flow_Mission_Passed()
	Mission_Cleanup()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------


PROC Mission_Failed(MFF_MISSION_FAIL_FLAGS fail_condition = mff_default)

	TRIGGER_MUSIC_EVENT("RHP1_FAIL")
	
	STRING strReason = ""
	
	//show fail message
	SWITCH fail_condition
		CASE mff_debug_fail
			strReason = ""
		BREAK	
		case mff_goods_destroyed
			strReason = "RHP_FDES"
		break
		case mff_goods_stuck
			strReason = "RHP_TRKSTCK"
		break
		case mff_got_away
			strReason = "RHP_FEND"
		break
		case mff_left_truck
			strReason = "RHP_FABN"
		break
		DEFAULT
			strReason = ""
		BREAk
	ENDSWITCH
	
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(strReason)
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		WAIT(0)
	ENDWHILE
		
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	
	for i = 0 to enum_to_int(NUM_OF_OBJECTS) -1
		if DOES_ENTITY_EXIST(objs[i])
			DELETE_OBJECT(objs[i])
		endif
	endfor
	
	MISSION_CLEANUP() // must only take 1 frame and terminate the thread
	TERMINATE_THIS_THREAD()
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		MISSION STAGE MANAGEMENT
// -----------------------------------------------------------------------------------------------------------

PROC Mission_stage_management()
	SWITCH stageswitch
		CASE STAGESWITCH_REQUESTED
			PRINTLN("[stageManagement] mission_stage switch requested from mission_stage:", mission_stage, "to mission_stage", requestedStage)
			stageswitch = STAGESWITCH_EXITING
			mission_substage = STAGE_EXIT
		BREAK
		CASE STAGESWITCH_EXITING
			PRINTLN("[StageManagement] Exiting mission_stage: ", mission_stage)
			stageSwitch = STAGESWITCH_ENTERING
			mission_substage = STAGE_ENTRY
			mission_stage = requestedStage
		BREAK
		CASE STAGESWITCH_ENTERING
			PRINTLN("[StageManagement] Entered mission_stage: ", mission_stage)
			requestedStage = -1
			stageSwitch = STAGESWITCH_IDLE
		BREAK
		CASE STAGESWITCH_IDLE
			IF (GET_GAME_TIMER() - iStageTimer) > 2500
			PRINTLN("[StageManagement] mission_stage: ", mission_stage, " mission_substage: ", mission_substage)
				iStageTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
FUNC BOOL Mission_Set_Stage(MSF_MISSION_STAGE_FLAGS newStage)
	If stageswitch = STAGESWITCH_IDLE
		requestedstage = ENUM_TO_INT(newStage)
		stageswitch = STAGESWITCH_REQUESTED
		return True
	else
		return false
	endif
ENDFUNC
// -----------------------------------------------------------------------------------------------------------
//		HOT SWAP
// -----------------------------------------------------------------------------------------------------------
FUNC BOOL Set_Current_Player_Ped(SELECTOR_SLOTS_ENUM pedChar, BOOL bWait = FALSE, BOOL bCleanUpModel = TRUE)

	IF bWait
		WHILE NOT SET_CURRENT_SELECTOR_PED(pedChar, bCleanUpModel)	
			WAIT(0)
		ENDWHILE
	ELSE
		IF NOT SET_CURRENT_SELECTOR_PED(pedChar, bCleanUpModel)
			RETURN FALSE
		ENDIF
	ENDIF
			
	// Additional stuff to set up ped for use in mission
	SWITCH pedChar
		CASE SELECTOR_PED_MICHAEL
			peds[mpf_mike].id = PLAYER_PED_ID()
		BREAK
		CASE SELECTOR_PED_FRANKLIN
			peds[mpf_frank].id = PLAYER_PED_ID()
		BREAK
		CASE SELECTOR_PED_TREVOR
			peds[mpf_trev].id = PLAYER_PED_ID()			
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
// -----------------------------------------------------------------------------------------------------------
//		
// -----------------------------------------------------------------------------------------------------------

proc load_asset_stage(MSF_MISSION_STAGE_FLAGS loadStage)	
	SWITCH loadStage			
		CASE msf_0_reached_ambush
			load_asset_model(sAssetData,S_M_M_MARINE_01)
			load_asset_model(sAssetData,s_M_Y_marine_03)
			Load_Asset_Model(sAssetData,CRUSADER)
			Load_Asset_Model(sAssetData,BARRACKS)
			Load_Asset_Waypoint(sAssetData,sWaypoint)	
			load_asset_model(sAssetData,PROP_MIL_CRATE_01)
		break		
		CASE msf_1_ambushing
			load_asset_model(sAssetData,s_M_y_marine_03)
			load_asset_model(sAssetData,s_M_M_marine_01)
			Load_Asset_Model(sAssetData,CRUSADER)
			Load_Asset_Model(sAssetData,BARRACKS)
			load_asset_model(sAssetData,PROP_MIL_CRATE_01)
		break		
		CASE msf_2_safety	
		CASE msf_3_passed
			Load_Asset_Model(sAssetData,BARRACKS)
			load_asset_model(sAssetData,PROP_MIL_CRATE_01)
		break			
	ENDSWITCH
endproc
PROC GET_SKIP_STAGE_COORD_AND_HEADING(MSF_MISSION_STAGE_FLAGS eStage, VECTOR &vCoord, FLOAT &fHeading)

	SWITCH eStage
		CASE msf_0_reached_ambush	vCoord = <<-260.5082, 2925.4458, 40.1977>>			fHeading = 303.6227		BREAK
		CASE msf_1_ambushing		vCoord = <<-276.0887, 2920.3860, 40.1317>>			fHeading = 0	 		BREAK
		CASE msf_2_safety			vCoord = <<-253.7471, 2941.4854, 29.1310>> 			fHeading = 330.8292		BREAK
		CASE msf_3_passed			vCoord = <<1367.32434, 3618.37427, 33.89140>>		fHeading =-118.28	 	BREAK	
	ENDSWITCH

ENDPROC
PROC SU_GENERAL()
	if IsEntityAlive(player_ped_id())
		ADD_PED_FOR_DIALOGUE(convo_struct, 0, Player_ped_id(), "TREVOR")
		SET_ENTITY_LOAD_COLLISION_FLAG(Player_ped_id(),true)
		RESTORE_PLAYER_PED_VARIATIONS(Player_ped_id())
		SET_PED_CONFIG_FLAG(Player_ped_id(),PCF_WillFlyThroughWindscreen,false)
//		SET_PED_RELATIONSHIP_GROUP_HASH(Player_ped_id(),rel_group_trev)
	endif
	DOOR_SYSTEM_SET_AUTOMATIC_DISTANCE(g_sAutoDoorData[AUTODOOR_MIL_BASE_BARRIER_IN].doorID, 30.0)
endproc

PROC SETUP_CONVOY_LEADER(VECTOR vPos, FLOAT fHead)
	vehs[mvf_Convoy1].id = CREATE_VEHICLE(CRUSADER, vPos, fHead)
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehs[mvf_Convoy1].id, "PS_PREP_VEHICLES_GROUP")
ENDPROC

PROC SETUP_CONVOY_TARGET(VECTOR vPos, FLOAT fHead)
	vehs[mvf_target].id = CREATE_VEHICLE(BARRACKS,vPos, fHead)	
	SET_ENTITY_HEALTH(vehs[mvf_target].id,(GET_ENTITY_HEALTH(vehs[mvf_target].id)*2))	
	SET_VEHICLE_STRONG(vehs[mvf_target].id,TRUE)
	SET_VEHICLE_ENGINE_HEALTH(vehs[mvf_target].id,(GET_VEHICLE_ENGINE_HEALTH(vehs[mvf_target].id)*2))
	SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehs[mvf_target].id,FALSE)
	SET_VEHICLE_PETROL_TANK_HEALTH(vehs[mvf_target].id,(GET_VEHICLE_PETROL_TANK_HEALTH(vehs[mvf_target].id)*2))
	SET_VEHICLE_CAN_LEAK_PETROL(vehs[mvf_target].id,FALSE)
	SET_VEHICLE_CAN_LEAK_OIL(vehs[mvf_target].id,FALSE)
	SET_VEHICLE_PROVIDES_COVER(vehs[mvf_target].id,TRUE)
	SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehs[mvf_target].id, FALSE)
	SET_VEHICLE_AS_RESTRICTED(vehs[mvf_target].id,0)
	//crates
	objs[objCrate1]  = CREATE_OBJECT(PROP_MIL_CRATE_01,<<1006.7649, 2128.3196, 48.0929>>)
	ATTACH_ENTITY_TO_ENTITY(objs[objCrate1],vehs[mvf_target].id,0,<<0,-0.5,1.5>>,<<0,0,-90>>)	
	objs[objCrate2]  = CREATE_OBJECT(PROP_MIL_CRATE_01,<<1006.7649, 2128.3196, 48.0929>>)
	ATTACH_ENTITY_TO_ENTITY(objs[objCrate2],vehs[mvf_target].id,0,<<0,-2.85,1.5>>,<<0,0,-90>>)
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehs[mvf_target].id, "PS_PREP_VEHICLES_GROUP")
ENDPROC

PROC SETUP_CONVOY_TAIL(VECTOR vPos, FLOAT fHead)
	vehs[mvf_Convoy2].id = create_vehicle(CRUSADER,vPos, fHead )
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehs[mvf_Convoy2].id, "PS_PREP_VEHICLES_GROUP")
ENDPROC

PROC SU_MSF_0_REACHED_AMBUSH()
	//convoy leader	
	if not IsEntityAlive(vehs[mvf_Convoy1].id)
		SETUP_CONVOY_LEADER(<<996.9448, 2140.0200, 47.9112>>, 39.7091)
	endif
	
	int enemy
	for enemy  = enum_to_int(epf_c1_D) to enum_to_int(epf_c1_e4)
		if not IsEntityAlive(enemies[enemy].id)
			create_enemy_in_vehicle(int_to_enum(EPF_ENEMY_PED_FLAGS, enemy),vehs[mvf_Convoy1].id)
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemies[enemy].id)
			if enemy  = enum_to_int(epf_c1_D)
				TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(enemies[epf_c1_D].id,vehs[mvf_Convoy1].id,sWaypoint,
					DRIVINGMODE_STOPFORCARS ,0,EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN,-1,F_CONVOY_MAX_CRUISE_SPEED)
			endif
		endif
	endfor
	
	if not IsEntityAlive(vehs[mvf_target].id)	
		SETUP_CONVOY_TARGET(<<1006.7649, 2128.3196, 48.0929>>, 39.8980)
	endif
	
	if not IsEntityAlive(enemies[epf_trgt_driver].id)
		create_enemy_in_vehicle(epf_trgt_driver,vehs[mvf_target].id)
		TASK_VEHICLE_ESCORT(enemies[epf_trgt_driver].id,vehs[mvf_target].id,vehs[mvf_Convoy1].id,VEHICLE_ESCORT_REAR,F_CONVOY_MAX_CRUISE_SPEED,DRIVINGMODE_STOPFORCARS, -1, 20, 0)
		INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemies[epf_trgt_driver].id)
	endif
	
	//convoy tail
	if not IsEntityAlive(vehs[mvf_Convoy2].id)
		SETUP_CONVOY_TAIL(<<1018.9946, 2112.9512, 48.4754>>, 34.6812)
	endif
	for enemy = enum_to_int(epf_c2_D) to enum_to_int(epf_c2_e4)
		if not IsEntityAlive(enemies[enemy].id)
			create_enemy_in_vehicle(int_to_enum(EPF_ENEMY_PED_FLAGS, enemy),vehs[mvf_Convoy2].id)
			INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemies[enemy].id)
			if enemy = enum_to_int(epf_c2_D)				
				TASK_VEHICLE_ESCORT(enemies[epf_c2_D].id,vehs[mvf_Convoy2].id,vehs[mvf_target].id,VEHICLE_ESCORT_REAR,F_CONVOY_MAX_CRUISE_SPEED,DRIVINGMODE_STOPFORCARS, -1, 20, 0)
			endif
		endif
	endfor
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)				
	endif
	
endproc
proc SU_MSF_1_AMBUSHING()
	
	//convoy leader
	SETUP_CONVOY_LEADER(<<-284.0175, 2949.1035, 28.3715>>, 46.1483)
	int enemy 
	for enemy  = enum_to_int(epf_c1_D) to enum_to_int(epf_c1_e4)
		create_enemy_in_vehicle(int_to_enum(EPF_ENEMY_PED_FLAGS, enemy),vehs[mvf_Convoy1].id)
		INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemies[enemy].id)
	endfor
	// target car
	SETUP_CONVOY_TARGET(<<-248.6621, 2950.0369, 28.9996>>, 93.5394)
	//convoy tail
	SETUP_CONVOY_TAIL(<<-223.1415, 2952.1880, 28.8053>>, 121.2339)
	
	for enemy = enum_to_int(epf_c2_D) to enum_to_int(epf_c2_e4)
		create_enemy_in_vehicle(int_to_enum(EPF_ENEMY_PED_FLAGS, enemy),vehs[mvf_Convoy2].id)
		INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(enemies[enemy].id)
	endfor
	bAmbush		= true
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)				
	endif	
	
endproc
proc SU_MSF_2_SAFETY()
	
	SETUP_CONVOY_TARGET(<<-248.6621, 2950.0369, 28.9996>>, 93.5394)
	bAmbush		= true
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)				
	endif
	
endproc
PROC su_passed()

	SETUP_CONVOY_TARGET(<<1357.7001, 3618.2803, 33.8905>>, 110.4135)
	bAmbush		= true
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
	ENDIF
	
endproc
proc mission_stage_skip()
//a skip has been made
	if bdoskip = true
		
		//begin the skip if the switching is idle
		if stageswitch = stageswitch_idle
			if not is_screen_faded_out()
				if not is_screen_fading_out()
					do_screen_fade_out(DEFAULT_FADE_TIME)
				endif
			else
				mission_set_stage(int_to_enum(msf_mission_stage_flags, iskiptostage))
			endif
		//needs to be carried out before states own entering stage
		elif stageswitch = stageswitch_entering
			
			render_script_cams(false,false)
			set_player_control(player_id(), true)
			
			reset_everything()
						
			Start_Skip_Streaming(sAssetData)
			
			// -------------------------- Not a Replay ----------------------------
			IF NOT IS_REPLAY_BEING_SET_UP()					
				VECTOR vWarpCoord
				FLOAT fWarpHeading
				GET_SKIP_STAGE_COORD_AND_HEADING(int_to_enum(msf_mission_stage_flags, iSkipToStage), vWarpCoord, fWarpHeading)				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpCoord)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fWarpHeading)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				Load_Asset_NewLoadScene_Sphere(sAssetData, vWarpCoord, 50.0)
			ENDIF
			// --------------------------------------------------------------------
						
			Load_asset_Stage(int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage))			
			WHILE NOT Update_Skip_Streaming(sAssetData)
				WAIT(0)				
			ENDWHILE					
			
			switch int_to_enum(msf_mission_stage_flags, mission_stage)			
				case msf_0_reached_ambush	su_msf_0_reached_ambush()	break				
				case msf_1_ambushing		su_msf_1_ambushing()		break				
				case msf_2_safety			su_msf_2_safety()			break
				case msf_3_passed			su_passed()					break
			endswitch
			
			bdoskip = false
			IF NOT IS_REPLAY_BEING_SET_UP()	
				NEW_LOAD_SCENE_STOP()
			endif
		endif
#if is_debug_build
	//check is a skip being asked for, dont allow skip during setup stage
	elif launch_mission_stage_menu(zmenunames, iskiptostage, mission_stage, true)
		if iskiptostage > enum_to_int(msf_num_of_stages)-1
			mission_passed()
		else
			iskiptostage = clamp_int(iskiptostage, 0, enum_to_int(msf_num_of_stages)-1)
			if is_screen_faded_in()
				do_screen_fade_out(default_fade_time_long)
				bdoskip = true
			endif
		endif
#endif
	endif
endproc

FUNC BOOL IS_ANY_CAR_WAITING_FOR_GATE()
	MVF_MISSION_VEHICLE_FLAGS eCar
	REPEAT MVF_NUM_OF_VEH eCar
		IF DOES_ENTITY_EXIST(Vehs[eCar].id)
		AND IS_VEHICLE_DRIVEABLE(Vehs[eCar].id)
		AND IS_ENTITY_IN_ANGLED_AREA(Vehs[eCar].id, <<-1591.019287,2803.730713,15.418117>>, <<-1569.920166,2777.878174,22.148561>>, 8.250000)
			RETURN TRUE
		ENDIF
	ENDREPEAT
	RETURN FALSE
ENDFUNC

PROC MANAGE_GATE()

	SWITCH eGateState
	
		CASE GATE_STATE_GATE_CLOSING
		
			IF fTargetGateState != 0.0
				fTargetGateState = 0.0
			ENDIF
			
			IF fCurrentGateState <= 0.0
				fCurrentGateState = 0.0
				eGateState = GATE_STATE_GATE_CLOSED
			ELIF fCurrentGateState > 0.0
				fCurrentGateState -= 0.02
			ENDIF
			
			FALLTHRU
			
		CASE GATE_STATE_GATE_CLOSED
		
			IF IS_ANY_CAR_WAITING_FOR_GATE()
				eGateState = GATE_STATE_GATE_OPENING
				fStartGateState = fCurrentGateState
				fTargetGateState = 1.0
			ENDIF
			
		BREAK
		
		CASE GATE_STATE_GATE_OPENING
		
			IF fTargetGateState != 1.0
				fTargetGateState = 1.0
			ENDIF
			
			IF fCurrentGateState >= 1.0
				eGateState = GATE_STATE_GATE_OPEN
				fCurrentGateState = 1.0
			ELIF fCurrentGateState < 1.0
				fCurrentGateState += 0.02
			ENDIF
			
			FALLTHRU
			
		CASE GATE_STATE_GATE_OPEN
		
			IF NOT IS_ANY_CAR_WAITING_FOR_GATE()
				eGateState = GATE_STATE_GATE_CLOSING
				fStartGateState = fCurrentGateState
				fTargetGateState = 0.0
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	IF eGateState = GATE_STATE_GATE_OPENING
	OR eGateState = GATE_STATE_GATE_CLOSING
		COSINE_INTERP_FLOAT(fStartGateState, fTargetGateState, fCurrentGateState)
		DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_MIL_BASE_BARRIER_IN].doorID, fCurrentGateState, FALSE, TRUE)
	    DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_MIL_BASE_BARRIER_IN].doorID, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
	ENDIF
	
ENDPROC

BOOL bVehGrabbedForStats = FALSE
BOOL bplayergrabbedForStats = FALSE
proc mission_checks()
//------------------death checks-------------------------------

	for i = 0 to enum_to_int(mpf_num_of_peds) -1
		if does_entity_exist(peds[i].id)
			if is_ped_injured(peds[i].id)
				SAFE_RELEASE_PED(peds[i].id)
			endif
		endif
	endfor
	for i = 0 to enum_to_int(mvf_num_of_veh) -1
		if does_entity_exist(vehs[i].id)
			if not is_vehicle_driveable(vehs[i].id)
				if i = ENUM_TO_INT(mvf_target)
					mission_failed(mff_goods_destroyed)
				endif
				SAFE_RELEASE_VEHICLE(vehs[i].id)
			else
				if vehs[i].id = vehs[mvf_target].id
					if IS_VEHICLE_PERMANENTLY_STUCK(vehs[mvf_target].id)
						mission_failed(mff_goods_stuck)
						SAFE_RELEASE_VEHICLE(vehs[mvf_target].id)
					endif
				endif
			endif
		endif
	endfor
	for i = 0 to enum_to_int(epf_num_of_enemies) -1
		if does_entity_exist(enemies[i].id)		
			if bAmbush				
				update_ai_ped_blip(enemies[i].id,enemies[i].blipstruct,-1,null,false,false,fLOSE_DISTANCE)
			endif
			if is_ped_injured(enemies[i].id)
//				if does_blip_exist(enemies[i].blip)
//					remove_blip(enemies[i].blip)
//				endif
				if enemies[i].bkilled = false
					inform_mission_stats_of_increment(rh1p_kills)
					enemies[i].bkilled = true
				endif
				set_ped_as_no_longer_needed(enemies[i].id)
			endif
		endif
		if IsEntityAlive(enemies[i].id)
		and get_game_timer() - iForceChatTimer	> 8000
		and bAmbush
		and not IS_AMBIENT_SPEECH_PLAYING(enemies[i].id)
		and not IS_PED_IN_ANY_VEHICLE(enemies[i].id)
			if GET_ENTITY_HEALTH(enemies[i].id) < 110
				PLAY_PED_AMBIENT_SPEECH_NATIVE(enemies[i].id, "DYING_MOAN", AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE))
				iForceChatTimer = get_game_timer() 
				println("moan")
			elif GET_ENTITY_HEALTH(enemies[i].id) < 140
				PLAY_PED_AMBIENT_SPEECH_NATIVE(enemies[i].id, "DYING_HELP", AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE))
				iForceChatTimer = get_game_timer() 
				println("help")
			ELIF IS_PED_GOING_INTO_COVER(enemies[i].id)
				PLAY_PED_AMBIENT_SPEECH_NATIVE(enemies[i].id, "TAKE_COVER", AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE))
				iForceChatTimer = get_game_timer() 
				println("cover")
			ELIF IS_PED_RUNNING(enemies[i].id)
				PLAY_PED_AMBIENT_SPEECH_NATIVE(enemies[i].id, "COVER_ME", AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE))
				iForceChatTimer = get_game_timer() 
				println("me")
			ELIF IS_PED_SHOOTING(enemies[i].id)
				PLAY_PED_AMBIENT_SPEECH_NATIVE(enemies[i].id, "COVER_YOU", AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE))
				iForceChatTimer = get_game_timer() 
				println("you")
			elif not IS_PED_WEAPON_READY_TO_SHOOT(enemies[i].id)
				PLAY_PED_AMBIENT_SPEECH_NATIVE(enemies[i].id, "RELOADING", AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE))
				iForceChatTimer = get_game_timer() 
				println("reload")
			elif IS_SCRIPT_TASK_RUNNING_OR_STARTING(enemies[i].id,SCRIPT_TASK_LEAVE_ANY_VEHICLE)
			or IS_SCRIPT_TASK_RUNNING_OR_STARTING(enemies[i].id,SCRIPT_TASK_LEAVE_VEHICLE)
				PLAY_PED_AMBIENT_SPEECH_NATIVE(enemies[i].id, "GENERIC_WAR_CRY", AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE))
				println("warcry")
				iForceChatTimer = get_game_timer() 
			endif
		endif
	endfor
	
	// police blips
	if GET_PLAYER_WANTED_LEVEL(player_id()) > 0
		SET_POLICE_RADAR_BLIPS(true)
	else
		SET_POLICE_RADAR_BLIPS(false)
	endif
		
	//Player at army base
	if isentityalive(vehs[mvf_target].id)
	and IsEntityAlive(player_ped_id())		
		if is_entity_in_angled_area(vehs[mvf_target].id,<<-1521.82935, 2725.00342, 16.64367>>,<<-1600.76294, 2794.87549, 21.45629>>,30)
		and is_ped_in_vehicle(player_ped_id(),vehs[mvf_target].id)
			SET_MAX_WANTED_LEVEL(5)
			SET_PLAYER_WANTED_LEVEL(player_id(),4)
			SET_PLAYER_WANTED_LEVEL_NOW(player_id())			
		endif
	endif
	
	//distace check
	if IsEntityAlive(vehs[mvf_target].id)
	and IsEntityAlive(PLAYER_PED_ID())
		if mission_stage > ENUM_TO_INT(msf_0_reached_ambush)
			if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehs[mvf_target].id),GET_ENTITY_COORDS(player_ped_id())) > 650
				Mission_Failed(mff_left_truck)
			elif GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehs[mvf_target].id),GET_ENTITY_COORDS(player_ped_id())) > 600
				if not bleftWarn	
					PRINT_NOW("RHP_LWARN",DEFAULT_GOD_TEXT_TIME,1)
					bleftWarn = true
				endif
			else
				bleftWarn = false
			endif
		endif
	endif
	
	//All dead cehck
	if IsEntityAlive(vehs[mvf_target].id)		
		bool bAlldead
		bAlldead = true
		int ienemy
		for ienemy =  0 to enum_to_int(EPF_NUM_OF_ENEMIES) -1
			if IsEntityAlive(enemies[ienemy].id)			
				bAlldead = false
			endif
		endfor
		if bAlldead
			if not IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehs[mvf_target].id)
			AND NOT IS_PED_IN_COMBAT(PLAYER_PED_ID())
			AND NOT IS_PED_SHOOTING(PLAYER_PED_ID())
				if not IS_PED_HEADTRACKING_ENTITY(player_ped_id(),vehs[mvf_target].id)
					TASK_LOOK_AT_ENTITY(player_ped_id(),vehs[mvf_target].id,-1,SLF_WHILE_NOT_IN_FOV,SLF_LOOKAT_LOW)
				endif
			ELSE
				IF IS_PED_HEADTRACKING_ENTITY(PLAYER_PED_ID(), vehs[mvf_target].id)
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				ENDIF
			ENDIF
		endif
	endif
	
	//stat check for stickybomb
	IF NOT bStickyBombStatPassed
	
		IF mission_stage = enum_to_int(msf_0_reached_ambush)
			IF fTimeSinceSpotted != 0.0
				fTimeSinceSpotted = 0.0
			ENDIF				
		ELIF mission_stage = enum_to_int(msf_1_ambushing)
			IF fTimeSinceSpotted < 5.0
				PRINTLN("fTimeSinceSpotted ", fTimeSinceSpotted)
				fTimeSinceSpotted+=GET_FRAME_TIME()
			ENDIF
		ENDIF
		
		IF fTimeSinceSpotted < 3.0
			INT isb
			REPEAT COUNT_OF(vehs) isb
				
				IF DOES_ENTITY_EXIST(vehs[isb].id)
				AND NOT DOES_ENTITY_EXIST(vehStats[isb])
					vehStats[isb] = vehs[isb].id
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehStats[isb])
					IF IS_ENTITY_DEAD(vehStats[isb])
					OR NOT IS_VEHICLE_DRIVEABLE(vehStats[isb])
						PRINTLN("veh ", isb, " is dead")
						WEAPON_TYPE wep
						wep = GET_VEHICLE_CAUSE_OF_DESTRUCTION(vehStats[isb])
						PRINTLN("weapon what did it is ", GET_WEAPON_NAME(wep))
						IF wep = WEAPONTYPE_STICKYBOMB
							PRINTLN("STICKBOMB STAT DEBUG - Entity is dead, stat set")
							INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(RH1P_CONVOY_STOPPED)
							bStickyBombStatPassed = TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDREPEAT
			
			//added this in below as entity from trigger scene won't hand over if it is destroyed as the script starts
			REPEAT COUNT_OF(g_sTriggerSceneAssets.veh) isb
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[isb])
					IF IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[isb])
					OR NOT IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[isb])
						PRINTLN("g_sTriggerSceneAssets.veh ", isb, " is dead")
						WEAPON_TYPE wep
						wep = GET_VEHICLE_CAUSE_OF_DESTRUCTION(g_sTriggerSceneAssets.veh[isb])
						PRINTLN("weapon what did it is ", GET_WEAPON_NAME(wep))
						IF wep = WEAPONTYPE_STICKYBOMB
							PRINTLN("STICKBOMB STAT DEBUG - Entity is dead, stat set")
							INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(RH1P_CONVOY_STOPPED)
							bStickyBombStatPassed = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
		
	ENDIF
		
//		REPEAT COUNT_OF(vehs) isb
//		
//			IF iStickyBombTime[isb] = -1
//			
//				IF g_sTriggerSceneAssets.id = isb
//				AND g_sTriggerSceneAssets.timer != -1
//					PRINTLN("STICKBOMB STAT DEBUG - stickybomb timer grabbed from trigger scene")
//					iStickyBombTime[isb] = g_sTriggerSceneAssets.timer
//				ENDIF
//				
//				IF DOES_ENTITY_EXIST(vehs[isb].id)
//				AND IS_EXPLOSION_IN_SPHERE(EXP_TAG_STICKYBOMB, GET_ENTITY_COORDS(vehs[isb].id), 5)
//					IF mission_stage = enum_to_int(msf_0_reached_ambush)
//						PRINTLN("STICKBOMB STAT DEBUG - Stickybpomb timer started")
//						iStickyBombTime[isb] = GET_GAME_TIMER()
//					ELSE
//						PRINTLN("not in msf_0_reached_ambush")
//					ENDIF
//				ENDIF
//				
//			ELSE
//			
//				IF GET_GAME_TIMER() - iStickyBombTime[isb] < 2000
//					PRINTLN("STICKBOMB STAT DEBUG - timer is below 2 seconds")
//					IF IS_ENTITY_DEAD(vehs[isb].id) 
//					OR NOT IS_VEHICLE_DRIVEABLE(vehs[isb].id, TRUE)
//						PRINTLN("STICKBOMB STAT DEBUG - Entity is dead, stat set")
//						INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(RH1P_CONVOY_STOPPED)
//						bStickyBombStatPassed = TRUE
//					ENDIF
//				ELSE
//					PRINTLN("STICKBOMB STAT DEBUG - Sticky bomb timer ", isb, " is set back to -1 ")
//					iStickyBombTime[isb] = -1
//				ENDIF
//				
//			ENDIF
//			
//		ENDREPEAT
//		
//	ENDIF
	
	ARMY_COMBAT()
	MONITOR_BATTLE_BUDDIES()
	MANAGE_GATE()
	RUN_AUDIO_SCENES()
	
	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(player_id())
	
	IF NOT bOnBase
		IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_MILITARY_BASE)
			bOnBase = TRUE
			RESET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN()
			SET_WANTED_LEVEL_MULTIPLIER(1.0)
			SET_MAX_WANTED_LEVEL(5)
			set_police_radar_blips(true)
			SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 3)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			bOnBase = TRUE
		ENDIF
	ELSE
		IF NOT IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_MILITARY_BASE)
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				enable_dispatch_service(dt_ambulance_department,false)
				enable_dispatch_service(dt_fire_department, 	false)
				enable_dispatch_service(dt_police_automobile, 	false)
				ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING,false)
				ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER ,false)
				SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(DT_POLICE_AUTOMOBILE,0)
				SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(DT_POLICE_HELICOPTER,0)
				SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(DT_POLICE_ROAD_BLOCK,0)
				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				SET_MAX_WANTED_LEVEL(0)
				set_police_radar_blips(false)
				bOnBase = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	//stats
	if does_entity_exist(player_ped_id())
	and not is_ped_injured(player_ped_id())
		if is_ped_in_any_vehicle(player_ped_id())
			IF NOT bVehGrabbedForStats
				PRINTLN("\n\n\n grab for stats \n\n\n")
				inform_mission_stats_of_damage_watch_entity(get_vehicle_ped_is_in(player_ped_id()), RH1P_CAR_DAMAGE)
				inform_mission_stats_of_speed_watch_entity(get_vehicle_ped_is_in(player_ped_id()), RH1P_MAX_SPEED)
				bVehGrabbedForStats = TRUE
			ENDIF
		else
			IF bVehGrabbedForStats
				PRINTLN("\n\n\n release for stats \n\n\n")
				//inform_mission_stats_of_damage_watch_entity(null, RH1P_CAR_DAMAGE) 
				//inform_mission_stats_of_speed_watch_entity(null, RH1P_MAX_SPEED)
				bVehGrabbedForStats = FALSE
			ENDIF
		endif
		
		IF NOT bplayergrabbedForStats
			inform_mission_stats_of_damage_watch_entity(PLAYER_PED_ID(), RH1P_DAMAGE)
			bplayergrabbedForStats = TRUE
		ENDIF
		
	endif
	
	IF NOT bCarImpounded
		VEHICLE_INDEX vehTemp = NULL
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
			AND IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
			AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(GET_PLAYERS_LAST_VEHICLE())) < 100*100
				vehTemp = GET_PLAYERS_LAST_VEHICLE()
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(vehTemp)
		AND IS_VEHICLE_DRIVEABLE(vehTemp)
		AND vehTemp != vehPlayerCar
		AND vehTemp != vehs[mvf_Convoy1].id
		AND vehTemp != vehs[mvf_Convoy2].id
		AND vehTemp != vehs[mvf_target].id
		AND IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(vehTemp))
			SET_ENTITY_AS_MISSION_ENTITY(vehTemp, TRUE, TRUE)
			vehPlayerCar = vehTemp
		ENDIF
	ENDIF
	
endproc
proc mission_setup()
	remove_relationship_group(rel_group_trev)	
	remove_relationship_group(rel_group_enemies)	
	
	add_relationship_group("trevor", rel_group_trev)
	add_relationship_group("enemies", rel_group_enemies)
	
//haters		
	set_relationship_between_groups(acquaintance_type_ped_hate,rel_group_enemies,relgrouphash_player)
	set_relationship_between_groups(acquaintance_type_ped_hate,rel_group_enemies,rel_group_trev)
	set_relationship_between_groups(acquaintance_type_ped_hate,rel_group_trev,rel_group_enemies)
//friends
	set_relationship_between_groups(acquaintance_type_ped_like,rel_group_enemies,RELGROUPHASH_COP)
	set_relationship_between_groups(acquaintance_type_ped_like,RELGROUPHASH_COP,rel_group_enemies)
	set_relationship_between_groups(acquaintance_type_ped_like,rel_group_enemies,rel_group_enemies)
	set_relationship_between_groups(acquaintance_type_ped_like,rel_group_enemies,RELGROUPHASH_AMBIENT_ARMY)
	set_relationship_between_groups(acquaintance_type_ped_like,RELGROUPHASH_AMBIENT_ARMY,rel_group_enemies)
	
	request_additional_text("ruralp", mission_text_slot)
	
	if is_player_playing(player_id())
		set_player_control(player_id(), true)	
	endif			
	
	enable_dispatch_service(dt_ambulance_department,false)
	enable_dispatch_service(dt_fire_department, 	false)
	enable_dispatch_service(dt_police_automobile, 	false)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING,false)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER ,false)
	SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(DT_POLICE_AUTOMOBILE,0)
	SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(DT_POLICE_HELICOPTER,0)
	SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(DT_POLICE_ROAD_BLOCK,0)
		
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	SET_MAX_WANTED_LEVEL(0)
	
	set_police_radar_blips(false)
		
	set_vehicle_model_is_suppressed(crusader,true)
	set_vehicle_model_is_suppressed(barracks,true)
	
	sbiGarage = ADD_SCENARIO_BLOCKING_AREA(<<1375.234009,3600.537842,35.394558>> - <<14.250000,7.000000,0.000000>>, <<1375.234009,3600.537842,35.394558>> + <<14.250000,7.000000,3.000000>>)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<1383.5099, 3596.5181, 33.8865>> - <<5,5, 1>>, <<1383.5099, 3596.5181, 33.8865>> + <<5,5,2>>, FALSE)
		
	if is_replay_in_progress()		
	or IS_REPEAT_PLAY_ACTIVE()
		if IS_REPLAY_IN_PROGRESS()
			iskiptostage = get_replay_mid_mission_stage()
			if g_bshitskipaccepted 
				iskiptostage++			
			endif
			if iskiptostage >= 3
				iSkipToStage = 3
			endif			
		elif IS_REPEAT_PLAY_ACTIVE()
			iSkipToStage = 0
		endif
		// New replay streaming 
		IF IS_REPLAY_IN_PROGRESS()
			VECTOR vSkipToCoord
			FLOAT fSkipToHeading
			GET_SKIP_STAGE_COORD_AND_HEADING(int_to_enum(MSF_MISSION_STAGE_FLAGS,iSkipToStage), vSkipToCoord, fSkipToHeading)
			START_REPLAY_SETUP(vSkipToCoord, fSkipToHeading)
		ENDIF
		bdoskip = true		
	else
		while not grab_mission_entites()
			PRINTLN("grabbing entities")
			wait(0)
		endwhile
		set_replay_mid_mission_stage_with_name(0,"stage 0: reached ambush")
		mission_stage = enum_to_int(msf_0_reached_ambush)					
		load_asset_stage(msf_0_reached_ambush)
	endif		
	mission_substage = stage_entry	
	
	INIT_BATTLEBUDDY_BEHAVIOUR_FOR_MISSION(SP_HEIST_RURAL_PREP_1)		
	
endproc
// -----------------------------------------------------------------------------------------------------------
//		Mission stages
// -----------------------------------------------------------------------------------------------------------
proc ST_0_REACHED_AMBUSH()
	switch mission_substage
		case STAGE_ENTRY	
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			WHILE g_replay.replayStageID = RS_NOT_RUNNING
				WAIT(0)
			ENDWHILE
			if IsEntityAlive(vehs[mvf_target].id)
			and not DOES_BLIP_EXIST(vehs[mvf_target].blip)
				IF GET_BLIP_FROM_ENTITY(vehs[mvf_target].id) != NULL
					vehs[mvf_target].blip = GET_BLIP_FROM_ENTITY(vehs[mvf_target].id)
				ELSE
					vehs[mvf_target].blip = CREATE_BLIP_FOR_VEHICLE(vehs[mvf_target].id)  
				ENDIF
			endif
			
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
			
			Print_now("RHP_GOODS",DEFAULT_GOD_TEXT_TIME,1)
			TRIGGER_MUSIC_EVENT("RHP1_START")
			print_help("RHP_HELP")
			armystage = ONROUTE			
			mission_substage++
		break
		case 1			
			if bAmbush	
				b_dialogue_played = false
				Mission_Set_Stage(msf_1_ambushing)
				mission_substage = STAGE_ENTRY
			endif
		break
	endswitch
endproc
proc ST_1_AMBUSHING()
	switch mission_substage
		case STAGE_ENTRY
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			int enemy
			for enemy = enum_to_int(epf_c1_D) to enum_to_int(epf_c2_e4)
				if IsEntityAlive(enemies[enemy].id)		
					if not b_dialogue_played
						ADD_PED_FOR_DIALOGUE(convo_struct,1,enemies[enemy].id,"ArmyPed")
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						PLAY_PED_AMBIENT_SPEECH_NATIVE(enemies[enemy].id, "GENERIC_WAR_CRY", AUDIO_SPEECH_GET_PARAM_STRING_FROM_ENUM(SPEECH_PARAMS_FORCE))
//						if CREATE_CONVERSATION(convo_struct,"RHPAUD","RHP_ATTACK",CONV_PRIORITY_MEDIUM)
						b_dialogue_played = true
//						endif
					endif
				endif
			endfor
			set_relationship_between_groups(acquaintance_type_ped_hate,relgrouphash_player,rel_group_enemies)
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
			mission_substage++
		break
		case 1
			if IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehs[mvf_target].id)
				REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<0,0,0>>,-1)
				if DOES_BLIP_EXIST(vehs[mvf_target].blip)
					REMOVE_BLIP(vehs[mvf_target].blip)
				endif				
				Mission_Set_Stage(msf_2_safety)
				mission_substage = STAGE_ENTRY
			else
				if IsEntityAlive(vehs[mvf_target].id)
				and not DOES_BLIP_EXIST(vehs[mvf_target].blip)
					vehs[mvf_target].blip = CREATE_BLIP_FOR_VEHICLE(vehs[mvf_target].id)   
				endif
			endif
		break
	endswitch
	
endproc
proc ST_2_SAFETY()

	IF NOT bCarImpounded
		IF DOES_ENTITY_EXIST(vehPlayerCar)
		AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehPlayerCar)
			SEND_VEHICLE_DATA_TO_IMPOUND_USING_VEHICLE(vehPlayerCar)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPlayerCar)
			bCarImpounded = TRUE
		ENDIF
	ENDIF
	
	REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(-94.3112, 3003.0156, 2711.2727, 4857.7627)
	
	switch mission_substage
		case STAGE_ENTRY
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(RH1P_VAN_TO_LAB_TIME)
			TRIGGER_MUSIC_EVENT("RHP1_TRUCK")
			REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
			mission_substage++
		break
		case 1		 
//			if GET_DISTANCE_BETWEEN_COORDS(<<1364.24829, 3620.51123, 33.89069>>,get_entity_coords(player_ped_id())) < 190	
//			and GET_DISTANCE_BETWEEN_COORDS(<<1364.24829, 3620.51123, 33.89069>>,get_entity_coords(player_ped_id())) > 100
//				clear_area(<<1358.19678, 3618.23413, 33.89066>>,10,true)
//				DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<1358.19678, 3618.23413, 33.89066>>,20)
//			endif
			if IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,vMethLab,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,true,vehs[mvf_target].id,"RHP_RTN","","RHP_BK",true)
			or (DOES_BLIP_EXIST(sLocatesData.LocationBlip)And IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_target].id,<<1359.47620, 3614.32593, 36.87141>>,<<1356.74805, 3621.60474, 33.72731>>,5))
				BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehs[mvf_target].id,DEFAULT_VEH_STOPPING_DISTANCE,-1)	
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_target].id,false)
				CLEAR_MISSION_LOCATE_STUFF(sLocatesData,true)
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE, RH1P_VAN_TO_LAB_TIME)
				bdisplay_msgKill = FALSE
				bdisplay_msgexit = false
				mission_substage++
			endif
		break
		case 2		
			bSafe = true
			int enemy
			for enemy =  0 to enum_to_int(EPF_NUM_OF_ENEMIES) -1
				if IsEntityAlive(enemies[enemy].id)
					if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(enemies[enemy].id),get_entity_coords(player_ped_id())) < 400
						if not IS_PED_IN_COMBAT(enemies[enemy].id)
							TASK_COMBAT_PED(enemies[enemy].id,player_ped_id())
						endif
						bSafe = false
					else	
						SET_PED_AS_NO_LONGER_NEEDED(enemies[enemy].id)
					endif
				endif
			endfor
			if bSafe
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				mission_substage++				
			else
				if not bdisplay_msgkill
					CLEAR_PRINTS()
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					PRINT_NOW("RHP_KILL",DEFAULT_GOD_TEXT_TIME,1)			
					bdisplay_msgKill = true
				endif
			endif
		break
		case 3
			if not IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_target].id)
				if IS_MESSAGE_BEING_DISPLAYED()
					CLEAR_PRINTS()
				endif
				if GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_TREVOR
					ADD_PED_FOR_DIALOGUE(convo_struct,0,player_ped_id(),"Trevor")
					if bdisplay_msgKill
						if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"RHPAUD","RHP_END","RHP_END_3",CONV_PRIORITY_MEDIUM)
							mission_substage = 4	
						endif
					else
						if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"RHPAUD","RHP_END","RHP_END_3",CONV_PRIORITY_MEDIUM)
							mission_substage = 4
						endif
					endif
				elif GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_MICHAEL
					ADD_PED_FOR_DIALOGUE(convo_struct,0,player_ped_id(),"Michael")
					if bdisplay_msgKill
						if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"RHPAUD","RHP_END","RHP_END_1",CONV_PRIORITY_MEDIUM)
							mission_substage = 4
						endif
					else
						if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"RHPAUD","RHP_END","RHP_END_1",CONV_PRIORITY_MEDIUM)
							mission_substage = 4
						endif
					endif
				endif
				IF MISSION_SUBSTAGE = 4
					SETTIMERA(0)
				ENDIF
			else
				if not bdisplay_msgexit
				and IS_SAFE_TO_DISPLAY_GODTEXT()
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 7.0, REPLAY_IMPORTANCE_HIGHEST)
					TRIGGER_MUSIC_EVENT("RHP1_END")
					PRINT_NOW("RHP_EXIT",DEFAULT_GOD_TEXT_TIME,1)
					bdisplay_msgexit = true
				endif
			endif
		break
		case 4
			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			OR TIMERA() > 6000
				mission_passed()
			endif
		break
	endswitch
endproc
PROC ST_3_PASSED()
	switch mission_substage
		case STAGE_ENTRY
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			endif
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			mission_substage++
		break
		case 1
			Mission_Passed()
		break
	endswitch
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		MISSION FLOW
// -----------------------------------------------------------------------------------------------------------
PROC mission_flow()
	Switch	int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage)
		case 	msf_0_reached_ambush 		ST_0_REACHED_AMBUSH() 	break
		case 	msf_1_ambushing				ST_1_AMBUSHING() 		break
		case 	msf_2_safety				ST_2_SAFETY()			break	
		case 	msf_3_passed				ST_3_PASSED()			break
	endswitch
ENDPROC
#IF IS_DEBUG_BUILD			
	PROC DO_DEBUG()
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_PASSED()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			MISSION_FAILED(mff_debug_fail)		
		ENDIF
	ENDPROC
	
	PROC BUILD_WIDGETS()
		widget_parent = START_WIDGET_GROUP("Paleto prep")
			widget_ambush = START_WIDGET_GROUP("Military Ambush Menu")
				ADD_WIDGET_BOOL("Ambush",bambush)
			STOP_WIDGET_GROUP()
			widget_debug = START_WIDGET_GROUP("debug information")
				twAmbush = ADD_TEXT_WIDGET("Ambush stage")
				twBlock = ADD_TEXT_WIDGET("Block stage")
				ADD_WIDGET_INT_READ_ONLY("blocking timer", iblockTimer)
				ADD_WIDGET_INT_READ_ONLY("blocking substage", iblockstage)
				twArmy = ADD_TEXT_WIDGET("Army stage")
				twApproach = ADD_TEXT_WIDGET("Approach stage")
				twMissionStage = ADD_TEXT_WIDGET("Mission stage")
				ADD_WIDGET_INT_READ_ONLY("Mission substage", mission_substage)
				ADD_WIDGET_INT_READ_ONLY("Max wanted level", iMaxWantedLevel)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		SET_LOCATES_HEADER_WIDGET_GROUP(widget_debug)
	ENDPROC
	
	PROC RUN_WIDGETS()
		SET_CONTENTS_OF_TEXT_WIDGET(twAmbush, 		GET_STRING_FROM_AMBUSH_STAGE(eAmbushstage))
		SET_CONTENTS_OF_TEXT_WIDGET(twBlock, 		GET_STRING_FROM_BLOCK_STAGE(blockstage))
		SET_CONTENTS_OF_TEXT_WIDGET(twArmy, 		GET_STRING_FROM_ARMY_STAGE(armystage))
		SET_CONTENTS_OF_TEXT_WIDGET(twApproach, 	GET_STRING_FROM_APPROACH_STAGE(eApproach))
		SET_CONTENTS_OF_TEXT_WIDGET(twMissionStage, GET_STRING_FROM_MISSION_STAGE(INT_TO_ENUM(MSF_MISSION_STAGE_FLAGS, mission_stage)))
		iMaxWantedLevel = GET_MAX_WANTED_LEVEL()
	ENDPROC
	
#endif
// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	PRINTSTRING("...rural bank prep 1 Mission Launched")
	PRINTNL()
	
	IF (HAS_FORCE_CLEANUP_OCCURRED())
		PRINTSTRING("...rural bank prep 1 Mission Force Cleanup")
		PRINTNL()
		Mission_Flow_Mission_Force_Cleanup()
		Mission_Cleanup()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	#if IS_DEBUG_BUILD
		//z menu for skipping stages
		zMenuNames[msf_0_reached_ambush].sTxtLabel 	=	"Stage 0: reached ambush"
		zMenuNames[msf_1_ambushing].sTxtLabel 		=	"Stage 1: Ambushing"
		zMenuNames[msf_2_safety].sTxtLabel 			=	"Stage 3: Safety"
		zMenuNames[msf_3_passed].sTxtLabel 			=	"------ PASSED ------"
		BUILD_WIDGETS()
	#endif
	
//	iStickyBombTime[0] = -1
//	iStickyBombTime[1] = -1
//	iStickyBombTime[2] = -1
	
	//initialize mission
	MISSION_SETUP()
	
	SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(TRUE)
	
	WHILE (TRUE)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ThePaletoScorePrep")
		//prestreaming loop
		Update_Asset_Management_System(sAssetData)		// Deals with loading any assets and keeps track of what has been loaded
		
		IF mission_stage = ENUM_TO_INT(msf_0_reached_ambush)
		AND DOES_ENTITY_EXIST(vehs[mvf_target].id)
		AND IS_VEHICLE_DRIVEABLE(vehs[mvf_target].id)
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehs[mvf_target].id)
		AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehs[mvf_target].id)) < 75*75
			BOOL bNeedToReload = FALSE
			IF NOT localChaseHintCam.bHint_toggleOn
			AND IS_PED_ON_FOOT(PLAYER_PED_ID())
			AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
				WEAPON_TYPE playerWep = WEAPONTYPE_INVALID
				GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), playerWep)
				IF playerWep != WEAPONTYPE_UNARMED
					INT iClipAmmo = 0
					GET_AMMO_IN_CLIP(PLAYER_PED_ID(), playerWep, iClipAmmo)
					IF iClipAmmo < GET_MAX_AMMO_IN_CLIP(PLAYER_PED_ID(), playerWep)
						bNeedToReload = TRUE
					ENDIF
				ENDIF
			ENDIF
			IF NOT bNeedToReload
				CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCam, vehs[mvf_target].id)
			ENDIF
		ELSE
			KILL_CHASE_HINT_CAM(localChaseHintCam)
		ENDIF
		
		Mission_stage_management()
		MISSION_STAGE_SKIP()		
		if not bDoSkip
			MISSION_CHECKS()
			mission_flow()
		endif
		
		#IF IS_DEBUG_BUILD
			DO_DEBUG()
			RUN_WIDGETS()
		#ENDIF
		
		WAIT(0)
	
	ENDWHILE
ENDSCRIPT
