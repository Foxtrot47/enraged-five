USING "commands_script.sch"
USING "commands_camera.sch"
USING "script_maths.sch"
USING "rural_bank_heist_support.sch"


ENUM TANK_STATE
	TANK_INIT,
	TANK_FIRE_FIRST_CANNED_SHOT,
	TANK_FIRE_SECOND_CANNED_SHOT,
	TANK_FIRE_THIRD_CANNED_SHOT,
	TANK_PREP_FOR_NEXT_SHOT,
	TANK_WAIT_TO_FIRE,
	TANK_FIRE,
	TANK_KILL_THE_PLAYER,
	TANK_KILL_THE_PLAYER_IDLE,
	TANK_IDLE
ENDENUM
TANK_STATE eTankState

ENUM TANK_CUTSCENE_STATE
	TANK_CUTSCENE_INIT,
	TANK_CUTSCENE_FIRST_FRAME,
	TANK_CUTSCENE_PLAYING_TRIGGER_SECOND_SHOT,
	TANK_CUTSCENE_PLAYING,
	TANK_CUTSCENE_DONE
ENDENUM
TANK_CUTSCENE_STATE eCutsceneTankState


structTimer	tmrTankShoot
FLOAT		flTimeToFire
VECTOR		vShotPos

structTimer	tmrTankSceneController
FLOAT	flWidgetTankRecordingStartTime = 5000.0
FLOAT	flWidgetTankRecordingSpeed = 0.75
#IF IS_DEBUG_BUILD
FLOAT	flWidgetTankFiredTime = 3.0
#ENDIF
FLOAT	flWidgetTankSceneFinishTime = 0.500

FLOAT	flWidgetTankRubberBandSlowSpeed = 0.1
FLOAT	flWidgetTankRubberBandModerateSpeed = 0.500
FLOAT	flWidgetTankRubberBandFastSpeed = 1.0
FLOAT	flWidgetTankKillDistance = 15.0
FLOAT	flWidgetTankTurretSpeed = 0.5
FLOAT	flWidgetTankSpeedDelta = 0.01
FLOAT	flWidgetTankSlowDistance = 25.0
FLOAT	flWidgetTankFastDistance = 30.0
FLOAT	flWidgetArcOfSafety		 = 30.0
FLOAT	flMinCannonShotDistance	 = 8.0
FLOAT	flMaxCannonShotDistance	 = 16.0
FLOAT	flTimeMinBetweenShots	= 1.0
FLOAT	flTimeMaxBetweenShots	= 5.0
BOOL	bWidgetBulldozerInvincible = FALSE

// READ-ONLY WIDGETS
FLOAT flMinimumSafeAngle
FLOAT flMaximumSafeAngle
FLOAT flTankShotAngle

FLOAT	flTankCurrentSpeed
VECTOR 	vJunkYardFence = <<-149.66592, 6257.47656, 30.59559>>
BOOL	bTankTaskedToKillPlayer

INT iSceneId

FUNC VECTOR GENERATE_SHOT()
	VECTOR vRetVal, vPlayerPos, vEnemyPos, vDirectionalVector, vShotVectorFromBulldozer, vShotOffset
	FLOAT flSafetyAngle, flScalarOfShotFromBulldozer, flZCoord
	
	// Get Bulldozer's position
	IF IS_ENTITY_OK(carBulldozer)
		vPlayerPos = GET_ENTITY_COORDS(carBulldozer)
	ENDIF
	
	// Get Tank's position
	IF IS_ENTITY_OK(EnemyTank.thisCar)
		vEnemyPos = GET_ENTITY_COORDS(EnemyTank.thisCar)
	ENDIF
	
	// Get the direction vector between them
	vDirectionalVector = vPlayerPos - vEnemyPos
	
	// Get the angle between tank and direction vector
	flTankShotAngle = ATAN2(vDirectionalVector.y, vDirectionalVector.x)
	
	// Determine the safety arc that won't shoot through the bulldozer
//	flTankShotAngle = RAD_TO_DEG(flTankShotAngle)

	// The minimum "safe" angle is the angle from tank to bulldozer plus our ArcOfSafety
	flMinimumSafeAngle = flTankShotAngle + flWidgetArcOfSafety
	
	// The maximum "safe" angle is the angle from tank to bulldozer minus our ArcOfSafety PLUS 2 PI
	flMaximumSafeAngle = flTankShotAngle - flWidgetArcOfSafety + 360

	flSafetyAngle = GET_RANDOM_FLOAT_IN_RANGE(flMinimumSafeAngle, flMaximumSafeAngle)
	
	// Determine the distance from the bulldozer to the shot
	flScalarOfShotFromBulldozer = GET_RANDOM_FLOAT_IN_RANGE(flMinCannonShotDistance, flMaxCannonShotDistance)
	
	vShotVectorFromBulldozer.x = COS(flSafetyAngle)
	vShotVectorFromBulldozer.y = SIN(flSafetyAngle)
	vShotVectorFromBulldozer.z = 0
	
	vShotOffset = flScalarOfShotFromBulldozer * vShotVectorFromBulldozer
	vRetVal = vPlayerPos + vShotOffset
	
	IF GET_GROUND_Z_FOR_3D_COORD(vRetVal, flZCoord)
		CPRINTLN(DEBUG_MISSION, "GET_GROUND_Z_FOR_3D_COORD was able to find a valid ground intersection")
		vRetVal.z = flZCoord
	ELSE
		CPRINTLN(DEBUG_MISSION, "GET_GROUND_Z_FOR_3D_COORD was unable to find a valid ground intersection")
	ENDIF
	
	CPRINTLN(DEBUG_MISSION, "vRetVal = ", vRetVal)
	
	RETURN vRetVal
	
ENDFUNC


PROC FIRE_THE_GUNS(VECTOR vPos)
	IF IS_ENTITY_OK(EnemyTank.thisDriver)
		CLEAR_PED_TASKS(EnemyTank.thisDriver)
		PRINTLN("PALETO SCORE: TANK TASKED TO FIRE SINGLE BULLET AT:", vPos)
		SET_PED_FIRING_PATTERN(EnemyTank.thisDriver, FIRING_PATTERN_SINGLE_SHOT)
		TASK_VEHICLE_SHOOT_AT_COORD(EnemyTank.thisDriver, vPos, 1)
		SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE", 0.025)
	ENDIF
ENDPROC


PROC RUBBER_BAND_TANK_SPEEDS()
	FLOAT flDistance
	
	IF IS_ENTITY_OK(EnemyTank.thisCar) AND IS_ENTITY_OK(carBulldozer)
		SET_VEHICLE_TURRET_SPEED_THIS_FRAME(EnemyTank.thisCar, flWidgetTankTurretSpeed)
		SET_PLAYBACK_SPEED(EnemyTank.thisCar, flTankCurrentSpeed)
		
		flDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(EnemyTank.thisCar), GET_ENTITY_COORDS(carBulldozer))
		// Slow the tank down when it gets close
		IF flDistance <= flWidgetTankSlowDistance
			IF flTankCurrentSpeed >= flWidgetTankRubberBandSlowSpeed
				flTankCurrentSpeed -= flWidgetTankSpeedDelta
			ENDIF
			
		// Bring it to normal speed when at a moderate distance
		ELIF flDistance >= flWidgetTankSlowDistance AND flDistance <= flWidgetTankFastDistance
			// Need to handle accelerating up or decelerating down to moderate speed.
			IF ABSF(flWidgetTankRubberBandModerateSpeed - flTankCurrentSpeed) <= 0.2
				IF flTankCurrentSpeed > flWidgetTankRubberBandModerateSpeed
					flTankCurrentSpeed -= flWidgetTankSpeedDelta
				ELSE
					flTankCurrentSpeed += flWidgetTankSpeedDelta
				ENDIF
			ENDIF
			
		//Speed it up when far away
		ELIF flDistance >= flWidgetTankFastDistance
			IF flTankCurrentSpeed <= flWidgetTankRubberBandFastSpeed
				flTankCurrentSpeed += flWidgetTankSpeedDelta
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_TANK_FIRING()
	
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_SPHERE(vShotPos, 0.5, 255, 0, 0)
	#ENDIF
	
	// Slow down the tank turret
	IF eTankState > TANK_INIT
		IF IS_ENTITY_OK(EnemyTank.thisCar)
			RUBBER_BAND_TANK_SPEEDS()
			
			// If the tank gets really close, kill the player
			IF NOT bTankTaskedToKillPlayer
				IF IS_ENTITY_OK(carBulldozer)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(EnemyTank.thisCar), GET_ENTITY_COORDS(carBulldozer)) <= flWidgetTankKillDistance
						bTankTaskedToKillPlayer = TRUE
						CPRINTLN(DEBUG_MISSION, "UPDATE_TANK_FIRING - Tank got too close, going to TANK_KILL_THE_PLAYER")
						eTankState = TANK_KILL_THE_PLAYER
					ELSE
						CPRINTLN(DEBUG_MISSION, "Distance between bulldozer and tank = ", GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(EnemyTank.thisCar), GET_ENTITY_COORDS(carBulldozer)))
					ENDIF
				ELSE
					CPRINTLN(DEBUG_MISSION, "The bulldozer isn't valid?!?  WTF!??!")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ENTITY_OK(carBulldozer)
		SET_ENTITY_INVINCIBLE(carBulldozer, bWidgetBulldozerInvincible)
	ENDIF
	
	SWITCH eTankState
		CASE TANK_INIT
			RESTART_TIMER_NOW(tmrTankShoot)
			IF IS_ENTITY_OK(EnemyTank.thisDriver)
				SET_PED_ACCURACY(EnemyTank.thisDriver, 0)
			ENDIF
			bTankTaskedToKillPlayer = FALSE
			CPRINTLN(DEBUG_MISSION, "UPDATE_TANK_FIRING - Going to TANK_FIRE_FIRST_CANNED_SHOT")
			eTankState = TANK_FIRE_FIRST_CANNED_SHOT
		BREAK
		
		CASE TANK_FIRE_FIRST_CANNED_SHOT
			IF GET_TIMER_IN_SECONDS(tmrTankShoot) >= 0.25
				//ADD_EXPLOSION(vJunkYardFence, EXP_TAG_TANKSHELL, 5.0, FALSE, FALSE, 5.0)
//				SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE", 0.05)
				IF IS_ENTITY_OK(EnemyTank.thisDriver)
					CLEAR_PED_TASKS(EnemyTank.thisDriver)
				ENDIF
				
				STOP_PLAYBACK_RECORDED_VEHICLE(EnemyTank.thisCar)
				START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(EnemyTank.thisCar, 2, "RBHEnemyTank", ENUM_TO_INT(SWITCH_ON_PLAYER_VEHICLE_IMPACT))
				SET_PLAYBACK_SPEED(EnemyTank.thisCar, flWidgetTankRubberBandModerateSpeed)
				RESTART_TIMER_NOW(tmrTankShoot)
				CPRINTLN(DEBUG_MISSION, "UPDATE_TANK_FIRING - Going to TANK_FIRE_SECOND_CANNED_SHOT")
				eTankState = TANK_FIRE_SECOND_CANNED_SHOT
			ENDIF
		BREAK
		
		CASE TANK_FIRE_SECOND_CANNED_SHOT
			IF GET_TIMER_IN_SECONDS(tmrTankShoot) >= 2.0
				vShotPos = <<-154.6912, 6276.9771, 34.8261>>
				FIRE_THE_GUNS(vShotPos)
				RESTART_TIMER_NOW(tmrTankShoot)
				CPRINTLN(DEBUG_MISSION, "UPDATE_TANK_FIRING - Going to TANK_FIRE_THIRD_CANNED_SHOT")
				eTankState = TANK_FIRE_THIRD_CANNED_SHOT
			ENDIF
		BREAK
		
		CASE TANK_FIRE_THIRD_CANNED_SHOT
			IF GET_TIMER_IN_SECONDS(tmrTankShoot) >= 2.0
				vShotPos = <<-121.3582, 6239.1836, 36.6719>>//<<-162.68047, 6257.50146, 30.48936>>
				FIRE_THE_GUNS(vShotPos)
				RESTART_TIMER_NOW(tmrTankShoot)
				CPRINTLN(DEBUG_MISSION, "UPDATE_TANK_FIRING - Going to TANK_PREP_FOR_NEXT_SHOT")
				eTankState = TANK_PREP_FOR_NEXT_SHOT
			ENDIF
		BREAK
		
		CASE TANK_PREP_FOR_NEXT_SHOT
			RESTART_TIMER_NOW(tmrTankShoot)
			flTimeToFire = GET_RANDOM_FLOAT_IN_RANGE(flTimeMinBetweenShots, flTimeMaxBetweenShots)
			CPRINTLN(DEBUG_MISSION, "UPDATE_TANK_FIRING - Going to TANK_WAIT_TO_FIRE")
			eTankState = TANK_WAIT_TO_FIRE
		BREAK
		
		CASE TANK_WAIT_TO_FIRE
			IF IS_ENTITY_OK(EnemyTank.thisDriver)
				TASK_VEHICLE_AIM_AT_PED(EnemyTank.thisDriver, PLAYER_PED_ID())
			ENDIF
			
			IF TIMER_DO_ONCE_WHEN_READY(tmrTankShoot, flTimeToFire)
				vShotPos = GENERATE_SHOT()//GENERATE_RANDOM_SHOT_FOR_TANK()
				CPRINTLN(DEBUG_MISSION, "UPDATE_TANK_FIRING - Going to TANK_FIRE")
				eTankState = TANK_FIRE
			ENDIF
		BREAK
		
		CASE TANK_FIRE
			
			FIRE_THE_GUNS(vShotPos)
			CPRINTLN(DEBUG_MISSION, "UPDATE_TANK_FIRING - Going to TANK_PREP_FOR_NEXT_SHOT")
			
			eTankState = TANK_PREP_FOR_NEXT_SHOT
		BREAK
		
		CASE TANK_KILL_THE_PLAYER
			IF IS_ENTITY_OK(EnemyTank.thisDriver)
			//AND TIMER_DO_ONCE_WHEN_READY(tmrTankShoot, 5)
				SET_PED_ACCURACY(EnemyTank.thisDriver, 100)
				CLEAR_PED_TASKS(EnemyTank.thisDriver)
								
				TASK_VEHICLE_SHOOT_AT_PED(EnemyTank.thisDriver, PLAYER_PED_ID(), 5)
				CPRINTLN(DEBUG_MISSION, "UPDATE_TANK_FIRING - Going to TANK_KILL_THE_PLAYER_IDLE")
				RESTART_TIMER_NOW(tmrTankShoot)
				eTankState = TANK_KILL_THE_PLAYER_IDLE
			ENDIF
		BREAK
		
		
		CASE TANK_KILL_THE_PLAYER_IDLE
			IF GET_TIMER_IN_SECONDS(tmrTankShoot) >= 3.5
				CPRINTLN(DEBUG_MISSION, "UPDATE_TANK_FIRING - Going to TANK_IDLE")
				eTankState = TANK_KILL_THE_PLAYER
			ENDIF
		BREAK
		
		CASE TANK_IDLE
			// Exactly what it says on the tin
			IF flTankCurrentSpeed > 0
				flTankCurrentSpeed -= flWidgetTankSpeedDelta
			ELSE 
				flTankCurrentSpeed = 0
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

BOOL bDone1stPersonPulse = FALSE
FUNC BOOL IS_TANK_CUTSCENE_COMPLETE()
	
	IF NOT IS_ENTITY_DEAD(EnemyTank.thisCar)
		SET_VEHICLE_TURRET_SPEED_THIS_FRAME(EnemyTank.thisCar, 0.5)
	ENDIF
	
	SWITCH eCutsceneTankState
		CASE TANK_CUTSCENE_INIT
			// Create tank
			
			INIT_SET_PIECE_COP_CAR(EnemyTank, RHINO, 1, "RBHEnemyTank", VS_FRONT_RIGHT, flWidgetTankRecordingStartTime,flWidgetTankRecordingSpeed, FALSE, S_M_Y_MARINE_03)
			SET_VEHICLE_DOORS_LOCKED(EnemyTank.thisCar, VEHICLELOCK_LOCKED)
			FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(EnemyTank.thisCar)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyTank.thisDriver, TRUE)
						
			TASK_VEHICLE_AIM_AT_COORD(EnemyTank.thisDriver, vJunkYardFence)
			CPRINTLN(DEBUG_MISSION, "IS_TANK_CUTSCENE_COMPLETE - Going to TANK_CUTSCENE_FIRST_FRAME")
			RESTART_TIMER_NOW(tmrTankSceneController)
			
				//Tank shot
			SET_CAM_COORD(initialCam,<<-185.712555,6285.777832,32.763523>>)
			SET_CAM_ROT(initialCam,<<0.018052,0.000000,30.394934>>)
			SET_CAM_FOV(initialCam,33.427639)

			SET_CAM_COORD(destinationCam,<<-189.656067,6285.817383,32.764137>>)
			SET_CAM_ROT(destinationCam,<<0.018052,-0.000000,23.002777>>)
			SET_CAM_FOV(destinationCam,33.427639)
			
			SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 2000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			IF NOT IS_ENTITY_DEAD(carBulldozer)
				START_PLAYBACK_RECORDED_VEHICLE(carBulldozer, 1, "RBHDZESC")
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(carBulldozer)
			ENDIF
			SET_CUTSCENE_RUNNING(TRUE)
			
			bDone1stPersonPulse = FALSE
			
			eCutsceneTankState = TANK_CUTSCENE_FIRST_FRAME
		BREAK
		
		CASE TANK_CUTSCENE_FIRST_FRAME
//			SET_CAM_COORD(initialCam, <<-190.4590, 6302.7163, 31.2334>>)
//			SET_CAM_ROT(initialCam, <<9.7098, 0.0000, 64.7441>>)
//			SET_CAM_FOV(initialCam, 45.3345)
//			SET_CAM_ACTIVE(initialCam, TRUE)
			
		
			START_AUDIO_SCENE("PS_2A_TANK_ARRIVES")
			RESTART_TIMER_NOW(tmrTankSceneController)
			CPRINTLN(DEBUG_MISSION, "IS_TANK_CUTSCENE_COMPLETE - Going to TANK_CUTSCENE_PLAYING")
			eCutsceneTankState = TANK_CUTSCENE_PLAYING_TRIGGER_SECOND_SHOT
		BREAK
		
		CASE TANK_CUTSCENE_PLAYING_TRIGGER_SECOND_SHOT
			//Wait for the tank
			IF GET_TIMER_IN_SECONDS(tmrTankSceneController) >= 2
				SET_CAM_COORD(initialCam,<<-209.982239,6317.295410,33.839745>>)
				SET_CAM_ROT(initialCam,<<-5.453514,0.000000,-130.510498>>)
				SET_CAM_FOV(initialCam,33.427639)

				SET_CAM_COORD(destinationCam,<<-207.832321,6318.571289,33.848900>>)
				SET_CAM_ROT(destinationCam,<<-3.725802,-0.000000,-136.960266>>)
				SET_CAM_FOV(destinationCam,33.427639)
				
				SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 2500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
			
				IF NOT IS_ENTITY_DEAD(EnemyTank.thisDriver)
					SET_PED_FIRING_PATTERN(EnemyTank.thisDriver, FIRING_PATTERN_SINGLE_SHOT)
					TASK_VEHICLE_SHOOT_AT_COORD(EnemyTank.thisDriver, vJunkYardFence, 1)
				ENDIF
							
				RESTART_TIMER_NOW(tmrTankSceneController)
				eCutsceneTankState = TANK_CUTSCENE_PLAYING
			ENDIF
		BREAK
		
		CASE TANK_CUTSCENE_PLAYING
			//Wait for the tank
			IF GET_TIMER_IN_SECONDS(tmrTankSceneController) >= 2 //flWidgetTankFiredTime
			
				vShotPos = vJunkYardFence
				CPRINTLN(DEBUG_MISSION, "IS_TANK_CUTSCENE_COMPLETE - Going to TANK_CUTSCENE_DONE")
				RESTART_TIMER_NOW(tmrTankSceneController)
				eCutsceneTankState = TANK_CUTSCENE_DONE
			ENDIF
		BREAK
		
		CASE TANK_CUTSCENE_DONE
			IF NOT bDone1stPersonPulse
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
					IF GET_TIMER_IN_SECONDS(tmrTankSceneController) >= flWidgetTankSceneFinishTime - 0.3
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						bDone1stPersonPulse = TRUE
					ENDIF
				ENDIF
			ENDIF		
		
			IF GET_TIMER_IN_SECONDS(tmrTankSceneController) >= flWidgetTankSceneFinishTime
				CANCEL_TIMER(tmrTankSceneController)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
		DO_SCREEN_FADE_OUT(500)
		WHILE IS_SCREEN_FADING_OUT()
			WAIT(0)
		ENDWHILE
		
		SET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(carBulldozer, 95.0, 1, "RBHDZESC")
		SET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(EnemyTank.thisCar, 20.0, 1, "RBHEnemyTank")
		
		DO_SCREEN_FADE_IN(500)
		RETURN TRUE
	ENDIF
	
	
	RETURN FALSE
ENDFUNC

PROC CREATE_OUTSIDE_FACTORY_VEHICLES()

	RESET_MISSION_STATS_ENTITY_WATCH()

//	outsideFactoryVehicles[0].thisCar = CREATE_VEHICLE(BARRACKS, << -133.8680, 6226.5273, 30.3617 >>, 86.7604)
//	outsideFactoryVehicles[1].thisCar = CREATE_VEHICLE(BARRACKS, << -144.2065, 6225.8672, 30.1892 >>, 62.2704)
//	outsideFactoryVehicles[2].thisCar = CREATE_VEHICLE(BARRACKS, << -152.9405, 6234.9385, 30.2014 >>, 34.745)
//	outsideFactoryVehicles[3].thisCar = CREATE_VEHICLE(BARRACKS, <<-106.1363, 6291.3018, 30.2428>>, 301.1030)
//	
	outsideFactoryVehicles[0].thisDriver = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_03,  <<-70.9615, 6312.3394, 30.3057>>, 144.7231)
	outsideFactoryVehicles[0].thisPassenger = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_03, <<-57.9858, 6297.6509, 30.3243>>, 30.9160)
	
	SET_PED_AS_DEFENSIVE_COP(outsideFactoryVehicles[0].thisDriver, WEAPONTYPE_CARBINERIFLE)
	SET_PED_AS_DEFENSIVE_COP(outsideFactoryVehicles[0].thisPassenger, WEAPONTYPE_CARBINERIFLE)
	
	SET_PED_ANGLED_DEFENSIVE_AREA(outsideFactoryVehicles[0].thisDriver, <<-90.786263,6286.269531,30.067406>>, <<-64.875671,6313.887695,33.070950>>, 19.500000)
	SET_PED_ANGLED_DEFENSIVE_AREA(outsideFactoryVehicles[0].thisPassenger, <<-90.786263,6286.269531,30.067406>>, <<-64.875671,6313.887695,33.070950>>, 19.500000)
			
//	INT i
//	
//	FOR i = 0 TO NUMBER_OF_COP_CARS_OUTSIDE_FACTORY - 1
//		IF DOES_ENTITY_EXIST(outsideFactoryVehicles[i].thisCar)
//		
//			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(outsideFactoryVehicles[i].thisCar, TRUE)
//			
//			#IF IS_DEBUG_BUILD
//				TEXT_LABEL_63 debugCarName = "ChTrucks "
//				debugCarName += i
//				SET_VEHICLE_NAME_DEBUG(outsideFactoryVehicles[i].thisCar, debugCarName)
//			#ENDIF
//		ENDIF
//	ENDFOR
		
ENDPROC


INT iSecondTank

PROC UPDATE_SECOND_TANK(BOOL bSkipToEnd = FALSE)

	SWITCH iSecondTank
	
		CASE 0
			IF NOT IS_PED_INJURED(outsideFactoryVehicles[0].thisDriver)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(outsideFactoryVehicles[0].thisDriver, 200.0)
			ENDIF
			IF NOT IS_PED_INJURED(outsideFactoryVehicles[0].thisPassenger)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(outsideFactoryVehicles[0].thisPassenger, 200.0)
			ENDIF
			iSecondTank++
		BREAK
	
		CASE 1
			REQUEST_MODEL(RHINO)
			REQUEST_VEHICLE_RECORDING(2, "RBHThirdTank")
			
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "RBHThirdTank")
				INIT_SET_PIECE_COP_CAR(thirdTank, RHINO, 2, "RBHThirdTank", VS_FRONT_RIGHT, 0.0, 0.9)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thirdTank.thisDriver, TRUE)
				SET_VEHICLE_DOORS_LOCKED(thirdTank.thisCar, VEHICLELOCK_LOCKED)
				TASK_VEHICLE_AIM_AT_PED(thirdTank.thisDriver, PLAYER_PED_ID())
				IF bSkipToEnd
					SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(thirdTank.thisCar)
				ENDIF
				iSecondTank++
			ENDIf
		BREAK
		
		
		CASE 2
		
			REQUEST_VEHICLE_RECORDING(1, "RBHNewTank")
			
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "RBHNewTank")
			
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-145.362183,6229.626465,29.426483>>, <<-80.418449,6293.631836,37.561554>>, 30.500000)
				OR bSkipToEnd
					INIT_SET_PIECE_COP_CAR(secondTank, RHINO, 1, "RBHNewTank")
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(secondTank.thisDriver, TRUE)
					SET_VEHICLE_DOORS_LOCKED(secondTank.thisCar, VEHICLELOCK_LOCKED)
					TASK_VEHICLE_AIM_AT_PED(secondTank.thisDriver, PLAYER_PED_ID())
					IF bSkipToEnd
						SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(secondTank.thisCar)
					ENDIF							
					
					IF NOT IS_PED_INJURED(EnemyTank.thisDriver)
					AND NOT IS_PED_INJURED(outsideFactoryVehicles[0].thisDriver)
						//SET_PED_SPHERE_DEFENSIVE_AREA(outsideFactoryVehicles[0].thisDriver, GET_ENTITY_COORDS(EnemyTank.thisDriver), 5.0)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(outsideFactoryVehicles[0].thisDriver, 200.0)
						SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(outsideFactoryVehicles[0].thisDriver, secondTank.thisCar, <<0.0, 0.0, 0.0>>, 9.0)
					ENDIF
					IF NOT IS_PED_INJURED(EnemyTank.thisDriver)
					AND NOT IS_PED_INJURED(outsideFactoryVehicles[0].thisPassenger)
						//SET_PED_SPHERE_DEFENSIVE_AREA(outsideFactoryVehicles[0].thisPassenger, GET_ENTITY_COORDS(EnemyTank.thisDriver), 5.0)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(outsideFactoryVehicles[0].thisPassenger, 200.0)
						SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(outsideFactoryVehicles[0].thisPassenger, secondTank.thisCar, <<0.0, 0.0, 0.0>>, 9.0)
					ENDIF
					
					iSecondTank++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT bSkipToEnd
				IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(secondTank.thisCar, "RBHNewTank") > 55.0
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_ARMTRK", CONV_PRIORITY_VERY_HIGH)
							iSecondTank++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
				
	ENDSWITCH

ENDPROC

CAMERA_INDEX cutsceneCamera
BOOL bCutscenePlaying = FALSE
BOOL bInitialBullDozerConvo = FALSE
BOOL bInitialBullDozerConvo2 = FALSE
BOOL bInitialSawTankConvo = FALSE


OBJECT_INDEX objWeapon

PROC stageRescueBuddys()

	doBuddyFailCheck(1.0, FALSE)
	
	
	UPDATE_SECOND_TANK()
	
	CPRINTLN(DEBUG_MISSION, "iStageRescueBuddys = ", iStageRescueBuddys)
	
	SWITCH iStageRescueBuddys

		CASE 0			
		
			KILL_FACE_TO_FACE_CONVERSATION()
		
			RESET_TYRE_POP_ARRAY()
			
			bInitialBullDozerConvo = FALSE
			bInitialBullDozerConvo2 = FALSE
			
			#IF IS_DEBUG_BUILD
			//	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			#ENDIF
			
			IF NOT IS_ENTITY_DEAD(dozerStageTank.thisDriver)
				CLEAR_PED_TASKS(dozerStageTank.thisDriver)
			ENDIF
			
			iSecondTank = 0
			REQUEST_CUTSCENE("RBH_2A_MCS_4")
			REQUEST_VEHICLE_RECORDING(1, "RBHEnemyTank")
			REQUEST_VEHICLE_RECORDING(2, "RBHEnemyTank")
			
			REQUEST_VEHICLE_RECORDING(1, "RBHDZESC")
			
			REQUEST_ANIM_DICT("MISSHeistBank_Bulldozer")
			REQUEST_MODEL(RHINO)
			IF NOT IS_ENTITY_DEAD(carBulldozer)
				SET_VEHICLE_BULLDOZER_ARM_POSITION(carBulldozer, -0.05, FALSE)
			ENDIF
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(pedFranklin()) 			
			g_iPaletoScoreTake = iCurrentTake	//UPdate the saved take
			eCutsceneTankState = TANK_CUTSCENE_INIT
			eTankState = TANK_INIT
			flTankCurrentSpeed = flWidgetTankRubberBandModerateSpeed
			iStageRescueBuddys = 2
		BREAK
	
		CASE 2
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", pedSelector.pedID[SELECTOR_PED_MICHAEL])
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", pedTrevor())
			
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "RBHEnemyTank")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "RBHEnemyTank")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "RBHDZESC")
			AND HAS_MODEL_LOADED(RHINO)
			AND HAS_ANIM_DICT_LOADED("MISSHeistBank_Bulldozer")
				
				iStageRescueBuddys++
			ENDIF
		BREAK
	
		CASE 3
		
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", pedSelector.pedID[SELECTOR_PED_MICHAEL])
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", pedTrevor())
				
			//CLEAR_AREA(<<-826.457886,180.473969,71.133858>>, 100.00, TRUE)
			CREATE_OUTSIDE_FACTORY_VEHICLES()
			
			SETUP_WANTED_LEVEL(5, TRUE, 50.0, 10.0, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
			
			RESET_CREW_ANIMS()
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			DELETE_BULLDOZER_SET_PIECE_CARS(TRUE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(mnCopPedModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnCopCarModel)
			SET_MODEL_AS_NO_LONGER_NEEDED(CRUSADER)
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(11, "STAGE_RESCUE_BUDDYS") 
			g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = iCurrentTake
	
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(PLAYER_PED_ID(), FALSE)
	
			iStageRescueBuddys = 99
		BREAK
		
		CASE 99
		
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", pedSelector.pedID[SELECTOR_PED_MICHAEL])
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", pedTrevor())
		
			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			
				IF NOT IS_ENTITY_DEAD(pedTrevor())
					CLEAR_PED_TASKS_IMMEDIATELY(pedTrevor())
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTrevor(), TRUE)
					SET_PED_CAN_RAGDOLL(pedTrevor(), FALSE)
					SET_ENTITY_PROOFS(pedTrevor(), TRUE, TRUE, TRUE, TRUE, TRUE)
					REGISTER_ENTITY_FOR_CUTSCENE(pedTrevor(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					CLEAR_PED_TASKS_IMMEDIATELY(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSelector.pedID[SELECTOR_PED_MICHAEL], TRUE)
					SET_PED_CAN_RAGDOLL(pedSelector.pedID[SELECTOR_PED_MICHAEL], FALSE)
					SET_ENTITY_PROOFS(pedSelector.pedID[SELECTOR_PED_MICHAEL], TRUE, TRUE, TRUE, TRUE, TRUE)
					REGISTER_ENTITY_FOR_CUTSCENE(pedSelector.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(carBulldozer)
					REGISTER_ENTITY_FOR_CUTSCENE(carBulldozer, "RBH_BULLDOZER", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)		
				ENDIF
				
				SET_CURRENT_PED_WEAPON(PedMichael(), PedMichaelWeaponType, TRUE)
				objWeapon = CREATE_WEAPON_OBJECT_FROM_CURRENT_PED_WEAPON_WITH_COMPONENTS(pedSelector.pedID[SELECTOR_PED_MICHAEL])   
				REGISTER_ENTITY_FOR_CUTSCENE(objWeapon, "Michaels_weapon", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
								
				START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				TRIGGER_MUSIC_EVENT("RH2A_PICK_UP")
				bCutscenePlaying = FALSE
				iStageRescueBuddys = 4
			ENDIF
		BREAK
	
		CASE 4
			
			HIDE_HUD_AND_RADAR_THIS_FRAME()
		
			IF IS_CUTSCENE_PLAYING()
			AND bCutscenePlaying = FALSE
			
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
				IF NOT IS_ENTITY_DEAD(carBulldozer)
					SET_VEHICLE_BULLDOZER_ARM_POSITION(carBulldozer, -0.05, TRUE)
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(carBulldozer)
				ENDIF
				CLEAR_AREA(<< -181.7382, 6289.4253, 34.5892>>, 6.00, TRUE)
				
				IF GET_CLOCK_HOURS() < 19
				AND GET_CLOCK_HOURS() >= 18
					SET_CLOCK_TIME(19, 00, 0)
				ENDIF
				
				IF IS_SCREEN_FADED_OUT()
				OR IS_SCREEN_FADING_OUT()
				
					DO_SCREEN_FADE_IN(250)
				ENDIF
				
				bDone1stPersonPulse = FALSE
				bCutscenePlaying = TRUE
			ENDIF
			
			DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()        
			
			//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("RBH_BULLDOZER") 
			IF NOT IS_CUTSCENE_PLAYING()
			//IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			AND bCutscenePlaying = TRUE
			//IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				IF IS_ENTITY_OK(carBulldozer)
					SET_ENTITY_HEADING(carBulldozer, 244.6549 )
					SET_ENTITY_COORDS(carBulldozer, <<-191.8555, 6290.6719, 30.4891>>)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(-10.7538)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-15.6910)
					
					IF NOT IS_ENTITY_DEAD(carBulldozer)
						SET_VEHICLE_BULLDOZER_ARM_POSITION(carBulldozer, -0.05, TRUE)
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(carBulldozer)
					ENDIF
					
					FREEZE_ENTITY_POSITION(carBulldozer, FALSE)
					//SET_VEHICLE_ON_GROUND_PROPERLY(carBulldozer)
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carBulldozer)
					SET_VEHICLE_ENGINE_ON(carBulldozer, TRUE, TRUE)
					PRINTLN("oh shit I printed this")
					
					IF IS_ENTITY_OK(pedTrevor())
						ATTACH_ENTITY_TO_ENTITY(pedTrevor(), carBulldozer, GET_ENTITY_BONE_INDEX_BY_NAME(carBulldozer, "seat_dside_r"), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						SET_CURRENT_PED_WEAPON(pedTrevor(), WEAPONTYPE_MINIGUN, TRUE)
						TASK_PLAY_ANIM(pedTrevor(), "MISSHeistBank_Bulldozer", "Shovel_Idle_Trevor", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedTrevor())
					ENDIF	
					IF IS_ENTITY_OK(pedSelector.pedID[SELECTOR_PED_MICHAEL])
						ATTACH_ENTITY_TO_ENTITY(pedSelector.pedID[SELECTOR_PED_MICHAEL], carBulldozer, GET_ENTITY_BONE_INDEX_BY_NAME(carBulldozer, "seat_pside_r"), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						SET_CURRENT_PED_WEAPON(pedSelector.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_COMBATMG, TRUE)
						TASK_PLAY_ANIM(pedSelector.pedID[SELECTOR_PED_MICHAEL], "MISSHeistBank_Bulldozer", "Shovel_Idle_Michael", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					ENDIF
				ENDIF
				
				IF IS_ENTITY_OK(EnemyTank.thisDriver)
					CLEAR_PED_TASKS(EnemyTank.thisDriver)
					TASK_VEHICLE_AIM_AT_PED(EnemyTank.thisDriver, PLAYER_PED_ID())
					SET_PED_FIRING_PATTERN(EnemyTank.thisDriver, FIRING_PATTERN_SINGLE_SHOT)
				ENDIF
		
				SET_VEHICLE_LIGHTS(carBulldozer, FORCE_VEHICLE_LIGHTS_ON)
						
				IS_TANK_CUTSCENE_COMPLETE()
				DELETE_ARRAY_OF_VEHICLES(chinooks)
				DELETE_ARRAY_OF_PEDS(chinookPilots)
				
				REPLAY_STOP_EVENT()
				
				START_AUDIO_SCENE("PS_2A_GO_TO_FACTORY")
				
				iStageRescueBuddys++
			ENDIF
		BREAK
			
		CASE 5
		
			HIDE_HUD_AND_RADAR_THIS_FRAME()
		
			IF NOT IS_CUTSCENE_PLAYING() 
				IF IS_TANK_CUTSCENE_COMPLETE() 
					
					REQUEST_WAYPOINT_RECORDING("RBHchkTrevor")
					REQUEST_WAYPOINT_RECORDING("RBHChkFrank")
					RESET_GAME_CAMERA()
					SET_CUTSCENE_RUNNING(FALSE)
				
					//IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
					//	DO_SWITCH_EFFECT(CHAR_FRANKLIN)
					//ENDIF
					
					//Prestream next stage:
					REQUEST_MODEL(S_M_Y_Marine_03) //?Move
					REQUEST_MODEL(A_C_HEN)			
					REQUEST_MODEL(S_F_Y_Factory_01) //- female factory worker
					REQUEST_ANIM_SET("move_f@scared")

					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SETUP_WANTED_LEVEL(5, TRUE, 100.0, 10.0, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
					RESTART_TIMER_NOW(tmrTankSceneController)
					
					IF NOT IS_ENTITY_DEAD(carBulldozer)
						STOP_PLAYBACK_RECORDED_VEHICLE(carBulldozer)
					ENDIF
					
					iStageRescueBuddys++
				ENDIF
			ENDIF
		BREAK
		
		
		CASE 6
			DISTANT_COP_CAR_SIRENS(FALSE)
			UPDATE_TANK_FIRING()
					
			//SETUP_WANTED_LEVEL_ONLY_HELIS(6)
			
			REMOVE_VEHICLE_COMBAT_AVOIDANCE_AREA(avoidAreaContinueDownStreet)
			ADD_VEHICLE_COMBAT_ANGLED_AVOIDANCE_AREA(<<-42.813847,6280.207031,29.390860>>, <<-156.063568,6188.274902,37.746803>>, 92.250000)
		
			sLocatesData.LocationBlip = CREATE_BLIP_FOR_COORD( << -71.3082, 6266.9468, 31.3839 >> )
			SET_BLIP_ROUTE(sLocatesData.LocationBlip, TRUE)
					
			iStageRescueBuddys++					
		BREAK
				
		CASE 7
			UPDATE_TANK_FIRING()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
		
			//IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, << -71.3082, 6266.9468, 31.3839 >>, <<7.75, 7.75, 6.0>>, FALSE, carBulldozer , "GETCHICK", "", "GETBCKDZ", FALSE, 0, FALSE)	
			IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, << -71.3082, 6266.9468, 31.3839 >>, <<7.75, 7.75, 6.0>>, FALSE, carBulldozer , "GETCHICK", "", "GETBCKDZ", FALSE, 0, FALSE)	
			
			//PRINT_NOW("TANKESC", DEFAULT_GOD_TEXT_TIME, 1)
			
		//	IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				//Fuck it, go through the chicken factory
				
				SETTIMERA(0)
				bInitialBullDozerConvo = FALSE
				bInitialSawTankConvo = FALSE
				REQUEST_ANIM_DICT("MISSHeistPaletoScore2")
				REQUEST_ANIM_SET("ANIM_GROUP_MOVE_BALLISTIC")
				REQUEST_ANIM_SET("MOVE_STRAFE_BALLISTIC")
				iStageRescueBuddys++	
		//	ENDIF
		BREAK
		
		CASE 8
		CASE 9
		CASE 10
			
			IF bInitialSawTankConvo = FALSE
			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_TANK2", CONV_PRIORITY_VERY_HIGH)
					bInitialSawTankConvo = TRUE
				ENDIF
			ENDIF
			
			//PLay some dialogue abotu the guy dying.
			IF NOT bIsBuddyAPro
			
				IF bInitialBullDozerConvo = FALSE
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_BULLDO", CONV_PRIORITY_VERY_HIGH)
						bInitialBullDozerConvo = TRUE
					ENDIF
				ENDIF
				
				IF bInitialBullDozerConvo2 = FALSE
				AND bInitialBullDozerConvo = TRUE
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
//					IF g_replay.iReplayInt[REPLAY_VALUE_PICKEDUP_BUDDYS_BAG] = 1
//						IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_HAVECASH", CONV_PRIORITY_VERY_HIGH)
							bInitialBullDozerConvo2 = TRUE
//						ENDIF
//					ELSE
//						IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_NOTGOT", CONV_PRIORITY_VERY_HIGH)
							bInitialBullDozerConvo2 = TRUE
//						ENDIF
//					ENDIF
				ENDIF
			ELSE
				IF bInitialBullDozerConvo = FALSE
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_BULLCHAT", CONV_PRIORITY_VERY_HIGH)
						bInitialBullDozerConvo = TRUE
					ENDIF
				ENDIF	
				
			ENDIF
			
			UPDATE_TANK_FIRING()
			IF iStageRescueBuddys = 8
				IF TIMERA() > 2000
					IF NOT IS_ENTITY_DEAD(EnemyTank.thisCar)
						IF IS_ENTITY_ON_SCREEN(EnemyTank.thisCar)
							IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_SEETANK", CONV_PRIORITY_VERY_HIGH)
								iStageRescueBuddys = 9
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_UD)
		
			IF iStageRescueBuddys = 9	
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_SEETANK2", CONV_PRIORITY_VERY_HIGH)
					iStageRescueBuddys = 10
				ENDIF
			ENDIF
				
//			ENEMY_TANK_SHOOT_AROUND_PLAYER()
		
			IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, << -71.3082, 6266.9468, 31.3839 >>, <<7.75, 7.75, 6.0>>, FALSE, carBulldozer , "GETCHICK", "", "GETBCKDZ", FALSE, 0, FALSE)	
			AND HAS_ANIM_DICT_LOADED("MISSHeistPaletoScore2")	
			AND HAS_ANIM_SET_LOADED("ANIM_GROUP_MOVE_BALLISTIC")	
			AND HAS_ANIM_SET_LOADED("move_strafe_ballistic")	
				
				CLEAR_PED_PARACHUTE_PACK_VARIATION(pedSelector.pedID[SELECTOR_PED_MICHAEL])
				
				SET_VEHICLE_LIGHTS(carBulldozer, FORCE_VEHICLE_LIGHTS_ON)
				
				SET_VEHICLE_BULLDOZER_ARM_POSITION(carBulldozer, -0.05, TRUE)
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(carBulldozer)
				
				WAIT(0)	//hack to fix bucket.
				
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL) 

				TRIGGER_MUSIC_EVENT("RH2A_CLUCK_ARRIVE")
						
				IF NOT IS_PED_INJURED(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					SET_PED_MOVEMENT_CLIPSET(pedSelector.pedID[SELECTOR_PED_MICHAEL], "ANIM_GROUP_MOVE_BALLISTIC")
				ENDIF
				IF NOT IS_PED_INJURED(pedTrevor())
					SET_PED_MOVEMENT_CLIPSET(pedTrevor(), "ANIM_GROUP_MOVE_BALLISTIC")
				ENDIF
	
				IF NOT IS_PED_INJURED(pedGunman)
					SET_PED_MOVEMENT_CLIPSET(pedGunman, "ANIM_GROUP_MOVE_BALLISTIC")
				ENDIF
				
				REMOVE_VEHICLE_RECORDING(1, "RBHEnemyTank")
				
				IF IS_ENTITY_OK(EnemyTank.thisDriver)
					CLEAR_PED_TASKS(EnemyTank.thisDriver)
				ENDIF
				
				CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_CHICKE", CONV_PRIORITY_VERY_HIGH)
				
				//CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_BACKOUT", CONV_PRIORITY_VERY_HIGH)
				
				RESET_TYRE_POP_ARRAY()
				
				IF NOT IS_ENTITY_DEAD(carBulldozer)
					SET_ENTITY_COORDS(carBulldozer, <<-76.3639, 6273.8164, 30.3830>>)
					SET_ENTITY_HEADING(carBulldozer, -125.280) // 230.9405)
//					SET_VEHICLE_BULLDOZER_ARM_POSITION(carBulldozer, 0.0, TRUE)
//					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(carBulldozer)
				ENDIF
							
				DETACH_ENTITY(pedSelector.pedID[SELECTOR_PED_MICHAEL])
				DETACH_ENTITY(pedTrevor())
				
				CLEAR_AREA(<<-76.3639, 6273.8164, 30.3830>>, 10.0, TRUE)
				
				CLEAR_PED_TASKS_IMMEDIATELY(pedTrevor())
				
				iSceneId = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			
				TASK_SYNCHRONIZED_SCENE(pedSelector.pedID[SELECTOR_PED_MICHAEL], iSceneId, "MISSHeistPaletoScore2", "Dump_Exit_Michael", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
				TASK_SYNCHRONIZED_SCENE(pedTrevor(), iSceneId, "MISSHeistPaletoScore2", "Dump_Exit_Trevor", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT) 	
				SET_SYNCHRONIZED_SCENE_LOOPED(iSceneId, FALSE)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSelector.pedID[SELECTOR_PED_MICHAEL])
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedTrevor())
								
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSceneId, carBulldozer, GET_ENTITY_BONE_INDEX_BY_NAME(carBulldozer, "bodyshell"))
		
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, 1000, TRUE, TRUE)
					
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0, ECF_USE_RIGHT_ENTRY)
		
				cutsceneCamera = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
			
				PLAY_SYNCHRONIZED_CAM_ANIM(cutsceneCamera, iSceneId, "dump_exit_camera", "MISSHeistpaletoscore2")
				SET_CAM_ACTIVE(cutsceneCamera, TRUE)
				
				SETTIMERA(0)
				
				STOP_AUDIO_SCENE("PS_2A_TANK_ARRIVES")
				
				IF IS_ENTITY_OK(EnemyTank.thisCar)
					SET_PLAYBACK_SPEED(EnemyTank.thisCar, 0)
				ENDIF
				
				IF IS_ENTITY_OK(EnemyTank.thisDriver)
					CLEAR_PED_TASKS(EnemyTank.thisDriver)
				ENDIF
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				SET_CUTSCENE_RUNNING(TRUE)
				iStageRescueBuddys = 11
	
			ENDIF
			
		BREAK
		
		CASE 11
			
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()				
				DO_SCREEN_FADE_OUT(500)
				WHILE  IS_SCREEN_FADING_OUT()
					WAIT(0)
				ENDWHILE
				bSkippedArriveAtChickenFactoryCut = TRUE
			ENDIF
		
		
			//IF TIMERA() > 2500
			IF GET_SYNCHRONIZED_SCENE_PHASE(iSceneId) >= 0.84//0.84
			OR bSkippedArriveAtChickenFactoryCut = TRUE
				bSkippedArriveAtChickenFactoryCut = FALSE
				
				destinationCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)

				SET_CAM_COORD(destinationCam,<<-74.218201,6271.293945,32.281242>>)
				SET_CAM_ROT(destinationCam,<<-11.386656,0.513621,-159.661423>>)
				SET_CAM_FOV(destinationCam,45.000000)
				SET_CAM_ACTIVE(destinationCam, TRUE)
			
				REPLAY_STOP_EVENT()
				
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 5.0)
			
				DO_SWITCH_EFFECT(CHAR_MICHAEL)
				IF NOT IS_PED_INJURED(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					CLEAR_PED_TASKS_IMMEDIATELY(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					TASK_GO_STRAIGHT_TO_COORD(pedSelector.pedID[SELECTOR_PED_MICHAEL], GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(pedSelector.pedID[SELECTOR_PED_MICHAEL], <<0.0, 2.0, -0.5>>), PEDMOVE_WALK)
					FORCE_PED_MOTION_STATE(pedSelector.pedID[SELECTOR_PED_MICHAEL], MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT, TRUE)
				ENDIF
				#IF IS_DEBUG_BUILD
				//	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
				#ENDIF				
				mission_stage = STAGE_CHICKEN_FACTORY
			ENDIF				
		BREAK

	ENDSWITCH
	
	//You destroyed the bulldozer!
	IF NOT IS_VEHICLE_DRIVEABLE(carBulldozer)
		IF IS_ENTITY_OK(EnemyTank.thisDriver)		
			CLEAR_PED_TASKS(EnemyTank.thisDriver)
		ENDIF
		MISSION_FLOW_SET_FAIL_REASON("DOZERDEST")
		Mission_Failed()

	ENDIF
	
	IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), carBulldozer) > 100.00
		MISSION_FLOW_SET_FAIL_REASON("DOZERLOST")
		Mission_Failed()
	ENDIF
	
	//If the player goes on an adventure.... kill him.
	IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-207.110428,6298.521484,29.897001>>, <<-57.193317,6247.187012,61.477657>>, 82.750000)

		SETUP_WANTED_LEVEL(5, TRUE, 100.0, 10.0, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
		//APPLY_DAMAGE_TO_PED(PLAYER_PED_ID(), 1000, TRUE)
	ELSE
		SETUP_WANTED_LEVEL(5, TRUE, 100.0, 10.0, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
	ENDIF
	
ENDPROC

