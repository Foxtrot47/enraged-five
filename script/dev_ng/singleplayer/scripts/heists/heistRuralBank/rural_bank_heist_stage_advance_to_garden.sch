USING "commands_script.sch"
USING "commands_camera.sch"
USING "script_maths.sch"
USING "rural_bank_heist_support.sch"

TRIGGER_BOX	tb_MotelTunnel
TRIGGER_BOX	tb_MaidStart

BOOL	bThirdFenceDestroyed = FALSE
BOOL	bPlayedMichaelHurryDialogue = FALSE
BOOL	bTrevorRespondedToCarBlowingUp = FALSE
INT		iMaidState = 0 
INT		iPoolCarState = 0
INT 	iBackupCarState = 0
INT 	iFlyoverHeliState = 0 


PROC CREATE_ADVANCE_THROUGH_GARDENS_TRIGGERS()
	
	tb_MotelTunnel = CREATE_TRIGGER_BOX(<<-153.489288,6429.287109,28.915987>>, <<-150.345718,6432.444336,34.665985>>, 100.000000)
	tb_MaidStart = CREATE_TRIGGER_BOX(<<-153.489288,6429.287109,30.915987>>, <<-150.345718,6432.444336,34.665985>>, 5.000000)
ENDPROC

PROC UNBLOCK_ALL_COPS_OUTSIDE_BANK()

	INT i
	FOR i = 0 TO NUMBER_OF_COPS_OUTSIDE_BANK - 1
		IF NOT IS_PED_INJURED(pedsCopsOutsideBank[i])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsCopsOutsideBank[i], FALSE)
		ENDIF
	ENDFOR

ENDPROC

PROC KILL_OFF_COPS_STAGE1()
	
	INT i
	
	FOR i = 0 TO NUMBER_OF_COPS_OUTSIDE_BANK - 1
		IF NOT IS_PED_INJURED(pedsCopsOutsideBank[i])
			APPLY_DAMAGE_TO_PED(pedsCopsOutsideBank[i], 101, TRUE) //101 will take a 200 health ped below 100 to make them injured (but still maintain movement)
			SET_PED_AS_NO_LONGER_NEEDED(pedsCopsOutsideBank[i])
		ENDIF
	ENDFOR

ENDPROC


PROC CREATE_SOME_COPS_FOR_GUYS_TO_KILL_IN_ADVANCE_TO_GARDENS()

	RESET_MISSION_STATS_ENTITY_WATCH()

	carsFodderVehicles[0] = CREATE_VEHICLE(mnCopCarModel, << -129.0669, 6421.2173, 30.5453 >>, 190.7542)
	
	carsFodderVehicles[1] = CREATE_VEHICLE(mnCopCarModel, << -93.5882, 6434.6997, 30.4578 >>, 20.3393)
	carsFodderVehicles[2] = CREATE_VEHICLE(mnCopCarModel, << -97.2424, 6441.7954, 30.3291 >>, 45.8091)
	//carsFodderVehicles[3] = CREATE_VEHICLE(mnCopCarModel, << -127.3383, 6446.6948, 30.5282 >>, 72.0379)
	carsFodderVehicles[4] = CREATE_VEHICLE(mnCopCarModel, << -102.5140, 6420.9795, 30.3402 >>, 160.4660)
//	carsFodderVehicles[5] = CREATE_VEHICLE(mnCopCarModel, << -129.7266, 6417.4912, 30.5967 >>, 15.3517)
//	carsFodderVehicles[6] = CREATE_VEHICLE(mnCopCarModel, << -129.7266, 6417.4912, 30.5967 >>, 15.3517)
//	carsFodderVehicles[7] = CREATE_VEHICLE(mnCopCarModel, << -129.7266, 6417.4912, 30.5967 >>, 15.3517)
	
	EXPLODE_VEHICLE(carsFodderVehicles[0], FALSE)
	EXPLODE_VEHICLE(carsFodderVehicles[1], FALSE)
	EXPLODE_VEHICLE(carsFodderVehicles[2], FALSE)
	//EXPLODE_VEHICLE(carsFodderVehicles[3], FALSE)
	EXPLODE_VEHICLE(carsFodderVehicles[4], FALSE)
//	EXPLODE_VEHICLE(carsFodderVehicles[5], FALSE)
//	EXPLODE_VEHICLE(carsFodderVehicles[6], FALSE)
//	EXPLODE_VEHICLE(carsFodderVehicles[7], FALSE)
	

	pedsFodderCops[0] = CREATE_PED(PEDTYPE_MISSION, mnCopPedModel, <<-110.7198, 6404.6348, 30.6035>>, 11.9760 )
	pedsFodderCops[1] = CREATE_PED(PEDTYPE_MISSION, mnCopPedModel,<< -129.1361, 6412.4023, 30.4472 >>, 287.5895)
	pedsFodderCops[2] = CREATE_PED(PEDTYPE_MISSION, mnCopPedModel,  <<-137.3303, 6425.9175, 30.8022>>, 299.0522  )
	SET_ENTITY_IS_TARGET_PRIORITY(pedsFodderCops[0], TRUE)

	SET_PED_AS_DEFENSIVE_COP(pedsFodderCops[0], WEAPONTYPE_PUMPSHOTGUN, FALSE)
	SET_PED_AS_DEFENSIVE_COP(pedsFodderCops[1], WEAPONTYPE_PISTOL, FALSE)
	SET_PED_AS_DEFENSIVE_COP(pedsFodderCops[2], WEAPONTYPE_PISTOL, FALSE)
	
	TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedsFodderCops[0], 100.0)
	TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedsFodderCops[1], 100.0)
	TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedsFodderCops[2], 100.0)
	
	FORCE_PED_AI_AND_ANIMATION_UPDATE(pedsFodderCops[0], TRUE)
	FORCE_PED_AI_AND_ANIMATION_UPDATE(pedsFodderCops[1], TRUE)
	FORCE_PED_AI_AND_ANIMATION_UPDATE(pedsFodderCops[2], TRUE)
	
ENDPROC


PROC CLEANUP_SOME_COPS_FOR_GUYS_TO_KILL_IN_ADVANCE_TO_GARDENS()

	IF DOES_ENTITY_EXIST(pedsFodderCops[0])
		IF NOT IS_PED_INJURED(pedsFodderCops[0])
			APPLY_DAMAGE_TO_PED(pedsFodderCops[0], 101, TRUE)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(pedsFodderCops[1])
		IF NOT IS_PED_INJURED(pedsFodderCops[1])
			SET_PED_KEEP_TASK(pedsFodderCops[1], TRUE)
			//APPLY_DAMAGE_TO_PED(pedsFodderCops[1], 101, TRUE)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(pedsFodderCops[2])
		IF NOT IS_PED_INJURED(pedsFodderCops[2])
			SET_PED_KEEP_TASK(pedsFodderCops[2], TRUE)
			//APPLY_DAMAGE_TO_PED(pedsFodderCops[2], 101, TRUE)
		ENDIF
	ENDIF

	SET_PED_AS_NO_LONGER_NEEDED(pedsFodderCops[0])
	SET_PED_AS_NO_LONGER_NEEDED(pedsFodderCops[1])
	SET_PED_AS_NO_LONGER_NEEDED(pedsFodderCops[2])
	
	INT i
	FOR i = 0 TO 7
		IF DOES_ENTITY_EXIST(carsFodderVehicles[i])
			SET_VEHICLE_AS_NO_LONGER_NEEDED(carsFodderVehicles[i])
		ENDIF
	ENDFOR		

ENDPROC

FUNC BOOL REQUEST_ADVANCE_TO_GARDEN_ASSETS()

	
	REQUEST_MODEL(mnCopPedModel)
	REQUEST_MODEL(mnCopCarModel)
	REQUEST_MODEL(POLMAV)
	REQUEST_ANIM_SET("ANIM_GROUP_MOVE_BALLISTIC")
	REQUEST_PTFX_ASSET()
	REQUEST_SCRIPT_AUDIO_BANK("PS2A_Jeep_Skids")

	#IF IS_DEBUG_BUILD
		IF bTriggeredByDebugDoForceLoad = TRUE
			bTriggeredByDebugDoForceLoad = FALSE
		ENDIF
	#ENDIF
	
	IF HAS_MODEL_LOADED(mnCopCarModel)
	AND HAS_MODEL_LOADED(mnCopPedModel)
	AND HAS_MODEL_LOADED(POLMAV)
	AND HAS_PTFX_ASSET_LOADED()
	AND HAS_ANIM_SET_LOADED("ANIM_GROUP_MOVE_BALLISTIC")
		RETURN TRUE

	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bIsSuperDebugEnabled
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.1, "STRING", "Attempting to stream...")
		ENDIF
	#ENDIF

	RETURN FALSE
ENDFUNC

PROC COMBAT_FODDER_PEDS_BUDDY()

	IF (GET_GAME_TIMER() - iReTargetBuddy) > 2000
		
		SET_PED_SPHERE_DEFENSIVE_AREA(pedGunman, GET_ENTITY_COORDS(pedGunman), 0.75)
		
		IF NOT IS_PED_INJURED(pedsFodderCops[0])
			TASK_SHOOT_AT_ENTITY(pedGunman, pedsFodderCops[0], INFINITE_TASK_TIME, FIRING_TYPE_RANDOM_BURSTS)
		ELIF NOT IS_PED_INJURED(pedsFodderCops[1])					
			TASK_SHOOT_AT_ENTITY(pedGunman, pedsFodderCops[1], INFINITE_TASK_TIME, FIRING_TYPE_RANDOM_BURSTS)
		ELIF NOT IS_PED_INJURED(pedsFodderCops[2])
			TASK_SHOOT_AT_ENTITY(pedGunman, pedsFodderCops[2], INFINITE_TASK_TIME, FIRING_TYPE_RANDOM_BURSTS)
		ENDIF
		iReTargetBuddy = GET_GAME_TIMER()
	ENDIF

ENDPROC

PROC COMBAT_FODDER_PEDS_MICHAEL()

	IF (GET_GAME_TIMER() - iReTargetMichael) > 2000
		
		SET_PED_SPHERE_DEFENSIVE_AREA(PedMichael(), GET_ENTITY_COORDS(PedMichael()), 0.75)
		
		IF NOT IS_PED_INJURED(pedsFodderCops[2])
			TASK_SHOOT_AT_ENTITY(PedMichael(), pedsFodderCops[2], INFINITE_TASK_TIME, FIRING_TYPE_RANDOM_BURSTS)
		ELIF NOT IS_PED_INJURED(pedsFodderCops[1])					
			TASK_SHOOT_AT_ENTITY(PedMichael(), pedsFodderCops[1], INFINITE_TASK_TIME, FIRING_TYPE_RANDOM_BURSTS)
		ELIF NOT IS_PED_INJURED(pedsFodderCops[0])
			TASK_SHOOT_AT_ENTITY(PedMichael(), pedsFodderCops[0], INFINITE_TASK_TIME, FIRING_TYPE_RANDOM_BURSTS)	
		ENDIF
		iReTargetMichael = GET_GAME_TIMER()
	ENDIF

ENDPROC


INT iFenceCheckSchedulerTimer

INT iFenceSmashStage = 0

FUNC BOOL HAS_PLAYER_SHOT_THE_FIRST_FENCE()
	
	SWITCH iFenceSmashStage
	
		CASE 0
			//Only check every half second to stop func taking 1ms
			IF GET_GAME_TIMER() - iFenceCheckSchedulerTimer > 500
				IF HAS_CLOSEST_OBJECT_OF_TYPE_BEEN_BROKEN(<<-173.05214, 6408.49072, 31.75>>, 1.0, PROP_FNCWOOD_01A, SEARCH_LOCATION_EXTERIORS)
				OR HAS_CLOSEST_OBJECT_OF_TYPE_BEEN_BROKEN(<<-174.78474, 6410.23535, 31.75>>, 2.0, PROP_FNCWOOD_01B, SEARCH_LOCATION_EXTERIORS)
					
					REPLAY_RECORD_BACK_FOR_TIME(3.0)
					
					iFenceSmashStage++
				ENDIF
				iFenceCheckSchedulerTimer = GET_GAME_TIMER()
			ENDIF
		
		BREAK
		
		CASE 1
			IF GET_GAME_TIMER() - iFenceCheckSchedulerTimer > 1500
				//IF NOT HAS_CLOSEST_OBJECT_OF_TYPE_BEEN_COMPLETELY_DESTROYED(<<-173.05214, 6408.49072, 31.75>>, 1.0, PROP_FNCWOOD_01A, SEARCH_LOCATION_EXTERIORS)
				IF NOT HAS_CLOSEST_OBJECT_OF_TYPE_BEEN_COMPLETELY_DESTROYED(<<-174.78474, 6410.23535, 31.75>>, 2.0, PROP_FNCWOOD_01B, SEARCH_LOCATION_EXTERIORS)
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-174.78726, 6410.16797, 31.72257>>) > 3.0
						ADD_EXPLOSION( <<-174.78726, 6410.16797, 31.72257>>, EXP_TAG_GRENADE, 0.4, FALSE, TRUE, 0.0)
						iFenceSmashStage = 0
						RETURN TRUE					
					ENDIF
				ELSE
					iFenceSmashStage = 0
					RETURN TRUE					
				ENDIF
				
			ENDIF
		BREAK
	
	ENDSWITCH
		
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_PLAYER_OR_BUDDY_SHOT_SECOND_FENCE()

	//Only check every half second to stop func taking 1ms
	IF GET_GAME_TIMER() - iFenceCheckSchedulerTimer > 500	
		IF HAS_CLOSEST_OBJECT_OF_TYPE_BEEN_BROKEN(<<-186.03355, 6390.39209, 31.60>>, 1.5, prop_fncwood_01a, SEARCH_LOCATION_EXTERIORS)
		OR HAS_CLOSEST_OBJECT_OF_TYPE_BEEN_BROKEN(<<-187.95229, 6392.29736, 31.64144>>, 2.0, prop_fncwood_01b, SEARCH_LOCATION_EXTERIORS)
			ADD_EXPLOSION( <<-187.95229, 6392.29736, 31.64144>>, EXP_TAG_GRENADE, 0.5, FALSE, TRUE, 0.0)
			RETURN TRUE
		ENDIF
		iFenceCheckSchedulerTimer = GET_GAME_TIMER()
	ENDIF
	

	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_PLAYER_OR_BUDDY_SHOT_THIRD_FENCE()

	//Only check every half second to stop func taking 1ms
	IF GET_GAME_TIMER() - iFenceCheckSchedulerTimer > 500
		IF HAS_CLOSEST_OBJECT_OF_TYPE_BEEN_BROKEN(<<-183.66119, 6371.80566, 31.68116>>, 4.0, prop_fncwood_01b, SEARCH_LOCATION_EXTERIORS)
			bThirdFenceDestroyed = TRUE
			RETURN TRUE
		ENDIF
		iFenceCheckSchedulerTimer = GET_GAME_TIMER()
	ENDIF
			
	RETURN FALSE
	
ENDFUNC



//INT iTimeOfMichaelGoingTowardsFence

PROC RUBBER_BAND_WAYPOINTS_AND_SHOOT_WHEN_PAUSED_CREW(BOOL bStopWhenOffScreen = FALSE, FLOAT fWalkDistance = 9.0)//, FLOAT fPauseDistance = 15.0)

	ENTITY_INDEX aimedAtEntity

	GET_ENTITY_PLAYER_IS_FREE_AIMING_AT(PLAYER_ID(), aimedAtEntity)

	IF bStopWhenOffScreen
	ENDIF
	
	IF fWalkDistance = 0.0
	ENDIF
	
	IF bLosingMichael
	AND bLosingBuddy
		IF NOT bDisplayLosingGodText
			IF NOT (NOT HAS_PLAYER_SHOT_THE_FIRST_FENCE() AND (iShootingStageCrewMember > 9))
				PRINT_STRING_IN_STRING("LOSINGB", strCrewMember, DEFAULT_GOD_TEXT_TIME, 1)
				bDisplayLosingGodText = TRUE
			ENDIF
		ENDIF
	ELSE
		bDisplayLosingGodText = FALSE
	ENDIF

	IF NOT IS_PED_INJURED(pedGunman)
	AND NOT IS_ENTITY_IN_ANGLED_AREA(pedGunman, <<-147.331482,6435.314453,30.915949>>, <<-158.268158,6424.815430,34.905342>>, 6.500000)
	
		//FLOAT fDistanceBuddy = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedGunman))
		
		INT iClosestWaypointToPlayerGunman
		INT iClosestWaypointToGunman
		WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT("RBHGdnCrew", GET_ENTITY_COORDS(PLAYER_PED_ID()), iClosestWaypointToPlayerGunman)
		WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT("RBHGdnCrew", GET_ENTITY_COORDS(pedGunman), iClosestWaypointToGunman)
			
		//IF fDistanceBuddy > (fWalkDistance * 0.92)
		IF iClosestWaypointToPlayerGunman < (iClosestWaypointToGunman - 6)
		//OR (NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(pedGunman), 1.4)  AND bStopWhenOffScreen)
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedGunman)
				WAYPOINT_PLAYBACK_OVERRIDE_SPEED(pedGunman, PEDMOVE_WALK)
			ENDIF
					
			#IF IS_DEBUG_BUILD IF bIsSuperDebugEnabled
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(pedGunman, "PEDMOVE_WALK", 0.0, 0, 0, 255)
			ENDIF #ENDIF
			
		ELSE
			
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedGunman)
				WAYPOINT_PLAYBACK_OVERRIDE_SPEED(pedGunman, PEDMOVE_RUN)
			ENDIF
			#IF IS_DEBUG_BUILD IF bIsSuperDebugEnabled
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(pedGunman, "PEDMOVE_RUN", 0.0, 0, 255, 0)
			ENDIF #ENDIF
		ENDIF
		
//		//IF (fDistanceBuddy > (fPauseDistance * 1.15) AND (iClosestWaypointToGunman >  iClosestWaypointToPlayerGunman))
//		IF (NOT HAS_PLAYER_SHOT_THE_FIRST_FENCE() AND (iShootingStageCrewMember > 8))
//		
//			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedGunman)
//				COMBAT_FODDER_PEDS_BUDDY()
//			ENDIF
//			
//			bLosingBuddy = TRUE
//			
//			#IF IS_DEBUG_BUILD IF bIsSuperDebugEnabled
//				DRAW_DEBUG_TEXT_ABOVE_ENTITY(pedGunman, "Pause!", 1.0, 255, 0, 0)
//			ENDIF #ENDIF
//		ELIF fDistanceBuddy < (fPauseDistance * 0.95) 
//			
//			bLosingBuddy = FALSE
//						
//			IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedGunman)
//				
//				iTimeOfMichaelGoingTowardsFence = GET_GAME_TIMER()
//								
//				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGunman, TRUE)
//				REMOVE_PED_DEFENSIVE_AREA(pedGunman)
//				SET_PED_COMBAT_MOVEMENT(pedGunman, CM_DEFENSIVE)
//				OPEN_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
//					TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHGdnCrew", 0, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_START_FROM_CLOSEST_POINT) //EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE
//					TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, << -184.9843, 6365.8989, 30.4902 >> , 1.5)
//					//TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, << -184.9843, 6365.8989, 30.4902 >>,  << -159.1230, 6356.5781, 30.4710 >>, PEDMOVE_WALK, TRUE)
//					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.00)
//				CLOSE_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
//				TASK_PERFORM_SEQUENCE(pedGunman, seqDontMoveBothAtSameTime)
//				CLEAR_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
//			ENDIF
//			#IF IS_DEBUG_BUILD IF bIsSuperDebugEnabled
//				DRAW_DEBUG_TEXT_ABOVE_ENTITY(pedGunman, "Unpause!", 1.0, 0, 255, 0)
//			ENDIF #ENDIF
//		ENDIF
	ENDIF
	
ENDPROC

PROC RUBBER_BAND_WAYPOINTS_AND_SHOOT_WHEN_PAUSED_MICHAEL(BOOL bStopWhenOffScreen = FALSE, FLOAT fWalkDistance = 9.0)//, FLOAT fPauseDistance = 15.0, BOOl bCombat = TRUE)
		
	ENTITY_INDEX aimedAtEntity

	GET_ENTITY_PLAYER_IS_FREE_AIMING_AT(PLAYER_ID(), aimedAtEntity)

	IF bStopWhenOffScreen
	ENDIF
	
	IF fWalkDistance = 0.0
	ENDIF
	
	IF bLosingMichael
	AND bLosingBuddy
		IF NOT bDisplayLosingGodText
			IF NOT (NOT HAS_PLAYER_SHOT_THE_FIRST_FENCE() AND iShootingStageMichael > 6)
				PRINT_STRING_IN_STRING("LOSINGB", strCrewMember, DEFAULT_GOD_TEXT_TIME, 1)
			ENDIF
			bDisplayLosingGodText = TRUE
		ENDIF
	ELSE
		bDisplayLosingGodText = FALSE
	ENDIF

		
	IF NOT IS_PED_INJURED(PedMichael())
	AND NOT IS_ENTITY_IN_ANGLED_AREA(PedMichael(), <<-147.331482,6435.314453,30.915949>>, <<-158.268158,6424.815430,34.905342>>, 6.500000)	
		
		//FLOAT fDistanceMichael= GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedMichael()))
		
		INT iClosestWaypointToPlayer
		INT iClosestWaypointToMichael
		WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT( "RBHGdnMichael", GET_ENTITY_COORDS(PLAYER_PED_ID()), iClosestWaypointToPlayer)
		WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT( "RBHGdnMichael", GET_ENTITY_COORDS(PedMichael()), iClosestWaypointToMichael)

		
		//IF fDistanceMichael > fWalkDistance
		IF iClosestWaypointToPlayer < (iClosestWaypointToMichael - 6)
		//OR (NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(PedMichael()), 1.4)  AND bStopWhenOffScreen)
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
				WAYPOINT_PLAYBACK_OVERRIDE_SPEED(PedMichael(), PEDMOVE_WALK)
			ENDIF
			#IF IS_DEBUG_BUILD
				IF bIsSuperDebugEnabled
					DRAW_DEBUG_TEXT_ABOVE_ENTITY(PedMichael(), "PEDMOVE_WALK", 0.0, 0, 0, 255)
				ENDIF
			#ENDIF
		ELSE	
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
				WAYPOINT_PLAYBACK_OVERRIDE_SPEED(PedMichael(), PEDMOVE_RUN)
			ENDIF
			#IF IS_DEBUG_BUILD
				IF bIsSuperDebugEnabled
					DRAW_DEBUG_TEXT_ABOVE_ENTITY(PedMichael(), "PEDMOVE_RUN", 0.0, 0, 0, 255)
				ENDIF
			#ENDIF
		ENDIF
		
		//PRINTLN("time of michael moving:", (GET_GAME_TIMER() - iTimeOfMichaelGoingTowardsFence))
		
		//IF (fDistanceMichael > fPauseDistance
		///AND (iClosestWaypointToMichael + 5 >  iClosestWaypointToPlayer))
//		IF (NOT HAS_PLAYER_SHOT_THE_FIRST_FENCE() AND iShootingStageMichael > 7)
//			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
//			AND bCombat
//				COMBAT_FODDER_PEDS_MICHAEL()
//				//SCRIPT_ASSERT("PAUSE!")
//			ENDIF
//			
//			bLosingMichael = TRUE
//			
//			#IF IS_DEBUG_BUILD 	IF bIsSuperDebugEnabled
//					DRAW_DEBUG_TEXT_ABOVE_ENTITY(PedMichael(), "Pause!", 1.0, 0, 255, 0)
//			ENDIF #ENDIF
//			
//			
//			
//		ELIF fDistanceMichael < (fPauseDistance * 0.9) AND (bLosingBuddy = FALSE) AND (GET_GAME_TIMER() - iTimeOfMichaelGoingTowardsFence) > 1500
//			
//			bLosingMichael = FALSE
//			
//			IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
//						
//				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedMichael(), TRUE)
//				REMOVE_PED_DEFENSIVE_AREA(PedMichael())
//				SET_PED_COMBAT_MOVEMENT(PedMichael(), CM_DEFENSIVE)
//				
//				OPEN_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
//					TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHGdnMichael", 0, EWAYPOINT_START_FROM_CLOSEST_POINT) //EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE
//					TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, << -181.6442, 6368.0581, 30.4902 >> , 1.5)
//					//TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, << -181.6442, 6368.0581, 30.4902 >>,  << -181.6744, 6351.2354, 30.4431 >>, PEDMOVE_WALK, TRUE)
//					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.00)
//				CLOSE_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
//				TASK_PERFORM_SEQUENCE(PedMichael(), seqDontMoveBothAtSameTime)
//				
//				CLEAR_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
//			
//				//SCRIPT_ASSERT("UNPAUSED!")
//			
//			ENDIF
//			#IF IS_DEBUG_BUILD IF bIsSuperDebugEnabled
//				DRAW_DEBUG_TEXT_ABOVE_ENTITY(PedMichael(), "Unpause!", 1.0, 255, 0, 0)
//			ENDIF #ENDIF
//		ENDIF
	ENDIF


ENDPROC


PROC EXPLODE_INCOMING_SETPIECES_IN_ADVANCE_TO_GARDENS()

	//mnCopCarModel
	IF NOT IS_ENTITY_DEAD(carsPoliceBackup[0].thisCar)
		//IF GET_ENTITY_SPEED(carsPoliceBackup[0].thisCar) < 12.00
		IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(carsPoliceBackup[1].thisCar, "CBCops") < 71.0
		OR GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(carsPoliceBackup[1].thisCar, "CBCops") > 79.0
			//PRINTLN("GET_ENTITY_SPEED(explodeEntity) < 15.00") PRINTFLOAT( GET_ENTITY_SPEED(carsPoliceBackup[0].thisCar)) PRINTNL()
			//NOt a typo, only explode this car if the other one isn't exploded!
			IF NOT IS_ENTITY_DEAD(carsPoliceBackup[1].thisCar)
				IF EXPLODE_VEHICLE_AT_HEALTH_LEVEL(carsPoliceBackup[0].thisCar, iWidgetCopCarExplosionHealth, FALSE, TRUE)
					//CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_COPBLT", CONV_PRIORITY_VERY_HIGH)
				ENDIF
			ELSE
				PRINTLN("other car sherriff dead ()\n")
			ENDIF
		ENDIF
	ENDIF
	
	//mnCopCarModel
	IF NOT IS_ENTITY_DEAD(carsPoliceBackup[1].thisCar)
		//IF GET_ENTITY_SPEED(carsPoliceBackup[1].thisCar) < 15.00
		IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(carsPoliceBackup[1].thisCar, "CBSwat") < 61.0
		OR GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(carsPoliceBackup[1].thisCar, "CBSwat") > 74.0
			//PRINTLN("GET_ENTITY_SPEED(explodeEntity) < 15.00") PRINTFLOAT( GET_ENTITY_SPEED(carsPoliceBackup[1].thisCar)) PRINTNL()
			//NOt a typo, only explode this car if the other one isn't exploded!
			IF NOT IS_ENTITY_DEAD(carsPoliceBackup[0].thisCar)
				IF EXPLODE_VEHICLE_AT_HEALTH_LEVEL(carsPoliceBackup[1].thisCar, iWidgetCopCarExplosionHealth, FALSE, TRUE)
					//CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_COPBLT", CONV_PRIORITY_VERY_HIGH)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
ENDPROC


//NAVDATA ndStructGunman
//NAVDATA ndStructMichael
	
PROC REEVALUATE_FODDER_TARGET()

	IF NOT IS_PED_INJURED(pedGunman)
		IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedGunman)
			ipedGunmanProgressStage1 = GET_PED_WAYPOINT_PROGRESS(pedGunman)
			
			//Reevaulate target ever so often
			IF ABSI((ipedGunmanProgressStage1Prev - ipedGunmanProgressStage1)) > 1
				//SCRIPT_ASSERT("boom")
				IF NOT IS_PED_INJURED(pedsFodderCops[0])
					WAYPOINT_PLAYBACK_START_SHOOTING_AT_PED(pedGunman, pedsFodderCops[0], FALSE)
				ELIF NOT IS_PED_INJURED(pedsFodderCops[1])					
					WAYPOINT_PLAYBACK_START_SHOOTING_AT_PED(pedGunman, pedsFodderCops[1], FALSE)
				ELIF NOT IS_PED_INJURED(pedsFodderCops[2])
					WAYPOINT_PLAYBACK_START_SHOOTING_AT_PED(pedGunman, pedsFodderCops[2], FALSE)
				ENDIF
				ipedGunmanProgressStage1Prev = ipedGunmanProgressStage1
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(PedMichael())
		IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
			iPedMichaelProgressStage1 = GET_PED_WAYPOINT_PROGRESS(PedMichael())
			//Reevaulate target ever so often
			IF ABSI((iPedMichaelProgressStage1Prev - iPedMichaelProgressStage1)) > 1
				//SCRIPT_ASSERT("michale boom")
				IF NOT IS_PED_INJURED(pedsFodderCops[0])
					WAYPOINT_PLAYBACK_START_SHOOTING_AT_PED(PedMichael(), pedsFodderCops[0], FALSE)
				ELIF NOT IS_PED_INJURED(pedsFodderCops[1])					
					WAYPOINT_PLAYBACK_START_SHOOTING_AT_PED(PedMichael(), pedsFodderCops[1], FALSE)
				ELIF NOT IS_PED_INJURED(pedsFodderCops[2])
					WAYPOINT_PLAYBACK_START_SHOOTING_AT_PED(PedMichael(), pedsFodderCops[2], FALSE)
				ENDIF
				iPedMichaelProgressStage1Prev = iPedMichaelProgressStage1
			ENDIF
		ENDIF
	ENDIF
			
ENDPROC

PROC REEVALUATE_POOLSIDE_TARGETS()

	IF NOT IS_PED_INJURED(pedGunman)
		IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedGunman)
			ipedGunmanProgressStage1 = GET_PED_WAYPOINT_PROGRESS(pedGunman)
			
			//Reevaulate target ever so often
			IF ABSI((ipedGunmanProgressStage1Prev - ipedGunmanProgressStage1)) > 1
				IF NOT IS_PED_INJURED(carsPolicePoolside[0].thisDriver)
					vGunmanTarget = GET_ENTITY_COORDS(carsPolicePoolside[0].thisDriver)
					WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(pedGunman, vGunmanTarget, FALSE)
				ELIF NOT IS_PED_INJURED(carsPolicePoolside[0].thisPassenger)					
					vGunmanTarget = GET_ENTITY_COORDS(carsPolicePoolside[0].thisPassenger)
					WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(pedGunman,vGunmanTarget, FALSE)
				ELIF NOT IS_PED_INJURED(carsPolicePoolside[1].thisDriver)
					vGunmanTarget = GET_ENTITY_COORDS(carsPolicePoolside[1].thisDriver)
					WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(pedGunman,vGunmanTarget, FALSE)
				ELIF NOT IS_PED_INJURED(carsPolicePoolside[1].thisPassenger)
					vGunmanTarget = GET_ENTITY_COORDS(carsPolicePoolside[1].thisPassenger)
					WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(pedGunman, vGunmanTarget, FALSE)
				ENDIF
				ipedGunmanProgressStage1Prev = ipedGunmanProgressStage1
			ENDIF
						
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(PedMichael())
		IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
			iPedMichaelProgressStage1 = GET_PED_WAYPOINT_PROGRESS(PedMichael())
			//Reevaulate target ever so often
			IF ABSI((iPedMichaelProgressStage1Prev - iPedMichaelProgressStage1)) > 1
				IF NOT IS_PED_INJURED(carsPolicePoolside[1].thisDriver)
					WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(PedMichael(), GET_ENTITY_COORDS(carsPolicePoolside[1].thisDriver), FALSE)
				ELIF NOT IS_PED_INJURED(carsPolicePoolside[1].thisPassenger)					
					WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(PedMichael(), GET_ENTITY_COORDS(carsPolicePoolside[1].thisPassenger), FALSE)
				ELIF NOT IS_PED_INJURED(carsPolicePoolside[0].thisDriver)
					WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(PedMichael(),GET_ENTITY_COORDS(carsPolicePoolside[0].thisDriver), FALSE)
				ELIF NOT IS_PED_INJURED(carsPolicePoolside[0].thisPassenger)
					WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(PedMichael(), GET_ENTITY_COORDS(carsPolicePoolside[0].thisPassenger), FALSE)
				ENDIF
				iPedMichaelProgressStage1Prev = iPedMichaelProgressStage1
			ENDIF
		ENDIF
	ENDIF
ENDPROC



PROC TASK_ALLY_SHOOT_FENCE1(PED_INDEX ped)
	SEQUENCE_INDEX taskSeq
	INT iTimeToFire = 900
	IF DOES_ENTITY_EXIST(ped) AND NOT IS_ENTITY_DEAD(ped)
		CLEAR_PED_TASKS(ped)
		OPEN_SEQUENCE_TASK(taskSeq)
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
			IF NOT HAS_CLOSEST_OBJECT_OF_TYPE_BEEN_COMPLETELY_DESTROYED(<<-174.12257, 6409.38867, 31.69038>>, 0.5, PROP_FNCWOOD_01B, SEARCH_LOCATION_EXTERIORS)
				TASK_SHOOT_AT_COORD(NULL, (<<-173.98236, 6409.24805, 32.09566>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-174.75789, 6410.13867, 32.12718>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
			ENDIF
			//IF NOT HAS_CLOSEST_OBJECT_OF_TYPE_BEEN_COMPLETELY_DESTROYED(<<-174.845,6410.146,31.052>>, 0.5, PROP_FNCWOOD_01B, SEARCH_LOCATION_EXTERIORS)
				TASK_SHOOT_AT_COORD(NULL, (<<-175.66832, 6410.93457, 32.10097>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
			//ENDIF
		SET_SEQUENCE_TO_REPEAT(taskSeq, REPEAT_FOREVER)
		CLOSE_SEQUENCE_TASK(taskSeq)
		TASK_PERFORM_SEQUENCE(ped, taskSeq)
		CLEAR_SEQUENCE_TASK(taskSeq)
	ENDIF
ENDPROC

PROC TASK_ALLY_SHOOT_FENCE2(PED_INDEX ped)
	SEQUENCE_INDEX taskSeq
	INT iTimeToFire = 500
	IF DOES_ENTITY_EXIST(ped) AND NOT IS_ENTITY_DEAD(ped)
		CLEAR_PED_TASKS(ped)
		OPEN_SEQUENCE_TASK(taskSeq)
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
			IF NOT HAS_CLOSEST_OBJECT_OF_TYPE_BEEN_COMPLETELY_DESTROYED(<<-186.13914, 6390.33154, 31.61727>>, 0.5, prop_fncwood_01a, SEARCH_LOCATION_EXTERIORS)
				TASK_SHOOT_AT_COORD(NULL, (<<-185.71625, 6389.94727, 30.99580>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-185.94463, 6390.17578, 31.00704>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-186.14894, 6390.37988, 31.00529>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-186.38776, 6390.61865, 31.00359>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-186.62468, 6390.85596, 31.01823>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
			ENDIF
			
			IF NOT HAS_CLOSEST_OBJECT_OF_TYPE_BEEN_COMPLETELY_DESTROYED(<<-187.38452, 6391.61475, 31.61837>>, 0.5, prop_fncwood_01b, SEARCH_LOCATION_EXTERIORS)
				TASK_SHOOT_AT_COORD(NULL, (<<-186.85980, 6391.08984, 31.00777>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-187.11484, 6391.34521, 31.00700>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-187.37485, 6391.60498, 31.00826>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-187.63492, 6391.86475, 31.01122>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-187.87077, 6392.10107, 31.02617>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
			ENDIF
			
			IF NOT HAS_CLOSEST_OBJECT_OF_TYPE_BEEN_COMPLETELY_DESTROYED(<<-188.62959, 6392.85986, 31.59967>>, 0.5, prop_fncwood_01b, SEARCH_LOCATION_EXTERIORS)
				TASK_SHOOT_AT_COORD(NULL, (<<-188.11876, 6392.34912, 31.00767>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-188.39644, 6392.62695, 30.97907>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-188.61389, 6392.84424, 31.01029>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-188.91495, 6393.14551, 31.01674>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-189.21432, 6393.43262, 30.99523>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
			ENDIF
		SET_SEQUENCE_TO_REPEAT(taskSeq, REPEAT_FOREVER)
		CLOSE_SEQUENCE_TASK(taskSeq)
		TASK_PERFORM_SEQUENCE(ped, taskSeq)
		CLEAR_SEQUENCE_TASK(taskSeq)
	ENDIF
ENDPROC

PROC TASK_ALLY_SHOOT_FENCE3(PED_INDEX ped)
	SEQUENCE_INDEX taskSeq
	INT iTimeToFire = 500
	IF DOES_ENTITY_EXIST(ped) AND NOT IS_ENTITY_DEAD(ped)
		CLEAR_PED_TASKS(ped)
		OPEN_SEQUENCE_TASK(taskSeq)
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
			
			IF NOT HAS_CLOSEST_OBJECT_OF_TYPE_BEEN_COMPLETELY_DESTROYED(<<-183.625,6371.763,30.982>>, 0.5, prop_fncwood_01b, SEARCH_LOCATION_EXTERIORS)
				TASK_SHOOT_AT_COORD(NULL, (<<-183.67509, 6371.67773, 31.00482>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-183.98904, 6371.36426, 31.02478>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-184.30922, 6371.04492, 31.00653>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-184.51685, 6370.83789, 31.03237>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
				TASK_SHOOT_AT_COORD(NULL, (<<-184.75349, 6370.58984, 30.98511>>), iTimeToFire, FIRING_TYPE_CONTINUOUS)
			ENDIF
			
		SET_SEQUENCE_TO_REPEAT(taskSeq, REPEAT_FOREVER)
		CLOSE_SEQUENCE_TASK(taskSeq)
		TASK_PERFORM_SEQUENCE(ped, taskSeq)
		CLEAR_SEQUENCE_TASK(taskSeq)
	ENDIF
ENDPROC

INT iExtraMotelGuysStage
PED_INDEX pedsExtraMotelEntranceGuys[3]
INT iExtraGuyTimer[3]
INT iExtraGuysSpawnedCounter
VECTOR vMotelCopSpawnPoint[3]
PROC SPAWN_SOME_GUYS_SO_THE_GUYS_DONT_STAND_MOTIONLESS_BY_THE_MOTEL_ENTRANCE()

	INT i
	vMotelCopSpawnPoint[0] = <<-136.1442, 6416.4824, 30.5889>>
	vMotelCopSpawnPoint[1] = <<-164.7589, 6447.4014, 30.9160>>
	//IN garden
	vMotelCopSpawnPoint[2] = <<-193.8550, 6426.8647, 30.5209>>

	SWITCH iExtraMotelGuysStage
	
		CASE 0
		
			IF eAllyAdvanceGardenState <= ALLY_GARDEN_WAIT_FOR_BOTH_THROUGH_TUNNEL//ALLY_GARDEN_WAIT_FOR_ALLIES_AT_MOTEL
		
				FOR i = 0 TO 2
				
					IF IS_PED_INJURED(pedsExtraMotelEntranceGuys[i])
					AND GET_GAME_TIMER() - iExtraGuyTimer[i] > 4000
						SET_PED_AS_NO_LONGER_NEEDED(pedsExtraMotelEntranceGuys[i])
				
					
						//Left box where you can see spawn point 0
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-129.332886,6398.491211,29.859959>>, <<-100.251808,6455.331543,37.969959>>, 28.000000)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-121.904655,6411.603516,30.225630>>, <<-172.393875,6357.170410,44.735134>>, 29.500000)
							vMotelCopSpawnPoint[0] = <<-120.6293, 6479.6953, 30.4417>>	
						ENDIF
				
						//Right box where you can see spawn point 1
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-207.319046,6445.890625,27.945650>>, <<-124.656677,6497.652832,42.878071>>, 43.000000)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-209.581085,6431.465820,29.214354>>, <<-177.635910,6464.754883,43.824341>>, 43.000000)
							vMotelCopSpawnPoint[1] = <<-99.3899, 6450.6147, 30.4544>>
						ENDIF
				
				
						pedsExtraMotelEntranceGuys[i] = CREATE_PED(PEDTYPE_COP, mnCopPedModel, vMotelCopSpawnPoint[i])
						IF GET_RANDOM_INT_IN_RANGE(0, 10) > 6
							SET_PED_AS_DEFENSIVE_COP(pedsExtraMotelEntranceGuys[i], WEAPONTYPE_PUMPSHOTGUN)
						ELSE
							SET_PED_AS_DEFENSIVE_COP(pedsExtraMotelEntranceGuys[i], WEAPONTYPE_PISTOL)
						ENDIF
						
						//Put on bullet proof jacket
						SET_PED_COMPONENT_VARIATION(pedsExtraMotelEntranceGuys[i], INT_TO_ENUM(PED_COMPONENT,9), 2, 0, 0) //(task)
						
						SET_PED_ARMOUR(pedsExtraMotelEntranceGuys[i], 50)
						
						SET_PED_NAME_DEBUG(pedsExtraMotelEntranceGuys[i], "Extra Motel")
						
						SET_PED_COMBAT_RANGE(pedsExtraMotelEntranceGuys[i], CR_NEAR)
						REMOVE_PED_DEFENSIVE_AREA(pedsExtraMotelEntranceGuys[i])
					
						SET_PED_COMBAT_ATTRIBUTES(pedsExtraMotelEntranceGuys[i], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
					
						IF i = 0
							IF NOT IS_PED_INJURED(pedMichael())
								//TASK_COMBAT_HATED_TARGETS_IN_AREA(pedsExtraMotelEntranceGuys[i], GET_ENTITY_COORDS(pedGunman), 4.0)
								SET_PED_COMBAT_ATTRIBUTES(pedsExtraMotelEntranceGuys[i], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
								//SET_PED_RUN_INTO_POSITION_WHILE_SHOOTING_AT_HATED_ENTITIES(pedsExtraMotelEntranceGuys[i], <<-130.7495, 6423.8149, 30.4484>>, GET_ENTITY_COORDS(pedMichael()))
							
								
							
								SET_PED_SPHERE_DEFENSIVE_AREA(pedsExtraMotelEntranceGuys[i],<<-133.6348, 6422.2285, 30.8007>>, 5.0, TRUE)
							
							ENDIF
						ELIF i = 1
							IF NOT IS_PED_INJURED(pedGunman)
								//TASK_COMBAT_HATED_TARGETS_IN_AREA(pedsExtraMotelEntranceGuys[i], GET_ENTITY_COORDS(pedMichael()), 4.0)
								SET_PED_COMBAT_ATTRIBUTES(pedsExtraMotelEntranceGuys[i], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
								//SET_PED_RUN_INTO_POSITION_WHILE_SHOOTING_AT_HATED_ENTITIES(pedsExtraMotelEntranceGuys[i], <<-156.8279, 6450.0859, 30.4237>>, GET_ENTITY_COORDS(pedGunman))
								SET_PED_SPHERE_DEFENSIVE_AREA(pedsExtraMotelEntranceGuys[i],<<-160.0975, 6449.9961, 30.6695>>, 5.0, TRUE)
							ENDIF
						ELIF i = 2
							IF NOT IS_PED_INJURED(pedGunman)
								//TASK_COMBAT_HATED_TARGETS_IN_AREA(pedsExtraMotelEntranceGuys[i], GET_ENTITY_COORDS(pedMichael()), 4.0)
								SET_PED_COMBAT_ATTRIBUTES(pedsExtraMotelEntranceGuys[i], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
								//SET_PED_RUN_INTO_POSITION_WHILE_SHOOTING_AT_HATED_ENTITIES(pedsExtraMotelEntranceGuys[i], <<-188.7772, 6432.8691, 30.5100>>, GET_ENTITY_COORDS(pedGunman))
								SET_PED_SPHERE_DEFENSIVE_AREA(pedsExtraMotelEntranceGuys[i],<<-188.7772, 6432.8691, 30.5100>>, 5.0, TRUE)
							ENDIF
						ENDIF
						
						SET_PED_COMBAT_MOVEMENT(pedsExtraMotelEntranceGuys[i], CM_WILLADVANCE)
						SET_PED_COMBAT_ATTRIBUTES(pedsExtraMotelEntranceGuys[i], CA_CAN_CHARGE, TRUE)
						SET_PED_COMBAT_ATTRIBUTES(pedsExtraMotelEntranceGuys[i], CA_AGGRESSIVE, TRUE)
						
						iExtraGuysSpawnedCounter++
						
						iExtraGuyTimer[i] = GET_GAME_TIMER()
					ENDIF
				ENDFOR
			ELSE
				iExtraMotelGuysStage++
			ENDIF
		BREAK
		
		CASE 1
			SET_PED_AS_NO_LONGER_NEEDED(pedsExtraMotelEntranceGuys[0])
			SET_PED_AS_NO_LONGER_NEEDED(pedsExtraMotelEntranceGuys[1])
			iExtraMotelGuysStage++
		BREAK
		
	ENDSWITCH

ENDPROC

FUNC BOOL HAS_PLAYER_GONE_PAST_MOTEL_YARD()

	RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-158.517746,6429.376465,30.915985>>, <<-177.308258,6411.886719,41.862740>>, 129.000000)

ENDFUNC

BOOL	bMichaelTaskedToGardenCover, bGunmanTaskedToGardenCover
BOOL	bMichaelAtFence1, bMichaelAtFence2
BOOL	bGunmanAtFence1, bGunmanAtFence2
FLOAT	flTimeToWaitAtFence1 = 6.0
FLOAT	flTimeToWaitAtFence2 = 2.0

INT iFenceShootDownTimer
INT iShotFenceTimer
BOOL bGunmanTaskedToShootFence

BOOL bGunmanOutOfSyncedScene = FALSE
BOOL bMichaelOutOfSyncedScene = FALSE

BOOL bMichaelCanContinue = FALSE
BOOL bGunmanCanContinue  = FALSE

INT iObjectiveReminderTimer

StructTimer tmrGardenAlliesController

PROC UPDATE_ALLIES_ADVANCE_THROUGH_GARDENS()

	SPAWN_SOME_GUYS_SO_THE_GUYS_DONT_STAND_MOTIONLESS_BY_THE_MOTEL_ENTRANCE()

	VECTOR vPos
	SWITCH eAllyAdvanceGardenState
		CASE ALLY_GARDEN_STATE_INIT
		
			IF DOES_ENTITY_EXIST(PedMichael()) AND NOT IS_ENTITY_DEAD(PedMichael())
				REMOVE_PED_DEFENSIVE_AREA(PedMichael())
				SET_PED_COMBAT_MOVEMENT(PedMichael(), CM_STATIONARY)
				OPEN_SEQUENCE_TASK(seqHoldOffCopsAtBank)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, vAnimStartPosMichael, <<-152.7720, 6459.5894, 31.0824>>, PEDMOVE_WALK, TRUE, 0.5, 5, TRUE)
					TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, vAnimStartPosMichael, 2.0)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 150.00)
				CLOSE_SEQUENCE_TASK(seqHoldOffCopsAtBank)
				TASK_PERFORM_SEQUENCE(PedMichael(), seqHoldOffCopsAtBank)
				CLEAR_SEQUENCE_TASK(seqHoldOffCopsAtBank)
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedGunman) AND NOT IS_ENTITY_DEAD(pedGunman)
				REMOVE_PED_DEFENSIVE_AREA(pedGunman)
				SET_PED_COMBAT_MOVEMENT(pedGunman, CM_STATIONARY)
				OPEN_SEQUENCE_TASK(seqHoldOffCopsAtBank)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, vAnimStartPosGunman, <<-124.3652, 6432.3018, 32.0577>>, PEDMOVE_WALK, TRUE, 0.5, 5, TRUE)
					TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, vAnimStartPosGunman, 2.0)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 150.00)
				CLOSE_SEQUENCE_TASK(seqHoldOffCopsAtBank)
				TASK_PERFORM_SEQUENCE(pedGunman, seqHoldOffCopsAtBank)
				CLEAR_SEQUENCE_TASK(seqHoldOffCopsAtBank)
			ENDIF
			
			SET_PED_RESET_FLAG(pedGunman, PRF_InstantBlendToAim, TRUE)
			SET_PED_RESET_FLAG(PedMichael(), PRF_InstantBlendToAim, TRUE)
			
			bGunmanTaskedToShootFence = FALSE
			bGunmanOutOfSyncedScene = FALSE
			bMichaelOutOfSyncedScene = FALSE
			iExtraGuysSpawnedCounter = 0
			bMichaelAtFence1 = FALSE
			bMichaelAtFence2 = FALSE
			bGunmanAtFence1 = FALSE
			bGunmanAtFence2 = FALSE
			
			bMichaelCanContinue = FALSE
			bGunmanCanContinue  = FALSE
			
			bMichaelTaskedToGardenCover = FALSE
			bGunmanTaskedToGardenCover = FALSE
			REQUEST_ANIM_DICT("MISSHeistPaletoScore2")
			REQUEST_WAYPOINT_RECORDING("RBHGdnMichael")	
			REQUEST_WAYPOINT_RECORDING("RBHGdnCrew")	
			CANCEL_TIMER(tmrGardenAlliesController)
			CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_ADVANCE_THROUGH_GARDENS - Going to ALLY_GARDEN_WAIT_FOR_STREAMING")
			eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_FOR_STREAMING
		BREAK
		
		CASE ALLY_GARDEN_WAIT_FOR_STREAMING
			IF GET_IS_WAYPOINT_RECORDING_LOADED("RBHGdnMichael")	
			AND GET_IS_WAYPOINT_RECORDING_LOADED("RBHGdnCrew")	
			AND HAS_ANIM_DICT_LOADED("MISSHeistPaletoScore2")
				CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_ADVANCE_THROUGH_GARDENS - Going to ALLY_GARDEN_WAIT_FOR_ALLIES_AT_MOTEL")
				eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_FOR_ALLIES_AT_MOTEL
			ENDIF
		BREAK
		
		//Wait for Michael and the gunman to be at the gate
		CASE ALLY_GARDEN_WAIT_FOR_ALLIES_AT_MOTEL
			IF NOT bPlayedMichaelHurryDialogue
				IF IS_ENTITY_AT_COORD(PedMichael(), vAnimStartPosMichael, <<2.0, 2.0, 2.0>>)
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()	
				
						IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_MOTEL", CONV_PRIORITY_HIGH)
							bPlayedMichaelHurryDialogue = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
						
			// Wait for Michael, and the Gunman at the gate, once both are there, proceed
			IF IS_ENTITY_AT_COORD(PedMichael(), vAnimStartPosMichael, <<1.6, 1.6, 2.0>>)
			AND IS_ENTITY_AT_COORD(pedGunman, vAnimStartPosGunman, <<1.6, 1.6, 2.0>>)
			AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-148.971573,6442.411133,29.938038>>, <<-139.157623,6432.313965,33.179058>>, 17.500000)
				AND (IS_ENTITY_ON_SCREEN(PedMichael())
					OR IS_ENTITY_ON_SCREEN(pedGunman)))
			OR iExtraGuysSpawnedCounter > 7
			OR (iMaidState >= 3
				AND IS_ENTITY_AT_COORD(PedMichael(), vAnimStartPosMichael, <<1.6, 1.6, 2.0>>)
				AND IS_ENTITY_AT_COORD(pedGunman, vAnimStartPosGunman, <<1.6, 1.6, 2.0>>))
//			AND (IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vAnimStartPosGunman, <<6.0, 6.0, 6.0>>)
//			 OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-147.189850,6435.490234,30.415926>>, <<-204.731812,6376.022461,34.771587>>, 40.000000))
//				CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_ADVANCE_THROUGH_GARDENS - Going to ALLY_GARDEN_TASK_MICHAEL_THROUGH_TUNNEL")
				eAllyAdvanceGardenState = ALLY_GARDEN_TASK_MICHAEL_THROUGH_TUNNEL
			ENDIF
		BREAK
		
		CASE ALLY_GARDEN_TASK_MICHAEL_THROUGH_TUNNEL
			/* START SYNCHRONIZED SCENE -  */
			KILL_ANY_CONVERSATION()
			sceneIdWalkThroughTunnel = CREATE_SYNCHRONIZED_SCENE(<< -150.890, 6428.918, 30.916 >>,  << 0.000, 0.000, -42.840 >>)
			IF NOT IS_PED_INJURED(PedMichael())
				REMOVE_PED_DEFENSIVE_AREA(PedMichael())
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedMichael(), TRUE)
				TASK_SYNCHRONIZED_SCENE(PedMichael(), sceneIdWalkThroughTunnel, "missheistpaletoscore2", "holdup_heavyarmour_crew", 1.0, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, SLOW_BLEND_IN)						
				SET_PED_COMBAT_MOVEMENT(PedMichael(), CM_DEFENSIVE)
				RESTART_TIMER_NOW(tmrGardenAlliesController)
				eAdvanceThroughGardenMessageState = MISSION_MESSAGE_10
				CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_ADVANCE_THROUGH_GARDENS - Going to ALLY_GARDEN_TASK_GUNMAN_THROUGH_TUNNEL")
				eAllyAdvanceGardenState = ALLY_GARDEN_TASK_GUNMAN_THROUGH_TUNNEL
			ENDIF
		BREAK
		
		CASE ALLY_GARDEN_TASK_GUNMAN_THROUGH_TUNNEL
			// Have the Gunman play his synch'd scene one second later
			IF GET_TIMER_IN_SECONDS(tmrGardenAlliesController) >= 0.65
				
				/* START SYNCHRONIZED SCENE -  */
				sceneIdWalkThroughTunnelGunman = CREATE_SYNCHRONIZED_SCENE(<< -150.890, 6428.918, 30.916 >>,  << 0.000, 0.000, -42.840 >>)

				IF NOT IS_PED_INJURED(pedGunman)
					REMOVE_PED_DEFENSIVE_AREA(pedGunman)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGunman, TRUE)
					TASK_SYNCHRONIZED_SCENE(pedGunman, sceneIdWalkThroughTunnelGunman, "missheistpaletoscore2", "holdup_heavyarmour_player0", 1.0, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, SLOW_BLEND_IN)
					SET_PED_COMBAT_MOVEMENT(pedGunman, CM_DEFENSIVE)
					CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_ADVANCE_THROUGH_GARDENS - Going to ALLY_GARDEN_WAIT_FOR_BOTH_THROUGH_TUNNEL")
					eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_FOR_BOTH_THROUGH_TUNNEL
				ENDIF
			ENDIF
		BREAK
		
		CASE ALLY_GARDEN_WAIT_FOR_BOTH_THROUGH_TUNNEL
			// Wait for Michael and Gunman to finish their tunnel anims
			WAYPOINT_RECORDING_GET_COORD("RBHGdnMichael", 4, vPos)
			
			IF (IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdWalkThroughTunnel)
				AND GET_SYNCHRONIZED_SCENE_PHASE(sceneIdWalkThroughTunnel) >= 0.75)
			OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PedMichael(), vPos) <= 2.0
			OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), PedMichael()) <= 2.0
			OR IS_PED_RAGDOLL(PedMichael())
			OR PLAYER_PED_ID() = PedMichael()
				// Task Michael to play his recording
						
				IF NOT IS_PED_INJURED(PedMichael())
				AND bMichaelOutOfSyncedScene = FALSE
					bMichaelOutOfSyncedScene = TRUE
					eAdvanceThroughGardenMessageState = MISSION_MESSAGE_11
				ENDIF				
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneIdWalkThroughTunnelGunman)
			AND bGunmanOutOfSyncedScene = FALSE
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdWalkThroughTunnelGunman) >= 0.95 //>= 1.0
				OR (GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedGunman) <= 2.0)
				OR IS_PED_RAGDOLL(pedGunman)
					CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_ADVANCE_THROUGH_GARDENS - Going to ALLY_GARDEN_WAIT_TO_REACH_FENCE1")
					REMOVE_ANIM_DICT("missheistpaletoscore2")
					bGunmanOutOfSyncedScene = TRUE
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(pedGunman)
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedGunman)
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(pedGunman, PEDMOVE_RUN)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(PedMichael())
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(PedMichael(), PEDMOVE_RUN)
				ENDIF
			ENDIF
						
		
			IF bMichaelOutOfSyncedScene = TRUE
				IF GET_DISTANCE_BETWEEN_ENTITIES(PedMichael(), PLAYER_PED_ID()) > 9.0
				AND NOT HAS_PLAYER_GONE_PAST_MOTEL_YARD()
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(PedMichael(), SCRIPT_TASK_PERFORM_SEQUENCE) //SCRIPT_TASK_SHOOT_AT_COORD)
						
						SET_PED_COMBAT_MOVEMENT(PedMichael(), CM_DEFENSIVE)
						OPEN_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_AIM_GUN_AT_COORD(NULL, <<-168.4743, 6433.4292, 31.8151>>, 2000)
						//	TASK_SHOOT_AT_COORD(NULL, <<-168.4743, 6433.4292, 31.8151>>, INFINITE_TASK_TIME, FIRING_TYPE_RANDOM_BURSTS)
							TASK_COMBAT_HATED_TARGETS_IN_AREA(NULL, <<-168.4743, 6433.4292, 31.8151>>, 100.0)
						CLOSE_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
						TASK_PERFORM_SEQUENCE(PedMichael(), seqDontMoveBothAtSameTime)
						CLEAR_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
						
					ENDIF					
					bMichaelCanContinue = FALSE
				ELSE
					//IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedGunman, SCRIPT_TASK_PERFORM_SEQUENCE)
					IF bMichaelCanContinue = FALSE
						SET_PED_COMBAT_MOVEMENT(PedMichael(), CM_DEFENSIVE)
						OPEN_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHGdnMichael", 4, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_START_FROM_CLOSEST_POINT)
						CLOSE_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
						TASK_PERFORM_SEQUENCE(PedMichael(), seqDontMoveBothAtSameTime)
						CLEAR_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
					ENDIF
					bMichaelCanContinue = TRUE
				ENDIF
			ENDIF
			
			
			IF bGunmanOutOfSyncedScene = TRUE
				IF GET_DISTANCE_BETWEEN_ENTITIES(pedGunman, PLAYER_PED_ID()) > 9.0
				AND NOT HAS_PLAYER_GONE_PAST_MOTEL_YARD()
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedGunman, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
						//TASK_SHOOT_AT_COORD(pedGunman, <<-168.4743, 6433.4292, 31.8151>>, INFINITE_TASK_TIME, FIRING_TYPE_RANDOM_BURSTS)
						TASK_COMBAT_HATED_TARGETS_IN_AREA(pedGunman, <<-168.4743, 6433.4292, 31.8151>>, 100.0)
					ENDIF					
					bGunmanCanContinue = FALSE
				ELSE
					// Task Gunman to play his recording
					IF NOT IS_PED_INJURED(pedGunman)
					AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedGunman, SCRIPT_TASK_PERFORM_SEQUENCE)
						SET_PED_COMBAT_MOVEMENT(pedGunman, CM_DEFENSIVE)
						OPEN_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
							TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHGdnCrew", 3,   EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_START_FROM_CLOSEST_POINT)
						CLOSE_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
						TASK_PERFORM_SEQUENCE(pedGunman, seqDontMoveBothAtSameTime)
						CLEAR_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
					ENDIF
					bGunmanCanContinue = TRUE
				ENDIF
			ENDIF
			
			IF bGunmanCanContinue
			AND bMichaelCanContinue
				eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_TO_REACH_FENCE1
			ENDIF
			
		BREAK
		
		CASE ALLY_GARDEN_WAIT_TO_REACH_FENCE1
		CASE ALLY_GARDEN_WAIT_TO_REACH_FENCE1_DIALOGUE			
		CASE ALLY_GARDEN_WAIT_TO_REACH_FENCE1_GOD_TEXT			
			REEVALUATE_POOLSIDE_TARGETS()
			
			//Run as they come out of the tunnel
			IF NOT IS_PED_INJURED(pedGunman)
				IF IS_ENTITY_IN_ANGLED_AREA(pedGunman, <<-158.316071,6429.197266,30.665987>>, <<-152.858398,6423.134277,39.497040>>, 5.000000)
										
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedGunman)
						WAYPOINT_PLAYBACK_OVERRIDE_SPEED(pedGunman, PEDMOVE_RUN)
					ELSE
						SET_PED_DESIRED_MOVE_BLEND_RATIO(pedGunman, PEDMOVE_RUN)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(PedMichael())
				IF IS_ENTITY_IN_ANGLED_AREA(PedMichael(), <<-158.316071,6429.197266,30.665987>>, <<-152.858398,6423.134277,39.497040>>, 5.000000)
								
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
						WAYPOINT_PLAYBACK_OVERRIDE_SPEED(PedMichael(), PEDMOVE_RUN)
					ELSE
						SET_PED_DESIRED_MOVE_BLEND_RATIO(PedMichael(), PEDMOVE_RUN)
					ENDIF
				ENDIF
			ENDIF
						
						
			//Were not going over this with 50 pounds of gear, blast it.
			IF eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_TO_REACH_FENCE1
			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF NOT HAS_CLOSEST_OBJECT_OF_TYPE_BEEN_BROKEN(<<-174.6, 6409.9, 31.1>>, 1.0, PROP_FNCWOOD_01B, SEARCH_LOCATION_EXTERIORS)
					IF CREATE_CONVERSATION(myScriptedSpeech,  "RBH2AUD", "RBH2A_FENCE", CONV_PRIORITY_VERY_HIGH)
						TRIGGER_MUSIC_EVENT("RH2A_FENCE")
						blipForFence = ADD_BLIP_FOR_COORD(<< -175.1908, 6409.6221, 31.6436 >>)
						SET_BLIP_COLOUR(blipForFence, BLIP_COLOUR_GREEN)
						eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_TO_REACH_FENCE1_DIALOGUE	
					ENDIF
				ELSE
					TRIGGER_MUSIC_EVENT("RH2A_FENCE")
					eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_TO_REACH_FENCE1_DIALOGUE	
				ENDIF
			ENDIF
			
			IF eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_TO_REACH_FENCE1_DIALOGUE
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
					PRINT("FENCE2", DEFAULT_GOD_TEXT_TIME + 2000, 1)
					eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_TO_REACH_FENCE1_GOD_TEXT
				ENDIF
			ENDIF
			
			// Once Michael has reached his position by the first fence, wait (CURRENTLY NODE 10 in recording RBHGdnMichael)
			IF NOT bMichaelAtFence1
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedMichael())
					//RUBBER_BAND_WAYPOINTS_AND_SHOOT_WHEN_PAUSED_MICHAEL(TRUE)
					IF GET_PED_WAYPOINT_PROGRESS(pedMichael()) = iCONST_MICHAEL_FENCE1_RECORDING_NODE
						CLEAR_PED_TASKS(pedMichael())
						SET_PED_COMBAT_MOVEMENT(pedMichael(), CM_STATIONARY)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedMichael(), 150.0)
						iFenceShootDownTimer = GET_GAME_TIMER()
						bMichaelAtFence1 = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			// Once Gunman has reached his position by the first fence, wait (CURRENTLY NODE 22 in recording RBHGdnCrew)
			IF NOT bGunmanAtFence1
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedGunman)
					//RUBBER_BAND_WAYPOINTS_AND_SHOOT_WHEN_PAUSED_CREW(TRUE)
					IF GET_PED_WAYPOINT_PROGRESS(pedGunman) = iCONST_GUNMAN_FENCE1_RECORDING_NODE
						CLEAR_PED_TASKS(pedGunman)
						SET_PED_COMBAT_MOVEMENT(pedGunman, CM_STATIONARY)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedGunman, 150.0)
						iFenceShootDownTimer = GET_GAME_TIMER()
						bGunmanAtFence1 = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_GAME_TIMER() - iFenceShootDownTimer > 8000
			AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedMichael()) < 10.0
			AND bGunmanTaskedToShootFence = FALSE
			AND bMichaelAtFence1 = TRUE
			AND bGunmanAtFence1 = TRUE
			OR (bGunmanTaskedToShootFence = FALSE
				AND DOES_ENTITY_EXIST(carsPolicePoolside[0].thisCar)
				AND IS_PED_INJURED(carsPolicePoolside[0].thisDriver)		//All the pool cops dead?
				AND IS_PED_INJURED(carsPolicePoolside[1].thisDriver)
				AND IS_PED_INJURED(carsPolicePoolside[2].thisDriver)
				AND IS_PED_INJURED(carsPolicePoolside[0].thisPassenger)
				AND IS_PED_INJURED(carsPolicePoolside[1].thisPassenger)
				AND IS_PED_INJURED(carsPolicePoolside[2].thisPassenger))
				
				TASK_ALLY_SHOOT_FENCE1(pedGunman)
				iShotFenceTimer = GET_GAME_TIMER()
				bGunmanTaskedToShootFence = TRUE
			ELSE
				//Some hurry up dialogue after you are told to shoot the fence.
				IF GET_GAME_TIMER() - iObjectiveReminderTimer > 5000
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF IS_PLAYER_TREVOR()
							IF CREATE_CONVERSATION(myScriptedSpeech,  "RBH2AUD", "RBH2A_FIRST", CONV_PRIORITY_VERY_HIGH)
								iObjectiveReminderTimer = GET_GAME_TIMER()
							ENDIF	
						ELSE
							IF CREATE_CONVERSATION(myScriptedSpeech,  "RBH2AUD", "RBH_MOVET", CONV_PRIORITY_VERY_HIGH)
								iObjectiveReminderTimer = GET_GAME_TIMER()
							ENDIF	
						ENDIF
					ELSE
						IF IS_PLAYER_TREVOR()
							IF CREATE_CONVERSATION(myScriptedSpeech,  "RBH2AUD", "RBH2A_FIRST", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
								iObjectiveReminderTimer = GET_GAME_TIMER()
							ENDIF	
						ELSE
							IF CREATE_CONVERSATION(myScriptedSpeech,  "RBH2AUD", "RBH_MOVET", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
								iObjectiveReminderTimer = GET_GAME_TIMER()
							ENDIF	
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
			
			// Once both are there, or if the player has shot the fence, move on
			IF (bMichaelAtFence1
				AND bGunmanAtFence1
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedMichael()) < 10.0
				AND bGunmanTaskedToShootFence = TRUE)
			OR (GET_GAME_TIMER() - iShotFenceTimer > 2000 AND HAS_PLAYER_SHOT_THE_FIRST_FENCE())
				
								
				RESTART_TIMER_NOW(tmrGardenAlliesController)
				CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_ADVANCE_THROUGH_GARDENS - Going to ALLY_GARDEN_WAIT_FOR_FENCE1_DESTROYED")
				eAdvanceThroughGardenMessageState = MISSION_MESSAGE_12
				eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_FOR_FENCE1_DESTROYED
			ENDIF
		BREAK
		
		// Wait for the player to destroy the first fence, if he hasn't, then task the guys to shoot the shit out of it
		CASE ALLY_GARDEN_WAIT_FOR_FENCE1_DESTROYED
								
			// Once the Fence has been destroyed, retask Michael and Gunman along their waypoints
			IF HAS_PLAYER_SHOT_THE_FIRST_FENCE()
			
//				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-174.78726, 6410.16797, 31.72257>>) > 3.0
//					ADD_EXPLOSION( <<-174.78726, 6410.16797, 31.72257>>, EXP_TAG_GRENADE, 0.4, FALSE, TRUE, 0.0)
//				ENDIF
				
				// Task Michael to resume his recording
				IF NOT IS_PED_INJURED(PedMichael())
					SET_PED_COMBAT_MOVEMENT(PedMichael(), CM_DEFENSIVE)
					OPEN_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED_TIMED(NULL, 100.0, 1000)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHGdnMichael", iCONST_MICHAEL_FENCE1_RECORDING_NODE, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_START_FROM_CLOSEST_POINT)
					CLOSE_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
					TASK_PERFORM_SEQUENCE(PedMichael(), seqDontMoveBothAtSameTime)
					CLEAR_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
				ENDIF
					
				// Task Gunman to resume his recording
				IF NOT IS_PED_INJURED(pedGunman)
					SET_PED_COMBAT_MOVEMENT(pedGunman, CM_DEFENSIVE)
					OPEN_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_PAUSE(NULL, 250)
						TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHGdnCrew", iCONST_GUNMAN_FENCE1_RECORDING_NODE,  EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_START_FROM_CLOSEST_POINT)
					CLOSE_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
					TASK_PERFORM_SEQUENCE(pedGunman, seqDontMoveBothAtSameTime)
					CLEAR_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
				ENDIF
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBankParkedCars[0])
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBankParkedCars[1])
							
				SET_MODEL_AS_NO_LONGER_NEEDED(WASHINGTON)
				REQUEST_STAGE_2_ASSETS()
				CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_ADVANCE_THROUGH_GARDENS - Going to ALLY_GARDEN_WAIT_TO_REACH_FENCE2")
					
				REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipForFence)
				
				
				IF IS_MESSAGE_BEING_DISPLAYED()				//Only clear the objective
				AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()  //Not a convo...
					CLEAR_PRINTS()
				ENDIF
				
				eAdvanceThroughGardenMessageState = MISSION_MESSAGE_15	//RBH_WRECKED
				
				eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_TO_REACH_FENCE2
			ENDIF
			
			// Player has taken too long, shoot down the fence
			IF TIMER_DO_ONCE_WHEN_READY(tmrGardenAlliesController, flTimeToWaitAtFence1)
				//TASK_ALLY_SHOOT_FENCE1(pedMichael())
				CANCEL_TIMER(tmrGardenAlliesController)
			ENDIF
		BREAK
		
		CASE ALLY_GARDEN_WAIT_TO_REACH_FENCE2
		CASE ALLY_GARDEN_WAIT_TO_REACH_FENCE2_DIALOGUE
		CASE ALLY_GARDEN_WAIT_FOR_FENCE2_DESTROYED_DIALOGUE
			//You were saying?
			IF eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_TO_REACH_FENCE2	
				IF CREATE_CONVERSATION(myScriptedSpeech,  "RBH2AUD", "RBH_GDMT", CONV_PRIORITY_VERY_HIGH)
					TRIGGER_MUSIC_EVENT("RH2A_FENCE")
					eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_TO_REACH_FENCE2_DIALOGUE	
				ENDIF
			ENDIF
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-175.208267,6409.409668,30.612455>>, <<-207.168716,6378.474121,35.099915>>, 34.750000)
			
				IF IS_AUDIO_SCENE_ACTIVE("PS_2A_SHOOT_FENCE_1")
					STOP_AUDIO_SCENE("PS_2A_SHOOT_FENCE_1")
				ENDIF
				IF NOT IS_AUDIO_SCENE_ACTIVE("PS_2A_ENTER_GARDENS")
					START_AUDIO_SCENE("PS_2A_ENTER_GARDENS")
				ENDIF
			ENDIF
		
			REEVALUATE_POOLSIDE_TARGETS()
			// Once Michael has reached his position by the second fence, wait (CURRENTLY NODE 24 in recording RBHGdnMichael)
			IF NOT bMichaelAtFence2
				IF NOT IS_PED_INJURED(pedMichael())
				AND IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedMichael())
				
					WAYPOINT_PLAYBACK_STOP_AIMING_OR_SHOOTING(PedMichael())
				
					RUBBER_BAND_WAYPOINTS_AND_SHOOT_WHEN_PAUSED_MICHAEL(TRUE)
					IF GET_PED_WAYPOINT_PROGRESS(pedMichael()) = iCONST_MICHAEL_FENCE2_RECORDING_NODE
						CLEAR_PED_TASKS(pedMichael())
						SET_PED_COMBAT_MOVEMENT(pedMichael(), CM_STATIONARY)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedMichael(), 150.0)
						bMichaelAtFence2 = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			// Once Gunman has reached his position by the second fence, wait (CURRENTLY NODE 39 in recording RBHGdnCrew)
			IF NOT bGunmanAtFence2
				IF NOT IS_PED_INJURED(pedGunman)
				AND IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedGunman)
				
					WAYPOINT_PLAYBACK_STOP_AIMING_OR_SHOOTING(pedGunman)
				
					RUBBER_BAND_WAYPOINTS_AND_SHOOT_WHEN_PAUSED_CREW(TRUE)
					IF GET_PED_WAYPOINT_PROGRESS(pedGunman) = iCONST_GUNMAN_FENCE2_RECORDING_NODE
						TASK_ALLY_SHOOT_FENCE2(pedGunman)
						bGunmanAtFence2 = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			//Go through the fence!
			IF eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_TO_REACH_FENCE2_DIALOGUE	
			AND (bMichaelAtFence2 OR bGunmanAtFence2)
				IF CREATE_CONVERSATION(myScriptedSpeech,  "RBH2AUD", "RBH2A_SMASHF", CONV_PRIORITY_VERY_HIGH)
					TRIGGER_MUSIC_EVENT("RH2A_FENCE")
					eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_FOR_FENCE2_DESTROYED_DIALOGUE	
				ENDIF
			ENDIF
			
			IF (bMichaelAtFence2 AND bGunmanAtFence2)
			OR HAS_PLAYER_OR_BUDDY_SHOT_SECOND_FENCE()
				RESTART_TIMER_NOW(tmrGardenAlliesController)
				CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_ADVANCE_THROUGH_GARDENS - Going to ALLY_GARDEN_WAIT_FOR_FENCE2_DESTROYED")
				eAdvanceThroughGardenMessageState = MISSION_MESSAGE_12
				eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_FOR_FENCE2_DESTROYED
			ENDIF
		BREAK
		
		// Wait for the player to destroy the second fence, if he hasn't, then task the guys to shoot the shit out of it
		CASE ALLY_GARDEN_WAIT_FOR_FENCE2_DESTROYED
			
			// Once the Fence has been destroyed, retask Michael and Gunman along their waypoints
			IF HAS_PLAYER_OR_BUDDY_SHOT_SECOND_FENCE()
				// Task Michael to resume his recording
				IF NOT IS_PED_INJURED(PedMichael())
					SET_PED_COMBAT_MOVEMENT(PedMichael(), CM_DEFENSIVE)
					OPEN_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHGdnMichael", iCONST_MICHAEL_FENCE2_RECORDING_NODE, EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_START_FROM_CLOSEST_POINT)
						TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, << -184.9843, 6365.8989, 30.4902 >> , 1.5)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, << -184.9843, 6365.8989, 30.4902 >>,  << -159.1230, 6356.5781, 30.4710 >>, PEDMOVE_WALK, FALSE)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.00)
					CLOSE_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
					TASK_PERFORM_SEQUENCE(PedMichael(), seqDontMoveBothAtSameTime)
					CLEAR_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
				ENDIF
					
				// Task Gunman to resume his recording
				IF NOT IS_PED_INJURED(pedGunman)
					SET_PED_COMBAT_MOVEMENT(pedGunman, CM_DEFENSIVE)
					OPEN_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_PAUSE(NULL, 350)
						TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHGdnCrew", iCONST_GUNMAN_FENCE2_RECORDING_NODE,  EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT | EWAYPOINT_NAVMESH_BACK_TO_WAYPOINT_IF_LEFT_ROUTE | EWAYPOINT_START_FROM_CLOSEST_POINT)
						TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, << -181.6442, 6368.0581, 30.4902 >> , 1.5)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, << -181.6442, 6368.0581, 30.4902 >>,  << -181.6744, 6351.2354, 30.4431 >>, PEDMOVE_WALK, FALSE)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 100.00)
					CLOSE_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
					TASK_PERFORM_SEQUENCE(pedGunman, seqDontMoveBothAtSameTime)
					CLEAR_SEQUENCE_TASK(seqDontMoveBothAtSameTime)
				ENDIF
				CLEANUP_SET_PIECE_COP_CAR(setpieceIniitalCops1, TRUE)
				CLEANUP_SET_PIECE_COP_CAR(setpieceIniitalCops2, TRUE)
				CLEANUP_SET_PIECE_COP_CAR(setpieceIniitalCops3, TRUE)
				CLEANUP_SET_PIECE_COP_CAR(setpieceIniitalCops4, TRUE)
				CLEANUP_SET_PIECE_COP_CAR(setpieceIniitalCops5, TRUE)
				CLEANUP_SET_PIECE_COP_CAR(carsPoliceBackup[0], TRUE)
				CLEANUP_SET_PIECE_COP_CAR(carsPoliceBackup[1], TRUE)
				CREATE_CONTINUE_DOWN_STREET_STAGE()
				CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_ADVANCE_THROUGH_GARDENS - Going to ALLY_GARDEN_WAIT_TO_TASK_ALLIES_TO_COVER")
				eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_TO_TASK_ALLIES_TO_COVER
			ENDIF
			
			IF TIMER_DO_ONCE_WHEN_READY(tmrGardenAlliesController, flTimeToWaitAtFence2)
				//TASK_ALLY_SHOOT_FENCE2(pedMichael())
				CANCEL_TIMER(tmrGardenAlliesController)
			ENDIF
			
		BREAK
		
		CASE ALLY_GARDEN_WAIT_TO_TASK_ALLIES_TO_COVER
			IF NOT bMichaelTaskedToGardenCover
				IF NOT IS_PED_INJURED(pedMichael())
				AND IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedMichael())
				//	RUBBER_BAND_WAYPOINTS_AND_SHOOT_WHEN_PAUSED_MICHAEL(TRUE)
					IF GET_PED_WAYPOINT_PROGRESS(pedMichael()) >= 36
					//	cpWallInGarden = ADD_COVER_POINT(<<-188.2885, 6368.1563, 30.8660>>, 229.3080, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_300TO0)
					//	TASK_SEEK_COVER_TO_COVER_POINT(PedMichael(), cpWallInGarden, <<-188.2885, 6368.1563, 30.8660>>, INFINITE_TASK_TIME)
						TASK_ALLY_SHOOT_FENCE3(pedMichael())
						bMichaelTaskedToGardenCover = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT bGunmanTaskedToGardenCover
				IF NOT IS_PED_INJURED(pedGunman)
				AND IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedGunman)
				//	RUBBER_BAND_WAYPOINTS_AND_SHOOT_WHEN_PAUSED_CREW(TRUE)
					IF GET_PED_WAYPOINT_PROGRESS(pedGunman) >= 50
						TASK_ALLY_SHOOT_FENCE3(pedGunman)
						bGunmanTaskedToGardenCover = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF bMichaelTaskedToGardenCover AND bGunmanTaskedToGardenCover
			OR HAS_PLAYER_OR_BUDDY_SHOT_THIRD_FENCE()
				CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_ADVANCE_THROUGH_GARDENS - Going to ALLY_GARDEN_WAIT_FOR_FENCE3_DESTROYED")
				eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_FOR_FENCE3_DESTROYED
			ENDIF
		BREAK
		
		CASE ALLY_GARDEN_WAIT_FOR_FENCE3_DESTROYED
		CASE ALLY_GARDEN_WAIT_FOR_FENCE3_DESTROYED_DIALOGUE
		
			//Go through the fence!
			IF eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_FOR_FENCE3_DESTROYED	
				IF CREATE_CONVERSATION(myScriptedSpeech,  "RBH2AUD", "RBH2A_LSTFNA", CONV_PRIORITY_VERY_HIGH)
					TRIGGER_MUSIC_EVENT("RH2A_FENCE")
					eAllyAdvanceGardenState = ALLY_GARDEN_WAIT_FOR_FENCE3_DESTROYED_DIALOGUE	
				ENDIF
			ENDIF
		
			IF HAS_PLAYER_OR_BUDDY_SHOT_THIRD_FENCE()
				CPRINTLN(DEBUG_MISSION, "UPDATE_ALLIES_ADVANCE_THROUGH_GARDENS - Going to ALLY_GARDEN_IDLE")
				eAllyAdvanceGardenState = ALLY_GARDEN_IDLE
			ENDIF
		BREAK
		
		CASE ALLY_GARDEN_IDLE
			// Do nothing
		BREAK
		
	ENDSWITCH
ENDPROC

//INT iSetPieceCounter
//INT iTimeOfLAstLoudSpeaker

//BOOL bMotelDestroyed = FALSE

PROC MANAGE_POOL_CARS()
	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(carsPolicePoolside[0].thisCar, "RBHPoolCops") >= 95.0
		REMOVE_VEHICLE_RECORDING(9, "RBHPoolCops")
		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(carsPolicePoolside[0])
	ENDIF
	
	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(carsPolicePoolside[1].thisCar, "RBHPoolCops") >= 95.0
		REMOVE_VEHICLE_RECORDING(1, "RBHPoolCops")
		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(carsPolicePoolside[1])
	ENDIF
	
	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(carsPolicePoolside[2].thisCar, "RBHPoolCops") >= 95.0
		REMOVE_VEHICLE_RECORDING(2, "RBHPoolCops")
		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(carsPolicePoolside[2], WEAPONTYPE_ASSAULTRIFLE, WEAPONTYPE_ASSAULTRIFLE)
	ENDIF
	//EXPLODE_VEHICLE_AT_HEALTH_LEVEL(carsPolicePoolside[0].thisCar, iWidgetCopCarExplosionHealth, FALSE, TRUE)
	EXPLODE_VEHICLE_AT_HEALTH_LEVEL(carsPolicePoolside[1].thisCar, iWidgetCopCarExplosionHealth, FALSE, TRUE)
	EXPLODE_VEHICLE_AT_HEALTH_LEVEL(carsPolicePoolside[2].thisCar, iWidgetCopCarExplosionHealth, FALSE, TRUE)
ENDPROC

PROC UPDATE_FLYOVER_HELI()
	SWITCH iFlyoverHeliState
		CASE 0 
			REQUEST_MODEL(POLMAV)
			REQUEST_VEHICLE_RECORDING(2, "RBHFlyover")
			iFlyoverHeliState = 1
		BREAK
		
		CASE 1
			IF HAS_MODEL_LOADED(POLMAV)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "RBHFlyover")
				INIT_SET_PIECE_COP_CAR(visualSetPieceChopperAdvanceToGardens, POLMAV, 2, "RBHFlyover", VS_FRONT_RIGHT, 1500.0, 0.85)
				IF IS_ENTITY_OK(visualSetPieceChopperAdvanceToGardens.thisCar)
					SET_VEHICLE_LIVERY(visualSetPieceChopperAdvanceToGardens.thisCar, 0)
				ENDIF
				iFlyoverHeliState = 2
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_ENTITY_DEAD(visualSetPieceChopperAdvanceToGardens.thisCar)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(visualSetPieceChopperAdvanceToGardens.thisCar)
					CLEANUP_SET_PIECE_COP_CAR(visualSetPieceChopperAdvanceToGardens, TRUE)
					REMOVE_VEHICLE_RECORDING(2, "RBHFlyover")
					iFlyoverHeliState = 3
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
		BREAK
		
	ENDSWITCH
		

ENDPROC

PROC UPDATE_MAID()
	SWITCH iMaidState
		CASE 0 
			REQUEST_MODEL(S_F_M_Maid_01)
			REQUEST_WAYPOINT_RECORDING("RBHMaid")
			REQUEST_ANIM_SET("move_f@scared")
			iMaidState = 1
		BREAK
		
		CASE 1
			IF HAS_MODEL_LOADED(S_F_M_MAID_01)
			AND GET_IS_WAYPOINT_RECORDING_LOADED("RBHMaid")
			AND HAS_ANIM_SET_LOADED("move_f@scared")
				iMaidState = 2
			ENDIF
		BREAK
		
		CASE 2
			IF IS_PLAYER_IN_TRIGGER_BOX(tb_MaidStart)
				//Create Maid
				pedMaid = CREATE_PED(PEDTYPE_MISSION, S_F_M_Maid_01, << -153.7919, 6422.5737, 31.0957 >>, 304.9655)
				SET_PED_MOVEMENT_CLIPSET(pedMaid, "move_f@scared")
				SET_PED_CAN_BE_TARGETTED(pedMaid, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMaid, TRUE)
								
				OPEN_SEQUENCE_TASK(maidSeq)
					TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHMaid", 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
					TASK_COWER(NULL, -1)
				CLOSE_SEQUENCE_TASK(maidSeq)
				TASK_PERFORM_SEQUENCE(pedMaid, maidSeq)
					
				CLEAR_SEQUENCE_TASK(maidSeq)
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 5, pedMaid, "RBHMaid")
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedMaid, "SCREAM_SCARED", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE_FRONTEND)
//				eAdvanceThroughGardenMessageState = MISSION_MESSAGE_06
				iMaidState = 3
			ENDIF
		BREAK
				
		CASE 3
		CASE 4
		
			IF iMaidState = 3
				IF NOT IS_PED_INJURED(pedMaid)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedMaid, "RBH2A_DLAA", "RBHMAID", SPEECH_PARAMS_FORCE_FRONTEND)
					iMaidState = 4
				ENDIF
			ENDIF
		
			IF IS_PED_DOING_TASK(pedMaid, SCRIPT_TASK_COWER)
			OR IS_PED_INJURED(pedMaid)
			OR IS_PED_DEAD_OR_DYING(pedMaid)
			OR IS_ENTITY_AT_COORD(pedMaid, (<<-161.68190, 6450.43506, 30.90704>>), (<<2, 2, 2>>))
				SET_MODEL_AS_NO_LONGER_NEEDED(S_F_M_MAID_01)
				REMOVE_ANIM_SET("move_f@scared")
				SET_PED_AS_NO_LONGER_NEEDED(pedMaid)
				REMOVE_WAYPOINT_RECORDING("RBHMaid")
				iMaidState++
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC UPDATE_POOL_CARS()
	SWITCH iPoolCarState
		CASE 0 
			REQUEST_VEHICLE_RECORDING(9, "RBHPoolCops")
			REQUEST_VEHICLE_RECORDING(1, "RBHPoolCops")
			REQUEST_VEHICLE_RECORDING(2, "RBHPoolCops")
			
			iPoolCarState = 1
		BREAK
		
		CASE 1
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(9, "RBHPoolCops")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "RBHPoolCops")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "RBHPoolCops")
				iPoolCarState = 2
			ENDIF
		BREAK
		
		CASE 2
			IF IS_PLAYER_IN_TRIGGER_BOX(tb_MaidStart)
				INIT_SET_PIECE_COP_CAR(carsPolicePoolside[0], mnCopCarModel, 9, "RBHPoolCops", VS_FRONT_RIGHT, 4000.0, 0.9)
				INIT_SET_PIECE_COP_CAR(carsPolicePoolside[1], mnCopCarModel, 1, "RBHPoolCops", VS_FRONT_RIGHT, 2000.0, 1.0)
				INIT_SET_PIECE_COP_CAR(carsPolicePoolside[2], mnCopCarModel, 2, "RBHPoolCops", VS_FRONT_RIGHT, 0.0, 1.0)
				
				//Give them some body armour so they look a little bad ass.
				SET_PED_COMPONENT_VARIATION(carsPolicePoolside[0].thisDriver, INT_TO_ENUM(PED_COMPONENT,9), 2, 0, 0) //(task)
				SET_PED_COMPONENT_VARIATION(carsPolicePoolside[0].thisPassenger, INT_TO_ENUM(PED_COMPONENT,9), 2, 0, 0) //(task)
				SET_PED_COMPONENT_VARIATION(carsPolicePoolside[1].thisDriver, INT_TO_ENUM(PED_COMPONENT,9), 2, 0, 0) //(task)
				SET_PED_COMPONENT_VARIATION(carsPolicePoolside[1].thisPassenger, INT_TO_ENUM(PED_COMPONENT,9), 2, 0, 0) //(task)
				SET_PED_COMPONENT_VARIATION(carsPolicePoolside[2].thisDriver, INT_TO_ENUM(PED_COMPONENT,9), 2, 0, 0) //(task)
				SET_PED_COMPONENT_VARIATION(carsPolicePoolside[2].thisPassenger, INT_TO_ENUM(PED_COMPONENT,9), 2, 0, 0) //(task)
					
//				eAdvanceThroughGardenMessageState = MISSION_MESSAGE_15
				iPoolCarState = 3
			ENDIF
		BREAK
		
		CASE 3
		CASE 4
		
			IF iPoolCarState = 3
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_SWIMPO", CONV_PRIORITY_HIGH)
					iPoolCarState = 4
				ENDIF
			ENDIF
		
			MANAGE_POOL_CARS()
		BREAK
		
	ENDSWITCH
ENDPROC


PROC UPDATE_BACKUP_CARS()
	INT i 
	
	IF iBackupCarState > 1
		EXPLODE_INCOMING_SETPIECES_IN_ADVANCE_TO_GARDENS()
	ENDIF
	
	SWITCH iBackupCarState
		CASE 0 
			REQUEST_VEHICLE_RECORDING(3, "CBCops")
			REQUEST_VEHICLE_RECORDING(1, "CBSwat")			
			iBackupCarState = 1
		BREAK
		
		CASE 1
			IF  HAS_VEHICLE_RECORDING_BEEN_LOADED(3, "CBCops")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "CBSwat")
				UNBLOCK_ALL_COPS_OUTSIDE_BANK()
				INIT_SET_PIECE_COP_CAR(carsPoliceBackup[0], mnCopCarModel, 3, "CBCops", VS_FRONT_RIGHT, 4500, 1.1)
				INIT_SET_PIECE_COP_CAR(carsPoliceBackup[1], mnCopCarModel, 1, "CBSwat", VS_FRONT_RIGHT, 2500, 1.1)
				SET_VEHICLE_STRONG(carsPoliceBackup[0].thisCar, TRUE)
				SET_VEHICLE_STRONG(carsPoliceBackup[1].thisCar, TRUE)
				iBackupCarState = 2
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_ENTITY_DEAD(carsPoliceBackup[0].thisCar)						
				IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(carsPoliceBackup[0].thisCar, "CBCops") >= 79.0
					eAdvanceThroughGardenMessageState = MISSION_MESSAGE_03
					iBackupCarState = 3
				ENDIF
				
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(carsPoliceBackup[0].thisCar)
					iBackupCarState = 3
				ENDIF
			ELSE 
				iBackupCarState = 3
			ENDIF
		BREAK
		
		CASE 3
			REMOVE_VEHICLE_RECORDING(3, "CBCops")
			STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(carsPoliceBackup[0])
			iBackupCarState = 4
		BREAK
		
		CASE 4
			IF NOT IS_ENTITY_DEAD(carsPoliceBackup[1].thisCar)
				IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(carsPoliceBackup[1].thisCar, "CBSWat") >= 85.0
					iBackupCarState = 5
				ENDIF
				
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(carsPoliceBackup[1].thisCar)
					iBackupCarState = 5
				ENDIF
				
			ELSE
				iBackupCarState = 5
			ENDIF
		BREAK
		
		CASE 5
			REMOVE_VEHICLE_RECORDING(1, "CBSWat")
			STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(carsPoliceBackup[1])
			iBackupCarState = 6
		BREAK
		
		CASE 6
			FOR i = 0 TO COUNT_OF(carsPoliceBackup) - 1
				IF DOES_ENTITY_EXIST(carsPoliceBackup[0].thisCar)
					IF IS_ENTITY_DEAD(carsPoliceBackup[0].thisCar)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(carsPoliceBackup[0].thisCar)
						carsPoliceBackup[0].thisCar = NULL
						IF NOT bTrevorRespondedToCarBlowingUp
							eAdvanceThroughGardenMessageState =	MISSION_MESSAGE_05
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
			
			IF NOT DOES_ENTITY_EXIST(carsPoliceBackup[0].thisCar)
			AND NOT DOES_ENTITY_EXIST(carsPoliceBackup[1].thisCar)
				iBackupCarState = 7
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC



PROC UPDATE_ADVANCE_THROUGH_GARDENS_CONVERSATIONS()
	
	SWITCH eAdvanceThroughGardenMessageState
		CASE MISSION_MESSAGE_01
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_CHPREACT", CONV_PRIORITY_VERY_HIGH)
					eAdvanceThroughGardenMessageState = MISSION_MESSAGE_02
				ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_02
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					PRINT_STRING_IN_STRING("GETTOMO", strCrewMember, DEFAULT_GOD_TEXT_TIME, 1)
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					PRINT_STRING_IN_STRING("GETTOMO2", strCrewMember, DEFAULT_GOD_TEXT_TIME, 1)
				ENDIF
				eAdvanceThroughGardenMessageState = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
		
		CASE MISSION_MESSAGE_03
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				//IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_GCOPS", CONV_PRIORITY_HIGH)
				IF CREATE_GUNMAN_CONVERSATION("RBH_MORE_GM", "RBH_MORE_DJ", "RBH_MORE_CH", "RBH_MORE_NR", "RBH_MORE_PM")
					eAdvanceThroughGardenMessageState = MISSION_MESSAGE_IDLE//MISSION_MESSAGE_04
				ENDIF
			ENDIF
		BREAK
		
//		CASE MISSION_MESSAGE_04
//			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
//				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_MCOPS2", CONV_PRIORITY_HIGH)
//					eAdvanceThroughGardenMessageState = MISSION_MESSAGE_IDLE
//				ENDIF
//			ENDIF
//		BREAK
		
		
		CASE MISSION_MESSAGE_05
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_TCRIES", CONV_PRIORITY_HIGH)
					bTrevorRespondedToCarBlowingUp = TRUE
					eAdvanceThroughGardenMessageState = MISSION_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK
		
//		CASE MISSION_MESSAGE_06
//			//IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()	
//			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
//				//IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_MAID", CONV_PRIORITY_HIGH)
//					IF NOT IS_PED_INJURED(pedMaid)
//						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedMaid, "RBH2A_DLAA", "RBHMAID", SPEECH_PARAMS_FORCE_FRONTEND)
//					ENDIF
//					eAdvanceThroughGardenMessageState = MISSION_MESSAGE_IDLE
//				//ENDIF
//			ENDIF
//		BREAK
		
		
		CASE MISSION_MESSAGE_10
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				//IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_PLANCH", CONV_PRIORITY_HIGH)
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_GET1", CONV_PRIORITY_HIGH)				
					REPLAY_RECORD_BACK_FOR_TIME(1.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
					eAdvanceThroughGardenMessageState = MISSION_MESSAGE_18
				ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_18
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_GUNMAN_CONVERSATION("RBH_WRNG_GM", "RBH_WRNG_DJ", "RBH_WRNG_CH", "RBH_WRNG_NR","RBH_WRNG_PM")
					eAdvanceThroughGardenMessageState = MISSION_MESSAGE_11//MISSION_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK
				
		CASE MISSION_MESSAGE_11
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_GUNMAN_CONVERSATION("RBH_MOVE_GM", "RBH_MOVE_DJ", "RBH_MOVE_CH", "RBH_MOVE_NR", "RBH_MOVE_PM")
					
					eAdvanceThroughGardenMessageState = MISSION_MESSAGE_17
				ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_17
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_MFOLL1", CONV_PRIORITY_HIGH)
					eAdvanceThroughGardenMessageState = MISSION_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK
		
		CASE MISSION_MESSAGE_12
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 7, NULL , "Sherrif1")	
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_LOUDSP2", CONV_PRIORITY_HIGH)
					eAdvanceThroughGardenMessageState = MISSION_MESSAGE_14
				ENDIF
			ENDIF
		BREAK
		
//		CASE MISSION_MESSAGE_13
//			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
//				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_LOUD", CONV_PRIORITY_HIGH)
//					eAdvanceThroughGardenMessageState = MISSION_MESSAGE_14
//				ENDIF	
//			ENDIF				
//		BREAK
		
		CASE MISSION_MESSAGE_14
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_GET2", CONV_PRIORITY_HIGH)
					eAdvanceThroughGardenMessageState = MISSION_MESSAGE_16
				ENDIF	
			ENDIF				
		BREAK
		
				
		CASE MISSION_MESSAGE_15
			//IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_SWIMPO", CONV_PRIORITY_HIGH)
			IF CREATE_GUNMAN_CONVERSATION("RBH_TALK", "RBH_B4DIE", "RBH_TALK_CH", "RBH_B4DIE_NR", "RBH_TALK_PM")
				eAdvanceThroughGardenMessageState = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK

		CASE MISSION_MESSAGE_16
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_WRECKED", CONV_PRIORITY_HIGH)
					eAdvanceThroughGardenMessageState = MISSION_MESSAGE_IDLE
				ENDIF
			ENDIF
		BREAK
		
		
	ENDSWITCH
	
ENDPROC

// TODO: Make sure the player switch works throughout the Advance Through Gardens section.
PROC stageAdvanceToGardens()
	
	IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
		doBuddyFailCheck(0.6)
	ENDIF
	
	MANAGE_GASSTATION_RAYFIRE()
	
	//MANAGE_EXTRA_TYRE_POPS()
	
	MANAGE_EXPLODING_CARS_AT_HOLD_OFF()
	
	PROCESS_MICHAEL_TREVOR_SWITCH()
	
	THROW_BANK_NOTES_IN_AIR_WHEN_SHOT(pedTrevor())
	
	MANAGE_SPAWNING_SWAT_CARS_FOR_DOWN_STREET()
	
	IF iStageAdvanceToGardens > 0
		UPDATE_ALLIES_ADVANCE_THROUGH_GARDENS()
		UPDATE_ADVANCE_THROUGH_GARDENS_CONVERSATIONS()
		UPDATE_POOL_CARS()
		UPDATE_FLYOVER_HELI()
		UPDATE_BACKUP_CARS()
		UPDATE_MAID()
	ENDIF
	
	KEEP_TIME_LOOKING_SWEET()
	
	DISABLE_VEHICLE_EXPLOSION_BREAK_OFF_PARTS()
	
	REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
	
	IF NOT IS_PED_INJURED(PedMichael())
		IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
			iShootingStageMichael = GET_PED_WAYPOINT_PROGRESS(PedMichael())
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedGunman)
		IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedGunman)
			iShootingStageCrewMember = GET_PED_WAYPOINT_PROGRESS(pedGunman)
		ENDIF
	ENDIF
	
	IF TIMERA() > 5500
	AND DOES_ENTITY_EXIST(chopperCops1)
		DELETE_VEHICLE(chopperCops1)
		SET_PED_AS_NO_LONGER_NEEDED(PilotChopperCops1)
		SET_PED_AS_NO_LONGER_NEEDED(pass1ChopperCops1)
		SET_PED_AS_NO_LONGER_NEEDED(pass2ChopperCops1)
		STOP_AUDIO_SCENE("PS_2A_HELI_CRASH")			
		
		//REMOVE_IPL("bnkheist_apt_norm")
		//SET_MOTEL_STATE(BUILDINGSTATE_DESTROYED)
	ENDIF
	
	//IF IS_SYNCHRONIZED_SCENE_RUNNING(iHeliCrashScene)
	//AND bMotelDestroyed = FALSE
	IF iStageAdvanceToGardens > 0
		FLOAT fHeliProgress 
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iHeliCrashScene)
			fHeliProgress = GET_SYNCHRONIZED_SCENE_PHASE(iHeliCrashScene)
		ELSE
			fHeliProgress = 1.5
		ENDIF
		MANAGE_MOTEL_RAYFIRE(fHeliProgress)
	ENDIF
	
	IF DOES_ENTITY_EXIST(chopperCops1)
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2222112
	ENDIF
	
	SWITCH iStageAdvanceToGardens

		CASE 0
					
			IF NOT IS_AUDIO_SCENE_ACTIVE("PS_2A_SHOOTOUT_START")
				START_AUDIO_SCENE("PS_2A_SHOOTOUT_START")
			ENDIF
		
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6, "STAGE_ADVANCE_TO_GARDENS")
			g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = iCurrentTake
			g_iPaletoScoreTake	= iCurrentTake
			eAllyAdvanceGardenState = ALLY_GARDEN_STATE_INIT
			iMaidState = 0 
			iPoolCarState = 0
			iBackupCarState = 0 
			iFlyoverHeliState = 0
			bThirdFenceDestroyed = FALSE
			bHaveSwatVansStopped = FALSE
			bHaveSwatVansBeenTriggered = FALSE
			bPlayedMichaelHurryDialogue = FALSE
			bTrevorRespondedToCarBlowingUp = FALSE
			eAdvanceThroughGardenMessageState = MISSION_MESSAGE_01
			
			REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipChopperCops1)
			SETTIMERA(0)
					
			IF NOT IS_PED_INJURED(pedGunman)	
				SET_PED_COMBAT_MOVEMENT(pedGunman, CM_STATIONARY)
			ENDIF
			IF NOT IS_PED_INJURED(PedMichael())
				SET_PED_COMBAT_MOVEMENT(PedMichael(), CM_STATIONARY)
			ENDIF
			
			DISTANT_COP_CAR_SIRENS(TRUE)
			REMOVE_CUTSCENE()
		
			set_create_random_cops(true)

			SETUP_WANTED_LEVEL(5, TRUE, 60.0, 5.5, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
			
			KILL_ANY_CONVERSATION()
		
			RESET_TYRE_POP_ARRAY()
		
			REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipChopperCops1)
			
			REQUEST_ADVANCE_TO_GARDEN_ASSETS()
						
			//bMotelDestroyed = FALSE
			IF IS_SCREEN_FADED_OUT()
				SET_MOTEL_STATE(BUILDINGSTATE_DESTROYED)
				//bMotelDestroyed = TRUE
				RESET_GAME_CAMERA()	
			ENDIF
			
			IF IS_PLAYER_TREVOR()
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PedMichael(), "MICHAEL")	
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, PLAYER_PED_ID(), "TREVOR")
			ELSE
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PLAYER_PED_ID(), "MICHAEL")	
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, PedMichael(), "TREVOR")
			ENDIF
			IF IS_GUNMAN_GUSTAVO()
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "GUSTAVO")
			ELIF IS_GUNMAN_DARYL()
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "DARYL")
			ELIF IS_GUNMAN_CHEF()
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "CHEF")
			ELIF IS_GUNMAN_NORM()
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "NORM")
			ELIF IS_GUNMAN_PACKIE()
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "PACKIE")
			ENDIF
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, pedFranklin(), "FRANKLIN")
						
			FADE_IN_IF_NEEDED()
			
			SETTIMERA(0)
			
			TRIGGER_MUSIC_EVENT("RH2A_POST_HELI_CRASH_MA")
			
			TASK_LOOK_AT_COORD(pedGunman, <<-135.0391, 6421.1465, 39.8513>>, 5000)
			TASK_LOOK_AT_COORD(pedMichael(), <<-135.0391, 6421.1465, 39.8513>>, 3000)
			TASK_LOOK_AT_COORD(PLAYER_PED_ID(), <<-135.0391, 6421.1465, 39.8513>>, 4000)
			
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							
			SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-139.0459, 6421.6182, 40.8127>>))
			CREATE_ADVANCE_THROUGH_GARDENS_TRIGGERS()
//			CREATE_SOME_COPS_FOR_GUYS_TO_KILL_IN_ADVANCE_TO_GARDENS()
			KILL_OFF_COPS_STAGE1()
			SET_CUTSCENE_RUNNING(FALSE)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(10.0)
			
			REMOVE_ANIM_DICT("missheistpaletoscore2chopper_crash")
			REMOVE_VEHICLE_RECORDING(3, "ChopBank")
			
			iStageAdvanceToGardens++
		BREAK
		
		CASE 1
			IF REQUEST_ADVANCE_TO_GARDEN_ASSETS()
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(PedMichael(), FALSE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedGunman, FALSE)
				TRIGGER_MUSIC_EVENT("RH2A_MOVE_AWAY_MA")
				iStageAdvanceToGardens++
			ENDIF
		BREAK
		
		CASE 2
			IF TIMERA() > 1000
			AND (TIMERA() > 2000 OR IS_PLAYER_PUSHING_ANALOGUE_STICKS())
				iStageAdvanceToGardens = 16
			ENDIF
		BREAK
			
		CASE 16
			cleanUpHeistCutsceneAndTanker()
			REQUEST_PTFX_ASSET()
			iStageAdvanceToGardens++
		BREAK
		
		CASE 17
				
			IF NOT IS_PED_INJURED(pedGunman)
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedGunman)
					ipedGunmanProgressStage1 = GET_PED_WAYPOINT_PROGRESS(pedGunman)		
				ELSE
					ipedGunmanProgressStage1 = 30 //Fail safe
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(PedMichael())
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
					iPedMichaelProgressStage1 = GET_PED_WAYPOINT_PROGRESS(PedMichael())
				ELSE
					iPedMichaelProgressStage1 = 25 //Fail safe
				ENDIF
			ENDIF
			
			IF ipedGunmanProgressStage1 >= 30
			AND iPedMichaelProgressStage1 >= 24
			
				#IF IS_DEBUG_BUILD
					IF bIsSuperDebugEnabled
						DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.1, "STRING", "Waiting for timer/4 dead cops")
					ENDIF
				#ENDIF
							
				ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "RBH2AUD", "RBH2A_MFOLL1", CONV_PRIORITY_HIGH)
				
				//vAnimStartPosGunman = GET_ANIM_INITIAL_OFFSET_POSITION("missheistpaletoscore2", "holdup_heavyarmour_player0", << -150.890, 6428.918, 30.916 >>,  << 0.000, 0.000, -42.840 >>)
				//vAnimStartPosMichael  = GET_ANIM_INITIAL_OFFSET_POSITION("missheistpaletoscore2", "holdup_heavyarmour_crew", << -150.890, 6428.918, 30.916 >>,  << 0.000, 0.000, -42.840 >>)
				
				ipedGunmanProgressStage1Prev = 0
				ipedGunmanProgressStage1 = 0
				iPedMichaelProgressStage1Prev = 0
				iPedMichaelProgressStage1 = 0
				REMOVE_SET_PIECE_BLIPS(carsPoliceBackup[0])
				REMOVE_SET_PIECE_BLIPS(carsPoliceBackup[1])
				
				//CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_MCRIES", CONV_PRIORITY_HIGH)
				
				iStageAdvanceToGardens++
			ENDIF
		BREAK
			
		CASE 18
			CONTROL_WAR_CRIES_DIALOGUE(FALSE)
			IF TIMERB() > 6000
//				eAdvanceThroughGardenMessageState = MISSION_MESSAGE_11
				iStageAdvanceToGardens++
			ENDIF
		BREAK
		
		CASE 19
			iStageAdvanceToGardens++
		BREAK
		
		CASE 20
			IF IS_PLAYER_IN_TRIGGER_BOX(tb_MotelTunnel)
					
				STOP_AUDIO_SCENE("PS_2A_SHOOTOUT_START")
				
				START_AUDIO_SCENE("PS_2A_SHOOT_FENCE_1")

				REMOVE_IPL("gasparticle_grp2")
				
				//Start the two new cop cars
				
				//Delete any stray entites that we dont need (and aren't in sight)
				CLEAR_AREA(vMotelCopSpawnPoint[0], 15.0, TRUE)
				CLEAR_AREA(vMotelCopSpawnPoint[1], 10.0, TRUE)
				
				REQUEST_STAGE_2_ASSETS()
				iStageAdvanceToGardens = 21
			ENDIF
		BREAK
		
		CASE 21
		CASE 22
		
			//Halfway through the tunnel
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-152.163589,6427.663574,30.820978>>, <<-154.940170,6430.227539,35.011360>>, 2.500000)
			OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-156.090576,6426.636719,30.165884>>, <<-207.853592,6373.587402,40.526039>>, 45.000000)
				KILL_ANY_CONVERSATION()
				
				SET_PLAYER_SPRINT(PLAYER_ID(), TRUE)
				
				
				
				iStageAdvanceToGardens = 25
			ENDIF
		BREAK
				
		CASE 25
			IF REQUEST_STAGE_2_ASSETS()
				iStageAdvanceToGardens++
			ENDIF
		BREAK
				
	ENDSWITCH


	IF bThirdFenceDestroyed
	AND iShootingStageCrewMember >= 49
		ADD_EXPLOSION( <<-184.53, 6371.0, 31.87>>, EXP_TAG_GRENADE, 0.4, FALSE, TRUE, 0.0)
		CLEANUP_SET_PIECE_COP_CAR(visualSetPieceChopperAdvanceToGardens, TRUE)
		CLEANUP_SET_PIECE_COP_CAR(carsPolicePoolside[0], TRUE)
		CLEANUP_SET_PIECE_COP_CAR(carsPolicePoolside[1], TRUE)
		CLEANUP_SET_PIECE_COP_CAR(carsPolicePoolside[2], TRUE)
		CLEANUP_SET_PIECE_COP_CAR(carsPoliceBackup[0], TRUE)
		CLEANUP_SET_PIECE_COP_CAR(carsPoliceBackup[1], TRUE)
		SET_PED_AS_NO_LONGER_NEEDED(pedMaid)
		REMOVE_VEHICLE_RECORDING(2, "RBHFlyover")
		REMOVE_VEHICLE_RECORDING(9, "RBHPoolCops")
		REMOVE_VEHICLE_RECORDING(1, "RBHPoolCops")
		REMOVE_VEHICLE_RECORDING(2, "RBHPoolCops")
		REMOVE_VEHICLE_RECORDING(3, "CBCops")
		REMOVE_VEHICLE_RECORDING(1, "CBSwat")
		SET_MODEL_AS_NO_LONGER_NEEDED(S_F_M_Maid_01)
		REMOVE_ANIM_SET("move_f@scared")
		REMOVE_WAYPOINT_RECORDING("RBHMaid")
		
		IF IS_AUDIO_SCENE_ACTIVE("PS_2A_ENTER_GARDENS")
			STOP_AUDIO_SCENE("PS_2A_ENTER_GARDENS")
		ENDIF
		DELETE_VEHICLE(vehBankParkedCars[0])
		DELETE_VEHICLE(vehBankParkedCars[1])
		SET_MODEL_AS_NO_LONGER_NEEDED(WASHINGTON)
		SETUP_STAGE_2()
		
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 7, NULL , "Sherrif1")		
		CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_LOUDSP2", CONV_PRIORITY_VERY_HIGH)
		
		mission_stage = STAGE_CONTINUE_DOWN_STREET
	ENDIF
			
ENDPROC

