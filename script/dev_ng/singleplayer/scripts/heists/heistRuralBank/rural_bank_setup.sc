//	*********************************************************************************************
//	*********************************************************************************************
//	*********************************************************************************************
//
//		MISSION NAME	:	rural_bank_setup.sc
//		AUTHOR			:	Paul Davies
//		DESCRIPTION		:	Michael(player), Trevor and Lester go to scope out the Paleto bank
//
//	*********************************************************************************************
//	*********************************************************************************************
//	*********************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "script_blips.sch"
USING "select_mission_stage.sch"
USING "player_ped_public.sch"
USING "dialogue_public.sch"
USING "chase_hint_cam.sch"                           
USING "commands_interiors.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "cutscene_public.sch"
USING "selector_public.sch"
USING "clearmissionarea.sch"
USING "CompletionPercentage_public.sch"
USING "taxi_functions.sch"
USING "mission_stat_public.sch"
USING "building_control_public.sch"
USING "timeLapse.sch"
USING "selector_public.sch"
USING "locates_public.sch"
USING "buddy_head_track_public.sch"
USING "commands_event.sch"
USING "commands_audio.sch"
USING "commands_recording.sch"

// ==============================================================================================
//		VARIABLES AND INDECES
// ==============================================================================================

CONST_INT RESPONSE_TIME 55000
CONST_INT SIREN_DISTANCE 150

CONST_FLOAT MIN_DIST_SWITCH_TO_AI_ON_ROAD 100.0
CONST_FLOAT MIN_DIST_SWITCH_TO_AI_OFF_ROAD 50.0

ENUM MISSION_STAGE_FLAG
	stage_intro_scene = 0,
	stage_drive_to_bus,
	stage_bus_stop_scene,
	stage_drive_to_bank,
	stage_box_reveal_scene,
	stage_shoot_box,
	stage_hide,
	stage_race_home,
	stage_lose_the_cops,
	stage_buy_gun,
	stage_security_fail,
	stage_hide_fail,
	stage_cap,
	stage_test
ENDENUM
MISSION_STAGE_FLAG current_mission_stage = stage_intro_scene
MISSION_STAGE_FLAG stage_save

ENUM HIDE_FAILS
	PSHF_NO_FAIL = 0,
	PSHF_SPOTTED_BY_BANKERS,
	PSHF_SPOTTED_BY_COPS,
	PSHF_CAR_SPOTTED_BY_COPS,
	PSHF_ASSAULTED_BIKER,
	PSHF_WANTED_LEVEL,
	PSHF_DICKING_AROUND
ENDENUM
HIDE_FAILS eReasonForFail = PSHF_NO_FAIL

#IF IS_DEBUG_BUILD
	CONST_INT NUM_OF_STAGES 9
	INT iStageHolder
	MissionStageMenuTextStruct skipMenuOptions[NUM_OF_STAGES]
#ENDIF

PED_INDEX pedLester
PED_INDEX bank_peds[4]
PED_INDEX pedBusDriver

BLIP_INDEX blipDriveTo
BLIP_INDEX blipVehicle
BLIP_INDEX blipMichael
BLIP_INDEX blipLester
BLIP_INDEX blipAlarm
BLIP_INDEX blipBuddy

OBJECT_INDEX objDummy
OBJECT_INDEX objAlarmHolder
OBJECT_INDEX objMethLabDoorGrab

VEHICLE_INDEX vehPlayerCar
VEHICLE_INDEX vehBus

CAMERA_INDEX camFinalFrame
CAMERA_INDEX camLapse

structTimelapse sTimelapse

SEQUENCE_INDEX seqMike  //, seqTrev

PTFX_ID ptfxMethlabDust

ENUM PS1_SCENARIO_BLOCKING_AREAS
	PS1SBA_TUNNEL = 0,
	PS1SBA_BANK,
	PS1SBA_LOT,
	PS1SBA_BUS,
	PS1SBA_GAS,
	PS1SBA_METHLAB,
	PS1SBA_NUMBER_OF_AREAS
ENDENUM

SCENARIO_BLOCKING_INDEX sbiArray[PS1SBA_NUMBER_OF_AREAS]
INT iBlockingFlags

REL_GROUP_HASH the_cops
REL_GROUP_HASH bank_peds_group

//SCENARIO_BLOCKING_INDEX sbiBank
SCENARIO_BLOCKING_INDEX sbiMethlab
//SCENARIO_BLOCKING_INDEX sbiBusStop

CHASE_HINT_CAM_STRUCT localChaseHintCamStruct

//-------------------------------------- VARS

VECTOR v_car_start = << 1397.8824, 3594.6257, 33.9272 >>
FLOAT f_car_start = 199.4271

VECTOR v_park_shoot = <<-132.282562,6498.743652,28.6281>>
FLOAT f_park_shoot = -102.165855

VECTOR v_blip_park_shoot = <<-124.71, 6504.69, 28.63>>

VECTOR v_park_hide = <<-88.9369, 6418.1641, 30.4645>>
FLOAT f_park_hide = 318.1260

VECTOR v_busstop_car_park = <<-348.9985, 6194.5923, 30.7398>>
FLOAT f_busstop_car_park = 134.3799

VECTOR v_player_start = <<1393.0525, 3613.7629, 37.9419>>
FLOAT f_player_start = 88.1351

VECTOR v_stand_shoot = << -131.6681, 6500.6992, 28.6280 >>
FLOAT f_stand_shoot = 214.1093

VECTOR v_trevor_start = <<1396.4149, 3610.1460, 37.9419>>
FLOAT f_trevor_start = 23.6499

VECTOR vBikerStartCoords = << -144.3017, 6359.2607, 30.4905 >>
FLOAT fBikerStartHeading = 29.4514

VECTOR scenePosition = <<-113.86, 6463.06, 34.10>>
VECTOR sceneRotation = << -0.000, 0.000, -45 >>

VECTOR v_park_lot = <<-131.08363, 6445.02783, 30.45136>>

VECTOR v_bank_door = <<-111.19, 6462.28, 32.08>>
VECTOR v_bank_interior = << -105.6964, 6467.5796, 31.6343 >>
VECTOR v_bank_alarm_pos = <<-110.375862,6482.126953,33.938354 -0.23>>
VECTOR v_bank_alarm_rot = <<0.00, 0.00, -135.00>>
VECTOR vTargetFocus
VECTOR vCurrentFocus
VECTOR vBankSceneStartPos[4], vBankSceneStartRot[4]

VECTOR vReplayCoords
FLOAT fReplayHeading

VECTOR vGPSDriveToPaleto[3]
INT iPaletoGPSPoint = 0

VECTOR vOffroadMountains[6]

BOOL bLookingAtHotGirl = FALSE
BOOL bTalkingAboutGirl = FALSE
BOOL bCopsArrived = FALSE
BOOL b_STAGE_READY
BOOL bSkip = FALSE
BOOL bCutsceneLoaded = FALSE
BOOL bCutscenePreStreamed = FALSE
BOOL b_security_fail = FALSE
BOOL bRecentGodText = FALSE
BOOL bHoldSpeechForHideInstruction = FALSE
BOOL bParked = FALSE
BOOL bFocusHelpPrinted
BOOL bFocusHelpUsed
BOOL bFocusHelpCleared
BOOL bAlarmSpark = FALSE
BOOL bRouteDone = FALSE
BOOL bSkipAlongInGameSeq = FALSE
BOOL bForcedPlayback = FALSE
BOOL bBikeFrozen = FALSE
BOOL bWantedGPS = FALSE

FLOAT fInterpVal

INT iIntroSceneId = -1
INT iLoopSceneId = -1
INT iOutroSceneId = -1
INT iStageProgress = 0
INT iBankPedsProgress = 0
INT iBikerStage = 0
INT iCopProgress = 0
INT i_main_cops = 1
INT iAlarmTime = 0
INT iReDirectLine = 0
INT iTimeOfLastRedirect = 0
INT iTimeLastDickedAround = 0

INT iCurrentConv = 0
INT iTimeSinceLastReminder = 0
INT iLastShootReminder = 0
INT iTimeOfSecurityFail = 0
INT iTimeInstructedToHide = -1
INT iNumberOfShotsAtAlarm

VECTOR vGasStationPos1 = <<-66.027748,6432.554688,29.627739>>
VECTOR vGasStationPos2 = <<-101.967804,6396.954590,52.851540>> 
FLOAT fGasStationWidth = 26.750000

STRING sBlockId = "RBS1AUD"
STRING s_rec_tag = "RBsetup"
STRING sBanterString

SELECTOR_CAM_STRUCT sCamDetails
SELECTOR_PED_STRUCT sSelectorPeds

//-------------------------------------------------------------------

//------------------------------------------------------------- Misc.rbse

PED_INDEX pedBiker
VEHICLE_INDEX vehTrevorBike

//-------------------------------------------------------------------

PROC FADE_IN(INT iTime = 500)
	IF NOT IS_SCREEN_FADED_IN()
	AND NOT IS_SCREEN_FADING_IN()
		DO_SCREEN_FADE_IN(iTime)
	ENDIF
ENDPROC

PROC FADE_OUT(INT iTime = 500)
	IF NOT IS_SCREEN_FADED_OUT()
	AND NOT IS_SCREEN_FADING_OUT()
		DO_SCREEN_FADE_OUT(iTime)
	ENDIF
ENDPROC

INT iStartLoadTime = -1

PROC DO_PLAYER_WARP_WITH_LOAD(VECTOR paramCoords, FLOAT fHeading = 0.0, BOOL bLoadPedsAndVehs = TRUE)
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		SET_FOCUS_POS_AND_VEL(paramCoords, <<COS(fHeading+90), SIN(fHeading+90), 0>>)
		//Load streaming volume at new warp position.
		
		IF iStartLoadTime = -1
			iStartLoadTime = GET_GAME_TIMER()
		ENDIF
		
		NEW_LOAD_SCENE_START(paramCoords, <<COS(fHeading+90), SIN(fHeading+90), 0>>, 100.0)
		WHILE NOT IS_NEW_LOAD_SCENE_LOADED() AND (GET_GAME_TIMER() - iStartLoadTime) < 20000
			WAIT(0)
		ENDWHILE
		
		iStartLoadTime = -1
		
		CLEAR_FOCUS()
		NEW_LOAD_SCENE_STOP()
		CLEAR_AREA(paramCoords, 500.0, TRUE, FALSE)
		
		IF bLoadPedsAndVehs
			INSTANTLY_FILL_PED_POPULATION()
			INSTANTLY_FILL_VEHICLE_POPULATION()
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_PED_AS_BUDDY(PED_INDEX ped)
	IF DOES_ENTITY_EXIST(ped)
	AND NOT IS_PED_INJURED(ped)
		SET_PED_AS_ENEMY(ped, FALSE)
		SET_PED_CAN_BE_TARGETTED(ped, FALSE)
		SET_PED_CAN_BE_DRAGGED_OUT(ped, FALSE)
		SET_PED_RELATIONSHIP_GROUP_HASH(ped, RELGROUPHASH_PLAYER)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_AGGRESSIVE, FALSE)
		SET_PED_COMBAT_ATTRIBUTES(ped, CA_LEAVE_VEHICLES, FALSE)
		SET_PED_CONFIG_FLAG(ped, PCF_GetOutUndriveableVehicle, FALSE)
		SET_PED_CONFIG_FLAG(ped, PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(ped, FALSE)
		SET_PED_CAN_PLAY_AMBIENT_ANIMS(ped, FALSE)
		SET_PED_KEEP_TASK(ped, TRUE)
	ENDIF
ENDPROC

//-------------------------------------------------------- Recordings

//-------------------------------------------------------------------
structPedsForConversation sSpeech

STRUCT TEXT_HOLDER
	STRING label
	BOOL played	
ENDSTRUCT

TEXT_HOLDER dhPreShoot

TEXT_HOLDER gtDriveToPaleto
TEXT_HOLDER gtDriveToBank
TEXT_HOLDER gtDriveRoundBack
TEXT_HOLDER gtShootBox
TEXT_HOLDER gtHideInStation
TEXT_HOLDER gtLoseCops
TEXT_HOLDER gtGetBackIn
TEXT_HOLDER gtGoBackToTrev
TEXT_HOLDER gtGoBackToBoth
TEXT_HOLDER gtGoBackToLester

FUNC TEXT_HOLDER CREATE_DIALOGUE(STRING text)
	TEXT_HOLDER returnDH
	returnDH.label = text
	returnDH.played = FALSE
	return returnDH
ENDFUNC

//---------------------------------------------------- God text stuff

FUNC TEXT_HOLDER CREATE_GOD_TEXT(STRING text)
	TEXT_HOLDER returnGT
	returnGT.label = text
	returnGT.played = FALSE
	return returnGT
ENDFUNC

PROC PRINT_GOD_TEXT(TEXT_HOLDER &gt)
	IF NOT gt.played
		IF NOT IS_STRING_NULL(gt.label)
			PRINT_NOW(gt.label, DEFAULT_GOD_TEXT_TIME, 1)
			gt.played = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC INIT_TEXT_AND_DIALOGUE()

	gtDriveToPaleto		= CREATE_GOD_TEXT("CBH_GOTOPB")
	gtDriveToBank 		= CREATE_GOD_TEXT("CBH_GOTOBNK")
	gtDriveRoundBack 	= CREATE_GOD_TEXT("CBH_GORDBCK")
	gtShootBox 			= CREATE_GOD_TEXT("CBH_SHTBOX")
	gtHideInStation 	= CREATE_GOD_TEXT("CBH_HIDELOT")
	gtLoseCops 			= CREATE_GOD_TEXT("CBH_LOSECOP")
	gtGetBackIn 		= CREATE_GOD_TEXT("CMN_GENGETBCK")
	gtGoBackToTrev 		= CREATE_GOD_TEXT("CBH_AB1TREV")
	gtGoBackToBoth 		= CREATE_GOD_TEXT("CBH_AB1BOTH")
	gtGoBackToLester	= CREATE_GOD_TEXT("CBH_LLEAVE")
	
	dhPreShoot			= CREATE_DIALOGUE("RBS1_PRESHT")
	
ENDPROC

STRUCT COP_CAR
	PED_INDEX cops[2]
	VEHICLE_INDEX veh
	INT iRecording
	BOOL bCreated
	BOOL bDismissed
	INT iResponseStage
	INT iTimeParked
ENDSTRUCT

COP_CAR cars[4]

FUNC BOOL CREATE_BUS(VECTOR vPos, FLOAT fHead)
	IF NOT DOES_ENTITY_EXIST(vehBus)
		REQUEST_MODEL(BUS)
		IF HAS_MODEL_LOADED(BUS)
			vehBus = CREATE_VEHICLE(bus, vPos, fHead)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehBus)
			SET_VEHICLE_COLOUR_COMBINATION(vehBus, 0)
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(pedBusDriver)
		REQUEST_MODEL(S_M_M_LSMETRO_01)
		IF HAS_MODEL_LOADED(S_M_M_LSMETRO_01)
			pedBusDriver = CREATE_PED_INSIDE_VEHICLE(vehBus, PEDTYPE_MISSION, S_M_M_LSMETRO_01, VS_DRIVER)
			SET_PED_CAN_BE_DRAGGED_OUT(pedBusDriver, FALSE)
			SET_PED_CAN_BE_SHOT_IN_VEHICLE(pedBusDriver, FALSE)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedBusDriver, RELGROUPHASH_PLAYER)
		ENDIF
	ENDIF
	RETURN DOES_ENTITY_EXIST(vehBus) AND DOES_ENTITY_EXIST(pedBusDriver)
ENDFUNC

PROC ABANDON_ALL_SCENES()
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iOutroSceneId)
	OR IS_SYNCHRONIZED_SCENE_RUNNING(iIntroSceneId)
	OR IS_SYNCHRONIZED_SCENE_RUNNING(iLoopSceneId)
		INT iTemp
		REPEAT COUNT_OF(bank_peds) iTemp
			IF NOT IS_PED_INJURED(bank_peds[iTemp])
				CLEAR_PED_TASKS(bank_peds[iTemp])
			ENDIF
		ENDREPEAT
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iOutroSceneId)
			IF NOT IS_PED_INJURED(cars[i_main_cops].cops[0])
				CLEAR_PED_TASKS(cars[i_main_cops].cops[0])
			ENDIF
			IF NOT IS_PED_INJURED(cars[i_main_cops].cops[1])
				CLEAR_PED_TASKS(cars[i_main_cops].cops[1])
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(cars[i_main_cops].veh)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iOutroSceneId)
					STOP_SYNCHRONIZED_ENTITY_ANIM(cars[i_main_cops].veh, NORMAL_BLEND_OUT, TRUE)
				ENDIF
//				STOP_ENTITY_ANIM(cars[1].veh, "Cops_Response_Outro_CAR", "missheistpaletoscoresetup_setup3", NORMAL_BLEND_OUT)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

STRUCT ALARM_BOX
	OBJECT_INDEX obj
	VECTOR pos
	VECTOR rot
	MODEL_NAMES model
	BOOL bInitialised
	BOOL bOn
	BOOL bShot
	BOOL bActive
	BOOL bTarget
	INT iPlayerDamage
	INT iHealthLastFrame
ENDSTRUCT

ALARM_BOX bankAlarm

PROC INIT_ALARM(VECTOR position, VECTOR rotation, BOOL already_shot = FALSE)
	bankAlarm.pos = position
	bankAlarm.rot = rotation
	IF already_shot
		bankAlarm.model = PROP_LD_ALARM_01_DAM
		bAlarmSpark = TRUE
	ELSE
		bankAlarm.model = PROP_LD_ALARM_01
		bAlarmSpark = FALSE
	ENDIF
	bankAlarm.bOn = already_shot
	bankAlarm.bInitialised = TRUE
	bankAlarm.bTarget = FALSE
	bankAlarm.iPlayerDamage = 0
	STOP_ALARM("PALETO_BAY_SCORE_ALARM", TRUE)
ENDPROC

FUNC BOOL HAS_ALARM_BEEN_DAMAGED()

	IF DOES_ENTITY_EXIST(bankAlarm.obj)
		IF GET_ENTITY_HEALTH(bankAlarm.obj) < bankAlarm.iHealthLastFrame
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(bankAlarm.obj, PLAYER_PED_ID(), TRUE)
			OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_GRENADE, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,-1.5>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,1.5>>), 3)
			OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_GRENADELAUNCHER, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,-1.5>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,1.5>>), 3)
			OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_STICKYBOMB, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,-1.5>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,1.5>>), 3)
			OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_MOLOTOV, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,-1.5>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,1.5>>), 3)
			OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_PROGRAMMABLEAR, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,-1.5>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,1.5>>), 3)
				bankAlarm.iPlayerDamage += bankAlarm.iHealthLastFrame - GET_ENTITY_HEALTH(bankAlarm.obj)
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(bankAlarm.obj)
				bankAlarm.iHealthLastFrame = GET_ENTITY_HEALTH(bankAlarm.obj)
			ENDIF
		ENDIF
		bankAlarm.iHealthLastFrame = GET_ENTITY_HEALTH(bankalarm.obj)
	ENDIF
	
	RETURN bankAlarm.iPlayerDamage > 0
	
ENDFUNC

PROC MANAGE_ALARM_BOX()

	IF NOT DOES_ENTITY_EXIST(objAlarmHolder)
	OR NOT DOES_ENTITY_EXIST(bankAlarm.obj)
	
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_bank_alarm_pos) < POW(100,2)
			REQUEST_MODEL(bankAlarm.model)
			REQUEST_PTFX_ASSET()
			IF HAS_MODEL_LOADED(bankAlarm.model)
				IF NOT DOES_ENTITY_EXIST(objAlarmHolder)
					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(v_bank_alarm_pos, 1, PROP_LD_ALARM_01)
						objAlarmHolder = GET_CLOSEST_OBJECT_OF_TYPE(v_bank_alarm_pos, 1, PROP_LD_ALARM_01, TRUE)
						SET_ENTITY_VISIBLE(objAlarmHolder, FALSE)
						SET_ENTITY_COLLISION(objAlarmHolder, FALSE)
					ENDIF
				ELSE
					bankAlarm.obj = CREATE_OBJECT(bankAlarm.model, v_bank_alarm_pos)
					SET_ENTITY_HEALTH(bankAlarm.obj, 50)
					bankAlarm.iHealthLastFrame = 50
					SET_ENTITY_ROTATION(bankAlarm.obj, v_bank_alarm_rot)
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
	
		IF DOES_ENTITY_EXIST(bankAlarm.obj)
		
			IF GET_ENTITY_MODEL(bankAlarm.obj) <> bankAlarm.model
				REQUEST_MODEL(bankAlarm.model)
				IF HAS_MODEL_LOADED(bankAlarm.model)
					DELETE_OBJECT(bankAlarm.obj)
				ENDIF
			ENDIF
			
			//check if the alarm has been shot
			IF NOT bankAlarm.bOn
				IF HAS_ALARM_BEEN_DAMAGED()
					bankAlarm.model = PROP_LD_ALARM_01_DAM
					PLAY_SOUND_FROM_ENTITY(-1, "Shoot_box", bankAlarm.obj, "Paleto_Score_Setup_Sounds")
					PRINTLN("turning alarm on due to damage")
					bankAlarm.bOn = TRUE
				ENDIF
			ELSE
				IF NOT bAlarmSpark
				AND bankAlarm.iPlayerDamage > 0
					REQUEST_PTFX_ASSET()
					IF HAS_PTFX_ASSET_LOADED()
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_alarm_damage_sparks", v_bank_alarm_pos, v_bank_alarm_rot)
						bAlarmSpark = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), bankAlarm.pos) < POW(150,2)
				IF DOES_ENTITY_EXIST(bankAlarm.obj)
					IF GET_ENTITY_MODEL(bankAlarm.obj) <> bankAlarm.model
						REQUEST_MODEL(bankAlarm.model)
						PREPARE_ALARM("PALETO_BAY_SCORE_ALARM")
						IF HAS_MODEL_LOADED(bankAlarm.model)
							DELETE_OBJECT(bankAlarm.obj)
						ENDIF
					ENDIF
				ENDIF
				IF NOT DOES_ENTITY_EXIST(bankAlarm.obj)
					REQUEST_MODEL(bankAlarm.model)
					IF HAS_MODEL_LOADED(bankAlarm.model)
						bankAlarm.obj = CREATE_OBJECT(bankAlarm.model, bankAlarm.pos)
						SET_ENTITY_ROTATION(bankAlarm.obj, bankAlarm.rot)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_bank_alarm_pos) > POW(150,2)
			IF DOES_ENTITY_EXIST(objAlarmHolder)
				IF NOT IS_ENTITY_VISIBLE(objAlarmHolder)
					SET_ENTITY_VISIBLE(objAlarmHolder, TRUE)
				ENDIF
				SET_OBJECT_AS_NO_LONGER_NEEDED(objAlarmHolder)
			ENDIF
			IF DOES_ENTITY_EXIST(bankAlarm.obj)
				DELETE_OBJECT(bankAlarm.obj)
			ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_ALARM_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_ALARM_01_DAM)
			REMOVE_PTFX_ASSET()
		ENDIF
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 sTemp
		IF DOES_ENTITY_EXIST(objAlarmHolder)
			sTemp = "objAlarmHolder exists "
			IF IS_ENTITY_VISIBLE(objAlarmHolder)
				sTemp += " and is visible"
			ENDIF
		ELSE
			sTemp = "objAlarmHolder does not exist"
		ENDIF
		
		DRAW_DEBUG_TEXT_2D(sTemp, <<0.01, 0.6, 0>>)
		
		IF DOES_ENTITY_EXIST(bankAlarm.obj)
			sTemp = "bankAlarm.obj exists "
			IF IS_ENTITY_VISIBLE(bankAlarm.obj)
				sTemp += " and is visible"
			ENDIF
		ELSE
			sTemp = "bankAlarm.obj does not exist"
		ENDIF
		
		DRAW_DEBUG_TEXT_2D(sTemp, <<0.01, 0.62, 0>>)
	#ENDIF
	
	IF bankAlarm.bOn
		IF NOT IS_ALARM_PLAYING("PALETO_BAY_SCORE_ALARM")
			IF PREPARE_ALARM("PALETO_BAY_SCORE_ALARM")
				START_ALARM("PALETO_BAY_SCORE_ALARM", FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bankAlarm.bOn
		IF IS_ALARM_PLAYING("PALETO_BAY_SCORE_ALARM")
			STOP_ALARM("PALETO_BAY_SCORE_ALARM", FALSE)
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL ARE_SCENARIOS_BLOCKED_IN_AREA(PS1_SCENARIO_BLOCKING_AREAS eArea)
	RETURN IS_BIT_SET(iBlockingFlags, ENUM_TO_INT(eArea))
ENDFUNC

PROC BLOCK_SCENARIOS_AT_AREA(PS1_SCENARIO_BLOCKING_AREAS eArea, BOOL bBlock = TRUE)

	IF bBlock
	
		VECTOR vMin, vMax
		
		SWITCH eArea
			CASE PS1SBA_TUNNEL
				vMin = <<-491.5252, 4927.5376, 146.0607>> - <<50,50,20>>
				vMax = <<-491.5252, 4927.5376, 146.0607>> + <<50,50,20>>
			BREAK
			CASE PS1SBA_BANK
				vMin = v_bank_door - <<20,20,10>>
				vMax = v_bank_door + <<20,20,20>>
			BREAK
			CASE PS1SBA_LOT
				vMin = <<-120.007309,6508.671387,28.283587>> - <<19.750000,32.500000,1.750000>>
				vMax = <<-120.007309,6508.671387,28.283587>> + <<19.750000,32.500000,1.750000>>
			BREAK
			CASE PS1SBA_BUS
				vMin = v_busstop_car_park - <<50,50,100>>
				vMax = v_busstop_car_park + <<50,50,100>>
			BREAK
			CASE PS1SBA_GAS
				vMin = v_park_hide - <<35, 35, 15>>
				vMax = v_park_hide + <<35, 35, 15>>
			BREAK
			CASE PS1SBA_METHLAB
				vMin = v_car_start - <<10, 10, 10>>
				vMin = v_car_start + <<10, 10, 10>>
			BREAK	
		ENDSWITCH
		
		IF NOT ARE_SCENARIOS_BLOCKED_IN_AREA(eArea)
			sbiArray[eArea] = ADD_SCENARIO_BLOCKING_AREA(vMin, vMax)
			SET_BIT(iBlockingFlags, ENUM_TO_INT(eArea))
		ENDIF
		
	ELSE
		
		IF ARE_SCENARIOS_BLOCKED_IN_AREA(eArea)
			REMOVE_SCENARIO_BLOCKING_AREA(sbiArray[eArea])
			CLEAR_BIT(iBlockingFlags, ENUM_TO_INT(eArea))
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL IS_POINT_BEHIND_LINE2(VECTOR O, VECTOR p1, VECTOR p2)
	RETURN ((p2.x - p1.x) * (O.y - p1.y) - (p2.y - p1.y) * (O.x - p1.x)) >= 0
ENDFUNC

FUNC BOOL IS_POINT_IN_2D_BOUNDS(VECTOR vPoint, VECTOR &vBounds[])

	IF COUNT_OF(vBounds) < 3
		SCRIPT_ASSERT("Calling IS_POINT_IN_BOUNDS() with a vector array of size < 3")
	ENDIF
	
	INT iTemp
	FOR iTemp = 1 TO COUNT_OF(vBounds) - 1
		IF NOT IS_POINT_BEHIND_LINE2(vPoint, vBounds[iTemp], vBounds[iTemp- 1])
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC


// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Cleanup()
	PRINTLN("@@@@@@@@@@@@@ Mission_Cleanup @@@@@@@@@@@@@@@@")
	IF IS_CUTSCENE_ACTIVE()
		IF IS_CUTSCENE_PLAYING()
			STOP_CUTSCENE()
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_HELMET(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING)
		PRINTLN("Trevor win loaded on cleanup")
	ENDIF
	
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING)
		PRINTLN("Michael win loaded on cleanup")
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(cars[i_main_cops].veh)
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iOutroSceneId)
			STOP_SYNCHRONIZED_ENTITY_ANIM(cars[i_main_cops].veh, 0.8, TRUE)
		ENDIF
//		STOP_ENTITY_ANIM(cars[1].veh, "Cops_Response_Outro_CAR", "missheistpaletoscoresetup_setup3", NORMAL_BLEND_OUT)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objAlarmHolder)
		SET_ENTITY_VISIBLE(objAlarmHolder, TRUE)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objAlarmHolder)
	ENDIF
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	STOP_GAMEPLAY_HINT()
	
	STOP_ALARM("PALETO_BAY_SCORE_ALARM", FALSE)
	
	REMOVE_PTFX_ASSET()
	REMOVE_SCENARIO_BLOCKING_AREAS()
	SET_PED_PATHS_BACK_TO_ORIGINAL(v_car_start + <<50,50,50>>, v_car_start - <<50,50,50>>)
	KILL_ANY_CONVERSATION()
	DISABLE_TAXI_HAILING(FALSE)
		
	REMOVE_SCENARIO_BLOCKING_AREA(sbiMethlab)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-122.75471, 6451.52881, 30.46846>> - <<4, 4, 3>>, <<-122.75471, 6451.52881, 30.46846>> + <<4, 4, 3>>, TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-128.91570, 6501.80225, 28.62807>> - <<6, 6, 3>>, <<-128.91570, 6501.80225, 28.62807>> + <<6, 6, 3>>, TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-94.00424, 6424.18457, 30.47077>> - <<6, 6, 3>>, <<-94.00424, 6424.18457, 30.47077>> + <<6, 6, 3>>, TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-346.8897, 6194.5376, 30.3076>> - <<20, 20, 10>>, <<-94.00424, 6424.18457, 30.47077>> + <<20, 20, 10>>, TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-157.2882, 6460.7593, 30.1739>> - <<5, 5, 3>>, <<-157.2882, 6460.7593, 30.1739>> + <<5, 5, 3>>, TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-118.874969,6448.463379,32.218449>>+<<8.000000,6.250000,2.250000>>, <<-118.874969,6448.463379,32.218449>> - <<8.000000,6.250000,2.250000>>, TRUE)
	
	
	IF IS_MUSIC_ONESHOT_PLAYING()
		CANCEL_MUSIC_EVENT("RH1_START")
	ENDIF
	
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(HAIRDO_SHOP_07_PB, FALSE)
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxMethlabDust)
		STOP_PARTICLE_FX_LOOPED(ptfxMethlabDust)
		REMOVE_PTFX_ASSET()
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		SET_VEHICLE_TYRES_CAN_BURST(vehPlayerCar, TRUE)
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(vehTrevorBike)
		SET_VEHICLE_TYRES_CAN_BURST(vehTrevorBike, TRUE)
	ENDIF
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-73.1887, 6426.1177, 30.4401>> - <<7,7,1>>, <<-73.1887, 6426.1177, 30.4401>> + <<7,7,3>>, TRUE)
	
	CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(1)
	CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
	
	INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL)
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)
	
	RESET_CUSTOM_CHASE_HINT_CAM_INTERP_IN_TIME(localChaseHintCamStruct)
	RESET_CUSTOM_CHASE_HINT_CAM_INTERP_OUT_TIME(localChaseHintCamStruct)
	
	SET_DOOR_STATE(DOORNAME_RURAL_BANK_F_L, DOORSTATE_FORCE_CLOSED_THIS_FRAME)
	SET_DOOR_STATE(DOORNAME_RURAL_BANK_F_R, DOORSTATE_FORCE_CLOSED_THIS_FRAME)
	SET_DOOR_STATE(DOORNAME_RURAL_BANK_F_L, DOORSTATE_LOCKED)
	SET_DOOR_STATE(DOORNAME_RURAL_BANK_F_R, DOORSTATE_LOCKED)
	
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_TRUCK_LOGS", TRUE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_HIKER", TRUE)
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_BICYCLE_MOUNTAIN", TRUE)
	SET_PED_PATHS_IN_AREA(<<-116.522598,6464.545410,40.818439>> +  <<29.750000,29.750000,12.250000>>, <<-116.522598,6464.545410,40.818439>> - <<29.750000,29.750000,12.250000>>, TRUE)
	TERMINATE_THIS_THREAD()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Passed()
	PRINTLN("@@@@@@@@@@@@@@@@@@@@@@ MISSION PASSED @@@@@@@@@@@@@@@@@@@@@@@@@@@@")
	// Ensures the planning board intro cutscene always plays as we pass the mission.
	KILL_ANY_CONVERSATION()
	Mission_Flow_Mission_Passed(TRUE) //Blocks mission passed screen until after planning board.
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_HELMET(PLAYER_PED_ID(), TRUE)
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(vehTrevorBike)
	AND (VDIST2(GET_ENTITY_COORDS(vehTrevorBike), v_player_start) < 50*50 OR GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL)
		PRINTLN("trevor's bike exists")
		IF IS_BIG_VEHICLE(vehTrevorBike)
			PRINTLN("setting veh gen to accomodate big vehicle")	
			SET_MISSION_VEHICLE_GEN_VEHICLE(vehTrevorBike, <<1422.6631, 3614.9785, 33.9476>>, 46.7525)
		ELSE
			PRINTLN("setting veh gen to accomodate normal vehicle")	
			SET_MISSION_VEHICLE_GEN_VEHICLE(vehTrevorBike, <<1405.8481, 3598.6001, 34.4038>>, 49.5670)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(pedLester)
		g_sTriggerSceneAssets.ped[0] = pedLester
	ENDIF
	IF DOES_ENTITY_EXIST(vehPlayerCar)
		g_sTriggerSceneAssets.veh[0] = vehPlayerCar
	ENDIF
	IF DOES_ENTITY_EXIST(vehTrevorBike)
		g_sTriggerSceneAssets.veh[1] = vehTrevorBike
	ENDIF
	Mission_Cleanup()
ENDPROC

FUNC BOOL IS_PLAYER_IN_THE_BANK()
	RETURN 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-116.802216,6472.056152,33.479370>>, 		<<-111.181877,6466.362793,30.634308>>, 7.750000)
	OR 		IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-102.217499,6471.639648,34.327644>>, 		<<-110.666519,6463.193848,30.638554>>, 5.000000)
	OR		IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-99.396065,6461.184082,33.669849>>, 		<<-104.390221,6466.200195,30.634308>>, 2.000000)
ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Failed(STRING reason)
	PRINTLN("@@@@@@@@@@@@@@@@@@@@@@ MISSION FAILED @@@@@@@@@@@@@@@@@@@@@@@@@@@@")
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(reason)
	ABANDON_ALL_SCENES()
	
	KILL_ANY_CONVERSATION()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	
	TRIGGER_MUSIC_EVENT("RH1_FAIL")
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		MANAGE_ALARM_BOX()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off).
		
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
			PRINTLN("PLAYER_SWITCH IS IN PROGRESS")
			IF sSelectorPeds.eNewSelectorPed = SELECTOR_PED_TREVOR
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
			ENDIF
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		PRINTLN("TAKING CONTROL OF SELECTOR PED 2")
		IF sSelectorPeds.eNewSelectorPed = SELECTOR_PED_TREVOR
			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedLester)
		DELETE_PED(pedLester)
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_HELMET(PLAYER_PED_ID(), TRUE)
	ENDIF
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehTrevorBike)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTrevorBike)
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehPlayerCar)	
	AND IS_ENTITY_IN_ANGLED_AREA(vehPlayerCar, <<-112.944084,6461.441895,27.968454>>, <<-98.550766,6476.092773,35.108562>>, 25.000000)
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			SET_ENTITY_COORDS_NO_OFFSET(vehPlayerCar, <<-116.9822, 6453.0942, 30.9129>>)
			SET_ENTITY_QUATERNION(vehPlayerCar, -0.0040, 0.0094, 0.3691, 0.9293)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCar)
		ELSE
			DELETE_VEHICLE(vehPlayerCar)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehTrevorBike)	
	AND IS_ENTITY_IN_ANGLED_AREA(vehTrevorBike, <<-112.944084,6461.441895,27.968454>>, <<-98.550766,6476.092773,35.108562>>, 25.000000)
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		AND IS_VEHICLE_DRIVEABLE(vehTrevorBike)
			SET_ENTITY_COORDS_NO_OFFSET(vehTrevorBike, <<-115.9370, 6455.0142, 30.9471>>)
			SET_ENTITY_QUATERNION(vehTrevorBike, 0.0006, -0.1305, -0.0066, 0.9914)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehTrevorBike)
		ELSE
			DELETE_VEHICLE(vehTrevorBike)
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_THE_BANK()
	OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-137.3856, 6475.4810, 32.4512>>, << 5, 5, 5 >>)
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-117.3143, 6461.5532, 30.4684>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 195.5377)
	ENDIF
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	
	// Remove the planning board priming 
	// NB. The heist controller will load in the planning board 
	// intro cutscene at this point and start it as soon as this
	// mission passes.	
	
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING, FALSE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING, FALSE)
	REMOVE_CUTSCENE()
	
	Mission_Cleanup() // must only take 1 frame and terminate the thread
	
ENDPROC

FUNC VECTOR GET_VECTOR_FROM_DIRECTION_AND_MAGNITUDE(VECTOR v, FLOAT m)
	VECTOR n = NORMALISE_VECTOR(v)
	n.x = n.x * m
	n.y = n.y * m
	n.z = n.z * m
	RETURN n
ENDFUNC

FUNC VECTOR GET_RANDOM_VECTOR_OF_LENGTH(FLOAT length, BOOL bXYOnly = TRUE)
	VECTOR retVec
	retVec.x = GET_RANDOM_FLOAT_IN_RANGE(-1, 1) 
	retVec.y = GET_RANDOM_FLOAT_IN_RANGE(-1, 1)
	IF NOT bXYOnly
		retVec.z = GET_RANDOM_FLOAT_IN_RANGE(-1, 1)
	ELSE
		retVec.z = 0
	ENDIF
	RETURN GET_VECTOR_FROM_DIRECTION_AND_MAGNITUDE(retVec, length)	
ENDFUNC

VECTOR vAmbientPositions[9]
FLOAT fAmbientHeadings[9]
//VECTOR vBankPedGoTo[5]

PROC INIT_AMBIENT_BANK()

	vAmbientPositions[0] = <<-107.2791, 6468.4185, 30.6267>>
	vAmbientPositions[1] = <<-105.5016, 6467.4492, 30.6267>>
	vAmbientPositions[2] = <<-105.3874, 6470.7588, 30.6267>>
	vAmbientPositions[3] = <<-102.1724, 6464.2979, 30.6267>>
//	vAmbientPositions[4] = <<-113.152794,6469.991211,30.6343>>
//	vAmbientPositions[5] = <<-112.481674,6466.497559,30.6343>>
//	vAmbientPositions[6] = <<-112.053658,6469.196289,30.6343>>
//	vAmbientPositions[7] = <<-110.914177,6468.053223,30.6343>>
//	vAmbientPositions[8] = << -113.6511, 6466.5913, 30.6343 >>
	
	fAmbientHeadings[0] = 297.6210
	fAmbientHeadings[1] = 346.4444
	fAmbientHeadings[2] = 142.5701
	fAmbientHeadings[3] = 40.5428
//	fAmbientHeadings[4] = -43.576214
//	fAmbientHeadings[5] = -51.144936
//	fAmbientHeadings[6] = -45.893616
//	fAmbientHeadings[7] = -34.219608
//	fAmbientHeadings[8] = 258.7163
	
//	vBankPedGoTo[0] = << -40.8530, 6516.1890, 30.4908 >>
//	vBankPedGoTo[1] = << -234.8172, 6422.8330, 30.1952 >>
//	vBankPedGoTo[2] = << -227.7736, 6395.7964, 30.4312 >>
//	vBankPedGoTo[3] = << -196.2114, 6362.1782, 30.4924 >>
//	vBankPedGoTo[4] = << -39.5856, 6446.3853, 30.4812 >>

ENDPROC

INT iCurrentBankPed //this is the global iterator for the bank ped array so they can be created over separate frames. SHOULD ALWAYS BE RESET TO -1

FUNC BOOL CREATE_AMBIENT_BANK_PEDS()

	INT iTemp
	
	REQUEST_MODEL(A_M_M_BUSINESS_01)
	REQUEST_MODEL(A_F_Y_BUSINESS_01)
	
	IF HAS_MODEL_LOADED(A_M_M_BUSINESS_01)
	AND HAS_MODEL_LOADED(A_F_Y_BUSINESS_01)
		IF iCurrentBankPed = -1
			iCurrentBankPed = 0
		ENDIF
		IF iCurrentBankPed = 0
		AND NOT DOES_ENTITY_EXIST(bank_peds[0])		
			bank_peds[0] = CREATE_PED(PEDTYPE_CIVMALE, A_M_M_BUSINESS_01, 	vAmbientPositions[0], fAmbientHeadings[0])
			SET_PED_COMPONENT_VARIATION(bank_peds[0], PED_COMP_HEAD, 	0, 2, 0)								   
			SET_PED_COMPONENT_VARIATION(bank_peds[0], PED_COMP_TORSO, 	0, 1, 0)								   
			SET_PED_COMPONENT_VARIATION(bank_peds[0], PED_COMP_LEG, 	0, 1, 0)								   
			SET_PED_COMPONENT_VARIATION(bank_peds[0], PED_COMP_JBIB, 	0, 0, 0)
		ENDIF
		IF iCurrentBankPed = 1
		AND NOT DOES_ENTITY_EXIST(bank_peds[1])
			bank_peds[1] = CREATE_PED(PEDTYPE_CIVFEMALE, A_F_Y_BUSINESS_01, vAmbientPositions[1], fAmbientHeadings[1])
			SET_PED_COMPONENT_VARIATION(bank_peds[1], PED_COMP_HEAD, 	0, 0, 0)								   
			SET_PED_COMPONENT_VARIATION(bank_peds[1], PED_COMP_HAIR, 	1, 0, 0)								   
			SET_PED_COMPONENT_VARIATION(bank_peds[1], PED_COMP_TORSO, 	1, 0, 0)								   
			SET_PED_COMPONENT_VARIATION(bank_peds[1], PED_COMP_LEG, 	0, 1, 0)
		ENDIF
		IF iCurrentBankPed = 2
		AND NOT DOES_ENTITY_EXIST(bank_peds[2])
			bank_peds[2] = CREATE_PED(PEDTYPE_CIVFEMALE, A_F_Y_BUSINESS_01, vAmbientPositions[2], fAmbientHeadings[2])
			SET_PED_COMPONENT_VARIATION(bank_peds[2], PED_COMP_HEAD, 	0, 0, 0)								   
			SET_PED_COMPONENT_VARIATION(bank_peds[2], PED_COMP_HAIR, 	0, 1, 0)								   
			SET_PED_COMPONENT_VARIATION(bank_peds[2], PED_COMP_TORSO, 	1, 0, 0)								   
			SET_PED_COMPONENT_VARIATION(bank_peds[2], PED_COMP_LEG, 	1, 0, 0)
		ENDIF
//		IF iCurrentBankPed = 3
//		AND NOT DOES_ENTITY_EXIST(bank_peds[3])
//			bank_peds[3] = CREATE_PED(PEDTYPE_CIVMALE, A_M_M_BUSINESS_01, 	vAmbientPositions[3], fAmbientHeadings[3])
//			SET_PED_COMPONENT_VARIATION(bank_peds[3], PED_COMP_HEAD, 	0, 1, 0)								   
//			SET_PED_COMPONENT_VARIATION(bank_peds[3], PED_COMP_TORSO, 	0, 1, 0)								   
//			SET_PED_COMPONENT_VARIATION(bank_peds[3], PED_COMP_LEG, 	0, 0, 0)								   
//			SET_PED_COMPONENT_VARIATION(bank_peds[3], PED_COMP_JBIB, 	0, 0, 0)
//		ENDIF
		IF iCurrentBankPed = 3
		AND NOT DOES_ENTITY_EXIST(bank_peds[3])
			bank_peds[3] = CREATE_PED(PEDTYPE_CIVMALE, A_M_M_BUSINESS_01, 	vAmbientPositions[3], fAmbientHeadings[3])
			SET_PED_COMPONENT_VARIATION(bank_peds[3], PED_COMP_HEAD, 	0, 1, 0)								   
			SET_PED_COMPONENT_VARIATION(bank_peds[3], PED_COMP_TORSO, 	0, 1, 0)								   
			SET_PED_COMPONENT_VARIATION(bank_peds[3], PED_COMP_LEG, 	0, 0, 0)								   
			SET_PED_COMPONENT_VARIATION(bank_peds[3], PED_COMP_JBIB, 	0, 0, 0)
			REPEAT COUNT_OF(bank_peds) iTemp
				IF NOT IS_PED_INJURED(bank_peds[iTemp])
					CLEAR_PED_TASKS(bank_peds[iTemp])
					SET_ENTITY_LOAD_COLLISION_FLAG(bank_peds[iTemp], TRUE)
					SET_PED_COMBAT_ATTRIBUTES(bank_peds[iTemp], CA_ALWAYS_FLEE, TRUE)
					SET_PED_HEARING_RANGE(bank_peds[iTemp], 15)
					SET_PED_SEEING_RANGE(bank_peds[iTemp], 15)
					SET_PED_CONFIG_FLAG(bank_peds[iTemp], PCF_RunFromFiresAndExplosions, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(bank_peds[iTemp], TRUE)
					SET_PED_KEEP_TASK(bank_peds[iTemp], TRUE)
				ENDIF
			ENDREPEAT
			iCurrentBankPed = -1
		ELSE
			iCurrentBankPed++
		ENDIF
		
//		IF iCurrentBankPed = 4
//		AND NOT DOES_ENTITY_EXIST(bank_peds[4])
//			bank_peds[4] = CREATE_PED(PEDTYPE_CIVMALE, A_M_M_BUSINESS_01, 	vAmbientPositions[4], fAmbientHeadings[4])
//			SET_PED_COMPONENT_VARIATION(bank_peds[4], PED_COMP_HEAD, 	1, 2, 0)
//			SET_PED_COMPONENT_VARIATION(bank_peds[4], PED_COMP_TORSO, 	0, 1, 0)
//			SET_PED_COMPONENT_VARIATION(bank_peds[4], PED_COMP_LEG, 	0, 1, 0)
//			SET_PED_COMPONENT_VARIATION(bank_peds[4], PED_COMP_JBIB, 	0, 0, 0)
//		ENDIF
//		IF iCurrentBankPed = 5
//		AND NOT DOES_ENTITY_EXIST(bank_peds[5])
//			bank_peds[5] = CREATE_PED(PEDTYPE_CIVMALE, A_M_M_BUSINESS_01,  	vAmbientPositions[5], fAmbientHeadings[5])
//			SET_PED_COMPONENT_VARIATION(bank_peds[5], PED_COMP_HEAD, 	1, 0, 0)
//			SET_PED_COMPONENT_VARIATION(bank_peds[5], PED_COMP_TORSO, 	1, 1, 0)
//			SET_PED_COMPONENT_VARIATION(bank_peds[5], PED_COMP_LEG, 	0, 2, 0)
//			SET_PED_COMPONENT_VARIATION(bank_peds[5], PED_COMP_JBIB, 	0, 1, 0)
//		ENDIF
//		IF iCurrentBankPed = 6
//		AND NOT DOES_ENTITY_EXIST(bank_peds[6])
//			bank_peds[6] = CREATE_PED(PEDTYPE_CIVFEMALE, A_F_Y_BUSINESS_01, vAmbientPositions[6], fAmbientHeadings[6])
//			SET_PED_COMPONENT_VARIATION(bank_peds[6], PED_COMP_HEAD, 	1, 0, 0)
//			SET_PED_COMPONENT_VARIATION(bank_peds[6], PED_COMP_HAIR, 	1, 0, 0)
//			SET_PED_COMPONENT_VARIATION(bank_peds[6], PED_COMP_TORSO, 	0, 2, 0)
//			SET_PED_COMPONENT_VARIATION(bank_peds[6], PED_COMP_LEG, 	0, 0, 0)
//		ENDIF
//		IF iCurrentBankPed = 7
//		AND NOT DOES_ENTITY_EXIST(bank_peds[7])
//			bank_peds[7] = CREATE_PED(PEDTYPE_CIVFEMALE, A_F_Y_BUSINESS_01, vAmbientPositions[7], fAmbientHeadings[7])
//			SET_PED_COMPONENT_VARIATION(bank_peds[7], PED_COMP_HEAD, 	1, 0, 0)
//			SET_PED_COMPONENT_VARIATION(bank_peds[7], PED_COMP_HAIR, 	0, 1, 0)
//			SET_PED_COMPONENT_VARIATION(bank_peds[7], PED_COMP_TORSO, 	1, 1, 0)
//			SET_PED_COMPONENT_VARIATION(bank_peds[7], PED_COMP_LEG, 	0, 2, 0)
//		ENDIF
//		IF iCurrentBankPed = 8
//		AND NOT DOES_ENTITY_EXIST(bank_peds[8])
//			bank_peds[8] = CREATE_PED(PEDTYPE_CIVFEMALE, A_F_Y_BUSINESS_01, vAmbientPositions[8], fAmbientHeadings[8])
//			SET_PED_COMPONENT_VARIATION(bank_peds[8], PED_COMP_HEAD, 	1, 0, 0)
//			SET_PED_COMPONENT_VARIATION(bank_peds[8], PED_COMP_HAIR, 	0, 0, 0)
//			SET_PED_COMPONENT_VARIATION(bank_peds[8], PED_COMP_TORSO, 	0, 1, 0)
//			SET_PED_COMPONENT_VARIATION(bank_peds[8], PED_COMP_LEG, 	1, 0, 0)
//			REPEAT COUNT_OF(bank_peds) iTemp
//				IF NOT IS_PED_INJURED(bank_peds[iTemp])
//					CLEAR_PED_TASKS_IMMEDIATELY(bank_peds[iTemp])
//					SET_ENTITY_LOAD_COLLISION_FLAG(bank_peds[iTemp], TRUE)
//					SET_PED_COMBAT_ATTRIBUTES(bank_peds[iTemp], CA_ALWAYS_FLEE, TRUE)
//					SET_PED_HEARING_RANGE(bank_peds[iTemp], 15)
//					SET_PED_SEEING_RANGE(bank_peds[iTemp], 15)
//					SET_PED_CONFIG_FLAG(bank_peds[iTemp], PCF_RunFromFiresAndExplosions, FALSE)
//					SET_PED_KEEP_TASK(bank_peds[iTemp], TRUE)
//				ENDIF
//			ENDREPEAT
//			iCurrentBankPed = -1
//		ELSE
//			iCurrentBankPed++
//		ENDIF
	ENDIF
	
	BOOL bRet = TRUE
	REPEAT COUNT_OF(bank_peds) iTemp
		IF NOT DOES_ENTITY_EXIST(bank_peds[iTemp])
			bRet = FALSE
		ENDIF
	ENDREPEAT
	
	RETURN bRet
	
ENDFUNC

PROC DELETE_BANK_PEDS()
	INT iTemp
	REPEAT COUNT_OF(bank_peds) iTemp
		IF DOES_ENTITY_EXIST(bank_peds[iTemp])
			IF NOT IS_PED_INJURED(bank_peds[iTemp])
				CLEAR_PED_TASKS_IMMEDIATELY(bank_peds[iTemp])
			ENDIF
			DELETE_PED(bank_peds[iTemp])
		ENDIF
	ENDREPEAT
ENDPROC

PROC RESET_BANK_SCENE()
	INT iTemp
	REPEAT COUNT_OF(bank_peds) iTemp
		IF DOES_ENTITY_EXIST(bank_peds[iTemp])
			DELETE_PED(bank_peds[iTemp])
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL SET_BANK_PEDS_TO_AMBIENT_STATE()
	IF CREATE_AMBIENT_BANK_PEDS()
		IF NOT IS_PED_INJURED(bank_peds[0])
			TASK_START_SCENARIO_IN_PLACE(bank_peds[0], "WORLD_HUMAN_STAND_IMPATIENT")
		ENDIF
		IF NOT IS_PED_INJURED(bank_peds[1])
			TASK_START_SCENARIO_IN_PLACE(bank_peds[1], "WORLD_HUMAN_HANG_OUT_STREET")
		ENDIF
		IF NOT IS_PED_INJURED(bank_peds[2])
			TASK_START_SCENARIO_IN_PLACE(bank_peds[2], "WORLD_HUMAN_STAND_IMPATIENT")
		ENDIF
		IF DOES_ENTITY_EXIST(bank_peds[3])
			DELETE_PED(bank_peds[3])
		ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

INT iLastGroundCheck
INT iAmbientBankStage
OBJECT_INDEX objVaultDoor
OBJECT_INDEX objDoorGrab
TIMEOFDAY bankTime

BOOL bClosed

PROC UPDATE_AMBIENT_BANK()
	SWITCH iAmbientBankStage
		CASE 0
			IF (GET_GAME_TIMER() - iLastGroundCheck) > 1000
				FLOAT fGroundCheck
				IF GET_GROUND_Z_FOR_3D_COORD(v_bank_interior, fGroundCheck)
					iAmbientBankStage++
				ENDIF
				iLastGroundCheck = GET_GAME_TIMER()
			ENDIF
		BREAK
		CASE 1
			IF SET_BANK_PEDS_TO_AMBIENT_STATE()
				SET_DOOR_STATE(DOORNAME_RURAL_BANK_F_L, DOORSTATE_LOCKED)
				SET_DOOR_STATE(DOORNAME_RURAL_BANK_F_R, DOORSTATE_LOCKED)
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L), 0)
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R), 0)
				bClosed = TRUE
				iAmbientBankStage++
			ENDIF
		BREAK
		CASE 2
			IF (GET_GAME_TIMER() - iLastGroundCheck) > 1000
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_bank_interior) > 200*200
					IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L)) != DOORSTATE_LOCKED
						DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L), 0)
						SET_DOOR_STATE(DOORNAME_RURAL_BANK_F_L, DOORSTATE_LOCKED)
					ENDIF
					IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R)) != DOORSTATE_LOCKED
						DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R), 0)
						SET_DOOR_STATE(DOORNAME_RURAL_BANK_F_R, DOORSTATE_LOCKED)
					ENDIF
					RESET_BANK_SCENE()
					iAmbientBankStage = 0
				ELSE
					bankTime = GET_CURRENT_TIMEOFDAY()
					IF bClosed
						IF GET_TIMEOFDAY_HOUR(bankTime) >= 7
						OR GET_TIMEOFDAY_HOUR(bankTime) <= 17
							SET_DOOR_STATE(DOORNAME_RURAL_BANK_F_L, DOORSTATE_UNLOCKED)
							SET_DOOR_STATE(DOORNAME_RURAL_BANK_F_R, DOORSTATE_UNLOCKED)
							bClosed = FALSE
						ENDIF
					ELSE
						IF GET_TIMEOFDAY_HOUR(bankTime) < 7
						OR GET_TIMEOFDAY_HOUR(bankTime) > 17
							IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L)) != DOORSTATE_LOCKED
								DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L), 0)
								SET_DOOR_STATE(DOORNAME_RURAL_BANK_F_L, DOORSTATE_LOCKED)
							ENDIF
							IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R)) != DOORSTATE_LOCKED
								DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R), 0)
								SET_DOOR_STATE(DOORNAME_RURAL_BANK_F_R, DOORSTATE_LOCKED)
							ENDIF
							bClosed = TRUE
						ENDIF
					ENDIF
				ENDIF
				iLastGroundCheck = GET_GAME_TIMER()
			ENDIF
		BREAK
	ENDSWITCH
	        
	IF IS_EXPLOSION_IN_AREA(EXP_TAG_STICKYBOMB, v_bank_door - <<75,75,10>>, v_bank_door + <<75,75,100>>)
	OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADE, v_bank_door - <<75,75,10>>, v_bank_door + <<75,75,100>>)
	OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADELAUNCHER, v_bank_door - <<75,75,10>>, v_bank_door + <<75,75,100>>)
	OR IS_EXPLOSION_IN_AREA(EXP_TAG_PETROL_PUMP, v_bank_door - <<75,75,10>>, v_bank_door + <<75,75,100>>)
	OR IS_EXPLOSION_IN_AREA(EXP_TAG_ROCKET, v_bank_door - <<75,75,10>>, v_bank_door + <<75,75,100>>)
	OR IS_PROJECTILE_TYPE_IN_AREA(v_bank_door - <<75,75,10>>, v_bank_door + <<75,75,100>>, WEAPONTYPE_RPG)
		Mission_Failed("CBH_BANKALERT")
	ENDIF
	
ENDPROC

PROC MANAGE_VAULT_DOOR()
	IF NOT DOES_ENTITY_EXIST(objDoorGrab)
	OR NOT DOES_ENTITY_EXIST(objVaultDoor)
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_bank_interior) < POW(100,2)
			REQUEST_MODEL(V_ILEV_CBANKVAULDOOR01)
			IF HAS_MODEL_LOADED(V_ILEV_CBANKVAULDOOR01)
				IF NOT DOES_ENTITY_EXIST(objDoorGrab)
					IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<-104.6, 6473.44, 31.8>>, 2, V_ILEV_CBANKVAULDOOR01)
						objDoorGrab = GET_CLOSEST_OBJECT_OF_TYPE(<<-104.6, 6473.44, 31.8>>, 2, V_ILEV_CBANKVAULDOOR01, TRUE)
						SET_ENTITY_VISIBLE(objDoorGrab, FALSE)
					ENDIF
				ELSE
					objVaultDoor = CREATE_OBJECT_NO_OFFSET(V_ILEV_CBANKVAULDOOR01, <<-104.6, 6473.44, 31.8>>)
					SET_ENTITY_ROTATION(objVaultDoor, <<-0.00, 0.00, 45.72>>)
					FREEZE_ENTITY_POSITION(objVaultDoor, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_bank_interior) > POW(150,2)
			IF DOES_ENTITY_EXIST(objDoorGrab)
				IF NOT IS_ENTITY_VISIBLE(objDoorGrab)
					SET_ENTITY_VISIBLE(objDoorGrab, TRUE)
				ENDIF
				SET_OBJECT_AS_NO_LONGER_NEEDED(objDoorGrab)
			ENDIF
			IF DOES_ENTITY_EXIST(objVaultDoor)
				DELETE_OBJECT(objVaultDoor)
			ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(V_ILEV_CBANKVAULDOOR01)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL SET_BANK_PEDS_TO_FLEE_BANK()
//	SEQUENCE_INDEX seqTemp
	REQUEST_ANIM_DICT("missheistpaletoscoresetup_setup1")
	IF HAS_ANIM_DICT_LOADED("missheistpaletoscoresetup_setup1")
	
		INT iTemp
		
//		FOR iTemp = 4 TO 8
//			IF NOT IS_PED_INJURED(bank_peds[iTemp])
//				VECTOR vTemp
//				vTemp = << -119.2339, 6454.9365, 30.4069 >> + GET_RANDOM_VECTOR_OF_LENGTH(GET_RANDOM_FLOAT_IN_RANGE(1.0, 5.0))
//				CLEAR_PED_TASKS_IMMEDIATELY(bank_peds[iTemp])
//				OPEN_SEQUENCE_TASK(seqTemp)
//				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vTemp, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
//				TASK_TURN_PED_TO_FACE_COORD(NULL, v_bank_door)
//				TASK_STAND_STILL(NULL, GET_RANDOM_INT_IN_RANGE(5000, 20000))
//				TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vBankPedGoTo[iTemp-4], PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
//				TASK_WANDER_STANDARD(NULL)
//				CLOSE_SEQUENCE_TASK(seqTemp)
//				TASK_PERFORM_SEQUENCE(bank_peds[iTemp], seqTemp)
//				CLEAR_SEQUENCE_TASK(seqTemp)
//				FORCE_PED_AI_AND_ANIMATION_UPDATE(bank_peds[iTemp])
//			ENDIF
//		ENDFOR

		vBankSceneStartPos[0] = GET_ANIM_INITIAL_OFFSET_POSITION("missheistpaletoscoresetup_setup1", "cops_response_intro_bank_manager", scenePosition, sceneRotation)
		vBankSceneStartPos[1] = GET_ANIM_INITIAL_OFFSET_POSITION("missheistpaletoscoresetup_setup1", "cops_response_intro_bank_clerk1", scenePosition, sceneRotation)
		vBankSceneStartPos[2] = GET_ANIM_INITIAL_OFFSET_POSITION("missheistpaletoscoresetup_setup1", "cops_response_intro_bank_clerk2", scenePosition, sceneRotation)
		vBankSceneStartPos[3] = GET_ANIM_INITIAL_OFFSET_POSITION("missheistpaletoscoresetup_setup1", "cops_response_intro_bank_clerk3", scenePosition, sceneRotation)
		vBankSceneStartRot[0] = GET_ANIM_INITIAL_OFFSET_ROTATION("missheistpaletoscoresetup_setup1", "cops_response_intro_bank_manager", scenePosition, sceneRotation)
		vBankSceneStartRot[1] = GET_ANIM_INITIAL_OFFSET_ROTATION("missheistpaletoscoresetup_setup1", "cops_response_intro_bank_clerk1", scenePosition, sceneRotation)
		vBankSceneStartRot[2] = GET_ANIM_INITIAL_OFFSET_ROTATION("missheistpaletoscoresetup_setup1", "cops_response_intro_bank_clerk2", scenePosition, sceneRotation)
		vBankSceneStartRot[3] = GET_ANIM_INITIAL_OFFSET_ROTATION("missheistpaletoscoresetup_setup1", "cops_response_intro_bank_clerk3", scenePosition, sceneRotation)
		FOR iTemp = 0 TO 3
			IF NOT IS_PED_INJURED(bank_peds[iTemp])
				TASK_FOLLOW_NAV_MESH_TO_COORD(bank_peds[iTemp], vBankSceneStartPos[iTemp], PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, vBankSceneStartRot[iTemp].z)
			ENDIF
		ENDFOR
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L), DOORSTATE_UNLOCKED)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R), DOORSTATE_UNLOCKED)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

BOOL bPedsSentInside = FALSE

PROC SET_BANK_PEDS_GO_BACK_INSIDE()
	INT iTemp
	FOR iTemp = 0 TO 3
		IF DOES_ENTITY_EXIST(bank_peds[iTemp])
		AND NOT IS_PED_INJURED(bank_peds[iTemp])
			IF GET_SCRIPT_TASK_STATUS(bank_peds[iTemp], SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
				CLEAR_PED_TASKS(bank_peds[iTemp])
				TASK_FOLLOW_NAV_MESH_TO_COORD(bank_peds[iTemp], << -100.5008, 6461.7300, 30.6343 >>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
			ENDIF
			IF IS_ENTITY_AT_COORD(bank_peds[iTemp], << -100.5008, 6461.7300, 30.6343 >>, <<2,2,3>>)
				DELETE_PED(bank_peds[iTemp])
			ENDIF
		ENDIF
	ENDFOR
	bPedsSentInside = TRUE
	IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L)) != DOORSTATE_LOCKED
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L), 0)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L), DOORSTATE_LOCKED)
	ENDIF
	IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R)) != DOORSTATE_LOCKED
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R), 0)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R), DOORSTATE_LOCKED)
	ENDIF
ENDPROC

FUNC BOOL PLAY_DIALOGUE(TEXT_HOLDER &dialogue)
	IF NOT dialogue.played
		IF CREATE_CONVERSATION(sSpeech, sBlockId, dialogue.label, CONV_PRIORITY_VERY_HIGH)
			dialogue.played = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//-------------------------------------------------------------------

PROC MANAGE_REDIRECT_LINE()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-104.046753,6493.075684,30.418570>>, <<-120.620209,6475.779297,35.107506>>, 19.250000)
	AND (GET_GAME_TIMER() - iTimeOfLastRedirect) > 10000
	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		TEXT_LABEL_63 temp
		temp = "RBS1_WRGWY_"
		temp += iReDirectLine
		PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, sBlockId, "RBS1_WRGWY", temp, CONV_PRIORITY_HIGH)
		iReDirectLine++
		IF iReDirectLine > 1 iReDirectLine = 0 ENDIF
		iTimeOfLastRedirect = GET_GAME_TIMER()
	ENDIF
ENDPROC

//------------------------------------------------------ Police stuff

//-------------------------------------------------------------------

// -----------------------------------------------------------------------------------------------------------
//		COP CAR FUNCTIONS
// -----------------------------------------------------------------------------------------------------------

FUNC COP_CAR CREATE_COP_CAR(VECTOR position, FLOAT heading, INT recording)
	COP_CAR returnCar
	IF HAS_MODEL_LOADED(SHERIFF)
	AND HAS_MODEL_LOADED(S_M_Y_COP_01)
		returnCar.bCreated = TRUE
		returnCar.iRecording = recording
		returnCar.veh = CREATE_VEHICLE(SHERIFF, position, heading)
		returnCar.cops[0] = CREATE_PED_INSIDE_VEHICLE(returnCar.veh, PEDTYPE_COP, S_M_Y_COP_01)
		GIVE_WEAPON_TO_PED(returnCar.cops[0], WEAPONTYPE_PISTOL, INFINITE_AMMO)
		returnCar.cops[1] = CREATE_PED_INSIDE_VEHICLE(returnCar.veh, PEDTYPE_COP, S_M_Y_COP_01, VS_FRONT_RIGHT)
		GIVE_WEAPON_TO_PED(returnCar.cops[1], WEAPONTYPE_PISTOL, INFINITE_AMMO)
		SET_PED_RELATIONSHIP_GROUP_HASH(returnCar.cops[0], the_cops)
		SET_PED_RELATIONSHIP_GROUP_HASH(returnCar.cops[1], the_cops)
		SET_VEHICLE_DISABLE_TOWING(returnCar.veh, TRUE)
		returnCar.iResponseStage = 0
	ENDIF
	RETURN returnCar
ENDFUNC

PROC DELETE_COP_CAR(COP_CAR &copcar)
	IF copcar.bCreated
		INT iTemp = 0
		REPEAT COUNT_OF(copcar.cops) iTemp
			IF DOES_ENTITY_EXIST(copcar.cops[iTemp])
				DELETE_PED(copcar.cops[iTemp])
			ENDIF
		ENDREPEAT
		IF DOES_ENTITY_EXIST(copcar.veh)
			DELETE_VEHICLE(copcar.veh)
		ENDIF
		copcar.bCreated = FALSE
	ENDIF
ENDPROC

FUNC BOOL CREATE_RESPONSE_TEAM(BOOL bEndPos)
	INT iTemp = 0
	REQUEST_MODEL(SHERIFF)
	REQUEST_MODEL(S_M_Y_COP_01)
	REQUEST_VEHICLE_RECORDING(1, s_rec_tag)
	REQUEST_VEHICLE_RECORDING(2, s_rec_tag)
	REQUEST_VEHICLE_RECORDING(3, s_rec_tag)
	REQUEST_VEHICLE_RECORDING(4, s_rec_tag)
	IF HAS_MODEL_LOADED(SHERIFF)
	AND HAS_MODEL_LOADED(S_M_Y_COP_01)
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, s_rec_tag)
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(2, s_rec_tag)
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(3, s_rec_tag)
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(4, s_rec_tag)
		REPEAT COUNT_OF(cars) iTemp
			cars[iTemp] = CREATE_COP_CAR(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iTemp+1, 0.0, s_rec_tag), 0.0, iTemp+1)
			IF NOT IS_PED_INJURED(cars[iTemp].cops[0])
			AND IS_VEHICLE_DRIVEABLE(cars[iTemp].veh)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(cars[iTemp].iRecording, s_rec_tag)
				SET_VEHICLE_ENGINE_ON(cars[iTemp].veh, TRUE, TRUE)
				SET_VEHICLE_SIREN(cars[iTemp].veh, NOT bEndPos)
				START_PLAYBACK_RECORDED_VEHICLE(cars[iTemp].veh, cars[iTemp].iRecording, s_rec_tag)
				IF bEndPos
					SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(cars[iTemp].veh)
				ENDIF
				TEXT_LABEL_23 sTemp = "cop car "
				sTemp += iTemp
				SET_VEHICLE_NAME_DEBUG(cars[iTemp].veh, sTemp)
			ENDIF
		ENDREPEAT
	ENDIF
	BOOL bDone = TRUE
	REPEAT COUNT_OF(cars) iTemp
		IF NOT DOES_ENTITY_EXIST(cars[iTemp].veh)
		OR NOT DOES_ENTITY_EXIST(cars[iTemp].cops[0])
		OR NOT DOES_ENTITY_EXIST(cars[iTemp].cops[1])
			bDOne = FALSE
		ENDIF
	ENDREPEAT
	RETURN bDone
ENDFUNC

PROC FORCE_CLEANUP_RESPONSE_TEAM()
	INT iTemp = 0
	REPEAT COUNT_OF(cars) iTemp
		DELETE_COP_CAR(cars[iTemp])
	ENDREPEAT
ENDPROC

ENUM ROAD_BLOCK_STATE
	CHECK_LOCATION = 0,
	CREATE_BLOCK_1,
	MANAGE_ROAD_BLOCK_1,
	CREATE_BLOCK_2,
	REMOVE_ALL
ENDENUM

ROAD_BLOCK_STATE roadBlockState
VEHICLE_INDEX vehBlockCar[4]
PED_INDEX pedBlock[6]

INT iSpeedRestriction[2]
//INT iTimeOfStopRestriction

PROC CHECK_FOR_ROAD_BLOCKS()
	INT iTemp
	SWITCH roadBlockState
	
		CASE CHECK_LOCATION
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-1054.7477, 5340.3237, -10>>, <<750,750,750>>)
				PRINTLN("ROAD BLOCK DEBUG: load road block 1")
				roadBlockState = CREATE_BLOCK_1
			ENDIF
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<1578.0135, 6427.9937, -10>>, <<750,750,750>>)
				PRINTLN("ROAD BLOCK DEBUG: load road block 2")
				roadBlockState = CREATE_BLOCK_2
			ENDIF
		BREAK
		
		CASE CREATE_BLOCK_1
			REQUEST_MODEL(SHERIFF)
			REQUEST_MODEL(S_M_Y_COP_01)
			IF HAS_MODEL_LOADED(SHERIFF)
			AND HAS_MODEL_LOADED(S_M_Y_COP_01)
				IF NOT DOES_ENTITY_EXIST(vehBlockCar[0])
					vehBlockCar[0] = CREATE_VEHICLE(SHERIFF, <<-990.6421, 5391.6709, 40.4409>>)
					SET_ENTITY_QUATERNION(vehBlockCar[0] , -0.0138, 0.0194, 0.0655, 0.9976)
				ELIF NOT DOES_ENTITY_EXIST(vehBlockCar[1])
					vehBlockCar[1] = CREATE_VEHICLE(SHERIFF, <<-994.8240, 5401.2280, 40.3984>>)
					SET_ENTITY_QUATERNION(vehBlockCar[1] , 0.0221, 0.0008, 0.8693, -0.4938)
				ELIF NOT DOES_ENTITY_EXIST(pedBlock[0])
					pedBlock[0] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01,	<<-992.6790, 5392.6333, 39.8664>>, 324.3472)
				ELIF NOT DOES_ENTITY_EXIST(pedBlock[1])
					pedBlock[1] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01,	<<-994.6719, 5399.0352, 39.8257>>, 218.1417)
				ELIF NOT DOES_ENTITY_EXIST(pedBlock[2])
					pedBlock[2] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01,	<<-996.4119, 5398.1162, 39.9129>>, 270.4941)
				ELIF NOT DOES_ENTITY_EXIST(pedBlock[3])
					pedBlock[3] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01,	<<-995.2369, 5390.1519, 40.0318>>, 346.7168)
					REPEAT COUNT_OF(vehBlockCar) iTemp
						IF IS_VEHICLE_DRIVEABLE(vehBlockCar[iTemp])
							SET_VEHICLE_SIREN(vehBlockCar[iTemp], TRUE)
						ENDIF
					ENDREPEAT
					REPEAT COUNT_OF(pedBlock) iTemp
						IF NOT IS_PED_INJURED(pedBlock[iTemp])
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBlock[iTemp], TRUE)
						ENDIF
					ENDREPEAT
					iSpeedRestriction[0] = ADD_ROAD_NODE_SPEED_ZONE(<<-987.41046, 5400.16553, 39.52042>>, 15, 7, TRUE)
					iSpeedRestriction[1] = ADD_ROAD_NODE_SPEED_ZONE(<<-993.56, 5395.73, 39.85>>, 3, 1, TRUE)
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-990.6421, 5391.6709, 40.4409>>, <<1000,1000,1000>>)
				PRINTLN("ROAD BLOCK DEBUG: left area for road block 1")
				roadBlockState = REMOVE_ALL
			ENDIF
		BREAK
		
		CASE CREATE_BLOCK_2
			REQUEST_MODEL(SHERIFF)
			REQUEST_MODEL(S_M_Y_COP_01)
			IF HAS_MODEL_LOADED(SHERIFF)
			AND HAS_MODEL_LOADED(S_M_Y_COP_01)
				IF NOT DOES_ENTITY_EXIST(vehBlockCar[0])
					vehBlockCar[0] = CREATE_VEHICLE(SHERIFF, <<1578.0135, 6427.9937, 24.4932>>)
					SET_ENTITY_QUATERNION(vehBlockCar[0] , -0.0181, -0.0008, 0.9997, -0.0188)
				ELIF NOT DOES_ENTITY_EXIST(vehBlockCar[1])					
					vehBlockCar[1] = CREATE_VEHICLE(SHERIFF, <<1573.8474, 6423.3301, 24.4071>>)
					SET_ENTITY_QUATERNION(vehBlockCar[1] , -0.0012, -0.0163, -0.3084, 0.9511)
				ELIF NOT DOES_ENTITY_EXIST(vehBlockCar[2])					
					vehBlockCar[2] = CREATE_VEHICLE(SHERIFF, <<1572.3234, 6409.7764, 24.5238>>)
					SET_ENTITY_QUATERNION(vehBlockCar[2] , -0.0023, -0.0158, -0.2387, 0.9710)
				ELIF NOT DOES_ENTITY_EXIST(vehBlockCar[3])
					vehBlockCar[3] = CREATE_VEHICLE(SHERIFF, <<1564.1204, 6406.3750, 24.3283>>)
					SET_ENTITY_QUATERNION(vehBlockCar[3] , 0.0037, -0.0152, -0.5892, 0.8078)
				ELIF NOT DOES_ENTITY_EXIST(pedBlock[0])
					pedBlock[0] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01,	<<1577.8823, 6424.1489, 23.9170>>, 263.6635)
				ELIF NOT DOES_ENTITY_EXIST(pedBlock[1])				   
					pedBlock[1] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01,	<<1581.4651, 6429.4956, 23.9937>>, 221.5831)
				ELIF NOT DOES_ENTITY_EXIST(pedBlock[2])				   
					pedBlock[2] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01,	<<1580.0614, 6421.4756, 24.0120>>, 276.7651)
				ELIF NOT DOES_ENTITY_EXIST(pedBlock[3])				   
					pedBlock[3] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01,	<<1568.1180, 6409.2944, 23.8110>>, 73.3582)
				ELIF NOT DOES_ENTITY_EXIST(pedBlock[4])				   
					pedBlock[4] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01,	<<1567.2507, 6415.1094, 23.7167>>, 54.1765)
				ELIF NOT DOES_ENTITY_EXIST(pedBlock[5])				   
					pedBlock[5] = CREATE_PED(PEDTYPE_COP, S_M_Y_COP_01,	<<1564.1042, 6407.8867, 23.7113>>, 54.9604)
					REPEAT COUNT_OF(vehBlockCar) iTemp
						IF IS_VEHICLE_DRIVEABLE(vehBlockCar[iTemp])
							SET_VEHICLE_SIREN(vehBlockCar[iTemp], TRUE)
						ENDIF
					ENDREPEAT
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<1578.0135, 6427.9937, -10>>, <<1000,1000,1000>>)
				roadBlockState = REMOVE_ALL
			ENDIF
		BREAK
		CASE REMOVE_ALL
			REMOVE_ROAD_NODE_SPEED_ZONE(iSpeedRestriction[0])
			REMOVE_ROAD_NODE_SPEED_ZONE(iSpeedRestriction[1])
			REPEAT COUNT_OF(pedBlock) iTemp
				SET_PED_AS_NO_LONGER_NEEDED(pedBlock[iTemp])
			ENDREPEAT
			REPEAT COUNT_OF(vehBlockCar) iTemp
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBlockCar[iTemp])
			ENDREPEAT
			SET_MODEL_AS_NO_LONGER_NEEDED(SHERIFF)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)
			roadBlockState = CHECK_LOCATION
		BREAK
	ENDSWITCH
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		misc procs
// -----------------------------------------------------------------------------------------------------------

PROC PRE_STREAM_CUTSCENE_AT_DISTANCE(STRING scene, VECTOR location, FLOAT distance, BOOL &checkSwitch)
	IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), location) < distance*distance 
	AND NOT checkSwitch
		REQUEST_CUTSCENE(scene)
		checkSwitch = TRUE
	ELSE
		checkSwitch = FALSE
	ENDIF
ENDPROC

//PROC CREATE_HOT_GIRL()
//
//	REQUEST_MODEL(a_f_y_bevhills_01)
//	WHILE NOT HAS_MODEL_LOADED(a_f_y_bevhills_01)
//		WAIT(0)
//	ENDWHILE
//	
//	INT iTemp
//	OPEN_SEQUENCE_TASK(hot_girl_sequence)
//	REPEAT COUNT_OF(v_hot_girl_path) iTemp
//		TASK_GO_STRAIGHT_TO_COORD(NULL, v_hot_girl_path[iTemp], PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//	ENDREPEAT
//	TASK_WANDER_STANDARD(NULL)
//	CLOSE_SEQUENCE_TASK(hot_girl_sequence)
//	
//	hot_girl = CREATE_PED(PEDTYPE_CIVFEMALE, a_f_y_bevhills_01, v_hot_girl_start)
//	SET_PED_COMPONENT_VARIATION(hot_girl, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
//	SET_PED_COMPONENT_VARIATION(hot_girl, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
//	SET_PED_COMPONENT_VARIATION(hot_girl, INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
//	SET_PED_COMPONENT_VARIATION(hot_girl, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
//	SET_PED_COMPONENT_VARIATION(hot_girl, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
//	
//	TASK_PERFORM_SEQUENCE(hot_girl, hot_girl_sequence)
//	CLEAR_SEQUENCE_TASK(hot_girl_sequence)
//	SET_PED_COMBAT_ATTRIBUTES(hot_girl, CA_ALWAYS_FLEE, TRUE)
//	
//	SET_MODEL_AS_NO_LONGER_NEEDED(a_f_y_bevhills_01)
//	
//ENDPROC

FUNC BOOL CREATE_PALETO_VEHICLE(VECTOR vPos, FLOAT fHead)
	IF NOT DOES_ENTITY_EXIST(vehPlayerCar)
		REQUEST_MODEL(PREMIER)
		IF HAS_MODEL_LOADED(PREMIER)
			vehPlayerCar = CREATE_VEHICLE(PREMIER, vPos, fHead)
			SET_VEHICLE_COLOURS(vehPlayerCar, 43, 43)
		ENDIF
	ENDIF
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		SET_ENTITY_COORDS(vehPlayerCar, vPos)
		SET_ENTITY_HEADING(vehPlayerCar, fHead)
		SET_VEHICLE_HAS_STRONG_AXLES(vehPlayerCar, TRUE)
	ENDIF
	RETURN DOES_ENTITY_EXIST(vehPlayerCar)
ENDFUNC

FUNC BOOL SETUP_CAR(VECTOR pos, FLOAT head)
	IF NOT DOES_ENTITY_EXIST(vehPlayerCar)
		CREATE_PLAYER_VEHICLE(vehPlayerCar, CHAR_MICHAEL, pos, head, TRUE, VEHICLE_TYPE_CAR)
	ENDIF
	IF DOES_ENTITY_EXIST(vehPlayerCar)
	AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		SET_ENTITY_COORDS(vehPlayerCar, pos)
		SET_ENTITY_HEADING(vehPlayerCar, head)
		SET_VEHICLE_DISABLE_TOWING(vehPlayerCar, TRUE)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehPlayerCar, SC_DOOR_FRONT_RIGHT, FALSE)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehPlayerCar, SC_DOOR_FRONT_LEFT, FALSE)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehPlayerCar, SC_DOOR_REAR_RIGHT, FALSE)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehPlayerCar, SC_DOOR_REAR_LEFT, FALSE)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehPlayerCar, SC_DOOR_BOOT, FALSE)
		SET_VEHICLE_LOD_MULTIPLIER(vehPlayerCar, 1.5)
		INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehPlayerCar)
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehPlayerCar)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC SET_LESTER_VARIATIONS()
	
	SET_PED_COMPONENT_VARIATION(pedLester, PED_COMP_HEAD, 		0, 0)
	SET_PED_COMPONENT_VARIATION(pedLester, PED_COMP_HAIR, 		0, 0)
	SET_PED_COMPONENT_VARIATION(pedLester, PED_COMP_TORSO, 		0, 1)
	SET_PED_COMPONENT_VARIATION(pedLester, PED_COMP_LEG, 		1, 1)
	SET_PED_COMPONENT_VARIATION(pedLester, PED_COMP_HAND, 		0, 0)
	SET_PED_COMPONENT_VARIATION(pedLester, PED_COMP_TEETH, 		0, 0)
	SET_PED_COMPONENT_VARIATION(pedLester, PED_COMP_SPECIAL, 	1, 0)
	SET_PED_PROP_INDEX(pedLester, ANCHOR_EYES, 0, 0)

ENDPROC

PROC SETUP_PEDS_IN_CAR(VECTOR position, FLOAT heading, BOOL b_set_player_in = TRUE, BOOL bLester = TRUE)

	WHILE NOT SETUP_CAR(position, heading)
		WAIT(0)
	ENDWHILE
	
	IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
	OR IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
	
		VEHICLE_SEAT vsTemp
		
		IF bLester
			vsTemp = VS_BACK_RIGHT
		ELSE
			vsTemp = VS_FRONT_RIGHT
		ENDIF
		
		DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, vehPlayerCar, vsTemp)
			WAIT(0)
		ENDWHILE
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			SET_PED_AS_BUDDY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		ENDIF
		
	ENDIF
	
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		IF bLester
		AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		AND GET_PED_IN_VEHICLE_SEAT(vehPlayerCar, VS_FRONT_RIGHT) = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
			SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar, VS_BACK_RIGHT)
		ENDIF
	ENDIF
	
	IF bLester
		IF NOT DOES_ENTITY_EXIST(pedLester)
		OR IS_PED_INJURED(pedLester)
			DELETE_PED(pedLester)
			WHILE NOT CREATE_NPC_PED_INSIDE_VEHICLE(pedLester, CHAR_LESTER, vehPlayerCar, VS_FRONT_RIGHT)
				WAIT(0)
			ENDWHILE
			SET_LESTER_VARIATIONS()
			IF NOT IS_PED_INJURED(pedLester)
				SET_PED_AS_BUDDY(pedLester)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		IF b_set_player_in
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayerCar, VS_DRIVER)
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar)
				IF bLester
					SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar, VS_BACK_RIGHT)
				ELSE
					SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar, VS_FRONT_RIGHT)
				ENDIF
			ENDIF
		ENDIF
		IF bLester
			IF NOT IS_PED_INJURED(pedLester)
				IF NOT IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
					FREEZE_ENTITY_POSITION(pedLester, FALSE)
					SET_ENTITY_COLLISION(pedLester, TRUE)
					SET_ENTITY_VISIBLE(pedLester, TRUE)
					SET_PED_INTO_VEHICLE(pedLester, vehPlayerCar, VS_FRONT_RIGHT)
				ENDIF
			ENDIF
		ENDIF
		SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCar)
	ENDIF
	
ENDPROC

PROC MISSION_VEHICLE_FAIL_CHECKS()
	IF DOES_ENTITY_EXIST(vehPlayerCar)
		IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			IF IS_VEHICLE_STUCK_TIMER_UP(vehPlayerCar, VEH_STUCK_ON_ROOF, ROOF_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(vehPlayerCar, VEH_STUCK_JAMMED, JAMMED_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(vehPlayerCar, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
			OR IS_VEHICLE_STUCK_TIMER_UP(vehPlayerCar, VEH_STUCK_ON_SIDE, SIDE_TIME)
				Mission_Failed("CBH_VEHSTUCK")
			ENDIF
		ELSE
			Mission_Failed("CMN_GENDEST")
		ENDIF
	ENDIF
ENDPROC

INT iTimeOfLastWarning = 0
FUNC BOOL HAS_PED_BEEN_ABANDONED(PED_INDEX ped, TEXT_HOLDER &gtWarning)
	IF DOES_ENTITY_EXIST(ped)
		IF NOT IS_PED_INJURED(ped)
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(ped)) > 150*150
				RETURN TRUE
			ELIF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(ped)) > 100*100 
			AND (GET_GAME_TIMER() - iTimeOfLastWarning) > 20000
				iTimeOfLastWarning = GET_GAME_TIMER()
				gtWarning.played = FALSE
				PRINT_GOD_TEXT(gtWarning)
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL START_IG_BANK_INTRO()
	REQUEST_ANIM_DICT("missheistpaletoscoresetup_setup1")
	IF HAS_ANIM_DICT_LOADED("missheistpaletoscoresetup_setup1")
		IF NOT IS_PED_INJURED(bank_peds[0])
		AND NOT IS_PED_INJURED(bank_peds[1])
		AND NOT IS_PED_INJURED(bank_peds[2])
		AND NOT IS_PED_INJURED(bank_peds[3])
			INT iTemp
			FOR iTemp = 0 TO 3
				CLEAR_PED_TASKS(bank_peds[iTemp])
			ENDFOR
			iIntroSceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE(bank_peds[0], iIntroSceneId, "missheistpaletoscoresetup_setup1", "cops_response_intro_bank_manager", SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			TASK_SYNCHRONIZED_SCENE(bank_peds[1], iIntroSceneId, "missheistpaletoscoresetup_setup1", "cops_response_intro_bank_clerk1", SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			TASK_SYNCHRONIZED_SCENE(bank_peds[2], iIntroSceneId, "missheistpaletoscoresetup_setup1", "cops_response_intro_bank_clerk2", SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			TASK_SYNCHRONIZED_SCENE(bank_peds[3], iIntroSceneId, "missheistpaletoscoresetup_setup1", "cops_response_intro_bank_clerk3", SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL START_IG_BANK_LOOP()
	REQUEST_ANIM_DICT("missheistpaletoscoresetup_setup2")
	IF HAS_ANIM_DICT_LOADED("missheistpaletoscoresetup_setup2")
		IF NOT IS_PED_INJURED(bank_peds[0])
		AND NOT IS_PED_INJURED(bank_peds[1])
		AND NOT IS_PED_INJURED(bank_peds[2])
		AND NOT IS_PED_INJURED(bank_peds[3])
			INT iTemp
			FOR iTemp = 0 TO 3
				CLEAR_PED_TASKS(bank_peds[iTemp])
			ENDFOR
			iLoopSceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE(bank_peds[0], iLoopSceneId, "missheistpaletoscoresetup_setup2", "cops_response_waitloop_bank_manager", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			TASK_SYNCHRONIZED_SCENE(bank_peds[1], iLoopSceneId, "missheistpaletoscoresetup_setup2", "cops_response_waitloop_bank_clerk1", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			TASK_SYNCHRONIZED_SCENE(bank_peds[2], iLoopSceneId, "missheistpaletoscoresetup_setup2", "cops_response_waitloop_bank_clerk2", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			TASK_SYNCHRONIZED_SCENE(bank_peds[3], iLoopSceneId, "missheistpaletoscoresetup_setup2", "cops_response_waitloop_bank_clerk3", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			SET_SYNCHRONIZED_SCENE_LOOPED(iLoopSceneId, TRUE)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL START_IG_BANK_OUTRO()
	REQUEST_ANIM_DICT("missheistpaletoscoresetup_setup3")
	IF HAS_ANIM_DICT_LOADED("missheistpaletoscoresetup_setup3")
		IF NOT IS_PED_INJURED(bank_peds[0])
		AND NOT IS_PED_INJURED(bank_peds[1])
		AND NOT IS_PED_INJURED(bank_peds[2])
		AND NOT IS_PED_INJURED(bank_peds[3])
		AND NOT IS_PED_INJURED(cars[i_main_cops].cops[0])
		AND NOT IS_PED_INJURED(cars[i_main_cops].cops[1])
		AND IS_VEHICLE_DRIVEABLE(cars[i_main_cops].veh)
			INT iTemp
			FOR iTemp = 0 TO 3
				CLEAR_PED_TASKS(bank_peds[iTemp])
			ENDFOR
			CLEAR_PED_TASKS(cars[i_main_cops].cops[0])
			CLEAR_PED_TASKS(cars[i_main_cops].cops[1])
			iOutroSceneId = CREATE_SYNCHRONIZED_SCENE(scenePosition, sceneRotation)
			TASK_SYNCHRONIZED_SCENE(cars[i_main_cops].cops[0], iOutroSceneId, "missheistpaletoscoresetup_setup3", "Cops_Response_Outro_Cop_Driver", 	INSTANT_BLEND_IN, 	INSTANT_BLEND_OUT, 	SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_DONT_INTERRUPT)
			TASK_SYNCHRONIZED_SCENE(cars[i_main_cops].cops[1], iOutroSceneId, "missheistpaletoscoresetup_setup3", "Cops_Response_Outro_Cop_Passenger", 	INSTANT_BLEND_IN, 	INSTANT_BLEND_OUT, 	SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_DONT_INTERRUPT)
			TASK_SYNCHRONIZED_SCENE(bank_peds[0], iOutroSceneId, "missheistpaletoscoresetup_setup3", "Cops_Response_Outro_Bank_Manager", 				SLOW_BLEND_IN, 		SLOW_BLEND_OUT, 	SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE)
			TASK_SYNCHRONIZED_SCENE(bank_peds[1], iOutroSceneId, "missheistpaletoscoresetup_setup3", "Cops_Response_Outro_Bank_Clerk1", 				SLOW_BLEND_IN, 		SLOW_BLEND_OUT, 	SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE)
			TASK_SYNCHRONIZED_SCENE(bank_peds[2], iOutroSceneId, "missheistpaletoscoresetup_setup3", "Cops_Response_Outro_Bank_Clerk2", 				SLOW_BLEND_IN, 		SLOW_BLEND_OUT, 	SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE)
			TASK_SYNCHRONIZED_SCENE(bank_peds[3], iOutroSceneId, "missheistpaletoscoresetup_setup3", "Cops_Response_Outro_Bank_Clerk3", 				SLOW_BLEND_IN, 		SLOW_BLEND_OUT, 	SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE)
			FREEZE_ENTITY_POSITION(cars[i_main_cops].veh, FALSE)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(cars[i_main_cops].veh, iOutroSceneId, "Cops_Response_Outro_CAR", "missheistpaletoscoresetup_setup3", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_BLOCK_MOVER_UPDATE) | ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS) |  ENUM_TO_INT(SYNCED_SCENE_DONT_INTERRUPT) , INSTANT_BLEND_IN)
//			PLAY_ENTITY_ANIM(cars[i_main_cops].veh, "Cops_Response_Outro_CAR", "missheistpaletoscoresetup_setup3", INSTANT_BLEND_IN, FALSE, FALSE, FALSE, 0, ENUM_TO_INT(SYNCED_SCENE_BLOCK_MOVER_UPDATE) | ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS) |  ENUM_TO_INT(SYNCED_SCENE_DONT_INTERRUPT))
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(cars[i_main_cops].veh)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DELETE_BLIP(BLIP_INDEX &this_blip)
	IF DOES_BLIP_EXIST(this_blip)
		REMOVE_BLIP(this_blip)
	ENDIF
ENDPROC

PROC DELETE_ALL_BLIPS()
	DELETE_BLIP(blipMichael)
	DELETE_BLIP(blipLester)
	DELETE_BLIP(blipVehicle)
	DELETE_BLIP(blipDriveTo)
	DELETE_BLIP(blipDriveTo)
	DELETE_BLIP(blipAlarm)
	DELETE_BLIP(blipBuddy)
ENDPROC

PROC SET_ALL_VARS_TO_DEFAULT()
	DELETE_ALL_BLIPS()
	iReDirectLine = 0
	bCutscenePreStreamed = FALSE
	iStageProgress = 0
	SETTIMERA(0)
	SETTIMERB(0)
ENDPROC

FUNC BOOL IS_MODEL_IN_LIST(MODEL_NAMES model, MODEL_NAMES &list[])
	INT iTemp
	BOOL bfound = FALSE
	REPEAT COUNT_OF(list) iTemp
		IF NOT bfound
			IF model = list[iTemp]
				bfound = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN bfound
ENDFUNC

FUNC BOOL ASSET_MANAGER(MISSION_STAGE_FLAG stage, BOOL loading, BOOL cleanup)

	MODEL_NAMES script_models[15]
	script_models[0] = PLAYER_TWO
	script_models[1] = A_M_M_SALTON_02
	script_models[2] = PREMIER
	script_models[3] = PROP_LD_ALARM_01
	script_models[4] = SHERIFF
	script_models[5] = S_M_Y_COP_01		
	script_models[6] = BUS
	script_models[7] = S_M_M_LSMETRO_01
	script_models[8] = PROP_LD_ALARM_01_DAM
	script_models[9]  =	A_M_M_BUSINESS_01 
	script_models[10] =	A_F_Y_BUSINESS_01
	
	INT iTemp
	MODEL_NAMES stage_models[15]
	REPEAT COUNT_OF(stage_models) iTemp
		stage_models[iTemp] = DUMMY_MODEL_FOR_SCRIPT
	ENDREPEAT
	
	INT	stage_recs[6]
	REPEAT COUNT_OF(stage_recs) iTemp
		stage_recs[iTemp] = 0
	ENDREPEAT
	
	SWITCH stage
	
		CASE stage_intro_scene
			stage_models[0] = PLAYER_TWO
			stage_models[1] = PREMIER
			stage_models[2] = PROP_LD_ALARM_01
			stage_models[3] = GET_NPC_PED_MODEL(CHAR_LESTER)
		BREAK
		
		CASE stage_drive_to_bus
			stage_models[0] = PLAYER_TWO
			stage_models[1] = PREMIER
			stage_models[2] = PROP_LD_ALARM_01
			stage_models[4] = GET_NPC_PED_MODEL(CHAR_LESTER)
		BREAK
		
		CASE stage_bus_stop_scene
			stage_models[0] = PLAYER_TWO
			stage_models[1] = BUS
			stage_models[2] = S_M_M_LSMETRO_01
			stage_models[3] = PREMIER
			stage_models[4] = PROP_LD_ALARM_01
			stage_models[5] = GET_NPC_PED_MODEL(CHAR_LESTER)
		BREAK
		
		CASE stage_drive_to_bank
			stage_models[0] = PLAYER_TWO
			stage_models[1] = A_M_M_SALTON_02
			stage_models[2] = BUS
			stage_models[3] = S_M_M_LSMETRO_01
			stage_models[4] = PREMIER
			stage_models[5] = PROP_LD_ALARM_01
			stage_models[6] = GET_NPC_PED_MODEL(CHAR_LESTER)
		BREAK
		
		CASE stage_box_reveal_scene
			stage_models[0] = PLAYER_TWO
			stage_models[1] = PREMIER
			stage_models[2] = PROP_LD_ALARM_01
			stage_models[3] = GET_NPC_PED_MODEL(CHAR_LESTER)
		BREAK
		
		CASE stage_shoot_box
			stage_models[0] = PLAYER_TWO
			stage_models[1] = PREMIER
			stage_models[2] = PROP_LD_ALARM_01
			stage_models[3] = PROP_LD_ALARM_01_DAM
			stage_models[4] = GET_NPC_PED_MODEL(CHAR_LESTER)
		BREAK
		
		CASE stage_hide
			stage_models[0] = PLAYER_TWO
			stage_models[1] = PREMIER
			stage_models[2] = SHERIFF
			stage_models[3] = S_M_Y_COP_01	
			stage_models[4] = PROP_LD_ALARM_01_DAM
			stage_models[5] = A_M_M_BUSINESS_01
			stage_models[6] = A_F_Y_BUSINESS_01
			stage_models[7] = GET_NPC_PED_MODEL(CHAR_LESTER)
			stage_recs[0] = 1
			stage_recs[1] = 2
			stage_recs[2] = 3
			stage_recs[3] = 4
		BREAK
		
		CASE stage_race_home
			stage_models[0] = PLAYER_TWO
			stage_models[1] = PREMIER
			stage_models[2] = SHERIFF
			stage_models[3] = S_M_Y_COP_01
			stage_models[4] = PROP_LD_ALARM_01_DAM
			stage_models[5] = A_M_M_BUSINESS_01
			stage_models[6] = A_F_Y_BUSINESS_01
			stage_models[7] = GET_NPC_PED_MODEL(CHAR_LESTER)
			stage_recs[0] = 1
			stage_recs[1] = 2
			stage_recs[2] = 3
			stage_recs[3] = 4
		BREAK
		
//		CASE stage_outro_scene
//			stage_models[0] = PLAYER_TWO
//			stage_models[1] = GET_NPC_PED_MODEL(CHAR_LESTER)
//			stage_models[2] = PREMIER
//			stage_models[3] = PROP_LD_ALARM_01
//		BREAK
		
	ENDSWITCH
	
	IF loading
	
		//send requests for models
		
		BOOL bLoaded = TRUE
		REPEAT COUNT_OF(stage_models) iTemp
			IF stage_models[iTemp] <> DUMMY_MODEL_FOR_SCRIPT
				REQUEST_MODEL(stage_models[iTemp])
				IF NOT HAS_MODEL_LOADED(stage_models[iTemp])
					bLoaded = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(stage_recs) iTemp
			IF stage_recs[iTemp] <> 0
				REQUEST_VEHICLE_RECORDING(stage_recs[iTemp], s_rec_tag)
				IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(stage_recs[iTemp], s_rec_tag)
					bLoaded = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
		
		RETURN bLoaded
		
	ENDIF
	
	IF cleanup
	
		//Cleanup any models loaded that are no longer needed
		REPEAT COUNT_OF(script_models) iTemp
			IF NOT IS_MODEL_IN_LIST(script_models[iTemp], stage_models)
				SET_MODEL_AS_NO_LONGER_NEEDED(script_models[iTemp])
			ENDIF
		ENDREPEAT
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// -----------------------------------------------------------------------------------------------------------
//		mission stage inits
// -----------------------------------------------------------------------------------------------------------

PROC GO_TO_STAGE(MISSION_STAGE_FLAG stage)
	IF stage <> stage_test
	AND stage <> stage_intro_scene
		IF IS_CUTSCENE_ACTIVE()
		AND IS_CUTSCENE_PLAYING()
			STOP_CUTSCENE()
			REMOVE_CUTSCENE()
			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
		ENDIF
		bCutsceneLoaded = FALSE
		IF NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)	
			WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
				PRINTLN("WAITING ON TEXT")
				WAIT(0)
			ENDWHILE
		ENDIF
	ENDIF
	iStageProgress = 0
	current_mission_stage = stage
	b_STAGE_READY = FALSE
ENDPROC
//
//INT iCurrentRoadBlock  = -1
//
//FUNC BOOL SET_BANK_ROADS_STATUS(BOOL bActive)
//	IF iCurrentRoadBlock = -1
//		iCurrentRoadBlock = 0
//	ENDIF
//	SWITCH iCurrentRoadBlock
//		CASE 0 SET_ROADS_IN_ANGLED_AREA(<<-228.530930,6154.761230,40.694798>>, <<-366.106812,6303.107422,27.811075>>, 29.250000, FALSE, bActive) BREAK
//		CASE 1 SET_ROADS_IN_ANGLED_AREA(<<69.936897,6605.138672,40.930729>>, <<-369.645325,6165.683105,29.365540>>, 9.250000, FALSE, bActive) BREAK
//		CASE 2 SET_ROADS_IN_ANGLED_AREA(<<-102.277191,6287.074707,40.848347>>, <<-180.381332,6365.334961,29.487217>>, 9.250000, FALSE, bActive) BREAK
//		CASE 3 SET_ROADS_IN_ANGLED_AREA(<<-108.457703,6416.611328,40.806339>>, <<-170.138702,6478.151855,28.110949>>, 9.250000, FALSE, bActive) BREAK
//		CASE 4 SET_ROADS_IN_ANGLED_AREA(<<-168.661606,6488.050781,25.203751>>, <<37.190262,6650.522461,37.991776>>, 55.750000, FALSE, bActive) BREAK
//		CASE 5 SET_PED_PATHS_IN_AREA(<<-127.48, 6445.71, 30.55>> + <<50,50,50>>, <<-127.48, 6445.71, 30.55>> - <<50,50,50>>, bActive) BREAK
//		CASE 6 SET_ROADS_IN_ANGLED_AREA( <<-43.158634,6610.609375,11.582199>>, <<-174.391464,6478.103516,49.247364>>, 37.750000, FALSE, bActive) BREAK
//		CASE 7 SET_ROADS_IN_ANGLED_AREA( <<-174.428894,6477.521973,11.751795>>, <<-372.553589,6302.189941,48.737434>>, 51.000000, FALSE, bActive) BREAK
//	ENDSWITCH
//	IF iCurrentRoadBlock < 7
//		iCurrentRoadBlock++
//	ELSE 
//		iCurrentRoadBlock = -1
//		RETURN TRUE
//	ENDIF
//	RETURN FALSE
//ENDFUNC

FUNC BOOL CREATE_BIKER()
	IF NOT DOES_ENTITY_EXIST(pedBiker)
		REQUEST_MODEL(RUFFIAN)
		REQUEST_MODEL(A_M_Y_GENSTREET_02)
		IF HAS_MODEL_LOADED(RUFFIAN)
		AND HAS_MODEL_LOADED(A_M_Y_GENSTREET_02)
			vehTrevorBike = CREATE_VEHICLE(RUFFIAN, vBikerStartCoords, fBikerStartHeading)
			pedBiker = CREATE_PED_INSIDE_VEHICLE(vehTrevorBike, PEDTYPE_MISSION, A_M_Y_GENSTREET_02)
			SET_PED_COMPONENT_VARIATION(pedBiker, PED_COMP_HEAD, 1, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedBiker, PED_COMP_TORSO, 0, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedBiker, PED_COMP_LEG, 0, 0, 0)
			SET_VEHICLE_COLOUR_COMBINATION(vehTrevorBike, 1)
			SET_VEHICLE_ENGINE_HEALTH(vehTrevorBike, 3000)
			SET_VEHICLE_STARTUP_REV_SOUND(vehTrevorBike, "Trevor_Revs_Off","PALETO_SCORE_SETUP_SOUNDS")
			SET_ENTITY_HEALTH(vehTrevorBike, 3000)
			SET_VEHICLE_DOORS_LOCKED(vehTrevorBike, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			SET_PED_COMBAT_ATTRIBUTES(pedBiker, CA_ALWAYS_FLEE, TRUE)
			SET_PED_FLEE_ATTRIBUTES(pedBiker, FA_DISABLE_HANDS_UP, TRUE)
			SET_PED_FLEE_ATTRIBUTES(pedBiker, FA_DISABLE_COWER, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_GENSTREET_02)
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedBiker, KNOCKOFFVEHICLE_HARD)
			SET_VEH_RADIO_STATION(vehTrevorBike, GET_RADIO_STATION_NAME(6))
			SET_PED_HELMET(pedBiker, FALSE)
		ENDIF
	ENDIF
	RETURN DOES_ENTITY_EXIST(pedBiker) AND DOES_ENTITY_EXIST(vehTrevorBike)
ENDFUNC

#IF IS_DEBUG_BUILD

	PROC JSKIP()
		IF current_mission_stage < stage_cap
			DELETE_ALL_BLIPS()
			SWITCH current_mission_stage
				CASE stage_drive_to_bank
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -129.8776, 6431.5283, 30.4176 >>) > 75*75 
						SETUP_PEDS_IN_CAR(<< -129.8776, 6431.5283, 30.4176 >>, 42.4173)
					ELSE
						GO_TO_STAGE(stage_box_reveal_scene)
						bSkip = TRUE
					ENDIF
				BREAK
				CASE stage_hide
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_park_hide) > 30*30 
						SETUP_PEDS_IN_CAR(v_park_hide, f_park_hide) //infront of parking space
					ELSE
						GO_TO_STAGE(stage_race_home)
						bSkip = TRUE
					ENDIF
				BREAK
				CASE stage_lose_the_cops
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				BREAK
				CASE stage_race_home
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1407.1555, 3591.4934, 33.9378>>)
					ELSE
						IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
						AND NOT IS_PED_INJURED(pedLester)
							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
							ENDIF
							IF NOT IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
								SET_PED_INTO_VEHICLE(pedLester, vehPlayerCar, VS_FRONT_RIGHT)
							ENDIF
							SET_ENTITY_COORDS(vehPlayerCar, <<1407.1555, 3591.4934, 33.9378>>)
						ENDIF
					ENDIF
				BREAK
				DEFAULT
					GO_TO_STAGE(INT_TO_ENUM(MISSION_STAGE_FLAG, ENUM_TO_INT(current_mission_stage)+1))
					bSkip = TRUE
				BREAK
			ENDSWITCH
			
		ENDIF
	ENDPROC
	
	PROC PSKIP()
		IF current_mission_stage > stage_intro_scene
			SWITCH current_mission_stage
				CASE stage_lose_the_cops
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				BREAK
				DEFAULT
					GO_TO_STAGE(INT_TO_ENUM(MISSION_STAGE_FLAG, ENUM_TO_INT(current_mission_stage)-1))
					bSkip = TRUE
				BREAK
			ENDSWITCH

			DELETE_ALL_BLIPS()
		ENDIF
	ENDPROC

	PROC DEBUG_CUTSCENE_CONTROL(BOOL bForceEnd = FALSE)
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			STOP_CUTSCENE()
			WAIT(0)
		ENDIF
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		OR bForceEnd
			STOP_CUTSCENE()
			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			bCutsceneLoaded = FALSE
			IF NOT bForceEnd
				IF current_mission_stage > stage_drive_to_bus
					PSKIP()
				ENDIF
			ENDIF
		ENDIF
	ENDPROC
	
#ENDIF

PROC INITIALISE()

	PRINTLN("Initialising mission - Paleto setup")
	REQUEST_MODEL(PLAYER_TWO)
	REQUEST_MODEL(PREMIER)
	REQUEST_ADDITIONAL_TEXT("BANKS1", MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT(sBlockId, MISSION_DIALOGUE_TEXT_SLOT)
	REQUEST_MODEL(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
	
	#IF IS_DEBUG_BUILD
		skipMenuOptions[0].sTxtLabel = "rbhs_int"
		skipMenuOptions[1].sTxtLabel = "Drive to the bus stop"
		skipMenuOptions[2].sTxtLabel = "rbhs_mcs_1"
		skipMenuOptions[3].sTxtLabel = "Drive to the bank"
		skipMenuOptions[4].sTxtLabel = "rbhs_mcs_2"
		skipMenuOptions[5].sTxtLabel = "Shoot the box"
		skipMenuOptions[6].sTxtLabel = "Hide"
		skipMenuOptions[7].sTxtLabel = "Race back to meth lab"
		skipMenuOptions[8].sTxtLabel = "grab "
	#ENDIF
	
	ADD_RELATIONSHIP_GROUP("THE_COPS", the_cops)
	ADD_RELATIONSHIP_GROUP("BANK_PEDS", bank_peds_group)
	
	SET_PED_PATHS_IN_AREA(v_car_start + <<50,50,50>>, v_car_start - <<50,50,50>>, FALSE)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, bank_peds_group)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-122.75471, 6451.52881, 30.46846>> - <<4, 4, 3>>, <<-122.75471, 6451.52881, 30.46846>> + <<4, 4, 3>>, FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-128.91570, 6501.80225, 28.62807>> - <<6, 6, 3>>, <<-128.91570, 6501.80225, 28.62807>> + <<6, 6, 3>>, FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-94.00424, 6424.18457, 30.47077>> - <<6, 6, 3>>, <<-94.00424, 6424.18457, 30.47077>> + <<6, 6, 3>>, FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-346.8897, 6194.5376, 30.3076>> - <<20, 20, 10>>, <<-94.00424, 6424.18457, 30.47077>> + <<20, 20, 10>>, FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-157.2882, 6460.7593, 30.1739>> - <<5, 5, 3>>, <<-157.2882, 6460.7593, 30.1739>> + <<5, 5, 3>>, FALSE)
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), RELGROUPHASH_PLAYER)			
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	ENDIF
	
	SETTIMERA(0)
	SETTIMERB(0)
	
	CLEAR_AREA_OF_VEHICLES(v_park_shoot, 1000)
	REQUEST_AMBIENT_AUDIO_BANK("SCRIPT\\Alarms")
	
	INIT_AMBIENT_BANK()
	INIT_ALARM(v_bank_alarm_pos, v_bank_alarm_rot)
	
	IF Is_Replay_In_Progress()
	
		FADE_OUT()
		
		INT iStage = GET_REPLAY_MID_MISSION_STAGE()
		IF g_bShitskipAccepted = TRUE
			iStage++
		ENDIF
		
		IF iStage > 3
		
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
					WAIT(0)
				ENDWHILE
			ENDIF
			
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING, TRUE)
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING, FALSE)
			
			START_REPLAY_SETUP(<<1393.5417, 3580.1377, 33.9722>>, 353.4546)
			
			END_REPLAY_SETUP()
			
			Mission_Passed()
			
		ELSE
			SWITCH iStage
				CASE 0
					vReplayCoords = v_player_start
					fReplayHeading = f_player_start
					CLEAR_AREA(v_car_start, 500, TRUE)
					GO_TO_STAGE(stage_drive_to_bus)
				BREAK
				CASE 1
					CLEAR_AREA(v_busstop_car_park, 500, TRUE)
					vReplayCoords = v_busstop_car_park
					fReplayHeading = f_busstop_car_park
					IF g_bShitskipAccepted
						GO_TO_STAGE(stage_bus_stop_scene)
					ELSE
						GO_TO_STAGE(stage_drive_to_bank)
					ENDIF
				BREAK
				CASE 2
					
					CLEAR_AREA(v_park_shoot, 1000, TRUE)
					IF (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL) = 0)
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 500, TRUE)
					ENDIF
					IF g_bShitskipAccepted
						vReplayCoords = v_park_shoot
						fReplayHeading = f_park_shoot
						bSkipAlongInGameSeq = TRUE
						GO_TO_STAGE(stage_box_reveal_scene)
					ELSE
						vReplayCoords = v_stand_shoot
						fReplayHeading = f_stand_shoot
						GO_TO_STAGE(stage_shoot_box)
					ENDIF
				BREAK
				CASE 3
					BLOCK_SCENARIOS_AT_AREA(PS1SBA_BANK)
					vReplayCoords = v_park_hide
					fReplayHeading = f_park_hide
					CLEAR_AREA(v_park_hide, 1000, TRUE)
					GO_TO_STAGE(stage_race_home)
				BREAK
			ENDSWITCH
			bSkip=TRUE
		ENDIF
		
		START_REPLAY_SETUP(vReplayCoords, fReplayHeading, FALSE)
		
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
			WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
				WAIT(0)
			ENDWHILE
		ENDIF
		
	ENDIF
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-73.1887, 6426.1177, 30.4401>> - <<7,7,1>>, <<-73.1887, 6426.1177, 30.4401>> + <<7,7,2>>, FALSE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(PREMIER, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(RUFFIAN, TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(S_M_Y_COP_01, TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(S_F_Y_COP_01, TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(S_M_Y_HWAYCOP_01, TRUE)
	SET_CREATE_RANDOM_COPS(FALSE)
	
	BLOCK_SCENARIOS_AT_AREA(PS1SBA_TUNNEL)
	BLOCK_SCENARIOS_AT_AREA(PS1SBA_BUS)
	BLOCK_SCENARIOS_AT_AREA(PS1SBA_METHLAB)
	
	SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_DRIVE_SOLO", FALSE)
	//SET_PED_PATHS_IN_AREA(<<-116.522598,6464.545410,40.818439>> +  <<29.750000,29.750000,12.250000>>, <<-116.522598,6464.545410,40.818439>> - <<29.750000,29.750000,12.250000>>, FALSE)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-118.874969,6448.463379,32.218449>>+<<8.000000,6.250000,2.250000>>, <<-118.874969,6448.463379,32.218449>> - <<8.000000,6.250000,2.250000>>, FALSE)
	
	INIT_ALARM(v_bank_alarm_pos, v_bank_alarm_rot)
	
	ADD_PED_FOR_DIALOGUE(sSpeech, 4, NULL, "PALETOSCANNER")
	ADD_PED_FOR_DIALOGUE(sSpeech, 5, NULL, "PALETOCOP1")
	
	SET_SHOP_IS_TEMPORARILY_UNAVAILABLE(HAIRDO_SHOP_07_PB, TRUE)
	
	DISABLE_TAXI_HAILING(TRUE)
	
	PREPARE_ALARM("PALETO_BAY_SCORE_ALARM")
	
	PRINTLN("Initialising complete - Paleto setup  v2")
	
ENDPROC

FUNC BOOL IS_MISSION_CALLED_FROM_DEBUG_MENU()
	BOOL bTooFar
	BOOL bNotInCar
	IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_car_start) > 50*50 
		bToofar = TRUE
	ENDIF
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		bNotInCar = TRUE
	ENDIF
	RETURN bTooFar OR bNotInCar
ENDFUNC

FUNC BOOL IS_PED_READY(PED_INDEX ped)
	IF NOT IS_PED_INJURED(ped)
		IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(ped)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC INIT_MISSION_STAGE(MISSION_STAGE_FLAG stage)

	INIT_TEXT_AND_DIALOGUE()
	ASSET_MANAGER(stage, FALSE, TRUE)
	KILL_ANY_CONVERSATION()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	SET_GPS_MULTI_ROUTE_RENDER(FALSE)
	
	REMOVE_ANIM_DICT("missheistpaletoscore1leadinoutrbhs_int_1")
	REMOVE_ANIM_DICT("missrbhsig_2")
	
	IF stage <> stage_shoot_box
	AND stage <> stage_bus_stop_scene
		IF DOES_CAM_EXIST(camFinalFrame)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			DESTROY_CAM(camFinalFrame)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(objMethLabDoorGrab)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objMethLabDoorGrab)
	ENDIF
	
	IF stage != stage_intro_scene
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
			SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			ENDIF
		ENDIF
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		STOP_GAMEPLAY_HINT()
	ENDIF
	
	IF current_mission_stage != stage_intro_scene
	AND current_mission_stage != stage_hide
		WHILE NOT ASSET_MANAGER(stage, TRUE, FALSE)
			PRINTLN("Loading stage assets")
			WAIT(0)
		ENDWHILE
	ELSE
		ASSET_MANAGER(stage, TRUE, FALSE)
	ENDIF
	
	IF current_mission_stage = stage_hide
	OR current_mission_stage = stage_race_home
		SET_ROADS_IN_ANGLED_AREA(<<99.730011,6625.367188,28.834698>>, <<-298.835266,6237.279785,38.427986>>, 201.375000, TRUE, FALSE)
		//SET_PED_DENSITY_MULTIPLIER(2.0)
	ELSE
		//SET_PED_DENSITY_MULTIPLIER(1.0)
		SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<99.730011,6625.367188,28.834698>>, <<-298.835266,6237.279785,38.427986>>, 201.375000)
	ENDIF
	
	IF stage = stage_drive_to_bank
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "stage_drive_to_bank")
	ELIF stage = stage_shoot_box
	OR stage = stage_hide
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "stage_shoot_box")
	ELIF stage = stage_race_home
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "stage_race_home",TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objDummy)
		DELETE_OBJECT(objDummy)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_TEST_01)
	ENDIF
	
	BLOCK_SCENARIOS_AT_AREA(PS1SBA_GAS, stage >= stage_drive_to_bank)
	BLOCK_SCENARIOS_AT_AREA(PS1SBA_LOT, stage >= stage_drive_to_bank)
	
	IF bSkip
	
		#IF IS_DEBUG_BUILD
			PRINTLN("Skipping in init_mission_stage")
		#ENDIF
		
		IF stage < stage_hide
			INIT_ALARM(v_bank_alarm_pos, v_bank_alarm_rot)
			SET_VEHICLE_POPULATION_BUDGET(3)
			SET_PED_POPULATION_BUDGET(3)
		ELSE
			INIT_ALARM(v_bank_alarm_pos, v_bank_alarm_rot, TRUE)
			SET_VEHICLE_POPULATION_BUDGET(1)
			SET_PED_POPULATION_BUDGET(1)
		ENDIF
		REMOVE_CUTSCENE()
		RESET_BANK_SCENE()
		SET_WIDESCREEN_BORDERS(FALSE, 0)
		IF DOES_ENTITY_EXIST(bankAlarm.obj)
			bankAlarm.bOn = FALSE
		ENDIF
		IF DOES_ENTITY_EXIST(pedBiker)
			DELETE_PED(pedBiker)
		ENDIF
		IF DOES_ENTITY_EXIST(vehTrevorBike)
			IF IS_VEHICLE_DRIVEABLE(vehTrevorBike)
				IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(vehTrevorBike))
					IF NOT IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(vehTrevorBike))
						SET_ENTITY_COORDS(GET_PED_IN_VEHICLE_SEAT(vehTrevorBike), GET_ENTITY_COORDS(vehTrevorBike))
					ENDIF
				ENDIF
			ENDIF
			DELETE_VEHICLE(vehTrevorBike)
		ENDIF
		IF DOES_CAM_EXIST(sTimelapse.splineCamera)
			IF IS_CAM_RENDERING(sTimelapse.splineCamera)
				SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE, FALSE, 0, TRUE, FALSE)
				DESTROY_CAM(sTimelapse.splineCamera)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(vehPlayerCar)
			SET_ENTITY_COLLISION(vehPlayerCar, TRUE)
		ENDIF
		IF DOES_ENTITY_EXIST(vehTrevorBike)
			SET_ENTITY_COLLISION(vehTrevorBike, TRUE)
		ENDIF
		STOP_AUDIO_SCENES()
		
	ENDIF
	
	IF DOES_CAM_EXIST(camLapse)
		DESTROY_CAM(camLapse)
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayerCar)
			STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayerCar)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		SET_HORN_ENABLED(vehPlayerCar, TRUE)
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehTrevorBike)
		SET_HORN_ENABLED(vehTrevorBike, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objDummy)
		DELETE_OBJECT(objDummy)
	ENDIF
	
	iStageProgress = 0
	
	SWITCH stage
	
		CASE stage_intro_scene
			IF DOES_ENTITY_EXIST(pedLester)
				DELETE_PED(pedLester)
			ENDIF
			IF bSkip
				IF DOES_ENTITY_EXIST(pedLester)
					DELETE_PED(pedLester)
				ENDIF
				FADE_OUT()
				DO_PLAYER_WARP_WITH_LOAD(v_player_start, f_player_start)
				IF DOES_ENTITY_EXIST(vehPlayerCar)
					SET_ENTITY_COORDS(vehPlayerCar, v_car_start)
					SET_ENTITY_HEADING(vehPlayerCar, f_car_start)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCar)
				ENDIF
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
				FADE_IN()
			ENDIF
		BREAK
		
		CASE stage_drive_to_bus
			IF DOES_ENTITY_EXIST(pedLester)
				DELETE_PED(pedLester)
			ENDIF
			IF bSkip
				FADE_OUT()
				RESET_BANK_SCENE()
				iAmbientBankStage = 0
				IF NOT IS_REPLAY_BEING_SET_UP()
					DO_PLAYER_WARP_WITH_LOAD(v_player_start, f_player_start, FALSE)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), v_player_start)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), f_player_start)
				ENDIF
				SETUP_PEDS_IN_CAR(v_car_start, f_car_start, FALSE, FALSE)
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], v_trevor_start)
					SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], f_trevor_start)
				ENDIF
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
				CLEAR_AREA(v_car_start, 50, TRUE)
				WHILE NOT IS_PED_READY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					WAIT(0)
				ENDWHILE
			ENDIF
			ADD_PED_FOR_DIALOGUE(sSpeech, 1, NULL, "LESTER")
			IF NOT IS_REPLAY_BEING_SET_UP()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			IF DOES_ENTITY_EXIST(pedLester)
				DELETE_PED(pedLester)
			ENDIF
			FORCE_CLEANUP_RESPONSE_TEAM()
			bParked = FALSE
			SET_ALL_VARS_TO_DEFAULT()
		BREAK
		
		CASE stage_bus_stop_scene
			IF bSkip
				FADE_OUT()
				IF NOT DOES_ENTITY_EXIST(pedLester)
					WHILE NOT CREATE_NPC_PED_ON_FOOT(pedLester, CHAR_LESTER, <<-317.9068, 6195.8940, 36.0577>>, 72.1587)
						WAIT(0)
					ENDWHILE
					SET_LESTER_VARIATIONS()
				ENDIF
				
				SETUP_PEDS_IN_CAR(v_busstop_car_park, f_busstop_car_park, TRUE, FALSE)
				DO_PLAYER_WARP_WITH_LOAD(v_busstop_car_park, f_busstop_car_park)
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
			ENDIF
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			FORCE_CLEANUP_RESPONSE_TEAM()
			SET_ALL_VARS_TO_DEFAULT()
		BREAK
		
		CASE stage_drive_to_bank
			IF bSkip
				FADE_OUT()
				CREATE_BUS(<<-333.1017, 6190.6001, 30.2311>>, 313.7933)
				RESET_BANK_SCENE()
				iAmbientBankStage = 0
				DO_PLAYER_WARP_WITH_LOAD(v_busstop_car_park, f_busstop_car_park)
				SETUP_PEDS_IN_CAR(v_busstop_car_park, f_busstop_car_park)				
				WHILE NOT IS_PED_READY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					WAIT(0)
				ENDWHILE
				WHILE NOT IS_PED_READY(pedLester)
					WAIT(0)
				ENDWHILE
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
			ENDIF
						
			IF IS_VEHICLE_DRIVEABLE(vehBus)
				SET_ENTITY_COORDS_NO_OFFSET(vehBus, <<-315.9196, 6208.7734, 31.3002>>)
				SET_ENTITY_QUATERNION(vehBus, 0.0104, 0.0149, -0.3583, 0.9334)
			ENDIF
			
			REMOVE_PED_FOR_DIALOGUE(sSpeech, 1)
			ADD_PED_FOR_DIALOGUE(sSpeech, 1, pedLester, "LESTER")
			FORCE_CLEANUP_RESPONSE_TEAM()
			SET_ALL_VARS_TO_DEFAULT()
		BREAK
		
		CASE stage_box_reveal_scene
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			FORCE_CLEANUP_RESPONSE_TEAM()
			SET_ALL_VARS_TO_DEFAULT()
			SET_CUSTOM_CHASE_HINT_CAM_INTERP_IN_TIME(localChaseHintCamStruct, 2000)
			SET_CUSTOM_CHASE_HINT_CAM_INTERP_OUT_TIME(localChaseHintCamStruct, 2000)
			IF bSkip
				FADE_OUT()
				bSkipAlongInGameSeq = TRUE
				RESET_BANK_SCENE()
				iAmbientBankStage = 0
				DO_PLAYER_WARP_WITH_LOAD(v_park_shoot, f_park_shoot)
				SETUP_PEDS_IN_CAR(v_park_shoot, f_park_shoot)
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
			ENDIF
		BREAK
		
		CASE stage_shoot_box
			iNumberOfShotsAtAlarm = 0
			FORCE_CLEANUP_RESPONSE_TEAM()
			SET_ALL_VARS_TO_DEFAULT()
			SET_CUSTOM_CHASE_HINT_CAM_INTERP_IN_TIME(localChaseHintCamStruct, 2000)
			SET_CUSTOM_CHASE_HINT_CAM_INTERP_OUT_TIME(localChaseHintCamStruct, 2000)
			IF bSkip
				FADE_OUT()
				CLEAR_AREA(v_park_shoot, 1000, TRUE)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), v_stand_shoot)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), f_stand_shoot)
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(17.280)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(3.160)
				DO_PLAYER_WARP_WITH_LOAD(<< -119.2339, 6454.9365, 30.4069 >>, 316.9120)
				SETUP_PEDS_IN_CAR(v_park_shoot, f_park_shoot,FALSE)
				WHILE NOT IS_PED_READY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					WAIT(0)
				ENDWHILE
				WHILE NOT IS_PED_READY(pedLester)
					WAIT(0)
				ENDWHILE
				IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
					SET_ENTITY_COORDS_NO_OFFSET(vehPlayerCar, <<-132.2803, 6498.7432, 29.1556>>)
					SET_ENTITY_QUATERNION(vehPlayerCar, 0.0108, 0.0008, 0.7774, -0.6289)
//					SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCar)
				ENDIF
				WAIT(1000)
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L), DOORSTATE_LOCKED)
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R), DOORSTATE_LOCKED)
			ENDIF
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
			AND GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL) <> 0
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, TRUE)
			ENDIF
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL)
			AND GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL) <> 0
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL, TRUE)
			ENDIF
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL)
			AND GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL) <> 0
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL, TRUE)
			ENDIF
			iLastShootReminder = GET_GAME_TIMER()
		BREAK
		
		CASE stage_hide
		
			iStageProgress = 0
			iBankPedsProgress = 0
			iCopProgress = 0
			iBikerStage = 0
			
			bRecentGodText = FALSE
			iCurrentConv = 0
			bLookingAtHotGirl = FALSE
			bTalkingAboutGirl = FALSE
			bCopsArrived = FALSE
			iTimeSinceLastReminder = GET_GAME_TIMER()
			
			iIntroSceneId = -1
			iLoopSceneId = -1
			iOutroSceneId = -1
			
			bFocusHelpUsed = FALSE
			bFocusHelpCleared = FALSE
			
			IF bSkip
				FADE_OUT()
//				WHILE NOT SET_BANK_ROADS_STATUS(FALSE)
//					WAIT(0)
//				ENDWHILE
				CLEAR_AREA(v_park_shoot, 300, TRUE)
				DO_PLAYER_WARP_WITH_LOAD(v_park_shoot, f_park_shoot)
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
				SETUP_PEDS_IN_CAR(v_park_shoot, f_park_shoot)
				IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
					SET_ENTITY_COORDS_NO_OFFSET(vehPlayerCar, <<-132.2819, 6498.7437, 29.1569>>)
					SET_ENTITY_QUATERNION(vehPlayerCar, 0.0109, 0.0008, 0.7780, -0.6281)
				ENDIF
			ENDIF
			
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L), DOORSTATE_UNLOCKED)
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R), DOORSTATE_UNLOCKED)
			
			FORCE_CLEANUP_RESPONSE_TEAM()
			SET_ALL_VARS_TO_DEFAULT()
			vCurrentFocus = v_bank_door
			vTargetFocus = v_bank_door
			iAlarmTime = GET_GAME_TIMER()
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE, 5000)
		BREAK
		
		CASE stage_race_home
			REMOVE_CUTSCENE() // throwing this in to avoid problem with intro repeating.
			SET_ALL_VARS_TO_DEFAULT()
			IF bSkip
				FADE_OUT()
//				WHILE NOT SET_BANK_ROADS_STATUS(FALSE)
//					WAIT(0)
//				ENDWHILE
				CLEAR_AREA(v_park_shoot, 300, TRUE)
				DO_PLAYER_WARP_WITH_LOAD(v_park_hide, f_park_hide)
				SETUP_PEDS_IN_CAR(v_park_hide, f_park_hide)
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
				WHILE NOT IS_PED_READY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					WAIT(0)
				ENDWHILE
				WHILE NOT IS_PED_READY(pedLester)
					WAIT(0)
				ENDWHILE
				IF DOES_ENTITY_EXIST(pedBiker)
					DELETE_PED(pedBiker)
				ENDIF
				WHILE NOT CREATE_BIKER()
					WAIT(0)
				ENDWHILE
				IF DOES_ENTITY_EXIST(pedBiker)
					DELETE_PED(pedBiker)
				ENDIF
				DELETE_BANK_PEDS()
				FORCE_CLEANUP_RESPONSE_TEAM()
				ABANDON_ALL_SCENES()
				CREATE_RESPONSE_TEAM(TRUE)
				DELETE_COP_CAR(cars[2])
				DELETE_COP_CAR(cars[3])
				WHILE NOT CREATE_AMBIENT_BANK_PEDS()
					WAIT(0)
				ENDWHILE
				WHILE NOT START_IG_BANK_OUTRO()
					WAIT(0)
				ENDWHILE
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iOutroSceneId)
					SET_SYNCHRONIZED_SCENE_PHASE(iOutroSceneId, 0.3)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehTrevorBike)
					SET_ENTITY_COORDS_NO_OFFSET(vehTrevorBike, <<-82.5716, 6420.5552, 31.0215>>)
					SET_ENTITY_QUATERNION(vehTrevorBike, 0.0792, -0.1038, 0.5940, 0.7938)
					IF DOES_ENTITY_EXIST(pedBiker)
						DELETE_PED(pedBiker)
					ENDIF
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
					SET_ENTITY_COORDS_NO_OFFSET(vehPlayerCar, <<-89.3137, 6420.1567, 30.9773>>)
					SET_ENTITY_QUATERNION(vehPlayerCar, 0.0010, 0.0036, -0.4005, 0.9163)
				ENDIF
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				IF NOT IS_AUDIO_SCENE_ACTIVE("PS_1_DRIVE_TO_GAS_STATION")
					START_AUDIO_SCENE("PS_1_DRIVE_TO_GAS_STATION")
				ENDIF
			ENDIF
		BREAK
		
		CASE stage_lose_the_cops
			SET_ALL_VARS_TO_DEFAULT()
			IF bSkip
				SETUP_PEDS_IN_CAR(v_park_hide, f_park_hide)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF IS_REPLAY_BEING_SET_UP()
		SWITCH current_mission_stage
			CASE stage_drive_to_bus
				END_REPLAY_SETUP(NULL, VS_DRIVER, FALSE)
			BREAK
			CASE stage_shoot_box
				END_REPLAY_SETUP()
			BREAK
			DEFAULT
				END_REPLAY_SETUP(vehPlayerCar)
			BREAK
		ENDSWITCH
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedLester)
		IF NOT IS_PED_INJURED(pedLester)
			IF sSpeech.PedInfo[1].Index = NULL
				ADD_PED_FOR_DIALOGUE(sSpeech, 1, pedLester, "LESTER")
			ENDIF
		ENDIF
	ELSE
		IF stage <> stage_drive_to_bus
			IF sSpeech.PedInfo[1].Index <> NULL
				REMOVE_PED_FOR_DIALOGUE(sSpeech, 1)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			IF sSpeech.PedInfo[2].Index = NULL
				ADD_PED_FOR_DIALOGUE(sSpeech, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
			ENDIF
		ENDIF
	ELSE
		IF sSpeech.PedInfo[2].Index <> NULL
			REMOVE_PED_FOR_DIALOGUE(sSpeech, 2)
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(sSpeech.PedInfo[0].Index)
		ADD_PED_FOR_DIALOGUE(sSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
	ENDIF
	
	SET_PED_AS_BUDDY(pedLester)
	SET_PED_AS_BUDDY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
	ASSET_MANAGER(stage, FALSE, TRUE)
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	IF current_mission_stage != stage_shoot_box
	AND current_mission_stage != stage_box_reveal_scene
		RESET_CUSTOM_CHASE_HINT_CAM_INTERP_IN_TIME(localChaseHintCamStruct)
		RESET_CUSTOM_CHASE_HINT_CAM_INTERP_OUT_TIME(localChaseHintCamStruct)
	ENDIF
	
	bSkip = FALSE
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		misc procs
// -----------------------------------------------------------------------------------------------------------



FUNC BOOL IS_PLAYER_OK_TO_SHOOT_ALARM()
	RETURN NOT 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-120.703255,6486.523926,35.468407>>,	<<-141.663223,6466.175293,30.688643>>, 9.250000)
	AND NOT 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-98.367554,6455.172852,42.135567>>,		<<-122.319847,6479.520020,30.458498>>, 45.750000)
	AND NOT 	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-112.995781,6484.136230,35.468681>>,	<<-124.835693,6485.979980,30.290354>>, 8.000000)
ENDFUNC

FUNC BOOL HAS_PLAYER_ALERTED_BANK_SECURITY()

	BOOL bRet = FALSE
	
	IF current_mission_stage = stage_drive_to_bus
		IF bankAlarm.bOn
			PRINTLN("BANK FAIL DEBUG: Bank alaram on during drive to bus stop")
			bRet = TRUE
		ENDIF
	ENDIF
	
	IF IS_PLAYER_IN_THE_BANK()
		IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)	
			PRINTLN("BANK FAIL DEBUG: Player armed in the bank")
			bRet = TRUE
		ENDIF
		IF IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
		AND IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
			PRINTLN("BANK FAIL DEBUG: Player fighting or aiming in the bank")
			bRet = TRUE
		ENDIF
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-109.289253,6468.679199,30.545216>>, <<-114.698524,6474.160156,33.929283>>, 1.500000)
			PRINTLN("BANK FAIL DEBUG: Player in the wrong part of the bank")
			bRet = TRUE
		ENDIF
	ENDIF
	
	IF current_mission_stage < stage_hide
		INT iTemp
		REPEAT COUNT_OF(bank_peds) iTemp
			IF NOT IS_PED_INJURED(bank_peds[iTemp])
			
				VECTOR vPedCoords
				vPedCoords = GET_ENTITY_COORDS(bank_peds[iTemp])
				
				IF IS_BULLET_IN_AREA(vPedCoords, 2)
					PRINTLN("BANK FAIL DEBUG: bank ped ", iTemp, " shot at")
					bRet = TRUE
				ENDIF
				
				IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_BIKE, 			vPedCoords, 20)
				OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_CAR, 				vPedCoords, 20)
				OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADE, 			vPedCoords, 20)
				OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADELAUNCHER, 	vPedCoords, 20)
				OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_MOLOTOV, 			vPedCoords, 20)
				OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_BZGAS,			vPedCoords, 20)
				OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_PROGRAMMABLEAR,	vPedCoords, 20)
				OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_ROCKET,			vPedCoords, 20)
					bRet = TRUE
				ENDIF
				
				IF IS_PED_INJURED(bank_peds[iTemp])
					PRINTLN("BANK FAIL DEBUG: bank ped ", iTemp, " is injured")
					bRet = TRUE
				ELSE
					IF IS_PED_RAGDOLL(bank_peds[iTemp])
					OR IS_PED_FLEEING(bank_peds[iTemp])
						PRINTLN("BANK FAIL DEBUG: bank ped ", iTemp, " is fleeing or ragdoll")
						bRet = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF NOT IS_PLAYER_OK_TO_SHOOT_ALARM() 
		WEAPON_TYPE playerWep
		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), playerWep)
		IF IS_PED_SHOOTING(PLAYER_PED_ID())
			PRINTLN("BANK FAIL DEBUG: Player is shooting too soon")
			bRet = TRUE
		ENDIF
	ENDIF
	
	VEHICLE_INDEX vehTemp
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	ELSE
		vehTemp = GET_PLAYERS_LAST_VEHICLE()
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehTemp)
		IF IS_ENTITY_IN_ANGLED_AREA(vehTemp, <<-107.391777,6466.308105,30.634338>>, <<-112.694305,6461.028809,33.968357>>, 4.750000)
			PRINTLN("BANK FAIL DEBUG: Vehicle is in doorway")
			bRet = TRUE
		ENDIF
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
	AND bankAlarm.bOn
	AND current_mission_stage < stage_race_home
		PRINTLN("BANK FAIL DEBUG: Player is wanted and alarm is on")
		bRet = TRUE
	ENDIF
	
	RETURN bRet
	
ENDFUNC

FUNC BOOL EVERYONE_OK(BOOL includeLester = TRUE)
	IF includeLester
		RETURN (IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		AND NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		AND NOT IS_PED_INJURED(pedLester))
	ELSE
		RETURN IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		AND NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL EVERYONE_IN(BOOL includeLester = TRUE)
	IF EVERYONE_OK(includeLester)
		IF includeLester
			RETURN(IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
			AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar)
			AND IS_PED_IN_VEHICLE(pedLester, vehPlayerCar))
		ELSE
			RETURN IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
			AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar)
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC MANAGE_GROUP_STATUS(BLIP_INDEX &dest, BLIP_INDEX &veh, VECTOR v_dest, TEXT_HOLDER &text, BOOL blipDest = TRUE, BOOL bLester = TRUE, BOOL bDrawRoute = TRUE, BOOL bPrintDestText = TRUE)

	IF EVERYONE_OK(bLester)
		IF EVERYONE_IN(bLester)
			IF DOES_BLIP_EXIST(veh)
				IF IS_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()// OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					CLEAR_PRINTS()
				ENDIF
				REMOVE_BLIP(veh)
			ENDIF
			IF blipDest
				IF NOT DOES_BLIP_EXIST(dest)
					dest = CREATE_BLIP_FOR_COORD(v_dest, bDrawRoute)
					IF IS_MESSAGE_BEING_DISPLAYED()
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()// OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						CLEAR_PRINTS()
					ENDIF
				ENDIF
				IF bPrintDestText
					PRINT_GOD_TEXT(text)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				IF blipDest
					IF DOES_BLIP_EXIST(dest)
						REMOVE_BLIP(dest)
					ENDIF
				ENDIF
				IF NOT DOES_BLIP_EXIST(veh)
					IF IS_MESSAGE_BEING_DISPLAYED()
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()// OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						CLEAR_PRINTS()
					ENDIF
					PRINT_GOD_TEXT(gtGetBackIn)
					veh = CREATE_BLIP_FOR_VEHICLE(vehPlayerCar)
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(veh)
					REMOVE_BLIP(veh)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_VECTOR_MAGNITUDE(VECTOR &v, FLOAT m)
	VECTOR n = NORMALISE_VECTOR(v)
	n.x = n.x * m
	n.y = n.y * m
	n.z = n.z * m
	v = n
ENDPROC

PROC RUN_FOCUS_CAM()

	vCurrentFocus += (vTargetFocus - vCurrentFocus)*fInterpVal
	
	IF NOT DOES_ENTITY_EXIST(objDummy)
		REQUEST_MODEL(PROP_LD_TEST_01)
		IF HAS_MODEL_LOADED(PROP_LD_TEST_01)
			objDummy = CREATE_OBJECT(PROP_LD_TEST_01, vCurrentFocus)
			FREEZE_ENTITY_POSITION(objDummy, TRUE)
			SET_ENTITY_COLLISION(objDummy, FALSE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(objDummy)
		SET_ENTITY_COORDS(objDummy, vCurrentFocus)
	ENDIF
	
	CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, objDummy, "CBH_FCUSHLP")
	
	IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
	OR bFocusHelpUsed
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CBH_FCUSHLP")
			IF NOT bFocusHelpCleared
				CLEAR_HELP(FALSE)
				bFocusHelpCleared = TRUE
			ENDIF
		ELSE
			IF NOT bFocusHelpUsed
				IF bFocusHelpPrinted 
					bFocusHelpPrinted = FALSE
				ENDIF
				IF bFocusHelpCleared
					bFocusHelpCleared = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

BOOL bConvStarted = FALSE
BOOL bConvOnHold = FALSE
TEXT_LABEL_23 ConvResumeLabel = ""

PROC MANAGE_CONVERSATION(STRING label, BOOL bCanPlay)
	IF bCanPlay
		IF NOT bConvStarted
			IF CREATE_CONVERSATION(sSpeech, sBlockId, label, CONV_PRIORITY_HIGH)
				bConvOnHold = FALSE
				bConvStarted = TRUE
			ENDIF
		ENDIF
		IF bConvOnHold
			IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, sBlockId, label, ConvResumeLabel, CONV_PRIORITY_HIGH)
				bConvOnHold = FALSE
			ENDIF
		ENDIF
	ENDIF
	IF NOT bCanPlay
		IF NOT bConvOnHold
			ConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
			KILL_FACE_TO_FACE_CONVERSATION()
			bConvOnHold = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_HIDE_CONVERSATION(STRING label, BOOL bCanPlay, INT iCurrentTime, INT iLastChance, INT iNoRestarts)
	IF bCanPlay
		IF NOT bConvStarted
			IF iCurrentTime < iLastChance
				IF NOT IS_MESSAGE_BEING_DISPLAYED()// OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					IF CREATE_CONVERSATION(sSpeech, sBlockId, label, CONV_PRIORITY_HIGH)
						bConvOnHold = FALSE
						bConvStarted = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF bConvOnHold
			IF iCurrentTime < iNoRestarts	
				IF NOT IS_MESSAGE_BEING_DISPLAYED()// OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, sBlockId, label, ConvResumeLabel, CONV_PRIORITY_HIGH)
						bConvOnHold = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF NOT bCanPlay
		IF NOT bConvOnHold
			ConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
			KILL_FACE_TO_FACE_CONVERSATION()
			bConvOnHold = TRUE
		ENDIF
	ENDIF
ENDPROC

// ===========================================================================================================
//		MISSION STAGE PROCEDURES
// ===========================================================================================================
BOOL  bLine = FALSE
PROC LOSE_THE_COPS()

	IF current_mission_stage <> stage_lose_the_cops
	
		CLEAR_PRINTS()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		KILL_ANY_CONVERSATION()
		IF DOES_BLIP_EXIST(blipDriveTo)
			REMOVE_BLIP(blipDriveTo)
		ENDIF
		IF DOES_BLIP_EXIST(blipMichael)
			SET_BLIP_ALPHA(blipMichael, 0)
		ENDIF
		IF DOES_BLIP_EXIST(blipLester)
			SET_BLIP_ALPHA(blipLester, 0)
		ENDIF
		IF DOES_BLIP_EXIST(blipAlarm)
			SET_BLIP_ALPHA(blipAlarm, 0)
		ENDIF
		CLEAR_GPS_MULTI_ROUTE()
		CLEAR_GPS_CUSTOM_ROUTE()
		stage_save = current_mission_stage
		current_mission_stage = stage_lose_the_cops
		bLine = FALSE	
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
			IF DOES_BLIP_EXIST(blipVehicle)
				REMOVE_BLIP(blipVehicle)
			ENDIF
			IF NOT bLine
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				AND IS_PED_SITTING_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar)
				AND NOT IS_MESSAGE_BEING_DISPLAYED()
					IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_WANTED", CONV_PRIORITY_HIGH)
						bLine = TRUE
					ENDIF
				ENDIF
			ENDIF
			PRINT_GOD_TEXT(gtLoseCops)
		ELSE
			IF IS_MESSAGE_BEING_DISPLAYED()
				CLEAR_PRINTS()
			ENDIF
		ENDIF
	ENDIF
		
	IF stage_save = stage_drive_to_bus
		IF HAS_PED_BEEN_ABANDONED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], gtGoBackToTrev)
			CLEAR_PRINTS()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			Mission_Failed("CBH_AB2TREV")
		ENDIF
	ELIF stage_save <> stage_race_home
		IF HAS_PED_BEEN_ABANDONED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], gtGoBackToBoth)
			CLEAR_PRINTS()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			Mission_Failed("CBH_AB2BOTH")
		ENDIF
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
		bLine = FALSE
		IF DOES_BLIP_EXIST(blipDriveTo)
			SET_BLIP_ROUTE(blipDriveTo, TRUE)
		ENDIF
		IF DOES_BLIP_EXIST(blipMichael)
			SET_BLIP_ALPHA(blipMichael, 255)
		ENDIF
		IF DOES_BLIP_EXIST(blipLester)
			SET_BLIP_ALPHA(blipLester, 255)
		ENDIF
		IF DOES_BLIP_EXIST(blipAlarm)
			SET_BLIP_ALPHA(blipAlarm, 255)
		ENDIF
		SET_MODEL_AS_NO_LONGER_NEEDED(SHERIFF)
		SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)
		current_mission_stage = stage_save
		gtLoseCops.played = FALSE
	ELSE
		IF bankAlarm.bOn
			Mission_Failed("CBH_COPFAIL")
		ENDIF
	ENDIF
	
ENDPROC

BOOL bEngineOff = FALSE
BOOL bStartedAsTrevor
INT iMethlabInnerDoor

PROC RUN_INTRO_CUTSCENE()

	SWITCH iStageProgress
	
		CASE 0
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				REQUEST_CUTSCENE("rbhs_INT")
			ELSE
				REQUEST_CUTSCENE("rbhs_INT")
				//REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("rbhs_INT", CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5 | CS_SECTION_6 | CS_SECTION_7 )
			ENDIF
			PRINTLN("requesting scene")
			iStageProgress++
		BREAK
		
		CASE 1
			iMethlabInnerDoor = GET_HASH_KEY("SIDE_DOOR_METHLAB")
			ADD_DOOR_TO_SYSTEM(iMethlabInnerDoor, V_ILEV_METHDOORSCUFF, <<1392.7399, 3611.3672, 39.0919>>)
			DOOR_SYSTEM_SET_DOOR_STATE(iMethlabInnerDoor, DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
			DOOR_SYSTEM_SET_OPEN_RATIO(iMethlabInnerDoor, 0)
			iStageProgress++
			PRINTLN("DOOR GRABBED AND SHUT")
		BREAK
		
		CASE 2
		
			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			
				IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE("rbhs_int")
				
					PRINTLN("Cutscene is loaded and awating player out of vehicle")
					
					IF NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					
						MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_RURAL_1)
						SET_ROADS_IN_AREA(v_car_start + <<500,500,500>>, v_car_start - <<500,500,500>>, FALSE)
						PRINTLN("scene loaded")
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
							SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
							sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = g_sTriggerSceneAssets.ped[0]
							g_sTriggerSceneAssets.ped[0] = NULL
						ENDIF
						
						IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PLAYER_TWO)
						ELIF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						CLEAR_AREA_OF_COPS(v_car_start,500)
						
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							START_CUTSCENE(CUTSCENE_PLAYER_FP_FLASH_MICHAEL)
						ELSE
							START_CUTSCENE()
						ENDIF
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							#IF IS_DEBUG_BUILD 
								PRINTLN("\n\n\nSTARTED AS TREVOR\n\n\n")
							#ENDIF
							bStartedAsTrevor = TRUE
						ENDIF
						
						ASSET_MANAGER(stage_drive_to_bus, TRUE, FALSE)
						REQUEST_ADDITIONAL_TEXT("BANKS1", MISSION_TEXT_SLOT)
						REQUEST_ADDITIONAL_TEXT(sBlockId, MISSION_DIALOGUE_TEXT_SLOT)
						REQUEST_MODEL(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
						iStageProgress++
						
					ELSE
					
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) <> PERFORMING_TASK
							TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
							PRINTLN("Tasking player out of vehicle")
						ENDIF
						
					ENDIF
					
				ELSE
				
					PRINTLN("Removing scene as wrong scene is loaded")
					REMOVE_CUTSCENE()
					iStageProgress = 0
					
				ENDIF
				
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTLN("Loading scene RBHS_INT")
				#ENDIF
			ENDIF
			
		BREAK
		
		CASE 3
		
			REQUEST_VEHICLE_ASSET(PREMIER)
			REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 300)
			
			VEHICLE_INDEX vehTemp
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				PRINTLN("PLAYER IS IN VEHICLE")
			ELSE
				vehTemp = GET_LAST_DRIVEN_VEHICLE()
				PRINTLN("PLAYER IS NOT IN VEHICLE")
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehTemp)
			AND IS_VEHICLE_DRIVEABLE(vehTemp)
				IF NOT IS_VEHICLE_MODEL(vehTemp, TAXI)
					IF VDIST(GET_ENTITY_COORDS(vehTemp), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 50
						PRINTLN("PLAYER VEHICLE IS NEARBY")
						SET_ENTITY_AS_MISSION_ENTITY(vehTemp, TRUE, TRUE)
						#IF IS_DEBUG_BUILD
							PRINTLN("Vehicle belongs to: ", GET_PLAYER_PED_PERSONAL_VEHICLE_BELONGS_TO(vehTemp))
						#ENDIF
						IF GET_PLAYER_PED_PERSONAL_VEHICLE_BELONGS_TO(vehTemp) = CHAR_MICHAEL
						OR IS_BIG_VEHICLE(vehTemp)
							DELETE_VEHICLE(vehTemp)
							PRINTLN("Deleting as this is the player's vehicle")
						ELSE
							SET_MISSION_VEHICLE_GEN_VEHICLE(vehTemp, <<1416.6530, 3622.3201, 33.8648>>, 20.6293)
							SET_ENTITY_COORDS(vehTemp, <<1416.6530, 3622.3201, 33.8648>>)
							SET_ENTITY_HEADING(vehTemp, 20.6293)
							PRINTLN("SETTING PLAYER VEH AS veh gen")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			CLEAR_AREA(v_player_start, 25.0, TRUE)
			CLEAR_AREA_OF_OBJECTS(<<1391.6508, 3607.2688, 37.9419>>, 10, CLEAROBJ_FLAG_FORCE)
			CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MISSION_VEH)
			//RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1387.578125,3591.521729,32.922253>>, <<1416.410034,3601.590332,40.911201>>, 38.500000, <<1416.6530, 3622.3201, 33.8648>>, 20.6293, <<6,6,10>>)
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
			
			iStageProgress++
			
		BREAK
		
		CASE 4
		
			IF IS_CUTSCENE_ACTIVE()
				IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				AND DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor"))
					sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = GET_PED_INDEX_FROM_ENTITY_INDEX((GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor")))
				ENDIF
			ENDIF
			
			SETUP_CAR(v_car_start, f_car_start)
			
			IF NOT bEngineOff
				IF DOES_ENTITY_EXIST(vehPlayerCar)
				AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
					SET_VEHICLE_ENGINE_ON(vehPlayerCar, FALSE, TRUE)
					bEngineOff = TRUE
				ENDIF
			ENDIF
			
			SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
			
			IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					#IF IS_DEBUG_BUILD 
						PRINTLN("\n\n HIT EXIT STATE FOR MICHAEL \n\n\n")
					#ENDIF
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), v_player_start)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), f_player_start)
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2500, 10)
					//TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), v_car_start, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
					//TASK_ENTER_VEHICLE(PLAYER_PED_ID(), vehPlayerCar, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK, ECF_USE_LEFT_ENTRY)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				
					#IF IS_DEBUG_BUILD 
						PRINTLN("\n\n HIT EXIT STATE FOR TREVOR \n\n\n")
					#ENDIF
					
					IF bStartedAsTrevor
						IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							#IF IS_DEBUG_BUILD 
								PRINTLN("\n\n CREATING TREVOR\n\n\n")
							#ENDIF
							CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, v_trevor_start, f_trevor_start, FALSE)
						ENDIF
						#IF IS_DEBUG_BUILD 
							PRINTLN("\n\n\SETTING PED AS MICHAEL\n\n\n")
						#ENDIF
						SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL, FALSE)
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), v_player_start)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), f_player_start)
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2500, 10)
					ENDIF
					
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], v_trevor_start)
						SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], f_trevor_start)
						CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
						//TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), v_car_start, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
						SET_PED_RESET_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PRF_UseFastEnterExitVehicleRates, TRUE)
					ENDIF
					
				ENDIF
				
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			IF NOT IS_CUTSCENE_ACTIVE()
			
				IF NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
				OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
					WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
					OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
						printstring("LOADING TEXT\n")
						WAIT(0)
					ENDWHILE
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					SET_PED_AS_ENEMY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
					SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
					SET_PED_CAN_BE_DRAGGED_OUT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], RELGROUPHASH_PLAYER)
				ENDIF
				
				REPLAY_STOP_EVENT()
				
				CLEAR_AREA(v_car_start, 20, TRUE)
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MISSION_VEH)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				//cutscene finished, back to game
				bCutsceneLoaded = FALSE
				REMOVE_CUTSCENE()
				GO_TO_STAGE(stage_drive_to_bus)
				
			ENDIF
			
			#IF IS_DEBUG_BUILD
				DEBUG_CUTSCENE_CONTROL()
			#ENDIF
			
		BREAK
	ENDSWITCH
	
	SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
	
ENDPROC

PROC BUILD_SIMPLE_CUSTOM_GPS_ROUTE(VECTOR &vRoute[], INT &iCurrentClosest)

	INT i = 0
	INT iClosestNode = -1
	FLOAT fClosestDist = 0.0
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	//Get the closest node to the player
	REPEAT (COUNT_OF(vRoute) - 1) i
		FLOAT fDist = VDIST2(vPlayerPos, vRoute[i])
		IF iClosestNode = -1 OR fDist < fClosestDist
			iClosestNode = i
			fClosestDist = fDist
		ENDIF
		#IF IS_DEBUG_BUILD
			IF i = iCurrentClosest
				DRAW_DEBUG_SPHERE(vRoute[i], 10, 0, 255, 0, 100)
			ELSE
				DRAW_DEBUG_SPHERE(vRoute[i], 10, 0, 0, 255, 100)
			ENDIF
		#ENDIF
	ENDREPEAT
	
	//If the closest node is closer to the next node than the player then it's the first node in the route, otherwise it's the next node.
	IF VDIST2(vPlayerPos, vRoute[iClosestNode+1]) < VDIST2(vRoute[iClosestNode], vRoute[iClosestNode+1])
		iClosestNode = iClosestNode + 1
	ENDIF
	
	IF iCurrentClosest != iClosestNode
	OR (bWantedGPS AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)	//Redraw the route as it was cleared with wanted
		IF iCurrentClosest >= 0 AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRoute[iCurrentClosest]) > 5*5
		OR (bWantedGPS AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0)	//Redraw the route as it was cleared with wanted
			
			PRINTLN("DRAWING GPS ROUTE!")
			
			CLEAR_GPS_MULTI_ROUTE()
			//Build the route starting from the node calculated above.
			START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE)
			
			FOR i = iClosestNode TO COUNT_OF(vRoute) - 1
				ADD_POINT_TO_GPS_MULTI_ROUTE(vRoute[i])
			ENDFOR
			
			ADD_POINT_TO_GPS_MULTI_ROUTE(vRoute[COUNT_OF(vRoute) - 1])
			SET_GPS_MULTI_ROUTE_RENDER(TRUE)
			PRINTLN("GPS DEBUG: Refreshing GPS route")
			
			bWantedGPS = FALSE
		ENDIF
		PRINTLN("GPS DEBUG: Updating GPS closest node")
		iCurrentClosest = iClosestNode
	ENDIF
	
ENDPROC

INT iSkipToHour
INT iSkipToMinute
INT iCamScene
BOOL bStopCOnv
BOOL bWaitPrinted
BOOL bCompsGrabbed
BOOL bPlayerWalk
BOOL bGoToConvDone 
BOOL bGoToTextDone 
STRING sWalkRec = "palsetupwalk"

PROC DRIVE_TO_BUS_STOP()

	HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
	
		IF NOT bParked
		
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_busstop_car_park) < 200*200
				SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
			ENDIF
			
			IF iStageProgress > 1
				IF iStageProgress < 4
					MANAGE_GROUP_STATUS(blipDriveTo, blipVehicle, v_bank_door, gtDriveToPaleto, TRUE, FALSE, VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_busstop_car_park) < 750*750)
				ELSE
					MANAGE_GROUP_STATUS(blipDriveTo, blipVehicle, v_busstop_car_park, gtDriveToPaleto, TRUE, FALSE, VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_busstop_car_park) < 750*750)
				ENDIF
				
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					PRINTLN("BUILDING GPS ROUTE STAGE: ", iStageProgress)
					BUILD_SIMPLE_CUSTOM_GPS_ROUTE(vGPSDriveToPaleto, iPaletoGPSPoint)
					bRouteDone = TRUE
				ELSE
					IF bRouteDone
						CLEAR_GPS_MULTI_ROUTE()
						iPaletoGPSPoint = COUNT_OF(vGPSDriveToPaleto) - 1
					ENDIF
				ENDIF
				
				//Flag for setting the player has been wanted
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					PRINTLN("PLAYER HAS GAINED WANTED bWantedGPS = TRUE")
					bWantedGPS = TRUE
				ENDIF
			ENDIF
			
			IF iStageProgress < 2
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					SET_PED_RESET_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PRF_SearchForClosestDoor, TRUE)
				ENDIF
			ENDIF
			
			SWITCH iStageProgress
			
				CASE 0
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])				
					AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
					
						bPlayerWalk = TRUE
						
						//TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar, DEFAULT_TIME_BEFORE_WARP * 3, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
						SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PCF_OpenDoorArmIK, TRUE)
						
						IF IS_SCREEN_FADED_OUT()
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
						ENDIF
						
						SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
						
						REQUEST_WAYPOINT_RECORDING(sWalkRec)
						
						SETTIMERA(0)
						SETTIMERB(0)
						
						vGPSDriveToPaleto[0] = <<2356.3787, 5864.7744, 46.1486>>
						vGPSDriveToPaleto[1] = <<230.8756, 6569.0801, 30.6632>>
						vGPSDriveToPaleto[2] = v_bank_door
						
						iPaletoGPSPoint = COUNT_OF(vGPSDriveToPaleto) - 1
						
						bRouteDone = FALSE
						iCamScene = -1
						bStopCOnv = FALSE
						bCompsGrabbed = FALSE
						bGoToConvDone = FALSE
						bGoToTextDone = FALSE
						
						REPLAY_RECORD_BACK_FOR_TIME(1.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
						
						iStageProgress++
					ENDIF
				BREAK
				
				CASE 1
					
					//force fade in
					IF NOT IS_SCREEN_FADED_IN()
					AND NOT IS_SCREEN_FADING_IN()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					
					
					// handle player walk from intro
					IF bPlayerWalk
						SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
						IF iStageProgress != 0
							INT iControlVal1,iControlVal2,iControlVal3
							GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iControlVal1, iControlVal2, iControlVal3, iControlVal3)
							IF ABSI(iControlVal1) + ABSI(iControlVal2) > 20
							OR TIMERB() > 2000
								bPlayerWalk = FALSE
							ENDIF
						ENDIF
					ENDIF
					
					//keep trevor walking behind the player so he won't bump
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					AND GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_ENTER_VEHICLE) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_ENTER_VEHICLE) != WAITING_TO_START_TASK 
//					AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					AND NOT IS_PED_IN_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
					AND NOT DOES_BLIP_EXIST(blipBuddy)
						IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							IF GET_IS_WAYPOINT_RECORDING_LOADED(sWalkRec)
								TASK_FOLLOW_WAYPOINT_RECORDING(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], sWalkRec, 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
							ENDIF
							IF IS_ENTITY_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar, << 3, 3, 3 >>)
								CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK)
							ENDIF
						ELSE
							INT iPlayerClosest
							INT iTrevorClosest
							VECTOR vPlayerNode
							WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(sWalkRec, GET_ENTITY_COORDS(PLAYER_PED_ID()), iPlayerClosest)
							WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(sWalkRec, GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]), iTrevorClosest)
							WAYPOINT_RECORDING_GET_COORD(sWalkRec, iPlayerClosest, vPlayerNode)
							IF NOT WAYPOINT_PLAYBACK_GET_IS_PAUSED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayerNode) < 6.25
								AND IS_ENTITY_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_PED_ID(), <<3,3,3>>)
								AND iPlayerClosest - iTrevorClosest < 3
								AND iPlayerClosest > iTrevorClosest
									WAYPOINT_PLAYBACK_PAUSE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
								ENDIF
							ELSE
								IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPlayerNode) > 9
								OR NOT IS_ENTITY_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_PED_ID(), <<3,3,3>>)
								OR iPlayerClosest - iTrevorClosest > 3
								OR iPlayerClosest < iTrevorClosest
									WAYPOINT_PLAYBACK_RESUME(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
								ENDIF
							ENDIF						
							IF GET_PED_WAYPOINT_PROGRESS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) > 28
								CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK)
							ENDIF
						ENDIF
					ENDIF
					
					//Do Trevor's "go to paleto" dialogue followed by the god text.
					IF NOT bGoToConvDone
						IF IS_SCREEN_FADED_IN()
						AND NOT IS_SCREEN_FADING_IN()
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_DRV1", CONV_PRIORITY_HIGH)
								bGoToConvDone = TRUE
							ENDIF
						ENDIF
					ELIF NOT bGoToTextDone
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
								PRINT_NOW("CBH_GOTOCAR", DEFAULT_GOD_TEXT_TIME, 1)
								bGoToTextDone = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					
					//handle player and buddy getting into vehicle and blips
					IF IS_SCREEN_FADED_IN()
					
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
						
							IF DOES_BLIP_EXIST(blipBuddy)
								IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar)
								OR NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
									REMOVE_BLIP(blipBuddy)
								ENDIF
							ELSE
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
								AND NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar)
									IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) > 3
										TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_SPRINT)
									ENDIF
									blipBuddy = CREATE_BLIP_FOR_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								ENDIF
							ENDIF
							
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
//								CLEAR_PRINTS()
								IF NOT IS_AUDIO_SCENE_ACTIVE("PS_1_DRIVE_TO_PALETO")
									START_AUDIO_SCENE("PS_1_DRIVE_TO_PALETO")
								ENDIF
								IF DOES_BLIP_EXIST(blipVehicle)
									REMOVE_BLIP(blipVehicle)
								ENDIF
								IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar)
									IF DOES_BLIP_EXIST(blipBuddy)
										REMOVE_BLIP(blipBuddy)
									ENDIF
									REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//									SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
									SETTIMERA(0)
									INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(RH1_LEISURELY_DRIVE)
									REMOVE_WAYPOINT_RECORDING(sWalkRec)
									iStageProgress++
								ELSE
									IF NOT bWaitPrinted
										IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) > 3
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											AND NOT IS_MESSAGE_BEING_DISPLAYED()
												PRINT("CBH_BUDSOUT", DEFAULT_GOD_TEXT_TIME, 1)
												bWaitPrinted = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT DOES_BLIP_EXIST(blipVehicle)
									blipVehicle = CREATE_BLIP_FOR_VEHICLE(vehPlayerCar)
								ENDIF
								IF DOES_BLIP_EXIST(blipBuddy)
									REMOVE_BLIP(blipBuddy)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 2
					IF TIMERA() > 7000
					AND EVERYONE_IN(FALSE)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_CALL", CONV_PRIORITY_HIGH)
							iStageProgress++
						ENDIF
					ENDIF
				BREAK
				
				CASE 3
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF IS_REPLAY_IN_PROGRESS()
							IF PLAYER_CALL_CHAR_CELLPHONE(sSpeech, CHAR_LESTER, sBlockId, "RBS1_PHONE", conv_priority_very_high)
								SETTIMERA(0)
								iStageProgress++
							ENDIF
						ELSE
							IF PLAYER_CALL_CHAR_CELLPHONE(sSpeech, CHAR_LESTER, sBlockId, "RBS1_PHONE2", conv_priority_very_high)
								SETTIMERA(0)
								iStageProgress++
							ENDIF						
						ENDIF
					ENDIF
				BREAK
				
				CASE 4
					IF EVERYONE_IN(FALSE)
					AND EVERYONE_OK(FALSE)
						IF HAS_CELLPHONE_CALL_FINISHED()
							SETTIMERA(0)
							IF DOES_BLIP_EXIST(blipDriveTo)
								REMOVE_BLIP(blipDriveTo)
								CLEAR_GPS_MULTI_ROUTE()
								vGPSDriveToPaleto[0] = <<2356.3787, 5864.7744, 46.1486>>
								vGPSDriveToPaleto[1] = <<230.8756, 6569.0801, 30.6632>>
								vGPSDriveToPaleto[2] = v_busstop_car_park
								bRouteDone = FALSE
								iPaletoGPSPoint = COUNT_OF(vGPSDriveToPaleto) - 1
							ENDIF
							iStageProgress++
						ENDIF
					ENDIF
				BREAK
				
				CASE 5
					IF TIMERA() > 3000
						SET_ROADS_BACK_TO_ORIGINAL(v_car_start + <<500,500,500>>, v_car_start - <<500,500,500>>)
						iStageProgress++
					ENDIF
				BREAK
				
				CASE 6
					IF IS_REPLAY_IN_PROGRESS()
						MANAGE_CONVERSATION("RBS1_DRV2", IS_VEHICLE_DRIVEABLE(vehPlayerCar) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar))
					ELSE
						MANAGE_CONVERSATION("RBS1_DRV4", IS_VEHICLE_DRIVEABLE(vehPlayerCar) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar))
					ENDIF
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT bConvOnHold
						bConvStarted = FALSE
						iStageProgress++
					ENDIF
				BREAK
				
				CASE 7	
					IF IS_REPLAY_IN_PROGRESS()
						MANAGE_CONVERSATION("RBS1_DRV2b", IS_VEHICLE_DRIVEABLE(vehPlayerCar) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar))
					ELSE
						MANAGE_CONVERSATION("RBS1_DRV4b", IS_VEHICLE_DRIVEABLE(vehPlayerCar) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar))
					ENDIF
				BREAK
				
			ENDSWITCH
			
			IF EVERYONE_IN(FALSE)
			AND EVERYONE_OK(FALSE)
			AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<v_busstop_car_park.x, v_busstop_car_park.y, 30.24>> , g_vAnyMeansLocate, TRUE)
			AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-351.528198,6189.989258,28.294226>>, <<-343.888580,6197.934082,34.295494>>, 7.000000)
			AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
			AND IS_ENTITY_UPRIGHT(vehPlayerCar)
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
				KILL_FACE_TO_FACE_CONVERSATION()
				CLEAR_GPS_MULTI_ROUTE()
				REQUEST_AMBIENT_AUDIO_BANK("TIME_LAPSE")
				iStageProgress = 0
				bParked = TRUE
				PRINTLN("PARKED")
			ENDIF
			
		ELSE
		
			IF NOT bStopCOnv
				IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_DRV3", CONV_PRIORITY_HIGH)
					bStopCOnv = TRUE
				ENDIF
			ENDIF
			
			SWITCH iStageProgress
			
				CASE 0
					IF EVERYONE_IN(FALSE)
					AND EVERYONE_OK(FALSE)
						IF DOES_BLIP_EXIST(blipDriveTo)
							REMOVE_BLIP(blipDriveTo)
						ENDIF
						iStageProgress++
					ENDIF
				BREAK
				
				CASE 1
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayerCar, 6,1)
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						SETTIMERA(0)
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						SET_VEHICLE_ENGINE_ON(vehPlayerCar, FALSE, FALSE)
						SET_VEHICLE_INTERIORLIGHT(vehPlayerCar, FALSE)
						SET_WANTED_LEVEL_MULTIPLIER(0.0)
						iStageProgress++
					ENDIF
				BREAK
				
				CASE 2
					//NEW_LOAD_SCENE_START(<<-348.4079, 6197.1118, 31.1131>>, <<-1.8603, 0.0000, -68.7741>>, 100.0)
					//WHILE NOT IS_NEW_LOAD_SCENE_LOADED() AND TIMERA() < 3000
					//	WAIT(0)
					//ENDWHILE
					//CLEAR_FOCUS()
					//NEW_LOAD_SCENE_STOP()
					iStageProgress++
				BREAK
				
				CASE 3
					IF bStopCOnv
					AND TIMERA() > 2000
						IF NOT DOES_CAM_EXIST(camLapse)
							iCamScene = CREATE_SYNCHRONIZED_SCENE(<<-330.690, 6190.950, 30.490>>, <<0,0,0>>)
							camLapse = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
							PLAY_SYNCHRONIZED_CAM_ANIM(camLapse, iCamScene, "not_for_cutscene_-_timelapse_lead_in_cam", "missheistpaletoscoresetup")
							CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.5)
							CASCADE_SHADOWS_ENABLE_FREEZER(FALSE)
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							SET_ENTITY_COORDS_NO_OFFSET(vehPlayerCar, <<-348.9985, 6194.5923, 30.7398>>)
							SET_ENTITY_QUATERNION(vehPlayerCar, -0.0211, 0.0095, 0.9216, 0.3876)
							FREEZE_ENTITY_POSITION(vehPlayerCar, TRUE)
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
							SET_WIDESCREEN_BORDERS(TRUE, 0)
							START_AUDIO_SCENE("PS_1_BUS_ARRIVES")
							
							REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
							
						ELSE
							SETTIMERA(0)
							REQUEST_MODEL(BUS)
							REQUEST_MODEL(S_M_M_LSMETRO_01)
							iStageProgress++
						ENDIF
					ENDIF
				BREAK
				
				CASE 4
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iCamScene)
						PRINTLN(GET_SYNCHRONIZED_SCENE_PHASE(iCamScene))
					ENDIF
					
					IF ((IS_SYNCHRONIZED_SCENE_RUNNING(iCamScene) AND GET_SYNCHRONIZED_SCENE_PHASE(iCamScene) > 0.99) OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iCamScene))
					AND REQUEST_AMBIENT_AUDIO_BANK("TIME_LAPSE")
					AND DOES_ENTITY_EXIST(pedLester)
					
						SET_ENTITY_COLLISION(pedLester, TRUE)
						SET_ENTITY_VISIBLE(pedLester, TRUE)
						FREEZE_ENTITY_POSITION(pedLester, FALSE)
						
						REMOVE_ANIM_DICT("missheistpaletoscoresetup")
						REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						sTimelapse.splineCamera = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
						ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, <<-351.9567, 6192.0483, 34.7371>>, <<21.6144, 0.0000, -126.3661>>, 5000)
						ADD_CAM_SPLINE_NODE(sTimelapse.splineCamera, <<-351.9567, 6192.0483, 34.7371>>, <<21.6144, 0.0000, -126.3661>>, 5000)
						SET_CAM_FOV(sTimelapse.splineCamera, 39.0)
						SET_CAM_ACTIVE(sTimelapse.splineCamera, TRUE)
						PLAY_SOUND_FRONTEND(-1, "TIME_LAPSE_MASTER")
						sTimelapse.sStartTimeOfDay = GET_CURRENT_TIMEOFDAY()
						
						CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(FALSE)
						SET_TODS_CUTSCENE_RUNNING(sTimelapse, TRUE, TRUE)
						iSkipToHour = GET_TIMEOFDAY_HOUR(sTimelapse.sStartTimeOfDay)+2
						iSkipToMinute = GET_TIMEOFDAY_MINUTE(sTimelapse.sStartTimeOfDay)
						IF iSkipToHour > 18
						OR iSkipToHour < 6
							iSkipToHour = 8
						ENDIF
						IF IS_AUDIO_SCENE_ACTIVE("PS_1_DRIVE_TO_PALETO")
							STOP_AUDIO_SCENE("PS_1_DRIVE_TO_PALETO")
						ENDIF
						iStageProgress++
						
					ENDIF
					
				BREAK
				
				CASE 5
					PRINTLN("SKIPPING TO ", iSkipToHour)
					CREATE_BUS(<<-397.9000, 6134.1348, 31.5204>>, -45.281322)
					IF SKIP_TO_TIME_DURING_SPLINE_CAMERA(iSkipToHour, iSkipToMinute,  "EXTRASUNNY", "cirrocumulus", sTimelapse, -1, 0, FALSE)
						REPLAY_STOP_EVENT()
						FREEZE_ENTITY_POSITION(vehPlayerCar, FALSE)
						RELEASE_AMBIENT_AUDIO_BANK()
						GO_TO_STAGE(stage_bus_stop_scene)
					ENDIF
				BREAK
				
			ENDSWITCH
			
			IF GET_IS_VEHICLE_ENGINE_RUNNING(vehPlayerCar)
				PRINTLN("Engine on")
			ELSE
				PRINTLN("Engine off")
			ENDIF
			
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				bParked = FALSE
				iStageProgress = 7
			ENDIF
			
		ENDIF
		
		IF HAS_PED_BEEN_ABANDONED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], gtGoBackToTrev)
			CLEAR_PRINTS()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			Mission_Failed("CBH_AB2TREV")
		ENDIF
		
	ENDIF
	
	IF NOT bCutscenePreStreamed
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_busstop_car_park) < 150*150
		AND DOES_ENTITY_EXIST(pedLester)
			REQUEST_ANIM_DICT("missheistpaletoscoresetup")
			PRE_STREAM_CUTSCENE_AT_DISTANCE("rbhs_mcs_1", v_busstop_car_park, DEFAULT_CUTSCENE_LOAD_DIST, bCutscenePreStreamed)
			ASSET_MANAGER(stage_bus_stop_scene, TRUE, FALSE)
		ENDIF
	ENDIF
	
	IF bCutscenePreStreamed
	
		IF NOT bCompsGrabbed
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())
				
				IF DOES_ENTITY_EXIST(pedLester)
				AND NOT IS_PED_INJURED(pedLester)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Lester", pedLester)
				ENDIF
								
				bCompsGrabbed = TRUE
			ENDIF
		ENDIF
		
		IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_busstop_car_park) > DEFAULT_CUTSCENE_UNLOAD_DIST
			REMOVE_CUTSCENE()
			bCutscenePreStreamed = FALSE
			bCompsGrabbed = FALSE
		ENDIF
		
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(pedLester)
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_busstop_car_park) < 200*200
			IF CREATE_NPC_PED_ON_FOOT(pedLester, CHAR_LESTER, <<-317.9068, 6195.8940, 36.0577>>, 72.1587)
				SET_LESTER_VARIATIONS()
				FREEZE_ENTITY_POSITION(pedLester, TRUE)
				SET_ENTITY_COLLISION(pedLester, FALSE)
				SET_ENTITY_VISIBLE(pedLester, FALSE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

BOOL bClarified = FALSE
BOOL bSpecified = FALSE
CAM_VIEW_MODE eSavedMode = NUM_CAM_VIEW_MODES

PROC DRIVE_TO_BANK()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_THIRD_PERSON_NEAR
		AND GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_THIRD_PERSON_FAR
		AND GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_THIRD_PERSON_MEDIUM
			KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		ENDIF
	ENDIF
	
	HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
	
	IF EVERYONE_OK()
	
		SWITCH iStageProgress
		
			CASE 0
				bConvStarted = FALSE
				bConvOnHold = FALSE
				bClarified = FALSE
				bSpecified = FALSE
				ConvResumeLabel = ""
				
				IF IS_VEHICLE_DRIVEABLE(vehBus)
				AND NOT IS_PED_INJURED(pedBusDriver)
					SEQUENCE_INDEX seqTemp
					OPEN_SEQUENCE_TASK(seqTemp)
					TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehBus, << -222.1229, 6146.4180, 30.2385 >>, 7.0, DRIVINGSTYLE_ACCURATE, BUS, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 50.0, 10.0)
					TASK_VEHICLE_DRIVE_WANDER(NULL, vehBus, 10.0, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
					CLOSE_SEQUENCE_TASK(seqTemp)
					TASK_PERFORM_SEQUENCE(pedBusDriver, seqTemp)
					CLEAR_SEQUENCE_TASK(seqTemp)
				ENDIF
				
				CLEAR_AREA_OF_PEDS(<<-117.54, 6484.41, 30.46>>, 50)
				SET_PED_PATHS_IN_AREA(<<-135.309174,6479.614746,32.652996>>,<<11.750000,6.750000,4.500000>>, FALSE)
				SET_PED_PATHS_IN_AREA(<<-134.387207,6492.855469,30.955853>>,<<11.750000,6.750000,4.500000>>, FALSE)
				SET_PED_PATHS_IN_AREA(<<-134.505310,6507.014160,30.966684>>,<<11.750000,6.750000,4.500000>>, FALSE)
				SET_PED_PATHS_IN_AREA(<<-135.343842,6520.587402,30.748960>>,<<11.750000,6.750000,4.500000>>, FALSE)
				SET_PED_PATHS_IN_AREA(<<-116.302612,6492.044922,33.905464>>,<<18.000000,19.250000,5.750000>>, FALSE)
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("PS_1_DRIVE_TO_BANK")
					START_AUDIO_SCENE("PS_1_DRIVE_TO_BANK")
				ENDIF
				SETTIMERA(0)
				
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				
				iStageProgress++
			BREAK
			
			CASE 1
				IF EVERYONE_IN()
				AND EVERYONE_OK()
					FADE_IN()
					IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_TOGO", CONV_PRIORITY_HIGH)
						iStageProgress++
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				MANAGE_GROUP_STATUS(blipDriveTo, blipVehicle, v_park_lot, gtDriveToBank, TRUE, TRUE, TRUE, FALSE)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRINT_GOD_TEXT(gtDriveToBank)
				ENDIF
				IF TIMERA()>5000
				AND gtDriveToBank.played
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 3
				MANAGE_GROUP_STATUS(blipDriveTo, blipVehicle, v_park_lot, gtDriveToBank)
				MANAGE_CONVERSATION("RBS1_TOBANK", IS_VEHICLE_DRIVEABLE(vehPlayerCar) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar))
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-109.772804,6414.385742,20.312847>>, <<-151.971451,6458.520996,42.391434>>, 32.500000)
				AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					KILL_ANY_CONVERSATION()
					ASSET_MANAGER(stage_box_reveal_scene, TRUE, FALSE)
					bConvStarted = FALSE
					bConvOnHold = FALSE
					ConvResumeLabel = ""
					iStageProgress++
					SETTIMERA(0)
				ENDIF
			BREAK
			
			CASE 4
				MANAGE_GROUP_STATUS(blipDriveTo, blipVehicle, v_park_lot, gtDriveToBank)
				IF TIMERA() > 0 AND TIMERA() < 100
					TASK_LOOK_AT_COORD(pedLester, v_bank_door, 10000)
				ENDIF
				IF TIMERA() > 200 AND TIMERA() < 250
					TASK_LOOK_AT_COORD(PLAYER_PED_ID(), v_bank_door, 5000)
				ENDIF
				IF TIMERA() > 300 AND TIMERA() < 350
					TASK_LOOK_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], v_bank_door, 7000)
				ENDIF
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_park_lot, g_vAnyMeansLocate, TRUE)
					AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-134.723053,6448.372559,28.472401>>, <<-127.356133,6440.835449,34.436275>>, 10.750000)
						REMOVE_BLIP(blipDriveTo)
						SETTIMERA(0)
						iStageProgress++
					ENDIF
				ENDIF
			BREAK
			
			CASE 5
			
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
					SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
				ENDIF
				
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayerCar)
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
					iStageProgress++
					PRINT_NOW("CBH_FOCGOD", DEFAULT_GOD_TEXT_TIME, 1)
					PRINT_HELP("CBH_FCUSHLP", 15000)
					TRIGGER_MUSIC_EVENT("RH1_START")
					IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_THIRD_PERSON_NEAR
					AND GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_THIRD_PERSON_FAR
					AND GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_THIRD_PERSON_MEDIUM
						eSavedMode = GET_FOLLOW_VEHICLE_CAM_VIEW_MODE()
					ELSE
						eSavedMode = NUM_CAM_VIEW_MODES
					ENDIF
					SETTIMERA(0)
				ENDIF
			BREAK
			
			CASE 6
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
//					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
//					DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
					SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
				ENDIF
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_park_lot, g_vAnyMeansLocate + <<2,2,0>>)
					IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
					OR IS_GAMEPLAY_HINT_ACTIVE()
						SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
						CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, v_bank_door + <<0,0,1.5>>, "", HINTTYPE_VEHICLE_HIGH_ZOOM, TRUE, FALSE)
						CLEAR_HELP()
						IF IS_GAMEPLAY_HINT_ACTIVE()
							SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
							iStageProgress++
						ENDIF
					ENDIF
				ELSE
					IF NOT localChaseHintCamStruct.bHintInterpingBack
					AND NOT IS_GAMEPLAY_HINT_ACTIVE()
						IF eSavedMode != NUM_CAM_VIEW_MODES
							SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(eSavedMode)
							eSavedMode = NUM_CAM_VIEW_MODES
						ENDIF
					ENDIF
					CLEAR_HELP()
					iStageProgress = 4
				ENDIF
			BREAK
			
			CASE 7
			
				IF NOT localChaseHintCamStruct.bHintInterpingBack
				AND NOT IS_GAMEPLAY_HINT_ACTIVE()
					IF eSavedMode != NUM_CAM_VIEW_MODES
						SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(eSavedMode)
						eSavedMode = NUM_CAM_VIEW_MODES
					ENDIF
				ENDIF
//				CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, v_bank_door + <<0,0,1.5>>, "CBH_FCUSHLP", HINTTYPE_VEHICLE_HIGH_ZOOM)
				MANAGE_CONVERSATION("RBS1_BNKCUT", IS_VEHICLE_DRIVEABLE(vehPlayerCar) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar))
				
				IF TIMERA() < 3000
					MANAGE_GROUP_STATUS(blipDriveTo, blipVehicle, v_blip_park_shoot, gtDriveRoundBack, FALSE)
				ELSE
					IF TIMERA() > 2000
						IF NOT DOES_BLIP_EXIST(blipDriveTo)
							MANAGE_GROUP_STATUS(blipDriveTo, blipVehicle, v_blip_park_shoot, gtDriveToBank, TRUE, TRUE, TRUE, FALSE)	
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_blip_park_shoot, g_vAnyMeansLocate, TRUE)
				ENDIF
				
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				ENDIF
				
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND TIMERA() > 3000
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
					bConvStarted = FALSE
					bConvOnHold = FALSE
					SETTIMERA(0)
					iStageProgress++
					bFocusHelpUsed = TRUE
					PRINT_GOD_TEXT(gtDriveRoundBack)
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				ENDIF
				
			BREAK
			
			CASE 8
			
				IF eSavedMode != NUM_CAM_VIEW_MODES
					IF NOT localChaseHintCamStruct.bHintInterpingBack
					AND NOT IS_GAMEPLAY_HINT_ACTIVE()
						SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(eSavedMode)
						eSavedMode = NUM_CAM_VIEW_MODES
					ENDIF
				ENDIF
			
				MANAGE_REDIRECT_LINE()
				MANAGE_GROUP_STATUS(blipDriveTo, blipVehicle, v_blip_park_shoot, gtDriveToBank, TRUE, TRUE, TRUE, FALSE)
				
				IF NOT bClarified
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_MESSAGE_BEING_DISPLAYED()
						IF EVERYONE_IN()
						AND EVERYONE_OK()
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-120.500092,6513.166016,27.815300>>, <<-152.470215,6478.050293,39.481831>>, 26.000000)
								bClarified = TRUE
							ELSE
								IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_CLARIFY", CONV_PRIORITY_HIGH)
									bClarified = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF TIMERA() > 5000
						MANAGE_CONVERSATION("RBS1_ATBANK", IS_VEHICLE_DRIVEABLE(vehPlayerCar) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar))
					ENDIF
				ENDIF
				
				IF TIMERA() > 25000
				AND NOT bSpecified
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT IS_MESSAGE_BEING_DISPLAYED()
					IF EVERYONE_IN()
					AND EVERYONE_OK()
						IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_SPEC", CONV_PRIORITY_HIGH)
							REPLAY_RECORD_BACK_FOR_TIME(2.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
							bSpecified = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_blip_park_shoot, g_vAnyMeansLocate, TRUE)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
				AND EVERYONE_IN()
				AND EVERYONE_OK()
					//IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-132.182404,6511.589355,28.182859>>, <<-117.362778,6498.661621,32.665176>>, 23.250000)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_blip_park_shoot, <<5,5,2.5>>)
					AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
						KILL_FACE_TO_FACE_CONVERSATION()
						CLEAR_PRINTS()
						IF DOES_BLIP_EXIST(blipDriveTo)
							REMOVE_BLIP(blipDriveTo)
						ENDIF
						WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayerCar, 5, 5)
							WAIT(0)
							REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ThePaletoScoreSetup")
						ENDWHILE
						
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
						
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						iStageProgress++
					ENDIF
				ENDIF
			BREAK
			
			CASE 9
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF IS_AUDIO_SCENE_ACTIVE("PS_1_DRIVE_TO_BANK")
						STOP_AUDIO_SCENE("PS_1_DRIVE_TO_BANK")
					ENDIF
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct, "CMN_HINT", TRUE)
					GO_TO_STAGE(stage_box_reveal_scene)
					iStageProgress = 0
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehBus)
	AND NOT IS_PED_INJURED(pedBusDriver)
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedBusDriver)) > 50*50 
		AND NOT IS_ENTITY_ON_SCREEN(vehBus)
			DELETE_PED(pedBusDriver)
			DELETE_VEHICLE(vehBus)
			SET_MODEL_AS_NO_LONGER_NEEDED(BUS)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_LSMETRO_01)
		ENDIF
	ENDIF
	
	IF bankAlarm.bOn
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		CLEAR_PRINTS()
		ASSET_MANAGER(stage_hide, TRUE, FALSE)
		IF PLAY_DIALOGUE(dhPreShoot)
			REMOVE_BLIP(blipDriveTo)
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
			GO_TO_STAGE(stage_hide)
		ENDIF
	ENDIF
	
	IF HAS_PED_BEEN_ABANDONED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], gtGoBackToBoth)
		CLEAR_PRINTS()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		Mission_Failed("CBH_AB2BOTH")
	ENDIF
	
	IF NOT bCutscenePreStreamed
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_park_shoot) < POW(50,2)
			REQUEST_ANIM_DICT("missheistpaletoscore1rbhs_mcs_2")
			bCutscenePreStreamed = TRUE
		ENDIF
	ELSE
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_park_shoot) > POW(75,2)
			REMOVE_ANIM_DICT("missheistpaletoscore1rbhs_mcs_2")
			bCutscenePreStreamed = FALSE
		ENDIF
	ENDIF
	
	//PRE_STREAM_CUTSCENE_AT_DISTANCE("rbhs_mcs_2", v_park_shoot, 75, bCutscenePreStreamed)
	
ENDPROC

PROC RUN_BANK_RECON_CUTSCENE()

	IF NOT bCutsceneLoaded
		IF NOT IS_CUTSCENE_ACTIVE()
			REQUEST_CUTSCENE("rbhs_mcs_2")
			WAIT(0)
		ENDIF
		IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			IF NOT IS_PED_INJURED(pedLester)
				REGISTER_ENTITY_FOR_CUTSCENE(pedLester, "Lester", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
				SET_VEHICLE_DOOR_CONTROL(vehPlayerCar, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0)
				SET_VEHICLE_DOOR_CONTROL(vehPlayerCar, SC_DOOR_FRONT_RIGHT, DT_DOOR_INTACT, 0)
				SET_VEHICLE_DOOR_CONTROL(vehPlayerCar, SC_DOOR_REAR_RIGHT, DT_DOOR_INTACT, 0)
				SET_VEHICLE_DOOR_CONTROL(vehPlayerCar, SC_DOOR_REAR_LEFT, DT_DOOR_INTACT, 0)
				REGISTER_ENTITY_FOR_CUTSCENE(vehPlayerCar, "RBHS_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			CLEAR_AREA(v_park_shoot, 50.0, TRUE)
			START_CUTSCENE()
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
			ASSET_MANAGER(stage_shoot_box, TRUE, FALSE)
			bCutsceneLoaded = TRUE
		ENDIF
	ELSE
		FADE_IN()
		IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar, VS_BACK_RIGHT)
					ENDIF			
				ENDIF
			ENDIF
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lester")
				IF NOT IS_PED_INJURED(pedLester)
					IF NOT IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
						SET_PED_INTO_VEHICLE(pedLester, vehPlayerCar, VS_FRONT_RIGHT)
					ENDIF			
				ENDIF
			ENDIF
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("RBHS_car")
				SET_VEHICLE_DOORS_SHUT(vehPlayerCar)
				SET_ENTITY_COORDS_NO_OFFSET(vehPlayerCar, <<-132.2819, 6498.7437, 29.1569>>)
				SET_ENTITY_QUATERNION(vehPlayerCar, 0.0109, 0.0008, 0.7780, -0.6281)
			ENDIF
		ENDIF
		
		IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			camFinalFrame = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			SET_CAM_PARAMS(camFinalFrame, GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), GET_FINAL_RENDERED_CAM_FOV())
			SET_CAM_PARAMS(camFinalFrame, <<-133.1701, 6501.5366, 30.1278>>, <<3.8013, 0.0717, -128.0776>>, 18.0000, 3000)
			SHAKE_CAM(camFinalFrame, "HAND_SHAKE", 0.5)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
//			SET_WIDESCREEN_BORDERS(TRUE, 0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			REPLAY_STOP_EVENT()
		ENDIF
		
		IF NOT IS_CUTSCENE_ACTIVE()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			bCutsceneLoaded = FALSE
			GO_TO_STAGE(stage_shoot_box)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			DEBUG_CUTSCENE_CONTROL()
		#ENDIF
	ENDIF
ENDPROC

BOOL bGetBackIn

FUNC BOOL HAS_PLAYER_GOT_SUITABLE_WEAPON_WITH_AMMO()
	IF 	(NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL				)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL				) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATPISTOL			) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL			)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_APPISTOL				) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_MICROSMG			)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_MICROSMG				) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SMG					)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SMG					) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE			) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_CARBINERIFLE		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_CARBINERIFLE			) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_ADVANCEDRIFLE		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_ADVANCEDRIFLE		) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_MG					)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_MG					) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATMG			)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATMG				) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN			)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN			) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SAWNOFFSHOTGUN		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SAWNOFFSHOTGUN		) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTSHOTGUN		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTSHOTGUN		) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE			)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE			) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER			)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_HEAVYSNIPER			) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_REMOTESNIPER		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_REMOTESNIPER			) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_MINIGUN				)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_MINIGUN				) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_PISTOL50		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_PISTOL50			) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_ASSAULTSMG		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_ASSAULTSMG		) = 0)
	//AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_HEAVYRIFLE		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_HEAVYRIFLE		) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_BULLPUPSHOTGUN	)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_BULLPUPSHOTGUN	) = 0)
	//AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_ASSAULTMG		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_ASSAULTMG		) = 0)
	//AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_ASSAULTSNIPER	)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_ASSAULTSNIPER	) = 0)
	//AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_PROGRAMMABLEAR	)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_PROGRAMMABLEAR	) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_SPECIALCARBINE	)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_SPECIALCARBINE	) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_BULLPUPRIFLE	)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_BULLPUPRIFLE		) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_HEAVYPISTOL		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_HEAVYPISTOL		) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_SNSPISTOL		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_SNSPISTOL		) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_VINTAGEPISTOL	)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_VINTAGEPISTOL	) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_GUSENBERG		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_GUSENBERG		) = 0)
	//AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_HARPOON			)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_HARPOON			) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_MUSKET			)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_MUSKET			) = 0)
	//AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_AMRIFLE			)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_AMRIFLE			) = 0)
	//AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_CROSSBOW		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_CROSSBOW			) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_HEAVYSHOTGUN	)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_HEAVYSHOTGUN		) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_MARKSMANRIFLE	)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_MARKSMANRIFLE	) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_COMBATPDW		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_COMBATPDW		) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_MARKSMANPISTOL	)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_MARKSMANPISTOL	) = 0)
	#IF IS_NEXTGEN_BUILD
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_RAILGUN			)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_RAILGUN			) = 0)
	#ENDIF
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_MACHINEPISTOL	)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_MACHINEPISTOL	) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_COMPACTRIFLE	)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_COMPACTRIFLE		) = 0)
	AND (NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_DBSHOTGUN		)	OR 	GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_DLC_DBSHOTGUN		) = 0)
		RETURN FALSE
		
	ENDIF
	
	RETURN TRUE

ENDFUNC

PROC RUN_BANK_RECON_IN_GAME()

	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)	

	SWITCH iStageProgress
	
		CASE 0
			REQUEST_ANIM_DICT("missheistpaletoscore1rbhs_mcs_2")
			IF HAS_ANIM_DICT_LOADED("missheistpaletoscore1rbhs_mcs_2")
				bGetBackIn = FALSE
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 1
			
			IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			AND EVERYONE_OK()
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				
					SET_WIDESCREEN_BORDERS(TRUE, 500)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					
					SET_VEHICLE_DOORS_SHUT(vehPlayerCar, TRUE)
					TASK_PLAY_ANIM(PLAYER_PED_ID(), 							"missheistpaletoscore1rbhs_mcs_2", "rbhs_mcs_2_leadin_action_michael",		SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY)
					TASK_PLAY_ANIM(pedLester, 									"missheistpaletoscore1rbhs_mcs_2", "rbhs_mcs_2_leadin_action_lestercrest",	SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY)
					TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],	"missheistpaletoscore1rbhs_mcs_2", "rbhs_mcs_2_leadin_action_trevor",		SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY)
					
					IF DOES_ENTITY_EXIST(bankAlarm.obj)
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), bankAlarm.obj, -1, SLF_EXTEND_YAW_LIMIT | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
						TASK_LOOK_AT_ENTITY(pedLester, bankAlarm.obj, 5000, SLF_EXTEND_YAW_LIMIT | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
						TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], bankAlarm.obj, 7000, SLF_EXTEND_YAW_LIMIT | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					ENDIF
					
					CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, bankAlarm.obj, DEFAULT, HINTTYPE_VEHICLE_HIGH_ZOOM, FALSE)
					
					CLEAR_PRINTS()
					
					bSkipAlongInGameSeq = TRUE
					
					FADE_IN()
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
					
					iStageProgress++
					
				ELSE
				
					IF NOT DOES_BLIP_EXIST(blipVehicle)
						blipVehicle = CREATE_BLIP_FOR_VEHICLE(vehPlayerCar)
						IF NOT bGetBackIn
							PRINT_NOW("CMN_GENGETBCK", DEFAULT_GOD_TEXT_TIME, 1)
							bGetBackIn = TRUE
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
		
			IF bSkipAlongInGameSeq
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missheistpaletoscore1rbhs_mcs_2", "rbhs_mcs_2_leadin_action_michael")
					SET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), 							"missheistpaletoscore1rbhs_mcs_2", "rbhs_mcs_2_leadin_action_michael",		 0.13)
				ENDIF
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missheistpaletoscore1rbhs_mcs_2", "rbhs_mcs_2_leadin_action_lestercrest")
					SET_ENTITY_ANIM_CURRENT_TIME(pedLester, 								"missheistpaletoscore1rbhs_mcs_2", "rbhs_mcs_2_leadin_action_lestercrest",	 0.13)
				ENDIF
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missheistpaletoscore1rbhs_mcs_2", "rbhs_mcs_2_leadin_action_trevor")
					SET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 	"missheistpaletoscore1rbhs_mcs_2", "rbhs_mcs_2_leadin_action_trevor",		 0.13)
				ENDIF
				bSkipAlongInGameSeq = FALSE
			ENDIF
			
			CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, bankAlarm.obj, DEFAULT, HINTTYPE_VEHICLE_HIGH_ZOOM, FALSE)
			
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missheistpaletoscore1rbhs_mcs_2", "rbhs_mcs_2_leadin_action_michael")
			AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "missheistpaletoscore1rbhs_mcs_2", "rbhs_mcs_2_leadin_action_michael") > 0.19
				IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_BNKBOX", CONV_PRIORITY_HIGH)
					iStageProgress++
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 3
		
			CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, bankAlarm.obj, DEFAULT, HINTTYPE_VEHICLE_HIGH_ZOOM, FALSE)
			
			#IF IS_DEBUG_BUILD
				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RRIGHT)
					PRINTLN(GET_THIS_SCRIPT_NAME() , " - PLAYER_CONTROL INPUT_SCRIPT_RRIGHT Pressed")
				ENDIF
			#ENDIF
			
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missheistpaletoscore1rbhs_mcs_2", "rbhs_mcs_2_leadin_action_michael")
			AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "missheistpaletoscore1rbhs_mcs_2", "rbhs_mcs_2_leadin_action_michael") > 0.99
			AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				IF HAS_PLAYER_GOT_SUITABLE_WEAPON_WITH_AMMO()
				OR GET_ACCOUNT_BALANCE(BANK_ACCOUNT_MICHAEL) < 500
					IF GET_ACCOUNT_BALANCE(BANK_ACCOUNT_MICHAEL) < 500
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 500)
					ENDIF
					TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					SETTIMERA(0)
					iStageProgress++
				ELSE
					REMOVE_ANIM_DICT("missheistpaletoscore1rbhs_mcs_2")
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_WIDESCREEN_BORDERS(FALSE, 500)
					GO_TO_STAGE(stage_buy_gun)
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 4
			IF TIMERA() > 1000
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
				REMOVE_ANIM_DICT("missheistpaletoscore1rbhs_mcs_2")
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				SET_WIDESCREEN_BORDERS(FALSE, 500)
				GO_TO_STAGE(stage_shoot_box)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		IF NOT GET_IS_VEHICLE_ENGINE_RUNNING(vehPlayerCar)
			SET_VEHICLE_ENGINE_ON(vehPlayerCar, TRUE, TRUE)
		ENDIF
	ENDIF
	
ENDPROC

BOOL bGoBackReminder = FALSE
BOOL bMissLineDue = FALSE
//<<-326.3011, 6077.1821, 30.4548>>
INT iGoToAmmunation
BOOL bGoToAmmunationPrinted = FALSE
BOOL bBuyGunPrinted = FALSE
BOOL bDriveBackPrinted = FALSE
BOOL bGunReminderDone = FALSE

PROC PLAYER_GOES_TO_AMMUNATION_AND_BUYS_A_GUN()

	IF GET_ACCOUNT_BALANCE(BANK_ACCOUNT_MICHAEL) < 500
	AND NOT HAS_PLAYER_GOT_SUITABLE_WEAPON_WITH_AMMO()
		CREDIT_BANK_ACCOUNT(CHAR_MICHAEL, BAAC_UNLOGGED_SMALL_ACTION, 500, FALSE, FALSE)
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
	AND NOT IS_PED_INJURED(pedLester)
	AND NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
	
		SWITCH iGoToAmmunation
		
			CASE 0
				bGoToAmmunationPrinted = FALSE
				bBuyGunPrinted = FALSE
				bDriveBackPrinted = FALSE
				bGunReminderDone = FALSE
				IF DOES_BLIP_EXIST(blipAlarm)
					REMOVE_BLIP(blipAlarm)
				ENDIF
				CLEAR_PRINTS()
				KILL_FACE_TO_FACE_CONVERSATION()
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					PRINT("CMN_GENGETIN", DEFAULT_GOD_TEXT_TIME, 1)
				ENDIF
				iGoToAmmunation++
			BREAK
			
			CASE 1
			
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				
					IF NOT bGunReminderDone
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedLester)) < 10
						AND NOT IS_MESSAGE_BEING_DISPLAYED()// OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_NOGUN", CONV_PRIORITY_HIGH)
								bGunReminderDone = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT DOES_BLIP_EXIST(blipDriveTo)
						blipDriveTo = CREATE_BLIP_FOR_COORD(<<-316.8679, 6071.7979, 30.1981>>, TRUE)
						IF NOT bGunReminderDone
						OR (bGunReminderDone AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
							CLEAR_PRINTS()
						ENDIF
					ENDIF
					
					IF NOT bGoToAmmunationPrinted
					AND bGunReminderDone
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()// OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						PRINT("CBH_GOTOSHOP", DEFAULT_GOD_TEXT_TIME, 1)
						bGoToAmmunationPrinted = TRUE
					ENDIF
					
					IF DOES_BLIP_EXIST(blipVehicle)
						REMOVE_BLIP(blipVehicle)
					ENDIF
				ELSE
					IF VDIST(GET_ENTITY_COORDS(vehPlayerCar), <<-326.3011, 6077.1821, 30.4548>>) > 20
						IF NOT DOES_BLIP_EXIST(blipVehicle)
							blipVehicle = CREATE_BLIP_FOR_VEHICLE(vehPlayerCar)
						ENDIF
						IF DOES_BLIP_EXIST(blipDriveTo)
							REMOVE_BLIP(blipDriveTo)
						ENDIF
					ELSE
						IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_BLIP_COORDS(blipDriveTo), <<-326.3011, 6077.1821, 30.4548>>, 15)
							IF NOT bBuyGunPrinted
								PRINT("CBH_BUYGUN", DEFAULT_GOD_TEXT_TIME, 1)
								bBuyGunPrinted = TRUE
							ENDIF
							SET_BLIP_COORDS(blipDriveTo, <<-326.3011, 6077.1821, 30.4548>>)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-316.8679, 6071.7979, 30.1981>>, g_vAnyMeansLocate, TRUE)
				AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					IF DOES_BLIP_EXIST(blipDriveTo)
						REMOVE_BLIP(blipDriveTo)
					ENDIF
					IF DOES_BLIP_EXIST(blipVehicle)
						REMOVE_BLIP(blipVehicle)
					ENDIF
					iGoToAmmunation++
				ENDIF
				
				IF IS_PLAYER_IN_SHOP(GUN_SHOP_05_PB)
				AND DOES_BLIP_EXIST(blipDriveTo)
				AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND ARE_VECTORS_EQUAL(GET_BLIP_COORDS(blipDriveTo), <<-326.3011, 6077.1821, 30.4548>>)
					iGoToAmmunation = 5
				ENDIF
				
			BREAK
			
			CASE 2
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayerCar)
					TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					iGoToAmmunation++
				ENDIF
			BREAK
			
			CASE 3
				IF NOT DOES_BLIP_EXIST(blipDriveTo)
					IF NOT bBuyGunPrinted
						PRINT("CBH_BUYGUN", DEFAULT_GOD_TEXT_TIME, 1)
						bBuyGunPrinted = TRUE
					ENDIF
					blipDriveTo = CREATE_BLIP_FOR_COORD(<<-326.3011, 6077.1821, 30.4548>>)
				ENDIF
				IF IS_PLAYER_IN_SHOP(GUN_SHOP_05_PB)
					iGoToAmmunation++
				ENDIF
			BREAK
			
			CASE 4
				IF IS_PLAYER_IN_SHOP(GUN_SHOP_05_PB)
				
					IF DOES_BLIP_EXIST(blipDriveTo)
						REMOVE_BLIP(blipDriveTo)
					ENDIF
					
					IF NOT IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
						IF HAS_PLAYER_GOT_SUITABLE_WEAPON_WITH_AMMO()
							blipVehicle = CREATE_BLIP_FOR_VEHICLE(vehPlayerCar)
							PRINT("CMN_GENGETBCK", DEFAULT_GOD_TEXT_TIME, 1)
							bGunReminderDone = FALSE
							iGoToAmmunation++
						ELSE
							IF NOT bGunReminderDone
								PRINT("CBH_GUNREM", DEFAULT_GOD_TEXT_TIME, 1)
								bGunReminderDone = TRUE
							ENDIF
						ENDIF
					ELSE
						IF bGunReminderDone
							bGunReminderDone = FALSE
						ENDIF
					ENDIF
					
				ELSE
					iGoToAmmunation--
				ENDIF
			BREAK
			
			CASE 5
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				
					IF NOT bGunReminderDone
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedLester)) < 10
						AND NOT IS_MESSAGE_BEING_DISPLAYED()// OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_GUNBACK", CONV_PRIORITY_HIGH)
								bGunReminderDone = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT DOES_BLIP_EXIST(blipDriveTo)
						blipDriveTo = CREATE_BLIP_FOR_COORD(v_park_shoot, TRUE)
					ENDIF
					
					IF NOT bDriveBackPrinted
					AND bGunReminderDone
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()// OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						PRINT("CBH_GOBKBNK", DEFAULT_GOD_TEXT_TIME, 1)
						bDriveBackPrinted = TRUE
					ENDIF
					
					IF DOES_BLIP_EXIST(blipVehicle)
						REMOVE_BLIP(blipVehicle)
					ENDIF
					
				ELSE
				
					IF VDIST(GET_ENTITY_COORDS(vehPlayerCar), v_park_shoot) > 30
						IF NOT DOES_BLIP_EXIST(blipVehicle)
							blipVehicle = CREATE_BLIP_FOR_VEHICLE(vehPlayerCar)
						ENDIF
						IF DOES_BLIP_EXIST(blipDriveTo)
							REMOVE_BLIP(blipDriveTo)
						ENDIF
					ELSE
						GO_TO_STAGE(stage_shoot_box)
					ENDIF
					
				ENDIF
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_park_shoot, g_vAnyMeansLocate, TRUE)
				AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					iGoToAmmunation++
				ENDIF
				
			BREAK
			
			CASE 6
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayerCar)
					TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					iGoToAmmunation = 0
					GO_TO_STAGE(stage_shoot_box)
				ENDIF
			BREAK
			
		ENDSWITCH
		
	ENDIF
	
	IF bankAlarm.bOn
		PRINTLN("Bank alarm is on, going to shoot box stage")
		GO_TO_STAGE(stage_shoot_box)
	ENDIF
	
	IF iGoToAmmunation < 5
		IF HAS_PLAYER_GOT_SUITABLE_WEAPON_WITH_AMMO()
			IF DOES_BLIP_EXIST(blipDriveTo)
				REMOVE_BLIP(blipDriveTo)
			ENDIF
			IF DOES_BLIP_EXIST(blipVehicle)
				REMOVE_BLIP(blipVehicle)
			ENDIF
			iGoToAmmunation = 5
		ENDIF
	ELSE
		IF NOT HAS_PLAYER_GOT_SUITABLE_WEAPON_WITH_AMMO()
			IF DOES_BLIP_EXIST(blipDriveTo)
				REMOVE_BLIP(blipDriveTo)
			ENDIF
			IF DOES_BLIP_EXIST(blipVehicle)
				REMOVE_BLIP(blipVehicle)
			ENDIF
			iGoToAmmunation = 0
		ENDIF
	ENDIF
	
ENDPROC

PROC SHOOT_BOX()

	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)	
	
	HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
	
	IF DOES_ENTITY_EXIST(bankAlarm.obj)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(bankAlarm.obj, PLAYER_PED_ID(), TRUE)
		OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_GRENADE, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,-1.5>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,1.5>>), 3)
		OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_GRENADELAUNCHER, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,-1.5>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,1.5>>), 3)
		OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_STICKYBOMB, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,-1.5>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,1.5>>), 3)
		OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_MOLOTOV, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,-1.5>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,1.5>>), 3)
		OR IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_PROGRAMMABLEAR, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,-1.5>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(bankAlarm.obj, <<0,1.5,1.5>>), 3)
			bankAlarm.iPlayerDamage += bankAlarm.iHealthLastFrame - GET_ENTITY_HEALTH(bankAlarm.obj)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(bankAlarm.obj)
			bankAlarm.iHealthLastFrame = GET_ENTITY_HEALTH(bankAlarm.obj)
		ENDIF
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		Mission_Failed("CBH_BANKALERT")
	ENDIF
	
	IF DOES_CAM_EXIST(camFinalFrame)
	
		BOOL bPlayerControlPressed = FALSE
		INT fAnalogue[4]
		INT iTemp
		
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(fAnalogue[0], fAnalogue[1], fAnalogue[2], fAnalogue[3])
		REPEAT COUNT_OF(fAnalogue) iTemp
			IF ABSI(fAnalogue[iTemp]) > 0
				bPlayerControlPressed = TRUE
			ENDIF
		ENDREPEAT
		
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ENTER)
		OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)
		OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_AIM)
		OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_PHONE)
		OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
		OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
		OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_DUCK)
		OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_COVER)
		OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_RELOAD)
		OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_DETONATE)
			bPlayerControlPressed = TRUE
		ENDIF
		
		IF bPlayerControlPressed
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(1.5552)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-2.8327)
			RENDER_SCRIPT_CAMS(FALSE,TRUE)
			DESTROY_CAM(camFinalFrame)
		ENDIF
		
	ELSE
	
		CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, v_bank_alarm_pos, DEFAULT, HINTTYPE_VEHICLE_HIGH_ZOOM, TRUE, FALSE)
		
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
	AND DOES_ENTITY_EXIST(bankAlarm.obj)
	
		IF NOT HAS_PLAYER_GOT_SUITABLE_WEAPON_WITH_AMMO()
		AND NOT DOES_CAM_EXIST(camFinalFrame)
		
			FADE_IN()
			GO_TO_STAGE(STAGE_BUY_GUN)
			
		ELSE
		
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_stand_shoot) <= 100*100
			
				SUPPRESS_SHOCKING_EVENTS_NEXT_FRAME()
				
				IF NOT DOES_BLIP_EXIST(blipAlarm)
					blipAlarm = CREATE_BLIP_FOR_OBJECT(bankAlarm.obj)
					SET_BLIP_NAME_FROM_TEXT_FILE(blipAlarm, "CBH_BLIPALARM")
					bGoBackReminder = FALSE
					SET_BLIP_AS_FRIENDLY(blipAlarm, FALSE)
				ENDIF
				
				IF DOES_BLIP_EXIST(blipVehicle)
					REMOVE_BLIP(blipVehicle)
				ENDIF
				
				IF iStageProgress > 0
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CBH_FCUSALM")
						IF IS_GAMEPLAY_HINT_ACTIVE()
						OR localChaseHintCamStruct.bHintInterpingIn
							CLEAR_HELP()
						ENDIF
					ENDIF
				ENDIF
				
				SWITCH iStageProgress
				
					CASE 0
//						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, sBlockId, "RBS1_SHOOT", "RBS1_SHOOT_1", CONV_PRIORITY_HIGH, DISPLAY_SUBTITLES)
							iLastShootReminder = GET_GAME_TIMER()
							IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_PED_ID(), -1)
							ENDIF
							IF NOT IS_PED_INJURED(pedLester)
								TASK_LOOK_AT_ENTITY(pedLester, PLAYER_PED_ID(), -1)
							ENDIF
							PRINT_HELP("CBH_FCUSALM", -1)
							FADE_IN()
							iStageProgress++
							SET_WANTED_LEVEL_MULTIPLIER(0.0)
							SETTIMERA(0)
//						ENDIF
					BREAK
					
					CASE 1
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							SETTIMERA(0)
							iStageProgress++
						ENDIF
					BREAK
					
					CASE 2
						IF TIMERA() > 1000
							PRINT_GOD_TEXT(gtShootBox)
							START_AUDIO_SCENE("PS_1_SHOOT_THE_ALARM")
							SETTIMERA(0)
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
							iStageProgress++
						ENDIF
					BREAK
					
					CASE 3
						IF TIMERA() > 5000
							iStageProgress++
						ENDIF
					BREAK
					
					CASE 4
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehPlayerCar)) <= 25*25
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
								IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-125.873146,6486.834961,25.790363>>, <<-156.462799,6518.318359,35.259811>>, 55.000000)
									IF (GET_GAME_TIMER() - iLastShootReminder) > 7500
										IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_GOBACK", CONV_PRIORITY_HIGH, DISPLAY_SUBTITLES)
											iLastShootReminder = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ELSE
									IF (GET_GAME_TIMER() - iLastShootReminder) > 7500
										IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_GETOUT", CONV_PRIORITY_HIGH, DISPLAY_SUBTITLES)
											iLastShootReminder = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ENDIF
							ELIF IS_PLAYER_OK_TO_SHOOT_ALARM()
								IF NOT bMissLineDue
									IF (GET_GAME_TIMER() - iLastShootReminder) > 15000
										IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_SHOOT", CONV_PRIORITY_HIGH, DISPLAY_SUBTITLES)
											iLastShootReminder = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ELSE
									IF (GET_GAME_TIMER() - iLastShootReminder) > 7500
										IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_MISS", CONV_PRIORITY_HIGH, DISPLAY_SUBTITLES)
											iLastShootReminder = GET_GAME_TIMER()
											bMissLineDue = FALSE
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF (GET_GAME_TIMER() - iLastShootReminder) > 7500
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_CLOSE", CONV_PRIORITY_HIGH, DISPLAY_SUBTITLES)
										iLastShootReminder = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
				ENDSWITCH
				
				WEAPON_TYPE wepcheck
				GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wepcheck)
				IF IS_PED_SHOOTING(PLAYER_PED_ID())
				AND wepcheck != WEAPONTYPE_PETROLCAN
					iNumberOfShotsAtAlarm++
					IF NOT bankAlarm.bOn
						IF NOT bMissLineDue
							bMissLineDue = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF iNumberOfShotsAtAlarm > 20
				AND NOT bankAlarm.bOn
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					Mission_Failed("CBH_BNKFAIL")
				ENDIF
				
			ELSE
			
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_stand_shoot) > 130*130
					CLEAR_PRINTS()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					Mission_Failed("CBH_LFTAREA")
				ENDIF
				
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					IF (GET_GAME_TIMER() - iLastShootReminder) > 7500
						IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_GOBACK", CONV_PRIORITY_HIGH, DISPLAY_SUBTITLES)
							iLastShootReminder = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(-1000, 1000)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bGoBackReminder
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
						PRINT("CBH_SHTBOX2", DEFAULT_GOD_TEXT_TIME, 1)
						bGoBackReminder = TRUE
					ENDIF
				ENDIF
				
			ENDIF
			
			IF EVERYONE_OK()
				IF bankAlarm.bOn
				AND DOES_ENTITY_EXIST(bankAlarm.obj)
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
					CLEAR_PRINTS()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
					ENDIF
					IF NOT IS_PED_INJURED(pedLester)
						TASK_LOOK_AT_ENTITY(pedLester, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
					ENDIF
					IF DOES_BLIP_EXIST(blipAlarm)
						DELETE_BLIP(blipAlarm)
					ENDIF
					ASSET_MANAGER(stage_hide, TRUE, FALSE)
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct, "CBH_FCUSALM")
					SET_PED_PATHS_IN_AREA(<<-135.309174,6479.614746,32.652996>>,<<11.750000,6.750000,4.500000>>, TRUE)
					SET_PED_PATHS_IN_AREA(<<-134.387207,6492.855469,30.955853>>,<<11.750000,6.750000,4.500000>>, TRUE)
					SET_PED_PATHS_IN_AREA(<<-134.505310,6507.014160,30.966684>>,<<11.750000,6.750000,4.500000>>, TRUE)
					SET_PED_PATHS_IN_AREA(<<-135.343842,6520.587402,30.748960>>,<<11.750000,6.750000,4.500000>>, TRUE)
					SET_PED_PATHS_IN_AREA(<<-116.302612,6492.044922,33.905464>>,<<18.000000,19.250000,5.750000>>, TRUE)
					CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
					GO_TO_STAGE(stage_hide)	
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC INT GET_CLOSEST_COP()
	INT iTemp 
	FLOAT dist = 99999
	INT closest = 0
	REPEAT COUNT_OF(cars) iTemp
		IF DOES_ENTITY_EXIST(cars[iTemp].veh)
			IF IS_VEHICLE_DRIVEABLE(cars[iTemp].veh)
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(cars[iTemp].veh)) < dist*dist 
					closest = iTemp
					dist = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(cars[iTemp].veh)) 
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN closest
ENDFUNC

FUNC BOOL CAN_SIRENS_BE_HEARD(VEHICLE_INDEX &veh)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(veh))<SIREN_DISTANCE*SIREN_DISTANCE 
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PLAYER_SPOOKED_PEDS_AT_BANK(VEHICLE_INDEX players_vehicle, BOOL bCheckBankArea = FALSE)

	BOOL bRet = FALSE
	
	// have the cops been damaged by the player at the bank
	INT iTemp, iTemp2
	REPEAT COUNT_OF(cars) iTemp
	
		FOR iTemp2 = 0 TO 1
			IF DOES_ENTITY_EXIST(cars[iTemp].cops[iTemp2])
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(cars[iTemp].cops[iTemp2], PLAYER_PED_ID())
				OR (NOT IS_PED_INJURED(cars[iTemp].cops[iTemp2]) AND IS_PED_IN_MELEE_COMBAT(cars[iTemp].cops[iTemp2]))
				OR (NOT IS_PED_INJURED(cars[iTemp].cops[iTemp2]) AND IS_PED_BEING_STEALTH_KILLED(cars[iTemp].cops[iTemp2]))
					bRet = TRUE
				ENDIF
			ENDIF
		ENDFOR
		
		IF IS_VEHICLE_DRIVEABLE(cars[iTemp].veh)
			IF IS_VEHICLE_DRIVEABLE(players_vehicle)
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vGasStationPos1, vGasStationPos2, fGasStationWidth)
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), cars[iTemp].veh, <<5,5,5>>)
						IF IS_ENTITY_TOUCHING_ENTITY(players_vehicle, cars[iTemp].veh)
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(cars[iTemp].veh)
								STOP_PLAYBACK_RECORDED_VEHICLE(cars[iTemp].veh)
							ENDIF
							IF iTemp = i_main_cops
							AND IS_SYNCHRONIZED_SCENE_RUNNING(iOutroSceneId)
//								STOP_ENTITY_ANIM(cars[i_main_cops].veh, "Cops_Response_Outro_CAR", "missheistpaletoscoresetup_setup3", NORMAL_BLEND_OUT)
								STOP_SYNCHRONIZED_ENTITY_ANIM(cars[i_main_cops].veh, 1, TRUE)
							ENDIF
							bRet = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), cars[iTemp].veh)
				bRet = TRUE
			ENDIF
		ENDIF
		
	ENDREPEAT
	
	REPEAT COUNT_OF(bank_peds) iTemp
		IF DOES_ENTITY_EXIST(bank_peds[iTemp])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(bank_peds[iTemp], PLAYER_PED_ID())
				bRet = TRUE
			ENDIF
			
			IF NOT IS_PED_INJURED(bank_peds[iTemp])
				IF IS_VEHICLE_DRIVEABLE(players_vehicle)
					IF IS_ENTITY_TOUCHING_ENTITY(bank_peds[iTemp], players_vehicle)
						bRet = TRUE
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
	ENDREPEAT
	
	// the larger area around the bank
	IF IS_PED_SHOOTING(PLAYER_PED_ID())
	AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-138.982849,6475.818848,51.223343>>, <<-90.295822,6426.384766,1.471224>>, 77.750000)
		bRet = TRUE
	ENDIF
	
	IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_MOLOTOV, v_bank_door, 100)
	OR (IS_VEHICLE_DRIVEABLE(vehTrevorBike) AND IS_ENTITY_ON_FIRE(vehTrevorBike))
		bRet = TRUE
	ENDIF
	
	IF IS_SHOCKING_EVENT_IN_SPHERE(EVENT_SHOCKING_CAR_CRASH, v_bank_door, 15)
		bRet = TRUE
	ENDIF
	
	IF bCheckBankArea
		//the area directly outside the bank
		IF(IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-126.465561,6476.683105,30.468256>>, <<-103.357613,6453.125000,37.219620>>, 29.500000) 
		OR (IS_VEHICLE_DRIVEABLE(players_vehicle) AND IS_ENTITY_IN_ANGLED_AREA(players_vehicle, <<-126.465561,6476.683105,30.468256>>, <<-103.357613,6453.125000,37.219620>>, 29.500000)))
			bRet = TRUE
		ENDIF
	ENDIF
	
	RETURN bRet
	
ENDFUNC

FUNC BOOL IS_TEAM_IN_POSITION_TO_SEE_BANK()
	 RETURN IS_VEHICLE_DRIVEABLE(vehPlayerCar)
	 AND (IS_ENTITY_IN_ANGLED_AREA(vehPlayerCar, <<-113.914551,6415.773926,20.924225>>, <<-151.946686,6377.942383,51.450142>>, 50.750000) 
	 OR IS_ENTITY_IN_ANGLED_AREA(vehPlayerCar, <<-158.922684,6484.175293,19.422247>>, <<-89.311874,6412.259766,57.344151>>, 50.750000)
	 OR IS_ENTITY_IN_ANGLED_AREA(vehPlayerCar, <<-135.832352,6496.841309,-8.871904>>, <<-124.462639,6508.425781,56.732361>>, 14.500000))
ENDFUNC

PROC INTERP_VEHICLE_TO_POSITION(VEHICLE_INDEX veh, VECTOR vStartPos, VECTOR vFinalPos, FLOAT &fQuatStart[], FLOAT &fQuatEnd[], FLOAT &fInterp)
	SET_ENTITY_COORDS(veh, LERP_VECTOR(vStartPos, vFinalPos, fInterp))
	FLOAT fQuatCurrent[4]
	SLERP_NEAR_QUATERNION(fInterp, fQuatStart[0], fQuatStart[1], fQuatStart[2], fQuatStart[3], fQuatEnd[0], fQuatEnd[1], fQuatEnd[2], fQuatEnd[3], fQuatCurrent[0], fQuatCurrent[1], fQuatCurrent[2], fQuatCurrent[3])
	SET_ENTITY_QUATERNION(veh, fQuatCurrent[0], fQuatCurrent[1], fQuatCurrent[2], fQuatCurrent[3])
ENDPROC

FLOAT fInterpQuatStart[4]
FLOAT fInterpQuatEnd[4]
VECTOR vInterpPosStart
FLOAT fCopCarInterVal
BOOL bAnimStarted = FALSE

PROC DO_HIDE_DIALOGUE()

	IF iTimeInstructedToHide = -1
		IF gtHideInStation.played
			iTimeInstructedToHide = GET_GAME_TIMER()
		ENDIF
	ENDIF
	
	bHoldSpeechForHideInstruction = (GET_GAME_TIMER() - iTimeInstructedToHide) < 5000
	
	IF (GET_GAME_TIMER() - iAlarmTime) > 5000
	AND IS_TEAM_IN_POSITION_TO_SEE_BANK()
	AND NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
	AND NOT IS_PED_INJURED(pedLester)
	
		BOOL bPlay
		
		IF IS_VEHICLE_DRIVEABLE(vehPlayerCar) 
		AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
		AND NOT bLookingAtHotGirl 
		AND NOT bRecentGodText// OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
			bPlay = TRUE
		ELSE
			bPlay = FALSE
		ENDIF
		
		PRINTLN("TIme since alarm is ", GET_GAME_TIMER() - iAlarmTime, " conv stage is ", iCurrentConv)
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			TEXT_LABEL_23 sTemp = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
			PRINTLN("Currently playing ", sTemp)
		ENDIF
		
		SWITCH iCurrentConv
			CASE 0
				REQUEST_VEHICLE_RECORDING(1, s_rec_tag)
				REQUEST_VEHICLE_RECORDING(2, s_rec_tag)
				REQUEST_VEHICLE_RECORDING(3, s_rec_tag)
				REQUEST_VEHICLE_RECORDING(4, s_rec_tag)
				MANAGE_HIDE_CONVERSATION("RBS1_HIDE1", bPlay, GET_GAME_TIMER() - iAlarmTime, 5000, 6000)			// 6000, 7000)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (GET_GAME_TIMER() - iAlarmTime) > 0
					bConvOnHold = FALSE
					bConvStarted = FALSE
					ConvResumeLabel = ""																			
					iCurrentConv++																					
				ENDIF																								
			BREAK
			CASE 1
				MANAGE_HIDE_CONVERSATION("RBS1_HIDE2", bPlay, GET_GAME_TIMER() - iAlarmTime, 18000, 20000)			// 12000, 17000)
				IF (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR bConvOnHold)
				AND (GET_GAME_TIMER() - iAlarmTime) > 20000
					bConvOnHold = FALSE
					bConvStarted = FALSE
					ConvResumeLabel = ""
					bAnimStarted = FALSE
					iCurrentConv++
				ENDIF
			BREAK
			CASE 2
				MANAGE_HIDE_CONVERSATION("RBS1_HIDE3", bPlay, GET_GAME_TIMER() - iAlarmTime, 35000, 39000)			// 32000, 37000)
				IF NOT bAnimStarted
				AND bConvStarted
					IF NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missrbhsig_2", "four_cars_trevor")
						TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missrbhsig_2", "four_cars_trevor", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					ENDIF
					IF NOT IS_ENTITY_PLAYING_ANIM(pedLester, "missrbhsig_2", "four_cars_lester")
						TASK_PLAY_ANIM(pedLester, "missrbhsig_2", "four_cars_lester", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					ENDIF
					IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missrbhsig_2", "four_cars_trevor")
					AND IS_ENTITY_PLAYING_ANIM(pedLester, "missrbhsig_2", "four_cars_Lester")
						bAnimStarted = TRUE
					ENDIF
				ENDIF
				IF (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR bConvOnHold)
				AND (GET_GAME_TIMER() - iAlarmTime) > 39000
					bConvOnHold = FALSE
					bConvStarted = FALSE
					ConvResumeLabel = ""
					bAnimStarted = FALSE
					iCurrentConv++
				ENDIF
			BREAK
			CASE 3
				MANAGE_HIDE_CONVERSATION("RBS1_HIDE4", bPlay, GET_GAME_TIMER() - iAlarmTime, 59000, 59000)
				IF NOT bAnimStarted
				AND bConvStarted
					IF NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missrbhsig_2", "what_do_you_got_trevor")
						TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missrbhsig_2", "what_do_you_got_trevor", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					ENDIF
					IF NOT IS_ENTITY_PLAYING_ANIM(pedLester, "missrbhsig_2", "what_do_you_got_lester")
						TASK_PLAY_ANIM(pedLester, "missrbhsig_2", "what_do_you_got_lester", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					ENDIF
					IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missrbhsig_2", "what_do_you_got_trevor")
					AND IS_ENTITY_PLAYING_ANIM(pedLester, "missrbhsig_2", "what_do_you_got_Lester")
						bAnimStarted = TRUE
					ENDIF
				ENDIF
				IF (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR bConvOnHold)
				AND (GET_GAME_TIMER() - iAlarmTime) > 59000
					bConvOnHold = FALSE
					bConvStarted = FALSE
					bAnimStarted = FALSE
					ConvResumeLabel = ""
					iCurrentConv++
				ENDIF
			BREAK
			CASE 4
				
			BREAK
		ENDSWITCH
		
	ELSE
	
		IF NOT IS_TEAM_IN_POSITION_TO_SEE_BANK()
		AND iCurrentConv > 1
			IF (GET_GAME_TIMER() - iTimeSinceLastReminder) > 5000
			AND NOT bHoldSpeechForHideInstruction
				IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_CNTSEE", CONV_PRIORITY_HIGH, DISPLAY_SUBTITLES)
					iTimeSinceLastReminder = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	IF bLookingAtHotGirl
	AND (bConvOnHold OR IS_CONVERSATION_STATUS_FREE())
	AND NOT bTalkingAboutGirl
		KILL_FACE_TO_FACE_CONVERSATION()
		IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_HOTTY", CONV_PRIORITY_HIGH)
			bTalkingAboutGirl = TRUE
		ENDIF
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-77.352592,6418.271484,25.794516>>, <<-165.427261,6505.104004,33.552460>>, 100.000000)
	AND (GET_GAME_TIMER() - iTimeLastDickedAround) > 5000
	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF IS_PLAYER_PRESSING_HORN(PLAYER_ID())
		OR IS_PED_SHOOTING(PLAYER_PED_ID())
		AND NOT bHoldSpeechForHideInstruction
			CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_MESS", CONV_PRIORITY_HIGH)
			iTimeLastDickedAround = GET_GAME_TIMER()
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL CAN_ENTITY_BE_SEEN_AT_BANK(ENTITY_INDEX ent)

	IF DOES_ENTITY_EXIST(ent)
		
		IF IS_ENTITY_IN_ANGLED_AREA(ent, <<-90.574783,6424.710938,40.489937>>, <<-170.878906,6505.735840,28.489124>>, 69.000000)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(ent, <<-168.825287,6502.428711,39.783463>>, <<-206.763199,6527.750977,10.097977>>, 69.000000)
			RETURN TRUE
		ENDIF
		
	ENDIF

	RETURN FALSE

ENDFUNC

//INT iLastLOScheckTime
//INT iCurrentCheckLOSCar

BOOL bCopsInArea = FALSE

PROC CHECK_HIDE_FAILS()
//	IF GET_GAME_TIMER() - iLastLOScheckTime > 250
//		IF NOT IS_PED_INJURED(	cars[iCurrentCheckLOSCar].cops[0])
//		AND NOT IS_PED_INJURED(	cars[iCurrentCheckLOSCar].cops[1])
//			IF IS_ENTITY_AT_ENTITY(cars[iCurrentCheckLOSCar].cops[0], PLAYER_PED_ID(), <<5,5,3>>)
//			OR IS_ENTITY_AT_ENTITY(cars[iCurrentCheckLOSCar].cops[1], PLAYER_PED_ID(), <<5,5,3>>)
//				IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(cars[iCurrentCheckLOSCar].cops[0], PLAYER_PED_ID())
//				OR HAS_ENTITY_CLEAR_LOS_TO_ENTITY(cars[iCurrentCheckLOSCar].cops[1], PLAYER_PED_ID())
//					PRINTLN("\n\n\n\n\n\n -&- FAIL 1")
//					eReasonForFail = PSHF_SPOTTED_BY_COPS
//				ENDIF
//				iLastLOScheckTime = GET_GAME_TIMER()
//				iCurrentCheckLOSCar++
//				IF iCurrentCheckLOSCar >= COUNT_OF(cars)
//					iCurrentCheckLOSCar = 0
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	
	IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-201.027161,6374.652344,28.046611>>, <<-41.195759,6532.620605,40.490757>>, 157.750000)
		CLEAR_PRINTS()
		KILL_ANY_CONVERSATION()
		WAIT(0)
		Mission_Failed("CBH_LFTAREA")
	ENDIF
	
	INT iTemp
	IF NOT bCopsInArea
	
		REPEAT COUNT_OF(cars) iTemp
			IF IS_VEHICLE_DRIVEABLE(cars[iTemp].veh)		
				IF IS_ENTITY_IN_ANGLED_AREA(cars[iTemp].veh, <<-145.085541,6484.484863,27.727989>>, <<-92.521248,6431.290527,40.355949>>, 58.000000)
					bCopsInArea = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
		
	ELSE
	
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vGasStationPos1, vGasStationPos2, fGasStationWidth)
		OR NOT IS_ENTITY_IN_ANGLED_AREA(vehPlayerCar, vGasStationPos1, vGasStationPos2, fGasStationWidth)
		
			PRINTLN("NOT IN GAS STATION")
		
			IF eReasonForFail = PSHF_NO_FAIL
				IF CAN_ENTITY_BE_SEEN_AT_BANK(PLAYER_PED_ID())
					eReasonForFail = PSHF_SPOTTED_BY_COPS
				ENDIF
			ENDIF
			
			IF eReasonForFail = PSHF_NO_FAIL
				IF CAN_ENTITY_BE_SEEN_AT_BANK(vehPlayerCar)
					eReasonForFail = PSHF_CAR_SPOTTED_BY_COPS
				ENDIF
			ENDIF
			
			IF eReasonForFail = PSHF_NO_FAIL
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vGasStationPos1, vGasStationPos2, fGasStationWidth)
					Mission_Failed("CBH_COPMISS")
				ELIF NOT IS_ENTITY_IN_ANGLED_AREA(vehPlayerCar, vGasStationPos1, vGasStationPos2, fGasStationWidth)
					Mission_Failed("CBH_COPMILT")
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
	IF HAS_PLAYER_SPOOKED_PEDS_AT_BANK(vehPlayerCar)
		CLEAR_PRINTS()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
//		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		IF bCopsArrived
			PRINTLN("\n\n\n\n\n\n -&- FAIL 2")
			eReasonForFail = PSHF_SPOTTED_BY_COPS
		ELSE
			PRINTLN("\n\n\n\n\n\n -&- FAIL 2.5")
			eReasonForFail = PSHF_SPOTTED_BY_BANKERS
		ENDIF
	ENDIF
	
	IF HAS_PED_BEEN_ABANDONED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], gtGoBackToBoth)
		CLEAR_PRINTS()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		Mission_Failed("CBH_AB2BOTH")
	ENDIF
	
	IF bCopsArrived
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vGasStationPos1, vGasStationPos2, fGasStationWidth)
		OR NOT IS_ENTITY_IN_ANGLED_AREA(vehPlayerCar, vGasStationPos1, vGasStationPos2, fGasStationWidth)
			IF IS_ENTITY_IN_ANGLED_AREA(vehPlayerCar, <<-104.128403,6385.126953,22.438019>>, <<-54.799271,6433.794922,61.377438>>, 26.750000)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-104.128403,6385.126953,22.438019>>, <<-54.799271,6433.794922,61.377438>>, 26.750000)
				Mission_Failed("CBH_LFTAREA")
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vGasStationPos1, vGasStationPos2, fGasStationWidth)
		IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
		AND IS_PED_SHOOTING(PLAYER_PED_ID())
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 1)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			PRINTLN("\n\n\n\n\n\n -&- FAIL 3")
			eReasonForFail = PSHF_SPOTTED_BY_COPS
		ENDIF
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		PRINTLN("\n\n\n\n\n\n -&- FAIL 5")
		eReasonForFail = PSHF_SPOTTED_BY_COPS
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedBiker)
		IF DOES_ENTITY_EXIST(vehTrevorBike)
			//check the bike has been damaged by the player whilst th biker is still in or near the bike
			IF NOT IS_PED_INJURED(pedbiker)
			AND IS_VEHICLE_DRIVEABLE(vehTrevorBike)
			AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehTrevorBike, PLAYER_PED_ID()) 
			AND (IS_PED_IN_VEHICLE(pedBiker, vehTrevorBike)  OR IS_ENTITY_AT_ENTITY(pedBiker, vehTrevorBike, <<5,5,3>>))
				eReasonForFail = PSHF_ASSAULTED_BIKER
			ENDIF
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBiker, PLAYER_PED_ID())
			AND NOT IS_ENTITY_DEAD(pedBiker)
				eReasonForFail = PSHF_ASSAULTED_BIKER			
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehTrevorBike)
		IF IS_VEHICLE_ALARM_ACTIVATED(vehTrevorBike)
			IF bCopsArrived
				PRINTLN("\n\n\n\n\n\n -&- FAIL 4")
				eReasonForFail = PSHF_SPOTTED_BY_COPS
			ELSE
				PRINTLN("\n\n\n\n\n\n -&- FAIL 4.5")
				eReasonForFail = PSHF_SPOTTED_BY_BANKERS
			ENDIF
		ENDIF
	ENDIF
	
	IF eReasonForFail <> PSHF_NO_FAIL
		GO_TO_STAGE(stage_hide_fail)
	ENDIF
	
ENDPROC

PROC DO_GET_IN_REMINDER()
	IF NOT IS_ENTITY_IN_ANGLED_AREA(vehPlayerCar, vGasStationPos1, vGasStationPos2, fGasStationWidth)
		IF NOT IS_MESSAGE_BEING_DISPLAYED()// OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
		AND NOT IS_PED_INJURED(pedLester)
			IF (GET_GAME_TIMER() - iTimeOfLastWarning) > 5000
			AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedLester)) < 15
				IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_GETIN", CONV_PRIORITY_HIGH)
					iTimeOfLastWarning = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC DISMISS_COP_CAR(INT iCopCar)
	IF IS_VEHICLE_DRIVEABLE(cars[iCopCar].veh)
	AND NOT IS_PED_INJURED(cars[iCopCar].cops[0])
	AND NOT IS_PED_INJURED(cars[iCopCar].cops[1])
		IF IS_PED_IN_VEHICLE(cars[iCopCar].cops[0], cars[iCopCar].veh)
			TASK_VEHICLE_DRIVE_TO_COORD(cars[iCopCar].cops[0], cars[iCopCar].veh, <<119.2184, 6559.7744, 30.6237>>, 15.0, DRIVINGSTYLE_NORMAL, SHERIFF, DRIVINGMODE_AVOIDCARS | DF_SteerAroundPeds, 5, 5)
		ENDIF
		cars[iCopCar].bDismissed = TRUE
	ENDIF
ENDPROC

INT iParkProg
SEQUENCE_INDEX seqBiker

PROC KEEP_PLAYER_IN_GAS_STATION()
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
	AND EVERYONE_OK()
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
			IF DOES_BLIP_EXIST(blipVehicle)
				REMOVE_BLIP(blipVehicle)
			ENDIF
			SWITCH iParkProg
				CASE 0
					IF NOT DOES_BLIP_EXIST(blipDriveTo)
						blipDriveTo = CREATE_BLIP_FOR_COORD(v_park_hide, TRUE)
						TASK_CLEAR_LOOK_AT(pedLester)
						TASK_CLEAR_LOOK_AT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					ENDIF
					IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-86.590034,6420.465332,30.214172>>, <<-91.028320,6416.427734,34.715988>>, 5.5) 
						OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-89.0773, 6420.3770, 31.0252>>, <<1,1,2.5>>, TRUE)
							vTargetFocus = v_bank_door
							REMOVE_BLIP(blipDriveTo)
							iParkProg++
						ENDIF
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vGasStationPos1, vGasStationPos2, fGasStationWidth)
						AND GET_ENTITY_SPEED(vehPlayerCar) < 0.1
							vTargetFocus = v_bank_door
							REMOVE_BLIP(blipDriveTo)
							iParkProg=2
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayerCar, 3, 3)
						iParkProg++	
					ENDIF
				BREAK
				CASE 2
					IF DOES_ENTITY_EXIST(objDummy)
						IF NOT IS_PED_HEADTRACKING_ENTITY(pedLester, objDummy)
							TASK_LOOK_AT_COORD(pedLester, v_bank_door, -1, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
						ENDIF
						IF NOT IS_PED_HEADTRACKING_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], objDummy)
							TASK_LOOK_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], v_bank_door, -1, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
						ENDIF
						IF NOT IS_PED_HEADTRACKING_ENTITY(PLAYER_PED_ID(), objDummy)
							TASK_LOOK_AT_COORD(PLAYER_PED_ID(), v_bank_door, -1, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vGasStationPos1, vGasStationPos2, fGasStationWidth)
						iParkProg = 0
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			IF DOES_BLIP_EXIST(blipDriveTo)
				REMOVE_BLIP(blipDriveTo)
			ENDIF
			IF NOT DOES_BLIP_EXIST(blipVehicle)
				blipVehicle = CREATE_BLIP_FOR_VEHICLE(vehPlayerCar)
				PRINT_GOD_TEXT(gtGetBackIn)
			ENDIF
			DO_GET_IN_REMINDER()
		ENDIF
	ENDIF
ENDPROC

BOOL bPrintHide
INT iShockingEvent

PROC HIDE2()

	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0, 0.0)
	
	CHECK_HIDE_FAILS()
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
			IF IS_AUDIO_SCENE_ACTIVE("PS_1_SHOOT_THE_ALARM")
				STOP_AUDIO_SCENE("PS_1_SHOOT_THE_ALARM")
			ENDIF
			IF NOT IS_AUDIO_SCENE_ACTIVE("PS_1_DRIVE_TO_GAS_STATION")
				START_AUDIO_SCENE("PS_1_DRIVE_TO_GAS_STATION")
			ENDIF
		ENDIF
	ENDIF
	
	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
	
	//Hiding from the cops.
	//this is split into three seperate switch controls
	//one for the Player's actions, one for the bank peds and one for the cops
	
	INT iTemp
	
	//***********************************************************************************************
	//This is the flow control for the player's group's actions during the stage
	
	IF iStageProgress < 5
		IF bCopsArrived
			KILL_FACE_TO_FACE_CONVERSATION()
			bConvOnHold = FALSE
			bConvStarted = FALSE
			bAnimStarted = FALSE
			SETTIMERA(0)
			bFocusHelpPrinted = FALSE
			iStageProgress = 5
			IF NOT IS_PED_INJURED(pedLester)
				TASK_LOOK_AT_COORD(pedLester, v_bank_door, -1, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
			ENDIF
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				TASK_LOOK_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], v_bank_door, -1, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH iStageProgress
	
		CASE 0
			IF TIMERA() > 1000
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					IF IS_PHONE_ONSCREEN()
						HANG_UP_AND_PUT_AWAY_PHONE()
					ENDIF
					IF NOT DOES_BLIP_EXIST(blipVehicle)
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehPlayerCar)) > 9
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_GETIN", CONV_PRIORITY_HIGH)
								blipVehicle = CREATE_BLIP_FOR_VEHICLE(vehPlayerCar)
							ENDIF
						ELSE
							blipVehicle = CREATE_BLIP_FOR_VEHICLE(vehPlayerCar)
						ENDIF
					ENDIF
				ENDIF
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				OR DOES_BLIP_EXIST(blipVehicle)
					bPrintHide = FALSE
					BLOCK_SCENARIOS_AT_AREA(PS1SBA_BANK)
//					IF iShockingEvent = 0
//						iShockingEvent = ADD_SHOCKING_EVENT_AT_POSITION(EVENT_SHOCKING_MUGGING, v_bank_door)
//					ENDIF
					FADE_IN()
					bCopsInArea = FALSE
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1 //player on their way to the gas station
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRINT("CMN_GENGETBCK", DEFAULT_GOD_TEXT_TIME, 1)
					iStageProgress++
				ENDIF
			ELSE
				REMOVE_BLIP(blipVehicle)
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 2
			//if player never gets in car the game will stay in here until cops arrive
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar, TRUE)
				IF DOES_BLIP_EXIST(blipVehicle)
					PRINTLN("REMOVING VEHICLE BLIP 2")
					REMOVE_BLIP(blipVehicle)
				ENDIF
				IF IS_PHONE_ONSCREEN()
					HANG_UP_AND_PUT_AWAY_PHONE()
				ENDIF
				IF NOT IS_ENTITY_IN_ANGLED_AREA(vehPlayerCar, vGasStationPos1, vGasStationPos2, fGasStationWidth)
					KILL_FACE_TO_FACE_CONVERSATION()
					IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_HIDE1", CONV_PRIORITY_HIGH)
						SETTIMERA(0)
						iStageProgress++
					ENDIF
				ELSE
					SETTIMERA(0)
					iStageProgress++
				ENDIF
			ELSE
				DO_GET_IN_REMINDER()
			ENDIF
		BREAK
		
		CASE 3
			REQUEST_ANIM_DICT("missrbhsig_2")
			IF HAS_ANIM_DICT_LOADED("missrbhsig_2")
				DO_HIDE_DIALOGUE()
			ENDIF
			IF TIMERA() > 3000
				KEEP_PLAYER_IN_GAS_STATION()
				IF NOT bPrintHide
				AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					bPrintHide = TRUE
				ENDIF
			ENDIF
			//if player never gets in car the game will stay in here until cops arrive
			IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				IF NOT IS_ENTITY_IN_ANGLED_AREA(vehPlayerCar, vGasStationPos1, vGasStationPos2, fGasStationWidth)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
						PRINT_NOW("CBH_HIDELOT", 5000, 1)
						SETTIMERA(0)
						vTargetFocus = v_bank_door
						iStageProgress++
					ENDIF
				ELSE
					iStageProgress++
				ENDIF
			ELSE
				DO_GET_IN_REMINDER()
			ENDIF
		BREAK
		
		CASE 4
			REQUEST_ANIM_DICT("missrbhsig_2")
			IF HAS_ANIM_DICT_LOADED("missrbhsig_2")
				//IF TIMERA() > 5000
					DO_HIDE_DIALOGUE()
				//ENDIF
			ENDIF
			IF iParkProg > 0
			AND NOT DOES_BLIP_EXIST(blipDriveTo)
				RUN_FOCUS_CAM()
			ENDIF
		BREAK
		
		CASE 5
			RUN_FOCUS_CAM()
			REQUEST_ANIM_DICT("missrbhsig_2")
			IF HAS_ANIM_DICT_LOADED("missrbhsig_2")
				MANAGE_CONVERSATION("RBS1_COPARR", IS_VEHICLE_DRIVEABLE(vehPlayerCar) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar))
				IF NOT bAnimStarted
				AND bConvStarted
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						IF NOT IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missrbhsig_2", "this_is_them_trevor")
							TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missrbhsig_2", "this_is_them_trevor", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
						ENDIF
					ENDIF
					IF NOT IS_PED_INJURED(pedLester)
						IF NOT IS_ENTITY_PLAYING_ANIM(pedLester, "missrbhsig_2", "this_is_them_lester")
							TASK_PLAY_ANIM(pedLester, "missrbhsig_2", "this_is_them_lester", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
						ENDIF
					ENDIF
//					IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missrbhsig_2", "this_is_them_michael")
//					AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
//						TASK_PLAY_ANIM(PLAYER_PED_ID(), "missrbhsig_2", "this_is_them_michael", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
//					ENDIF
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					AND NOT IS_PED_INJURED(pedLester)
						IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missrbhsig_2", "this_is_them_trevor")
						AND IS_ENTITY_PLAYING_ANIM(pedLester, "missrbhsig_2", "this_is_them_Lester")
	//					AND (IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missrbhsig_2", "this_is_them_michael") OR NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar))
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
							bAnimStarted = TRUE
						ENDIF
					ENDIF
				ENDIF
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT bConvOnHold
				OR (NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar) AND TIMERA() > 15000)
					TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorBike, 15000, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
					IF NOT IS_PED_INJURED(cars[i_main_cops].cops[0])
						REMOVE_PED_FOR_DIALOGUE(sSpeech, 5)
						ADD_PED_FOR_DIALOGUE(sSpeech, 5, cars[i_main_cops].cops[0], "PALETOCOP1")
					ENDIF
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
					iStageProgress++
					bConvOnHold = FALSE
					bConvStarted = FALSE
					ConvResumeLabel = ""
					SETTIMERA(0)
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			RUN_FOCUS_CAM()
			MANAGE_CONVERSATION("RBS1_WAIT", IS_VEHICLE_DRIVEABLE(vehPlayerCar) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar))
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT bConvOnHold
			OR (NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar) AND TIMERA() > 15000)
				TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorBike, 15000, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
				iStageProgress++
				bConvOnHold = FALSE
				bConvStarted = FALSE
				ConvResumeLabel = ""
				SETTIMERA(0)
			ENDIF
		BREAK
		
		CASE 7
//			RUN_FOCUS_CAM()
//			IF IS_SYNCHRONIZED_SCENE_RUNNING(iOutroSceneId)
//			AND GET_SYNCHRONIZED_SCENE_PHASE(iOutroSceneId) > 0.2
//				MANAGE_CONVERSATION("RBS1_COMMENT", IS_VEHICLE_DRIVEABLE(vehPlayerCar) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar))
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() AND NOT bConvOnHold
//				OR (NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar) AND TIMERA() > 15000)
//					TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorBike, 15000, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
					iStageProgress++
//					bConvOnHold = FALSE
//					bConvStarted = FALSE
//					ConvResumeLabel = ""
//					SETTIMERA(0)
//				ENDIF
//			ENDIF
		BREAK
		
		CASE 8
			RUN_FOCUS_CAM()
			IF TIMERA() > 4000
				MANAGE_CONVERSATION("RBS1_COPREP", IS_VEHICLE_DRIVEABLE(vehPlayerCar) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar))
				IF (NOT bConvOnHold AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
				OR (TIMERA() > 17000 AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar))
					IF NOT IS_PED_INJURED(pedLester)
						TASK_LOOK_AT_COORD(pedLester, v_bank_door, 10000, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
					ENDIF
					bConvOnHold = FALSE
					bConvStarted = FALSE
					ConvResumeLabel = ""
					REMOVE_SHOCKING_EVENT(iShockingEvent)
					iShockingEvent = 0
					GO_TO_STAGE(stage_race_home)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF NOT IS_PED_INJURED(cars[i_main_cops].cops[0])
	AND bCopsArrived
		IF VDIST2(GET_ENTITY_COORDS(cars[i_main_cops].cops[0]), GET_ENTITY_COORDS(PLAYER_PED_ID())) < POW(65,2)
			vTargetFocus = GET_ENTITY_COORDS(cars[i_main_cops].cops[0])
			fInterpVal = 0.025
		ENDIF
	ENDIF
	
	IF (GET_GAME_TIMER() - iAlarmTime) < 7000
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableActionMode, TRUE)
	ENDIF
	
	IF NOT bCopsArrived
		IF CAN_SIRENS_BE_HEARD(cars[GET_CLOSEST_COP()].veh)
			bCopsArrived = TRUE
		ENDIF
	ENDIF
	
	IF bCopsArrived
		IF NOT bFocusHelpCleared
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CBH_FCUSHLP")
				CLEAR_HELP(FALSE)
				bFocusHelpCleared = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF iStageProgress > 3
		KEEP_PLAYER_IN_GAS_STATION()
	ENDIF
	
	//***********************************************************************************************
	
	//***********************************************************************************************
	// This is the flow control for the cops
	
	SWITCH iCopProgress
		CASE 0
			IF (GET_GAME_TIMER() - iAlarmTime) > 55000
				IF CREATE_RESPONSE_TEAM(FALSE)
					iCopProgress++
				ENDIF
			ENDIF
		BREAK
		CASE 1
			REQUEST_ANIM_DICT("missheistpaletoscoresetup_setup3")
			IF NOT IS_PED_INJURED(cars[i_main_cops].cops[0])
			AND NOT IS_PED_INJURED(cars[i_main_cops].cops[1])
			AND IS_VEHICLE_DRIVEABLE(cars[i_main_cops].veh)
			AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(cars[i_main_cops].veh)
			AND HAS_ANIM_DICT_LOADED("missheistpaletoscoresetup_setup3")
				FREEZE_ENTITY_POSITION(cars[i_main_cops].veh, TRUE)
				vInterpPosStart = GET_ENTITY_COORDS(cars[i_main_cops].veh)
				GET_ENTITY_QUATERNION(cars[i_main_cops].veh, fInterpQuatStart[0], fInterpQuatStart[1], fInterpQuatStart[2], fInterpQuatStart[3])
				fInterpQuatEnd[0] = 0.0041	//-0.0002
				fInterpQuatEnd[1] = 0.0030	//0.0028
				fInterpQuatEnd[2] = 0.0955	//0.0955
				fInterpQuatEnd[3] = 0.9954	//0.9954
				fCopCarInterVal = 0
				iCopProgress++
			ENDIF
		BREAK
		CASE 2
			IF IS_VEHICLE_DRIVEABLE(cars[i_main_cops].veh)
			
				IF fCopCarInterVal <= 1
					INTERP_VEHICLE_TO_POSITION(cars[i_main_cops].veh, vInterpPosStart, <<-126.1263, 6444.1216, 31.1441>>, fInterpQuatStart, fInterpQuatEnd, fCopCarInterVal)
					#IF IS_DEBUG_BUILD
						PRINTLN(fCopCarInterVal)
					#ENDIF
					fCopCarInterVal += 0.1
				ENDIF
				
			ENDIF
			
			IF fCopCarInterVal >= 1
				IF IS_VEHICLE_DRIVEABLE(cars[i_main_cops].veh)
					INTERP_VEHICLE_TO_POSITION(cars[i_main_cops].veh, vInterpPosStart, <<-126.1263, 6444.1216, 31.1441>>, fInterpQuatStart, fInterpQuatEnd, fCopCarInterVal)
					IF START_IG_BANK_OUTRO()
						REMOVE_ANIM_DICT("missheistpaletoscoresetup_setup2")	
						SET_VEHICLE_SIREN(cars[i_main_cops].veh, FALSE)
						iCopProgress=5
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 5
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iOutroSceneId)
			
				IF GET_SYNCHRONIZED_SCENE_PHASE(iOutroSceneId) > 0.3
					IF NOT cars[2].bDismissed
						REPEAT COUNT_OF(cars) iTemp
							IF IS_VEHICLE_DRIVEABLE(cars[iTemp].veh)
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(cars[iTemp].veh)
									REMOVE_VEHICLE_RECORDING(cars[iTemp].iRecording, s_rec_tag)
								ENDIF
							ENDIF
						ENDREPEAT
						DISMISS_COP_CAR(2)
					ENDIF
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(iOutroSceneId) > 0.37
					IF bankAlarm.bOn
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
						bankAlarm.bOn = FALSE
						bankAlarm.iPlayerDamage = 0
						STOP_ALARM("PALETO_BAY_SCORE_ALARM", TRUE)
					ENDIF
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(iOutroSceneId) > 0.45
					IF NOT cars[3].bDismissed
						DISMISS_COP_CAR(3)
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
	ENDSWITCH
	
	INT iCountCopCars
	IF bCopsArrived
	AND current_mission_stage = stage_hide
		REPEAT COUNT_OF(cars) iCountCopCars
			//check playbacks for cops
			IF iCountCopCars <> i_main_cops
				IF IS_VEHICLE_DRIVEABLE(cars[iCountCopCars].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(cars[iCountCopCars].veh)
						IF IS_VEHICLE_SIREN_ON(cars[iCountCopCars].veh)
							SET_VEHICLE_SIREN(cars[iCountCopCars].veh, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDREPEAT
	ENDIF
	//***********************************************************************************************
	
	//***********************************************************************************************
	//this is the flow control for the peds in the bank
	SWITCH iBankPedsProgress
		CASE 0
			IF CREATE_AMBIENT_BANK_PEDS()
				iBankPedsProgress++
			ENDIF
		BREAK
		CASE 1
			REQUEST_ANIM_DICT("missheistpaletoscoresetup_setup1")
			IF HAS_ANIM_DICT_LOADED("missheistpaletoscoresetup_setup1")
				iBankPedsProgress++
			ENDIF
		BREAK
		CASE 2
			IF SET_BANK_PEDS_TO_FLEE_BANK()
				iBankPedsProgress++
			ENDIF
		BREAK
		CASE 3
			BOOL bReady
			bReady = TRUE
			FOR iTemp = 0 TO 3
				IF NOT IS_PED_INJURED(bank_peds[iTemp])
					FLOAT fTemp
					fTemp = ABSF(GET_ENTITY_HEADING(bank_peds[iTemp]) - vBankSceneStartRot[iTemp].z)
					IF fTemp >= 180
						fTemp -= 360
					ENDIF
					IF fTemp < -180
						fTemp += 360
					ENDIF
					#IF IS_DEBUG_BUILD
						DRAW_DEBUG_TEXT(GET_STRING_FROM_FLOAT(VDIST(GET_ENTITY_COORDS(bank_peds[iTemp]), vBankSceneStartPos[iTemp])), GET_ENTITY_COORDS(bank_peds[iTemp])+<<0.0,0.0,0.5>>) 
						DRAW_DEBUG_TEXT(GET_STRING_FROM_FLOAT(fTemp), GET_ENTITY_COORDS(bank_peds[iTemp])+<<0.0,0.0,0.7>>)
					#ENDIF
					IF VDIST(GET_ENTITY_COORDS(bank_peds[iTemp]), vBankSceneStartPos[iTemp]) > 2.0
					OR fTemp >  20
						bReady = FALSE
					ENDIF
				ENDIF
			ENDFOR
			IF bReady 
				iBankPedsProgress++
			ENDIF
		BREAK
		CASE 4
			IF START_IG_BANK_INTRO()
				iBankPedsProgress++
			ENDIF
		BREAK
		CASE 5
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iIntroSceneId)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iIntroSceneId) >= 1.0
					iBankPedsProgress++
				ENDIF
			ENDIF
		BREAK
		CASE 6
			IF START_IG_BANK_LOOP()
				REMOVE_ANIM_DICT("missheistpaletoscoresetup_setup1")
				iBankPedsProgress++
			ENDIF
		BREAK
	ENDSWITCH
	//***********************************************************************************************
	
	//***********************************************************************************************
	// This is the control for the biker
	
	SWITCH iBikerStage
		CASE 0
			REQUEST_VEHICLE_RECORDING(7, s_rec_tag)
			REQUEST_ANIM_DICT("missheistpaletoscore1leadinoutrbhs_int_1")
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(7, s_rec_tag)
			AND HAS_ANIM_DICT_LOADED("missheistpaletoscore1leadinoutrbhs_int_1")
			AND (GET_GAME_TIMER() - iAlarmTime) > 14000
				IF CREATE_BIKER()
					iBikerStage++
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF IS_VEHICLE_DRIVEABLE(vehTrevorBike)
			AND NOT IS_PED_INJURED(pedBiker)
				SET_VEHICLE_ALARM(vehTrevorBike, TRUE)
				START_PLAYBACK_RECORDED_VEHICLE(vehTrevorBike, 7, s_rec_tag)
				SET_PLAYBACK_SPEED(vehTrevorBike, 0.85)
				//ADD_PED_FOR_DIALOGUE(sSpeech, 5, pedBiker, "PALETOBIKER")
				iBikerStage++
			ENDIF
		BREAK
		
		CASE 2
			IF IS_VEHICLE_DRIVEABLE(vehTrevorBike)
			AND NOT IS_PED_INJURED(pedBiker)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrevorBike)
					PLAY_PED_AMBIENT_SPEECH(pedBiker, "FIGHT_RUN", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
//					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBiker, "RURS1_BSAA", "PALETOBIKER", SPEECH_PARAMS_FORCE_FRONTEND)
					REMOVE_VEHICLE_RECORDING(7, s_rec_tag)
					SET_VEHICLE_ENGINE_ON(vehTrevorBike, FALSE, FALSE)
					OPEN_SEQUENCE_TASK(seqBiker)
					TASK_LEAVE_VEHICLE(NULL, vehTrevorBike)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -76.8476, 6417.1646, 30.4902 >>, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -59.8699, 6423.7739, 30.4862 >>, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-63.1039, 6427.2490, 30.4869>>, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
					CLOSE_SEQUENCE_TASK(seqBiker)
					TASK_PERFORM_SEQUENCE(pedBiker, seqBiker)
					CLEAR_SEQUENCE_TASK(seqBiker)
					iBikerStage++
				ELSE
					//check for collision
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrevorBike)
					
						IF NOT IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(vehTrevorBike)
						
							VECTOR vFrontPos1
							vFrontPos1 = GET_ENTITY_COORDS(vehTrevorBike) - <<0,0,2>>
							VECTOR vFrontPos2
							vFrontPos2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTrevorBike, <<0, CLAMP(GET_ENTITY_SPEED(vehTrevorBike)/10 +12, 12, 25), 2>>)
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vFrontPos1, vFrontPos2, 3.5)
								SET_PLAYBACK_TO_USE_AI(vehTrevorBike, DRIVINGMODE_AVOIDCARS)
							ENDIF
							
						ENDIF
						
						IF NOT IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(vehTrevorBike)	
						
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehTrevorBike)) < 3
								PRINTLN("BIKER IS LESS THAN 4m FROM PLAYER")
								IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), vehTrevorBike)
								OR IS_ENTITY_TOUCHING_ENTITY(vehPlayerCar, vehTrevorBike)
									SET_PLAYBACK_TO_USE_AI(vehTrevorBike, DRIVINGMODE_AVOIDCARS)
								ENDIF
							ENDIF
							
						ENDIF
						
						#IF IS_DEBUG_BUILD
						
							IF NOT IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(vehTrevorBike)
								IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
//									REMOVE_VEHICLE_RECORDING(7, s_rec_tag)
//									SET_VEHICLE_ENGINE_ON(vehTrevorBike, FALSE, FALSE)
									OPEN_SEQUENCE_TASK(seqBiker)
									TASK_LEAVE_VEHICLE(NULL, vehTrevorBike)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -76.8476, 6417.1646, 30.4902 >>, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -59.8699, 6423.7739, 30.4862 >>, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-63.1039, 6427.2490, 30.4869>>, PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
									CLOSE_SEQUENCE_TASK(seqBiker)
									TASK_PERFORM_SEQUENCE(pedBiker, seqBiker)
									CLEAR_SEQUENCE_TASK(seqBiker)
								ENDIF
							ENDIF
							
						#ENDIF
						
					ENDIF
				ENDIF
			ENDIF
			
			//check for damage
			IF NOT IS_VEHICLE_DRIVEABLE(vehTrevorBike)
			OR IS_PED_INJURED(pedBiker)
			OR (IS_VEHICLE_DRIVEABLE(vehTrevorBike) AND  HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehTrevorBike, PLAYER_PED_ID()))
			OR (NOT IS_PED_INJURED(pedBIker) AND  HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBiker, PLAYER_PED_ID()))
				IF NOT IS_ENTITY_DEAD(vehTrevorBike)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrevorBike)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehTrevorBike)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF DOES_ENTITY_EXIST(pedBiker)
			AND NOT IS_PED_INJURED(pedBiker)
				IF IS_ENTITY_IN_ANGLED_AREA(pedBiker, <<-64.708885,6428.822754,30.487309>>, <<-60.184513,6424.345703,35.236881>>, 3.750000)
					IF IS_ENTITY_OCCLUDED(pedbiker)
						REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
						DELETE_PED(pedBiker)
					ELSE
						IF GET_SCRIPT_TASK_STATUS(pedBiker, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
							IF NOT IS_ENTITY_PLAYING_ANIM(pedBiker, "missheistpaletoscore1leadinoutrbhs_int_1", "_leadin_trevor")
								TASK_PLAY_ANIM(pedBiker, "missheistpaletoscore1leadinoutrbhs_int_1", "_leadin_trevor", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
								iBikerStage++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF DOES_ENTITY_EXIST(pedBiker)
			AND NOT IS_PED_INJURED(pedBiker)
				IF IS_ENTITY_OCCLUDED(pedbiker)
				AND VDIST2(GET_ENTITY_COORDS(pedBiker), GET_ENTITY_COORDS(PLAYER_PED_ID()))>20*20
					REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
					DELETE_PED(pedBiker)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	//***********************************************************************************************
	
	DOOR_SYSTEM_SET_HOLD_OPEN(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L), TRUE)
	DOOR_SYSTEM_SET_HOLD_OPEN(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R), TRUE)
	
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iStageProgress), <<0.02, 0.02, 0>>)
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iBankPedsProgress), <<0.02, 0.05, 0>>)
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iCopProgress), <<0.02, 0.08, 0>>)
	#ENDIF
	
ENDPROC

SEQUENCE_INDEX seqDriver
SEQUENCE_INDEX seqPassenger
VECTOR vGoto
INT iFailProgress = 0
INT iShockingFailEvent

PROC SET_BANKERS_AND_COPS_FAIL_TASKS()
	iShockingFailEvent = ADD_SHOCKING_EVENT_AT_POSITION(EVENT_SHOCKING_MUGGING, GET_ENTITY_COORDS(PLAYER_PED_ID()))
	INT iTemp
	REPEAT COUNT_OF(bank_peds) iTemp
		IF NOT IS_PED_INJURED(bank_peds[iTemp])
			CLEAR_PED_TASKS(bank_peds[iTemp])
			SET_PED_COMBAT_ATTRIBUTES(bank_peds[iTemp], CA_ALWAYS_FLEE, TRUE)
			SET_PED_RELATIONSHIP_GROUP_HASH(bank_peds[iTemp], RELGROUPHASH_HATES_PLAYER)
			IF NOT bCopsArrived
				TASK_REACT_AND_FLEE_PED(bank_peds[iTemp], PLAYER_PED_ID())
			ELSE
				TASK_SHOCKING_EVENT_REACT(bank_peds[iTemp], iShockingFailEvent)
			ENDIF
		ENDIF
	ENDREPEAT
	REPEAT COUNT_OF(cars) iTemp
		IF IS_VEHICLE_DRIVEABLE(cars[iTemp].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(cars[iTemp].veh)
				STOP_PLAYBACK_RECORDED_VEHICLE(cars[iTemp].veh)
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(cars[iTemp].cops[0])
			vGoto = GET_ENTITY_COORDS(PLAYER_PED_ID()) + GET_VECTOR_FROM_DIRECTION_AND_MAGNITUDE(GET_ENTITY_COORDS(cars[iTemp].cops[0]) - GET_ENTITY_COORDS(PLAYER_PED_ID()), 5)
			OPEN_SEQUENCE_TASK(seqDriver)
			IF IS_VEHICLE_DRIVEABLE(cars[iTemp].veh)
			AND IS_PED_IN_VEHICLE(cars[iTemp].cops[0], cars[iTemp].veh)
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(cars[iTemp].veh)) < 30*30
					TASK_VEHICLE_MISSION_COORS_TARGET(NULL, cars[iTemp].veh, GET_ENTITY_COORDS(PLAYER_PED_ID()), MISSION_GOTO, 20, DRIVINGMODE_AVOIDCARS, 20, 10)
				ENDIF
				TASK_VEHICLE_MISSION(NULL, cars[iTemp].veh, NULL, MISSION_STOP, 10, DRIVINGMODE_AVOIDCARS, 10, 10)
				TASK_LEAVE_VEHICLE(NULL, cars[iTemp].veh, ECF_DONT_CLOSE_DOOR)
			ENDIF
			TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, vGoto, PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK, FALSE)
			CLOSE_SEQUENCE_TASK(seqDriver)
			TASK_PERFORM_SEQUENCE(cars[iTemp].cops[0], seqDriver)
			CLEAR_SEQUENCE_TASK(seqDriver)
		ENDIF
		IF NOT IS_PED_INJURED(cars[iTemp].cops[1])
			vGoto = GET_ENTITY_COORDS(PLAYER_PED_ID()) + GET_VECTOR_FROM_DIRECTION_AND_MAGNITUDE(GET_ENTITY_COORDS(cars[iTemp].cops[1]) - GET_ENTITY_COORDS(PLAYER_PED_ID()), 5)
			OPEN_SEQUENCE_TASK(seqPassenger)
			IF IS_VEHICLE_DRIVEABLE(cars[iTemp].veh)
			AND IS_PED_IN_VEHICLE(cars[iTemp].cops[1], cars[iTemp].veh)
				TASK_LEAVE_VEHICLE(NULL, cars[iTemp].veh, ECF_DONT_CLOSE_DOOR)
			ENDIF
			TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, vGoto, PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK, FALSE)
			CLOSE_SEQUENCE_TASK(seqPassenger)
			TASK_PERFORM_SEQUENCE(cars[iTemp].cops[1], seqPassenger)
			CLEAR_SEQUENCE_TASK(seqPassenger)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL DO_FAIL_DIALOGUE(STRING sLabel, PED_INDEX pedSpeaker)
	IF DOES_ENTITY_EXIST(pedSpeaker)
		IF NOT IS_PED_INJURED(pedSpeaker)
		AND NOT IS_PED_INJURED(pedLester)
			IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedSpeaker)) < 20*20
			AND	VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedLester)) < 20*20
				PRINTLN("@@@@@@@@@@@@@@@@@ DO_FAIL_DIALOGUE 1 @@@@@@@@@@@@@@@@@@@@@@@")
				IF CREATE_CONVERSATION(sSpeech, sBlockId, sLabel, CONV_PRIORITY_VERY_HIGH)
					RETURN TRUE
				ENDIF
			ELIF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedSpeaker)) < 20*20
				PRINTLN("@@@@@@@@@@@@@@@@@ DO_FAIL_DIALOGUE 2 @@@@@@@@@@@@@@@@@@@@@@@")
				TEXT_LABEL_63 sTemp
				sTemp = sLabel
				sTemp += "_1"
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, sBlockId, sLabel, sTemp, CONV_PRIORITY_VERY_HIGH)
					RETURN TRUE
				ENDIF
			ELIF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedLester)) < 20*20
				PRINTLN("@@@@@@@@@@@@@@@@@ DO_FAIL_DIALOGUE 3 @@@@@@@@@@@@@@@@@@@@@@@")
				TEXT_LABEL_63 sTemp
				sTemp = sLabel
				sTemp += "_2"
				IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, sBlockId, sLabel, sTemp, CONV_PRIORITY_VERY_HIGH)
					RETURN TRUE
				ENDIF
			ELSE
				SETTIMERA(0)
				RETURN TRUE
			ENDIF
		ELSE
			SETTIMERA(0)
			RETURN TRUE
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(pedLester)
		AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedLester)) < 20*20
			PRINTLN("@@@@@@@@@@@@@@@@@ DO_FAIL_DIALOGUE 4 @@@@@@@@@@@@@@@@@@@@@@@")
			IF CREATE_CONVERSATION(sSpeech, sBlockId, sLabel, CONV_PRIORITY_VERY_HIGH)
				RETURN TRUE
			ENDIF
		ELSE
			SETTIMERA(0)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DO_HIDE_FAIL()

	#IF IS_DEBUG_BUILD
		PRINTLN("IN HIDE_FAIL fail reason is ", ENUM_TO_INT(eReasonForFail), " Fail progress is ", iFailProgress )
	#ENDIF
	
	SWITCH eReasonForFail
	
		CASE PSHF_ASSAULTED_BIKER
			SWITCH iFailProgress
				CASE 0
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					IF NOT IS_PED_INJURED(pedBiker)
					AND NOT IS_PED_FLEEING(pedBiker)
						IF NOT IS_PED_INJURED(pedBiker)
							TASK_SMART_FLEE_PED(pedBiker, PLAYER_PED_ID(), 250, -1)
						ENDIF
					ENDIF
					IF DO_FAIL_DIALOGUE("RBS1_BKRFAIL", pedBiker)
						iFailProgress++
					ENDIF
				BREAK
				CASE 1
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND TIMERA() > 2000
						Mission_Failed("CBH_SECALT")
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE PSHF_CAR_SPOTTED_BY_COPS
			SWITCH iFailProgress
				CASE 0
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					SET_BANKERS_AND_COPS_FAIL_TASKS()
					iFailProgress++
				BREAK
				CASE 1
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND TIMERA() > 2000
						IF bCopsArrived
							Mission_Failed("CBH_SPOTLT")
						ELSE
							Mission_Failed("CBH_SPOTLT")
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE PSHF_SPOTTED_BY_COPS
		CASE PSHF_SPOTTED_BY_BANKERS
			SWITCH iFailProgress
				CASE 0
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					SET_BANKERS_AND_COPS_FAIL_TASKS()
					iFailProgress++
				BREAK
				CASE 1
					IF DO_FAIL_DIALOGUE("RBS1_BNKFAIL", bank_peds[0])
						iFailProgress++
					ENDIF
				BREAK
				CASE 2
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND TIMERA() > 2000
						IF bCopsArrived
							Mission_Failed("CBH_COPFAIL")
						ELSE
							Mission_Failed("CBH_BNKFAIL")
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE PSHF_WANTED_LEVEL
			SWITCH iFailProgress
				CASE 0
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					SET_BANKERS_AND_COPS_FAIL_TASKS()
					iFailProgress++
				BREAK
				CASE 1
					IF DO_FAIL_DIALOGUE("RBS1_WLFAIL", NULL)
						iFailProgress++
					ENDIF
				BREAK
				CASE 2
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND TIMERA() > 2000
						Mission_Failed("CBH_COPFAIL")
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		
		CASE PSHF_DICKING_AROUND
		
		BREAK
	ENDSWITCH
ENDPROC

CONST_INT MAX_ROUTE_NODES 100
VECTOR route_path_mike[MAX_ROUTE_NODES]
VECTOR route_path_trevor[MAX_ROUTE_NODES]


FUNC FLOAT GET_TIME_POSITION_IN_RECORDING_FROM_POSITION(VECTOR point, INT iRecNumber, STRING sRecName, VECTOR &route[])

	IF NOT IS_STRING_NULL_OR_EMPTY(sRecName)
	AND iRecNumber > 0

		IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecNumber, sRecName)

			INT iTemp, iTemp2
			
			//three closest nodes, seeded at 0, 1 and 2 to prevent stacking on 0
			INT iClosestNodes[3]
			iClosestNodes[0] = MAX_ROUTE_NODES - 1
			iClosestNodes[1] = MAX_ROUTE_NODES - 2
			iClosestNodes[2] = MAX_ROUTE_NODES - 3
			
			//Cycle through all points
			REPEAT COUNT_OF(route) iTemp
				//get distance to current point
				FLOAT fDistX = ABSF(point.x - route[iTemp].x)
				FLOAT fDistY = ABSF(point.y - route[iTemp].y) 
				FLOAT fDist = fDistX + fDistY
				//cycle through current three closest points and get distance to all to figure furthest
				FLOAT fFurthest = 0.0
				INT iFurthest = 0
				REPEAT COUNT_OF(iClosestNodes) iTemp2
					IF iTemp <> iClosestNodes[iTemp2]
						FLOAT fTempDistX = ABSF(point.x - route[iClosestNodes[iTemp2]].x)
						FLOAT fTempDistY = ABSF(point.y - route[iClosestNodes[iTemp2]].y)
						FLOAT fTempDist = fTempDistX + fTempDistY
						IF fTempDist > fFurthest
							iFurthest = iTemp2
							fFurthest = fTempDist
						ENDIF
					ENDIF
				ENDREPEAT
				//if this points distance is closer than the furthest in the set of three closest nodes then replace it with this node.
				IF fDist < fFurthest
					iClosestNodes[iFurthest] = iTemp
				ENDIF
			ENDREPEAT
			
			#IF IS_DEBUG_BUILD
				REPEAT COUNT_OF(iClosestNodes) iTemp2
					DRAW_DEBUG_SPHERE(route[iClosestNodes[iTemp2]]+<<0,0,0.3*iTemp2+0.3>>, 0.1, 255,0,0)
				ENDREPEAT
			#ENDIF
			
			//now we have the three closest points check line positions to see which line is closest
			FLOAT fClosestDist = 999999
			INT iClosestLineStart
			VECTOR vClosestPoint
			
			REPEAT COUNT_OF(iClosestNodes) iTemp
				VECTOR vTemp1
				VECTOR vTemp2
				//get the distnace to the line either side of the nearest point, wrapping to start and end point if necessary.
				IF iClosestNodes[iTemp] > 0
					vTemp1 = GET_CLOSEST_POINT_ON_LINE(point, route[iClosestNodes[iTemp]], route[iClosestNodes[iTemp]-1])
				ELSE
					vTemp1 = route[0]
				ENDIF
				IF iClosestNodes[iTemp] < COUNT_OF(route)-2
					vTemp2 = GET_CLOSEST_POINT_ON_LINE(point, route[iClosestNodes[iTemp]], route[iClosestNodes[iTemp]+1])
				ELSE
					vTemp2 = route[COUNT_OF(route)-1]
				ENDIF
				//Check which line is closer
				VECTOR vClosest
				BOOL bAhead = FALSE //this is a flag to show that the player is ahead of the current closest point
				IF VDIST2(point, vTemp1) < VDIST2(point, vTemp2)  
					vClosest = vTemp1
				ELSE
					bAhead = TRUE
					vClosest = vTemp2
				ENDIF
				//check if the closest point to the line from the current closest node is closer than the current closest
				IF VDIST2(point, vClosest) < fClosestDist 
					IF bAhead
						iClosestLineStart = iClosestNodes[iTemp]
					ELSE
						iClosestLineStart = iClosestNodes[iTemp] - 1
					ENDIF
					fClosestDist = VDIST2(point, vClosest) 
					vClosestPoint = vClosest
				ENDIF
			ENDREPEAT
			
			iClosestLineStart = CLAMP_INT(iClosestLineStart, 0, MAX_ROUTE_NODES-2)
			FLOAT fTimePerNode = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iRecNumber, sRecName)/(MAX_ROUTE_NODES-1)
			FLOAT fCurrentLineProg = (VDIST(route[iClosestLineStart], vClosestPoint)/VDIST(route[iClosestLineStart], route[iClosestLineStart+1]))*fTimePerNode
			
			#IF IS_DEBUG_BUILD
				DRAW_DEBUG_SPHERE(vClosestPoint + <<0.2,0,0>>, 0.1, 255, 0, 255)
			#ENDIF
			
			RETURN fCurrentLineProg + (iClosestLineStart*fTimePerNode)
		
		ENDIF
		
	ENDIF
	
	RETURN -1.0
	
ENDFUNC

//draws a car recording (LOW RES)
PROC DRAW_UBER_ROUTE_DEBUG_PATH(VECTOR &path[], INT red = 0, INT green = 0, INT blue = 255)
	INT iTemp
	REPEAT COUNT_OF(path) iTemp
		DRAW_DEBUG_SPHERE(path[iTemp], 0.2, red, green, blue, 170)
		IF iTemp > 0
			DRAW_DEBUG_LINE(path[iTemp], path[iTemp-1], red, green, blue, 170)
		ENDIF
	ENDREPEAT
ENDPROC

//Builds an array of vectors for a car recording path
PROC BUILD_UBER_ROUTE_DEBUG_PATH(VECTOR &path[], INT FileNumber, STRING pRecordingName)
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(FileNumber, pRecordingName)
		FLOAT duration = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(FileNumber, pRecordingName)
		FLOAT route_segment = duration/(MAX_ROUTE_NODES-1)
		INT iTemp
		path[MAX_ROUTE_NODES-1] = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(FileNumber, duration, pRecordingName)
		REPEAT COUNT_OF(path) iTemp
			path[iTemp] = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(FileNumber, route_segment*iTemp, pRecordingName)
		ENDREPEAT
	ENDIF
ENDPROC

//BOOL bDrivingSafely = FALSE

FLOAT fNPCProg
FLOAT fPlayerProg

FUNC BOOL IS_PED_ON_DANGEROUS_PATH_SECTION(PED_INDEX ped)
	RETURN IS_ENTITY_IN_ANGLED_AREA(ped, <<-824.968811,5228.710938,116.867775>>, <<-852.028687,5341.011719,54.161114>>, 65.750000)
	OR IS_ENTITY_IN_ANGLED_AREA(ped, <<-171.554703,4652.350098,140.703827>>, <<-33.917088,4546.746582,106.957390>>, 65.750000)
	OR IS_ENTITY_IN_ANGLED_AREA(ped, <<-89.917274,4308.339844,58.665241>>, <<-193.938660,4219.963867,35.347736>>, 38.250000)
	OR IS_ENTITY_IN_ANGLED_AREA(ped, <<-223.024384,3932.076172,48.885773>>, <<-215.288055,3832.037109,28.850357>>, 56.500000)
	OR IS_ENTITY_IN_ANGLED_AREA(ped, <<-215.463135,3720.155762,61.359421>>, <<-125.230385,3631.988525,35.598236>>, 56.500000)
	OR IS_ENTITY_IN_ANGLED_AREA(ped, <<85.419563,3496.506348,51.257030>>, <<177.078384,3396.791748,28.676632>>, 119.250000)
	OR IS_ENTITY_IN_ANGLED_AREA(ped, <<35.187660,4455.668945,81.755875>>, <<-37.359905,4422.036133,54.018738>>, 62.250000)
	OR IS_ENTITY_IN_ANGLED_AREA( ped, <<-809.763611,5265.458984,77.198647>>, <<-726.477478,5220.504883,124.458748>>, 61.750000)
	OR IS_ENTITY_IN_ANGLED_AREA( ped, <<-652.957581,5133.302246,114.413513>>, <<-587.371216,5025.889648,164.566162>>, 61.750000)
	OR IS_ENTITY_IN_ANGLED_AREA( ped, <<-243.411575,4721.370605,91.370911>>, <<-97.041641,4589.317383,148.161987>>, 22.000000)
	OR IS_ENTITY_IN_ANGLED_AREA( ped, <<-51.048225,4561.023438,76.794930>>, <<24.164288,4525.126953,129.619980>>, 43.000000)
	OR IS_ENTITY_IN_ANGLED_AREA( ped, <<-35.085011,3605.691895,-0.894375>>, <<128.688843,3553.018799,63.646278>>, 70.500000)
	OR IS_ENTITY_IN_ANGLED_AREA( ped, <<-419.152985,6119.916016,30.156736>>, <<-435.434814,6059.347656,38.902100>>, 39.250000)
ENDFUNC

FLOAT fCurrentSwitchDist = MIN_DIST_SWITCH_TO_AI_ON_ROAD
DRIVINGMODE eCurrentDrivingMode
DRIVINGMODE ePreviousDrivingMode

PROC KEEP_PED_ON_REC_PATH2(PED_INDEX ped, VEHICLE_INDEX veh, INT iRecordingNumber, STRING pRecordingName, VECTOR &route_path[], BOOL bCatchup, FLOAT fPlayerRaceProg)

	IF NOT IS_PED_INJURED(ped)
	AND IS_VEHICLE_DRIVEABLE(veh)
	
		fNPCProg = GET_TIME_POSITION_IN_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(veh), iRecordingNumber, pRecordingName, route_path)
		
		IF IS_PED_ON_DANGEROUS_PATH_SECTION(ped)
			#IF IS_DEBUG_BUILD
				DRAW_DEBUG_TEXT_2D("SLOW!!", <<0.05, 0.02, 0>>)
			#ENDIF
			IF IS_PED_IN_VEHICLE(ped, veh)
				SET_DRIVE_TASK_MAX_CRUISE_SPEED(ped, 25)
			ENDIF
			IF eCurrentDrivingMode <> DRIVINGMODE_PLOUGHTHROUGH
				eCurrentDrivingMode = DRIVINGMODE_PLOUGHTHROUGH
			ENDIF
		ELSE
			IF IS_PED_IN_VEHICLE(ped, veh)
				SET_DRIVE_TASK_MAX_CRUISE_SPEED(ped, GET_VEHICLE_MODEL_ESTIMATED_MAX_SPEED(GET_ENTITY_MODEL(veh)))
			ENDIF
			IF eCurrentDrivingMode <> DRIVINGMODE_AVOIDCARS
				eCurrentDrivingMode = DRIVINGMODE_AVOIDCARS
			ENDIF
		ENDIF
		
		IF IS_POINT_IN_2D_BOUNDS(GET_ENTITY_COORDS(veh), vOffroadMountains)
			#IF IS_DEBUG_BUILD
				INT iLines
				REPEAT COUNT_OF(vOffroadMountains) iLines
					INT p2 = iLines+1
					IF iLines = COUNT_OF(vOffroadMountains) - 1
						p2 = 0						
					ENDIF
					DRAW_DEBUG_LINE(<<vOffroadMountains[iLines].x, vOffroadMountains[iLines].y, 200>>, <<vOffroadMountains[p2].x, vOffroadMountains[p2].y, 200>>, 0, 255, 0)
				ENDREPEAT
			#ENDIF
			IF fCurrentSwitchDist != MIN_DIST_SWITCH_TO_AI_OFF_ROAD
				fCurrentSwitchDist = MIN_DIST_SWITCH_TO_AI_OFF_ROAD
			ENDIF
			IF eCurrentDrivingMode <> DRIVINGMODE_PLOUGHTHROUGH
				eCurrentDrivingMode = DRIVINGMODE_PLOUGHTHROUGH
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				INT iLines
				REPEAT COUNT_OF(vOffroadMountains) iLines
					INT p2 = iLines+1
					IF iLines = COUNT_OF(vOffroadMountains) - 1
						p2 = 0						
					ENDIF
					DRAW_DEBUG_LINE(<<vOffroadMountains[iLines].x, vOffroadMountains[iLines].y, 200>>, <<vOffroadMountains[p2].x, vOffroadMountains[p2].y, 200>>, 255, 0, 0)
				ENDREPEAT
			#ENDIF
			IF eCurrentDrivingMode <> DRIVINGMODE_AVOIDCARS
				eCurrentDrivingMode = DRIVINGMODE_AVOIDCARS
			ENDIF
			IF fCurrentSwitchDist != MIN_DIST_SWITCH_TO_AI_ON_ROAD
				fCurrentSwitchDist = MIN_DIST_SWITCH_TO_AI_ON_ROAD
			ENDIF
		ENDIF
		
		IF eCurrentDrivingMode <> ePreviousDrivingMode
			IF IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(veh)
				SET_PLAYBACK_TO_USE_AI(veh, eCurrentDrivingMode)
			ENDIF
			ePreviousDrivingMode = eCurrentDrivingMode
		ENDIF
		
		#IF IS_DEBUG_BUILD 
			IF eCurrentDrivingMode = DRIVINGMODE_AVOIDCARS
				DRAW_DEBUG_TEXT_2D("NPC DRIVINGMODE_AVOIDCARS", <<0.01, 0.04, 0>>)
			ELIF eCurrentDrivingMode = DRIVINGMODE_PLOUGHTHROUGH
				DRAW_DEBUG_TEXT_2D("NPC DRIVINGMODE_PLOUGHTHROUGH", <<0.01, 0.04, 0>>)
			ENDIF
			IF fCurrentSwitchDist = MIN_DIST_SWITCH_TO_AI_ON_ROAD
				DRAW_DEBUG_TEXT_2D("MIN_DIST_SWITCH_TO_AI_ON_ROAD", <<0.01, 0.06, 0>>)
			ELSE
				DRAW_DEBUG_TEXT_2D("MIN_DIST_SWITCH_TO_AI_OFF_ROAD", <<0.01, 0.06, 0>>)
			ENDIF
		#ENDIF
		
		IF IS_PED_IN_VEHICLE(ped, veh)
		
			IF NOT bForcedPlayback
			
				#IF IS_DEBUG_BUILD
					DRAW_DEBUG_TEXT_2D("NPC ON AI RECORDING", <<0.01, 0.2, 0>>)
				#ENDIF
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
				AND NOT IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(veh)
					SET_PLAYBACK_TO_USE_AI(veh, eCurrentDrivingMode)
				ENDIF
				
				IF NOT IS_POINT_IN_ANGLED_AREA(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNumber, fNPCProg, pRecordingName), <<-494.934326,4928.820313,157.124222>>, <<-134.457245,4615.543945,124.083611>>, 30.250000)
					FLOAT fDist2 = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(ped))
					IF fDist2 > fCurrentSwitchDist*fCurrentSwitchDist 
					AND (fNPCProg > 10000 OR fDist2 > 10000)
					AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecordingNumber, pRecordingName)
					AND fNPCProg < GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iRecordingNumber, pRecordingName) - 10000
						IF IS_ENTITY_OCCLUDED(ped)
						AND IS_ENTITY_OCCLUDED(veh)
						AND NOT IS_SPHERE_VISIBLE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNumber, fNPCProg, pRecordingName), 4)
							IF NOT IS_PED_IN_VEHICLE(ped, veh)
								SET_PED_INTO_VEHICLE(ped, veh)
							ENDIF
							SET_ENTITY_COLLISION(veh, FALSE)
							STOP_PLAYBACK_RECORDED_VEHICLE(veh)
							START_PLAYBACK_RECORDED_VEHICLE(veh, iRecordingNumber, pRecordingName)
							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(veh, fNPCProg)
							#IF IS_DEBUG_BUILD
								PRINTLN("SETTING PED TO RACE FROM ", fNPCProg, " at coords ", GET_STRING_FROM_VECTOR( GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNumber, fNPCProg, pRecordingName)))
							#ENDIF
							bForcedPlayback = TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ELSE
			
				#IF IS_DEBUG_BUILD
					DRAW_DEBUG_TEXT_2D("NPC ON RECORDING", <<0.01, 0.2, 0>>)
				#ENDIF
				
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(ped)) < fCurrentSwitchDist*fCurrentSwitchDist*0.9
				OR (NOT IS_ENTITY_OCCLUDED(veh) AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(veh)) < MIN_DIST_SWITCH_TO_AI_ON_ROAD*MIN_DIST_SWITCH_TO_AI_ON_ROAD*1.5)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
					AND IS_VEHICLE_DRIVEABLE(veh)
						SET_PLAYBACK_SPEED(veh, 1.0)
						SET_ENTITY_COLLISION(veh, TRUE)
						bForcedPlayback = FALSE
					ENDIF
				ELSE
					IF bCatchup
						IF fPlayerRaceProg > fNPCProg
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
							AND IS_VEHICLE_DRIVEABLE(veh)
								SET_PLAYBACK_SPEED(veh, CLAMP((fPlayerRaceProg-fNPCProg)/1000, 1.0, 3.0))
							ENDIF
						ELSE
							IF IS_VEHICLE_DRIVEABLE(veh)
								SET_PLAYBACK_SPEED(veh, 1.0)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
		ELSE
		
			IF NOT IS_PED_IN_VEHICLE(ped, veh)
				IF GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
					TASK_ENTER_VEHICLE(ped, veh, DEFAULT_TIME_NEVER_WARP)
				ENDIF
				IF IS_ENTITY_OCCLUDED(veh)
				AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(ped))>50
				AND VDIST2(GET_ENTITY_COORDS(veh), GET_ENTITY_COORDS(PLAYER_PED_ID()))>50
					IF NOT IS_SPHERE_VISIBLE(GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNumber, fNPCProg, pRecordingName), 2)
						SET_ENTITY_COORDS(veh, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRecordingNumber, fNPCProg, pRecordingName))
					ENDIF
					IF IS_ENTITY_OCCLUDED(ped)
						SET_PED_INTO_VEHICLE(ped, veh)
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

PROC CHECK_RACE_FAILS()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		//If Michael check Trevor dead, lester dead, car dead, and wanted level		
		IF IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		OR IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		OR IS_ENTITY_ON_FIRE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			PRINTLN("\n\n\n\n\n Trevor died here 2 \n\n\n\n")
			Mission_Failed("CMN_TDIED")
		ENDIF
		IF IS_PED_INJURED(pedLester)
		OR IS_ENTITY_DEAD(pedLester)
		OR IS_ENTITY_ON_FIRE(pedLester)
			Mission_Failed("CBH_LESTDED")
		ENDIF
		IF NOT IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		OR IS_ENTITY_DEAD(vehPlayerCar)
			Mission_Failed("CMN_GENDEST")			
		ENDIF
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			Mission_Failed("CBH_COPFAIL")
		ENDIF
	ENDIF
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		OR IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		OR IS_ENTITY_ON_FIRE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			Mission_Failed("CMN_MDIED")
		ENDIF
		IF IS_PED_INJURED(pedLester)
		OR IS_ENTITY_DEAD(pedLester)
		OR IS_ENTITY_ON_FIRE(pedLester)
			Mission_Failed("CBH_LESTDED")
		ENDIF
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			Mission_Failed("CBH_COPFAILT")
		ENDIF
	ENDIF
	
ENDPROC

STRING sRaceHomeDialogues[2]
BOOL bDialogueCanPlay[2]
BOOL bDialogueDone[2]
BOOL bNpcFinished

VECTOR vWinAreaPos1 = <<1391.680420,3582.977539,33.746563>>
VECTOR vWinAreaPos2 = <<1430.257935,3597.642334,40.459114>> 
FLOAT fWinAreaWidth = 33.000000

VECTOR vRaceFinish = <<1403.7411, 3593.7473, 33.8199>>

SELECTOR_SLOTS_ENUM npcPed
VEHICLE_INDEX playerVeh
VEHICLE_INDEX npcVeh
BOOL bRaceConv = FALSE
BOOL bSwitchAvailable = FALSE
BOOL bSwitchTaken = FALSE
BOOL bRacePedInit = FALSE

BOOL bMainConvOnHold
BOOL bMainConvStarted
TEXT_LABEL_23 sMainConvResumeLabel

INT iTimeLastBanter

INT iDriveHomeConvStage
INT iDriveHomeConvSavedStage
INT iInterruptDialogue

INT iLastFinger

PROC DO_RACE_HOME_DIALOGUE()

	INT iTemp
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 sTemp
		sTemp = "Current dialogue stage "
		sTemp += iDriveHomeConvStage
		DRAW_DEBUG_TEXT_2D(sTemp, <<0.01,0.5,0>>)
		sTemp = "Current saved dialogue stage "
		sTemp += iDriveHomeConvSavedStage
		DRAW_DEBUG_TEXT_2D(sTemp, <<0.01,0.52,0>>)
		
		sTemp = "bDialogueCanPlay[0] " 
		IF bDialogueCanPlay[0] 
			sTemp += "TRUE"
		ELSE
			sTemp += "FALSE"
		ENDIF
		DRAW_DEBUG_TEXT_2D(sTemp, <<0.01,0.54,0>>)
		
		sTemp = "bDialogueDone[0] " 
		IF bDialogueDone[0] 
			sTemp += "TRUE"
		ELSE
			sTemp += "FALSE"
		ENDIF
		DRAW_DEBUG_TEXT_2D(sTemp, <<0.01,0.56,0>>)
		
		sTemp = "bDialogueCanPlay[1] " 
		IF bDialogueCanPlay[1] 
			sTemp += "TRUE"
		ELSE
			sTemp += "FALSE"
		ENDIF
		DRAW_DEBUG_TEXT_2D(sTemp, <<0.01,0.58,0>>)
		
		sTemp = "bDialogueDone[1] " 
		IF bDialogueDone[1] 
			sTemp += "TRUE"
		ELSE
			sTemp += "FALSE"
		ENDIF
		DRAW_DEBUG_TEXT_2D(sTemp, <<0.01,0.6,0>>)
	#ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(playerVeh)
	AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
	
		SWITCH iDriveHomeConvStage
		
			CASE 0
				sRaceHomeDialogues[0] = "RBS1_LES2"
				sRaceHomeDialogues[1] = "RBS1_LES3"
				iDriveHomeConvStage++
			BREAK
			
			CASE 1
				IF (NOT bDialogueCanPlay[0] OR (bDialogueCanPlay[0] AND bDialogueDone[0]))
				AND (NOT bDialogueCanPlay[1] OR (bDialogueCanPlay[1] AND bDialogueDone[1]))
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), playerVeh)
					IF NOT bMainConvStarted
						IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_LES1", CONV_PRIORITY_HIGH)
							bMainConvStarted = TRUE
						ENDIF
					ENDIF
					IF bMainConvOnHold
						IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, sBlockId, "RBS1_LES1", sMainConvResumeLabel, CONV_PRIORITY_HIGH)
							bMainConvOnHold = FALSE
						ENDIF
					ENDIF
				ELSE
					PRINTLN("HOLDING CONV")
					IF NOT bMainConvOnHold
						sMainConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
						KILL_FACE_TO_FACE_CONVERSATION()
						bMainConvOnHold = TRUE
						iDriveHomeConvSavedStage = iDriveHomeConvStage
						iDriveHomeConvStage = 100
					ENDIF
				ENDIF
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT bMainConvOnHold
					PRINTLN("GOING TO NEXT CONV STAGE")
					bMainConvStarted = FALSE
					sMainConvResumeLabel = ""
					iDriveHomeConvStage++
				ENDIF
			BREAK
			
			CASE 2
				IF (NOT bDialogueCanPlay[0] OR (bDialogueCanPlay[0] AND bDialogueDone[0]))
				AND (NOT bDialogueCanPlay[1] OR (bDialogueCanPlay[1] AND bDialogueDone[1]))
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), playerVeh)
					IF NOT bMainConvStarted
						IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_LES1b", CONV_PRIORITY_HIGH)
							bMainConvStarted = TRUE
						ENDIF
					ENDIF
					IF bMainConvOnHold
						IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, sBlockId, "RBS1_LES1b", sMainConvResumeLabel, CONV_PRIORITY_HIGH)
							bMainConvOnHold = FALSE
						ENDIF
					ENDIF
				ELSE
					IF NOT bMainConvOnHold
						sMainConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
						KILL_FACE_TO_FACE_CONVERSATION()
						bMainConvOnHold = TRUE
						iDriveHomeConvSavedStage = iDriveHomeConvStage
						iDriveHomeConvStage = 100
					ENDIF
				ENDIF
			BREAK
			
			CASE 100
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT IS_MESSAGE_BEING_DISPLAYED()// OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					iInterruptDialogue = -1
					REPEAT COUNT_OF(bDialogueDone) iTemp
						IF bDialogueCanPlay[iTemp]
						AND NOT bDialogueDone[iTemp]
							IF iInterruptDialogue = -1
								iInterruptDialogue = iTemp
							ENDIF
							MANAGE_CONVERSATION(sRaceHomeDialogues[iTemp], IS_VEHICLE_DRIVEABLE(playerVeh) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), playerVeh))
						ENDIF
					ENDREPEAT
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT bConvOnHold
					AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), playerVeh)
						bConvStarted = FALSE
						IF iInterruptDialogue <> -1
							bDialogueDone[iInterruptDialogue] = TRUE
						ENDIF
						ConvResumeLabel = ""
						iDriveHomeConvStage = iDriveHomeConvSavedStage
						iDriveHomeConvSavedStage = 1000
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		IF NOT bDialogueCanPlay[0]
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-706.7669, 5536.0161, 34.65338>>, <<-805.068420,5471.531250,39.915943>>, 24.000000)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), playerVeh)
				bDialogueCanPlay[0] = TRUE
			ENDIF
		ENDIF
		IF NOT bDialogueCanPlay[1]
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-262.86, 4738.64,119.116486>>, <<-163.374329,4644.937012,147.835770>>, 14.250000)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), playerVeh)
				bDialogueCanPlay[1] = TRUE
			ENDIF
		ENDIF
		
	ENDIF
	
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[npcPed])
	AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[npcPed], npcVeh)
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	
		IF npcPed = SELECTOR_PED_TREVOR
		
			IF (GET_GAME_TIMER() - iTimeLastBanter) > 6000
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[npcPed])) < 25
					IF NOT bConvOnHold
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_MESSAGE_BEING_DISPLAYED()// OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
						IF IS_VEHICLE_DRIVEABLE(playerVeh)
						AND IS_VEHICLE_DRIVEABLE(npcVeh)
							IF IS_ENTITY_TOUCHING_ENTITY(npcVeh, playerVeh)
							OR IS_ENTITY_IN_ANGLED_AREA(npcVeh, GET_ENTITY_COORDS(playerVeh), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(playerVeh, <<0, 4, 0>>), 2)
								IF GET_RANDOM_BOOL()
									IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_ASSHLEM", CONV_PRIORITY_HIGH)
										iTimeLastBanter = GET_GAME_TIMER()
									ENDIF
								ELSE
									IF CREATE_CONVERSATION(sSpeech, sBlockId, sBanterString, CONV_PRIORITY_HIGH)
										iTimeLastBanter = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[npcPed])) < POW(15,2)
				SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[npcPed], WEAPONTYPE_UNARMED, TRUE)
				SET_CURRENT_PED_VEHICLE_WEAPON(sSelectorPeds.pedID[npcPed], WEAPONTYPE_UNARMED)
				SET_PED_CAN_SWITCH_WEAPON(sSelectorPeds.pedID[npcPed], FALSE)
				WEAPON_TYPE wepcheck
				GET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[npcPed], wepcheck)
				IF wepcheck = WEAPONTYPE_UNARMED
					IF NOT IS_PED_DOING_DRIVEBY(sSelectorPeds.pedID[npcPed])
						IF GET_GAME_TIMER() - iLastFinger > 8000
							TASK_DRIVE_BY(sSelectorPeds.pedID[npcPed], PLAYER_PED_ID(), NULL, <<0,0,0>>, 25, 80, TRUE, FIRING_PATTERN_BURST_FIRE_DRIVEBY)
							iLastFinger = GET_GAME_TIMER()
						ENDIF
					ELSE
						IF GET_GAME_TIMER() - iLastFinger > 3000
							CLEAR_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(sSelectorPeds.pedID[npcPed])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

STRUCT ROUTE_POINT
	VECTOR m_vPoint
	FLOAT m_fDistAlongRoute
	BOOL m_bFlaggedForRoute
ENDSTRUCT

ROUTE_POINT sTrevorRouteToMethlab[4]
ROUTE_POINT sMichaelRouteToMethlab[5]

INT iMichaelClosestPoint
INT iTrevorClosestPoint

PROC ADD_POINT_TO_ROUTE_POINT_ARRAY(VECTOR vPoint, ROUTE_POINT &sRoute[])
	INT iTemp
	REPEAT COUNT_OF(sRoute) iTemp
		IF ARE_VECTORS_EQUAL(sRoute[iTemp].m_vPoint, <<0,0,0>>)
			sRoute[iTemp].m_vPoint = vPoint
			EXIT
		ENDIF
	ENDREPEAT
ENDPROC

PROC BUILD_ROUTE_FOR_GPS_ALONG_RECORDING(ROUTE_POINT &sRoute[], INT iRecordingNumber, STRING pRecordingName, VECTOR &vRoutePath[])
	REQUEST_VEHICLE_RECORDING(iRecordingNumber, pRecordingName)
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(iRecordingNumber, pRecordingName)
		INT iTemp
		REPEAT COUNT_OF(sRoute) iTemp
			sRoute[iTemp].m_fDistAlongRoute = GET_TIME_POSITION_IN_RECORDING_FROM_POSITION(sRoute[iTemp].m_vPoint, iRecordingNumber, pRecordingName, vRoutePath)
			sRoute[iTemp].m_bFlaggedForRoute = TRUE			
		ENDREPEAT
	ENDIF
ENDPROC

PROC RESET_CUSTOM_GPS_ROUTE(INT &iCurrentClosest)
	IF iCurrentClosest <> -1
		PRINTLN("GPS DEBUG: Clearing GPS")
		CLEAR_GPS_MULTI_ROUTE()
		iCurrentClosest = -1
	ENDIF
ENDPROC

PROC BUILD_CUSTOM_GPS_ROUTE(ROUTE_POINT &vRoute[], INT &iCurrentClosest, FLOAT fPlayerProgress)

	INT i = 0
	INT iClosestNode = COUNT_OF(vRoute) - 1
	
	//Get the closest node to the player
	REPEAT COUNT_OF(vRoute) i
		IF fPlayerProgress <= vRoute[i].m_fDistAlongRoute
		AND iClosestNode = COUNT_OF(vRoute) - 1
			iClosestNode = i
		ENDIF
		#IF IS_DEBUG_BUILD
			IF i = iCurrentClosest
				DRAW_DEBUG_SPHERE(vRoute[i].m_vPoint, 10, 0, 255, 0, 100)
			ELSE
				DRAW_DEBUG_SPHERE(vRoute[i].m_vPoint, 10, 0, 0, 255, 100)
			ENDIF
		#ENDIF
	ENDREPEAT
	
	IF iCurrentClosest != iClosestNode
		PRINTLN("GPS DEBUG: Updating GPS closest node")
		IF (iCurrentClosest >= 0 AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRoute[iCurrentClosest].m_vPoint) > 10*10)
		OR iCurrentClosest = -1
			CLEAR_GPS_MULTI_ROUTE()
			//Build the route starting from the node calculated above.
			START_GPS_MULTI_ROUTE(HUD_COLOUR_OBJECTIVE_ROUTE)
			FOR i = iClosestNode TO COUNT_OF(vRoute) - 1
				ADD_POINT_TO_GPS_MULTI_ROUTE(vRoute[i].m_vPoint)
			ENDFOR
			SET_GPS_MULTI_ROUTE_RENDER(TRUE)
			PRINTLN("GPS DEBUG: Refreshing GPS route")
		ENDIF
		iCurrentClosest = iClosestNode
	ENDIF
	
ENDPROC

BOOL bRaceText
BOOL bSequenced
BOOL bSequenced2
BOOL bRecStarted = FALSE
BOOL bAssholeLine = FALSE
BOOL bBankDumped = FALSE

INT iTrevorFaceStage
INT iTrevorWaitStage

SEQUENCE_INDEX seqTurnTask
SEQUENCE_INDEX seqAnimLoop

PROC RUN_TREVOR_WAIT_LOOP()

	IF iTrevorWaitStage < 3
	AND iTrevorWaitStage > 0
		IF (NOT IS_SPHERE_VISIBLE(<<1405.9346, 3589.4290, 33.9496>>, 15) OR VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) > 150*150)
		AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) > 75*75	
			SET_ENTITY_COORDS_NO_OFFSET(vehTrevorBike, <<1405.8481, 3598.6001, 34.4038>>)
			SET_ENTITY_QUATERNION(vehTrevorBike, 0.0483, -0.1174, 0.4164, 0.9003)
			SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], << 1404.870, 3597.943, 34.894 >>)
			SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 135)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehTrevorBike)
			PRINTLN(" WARPING TREVOR AND BIKE TO STAND AND WAIT ")
			iTrevorWaitStage= 3
		ENDIF
	ENDIF
	
	SWITCH iTrevorWaitStage
	
		CASE 0
		
			REQUEST_ANIM_DICT("missheistpaletoscoresetup")
			IF HAS_ANIM_DICT_LOADED("missheistpaletoscoresetup")
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				AND NOT IS_ENTITY_DEAD(vehTrevorBike)
					TASK_VEHICLE_PARK(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorBike, <<1405.8481, 3598.6001, 34.4038>>, 49.5670, PARK_TYPE_PERPENDICULAR_NOSE_IN, 90)
					iTrevorWaitStage++
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 1
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			AND NOT IS_ENTITY_DEAD(vehTrevorBike)
				IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_VEHICLE_PARK) <> PERFORMING_TASK
					SEQUENCE_INDEX seqPark
					OPEN_SEQUENCE_TASK(seqPark)
						TASK_VEHICLE_MISSION_COORS_TARGET(NULL, vehTrevorBike, GET_ENTITY_COORDS(vehTrevorBike), MISSION_STOP, 1, DRIVINGMODE_AVOIDCARS, 10, 10)
						TASK_LEAVE_VEHICLE(NULL, vehTrevorBike, ECF_USE_LEFT_ENTRY)
						TASK_ACHIEVE_HEADING(NULL, 135)	
					CLOSE_SEQUENCE_TASK(seqPark)
					TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqPark)
					CLEAR_SEQUENCE_TASK(seqPark)
					iTrevorWaitStage++
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 2
			
			IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorBike)
			AND GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
			AND GET_ENTITY_SPEED(vehTrevorBike) < 0.1
				iTrevorWaitStage++
			ENDIF
			
		BREAK
		
		CASE 3
		
			IF iTrevorFaceStage > 1
				IF NOT IS_PED_FACING_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_PED_ID(), 60)
					iTrevorFaceStage = 0
				ENDIF
			ENDIF
			
			SWITCH iTrevorFaceStage
				CASE 0
					CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT | SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
					OPEN_SEQUENCE_TASK(seqTurnTask)
					TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seqTurnTask)
					TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqTurnTask)
					CLEAR_SEQUENCE_TASK(seqTurnTask)
					iTrevorFaceStage++
				BREAK
				CASE 1
					IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
						iTrevorFaceStage++
					ENDIF
				BREAK
				CASE 2
					OPEN_SEQUENCE_TASK(seqAnimLoop)
						TASK_PLAY_ANIM(NULL, "missheistpaletoscoresetup", "trevor_impatient_wait_1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
						TASK_PLAY_ANIM(NULL, "missheistpaletoscoresetup", "trevor_impatient_wait_2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
						TASK_PLAY_ANIM(NULL, "missheistpaletoscoresetup", "trevor_impatient_wait_3", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
						TASK_PLAY_ANIM(NULL, "missheistpaletoscoresetup", "trevor_impatient_wait_4", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
					CLOSE_SEQUENCE_TASK(seqAnimLoop)
					TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqAnimLoop)
					CLEAR_SEQUENCE_TASK(seqAnimLoop)
					iTrevorFaceStage++
				BREAK
			ENDSWITCH
			
		BREAK
	ENDSWITCH
ENDPROC

BOOL bBlipIsVeh

PROC BLIP_NPC(PED_INDEX ped, VEHICLE_INDEX veh)
	IF IS_VEHICLE_DRIVEABLE(veh)
	AND NOT IS_PED_INJURED(ped)
		IF DOES_BLIP_EXIST(blipBuddy)
		
			IF bBlipIsVeh
				IF NOT IS_PED_IN_VEHICLE(ped, veh)
					SET_BLIP_SCALE(blipBuddy, BLIP_SIZE_PED)
					bBlipIsVeh = FALSE
				ENDIF
			ELSE
				IF IS_PED_IN_VEHICLE(ped, veh)
					SET_BLIP_SCALE(blipBuddy, BLIP_SIZE_VEHICLE)
					bBlipIsVeh = TRUE
				ENDIF
			ENDIF
			
			VECTOR vBlipPosition 	= GET_BLIP_COORDS(blipBuddy)
			VECTOR vVehiclePosition = GET_ENTITY_COORDS(ped)
			
			vBlipPosition.X = vBlipPosition.X + ((vVehiclePosition.X - vBlipPosition.X) / 10.0)
			vBlipPosition.Y = vBlipPosition.Y + ((vVehiclePosition.Y - vBlipPosition.Y) / 10.0)
			vBlipPosition.Z = vBlipPosition.Z + ((vVehiclePosition.Z - vBlipPosition.Z) / 10.0)
			
			SET_BLIP_COORDS(blipBuddy, vBlipPosition)
						
		ELSE
		
			blipBuddy = CREATE_BLIP_FOR_COORD(GET_ENTITY_COORDS(ped))
			SET_BLIP_COLOUR(blipBuddy, BLIP_COLOUR_BLUE)
			SET_BLIP_PRIORITY(blipBuddy, BLIPPRIORITY_LOW)
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_BLIP_NAME_FROM_TEXT_FILE(blipBuddy, "CBH_BLIPTREV")
			ELSE
				SET_BLIP_NAME_FROM_TEXT_FILE(blipBuddy, "CBH_BLIPMIKE")
			ENDIF
			
		ENDIF
		
	ELSE
		IF DOES_BLIP_EXIST(blipBuddy)
			REMOVE_BLIP(blipBuddy)
		ENDIF
	ENDIF
ENDPROC

BOOL bCop1SetToWalk = FALSE
BOOL bCop2SetToWalk = FALSE
BOOL bWInLoseChatDone = FALSE
BOOL bCarStopped = FALSE
BOOL bTrevorWonLine = FALSE

PROC RACE_HOME()

	HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
	
	SWITCH iStageProgress
	
		CASE 0
		
			IF IS_VEHICLE_DRIVEABLE(vehTrevorBike)
			AND NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				
				IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorBike, TRUE)
				AND GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
				
					FREEZE_ENTITY_POSITION(vehTrevorBike, TRUE)
					bBikeFrozen = TRUE
					
					SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_TRUCK_LOGS", FALSE)
					SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_HIKER", FALSE)
					SET_SCENARIO_TYPE_ENABLED("WORLD_VEHICLE_BICYCLE_MOUNTAIN", FALSE)
					
					SET_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
					SET_VEHICLE_ALARM(vehTrevorBike, FALSE)
					bRaceConv = FALSE
					
					INT iTemp
					REPEAT COUNT_OF(bDialogueCanPlay) iTemp
						bDialogueCanPlay[iTemp] = FALSE
						bDialogueDone[iTemp] = FALSE
					ENDREPEAT
					
					bNpcFinished = FALSE
					bSwitchAvailable = TRUE
					bSwitchTaken = FALSE
					bRacePedInit = FALSE
					bForcedPlayback = FALSE
					bRaceText = FALSE
					bSequenced = FALSE
					bSequenced2 = FALSE
					bRecStarted = FALSE
					bAssholeLine = FALSE
					bBankDumped = FALSE
					bPedsSentInside = FALSE
					bTrevorWonLine = FALSE
					
					bCop1SetToWalk = FALSE
					bCop2SetToWalk = FALSE
					
					vOffroadMountains[0] = <<262.31213, 4447.51123, 53.83961>>
					vOffroadMountains[1] = <<-210.83116, 4482.65576, 60.27203>>
					vOffroadMountains[2] = <<-1119.85730, 5254.62793, 74.45103>>
					vOffroadMountains[3] = <<-912.04346, 5407.72656, 36.07696>>
					vOffroadMountains[4] = <<-791.72174, 5470.60400, 33.07196>>
					vOffroadMountains[5] = <<-625.39935, 5531.64941, 44.20467>>
					
					iTrevorClosestPoint = COUNT_OF(sTrevorRouteToMethlab) -1 
					iMichaelClosestPoint = COUNT_OF(sMichaelRouteToMethlab) -1
					
					ADD_POINT_TO_ROUTE_POINT_ARRAY(<<-415.5206, 6124.9604, 30.3722>>, sMichaelRouteToMethlab)
					ADD_POINT_TO_ROUTE_POINT_ARRAY(<<-653.8552, 5356.8843, 57.9293>>, sMichaelRouteToMethlab)
					ADD_POINT_TO_ROUTE_POINT_ARRAY(<<-943.5536, 5257.1318, 81.4484>>, sMichaelRouteToMethlab)
					ADD_POINT_TO_ROUTE_POINT_ARRAY(<<-483.1230, 4917.2988, 145.827>>, sMichaelRouteToMethlab)
					ADD_POINT_TO_ROUTE_POINT_ARRAY(vRaceFinish, sMichaelRouteToMethlab)
					
					ADD_POINT_TO_ROUTE_POINT_ARRAY(<<-415.5206, 6124.9604, 30.3722>>, sTrevorRouteToMethlab)
					ADD_POINT_TO_ROUTE_POINT_ARRAY(<<-653.8552, 5356.8843, 57.9293>>, sTrevorRouteToMethlab)
					ADD_POINT_TO_ROUTE_POINT_ARRAY(<<-483.1230, 4917.2988, 145.827>>, sTrevorRouteToMethlab)
					ADD_POINT_TO_ROUTE_POINT_ARRAY(vRaceFinish, sTrevorRouteToMethlab)
					
					npcPed = SELECTOR_PED_TREVOR
					playerVeh = vehPlayerCar
					npcVeh = vehTrevorBike
					
					REQUEST_VEHICLE_RECORDING(10, "RBsetup")
					REQUEST_VEHICLE_RECORDING(11, "RBsetup")
					REMOVE_VEHICLE_RECORDING(1, s_rec_tag)
					REMOVE_VEHICLE_RECORDING(2, s_rec_tag)
					REMOVE_VEHICLE_RECORDING(3, s_rec_tag)
					REMOVE_VEHICLE_RECORDING(4, s_rec_tag)
					
					SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,TRUE)
					SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_MICHAEL,TRUE)
					SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,FALSE)
					
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L), DOORSTATE_UNLOCKED)
					DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R), DOORSTATE_UNLOCKED)
					
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
					
					REMOVE_CUTSCENE()
					REPLAY_RECORD_BACK_FOR_TIME(2)
					iStageProgress++
					
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 1
		
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[npcPed])
			AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			AND IS_VEHICLE_DRIVEABLE(vehTrevorBike)
				
				IF NOT bRaceConv
				AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[npcPed], vehPlayerCar)
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					PRINTLN("@@@@@@@@@@@@@@@@@@ SET_PLAYER_CONTROL(PLAYER_ID(), FALSE @@@@@@@@@@@@@@@@@@@")
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_RACE", CONV_PRIORITY_HIGH)
						SETTIMERA(0)
						FADE_IN()
						bRaceConv = TRUE
					ENDIF
				ELSE
					IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar)
						IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_LEAVE_VEHICLE) != PERFORMING_TASK
							TASK_LEAVE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar, ECF_WARP_IF_DOOR_IS_BLOCKED)
						ENDIF
					ENDIF
					IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar)
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						PRINTLN("@@@@@@@@@@@@@@@@@@ SET_PLAYER_CONTROL(PLAYER_ID(), TRUE @@@@@@@@@@@@@@@@@@@")
					ENDIF
					IF TIMERA() > 1500
					AND GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[npcPed], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
					AND NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[npcPed], vehTrevorBike)
					AND NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[npcPed], vehPlayerCar, TRUE)
						SEQUENCE_INDEX seqTemp
						OPEN_SEQUENCE_TASK(seqTemp)
						TASK_ENTER_VEHICLE(NULL, vehTrevorBike, DEFAULT_TIME_NEVER_WARP)
						CLOSE_SEQUENCE_TASK(seqTemp)
						TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[npcPed], seqTemp)
						CLEAR_SEQUENCE_TASK(seqTemp)
						iStageProgress++
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 2
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[npcPed])
			AND IS_VEHICLE_DRIVEABLE(vehTrevorBike)
			AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[npcPed], vehPlayerCar)
						IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						ENDIF
					ELSE
						IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					ENDIF
				ENDIF
				IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[npcPed], vehPlayerCar)
					IF IS_AUDIO_SCENE_ACTIVE("PS_1_DRIVE_TO_GAS_STATION")
						STOP_AUDIO_SCENE("PS_1_DRIVE_TO_GAS_STATION")
					ENDIF
					START_AUDIO_SCENE("PS_1_RACE_AS_MICHAEL")
					blipDriveTo = CREATE_BLIP_FOR_COORD(vRaceFinish)
					SET_ROADS_IN_ANGLED_AREA(<<162.842850,4416.564453,46.241264>>, <<102.730545,4437.815430,82.756279>>, 12.750000, TRUE, FALSE)
					SET_ROADS_IN_ANGLED_AREA(<<84.069702,4480.673340,12.788040>>, <<-201.995392,4211.446777,99.187950>>, 131.75000, TRUE, FALSE)
					SETTIMERA(0)
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
		
			IF NOT bSequenced
			
				IF IS_VEHICLE_DRIVEABLE(vehTrevorBike)
				AND npcPed = SELECTOR_PED_TREVOR
				AND NOT IS_PED_INJURED(sSelectorPeds.pedID[npcPed])
					
					IF bBikeFrozen
						IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorBike, TRUE)
							FREEZE_ENTITY_POSITION(vehTrevorBike, FALSE)
							bBikeFrozen = FALSE
						ENDIF
					ENDIF
					
					IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[npcPed], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
					AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[npcPed], vehTrevorBike)
					AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrevorBike)
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sSelectorPeds.pedID[npcPed], KNOCKOFFVEHICLE_NEVER)
						SEQUENCE_INDEX seqTemp
						OPEN_SEQUENCE_TASK(seqTemp)
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehTrevorBike, <<-91.8094, 6431.1475, 30.3459>>, 15, DRIVINGSTYLE_NORMAL, RUFFIAN, DRIVINGMODE_STOPFORCARS_STRICT, 3, 20)
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehTrevorBike, <<-127.9359, 6405.8638, 30.4238>>, 15, DRIVINGSTYLE_NORMAL, RUFFIAN, DRIVINGMODE_AVOIDCARS, 10, 50)
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehTrevorBike, <<-390.4958, 6148.4390, 30.6151>>, 15, DRIVINGSTYLE_NORMAL, RUFFIAN, DRIVINGMODE_AVOIDCARS, 10, 50)
						CLOSE_SEQUENCE_TASK(seqTemp)
						TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqTemp)
						CLEAR_SEQUENCE_TASK(seqTemp)
						bSequenced = TRUE
					ENDIF
					IF NOT IS_AUDIO_SCENE_ACTIVE("PS_1_TREVOR_DRIVES_OFF")
					AND NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[npcPed], vehTrevorBike)
					AND VDIST2(GET_ENTITY_COORDS(sSelectorPeds.pedID[npcPed]), GET_ENTITY_COORDS(vehTrevorBike)) < 2*2
						START_AUDIO_SCENE("PS_1_TREVOR_DRIVES_OFF")
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTrevorBike, "PS_1_TREVOR_BIKE")
					ENDIF
				ENDIF
				
			ELSE
			
				IF NOT bSequenced2
					IF NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					AND NOT IS_ENTITY_DEAD(vehTrevorBike)
						IF IS_ENTITY_IN_ANGLED_AREA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<-90.507866,6423.876953,30.389437>>, <<-103.067696,6409.821777,35.491501>>, 41.500000)
							CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							SEQUENCE_INDEX seqTemp
							OPEN_SEQUENCE_TASK(seqTemp)
							TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehTrevorBike,  <<-120.4565, 6413.6021, 30.3924>>, 15, DRIVINGSTYLE_NORMAL, RUFFIAN, DRIVINGMODE_STOPFORCARS_STRICT, 5, 50)
							TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehTrevorBike, <<-390.4958, 6148.4390, 30.6151>>, 15, DRIVINGSTYLE_NORMAL, RUFFIAN, DRIVINGMODE_AVOIDCARS, 10, 50)
							CLOSE_SEQUENCE_TASK(seqTemp)
							TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqTemp)
							CLEAR_SEQUENCE_TASK(seqTemp)
							bSequenced2 = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bRecStarted
					IF NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					AND NOT IS_ENTITY_DEAD(vehTrevorBike)
						IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorBike)
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrevorBike)
							AND NOT IS_ENTITY_AT_COORD(vehTrevorBike, v_park_hide, <<40,40,40>>)
								CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehTrevorBike, 11, "RBsetup", 10, DRIVINGMODE_AVOIDCARS)
								SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sSelectorPeds.pedID[npcPed], KNOCKOFFVEHICLE_NEVER)
								bRecStarted = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			IF bSwitchAvailable
			
				SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_TREVOR, TRUE)
				PRINT_HELP_FOREVER("CBH_SWITCH")
				IF UPDATE_SELECTOR_HUD(sSelectorPeds, FALSE, TRUE)
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					sSelectorPeds.eNewSelectorPed = SELECTOR_PED_TREVOR
					sCamDetails.pedTo = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
					CLEAR_HELP()
					bSwitchTaken = TRUE
				ENDIF
				
			ENDIF
			
			IF bSwitchTaken
			
				IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails)
					IF sCamdetails.bOKToSwitchPed
					AND NOT sCamdetails.bPedSwitched
					AND sSelectorPeds.eNewSelectorPed = SELECTOR_PED_TREVOR
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
						PRINTLN("@@@@@@@@@@@@@@@ SELECTOR_PED_TREVOR @@@@@@@@@@@@@@@@@@")
						SET_VEH_RADIO_STATION(vehTrevorBike, GET_RADIO_STATION_NAME(6))
						SET_INITIAL_PLAYER_STATION(GET_RADIO_STATION_NAME(6))
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
					ENDIF
				ELSE
					PRINTLN("@@@@@@@@@@@@@@@ SET_NO_DUCKING_FOR_CONVERSATION @@@@@@@@@@@@@@@@@@")
					SET_NO_DUCKING_FOR_CONVERSATION(TRUE)						
					FREEZE_ENTITY_POSITION(vehTrevorBike, FALSE)
					bBikeFrozen = FALSE
					PRINTLN("SWITCH DONE")
					//flip audio scenes
					IF IS_AUDIO_SCENE_ACTIVE("PS_1_TREVOR_DRIVES_OFF")
						REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehTrevorBike)
						STOP_AUDIO_SCENE("PS_1_TREVOR_DRIVES_OFF")
						PRINTLN("\n\n\n\n\n\n\n STOPPING TREVOR DRIVES OFF \n\n\n\n\n\n\n")
					ENDIF
					IF IS_AUDIO_SCENE_ACTIVE("PS_1_RACE_AS_MICHAEL")
						STOP_AUDIO_SCENE("PS_1_RACE_AS_MICHAEL")
					ENDIF
					START_AUDIO_SCENE("PS_1_RACE_AS_TREVOR")
					
					//Tasks for Mike and Lester
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
						IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehPlayerCar)
							TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehPlayerCar, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_RUN)
						ENDIF
					ENDIF
					
					IF NOT IS_PED_INJURED(pedLester)
						IF NOT IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
						AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
							SET_PED_INTO_VEHICLE(pedLester, vehPlayerCar, VS_FRONT_RIGHT)
						ENDIF
					ENDIF
					
					//Clear tasks for player unless getting on bike
					IF NOT IS_ENTITY_DEAD(vehTrevorBike)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTrevorBike, TRUE)
						OR IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehTrevorBike)
							CLEAR_PED_TASKS(PLAYER_PED_ID())
						ENDIF
					ENDIF
					
					INFORM_MISSION_STATS_OF_INCREMENT(RH1_SWITCHES)
					
					//Set selector and race variables
					sCamDetails.bPedSwitched = TRUE
					sCamDetails.pedTo = NULL
					bSwitchAvailable = FALSE
					npcPed = SELECTOR_PED_MICHAEL
					playerVeh = vehTrevorBike
					npcVeh = vehPlayerCar
					
//					inform_mission_sta
					
					//kill any cams
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					REMOVE_BLIP(blipBuddy)
				ENDIF
				
			ELSE
			
				IF TIMERA() > 20000
				OR VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) > 100*100
					bSwitchAvailable = FALSE
				ENDIF
				IF NOT bAssholeLine
					IF TIMERA() > 10000
					AND DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorBike)
					AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])) < POW(30,2)
						IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_TRE1", CONV_PRIORITY_HIGH)
							bAssholeLine = TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			IF NOT bSwitchAvailable
			AND NOT bRacePedInit
			AND NOT IS_PED_INJURED(sSelectorPeds.pedID[npcPed])
			AND IS_VEHICLE_DRIVEABLE(npcVeh)
			AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
				SET_ENTITY_PROOFS(npcVeh, TRUE, TRUE, FALSE, TRUE, FALSE)
				SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[npcPed], TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sSelectorPeds.pedID[npcPed], TRUE)
				BUILD_UBER_ROUTE_DEBUG_PATH(route_path_mike, 10, "RBsetup")
				BUILD_UBER_ROUTE_DEBUG_PATH(route_path_trevor, 11, "RBsetup")
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					RESET_VEHICLE_STARTUP_REV_SOUND(vehTrevorBike)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehPlayerCar, TRUE)
					BUILD_ROUTE_FOR_GPS_ALONG_RECORDING(sTrevorRouteToMethlab, 11, "RBsetup", route_path_trevor)
					IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
						SET_VEHICLE_TYRES_CAN_BURST(vehPlayerCar, FALSE)
					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(vehTrevorBike)
						SET_VEHICLE_TYRES_CAN_BURST(vehTrevorBike, FALSE)
					ENDIF
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehTrevorBike, TRUE)
					BUILD_ROUTE_FOR_GPS_ALONG_RECORDING(sMichaelRouteToMethlab, 10, "RBsetup", route_path_mike)
				ENDIF
				SET_HORN_ENABLED(npcVeh, FALSE)
				bRacePedInit = TRUE
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[npcPed])
			
				IF bRacePedInit
					PRINTLN("Check3")
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						bSequenced = FALSE
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(), KNOCKOFFVEHICLE_HARD)
						RESET_VEHICLE_STARTUP_REV_SOUND(vehTrevorBike)
						SET_VEHICLE_DOORS_LOCKED(vehTrevorBike, VEHICLELOCK_UNLOCKED)
						IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
							SET_VEHICLE_STRONG(vehPlayerCar, TRUE)
							SET_ENTITY_PROOFS(vehPlayerCar, FALSE, TRUE, TRUE, FALSE, FALSE)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehPlayerCar, FALSE)
						ENDIF
						iStageProgress = 5
					ELSE
						iStageProgress = 4
					ENDIF
					TRIGGER_MUSIC_EVENT("RH1_RACE")
					SET_PED_CAN_SWITCH_WEAPON(sSelectorPeds.pedID[npcPed], FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[npcPed], TRUE)
					CLEAR_HELP()
					SETTIMERA(0)
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 4
			//player is Mike
			MISSION_VEHICLE_FAIL_CHECKS()
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			AND IS_VEHICLE_DRIVEABLE(vehTrevorBike)
			AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			
				IF bBikeFrozen
					IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorBike, TRUE)
						FREEZE_ENTITY_POSITION(vehTrevorBike, FALSE)
						bBikeFrozen = FALSE
					ENDIF
				ENDIF
				
				IF NOT bSequenced
					IF IS_VEHICLE_DRIVEABLE(vehTrevorBike)
					AND npcPed = SELECTOR_PED_TREVOR
					AND NOT IS_PED_INJURED(sSelectorPeds.pedID[npcPed])
						IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[npcPed], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
						AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[npcPed], vehTrevorBike)
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sSelectorPeds.pedID[npcPed], KNOCKOFFVEHICLE_NEVER)
							SEQUENCE_INDEX seqTemp
							OPEN_SEQUENCE_TASK(seqTemp)
							TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehTrevorBike, <<-91.8094, 6431.1475, 30.3459>>, 15, DRIVINGSTYLE_NORMAL, RUFFIAN, DRIVINGMODE_STOPFORCARS, 3, 20)
							TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehTrevorBike, <<-127.9359, 6405.8638, 30.4238>>, 15, DRIVINGSTYLE_NORMAL, RUFFIAN, DRIVINGMODE_AVOIDCARS, 10, 50)
							TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehTrevorBike, <<-390.4958, 6148.4390, 30.6151>>, 15, DRIVINGSTYLE_NORMAL, RUFFIAN, DRIVINGMODE_AVOIDCARS, 10, 50)
							CLOSE_SEQUENCE_TASK(seqTemp)
							TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqTemp)
							CLEAR_SEQUENCE_TASK(seqTemp)
							bSequenced = TRUE
						ENDIF
						IF NOT IS_AUDIO_SCENE_ACTIVE("PS_1_TREVOR_DRIVES_OFF")
						AND NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[npcPed], vehTrevorBike)
						AND VDIST2(GET_ENTITY_COORDS(sSelectorPeds.pedID[npcPed]), GET_ENTITY_COORDS(vehTrevorBike)) < 2*2
							START_AUDIO_SCENE("PS_1_TREVOR_DRIVES_OFF")
							ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehTrevorBike, "PS_1_TREVOR_BIKE")
						ENDIF
					ENDIF
				ELSE
					IF NOT bSequenced2
						IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
						AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorBike)
							IF IS_ENTITY_IN_ANGLED_AREA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<-90.507866,6423.876953,30.389437>>, <<-103.067696,6409.821777,35.491501>>, 41.500000)
								CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								SEQUENCE_INDEX seqTemp
								OPEN_SEQUENCE_TASK(seqTemp)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehTrevorBike,  <<-120.4565, 6413.6021, 30.3924>>, 15, DRIVINGSTYLE_NORMAL, RUFFIAN, DRIVINGMODE_AVOIDCARS, 10, 50)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehTrevorBike, <<-390.4958, 6148.4390, 30.6151>>, 15, DRIVINGSTYLE_NORMAL, RUFFIAN, DRIVINGMODE_AVOIDCARS, 10, 50)
								CLOSE_SEQUENCE_TASK(seqTemp)
								TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqTemp)
								CLEAR_SEQUENCE_TASK(seqTemp)
								bSequenced2 = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bRaceText
				AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND TIMERA() > 1000
					CLEAR_PRINTS()
					sBanterString = "RBS1_BANT1"
					PRINT_NOW("CBH_RACEM", DEFAULT_GOD_TEXT_TIME, 1)
					SETTIMERA(0)
					bRaceText = TRUE
				ENDIF
				
				IF bRaceText
				AND TIMERA() > DEFAULT_GOD_TEXT_TIME
					DO_RACE_HOME_DIALOGUE()
				ENDIF
				
				IF NOT bRecStarted
					IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorBike)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrevorBike)
						AND (NOT IS_ENTITY_AT_COORD(vehTrevorBike, v_park_hide, <<40,40,40>>) OR GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK)
							CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehTrevorBike, 11, "RBsetup", 10, DRIVINGMODE_AVOIDCARS)
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sSelectorPeds.pedID[npcPed], KNOCKOFFVEHICLE_NEVER)
							bRecStarted = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					IF NOT DOES_BLIP_EXIST(blipDriveTo)
						blipDriveTo = CREATE_BLIP_FOR_COORD(vRaceFinish)
					ENDIF
					IF DOES_BLIP_EXIST(blipVehicle)
						REMOVE_BLIP(blipVehicle)
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(blipDriveTo)
						REMOVE_BLIP(blipDriveTo)
					ENDIF
					IF NOT DOES_BLIP_EXIST(blipVehicle)
						blipVehicle = CREATE_BLIP_FOR_VEHICLE(vehPlayerCar)
					ENDIF
				ENDIF
				
				IF NOT bNpcFinished
				
					KEEP_PED_ON_REC_PATH2(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehTrevorBike, 11, "RBsetup", route_path_trevor, FALSE, 0)
					IF IS_ENTITY_IN_ANGLED_AREA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vWinAreaPos1, vWinAreaPos2, fWinAreaWidth)
						IF IS_VEHICLE_DRIVEABLE(vehTrevorBike)
							SET_ENTITY_COLLISION(vehTrevorBike, TRUE)
						ENDIF
						bNpcFinished = TRUE
					ENDIF
					
					FLOAT fPlayerProgOnNPCTrack
					fPlayerProgOnNPCTrack = GET_TIME_POSITION_IN_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(PLAYER_PED_ID()), 11, "RBsetup", route_path_trevor)
					
					IF fPlayerProgOnNPCTrack > fNPCProg																		//if the player (Mike) is going to win
					
						IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING)
							SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING, FALSE)
						ENDIF
						
						IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING)				//then if the player is less than 50m from the finish
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRaceFinish) < POW(DEFAULT_CUTSCENE_LOAD_DIST,2)
								SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING, TRUE)		//set the flag to load the mike win version
							ENDIF
						ELSE
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRaceFinish) > POW(DEFAULT_CUTSCENE_UNLOAD_DIST,2)					//if the flag for the Mike win end is loaded and the player is > 75m away
								SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING, FALSE)		//set the mike win end flag to false
							ENDIF
						ENDIF
						
					ELSE
					
						IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING)
							SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING, FALSE)
						ENDIF
						
						IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING)				//if the player isn't winning and the flag for Trevor win scene isn't set
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRaceFinish) < POW(DEFAULT_CUTSCENE_LOAD_DIST,2)					//and if the player is within 50m of the finish
								SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING, TRUE)		//load the trevor win scene
							ENDIF
						ELSE
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRaceFinish) > POW(DEFAULT_CUTSCENE_UNLOAD_DIST,2)					//if the flag for the Trevor win end is loaded and the player is > 75m away
								SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING, FALSE)		//set the Trevor win end flag to false
							ENDIF
						ENDIF
						
					ENDIF
				
				ELSE
				
					IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING)
						SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING, FALSE)
					ENDIF
					
					IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING)
						SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING, TRUE)
					ENDIF
					
					RUN_TREVOR_WAIT_LOOP()
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vWinAreaPos1, vWinAreaPos2, fWinAreaWidth)
					IF NOT IS_PED_INJURED(pedLester)
					AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedLester)) < 100*100
						KILL_FACE_TO_FACE_CONVERSATION()
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTrevorBike)
						AND NOT IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(vehTrevorBike)
							SET_PLAYBACK_TO_USE_AI(vehTrevorBike, DRIVINGMODE_AVOIDCARS)
						ENDIF
						iStageProgress = 6
					ENDIF
				ENDIF
				
				IF HAS_PED_BEEN_ABANDONED(pedLester, gtGoBackToLester)
					Mission_Failed("CBH_LLEFT")
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 5
		
			//player is Trevor
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			
				IF DOES_BLIP_EXIST(blipDriveTo)
					fPlayerProg = GET_TIME_POSITION_IN_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(PLAYER_PED_ID()), 11, "RBsetup", route_path_trevor)
					BUILD_CUSTOM_GPS_ROUTE(sTrevorRouteToMethlab, iTrevorClosestPoint, fPlayerProg)
				ENDIF
				
				FLOAT fPlayerProgOnNPCTrack
				fPlayerProgOnNPCTrack = GET_TIME_POSITION_IN_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10, "RBsetup", route_path_mike)
				
				IF NOT bAssholeLine
					IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehPlayerCar)
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])) < POW(30,2)
							IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_ASSHLEM", CONV_PRIORITY_HIGH)
								bAssholeLine = TRUE
							ENDIF
						ELSE
							bAssholeLine = TRUE
						ENDIF						
					ENDIF
				ENDIF
				
				IF NOT bRaceText
				AND bAssholeLine
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND TIMERA() > 1000
					CLEAR_PRINTS()
					sBanterString = "RBS1_BANT2"
					REMOVE_PED_FOR_DIALOGUE(sSpeech, 0)
					ADD_PED_FOR_DIALOGUE(sSpeech, 0, PLAYER_PED_ID(), "TREVOR")
					PRINT_NOW("CBH_RACET", DEFAULT_GOD_TEXT_TIME, 1)
					bRaceText = TRUE
					SETTIMERA(0)
				ENDIF
				
				IF bRaceText
				AND TIMERA() > DEFAULT_GOD_TEXT_TIME
					DO_RACE_HOME_DIALOGUE()
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehPlayerCar, FALSE)
					IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehPlayerCar)
					
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayerCar)
							IF NOT IS_ENTITY_AT_COORD(vehPlayerCar, v_park_hide, <<40,40,40>>)
								CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
								START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehPlayerCar, 10, "RBsetup", 10, DRIVINGMODE_AVOIDCARS)
							ELSE
								IF NOT bSequenced
									IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
									AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
									AND IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehPlayerCar)
										IF IS_ENTITY_IN_ANGLED_AREA(vehPlayerCar, <<-85.305443,6422.383301,30.484310>>, <<-102.256691,6405.207520,34.740208>>, 7.500000)
											VECTOR vTowardsNE
											vTowardsNE = <<-85.305443,6422.383301,30.484310>> - <<-102.256691,6405.207520,34.740208>>
											vTowardsNE.z = 0
											VECTOR vPlayerForwards
											vPlayerForwards = GET_ENTITY_FORWARD_VECTOR(vehPlayerCar)
											IF ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vTowardsNE.x, vTowardsNE.y, vPlayerForwards.x, vPlayerForwards.y))	< 90.0
												OPEN_SEQUENCE_TASK(seqMike)
												TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehPlayerCar, <<-83.4558, 6424.9326, 30.4905>>, 10, DRIVINGSTYLE_ACCURATE, PREMIER, DRIVINGMODE_PLOUGHTHROUGH, 2, 20)
												TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehPlayerCar, <<-88.8696, 6432.0767, 30.4084>>, 10, DRIVINGSTYLE_NORMAL, PREMIER, DRIVINGMODE_AVOIDCARS, 10, 100)
												TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehPlayerCar, <<-132.3165, 6403.1646, 30.4015>>, 15, DRIVINGSTYLE_NORMAL, PREMIER, DRIVINGMODE_AVOIDCARS, 10, 20)
												TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehPlayerCar, <<-412.9446, 6128.7290, 30.2517>>, 15, DRIVINGSTYLE_NORMAL, PREMIER, DRIVINGMODE_AVOIDCARS, 10, 20)
												CLOSE_SEQUENCE_TASK(seqMike)
												TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seqMike)
												CLEAR_SEQUENCE_TASK(seqMike)
												#IF IS_DEBUG_BUILD
													PRINTLN("GOING THE LONG WAY ROUND")
												#ENDIF
											ELSE
												TASK_VEHICLE_DRIVE_TO_COORD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehPlayerCar, <<-412.9446, 6128.7290, 30.2517>>, 15, DRIVINGSTYLE_NORMAL, PREMIER, DRIVINGMODE_AVOIDCARS, 10, 20)
												#IF IS_DEBUG_BUILD
													PRINTLN("FACING THE CORRECT WAY")
												#ENDIF
											ENDIF
										ELSE
											TASK_VEHICLE_DRIVE_TO_COORD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehPlayerCar, <<-412.9446, 6128.7290, 30.2517>>, 15, DRIVINGSTYLE_NORMAL, PREMIER, DRIVINGMODE_AVOIDCARS, 10, 20)
											#IF IS_DEBUG_BUILD
												PRINTLN("OUT OF TRICKY AREA")
											#ENDIF
										ENDIF
										bSequenced = TRUE
										SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
				IF NOT bNpcFinished
				
					KEEP_PED_ON_REC_PATH2(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehPlayerCar, 10, "RBsetup", route_path_mike, TRUE, fPlayerProgOnNPCTrack)
				
					IF IS_ENTITY_IN_ANGLED_AREA(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vWinAreaPos1, vWinAreaPos2, fWinAreaWidth)
						IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
							SET_ENTITY_COLLISION(vehPlayerCar, TRUE)
						ENDIF
						bNpcFinished = TRUE
					ENDIF
					
					IF fPlayerProgOnNPCTrack > fNPCProg																		//if the player (Trevor) is going to win
				
						IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING)
							SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING, FALSE)
						ENDIF
						
						IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING)				//if the player isn't winning and the flag for Mike win scene isn't set
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRaceFinish) < POW(DEFAULT_CUTSCENE_LOAD_DIST,2)					//and if the player is within 50m of the finish
								SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING, TRUE)		//load the trevor win scene
							ENDIF
						ELSE
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRaceFinish) > POW(DEFAULT_CUTSCENE_UNLOAD_DIST,2)					//if the flag for the Trevor win end is loaded and the player is > 75m away
								SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING, FALSE)		//set the Mike win end flag to false
							ENDIF
						ENDIF
					
					ELSE
					
						IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING)
							SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING, FALSE)
						ENDIF
						
						IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING)				//then if the player is less than 50m from the finish
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRaceFinish) < POW(DEFAULT_CUTSCENE_LOAD_DIST,2)
								SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING, TRUE)		//set the flag to load the Trevor win version
							ENDIF
						ELSE
							IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vRaceFinish) > POW(DEFAULT_CUTSCENE_UNLOAD_DIST,2)					//if the flag for the Trevor win end is loaded and the player is > 75m away
								SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING, FALSE)		//set the Trevor win end flag to false
							ENDIF
						ENDIF
						
					ENDIF
					
				ELSE
				
					IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING)
						SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING, FALSE)
					ENDIF
					
					IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING)
						SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING, TRUE)
					ENDIF
					
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vWinAreaPos1, vWinAreaPos2, fWinAreaWidth)
					KILL_FACE_TO_FACE_CONVERSATION()
					SETTIMERA(0)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayerCar)
					AND NOT IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(vehPlayerCar)
						IF VDIST2(GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 1000
							CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							SEQUENCE_INDEX seqTemp
							OPEN_SEQUENCE_TASK(seqTemp)
								TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehPlayerCar, <<1412.2968, 3599.6382, 33.8914>>, 20, DRIVINGSTYLE_ACCURATE, PREMIER, DRIVINGMODE_AVOIDCARS, 3, 3)
								TASK_PAUSE(NULL, 10000)
							CLOSE_SEQUENCE_TASK(seqTemp)
							TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seqTemp)
							CLEAR_SEQUENCE_TASK(seqTemp)
							SET_PLAYBACK_TO_USE_AI(vehPlayerCar, DRIVINGMODE_AVOIDCARS)
							SET_PED_KEEP_TASK(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
						ELSE
							SET_PLAYBACK_TO_USE_AI(vehPlayerCar, DRIVINGMODE_AVOIDCARS)
						ENDIF
						SET_PED_KEEP_TASK(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
					ENDIF
					SET_PED_KEEP_TASK(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
					iStageProgress++
				ENDIF
				
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
					IF DOES_BLIP_EXIST(blipVehicle)
						REMOVE_BLIP(blipVehicle)
					ENDIF
				ELSE
					IF NOT DOES_BLIP_EXIST(blipVehicle)
						IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
						AND IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
						AND NOT IS_ENTITY_IN_WATER(GET_PLAYERS_LAST_VEHICLE())
							blipVehicle = CREATE_BLIP_FOR_VEHICLE(GET_PLAYERS_LAST_VEHICLE())
						ENDIF
					ELSE
						IF NOT DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
						OR (
							DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE()) 
							AND 
							(NOT IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE()) OR IS_ENTITY_IN_WATER(GET_PLAYERS_LAST_VEHICLE()))
							)
							REMOVE_BLIP(blipVehicle)
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 6
		
			IF DOES_BLIP_EXIST(blipBuddy)
				REMOVE_BLIP(blipBuddy)
			ENDIF
		
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(npcVeh)
			AND IS_VEHICLE_DRIVEABLE(npcVeh)
				SET_PLAYBACK_SPEED(npcVeh, 1.0)
				SET_ENTITY_COLLISION(npcVeh, TRUE)
			ENDIF
			
			IF NOT bNpcFinished
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					IF IS_ENTITY_AT_COORD(npcVeh, vRaceFinish, <<10,10,10>>)
						RUN_TREVOR_WAIT_LOOP()
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT bWInLoseChatDone
			
				TEXT_LABEL sTemp
				
				IF NOT bNpcFinished
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(RH1_WINNER)
					sTemp = "RBS1_LES4"
				ELSE
					sTemp = "RBS1_LES5"
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					IF CREATE_CONVERSATION(sSpeech, sBlockId, sTemp, CONV_PRIORITY_HIGH)
						SETTIMERA(0)
						REPLAY_RECORD_BACK_FOR_TIME(10.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
						bWInLoseChatDone = TRUE
					ENDIF
				ELSE
					IF NOT bNpcFinished
						SETTIMERA(0)
						REPLAY_RECORD_BACK_FOR_TIME(10.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
						bWInLoseChatDone = TRUE
					ELSE
						IF CREATE_CONVERSATION(sSpeech, sBlockId, "RBS1_TRE5", CONV_PRIORITY_HIGH)
							REPLAY_RECORD_BACK_FOR_TIME(10.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
							bWInLoseChatDone = TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			IF NOT bCarStopped
				IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vRaceFinish, g_vAnyMeansLocate, TRUE)
							IF DOES_BLIP_EXIST(blipDriveTo)
								REMOVE_BLIP(blipDriveTo)
							ENDIF
							WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								WAIT(0)	
							ENDWHILE
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
							bCarStopped = TRUE
						ENDIF
					ENDIF
				ELSE
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vWinAreaPos1, vWinAreaPos2, fWinAreaWidth)
						bCarStopped = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF bCarStopped
				IF DOES_BLIP_EXIST(blipVehicle)
					REMOVE_BLIP(blipVehicle)
				ENDIF
				IF bWInLoseChatDone
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						IF iTrevorWaitStage > 0
							IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								VECTOR vToMike
								vToMike = GET_ENTITY_COORDS(PLAYER_PED_ID()) - GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								FLOAT fFinalHead
								fFinalHead = GET_ANGLE_BETWEEN_2D_VECTORS(0, 1, vToMike.x, vToMike.y)
								TASK_LOOK_AT_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_PED_ID(), 1000, SLF_WHILE_NOT_IN_FOV | SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
								SEQUENCE_INDEX seqTemp
								OPEN_SEQUENCE_TASK(seqTemp)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]), PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT, ENAV_STOP_EXACTLY, fFinalHead)
								TASK_PLAY_ANIM(NULL, "missheistpaletoscoresetup", "trevor_arrival_1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
								CLOSE_SEQUENCE_TASK(seqTemp)
								TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seqTemp)
								CLEAR_SEQUENCE_TASK(seqTemp)
							ENDIF
						ENDIF
						SETTIMERA(0)
						iStageProgress++
					ELSE
						IF TIMERA() > 3000
							iStageProgress++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 7
		
			IF IS_AUDIO_SCENE_ACTIVE("PS_1_RACE_AS_MICHAEL")
				STOP_AUDIO_SCENE("PS_1_RACE_AS_MICHAEL")
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("PS_1_RACE_AS_TREVOR")
				STOP_AUDIO_SCENE("PS_1_RACE_AS_TREVOR")
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF NOT bTrevorWonLine
				AND NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missheistpaletoscoresetup", "trevor_arrival_1")
						println("!!")
						PLAY_PED_AMBIENT_SPEECH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "GENERIC_SHOCKED_MED", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
						bTrevorWonLine = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
			
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
			OR TIMERA() > 3500
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					REPLAY_RECORD_BACK_FOR_TIME(10.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					Mission_Passed()
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	IF iStageProgress > 2
	
		IF DOES_BLIP_EXIST(blipDriveTo)
			IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vRaceFinish, <<1,1,g_vAnyMeansLocate.z>>, TRUE)
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				fPlayerProg = GET_TIME_POSITION_IN_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(PLAYER_PED_ID()), 11, "RBsetup", route_path_trevor)
				BUILD_CUSTOM_GPS_ROUTE(sTrevorRouteToMethlab, iTrevorClosestPoint, fPlayerProg)
			ELSE
				fPlayerProg = GET_TIME_POSITION_IN_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10, "RBsetup", route_path_mike)
				BUILD_CUSTOM_GPS_ROUTE(sMichaelRouteToMethlab, iMichaelClosestPoint, fPlayerProg)
			ENDIF
		ELSE
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				RESET_CUSTOM_GPS_ROUTE(iTrevorClosestPoint)
			ELSE
				RESET_CUSTOM_GPS_ROUTE(iMichaelClosestPoint)
			ENDIF
		ENDIF
		
	ENDIF
	
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING)
		DRAW_DEBUG_TEXT_2D("MIKE WIN CUTSCENE SET TO LOAD", <<0,0.02,0>>, 255,0,0)
	ENDIF
	
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING)
		DRAW_DEBUG_TEXT_2D("TREVOR WIN CUTSCENE SET TO LOAD", <<0,0.04,0>>, 255,0,0)
	ENDIF
	
	IF (DOES_ENTITY_EXIST(sSelectorPeds.pedID[npcPed]) 
		AND NOT IS_PED_INJURED(sSelectorPeds.pedID[npcPed]) 
		AND IS_PED_IN_ANY_VEHICLE(sSelectorPeds.pedID[npcPed]) 
		AND ARE_CHARS_IN_SAME_VEHICLE(PLAYER_PED_ID(), sSelectorPeds.pedID[npcPed]))
	OR DOES_BLIP_EXIST(blipVehicle)
	OR iStageProgress > 6
		IF DOES_BLIP_EXIST(blipBuddy)
			REMOVE_BLIP(blipBuddy)
		ENDIF
	ELSE
		BLIP_NPC(sSelectorPeds.pedID[npcPed], npcVeh)
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF HAS_PLAYER_SPOOKED_PEDS_AT_BANK(vehTrevorBike, IS_SYNCHRONIZED_SCENE_RUNNING(iOutroSceneId) AND GET_SYNCHRONIZED_SCENE_PHASE(iOutroSceneId) < 0.9)
			Mission_Failed("CBH_BNKFAILT")
		ENDIF
	ELSE
		IF HAS_PLAYER_SPOOKED_PEDS_AT_BANK(vehPlayerCar, IS_SYNCHRONIZED_SCENE_RUNNING(iOutroSceneId) AND GET_SYNCHRONIZED_SCENE_PHASE(iOutroSceneId) < 0.9)
			Mission_Failed("CBH_BNKFAIL")
		ENDIF
	ENDIF
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iOutroSceneId)
		
		IF GET_SYNCHRONIZED_SCENE_PHASE(iOutroSceneId) > 0.7
			IF NOT cars[0].bDismissed
				DISMISS_COP_CAR(0)
			ENDIF
		ENDIF
		
		IF GET_SYNCHRONIZED_SCENE_PHASE(iOutroSceneId) > 0.3
			IF NOT cars[2].bDismissed
				DISMISS_COP_CAR(2)
			ENDIF
		ENDIF
		
		IF GET_SYNCHRONIZED_SCENE_PHASE(iOutroSceneId) > 0.37
			IF bankAlarm.bOn
				bankAlarm.bOn = FALSE
				bankAlarm.iPlayerDamage = 0
				STOP_ALARM("PALETO_BAY_SCORE_ALARM", TRUE)
			ENDIF
		ENDIF
		
		IF GET_SYNCHRONIZED_SCENE_PHASE(iOutroSceneId) > 0.45
			IF NOT cars[3].bDismissed
				DISMISS_COP_CAR(3)
			ENDIF
		ENDIF
		
		IF GET_SYNCHRONIZED_SCENE_PHASE(iOutroSceneId) > 0.8
		
			IF NOT bPedsSentInside
				SET_BANK_PEDS_GO_BACK_INSIDE()
			ENDIF
			
			IF NOT cars[3].bDismissed
				DISMISS_COP_CAR(3)
			ENDIF
			
			IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L)) != DOORSTATE_LOCKED
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L), DOORSTATE_LOCKED)
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L), 0.0)
			ENDIF
			
			IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R)) != DOORSTATE_LOCKED
				DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R), DOORSTATE_LOCKED)
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R), 0.0)
			ENDIF
			
		ELSE
			DOOR_SYSTEM_SET_HOLD_OPEN(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L), TRUE)
			DOOR_SYSTEM_SET_HOLD_OPEN(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R), TRUE)
		ENDIF
		
		IF NOT bCop2SetToWalk
			IF (IS_SYNCHRONIZED_SCENE_RUNNING(iOutroSceneId) AND GET_SYNCHRONIZED_SCENE_PHASE(iOutroSceneId) > 0.828)
			OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iOutroSceneId)
				TASK_ENTER_VEHICLE(cars[i_main_cops].cops[1], cars[i_main_cops].veh, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
				FORCE_PED_MOTION_STATE(cars[i_main_cops].cops[1], MS_ON_FOOT_WALK)
				bCop2SetToWalk = TRUE
			ENDIF
		ENDIF
		
		IF NOT bCop1SetToWalk
			IF (IS_SYNCHRONIZED_SCENE_RUNNING(iOutroSceneId) AND GET_SYNCHRONIZED_SCENE_PHASE(iOutroSceneId) > 0.828)
			OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iOutroSceneId)
				TASK_ENTER_VEHICLE(cars[i_main_cops].cops[0], cars[i_main_cops].veh, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK)
				FORCE_PED_MOTION_STATE(cars[i_main_cops].cops[1], MS_ON_FOOT_WALK)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iOutroSceneId)
					STOP_SYNCHRONIZED_ENTITY_ANIM(cars[i_main_cops].veh, NORMAL_BLEND_OUT, TRUE)
				ENDIF
//				STOP_ENTITY_ANIM(cars[1].veh, "Cops_Response_Outro_CAR", "missheistpaletoscoresetup_setup3", NORMAL_BLEND_OUT)
				bCop1SetToWalk = TRUE
			ENDIF
		ENDIF
		
	ELSE
	
		IF NOT cars[i_main_cops].bDismissed
			IF IS_VEHICLE_DRIVEABLE(cars[i_main_cops].veh)
			AND NOT IS_PED_INJURED(cars[i_main_cops].cops[0])
			AND NOT IS_PED_INJURED(cars[i_main_cops].cops[1])
			AND IS_PED_SITTING_IN_VEHICLE(cars[i_main_cops].cops[0], cars[i_main_cops].veh)
			AND IS_PED_SITTING_IN_VEHICLE(cars[i_main_cops].cops[1], cars[i_main_cops].veh)
				DISMISS_COP_CAR(1)
			ENDIF
			REMOVE_ANIM_DICT("missheistpaletoscoresetup_setup1")
			REMOVE_ANIM_DICT("missheistpaletoscoresetup_setup2")
			REMOVE_ANIM_DICT("missheistpaletoscoresetup_setup3")
		ENDIF
		
	ENDIF
	
	INT iDismissCount
	REPEAT COUNT_OF(cars) iDismissCount
		IF DOES_ENTITY_EXIST(cars[iDismissCount].veh)
		AND IS_VEHICLE_DRIVEABLE(cars[iDismissCount].veh)
		AND NOT IS_PED_INJURED(cars[iDismissCount].cops[0])
		AND cars[iDismissCount].bDismissed
		AND IS_PED_IN_VEHICLE(cars[iDismissCount].cops[0], cars[iDismissCount].veh)
		AND IS_PED_IN_VEHICLE(cars[iDismissCount].cops[1], cars[iDismissCount].veh)
			IF GET_SCRIPT_TASK_STATUS(cars[iDismissCount].cops[0], SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> PERFORMING_TASK
			AND GET_SCRIPT_TASK_STATUS(cars[iDismissCount].cops[0], SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> WAITING_TO_START_TASK
			AND GET_SCRIPT_TASK_STATUS(cars[iDismissCount].cops[0], SCRIPT_TASK_VEHICLE_DRIVE_WANDER) <> DORMANT_TASK
				IF VDIST2(GET_ENTITY_COORDS(cars[iDismissCount].veh), <<119.2184, 6559.7744, 30.6237>>) < POW(250,2)
					TASK_VEHICLE_DRIVE_WANDER(cars[iDismissCount].cops[0], cars[iDismissCount].veh, 10, DRIVINGMODE_STOPFORCARS_IGNORELIGHTS | DF_SteerAroundPeds)
				ENDIF
			ENDIF
			IF VDIST2(GET_ENTITY_COORDS(cars[iDismissCount].veh), GET_ENTITY_COORDS(PLAYER_PED_ID())) > POW(100,2)
			AND IS_ENTITY_OCCLUDED(cars[iDismissCount].veh)
			AND IS_ENTITY_OCCLUDED(cars[iDismissCount].cops[0])
			AND IS_ENTITY_OCCLUDED(cars[iDismissCount].cops[1])
				DELETE_COP_CAR(cars[iDismissCount])
			ENDIF
		ENDIF
	ENDREPEAT
	
	CHECK_RACE_FAILS()
	
//	IF IS_PED_INJURED(sSelectorPeds.pedID[npcPed])
//	OR IS_ENTITY_DEAD(sSelectorPeds.pedID[npcPed])
//	OR IS_ENTITY_ON_FIRE(sSelectorPeds.pedID[npcPed])
//		IF npcPed = SELECTOR_PED_TREVOR
//			PRINTLN("\n\n\n\n\n Trevor died here1 \n\n\n\n")
//			Mission_Failed("CMN_TDIED")
//		ELSE
//			Mission_Failed("CMN_MDIED")
//		ENDIF
//	ENDIF
	
	INT iTemp
	REPEAT COUNT_OF(bank_peds) iTemp
		IF DOES_ENTITY_EXIST(bank_peds[iTemp])
			IF IS_ENTITY_AT_COORD(bank_peds[iTemp], <<-100.0933, 6461.7124, 30.6267>>, <<3,3,3>>)
			AND IS_ENTITY_OCCLUDED(bank_peds[iTemp])
				DELETE_PED(bank_peds[iTemp])
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT bBankDumped
	AND iStageProgress > 3
		IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_bank_door, <<100, 100, 100>>)
			REPEAT COUNT_OF(bank_peds) iTemp
				IF DOES_ENTITY_EXIST(bank_peds[iTemp])
					SET_PED_AS_NO_LONGER_NEEDED(bank_peds[iTemp])
				ENDIF
			ENDREPEAT
			REPEAT COUNT_OF(cars) iTemp
				IF DOES_ENTITY_EXIST(cars[iTemp].cops[0])
					SET_PED_AS_NO_LONGER_NEEDED(cars[iTemp].cops[0])
				ENDIF
				IF DOES_ENTITY_EXIST(cars[iTemp].cops[1])
					SET_PED_AS_NO_LONGER_NEEDED(cars[iTemp].cops[1])
				ENDIF
				IF DOES_ENTITY_EXIST(cars[iTemp].veh)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(cars[iTemp].veh)
				ENDIF
			ENDREPEAT
			IF DOES_ENTITY_EXIST(pedBiker)
				SET_PED_AS_NO_LONGER_NEEDED(pedBiker)
			ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_BUSINESS_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_BUSINESS_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(SHERIFF)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_COP_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_Y_GENSTREET_02)
			REMOVE_ANIM_DICT("missheistpaletoscoresetup_setup1")
			REMOVE_ANIM_DICT("missheistpaletoscoresetup_setup2")
			REMOVE_ANIM_DICT("missheistpaletoscoresetup_setup3")
			bBankDumped = TRUE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[npcPed])
			DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sSelectorPeds.pedID[npcPed]))
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iStageProgress), <<0.02, 0.8, 0>>)
		ENDIF
	#ENDIF
	
ENDPROC

PROC RUN_BUS_STOP_CUTSCENE()

	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_SCENARIO_PED_DENSITY_MULTIPLIER_THIS_FRAME(1.0, 0.0)
	
	IF NOT bCutsceneLoaded
	
		IF NOT IS_CUTSCENE_ACTIVE()
			REQUEST_CUTSCENE("rbhs_mcs_1")
		ENDIF
		
		IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE("rbhs_mcs_1")
				IF NOT DOES_ENTITY_EXIST(vehBus)
					CREATE_BUS(<<-397.9000, 6134.1348, 31.5204>>, -45.281322)
				ENDIF
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				IF NOT DOES_ENTITY_EXIST(pedLester)
					REGISTER_ENTITY_FOR_CUTSCENE(pedLester, "Lester", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_LESTERCREST)
				ELIF NOT IS_PED_INJURED(pedLester)
					REGISTER_ENTITY_FOR_CUTSCENE(pedLester, "Lester", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
					SET_VEHICLE_DOORS_SHUT(vehPlayerCar)
					SET_VEHICLE_DOOR_CONTROL(vehPlayerCar, SC_DOOR_FRONT_LEFT, DT_DOOR_INTACT, 0)
					SET_VEHICLE_DOOR_CONTROL(vehPlayerCar, SC_DOOR_FRONT_RIGHT, DT_DOOR_INTACT, 0)
					SET_VEHICLE_DOOR_CONTROL(vehPlayerCar, SC_DOOR_REAR_RIGHT, DT_DOOR_INTACT, 0)
					SET_VEHICLE_DOOR_CONTROL(vehPlayerCar, SC_DOOR_REAR_LEFT, DT_DOOR_INTACT, 0)
					REGISTER_ENTITY_FOR_CUTSCENE(vehPlayerCar, "Michaels_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehPlayerCar, TRUE, TRUE, TRUE)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehBus)
					REGISTER_ENTITY_FOR_CUTSCENE(vehBus, "RBHS_bus", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF NOT IS_PED_INJURED(pedBusDriver)
					REGISTER_ENTITY_FOR_CUTSCENE(pedBusDriver, "S_M_M_LSMetro_01", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				SET_ROADS_IN_AREA(v_busstop_car_park + <<100,100,100>>, v_busstop_car_park - <<100,100,100>>, FALSE)
				SET_PED_PATHS_IN_AREA(v_busstop_car_park + <<100,100,100>>, v_busstop_car_park - <<100,100,100>>, FALSE)
				START_CUTSCENE()
				
				REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ThePaletoScoreSetup")
				
				WAIT(0)
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehBus)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehBus)
				ENDIF
				IF IS_AUDIO_SCENE_ACTIVE("PS_1_BUS_ARRIVES")
					STOP_AUDIO_SCENE("PS_1_BUS_ARRIVES")
				ENDIF
				
				CLEAR_AREA(v_busstop_car_park, 500.0, TRUE)
				ASSET_MANAGER(stage_drive_to_bank, TRUE, FALSE)
				INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_START()
				
				DO_SCREEN_FADE_IN(500)
				
				bCutsceneLoaded = TRUE
			ENDIF
		ENDIF
		
	ELSE
	
		IF NOT DOES_ENTITY_EXIST(pedLester)
		AND DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester"))
			pedLester = GET_PED_INDEX_FROM_ENTITY_INDEX((GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester")))
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehBus)
			IF NOT IS_ENTITY_VISIBLE(vehBus)
				SET_ENTITY_VISIBLE(vehBus, TRUE)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				PRINTLN("Mike out")
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					PRINTLN("setting mike into veh")
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
					PRINTLN("Trevor out")
					IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar)
						PRINTLN("setting Trevor into veh")
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar, VS_BACK_RIGHT)
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(pedLester)
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lester")
					PRINTLN("Lester out")
					IF NOT IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
						PRINTLN("setting Lester into veh")
						SET_PED_INTO_VEHICLE(pedLester, vehPlayerCar, VS_FRONT_RIGHT)
					ENDIF
				ENDIF
			ENDIF
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_car")
				SET_VEHICLE_DOORS_SHUT(vehPlayerCar)
				CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(1)
				PRINTLN("car out")
			ENDIF
		ENDIF
		
		IF NOT IS_CUTSCENE_ACTIVE()
			REPLAY_STOP_EVENT()
			IF DOES_ENTITY_EXIST(pedBusDriver)
			AND DOES_ENTITY_EXIST(vehBus)
				IF NOT IS_PED_INJURED(pedBusDriver)
				AND IS_VEHICLE_DRIVEABLE(vehBus)
					SET_PED_INTO_VEHICLE(pedBusDriver, vehBus)
				ENDIF
			ENDIF
			CASCADE_SHADOWS_ENABLE_ENTITY_TRACKER(TRUE)
			SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE, FALSE, 0, TRUE, FALSE)
			INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			bCutsceneLoaded = FALSE
			SET_ROADS_BACK_TO_ORIGINAL(v_busstop_car_park + <<100,100,100>>, v_busstop_car_park - <<100,100,100>>)
			SET_PED_PATHS_IN_AREA(v_busstop_car_park + <<100,100,100>>, v_busstop_car_park - <<100,100,100>>, TRUE)
			GO_TO_STAGE(stage_drive_to_bank)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			DEBUG_CUTSCENE_CONTROL()
		#ENDIF
		
	ENDIF
	
ENDPROC

//╞═════════╡DEBUG PROCEDURES

#IF IS_DEBUG_BUILD

	PROC DEBUG_PROCS()
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_PASSED()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			Mission_Failed("CBH_FAIL")
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			JSKIP()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			PSKIP()
		ENDIF
		IF LAUNCH_MISSION_STAGE_MENU(skipMenuOptions, iStageHolder)
			IF iStageHolder = 8
				GO_TO_STAGE(stage_test)
			ELSE
				GO_TO_STAGE(INT_TO_ENUM(MISSION_STAGE_FLAG, iStageHolder))
				bSkip = TRUE
			ENDIF
		ENDIF
	ENDPROC
	
	VEHICLE_INDEX vehTest
	
	PROC GRAB_TEST()
		SWITCH iStageProgress
			CASE 0
				FADE_OUT()
				IF IS_SCREEN_FADED_OUT()
					iStageProgress++
					SETTIMERA(0)
				ENDIF
			BREAK
			CASE 1
				CLEAR_AREA(<<-113.1941, 6461.0596, 30.4685>>, 300, TRUE)
//				NEW_LOAD_SCENE_START_SPHERE(<<-113.1941, 6461.0596, 30.4685>>, 50.0)
//				WHILE NOT IS_NEW_LOAD_SCENE_LOADED() AND TIMERA() < 5000
//					WAIT(0)
//				ENDWHILE
//				NEW_LOAD_SCENE_STOP()
				DO_PLAYER_WARP_WITH_LOAD(<<-113.1941, 6461.0596, 30.4685>>, 144)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-113.1941, 6461.0596, 30.4685>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 144.0543)
				DELETE_ALL_BLIPS()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				CLEAR_PRINTS()
				iStageProgress++
			BREAK
			CASE 2
				REQUEST_MODEL(SHERIFF)
				REQUEST_VEHICLE_RECORDING(i_main_cops+1, s_rec_tag)
				IF HAS_MODEL_LOADED(SHERIFF)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(i_main_cops+1, s_rec_tag)
					vehTest = CREATE_VEHICLE(SHERIFF, <<-119.8739, 6454.0259, 30.4456>>, 135.6175)
					START_PLAYBACK_RECORDED_VEHICLE(vehTest, i_main_cops+1, s_rec_tag)
					iStageProgress++
				ENDIF
			BREAK
			CASE 3
				IF IS_VEHICLE_DRIVEABLE(vehTest)
				AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTest)
					SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(vehTest)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					FADE_IN()
					iStageProgress++
				ENDIF
			BREAK
			CASE 4
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
			BREAK
		ENDSWITCH
	ENDPROC
	
	PROC CLEANUP_TEST()
		DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		DELETE_VEHICLE(vehTrevorBike)
		iStageProgress=0
	ENDPROC
	
	
	
#ENDIF

PROC RUN_METHLAB_PTFX()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1398.830933,3609.659424,37.989201>>, <<1387.375366,3605.515625,41.085896>>, 15.750000)
		IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxMethlabDust)
			REQUEST_PTFX_ASSET()
			IF HAS_PTFX_ASSET_LOADED()
				ptfxMethlabDust = START_PARTICLE_FX_LOOPED_AT_COORD("cs_rbhs_int_delap_dust", <<1392.17175, 3604.29932, 39.19458>>, <<0, 0, 0>>)
			ENDIF
		ENDIF
	ELSE
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxMethlabDust)
			STOP_PARTICLE_FX_LOOPED(ptfxMethlabDust)
			REMOVE_PTFX_ASSET()
		ENDIF
	ENDIF
ENDPROC

INT iLastExileAreaCheck
INT iExileWarningLevel

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED()
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING, FALSE)
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING, FALSE)
		Mission_Flow_Mission_Force_Cleanup()
		Mission_Cleanup()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	INITIALISE()
	REQUEST_VEHICLE_ASSET(PREMIER)
	
	WHILE (TRUE)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ThePaletoScoreSetup")
		
		IF bPedsSentInside
			INT iIndex
			FOR iIndex = 0 TO 3
				IF DOES_ENTITY_EXIST(bank_peds[iIndex])
				AND NOT IS_PED_INJURED(bank_peds[iIndex])
					FLOAT fReplayBlockRange = REPLAY_GET_MAX_DISTANCE_ALLOWED_FROM_PLAYER() + 5.0
					
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(bank_peds[iIndex], FALSE)) < (fReplayBlockRange*fReplayBlockRange)	//30.0f range for replay recording
						REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2222035
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
		
		IF NOT b_STAGE_READY
			IF current_mission_stage <> stage_test
				INIT_MISSION_STAGE(current_mission_stage)
			ENDIF
			b_STAGE_READY = TRUE
		ENDIF
		
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			PRINTLN("Radio station is: ", GET_PLAYER_RADIO_STATION_INDEX())
//		ENDIF
		
		SWITCH current_mission_stage
		
			CASE stage_intro_scene
				RUN_INTRO_CUTSCENE()
			BREAK
			
			CASE stage_drive_to_bus
				DRIVE_TO_BUS_STOP()
				UPDATE_AMBIENT_BANK()
			BREAK
			
			CASE stage_bus_stop_scene
				RUN_BUS_STOP_CUTSCENE()
			BREAK
			
			CASE stage_drive_to_bank
				DRIVE_TO_BANK()
				UPDATE_AMBIENT_BANK()
			BREAK
			
			CASE stage_box_reveal_scene
				RUN_BANK_RECON_IN_GAME()
			BREAK
			
			CASE stage_shoot_box
				SHOOT_BOX()
				UPDATE_AMBIENT_BANK()
			BREAK
			
			CASE stage_hide
				HIDE2()
			BREAK
			
			CASE stage_race_home
				CHECK_FOR_ROAD_BLOCKS()
				RACE_HOME()
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-986.004150,5367.315430,50.055317>>, <<-1003.946289,5402.259277,39.605061>>, 24.250000)
				OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1511.873291,6236.630371,-81.146416>>, <<1527.349731,6704.018066,71.625160>>, 215.500000)
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						Mission_Failed("CBH_COPFAIL")
					ELSE
						Mission_Failed("CBH_COPFAILT")
					ENDIF
				ENDIF
			BREAK
			
			CASE stage_lose_the_cops
				LOSE_THE_COPS()
			BREAK
			
			CASE stage_hide_fail
				DO_HIDE_FAIL()
			BREAK
			
			CASE stage_buy_gun
				PLAYER_GOES_TO_AMMUNATION_AND_BUYS_A_GUN()
			BREAK
			
			CASE stage_test
				#IF IS_DEBUG_BUILD
					GRAB_TEST()
				#ENDIF
			BREAK
			
			CASE stage_security_fail
			
				IF (GET_GAME_TIMER() - iTimeOfSecurityFail) > 1000
					IF NOT bankAlarm.bOn
						bankAlarm.bOn = TRUE
					ENDIF
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					CLEAR_PRINTS()
				ENDIF
				
				IF (GET_GAME_TIMER() - iTimeOfSecurityFail) > 2000
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					ENDIF
				ENDIF
				
				IF (GET_GAME_TIMER() - iTimeOfSecurityFail) > 500
					INT iTemp
					REPEAT COUNT_OF(bank_peds) iTemp
						IF NOT IS_PED_INJURED(bank_peds[iTemp])
						AND NOT IS_PED_FLEEING(bank_peds[iTemp])
							IF (GET_GAME_TIMER() - iTimeOfSecurityFail) > 500 + GET_RANDOM_INT_IN_RANGE(0, 1500)
								CLEAR_PED_TASKS(bank_peds[iTemp])
								TASK_SMART_FLEE_PED(bank_peds[iTemp], PLAYER_PED_ID(), 200, 200000)
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				IF (GET_GAME_TIMER() - iTimeOfSecurityFail) > 3500
					PRINTLN("\n\n\n\n FAILED HERE 2\n\n\n\n")
					Mission_Failed("CBH_BNKFAIL")
				ENDIF
				
			BREAK
			
		ENDSWITCH
		
		IF IS_PLAYER_IN_THE_BANK()
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				Mission_Failed("CBH_BNKFAIL")
			ELSE
				Mission_Failed("CBH_BNKFAILT")
			ENDIF
		ENDIF
		
		IF IS_GAMEPLAY_HINT_ACTIVE()
			IF NOT IS_AUDIO_SCENE_ACTIVE("PS_1_FOCUS_ON_BANK")
				START_AUDIO_SCENE("PS_1_FOCUS_ON_BANK")
			ENDIF
		ELSE
			IF IS_AUDIO_SCENE_ACTIVE("PS_1_FOCUS_ON_BANK")
				STOP_AUDIO_SCENE("PS_1_FOCUS_ON_BANK")
			ENDIF
		ENDIF
		
		MANAGE_ALARM_BOX()
		MANAGE_VAULT_DOOR()
		
		IF DOES_ENTITY_EXIST(pedLester)
			IF IS_PED_INJURED(pedLester)
			OR IS_ENTITY_DEAD(pedLester)
			OR IS_ENTITY_ON_FIRE(pedLester)
				Mission_Failed("CBH_LESTDED")
			ENDIF
		ENDIF
		
		IF current_mission_stage < stage_race_home
		OR current_mission_stage = stage_lose_the_cops
			MISSION_VEHICLE_FAIL_CHECKS()
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				IF IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				OR IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				OR IS_ENTITY_ON_FIRE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					PRINTLN("\n\n\n\n\n Trevor died here 3 \n\n\n\n")
					Mission_Failed("CMN_TDIED")
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_GAME_TIMER() - iLastExileAreaCheck > 3000
			BOOL bCity = FALSE
			IF CITY_EXILE_AREA_CHECK(GET_ENTITY_COORDS(PLAYER_PED_ID()),bCity)
				IF bCity
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					OR DOES_ENTITY_EXIST(pedLester)
						PED_INDEX pedTemp
						IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							pedTemp = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
						ENDIF
						IF DOES_ENTITY_EXIST(pedLester) 
							pedTemp = pedLester
						ENDIF
						IF NOT IS_PED_INJURED(pedTemp)
							IF ARE_CHARS_IN_SAME_VEHICLE(pedTemp, PLAYER_PED_ID())
							OR (NOT IS_PED_IN_ANY_VEHICLE(pedTemp) AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedTemp)) < 20*20)
								TEXT_LABEL_23 sTemp
								IF iExileWarningLevel = 0
									sTemp = "RBS1_"
									IF pedTemp = pedLester
										sTemp += "LCITY1"
									ELSE
										sTemp += "TCITY1"
									ENDIF	
									IF CREATE_CONVERSATION(sSpeech, sBlockId, sTemp, CONV_PRIORITY_HIGH)
										iExileWarningLevel++
									ENDIF
								ELIF iExileWarningLevel = 1
									sTemp = "RBS1_"
									IF pedTemp = pedLester
										sTemp += "LCITY2"
									ELSE
										sTemp += "TCITY2"
									ENDIF
									IF CREATE_CONVERSATION(sSpeech, sBlockId, sTemp, CONV_PRIORITY_HIGH)
										iExileWarningLevel++
									ENDIF
								ELIF iExileWarningLevel = 2
									MISSION_FAILED("CBH_CFAIL")
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
			iLastExileAreaCheck = GET_GAME_TIMER()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD1)
				REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
				DEBIT_BANK_ACCOUNT(CHAR_MICHAEL, BAAC_UNLOGGED_SMALL_ACTION, GET_ACCOUNT_BALANCE(BANK_ACCOUNT_MICHAEL))
			ENDIF
			IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD2)
				REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID())
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, 1)
			ENDIF
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				DRAW_DEBUG_TEXT("SELECTOR PED MIKE", GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]) + <<0,0,1.5>>, 0,255,0)
			ENDIF
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				DRAW_DEBUG_TEXT("SELECTOR PED TREV", GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) + <<0,0,1.5>>, 255,0,0)
			ENDIF
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				DRAW_DEBUG_TEXT("SELECTOR PED FRANK", GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) + <<0,0,1.5>>)
			ENDIF
			DEBUG_PROCS()
		#ENDIF
		
		RUN_METHLAB_PTFX()
		
		IF NOT b_security_fail
			IF HAS_PLAYER_ALERTED_BANK_SECURITY()
				INT iTemp
				REPEAT COUNT_OF(bank_peds) iTemp
					IF NOT IS_PED_INJURED(bank_peds[iTemp])
						TASK_REACT_AND_FLEE_PED(bank_peds[iTemp], PLAYER_PED_ID())
					ENDIF
				ENDREPEAT
				PRINTLN("\n\n\n\n IN HERE 1\n\n\n\n")
				b_security_fail = TRUE
				bankAlarm.bOn = TRUE
				DELETE_ALL_BLIPS()
				iTimeOfSecurityFail = GET_GAME_TIMER()
				current_mission_stage = stage_security_fail
			ENDIF
		ENDIF
		
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		AND NOT b_security_fail
			IF current_mission_stage > stage_intro_scene
			AND current_mission_stage < stage_hide
				LOSE_THE_COPS()
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			IF current_mission_stage = stage_drive_to_bus
				IF NOT bParked
				AND iStageProgress > 1
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar)
						AND GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
							TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVE_RUN)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF current_mission_stage = stage_drive_to_bank
				OR current_mission_stage = stage_shoot_box
				OR current_mission_stage = stage_hide
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar)
						AND GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
							TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar, DEFAULT_TIME_NEVER_WARP, VS_BACK_RIGHT, PEDMOVE_RUN)
						ENDIF
					ENDIF
					IF NOT IS_PED_INJURED(pedLester)
						IF NOT IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
						AND GET_SCRIPT_TASK_STATUS(pedLester, SCRIPT_TASK_ENTER_VEHICLE) <> PERFORMING_TASK
							TASK_ENTER_VEHICLE(pedLester, vehPlayerCar, DEFAULT_TIME_NEVER_WARP, VS_FRONT_RIGHT, PEDMOVE_RUN)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		 
		IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			IF NOT IS_PED_INJURED(pedLester)
				IF IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
					SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(pedLester, FALSE)
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehPlayerCar)
					SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
				ENDIF
			ENDIF
		ENDIF
		
		IF bSkip
			DRAW_DEBUG_TEXT_2D("SKIP IS ACTIVE", <<0.9, 0.02, 0>>)
		ENDIF
		
		WAIT(0)
		
	ENDWHILE
	
ENDSCRIPT
