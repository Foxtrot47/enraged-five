// _________________________________________________________________________________________
//
//									Rural Bank Heist 2A /2B
//								  Ross Wallace 26/03/2010
//
// ____________________________________ INCLUDES ___________________________________________

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_script.sch"
USING "script_heist.sch"
USING "commands_fire.sch"
USING "selector_public.sch"
USING "flow_public_core_override.sch"
USING "commands_cutscene.sch"
USING "script_blips.sch" 
USING "Dialogue_public.sch"
USING "locates_public.sch"
USING "player_ped_data.sch"
USING "replay_public.sch"
USING "building_control_public.sch"
USING "script_ped.sch"
USING "vehicle_gen_public.sch"
USING "cutscene_public.sch"
USING "rappel_public.sch"
USING "script_heist.sch"
USING "CompletionPercentage_public.sch"
USING "taxi_functions.sch"
USING "commands_shapetest.sch"
USING "timeLapse.sch"
USING "shared_hud_displays.sch"
USING "flow_public_core.sch"
USING "rgeneral_include.sch"
USING "heist_end_screen.sch"
USING "achievement_public.sch"
USING "lineactivation.sch"
USING "cheat_controller_public.sch"

BOOL bHasChangedClothesMichael
BOOL bHasChangedClothesTrevor

CONST_INT REPLAY_VALUE_CURRENT_TAKE 		0
CONST_INT REPLAY_VALUE_GAS_STATION_STATE 	1
CONST_INT REPLAY_VALUE_PICKEDUP_BUDDYS_BAG 	2

#IF IS_DEBUG_BUILD

	USING "shared_debug.sch"
	USING "select_mission_stage.sch"
	USING "script_debug.sch"
	
	BOOL bIsSuperDebugEnabled
	
	FLOAT debugFloat1
	FLOAT debugFloat2
	
	BOOL bTriggeredByDebugDoForceLoad
	WIDGET_GROUP_ID ruralHeistWidgetGroup
	
	BOOL bDebugInitialised
	BOOL bRunDebugLocates
	VECTOR vLocateDimensions
	VECTOR vPlayersCoords
	INT iDebugMissionStage
	
	CONST_INT MAX_SKIP_MENU_LENGTH 21	                   // number of stages in mission + 2 (for menu )
	
	MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]      // struct containing the debug menu 
	
	STRING strMissionName
	VECTOR vDebugForce = <<0.0, 0.0, 10.0>>
	VECTOR vDebugOffset = <<0.0, -4.0, 00.0>>
	
	//BOOL bJSkipInProgress
	
	INT iPedCount
	INT iVehicleCount
	INT iObjectCount
#ENDIF

INT iWidgetCopCarStartingHealth = 950
INT iWidgetCopCarExplosionHealth = 125

INTERIOR_INSTANCE_INDEX chickenFactoryInterior
INT iReturnStage								// mission stage to jump to

//INT iTimerShouldSwitchToMichael

MODEL_NAMES mnCopPedModel = S_M_Y_SHERIFF_01 	// Cop Ped Model
MODEL_NAMES mnSwatPedModel = S_M_Y_SWAT_01 		// Swat Ped Model
MODEL_NAMES mnCopCarModel = SHERIFF 			// Cop Car Model

OBJECT_INDEX oiBankDoorLeft
OBJECT_INDEX oiBankDoorRight

OBJECT_INDEX oiBagTrevor

STRUCT SET_PIECE_COP
	
	VEHICLE_INDEX thisCar
	PED_INDEX thisDriver
	PED_INDEX thisPassenger
	PED_INDEX thisPassenger2
	BLIP_INDEX blipDriver
	BLIP_INDEX blipPassenger
	
ENDSTRUCT



ENUM MISSION_STAGE_FLAG 

	STAGE_TIME_LAPSE,								//0
	STAGE_INITIALISE,								//1
	STAGE_INTRO_MOCAP,								//2
	STAGE_DEAL_WITH_FRANKLIN,						//3
	STAGE_GET_TO_BANK,								//4
	STAGE_ROB_THE_BANK,								//5					
	STAGE_HEIST_CUTSCENE,							//6
	STAGE_HOLD_OFF_COPS_AT_BANK,					//7
	STAGE_CHOPPER_TURNS_UP,							//8
	STAGE_ADVANCE_TO_GARDENS,						//9
	STAGE_CONTINUE_DOWN_STREET,						//10
	STAGE_PROCEED_TO_ALLEY,							//11
	STAGE_HOLD_OFF_ARMY_AT_JUNK_YARD,				//12
	STAGE_HOT_SWAP_TO_BULLDOZER,					//13
	STAGE_SWAPPED_TO_BULLDOZER,						//14
	STAGE_RESCUE_BUDDYS,							//15
	STAGE_CHICKEN_FACTORY, 							//16
	STAGE_TRAIN_PLATFORM,							//17
	STAGE_END_MOCAP_SCENE,							//18
	STAGE_ALL_DRIVE_OFF,							//19
	STAGE_END_SCREEN								//20
ENDENUM

MISSION_STAGE_FLAG mission_stage = STAGE_TIME_LAPSE  //STAGE_INITIALISE
MISSION_STAGE_FLAG skip_mission_stage	//For debug and replays.


CONST_INT	iCONST_MICHAEL_FENCE1_RECORDING_NODE	10
CONST_INT	iCONST_GUNMAN_FENCE1_RECORDING_NODE		22

CONST_INT	iCONST_MICHAEL_FENCE2_RECORDING_NODE	24
CONST_INT	iCONST_GUNMAN_FENCE2_RECORDING_NODE		39


ENUM CHICKEN_FACTORY_PLAYER_SWITCH_STATE
	SWITCH_SETUP,
	SWITCH_SELECTING, 
	SWITCH_SWAP_PLAYER_PEDS,
	SWITCH_COMPLETE
ENDENUM
CHICKEN_FACTORY_PLAYER_SWITCH_STATE switchStateChickenFactory

ENUM ALLY_BANK_HOLD_UP_STATE
	ALLY_HOLD_UP_INIT,
	ALLY_HOLD_UP_WALK_TO_BUSHES,
	ALLY_HOLD_UP_WAIT_TO_MOVE_TO_MOTEL,
	ALLY_HOLD_UP_TASK_TO_MOTEL,
	ALLY_HOLD_UP_WAIT_AT_DOORWAY,
	ALLY_HOLD_UP_DONE
ENDENUM
ALLY_BANK_HOLD_UP_STATE	eAllyHoldUpState

USING "rural_bank_heist_support.sch"
USING "rural_bank_heist_stage_rob_the_bank.sch"
USING "rural_bank_heist_stage_advance_to_garden.sch"
USING "rural_bank_heist_stage_down_street.sch"
USING "rural_bank_heist_stage_rescue_buddies.sch"
USING "rural_bank_heist_stage_chicken_factory.sch"
USING "rural_bank_heist_stage_train_platform.sch"


PROC CREATE_BANK_PARKED_CARS()
	vehBankParkedCars[0] = CREATE_VEHICLE(WASHINGTON, (<<-115.2128, 6446.1265, 30.4672>>), 137.7988)
	IF DOES_ENTITY_EXIST(vehBankParkedCars[0])
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBankParkedCars[0], FALSE)
		SET_ENTITY_HEALTH(vehBankParkedCars[0], iWidgetCopCarStartingHealth)
	ENDIF
	
//	vehBankParkedCars[1] = CREATE_VEHICLE(WASHINGTON, (<<-128.0256, 6456.7866, 30.4684>>), 148.0000)
//	IF DOES_ENTITY_EXIST(vehBankParkedCars[1])
//		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBankParkedCars[1], FALSE)
//		SET_ENTITY_HEALTH(vehBankParkedCars[1], iWidgetCopCarStartingHealth)
//	ENDIF
	
ENDPROC


PROC CREATE_COP_CARS_HOLD_OFF_COPS_AT_BANK()
	INT i
	
	CopCarsParked[0] = CREATE_VEHICLE(mnCopCarModel, << -102.6380, 6442.3481, 30.4864 >>   , 11.6552)
	CopCarsParked[1] = CREATE_VEHICLE(mnCopCarModel, << -126.5993, 6447.2339, 30.2360 >>, 4.0486)

	pedsCopsOutsideBank[0] = CREATE_PED(PEDTYPE_MISSION, mnCopPedModel , <<-114.8641, 6438.6440, 30.5844>>, 301.4686)
	pedsCopsOutsideBank[1] = CREATE_PED(PEDTYPE_COP, mnCopPedModel , <<-143.5829, 6462.7627, 30.4849>>, 227.6863)
	pedsCopsOutsideBank[2] = CREATE_PED(PEDTYPE_COP, mnCopPedModel , <<-116.9066, 6439.1030, 30.6178>>, 21.3333 )
	pedsCopsOutsideBank[3] = CREATE_PED(PEDTYPE_COP, mnCopPedModel ,  << -101.9852, 6439.2788, 30.3229 >>, 73.7334)
	pedsCopsOutsideBank[4] = CREATE_PED(PEDTYPE_COP, mnCopPedModel , << -101.5087, 6443.3774, 30.4827 >>  , 78.4434)
	pedsCopsOutsideBank[5] = CREATE_PED(PEDTYPE_COP, mnCopPedModel ,<< -128.2090, 6447.4385, 30.5076 >>, 324.2866)
	pedsCopsOutsideBank[6] = CREATE_PED(PEDTYPE_COP, mnCopPedModel , << -126.8094, 6444.1953, 30.5353 >>, 310.5773)
	
	FOR i = 0 TO NUMBER_OF_COP_CARS_OUTSIDE_BANK - 1
		IF DOES_ENTITY_EXIST(CopCarsParked[i])
			SET_VEHICLE_SIREN(CopCarsParked[i], TRUE)
			SET_SIREN_WITH_NO_DRIVER(CopCarsParked[i], TRUE)
			SET_VEHICLE_DOORS_LOCKED(CopCarsParked[i], VEHICLELOCK_LOCKED)
			//SET_ENTITY_HEALTH(CopCarsParked[i], 150)
			
			CopCarsParkedPrevHealth[i] = GET_ENTITY_HEALTH(CopCarsParked[i])
			
			TEXT_LABEL_63 debugCarName = "CopCar "
			debugCarName += i
		
			SET_VEHICLE_ON_GROUND_PROPERLY(CopCarsParked[i])
		
			SET_VEHICLE_NAME_DEBUG(CopCarsParked[i], debugCarName)
		ENDIF
	ENDFOR

	FOR i = 0 TO NUMBER_OF_COPS_OUTSIDE_BANK - 1
		IF DOES_ENTITY_EXIST(pedsCopsOutsideBank[i])

			TEXT_LABEL_63 debugPedName = "CopPed "
			debugPedName += i

			SET_PED_NAME_DEBUG(pedsCopsOutsideBank[i], debugPedName)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedsCopsOutsideBank[i], TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsCopsOutsideBank[i], TRUE)	
			GIVE_WEAPON_TO_PED(pedsCopsOutsideBank[i], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
			TASK_AIM_GUN_AT_COORD(pedsCopsOutsideBank[i], << -113.4377, 6460.4385, 30.4685>>, 7500, TRUE)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedsCopsOutsideBank[i], TRUE)
			SET_PED_KEEP_TASK(pedsCopsOutsideBank[i], TRUE)
			
			SET_PED_RESET_FLAG(pedsCopsOutsideBank[i], PRF_InstantBlendToAim, TRUE)
			
			//Random shades
			SET_PED_PROP_INDEX(pedsCopsOutsideBank[i], INT_TO_ENUM(PED_PROP_POSITION,1), GET_RANDOM_INT_IN_RANGE(0, 2), 0)	
			
			SET_PED_PROP_INDEX(pedsCopsOutsideBank[i], INT_TO_ENUM(PED_PROP_POSITION,0), GET_RANDOM_INT_IN_RANGE(0, 1), 0)	
			
			IF GET_RANDOM_BOOL()
				CLEAR_PED_PROP(pedsCopsOutsideBank[i], INT_TO_ENUM(PED_PROP_POSITION,0))
			ELSE
				CLEAR_PED_PROP(pedsCopsOutsideBank[i], INT_TO_ENUM(PED_PROP_POSITION,1))
			ENDIF
						
		ENDIF
	ENDFOR

	SET_PED_COMPONENT_VARIATION(pedsCopsOutsideBank[5], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
	SET_PED_COMPONENT_VARIATION(pedsCopsOutsideBank[5], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(pedsCopsOutsideBank[5], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(pedsCopsOutsideBank[5], INT_TO_ENUM(PED_COMPONENT,5), 1, 0, 0) //(hand)
	SET_PED_COMPONENT_VARIATION(pedsCopsOutsideBank[5], INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
	SET_PED_PROP_INDEX(pedsCopsOutsideBank[5], INT_TO_ENUM(PED_PROP_POSITION,0), 1, 0)
	
	SET_PED_COMPONENT_VARIATION(pedsCopsOutsideBank[6], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
	SET_PED_COMPONENT_VARIATION(pedsCopsOutsideBank[6], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
	SET_PED_COMPONENT_VARIATION(pedsCopsOutsideBank[6], INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
	SET_PED_COMPONENT_VARIATION(pedsCopsOutsideBank[6], INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
	SET_PED_COMPONENT_VARIATION(pedsCopsOutsideBank[6], INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
	SET_PED_PROP_INDEX(pedsCopsOutsideBank[6], INT_TO_ENUM(PED_PROP_POSITION,1), 1, 0)


	//Moving cop cars
	
	INIT_SET_PIECE_COP_CAR(setpieceIniitalCops1, SHERIFF, 1, "RBHINitial", VS_FRONT_RIGHT, 1000, 1.0, FALSE, DUMMY_MODEL_FOR_SCRIPT, TRUE, TRUE)
	INIT_SET_PIECE_COP_CAR(setpieceIniitalCops2, SHERIFF, 2, "RBHINitial", VS_FRONT_RIGHT, 0000, 0.9, FALSE, DUMMY_MODEL_FOR_SCRIPT, TRUE, TRUE)
	INIT_SET_PIECE_COP_CAR(setpieceIniitalCops3, SHERIFF, 3, "RBHINitial", VS_FRONT_RIGHT, 0000, 1.0, FALSE, DUMMY_MODEL_FOR_SCRIPT, TRUE, TRUE)
	INIT_SET_PIECE_COP_CAR(setpieceIniitalCops4, SHERIFF, 7, "RBHINitial", VS_FRONT_RIGHT, 0000, 1.0, FALSE, DUMMY_MODEL_FOR_SCRIPT, TRUE, TRUE)
	INIT_SET_PIECE_COP_CAR(setpieceIniitalCops5, SHERIFF, 6, "RBHINitial", VS_FRONT_RIGHT, 7000, 1.0, FALSE, DUMMY_MODEL_FOR_SCRIPT, TRUE, TRUE)

	FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(setpieceIniitalCops5.thisCar)

	SET_VEHICLE_CAN_BE_TARGETTED(setpieceIniitalCops1.thisCar, FALSE)
	SET_ENTITY_IS_TARGET_PRIORITY(setpieceIniitalCops1.thisCar, FALSE)
	SET_VEHICLE_CAN_BE_TARGETTED(setpieceIniitalCops3.thisCar, FALSE)
	SET_ENTITY_IS_TARGET_PRIORITY(setpieceIniitalCops3.thisCar, FALSE)

	SET_VEHICLE_CAN_BE_TARGETTED(setpieceIniitalCops5.thisCar, FALSE)
	SET_ENTITY_IS_TARGET_PRIORITY(setpieceIniitalCops5.thisCar, FALSE)

ENDPROC

PROC CLEANUP_CARS_OUTSIDE_CHICKEN_FACTORY()
	
	INT i
	FOR i = 0 TO NUMBER_OF_COP_CARS_OUTSIDE_FACTORY - 1
		CLEANUP_SET_PIECE_COP_CAR(outsideFactoryVehicles[i])
	ENDFOR

ENDPROC


PROC removeAllBlipsCops3()
	INT i
	FOR i = 0 TO NUMBER_OF_INITIAL_ARMY_PEDS - 1
		REMOVE_BLIP_AND_CHECK_IF_EXISTS(blippedsArmyInJunkYard[i])	
	ENDFOR	
ENDPROC

PROC SPAWN_NEW_SET_OF_ARMY_GUYS(BOOL bWithGaskMAsk = FALSE)
	
	INT i
	
	removeAllBlipsCops3()
	
	RESET_MISSION_STATS_ENTITY_WATCH()
	
	//This will be called a couple of times.
	//Dont recreate stationary guys
	IF NOT bOnlyCreateArmyGuysOnce
//		CopCarsParked3[0] = CREATE_VEHICLE(Barracks, << -221.3816, 6296.6538, 30.4905 >>, 319.1075  )
//		CopCarsParked3[1] = CREATE_VEHICLE(Barracks, << -212.5283, 6304.9902, 30.4974 >>, 318.0208)
//		
//		pedsArmyInJunkYard[5] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , << -188.1388, 6284.5415, 30.4901 >>, 321.5203 )
//		pedsArmyInJunkYard[6] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , << -182.9337, 6281.8276, 30.4903 >>, 131.0816)
//
//
//		pedsArmyInJunkYard[7] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , <<-184.0995, 6300.9248, 30.4892>>, 158.3237)
//		
//		coverObjects[0] = CREATE_OBJECT(PROP_CONC_BLOCKS01A,  << -187.2594, 6284.7173, 30.4903 >>)
//		SET_OBJECT_TARGETTABLE(coverObjects[0], FALSE)
//		SET_ENTITY_HEADING(coverObjects[0], 145.4832)
//		FREEZE_ENTITY_POSITION(coverObjects[0], TRUE)
//	
//		coverObjects[1] = CREATE_OBJECT(PROP_afasBOX_WOOD03A,  << -182.1935, 6282.2861, 30.4903 >>)
//		SET_OBJECT_TARGETTABLE(coverObjects[1], FALSE)
//		SET_ENTITY_HEADING(coverObjects[1], 131.2220)
//		FREEZE_ENTITY_POSITION(coverObjects[1], TRUE)
//	
//		coverObjects[2] = CREATE_OBJECT(PROP_CONC_BLOCKS01A,  << -182.1935, 6282.2861, 30.4903 >>)
//		SET_OBJECT_TARGETTABLE(coverObjects[2], FALSE)
//		//SET_ENTITY_COORDS(coverObjects[2], <<-174.2093, 6292.4023, 30.4894>>)
//		//SET_ENTITY_HEADING(coverObjects[2], 215.6256)
//		SET_ENTITY_COORDS_NO_OFFSET(coverObjects[2], <<-175.1123, 6292.6372, 30.4900>>)
//		SET_ENTITY_ROTATION(coverObjects[2], <<0.0000, 0.0000, 122.0400>>)
//		SET_ENTITY_QUATERNION(coverObjects[2], 0.0000, 0.0000, 0.8748, 0.4845)
//		FREEZE_ENTITY_POSITION(coverObjects[2], TRUE)
		
		bOnlyCreateArmyGuysOnce = TRUE
	ENDIF
	
	//4 respawn guys

	
	
	IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-199.544785,6265.691406,58.015686>>, <<-263.874298,6204.558594,29.770254>>, 75.750000)
		pedsArmyInJunkYard[1] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , << -202.3735, 6255.2959, 30.4904 >>, 36.4417)
		pedsArmyInJunkYard[2] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , << -201.7924, 6257.6372, 30.4905 >>, 36.4417 )
		pedsArmyInJunkYard[3] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 ,  <<-230.3228, 6275.2451, 30.6739>>, 209.6445)
		pedsArmyInJunkYard[4] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , <<-228.1700, 6272.7397, 30.6274>>, 222.2370 )
	ELSE
		pedsArmyInJunkYard[1] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , <<-168.0640, 6306.7139, 30.2630>>, 251.4997)
		pedsArmyInJunkYard[2] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , <<-166.6318, 6304.6650, 30.2925>>, 205.0103)
		pedsArmyInJunkYard[3] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , <<-158.7476, 6292.9678, 30.5832>>, 40.9089)
		pedsArmyInJunkYard[4] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , <<-159.8854, 6294.3267, 30.5973>>, 39.5548)	
	ENDIF
	
	FOR i = 0 TO NUMBER_OF_COP_CARS_OUTSIDE_BANK3 - 1
		IF DOES_ENTITY_EXIST(CopCarsParked3[i])
			IF NOT IS_ENTITY_DEAD(CopCarsParked3[i])
				//SET_VEHICLE_SIREN(CopCarsParked3[i], TRUE)
				SET_VEHICLE_DOORS_LOCKED(CopCarsParked3[i], VEHICLELOCK_LOCKED)
				
				TEXT_LABEL_63 debugCarName = "ArmyTruck "
				debugCarName += i
			
				SET_VEHICLE_NAME_DEBUG(CopCarsParked3[i], debugCarName)
			ENDIF
		ENDIF
	ENDFOR
	
	FOR i = 0 TO NUMBER_OF_INITIAL_ARMY_PEDS - 1
		IF DOES_ENTITY_EXIST(pedsArmyInJunkYard[i])
			
			SET_PED_AS_DEFENSIVE_COP(pedsArmyInJunkYard[i], WEAPONTYPE_CARBINERIFLE, FALSE, FALSE, 2.1, FALSE, bWithGaskMAsk)
			SET_PED_CAN_BE_TARGETTED(pedsArmyInJunkYard[i], TRUE)
			TEXT_LABEL_63 debugPedName = "ArmyPed "
			debugPedName += i
		
			SET_PED_NAME_DEBUG(pedsArmyInJunkYard[i], debugPedName)
			
			//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsArmyInJunkYard[i], TRUE)
			
			SET_COMBAT_FLOAT(pedsArmyInJunkYard[i], CCF_STRAFE_WHEN_MOVING_CHANCE, 0.4)
			
			SET_PED_COMBAT_RANGE(pedsArmyInJunkYard[i], CR_NEAR)
			SET_PED_COMBAT_MOVEMENT(pedsArmyInJunkYard[i], CM_WILLADVANCE)
			SET_PED_COMBAT_ATTRIBUTES(pedsArmyInJunkYard[i], CA_CAN_CHARGE, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedsArmyInJunkYard[i], CA_AGGRESSIVE, TRUE)	
			
			blippedsArmyInJunkYard[i] = CREATE_BLIP_FOR_PED_WRAPPER(pedsArmyInJunkYard[i], TRUE)
			
		ENDIF
	ENDFOR
	
	//SET_PED_ANGLED_DEFENSIVE_AREA( pedsArmyInJunkYard[1], <<-190.796280,6268.413574,29.512257>>, <<-205.069809,6282.689941,36.240379>>, 27.250000)
//	SET_PED_ANGLED_DEFENSIVE_AREA( pedsArmyInJunkYard[2], <<-190.796280,6268.413574,29.512257>>, <<-205.069809,6282.689941,36.240379>>, 27.250000)
	//SET_PED_ANGLED_DEFENSIVE_AREA( pedsArmyInJunkYard[3], <<-190.796280,6268.413574,29.512257>>, <<-205.069809,6282.689941,36.240379>>, 27.250000)
	//SET_PED_ANGLED_DEFENSIVE_AREA( pedsArmyInJunkYard[4], <<-190.796280,6268.413574,29.512257>>, <<-205.069809,6282.689941,36.240379>>, 27.250000)

	
	IF DOES_ENTITY_EXIST(pedsArmyInJunkYard[5])
		IF NOT IS_PED_INJURED(pedsArmyInJunkYard[5])
			SET_PED_COMBAT_MOVEMENT(pedsArmyInJunkYard[5], CM_DEFENSIVE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsArmyInJunkYard[5], FALSE)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(pedsArmyInJunkYard[6])
		IF NOT IS_PED_INJURED(pedsArmyInJunkYard[6])
			SET_PED_COMBAT_MOVEMENT(pedsArmyInJunkYard[6], CM_DEFENSIVE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsArmyInJunkYard[6], FALSE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedsArmyInJunkYard[7])
		IF NOT IS_PED_INJURED(pedsArmyInJunkYard[7])
			SET_PED_COMBAT_MOVEMENT(pedsArmyInJunkYard[7], CM_DEFENSIVE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsArmyInJunkYard[7], FALSE)
		ENDIF
	ENDIF
	
	iBodyCount = 0

	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
ENDPROC




PROC SET_NEW_SET_OF_ARMY_GUYS_RUNNING_TO_COVER()

	IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -193.5920, 6291.4912, 30.4901 >>, <<5.0, 5.0, 5.0>>)
		SET_PED_RUN_INTO_POSITION_AND_COMBAT(pedsArmyInJunkYard[1], << -193.5920, 6291.4912, 30.4901 >>, TRUE, 0, PEDMOVE_SPRINT)
	ELSE
		DELETE_PED(pedsArmyInJunkYard[1])
	ENDIF
	IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< -188.4366, 6272.0742, 30.4905 >>, <<5.0, 5.0, 5.0>>)
		SET_PED_RUN_INTO_POSITION_AND_COMBAT(pedsArmyInJunkYard[2], << -188.4366, 6272.0742, 30.4905 >>, TRUE, 0, PEDMOVE_SPRINT)
	ELSE
		DELETE_PED(pedsArmyInJunkYard[2])
	ENDIF
	IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -203.0336, 6283.3296, 30.4904 >>, <<5.0, 5.0, 5.0>>)
		SET_PED_RUN_INTO_POSITION_AND_COMBAT(pedsArmyInJunkYard[3], << -203.0336, 6283.3296, 30.4904 >>, TRUE, 0, PEDMOVE_SPRINT)
	ELSE
		DELETE_PED(pedsArmyInJunkYard[3])
	ENDIF

	IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(),  << -187.7998, 6283.6255, 30.4903 >>, <<5.0, 5.0, 5.0>>)
		SET_PED_RUN_INTO_POSITION_AND_COMBAT(pedsArmyInJunkYard[4], << -187.7998, 6283.6255, 30.4903 >>, TRUE, 0, PEDMOVE_SPRINT)
	ELSE
		DELETE_PED(pedsArmyInJunkYard[4])
	ENDIF
				
ENDPROC

PROC DELETE_PEDS_THAT_WILL_RUN_NEAR_TO_PLAYER()

	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -193.5920, 6291.4912, 30.4901 >>, <<5.0, 5.0, 5.0>>)
		DELETE_PED(pedsArmyInJunkYard[1])
	ENDIF
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< -188.4366, 6272.0742, 30.4905 >>, <<5.0, 5.0, 5.0>>)
		DELETE_PED(pedsArmyInJunkYard[2])
	ENDIF
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -203.0336, 6283.3296, 30.4904 >>, <<5.0, 5.0, 5.0>>)
		DELETE_PED(pedsArmyInJunkYard[3])
	ENDIF
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),  << -187.7998, 6283.6255, 30.4903 >>, <<5.0, 5.0, 5.0>>)
		DELETE_PED(pedsArmyInJunkYard[4])
	ENDIF
				
ENDPROC




PROC KILL_OFF_COPS_STAGE2()
	
	INT i
	
	FOR i = 0 TO NUMBER_OF_COPS_IN_STREET - 1
		IF NOT IS_PED_INJURED(pedsCopsInStreet[i])
			APPLY_DAMAGE_TO_PED(pedsCopsInStreet[i], 101, TRUE) //101 will take a 200 health ped below 100 to make them injured (but still maintain movement)
			SET_PED_AS_NO_LONGER_NEEDED(pedsCopsInStreet[i])
		ENDIF
	ENDFOR

ENDPROC

PROC KILL_OFF_ARMY_STAGE3()
	
	INT i
	
	FOR i = 0 TO NUMBER_OF_INITIAL_ARMY_PEDS - 1
		IF NOT IS_PED_INJURED(pedsArmyInJunkYard[i])
			APPLY_DAMAGE_TO_PED(pedsArmyInJunkYard[i], 101, TRUE) //101 will take a 200 health ped below 100 to make them injured (but still maintain movement)
		ENDIF
	ENDFOR

ENDPROC

PROC removeAllBlipsCops1()
	INT i
	FOR i = 0 TO NUMBER_OF_COPS_OUTSIDE_BANK - 1
		REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipPedsCopsOutsideBank[i])	
	ENDFOR	
		
ENDPROC





PROC COMBAT_FODDER_PEDS()

	IF (GET_GAME_TIMER() - iReTarget) > 2000
		IF NOT IS_PED_INJURED(pedsFodderCops[2])
			TASK_COMBAT_PED(pedGunman, pedsFodderCops[2])
		ELIF NOT IS_PED_INJURED(pedsFodderCops[1])					
			TASK_COMBAT_PED(pedGunman, pedsFodderCops[1])
		ELIF NOT IS_PED_INJURED(pedsFodderCops[0])
			TASK_COMBAT_PED(pedGunman, pedsFodderCops[0])
		ENDIF

		IF NOT IS_PED_INJURED(pedsFodderCops[2])
			TASK_COMBAT_PED(PedMichael(), pedsFodderCops[2])
		ELIF NOT IS_PED_INJURED(pedsFodderCops[0])					
			TASK_COMBAT_PED(PedMichael(), pedsFodderCops[0])
		ELIF NOT IS_PED_INJURED(pedsFodderCops[1])
			TASK_COMBAT_PED(PedMichael(), pedsFodderCops[1])			
		ENDIF
		iReTarget = GET_GAME_TIMER()
	ENDIF

ENDPROC




PROC COMBAT_ARMY_PEDS_MICHAEL()

	IF (GET_GAME_TIMER() - iReTargetMichael) > 3000
		
		//SET_PED_SPHERE_DEFENSIVE_AREA(PedMichael(), GET_ENTITY_COORDS(PedMichael()), 0.5)
		
//		IF NOT IS_PED_INJURED(pedsArmyInJunkYard[3])
//			TASK_SHOOT_AT_ENTITY(PedMichael(), pedsArmyInJunkYard[3], INFINITE_TASK_TIME, FIRING_TYPE_CONTINUOUS)
//		ELIF NOT IS_PED_INJURED(pedsArmyInJunkYard[2])
//			TASK_SHOOT_AT_ENTITY(PedMichael(), pedsArmyInJunkYard[2], INFINITE_TASK_TIME, FIRING_TYPE_CONTINUOUS)	
//		ELIF NOT IS_PED_INJURED(pedsArmyInJunkYard[1])					
//			TASK_SHOOT_AT_ENTITY(PedMichael(), pedsArmyInJunkYard[1], INFINITE_TASK_TIME, FIRING_TYPE_CONTINUOUS)
//		ELIF NOT IS_PED_INJURED(pedsArmyInJunkYard[0])
//			TASK_SHOOT_AT_ENTITY(PedMichael(), pedsArmyInJunkYard[0], INFINITE_TASK_TIME, FIRING_TYPE_CONTINUOUS)	
//		ENDIF
		iReTargetMichael = GET_GAME_TIMER()
	ENDIF

ENDPROC



PROC MANAGE_COP_RESPAWN_IN_STREET()

	INT i

	IF iCopRespawnInStreetIndex < NUMBER_OF_COPS_IN_STREET - 1
		iCopRespawnInStreetIndex++
	ELSE
		iCopRespawnInStreetIndex = 5
	ENDIF
		
	i = iCopRespawnInStreetIndex
		
		IF IS_PED_INJURED(pedsCopsInStreet[i])
		OR IS_PED_HURT(pedsCopsInStreet[i])
		OR IS_ENTITY_DEAD(pedsCopsInStreet[i])		
			INT iScreamType = GET_RANDOM_INT_IN_RANGE(0, 3)
				
			IF iScreamType = 0
				PLAY_SOUND_FROM_COORD(-1, "TEST_SCREAM_LONG", pedsCopsInStreetSpawnPosition[i])
			ELIF iScreamType = 1
				PLAY_SOUND_FROM_COORD(-1, "TEST_SCREAM_SHORT", pedsCopsInStreetSpawnPosition[i])
			ENDIF
			
		
			SET_PED_AS_NO_LONGER_NEEDED(pedsCopsInStreet[i]) //Allow body to cleanup
			
			//The guys comment on the death of a cop
			IF (i % 2 = 0) //Only do every second ped.
			AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				INT randDialogue = GET_RANDOM_INT_IN_RANGE(0,2)
				
				SWITCH randDialogue
					CASE 0
						IF IS_PED_SHOOTING(PLAYER_PED_ID())
							CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_TCOPDE", CONV_PRIORITY_HIGH)
						ENDIF
					BREAK
								
					CASE 1
						IF NOT IS_ENTITY_DEAD(PedMichael())
							IF IS_PED_SHOOTING(PedMichael())
								CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_MCOPDE", CONV_PRIORITY_HIGH)
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH	
			ENDIF
			
			REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipPedsCopsInStreet[i])
			bThisPedInjured[i] = TRUE
			iBodyCount++
						
		ENDIF
	
		IF bThisPedInjured[i] = TRUE
		AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), pedsCopsInStreetSpawnPosition[i], << 4.0, 4.0, 4.0>>)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-94.395683,6315.781250,39.072277>>, <<-129.332092,6279.796875,30.176098>>, 31.000000)
		AND NOT DOES_ENTITY_EXIST(pedsCopsInStreet[i])
	//	AND (i % 2 = 0) //Only do every second ped.
		AND VMAG(pedsCopsInStreetSpawnPosition[i]) > 0.0
		AND GET_GAME_TIMER() - iTimeOfLastRespawn > 1000
		//AND NOT IS_SPHERE_VISIBLE(pedsCopsInStreetSpawnPosition[i], 1.0)
			
			PRINTNL()PRINTLN("Respawn and run to:", pedsCopsInStreetSpawnPosition[i])
			PRINTNL()
			
				
				IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
					pedsCopsInStreet[i] = CREATE_PED(PEDTYPE_COP, mnSwatPedModel, spawnPoint2InStreet)
				ELSE
					pedsCopsInStreet[i] = CREATE_PED(PEDTYPE_COP, mnSwatPedModel, spawnPoint1InStreet)
				ENDIF
				
			IF GET_RANDOM_INT_IN_RANGE(0, 10) > 6
				SET_PED_AS_DEFENSIVE_COP(pedsCopsInStreet[i], WEAPONTYPE_PUMPSHOTGUN)
			ELSE
				SET_PED_AS_DEFENSIVE_COP(pedsCopsInStreet[i], WEAPONTYPE_CARBINERIFLE)
				//GIVE_WEAPON_COMPONENT_TO_PED(pedsCopsInStreet[i], WEAPONTYPE_CARBINERIFLE, WEAPONCOMPONENT_AT_AR_LASR)
			ENDIF

			SET_PED_CONFIG_FLAG(pedsCopsInStreet[i], PCF_RunFromFiresAndExplosions, FALSE)		
			REMOVE_PED_DEFENSIVE_AREA(pedsCopsInStreet[i], FALSE)
			SET_PED_COMBAT_MOVEMENT (pedsCopsInStreet[i], CM_WILLADVANCE)
							
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_63 debugPedName = "InSt:"
				debugPedName += i
				SET_PED_NAME_DEBUG(pedsCopsInStreet[i], debugPedName)
			#ENDIF
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedsCopsInStreet[i], 170.0)
			bThisPedInjured[i] = FALSE				
			iTimeOfLastRespawn = GET_GAME_TIMER()
		ENDIF
		
	
ENDPROC


PROC CLEANUP_COPS_AND_CARS_OUTSIDE_BANK()
	DELETE_ARRAY_OF_VEHICLES(CopCarsParked)
	DELETE_ARRAY_OF_PEDS(pedsCopsOutsideBank)
ENDPROC


PROC CLEANUP_COPS_AND_CARS_DOWN_STREET(BOOL bDelete = TRUE)

	INT i
	
	FOR i = 0 TO NUMBER_OF_COP_CARS_IN_STREET - 1
		IF bDelete
			DELETE_VEHICLE(CopCarsParkedDownStreet[i])
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(CopCarsParkedDownStreet[i])
		ENDIF
	ENDFOR
	
	FOR i = 0 TO NUMBER_OF_COPS_IN_STREET - 1
		IF bDelete
			DELETE_PED(pedsCopsInStreet[i])
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(pedsCopsInStreet[i])
		ENDIF
	ENDFOR


//	SET_MODEL_AS_NO_LONGER_NEEDED(AVERA)
//	SET_MODEL_AS_NO_LONGER_NEEDED(RUINER)
	SET_MODEL_AS_NO_LONGER_NEEDED(CRUSADER)
	//SET_MODEL_AS_NO_LONGER_NEEDED(DOMINATOR)
	
ENDPROC






PROC setCrewRelationShipGroups()

	IF DOES_ENTITY_EXIST(pedFranklin())
		IF NOT IS_ENTITY_DEAD(pedFranklin())
			SET_PED_RELATIONSHIP_GROUP_HASH(pedFranklin(), robbersGroup)
			SET_PED_CAN_BE_TARGETTED(pedFranklin(), FALSE)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(PedMichael())
		IF NOT IS_ENTITY_DEAD(PedMichael())
			SET_PED_RELATIONSHIP_GROUP_HASH(PedMichael(), robbersGroup)
			SET_PED_CAN_BE_TARGETTED(PedMichael(), FALSE)
		ENDIF
	ENDIF	
	IF DOES_ENTITY_EXIST(pedTrevor())
		IF NOT IS_ENTITY_DEAD(pedTrevor())
			SET_PED_RELATIONSHIP_GROUP_HASH(pedTrevor(), robbersGroup)
			SET_PED_CAN_BE_TARGETTED(pedTrevor(), FALSE)
		ENDIF
	ENDIF	
	IF DOES_ENTITY_EXIST(pedGunman)
		IF NOT IS_ENTITY_DEAD(pedGunman)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedGunman, robbersGroup)
			SET_PED_CAN_BE_TARGETTED(pedGunman, FALSE)
		ENDIF
	ENDIF	
	
	SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), robbersGroup)

ENDPROC

PROC FORCE_SWITCH_TO_TREVOR(BOOL bSwitchImmediately = FALSE)
		
	IF bSwitchImmediately
	ENDIF
	
	SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_TREVOR, FALSE)
		
	UPDATE_SELECTOR_HUD(pedSelector, FALSE, FALSE)
	
	PRINTLN("FORCE_SWITCH_TO_TREVOR...")
		
	IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_TREVOR
	
		MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_TREVOR)
		TAKE_CONTROL_OF_SELECTOR_PED(pedSelector)
			
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PedMichael(), "MICHAEL")
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, PLAYER_PED_ID(), "TREVOR")		
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, pedFranklin(), "FRANKLIN")
		
		IF IS_GUNMAN_GUSTAVO()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "GUSTAVO")
		ELIF IS_GUNMAN_DARYL()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "DARYL")
		ELIF IS_GUNMAN_CHEF()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "CHEF")
		ELIF IS_GUNMAN_NORM()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "NORM")
		ELIF IS_GUNMAN_PACKIE()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "PACKIE")
		ENDIF
		
		SET_PED_MAX_HEALTH_WITH_SCALE(PLAYER_PED_ID(), 1000)
	
		setCrewRelationShipGroups()
	ELSE
		PRINTSTRING("WARNING: FORCE_SWITCH_TO_TREVOR - switching from wrong char!")
	ENDIF
	
	
ENDPROC

PROC FORCE_SWITCH_TO_MICHAEL(BOOL bKeepTasks = FALSE)
	
	SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_MICHAEL, FALSE)
	
	UPDATE_SELECTOR_HUD(pedSelector, FALSE, FALSE)
	
	PRINTLN("FORCE_SWITCH_TO_MICHAEL...")
		
	IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
	
		MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_MICHAEL)
		
		IF bKeepTasks
			TAKE_CONTROL_OF_SELECTOR_PED(pedSelector, TRUE, bKeepTasks, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
		ELSE
			TAKE_CONTROL_OF_SELECTOR_PED(pedSelector, TRUE, FALSE)
		ENDIF
	
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedTrevor(), "TREVOR")
		IF IS_GUNMAN_GUSTAVO()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "GUSTAVO")
		ELIF IS_GUNMAN_DARYL()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "DARYL")
		ELIF IS_GUNMAN_CHEF()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "CHEF")
		ELIF IS_GUNMAN_NORM()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "NORM")
		ELIF IS_GUNMAN_PACKIE()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "PACKIE")
		ENDIF
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, pedFranklin(), "FRANKLIN")
	
		SET_PED_MAX_HEALTH_WITH_SCALE(PLAYER_PED_ID(), 1000)
	
		setCrewRelationShipGroups()
	ELSE
		PRINTSTRING("WARNING: FORCE_SWITCH_TO_MICHAEL - switching from wrong char!")
	ENDIF
			
ENDPROC

PROC FORCE_SWITCH_TO_FRANKLN()
	
	SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_FRANKLIN, FALSE)
	
	UPDATE_SELECTOR_HUD(pedSelector, FALSE, FALSE)
	
	PRINTLN("FORCE_SWITCH_TO_FRANKLN...")
		
	IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
	
		MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_FRANKLIN)
		TAKE_CONTROL_OF_SELECTOR_PED(pedSelector)
	
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, pedMichael(), "MICHAEL")
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedTrevor(), "TREVOR")
		IF IS_GUNMAN_GUSTAVO()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "GUSTAVO")
		ELIF IS_GUNMAN_DARYL()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "DARYL")
		ELIF IS_GUNMAN_CHEF()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "CHEF")
		ELIF IS_GUNMAN_NORM()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "NORM")
		ELIF IS_GUNMAN_PACKIE()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "PACKIE")
		ENDIF
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, PLAYER_PED_ID(), "FRANKLIN")
	
		setCrewRelationShipGroups()
	ELSE
		PRINTSTRING("WARNING: FORCE_SWITCH_TO_FRANKLN - switching from wrong char!")
	ENDIF
	
ENDPROC

BOOL bOnlyGiveWeaponsAtFirst

PROC EQUIP_TREVOR_WITH_WEAPONS()

	IF NOT bOnlyGiveWeaponsAtFirst
		
		bOnlyGiveWeaponsAtFirst = TRUE
	ENDIF
	
	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_MINIGUN, TRUE)

ENDPROC

PROC SET_CREW_IN_BALLISTIC_ARMOUR(BOOL bIsReplay)
	
	IF bIsBuddyAPro
		pedGunmanWeaponType = WEAPONTYPE_COMBATMG
	ELSE
		pedGunmanWeaponType = WEAPONTYPE_CARBINERIFLE
	ENDIF
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-10000.0, -10000.0, -200.0>>, <<10000.0, 10000.0, 200.0>>, FALSE)
	REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<-10000.0, -10000.0, -200.0>>, <<10000.0, 10000.0, 200.0>>)
	
	SET_ROADS_IN_AREA(<<vBankCoordinates.x - 200.0, vBankCoordinates.y - 200.0, vBankCoordinates.z - 200.0>>, <<vBankCoordinates.x + 200.0, vBankCoordinates.y + 200.0, vBankCoordinates.z + 200.0>>, FALSE)
	
	//CLEAR_AREA_OF_VEHICLES(<<0.0, 0.0, 0.0>>, 100000.00)

	SET_REDUCE_VEHICLE_MODEL_BUDGET(TRUE)
	SET_REDUCE_PED_MODEL_BUDGET(TRUE)
	SET_VEHICLE_POPULATION_BUDGET(0)
	SET_PED_POPULATION_BUDGET(0)
	
	//At all times when they are wearing the armour we wont want random cars etc.. until tank section 2b

	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)	
	
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		pedSelector.pedID[SELECTOR_PED_MICHAEL] = PLAYER_PED_ID()
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		pedSelector.pedID[SELECTOR_PED_TREVOR] = PLAYER_PED_ID()
	ENDIF
	
	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P2_HEADSET)
		
	IF NOT IS_PED_INJURED(pedGunman)
		SET_HEIST_CREW_MEMBER_OUTFIT(pedGunman, GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_RURAL_BANK, 0), CREW_OUTFIT_HEAVY_ARMOR)
		//SET_PED_COMBAT_ATTRIBUTES(pedGunman, CA_USE_COVER, FALSE)
		GIVE_WEAPON_TO_PED(pedGunman, pedGunmanWeaponType, INFINITE_AMMO, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedGunman, CA_USE_VEHICLE,FALSE)
		SET_PED_CAN_BE_TARGETTED(pedGunman, FALSE)
		
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedGunman, TRUE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedGunman, FALSE)
		
		
		//SET_PED_FIRING_PATTERN(pedGunman, FIRING_PATTERN_FULL_AUTO)
		
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedGunman, FALSE)
		SET_PED_SUFFERS_CRITICAL_HITS(pedGunman, FALSE)
		
		SET_PED_MAX_HEALTH(pedGunman, 1000)
		SET_ENTITY_HEALTH(pedGunman, 1000)
		
		//SET_PED_COMBAT_ATTRIBUTES(pedGunman, CA_USE_COVER, FALSE)	
		//SET_PED_COMBAT_ATTRIBUTES(pedGunman, CA_AGGRESSIVE, TRUE)	
		SET_PED_COMBAT_RANGE(pedGunman, CR_FAR)
		SET_PED_TARGET_LOSS_RESPONSE(pedGunman, TLR_NEVER_LOSE_TARGET)
		SET_PED_SHOOT_RATE(pedGunman, 1000)
		SET_PED_ACCURACY(pedGunman, 5)
		SET_PED_NAME_DEBUG(pedGunman, "Crew Member")						
		SET_PED_CONFIG_FLAG(pedGunman, PCF_DontActivateRagdollFromFire, TRUE)
		SET_PED_CONFIG_FLAG(pedGunman, PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(pedGunman, PCF_DisableExplosionReactions, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedGunman, CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
		
		SET_PED_COMBAT_ATTRIBUTES(pedGunman, CA_DISABLE_BULLET_REACTIONS , TRUE)
		
		SET_ENTITY_PROOFS(pedGunman, TRUE, TRUE, FALSE, FALSE, FALSE)
		//Bag
		SET_PED_COMPONENT_VARIATION(pedGunman, INT_TO_ENUM(PED_COMPONENT,6), 4, 0, 0) //(feet)
		
		SET_WEAPON_ANIMATION_OVERRIDE(pedGunman, HASH("BALLISTIC"))
		
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedGunman, "PS_2A_BUDDY_GROUP")
		
		SET_PED_COMBAT_ATTRIBUTES(pedGunman, CA_BLIND_FIRE_IN_COVER, FALSE)
		
		SET_PED_PATH_AVOID_FIRE(pedGunman, FALSE)
				
	ENDIF
		
	IF NOT IS_PED_INJURED(pedSelector.pedID[SELECTOR_PED_MICHAEL])
		
		//GIVE_WEAPON_TO_PED(pedSelector.pedID[SELECTOR_PED_MICHAEL], PedMichaelWeaponType, 1000, TRUE)
		
		IF GET_AMMO_IN_PED_WEAPON(pedSelector.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_COMBATMG) < 1000
			GIVE_WEAPON_TO_PED(pedSelector.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_COMBATMG, 1000 - GET_AMMO_IN_PED_WEAPON(pedSelector.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_COMBATMG), TRUE, TRUE)
		ELSE
			SET_CURRENT_PED_WEAPON(pedSelector.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_COMBATMG, TRUE)
		ENDIF
		
		SET_PED_COMBAT_ATTRIBUTES(pedSelector.pedID[SELECTOR_PED_MICHAEL], CA_USE_VEHICLE, FALSE)
		
		SET_PED_CAN_BE_TARGETTED(pedSelector.pedID[SELECTOR_PED_MICHAEL], FALSE)
		SET_PED_CONFIG_FLAG(pedSelector.pedID[SELECTOR_PED_MICHAEL], PCF_DontActivateRagdollFromFire, TRUE)
		SET_PED_SUFFERS_CRITICAL_HITS(pedSelector.pedID[SELECTOR_PED_MICHAEL], FALSE)	
		SET_PED_SUFFERS_CRITICAL_HITS(pedSelector.pedID[SELECTOR_PED_MICHAEL], FALSE)
		SET_PED_CAN_BE_TARGETTED(pedSelector.pedID[SELECTOR_PED_MICHAEL], FALSE)	
		SET_PED_MAX_HEALTH(pedSelector.pedID[SELECTOR_PED_MICHAEL], 1000)
				
		SET_ENTITY_HEALTH(pedSelector.pedID[SELECTOR_PED_MICHAEL], 1000)
		SET_PED_SHOOT_RATE(pedSelector.pedID[SELECTOR_PED_MICHAEL], 1000)
		SET_PED_COMBAT_RANGE(pedSelector.pedID[SELECTOR_PED_MICHAEL], CR_FAR)
		SET_PED_TARGET_LOSS_RESPONSE(pedSelector.pedID[SELECTOR_PED_MICHAEL], TLR_NEVER_LOSE_TARGET)
		
		SET_ENTITY_PROOFS(pedSelector.pedID[SELECTOR_PED_MICHAEL], FALSE, TRUE, FALSE, FALSE, FALSE)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedSelector.pedID[SELECTOR_PED_MICHAEL], TRUE)
		
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedSelector.pedID[SELECTOR_PED_MICHAEL], FALSE)
		
		SET_PED_CONFIG_FLAG(pedSelector.pedID[SELECTOR_PED_MICHAEL], PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(pedSelector.pedID[SELECTOR_PED_MICHAEL], PCF_DisableExplosionReactions, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedSelector.pedID[SELECTOR_PED_MICHAEL], CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
		
		SET_PED_COMBAT_ATTRIBUTES(pedSelector.pedID[SELECTOR_PED_MICHAEL], CA_DISABLE_BULLET_REACTIONS , TRUE)
		SET_PED_NAME_DEBUG(pedSelector.pedID[SELECTOR_PED_MICHAEL], "Michael")	
		SET_PED_COMP_ITEM_CURRENT_SP(pedSelector.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_BALLISTICS, FALSE)
	
		SET_PED_COMP_ITEM_CURRENT_SP(pedSelector.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_BALLISTICS, FALSE)
		SET_PED_COMPONENT_VARIATION(pedSelector.pedID[SELECTOR_PED_MICHAEL], INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
		
		SET_PED_COMP_ITEM_CURRENT_SP(pedSelector.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
		CLEAR_PED_PROP(pedSelector.pedID[SELECTOR_PED_MICHAEL], ANCHOR_EYES)
		
		SET_WEAPON_ANIMATION_OVERRIDE(pedSelector.pedID[SELECTOR_PED_MICHAEL], HASH("BALLISTIC"))
		
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedSelector.pedID[SELECTOR_PED_MICHAEL], "PS_2A_BUDDY_GROUP")
	
		SET_PED_PROP_INDEX(pedSelector.pedID[SELECTOR_PED_MICHAEL], INT_TO_ENUM(PED_PROP_POSITION,0), 26, 0)
	
		SET_PED_PATH_AVOID_FIRE(pedSelector.pedID[SELECTOR_PED_MICHAEL], FALSE)
	
		//SET_PED_BOUNDS_SIZE()
	ENDIF
	
	SET_PED_MAX_HEALTH(PLAYER_PED_ID(), 1000)
	SET_ENTITY_HEALTH(PLAYER_PED_ID(), 1000)
	SET_PED_ARMOUR(PLAYER_PED_ID(), 100)
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, TRUE, FALSE, FALSE, FALSE)
//		REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), WEAPONTYPE_MINIGUN)
		CLEAR_PED_PARACHUTE_PACK_VARIATION(PLAYER_PED_ID())
					
		IF NOT bIsReplay		
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MINIGUN, 7000, TRUE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, 300, TRUE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADELAUNCHER, 10, TRUE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_COMBATMG, 500, TRUE)		
		ENDIF
		
		EQUIP_TREVOR_WITH_WEAPONS()
		
		SET_PED_MAX_HEALTH(PLAYER_PED_ID(), 1000)
		SET_ENTITY_HEALTH(PLAYER_PED_ID(), 1000)
		
	ELSE
		//GIVE_WEAPON_TO_PED(pedTrevor(), WEAPONTYPE_MI NIGUN, 10000, TRUE)
	ENDIF
	
	//Trevor/player
	//CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 200.00, TRUE)	
	CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_EYES)
	SET_PED_COMP_ITEM_CURRENT_SP(pedSelector.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_OUTFIT, OUTFIT_P2_BALLISTICS, FALSE)
	SET_PED_COMPONENT_VARIATION(pedSelector.pedID[SELECTOR_PED_TREVOR], INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
	ADD_ENTITY_TO_AUDIO_MIX_GROUP(pedSelector.pedID[SELECTOR_PED_TREVOR], "PS_2A_BUDDY_GROUP")
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedSelector.pedID[SELECTOR_PED_TREVOR], FALSE)
	SET_PED_CONFIG_FLAG(pedSelector.pedID[SELECTOR_PED_TREVOR], PCF_DontActivateRagdollFromFire, TRUE)
	
	SET_PED_PATH_AVOID_FIRE(pedSelector.pedID[SELECTOR_PED_TREVOR], FALSE)
	SET_PED_ARMOUR(pedSelector.pedID[SELECTOR_PED_TREVOR], 100)
	
	SET_WEAPON_ANIMATION_OVERRIDE(PLAYER_PED_ID(), HASH("BALLISTIC"))
	
	REQUEST_ANIM_SET("ANIM_GROUP_MOVE_BALLISTIC")	
	REQUEST_ANIM_SET("MOVE_STRAFE_BALLISTIC")

	REQUEST_CLIP_SET("move_ballistic_2h")

	REQUEST_PTFX_ASSET()
	
	WHILE NOT HAS_ANIM_SET_LOADED("ANIM_GROUP_MOVE_BALLISTIC")
	OR NOT HAS_ANIM_SET_LOADED("MOVE_STRAFE_BALLISTIC")
	OR NOT HAS_ANIM_SET_LOADED("move_ballistic_2h")
	OR NOT HAS_PTFX_ASSET_LOADED()
		WAIT(0)
	ENDWHILE
	IF NOT IS_PED_INJURED(PedMichael())
		SET_PED_MOVEMENT_CLIPSET(PedMichael(), "ANIM_GROUP_MOVE_BALLISTIC")
		//SET_PED_STRAFE_CLIPSET(PedMichael(), "MOVE_STRAFE_BALLISTIC")
	ENDIF	
	SET_PED_MOVEMENT_CLIPSET(pedTrevor(), "ANIM_GROUP_MOVE_BALLISTIC")
	SET_PED_STRAFE_CLIPSET(pedTrevor(), "MOVE_STRAFE_BALLISTIC")
	
	IF NOT IS_PED_INJURED(pedGunman)
		SET_PED_MOVEMENT_CLIPSET(pedGunman, "ANIM_GROUP_MOVE_BALLISTIC")
		//SET_PED_STRAFE_CLIPSET(pedGunman, "MOVE_STRAFE_BALLISTIC")
	ENDIF
	
	REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID())
	
	SET_PLAYER_HEALTH_RECHARGE_MULTIPLIER(PLAYER_ID(), 5.0)
	
	PRINTLN("Setting Crew into Ballistic Armour")
	
ENDPROC


/// PURPOSE:
///     Checks all ped variations have streamed in (for replays etc..)
/// RETURNS:
///    BOOL
FUNC BOOL HAVE_ALL_STREAMING_REQUESTS_COMPLETED_ON_CREW()

	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(PedMichael())
		IF NOT IS_ENTITY_DEAD(PedMichael())
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PedMichael())
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedTrevor())
		IF NOT IS_ENTITY_DEAD(pedTrevor())
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedTrevor())
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedFranklin())
		IF NOT IS_ENTITY_DEAD(pedFranklin())
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedFranklin())
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(pedGunman)
		IF NOT IS_ENTITY_DEAD(pedGunman)
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedGunman)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDIF
	
	REQUEST_WEAPON_ASSET(WEAPONTYPE_MINIGUN)
	IF NOT HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_MINIGUN)
		RETURN FALSE
	ENDIF
	
	REQUEST_WEAPON_ASSET(WEAPONTYPE_CARBINERIFLE)
	IF NOT HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_CARBINERIFLE)
		RETURN FALSE
	ENDIF
		
	RETURN TRUE
			
ENDFUNC


PROC SET_CREW_IN_NORMAL_OUTFITS()
	
	RESET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID())	
	RESET_PED_STRAFE_CLIPSET(PLAYER_PED_ID())
	
	CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 200.00, TRUE)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ped_comp_head, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ped_comp_hand, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ped_comp_torso, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ped_comp_leg, 0, 0)
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), ped_comp_special, 0, 0)	
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL, 0, 0)	
	SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, 0, 0)	
		
	IF NOT IS_PED_INJURED(pedTrevor())
		SET_PED_COMPONENT_VARIATION(pedTrevor(), INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
		SET_PED_COMPONENT_VARIATION(pedTrevor(), ped_comp_head, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedTrevor(), ped_comp_hand, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedTrevor(), ped_comp_torso, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedTrevor(), ped_comp_leg, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedTrevor(), ped_comp_special, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedTrevor(), ped_comp_special2, 0, 0)
		RESET_PED_MOVEMENT_CLIPSET(pedTrevor())
		RESET_PED_STRAFE_CLIPSET(pedTrevor())
	ENDIF
	
	IF NOT IS_PED_INJURED(pedGunman)
		SET_HEIST_CREW_MEMBER_OUTFIT(pedGunman, GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_RURAL_BANK, 0), CREW_OUTFIT_SUIT)
	ENDIF	
	
	REMOVE_ANIM_SET("ANIM_GROUP_MOVE_BALLISTIC")
	REMOVE_ANIM_SET("move_strafe_ballistic")
	
			
ENDPROC







PROC cleanUpBeforeBankStages()
	
	
	DELETE_PED(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
	SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
	IF NOT IS_ENTITY_DEAD(arrivalBurrito)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), arrivalBurrito)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		ENDIF
	ENDIF
	DELETE_VEHICLE(arrivalBurrito)
	SET_MODEL_AS_NO_LONGER_NEEDED(BURRITO3)
		
//	REMOVE_VEHICLE_RECORDING(1, "CBTruck")
//	REMOVE_VEHICLE_RECORDING(2, "CBTruck")
	
	RESET_GAME_CAMERA()
	
	DISABLE_TAXI_HAILING(TRUE)

ENDPROC

PROC CLEANUP_ADVANCE_TO_GARDEN()

	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipForFence)
	DELETE_PED(pedMaid)
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(iSmokeEffect)
		STOP_PARTICLE_FX_LOOPED(iSmokeEffect)
	ENDIF
	IF DOES_PARTICLE_FX_LOOPED_EXIST(iSmokeEffectPetrolStation)
		STOP_PARTICLE_FX_LOOPED(iSmokeEffectPetrolStation)
	ENDIF

	CLEANUP_SET_PIECE_COP_CAR(carsPoliceBackup[0])
	CLEANUP_SET_PIECE_COP_CAR(carsPoliceBackup[1])
	CLEANUP_SET_PIECE_COP_CAR(carsPolicePoolside[0])
	CLEANUP_SET_PIECE_COP_CAR(carsPolicePoolside[1])
	CLEANUP_SET_PIECE_COP_CAR(carsPolicePoolside[2])
	
	CLEANUP_FODDER_PEDS()

ENDPROC

PROC SETUP_HOLD_OFF_COPS_AT_BANK()
	
	cleanUpBeforeBankStages()
		
	//EQUIP_TREVOR_WITH_WEAPONS()
	
	IF NOT IS_PED_INJURED(PedMichael())
		REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipMichael)
		blipMichael = CREATE_BLIP_FOR_PED_WRAPPER(PedMichael())
	ENDIF
	
	IF NOT IS_PED_INJURED(pedGunman)
		REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipBuddy)
		blipBuddy = CREATE_BLIP_FOR_PED_WRAPPER(pedGunman)
	ENDIF
			
ENDPROC


PROC POSITION_CREW_OUTSIDE_BANK()

	//SCRIPT_ASSERT("POSITION_CREW_OUTSIDE_BANK() called!")
				
	IF NOT IS_PED_INJURED(pedGunman)
		CLEAR_PED_TASKS(pedGunman)
		TASK_LOOK_AT_ENTITY(pedGunman, PLAYER_PED_ID(), 5000)
		SET_ENTITY_COORDS(pedGunman, <<-114.54, 6460.3174, 30.46>>)//<<-115.3569, 6456.1392, 30.4535>>)
		SET_ENTITY_HEADING(pedGunman,  141.1103)	
		//CLEAR_ROOM_FOR_ENTITY(pedGunman)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedGunman, robbersGroup)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGunman, FALSE)
		TASK_AIM_GUN_AT_COORD(pedGunman, << -124.9973, 6447.6924, 31.6209 >>, 1000, TRUE)
//		FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGunman, DEFAULT, TRUE) //[MF] Commenting out for fix B* 1722189
	ENDIF
	
	IF NOT IS_PED_INJURED(PedMichael())
		CLEAR_PED_TASKS(PedMichael())
		TASK_LOOK_AT_ENTITY(PedMichael(), PLAYER_PED_ID(), 5000)
		SET_ENTITY_COORDS(PedMichael(), <<-112.66, 6458.59, 30.45>>)//<<-115.9381, 6460.2344, 30.4685>>)
		SET_ENTITY_HEADING(PedMichael(), 135.2154)
		//CLEAR_ROOM_FOR_ENTITY(pedGunman)
		SET_PED_RELATIONSHIP_GROUP_HASH(PedMichael(), robbersGroup)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedMichael(), FALSE)
		TASK_AIM_GUN_AT_COORD(PedMichael(), << -124.9973, 6447.6924, 31.6209 >>, 1000, TRUE)
//		FORCE_PED_AI_AND_ANIMATION_UPDATE(PedMichael(), DEFAULT, TRUE) //[MF] Commenting out for fix B* 1722189
		
	ENDIF				
	
	IF NOT IS_PED_INJURED(pedFranklin())
		SET_PED_RELATIONSHIP_GROUP_HASH(pedFranklin(), robbersGroup)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -114.39, 6458.7, 30.4684 >>)// << -114.6883, 6459.0054, 30.4684 >>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 139.1)
		SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), robbersGroup)
		//TASK_AIM_GUN_AT_COORD(PLAYER_PED_ID(), << -124.9973, 6447.6924, 31.6209 >>, 1000, TRUE)
		//TASK_PLAY_ANIM(PLAYER_PED_ID(), "WEAPONS@HEAVY@MINIGUN", "AIM_MED_LOOP", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
//		FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID()) //[MF] Commenting out for fix B* 1722189
	ENDIF

ENDPROC


PROC POSITION_CREW_AFTER_CHOPPER_EXPLODES(BOOL  bDoPLayer = TRUE)
	
	//Dynamically repositoin the player after the scene depending where he currently is.
	//IF	IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-112.789253,6448.438965,29.902973>>, <<-86.123932,6420.171875,33.087326>>, 20.250000)
	// IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-89.920929,6426.372559,27.224236>>, <<-139.286118,6475.265137,40.723373>>, 16.250000)
		//1st Angled Area Info
		
		IF bDoPLayer 
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-118.8032, 6454.9033, 30.3969>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 133.6404)	
		ENDIF
		IF NOT IS_PED_INJURED(pedGunman)
			SET_ENTITY_COORDS(pedGunman, <<-122.6378, 6446.7070, 30.5289>>)
			SET_ENTITY_HEADING(pedGunman,  120.2095)
		ENDIF
		IF NOT IS_PED_INJURED(PedMichael())	
			SET_ENTITY_COORDS(PedMichael(), <<-125.5384, 6450.8394, 30.5806>>)
			SET_ENTITY_HEADING(PedMichael(), 130.9538)
		ENDIF
		PRINTLN("ONE")
//	ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-112.789253,6448.438965,29.902973>>, <<-139.775772,6474.873047,33.973381>>, 20.250000)
//		SET_ENTITY_COORDS(PLAYER_PED_ID(), << -112.0260, 6450.0034, 30.4082 >>)
//		SET_ENTITY_HEADING(PLAYER_PED_ID(), 175.8292)
//		IF NOT IS_PED_INJURED(pedGunman)
//			SET_ENTITY_COORDS(pedGunman, << -108.5679, 6443.0996, 30.2999  >>)
//			SET_ENTITY_HEADING(pedGunman,  189.6457)
//		ENDIF
//		IF NOT IS_PED_INJURED(PedMichael())	
//			SET_ENTITY_COORDS(PedMichael(), << -106.7988, 6443.5405, 30.2999  >>)
//			SET_ENTITY_HEADING(PedMichael(), 172.8138)
//		ENDIF
//		PRINTLN("TWO")
//	ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-122.002777,6424.037109,27.066219>>, <<-97.364403,6399.529297,39.454288>>, 28.750000)
//		//2nd Angled Area Info
//		//2nd reposition point
//		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<  -110.9361, 6424.8779, 30.3421  >>)
//		SET_ENTITY_HEADING(PLAYER_PED_ID(), 112.5741)	
//		
//		IF NOT IS_PED_INJURED(pedGunman)
//			SET_ENTITY_COORDS(pedGunman, <<-114.7928, 6423.6880, 30.2834>>)
//			SET_ENTITY_HEADING(pedGunman, 89.5643)
//		ENDIF
//		IF NOT IS_PED_INJURED(PedMichael())	
//			SET_ENTITY_COORDS(PedMichael(), <<-113.7521, 6428.2407, 30.2913>>)
//			SET_ENTITY_HEADING(PedMichael(), 96.5671)
//		ENDIF
//		PRINTLN("THREE")
//	ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-121.371330,6423.742676,27.068905>>, <<-167.058044,6469.354980,38.553345>>, 28.750000)
//		//3rd Angled Area Info
//		//3rd reposition point
//		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-120.6597, 6439.8047, 30.5407 >>)
//		SET_ENTITY_HEADING(PLAYER_PED_ID(), 174.9532)	
//		IF NOT IS_PED_INJURED(pedGunman)
//			SET_ENTITY_COORDS(pedGunman, <<-119.5651, 6435.6787, 30.5732>>)
//			SET_ENTITY_HEADING(pedGunman, 183.9197)
//		ENDIF
//		IF NOT IS_PED_INJURED(PedMichael())	
//			SET_ENTITY_COORDS(PedMichael(), <<-124.7099, 6438.9648, 30.4020>>)
//			SET_ENTITY_HEADING(PedMichael(), 156.9534)
//		ENDIF
//		PRINTLN("FOUR")
//	ELSE
//		SET_ENTITY_COORDS(PLAYER_PED_ID(), << -105.0679, 6444.5288, 30.2999 >>)
//		SET_ENTITY_HEADING(PLAYER_PED_ID(), 156.4271)	
//		IF NOT IS_PED_INJURED(pedGunman)
//			SET_ENTITY_COORDS(pedGunman, << -108.5679, 6443.0996, 30.2999  >>)
//			SET_ENTITY_HEADING(pedGunman,  189.6457)
//		ENDIF
//		IF NOT IS_PED_INJURED(PedMichael())	
//			SET_ENTITY_COORDS(PedMichael(), << -106.7988, 6443.5405, 30.2999  >>)
//			SET_ENTITY_HEADING(PedMichael(), 172.8138)
//		ENDIF
//		PRINTLN("FIVE")
//	ENDIF

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		SET_ENTITY_COORDS(PLAYER_PED_ID(), << -105.0679, 6444.5288, 30.2999 >>)
//		SET_ENTITY_HEADING(PLAYER_PED_ID(), 156.4271)	
		CLEAR_PED_TASKS(PLAYER_PED_ID())		
		//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
	ENDIF
	
	IF NOT IS_PED_INJURED(pedGunman)
		//SET_ENTITY_COORDS(pedGunman, << -108.5679, 6443.0996, 30.2999  >>)
		//SET_ENTITY_HEADING(pedGunman,  189.6457)
		CLEAR_PED_TASKS(pedGunman)	
		//CLEAR_ROOM_FOR_ENTITY(pedGunman)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedGunman, robbersGroup)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGunman, FALSE)
		
		//FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGunman)
	ENDIF			
	IF NOT IS_PED_INJURED(PedMichael())				
		//SET_ENTITY_COORDS(PedMichael(), << -106.7988, 6443.5405, 30.2999  >>)
		//SET_ENTITY_HEADING(PedMichael(), 172.8138)
		CLEAR_PED_TASKS(PedMichael())	
		SET_PED_RELATIONSHIP_GROUP_HASH(PedMichael(), robbersGroup)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedMichael(), FALSE)
		//FORCE_PED_AI_AND_ANIMATION_UPDATE(PedMichael())
	ENDIF
	
	RESET_GAME_CAMERA()
		
ENDPROC

PROC POSITION_CREW_IN_ALLEY()

	IF NOT IS_PED_INJURED(PedMichael())
		SET_ENTITY_COORDS(PedMichael(), << -161.2710, 6298.8682, 30.2212 >>)
		SET_ENTITY_HEADING(PedMichael(),  128.503)
		SET_ENTITY_MAX_HEALTH(PedMichael(), 1000)
		RESTORE_PLAYER_PED_HEALTH(PedMichael())
	ENDIF

	IF NOT IS_PED_INJURED(pedGunman)
		CPRINTLN(DEBUG_MISSION, "POSITION_CREW_IN_ALLEY - teleporting gunman")
		SET_ENTITY_COORDS(pedGunman,<< -160.3512, 6300.1367, 30.1831 >>)
		SET_ENTITY_HEADING(pedGunman, 136.2267)		
	ENDIF			

	//IF bTrevorUsingMinigun
		IF IS_PLAYER_TREVOR()
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtTrevorWeaponEnteringCutscene, TRUE)
			SET_CURRENT_PED_WEAPON(PedMichael(), wtMichaelWeaponEnteringCutscene, TRUE)
		ELSE
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtMichaelWeaponEnteringCutscene, TRUE)
			SET_CURRENT_PED_WEAPON(pedTrevor(), wtTrevorWeaponEnteringCutscene, TRUE)
		ENDIF
	//ELSE
	//	IF IS_PLAYER_TREVOR()
			//SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtMichaelWeaponEnteringCutscene, TRUE)
		//ENDIF
		//SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, TRUE)
	//ENDIF
	

	IF IS_ENTITY_OK(PLAYER_PED_ID())
		
		IF bIsBuddyAPro
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-150.6469, 6311.3154, 30.3516>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(),  136.6582)
		ELSE
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-150.4590, 6311.4849, 30.3521>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 136.6363)
		ENDIF
		IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_TREVOR
		 	SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_RUN, 2000, 0.0, TRUE, TRUE)
			FORCE_PED_MOTION_STATE(player_ped_id(), MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT)
		ELIF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL
			SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_RUN, 2000, 0.0, TRUE, TRUE)
			FORCE_PED_MOTION_STATE(player_ped_id(), MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT)
		ENDIF
	ENDIF
		

ENDPROC

structTimelapse bh2Timelapse
BOOL bMIssionTriggeredAsMichael
BOOL bVehicleResolved = FALSE
BOOL bBoardPinned

PROC timeLapseCutscene()

	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		bMIssionTriggeredAsMichael = TRUE
	ELSE
		bMIssionTriggeredAsMichael = FALSE
	ENDIF
	
	IF NOT IS_GAMEPLAY_CAM_RENDERING()
	AND bVehicleResolved = FALSE
		
		DISPLAY_RADAR(FALSE)
		
		SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
		RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<1387.460205,3577.143799,33.484192>>, <<1372.830566,3623.059570,36.641121>>, 11.500000, << 1362.7167, 3591.3867, 33.9257 >>, 292.1517, <<5.0, 7.0, 8.5>>, TRUE)
		//DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<< 1362.7167, 3591.3867, 33.9257 >>, 292.1517)
		SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<< 1362.7167, 3591.3867, 33.9257 >>, 292.1517)
		bVehicleResolved = TRUE
	ENDIF
	
	IF bBoardPinned = FALSE
		PIN_HEIST_BOARD_IN_MEMORY(HEIST_RURAL_BANK, TRUE)
		bBoardPinned = TRUE
	ENDIF

	IF NOT IS_REPLAY_IN_PROGRESS()
	AND NOT IS_REPEAT_PLAY_ACTIVE()
				
		IF NOT DO_TIMELAPSE(SP_HEIST_RURAL_2, bh2Timelapse, TRUE, TRUE, FALSE, FALSE, TRUE)
		OR IS_SCREEN_FADING_OUT()
		OR IS_SCREEN_FADED_OUT()
			SWITCH iTimelapsePreloadCutsceneState
				CASE 0 
				
					
					
					//REQUEST_MODEL(HC_GUNMAN)
					SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE, FALSE)
					SWITCH GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_RURAL_BANK, 0))
						CASE CMSK_GOOD
							bIsBuddyAPro = TRUE
						BREAK
						CASE CMSK_MEDIUM
							bIsBuddyAPro = FALSE
						BREAK
						CASE CMSK_BAD
							bIsBuddyAPro = FALSE
						BREAK
					ENDSWITCH
					CPRINTLN(DEBUG_MISSION, "timeLapseCutscene - iTimelapsePreloadCutsceneState = 1")
					iTimelapsePreloadCutsceneState = 1
				BREAK
				
				CASE 1
//					IF HAS_MODEL_LOADED(HC_GUNMAN)
//						IF NOT DOES_ENTITY_EXIST(pedGunman)
//							pedGunman = CREATE_HEIST_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_RURAL_BANK,0), << 1375.4186, 3605.7351, 33.8493 >>, 198.8440, CREW_OUTFIT_SUIT)
//							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedGunman, TRUE)
//							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGunman, TRUE)
//							SET_PED_CAN_BE_DRAGGED_OUT(pedGunman, FALSE)
//							SET_PED_SUFFERS_CRITICAL_HITS(pedGunman, FALSE)
//							SET_PED_COMPONENT_VARIATION(pedGunman, INT_TO_ENUM(PED_COMPONENT,1), 1, 0, 0) //(berd) //Turn off harness
//						ENDIF
						
						REQUEST_CUTSCENE("RBH_2A_INT")
						
						REQUEST_MODELS_FOR_HEIST_CREW(HEIST_RURAL_BANK)
						REQUEST_MODEL(BURRITO3)
						REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_MICHAEL)) 
						REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
						REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
						
						PIN_HEIST_BOARD_IN_MEMORY(HEIST_RURAL_BANK, TRUE)
						
						CPRINTLN(DEBUG_MISSION, "timeLapseCutscene - iTimelapsePreloadCutsceneState = 2")
						iTimelapsePreloadCutsceneState = 2
//					ENDIF
				BREAK
				
				CASE 2
					IF NOT HAS_CUTSCENE_LOADED()					
						IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
							CPRINTLN(DEBUG_MISSION, "RBH_2A_INT - Setting ped varation in timeLapseCutscene")
							
							IF NOT bIsBuddyAPro
								SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("gunman_selection_1", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_RURAL_BANK, 0), CREW_OUTFIT_SUIT)
							ELSE
								SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("gunman_selection_2", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_RURAL_BANK, 0), CREW_OUTFIT_SUIT)
							ENDIF
						
							
							SET_CUTSCENE_PED_OUTFIT("Michael", PLAYER_ZERO, OUTFIT_P0_JEWEL_HEIST)
							SET_CUTSCENE_PED_OUTFIT("Trevor", PLAYER_TWO, OUTFIT_P2_STYLESUIT_5)
			
							
							SET_CUTSCENE_PED_OUTFIT("Franklin", PLAYER_ONE, OUTFIT_P1_STEALTH_NO_MASK)
							
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,0), 0, 0) //(head)
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,2), 0, 0) //(hair)
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,3), 0, 1) //(uppr)
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,4), 1, 1) //(lowr)
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,5), 0, 0) //(hand)
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,7), 0, 0) //(teef)
							SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,8), 1, 0) //(accs)
							SET_CUTSCENE_PED_PROP_VARIATION("Lester", INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
							SET_CUTSCENE_PED_PROP_VARIATION("Lester", INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
														
							CPRINTLN(DEBUG_MISSION, "timeLapseCutscene - iTimelapsePreloadCutsceneState = 3, we're done")
							iTimelapsePreloadCutsceneState = 3							
						ENDIF
					ENDIF 
				BREAK
				
				CASE 3
					IF IS_SCREEN_FADED_OUT()
					AND HAS_HEIST_BOARD_LOADED(HEIST_RURAL_BANK)
						CREATE_PINNED_HEIST_BOARD(HEIST_RURAL_BANK)
						CLEAR_AREA_OF_VEHICLES(<< 1375.4186, 3605.7351, 33.8493 >>, 100.0)
						//SCRIPT_ASSERT("board created")		
						//WAIT(5000)
						mission_stage = STAGE_INITIALISE	
					ENDIF
				BREAK
								
			ENDSWITCH
		ELSE
			IF HAS_HEIST_BOARD_LOADED(HEIST_RURAL_BANK)
				CREATE_PINNED_HEIST_BOARD(HEIST_RURAL_BANK)
				CLEAR_AREA_OF_VEHICLES(<< 1375.4186, 3605.7351, 33.8493 >>, 100.0)
				//SCRIPT_ASSERT("board created")
				//WAIT(5000)
				mission_stage = STAGE_INITIALISE	
			ENDIF
		ENDIF
	ELSE
		IF HAS_HEIST_BOARD_LOADED(HEIST_RURAL_BANK)
			CREATE_PINNED_HEIST_BOARD(HEIST_RURAL_BANK)
			CLEAR_AREA_OF_VEHICLES(<< 1375.4186, 3605.7351, 33.8493 >>, 100.0)
			//SCRIPT_ASSERT("board created")
			//WAIT(5000)
			mission_stage = STAGE_INITIALISE	
		ENDIF
	ENDIF
	
ENDPROC

FUNC MISSION_STAGE_FLAG GET_GAMEPLAY_STAGE_BY_NUMBER(INT iReplayStageNumber)
	SWITCH iReplayStageNumber
		CASE 0
		CASE 1
			RETURN STAGE_DEAL_WITH_FRANKLIN
		BREAK
		
		CASE 2
			RETURN STAGE_GET_TO_BANK
		BREAK
		
		CASE 3
			RETURN STAGE_ROB_THE_BANK
		BREAK
		
		CASE 4
			RETURN STAGE_HEIST_CUTSCENE
		BREAK
		
		CASE 5
			RETURN STAGE_HOLD_OFF_COPS_AT_BANK
		BREAK
				
		CASE 6
			RETURN STAGE_ADVANCE_TO_GARDENS
		BREAK
		
		CASE 7
			RETURN STAGE_CONTINUE_DOWN_STREET
		BREAK
		
		CASE 8
			RETURN STAGE_PROCEED_TO_ALLEY
		BREAK
		
		CASE 9
			RETURN STAGE_HOLD_OFF_ARMY_AT_JUNK_YARD
		BREAK
		
		CASE 10
			RETURN STAGE_SWAPPED_TO_BULLDOZER
		BREAK
		
		CASE 11
			RETURN STAGE_RESCUE_BUDDYS
		BREAK
		
		CASE 12
			RETURN STAGE_CHICKEN_FACTORY
		BREAK
		
//		CASE 15
//			RETURN STAGE_TRAIN_PLATFORM
//		BREAK
		
	ENDSWITCH
	
	CPRINTLN(DEBUG_MISSION, "GET_GAMEPLAY_STAGE_BY_NUMBER - FUCK!!!  We didn't find a stage!!!  This is REALLY REALLY BAD!!!")
	RETURN STAGE_INITIALISE
ENDFUNC

PROC INITIALISE_MISSION_COMMON(MISSION_STAGE_FLAG msfFlag = STAGE_INITIALISE)

	VECTOR vStartPosition

	vStartPosition	= << 1372.4972, 3618.1499, 33.8920 >>	
	
	IF IS_REPLAY_IN_PROGRESS()
		msfFlag = GET_GAMEPLAY_STAGE_BY_NUMBER(Get_Replay_Mid_Mission_Stage())
	ENDIF
	
	//IF msfFlag <> STAGE_INITIALISE
		 
		SWITCH msfFlag
		 	
			CASE STAGE_GET_TO_BANK
			CASE STAGE_ROB_THE_BANK
			CASE STAGE_HEIST_CUTSCENE
			CASE STAGE_HOLD_OFF_COPS_AT_BANK
			CASE STAGE_CHOPPER_TURNS_UP
			CASE STAGE_ADVANCE_TO_GARDENS
						
				vStartPosition	= vBankCoordinates
			BREAK
			
			CASE STAGE_CONTINUE_DOWN_STREET
				vStartPosition	= <<-179.8882, 6368.3516, 30.4719>>
			BREAK
			
			CASE STAGE_PROCEED_TO_ALLEY
				vStartPosition	= <<-153.2334, 6306.7729, 30.4021>>
			BREAK
			
			CASE STAGE_HOLD_OFF_ARMY_AT_JUNK_YARD
				vStartPosition	= <<-153.2334, 6306.7729, 30.4021>>
			BREAK
			
			CASE STAGE_HOT_SWAP_TO_BULLDOZER
				vStartPosition	= <<-180.7744, 6287.9375, 30.4893>>
			BREAK
			
			CASE STAGE_SWAPPED_TO_BULLDOZER
				vStartPosition	= <<53.1923, 6544.6079, 30.0292>>
			BREAK
			
				CASE STAGE_RESCUE_BUDDYS
					vStartPosition	= <<-191.8555, 6290.6719, 30.4891>>
				BREAK
				
				CASE STAGE_CHICKEN_FACTORY
					vStartPosition	= <<-73.9761, 6266.3618, 30.2867 >>	
				BREAK
			
			CASE STAGE_TRAIN_PLATFORM
				vStartPosition	= << -73.9761, 6266.3618, 30.2867 >>	
			BREAK
			
			CASE STAGE_END_MOCAP_SCENE
				vStartPosition	= <<2089.8059, 3678.8074, 36.7033>>	
			BREAK
			
		ENDSWITCH
		 	 		 
	//ENDIF
	
	IF msfFlag >= STAGE_ADVANCE_TO_GARDENS
		SET_MOTEL_STATE(BUILDINGSTATE_DESTROYED)
	ELSE
		SET_MOTEL_STATE(BUILDINGSTATE_NORMAL)
	ENDIF
	
	IF msfFlag > STAGE_HOLD_OFF_COPS_AT_BANK
		IF g_replay.iReplayInt[REPLAY_VALUE_GAS_STATION_STATE] > 0
			//SCRIPT_ASSERT("yeah its funcking brunin innit REPLAY")
			SET_GAS_STATION_STATE(BUILDINGSTATE_DESTROYED)
		ELSE
			//SCRIPT_ASSERT("yeah its fucking intact innit REPLAY")
			SET_GAS_STATION_STATE(BUILDINGSTATE_NORMAL)
		ENDIF
	ELSE
		SET_GAS_STATION_STATE(BUILDINGSTATE_NORMAL)
	ENDIF
	
	START_REPLAY_SETUP(vStartPosition, 0.0, FALSE)
	

	#IF IS_DEBUG_BUILD
		g_b_CellDialDebugTextToggle = FALSE
	#ENDIF
	
	IF (Is_Replay_In_Progress())
		PRINTLN("PALETO SCORE 2A: INT_TO_ENUM(MISSION_STAGE_FLAG, Get_Replay_Mid_Mission_Stage():", INT_TO_ENUM(MISSION_STAGE_FLAG, Get_Replay_Mid_Mission_Stage()))
		//SCRIPT_ASSERT("replay!")
		//One a replay assume we are Michael before switching to other stages, keeps things consistent.
		WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
			WAIT(0)
		ENDWHILE
	ENDIF

	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)

	IF NOT DOES_CAM_EXIST(destinationCam)
		destinationCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	ENDIF
	IF NOT DOES_CAM_EXIST(initialCam)
		initialCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	ENDIF
	
	DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, TRUE)
	DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, TRUE)
	
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("TIME_LAPSE")
	
	SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
	
	REMOVE_RELATIONSHIP_GROUP(copGroup)
	REMOVE_RELATIONSHIP_GROUP(dummyGroup)
	
	ADD_RELATIONSHIP_GROUP("DUMMY", dummyGroup)
	
	ADD_RELATIONSHIP_GROUP("COPS", copGroup)	
	
	//ADD_RELATIONSHIP_GROUP("RURALROBBERS", robbersGroup)
    robbersGroup = RELGROUPHASH_PLAYER
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, copGroup, robbersGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, robbersGroup, copGroup)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, copGroup, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, copGroup)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, copGroup, dummyGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, dummyGroup, robbersGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, robbersGroup, dummyGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, dummyGroup)
		
//	IF NOT g_savedGlobals.sFlow.isGameflowActive
//		SET_GAS_STATION_STATE(BUILDINGSTATE_NORMAL)
//	ENDIF
	
	DISABLE_TAXI_HAILING(TRUE)
	iCopRespawnIndex = iCopRespawnIndex
	REQUEST_ADDITIONAL_TEXT("BANKH1", MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("RBH2AUD", MISSION_DIALOGUE_TEXT_SLOT)

	// Load the models
	CPRINTLN(DEBUG_MISSION, "INITIALISE_MISSION_COMMON - Requesting models for the heist crew")
	REQUEST_MODELS_FOR_HEIST_CREW(HEIST_RURAL_BANK)
	REQUEST_MODEL(BURRITO3)
	REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_MICHAEL)) 
	REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
	REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
	
	//Stop scenarios in chicken factory...
	sbiBlockIndex1 = ADD_SCENARIO_BLOCKING_AREA(<< -219.6990, 6046.3076, -32.6326 >>, << -75.2492, 6314.1445, 60.3442 >>)
	SET_PED_NON_CREATION_AREA(<< -219.6990, 6046.3076, -32.6326 >>, <<-9.6898, 6343.2207,60.3442 >>)
	
	//At bank
	sbiBlockIndex2 = ADD_SCENARIO_BLOCKING_AREA(<<-144.8006, 6424.3433, 28.4045>>, <<-79.3863, 6508.7993, 42.4219>>)
	SET_PED_NON_CREATION_AREA(<<-144.8006, 6424.3433, 28.4045>>, <<-79.3863, 6508.7993, 42.4219>>)
	
	//At bank
	sbiBlockIndex3JunkYard = ADD_SCENARIO_BLOCKING_AREA(<<-335.8466, 6115.8525, 26.8181>>, <<93.8773, 6562.6846, 45.4410>>)
	SET_PED_NON_CREATION_AREA(<<-335.8466, 6115.8525, 26.8181>>, <<93.8773, 6562.6846, 45.4410>>)
	
		
	//Remove vehicle gens from near the bank
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-130.1514, 6424.5127, 28.8839>>, <<-97.6329, 6508.9307, 47.8846>>, FALSE)
	REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<-130.1514, 6424.5127, 28.8839>>, <<-97.6329, 6508.9307, 47.8846>>)

	IF IS_SCREEN_FADED_OUT()
		LOAD_ALL_OBJECTS_NOW()
	ENDIF

	WHILE NOT HAVE_MODELS_LOADED_FOR_HEIST_CREW(HEIST_RURAL_BANK) //HAS_MODEL_LOADED(G_M_M_Ballistic_01)
	OR NOT HAS_MODEL_LOADED(BURRITO3)
	OR NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
	OR NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_TREVOR))
	OR NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		WAIT(0)
	ENDWHILE
		
	DELETE_VEHICLE(arrivalBurrito) //Delete old burrito (replay)
	arrivalBurrito = CREATE_VEHICLE(BURRITO3,  vStartPosition, 198.5876)
	IF IS_ENTITY_OK(arrivalBurrito)
		SET_ENTITY_LOD_DIST(arrivalBurrito, 150)
		ADD_VEHICLE_UPSIDEDOWN_CHECK(arrivalBurrito)
		SET_VEHICLE_HAS_STRONG_AXLES(arrivalBurrito, TRUE)
		SET_VEHICLE_COLOURS(arrivalBurrito, 134, 134)
		SET_VEHICLE_DIRT_LEVEL(arrivalBurrito, 7.0)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(arrivalBurrito, SC_DOOR_REAR_LEFT, FALSE)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(arrivalBurrito, SC_DOOR_REAR_RIGHT, FALSE)
	ENDIF
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BURRITO, TRUE)	//Suppress this too standard burrito looks a bit too similar.
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BURRITO3, TRUE)
	
	IF NOT DOES_ENTITY_EXIST(pedGunman)
		pedGunman = CREATE_HEIST_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_RURAL_BANK,0), vStartPosition, 198.8440, CREW_OUTFIT_SUIT)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedGunman)
		IF NOT IS_PED_INJURED(pedGunman)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedGunman, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGunman, TRUE)
			SET_PED_CAN_BE_DRAGGED_OUT(pedGunman, FALSE)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedGunman, robbersGroup)
			SET_PED_SUFFERS_CRITICAL_HITS(pedGunman, FALSE)
			SET_PED_COMPONENT_VARIATION(pedGunman, INT_TO_ENUM(PED_COMPONENT,1), 1, 0, 0) //(berd) //Turn off harness
			SET_PED_INTO_VEHICLE(pedGunman, arrivalBurrito, VS_BACK_RIGHT)
			
			GIVE_WEAPON_TO_PED(pedGunman, WEAPONTYPE_PISTOL, INFINITE_AMMO)
			
		ENDIF
	ENDIF
	
	SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), robbersGroup)

	IF IS_GUNMAN_GUSTAVO()
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "GUSTAVO")
	ELIF IS_GUNMAN_DARYL()
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "DARYL")
	ELIF IS_GUNMAN_CHEF()
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "CHEF")
	ELIF IS_GUNMAN_NORM()
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "NORM")
	ELIF IS_GUNMAN_PACKIE()
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "PACKIE")
	ENDIF
	 
	strCrewMember = GET_CREW_MEMBER_FIRST_NAME_LABEL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_RURAL_BANK,0))

	SWITCH GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_RURAL_BANK, 0))
		CASE CMSK_GOOD
			bIsBuddyAPro = TRUE
		BREAK
		CASE CMSK_MEDIUM
			bIsBuddyAPro = FALSE
		BREAK
		CASE CMSK_BAD
			bIsBuddyAPro = FALSE
		BREAK
	ENDSWITCH

	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)	  
	
	REGISTER_SCRIPT_WITH_AUDIO()

	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
		
	vDefensiveAreasOutSideBank[0] = << -104.2944, 6448.7598, 30.6407 >>
	vDefensiveAreasOutSideBank[1] = << -114.6959, 6439.7471, 30.5856 >>
	vDefensiveAreasOutSideBank[2] = << -117.1274, 6440.2896, 30.6160 >>
	vDefensiveAreasOutSideBank[3] = << -118.6372, 6440.4424, 30.6077 >>
	vDefensiveAreasOutSideBank[4] = << -120.6828, 6442.4619, 30.5654 >>
	vDefensiveAreasOutSideBank[5] = << -122.9414, 6444.7603, 30.5648 >>
	vDefensiveAreasOutSideBank[6] = << -127.2614, 6452.2129, 30.5805 >>
	vDefensiveAreasOutSideBank[7] = << -143.2521, 6463.7842, 30.5141 >>

	//Create Trigger Boxes
	tbChopperAndAllyMove = CREATE_TRIGGER_BOX(<<-126.384926,6427.589844,30.062336>>, <<-155.502701,6457.361816,36.461121>>, 21.750000)//<<-129.764923,6453.356934,39.543861>>, <<-161.497742,6420.603516,29.915985>>, 60.000000)
	tbTriggerSwatVansDownStreet = CREATE_TRIGGER_BOX(<<-191.847321,6379.657227,29.865108>>, <<-158.847275,6346.742188,34.584343>>, 30.000000)
	tbTaskArmyTrucksInJunkYard = CREATE_TRIGGER_BOX(<<-158.722702,6302.036133,29.396208>>, <<-190.770477,6272.418945,35.489235>>, 50.000000)

	tbBulldozerTriggerCarWave2 = CREATE_TRIGGER_BOX(<<-77.165703,6454.854980,30.203167>>, <<-123.846001,6407.049805,34.689064>>, 34.000000)//<<-88.800217,6439.700195,29.443138>>, <<-127.841179,6402.817871,34.434330>>, 40.000000)
	tbBulldozerTriggerCarWave3 = CREATE_TRIGGER_BOX(<<-136.359436,6395.908691,29.529219>>, <<-193.870132,6340.311035,34.513885>>, 70.0)//50.000000)
	tbBulldozerTriggerCarMid1 = CREATE_TRIGGER_BOX(<<5.608665,6539.974609,30.184906>>, <<-36.466759,6498.897949,34.189026>>, 28.000000)
	tbBulldozerTriggerCarMid2 = CREATE_TRIGGER_BOX(<<-49.851112,6485.619629,29.437103>>, <<-93.430847,6441.227051,34.430016>>, 50.000000)
	//tbBulldozerTriggerTank = CREATE_TRIGGER_BOX(<<-187.608597,6344.820313,29.495378>>, <<-214.807648,6317.607422,34.477242>>, 30.000000)

	tbOnTrainPlatform = CREATE_TRIGGER_BOX(<<-141.805969,6151.607910,29.589340>>, <<-157.107986,6136.521973,35.441154>>, 11.750000)
	tbTrainJump = CREATE_TRIGGER_BOX(<<-148.595108,6140.896484,31.366983>>, <<-139.879333,6149.674805,34.111122>>, 6.500000)

	//Cover Blocking Areas
//	ADD_COVER_BLOCKING_AREA(<<-182.1849, 6308.8022, 28.3864>>, <<-193.1495, 6329.4111, 36.7905>>, TRUE, TRUE, TRUE)
	
	//SET_PLAYER_CAN_USE_COVER(PLAYER_ID(), FALSE)
		
ENDPROC

PROC SETUP_CREW_VARIABLES(PED_INDEX thisPed, BOOL bAggressive = FALSE)

	SET_PED_RELATIONSHIP_GROUP_HASH(thisPed, robbersGroup) 
	SET_PED_SUFFERS_CRITICAL_HITS(thisPed, FALSE)
	SET_PED_CAN_BE_TARGETTED(thisPed, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_USE_COVER, TRUE)
	IF bAggressive
		SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_AGGRESSIVE, TRUE)	
	ENDIF
	SET_PED_COMBAT_RANGE(thisPed, CR_FAR)
	SET_PED_TARGET_LOSS_RESPONSE(thisPed, TLR_NEVER_LOSE_TARGET)
	//SET_PED_SHOOT_RATE(thisPed, 500)
	
	SET_PED_SHOOT_RATE(thisPed, 80)
	
	SET_PED_ACCURACY(thisPed, 5)
	//SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_USE_VEHICLE, FALSE)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(thisPed, TRUE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thisPed, TRUE)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(thisPed, FALSE)
	SET_PED_CAN_BE_DRAGGED_OUT(thisPed, FALSE)
	
ENDPROC


PROC DELETE_PLAYERS_AND_CREW()
	
	IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
		IF DOES_ENTITY_EXIST(pedSelector.pedID[SELECTOR_PED_MICHAEL])
			IF NOT IS_ENTITY_DEAD(pedSelector.pedID[SELECTOR_PED_MICHAEL])
				IF NOT IS_PED_A_PLAYER(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					DELETE_PED(pedSelector.pedID[SELECTOR_PED_MICHAEL])
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_TREVOR
		IF DOES_ENTITY_EXIST(pedSelector.pedID[SELECTOR_PED_TREVOR])
			IF NOT IS_ENTITY_DEAD(pedSelector.pedID[SELECTOR_PED_TREVOR])
				IF NOT IS_PED_A_PLAYER(pedSelector.pedID[SELECTOR_PED_TREVOR])
					DELETE_PED(pedSelector.pedID[SELECTOR_PED_TREVOR])
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
		IF DOES_ENTITY_EXIST(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
			IF NOT IS_ENTITY_DEAD(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
				IF NOT IS_PED_A_PLAYER(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
					DELETE_PED(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(pedGunman)
		IF NOT IS_ENTITY_DEAD(pedGunman)
			IF NOT IS_PED_A_PLAYER(pedGunman)
				DELETE_PED(pedGunman)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


PROC CREATE_ARMY_PEDS_FOR_PICKUP_CUTSCENE()

	DELETE_ARRAY_OF_PEDS(pedsArmyInJunkYard)
	SPAWN_NEW_SET_OF_ARMY_GUYS()
	DELETE_ARRAY_OF_BLIPS(blippedsArmyInJunkYard)
					
	IF NOT IS_PED_INJURED(pedsArmyInJunkYard[0])
		SET_PED_COMBAT_ATTRIBUTES(pedsArmyInJunkYard[0], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsArmyInJunkYard[0] ,FALSE)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedsArmyInJunkYard[0], TRUE)
		SET_ENTITY_COORDS(pedsArmyInJunkYard[0], << -178.3539, 6276.7661, 30.4902 >>)
		SET_ENTITY_HEADING(pedsArmyInJunkYard[0], 151.97)
		SET_PED_SPHERE_DEFENSIVE_AREA(pedsArmyInJunkYard[0], << -178.3539, 6276.7661, 30.4902 >>, 2.0)
		TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedsArmyInJunkYard[0], 100.00)
		
		SET_PED_ACCURACY(pedsArmyInJunkYard[0], 1)
		
	ENDIF
	IF NOT IS_PED_INJURED(pedsArmyInJunkYard[1])
		SET_PED_COMBAT_ATTRIBUTES(pedsArmyInJunkYard[1], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, FALSE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsArmyInJunkYard[1] ,FALSE)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedsArmyInJunkYard[1], TRUE)
		SET_ENTITY_COORDS(pedsArmyInJunkYard[1], << -204.3328, 6268.1226, 30.4392 >>)
		SET_ENTITY_HEADING(pedsArmyInJunkYard[1], 336.9375)
		SET_PED_SPHERE_DEFENSIVE_AREA(pedsArmyInJunkYard[1], << -204.3328, 6268.1226, 30.4392 >>, 2.0)
		TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedsArmyInJunkYard[1], 100.00)
		SET_PED_ACCURACY(pedsArmyInJunkYard[1], 1)
	ENDIF
	IF NOT IS_PED_INJURED(pedsArmyInJunkYard[2])
		SET_PED_COMBAT_ATTRIBUTES(pedsArmyInJunkYard[2], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, FALSE)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedsArmyInJunkYard[2], TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsArmyInJunkYard[2] ,FALSE)
		SET_ENTITY_COORDS(pedsArmyInJunkYard[2], << -175.9242, 6295.5908, 30.4906 >>)
		SET_ENTITY_HEADING(pedsArmyInJunkYard[2], 51.7215)
		SET_PED_SPHERE_DEFENSIVE_AREA(pedsArmyInJunkYard[2], << -175.9242, 6295.5908, 30.4906 >>, 2.0)
		TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedsArmyInJunkYard[2], 100.00)
		SET_PED_ACCURACY(pedsArmyInJunkYard[2], 1)
	ENDIF
	IF NOT IS_PED_INJURED(pedsArmyInJunkYard[3])
		SET_PED_COMBAT_ATTRIBUTES(pedsArmyInJunkYard[3], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, FALSE)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedsArmyInJunkYard[3], TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsArmyInJunkYard[3] ,FALSE)
		SET_ENTITY_COORDS(pedsArmyInJunkYard[3], <<-170.7897, 6288.6826, 30.4894>>)
		SET_ENTITY_HEADING(pedsArmyInJunkYard[3], 46.8856)
		SET_PED_SPHERE_DEFENSIVE_AREA(pedsArmyInJunkYard[3], <<-170.7897, 6288.6826, 30.4894>>, 2.0)
		TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedsArmyInJunkYard[3], 100.00)
		SET_PED_ACCURACY(pedsArmyInJunkYard[3], 1)
		
		SET_PED_CONFIG_FLAG(pedsArmyInJunkYard[3],	PCF_DisableExplosionReactions, TRUE)
		
	ENDIF
	
	DELETE_PED(pedsArmyInJunkYard[2])
	DELETE_PED(pedsArmyInJunkYard[4])
	DELETE_PED(pedsArmyInJunkYard[5])
	DELETE_PED(pedsArmyInJunkYard[6])

ENDPROC

//PROC REPLAY_OR_Z_SKIP_REPOSITION(BOOL bIsReplay, VECTOR vPos, FLOAT fHeading)
//
//	IF bIsReplay
//		START_REPLAY_SETUP(vPos, fHeading)		
//	ELSE
//		
//		SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos)
//		SET_ENTITY_HEADING(PLAYER_PED_ID(), fHeading)
//		LOAD_SCENE(vPos)
//	ENDIF
//
//ENDPROC


VECTOR vPossibleCoverPointsJunk[3]
BOOL bCoverTakenJunk[3]
BOOL bCoverStolenJunk[3]

FUNC VECTOR FREE_COVER_POINT_IN_JUNK_YARD()

	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)

	INT i
	PRINTNL()
	FOR i = 0 TO 2
		PRINTLN("i:", i, "stolen:", bCoverStolenJunk[i], "taken: ", bCoverTakenJunk[i])
		IF bCoverStolenJunk[i] = FALSE
		AND bCoverTakenJunk[i] = FALSE
			//DRAW_DEBUG_SPHERE(vPossibleCoverPointsJunk[i], 2.0, 0, 255,0 )
		
			//bCoverTakenJunk[i] = TRUE
			PRINTLN("Returning: ", i, "vec:",vPossibleCoverPointsJunk[i])
			RETURN vPossibleCoverPointsJunk[i]
		ELSE
			//DRAW_DEBUG_SPHERE(vPossibleCoverPointsJunk[i], 2.0, 255, 0,0 )
		ENDIF
	ENDFOR
	
	RETURN <<0.0, 0.0, 0.0>>

ENDFUNC


PROC POSITION_GUYS_AFTER_ALLEY_CUT()

	POSITION_CREW_IN_ALLEY()			
	
	IF NOT IS_ENTITY_DEAD(PedMichael())
		IF NOT bIsBuddyAPro
			CLEAR_PED_TASKS_IMMEDIATELY(PedMichael())
			
			//REmember PedMichael() returns Michael OR Trevor if you are playing as Michael
			IF IS_PLAYER_MICHAEL()	//Reposition him so it doesn't look too weird after the cut.
				SET_ENTITY_COORDS(PedMichael(), <<-156.6591, 6307.1577, 30.4852>>)
				SET_ENTITY_HEADING(PedMichael(), 109.6252)
			ELSE
				SET_ENTITY_COORDS(PedMichael(),<<-157.9228, 6304.4980, 30.4202>>)
				SET_ENTITY_HEADING(PedMichael(), 65.2353)
			ENDIF
			
			TASK_SHOOT_AT_COORD(PedMichael(), << -178.5373, 6318.2622, 30.2044 >>, 1000, FIRING_TYPE_CONTINUOUS)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PedMichael(), TRUE)
			SET_PED_RESET_FLAG(PedMichael(), PRF_InstantBlendToAim, TRUE)
		ELSE
			//REmember PedMichael() returns Michael OR Trevor if you are playing as Michael
			IF IS_PLAYER_MICHAEL()	//Reposition him so it doesn't look too weird after the cut.
				SET_ENTITY_COORDS(PedMichael(),<<-152.6751, 6311.7100, 30.4863>>)
				SET_ENTITY_HEADING(PedMichael(), 131.4041)
			ELSE
				//CLEAR_PED_TASKS_IMMEDIATELY(PedMichael())
				SET_ENTITY_COORDS(PedMichael(),<< -158.6604, 6304.3594, 30.4147 >>)
				SET_ENTITY_HEADING(PedMichael(), 136.2459)
			ENDIF
			
		ENDIF
		
		SET_PED_ACCURACY(PedMichael(), 100)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedMichael(), TRUE)
		SET_PED_COMBAT_ABILITY(PedMichael(), CAL_PROFESSIONAL)
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(pedGunman)
		IF NOT bIsBuddyAPro
			//CLEAR_PED_TASKS_IMMEDIATELY(pedGunman)
			SET_ENTITY_COORDS(pedGunman,<< -160.3512, 6300.1367, 30.1831 >>)
			SET_ENTITY_HEADING(pedGunman, 300.2267)
			TASK_AIM_GUN_AT_COORD(pedGunman, << -143.8007, 6313.9688, 32.1725 >>, INFINITE_TASK_TIME)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGunman, TRUE)
			SET_PED_CAN_RAGDOLL(pedGunman, FALSE)
			SET_ENTITY_INVINCIBLE(pedGunman, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGunman, TRUE)
			SET_PED_RESET_FLAG(pedGunman, PRF_InstantBlendToAim, TRUE)
		ELSE
			//CLEAR_PED_TASKS_IMMEDIATELY(pedGunman)
			SET_ENTITY_COORDS(pedGunman,<< -160.3512, 6300.1367, 30.1831 >>)
			SET_ENTITY_HEADING(pedGunman, 136.2267)
		ENDIF
	ENDIF
	
ENDPROC

PROC TASK_PEDS_AFTER_ALLEY_CUT(WEAPON_TYPE wtMWeapon, WEAPON_TYPE wtGWeapon)

	vPossibleCoverPointsJunk[0] = <<-170.8159, 6288.7129, 30.4894>>
	vPossibleCoverPointsJunk[1] = <<-173.7987, 6293.0923, 30.4894>>
	vPossibleCoverPointsJunk[2] = <<-172.8244, 6296.8516, 30.4894>>
									
	//WAYPOINT_RECORDING_GET_COORD("RBHArmySec2", 0, vMichaelWayPointStart)
	IF NOT IS_ENTITY_DEAD(PedMichael())
		TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedMichael(), TRUE)
		OPEN_SEQUENCE_TASK(seqShootThroughBigFence)	
			TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, << -170.2119, 6288.2974, 30.4905 >>, << -170.2119, 6288.2974, 30.4905 >>, PEDMOVE_RUN, TRUE)
			TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, vPossibleCoverPointsJunk[0], 2.5)
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 120.0)
		CLOSE_SEQUENCE_TASK(seqShootThroughBigFence)
		TASK_PERFORM_SEQUENCE(PedMichael(), seqShootThroughBigFence)
		CLEAR_SEQUENCE_TASK(seqShootThroughBigFence)
		SET_CURRENT_PED_WEAPON(PedMichael(), wtMWeapon, TRUE)
		FORCE_PED_MOTION_STATE(PedMichael(), MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT, TRUE)
	ENDIF


	IF NOT IS_ENTITY_DEAD(pedGunman)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGunman, TRUE)
		OPEN_SEQUENCE_TASK(seqShootThroughBigFence)	
			
			TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, <<-173.0214, 6292.8364, 30.4894>>, <<-173.0214, 6292.8364, 30.4894>>, PEDMOVE_RUN, TRUE)
			//TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, <<-173.0214, 6292.8364, 30.4894>>, 2.0)						
			TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
			TASK_COMBAT_HATED_TARGETS_IN_AREA(NULL,<<-199.5959, 6278.3799, 31.1552>>, 25.0)
		CLOSE_SEQUENCE_TASK(seqShootThroughBigFence)
		TASK_PERFORM_SEQUENCE(pedGunman, seqShootThroughBigFence)
		CLEAR_SEQUENCE_TASK(seqShootThroughBigFence)
		
		//SET_PED_ANGLED_DEFENSIVE_AREA(pedGunman, <<-178.510406,6291.511719,29.989462>>, <<-170.821182,6302.328613,32.739464>>, 7.500000)
		
		SET_PED_SPHERE_DEFENSIVE_AREA(pedGunman, vPossibleCoverPointsJunk[1], 2.5, TRUE)
		SET_CURRENT_PED_WEAPON(pedGunman, wtGWeapon, TRUE)
		FORCE_PED_MOTION_STATE(pedGunman, MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT, TRUE)
	ENDIF
				
ENDPROC


BOOL bClearGateArea1 = FALSE
BOOL bClearGateArea2 = FALSE

PROC MANAGE_SKIP(MISSION_STAGE_FLAG to_this_mission_stage, BOOL bIsReplay = FALSE)
		
	iReturnStage = ENUM_TO_INT(to_this_mission_stage)
	
	PRINTLN("-----------------------------------------")
	PRINTLN("RBH MANAGE_SKIP selecting:",  iReturnStage)
	PRINTLN("-----------------------------------------")

	bTimeSetInit = FALSE
	
	bClearGateArea1 = FALSE
	bClearGateArea2 = FALSE

	
	IF iReturnStage > ENUM_TO_INT(STAGE_ROB_THE_BANK)			
		IF iCurrentTake = 0
			iCurrentTake = RURAL_HEIST_TAKE	
			iDisplayedTake = iCurrentTake
		ENDIF
	ENDIF
	
	IF to_this_mission_stage >= STAGE_HOLD_OFF_COPS_AT_BANK
		SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_SHOOTING )
	ENDIF

	SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")

	//Do additional work when selecting stage
	SWITCH to_this_mission_stage
	
		CASE STAGE_INTRO_MOCAP	//2
		
		BREAK
		
		CASE STAGE_DEAL_WITH_FRANKLIN	//3
			
										
			IF IS_ENTITY_OK(PLAYER_PED_ID()) AND IS_ENTITY_OK(arrivalBurrito)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), arrivalBurrito)								
			ENDIF
			
			END_REPLAY_SETUP(arrivalBurrito)
			
		BREAK
	
		CASE STAGE_GET_TO_BANK	//4
					
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "STAGE_GET_TO_BANK")
		
			//initialiseMission2A()
			IF IS_ENTITY_OK(PLAYER_PED_ID()) AND IS_ENTITY_OK(arrivalBurrito)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), arrivalBurrito)												
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), arrivalBurrito)
				ENDIF
				SET_ENTITY_HEADING(arrivalBurrito,  105.9750)
				SET_ENTITY_COORDS(arrivalBurrito, << 10.6583, 6647.2236, 30.5232 >>)
				SET_VEHICLE_ON_GROUND_PROPERLY(arrivalBurrito)				
				SET_VEHICLE_ENGINE_ON(arrivalBurrito, TRUE, TRUE)				
			ENDIF
			KILL_ANY_CONVERSATION()
			CLEAR_PRINTS()
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			iGetDealWithFranklin = 9
			iReturnStage--
			
			END_REPLAY_SETUP(arrivalBurrito)
				
		BREAK
	
		CASE STAGE_ROB_THE_BANK	//SkipMenuStruct[1].sTxtLabel = "STAGE_HEIST_CUTSCENE"   
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "STAGE_ROB_THE_BANK")
			REQUEST_MODEL(WASHINGTON)
			WHILE NOT HAS_MODEL_LOADED(WASHINGTON)
				WAIT(0)
			ENDWHILE
			CREATE_BANK_PARKED_CARS()
			
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)					
			IF IS_ENTITY_OK(PLAYER_PED_ID()) AND IS_ENTITY_OK(arrivalBurrito)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), arrivalBurrito)
				SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), <<-127.7515, 6465.9473, 30.4164>>)//vBankParkingLocation)
				SET_ENTITY_HEADING(arrivalBurrito, -45)
			ENDIF
			
			IF IS_ENTITY_OK(pedTrevor())
				GIVE_WEAPON_TO_PED(pedTrevor(), WEAPONTYPE_SAWNOFFSHOTGUN, 150, TRUE, TRUE)
			ENDIF
			
			IF IS_ENTITY_OK(pedGunman)
				GIVE_WEAPON_TO_PED(pedGunman, pedGunmanWeaponType, INFINITE_AMMO, TRUE, TRUE)
			ENDIF
			
			END_REPLAY_SETUP(arrivalBurrito)
			
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL, SPECIAL_P0_BALACLAVA , FALSE)
			IF NOT IS_PED_INJURED(pedTrevor())
				SET_PED_COMP_ITEM_CURRENT_SP(pedTrevor(), COMP_TYPE_SPECIAL, SPECIAL_P2_BALACLAVA , FALSE)			
			ENDIF
			IF NOT IS_PED_INJURED(pedGunman)
				SET_PED_COMPONENT_VARIATION(pedGunman, INT_TO_ENUM(PED_COMPONENT,9), 4, 0, 0) //(task)
				SET_PED_COMPONENT_VARIATION(pedGunman, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
			ENDIF
			iBalaclavaStage = -1 //Dont play the put on balaclavas animation
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
		BREAK
		
		CASE STAGE_HEIST_CUTSCENE	//SkipMenuStruct[1].sTxtLabel = "STAGE_HEIST_CUTSCENE"   
			
			cleanUpBeforeBankStages()
			
			FORCE_SWITCH_TO_TREVOR()
			
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "STAGE_HEIST_CUTSCENE")
			
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)					
			
			SET_PED_COORDS_KEEP_VEHICLE(PLAYER_PED_ID(), << -114.4287, 6459.3423, 30.4685 >>)
			DELETE_VEHICLE(arrivalBurrito)
						
			IF trevorVehicle.model <> DUMMY_MODEL_FOR_SCRIPT
				SET_MODEL_AS_NO_LONGER_NEEDED(trevorVehicle.model)
			ENDIF
			
			REQUEST_MODEL(WASHINGTON)
			REQUEST_MODEL(mnCopCarModel)
			REQUEST_MODEL(mnCopPedModel)
			WHILE NOT HAS_MODEL_LOADED(mnCopCarModel)
			OR NOT HAS_MODEL_LOADED(mnCopPedModel)
			OR NOT HAS_MODEL_LOADED(WASHINGTON)
				PRINTLN("waiting")
				WAIT(0)
			ENDWHILE
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED_ON_CREW()
				WAIT(0)
			ENDWHILE
			
			WHILE NOT setupHeistCutscene1()	//Creates tanker...
				PRINTSTRING("waiting setupHeistCutscene1 1")
				WAIT(0)
			ENDWHILE
			
			//skipToEndOfTankerRec()
			CREATE_BANK_PARKED_CARS()
		
			//LOAD_SCENE(<< -114.4576, 6458.6104, 30.4687 >>)
			TRIGGER_MUSIC_EVENT("RH2A_BANK_RESTART")
			
			END_REPLAY_SETUP()
		
			
		BREAK
		
		CASE STAGE_HOLD_OFF_COPS_AT_BANK	//SkipMenuStruct[3].sTxtLabel = "STAGE_HOLD_OFF_COPS_AT_BANK" 
			
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "STAGE_HOLD_OFF_COPS_AT_BANK")
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
			
			//FORCE_SWITCH_TO_MICHAEL()
			FORCE_SWITCH_TO_TREVOR(TRUE)
		
			POSITION_CREW_OUTSIDE_BANK()
			REQUEST_COLLISION_AT_COORD(<< -114.4576, 6458.6104, 30.4687 >>)
			REQUEST_MODEL(WASHINGTON)
			REQUEST_VEHICLE_RECORDING(1, "RBHInitial")
			REQUEST_VEHICLE_RECORDING(2, "RBHInitial")
			REQUEST_VEHICLE_RECORDING(3, "RBHInitial")
			REQUEST_VEHICLE_RECORDING(6, "RBHINitial")
			REQUEST_VEHICLE_RECORDING(7, "RBHINitial")
			
			WHILE NOT setupHeistCutscene1()		
			OR NOT HAS_MODEL_LOADED(WASHINGTON)
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "RBHInitial")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "RBHInitial")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(3, "RBHInitial")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(6, "RBHINitial")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(7, "RBHINitial")
				PRINTLN("waiting")
				WAIT(0)
			ENDWHILE
			CREATE_BANK_PARKED_CARS()
			SET_CREW_IN_BALLISTIC_ARMOUR(bIsReplay)
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED_ON_CREW()
				WAIT(0)
			ENDWHILE
			
			PRINTLN("HOld off cops at bank Skip: models all loaded")
			CREATE_COP_CARS_HOLD_OFF_COPS_AT_BANK()
			WHILE NOT setupHeistCutscene1()	//Creates tanker...
				WAIT(0)
			ENDWHILE
			PRINTLN("HOld off cops at bank Skip: setupHeistCutscene1() all loaded")
			//skipToEndOfTankerRec()
			SETUP_HOLD_OFF_COPS_AT_BANK()
			
			//LOAD_SCENE(<< -114.4576, 6458.6104, 30.4687 >>)
			//WAIT(200)
			POSITION_CREW_OUTSIDE_BANK()
			
			TRIGGER_MUSIC_EVENT("RH2A_BANK_RESTART")
			
			cleanUpBeforeBankStages()
			
			END_REPLAY_SETUP()
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_MINIGUN, TRUE)
			
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_InstantBlendToAim, TRUE)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_AIMING, TRUE)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
			
			SET_PLAYER_SIMULATE_AIMING(PLAYER_ID(), TRUE)
			SET_EQIPPED_WEAPON_START_SPINNING_AT_FULL_SPEED(PLAYER_PED_ID())
			SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(PLAYER_PED_ID())
			
					
		BREAK
		
		CASE STAGE_CHOPPER_TURNS_UP	//SkipMenuStruct[4].sTxtLabel = "STAGE_CHOPPER_TURNS_UP"  
		
			
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -106.8262, 6443.5454, 30.3880 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 208.8560)
			WAIT(1000)
											
			FORCE_SWITCH_TO_TREVOR(TRUE)
			
			REQUEST_MODEL(mnCopCarModel)
			REQUEST_MODEL(mnCopPedModel)
			REQUEST_MODEL(WASHINGTON)
			WHILE NOT HAS_MODEL_LOADED(mnCopCarModel)
			OR NOT HAS_MODEL_LOADED(mnCopPedModel)
			OR NOT HAS_MODEL_LOADED(WASHINGTON)
				WAIT(0)
			ENDWHILE
			WHILE NOT setupHeistCutscene1()	//Creates tanker...
				WAIT(0)
			ENDWHILE
			SETUP_HOLD_OFF_COPS_AT_BANK()
			cleanUpBeforeBankStages()
		
			CREATE_BANK_PARKED_CARS()
			POSITION_CREW_AFTER_CHOPPER_EXPLODES()
					
			END_REPLAY_SETUP()
			
			SET_CREW_IN_BALLISTIC_ARMOUR(bIsReplay)
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED_ON_CREW()
				WAIT(0)
			ENDWHILE
						
			TRIGGER_MUSIC_EVENT("RH2A_HELI_ARRIVE_RESTART")
			
		BREAK
		
		CASE STAGE_ADVANCE_TO_GARDENS	//SkipMenuStruct[5].sTxtLabel = "STAGE_ADVANCE_TO_GARDENS"  
			
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6, "STAGE_ADVANCE_TO_GARDENS")
			DELETE_VEHICLE(arrivalBurrito)
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
				
			
			FORCE_SWITCH_TO_TREVOR(TRUE)
			
			CREATE_PLAYER_PED_ON_FOOT(pedSelector.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vAnimStartPosMichael, 339.2789)
					
			REQUEST_MODEL(WASHINGTON)
			WHILE NOT HAS_MODEL_LOADED(WASHINGTON)
				WAIT(0)
			ENDWHILE
			
			WHILE NOT REQUEST_ADVANCE_TO_GARDEN_ASSETS()
				WAIT(0)
			ENDWHILE
			
			CREATE_BANK_PARKED_CARS()
			SETUP_HOLD_OFF_COPS_AT_BANK()
			cleanUpBeforeBankStages()
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-136.2896, 6443.8965, 30.5362>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(),  131.4219)
			ENDIF
			IF NOT IS_PED_INJURED(pedGunman)
				SET_ENTITY_COORDS(pedGunman, vAnimStartPosGunman - <<0.0, 0.0,  1.0>>)
				SET_ENTITY_HEADING(pedGunman,  317.8662)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGunman, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(PedMichael())	
				SET_ENTITY_COORDS(PedMichael(), vAnimStartPosMichael - <<0.0, 0.0,  1.0>>)
				SET_ENTITY_HEADING(PedMichael(), 339.2789)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PedMichael(), TRUE)
			ENDIF
			
			bHaveSwatVansBeenTriggered = FALSE
			bHaveSwatVansStopped = FALSE
			
			END_REPLAY_SETUP()
			
			SET_CREW_IN_BALLISTIC_ARMOUR(bIsReplay)
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED_ON_CREW()
				WAIT(0)
			ENDWHILE
			
			iMotelExplosion = -1
						
			TRIGGER_MUSIC_EVENT("RH2A_PAUSE_RESTART")
					
		BREAK
				
		CASE STAGE_CONTINUE_DOWN_STREET	//SkipMenuStruct[7].sTxtLabel = "STAGE_CONTINUE_DOWN_STREET" 
		
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7, "STAGE_CONTINUE_DOWN_STREET")
			DELETE_VEHICLE(arrivalBurrito)
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
			FORCE_SWITCH_TO_TREVOR(TRUE)
			CREATE_PLAYER_PED_ON_FOOT(pedSelector.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, (<<-178.71535, 6368.59521, 30.32682>>), 218.3484)
			SET_CREW_IN_BALLISTIC_ARMOUR(bIsReplay)
			
			WHILE NOT REQUEST_STAGE_2_ASSETS()
				WAIT(0)
			ENDWHILE

			CREATE_CONTINUE_DOWN_STREET_STAGE()

			SETUP_STAGE_2()

			setCrewRelationShipGroups()
			setupContinueDownStreet()
			cleanUpBeforeBankStages()
			SETUP_HOLD_OFF_COPS_AT_BANK()

			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-179.8882, 6368.3516, 30.4719>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 224.8545)
			
			IF NOT IS_ENTITY_DEAD(PedMichael())
				SET_ENTITY_COORDS(PedMichael(), << -184.7136, 6364.8701, 30.4872 >>, FALSE)
				SET_ENTITY_HEADING(PedMichael(), 214.1991)
			ENDIF
			IF NOT IS_ENTITY_DEAD(pedGunman)
				SET_ENTITY_COORDS(pedGunman, << -181.5294, 6367.6968, 30.4872 >>,  FALSE)
				SET_ENTITY_HEADING(pedGunman,   208.3195)
			ENDIF
						
			TRIGGER_MUSIC_EVENT("RH2A_PAUSE_RESTART")
			bHaveSwatVansBeenTriggered = FALSE
			
			END_REPLAY_SETUP()		
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED_ON_CREW()
				WAIT(0)
			ENDWHILE
						
		BREAK
		
		CASE STAGE_PROCEED_TO_ALLEY	//SkipMenuStruct[7].sTxtLabel = "STAGE_CONTINUE_DOWN_STREET" 
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(8, "STAGE_PROCEED_TO_ALLEY")
			
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)					
			FORCE_SWITCH_TO_TREVOR(TRUE)
			WHILE NOT REQUEST_STAGE_2_ASSETS()
				WAIT(0)
			ENDWHILE
			CREATE_CONTINUE_DOWN_STREET_STAGE()
			SETUP_STAGE_2()
			SET_CREW_IN_BALLISTIC_ARMOUR(bIsReplay)
		
			
			setupContinueDownStreet()
			cleanUpBeforeBankStages()
			SETUP_HOLD_OFF_COPS_AT_BANK()
			
			CLEAR_AREA(<< -155.9459, 6341.0117, 30.6268 >>, 50.0, TRUE)
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -155.9459, 6341.0117, 30.6268 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 228.0564)
			
			IF NOT IS_PED_INJURED(PedMichael())
				SET_ENTITY_COORDS(PedMichael(), << -153.2694, 6337.7349, 30.6228 >>, FALSE)
				SET_ENTITY_HEADING(PedMichael(), 216.6584)
			ELSE
				SCRIPT_ASSERT("where's Michael?")
			
			ENDIF
			IF NOT IS_ENTITY_DEAD(pedGunman)
				SET_ENTITY_COORDS(pedGunman, << -151.6948, 6333.2271, 30.5735 >>,  FALSE)
				SET_ENTITY_HEADING(pedGunman,  203.8958)
			ENDIF
			
			END_REPLAY_SETUP()
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED_ON_CREW()
				WAIT(0)
			ENDWHILE
			
			TRIGGER_MUSIC_EVENT("RH2A_PAUSE_RESTART")
			
			
			
		BREAK

		CASE STAGE_HOLD_OFF_ARMY_AT_JUNK_YARD	//SkipMenuStruct[9].sTxtLabel = "STAGE_HOLD_OFF_ARMY_AT_JUNK_YARD" 
		
			cleanUpBeforeBankStages()
		
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(9, "STAGE_HOLD_OFF_ARMY_AT_JUNK_YARD")
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
			DELETE_VEHICLE(arrivalBurrito)
			DELETE_PED(pedSelector.pedID[SELECTOR_PED_FRANKLIN])

			FORCE_SWITCH_TO_TREVOR(TRUE)
			//	//Stage yard...
			
			SET_CREW_IN_BALLISTIC_ARMOUR(bIsReplay)
			
			//POSITION_CREW_IN_ALLEY()
			
			TRIGGER_MUSIC_EVENT("RH2A_PAUSE_RESTART")
			
			cleanUpBeforeBankStages()
			
			END_REPLAY_SETUP()
			
			POSITION_GUYS_AFTER_ALLEY_CUT()	
			
			IF bIsBuddyAPro
				TASK_PEDS_AFTER_ALLEY_CUT(PedMichaelWeaponType, pedGunmanWeaponType)
			ENDIF
			
		BREAK
	
		CASE STAGE_HOT_SWAP_TO_BULLDOZER	//SkipMenuStruct[10].sTxtLabel = "STAGE_HOT_SWAP_TO_BULLDOZER" 
			

			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(X, "STAGE_HOT_SWAP_TO_BULLDOZER")
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
			
			//CLEAR_PED_TASKS_IMMEDIATELY(pedSelector.pedID[SELECTOR_PED_TREVOR])
			//CLEAR_PED_TASKS_IMMEDIATELY(pedSelector.pedID[SELECTOR_PED_MICHAEL])
			
			DELETE_VEHICLE(arrivalBurrito)
			
		
			FORCE_SWITCH_TO_TREVOR(TRUE)
			
			DELETE_PED(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(),  << -170.4091, 6289.4009, 30.4906 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 113.2991)
			SET_ENTITY_COORDS(PedMichael(), << -170.9611, 6291.0601, 30.4905 >>, FALSE)
			
			SET_CREW_IN_BALLISTIC_ARMOUR(bIsReplay)
			DELETE_VEHICLE(arrivalBurrito)
			IF NOT bIsBuddyAPro
				DELETE_PED(pedGunman)
			ELSE
				SET_ENTITY_COORDS(pedGunman,  <<-197.4391, 6281.6450, 30.4893>>)
				SET_ENTITY_HEADING(pedGunman, 96.2565)
			ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(BURRITO3)
			
			cleanUpBeforeBankStages()
			REQUEST_MODEL(BARRACKS)
			REQUEST_MODEL(S_M_Y_Marine_03)
			LOAD_ALL_OBJECTS_NOW()
			WHILE NOT HAS_MODEL_LOADED(BARRACKS)
			OR NOT HAS_MODEL_LOADED(S_M_Y_Marine_03)
				WAIT(0)
			ENDWHILE
		
			TRIGGER_MUSIC_EVENT("RH2A_SWITCH_1_RESTART")
			
			END_REPLAY_SETUP()
			
		BREAK
		
		CASE STAGE_SWAPPED_TO_BULLDOZER
			
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(10, "STAGE_SWAPPED_TO_BULLDOZER")
			
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
					
			FORCE_SWITCH_TO_TREVOR(TRUE)

			cleanUpBeforeBankStages()
			
			REQUEST_MODEL(BARRACKS)
			REQUEST_MODEL(S_M_Y_Marine_03)
			REQUEST_MODEL(BULLDOZER)
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
			REQUEST_ANIM_SET("ANIM_GROUP_MOVE_BALLISTIC")
			REQUEST_ANIM_SET("MOVE_STRAFE_BALLISTIC")
			REQUEST_CLIP_SET("move_ballistic_2h")
			
			REQUEST_VEHICLE_RECORDING(1, "Dozer")
			
			LOAD_ALL_OBJECTS_NOW()
			
			WHILE NOT HAS_MODEL_LOADED(BARRACKS)
			OR NOT HAS_MODEL_LOADED(S_M_Y_Marine_03)
			OR NOT HAS_MODEL_LOADED(BULLDOZER)
			OR NOT HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
			OR NOT HAS_ANIM_SET_LOADED("ANIM_GROUP_MOVE_BALLISTIC")
			OR NOT HAS_ANIM_SET_LOADED("move_strafe_ballistic")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Dozer")
				WAIT(0)
			ENDWHILE
					
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_MICHAEL, FALSE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_TREVOR, FALSE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_FRANKLIN, FALSE)
			SET_SELECTOR_PED_HINT(pedSelector, SELECTOR_PED_FRANKLIN, TRUE)

//			CREATE_PLAYER_PED_ON_FOOT(pedSelector.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, (<< -199.6319, 6281.1309, 30.4897 >>), 335.6462)
//			CREATE_PLAYER_PED_ON_FOOT(pedSelector.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, (<< -203.4726, 6279.3066, 30.4903 >>), 266.8397)
			SPAWN_NEW_SET_OF_ARMY_GUYS()
			SET_CREW_IN_BALLISTIC_ARMOUR(bIsReplay)
			
			carBulldozer = CREATE_VEHICLE(BULLDOZER, << 65.3256, 6561.9180, 27.6343 >>, 153.5209)
			CREATE_PLAYER_PED_INSIDE_VEHICLE(pedSelector.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, carBulldozer)
			PRELOAD_OUTFIT(pedSelector.pedID[SELECTOR_PED_FRANKLIN], OUTFIT_P1_STEALTH_NO_MASK)
			SET_PED_COMP_ITEM_CURRENT_SP(pedSelector.pedID[SELECTOR_PED_FRANKLIN], COMP_TYPE_OUTFIT, OUTFIT_P1_STEALTH_NO_MASK, FALSE)
			
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedSelector.pedID[SELECTOR_PED_FRANKLIN], FALSE)
			sCamDetails.pedTo = pedSelector.pedID[SELECTOR_PED_FRANKLIN]
			
			IF NOT IS_ENTITY_DEAD(carBulldozer)
				START_PLAYBACK_RECORDED_VEHICLE(carBulldozer, 1, "Dozer")
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(carBulldozer, 4000)
				SET_PLAYBACK_SPEED(carBulldozer, 0.75)
				SET_VEHICLE_ENGINE_ON(carBulldozer, TRUE, TRUE)
				
				PAUSE_PLAYBACK_RECORDED_VEHICLE(carBulldozer)
				
			ENDIF
			
			IF NOT bIsBuddyAPro
				DELETE_PED(pedGunman)
			ELSE
				SET_ENTITY_COORDS(pedGunman,  <<-197.4391, 6281.6450, 30.4893>>)
				SET_ENTITY_HEADING(pedGunman, 96.2565)
				
				SET_PED_SPHERE_DEFENSIVE_AREA(pedGunman, <<-197.4391, 6281.6450, 30.4893>>, 5.0)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGunman, FALSE)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedGunman, 100.0)
			ENDIF
			
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, pedFranklin(), "FRANKLIN")
									
			UPDATE_SELECTOR_HUD(pedSelector)
			
			WHILE NOT MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_FRANKLIN)
				PRINTLN("for fuck sake")
				WAIT(0)
			ENDWHILE
			WHILE NOT TAKE_CONTROL_OF_SELECTOR_PED(pedSelector)
				PRINTLN("for christ sake")
				WAIT(0)
			ENDWHILE
			TRIGGER_MUSIC_EVENT("RH2A_SWITCH_2_RESTART")
		
			END_REPLAY_SETUP(carBulldozer)
			
		BREAK
		
		CASE STAGE_RESCUE_BUDDYS	//SkipMenuStruct[11].sTxtLabel = "STAGE_RESCUE_BUDDYS" 
			
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(11, "STAGE_RESCUE_BUDDYS")
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)

			//short stage so go back one more
			//iReturnStage--
		
			REQUEST_MODEL(BULLDOZER)
			WHILE NOT HAS_MODEL_LOADED(BULLDOZER)
				WAIT(0)
			ENDWHILE
			carBulldozer = CREATE_VEHICLE(BULLDOZER, << -189.2322, 6284.7402, 30.4903 >>, 233.0396)
			
			REQUEST_MODEL(RHINO)
			REQUEST_MODEL(BARRACKS)
			REQUEST_MODEL(S_M_Y_Marine_03)
			REQUEST_VEHICLE_RECORDING(2, "RBHEnemyTank")
			WHILE  NOT HAS_MODEL_LOADED(RHINO)
			OR NOT HAS_MODEL_LOADED(BARRACKS)
			OR NOT HAS_MODEL_LOADED(S_M_Y_Marine_03)
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "RBHEnemyTank")
				WAIT(0)
			ENDWHILE
			
			DELETE_PED(pedGunman)
			
//			CREATE_PLAYER_PED_ON_FOOT(pedSelector.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, (<< -196.9613, 6284.8184, 30.4902 >>), 218.3484)
//			CREATE_PLAYER_PED_ON_FOOT(pedSelector.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, (<< -196.4857, 6281.4004, 30.4902 >>), 218.3484)
//			
//			WAIT(0)
//			FORCE_SWITCH_TO_MICHAEL()
			FORCE_SWITCH_TO_TREVOR(TRUE)
			WAIT(0)
			SET_CREW_IN_BALLISTIC_ARMOUR(bIsReplay)
			WAIT(0)
			FORCE_SWITCH_TO_FRANKLN()

			IF IS_ENTITY_OK(PLAYER_PED_ID()) AND IS_ENTITY_OK(carBulldozer)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -189.2322, 6284.7402, 33.4903 >>)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), carBulldozer, VS_DRIVER)						
			ENDIF				
			SET_ENTITY_COORDS(PedMichael(),<< -196.9613, 6284.8184, 30.4902 >>, FALSE)
			SET_ENTITY_COORDS(pedTrevor(),<< -196.4857, 6281.4004, 30.4902 >>, FALSE)
			
			IF NOT IS_ENTITY_DEAD(carBulldozer)
				SET_ENTITY_COORDS(carBulldozer, << -189.2322, 6284.7402, 30.4903 >>)
				SET_ENTITY_HEADING(carBulldozer,  233.0396)
			ENDIF
			cleanUpBeforeBankStages()
				
			CREATE_ARMY_PEDS_FOR_PICKUP_CUTSCENE()

			TRIGGER_MUSIC_EVENT("RH2A_RESCUE_RESTART")
			
			END_REPLAY_SETUP()
			
		BREAK
		
		CASE STAGE_CHICKEN_FACTORY	//SkipMenuStruct[12].sTxtLabel = "STAGE_CHICKEN_FACTORY"
			
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				CPRINTLN(DEBUG_MISSION, "MANAGE_SKIP - Chicken Factory: Teleporting Franklin")
//				SET_ENTITY_COORDS(PLAYER_PED_ID(),  << -71.3648, 6267.8774, 30.2180 >>)
//				SET_ENTITY_HEADING(PLAYER_PED_ID(), 184.9243)
//				//REMOVE_PED_FROM_GROUP(PLAYER_PED_ID())
//			ENDIF	
				
			DELETE_PED(pedGunman)
			DELETE_VEHICLE(arrivalBurrito)
			SET_MODEL_AS_NO_LONGER_NEEDED(BURRITO3)
			SET_MODEL_AS_NO_LONGER_NEEDED(HC_GUNMAN)
	
			REQUEST_MODEL(S_F_Y_FACTORY_01)
			REQUEST_MODEL(S_M_Y_MARINE_03)
			REQUEST_MODEL(BULLDOZER)
			REQUEST_MODEL(A_C_HEN)
			REQUEST_MODEL(RHINO)
			REQUEST_WAYPOINT_RECORDING("RBHChkFrank")
			REQUEST_VEHICLE_RECORDING(1, "RBHEnemyTank")

			WHILE NOT HAS_MODEL_LOADED(S_F_Y_FACTORY_01)
			OR NOT HAS_MODEL_LOADED(S_M_Y_MARINE_03)
			OR NOT HAS_MODEL_LOADED(BULLDOZER)
			OR NOT HAS_MODEL_LOADED(A_C_HEN)
			OR NOT HAS_MODEL_LOADED(RHINO)
			OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("RBHChkFrank")
			OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "RBHEnemyTank")
				WAIT(0)
			ENDWHILE					
			
			INIT_SET_PIECE_COP_CAR(EnemyTank, RHINO, 1, "RBHEnemyTank", VS_FRONT_RIGHT, 27500.0, 0.70, FALSE, S_M_Y_MARINE_03)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyTank.thisDriver, TRUE)
			
			SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(EnemyTank.thisCar)
			carBulldozer = CREATE_VEHICLE(BULLDOZER, << -76.0888, 6272.3530, 30.3770 >>, 238.8044)
			
			IF IS_ENTITY_OK(pedFranklin()) AND IS_ENTITY_OK(carBulldozer)
				SET_PED_INTO_VEHICLE(pedFranklin(), carBulldozer, VS_DRIVER)
			ENDIF
			
			FORCE_SWITCH_TO_TREVOR(TRUE)
			
			SET_CREW_IN_BALLISTIC_ARMOUR(bIsReplay)
			//WAIT(0)
			FORCE_SWITCH_TO_FRANKLN()
			
			IF NOT IS_PED_INJURED(PedMichael())
				CPRINTLN(DEBUG_MISSION, "MANAGE_SKIP - Chicken Factory: Teleporting Michael")
				SET_ENTITY_COORDS(PedMichael(),  << -72.9415, 6268.2334, 30.3422 >>)
				SET_ENTITY_HEADING(PedMichael(), 193.2468)
				REMOVE_PED_FROM_GROUP(PedMichael())
			ENDIF
		
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CPRINTLN(DEBUG_MISSION, "MANAGE_SKIP - Chicken Factory: Teleporting Franklin")
				SET_ENTITY_COORDS(PLAYER_PED_ID(),  <<-77.6866, 6269.7437, 30.3750>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 221.9893)
								
				//REMOVE_PED_FROM_GROUP(PLAYER_PED_ID())
			ENDIF				
		
			IF NOT IS_PED_INJURED(pedTrevor())
				CPRINTLN(DEBUG_MISSION, "MANAGE_SKIP - Chicken Factory: Teleporting Trevor")
				SET_ENTITY_COORDS(pedTrevor(),  <<-70.1194, 6268.4014, 30.2188>>)
				SET_ENTITY_HEADING(pedTrevor(), 43.8726)
				REMOVE_PED_FROM_GROUP(pedTrevor())
			ENDIF
			
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(PLAYER_PED_ID(), FALSE)
						
			SET_CLOCK_TIME(20, 00, 0)
						
			switchStateChickenFactory = SWITCH_SETUP
			
			TRIGGER_MUSIC_EVENT("RH2A_CLUCK_ARRIVE_RESTART")
			
			END_REPLAY_SETUP()
			
			RESET_GAME_CAMERA()
			
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
		BREAK
		
		CASE STAGE_TRAIN_PLATFORM
	
	
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(X, "STAGE_TRAIN_PLATFORM")
			DELETE_PED(pedGunman)
			
			FORCE_SWITCH_TO_TREVOR(TRUE)
			
			SET_CREW_IN_BALLISTIC_ARMOUR(bIsReplay)
			FORCE_SWITCH_TO_FRANKLN()
			FORCE_SWITCH_TO_MICHAEL()
			
			REQUEST_MODEL(S_M_Y_Marine_03)
			REQUEST_ANIM_SET("ANIM_GROUP_MOVE_BALLISTIC")
			REQUEST_ANIM_SET("MOVE_STRAFE_BALLISTIC")
			REQUEST_CLIP_SET("move_ballistic_2h")
			
			WHILE NOT HAS_MODEL_LOADED(S_M_Y_Marine_03)
			OR NOT HAS_ANIM_SET_LOADED("ANIM_GROUP_MOVE_BALLISTIC")
			OR NOT HAS_ANIM_SET_LOADED("move_strafe_ballistic")
			OR NOT HAS_ANIM_SET_LOADED("move_ballistic_2h")
				WAIT(0)
			ENDWHILE
			
			chickenFactoryInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(<< -149.8199, 6144.9775, 31.3353 >>, "V_factory4")
			
			PIN_INTERIOR_IN_MEMORY(chickenFactoryInterior)
			
			WHILE NOT IS_INTERIOR_READY(chickenFactoryInterior)
				WAIT(0)
			ENDWHILE
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(),  << -149.8199, 6144.9775, 31.3353 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 74.8513 )
			
			IF NOT IS_PED_INJURED(pedFranklin())
				SET_ENTITY_COORDS(pedFranklin(),  << -148.4596, 6146.8511, 31.3353 >>)
				SET_ENTITY_HEADING(pedFranklin(), 73.3972)
				REMOVE_PED_FROM_GROUP(pedFranklin())
				SETUP_FRANKLIN_FOR_CHICKEN_FACTORY()
			ENDIF				
		
			IF NOT IS_PED_INJURED(pedTrevor())
				SET_ENTITY_COORDS(pedTrevor(),  << -146.0161, 6145.2954, 31.3353 >>)
				SET_ENTITY_HEADING(pedTrevor(), 66.9086)
				REMOVE_PED_FROM_GROUP(pedTrevor())
				SETUP_TREVOR_FOR_CHICKEN_FACTORY()
			ENDIF
			
					
			IF NOT IS_ENTITY_DEAD(pedFranklin())
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedFranklin(), TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFranklin(), TRUE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedFranklin(), FALSE)
				GIVE_WEAPON_TO_PED(pedFranklin(), WEAPONTYPE_PUMPSHOTGUN, INFINITE_AMMO, TRUE)	
				
				SET_PED_RELATIONSHIP_GROUP_HASH(pedFranklin(), robbersGroup)
				SET_PED_SUFFERS_CRITICAL_HITS(pedFranklin(), FALSE)
				SET_PED_CAN_BE_TARGETTED(pedFranklin(), FALSE)
				SET_PED_MAX_HEALTH(pedFranklin(), 1000)
				SET_ENTITY_HEALTH(pedFranklin(), 1000)	
				
				SET_PED_COMBAT_MOVEMENT(pedFranklin(), CM_DEFENSIVE)
			ENDIF
			
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(PLAYER_PED_ID(), FALSE)
			
			SET_CLOCK_TIME(20, 00, 0)
			
			TRIGGER_MUSIC_EVENT("RH2A_CLUCK_ARRIVE_RESTART")
			switchStateChickenFactory = SWITCH_SETUP
			blipFranklin = CREATE_BLIP_FOR_PED_WRAPPER(pedFranklin())
			blipTrevor = CREATE_BLIP_FOR_PED_WRAPPER(pedTrevor())
			
			END_REPLAY_SETUP()
		BREAK
			
		CASE STAGE_END_MOCAP_SCENE
			//initialiseMission2A()			
			FORCE_SWITCH_TO_TREVOR(TRUE)
			//SET_CREW_IN_NORMAL_OUTFITS()
			WAIT(0)
			FORCE_SWITCH_TO_FRANKLN()
			WAIT(0)
			FORCE_SWITCH_TO_MICHAEL()			
			
			END_REPLAY_SETUP()
			
			DELETE_VEHICLE(arrivalBurrito)
			DELETE_PED(pedGunman)
			
			
		BREAK
				
	ENDSWITCH
	
	//Skip only stuff
	IF NOT bIsReplay
		IF to_this_mission_stage >= STAGE_ADVANCE_TO_GARDENS
			SET_MOTEL_STATE(BUILDINGSTATE_DESTROYED)
		ELSE
			SET_MOTEL_STATE(BUILDINGSTATE_NORMAL)
		ENDIF
		
		IF to_this_mission_stage > STAGE_HOLD_OFF_COPS_AT_BANK
			IF g_replay.iReplayInt[REPLAY_VALUE_GAS_STATION_STATE] > 0
				SET_GAS_STATION_STATE(BUILDINGSTATE_DESTROYED)
			ELSE
				SET_GAS_STATION_STATE(BUILDINGSTATE_NORMAL)
			ENDIF
		ELSE
			SET_GAS_STATION_STATE(BUILDINGSTATE_NORMAL)
		ENDIF
	
	ENDIF
	
	mission_stage = INT_TO_ENUM(MISSION_STAGE_FLAG, iReturnStage)

	CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100.00, TRUE)
	RESET_GAME_CAMERA()
	
	IF IS_SCREEN_FADED_OUT()
		LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID())) 
	ENDIF		
	
	//mission_stage = INT_TO_ENUM(MISSION_STAGE_FLAG, iReturnStage)
	
	IF mission_stage = STAGE_CHICKEN_FACTORY
	OR mission_stage = STAGE_TRAIN_PLATFORM
		IF chickenFactoryInterior <> NULL
			UNPIN_INTERIOR(chickenFactoryInterior)
		ENDIF
	ENDIF
	
	
	WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED_ON_CREW()
		WAIT(0)
	ENDWHILE
	
	IF DOES_ENTITY_EXIST(pedFranklin())
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, pedFranklin(), "FRANKLIN")
	ENDIF
	
	IF IS_PLAYER_TREVOR()
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PedMichael(), "MICHAEL")
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, PLAYER_PED_ID(), "TREVOR")	
	ELIF IS_PLAYER_MICHAEL()
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedTrevor(), "TREVOR")	
	ELIF IS_PLAYER_FRANKLIN()
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PedMichael(), "MICHAEL")
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedTrevor(), "TREVOR")
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, pedFranklin(), "FRANKLIN")
	ENDIF
		
//	//Needs to be done after area is loaded.
//	IF mission_stage = STAGE_CONTINUE_DOWN_STREET	
//		//Blow out fence...
//		WAIT(500)
//		ADD_EXPLOSION( <<-184.53, 6371.0, 31.87>>, EXP_TAG_GRENADE, 0.3, FALSE, TRUE, 0.0)
//	ENDIF
				
	PRINTSTRING("SKIPPED! : ") PRINTINT(iReturnStage)PRINTNL()
	
//	IF IS_SCREEN_FADED_OUT()
//	OR IS_SCREEN_FADING_OUT()
//		DO_SCREEN_FADE_IN(500)
//	ENDIF

ENDPROC


PROC SETUP_MISSION_2A()

	DISABLE_TAXI_HAILING(TRUE)
	
	#IF IS_DEBUG_BUILD
		strMissionName = "Paleto Score 2A"
	#ENDIF
	
	SET_AUDIO_FLAG("ScriptedConvListenerMaySpeak", TRUE) 
			
	RESET_GAME_CAMERA()
	
	IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_TREVOR 
		CREATE_PLAYER_PED_INSIDE_VEHICLE(pedSelector.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, arrivalBurrito,  VS_BACK_LEFT)
		IF NOT IS_REPLAY_IN_PROGRESS()		
			STORE_PLAYER_PED_VARIATIONS(pedSelector.pedID[SELECTOR_PED_TREVOR], TRUE)
		ENDIF
		SETUP_CREW_VARIABLES(pedTrevor())
		SET_PED_COMP_ITEM_CURRENT_SP(pedTrevor(), COMP_TYPE_OUTFIT, OUTFIT_P2_STYLESUIT_5 , FALSE)
		SET_PED_NAME_DEBUG(pedTrevor(), "Trevor")
	ENDIF
		
	IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
		CREATE_PLAYER_PED_INSIDE_VEHICLE(pedSelector.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, arrivalBurrito,  VS_FRONT_RIGHT)
		IF NOT IS_REPLAY_IN_PROGRESS()	
			STORE_PLAYER_PED_VARIATIONS(pedSelector.pedID[SELECTOR_PED_FRANKLIN], TRUE)
		ENDIF
		//RESTORE_PLAYER_PED_VARIATIONS(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
		SETUP_CREW_VARIABLES(pedFranklin())	
		SET_PED_NAME_DEBUG(pedFranklin(), "Franklin")
		PRELOAD_OUTFIT(pedSelector.pedID[SELECTOR_PED_FRANKLIN], OUTFIT_P1_STEALTH_NO_MASK)
		SET_PED_COMP_ITEM_CURRENT_SP(pedSelector.pedID[SELECTOR_PED_FRANKLIN], COMP_TYPE_OUTFIT, OUTFIT_P1_STEALTH_NO_MASK, FALSE)
	ENDIF

	
	
	IF NOT IS_REPLAY_IN_PROGRESS()		
		STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
	ENDIF
	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_JEWEL_HEIST , FALSE)
	
	//PedMichael() = PLAYER_PED_ID()
	ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
	ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedTrevor(), "TREVOR")
	ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, pedFranklin(), "FRANKLIN")
	
	bHasChangedClothesMichael = GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)  //ensure bChange is set to the state of “player has changed outfit” at the start of the mission
	bHasChangedClothesTrevor = GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR)  //ensure bChange is set to the state of “player has changed outfit” at the start of the mission
	
	SETTIMERA(0)
	
	SET_BUILDING_STATE(BUILDINGNAME_IPL_PALETO_CHICKEN_FACTORY_INTERIOR_1, BUILDINGSTATE_DESTROYED)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_PALETO_CHICKEN_FACTORY_INTERIOR_2, BUILDINGSTATE_DESTROYED)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_PALETO_CHICKEN_FACTORY_INTERIOR_3, BUILDINGSTATE_DESTROYED)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_PALETO_CHICKEN_FACTORY_INTERIOR_4, BUILDINGSTATE_DESTROYED)
	
	RESET_GAME_CAMERA()

ENDPROC


FUNC MISSION_STAGE_FLAG DO_SHIT_SKIP(MISSION_STAGE_FLAG &replayStageToSkip)

	IF g_bShitskipAccepted = TRUE
	
		SWITCH replayStageToSkip
			CASE STAGE_INITIALISE
				RETURN STAGE_HOLD_OFF_COPS_AT_BANK		
			BREAK
		
			CASE STAGE_DEAL_WITH_FRANKLIN
				RETURN STAGE_GET_TO_BANK
			BREAK
			
			CASE STAGE_GET_TO_BANK		
				RETURN STAGE_ROB_THE_BANK
			BREAK
			
			CASE STAGE_ROB_THE_BANK
				RETURN STAGE_HEIST_CUTSCENE
			BREAK
			
			CASE STAGE_HOLD_OFF_COPS_AT_BANK
				RETURN STAGE_ADVANCE_TO_GARDENS
			BREAK
			
			CASE STAGE_ADVANCE_TO_GARDENS
				RETURN STAGE_CONTINUE_DOWN_STREET
			BREAK
	
			CASE STAGE_CONTINUE_DOWN_STREET
				RETURN STAGE_PROCEED_TO_ALLEY
			BREAK
	
			CASE STAGE_HOLD_OFF_ARMY_AT_JUNK_YARD
			CASE STAGE_HOT_SWAP_TO_BULLDOZER					
				RETURN STAGE_SWAPPED_TO_BULLDOZER
			BREAK
	
			CASE STAGE_SWAPPED_TO_BULLDOZER			
				RETURN STAGE_RESCUE_BUDDYS
			BREAK
			
			CASE STAGE_RESCUE_BUDDYS			
				RETURN STAGE_CHICKEN_FACTORY
			BREAK
			
			CASE STAGE_CHICKEN_FACTORY
				RETURN STAGE_END_MOCAP_SCENE
			BREAK
			
//			CASE STAGE_TRAIN_PLATFORM
//				RETURN STAGE_END_MOCAP_SCENE
//			BREAK
				
//			CASE STAGE_ESCAPE_IN_TANK_LOST_WANTED_LEVEL
//			
//			BREAK
				
		ENDSWITCH
	
	ENDIF
	
	RETURN replayStageToSkip

ENDFUNC


PROC initialiseMission2A()

	//Safeguard for: Bug Change: 1596586 
	IF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_RURAL_BANK,0) = CM_EMPTY
		g_savedGlobals.sHeistData.eSelectedCrew[HEIST_CHOICE_RURAL_NO_TANK][0] = CM_GUNMAN_G_GUSTAV
		SCRIPT_ASSERT("Crew member selection went wrong, defaulting to Gustavo. Speak to Ben R")
	ENDIF

	INITIALISE_MISSION_COMMON()
	SETUP_MISSION_2A()
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	
	mission_stage = STAGE_INTRO_MOCAP
		
	IF (Is_Replay_In_Progress())
		PRINTLN("PALETO SCORE 2A: INT_TO_ENUM(MISSION_STAGE_FLAG, Get_Replay_Mid_Mission_Stage():", INT_TO_ENUM(MISSION_STAGE_FLAG, Get_Replay_Mid_Mission_Stage()))
		//SCRIPT_ASSERT("replay!")
		
		iCurrentTake = g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE]	
		iDisplayedTake = iCurrentTake
		//One a replay assume we are Michael before switching to other stages, keeps things consistent.
		//SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
				
		skip_mission_stage = GET_GAMEPLAY_STAGE_BY_NUMBER(Get_Replay_Mid_Mission_Stage())
		
		skip_mission_stage = DO_SHIT_SKIP(skip_mission_stage)
	
		MANAGE_SKIP(skip_mission_stage, TRUE)
	ELSE
		g_iPaletoScoreTake = 0			// Reset take stored for replays.
	ENDIF
		  
		
ENDPROC



BOOL bFranklinEntityStolen
PROC stageIntroMocap()

	HIDE_HUD_AND_RADAR_THIS_FRAME()

	SWITCH iIntroMocapStage

		CASE 0			
			
			REQUEST_CUTSCENE("RBH_2A_INT")
			bFranklinEntityStolen = FALSE
			REQUEST_VEHICLE_ASSET(BURRITO3, 2)
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
				DELETE_PED(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
			ENDIF
			iIntroMocapStage++
		FALLTHRU
	 
		CASE 1
		
			//IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				CPRINTLN(DEBUG_MISSION, "RBH_2A_INT - Setting ped varation in stageIntroMocap")
				IF NOT IS_ENTITY_DEAD(pedGunman)
					IF NOT bIsBuddyAPro
						SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("gunman_selection_1", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_RURAL_BANK, 0), CREW_OUTFIT_SUIT)
					ELSE
						SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("gunman_selection_2", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_RURAL_BANK, 0), CREW_OUTFIT_SUIT)
					ENDIF
				ENDIF
			
				SET_CUTSCENE_PED_OUTFIT("Franklin", PLAYER_ONE, OUTFIT_P1_STEALTH_NO_MASK)				
				SET_CUTSCENE_PED_OUTFIT("Michael", PLAYER_ZERO, OUTFIT_P0_JEWEL_HEIST)
				SET_CUTSCENE_PED_OUTFIT("Trevor", PLAYER_TWO, OUTFIT_P2_STYLESUIT_5)
				
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,0), 0, 0) //(head)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,2), 0, 0) //(hair)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,3), 0, 1) //(uppr)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,4), 1, 1) //(lowr)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,5), 0, 0) //(hand)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,7), 0, 0) //(teef)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,8), 1, 0) //(accs)
				SET_CUTSCENE_PED_PROP_VARIATION("Lester", INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
				SET_CUTSCENE_PED_PROP_VARIATION("Lester", INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
				
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID(), PLAYER_ZERO)
					CPRINTLN(DEBUG_MISSION, "Setting Michaels cutscene variations for Intro")
				ENDIF
				
				CPRINTLN(DEBUG_MISSION, "timeLapseCutscene - iTimelapsePreloadCutsceneState = 3, we're done")
				iTimelapsePreloadCutsceneState = 3
			//ENDIF
		
			IF HAS_VEHICLE_ASSET_LOADED(BURRITO3)
						
				IF HAS_CUTSCENE_LOADED()
					iIntroMocapStage++				 
				ENDIF
			ENDIF
			
		BREAK
	
		CASE 2
			//CLEAR_AREA(<<-826.457886,180.473969,71.133858>>, 100.00, TRUE)
		
			
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
				CREATE_PLAYER_PED_ON_FOOT(pedSelector.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<0.0, 0.0, 1.0>>)
				MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(pedSelector)
				SET_PED_COMP_ITEM_CURRENT_SP(pedSelector.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_SHIRT_SHORTS_1 , FALSE)
			ENDIF
			
			SET_SRL_POST_CUTSCENE_CAMERA(<<1381.6334, 3625.0225, 35.7988>>, NORMALISE_VECTOR(<<1381.0353, 3624.5269, 35.7581>> - <<1381.6334, 3625.0225, 35.7988>>))
		
	
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Michael", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PLAYER_ZERO)
			ENDIF
			
			IF NOT  IS_PED_INJURED(pedSelector.pedID[SELECTOR_PED_TREVOR])
				CLEAR_PED_TASKS_IMMEDIATELY(pedSelector.pedID[SELECTOR_PED_TREVOR])
				REGISTER_ENTITY_FOR_CUTSCENE(pedSelector.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
			ENDIF
			
			
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Franklin", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PLAYER_ONE)
			
						
			IF NOT bIsBuddyAPro
				IF NOT IS_ENTITY_DEAD(pedGunman)
					REGISTER_ENTITY_FOR_CUTSCENE(pedGunman,"gunman_selection_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, HC_GUNMAN)
				ENDIF
				REGISTER_ENTITY_FOR_CUTSCENE(NULL,"gunman_selection_2", CU_DONT_ANIMATE_ENTITY, HC_GUNMAN)
				REGISTER_ENTITY_FOR_CUTSCENE(NULL,"P_Novel_01_S", CU_DONT_ANIMATE_ENTITY, P_Novel_01_S)
				REGISTER_ENTITY_FOR_CUTSCENE(NULL,"gunman_chair^2", CU_DONT_ANIMATE_ENTITY, PROP_TABLE_03_CHR)
			ELSE
				IF NOT IS_ENTITY_DEAD(pedGunman)
					REGISTER_ENTITY_FOR_CUTSCENE(pedGunman,"gunman_selection_2", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, HC_GUNMAN)
				ENDIF
				REGISTER_ENTITY_FOR_CUTSCENE(NULL,"gunman_selection_1", CU_DONT_ANIMATE_ENTITY, HC_GUNMAN)
				
				REGISTER_ENTITY_FOR_CUTSCENE(NULL,"gunman_chair^1", CU_DONT_ANIMATE_ENTITY, PROP_TABLE_03_CHR)
				
			ENDIF
						
					
			PRINTLN("boom", bh2Timelapse.iTimelapseCut)
			
			IF bh2Timelapse.iTimelapseCut = 0
				SET_CUTSCENE_FADE_VALUES(FALSE, TRUE, FALSE, FALSE)
			ENDIF
			
			START_CUTSCENE()
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
			CREATE_MODEL_HIDE(<<1395.71, 3607.15, 37.94>>, 1.0, PROP_TABLE_03_CHR, FALSE)
						
			//NEW_LOAD_SCENE_START_SPHERE(<<1375.2996, 3619.8562, 33.8919>>, 20.0)
			
			iIntroMocapStage++
		BREAK
	
		CASE 3		
	
			IF IS_CUTSCENE_PLAYING()
				SET_TODS_CUTSCENE_RUNNING(bh2Timelapse, FALSE)
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)
				IF NOT IS_PED_INJURED(pedSelector.pedID[SELECTOR_PED_TREVOR])
					CLEAR_PED_WETNESS(pedSelector.pedID[SELECTOR_PED_TREVOR])
					CLEAR_PED_BLOOD_DAMAGE(pedSelector.pedID[SELECTOR_PED_TREVOR])
					RESET_PED_VISIBLE_DAMAGE(pedSelector.pedID[SELECTOR_PED_TREVOR])
				ENDIF
				IF NOT IS_PED_INJURED(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					CLEAR_PED_WETNESS(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					CLEAR_PED_BLOOD_DAMAGE(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					RESET_PED_VISIBLE_DAMAGE(pedSelector.pedID[SELECTOR_PED_MICHAEL])
				ENDIF
				IF NOT IS_PED_INJURED(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
					CLEAR_PED_WETNESS(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
					CLEAR_PED_BLOOD_DAMAGE(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
					RESET_PED_VISIBLE_DAMAGE(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
				ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					CLEAR_PED_WETNESS(PLAYER_PED_ID())
					CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
					RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
				ENDIF
				CLEAR_AREA(<< 1374.4703, 3618.6980, 33.8921 >>, 200.00, TRUE, TRUE)
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
						
				iIntroMocapStage++
			ENDIF
		BREAK
	
		CASE 4
			
			SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
				pedSelector.pedID[SELECTOR_PED_MICHAEL] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael", PLAYER_ZERO))
			ENDIF
			
			IF NOT bFranklinEntityStolen
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklin", PLAYER_ONE))
					pedSelector.pedID[SELECTOR_PED_FRANKLIN] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklin", PLAYER_ONE))
					bFranklinEntityStolen = TRUE
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				RESET_GAME_CAMERA()
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				IF IS_ENTITY_OK(pedSelector.pedID[SELECTOR_PED_TREVOR]) 
					CPRINTLN(DEBUG_MISSION, "Trevor's Exit state.  He exists")
					CLEAR_PED_TASKS_IMMEDIATELY(pedSelector.pedID[SELECTOR_PED_TREVOR])
					SET_ENTITY_COORDS(pedSelector.pedID[SELECTOR_PED_TREVOR], (<<1371.64160, 3621.69702, 33.89125>>))
					SET_ENTITY_HEADING(pedSelector.pedID[SELECTOR_PED_TREVOR], 196.79)
					IF IS_ENTITY_OK(arrivalBurrito)
						TASK_ENTER_VEHICLE(pedSelector.pedID[SELECTOR_PED_TREVOR], arrivalBurrito,DEFAULT_TIME_BEFORE_WARP, VS_BACK_LEFT, PEDMOVE_WALK, ECF_RESUME_IF_INTERRUPTED | ECF_WARP_ENTRY_POINT)
						CPRINTLN(DEBUG_MISSION, "Trevor's Exit state.  Burrito exists")
					ELSE
						CPRINTLN(DEBUG_MISSION, "Trevor's Exit state.  Burrito doesn't exist.")
					ENDIF
				ELSE
					CPRINTLN(DEBUG_MISSION, "Trevor's Exit state.  Either he doesn't exist.")
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael") //HAS_CUTSCENE_FINISHED()

				IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
					MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_MICHAEL)
					TAKE_CONTROL_OF_SELECTOR_PED(pedSelector)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(arrivalBurrito)
					CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())//<<1378.2544, 3621.7080, 33.8918>>, 129.6167
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 129.6167)//130.5060)
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1378.2544, 3621.7080, 33.8918>>)//<<1378.8617, 3623.0049, 33.8917>>)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000)
					IF GET_FOLLOW_PED_CAM_VIEW_MODE() <> CAM_VIEW_MODE_FIRST_PERSON
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					ELSE
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_AIMING, FALSE, FAUS_CUTSCENE_EXIT)
					ENDIF
				ENDIF
				
				IF IS_SCREEN_FADED_OUT()
					LOAD_SCENE(<<1378.8617, 3623.0049, 33.8917>>)
				ENDIF
				
				IF IS_ENTITY_OK(pedGunman) AND IS_ENTITY_OK(arrivalBurrito)
					IF NOT (pedGunman = GET_PED_IN_VEHICLE_SEAT(arrivalBurrito, VS_BACK_RIGHT))
						SET_PED_INTO_VEHICLE(pedGunman, arrivalBurrito, VS_BACK_RIGHT)
					ENDIF
				ENDIF
				
				IF IS_ENTITY_OK(pedSelector.pedID[SELECTOR_PED_FRANKLIN]) AND IS_ENTITY_OK(arrivalBurrito)
					SET_PED_INTO_VEHICLE(pedSelector.pedID[SELECTOR_PED_FRANKLIN], arrivalBurrito, VS_FRONT_RIGHT)
				ENDIF
				
				DISPLAY_RADAR(TRUE)
				
				REPLAY_STOP_EVENT()
						
		 		IF NOT bMIssionTriggeredAsMichael
					DO_SWITCH_EFFECT(CHAR_MICHAEL)
				ENDIF
				
				PIN_HEIST_BOARD_IN_MEMORY(HEIST_RURAL_BANK, FALSE)
				
				REMOVE_MODEL_HIDE(<<1395.71, 3607.15, 37.94>>, 1.0, PROP_TABLE_03_CHR, FALSE)
				SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
				mission_stage = STAGE_DEAL_WITH_FRANKLIN
			ENDIF
				
		BREAK
					
	ENDSWITCH

ENDPROC


PROC doBuddyFailCheckStageDozerTankStage()

	IF DOES_ENTITY_EXIST(pedTrevor())
		IF IS_PED_INJURED(pedTrevor())
			//PRINT_NOW("CMN_TDIED", DEFAULT_GOD_TEXT_TIME, 1) //
			MISSION_FLOW_SET_FAIL_REASON("CMN_TDIED")
			Mission_Failed()
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(PedMichael())
		IF IS_PED_INJURED(PedMichael())
			//PRINT_NOW("CMN_FDIED", DEFAULT_GOD_TEXT_TIME, 1) //
			MISSION_FLOW_SET_FAIL_REASON("CMN_MDIED")
			Mission_Failed()
		ENDIF
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), carBulldozer) > 75.00
		MISSION_FLOW_SET_FAIL_REASON("DOZERLOST")
		Mission_Failed()
	ENDIF
	
	IF iBullDozerStage >= 3
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<67.973251,6604.539551,30.164473>>, <<-277.692322,6256.652832,69.740997>>, 131.000000)
			MISSION_FLOW_SET_FAIL_REASON("LOSTMT")
			Mission_Failed()
		ENDIF
	ENDIF

ENDPROC



PROC manageTyrePopping()

	INT i

	//FOR i = 0 TO NUMBER_OF_COP_CARS_OUTSIDE_BANK -1
	
	IF iTyrePopIndex > NUMBER_OF_COP_CARS_OUTSIDE_BANK - 1
		iTyrePopIndex = 0
	ENDIF
	
	i = iTyrePopIndex
		IF DOES_ENTITY_EXIST(CopCarsParked[i])
		AND (GET_ENTITY_MODEL(CopCarsParked[i]) <> RIOT)
			IF NOT IS_ENTITY_DEAD(CopCarsParked[i])
				IF GET_ENTITY_HEALTH(CopCarsParked[i]) < 900
					SET_VEHICLE_TYRE_BURST(CopCarsParked[i], SC_WHEEL_CAR_FRONT_LEFT)
					SMASH_VEHICLE_WINDOW(CopCarsParked[i], SC_WINDOW_FRONT_LEFT)
				ENDIF
				
				IF GET_ENTITY_HEALTH(CopCarsParked[i]) < 800
					SET_VEHICLE_TYRE_BURST(CopCarsParked[i], SC_WHEEL_CAR_FRONT_RIGHT)
					SMASH_VEHICLE_WINDOW(CopCarsParked[i], SC_WINDOW_FRONT_RIGHT)
				ENDIF
				
				IF GET_ENTITY_HEALTH(CopCarsParked[i]) < 700
					SET_VEHICLE_TYRE_BURST(CopCarsParked[i], SC_WHEEL_CAR_REAR_LEFT)
					SMASH_VEHICLE_WINDOW(CopCarsParked[i], SC_WINDOW_REAR_LEFT)
				ENDIF
				
				IF GET_ENTITY_HEALTH(CopCarsParked[i]) < 500
					SET_VEHICLE_TYRE_BURST(CopCarsParked[i], SC_WHEEL_CAR_REAR_RIGHT)
					SMASH_VEHICLE_WINDOW(CopCarsParked[i], SC_WINDOW_REAR_RIGHT)
				ENDIF
				
					//If damage changed by 100 then knock a door off
				IF (CopCarsParkedPrevHealth[i] - GET_ENTITY_HEALTH(CopCarsParked[i]))  > 250
					INT iRandomDoor = GET_RANDOM_INT_IN_RANGE(0, 5)
					IF NOT IS_VEHICLE_DOOR_DAMAGED(CopCarsParked[i], INT_TO_ENUM(SC_DOOR_LIST, iRandomDoor))
						SET_VEHICLE_DOOR_BROKEN(CopCarsParked[i], INT_TO_ENUM(SC_DOOR_LIST, iRandomDoor), FALSE)
					ENDIF
				ENDIF
				
				CopCarsParkedPrevHealth[i] = GET_ENTITY_HEALTH(CopCarsParked[i])
			ENDIF
		ENDIF
	
	//ENDFOR
	iTyrePopIndex++

ENDPROC



BOOL bMusicStarted = FALSE

PROC stageGetTobank()

	doBuddyFailCheck(2.0)
	
	IF NOT IS_VEHICLE_DRIVEABLE(arrivalBurrito)
	OR IS_VEHICLE_PERMANENTLY_STUCK(arrivalBurrito)
		MISSION_FLOW_SET_FAIL_REASON("VANDIED")
		Mission_Failed()
	ENDIF
	

	IF GET_DISTANCE_BETWEEN_COORDS(vBankCoordinates, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 60.0
		IF IS_PED_SHOOTING(PLAYER_PED_ID())
			MISSION_FLOW_SET_FAIL_REASON("BLEWHEIST")
			Mission_Failed()	
		ENDIF
	ENDIF	
	
	WEAPON_TYPE currentWeapon
		
	IF IS_PED_ON_FOOT(PLAYER_PED_ID())
		IF GET_DISTANCE_BETWEEN_COORDS(<< -116.4650, 6457.3716, 30.4554 >>, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 20.0
			GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), currentWeapon)
			IF currentWeapon <> WEAPONTYPE_UNARMED
				MISSION_FLOW_SET_FAIL_REASON("BLEWHEIST")
				Mission_Failed()	
			ENDIF
		ENDIF
		
	ENDIF
	
	IF DOES_ENTITY_EXIST(franklinsBoat)
		IF IS_ENTITY_DEAD(franklinsBoat)
			MISSION_FLOW_SET_FAIL_REASON("GETAWAYDEST")
			Mission_Failed()
		ENDIF
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_COORDS(<< -116.4650, 6457.3716, 30.4554 >>, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 40.0
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			MISSION_FLOW_SET_FAIL_REASON("BLEWHEIST")
			Mission_Failed()	
		ENDIF
	ENDIF
	SWITCH iGetToBankStage
	
		CASE 0
		CASE 100	//Repeat if streamed out.
			
			IF GET_DISTANCE_BETWEEN_COORDS(<< -116.4650, 6457.3716, 30.4554 >>, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 160.0
						
				IF NOT IS_AUDIO_SCENE_ACTIVE("PS_2A_DRIVE_TO_PALETO")
					START_AUDIO_SCENE("PS_2A_DRIVE_TO_PALETO")
				ENDIF
	//			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<vBankCoordinates.x - 20.0, vBankCoordinates.y - 20.0, vBankCoordinates.z - 20.0>>, <<vBankCoordinates.x + 20.0, vBankCoordinates.y + 20.0, vBankCoordinates.z + 20.0>>, FALSE)		
				//playersGroup = GET_PLAYER_GROUP(PLAYER_ID())
				SET_PED_NON_CREATION_AREA(<<-146.351227,6464.542480,37.144920>>, <<-75.653709,6460.934082,29.918730>>)
				SET_CREATE_RANDOM_COPS(FALSE)
				
				REQUEST_MODEL(mnCopPedModel)
				REQUEST_MODEL(mnCopCarModel)
				REQUEST_MODEL(WASHINGTON)
							
				CLEAR_AREA(vBankCoordinates, 30.0, TRUE)
				CLEAR_AREA_OF_VEHICLES(vBankCoordinates, 30.0)	
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "STAGE_GET_TO_BANK")
				g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = iCurrentTake
				g_iPaletoScoreTake = iCurrentTake	//UPdate the saved take
				
				SET_VEHICLE_DOOR_SHUT(arrivalBurrito, SC_DOOR_REAR_LEFT, FALSE)
				SET_VEHICLE_DOOR_SHUT(arrivalBurrito, SC_DOOR_REAR_RIGHT, FALSE)
				
				bMusicStarted = FALSE
				FADE_IN_IF_NEEDED()
				iGetToBankStage++
			ENDIF
		BREAK

		CASE 1
		CASE 101
			IF HAS_MODEL_LOADED(WASHINGTON)
				CREATE_BANK_PARKED_CARS()
			//IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_LETSGO", CONV_PRIORITY_VERY_HIGH)
				iGetToBankStage++	
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				//PRINT_NOW("GETTO", DEFAULT_GOD_TEXT_TIME, 1)
				//SET_BIT(sLocatesData.iLocatesBitSet, BS_INITIAL_GOD_TEXT_PRINTED)
				iGetToBankStage++
			ENDIF			
		BREAK
		
		CASE 3
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_BANT2", CONV_PRIORITY_VERY_HIGH)
					iGetToBankStage++
				ENDIF
			ENDIF							
		BREAK
		
		CASE 4
		CASE 102
			//If you go too far from the entrance...
			IF GET_DISTANCE_BETWEEN_COORDS(<< -116.4650, 6457.3716, 30.4554 >>, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 170.0
				
				SET_MODEL_AS_NO_LONGER_NEEDED(mnCopPedModel)
				SET_MODEL_AS_NO_LONGER_NEEDED(mnCopCarModel)
				SET_MODEL_AS_NO_LONGER_NEEDED(WASHINGTON)
							
				DELETE_VEHICLE(vehBankParkedCars[0])
				iGetToBankStage = 100
			ENDIF
		BREAK
		
			
	ENDSWITCH	

	
	PROCESS_BALACLAVAS()
	
			
	IF bMusicStarted = FALSE
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vBankCoordinates, <<30.0, 30.0, 30.0>>)
			TRIGGER_MUSIC_EVENT("RH2A_RADIO_ARRIVAL")
			bMusicStarted = TRUE
		ENDIF
	ENDIF
			
	
	IF NOT IS_ENTITY_DEAD(arrivalBurrito)
	AND iGetToBankStage > 3
		IF NOT DOES_BLIP_EXIST(sLocatesData.LocationBlip)
			IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
				PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
			ENDIF
		ELSE
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	IF NOT IS_ENTITY_DEAD(arrivalBurrito)
		//IF (IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData, vBankParkingLocation, <<25.0, 25.0, 6.0>>, FALSE, pedTrevor(), pedGunman, NULL, arrivalBurrito , "GETTO", "LEFTREVOR", "", "", "", "GETINVAN", "GETBACKIN", FALSE, TRUE)
		IF (IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData, vBankParkingLocation - <<0.0, 0.0, 1.8>>, <<0.001, 0.001, 0.001>>, TRUE, pedTrevor(), pedGunman, NULL, arrivalBurrito , "PARKVAN", "LEFTREVOR", "", "", "", "GETINVAN", "GETBACKIN", FALSE, TRUE)
		OR (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-148.955246,6481.374023,28.444906>>, <<-107.479897,6439.190918,36.280029>>, 35.500000)
			AND DOES_BLIP_EXIST(sLocatesData.LocationBlip)))
		AND CAN_PLAYER_START_CUTSCENE()	
			IF NOT IS_ENTITY_DEAD(PedMichael())
				REMOVE_PED_FROM_GROUP(PedMichael())
				REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipMichael)
			ENDIF
			IF NOT IS_ENTITY_DEAD(pedGunman)
				REMOVE_PED_FROM_GROUP(pedGunman)
				REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipBuddy)
				GIVE_WEAPON_TO_PED(pedGunman, pedGunmanWeaponType, INFINITE_AMMO, TRUE, TRUE)
			ENDIF
			IF IS_ENTITY_OK(pedTrevor())
				GIVE_WEAPON_TO_PED(pedTrevor(), WEAPONTYPE_SAWNOFFSHOTGUN, 150, TRUE, TRUE)
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0)
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
			
			//CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
			mission_stage = STAGE_ROB_THE_BANK
		ENDIF
	ENDIF
	
ENDPROC


PROC requestAssetsStageGetToBank()

	REQUEST_MODEL(mnCopPedModel )		
	REQUEST_MODEL(mnCopCarModel)
	
	IF NOT IS_ENTITY_DEAD(PedMichael())
		REMOVE_PED_FROM_GROUP(PedMichael())
		REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipMichael)
	ENDIF
	IF NOT IS_ENTITY_DEAD(pedGunman)
		REMOVE_PED_FROM_GROUP(pedGunman)
		REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipBuddy)
	ENDIF
	
//	#IF IS_DEBUG_BUILD
//		IF bTriggeredByDebugDoForceLoad = TRUE
//			LOAD_ALL_OBJECTS_NOW()
//			bTriggeredByDebugDoForceLoad = FALSE
//		ENDIF
//	#ENDIF
	
ENDPROC


//////////////////////    
/// MISSION STAGES ///
//////////////////////    
///    
INT iTrevorsAmmoCount
INT iFranklinsAmmoCount

PROC stageDealWithFranklin()

	doBuddyFailCheck()
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS ( <<1377.96, 3611.06, 35.62>>, 2.0, PROP_ARM_GATE_L)
		IF NOT bClearGateArea1
			CLEAR_AREA(<<1377.96, 3611.06, 35.62>>, 2.0, TRUE)
			bClearGateArea1 = TRUE
		ENDIF
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_ARM_GATE_L, <<1377.96, 3611.06, 35.62>>, TRUE, -1.0)
	ENDIF
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS ( <<1372.36, 3609.02, 35.62>>, 2.0, PROP_ARM_GATE_L)
		IF NOT bClearGateArea2
			CLEAR_AREA(<<1372.36, 3609.02, 35.62>>, 2.0, TRUE)
			bClearGateArea2 = FALSE
		ENDIF
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_ARM_GATE_L, <<1372.36, 3609.02, 35.62>>, TRUE, 1.0)
	ENDIF

	//PRINTLN(HASH("FIRING_PATTERN_SHORT_BURSTS"))

//	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	#IF IS_DEBUG_BUILD
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_I)
			SCRIPT_ASSERT("boom")
			IF NOT IS_ENTITY_DEAD(PedMichael())
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PedMichael(), "BUDDY_SEES_TREVOR_DEATH", "MICHAEL_NORMAL")
			ENDIF
			IF NOT IS_ENTITY_DEAD(pedTrevor())
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedTrevor(), "BUDDY_SEES_MICHAEL_DEATH", "TREVOR_NORMAL")
			ENDIF	
			IF NOT IS_ENTITY_DEAD(pedFranklin())
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFranklin(), "BUDDY_SEES_MICHAEL_DEATH", "FRANKLIN_NORMAL")
			ENDIF	
		ENDIF
	
	#ENDIF
	
	IF NOT IS_VEHICLE_DRIVEABLE(arrivalBurrito)
	OR IS_VEHICLE_PERMANENTLY_STUCK(arrivalBurrito)  //IS_VEHICLE_STUCK_ON_ROOF(arrivalBurrito)
		//PRINT_NOW("VANDIED", DEFAULT_GOD_TEXT_TIME, 1) //
		MISSION_FLOW_SET_FAIL_REASON("VANDIED")
		Mission_Failed()
	ENDIF


	GET_AMMO_IN_CLIP(pedTrevor(), WEAPONTYPE_PISTOL, iTrevorsAmmoCount)
	IF iTrevorsAmmoCount < 2
		SET_AMMO_IN_CLIP(pedTrevor(), WEAPONTYPE_PISTOL, 8)
	ENDIF
	GET_AMMO_IN_CLIP(pedTrevor(), WEAPONTYPE_PISTOL, iTrevorsAmmoCount)
	IF iFranklinsAmmoCount < 2
		SET_AMMO_IN_CLIP(pedFranklin(), WEAPONTYPE_PISTOL, 8)
	ENDIF
	
	SWITCH iGetDealWithFranklin
		
		CASE 0
		
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				NEW_LOAD_SCENE_STOP()
			ENDIF

			IF NOT IS_ENTITY_DEAD(pedTrevor())
				SETUP_CREW_VARIABLES(pedTrevor())
				SET_PED_NAME_DEBUG(pedTrevor(), "Trevor")
			ENDIF
		
			IF NOT IS_ENTITY_DEAD(pedFranklin())
				SETUP_CREW_VARIABLES(pedFranklin())	
				SET_PED_NAME_DEBUG(pedFranklin(), "Franklin")
			ENDIF

			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, pedTrevor(), "TREVOR")
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, pedFranklin(), "FRANKLIN")
	
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(arrivalBurrito) 
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(arrivalBurrito) 
			
			GIVE_WEAPON_TO_PED(pedTrevor(), WEAPONTYPE_PISTOL, 200)
			GIVE_WEAPON_TO_PED(pedFranklin(), WEAPONTYPE_PISTOL, 200)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTrevor(), FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFranklin(), FALSE)
		
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "STAGE_DEAL_WITH_FRANKLIN")
			g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = iCurrentTake
			g_iPaletoScoreTake = iCurrentTake	//UPdate the saved take
			FADE_IN_IF_NEEDED()
			SET_ENTITY_HEALTH(PLAYER_PED_ID(), 300)
			
			REQUEST_WAYPOINT_RECORDING("RBHFRANKBOAT")
			
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 8.0)
			
			iGetDealWithFranklin++
		BREAK
			
		CASE 1
			IF IS_FOLLOW_PED_CAM_ACTIVE() 
			OR GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
			OR IS_FOLLOW_VEHICLE_CAM_ACTIVE()
				RESET_GAME_CAMERA()
				SET_CUTSCENE_RUNNING(FALSE, TRUE)	
				iGetDealWithFranklin++
			ENDIF
		BREAK
			
		CASE 2
			IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_LETSGO", CONV_PRIORITY_VERY_HIGH)
				iGetDealWithFranklin++	
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				//Get to the Pier
				//sLocatesData.LocationBlip = CREATE_BLIP_FOR_COORD(<< 10.6583, 6647.2236, 31.0232 >>, TRUE)
				//Fix for 307560
				//SET_BIT(sLocatesData.iLocatesBitSet, BS_INITIAL_GOD_TEXT_PRINTED)
				//PRINT_NOW("GETTOPIER", DEFAULT_GOD_TEXT_TIME, 1)
				iGetDealWithFranklin++
			ENDIF
		BREAK
	
		
		CASE 4
		CASE 5
		CASE 6
		CASE 7
		CASE 8
		CASE 9
		CASE 10
		
			IF IS_SCREEN_FADED_OUT()
			OR IS_SCREEN_FADING_OUT()
				WHILE IS_SCREEN_FADING_OUT()
					WAIT(0)
				ENDWHILE
				SET_BIT(sLocatesData.iLocatesBitSet, BS_INITIAL_GOD_TEXT_PRINTED)
				DO_SCREEN_FADE_IN(500)
				WHILE IS_SCREEN_FADING_IN()
					WAIT(0)
				ENDWHILE
			ENDIF
		
			IF NOT IS_ENTITY_DEAD(arrivalBurrito)
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				AND iGetDealWithFranklin = 4
					REQUEST_MODEL(SQUALO)
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_BANT1", CONV_PRIORITY_VERY_HIGH)
						iGetDealWithFranklin = 5
					ENDIF
				ENDIF
				
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				AND DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				AND iGetDealWithFranklin = 5	
//					IF bisBuddyAPro
//						IF	CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_BANT1B", CONV_PRIORITY_VERY_HIGH)
//							iGetDealWithFranklin = 6
//						ENDIF
//					ELSE
//						IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_BANTB2", CONV_PRIORITY_VERY_HIGH)
//							iGetDealWithFranklin = 6
//						ENDIF
//					ENDIF
			
					IF CREATE_GUNMAN_CONVERSATION("RBH2A_BANT1b", "RBH2A_1ST_DJ", "RBH2A_1ST_CH", "RBH2A_1ST_NR", "RBH2A_1ST_PM")
						iGetDealWithFranklin = 6
					ENDIF
				ENDIF
				
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				AND DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				AND iGetDealWithFranklin = 6
					
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_BANT1C", CONV_PRIORITY_VERY_HIGH)
						iGetDealWithFranklin = 7
					ENDIF
				ENDIF
				
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				AND DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				AND iGetDealWithFranklin = 7
					
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_BANT1D", CONV_PRIORITY_VERY_HIGH)
						iGetDealWithFranklin = 8
					ENDIF
				ENDIF
				
				
				//IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				//AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << 10.6583, 6647.2236, 29.0232 >>) < 330.0
				AND iGetDealWithFranklin <= 8
					KILL_FACE_TO_FACE_CONVERSATION()
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_DROPOF", CONV_PRIORITY_VERY_HIGH)
						iGetDealWithFranklin = 9
					ENDIF
				ENDIF
				
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				AND iGetDealWithFranklin = 9
					IF PRELOAD_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_DROP2", CONV_PRIORITY_VERY_HIGH)	
						iGetDealWithFranklin = 10
					ENDIF
				ENDIF
				
			ENDIF
		
			IF iGetDealWithFranklin < 10	
				IF NOT IS_ENTITY_DEAD(arrivalBurrito)
					IF NOT DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					OR IS_ENTITY_UPSIDEDOWN(arrivalBurrito)
						IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
							IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedTrevor(), "RBH2A_KGAA", "TREVOR")
							ENDIF
						ENDIF
					ELSE
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
	
			IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData, << 10.6583, 6647.2236, 28.7 >>, <<0.1, 0.1, 0.1>>, TRUE, pedFranklin(), pedTrevor(),  pedGunman, arrivalBurrito , "GETTOPIER", "LEFTCREW", "LEFTCREW", "", "LEFTCREW", "GETINVAN", "GETBACKIN", FALSE, TRUE)
			OR (DOES_BLIP_EXIST(sLocatesData.LocationBlip) AND IS_ENTITY_IN_ANGLED_AREA(arrivalBurrito, <<10.543437,6643.602051,29.789328>>, <<8.626600,6649.545410,33.988613>>, 7.250000))
			//IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vBankCoordinates, <<3.0, 3.0, 3.0>>)

							
				REQUEST_MODEL(SQUALO)
				REQUEST_WAYPOINT_RECORDING("RBHFRANKBOAT")
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				
				IF HAS_MODEL_LOADED(SQUALO)
				AND GET_IS_WAYPOINT_RECORDING_LOADED("RBHFRANKBOAT")
							
//					WHILE IS_SCRIPTED_CONVERSATION_ONGOING()
//						WAIT(0)
//					ENDWHILE
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "STAGE_GET_TO_BANK")

					IF iGetDealWithFranklin < 10 
					
						INT iDealWithFranklinCounter
						iDealWithFranklinCounter = 0
						WHILE NOT PRELOAD_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_DROP2", CONV_PRIORITY_VERY_HIGH)	
						AND iDealWithFranklinCounter < 240
							iDealWithFranklinCounter++
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
						
							WAIT(0)
							printstring("here1") printnl()
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
						ENDWHILE
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(arrivalBurrito)
						WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(arrivalBurrito)
						
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
						
							WAIT(0)
							printstring("here2") printnl()
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
						ENDWHILE					
					ENDIF
					
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
				
					SET_GPS_MULTI_ROUTE_RENDER(FALSE)
					CLEAR_GPS_MULTI_ROUTE()
					//bCustomGPSActive = FALSE
															
					REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipPier)
					
					SET_ROADS_IN_AREA(<<-178.7640, 6411.6133, 23.2231>>, <<-83.2894, 6493.1436, 43.5919>>, FALSE)
								
					franklinsBoat = CREATE_VEHICLE(SQUALO, << -121.7210, 6727.2910, -0.5835 >>, 51.0306)
					SET_BOAT_ANCHOR(franklinsBoat, TRUE)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(franklinsBoat, FALSE)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(SQUALO)
									
					SEQUENCE_INDEX seqFranklinLeave
					
					OPEN_SEQUENCE_TASK(seqFranklinLeave)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_LEAVE_ANY_VEHICLE(NULL)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						//TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), 4000)
						//TASK_PAUSE(NULL, 4000)
						TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHFRANKBOAT", 0, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_NAVMESH_TO_INITIAL_WAYPOINT)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -117.7753, 6729.6235, 0.0268 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
						//TASK_ACHIEVE_HEADING(NULL, 179.8780)
						TASK_ENTER_VEHICLE(NULL, franklinsBoat)
					CLOSE_SEQUENCE_TASK(seqFranklinLeave)
					
					IF NOT IS_PED_INJURED(pedFranklin())
						REMOVE_PED_FROM_GROUP(pedFranklin())
						TASK_PERFORM_SEQUENCE(pedFranklin(), seqFranklinLeave)
					ENDIF
					
					SET_ENTITY_LOD_DIST(pedFranklin(), 200)
					
					CLEAR_SEQUENCE_TASK(seqFranklinLeave)
				
					requestAssetsStageGetToBank()
					
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
					
					iGetDealWithFranklin = 11
					
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 11
		
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		
			//IF 	CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_DROP2", CONV_PRIORITY_VERY_HIGH)	
				BEGIN_PRELOADED_CONVERSATION()
				iGetDealWithFranklin++
			//ENDIF
		BREAK
	
		CASE 12
		
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		
			IF NOT IS_PED_INJURED(pedFranklin())
				IF NOT IS_PED_IN_VEHICLE(pedFranklin(), arrivalBurrito) //IS_PED_ON_FOOT(pedFranklin())
				//AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					mission_stage = STAGE_GET_TO_BANK
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH
	

ENDPROC


PROC SETUP_CUSTOM_SWITCH_CAM_CHICKEN_FACTORY_TO_TREVOR()
	  sCamDetails.bRun = TRUE
       
    //Destroy the spline cam itself to reset
    IF DOES_CAM_EXIST(sCamDetails.camID)
        DESTROY_CAM(sCamDetails.camID)
    ENDIF
    
    //Recreate the spline cam
   // sCamDetails.camID = CREATE_CAMERA(CAMTYPE_SPLINE_SMOOTHED, FALSE)
    sCamDetails.camID = CREATE_CAMERA(CAMTYPE_SPLINE_DEFAULT, FALSE)
	SET_CAM_SPLINE_SMOOTHING_STYLE(sCamDetails.camID, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
	
    //Add the starting node, I'm using the FINAL_RENDERED_CAM which is whatever is rendering on screen when this is called.
   // ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), GET_FINAL_RENDERED_CAM_FOV(), TRUE), 0)
          
    //Add Spline Nodes to sCamDetails.camID from the closest node until the end of the array splineNodes
	ADD_CAM_SPLINE_NODE(sCamDetails.camID, GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), 1000)
	
	ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-92.301842,6213.175781,31.612413>>,<<0.222034,0.000000,-92.264275>>,50.000000, TRUE), 1000)
	ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-75.810150,6223.402832,31.817638>>,<<1.645923,-7.815183,21.762142>>,50.000000, TRUE), 1000)
	ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-80.819580,6233.528809,32.334766>>,<<-4.976900,15.441675,-50.074341>>,50.000000, TRUE), 1000)
	ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-67.245407,6242.639160,31.843157>>,<<-0.262811,-7.677924,21.794558>>,50.000000, TRUE), 1000)
	ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-73.668900,6255.863770,31.774250>>,<<1.125606,10.509629,-11.638501>>,50.000000, TRUE), 1000)
	ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-71.086189,6268.126953,31.812103>>,<<3.391589,-9.402388,15.975072>>,50.000000, TRUE), 1000)
	ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<-76.882622,6267.985840,32.379868>>,<<0.373137,-6.110023,60.943493>>,50.000000, TRUE), 1000)


	ADD_CAM_SPLINE_NODE_USING_GAMEPLAY_FRAME(sCamDetails.camID, 750)

    //Set the overall spline duration and smoothing style. 
   // SET_CAM_SPLINE_DURATION(sCamDetails.camID, 15000)
    SET_CAM_SPLINE_SMOOTHING_STYLE(sCamDetails.camID, CAM_SPLINE_NO_SMOOTH)
	//SET_CAM_SPLINE_SMOOTHING_STYLE(sCamDetails.camID, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
	
    sCamDetails.bSplineCreated = TRUE
    
   // sCamDetails.pedTo = pedSelector.pedID[pedSelector.eNewSelectorPed]
    sCamDetails.bRun = TRUE
	

ENDPROC

PROC SETUP_CUSTOM_SWITCH_CAM_CHICKEN_FACTORY_TO_MICHAEL()
	  sCamDetails.bRun = TRUE
       
    //Destroy the spline cam itself to reset
    IF DOES_CAM_EXIST(sCamDetails.camID)
        DESTROY_CAM(sCamDetails.camID)
    ENDIF
    
    //Recreate the spline cam
   // sCamDetails.camID = CREATE_CAMERA(CAMTYPE_SPLINE_SMOOTHED, FALSE)
    sCamDetails.camID = CREATE_CAMERA(CAMTYPE_SPLINE_DEFAULT, FALSE)
	SET_CAM_SPLINE_SMOOTHING_STYLE(sCamDetails.camID, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
	
    //Add the starting node, I'm using the FINAL_RENDERED_CAM which is whatever is rendering on screen when this is called.
   // ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), GET_FINAL_RENDERED_CAM_FOV(), TRUE), 0)
          
    //Add Spline Nodes to sCamDetails.camID from the closest node until the end of the array splineNodes
	ADD_CAM_SPLINE_NODE(sCamDetails.camID, GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), 1000)
	ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<-71.527428,6269.575195,31.253708>>,<<-1.040228,-2.311345,-171.259323>>,50.0, TRUE), 1000)
	ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<-73.550949,6257.142578,31.604795>>,<<2.894839,-8.956678,-161.445068>>,50.000000, TRUE), 1000)
	ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<-68.287483,6243.393066,32.210320>>,<<-0.174633,14.889684,135.781067>>,50.000000, TRUE), 1000)
	ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<-81.067390,6234.020996,31.395161>>,<<5.917362,-8.469506,-142.504929>>,50.000000, TRUE), 1000)
	ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<-75.708313,6224.997070,30.824867>>,<<1.192224,11.552670,151.153519>>,50.000000, TRUE), 1000)
	ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<-87.534920,6213.356934,31.603662>>,<<-3.426521,3.066668,98.182205>>,50.000000, TRUE), 1000)
	
		
	ADD_CAM_SPLINE_NODE_USING_GAMEPLAY_FRAME(sCamDetails.camID, 750)

    //Set the overall spline duration and smoothing style. 
   // SET_CAM_SPLINE_DURATION(sCamDetails.camID, 15000)
    //SET_CAM_SPLINE_SMOOTHING_STYLE(sCamDetails.camID, CAM_SPLINE_NO_SMOOTH)
	SET_CAM_SPLINE_SMOOTHING_STYLE(sCamDetails.camID, CAM_SPLINE_SLOW_IN_OUT_SMOOTH)
	
    sCamDetails.bSplineCreated = TRUE
    
   // sCamDetails.pedTo = pedSelector.pedID[pedSelector.eNewSelectorPed]
    sCamDetails.bRun = TRUE
	

ENDPROC


BOOL haveCleanedUpRobberyObjects

VEHICLE_INDEX cutCopCar0
VEHICLE_INDEX cutCopCar1
VEHICLE_INDEX cutCopCar2
VEHICLE_INDEX cutCopCar3
VEHICLE_INDEX cutCopCar4

PROC stageHeistCutscene()

//	IF DOES_ENTITY_EXIST(oiBankDoorRight)
//		REMOVE_MODEL_HIDE(<<-109.65, 6462.11, 31.99>>, 10.0, V_ilev_bank4door01, FALSE)
//		DELETE_OBJECT(oiBankDoorRight)
//		SET_MODEL_AS_NO_LONGER_NEEDED(V_ilev_bank4door01)
//	ENDIF

	IF iHeistCutseneStage < 6
		CLOSE_BANK_DOORS(TRUE)
	ENDIF

	IF iHeistCutseneStage > 5
		HIDE_HUD_AND_RADAR_THIS_FRAME()
	ENDIF

	SWITCH iHeistCutseneStage
	
		CASE 0
			//REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("RBH_2A_MCS_2", CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8  | CS_SECTION_9 | CS_SECTION_10 | CS_SECTION_11 | CS_SECTION_12 | CS_SECTION_13 | CS_SECTION_14 | CS_SECTION_15)
			
			//REQUEST_CUTSCENE("RBH_2A_MCS_2")
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "STAGE_HEIST_CUTSCENE")
			g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = iCurrentTake
			REQUEST_HEIST_CUTSCENE_ASSETS()
			
			DISTANT_COP_CAR_SIRENS(TRUE)
			iHeistCutseneStage = 2
		BREAK
			
		CASE 2
			
			IF HAS_CUTSCENE_LOADED()
			AND HAS_WEAPON_ASSET_LOADED(pedGunmanWeaponType)
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_COMBATMG)
			AND HAS_ANIM_SET_LOADED("ANIM_GROUP_MOVE_BALLISTIC")
			AND HAS_ANIM_SET_LOADED("MOVE_STRAFE_BALLISTIC")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "RBHInitial")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "RBHInitial")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(3, "RBHInitial")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(6, "RBHINitial")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(7, "RBHINitial")
			AND HAS_PTFX_ASSET_LOADED()
		
				IF NOT IS_ENTITY_DEAD(pedTrevor())
					REGISTER_ENTITY_FOR_CUTSCENE(pedTrevor(), "TREVOR", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF				
				IF NOT IS_ENTITY_DEAD(pedGunman)
					REGISTER_ENTITY_FOR_CUTSCENE(pedGunman, "gunman_selection_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF				
				IF NOT IS_ENTITY_DEAD(PedMichael())
					REGISTER_ENTITY_FOR_CUTSCENE(PedMichael(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedMichael(), TRUE)
				ENDIF
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				REGISTER_ENTITY_FOR_CUTSCENE(cutCopCar0, "Police_Car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, SHERIFF)
				REGISTER_ENTITY_FOR_CUTSCENE(cutCopCar1, "Police_Car^1", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, SHERIFF)
				REGISTER_ENTITY_FOR_CUTSCENE(cutCopCar2, "Police_Car^2", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, SHERIFF)
				REGISTER_ENTITY_FOR_CUTSCENE(cutCopCar3, "Police_Car^3", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, SHERIFF)
				REGISTER_ENTITY_FOR_CUTSCENE(cutCopCar4, "Police_Car^4", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, SHERIFF)
			
				
				START_CUTSCENE()
								
				SET_MODEL_AS_NO_LONGER_NEEDED(P_LD_HEIST_BAG_S_1)
				SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Welding_Mask_01_S)
				SET_MODEL_AS_NO_LONGER_NEEDED(prop_tool_blowtorch)
	
				SET_MODEL_AS_NO_LONGER_NEEDED(S_F_Y_AIRHOSTESS_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_BUSINESS_01)
				
				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(pedGunman, TRUE)
				
				DELETE_ARRAY_OF_PEDS(pedsBankers, TRUE)
	
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				
				TRIGGER_MUSIC_EVENT("RH2A_TREV_FACE")
				
				haveCleanedUpRobberyObjects = FALSE
				iHeistCutseneStage =5
			ENDIF
			
		BREAK
				
		CASE 5
		CASE 6
		CASE 7
		CASE 8
		CASE 9
		CASE 10
		CASE 99
			
			IF IS_CUTSCENE_PLAYING()
				IF (haveCleanedUpRobberyObjects = FALSE)
				
					OPEN_BANK_DOORS()		
				
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MINIGUN, 7000, TRUE)
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADELAUNCHER, 10, TRUE)
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_COMBATMG, 500, TRUE)
				
					FADE_IN_IF_NEEDED()
				
					DELETE_OBJECT(oiBagTrevor)
					DELETE_OBJECT(oiBlowTorch)
					DELETE_OBJECT(oiWeldingMask)
					
					DELETE_OBJECT(oiBagGunman)
					DELETE_OBJECT(oiBagMichael)
					DELETE_OBJECT(oiBagTrevor)
					DELETE_OBJECT(oiBagStrapTrevor)
					
					DELETE_VEHICLE(vehCopCarOutFront)
					DELETE_PED(pedSheriff_CarDoor)
					DELETE_PED(pedSheriff_cover)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(p_csh_strap_01_s)
	
					REMOVE_BLIP(blipInsideBank)
					REMOVE_BLIP(blipSafe)
					DELETE_OBJECT(oiStoreDoorTempVault)
					
					SET_CLOCK_TIME(18, 0, 0)
					
					SET_SKIP_MINIGUN_SPIN_UP_AUDIO(true)
										
					haveCleanedUpRobberyObjects = TRUE
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(cutCopCar0)
					cutCopCar0 = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Police_Car"))
					SET_VEHICLE_SIREN(cutCopCar0, TRUE)
					SET_SIREN_WITH_NO_DRIVER(cutCopCar0, TRUE)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(cutCopCar1)
					cutCopCar1 = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Police_Car^1"))
					SET_VEHICLE_SIREN(cutCopCar1, TRUE)
					SET_SIREN_WITH_NO_DRIVER(cutCopCar1, TRUE)					
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(cutCopCar2)
					cutCopCar2 = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Police_Car^2"))
					SET_VEHICLE_SIREN(cutCopCar2, TRUE)
					SET_SIREN_WITH_NO_DRIVER(cutCopCar2, TRUE)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(cutCopCar3)
					cutCopCar3 = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Police_Car^3"))
					SET_VEHICLE_SIREN(cutCopCar3, TRUE)
					SET_SIREN_WITH_NO_DRIVER(cutCopCar3, TRUE)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(cutCopCar4)
					cutCopCar4 = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Police_Car^4"))
					SET_VEHICLE_SIREN(cutCopCar4, TRUE)
					SET_SIREN_WITH_NO_DRIVER(cutCopCar4, TRUE)
				ENDIF
				
				IF NOT IS_PED_INJURED(pedGunman)
					SET_PED_COMPONENT_VARIATION(pedGunman, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
					//Bag
					SET_PED_COMPONENT_VARIATION(pedGunman, INT_TO_ENUM(PED_COMPONENT,6), 1, 0, 0) //(feet)
				ENDIF
				//Bag
				IF NOT IS_PED_INJURED(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					SET_PED_COMPONENT_VARIATION(pedSelector.pedID[SELECTOR_PED_MICHAEL], INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
				ENDIF
				//Bag
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)	
				ENDIF
				
				IF GET_CUTSCENE_TIME() > 3000.164
				AND iHeistCutseneStage < 6
					iHeistCutseneStage = 6
				ENDIF
							
				IF GET_CUTSCENE_TIME() > 5000.92
				AND iHeistCutseneStage < 7
					iHeistCutseneStage = 7
				ENDIF
				
				IF GET_CUTSCENE_TIME() > 13000.92
				AND iHeistCutseneStage < 8
					SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), FALSE)
					iHeistCutseneStage = 8
				ENDIF
									
			ENDIF		
			
						
			IF NOT IS_CUTSCENE_ACTIVE()
				IF NOT IS_PED_INJURED(pedGunman)
					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(pedGunman, FALSE)				
					
					SET_ENTITY_HEADING(pedGunman, 140.3602)
				
					GIVE_WEAPON_TO_PED(pedGunman, pedGunmanWeaponType, INFINITE_AMMO, TRUE)
					SET_CURRENT_PED_WEAPON(pedGunman, pedGunmanWeaponType, TRUE)
					TASK_AIM_GUN_AT_COORD(pedGunman, << -125.7645, 6447.5020, 31.5079 >>, 14000, TRUE)
					FORCE_PED_MOTION_STATE(pedGunman, MS_AIMING, TRUE, FAUS_CUTSCENE_EXIT)
					//FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGunman)
					
					PRINTLN("Exit state: gunman_selection_1")
					
					
					SET_PED_COMPONENT_VARIATION(pedGunman, INT_TO_ENUM(PED_COMPONENT,6), 4, 0, 0) //(feet)
					
				ENDIF
			ENDIF
			
			IF NOT IS_CUTSCENE_ACTIVE()
	
				IF NOT IS_PED_INJURED(PedMichael())
					SET_ENTITY_HEADING(PedMichael(), 128.0643)
					GIVE_WEAPON_TO_PED(PedMichael(), PedMichaelWeaponType, 200, TRUE)
					SET_CURRENT_PED_WEAPON(PedMichael(), PedMichaelWeaponType, TRUE)
					TASK_AIM_GUN_AT_COORD(PedMichael(), << -125.7645, 6447.5020, 31.5079 >>, 14000, TRUE)
					FORCE_PED_MOTION_STATE(PedMichael(), MS_AIMING, TRUE, FAUS_CUTSCENE_EXIT)
				
					PRINTLN("Exit state: Michael")
					
					SET_PED_COMPONENT_VARIATION(pedSelector.pedID[SELECTOR_PED_MICHAEL], INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
					
				ENDIF
			ENDIF


			IF NOT IS_CUTSCENE_ACTIVE()		
			
				REPLAY_STOP_EVENT()
			
				DELETE_VEHICLE(cutCopCar0)
				
				DELETE_VEHICLE(cutCopCar1)
				DELETE_VEHICLE(cutCopCar2)
				DELETE_VEHICLE(cutCopCar3)
				DELETE_VEHICLE(cutCopCar4)
			
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 139.1)
								
				SETUP_HOLD_OFF_COPS_AT_BANK()
				
				SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
				
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_InstantBlendToAim, TRUE)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_AIMING, TRUE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
				
				SET_PLAYER_SIMULATE_AIMING(PLAYER_ID(), TRUE)
				
				SET_EQIPPED_WEAPON_START_SPINNING_AT_FULL_SPEED(PLAYER_PED_ID())
				
				SETUP_HOLD_OFF_COPS_AT_BANK()
				CREATE_COP_CARS_HOLD_OFF_COPS_AT_BANK()
				DELETE_VEHICLE(arrivalBurrito)
				DELETE_PED(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
				CLEAR_AREA_OF_VEHICLES(vBankCoordinates, 60.00)
				
				SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(PLAYER_PED_ID())
						
				IF bankInterior <> NULL 		
					UNPIN_INTERIOR(bankInterior)
				ENDIF
				
				REMOVE_BLIP(blipInsideBank)
				REMOVE_BLIP(blipSafe)
				
				CLOSE_BANK_DOORS()
				PRINTLN("Cutscene Finished!")	
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				SET_CREW_IN_BALLISTIC_ARMOUR(FALSE)
				SETTIMERA(0)
				mission_stage = STAGE_HOLD_OFF_COPS_AT_BANK
			ENDIF
			
		BREAK
		 
	ENDSWITCH
ENDPROC



PROC TASK_GO_TO_TWO_POINTS(PED_INDEX thisPed, VECTOR vec1, VECTOR vec2, FLOAT fMoveBlend = PEDMOVE_WALK)

	SEQUENCE_INDEX thisSeq

	OPEN_SEQUENCE_TASK(thisSeq)
		TASK_GO_STRAIGHT_TO_COORD(NULL, vec1,fMoveBlend)
		TASK_GO_STRAIGHT_TO_COORD(NULL, vec2,fMoveBlend)
	CLOSE_SEQUENCE_TASK(thisSeq)
	
	TASK_PERFORM_SEQUENCE(thisPed, thisSeq)
	CLEAR_SEQUENCE_TASK(thisSeq)

ENDPROC


PROC CLEANUP_COPS_TURN_UP_CUTSCENE()
				
	int i
	FOR i = 0 TO 7
		DELETE_PED(cutscenePeds[i])
	ENDFOR
	FOR i = 0 TO 7
		DELETE_VEHICLE(cutsceneCars[i])
	ENDFOR
	
	FOR i = 0 TO NUMBER_OF_COPS_OUTSIDE_BANK - 1
		DELETE_PED(pedsCopsOutsideBank[i])
	ENDFOR
	
	FOR i = 0 TO NUMBER_OF_COP_CARS_OUTSIDE_BANK - 1
		DELETE_VEHICLE(CopCarsParked[i])
	ENDFOR
	
	FOR i = 0 TO 2
		DELETE_PED(runnerCops1[i])
	ENDFOR

ENDPROC







/// PURPOSE: Explodes a vehicle when it reaches desired health level. Returns TRUE when it explodes.
FUNC BOOL EXPLODE_VEHICLE_AT_HEALTH_LEVEL_NO_BUDDY(VEHICLE_INDEX &thisCar, INT iExplodeHealth)


	IF NOT IS_ENTITY_DEAD(thisCar)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(thisCar, TRUE)
		
		//Don't blow up cars that are really close the player or his crew.
		IF GET_DISTANCE_BETWEEN_ENTITIES(thisCar, PLAYER_PED_ID(), FALSE)  > 7.0
		AND GET_DISTANCE_BETWEEN_ENTITIES(thisCar, PedMichael(), FALSE)		> 7.0
				
			IF IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(thisCar), 1.0)
				//Think up a check to see if you have just damaged it this frame. FIXLATER
				IF GET_ENTITY_HEALTH(thisCar)  < iExplodeHealth
					FREEZE_ENTITY_POSITION(thisCar, FALSE)
					EXPLODE_VEHICLE(thisCar)
					RETURN TRUE				
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
	
	RETURN FALSE	

ENDFUNC

PROC checkForPlayerInputThenClearTAsks()

	INT padLX, padLy, padRX, padRy

	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(padLX, padLy, padRX, padRy)
	
	IF ABSI(padLX) > 75
	OR ABSI(padLY) > 75
	OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_AIM)
	
		IF bPlayerHasTakenControl = FALSE
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			bPlayerHasTakenControl = TRUE
		ENDIF
	ENDIF
	
ENDPROC





PROC ROTATE_VECTOR1(VECTOR &InVec, VECTOR rotation)
      
	FLOAT CosAngle
	FLOAT SinAngle
	VECTOR ReturnVec

	// Rotation about the x axis 
	CosAngle = COS(rotation.x)
	SinAngle = SIN(rotation.x)
	ReturnVec.x = InVec.x
	ReturnVec.y = (CosAngle * InVec.y) - (SinAngle * InVec.z)
	ReturnVec.z = (SinAngle * InVec.y) + (CosAngle * InVec.z)
	InVec = ReturnVec

	// Rotation about the y axis
	CosAngle = COS(rotation.y)
	SinAngle = SIN(rotation.y)
	ReturnVec.x = (CosAngle * InVec.x) + (SinAngle * InVec.z)
	ReturnVec.y = InVec.y
	ReturnVec.z = (CosAngle * InVec.z) - (SinAngle * InVec.x) 
	InVec = ReturnVec

	// Rotation about the z axis 
	CosAngle = COS(rotation.z)
	SinAngle = SIN(rotation.z)
	ReturnVec.x = (CosAngle * InVec.x) - (SinAngle * InVec.y)
	ReturnVec.y = (SinAngle * InVec.x) + (CosAngle * InVec.y)
	ReturnVec.z = InVec.z
	InVec = ReturnVec
	
ENDPROC



FUNC VECTOR GET_SCREEN_OFFSET_VECTOR2()

    VECTOR vStart
    VECTOR vEnd
    VECTOR vYoffset
    VECTOR vZoffset
    
//    FLOAT fyOffset = fx
//    FLOAT fXOffset = fy
//    FLOAT fZOffset = fz
    
    //Start vector
    vStart = GET_FINAL_RENDERED_CAM_COORD()
    //end vector - pointing north
    vEnd    = <<0.0, 1.0, 0.0>>                     
    //point it in the same direction as the camera
    ROTATE_VECTOR(vEnd, GET_FINAL_RENDERED_CAM_ROT())                       
    //Make the normilised roted vector 10 times larger
    vEnd.x *= fXOffset  
    vEnd.y *= fXOffset  
    vEnd.z *= fXOffset  
    
    //Add an offset
    vYoffset        = <<1.0, 0.0, 0.0>>
    //point it in the same direction as the camera
    ROTATE_VECTOR(vYoffset, GET_FINAL_RENDERED_CAM_ROT())
    //Make the normilised roted vector 10 times larger
    
    vYoffset.x *= fyOffset 
    vYoffset.y *= fyOffset
    vYoffset.z *=fyOffset
    
    //Add an offset
    vZoffset        = <<0.0, 0.0, 1.0>>
    //point it in the same direction as the camera
    ROTATE_VECTOR(vZoffset, GET_FINAL_RENDERED_CAM_ROT())
    //Make the normilised roted vector 10 times larger
    vZoffset.x *= fZOffset  
    vZoffset.y *= fZOffset
    vZoffset.z *= fZOffset
    
    //Add them all up
    vEnd += (vStart + vYoffset + vZoffset)

    RETURN vEnd
ENDFUNC

INT iTimeBuddyLastRepositionHimself

FUNC VECTOR GET_COORD_ON_RIGHT_HAND_SIDE_OF_SCREEN(VECTOR &vReturn, SHAPETEST_INDEX &passed1)
        
    INT iTemp
    
    //Set up the start and end vectors
	VECTOR vStart
    //end vector - pointing north
	VECTOR vEnd = <<0.0, 1.0, 0.0>>
    VECTOR vReturnTemp
    VECTOR vNormal
	ENTITY_INDEX hitEntity
      
	
	IF (GET_GAME_TIMER() - iTimeBuddyLastRepositionHimself) > 2000
    
		vStart = GET_SCREEN_OFFSET_VECTOR2()
	
		 //point it in the same direction as the camera
	    ROTATE_VECTOR(vEnd,   GET_ENTITY_ROTATION(PLAYER_PED_ID()) + vRotationModifier) 
	    //Make the normilised roted vector 400 times larger
	    vEnd.x *= 600  
	    vEnd.y *= 600  
	    vEnd.z *= 600  
	    //add it on to the start vector to get the end vector coordinates
	    vEnd += vStart
	
		
		IF passed1 = NULL
           	passed1 = START_SHAPE_TEST_LOS_PROBE(vStart, vEnd)	
		ENDIF
		iTimeBuddyLastRepositionHimself = GET_GAME_TIMER()
	ELSE
        IF GET_SHAPE_TEST_RESULT(passed1, iTemp, vReturnTemp, vNormal, hitEntity) = SHAPETEST_STATUS_RESULTS_READY
            passed1 = NULL
        ENDIF
    ENDIF

    IF  vReturnTemp.x = 0.0
    AND vReturnTemp.y = 0.0
    AND vReturnTemp.z = 0.0
		#IF IS_DEBUG_BUILD
        	IF bIsSuperDebugEnabled
				PRINTSTRING("GET_COORD_IN_CENTER_OF_SCREEN - vReturnTemp = <<0.0, 0.0, 0.0>>")PRINTNL()
			ENDIF
		#ENDIF
        vReturnTemp = vReturn
    ELSE
        vReturn = vReturnTemp
    ENDIF
    
	#IF IS_DEBUG_BUILD
		If bIsSuperDebugEnabled
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
    		DRAW_DEBUG_LINE(vStart, vEnd)
    		DRAW_DEBUG_SPHERE(vStart, 1.0, 255, 0, 0, 180)
		
    		DRAW_DEBUG_SPHERE(vReturnTemp, 1.0, 0, 255, 0, 255)
		ENDIF
	#ENDIF
	
    RETURN vReturnTemp
        
ENDFUNC

VECTOR vCoronaPos = vDefensiveAreasOutSideBank[0]
VECTOR vLastPointWalkedTo = vDefensiveAreasOutSideBank[0]
INT iPoints
SHAPETEST_INDEX shapeTest1
 SEQUENCE_INDEX seqAlwaysStayOnCamera
//BOOL bSwitching

PROC KEEP_YOUR_FRIENDS_CLOSE(PED_INDEX thisPed, BOOL bOutSideBank = FALSE, FLOAT fDefAreaSize = 2.5)

	IF NOT IS_ENTITY_DEAD(thisPed)

		GET_COORD_ON_RIGHT_HAND_SIDE_OF_SCREEN(vCoronaPos, shapeTest1)
		
		//Choose a close predefined point if outside bank.
		IF bOutSideBank
			IF iPoints > 7
				iPoints = 0
			ENDIF
			
			IF GET_DISTANCE_BETWEEN_COORDS(vDefensiveAreasOutSideBank[iPoints], vCoronaPos) < 7.0
				vCoronaPos = vDefensiveAreasOutSideBank[iPoints]
			ELSE
				vCoronaPos = vLastPointWalkedTo
			ENDIF
			iPoints++
		ELSE
		
			IF IS_POINT_IN_ANGLED_AREA( vCoronaPos, <<-158.189499,6248.646973,29.099573>>, <<-211.636978,6301.110840,33.661129>>, 58.000000)
				vCoronaPos = vLastPointWalkedTo
			ENDIF
		ENDIF
				
		
		IF VMAG2((vLastPointWalkedTo - vCoronaPos)) > 0.1		//Only if the vector has changed...
			IF GET_DISTANCE_BETWEEN_COORDS(vCoronaPos, GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE) < 12.0 //Dont go too far away..
			AND GET_DISTANCE_BETWEEN_COORDS(vCoronaPos, GET_ENTITY_COORDS(thisPed), FALSE) > 2.5		//Dont go if you are already close
				PRINTLN("KFC: Valid point found", vCoronaPos)
					
					SET_PED_COMBAT_ATTRIBUTES(thisPed,CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH,TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thisPed, FALSE)
					SET_PED_SPHERE_DEFENSIVE_AREA(thisPed, vCoronaPos, fDefAreaSize)
				
					vLastPointWalkedTo = vCoronaPos
					PRINTLN("KFC: Walk to: ", vCoronaPos)
				//ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


PROC KEEP_YOUR_FRIENDS_CLOSE_SHOOT_ENTITY(PED_INDEX thisPed, ENTITY_INDEX thisEntity)

	GET_COORD_ON_RIGHT_HAND_SIDE_OF_SCREEN(vCoronaPos, shapeTest1)
		
	IF VMAG2((vLastPointWalkedTo - vCoronaPos)) <> 0.0		//Only of the vector has changed...
		IF GET_DISTANCE_BETWEEN_COORDS(vCoronaPos, GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE) < 12.0 //Dont go too far away..
		AND GET_DISTANCE_BETWEEN_COORDS(vCoronaPos, GET_ENTITY_COORDS(thisPed), FALSE) > 2.5		//Dont go if you are already close
			PRINTLN("KFC: Valid point found", vCoronaPos)
			//UPdate task...
			IF NOT IS_PED_DOING_TASK(thisPed, SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY)
				
				OPEN_SEQUENCE_TASK(seqAlwaysStayOnCamera)
					TASK_CLEAR_DEFENSIVE_AREA(NULL)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					IF NOT IS_ENTITY_DEAD(thisEntity)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, vCoronaPos, thisEntity, PEDMOVE_SPRINT, TRUE)
					ENDIF
					TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, vCoronaPos, 4.0)
					TASK_SHOOT_AT_ENTITY(NULL, thisEntity, INFINITE_TASK_TIME, FIRING_TYPE_RANDOM_BURSTS)
				CLOSE_SEQUENCE_TASK(seqAlwaysStayOnCamera)
				
				TASK_PERFORM_SEQUENCE(thisPed, seqAlwaysStayOnCamera)
				CLEAR_SEQUENCE_TASK(seqAlwaysStayOnCamera)
				vLastPointWalkedTo = vCoronaPos
				PRINTLN("KFCSE: Walk to: ", vCoronaPos)
			ENDIF
		ENDIF
	ENDIF
			
ENDPROC




func int get_total_number_of_cops_killed_stat()

      int player_0
      int player_1
      int player_2
      
      STAT_GET_INT(SP0_KILLS_COP, player_0) 
      STAT_GET_INT(SP1_KILLS_COP, player_1) 
      STAT_GET_INT(SP2_KILLS_COP, player_2) 

      return (player_0 + player_1 + player_2)

endfunc 




INT iPreviousNumberOfCopsKilled

FUNC BOOL HAS_COP_JUST_BEEN_KILLED()

	IF get_total_number_of_cops_killed_stat() > iPreviousNumberOfCopsKilled
		
		//The guys comment on the death of a cop
		IF (iPreviousNumberOfCopsKilled % 2 = 0) //Only do every second ped.
		AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
			INT randDialogue = GET_RANDOM_INT_IN_RANGE(0,2)
			
			SWITCH randDialogue
				CASE 0
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "RBH2AUD", "RBH2A_TCOPDE", CONV_PRIORITY_HIGH)
				BREAK
							
				CASE 1
				CASE 2
					IF NOT IS_ENTITY_DEAD(PedMichael())
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "RBH2AUD", "RBH2A_MCOPDE", CONV_PRIORITY_HIGH)
					ENDIF
				BREAK
			ENDSWITCH	
		ENDIF		
		
		INFORM_MISSION_STATS_OF_INCREMENT(RH2_KILLS)
		
		iPreviousNumberOfCopsKilled = get_total_number_of_cops_killed_stat()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC



// **********************************************************************
// *************************** MISSION STAGES ***************************
// **********************************************************************





PROC MANAGE_ALLIES_TASKS_BANK_HOLD_UP()
	SWITCH eAllyHoldUpState
		CASE ALLY_HOLD_UP_INIT
			CPRINTLN(DEBUG_MISSION, "MANAGE_ALLIES_TASKS_BANK_HOLD_UP - Going to ALLY_HOLD_UP_WALK_TO_BUSHES")
			eAllyHoldUpState = ALLY_HOLD_UP_WALK_TO_BUSHES
		BREAK
		
		CASE ALLY_HOLD_UP_WALK_TO_BUSHES
			IF NOT IS_PED_INJURED(PedMichael())
				SET_PED_SHOOT_RATE(PedMichael(),1000) 
				
				SET_PED_ACCURACY(PedMichael(), 10)
				OPEN_SEQUENCE_TASK(seqHoldOffCopsAtBank)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
						TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, <<-114.1661, 6449.8052, 30.4060>>, <<-152.7720, 6459.5894, 31.0824>>, PEDMOVE_WALK, TRUE, 0.5, 5, TRUE)
						TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, <<-122.6378, 6446.7070, 30.5289>>, <<-124.3652, 6432.3018, 32.0577>>, PEDMOVE_WALK, TRUE, 0.5, 5, TRUE)
						TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, <<-122.6378, 6446.7070, 30.5289>>, 2.0)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 150.00)
				CLOSE_SEQUENCE_TASK(seqHoldOffCopsAtBank)
				TASK_PERFORM_SEQUENCE(PedMichael(), seqHoldOffCopsAtBank)
				CLEAR_SEQUENCE_TASK(seqHoldOffCopsAtBank)
			ENDIF
			
			IF NOT IS_PED_INJURED(pedGunman)
				SET_PED_SHOOT_RATE(pedGunman,1000) 
				
				SET_PED_ACCURACY(pedGunman, 10)
				OPEN_SEQUENCE_TASK(seqHoldOffCopsAtBank)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, <<-123.9051, 6456.6616, 30.4685>>, <<-152.7720, 6459.5894, 31.0824>>, PEDMOVE_WALK, TRUE, 0.5, 5, TRUE)
					TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, <<-125.5384, 6450.8394, 30.5806>>, <<-152.7720, 6459.5894, 31.0824>>, PEDMOVE_WALK, TRUE, 0.5, 5, TRUE)
					TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, <<-125.5384, 6450.8394, 30.5806>>, 2.0)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 150.00)
				CLOSE_SEQUENCE_TASK(seqHoldOffCopsAtBank)
				TASK_PERFORM_SEQUENCE(pedGunman, seqHoldOffCopsAtBank)
				CLEAR_SEQUENCE_TASK(seqHoldOffCopsAtBank)
			ENDIF

			CPRINTLN(DEBUG_MISSION, "MANAGE_ALLIES_TASKS_BANK_HOLD_UP - Going to ALLY_HOLD_UP_WAIT_TO_MOVE_TO_MOTEL")
			eAllyHoldUpState = ALLY_HOLD_UP_WAIT_TO_MOVE_TO_MOTEL
		BREAK
		
		// We will task the AI's to move up if the player has hit a trigger area, or if they reach their destinations
		// and have been there for X seconds (currently 3)
		CASE ALLY_HOLD_UP_WAIT_TO_MOVE_TO_MOTEL
			// Check for the player to walk forward
			IF IS_PLAYER_IN_TRIGGER_BOX(tbChopperAndAllyMove)
				CPRINTLN(DEBUG_MISSION, "MANAGE_ALLIES_TASKS_BANK_HOLD_UP - Going to ALLY_HOLD_UP_TASK_TO_MOTEL")
				eAllyHoldUpState = ALLY_HOLD_UP_TASK_TO_MOTEL
			ENDIF
			
			// Check for the AI to reach their destinations and wait 3 seconds
			IF NOT IS_TIMER_STARTED(tmrChopperStart)
				// Wait for Michael to reach his destination
				IF NOT IS_PED_INJURED(pedMichael())
					IF GET_SCRIPT_TASK_STATUS(pedMichael(), SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
					AND GET_SEQUENCE_PROGRESS(pedMichael()) >= 3
						bMichaelInPositionAtBankLot = TRUE
					ENDIF
				ENDIF
				
				// Wait for the gunman to reach his destination
				IF NOT IS_PED_INJURED(pedGunman)
					IF GET_SCRIPT_TASK_STATUS(pedGunman, SCRIPT_TASK_PERFORM_SEQUENCE) = PERFORMING_TASK
					AND GET_SEQUENCE_PROGRESS(pedGunman) >= 3
						bGunmanInPositionAtBankLot = TRUE
					ENDIF
				ENDIF
				
				// Once both are in position, start the timer
				IF bMichaelInPositionAtBankLot AND bGunmanInPositionAtBankLot
					RESTART_TIMER_NOW(tmrChopperStart)
				ENDIF
			ELIF GET_TIMER_IN_SECONDS(tmrChopperStart) > 3.0
				// Once the timer's passed, move 'em up!
				CPRINTLN(DEBUG_MISSION, "MANAGE_ALLIES_TASKS_BANK_HOLD_UP - Going to ALLY_HOLD_UP_TASK_TO_MOTEL")
				eAllyHoldUpState = ALLY_HOLD_UP_TASK_TO_MOTEL
			ENDIF
		BREAK
		
		CASE ALLY_HOLD_UP_TASK_TO_MOTEL
			IF DOES_ENTITY_EXIST(PedMichael()) AND NOT IS_ENTITY_DEAD(PedMichael())
				REMOVE_PED_DEFENSIVE_AREA(PedMichael())
				SET_PED_COMBAT_MOVEMENT(PedMichael(), CM_STATIONARY)
				OPEN_SEQUENCE_TASK(seqHoldOffCopsAtBank)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, vAnimStartPosMichael, <<-152.7720, 6459.5894, 31.0824>>, PEDMOVE_WALK, TRUE, 0.5, 5, TRUE)
					TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, vAnimStartPosMichael, 2.0)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 150.00)
				CLOSE_SEQUENCE_TASK(seqHoldOffCopsAtBank)
				TASK_PERFORM_SEQUENCE(PedMichael(), seqHoldOffCopsAtBank)
				CLEAR_SEQUENCE_TASK(seqHoldOffCopsAtBank)
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedGunman) AND NOT IS_ENTITY_DEAD(pedGunman)
				REMOVE_PED_DEFENSIVE_AREA(pedGunman)
				SET_PED_COMBAT_MOVEMENT(pedGunman, CM_STATIONARY)
				OPEN_SEQUENCE_TASK(seqHoldOffCopsAtBank)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED_TIMED(NULL, 100.0, 1500)
					TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, vAnimStartPosGunman, <<-124.3652, 6432.3018, 32.0577>>, PEDMOVE_WALK, TRUE, 0.5, 5, TRUE)
					TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, vAnimStartPosGunman, 2.0)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 150.00)
				CLOSE_SEQUENCE_TASK(seqHoldOffCopsAtBank)
				TASK_PERFORM_SEQUENCE(pedGunman, seqHoldOffCopsAtBank)
				CLEAR_SEQUENCE_TASK(seqHoldOffCopsAtBank)
			ENDIF
			CPRINTLN(DEBUG_MISSION, "MANAGE_ALLIES_TASKS_BANK_HOLD_UP - Going to ALLY_HOLD_UP_WAIT_AT_DOORWAY")
			eAllyHoldUpState = ALLY_HOLD_UP_WAIT_AT_DOORWAY
		BREAK
		
		CASE ALLY_HOLD_UP_WAIT_AT_DOORWAY
			// Do nothing
		BREAK
	ENDSWITCH
ENDPROC

INT iActionDelay

PROC stageHoldOffCopsAtBank()

	PED_INDEX copWhoMentionsMiniGun

	doBuddyFailCheck()
	DISTANT_COP_CAR_SIRENS(TRUE)
	manageTyrePopping()
	
	HAS_COP_JUST_BEEN_KILLED()
	
	MANAGE_EXTRA_TYRE_POPS()
	
	THROW_BANK_NOTES_IN_AIR_WHEN_SHOT(PedMichael())
	
	MANAGE_GASSTATION_RAYFIRE()
	
	MANAGE_ALLIES_TASKS_BANK_HOLD_UP()
	
	checkForPlayerInputThenClearTAsks()
	
	IF DOES_ENTITY_EXIST(oiBankDoorRight)
		REMOVE_MODEL_HIDE(<<-109.65, 6462.11, 31.99>>, 10.0, V_ilev_bank4door01, FALSE)
		DELETE_OBJECT(oiBankDoorRight)
		SET_MODEL_AS_NO_LONGER_NEEDED(V_ilev_bank4door01)
	ENDIF
	
	DISABLE_VEHICLE_EXPLOSION_BREAK_OFF_PARTS()
	
	SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())

	INT i

	//Cant take cover with a Minigun.. //WAS NEVER RECORDED!
//	IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_TREVOR
//		IF (GET_GAME_TIMER() - iTimeOfMiniGunReminder) > 5000
//			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_COVER)
//				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_MINICANT", CONV_PRIORITY_VERY_HIGH)
//					iTimeOfMiniGunReminder = GET_GAME_TIMER()
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF


	PROCESS_MICHAEL_TREVOR_SWITCH()

	SWITCH iControlBuddiesStage1
	
		CASE 0
			
			g_replay.iReplayInt[REPLAY_VALUE_GAS_STATION_STATE] = 0
		
			bOnlyGiveWeaponsAtFirst = FALSE
		
			IF NOT IS_AUDIO_SCENE_ACTIVE("PS_2A_SHOOTOUT_START")
				START_AUDIO_SCENE("PS_2A_SHOOTOUT_START")
			ENDIF
		
			IF NOT IS_PED_INJURED(pedMichael())
				SET_PED_FIRING_PATTERN(pedMichael(), FIRING_PATTERN_BURST_FIRE)
			ENDIF
			IF NOT IS_PED_INJURED(pedGunman)
				SET_PED_FIRING_PATTERN(pedGunman, FIRING_PATTERN_BURST_FIRE)
			ENDIF
			
			iCurrentTake = RURAL_HEIST_TAKE
			
			SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
			
			pedSelector.pedID[SELECTOR_PED_MICHAEL] = PedMichael()			
			pedSelector.pedID[SELECTOR_PED_TREVOR] = PLAYER_PED_ID()
			
			//SET_SELECTOR_PED_HINT(pedSelector, SELECTOR_PED_MICHAEL, TRUE)
			//SET_SELECTOR_PED_HINT(pedSelector, SELECTOR_PED_TREVOR, TRUE)
			//SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_FRANKLIN, TRUE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_MICHAEL, FALSE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_TREVOR, FALSE)			
			
			SET_SELECTOR_PED_PRIORITY(pedSelector, SELECTOR_PED_MICHAEL, SELECTOR_PED_TREVOR, SELECTOR_PED_FRANKLIN)
			
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, copGroup, RELGROUPHASH_COP)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, copGroup)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_COP, robbersGroup)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, robbersGroup, RELGROUPHASH_COP)

			iInitialNumberOfCopsKilled = get_total_number_of_cops_killed_stat()
			iInitialNumberOfCopCarsExploded = GET_TOTAL_NUMBER_OF_COP_CARS_EXPLODED()
			eAllyHoldUpState = ALLY_HOLD_UP_INIT
			bMichaelInPositionAtBankLot = FALSE
			bGunmanInPositionAtBankLot = FALSE
			bCopsOutsideBankFled = FALSE
			CANCEL_TIMER(tmrChopperStart)
			
			REQUEST_PTFX_ASSET()
//			REQUEST_VEHICLE_RECORDING(1, "RBHHoldOff") 
//			REQUEST_VEHICLE_RECORDING(2, "RBHHoldOff") 
						
			//SoundIdAlarm = GET_SOUND_ID()
			//PLAY_SOUND_FROM_COORD(SoundIdAlarm,"Generic_Alarm_Burglar_Bell" , << -108.5419, 6465.4390, 30.6343 >>)
			
			START_ALARM("PALETO_BAY_SCORE_ALARM", TRUE)
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "STAGE_HOLD_OFF_COPS_AT_BANK")
			g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = iCurrentTake
			g_iPaletoScoreTake = iCurrentTake	//UPdate the saved take
			
			IF NOT IS_PED_INJURED(pedGunman)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGunman, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(PedMichael())
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedMichael(), TRUE)
			ENDIF
			
//				TASK_PLAY_ANIM(PLAYER_PED_ID(),  "WEAPONS@HEAVY@MINIGUN", "AIM_MED_LOOP", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 8000, AF_UPPERBODY | AF_SECONDARY)
//				SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
		
				IF NOT IS_PED_INJURED(pedGunman)
					TASK_AIM_GUN_AT_COORD(pedGunman, << -125.7645, 6447.5020, 31.5079 >>, 14000, TRUE)
				ENDIF
				IF NOT IS_PED_INJURED(PedMichael())
					TASK_AIM_GUN_AT_COORD(PedMichael(), << -125.7645, 6447.5020, 31.5079 >>, 14000, TRUE)
				ENDIF
			
			SET_PED_MAX_HEALTH(PLAYER_PED_ID(), 1000)
			SET_ENTITY_HEALTH(PLAYER_PED_ID(), 1000)
			
			cpTelephonePole = ADD_COVER_POINT(<< -127.0881, 6452.0928, 30.5795 >>, 103.1343, COVUSE_WALLTONEITHER, COVHEIGHT_HIGH, COVARC_90)
			
			cleanUpBeforeBankStages()
			
			//behind bank
			//avoidAreaOutsideBank = ADD_VEHICLE_COMBAT_ANGLED_AVOIDANCE_AREA(<<-67.231354,6465.333008,29.671114>>, <<-88.845146,6443.213867,33.725227>>, 37.250000)

			avoidAreaOutsideBank = ADD_VEHICLE_COMBAT_ANGLED_AVOIDANCE_AREA(<<-107.795914,6445.021973,30.097368>>, <<-138.961838,6474.645020,33.218407>>, 14.500000)
	
			SETUP_WANTED_LEVEL(5, TRUE, 100.0, 0.5, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
			
			iDispatchBlockingArea = ADD_DISPATCH_SPAWN_ANGLED_BLOCKING_AREA(<<-18.137819,6443.204102,27.175245>>, <<-126.315544,6549.449219,37.428650>>, 96.000000)
			
			SET_WANTED_RESPONSE_NUM_PEDS_TO_SPAWN(dt_police_automobile, 2)			
			SET_DISPATCH_IDEAL_SPAWN_DISTANCE(100.00)
			SET_DISPATCH_TIME_BETWEEN_SPAWN_ATTEMPTS(DT_POLICE_AUTOMOBILE, 2.5)
			
			CLOSE_BANK_DOORS(TRUE)
			SET_INSTANCE_PRIORITY_HINT( INSTANCE_HINT_SHOOTING )
			//Maybe take out..
			SET_DISPATCH_SPAWN_LOCATION( << -137.9621, 6445.5176, 30.5350 >>)
			
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBankParkedCars[0])
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBankParkedCars[1])
			SET_MODEL_AS_NO_LONGER_NEEDED(WASHINGTON)
			
			bMusicEvent1 = FALSE		
			SETTIMERA(0)
			IF IS_SCREEN_FADED_OUT()
				iActionDelay = 2250
			ELSE
				iActionDelay =1750
			ENDIF
			FADE_IN_IF_NEEDED()
			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 7, pedsCopsOutsideBank[5] , "Sherrif1")
			WAIT(0)
			CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_LOUDSP", CONV_PRIORITY_VERY_HIGH)
			iControlBuddiesStage1++
			
//			FOR i = 0 TO NUMBER_OF_COPS_OUTSIDE_BANK - 1
//			IF DOES_ENTITY_EXIST(pedsCopsOutsideBank[i])
//				IF NOT IS_PED_INJURED(pedsCopsOutsideBank[i])
//						TASK_COMBAT_HATED_TARGETS_IN_AREA(pedsCopsOutsideBank[i], GET_ENTITY_COORDS(PLAYER_PED_ID()), 100.0)
//						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedsCopsOutsideBank[i], TRUE)
//					ENDIF
//				ENDIF
//			ENDFOR
			
		BREAK
	
		CASE 1
		CASE 2
					
		
			IF IS_PED_SHOOTING(PLAYER_PED_ID())
			OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_AIM)
			OR TIMERA() > iActionDelay
				IF iControlBuddiesStage1 = 1
					FOR i = 0 TO NUMBER_OF_COPS_OUTSIDE_BANK - 1
						IF DOES_ENTITY_EXIST(pedsCopsOutsideBank[i])
							IF NOT IS_PED_INJURED(pedsCopsOutsideBank[i])
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsCopsOutsideBank[i], FALSE)		
								SET_PED_AS_NO_LONGER_NEEDED(pedsCopsOutsideBank[i])
							ENDIF	
						ENDIF
					ENDFOR
					SET_SKIP_MINIGUN_SPIN_UP_AUDIO(FALSE)
					REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, 7)						
					
					iControlBuddiesStage1 = 2
				ENDIF
			ENDIF
		
			
			//IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
			AND iControlBuddiesStage1 = 2
				PRINT_NOW("HOLDOFF", DEFAULT_GOD_TEXT_TIME, 1)
				iControlBuddiesStage1 = 3				
			ENDIF
			
		BREAK
		
		CASE 3
			IF bInitialHoldOffGodText = FALSE
			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_TRVHI", CONV_PRIORITY_VERY_HIGH)
				
	//				PRINT_STRING_IN_STRING("HOLDOFF", strCrewMember, DEFAULT_GOD_TEXT_TIME, 1)
					IF GET_RANDOM_BOOL()
						PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_PS_2A_01", 0.0)
					ELSE
						PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_PS_2A_02", 0.0)
					ENDIF
					
					iControlBuddiesStage1++
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
		CASE 5
				
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				GET_CLOSEST_PED(GET_ENTITY_COORDS(PLAYER_PED_ID()), 20.0, TRUE, TRUE, copWhoMentionsMiniGun, FALSE, TRUE)
				IF DOES_ENTITY_EXIST(copWhoMentionsMiniGun)
					IF NOT IS_ENTITY_DEAD(copWhoMentionsMiniGun)
						ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 8, copWhoMentionsMiniGun, "Sherrif2")
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "RBH2AUD", "RBH_MINIGUN2", CONV_PRIORITY_HIGH)
						iControlBuddiesStage1++
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
		
		CASE 6
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				//Objective reminder plays different one depending if you are Trevor or Michael.
				IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_TREVOR
					CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_REMIND1", CONV_PRIORITY_VERY_HIGH)
					iControlBuddiesStage1++
				ELSE
					CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_REMIND2", CONV_PRIORITY_VERY_HIGH)
					iControlBuddiesStage1++
				ENDIF
				
			ENDIF
		BREAK
		
		CASE 7
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				//{They're surrounded} - 1
				SWITCH (GET_FAILS_COUNT_WITHOUT_PROGRESS_FOR_THIS_MISSION_SCRIPT() % 3)
					CASE 0
						IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_SURR", CONV_PRIORITY_VERY_HIGH)
							iControlBuddiesStage1++
						ENDIF 
					BREAK
					CASE 1
						IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_SURR2", CONV_PRIORITY_VERY_HIGH)
							iControlBuddiesStage1++
						ENDIF 
					BREAK
					CASE 2
						IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_SURR3", CONV_PRIORITY_VERY_HIGH)
							iControlBuddiesStage1++
						ENDIF 
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		
		CASE 8
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_TREVOR
					CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_MCLEFT", CONV_PRIORITY_VERY_HIGH)
					iControlBuddiesStage1++
				ELSE
					CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_TLEFT", CONV_PRIORITY_VERY_HIGH)
					iControlBuddiesStage1++
				ENDIF
			ENDIF
		BREAK
		
		CASE 9
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_CHAT", CONV_PRIORITY_VERY_HIGH)
					iControlBuddiesStage1++
				ENDIF
				
			ENDIF
		BREAK
		
	ENDSWITCH


	IF bMusicEvent1 = FALSE
		IF PREPARE_MUSIC_EVENT("RH2A_FIGHT_START")
			IF TRIGGER_MUSIC_EVENT("RH2A_FIGHT_START")
				bMusicEvent1 = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	
	IF NOT IS_ENTITY_DEAD(setpieceIniitalCops1.thisCar)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(setpieceIniitalCops1.thisCar)
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setpieceIniitalCops1.thisCar, "RBHInitial") >= 97.0	
				STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(setpieceIniitalCops1)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(setpieceIniitalCops2.thisCar)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(setpieceIniitalCops2.thisCar)
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setpieceIniitalCops2.thisCar, "RBHInitial") >= 97.0	
				STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(setpieceIniitalCops2)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(setpieceIniitalCops3.thisCar)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(setpieceIniitalCops3.thisCar)
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setpieceIniitalCops3.thisCar, "RBHInitial") >= 97.0	
				STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(setpieceIniitalCops3)
			ENDIF
		ENDIF
	ENDIF
	
//	IF NOT IS_ENTITY_DEAD(setpieceIniitalCops4.thisCar)
//		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(setpieceIniitalCops4.thisCar)
//			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setpieceIniitalCops4.thisCar, "RBHInitial") >= 97.0	
//				STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(setpieceIniitalCops4)
//			ENDIF
//		ENDIF
//	ENDIF
	
	IF NOT IS_ENTITY_DEAD(setpieceIniitalCops5.thisCar)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(setpieceIniitalCops5.thisCar)
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setpieceIniitalCops5.thisCar, "RBHInitial") >= 96.0	
				STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(setpieceIniitalCops5)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(setpieceIniitalCops4.thisCar)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(setpieceIniitalCops4.thisCar)
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setpieceIniitalCops4.thisCar, "RBHInitial") >= 96.0	
				STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(setpieceIniitalCops4)
			ENDIF
		ENDIF
	ENDIF
	
	//Avoid pop in aiming after cutscene
	IF TIMERA() < 5000
		
		FOR i = 0 TO NUMBER_OF_COPS_OUTSIDE_BANK - 1
			IF DOES_ENTITY_EXIST(pedsCopsOutsideBank[i])
				IF NOT IS_PED_INJURED(pedsCopsOutsideBank[i])
					SET_PED_RESET_FLAG(pedsCopsOutsideBank[i], PRF_InstantBlendToAim, TRUE)
					//SCRIPT_ASSERT("bbiiing")
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	MANAGE_EXPLODING_CARS_AT_HOLD_OFF()
	CONTROL_WAR_CRIES_DIALOGUE()
			
	//IF iBodyCount > BODY_COUNT_TO_ADVANCE // 15

	iBodyCount = ( GET_TOTAL_NUMBER_OF_COP_CARS_EXPLODED() - iInitialNumberOfCopCarsExploded)
	
	// RJM - Bring in the chopper based entirely on the allies, or the player walking ahead
	/*
	IF TIMERA() > 15000
		IF ( GET_TOTAL_NUMBER_OF_COP_CARS_EXPLODED() - iInitialNumberOfCopCarsExploded) >= 7 //+ (get_total_number_of_cops_killed_stat() - iInitialNumberOfCopsKilled)  > 10
		OR (IS_PED_INJURED(setpieceIniitalCops1.thisDriver) AND IS_PED_INJURED(setpieceIniitalCops1.thisPassenger) //in case the other guys kill everyone
			AND IS_PED_INJURED(setpieceIniitalCops2.thisDriver) AND IS_PED_INJURED(setpieceIniitalCops2.thisPassenger)
			AND IS_PED_INJURED(setpieceIniitalCops3.thisDriver) AND IS_PED_INJURED(setpieceIniitalCops3.thisPassenger)
			AND IS_PED_INJURED(setpieceIniitalCops4.thisDriver) AND IS_PED_INJURED(setpieceIniitalCops4.thisPassenger)
			AND IS_PED_INJURED(setpieceIniitalCops5.thisDriver) AND IS_PED_INJURED(setpieceIniitalCops5.thisPassenger))
	*/
	IF IS_PLAYER_IN_TRIGGER_BOX(tbChopperAndAllyMove)
	OR (IS_TIMER_STARTED(tmrChopperStart) AND GET_TIMER_IN_SECONDS(tmrChopperStart) > 2.9)
	OR iBodyCount >= 9
		STOP_PLAYER_SWITCH()
		SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_MICHAEL, TRUE)
		SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_TREVOR, TRUE)
	ENDIF
	
	IF IS_PLAYER_IN_TRIGGER_BOX(tbChopperAndAllyMove)
	OR (IS_TIMER_STARTED(tmrChopperStart) AND GET_TIMER_IN_SECONDS(tmrChopperStart) > 3.0)
	OR iBodyCount >= 10
		//Make sure gas station isnt in the middle of exploding.
		REMOVE_COVER_POINT(cpTelephonePole)
		CLEANUP_SET_PIECE_COP_CAR(setpieceIniitalCops1)
		CLEANUP_SET_PIECE_COP_CAR(setpieceIniitalCops2)
		CLEANUP_SET_PIECE_COP_CAR(setpieceIniitalCops3)
		CLEANUP_SET_PIECE_COP_CAR(setpieceIniitalCops4)
		CLEANUP_SET_PIECE_COP_CAR(setpieceIniitalCops5)
		REMOVE_VEHICLE_RECORDING(1, "RBHInitial")
		REMOVE_VEHICLE_RECORDING(2, "RBHInitial")
		REMOVE_VEHICLE_RECORDING(3, "RBHInitial")
		REMOVE_VEHICLE_RECORDING(6, "RBHInitial")
		REMOVE_VEHICLE_RECORDING(7, "RBHInitial")
		
		
		mission_stage = STAGE_CHOPPER_TURNS_UP

	ENDIF
	
ENDPROC


PROC stageChopperTurnsUp()

	DISTANT_COP_CAR_SIRENS(TRUE)

	MANAGE_GASSTATION_RAYFIRE()
	
//	IF iChopperStage < 6
//		PROCESS_MICHAEL_TREVOR_SWITCH()
//	ENDIF

	MANAGE_ALLIES_TASKS_BANK_HOLD_UP()
	MANAGE_EXPLODING_CARS_AT_HOLD_OFF()
	
	IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
		doBuddyFailCheck()
	ENDIF
	THROW_BANK_NOTES_IN_AIR_WHEN_SHOT(PedMichael(), 2000, 1000, 5000)
	
	SET_USE_HI_DOF()
	
	// url:bugstar:2053916
	IF iChopperStage >= 9
		DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
	ENDIF
	
	IF iChopperStage > 1
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2222112
	ENDIF
	
	SWITCH iChopperStage
	
		CASE 0
			STOP_PLAYER_SWITCH()
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("PS_2A_SHOOTOUT_START")
				START_AUDIO_SCENE("PS_2A_SHOOTOUT_START")
			ENDIF
			
			START_AUDIO_SCENE("PS_2A_SHOOTOUT_HELI_ARRIVES")
			
			bPlayerAtHotelDoorEarly = FALSE
			SETUP_WANTED_LEVEL(5, TRUE, 100.0, 10.0, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
			removeAllBlipsCops1()
//			IF NOT IS_ENTITY_DEAD(truckTankerTrailer)
//				SET_ENTITY_HEALTH(truckTankerTrailer, 1000)
//			ENDIF
			SET_PED_AS_NO_LONGER_NEEDED(runnerCops1[0])
			SET_PED_AS_NO_LONGER_NEEDED(runnerCops1[1])
			SET_PED_AS_NO_LONGER_NEEDED(runnerCops1[2])
			
			REQUEST_MODEL(POLMAV)
			REQUEST_VEHICLE_RECORDING(4, "ChopBank")
			REQUEST_MODEL(mnSwatPedModel)
			
			REQUEST_ANIM_DICT("missheistpaletoscore2chopper_crash")
	
			//REQUEST_IPL("bnkheist_apt_dest")
			
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("PALETO_BAY_SCORE_ALARM")
			
			//REQUEST_VEHICLE_RECORDING(1, "RBHHeliCrash")
			FADE_IN_IF_NEEDED()
			
			iChopperStage++					
		BREAK
		
				
		CASE 1
			STOP_PLAYER_SWITCH()
			IF HAS_MODEL_LOADED(POLMAV)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(4, "ChopBank")
			//AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "ChopBank")
			AND HAS_MODEL_LOADED(mnSwatPedModel)
			AND HAS_ANIM_DICT_LOADED("missheistpaletoscore2chopper_crash")
			AND LOAD_STREAM("PALETO_SCORE_HELICOPTER_CRASH_MASTER")
			//AND HAS_CUTSCENE_LOADED()
			AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()		//1749800				
				SETTIMERB(0)
							
				iChopperStage++	
			ENDIF
		BREAK
	
				
		CASE 2
			STOP_PLAYER_SWITCH()
			chopperCops1 = CREATE_VEHICLE(POLMAV, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(4, 3000.0, "ChopBank"))
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(chopperCops1, TRUE)
			SET_VEHICLE_LIVERY(chopperCops1, 0)
			SET_HELI_BLADES_FULL_SPEED(chopperCops1)
			//SET_ENTITY_PROOFS(chopperCops1, TRUE, TRUE, TRUE, TRUE, TRUE)
			//SET_ENTITY_INVINCIBLE(chopperCops1, TRUE)
			SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(chopperCops1, FALSE)
		
			SET_VEHICLE_CAN_BREAK(chopperCops1, FALSE)
		
			SET_ENTITY_HEALTH(chopperCops1, 2000)
					
			SET_VEHICLE_ENGINE_HEALTH(chopperCops1, 2000.0)
			SET_VEHICLE_PETROL_TANK_HEALTH(chopperCops1, 2000.0)
			
			SET_ENTITY_PROOFS(chopperCops1, FALSE, TRUE, FALSE, TRUE, TRUE)
			SET_ENTITY_INVINCIBLE(chopperCops1, FALSE)
		
			PilotChopperCops1 = CREATE_PED_INSIDE_VEHICLE(chopperCops1, PEDTYPE_COP, mnSwatPedModel )
			pass1ChopperCops1 = CREATE_PED_INSIDE_VEHICLE(chopperCops1, PEDTYPE_COP, mnSwatPedModel, VS_BACK_LEFT )
			pass2ChopperCops1 = CREATE_PED_INSIDE_VEHICLE(chopperCops1, PEDTYPE_COP, mnSwatPedModel, VS_BACK_RIGHT )
			
			SET_PED_CONFIG_FLAG(PilotChopperCops1, PCF_DontBlipCop, TRUE)
			SET_PED_CONFIG_FLAG(pass1ChopperCops1, PCF_DontBlipCop, TRUE)
			SET_PED_CONFIG_FLAG(pass2ChopperCops1, PCF_DontBlipCop, TRUE)
			
			SET_PED_AS_DEFENSIVE_COP(pass2ChopperCops1, WEAPONTYPE_CARBINERIFLE, TRUE)
			SET_PED_AS_DEFENSIVE_COP(pass1ChopperCops1, WEAPONTYPE_CARBINERIFLE, TRUE)
			
			IF IS_ENTITY_OK(PilotChopperCops1)
	            GIVE_WEAPON_TO_PED(pass1ChopperCops1, WEAPONTYPE_CARBINERIFLE, INFINITE_AMMO, FALSE)
	            //GIVE_WEAPON_COMPONENT_TO_PED(pass1ChopperCops1, WEAPONTYPE_CARBINERIFLE, WEAPONCOMPONENT_AT_AR_LASR)
	            SET_PED_HEARING_RANGE(pass1ChopperCops1, 150) 
	            SET_PED_SEEING_RANGE(pass1ChopperCops1, 150)
	            SET_PED_ID_RANGE(pass1ChopperCops1, 150)
	  			SET_PED_HIGHLY_PERCEPTIVE(pass1ChopperCops1, TRUE)
				//SET_ENTITY_PROOFS(PilotChopperCops1, TRUE, TRUE, TRUE, TRUE, TRUE)
				
				SET_PED_ARMOUR(pass1ChopperCops1, 100)
				
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(PilotChopperCops1, TRUE)
				
			ENDIF

			IF IS_ENTITY_OK(pass2ChopperCops1)
				GIVE_WEAPON_TO_PED(pass2ChopperCops1, WEAPONTYPE_CARBINERIFLE, INFINITE_AMMO, FALSE)
	            //GIVE_WEAPON_COMPONENT_TO_PED(pass2ChopperCops1, WEAPONTYPE_CARBINERIFLE, WEAPONCOMPONENT_AT_AR_LASR)
	            SET_PED_HEARING_RANGE(pass2ChopperCops1, 150) 
	            SET_PED_SEEING_RANGE(pass2ChopperCops1, 150)
	            SET_PED_ID_RANGE(pass2ChopperCops1, 150)
	  			SET_PED_HIGHLY_PERCEPTIVE(pass2ChopperCops1, TRUE)
				//SET_ENTITY_PROOFS(pass2ChopperCops1, TRUE, TRUE, TRUE, TRUE, TRUE)
				
				SET_PED_ARMOUR(pass2ChopperCops1, 100)
				SET_ENTITY_PROOFS(chopperCops1, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pass2ChopperCops1, TRUE)
				
			ENDIF
			
			IF IS_ENTITY_OK(pass1ChopperCops1)
				//SET_ENTITY_PROOFS(pass1ChopperCops1, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_PED_ARMOUR(pass1ChopperCops1, 100)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pass1ChopperCops1, TRUE)
				
			ENDIF
			
			//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pass1ChopperCops1, TRUE)
			//SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pass2ChopperCops1, TRUE)
			TASK_DRIVE_BY(pass1ChopperCops1, PLAYER_PED_ID(), NULL, <<0.0, 0.0, 0.0>>, 200.0, 100, FALSE, FIRING_PATTERN_FULL_AUTO)
			TASK_DRIVE_BY(pass2ChopperCops1, PLAYER_PED_ID(), NULL,  <<0.0, 0.0, 0.0>>, 200.0, 100, FALSE, FIRING_PATTERN_FULL_AUTO)
			
			SET_PED_CAN_BE_TARGETTED(PilotChopperCops1, FALSE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PilotChopperCops1, TRUE)
			
				
			START_PLAYBACK_RECORDED_VEHICLE(chopperCops1, 4, "ChopBank")
			SET_PLAYBACK_SPEED(chopperCops1, 1.50)
			SET_VEHICLE_RECORDING_PLAYBACK_POSITION(chopperCops1, 5000)			
			FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(chopperCops1)
			SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(chopperCops1, (<< -1, 0, -2>>))
			IF NOT IS_ENTITY_DEAD(chopperCops1)
				SET_ENTITY_VISIBLE(chopperCops1, TRUE)
			ENDIF	
			
			IF DOES_ENTITY_EXIST(PedMichael()) AND NOT IS_ENTITY_DEAD(PedMichael())
				REMOVE_PED_DEFENSIVE_AREA(PedMichael())
				SET_PED_COMBAT_MOVEMENT(PedMichael(), CM_STATIONARY)
				OPEN_SEQUENCE_TASK(seqHoldOffCopsAtBank)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, vAnimStartPosMichael, <<-152.7720, 6459.5894, 31.0824>>, PEDMOVE_WALK, TRUE, 0.5, 5, TRUE)
					TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, vAnimStartPosMichael, 2.0)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 150.00)
				CLOSE_SEQUENCE_TASK(seqHoldOffCopsAtBank)
				TASK_PERFORM_SEQUENCE(PedMichael(), seqHoldOffCopsAtBank)
				CLEAR_SEQUENCE_TASK(seqHoldOffCopsAtBank)
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedGunman) AND NOT IS_ENTITY_DEAD(pedGunman)
				REMOVE_PED_DEFENSIVE_AREA(pedGunman)
				SET_PED_COMBAT_MOVEMENT(pedGunman, CM_STATIONARY)
				OPEN_SEQUENCE_TASK(seqHoldOffCopsAtBank)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
					TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, vAnimStartPosGunman, <<-124.3652, 6432.3018, 32.0577>>, PEDMOVE_WALK, TRUE, 0.5, 5, TRUE)
					TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, vAnimStartPosGunman, 2.0)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 150.00)
				CLOSE_SEQUENCE_TASK(seqHoldOffCopsAtBank)
				TASK_PERFORM_SEQUENCE(pedGunman, seqHoldOffCopsAtBank)
				CLEAR_SEQUENCE_TASK(seqHoldOffCopsAtBank)
			ENDIF
			
			iChopperStage++	
		BREAK
		
		CASE 3
			//IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_CHPSHOOT", CONV_PRIORITY_VERY_HIGH)
			IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_TCHOP", CONV_PRIORITY_VERY_HIGH)
					iChopperStage++		
				ENDIF
			ELSE
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_CHOPPE", CONV_PRIORITY_VERY_HIGH)
					iChopperStage++		
				ENDIF
			ENDIF
		BREAK
		
		
		CASE 4
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_TCHOP2", CONV_PRIORITY_VERY_HIGH)
						iChopperStage++		
					ENDIF
				ELSE
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_MCHOP", CONV_PRIORITY_VERY_HIGH)
						iChopperStage++		
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF) //TIMERA() > 3000
				
				IF NOT IS_ENTITY_DEAD(chopperCops1)
					SET_ENTITY_PROOFS(chopperCops1, FALSE, TRUE, FALSE, TRUE, TRUE)
					SET_VEHICLE_CAN_BE_TARGETTED(chopperCops1, TRUE)
					SET_ENTITY_IS_TARGET_PRIORITY(chopperCops1, TRUE)	
				ENDIF			
								
				PRINT_NOW("CHOPPER3", DEFAULT_GOD_TEXT_TIME, 1)
				blipChopperCops1 = CREATE_BLIP_FOR_VEHICLE(chopperCops1)
				SET_BLIP_AS_FRIENDLY(blipChopperCops1, FALSE)

				SETTIMERA(0)
				iChopperStage++
			ENDIF
			
		BREAK
		
		CASE 6
			IF NOT bPlayerAtHotelDoorEarly
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), (<<-145.5280, 6436.0591, 30.4324>>), (<<3, 3, 3>>))
//					SET_ENTITY_HEALTH(chopperCops1, 710)
//					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(chopperCops1, FALSE)
					bPlayerAtHotelDoorEarly = TRUE
				ENDIF
			ENDIF
		
			IF TIMERA() > 4000
			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)	
				IF GET_RANDOM_INT_IN_RANGE(0, 10) > 3
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_CHPSHOOT", CONV_PRIORITY_VERY_HIGH)
						//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedMichael(), "RBH2A_GGAA", "MICHAEL", SPEECH_PARAMS_FORCE_FRONTEND)	
						SETTIMERA(0)
					ENDIF
				ELSE
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_TCHOP1", CONV_PRIORITY_VERY_HIGH)
						//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedMichael(), "RBH2A_GGAA", "MICHAEL", SPEECH_PARAMS_FORCE_FRONTEND)	
						SETTIMERA(0)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(chopperCops1)
				
				IF GET_VEHICLE_ENGINE_HEALTH(chopperCops1) < 200.0
					SET_VEHICLE_ENGINE_HEALTH(chopperCops1, 200.0)
				ENDIF
				
				IF GET_VEHICLE_PETROL_TANK_HEALTH(chopperCops1) < 50.0
					SET_VEHICLE_PETROL_TANK_HEALTH(chopperCops1, 50.0)
				ENDIF
				
				IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chopperCops1, "ChopBank") > 99.9
				AND ( (NOT IS_PED_INJURED(pass1ChopperCops1))
					OR (NOT IS_PED_INJURED(pass2ChopperCops1)))
					
					IF NOT IS_PED_INJURED(PedMichael())
						SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_ENTITY_COORDS(chopperCops1) + <<0.0, 0.0, -2.0>>, GET_PED_BONE_COORDS(pedsCopsInStreet[0], BONETAG_HEAD, <<0.0, 0.0, 0.0>>), 25, TRUE)
						APPLY_DAMAGE_TO_PED(PedMichael(), 1000, TRUE)
					ENDIF
					SET_ENTITY_HEALTH(PedMichael(), 0)
				ENDIF
				
				SET_HELI_BLADES_FULL_SPEED(chopperCops1)
				
				IF IS_SPECIAL_ABILITY_ACTIVE(PLAYER_ID())
					SET_VEHICLE_STRONG(chopperCops1, TRUE)
				ELSE
					SET_VEHICLE_STRONG(chopperCops1, FALSE)
				ENDIF
				
				IF GET_ENTITY_HEALTH(chopperCops1) < 1300
				OR GET_VEHICLE_PETROL_TANK_HEALTH(chopperCops1) < 50.0
				OR GET_VEHICLE_ENGINE_HEALTH(chopperCops1) < 200.0
				OR GET_HELI_MAIN_ROTOR_HEALTH(chopperCops1) < 200.0
				OR GET_HELI_TAIL_ROTOR_HEALTH(chopperCops1) < 200.0
				OR GET_HELI_TAIL_BOOM_HEALTH(chopperCops1) < 200.0
				OR IS_PED_INJURED(PilotChopperCops1)
				OR (IS_PED_INJURED(pass1ChopperCops1) AND IS_PED_INJURED(pass2ChopperCops1))
				OR (NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(chopperCops1))
				
					IF NOT IS_ENTITY_DEAD(chopperCops1)
						SET_ENTITY_INVINCIBLE(chopperCops1, TRUE)
						SET_VEHICLE_ENGINE_CAN_DEGRADE(chopperCops1, FALSE)
					ENDIF
					
					iChopperStage++
				ENDIF
			ELSE
				PRINTLN("heli died before cutscene")
				iChopperStage++
			ENDIF
		BREAK
		
		CASE 7
			REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipChopperCops1)
			
			IF NOT IS_ENTITY_DEAD(chopperCops1)		
				SET_ENTITY_INVINCIBLE(chopperCops1, TRUE)
				SET_ENTITY_PROOFS(chopperCops1, TRUE, TRUE, TRUE, TRUE, TRUE)
				SET_VEHICLE_ENGINE_HEALTH(chopperCops1, 40.0)
				
				
			ENDIF				
			
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SETTIMERA(0)		
			
			CANCEL_MUSIC_EVENT("RH2A_SHOOT_TANK")
			iChopperStage= 8
		BREAK
						
		CASE 8
			IF IS_ENTITY_DEAD(chopperCops1)
				DELETE_VEHICLE(chopperCops1)
				chopperCops1 = CREATE_VEHICLE(POLMAV, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(4, 3000.0, "ChopBank"))
				SET_VEHICLE_LIVERY(chopperCops1, 0)
				SET_HELI_BLADES_FULL_SPEED(chopperCops1)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(chopperCops1)
				
				REQUEST_ADVANCE_TO_GARDEN_ASSETS()
				STOP_PLAYBACK_RECORDED_VEHICLE(chopperCops1)			
				iHeliCrashScene = CREATE_SYNCHRONIZED_SCENE(<< -132.363, 6421.871, 38.697 >>, << -0.000, 0.000, 45.000 >>)
				
				PLAY_SYNCHRONIZED_ENTITY_ANIM(chopperCops1, iHeliCrashScene, "Chopper_Crash", "missheistpaletoscore2chopper_crash", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				SET_SYNCHRONIZED_SCENE_LOOPED(iHeliCrashScene, FALSE)
				
				//SET_SYNCHRONIZED_SCENE_PHASE(iHeliCrashScene, 0.19)
				
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(chopperCops1) 
				
				SETTIMERA(0)
								
				CLEAR_PRINTS()
				CLEAR_HELP(TRUE)
				
				IF NOT IS_ENTITY_DEAD(CopCarsParked[0])
					EXPLODE_VEHICLE(CopCarsParked[0])
				ENDIF

				SET_CAM_COORD(initialCam,<<-134.638474,6412.690430,55.177444>>)
				SET_CAM_ROT(initialCam,<<-30.838787,12.298615,-33.723751>>)
				SET_CAM_FOV(initialCam,26.445715)
				
				SET_CAM_COORD(destinationCam,<<-139.205719,6411.244629,53.496490>>)
				SET_CAM_ROT(destinationCam,<<-26.765345,17.378477,-56.458050>>)
				SET_CAM_FOV(destinationCam,26.445715)
				
				SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 3000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				
				SHAKE_CAM(destinationCam, "HAND_SHAKE", 2.5)
				SET_CLOCK_TIME(18, 0, 0)
				SET_CUTSCENE_RUNNING(TRUE)
				
				//Clean up way a little bit.
				CLEANUP_SET_PIECE_COP_CAR(setpieceIniitalCops5, TRUE)
				DELETE_VEHICLE(CopCarsParked[0])
								
				REMOVE_VEHICLE_RECORDING(4, "ChopBank")
				
				TASK_AIM_GUN_AT_ENTITY(PLAYER_PED_ID(), chopperCops1, 3000)
		
				STOP_AUDIO_SCENE("PS_2A_SHOOTOUT_HELI_ARRIVES")
				
				START_AUDIO_SCENE("PS_2A_HELI_CRASH")
				
				PLAY_STREAM_FRONTEND()
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				
				RESET_GAME_CAMERA()
				//See if we can get away with cleaning up some dead bodies.
				CLEAR_AREA_OF_PEDS(<<-138.5488, 6442.2070, 30.4359>>, 50.0)
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				// url:bugstar:2053916
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
				
				iChopperStage++
				
			ENDIF
		BREAK
		
		CASE 9
			IF PREPARE_MUSIC_EVENT("RH2A_SHOOT_TANK")
				TRIGGER_MUSIC_EVENT("RH2A_SHOOT_TANK")
				iChopperStage++
			ELSE
				CPRINTLN(DEBUG_MISSION, "WAITING FOR MUSIC TO LOAD")
			ENDIF
		BREAK
		
		CASE 10
			IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_COME", CONV_PRIORITY_VERY_HIGH)
				iChopperStage++		
			ENDIF	
		BREAK
		
		CASE 11
			IF GET_SYNCHRONIZED_SCENE_PHASE(iHeliCrashScene) > 0.22
				IF NOT IS_ENTITY_DEAD(chopperCops1)
					SET_VEHICLE_ENGINE_HEALTH(chopperCops1, 1.0)
					SET_VEHICLE_PETROL_TANK_HEALTH(chopperCops1, 1.0)
				ENDIF
				iChopperStage++
				IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON)						
					ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
					PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
				ENDIF
			ENDIF

		BREAK
						
		CASE 12
			IF GET_SYNCHRONIZED_SCENE_PHASE(iHeliCrashScene) > 0.29
			OR TIMERA() >= 3000 //1600
//				SET_CAM_COORD(initialCam,<<-161.958282,6387.369141,61.945179>>)
//				SET_CAM_ROT(initialCam,<<-26.258224,-6.260342,-25.405045>>)
//				SET_CAM_FOV(initialCam,28.025579)
//
//				SET_CAM_COORD(destinationCam,<<-156.931091,6385.398438,62.476128>>)
//				SET_CAM_ROT(destinationCam,<<-27.172033,-6.260342,-22.918591>>)
//				SET_CAM_FOV(destinationCam,28.025579)
//			
//				SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 2000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
//				
//				SHAKE_CAM(destinationCam, "HAND_SHAKE", 1.5)
				
				IF NOT IS_ENTITY_DEAD(pass1ChopperCops1)
					SET_ENTITY_HEALTH(pass1ChopperCops1, 0)
				ENDIF
				IF NOT IS_ENTITY_DEAD(pass2ChopperCops1)
					SET_ENTITY_HEALTH(pass2ChopperCops1, 0)
				ENDIF
				IF NOT IS_ENTITY_DEAD(PilotChopperCops1)
					SET_ENTITY_HEALTH(PilotChopperCops1, 0)
				ENDIF
				
				REPLAY_STOP_EVENT()
				
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 8.0)
				
				SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_MICHAEL, FALSE)
				SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_TREVOR, FALSE)
				
				mission_stage = STAGE_ADVANCE_TO_GARDENS
				
				iChopperStage++
			ENDIF
		BREAK
						
	ENDSWITCH	
	
	FLOAT fHeliCrashPhase
	
	IF IS_SYNCHRONIZED_SCENE_RUNNING(iHeliCrashScene)
		fHeliCrashPhase = GET_SYNCHRONIZED_SCENE_PHASE(iHeliCrashScene)
	ELSE
		//fHeliCrashPhase = 1.5
	ENDIF
	
	MANAGE_MOTEL_RAYFIRE(fHeliCrashPhase)
	
ENDPROC


PROC CLEANUP_CONTINUE_DOWN_STREET()

	CLEANUP_SET_PIECE_COP_CAR(copCarsPullingUpStage2[0])
	CLEANUP_SET_PIECE_COP_CAR(copCarsPullingUpStage2[1])
			
	CLEANUP_SET_PIECE_COP_CAR(copCarsBlocking[0])
	CLEANUP_SET_PIECE_COP_CAR(copCarsBlocking[1])
	
	CLEANUP_SET_PIECE_COP_CAR(carsPoliceBackup[0])
	CLEANUP_SET_PIECE_COP_CAR(carsPoliceBackup[1])
	
	CLEANUP_SET_PIECE_COP_CAR(carsPolicePoolside[0])
	CLEANUP_SET_PIECE_COP_CAR(carsPolicePoolside[1])
	CLEANUP_SET_PIECE_COP_CAR(carsPolicePoolside[2])
	
	CLEANUP_COPS_AND_CARS_OUTSIDE_BANK()
	CLEANUP_COPS_AND_CARS_DOWN_STREET(FALSE)
	
	CLEANUP_SET_PIECE_COP_CAR(copCarsPullingUpStage2[4])
	
	DELETE_ARRAY_OF_PEDS(stage2ExtraRunners)
							
	REMOVE_ANIM_DICT("MISSHeistPaletoScore2")
	
	CLEANUP_SET_PIECE_COP_CAR(chopperFlyover)
						
	SET_MODEL_AS_NO_LONGER_NEEDED(POLMAV)
	SET_MODEL_AS_NO_LONGER_NEEDED(RIOT)
	SET_MODEL_AS_NO_LONGER_NEEDED(mnCopCarModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(mnCopPedModel)
		
ENDPROC


FUNC  BOOL IS_A_BUDDY_VISIBLE()

	IF IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(pedGunman), 0.5) 
	OR IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(PedMichael()), 0.5) 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC






CONST_INT TAKE_LOST_BY_GUNMAN 2000000

PROC PICKUP_BUDDYS_BAG()

	//IF DOES_BLIP_EXIST(blipBuddysBag)
	IF DOES_ENTITY_EXIST(oiBuddysBag)
		//Collect the bag
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), oiBuddysBag, <<1.0, 1.0, 1.0>>)
						
			DELETE_OBJECT(oiBuddysBag)
			REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipBuddysBag)
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 500, 255)
			//Add the money back on
			iCurrentTake = iCurrentTake + TAKE_LOST_BY_GUNMAN
			
			IF IS_PLAYER_TREVOR()
				ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "RBH2AUD", "RBH2A_GRABT", CONV_PRIORITY_VERY_HIGH)
			ELSE
				ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "RBH2AUD", "RBH2A_GRABM", CONV_PRIORITY_VERY_HIGH)
			ENDIF
			
			g_replay.iReplayInt[REPLAY_VALUE_PICKEDUP_BUDDYS_BAG] = 1
			
		ELSE
			//Remove it if you go too far
			IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), oiBuddysBag, <<35.0, 35.0, 2.0>>)
				DELETE_OBJECT(oiBuddysBag)
				SET_MODEL_AS_NO_LONGER_NEEDED(P_LD_HEIST_BAG_S_1)
				REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipBuddysBag)
			ENDIF
		ENDIF		
	ENDIF
		
ENDPROC

//PROC RUBBER_BAND_WAYPOINT_INTO_JUNKYARD(FLOAT fWalkDistance = 9.0, FLOAT fPauseDistance = 15.0)
//
//	
//	IF bLosingMichael	
//		
//	ELSE
//		
//	ENDIF
//	
//	
//	IF NOT IS_PED_INJURED(PedMichael())
//	AND NOT bIsBuddyAPro
//		
//		FLOAT fDistanceMichael= GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(PedMichael()))
//		
//		INT iClosestWaypointToPlayer
//		INT iClosestWaypointToMichael
//		WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT( "RBHArmySec2", GET_ENTITY_COORDS(PLAYER_PED_ID()), iClosestWaypointToPlayer)
//		WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT( "RBHArmySec2", GET_ENTITY_COORDS(PedMichael()), iClosestWaypointToMichael)
//		
//		IF fDistanceMichael > fWalkDistance
//		AND iClosestWaypointToMichael >  iClosestWaypointToPlayer
//			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
//				WAYPOINT_PLAYBACK_OVERRIDE_SPEED(PedMichael(), PEDMOVE_WALK)
//			ENDIF
//			
//			IF NOT bDisplayLosingGodText
//				IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL
//					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_SLWDWNT", CONV_PRIORITY_VERY_HIGH)
//						bDisplayLosingGodText = TRUE
//					ENDIF
//				ELSE
//					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_SLWDWNM", CONV_PRIORITY_VERY_HIGH)
//						bDisplayLosingGodText = TRUE
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			#IF IS_DEBUG_BUILD
//				IF bIsSuperDebugEnabled
//					DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.22, "STRING", "PEDMOVE_WALK MICHAEL")
//				ENDIF
//			#ENDIF
//		ELSE		
//			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
//				WAYPOINT_PLAYBACK_OVERRIDE_SPEED(PedMichael(), PEDMOVE_RUN)
//			ENDIF
//			
//			bDisplayLosingGodText = FALSE
//			
//			#IF IS_DEBUG_BUILD
//				IF bIsSuperDebugEnabled
//					DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.27, "STRING", "PEDMOVE_RUN MICHAEL")
//				ENDIF
//			#ENDIF
//		ENDIF
//		
//		IF fDistanceMichael > fPauseDistance
//		AND (iClosestWaypointToMichael >  iClosestWaypointToPlayer)
//			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
//				IF NOT IS_ENTITY_DEAD(PedMichael())
//					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedMichael(), FALSE)
//					SET_PED_SPHERE_DEFENSIVE_AREA(PedMichael(), GET_ENTITY_COORDS(PedMichael()), 1.5)
//					TASK_COMBAT_HATED_TARGETS_AROUND_PED(PedMichael(), 120.0)
//				ENDIF
//			ENDIF
//			
//			bLosingMichael = TRUE
//			
//			#IF IS_DEBUG_BUILD
//				IF bIsSuperDebugEnabled
//					DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.32, "STRING", "Pause: MICHAEL")
//				ENDIF
//			#ENDIF
//		ELIF fDistanceMichael < (fPauseDistance * 0.9)
//			
//			bLosingMichael = FALSE
//			
//			IF NOT IS_PED_DOING_TASK(PedMichael(), SCRIPT_TASK_PERFORM_SEQUENCE)
//				IF NOT IS_ENTITY_DEAD(PedMichael())
//					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedMichael(), TRUE)
//					OPEN_SEQUENCE_TASK(seqShootThroughBigFence)		
//						TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHArmySec2", 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
//						TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, << -170.2119, 6288.2974, 30.4905 >>, 1.0)
//						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 120.0)
//					CLOSE_SEQUENCE_TASK(seqShootThroughBigFence)
//					TASK_PERFORM_SEQUENCE(PedMichael(), seqShootThroughBigFence)
//					CLEAR_SEQUENCE_TASK(seqShootThroughBigFence)
//				ENDIF
//			ENDIF
//				
//			#IF IS_DEBUG_BUILD
//				IF bIsSuperDebugEnabled
//					DISPLAY_TEXT_WITH_LITERAL_STRING(0.5, 0.36, "STRING", "Unpause: MICHAEL")
//				ENDIF
//			#ENDIF
//		ENDIF
//	ENDIF
//
//	IF NOT IS_PED_INJURED(PedMichael())
//		IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
//			
//			#IF IS_DEBUG_BUILD IF bIsSuperDebugEnabled
//				DRAW_DEBUG_TEXT_ABOVE_ENTITY(PedMichael(), GET_STRING_FROM_INT(iShootingStageMichael), -0.5, 128, 128, 0)
//			ENDIF #ENDIF
//			
//			iShootingStageMichael = GET_PED_WAYPOINT_PROGRESS(PedMichael())
//			
//					
//			SWITCH iShootingSwitchM
//				
//				CASE 0
//					//SCRIPT_ASSERT("boom time")
//					IF iShootingStageMichael >= 0
//						WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(PedMichael(), << -128.5561, 6299.9170, 32.3576 >> , FALSE)		 
//						iShootingSwitchM++
//					ENDIF
//				BREAK
//				
//				CASE 1
//					IF iShootingStageMichael >= 6
//						WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(PedMichael(), << -179.5631, 6283.9448, 31.7902 >> , FALSE)
//						pedToShootM = NULL
//						iShootingSwitchM++
//					ENDIF
//				BREAK
//				
//				CASE 2
//					IF iShootingStageMichael >= 20
//						GET_CLOSEST_PED(<< -179.5631, 6283.9448, 31.7902 >>, 75.0, TRUE, TRUE, pedToShootM, FALSE, TRUE)
//				
//						IF DOES_ENTITY_EXIST(pedToShootM)
//							WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(PedMichael(),GET_ENTITY_COORDS(pedToShootM, FALSE) , FALSE)						
//							iShootingSwitchM++
//						ENDIF
//					ENDIF
//				BREAK
//								
//				CASE 3
//					
//					IF NOT DOES_ENTITY_EXIST(pedToShootM)
//					OR IS_PED_INJURED(pedToShootM)
//					OR IS_PED_HURT(pedToShootM)
//						iShootingSwitchM = 2
//					ENDIF
//				BREAK
//								
//			ENDSWITCH
//			
//		ENDIF
//	ENDIF
//
//
//ENDPROC

//OBJECT_INDEX oiAlleyCutWeaponGunman
//OBJECT_INDEX oiAlleyCutWeaponMichael

BOOL bCutsceneClearingDone

PROC stageProceedToAlley()
	doBuddyFailCheck()

	KEEP_TIME_LOOKING_SWEET()

	PRINTINT(TIMERA()) PRINTNL()

	SET_USE_HI_DOF()
	
	DISABLE_SELECTOR_THIS_FRAME()

	SWITCH iProceedToAlleyStage
	
		CASE 0
			SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(8, "STAGE_PROCEED_TO_ALLEY")		
			
			g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = iCurrentTake
			
			IF NOT IS_PED_INJURED(pedMichael())
				TASK_GO_STRAIGHT_TO_COORD(pedMichael(), <<-147.2652, 6333.3965, 30.6179>>, PEDMOVE_RUN)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMichael(), TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(pedGunman)
				TASK_GO_STRAIGHT_TO_COORD(pedGunman, <<-145.7007, 6333.0366, 30.6069>>, PEDMOVE_RUN)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGunman, TRUE)
			ENDIF
		
			CLEANUP_SET_PIECE_COP_CAR(setPieceRiotVan1)
			CLEANUP_SET_PIECE_COP_CAR(setPieceRiotVan2)
			
			CLEANUP_SET_PIECE_COP_CAR(chopperFlyover)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(RIOT)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnSwatPedModel)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(POLMAV)
		
			REMOVE_PTFX_ASSET()
		
			REMOVE_VEHICLE_RECORDING(4, "RBHStreet")
			REMOVE_VEHICLE_RECORDING(6, "RBHStreet")
			REMOVE_VEHICLE_RECORDING(7, "RBHStreet")
			REMOVE_VEHICLE_RECORDING(8, "RBHStreet")
//			REMOVE_VEHICLE_RECORDING(9, "RBHStreet")
			REMOVE_VEHICLE_RECORDING(10, "RBHStreet")
			REMOVE_VEHICLE_RECORDING(11, "RBHStreet")
						
			bTrevorUsingMinigun = FALSE
			bPlayerWasTrevor = FALSE
			REQUEST_WEAPON_ASSET(WEAPONTYPE_MINIGUN)
			REQUEST_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE)
						
			iProceedToAlleyStage = 99
		BREAK
		
		CASE 99
			IF HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_MINIGUN)
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_ASSAULTRIFLE)
			
				GET_CURRENT_PED_WEAPON(pedSelector.pedID[SELECTOR_PED_TREVOR], wtTrevorWeaponEnteringCutscene)
				GET_CURRENT_PED_WEAPON(pedSelector.pedID[SELECTOR_PED_MICHAEL], wtMichaelWeaponEnteringCutscene)
				
				IF (wtTrevorWeaponEnteringCutscene <> WEAPONTYPE_MINIGUN)
					SET_CUTSCENE_AUDIO_OVERRIDE("_AK")
				ENDIF
				REQUEST_CUTSCENE("RBH_2AB_MCS_6")
				iProceedToAlleyStage = 1
			ENDIF
		BREAK
		
		CASE 1
			IF HAS_CUTSCENE_LOADED()
			
			
				GET_CURRENT_PED_WEAPON(pedSelector.pedID[SELECTOR_PED_TREVOR], wtTrevorWeaponEnteringCutscene)
				IF (wtTrevorWeaponEnteringCutscene = WEAPONTYPE_MINIGUN
				AND GET_AMMO_IN_PED_WEAPON(pedSelector.pedID[SELECTOR_PED_TREVOR], WEAPONTYPE_MINIGUN) <> 0)
					bTrevorUsingMinigun = TRUE
				ENDIF
				
				IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL
					bPlayerWasTrevor = FALSE
				ELIF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_TREVOR
					bPlayerWasTrevor = TRUE
				ENDIF
				
				IF bPlayerWasTrevor
					FORCE_SWITCH_TO_MICHAEL(TRUE)
					SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(pedSelector.pedID[SELECTOR_PED_TREVOR])
				ENDIF
				
				KILL_ANY_CONVERSATION()
				KILL_OFF_COPS_STAGE2()
				
				IF IS_ENTITY_OK(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					CPRINTLN(DEBUG_MISSION, "RBH_2AB_MCS_6 - Registered Michael")
					REGISTER_ENTITY_FOR_CUTSCENE(pedSelector.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
				ENDIF
				
				IF IS_ENTITY_OK(pedGunman)
					CPRINTLN(DEBUG_MISSION, "RBH_2AB_MCS_6 - Registered Gunman")
					REGISTER_ENTITY_FOR_CUTSCENE(pedGunman, "gunman_selection_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, HC_GUNMAN)
				ENDIF
				
				//TREVOR - This is gonna be a BITCH!
				IF IS_ENTITY_OK(pedSelector.pedID[SELECTOR_PED_TREVOR])
					// If Trevor is armed with a minigun, WITH ammo, register that with the cutscene
					IF bTrevorUsingMinigun
					 	CPRINTLN(DEBUG_MISSION, "RBH_2AB_MCS_6 - Registered Trevor as Minigun version")
						
						REGISTER_ENTITY_FOR_CUTSCENE(pedSelector.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
						REGISTER_ENTITY_FOR_CUTSCENE(NULL,"Trevor_2_handed_weapon", CU_DONT_ANIMATE_ENTITY, PLAYER_TWO)
						
						REGISTER_ENTITY_FOR_CUTSCENE(NULL,"Rifle_Mag1^1", CU_DONT_ANIMATE_ENTITY, GET_WEAPON_COMPONENT_TYPE_MODEL(WEAPONCOMPONENT_ASSAULTRIFLE_CLIP_01))
						
						oiTrevorWeaponForCutscene = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(pedSelector.pedID[SELECTOR_PED_TREVOR], wtTrevorWeaponEnteringCutscene)
						IF DOES_ENTITY_EXIST(oiTrevorWeaponForCutscene)
							CPRINTLN(DEBUG_MISSION, "RBH_2AB_MCS_6 - Registered the Minigun")
							REGISTER_ENTITY_FOR_CUTSCENE(oiTrevorWeaponForCutscene, "Trevor_Minigun", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Trevor_AssaultRifle", CU_DONT_ANIMATE_ENTITY, GET_WEAPONTYPE_MODEL(WEAPONTYPE_ASSAULTRIFLE))
						
					// Otherwise, register him with a 2-handed weapon
					ELSE
						CPRINTLN(DEBUG_MISSION, "RBH_2AB_MCS_6 - Registered Trevor as 2-handed version")
						REGISTER_ENTITY_FOR_CUTSCENE(pedSelector.pedID[SELECTOR_PED_TREVOR],"Trevor_2_handed_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Trevor", CU_DONT_ANIMATE_ENTITY, PLAYER_TWO)
						
						oiTrevorWeaponForCutscene = CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(pedSelector.pedID[SELECTOR_PED_TREVOR], wtTrevorWeaponEnteringCutscene)
						IF DOES_ENTITY_EXIST(oiTrevorWeaponForCutscene)
							CPRINTLN(DEBUG_MISSION, "RBH_2AB_MCS_6 - Registered the 2-handed gun")
							REGISTER_ENTITY_FOR_CUTSCENE(oiTrevorWeaponForCutscene, "Trevor_AssaultRifle", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Trevor_Minigun", CU_DONT_ANIMATE_ENTITY, GET_WEAPONTYPE_MODEL(WEAPONTYPE_MINIGUN))
					ENDIF
				ENDIF
							
				IF IS_AUDIO_SCENE_ACTIVE("PS_2A_SHOOTOUT_STREET_MAIN")
					STOP_AUDIO_SCENE("PS_2A_SHOOTOUT_STREET_MAIN")
				ENDIF
				
				//SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, TRUE)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				bCutsceneClearingDone = FALSE
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				FADE_IN_IF_NEEDED()
				iProceedToAlleyStage = 3
			ELSE
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				
					IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", pedSelector.pedID[SELECTOR_PED_TREVOR])
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor_2_handed_weapon", pedSelector.pedID[SELECTOR_PED_TREVOR])
					ELIF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_TREVOR
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", pedSelector.pedID[SELECTOR_PED_MICHAEL])
						IF bTrevorUsingMinigun
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID())
						ELSE
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor_2_handed_weapon", PLAYER_PED_ID())
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(pedGunman)
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("gunman_selection_1", pedGunman)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
		CASE 4
		
			IF IS_CUTSCENE_PLAYING()
			AND bCutsceneClearingDone = FALSE
				
				CLEANUP_SET_PIECE_COP_CAR(setPieceDownStreetSwat2ndWave, TRUE)
				
				IF NOT IS_PED_INJURED(setPieceExtraStreetCopCar.thisDriver)
					APPLY_DAMAGE_TO_PED(setPieceExtraStreetCopCar.thisDriver, 120, TRUE)
				ENDIF
				IF NOT IS_PED_INJURED(setPieceExtraStreetCopCar.thisPassenger)
					APPLY_DAMAGE_TO_PED(setPieceExtraStreetCopCar.thisPassenger, 120, TRUE)
				ENDIF
								
				CLEANUP_CONTINUE_DOWN_STREET()
								
				CLEANUP_SET_PIECE_COP_CAR(setPieceRiotVan1, TRUE)
				CLEANUP_SET_PIECE_COP_CAR(setPieceRiotVan2, TRUE)
				CLEANUP_SET_PIECE_COP_CAR(setPieceDownStreetInitialCop1, FALSE)
	//			CLEANUP_SET_PIECE_COP_CAR(setPieceDownStreetInitialCop2, TRUE)
				CLEANUP_SET_PIECE_COP_CAR(setPieceDownStreetInitialCop3, FALSE)
				CLEANUP_SET_PIECE_COP_CAR(chopperFlyover, TRUE)
				
				CLEANUP_SET_PIECE_COP_CAR(setPieceExtraStreetCopCar, FALSE)
				
				CLEANUP_COPS_AND_CARS_DOWN_STREET(TRUE)
				DELETE_ARRAY_OF_VEHICLES(CopCarsParkedDownStreet)
				
				DELETE_ARRAY_OF_PEDS(pedsExtraSwatInStreet)
				DELETE_ARRAY_OF_PEDS(pedsCopsInStreet)
				DELETE_VEHICLE(CopCarsParkedDownStreet[3])
				
				CLEAR_AREA(<<-144.8663, 6328.2754, 30.6298>>, 5.0, FALSE)
				CLEAR_AREA(<< -140.0300, 6319.6377, 30.4919 >>, 5.0, FALSE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(POLMAV)
				//SET_MODEL_AS_NO_LONGER_NEEDED(DOMINATOR)
				SET_MODEL_AS_NO_LONGER_NEEDED(mnSwatPedModel)
				SET_MODEL_AS_NO_LONGER_NEEDED(RIOT)
				
				IF NOT IS_PED_INJURED(setPieceDownStreetSwat2ndWave.thisDriver)
					APPLY_DAMAGE_TO_PED(setPieceDownStreetSwat2ndWave.thisDriver, 120, TRUE)
				ENDIF
				IF NOT IS_PED_INJURED(setPieceDownStreetSwat2ndWave.thisPassenger)
					APPLY_DAMAGE_TO_PED(setPieceDownStreetSwat2ndWave.thisPassenger, 120, TRUE)
				ENDIF
				
				IF NOT bIsBuddyAPro
					REQUEST_WAYPOINT_RECORDING("RBHArmySec1")
					REQUEST_MODEL(mnCopCarModel)
					REQUEST_MODEL(mnCopPedModel)
					REQUEST_VEHICLE_RECORDING(1, "CBRAM")
					REQUEST_ANIM_DICT("MissHeistPaletoPinned")
				ENDIF
				
				REQUEST_MODEL(CRUSADER)
				REQUEST_VEHICLE_RECORDING(2, "RBHChinDrop")
				REQUEST_MODEL(BARRACKS)
				REQUEST_MODEL(S_M_Y_Marine_03)
				REQUEST_VEHICLE_RECORDING(998, "RBHArmySt")
				REQUEST_VEHICLE_RECORDING(999, "RBHArmySt")
				
				REQUEST_SCRIPT_AUDIO_BANK("PS2A_Jeep_Skids")		
				REQUEST_MODEL(S_M_Y_Marine_03)
				
				bCutsceneClearingDone = TRUE
			ENDIF
			
			IF iProceedToAlleyStage = 3
				IF PREPARE_MUSIC_EVENT("RH2A_FIGHT_PAUSE")
					TRIGGER_MUSIC_EVENT("RH2A_FIGHT_PAUSE")
					iProceedToAlleyStage = 4
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				//SET_CURRENT_PED_WEAPON(pedSelector.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_ASSAULTRIFLE, TRUE)
				IF DOES_ENTITY_EXIST(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					SET_CURRENT_PED_WEAPON(pedSelector.pedID[SELECTOR_PED_MICHAEL], wtMichaelWeaponEnteringCutscene, TRUE)
				ENDIF
			ENDIF
			
			IF bTrevorUsingMinigun
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
					
					IF bPlayerWasTrevor
						FORCE_SWITCH_TO_TREVOR()
					ENDIF
										
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor_Minigun")
					CPRINTLN(DEBUG_MISSION, "Exiting for 'Trevor' - Setting minigun in his hand.")
					IF IS_PLAYER_TREVOR()
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtTrevorWeaponEnteringCutscene, TRUE)
					ENDIF
				ENDIF
			ELSE
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor_2_handed_weapon")
					CPRINTLN(DEBUG_MISSION, "Exiting for 'Trevor_2_handed_weapon' - Setting assault rifle in his hand.")
					IF bPlayerWasTrevor
						FORCE_SWITCH_TO_TREVOR()
					ENDIF
										
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor_AssaultRifle")
						CPRINTLN(DEBUG_MISSION, "Exiting for 'Trevor_2_handed_weapon' - Setting assault rifle in his hand.")
						IF IS_PLAYER_TREVOR()
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtTrevorWeaponEnteringCutscene, TRUE)
						ENDIF
					ENDIF
										
				ENDIF
			ENDIF
			
			IF HAS_CUTSCENE_FINISHED() //CAN_SET_EXIT_STATE_FOR_CAMERA() //
				
				REPLAY_STOP_EVENT()
									
				POSITION_GUYS_AFTER_ALLEY_CUT()		
				
				IF bIsBuddyAPro
					TASK_PEDS_AFTER_ALLEY_CUT(wtMichaelWeaponEnteringCutscene, pedGunmanWeaponType)
				ENDIF
								
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				CLEANUP_CONTINUE_DOWN_STREET()			
				DELETE_OBJECT(oiTrevorWeaponForCutscene)
					
				RESET_GAME_CAMERA()			
				REMOVE_WEAPON_ASSET(WEAPONTYPE_MINIGUN)
				REMOVE_WEAPON_ASSET(WEAPONTYPE_ASSAULTRIFLE)
								
				IF IS_PLAYER_TREVOR()			
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PedMichael(), "MICHAEL")
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, PLAYER_PED_ID(), "TREVOR")		
				ELSE							
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, PedMichael(), "TREVOR")		
				ENDIF
				
				//FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				
				mission_stage = STAGE_HOLD_OFF_ARMY_AT_JUNK_YARD
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

PROC cleanUpHoldOffCopsAtUsedCarLot()
	INT i
	FOR i = 0 TO NUMBER_OF_COPS_IN_STREET - 1 
		DELETE_PED(pedsCopsInStreet[i])
	ENDFOR

	FOR i = 0 TO NUMBER_OF_COP_CARS_IN_STREET - 1 
		DELETE_VEHICLE(CopCarsParkedDownStreet[i])
	ENDFOR

	DELETE_VEHICLE(carCover)

	SET_MODEL_AS_NO_LONGER_NEEDED(mnCopCarModel)
	SET_MODEL_AS_NO_LONGER_NEEDED(mnCopPedModel)
ENDPROC


PROC REMOVE_BLIP_AND_SCREAM_WHEN_DEAD(BLIP_INDEX &bBlip, PED_INDEX &pedIndex, BOOL bArmyDialogue = FALSE)

	PED_INDEX tempCop

	IF DOES_BLIP_EXIST(bBlip)	
		IF DOES_ENTITY_EXIST(pedIndex)
			IF IS_PED_INJURED(pedIndex)
			OR IS_PED_HURT(pedIndex)
				REMOVE_BLIP_AND_CHECK_IF_EXISTS(bBlip)	
				INT iScreamType = GET_RANDOM_INT_IN_RANGE(0, 3)
								
				IF iScreamType = 0
					PLAY_SOUND_FROM_COORD(-1, "TEST_SCREAM_LONG", GET_DEAD_PED_PICKUP_COORDS(pedIndex))
				ELIF iScreamType = 1
					PLAY_SOUND_FROM_COORD(-1, "TEST_SCREAM_SHORT", GET_DEAD_PED_PICKUP_COORDS(pedIndex))
				ENDIF
				SET_PED_AS_NO_LONGER_NEEDED(pedIndex)
				
				IF bArmyDialogue
					IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							GET_CLOSEST_PED(GET_ENTITY_COORDS(PLAYER_PED_ID()), 40.0, TRUE, TRUE, tempCop, FALSE, TRUE)
							REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, 8)
							ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 8, tempCop, "RBHArmy")
							//ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "RBH2AUD", "RBH_ARMYSHT", CONV_PRIORITY_HIGH)
						ENDIF
					ENDIF
				ENDIF
				
				iBodyCount++				
			ENDIF
		ENDIF
	ENDIF


ENDPROC

INT iControlRamGunshots = 0
FLOAT fGunmanAnimTime
VECTOR vAimCoords = <<-160.8199, 6300.6353, 31.5069>>

BOOL bBagCreated
BOOL bForceApplied = FALSE
BOOL bWasAttached = FALSE
BOOL bForceCleanupOfRammingCar

PROC CONTROL_WALL_RAM_GUNSHOTS()
	
	REQUEST_MODEL(P_LD_HEIST_BAG_S_1)
	
	IF NOT IS_PED_INJURED(pedGunman)
		IF IS_ENTITY_PLAYING_ANIM(pedGunman, "MissHeistPaletoPinned", "pinned_against_wall_pro_buddy")
			fGunmanAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(pedGunman, "MissHeistPaletoPinned", "pinned_against_wall_pro_buddy")
			PRINTLN("Running PRO:", fGunmanAnimTime, "  Stage:", iControlRamGunshots)
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedGunman)
		IF IS_ENTITY_PLAYING_ANIM(pedGunman, "MissHeistPaletoPinned", "pinned_against_wall_noob_buddy")
			fGunmanAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(pedGunman, "MissHeistPaletoPinned", "pinned_against_wall_noob_buddy")
			PRINTLN("Running NOOB:", fGunmanAnimTime, "  Stage:", iControlRamGunshots)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(copCarsPullingUpStage2[5].thisDriver)
		IF NOT IS_PED_INJURED(copCarsPullingUpStage2[5].thisDriver)
			GET_PED_BONE_COORDS(copCarsPullingUpStage2[5].thisDriver, BONETAG_NECK, <<0.0, 0.0, 0.0>>)
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(copCarsPullingUpStage2[5].thisCar)
		USE_SIREN_AS_HORN(copCarsPullingUpStage2[5].thisCar, FALSE)
		OVERRIDE_VEH_HORN(copCarsPullingUpStage2[5].thisCar,true,hash("SIRENS_AIRHORN_VB"))	 //Change to: SIRENS_AIRHORN_VB
	ENDIF
	
	
	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(copCarsPullingUpStage2[5].thisCar, "CBRAM") > 54.0
	AND NOT DOES_ENTITY_EXIST(oiBuddysBag)
	AND bBagCreated = FALSE
		oiBuddysBag = CREATE_OBJECT(Prop_CS_Heist_Bag_02, <<-159.3216, 6299.5708, 31.7096>>)
		blipBuddysBag = CREATE_BLIP_FOR_OBJECT(oiBuddysBag)
		ACTIVATE_PHYSICS(oiBuddysBag)
		SET_ENTITY_DYNAMIC(oiBuddysBag, TRUE)				
		//Bag remove
		SET_PED_COMPONENT_VARIATION(pedGunman, INT_TO_ENUM(PED_COMPONENT,6), 1, 0, 0) //(feet)
				
		REPLAY_RECORD_BACK_FOR_TIME(3.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
		
		iCurrentTake = iCurrentTake - TAKE_LOST_BY_GUNMAN
		bBagCreated = TRUE
		bForceApplied = FALSE
	ENDIF
	
	IF bBagCreated = TRUE
	AND bForceApplied = FALSE
		IF DOES_ENTITY_HAVE_PHYSICS(oiBuddysBag)
			APPLY_FORCE_TO_ENTITY(oiBuddysBag, APPLY_TYPE_EXTERNAL_IMPULSE, <<2.0, 0.0, -1.0>>, <<0.1, 0.0, 0.0>>, 0, TRUE, TRUE, TRUE)
			bForceApplied = TRUE
		ENDIF
	ENDIF
	
	
	IF DOES_ENTITY_EXIST(pedGunman)
		IF NOT bIsBuddyAPro
		
			SWITCH iControlRamGunshots

				CASE 0
					START_AUDIO_SCENE("PS_2A_CAR_RAMS_BUDDY")
					bWasAttached = FALSE
					IF DOES_ENTITY_EXIST(copCarsPullingUpStage2[5].thisDriver)
						IF NOT IS_PED_INJURED(copCarsPullingUpStage2[5].thisDriver)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(copCarsPullingUpStage2[5].thisDriver, TRUE)
						ENDIF
					ENDIF
					REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL)
					IF HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_PISTOL)
					ENDIF
					REQUEST_WEAPON_ASSET(pedGunmanWeaponType)
					IF HAS_WEAPON_ASSET_LOADED(pedGunmanWeaponType)
						SET_CURRENT_PED_WEAPON(pedGunman, pedGunmanWeaponType, TRUE)
						fGunmanAnimTime = 0.0
						iControlRamGunshots++
					ENDIF
				BREAK 
				
				CASE 1
					IF fGunmanAnimTime >= 0.049
						SET_PED_SHOOTS_AT_COORD(pedGunman, vAimCoords)	
					ENDIF
				
					IF fGunmanAnimTime >= 0.099
						iControlRamGunshots++
					ENDIF
				BREAK
			
				CASE 2
					IF fGunmanAnimTime >= 0.286
						SET_PED_SHOOTS_AT_COORD(pedGunman, vAimCoords)					
						
						IF DOES_ENTITY_EXIST(copCarsPullingUpStage2[5].thisDriver)
							IF NOT IS_PED_INJURED(copCarsPullingUpStage2[5].thisDriver)
								SET_PED_PLAYS_HEAD_ON_HORN_ANIM_WHEN_DIES_IN_VEHICLE(copCarsPullingUpStage2[5].thisDriver, TRUE)
								APPLY_DAMAGE_TO_PED(copCarsPullingUpStage2[5].thisDriver, 400, TRUE)
							ENDIF
						ENDIF
						
					ENDIF
				
					IF fGunmanAnimTime > 0.422
						PRINTLN("muting sirens")
												
						IF NOT IS_ENTITY_DEAD(copCarsPullingUpStage2[5].thisCar)
						
							SET_VEHICLE_HAS_MUTED_SIRENS(copCarsPullingUpStage2[5].thisCar, TRUE)
							SET_VEHICLE_SIREN(copCarsPullingUpStage2[5].thisCar, FALSE)
						
							//SCRIPT_ASSERT("bangin on the horn")
							//SET_HORN_PERMANENTLY_ON(copCarsPullingUpStage2[5].thisCar)
							SET_HORN_PERMANENTLY_ON_TIME(copCarsPullingUpStage2[5].thisCar, 5000.0)//5.0)
							USE_SIREN_AS_HORN(copCarsPullingUpStage2[5].thisCar, FALSE)
						ELSE
							SCRIPT_ASSERT("Car is dead")
						ENDIF
						iControlRamGunshots++
					ENDIF
				BREAK
				
				CASE 3
					IF fGunmanAnimTime >= 0.461
						SET_PED_DROPS_WEAPON(pedGunman)
						iControlRamGunshots++
					ENDIF
				BREAK
				
				CASE 4
					GIVE_WEAPON_TO_PED(pedGunman, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
					SET_CURRENT_PED_WEAPON(pedGunman, WEAPONTYPE_PISTOL, TRUE)
					iControlRamGunshots++
				BREAK
				
				CASE 5
					IF fGunmanAnimTime >= 0.536
						SET_PED_SHOOTS_AT_COORD(pedGunman, vAimCoords)	
						iControlRamGunshots++
					ENDIF
				BREAK
				
				CASE 6
					IF fGunmanAnimTime >= 0.575 
						SET_PED_SHOOTS_AT_COORD(pedGunman, vAimCoords)	
						iControlRamGunshots++
					ENDIF
				BREAK
				
				CASE 7
					IF fGunmanAnimTime >= 0.605
						SET_PED_SHOOTS_AT_COORD(pedGunman, vAimCoords)
						
				
						
						iControlRamGunshots++
					ENDIF
				BREAK
				
				CASE 8
					
					IF NOT IS_ENTITY_DEAD(copCarsPullingUpStage2[5].thisCar)
						SET_VEHICLE_ENGINE_HEALTH(copCarsPullingUpStage2[5].thisCar, 0.0)
						//DETACH_ENTITY(copCarsPullingUpStage2[5].thisCar)
						FREEZE_ENTITY_POSITION(copCarsPullingUpStage2[5].thisCar, FALSE)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(copCarsPullingUpStage2[5].thisDriver)
						SET_ENTITY_HEALTH(copCarsPullingUpStage2[5].thisDriver, 0)
					ENDIF
					
					IF NOT IS_PED_INJURED(pedGunman)
						//DETACH_ENTITY(pedGunman)
											
						SET_ENTITY_INVINCIBLE(pedGunman, FALSE)
						SET_ENTITY_PROOFS(pedGunman, FALSE, fALSE, FALSE, FALSE, FALSE, FALSE)				
						SET_PED_KEEP_TASK(pedGunman, TRUE)
						//SET_PED_AS_NO_LONGER_NEEDED(pedGunman)
						//SET_MODEL_AS_NO_LONGER_NEEDED(HC_GUNMAN)
					ENDIF
					
										
					iControlRamGunshots++
				BREAK
					
			ENDSWITCH
		ELSE
		
//			SWITCH iControlRamGunshots
//
//				CASE 0				
//					IF fGunmanAnimTime >= 0.060
//						SET_PED_SHOOTS_AT_COORD(pedGunman, vAimCoords)	
//					ENDIF
//				
//					IF fGunmanAnimTime >= 0.143
//						IF NOT IS_ENTITY_DEAD(copCarsPullingUpStage2[5].thisCar)
//							SCRIPT_ASSERT("bangin on the horn")
//							SET_HORN_PERMANENTLY_ON(copCarsPullingUpStage2[5].thisCar)
//							SET_HORN_PERMANENTLY_ON_TIME(copCarsPullingUpStage2[5].thisCar, 10000.0)
//						ENDIF
//						
//						IF DOES_ENTITY_EXIST(copCarsPullingUpStage2[5].thisDriver)
//							IF NOT IS_PED_INJURED(copCarsPullingUpStage2[5].thisDriver)
//								APPLY_DAMAGE_TO_PED(copCarsPullingUpStage2[5].thisDriver, 400, TRUE)
//							ENDIF
//						ENDIF
//						
//						SETTIMERB(0)
//						iControlRamGunshots++
//					ENDIF
//				BREAK
//						
//				CASE 2
//					IF fGunmanAnimTime >= 0.99
//						iControlRamGunshots++
//					ENDIF
//				BREAK
//				
//				CASE 3
//					IF TIMERB() > 10000
//						iControlRamGunshots++
//					ENDIF
//				BREAK
//				
//				CASE 4
//					iControlRamGunshots++
//				BREAK
					
//			ENDSWITCH
		
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedGunman)
		IF IS_ENTITY_ATTACHED(pedGunman)
			bWasAttached = TRUE
		ENDIF
	ENDIF
	
	IF NOT bIsBuddyAPro
	//AND DOES_ENTITY_EXIST(copCarsPullingUpStage2[5].thisCar)
	
		IF NOT IS_ENTITY_DEAD(copCarsPullingUpStage2[5].thisCar)
		AND bForceCleanupOfRammingCar = FALSE
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), copCarsPullingUpStage2[5].thisCar)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(copCarsPullingUpStage2[5].thisCar)
					STOP_PLAYBACK_RECORDED_VEHICLE(copCarsPullingUpStage2[5].thisCar)
				ENDIF
				FREEZE_ENTITY_POSITION(copCarsPullingUpStage2[5].thisCar, FALSE)
				IF NOT IS_PED_INJURED(copCarsPullingUpStage2[5].thisDriver)
					APPLY_DAMAGE_TO_PED(copCarsPullingUpStage2[5].thisDriver, 400, TRUE)
				ENDIF
				IF NOT IS_PED_INJURED(pedGunman)
					DETACH_ENTITY(pedGunman)
					//CLEAR_PED_TASKS(pedGunman)
					SET_PED_CAN_RAGDOLL(pedGunman, TRUE)
					SET_PED_TO_RAGDOLL(pedGunman, 0, 60000, TASK_RELAX)
					APPLY_DAMAGE_TO_PED(pedGunman, 400, TRUE)		
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(pedGunman)
			AND bWasAttached = TRUE
				DETACH_ENTITY(pedGunman)
				
				IF NOT IS_ENTITY_DEAD(copCarsPullingUpStage2[5].thisCar)
					DETACH_ENTITY(copCarsPullingUpStage2[5].thisCar)
				ENDIF
				
				CLEAR_PED_TASKS(pedGunman)

				SET_PED_CAN_RAGDOLL(pedGunman, TRUE)
				SET_PED_TO_RAGDOLL(pedGunman, 0, 60000, TASK_RELAX)
				APPLY_DAMAGE_TO_PED(pedGunman, 500, TRUE)		
				SET_ENTITY_HEALTH(pedGunman, 0)			
				SET_PED_AS_NO_LONGER_NEEDED(pedGunman)
				SET_MODEL_AS_NO_LONGER_NEEDED(HC_GUNMAN)
				CLEANUP_SET_PIECE_COP_CAR(copCarsPullingUpStage2[5])
				SET_MODEL_AS_NO_LONGER_NEEDED(SHERIFF)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

CONST_FLOAT ROPE_LENGTH 6.0

CONST_FLOAT CHOPPER_ROPE_OFFSET_X 0.7
CONST_FLOAT CHOPPER_ROPE_OFFSET_Y 0.7
CONST_FLOAT CHOPPER_ROPE_OFFSET_Z -0.85
CONST_FLOAT CONTAINER_ROPE_OFFSET_X 1.2
CONST_FLOAT CONTAINER_ROPE_OFFSET_Y 1.7
CONST_FLOAT CONTAINER_ROPE_OFFSET_Z 1.2

// obtain the offset to attach the particular rope to the chopper
FUNC VECTOR GET_ROPE_OFFSET_FROM_CHOPPER(INT iRope)
	SWITCH iRope
		CASE 0
			RETURN <<CHOPPER_ROPE_OFFSET_X, CHOPPER_ROPE_OFFSET_Y, CHOPPER_ROPE_OFFSET_Z>>
		BREAK
		CASE 1
			RETURN <<-CHOPPER_ROPE_OFFSET_X, CHOPPER_ROPE_OFFSET_Y, CHOPPER_ROPE_OFFSET_Z>>
		BREAK
		CASE 2
			RETURN <<CHOPPER_ROPE_OFFSET_X, -CHOPPER_ROPE_OFFSET_Y, CHOPPER_ROPE_OFFSET_Z>>
		BREAK
		CASE 3
			RETURN <<-CHOPPER_ROPE_OFFSET_X, -CHOPPER_ROPE_OFFSET_Y, CHOPPER_ROPE_OFFSET_Z>>
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Passed in a bad rope to GET_ROPE_OFFSET_FROM_CHOPPER. Use 0-3.")
	RETURN <<0,0,0>>
ENDFUNC

// obtain the offset to attach the rope to the container
FUNC VECTOR GET_ROPE_OFFSET_FROM_CONTAINER(INT iRope)
	SWITCH iRope
		CASE 0
			RETURN <<CONTAINER_ROPE_OFFSET_X, CONTAINER_ROPE_OFFSET_Y, CONTAINER_ROPE_OFFSET_Z>>
		BREAK
		CASE 1
			RETURN <<-CONTAINER_ROPE_OFFSET_X, CONTAINER_ROPE_OFFSET_Y, CONTAINER_ROPE_OFFSET_Z>>
		BREAK
		CASE 2
			RETURN <<CONTAINER_ROPE_OFFSET_X, -CONTAINER_ROPE_OFFSET_Y, CONTAINER_ROPE_OFFSET_Z>>
		BREAK
		CASE 3
			RETURN <<-CONTAINER_ROPE_OFFSET_X, -CONTAINER_ROPE_OFFSET_Y, CONTAINER_ROPE_OFFSET_Z>>
		BREAK
	ENDSWITCH
	
	SCRIPT_ASSERT("Passed in a bad rope to GET_ROPE_OFFSET_FROM_CONTAINER. Use 0-3.")
	RETURN <<0,0,0>>
ENDFUNC

PROC CREATE_TANK_ROPES(VEHICLE_INDEX &chopperVehicle, ROPE_INDEX &tankRopes[], BOOL &bRopesCreated, BOOL bTaut = FALSE)
	IF NOT bRopesCreated
	    INT i 
	    VECTOR vCreatePos 
	    IF IS_VEHICLE_DRIVEABLE(chopperVehicle)
			REPEAT 4 i
			    vCreatePos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(chopperVehicle, GET_ROPE_OFFSET_FROM_CHOPPER(i))
			    PHYSICS_ROPE_TYPE ropeType
			    IF bTaut
			          ropeType = PHYSICS_ROPE_THIN
			    ELSE
			          ropeType = PHYSICS_ROPE_THIN
			    ENDIF
			    tankRopes[i] = ADD_ROPE(vCreatePos, <<0,0,0>>, ROPE_LENGTH, ropeType)
			ENDREPEAT
			bRopesCreated = TRUE
	    ENDIF
	ENDIF
ENDPROC


// attach chopper to container with rope
PROC ATTACH_CHOPPER_TO_TANK_WITH_ROPE(VEHICLE_INDEX &chopperVehicle, VEHICLE_INDEX &tankVehicle, ROPE_INDEX &tankRopes[], BOOL &bRopesCreated, BOOL &bRopesAttached, BOOL bPin = FALSE, BOOL bTaut = FALSE, bool force_ropes_to_create = false)
      
	IF NOT bRopesCreated
		CREATE_TANK_ROPES(chopperVehicle, tankRopes, bRopesCreated, bTaut)
	ENDIF

	IF bRopesCreated
	    IF NOT bRopesAttached
	          INT i 
	          VECTOR vChopperAttachPos
	          VECTOR vContainerAttachPos
	          IF IS_VEHICLE_DRIVEABLE(chopperVehicle)
	          AND DOES_ENTITY_EXIST(tankVehicle)
	                IF DOES_ENTITY_HAVE_PHYSICS(tankVehicle)
	                OR bPin
	                or force_ropes_to_create
	                
	                      DETACH_ENTITY(tankVehicle, false, false)
	                      REPEAT 4 i
	                            vChopperAttachPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(chopperVehicle, GET_ROPE_OFFSET_FROM_CHOPPER(i))
	                            vContainerAttachPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(tankVehicle, GET_ROPE_OFFSET_FROM_CONTAINER(i))
	                            IF bPin
	                                  //DETACH_ROPE_FROM_ENTITY(tankRopes[i], tankVehicle)
	                                  PIN_ROPE_VERTEX(tankRopes[i], 0, vChopperAttachPos)
	                                  PIN_ROPE_VERTEX(tankRopes[i], GET_ROPE_VERTEX_COUNT(tankRopes[i])-1, vContainerAttachPos)
	                            ELSE
	                                  UNPIN_ROPE_VERTEX(tankRopes[i], 0)
	                                  UNPIN_ROPE_VERTEX(tankRopes[i], GET_ROPE_VERTEX_COUNT(tankRopes[i])-1)
	                                  ATTACH_ENTITIES_TO_ROPE(tankRopes[i], tankVehicle, chopperVehicle, vContainerAttachPos, vChopperAttachPos, ROPE_LENGTH, 0, 0)
	                            ENDIF
	                      ENDREPEAT
						  
						  SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(tankVehicle, FALSE)
	                      
	                      IF NOT bPin
	                           // SET_OBJECT_PHYSICS_PARAMS(tankVehicle, 1000, 1.0,  <<0.02, 0.02, 0.003>>, <<0.02, 0.4, 0.4>>) //old mass = 1600
	                            								
								//ACTIVATE_PHYSICS(tankVehicle)
								//APPLY_FORCE_TO_ENTITY(tankVehicle, APPLY_TYPE_EXTERNAL_IMPULSE, <<0,0,0.1>>, <<0,0,0>>, 0, FALSE, TRUE, TRUE)
	                            set_entity_dynamic(tankVehicle, true)
	                      ENDIF
	                      bRopesAttached = TRUE
	                ENDIF
	          ENDIF
	    ENDIF
	ENDIF
ENDPROC




VECTOR vRamOffset
VECTOR vRamRotation = <<0.0, 0.0, 0.0>>

PROC DEAL_WITH_CHINOOK(FLOAT fStartTime = 6000.0)

	VECTOR vTankRotation

	SWITCH iDealWithChinooks
	
		CASE 0
			REQUEST_MODEL(CARGOBOB)
			REQUEST_MODEL(RHINO)
			REQUEST_MODEL(S_M_Y_Marine_03)
			REQUEST_VEHICLE_RECORDING(5, "RBHChin")
			//REQUEST_VEHICLE_RECORDING(2, "RBHChin")
			//REQUEST_VEHICLE_RECORDING(3, "RBHChin")
			
			ROPE_LOAD_TEXTURES()
			
			bAllowChinookRestart = FALSE
			iDealWithChinooks++
		BREAK
			
		CASE 1
		
			IF HAS_MODEL_LOADED(CARGOBOB)
			AND HAS_MODEL_LOADED(RHINO)
			AND ROPE_ARE_TEXTURES_LOADED()
				IF HAS_MODEL_LOADED(S_M_Y_Marine_03)
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(5, "RBHChin")
						START_AUDIO_SCENE("PS_2A_SHOOTOUT_YARD_FLYING_TANK")
					
						chinooks[0] = CREATE_VEHICLE(CARGOBOB, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(5, fStartTime, "RBHChin"))
						SET_VEHICLE_ACTIVE_DURING_PLAYBACK(chinooks[0], TRUE)
						SET_HELI_BLADES_FULL_SPEED(chinooks[0])
						chinookPilots[0] = CREATE_PED_INSIDE_VEHICLE(chinooks[0], PEDTYPE_MISSION, S_M_Y_Marine_03)
						SET_PED_AS_DEFENSIVE_COP(chinookPilots[0], WEAPONTYPE_CARBINERIFLE)	
						SET_PED_CAN_BE_TARGETTED(chinookPilots[0], FALSE)		
						
						SET_ENTITY_PROOFS(chinooks[0], TRUE, TRUE, TRUE, TRUE, TRUE, TRUE)
						
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(chinooks[0])
							STOP_PLAYBACK_RECORDED_VEHICLE(chinooks[0])
						ENDIF
						START_PLAYBACK_RECORDED_VEHICLE(chinooks[0], 5, "RBHChin")
						SET_PLAYBACK_SPEED(chinooks[0], 0.85)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(chinooks[0], fStartTime)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(chinooks[0])
						
						vTankRotation = GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(5, fStartTime, "RBHChin")
												
						chinooksTank = CREATE_VEHICLE(RHINO, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(chinooks[0], <<0.0, 0.0, -6.0>>), vTankRotation.z )
						SET_ENTITY_ROTATION(chinooksTank, vTankRotation)
						
						SET_ENTITY_VELOCITY(chinooksTank, GET_ENTITY_VELOCITY(chinooks[0]))
						
						STABILISE_ENTITY_ATTACHED_TO_HELI(chinooks[0], chinooksTank, -6.5)
						
						IF DOES_ENTITY_EXIST(chinooksTank) AND NOT IS_ENTITY_DEAD(chinooksTank)
							chinooksTankDriver = CREATE_PED_INSIDE_VEHICLE(chinooksTank, PEDTYPE_MISSION, S_M_Y_Marine_03)
							IF DOES_ENTITY_EXIST(chinooksTankDriver) AND NOT IS_ENTITY_DEAD(chinooksTankDriver)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(chinooksTankDriver, TRUE)
							ENDIF
						ENDIF
						ATTACH_CHOPPER_TO_TANK_WITH_ROPE(chinooks[0], chinooksTank, ropesTank, bRopesCreatedTank[0], bRopesAttachedTank[0])
						
						SET_ENTITY_LOD_DIST(chinooks[0], 800)
						
						iDealWithChinooks++	
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chinooks[0], "RBHChin") > 9.0
				iDealWithChinooks++	
			ENDIF
		BREAK
		
		CASE 3
			//Maybe some dialogue?
			IF NOT IS_ENTITY_DEAD(chinooks[0])
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(chinooks[0])
					IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chinooks[0], "RBHChin") > 95.0
						IF NOT IS_ENTITY_DEAD(chinooksTank)
							iDealWithChinooks++		
						ENDIF
					ENDIF
				ELSE
					iDealWithChinooks++
				ENDIF
			ELSE
				iDealWithChinooks++
			ENDIF
		BREAK
		
		CASE 4
			IF NOT IS_ENTITY_DEAD(chinooksTank)
				DETACH_ROPE_FROM_ENTITY(ropesTank[0], chinooksTank)
				DETACH_ROPE_FROM_ENTITY(ropesTank[1], chinooksTank)
				DETACH_ROPE_FROM_ENTITY(ropesTank[2], chinooksTank)
				DETACH_ROPE_FROM_ENTITY(ropesTank[3], chinooksTank)
			ENDIF
			DELETE_ROPE(ropesTank[0])
			DELETE_ROPE(ropesTank[1])
			DELETE_ROPE(ropesTank[2])
			DELETE_ROPE(ropesTank[3])
						
			STOP_AUDIO_SCENE("PS_2A_SHOOTOUT_YARD_FLYING_TANK")
			iDealWithChinooks++
		BREAk
		
		CASE 5
			
			IF NOT IS_ENTITY_DEAD(chinooks[0])
				//IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(chinooks[0])//GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chinooks[0], "RBHChin") > 99.0
					DELETE_VEHICLE(chinooks[0])
					SET_MODEL_AS_NO_LONGER_NEEDED(CARGOBOB)
					SET_MODEL_AS_NO_LONGER_NEEDED(RHINO)
					DELETE_PED(chinookPilots[0])
					DELETE_VEHICLE(chinooksTank)
					DELETE_PED(chinooksTankDriver)
					
					ROPE_UNLOAD_TEXTURES()
					
				//ENDIF
			ENDIF
			
			
			IF NOT DOES_ENTITY_EXIST(chinooks[0])
			AND bAllowChinookRestart 
				//iDealWithChinooks = 0
			ENDIF
			
		BREAK
	
	ENDSWITCH
	
ENDPROC


CONST_INT NUMBER_OF_ARMY_TO_HOLD_OFF	4
INT iArmyRespawnIndex
PED_INDEX pedsArmyGuysToHoldOff[NUMBER_OF_ARMY_TO_HOLD_OFF]
BLIP_INDEX blipsArmyGuysToHoldOff[NUMBER_OF_ARMY_TO_HOLD_OFF]
VECTOR vArmyRespawnPoint1 = <<-230.9949, 6273.4370, 30.6023>>
VECTOR vArmyRespawnPoint2 = <<-158.5322, 6291.9849, 30.5764>>
FLOAT fArmyRespawnHeading1 = 323.8667
FLOAT fArmyRespawnHeading2 = 47.2790

//SEQUENCE_INDEX seqGetIntoPosition

PROC MANAGE_ARMY_RESPAWN_WAITING_FOR_FRANKLIN()

	INT i

	IF iArmyRespawnIndex < NUMBER_OF_ARMY_TO_HOLD_OFF - 1
		iArmyRespawnIndex++
	ELSE
		iArmyRespawnIndex = 0
	ENDIF
	
	VECTOR vRandomOffset
	
	i = iArmyRespawnIndex
		
	IF IS_PED_INJURED(pedsArmyGuysToHoldOff[i])
	OR IS_PED_HURT(pedsArmyGuysToHoldOff[i])
	OR IS_ENTITY_DEAD(pedsArmyGuysToHoldOff[i])
														
		SET_PED_AS_NO_LONGER_NEEDED(pedsArmyGuysToHoldOff[i]) //Allow body to cleanup
		
		PRINTLN("Ped:", i, " is dead")
		
		bThisPedInjured[i] = TRUE
		iBodyCount++
					
	ENDIF

	IF bThisPedInjured[i] = TRUE
	AND NOT DOES_ENTITY_EXIST(pedsArmyGuysToHoldOff[i])
	AND GET_GAME_TIMER() - iTimeOfLastRespawn > 2300
		
		PRINTLN("Respawning ped:", i, " ")
		
		vRandomOffset.x = GET_RANDOM_FLOAT_IN_RANGE(-0.5, 0.5)
		vRandomOffset.y = GET_RANDOM_FLOAT_IN_RANGE(-0.5, 0.5)
			
		IF iArmyRespawnIndex % 2 = 0
		
			//IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vArmyRespawnPoint1, <<7.0, 7.0, 7.0>>)
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-199.544785,6265.691406,58.015686>>, <<-263.874298,6204.558594,29.770254>>, 75.750000)
							
				pedsArmyGuysToHoldOff[i] = CREATE_PED(PEDTYPE_COP, S_M_Y_Marine_03, vArmyRespawnPoint1 + vRandomOffset,fArmyRespawnHeading1)
			ELSE
				pedsArmyGuysToHoldOff[i] = CREATE_PED(PEDTYPE_COP, S_M_Y_Marine_03, vArmyRespawnPoint2 + vRandomOffset, fArmyRespawnHeading2)
			ENDIF
		ELSE
			//IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-199.5139, 6253.4839, 30.4895>>, <<7.0, 7.0, 7.0>>)
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-199.544785,6265.691406,58.015686>>, <<-263.874298,6204.558594,29.770254>>, 75.750000)
				pedsArmyGuysToHoldOff[i] = CREATE_PED(PEDTYPE_COP, S_M_Y_Marine_03, <<-199.5139, 6253.4839, 30.4895>> + vRandomOffset,fArmyRespawnHeading1)
			ELSE
				pedsArmyGuysToHoldOff[i] = CREATE_PED(PEDTYPE_COP, S_M_Y_Marine_03, vArmyRespawnPoint2 + vRandomOffset, fArmyRespawnHeading2)
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 debugPedName = "ArmRes:"
			debugPedName += i
			SET_PED_NAME_DEBUG(pedsArmyGuysToHoldOff[i], debugPedName)
		#ENDIF
					
		IF GET_RANDOM_INT_IN_RANGE(0, 10) > 6
			SET_PED_AS_DEFENSIVE_COP(pedsArmyGuysToHoldOff[i], WEAPONTYPE_PISTOL, TRUE)
		ELSE
			SET_PED_AS_DEFENSIVE_COP(pedsArmyGuysToHoldOff[i], WEAPONTYPE_CARBINERIFLE, TRUE)
		ENDIF
				
		SET_PED_CAN_BE_TARGETTED(pedsArmyGuysToHoldOff[i], TRUE)
		SET_PED_CONFIG_FLAG(pedsArmyGuysToHoldOff[i], PCF_RunFromFiresAndExplosions, FALSE)		
		//REMOVE_PED_DEFENSIVE_AREA(pedsArmyGuysToHoldOff[i], FALSE)
		
		SET_PED_COMBAT_ATTRIBUTES(pedsArmyGuysToHoldOff[i],	CA_AGGRESSIVE, TRUE)
		
		SET_PED_STEERS_AROUND_OBJECTS(pedsArmyGuysToHoldOff[i], FALSE)
		SET_PED_STEERS_AROUND_PEDS(pedsArmyGuysToHoldOff[i], FALSE)
	
		SET_COMBAT_FLOAT(pedsArmyGuysToHoldOff[i], CCF_STRAFE_WHEN_MOVING_CHANCE, 0.4)
		
		SWITCH i
			CASE 0
				//SET_PED_RUN_INTO_POSITION_AND_COMBAT_DA(pedsArmyGuysToHoldOff[i], << -193.5920, 6291.4912, 30.4901 >>,  FALSE, 4.0, FALSE)
				
				SET_PED_SPHERE_DEFENSIVE_AREA(pedsArmyGuysToHoldOff[i], <<-214.3985, 6281.4414, 30.4895>>, 3.0, TRUE)
				
				
//				OPEN_SEQUENCE_TASK(seqGetIntoPosition)
//					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
//					TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, << -193.5920, 6291.4912, 30.4901 >>,  <<-108.9987, 6205.5713, 31.5794>>, PEDMOVE_SPRINT, FALSE)
//					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//					TASK_COMBAT_HATED_TARGETS_IN_AREA(NULL, << -193.5920, 6291.4912, 30.4901 >>, 30.0)
//				CLOSE_SEQUENCE_TASK(seqGetIntoPosition)
//				TASK_PERFORM_SEQUENCE(pedsArmyGuysToHoldOff[i], seqGetIntoPosition)
//				CLEAR_SEQUENCE_TASK(seqGetIntoPosition)
			
			BREAK
			CASE 1
				
				SET_PED_SPHERE_DEFENSIVE_AREA(pedsArmyGuysToHoldOff[i], << -188.4366, 6272.0742, 30.4905 >>, 3.0, TRUE)
				
				
				//SET_PED_RUN_INTO_POSITION_AND_COMBAT_DA(pedsArmyGuysToHoldOff[i], << -188.4366, 6272.0742, 30.4905 >>,  FALSE, 4.0, FALSE)
//				OPEN_SEQUENCE_TASK(seqGetIntoPosition)
//					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, TRUE)
//					TASK_GO_TO_COORD_AND_AIM_AT_HATED_ENTITIES_NEAR_COORD(NULL, << -188.4366, 6272.0742, 30.4905 >>,  << -188.4366, 6272.0742, 30.4905 >>, PEDMOVE_SPRINT, FALSE)
//					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
//					TASK_COMBAT_HATED_TARGETS_IN_AREA(NULL, << -188.4366, 6272.0742, 30.4905 >>, 30.0)
//				CLOSE_SEQUENCE_TASK(seqGetIntoPosition)
//				TASK_PERFORM_SEQUENCE(pedsArmyGuysToHoldOff[i], seqGetIntoPosition)
//				CLEAR_SEQUENCE_TASK(seqGetIntoPosition)
			BREAK
			CASE 2
					
				SET_PED_SPHERE_DEFENSIVE_AREA(pedsArmyGuysToHoldOff[i], <<-205.4045, 6279.2466, 30.4892>>, 3.0, TRUE)
				
				//SET_PED_RUN_INTO_POSITION_AND_COMBAT_DA(pedsArmyGuysToHoldOff[i], <<-205.4045, 6279.2466, 30.4892>>,  FALSE, 4.0, FALSE)
			BREAK
			CASE 3
				
				SET_PED_SPHERE_DEFENSIVE_AREA(pedsArmyGuysToHoldOff[i], <<-206.3989, 6266.9790, 30.4892>>, 3.0, TRUE)
				
				//SET_PED_RUN_INTO_POSITION_AND_COMBAT_DA(pedsArmyGuysToHoldOff[i], <<-206.3989, 6266.9790, 30.4892>>, FALSE, 4.0, FALSE)
			BREAK
		ENDSWITCH
		
	//	TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedsArmyGuysToHoldOff[i], 170.0)
		bThisPedInjured[i] = FALSE				
		iTimeOfLastRespawn = GET_GAME_TIMER()
		
	ENDIF
	
	FOR i = 0 TO 3
		IF DOES_ENTITY_EXIST(pedsArmyGuysToHoldOff[i])
			IF GET_DISTANCE_BETWEEN_ENTITIES(pedsArmyGuysToHoldOff[i], PLAYER_PED_ID()) < 9.0
			OR GET_DISTANCE_BETWEEN_ENTITIES(pedsArmyGuysToHoldOff[i], PedMichael()) < 9.0
				SET_PED_COMBAT_MOVEMENT (pedsArmyGuysToHoldOff[i], CM_WILLADVANCE)
				REMOVE_PED_DEFENSIVE_AREA(pedsArmyGuysToHoldOff[i], FALSE)		
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedGunman)
		AND DOES_ENTITY_EXIST(pedsArmyGuysToHoldOff[i])
			IF GET_DISTANCE_BETWEEN_ENTITIES(pedsArmyGuysToHoldOff[i], pedGunman) < 9.0
				SET_PED_COMBAT_MOVEMENT (pedsArmyGuysToHoldOff[i], CM_WILLADVANCE)
				REMOVE_PED_DEFENSIVE_AREA(pedsArmyGuysToHoldOff[i], FALSE)	
			ENDIF
		ENDIF
		
	ENDFOR
	
ENDPROC



PROC MANAGE_ARMY_RESPAWN_WAITING_FOR_FRANKLIN_VERSION_2()

	INT i

	IF iArmyRespawnIndex < NUMBER_OF_ARMY_TO_HOLD_OFF - 1
		iArmyRespawnIndex++
	ELSE
		iArmyRespawnIndex = 0
	ENDIF
	
	VECTOR vRandomOffset
	
	i = iArmyRespawnIndex
	IF DOES_ENTITY_EXIST(pedsArmyGuysToHoldOff[i])
		IF IS_PED_INJURED(pedsArmyGuysToHoldOff[i])
		OR IS_PED_HURT(pedsArmyGuysToHoldOff[i])
		OR IS_ENTITY_DEAD(pedsArmyGuysToHoldOff[i])
			PRINTLN("Ped:", i, " is dead")
			SET_PED_AS_NO_LONGER_NEEDED(pedsArmyGuysToHoldOff[i]) //Allow body to cleanup
			REMOVE_BLIP(blipsArmyGuysToHoldOff[i])
			bThisPedInjured[i] = TRUE
			iBodyCount++
		ENDIF
	ELSE
		bThisPedInjured[i] = TRUE
	ENDIF
	
	IF bThisPedInjured[i] = TRUE
	AND NOT DOES_ENTITY_EXIST(pedsArmyGuysToHoldOff[i])
	AND GET_GAME_TIMER() - iTimeOfLastRespawn > 2300
		
		PRINTLN("Respawning ped:", i, " ")
		
		vRandomOffset.x = GET_RANDOM_FLOAT_IN_RANGE(-0.5, 0.5)
		vRandomOffset.y = GET_RANDOM_FLOAT_IN_RANGE(-0.5, 0.5)
			
		IF iArmyRespawnIndex % 2 = 0
			//IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vArmyRespawnPoint1, <<7.0, 7.0, 7.0>>)
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-199.544785,6265.691406,58.015686>>, <<-263.874298,6204.558594,29.770254>>, 75.750000)
				pedsArmyGuysToHoldOff[i] = CREATE_PED(PEDTYPE_COP, S_M_Y_Marine_03, vArmyRespawnPoint1 + vRandomOffset,fArmyRespawnHeading1)
			ELSE
				pedsArmyGuysToHoldOff[i] = CREATE_PED(PEDTYPE_COP, S_M_Y_Marine_03, vArmyRespawnPoint2 + vRandomOffset, fArmyRespawnHeading2)
			ENDIF
		ELSE
			//IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-199.5139, 6253.4839, 30.4895>>, <<7.0, 7.0, 7.0>>)
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-199.544785,6265.691406,58.015686>>, <<-263.874298,6204.558594,29.770254>>, 75.750000)
				pedsArmyGuysToHoldOff[i] = CREATE_PED(PEDTYPE_COP, S_M_Y_Marine_03, <<-199.5139, 6253.4839, 30.4895>> + vRandomOffset,fArmyRespawnHeading1)
			ELSE
				pedsArmyGuysToHoldOff[i] = CREATE_PED(PEDTYPE_COP, S_M_Y_Marine_03, vArmyRespawnPoint2 + vRandomOffset, fArmyRespawnHeading2)
			ENDIF
			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedsArmyGuysToHoldOff[i], "COVER_ME", "s_m_y_genericmarine_01_black_mini_01", SPEECH_PARAMS_FORCE)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			TEXT_LABEL_63 debugPedName = "ArmRes:"
			debugPedName += i
			SET_PED_NAME_DEBUG(pedsArmyGuysToHoldOff[i], debugPedName)
		#ENDIF
					
		IF GET_RANDOM_INT_IN_RANGE(0, 10) > 6
			SET_PED_AS_DEFENSIVE_COP(pedsArmyGuysToHoldOff[i], WEAPONTYPE_PISTOL, TRUE)
		ELSE
			SET_PED_AS_DEFENSIVE_COP(pedsArmyGuysToHoldOff[i], WEAPONTYPE_CARBINERIFLE, TRUE)
		ENDIF
		
		//NEW settings
		SET_PED_COMBAT_MOVEMENT(pedsArmyGuysToHoldOff[i], CM_WILLADVANCE)
		SET_PED_COMBAT_ATTRIBUTES(pedsArmyGuysToHoldOff[i], CA_CAN_CHARGE, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(pedsArmyGuysToHoldOff[i], CA_AGGRESSIVE, TRUE)
		
		SET_PED_COMBAT_RANGE(pedsArmyGuysToHoldOff[i], CR_NEAR)
		//end of settings
		
		SET_PED_CONFIG_FLAG(pedsArmyGuysToHoldOff[i], PCF_RunFromFiresAndExplosions, FALSE)		
		
		SET_PED_COMBAT_ATTRIBUTES(pedsArmyGuysToHoldOff[i],	CA_AGGRESSIVE, TRUE)
		SET_PED_AS_OPEN_COMBAT_WITH_CHARGE(pedsArmyGuysToHoldOff[i])
		
		SET_PED_STEERS_AROUND_OBJECTS(pedsArmyGuysToHoldOff[i], FALSE)
		SET_PED_STEERS_AROUND_PEDS(pedsArmyGuysToHoldOff[i], FALSE)
		SET_PED_ANGLED_DEFENSIVE_AREA(pedsArmyGuysToHoldOff[i], <<-169.668091,6292.589355,29.489435>>, <<-203.407669,6259.535156,35.489353>>, 70.000000)

		SET_COMBAT_FLOAT(pedsArmyGuysToHoldOff[i], CCF_STRAFE_WHEN_MOVING_CHANCE, 0.4)
		blipsArmyGuysToHoldOff[i] = CREATE_BLIP_FOR_PED_WRAPPER(pedsArmyGuysToHoldOff[i], TRUE)
		
		
	//	TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedsArmyGuysToHoldOff[i], 170.0)
		bThisPedInjured[i] = FALSE				
		iTimeOfLastRespawn = GET_GAME_TIMER()
		
	ENDIF
	
ENDPROC

PROC MANAGE_EXPLODING_CARS_JUNK_YARD()
	EXPLODE_VEHICLE_AT_HEALTH_LEVEL(ArmyTruckStreet1.thisCar, 500, FALSE, TRUE)
	//EXPLODE_VEHICLE_AT_HEALTH_LEVEL(ArmyTruckStreet2.thisCar, 400, FALSE, TRUE)
ENDPROC

FUNC BOOL IS_PLAYER_NEAR_JUNKYARD_ENTRANCE()
	
	RETURN IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-156.593872,6304.245605,29.948252>>, <<-211.043030,6255.440918,37.989227>>, 158.500000)

ENDFUNC

PROC UPDATE_CREATE_JUNKYARD_JEEPS()
	IF NOT bHaveSetPieceArmyTriggered
		IF HAS_MODEL_LOADED(CRUSADER)	
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(998, "RBHArmySt")
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(999, "RBHArmyST")
		AND IS_PLAYER_NEAR_JUNKYARD_ENTRANCE()
		
			IF NOT REQUEST_SCRIPT_AUDIO_BANK("PS2A_Jeep_Skids")
				SCRIPT_ASSERT("audio didnae load")
			ENDIF
		
			tbTaskArmyTrucksInJunkYard = tbTaskArmyTrucksInJunkYard
			CLEANUP_SET_PIECE_COP_CAR(chinnokLandingAtEndOfStreet, TRUE)
			
			INIT_SET_PIECE_COP_CAR(ArmyTruckStreet1, CRUSADER, 998, "RBHArmySt", VS_FRONT_RIGHT, 5000, 1.00, TRUE)
			INIT_SET_PIECE_COP_CAR(ArmyTruckStreet2, CRUSADER, 999, "RBHArmySt", VS_FRONT_RIGHT, 4000, 1.00, TRUE)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(ArmyTruckStreet1.thisCar, TRUE)
			SET_ENTITY_PROOFS(ArmyTruckStreet1.thisCar, FALSE, FALSE , FALSE, TRUE, FALSE)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(ArmyTruckStreet2.thisCar, TRUE)
			SET_ENTITY_PROOFS(ArmyTruckStreet2.thisCar, FALSE, FALSE , FALSE, TRUE, FALSE)
					
			
			IF IS_ENTITY_OK(copCarsPullingUpStage2[5].thisCar)
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(copCarsPullingUpStage2[5].thisCar)
			ENDIF
			bHaveSetPieceArmyTriggered = TRUE
		
		ENDIF
	ELSE
		MANAGE_EXPLODING_CARS_JUNK_YARD()
		REMOVE_BLIP_AND_SCREAM_WHEN_DEAD(ArmyTruckStreet1.blipDriver, ArmyTruckStreet1.thisDriver)
		REMOVE_BLIP_AND_SCREAM_WHEN_DEAD(ArmyTruckStreet1.blipPassenger, ArmyTruckStreet1.thisPassenger)
		
		REMOVE_BLIP_AND_SCREAM_WHEN_DEAD(ArmyTruckStreet2.blipDriver, ArmyTruckStreet2.thisDriver)
		REMOVE_BLIP_AND_SCREAM_WHEN_DEAD(ArmyTruckStreet2.blipPassenger, ArmyTruckStreet2.thisPassenger)

		IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(ArmyTruckStreet1.thisCar, "RBHArmySt") >= 95.0
		OR (IS_PED_INJURED(ArmyTruckStreet1.thisDriver) AND IS_PED_INJURED(ArmyTruckStreet1.thisPassenger))
			STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(ArmyTruckStreet1)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(ArmyTruckStreet1.thisCar)
			REMOVE_VEHICLE_RECORDING(998, "RBHArmySt")
		ENDIF
	
		IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(ArmyTruckStreet2.thisCar, "RBHArmySt") >= 95.0
		OR (IS_PED_INJURED(ArmyTruckStreet2.thisDriver) AND IS_PED_INJURED(ArmyTruckStreet2.thisPassenger))
			STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(ArmyTruckStreet2)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(ArmyTruckStreet2.thisCar)
			REMOVE_VEHICLE_RECORDING(999, "RBHArmySt")
		ENDIF
	ENDIF
ENDPROC


//INT iCoverUpdateTimer
//INT iChangeGuysEachUpdate

PROC UPDATE_COVER_POINTS_IN_JUNK_YARD()

	INT i

	VECTOR vDefArea
	FLOAT fCoverAreaSize = 2.6

	FOR i = 0 TO 2
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), 	vPossibleCoverPointsJunk[i], <<2.0, 2.0, 2.0>>)
			bCoverStolenJunk[i] = TRUE
			//bCoverTakenJunk[i] = FALSE
		ELSE
			bCoverStolenJunk[i] = FALSE
		ENDIF
	ENDFOR
	
	IF NOT IS_PLAYER_TREVOR()
		WEAPON_TYPE wtTrevorsCurrentWeapon
		IF GET_CURRENT_PED_WEAPON(pedSelector.pedID[SELECTOR_PED_TREVOR], wtTrevorsCurrentWeapon)
		
			IF VDIST(GET_ENTITY_COORDS(pedSelector.pedID[SELECTOR_PED_TREVOR]), vPossibleCoverPointsJunk[0]) < 1.5
			OR VDIST(GET_ENTITY_COORDS(pedSelector.pedID[SELECTOR_PED_TREVOR]), vPossibleCoverPointsJunk[1]) < 1.5
			OR VDIST(GET_ENTITY_COORDS(pedSelector.pedID[SELECTOR_PED_TREVOR]), vPossibleCoverPointsJunk[2]) < 1.5
				IF wtTrevorsCurrentWeapon = WEAPONTYPE_MINIGUN
					//SCRIPT_ASSERT("boom")
					SET_CURRENT_PED_WEAPON(pedSelector.pedID[SELECTOR_PED_TREVOR], WEAPONTYPE_COMBATMG)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	FOR i = 0 TO 2
		IF NOT IS_ENTITY_DEAD(pedSelector.pedID[SELECTOR_PED_MICHAEL])
		AND NOT IS_ENTITY_DEAD(pedSelector.pedID[SELECTOR_PED_TREVOR])
		AND (NOT IS_ENTITY_DEAD(pedGunman) AND bIsBuddyAPro)
//			IF IS_ENTITY_AT_COORD(pedSelector.pedID[SELECTOR_PED_MICHAEL], 	vPossibleCoverPointsJunk[i], <<1.8, 1.8, 2.0>>)
//			OR IS_ENTITY_AT_COORD(pedSelector.pedID[SELECTOR_PED_TREVOR], 	vPossibleCoverPointsJunk[i], <<1.8, 1.8, 2.0>>)
//			OR IS_ENTITY_AT_COORD(pedGunman, 	vPossibleCoverPointsJunk[i], <<1.8, 1.8, 1.8>>)
//			//OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), 	vPossibleCoverPointsJunk[i], <<2.0, 2.0, 2.0>>)
			
			
			bCoverTakenJunk[i] = FALSE
			
			IF PLAYER_PED_ID() != pedSelector.pedID[SELECTOR_PED_MICHAEL]
				IF IS_PED_DEFENSIVE_AREA_ACTIVE(pedSelector.pedID[SELECTOR_PED_MICHAEL], FALSE)
					IF VDIST(GET_PED_DEFENSIVE_AREA_POSITION(pedSelector.pedID[SELECTOR_PED_MICHAEL]), vPossibleCoverPointsJunk[i]) < 0.5
						bCoverTakenJunk[i] = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF PLAYER_PED_ID() != pedSelector.pedID[SELECTOR_PED_TREVOR]
				IF IS_PED_DEFENSIVE_AREA_ACTIVE(pedSelector.pedID[SELECTOR_PED_TREVOR], FALSE)
					IF VDIST(GET_PED_DEFENSIVE_AREA_POSITION(pedSelector.pedID[SELECTOR_PED_TREVOR]), vPossibleCoverPointsJunk[i]) < 0.5
						bCoverTakenJunk[i] = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_PED_DEFENSIVE_AREA_ACTIVE(pedGunman, FALSE)
				IF (VDIST(GET_PED_DEFENSIVE_AREA_POSITION(pedGunman), vPossibleCoverPointsJunk[i]) < 0.5 AND bIsBuddyAPro)
					bCoverTakenJunk[i] = TRUE
				ENDIF
			ENDIF
			
		ENDIF
	ENDFOR
		
	IF GET_GAME_TIMER() - iCoverUpdateTimer > 250
	
		FOR i = 0 TO 2
		
			IF NOT IS_ENTITY_DEAD(pedSelector.pedID[SELECTOR_PED_MICHAEL])
			AND NOT IS_PLAYER_MICHAEL()
				//IF IS_ENTITY_AT_COORD(pedSelector.pedID[SELECTOR_PED_MICHAEL],vPossibleCoverPointsJunk[i], <<2.0, 2.0, 2.0>>)
				//IF VDIST(GET_PED_DEFENSIVE_AREA_POSITION(pedSelector.pedID[SELECTOR_PED_MICHAEL]), vPossibleCoverPointsJunk[i]) < 0.5
				IF bCoverStolenJunk[i] = TRUE
				OR bJustSwitched
					
					REMOVE_PED_DEFENSIVE_AREA(PLAYER_PED_ID())
					
					vDefArea = FREE_COVER_POINT_IN_JUNK_YARD()
					IF VMAG(vDefArea) > 0.0
						//SCRIPT_ASSERT("setting area for Michael")
						SET_PED_SPHERE_DEFENSIVE_AREA(pedSelector.pedID[SELECTOR_PED_MICHAEL], vDefArea, fCoverAreaSize, TRUE)
					ENDIF
											
					bJustSwitched = FALSE
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedSelector.pedID[SELECTOR_PED_TREVOR])
			AND NOT IS_PLAYER_TREVOR()
				//IF IS_ENTITY_AT_COORD(pedSelector.pedID[SELECTOR_PED_TREVOR],vPossibleCoverPointsJunk[i], <<2.0, 2.0, 2.0>>)
				//IF VDIST(GET_PED_DEFENSIVE_AREA_POSITION(pedSelector.pedID[SELECTOR_PED_TREVOR]), vPossibleCoverPointsJunk[i]) < 0.5
				IF bCoverStolenJunk[i] = TRUE
				OR bJustSwitched
					
					REMOVE_PED_DEFENSIVE_AREA(PLAYER_PED_ID())
					
					vDefArea = FREE_COVER_POINT_IN_JUNK_YARD()
					IF VMAG(vDefArea) > 0.0
						//SCRIPT_ASSERT("setting area for Trevor")
						SET_PED_SPHERE_DEFENSIVE_AREA(pedSelector.pedID[SELECTOR_PED_TREVOR], vDefArea, fCoverAreaSize, TRUE)
					ENDIF						
										
					bJustSwitched = FALSE
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedGunman)
			AND bIsBuddyAPro
				//IF IS_ENTITY_AT_COORD(pedGunman,vPossibleCoverPointsJunk[i], <<2.0, 2.0, 2.0>>)
				IF IS_PED_DEFENSIVE_AREA_ACTIVE(pedGunman, FALSE)
					IF VDIST(GET_PED_DEFENSIVE_AREA_POSITION(pedGunman), vPossibleCoverPointsJunk[i]) < 0.5
					AND bCoverStolenJunk[i] = TRUE
					OR bJustSwitched
						
						REMOVE_PED_DEFENSIVE_AREA(PLAYER_PED_ID())
					
						vDefArea = FREE_COVER_POINT_IN_JUNK_YARD()
						IF VMAG(vDefArea) > 0.0
						
							//SCRIPT_ASSERT("setting area for Gunman")
							SET_PED_SPHERE_DEFENSIVE_AREA(pedGunman, vDefArea, fCoverAreaSize, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
								
		ENDFOR
	
		iCoverUpdateTimer = GET_GAME_TIMER()
	
	ENDIF
	
ENDPROC

BOOL bJunkyardAudioScenesSwitched = FALSE
PROC UPDATE_JUNKYARD_AUDIO_SCENE_SWITCH()
	IF NOT bJunkyardAudioScenesSwitched
		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-126.154884,6251.435059,27.183355>>, <<-218.489990,6343.699219,34.287514>>, 4.500000)
			//Hold off the national guard until franklin arrives
			PRINT_NOW("ARMY1", DEFAULT_GOD_TEXT_TIME, 1)
			
			IF IS_AUDIO_SCENE_ACTIVE("PS_2A_CAR_RAMS_BUDDY")
				STOP_AUDIO_SCENE("PS_2A_CAR_RAMS_BUDDY")
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("PS_2A_SHOOTOUT_ALLEY")
				STOP_AUDIO_SCENE("PS_2A_SHOOTOUT_ALLEY")
			ENDIF
			
			START_AUDIO_SCENE("PS_2A_SHOOTOUT_YARD")
			bJunkyardAudioScenesSwitched = TRUE
		ENDIF
	ENDIF
ENDPROC


INT iTimeOfLastGunmanLine

PROC PROCESS_GUNMAN_DIALOGUE()
	
	IF GET_GAME_TIMER() - iTimeOfLastGunmanLine > 10000
	AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
		IF IS_GUNMAN_GUSTAVO()
		OR IS_GUNMAN_CHEF()
		OR IS_GUNMAN_PACKIE()
			IF CREATE_GUNMAN_CONVERSATION("RBH_FIGHT_GM", "", "RBH_FIGHT_CH", "", "RBH_FIGHT_PM")
				iTimeOfLastGunmanLine = GET_GAME_TIMER()	
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PED_INDEX tempArmyVoice

PROC stageHoldOffArmyAtJunkYard()

	KEEP_TIME_LOOKING_SWEET()
	
	
	
	doBuddyFailCheck(1.0, FALSE) //Doesn't check for buddy2 as we kill him off...

	MANAGE_EXTRA_TYRE_POPS()

	checkForPlayerInputThenClearTAsks()

	CONTROL_WALL_RAM_GUNSHOTS()

	//Allow money counter to display when needed.
	IF iJunkYardState >= 9
	//AND iCurrentTake = iDisplayedTake
		
		SET_SWITCH_WHEEL_UNDER_HUD_THIS_FRAME()
		PROCESS_MICHAEL_TREVOR_SWITCH()
	ENDIF
	
	PICKUP_BUDDYS_BAG()

	DISABLE_VEHICLE_EXPLOSION_BREAK_OFF_PARTS()

	//VECTOR vMichaelWayPointStart
	INT iTotalBodyCount
	INT i

	IF iJunkYardState > 0
		UPDATE_CREATE_JUNKYARD_JEEPS()
		UPDATE_JUNKYARD_AUDIO_SCENE_SWITCH()
		
		UPDATE_COVER_POINTS_IN_JUNK_YARD()
		
		PROCESS_GUNMAN_DIALOGUE()
		
	ENDIF

	IF NOT bIsBuddyAPro
		CPRINTLN(DEBUG_MISSION, "iJunkYardState = ", iJunkYardState)
	ENDIF
	
	SWITCH iJunkYardState
	
		CASE 0		
		
			//Makes sure Trevor and Michael are only damagable by the palyer to fix ROb Nelson bug: 1601081
			IF NOT IS_PLAYER_MICHAEL()
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(PedMichael(), TRUE)
			ENDIF
			IF NOT IS_PLAYER_TREVOR()
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedTrevor(), TRUE)
			ENDIF
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(PLAYER_PED_ID(), FALSE)	//Always disable this on the player
		
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(9, "STAGE_HOLD_OFF_ARMY_AT_JUNK_YARD")
			g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = iCurrentTake
			
			vPossibleCoverPointsJunk[0] = <<-170.8159, 6288.7129, 30.4894>>
			vPossibleCoverPointsJunk[1] = <<-173.7987, 6293.0923, 30.4894>>
			vPossibleCoverPointsJunk[2] = <<-172.8244, 6296.8516, 30.4894>>

			IF NOT IS_PED_INJURED(pedMichael())
				SET_PED_SPHERE_DEFENSIVE_AREA(PedMichael(), vPossibleCoverPointsJunk[0], 2.0, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(pedTrevor())
				SET_PED_SPHERE_DEFENSIVE_AREA(pedTrevor(), vPossibleCoverPointsJunk[1], 2.0, TRUE)
			ENDIF
			IF NOT IS_PED_INJURED(pedGunman)
			AND bIsBuddyAPro
				SET_PED_SPHERE_DEFENSIVE_AREA(pedGunman, vPossibleCoverPointsJunk[2], 2.0, TRUE)
			ENDIF
			bJustSwitched  = TRUE
			REMOVE_PED_DEFENSIVE_AREA(PLAYER_PED_ID())

			IF NOT IS_PED_INJURED(pedMichael())
				SET_PED_FIRING_PATTERN(pedMichael(), FIRING_PATTERN_BURST_FIRE)
			ENDIF
			IF NOT IS_PED_INJURED(pedGunman)
				SET_PED_FIRING_PATTERN(pedGunman, FIRING_PATTERN_BURST_FIRE)
			ENDIF
			bBagCreated = FALSE
			bHaveSetPieceArmyTriggered = FALSE
			iShootingSwitchM = 0
			iShootingStageMichael = 0
			iControlRamGunshots = 0					
			iBodyCount = 0
			bJunkyardAudioScenesSwitched = FALSE
			SETUP_WANTED_LEVEL(5, TRUE, 100.0, 10.0, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
			SET_POLICE_RADAR_BLIPS(FALSE)
			DISTANT_COP_CAR_SIRENS(TRUE)
			
			DELETE_ARRAY_OF_PEDS(pedsCopsInStreet, TRUE)
			DELETE_ARRAY_OF_PEDS(pedsExtraSwatInStreet, TRUE)
			DELETE_ARRAY_OF_BLIPS(blipPedsCopsInStreet)
			CLEANUP_SET_PIECE_COP_CAR(continueDownStreetPullUpCar)
			CLEANUP_SET_PIECE_COP_CAR(chopperFlyover)
			
			
			CLEANUP_SET_PIECE_COP_CAR(setPieceRiotVan1)
			CLEANUP_SET_PIECE_COP_CAR(setPieceRiotVan2)
			CLEANUP_SET_PIECE_COP_CAR(setPieceExtraStreetCopCar)
			CLEANUP_SET_PIECE_COP_CAR(setPieceDownStreetInitialCop1)
//			CLEANUP_SET_PIECE_COP_CAR(setPieceDownStreetInitialCop2)
			CLEANUP_SET_PIECE_COP_CAR(setPieceDownStreetInitialCop3)
			
			CLEANUP_SET_PIECE_COP_CAR(setPieceDownStreetSwat2ndWave)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(RIOT)
			SET_MODEL_AS_NO_LONGER_NEEDED(mnSwatPedModel)
			
			//Reset checkForPlayerInputThenClearTAsks() function.
			bPlayerHasTakenControl = FALSE
		
			REMOVE_PTFX_ASSET()
			
			REMOVE_VEHICLE_ASSET(RIOT)
		
			RESET_TYRE_POP_ARRAY()
			
			IF NOT bIsBuddyAPro
				REQUEST_WAYPOINT_RECORDING("RBHArmySec1")
				REQUEST_MODEL(mnCopCarModel)
				REQUEST_MODEL(mnCopPedModel)
				REQUEST_VEHICLE_RECORDING(1, "CBRAM")
				REQUEST_ANIM_DICT("MissHeistPaletoPinned")
			ENDIF
			
			REQUEST_MODEL(CRUSADER)
			REQUEST_VEHICLE_RECORDING(2, "RBHChinDrop")
			REQUEST_MODEL(BARRACKS)
			REQUEST_MODEL(S_M_Y_Marine_03)
			REQUEST_VEHICLE_RECORDING(998, "RBHArmySt")
			REQUEST_VEHICLE_RECORDING(999, "RBHArmySt")
			
			REQUEST_SCRIPT_AUDIO_BANK("PS2A_Jeep_Skids")
			
			IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_TREVOR
				IF wtTrevorWeaponEnteringCutscene = WEAPONTYPE_MINIGUN
					IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), wtTrevorWeaponEnteringCutscene) = 0
						SET_AMMO_IN_CLIP(PLAYER_PED_ID(), wtTrevorWeaponEnteringCutscene, 2000)
					ENDIF
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_MINIGUN, TRUE)
				ENDIF
			ELSE
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wtMichaelWeaponEnteringCutscene, TRUE)//PedMichaelWeaponType, TRUE)
			ENDIF
			
			CREATE_MODEL_HIDE(<<-218.5023, 6271.4863, 30.5479>>, 2.0, PROP_FNCLINK_04F, TRUE)

			START_AUDIO_SCENE("PS_2A_SHOOTOUT_ALLEY")

			TRIGGER_MUSIC_EVENT("RH2A_FIGHT_RAMP_UP")
			FADE_IN_IF_NEEDED()
			IF NOT bIsBuddyAPro
				iJunkYardState = 1
			ELSE				
				//**************************
				///*** Good gunman start ***
				//**************************
				
				SET_PED_COMBAT_RANGE(pedMichael(), CR_NEAR)
				SET_PED_COMBAT_RANGE(pedGunman, CR_NEAR)
				
				//TASK_PEDS_AFTER_ALLEY_CUT()
							
				iJunkYardState = 7
			ENDIF
		BREAK
	
		CASE 1
			IF HAS_ANIM_DICT_LOADED("MissHeistPaletoPinned")			
			AND HAS_MODEL_LOADED(mnCopCarModel)
			AND HAS_MODEL_LOADED(mnCopPedModel)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "CBRAM")
				iJunkYardState++
			ENDIF
		BREAK

		CASE 2
			CPRINTLN(DEBUG_MISSION, "In Junkyard State 2")
			CPRINTLN(DEBUG_MISSION, "Gunman distance to coord is", GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(pedGunman, << -160.3512, 6300.1367, 30.1831 >>))
			IF DOES_ENTITY_EXIST(pedGunman)
				CPRINTLN(DEBUG_MISSION, "Gunman isn't dead")
				CPRINTLN(DEBUG_MISSION, "Gunman distance to coord is ", GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(pedGunman, << -160.3512, 6300.1367, 30.1831 >>))
				IF IS_ENTITY_AT_COORD(pedGunman, << -160.3512, 6300.1367, 30.1831 >>, <<3.5, 3.5, 2.5>>)
					
					SET_VEHICLE_AS_NO_LONGER_NEEDED(carCover)
					SET_MODEL_AS_NO_LONGER_NEEDED(ruiner)
					
					SET_ENTITY_INVINCIBLE(pedGunman, TRUE)
					
					CLEAR_PED_TASKS(pedGunman)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGunman, TRUE)
					REMOVE_PED_DEFENSIVE_AREA(pedGunman)			
					OPEN_SEQUENCE_TASK(seqHoldOffCopsAtBank)
						TASK_SHOOT_AT_COORD(NULL, << -178.5373, 6318.2622, 30.2044 >>, INFINITE_TASK_TIME, FIRING_TYPE_CONTINUOUS)
					CLOSE_SEQUENCE_TASK(seqHoldOffCopsAtBank)
					TASK_PERFORM_SEQUENCE(pedGunman, seqHoldOffCopsAtBank)
					CLEAR_SEQUENCE_TASK(seqHoldOffCopsAtBank)
						
//					IF NOT IS_ENTITY_DEAD(PedMichael())
//						TASK_SHOOT_AT_COORD(PedMichael(), << -178.5373, 6318.2622, 30.2044 >>, 1000, FIRING_TYPE_CONTINUOUS)
//						SET_PED_RESET_FLAG(pedGunman, PRF_InstantBlendToAim)
//					ENDIF		
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PedMichael(), "MICHAEL")
					iJunkYardState++
				ENDIF
			ENDIF
		
		BREAK
				
		CASE 3
			
			IF GET_IS_WAYPOINT_RECORDING_LOADED("RBHArmySec1")
		
				INIT_SET_PIECE_COP_CAR(copCarsPullingUpStage2[5], mnCopCarModel, 1, "CBRAM", VS_FRONT_RIGHT, 2500.0, 1.0, FALSE, DUMMY_MODEL_FOR_SCRIPT, FALSE)
				IF IS_ENTITY_OK(copCarsPullingUpStage2[5].thisCar)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(copCarsPullingUpStage2[5].thisCar, FALSE)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(copCarsPullingUpStage2[5].thisCar, SC_DOOR_BONNET, FALSE)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(copCarsPullingUpStage2[5].thisDriver, TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(copCarsPullingUpStage2[5].thisDriver, TRUE)
					SET_PED_CAN_BE_TARGETTED(copCarsPullingUpStage2[5].thisDriver, FALSE)
					SET_VEHICLE_CAN_BE_TARGETTED(copCarsPullingUpStage2[5].thisCar, FALSE)
					DELETE_PED(copCarsPullingUpStage2[5].thisPassenger)
					
					SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(copCarsPullingUpStage2[5].thisCar, <<-0.1, -0.3, 0.05>>)//<<0.33, -0.30, 0.06>>)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(copCarsPullingUpStage2[5].thisCar, "PS_2A_RAMMING_POLICE_CAR")
				ENDIF
								
				IF NOT IS_ENTITY_DEAD(PedMichael())
					OPEN_SEQUENCE_TASK(seqShootThroughBigFence)
						TASK_SHOOT_AT_ENTITY(NULL, copCarsPullingUpStage2[5].thisDriver, 1000, FIRING_TYPE_CONTINUOUS)
						TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "RBHArmySec1", 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
						TASK_SET_SPHERE_DEFENSIVE_AREA(NULL, << -170.2119, 6288.2974, 30.4905 >>, 1.0)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL, FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 120.0)
					CLOSE_SEQUENCE_TASK(seqShootThroughBigFence)
					TASK_PERFORM_SEQUENCE(PedMichael(), seqShootThroughBigFence)
					CLEAR_SEQUENCE_TASK(seqShootThroughBigFence)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(pedGunman)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedGunman, FALSE)
				ENDIF
				START_AUDIO_SCENE("PS_2A_SHOOTOUT_STREET_CREW_DIES")
				iJunkYardState++
			ENDIF
		BREAK
	
		CASE 4
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(copCarsPullingUpStage2[5].thisCar, "CBRAM") > 54.0
						
				//IF NOT NOT bIsBuddyAPro
			
					
					vRamOffset   = GET_ANIM_INITIAL_OFFSET_POSITION("MissHeistPaletoPinned", "pinned_against_wall_intro_buddy", <<0.0, 0.0, 0.15>>, <<0.0, 0.0, 0.0>>)
					vRamRotation = GET_ANIM_INITIAL_OFFSET_ROTATION("MissHeistPaletoPinned", "pinned_against_wall_intro_buddy", <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						
			
					IF NOT IS_PED_INJURED(copCarsPullingUpStage2[5].thisDriver)
						//FREEZE_ENTITY_POSITION(pedGunman, TRUE)
						
						
						IF NOT IS_ENTITY_DEAD(copCarsPullingUpStage2[5].thisCar)
						AND NOT IS_PED_INJURED(pedGunman)
							//vRamRotation = <<0.0, 0.0, 180.0>>
							PRINTLN("************* BOOOOM!: ", vRamOffset, vRamRotation)
						
							ATTACH_ENTITY_TO_ENTITY(pedGunman, copCarsPullingUpStage2[5].thisCar, 0, vRamOffset, vRamRotation)
							PLAY_SOUND_FROM_ENTITY(-1, "TEST_SCREAM_SHORT", pedGunman)
							SET_ENTITY_COLLISION(pedGunman, FALSE)
							
							SHAKE_GAMEPLAY_CAM("JOLT_SHAKE", 1.5)
							SET_CONTROL_SHAKE(PLAYER_CONTROL, 500, 128)
							
							SET_PED_RELATIONSHIP_GROUP_HASH(pedGunman, dummyGroup)
							
						ELSE
							PRINTLN("************* Buddy or can dead in PINNED animations!: ", vRamOffset, vRamRotation)
						ENDIF
						
						OPEN_SEQUENCE_TASK(seqPlayPinnedAnims)
							
							TASK_PLAY_ANIM(NULL, "MissHeistPaletoPinned", "pinned_against_wall_intro_COP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1) //, AF_NOT_INTERRUPTABLE)
							TASK_PLAY_ANIM(NULL, "MissHeistPaletoPinned", "pinned_against_wall_pro_COP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1) //, AF_NOT_INTERRUPTABLE)
							TASK_PLAY_ANIM(NULL, "MissHeistPaletoPinned", "pinned_against_wall_pro_loop_COP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
						CLOSE_SEQUENCE_TASK(seqPlayPinnedAnims)
								
						TASK_PERFORM_SEQUENCE(copCarsPullingUpStage2[5].thisDriver, seqPlayPinnedAnims)
						
						CLEAR_SEQUENCE_TASK(seqPlayPinnedAnims)
					ENDIF
					
					IF NOT IS_PED_INJURED(pedGunman)
						
						OPEN_SEQUENCE_TASK(seqPlayPinnedAnims)
							
							TASK_PLAY_ANIM(NULL, "MissHeistPaletoPinned", "pinned_against_wall_intro_buddy", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							TASK_PLAY_ANIM(NULL, "MissHeistPaletoPinned", "pinned_against_wall_pro_buddy", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							TASK_PLAY_ANIM(NULL, "MissHeistPaletoPinned", "pinned_against_wall_pro_loop_buddy", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
						CLOSE_SEQUENCE_TASK(seqPlayPinnedAnims)
						
						TASK_PERFORM_SEQUENCE(pedGunman, seqPlayPinnedAnims)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGunman)
						CLEAR_SEQUENCE_TASK(seqPlayPinnedAnims)
						
					ENDIF
				iJunkYardState++	
			ENDIF
		BREAK
		
		CASE 5
			//RBH_MANRESP
			IF IS_PLAYER_TREVOR()
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_MLEAVE", CONV_PRIORITY_VERY_HIGH )						
					iJunkYardState++	
				ENDIF
			ELSE			
				IF GET_RANDOM_BOOL()
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_GNDMIC", CONV_PRIORITY_VERY_HIGH )						
						iJunkYardState++	
					ENDIF
				ELSE
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_TLEAVE2", CONV_PRIORITY_VERY_HIGH )						
						iJunkYardState++	
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 6
		
			IF NOT IS_ENTITY_DEAD(copCarsPullingUpStage2[5].thisCar)
			AND NOT IS_PED_INJURED(pedGunman)
				ATTACH_ENTITY_TO_ENTITY(pedGunman, copCarsPullingUpStage2[5].thisCar, 0, vRamOffset, vRamRotation)
			ENDIF
		
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(copCarsPullingUpStage2[5].thisCar, "CBRAM") > 62.0

				REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipBuddy)
				
				STOP_PLAYBACK_RECORDED_VEHICLE(copCarsPullingUpStage2[5].thisCar)
				FREEZE_ENTITY_POSITION(copCarsPullingUpStage2[5].thisCar, TRUE)
												
				REMOVE_VEHICLE_RECORDING(1, "CBRAM")
				iJunkYardState++		
			ENDIF

		BREAK
		
		//**************************************
		//***   End of Bad Gunman sequence   ***
		//**************************************
		
		CASE 7
		
			IF HAS_MODEL_LOADED(BARRACKS)	
			AND HAS_MODEL_LOADED(S_M_Y_Marine_03)
				//Splitting up entity creation...
				CopCarsParked3[0] = CREATE_VEHICLE(Barracks, << -221.3816, 6296.6538, 30.4905 >>, 319.1075  )
				CopCarsParked3[1] = CREATE_VEHICLE(Barracks, << -212.5283, 6304.9902, 30.4974 >>, 318.0208)
				
				pedsArmyInJunkYard[5] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , << -188.1388, 6284.5415, 30.4901 >>, 321.5203 )
				pedsArmyInJunkYard[6] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , << -182.9337, 6281.8276, 30.4903 >>, 131.0816)
				pedsArmyInJunkYard[7] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , <<-184.0995, 6300.9248, 30.4892>>, 158.3237)
			
				SPAWN_NEW_SET_OF_ARMY_GUYS()
				iJunkYardState++	
			ENDIF
		BREAK
		
		CASE 8
		
			//RUBBER_BAND_WAYPOINT_INTO_JUNKYARD()
		
//			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedGunman)
//				//WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(pedGunman, << -163.4811, 6297.1904, 32.6240 >>, FALSE, FIRING_TYPE_RANDOM_BURSTS)
//				//ADD_EXPLOSION(<< -163.4182, 6297.6895, 35.0740 >>, EXP_TAG_GRENADE, 0.1, FALSE, TRUE, 0.0)
//				//KILL_ANY_CONVERSATION()
//				iJunkYardState++		
//			ELSE
//				//Only skip to next section if Michael shouldn't already be playing waypoint rec.
//				//IF NOT bIsBuddyAPro
//					iJunkYardState++		
//				//ENDIF
//			ENDIF
			iJunkYardState++	
		BREAK
		
		CASE 9
			
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF bIsBuddyAPro
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
						WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(PedMichael(), << -186.2904, 6284.9580, 30.8468 >>, FALSE, FIRING_TYPE_RANDOM_BURSTS)
					ENDIF
					
					//RUBBER_BAND_WAYPOINT_INTO_JUNKYARD()
					
					iJunkYardState++
				ELSE
					//ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "RBHGoodgunman")
					//IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_RAMMED", CONV_PRIORITY_VERY_HIGH )						
					IF (IS_GUNMAN_NORM() AND
						CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_DIE_NR", CONV_PRIORITY_VERY_HIGH))
					OR (IS_GUNMAN_DARYL() AND
						CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "IG_4_P6_A1", CONV_PRIORITY_VERY_HIGH))
					
						IF IS_AUDIO_SCENE_ACTIVE("PS_2A_SHOOTOUT_STREET_CREW_DIES")
							STOP_AUDIO_SCENE("PS_2A_SHOOTOUT_STREET_CREW_DIES")
						ENDIF
						IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
							WAYPOINT_PLAYBACK_START_SHOOTING_AT_COORD(PedMichael(), << -186.2904, 6284.9580, 30.8468 >>, FALSE, FIRING_TYPE_RANDOM_BURSTS)
						ENDIF
						iJunkYardState++
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
	

		CASE 10	
			IF bIsBuddyAPro
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					///F CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_FENCE3", CONV_PRIORITY_VERY_HIGH )
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_ARMY", CONV_PRIORITY_VERY_HIGH )					    	 
						iJunkYardState++
					ENDIF
				ENDIF
			ELSE
				iJunkYardState++
			ENDIF
		BREAK
			
		CASE 11			
		
			//RUBBER_BAND_WAYPOINT_INTO_JUNKYARD()

			IF HAS_MODEL_LOADED(BARRACKS)
			AND HAS_MODEL_LOADED(S_M_Y_Marine_03)
				SETTIMERA(0)
				iJunkYardState++
			ENDIF
			
		BREAK
	
		CASE 12
		
			//RUBBER_BAND_WAYPOINT_INTO_JUNKYARD()
		
			//IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-126.154884,6251.435059,27.183355>>, <<-218.489990,6343.699219,34.287514>>, 4.500000)
			IF TIMERA() > 500
				//Hold off the national guard until franklin arrives
				//PRINT_NOW("ARMY1", DEFAULT_GOD_TEXT_TIME, 1)
				SET_PED_RUN_INTO_POSITION_AND_COMBAT_DA(pedsArmyInJunkYard[1], <<-196.5465, 6287.7700, 30.4891>>,   TRUE, 4.0, TRUE)
				SET_PED_RUN_INTO_POSITION_AND_COMBAT_DA(pedsArmyInJunkYard[2], << -188.4366, 6272.0742, 30.4905 >>,  TRUE, 4.0, TRUE)
				SET_PED_RUN_INTO_POSITION_AND_COMBAT_DA(pedsArmyInJunkYard[3], << -203.0336, 6283.3296, 30.4904 >>, TRUE, 4.0, TRUE)
				SET_PED_RUN_INTO_POSITION_AND_COMBAT_DA(pedsArmyInJunkYard[4], <<-200.5304, 6280.5313, 30.4718>>,  TRUE, 4.0, TRUE)
				
				SET_PED_RUN_INTO_POSITION_AND_COMBAT_DA(pedsArmyInJunkYard[5], << -188.1388, 6284.5415, 30.4901 >>, TRUE, 4.0, TRUE)
				SET_PED_RUN_INTO_POSITION_AND_COMBAT_DA(pedsArmyInJunkYard[6], << -182.9337, 6281.8276, 30.4903 >>, TRUE, 4.0, TRUE)
			
				//SET_PED_RUN_INTO_POSITION_AND_COMBAT_DA(pedsArmyInJunkYard[7], <<-184.0995, 6300.9248, 30.4892>>, TRUE, 0, PEDMOVE_RUN, FALSE, FALSE, 4.0, FALSE)
				IF NOT IS_PED_INJURED(pedsArmyInJunkYard[7])
					SET_PED_COMBAT_ATTRIBUTES(pedsArmyInJunkYard[7], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
					SET_PED_SPHERE_DEFENSIVE_AREA(pedsArmyInJunkYard[7], <<-184.0995, 6300.9248, 30.4892>>, 2.5)
					SET_PED_COMBAT_MOVEMENT(pedsArmyInJunkYard[7], CM_DEFENSIVE)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedsArmyInJunkYard[7], "COVER_ME", "s_m_y_genericmarine_01_black_mini_01", SPEECH_PARAMS_FORCE)
				ENDIF
				
				
				SET_MODEL_AS_NO_LONGER_NEEDED(BARRACKS)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(CopCarsParked3[0])
				SET_VEHICLE_AS_NO_LONGER_NEEDED(CopCarsParked3[1])
				
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					GET_CLOSEST_PED(GET_ENTITY_COORDS(PLAYER_PED_ID()), 40.0, TRUE, TRUE, tempArmyVoice, FALSE, TRUE)
					IF DOES_ENTITY_EXIST(tempArmyVoice)
						ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 8, tempArmyVoice, "RBHArmy")
					ENDIF
					//Add ambient dialague!!!
					//ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "RBH2AUD", "RBH_ARMYCHT", CONV_PRIORITY_HIGH)
				ENDIF			
				REQUEST_MODEL(CRUSADER)
				REQUEST_VEHICLE_RECORDING(998, "RBHArmySt")
				REQUEST_VEHICLE_RECORDING(999, "RBHArmySt")
				iJunkYardState++
			ENDIF
		BREAK

		
		CASE 13
			iJunkYardState++
		BREAK
		
		CASE 14
		
			//RUBBER_BAND_WAYPOINT_INTO_JUNKYARD()
		
//			IF NOT IS_PED_INJURED(PedMichael())
//				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(PedMichael())
//					KEEP_YOUR_FRIENDS_CLOSE(PedMichael())
//				ENDIF
//			ENDIF
		
			FOR i = 0 TO NUMBER_OF_INITIAL_ARMY_PEDS -1
				REMOVE_BLIP_AND_SCREAM_WHEN_DEAD(blippedsArmyInJunkYard[i], pedsArmyInJunkYard[i], TRUE)
			ENDFOR
			
			REQUEST_MODEL(CRUSADER)
			REQUEST_VEHICLE_RECORDING(998, "RBHArmySt")
			REQUEST_VEHICLE_RECORDING(999, "RBHArmySt")
		
			IF HAS_MODEL_LOADED(CRUSADER)	
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(998, "RBHArmySt")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(999, "RBHArmyST")
			
				IF IS_PED_INJURED(pedsArmyInJunkYard[1])
				OR IS_PED_INJURED(pedsArmyInJunkYard[2])
				OR IS_PED_INJURED(pedsArmyInJunkYard[3])
				OR IS_PED_INJURED(pedsArmyInJunkYard[4])
				OR IS_PED_INJURED(pedsArmyGuysToHoldOff[0])
				OR IS_PED_INJURED(pedsArmyGuysToHoldOff[1])
				OR IS_PED_INJURED(pedsArmyGuysToHoldOff[2])
				OR IS_PED_INJURED(pedsArmyGuysToHoldOff[3])
				OR IS_PED_INJURED(ArmyTruckStreet1.thisDriver)
				OR IS_PED_INJURED(ArmyTruckStreet1.thisPassenger)
				OR IS_PED_INJURED(ArmyTruckStreet1.thisPassenger2)
					IF NOT IS_PED_INJURED(pedsArmyInJunkYard[1])
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsArmyInJunkYard[1], FALSE)
					ENDIF
					SET_PED_AS_NO_LONGER_NEEDED(pedsArmyInJunkYard[1])
					IF NOT IS_PED_INJURED(pedsArmyInJunkYard[2])
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsArmyInJunkYard[2], FALSE)
					ENDIF
					SET_PED_AS_NO_LONGER_NEEDED(pedsArmyInJunkYard[2])
					IF NOT IS_PED_INJURED(pedsArmyInJunkYard[3])
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsArmyInJunkYard[3], FALSE)
					ENDIF
					SET_PED_AS_NO_LONGER_NEEDED(pedsArmyInJunkYard[3])
					IF NOT IS_PED_INJURED(pedsArmyInJunkYard[4])
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsArmyInJunkYard[4], FALSE)
					ENDIF
					SET_PED_AS_NO_LONGER_NEEDED(pedsArmyInJunkYard[4])	
					SPAWN_NEW_SET_OF_ARMY_GUYS()
					
					SET_PED_RUN_INTO_POSITION_AND_COMBAT_DA(pedsArmyInJunkYard[1], << -193.5920, 6291.4912, 30.4901 >>, TRUE, 4.0)
					SET_PED_RUN_INTO_POSITION_AND_COMBAT_DA(pedsArmyInJunkYard[2], << -188.4366, 6272.0742, 30.4905 >>, TRUE, 4.0)
					SET_PED_RUN_INTO_POSITION_AND_COMBAT_DA(pedsArmyInJunkYard[3], << -203.0336, 6283.3296, 30.4904 >>, TRUE, 4.0)
					SET_PED_RUN_INTO_POSITION_AND_COMBAT_DA(pedsArmyInJunkYard[4], << -187.7998, 6283.6255, 30.4903 >>, TRUE, 4.0)
			
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedsArmyInJunkYard[4], "COVER_ME", "s_m_y_genericmarine_01_black_mini_01", SPEECH_PARAMS_FORCE)
			
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						GET_CLOSEST_PED(GET_ENTITY_COORDS(PLAYER_PED_ID()), 40.0, TRUE, TRUE, tempArmyVoice, FALSE, TRUE)
						IF DOES_ENTITY_EXIST(tempArmyVoice)
							IF NOT IS_PED_INJURED(tempArmyVoice)
								REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, 8)
								ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 8, tempArmyVoice, "RBHArmy")
								ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "RBH2AUD", "RBH_ARMYCHT", CONV_PRIORITY_HIGH)
							ENDIF	
						ENDIF	
					ENDIF
					bAllowChinookRestart = TRUE//iDealWithChinooks = 0 //Restart the chinooks for 2nd flyover!!
					iJunkYardState++
				ENDIF
			ENDIF
		BREAK
	
		CASE 15
		
			//KEEP_YOUR_FRIENDS_CLOSE(PedMichael())
		
			FOR i = 0 TO NUMBER_OF_INITIAL_ARMY_PEDS -1
				REMOVE_BLIP_AND_SCREAM_WHEN_DEAD(blippedsArmyInJunkYard[i], pedsArmyInJunkYard[i])		
			ENDFOR
			
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				KILL_ANY_CONVERSATION()
				//IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_FARM", CONV_PRIORITY_VERY_HIGH)
				IF IS_GUNMAN_PACKIE()
				OR IS_GUNMAN_GUSTAVO()
				OR IS_GUNMAN_CHEF()
					IF CREATE_GUNMAN_CONVERSATION("RBH_ARMY_GM", "", "RBH_ARMY_CH", "", "RBH_ARMY_PM")
						iJunkYardState++
					ENDIF
				ELSE
					iJunkYardState++
				ENDIF
			ENDIF
		BREAK
		
		CASE 16
		
			//KEEP_YOUR_FRIENDS_CLOSE(PedMichael())
			
			COMBAT_ARMY_PEDS_MICHAEL()
		
			CHECK_SET_PIECE_FOR_BLIP_REMOVAL(ArmyTruckStreet1)
			
			IF IS_ENTITY_DEAD(ArmyTruckStreet1.thisCar)
			AND IS_ENTITY_DEAD(ArmyTruckStreet2.thisCar)
				iJunkYardState=17
			ENDIF			
		BREAK
		
		CASE 17
		
			EXPLODE_VEHICLE_AT_HEALTH_LEVEL_NO_BUDDY(ArmyTruckStreet1.thisCar, iWidgetCopCarExplosionHealth)
			EXPLODE_VEHICLE_AT_HEALTH_LEVEL_NO_BUDDY(ArmyTruckStreet2.thisCar, iWidgetCopCarExplosionHealth)
		
			IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_ARV1", CONV_PRIORITY_VERY_HIGH)
				iJunkYardState = 23
			ENDIF
		BREAK
		
		CASE 23
		
			//KEEP_YOUR_FRIENDS_CLOSE(PedMichael())
			COMBAT_ARMY_PEDS_MICHAEL()

			FOR i = 0 TO NUMBER_OF_INITIAL_ARMY_PEDS -1
				REMOVE_BLIP_AND_SCREAM_WHEN_DEAD(blippedsArmyInJunkYard[i], pedsArmyInJunkYard[i])
			ENDFOR
		
			//Wait til you have killed one of those motherfuckers
			
			REQUEST_MODEL(BARRACKS)
			
			IF HAS_MODEL_LOADED(BARRACKS)
				iJunkYardState = 25
			ENDIF
			
		BREAK
			
		CASE 25
		
			FOR i = 0 TO NUMBER_OF_INITIAL_ARMY_PEDS -1
				REMOVE_BLIP_AND_SCREAM_WHEN_DEAD(blippedsArmyInJunkYard[i], pedsArmyInJunkYard[i])
			ENDFOR
			
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				GET_CLOSEST_PED(GET_ENTITY_COORDS(PLAYER_PED_ID()), 40.0, TRUE, TRUE, tempArmyVoice, FALSE, TRUE)
				IF DOES_ENTITY_EXIST(tempArmyVoice)
					IF NOT IS_ENTITY_DEAD(tempArmyVoice)
						REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, 8)
						ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 8, tempArmyVoice, "RBHArmy")
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "RBH2AUD", "RBH_ARMYCHT", CONV_PRIORITY_HIGH)
					ENDIF
				ENDIF
			ENDIF
			//Allow BARRACKS model to stream out
			SET_MODEL_AS_NO_LONGER_NEEDED(BARRACKS)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(CopCarsParked3[0])
			SET_VEHICLE_AS_NO_LONGER_NEEDED(CopCarsParked3[1])
			SETTIMERA(0)
			iJunkYardState++	
			
		BREAK
		
		CASE 26
					
			//KEEP_YOUR_FRIENDS_CLOSE(PedMichael())
			COMBAT_ARMY_PEDS_MICHAEL()
			
			FOR i = 0 TO NUMBER_OF_INITIAL_ARMY_PEDS -1
				REMOVE_BLIP_AND_SCREAM_WHEN_DEAD(blippedsArmyInJunkYard[i], pedsArmyInJunkYard[i])
			ENDFOR
			
			iTotalBodyCount = iBodyCount + GET_NUMBER_OF_DEAD_PEDS(pedsArmyInJunkYard)
			CPRINTLN(DEBUG_MISSION, "iTotalBodyCount = ", iTotalBodyCount)
			
			IF iTotalBodyCount >= 10
				iJunkYardState = 27
			ENDIF
		BREAK
			
		CASE 27
				
			cleanUpHoldOffCopsAtUsedCarLot()
						
			mission_stage = STAGE_HOT_SWAP_TO_BULLDOZER
			
		BREAK
	
	ENDSWITCH
	
	IF NOT IS_PED_INJURED(copCarsPullingUpStage2[5].thisDriver)
		SET_PED_RESET_FLAG(copCarsPullingUpStage2[5].thisDriver, PRF_DisablePotentialBlastReactions, TRUE)
		SET_PED_CONFIG_FLAG(copCarsPullingUpStage2[5].thisDriver, PCF_RunFromFiresAndExplosions, FALSE)
	ENDIF
	
	IF iJunkYardState > 19
		MANAGE_ARMY_RESPAWN_WAITING_FOR_FRANKLIN_VERSION_2()
	ENDIF
	
	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chinookDropSetPiece.thisCar, "RBHChinDrop") >= 65.0	
		IF NOT IS_ENTITY_DEAD(chinookDropSetPiece.thisCar)
		
			PRINTLN( GET_ENTITY_HEALTH(chinookDropSetPiece.thisCar), "<------------*****!!!")
		
			IF GET_ENTITY_HEALTH(chinookDropSetPiece.thisCar) < 300
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(chinookDropSetPiece.thisCar)
					STOP_PLAYBACK_RECORDED_VEHICLE(chinookDropSetPiece.thisCar)
					SET_VEHICLE_OUT_OF_CONTROL(chinookDropSetPiece.thisCar, TRUE, TRUE)
				ENDIF
			ENDIF
		ENDIF	
	ENDIF		
	
	//Control the guys war cries!
	IF TIMERB() > 7000
	AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
	AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
	AND iJunkYardState >= 14
		IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
			IF IS_PED_SHOOTING(PLAYER_PED_ID())
				CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_ARMYT", CONV_PRIORITY_HIGH)
				SETTIMERB(0)
			ENDIF
		ELSE
			IF NOT IS_ENTITY_DEAD(PedMichael())
				IF IS_PED_SHOOTING(PLAYER_PED_ID())
					CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_ARMYM", CONV_PRIORITY_HIGH)
					SETTIMERB(0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	

ENDPROC

//INT iTimeOfLastScream
//
//FLOAT fFleeDistance[NUMBER_OF_COPS_DOZER_STAGE]
//
//PROC GENERATE_FLEE_DISTANCES()
//	INT i
//	FOR i = 0 TO NUMBER_OF_COPS_DOZER_STAGE  -1
//		 fFleeDistance[i] = GET_RANDOM_FLOAT_IN_RANGE(4.25, 6.5)	
//	ENDFOR
//ENDPROC


BOOL bAlternateFranklinCalls
structTimer tmrFranklinSwitch
CAMERA_INDEX	camFranklinSwitchFirstFrame
CAMERA_INDEX	camFranklinSwitchAnimated
INT				iSynchSceneCamera
SWITCH_STATE swState

// Failsafes!  These are REALLY BAD, and hopefully can be removed soon, but there is an issue where
// the mission won't force-switch to Franklin when we want it to.  Appears to be a memory issue, 
// so this failsafe timer will bypass the streaming stage and allow the switch to happen.  THIS IS AN
// AWFUL HACK, and may cause major problems during the bulldozer stage
structTimer tmrOMGHAXFailsafeTimer
BOOL	bOMGHAXFailsafeTriggered
PROC stageSwitchToBullDozer()
	
	INT i
	
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS ( <<23.6, 6546.8, 32.0>>, 2.0, PROP_FNCLINK_03GATE1)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_FNCLINK_03GATE1, <<23.6, 6546.8, 32.0>>, TRUE, 1.0)
	ENDIF
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS ( <<15.4, 6538.5, 32.0>>, 2.0, PROP_FNCLINK_03GATE4)
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_FNCLINK_03GATE4, <<15.4, 6538.5, 32.0>>, TRUE, -1.0)
	ENDIF
	
	DEAL_WITH_CHINOOK(6000)
	
	IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
		THROW_BANK_NOTES_IN_AIR_WHEN_SHOT(pedMichael(), 2500, 1000, TIMERB())
	ENDIF
	
	MANAGE_ARMY_RESPAWN_WAITING_FOR_FRANKLIN_VERSION_2()

	CONTROL_WALL_RAM_GUNSHOTS()

	CPRINTLN(DEBUG_MISSION, "iStageSwitchToBullDozer = ", iStageSwitchToBullDozer )

	SWITCH iStageSwitchToBullDozer
	
		CASE 0
			bForceCleanupOfRammingCar = TRUE
		
			removeAllBlipsCops3()
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(X, "STAGE_HOT_SWAP_TO_BULLDOZER")
			REMOVE_ANIM_DICT("MissHeistPaletoScore1")
			
			REMOVE_ANIM_DICT("MissHeistPaletoPinned")
			
			CLEANUP_SET_PIECE_COP_CAR(ArmyTruckStreet1)
			CLEANUP_SET_PIECE_COP_CAR(ArmyTruckStreet2)
			
			CANCEL_TIMER(tmrFranklinSwitch)
			
			REQUEST_MODEL(BULLDOZER)
			REQUEST_MODEL(CRUSADER)
			REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
			REQUEST_ANIM_DICT("missheistpaletoscore2switch_bulldozer")
			REQUEST_VEHICLE_RECORDING(1, "Dozer")
			
			//REQUEST_VEHICLE_RECORDING(1, "ChargingTrucks")
			REQUEST_VEHICLE_RECORDING(2, "ChargingTrucks")
						
			REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipMichael)
			REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipBuddy)
			
			SETUP_WANTED_LEVEL(5, TRUE, 100.0, 10.0, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
			//Fix for 297916
			
			PREPARE_MUSIC_EVENT("RH2A_SWITCH_1")
			bAlternateFranklinCalls = FALSE
			IF NOT IS_PED_INJURED(pedMichael())
				SET_PED_SPHERE_DEFENSIVE_AREA(pedMichael(), <<-200.9841, 6284.2734, 30.4892>>, 3.0)
			ENDIF
			
			IF NOT IS_PED_INJURED(pedGunman)
			AND bIsBuddyAPro
				SET_PED_SPHERE_DEFENSIVE_AREA(pedGunman, <<-198.0981, 6281.8379, 30.4845>>, 3.0)
			ENDIF
			
			CREATE_MODEL_HIDE(<<-218.5023, 6271.4863, 30.5479>>, 2.0, PROP_FNCLINK_04F, TRUE)
				
			FADE_IN_IF_NEEDED()
			
			bOMGHAXFailsafeTriggered = FALSE
			
			SETTIMERA(0)
			SETTIMERB(0)
			iStageSwitchToBullDozer++
		BREAK
		
		CASE 1
			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chinooks[0], "RBHChin") > 21.5//21.0
			
//				SET_CAM_COORD(initialCam,<<-190.995239,6283.070800,32.258236>>)
//				SET_CAM_ROT(initialCam,<<-6.479186,-2.470430,-150.727753>>)
//				SET_CAM_FOV(initialCam,25.712664)
//
//				SET_CAM_COORD(destinationCam,<<-188.659683,6282.004883,31.962132>>)
//				SET_CAM_ROT(destinationCam,<<4.940372,-2.470428,99.996994>>)
//				SET_CAM_FOV(destinationCam,24.124851)

				SET_CAM_COORD(initialCam,<<-190.174149,6282.393555,31.954981>>)
				SET_CAM_ROT(initialCam,<<10.181311,-3.276431,177.878098>>)
				SET_CAM_FOV(initialCam,32.724461)

				SET_CAM_COORD(destinationCam,<<-189.226685,6281.786133,31.887020>>)
				SET_CAM_ROT(destinationCam,<<10.181314,-3.276432,94.829697>>)
				SET_CAM_FOV(destinationCam,32.724461)
				
				SHAKE_CAM(destinationCam, "ROAD_VIBRATION_SHAKE", 1.0)

				IF DOES_ENTITY_EXIST(pedGunman)
					IF NOT IS_PED_INJURED(pedGunman)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGunman, TRUE)
						CLEAR_PED_TASKS_IMMEDIATELY(pedGunman)
						SET_ENTITY_COORDS(pedGunman, <<-190.0519, 6281.4531, 30.4891>>)
						SET_ENTITY_HEADING(pedGunman, 88.9563)
						
						TASK_AIM_GUN_AT_COORD(pedGunman,  <<-285.3361, 6285.9922, 53.2497>>, 5000)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGunman)
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED(pedMichael())
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMichael(), TRUE)
						CLEAR_PED_TASKS_IMMEDIATELY(pedMichael())
						SET_ENTITY_COORDS(pedMichael(), <<-190.0519, 6281.4531, 30.4891>>)
						SET_ENTITY_HEADING(pedMichael(), 88.9563)
					
						TASK_AIM_GUN_AT_COORD(pedMichael(),  <<-285.3361, 6285.9922, 53.2497>>, 5000)
						
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(pedMichael())
					TASK_AIM_GUN_AT_COORD(pedMichael(),  <<-285.3361, 6285.9922, 53.2497>>, 5000)
				ENDIF
				//TASK_AIM_GUN_AT_COORD(PLAYER_PED_ID(),  <<-285.3361, 6285.9922, 53.2497>>, 5000)
				
				SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 2000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)	
				SHAKE_CAM(destinationCam, "HAND_SHAKE", 0.75)
				SET_CUTSCENE_RUNNING(TRUE)
		
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
		
				KILL_ANY_CONVERSATION()
		
				SETTIMERA(0)
				iStageSwitchToBullDozer++
			ENDIF
		BREAK
		
		CASE 2
			
			IF bIsBuddyAPro
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, NULL, "FRANKLIN")
				//ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedGunman, "RBHGOODGUNMAN")
				//IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_FLYTNKGG", CONV_PRIORITY_VERY_HIGH)
				IF CREATE_GUNMAN_CONVERSATION("RBH_FLYTNKGG", "", "RBH_TANK_CH", "", "RBH_TANK_PM")
					iStageSwitchToBullDozer++		
				ENDIF
			ELSE
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, NULL, "FRANKLIN")
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_FLYINTNK", CONV_PRIORITY_VERY_HIGH)
					iStageSwitchToBullDozer++		
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 3
			IF TIMERA() > 2000
				SET_CAM_COORD(initialCam,<<-189.226685,6281.786133,31.887020>>)
				SET_CAM_ROT(initialCam,<<10.181314,-3.276432,94.829697>>)
				SET_CAM_FOV(initialCam,32.724461)
				
				SET_CAM_COORD(initialCam,GET_FINAL_RENDERED_CAM_COORD())
				SET_CAM_ROT(initialCam,GET_FINAL_RENDERED_CAM_ROT())
				SET_CAM_FOV(initialCam,32.724461)
				
				SET_CAM_COORD(destinationCam,<<-189.226685,6281.786133,31.887020>>)
				SET_CAM_ROT(destinationCam,<<10.181313,-3.276431,93.643410>>)
				SET_CAM_FOV(destinationCam,23.706486)

				SHAKE_CAM(destinationCam, "ROAD_VIBRATION_SHAKE", 1.0)

				SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 666)	
				SETTIMERA(0)
				iStageSwitchToBullDozer++	
			ENDIF
		BREAK
		
		CASE 4
			IF TIMERA() > 666
				SETTIMERA(0)
				iStageSwitchToBullDozer++	
			ENDIF
		BREAK
		
		CASE 5
			IF TIMERA() > 1000 //666
			//IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chinooks[0], "RBHChin") > 25.0
				//change angle
				
//				SET_CAM_COORD(initialCam,<<-314.028442,6290.714355,55.799244>>)
//				SET_CAM_ROT(initialCam,<<-4.614602,2.660856,-94.729012>>)
//				SET_CAM_FOV(initialCam,23.371109)
//
//				SET_CAM_COORD(destinationCam,<<-314.288116,6290.894531,52.408436>>)
//				SET_CAM_ROT(destinationCam,<<-4.614602,2.660856,-94.729012>>)
//				SET_CAM_FOV(destinationCam,23.371109)
				
//				SET_CAM_COORD(initialCam,<<-331.305725,6290.306641,51.030296>>)
//				SET_CAM_ROT(initialCam,<<-5.573050,2.660809,-92.235886>>)
//				SET_CAM_FOV(initialCam,25.061300)
//
//				SET_CAM_COORD(destinationCam,<<-283.704102,6285.799316,47.213955>>)
//				SET_CAM_ROT(destinationCam,<<-9.215590,2.660808,-87.537437>>)
//				SET_CAM_FOV(destinationCam,25.061300)
				
				SET_CAM_COORD(initialCam,<<-375.483765,6283.354004,53.162518>>)
				SET_CAM_ROT(initialCam,<<-3.986583,2.654638,-93.821449>>)
				SET_CAM_FOV(initialCam,25.061300)

				SET_CAM_COORD(destinationCam,<<-325.819031,6282.219727,52.987309>>)
				SET_CAM_ROT(destinationCam,<<-10.952655,2.654633,-88.004578>>)
				SET_CAM_FOV(destinationCam,25.061300)
				
				SHAKE_CAM(destinationCam, "HAND_SHAKE", 1.0)
				
				SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 3000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)	
				SETTIMERA(0)
				iStageSwitchToBullDozer++
			ENDIF
		BREAK
				
		CASE 6
			//IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chinooks[0], "RBHChin") > 28.0
			IF TIMERA() > 2500	
				IF NOT IS_PED_INJURED(pedMichael())
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMichael(), FALSE)
				ENDIF
				
				IF NOT IS_PED_INJURED(pedGunman)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedGunman, FALSE)
				ENDIF
				
				IF NOT IS_PED_IN_COVER(PLAYER_PED_ID())
					SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_HEADING_BETWEEN_VECTORS_2D(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(chinooks[0])))
					CLEAR_PED_TASKS(PLAYER_PED_ID())
				ENDIF
				
				IF bIsBuddyAPro
					INCREMENT_ALL_CREW_MEMBER_STATS_DURING_HEIST(HEIST_RURAL_BANK, TRUE)
					//INCREMENT_GUNMAN_STATS_DURING_HEIST(HEIST_RURAL_BANK, GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_RURAL_BANK, 0))
				ENDIF
				
				// Start the OMGHAX timer to try to catch the streaming issue
				RESTART_TIMER_NOW(tmrOMGHAXFailsafeTimer)
				RESET_GAME_CAMERA()
				
				REPLAY_STOP_EVENT()
				
				SET_CUTSCENE_RUNNING(FALSE)
				iStageSwitchToBullDozer++
			ENDIF
		BREAK
		
		CASE 7
		
			doBuddyFailCheck(1.0, FALSE) //Doesn't check for buddy2 as we kill him off...
		
			// OMGHAX - If we're stuck in this stage for more than 15 seconds, memory's done got fucked up.
			// We're gonna skip this stage, and hopefully everything will be ok?!?!?
			IF NOT bOMGHAXFailsafeTriggered
				IF TIMER_DO_ONCE_WHEN_READY(tmrOMGHAXFailsafeTimer, 15)
					bOMGHAXFailsafeTriggered = TRUE
				ENDIF
			ENDIF
		
			IF HAS_MODEL_LOADED(BULLDOZER)
			 AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Dozer")
			 AND HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
			 AND HAS_ANIM_DICT_LOADED("missheistpaletoscore2switch_bulldozer")
			OR bOMGHAXFailsafeTriggered
			
				carBulldozer = CREATE_VEHICLE(BULLDOZER, << 65.3256, 6561.9180, 27.6343 >>, 153.5209)
								
				CREATE_PLAYER_PED_INSIDE_VEHICLE(pedSelector.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, carBulldozer)
				PRELOAD_OUTFIT(pedSelector.pedID[SELECTOR_PED_FRANKLIN], OUTFIT_P1_STEALTH_NO_MASK)
				SET_PED_COMP_ITEM_CURRENT_SP(pedSelector.pedID[SELECTOR_PED_FRANKLIN], COMP_TYPE_OUTFIT, OUTFIT_P1_STEALTH_NO_MASK, FALSE)
				
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedSelector.pedID[SELECTOR_PED_FRANKLIN], FALSE)
				sCamDetails.pedTo = pedSelector.pedID[SELECTOR_PED_FRANKLIN]
				
				//SET_PED_COMP_ITEM_CURRENT_SP(pedSelector.pedID[SELECTOR_PED_FRANKLIN], COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT)
				
				SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_FRANKLIN, FALSE)
				SET_SELECTOR_PED_HINT(pedSelector, SELECTOR_PED_FRANKLIN, TRUE)
				SET_SELECTOR_PED_HINT(pedSelector, SELECTOR_PED_MICHAEL, FALSE)
				SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_MICHAEL, TRUE)
				SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_TREVOR, TRUE)
				//Switch To Franklin.
			
						
				//PRINT_NOW("RBHSWITCH", 30000, 1 )// DEFAULT_GOD_TEXT_TIME, 1)
				
				iStageSwitchToBullDozer++
			ENDIF
		
		BREAK

		//CASE 6
		//CASE 7
		CASE 8
		CASE 9
		CASE 10
		CASE 11
		CASE 12
					
			doBuddyFailCheck(0.75, FALSE) //Doesn't check for buddy2 as we kill him off...
			
			IF iStageSwitchToBullDozer = 8
				IF NOT bIsBuddyAPro				
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						
						IF IS_GUNMAN_NORM()
							IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_NOGUN", CONV_PRIORITY_VERY_HIGH )
								iStageSwitchToBullDozer = 9
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_NOGUND", CONV_PRIORITY_VERY_HIGH )
								iStageSwitchToBullDozer = 9
							ENDIF
						ENDIF
					ENDIF
				ELSE
					iStageSwitchToBullDozer = 9
				ENDIF
			ENDIF

			IF iStageSwitchToBullDozer = 9
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_HOLD", CONV_PRIORITY_VERY_HIGH )
						LOAD_STREAM("PS2A_DIGGER_GUNFIGHT_DISTANT_MASTER")
						iStageSwitchToBullDozer = 10
					ENDIF
				ENDIF
			ENDIF

			IF iStageSwitchToBullDozer = 10
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_HOLD2", CONV_PRIORITY_VERY_HIGH )
						iStageSwitchToBullDozer = 11
					ENDIF
				ENDIF
			ENDIF
			/*						
			IF iStageSwitchToBullDozer = 9
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
					PRINT("RBHSWITCH", DEFAULT_GOD_TEXT_TIME, 1 )
					iStageSwitchToBullDozer = 10
				ENDIF
			ENDIF
			
			IF iStageSwitchToBullDozer = 10
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_NEEDF", CONV_PRIORITY_VERY_HIGH )
						iStageSwitchToBullDozer = 11
					ENDIF
				ENDIF
			ENDIF
			*/
			FOR i = 0 TO NUMBER_OF_INITIAL_ARMY_PEDS -1
				REMOVE_BLIP_AND_SCREAM_WHEN_DEAD(blippedsArmyInJunkYard[i], pedsArmyInJunkYard[i])
			ENDFOR
			
			//Remind player.....
			IF TIMERA() > 10000
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					IF bAlternateFranklinCalls
						IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_RADIOM", CONV_PRIORITY_VERY_HIGH )
							bAlternateFranklinCalls = FALSE
							SETTIMERA(0)
						ENDIF
					ELSE
						IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_NEEDF", CONV_PRIORITY_VERY_HIGH )
							bAlternateFranklinCalls = TRUE
							SETTIMERA(0)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			UPDATE_SELECTOR_HUD(pedSelector)
			
			IF HAS_SELECTOR_PED_BEEN_SELECTED(pedSelector, SELECTOR_PED_FRANKLIN)

				iStageSwitchToBullDozer = 13
			ENDIF
			
			IF (m_bForceSwapToFranklin 
			 AND iStageSwitchToBullDozer > 10
			 AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF))
			OR bOMGHAXFailsafeTriggered
				IF NOT IS_TIMER_STARTED(tmrFranklinSwitch)
					RESTART_TIMER_NOW(tmrFranklinSwitch)
				ELSE 
					IF TIMER_DO_ONCE_WHEN_READY(tmrFranklinSwitch, 1.0)
						MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_FRANKLIN) 
						iStageSwitchToBullDozer = 13
					ENDIF
				ENDIF
			ENDIF	
		BREAK
		
		CASE 13
			IF PREPARE_MUSIC_EVENT("RH2A_SWITCH_1")
			
				KILL_FACE_TO_FACE_CONVERSATION()
			
				TRIGGER_MUSIC_EVENT("RH2A_SWITCH_1")
				IF DOES_ENTITY_EXIST(pedSelector.pedID[SELECTOR_PED_MICHAEL]) AND NOT IS_ENTITY_DEAD(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedSelector.pedID[SELECTOR_PED_MICHAEL], 100.0)
				ENDIF
				IF DOES_ENTITY_EXIST(pedSelector.pedID[SELECTOR_PED_TREVOR]) AND NOT IS_ENTITY_DEAD(pedSelector.pedID[SELECTOR_PED_TREVOR])
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedSelector.pedID[SELECTOR_PED_TREVOR], 100.0)
				ENDIF
				
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, pedFranklin(), "FRANKLIN")
				
				IF NOT IS_ENTITY_DEAD(carBulldozer)
					START_PLAYBACK_RECORDED_VEHICLE(carBulldozer, 1, "Dozer")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(carBulldozer, 3000)
					SET_PLAYBACK_SPEED(carBullDozer, 0.01)
					SET_VEHICLE_ENGINE_ON(carBulldozer, TRUE, TRUE)
				ENDIF
				
				removeAllBlipsCops3()
				LOAD_STREAM("PS2A_DIGGER_GUNFIGHT_DISTANT_MASTER")
				START_AUDIO_SCENE("PS_2A_SWITCH_TO_FRANKLIN")
				camFranklinSwitchFirstFrame = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<43.9522, 6535.3115, 63.2852>>, <<-77.1084, 0.0014, -26.1505>>, 55.2664, TRUE)
				
				IF IS_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					CLEAR_PRINTS()
				ENDIF
				
				sCamDetails.pedTo 		= pedSelector.pedID[pedSelector.eNewSelectorPed]			
				sCamDetails.camTo 		= camFranklinSwitchFirstFrame
				iStageSwitchToBullDozer = 14
			ENDIF
		BREAK
		
		CASE 14
			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_CAM(sCamDetails, camFranklinSwitchFirstFrame, SWITCH_TYPE_MEDIUM, ENUM_TO_INT(SWITCH_FLAG_SKIP_OUTRO), DEFAULT, DEFAULT, FALSE, TRUE)
				IF sCamDetails.bOKToSwitchPed
					IF NOT sCamDetails.bPedSwitched
						IF TAKE_CONTROL_OF_SELECTOR_PED(pedSelector)          
							sCamDetails.bPedSwitched = TRUE
							iStageSwitchToBullDozer = 15
						ENDIF
					ENDIF 
				ENDIF
			ENDIF
		BREAK
		            
		CASE 15
			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_CAM(sCamDetails, camFranklinSwitchFirstFrame, SWITCH_TYPE_MEDIUM, ENUM_TO_INT(SWITCH_FLAG_SKIP_OUTRO), DEFAULT, DEFAULT, FALSE, TRUE)
				swState = GET_PLAYER_SWITCH_STATE()
				IF swState >= SWITCH_STATE_PAN
					
					IF swState >= SWITCH_STATE_JUMPCUT_DESCENT
						ALLOW_PLAYER_SWITCH_OUTRO()
		                iStageSwitchToBullDozer = 16
						FALLTHRU
					ENDIF
				ENDIF
			ELSE
				CPRINTLN(DEBUG_MISSION, "We're in CASE 15, and RUN_SWITCH_CAM thinks it's done!")
			ENDIF
		BREAK
		            
		CASE 16
			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_CAM(sCamDetails, camFranklinSwitchFirstFrame, SWITCH_TYPE_MEDIUM, ENUM_TO_INT(SWITCH_FLAG_SKIP_OUTRO), DEFAULT, DEFAULT, FALSE, TRUE)
			 	
				swState = GET_PLAYER_SWITCH_STATE()
				IF swState >= SWITCH_STATE_OUTRO_HOLD
					
					//iSynchSceneCamera = CREATE_SYNCHRONIZED_SCENE((<<50.473, 6543.685, 30.276>>), (<<0,0,5>>))
					//iSynchSceneCamera = CREATE_SYNCHRONIZED_SCENE((<< 34.454, 6524.233, 30.428 >>), (<< 0.000, 0.000, 104.040 >>))
					iSynchSceneCamera = CREATE_SYNCHRONIZED_SCENE((<< 37.753, 6557.316, 30.323 >>), (<< 0.000, 0.000, 9.360 >>))
							
					
				    camFranklinSwitchAnimated = CREATE_CAMERA(CAMTYPE_ANIMATED)
					
	                PLAY_SYNCHRONIZED_CAM_ANIM(camFranklinSwitchAnimated, iSynchSceneCamera, "switch_exit_cam", "missheistpaletoscore2switch_bulldozer")
	                SET_CAM_ACTIVE(camFranklinSwitchAnimated, TRUE)
	                RENDER_SCRIPT_CAMS(TRUE, FALSE)
	                DISPLAY_HUD(FALSE)
	                DISPLAY_RADAR(FALSE)
					
					PLAY_SOUND_FRONTEND(-1, "DiggerRevOneShot", "BulldozerDefault")
					
	    			iStageSwitchToBullDozer = 17
				ENDIF
			ENDIF
		BREAK
		            
		CASE 17
			IF NOT RUN_SWITCH_CAM_FROM_PLAYER_TO_CAM(sCamDetails, camFranklinSwitchFirstFrame, SWITCH_TYPE_MEDIUM, ENUM_TO_INT(SWITCH_FLAG_SKIP_OUTRO), DEFAULT, DEFAULT, FALSE, TRUE)
			 	//iSynchSceneCamera = CREATE_SYNCHRONIZED_SCENE((<<50.473, 6543.685, 30.276>>), (<<0,0,5>>))
				//iSynchSceneCamera = CREATE_SYNCHRONIZED_SCENE((<< 34.454, 6524.233, 30.428 >>), (<< 0.000, 0.000, 104.040 >>))
				IF NOT IS_ENTITY_DEAD(carBulldozer)
					SET_VEHICLE_ACTIVE_DURING_PLAYBACK(carBulldozer, TRUE)
					UNPAUSE_PLAYBACK_RECORDED_VEHICLE(carBulldozer)
					SET_PLAYBACK_SPEED(carBulldozer, 0.75)
				ENDIF
    			iStageSwitchToBullDozer = 18
			ENDIF
		BREAK

		CASE 18
			IF GET_SYNCHRONIZED_SCENE_PHASE(iSynchSceneCamera) >= 1.0
			 	SET_CAM_ACTIVE(camFranklinSwitchFirstFrame, FALSE)
		        SET_CAM_ACTIVE(camFranklinSwitchAnimated, FALSE)
				DESTROY_CAM(camFranklinSwitchFirstFrame)
		        DESTROY_CAM(camFranklinSwitchAnimated)
		        DELETE_ARRAY_OF_PEDS(pedsArmyGuysToHoldOff, TRUE)
		        DELETE_ARRAY_OF_BLIPS(blipsArmyGuysToHoldOff)
		        DELETE_ARRAY_OF_PEDS(chinookPilots, FALSE)
		        DELETE_ARRAY_OF_VEHICLES(chinooks)
		                      
		        SET_MODEL_AS_NO_LONGER_NEEDED(CARGOBOB)
		              
		        SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sCamDetails.pedTo)
		        RENDER_SCRIPT_CAMS(FALSE, FALSE)
		        DISPLAY_HUD(TRUE)
		        DISPLAY_RADAR(TRUE)
		        STOP_AUDIO_SCENE("PS_2A_SWITCH_TO_FRANKLIN")

				REMOVE_VEHICLE_RECORDING(1, "Dozer")
			 
			 	REMOVE_ANIM_DICT("missheistpaletoscore2switch_bulldozer")

		        mission_stage = STAGE_SWAPPED_TO_BULLDOZER
			ENDIF
		BREAK

	ENDSWITCH


ENDPROC


INT iTimeOfLastMoneyLoss
INT iMoneyLossInterval = 10000
OBJECT_INDEX oiFenceL
OBJECT_INDEX oiFenceR
INT iFenceCrashAudio

PROC PROCESS_FENCE_AUDIO()

	SWITCH iFenceCrashAudio
	
		CASE 0
			IF REQUEST_AMBIENT_AUDIO_BANK("PS2A_Drive_Bulldozer")
				iFenceCrashAudio++
			ENDIF
		BREAK
		
		CASE 1
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS ( <<-199.72, 6314.28, 31.84>>, 2.0, PROP_FACGATE_03_L)
			AND DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS ( <<-205.84, 6307.16, 31.84>>, 2.0, PROP_FACGATE_03_R)
				oiFenceL = GET_CLOSEST_OBJECT_OF_TYPE(<<-199.72, 6314.28, 31.84>>, 2.0, PROP_FACGATE_03_L)
				oiFenceR = GET_CLOSEST_OBJECT_OF_TYPE(<<-205.84, 6307.16, 31.84>>, 2.0, PROP_FACGATE_03_R)
				iFenceCrashAudio++
			ENDIF
		BREAK
		
		CASE 2
			IF DOES_ENTITY_EXIST(carBulldozer)
				IF NOT IS_ENTITY_DEAD(carBulldozer)
					IF IS_ENTITY_TOUCHING_ENTITY(carBulldozer, oiFenceL)
					OR IS_ENTITY_TOUCHING_ENTITY(carBulldozer, oiFenceR)
						PLAY_SOUND_FROM_ENTITY(-1, "PS2A_TRACTOR_THRU_CHAIN_LINK_MASTER", carBulldozer)
						iFenceCrashAudio++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("carBUlldzoer")
			iFenceCrashAudio++
		BREAK
	
	ENDSWITCH
		
ENDPROC

PROC SET_BULLDOZER_STAGE_ENEMY_CAR_PROPERTIES(VEHICLE_INDEX &vehCar)
	IF DOES_ENTITY_EXIST(vehCar) AND NOT IS_ENTITY_DEAD(vehCar)
		//SET_CGOFFSET(vehCar, vCenterOfGravityOffset)
//		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehCar, FALSE)
	ENDIF
		
ENDPROC

PROC TASK_BULLDOZER_COPS_EXIT_CARS()
	FLOAT flRecordingProgress = 95
	INT	iAccuracy = 1
	// Wave 1
	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(dozerStageWave1Cop1.thisCar, "RBHdzCop") > flRecordingProgress
		REMOVE_VEHICLE_RECORDING(997, "RBHdzCop")
		SET_ACCURACY_FOR_SET_PIECE_CAR_GUYS(dozerStageWave1Cop1, iAccuracy)
		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(dozerStageWave1Cop1)
	ENDIF

//	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(dozerStageWave1Cop2.thisCar, "RBHdzCop") > flRecordingProgress
//		REMOVE_VEHICLE_RECORDING(998, "RBHdzCop")
//		SET_ACCURACY_FOR_SET_PIECE_CAR_GUYS(dozerStageWave1Cop2, iAccuracy)
//		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(dozerStageWave1Cop2)
//	ENDIF
	
	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(dozerStageWave1Cop3.thisCar, "RBHdzCop") > flRecordingProgress
		REMOVE_VEHICLE_RECORDING(999, "RBHdzCop")
		SET_ACCURACY_FOR_SET_PIECE_CAR_GUYS(dozerStageWave1Cop3, iAccuracy)
		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(dozerStageWave1Cop3)
	ENDIF
	
	// Wave 2
//	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(dozerStageWave2Cop1.thisCar, "RBHdzCop") > flRecordingProgress
//		REMOVE_VEHICLE_RECORDING(1000, "RBHdzCop")
//		SET_ACCURACY_FOR_SET_PIECE_CAR_GUYS(dozerStageWave2Cop1, iAccuracy)
//		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(dozerStageWave2Cop1)
//	ENDIF
//	
//	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(dozerStageWave2Cop2.thisCar, "RBHdzCop") > flRecordingProgress
//		REMOVE_VEHICLE_RECORDING(1001, "RBHdzCop")
//		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(dozerStageWave2Cop2)
//	ENDIF
	
	// Wave 3
	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(dozerStageWave3Cop1.thisCar, "RBHdzCop") > flRecordingProgress
		REMOVE_VEHICLE_RECORDING(1002, "RBHdzCop")
		SET_ACCURACY_FOR_SET_PIECE_CAR_GUYS(dozerStageWave3Cop1, iAccuracy)
		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(dozerStageWave3Cop1)
	ENDIF

	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(dozerStageWave3Cop2.thisCar, "RBHdzCop") > flRecordingProgress
		REMOVE_VEHICLE_RECORDING(1003, "RBHdzCop")
		SET_ACCURACY_FOR_SET_PIECE_CAR_GUYS(dozerStageWave3Cop2, iAccuracy)
		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(dozerStageWave3Cop2)
	ENDIF
	
//	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(dozerStageWave3Cop3.thisCar, "RBHdzCop") > flRecordingProgress
//		REMOVE_VEHICLE_RECORDING(1004, "RBHdzCop")
//		SET_ACCURACY_FOR_SET_PIECE_CAR_GUYS(dozerStageWave3Cop3, iAccuracy)
//		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(dozerStageWave3Cop3)
//	ENDIF
	
	// Mid cops
	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(dozerStageMidCop1.thisCar, "RBHdzCop") > 69.0 //flRecordingProgress
		REMOVE_VEHICLE_RECORDING(1005, "RBHdzCop")
		SET_ACCURACY_FOR_SET_PIECE_CAR_GUYS(dozerStageMidCop1, iAccuracy)
		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(dozerStageMidCop1)
	ENDIF
	
	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(dozerStageMidCop2.thisCar, "RBHdzCop") > flRecordingProgress
		REMOVE_VEHICLE_RECORDING(1006, "RBHdzCop")
		SET_ACCURACY_FOR_SET_PIECE_CAR_GUYS(dozerStageMidCop2, iAccuracy)
		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(dozerStageMidCop2)
	ENDIF
	
	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(dozerStageMidCop3.thisCar, "RBHdzCop") > 93.0//flRecordingProgress
		REMOVE_VEHICLE_RECORDING(007, "RBHdzCop")
		SET_ACCURACY_FOR_SET_PIECE_CAR_GUYS(dozerStageMidCop3, iAccuracy)
		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(dozerStageMidCop3)
	ENDIF
	
	IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(dozerStageMidCop4.thisCar, "RBHdzCop") > flRecordingProgress
		REMOVE_VEHICLE_RECORDING(1008, "RBHdzCop")
		SET_ACCURACY_FOR_SET_PIECE_CAR_GUYS(dozerStageMidCop4, iAccuracy)
		STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(dozerStageMidCop4)
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_TOUCHING_CRUSADER()

	IF NOT IS_ENTITY_DEAD(carBulldozer)

		IF (NOT IS_ENTITY_DEAD(dozerStageWave1Cop1.thisCar) AND IS_ENTITY_TOUCHING_ENTITY(carBulldozer, dozerStageWave1Cop1.thisCar))
		OR (NOT IS_ENTITY_DEAD(dozerStageWave1Cop2.thisCar) AND IS_ENTITY_TOUCHING_ENTITY(carBulldozer, dozerStageWave1Cop2.thisCar))
		OR (NOT IS_ENTITY_DEAD(dozerStageWave1Cop3.thisCar) AND IS_ENTITY_TOUCHING_ENTITY(carBulldozer, dozerStageWave1Cop3.thisCar))
		OR (NOT IS_ENTITY_DEAD(dozerStageMidCop1.thisCar) AND IS_ENTITY_TOUCHING_ENTITY(carBulldozer, dozerStageMidCop1.thisCar))
		OR (NOT IS_ENTITY_DEAD(dozerStageMidCop2.thisCar) AND IS_ENTITY_TOUCHING_ENTITY(carBulldozer, dozerStageMidCop2.thisCar))
		OR (NOT IS_ENTITY_DEAD(dozerStageMidCop3.thisCar) AND IS_ENTITY_TOUCHING_ENTITY(carBulldozer, dozerStageMidCop3.thisCar))
		OR (NOT IS_ENTITY_DEAD(dozerStageMidCop4.thisCar) AND IS_ENTITY_TOUCHING_ENTITY(carBulldozer, dozerStageMidCop4.thisCar))
		OR (NOT IS_ENTITY_DEAD(dozerStageWave2Cop1.thisCar) AND IS_ENTITY_TOUCHING_ENTITY(carBulldozer, dozerStageWave2Cop1.thisCar))
		OR (NOT IS_ENTITY_DEAD(dozerStageWave2Cop2.thisCar) AND IS_ENTITY_TOUCHING_ENTITY(carBulldozer, dozerStageWave2Cop2.thisCar))
		OR (NOT IS_ENTITY_DEAD(dozerStageWave3Cop1.thisCar) AND IS_ENTITY_TOUCHING_ENTITY(carBulldozer, dozerStageWave3Cop1.thisCar))
		OR (NOT IS_ENTITY_DEAD(dozerStageWave3Cop2.thisCar) AND IS_ENTITY_TOUCHING_ENTITY(carBulldozer, dozerStageWave3Cop2.thisCar))
		OR (NOT IS_ENTITY_DEAD(dozerStageWave3Cop3.thisCar) AND IS_ENTITY_TOUCHING_ENTITY(carBulldozer, dozerStageWave3Cop3.thisCar))
			RETURN TRUE
		ENDIF

	ENDIF
	
	RETURN FALSE
	
ENDFUNC

INT iTimeOfEnteringSlowDownLocate
FLOAT fTankPlaybackSpeed = 0.4

PROC MANAGE_CAR_TRIGGERS_DURING_BULLDOZER_STAGE()
	
	IF iSetPieceCopStage >= 2
		TASK_BULLDOZER_COPS_EXIT_CARS()
	ENDIF
	
	SWITCH iSetpieceCopStage
	
		CASE 0
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "RBHdzTank")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(997, "RBHdzCop")
			//AND HAS_VEHICLE_RECORDING_BEEN_LOADED(998, "RBHdzCop")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(999, "RBHdzCop")
//			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1000, "RBHdzCop")
//			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1001, "RBHdzCop")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1002, "RBHdzCop")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1003, "RBHdzCop")
			//AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1004, "RBHdzCop")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1005, "RBHdzCop")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1006, "RBHdzCop")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(007, "RBHdzCop")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1008, "RBHdzCop")
			AND HAS_MODEL_LOADED(CRUSADER)
			AND HAS_MODEL_LOADED(S_M_Y_Marine_03)
				fTankPlaybackSpeed = 0.0
				iSetpieceCopStage++	
			ENDIF
		BREAK
		
		CASE 1
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << 30.3746, 6540.7930, 30.5225 >> , <<9.0, 9.0, 5.0>>)
								
				INIT_SET_PIECE_COP_CAR(dozerStageWave1Cop1, CRUSADER, 997, "RBHdzCop", VS_FRONT_RIGHT, 5000.0, 1.0, FALSE)
				//INIT_SET_PIECE_COP_CAR(dozerStageWave1Cop2, CRUSADER, 998, "RBHdzCop", VS_FRONT_RIGHT, 5000.0, 1.0, FALSE)
				INIT_SET_PIECE_COP_CAR(dozerStageWave1Cop3, CRUSADER, 999, "RBHdzCop", VS_FRONT_RIGHT, 3000.0, 1.0, FALSE)
				SET_BULLDOZER_STAGE_ENEMY_CAR_PROPERTIES(dozerStageWave1Cop1.thisCar)
				SET_BULLDOZER_STAGE_ENEMY_CAR_PROPERTIES(dozerStageWave1Cop2.thisCar)
				SET_BULLDOZER_STAGE_ENEMY_CAR_PROPERTIES(dozerStageWave1Cop3.thisCar)
				
				SET_PED_COMBAT_ATTRIBUTES(dozerStageWave1Cop1.thisDriver, CA_USE_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(dozerStageWave1Cop1.thisDriver, CA_USE_VEHICLE_ATTACK, FALSE)
				SET_PED_CONFIG_FLAG(dozerStageWave1Cop1.thisDriver, PCF_DontBlipCop, TRUE)
						
				SET_PED_COMBAT_ATTRIBUTES(dozerStageWave1Cop3.thisDriver, CA_USE_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(dozerStageWave1Cop3.thisDriver, CA_USE_VEHICLE_ATTACK, FALSE)
				SET_PED_CONFIG_FLAG(dozerStageWave1Cop3.thisDriver, PCF_DontBlipCop, TRUE)
				SET_PED_CONFIG_FLAG(dozerStageWave1Cop3.thisPassenger, PCF_DontBlipCop, TRUE)
				
				
				DELETE_PED(dozerStageWave1Cop1.thisPassenger)	//BUg 1398701
								
				CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_TALKSL", CONV_PRIORITY_VERY_HIGH )
				iSetpieceCopStage++
			ENDIF
		BREAK
		
		CASE 2
			IF IS_PLAYER_IN_TRIGGER_BOX(tbBulldozerTriggerCarMid1)
				INIT_SET_PIECE_COP_CAR(dozerStageMidCop1, CRUSADER, 1005, "RBHdzCop", VS_FRONT_RIGHT, 1000.0, 1.0, FALSE)
				INIT_SET_PIECE_COP_CAR(dozerStageMidCop2, CRUSADER, 1006, "RBHdzCop", VS_FRONT_RIGHT, 1000.0, 1.0, FALSE)
				
				SET_PED_COMBAT_ATTRIBUTES(dozerStageMidCop1.thisDriver, CA_USE_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(dozerStageMidCop1.thisDriver, CA_USE_VEHICLE_ATTACK, FALSE)
				SET_PED_CONFIG_FLAG(dozerStageMidCop1.thisDriver, PCF_DontBlipCop, TRUE)
								
				SET_PED_COMBAT_ATTRIBUTES(dozerStageMidCop1.thisPassenger, CA_USE_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(dozerStageMidCop1.thisPassenger, CA_USE_VEHICLE_ATTACK, FALSE)
				SET_PED_CONFIG_FLAG(dozerStageMidCop1.thisPassenger, PCF_DontBlipCop, TRUE)
		
				SET_PED_COMBAT_ATTRIBUTES(dozerStageMidCop2.thisDriver, CA_USE_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(dozerStageMidCop2.thisDriver, CA_USE_VEHICLE_ATTACK, FALSE)
				SET_PED_CONFIG_FLAG(dozerStageMidCop2.thisDriver, PCF_DontBlipCop, TRUE)
		
		
				//DELETE_PED(dozerStageMidCop1.thisPassenger)
				DELETE_PED(dozerStageMidCop2.thisPassenger)
				
				SET_BULLDOZER_STAGE_ENEMY_CAR_PROPERTIES(dozerStageMidCop1.thisCar)
				SET_BULLDOZER_STAGE_ENEMY_CAR_PROPERTIES(dozerStageMidCop2.thisCar)
				iSetpieceCopStage++
			ENDIF
		BREAK
		
		CASE 3
			IF IS_PLAYER_IN_TRIGGER_BOX(tbBulldozerTriggerCarMid2)
				INIT_SET_PIECE_COP_CAR(dozerStageMidCop3, CRUSADER, 007, "RBHdzCop", VS_FRONT_RIGHT, 1000.0, 1.0, FALSE)
//				INIT_SET_PIECE_COP_CAR(dozerStageMidCop4, CRUSADER, 1008, "RBHdzCop", VS_FRONT_RIGHT, 1000.0, 1.0, FALSE)
				SET_BULLDOZER_STAGE_ENEMY_CAR_PROPERTIES(dozerStageMidCop3.thisCar)
//				SET_BULLDOZER_STAGE_ENEMY_CAR_PROPERTIES(dozerStageMidCop4.thisCar)

				SET_PED_CONFIG_FLAG(dozerStageMidCop3.thisDriver, PCF_DontBlipCop, TRUE)
				SET_PED_CONFIG_FLAG(dozerStageMidCop3.thisPassenger, PCF_DontBlipCop, TRUE)

				iSetpieceCopStage++
			ENDIF
		BREAK
		
		CASE 4
			IF IS_PLAYER_IN_TRIGGER_BOX(tbBulldozerTriggerCarWave2)
				INIT_SET_PIECE_COP_CAR(dozerStageWave2Cop1, CRUSADER, 1000, "RBHdzCop", VS_FRONT_RIGHT, 2000.0, 1.0, FALSE)
				INIT_SET_PIECE_COP_CAR(dozerStageWave2Cop2, CRUSADER, 1001, "RBHdzCop", VS_FRONT_RIGHT, 2000.0, 1.0, FALSE)
				SET_BULLDOZER_STAGE_ENEMY_CAR_PROPERTIES(dozerStageWave2Cop1.thisCar)
				SET_BULLDOZER_STAGE_ENEMY_CAR_PROPERTIES(dozerStageWave2Cop2.thisCar)
				
				SET_PED_COMBAT_ATTRIBUTES(dozerStageWave2Cop1.thisDriver, CA_USE_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(dozerStageWave2Cop1.thisDriver, CA_USE_VEHICLE_ATTACK, FALSE)
				SET_PED_CONFIG_FLAG(dozerStageWave2Cop1.thisDriver, PCF_DontBlipCop, TRUE)
				
				SET_PED_COMBAT_ATTRIBUTES(dozerStageWave2Cop2.thisDriver, CA_USE_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(dozerStageWave2Cop2.thisDriver, CA_USE_VEHICLE_ATTACK, FALSE)
				SET_PED_CONFIG_FLAG(dozerStageWave2Cop2.thisDriver, PCF_DontBlipCop, TRUE)
				
				DELETE_PED(dozerStageWave2Cop1.thisPassenger)
				DELETE_PED(dozerStageWave2Cop2.thisPassenger)
				
				REQUEST_MODEL(RHINO)
				
				iSetpieceCopStage++
			ENDIF
		BREAK
		
		CASE 5
			IF IS_PLAYER_IN_TRIGGER_BOX(tbBulldozerTriggerCarWave3)
				INIT_SET_PIECE_COP_CAR(dozerStageWave3Cop1, CRUSADER, 1002, "RBHdzCop", VS_FRONT_RIGHT, 2500.0, 1.09, FALSE)
				INIT_SET_PIECE_COP_CAR(dozerStageWave3Cop2, CRUSADER, 1003, "RBHdzCop", VS_FRONT_RIGHT, 500.0, 1.09, FALSE)
				//INIT_SET_PIECE_COP_CAR(dozerStageWave3Cop3, CRUSADER, 1004, "RBHdzCop", VS_FRONT_RIGHT, 1000.0, 1.0, FALSE)
				SET_BULLDOZER_STAGE_ENEMY_CAR_PROPERTIES(dozerStageWave3Cop1.thisCar)
				SET_BULLDOZER_STAGE_ENEMY_CAR_PROPERTIES(dozerStageWave3Cop2.thisCar)
				//SET_BULLDOZER_STAGE_ENEMY_CAR_PROPERTIES(dozerStageWave3Cop3.thisCar)
				
				SET_PED_COMBAT_ATTRIBUTES(dozerStageWave3Cop1.thisDriver, CA_USE_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(dozerStageWave3Cop1.thisDriver, CA_USE_VEHICLE_ATTACK, FALSE)
				SET_PED_CONFIG_FLAG(dozerStageWave3Cop1.thisDriver, PCF_DontBlipCop, TRUE)
				
				SET_PED_COMBAT_ATTRIBUTES(dozerStageWave3Cop2.thisDriver, CA_USE_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(dozerStageWave3Cop2.thisDriver, CA_USE_VEHICLE_ATTACK, FALSE)
				SET_PED_CONFIG_FLAG(dozerStageWave3Cop2.thisDriver, PCF_DontBlipCop, TRUE)
				
				DELETE_PED(dozerStageWave3Cop2.thisPassenger)
								
				iSetpieceCopStage++
			ENDIF
		BREAK
		
		CASE 6
			//IF IS_PLAYER_IN_TRIGGER_BOX(tbBulldozerTriggerTank)
			IF HAS_MODEL_LOADED(RHINO)
				INIT_SET_PIECE_COP_CAR(dozerStageTank, RHINO, 1, "RBHdzTank", VS_FRONT_RIGHT, 3000.0, 0.47)
				SET_VEHICLE_DOORS_LOCKED(dozerStageTank.thisCar, VEHICLELOCK_LOCKED)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(dozerStageTank.thisCar, FALSE)
				IF DOES_ENTITY_EXIST(dozerStageTank.thisDriver) AND NOT IS_ENTITY_DEAD(dozerStageTank.thisDriver)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(dozerStageTank.thisDriver, TRUE)
				ENDIF
				IF DOES_ENTITY_EXIST(dozerStageTank.thisCar) AND NOT IS_ENTITY_DEAD(dozerStageTank.thisCar)
					SET_ENTITY_INVINCIBLE(dozerStageTank.thisCar, TRUE)
				ENDIF
				
				SET_PED_CONFIG_FLAG(dozerStageTank.thisDriver, PCF_DontBlipCop, TRUE)
				
				iSetpieceCopStage++
			ENDIF
		BREAK
		
		CASE 7
			// Do nothing			
			IF NOT IS_PED_INJURED(dozerStageTank.thisDriver)
				IF IS_ENTITY_IN_ANGLED_AREA(dozerStageTank.thisCar, <<-167.056137,6361.035645,29.690443>>, <<-214.920563,6313.808594,35.621735>>, 38.500000)
					TASK_VEHICLE_AIM_AT_PED(dozerStageTank.thisDriver, PLAYER_PED_ID())
					iTimeOfEnteringSlowDownLocate = GET_GAME_TIMER()
					iSetpieceCopStage++
				ENDIF		
			ENDIF		
		BREAK
		
		CASE 8
			IF GET_GAME_TIMER() - iTimeOfEnteringSlowDownLocate > 12000
			OR IS_PED_ON_FOOT(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(dozerStageTank.thisDriver)
					TASK_VEHICLE_SHOOT_AT_COORD(dozerStageTank.thisDriver, <<-201.7992, 6352.1924, 35.1415>>, 0)
					SET_PED_FIRING_PATTERN(dozerStageTank.thisDriver, FIRING_PATTERN_SINGLE_SHOT)
					iSetpieceCopStage++
				ENDIF
			ENDIF
		BREAK
		
		CASE 9
			IF GET_GAME_TIMER() - iTimeOfEnteringSlowDownLocate > 14000
			OR IS_PED_ON_FOOT(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(dozerStageTank.thisDriver)
					TASK_VEHICLE_AIM_AT_PED(dozerStageTank.thisDriver, PLAYER_PED_ID())
					iSetpieceCopStage++
				ENDIF
			ENDIF
		BREAK
						
		CASE 10
			IF GET_GAME_TIMER() - iTimeOfEnteringSlowDownLocate > 23000
			OR IS_PED_ON_FOOT(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(dozerStageTank.thisDriver)
					TASK_VEHICLE_SHOOT_AT_PED(dozerStageTank.thisDriver, PLAYER_PED_ID())
					iSetpieceCopStage++
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	//Control Tank speed.
	IF NOT IS_ENTITY_DEAD(dozerStageTank.thisCar)
		IF IS_ENTITY_IN_ANGLED_AREA(dozerStageTank.thisCar, <<-167.056137,6361.035645,29.690443>>, <<-214.920563,6313.808594,35.621735>>, 38.500000)
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-178.397278,6352.695313,29.719213>>, <<-214.920563,6313.808594,35.621735>>, 38.500000)
				IF NOT IS_ENTITY_DEAD(dozerStageTank.thisCar)	
					IF fTankPlaybackSpeed > 0.0
						fTankPlaybackSpeed = fTankPlaybackSpeed -@ 0.1
					ELSE
						fTankPlaybackSpeed = 0.0
					ENDIF
				ENDIF
			ELSE
				IF fTankPlaybackSpeed < 0.4
					fTankPlaybackSpeed = fTankPlaybackSpeed +@ 0.2
				ELSE
					fTankPlaybackSpeed = 0.4
				ENDIF
			ENDIF		
			SET_PLAYBACK_SPEED(dozerStageTank.thisCar, fTankPlaybackSpeed)
		ENDIF
	ENDIF
	
	
ENDPROC


INT 	iBulldozerAudioState

PROC UPDATE_BULLDOZER_AUDIO_BANK()
	CPRINTLN(DEBUG_MISSION, "iBulldozerAudioState = ", iBulldozerAudioState)
	SWITCH iBulldozerAudioState
		CASE 0
			IF LOAD_STREAM("PS2A_DIGGER_GUNFIGHT_DISTANT_MASTER")
				iBulldozerAudioState++
			ENDIF
		BREAK
		
		CASE 1
			PLAY_STREAM_FROM_POSITION( <<-189.6755, 6286.7969, 30.4893>>)
			iBulldozerAudioState++
		BREAK
		
		CASE 2
			IF IS_ENTITY_OK(PLAYER_PED_ID())
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<-183.962082,6350.545898,29.535540>>, <<-226.008881,6308.446777,34.506172>>, 50.000000)
					STOP_STREAM()
					iBulldozerAudioState++
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

INT iPreStreamCutscene

INT iStatusDialogueTimer

BOOL bAlternateRamLines =  TRUE
BOOL bAlternateNotTouchingLInes

PROC stageSwappedToBulldozer()

//	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS ( <<23.6, 6546.8, 32.0>>, 2.0, PROP_FNCLINK_03GATE1)
//		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_FNCLINK_03GATE1, <<23.6, 6546.8, 32.0>>, TRUE, -1.0)
//	ENDIF
//	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS ( <<15.4, 6538.5, 32.0>>, 2.0, PROP_FNCLINK_03GATE4)
//		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_FNCLINK_03GATE4, <<15.4, 6538.5, 32.0>>, TRUE, -1.0)
//	ENDIF
		
	SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
	
	REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
	
	doBuddyFailCheckStageDozerTankStage() //Doesn't check for buddy2 as we kill him off...
	
	//Lost money if you take forever
	IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedSelector.pedID[SELECTOR_PED_MICHAEL]) > 60.0	
		IF GET_GAME_TIMER() - iTimeOfLastMoneyLoss > iMoneyLossInterval
			iCurrentTake = iCurrentTake - GET_RANDOM_INT_IN_RANGE(500, 5000)
			iTimeOfLastMoneyLoss  = GET_GAME_TIMER()
			
			IF iMoneyLossInterval > 1000
				iMoneyLossInterval = iMoneyLossInterval - 250
			ENDIF
			
		ENDIF
	ENDIF
	
//	CREATE_ENTITIES_FOR_DOZER_STAGE_OVER_MULTIPLE_FRAMES()
	
	
	SWITCH iPreStreamCutscene
	
		CASE 0
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -200.9094, 6283.7466, 31.4733 >>, <<DEFAULT_CUTSCENE_LOAD_DIST, DEFAULT_CUTSCENE_LOAD_DIST, DEFAULT_CUTSCENE_LOAD_DIST>>, FALSE)
				REQUEST_CUTSCENE("RBH_2A_MCS_4")
				iPreStreamCutscene++
			ENDIF	
		BREAK
			
		CASE 1
		
			IF NOT HAS_CUTSCENE_LOADED()
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", pedSelector.pedID[SELECTOR_PED_MICHAEL])
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", pedTrevor())
			ELSE
				iPreStreamCutscene++
			ENDIF
		BREAK
		
	ENDSWITCH
	
	PROCESS_FENCE_AUDIO()
	
	IF iBullDozerStage > 0
		MANAGE_CAR_TRIGGERS_DURING_BULLDOZER_STAGE()
		UPDATE_BULLDOZER_AUDIO_BANK()
	ENDIF
	
	SWITCH iBullDozerStage

		CASE 0
			iPreStreamCutscene = 0
			IF IS_AUDIO_SCENE_ACTIVE("PS_2A_SHOOTOUT_YARD")
				STOP_AUDIO_SCENE("PS_2A_SHOOTOUT_YARD")
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("PS_2A_SHOOTOUT_YARD_FLYING_TANK")
				STOP_AUDIO_SCENE("PS_2A_SHOOTOUT_YARD_FLYING_TANK")
			ENDIF
			RESET_TYRE_POP_ARRAY()
			
			IF NOT IS_ENTITY_DEAD(chinooksTank)
				DETACH_ROPE_FROM_ENTITY(ropesTank[0], chinooksTank)
				DETACH_ROPE_FROM_ENTITY(ropesTank[1], chinooksTank)
				DETACH_ROPE_FROM_ENTITY(ropesTank[2], chinooksTank)
				DETACH_ROPE_FROM_ENTITY(ropesTank[3], chinooksTank)
			ENDIF
			
			DELETE_ROPE(ropesTank[0])
			DELETE_ROPE(ropesTank[1])
			DELETE_ROPE(ropesTank[2])
			DELETE_ROPE(ropesTank[3])
	
			DELETE_VEHICLE(chinooks[0])
			SET_MODEL_AS_NO_LONGER_NEEDED(CARGOBOB)
			SET_MODEL_AS_NO_LONGER_NEEDED(RHINO)
			DELETE_PED(chinookPilots[0])
			DELETE_VEHICLE(chinooksTank)
			DELETE_PED(chinooksTankDriver)
		
		
			iFenceCrashAudio = 0
			DELETE_VEHICLE(CopCarsParked3[0])
			DELETE_VEHICLE(CopCarsParked3[1])
			
			SET_MAX_WANTED_LEVEL(0)
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			SETUP_WANTED_LEVEL(0, TRUE, 100.0, 10.0, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
								
			//pedTrevor() = pedSelector.pedID[pedSelector.ePreviousSelectorPed]
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(10, "STAGE_SWAPPED_TO_BULLDOZER") 
			g_replay.iReplayInt[REPLAY_VALUE_CURRENT_TAKE] = iCurrentTake
			
			g_iPaletoScoreTake = iCurrentTake	//UPdate the saved take
				
										
			IF NOT IS_ENTITY_DEAD(PedMichael())
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(PedMichael(), TRUE)
				SET_ENTITY_COORDS(PedMichael(), << -200.9094, 6283.7466, 31.4733 >> )
				SET_ENTITY_HEADING(PedMichael(), 335.6462 )
				SET_PED_ACCURACY(PedMichael(), 7)
				SET_PED_SPHERE_DEFENSIVE_AREA(PedMichael(), << -200.9094, 6283.7466, 31.4733 >>, 2.5 )
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(PedMichael(), 100.0)
			ENDIF
			
			DELETE_ARRAY_OF_BLIPS(blippedsArmyInJunkYard)
			DELETE_ARRAY_OF_BLIPS(blipsArmyGuysToHoldOff)
			DELETE_ARRAY_OF_PEDS(pedsArmyInJunkYard)
			DELETE_ARRAY_OF_VEHICLES(chinooks)
			DELETE_ARRAY_OF_PEDS(chinookPilots)
			
			DELETE_VEHICLE(chinooksTank)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(CARGOBOB)
			SET_MODEL_AS_NO_LONGER_NEEDED(RHINO)
			
			
			REMOVE_VEHICLE_RECORDING(5, "RBHChin")
			REMOVE_VEHICLE_RECORDING(2, "RBHFlyover")
			REMOVE_VEHICLE_RECORDING(1, "CBRam")
			
			CLEAR_AREA(<< -188.1118, 6285.0000, 31.7185 >>, 50.0, TRUE)
			
			REMOVE_PTFX_ASSET()
			
			SET_PLAYER_CAN_USE_COVER(PLAYER_ID(), TRUE)
			
			SET_PED_MAX_HEALTH(PLAYER_PED_ID(), 850)
			SET_ENTITY_HEALTH(PLAYER_PED_ID(), 850)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_MICROSMG, 400)
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_MICROSMG, TRUE)
			
			IF NOT IS_ENTITY_DEAD(pedTrevor())
				
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedTrevor(), TRUE)
				SET_ENTITY_COORDS(pedTrevor(), <<-197.7659, 6282.7568, 30.4903 >>)
				SET_ENTITY_HEADING(pedTrevor(), 266.8397)
				SET_PED_SPHERE_DEFENSIVE_AREA(pedTrevor(), <<-197.7659, 6282.7568, 31.4733 >>, 2.5 )
				SET_PED_ACCURACY(pedTrevor(), 7)
				
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedTrevor(), TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTrevor(), TRUE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedTrevor(), FALSE)
				GIVE_WEAPON_TO_PED(pedTrevor(), pedGunmanWeaponType, 400, TRUE)	
				
				SET_PED_MOVEMENT_CLIPSET(pedTrevor(), "ANIM_GROUP_MOVE_BALLISTIC")
				
				SET_PED_RELATIONSHIP_GROUP_HASH(pedTrevor(), robbersGroup)
				SET_PED_SUFFERS_CRITICAL_HITS(pedTrevor(), FALSE)
				SET_PED_CAN_BE_TARGETTED(pedTrevor(), FALSE)
				SET_PED_MAX_HEALTH(pedTrevor(), 1000)
				SET_ENTITY_HEALTH(pedTrevor(), 1000)
				
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedTrevor(), 100.0)
				
			ENDIF
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), robbersGroup)
										
			CREATE_ARMY_PEDS_FOR_PICKUP_CUTSCENE()
								
			DELETE_VEHICLE(ArmyTruckStreet1.thisCar)
			DELETE_VEHICLE(ArmyTruckStreet2.thisCar)
			CLEANUP_SET_PIECE_COP_CAR(ArmyTruckStreet1)
			CLEANUP_SET_PIECE_COP_CAR(ArmyTruckStreet2)
			 //Delete a truck to allow room for bulldozer

			//KILL_OFF_ARMY_STAGE3()
			IF NOT IS_ENTITY_DEAD(carBulldozer)
				ADD_VEHICLE_UPSIDEDOWN_CHECK(carBulldozer)
				UNPAUSE_PLAYBACK_RECORDED_VEHICLE(carBulldozer)
				SET_PLAYBACK_SPEED(carBulldozer, 0.75)
				
				SET_ENTITY_PROOFS(carBulldozer, TRUE, FALSE, FALSE, FALSE, FALSE, FALSE)
				
			ENDIF
			
			//REQUEST_VEHICLE_RECORDING(1, "ChargingTrucks")
			REQUEST_VEHICLE_RECORDING(2, "ChargingTrucks")
			
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(pedFranklin()) 
			
			SET_MODEL_AS_NO_LONGER_NEEDED(BARRACKS)
			
			START_AUDIO_SCENE("PS_2A_DRIVE_BULLDOZER")
			
			//moved to here to avoid assert after a load scene.
			REQUEST_VEHICLE_RECORDING(1, "RBHdzTank")
			REQUEST_VEHICLE_RECORDING(997, "RBHdzCop")
			//REQUEST_VEHICLE_RECORDING(998, "RBHdzCop")
			REQUEST_VEHICLE_RECORDING(999, "RBHdzCop")
			REQUEST_VEHICLE_RECORDING(1000, "RBHdzCop")
			REQUEST_VEHICLE_RECORDING(1001, "RBHdzCop")
			REQUEST_VEHICLE_RECORDING(1002, "RBHdzCop")
			REQUEST_VEHICLE_RECORDING(1003, "RBHdzCop")
			//REQUEST_VEHICLE_RECORDING(1004, "RBHdzCop")
			REQUEST_VEHICLE_RECORDING(1005, "RBHdzCop")
			REQUEST_VEHICLE_RECORDING(1006, "RBHdzCop")
			REQUEST_VEHICLE_RECORDING(007, "RBHdzCop")
			REQUEST_VEHICLE_RECORDING(1008, "RBHdzCop")

			REQUEST_SCRIPT_AUDIO_BANK("PS2A_Jeep_Skids")

			REQUEST_MODEL(CRUSADER)
			REQUEST_MODEL(S_M_Y_Marine_03)
			//REQUEST_MODEL(RHINO)
			
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
			
			FADE_IN_IF_NEEDED()
			iBulldozerAudioState = 0
			PRINT_HELP("RBH_SCOOP")
//			GENERATE_FLEE_DISTANCES()
			iBullDozerStage++
			SETTIMERA(0)	
			//REQUEST_CUTSCENE("RBH_2A_MCS_4")
		BREAK
		
		CASE 1
		
			//
		
			IF TIMERA() > 4500
			OR IS_PLAYER_PUSHING_ANALOGUE_STICKS()
				IF NOT IS_ENTITY_DEAD(carBulldozer)
					STOP_PLAYBACK_RECORDED_VEHICLE(carBulldozer)					
				ENDIF
				REMOVE_VEHICLE_RECORDING(1, "dozer")
				
				TRIGGER_MUSIC_EVENT("RH2A_SWITCH_2")
				
				CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_RADIOF", CONV_PRIORITY_VERY_HIGH )
				iBullDozerStage++
			ENDIF
		BREAK
		
		CASE 2
		
			//IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
//				PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_PS_2A_03", 0.0)
				iBullDozerStage++
			ENDIF
						
			IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, << -188.1118, 6285.0000, 31.7185 >>, <<10.0, 10.0, 10.0>>, FALSE, carBulldozer , "DOZER", "GETBCKDZ", "GETBCKDZ", FALSE, 0, FALSE)	
			
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				SET_BLIP_COLOUR(sLocatesData.LocationBlip, BLIP_COLOUR_BLUE)
				SET_BLIP_ROUTE_COLOUR(sLocatesData.LocationBlip, BLIP_COLOUR_BLUE)
			ENDIF
			
		BREAK
		
		CASE 3
		CASE 4
		
			IF iBullDozerStage = 3
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<  -188.1118, 6285.0000, 31.7185 >>, <<35.0, 35.0, 20.0>>)
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_DIGGER", CONV_PRIORITY_VERY_HIGH )	
					iBullDozerStage = 4
				ENDIF
			ENDIF
			
//			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chargingBarracks1.thisCar, "ChargingTrucks") > 94.0
//				STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(chargingBarracks1, WEAPONTYPE_CARBINERIFLE, WEAPONTYPE_CARBINERIFLE, 4.0)
//			ENDIF
		
//			IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(chargingBarracks2.thisCar, "ChargingTrucks") > 91.0
//				STOP_SET_PIECE_AND_GET_GUYS_OUT_INTO_COMBAT(chargingBarracks2, WEAPONTYPE_CARBINERIFLE, WEAPONTYPE_CARBINERIFLE, 4.0)
//			ENDIF
		
			//Blip the car lot... smash trhough some shit...
			
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				SET_BLIP_COLOUR(sLocatesData.LocationBlip, BLIP_COLOUR_BLUE)
				SET_BLIP_ROUTE_COLOUR(sLocatesData.LocationBlip, BLIP_COLOUR_BLUE)
			ENDIF
			
			
			IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, << -188.1118, 6285.0000, 31.7185 >>, <<12.0, 12.0, 10.0>>, FALSE, carBulldozer , "DOZER", "GETBCKDZ", "GETBCKDZ", FALSE, 0, FALSE)	
			
				//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0)
		
				IF DOES_BLIP_EXIST(blipUsedCarLot)
					SET_BLIP_ROUTE(blipUsedCarLot, FALSE)
				ENDIF
				
				REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipUsedCarLot)
								
				CLEANUP_COPS_AND_CARS_OUTSIDE_BANK()
				SET_OBJECT_AS_NO_LONGER_NEEDED(oiFenceL)
				SET_OBJECT_AS_NO_LONGER_NEEDED(oiFenceR)
				
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("PS2A_Jeep_Skids")
								
				STOP_AUDIO_SCENE("PS_2A_DRIVE_BULLDOZER")
				
				SET_MODEL_AS_NO_LONGER_NEEDED(mnCopCarModel)
				SET_MODEL_AS_NO_LONGER_NEEDED(mnCopPedModel)
				SET_MODEL_AS_NO_LONGER_NEEDED(PROP_FACGATE_03_L)
				SET_MODEL_AS_NO_LONGER_NEEDED(PROP_FACGATE_03_R)
				mission_stage = STAGE_RESCUE_BUDDYS
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carBulldozer)
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
		ENDIF
	ENDIF
	
	PED_INDEX tempCop
	
	SWITCH iCopMegaPhoneDialogueStage
		
		CASE 0
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND iBullDozerStage >= 2
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_STATUS4", CONV_PRIORITY_VERY_HIGH)
					iCopMegaPhoneDialogueStage++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
		CASE 2
			
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND iCopMegaPhoneDialogueStage = 1	
				PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_PS_2A_03", 0.0)
				iCopMegaPhoneDialogueStage = 2
			ENDIF
						
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-48.631172,6459.656250,37.216557>>, <<-71.482841,6483.615234,30.077419>>, 15.000000)
				//SCRIPT_ASSERT("boom")
				SET_MAX_WANTED_LEVEL(4)
				SETUP_WANTED_LEVEL(4, TRUE, 50.0, 10.0, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)			
				ADD_VEHICLE_COMBAT_ANGLED_AVOIDANCE_AREA(<<16.295980,6553.105957,29.673317>>, <<-224.059769,6311.169922,42.503540>>, 25.250000)

				iCopMegaPhoneDialogueStage = 3
			ENDIF
		BREAK
		
		CASE 3
			IF IS_PED_INJURED(tempCop)
			OR NOT DOES_ENTITY_EXIST(tempCop)
				GET_CLOSEST_PED(GET_ENTITY_COORDS(PLAYER_PED_ID()), 40.0, TRUE, TRUE, tempCop, FALSE, TRUE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(tempCop)	
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 7, tempCop, "RBHCop")
					
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_STOPDZ", CONV_PRIORITY_VERY_HIGH)
					iCopMegaPhoneDialogueStage++
				ENDIF
			ENDIF
		BREAK		
				
		
		CASE 4

			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, 7)
				iCopMegaPhoneDialogueStage++
			ENDIF
		
		BREAK
		
		CASE 5
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_RAM2", CONV_PRIORITY_VERY_HIGH )
					iCopMegaPhoneDialogueStage++
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				GET_CLOSEST_PED(GET_ENTITY_COORDS(PLAYER_PED_ID()), 40.0, TRUE, TRUE, tempCop, FALSE, TRUE)
				IF DOES_ENTITY_EXIST(tempCop)					
					//ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 7, tempCop, "RBHCop")
					//IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_STOPDZ", CONV_PRIORITY_VERY_HIGH)
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 7, tempCop, "RBHCOP")
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_STOPDZ", CONV_PRIORITY_VERY_HIGH)
						iCopMegaPhoneDialogueStage =7
					ENDIF
				ELSE
					iCopMegaPhoneDialogueStage = 7
				ENDIF
					
			ENDIF
		BREAK	
		
		CASE 7
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_RAM2", CONV_PRIORITY_VERY_HIGH )
					iCopMegaPhoneDialogueStage++
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_STATUS1", CONV_PRIORITY_VERY_HIGH )
					SETTIMERB(0)
					iStatusDialogueTimer = GET_GAME_TIMER()
					iCopMegaPhoneDialogueStage++
				ENDIF
			ENDIF
		BREAK
		
		CASE 9
		CASE 10
		CASE 11
		CASE 12
		CASE 13
		CASE 14
		
			IF  GET_GAME_TIMER() - iStatusDialogueTimer > 10000
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				AND iCopMegaPhoneDialogueStage = 9
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_STATUS2", CONV_PRIORITY_VERY_HIGH )
						iCopMegaPhoneDialogueStage = 10
					ENDIF
				ENDIF
			ENDIF
			IF GET_GAME_TIMER() - iStatusDialogueTimer > 20000
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				AND iCopMegaPhoneDialogueStage = 10
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_STATUS3", CONV_PRIORITY_VERY_HIGH )
						iCopMegaPhoneDialogueStage = 11
					ENDIF
				ENDIF
			ENDIF
			IF  GET_GAME_TIMER() - iStatusDialogueTimer > 30000
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				AND iCopMegaPhoneDialogueStage = 11
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_STATUS5", CONV_PRIORITY_VERY_HIGH )
						iCopMegaPhoneDialogueStage = 12
					ENDIF
				ENDIF
			ENDIF
			IF  GET_GAME_TIMER() - iStatusDialogueTimer > 40000
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				AND iCopMegaPhoneDialogueStage = 13
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_STATUS6", CONV_PRIORITY_VERY_HIGH )
						iCopMegaPhoneDialogueStage = 14
					ENDIF
				ENDIF
			ENDIF
			//Fail if you take fucking ages but not if you are close (tank takes over)
			IF  GET_GAME_TIMER() - iStatusDialogueTimer > 60000
			AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedSelector.pedID[SELECTOR_PED_MICHAEL]) > 60.0	
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					IF NOT IS_PED_INJURED(pedSelector.pedID[SELECTOR_PED_MICHAEL])
						SET_ENTITY_HEALTH(pedSelector.pedID[SELECTOR_PED_MICHAEL], 0)
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
	
	ENDSWITCH
	
		IF TIMERA() > 4000
			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), carBulldozer)
				IF NOT IS_PLAYER_TOUCHING_CRUSADER()
					//Only trigger these if there is a guy nearby
					GET_CLOSEST_PED(GET_ENTITY_COORDS(PLAYER_PED_ID()), 40.0, TRUE, TRUE, tempCop, FALSE, TRUE)
					IF DOES_ENTITY_EXIST(tempCop)					
						//ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 7, tempCop, "RBHCop")
						ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 7, tempCop, "RBHCop")
					
						IF bAlternateNotTouchingLInes = FALSE
							//IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_BULLDOZE", CONV_PRIORITY_VERY_HIGH)
							IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_STOPDZ", CONV_PRIORITY_VERY_HIGH)
								bAlternateNotTouchingLInes = TRUE
								SETTIMERA(0)	
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_CMBTF", CONV_PRIORITY_VERY_HIGH)
								bAlternateNotTouchingLInes = FALSE
								SETTIMERA(0)	
							ENDIF
						ENDIF					
					ENDIF
				ELSE		
				
					//SCRIPT_ASSERT("touching!")
				
					IF bAlternateRamLines =  FALSE
						IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_RAM", CONV_PRIORITY_VERY_HIGH )
							bAlternateRamLines =  TRUE
							//SETTIMERA(0)				
						ENDIF
					ELSE
						IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_RAM2", CONV_PRIORITY_VERY_HIGH )
							bAlternateRamLines =  FALSE
							//SETTIMERA(0)	
						ENDIF
					ENDIF						
				ENDIF
			ENDIF
	
	//You destroyed the Dozer!
	IF iBullDozerStage >= 3
		IF DOES_ENTITY_EXIST(carBulldozer)
			IF NOT IS_VEHICLE_DRIVEABLE(carBulldozer)
				MISSION_FLOW_SET_FAIL_REASON("VANDIED")
				Mission_Failed()
			ELSE
				IF IS_VEHICLE_PERMANENTLY_STUCK(carBulldozer)
					MISSION_FLOW_SET_FAIL_REASON("VANDIED")
					Mission_Failed()
				ENDIF
			ENDIF
		ENDIF
	
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<67.973251,6604.539551,30.164473>>, <<-277.692322,6256.652832,69.740997>>, 131.000000)
			MISSION_FLOW_SET_FAIL_REASON("LOSTMT")
			Mission_Failed()
		ENDIF
	ENDIF
	

ENDPROC





PROC cleanUpChickenFactory()
	
	DELETE_ARRAY_OF_PEDS(swatStage1)
	DELETE_ARRAY_OF_PEDS(swatStage2)
	DELETE_ARRAY_OF_PEDS(swatStage3)
	DELETE_ARRAY_OF_PEDS(swatStage4)
	DELETE_ARRAY_OF_PEDS(swatStage5)
	DELETE_ARRAY_OF_PEDS(swatStage6)

ENDPROC


	
VEHICLE_INDEX michaelsCar
	
PROC stageEndMocapScene()
	IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	ENDIF
		
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)

	SEQUENCE_INDEX	taskSeq
	SWITCH iStageEndMocap

		CASE 0
			REQUEST_MODEL(BLAZER)
			
			REQUEST_TRAIN_MODELS()
			SET_RANDOM_TRAINS(FALSE) 
			
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
				MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(pedSelector)
			ENDIF
			
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			iStageEndMocap++
		BREAK

		CASE 1
			STOP_SOUND(iAlarmSounds)
			
			STOP_ALARM("PALETO_BAY_SCORE_CHICKEN_FACTORY_ALARM", FALSE)
			
			SET_CLOCK_TIME(00, 00, 00)
			
			RESTORE_PLAYER_PED_VARIATIONS(pedTrevor())
			RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
			
			CLEAR_PED_PARACHUTE_PACK_VARIATION(pedTrevor())
						
			REMOVE_WEAPON_FROM_PED(pedSelector.pedID[SELECTOR_PED_TREVOR], WEAPONTYPE_MINIGUN)
			//REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), WEAPONTYPE_COMBATMG)
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << 2105.8684, 3661.8772, 37.3045 >>)
			LOAD_SCENE(<< 2105.8684, 3661.8772, 37.3045 >>)
			
			CLEAR_ALL_PED_PROPS(PLAYER_PED_ID())
			CLEAR_ALL_PED_PROPS(pedSelector.pedID[SELECTOR_PED_TREVOR])
			
			IF NOT IS_PED_INJURED(pedSelector.pedID[SELECTOR_PED_TREVOR])
				CLEAR_PED_WETNESS(pedSelector.pedID[SELECTOR_PED_TREVOR])
				CLEAR_PED_BLOOD_DAMAGE(pedSelector.pedID[SELECTOR_PED_TREVOR])
				RESET_PED_VISIBLE_DAMAGE(pedSelector.pedID[SELECTOR_PED_TREVOR])
			ENDIF
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
				CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
				RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
			ENDIF
			IF NOT IS_PED_INJURED(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
				CLEAR_PED_WETNESS(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
				CLEAR_PED_BLOOD_DAMAGE(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
				RESET_PED_VISIBLE_DAMAGE(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
			ENDIF
			
			//SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_JEWEL_HEIST)
			
			//SET_PED_COMP_ITEM_CURRENT_SP(pedTrevor(), COMP_TYPE_OUTFIT, OUTFIT_P2_STYLESUIT_5)
			
			REQUEST_CUTSCENE("RBH_2A_EXT_1")			
			iStageEndMocap++
		BREAK
	 
		CASE 2
		
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())
			ENDIF
			IF NOT IS_ENTITY_DEAD(pedSelector.pedID[SELECTOR_PED_TREVOR])
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", pedSelector.pedID[SELECTOR_PED_TREVOR])
			ENDIF
			
			SET_CUTSCENE_PED_OUTFIT("Franklin", PLAYER_ONE, OUTFIT_P1_STEALTH_NO_MASK)				
			SET_CUTSCENE_PED_OUTFIT("Michael", PLAYER_ZERO, OUTFIT_P0_JEWEL_HEIST)
			SET_CUTSCENE_PED_OUTFIT("Trevor", PLAYER_TWO, OUTFIT_P2_STYLESUIT_5)
				
		
			IF HAS_CUTSCENE_LOADED()
			AND HAS_MODEL_LOADED(BLAZER)
			AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				iStageEndMocap++
			ENDIF
		BREAK
	
		CASE 3
		
			REGISTER_ENTITY_FOR_CUTSCENE(michaelsCar, "RONS_QUAD", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, BLAZER)
		
			REGISTER_ENTITY_FOR_CUTSCENE(NULL, "RBH_Truck", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, UTILLITRUCK)

			IF NOT IS_ENTITY_DEAD(pedTrevor())
				REGISTER_ENTITY_FOR_CUTSCENE(pedTrevor(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY )
			ENDIF				
			IF NOT IS_ENTITY_DEAD(pedFranklin())
				REGISTER_ENTITY_FOR_CUTSCENE(pedFranklin(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF				
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			SET_CUTSCENE_FADE_VALUES(FALSE, TRUE, FALSE, TRUE)
			START_CUTSCENE()
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
			//FADE_IN_IF_NEEDED()
			iStageEndMocap++
		BREAK
	
		CASE 4
		
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("RBH_Truck"))
				IF NOT IS_ENTITY_DEAD(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("RBH_Truck")))
				AND GET_CUTSCENE_TIME() > 66889
					
					SET_VEHICLE_USE_PLAYER_LIGHT_SETTINGS(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("RBH_Truck")), TRUE)
					
					SET_VEHICLE_LIGHTS(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("RBH_Truck")), FORCE_VEHICLE_LIGHTS_ON)
					//SET_VEHICLE_FULLBEAM(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("RBH_Truck")), TRUE)
				ENDIF
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				REPLAY_STOP_EVENT()
				RESET_GAME_CAMERA()
			ENDIF
			
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("RBH_Truck"))
				cutsceneTruck = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("RBH_Truck"))
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(michaelsCar)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("RONS_QUAD"))
					michaelsCar = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("RONS_QUAD"))
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				IF IS_ENTITY_OK(cutsceneTruck) AND IS_ENTITY_OK(pedTrevor())
					SET_PED_INTO_VEHICLE(pedTrevor(), cutsceneTruck)
				ENDIF
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				IF IS_ENTITY_OK(cutsceneTruck) AND IS_ENTITY_OK(pedFranklin())
					SET_PED_INTO_VEHICLE(pedFranklin(), cutsceneTruck, VS_FRONT_RIGHT)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedFranklin())
				ENDIF
			ENDIF

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("RBH_Truck")
				IF IS_ENTITY_OK(cutsceneTruck) AND IS_ENTITY_OK(pedTrevor())
					CPRINTLN(DEBUG_MISSION, "Tasking cutscene truck to drive")
					
					SET_VEHICLE_ENGINE_ON(cutsceneTruck, TRUE, TRUE)
					SET_VEHICLE_FORWARD_SPEED(cutsceneTruck, 10.0)
					
					SET_PED_KEEP_TASK(pedTrevor(), TRUE)
					IF NOT IS_PED_IN_ANY_VEHICLE(pedTrevor(), TRUE)
						SET_PED_INTO_VEHICLE(pedTrevor(), cutsceneTruck)
					ENDIF
					
//					SET_PED_INTO_VEHICLE(pedFranklin(), cutsceneTruck, VS_FRONT_RIGHT)
					
					OPEN_SEQUENCE_TASK(taskSeq)
						TASK_VEHICLE_DRIVE_TO_COORD(NULL, cutsceneTruck, (<<2223.78491, 3810.99438, 32.89964>>), 30.0, DRIVINGSTYLE_NORMAL, UTILLITRUCK, DRIVINGMODE_AVOIDCARS, 1.0, -1)
						TASK_SMART_FLEE_PED(NULL, PLAYER_PED_ID(), 200, -1)
						//TASK_VEHICLE_DRIVE_WANDER(NULL, cutsceneTruck, 30.0, DRIVINGMODE_AVOIDCARS)
					CLOSE_SEQUENCE_TASK(taskSeq)
					TASK_PERFORM_SEQUENCE(pedTrevor(), taskSeq)
					CLEAR_SEQUENCE_TASK(taskSeq)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedTrevor())
					
									
				ENDIF
			ENDIF
		
			IF HAS_CUTSCENE_FINISHED()
				
				pedSelector.pedID[SELECTOR_PED_TREVOR] = NULL
				pedSelector.pedID[SELECTOR_PED_FRANKLIN] = NULL
				
				DISPLAY_HUD(TRUE)
				DISPLAY_RADAR(TRUE)
				
				IF HAVE_TRAIN_MODELS_LOADED()
					chickenTrain = CREATE_MISSION_TRAIN(2, <<2057.9905, 3651.9834, 37.5503>>, TRUE)
					//SCRIPT_ASSERT("1!")
					SET_TRAIN_CRUISE_SPEED(chickenTrain, 0.0)
				ENDIF
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				//RESET_GAME_CAMERA()
				//michaelsCar = CREATE_VEHICLE(BLAZER, <<2074.8586, 3695.6160, 32.7810>>, 26.8363)
				//SET_VEHICLE_ON_GROUND_PROPERLY(michaelsCar)
				mission_stage = STAGE_END_SCREEN //Mission_Passed()
				iStageEndMocap++	
			ENDIF
			
		BREAK
					
	ENDSWITCH
	
ENDPROC

PROC stageEndScreen()
	
	//Stat details for : RH2_ACTUAL_TAKE
	//MISSION_STAT_TYPE_FINANCE_DIRECT // A dollar value set by mission
	//INFORM_MISSION_STATS_OF_FINANCIAL_DAMAGE(iCurrentTake)
	
	//Stat details for : RH2_PERSONAL_TAKE
	//MISSION_STAT_TYPE_FINANCE_DIRECT // A dollar value set by mission - need to speak to Rolli about getting this divided up
	//INFORM_MISSION_STATS_OF_FINANCIAL_DAMAGE(iCurrentTake)
	
	FADE_IN_IF_NEEDED()
	
	SET_HEIST_END_SCREEN_POTENTIAL_TAKE(HEIST_RURAL_BANK, RURAL_HEIST_TAKE)
	SET_HEIST_END_SCREEN_ACTUAL_TAKE(HEIST_RURAL_BANK, RURAL_HEIST_TAKE)
	ADD_PENALTY_TO_HEIST(HEIST_RURAL_BANK, HEIST_PENALTY_MONEY_DROPPED, RURAL_HEIST_TAKE - iCurrentTake)
	
	IF bIsBuddyAPro
		SET_HEIST_END_SCREEN_CREW_STATUS(HEIST_RURAL_BANK, GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_RURAL_BANK,0), CMST_FINE)
	ELSE	
		SET_HEIST_END_SCREEN_CREW_STATUS(HEIST_RURAL_BANK, GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_RURAL_BANK,0), CMST_KILLED)
	ENDIF
	
	//DISPLAY_HEIST_END_SCREEN(HEIST_RURAL_BANK)
	
	MISSION_PASSED()

ENDPROC

PROC CLEANUP_ALL_ENTITIES()

	//Delete all entities...
	
	bForceCleanupOfRammingCar = FALSE
	
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	
	IF iMoneyCountEffect <> -1
	  	//STOP_SOUND(iMoneyCountEffect)
		iMoneyCountEffect = -1
	ENDIF
	
	TRIGGER_MUSIC_EVENT("RH2A_STOP_TRACK")
	
	bBagCreated = FALSE
	DELETE_OBJECT(oiBuddysBag)
	REMOVE_BLIP(blipBuddysBag)	
	bForceApplied = FALSE
	
	iSecondTank= 0

	IF IPL_GROUP_SWAP_IS_ACTIVE()
		IPL_GROUP_SWAP_CANCEL()
	ENDIF
	
	STOP_AUDIO_SCENES()
	CLEANUP_SET_PIECE_COP_CAR(secondTank, TRUE)
	CLEANUP_SET_PIECE_COP_CAR(thirdTank, TRUE)
					
	DELETE_ROPE(ropesTank[0])
	DELETE_ROPE(ropesTank[1])
	DELETE_ROPE(ropesTank[2])
	DELETE_ROPE(ropesTank[3])
			
	DELETE_VEHICLE(chinooks[0])
			
	DELETE_PED(chinookPilots[0])
	DELETE_VEHICLE(chinooksTank)
	DELETE_PED(chinooksTankDriver)
	
		
	DELETE_OBJECT(oiBagGunman)
	DELETE_OBJECT(oiBagMichael)
	DELETE_OBJECT(oiBagTrevor)
	DELETE_OBJECT(oiBagStrapTrevor)
	
	DELETE_PED(pedSheriff_cover)
	DELETE_PED(pedSheriff_CarDoor)
	
	REMOVE_COVER_POINT(spareCoverPoint)
	REMOVE_COVER_POINT(extraChickenFactoryCover)
	REMOVE_COVER_POINT(cpTelephonePole)
	REMOVE_COVER_POINT(coverPointOnRoof)
	REMOVE_COVER_POINT(cpAtEndOfHallInChickenFactory)
	REMOVE_COVER_POINT(cpAtEndOfHallInChickenFactory2)
	REMOVE_COVER_POINT(cpAtStartOfHallInChickenFactory)	
	REMOVE_COVER_POINT(cpBeforeLastRoom)
	
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("PS2A_Enter_Bank")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("PS2A_Enter_Bank_2")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("carBUlldzoer")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("Test_Pain")
				
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("PS2A_Gas_Station_Explosion_L")
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("PS2A_Gas_Station_Explosion_R")
	
	CLEAR_AREA(<<0.0, 0.0, 0.0>>, 3000.0, TRUE)
	
	STOP_ALARM("PALETO_BAY_SCORE_ALARM", TRUE)
	STOP_ALARM("PALETO_BAY_SCORE_CHICKEN_FACTORY_ALARM", TRUE)
	
	REMOVE_COVER_POINT(cpAtEndOfHallInChickenFactory)
	
	CLOSE_BANK_DOORS()
	
	//RESTORE_PLAYER_PED_VARIATIONS(pedFranklin())
	RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
	RESTORE_PLAYER_PED_VARIATIONS(PedMichael())
	RESTORE_PLAYER_PED_VARIATIONS(pedTrevor())
	
	RESET_INJURED_BOOLS()
	
	REMOVE_VEHICLE_ASSET(BURRITO3)
	
	IF NOT IS_PED_INJURED(pedFranklin())
		CLEAR_PED_TASKS_IMMEDIATELY(pedFranklin())
	ENDIF
	IF NOT IS_PED_INJURED(pedTrevor())
		CLEAR_PED_TASKS_IMMEDIATELY(pedTrevor())
	ENDIF
	IF NOT IS_PED_INJURED(pedMichael())
		CLEAR_PED_TASKS_IMMEDIATELY(pedMichael())
	ENDIF
	
	IF DOES_ENTITY_EXIST(chickenTrain)
		SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(chickenTrain)
	ENDIF
	
	DELETE_VEHICLE(chinooksTank)
	DELETE_ARRAY_OF_VEHICLES(chinooks)
	DELETE_ARRAY_OF_PEDS(chinookPilots)
	
	DELETE_ARRAY_OF_PEDS(pedsExtraMotelEntranceGuys)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(RHINO)
	SET_MODEL_AS_NO_LONGER_NEEDED(CARGOBOB)
			
	REMOVE_VEHICLE_RECORDING(5, "RBHChin")
	REMOVE_VEHICLE_RECORDING(2, "RBHFlyover")
	REMOVE_VEHICLE_RECORDING(1, "CBRam")
	
	SET_MODEL_AS_NO_LONGER_NEEDED(S_F_Y_AIRHOSTESS_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_BUSINESS_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(P_LD_HEIST_BAG_S_1)
	
	DELETE_ARRAY_OF_PEDS(pedsBankers, TRUE)
	
	DELETE_ARRAY_OF_PEDS(pedsArmyGuysToHoldOff, TRUE)
	DELETE_ARRAY_OF_BLIPS(blipsArmyGuysToHoldOff)
	
	DELETE_VEHICLE(vehCopCarOutFront)
	SET_VEHICLE_AS_NO_LONGER_NEEDED(michaelsCar)
	REMOVE_ANIM_DICT("MissHeistPaletoScore1")

	REMOVE_ANIM_DICT("MissHeistPaletoPinned")
					
	SET_MODEL_AS_NO_LONGER_NEEDED(CRUSADER)
	
	REMOVE_WAYPOINT_RECORDING("RBHGdnCrew")
	REMOVE_WAYPOINT_RECORDING("RBHGdnMichael")
	
	REMOVE_WAYPOINT_RECORDING("RBHFRANKBOAT")
	
	//REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipMotel)
	
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipInsideFactory)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipInsideBank)
	
	DELETE_VEHICLE(franklinsBoat)
	SET_MODEL_AS_NO_LONGER_NEEDED(SQUALO)
	
	DELETE_OBJECT(oiBankDoorLeft)
	DELETE_OBJECT(oiBankDoorRight)
	//DELETE_OBJECT(oiStoreDoorTempCount)
	DELETE_OBJECT(oiStoreDoorTempVault)
	DELETE_OBJECT(oiBagTrevor)
	DELETE_OBJECT(oiBlowTorch)
	DELETE_OBJECT(oiWeldingMask)
	
	DELETE_PED(pedsBankers[0])
	DELETE_PED(pedsBankers[1])
	DELETE_PED(pedsBankers[2])
	DELETE_PED(pedsBankers[3])
	DELETE_PED(pedsBankers[4])
	
	CLEANUP_SET_PIECE_COP_CAR(setPieceRiotVan1, TRUE)
	CLEANUP_SET_PIECE_COP_CAR(setPieceRiotVan2, TRUE)
	CLEANUP_SET_PIECE_COP_CAR(setPieceExtraStreetCopCar, TRUE)
	CLEANUP_SET_PIECE_COP_CAR(setPieceDownStreetInitialCop1, TRUE)
//	CLEANUP_SET_PIECE_COP_CAR(setPieceDownStreetInitialCop2, TRUE)
	CLEANUP_SET_PIECE_COP_CAR(setPieceDownStreetInitialCop3, TRUE)
	CLEANUP_SET_PIECE_COP_CAR(setPieceDownStreetSwat2ndWave, TRUE)
	
	
	REMOVE_VEHICLE_RECORDING(4, "RBHStreet")
	REMOVE_VEHICLE_RECORDING(6, "RBHStreet")
	REMOVE_VEHICLE_RECORDING(7, "RBHStreet")
	REMOVE_VEHICLE_RECORDING(8, "RBHStreet")
//	REMOVE_VEHICLE_RECORDING(9, "RBHStreet")
	REMOVE_VEHICLE_RECORDING(10, "RBHStreet")
	
	REMOVE_VEHICLE_RECORDING(1, "RBHInitial")
	REMOVE_VEHICLE_RECORDING(2, "RBHInitial")
	REMOVE_VEHICLE_RECORDING(3, "RBHInitial")
	REMOVE_VEHICLE_RECORDING(6, "RBHInitial")
	REMOVE_VEHICLE_RECORDING(7, "RBHInitial")
	
	REMOVE_ANIM_DICT("missheistpaletoscore2mcs_2_pt1")
	REMOVE_ANIM_DICT("missheistpaletoscore2mcs_2_p5")
	REMOVE_ANIM_DICT("missheistpaletoscore2mcs_2_p6")
	
	CLEANUP_SET_PIECE_COP_CAR(chargingBarracks1)
	//CLEANUP_SET_PIECE_COP_CAR(chargingBarracks2)
	CLEANUP_SET_PIECE_COP_CAR(EnemyTank)
	CLEANUP_SET_PIECE_COP_CAR(continueDownStreetPullUpCar)
	
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipSafe)	
	
	REMOVE_COVER_POINT(extraChickenFactoryCover)
	
	CLEANUP_CARS_OUTSIDE_CHICKEN_FACTORY()
	
	CLEANUP_SET_PIECE_COP_CAR(chinnokLandingAtEndOfStreet, TRUE)
	//DELETE_ARRAY_OF_VEHICLES(tyrePopArray)

	DELETE_OBJECT(oiBlowTorch)
	DELETE_OBJECT(oiWeldingMask)
				
	SET_MODEL_AS_NO_LONGER_NEEDED(P_LD_HEIST_BAG_S_1)
	SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Welding_Mask_01_S)
	SET_MODEL_AS_NO_LONGER_NEEDED(prop_tool_blowtorch)

	DELETE_OBJECT(oiBankDoorLeft)
	DELETE_OBJECT(oiBankDoorRight)

	DELETE_OBJECT(oiBagTrevor)
	//DELETE_OBJECT(oiBagGunman)
	
//	DELETE_VEHICLE(truckTanker)
//	DELETE_VEHICLE(truckTankerTrailer)
	
	DELETE_ARRAY_OF_VEHICLES(cutsceneCars)
	
	DELETE_ARRAY_OF_PEDS(cutscenePeds)

	DELETE_ARRAY_OF_VEHICLES(CopCarsParked)
	DELETE_ARRAY_OF_BLIPS(blipPedsCopsOutsideBank)
	DELETE_ARRAY_OF_PEDS(pedsCopsOutsideBank)

	DELETE_ARRAY_OF_PEDS(runnerCops1)
	DELETE_ARRAY_OF_PEDS(runnerCops2)

	CLEANUP_SET_PIECE_COP_CAR(setpieceHoldOffStage1)
	CLEANUP_SET_PIECE_COP_CAR(setpieceHoldOffStage2)
	
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipEnemyTank)
	
	DELETE_VEHICLE(chopperCops1)
	DELETE_PED(PilotChopperCops1)
	DELETE_PED(Pass1ChopperCops1)
	DELETE_PED(Pass2ChopperCops1)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipChopperCops1)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipChopperCops1)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipForFence)

	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipMichael)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipBuddy)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipFranklin)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipTrevor)

	DELETE_PED(pedMaid)
	DELETE_ARRAY_OF_PEDS(pedsFodderCops)

	DELETE_ARRAY_OF_VEHICLES(carsFodderVehicles)

	KILL_PTFX(iSmokeEffectPetrolStation)
	KILL_PTFX(iSmokeEffect)

	DELETE_ARRAY_OF_SET_PIECECOPS(carsPoliceBackup)
	DELETE_ARRAY_OF_SET_PIECECOPS(carsPolicePoolside)

	CLEANUP_SET_PIECE_COP_CAR(visualSetPieceChopperAdvanceToGardens)

	DELETE_ARRAY_OF_VEHICLES(CopCarsParkedDownStreet)
	DELETE_ARRAY_OF_PEDS(pedsCopsInStreet)
	DELETE_ARRAY_OF_PEDS(pedsExtraSwatInStreet)
	
	DELETE_ARRAY_OF_BLIPS(blipPedsCopsInStreet)

	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipCoverCar)
	DELETE_VEHICLE(carCover)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipCarCover)
	CLEANUP_SET_PIECE_COP_CAR(chopperFlyover)
	CLEANUP_SET_PIECE_COP_CAR(ArmyTruckStreet1)
	CLEANUP_SET_PIECE_COP_CAR(ArmyTruckStreet2)

	REMOVE_SCRIPT_FIRE(fiScriptFire)
	REMOVE_SCRIPT_FIRE(fiScriptFire2)

	DELETE_ARRAY_OF_SET_PIECECOPS(copCarsPullingUpStage2)
	DELETE_ARRAY_OF_PEDS(stage2ExtraRunners)
	DELETE_ARRAY_OF_SET_PIECECOPS(copCarsBlocking)

	DELETE_ARRAY_OF_VEHICLES(CopCarsParked3)
	DELETE_ARRAY_OF_PEDS(pedsArmyInJunkYard)
	
	DELETE_ARRAY_OF_BLIPS(blippedsArmyInJunkYard)
	DELETE_ARRAY_OF_BLIPS(blipsArmyGuysToHoldOff)
	DELETE_ARRAY_OF_VEHICLES(chinooks)
	DELETE_ARRAY_OF_PEDS(chinookPilots)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipUsedCarLot)
	CLEANUP_SET_PIECE_COP_CAR(chinookDropSetPiece)

	DELETE_VEHICLE(carBulldozer)
	
	SET_MODEL_AS_NO_LONGER_NEEDED(BULLDOZER)
	SET_MODEL_AS_NO_LONGER_NEEDED(RHINO)
	
	CLEANUP_SET_PIECE_COP_CAR(dozerStageWave1Cop1)
	CLEANUP_SET_PIECE_COP_CAR(dozerStageWave1Cop2)
	CLEANUP_SET_PIECE_COP_CAR(dozerStageWave1Cop3)
	DELETE_BULLDOZER_SET_PIECE_CARS()
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipChickenFactory)

	DELETE_ARRAY_OF_PEDS(swatStage1)
	//DELETE_ARRAY_OF_BLIPS(blipSwatStage1)
	DELETE_ARRAY_OF_PEDS(chickenPed)
	DELETE_ARRAY_OF_PEDS(swatStage2)
	//DELETE_ARRAY_OF_BLIPS(blipSwatStage2)
	DELETE_ARRAY_OF_PEDS(swatStage3)
	//DELETE_ARRAY_OF_BLIPS(blipSwatStage3)
	DELETE_ARRAY_OF_PEDS(swatStage4)
	//DELETE_ARRAY_OF_BLIPS(blipSwatStage4)
	DELETE_ARRAY_OF_PEDS(swatStage5)
	//DELETE_ARRAY_OF_BLIPS(blipSwatStage5)
	DELETE_ARRAY_OF_PEDS(swatStage6)
	//DELETE_ARRAY_OF_BLIPS(blipSwatStage6)

	DELETE_ARRAY_OF_PEDS(pedFactoryWorker)
	//REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipTrainTracks)

	REMOVE_COVER_POINT(spareCoverPoint)
	REMOVE_COVER_POINT(cpTelephonePole)
	REMOVE_COVER_POINT(cpChickenFactFrier)
	
	IF DOES_ENTITY_EXIST(chickenTrain)
		DELETE_MISSION_TRAIN(chickenTrain)
	ENDIF
	IF DOES_ENTITY_EXIST(chickenTrainPlatform)
		DELETE_MISSION_TRAIN(chickenTrainPlatform)
	ENDIF
	
	IF DOES_ENTITY_EXIST(arrivalBurrito)
		IF NOT IS_ENTITY_DEAD(arrivalBurrito)
			SET_ENTITY_AS_MISSION_ENTITY(arrivalBurrito)
		ENDIF
		DELETE_VEHICLE(arrivalBurrito)
	ENDIF
	
	DELETE_VEHICLE(cutsceneTruck)
	
	DELETE_ARRAY_OF_VEHICLES(vehBankParkedCars)
	CLEANUP_SET_PIECE_COP_CAR(setpieceIniitalCops1)
	CLEANUP_SET_PIECE_COP_CAR(setpieceIniitalCops2)
	CLEANUP_SET_PIECE_COP_CAR(setpieceIniitalCops3)
	CLEANUP_SET_PIECE_COP_CAR(setpieceIniitalCops4)
	CLEANUP_SET_PIECE_COP_CAR(setpieceIniitalCops5)

ENDPROC


#IF IS_DEBUG_BUILD
		
	INT iDebugeAllyAdvanceGardenState
		
	PROC debugProcedures()


 		iDebugeAllyAdvanceGardenState = ENUM_TO_INT(eAllyAdvanceGardenState)
		iDebugMissionStage = ENUM_TO_INT(mission_stage)

		INT iTotalEntityCount = iVehicleCount + iPedCount + iObjectCount

		IF bDebugInitialised = FALSE
			//Init debug stuff.
			ruralHeistWidgetGroup = START_WIDGET_GROUP("HEIST - RURAL BANK")
//				
//				ADD_WIDGET_VECTOR_SLIDER("vCigarOffset", vCigarOffset, -1000.0, 1000.0, 0.01)
//				ADD_WIDGET_VECTOR_SLIDER("vCigarRotation", vCigarRotation, -360.0, 360.0, 1.0)
							
				START_WIDGET_GROUP("Train Speeds On Platform")
					ADD_WIDGET_FLOAT_SLIDER("Train Speed", flRAGTrainSpeed, 0.0, 50.0, 0.5)
					ADD_WIDGET_FLOAT_SLIDER("Train Cruise Speed", flRAGTrainCruiseSpeed, 0.0, 50.0, 0.5)
//					ADD_WIDGET_FLOAT_SLIDER("Train Failure Time", flRAGTimeToFailPlatform, 0.0, 50.0, 0.5)
//					ADD_WIDGET_FLOAT_SLIDER("Train Warn Dialogue Time", flRAGTimeToWarnTrain, 0.0, 50.0, 0.5)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Scripted Tank Scene")
					ADD_WIDGET_FLOAT_SLIDER("Tank Start Time", flWidgetTankRecordingStartTime, 0.0, 10000.0, 1.0)			
					ADD_WIDGET_FLOAT_SLIDER("Tank Speed", flWidgetTankRecordingSpeed, 0.0, 2.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Time to Fire the Gun", flWidgetTankFiredTime, 0.0, 10.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Time to End Scene", flWidgetTankSceneFinishTime, 0.0, 10.0, 0.01)
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Rescue Buddies Scene")
					ADD_WIDGET_BOOL("Dozer Invincible?", bWidgetBulldozerInvincible)
					ADD_WIDGET_FLOAT_SLIDER("Tank Turret Speed", flWidgetTankTurretSpeed, 0.0, 5.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Tank Slow Speed", flWidgetTankRubberBandSlowSpeed, 0.0, 5.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Tank Moderate Speed", flWidgetTankRubberBandModerateSpeed, 0.0, 5.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Tank Fast Speed", flWidgetTankRubberBandFastSpeed, 0.0, 5.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Tank Speed Delta", flWidgetTankSpeedDelta, 0.0, 1.0, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("Tank Distance to Speed Up", flWidgetTankFastDistance, 0.0, 300.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Tank Distance to Slow Down", flWidgetTankSlowDistance, 0.0, 300.0, 0.01)
					ADD_WIDGET_FLOAT_SLIDER("Tank Kill Distance", flWidgetTankKillDistance, 0.0, 300.0, 1.0)
					ADD_WIDGET_FLOAT_SLIDER("Bulldozer Safety Arc", flWidgetArcOfSafety, 0.0, 300.0, 1.0)
					ADD_WIDGET_FLOAT_SLIDER("Shot Min Distance", flMinCannonShotDistance, 0.0, 300.0, 1.0)
					ADD_WIDGET_FLOAT_SLIDER("Shot Max Distance", flMaxCannonShotDistance, 0.0, 300.0, 1.0)
					ADD_WIDGET_FLOAT_SLIDER("Shot Min Time", flTimeMinBetweenShots, 0.0, 300.0, 1.0)
					ADD_WIDGET_FLOAT_SLIDER("Shot Max Time", flTimeMaxBetweenShots, 0.0, 300.0, 1.0)
					
					ADD_WIDGET_STRING("READ-ONLY TANK SHOT TUNING VALUES")
					ADD_WIDGET_FLOAT_READ_ONLY("flTankCurrentSpeed", flTankCurrentSpeed)
					ADD_WIDGET_FLOAT_READ_ONLY("flMinimumSafeAngle", flMinimumSafeAngle)
					ADD_WIDGET_FLOAT_READ_ONLY("flMaximumSafeAngle", flMaximumSafeAngle)
					ADD_WIDGET_FLOAT_READ_ONLY("flTankShotAngle", flTankShotAngle)
					ADD_WIDGET_FLOAT_READ_ONLY("flWidgetArcOfSafety", flWidgetArcOfSafety)
					
				STOP_WIDGET_GROUP()
				
				ADD_WIDGET_INT_SLIDER("Vehicle Starting Health", iWidgetCopCarStartingHealth, 0, 2000, 10)
				ADD_WIDGET_INT_SLIDER("Vehicle Explosion Health", iWidgetCopCarExplosionHealth, 0, 2000, 10)
			
				ADD_WIDGET_BOOL("bIsSuperDebugEnabled", bIsSuperDebugEnabled)
								
				START_WIDGET_GROUP("Entity Counter")	
					ADD_WIDGET_INT_READ_ONLY("iPedCount", iPedCount)
					ADD_WIDGET_INT_READ_ONLY("iVehicleCount", iVehicleCount)
					ADD_WIDGET_INT_READ_ONLY("iObjectCount", iObjectCount)
				
					ADD_WIDGET_INT_READ_ONLY("TOTAL ", iTotalEntityCount)
				STOP_WIDGET_GROUP()
				
				ADD_WIDGET_STRING("Stage Variables")
			
				ADD_WIDGET_INT_READ_ONLY("mission_stage", iDebugMissionStage)
								
				
				ADD_WIDGET_INT_READ_ONLY("iIntroMocapStage", iIntroMocapStage)
				ADD_WIDGET_INT_READ_ONLY("iGetDealWithFranklin", iGetDealWithFranklin)

				

				
				ADD_WIDGET_INT_READ_ONLY("iGetToBankStage", iGetToBankStage)
				ADD_WIDGET_INT_READ_ONLY("iRobBankStage", iRobBankStage)
				ADD_WIDGET_INT_READ_ONLY("iRobBankStageGunman", iRobBankStageGunman)
				
				
				ADD_WIDGET_INT_READ_ONLY("iHeistCutseneStage", iHeistCutseneStage)
				ADD_WIDGET_INT_READ_ONLY("iGasStationExplosion", iGasStationExplosion)
				ADD_WIDGET_INT_READ_ONLY("iChopperStage", iChopperStage)
				ADD_WIDGET_INT_READ_ONLY("iStageAdvanceToGardens", iStageAdvanceToGardens)
				ADD_WIDGET_INT_READ_ONLY("iFenceSmashStage", iFenceSmashStage)
				ADD_WIDGET_INT_READ_ONLY("eAllyAdvanceGardenState", iDebugeAllyAdvanceGardenState)


				ADD_WIDGET_INT_READ_ONLY("iShootingStageMichael", iShootingStageMichael)
				ADD_WIDGET_INT_READ_ONLY("iShootingStageCrewMember", iShootingStageCrewMember)
				ADD_WIDGET_INT_READ_ONLY("iContinueDownStreetStage", iContinueDownStreetStage)

				ADD_WIDGET_BOOL("bIsBuddyAPro", bIsBuddyAPro)
				ADD_WIDGET_INT_READ_ONLY("iJunkYardState", iJunkYardState	)
				ADD_WIDGET_INT_READ_ONLY("iControlRamGunshots", iControlRamGunshots	)
				
				
				ADD_WIDGET_INT_READ_ONLY("iStageSwitchToBullDozer", iStageSwitchToBullDozer	)
								
				ADD_WIDGET_INT_READ_ONLY("iStageRescueBuddys", iStageRescueBuddys)
							
				ADD_WIDGET_INT_READ_ONLY("iBullDozerStage", iBullDozerStage)
				ADD_WIDGET_INT_READ_ONLY("iStageChickenFactory", iStageChickenFactory	)
				
				ADD_WIDGET_INT_READ_ONLY("iDealWithChinooks", iDealWithChinooks)
				
				ADD_WIDGET_INT_READ_ONLY("iMotelExplosion", iMotelExplosion)
				
				
				
				
				ADD_WIDGET_STRING("end of Stage Variables")

				ADD_WIDGET_INT_READ_ONLY("iCopRespawnIndex", iCopRespawnIndex)
				ADD_WIDGET_INT_READ_ONLY("iCopRespawnInStreetIndex", iCopRespawnInStreetIndex)
				ADD_WIDGET_INT_READ_ONLY("iArmyRespawnIndex", iArmyRespawnIndex)
				
				ADD_WIDGET_INT_READ_ONLY("iBodyCount", iBodyCount)

				ADD_WIDGET_INT_READ_ONLY("iNumberOfSoldiersSpawned", iNumberOfSoldiersSpawned)
				
				ADD_WIDGET_INT_READ_ONLY("iShootingSwitchC", iShootingSwitchC)
				ADD_WIDGET_INT_READ_ONLY("iShootingSwitchM", iShootingSwitchM)
				
				ADD_WIDGET_INT_READ_ONLY("iCopMegaPhoneDialogueStage", iCopMegaPhoneDialogueStage)
				
				ADD_WIDGET_INT_READ_ONLY("iCurrentTake", iCurrentTake)
				ADD_WIDGET_INT_READ_ONLY("iDisplayedTake", iDisplayedTake)
				
				
						
				START_WIDGET_GROUP("Line Probe stuff")						
					ADD_WIDGET_FLOAT_SLIDER("fXOffset", fXOffset, -100.0, 100.0, 0.05)			
					ADD_WIDGET_FLOAT_SLIDER("fyOffset", fyOffset, -100.0, 100.0, 0.05)			
					ADD_WIDGET_FLOAT_SLIDER("fZOffset", fZOffset, -100.0, 100.0, 0.05)			
				
					ADD_WIDGET_VECTOR_SLIDER("vRotationModifier", vRotationModifier, -3000.0, 3000.0, 0.1)
				STOP_WIDGET_GROUP()
					
				ADD_WIDGET_VECTOR_SLIDER("vLocateDimensions", vLocateDimensions, -3000.0, 3000.0, 0.1)
				
				START_WIDGET_GROUP("Attachment offsets")		
					ADD_WIDGET_VECTOR_SLIDER("vMaskRotation", vMaskRotation, -360.0, 360.0, 0.5)
					ADD_WIDGET_VECTOR_SLIDER("vTorchPosition", vTorchPosition, -999.0, 999.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("vTorchRotation", vTorchRotation, -360.0, 360.0, 0.5)
				
				
					ADD_WIDGET_VECTOR_SLIDER("vTorchEffectPosition", vTorchEffectPosition, -999.0, 999.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("vTorchEffectRotation", vTorchEffectRotation, -360.0, 360.0, 0.5)
				STOP_WIDGET_GROUP()				
								
				ADD_WIDGET_BOOL("bRunDebugLocates", bRunDebugLocates)
				
//				START_WIDGET_GROUP("Line Probe stuff")						
//					//ADD_WIDGET_FLOAT_SLIDER("fPlaybackSpeedNumerator", fPlaybackSpeedNumerator, 0.0, 100.0, 0.05)
//					ADD_WIDGET_FLOAT_READ_ONLY("mainPlaybackSpeed", mainPlaybackSpeed)
//					ADD_WIDGET_FLOAT_SLIDER("fMinPlaybackSpeed", fMinPlaybackSpeed, 0.0, 10.0, 0.05)				
//					ADD_WIDGET_FLOAT_SLIDER("fMaxPlaybackSpeed", fMaxPlaybackSpeed, 0.0, 10.0, 0.05)				
//				STOP_WIDGET_GROUP()
				
				
				//ADD_WIDGET_FLOAT_SLIDER("fVolumeControl", fVolumeControl, 0.0, 1000.0, 0.5)
				
				ADD_WIDGET_FLOAT_READ_ONLY("debugFloat1", debugFloat1)
				ADD_WIDGET_FLOAT_READ_ONLY("debugFloat2", debugFloat2)
//				
//				ADD_WIDGET_FLOAT_READ_ONLY("fDistanceToMotelEntranceDelta", fDistanceToMotelEntranceDelta)
//				ADD_WIDGET_FLOAT_READ_ONLY("fDistanceToMotelEntrancePrev", fDistanceToMotelEntrancePrev)
//				ADD_WIDGET_FLOAT_READ_ONLY("fDistanceToMotelEntrance", fDistanceToMotelEntrance)
				
				ADD_WIDGET_VECTOR_SLIDER("vDebugForce", vDebugForce, -3000.0, 3000.0, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("vDebugOffset", vDebugOffset, -3000.0, 3000.0, 0.1)
				
			STOP_WIDGET_GROUP()		
			
			SET_LOCATES_HEADER_WIDGET_GROUP(ruralHeistWidgetGroup)
			
			SkipMenuStruct[ENUM_TO_INT(STAGE_TIME_LAPSE)].bSelectable = FALSE			
			SkipMenuStruct[ENUM_TO_INT(STAGE_INITIALISE)].bSelectable = FALSE			
			SkipMenuStruct[ENUM_TO_INT(STAGE_END_SCREEN)].bSelectable = FALSE
			
			SkipMenuStruct[ENUM_TO_INT(STAGE_INTRO_MOCAP)].sTxtLabel = "RBH_2A_INT"                      
			SkipMenuStruct[ENUM_TO_INT(STAGE_DEAL_WITH_FRANKLIN)].sTxtLabel = "Deal with Franklin"       
			SkipMenuStruct[ENUM_TO_INT(STAGE_GET_TO_BANK)].sTxtLabel = "Get to Bank"                     
			SkipMenuStruct[ENUM_TO_INT(STAGE_ROB_THE_BANK)].sTxtLabel = "Rob the Bank"                   
			SkipMenuStruct[ENUM_TO_INT(STAGE_HEIST_CUTSCENE)].sTxtLabel = "Heist Cut: RBH_2A_MCS_2_P7"      
			SkipMenuStruct[ENUM_TO_INT(STAGE_HOLD_OFF_COPS_AT_BANK)].sTxtLabel = "Hold off cops at bank" 
			SkipMenuStruct[ENUM_TO_INT(STAGE_CHOPPER_TURNS_UP)].sTxtLabel = "Chopper turns up"           
			SkipMenuStruct[ENUM_TO_INT(STAGE_ADVANCE_TO_GARDENS)].sTxtLabel = "Advance to gardens.."     
			SkipMenuStruct[ENUM_TO_INT(STAGE_CONTINUE_DOWN_STREET)].sTxtLabel = "Continue Down Street"   
			SkipMenuStruct[ENUM_TO_INT(STAGE_PROCEED_TO_ALLEY)].sTxtLabel = "Buddy dies/gets stuck: RBH_2AB_MCS_6 "              
			SkipMenuStruct[ENUM_TO_INT(STAGE_HOLD_OFF_ARMY_AT_JUNK_YARD)].sTxtLabel = "Hold off army in junk yard"               
			SkipMenuStruct[ENUM_TO_INT(STAGE_HOT_SWAP_TO_BULLDOZER)].sTxtLabel = "SWITCH to BULLDOZER"                   
			SkipMenuStruct[ENUM_TO_INT(STAGE_SWAPPED_TO_BULLDOZER)].sTxtLabel = "SWITCHED to BULLDOZER"                  
			SkipMenuStruct[ENUM_TO_INT(STAGE_RESCUE_BUDDYS)].sTxtLabel = "Rescue buddies: RBH_2A_MCS_4"                  
			SkipMenuStruct[ENUM_TO_INT(STAGE_CHICKEN_FACTORY)].sTxtLabel = "CHICKEN_FACTORY"                       
			SkipMenuStruct[ENUM_TO_INT(STAGE_TRAIN_PLATFORM)].sTxtLabel = "Jump on train: RBH_2A_MCS_5"                   
			SkipMenuStruct[ENUM_TO_INT(STAGE_END_MOCAP_SCENE)].sTxtLabel = "End scene: RBH_2A_EXT_1"                  
			SkipMenuStruct[ENUM_TO_INT(STAGE_ALL_DRIVE_OFF)].sTxtLabel = "All drive off"                       
		
			bDebugInitialised = TRUE
		ENDIF

		DONT_DO_J_SKIP(sLocatesData)	//Stop the locates header J skipping...

		IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, ENUM_TO_INT(mission_stage), TRUE, strMissionName)
			
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_FRANKLIN, FALSE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_MICHAEL, FALSE)
			SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_TREVOR, FALSE)
			
			DO_SCREEN_FADE_OUT(500)
						
			STOP_CUTSCENE()
			REMOVE_CUTSCENE()
			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
			
			WHILE IS_SCREEN_FADING_OUT()
				WAIT(0)
			ENDWHILE
	
			RESET_CREW_ANIMS()
			
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
				MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(pedSelector)
			ENDIF
			
			REMOVE_SCENARIO_BLOCKING_AREAS()
			
			//SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
			
			CLEAR_AREA(<<0.0, 0.0, 0.0>>, 6000.0, TRUE, FALSE)
			
			SET_GPS_MULTI_ROUTE_RENDER(FALSE)
			CLEAR_GPS_MULTI_ROUTE()
			//bCustomGPSActive = FALSE
			
			CLEAR_AREA(<<0.0, 0.0, 0.0>>, 5000.00, TRUE)
			SET_PLAYER_SPRINT(PLAYER_ID(), TRUE)
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			
			CLEANUP_ALL_ENTITIES()
			DELETE_PLAYERS_AND_CREW()
									
			SET_TIME_SCALE(1.0)	
								
			cleanUpHeistCutsceneAndTanker()
			CLEANUP_COPS_AND_CARS_OUTSIDE_BANK()
			CLEANUP_COPS_AND_CARS_DOWN_STREET()
			CLEANUP_ARMY_IN_JUNK_YARD()
			cleanUpHoldOffCopsAtUsedCarLot()
			cleanUpChickenFactory()
						
			//	Sub Stages
			
			iIntroMocapStage = 0
			iGetDealWithFranklin = 0
			iGetToBankStage = 0
			iControlBuddiesStage1 = 0
			iRobBankStage = 0
			iRobBankStageGunman = 0
			iHeistCutseneStage = 0
			iChopperStage = 0
			iStageAdvanceToGardens = 0

			iContinueDownStreetStage = 0

			iExplodeChopperSubstage = 0
			iProceedToAlleyStage = 0
			iShootingStageMichael = 0
			iShootingStageCrewMember = 0
			iShootingSwitchM = 0
			iShootingSwitchC = 0
			iJunkYardState = 0
			iDealWithChinooks = 0
			iStageSwitchToBullDozer = 0

			iBullDozerStage = 0
			iStageRescueBuddys = 0

			iStageChickenFactory = 0
			iChickenFactoryTrainState = 0
			
			iNumberOfSoldiersSpawned = 0
			eTrainState = TRAIN_PLATFORM_STATE_INIT
			
			eAllyAdvanceGardenState = ALLY_GARDEN_STATE_INIT
			
			iStageEndMocap = 0
//			irandomCarExplode1 = 0
//			irandomCarExplode2 = 0
			iSetpieceCopStage = 0
			iCopMegaPhoneDialogueStage = 0

			iCopRespawnIndex = 0
			iTyrePopIndex = 0
			iBodyCount = 0
			
			iGasStationExplosion = 0
			iGasStationExplosionTime = 0

			iMotelExplosion = 0
			
			iControlRamGunshots = 0
			
			iBalaclavaStage = 0
			
			iPedMichaelProgressStage1 = 0
			iPedMichaelProgressStage1Prev = -4
			ipedGunmanProgressStage1 = 0
			ipedGunmanProgressStage1Prev = -4

			REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipChickenFactory)
			REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipPier)

			NEW_LOAD_SCENE_STOP()
	
			SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID(), robbersGroup)
			
			SET_CUTSCENE_RUNNING(FALSE)
			CLEAR_TIMECYCLE_MODIFIER()
			
			CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
			CLEAR_PRINTS()
			CLEAR_HELP()
			KILL_ANY_CONVERSATION()
			CLEANUP_COPS_TURN_UP_CUTSCENE()
			CLEANUP_ADVANCE_TO_GARDEN()

			bTriggeredByDebugDoForceLoad = TRUE
			
			CLEANUP_CONTINUE_DOWN_STREET()
			
			bOnlyCreateArmyGuysOnce = FALSE
			
			//Set stage enum to required stage
			skip_mission_stage = INT_TO_ENUM(MISSION_STAGE_FLAG, iReturnStage)
			
			PRINTSTRING("RURAL BANK HEIST - Skipping to stage: ") PRINTINT(iReturnStage)PRINTNL()
			
			//FORCE_SWITCH_TO_MICHAEL() //Only switches if the player is franklin!
			
//			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
//				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
//					WAIT(0)
//				ENDWHILE
//			ENDIF


			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			
					
			INITIALISE_MISSION_COMMON(skip_mission_stage)
			
			SETUP_MISSION_2A()
			
			MANAGE_SKIP(skip_mission_stage)
			
			LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			
		ENDIF
			
		IF bRunDebugLocates
			vPlayersCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vPlayersCoords, vLocateDimensions, FALSE, FALSE, TM_ANY)
		
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
				SAVE_STRING_TO_DEBUG_FILE("IS_ENTITY_AT_COORD(PLAYER_PED_ID(), ")
				SAVE_VECTOR_TO_DEBUG_FILE(vPlayersCoords)
				SAVE_STRING_TO_DEBUG_FILE(", ")
				SAVE_VECTOR_TO_DEBUG_FILE(vLocateDimensions)
				SAVE_STRING_TO_DEBUG_FILE(", FALSE, TM_ANY_3D)")
			ENDIF
		
		ENDIF

		IF IS_KEYBOARD_KEY_PRESSED(KEY_S)		
			MISSION_PASSED()
		ENDIF
	
		IF IS_KEYBOARD_KEY_PRESSED(KEY_f)	
			bIsSuperDebugEnabled = TRUE
			Mission_Failed()
		ENDIF
		
//		IF IS_KEYBOARD_KEY_PRESSED(KEY_C)	
//			SET_CREW_IN_BALLISTIC_ARMOUR()
//		ENDIF
		
	ENDPROC

#ENDIF	

PROC CONTROL_DENSITIES()

	SWITCH mission_stage  

		CASE STAGE_TIME_LAPSE
		CASE STAGE_INTRO_MOCAP								
		CASE STAGE_ROB_THE_BANK						
		CASE STAGE_HEIST_CUTSCENE
		CASE STAGE_HOLD_OFF_COPS_AT_BANK
		CASE STAGE_CHOPPER_TURNS_UP
		CASE STAGE_ADVANCE_TO_GARDENS
		CASE STAGE_CONTINUE_DOWN_STREET
		CASE STAGE_PROCEED_TO_ALLEY
		CASE STAGE_HOLD_OFF_ARMY_AT_JUNK_YARD
		CASE STAGE_HOT_SWAP_TO_BULLDOZER
		CASE STAGE_SWAPPED_TO_BULLDOZER
		CASE STAGE_RESCUE_BUDDYS
		CASE STAGE_CHICKEN_FACTORY
		CASE STAGE_TRAIN_PLATFORM				
		CASE STAGE_END_MOCAP_SCENE						
		CASE STAGE_ALL_DRIVE_OFF	
			SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		BREAK
		
	ENDSWITCH
	
ENDPROC

// ____________________________________ MISSION SCRIPT _____________________________________

SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()	
		Mission_Flow_Mission_Force_Cleanup()
		MISSION_CLEANUP()   
	ENDIF
	
	// ____________________________________ MISSION LOOP _______________________________________
	
	SET_MISSION_FLAG(TRUE)
	
	//BREAK_ON_NATIVE_COMMAND("DISABLE_CONTROL_ACTION", FALSE)
		
	WHILE TRUE	
		
		//SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_TREVOR, FALSE)
		
		PICKUP_BUDDYS_BAG()
		
		CONTROL_DENSITIES()
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ThePaletoScoreFinale") // use GET_MISSION_TITLE?
		
		SWITCH mission_stage 
		
			CASE STAGE_TIME_LAPSE
				timeLapseCutscene()
			BREAK
		
			CASE STAGE_INITIALISE
				initialiseMission2A()			
			BREAK		
			
			CASE STAGE_INTRO_MOCAP
				stageIntroMocap()
			BREAK
			
			CASE STAGE_DEAL_WITH_FRANKLIN
				stageDealWithFranklin()
			BREAK
			
			CASE STAGE_GET_TO_BANK
				stageGetToBank()	
			BREAK
			
			CASE STAGE_ROB_THE_BANK
				stageRobTheBank()
			BREAK
			
			CASE STAGE_HEIST_CUTSCENE
				stageHeistCutscene()
				DISPLAY_TAKE(FALSE)
			BREAK
					
			CASE STAGE_HOLD_OFF_COPS_AT_BANK
				 stageHoldOffCopsAtBank()
				 DISABLE_JUMPING()
				 DISPLAY_TAKE()
				 DISABLE_MELEE_WEAPONS_IF_NOT_FRANKLIN()
			BREAK
			
			CASE STAGE_CHOPPER_TURNS_UP
				stageChopperTurnsUp()
				DISABLE_JUMPING()
				DISPLAY_TAKE()
				DISABLE_MELEE_WEAPONS_IF_NOT_FRANKLIN()
			BREAK
	
			CASE STAGE_ADVANCE_TO_GARDENS
				//Replay
				stageAdvanceToGardens()
				DISABLE_JUMPING()
				DISPLAY_TAKE()
				DISABLE_MELEE_WEAPONS_IF_NOT_FRANKLIN()
			BREAK
						
			CASE STAGE_CONTINUE_DOWN_STREET
				stageContinueDownStreet()
				DISABLE_JUMPING()
				DISPLAY_TAKE()
				DISABLE_MELEE_WEAPONS_IF_NOT_FRANKLIN()
			BREAK
			
			CASE STAGE_PROCEED_TO_ALLEY
				stageProceedToAlley()
				DISPLAY_TAKE()
				DISABLE_MELEE_WEAPONS_IF_NOT_FRANKLIN()
			BREAK
						
			CASE STAGE_HOLD_OFF_ARMY_AT_JUNK_YARD
				stageHoldOffArmyAtJunkYard()
				DISABLE_JUMPING()
				DISPLAY_TAKE()
				DISABLE_MELEE_WEAPONS_IF_NOT_FRANKLIN()
			BREAK
			
			CASE STAGE_HOT_SWAP_TO_BULLDOZER
				stageSwitchToBullDozer()
				DISABLE_JUMPING()
				DISPLAY_TAKE()
				DISABLE_MELEE_WEAPONS_IF_NOT_FRANKLIN()
				//Replay
			BREAK
			
			CASE STAGE_SWAPPED_TO_BULLDOZER
				stageSwappedToBulldozer()
				DISPLAY_TAKE()
				DISABLE_MELEE_WEAPONS_IF_NOT_FRANKLIN()
				//Replay
			BREAK
			
			CASE STAGE_RESCUE_BUDDYS
				stageRescueBuddys()
				DISPLAY_TAKE()
				DISABLE_MELEE_WEAPONS_IF_NOT_FRANKLIN()
			BREAK
			
			CASE STAGE_CHICKEN_FACTORY
				stageChickenFactory()
				DISABLE_JUMPING()
				DISPLAY_TAKE()
				DISABLE_MELEE_WEAPONS_IF_NOT_FRANKLIN()
				//Replay
			BREAK
						
			CASE STAGE_TRAIN_PLATFORM	//2A only
				stageTrainPlatform()
				DISABLE_JUMPING()	//Deal with switchiing to Franklin.
				IF NOT IS_CUTSCENE_ACTIVE()
					DISPLAY_TAKE()
				ENDIF
				DISABLE_MELEE_WEAPONS_IF_NOT_FRANKLIN()
			BREAK
			
			CASE STAGE_END_MOCAP_SCENE
				stageEndMocapScene()
			BREAK
					
			CASE STAGE_END_SCREEN
				stageEndScreen()			
			BREAK
						
			DEFAULT
				SCRIPT_ASSERT("Unrecognised Mission Stage!")
			BREAK
			
		ENDSWITCH	

		#IF IS_DEBUG_BUILD
			debugProcedures()
		#ENDIF
		
		WAIT(0)
		
	ENDWHILE
	
	//	should never reach here - always ends by going through the cleanup function

ENDSCRIPT					



