USING "commands_script.sch"
USING "commands_camera.sch"
USING "script_maths.sch"
USING "rural_bank_heist_support.sch"


ENUM TREVOR_INTIMIDATION_STATE
	TI_INTRO,
	TI_LOOP1,
	TI_LOOP2,
	TI_TRANSITION,
	TI_END_LOOP,
	TI_DONE
ENDENUM
TREVOR_INTIMIDATION_STATE	eTrevorIntimidationState = TI_INTRO


STRING	strIntimidationDictionary = "missheistpaletoscore2mcs_2_pt1"

PED_INDEX pedsBankers[5]

SEQUENCE_INDEX	taskSeqBank

INT iTimeOflAstShooting
INT iPedIterator
BOOL bHasPedbeenMurdered[5]
//BOOL bHasCommentedOnMurder[5]
INT iUpdateWhimpers
INT iTimeOfLastWhimper
INT iRandomPedToWhimper
INT iTimeOflastScreaming



//bool haveSetScriptCams = FALSE

INT iBurstInBankSyncedSceneTrevor
INT iBurstInBankSyncedSceneTrevorDoors
INT iBurstInBankSyncedSceneGunman
INT iExitVanpeds

//OBJECT_INDEX oiStoreDoorTempCount
OBJECT_INDEX oiStoreDoorTempVault

OBJECT_INDEX oiBagGunman
OBJECT_INDEX oiBagMichael

//OBJECT_INDEX oiSafe01
//OBJECT_INDEX oiSafe02
//OBJECT_INDEX oiSafe03
//OBJECT_INDEX oiSafe04
//OBJECT_INDEX oiSafe05
//OBJECT_INDEX oiSafe06

OBJECT_INDEX oiBlowTorch
OBJECT_INDEX oiWeldingMask
//OBJECT_INDEX oiBagGunman

VEHICLE_INDEX vehCopCarOutFront

PED_INDEX pedSheriff_cover
PED_INDEX pedSheriff_CarDoor

PTFX_ID ptfxTorch
INT iSoundTorch

VECTOR vBankRobStartTrevor
VECTOR vBankRobStartTrevorRot
VECTOR vBankRobStartGunman
VECTOR vBankRobStartGunmanRot

//BOOL bToldToBackUp

BOOL bTrevorReady = FALSE
BOOL bGunmanReady = FALSE

BOOL bGunmanLooping = FALSE
BOOL bTrevorLooping = FALSE

BOOL bTrevorPlayIntimidationAnims = FALSE
structTimer tmrRobbery

BOOL	bRunSwitch = FALSE
INT		iRobShootingDialogueState
structTimer	tmrShootingDialogue

FUNC BOOL ARE_ALL_BANKERS_DEAD()
	
	INT i 
	FOR i = 0 TO COUNT_OF(pedsBankers) - 1
		IF IS_ENTITY_OK(pedsBankers[i])
			RETURN FALSE
		ENDIF
	ENDFOR
	
	RETURN TRUE
	
ENDFUNC

PROC UPDATE_SHOOTING_DIALOGUE()
	SWITCH iRobShootingDialogueState
		CASE 0 
			RESTART_TIMER_NOW(tmrShootingDialogue)
			iRobShootingDialogueState = 1
		BREAK
		
		CASE 1
			IF GET_TIMER_IN_SECONDS(tmrShootingDialogue) >= 5.0
				iRobShootingDialogueState = 2
			ENDIF
		BREAK
		
		CASE 2
			IF IS_ENTITY_OK(PLAYER_PED_ID())
				IF IS_PED_SHOOTING(PLAYER_PED_ID())
					iRobShootingDialogueState = 3
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT ARE_ALL_BANKERS_DEAD()
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_ROBMCLM", CONV_PRIORITY_VERY_HIGH)
						iRobShootingDialogueState = 0
					ENDIF
				ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_ROBTCLM", CONV_PRIORITY_VERY_HIGH)
						iRobShootingDialogueState = 0
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC



PROC UPDATE_WHIMPERS()

	SWITCH iUpdateWhimpers
	
		CASE 0
			FOR iPedIterator = 0 To 5 - 1
				bHasPedbeenMurdered[iPedIterator] = FALSE
			ENDFOR
			iUpdateWhimpers++
		BREAK
		
		CASE 1
		
			iRandomPedToWhimper = GET_RANDOM_INT_IN_RANGE(0, 5)
		
			IF NOT IS_PED_INJURED(pedsBankers[iRandomPedToWhimper])
					
					IF (GET_GAME_TIMER() - iTimeOflAstShooting) > 4000
						IF (GET_GAME_TIMER() - iTimeOfLastWhimper) > 750
						
							IF IS_PED_MALE(pedsBankers[iRandomPedToWhimper])
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedsBankers[iRandomPedToWhimper], "WHIMPER", "WAVELOAD_PAIN_MALE") //, SPEECH_PARAMS_FORCE)	
							ELSE
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedsBankers[iRandomPedToWhimper], "WHIMPER", "WAVELOAD_PAIN_FEMALE") //, SPEECH_PARAMS_FORCE)	
							ENDIF
							iTimeOfLastWhimper = GET_GAME_TIMER()
						ENDIF
					ELSE
						IF (GET_GAME_TIMER() - iTimeOflastScreaming) > 250
							
							IF GET_RANDOM_BOOL()
								//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedsBankers[iRandomPedToWhimper], "SCREAM_PANIC", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE)	
								PLAY_PAIN(pedsBankers[iRandomPedToWhimper], AUD_DAMAGE_REASON_SCREAM_PANIC)
							ELSE
								//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedsBankers[iRandomPedToWhimper], "SCREAM_SHOCKED", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE)	
								PLAY_PAIN(pedsBankers[iRandomPedToWhimper], AUD_DAMAGE_REASON_SCREAM_TERROR)								
							ENDIF
							
							iTimeOflastScreaming = GET_GAME_TIMER()
						ENDIF
					ENDIF
					
				
				iUpdateWhimpers++
			ENDIF
			
		BREAK
		
		CASE 2
			IF (GET_GAME_TIMER() - iTimeOfLastWhimper) > 500
				iUpdateWhimpers = 1
			ENDIF
		BREAK
	
	ENDSWITCH
			
	
	IF IS_PED_SHOOTING(PLAYER_PED_ID())
		//IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
			IF (GET_GAME_TIMER() - iTimeOflAstShooting) > 2000
				
				//RunConversation("JH_SHOOTING", TRUE, TRUE, TRUE, CONV_PRIORITY_VERY_HIGH)
				iTimeOflAstShooting = GET_GAME_TIMER()
				
			ENDIF
		//ENDIF
	ENDIF
	
	//IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
		FOR iPedIterator = 0 TO 5 -1
			//IF iPedIterator <> 4	//Dont include the sec guard
				IF NOT bHasPedbeenMurdered[iPedIterator]
					IF DOES_ENTITY_EXIST(pedsBankers[iPedIterator])
						IF IS_ENTITY_DEAD(pedsBankers[iPedIterator])
							PRINTLN("pedsBankers: ", iPedIterator)
							
							IF NOT IS_PED_INJURED(pedGunman)
								IF IS_GUNMAN_GUSTAVO()
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedGunman, "RBH2A_AYAA", "GUSTAVO", SPEECH_PARAMS_FORCE_FRONTEND)
								ELIF IS_GUNMAN_DARYL()
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedGunman, "RBH2A_AZAA", "DARYL", SPEECH_PARAMS_FORCE_FRONTEND)
								ELIF IS_GUNMAN_CHEF()
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedGunman, "RBH2A_CZAA", "CHEF", SPEECH_PARAMS_FORCE_FRONTEND)
								ELIF IS_GUNMAN_NORM()
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedGunman, "RBH2A_DAAA", "NORM", SPEECH_PARAMS_FORCE_FRONTEND)
								ELIF IS_GUNMAN_PACKIE()
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedGunman, "RBH2A_MVAA", "PACKIE", SPEECH_PARAMS_FORCE_FRONTEND)
								ENDIF
							ENDIF
							
							//RunConversation("JH_KILLREACT", TRUE, TRUE, TRUE, CONV_PRIORITY_VERY_HIGH)
							bHasPedbeenMurdered[iPedIterator] = TRUE
						ENDIF
					ENDIF
//				ELSE
//					IF NOT bHasCommentedOnMurder[iPedIterator]
//						SWITCH iPedIterator
//							CASE 0
//								ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 5, pedsBankers[iPedIterator], "JewelSales")
//								//RunConversation("JH_CWER3", TRUE, TRUE, TRUE, CONV_PRIORITY_VERY_HIGH)	
//							BREAK
//							CASE 1
//								ADD_PED_FOR_DIALOGUE(myScriptedSpeech, ConvertSingleCharacter("B"), pedsBankers[iPedIterator], "JewelShop0")
//								//RunConversation("JH_CWER1", TRUE, TRUE, TRUE, CONV_PRIORITY_VERY_HIGH)
//							BREAK
//							CASE 2
//								ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 5, pedsBankers[iPedIterator], "JewelSales")
//								//RunConversation("JH_CWER3", TRUE, TRUE, TRUE, CONV_PRIORITY_VERY_HIGH)	
//							BREAK
//							
//							CASE 3
//								ADD_PED_FOR_DIALOGUE(myScriptedSpeech, ConvertSingleCharacter("C"), pedsBankers[iPedIterator], "JewelShop2")
//								//RunConversation("JH_CWER2", TRUE, TRUE, TRUE, CONV_PRIORITY_VERY_HIGH)	
//							BREAK
//							
//							CASE 5
//								ADD_PED_FOR_DIALOGUE(myScriptedSpeech, ConvertSingleCharacter("B"), pedsBankers[iPedIterator], "JewelShop2")
//								//RunConversation("JH_CWER1", TRUE, TRUE, TRUE, CONV_PRIORITY_VERY_HIGH)
//							BREAK
//							
//						ENDSWITCH
//						bHasCommentedOnMurder[iPedIterator] = TRUE
//					ENDIF
				ENDIF
			//ENDIF
		ENDFOR
	//ENDIF
	
ENDPROC

INT iKickVaultReminderTimer
BOOL bPrintedKickingHelpText = FALSE

PROC UPDATE_GUNMAN_ROB_BANK()
	//Gunman Control
	SWITCH iRobBankStageGunman
	
		CASE 0
			//IF iRobBankStage = 8
////				IF IS_SYNCHRONIZED_SCENE_RUNNING(iBurstInBankSyncedSceneGunman)
////					IF GET_SYNCHRONIZED_SCENE_PHASE(iBurstInBankSyncedSceneGunman) > 0.68
//						OPEN_LEFT_BANK_DOOR()
////						IF DOES_ENTITY_EXIST(oiBankDoorLeft)
////							
////							REMOVE_MODEL_HIDE(<<-111.5, 6463.9, 31.99>>, 10.0, V_ilev_bank4door02, FALSE)	
////							DELETE_OBJECT(oiBankDoorLeft)
////						ENDIF
////					ENDIF
////				ENDIF
//			ENDIF
			
			IF iRobBankStage > 8
			AND (iRobBankStage != 50 AND iRobBankStage != 51 AND iRobBankStage != 52 AND iRobBankStage != 53)
				
				iRobBankStageGunman++
			ENDIF
		BREAK
		
		CASE 1
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iBurstInBankSyncedSceneGunman)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iBurstInBankSyncedSceneGunman) >= 0.99	
					SETTIMERB(0)
					iRobBankStageGunman = 2
				ENDIF
			ELSE
				SETTIMERB(0)
				iRobBankStageGunman = 2
			ENDIF
		
		BREAK
		
		CASE 2
			IF TIMERB() > 1000
			
				STOP_SYNCHRONIZED_ENTITY_ANIM(pedGunman, SLOW_BLEND_OUT, TRUE)
				//CLEAR_PED_TASKS_IMMEDIATELY(pedGunman)
				OPEN_SEQUENCE_TASK(taskSeqBank)
					TASK_AIM_GUN_AT_ENTITY(NULL, pedsBankers[4], 1500)
					//TASK_AIM_GUN_AT_COORD(NULL, (<<-102.51502, 6470.93848, 32.13235>>), 1000)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-105.5042, 6471.9707, 30.6267>>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)	
				CLOSE_SEQUENCE_TASK(taskSeqBank)
				TASK_PERFORM_SEQUENCE(pedGunman, taskSeqBank)
				CLEAR_SEQUENCE_TASK(taskSeqBank)
				
				iRobBankStageGunman = 3
			ENDIF
		BREAK
		
		CASE 3
		
			IF IS_ENTITY_AT_COORD(pedGunman, <<-105.5042, 6471.9707, 30.6267>>, <<1.0, 1.0, 1.5>>)
				iBurstInBankSyncedSceneGunman = CREATE_SYNCHRONIZED_SCENE(<<-105.414, 6474.930, 30.630>>, <<-00, 0.0, -45.0>>)
				TASK_SYNCHRONIZED_SCENE(pedGunman, iBurstInBankSyncedSceneGunman, "missheistpaletoscore2mcs_2_p5", "burn_door_intro_gunman", SLOW_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, NORMAL_BLEND_IN)
				oiBlowTorch = CREATE_OBJECT(prop_tool_blowtorch, <<-111.5, 6463.9, 31.99>>)
				ATTACH_ENTITY_TO_ENTITY(oiBlowTorch, pedGunman, GET_PED_BONE_INDEX(pedGunman, BONETAG_PH_L_HAND), vTorchPosition, vTorchRotation)
				PLAY_ENTITY_ANIM(oiWeldingMask, "burn_door_intro_Mask", "missheistpaletoscore2mcs_2_p5", INSTANT_BLEND_IN, FALSE, TRUE)	
				SET_SYNCHRONIZED_SCENE_LOOPED(iBurstInBankSyncedSceneGunman, FALSE)	
				SET_CURRENT_PED_WEAPON(pedGunman, WEAPONTYPE_UNARMED, TRUE)
				iRobBankStageGunman++
			ENDIF
		BREAK
		
		CASE 4
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iBurstInBankSyncedSceneGunman)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iBurstInBankSyncedSceneGunman) >= 1.0
					iBurstInBankSyncedSceneGunman = CREATE_SYNCHRONIZED_SCENE(<<-105.414, 6474.930, 30.630>>, <<-00, 0.0, -45.0>>)
					TASK_SYNCHRONIZED_SCENE(pedGunman, iBurstInBankSyncedSceneGunman, "missheistpaletoscore2mcs_2_p5", "burn_door_loop_gunman", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					PLAY_ENTITY_ANIM(oiWeldingMask, "burn_door_loop_Mask", "missheistpaletoscore2mcs_2_p5", INSTANT_BLEND_IN, TRUE, FALSE)	
				
					ptfxTorch = START_PARTICLE_FX_LOOPED_ON_ENTITY("cs_paleto_blowtorch", oiBlowTorch, vTorchEffectPosition, vTorchEffectRotation)  
			
					SET_SYNCHRONIZED_SCENE_LOOPED(iBurstInBankSyncedSceneGunman, TRUE)	
					
					iSoundTorch = GET_SOUND_ID()
					
					PLAY_SOUND_FROM_ENTITY(iSoundTorch, "PS2A_WELDTORCH_MASTER", oiBlowTorch)
					
					
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), oiBlowTorch, 30000)//,  SLF_ )
						
					bPrintedKickingHelpText = FALSE
					SETTIMERA(0)
					
					REQUEST_CUTSCENE("RBH_2A_MCS_2_P3")
					REQUEST_ACTION_MODE_ASSET("TREVOR_ACTION")
					RESTART_TIMER_NOW(tmrRobbery)
					iRobBankStageGunman++
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			//IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-105.5042, 6471.9707, 30.6267>>, <<3.7, 3.7, 1.5>>)
			IF GET_TIMER_IN_SECONDS(tmrRobbery) >= 7.0
				//IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_ROB2", CONV_PRIORITY_VERY_HIGH)
				IF CREATE_GUNMAN_CONVERSATION("RBH_ROB2", "RBH_ROB2_DJ", "RBH_ROB2_CH", "RBH_ROB2_NR", "RBH_ROB2_PM")
					iRobBankStageGunman++		
				ENDIF	
			ENDIF	
		BREAK
		
		CASE 6
		CASE 7
			IF NOT bVaultObjectiveDisplayed
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND IS_PLAYER_MICHAEL()		
					IF NOT DOES_BLIP_EXIST(blipSafe)
						blipSafe = CREATE_BLIP_FOR_COORD(<<-105.29419, 6473.09521, 31.48736>>)
					ENDIF
					PRINT_NOW("FETCHCREW", DEFAULT_GOD_TEXT_TIME, 1)
					bVaultObjectiveDisplayed = TRUE
				ENDIF
			ELSE
			
				IF GET_GAME_TIMER() - iKickVaultReminderTimer > 7000
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_GUNMAN_CONVERSATION("RBH_AGAIN_GM", "RBH_AGAIN_DJ", "RBH_AGAIN_CH", "RBH_AGAIN_NR", "RBH_AGAIN_PM")
						iKickVaultReminderTimer = GET_GAME_TIMER()
					ENDIF	
				ENDIF	
			
			ENDIF
			
			//Print Help text..
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-104.4680, 6472.2070, 30.6267>>, <<2.1, 2.1, 1.8>>)
				AND NOT IS_ENTITY_AT_COORD(pedTrevor(), <<-104.4680, 6472.2070, 30.6267>>, <<3.1, 3.1, 1.8>>)
				AND IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(PLAYER_PED_ID()), 40.0, 180.0)
				
				//AND GET_ENTITY_HEADING(PLAYER_PED_ID()) > 06.0//16.8881 
				//AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < 66.0//56.8881 
					
					IF bPrintedKickingHelpText = FALSE
						//IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-104.4680, 6472.2070, 30.6267>>, <<1.7, 1.7, 1.5>>, FALSE)
							//PRINT_HELP("KICKDOOR")
							bPrintedKickingHelpText = TRUE
						ENDIF
					ENDIF
					
				//	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)//INPUT_FRONTEND_LB)				//Are they pressing the smash button?
					//IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
					IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						bRunSwitch = FALSE
						iBurstInBankSyncedSceneGunman = CREATE_SYNCHRONIZED_SCENE(<<-105.414, 6474.930, 30.630>>, <<-00, 0.0, -45.0>>)
						TASK_SYNCHRONIZED_SCENE(pedGunman, iBurstInBankSyncedSceneGunman, "missheistpaletoscore2mcs_2_p5", "burn_door_gunman", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iBurstInBankSyncedSceneGunman, "missheistpaletoscore2mcs_2_p5", "burn_door_player0", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
					
						SET_SYNCHRONIZED_SCENE_LOOPED(iBurstInBankSyncedSceneGunman, FALSE)	
						
						CPRINTLN(DEBUG_MISSION, "1339066 - Before playing burn_door_VaultGate anims")
						PLAY_SYNCHRONIZED_ENTITY_ANIM(oiStoreDoorTempVault, iBurstInBankSyncedSceneGunman, "burn_door_VaultGate", "missheistpaletoscore2mcs_2_p5", INSTANT_BLEND_IN)
						CPRINTLN(DEBUG_MISSION, "1339066 - After playing burn_door_VaultGate anims")
						PLAY_ENTITY_ANIM(oiWeldingMask, "burn_door_Mask", "missheistpaletoscore2mcs_2_p5", INSTANT_BLEND_IN, FALSE, TRUE)			
						
						
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(), TRUE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGunman, TRUE)
						
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiStoreDoorTempVault)
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiWeldingMask)
						
						REMOVE_MODEL_HIDE(<<-111.4800, 6463.9399, 31.9850>>, 5.0, V_ilev_bank4door02, FALSE)
						SET_MODEL_AS_NO_LONGER_NEEDED(V_ilev_bank4door02)

						REMOVE_MODEL_HIDE(<<-109.6500, 6462.1099, 31.9850>>, 5.0, V_ilev_bank4door01, FALSE)
						SET_MODEL_AS_NO_LONGER_NEEDED(V_ilev_bank4door01)
						
						IF DOES_ENTITY_EXIST(oiBankDoorLeft)
							STOP_SYNCHRONIZED_ENTITY_ANIM(oiBankDoorLeft, INSTANT_BLEND_OUT, TRUE)
							DELETE_OBJECT(oiBankDoorLeft)
						ENDIF
						
						IF DOES_ENTITY_EXIST(oiBankDoorRight)
							STOP_SYNCHRONIZED_ENTITY_ANIM(oiBankDoorRight, INSTANT_BLEND_OUT, TRUE)
							DELETE_OBJECT(oiBankDoorRight)							
						ENDIF
						
						SET_CAM_COORD(initialCam,<<-106.468536,6475.628418,31.617807>>)
						SET_CAM_ROT(initialCam,<<3.753865,-4.805548,-157.816940>>)
						SET_CAM_FOV(initialCam,19.333998)
			
						SET_CAM_COORD(destinationCam,<<-106.468536,6475.628418,31.617807>>)
						SET_CAM_ROT(destinationCam,<<3.753865,-4.805548,-157.816940>>)
						SET_CAM_FOV(destinationCam,22.333998)
						
						SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 3000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
						
						SHAKE_CAM(destinationCam, "HAND_SHAKE", 1.5)
											
						CLEAR_PED_TASKS(pedTrevor())
						
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
						
						CLEAR_AREA_OF_PROJECTILES(<<-106.468536,6475.628418,31.617807>>, 3000.0)
						
						SET_CUTSCENE_RUNNING(TRUE)
						CLEAR_HELP()
						CLEAR_PRINTS()
						STOP_AUDIO_SCENE("PS_2A_ENTER_BANK")
						START_AUDIO_SCENE("PS_2A_KICK_DOOR_OPEN")	
						iRobBankStageGunman = 8
					ENDIF
				ELSE
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
					bPrintedKickingHelpText = FALSE
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			//IF PREPARE_MUSIC_EVENT("RH2A_ENTER_GATE")
				CLEAR_HELP()
				TRIGGER_MUSIC_EVENT("RH2A_ENTER_GATE")
				
				REPLAY_RECORD_BACK_FOR_TIME(1.5)
				
				iRobBankStageGunman++
			//ENDIF
		BREAK
		
		CASE 9
			
			IF NOT HAS_SOUND_FINISHED(iSoundTorch)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iBurstInBankSyncedSceneGunman)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iBurstInBankSyncedSceneGunman) >= 0.1
						STOP_PARTICLE_FX_LOOPED(ptfxTorch)
						STOP_SOUND(iSoundTorch)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iBurstInBankSyncedSceneGunman)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iBurstInBankSyncedSceneGunman) >= 0.2
					
					SET_ENTITY_COORDS(pedTrevor(), <<-111.4351, 6464.3940, 30.6267>>)
					TASK_PUT_PED_DIRECTLY_INTO_COVER(pedTrevor(), <<-111.4351, 6464.3940, 30.6267>>, 4000)
					iRobBankStageGunman++
				ENDIF
			ENDIF
		BREAK
		
		
		CASE 10
			iRobBankStageGunman++
		BREAK
			
		CASE 11
			IF HAS_CUTSCENE_LOADED()			
			AND HAS_ACTION_MODE_ASSET_LOADED("TREVOR_ACTION")
				
				CLEAR_PED_TASKS_IMMEDIATELY(pedTrevor())
				
				REGISTER_ENTITY_FOR_CUTSCENE(vehCopCarOutFront, "police_car", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, SHERIFF)
			
				REGISTER_ENTITY_FOR_CUTSCENE(pedSheriff_cover, "Sheriff_cover", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, mnCopPedModel)
				REGISTER_ENTITY_FOR_CUTSCENE(pedSheriff_CarDoor, "Sheriff_CarDoor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, mnCopPedModel)
			
				SET_SRL_POST_CUTSCENE_CAMERA(<<-113.536072,6465.524902,32.348522>>, NORMALISE_VECTOR(<<-7.999923,0.000000,-77.397736>> - <<-7.999923,0.000000,-77.397736>>))
						
				IF NOT IS_ENTITY_DEAD(pedTrevor())
					REGISTER_ENTITY_FOR_CUTSCENE(pedTrevor(), "TREVOR", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF				
//				IF NOT IS_ENTITY_DEAD(pedGunman)
//					REGISTER_ENTITY_FOR_CUTSCENE(pedGunman, "gunman_selection_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF				
//				IF NOT IS_ENTITY_DEAD(PedMichael())
//					REGISTER_ENTITY_FOR_CUTSCENE(PedMichael(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedMichael(), TRUE)
//				ENDIF
				
				SET_CURRENT_PED_WEAPON(PedMichael(), WEAPONTYPE_UNARMED, TRUE)
				
				
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("PS2A_Enter_Bank")
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("PS2A_Enter_Bank_2")
				
				STOP_AUDIO_SCENE("PS_2A_KICK_DOOR_OPEN")	
								
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				//CLEAR_AREA(<<-114.3841, 6458.6694, 30.4685>>, 20.0, TRUE)
				//1825097
				CLEAR_AREA_OF_PROJECTILES(<<-114.3841, 6458.6694, 30.4685>>, 20.0)
				CLEAR_AREA_OF_VEHICLES(<<-114.3841, 6458.6694, 30.4685>>, 20.0)
				CLEAR_AREA_OF_PEDS(<<-114.3841, 6458.6694, 30.4685>>, 20.0)
				
				DELETE_VEHICLE(arrivalBurrito)
				REMOVE_BLIP(blipSafe)
				//OPEN_BANK_DOORS()			

				iRobBankStageGunman++
			ENDIF
		
		BREAK
		
		CASE 12
		CASE 13
		CASE 14
		
			IF IS_CUTSCENE_PLAYING()
			AND iRobBankStageGunman = 12
				DELETE_OBJECT(oiBlowTorch)
				SET_MODEL_AS_NO_LONGER_NEEDED(prop_tool_blowtorch)
		
				DELETE_OBJECT(oiWeldingMask)
				SET_MODEL_AS_NO_LONGER_NEEDED(Prop_Welding_Mask_01_S)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
							
				iRobBankStageGunman = 13
			ENDIF
		
//			IF IS_CUTSCENE_PLAYING()
//				IF NOT IS_ENTITY_DEAD(pedSelector.pedID[SELECTOR_PED_TREVOR])
//					SET_ENTITY_COORDS(pedSelector.pedID[SELECTOR_PED_TREVOR], <<-111.5747, 6466.3730, 30.6267>>)
//				ENDIF
//				IF NOT IS_ENTITY_DEAD(pedSelector.pedID[SELECTOR_PED_MICHAEL])
//					SET_ENTITY_COORDS(pedSelector.pedID[SELECTOR_PED_MICHAEL], <<-105.414, 6474.930, 30.630>>)
//				ENDIF
//			ENDIF
		
			SET_CLOCK_TIME(16, 0, 0)
		
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("police_car"))
				vehCopCarOutFront = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("police_car"))
			ENDIF	
			
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Sheriff_cover"))
				pedSheriff_cover = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Sheriff_cover"))
			ENDIF	
			
			IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Sheriff_CarDoor"))
				pedSheriff_CarDoor = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Sheriff_CarDoor"))
			ENDIF	
		
			IF GET_CUTSCENE_TIME() > 59000.00
			AND iRobBankStageGunman < 13
				//IF PREPARE_MUSIC_EVENT("RH2A_TREV_DOOR")
					TRIGGER_MUSIC_EVENT("RH2A_TREV_DOOR")
					iRobBankStageGunman = 14
				//ENDIF
			ENDIF
			
			IF WAS_CUTSCENE_SKIPPED()
				IF iCurrentTake < 4865324	
					iCurrentTake = 4865324
				ENDIF
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Sheriff_cover")
				IF NOT IS_ENTITY_DEAD(pedSheriff_cover)
					SET_PED_AS_DEFENSIVE_COP(pedSheriff_cover, WEAPONTYPE_PISTOL, TRUE, FALSE, 2.1, FALSE, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSheriff_cover, TRUE)
					SET_PED_SPHERE_DEFENSIVE_AREA(pedSheriff_cover,<<-123.3953, 6445.9507, 30.5228>>, 5.0)
					//TASK_SEEK_COVER_FROM_POS(pedSheriff_cover, <<-108.5078, 6464.6729, 30.6267>>, 5000)
					TASK_AIM_GUN_AT_ENTITY(pedSheriff_cover, PLAYER_PED_ID(), INFINITE_TASK_TIME, TRUE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSheriff_cover, TRUE)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Sheriff_CarDoor")
				IF NOT IS_ENTITY_DEAD(pedSheriff_CarDoor)
					SET_PED_AS_DEFENSIVE_COP(pedSheriff_CarDoor, WEAPONTYPE_PISTOL, TRUE, FALSE, 2.1, FALSE, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSheriff_CarDoor, TRUE)
					SET_PED_SPHERE_DEFENSIVE_AREA(pedSheriff_CarDoor, GET_ENTITY_COORDS(pedSheriff_CarDoor), 5.0)
					TASK_AIM_GUN_AT_ENTITY(pedSheriff_CarDoor, PLAYER_PED_ID(), INFINITE_TASK_TIME, TRUE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSheriff_CarDoor, TRUE)
					//TASK_SEEK_COVER_FROM_POS(pedSheriff_CarDoor, <<-108.5078, 6464.6729, 30.6267>>, 5000)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_TREVOR
					MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_TREVOR)
					TAKE_CONTROL_OF_SELECTOR_PED(pedSelector)
				ENDIF
				STOP_CURRENT_PLAYING_AMBIENT_SPEECH(PLAYER_PED_ID())
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				//
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-111.5747, 6466.3730, 30.6267>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 282.6058)
				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				IF NOT IS_ENTITY_DEAD(pedSelector.pedID[SELECTOR_PED_MICHAEL])
					FREEZE_ENTITY_POSITION(pedSelector.pedID[SELECTOR_PED_MICHAEL], FALSE)
					SET_ENTITY_COORDS(pedSelector.pedID[SELECTOR_PED_MICHAEL], <<-105.414, 6474.930, 30.630>>)
					STOP_CURRENT_PLAYING_AMBIENT_SPEECH(pedSelector.pedID[SELECTOR_PED_MICHAEL])
				ENDIF
			ENDIF
			
			//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
//				IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_TREVOR
//					MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_TREVOR)
//					TAKE_CONTROL_OF_SELECTOR_PED(pedSelector)
//				ENDIF
				
				REPLAY_STOP_EVENT()
				
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, pedMichael(), "MICHAEL")
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, PLAYER_PED_ID(), "TREVOR")
								
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-111.5747, 6466.3730, 30.6267>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 282.6058)
				//TASK_PUT_PED_DIRECTLY_INTO_COVER(PLAYER_PED_ID(), <<-111.4351, 6464.3940, 30.6267>>, 4000)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_SAWNOFFSHOTGUN, TRUE)
				
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE, -1, "TREVOR_ACTION")
				
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ACTIONMODE_IDLE, TRUE, FAUS_CUTSCENE_EXIT)
				
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedMichael(), INFINITE_TASK_TIME)
				
				REMOVE_ANIM_DICT("missheistpaletoscore2mcs_2_p5")
				REQUEST_ANIM_DICT("missheistpaletoscore2mcs_2_p6")
				
				iBurstInBankSyncedSceneTrevor = CREATE_SYNCHRONIZED_SCENE(<<-109.622, 6467.932, 30.712>>, <<-00, 0.0, -45.0>>)
				CPRINTLN(DEBUG_MISSION, "1339066 - Before playing end_loop_player2_bag anims")
				PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBagTrevor, iBurstInBankSyncedSceneTrevor, "end_loop_player2_bag", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
				CPRINTLN(DEBUG_MISSION, "1339066 - After playing end_loop_player2_bag anims")
				
				IF NOT IS_PED_INJURED(pedsBankers[0])
					TASK_SYNCHRONIZED_SCENE(pedsBankers[0], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "end_loop_F", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				IF NOT IS_PED_INJURED(pedsBankers[1])
					TASK_SYNCHRONIZED_SCENE(pedsBankers[1], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "end_loop_M1", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				IF NOT IS_PED_INJURED(pedsBankers[2])
					TASK_SYNCHRONIZED_SCENE(pedsBankers[2], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "end_loop_M2", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				IF NOT IS_PED_INJURED(pedsBankers[3])
					TASK_SYNCHRONIZED_SCENE(pedsBankers[3], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "end_loop_M3", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				IF NOT IS_PED_INJURED(pedsBankers[4])
					TASK_SYNCHRONIZED_SCENE(pedsBankers[4], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "end_loop_M4", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				SET_SYNCHRONIZED_SCENE_LOOPED(iBurstInBankSyncedSceneTrevor, TRUE)
		
				SET_CURRENT_PED_WEAPON(PedMichael(), WEAPONTYPE_UNARMED, TRUE)
				
				IF DOES_ENTITY_EXIST(oiBankDoorRight)
					REMOVE_MODEL_HIDE(<<-109.65, 6462.11, 31.99>>, 10.0, V_ilev_bank4door01, FALSE)
					DELETE_OBJECT(oiBankDoorRight)
					SET_MODEL_AS_NO_LONGER_NEEDED(V_ilev_bank4door01)
				ENDIF
				IF DOES_ENTITY_EXIST(oiBankDoorLeft)
					REMOVE_MODEL_HIDE(<<-111.5, 6463.9, 31.99>>, 10.0, V_ilev_bank4door02, FALSE)	
					DELETE_OBJECT(oiBankDoorLeft)
					SET_MODEL_AS_NO_LONGER_NEEDED(V_ilev_bank4door02)
				ENDIF
				
				CLOSE_BANK_DOORS(FALSE)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				SET_CUTSCENE_RUNNING(FALSE)
				iRobBankStageGunman = 15
			ENDIF
		BREAK
		
		CASE 15
			//Go down and see the dudes
			START_AUDIO_SCENE("PS_2A_GO_TO_SAFE")
			SETUP_WANTED_LEVEL(5, TRUE, 100.0, 0.5, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
			PRINT_NOW("FETCHCREW", DEFAULT_GOD_TEXT_TIME, 1)
			blipSafe = CREATE_BLIP_FOR_COORD(<<-106.7429, 6474.9019, 30.6267>>)
			DO_SWITCH_EFFECT(CHAR_TREVOR)
			REQUEST_HEIST_CUTSCENE_ASSETS()
			
			iRobBankStageGunman++
		BREAK
		
		CASE 16

			REQUEST_ANIM_DICT("missheistpaletoscore2mcs_2_p6")
			
			IF HAS_ANIM_DICT_LOADED("missheistpaletoscore2mcs_2_p6")
				
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedGunman, FALSE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedMichael(), FALSE)
				
				iBurstInBankSyncedSceneGunman = CREATE_SYNCHRONIZED_SCENE(<<-105.414, 6474.930, 30.630>>, <<-00, 0.0, -45.0>>)
				TASK_SYNCHRONIZED_SCENE(pedGunman, iBurstInBankSyncedSceneGunman, "missheistpaletoscore2mcs_2_p6", "Empty_Safe_Loop_gunman", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
				TASK_SYNCHRONIZED_SCENE(pedMichael(), iBurstInBankSyncedSceneGunman, "missheistpaletoscore2mcs_2_p6", "Empty_Safe_Loop_Player0", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
				
				oiBagGunman = CREATE_OBJECT(P_LD_HEIST_BAG_S_1, <<-105.414, 6474.930, 30.630>>)
				oiBagMichael = CREATE_OBJECT(P_LD_HEIST_BAG_S_1, <<-105.414, 6474.930, 33.630>>)
				
				PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBagGunman, iBurstInBankSyncedSceneGunman, "Empty_Safe_Loop_gunman_bag", "missheistpaletoscore2mcs_2_p6", INSTANT_BLEND_IN)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBagMichael, iBurstInBankSyncedSceneGunman, "Empty_Safe_Loop_Player0_bag", "missheistpaletoscore2mcs_2_p6", INSTANT_BLEND_IN)
	
				//Bag remove
				SET_PED_COMPONENT_VARIATION(pedGunman, INT_TO_ENUM(PED_COMPONENT,6), 1, 0, 0) //(feet)
				SET_PED_COMPONENT_VARIATION(pedGunman, INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
				
				//Bag remove
				SET_PED_COMPONENT_VARIATION(pedSelector.pedID[SELECTOR_PED_MICHAEL], INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
							
				SET_SYNCHRONIZED_SCENE_LOOPED(iBurstInBankSyncedSceneGunman, FALSE)	
				
				iRobBankStageGunman++	
			ENDIF
		BREAK
		
		CASE 17
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2222078
			
			IF DOES_ENTITY_EXIST(pedSheriff_CarDoor)
			AND TIMERB() > 9000
			AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)	
				IF NOT IS_ENTITY_DEAD(pedSheriff_CarDoor)
					
					IF GET_RANDOM_INT_IN_RANGE(0, 10) >= 2
						ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 7, pedSheriff_CarDoor, "Sherrif1")
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "RBH2AUD", "RBH_LOUDSP2", CONV_PRIORITY_HIGH)						
					ELSE
						ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 7, pedSheriff_CarDoor, "LOUDCOP2")
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "RBH2AUD", "RBH_LOUD", CONV_PRIORITY_HIGH)						
					ENDIF
					SETTIMERB(0)
				ENDIF
			ENDIF
			//1st locate by vaults. 2nd Locate out side - force cutscene to stop player fuckign with shit
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-106.227066,6467.968262,30.626730>>, <<-101.953873,6471.887207,33.145638>>, 4.750000)
				iRobBankStageGunman = 18
			ENDIF
			
			//Get rid of safe blip.
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-106.7429, 6474.9019, 30.6267>>, <<3.0, 3.0 ,3.0>>)
				IF DOES_BLIP_EXIST(blipSafe)
					REMOVE_BLIP(blipSafe)
				ENDIF
			ENDIF

			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-112.725021,6461.234375,29.218456>>, <<-143.549149,6432.169922,43.779541>>, 49.250000)
			OR IS_BULLET_IN_ANGLED_AREA(<<-112.725021,6461.234375,29.218456>>, <<-143.549149,6432.169922,43.779541>>, 49.250000)
			OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-112.725021,6461.234375,29.218456>>, <<-143.549149,6432.169922,43.779541>>, 49.250000, WEAPONTYPE_GRENADE)
			OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-112.725021,6461.234375,29.218456>>, <<-143.549149,6432.169922,43.779541>>, 49.250000, WEAPONTYPE_STICKYBOMB)
			OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-112.725021,6461.234375,29.218456>>, <<-143.549149,6432.169922,43.779541>>, 49.250000, WEAPONTYPE_RPG)
			OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-112.725021,6461.234375,29.218456>>, <<-143.549149,6432.169922,43.779541>>, 49.250000, WEAPONTYPE_GRENADELAUNCHER)
				iRobBankStageGunman = 21
			ENDIF
			
		BREAK

		CASE 18
			iBurstInBankSyncedSceneGunman = CREATE_SYNCHRONIZED_SCENE(<<-105.414, 6474.930, 30.630>>, <<-00, 0.0, -45.0>>)
			TASK_SYNCHRONIZED_SCENE(pedGunman, iBurstInBankSyncedSceneGunman, "missheistpaletoscore2mcs_2_p6", "Empty_Safe_Exit_Gunman", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
			TASK_SYNCHRONIZED_SCENE(pedMichael(), iBurstInBankSyncedSceneGunman, "missheistpaletoscore2mcs_2_p6", "Empty_Safe_Exit_PLayer0", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
			
			PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBagGunman, iBurstInBankSyncedSceneGunman, "Empty_Safe_Exit_Gunman_bag", "missheistpaletoscore2mcs_2_p6", INSTANT_BLEND_IN)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBagMichael, iBurstInBankSyncedSceneGunman, "Empty_Safe_Exit_PLayer0_bag", "missheistpaletoscore2mcs_2_p6", INSTANT_BLEND_IN)
			
			REQUEST_HEIST_CUTSCENE_ASSETS()
			
			SET_SYNCHRONIZED_SCENE_LOOPED(iBurstInBankSyncedSceneGunman, FALSE)	
			iRobBankStageGunman++	
		BREAK
		
		CASE 19
			IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_ROB3", CONV_PRIORITY_VERY_HIGH)
				iRobBankStageGunman++		
			ENDIF	
		BREAK
		
		CASE 20
			//Play loop then break to second part of mocap
			//Once intro anims stop
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iBurstInBankSyncedSceneGunman)
				IF GET_SYNCHRONIZED_SCENE_PHASE(iBurstInBankSyncedSceneGunman) >= 0.92
				OR (NOT IS_SCRIPTED_CONVERSATION_ONGOING())
					iRobBankStageGunman++								
				ENDIF
			ENDIF
		BREAK
		
		CASE 21
			REMOVE_BLIP(blipSafe)
						
			iCurrentTake = RURAL_HEIST_TAKE //8000000
	
			TASK_CLEAR_LOOK_AT(pedMichael())
			
			SET_MODEL_AS_NO_LONGER_NEEDED(S_F_Y_AIRHOSTESS_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_BUSINESS_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(P_LD_HEIST_BAG_S_1)
			SET_MODEL_AS_NO_LONGER_NEEDED(V_ilev_cbankvaulgate01)
			
			DELETE_OBJECT(oiBagTrevor)
			DELETE_ARRAY_OF_PEDS(pedsBankers, TRUE)
			
			REMOVE_ANIM_DICT("missheistpaletoscore2mcs_2_pt1")
			REMOVE_ANIM_DICT("missheistpaletoscore2mcs_2_p5")
			REMOVE_ANIM_DICT("missheistpaletoscore2mcs_2_p6")
			REMOVE_ANIM_DICT("missheistpaletoscore2ig_8_p2")
			REMOVE_ANIM_DICT("missheistpaletoscore2ig_8")
			REMOVE_VEHICLE_ASSET(BURRITO3)
//					NEW_LOAD_SCENE_STOP()
			STOP_AUDIO_SCENE("PS_2A_GO_TO_SAFE")					
			mission_stage = STAGE_HEIST_CUTSCENE
		BREAK
		
	ENDSWITCH
ENDPROC

INT iIntimidationTimer
INT iIntimidationInterval = 5000
BOOL bContinueIntimidate

BOOL bCommentOnDeadBankers = FALSE

PROC UPDATE_TREVOR_INTIMIDATION()
	IF IS_ENTITY_OK(pedTrevor())
	
		SET_PED_RESET_FLAG(pedTrevor(), PRF_DisablePotentialBlastReactions, TRUE)
		SET_PED_RESET_FLAG(pedTrevor(), PRF_DisableVehicleDamageReactions, TRUE)
	
	
		IF GET_GAME_TIMER() - iIntimidationTimer > iIntimidationInterval
		
			iIntimidationInterval = GET_RANDOM_INT_IN_RANGE(5000, 8000)
		
		
			IF NOT ARE_ALL_BANKERS_DEAD()
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				AND NOT IS_MESSAGE_BEING_DISPLAYED()
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_PACE2", CONV_PRIORITY_MEDIUM)
						iIntimidationTimer = GET_GAME_TIMER() 
					ENDIF
				ELSE
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedTrevor(), "RBH2A_MCAA", "TREVOR", SPEECH_PARAMS_FORCE_FRONTEND)	
					iIntimidationTimer = GET_GAME_TIMER() 
				ENDIF
			ELSE
			
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				AND NOT IS_MESSAGE_BEING_DISPLAYED()
					IF bCommentOnDeadBankers = FALSE
						IF IS_PLAYER_TREVOR()
							IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_DEAD_T", CONV_PRIORITY_MEDIUM)
								bCommentOnDeadBankers = TRUE
							ENDIF						
						ELSE
							IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_DEAD_M", CONV_PRIORITY_MEDIUM)
								bCommentOnDeadBankers = TRUE
							ENDIF						
						ENDIF
					ENDIF
				ENDIF
			
				IF bCommentOnDeadBankers = TRUE
			
					IF GET_RANDOM_BOOL()
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						AND NOT IS_MESSAGE_BEING_DISPLAYED()
							IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_PACE1", CONV_PRIORITY_MEDIUM)
								iIntimidationTimer = GET_GAME_TIMER() 
							ENDIF	
						ELSE
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedTrevor(), "RBH2A_MBAA", "TREVOR", SPEECH_PARAMS_FORCE_FRONTEND)	
							iIntimidationTimer = GET_GAME_TIMER() 
						ENDIF
					ELSE
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						AND NOT IS_MESSAGE_BEING_DISPLAYED()
							IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_PACE3", CONV_PRIORITY_MEDIUM)
								iIntimidationTimer = GET_GAME_TIMER() 
							ENDIF
						ELSE
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedTrevor(), "RBH2A_MDAA", "TREVOR", SPEECH_PARAMS_FORCE_FRONTEND)	
							iIntimidationTimer = GET_GAME_TIMER() 
						ENDIF
					ENDIF
				
				ENDIF
			
			ENDIF
		ENDIF
	
		IF NOT IS_PLAYER_TREVOR()
		AND iSwitchStage = 0
		AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedTrevor(), SCRIPT_TASK_PERFORM_SEQUENCE)
			SWITCH eTrevorIntimidationState
				CASE TI_INTRO
					// Play 
					TASK_PLAY_ANIM(pedTrevor(), strIntimidationDictionary, "intimidate_intro_trevor", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME)
					CPRINTLN(DEBUG_MISSION, "UPDATE_TREVOR_INTIMIDATION - going to TI_LOOP1")
					bContinueIntimidate = FALSE
					eTrevorIntimidationState = TI_LOOP1
				BREAK
				
				CASE TI_LOOP1
										
					//IF GET_SCRIPT_TASK_STATUS(pedTrevor(), SCRIPT_TASK_PLAY_ANIM) = FINISHED_TASK
					
					IF IS_ENTITY_PLAYING_ANIM(pedTrevor(), strIntimidationDictionary, "intimidate_intro_trevor")
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedTrevor(),strIntimidationDictionary, "intimidate_intro_trevor") > 0.999
							bContinueIntimidate = TRUE
						ENDIF
					ENDIF
					
					IF IS_ENTITY_PLAYING_ANIM(pedTrevor(), strIntimidationDictionary, "intimidate_loop_2_trevor")
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedTrevor(),strIntimidationDictionary, "intimidate_loop_2_trevor") > 0.999
							bContinueIntimidate = TRUE
						ENDIF
					ENDIF
					
					IF bContinueIntimidate = TRUE
						TASK_PLAY_ANIM(pedTrevor(), strIntimidationDictionary, "intimidate_loop_1_trevor", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS| AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME)
						CPRINTLN(DEBUG_MISSION, "UPDATE_TREVOR_INTIMIDATION - going to TI_LOOP2")
						eTrevorIntimidationState = TI_LOOP2
					ENDIF
					
					IF ARE_ALL_BANKERS_DEAD()
						CPRINTLN(DEBUG_MISSION, "UPDATE_TREVOR_INTIMIDATION - In LOOP1, but everyone's dead, going to TI_TRANSITION")
						bContinueIntimidate = FALSE
						eTrevorIntimidationState = TI_TRANSITION
					ENDIF
					
				BREAK
				
				CASE TI_LOOP2
					IF ARE_ALL_BANKERS_DEAD()
						CPRINTLN(DEBUG_MISSION, "UPDATE_TREVOR_INTIMIDATION - In LOOP2, but everyone's dead, going to TI_TRANSITION")
									
						eTrevorIntimidationState = TI_TRANSITION
					ENDIF
					//IF GET_SCRIPT_TASK_STATUS(pedTrevor(), SCRIPT_TASK_PLAY_ANIM) = FINISHED_TASK
					IF GET_SCRIPT_TASK_STATUS(pedTrevor(), SCRIPT_TASK_PLAY_ANIM) = PERFORMING_TASK
					AND GET_ENTITY_ANIM_CURRENT_TIME(pedTrevor(),strIntimidationDictionary, "intimidate_loop_1_trevor") > 0.999
						TASK_PLAY_ANIM(pedTrevor(), strIntimidationDictionary, "intimidate_loop_2_trevor", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS| AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME)
						CPRINTLN(DEBUG_MISSION, "UPDATE_TREVOR_INTIMIDATION - going to TI_LOOP1")
						bContinueIntimidate = FALSE
						eTrevorIntimidationState = TI_LOOP1
					ENDIF
				BREAK
				
				CASE TI_TRANSITION
				
					IF IS_ENTITY_PLAYING_ANIM(pedTrevor(), strIntimidationDictionary, "intimidate_loop_1_trevor") 
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedTrevor(),strIntimidationDictionary, "intimidate_loop_1_trevor") > 0.999
							bContinueIntimidate = TRUE
						ENDIF
					ENDIF
					
					IF IS_ENTITY_PLAYING_ANIM(pedTrevor(), strIntimidationDictionary, "intimidate_loop_2_trevor")
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedTrevor(),strIntimidationDictionary, "intimidate_loop_2_trevor") > 0.999
							bContinueIntimidate = TRUE
						ENDIF
					ENDIF
				
					IF bContinueIntimidate
						TASK_PLAY_ANIM(pedTrevor(), strIntimidationDictionary, "intimidate_transition_trevor", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS| AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME)
						CPRINTLN(DEBUG_MISSION, "UPDATE_TREVOR_INTIMIDATION - going to TI_END_LOOP")
						eTrevorIntimidationState = TI_END_LOOP
					ENDIF
				BREAK
				
				CASE TI_END_LOOP
					//IF GET_SCRIPT_TASK_STATUS(pedTrevor(), SCRIPT_TASK_PLAY_ANIM) = FINISHED_TASK
					IF IS_ENTITY_PLAYING_ANIM(pedTrevor(), strIntimidationDictionary, "intimidate_transition_trevor")
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedTrevor(),strIntimidationDictionary, "intimidate_transition_trevor") > 0.999
					
							TASK_PLAY_ANIM(pedTrevor(), strIntimidationDictionary, "intimidate_end_loop_trevor", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1,  AF_USE_KINEMATIC_PHYSICS| AF_USE_MOVER_EXTRACTION | AF_HOLD_LAST_FRAME)
							CPRINTLN(DEBUG_MISSION, "UPDATE_TREVOR_INTIMIDATION - going to TI_DONE")
						
							eTrevorIntimidationState = TI_DONE
						ENDIF
					ENDIF
				BREAK
				
				CASE TI_DONE
				BREAK
			ENDSWITCH
		
		ENDIF
		
	ENDIF
ENDPROC

FUNC BOOL IS_ENTITY_AT_BANK_ENTRANCE(ENTITY_INDEX thisEntity)

	RETURN IS_ENTITY_IN_ANGLED_AREA(thisEntity, <<-116.575645,6463.428223,30.218456>>, <<-109.751030,6456.450195,33.818710>>, 7.500000)

ENDFUNC



BOOL bActionModeSet = FALSE
BOOL bLetsDoThisSaid = FALSE
BOOL bDoorAnimStoppedEarly = FALSE
BOOL bRandomlySelectTorM
VEHICLE_INDEX vehicleForBeingADick
INT iNavBlockObject

PROC stageRobTheBank()

	INT i
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 debugPedName
	#ENDIF
	
	doBuddyFailCheck(2.0)
	
//	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORNAME_PALETO_BANK_TELLER))
	IF iRobBankStage > 0
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-114.719948,6474.139160,30.651968>>, <<-109.286812,6468.747559,33.392658>>, 1.500000)
//			PRINTLN("@@@@@@@@@@ DOOR_SYSTEM_SET_DOOR_STATE: UNLOCK @@@@@@@@@@")
//			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORNAME_PALETO_BANK_TELLER), DOORSTATE_UNLOCKED)
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_CBANKCOUNTDOOR01, <<-108.9147, 6469.1045, 31.9103>>, FALSE, 0.0)
		ELSE
//			PRINTLN("@@@@@@@@@@ DOOR_SYSTEM_SET_DOOR_STATE: LOCK @@@@@@@@@@")
//			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORNAME_PALETO_BANK_TELLER), DOORSTATE_LOCKED)
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_CBANKCOUNTDOOR01, <<-108.9147, 6469.1045, 31.9103>>, TRUE, 0.0)
		ENDIF
	ENDIF
//	ENDIF
	
	IF iRobBankStageGunman >= 5
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-97.442924,6456.161133,39.251194>>, <<-121.488907,6479.176758,30.202539>>, 34.000000)
		AND IS_PLAYER_CONTROL_ON(PLAYER_ID())
			MISSION_FLOW_SET_FAIL_REASON("ALLCREWLEFT")
			mission_failed()
		ENDIF
	ELSE
		IF GET_DISTANCE_BETWEEN_COORDS(vBankCoordinates, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100.0
			MISSION_FLOW_SET_FAIL_REASON("ABANDONHEIST")
			mission_failed()
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(arrivalBurrito)
		IF NOT IS_VEHICLE_DRIVEABLE(arrivalBurrito)
		OR IS_VEHICLE_PERMANENTLY_STUCK(arrivalBurrito)
			MISSION_FLOW_SET_FAIL_REASON("VANDIED")
			Mission_Failed()
		ENDIF
	ENDIF

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		vehicleForBeingADick = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(vehicleForBeingADick)
			IF NOT IS_ENTITY_DEAD(vehicleForBeingADick)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehicleForBeingADick)
				AND GET_ENTITY_SPEED(vehicleForBeingADick) > 0.4
				AND NOT IS_PED_IN_VEHICLE(pedGunman, vehicleForBeingADick)
				AND NOT IS_PED_IN_VEHICLE(pedTrevor(), vehicleForBeingADick)
					IF IS_ENTITY_TOUCHING_ENTITY(vehicleForBeingADick, pedGunman)
						SET_ENTITY_HEALTH(pedGunman, 0)
					ENDIF
					IF IS_ENTITY_TOUCHING_ENTITY(vehicleForBeingADick, pedTrevor())
						SET_ENTITY_HEALTH(pedTrevor(), 0)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehicleForBeingADick)
	ENDIF

	IF iRobBankStage < 9
		IF GET_DISTANCE_BETWEEN_COORDS(vBankCoordinates, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 60.0
			IF IS_PED_SHOOTING(PLAYER_PED_ID())
                weapon_type players_weapon
                IF get_current_ped_weapon(player_ped_id(), players_weapon)            
                    IF GET_WEAPONTYPE_GROUP(players_weapon) != WEAPONGROUP_THROWN

						MISSION_FLOW_SET_FAIL_REASON("BLEWHEIST")	
						Mission_Failed()		
					ENDIF
				ENDIF
			ENDIF
			IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_BZGAS, GET_ENTITY_COORDS(PLAYER_PED_ID()), 60.0)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADE, GET_ENTITY_COORDS(PLAYER_PED_ID()), 60.0)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADELAUNCHER, GET_ENTITY_COORDS(PLAYER_PED_ID()), 60.0)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_FLAME, GET_ENTITY_COORDS(PLAYER_PED_ID()), 60.0)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_FLAME_EXPLODE, GET_ENTITY_COORDS(PLAYER_PED_ID()), 60.0)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DIR_GAS_CANISTER, GET_ENTITY_COORDS(PLAYER_PED_ID()), 60.0)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_ROCKET, GET_ENTITY_COORDS(PLAYER_PED_ID()), 60.0)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_MOLOTOV, GET_ENTITY_COORDS(PLAYER_PED_ID()), 60.0)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_STICKYBOMB, GET_ENTITY_COORDS(PLAYER_PED_ID()), 60.0)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_SMOKE_GRENADE, GET_ENTITY_COORDS(PLAYER_PED_ID()), 60.0)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_SMOKE_GRENADE_LAUNCHER, GET_ENTITY_COORDS(PLAYER_PED_ID()), 60.0)
						
				MISSION_FLOW_SET_FAIL_REASON("BLEWHEIST")	
				Mission_Failed()	
			ENDIF
		ENDIF	
	ENDIF	

	IF DOES_ENTITY_EXIST(pedSelector.pedID[SELECTOR_PED_TREVOR])
		SET_PED_CONFIG_FLAG(pedSelector.pedID[SELECTOR_PED_TREVOR], PCF_RunFromFiresAndExplosions, FALSE)
		SET_PED_CONFIG_FLAG(pedSelector.pedID[SELECTOR_PED_TREVOR], PCF_DisableExplosionReactions, TRUE)
		SET_PED_RESET_FLAG(pedSelector.pedID[SELECTOR_PED_TREVOR], PRF_DisablePotentialBlastReactions, TRUE)
	ENDIF
	SET_PED_CONFIG_FLAG(pedGunman, PCF_RunFromFiresAndExplosions, FALSE)
	SET_PED_CONFIG_FLAG(pedGunman, PCF_DisableExplosionReactions, TRUE)
	SET_PED_RESET_FLAG(pedGunman, PRF_DisablePotentialBlastReactions, TRUE)
	
	IF NOT IS_PED_INJURED(pedsBankers[2])
		SET_PED_RESET_FLAG(pedsBankers[2], PRF_DisablePotentialBlastReactions, TRUE)
	ENDIF
	IF NOT IS_PED_INJURED(pedsBankers[3])
		SET_PED_RESET_FLAG(pedsBankers[3], PRF_DisablePotentialBlastReactions, TRUE)
	ENDIF
	IF NOT IS_PED_INJURED(pedsBankers[4])
		SET_PED_RESET_FLAG(pedsBankers[4], PRF_DisablePotentialBlastReactions, TRUE)
	ENDIF
	
	//SEQUENCE_INDEX seqGetInPosOutsideBank
	
	IF iRobBankStageGunman > 11
		INCREMENT_TAKE()
	ENDIF
	
	IF iRobBankStageGunman > 11
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS ( <<-106.47, 6476.16, 31.95>>, 2.0, V_ILEV_CBANKVAULGATE02)
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_CBANKVAULGATE02, <<-106.47, 6476.16, 31.95>>, TRUE, -1.0)
		ENDIF
		DISPLAY_TAKE(FALSE)
	ENDIF
	
	UPDATE_GUNMAN_ROB_BANK()
		
	IF iRobBankStage > 12
		UPDATE_WHIMPERS()
	ENDIF
	
	IF iRobBankStage < 14
		//Slow player to a walk and disable jump only when close to the bank entrance
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-109.622, 6467.932, 30.712>>) < 15.5
		
			IF bActionModeSet = FALSE
			//AND NOT IS_PED_USING_ACTION_MODE(PLAYER_PED_ID())
			AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<-109.622, 6467.932, 30.712>>) < 10.5
				SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
				bActionModeSet = TRUE
			ENDIF
			
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		ENDIF
	ENDIF
	
	FOR i = 0 TO 4
		IF NOT IS_PED_INJURED(pedsBankers[i])
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedsBankers[i], PLAYER_PED_ID())
				APPLY_DAMAGE_TO_PED(pedsBankers[i], 100, TRUE)
			ENDIF
		ENDIF
	ENDFOR
	
	IF DOES_ENTITY_EXIST(oiWeldingMask)
		ATTACH_ENTITY_TO_ENTITY(oiWeldingMask, pedGunman, GET_PED_BONE_INDEX(pedGunman, BONETAG_HEAD), <<0.1, 0.01, 0.0>>, vMaskRotation)
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiBlowTorch)
		ATTACH_ENTITY_TO_ENTITY(oiBlowTorch, pedGunman, GET_PED_BONE_INDEX(pedGunman, BONETAG_PH_L_HAND), vTorchPosition, vTorchRotation)
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxTorch)
		SET_PARTICLE_FX_LOOPED_OFFSETS(ptfxTorch, vTorchEffectPosition, vTorchEffectRotation)
	ENDIF
	
	IF iRobBankStageGunman >= 13//15
		CLOSE_BANK_DOORS(TRUE)
	ENDIF
	
	IF bRunSwitch
		UPDATE_SHOOTING_DIALOGUE()
		PROCESS_MICHAEL_TREVOR_SWITCH()
	ENDIF
	
	IF bTrevorPlayIntimidationAnims
	AND NOT IS_CUTSCENE_PLAYING()
	AND iRobBankStageGunman < 7
		UPDATE_TREVOR_INTIMIDATION()
	ENDIF
	
	PROCESS_BALACLAVAS()
	
	//ptfxTorch
	SWITCH iRobBankStage
		
		CASE 0
				
			bActionModeSet = FALSE
			bTrevorReady = FALSE
			bGunmanReady = FALSE
			bVaultObjectiveDisplayed = FALSE
			bRunSwitch = FALSE
			bTrevorPlayIntimidationAnims = FALSE
			iRobShootingDialogueState = 0
			eTrevorIntimidationState = TI_INTRO
			
			CLOSE_BANK_DOORS()
			
			iNavBlockObject = ADD_NAVMESH_BLOCKING_OBJECT(<<-119.6544, 6466.6704, 30.5684>>, <<2.0, 2.0, 2.0>>, 0.0)
					
			SET_ENTITY_LOD_DIST(pedGunman, 200)
			SET_ENTITY_LOD_DIST(pedTrevor(), 200)
		
			CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_EYES)
			CLEAR_PED_PROP(pedTrevor(), ANCHOR_EYES)
			CLEAR_PED_PROP(pedGunman, ANCHOR_EYES)
			
			CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
			CLEAR_PED_PROP(pedTrevor(), ANCHOR_HEAD)
			CLEAR_PED_PROP(pedGunman, ANCHOR_HEAD)
		
			DELETE_VEHICLE(franklinsBoat)
			SET_MODEL_AS_NO_LONGER_NEEDED(SQUALO)
			
			REMOVE_CUTSCENE()
			REMOVE_WAYPOINT_RECORDING("RBHFRANKBOAT")
			
		
			REQUEST_WEAPON_ASSET(WEAPONTYPE_MINIGUN)
			REQUEST_WEAPON_ASSET(pedGunmanWeaponType)
			
			REQUEST_MODEL(S_F_Y_AIRHOSTESS_01)
			REQUEST_MODEL(A_M_M_BUSINESS_01)
			REQUEST_MODEL(P_LD_HEIST_BAG_S_1)
			REQUEST_MODEL(p_csh_strap_01_s)			
			REQUEST_MODEL(Prop_Welding_Mask_01_S)
			REQUEST_MODEL(prop_tool_blowtorch)
						 
			REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, 4)
			DELETE_PED(pedSelector.pedID[SELECTOR_PED_FRANKLIN])
			
			KILL_ANY_CONVERSATION()
			
			bankInterior = GET_INTERIOR_AT_COORDS(<< -110.6969, 6463.8066, 30.6343 >>)
			
			PIN_INTERIOR_IN_MEMORY(bankInterior)
			
			//sLocatesData.LocationBlip = CREATE_BLIP_FOR_COORD(vBankParkingLocation, FALSE)
					
			WHILE NOT setupHeistCutscene1()	//Creates tanker...
				PRINTSTRING("waiting 1")
				WAIT(0)
			ENDWHILE
			
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
			bDoorAnimStoppedEarly = FALSE
			iCurrentTake = 0
			
			//NEW_LOAD_SCENE_START_SPHERE(<<-107.9933, 6466.3350, 30.6267>>, 0.5)
					
			REQUEST_ANIM_DICT("missheistpaletoscore2mcs_2_pt1")
			REQUEST_ANIM_DICT("missheistpaletoscore2mcs_2_p5")
			REQUEST_ANIM_DICT("missheistpaletoscore2ig_8")
			REQUEST_ANIM_DICT("missheistpaletoscore2ig_8_p2")
			
			REQUEST_VEHICLE_ASSET(BURRITO3)
			REQUEST_MODEL(Prop_Welding_Mask_01_S)
			REQUEST_MODEL(V_ilev_bank4door01)
			REQUEST_MODEL(V_ilev_bank4door02)
			REQUEST_MODEL(V_ilev_cbankcountdoor01)
			REQUEST_MODEL(V_ilev_cbankvaulgate01)
			
			bCommentOnDeadBankers = FALSE
			
			IF NOT IS_ENTITY_DEAD(pedTrevor())
				GIVE_WEAPON_TO_PED(pedTrevor(), WEAPONTYPE_SAWNOFFSHOTGUN, 150, TRUE, TRUE)
			ENDIF
			IF NOT IS_ENTITY_DEAD(pedGunman)
				GIVE_WEAPON_TO_PED(pedGunman, pedGunmanWeaponType, INFINITE_AMMO, TRUE, TRUE)
			ENDIF
			FADE_IN_IF_NEEDED()
			START_AUDIO_SCENE("PS_2A_REVERSE_TO_ENTRANCE")
			//tell them Back van up into position.
			//PRINT_NOW("BACKUPVAN", DEFAULT_GOD_TEXT_TIME, 1)
						
			iRobBankStage++
		BREAK
	
		
		CASE 1
			IF HAS_ANIM_DICT_LOADED("missheistpaletoscore2mcs_2_pt1")
			AND HAS_ANIM_DICT_LOADED("missheistpaletoscore2mcs_2_p5")
			AND HAS_ANIM_DICT_LOADED("missheistpaletoscore2ig_8")
			AND HAS_ANIM_DICT_LOADED("missheistpaletoscore2ig_8_p2")
			AND HAS_MODEL_LOADED(Prop_Welding_Mask_01_S)
			AND HAS_MODEL_LOADED(V_ilev_bank4door01)
			AND HAS_MODEL_LOADED(V_ilev_bank4door02)
			AND HAS_MODEL_LOADED(V_ilev_cbankcountdoor01)
			AND HAS_MODEL_LOADED(V_ilev_cbankvaulgate01)
			
			AND HAS_MODEL_LOADED(p_csh_strap_01_s)
			AND HAS_MODEL_LOADED(S_F_Y_AIRHOSTESS_01)
			AND HAS_MODEL_LOADED(A_M_M_BUSINESS_01)
			AND REQUEST_AMBIENT_AUDIO_BANK("PS2A_Enter_Bank")
			AND REQUEST_AMBIENT_AUDIO_BANK("PS2A_Enter_Bank_2")
				IF PREPARE_MUSIC_EVENT("RH2A_MISSION_START")
					TRIGGER_MUSIC_EVENT("RH2A_MISSION_START")
	 
//					IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(DOORHASH_RURAL_BANK_F_L)
//						ADD_DOOR_TO_SYSTEM(DOORHASH_RURAL_BANK_F_L, V_ilev_bank4door01, <<-109.6500, 6462.1099, 31.9850>>)
//					ENDIF
//					IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(DOORHASH_RURAL_BANK_F_R)
//						ADD_DOOR_TO_SYSTEM(DOORHASH_RURAL_BANK_F_R, V_ilev_bank4door02, <<-111.4800, 6463.9399, 31.9850>>)
//					ENDIF
				
					IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L))
    		            DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L), 1.0, FALSE, FALSE)
		                DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_L), DOORSTATE_LOCKED, FALSE, TRUE)
            		ENDIF
            		IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R))
            			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R), -1.0, FALSE, FALSE)
                		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_RURAL_BANK_F_R), DOORSTATE_LOCKED, FALSE, TRUE)
            		ENDIF
					
					IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORNAME_PALETO_BANK_TELLER))
						DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORNAME_PALETO_BANK_TELLER), DOORSTATE_LOCKED)
					ENDIF
										
					CREATE_MODEL_HIDE(<<-109.6500, 6462.1099, 31.9850>>, 5.0, V_ilev_bank4door01, FALSE)
					CREATE_MODEL_HIDE(<<-111.4800, 6463.9399, 31.9850>>, 5.0, V_ilev_bank4door02, FALSE)	
					//CREATE_MODEL_HIDE(<<-108.9147, 6469.1045, 31.9103>>, 5.0, V_ilev_cbankcountdoor01, FALSE)
					iRobBankStage++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
		
			//Guys burst out and into the bank...
			iBurstInBankSyncedSceneTrevor = CREATE_SYNCHRONIZED_SCENE(<<-109.622, 6467.932, 30.712>>, <<-00, 0.0, -45.0>>)
			
			SET_SYNCHRONIZED_SCENE_LOOPED(iBurstInBankSyncedSceneTrevor, TRUE)
			
			DELETE_OBJECT(oiBankDoorRight)
			DELETE_OBJECT(oiBankDoorLeft)
			//DELETE_OBJECT(oiStoreDoorTempCount)
			oiBankDoorRight = CREATE_OBJECT(V_ilev_bank4door01, <<-109.6500, 6462.1099, 31.9850>>)
			oiBankDoorLeft = CREATE_OBJECT(V_ilev_bank4door02, <<-111.4800, 6463.9399, 31.9850>>)
//			oiStoreDoorTempCount = CREATE_OBJECT(V_ilev_cbankcountdoor01, <<-108.9147, 6469.1045, 31.9103>>)
			
			PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBankDoorRight, iBurstInBankSyncedSceneTrevor, "start_loop_door_r", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
			PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBankDoorLeft, iBurstInBankSyncedSceneTrevor, "start_loop_door_l", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
			//PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-108.9147, 6469.1045, 31.9103>>, 1.0, V_ILEV_CBANKCOUNTDOOR01, iBurstInBankSyncedSceneTrevor, "start_loop_CountDoor", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
			
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiBankDoorLeft)
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiBankDoorRight)
//			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiStoreDoorTempCount)
			
			oiWeldingMask = CREATE_OBJECT(Prop_Welding_Mask_01_S, <<-105.414, 6474.930, 30.630>>)
						
//			bToldToBackUp = FALSE
			
			iRobBankStage++
		
		BREAK
		
		CASE 3
			//check van is in position and heading.
		
			IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData, vBankParkingLocation - <<0.0, 0.0, 1.8>>, <<0.001, 0.001, 0.001>>, TRUE, pedTrevor(), pedGunman, NULL, arrivalBurrito , "PARKVAN", "LEFTREVOR", "", "", "", "GETINVAN", "GETBACKIN", FALSE, TRUE)
		
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)		
				IF IS_ENTITY_AT_COORD(arrivalBurrito, vBankParkingLocation, <<4.25, 4.25, 3.0>>, FALSE)
				
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "STAGE_ROB_THE_BANK")
				
					IF GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATMG) < 1000
						GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_COMBATMG, 1000 - GET_AMMO_IN_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_COMBATMG), TRUE, TRUE)
					ENDIF
					
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
				
					//vBankRobStartTrevor = <<-111.7142, 6460.5825, 31.5074>>//<<-111.6276, 6460.5522, 31.5068>>//GET_ANIM_INITIAL_OFFSET_POSITION("missheistpaletoscore2mcs_2_pt1", "start_loop_player2", <<-109.622, 6467.932, 30.712>>, <<-00, 0.0, -45.0>>)
					//vBankRobStartGunman = <<-112.8210, 6461.3936, 31.4957>>//<<-112.8210, 6461.3936, 31.4957>>//GET_ANIM_INITIAL_OFFSET_POSITION("missheistpaletoscore2mcs_2_pt1", "start_loop_gunman", <<-109.622, 6467.932, 30.712>>, <<-00, 0.0, -45.0>>)
					
					vBankRobStartTrevor = GET_ANIM_INITIAL_OFFSET_POSITION("missheistpaletoscore2ig_8_p2", "Walk_In_Bank_Intro_Player2", <<-109.622, 6467.932, 30.712>>, <<-00, 0.0, -45.0>>)
					vBankRobStartGunman = GET_ANIM_INITIAL_OFFSET_POSITION("missheistpaletoscore2ig_8_p2", "Walk_In_Bank_Intro_gunman", <<-109.622, 6467.932, 30.712>>, <<-00, 0.0, -45.0>>)
					
					
					vBankRobStartTrevorRot = GET_ANIM_INITIAL_OFFSET_ROTATION("missheistpaletoscore2ig_8_p2", "Walk_In_Bank_Intro_Player2", <<-109.622, 6467.932, 30.712>>, <<-00, 0.0, -45.0>>)
					vBankRobStartGunmanRot = GET_ANIM_INITIAL_OFFSET_ROTATION("missheistpaletoscore2ig_8_p2", "Walk_In_Bank_Intro_gunman", <<-109.622, 6467.932, 30.712>>, <<-00, 0.0, -45.0>>)
					
											
					PRINTLN("RBH: vBankRobStartTrevor:", vBankRobStartTrevor)
					PRINTLN("RBH: vBankRobStartGunman:", vBankRobStartGunman)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
						CLEAR_PRINTS()
					
					bLetsDoThisSaid = FALSE
					bRandomlySelectTorM = GET_RANDOM_BOOL()
					WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(arrivalBurrito, 3.0)
										
						IF bLetsDoThisSaid = FALSE
							IF bRandomlySelectTorM
								IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_READYM", CONV_PRIORITY_VERY_HIGH)
									bLetsDoThisSaid = TRUE
								ENDIF
							ELSE
								IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_READYT", CONV_PRIORITY_VERY_HIGH)
									bLetsDoThisSaid = TRUE
								ENDIF
							ENDIF
						ENDIF
						WAIT(0)
					ENDWHILE
					
					IF NOT IS_ENTITY_DEAD(PedTrevor())
						REMOVE_PED_FROM_GROUP(PedTrevor())							
					ENDIF
					IF NOT IS_ENTITY_DEAD(pedGunman)
						REMOVE_PED_FROM_GROUP(pedGunman)
					ENDIF
					
					STOP_AUDIO_SCENE("PS_2A_DRIVE_TO_PALETO")
					STOP_AUDIO_SCENE("PS_2A_REVERSE_TO_ENTRANCE")
								
					iExitVanpeds = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)

					IF NOT IS_ENTITY_DEAD(arrivalBurrito)	
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iExitVanpeds, arrivalBurrito, 0)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(arrivalBurrito, iExitVanpeds, "Exit_Van_ToBank_Van", "missheistpaletoscore2ig_8", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_BLOCK_MOVER_UPDATE) | ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS))
					ENDIF					

					IF NOT IS_PED_INJURED(pedGunman)								
						SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedGunman, FALSE)
						CLEAR_PED_TASKS_IMMEDIATELY(pedGunman)
						GIVE_WEAPON_TO_PED(pedGunman, pedGunmanWeaponType, INFINITE_AMMO, TRUE, TRUE)
						
						SET_RAGDOLL_BLOCKING_FLAGS(pedGunman, RBF_PED_RAGDOLL_BUMP)
						
						TASK_SYNCHRONIZED_SCENE(pedGunman, iExitVanpeds, "missheistpaletoscore2ig_8", "Exit_Van_ToBank_Gunman", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_NONE, RBF_NONE, INSTANT_BLEND_IN, AIK_USE_LEG_ALLOW_TAGS)//, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, SLOW_BLEND_IN)
					ENDIF
					
					IF NOT IS_PED_INJURED(pedTrevor())
						CLEAR_PED_TASKS_IMMEDIATELY(pedTrevor())
						GIVE_WEAPON_TO_PED(pedTrevor(), WEAPONTYPE_SAWNOFFSHOTGUN, 150, TRUE, TRUE)
						SET_RAGDOLL_BLOCKING_FLAGS(pedTrevor(), RBF_PED_RAGDOLL_BUMP)
						TASK_SYNCHRONIZED_SCENE(pedTrevor(), iExitVanpeds, "missheistpaletoscore2ig_8", "Exit_Van_ToBank_Player2", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_NONE, RBF_NONE, INSTANT_BLEND_IN, AIK_USE_LEG_ALLOW_TAGS)//, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, SLOW_BLEND_IN)
					ENDIF

					oiBagTrevor = CREATE_OBJECT(P_LD_HEIST_BAG_S_1, <<-111.5, 6463.9, 31.99>>)
					oiBagStrapTrevor = CREATE_OBJECT(p_csh_strap_01_s, <<-111.5, 6463.9, 31.99>>)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBagTrevor, iExitVanpeds, "Exit_Van_ToBank_Player2_Bag", "missheistpaletoscore2ig_8", INSTANT_BLEND_IN)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBagStrapTrevor, iExitVanpeds, "Exit_Van_ToBank_Player2_strap", "missheistpaletoscore2ig_8", INSTANT_BLEND_IN)
					
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiBagTrevor)
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiBagStrapTrevor)
					
					SET_ENTITY_LOD_DIST(oiBagTrevor, 200)
					SET_ENTITY_LOD_DIST(oiBagStrapTrevor, 200)
					
					SET_SYNCHRONIZED_SCENE_RATE(iExitVanpeds, 1.5)
					iRobBankStage = 50

				ENDIF
			ENDIF
		BREAK
		
		CASE 50
			IF GET_SYNCHRONIZED_SCENE_PHASE(iExitVanpeds) > 0.52
			OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iExitVanpeds)
				SET_MAX_WANTED_LEVEL(0)
				CLEAR_PED_TASKS(pedGunman)
				OPEN_SEQUENCE_TASK(taskSeqBank)
					//TASK_PAUSE(NULL, 375)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vBankRobStartGunman, PEDMOVE_SPRINT, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, vBankRobStartGunmanRot.z)//304)
					TASK_PAUSE(NULL, 300)
				CLOSE_SEQUENCE_TASK(taskSeqBank)
				TASK_PERFORM_SEQUENCE(pedGunman, taskSeqBank)
				CLEAR_SEQUENCE_TASK(taskSeqBank)
				SET_PED_CONFIG_FLAG(pedGunman, PCF_DisableExplosionReactions, TRUE)
				iRobBankStage = 51
			ENDIF
		BREAK
		
		CASE 51
			IF GET_SYNCHRONIZED_SCENE_PHASE(iExitVanpeds) > 0.6
			OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(iExitVanpeds)
				SET_MAX_WANTED_LEVEL(0)
				
				CLEAR_PED_TASKS(pedTrevor())
				
				OPEN_SEQUENCE_TASK(taskSeqBank)
					//TASK_PAUSE(NULL, 750)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vBankRobStartTrevor, PEDMOVE_SPRINT, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, vBankRobStartTrevorRot.z)//304,)
					TASK_PAUSE(NULL, 300)
				CLOSE_SEQUENCE_TASK(taskSeqBank)
				TASK_PERFORM_SEQUENCE(pedTrevor(), taskSeqBank)
				CLEAR_SEQUENCE_TASK(taskSeqBank)
				
				SET_PED_CONFIG_FLAG(pedTrevor(), PCF_DisableExplosionReactions, TRUE)
				//- clip dictionary: missheistpaletoscore2mcs_2_pt1
				//- Please attach props root to Trevor's spine "SKEL_Spine2"

				//Exported anims:
					//depot/gta5/art/anim/export_mb/MISS/HeistPaletoScore2/MCS_2_PT1/
				//AI_Loop_Player2_Bag.anim
				//AI_Loop_Player2_Strap.anim
				
				STOP_SYNCHRONIZED_ENTITY_ANIM(oiBagTrevor, INSTANT_BLEND_OUT, FALSE)
				STOP_SYNCHRONIZED_ENTITY_ANIM(oiBagStrapTrevor, INSTANT_BLEND_OUT, FALSE)
				
				ATTACH_ENTITY_TO_ENTITY(oiBagTrevor, pedTrevor(), GET_PED_BONE_INDEX(pedTrevor(), BONETAG_SPINE2), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				ATTACH_ENTITY_TO_ENTITY(oiBagStrapTrevor, pedTrevor(), GET_PED_BONE_INDEX(pedTrevor(), BONETAG_SPINE2), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				
				PLAY_ENTITY_ANIM(oiBagTrevor, "AI_Loop_Player2_Bag", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN, TRUE, FALSE)
				PLAY_ENTITY_ANIM(oiBagStrapTrevor, "AI_Loop_Player2_Strap", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN, TRUE, FALSE)
				
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiBagTrevor)
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiBagStrapTrevor)
				
				PRINT_NOW("GOINSIDE", DEFAULT_GOD_TEXT_TIME, 1)
				blipInsideBank = CREATE_BLIP_FOR_COORD(<<-110.0883, 6463.6597, 30.6267>>)
				
				
				bTrevorReady = FALSE
				bTrevorLooping = FALSE
				bGunmanReady = FALSE
				bGunmanLooping = FALSE
				STOP_SYNCHRONIZED_ENTITY_ANIM(arrivalBurrito, INSTANT_BLEND_OUT, TRUE)
				RESTART_TIMER_NOW(tmrRobbery)
				iRobBankStage = 52
			ENDIF
		BREAK
		
		
		CASE 52
			IF GET_TIMER_IN_SECONDS(tmrRobbery) >= 1.0
				SET_PED_CLOTH_PACKAGE_INDEX(PLAYER_PED_ID(), 2 )
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				iRobBankStage = 53
			ENDIF
		BREAK
		
		CASE 53
		
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				iRobBankStage = 6
			ENDIF
		BREAK
		
		CASE 6
			IF NOT bTrevorReady 
				IF GET_SCRIPT_TASK_STATUS(pedTrevor(), SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
				//IF IS_ENTITY_AT_COORD(pedTrevor(), vBankRobStartTrevor, <<1.35, 1.35, 1.5>>)
				//AND GET_ENTITY_SPEED(pedTrevor()) < 0.15
				
					//Guys burst out and into the bank...
				
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBankParkedCars[0])
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBankParkedCars[1])
					SET_MODEL_AS_NO_LONGER_NEEDED(WASHINGTON)
				
					iBurstInBankSyncedSceneTrevor = CREATE_SYNCHRONIZED_SCENE(<<-109.622, 6467.932, 30.712>>, <<-00, 0.0, -45.0>>)
					CPRINTLN(DEBUG_MISSION, "iBurstInBankSyncedSceneTrevor (walk_in_bank_intro) = ", iBurstInBankSyncedSceneTrevor)
					TASK_SYNCHRONIZED_SCENE(pedTrevor(), iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2ig_8_p2", "Walk_In_Bank_Intro_Player2", SLOW_BLEND_IN, INSTANT_BLEND_OUT)//, SYNCED_SCENE_, RBF_NONE, SLOW_BLEND_IN)
					
					SET_SYNCHRONIZED_SCENE_LOOPED(iBurstInBankSyncedSceneTrevor, FALSE)
					DETACH_ENTITY(oiBagTrevor)
					DETACH_ENTITY(oiBagStrapTrevor)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBagTrevor, iBurstInBankSyncedSceneTrevor, "Walk_In_Bank_Intro_Player2_Bag", "missheistpaletoscore2ig_8_p2", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBagStrapTrevor, iBurstInBankSyncedSceneTrevor, "Walk_In_Bank_Intro_Player2_strap", "missheistpaletoscore2ig_8_p2", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedTrevor())
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiBagTrevor)	
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiBagStrapTrevor)
					CPRINTLN(DEBUG_MISSION, "stageRobTheBank - Trevor has gotten into position.  bTrevorReady = TRUE ")
					bTrevorReady = TRUE
				ENDIF
			ELSE
				IF NOT bTrevorLooping
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iBurstInBankSyncedSceneTrevor)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iBurstInBankSyncedSceneTrevor) >= 1.0
							iBurstInBankSyncedSceneTrevor = CREATE_SYNCHRONIZED_SCENE(<<-109.622, 6467.932, 30.712>>, <<-00, 0.0, -45.0>>)
							CPRINTLN(DEBUG_MISSION, "iBurstInBankSyncedSceneTrevor (Start_Loop_Player2) = ", iBurstInBankSyncedSceneTrevor)
							TASK_SYNCHRONIZED_SCENE(pedTrevor(), iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "Start_Loop_Player2", INSTANT_BLEND_IN, INSTANT_BLEND_OUT) //, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, SLOW_BLEND_IN)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBagTrevor, iBurstInBankSyncedSceneTrevor, "Start_Loop_Player2_bag", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBagStrapTrevor, iBurstInBankSyncedSceneTrevor, "Start_Loop_Player2_strap", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
							
							SET_SYNCHRONIZED_SCENE_LOOPED(iBurstInBankSyncedSceneTrevor, TRUE)
													
							//into the bank...
								
							pedsBankers[0] = CREATE_PED(PEDTYPE_MISSION, S_F_Y_AIRHOSTESS_01, <<-109.622, 6467.932, 30.712>>)
							pedsBankers[1] = CREATE_PED(PEDTYPE_MISSION, A_M_M_BUSINESS_01, <<-109.622, 6467.932, 30.712>>)
							pedsBankers[2] =CREATE_PED(PEDTYPE_MISSION, A_M_M_BUSINESS_01, <<-109.622, 6467.932, 30.712>>)
							pedsBankers[3] =CREATE_PED(PEDTYPE_MISSION, A_M_M_BUSINESS_01, <<-109.622, 6467.932, 30.712>>)
							pedsBankers[4] =CREATE_PED(PEDTYPE_MISSION, A_M_M_BUSINESS_01, <<-109.622, 6467.932, 30.712>>)
							
							FOR i = 0 TO 4
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsBankers[i], TRUE)
								SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedsBankers[i], FALSE)
								SET_PED_CONFIG_FLAG(pedsBankers[i], PCF_DisableExplosionReactions, TRUE)
								SET_PED_KEEP_TASK(pedsBankers[i], TRUE)							
								#IF IS_DEBUG_BUILD
									debugPedName = "BankPed:"
									debugPedName += i
									SET_PED_NAME_DEBUG(pedsBankers[i], debugPedName)
								#ENDIF
							ENDFOR
							
							IF NOT IS_PED_INJURED(pedsBankers[0])
								TASK_SYNCHRONIZED_SCENE(pedsBankers[0], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "start_loop_F", NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
							ENDIF
							IF NOT IS_PED_INJURED(pedsBankers[1])
								TASK_SYNCHRONIZED_SCENE(pedsBankers[1], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "start_loop_M1", NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
							ENDIF
							IF NOT IS_PED_INJURED(pedsBankers[2])
								TASK_SYNCHRONIZED_SCENE(pedsBankers[2], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "start_loop_M2", NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
							ENDIF
							IF NOT IS_PED_INJURED(pedsBankers[3])
								TASK_SYNCHRONIZED_SCENE(pedsBankers[3], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "start_loop_M3", NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
							ENDIF
							IF NOT IS_PED_INJURED(pedsBankers[4])
								TASK_SYNCHRONIZED_SCENE(pedsBankers[4], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "start_loop_M4", NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
							ENDIF
						
							CPRINTLN(DEBUG_MISSION, "1339066 - Before playing start_loop_door anims")
							PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBankDoorRight, iBurstInBankSyncedSceneTrevor, "start_loop_door_r", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBankDoorLeft, iBurstInBankSyncedSceneTrevor, "start_loop_door_l", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
							PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-108.9147, 6469.1045, 31.9103>>, 1.0, V_ILEV_CBANKCOUNTDOOR01, iBurstInBankSyncedSceneTrevor, "start_loop_CountDoor", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
							CPRINTLN(DEBUG_MISSION, "1339066 - After playing start_loop_door anims")
							
							FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiBankDoorLeft)
							FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiBankDoorRight)
							//FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiStoreDoorTempCount)
							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedTrevor())
							FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiBagTrevor)	
							FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiBagStrapTrevor)	
							CPRINTLN(DEBUG_MISSION, "stageRobTheBank - bTrevorLooping = TRUE ")
							bTrevorLooping = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			IF NOT bGunmanReady
				IF GET_SCRIPT_TASK_STATUS(pedGunman, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
				
					//Guys burst out and into the bank...
				
					iBurstInBankSyncedSceneGunman = CREATE_SYNCHRONIZED_SCENE(<<-109.622, 6467.932, 30.712>>, <<-00, 0.0, -45.0>>)
					CPRINTLN(DEBUG_MISSION, "iBurstInBankSyncedSceneGunman (Walk_In_Bank_Intro_Gunman) = ", iBurstInBankSyncedSceneGunman)
							
					TASK_SYNCHRONIZED_SCENE(pedGunman, iBurstInBankSyncedSceneGunman, "missheistpaletoscore2ig_8_p2", "Walk_In_Bank_Intro_Gunman", SLOW_BLEND_IN, INSTANT_BLEND_OUT) //, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, SLOW_BLEND_IN )
					
					SET_SYNCHRONIZED_SCENE_LOOPED(iBurstInBankSyncedSceneGunman, FALSE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGunman)	
					CPRINTLN(DEBUG_MISSION, "bGunmanReady = TRUE")
					bGunmanReady = TRUE
				ENDIF
			ELSE
				IF NOT bGunmanLooping
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iBurstInBankSyncedSceneGunman)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iBurstInBankSyncedSceneGunman) >= 1.0
							iBurstInBankSyncedSceneGunman = CREATE_SYNCHRONIZED_SCENE(<<-109.622, 6467.932, 30.712>>, <<-00, 0.0, -45.0>>)
							TASK_SYNCHRONIZED_SCENE(pedGunman, iBurstInBankSyncedSceneGunman, "missheistpaletoscore2ig_8_p2", "Start_Loop_A_Gunman", INSTANT_BLEND_IN, INSTANT_BLEND_OUT) //, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, SLOW_BLEND_IN)
							CPRINTLN(DEBUG_MISSION, "iBurstInBankSyncedSceneGunman (Start_Loop_A_Gunman) = ", iBurstInBankSyncedSceneGunman)
							SET_SYNCHRONIZED_SCENE_LOOPED(iBurstInBankSyncedSceneGunman, TRUE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedGunman)	
							CPRINTLN(DEBUG_MISSION, "bGunmanLooping = TRUE")
							bGunmanLooping = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bTrevorLooping
			AND bGunmanLooping
			AND CREATE_GUNMAN_CONVERSATION("RBH2A_HLD_GM", "RBH2A_HLD_DJ", "RBH2A_HLD_CH", "RBH2A_HLD_NR", "RBH2A_HLD_PM")
				REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlockObject)
				iRobBankStage = 8
			ENDIF

		BREAK
		
		CASE 8
			IF IS_ENTITY_AT_BANK_ENTRANCE(PLAYER_PED_ID())
			AND PREPARE_ALARM("PALETO_BAY_SCORE_ALARM")
			
				SET_MODEL_AS_NO_LONGER_NEEDED(BURRITO3)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(arrivalBurrito)
								
				//Guys burst out and into the bank...
				iBurstInBankSyncedSceneTrevor = CREATE_SYNCHRONIZED_SCENE(<<-109.622, 6467.932, 30.712>>, <<-00, 0.0, -45.0>>)
				iBurstInBankSyncedSceneGunman = CREATE_SYNCHRONIZED_SCENE(<<-109.622, 6467.932, 30.712>>, <<-00, 0.0, -45.0>>)
				CPRINTLN(DEBUG_MISSION, "iBurstInBankSyncedSceneTrevor (walk_in_bank_player2) = ", iBurstInBankSyncedSceneTrevor)
				CPRINTLN(DEBUG_MISSION, "iBurstInBankSyncedSceneGunman (walk_in_bank_gunman) = ", iBurstInBankSyncedSceneGunman)
				
//				CLEAR_PED_TASKS_IMMEDIATELY(pedGunman)
//				CLEAR_PED_TASKS_IMMEDIATELY(pedTrevor())
				
				TASK_SYNCHRONIZED_SCENE(pedTrevor(), iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "walk_in_bank_player2", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				TASK_SYNCHRONIZED_SCENE(pedGunman, iBurstInBankSyncedSceneGunman, "missheistpaletoscore2mcs_2_pt1", "walk_in_bank_gunman", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				
				SET_SYNCHRONIZED_SCENE_LOOPED(iBurstInBankSyncedSceneTrevor, FALSE)
				SET_SYNCHRONIZED_SCENE_LOOPED(iBurstInBankSyncedSceneGunman, FALSE)
				
				IF NOT IS_PED_INJURED(pedsBankers[0])
					TASK_SYNCHRONIZED_SCENE(pedsBankers[0], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "walk_in_bank_F", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				IF NOT IS_PED_INJURED(pedsBankers[1])
					TASK_SYNCHRONIZED_SCENE(pedsBankers[1], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "walk_in_bank_M1", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				IF NOT IS_PED_INJURED(pedsBankers[2])
					TASK_SYNCHRONIZED_SCENE(pedsBankers[2], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "walk_in_bank_M2", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				IF NOT IS_PED_INJURED(pedsBankers[3])
					TASK_SYNCHRONIZED_SCENE(pedsBankers[3], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "walk_in_bank_M3", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				IF NOT IS_PED_INJURED(pedsBankers[4])
					TASK_SYNCHRONIZED_SCENE(pedsBankers[4], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "walk_in_bank_M4", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				
									
				iBurstInBankSyncedSceneTrevorDoors = CREATE_SYNCHRONIZED_SCENE(<<-109.622, 6467.932, 30.712>>, <<-00, 0.0, -45.0>>)
				
				SET_SYNCHRONIZED_SCENE_LOOPED(iBurstInBankSyncedSceneTrevorDoors, FALSE)
				
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iBurstInBankSyncedSceneTrevorDoors, TRUE)
							
				CPRINTLN(DEBUG_MISSION, "1339066 - Before playing walk_in_bank_door anims")
				IF DOES_ENTITY_EXIST(oiBankDoorRight)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBankDoorRight, iBurstInBankSyncedSceneTrevorDoors, "walk_in_bank_door_r", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
				ENDIF
			
				IF DOES_ENTITY_EXIST(oiBankDoorLeft)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBankDoorLeft, iBurstInBankSyncedSceneTrevorDoors, "walk_in_bank_door_l", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
				ENDIF
				
				
				//IF DOES_ENTITY_EXIST(oiStoreDoorTempCount)
					PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-108.9147, 6469.1045, 31.9103>>, 1.0, V_ILEV_CBANKCOUNTDOOR01, iBurstInBankSyncedSceneTrevor, "walk_in_bank_CountDoor", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
				//ENDIF

				PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBagTrevor, iBurstInBankSyncedSceneTrevor, "walk_in_bank_player2_bag", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBagStrapTrevor, iBurstInBankSyncedSceneTrevor, "walk_in_bank_player2_strap", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
				CPRINTLN(DEBUG_MISSION, "1339066 - After playing walk_in_bank_door anims")
				
				//SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iBurstInBankSyncedSceneTrevor, TRUE)
				
				//PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBagGunman, iBurstInBankSyncedSceneTrevor, "walk_in_bank_gunman_bag", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
				
					
				iBurstInBankSyncedSceneGunman = CREATE_SYNCHRONIZED_SCENE(<<-105.414, 6474.930, 30.630>>, <<-00, 0.0, -45.0>>)	
				CREATE_MODEL_HIDE(<<-104.81, 6473.65, 31.95>>, 10.0, V_ilev_cbankvaulgate01, FALSE)			
				oiStoreDoorTempVault = CREATE_OBJECT(V_ilev_cbankvaulgate01, <<-107.1207, 6466.7041, 30.6267>>)
			
				CPRINTLN(DEBUG_MISSION, "1339066 - Before playing burn_door_loop_VaultGate anims")
				PLAY_SYNCHRONIZED_ENTITY_ANIM(oiStoreDoorTempVault, iBurstInBankSyncedSceneGunman, "burn_door_loop_VaultGate", "missheistpaletoscore2mcs_2_p5", INSTANT_BLEND_IN)
				CPRINTLN(DEBUG_MISSION, "1339066 - After playing burn_door_loop_VaultGate anims")
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiStoreDoorTempVault)	
				
				START_AUDIO_SCENE("PS_2A_ENTER_BANK")
				
				iRobBankStage++
			ENDIF
		BREAK
		
		CASE 9
			
			//Gunshot // doors
			IF GET_SYNCHRONIZED_SCENE_PHASE(iBurstInBankSyncedSceneTrevor) >= 0.148
				SET_PED_SHOOTS_AT_COORD(pedTrevor(), <<-108.2059, 6462.7246, 33.3677>>) //<<-108.7076, 6462.1543, 33.3609>>)			
				REMOVE_BLIP(blipInsideBank)
				
				REPLAY_RECORD_BACK_FOR_TIME(3.5)
				
				iRobBankStage++
			ENDIF
		BREAK
		
		CASE 10
			IF PREPARE_MUSIC_EVENT("RH2A_ENTER_BANK")
				TRIGGER_MUSIC_EVENT("RH2A_ENTER_BANK")
				iRobBankStage++		
			ENDIF
		BREAK
		
		CASE 11
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_GUNMAN_CONVERSATION("RBH2A_DWN_GM", "RBH2A_DWN_DJ", "RBH2A_DWN_CH", "RBH2A_DWN_NR", "RBH2A_DWN_PM")
					iRobBankStage++		
				ENDIF	
			ENDIF
		BREAK
		
		CASE 12
			
			//Screams
			IF GET_SYNCHRONIZED_SCENE_PHASE(iBurstInBankSyncedSceneTrevor) >= 0.151
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedsBankers[0], "SCREAM_PANIC", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE_FRONTEND)	
				
				//SoundIdAlarm = GET_SOUND_ID()
				//PLAY_SOUND_FROM_COORD(SoundIdAlarm,"Generic_Alarm_Burglar_Bell" , << -108.5419, 6465.4390, 30.6343 >>)
			
				START_ALARM("PALETO_BAY_SCORE_ALARM", FALSE)
			
				iRobBankStage = 13
			ENDIF
		
		BREAK
		
		CASE 13
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_ROB", CONV_PRIORITY_VERY_HIGH)
					iRobBankStage = 14
				ENDIF	
			ENDIF
		BREAK
				
		CASE 14
		CASE 15
		CASE 16
			
			IF NOT ARE_ALL_BANKERS_DEAD()
			AND iRobBankStage = 14
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_ROBb", CONV_PRIORITY_VERY_HIGH)
						iRobBankStage = 15
					ENDIF	
				ENDIF
			ENDIF
			
			IF iRobBankStage = 15
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_GUNMAN_CONVERSATION("RBH2A_VLT_GM", "RBH2A_VLT_DJ", "RBH2A_VLT_CH", "RBH2A_VLT_NR", "RBH2A_VLT_PM")
						iRobBankStage = 16
					ENDIF
				ENDIF
			ENDIF					
		
			IF GET_SYNCHRONIZED_SCENE_PHASE(iBurstInBankSyncedSceneTrevor) >= 0.99
				bRunSwitch = TRUE
				iBurstInBankSyncedSceneTrevor = CREATE_SYNCHRONIZED_SCENE(<<-109.622, 6467.932, 30.712>>, <<-00, 0.0, -45.0>>)
				vTrevorBankPosition = GET_ENTITY_COORDS(pedTrevor())
				flTrevorBankRotation = GET_ENTITY_HEADING(pedTrevor())
				
				bTrevorPlayIntimidationAnims = TRUE
				UPDATE_TREVOR_INTIMIDATION()
				
//				TASK_SYNCHRONIZED_SCENE(pedTrevor(), iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "end_loop_player2", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON)
//				CPRINTLN(DEBUG_MISSION, "1339066 - Before playing end_loop_player2_bag anims")
				PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBagTrevor, iBurstInBankSyncedSceneTrevor, "end_loop_player2_bag", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
				CPRINTLN(DEBUG_MISSION, "1339066 - After playing end_loop_player2_bag anims")
						
				IF NOT IS_PED_INJURED(pedsBankers[0])
					TASK_SYNCHRONIZED_SCENE(pedsBankers[0], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "end_loop_F", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				IF NOT IS_PED_INJURED(pedsBankers[1])
					TASK_SYNCHRONIZED_SCENE(pedsBankers[1], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "end_loop_M1", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				IF NOT IS_PED_INJURED(pedsBankers[2])
					TASK_SYNCHRONIZED_SCENE(pedsBankers[2], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "end_loop_M2", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				IF NOT IS_PED_INJURED(pedsBankers[3])
					TASK_SYNCHRONIZED_SCENE(pedsBankers[3], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "end_loop_M3", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
				IF NOT IS_PED_INJURED(pedsBankers[4])
					TASK_SYNCHRONIZED_SCENE(pedsBankers[4], iBurstInBankSyncedSceneTrevor, "missheistpaletoscore2mcs_2_pt1", "end_loop_M4", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				ENDIF
//				PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBankDoorLeft, iBurstInBankSyncedSceneTrevor, "end_loop_door_l", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
//				PLAY_SYNCHRONIZED_ENTITY_ANIM(oiBankDoorRight, iBurstInBankSyncedSceneTrevor, "end_loop_door_r", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)			
//				PLAY_SYNCHRONIZED_ENTITY_ANIM(oiStoreDoorTempCount, iBurstInBankSyncedSceneTrevor, "end_loop_countdoor", "missheistpaletoscore2mcs_2_pt1", INSTANT_BLEND_IN)
//			
//				DELETE_OBJECT(oiStoreDoorTempCount)
				//REMOVE_MODEL_HIDE(<<-108.9147, 6469.1045, 31.9103>>, 5.0, V_ilev_cbankcountdoor01, FALSE)
				
				STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-108.9147, 6469.1045, 31.9103>>, 1.0, V_ILEV_CBANKCOUNTDOOR01, INSTANT_BLEND_OUT)
				
				IF bDoorAnimStoppedEarly = FALSE
					SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_CBANKCOUNTDOOR01, <<-108.9147, 6469.1045, 31.9103>>, TRUE, 0.0)
				ENDIF
				
				SET_MODEL_AS_NO_LONGER_NEEDED(V_ilev_cbankcountdoor01)
				SET_SYNCHRONIZED_SCENE_LOOPED(iBurstInBankSyncedSceneTrevor, TRUE)
				iRobBankStage = 17
			ENDIF
			
		BREAK
		
		CASE 17
		BREAK
		
	ENDSWITCH

	IF DOES_ENTITY_EXIST(pedsBankers[0])
	AND DOES_ENTITY_EXIST(pedsBankers[3])
		IF IS_PED_INJURED(pedsBankers[0])
		AND IS_PED_INJURED(pedsBankers[3])
			IF bDoorAnimStoppedEarly = FALSE
				STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-108.9147, 6469.1045, 31.9103>>, 1.0, V_ILEV_CBANKCOUNTDOOR01, INSTANT_BLEND_OUT)
			
				bDoorAnimStoppedEarly = TRUE
			ENDIF
		ENDIF
		//Once the door is past a certain point and if one is dead then we can return the door to be physical
		IF IS_PED_INJURED(pedsBankers[0])
		OR IS_PED_INJURED(pedsBankers[3])		
			IF GET_SYNCHRONIZED_SCENE_PHASE(iBurstInBankSyncedSceneTrevor) >= 0.696
				IF bDoorAnimStoppedEarly = FALSE
					STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-108.9147, 6469.1045, 31.9103>>, 1.0, V_ILEV_CBANKCOUNTDOOR01, INSTANT_BLEND_OUT)			
					bDoorAnimStoppedEarly = TRUE
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC



