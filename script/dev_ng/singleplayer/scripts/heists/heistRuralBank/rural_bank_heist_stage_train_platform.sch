USING "rural_bank_heist_support.sch"


INT iRandomDialogue
BLIP_INDEX blipTrainPlatform
PROC CONTROL_PLATFORM_DIALOGUE()
	
	IF TIMERB() > 2500
	AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			
		SWITCH iRandomDialogue
		
			CASE 0
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_HLD_MIK", CONV_PRIORITY_MEDIUM)
					SETTIMERB(0)
					iRandomDialogue = GET_RANDOM_INT_IN_RANGE(0, 3)
				ENDIF
			BREAK
		
			CASE 1
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_HLD_TRV", CONV_PRIORITY_MEDIUM)
					SETTIMERB(0)
					iRandomDialogue = GET_RANDOM_INT_IN_RANGE(0, 3)
				ENDIF
			BREAK
			
			CASE 2
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_HLD_FRN", CONV_PRIORITY_MEDIUM)
					SETTIMERB(0)
					iRandomDialogue = GET_RANDOM_INT_IN_RANGE(0, 3)
				ENDIF
			BREAK
		
		ENDSWITCH
		
	ENDIF

ENDPROC

ENUM TRAIN_PLATFORM_CUTSCENE_STATE
	TRAIN_PLATFORM_CUTSCENE_WAIT_FOR_STREAMING,
	TRAIN_PLATFORM_CUTSCENE_INIT,
	TRAIN_PLATFORM_CUTSCENE_FIRST_FRAME,
	TRAIN_PLATFORM_CUTSCENE_WAIT,
	TRAIN_PLATFORM_CUTSCENE_DONE
ENDENUM
TRAIN_PLATFORM_CUTSCENE_STATE	eTrainPlatformCutscene

ENUM TRAIN_PLATFORM_STATE
	TRAIN_PLATFORM_STATE_INIT,
	TRAIN_PLATFORM_STATE_WAIT_FOR_STREAMING,
	TRAIN_PLATFORM_STATE_WAIT_TO_START,
	TRAIN_PLATFORM_STATE_WAIT_FOR_TRAIN_TO_PASS,
	TRAIN_PLATFORM_STATE_SUCCESS_CUTSCENE,
	TRAIN_PLATFORM_STATE_FAIL,
	TRAIN_PLATFORM_STATE_DONE
ENDENUM
TRAIN_PLATFORM_STATE	eTrainState
structTimer				tmrTrainController
BOOL					bFranklinTaskedOnPlatform//, bTrainPlatformWarningPlayed
INT 					iNumberOfSoldiersToHoldOff = 20
INT						iNumberOfSoldiersToSpawnTrain
VECTOR					vBlipPlatformLocation = <<-142.30260, 6143.63965, 31.44318>>

//FLOAT					flRAGTimeToFailPlatform = 20.0
//FLOAT					flRAGTimeToWarnTrain = 10.0
FLOAT					flRAGTrainSpeed = 7.5
FLOAT					flRAGTrainCruiseSpeed = 7.5

FUNC BOOL IS_TRAIN_PLATFORM_CUTSCENE_DONE()
	SWITCH eTrainPlatformCutscene
		CASE TRAIN_PLATFORM_CUTSCENE_WAIT_FOR_STREAMING
					
			IF HAS_CUTSCENE_LOADED()
				eTrainPlatformCutscene = TRAIN_PLATFORM_CUTSCENE_INIT
				CPRINTLN(DEBUG_MISSION, "IS_TRAIN_PLATFORM_CUTSCENE_DONE - Going to TRAIN_PLATFORM_CUTSCENE_INIT")
			ENDIF
		BREAK
		
		CASE TRAIN_PLATFORM_CUTSCENE_INIT
		
			IF IS_AUDIO_SCENE_ACTIVE("PS_2A_TRAIN_ARRIVES")
				STOP_AUDIO_SCENE("PS_2A_TRAIN_ARRIVES")
			ENDIF
			
			//CLEAR_AREA(<<-826.457886,180.473969,71.133858>>, 100.00, TRUE)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()			
			DELETE_ARRAY_OF_PEDS(pedsAtChickenFactoryPlatform, TRUE)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_MARINE_03)
			
			//SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(chickenTrain)
			
			RESET_CREW_ANIMS()
							
			
			IF NOT IS_ENTITY_DEAD(pedTrevor())
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTrevor(), TRUE)
				REGISTER_ENTITY_FOR_CUTSCENE(pedTrevor(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(PedMichael())
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedMichael(), TRUE)
				REGISTER_ENTITY_FOR_CUTSCENE(PedMichael(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedFranklin())
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFranklin(), TRUE)
				REGISTER_ENTITY_FOR_CUTSCENE(pedFranklin(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
			
			SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
			START_CUTSCENE()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			STOP_AUDIO_SCENE("PS_2A_SHOOTOUT_WAREHOUSE")
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
			eTrainPlatformCutscene = TRAIN_PLATFORM_CUTSCENE_FIRST_FRAME
			CPRINTLN(DEBUG_MISSION, "IS_TRAIN_PLATFORM_CUTSCENE_DONE - Going to TRAIN_PLATFORM_CUTSCENE_FIRST_FRAME")
		
		BREAK
		
		CASE TRAIN_PLATFORM_CUTSCENE_FIRST_FRAME
			IF IS_CUTSCENE_PLAYING()
				
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)
				
				IF DOES_ENTITY_EXIST(chickenTrainPlatform)
					DELETE_MISSION_TRAIN(chickenTrainPlatform)
				ENDIF
				IF DOES_ENTITY_EXIST(chickenTrain)
					DELETE_MISSION_TRAIN(chickenTrain)
				ENDIF
				SET_TRAIN_MODELS_AS_NO_LONGER_NEEDED()
				
				IF PREPARE_MUSIC_EVENT("RH2A_TRAIN")
					TRIGGER_MUSIC_EVENT("RH2A_TRAIN")
					eTrainPlatformCutscene = TRAIN_PLATFORM_CUTSCENE_WAIT
					CPRINTLN(DEBUG_MISSION, "IS_TRAIN_PLATFORM_CUTSCENE_DONE - Going to TRAIN_PLATFORM_CUTSCENE_WAIT")
				ENDIF
			ENDIF
		BREAK
		
		CASE TRAIN_PLATFORM_CUTSCENE_WAIT
			IF GET_CUTSCENE_TIME() > 7000.00
				TRIGGER_MUSIC_EVENT("RH2A_STOP_TRACK")
				eTrainPlatformCutscene = TRAIN_PLATFORM_CUTSCENE_DONE
				CPRINTLN(DEBUG_MISSION, "IS_TRAIN_PLATFORM_CUTSCENE_DONE - Going to TRAIN_PLATFORM_CUTSCENE_DONE")
			ENDIF
		BREAK
		
		CASE TRAIN_PLATFORM_CUTSCENE_DONE
			IF HAS_CUTSCENE_FINISHED()
				REPLAY_STOP_EVENT()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC


PROC SPAWN_NEW_SET_OF_ARMY_GUYS_ON_PLATFORM(INT iSeed)
	
	INT i
	
	RESET_MISSION_STATS_ENTITY_WATCH()
	
	//DELETE_ARRAY_OF_BLIPS(blippedsAtChickenFactoryPlatform)
	
	//4 respawn guys
	
	IF iSeed % 2 = 0
	
	
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -175.4348, 6166.5396, 30.2065 >>) > 2.5
			IF IS_PED_INJURED(pedsAtChickenFactoryPlatform[0])
				SET_PED_AS_NO_LONGER_NEEDED(pedsAtChickenFactoryPlatform[0])
				pedsAtChickenFactoryPlatform[0] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , << -175.4348, 6166.5396, 30.2065 >>, 315.1821)
			ENDIF
		ENDIF
		IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
			IF NOT IS_SPHERE_VISIBLE(<< -165.3616, 6173.0171, 30.2066 >>, 1.0)
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -165.3616, 6173.0171, 30.2066 >>) > 2.5
				IF IS_PED_INJURED(pedsAtChickenFactoryPlatform[1])
					SET_PED_AS_NO_LONGER_NEEDED(pedsAtChickenFactoryPlatform[1])
					pedsAtChickenFactoryPlatform[1] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , << -165.3616, 6173.0171, 30.2066 >>, 185.8168)
				ENDIF
			ENDIF
		ENDIF
		IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
			IF NOT IS_SPHERE_VISIBLE(<< -172.4934, 6163.8379, 30.2066 >>, 1.0)
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -172.4934, 6163.8379, 30.2066 >>) > 2.5
				IF IS_PED_INJURED(pedsAtChickenFactoryPlatform[2])
					SET_PED_AS_NO_LONGER_NEEDED(pedsAtChickenFactoryPlatform[2])
					pedsAtChickenFactoryPlatform[2] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , << -172.4934, 6163.8379, 30.2066 >>, 172.1139)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_SPHERE_VISIBLE(<< -177.8697, 6164.0288, 30.1444 >>, 1.0)
		AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -177.8697, 6164.0288, 30.1444 >>) > 2.5
			IF IS_PED_INJURED(pedsAtChickenFactoryPlatform[3])
				SET_PED_AS_NO_LONGER_NEEDED(pedsAtChickenFactoryPlatform[3])
				pedsAtChickenFactoryPlatform[3] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , << -177.8697, 6164.0288, 30.1444 >>, 314.7321)
			ENDIF
		ENDIF
	
	ELSE
		IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
			IF NOT IS_SPHERE_VISIBLE(<< -141.9374, 6156.9868, 30.2062 >>, 1.0)
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -141.9374, 6156.9868, 30.2062 >>) > 2.5
				IF IS_PED_INJURED(pedsAtChickenFactoryPlatform[0])
					SET_PED_AS_NO_LONGER_NEEDED(pedsAtChickenFactoryPlatform[0])
					pedsAtChickenFactoryPlatform[0] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , << -141.9374, 6156.9868, 30.2062 >>, 134.4970)
				ENDIF
			ENDIF
		ENDIF
		IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
			IF NOT IS_SPHERE_VISIBLE(<< -139.9242, 6158.4917, 30.2062 >>, 1.0)
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -139.9242, 6158.4917, 30.2062 >>) > 2.5
				IF IS_PED_INJURED(pedsAtChickenFactoryPlatform[1])
					SET_PED_AS_NO_LONGER_NEEDED(pedsAtChickenFactoryPlatform[1])
					pedsAtChickenFactoryPlatform[1] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , << -139.9242, 6158.4917, 30.2062 >>, 134.4970)
				ENDIF
			ENDIF
		ENDIF
		IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
			IF NOT IS_SPHERE_VISIBLE(<< -137.4056, 6155.7119, 30.2062 >>, 1.0)
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -137.4056, 6155.7119, 30.2062 >>) > 2.5
				IF IS_PED_INJURED(pedsAtChickenFactoryPlatform[2])
					SET_PED_AS_NO_LONGER_NEEDED(pedsAtChickenFactoryPlatform[2])
					pedsAtChickenFactoryPlatform[2] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 ,<< -137.4056, 6155.7119, 30.2062 >>, 134.4970 )
				ENDIF
			ENDIF
		ENDIF
		IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
			IF NOT IS_SPHERE_VISIBLE(<< -136.2893, 6158.7217, 30.1484 >>, 1.0)
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -136.2893, 6158.7217, 30.1484 >>) > 2.5
				IF IS_PED_INJURED(pedsAtChickenFactoryPlatform[3])
					SET_PED_AS_NO_LONGER_NEEDED(pedsAtChickenFactoryPlatform[3])
					pedsAtChickenFactoryPlatform[3] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_Marine_03 , << -136.2893, 6158.7217, 30.1484 >>, 57.5364)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
			
	FOR i = 0 TO 3
		IF DOES_ENTITY_EXIST(pedsAtChickenFactoryPlatform[i])
			IF NOT IS_PED_INJURED(pedsAtChickenFactoryPlatform[i])
				SET_PED_AS_DEFENSIVE_COP(pedsAtChickenFactoryPlatform[i], WEAPONTYPE_CARBINERIFLE, FALSE, FALSE, 2.1, TRUE)
				
				
				SWITCH i 
					
					CASE 0
						SET_PED_SPHERE_DEFENSIVE_AREA(pedsAtChickenFactoryPlatform[i], <<-161.6081, 6148.9517, 30.2064>>, 2.0, TRUE)
					BREAK
					
					CASE 1
						//SET_PED_COMBAT_ATTRIBUTES(pedsAtChickenFactoryPlatform[i], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, FALSE)
						SET_PED_SPHERE_DEFENSIVE_AREA(pedsAtChickenFactoryPlatform[i], <<-147.2894, 6161.0806, 30.2062>>, 2.0, TRUE)
					BREAK
					
					CASE 2
						SET_PED_SPHERE_DEFENSIVE_AREA(pedsAtChickenFactoryPlatform[i], <<-149.5275, 6152.6123, 30.2063>>, 2.0, TRUE)
					BREAK
					
					
					CASE 3
						SET_PED_ANGLED_DEFENSIVE_AREA(pedsAtChickenFactoryPlatform[i], <<-147.384979,6157.576172,29.706230>>, <<-167.002441,6139.723145,34.065205>>, 11.750000, TRUE)
						//SET_PED_COMBAT_ATTRIBUTES(pedsAtChickenFactoryPlatform[i], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, FALSE)
					BREAK
										
				ENDSWITCH
						
				SET_PED_COMBAT_ATTRIBUTES(pedsAtChickenFactoryPlatform[i], CA_OPEN_COMBAT_WHEN_DEFENSIVE_AREA_IS_REACHED, TRUE)
												
				SET_PED_COMBAT_ATTRIBUTES(pedsAtChickenFactoryPlatform[i], 	CA_AGGRESSIVE, TRUE)
				SET_PED_COMBAT_ATTRIBUTES(pedsAtChickenFactoryPlatform[i],	CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH,TRUE)
				
				TEXT_LABEL_63 debugPedName = "holdOffPed "
				debugPedName += i
			
				SET_PED_NAME_DEBUG(pedsAtChickenFactoryPlatform[i], debugPedName)
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedsAtChickenFactoryPlatform[i], FALSE)

				TASK_COMBAT_HATED_TARGETS_AROUND_PED(pedsAtChickenFactoryPlatform[i], 100.00)		
			ENDIF
		ENDIF
	ENDFOR
	
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	
	SETUP_WANTED_LEVEL(5, TRUE, 100.0, 10.0, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)

ENDPROC

PROC UPDATE_PLATFORM_ENEMY_WAVES()
	UPDATE_AI_PED_BLIP_ARRAY(pedsAtChickenFactoryPlatform, blippedsAtChickenFactoryPlatform)

	//Continually spawn guys...
	IF GET_NUMBER_OF_PEDS_INJURED(pedsAtChickenFactoryPlatform) >= 1  //IS_ARRAY_OF_PEDS_INJURED(pedsAtChickenFactoryPlatform)
	//AND (iJumpOnTrainStage = 3 OR TIMERB() > 4000)
	//AND TIMERA() > 4000	//
	//AND TIMERB() > 1000
	AND NOT DOES_BLIP_EXIST(blipInsideFactory)
		SPAWN_NEW_SET_OF_ARMY_GUYS_ON_PLATFORM(iNumberOfSoldiersSpawned)
		PRINTLN("******* SOME NEW DUDES SPAWNED **********")
	//	SETTIMERB(0)
		iNumberOfSoldiersSpawned++
	ENDIF
ENDPROC

INT iGetOnTrainReminderTime
BOOL bAlternateTrainLine = FALSE

PROC UPDATE_TRAIN_PLATFORM_CONVERSATIONS()
	SWITCH eTrainPlatformMessageState
		
		CASE MISSION_MESSAGE_00
			IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_GO_TR", CONV_PRIORITY_VERY_HIGH)
				eTrainPlatformMessageState = MISSION_MESSAGE_01
			ENDIF
		BREAK
		
		// First piece of dialogue at start of the stage, then objective
		CASE MISSION_MESSAGE_01
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-157.908569,6137.305664,31.121231>>, <<-142.010498,6151.812988,36.249710>>, 6.000000)
			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				
				IF bIsBuddyAPro
					//IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_INFACT3", CONV_PRIORITY_HIGH)
					IF CREATE_GUNMAN_CONVERSATION("RBH_INFACT3", "", "RBH_RAD3_CH", "", "RBH_RAD3_PM")
						eTrainPlatformMessageState = MISSION_MESSAGE_11
					ENDIF
				ELSE
					
					IF CREATE_GUNMAN_CONVERSATION("", "RBH2A_NOGUND", "", "RBH2A_NOGUN", "")
					//IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_INFACT3B", CONV_PRIORITY_HIGH)
						eTrainPlatformMessageState = MISSION_MESSAGE_11
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		// Objective: Hold off the National Guard dialogue
		CASE MISSION_MESSAGE_11
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH_COMING", CONV_PRIORITY_HIGH)
				PLAY_SOUND_FRONTEND(-1, "PS2A_DISTANT_TRAIN_HORNS_MASTER")
					eTrainPlatformMessageState = MISSION_MESSAGE_02
				ENDIF
			ENDIF
		BREAK	
		
		
		// Objective: Hold off the National Guard god text
		CASE MISSION_MESSAGE_02
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				PRINT_NOW("GETRAIL2", DEFAULT_GOD_TEXT_TIME, 1)
				PLAY_SOUND_FRONTEND(-1, "PS2A_DISTANT_TRAIN_HORNS_MASTER")
				eTrainPlatformMessageState = MISSION_MESSAGE_IDLE
			ENDIF
		BREAK
		
		// Dialogue once the train arrives
		CASE MISSION_MESSAGE_03
			IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_TRAIN", CONV_PRIORITY_VERY_HIGH)
				eTrainPlatformMessageState = MISSION_MESSAGE_04
			ENDIF	
		BREAK
		
		CASE MISSION_MESSAGE_04
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				blipTrainPlatform = CREATE_BLIP_FOR_COORD(vBlipPlatformLocation)
				PRINT_NOW("GETTRAIN", DEFAULT_GOD_TEXT_TIME, 1)
				eTrainPlatformMessageState = MISSION_MESSAGE_05
			ENDIF
		BREAK
		
		// Dialogue when the train is about to leave
		CASE MISSION_MESSAGE_05
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF bAlternateTrainLine = FALSE	
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_JMPTRN", CONV_PRIORITY_HIGH)
						iGetOnTrainReminderTime = GET_GAME_TIMER()
						bAlternateTrainLine = TRUE
						eTrainPlatformMessageState = MISSION_MESSAGE_06
					ENDIF
				ELSE
					IF CREATE_CONVERSATION(myScriptedSpeech, "RBH2AUD", "RBH2A_TRAIN", CONV_PRIORITY_VERY_HIGH)
						bAlternateTrainLine = FALSE
						eTrainPlatformMessageState = MISSION_MESSAGE_06
					ENDIF
				ENDIF
			ENDIF	
		BREAK
		
		CASE MISSION_MESSAGE_06
			IF GET_GAME_TIMER() - iGetOnTrainReminderTime > 10000
				eTrainPlatformMessageState = MISSION_MESSAGE_05	
			ENDIF
		BREAK
		
		
		
	ENDSWITCH
ENDPROC


PROC UPDATE_TASKING_FRANKLIN()
	IF NOT bFranklinTaskedOnPlatform
		IF IS_PLAYER_IN_TRIGGER_BOX(tbOnTrainPlatform)
			IF NOT IS_PED_INJURED(pedFranklin())
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedFranklin(), TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFranklin(), FALSE)
				SET_PED_SPHERE_DEFENSIVE_AREA(pedFranklin(),  <<-148.5988, 6147.7378, 31.3352>>, 2.75, TRUE)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED_TIMED(pedFranklin(), 130.0, INFINITE_TASK_TIME)
				bFranklinTaskedOnPlatform = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CHOOSE_COVER_POINTS_FOR_THREE_CHARACTERS_INC_FRANKLIN(VECTOR vCoverPoint1, VECTOR vCoverPoint2, VECTOR vBackUpCoverPoint)

	IF IS_PLAYER_FRANKLIN()

		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCoverPoint2, <<1.25, 1.25, 4.0>>)
			IF DOES_ENTITY_EXIST(pedAIBuddy2)
			AND IS_ENTITY_AT_COORD(pedAIBuddy2, vCoverPoint1, <<1.25, 1.25, 4.0>>)
				SET_PED_SPHERE_DEFENSIVE_AREA(pedAIBuddy1, vBackUpCoverPoint, 4.0)
			ELSE
				SET_PED_SPHERE_DEFENSIVE_AREA(pedAIBuddy1, vCoverPoint1, 4.0)
			ENDIF				
		ELSE
			IF DOES_ENTITY_EXIST(pedAIBuddy1)
				SET_PED_SPHERE_DEFENSIVE_AREA(pedAIBuddy1, vCoverPoint2, 4.0)
			ENDIF
		ENDIF				

		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCoverPoint1, <<1.25, 1.25, 4.0>>)
			IF DOES_ENTITY_EXIST(pedAIBuddy1)
			AND IS_ENTITY_AT_COORD(pedAIBuddy1, vCoverPoint2, <<1.25, 1.25, 4.0>>)	
				IF DOES_ENTITY_EXIST(pedAIBuddy2)
					SET_PED_SPHERE_DEFENSIVE_AREA(pedAIBuddy2, vBackUpCoverPoint, 4.0)
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(pedAIBuddy2)
					SET_PED_SPHERE_DEFENSIVE_AREA(pedAIBuddy2, vCoverPoint2, 4.0)
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(pedAIBuddy2)
				SET_PED_SPHERE_DEFENSIVE_AREA(pedAIBuddy2, vCoverPoint1, 4.0)
			ENDIF
		ENDIF
	
	
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCoverPoint1, <<1.25, 1.25, 4.0>>)
			IF DOES_ENTITY_EXIST(pedAIBuddy1)
			AND IS_ENTITY_AT_COORD(pedAIBuddy1, vBackUpCoverPoint, <<1.25, 1.25, 4.0>>)	
				IF DOES_ENTITY_EXIST(pedAIBuddy2)
					SET_PED_SPHERE_DEFENSIVE_AREA(pedAIBuddy2, vCoverPoint1, 4.0)
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(pedFranklin())
					SET_PED_SPHERE_DEFENSIVE_AREA(pedFranklin(), vBackUpCoverPoint, 4.0)
				ENDIF
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(pedAIBuddy2)
				SET_PED_SPHERE_DEFENSIVE_AREA(pedAIBuddy2, vCoverPoint1, 4.0)
			ENDIF
		ENDIF
	
		
	
	ELSE
		
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vCoverPoint1, <<1.25, 1.25, 2.0>>)
			IF DOES_ENTITY_EXIST(pedAIBuddy1)
				SET_PED_SPHERE_DEFENSIVE_AREA(pedAIBuddy1, vCoverPoint2, 4.0)
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(pedAIBuddy1)
				SET_PED_SPHERE_DEFENSIVE_AREA(pedAIBuddy1, vCoverPoint1, 4.0)
			ENDIF
		ENDIF
		
		SET_PED_SPHERE_DEFENSIVE_AREA(pedFranklin(), vBackUpCoverPoint, 4.0)
	
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Checks if a given ped is currently hanging around where another given ped is trying to task to.
/// PARAMS:
///    pedToCheckForSteal - The ped that we want to check is stealing cover from our AI ped.
///    pedToMove - The AI ped that we want to move to the given destination.
///    vDestination - The destination
///    fMaxDistBeforeCheck - The moving ped must be within this distance of the destination before we start checking what the other ped is doing.
///    fMaxDistConsideredAsStealing - pedToCheckForSteal must be within this distance of the destination to be considered stealing it.
///    bPlayerMustBeInCover - If TRUE then the ped must be in cover for them to be considered in the way.  
FUNC BOOL IS_PED_STEALING_PEDS_DESTINATION(PED_INDEX pedToCheckForSteal, PED_INDEX pedToMove, VECTOR vDestination, FLOAT fMaxDistBeforeCheck, FLOAT fMaxDistConsideredAsStealing = 3.25, BOOL bMustBeInCover = FALSE)
      IF NOT IS_PED_INJURED(pedToMove)
      AND NOT IS_PED_INJURED(pedToCheckForSteal)
            VECTOR vPed1Pos = GET_ENTITY_COORDS(pedToCheckForSteal)
            VECTOR vPed2Pos = GET_ENTITY_COORDS(pedToMove)

            FLOAT fDistFromPed2ToDest = VDIST2(vPed2Pos, vDestination)
            FLOAT fDistFromPed1ToDest = VDIST2(vPed1Pos, vDestination)

            //NOTE: the values passed in are in real metres, they need to be converted to squared values for comparisons.
            IF fDistFromPed2ToDest < (fMaxDistBeforeCheck * fMaxDistBeforeCheck)
                  IF fDistFromPed1ToDest < fDistFromPed2ToDest
                  AND fDistFromPed1ToDest < (fMaxDistConsideredAsStealing * fMaxDistConsideredAsStealing)
                        IF bMustBeInCover
                              IF IS_PED_IN_COVER(pedToCheckForSteal)
                                    RETURN TRUE
                              ENDIF
                        ELSE
                              RETURN TRUE
                        ENDIF
                  ENDIF
            ENDIF
      ENDIF

      RETURN FALSE
ENDFUNC

///Finds a free destination for the ped given three different points. This is useful for selector ped shootouts: given three peds and three destinations it will find a spot 
///that isn't blocked by one of the other peds (or the player).
///    pedToMove - The ped we want to move to a new destination.
///    pedInTheWay1 - The first ped that we want to check for blocking any of the given destinations.
///    pedInTheWay2 - The second ped that we want to check for blocking any of the given destinations.
///    vBuddyDest1 - The current destination of the first buddy ped.
///    vBuddyDest2 - The current destination of the second buddy ped.
///    vCurrentDest - The current destination for this ped: this will be checked first, if it's not being stolen then there's no need to look for a new point.
///    vDest1 - The first potential destination to check.
///    vDest2 - The second potential destination to check.
///    vDest3 - The third potential destination to check.
FUNC VECTOR GET_FREE_DESTINATION_POINT_FOR_PED(PED_INDEX pedToMove, PED_INDEX pedBuddy1, PED_INDEX pedBuddy2, VECTOR vBuddyDest1, VECTOR vBuddyDest2, VECTOR vCurrentDest, VECTOR vDest1, VECTOR vDest2, VECTOR vDest3)
      IF NOT ARE_VECTORS_ALMOST_EQUAL(vCurrentDest, <<0.0, 0.0, 0.0>>)
            IF NOT IS_PED_STEALING_PEDS_DESTINATION(pedBuddy1, pedToMove, vCurrentDest, 5.0, 3.0)
            AND NOT IS_PED_STEALING_PEDS_DESTINATION(pedBuddy2, pedToMove, vCurrentDest, 5.0, 3.0)
                  RETURN vCurrentDest
            ENDIF
      ENDIF 

      IF NOT ARE_VECTORS_ALMOST_EQUAL(vCurrentDest, vDest1)
            //First check that another buddy isn't already using this destination.
            IF NOT ARE_VECTORS_ALMOST_EQUAL(vDest1, vBuddyDest1)
            AND NOT ARE_VECTORS_ALMOST_EQUAL(vDest1, vBuddyDest2)
                  //Now check that one of the other buddies isn't closer to this destination.
                  IF NOT IS_PED_STEALING_PEDS_DESTINATION(pedBuddy1, pedToMove, vDest1, 5.0, 3.0)
                  AND NOT IS_PED_STEALING_PEDS_DESTINATION(pedBuddy2, pedToMove, vDest1, 5.0, 3.0)
                        RETURN vDest1
                  ENDIF
            ENDIF
      ENDIF
      
      IF NOT ARE_VECTORS_ALMOST_EQUAL(vCurrentDest, vDest2)
            IF NOT ARE_VECTORS_ALMOST_EQUAL(vDest2, vBuddyDest1)
            AND NOT ARE_VECTORS_ALMOST_EQUAL(vDest2, vBuddyDest2)
                  IF NOT IS_PED_STEALING_PEDS_DESTINATION(pedBuddy1, pedToMove, vDest2, 5.0, 3.0)
                  AND NOT IS_PED_STEALING_PEDS_DESTINATION(pedBuddy2, pedToMove, vDest2, 5.0, 3.0)
                        RETURN vDest2
                  ENDIF
            ENDIF
      ENDIF
      
      IF NOT ARE_VECTORS_ALMOST_EQUAL(vCurrentDest, vDest3)
            IF NOT ARE_VECTORS_ALMOST_EQUAL(vDest3, vBuddyDest1)
            AND NOT ARE_VECTORS_ALMOST_EQUAL(vDest3, vBuddyDest2)
                  IF NOT IS_PED_STEALING_PEDS_DESTINATION(pedBuddy1, pedToMove, vDest3, 5.0, 3.0)
                  AND NOT IS_PED_STEALING_PEDS_DESTINATION(pedBuddy2, pedToMove, vDest3, 5.0, 3.0)
                        RETURN vDest3
                  ENDIF
            ENDIF
      ENDIF
      
      //As a failsafe return the first choice.
      RETURN vDest1
ENDFUNC


VECTOR vPossibleCoverPoints[3]
BOOL bCoverTaken[3]
BOOL bCoverStolen[3]

FUNC VECTOR FREE_COVER_POINT()

	INT i
	PRINTNL()
	FOR i = 0 TO 2
		PRINTLN("i:", i, "stolen:", bCoverStolen[i], "taken: ", bCoverTaken[i])
		IF bCoverStolen[i] = FALSE
		AND bCoverTaken[i] = FALSE
			//bCoverTaken[i] = TRUE
			PRINTLN("Returning: ", i, "vec:",vPossibleCoverPoints[i])
			RETURN vPossibleCoverPoints[i]
		ENDIF
	ENDFOR
	
	RETURN <<0.0, 0.0, 0.0>>

ENDFUNC

FUNC INT FREE_COVER_POINT_INDEX()

	INT i
	PRINTNL()
	FOR i = 0 TO 2
		PRINTLN("i:", i, "stolen:", bCoverStolen[i], "taken: ", bCoverTaken[i])
		IF bCoverStolen[i] = FALSE
		AND bCoverTaken[i] = FALSE
			//bCoverTaken[i] = TRUE
			PRINTLN("Returning: ", i, "vec:",vPossibleCoverPoints[i])
			RETURN i
		ENDIF
	ENDFOR
	
	RETURN -1 //<<0.0, 0.0, 0.0>>

ENDFUNC

INT iCoverUpdateTimer

PROC UPDATE_COVER_POINTS()

	INT i

	//VECTOR vDefArea
	INT iDefArea
	//FLOAT fCoverAreaSize = 4.0

	FOR i = 0 TO 2
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), 	vPossibleCoverPoints[i], <<2.0, 2.0, 2.0>>)
			bCoverStolen[i] = TRUE
			//bCoverTaken[i] = FALSE
		ELSE
			bCoverStolen[i] = FALSE
		ENDIF
	ENDFOR
	
	FOR i = 0 TO 2
		IF NOT IS_ENTITY_DEAD(pedAIBuddy1)
		AND NOT IS_ENTITY_DEAD(pedAIBuddy2)
		AND NOT IS_ENTITY_DEAD(pedFranklin())
			IF IS_ENTITY_AT_COORD(pedAIBuddy1, 	vPossibleCoverPoints[i], <<2.0, 2.0, 2.0>>)
			OR IS_ENTITY_AT_COORD(pedAIBuddy2, 	vPossibleCoverPoints[i], <<2.0, 2.0, 2.0>>)
			OR IS_ENTITY_AT_COORD(pedFranklin(), 	vPossibleCoverPoints[i], <<2.0, 2.0, 2.0>>)
			//OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), 	vPossibleCoverPoints[i], <<2.0, 2.0, 2.0>>)
				bCoverTaken[i] = TRUE
			ELSE
				bCoverTaken[i] = FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	IF GET_GAME_TIMER() - iCoverUpdateTimer > 1000
	
		FOR i = 0 TO 2
			IF NOT IS_ENTITY_DEAD(pedAIBuddy1)
				IF IS_ENTITY_AT_COORD(pedAIBuddy1,vPossibleCoverPoints[i], <<2.0, 2.0, 2.0>>)
				AND bCoverStolen[i] = TRUE
					//vDefArea = FREE_COVER_POINT()
					
					iDefArea = FREE_COVER_POINT_INDEX()
					
//					IF VMAG(vDefArea) > 0.0
//						SET_PED_SPHERE_DEFENSIVE_AREA(pedAIBuddy1, vDefArea, fCoverAreaSize)
					IF iDefArea <> - 1
						IF iDefArea = 0
							SET_PED_ANGLED_DEFENSIVE_AREA(pedAIBuddy1, <<-154.295975,6145.266113,31.335117>>, <<-157.023438,6142.633789,33.835121>>, 2.750000)
						ELIF iDefArea = 1
							SET_PED_ANGLED_DEFENSIVE_AREA(pedAIBuddy1, <<-150.592117,6146.632324,31.335106>>, <<-147.666565,6149.532227,33.835106>>, 2.750000)
						ELIF iDefArea = 2
							SET_PED_ANGLED_DEFENSIVE_AREA(pedAIBuddy1, <<-155.375732,6145.769531,31.335167>>, <<-153.846924,6147.230957,33.649773>>, 1.750000)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedAIBuddy1)
				IF IS_ENTITY_AT_COORD(pedAIBuddy2,vPossibleCoverPoints[i], <<2.0, 2.0, 2.0>>)
				AND bCoverStolen[i] = TRUE
					//vDefArea = FREE_COVER_POINT()
					//IF VMAG(vDefArea) > 0.0
						//SET_PED_SPHERE_DEFENSIVE_AREA(pedAIBuddy2, vDefArea, fCoverAreaSize)
						
					iDefArea = FREE_COVER_POINT_INDEX()
					IF iDefArea <> - 1	
						IF iDefArea = 0
							SET_PED_ANGLED_DEFENSIVE_AREA(pedAIBuddy2, <<-154.295975,6145.266113,31.335117>>, <<-157.023438,6142.633789,33.835121>>, 2.750000)
						ELIF iDefArea = 1
							SET_PED_ANGLED_DEFENSIVE_AREA(pedAIBuddy2, <<-150.592117,6146.632324,31.335106>>, <<-147.666565,6149.532227,33.835106>>, 2.750000)
						ELIF iDefArea = 2
							SET_PED_ANGLED_DEFENSIVE_AREA(pedAIBuddy2, <<-155.375732,6145.769531,31.335167>>, <<-153.846924,6147.230957,33.649773>>, 1.750000)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedFranklin())
				IF IS_ENTITY_AT_COORD(pedFranklin(),vPossibleCoverPoints[i], <<2.0, 2.0, 2.0>>)
				AND bCoverStolen[i] = TRUE
//					vDefArea = FREE_COVER_POINT()
//					IF VMAG(vDefArea) > 0.0
//						SET_PED_SPHERE_DEFENSIVE_AREA(pedFranklin(), vDefArea, 2.0)

					iDefArea = FREE_COVER_POINT_INDEX()
					IF iDefArea <> - 1
						IF iDefArea = 0
							SET_PED_ANGLED_DEFENSIVE_AREA(pedFranklin(), <<-154.295975,6145.266113,31.335117>>, <<-157.023438,6142.633789,33.835121>>, 2.750000)
						ELIF iDefArea = 1
							SET_PED_ANGLED_DEFENSIVE_AREA(pedFranklin(), <<-150.592117,6146.632324,31.335106>>, <<-147.666565,6149.532227,33.835106>>, 2.750000)
						ELIF iDefArea = 2
							SET_PED_ANGLED_DEFENSIVE_AREA(pedFranklin(), <<-155.375732,6145.769531,31.335167>>, <<-153.846924,6147.230957,33.649773>>, 1.750000)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDFOR
	
		iCoverUpdateTimer = GET_GAME_TIMER()
	
	ENDIF
	
ENDPROC

PROC stageTrainPlatform()

	vPossibleCoverPoints[0] = <<-154.9355, 6142.9629, 31.3351>>
	vPossibleCoverPoints[1] = <<-148.9717, 6147.4966, 31.3351>>
	vPossibleCoverPoints[2] = <<-154.8556, 6146.7222, 31.3352>>

	PROCESS_MICHAEL_TREVOR_SWITCH_CHICKEN_FACTORY()
	
	UPDATE_AI_PED_BLIP_ARRAY(swatStage6, blipSwatStage6)
	
	//IF NOT IS_CUTSCENE_ACTIVE()
	//AND eTrainState <= TRAIN_PLATFORM_STATE_WAIT_FOR_TRAIN_TO_PASS
		doBuddyFailCheckStageChickenFactory()
	//ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-178.815399,6167.368164,30.155563>>, <<-183.615173,6162.586914,36.410988>>, 9.500000)
		MISSION_FLOW_SET_FAIL_REASON("ALLCREWLEFT")
		Mission_Failed()
	ENDIF
	
	IF eTrainState > TRAIN_PLATFORM_STATE_INIT
	AND NOT IS_CUTSCENE_PLAYING()
		UPDATE_TRAIN_PLATFORM_CONVERSATIONS()
	ENDIF

	//UPDATE_COVER_POINTS()
	
	SWITCH eTrainState
		CASE TRAIN_PLATFORM_STATE_INIT
			// Request the train assets
			
			REQUEST_AMBIENT_AUDIO_BANK("PS2A_CHICKEN_FACTORY")
						
			REQUEST_TRAIN_MODELS()
			START_AUDIO_SCENE("PS_2A_SHOOTOUT_WAREHOUSE")
			iNumberOfSoldiersToHoldOff = iNumberOfSoldiersToHoldOff
			SET_RANDOM_TRAINS(FALSE)
			DELETE_ARRAY_OF_PEDS(chickenPed)
			DELETE_ARRAY_OF_PEDS(pedFactoryWorker)
			
			SET_VEHICLE_AS_NO_LONGER_NEEDED(carBulldozer)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(EnemyTank.thisCar)
			DELETE_ARRAY_OF_PEDS(pedFactoryWorker)
			DELETE_ARRAY_OF_PEDS(chickenPed)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(A_C_HEN)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_F_Y_FACTORY_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(RHINO)
			SET_MODEL_AS_NO_LONGER_NEEDED(BULLDOZER)
			
			REMOVE_ANIM_SET("move_f@scared")
			REMOVE_PTFX_ASSET()
			SET_MODEL_AS_NO_LONGER_NEEDED(mnCopPedModel)
			TRIGGER_MUSIC_EVENT("RH2A_PLATFORM")
			
			IF IS_ENTITY_OK(pedTrevor())
				GIVE_WEAPON_TO_PED(pedTrevor(), WEAPONTYPE_ASSAULTRIFLE, 1500, TRUE)			
				SET_PED_ACCURACY(pedTrevor(), 4)
				SET_PED_SPHERE_DEFENSIVE_AREA(pedTrevor(),   <<-154.8010, 6143.1074, 31.3351>>, 2.75, TRUE)
			ENDIF
			
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(15, "STAGE_TRAIN_PLATFORM")
			//SET_PED_RUN_INTO_POSITION_AND_COMBAT(pedTrevor(),   <<-154.8010, 6143.1074, 31.3351>>, FALSE, 0, PEDMOVE_RUN, FALSE, FALSE, 3.0, FALSE)
			
			pedAIBuddy1 = pedTrevor()
			pedAIBuddy2 = PedMichael()
						
			IF NOT IS_PLAYER_TREVOR()
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTrevor(), FALSE)
				SET_PED_SPHERE_DEFENSIVE_AREA(pedTrevor(), vPossibleCoverPoints[0], 3.0, TRUE)
				TASK_COMBAT_HATED_TARGETS_IN_AREA(pedTrevor(), <<-154.8010, 6143.1074, 31.3351>>, 100.0)
				
				SET_PED_STEERS_AROUND_DEAD_BODIES(pedTrevor(), FALSE)
				
			ENDIF
			IF NOT IS_PLAYER_MICHAEL()
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PedMichael(), FALSE)
				SET_PED_SPHERE_DEFENSIVE_AREA(PedMichael(), vPossibleCoverPoints[1], 3.0, TRUE)
				TASK_COMBAT_HATED_TARGETS_IN_AREA(PedMichael(), <<-154.8010, 6143.1074, 31.3351>>, 100.0)				
				
				SET_PED_STEERS_AROUND_DEAD_BODIES(PedMichael(), FALSE)
				
			ENDIF
			IF NOT IS_PLAYER_FRANKLIN()
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFranklin(), FALSE)
				SET_PED_SPHERE_DEFENSIVE_AREA(pedFranklin(), vPossibleCoverPoints[2], 2.0, TRUE)
				TASK_COMBAT_HATED_TARGETS_IN_AREA(pedFranklin(), <<-154.8010, 6143.1074, 31.3351>>, 100.0)
				
				SET_PED_STEERS_AROUND_DEAD_BODIES(pedFranklin(), FALSE)
				
			ENDIF
			
			IF bIsBuddyAPro
				iNumberOfSoldiersToSpawnTrain = 15 //12
			ELSE
				iNumberOfSoldiersToSpawnTrain = 30 //20
			ENDIF
			
			bFranklinTaskedOnPlatform = FALSE
			//bTrainPlatformWarningPlayed = FALSE
			iNumberOfSoldiersSpawned = 0
			iNumberOfSoldiersToHoldOff = 20
			
			REQUEST_CUTSCENE("RBH_2A_MCS_5")			
			
			FADE_IN_IF_NEEDED()
			SETTIMERA(0)
			
//			IF bIsBuddyAPro
//				INCREMENT_GUNMAN_STATS_DURING_HEIST(HEIST_RURAL_BANK, GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_RURAL_BANK))
//			ENDIF
			
			eTrainPlatformMessageState = MISSION_MESSAGE_00
			eTrainPlatformCutscene = TRAIN_PLATFORM_CUTSCENE_WAIT_FOR_STREAMING
			eTrainState = TRAIN_PLATFORM_STATE_WAIT_FOR_STREAMING
			CPRINTLN(DEBUG_MISSION, "stageTrainPlatform - Going to TRAIN_PLATFORM_STATE_WAIT_FOR_STREAMING")
		BREAK
		
		CASE TRAIN_PLATFORM_STATE_WAIT_FOR_STREAMING
			
			//Fucking 2014912
//			IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL
//				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())	
//				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", pedFranklin())	
//				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", pedTrevor())	
//			ELIF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_TREVOR
//				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", pedMichael())	
//				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", pedFranklin())	
//				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID())
//			ELIF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_FRANKLIN
//				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", pedSelector.pedID[SELECTOR_PED_MICHAEL] )	
//				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", PLAYER_PED_ID())	
//				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", pedTrevor())
//			ENDIF

			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", INT_TO_ENUM(PED_COMPONENT,0), 0, 0) //(head)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", INT_TO_ENUM(PED_COMPONENT,1), 0, 0) //(berd)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", INT_TO_ENUM(PED_COMPONENT,2), 0, 0) //(hair)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", INT_TO_ENUM(PED_COMPONENT,3), 5, 0) //(uppr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", INT_TO_ENUM(PED_COMPONENT,4), 5, 0) //(lowr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", INT_TO_ENUM(PED_COMPONENT,5), 1, 0) //(hand)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", INT_TO_ENUM(PED_COMPONENT,6), 1, 0) //(feet)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", INT_TO_ENUM(PED_COMPONENT,7), 0, 0) //(teef)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", INT_TO_ENUM(PED_COMPONENT,8), 5, 0) //(accs)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", INT_TO_ENUM(PED_COMPONENT,9), 1, 0) //(task)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", INT_TO_ENUM(PED_COMPONENT,10), 0, 0) //(decl)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", INT_TO_ENUM(PED_COMPONENT,11), 0, 0) //(jbib)
			SET_CUTSCENE_PED_PROP_VARIATION("Michael", INT_TO_ENUM(PED_PROP_POSITION,0), 26)
			SET_CUTSCENE_PED_PROP_VARIATION("Michael", INT_TO_ENUM(PED_PROP_POSITION,2), 0)
									
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Franklin", INT_TO_ENUM(PED_COMPONENT,0), 0, 0) //(head)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Franklin", INT_TO_ENUM(PED_COMPONENT,1), 0, 0) //(berd)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Franklin", INT_TO_ENUM(PED_COMPONENT,2), 0, 0) //(hair)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Franklin", INT_TO_ENUM(PED_COMPONENT,3), 16, 0) //(uppr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Franklin", INT_TO_ENUM(PED_COMPONENT,4), 16, 0) //(lowr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Franklin", INT_TO_ENUM(PED_COMPONENT,5), 4, 0) //(hand)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Franklin", INT_TO_ENUM(PED_COMPONENT,6), 5, 0) //(feet)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Franklin", INT_TO_ENUM(PED_COMPONENT,7), 0, 0) //(teef)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Franklin", INT_TO_ENUM(PED_COMPONENT,8), 14, 0) //(accs)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Franklin", INT_TO_ENUM(PED_COMPONENT,9), 0, 0) //(task)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Franklin", INT_TO_ENUM(PED_COMPONENT,10), 0, 0) //(decl)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Franklin", INT_TO_ENUM(PED_COMPONENT,11), 0, 0) //(jbib)
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", INT_TO_ENUM(PED_COMPONENT,0), 0, 0) //(head)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", INT_TO_ENUM(PED_COMPONENT,1), 0, 0) //(berd)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", INT_TO_ENUM(PED_COMPONENT,2), 0, 0) //(hair)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", INT_TO_ENUM(PED_COMPONENT,3), 2, 0) //(uppr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", INT_TO_ENUM(PED_COMPONENT,4), 2, 0) //(lowr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", INT_TO_ENUM(PED_COMPONENT,5), 1, 0) //(hand)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", INT_TO_ENUM(PED_COMPONENT,6), 1, 0) //(feet)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", INT_TO_ENUM(PED_COMPONENT,7), 0, 0) //(teef)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", INT_TO_ENUM(PED_COMPONENT,8), 2, 0) //(accs)
//			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", INT_TO_ENUM(PED_COMPONENT,9), 1, 0) //(task)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", INT_TO_ENUM(PED_COMPONENT,10), 0, 0) //(decl)
			SET_CUTSCENE_PED_PROP_VARIATION("Trevor", INT_TO_ENUM(PED_PROP_POSITION,0), 24)
			
			//The bags are animated so make sure these are off.
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", INT_TO_ENUM(PED_COMPONENT,9), 0, 0) //(task)		
			SET_CUTSCENE_PED_COMPONENT_VARIATION("Trevor", INT_TO_ENUM(PED_COMPONENT,9), 0, 0) //(task)
		
			
			UPDATE_TASKING_FRANKLIN()
			// Wait for the train assets to stream
			IF HAVE_TRAIN_MODELS_LOADED()
			AND HAS_CUTSCENE_LOADED()
				eTrainState = TRAIN_PLATFORM_STATE_WAIT_TO_START
				CPRINTLN(DEBUG_MISSION, "stageTrainPlatform - Going to TRAIN_PLATFORM_STATE_WAIT_TO_START")
			ELSE
				CPRINTLN(DEBUG_MISSION, "stageTrainPlatform - Waiting for train models to load")
			ENDIF
		BREAK
		
		CASE TRAIN_PLATFORM_STATE_WAIT_TO_START
			UPDATE_PLATFORM_ENEMY_WAVES()
			UPDATE_TASKING_FRANKLIN()
			CONTROL_PLATFORM_DIALOGUE()
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-157.908569,6137.305664,31.121231>>, <<-142.010498,6151.812988,36.249710>>, 6.000000)
				REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipInsideFactory)
			ENDIF
			
			// Wait for a certain number of dudes to die, then begin the train
			IF iNumberOfSoldiersSpawned > iNumberOfSoldiersToSpawnTrain
			AND NOT IS_SPHERE_VISIBLE(<<-167.2592, 6115.0825, 31.4414>>, 0.5)
			
				//PLAY_SOUND_FROM_COORD(-1, "PS2A_DISTANT_TRAIN_HORNS_MASTER",  <<-144.0582, 6143.7012, 31.3351>> )
			
				PLAY_SOUND_FRONTEND(-1, "PS2A_DISTANT_TRAIN_HORNS_MASTER")
						
				START_AUDIO_SCENE("PS_2A_TRAIN_ARRIVES")
				
				SET_RANDOM_TRAINS(FALSE) 
				chickenTrain = CREATE_MISSION_TRAIN(17, <<-167.2592, 6115.0825, 31.4414>>, TRUE) 
				
				SET_ENTITY_INVINCIBLE(chickenTrain, TRUE)
				
				SET_TRAIN_CRUISE_SPEED(chickenTrain, flRAGTrainCruiseSpeed)
				SET_TRAIN_SPEED(chickenTrain, flRAGTrainSpeed)
				
				START_VEHICLE_HORN(chickenTrain, 10000)
				
				RESTART_TIMER_NOW(tmrTrainController)
				eTrainPlatformMessageState = MISSION_MESSAGE_03		// PLAY DIALOGUE OF THE TRAIN ARRIVING
				eTrainState = TRAIN_PLATFORM_STATE_WAIT_FOR_TRAIN_TO_PASS
				CPRINTLN(DEBUG_MISSION, "stageTrainPlatform - Going to TRAIN_PLATFORM_STATE_WAIT_FOR_TRAIN_TO_PASS")
			ENDIF
		BREAK
		
		CASE TRAIN_PLATFORM_STATE_WAIT_FOR_TRAIN_TO_PASS
			UPDATE_PLATFORM_ENEMY_WAVES()
			UPDATE_TASKING_FRANKLIN()
			
			
			CPRINTLN(DEBUG_MISSION, "tmrTrainController elapsed train time is: ", (GetGameTimerSeconds() - tmrTrainController.StartTime))
			
			// Fail if we've been on the platform for too long
			//DUMMY_REFERENCE_FLOAT(flRAGTimeToFailPlatform)
			//IF GET_TIMER_IN_SECONDS(tmrTrainController) >= flRAGTimeToFailPlatform
			
			IF DOES_BLIP_EXIST(blipTrainPlatform)
				IF NOT IS_AREA_OCCUPIED(<<-153.657547,6131.889648,29.414141>>, <<-132.937073,6155.973633,40.317234>>, FALSE, TRUe, FALSE, FALSE, FALSE)
				
					REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipTrainPlatform)
					eTrainState = TRAIN_PLATFORM_STATE_FAIL
					CPRINTLN(DEBUG_MISSION, "stageTrainPlatform - Going to TRAIN_PLATFORM_STATE_FAIL")
				ENDIF
			ELSE
				CONTROL_PLATFORM_DIALOGUE()
			ENDIF
			
			// Succeed if the train's still going, and we approach the platform
			IF DOES_BLIP_EXIST(blipTrainPlatform)
				IF IS_PLAYER_IN_TRIGGER_BOX(tbTrainJump)
					IF NOT IS_PED_INJURED(pedTrevor())
						SET_PED_DROPS_WEAPON(pedTrevor())
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTrevor(), TRUE)
					ENDIF
					
					IF NOT IS_PED_INJURED(pedFranklin())
						SET_PED_DROPS_WEAPON(pedFranklin())
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFranklin(), TRUE)
					ENDIF
					
					REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipTrainPlatform)
					eTrainState = TRAIN_PLATFORM_STATE_SUCCESS_CUTSCENE
					CPRINTLN(DEBUG_MISSION, "stageTrainPlatform - Going to TRAIN_PLATFORM_STATE_SUCCESS_CUTSCENE")
				ENDIF
			ENDIF
			
			// Play a warning if the player hasn't gotten on the train yet.
			//DUMMY_REFERENCE_FLOAT(flRAGTimeToWarnTrain)
//			IF NOT bTrainPlatformWarningPlayed
//				IF GET_TIMER_IN_SECONDS(tmrTrainController) >= flRAGTimeToWarnTrain
//				AND DOES_BLIP_EXIST(blipTrainPlatform)
//					eTrainPlatformMessageState = MISSION_MESSAGE_05
//					RESTART_TIMER_NOW(tmrTrainController)
//					//bTrainPlatformWarningPlayed = TRUE
//				ENDIF
//			ENDIF
		BREAK
		
		CASE TRAIN_PLATFORM_STATE_SUCCESS_CUTSCENE
			IF IS_TRAIN_PLATFORM_CUTSCENE_DONE()
				CLEANUP_SET_PIECE_COP_CAR(EnemyTank, TRUE)
				mission_stage = STAGE_END_MOCAP_SCENE
				eTrainState = TRAIN_PLATFORM_STATE_DONE
				CPRINTLN(DEBUG_MISSION, "stageTrainPlatform - Going to TRAIN_PLATFORM_STATE_DONE")
			ENDIF
		BREAK
		
		CASE TRAIN_PLATFORM_STATE_FAIL
			MISSION_FLOW_SET_FAIL_REASON("RBH_TRNFAIL")
			Mission_Failed()
			eTrainState = TRAIN_PLATFORM_STATE_DONE
			CPRINTLN(DEBUG_MISSION, "stageTrainPlatform - Going to TRAIN_PLATFORM_STATE_DONE")
		BREAK
		
		CASE TRAIN_PLATFORM_STATE_DONE
			// Do nothing
		BREAK
	ENDSWITCH
	
ENDPROC
