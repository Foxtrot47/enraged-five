//╔═════════════════════════════════════════════════════════════════════════════╗
//║																				║
//║ 		Author: Michael Wadelin				 Date: 	09/01/2012				║
//║																				║
//║			Prev:																║
//║																				║
//╠═════════════════════════════════════════════════════════════════════════════╣
//║ 																			║
//║ 		The Agency Heist 3B Finale - Steal FIB Docs(Helicopter)				║
//║																				║
//║ 		Michael, Franklin, a Gunman and a Hacker infiltrate the Agency HQ,	║
//║			download and steal sensitive documents.								║
//║ 																			║
//║ 		Location: 	Helipad, The FIB building								║
//║ 		Notes:  															║
//║ 																			║
//╚═════════════════════════════════════════════════════════════════════════════╝
 
 
/*
											 _     _  _______  _______  ______   _______  ______    ______ 
											(_)   (_)(_______)(_______)(______) (_______)(_____ \  / _____)
											 _______  _____    _______  _     _  _____    _____) )( (____  
											|  ___  ||  ___)  |  ___  || |   | ||  ___)  |  __  /  \____ \ 
											| |   | || |_____ | |   | || |__/ / | |_____ | |  \ \  _____) )
											|_|   |_||_______)|_|   |_||_____/  |_______)|_|   |_|(______/ 
*/
     
USING "rage_builtins.sch"
USING "globals.sch"

USING "flow_public_core_override.sch"
USING "select_mission_stage.sch"
USING "replay_public.sch"
USING "CompletionPercentage_public.sch"

USING "selector_public.sch"
USING "locates_public.sch"
USING "script_heist.sch"
USING "chase_hint_cam.sch"

USING "commands_script.sch"
USING "commands_task.sch"
USING "commands_itemsets.sch"
USING "commands_graphics.sch"

USING "taxi_functions.sch"
USING "hacking_public.sch"
USING "rappel_public.sch"
USING "shared_hud_displays.sch"

USING "mission_stat_public.sch"
USING "building_control_public.sch"

USING "asset_management_public.sch"
USING "TimeLapse.sch"
USING "heist_end_screen.sch"
USING "script_ped.sch"
USING "achievement_public.sch"
USING "commands_recording.sch"

/* 
											 _     _  _______  ______   _  _______  ______   _        _______   ______ 
											(_)   (_)(_______)(_____ \ | |(_______)(____  \ (_)      (_______) / _____)
											 _     _  _______  _____) )| | _______  ____)  ) _        _____   ( (____  
											| |   | ||  ___  ||  __  / | ||  ___  ||  __  ( | |      |  ___)   \____ \ 
											 \ \ / / | |   | || |  \ \ | || |   | || |__)  )| |_____ | |_____  _____) )
											  \___/  |_|   |_||_|   |_||_||_|   |_||______/ |_______)|_______)(______/
*/
	
// Enumerators
//---------------------------------------------------------------------------------------------------------------------------------------------------

	// An enum for all the different stages of the mission
	ENUM MISSION_STAGE_FLAG
		msf_0_intro_cut,
		msf_1_go_to_helipad,
		msf_2_fly_to_jump_zone,
		msf_3_sky_dive_and_land,
		msf_4_hack_the_computer,
		msf_5_download_shootout,
		msf_6_stairwell,
		msf_7_burnt_out_floor_1,
		msf_8_burnt_out_floor_2,
		msf_9_abseil_down,
		msf_10_get_to_getaway_veh,
		msf_11_escape_to_franklins,
		msf_12_mission_passed,
		msf_num_stages
	ENDENUM

	// Enum used for mission_stage management
	ENUM STAGE_SWITCHSTATE
		STAGESWITCH_IDLE,
		STAGESWITCH_REQUESTED,
		STAGESWITCH_EXITING,
		STAGESWITCH_ENTERING
	ENDENUM

	// Hot swap status
	enum SWITCHSTATE
		SWITCH_NOSTATE,
		SWITCH_COMPLETE,
		SWITCH_IN_PROGRESS
	ENDENUM

	// All possible fail conditions
	ENUM MISSION_FAIL_FLAG
		mff_debug_forced,
		// Crew deaths
		mff_mike_dead,
		mff_frank_dead,
		mff_hacker_dead,
		mff_goon_dead,
		mff_driver_dead,
		mff_pilot_dead,
		mff_lester_dead,
		
		// Vehicle deaths
		mff_heli_destroyed,
		mff_heli_stuck,
		mff_heli_unable_to_takeoff,
		mff_getaway_van_destroyed,
		mff_getaway_van_stuck,
		 
		// Parachute fails
		mff_para_deployed_late_mike,
		mff_para_landing_missed_mike,
		mff_para_landing_missed_frank,
		mff_para_landing_missed_gunman,
		
		// Abandoned fails
		mff_abandoned_mike,
		mff_abandoned_frank,
		mff_abandoned_gunman,
		mff_abandoned_pilot,
		mff_abandoned_driver,
		mff_crew_abandoned,
		
		// Misc
		mff_comp_destroyed,
		mff_hack_phone_destroyed,
		mff_out_of_explosives,
		mff_spotted,
		mff_bought_police_to_base,
		
		mff_default
	ENDENUM

	// List of key events in the mission
	ENUM MISSION_EVENT_FLAG
		mef_manage_radar_map,
		mef_manage_sprinklers,
		mef_manage_clock,
		mef_manage_smoke_vfx,
		mef_manage_fire_meshes,
	
		mef_manage_parachuting,
		mef_manage_parachuting_buddies,
		mef_manage_parachute_marker,
		
		mef_manage_switch,
			
		mef_manage_shootout_doors,
		mef_manage_download_shootout_spawning,
		mef_manage_download_shootout_rooftop,
		mef_post_heli_crash_enemies,						// Agents directly after the crash cutscene
		mef_gas_grenades,
		
		// IG Animations
		mef_ig2_try_door,				
		mef_ig3_swat_takedown,		
		mef_ig4_kick_door,
		mef_ig5_push_door,
		mef_ig7_elevator,			
		mef_ig8_paramedic,
		mef_ig9_dismount_player,
		mef_ig9_dismount_buddies,
		
		mef_pre_crashed_heli_corridor,
		mef_comms_2,
		mef_rappel_room,
		
		mef_bodies,
		
		mef_leadout_MCS_2,
		
		mef_heli_crash_cutscene,
		mef_heli_to_heli_missile,
		
		mef_heli_rpl_flyby,
		mef_heli_rpl_attack,
		mef_heli_news,
		
		mef_emergency_services,
		mef_getaway_driver_arrival,
		mef_swat_fib_arrival,
		mef_manage_wanted_level,
		
		// agents outside the building after you reach the ground
		mef_ground_agents,
		
		mef_kill_sprinklers,

		mef_num_events
	ENDENUM
	
		// List of synced scenes
	ENUM SYNCED_SCENE_FLAGS
		ssf_IG1_sky_dive,
		ssf_IG2_try_door,
		ssf_IG3_killswat,
		ssf_IG4_door_kick_goon,
		ssf_IG5_push_door_frank,
		ssf_IG6_rubble_slide,
		ssf_IG7_elevator,
		ssf_IG8_paramedic,
		ssf_IG8_agents,
		ssf_IG9_rappel_dismount_player,
		ssf_IG9_rappel_dismount_buddies,
		ssf_IG10_rappel_small,
		ssf_leadout_MCS_2,
		ssf_MCS6_dead_pilot,
		ssf_helicrash,
		ssf_helicrashCam,
		ssf_doorblownoff,
		ssf_gas_grenade,
		ssf_num_scenes
	ENDENUM
	
	// List of all the doors
	ENUM DOOR_FLAGS
		mdf_invalid	= -1,
//		mdf_hack_room,
		mdf_topfloor_main_l,
		mdf_topfloor_main_r,
		mdf_topfloor_fake_l,
		mdf_topfloor_fake_r,
		mdf_topfloor_fake_c,
		mdf_topfloor_pre_stairs,
		mdf_stairwell_53,
		mdf_stairwell_52,
		mdf_stairwell_47,
		mdf_burntout1_pre_stairs,
		mdf_burntout1_entrance_l,
		mdf_burntout1_entrance_r,
		mdf_burntout2_double_l,
		mdf_burntout2_double_r,
		mdf_burntout2_51_stairwell,
		mdf_burntout2_50_stairwell,
		mdf_boardroom_l,
//		mdf_goverment_facil_l,
//		mdf_goverment_facil_r,
		mdf_helipad,
		mdf_gov_building_gate,
		
		mdf_num_doors
	ENDENUM

	// List of peds used in the mission
	ENUM MISSION_PED_FLAGS
		mpf_mike,
		mpf_frank,
		mpf_lester,
		mpf_goon,
		mpf_driver,
		mpf_pilot,
		mpf_hacker,
		
		mpf_sec_guard_1,
		mpf_sec_guard_2,
		mpf_sec_guard_3,
		
		// download shootout guys
		mpf_download_1,
		mpf_download_2,
		mpf_download_3,
		mpf_download_4,
		mpf_download_5,
		mpf_download_6,
		mpf_download_7,
		mpf_download_8,
		mpf_download_9,
		mpf_download_10,
		mpf_download_11,
		mpf_download_12,
		mpf_download_13,
		mpf_download_14,
		mpf_download_15,
		mpf_download_16,
		mpf_download_17,
		mpf_download_18,
		mpf_download_19,
		mpf_download_20,
		mpf_download_21,
		mpf_download_22,
		mpf_download_23,
		mpf_download_24,
		
		mpf_gas_roof_1,
		
		// more agents directly after the cutscene, already in the room
		mpf_post_heli_crash_1,
		mpf_post_heli_crash_2,
		mpf_post_heli_crash_3,
		mpf_post_heli_crash_4,
		
		// Swat on the way to the lower floor
		mpf_stair_1,
		mpf_stair_2,
		mpf_stair_3,
		mpf_stair_4,
		mpf_stair_5,
		
		// agents on lower floors
		mpf_before_heli_fall_1,
		mpf_before_heli_fall_2,
		mpf_before_heli_fall_3,
		mpf_before_heli_fall_4,
		mpf_before_heli_fall_5,

		mpf_ig7_elevator,
		
		mpf_post_heli_fall_1,
		
		mpf_1st_comms_room_1,
		mpf_1st_comms_room_2,
		mpf_2nd_comms_room_1,
		mpf_2nd_comms_room_2,
		
		mpf_pre_final_stairwell_1,
		mpf_pre_final_stairwell_2,
		mpf_pre_final_stairwell_3,
		
		mpf_final_rappel_room_1,
		mpf_final_rappel_room_2,
		
		mpf_dead_agent_1,
		mpf_dead_agent_2,
		mpf_dead_agent_3,
		
		//peds in the corridor paramedic scene
		mpf_ig8_paramedic_1,
		mpf_ig8_victim,
		mpf_ig8_agent1,
		mpf_ig8_agent2,
		
		// Agents/Swat on the ground outside the building
		mpf_ground_pre_ent_1,
		mpf_ground_pre_ent_2,
		mpf_ground_pre_ent_3,
		
		// Agents coming from the front of the building
		mpf_ground_ent_1,
		mpf_ground_ent_2,
		mpf_ground_ent_3,
		
		mpf_num_peds 
	ENDENUM

	// List of vehicles used
	ENUM MISSION_VEHICLE_FLAGS
		mvf_mikesCar,
		mvf_heli,
		mvf_heli_attack,
		mvf_heli_news,
		
		mvf_police_car_1,
		mvf_police_car_2,
		mvf_police_car_3,
		mvf_police_car_4,
		mvf_police_car_5,
		mvf_police_car_6,
		mvf_police_car_7,
		mvf_police_car_8,
		mvf_firetruck_1,
		mvf_firetruck_2,
		mvf_ambulance_1,
		mvf_ambulance_2,

		mvf_escape,
		mvf_news_van,
		mvf_late_fib_1,
		mvf_late_fib_2,
		
		mvf_num_vehicles
	ENDENUM
	
	// Mission camera flag list
	ENUM CAM_LIST
		mcf_generic,
		mcf_tod_1,
		mcf_tod_2,
		mcf_helijump1,
		mcf_helijump2,
		mcf_hack,
		mcf_dc_progress,
		mcf_helicrash_1,
		mcf_helicrash_2,
		mcf_helicrash_3,
		mcf_rappel_cam,
		mcf_num_cams
	ENDENUM
	
	ENUM MISSION_OBJECT_FLAG
		mof_walking_stick,
		mof_lester_crate,
		mof_hack_computer,
		mof_hack_chair,
		mof_hack_door_fake,
		mof_hack_phone,
		mof_heli_rocket,
		mof_glass_smoke_1,
		mof_rubble_blocker,
		mof_table,
		mof_rappel_rope,
		mof_gas_grenade,
		
		mof_cutscene_weapon_mike,
		mof_cutscene_weapon_frank,
		mof_cutscene_weapon_goon,
		
		mof_alarm_lights,
		mof_alarm_lights_2,
		mof_alarm_lights_3,
		mof_crashed_heli,
		
		mof_equipment_bag,
		
		mof_num_objects
	ENDENUM
	
	ENUM MISSION_PICKUP_FLAGS
		mpuf_health_paramedic,
		mpuf_num_pickups
	ENDENUM
	
	ENUM CREW_MEMBER
		cmf_mike,
		cmf_frank,
		cmf_hacker,
		cmf_goon,
		cmf_driver,
		cmf_num_crew_members
	ENDENUM
	
	ENUM SFX_FLAGS
		sfx_downloading,
		sfx_downloaded,
		sfx_smoke_nade,
		sfx_heli_crash_exp,
		sfx_heli_base_jump,
		sfx_distant_sirens_1,
		sfx_distant_sirens_2,
		sfx_distant_sirens_fight,
		sfx_num_sounds
	ENDENUM
	
	ENUM PTFX_LIST
		ptfx_sweat_shop,
		ptfx_tail_rotor,
		ptfx_gas_grenade_trail,
		ptfx_door_haze,
		ptfx_sparks_wall_right,
		ptfx_sparks_ceiling,
		NUM_OF_PTFXS
	ENDENUM
	
	ENUM EVENT_STATE
		EVENTSTATE_READY,		// event is ready to run
			// Running States
			EVENTSTATE_RUNNING,	// running normally
			EVENTSTATE_DELAYED,	// a delay between now and the next stage has been added
			EVENTSTATE_PAUSED,	// event is paused completely
		EVENTSTATE_COMPLETED,	// event has completed or has been forced to stop now
		EVENTSTATE_RESET		// event has been reset, same as completed just helps the debug drawer
	ENDENUM
	
	ENUM FPS_DOWNLOAD_CAM
		DC_PROGRESS_CAM_ENTER,
		DC_PROGRESS_CAM_CURRENT,
		
		DC_CAM_COVER_ENTER,
		DC_CAM_COVER_CURRENT
	ENDENUM
	
	ENUM RAPPEL_CAM_TYPE
		RAPPELCAM_LONG_DEFAULT,
		RAPPELCAM_LONG_CRASH,
		RAPPELCAM_LONG_911,
		RAPPELCAM_LONG_HELI,
		RAPPELCAM_SHORT_DEFAULT
	ENDENUM
	
	ENUM HACKING_STAGE_ENUM
		HACKSTAGE_DESKTOP		= 2,
		HACKSTAGE_MYCOMP		= 3,
		HACKSTAGE_PHONE			= 4,
		HACKSTAGE_HCONNECT 		= 5,
		HACKSTAGE_HBRUTE 		= 6,
		HACKSTAGE_DELAY 		= 7,
		HACKSTAGE_HDOWNLOAD		= 8
	ENDENUM
	
	ENUM DOWNLOAD_SHOOTOUT_DEFENSIVE_AREA_ENUM
		dsd_invalid = -1,
		dsd_main_post,
		dsd_main_chairs_left_rear_l,
		dsd_main_chairs_left_rear_r,
		dsd_main_chairs_left_front_l,
		dsd_main_chairs_left_front_r,
		dsd_main_chairs_right_rear_l,
		dsd_main_chairs_right_rear_r,
		dsd_main_chairs_right_front_l,
		dsd_main_chairs_right_front_r,
		dsd_main_planter_left_rear,
		dsd_main_planter_right_rear,
		dsd_main_planter_left_front,
		dsd_main_planter_right_front,
		dsd_main_glass_room_left,
		dsd_main_glass_room_right_front,
		dsd_main_glass_room_right_rear,
		dsd_main_mini_planter_front_left,
		dsd_main_mini_planter_front_right,
		dsd_roof_rear_l,
		dsd_roof_rear_mid_l,
		dsd_roof_rear_mid_r,
		dsd_roof_rear_r,
		dsd_roof_leftside_front,
		dsd_roof_leftside_rear,
		dsd_roof_rightside_front,
		dsd_roof_rightside_rear,
		dsd_num_areas
	ENDENUM
	
	ENUM SPAWN_POINT_FLAG
		spf_invalid = -1,
		spf_main_entrance,
		spf_main_fake_left,
		spf_main_fake_mid,
		spf_main_fake_right,
		spf_roof_rear_left,
		spf_roof_rear_right,
		spf_roof_front_left,
		spf_roof_front_right,
		spf_num_spawn_points
	ENDENUM
	
	ENUM HELI_AI
		HELIAI_ATTACK_STATIONARY,
		HELIAI_ATTACK_MOVING,
		HELIAI_EVADE
	ENDENUM

// Structs
//---------------------------------------------------------------------------------------------------------------------------------------------------
	
	// Event struct
	STRUCT MISSION_EVENT_STRUCT
		EVENT_STATE		state
		INT 			iStage
		INT				iTimeStamp
		INT 			iDelay
		INT				iTimeSinceLastRunning
	ENDSTRUCT

	// struct, info about a ped
	STRUCT PED_STRUCT
		PED_INDEX			id
		COVERPOINT_INDEX	cov
		BOOL				bDoesCovExist
		BLIP_INDEX			objective_blip
		INT					iGenericFlag	// this generic flag can be used in different situations where required
		INT					iDeadTimer
		BOOL				bAIBlipsActive
		AI_BLIP_STRUCT		ai_blip
		INT					iCurrentlyInRoom 	= -1
		INT					iExitingThisRoom 	= -1
		INT					iFlashlightStatus 	= -1
		BOOL				bDoNotCleanUp
		BOOL				bHasBeenKilled
	ENDSTRUCT

	// Struct for holding all data to do with vehicles
	STRUCT VEH_STRUCT
		VEHICLE_INDEX		id
		PED_STRUCT			peds[4] 						// max 4 ppl in a vehicle
		BLIP_INDEX			blip
		
		// recording related stuff
		FLOAT				fStartSpeed
		FLOAT				fDesiredSpeed
		FLOAT				fCurrentSpeed				// keeps track of the vehicles playback speed on a recording
		INT					iSpeedTimeStamp
		INT					iDesiredDuration
	ENDSTRUCT
	
	// Struct used to hold info about doors
	STRUCT DOOR_STRUCT
		INT				iHash
		BOOL			bChangingState
		FLOAT			fDesiredOpenRatio
		FLOAT			fStartOpenRatio
		BOOL			bDesiredLockState
		INT				iInterpTime
		INT				iTimeStamp
		BOOL			bExistingDoor
	ENDSTRUCT
	
	// Spawn spot struct for spawning peds
	STRUCT SPAWN_SPOT
		VECTOR			coord
		FLOAT			heading
	ENDSTRUCT
	
	// struct is used so I can different between props I've added myself and ones that I've grabbed and used from the interior
	STRUCT PROP_STRUCT
		OBJECT_INDEX	id
		BOOL			bCustom
	ENDSTRUCT

	// Rocket struct
	STRUCT ROCKET_DATA
		OBJECT_INDEX obj
		VEHICLE_INDEX veh_target
		VECTOR v_dir
		VECTOR v_start
		VECTOR v_rot
		VECTOR v_offset
		VECTOR v_vel
		FLOAT f_RocketSpeed
		BOOL b_reached_target
		BOOL b_add_entity_speed
		BOOL b_rocket_belongs_to_player
		INT i_explode_timer
		FLOAT f_speed_multiplier
		PTFX_ID ptfx
	ENDSTRUCT
	
	STRUCT SPRINKLER_STRUCT
		VECTOR					vCoord
		MODEL_NAMES				iCurrentModel 				= V_ILEV_FIB_SPRKLR
		BOOL					bMustCreateTheSprinkler		= FALSE
		OBJECT_INDEX			objSprinkler
	ENDSTRUCT
	
	STRUCT COVERPOINT_PAIR
		COVERPOINT_INDEX 	covID[2]
		INT					id
	ENDSTRUCT
	
	STRUCT SHOOTOUT_DEFENSIVE_AREA_STRUCT
		VECTOR		vCoord
		FLOAT		fRadius
		BOOL		bIsRoofPoint
	ENDSTRUCT

	STRUCT SPAWN_POINT_STRUCT
		VECTOR		vCoord
		FLOAT		fHeading
		BOOL		bIsRoofPoint
	ENDSTRUCT

	STRUCT WAVE_PED_STRUCT
		// Initialisation 
		SPAWN_POINT_FLAG								eSpawnPoint						= spf_invalid
		DOWNLOAD_SHOOTOUT_DEFENSIVE_AREA_ENUM			ePreferredDefensiveArea			= dsd_invalid
		MODEL_NAMES										eModel							= DUMMY_MODEL_FOR_SCRIPT
		WEAPON_TYPE										eWeapon							= WEAPONTYPE_INVALID
		WEAPONCOMPONENT_TYPE							eWeaponComp						= WEAPONCOMPONENT_INVALID
		INT												iAccuracy						= -1
		INT												iHealth							= -1
		BOOL											bOpenCombat	
		BOOL											bCharge
		BOOL											bChargeNow
		// Spawn conditions
		INT												iKillsSinceLast					= -1	// number of kills needed since the last ped to trigger the next
		INT												iTimeSinceLast					= -1	// time that is required to pass before this enemy can spawn
		FLOAT											fProgress						= 0.0	// the minimum download progress before we can spawn this next ped
		BOOL											bWaitForGasGrenade				= FALSE
		// Ped status
		BOOL											bSpawned						= FALSE
		BOOL											bIsDead							= FALSE
	ENDSTRUCT
	
	STRUCT EMERGENCY_SERVICE_ARRIVE_STRUCT
		MISSION_VEHICLE_FLAGS	eVeh
		MODEL_NAMES				eModel
		INT						iRecNumber
		INT 					iStartTime
		INT						iPauseLength
		INT						iPauseTimer
		INT						iStage
		BOOL					bSpawned
	ENDSTRUCT
	
	STRUCT RAPPEL_DATA_ADDITIONAL
		RAPPEL_DATA		sRappelData
		BOOL			bIsRappeling					= FALSE
		BOOL			bReachedEnd						= FALSE
		FLOAT			fAIRandomJumpCutoff				= -1.0
		BOOL 			bCancelAtLimit					= FALSE
		BOOL 			bIsAI							= FALSE
		BOOL 			bDoCam							= FALSE
		FLOAT 			fZTarget						= 0.0
		BOOL 			bSwitchToFreeRope				= FALSE
		FLOAT 			fSwitchLimit 					= 0.0
		BOOL 			bBlockBigJumps					= FALSE
		WEAPON_TYPE 	eDismountWeap 					= WEAPONTYPE_UNARMED
		INT				iAIJumpTimer					= -1
		INT				iAIFirstJumpDelay				= 0
	ENDSTRUCT
	
	STRUCT PTFX_EVO
		FLOAT			fCurrent
		FLOAT			fStart
		FLOAT			fDesired
		INT				iTimeStarted
	ENDSTRUCT

	
// Constants & non-changing variables
//---------------------------------------------------------------------------------------------------------------------------------------------------
	
	// Stage management consts
	CONST_INT 		STAGE_ENTRY			0		
	CONST_INT 		STAGE_EXIT			-1
	
	CONST_FLOAT 	DOORS_KEEP_CURRENT_RATIO 	100.0
	
	TEXT_LABEL_7	strDialogue					= "AH3BAUD"							// text block name for dialogue
	CONST_INT		iHeistID_Hacker 			0
	CONST_INT		iHeistID_Goon 				1
	CONST_INT		iHeistID_Driver				2
	CONST_INT		iStartHours					0
	
	CONST_INT		WAVE_PED_START						mpf_download_1
	CONST_INT		WAVE_PED_END						mpf_download_24
	CONST_INT 		NUM_DOWNLOAD_SHOOTOUT_ENEMIES		WAVE_PED_END - (WAVE_PED_START-1)
	CONST_INT 		I_NUM_MAX_ENEMIES_SPAWNED			12
	
	CONST_INT 		DEFAULT_MISSION_PED_ARMOUR 			0
	
	// Rappel Cam setting
	TWEAK_FLOAT		fStartRappelInterp				-80.0
	
	TWEAK_FLOAT		fDefaultHeading_START			-240.88
	TWEAK_FLOAT 	fYDist_START					8.495
	TWEAK_FLOAT 	fXYInputToDegrees_START			2.0
	TWEAK_FLOAT 	fClockwiseLimit_START			11.625
	TWEAK_FLOAT 	fAntiClockwiseLimit_START		56.515
	TWEAK_FLOAT 	fXYSpeed_START					1.25
	TWEAK_FLOAT 	fZDist_START					6.535
	TWEAK_FLOAT 	fZInputToHeightFactor_START		0.04
	TWEAK_FLOAT 	fZUpperLimit_START				7.855
	TWEAK_FLOAT 	fZLowerLimit_START				-7.2
	TWEAK_FLOAT 	fZSpeed_START					1.75
	TWEAK_FLOAT 	fZFOV_START						30.105
	VECTOR			vLookAtOffset_START				= <<-0.9,-1.6,-0.3>>
	
	TWEAK_FLOAT		fDefaultHeading_END				-245.530
	TWEAK_FLOAT 	fYDist_END						5.275
	TWEAK_FLOAT 	fXYInputToDegrees_END			2.0
	TWEAK_FLOAT 	fClockwiseLimit_END				6.7
	TWEAK_FLOAT 	fAntiClockwiseLimit_END			63.790
	TWEAK_FLOAT 	fXYSpeed_END					1.25
	TWEAK_FLOAT 	fZDist_END						1.080
	TWEAK_FLOAT 	fZInputToHeightFactor_END		0.04
	TWEAK_FLOAT 	fZUpperLimit_END				3.505
	TWEAK_FLOAT 	fZLowerLimit_END				-1.550
	TWEAK_FLOAT 	fZSpeed_END						1.750
	TWEAK_FLOAT 	fZFOV_END						40.0
	VECTOR			vLookAtOffset_END				= <<0.0,-0.725,0.0>>

	// Anim dictionaries & anim names
	TEXT_LABEL_23		animDict_IG1_sky_dive		= "missheistfbi3b_ig1"
	TEXT_LABEL_23		anim_sky_dive_loop			= "Heli_Loop_Michael"
	TEXT_LABEL_23		anim_sky_dive_jump			= "Heli_Jump_Michael"
	
	TEXT_LABEL_23		animDict_IG2_try_door		= "missheistfbi3b_ig2"
	TEXT_LABEL_23		anim_try_door				= "Door_TryOpen_Michael"
	VECTOR				v_ig2_coord					= << 150.59, -762.38, 258.30 >>
	VECTOR				v_ig2_rot					= << 0.0, 0.0, 159.0 >>
	
	TEXT_LABEL_23		animDict_IG3_kill_swat 		= "missheistfbi3b_ig3"
	TEXT_LABEL_31		anim_killswat_frank			= "Corridor_KillSwat_Franklin"
	TEXT_LABEL_31		anim_killswat_goon			= "Corridor_KillSwat_Gunman"
	TEXT_LABEL_31		anim_killswat_swat			= "Corridor_KillSwat_Swat"
	VECTOR				v_ig3_coord					= <<119.087, -736.397, 257.130>>
	VECTOR 				v_ig3_rot					= <<0,0,159.750>>
	CONST_INT			I_FRANK_WP_START			1
	CONST_INT			I_GOON_WP_START				1
	
	TEXT_LABEL_23		animDict_IG4_sw_door_kick	= "missheistfbi3b_ig4"
	TEXT_LABEL_23		anim_goon_door_kick			= "shoot_opendoor_michael"
	
	TEXT_LABEL_23		animDict_IG5_sw_push_door	= "missheistfbi3b_ig5"
	TEXT_LABEL_23		anim_frank_push_door		= "stairs_opendoor_michael"
	VECTOR				v_ig5_coord					= << 129.177, -731.754, 254.302 >>
	VECTOR				v_ig5_rot					= << -0.000, 0.000, -20.160 >>
	
	TEXT_LABEL_23		animDict_IG6_michael_slide	= "missHeistfbi3B_IG6_v2"
	VECTOR				v_ig6_coord					= <<150.936, -729.071, 249.166>>
	VECTOR				v_ig6_rot					= <<0.0,0.0,159.750>>
	
	TEXT_LABEL_23		animDictMCS6Pilot			= "missah_3bmcs_6_p1_pilot"
	
	TEXT_LABEL_23		animDict_IG7_elevator		= "missheistfbi3b_ig7"
	TEXT_LABEL_23		anim_elevator_loop			= "lift_fibagent_loop"
	TEXT_LABEL_23		anim_elevator_exit			= "lift_fibagent_out"
	VECTOR				v_ig7_coord					= <<137.201, -733.135, 249.150>>
	VECTOR				v_ig7_rot					= <<0.0,0.0,-109.5>>
	
	VECTOR				v_ig8_coord					= <<130.830, -739.857, 249.159>>
	VECTOR				v_ig8_rot					= <<0,0,-110.500>>
	
	TEXT_LABEL_23		animDict_IG9_dismount		= "missheistfbi3b_ig9"
	VECTOR				v_ig9_coord					= <<166.175, -763.430, 73.150>>
	VECTOR				v_ig9_rot					= <<0,0,159.656>>
	
	TEXT_LABEL_23		animDict_IG10_rappel_over	= "missheistfbi3b_ig10"
	
	TEXT_LABEL_23		animDict_WaitIdles			= "missbigscore2big_2"
	
	VECTOR				v_hack_phone_coord			= <<151.26, -765.84, 258.058>>
	VECTOR				v_hack_phone_rot			= <<-90.0, 0.0, -106.57>>
	
	VECTOR 				v_mcs_2_leadout_coord 		= << 134.7172, -752.7620, 257.1572 >>
	VECTOR 				v_mcs_2_leadout_rot			= << 0.000, 0.000, 159.855225 >>
	
	// Waypoints
	TEXT_LABEL_23		wp_try_door					= "agen3b_try_door"

	TEXT_LABEL_23		wp_sw_0_frank				= "agenc3b_sw_0_frank"
	TEXT_LABEL_23		wp_sw_0_goon				= "agenc3b_sw_0_goon"
	
	// Model names 
	MODEL_NAMES		mod_heli				= MAVERICK
	MODEL_NAMES		mod_heli_attack			= BUZZARD
	MODEL_NAMES		mod_fib_agent_1 		= S_M_M_FIBOFFICE_01 
	MODEL_NAMES		mod_fib_agent_2			= S_M_M_HIGHSEC_01 
	MODEL_NAMES		mod_fib_agent_3 		= S_M_M_CIASEC_01 
	MODEL_NAMES		mod_swat				= S_M_Y_SWAT_01
	MODEL_NAMES		mod_esc_veh				= BURRITO3
	MODEL_NAMES		mod_news_van			= RUMPO
	MODEL_NAMES		mod_rubble_blocker	 	= V_ILEV_FIB_DEBRIS
	MODEL_NAMES		mod_hack_phone			= PROP_PHONE_ING
	MODEL_NAMES		mod_pilot				= S_M_Y_PILOT_01
	MODEL_NAMES		mod_pilot_enemy			= S_M_M_PILOT_02
	MODEL_NAMES 	mod_OriginalHackDoor	= PROP_SECDOOR_01 //V_ILEV_ARM_SECDOOR // V_ILEV_FIB_DOOR1
	 
	// Weapons
	WEAPON_TYPE		weap_Mike					= WEAPONTYPE_CARBINERIFLE
	WEAPON_TYPE		weap_Frank					= WEAPONTYPE_CARBINERIFLE
	
	WEAPON_TYPE		weap_Driver					= WEAPONTYPE_COMBATPISTOL
	
	WEAPON_TYPE				weap_Goon			= WEAPONTYPE_INVALID
	WEAPONCOMPONENT_TYPE	weapComp_GoonFlash	= WEAPONCOMPONENT_INVALID
	WEAPONCOMPONENT_TYPE	weapComp_GoonSupp	= WEAPONCOMPONENT_INVALID
	WEAPONCOMPONENT_TYPE	weapComp_GoonGrip	= WEAPONCOMPONENT_INVALID
	WEAPONCOMPONENT_TYPE	weapComp_GoonScope	= WEAPONCOMPONENT_INVALID
	WEAPONCOMPONENT_TYPE	weapComp_GoonClip	= WEAPONCOMPONENT_INVALID
	
	CONST_INT		ammo_Mike				240
	CONST_INT		ammo_Frank				240
	CONST_INT		ammo_Goon				INFINITE_AMMO
	CONST_INT		ammo_Driver				INFINITE_AMMO
	
	CONST_FLOAT		DOOR_CHECK_RADIUS		1.0

	// Vehicle recording route name & recording indices
	TEXT_LABEL_7	str_car_recs				= "mwah"
	
	CONST_INT		rec_heli_rpl_flyby			2
	CONST_INT		rec_heli_rpl_attack			4
	
	// NEW HELI CUTSCENE STUFF
	CONST_INT		rec_gunship_attack_run		103
	CONST_INT		rec_gunship_flyby			104
	CONST_INT		rec_heli_rescue				101
	CONST_INT		rec_heli_hover				201
	CONST_INT		rec_heli_rocket_hit			102
	CONST_INT		rec_heli_hit_building		203
	
	CONST_INT		rec_driver_ambulance_arrive	23
	CONST_INT 		rec_driver_ambulance_pullup	15
	CONST_INT		rec_driver_van				8
	
	CONST_INT		rec_fib_late_1				9
	CONST_INT		rec_fib_late_2				202
	
	CONST_INT		rec_police_arrive_1			1
	CONST_INT		rec_police_arrive_2			3
	CONST_INT		rec_police_arrive_3			5
	CONST_INT		rec_police_arrive_4			6
	CONST_INT		rec_police_arrive_5			7
	CONST_INT		rec_police_arrive_6			10
	CONST_INT		rec_police_arrive_7			17
	CONST_INT		rec_police_arrive_8			18
	CONST_INT		rec_fireturk_arrive_1		19
	CONST_INT		rec_fireturk_arrive_2		20
	CONST_INT		rec_ambulance_arrive_1		21
	CONST_INT		rec_ambulance_arrive_2		22
	
	CONST_INT		rec_police_car_pull_up_1	11
	CONST_INT		rec_police_car_pull_up_2	12
	CONST_INT		rec_police_car_pull_up_3	13
	CONST_INT		rec_firetruck_pullup_1		14
	CONST_INT		rec_firetruck_pullup_2		16
	
	TWEAK_FLOAT		f_cam_limits_heading_min		64.665
	TWEAK_FLOAT 	f_cam_limits_heading_max		98.325
	TWEAK_FLOAT		f_cam_limits_pitch_min			-7.2
	TWEAK_FLOAT 	f_cam_limits_pitch_max			12.960
	TWEAK_FLOAT		f_cam_limits_orbit_min			0.9
	TWEAK_FLOAT 	f_cam_limits_orbit_max			1.2
	
	TWEAK_FLOAT		f_cam_limits_aim_heading_min	17.185
	TWEAK_FLOAT 	f_cam_limits_aim_heading_max	99.375
	TWEAK_FLOAT		f_cam_limits_aim_pitch_min		-18.850
	TWEAK_FLOAT 	f_cam_limits_aim_pitch_max		22.685
	TWEAK_FLOAT		f_cam_limits_aim_orbit_min		0.9
	TWEAK_FLOAT 	f_cam_limits_aim_orbit_max		1.2
	
	#IF IS_NEXTGEN_BUILD
	TWEAK_FLOAT		f_Hacking_Render_Target_x		0.426 //0.476
	TWEAK_FLOAT		f_Hacking_Render_Target_y		0.366 //0.366
	TWEAK_FLOAT		f_Hacking_Render_Target_width	0.925 //1.000
	TWEAK_FLOAT		f_Hacking_Render_Target_height	0.723 //0.760
	#ENDIF
	
	#IF NOT IS_NEXTGEN_BUILD
	
	TWEAK_FLOAT		f_Hacking_Render_Target_x		0.426 //0.101
	TWEAK_FLOAT		f_Hacking_Render_Target_y		0.366 //0.178
	TWEAK_FLOAT		f_Hacking_Render_Target_width	0.879 //0.216
	TWEAK_FLOAT		f_Hacking_Render_Target_height	0.710 //0.347
	
	#ENDIF
	
	CONST_FLOAT		F_LANDING_Z_CUTOFF				 230.0

	// Mission locates, headings, etc
	// >>> Sweat shop start <<<
	VECTOR 			vStartDriveCarCoord 		= << 718.3592, -983.2876, 23.1379 >>
	CONST_FLOAT 	fStartDriveCarHeading 		270.6242
	
	// >>> Meet at the heli pad <<<
	VECTOR			vHeliPadCoord				= << 2504.6204, -317.7861, 91.9929 >>
	
	VECTOR			vHeliStartCoord				= << 2510.9185, -342.3889, 117.1862 >>
	CONST_FLOAT		fHeliStartHeading			116.2210
	
	// >>> Sky dive point <<<
	VECTOR			vHeliJumpBlipCoord			= << 1036.6469, -548.8130, 791.0118 >>
	VECTOR			vHeliJumpCoord				= << 1036.6469, -548.8130, 1100.0 >>
	CONST_FLOAT		fHeliJumpHeading			182.1244
	
	VECTOR			vSkyDiveLandingCoord		= << 139.7955, -742.1393, 261.8531 >>
	VECTOR			vFrankSkyDiveLandCoord		= << 122.9093, -741.5135, 261.8533 >>
	VECTOR			vGoonSkyDiveLandCoord		= << 143.7658, -764.5782, 261.8533 >>

	// >>> FBI offices <<<
	VECTOR 			v_ShootoutCoverMike			= << 150.2690, -761.6932, 257.1522 >>
	VECTOR 			v_ShootoutCoverFrank		= <<148.8185, -751.3002, 257.1518>>
	VECTOR 			v_ShootoutCoverGoon			= <<146.6870, -756.3611, 257.1695>>
	VECTOR 			vStreetPickupLocation 		= << 67.5801, -737.1165, 43.2183 >>
		
	// >>> End at franklins <<<
	VECTOR			vEndAtFranklinsCoord		= <<7.6432, 550.7535, 174.6488>>
	
	CONST_INT 		I_DOWNLOAD_DURATION 		120000 // (120 seconds/ 2:00)
	
	CONST_FLOAT 	fRopeLength					177.0
	CONST_FLOAT 	fCreateRopeLength 			162.6
	CONST_FLOAT 	fRopeLength2				27.1
	CONST_FLOAT 	fCreateRopeLength2 			15.0
	CONST_FLOAT 	fZLimitOffset				1.0
	
	CONST_INT		I_HELI_VARIATION			0
	CONST_INT		I_ESC_VAN_VARIATION			2
	CONST_INT		I_NEWS_VAN_VARIATION		0
	CONST_INT		I_NEWS_VAN_LIVERY			0
	
	VECTOR 			vSweatShopSmokeCoord 		= <<723.41, -966.83, 35.86>>
	VECTOR 			vSweatShopSmokeRot 			= <<0,0,0>>
	CONST_FLOAT 	fSweatShopSmokeScale		5.0
	
	VECTOR			vCleanupAreaCoord 			= <<136.024246,-749.247070,138.736572>> 
	CONST_FLOAT 	fCleanupAreaRadius 			137.812500
		
// Variables
//---------------------------------------------------------------------------------------------------------------------------------------------------

	structPedsForConversation	sConvo							// used for dialogue
	SEQUENCE_INDEX				seq								// sequence used for tasking peds
	BOOL						bSequenceOpen					// used with custom tasks to detect if sequence is open or not
	LOCATES_HEADER_DATA			locates_Data					// locates header struct, used for locate tests
	REL_GROUP_HASH				REL_CREW						// relationship group for buddies
	REL_GROUP_HASH				REL_CREW_IGNORE						
	REL_GROUP_HASH				REL_ENEMY						// ... for enemies
	REL_GROUP_HASH				REL_911							// ... for emergency service
	BLIP_INDEX					blip_Objective					// blip used for mission objectives
	BOOL 						b_IsDoingFail
	BOOL						bVehStatsGrabbed				= FALSE
	
	// Mission entity arrays
	PED_STRUCT					peds[mpf_num_peds]				// ped array
	VEH_STRUCT					vehs[mvf_num_vehicles]			// vehicle array
	CAMERA_INDEX				cams[mcf_num_cams]				// camera array
	PROP_STRUCT					objs[mof_num_objects]			// object array
	PICKUP_INDEX				pkups[mpuf_num_pickups]			// pickups array
	INT							sounds[sfx_num_sounds]			// sound array
	INT							syncedScenes[ssf_num_scenes]	// synced scene id array
	PTFX_ID						ptfxs[NUM_OF_PTFXS]				// particle effects
	SPRINKLER_STRUCT			s_Sprinklers[40]				// array of sprinklers
	
	// TOD stuff
	structTimelapse				sTimelapse
	
	// Crew Variables
	CrewMember					crewHacker
	CrewMember					crewGoon
	CrewMember 					crewDriver
	INT							iCrewAIStage		// Crew AI helpers
	BOOL						bBuddyReadyToMoveOn
	BOOL 						bGunmanReadyToMoveOn
	BOOL						bFailOnDeadPilot
	BOOL						bFailChecksOnHeli
	BOOL						bFailChecksOnVan
	BOOL						bFailAbandonDriver
	BOOL						bFailAbandonGunman
	BOOL						bFailAbandonPilot
	VECTOR 						vGoToExactlyBuddy
	VECTOR 						vGoToExactlyGunman
	VECTOR						vGoToExactlyBuddyRot
	VECTOR						vGoToExactlyGunmanRot
	BOOL 						bRetaskBuddy
	BOOL 						bRetaskGunman
	INT							iBuddySeqLength
	BOOL 						bExitStateMichael, bExitStateFrank, bExitStateLester, bExitStateGoon, bExitStateCam
	BOOL						bWeaponSetMichael, bWeaponSetFrank
	INT 						i_SeqLengthFrank	
	INT 						i_SotredChuteColour_Mike
	INT 						i_SotredChuteColour_Frank
	INT							i_CrewAbandonedTimer = -1
	
	BOOL 						bStreamingSceneIn
	
	// Dialogue stuff
	INT 						i_dialogue_stage
	INT 						i_dialogue_substage
	INT							i_time_of_last_convo 	= 0
	TEXT_LABEL_23 				str_dialogue_root
	TEXT_LABEL_23 				str_dialogue_line
	INT							i_shootout_dialogue_selector
	INT 						i_dialogue_timer
	
	
	INT							i_TimerA
	INT							i_FlashTimer

	// Event and loading stuff
	MISSION_EVENT_STRUCT		mEvents[mef_num_events]			// event array
	ASSET_MANAGEMENT_DATA		sAssetData						// monitors what is loaded and is helpful in z skips
	DOOR_STRUCT					sDoors[mdf_num_doors]			// door array
	CUTSCENE_PED_VARIATION 		sCutscenePedVariationRegister[6] // used by the cutscene prestreaming system
	
	// Stage management and skip stuff
	STAGE_SWITCHSTATE 			stageSwitch						// current switching status
	INT 						mission_stage					// current mission mission_stage
	INT 						mission_substage				// current mission mission_substage (switch flag used in mission_stage procs)
	INT 						requestedStage					// the mission_stage requested by a mission_stage switch
	INT							iStageTimer						// timer used for debug
	BOOL						bDoSkip							// triggers a stage skip
	INT							iSkipToStage					// the stage to skip to
	
	// Hotswap stuff
	SWITCHSTATE 				swState
	SELECTOR_PED_STRUCT			sSelectorPeds					
	SELECTOR_CAM_STRUCT 		sCamDetails
	
	
	INTERIOR_INSTANCE_INDEX		interior_sweatshop
	INTERIOR_INSTANCE_INDEX		interior_fib03					// atrium and upper burntout section
	INTERIOR_INSTANCE_INDEX		interior_fib02					// lower burntout section
	INTERIOR_INSTANCE_INDEX		interior_fib04					// stairwell
	INTERIOR_INSTANCE_INDEX		interior_franklins				// 
	
	INT 						i_AudioSceneStage = 0
	
	// Parachuting
	INT 						i_heli_cam_type 		= 0
	INT 						i_heli_cam_timer
	INT 						i_heli_cam_duration
	BOOL						bBailHeliNow
	BOOL 						b_frank_parachute_deployed		// indicates that franklin has pulled his chute
	BOOL 						b_goon_parachute_deployed		// indicates that the gunman has pulled his chute
	BOOL						bPlayerLandedSafely
	BOOL						bFrankLandedSafely
	BOOL						bGoonLandedSafely
	BOOL						bDisplayedParaHelp3
	INT							iHelpTimer				= -1
	INT							iFrankLandingDia
	INT							iGoonLandingDia
	
	// Hacking stuff
	BOOL 						bAlarmsTriggered
	S_HACKING_DATA				sHackingData
	FLOAT						fDownloadProgress
	FLOAT 						fDownloadSpeedMultiplier 			= 1.0
	BOOL						bDownloadStarted
	BOOL 						bDownloadComplete
	INT							i_prev_lives
	BOOL						bDoorHidden
	BOOL						bSwappedGlass
	INT							rt_default
	INT							rt_hack_monitor
	BOOL						bManageMichaelInCover
	BOOL 						bManageShootoutAI
	INT 						iFileIndex
	INT 						i_ExplosiveTimer
	BOOL						bFailOnMonitorDamage
	INT							iMonitorStartDamageFailCheck
	INT 						iLastTimeRemainingUpdate
	INT							iAlarmDelay
	BOOL 						bCheckingForBruteForceMistake
	
	// Michael locked in cover stuff
	FPS_DOWNLOAD_CAM			i_fps_state						= DC_CAM_COVER_CURRENT
	VECTOR						v_dc_progress_current_look_at
	VECTOR 						vCompOffset 					= <<0,0,0.25>>
	BOOL						bHasLookedAtScreenBefore		= FALSE
	INT 						iGlanceHoldTimer 				= -1
	INT 						i_CamLimiterInterpTimer 		= 0
	INT 						i_CamLimiterInterpStage			= 0
	INT							i_TimeOfCameraStateChange		= -1
	
	// Shootout variables
	SHOOTOUT_DEFENSIVE_AREA_STRUCT		s_ShootOutDefensiveAreas[dsd_num_areas]
	SPAWN_POINT_STRUCT					s_SpawnPoints[spf_num_spawn_points]
	WAVE_PED_STRUCT 					s_WavePed[NUM_DOWNLOAD_SHOOTOUT_ENEMIES]
	INT 								iRoofPedQueue[20]								// queue used to queue up the rooftop peds in
	INT									i_TimeLastSpawned
	INT									i_KillsSinceLastSpawned
	INT									i_EnemiesAlive
	INT									i_LastPedKilledTimer
	BOOL 								bPlayingKeyDialogue
	INT 								i_EnemiesLeftPrev, i_EnemiesRightPrev, i_EnemiesMainPrev, i_EnemiesRoofLeftPrev, i_EnemiesRoofRightPrev, i_EnemiesRoofCentrePrev
	INT 								i_EnemyLocationCheckTimer
	BOOL 								bUseMostIncrease
	COVERPOINT_INDEX					covFIBPlayerPriorityCover[10]
	INT									i_KillsFinishShootoutStart
	INT									i_KillsRequiredToFinishShootout

	COVERPOINT_PAIR 				covPair_buddy
	COVERPOINT_PAIR					covPair_gunman
	BOOL							bStartCrashCutscene
	BOOL 							bHasGasGrenadeGoneOff
	INT 							i_NavMeshBlockingAreas[6]
	BOOL							bSightedOnRoof
	
	BOOL 							bWillDisableSprinklers
	
	// Heli crash cutscene
	INT 							i_crash_cutscene_cam_stage
	INT 							i_crash_cutscene_cam_timer
	INT								i_crash_cutscene_dialogue
	ROCKET_DATA						sRocket
	BOOL							bCrashReturnShakeAndPTFX
	
	// Rappeling stuff
	RAPPEL_DATA_ADDITIONAL			s_rappel_mike, 		s_rappel_frank, 	s_rappel_goon
	RAPPEL_DATA_ADDITIONAL			s_rappel_mike2, 	s_rappel_frank2, 	s_rappel_goon2
	BOOL							bRappelShownHelp		// has fast rappel help text been shown
	INT 							i911SpawnTimer
	EMERGENCY_SERVICE_ARRIVE_STRUCT s_EmergencyServicesSpawner[13]
	BOOL							bHalfWayMusic
	
	// Heli attack stuff
	HELI_AI 						eHeliAIState
	HELI_AI 						eHeliAIStatePrev
	VECTOR 							vHeliAttackCoords[5]
	VECTOR							vHeliAttackBuddyDefensiveArea[5]
	VECTOR							vHeliAttackGoonDefensiveArea[5]
	INT 							iHeliAttackSelectedCoord
	INT 							iHeliAttackAITimer
	INT 							iHeliAttackTargetUpdateTimer
	INT 							iHeliAttackPilotDamageTaken
	INT 							iHeliAttackDamageTaken
	INT 							iHeliAttackPilotHealthLastFrame
	INT 							iHeliAttackHealthLastFrame
	PED_INDEX 						pedHeliAttackTarget
	
	// IG specific vars
	INT 							i_dialogue_ig3_frank_count
	
	// VFX
	RAYFIRE_INDEX					collapsing_floor
	PTFX_ID							ptfx_playerAttachedSmokeFX
	PED_INDEX 						ped_PlayerAttachedSmokeAttachedPed
	PTFX_EVO 						s_PlayerAttachedSmokeEvo_SmokeDensity
	PTFX_EVO 						s_PlayerAttachedSmokeEvo_SmokeStrength
	PTFX_EVO 						s_PlayerAttachedSmokeEvo_CinderDensity
	PTFX_EVO 						s_PlayerAttachedSmokeEvo_DebrisDensity
	PTFX_EVO 						s_PlayerAttachedSmokeEvo_WindSpeed
	BOOL							b_TimeCycleModifierActive
	BOOL							b_HeliRayFireExplosionHappened
	INT								i_Silt_Timer_1									= -1
	INT								i_Silt_Timer_2									= -1
	INT								i_Silt_Timer_3									= -1
	
	// Road blocking stuff
	SCENARIO_BLOCKING_INDEX 		sbi_gov_facility_security_guards[4]
	SCENARIO_BLOCKING_INDEX 		sbi_outside_fib_building
	SCENARIO_BLOCKING_INDEX			sbi_sec_guard_entrance
	SCENARIO_BLOCKING_INDEX 		sbi_bin_sweatshop
	SCENARIO_BLOCKING_INDEX			sbi_franklins
	
	// Music Cues
	BOOL 							bLiftedOff
	
	BOOL 							b_CutsceneStreamed
	
	INT								iDesiredSprinklerState		= -1
	CHASE_HINT_CAM_STRUCT			localChaseHintCamStruct
	
	// End getaway scene
	BOOL 							bScorePlaying
	COVERPOINT_INDEX 				cov_FIBEntranceCover[3]
	BOOL 							bDriverHasArrived
	BOOL							bGetwayPickUpReached
	BOOL 							bSwatHaveArrived
	BOOL							bSwatSeenAmbulance
	BOOL							bReachedSafeDistanceFromCrimeScene
	BOOL							bBlockSpawningCops
	INT								iTimeDriverArrived
	INT 							iKilledCopsCount
	INT								i_EscapeAreaTimer
	BOOL 							bPrintLoseCops
	BOOL 							bPickupBuddys
	
//-------------------------------------------------------------------------
//Debug vars
//--------------------------------------------------------------------------
#IF IS_DEBUG_BUILD
	BOOL							bDisplayEventDebug				= FALSE		// Make event debug info display
	MissionStageMenuTextStruct		zMenuNames[21] 					// z menu names
	WIDGET_GROUP_ID 				widget_debug								// debug widget
	// General Debug
	BOOL							b_debug_player_control
	FLOAT							f_debug_gam_cam_rel_heading
	FLOAT							f_debug_gam_cam_rel_pitch
	FLOAT							f_debug_gam_cam_world_heading
	FLOAT							f_debug_player_heading
	INT								i_debug_room_key
	// Rappel Debug
	VECTOR							v_debug_offset_m
	VECTOR							v_debug_offset_f
	VECTOR							v_debug_offset_g
	VECTOR							v_debug_offset_m2
	VECTOR							v_debug_offset_f2
	VECTOR							v_debug_offset_g2
	FLOAT							f_debug_heading_m
	FLOAT							f_debug_heading_f
	FLOAT							f_debug_heading_g
	FLOAT							f_debug_heading_m2
	FLOAT							f_debug_heading_f2
	FLOAT							f_debug_heading_g2
	// Misc
	FLOAT							f_debug_rec_pos1
	FLOAT							f_debug_rec_pos2
	INT 							i_event_page
	INT								i_debug_aim_cam	
	INT								i_debug_EnemiesSpawned
	FLOAT							f_debug_CurrentRate
	
	INT 							debug_i_crewGunmanNextSkip
	INT 							debug_i_crewHackerNextSkip
	INT 							debug_i_crewDriverNextSkip 
	
	BOOL							debug_b_PTFXDebugIsRunning
	
	structSharedDebugVars			debug_PTFXTester
	
	INT								debug_i_EnemiesManuallyCounted

#ENDIF




/*
												 _     _  _______  _        ______  _______  ______    ______ 
												(_)   (_)(_______)(_)      (_____ \(_______)(_____ \  / _____)
												 _______  _____    _        _____) )_____    _____) )( (____  
												|  ___  ||  ___)  | |      |  ____/|  ___)  |  __  /  \____ \ 
												| |   | || |_____ | |_____ | |     | |_____ | |  \ \  _____) )
												|_|   |_||_______)|_______)|_|     |_______)|_|   |_|(______/ 
*/

// PURPOSE: Sets up and cleans up the PC rappel controls
BOOL bPCRappelControlsSetup = FALSE

PROC SETUP_PC_RAPPEL_CONTROLS()

	IF bPCRappelControlsSetup = FALSE
		INIT_PC_SCRIPTED_CONTROLS("Rappelling")
		bPCRappelControlsSetup = TRUE
	ENDIF

ENDPROC


PROC CLEANUP_PC_RAPPEL_CONTROLS()

	IF bPCRappelControlsSetup
		SHUTDOWN_PC_SCRIPTED_CONTROLS()
	ENDIF

ENDPROC


//PURPOSE: Returns Michael's ped index
FUNC PED_INDEX MIKE_PED_ID()
	return peds[mpf_mike].id
ENDFUNC

//PURPOSE: Returns Franklin's ped index
FUNC PED_INDEX FRANK_PED_ID()
	return peds[mpf_frank].id
ENDFUNC

//PURPOSE: Returns the PED_INDEX of the other player character not being controlled currently.
FUNC PED_INDEX BUDDY_PED_ID()
	IF PLAYER_PED_ID() = MIKE_PED_ID()
		RETURN FRANK_PED_ID()
	ELIF PLAYER_PED_ID() = FRANK_PED_ID()
		RETURN MIKE_PED_ID()
	ENDIF
	
	RETURN NULL
ENDFUNC

//PURPOSE: Retrieves the buddies ped flag
FUNC MISSION_PED_FLAGS PLAYER_MPF()
	IF PLAYER_PED_ID() = MIKE_PED_ID()
		RETURN mpf_mike
	ELIF PLAYER_PED_ID() = FRANK_PED_ID()
		RETURN mpf_frank
	ENDIF
	
	RETURN mpf_frank
ENDFUNC

//PURPOSE: Retrieves the buddies ped flag
FUNC MISSION_PED_FLAGS BUDDY_MPF()
	IF PLAYER_PED_ID() = MIKE_PED_ID()
		RETURN mpf_frank
	ELIF PLAYER_PED_ID() = FRANK_PED_ID()
		RETURN mpf_mike
	ENDIF
	
	RETURN mpf_frank
ENDFUNC

PROC RESET_RAPPEL_ADDITIONAL(RAPPEL_DATA_ADDITIONAL &rData)
	
	rData.bIsRappeling				= FALSE
	rData.bReachedEnd				= FALSE
	rData.fAIRandomJumpCutoff		= -1.0
	rData.bCancelAtLimit			= FALSE
	rData.bIsAI						= FALSE
	rData.bDoCam					= FALSE
	rData.fZTarget					= 0.0
	rData.bSwitchToFreeRope			= FALSE
	rData.fSwitchLimit				= 0.0
	rData.bBlockBigJumps			= FALSE
	rData.eDismountWeap				= WEAPONTYPE_UNARMED
	CLEANUP_RAPPEL(rData.sRappelData)
	
ENDPROC

PROC SET_PED_FLASH_LIGHT(PED_STRUCT &ped, BOOL bEnable)
	IF bEnable
		ped.iFlashLightStatus = 1
	ELSE
		ped.iFlashLightStatus = 0
	ENDIF
ENDPROC

PROC GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER(STRING strRoot, BOOL bIncludeGunman, TEXT_LABEL_23 &strResult, BOOL bDoNotIncludePlayer = FALSE, BOOL bIncludeBuddy = TRUE, BOOL bIncludeMike = TRUE, BOOL bIncludeFrank = TRUE)

	IF IS_STRING_NULL_OR_EMPTY(strResult)
	
		INT iRange
		CREW_MEMBER eUseableMembers[cmf_num_crew_members]
		
		IF NOT bDoNotIncludePlayer
			IF PLAYER_PED_ID() = MIKE_PED_ID()
				IF bIncludeMike
					eUseableMembers[iRange] = cmf_mike
					iRange++
				ENDIF
			ELIF PLAYER_PED_ID() = FRANK_PED_ID()
				IF bIncludeFrank
					eUseableMembers[iRange] = cmf_frank
					iRange++
				ENDIF
			ENDIF
			
		ENDIF
		
		IF bIncludeBuddy
			IF BUDDY_PED_ID() = MIKE_PED_ID()
				IF bIncludeMike
					eUseableMembers[iRange] = cmf_mike
					iRange++
				ENDIF
			ELIF BUDDY_PED_ID() = FRANK_PED_ID()
				IF bIncludeFrank
					eUseableMembers[iRange] = cmf_frank
					iRange++
				ENDIF	
			ENDIF
			
		ENDIF
		
		IF bIncludeGunman
			eUseableMembers[iRange] = cmf_goon
			iRange++
		ENDIF

		IF iRange > 0

			strResult = strRoot
			INT iRnd = GET_RANDOM_INT_IN_RANGE(0, iRange)
			
			SWITCH eUseableMembers[iRnd]
				CASE cmf_mike			strResult += "_M"		BREAK
				CASE cmf_frank			strResult += "_F"		BREAK
				CASE cmf_goon		 	strResult = APPEND_CREW_ID_TO_CONV_ROOT(crewGoon, strRoot)		BREAK
				DEFAULT
					SCRIPT_ASSERT("GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER() hit default case")
				BREAK
			ENDSWITCH
			
		ELSE
			SCRIPT_ASSERT("GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER() NOT INCLUDED ANY CREW MEMBERS")
		ENDIF
		
	ENDIF

ENDPROC

//PURPOSE:
FUNC BOOL Set_Vehicle_Playback_Speed(VEH_STRUCT &veh, FLOAT fNewSpeed, INT iDuration = 0)
	IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh.id)
		SCRIPT_ASSERT("Playback not ")
		RETURN TRUE
	ENDIF
	
	// Must be starting a new 
	IF veh.fDesiredSpeed != fNewSpeed
	OR veh.iDesiredDuration != iDuration
		veh.iSpeedTimeStamp = GET_GAME_TIMER()
		veh.fDesiredSpeed = fNewSpeed
		veh.fStartSpeed = veh.fCurrentSpeed
		veh.iDesiredDuration = iDuration
	ENDIF
	
	IF veh.fCurrentSpeed = veh.fDesiredSpeed
		RETURN TRUE	// already equal, return true
	ELSE
		// Calculate the speed
		FLOAT fSpeedChange = ABSF(veh.fDesiredSpeed - veh.fStartSpeed) * CLAMP((GET_GAME_TIMER() - veh.iSpeedTimeStamp)/TO_FLOAT(veh.iDesiredDuration), 0.0, 1.0)
		
		IF veh.fDesiredSpeed < veh.fStartSpeed
			veh.fCurrentSpeed = CLAMP(veh.fCurrentSpeed - fSpeedChange, veh.fDesiredSpeed, veh.fStartSpeed)
			SET_PLAYBACK_SPEED(veh.id, veh.fCurrentSpeed)
		ELSE
			veh.fCurrentSpeed = CLAMP(veh.fCurrentSpeed + fSpeedChange, veh.fStartSpeed, veh.fDesiredSpeed)
			SET_PLAYBACK_SPEED(veh.id, veh.fCurrentSpeed)
		ENDIF
		
		CDEBUG1LN(DEBUG_MIKE, "******** PLAYBACK SPEED CONVERGING: StartSpeed: ", veh.fStartSpeed," DesiredSpeed: ", veh.fDesiredSpeed," CurrentSpeed: ", veh.fCurrentSpeed)
		IF veh.fCurrentSpeed != fNewSpeed
			RETURN FALSE
		ELSE
			CDEBUG1LN(DEBUG_MIKE, "******** PLAYBACK SPEED COMPLETE! *********")
			RETURN TRUE
		ENDIF
	ENDIF
ENDFUNC

//PURPOSE: Disables the appropriate roads and paths around the FIB building
PROC Disable_Roads_And_Paths_Around_FIB_Building(BOOL bEnable)

	VECTOR vMin = <<124.238640,-690.152954,113.714790>>-<<250.000000,250.000000,87.500000>>
	VECTOR vMax = <<124.238640,-690.152954,113.714790>>+<<250.000000,250.000000,87.500000>>

	IF bEnable

		// Underpass
		SET_ROADS_IN_ANGLED_AREA(<<178.629013,-798.900269,29.817137>>, <<298.631531,-481.388367,48.373196>>, 38.875000, FALSE, FALSE)
		// Slip to under
		SET_ROADS_IN_ANGLED_AREA(<<5.683404,-534.347595,32.359512>>, <<255.550278,-546.460632,48.292702>>, 18.937500, 	FALSE, FALSE)
		// CIA Side
		SET_ROADS_IN_ANGLED_AREA(<<229.966644,-618.448792,39.434505>>, <<119.966393,-577.765564,48.681129>>, 18.937500, FALSE, FALSE)
		// Main Stretch
		SET_ROADS_IN_ANGLED_AREA(<<-28.650225,-938.397888,27.670048>>, <<181.116150,-353.392487,49.062332>>, 40.250000, FALSE, FALSE)
		// Maze Union
		SET_ROADS_IN_ANGLED_AREA(<<-203.868988,-676.714417,32.087894>>, <<19.479160,-762.370300,49.258488>>, 26.500000, FALSE, FALSE)
	
		SET_PED_NON_CREATION_AREA(vMin, vMax)
		SET_PED_PATHS_IN_AREA(vMin, vMax, FALSE)
		
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vMin, vMax, FALSE)
		REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(vMin, vMax)

		IF sbi_outside_fib_building = NULL
			sbi_outside_fib_building = ADD_SCENARIO_BLOCKING_AREA(vMin, vMax)
		ENDIF

		CLEAR_AREA(<<124.238640,-690.152954,113.714790>>, 250, FALSE, FALSE)
	
	ELSE
	
		// Underpass
		SET_ROADS_IN_ANGLED_AREA(<<178.629013,-798.900269,29.817137>>, <<298.631531,-481.388367,48.373196>>, 38.875000, FALSE, TRUE)
		// Slip to under
		SET_ROADS_IN_ANGLED_AREA(<<5.683404,-534.347595,32.359512>>, <<255.550278,-546.460632,48.292702>>, 18.937500, 	FALSE, TRUE)
		// CIA Side
		SET_ROADS_IN_ANGLED_AREA(<<229.966644,-618.448792,39.434505>>, <<119.966393,-577.765564,48.681129>>, 18.937500, FALSE, TRUE)
		// Main Stretch
		SET_ROADS_IN_ANGLED_AREA(<<-28.650225,-938.397888,27.670048>>, <<181.116150,-353.392487,49.062332>>, 40.250000, FALSE, TRUE)
		// Maze Union
		SET_ROADS_IN_ANGLED_AREA(<<-203.868988,-676.714417,32.087894>>, <<19.479160,-762.370300,49.258488>>, 26.500000, FALSE, TRUE)

		CLEAR_PED_NON_CREATION_AREA()
		SET_PED_PATHS_BACK_TO_ORIGINAL(vMin, vMax)
		
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vMin, vMax, TRUE)
		
		IF sbi_outside_fib_building != NULL
			REMOVE_SCENARIO_BLOCKING_AREA(sbi_outside_fib_building)
		ENDIF
	
	ENDIF
	
	// re-enable any road nodes on the highway that were accidentally deisabled.
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<382.245575,-510.350708,31.579483>>, <<-30.697897,-510.543488,40.224834>>, 49.625000)
	
ENDPROC

//PURPOSE: Safely open the main sequence
PROC SAFE_OPEN_SEQUENCE(BOOL bClearTask = TRUE)
	IF bClearTask
		CLEAR_SEQUENCE_TASK(seq)
	ENDIF
	IF NOT bSequenceOpen
		OPEN_SEQUENCE_TASK(seq)
		bSequenceOpen = TRUE
	ENDIF
ENDPROC

//PURPOSE: Safely close the main sequence
PROC SAFE_CLOSE_SEQUENCE()
	IF bSequenceOpen
		CLOSE_SEQUENCE_TASK(seq)
		bSequenceOpen = FALSE
	ENDIF
ENDPROC

//PURPOSE: Makes sure a sequence is closed before using it. Also auto cleans the seqence if required
PROC SAFE_PERFORM_SEQUENCE(PED_INDEX ped, BOOL bCleanupSequence = TRUE)
	SAFE_CLOSE_SEQUENCE()
	IF NOT IS_PED_INJURED(ped)
		//CLEAR_PED_TASKS(ped)
		TASK_PERFORM_SEQUENCE(ped, seq)
	ENDIF
	IF bCleanupSequence
		CLEAR_SEQUENCE_TASK(seq)
	ENDIF
ENDPROC

//PURPOSE: Safely remove blip
PROC SAFE_REMOVE_BLIP(BLIP_INDEX &blip)
	IF DOES_BLIP_EXIST(blip)
		REMOVE_BLIP(blip)
	ENDIF
ENDPROC

//PURPOSE: Safely blip a coordindate
PROC SAFE_BLIP_COORD(BLIP_INDEX &blip, VECTOR vec, BOOL bShowRoute = FALSE)
	SAFE_REMOVE_BLIP(blip)
	blip = CREATE_BLIP_FOR_COORD(vec, bShowRoute)
ENDPROC

//PURPOSE: Safely blip a ped
PROC SAFE_BLIP_PED(BLIP_INDEX &blip, PED_INDEX ped, BOOL bIsEnemy = FALSE)
	IF NOT IS_PED_INJURED(ped)
		//SAFE_REMOVE_BLIP(blip)
		// bug 1922063
		IF NOT DOES_BLIP_EXIST(blip)
			blip = CREATE_BLIP_FOR_PED(ped, bIsEnemy)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Safely blip a vehicle
PROC SAFE_BLIP_VEHICLE(BLIP_INDEX &blip, VEHICLE_INDEX veh, BOOL bIsEnemy = FALSE)
	IF IS_VEHICLE_DRIVEABLE(veh)
		SAFE_REMOVE_BLIP(blip)
		blip = CREATE_BLIP_FOR_VEHICLE(veh, bIsEnemy)
	ENDIF
ENDPROC

// Enable/disable peds blips in the new blip system
PROC SET_PED_AUTO_BLIPS_ACTIVE(PED_STRUCT &ped, BOOL bActive)
	ped.bAIBlipsActive = bActive
ENDPROC

//PURPOSE: Deletes the specified vehicle safely.
PROC SAFE_DELETE_VEHICLE(VEH_STRUCT &vehicle)
	IF DOES_ENTITY_EXIST(vehicle.id)
		INT x
		FOR x = 0 TO 3
			IF DOES_ENTITY_EXIST(vehicle.peds[x].id)
			AND IS_PED_IN_VEHICLE(vehicle.peds[x].id, vehicle.id)
				DELETE_PED(vehicle.peds[x].id)
			ENDIF
		ENDFOR
	
		VEHICLE_SEAT seat
		FOR seat = VS_DRIVER to VS_BACK_RIGHT
			PED_INDEX ped = GET_PED_IN_VEHICLE_SEAT(vehicle.id, seat)
			IF DOES_ENTITY_EXIST(ped)
			AND IS_ENTITY_A_MISSION_ENTITY(ped)
				DELETE_PED(ped)
			ENDIF
		ENDFOR
		DELETE_VEHICLE(vehicle.id)
	ENDIF
ENDPROC

//PURPOSE: Create a coverpoint and delete any existing one there already
PROC SAFE_CREATE_COVERPOINT(COVERPOINT_INDEX &cov, VECTOR vCoord, FLOAT fDir, COVERPOINT_USAGE usage, COVERPOINT_HEIGHT height, COVERPOINT_ARC arc)
	IF cov != null
		REMOVE_COVER_POINT(cov)
	ENDIF

	IF NOT DOES_SCRIPTED_COVER_POINT_EXIST_AT_COORDS(vCoord)
		cov = ADD_COVER_POINT(vCoord, fDir, usage, height, arc)
	ENDIF
ENDPROC

PROC CREATE_COVERPOINT_PAIR(COVERPOINT_PAIR &sCover, VECTOR vCoord, FLOAT fDir, COVERPOINT_USAGE usage, COVERPOINT_HEIGHT height, COVERPOINT_ARC arc)
	sCover.id = 1 - sCover.id
	SAFE_CREATE_COVERPOINT(sCover.covID[sCover.id], vCoord, fDir, usage, height, arc)
ENDPROC

PROC RESET_COVERPOINT_PAIR(COVERPOINT_PAIR &sCover)

	sCover.id = 0
	IF sCover.covID[0] != null
		REMOVE_COVER_POINT(sCover.covID[0])
	ENDIF
	sCover.covID[0] = null
	
	IF sCover.covID[1] != null
		REMOVE_COVER_POINT(sCover.covID[1])
	ENDIF
	sCover.covID[1] = null

ENDPROC

// Checks if a world point lies within a given screen area
FUNC BOOL IS_POINT_IN_SCREEN_AREA(vector vWorldCoord, float x1, float y1, float x2, float y2)
      float sX, sY
      GET_SCREEN_COORD_FROM_WORLD_COORD(vWorldCoord,sX,sY)
      if sX>= x1 and sX<=x2
            if sY>=y1  and sy<=y2
                  return TRUE
            ENDIF
      ENDIF
      return FALSE
ENDFUNC

//PURPOSE: Skips to a time position in recorded vehicle
PROC Jump_To_Time_In_Recording(VEHICLE_INDEX veh, FLOAT fTime)
	IF DOES_ENTITY_EXIST(veh)
	AND IS_VEHICLE_DRIVEABLE(veh)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
			FLOAT fCurrentTime = GET_TIME_POSITION_IN_RECORDING(veh)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(veh, fTime - fCurrentTime)
		ENDIF
	ENDIF
ENDPROC

//PURPOS: Sets the gameplay camera'heading relative to the world coordinate system (not relative to the players local heading)
PROC SET_GAMEPLAY_CAM_WORLD_HEADING(FLOAT fHeading = 0.0)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(fHeading - GET_ENTITY_HEADING(PLAYER_PED_ID()))
ENDPROC

//PURPOSE: Returns the world heading of the gameplay camera.
FUNC FLOAT GET_GAMEPLAY_CAM_WORLD_HEADING()
	RETURN WRAP(GET_GAMEPLAY_CAM_RELATIVE_HEADING() + GET_ENTITY_HEADING(PLAYER_PED_ID()),-180,180)
ENDFUNC

//PURPOSE: Points the gameplay cam heading at a target coordinate given in wolrd coords
PROC SET_GAMEPLAY_CAM_LOOK_AT_WORLD_TARGET(VECTOR vTarget)
	VECTOR vLookAt = vTarget - GET_ENTITY_COORDS(PLAYER_PED_ID())
	SET_GAMEPLAY_CAM_WORLD_HEADING(GET_HEADING_FROM_VECTOR_2D(vLookAt.x, vLookAt.y))
ENDPROC

//PURPOSE: Return the specified coord with an adjusted z to match the ground below
FUNC VECTOR GET_ACTUAL_GROUND_COORD(VECTOR vCoord)
	FLOAT fGroundZ
	GET_GROUND_Z_FOR_3D_COORD(vCoord, fGroundZ)
	
	vCoord.z = fGroundZ
	
	RETURN vCoord
ENDFUNC

FUNC BOOL DOES_CONVO_EXIST(STRING strConvo)
	
	TEXT_LABEL_23 strTest = strConvo
	strTest += "SL"
	
	IF DOES_TEXT_LABEL_EXIST(strTest)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Adds all peds required for dialogue.
PROC Add_Peds_For_Dialogue()
	IF DOES_ENTITY_EXIST(MIKE_PED_ID())
		ADD_PED_FOR_DIALOGUE(sConvo, 0, MIKE_PED_ID(), "MICHAEL")
		STOP_PED_SPEAKING(MIKE_PED_ID(), TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(FRANK_PED_ID())
		ADD_PED_FOR_DIALOGUE(sConvo, 1, FRANK_PED_ID(), "FRANKLIN")
		STOP_PED_SPEAKING(FRANK_PED_ID(), TRUE)
	ENDIF
	
	// PILOT
	IF DOES_ENTITY_EXIST(peds[mpf_pilot].id)
		ADD_PED_FOR_DIALOGUE(sConvo, 2, peds[mpf_pilot].id, "PILOTAH3")
	ELSE
		ADD_PED_FOR_DIALOGUE(sConvo, 2, null, "PILOTAH3")
	ENDIF

	// HACKER
	SWITCH crewHacker
		CASE CM_HACKER_B_RICKIE_UNLOCK
			IF DOES_ENTITY_EXIST(peds[mpf_hacker].id)
				ADD_PED_FOR_DIALOGUE(sConvo, 3, peds[mpf_hacker].id, "LIEngineer")
			ELSE
				ADD_PED_FOR_DIALOGUE(sConvo, 3, null, "LIEngineer")
			ENDIF
		BREAK
		CASE CM_HACKER_M_CHRIS
			IF DOES_ENTITY_EXIST(peds[mpf_hacker].id)
				ADD_PED_FOR_DIALOGUE(sConvo, 3, peds[mpf_hacker].id, "CHRISTIAN")
			ELSE
				ADD_PED_FOR_DIALOGUE(sConvo, 3, null, "CHRISTIAN")
			ENDIF
		BREAK
		CASE CM_HACKER_G_PAIGE
			IF DOES_ENTITY_EXIST(peds[mpf_hacker].id)
				ADD_PED_FOR_DIALOGUE(sConvo, 3, peds[mpf_hacker].id, "PAIGE")
			ELSE
				ADD_PED_FOR_DIALOGUE(sConvo, 3, null, "PAIGE")
			ENDIF
		BREAK
	ENDSWITCH
	
	// DRIVER
	SWITCH crewDriver
		CASE CM_DRIVER_B_KARIM
			IF DOES_ENTITY_EXIST(peds[mpf_driver].id)
				ADD_PED_FOR_DIALOGUE(sConvo, 4, peds[mpf_driver].id, "KARIM")
			ELSE
				ADD_PED_FOR_DIALOGUE(sConvo, 4, null, "KARIM")
			ENDIF
		BREAK
		CASE CM_DRIVER_G_EDDIE
			IF DOES_ENTITY_EXIST(peds[mpf_driver].id)
				ADD_PED_FOR_DIALOGUE(sConvo, 4, peds[mpf_driver].id, "EDDIE")
			ELSE
				ADD_PED_FOR_DIALOGUE(sConvo, 4, null, "EDDIE")
			ENDIF
		BREAK
		CASE CM_DRIVER_G_TALINA_UNLOCK
			IF DOES_ENTITY_EXIST(peds[mpf_driver].id)
				ADD_PED_FOR_DIALOGUE(sConvo, 4, peds[mpf_driver].id, "TALINA")
			ELSE
				ADD_PED_FOR_DIALOGUE(sConvo, 4, null, "TALINA")
			ENDIF
		BREAK
	ENDSWITCH
	
	// GUNMAN
	IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
		SWITCH crewGoon
			CASE CM_GUNMAN_B_DARYL
				ADD_PED_FOR_DIALOGUE(sConvo, 5, peds[mpf_goon].id, "DARYL")
			BREAK
			CASE CM_GUNMAN_B_NORM
				ADD_PED_FOR_DIALOGUE(sConvo, 5, peds[mpf_goon].id, "NORM")
			BREAK
			CASE CM_GUNMAN_M_HUGH
				ADD_PED_FOR_DIALOGUE(sConvo, 5, peds[mpf_goon].id, "HUGH")
			BREAK
			CASE CM_GUNMAN_G_CHEF_UNLOCK
				ADD_PED_FOR_DIALOGUE(sConvo, 5, peds[mpf_goon].id, "CHEF")
			BREAK
			CASE CM_GUNMAN_G_GUSTAV
				ADD_PED_FOR_DIALOGUE(sConvo, 5, peds[mpf_goon].id, "GUSTAVO")
			BREAK
			CASE CM_GUNMAN_G_KARL
				ADD_PED_FOR_DIALOGUE(sConvo, 5, peds[mpf_goon].id, "KARL")
			BREAK
			CASE CM_GUNMAN_G_PACKIE_UNLOCK
				ADD_PED_FOR_DIALOGUE(sConvo, 5, peds[mpf_goon].id, "PACKIE")
			BREAK
		ENDSWITCH
	ENDIF

ENDPROC

//PURPOSE: Removes all peds from dialogue conversation struct.
PROC Remove_All_Peds_For_Dialogue()
	REMOVE_PED_FOR_DIALOGUE(sConvo, 0)
	REMOVE_PED_FOR_DIALOGUE(sConvo, 1)
	REMOVE_PED_FOR_DIALOGUE(sConvo, 2)
	REMOVE_PED_FOR_DIALOGUE(sConvo, 3)
	REMOVE_PED_FOR_DIALOGUE(sConvo, 4)
	REMOVE_PED_FOR_DIALOGUE(sConvo, 5)
ENDPROC

FUNC CrewMemberSkill GET_DRIVER_SKILL_LEVEL()

	CrewMemberSkill eDriverSkill = GET_CREW_MEMBER_SKILL(crewDriver)
	IF eDriverSkill = CMSK_MEDIUM
		eDriverSkill = CMSK_BAD
	ENDIF
	
	RETURN eDriverSkill
ENDFUNC

PROC CREW_SET_PARAMS(PED_STRUCT &ped, INT iAccuracy, BOOL bBlip, BOOL bRelGroupIgnore)

	CPRINTLN(DEBUG_MIKE, "CREW_SET_PARAMS() - Ped ID: ", NATIVE_TO_INT(ped.id))

	IF bBlip
		IF DOES_BLIP_EXIST(ped.objective_blip)
			REMOVE_BLIP(ped.objective_blip)
		ENDIF
		SAFE_BLIP_PED(ped.objective_blip, ped.id, FALSE)
	ENDIF
	SET_PED_ACCURACY(ped.id, iAccuracy)
	
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.id, TRUE)
	SET_PED_CAN_BE_TARGETTED(ped.id, false)
	SET_PED_SUFFERS_CRITICAL_HITS(ped.id, FALSE)
	//SET_ENTITY_HEALTH(ped.id, 800)
	SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(ped.id, FALSE)
	SET_RAGDOLL_BLOCKING_FLAGS(ped.id, RBF_BULLET_IMPACT)
	SET_PED_COMBAT_ATTRIBUTES(ped.id, CA_DISABLE_PINNED_DOWN, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(ped.id, CA_DISABLE_PIN_DOWN_OTHERS, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(ped.id, CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
	SET_PED_CONFIG_FLAG(ped.id, PCF_DisableHurt, TRUE)
	
	IF bRelGroupIgnore
		SET_PED_RELATIONSHIP_GROUP_HASH(ped.id, REL_CREW_IGNORE)
	ELSE
		SET_PED_RELATIONSHIP_GROUP_HASH(ped.id, REL_CREW)
	ENDIF

ENDPROC

//PURPOSE: Creates a specified crew member, can be either a player crew member or a hired crew member.
FUNC BOOL CREATE_CREW_MEMBER(PED_STRUCT &ped, CREW_MEMBER crewID, VECTOR vSpawnCoord, FLOAT fSpawnHeading, HeistCrewOutfit crewOutfit = CREW_OUTFIT_STEALTH, INT iAccuracy = 5, BOOL bWait = FALSE, BOOL bCleanUpModel = TRUE, BOOL bBlip = FALSE, BOOL bRelGroupIgnore = FALSE)
	IF NOT DOES_ENTITY_EXIST(ped.id)
	
		MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
		PED_COMP_NAME_ENUM	outfit
	
		// Selector ped crew member
		IF enum_to_int(crewID) <= enum_to_int(cmf_frank)
		
			enumCharacterList 	crewChar
			SWITCH crewID
				CASE cmf_mike	
					crewChar	= CHAR_MICHAEL
					IF crewOutfit = CREW_OUTFIT_STEALTH
						outfit = OUTFIT_P0_STEALTH
					ELSE
						outfit = OUTFIT_P0_DEFAULT
					ENDIF
				BREAK
				CASE cmf_frank		
					crewChar	= CHAR_FRANKLIN		
					IF crewOutfit = CREW_OUTFIT_STEALTH
						outfit = OUTFIT_P1_STEALTH
					ELSE
						outfit = OUTFIT_P1_DEFAULT
					ENDIF
				BREAK
			ENDSWITCH
			
			IF bWait
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(ped.id, crewChar, vSpawnCoord, fSpawnHeading, bCleanUpModel)
					WAIT(0)
				ENDWHILE
				IF outfit != OUTFIT_P0_DEFAULT
				AND outfit != OUTFIT_P1_DEFAULT
					SET_PED_COMP_ITEM_CURRENT_SP(ped.id, COMP_TYPE_OUTFIT, outfit, FALSE)
				ENDIF
			ELSE
				IF CREATE_PLAYER_PED_ON_FOOT(ped.id, crewChar, vSpawnCoord, fSpawnHeading, bCleanUpModel)
					IF outfit != OUTFIT_P0_DEFAULT
					AND outfit != OUTFIT_P1_DEFAULT
						SET_PED_COMP_ITEM_CURRENT_SP(ped.id, COMP_TYPE_OUTFIT, outfit, FALSE)
					ENDIF
				ENDIF
			ENDIF

		// Heist crew member
		ELSE
		
			CrewMember crewMemID
			SWITCH crewID
				CASE cmf_hacker		
					crewMemID = crewHacker
				BREAK
				CASE cmf_goon		
					crewMemID = crewGoon
				BREAK
				CASE cmf_driver		
					crewMemID = crewDriver
				BREAK
			ENDSWITCH
			
			model = GET_CREW_MEMBER_MODEL(crewMemID)
			Load_Asset_Model(sAssetData, model)
			// Create the crew member
			IF bWait
				WHILE NOT HAS_MODEL_LOADED(model)
					WAIT(0)
				ENDWHILE
				
				ped.id = CREATE_HEIST_CREW_MEMBER(crewMemID, vSpawnCoord, fSpawnHeading, crewOutfit, TRUE)
				
			ELIF HAS_MODEL_LOADED(model)
				ped.id = CREATE_HEIST_CREW_MEMBER(crewMemID, vSpawnCoord, fSpawnHeading, crewOutfit, TRUE)
			ENDIF
		ENDIF
		
		// Ped has been created now, set up relationships and stuff
		IF DOES_ENTITY_EXIST(ped.id)
			
			CREW_SET_PARAMS(ped, iAccuracy, bBlip, bRelGroupIgnore)
			
			// Clean up model if used
			IF bCleanUpModel
			AND model != DUMMY_MODEL_FOR_SCRIPT
				Unload_Asset_Model(sAssetData, model)
			ENDIF
			
			RETURN TRUE
		
		// Not yet created still waiting
		ELSE
			RETURN FALSE
		ENDIF
				
	// Entity already exists
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC

//PURPOSE: Creates a specified crew member, can be either a player crew member or a hired crew member.
FUNC BOOL CREATE_CREW_MEMBER_IN_VEHICLE(PED_STRUCT &ped, CREW_MEMBER crewID, VEH_STRUCT &veh, VEHICLE_SEAT vs, HeistCrewOutfit crewOutfit = CREW_OUTFIT_STEALTH, INT iAccuracy = 5, BOOL bWait = FALSE, BOOL bCleanUpModel = TRUE, BOOL bBlip = FALSE, BOOL bRelGroupIgnore = FALSE)
	IF NOT DOES_ENTITY_EXIST(ped.id)
	AND IS_VEHICLE_DRIVEABLE(veh.id)
	AND IS_VEHICLE_SEAT_FREE(veh.id, vs)
	
		MODEL_NAMES model = DUMMY_MODEL_FOR_SCRIPT
		PED_COMP_NAME_ENUM	outfit
	
		// Selector ped crew member
		IF enum_to_int(crewID) <= enum_to_int(cmf_frank)
		
			enumCharacterList 	crewChar
			SWITCH crewID
				CASE cmf_mike	
					crewChar	= CHAR_MICHAEL
					IF crewOutfit = CREW_OUTFIT_STEALTH
						outfit = OUTFIT_P0_STEALTH
					ELSE
						outfit = OUTFIT_P0_DEFAULT
					ENDIF
				BREAK
				CASE cmf_frank		
					crewChar	= CHAR_FRANKLIN		
					IF crewOutfit = CREW_OUTFIT_STEALTH
						outfit = OUTFIT_P1_STEALTH
					ELSE
						outfit = OUTFIT_P1_DEFAULT
					ENDIF
				BREAK
			ENDSWITCH
			
			IF bWait
				WHILE IS_VEHICLE_DRIVEABLE(veh.id)
				AND NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(ped.id, crewChar, veh.id, vs, bCleanUpModel)
					WAIT(0)
				ENDWHILE
				//SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(crewChar), COMP_TYPE_OUTFIT, outfit, TRUE)
				SET_PED_COMP_ITEM_CURRENT_SP(ped.id, COMP_TYPE_OUTFIT, outfit, FALSE)
			ELSE
				IF CREATE_PLAYER_PED_INSIDE_VEHICLE(ped.id, crewChar, veh.id, vs, bCleanUpModel)
					//SET_PED_COMP_ITEM_ACQUIRED_SP(GET_PLAYER_PED_MODEL(crewChar), COMP_TYPE_OUTFIT, outfit, TRUE)
					SET_PED_COMP_ITEM_CURRENT_SP(ped.id, COMP_TYPE_OUTFIT, outfit, FALSE)
				ENDIF
			ENDIF

		// Heist crew member
		ELSE
		
			CrewMember crewMemID
			SWITCH crewID
				CASE cmf_hacker		
					crewMemID = crewHacker
				BREAK
				CASE cmf_goon		
					crewMemID = crewGoon
				BREAK
				CASE cmf_driver		
					crewMemID = crewDriver
				BREAK
			ENDSWITCH
			
			model = GET_CREW_MEMBER_MODEL(crewMemID)
			Load_Asset_Model(sAssetData, model)
			// Create the crew member
			IF bWait
				WHILE NOT HAS_MODEL_LOADED(model)
					WAIT(0)
				ENDWHILE
				
				IF IS_VEHICLE_DRIVEABLE(veh.id)
					ped.id = CREATE_HEIST_CREW_MEMEBER_IN_VEHICLE(crewMemID, veh.id, vs, crewOutfit, TRUE)
				ENDIF
				
				
			ELIF HAS_MODEL_LOADED(model)
				ped.id = CREATE_HEIST_CREW_MEMEBER_IN_VEHICLE(crewMemID, veh.id, vs, crewOutfit, TRUE)
			ENDIF
		ENDIF
		
		// Ped has been created now, set up relationships and stuff
		IF DOES_ENTITY_EXIST(ped.id)
			
			CREW_SET_PARAMS(ped, iAccuracy, bBlip, bRelGroupIgnore)
					
			// Clean up model if used
			IF bCleanUpModel
			AND model != DUMMY_MODEL_FOR_SCRIPT
				Unload_Asset_Model(sAssetData, model)
			ENDIF
			
			RETURN TRUE
		
		// Not yet created still waiting
		ELSE
			RETURN FALSE
		ENDIF
				
	// Entity already exists
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC

//PURPOSE: Sets the currently selected selector ped
FUNC BOOL SET_CURRENT_PLAYER_PED(SELECTOR_SLOTS_ENUM pedChar, BOOL bWait = FALSE, BOOL bCleanUpModel = TRUE)
	
	// SELECTOR_PED_MULTIPLAYER NOT ALLOWED!
	IF pedChar = SELECTOR_PED_MULTIPLAYER 
		SCRIPT_ASSERT("INVALID SELECTOR PED: Cannot select SELECTOR_PED_MULTIPLAYER") 
		RETURN FALSE 	
	ENDIF
	
	IF bWait
		WHILE NOT SET_CURRENT_SELECTOR_PED(pedChar, bCleanUpModel)	
			WAIT(0)
		ENDWHILE
	ELSE
		IF NOT SET_CURRENT_SELECTOR_PED(pedChar, bCleanUpModel)
			RETURN FALSE
		ENDIF
	ENDIF
			
	// Additional stuff to set up ped for use in mission
	SWITCH pedChar
		CASE SELECTOR_PED_MICHAEL
			peds[mpf_mike].id = PLAYER_PED_ID()
			sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = peds[mpf_mike].id
		BREAK
		CASE SELECTOR_PED_FRANKLIN
			peds[mpf_frank].id = PLAYER_PED_ID()
			sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = peds[mpf_frank].id
		BREAK
	ENDSWITCH
	
	IF sSelectorPeds.ePreviousSelectorPed != pedChar
		MISSION_PED_FLAGS pedFlag
		SWITCH sSelectorPeds.ePreviousSelectorPed
			CASE SELECTOR_PED_MICHAEL
				pedFlag = mpf_mike
			BREAK
			CASE SELECTOR_PED_FRANKLIN
				pedFlag = mpf_frank
			BREAK
		ENDSWITCH
		peds[pedFlag].id = NULL
		SAFE_REMOVE_BLIP(peds[pedFlag].objective_blip)
	ENDIF
	
	Add_Peds_For_Dialogue()
	
	RETURN TRUE
ENDFUNC

PROC SET_MISSION_PED_CREATION_PARAMS(PED_STRUCT &ped, STRING debugName, REL_GROUP_HASH relGroup, WEAPON_TYPE weapon = WEAPONTYPE_UNARMED, WEAPONCOMPONENT_TYPE weaponComp = WEAPONCOMPONENT_INVALID, INT accuracy = 5, INT health = -1, INT armour = DEFAULT_MISSION_PED_ARMOUR, BOOL bFlashLightOnByDefault = TRUE, BOOL bUseActionMode = TRUE)

	SET_PED_RELATIONSHIP_GROUP_HASH(ped.id, relGroup)
	IF weapon != WEAPONTYPE_INVALID
	AND weapon != WEAPONTYPE_UNARMED
		GIVE_WEAPON_TO_PED(ped.id , weapon, INFINITE_AMMO, true)
	ENDIF
	IF weaponComp != WEAPONCOMPONENT_INVALID
		GIVE_WEAPON_COMPONENT_TO_PED(ped.id, weapon, weaponComp)
	ENDIF
	
	SET_PED_FLASH_LIGHT(ped, bFlashLightOnByDefault)
	
	IF health != -1
		SET_ENTITY_HEALTH(ped.id, health)
	ENDIF
	IF armour != -1
		SET_PED_ARMOUR(ped.id, armour)
	ENDIF
	
	SET_PED_MONEY(ped.id, 0)
	SET_PED_ACCURACY(ped.id, accuracy)
	SET_PED_KEEP_TASK(ped.id, TRUE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.id , true)
	SET_PED_NAME_DEBUG(ped.id, debugName)
	
	IF bUseActionMode
		SET_PED_USING_ACTION_MODE(ped.id, TRUE, -1, "DEFAULT_ACTION")
	ENDIF
	
	SET_PED_AUTO_BLIPS_ACTIVE(ped, TRUE)
	ADD_DEADPOOL_TRIGGER(ped.id, AH3B_KILLS)
	INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(ped.id)
	
ENDPROC

//PURPOSE: Creates and sets up a ped for use in the mission
PROC CREATE_MISSION_PED(PED_STRUCT &ped, MODEL_NAMES model, VECTOR position, FLOAT heading, STRING debugName, REL_GROUP_HASH relGroup, WEAPON_TYPE weapon = WEAPONTYPE_UNARMED, WEAPONCOMPONENT_TYPE weaponComp = WEAPONCOMPONENT_INVALID, INT accuracy = 5, INT health = -1, INT armour = DEFAULT_MISSION_PED_ARMOUR, BOOL bFlashLightOnByDefault = TRUE, BOOL bUseActionMode = TRUE)
	
	ped.id = CREATE_PED(PEDTYPE_MISSION, model, position, heading)
	SET_MISSION_PED_CREATION_PARAMS(ped, debugName, relGroup, weapon, weaponComp, accuracy, health, armour, bFlashLightOnByDefault, bUseActionMode)
	
	IF model = mod_fib_agent_3
		SET_PED_COMPONENT_VARIATION(ped.id, PED_COMP_SPECIAL2, 2, 0)
	ELIF model = mod_swat
		SET_PED_COMPONENT_VARIATION(ped.id, PED_COMP_DECL, 0, 1)
	ENDIF

ENDPROC

//PURPOSE: Creates and sets up a ped inside a vehicle for use in the mission
PROC CREATE_MISSION_PED_IN_VEHICLE(PED_STRUCT &ped, MODEL_NAMES model, VEH_STRUCT &veh, VEHICLE_SEAT seat, STRING debugName, REL_GROUP_HASH relGroup, WEAPON_TYPE weapon = WEAPONTYPE_UNARMED, WEAPONCOMPONENT_TYPE weaponComp = WEAPONCOMPONENT_INVALID, INT accuracy = 5, INT health = -1, INT armour = DEFAULT_MISSION_PED_ARMOUR, BOOL bFlashLightOnByDefault = TRUE, BOOL bUseActionMode = TRUE)
	
	ped.id = CREATE_PED_INSIDE_VEHICLE(veh.id, PEDTYPE_MISSION, model, seat)
	SET_MISSION_PED_CREATION_PARAMS(ped, debugName, relGroup, weapon, weaponComp, accuracy, health, armour, bFlashLightOnByDefault, bUseActionMode)

ENDPROC

//PURPOSE: Creates a custom cover point associated with a ped
PROC CREATE_PED_COVER_POINT(PED_STRUCT &ped, VECTOR vCovCoord, FLOAT fCovDir, COVERPOINT_USAGE usage, COVERPOINT_HEIGHT height, COVERPOINT_ARC arc)
	IF ped.bDoesCovExist
		REMOVE_COVER_POINT(ped.cov)
	ENDIF
	ped.cov = ADD_COVER_POINT(vCovCoord, fCovDir, usage, height, arc)
	ped.bDoesCovExist = TRUE
ENDPROC

PROC ADD_CREW_SHOOTOUT_COVER_POINTS()
	Create_Ped_Cover_Point(peds[mpf_mike], 	v_ShootoutCoverMike, 	69.9150, COVUSE_WALLTOLEFT, 	COVHEIGHT_TOOHIGH, COVARC_120)
	Create_Ped_Cover_Point(peds[mpf_frank], v_ShootoutCoverFrank, 	69.9150, COVUSE_WALLTORIGHT, 	COVHEIGHT_LOW, COVARC_120)
	Create_Ped_Cover_Point(peds[mpf_goon], 	v_ShootoutCoverGoon, 	69.9150, COVUSE_WALLTOLEFT, 	COVHEIGHT_LOW, COVARC_120)
ENDPROC

//PURPOSE: Sets the combat parameters of the ped
PROC SET_PED_COMBAT_PARAMS(PED_STRUCT &ped, VECTOR defensiveSphereCoord, FLOAT defensiveSphereRadius, COMBAT_MOVEMENT cm = CM_DEFENSIVE, COMBAT_RANGE cr = CR_MEDIUM, COMBAT_TARGET_LOSS_RESPONSE tlr = TLR_NEVER_LOSE_TARGET, BOOL bBlockNonTempEvents = TRUE, FLOAT fChanceOfStrafing = 1.0, FLOAT fChanceOfWalkingWhileStrafing = 0.0, BOOL bOpenCombatWhenDefAreaReached = FALSE, BOOL bSetPedDiesWhenInjured = FALSE, BOOL bCharge = FALSE, BOOL bChargeNow = FALSE)
	IF NOT IS_PED_INJURED(ped.id)
		SET_PED_COMBAT_MOVEMENT(ped.id, cm)
		SET_PED_COMBAT_RANGE(ped.id, cr)
		SET_PED_TARGET_LOSS_RESPONSE(ped.id, tlr)
		SET_COMBAT_FLOAT(ped.id, CCF_STRAFE_WHEN_MOVING_CHANCE, fChanceOfStrafing)
		SET_COMBAT_FLOAT(ped.id, CCF_WALK_WHEN_STRAFING_CHANCE, fChanceOfWalkingWhileStrafing)
		SET_PED_COMBAT_ATTRIBUTES(ped.id, CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, bOpenCombatWhenDefAreaReached)
		IF bSetPedDiesWhenInjured
			SET_PED_CONFIG_FLAG(ped.id, PCF_DisableHurt, bSetPedDiesWhenInjured)
		ENDIF
		SET_PED_DIES_WHEN_INJURED(ped.id, bSetPedDiesWhenInjured)
		SET_PED_COMBAT_ATTRIBUTES(ped.id, CA_CAN_CHARGE, bCharge)
		SET_PED_CONFIG_FLAG(ped.id, PCF_ShouldChargeNow, bChargeNow)
		
		IF NOT IS_VECTOR_ZERO(defensiveSphereCoord) AND defensiveSphereRadius != 0.0
			SET_PED_SPHERE_DEFENSIVE_AREA(ped.id, defensiveSphereCoord, defensiveSphereRadius, TRUE)
		ENDIF
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped.id, bBlockNonTempEvents)
	ENDIF
ENDPROC

//PURPOSE: Tasks a ped into a custom cover point created as needed
PROC Task_Put_Ped_Directly_Into_Custom_Cover(PED_STRUCT &ped, VECTOR vCoverPos, FLOAT fCovDir, INT Time, COVERPOINT_USAGE usage, COVERPOINT_HEIGHT height, COVERPOINT_ARC arc, BOOL CanPeekAndAim = FALSE, FLOAT BlendInDuration = 0.0, bool ForceInitialFacingDirection = FALSE, bool bForceFaceLeft = FALSE)
	CREATE_PED_COVER_POINT(ped, vCoverPos, fCovDir, usage, height, arc)
	IF bSequenceOpen
		TASK_PUT_PED_DIRECTLY_INTO_COVER(NULL, vCoverPos, Time, CanpeekAndAim, BlendInDuration, ForceInitialFacingDirection, bForceFaceLeft, ped.cov)
	ELSE
		TASK_PUT_PED_DIRECTLY_INTO_COVER(ped.id, vCoverPos, Time, CanpeekAndAim, BlendInDuration, ForceInitialFacingDirection, bForceFaceLeft, ped.cov)
	ENDIF
ENDPROC

PROC LOAD_CREW_WEAPON_ASSETS()

	EXTRA_WEAPON_COMPONENT_RESOURCE_FLAGS weapCompsCarbine
	EXTRA_WEAPON_COMPONENT_RESOURCE_FLAGS weapCompsAlt
	weapCompsCarbine = WEAPON_COMPONENT_FLASH | WEAPON_COMPONENT_SUPP
	
	// add additional components to enum
	IF weap_Goon = WEAPONTYPE_CARBINERIFLE
		IF weapComp_GoonClip != WEAPONCOMPONENT_INVALID
			weapCompsCarbine = weapCompsCarbine | WEAPON_COMPONENT_SCLIP2
		ENDIF
		IF weapComp_GoonScope != WEAPONCOMPONENT_INVALID
			weapCompsCarbine = weapCompsCarbine | WEAPON_COMPONENT_SCOPE
		ENDIF
		IF weapComp_GoonGrip != WEAPONCOMPONENT_INVALID
			weapCompsCarbine = weapCompsCarbine | WEAPON_COMPONENT_GRIP
		ENDIF
	
	// decide components and load separate weapon
	ELSE
		IF weapComp_GoonFlash != WEAPONCOMPONENT_INVALID
			weapCompsAlt = weapCompsAlt | WEAPON_COMPONENT_FLASH
		ENDIF
		IF weapComp_GoonSupp != WEAPONCOMPONENT_INVALID
			weapCompsAlt = weapCompsAlt | WEAPON_COMPONENT_SUPP
		ENDIF
		IF weapComp_GoonClip != WEAPONCOMPONENT_INVALID
			weapCompsAlt = weapCompsAlt | WEAPON_COMPONENT_SCLIP2
		ENDIF
		IF weapComp_GoonScope != WEAPONCOMPONENT_INVALID
			weapCompsAlt = weapCompsAlt | WEAPON_COMPONENT_SCOPE
		ENDIF
		IF weapComp_GoonGrip != WEAPONCOMPONENT_INVALID
			weapCompsAlt = weapCompsAlt | WEAPON_COMPONENT_GRIP
		ENDIF
		
		Load_Asset_Weapon_Asset(sAssetData, weap_Goon, WRF_REQUEST_ALL_ANIMS, weapCompsAlt)
	ENDIF
	Load_Asset_Weapon_Asset(sAssetData, WEAPONTYPE_CARBINERIFLE, WRF_REQUEST_ALL_ANIMS, weapCompsCarbine)

ENDPROC


PROC SAFE_GIVE_PED_WEAPON_AND_AMMO(PED_INDEX ped, WEAPON_TYPE weapon, INT iAmmo = 120, BOOL bEquip = TRUE)
	IF HAS_PED_GOT_WEAPON(ped, weapon)
		SET_CURRENT_PED_WEAPON(ped, weapon)
		SET_PED_AMMO(ped, weapon, iAmmo)
	ELSE
		GIVE_WEAPON_TO_PED(ped, weapon, iAmmo, bEquip, bEquip)
	ENDIF
ENDPROC

PROC SAFE_GIVE_PED_WEAPON_COMPONENT(PED_INDEX ped, WEAPON_TYPE weapon, WEAPONCOMPONENT_TYPE comp)
	IF comp != WEAPONCOMPONENT_INVALID
		IF NOT HAS_PED_GOT_WEAPON_COMPONENT(ped, weapon, comp)
			GIVE_WEAPON_COMPONENT_TO_PED(ped, weapon, comp)
		ENDIF
	ENDIF
ENDPROC

PROC GIVE_CREW_MEMBER_WEAPON(CREW_MEMBER crewMem, BOOL bIncludeSuppressor, BOOL bEquip = TRUE)

	SWITCH crewMem
		CASE cmf_mike
		
			SAFE_GIVE_PED_WEAPON_AND_AMMO(peds[mpf_mike].id, weap_Mike, ammo_Mike, bEquip)
			SAFE_GIVE_PED_WEAPON_COMPONENT(peds[mpf_mike].id, weap_Mike, WEAPONCOMPONENT_AT_AR_FLSH)
			IF bIncludeSuppressor
				SAFE_GIVE_PED_WEAPON_COMPONENT(peds[mpf_mike].id, weap_Mike, WEAPONCOMPONENT_AT_AR_SUPP)
			ENDIF
		
		BREAK
		CASE cmf_frank
		
			SAFE_GIVE_PED_WEAPON_AND_AMMO(peds[mpf_frank].id, weap_Frank, ammo_Frank, bEquip)
			SAFE_GIVE_PED_WEAPON_COMPONENT(peds[mpf_frank].id, weap_Frank, WEAPONCOMPONENT_AT_AR_FLSH)
			IF bIncludeSuppressor
				SAFE_GIVE_PED_WEAPON_COMPONENT(peds[mpf_frank].id, weap_Frank, WEAPONCOMPONENT_AT_AR_SUPP)
			ENDIF
		
		BREAK
		CASE cmf_goon
		
			SAFE_GIVE_PED_WEAPON_AND_AMMO(peds[mpf_goon].id, weap_Goon, ammo_Goon, bEquip)
			SAFE_GIVE_PED_WEAPON_COMPONENT(peds[mpf_goon].id, weap_Goon, weapComp_GoonFlash)
			SAFE_GIVE_PED_WEAPON_COMPONENT(peds[mpf_goon].id, weap_Goon, weapComp_GoonGrip)
			SAFE_GIVE_PED_WEAPON_COMPONENT(peds[mpf_goon].id, weap_Goon, weapComp_GoonScope)
			SAFE_GIVE_PED_WEAPON_COMPONENT(peds[mpf_goon].id, weap_Goon, weapComp_GoonClip)
			IF bIncludeSuppressor
				SAFE_GIVE_PED_WEAPON_COMPONENT(peds[mpf_goon].id, weap_Goon, weapComp_GoonSupp)
			ENDIF
		
		BREAK
		CASE cmf_driver
		
			SAFE_GIVE_PED_WEAPON_AND_AMMO(peds[mpf_driver].id, 	weap_Driver, 	ammo_Driver, bEquip)
		
		BREAK
	ENDSWITCH

ENDPROC

PROC SETUP_SPAWN_POINT(SPAWN_POINT_STRUCT &sSpawnPoint, VECTOR vCoord, FLOAT fHeading, BOOL bIsRoofPoint)

	sSpawnPoint.vCoord				= vCoord
	sSpawnPoint.fHeading			= fHeading
	sSpawnPoint.bIsRoofPoint		= bIsRoofPoint

ENDPROC

PROC SETUP_SHOOTOUT_DEFENSIVE_AREA(SHOOTOUT_DEFENSIVE_AREA_STRUCT &sDefensiveArea, VECTOR vCoord, FLOAT fRadius, BOOL bIsRoofPoint)

	sDefensiveArea.vCoord 			= vCoord
	sDefensiveArea.fRadius 			= fRadius
	sDefensiveArea.bIsRoofPoint		= bIsRoofPoint

ENDPROC

PROC SETUP_WAVE_PED(WAVE_PED_STRUCT &sWavePed, SPAWN_POINT_FLAG eSpawnPoint, DOWNLOAD_SHOOTOUT_DEFENSIVE_AREA_ENUM ePreferredDefensiveArea,
	MODEL_NAMES eModel, WEAPON_TYPE eWeapon, WEAPONCOMPONENT_TYPE eWeaponComp, INT iAccuracy, INT iHealth,
	INT iKillsSinceLast, INT iTimeSinceLast, FLOAT fForceInProgress, BOOL bWaitForGasGrenade,
	BOOL bOpenCombat, BOOL bCharge, BOOL bChargeNow)

	sWavePed.eSpawnPoint					= eSpawnPoint
	sWavePed.ePreferredDefensiveArea		= ePreferredDefensiveArea
	
	sWavePed.eModel							= eModel
	sWavePed.eWeapon						= eWeapon
	sWavePed.eWeaponComp					= eWeaponComp
	sWavePed.iAccuracy						= iAccuracy
	sWavePed.iHealth						= iHealth
	
	sWavePed.iKillsSinceLast				= iKillsSinceLast
	sWavePed.iTimeSinceLast					= iTimeSinceLast
	sWavePed.fProgress						= fForceInProgress
	sWavePed.bWaitForGasGrenade				= bWaitForGasGrenade
	
	sWavePed.bOpenCombat					= bOpenCombat
	sWavePed.bCharge						= bCharge
	sWavePed.bChargeNow						= bChargeNow
	
ENDPROC

PROC SETUP_EMERGENCY_SERVICE_STRUCT(EMERGENCY_SERVICE_ARRIVE_STRUCT &sServices, MISSION_VEHICLE_FLAGS eVeh, MODEL_NAMES eModel, INT iRecNumber, INT iStartTime, INT iPauseLength = 0)

	sServices.eVeh			= eVeh
	sServices.eModel 		= eModel
	sServices.iRecNumber	= iRecNumber
	sServices.iStartTime	= iStartTime
	sServices.iPauseLength	= iPauseLength
	
	sServices.iPauseTimer	= 0
	sServices.iStage		= 0
	sServices.bSpawned		= FALSE

ENDPROC

PROC SPAWN_WAVE_PED(INT iNewPed)

	VECTOR vDefensiveCoord
	FLOAT fDefensiveRadius
	COMBAT_MOVEMENT	cmMovement
	IF s_WavePed[iNewPed].ePreferredDefensiveArea != dsd_invalid
		vDefensiveCoord		= s_ShootOutDefensiveAreas[s_WavePed[iNewPed].ePreferredDefensiveArea].vCoord
		fDefensiveRadius	= s_ShootOutDefensiveAreas[s_WavePed[iNewPed].ePreferredDefensiveArea].fRadius
		cmMovement			= CM_DEFENSIVE
	ELSE
		vDefensiveCoord 	= <<0,0,0>>
		fDefensiveRadius 	= 0.0
		cmMovement			= CM_WILLADVANCE
	ENDIF

	TEXT_LABEL_23 strDebugName = "WAVEPED_"
	strDebugName += iNewPed
	CREATE_MISSION_PED(peds[WAVE_PED_START + iNewPed], s_WavePed[iNewPed].eModel, 
				s_SpawnPoints[s_WavePed[iNewPed].eSpawnPoint].vCoord, s_SpawnPoints[s_WavePed[iNewPed].eSpawnPoint].fHeading, 
				strDebugName, REL_ENEMY,
				s_WavePed[iNewPed].eWeapon, s_WavePed[iNewPed].eWeaponComp, s_WavePed[iNewPed].iAccuracy)
				
	SET_PED_COMBAT_PARAMS(peds[WAVE_PED_START + iNewPed], 
				vDefensiveCoord, fDefensiveRadius,
				cmMovement, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0,
				s_WavePed[iNewPed].bOpenCombat, s_SpawnPoints[s_WavePed[iNewPed].eSpawnPoint].bIsRoofPoint, s_WavePed[iNewPed].bCharge, s_WavePed[iNewPed].bChargeNow)
				
	SET_PED_AUTO_BLIPS_ACTIVE(peds[WAVE_PED_START + iNewPed], FALSE)
	
	IF s_WavePed[iNewPed].eModel = S_M_Y_SWAT_01
		SET_ENTITY_PROOFS(peds[WAVE_PED_START + iNewPed].id, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE, TRUE)
	ENDIF
	
	SET_PED_USING_ACTION_MODE(peds[WAVE_PED_START + iNewPed].id, TRUE, -1, "DEFAULT_ACTION")
	SET_ENTITY_HEALTH(peds[WAVE_PED_START + iNewPed].id, s_WavePed[iNewPed].iHealth)
	SET_PED_ARMOUR(peds[WAVE_PED_START + iNewPed].id, 0)

	TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[WAVE_PED_START + iNewPed].id, GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0)
	
	s_WavePed[iNewPed].bSpawned = TRUE
	
	i_TimeLastSpawned 			= GET_GAME_TIMER()
	i_KillsSinceLastSpawned		= 0
	i_EnemiesAlive++
	#IF IS_DEBUG_BUILD
	i_debug_EnemiesSpawned++
	#ENDIF

ENDPROC

//PURPOSE: Prepares the hotswap peds, should be called when a change is made to the availability of peds
PROC Prep_Hotswap(BOOL bBlockMike = FALSE, BOOL bBlockFrank = FALSE, BOOL bBlockTrev = FALSE,
					BOOL bHintMike = FALSE, BOOL bHintFrank = FALSE, BOOL bHintTrev = FALSE)

	if DOES_ENTITY_EXIST(peds[mpf_frank].id)
		sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = peds[mpf_frank].id
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,bBlockFrank) 
		SET_SELECTOR_PED_HINT(sSelectorPeds,SELECTOR_PED_FRANKLIN,bHintFrank)
	ELSE
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,true)
	ENDIF
	
	IF bBlockTrev
	AND bHintTrev
	ENDIF
	
	if DOES_ENTITY_EXIST(peds[mpf_mike].id)	
		sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = peds[mpf_mike].id
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_MICHAEL,bBlockMike) 
		SET_SELECTOR_PED_HINT(sSelectorPeds,SELECTOR_PED_MICHAEL,bHintMike)
	ELSE
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_MICHAEL,true)
	ENDIF

ENDPROC


//PURPOSE: Checks players ammo for explosive weapons
FUNC INT Get_Ped_Explosive_Ammo_Count(PED_INDEX ped)
	INT iExplosiveRounds = 0
	IF HAS_PED_GOT_WEAPON(ped, WEAPONTYPE_STICKYBOMB)		iExplosiveRounds += GET_AMMO_IN_PED_WEAPON(ped, WEAPONTYPE_STICKYBOMB)			ENDIF	
	IF HAS_PED_GOT_WEAPON(ped, WEAPONTYPE_RPG)				iExplosiveRounds += GET_AMMO_IN_PED_WEAPON(ped, WEAPONTYPE_RPG)					ENDIF
	IF HAS_PED_GOT_WEAPON(ped, WEAPONTYPE_GRENADE)			iExplosiveRounds += GET_AMMO_IN_PED_WEAPON(ped, WEAPONTYPE_GRENADE)				ENDIF	
	IF HAS_PED_GOT_WEAPON(ped, WEAPONTYPE_GRENADELAUNCHER)	iExplosiveRounds += GET_AMMO_IN_PED_WEAPON(ped, WEAPONTYPE_GRENADELAUNCHER)		ENDIF	
	RETURN iExplosiveRounds
ENDFUNC

FUNC BOOL LERP_PTFX_EVO(PTFX_EVO &ptfxEvo, FLOAT fNewDesiredValue, INT iInterpTime)

	IF ptfxEvo.fCurrent != fNewDesiredValue
		IF ptfxEvo.fDesired != fNewDesiredValue
			ptfxEvo.fDesired 			= fNewDesiredValue
			ptfxEvo.fStart				= ptfxEvo.fCurrent
			ptfxEvo.iTimeStarted		= GET_GAME_TIMER()
		ENDIF
		
		FLOAT fAlpha 		= CLAMP((GET_GAME_TIMER() - ptfxEvo.iTimeStarted)/TO_FLOAT(iInterpTime), 0.0, 1.0)
		ptfxEvo.fCurrent 	= LERP_FLOAT(ptfxEvo.fStart, ptfxEvo.fDesired, fAlpha)
																
		IF fAlpha = 1.0
			ptfxEvo.iTimeStarted 	= -1
			ptfxEvo.fDesired		= -1
			ptfxEvo.fStart			= -1
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC PLAY_ALARM_SECURITY(BOOL bEnable, BOOL bInstantly, BOOL bSpawnAtriumLights = FALSE)

	IF bEnable

		IF NOT IS_ALARM_PLAYING("AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER_B")
			IF PREPARE_ALARM("AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER_B")
				START_ALARM("AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER_B", bInstantly)
			ENDIF
		ENDIF
		
		TRIGGER_MUSIC_EVENT("AH3B_ALARM")
		
		IF bSpawnAtriumLights
			IF HAS_MODEL_LOADED(V_ILEV_PROP_74_EMR_3B)
			AND HAS_MODEL_LOADED(V_ILEV_PROP_74_EMR_3B_02)
			
				IF NOT DOES_ENTITY_EXIST(objs[mof_alarm_lights].id)
					objs[mof_alarm_lights].id 		= CREATE_OBJECT_NO_OFFSET(V_ILEV_PROP_74_EMR_3B, <<135.0580, -748.2704, 258.9249>>)
					objs[mof_alarm_lights].bCustom 	= TRUE
					SET_ENTITY_HEADING(objs[mof_alarm_lights].id, -110.008)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(objs[mof_alarm_lights_2].id)
					objs[mof_alarm_lights_2].id 		= CREATE_OBJECT_NO_OFFSET(V_ILEV_PROP_74_EMR_3B_02, <<117.4667, -740.8329, 259.0079>>)
					objs[mof_alarm_lights_2].bCustom 	= TRUE
					SET_ENTITY_HEADING(objs[mof_alarm_lights_2].id, 160.0)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(objs[mof_alarm_lights_3].id)
					objs[mof_alarm_lights_3].id 		= CREATE_OBJECT_NO_OFFSET(V_ILEV_PROP_74_EMR_3B_02, <<154.0723, -759.2141, 259.0079>>)
					objs[mof_alarm_lights_3].bCustom 	= TRUE
					SET_ENTITY_HEADING(objs[mof_alarm_lights_3].id, -110.000)
				ENDIF
				
			ENDIF
		ENDIF
	
	ELSE
	
		IF IS_ALARM_PLAYING("AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER_B")
			Unload_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER_B", bInstantly)
		ENDIF
		
		IF DOES_ENTITY_EXIST(objs[mof_alarm_lights].id)
			IF bInstantly
				DELETE_OBJECT(objs[mof_alarm_lights].id)
			ELSE
				SET_OBJECT_AS_NO_LONGER_NEEDED(objs[mof_alarm_lights].id)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(objs[mof_alarm_lights_2].id)
			IF bInstantly
				DELETE_OBJECT(objs[mof_alarm_lights_2].id)
			ELSE
				SET_OBJECT_AS_NO_LONGER_NEEDED(objs[mof_alarm_lights_2].id)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(objs[mof_alarm_lights_3].id)
			IF bInstantly
				DELETE_OBJECT(objs[mof_alarm_lights_3].id)
			ELSE
				SET_OBJECT_AS_NO_LONGER_NEEDED(objs[mof_alarm_lights_3].id)
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

PROC PLAY_ALARM_FIRE(BOOL bEnable, BOOL bInstantly)

	IF bEnable

		IF NOT IS_ALARM_PLAYING("AGENCY_HEIST_FIB_TOWER_ALARMS")
			IF PREPARE_ALARM("AGENCY_HEIST_FIB_TOWER_ALARMS")
				START_ALARM("AGENCY_HEIST_FIB_TOWER_ALARMS", bInstantly)
			ENDIF
		ENDIF
		IF NOT IS_ALARM_PLAYING("AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER")
			IF PREPARE_ALARM("AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER")
				START_ALARM("AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER", bInstantly)
			ENDIF
		ENDIF
	
	ELSE
	
		IF IS_ALARM_PLAYING("AGENCY_HEIST_FIB_TOWER_ALARMS")
			Unload_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS", bInstantly)
		ENDIF
		
		IF IS_ALARM_PLAYING("AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER")
			Unload_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER", bInstantly)
		ENDIF
	
	ENDIF

ENDPROC

// DOOR MANAGEMENT

//PURPOSE: Adds a new door to the doors array
PROC DOORS_Add_New(DOOR_FLAGS slot, VECTOR vCoord, MODEL_NAMES model, BOOL bResetDoor = FALSE)

	IF slot = mdf_invalid
		EXIT
	ENDIF

	TEXT_LABEL_23 strDoorName = "AH3B_DOOR_"
	strDoorName += ENUM_TO_INT(slot)
	sDoors[slot].iHash	= GET_HASH_KEY(strDoorName)
	
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(sDoors[slot].iHash)
		ADD_DOOR_TO_SYSTEM(sDoors[slot].iHash, model, vCoord, FALSE)
	ENDIF
		
	IF bResetDoor
		DOOR_SYSTEM_SET_OPEN_RATIO(sDoors[slot].iHash, 0.0, FALSE, TRUE)
		DOOR_SYSTEM_SET_DOOR_STATE(sDoors[slot].iHash, DOORSTATE_FORCE_CLOSED_THIS_FRAME)
		DOOR_SYSTEM_SET_DOOR_STATE(sDoors[slot].iHash, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	ENDIF
	
ENDPROC

FUNC BOOL Is_Door_LockState_Locked(DOOR_STATE_ENUM eDoorState)
	IF eDoorState = DOORSTATE_LOCKED
	OR eDoorState = DOORSTATE_FORCE_LOCKED_THIS_FRAME
	OR eDoorState = DOORSTATE_FORCE_LOCKED_UNTIL_OUT_OF_AREA
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

//PURPOSE: Changes the state of the door
PROC DOORS_Set_State(DOOR_FLAGS eDoor, BOOL lockState, FLOAT openRatio, INT iTime = 0)
	
	IF eDoor != mdf_invalid
	AND sDoors[eDoor].iHash != 0
		// No update over time, do NOW!
		IF iTime <= 0
			IF openRatio != DOORS_KEEP_CURRENT_RATIO
				DOOR_SYSTEM_SET_OPEN_RATIO(sDoors[eDoor].iHash, openRatio, FALSE, TRUE)
			ENDIF
			
			IF lockState
				DOOR_SYSTEM_SET_DOOR_STATE(sDoors[eDoor].iHash, DOORSTATE_LOCKED, FALSE, TRUE)
			ELSE
				DOOR_SYSTEM_SET_DOOR_STATE(sDoors[eDoor].iHash, DOORSTATE_UNLOCKED, FALSE, TRUE)
			ENDIF
			
			sDoors[eDoor].iTimeStamp = 0
			sDoors[eDoor].iInterpTime = 0
			
		// Needs updating over time, mark for frame by frame update
		ELIF openRatio != DOORS_KEEP_CURRENT_RATIO 
			sDoors[eDoor].bChangingState = TRUE
			sDoors[eDoor].bDesiredLockState = lockState
			sDoors[eDoor].fDesiredOpenRatio = openRatio
			
			sDoors[eDoor].iTimeStamp = GET_GAME_TIMER()
			sDoors[eDoor].iInterpTime = iTime
			
			sDoors[eDoor].fStartOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO(sDoors[eDoor].iHash)
		ELSE
			SCRIPT_ASSERT("DOOR_SYSTEM: CANNOT OPEN DOOR OVER TIME TO KEEP THE SAME DOOR RATIO, POINTLESS!")
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Obtains the state of the door
PROC DOORS_Get_State(DOOR_FLAGS eDoor, BOOL &bLockState, FLOAT &fOpenRatio)
	IF eDoor != mdf_invalid
	AND sDoors[eDoor].iHash != 0
		fOpenRatio = DOOR_SYSTEM_GET_OPEN_RATIO(sDoors[eDoor].iHash)
		DOOR_STATE_ENUM eDoorState = DOOR_SYSTEM_GET_DOOR_STATE(sDoors[eDoor].iHash)
		//DOOR_STATE_ENUM eDoorState = DOOR_SYSTEM_GET_DOOR_PENDING_STATE(sDoors[eDoor].iHash)
		
		bLockState = Is_Door_LockState_Locked(eDoorState)
	ENDIF
ENDPROC

//PURPOSE: Initialises the door structs 
PROC DOORS_Initialise()

	DOORS_Add_New(mdf_topfloor_main_l, 			<< 116.51, -735.44, 258.42 >>, 			V_ILEV_FIB_DOOR1, 		TRUE)
	DOORS_Add_New(mdf_topfloor_main_r, 			<< 119.16, -736.40, 258.40 >>, 			V_ILEV_FIB_DOOR1, 		TRUE)
	DOORS_Add_New(mdf_topfloor_fake_l,			<< 121.66, -757.86, 258.30 >>, 			V_ILEV_FIB_DOOR1, 		TRUE)
	DOORS_Add_New(mdf_topfloor_fake_r,			<< 139.02, -735.12, 258.30 >>, 			V_ILEV_FIB_DOOR1, 		TRUE)
	DOORS_Add_New(mdf_topfloor_fake_c,			<< 114.4338, -742.4111, 258.3478 >>, 	V_ILEV_FIB_DOOR1, 		TRUE)
	DOORS_Add_New(mdf_topfloor_pre_stairs,	 	<< 124.0, -727.2, 258.3 >>,				V_ILEV_FIB_DOOR1, 		TRUE)
	DOORS_Add_New(mdf_boardroom_l,				<< 140.99, -758.15, 258.34 >>,			V_ILEV_FIB_DOOR1, 		TRUE)
	DOORS_Add_New(mdf_stairwell_53,				<< 129, -732, 258 >>,					V_ILEV_FIB_DOOR1,		TRUE)
	DOORS_Add_New(mdf_stairwell_52,				<< 128.6526, -731.6434, 254.3504 >>,	V_ILEV_FIB_DOOR1, 		TRUE)
	DOORS_Add_New(mdf_stairwell_47,				<< 128.3689, -731.4404, 233.9953 >>,	V_ILEV_FIB_DOOR1, 		TRUE)
	DOORS_Add_New(mdf_burntout1_pre_stairs,		<< 124.0606, -727.2556, 254.3019 >>,	V_ILEV_FIB_DOOR1, 		TRUE)
	DOORS_Add_New(mdf_burntout1_entrance_l,		<< 118.67, -736.01, 254.26 >>,			V_ILEV_FIB_DOOR1, 		TRUE)
	DOORS_Add_New(mdf_burntout1_entrance_r,		<< 116.73, -735.30, 254.17 >>,			V_ILEV_FIB_DOOR1, 		TRUE)
	DOORS_Add_New(mdf_burntout2_double_l,		<< 155.3142, -763.3002, 250.3019 >>,	V_ILEV_FIB_DOORBRN, 	TRUE)
	DOORS_Add_New(mdf_burntout2_double_r,		<< 152.8708, -762.4113, 250.3019 >>,	V_ILEV_FIB_DOORBRN, 	TRUE)
	DOORS_Add_New(mdf_burntout2_51_stairwell,	<< 141.5514, -766.9590, 250.3019 >>,	V_ILEV_FIB_DOORBRN, 	TRUE)
	DOORS_Add_New(mdf_burntout2_50_stairwell,	<< 141.5514, -766.9590, 246.3019 >>,	V_ILEV_FIB_DOORBRN, 	TRUE)
	DOORS_Add_New(mdf_gov_building_gate,		<<2491.8682, -303.4783, 91.9924>>,		PROP_FACGATE_01,		TRUE)
	
	sDoors[mdf_helipad].iHash 					= enum_to_int(DOORHASH_NOSE_HELIPAD)
	sDoors[mdf_helipad].bExistingDoor			= TRUE
	
ENDPROC

//PURPOSE: Resets all the doors streamed in
PROC DOORS_Reset_All()
	INT x
	REPEAT COUNT_OF(sDoors) x
		IF sDoors[x].iHash != 0
			DOOR_SYSTEM_SET_OPEN_RATIO(sDoors[x].iHash, 0.0, FALSE, TRUE)
			DOOR_SYSTEM_SET_DOOR_STATE(sDoors[x].iHash, DOORSTATE_UNLOCKED, FALSE, TRUE)
		ENDIF
		
		sDoors[x].bChangingState 	= FALSE
		sDoors[x].iInterpTime 		= 0
		sDoors[x].iTimeStamp		= 0
		sDoors[x].fDesiredOpenRatio	= 0.0
		sDoors[x].bDesiredLockState = FALSE
		sDoors[x].fStartOpenRatio	= 0.0
	ENDREPEAT
ENDPROC

PROC DOORS_LockOpenNonAnimDoors()
	DOORS_Set_State(mdf_burntout1_pre_stairs, TRUE, -1.0)
	DOORS_Set_State(mdf_burntout1_entrance_l, TRUE, -1.0)
	DOORS_Set_State(mdf_burntout1_entrance_r, TRUE, 1.0)
	DOORS_Set_State(mdf_burntout2_double_l, TRUE, 1.0)
	DOORS_Set_State(mdf_burntout2_double_r, TRUE, -1.0)
	DOORS_Set_State(mdf_burntout2_51_stairwell, TRUE, 1.0)
	DOORS_Set_State(mdf_burntout2_50_stairwell, TRUE, -1.0)
ENDPROC

//PURPOSE: Manage locking and opening of doors
PROC DOORS_Manage_Door_System()
	INT i
	FOR i = 0 TO enum_to_int(mdf_num_doors) -1
		IF sDoors[i].iHash != 0
		AND sDoors[i].bChangingState
		
			BOOL bLockState = FALSE
			FLOAT fOpenRatio = 0.0
			DOORS_Get_State(int_to_enum(DOOR_FLAGS, i), bLockState, fOpenRatio)
	
			// If state does not match then update to new state
			IF bLockState != sDoors[i].bDesiredLockState
			OR fOpenRatio != sDoors[i].fDesiredOpenRatio

				IF sDoors[i].iInterpTime > 0
				AND sDoors[i].iTimeStamp > 0

					FLOAT fInterpProg = CLAMP((GET_GAME_TIMER() - sDoors[i].iTimeStamp)/TO_FLOAT(sDoors[i].iInterpTime), 0.0, 1.0)

					IF fInterpProg != 1.0
						
						// change in progress
						fOpenRatio = sDoors[i].fStartOpenRatio + (fInterpProg * (sDoors[i].fDesiredOpenRatio - sDoors[i].fStartOpenRatio))
						bLockState = TRUE
						
					ELSE
						
						// door finished
						bLockState = sDoors[i].bDesiredLockState
						fOpenRatio = sDoors[i].fDesiredOpenRatio
						sDoors[i].bChangingState = FALSE

						sDoors[i].iInterpTime		= 0
						sDoors[i].iTimeStamp 		= 0
						sDoors[i].bChangingState 	= FALSE
					ENDIF
				ELSE
//					SCRIPT_ASSERT("DOOR SYSTEM: DOOR NOT INTERP TIME SET!")
					// Set to the desired values as no time was present
					IF sDoors[i].bDesiredLockState
						bLockState = TRUE
					ELSE
						bLockState = FALSE
					ENDIF
					fOpenRatio = sDoors[i].fDesiredOpenRatio
					
					sDoors[i].iInterpTime		= 0
					sDoors[i].iTimeStamp 		= 0
					sDoors[i].bChangingState 	= FALSE
				ENDIF
				
				IF fOpenRatio != DOORS_KEEP_CURRENT_RATIO
					DOOR_SYSTEM_SET_OPEN_RATIO(sDoors[i].iHash, fOpenRatio, FALSE, TRUE)
				ENDIF
				
				// Set the door
				IF bLockState
					DOOR_SYSTEM_SET_DOOR_STATE(sDoors[i].iHash, DOORSTATE_LOCKED, FALSE, TRUE)
				ELSE
					DOOR_SYSTEM_SET_DOOR_STATE(sDoors[i].iHash, DOORSTATE_UNLOCKED, FALSE, TRUE)
				ENDIF
			ELSE
				
				DOOR_SYSTEM_SET_OPEN_RATIO(sDoors[i].iHash, fOpenRatio, FALSE, TRUE)
				
				// Set the door
				IF bLockState
					DOOR_SYSTEM_SET_DOOR_STATE(sDoors[i].iHash, DOORSTATE_LOCKED, FALSE, TRUE)
				ELSE
					DOOR_SYSTEM_SET_DOOR_STATE(sDoors[i].iHash, DOORSTATE_UNLOCKED, FALSE, TRUE)
				ENDIF
			
				// door already in the desired stage
				sDoors[i].iInterpTime		= 0
				sDoors[i].iTimeStamp 		= 0
				sDoors[i].bChangingState 	= FALSE
			ENDIF
		ENDIF
	ENDFOR
ENDPROC


/*
													 _______  _     _  _______  _______  _______  ______ 
													(_______)(_)   (_)(_______)(_______)(_______)/ _____)
													 _____    _     _  _____    _     _     _   ( (____  
													|  ___)  | |   | ||  ___)  | |   | |   | |   \____ \ 
													| |_____  \ \ / / | |_____ | |   | |   | |   _____) )
													|_______)  \___/  |_______)|_|   |_|   |_|  (______/ 
*/


// EVENT MANAGEMENT

//PURPOSE: Indicates whether an event has been triggered or not
FUNC BOOL Is_Event_Triggered(mission_event_flag event)
	IF mEvents[event].state = EVENTSTATE_READY
	OR mEvents[event].state = EVENTSTATE_RESET
		RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC

//PURPOSE: Indicates whether an event has completed
FUNC BOOL Is_Event_Complete(mission_event_flag event)
	IF mEvents[event].state = EVENTSTATE_COMPLETED
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

//PURPOSE: Indicates if an event is ready to be used again
FUNC BOOL Is_Event_Ready(mission_event_flag event)
	IF mEvents[event].state = EVENTSTATE_READY
	OR mEvents[event].state = EVENTSTATE_RESET
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL Is_Event_Running(mission_event_flag event)
	IF Is_Event_Triggered(event)
	AND (NOT Is_Event_Complete(event))
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

//PURPOSE: Triggers an event
FUNC BOOL Event_Trigger(mission_event_flag event, INT delay = 0)
	
	IF mEvents[event].state = EVENTSTATE_READY
	OR mEvents[event].state = EVENTSTATE_RESET
	OR (mEvents[event].state = EVENTSTATE_DELAYED AND mEvents[event].iStage = 1) // a event with a start delay that has not yet kicked in
		IF delay > 0
			mEvents[event].state = EVENTSTATE_DELAYED
			mEvents[event].iDelay = delay
		ELSE
			mEvents[event].state = EVENTSTATE_RUNNING
			mEvents[event].iDelay = 0
		ENDIF
		mEvents[event].iTimeStamp = GET_GAME_TIMER()
		mEvents[event].iStage = 1
		RETURN TRUE
	ELSE
		RETURN FALSE // already triggered, do nothing
	ENDIF
ENDFUNC

// Marks event as completed
PROC Event_Finish(mission_event_flag event)
	IF mEvents[event].state != EVENTSTATE_COMPLETED
		mEvents[event].state = EVENTSTATE_COMPLETED
		mEvents[event].iTimeSinceLastRunning = GET_GAME_TIMER()
		mEvents[event].iDelay = 0
	ENDIF
ENDPROC

// Pauses the event
PROC Event_Pause(mission_event_flag event)
	IF mEvents[event].state != EVENTSTATE_COMPLETED
	AND mEvents[event].state != EVENTSTATE_READY
		mEvents[event].state = EVENTSTATE_PAUSED
	ENDIF
ENDPROC

// Resumes a paused event
PROC Event_Resume(mission_event_flag event)
	IF mEvents[event].state = EVENTSTATE_PAUSED
		mEvents[event].state = EVENTSTATE_RUNNING
	ENDIF
ENDPROC

// Resets the given event
PROC Event_Reset(mission_event_flag event)
	mEvents[event].state = EVENTSTATE_RESET
	mEvents[event].iTimeStamp = 0
	mEvents[event].iTimeSinceLastRunning = GET_GAME_TIMER()
	mEvents[event].iDelay = 0
	mEvents[event].iStage= 1
ENDPROC

// Resets all the events in the mission
PROC Events_Reset_All()
	INT i
	FOR i = 0 TO ENUM_TO_INT(mef_num_events)-1
		Event_Reset(INT_TO_ENUM(mission_event_flag, i))	
	ENDFOR
ENDPROC

// Used to delay events internally insde their procs
PROC Event_Next_Stage(mission_event_flag event, INT delay = 0)
	// Add delay if marked
	IF delay > 0
		mEvents[event].state = EVENTSTATE_DELAYED
		mEvents[event].iTimeStamp = GET_GAME_TIMER()
		mEvents[event].iDelay = delay
	ENDIF
	mEvents[event].iStage++
ENDPROC

//PURPOSE: Wipes the render target back to its plain black colour
PROC Wipe_RenderTargets(BOOL bFullScreen)
	IF bFullScreen
		DRAW_RECT(0.5,0.5,1.0,1.0,0,0,0,255)
	ELSE	
		SET_TEXT_RENDER_ID(rt_hack_monitor)
		DRAW_RECT(f_Hacking_Render_Target_x, f_Hacking_Render_Target_y, f_Hacking_Render_Target_width, f_Hacking_Render_Target_height, 0, 0, 0, 255)  
		SET_TEXT_RENDER_ID(rt_default)
	ENDIF
ENDPROC

//PURPOSE: Clears the render targets to their blank start states
PROC Cleanup_RenderTargets()
	CDEBUG1LN(DEBUG_MIKE, "Cleanup_RenderTargets() called")
	Wipe_RenderTargets(FALSE)
	IF IS_NAMED_RENDERTARGET_REGISTERED("tvscreen")
		RELEASE_NAMED_RENDERTARGET("tvscreen")
		CDEBUG1LN(DEBUG_MIKE, "Cleanup_RenderTargets() named render target tvscreen found & released")
	ENDIF
	
ENDPROC

//PURPOSE: Grabs the props 
FUNC BOOL Setup_Hack_Room(BOOL bHideDoor = FALSE, BOOL bGrabMonitor = TRUE, BOOL bGrabChair = TRUE)
	BOOL bFinished = TRUE

// Hack computer
	IF bGrabMonitor
		IF NOT DOES_ENTITY_EXIST(objs[mof_hack_computer].id)
			Load_Asset_Model(sAssetData, PROP_MONITOR_01B)
			IF HAS_MODEL_LOADED(PROP_MONITOR_01B)
			AND DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<151.31, -765.49, 258.05>>, 1.0, PROP_MONITOR_01B)
				
				objs[mof_hack_computer].id = GET_CLOSEST_OBJECT_OF_TYPE(<<151.31, -765.49, 258.05>>, 1.0, PROP_MONITOR_01B)
				FREEZE_ENTITY_POSITION(objs[mof_hack_computer].id, TRUE)
				
				IF NOT IS_NAMED_RENDERTARGET_REGISTERED("tvscreen")
					REGISTER_NAMED_RENDERTARGET("tvscreen")
				ENDIF
				IF NOT IS_NAMED_RENDERTARGET_LINKED(PROP_MONITOR_01B)
					LINK_NAMED_RENDERTARGET(PROP_MONITOR_01B)
				ENDIF
				rt_hack_monitor		= GET_NAMED_RENDERTARGET_RENDER_ID("tvscreen")
				rt_default			= GET_DEFAULT_SCRIPT_RENDERTARGET_RENDER_ID()
				
				Unload_Asset_Model(sAssetData, PROP_MONITOR_01B)
				
			ELSE
				CDEBUG1LN(DEBUG_MIKE, "Could not find the monitor")
				bFinished = FALSE
			ENDIF
			
		ENDIF
	ENDIF
	
// Hack chair
	IF bGrabChair
		IF NOT DOES_ENTITY_EXIST(objs[mof_hack_chair].id)
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<150.2778, -766.1879, 257.1537>>, 1.0, V_CORP_OFFCHAIR)
				objs[mof_hack_chair].id = GET_CLOSEST_OBJECT_OF_TYPE(<<150.2778, -766.1879, 257.1537>>, 1.0, V_CORP_OFFCHAIR)
				FREEZE_ENTITY_POSITION(objs[mof_hack_chair].id, TRUE)
			ELSE
				CDEBUG1LN(DEBUG_MIKE, "Could not find the chair")
				bFinished = FALSE
			ENDIF
		ENDIF
	ENDIF
	
// Hack door
	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<150.5931, -762.3801, 258.3018>>, 1.0, mod_OriginalHackDoor)
	
		// We need the door hidden
		IF bHideDoor
			IF NOT bDoorHidden
				CREATE_MODEL_HIDE(<<150.5931, -762.3801, 258.3018>>, 1.0, mod_OriginalHackDoor, TRUE)
				bDoorHidden = TRUE
			ENDIF
		// ... we don't hide it so grab it so we can check its health
		ELSE
			IF NOT DOES_ENTITY_EXIST(objs[mof_hack_door_fake].id)
				objs[mof_hack_door_fake].id = GET_CLOSEST_OBJECT_OF_TYPE(<<150.5931, -762.3801, 258.3018>>, 1.0, mod_OriginalHackDoor)
			ENDIF
		ENDIF
	ELSE
		CDEBUG1LN(DEBUG_MIKE, "Can't find the door yet, probably waiting for streaming.")
		bFinished = FALSE
	ENDIF

	RETURN bFinished
ENDFUNC

// PURPOSE: Draws the intial download screen to the RT
PROC DOWNLOAD_Setup_File_Download()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "OPEN_LOADING_PROGRESS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_LOADING_PROGRESS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
	END_SCALEFORM_MOVIE_METHOD()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_LOADING_TIME")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(I_DOWNLOAD_DURATION)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("A3B_DLTIME")
	END_SCALEFORM_MOVIE_METHOD()
	
	bDownloadStarted 			= TRUE
	bDownloadComplete 			= FALSE
	fDownloadProgress 			= 0.0
	fDownloadSpeedMultiplier	= 1.0
	iFileIndex 					= 0
ENDPROC

/// PURPOSE: Use to set the download as finished
PROC DOWNLOAD_Set_Complete()
	// Play download complete sfx
	STOP_SOUND(sounds[sfx_downloading])
	PLAY_SOUND_FROM_COORD(sounds[sfx_downloaded], "HACKING_DOWNLOADED", GET_ENTITY_COORDS(objs[mof_hack_computer].id))

	// Fill bar 100% but remove the number from the screen
	BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_LOADING_PROGRESS")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(100)
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	END_SCALEFORM_MOVIE_METHOD()
	
	// Change to the transfer complete message
	BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_LOADING_MESSAGE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("A3B_DLCOMP")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(2)
	END_SCALEFORM_MOVIE_METHOD()
	
	// No time remaining
	BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_LOADING_TIME")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
	END_SCALEFORM_MOVIE_METHOD()
	
	bDownloadComplete 		= TRUE
ENDPROC

//PURPOSE: Draws the computer monitor
PROC DOWNLOAD_Draw_PC_Screen(BOOL bDrawFullScreen, BOOL bWipeToBlack = FALSE)

	IF HAVE_HACKING_ASSETS_LOADED(FALSE)
	//IF HAS_SCALEFORM_MOVIE_LOADED(sfHacking)
	
		//IF NOT sHackingData.b_wallpaper_initialised
		IF NOT IS_BIT_SET(sHackingData.bsHacking,BS_WALLPAPER_INITIALISED)
			INITIALISE_HACKING_WALLPAPER_AND_ICONS(sHackingData, WALL_FBI)
		ENDIF

		IF bDrawFullScreen
			IF bWipeToBlack
				Wipe_RenderTargets(TRUE)
			ELSE
				IF GET_PAUSE_MENU_STATE() = PM_INACTIVE
				AND NOT IS_PAUSE_MENU_ACTIVE()
					DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfHacking, 255,255,255,255)
				ENDIF
			ENDIF 
		ELSE
			IF bWipeToBlack
				Wipe_RenderTargets(FALSE)
			ELSE
				IF DOES_ENTITY_EXIST(objs[mof_hack_computer].id)	
			
					SET_TEXT_RENDER_ID(rt_hack_monitor)
					DRAW_SCALEFORM_MOVIE(sfHacking, f_Hacking_Render_Target_x, f_Hacking_Render_Target_y, f_Hacking_Render_Target_width, f_Hacking_Render_Target_height, 255, 255, 255, 255)
					SET_TEXT_RENDER_ID(rt_default)
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF
	
ENDPROC

//PURPOSE: Manages the drawing of a progress bar for the download
PROC DOWNLOAD_Manage_File_Download(BOOL bFullScreen = FALSE)

	IF bDownloadStarted
		IF NOT bDownloadComplete
			
			FLOAT fCurrentDownloadRate
			INT iProgressBar

			fCurrentDownloadRate	= (1000.0/I_DOWNLOAD_DURATION) * fDownloadSpeedMultiplier
			#IF IS_DEBUG_BUILD
				f_debug_CurrentRate 	= fCurrentDownloadRate 
			#ENDIF
			fDownloadProgress 		+= TIMESTEP() * fCurrentDownloadRate
			fDownloadProgress 		= CLAMP(fDownloadProgress, 0.0, 1.0)
			iProgressBar 			= CLAMP_INT(ROUND(fDownloadProgress * 100), 0, 100)

			// Update the download progress on the monitor
			BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_LOADING_PROGRESS")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iProgressBar)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
			END_SCALEFORM_MOVIE_METHOD()
			
			// Update the time remaining
			IF GET_GAME_TIMER() - iLastTimeRemainingUpdate > 1500

				INT	iDownloadRemainingTime = ROUND((1.0 - fDownloadProgress)/fCurrentDownloadRate)
				
				BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_LOADING_TIME")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iDownloadRemainingTime)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("A3B_DLTIME")
				END_SCALEFORM_MOVIE_METHOD()
				
				iLastTimeRemainingUpdate = GET_GAME_TIMER()
			ENDIF
			
			// Update the text label
			IF fDownloadProgress >= iFileIndex * (1.0/26)
			
				TEXT_LABEL_23 strMsgLabel 
				IF iFileIndex <= 12
					strMsgLabel = "A3B_DL"
					strMsgLabel += iFileIndex
				ELSE
					strMsgLabel = "A3B_DEL"
					strMsgLabel += (iFileIndex - 13)
				ENDIF
			
				// Change to the transfer complete message
				BEGIN_SCALEFORM_MOVIE_METHOD(sfHacking, "SET_LOADING_MESSAGE")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(strMsgLabel)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				END_SCALEFORM_MOVIE_METHOD()	
				
				iFileIndex++
				IF iFileINdex >= 26
					iFileIndex = 0
				ENDIF
			ENDIF
		ENDIF

		DOWNLOAD_Draw_PC_Screen(bFullScreen)
		
		IF NOT bDownloadComplete
		AND fDownloadProgress = 1.0
			DOWNLOAD_Set_Complete()
		ENDIF
		
	ENDIF
ENDPROC

PROC UPDATE_LOCKED_COVER_CAM_LIMITER()

	IF PLAYER_PED_ID() = MIKE_PED_ID()
	AND ( IS_PED_IN_COVER(MIKE_PED_ID(), FALSE)
	OR IS_PED_GOING_INTO_COVER( MIKE_PED_ID() )
	OR IS_PED_AIMING_FROM_COVER( MIKE_PED_ID() ) )
	
		IF IS_PED_AIMING_FROM_COVER(MIKE_PED_ID())
		
			IF i_CamLimiterInterpStage != 1
				i_CamLimiterInterpStage = 1
				i_CamLimiterInterpTimer = GET_GAME_TIMER()
			ENDIF
			SET_PLAYER_FORCED_ZOOM(PLAYER_ID(), TRUE)
		
		ELSE
			IF i_CamLimiterInterpStage != -1
				i_CamLimiterInterpStage = -1
				i_CamLimiterInterpTimer = GET_GAME_TIMER()
			ENDIF
		ENDIF
		
		FLOAT fAlpha = CLAMP((GET_GAME_TIMER() - i_CamLimiterInterpTimer)/500.0, 0.0, 1.0)
		
		IF i_CamLimiterInterpStage = -1
			fAlpha = 1.0 - fAlpha
		ENDIF
		
		SET_THIRD_PERSON_CAM_RELATIVE_HEADING_LIMITS_THIS_UPDATE(
			LERP_FLOAT(f_cam_limits_heading_min, f_cam_limits_aim_heading_min, fAlpha), 
			LERP_FLOAT(f_cam_limits_heading_max, f_cam_limits_aim_heading_max, fAlpha))
			
		SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(
			LERP_FLOAT(f_cam_limits_pitch_min, f_cam_limits_aim_pitch_min, fAlpha), 
			LERP_FLOAT(f_cam_limits_pitch_max, f_cam_limits_aim_pitch_max, fAlpha))
			
		SET_THIRD_PERSON_CAM_ORBIT_DISTANCE_LIMITS_THIS_UPDATE(
			LERP_FLOAT(f_cam_limits_orbit_min, f_cam_limits_aim_orbit_min, fAlpha), 
					LERP_FLOAT(f_cam_limits_orbit_max, f_cam_limits_aim_orbit_max, fAlpha))
		
	ENDIF
ENDPROC

//PURPOSE: Manages michael in cover by the computer, fixing him in place. Also deals with the first person camera that looks at the computer.
PROC UPDATE_LOCKED_COVER_PED()

	CONST_INT		I_GLANCE_SPEED_TIME			400
	CONST_INT		I_GLANCE_HOLD_TIME			150
	
	#IF IS_DEBUG_BUILD
	CAM_VIEW_MODE e_debug_aim_state = GET_PED_AIM_CAM_VIEW_MODE()
	i_debug_aim_cam = enum_to_int(e_debug_aim_state)
	#ENDIF
	
	CONTROL_TYPE	eLookAtPCControlType	= PLAYER_CONTROL		//PLAYER_CONTROL
	CONTROL_ACTION 	eLookAtPCButton 		= INPUT_RELOAD 			//INPUT_DUCK
	
	INT iAmmo = GET_AMMO_IN_PED_WEAPON(MIKE_PED_ID(), WEAPONTYPE_CARBINERIFLE)
	IF iAmmo <= 30
		SET_PED_AMMO(MIKE_PED_ID(), WEAPONTYPE_CARBINERIFLE, 100)
	ENDIF
	
	// Switching or cutscene is active, clear help
	IF PLAYER_PED_ID() != MIKE_PED_ID()
	OR swState != SWITCH_NOSTATE
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
		AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("A3B_HLPFPS")
			CLEAR_HELP()
		ENDIF
	ENDIF
	
	SET_RAGDOLL_BLOCKING_FLAGS(MIKE_PED_ID(), RBF_BULLET_IMPACT | RBF_FIRE | RBF_IMPACT_OBJECT | RBF_MELEE | RBF_EXPLOSION)
	SET_PED_CONFIG_FLAG(MIKE_PED_ID(), PCF_DisableExplosionReactions, TRUE)
	
	IF NOT IS_PED_IN_COVER(MIKE_PED_ID(), FALSE)
	AND NOT IS_PED_GOING_INTO_COVER( MIKE_PED_ID() )
	AND NOT IS_PED_AIMING_FROM_COVER( MIKE_PED_ID() )
	
		IF MIKE_PED_ID() = PLAYER_PED_ID()
			IF swState != SWITCH_IN_PROGRESS
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(MIKE_PED_ID(), SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
				AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(MIKE_PED_ID(), SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER)
					TASK_PUT_PED_DIRECTLY_INTO_COVER(MIKE_PED_ID(), v_ShootoutCoverMike, -1, TRUE, 0, TRUE, FALSE, peds[mpf_mike].cov, TRUE)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(MIKE_PED_ID(), SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
			AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(MIKE_PED_ID(), SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER)
				TASK_SEEK_COVER_TO_COVER_POINT(MIKE_PED_ID(), peds[mpf_mike].cov, <<142.6137, -757.5621, 258.5244>>, -1, TRUE)
			ENDIF
		ENDIF
		
	ELIF MIKE_PED_ID() != PLAYER_PED_ID()
		IF NOT IS_PED_IN_COMBAT(MIKE_PED_ID())
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(MIKE_PED_ID(), 70.0)
		ENDIF
	ENDIF

	IF (PLAYER_PED_ID() = MIKE_PED_ID()
	OR (swState != SWITCH_NOSTATE AND sSelectorPeds.eNewSelectorPed = SELECTOR_PED_MICHAEL))
	AND NOT DOES_CAM_EXIST(cams[mcf_helicrash_1])
	AND NOT DOES_CAM_EXIST(cams[mcf_helicrash_2])
	AND NOT DOES_CAM_EXIST(cams[mcf_helicrash_3])
	
		IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interior_fib03, "V_FIB03_door_light")
			ACTIVATE_INTERIOR_ENTITY_SET(interior_fib03, "V_FIB03_door_light")
		ENDIF
	
		IF GET_DISABLED_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) <= 0.5
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)	
		ENDIF
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
		DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
		
		IF IS_PHONE_ONSCREEN()
			HANG_UP_AND_PUT_AWAY_PHONE()
		ENDIF
		
		// block aiming while viewing the computer
		IF i_fps_state != DC_CAM_COVER_CURRENT
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		ENDIF
		
		VECTOR vStickDir, vDesiredLookAt, vLookAt
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			vStickDir = <<0, 0, 0>> // we're zeroing out the input here for mouse as the movement is far too jerky and doesn't look nice.
		ELSE
			vStickDir = <<GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X), 0, -GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y)>> 
		ENDIF
		
		FPS_DOWNLOAD_CAM e_FPSPrevState = i_fps_state

		SWITCH i_fps_state
			// Main management of the fps stuff
			CASE DC_PROGRESS_CAM_ENTER	
				IF NOT IS_INTERPOLATING_TO_SCRIPT_CAMS()
					i_fps_state = DC_PROGRESS_CAM_CURRENT
				ENDIF
			BREAK
			CASE DC_PROGRESS_CAM_CURRENT
				IF NOT IS_CONTROL_PRESSED(eLookAtPCControlType, eLookAtPCButton)
				OR swState != SWITCH_NOSTATE
					RENDER_SCRIPT_CAMS(FALSE, TRUE, I_GLANCE_SPEED_TIME, TRUE)
					SET_CAM_CONTROLS_MINI_MAP_HEADING(cams[mcf_dc_progress], FALSE)
					
					IF IS_AUDIO_SCENE_ACTIVE("AH_3B_COMPUTER_FOCUS_CAM")
						STOP_AUDIO_SCENE("AH_3B_COMPUTER_FOCUS_CAM")
					ENDIF
				
					SET_CAM_MOTION_BLUR_STRENGTH(cams[mcf_dc_progress], 0.2)
					i_fps_state = DC_CAM_COVER_ENTER
				ELSE
					SET_CAM_MOTION_BLUR_STRENGTH(cams[mcf_dc_progress], 0.0)
				
					IF bHasLookedAtScreenBefore = FALSE
					AND IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
						bHasLookedAtScreenBefore = TRUE
					ENDIF
					
					vStickDir.x *= 0.15
					vStickDir.z *= 0.1
					
					// Player is moving the stick
					IF vStickDir.x != 0.0 OR vStickDir.z != 0.0
						vDesiredLookAt = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(objs[mof_hack_computer].id), 186.2489, vStickDir + vCompOffset)
					// Player is not moving the stick
					ELSE
						vDesiredLookAt = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(objs[mof_hack_computer].id), 186.2489, vCompOffset)
					ENDIF
					
					IF ARE_VECTORS_ALMOST_EQUAL(v_dc_progress_current_look_at, vDesiredLookAt, 0.01)
						vLookAt = vDesiredLookAt
					ELSE
						vLookAt = LERP_VECTOR(v_dc_progress_current_look_at, vDesiredLookAt, 8.0 * GET_FRAME_TIME())
					ENDIF

					POINT_CAM_AT_COORD(cams[mcf_dc_progress], vLookAt)
					v_dc_progress_current_look_at = vLookAt
				ENDIF
			BREAK
			
			CASE DC_CAM_COVER_ENTER
				IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
					i_fps_state = DC_CAM_COVER_CURRENT
				ENDIF
			BREAK
			CASE DC_CAM_COVER_CURRENT
				IF IS_CONTROL_PRESSED(eLookAtPCControlType, eLookAtPCButton)
				AND swState = SWITCH_NOSTATE
				AND (NOT bStartCrashCutscene OR Is_Event_Complete(mef_heli_crash_cutscene))
					IF iGlanceHoldTimer = -1
						iGlanceHoldTimer = GET_GAME_TIMER() + I_GLANCE_HOLD_TIME
					ENDIF
					
					IF GET_GAME_TIMER() > iGlanceHoldTimer							
						
						SET_CAM_ACTIVE(cams[mcf_dc_progress], TRUE)
						SET_CAM_CONTROLS_MINI_MAP_HEADING(cams[mcf_dc_progress], TRUE)
						RENDER_SCRIPT_CAMS(TRUE, TRUE, I_GLANCE_SPEED_TIME)
						
						IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_COMPUTER_FOCUS_CAM")
							START_AUDIO_SCENE("AH_3B_COMPUTER_FOCUS_CAM")
						ENDIF
						
						SET_CAM_MOTION_BLUR_STRENGTH(cams[mcf_dc_progress], 0.2)
						i_fps_state = DC_PROGRESS_CAM_ENTER
					ENDIF
				ELSE
					iGlanceHoldTimer = -1
					
					// show help
					IF swState = SWITCH_NOSTATE
					AND NOT bHasLookedAtScreenBefore
					AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP_FOREVER("A3B_HLPFPS")
					ENDIF
					
				ENDIF
			BREAK

		ENDSWITCH
		
		IF e_FPSPrevState != i_fps_state
			i_TimeOfCameraStateChange = GET_GAME_TIMER()
		ENDIF
		
		
		#IF IS_DEBUG_BUILD
			IF bDisplayEventDebug				
				IF NOT IS_VECTOR_ZERO(vLookAt)
					DRAW_DEBUG_SPHERE(vLookAt, 0.1, 0,255,0,255)
				ENDIF
				IF NOT IS_VECTOR_ZERO(vDesiredLookAt)
					DRAW_DEBUG_SPHERE(vDesiredLookAt, 0.1, 255,0,0,255)
				ENDIF
			ENDIF
		#ENDIF
		
	ELSE
	
		IF IS_AUDIO_SCENE_ACTIVE("AH_3B_COMPUTER_FOCUS_CAM")
			STOP_AUDIO_SCENE("AH_3B_COMPUTER_FOCUS_CAM")
		ENDIF
		
		IF IS_INTERIOR_ENTITY_SET_ACTIVE(interior_fib03, "V_FIB03_door_light")
			DEACTIVATE_INTERIOR_ENTITY_SET(interior_fib03, "V_FIB03_door_light")
		ENDIF
		
	ENDIF
ENDPROC

//PURPOSE: Sets up the stuff needed for the first person cam stuff 
PROC Setup_Michael_In_Cover(BOOL blendBack)
	
	IF HAS_PED_GOT_WEAPON(MIKE_PED_ID(), weap_Mike)
		SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), weap_Mike)
		
		IF GET_AMMO_IN_PED_WEAPON(MIKE_PED_ID(), weap_Mike) < ammo_Mike
			SET_PED_AMMO(MIKE_PED_ID(), weap_Mike, ammo_Mike)
		ENDIF
	ELSE					
		GIVE_WEAPON_TO_PED(MIKE_PED_ID(), weap_Mike, ammo_Mike, TRUE, TRUE)
	ENDIF
	
	IF blendBack
		TASK_PUT_PED_DIRECTLY_INTO_COVER(MIKE_PED_ID(), v_ShootoutCoverMike, 1000, TRUE, 0.5, TRUE, FALSE, peds[mpf_mike].cov)
	ELSE
		SET_ENTITY_COORDS(MIKE_PED_ID(), v_ShootoutCoverMike)
		SET_ENTITY_HEADING(MIKE_PED_ID(), 339.1769)
		TASK_PUT_PED_DIRECTLY_INTO_COVER(MIKE_PED_ID(), v_ShootoutCoverMike, 1000, TRUE, 0, TRUE, FALSE, peds[mpf_mike].cov)
	ENDIF
	
	FORCE_PED_AI_AND_ANIMATION_UPDATE(MIKE_PED_ID(), TRUE)
	
	SET_PED_SPHERE_DEFENSIVE_AREA(MIKE_PED_ID(), <<150.547668,-761.117310,257.152161>>, 1.5)
	
	// setup the first person cam
	cams[mcf_dc_progress] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 150.9026, -762.4860, 258.6959 >>, << -5.6055, -3.5952, -172.9336 >>, 10.0, false)
	SHAKE_CAM(cams[mcf_dc_progress], "HAND_SHAKE", 0.1)
	v_dc_progress_current_look_at = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objs[mof_hack_computer].id, vCompOffset)
	POINT_CAM_AT_COORD(cams[mcf_dc_progress], v_dc_progress_current_look_at)
	
	i_fps_state = DC_CAM_COVER_CURRENT
ENDPROC

//PURPOSE: Manages the rappeling of a ped
FUNC BOOL RAPPEL_Manage_Ped(RAPPEL_DATA_ADDITIONAL &rData, PED_STRUCT &ped, BOOL bAdjustSpeeds = FALSE)

	CONST_FLOAT			RAPPEL_JUMP_LARGE									22.8			// 21.8(without error bounds)
	CONST_FLOAT			RAPPEL_JUMP_SMALL									9.0				// 8.0(without error bounds)
	
	FLOAT 				F_AI_DISTANCE_FROM_TARGET_TO_USE_SMALL_JUMPS		= RAPPEL_JUMP_SMALL/4
	FLOAT 				F_AI_DISTANCE_FROM_TARGET_TO_HOLD_RAPPEL 			= -(RAPPEL_JUMP_SMALL/2)
	
	FLOAT fActualZLimit = 0.0
	
	IF rData.sRappelData.vAttachPos.z <= rData.sRappelData.fLimitZ
	
		SET_RAPPEL_AI(rData.sRappelData, RAPPEL_AI_STOP)
		SET_RAPPEL_ALLOW_BIG_JUMPS(rData.sRappelData, 	FALSE)
		SET_RAPPEL_ALLOW_SMALL_JUMPS(rData.sRappelData, FALSE)
		
		IF rData.bCancelAtLimit
			IF rData.sRappelData.bFreeRoping
				DISMOUNT_RAPPEL(rData.sRappelData, rData.eDismountWeap)
				HANDLE_RAPPEL(rData.sRappelData, rData.bIsAI, rData.bDoCam)
				IF rData.sRappelData.bUnpinnedForDismount
					RETURN TRUE	
				ENDIF
			ELSE
				IF rData.sRappelData.state = RAPPEL_STATE_IDLING
					CANCEL_RAPPEL(rData.sRappelData)
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
			HANDLE_RAPPEL(rData.sRappelData, rData.bIsAI, rData.bDoCam)
		
			IF rData.sRappelData.state = RAPPEL_STATE_IDLING
				RETURN TRUE
			ENDIF
		ENDIF

	ELSE
		
		// Alter the z limit in order to stop the player going past the free rope switch point
		// this is resotred later on 
		IF rData.bSwitchToFreeRope
			IF NOT rData.sRappelData.bFreeRoping
				fActualZLimit = rData.sRappelData.fLimitZ
				rData.sRappelData.fLimitZ = rData.fSwitchLimit
			ENDIF
		ENDIF
	
		// Check if this ped needs converting to free roping
		IF rData.bSwitchToFreeRope
		AND rData.sRappelData.vAttachPos.z <= rData.fSwitchLimit + 0.25
		AND NOT rData.sRappelData.bFreeRoping
			SET_RAPPEL_AI(rData.sRappelData, RAPPEL_AI_STOP)
			rData.bIsAI = TRUE
			
			IF CONVERT_RAPPEL_IS_FREE_ROPING(rData.sRappelData, TRUE)
				IF DOES_ENTITY_EXIST(rData.sRappelData.pulleyObject)
					DELETE_OBJECT(rData.sRappelData.pulleyObject)
				ENDIF
			ENDIF
			
		// Handle rappel movement
		ELSE
			// Do AI update first
			IF rData.bIsAI
				IF rData.sRappelData.bFreeRoping
					SET_RAPPEL_AI(rData.sRappelData, RAPPEL_AI_MOVE_FAST)
				ELSE
					SET_RAPPEL_FORCE_SMALL_JUMP_ON_BIG_JUMP_CONTROL(rData.sRappelData, FALSE)

					VECTOR vAIPos = GET_ENTITY_COORDS(ped.id)
					
					// STOP MOVING
					IF vAIPos.z < rData.fZTarget + F_AI_DISTANCE_FROM_TARGET_TO_HOLD_RAPPEL
						IF ABSF(rData.sRappelData.fSpeed) <= 0.3
							SET_RAPPEL_AI(rData.sRappelData, RAPPEL_AI_STOP)
							SET_RAPPEL_ALLOW_BIG_JUMPS(rData.sRappelData, 	FALSE)
							SET_RAPPEL_ALLOW_SMALL_JUMPS(rData.sRappelData, FALSE)
							rData.iAIJumpTimer = -1
						ENDIF
					
					// CONTINUE MOVING
					ELSE 
					
						IF rData.iAIJumpTimer = -1
						
							rData.iAIJumpTimer = GET_GAME_TIMER() + rData.iAIFirstJumpDelay
						
						ELIF GET_GAME_TIMER() > rData.iAIJumpTimer
						
							// DO SMALL JUMPS
							IF vAIPos.z < rData.fZTarget + F_AI_DISTANCE_FROM_TARGET_TO_USE_SMALL_JUMPS
								SET_RAPPEL_AI(rData.sRappelData, RAPPEL_AI_MOVE_FAST)
								SET_RAPPEL_ALLOW_BIG_JUMPS(rData.sRappelData, 	FALSE)
								SET_RAPPEL_ALLOW_SMALL_JUMPS(rData.sRappelData, TRUE)
							// DO LARGE JUMPS
							ELSE
								SET_RAPPEL_AI(rData.sRappelData, RAPPEL_AI_MOVE_FAST)
								SET_RAPPEL_ALLOW_BIG_JUMPS(rData.sRappelData, 	TRUE)
								SET_RAPPEL_ALLOW_SMALL_JUMPS(rData.sRappelData, TRUE)
							ENDIF
						ENDIF
					ENDIF
										
				ENDIF
			ELSE
				SET_RAPPEL_ALLOW_BIG_JUMPS(rData.sRappelData, NOT rData.bBlockBigJumps)
				SET_RAPPEL_ALLOW_SMALL_JUMPS(rData.sRappelData, TRUE)
			ENDIF
			
			// limits to stop ped from rappeling past the CancelLimit and/or the SwitchLimit
			// Do not impose limits for peds in free roping
			IF NOT rData.sRappelData.bFreeRoping
				
				// No jumping!
				IF (rData.sRappelData.vAttachPos.z - RAPPEL_JUMP_SMALL <= rData.sRappelData.fLimitZ - 0.25)
				OR (rData.bSwitchToFreeRope AND (rData.sRappelData.vAttachPos.z - RAPPEL_JUMP_SMALL <= rData.fSwitchLimit - 0.25))
				
					IF rData.bIsAI
						SET_RAPPEL_AI(rData.sRappelData, int_to_enum(RAPPEL_AI_ENUM, CLAMP_INT(enum_to_int(rData.sRappelData.ai), enum_to_int(RAPPEL_AI_STOP), enum_to_int(RAPPEL_AI_MOVE_FAST))))
					ENDIF
					
					SET_RAPPEL_ALLOW_BIG_JUMPS(rData.sRappelData, 	FALSE)
					SET_RAPPEL_ALLOW_SMALL_JUMPS(rData.sRappelData, TRUE)
			
				// Limit to small jumps
				ELIF (rData.sRappelData.vAttachPos.z - RAPPEL_JUMP_LARGE <= rData.sRappelData.fLimitZ - 0.25)
				OR (rData.bSwitchToFreeRope AND (rData.sRappelData.vAttachPos.z - RAPPEL_JUMP_LARGE <= rData.fSwitchLimit - 0.25))
				
					IF rData.bIsAI 
						SET_RAPPEL_AI(rData.sRappelData, int_to_enum(RAPPEL_AI_ENUM, CLAMP_INT(enum_to_int(rData.sRappelData.ai), enum_to_int(RAPPEL_AI_STOP), enum_to_int(RAPPEL_AI_MOVE_FAST))))
					ENDIF
					
					SET_RAPPEL_FORCE_SMALL_JUMP_ON_BIG_JUMP_CONTROL(rData.sRappelData, TRUE)
				ENDIF
			ENDIF
			
			IF rData.bBlockBigJumps
				SET_RAPPEL_ALLOW_BIG_JUMPS(rData.sRappelData, 	FALSE)
			ENDIF
			
			// Adjust the speed of the rappelling AI peds
			IF bAdjustSpeeds
				
				IF NOT rData.bIsAI
					rData.sRappelData.fOverrideBigJumpSpeed		= DEFAULT_BIG_JUMP_SPEED
					rData.sRappelData.fOverrideSmallJumpSpeed	= DEFAULT_SMALL_JUMP_SPEED
				ELSE
					VECTOR vAIPos = GET_ENTITY_COORDS(ped.id)
				
					IF vAIPos.z < rData.fZTarget
						rData.sRappelData.fOverrideBigJumpSpeed		= CLAMP(rData.sRappelData.fOverrideBigJumpSpeed*(-TIMESTEP()*2.0), 		DEFAULT_BIG_JUMP_SPEED-0.15, 	DEFAULT_BIG_JUMP_SPEED)
						rData.sRappelData.fOverrideSmallJumpSpeed	= CLAMP(rData.sRappelData.fOverrideSmallJumpSpeed*(-TIMESTEP()*2.0), 	DEFAULT_SMALL_JUMP_SPEED-0.1, 	DEFAULT_SMALL_JUMP_SPEED)
					ELSE
						rData.sRappelData.fOverrideBigJumpSpeed		= CLAMP(rData.sRappelData.fOverrideBigJumpSpeed*(TIMESTEP()*2.0), 		DEFAULT_BIG_JUMP_SPEED+0.15, 	DEFAULT_BIG_JUMP_SPEED)
						rData.sRappelData.fOverrideSmallJumpSpeed	= CLAMP(rData.sRappelData.fOverrideSmallJumpSpeed*(TIMESTEP()*2.0), 	DEFAULT_SMALL_JUMP_SPEED+0.1, 	DEFAULT_SMALL_JUMP_SPEED)
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
		
		HANDLE_RAPPEL(rData.sRappelData, rData.bIsAI, rData.bDoCam)
		
		// RESTORE THE ACTUAL Z LIMIT IF ONE IS STORED
		IF fActualZLimit != 0.0
			rData.sRappelData.fLimitZ = fActualZLimit 
		ENDIF

	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC RAPPEL_Manage_1st_Descent_Player()

	// mike
	IF PLAYER_PED_ID() = MIKE_PED_ID()
		IF NOT s_rappel_mike.bIsRappeling
		
			INIT_RAPPEL_WITH_ROPE(s_rappel_mike.sRappelData, NULL, 
				<<164.243, -762.049, 250.620>>, <<0.0000,0.0000,71.145>>, 
				-10.809, -175.843, fRopeLength, TRUE, FALSE, FALSE, MIKE_PED_ID(), TRUE, TRUE, DEFAULT_ROPE_WEIGHT, fCreateRopeLength, TRUE)
			
			SET_RAPPEL_ALLOW_BIG_JUMPS(s_rappel_mike.sRappelData, TRUE)
			SET_RAPPEL_ALLOW_SMALL_JUMPS(s_rappel_mike.sRappelData, TRUE)
			
			#IF IS_DEBUG_BUILD
				v_debug_offset_m 	= GET_ENTITY_COORDS(s_rappel_mike.sRappelData.anchorObject)
				f_debug_heading_m 	= GET_ENTITY_HEADING(s_rappel_mike.sRappelData.anchorObject)
			#ENDIF
			
			s_rappel_mike.bIsRappeling 			= TRUE
			s_rappel_mike.bDoCam				= TRUE
			s_rappel_mike.bCancelAtLimit 		= FALSE
			s_rappel_mike.bSwitchToFreeRope		= FALSE
			s_rappel_mike.bBlockBigJumps		= FALSE
			s_rappel_mike.iAIFirstJumpDelay		= 250
			s_rappel_mike.eDismountWeap			= WEAPONTYPE_UNARMED
		ENDIF
	
		s_rappel_mike.bIsAI						= FALSE
		s_rappel_mike.bReachedEnd 				= RAPPEL_Manage_Ped(s_rappel_mike, peds[mpf_mike])
		IF s_rappel_mike.sRappelData.vAttachPos.z < s_rappel_mike.sRappelData.fLimitZ
			s_rappel_mike.sRappelData.vAttachPos.z = s_rappel_mike.sRappelData.fLimitZ
		ENDIF
		IF s_rappel_mike.sRappelData.vAttachPos.z < -88.5
			IF NOT bHalfWayMusic
				TRIGGER_MUSIC_EVENT("AH3B_HALF_RAPPEL")
				bHalfWayMusic = TRUE
			ENDIF
		ENDIF
	
	// frank
	ELIF PLAYER_PED_ID() = FRANK_PED_ID()
		IF NOT s_rappel_frank.bIsRappeling
	
			INIT_RAPPEL_WITH_ROPE(s_rappel_frank.sRappelData, null,
				<<164.740, -760.694, 250.770>>, <<0.0000, 0.0000, 69.395>>, 
				-10.809, -176.637, fRopeLength, TRUE, FALSE, FALSE, FRANK_PED_ID(), TRUE, TRUE, DEFAULT_ROPE_WEIGHT, fCreateRopeLength, TRUE)
			
			SET_RAPPEL_ALLOW_BIG_JUMPS(s_rappel_frank.sRappelData, TRUE)
			SET_RAPPEL_ALLOW_SMALL_JUMPS(s_rappel_frank.sRappelData, TRUE)
			
			#IF IS_DEBUG_BUILD
				v_debug_offset_f 	= GET_ENTITY_COORDS(s_rappel_frank.sRappelData.anchorObject)
				f_debug_heading_f	= GET_ENTITY_HEADING(s_rappel_frank.sRappelData.anchorObject)
			#ENDIF
			
			s_rappel_frank.bIsRappeling 		= TRUE
			s_rappel_frank.bDoCam				= TRUE
			s_rappel_frank.bCancelAtLimit 		= FALSE
			s_rappel_frank.bSwitchToFreeRope	= FALSE
			s_rappel_frank.bBlockBigJumps		= FALSE
			s_rappel_frank.iAIFirstJumpDelay	= 250
			s_rappel_frank.eDismountWeap		= WEAPONTYPE_UNARMED
		ENDIF
	
		s_rappel_frank.bIsAI				= FALSE
		s_rappel_frank.bReachedEnd 			= RAPPEL_Manage_Ped(s_rappel_frank, peds[mpf_frank])
		IF s_rappel_frank.sRappelData.vAttachPos.z < s_rappel_frank.sRappelData.fLimitZ
			s_rappel_frank.sRappelData.vAttachPos.z = s_rappel_frank.sRappelData.fLimitZ
		ENDIF
		IF s_rappel_frank.sRappelData.vAttachPos.z < -88.5
			IF NOT bHalfWayMusic
				TRIGGER_MUSIC_EVENT("AH3B_HALF_RAPPEL")
				bHalfWayMusic = TRUE
			ENDIF
		ENDIF
	ENDIF

	OVERRIDE_PLAYER_GROUND_MATERIAL(GET_HASH_KEY("AM_BASE_GLASS_STRONG"),TRUE)
	
ENDPROC

PROC RAPPEL_Manage_1st_Descent_Buddies()

	FLOAT fTargetZ
	IF (PLAYER_PED_ID() = MIKE_PED_ID() AND s_rappel_mike.sRappelData.vAttachPos.z < -152)
	OR (PLAYER_PED_ID() = FRANK_PED_ID() AND s_rappel_frank.sRappelData.vAttachPos.z < -152)
		fTargetZ = 72.0		// make target lower than the ground point so that the peds AI will go straight down after this point
	ELSE
		VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		fTargetZ = vPlayerPos.z
	ENDIF
	
	// mike
	IF BUDDY_PED_ID() = MIKE_PED_ID()
		IF NOT s_rappel_mike.bIsRappeling
			INIT_RAPPEL_WITH_ROPE(s_rappel_mike.sRappelData, NULL, 
				<<164.243, -762.049, 250.620>>, <<0.0000,0.0000,71.145>>, 
				-10.809, -175.843, fRopeLength, FALSE, FALSE, FALSE, MIKE_PED_ID(), TRUE, TRUE, DEFAULT_ROPE_WEIGHT, fCreateRopeLength, TRUE)
			
			SET_RAPPEL_ALLOW_BIG_JUMPS(s_rappel_mike.sRappelData, TRUE)
			SET_RAPPEL_ALLOW_SMALL_JUMPS(s_rappel_mike.sRappelData, TRUE)
			
			#IF IS_DEBUG_BUILD
				v_debug_offset_m 	= GET_ENTITY_COORDS(s_rappel_mike.sRappelData.anchorObject)
				f_debug_heading_m 	= GET_ENTITY_HEADING(s_rappel_mike.sRappelData.anchorObject)
			#ENDIF
			
			s_rappel_mike.bIsRappeling 			= TRUE
			s_rappel_mike.bDoCam				= FALSE
			s_rappel_mike.bCancelAtLimit 		= FALSE
			s_rappel_mike.bSwitchToFreeRope		= FALSE
			s_rappel_mike.bBlockBigJumps		= FALSE
			s_rappel_mike.iAIFirstJumpDelay		= 250
			s_rappel_mike.eDismountWeap			= WEAPONTYPE_UNARMED
		ENDIF
	
		s_rappel_mike.bIsAI					= TRUE
		s_rappel_mike.fZTarget				= fTargetZ
		s_rappel_mike.bReachedEnd 			= RAPPEL_Manage_Ped(s_rappel_mike, peds[mpf_mike])
		IF s_rappel_mike.sRappelData.vAttachPos.z < s_rappel_mike.sRappelData.fLimitZ
			s_rappel_mike.sRappelData.vAttachPos.z = s_rappel_mike.sRappelData.fLimitZ
		ENDIF
	ENDIF

	// frank
	IF BUDDY_PED_ID() = FRANK_PED_ID()
		IF NOT s_rappel_frank.bIsRappeling
	
			INIT_RAPPEL_WITH_ROPE(s_rappel_frank.sRappelData, null,
				<<164.740, -760.694, 250.770>>, <<0.0000, 0.0000, 69.395>>, 
				-10.809, -176.637, fRopeLength, FALSE, FALSE, FALSE, FRANK_PED_ID(), TRUE, TRUE, DEFAULT_ROPE_WEIGHT, fCreateRopeLength, TRUE)
			
			SET_RAPPEL_ALLOW_BIG_JUMPS(s_rappel_frank.sRappelData, TRUE)
			SET_RAPPEL_ALLOW_SMALL_JUMPS(s_rappel_frank.sRappelData, TRUE)
			
			#IF IS_DEBUG_BUILD
				v_debug_offset_f 	= GET_ENTITY_COORDS(s_rappel_frank.sRappelData.anchorObject)
				f_debug_heading_f	= GET_ENTITY_HEADING(s_rappel_frank.sRappelData.anchorObject)
			#ENDIF
			
			s_rappel_frank.bIsRappeling 		= TRUE
			s_rappel_frank.bDoCam				= FALSE
			s_rappel_frank.bCancelAtLimit 		= FALSE
			s_rappel_frank.bSwitchToFreeRope	= FALSE
			s_rappel_frank.bBlockBigJumps		= FALSE
			s_rappel_frank.iAIFirstJumpDelay	= 250
			s_rappel_frank.eDismountWeap		= WEAPONTYPE_UNARMED
			
		ENDIF

		s_rappel_frank.bIsAI				= TRUE
		s_rappel_frank.fZTarget				= fTargetZ
		s_rappel_frank.bReachedEnd 			= RAPPEL_Manage_Ped(s_rappel_frank, peds[mpf_frank])
		IF s_rappel_frank.sRappelData.vAttachPos.z < s_rappel_frank.sRappelData.fLimitZ
			s_rappel_frank.sRappelData.vAttachPos.z = s_rappel_frank.sRappelData.fLimitZ
		ENDIF
	ENDIF

	// gunman
	IF NOT s_rappel_goon.bIsRappeling
		INIT_RAPPEL_WITH_ROPE(s_rappel_goon.sRappelData, null, 
			<<163.774, -763.280, 250.820>>, <<0.0000, 0.0000, 69.395>>, 
			-14.823, -176.682, fRopeLength, FALSE, FALSE, FALSE, peds[mpf_goon].id, TRUE, TRUE, DEFAULT_ROPE_WEIGHT, fCreateRopeLength, TRUE)
		
		SET_RAPPEL_ALLOW_BIG_JUMPS(s_rappel_goon.sRappelData, TRUE)
		SET_RAPPEL_ALLOW_SMALL_JUMPS(s_rappel_goon.sRappelData, TRUE)
		
		#IF IS_DEBUG_BUILD
			v_debug_offset_g 	= GET_ENTITY_COORDS(s_rappel_goon.sRappelData.anchorObject)
			f_debug_heading_g	= GET_ENTITY_HEADING(s_rappel_goon.sRappelData.anchorObject)
		#ENDIF
		
		s_rappel_goon.bIsRappeling 			= TRUE
		s_rappel_goon.bIsAI					= TRUE
		s_rappel_goon.bDoCam				= FALSE
		s_rappel_goon.bCancelAtLimit 		= FALSE
		s_rappel_goon.bSwitchToFreeRope		= FALSE
		s_rappel_goon.bBlockBigJumps		= FALSE
		s_rappel_goon.iAIFirstJumpDelay		= 750
		s_rappel_goon.eDismountWeap			= WEAPONTYPE_UNARMED
	ENDIF
	
	s_rappel_goon.fZTarget					= fTargetZ
	s_rappel_goon.bReachedEnd 				= RAPPEL_Manage_Ped(s_rappel_goon, peds[mpf_goon])
	IF s_rappel_goon.sRappelData.vAttachPos.z < s_rappel_goon.sRappelData.fLimitZ
		s_rappel_goon.sRappelData.vAttachPos.z = s_rappel_goon.sRappelData.fLimitZ
	ENDIF
	
	#IF IS_DEBUG_BUILD
		
		// Move the anchor
		IF DOES_ENTITY_EXIST(s_rappel_mike.sRappelData.anchorObject)
			IF NOT ARE_VECTORS_EQUAL(GET_ENTITY_COORDS(s_rappel_mike.sRappelData.anchorObject), v_debug_offset_m)
				SET_ENTITY_COORDS(s_rappel_mike.sRappelData.anchorObject, v_debug_offset_m)
			ENDIF
			IF GET_ENTITY_HEADING(s_rappel_mike.sRappelData.anchorObject) != f_debug_heading_m
				SET_ENTITY_ROTATION(s_rappel_mike.sRappelData.anchorObject, <<0,0, f_debug_heading_m>>)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(s_rappel_frank.sRappelData.anchorObject)
			IF NOT ARE_VECTORS_EQUAL(GET_ENTITY_COORDS(s_rappel_frank.sRappelData.anchorObject), v_debug_offset_f)
				SET_ENTITY_COORDS(s_rappel_frank.sRappelData.anchorObject, v_debug_offset_f)
			ENDIF
			IF GET_ENTITY_HEADING(s_rappel_frank.sRappelData.anchorObject) != f_debug_heading_f
				SET_ENTITY_ROTATION(s_rappel_frank.sRappelData.anchorObject, <<0,0, f_debug_heading_f>>)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(s_rappel_goon.sRappelData.anchorObject)
			IF NOT ARE_VECTORS_EQUAL(GET_ENTITY_COORDS(s_rappel_goon.sRappelData.anchorObject), v_debug_offset_g)
				SET_ENTITY_COORDS(s_rappel_goon.sRappelData.anchorObject, v_debug_offset_g)
			ENDIF
			IF GET_ENTITY_HEADING(s_rappel_goon.sRappelData.anchorObject) != f_debug_heading_g
				SET_ENTITY_ROTATION(s_rappel_goon.sRappelData.anchorObject, <<0,0, f_debug_heading_g>>)
			ENDIF
		ENDIF

	#ENDIF
	
ENDPROC

PROC RAPPEL_Manage_Cams_1st_Descent()

	FLOAT fAlpha
	
	// Make sure the player ped's cam is the last 1 set active
	IF PLAYER_PED_ID() = MIKE_PED_ID()
		SET_CAM_ACTIVE(s_rappel_mike.sRappelData.cam, TRUE)
		
		IF s_rappel_mike.sRappelData.vAttachPos.z > fStartRappelInterp
	
			DEFINE_RAPPEL_CAM_PROPERTIES(s_rappel_mike.sRappelData, 
				fDefaultHeading_START, 
				fYDist_START, 
				fXYInputToDegrees_START, 
				fClockwiseLimit_START, 
				fAntiClockwiseLimit_START, 
				fXYSpeed_START, 
				fZDist_START, 
				fZInputToHeightFactor_START, 
				fZUpperLimit_START, 
				fZLowerLimit_START, 
				fZSpeed_START, 
				fZFOV_START
					)	
			
			MODIFY_RAPPEL_CAM_POINT_AT(s_rappel_mike.sRappelData, vLookAtOffset_START)	
		
		// start interpolation
		ELSE
			
			fAlpha 							= CLAMP(ABSF((s_rappel_mike.sRappelData.vAttachPos.z-fStartRappelInterp)/(s_rappel_mike.sRappelData.fLimitZ - fStartRappelInterp)), 0.0, 1.0)

			DEFINE_RAPPEL_CAM_PROPERTIES(s_rappel_mike.sRappelData,
				LERP_FLOAT(fDefaultHeading_START, 			fDefaultHeading_END, 			fAlpha),
				LERP_FLOAT(fYDist_START,					fYDist_END,						fAlpha),
				LERP_FLOAT(fXYInputToDegrees_START, 		fXYInputToDegrees_END, 			fAlpha),
				LERP_FLOAT(fClockwiseLimit_START,			fClockwiseLimit_END, 			fAlpha),
				LERP_FLOAT(fAntiClockwiseLimit_START, 		fAntiClockwiseLimit_END,		fAlpha),
				LERP_FLOAT(fXYSpeed_START, 					fXYSpeed_END,					fAlpha),
				LERP_FLOAT(fZDist_START, 					fZDist_END,						fAlpha),
				LERP_FLOAT(fZInputToHeightFactor_START, 	fZInputToHeightFactor_END,		fAlpha),
				LERP_FLOAT(fZUpperLimit_START,				fZUpperLimit_END,				fAlpha),
				LERP_FLOAT(fZLowerLimit_START, 				fZLowerLimit_END,				fAlpha),
				LERP_FLOAT(fZSpeed_START, 					fZSpeed_END,					fAlpha),
				LERP_FLOAT(fZFOV_START, 					fZFOV_END,						fAlpha)
				)
					
			MODIFY_RAPPEL_CAM_POINT_AT(s_rappel_mike.sRappelData, 	
				LERP_VECTOR(vLookAtOffset_START, 			vLookAtOffset_END,				fAlpha)
				)
			
		ENDIF
		
	ELSE
		SET_CAM_ACTIVE(s_rappel_frank.sRappelData.cam, TRUE)
		
		IF s_rappel_frank.sRappelData.vAttachPos.z > fStartRappelInterp 
	
			DEFINE_RAPPEL_CAM_PROPERTIES(s_rappel_frank.sRappelData, 
				fDefaultHeading_START, 
				fYDist_START, 
				fXYInputToDegrees_START, 
				fClockwiseLimit_START, 
				fAntiClockwiseLimit_START, 
				fXYSpeed_START, 
				fZDist_START, 
				fZInputToHeightFactor_START, 
				fZUpperLimit_START, 
				fZLowerLimit_START, 
				fZSpeed_START, 
				fZFOV_START
					)	
			
			MODIFY_RAPPEL_CAM_POINT_AT(s_rappel_frank.sRappelData, vLookAtOffset_START)	
		
		// start interpolation
		ELSE
			
			fAlpha 							= CLAMP(ABSF((s_rappel_frank.sRappelData.vAttachPos.z - fStartRappelInterp)/(s_rappel_frank.sRappelData.fLimitZ - fStartRappelInterp)), 0.0, 1.0)

			DEFINE_RAPPEL_CAM_PROPERTIES(s_rappel_frank.sRappelData,
				LERP_FLOAT(fDefaultHeading_START, 			fDefaultHeading_END, 			fAlpha),
				LERP_FLOAT(fYDist_START,					fYDist_END,						fAlpha),
				LERP_FLOAT(fXYInputToDegrees_START, 		fXYInputToDegrees_END, 			fAlpha),
				LERP_FLOAT(fClockwiseLimit_START,			fClockwiseLimit_END, 			fAlpha),
				LERP_FLOAT(fAntiClockwiseLimit_START, 		fAntiClockwiseLimit_END,		fAlpha),
				LERP_FLOAT(fXYSpeed_START, 					fXYSpeed_END,					fAlpha),
				LERP_FLOAT(fZDist_START, 					fZDist_END,						fAlpha),
				LERP_FLOAT(fZInputToHeightFactor_START, 	fZInputToHeightFactor_END,		fAlpha),
				LERP_FLOAT(fZUpperLimit_START,				fZUpperLimit_END,				fAlpha),
				LERP_FLOAT(fZLowerLimit_START, 				fZLowerLimit_END,				fAlpha),
				LERP_FLOAT(fZSpeed_START, 					fZSpeed_END,					fAlpha),
				LERP_FLOAT(fZFOV_START, 					fZFOV_END,						fAlpha)
				)
					
			MODIFY_RAPPEL_CAM_POINT_AT(s_rappel_frank.sRappelData, 	
				LERP_VECTOR(vLookAtOffset_START, 			vLookAtOffset_END,				fAlpha)
				)
			
		ENDIF
	ENDIF
	
	SET_USE_HI_DOF()
	CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(2.0)
	CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE(2.1)

ENDPROC

PROC RAPPEL_Manage_2nd_Descent()

	// Manage mikes small rappel
	IF s_rappel_mike2.bIsRappeling
	
		s_rappel_mike2.bDoCam				= TRUE
		s_rappel_mike2.bCancelAtLimit 		= TRUE
		s_rappel_mike2.bSwitchToFreeRope	= TRUE
		s_rappel_mike2.fSwitchLimit			= -20.0
		s_rappel_mike2.bBlockBigJumps		= TRUE
		s_rappel_mike2.eDismountWeap		= WEAPONTYPE_CARBINERIFLE
	
		IF PLAYER_PED_ID() = MIKE_PED_ID()
			s_rappel_mike2.bIsAI			= FALSE
			s_rappel_mike2.bReachedEnd 		= RAPPEL_Manage_Ped(s_rappel_mike2, peds[mpf_mike])
		ELSE
			s_rappel_mike2.bIsAI			= TRUE
			s_rappel_mike2.fZTarget			= 46.0770
			s_rappel_mike2.bReachedEnd 		= RAPPEL_Manage_Ped(s_rappel_mike2, peds[mpf_mike])
		ENDIF
		
		IF s_rappel_mike2.bReachedEnd
			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(MIKE_PED_ID(), FALSE)
			Create_Ped_Cover_Point(peds[mpf_mike], <<144.6043, -720.4959, 46.0770>>, 71.7143, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
			s_rappel_mike2.bIsRappeling 	= FALSE
			
			IF PLAYER_PED_ID() = MIKE_PED_ID()
				TRIGGER_MUSIC_EVENT("AH3B_ON_GROUND")
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_RAPPEL_02")
					STOP_AUDIO_SCENE("AH_3B_RAPPEL_02")
				ENDIF
				IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_GET_TO_VEHICLE")
					START_AUDIO_SCENE("AH_3B_GET_TO_VEHICLE")
				ENDIF
				
				RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT_INTERP_TO_FROM_GAME, TRUE)
				CLEAR_HELP()
			ENDIF
		ELSE
			IF PLAYER_PED_ID() = MIKE_PED_ID()
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("A3B_RPLHLP2")
				AND s_rappel_mike2.sRappelData.bFreeRoping
					PRINT_HELP_FOREVER("A3B_RPLHLP2")
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	// Manage Frank's small rappel
	IF s_rappel_frank2.bIsRappeling
	
		s_rappel_frank2.bDoCam					= TRUE
		s_rappel_frank2.bCancelAtLimit 			= TRUE
		s_rappel_frank2.bSwitchToFreeRope		= TRUE
		s_rappel_frank2.fSwitchLimit			= -20.0
		s_rappel_frank2.bBlockBigJumps			= TRUE
		s_rappel_frank2.eDismountWeap			= WEAPONTYPE_CARBINERIFLE
	
		IF PLAYER_PED_ID() = FRANK_PED_ID()
			s_rappel_frank2.bIsAI				= FALSE
			s_rappel_frank2.bReachedEnd 		= RAPPEL_Manage_Ped(s_rappel_frank2, peds[mpf_frank])
		ELSE
			s_rappel_frank2.bIsAI				= TRUE
			s_rappel_frank2.fZTarget			= 46.0770
			s_rappel_frank2.bReachedEnd 		= RAPPEL_Manage_Ped(s_rappel_frank2, peds[mpf_frank])
		ENDIF
	
		IF s_rappel_frank2.bReachedEnd
		
			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(FRANK_PED_ID(), FALSE)
			Create_Ped_Cover_Point(peds[mpf_frank], <<153.2881, -723.7296, 46.0770>>, 69.0961, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
			s_rappel_frank2.bIsRappeling 	= FALSE
			
			IF PLAYER_PED_ID() = FRANK_PED_ID()
				TRIGGER_MUSIC_EVENT("AH3B_ON_GROUND")
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_RAPPEL_02")
					STOP_AUDIO_SCENE("AH_3B_RAPPEL_02")
				ENDIF
				IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_GET_TO_VEHICLE")
					START_AUDIO_SCENE("AH_3B_GET_TO_VEHICLE")
				ENDIF
				RENDER_SCRIPT_CAMS(FALSE, TRUE, DEFAULT_INTERP_TO_FROM_GAME, TRUE)
				CLEAR_HELP()
			ENDIF
		ELSE
			IF PLAYER_PED_ID() = FRANK_PED_ID()
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("A3B_RPLHLP2")
				AND s_rappel_mike2.sRappelData.bFreeRoping
					PRINT_HELP_FOREVER("A3B_RPLHLP2")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	// Manage gunman's small rappel
	IF s_rappel_goon2.bIsRappeling
	
		s_rappel_goon2.bDoCam					= FALSE
		s_rappel_goon2.bCancelAtLimit 			= TRUE
		s_rappel_goon2.bSwitchToFreeRope		= TRUE
		s_rappel_goon2.fSwitchLimit				= -20.0
		s_rappel_goon2.bBlockBigJumps			= TRUE
		s_rappel_goon2.eDismountWeap			= WEAPONTYPE_CARBINERIFLE
		
		s_rappel_goon2.bIsAI					= TRUE
		s_rappel_goon2.fZTarget					= 46.0770
	
		IF RAPPEL_Manage_Ped(s_rappel_goon2, peds[mpf_goon])
			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(peds[mpf_goon].id, FALSE)
			Create_Ped_Cover_Point(peds[mpf_goon], <<134.3783, -721.2941, 46.0770>>, 70.2955, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120)
			
			s_rappel_goon2.bIsRappeling 	= FALSE
			s_rappel_goon2.bReachedEnd 		= TRUE
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		
		// Move the anchor
		IF DOES_ENTITY_EXIST(s_rappel_mike2.sRappelData.anchorObject)
			IF NOT ARE_VECTORS_EQUAL(GET_ENTITY_COORDS(s_rappel_mike2.sRappelData.anchorObject), v_debug_offset_m2)
				SET_ENTITY_COORDS(s_rappel_mike2.sRappelData.anchorObject, v_debug_offset_m2)
			ENDIF
			IF GET_ENTITY_HEADING(s_rappel_mike2.sRappelData.anchorObject) != f_debug_heading_m2
				SET_ENTITY_ROTATION(s_rappel_mike2.sRappelData.anchorObject, <<0,0, f_debug_heading_m2>>)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(s_rappel_frank2.sRappelData.anchorObject)
			IF NOT ARE_VECTORS_EQUAL(GET_ENTITY_COORDS(s_rappel_frank2.sRappelData.anchorObject), v_debug_offset_f2)
				SET_ENTITY_COORDS(s_rappel_frank2.sRappelData.anchorObject, v_debug_offset_f2)
			ENDIF
			IF GET_ENTITY_HEADING(s_rappel_frank2.sRappelData.anchorObject) != f_debug_heading_f2
				SET_ENTITY_ROTATION(s_rappel_frank2.sRappelData.anchorObject, <<0,0, f_debug_heading_f2>>)
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(s_rappel_goon2.sRappelData.anchorObject)
			IF NOT ARE_VECTORS_EQUAL(GET_ENTITY_COORDS(s_rappel_goon2.sRappelData.anchorObject), v_debug_offset_g2)
				SET_ENTITY_COORDS(s_rappel_goon2.sRappelData.anchorObject, v_debug_offset_g2)
			ENDIF
			IF GET_ENTITY_HEADING(s_rappel_goon2.sRappelData.anchorObject) != f_debug_heading_g2
				SET_ENTITY_ROTATION(s_rappel_goon2.sRappelData.anchorObject, <<0,0, f_debug_heading_g2>>)
			ENDIF
		ENDIF

	#ENDIF

ENDPROC


PROC RAPPEL_Do_Random_Dialogue()

	IF IS_SAFE_TO_START_CONVERSATION()
	AND GET_GAME_TIMER() > i_dialogue_timer
	AND GET_GAME_TIMER() - i_time_of_last_convo > 2000

		IF IS_STRING_NULL_OR_EMPTY(str_dialogue_root)
			IF PLAYER_PED_ID() = MIKE_PED_ID()
				IF s_rappel_mike.sRappelData.state = RAPPEL_STATE_WALKING OR s_rappel_mike.sRappelData.state = RAPPEL_STATE_JUMPING_STATIC OR s_rappel_mike.sRappelData.state = RAPPEL_STATE_JUMPING_DOWN
				OR s_rappel_mike2.sRappelData.state = RAPPEL_STATE_WALKING OR s_rappel_mike2.sRappelData.state = RAPPEL_STATE_JUMPING_STATIC OR s_rappel_mike2.sRappelData.state = RAPPEL_STATE_JUMPING_DOWN
					str_dialogue_root = "AH_MICJUMP"
				ELSE
					str_dialogue_root = ""
				ENDIF
			ELSE
				IF s_rappel_frank.sRappelData.state = RAPPEL_STATE_WALKING OR s_rappel_frank.sRappelData.state = RAPPEL_STATE_JUMPING_STATIC OR s_rappel_frank.sRappelData.state = RAPPEL_STATE_JUMPING_DOWN
				OR s_rappel_frank2.sRappelData.state = RAPPEL_STATE_WALKING OR s_rappel_frank2.sRappelData.state = RAPPEL_STATE_JUMPING_STATIC OR s_rappel_frank2.sRappelData.state = RAPPEL_STATE_JUMPING_DOWN
					str_dialogue_root = "AH_FRANJUMP"
				ELSE
					str_dialogue_root = ""
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(str_dialogue_root)
			IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
				i_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(6000, 7000)
				str_dialogue_root = ""
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

PROC SETUP_FIB_COVERPOINTS(BOOL bEnable)
	
	IF bEnable
	
		// Outside destroyed communiations room
		covFIBPlayerPriorityCover[0] = ADD_COVER_POINT(<<119.9658, -740.4022, 249.1520>>, 161.4701, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
		
		// Doorway just before the paramedic
		covFIBPlayerPriorityCover[1] = ADD_COVER_POINT(<<135.5736, -742.7592, 249.1520>>, 72.4091, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120, TRUE)
		
		// Fallen over the server stack, corridor before the crashed heli
		covFIBPlayerPriorityCover[2] = ADD_COVER_POINT(<<122.1301, -740.8215, 253.1523>>, 254.1523, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120, TRUE)
	
	ELSE
		INT i
		REPEAT COUNT_OF(covFIBPlayerPriorityCover) i
			IF covFIBPlayerPriorityCover[i] != NULL
				REMOVE_COVER_POINT(covFIBPlayerPriorityCover[i])
				covFIBPlayerPriorityCover[i] = NULL
			ENDIF
		ENDREPEAT
	ENDIF
	
ENDPROC

PROC RESET_SPRINKLERS()

	Event_Reset(mef_manage_sprinklers)

	INT i
	REPEAT COUNT_OF(s_Sprinklers) i
	
		IF s_Sprinklers[i].iCurrentModel != V_ILEV_FIB_SPRKLR
		AND NOT IS_VECTOR_ZERO(s_Sprinklers[i].vCoord)
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(s_Sprinklers[i].vCoord, 0.25, s_Sprinklers[i].iCurrentModel)
				REMOVE_MODEL_SWAP(s_Sprinklers[i].vCoord, 0.25, V_ILEV_FIB_SPRKLR, s_Sprinklers[i].iCurrentModel, FALSE)
			ENDIF
		ENDIF
		
		s_Sprinklers[i].iCurrentModel = V_ILEV_FIB_SPRKLR
	ENDREPEAT

ENDPROC

PROC MANAGE_BUDDIES_ATTACKING_BASED_OFF_WANTED()

// BUDDY
//--------------------------
	
	IF DOES_ENTITY_EXIST(BUDDY_PED_ID())
	
	// Stop the peds attacking hated enemies, while you don't have a wanted level
	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
	AND NOT ARE_PLAYER_STARS_GREYED_OUT(PLAYER_ID())
		SET_PED_COMBAT_ATTRIBUTES(BUDDY_PED_ID(), CA_DO_DRIVEBYS, TRUE)
		SET_PED_RESET_FLAG(BUDDY_PED_ID(), PRF_BlockWeaponFire, FALSE)
		SET_PED_CONFIG_FLAG(BUDDY_PED_ID(), PCF_OnlyAttackLawIfPlayerIsWanted, FALSE)
	ELSE
		SET_PED_COMBAT_ATTRIBUTES(BUDDY_PED_ID(), CA_DO_DRIVEBYS, FALSE)
		SET_PED_RESET_FLAG(BUDDY_PED_ID(), PRF_BlockWeaponFire, TRUE)
		SET_PED_CONFIG_FLAG(BUDDY_PED_ID(), PCF_OnlyAttackLawIfPlayerIsWanted, TRUE)
	ENDIF

	IF HAS_PED_GOT_WEAPON(BUDDY_PED_ID(), weap_Frank)
		IF HAS_PED_GOT_WEAPON_COMPONENT(BUDDY_PED_ID(), weap_Frank, WEAPONCOMPONENT_AT_AR_SUPP)
			REMOVE_WEAPON_COMPONENT_FROM_PED(BUDDY_PED_ID(), weap_Frank, WEAPONCOMPONENT_AT_AR_SUPP)
		ENDIF
		SET_PED_FLASH_LIGHT(peds[BUDDY_MPF()], FALSE)
		
		IF GET_AMMO_IN_PED_WEAPON(BUDDY_PED_ID(), weap_Frank) <= 60
			SET_PED_AMMO(BUDDY_PED_ID(), weap_Frank, 600)
		ENDIF
	
		WEAPON_TYPE eWeap
		eWeap = WEAPONTYPE_INVALID
		GET_CURRENT_PED_VEHICLE_WEAPON(BUDDY_PED_ID(), eWeap)
		IF eWeap != weap_Frank
			SET_CURRENT_PED_VEHICLE_WEAPON(BUDDY_PED_ID(), weap_Frank)
		ENDIF
	ENDIF
	
	SET_PED_ACCURACY(BUDDY_PED_ID(), 40)
		
	ENDIF
	
// GUNMAN
//--------------------------
	
	IF DOES_ENTITY_EXIST(peds[mpf_goon].id)

	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
	AND NOT ARE_PLAYER_STARS_GREYED_OUT(PLAYER_ID())
		SET_PED_COMBAT_ATTRIBUTES(peds[mpf_goon].id, CA_DO_DRIVEBYS, TRUE)
		SET_PED_RESET_FLAG(peds[mpf_goon].id, PRF_BlockWeaponFire, FALSE)
		SET_PED_CONFIG_FLAG(peds[mpf_goon].id, PCF_OnlyAttackLawIfPlayerIsWanted, FALSE)
	ELSE
		SET_PED_COMBAT_ATTRIBUTES(peds[mpf_goon].id, CA_DO_DRIVEBYS, FALSE)
		SET_PED_RESET_FLAG(peds[mpf_goon].id, PRF_BlockWeaponFire, TRUE)
		SET_PED_CONFIG_FLAG(peds[mpf_goon].id, PCF_OnlyAttackLawIfPlayerIsWanted, TRUE)
	ENDIF
	
	IF HAS_PED_GOT_WEAPON(peds[mpf_goon].id, weap_Goon)
		IF HAS_PED_GOT_WEAPON_COMPONENT(peds[mpf_goon].id, weap_Goon, WEAPONCOMPONENT_AT_AR_SUPP)
			REMOVE_WEAPON_COMPONENT_FROM_PED(peds[mpf_goon].id, weap_Goon, WEAPONCOMPONENT_AT_AR_SUPP)
		ENDIF
		SET_PED_FLASH_LIGHT(peds[mpf_goon], FALSE)
		
		IF GET_AMMO_IN_PED_WEAPON(peds[mpf_goon].id, weap_Goon) <= 60
			SET_PED_AMMO(peds[mpf_goon].id, weap_Goon, 600)
		ENDIF

		WEAPON_TYPE eWeap
		eWeap = WEAPONTYPE_INVALID
		GET_CURRENT_PED_VEHICLE_WEAPON(peds[mpf_goon].id, eWeap)
		IF eWeap != weap_Goon
			SET_CURRENT_PED_VEHICLE_WEAPON(peds[mpf_goon].id, weap_Goon)
		ENDIF
	ENDIF
	
	SET_PED_ACCURACY(peds[mpf_goon].id, 40)
	
	ENDIF
	
// DRIVER
//--------------------------
	
	IF DOES_ENTITY_EXIST(peds[mpf_driver].id)

	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
	AND NOT ARE_PLAYER_STARS_GREYED_OUT(PLAYER_ID())
		SET_PED_COMBAT_ATTRIBUTES(peds[mpf_driver].id, CA_DO_DRIVEBYS, TRUE)
		SET_PED_RESET_FLAG(peds[mpf_driver].id, PRF_BlockWeaponFire, FALSE)
		SET_PED_CONFIG_FLAG(peds[mpf_driver].id, PCF_OnlyAttackLawIfPlayerIsWanted, FALSE)
	ELSE
		SET_PED_COMBAT_ATTRIBUTES(peds[mpf_driver].id, CA_DO_DRIVEBYS, FALSE)
		SET_PED_RESET_FLAG(peds[mpf_driver].id, PRF_BlockWeaponFire, TRUE)
		SET_PED_CONFIG_FLAG(peds[mpf_driver].id, PCF_OnlyAttackLawIfPlayerIsWanted, TRUE)
	ENDIF
	
	IF HAS_PED_GOT_WEAPON(peds[mpf_driver].id, weap_Driver)
		IF HAS_PED_GOT_WEAPON_COMPONENT(peds[mpf_driver].id, weap_Driver, WEAPONCOMPONENT_AT_AR_SUPP)
			REMOVE_WEAPON_COMPONENT_FROM_PED(peds[mpf_driver].id, weap_Driver, WEAPONCOMPONENT_AT_AR_SUPP)
		ENDIF
		SET_PED_FLASH_LIGHT(peds[mpf_driver], FALSE)
		
		IF GET_AMMO_IN_PED_WEAPON(peds[mpf_driver].id, weap_Driver) <= 60
			SET_PED_AMMO(peds[mpf_driver].id, weap_Driver, 600)
		ENDIF

		WEAPON_TYPE eWeap
		eWeap = WEAPONTYPE_INVALID
		GET_CURRENT_PED_VEHICLE_WEAPON(peds[mpf_driver].id, eWeap)
		IF eWeap != weap_Goon
			SET_CURRENT_PED_VEHICLE_WEAPON(peds[mpf_driver].id, weap_Driver)
		ENDIF
	ENDIF
	
	SET_PED_ACCURACY(peds[mpf_driver].id, 40)
	
	ENDIF
	
ENDPROC

PROC MANAGE_AI_BUDDY_WEAPON()
	WEAPON_TYPE eWeapDesired
	WEAPON_TYPE eWeapCurrent
	
	IF NOT IS_CUTSCENE_PLAYING()
	
		GET_CURRENT_PED_WEAPON(BUDDY_PED_ID(), eWeapCurrent)
	
		IF BUDDY_PED_ID() = MIKE_PED_ID()
			eWeapDesired = weap_Mike
		ELIF BUDDY_PED_ID() = FRANK_PED_ID()
			eWeapDesired = weap_Frank
		ENDIF
		
		BOOL bSetAWeapon
		
		IF HAS_PED_GOT_WEAPON(BUDDY_PED_ID(), eWeapDesired)
		
			IF GET_AMMO_IN_PED_WEAPON(BUDDY_PED_ID(), eWeapDesired) != 0
				IF eWeapCurrent != eWeapDesired
					SET_CURRENT_PED_WEAPON(BUDDY_PED_ID(), eWeapDesired, TRUE)
				ENDIF
				bSetAWeapon = TRUE
			ENDIF
		ENDIF
		
		// Look for best weapon
		IF NOT bSetAWeapon
			eWeapDesired = GET_BEST_PED_WEAPON(BUDDY_PED_ID())
			IF eWeapDesired != WEAPONTYPE_INVALID
				IF eWeapCurrent != eWeapDesired
					SET_CURRENT_PED_WEAPON(BUDDY_PED_ID(), eWeapDesired, TRUE)
				ENDIF
				bSetAWeapon = TRUE
			ENDIF
		ENDIF
	
	ENDIF
	
ENDPROC

PROC SET_BUILDING_IPLS(MISSION_STAGE_FLAG eStage)

	BOOL bDestroyed

	SWITCH eStage
		CASE msf_0_intro_cut				bDestroyed = FALSE		BREAK
		CASE msf_1_go_to_helipad			bDestroyed = FALSE		BREAK
		CASE msf_2_fly_to_jump_zone			bDestroyed = FALSE		BREAK
		CASE msf_3_sky_dive_and_land		bDestroyed = FALSE		BREAK
		CASE msf_4_hack_the_computer		bDestroyed = FALSE		BREAK
		CASE msf_5_download_shootout		bDestroyed = FALSE		BREAK
		CASE msf_6_stairwell				bDestroyed = FALSE		BREAK
		CASE msf_7_burnt_out_floor_1		bDestroyed = TRUE		BREAK
		CASE msf_8_burnt_out_floor_2		bDestroyed = TRUE		BREAK
		CASE msf_9_abseil_down				bDestroyed = TRUE		BREAK
		CASE msf_10_get_to_getaway_veh		bDestroyed = TRUE		BREAK
		CASE msf_11_escape_to_franklins		bDestroyed = TRUE		BREAK
		CASE msf_12_mission_passed			bDestroyed = TRUE		BREAK
	ENDSWITCH

	IF bDestroyed
	
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_HOLE_PLUG, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_C4, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
		SET_BUILDING_STATE(BUILDINGNAME_IPL_HELIHOLE, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
		SET_BUILDING_STATE(BUILDINGNAME_MS_FIB_DAMAGE_HLOD, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
		SET_BUILDING_STATE(BUILDINGNAME_MS_FIB_DAMAGE_LOD, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
		SET_BUILDING_STATE(BUILDINGNAME_MS_FIB_DAMAGE_SLOD, BUILDINGSTATE_DESTROYED, FALSE, DEFAULT, TRUE )
	
	ELSE
	
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_HOLE_PLUG, BUILDINGSTATE_NORMAL, FALSE, DEFAULT, TRUE )
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_C4, BUILDINGSTATE_NORMAL, FALSE, DEFAULT, TRUE )
		SET_BUILDING_STATE(BUILDINGNAME_IPL_HELIHOLE, BUILDINGSTATE_NORMAL, FALSE, DEFAULT, TRUE )
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_REPAIR, BUILDINGSTATE_NORMAL, FALSE, DEFAULT, TRUE )
		SET_BUILDING_STATE(BUILDINGNAME_MS_FIB_DAMAGE_HLOD, BUILDINGSTATE_NORMAL, FALSE, DEFAULT, TRUE )
		SET_BUILDING_STATE(BUILDINGNAME_MS_FIB_DAMAGE_LOD, BUILDINGSTATE_NORMAL, FALSE, DEFAULT, TRUE )
		SET_BUILDING_STATE(BUILDINGNAME_MS_FIB_DAMAGE_SLOD, BUILDINGSTATE_NORMAL, FALSE, DEFAULT, TRUE )	
	
	ENDIF

ENDPROC

//PURPOSE: Resets everything ready for a skip, clears all entities, resets flags, etc...
PROC Mission_Reset_Everything()	
	RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
	
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
		
		//Parachute stored set back
		IF i_SotredChuteColour_Mike != -1
			// ped already has chute, make sure the colour is right
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
				SET_PED_PARACHUTE_TINT_INDEX(PLAYER_PED_ID(), i_SotredChuteColour_Mike)
			
			// doesnt have chute, give to ped and set colour
			ELSE
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1, FALSE, FALSE)
				SET_PED_PARACHUTE_TINT_INDEX(PLAYER_PED_ID(), i_SotredChuteColour_Mike)
			ENDIF
			i_SotredChuteColour_Mike = -1
		// no stored chute, remove chute from player
		ELSE
			REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
			CLEAR_PED_PARACHUTE_PACK_VARIATION(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
	Remove_All_Peds_For_Dialogue()
	Events_Reset_All()
	DOORS_Reset_All()
	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(locates_data)
	KILL_ANY_CONVERSATION()
	CLEAR_PRINTS()
	CLEAR_HELP()
	DESTROY_ALL_CAMS()
	
	Wipe_RenderTargets(FALSE)
	
	IF NOT IS_PED_INJURED(MIKE_PED_ID())
		RESTORE_PLAYER_PED_INFO(MIKE_PED_ID())
	ENDIF
	
	IF NOT IS_PED_INJURED(FRANK_PED_ID())
		RESTORE_PLAYER_PED_INFO(FRANK_PED_ID())
	ENDIF
	
	CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	
	SET_RAPPEL_ASSETS_AS_NO_LONGER_NEEDED()
	
	// Delete all peds
	INT i, j
	REPEAT COUNT_OF(peds) i
		IF DOES_ENTITY_EXIST(peds[i].id)
		AND PLAYER_PED_ID() != peds[i].id
			DELETE_PED(peds[i].id)
		ENDIF
		
		SAFE_REMOVE_BLIP(peds[i].objective_blip)
		IF peds[i].bDoesCovExist
			REMOVE_COVER_POINT(peds[i].cov)
			peds[i].bDoesCovExist = FALSE
		ENDIF
		peds[i].iGenericFlag = 0
		peds[i].iDeadTimer = 0
		peds[i].bHasBeenKilled = FALSE
	ENDREPEAT
	
	// Delete all vehicles	
	REPEAT COUNT_OF(vehs) i
	
		FOR j = 0 TO 3
			IF DOES_ENTITY_EXIST(vehs[i].peds[j].id)
			AND PLAYER_PED_ID() != vehs[i].peds[j].id
				SAFE_REMOVE_BLIP(vehs[i].peds[j].objective_blip)
				IF vehs[i].peds[j].bDoesCovExist
					REMOVE_COVER_POINT(vehs[i].peds[j].cov)
					vehs[i].peds[j].bDoesCovExist = FALSE
				ENDIF
				vehs[i].peds[j].iGenericFlag = 0
				vehs[i].peds[j].iDeadTimer = 0
				DELETE_PED(vehs[i].peds[j].id)
			ENDIF
		ENDFOR	
	
		IF DOES_ENTITY_EXIST(vehs[i].id)
		
			IF IS_VEHICLE_DRIVEABLE(vehs[i].id) 
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[i].id)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehs[i].id)
				ENDIF
			ENDIF

			DELETE_VEHICLE(vehs[i].id)
		ENDIF
	ENDREPEAT
	
	IF bDoorHidden
		CLEAR_AREA(<<151.150497,-762.697021,257.152008>> , 5.125, TRUE)
		REMOVE_MODEL_HIDE(<<150.5931, -762.3801, 258.3018>>, 1.0, mod_OriginalHackDoor)
		CDEBUG1LN(DEBUG_MIKE, DEBUG_MIKE, "Removing model hide")
	ENDIF
	
	IF bSwappedGlass
		CLEAR_AREA(<<139.4704, -745.9282, 263.1190>>, 1.0, TRUE)
		//REMOVE_MODEL_SWAP(<<139.4704, -745.9282, 263.1190>>, 0.1, V_ILEV_FIB_ATRGLSWAP, PROP_FIB_SKYLIGHT_PLUG)
		
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_ROOF, BUILDINGSTATE_NORMAL)
		
		CDEBUG1LN(DEBUG_MIKE, DEBUG_MIKE, "Removing glass model swap")
	ENDIF
	
	REPEAT enum_to_int(mpuf_num_pickups) i
		IF DOES_PICKUP_EXIST(pkups[i])
			REMOVE_PICKUP(pkups[i])
		ENDIF
	ENDREPEAT
	
	// Delete all objects
	FOR i = 0 TO enum_to_int(mof_num_objects)-1
		IF DOES_ENTITY_EXIST(objs[i].id)
			IF objs[i].bCustom
				DELETE_OBJECT(objs[i].id)
			ELSE
				VECTOR vPos = GET_ENTITY_COORDS(objs[i].id)
				SET_OBJECT_AS_NO_LONGER_NEEDED(objs[i].id)
				CLEAR_AREA(vPos, 0.5, TRUE, TRUE, FALSE, FALSE)
			ENDIF
		ENDIF
	ENDFOR
	CLEAR_AREA_OF_OBJECTS(<<151.29, -766.31, 258.05>>, 2.0)
	
	REPEAT COUNT_OF(s_Sprinklers) i
		IF DOES_ENTITY_EXIST(s_Sprinklers[i].objSprinkler)
			DELETE_OBJECT(s_Sprinklers[i].objSprinkler)
		ENDIF
		s_Sprinklers[i].iCurrentModel = V_ILEV_FIB_SPRKLR
	ENDREPEAT
	
	REPEAT COUNT_OF(ptfxs) i
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxs[i])
			STOP_PARTICLE_FX_LOOPED(ptfxs[i])
		ENDIF
	ENDREPEAT
	
	// remove objective blip
	IF DOES_BLIP_EXIST(blip_Objective)
		REMOVE_BLIP(blip_Objective)
	ENDIF
	
	REPEAT COUNT_OF(sounds) i
		IF sounds[i] != 0
			STOP_SOUND(sounds[i])
		ENDIF
	ENDREPEAT
	
	Remove_All_Peds_For_Dialogue()
	
	// Reset flags
	iCrewAIStage				= 0
	bRappelShownHelp			= FALSE
	#IF IS_DEBUG_BUILD
		i_debug_EnemiesSpawned		= 0
	#ENDIF
	i_EnemiesAlive				= 0
	i_TimeLastSpawned			= 0
	i_KillsSinceLastSpawned		= 0
	i_fps_state					= DC_CAM_COVER_ENTER
	i_heli_cam_type 			= 0
	i_heli_cam_timer			= 0
	i_heli_cam_duration			= 0
	bPlayerLandedSafely			= FALSE
	bFrankLandedSafely			= FALSE
	bGoonLandedSafely			= FALSE
	b_frank_parachute_deployed	= FALSE
	b_goon_parachute_deployed	= FALSE
	bHasLookedAtScreenBefore	= FALSE
	iHelpTimer					= -1
	bDoorHidden					= FALSE
	bGetwayPickUpReached		= FALSE
	bDriverHasArrived 			= FALSE
	bSwatHaveArrived			= FALSE
	iDesiredSprinklerState		= -1
	bSwatSeenAmbulance			= FALSE
	
	RESET_RAPPEL_ADDITIONAL(s_rappel_mike)
	RESET_RAPPEL_ADDITIONAL(s_rappel_frank)
	RESET_RAPPEL_ADDITIONAL(s_rappel_goon)
	RESET_RAPPEL_ADDITIONAL(s_rappel_mike2)
	RESET_RAPPEL_ADDITIONAL(s_rappel_frank2)
	RESET_RAPPEL_ADDITIONAL(s_rappel_goon2)
	
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)

	REMOVE_DECALS_IN_RANGE(vCleanupAreaCoord, fCleanupAreaRadius)
	REMOVE_PARTICLE_FX_IN_RANGE(vCleanupAreaCoord, fCleanupAreaRadius)
	CLEAR_AREA(vCleanupAreaCoord, fCleanupAreaRadius, TRUE)
	
	IF IS_CELLPHONE_DISABLED()
		DISABLE_CELLPHONE(FALSE)
	ENDIF
	
	// bug 1748503
//	SET_BUILDING_IPLS( msf_0_intro_cut )
	
	Disable_Roads_And_Paths_Around_FIB_Building(FALSE)
	
	RESET_HACKING_DATA(sHackingData, FALSE)
	
//	IF NOT IS_REPLAY_IN_PROGRESS()
//		IF HAS_SCALEFORM_MOVIE_LOADED(sfHacking)
//			CALL_SCALEFORM_MOVIE_METHOD(sfHacking, "RESTART_MOVIE")
//		ENDIF
//	ENDIF
	
	IF covPair_buddy.covID[0] != NULL
		REMOVE_COVER_POINT(covPair_buddy.covID[0])
	ENDIF
	IF covPair_buddy.covID[1] != NULL
		REMOVE_COVER_POINT(covPair_buddy.covID[1])
	ENDIF
	covPair_buddy.id = 0
	
	IF covPair_gunman.covID[0] != NULL
		REMOVE_COVER_POINT(covPair_gunman.covID[0])
	ENDIF
	IF covPair_gunman.covID[1] != NULL
		REMOVE_COVER_POINT(covPair_gunman.covID[1])
	ENDIF
	covPair_gunman.id = 0
	
	PLAY_ALARM_SECURITY(FALSE, TRUE)
	PLAY_ALARM_FIRE(FALSE, TRUE)
	
	REFRESH_INTERIOR(interior_fib03)
	
	STOP_AUDIO_SCENES()
	DISTANT_COP_CAR_SIRENS(FALSE)

	REPEAT COUNT_OF(i_NavMeshBlockingAreas) i
		IF i_NavMeshBlockingAreas[i] != -1
			REMOVE_NAVMESH_BLOCKING_OBJECT(i_NavMeshBlockingAreas[i])
			i_NavMeshBlockingAreas[i] = -1
		ENDIF
	ENDREPEAT

	PAUSE_CLOCK(FALSE)
	
	FOR i = 0 TO NUM_DOWNLOAD_SHOOTOUT_ENEMIES-1
		s_WavePed[i].bSpawned	= FALSE
		s_WavePed[i].bIsDead	= FALSE
	ENDFOR
	
	#IF IS_DEBUG_BUILD
		
		SWITCH debug_i_crewGunmanNextSkip
			CASE 0	
				crewGoon 		= GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_AGENCY, iHeistID_Goon)	
			BREAK
			CASE 1
				crewGoon		= CM_GUNMAN_B_DARYL
			BREAK
			CASE 2
				crewGoon		= CM_GUNMAN_B_NORM
			BREAK
			CASE 3
				crewGoon		= CM_GUNMAN_M_HUGH
			BREAK
			CASE 4
				crewGoon		= CM_GUNMAN_G_GUSTAV
			BREAK
			CASE 5
				crewGoon		= CM_GUNMAN_G_KARL
			BREAK
			CASE 6
				crewGoon		= CM_GUNMAN_G_CHEF_UNLOCK
			BREAK
			CASE 7
				crewGoon		= CM_GUNMAN_G_PACKIE_UNLOCK
			BREAK
		ENDSWITCH
		
		SWITCH debug_i_crewHackerNextSkip
			CASE 0
				crewHacker 		= GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_AGENCY, iHeistID_Hacker)
			BREAK
			CASE 1
				crewHacker		= CM_HACKER_M_CHRIS
			BREAK
			CASE 2
				crewHacker		= CM_HACKER_G_PAIGE
			BREAK
			CASE 3
				crewHacker		= CM_HACKER_B_RICKIE_UNLOCK
			BREAK

		ENDSWITCH
	
		SWITCH debug_i_crewDriverNextSkip
			CASE 0
				crewDriver 		= GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_AGENCY, iHeistID_Driver)
			BREAK
			CASE 1
				crewDriver		= CM_DRIVER_B_KARIM
			BREAK
			CASE 2
				crewDriver		= CM_DRIVER_G_EDDIE
			BREAK
			CASE 3
				crewDriver		= CM_DRIVER_G_TALINA_UNLOCK
			BREAK
		ENDSWITCH
	
	#ENDIF
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_MAX_WANTED_LEVEL(5)
	SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
	SET_CREATE_RANDOM_COPS(TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, 		TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, 		TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, 		TRUE) 
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, 		TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, 			TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, 		TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, 		TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, 	TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, 	TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, 		TRUE)
	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	
	SETUP_FIB_COVERPOINTS(FALSE)
	
	s_PlayerAttachedSmokeEvo_SmokeStrength.fCurrent		= 0.0
	s_PlayerAttachedSmokeEvo_SmokeStrength.fStart		= 0.0
	s_PlayerAttachedSmokeEvo_SmokeStrength.fDesired		= 0.0
	s_PlayerAttachedSmokeEvo_SmokeStrength.iTimeStarted	= -1
	
	s_PlayerAttachedSmokeEvo_SmokeDensity.fCurrent		= 0.0
	s_PlayerAttachedSmokeEvo_SmokeDensity.fStart		= 0.0
	s_PlayerAttachedSmokeEvo_SmokeDensity.fDesired		= 0.0
	s_PlayerAttachedSmokeEvo_SmokeDensity.iTimeStarted	= -1
	
	s_PlayerAttachedSmokeEvo_CinderDensity.fCurrent		= 0.0
	s_PlayerAttachedSmokeEvo_CinderDensity.fStart		= 0.0
	s_PlayerAttachedSmokeEvo_CinderDensity.fDesired		= 0.0
	s_PlayerAttachedSmokeEvo_CinderDensity.iTimeStarted	= -1
	
	s_PlayerAttachedSmokeEvo_DebrisDensity.fCurrent		= 0.0
	s_PlayerAttachedSmokeEvo_DebrisDensity.fStart		= 0.0
	s_PlayerAttachedSmokeEvo_DebrisDensity.fDesired		= 0.0
	s_PlayerAttachedSmokeEvo_DebrisDensity.iTimeStarted	= -1
	
	s_PlayerAttachedSmokeEvo_WindSpeed.fCurrent			= 0.0
	s_PlayerAttachedSmokeEvo_WindSpeed.fStart			= 0.0
	s_PlayerAttachedSmokeEvo_WindSpeed.fDesired			= 0.0
	s_PlayerAttachedSmokeEvo_WindSpeed.iTimeStarted		= -1
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_playerAttachedSmokeFX)
		STOP_PARTICLE_FX_LOOPED(ptfx_playerAttachedSmokeFX)
	ENDIF
	
	b_HeliRayFireExplosionHappened		= FALSE
	i_Silt_Timer_1						= -1
	i_Silt_Timer_2						= -1
	i_Silt_Timer_3						= -1
	
	IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
		CLEAR_TIMECYCLE_MODIFIER()
	ENDIF
	b_TimeCycleModifierActive = FALSE
	
	RESET_SPRINKLERS()
	
	Cleanup_RenderTargets()
		
	SET_DOOR_STATE(DOORNAME_NOSE_REAR_L, DOORSTATE_LOCKED)
	SET_DOOR_STATE(DOORNAME_NOSE_REAR_R, DOORSTATE_LOCKED)
	DOORS_Set_State(mdf_helipad, TRUE, 0.0)
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbi_franklins)
	SET_ROADS_BACK_TO_ORIGINAL(<<4.020949,550.573975,170.556335>> - <<153.125000,32.000000,43.187500>>, <<4.020949,550.573975,170.556335>> + <<153.125000,32.000000,43.187500>>)
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<291.467987,735.869751,133.466675>>, <<197.176666,723.254272,201.064758>>, 59.812500)
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<234.179382,713.081238,180.183334>>, <<168.204880,547.548340,200.742401>>, 59.812500)
	
	REMOVE_ALL_COVER_BLOCKING_AREAS()
	
	IF IS_SCRIPT_GLOBAL_SHAKING()
		STOP_SCRIPT_GLOBAL_SHAKING(TRUE)
	ENDIF
	
	SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_DESTROYED )
	SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_CLEANUP ) 
	SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_DESTROYED ) 
	SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_NORMAL ) 
	SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_NORMAL )
	
	SET_GAME_PAUSES_FOR_STREAMING(TRUE)
	END_SRL()
	
	IF IS_INTERIOR_ENTITY_SET_ACTIVE(interior_fib03, "V_FIB03_door_light")
		DEACTIVATE_INTERIOR_ENTITY_SET(interior_fib03, "V_FIB03_door_light")
	ENDIF
	
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)

ENDPROC


//PURPOSE: Cleans up all mission assets at upon terminating the script
PROC Mission_Cleanup()
	KILL_ANY_CONVERSATION()
	CLEAR_PRINTS()
	Cleanup_RenderTargets()
	
	CLEAR_MISSION_LOCATE_STUFF(locates_Data)
	
	CLEANUP_PC_RAPPEL_CONTROLS()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
				
		//Parachute stored set back
		IF i_SotredChuteColour_Mike != -1
			// ped already has chute, make sure the colour is right
			IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
				SET_PED_PARACHUTE_TINT_INDEX(PLAYER_PED_ID(), i_SotredChuteColour_Mike)
			
			// doesnt have chute, give to ped and set colour
			ELSE
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1, FALSE, FALSE)
				SET_PED_PARACHUTE_TINT_INDEX(PLAYER_PED_ID(), i_SotredChuteColour_Mike)
			ENDIF
			i_SotredChuteColour_Mike = -1
		// no stored chute, remove chute from player
		ELSE
			REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
			CLEAR_PED_PARACHUTE_PACK_VARIATION(PLAYER_PED_ID())
		ENDIF

	ENDIF
	
	INT i, j
	FOR i = 0 TO enum_to_int(mpf_num_peds)-1
		IF DOES_ENTITY_EXIST(peds[i].id)
		AND PLAYER_PED_ID() != peds[i].id
			IF NOT IS_PED_INJURED(peds[i].id)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[i].id, TRUE)
				SET_PED_KEEP_TASK(peds[i].id, TRUE)
				REMOVE_PED_FROM_GROUP(peds[i].id)
			ENDIF
			CPRINTLN(DEBUG_MIKE, "ATTEMPTING TO CLEAR PED: ", i)
			SET_PED_AS_NO_LONGER_NEEDED(peds[i].id)
		ENDIF
	ENDFOR
	
	FOR i = 0 TO enum_to_int(mvf_num_vehicles)-1
	
		FOR j = 0 TO 3
			IF DOES_ENTITY_EXIST(vehs[i].peds[j].id)
			AND PLAYER_PED_ID() != vehs[i].peds[j].id
				SAFE_REMOVE_BLIP(vehs[i].peds[j].objective_blip)
				IF vehs[i].peds[j].bDoesCovExist
					REMOVE_COVER_POINT(vehs[i].peds[j].cov)
					vehs[i].peds[j].bDoesCovExist = FALSE
				ENDIF
				vehs[i].peds[j].iGenericFlag = 0
				vehs[i].peds[j].iDeadTimer = 0
				SET_PED_AS_NO_LONGER_NEEDED(vehs[i].peds[j].id)
			ENDIF
		ENDFOR	
	
		IF DOES_ENTITY_EXIST(vehs[i].id)
		
			IF IS_VEHICLE_DRIVEABLE(vehs[i].id) 
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[i].id)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehs[i].id)
				ENDIF
			ENDIF

			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[i].id)
		ENDIF

	ENDFOR
	
	REPEAT enum_to_int(mpuf_num_pickups) i
		IF DOES_PICKUP_EXIST(pkups[i])
			REMOVE_PICKUP(pkups[i])
		ENDIF
	ENDREPEAT
	
	FOR i = 0 TO enum_to_int(mcf_num_cams)-1
		IF DOES_CAM_EXIST(cams[i])
			DESTROY_CAM(cams[i])		
		ENDIF
	ENDFOR
	
	DESTROY_ALL_CAMS()
	
	FOR i = 0 TO enum_to_int(mof_num_objects)-1
		IF DOES_ENTITY_EXIST(objs[i].id)
			SET_OBJECT_AS_NO_LONGER_NEEDED(objs[i].id)
		ENDIF
	ENDFOR
	
	REPEAT COUNT_OF(s_Sprinklers) i
		IF DOES_ENTITY_EXIST(s_Sprinklers[i].objSprinkler)
			SET_OBJECT_AS_NO_LONGER_NEEDED(s_Sprinklers[i].objSprinkler)
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(ptfxs) i
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxs[i])
			STOP_PARTICLE_FX_LOOPED(ptfxs[i])
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(sDoors) i
		IF IS_DOOR_REGISTERED_WITH_SYSTEM(sDoors[i].iHash)
		AND NOT sDoors[i].bExistingDoor
			REMOVE_DOOR_FROM_SYSTEM(sDoors[i].iHash)
		ENDIF
	ENDREPEAT
	
//	DELETE_CHECKPOINT(cp_heli_to_marker)
	
	DISABLE_TAXI_HAILING(FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL), TRUE)
	
	SET_WANTED_LEVEL_MULTIPLIER(1.0)
	SET_MAX_WANTED_LEVEL(5)
	SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
	SET_CREATE_RANDOM_COPS(TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, 		TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, 		TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, 		TRUE) 
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, 		TRUE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, 			TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, 		TRUE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, 		TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, 	TRUE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, 	TRUE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, 		TRUE)
	
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(widget_debug)
			DELETE_WIDGET_GROUP(widget_debug)
		ENDIF
	#ENDIF
	
	REPEAT COUNT_OF(sounds) i
		IF sounds[i] != -1
			STOP_SOUND(sounds[i])
			RELEASE_SOUND_ID(sounds[i])
			sounds[i] = -1
		ENDIF
	ENDREPEAT
	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		
	IF IS_CELLPHONE_DISABLED()
		DISABLE_CELLPHONE(FALSE)
	ENDIF
	
	IF bDoorHidden
		REMOVE_MODEL_HIDE(<<150.5931, -762.3801, 258.3018>>, 1.0, mod_OriginalHackDoor, TRUE)
		CDEBUG1LN(DEBUG_MIKE, DEBUG_MIKE, "Removing model hide")
		bDoorHidden = FALSE
	ENDIF
	
	IF bSwappedGlass
		//REMOVE_MODEL_SWAP(<<139.4704, -745.9282, 263.1190>>, 0.1, V_ILEV_FIB_ATRGLSWAP, PROP_FIB_SKYLIGHT_PLUG, TRUE)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_ROOF, BUILDINGSTATE_NORMAL)
		CDEBUG1LN(DEBUG_MIKE, DEBUG_MIKE, "Reseting build state")
		bSwappedGlass = FALSE
	ENDIF

	// replace the collision mesh to stop players getting into the fib building off mission
	//REMOVE_MODEL_HIDE(<< 136.1795, -750.7010, 262.0516 >>, 10.0, V_ILEV_FIB_ATRCOL)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_ROOF, BUILDINGSTATE_NORMAL)
	
	RESET_RAPPEL_ADDITIONAL(s_rappel_mike)
	RESET_RAPPEL_ADDITIONAL(s_rappel_frank)
	RESET_RAPPEL_ADDITIONAL(s_rappel_goon)
	RESET_RAPPEL_ADDITIONAL(s_rappel_mike2)
	RESET_RAPPEL_ADDITIONAL(s_rappel_frank2)
	RESET_RAPPEL_ADDITIONAL(s_rappel_goon2)
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbi_bin_sweatshop)
	REMOVE_SCENARIO_BLOCKING_AREA(sbi_sec_guard_entrance)
	REMOVE_SCENARIO_BLOCKING_AREA(sbi_gov_facility_security_guards[0])
	REMOVE_SCENARIO_BLOCKING_AREA(sbi_gov_facility_security_guards[1])
	REMOVE_SCENARIO_BLOCKING_AREA(sbi_gov_facility_security_guards[2])
	REMOVE_SCENARIO_BLOCKING_AREA(sbi_gov_facility_security_guards[3])
	REMOVE_SCENARIO_BLOCKING_AREA(sbi_franklins)
	
	SET_ROADS_BACK_TO_ORIGINAL(<<4.020949,550.573975,170.556335>> - <<153.125000,32.000000,43.187500>>, <<4.020949,550.573975,170.556335>> + <<153.125000,32.000000,43.187500>>)
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<291.467987,735.869751,133.466675>>, <<197.176666,723.254272,201.064758>>, 59.812500)
	SET_ROADS_BACK_TO_ORIGINAL_IN_ANGLED_AREA(<<234.179382,713.081238,180.183334>>, <<168.204880,547.548340,200.742401>>, 59.812500)
	
	Reset_Cutscene_Ped_Variation_Streaming(sCutscenePedVariationRegister)
	
	PLAY_ALARM_SECURITY(FALSE, FALSE)
	PLAY_ALARM_FIRE(FALSE, FALSE)
	
	STOP_AUDIO_SCENES()
	DISTANT_COP_CAR_SIRENS(FALSE)
	
	REPEAT COUNT_OF(i_NavMeshBlockingAreas) i
		IF i_NavMeshBlockingAreas[i] != -1
			REMOVE_NAVMESH_BLOCKING_OBJECT(i_NavMeshBlockingAreas[i])
			i_NavMeshBlockingAreas[i] = -1
		ENDIF
	ENDREPEAT
	
	SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
	
	PAUSE_CLOCK(FALSE)
	
	SETUP_FIB_COVERPOINTS(FALSE)
	
	RESET_SPRINKLERS()
	
	SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_HOLE_PLUG, BUILDINGSTATE_NORMAL, FALSE, DEFAULT, TRUE)
	
	SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_MICHAEL, 	SAVEHOUSE_FRANKLIN_VH, FALSE)
	SET_DOOR_STATE(DOORNAME_F_HOUSE_VH_F, DOORSTATE_LOCKED)
	
	SET_DOOR_STATE(DOORNAME_NOSE_REAR_L, DOORSTATE_LOCKED)
	SET_DOOR_STATE(DOORNAME_NOSE_REAR_R, DOORSTATE_LOCKED)
	DOORS_Set_State(mdf_helipad, TRUE, 0.0)
	
	SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_DESTROYED, DEFAULT, DEFAULT, TRUE)
	SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_CLEANUP, DEFAULT, DEFAULT, TRUE) 
	SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_DESTROYED, DEFAULT, DEFAULT, TRUE ) 
	SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_NORMAL, DEFAULT, DEFAULT, TRUE ) 
	SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_NORMAL, DEFAULT, DEFAULT, TRUE )
	
	IF IS_SCRIPT_GLOBAL_SHAKING()
		STOP_SCRIPT_GLOBAL_SHAKING(TRUE)
	ENDIF
	
	SET_GAME_PAUSES_FOR_STREAMING(TRUE)
	END_SRL()
	
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
ENDPROC

//PURPOSE: Called when the mission has failed and for what reason
PROC Mission_Failed(mission_fail_flag fail_condition = mff_debug_forced)
	
	TRIGGER_MUSIC_EVENT("AH3B_DEAD")

	// Force failed - player death, arrested, switch to Multiplayer, etc
	IF fail_condition = mff_default
		b_IsDoingFail = FALSE
		Mission_Cleanup()
		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		TERMINATE_THIS_THREAD()
		
	// Normal missin fails
	ELSE
	
		IF NOT b_IsDoingFail
		
			KILL_ANY_CONVERSATION()
	
			TEXT_LABEL_15 strFailMsg, strSubstring
			SWITCH fail_condition
				CASE mff_debug_forced			strFailMsg = "A3B_FF"		BREAK
				
			// Death fails
				CASE mff_mike_dead				
					strFailMsg		= "CMN_MDIED"
				BREAK
				CASE mff_frank_dead				
					strFailMsg		= "CMN_FDIED"
				BREAK
				CASE mff_hacker_dead
					strFailMsg 		= "A3B_DIED"	
					strSubstring 	= GET_CREW_MEMBER_NAME_LABEL(crewHacker)
				BREAK
				CASE mff_goon_dead
					strFailMsg 		= "A3B_DIED"	
					strSubstring 	= GET_CREW_MEMBER_NAME_LABEL(crewGoon)
				BREAK
				CASE mff_driver_dead
					strFailMsg 		= "A3B_DIED"	
					strSubstring 	= GET_CREW_MEMBER_NAME_LABEL(crewDriver)
				BREAK
				CASE mff_pilot_dead
					strFailMsg 		= "A3B_DIED"	
					strSubstring 	= "A3B_PILOT"
				BREAK
				CASE mff_lester_dead
					strFailMsg		= "CMN_LDIED"
				BREAk
				
			// Abandoned fails
				CASE mff_abandoned_mike
					strFailMsg 		= "CMN_MLEFT"
				BREAK
				CASE mff_abandoned_frank
					strFailMsg 		= "CMN_FLEFT"
				BREAK
				CASE mff_abandoned_gunman
					strFailMsg 		= "A3B_ABAND1"
					strSubstring	= GET_CREW_MEMBER_NAME_LABEL(crewGoon)
				BREAK
				CASE mff_abandoned_pilot
					strFailMsg 		= "A3B_ABAND1"
					strSubstring	= "A3B_PILOT"
				BREAK
				CASE mff_abandoned_driver
					strFailMsg 		= "A3B_ABAND1"
					strSubstring	= GET_CREW_MEMBER_NAME_LABEL(crewDriver)
				BREAK
				
			// Parachute fails
				CASE mff_para_deployed_late_mike
					strFailMsg		= "A3B_FLATE"
					strSubstring 	= "A3B_MIKE"
				BREAK				
				CASE mff_para_landing_missed_mike
					strFailMsg		= "A3B_FLAND"
					strSubstring 	= "A3B_MIKE"
				BREAK
				CASE mff_para_landing_missed_frank
					strFailMsg		= "A3B_FLAND"
					strSubstring 	= "A3B_FRANK"
				BREAK
				CASE mff_para_landing_missed_gunman
					strFailMsg 		= "A3B_FLAND"	
					strSubstring 	= GET_CREW_MEMBER_NAME_LABEL(crewGoon)
				BREAK
				
			// everything else
				CASE mff_comp_destroyed			strFailMsg = "A3B_FDCOMP"	BREAK
				CASE mff_hack_phone_destroyed	strFailMsg = "A3B_FDPHONE"	BREAK
				CASE mff_out_of_explosives		strFailMsg = "A3B_FAMMO"	BREAK
				CASE mff_spotted				strFailMsg = "A3B_HSPOT" 	BREAK
				CASE mff_heli_destroyed			strFailMsg = "A3B_HDEST"	BREAK
				CASE mff_heli_stuck				strFailMsg = "A3B_HSTUCK"	BREAK
				CASE mff_heli_unable_to_takeoff	strFailMsg = "A3B_HTAKE"	BREAK
				CASE mff_getaway_van_destroyed	strFailMsg = "A3B_VDEST"	BREAK
				CASE mff_getaway_van_stuck		strFailMsg = "A3B_VSTUCK"	BREAK
				CASE mff_crew_abandoned			strFailMsg = "A3B_ABAND2"	BREAK
				CASE mff_bought_police_to_base	strFailMsg = "A3B_WNTDBASE"	BREAK
				
				// should never get here but use default fail message
				DEFAULT					strFailMsg = "A3B_DF"		BREAK
			ENDSWITCH

			IF IS_STRING_NULL_OR_EMPTY(strSubstring)
				MISSION_FLOW_MISSION_FAILED_WITH_REASON(strFailMsg)
			ELSE
				MISSION_FLOW_MISSION_FAILED_WITH_REASON_CONTAINING_TEXT(strFailMsg, strSubstring)
			ENDIF
			
			b_IsDoingFail = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC Process_Fail()
	IF b_IsDoingFail
	AND (GET_MISSION_FLOW_SAFE_TO_CLEANUP() OR IS_SCREEN_FADED_OUT())
	
	
		IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_fib02
		OR GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_fib03
			MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<758.9724, -980.9376, 24.4333>>, 88.2707)
			SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<744.4968, -985.3074, 23.7293>>, 279.2123)	
		ELSE
			SWITCH int_to_enum(MISSION_STAGE_FLAG, mission_stage)
				CASE msf_2_fly_to_jump_zone				FALLTHRU
				CASE msf_3_sky_dive_and_land
					MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<758.9724, -980.9376, 24.4333>>, 88.2707)
					SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<744.4968, -985.3074, 23.7293>>, 279.2123)	
				BREAK
				CASE msf_4_hack_the_computer			FALLTHRU
				CASE msf_5_download_shootout			FALLTHRU
				CASE msf_6_stairwell					FALLTHRU
				CASE msf_7_burnt_out_floor_1			FALLTHRU
				CASE msf_8_burnt_out_floor_2			FALLTHRU
				CASE msf_9_abseil_down					
					MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<758.9724, -980.9376, 24.4333>>, 88.2707)
					SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<744.4968, -985.3074, 23.7293>>, 279.2123)	
				BREAK
			ENDSWITCH
		ENDIF
		
		
		Mission_Reset_Everything()
		INT i
		REPEAT COUNT_OF(sDoors) i
			IF IS_DOOR_REGISTERED_WITH_SYSTEM(sDoors[i].iHash)
			AND NOT sDoors[i].bExistingDoor
				REMOVE_DOOR_FROM_SYSTEM(sDoors[i].iHash)
			ENDIF
		ENDREPEAT
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC

//PURPOSE: Called when the mission has been successfully completed.
PROC Mission_Passed()	
	CLEAR_PRINTS()
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	 
	SET_HEIST_END_SCREEN_POTENTIAL_TAKE(HEIST_AGENCY, 1000000)
	AWARD_ACHIEVEMENT_FOR_MISSION(ACH08) // The Government Gimps
	Mission_Cleanup()
	Mission_Flow_Mission_Passed()
	
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE: Manages the swap between stages
PROC Mission_Stage_Management()
	SWITCH stageSwitch
		CASE STAGESWITCH_REQUESTED 
			CDEBUG1LN(DEBUG_MIKE, "[StageManagement] mission_stage switch requested from mission_stage:", mission_stage+1, " to mission_stage:", requestedStage+1)
			stageSwitch = STAGESWITCH_EXITING
			mission_substage = STAGE_EXIT
			iCrewAIStage = STAGE_EXIT
		BREAK
		CASE STAGESWITCH_EXITING
			CDEBUG1LN(DEBUG_MIKE, "[StageManagement] Exiting mission_stage: ", mission_stage+1)
			stageSwitch = STAGESWITCH_ENTERING
			mission_substage = STAGE_ENTRY
			iCrewAIStage = STAGE_ENTRY
			mission_stage = requestedStage
		BREAK
		CASE STAGESWITCH_ENTERING
			CDEBUG1LN(DEBUG_MIKE, "[StageManagement] Entered mission_stage: ", mission_stage+1)
			requestedStage = -1
			stageSwitch = STAGESWITCH_IDLE
		BREAK
		CASE STAGESWITCH_IDLE
			IF (GET_GAME_TIMER() - iStageTimer) > 2500
				CDEBUG1LN(DEBUG_MIKE, "[StageManagement] mission_stage: ", mission_stage+1, " mission_substage: ", mission_substage)
				iStageTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE Set mission_stage, will only switch while mission_stage switch management is idle. returns false if not idle
PROC Mission_Set_Stage(mission_stage_flag newStage)
	IF stageSwitch = STAGESWITCH_IDLE	
		requestedStage = enum_to_int(newStage)
		stageSwitch = STAGESWITCH_REQUESTED
	ENDIF
ENDPROC

//PURPOSE: Carries out death checks on the entity arrays
PROC Mission_Checks(BOOL bDeathChecksOnly)

	VECTOR 		vPlayerCoords
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
	ENDIF

	INT i, j
	REPEAT COUNT_OF(vehs) i
		
		// VEHICLES
		IF vehs[i].id != NULL
		AND DOES_ENTITY_EXIST(vehs[i].id)
			IF NOT IS_VEHICLE_DRIVEABLE(vehs[i].id)
			
				IF i = enum_to_int(mvf_heli)
					IF bFailChecksOnHeli
						Mission_Failed(mff_heli_destroyed)
					ENDIF
				ELIF i = enum_to_int(mvf_escape)
					// FAIL: get awayt vehicle undrivable
					IF bFailChecksOnVan
						Mission_Failed(mff_getaway_van_destroyed)
					ENDIF
				ENDIF
				
			ELIF NOT bDeathChecksOnly
				
				SWITCH int_to_enum(MISSION_VEHICLE_FLAGS, i)
					CASE mvf_escape
						IF bFailChecksOnVan
							// Check if the get away vehicle is stuck
							IF (IS_VEHICLE_STUCK_TIMER_UP(vehs[mvf_escape].id, VEH_STUCK_ON_SIDE, 5000) 
							OR IS_VEHICLE_STUCK_TIMER_UP(vehs[mvf_escape].id, VEH_STUCK_ON_ROOF, 5000))
								Mission_Failed(mff_getaway_van_stuck)
							ENDIF
						ENDIF
					BREAK
					CASE mvf_heli
						IF bFailChecksOnHeli
							// Check if the player has knocked the heli off the heli pad in some way
							IF mission_stage = enum_to_int(msf_1_go_to_helipad)
								IF NOT IS_ENTITY_AT_COORD(vehs[mvf_heli].id, <<2510.891846,-342.214752,118.998756>>,<<4.375000,4.250000,2.750000>>)
									Mission_failed(mff_heli_unable_to_takeoff)
								ENDIF
							ENDIF
						
							// Fail if the heli is flipped for a prolonged time
							IF (IS_VEHICLE_STUCK_TIMER_UP(vehs[mvf_heli].id, VEH_STUCK_ON_SIDE, 5000) 
							OR IS_VEHICLE_STUCK_TIMER_UP(vehs[mvf_heli].id, VEH_STUCK_ON_ROOF, 5000))
								Mission_failed(mff_heli_stuck)
							ENDIF
							
							IF mission_stage = enum_to_int(msf_2_fly_to_jump_zone)
								// FAIL: player has left the heli behind with all the crew in it
								IF VDIST2(vPlayerCoords, GET_ENTITY_COORDS(vehs[mvf_heli].id)) >= 22500 // 150^2
									Mission_Failed(mff_crew_abandoned)
								ENDIF
							ENDIF
							
						ENDIF
					BREAK
				ENDSWITCH
				
			ENDIF
		ENDIF

		// PASSENGERS
		REPEAT 4 j
			IF vehs[i].peds[j].id != NULL
			AND DOES_ENTITY_EXIST(vehs[i].peds[j].id)
				IF vehs[i].peds[j].bAIBlipsActive
					IF GET_PED_RELATIONSHIP_GROUP_HASH(vehs[i].peds[j].id) = REL_ENEMY
						UPDATE_AI_PED_BLIP(vehs[i].peds[j].id, vehs[i].peds[j].ai_blip)
					ENDIF
				ENDIF
			
				IF IS_PED_INJURED(vehs[i].peds[j].id)
					IF DOES_BLIP_EXIST(vehs[i].peds[j].objective_blip)
						REMOVE_BLIP(vehs[i].peds[j].objective_blip)
					ENDIF
					
					IF vehs[i].peds[j].bDoesCovExist
						REMOVE_COVER_POINT(vehs[i].peds[j].cov)
					ENDIF
					
					IF NOT IS_PED_RAGDOLL(vehs[i].peds[j].id)
					AND NOT vehs[i].peds[j].bDoNotCleanUp
						SET_PED_AS_NO_LONGER_NEEDED(vehs[i].peds[j].id)
					ENDIF
					vehs[i].peds[j].bHasBeenKilled = TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDREPEAT
	
	FOR i = 0 TO enum_to_int(mpf_num_peds)-1
	
		BOOL bThisPedIsInjured
		BOOL bThisPedIsThePlayer
		bThisPedIsInjured = FALSE
	
		IF peds[i].id != NULL
		AND DOES_ENTITY_EXIST(peds[i].id)
		
			IF IS_PED_INJURED(peds[i].id)
				bThisPedIsInjured = TRUE
			ELSE
				bThisPedIsInjured = FALSE
			ENDIF
			
			IF PLAYER_PED_ID() = peds[i].id
				bThisPedIsThePlayer = TRUE
			ELSE
				bThisPedIsThePlayer = FALSE
			ENDIF
			
			IF bThisPedIsInjured
				// Fail on various members of the crew
				IF i = enum_to_int(mpf_mike)
					Mission_Failed(mff_mike_dead)	
				ELIF i = enum_to_int(mpf_frank)
					Mission_Failed(mff_frank_dead)
				ELIF i = enum_to_int(mpf_lester)
					Mission_Failed(mff_lester_dead)
				ELIF i = enum_to_int(mpf_goon)
					Mission_Failed(mff_goon_dead)
				ELIF i = enum_to_int(mpf_hacker)
					Mission_Failed(mff_hacker_dead)
				ELIF i = enum_to_int(mpf_driver)
					Mission_Failed(mff_driver_dead)
				// the pilot dies intentionally during this mission so we need to be able to disable death fail
				ELIF bFailOnDeadPilot	
				AND i = enum_to_int(mpf_pilot)
					 Mission_Failed(mff_pilot_dead)
				ENDIF
			ENDIF
			
			IF NOT bDeathChecksOnly
			
				// Stats watchers for the player ped
				IF bThisPedIsThePlayer
				AND NOT bThisPedIsInjured
					// - Vehicle damage and max speed
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF NOT bVehStatsGrabbed
							INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), AH3B_CAR_DAMAGE)
							INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), AH3B_MAX_SPEED)
							INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, AH3B_DAMAGE)
							bVehStatsGrabbed = TRUE
						ENDIF
					ELSE
						IF bVehStatsGrabbed
							INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL, AH3B_MAX_SPEED)
							INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, AH3B_CAR_DAMAGE)
							INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(), AH3B_DAMAGE)
							bVehStatsGrabbed = FALSE
						ENDIF
					ENDIF
				ENDIF
			
				// Update AI blip, no death check needed
				IF peds[i].bAIBlipsActive
					IF GET_PED_RELATIONSHIP_GROUP_HASH(peds[i].id) = REL_ENEMY
						UPDATE_AI_PED_BLIP(peds[i].id, peds[i].ai_blip)
					ENDIF
				ENDIF
				
				// Update flashlight status
				IF NOT bThisPedIsInjured
					IF peds[i].iFlashlightStatus = 1
						SET_PED_RESET_FLAG(peds[i].id, PRF_ForceEnableFlashLightForAI, TRUE)
						SET_PED_RESET_FLAG(peds[i].id, PRF_DisableFlashLight, FALSE)
					ELIF peds[i].iFlashlightStatus = 0
						SET_PED_RESET_FLAG(peds[i].id, PRF_ForceEnableFlashLightForAI, FALSE)
						SET_PED_RESET_FLAG(peds[i].id, PRF_DisableFlashLight, TRUE)
					ENDIF
				ENDIF
				
			ENDIF
			
			IF bThisPedIsInjured
			
				// If ped is injured then clean them up when appropriate
				IF bThisPedIsInjured
					IF DOES_BLIP_EXIST(peds[i].objective_blip)
						REMOVE_BLIP(peds[i].objective_blip)
					ENDIF
					IF peds[i].bDoesCovExist
						REMOVE_COVER_POINT(peds[i].cov)
					ENDIF
					IF NOT IS_PED_RAGDOLL(peds[i].id)
					AND NOT peds[i].bDoNotCleanUp
						SET_PED_AS_NO_LONGER_NEEDED(peds[i].id)
					ENDIF
					peds[i].bHasBeenKilled = TRUE
				ENDIF
					
			// all other cases of not dead
			ELIF NOT bDeathChecksOnly
				
				// Abandon fails
				IF NOT IS_CUTSCENE_PLAYING()
				
					IF mission_stage = enum_to_int(msf_1_go_to_helipad)
					OR mission_stage = enum_to_int(msf_2_fly_to_jump_zone)
					OR mission_stage = enum_to_int(msf_10_get_to_getaway_veh)
					OR mission_stage = enum_to_int(msf_11_escape_to_franklins)
					
						// FAIL: player has left michael behind
						IF NOT bThisPedIsThePlayer
												
							IF VDIST2(vPlayerCoords, GET_ENTITY_COORDS(peds[i].id)) >= 40000 //200^2
														
								SWITCH int_to_enum(MISSION_PED_FLAGS, i)
									CASE mpf_mike			Mission_Failed(mff_abandoned_mike)			BREAK
									CASE mpf_frank			Mission_Failed(mff_abandoned_frank)			BREAK
								ENDSWITCH
							
							ENDIF
						ENDIF
					
					ENDIF
					
					IF mission_stage = enum_to_int(msf_2_fly_to_jump_zone)
					OR mission_stage = enum_to_int(msf_10_get_to_getaway_veh)
					OR mission_stage = enum_to_int(msf_11_escape_to_franklins)
					
						IF NOT bThisPedIsThePlayer
							BOOL bDoCheck
						
							SWITCH int_to_enum(MISSION_PED_FLAGS, i)
								CASE mpf_goon
									IF bFailAbandonGunman	
										bDoCheck = TRUE
									ENDIF
								BREAK
								CASE mpf_pilot
									IF bFailAbandonPilot
										bDoCheck = TRUE
									ENDIF
								BREAK
								CASE mpf_driver
									IF bFailAbandonDriver
										bDoCheck = TRUE
									ENDIF
								BREAK
							ENDSWITCH
							
							IF bDoCheck
								// FAIL: player has left michael behind
								IF VDIST2(vPlayerCoords, GET_ENTITY_COORDS(peds[i].id)) >= 40000 //200^2
								
									SWITCH int_to_enum(MISSION_PED_FLAGS, i)
										CASE mpf_goon			Mission_Failed(mff_abandoned_gunman)			BREAK
										CASE mpf_pilot			Mission_Failed(mff_abandoned_pilot)				BREAK
										CASE mpf_driver			Mission_Failed(mff_abandoned_driver)			BREAK
									ENDSWITCH
								
								ENDIF
							ENDIF
							
						ENDIF
						
					ENDIF
					
				ENDIF
			
			// Update blips
				IF mission_stage >= enum_to_int(msf_3_sky_dive_and_land)
				AND mission_stage < enum_to_int(msf_11_escape_to_franklins)
					// not in a vehicle blip the ped
					IF NOT IS_PED_IN_ANY_VEHICLE(peds[i].id)
						IF (peds[i].objective_blip = NULL
						OR NOT DOES_BLIP_EXIST(peds[i].objective_blip))
						AND GET_BLIP_FROM_ENTITY(peds[i].id) = NULL
							IF i = enum_to_int(mpf_mike)	
								IF PLAYER_PED_ID() != peds[mpf_mike].id
									SAFE_BLIP_PED(peds[mpf_mike].objective_blip, peds[mpf_mike].id, FALSE)
								ENDIF
							ELIF i = enum_to_int(mpf_frank)
								IF PLAYER_PED_ID() != peds[mpf_frank].id
									SAFE_BLIP_PED(peds[mpf_frank].objective_blip, peds[mpf_frank].id, FALSE)
								ENDIF
							ELIF i = enum_to_int(mpf_goon)
								SAFE_BLIP_PED(peds[mpf_goon].objective_blip, peds[mpf_goon].id, FALSE)
							ENDIF
						ENDIF
					// in a vehicle remove the blip
					ELIF peds[i].objective_blip != NULL
					AND DOES_BLIP_EXIST(peds[i].objective_blip)
						SAFE_REMOVE_BLIP(peds[i].objective_blip)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	REPEAT COUNT_OF(objs) i
		
		IF objs[i].id != NULL
		AND DOES_ENTITY_EXIST(objs[i].id)
		
			SWITCH INT_TO_ENUM(MISSION_OBJECT_FLAG, i)
				CASE mof_hack_computer
					// FAIL: if computer is damaged before obtaining the docs
					IF bFailOnMonitorDamage
						IF (mission_stage >= enum_to_int(msf_4_hack_the_computer))
						AND (mission_stage < enum_to_int(msf_6_stairwell))
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(objs[mof_hack_computer].id)
								START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_agency3b_elec_box", objs[mof_hack_computer].id, <<0,0,0>>, <<0,0,0>>, 1)
								IF NOT bDownloadComplete
									Wipe_RenderTargets(FALSE)
									Mission_Failed(mff_comp_destroyed)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE mof_hack_phone
					// FAIL: if hack device is damaged at any time
					IF (mission_stage >= enum_to_int(msf_4_hack_the_computer))
					AND (mission_stage < enum_to_int(msf_6_stairwell))
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(objs[mof_hack_phone].id)
							Wipe_RenderTargets(FALSE) // severs connection to the computer so make the screen go blank
							Mission_Failed(mff_hack_phone_destroyed)
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			
		ENDIF

	ENDREPEAT
	
	
// GENERAL FAIL CHECKS
//--------------------------------------------------------------------------
	IF NOT bDeathChecksOnly
	
//		IF mission_stage < enum_to_int(msf_3_sky_dive_and_land)
//			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
//				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2462.995605,-454.884338,88.989372>>, <<2589.017822,-254.453842,147.931625>>, 149.250000)
//					Mission_failed(mff_bought_police_to_base)
//				ENDIF
//			ENDIF
//		ENDIF
	
		IF mission_stage < enum_to_int(msf_3_sky_dive_and_land)
			// Fail if the player is within 100 meters of the FBI building during the the flight there
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<136.243256,-749.383423,265.611542>>, FALSE) < 100.0
				Mission_Failed(mff_spotted)
			ENDIF
		ENDIF
		
		IF mission_stage >= enum_to_int(msf_6_stairwell)
		AND mission_stage <= enum_to_int(msf_8_burnt_out_floor_2)
			IF NOT IS_INTERIOR_SCENE()
			AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != interior_fib02
			AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != interior_fib03
			AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) != interior_fib04
				IF i_CrewAbandonedTimer = -1
					i_CrewAbandonedTimer = GET_GAME_TIMER() + 3000
				ELIF GET_GAME_TIMER() > i_CrewAbandonedTimer
					Mission_Failed(mff_crew_abandoned)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


//PURPOSE: Any debug variables that need updating go inside here.
PROC Mission_Debug()
#IF IS_DEBUG_BUILD
	f_debug_gam_cam_rel_heading		= GET_GAMEPLAY_CAM_RELATIVE_HEADING()
	f_debug_gam_cam_rel_pitch		= GET_GAMEPLAY_CAM_RELATIVE_PITCH()
	f_debug_gam_cam_world_heading	= GET_GAMEPLAY_CAM_WORLD_HEADING()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		f_debug_player_heading			= GET_ENTITY_HEADING(PLAYER_PED_ID())
	ENDIF
	i_debug_room_key				= GET_ROOM_KEY_FOR_GAME_VIEWPORT()
	b_debug_player_control			= IS_PLAYER_CONTROL_ON(PLAYER_ID())

	IF NOT debug_b_PTFXDebugIsRunning
		IF debug_PTFXTester.ptfxTesterEnabled
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<156.4535, -732.8068, 257.1541>>)
			ENDIF
			
			IF DOES_ENTITY_EXIST(BUDDY_PED_ID())
			AND NOT IS_PED_INJURED(BUDDY_PED_ID())
				SET_ENTITY_COORDS(BUDDY_PED_ID(), <<157.0310, -734.7968, 257.6641>>)
			ENDIF
			
			SET_CONTENTS_OF_TEXT_WIDGET(debug_PTFXTester.ptfxDictionary, "script")
			SET_CONTENTS_OF_TEXT_WIDGET(debug_PTFXTester.ptfxName, "scr_agency3b_heli_expl")
			debug_PTFXTester.ptfxPos 					= <<154.68, -730.11, 252.49>>
			debug_PTFXTester.ptfxRot 					= <<-90,0,-28.9>>
			debug_PTFXTester.ptfxScale 					= 1.0
			debug_PTFXTester.playPtfxLooped 			= FALSE
			debug_PTFXTester.playPtfxNonLooped 			= TRUE
			debug_PTFXTester.ptfxIsRelativeToEntity 	= FALSE
			
			debug_b_PTFXDebugIsRunning = TRUE
		ENDIF
	ELSE
		IF NOT debug_PTFXTester.ptfxTesterEnabled
			debug_b_PTFXDebugIsRunning = TRUE
		ELSE
			Maintain_Ptfx_Tester_Widgets(debug_PTFXTester)
		ENDIF
	ENDIF
	
	TEXT_LABEL_63 strDebug = "i_dialogue_stage: "
	strDebug += i_dialogue_stage
	DRAW_DEBUG_TEXT_2D(strDebug, <<0.1, 0.5, 0.0>>, 0, 0, 0, 255)
	
	strDebug = "i_dialogue_substage: "
	strDebug += i_dialogue_substage
	DRAW_DEBUG_TEXT_2D(strDebug, <<0.1, 0.525, 0.0>>, 0, 0, 0, 255)
	
	strDebug = "i_KillsRequiredToFinishShootout: "
	strDebug += i_KillsRequiredToFinishShootout
	DRAW_DEBUG_TEXT_2D(strDebug, <<0.1, 0.55, 0.0>>, 0, 0, 0, 255)
	
	strDebug = "i_KillsFinishShootoutStart: "
	strDebug += i_KillsFinishShootoutStart
	DRAW_DEBUG_TEXT_2D(strDebug, <<0.1, 0.575, 0.0>>, 0, 0, 0, 255)
	
	strDebug = "i_EnemiesAlive: "
	strDebug += i_EnemiesAlive
	DRAW_DEBUG_TEXT_2D(strDebug, <<0.1, 0.6, 0.0>>, 0, 0, 0, 255)
	
	strDebug = "i_prev_lives: "
	strDebug += i_prev_lives
	DRAW_DEBUG_TEXT_2D(strDebug, <<0.1, 0.625, 0.0>>, 0, 0, 0, 255)
	
	strDebug = "sHackingData.i_lives: "
	strDebug += sHackingData.i_lives
	DRAW_DEBUG_TEXT_2D(strDebug, <<0.1, 0.65, 0.0>>, 0, 0, 0, 255)
	
	strDebug = "i_fps_state: "
	strDebug += enum_to_int(i_fps_state)
	DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.6, 0.0>>, 255, 255, 255, 255)
	
	strDebug = "swState: "
	strDebug += enum_to_int(swState)
	DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.625, 0.0>>, 255, 255, 255, 255)
	
#ENDIF
ENDPROC

PROC GET_SKIP_STAGE_COORD_AND_HEADING(int iStage, VECTOR &vCoord, FLOAT &fHeading)

	SWITCH int_to_enum(MISSION_STAGE_FLAG, iStage)
		CASE msf_0_intro_cut					vCoord = <<711.8967, -962.9301, 29.3956>>			fHeading = 173.1497					BREAK
		CASE msf_1_go_to_helipad				vCoord = <<718.0844, -974.8945, 23.9197>>			fHeading = 184.0478					BREAK
		CASE msf_2_fly_to_jump_zone				vCoord = vHeliStartCoord + <<1,1,0>>				fHeading = 0.0						BREAK
		CASE msf_3_sky_dive_and_land			vCoord = vHeliJumpCoord								fHeading = 0.0 						BREAK
		CASE msf_4_hack_the_computer			vCoord = <<137.7823, -747.0770, 257.1519>>			fHeading = 226.0318					BREAK
		CASE msf_5_download_shootout			vCoord = v_ShootoutCoverMike 						fHeading = 67.9292					BREAK
		CASE msf_6_stairwell					vCoord = <<150.8420, -760.4207, 257.1519>>			fHeading = 56.8501					BREAK
		CASE msf_7_burnt_out_floor_1			vCoord = <<121.7277, -725.8613, 253.1521>>			fHeading = 161.4410					BREAK
		CASE msf_8_burnt_out_floor_2			vCoord = <<152.5221, -731.9482, 249.1525>> 			fHeading = 98.8415					BREAK
		CASE msf_9_abseil_down					vCoord = <<166.3548, -762.9729, 245.1203>> 			fHeading = 257.2595					BREAK
		CASE msf_10_get_to_getaway_veh			vCoord = <<144.6043, -720.4959, 46.0770>> 			fHeading = 160.9092					BREAK
		CASE msf_11_escape_to_franklins			vCoord = <<63.5075, -738.0371, 43.2205>> 			fHeading = 335.5202 				BREAK
		CASE msf_12_mission_passed				vCoord = <<5.8674, 531.4399, 174.3445>> 			fHeading = 155.4157					BREAK
	ENDSWITCH

ENDPROC

#IF IS_DEBUG_BUILD

PROC DEBUG_CHECK_FOR_FORCED_PASS_FAIL()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)			
		Mission_Passed()
	ENDIF
	IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
		Mission_Failed(mff_debug_forced)
	ENDIF
ENDPROC

#ENDIF

PROC SKIP_WAIT(INT iDuration)
	WAIT(iDuration)
	#IF IS_DEBUG_BUILD
		DEBUG_CHECK_FOR_FORCED_PASS_FAIL()
		Process_Fail()
	#ENDIF
ENDPROC

//PURPOSE: Checks wthether its ok to let the screen fade in yet
PROC IS_SKIP_OK_TO_FADE_IN_NOW()

	BOOL bRunLoop = TRUE
	WHILE bRunLoop
		BOOL bFinished = TRUE
		
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID()) 
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
				CDEBUG1LN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() Player Ped: Not safe for fade in")
				bFinished = FALSE
			ELSE
				CDEBUG1LN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() Player Ped: Safe for fade in")
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(peds[mpf_frank].id)
		AND NOT IS_PED_INJURED(peds[mpf_frank].id)
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(peds[mpf_frank].id)
				CDEBUG1LN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() Frank Ped: Not safe for fade in")
				bFinished = FALSE
			ELSE
				CDEBUG1LN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() Frank Ped: Safe for fade in")
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(peds[mpf_goon].id) 
		AND NOT IS_PED_INJURED(peds[mpf_goon].id)
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(peds[mpf_goon].id)
				CDEBUG1LN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() Goon Ped: Not safe for fade in")
				bFinished = FALSE
			ELSE
				CDEBUG1LN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() Goon Ped: Safe for fade in")
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(peds[mpf_pilot].id) 
		AND NOT IS_PED_INJURED(peds[mpf_pilot].id)
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(peds[mpf_pilot].id)
				CDEBUG1LN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() Pilot Ped: Not safe for fade in")
				bFinished = FALSE
			ELSE
				CDEBUG1LN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() Pilot Ped: Safe for fade in")
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(peds[mpf_driver].id) 
		AND NOT IS_PED_INJURED(peds[mpf_driver].id)
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(peds[mpf_driver].id)
				CDEBUG1LN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() Driver Ped: Not safe for fade in")
				bFinished = FALSE
			ELSE
				CDEBUG1LN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() Driver Ped: Safe for fade in")
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(peds[mpf_hacker].id) 
		AND NOT IS_PED_INJURED(peds[mpf_hacker].id)
			IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(peds[mpf_hacker].id)
				CDEBUG1LN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() Hacker Ped: Not safe for fade in")
				bFinished = FALSE
			ELSE
				CDEBUG1LN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() Hacker Ped: Safe for fade in")
			ENDIF
		ENDIF
		
		IF bFinished
			bRunLoop = FALSE
			CPRINTLN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() FINISHED")
		ELSE
			SKIP_WAIT(0)
			Mission_Checks(TRUE)
			CPRINTLN(DEBUG_MIKE, "IS_SKIP_OK_TO_FADE_IN_NOW() WAITING")
		ENDIF
	ENDWHILE
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
		Unload_Asset_NewLoadScene(sAssetData)	
	ENDIF
	
ENDPROC

//PURPOSE: Setup and initialise anything needed at the start of the mission.
PROC Mission_Setup()

// Handle replay
//----------------------------------------------------------------------
	IF IS_REPLAY_IN_PROGRESS()	// RETRIES
	OR IS_REPEAT_PLAY_ACTIVE()	// REPLAY VIA START MENU
	
		IF IS_REPLAY_IN_PROGRESS()
			
			iSkipToStage = GET_REPLAY_MID_MISSION_STAGE()
			CPRINTLN(DEBUG_MIKE, "AH3B: GET_REPLAY_MID_MISSION_STAGE() = ", iSkipToStage)
			IF g_bShitskipAccepted
				iSkipToStage += 1
				CPRINTLN(DEBUG_MIKE, "AH3B: g_bShitskipAccepted")
			ENDIF
			
		ELIF IS_REPEAT_PLAY_ACTIVE()
		
			iSkipToStage = 0
		
		ENDIF
		
		CPRINTLN(DEBUG_MIKE, "AH3B: Checkpoint skipping to ", iSkipToStage)
		
		// Translate checkpoint into mission stage
		SWITCH iSkipToStage
			CASE 0			iSkipToStage = enum_to_int(msf_0_intro_cut)					BREAK
			CASE 1			iSkipToStage = enum_to_int(msf_1_go_to_helipad)				BREAK
			CASE 2			iSkipToStage = enum_to_int(msf_2_fly_to_jump_zone)			BREAK
			CASE 3			iSkipToStage = enum_to_int(msf_3_sky_dive_and_land)			BREAK
			CASE 4			iSkipToStage = enum_to_int(msf_4_hack_the_computer)			BREAK
			CASE 5			iSkipToStage = enum_to_int(msf_5_download_shootout)			BREAK
			CASE 6			iSkipToStage = enum_to_int(msf_6_stairwell)					BREAK
			CASE 7			iSkipToStage = enum_to_int(msf_8_burnt_out_floor_2)			BREAK
			CASE 8			iSkipToStage = enum_to_int(msf_9_abseil_down)				BREAK
			CASE 9			iSkipToStage = enum_to_int(msf_10_get_to_getaway_veh)		BREAK
			CASE 10			iSkipToStage = enum_to_int(msf_11_escape_to_franklins)		BREAK
			CASE 11			iSkipToStage = enum_to_int(msf_12_mission_passed)			BREAK
		ENDSWITCH
		
		CPRINTLN(DEBUG_MIKE, "AH3B: stage skipping to ", iSkipToStage)
		
		SET_BUILDING_IPLS( int_to_enum( MISSION_STAGE_FLAG, iSkipToStage ) )
		
		// New replay streaming 
		IF IS_REPLAY_IN_PROGRESS()
			VECTOR vSkipToCoord
			FLOAT fSkipToHeading
			GET_SKIP_STAGE_COORD_AND_HEADING(iSkipToStage, vSkipToCoord, fSkipToHeading)
			START_REPLAY_SETUP(vSkipToCoord, fSkipToHeading)
			CPRINTLN(DEBUG_MIKE, "AH3B: Using replay warp system - Warp Coord ", vSkipToCoord, " Warp Heading ", fSkipToHeading)
		ENDIF
		
		bDoSkip = TRUE
	ELSE
		SET_BUILDING_IPLS( msf_0_intro_cut )
	ENDIF

	g_bDontLetBuddiesReactToNextPlayerDeath = TRUE
	
	SETUP_PC_RAPPEL_CONTROLS()
	
	 // Needed because mouse is read differently than gamepad - Steve R LDS
	INIT_RAPPEL_CAM_MOUSE_PARAMS(s_rappel_mike.sRappelData, 1.0, 0.65, 0.025, 0.0025)
	INIT_RAPPEL_CAM_MOUSE_PARAMS(s_rappel_mike2.sRappelData, 1.0, 0.65, 0.025, 0.0025)
	INIT_RAPPEL_CAM_MOUSE_PARAMS(s_rappel_frank.sRappelData, 1.0, 0.65, 0.025, 0.0025) 
	INIT_RAPPEL_CAM_MOUSE_PARAMS(s_rappel_frank2.sRappelData, 1.0, 0.65, 0.025, 0.0025)
				
	#IF IS_DEBUG_BUILD
			zMenuNames[0].sTxtLabel 	= "[0] 0: CUT: INT"
			zMenuNames[1].sTxtLabel 	= "[1] 1: Go to helipad"
			zMenuNames[2].sTxtLabel 	= "[2] 2: Fly heli to jump zone"
		zMenuNames[3].sTxtLabel 	= "[3] CUT: MCS_1 - SkyDive Prep"
		zMenuNames[3].bSelectable	= FALSE
			zMenuNames[4].sTxtLabel 	= "[4] 3: Skydive on to rooftop"
		zMenuNames[5].sTxtLabel 	= "[5] CUT: MCS_2 - Break-in"
		zMenuNames[5].bSelectable	= FALSE
			zMenuNames[6].sTxtLabel = "[6] 4: Hack the Computer"
		zMenuNames[7].sTxtLabel 	= "[7] CUT: MCS_3 - Begin Hack"
		zMenuNames[7].bSelectable	= FALSE
		zMenuNames[8].sTxtLabel 	= "[8] CUT: MCS_4 - Finish Hack"
		zMenuNames[8].bSelectable	= FALSE
			zMenuNames[9].sTxtLabel = "[9] 5: Downloading Shootout"
		zMenuNames[10].sTxtLabel 	= "[10] CUT: MCS_5 - Collect Data"
		zMenuNames[10].bSelectable	= FALSE
			zMenuNames[11].sTxtLabel = "[11] 6: Stairwell"
			zMenuNames[12].sTxtLabel = "[12] 7: Burntout floor 1"
		zMenuNames[13].sTxtLabel 	= "[13] CUT: MCS_6 - Heli Fall"
		zMenuNames[13].bSelectable	= FALSE
			zMenuNames[14].sTxtLabel = "[14] 8: Burntout floor 2(after heli has fallen)"
		zMenuNames[15].sTxtLabel 	= "[15] CUT: MCS_7 - Setup Rappel"
		zMenuNames[15].bSelectable	= FALSE
			zMenuNames[16].sTxtLabel = "[16] 9: Rappel out of building"
			zMenuNames[17].sTxtLabel = "[17] 10: Get to the getaway vehicle"
			zMenuNames[18].sTxtLabel = "[18] 11: Escape to Franklin's"
		zMenuNames[19].sTxtLabel 	= "[19] CUT: MCS_EXT"
		zMenuNames[19].bSelectable	= FALSE
			zMenuNames[20].sTxtLabel = "[20] 12: Mission Passed"
		
		widget_debug = START_WIDGET_GROUP("The Agency 3B Finale") 
			START_WIDGET_GROUP("Crew") 
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("---No Change---")
					ADD_TO_WIDGET_COMBO(Get_String_From_TextLabel(GET_CREW_MEMBER_NAME_LABEL(CM_GUNMAN_B_DARYL)))
					ADD_TO_WIDGET_COMBO(Get_String_From_TextLabel(GET_CREW_MEMBER_NAME_LABEL(CM_GUNMAN_B_NORM)))
					ADD_TO_WIDGET_COMBO(Get_String_From_TextLabel(GET_CREW_MEMBER_NAME_LABEL(CM_GUNMAN_M_HUGH)))
					ADD_TO_WIDGET_COMBO(Get_String_From_TextLabel(GET_CREW_MEMBER_NAME_LABEL(CM_GUNMAN_G_GUSTAV)))
					ADD_TO_WIDGET_COMBO(Get_String_From_TextLabel(GET_CREW_MEMBER_NAME_LABEL(CM_GUNMAN_G_KARL)))
					ADD_TO_WIDGET_COMBO(Get_String_From_TextLabel(GET_CREW_MEMBER_NAME_LABEL(CM_GUNMAN_G_CHEF_UNLOCK)))
					ADD_TO_WIDGET_COMBO(Get_String_From_TextLabel(GET_CREW_MEMBER_NAME_LABEL(CM_GUNMAN_G_PACKIE_UNLOCK)))
				STOP_WIDGET_COMBO("Gunman", debug_i_crewGunmanNextSkip)
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("---No Change---")
					ADD_TO_WIDGET_COMBO(Get_String_From_TextLabel(GET_CREW_MEMBER_NAME_LABEL(CM_HACKER_M_CHRIS)))
					ADD_TO_WIDGET_COMBO(Get_String_From_TextLabel(GET_CREW_MEMBER_NAME_LABEL(CM_HACKER_G_PAIGE)))
					ADD_TO_WIDGET_COMBO(Get_String_From_TextLabel(GET_CREW_MEMBER_NAME_LABEL(CM_HACKER_B_RICKIE_UNLOCK)))
				STOP_WIDGET_COMBO("Hacker", debug_i_crewHackerNextSkip)
				START_NEW_WIDGET_COMBO()
					ADD_TO_WIDGET_COMBO("---No Change---")
					ADD_TO_WIDGET_COMBO(Get_String_From_TextLabel(GET_CREW_MEMBER_NAME_LABEL(CM_DRIVER_B_KARIM)))
					ADD_TO_WIDGET_COMBO(Get_String_From_TextLabel(GET_CREW_MEMBER_NAME_LABEL(CM_DRIVER_G_EDDIE)))
					ADD_TO_WIDGET_COMBO(Get_String_From_TextLabel(GET_CREW_MEMBER_NAME_LABEL(CM_DRIVER_G_TALINA_UNLOCK)))
				STOP_WIDGET_COMBO("Driver", debug_i_crewDriverNextSkip)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Debug Info")
				ADD_WIDGET_BOOL("Is Player Control On", 				b_debug_player_control)
				ADD_WIDGET_FLOAT_READ_ONLY("GameCamRelHeading", 		f_debug_gam_cam_rel_heading)
				ADD_WIDGET_FLOAT_READ_ONLY("GameCamWorldHeading", 		f_debug_gam_cam_world_heading)
				ADD_WIDGET_FLOAT_READ_ONLY("GameCamRelPitch", 			f_debug_gam_cam_rel_pitch)
				ADD_WIDGET_FLOAT_READ_ONLY("PlayerHeading", 			f_debug_player_heading)
				ADD_WIDGET_INT_READ_ONLY("Interior Room Key", 			i_debug_room_key)
				ADD_WIDGET_FLOAT_READ_ONLY("Recording Progress 1", 		f_debug_rec_pos1)
				ADD_WIDGET_FLOAT_READ_ONLY("Recording Progress 2", 		f_debug_rec_pos2)
				ADD_WIDGET_INT_READ_ONLY("AimState", 					i_debug_aim_cam)
				ADD_WIDGET_INT_READ_ONLY("Escape area timer", 			i_EscapeAreaTimer)
				ADD_WIDGET_INT_READ_ONLY("Killed cops count", 			iKilledCopsCount)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Download Shootout")
				ADD_WIDGET_INT_READ_ONLY("Shootout enemies spawned", 	i_debug_EnemiesSpawned)
				ADD_WIDGET_INT_READ_ONLY("Shootout enemies alive", 		i_EnemiesAlive)
				ADD_WIDGET_INT_READ_ONLY("Shootout enemies manually counted", 		debug_i_EnemiesManuallyCounted)
				ADD_WIDGET_INT_READ_ONLY("Killed since last spawned", 	i_KillsSinceLastSpawned)
				ADD_WIDGET_FLOAT_READ_ONLY("Download Progress", 		fDownloadProgress)
				ADD_WIDGET_FLOAT_READ_ONLY("Download Rate", 			f_debug_CurrentRate)
				ADD_WIDGET_FLOAT_READ_ONLY("Download Rate Multiplier", 	fDownloadSpeedMultiplier)
				
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Michael's cover cam limiter")
				START_WIDGET_GROUP("Cover")
					ADD_WIDGET_FLOAT_SLIDER("Heading Min", f_cam_limits_heading_min, -360.0, 360, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Heading Max", f_cam_limits_heading_max, -360.0, 360, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Pitch Min", f_cam_limits_pitch_min, -360.0, 360, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Pitch Max", f_cam_limits_pitch_max, -360.0, 360, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Orbit Min", f_cam_limits_orbit_min, 0.0, 10.0, 0.05)
					ADD_WIDGET_FLOAT_SLIDER("Orbit Max", f_cam_limits_orbit_max, 0.0, 10.0, 0.05)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("Aiming")
					ADD_WIDGET_FLOAT_SLIDER("Heading Min", f_cam_limits_aim_heading_min, -360.0, 360, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Heading Max", f_cam_limits_aim_heading_max, -360.0, 360, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Pitch Min", f_cam_limits_aim_pitch_min, -360.0, 360, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Pitch Max", f_cam_limits_aim_pitch_max, -360.0, 360, 0.1)
					ADD_WIDGET_FLOAT_SLIDER("Orbit Min", f_cam_limits_aim_orbit_min, 0.0, 10.0, 0.05)
					ADD_WIDGET_FLOAT_SLIDER("Orbit Max", f_cam_limits_aim_orbit_max, 0.0, 10.0, 0.05)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Rappel Widgets")
				START_WIDGET_GROUP("Rappel 1st")
					START_WIDGET_GROUP("Mike")
						ADD_WIDGET_VECTOR_SLIDER("Mike Anchor Pos", v_debug_offset_m, -99999, 99999, 0.05)
						ADD_WIDGET_FLOAT_SLIDER("Mike Anchor Rot", f_debug_heading_m, -360, 360, 0.5)
						ADD_WIDGET_FLOAT_READ_ONLY("Mike Attach Z", s_rappel_mike.sRappelData.vAttachPos.z)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Frank")
						ADD_WIDGET_VECTOR_SLIDER("Frank Anchor Pos", v_debug_offset_f, -99999, 99999, 0.05)
						ADD_WIDGET_FLOAT_SLIDER("Frank Anchor Rot", f_debug_heading_f, -360, 360, 0.5)
						ADD_WIDGET_FLOAT_READ_ONLY("Frank Attach Z", s_rappel_frank.sRappelData.vAttachPos.z)
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Gunman")
						ADD_WIDGET_VECTOR_SLIDER("Gunman Anchor Pos", v_debug_offset_g, -99999, 99999, 0.05)
						ADD_WIDGET_FLOAT_SLIDER("Gunman Anchor Rot", f_debug_heading_g, -360, 360, 0.5)
						ADD_WIDGET_FLOAT_READ_ONLY("Goon Attach Z", s_rappel_goon.sRappelData.vAttachPos.z)
					STOP_WIDGET_GROUP()
					ADD_WIDGET_FLOAT_SLIDER("fStartRappelInterp", fStartRappelInterp, -200, 0, 0.1)
					START_WIDGET_GROUP("Cam Start")
						ADD_WIDGET_FLOAT_SLIDER("fCamDefaultHeading", 			fDefaultHeading_START,			-360, 360, 0.1)
						ADD_WIDGET_FLOAT_SLIDER("fCamYDist", 					fYDist_START, 					-9999, 9999, 0.02)
						ADD_WIDGET_FLOAT_SLIDER("fCamXYInputToDegreesFacror", 	fXYInputToDegrees_START, 		-360, 360, 0.1)
						ADD_WIDGET_FLOAT_SLIDER("fCamClockwiseLimit", 			fClockwiseLimit_START, 			-360, 360, 0.1)
						ADD_WIDGET_FLOAT_SLIDER("fCamAnticlockwiseLimit", 		fAntiClockwiseLimit_START, 		-360, 360, 0.1)
						ADD_WIDGET_FLOAT_SLIDER("fCamXYSpeed", 					fXYSpeed_START, 				0, 9999, 0.02)
						ADD_WIDGET_FLOAT_SLIDER("fCamZDist", 					fZDist_START, 					-9999, 9999, 0.02)
						ADD_WIDGET_FLOAT_SLIDER("fCamZInputToHeightFactor", 	fZInputToHeightFactor_START,	0, 9999, 0.02)
						ADD_WIDGET_FLOAT_SLIDER("fCamZUpperLimit", 				fZUpperLimit_START,				-9999, 9999, 0.02)
						ADD_WIDGET_FLOAT_SLIDER("fCamZLowerLimit", 				fZLowerLimit_START,				-9999, 9999, 0.02)
						ADD_WIDGET_FLOAT_SLIDER("fCamZSpeed", 					fZSpeed_START,					0, 9999, 0.02)
						ADD_WIDGET_FLOAT_SLIDER("fCamFOV", 						fZFOV_START,					0.0, 180.0, 0.02)
						ADD_WIDGET_VECTOR_SLIDER("vLookAtOffset", 				vLookAtOffset_START,			-9999.9, 9999.9, 0.1)
					STOP_WIDGET_GROUP()
					
					START_WIDGET_GROUP("Cam End")
						ADD_WIDGET_FLOAT_SLIDER("fCamDefaultHeading", 			fDefaultHeading_END,			-360, 360, 0.1)
						ADD_WIDGET_FLOAT_SLIDER("fCamYDist", 					fYDist_END, 					-9999, 9999, 0.02)
						ADD_WIDGET_FLOAT_SLIDER("fCamXYInputToDegreesFacror", 	fXYInputToDegrees_END, 			-360, 360, 0.1)
						ADD_WIDGET_FLOAT_SLIDER("fCamClockwiseLimit", 			fClockwiseLimit_END, 			-360, 360, 0.1)
						ADD_WIDGET_FLOAT_SLIDER("fCamAnticlockwiseLimit", 		fAntiClockwiseLimit_END, 		-360, 360, 0.1)
						ADD_WIDGET_FLOAT_SLIDER("fCamXYSpeed", 					fXYSpeed_END, 					0, 9999, 0.02)
						ADD_WIDGET_FLOAT_SLIDER("fCamZDist", 					fZDist_END, 					-9999, 9999, 0.02)
						ADD_WIDGET_FLOAT_SLIDER("fCamZInputToHeightFactor", 	fZInputToHeightFactor_END,		0, 9999, 0.02)
						ADD_WIDGET_FLOAT_SLIDER("fCamZUpperLimit", 				fZUpperLimit_END,				-9999, 9999, 0.02)
						ADD_WIDGET_FLOAT_SLIDER("fCamZLowerLimit", 				fZLowerLimit_END,				-9999, 9999, 0.02)
						ADD_WIDGET_FLOAT_SLIDER("fCamZSpeed", 					fZSpeed_END,					0, 9999, 0.02)
						ADD_WIDGET_FLOAT_SLIDER("fCamFOV", 						fZFOV_END,						0.0, 180.0, 0.02)
						ADD_WIDGET_VECTOR_SLIDER("vLookAtOffset", 				vLookAtOffset_END,				-9999.9, 9999.9, 0.1)
					STOP_WIDGET_GROUP()
					
				STOP_WIDGET_GROUP()
				
				START_WIDGET_GROUP("Rappel 2nd")
					START_WIDGET_GROUP("Mike")
						ADD_WIDGET_VECTOR_SLIDER("Mike Anchor Pos", v_debug_offset_m2, -99999, 99999, 0.05)
						ADD_WIDGET_FLOAT_SLIDER("Mike Anchor Rot", f_debug_heading_m2, -360, 360, 0.5)
						ADD_WIDGET_FLOAT_READ_ONLY("Mike Attach Z", s_rappel_mike2.sRappelData.vAttachPos.z)
						START_WIDGET_GROUP("Cam")
							ADD_WIDGET_FLOAT_SLIDER("fCamDefaultHeading", 			s_rappel_mike2.sRappelData.fCamDefaultHeading,			-360, 360, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("fCamYDist", 					s_rappel_mike2.sRappelData.fCamYDist, 					-9999, 9999, 0.02)
							ADD_WIDGET_FLOAT_SLIDER("fCamXYInputToDegreesFacror", 	s_rappel_mike2.sRappelData.fCamXYInputToDegreesFactor, 	-360, 360, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("fCamClockwiseLimit", 			s_rappel_mike2.sRappelData.fCamClockwiseLimit, 			-360, 360, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("fCamAnticlockwiseLimit", 		s_rappel_mike2.sRappelData.fCamAnticlockwiseLimit, 		-360, 360, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("fCamXYSpeed", 					s_rappel_mike2.sRappelData.fCamXYSpeed, 					0, 9999, 0.02)
							ADD_WIDGET_FLOAT_SLIDER("fCamZDist", 					s_rappel_mike2.sRappelData.fCamZDist, 					-9999, 9999, 0.02)
							ADD_WIDGET_FLOAT_SLIDER("fCamZInputToHeightFactor", 	s_rappel_mike2.sRappelData.fCamZInputToHeightFactor,		0, 9999, 0.02)
							ADD_WIDGET_FLOAT_SLIDER("fCamZUpperLimit", 				s_rappel_mike2.sRappelData.fCamZUpperLimit,				-9999, 9999, 0.02)
							ADD_WIDGET_FLOAT_SLIDER("fCamZLowerLimit", 				s_rappel_mike2.sRappelData.fCamZLowerLimit,				-9999, 9999, 0.02)
							ADD_WIDGET_FLOAT_SLIDER("fCamZSpeed", 					s_rappel_mike2.sRappelData.fCamZSpeed,					0, 9999, 0.02)
							ADD_WIDGET_FLOAT_SLIDER("fCamFOV", 						s_rappel_mike2.sRappelData.fCamFOV,						0.0, 180.0, 0.02)
							ADD_WIDGET_VECTOR_SLIDER("vLookAtOffset", 				s_rappel_mike2.sRappelData.vModifyCamPointAt,			-9999.9, 9999.9, 0.1)
						STOP_WIDGET_GROUP()
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Frank")
						ADD_WIDGET_VECTOR_SLIDER("Frank Anchor Pos", v_debug_offset_f2, -99999, 99999, 0.05)
						ADD_WIDGET_FLOAT_SLIDER("Frank Anchor Rot", f_debug_heading_f2, -360, 360, 0.5)
						ADD_WIDGET_FLOAT_READ_ONLY("Frank Attach Z", s_rappel_frank2.sRappelData.vAttachPos.z)
						START_WIDGET_GROUP("Rappel Cam Frank 2")
							ADD_WIDGET_FLOAT_SLIDER("fCamDefaultHeading", 			s_rappel_frank2.sRappelData.fCamDefaultHeading,			-360, 360, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("fCamYDist", 					s_rappel_frank2.sRappelData.fCamYDist, 					-9999, 9999, 0.02)
							ADD_WIDGET_FLOAT_SLIDER("fCamXYInputToDegreesFacror", 	s_rappel_frank2.sRappelData.fCamXYInputToDegreesFactor, 	-360, 360, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("fCamClockwiseLimit", 			s_rappel_frank2.sRappelData.fCamClockwiseLimit, 			-360, 360, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("fCamAnticlockwiseLimit", 		s_rappel_frank2.sRappelData.fCamAnticlockwiseLimit, 		-360, 360, 0.1)
							ADD_WIDGET_FLOAT_SLIDER("fCamXYSpeed", 					s_rappel_frank2.sRappelData.fCamXYSpeed, 					0, 9999, 0.02)
							ADD_WIDGET_FLOAT_SLIDER("fCamZDist", 					s_rappel_frank2.sRappelData.fCamZDist, 					-9999, 9999, 0.02)
							ADD_WIDGET_FLOAT_SLIDER("fCamZInputToHeightFactor", 	s_rappel_frank2.sRappelData.fCamZInputToHeightFactor,		0, 9999, 0.02)
							ADD_WIDGET_FLOAT_SLIDER("fCamZUpperLimit", 				s_rappel_frank2.sRappelData.fCamZUpperLimit,				-9999, 9999, 0.02)
							ADD_WIDGET_FLOAT_SLIDER("fCamZLowerLimit", 				s_rappel_frank2.sRappelData.fCamZLowerLimit,				-9999, 9999, 0.02)
							ADD_WIDGET_FLOAT_SLIDER("fCamZSpeed", 					s_rappel_frank2.sRappelData.fCamZSpeed,					0, 9999, 0.02)
							ADD_WIDGET_FLOAT_SLIDER("fCamFOV", 						s_rappel_frank2.sRappelData.fCamFOV,						0.0, 180.0, 0.02)
							ADD_WIDGET_VECTOR_SLIDER("vLookAtOffset", 				s_rappel_frank2.sRappelData.vModifyCamPointAt,			-9999.9, 9999.9, 0.1)
						STOP_WIDGET_GROUP()
					STOP_WIDGET_GROUP()
					START_WIDGET_GROUP("Gunman")
						ADD_WIDGET_VECTOR_SLIDER("Gunman Anchor Pos", v_debug_offset_g2, -99999, 99999, 0.05)
						ADD_WIDGET_FLOAT_SLIDER("Gunman Anchor Rot", f_debug_heading_g2, -360, 360, 0.5)
						ADD_WIDGET_FLOAT_READ_ONLY("Goon Attach Z", s_rappel_goon2.sRappelData.vAttachPos.z)
					STOP_WIDGET_GROUP()
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Crew AI")
				ADD_WIDGET_BOOL("Buddy Ready?", bBuddyReadyToMoveOn)
				ADD_WIDGET_BOOL("Goon Ready?", bGunmanReadyToMoveOn)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Scaleform render size")
				ADD_WIDGET_FLOAT_SLIDER("PosX", f_Hacking_Render_Target_x, -1.0, 1.0, 0.05)
				ADD_WIDGET_FLOAT_SLIDER("PosY", f_Hacking_Render_Target_y, -1.0, 1.0, 0.05)
				ADD_WIDGET_FLOAT_SLIDER("Width", f_Hacking_Render_Target_width, -1.0, 1.0, 0.05)
				ADD_WIDGET_FLOAT_SLIDER("Height", f_Hacking_Render_Target_height, -1.0, 1.0, 0.05)
			STOP_WIDGET_GROUP()
			ADD_WIDGET_BOOL("bDoorHidden", bDoorHidden)
			Create_Ptfx_Tester_Widgets(debug_PTFXTester)
		STOP_WIDGET_GROUP()
		SET_LOCATES_HEADER_WIDGET_GROUP(widget_debug)
			
	#ENDIF
 
	REQUEST_ADDITIONAL_TEXT("AH3B", MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("HACK", MINIGAME_TEXT_SLOT)
	
//	WHILE (NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT))
//	OR (NOT HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT))
//		WAIT(0)
//	ENDWHILE
	
	INT i
	REPEAT COUNT_OF(sounds) i
		sounds[i] = GET_SOUND_ID()
	ENDREPEAT
	
	DISABLE_TAXI_HAILING(TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL), TRUE)
	
	// Crew stuff
	crewHacker = GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_AGENCY, iHeistID_Hacker)
	
	crewGoon = GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_AGENCY, iHeistID_Goon)	
	
	crewDriver = GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_AGENCY, iHeistID_Driver)
	
	// Setup relationship groups
	ADD_RELATIONSHIP_GROUP("Crew", 			REL_CREW)
	ADD_RELATIONSHIP_GROUP("Enemy", 		REL_ENEMY)
	ADD_RELATIONSHIP_GROUP("911", 			REL_911)
	ADD_RELATIONSHIP_GROUP("CREW_IGNORE",	REL_CREW_IGNORE)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, 	REL_ENEMY, REL_CREW)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, 	REL_ENEMY, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, 	REL_ENEMY, REL_911)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, 	REL_ENEMY, REL_CREW_IGNORE)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, 	REL_CREW, REL_ENEMY)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, 	REL_CREW, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, 	REL_CREW, REL_911)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, 	REL_CREW, REL_CREW_IGNORE)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, 	REL_CREW_IGNORE, REL_ENEMY)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, 	REL_CREW_IGNORE, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, 	REL_CREW_IGNORE, REL_911)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, 	REL_CREW_IGNORE, REL_CREW)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, 	RELGROUPHASH_PLAYER, REL_ENEMY)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, 	RELGROUPHASH_PLAYER, REL_CREW)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, 	RELGROUPHASH_PLAYER, REL_911)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, 	RELGROUPHASH_PLAYER, REL_CREW_IGNORE)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,	REL_911,	REL_ENEMY)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,	REL_911,	REL_CREW)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,	REL_911,	RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, 	REL_911, 	REL_CREW_IGNORE)
	
	interior_sweatshop	= 	GET_INTERIOR_AT_COORDS_WITH_TYPE(<<717.1278, -964.4892, 29.3957>>, 		"v_sweatempty")
	interior_fib03 		= 	GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 130.0661, -746.8650, 253.1522 >>, 	"v_fib03")
	interior_fib02 		= 	GET_INTERIOR_AT_COORDS_WITH_TYPE(<< 136.0289, -732.2351, 249.1521 >>, 	"v_fib02")
	interior_fib04		= 	GET_INTERIOR_AT_COORDS_WITH_TYPE(<<129.6901, -734.8077, 253.3510>>, 	"v_fib04")
	interior_franklins	= 	GET_INTERIOR_AT_COORDS_WITH_TYPE(<<0.4695, 524.2726, 173.6277>>, 		"v_franklinshouse")
	IF NOT IS_VALID_INTERIOR(interior_sweatshop)
	AND NOT IS_VALID_INTERIOR(interior_fib03)
	AND NOT IS_VALID_INTERIOR(interior_fib02)
	AND NOT IS_VALID_INTERIOR(interior_fib04)
	AND NOT IS_VALID_INTERIOR(interior_franklins)
		SCRIPT_ASSERT("INTERIOR INVALID")
	ENDIF
	
	IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interior_fib02, "V_FIB02_set_AH3b")
		ACTIVATE_INTERIOR_ENTITY_SET(interior_fib02, "V_FIB02_set_AH3b")
	ENDIF
	
	IF IS_INTERIOR_ENTITY_SET_ACTIVE(interior_fib02, "V_FIB02_set_AH3a")
		DEACTIVATE_INTERIOR_ENTITY_SET(interior_fib02, "V_FIB02_set_AH3a")
	ENDIF
	
	IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interior_fib03, "V_FIB03_set_AH3B")
		ACTIVATE_INTERIOR_ENTITY_SET(interior_fib03, "V_FIB03_set_AH3B")
	ENDIF
	
	IF IS_INTERIOR_ENTITY_SET_ACTIVE(interior_fib03, "V_FIB03_set_AH3a")
		DEACTIVATE_INTERIOR_ENTITY_SET(interior_fib03, "V_FIB03_set_AH3a")
	ENDIF
	
	IF IS_INTERIOR_ENTITY_SET_ACTIVE(interior_fib04, "V_FIB04_set_AH3a")
		DEACTIVATE_INTERIOR_ENTITY_SET(interior_fib04, "V_FIB04_set_AH3a")
	ENDIF
	
	REFRESH_INTERIOR(interior_fib02)
	REFRESH_INTERIOR(interior_fib03)
	REFRESH_INTERIOR(interior_fib04)
	
	// Initialise all the doors we're gonna be using
	DOORS_Initialise()
	DOORS_Reset_All()
	
	// Download shootout spawn positions
	SETUP_SPAWN_POINT(s_SpawnPoints[spf_main_entrance], 		<<118.8291, -732.9988, 257.1522>>, 163.0110, FALSE)
	SETUP_SPAWN_POINT(s_SpawnPoints[spf_main_fake_left], 		<<141.6208, -734.0158, 257.1519>>, 128.4372, FALSE)
	SETUP_SPAWN_POINT(s_SpawnPoints[spf_main_fake_mid], 		<<114.2868, -743.7798, 257.1523>>, 342.0726, FALSE)
	SETUP_SPAWN_POINT(s_SpawnPoints[spf_main_fake_right], 		<<123.4929, -760.5908, 257.1519>>, 34.6909, FALSE)
	SETUP_SPAWN_POINT(s_SpawnPoints[spf_roof_front_left], 		<<148.3051, -735.3570, 261.8402>>, 145.8903, TRUE)
	SETUP_SPAWN_POINT(s_SpawnPoints[spf_roof_front_right], 		<<130.7267, -766.4357, 261.8663>>, 341.4502, TRUE)
	SETUP_SPAWN_POINT(s_SpawnPoints[spf_roof_rear_left], 		<<122.5439, -733.2710, 261.8524>>, 222.6655, TRUE)
	SETUP_SPAWN_POINT(s_SpawnPoints[spf_roof_rear_right], 		<<113.8266, -753.9981, 261.8492>>, 275.2845, TRUE)
	
	// Download shootout cover positions
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_post], 						<<117.0829, -742.7855, 257.1523>>, 		1.0,		FALSE)
	
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_chairs_left_rear_l], 		<<126.0562, -741.8318, 257.1520>>, 		1.0,		FALSE)
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_chairs_left_rear_r],		<<125.3624, -743.7374, 257.1520>>,		1.0,		FALSE)
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_chairs_left_front_l], 		<<128.5511, -742.7452, 257.1519>>,		1.0,		FALSE)
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_chairs_left_front_r], 		<<127.8216, -744.7714, 257.1519>>,		1.0,		FALSE)
	
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_chairs_right_rear_l], 		<<124.2615, -746.7748, 257.1519>>,		1.0,		FALSE)
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_chairs_right_rear_r], 		<<123.6546, -748.7203, 257.1519>>,		1.0,		FALSE)
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_chairs_right_front_l], 		<<126.7811, -747.5624, 257.1519>>,		1.0,		FALSE)
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_chairs_right_front_r], 		<<126.0501, -749.6313, 257.1519>>,		1.0,		FALSE)
	
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_planter_left_front], 		<<139.1954, -747.4327, 257.1697>>,		2.0,		FALSE)
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_planter_left_rear], 		<<131.2523, -744.4833, 257.1697>>,		2.0,		FALSE)
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_planter_right_front], 		<<137.1780, -752.9827, 257.1695>>,		2.0,		FALSE)
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_planter_right_rear], 		<<129.2593, -749.9651, 257.1697>>,		2.0,		FALSE)
	
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_glass_room_right_front],	<<136.3391, -761.1171, 257.1519>>,		1.0,		FALSE)
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_glass_room_right_rear],		<<124.8576, -753.7386, 257.1519>>,		1.0,		FALSE)
	
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_mini_planter_front_left],	<<145.3237, -746.3768, 257.1519>>,		1.0,		FALSE)
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_main_mini_planter_front_right],	<<141.4005, -757.4299, 257.1519>>,		1.0,		FALSE)
	
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_roof_rear_l], 					<<128.9251, -743.8738, 261.8518>>,		1.0,		TRUE)
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_roof_rear_mid_l], 				<<127.8759, -745.3080, 261.8515>>,		1.0,		TRUE)
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_roof_rear_mid_r], 				<<127.2251, -747.4048, 261.8544>>,		1.0,		TRUE)
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_roof_rear_r], 					<<126.7614, -748.9018, 261.8517>>,		1.0,		TRUE)
	
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_roof_rightside_rear], 			<<128.4935, -754.3387, 261.8497>>,		1.0,		TRUE)
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_roof_rightside_front], 			<<132.4571, -755.5112, 261.8494>>,		1.0,		TRUE)
	
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_roof_leftside_rear], 			<<132.7355, -740.4968, 261.8568>>,		1.0,		TRUE)
	SETUP_SHOOTOUT_DEFENSIVE_AREA(s_ShootOutDefensiveAreas[dsd_roof_leftside_front], 			<<134.9779, -741.5173, 261.8590>>,		1.0,		TRUE)
	
	// Set up download shootout peds
	// NOTE TO SELF: have doubled all accurracies and made all shotguns not higher than 2
// 1
	SETUP_WAVE_PED(s_WavePed[0], 	spf_main_entrance,			dsd_main_planter_left_front,
		mod_fib_agent_1, 			WEAPONTYPE_SMG,	 			WEAPONCOMPONENT_INVALID,			 4,		200,
		0, 0, 0.0, FALSE,
		TRUE, TRUE, FALSE)
// 3
	SETUP_WAVE_PED(s_WavePed[1], 	spf_main_fake_mid,			dsd_main_chairs_right_rear_l,
		mod_fib_agent_3, 			WEAPONTYPE_PISTOL,	 		WEAPONCOMPONENT_INVALID,			10,		200,
		0, 1000, 0.0, FALSE,
		TRUE, TRUE, FALSE)
// 2	
	SETUP_WAVE_PED(s_WavePed[2], 	spf_main_entrance,			dsd_main_planter_left_rear,
		mod_fib_agent_2, 			WEAPONTYPE_PISTOL,	 		WEAPONCOMPONENT_INVALID,			10,		200,
		0, 1000, 0.0, FALSE,
		TRUE, TRUE, FALSE)	
// 4		
	SETUP_WAVE_PED(s_WavePed[3], 	spf_main_fake_right,		dsd_main_mini_planter_front_right,
		mod_fib_agent_1, 			WEAPONTYPE_SMG,	 			WEAPONCOMPONENT_INVALID,			4,		200,
		1, 0, 0.10, FALSE,
		TRUE, TRUE, FALSE)
// 5		
	SETUP_WAVE_PED(s_WavePed[4], 	spf_main_fake_mid,			dsd_main_planter_right_front,
		mod_fib_agent_2, 			WEAPONTYPE_PUMPSHOTGUN,		WEAPONCOMPONENT_INVALID,			2,		200,
		0, 1000, 0.0, FALSE,
		TRUE, TRUE, FALSE)
// 6		
	SETUP_WAVE_PED(s_WavePed[5], 	spf_roof_rear_right,		dsd_roof_rear_r,
		mod_swat, 					WEAPONTYPE_SMG,	 			WEAPONCOMPONENT_INVALID,			10,		130,
		1, 0, 0.15, FALSE,
		FALSE, FALSE, FALSE)
// 7		
	SETUP_WAVE_PED(s_WavePed[6], 	spf_roof_rear_left,			dsd_roof_rear_l,
		mod_swat, 					WEAPONTYPE_CARBINERIFLE,	 WEAPONCOMPONENT_INVALID,			10,		130,
		0, 1000, 0.0, FALSE,
		FALSE, FALSE, FALSE)
// 8		
	SETUP_WAVE_PED(s_WavePed[7], 	spf_roof_front_right,		dsd_roof_rightside_front,
		mod_swat, 					WEAPONTYPE_CARBINERIFLE,	 WEAPONCOMPONENT_INVALID,			10,		130,
		1, 0, 0.25,	 FALSE,
		FALSE, FALSE, FALSE)
// 9		
	SETUP_WAVE_PED(s_WavePed[8], 	spf_roof_front_left,		dsd_roof_leftside_rear,
		mod_swat, 					WEAPONTYPE_SMG,	 			WEAPONCOMPONENT_INVALID,			10,		130,
		0, 1000, 0.0, FALSE,
		FALSE, FALSE, FALSE)
// 10	
	SETUP_WAVE_PED(s_WavePed[9], 	spf_main_entrance,			dsd_invalid, //dsd_main_planter_right_front,
		mod_fib_agent_3, 			WEAPONTYPE_PISTOL,	 		WEAPONCOMPONENT_INVALID,			10,		200,
		0, 500, 0.0, FALSE,
		TRUE, TRUE, TRUE)
// 10a		
	SETUP_WAVE_PED(s_WavePed[10], 	spf_main_fake_left,			dsd_main_planter_left_rear,
		mod_fib_agent_1, 			WEAPONTYPE_SMG,	 			WEAPONCOMPONENT_INVALID,			4,		200,
		0, 2000, 0.0, FALSE,
		TRUE, TRUE, FALSE)

// 11		
	SETUP_WAVE_PED(s_WavePed[11], 	spf_main_fake_right,		dsd_main_glass_room_right_front,
		mod_fib_agent_2, 			WEAPONTYPE_PISTOL,	 		WEAPONCOMPONENT_INVALID,			10,		200,
		0, 2000, 0.0, FALSE,
		TRUE, TRUE, FALSE)
// 14		
	SETUP_WAVE_PED(s_WavePed[12], 	spf_main_fake_mid,			dsd_main_chairs_right_front_l,
		mod_fib_agent_1, 			WEAPONTYPE_PISTOL,	 		WEAPONCOMPONENT_INVALID,			10,		200,
		0, 2000, 0.0, FALSE,
		TRUE, TRUE, FALSE)
// 15
	SETUP_WAVE_PED(s_WavePed[13], 	spf_roof_rear_right,		dsd_roof_rear_mid_r,
		mod_swat, 					WEAPONTYPE_CARBINERIFLE,	 WEAPONCOMPONENT_INVALID,			10,		130,
		2, 1000, 0.0, FALSE,
		FALSE, FALSE, FALSE)
// 16
	SETUP_WAVE_PED(s_WavePed[14], 	spf_roof_rear_left,			dsd_roof_rear_l,
		mod_swat, 					WEAPONTYPE_SMG,	 			WEAPONCOMPONENT_INVALID,			10,		130,
		0, 2000, 0.0, FALSE,
		FALSE, FALSE, FALSE)
// 12
	SETUP_WAVE_PED(s_WavePed[15], 	spf_main_entrance,			dsd_invalid, //dsd_main_planter_left_front,
		mod_fib_agent_2, 			WEAPONTYPE_SMG,	 			WEAPONCOMPONENT_INVALID,			4,		200,
		2, 0, 0.35, FALSE,
		TRUE, TRUE, TRUE)
// 13
	SETUP_WAVE_PED(s_WavePed[16], 	spf_main_fake_mid,			dsd_main_chairs_left_rear_r,
		mod_fib_agent_1, 			WEAPONTYPE_PISTOL,	 		WEAPONCOMPONENT_INVALID,			10,		200,
		0, 2000, 0.0, FALSE,
		TRUE, TRUE, FALSE)
		
// HELI CRASH HAPPENS

// SWAT PEDS COME IN AFTER THE GAS GRENADE
	SETUP_WAVE_PED(s_WavePed[17], 	spf_main_entrance,			dsd_main_planter_left_front,
		mod_swat, 					WEAPONTYPE_SMG,	 			WEAPONCOMPONENT_INVALID,			10,		180,
		0, 0, 0.0, TRUE,
		TRUE, TRUE, FALSE)

	SETUP_WAVE_PED(s_WavePed[18], 	spf_main_fake_mid,			dsd_invalid, //dsd_main_planter_right_front,
		mod_swat, 					WEAPONTYPE_CARBINERIFLE,	 WEAPONCOMPONENT_INVALID,			10,		200,
		0, 0, 0.0, FALSE,
		TRUE, TRUE, TRUE)
	
	SETUP_WAVE_PED(s_WavePed[19], 	spf_main_entrance,			dsd_main_glass_room_right_front,
		mod_swat, 					WEAPONTYPE_CARBINERIFLE,	 WEAPONCOMPONENT_INVALID,			10,		180,
		0, 0, 0.0, FALSE,
		TRUE, TRUE, FALSE)
		
	SETUP_WAVE_PED(s_WavePed[20], 	spf_main_fake_mid,			dsd_main_mini_planter_front_right,
		mod_swat, 					WEAPONTYPE_SMG,	 			WEAPONCOMPONENT_INVALID,			10,		180,
		0, 0, 0.0, FALSE,
		TRUE, TRUE, FALSE)
		
	SETUP_WAVE_PED(s_WavePed[21], 	spf_main_entrance,			dsd_main_mini_planter_front_left,
		mod_swat, 					WEAPONTYPE_PUMPSHOTGUN,	 	WEAPONCOMPONENT_INVALID,			2,		180,
		2, 1500, 0.0, FALSE,
		TRUE, TRUE, FALSE)
	
	SETUP_WAVE_PED(s_WavePed[22], 	spf_main_fake_mid,			dsd_invalid, //dsd_main_planter_left_front,
		mod_swat, 					WEAPONTYPE_CARBINERIFLE,	 WEAPONCOMPONENT_INVALID,			10,		200,
		0, 500, 0.0, FALSE,
		TRUE, TRUE, TRUE)
		
	SETUP_WAVE_PED(s_WavePed[23], 	spf_main_entrance,			dsd_main_planter_left_front,
		mod_swat, 					WEAPONTYPE_SMG,	 			WEAPONCOMPONENT_INVALID,			10,		180,
		0, 500, 0.0, FALSE,
		TRUE, TRUE, FALSE)

	
	// Sprinkler positions
	// top floor
//	s_Sprinklers[0].vCoord			= <<153.96, -759.02, 260.74>>
//	s_Sprinklers[1].vCoord			= <<149.50, -757.39, 260.75>>
//	s_Sprinklers[2].vCoord			= <<151.53, -751.81, 260.75>>
//	s_Sprinklers[3].vCoord			= <<143.92, -755.36, 260.75>>
//	s_Sprinklers[4].vCoord			= <<145.95, -749.78, 260.75>>
//	s_Sprinklers[5].vCoord			= <<137.18, -746.03, 259.50>>
//	s_Sprinklers[7].vCoord			= <<131.84, -757.29, 260.95>>
//	s_Sprinklers[8].vCoord			= <<126.26, -755.25, 260.95>>
//	s_Sprinklers[9].vCoord			= <<120.67, -753.22, 260.95>>
//	s_Sprinklers[10].vCoord			= <<151.74, -744.30, 260.95>>
//	s_Sprinklers[11].vCoord			= <<146.15, -742.27, 260.95>>
//	s_Sprinklers[12].vCoord			= <<140.75, -740.24, 260.95>>
//	s_Sprinklers[13].vCoord			= <<126.05, -748.86, 260.95>>
//	s_Sprinklers[14].vCoord			= <<128.09, -743.28, 260.95>>
//	s_Sprinklers[15].vCoord			= <<120.47, -746.83, 260.95>>
//	s_Sprinklers[16].vCoord			= <<122.50, -741.25, 260.95>>
//	s_Sprinklers[17].vCoord			= <<116.92, -739.22, 260.95>>
//	s_Sprinklers[18].vCoord			= <<134.58, -739.32, 260.95>>
//	s_Sprinklers[19].vCoord			= <<137.42, -759.32, 260.95>>
//	s_Sprinklers[20].vCoord			= <<119.76, -731.40, 260.73>>
//	s_Sprinklers[21].vCoord			= <<121.39, -726.94, 260.73>>
	
	// burntout floor 1
	s_Sprinklers[0].vCoord			= <<129.65, -729.84, 256.45>>
	s_Sprinklers[1].vCoord			= <<125.18, -728.22, 256.45>>
	s_Sprinklers[2].vCoord			= <<120.73, -727.85, 256.45>>
	s_Sprinklers[3].vCoord			= <<119.10, -732.32, 256.45>>
	s_Sprinklers[4].vCoord			= <<116.56, -738.97, 256.45>>
	s_Sprinklers[5].vCoord			= <<121.09, -740.61, 256.45>>
	s_Sprinklers[6].vCoord			= <<148.73, -750.67, 256.45>>
	
	// rayfire attached ones
	s_Sprinklers[37].vCoord			= <<125.54, -742.21, 256.41>>
	s_Sprinklers[37].bMustCreateTheSprinkler	= TRUE
	s_Sprinklers[38].vCoord			= <<130.03, -743.85, 256.41>>
	s_Sprinklers[38].bMustCreateTheSprinkler	= TRUE
	s_Sprinklers[39].vCoord			= <<134.48, -745.49, 256.40>>
	s_Sprinklers[39].bMustCreateTheSprinkler	= TRUE
	
	// burntout floor 2
	s_Sprinklers[7].vCoord			= <<139.05, -733.11, 252.25>>
	s_Sprinklers[8].vCoord			= <<134.56, -731.49, 252.25>>
	s_Sprinklers[9].vCoord			= <<138.13, -739.08, 252.25>>
	s_Sprinklers[10].vCoord			= <<136.51, -743.54, 252.25>>
	s_Sprinklers[11].vCoord			= <<131.63, -743.05, 252.25>>
	s_Sprinklers[12].vCoord			= <<127.19, -741.43, 252.25>>
	s_Sprinklers[13].vCoord			= <<122.72, -739.81, 252.25>>
	s_Sprinklers[14].vCoord			= <<118.26, -738.18, 252.25>>
	s_Sprinklers[15].vCoord			= <<117.93, -743.12, 252.25>>
	s_Sprinklers[16].vCoord			= <<116.31, -747.58, 252.25>>
	s_Sprinklers[17].vCoord			= <<117.26, -751.79, 252.25>>
	s_Sprinklers[18].vCoord			= <<120.21, -754.08, 252.25>>
	s_Sprinklers[19].vCoord			= <<127.08, -756.48, 252.25>>
	s_Sprinklers[20].vCoord			= <<131.54, -758.10, 252.25>>
	s_Sprinklers[21].vCoord			= <<136.41, -758.61, 252.25>>
	s_Sprinklers[22].vCoord			= <<139.21, -754.95, 252.25>>
	s_Sprinklers[23].vCoord			= <<143.67, -756.58, 252.25>>
	s_Sprinklers[24].vCoord			= <<148.14, -758.20, 252.25>>
	s_Sprinklers[25].vCoord			= <<152.60, -759.83, 252.25>>
	s_Sprinklers[26].vCoord			= <<152.69, -766.71, 252.25>>
	s_Sprinklers[27].vCoord			= <<151.07, -771.18, 252.25>>
	s_Sprinklers[28].vCoord			= <<145.06, -770.26, 252.25>>
	s_Sprinklers[29].vCoord			= <<140.60, -768.64, 252.25>>
	s_Sprinklers[30].vCoord			= <<141.98, -765.70, 252.25>>
	
	// Lower floor exit
	s_Sprinklers[31].vCoord			= <<140.59, -768.64, 248.25>>
	s_Sprinklers[32].vCoord			= <<145.06, -770.26, 248.25>>
	s_Sprinklers[33].vCoord			= <<150.64, -772.32, 248.25>>
	s_Sprinklers[34].vCoord			= <<152.28, -767.86, 248.25>>
	s_Sprinklers[35].vCoord			= <<155.27, -760.92, 248.25>>
	s_Sprinklers[36].vCoord			= <<160.30, -762.75, 248.25>>
	
	sHacking[0] = -1
	sHacking[1] = -1
	
	REPEAT COUNT_OF(i_NavMeshBlockingAreas) i
		IF i_NavMeshBlockingAreas[i] != -1
			i_NavMeshBlockingAreas[i] = -1
		ENDIF
	ENDREPEAT
	
	// Heli attack
	vHeliAttackCoords[0] = <<223.6732, -767.7599, 80.0>>
	vHeliAttackCoords[1] = <<216.6973, -741.3337, 80.0>>
	vHeliAttackCoords[2] = <<207.3634, -716.4969, 80.0>>
	vHeliAttackCoords[3] = <<187.4426, -688.3856, 80.0>>
	vHeliAttackCoords[4] = <<159.2153, -673.8060, 80.0>>
	
	vHeliAttackBuddyDefensiveArea[0] = <<169.3769, -762.2784, 73.1555>>
	vHeliAttackBuddyDefensiveArea[1] = <<171.2729, -752.6063, 73.1555>>
	vHeliAttackBuddyDefensiveArea[2] = <<169.9714, -741.0135, 73.1555>>
	vHeliAttackBuddyDefensiveArea[3] = <<164.3842, -731.3631, 73.1555>>
	vHeliAttackBuddyDefensiveArea[4] = <<155.0101, -724.1595, 73.1555>>
	
	vHeliAttackGoonDefensiveArea[0] = <<167.6451, -766.4635, 73.1555>>
	vHeliAttackGoonDefensiveArea[1] = <<170.7112, -756.7259, 73.1555>>
	vHeliAttackGoonDefensiveArea[2] = <<170.9940, -745.4851, 73.1555>>
	vHeliAttackGoonDefensiveArea[3] = <<167.0817, -735.4847, 73.1555>>
	vHeliAttackGoonDefensiveArea[4] = <<158.9559, -727.4032, 73.1555>>
	
	sbi_sec_guard_entrance = ADD_SCENARIO_BLOCKING_AREA(<<105.119164,-741.315491,45.506779>> - <<1.000000,1.000000,1.000000>>,
		<<105.119164,-741.315491,45.506779>> + <<1.000000,1.000000,1.000000>>)
		
	sbi_bin_sweatshop = ADD_SCENARIO_BLOCKING_AREA(<<714.975647,-992.737610,24.715918>>-<<4.875000,3.937500,2.750000>>,
		<<714.975647,-992.737610,24.715918>>+<<4.875000,3.937500,2.750000>>)
		
	CLEAR_AREA_OF_PEDS(<<714.975647,-992.737610,24.715918>>, 5.0)
		
	sbi_gov_facility_security_guards[0] = ADD_SCENARIO_BLOCKING_AREA(<<2577.974365,-292.424652,94.578201>>-<<9.437500,25.000000,2.750000>>,
		<<2577.974365,-292.424652,94.578201>>+<<9.437500,25.000000,2.750000>>)
		
	sbi_gov_facility_security_guards[1] = ADD_SCENARIO_BLOCKING_AREA(<<2567.865967,-332.262024,94.628944>>-<<6.125000,8.625000,2.750000>>,
		<<2567.865967,-332.262024,94.628944>>+<<6.125000,8.625000,2.750000>>)
		
	sbi_gov_facility_security_guards[2] = ADD_SCENARIO_BLOCKING_AREA(<<2492.863281,-309.295624,94.492393>>-<<6.125000,8.625000,2.750000>>,
		<<2492.863281,-309.295624,94.492393>>+<<6.125000,8.625000,2.750000>>)
		
	sbi_gov_facility_security_guards[3] = ADD_SCENARIO_BLOCKING_AREA(<<2561.146729,-601.210571,65.593643>>-<<9.187500,16.312500,2.750000>>,
		<<2561.146729,-601.210571,65.593643>>+<<9.187500,16.312500,2.750000>>)
		
	Reset_Cutscene_Ped_Variation_Streaming(sCutscenePedVariationRegister)
	
	// Overall skill determines the weapon
	SWITCH GET_CREW_MEMBER_SKILL(crewGoon)
		CASE CMSK_BAD		weap_Goon 	= WEAPONTYPE_ASSAULTRIFLE			BREAK
		CASE CMSK_MEDIUM	weap_Goon	= WEAPONTYPE_CARBINERIFLE			BREAK
		CASE CMSK_GOOD		weap_Goon	= WEAPONTYPE_CARBINERIFLE			BREAK
	ENDSWITCH
	
// Weapon skill determines the attachments
	INT iWeaponSkill = GET_CREW_MEMBER_GUNMAN_STAT_AS_PERCENTAGE(crewGoon, CGS_WEAPON_CHOICE)
	
	// Grip
	IF iWeaponSkill > 45
		weapComp_GoonGrip	= WEAPONCOMPONENT_AT_AR_AFGRIP
	ENDIF
		
	// Flash light and suppressor
	IF iWeaponSkill > 50
		weapComp_GoonFlash 	= WEAPONCOMPONENT_AT_AR_FLSH
		IF weap_Goon	= WEAPONTYPE_CARBINERIFLE
			weapComp_GoonSupp	= WEAPONCOMPONENT_AT_AR_SUPP
		ELIF
			weapComp_GoonSupp	= WEAPONCOMPONENT_AT_AR_SUPP_02
		ENDIF
	ENDIF
	
	// Clip
	IF iWeaponSkill > 60
		IF weap_Goon	= WEAPONTYPE_CARBINERIFLE
			weapComp_GoonClip	= WEAPONCOMPONENT_CARBINERIFLE_CLIP_02
		ELSE
			weapComp_GoonClip	= WEAPONCOMPONENT_ASSAULTRIFLE_CLIP_02
		ENDIF
	ENDIF
	
	// Scope
	IF iWeaponSkill > 75
		IF weap_Goon	= WEAPONTYPE_CARBINERIFLE
			weapComp_GoonScope	= WEAPONCOMPONENT_AT_SCOPE_MEDIUM
		ELIF
			weapComp_GoonScope	= WEAPONCOMPONENT_AT_SCOPE_MACRO
		ENDIF
	ENDIF

	SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)
	
	IS_PED_INJURED(PLAYER_PED_ID())
	
ENDPROC

// MISSION EVENTS

PROC me_manage_parachuting()
	VECTOR vPedPos
	SWITCH mEvents[mef_manage_parachuting].iStage
		CASE 1
			IF GET_PED_PARACHUTE_STATE(MIKE_PED_ID()) = PPS_DEPLOYING
			OR GET_PED_PARACHUTE_STATE(MIKE_PED_ID()) = PPS_PARACHUTING
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_SKYDIVE_FREEFALL")
					STOP_AUDIO_SCENE("AH_3B_SKYDIVE_FREEFALL")
				ENDIF
				IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_SKYDIVE_PARACHUTE")
					START_AUDIO_SCENE("AH_3B_SKYDIVE_PARACHUTE")
				ENDIF
				mEvents[mef_manage_parachuting].iStage++
			ELSE
				vPedPos = GET_ENTITY_COORDS(peds[mpf_mike].id)
				IF vPedPos.z < F_LANDING_Z_CUTOFF
					Mission_Failed(mff_para_deployed_late_mike)
				ENDIF
			ENDIF
		BREAK
		CASE 2	
			IF NOT bPlayerLandedSafely
				vPedPos = GET_ENTITY_COORDS(MIKE_PED_ID())
				
				// Too low to land on the building
				IF vPedPos.z < F_LANDING_Z_CUTOFF
					Mission_Failed(mff_para_landing_missed_mike)
				ENDIF
			
				// No longer parachuting
				IF GET_PED_PARACHUTE_STATE(MIKE_PED_ID()) = PPS_INVALID
				OR GET_PED_PARACHUTE_STATE(MIKE_PED_ID()) = PPS_LANDING
					// Wait until the player is on the ground to check if they are in the right area before failing
					IF NOT IS_ENTITY_IN_AIR(MIKE_PED_ID())
						// Landed in correct place
						IF IS_ENTITY_IN_ANGLED_AREA(MIKE_PED_ID(), <<102.731689,-738.674255,247.342728>>, <<168.890320,-764.336609,268.466309>>, 56.437500)
							CLEAR_HELP()
							bDisplayedParaHelp3	= FALSE
							bPlayerLandedSafely = TRUE
							Event_Finish(mef_manage_parachute_marker)
						// Landed at the wrong location
						ELSE
							Mission_Failed(mff_para_landing_missed_mike)
						ENDIF
					ENDIF
					
				// else is still parachuting
				ELIF GET_PED_PARACHUTE_STATE(MIKE_PED_ID()) = PPS_PARACHUTING
					IF NOT bDisplayedParaHelp3
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vSkyDiveLandingCoord) < 200.0
							// Nearing the landing zone, pin interior and show help text
							IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
								PRINT_HELP_FOREVER("A3B_PARAHLP3_KM")
							ELSE
								PRINT_HELP_FOREVER("A3B_PARAHLP3")
							ENDIF
							bDisplayedParaHelp3 = TRUE
						ENDIF
					ELSE
						// Re-show help if controls change (because PC uses a different help string).
						IF IS_PC_VERSION()
							IF HAVE_CONTROLS_CHANGED(PLAYER_CONTROL)
								bDisplayedParaHelp3 = FALSE
							ENDIF
						ENDIF
					
					ENDIF
				ENDIF
				
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("A3B_PARAHLP3")
					IF iHelpTimer = -1
						IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
						AND IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
							iHelpTimer = GET_GAME_TIMER() + 2000
						ENDIF
					ELSE
						IF GET_GAME_TIMER() > iHelpTimer
							CLEAR_HELP(FALSE)
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF

			IF bPlayerLandedSafely
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
				VECTOR vPlayerCoord 
				vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
				
				// Landed properly, on the main rooftop and not on the walkway above or below
				IF GET_PED_PARACHUTE_STATE(MIKE_PED_ID()) = PPS_LANDING
				AND vPlayerCoord.z > 261.2
				AND vPlayerCoord.z < 266.3
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(AH3B_LANDING_ACCURACY)
				ENDIF
				
				Event_Finish(mef_manage_parachuting)
			ENDIF
		BREAK
	ENDSWITCH	
ENDPROC

PROC me_manage_parachuting_buddies()
	
	CONST_FLOAT 	fMaxBoost 					5.0
	CONST_FLOAT		fMinDistBeforeCutoff		200.0
	CONST_FLOAT		fBoostStartDist				50.0
	
	VECTOR vPedPos	
	FLOAT fBoost, fDifference, fAlpha

	SWITCH mEvents[mef_manage_parachuting_buddies].iStage
		CASE 1
			// Start franklin skydive
			CLEAR_PED_TASKS_IMMEDIATELY(peds[mpf_frank].id)
			SET_ENTITY_COORDS_NO_OFFSET(peds[mpf_frank].id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehs[mvf_heli].id, <<10.0,-10.0,-20.0>>))
			SET_ENTITY_HEADING(peds[mpf_frank].id, 87.1315)				
			SET_ENTITY_LOAD_COLLISION_FLAG(peds[mpf_frank].id, TRUE) 
			SET_ENTITY_INVINCIBLE(peds[mpf_frank].id, TRUE)
			SET_ENTITY_LOD_DIST(peds[mpf_frank].id, 5000)
			SET_PED_LOD_MULTIPLIER(peds[mpf_frank].id, 5.0)
			TASK_SKY_DIVE(peds[mpf_frank].id)			
			
			// Start gunman skydive				
			CLEAR_PED_TASKS_IMMEDIATELY(peds[mpf_goon].id)
			SET_ENTITY_COORDS_NO_OFFSET(peds[mpf_goon].id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehs[mvf_heli].id, <<30.0,15.0,-20.0>>))
			SET_ENTITY_HEADING(peds[mpf_goon].id, 87.1315)
			SET_ENTITY_LOAD_COLLISION_FLAG(peds[mpf_goon].id, TRUE)
			SET_ENTITY_INVINCIBLE(peds[mpf_goon].id, TRUE)
			SET_ENTITY_LOD_DIST(peds[mpf_goon].id, 5000)
			SET_PED_LOD_MULTIPLIER(peds[mpf_goon].id, 5.0)
			TASK_SKY_DIVE(peds[mpf_goon].id)
			
			iFrankLandingDia	= 0
			iGoonLandingDia 	= 0
			
			mEvents[mef_manage_parachuting_buddies].iTimeStamp = GET_GAME_TIMER()
			mEvents[mef_manage_parachuting_buddies].iStage++
		BREAK
		CASE 2
			FLOAT fDistanceFromTargetMike, fDistanceFromTargetFrank, fDistanceFromTargetGoon
			fDistanceFromTargetMike 	= GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(MIKE_PED_ID()), vSkyDiveLandingCoord, FALSE)
			fDistanceFromTargetFrank 	= GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(FRANK_PED_ID()), vSkyDiveLandingCoord, FALSE)
			fDistanceFromTargetGoon 	= GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(peds[mpf_goon].id), vSkyDiveLandingCoord, FALSE)

			vPedPos = GET_ENTITY_COORDS(FRANK_PED_ID())
			IF vPedPos.z < F_LANDING_Z_CUTOFF
				Mission_Failed(mff_para_landing_missed_frank)
			ENDIF
				
			// chute yet to be deployed
			IF NOT b_frank_parachute_deployed
				
				IF GET_GAME_TIMER() - mEvents[mef_manage_parachuting_buddies].iTimeStamp >= 6000
					IF GET_PED_PARACHUTE_STATE(FRANK_PED_ID()) = PPS_SKYDIVING

						TASK_PARACHUTE_TO_TARGET(FRANK_PED_ID(), vFrankSkyDiveLandCoord)
						
						b_frank_parachute_deployed = TRUE
					ENDIF
				ENDIF
				
			// has deployed chute
			ELSE
				IF NOT bFrankLandedSafely
				
					IF GET_PED_PARACHUTE_STATE(FRANK_PED_ID()) = PPS_PARACHUTING			
						
						// Manage player ped relative boost
						fBoost = 0.0
						IF fDistanceFromTargetFrank > fMinDistBeforeCutoff
							fDifference 	= fDistanceFromTargetMike - fDistanceFromTargetFrank
							fAlpha 			= fDifference/fBoostStartDist
							IF fDifference <= fBoostStartDist
								fBoost = CLAMP((1-fAlpha)*fMaxBoost, 0, fMaxBoost)
							ENDIF
						ENDIF
						SET_PARACHUTE_TASK_THRUST(FRANK_PED_ID(), fBoost)
					
					// No longer parachuting
					ELIF GET_PED_PARACHUTE_STATE(FRANK_PED_ID()) = PPS_INVALID
					
						// Wait until the player is on the ground to check if they are in the right area before failing
						IF NOT IS_ENTITY_IN_AIR(FRANK_PED_ID())
							// Landed in correct place
							IF IS_ENTITY_IN_ANGLED_AREA(FRANK_PED_ID(), <<102.731689,-738.674255,247.342728>>, <<168.890320,-764.336609,268.466309>>, 56.437500)
								SET_ENTITY_INVINCIBLE(FRANK_PED_ID(), FALSE)
								SET_PED_STEALTH_MOVEMENT(FRANK_PED_ID(), TRUE)
								
								TASK_FOLLOW_NAV_MESH_TO_COORD(peds[mpf_frank].id, <<138.7255, -741.7317, 261.8488>>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT, DEFAULT, 196.0327)
								TASK_LOOK_AT_COORD(peds[mpf_frank].id, <<139.0352, -744.2580, 262.0591>>, -1)
								
								bFrankLandedSafely = TRUE
							// Landed at the wrong location
							ELSE
								Mission_Failed(mff_para_landing_missed_frank)
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
			

			vPedPos = GET_ENTITY_COORDS(peds[mpf_goon].id)
			IF vPedPos.z < F_LANDING_Z_CUTOFF
				Mission_Failed(mff_para_landing_missed_gunman)
			ENDIF
				
			// chute yet to be deployed
			IF NOT b_goon_parachute_deployed
				
				IF GET_GAME_TIMER() - mEvents[mef_manage_parachuting_buddies].iTimeStamp >= 7000
					IF GET_PED_PARACHUTE_STATE(peds[mpf_goon].id) = PPS_SKYDIVING
						TASK_PARACHUTE_TO_TARGET(peds[mpf_goon].id, vGoonSkyDiveLandCoord)
						b_goon_parachute_deployed = TRUE
					ENDIF
				ENDIF
				
			// has deployed chute
			ELSE
				IF NOT bGoonLandedSafely
					
					IF GET_PED_PARACHUTE_STATE(peds[mpf_goon].id) = PPS_PARACHUTING		

						// Manage player ped relative boost
						fBoost = 0.0
						IF fDistanceFromTargetGoon > fMinDistBeforeCutoff
							fDifference 	= fDistanceFromTargetMike - fDistanceFromTargetGoon
							fAlpha 			= fDifference/fBoostStartDist
							IF fDifference <= fBoostStartDist
								fBoost = CLAMP((1-fAlpha)*fMaxBoost, 0, fMaxBoost)
							ENDIF
						ENDIF
						SET_PARACHUTE_TASK_THRUST(peds[mpf_goon].id, fBoost)					
					
					// No longer parachuting
					ELIF GET_PED_PARACHUTE_STATE(peds[mpf_goon].id) = PPS_INVALID
					
						// Wait until the player is on the ground to check if they are in the right area before failing
						IF NOT IS_ENTITY_IN_AIR(peds[mpf_goon].id)
							// Landed in correct place
							IF IS_ENTITY_IN_ANGLED_AREA(peds[mpf_goon].id, <<102.731689,-738.674255,247.342728>>, <<168.890320,-764.336609,268.466309>>, 56.437500)
								SET_ENTITY_INVINCIBLE(peds[mpf_goon].id, FALSE)
								SET_PED_STEALTH_MOVEMENT(peds[mpf_goon].id, TRUE, "DEFAULT_ACTION")
								
								TASK_FOLLOW_NAV_MESH_TO_COORD(peds[mpf_goon].id, <<143.6292, -743.4308, 261.8488>>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT, DEFAULT, 118.0060)
								TASK_LOOK_AT_COORD(peds[mpf_goon].id, <<135.5912, -747.2527, 259.9271>>, -1)
								bGoonLandedSafely = TRUE
							// Landed at the wrong location
							ELSE
								Mission_Failed(mff_para_landing_missed_gunman)
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
			
			// Franklin dialogue
			SWITCH iFrankLandingDia
				CASE 0
					IF fDistanceFromTargetFrank < 100
					AND IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_LAND_FRAN", CONV_PRIORITY_HIGH)
							iFrankLandingDia++
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			
			// Gunman dialogue
			SWITCH iGoonLandingDia
				CASE 0
					IF fDistanceFromTargetGoon < 100
					AND IS_SAFE_TO_START_CONVERSATION()
						str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewGoon, "AH_LAND")
						IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
							iGoonLandingDia++
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF bGoonLandedSafely
					AND IS_SAFE_TO_START_CONVERSATION()
						str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewGoon, "AH_LANDD")
						IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
							iGoonLandingDia++
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			
			IF bFrankLandedSafely AND bGoonLandedSafely
				Event_Finish(mef_manage_parachuting_buddies)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC me_manage_parachute_marker()	
	VECTOR 			v_landing_marker_coord 			= << 135.8793, -749.3595, 261.8527 >>
	CONST_FLOAT		f_landing_dist_threshold		500.0
	CONST_FLOAT		f_landing_height_threshold		25.0
	CONST_FLOAT		f_landing_cylinder_min_height 	1.5
	CONST_FLOAT		f_landing_cylinder_max_height 	50.0
	CONST_INT		i_landing_marker_alpha			75
	CONST_INT		i_landing_marker_radius			25

	FLOAT 	f_dist 		= GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(MIKE_PED_ID()), v_landing_marker_coord)
	FLOAT 	f_height 	= f_landing_cylinder_max_height
	INT 	i_alpha		= i_landing_marker_alpha
	
	IF f_dist <= f_landing_dist_threshold
		f_height = CLAMP(f_height * POW((f_dist / f_landing_dist_threshold), 2.0), f_landing_cylinder_min_height, f_landing_cylinder_max_height)
	ENDIF
	
	// Fade out the marker as the player is close to landing
	VECTOR 	v_player_coord = GET_ENTITY_COORDS(MIKE_PED_ID())
	FLOAT	f_z_difference = ABSF(v_player_coord.z - v_landing_marker_coord.z)
	IF f_z_difference <= f_landing_height_threshold
		i_alpha = CLAMP_INT(ROUND(i_alpha * POW((f_z_difference / f_landing_height_threshold), 2.0)), 0, i_landing_marker_alpha)
	ENDIF
	
	INT iColR, iColG, iColB, iColA
	GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iColR, iColG, iColB, iColA)
	
	IF i_alpha != 0
		DRAW_MARKER(MARKER_CYLINDER, v_landing_marker_coord, <<0,0,0>>, <<0,0,0>>, <<i_landing_marker_radius, i_landing_marker_radius, f_height>>, iColR, iColG, iColB, i_alpha)
	ENDIF
ENDPROC

//PURPOSE: Manages hotswapping during the download shootout
PROC me_manage_switch()

	IF NOT sCamDetails.bRun
		// Has player selected a character
		IF UPDATE_SELECTOR_HUD(sSelectorPeds, FALSE)     // Returns TRUE when the player has made a selection
			IF NOT HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MULTIPLAYER)
				sCamDetails.pedTo 			= sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
				sCamDetails.bRun  			= TRUE
			ENDIF
		ENDIF
	ENDIF
	
	// Run the swap function, will do nothing and return no state if no swap has been initiated.		
	IF NOT sCamDetails.bRun
		swState = SWITCH_NOSTATE
	ELSE
		IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails, SWITCH_TYPE_AUTO)
			IF sCamDetails.bOKToSwitchPed
				IF NOT sCamDetails.bPedSwitched
					IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
						CPRINTLN(DEBUG_MIKE, "TAKING CONTROL OF SELECTOR PED")
						sCamDetails.bPedSwitched = TRUE

						IF DOES_ENTITY_EXIST(FRANK_PED_ID())		
							if sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = NULL
								peds[mpf_frank].id = PLAYER_PED_ID()
								SAFE_REMOVE_BLIP(peds[mpf_frank].objective_blip)
							ELSE
								peds[mpf_frank].id = sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] 
								SET_PED_COMBAT_ATTRIBUTES(peds[mpf_frank].id, CA_DISABLE_PIN_DOWN_OTHERS, FALSE)
								SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(peds[mpf_frank].id, FALSE)
							ENDIF
							
							SET_PED_CAN_BE_TARGETTED(FRANK_PED_ID(), FALSE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(FRANK_PED_ID(), TRUE)
						ENDIF
						
						IF DOES_ENTITY_EXIST(MIKE_PED_ID())		
							if sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = NULL
								peds[mpf_mike].id = PLAYER_PED_ID()
								SAFE_REMOVE_BLIP(peds[mpf_mike].objective_blip)
							ELSE
								peds[mpf_mike].id = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
								SET_PED_COMBAT_ATTRIBUTES(peds[mpf_mike].id, CA_DISABLE_PIN_DOWN_OTHERS, FALSE)
								SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(peds[mpf_mike].id, FALSE)
							ENDIF
							
							SET_PED_CAN_BE_TARGETTED(MIKE_PED_ID(), FALSE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_PED_ID(), TRUE)
						ENDIF
						
						SET_PED_COMBAT_ATTRIBUTES(PLAYER_PED_ID(), CA_DISABLE_PIN_DOWN_OTHERS, TRUE)
					ENDIF
				ENDIF
			ENDIF	
		
			swState = SWITCH_IN_PROGRESS
		ELSE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
				//FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				SET_PED_CAN_BE_TARGETTED(PLAYER_PED_ID(), TRUE)
				
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
				ENDIF
			ENDIF
			
			IF sCamDetails.pedTo = MIKE_PED_ID()
				i_fps_state = DC_CAM_COVER_CURRENT
			ENDIF
			
			sCamDetails.bRun  = FALSE
			INFORM_MISSION_STATS_OF_INCREMENT(AH3B_SWITCHES)
			swState = SWITCH_COMPLETE
		ENDIF
	ENDIF

ENDPROC

PROC me_manage_radar_map()
	// If player is in the V_FIB03 interior...
	IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_fib03
		VECTOR v_player_pos
		v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		// ... yet they have dropped down into the other floor 
		// (technically still in the v_fib03 interior but needs the v_fib02 map)
		// override the map this frame
		IF v_player_pos.z < 253.0016
			SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FIB03"), 135.332, -746.367, 250, 4)
		ENDIF
		
		// if they are above don't override, still need the V_FIB03 map
	ENDIF
ENDPROC

PROC me_manage_sprinklers()

	IF IS_INTERIOR_SCENE()
	
		MODEL_NAMES modDesiredSprinkler, modNextSprinkler, modNotNeeded
		INT iNextSprinklerState, iNotNeeded
		SWITCH iDesiredSprinklerState
			CASE -1			modDesiredSprinkler = V_ILEV_FIB_SPRKLR				iNextSprinklerState = 1			BREAK
			CASE 0			modDesiredSprinkler = V_ILEV_FIB_SPRKLR_OFF			iNextSprinklerState = -1		BREAK
			CASE 1			modDesiredSprinkler = V_ILEV_FIB_SPRKLR_ON			iNextSprinklerState = 0			BREAK
		ENDSWITCH
		
		SWITCH iNextSprinklerState
			CASE -1			modNextSprinkler = V_ILEV_FIB_SPRKLR				iNotNeeded = 1		BREAK
			CASE 0			modNextSprinkler = V_ILEV_FIB_SPRKLR_OFF			iNotNeeded = -1		BREAK
			CASE 1			modNextSprinkler = V_ILEV_FIB_SPRKLR_ON				iNotNeeded = 0		BREAK
		ENDSWITCH
		
		SWITCH iNotNeeded
			CASE -1			modNotNeeded = V_ILEV_FIB_SPRKLR			BREAK
			CASE 0			modNotNeeded = V_ILEV_FIB_SPRKLR_OFF		BREAK
			CASE 1			modNotNeeded = V_ILEV_FIB_SPRKLR_ON			BREAK
		ENDSWITCH
		
		IF iDesiredSprinklerState = 0
		OR iDesiredSprinklerState = 1
			IF DOES_ENTITY_EXIST(BUDDY_PED_ID()) AND NOT IS_PED_INJURED(BUDDY_PED_ID())
				SET_PED_WETNESS_ENABLED_THIS_FRAME(BUDDY_PED_ID())
			ENDIF
			IF DOES_ENTITY_EXIST(peds[mpf_goon].id) AND NOT IS_PED_INJURED(peds[mpf_goon].id)
				SET_PED_WETNESS_ENABLED_THIS_FRAME(peds[mpf_goon].id)
			ENDIF
		ENDIF
	
		Load_Asset_Model(sAssetData, modDesiredSprinkler)
		Load_Asset_Model(sAssetData, modNextSprinkler)
		Unload_Asset_Model(sAssetData, modNotNeeded)
		
		IF HAS_MODEL_LOADED(modDesiredSprinkler)
		AND HAS_MODEL_LOADED(modNextSprinkler)
			
			IF s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].iCurrentModel != modDesiredSprinkler
			AND NOT IS_VECTOR_ZERO(s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].vCoord)
			
				// Sprinklers that have to be created
				IF s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].bMustCreateTheSprinkler
				
					// delete the existing object if one exists and is not the right model
					IF DOES_ENTITY_EXIST(s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].objSprinkler)
						IF s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].iCurrentModel != modDesiredSprinkler
						OR GET_ENTITY_MODEL(s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].objSprinkler) != modDesiredSprinkler
							DELETE_OBJECT(s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].objSprinkler)
						ENDIF
					ENDIF
				
					// create a new object if one doesn't exist of the desired model type
					IF NOT DOES_ENTITY_EXIST(s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].objSprinkler)
						s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].objSprinkler = CREATE_OBJECT_NO_OFFSET(modDesiredSprinkler, 
							s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].vCoord)
							
						s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].iCurrentModel = modDesiredSprinkler
					ENDIF 
				
				// sprinklers that have to be swapped
				ELSE
			
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].vCoord) < 900.0 // 30^2
						
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].vCoord, 0.25, s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].iCurrentModel)
							IF s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].iCurrentModel != V_ILEV_FIB_SPRKLR
								REMOVE_MODEL_SWAP(s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].vCoord, 0.25, V_ILEV_FIB_SPRKLR, s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].iCurrentModel)
								s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].iCurrentModel = V_ILEV_FIB_SPRKLR
							ENDIF
							
							IF modDesiredSprinkler != V_ILEV_FIB_SPRKLR
								CREATE_MODEL_SWAP(s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].vCoord, 0.25, s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].iCurrentModel, modDesiredSprinkler, TRUE)
							ENDIF
							s_Sprinklers[mEvents[mef_manage_sprinklers].iStage].iCurrentModel = modDesiredSprinkler
						ENDIF
					
					ENDIF
				
				ENDIF
			
			ENDIF
			
			mEvents[mef_manage_sprinklers].iStage++
			
			IF mEvents[mef_manage_sprinklers].iStage >= COUNT_OF(s_Sprinklers) 
				mEvents[mef_manage_sprinklers].iStage = 0	// reset the loop
			ENDIF
		
		ENDIF
	
	ENDIF
	
ENDPROC

PROC me_manage_clock()
	SWITCH mEvents[mef_manage_clock].iStage
		CASE 1
			// Has player entered building
			IF mission_stage >= enum_to_int(msf_4_hack_the_computer)
			AND mission_stage <= enum_to_int(msf_8_burnt_out_floor_2)
			
				// Check current time and lock it if at night
				IF GET_CLOCK_HOURS() >= iStartHours AND GET_CLOCK_HOURS() <= 4
				
					mEvents[mef_manage_clock].iStage++
					
				ELIF mission_stage = enum_to_int(msf_7_burnt_out_floor_1)
				
					ADVANCE_CLOCK_TIME_TO(iStartHours, 0, 0)
					mEvents[mef_manage_clock].iStage++
					
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF mission_stage >= enum_to_int(msf_9_abseil_down)
				PAUSE_CLOCK(FALSE)
				Event_Finish(mef_manage_clock)
			ELSE
				PAUSE_CLOCK(TRUE)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC me_manage_fire_meshes()

	INT i

	IF mission_stage > enum_to_int(msf_8_burnt_out_floor_2)
	AND NOT IS_CUTSCENE_PLAYING()
	
		REPEAT ASSETS_MAX_NUM_MOVIE_MESH_SETS i
			IF sAssetData.sMovieMeshSets[i].status.bRequested
				sAssetData.sMovieMeshSets[i].status.bCleanup = TRUE
			ENDIF
			sAssetData.bStreamingLoopAwake = TRUE
		ENDREPEAT
		Event_Finish(mef_manage_fire_meshes)
	
	ELSE

		// Player has moved room
		IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) !=  mEvents[mef_manage_fire_meshes].iStage
		
			INTERIOR_INSTANCE_INDEX interiorsNeeded[5]
			STRING strSetsNeeded[5]
			
			SWITCH GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
				CASE HASH("V_FIB03_IT3_stai")
					strSetsNeeded[0]		= "agency_IT3_01_3b.xml"
					interiorsNeeded[0]		= interior_fib03
				BREAK
				CASE HASH("V_FIB03_IT3_post")
					strSetsNeeded[0]		= "agency_IT3_01_3b.xml"
					interiorsNeeded[0]		= interior_fib03
					
					strSetsNeeded[1]		= "agency_IT3_02_3b.xml"
					interiorsNeeded[1]		= interior_fib03
				BREAK
				CASE HASH("V_FIB03_IT3_hall")
					strSetsNeeded[0]		= "agency_IT3_01_3b.xml"
					interiorsNeeded[0]		= interior_fib03
					
					strSetsNeeded[1]		= "agency_IT3_02_3b.xml"
					interiorsNeeded[1]		= interior_fib03
					
					strSetsNeeded[2]		= "agency_IT3_03_3b.xml"
					interiorsNeeded[2]		= interior_fib03
				BREAK
				CASE HASH("V_FIB03_IT3_cor1")
					strSetsNeeded[0]		= "agency_IT3_01_3b.xml"
					interiorsNeeded[0]		= interior_fib03
					
					strSetsNeeded[1]		= "agency_IT3_02_3b.xml"
					interiorsNeeded[1]		= interior_fib03
					
					strSetsNeeded[2]		= "agency_IT3_03_3b.xml"
					interiorsNeeded[2]		= interior_fib03
				BREAK
				CASE HASH("V_FIB03_IT3_open")
					strSetsNeeded[0]		= "agency_IT3_01_3b.xml"
					interiorsNeeded[0]		= interior_fib03
					
					strSetsNeeded[1]		= "agency_IT3_02_3b.xml"
					interiorsNeeded[1]		= interior_fib03
					
					strSetsNeeded[2]		= "agency_IT3_03_3b.xml"
					interiorsNeeded[2]		= interior_fib03
					
					strSetsNeeded[3]		= "agency_IT2_01_3b.xml"
					interiorsNeeded[3]		= interior_fib02
					
					strSetsNeeded[4]		= "agency_IT2_02_3b.xml"
					interiorsNeeded[4]		= interior_fib02
				BREAK
				CASE HASH("V_FIB02_IT2_elev")
					strSetsNeeded[0]		= "agency_IT3_03_3b.xml"
					interiorsNeeded[0]		= interior_fib03
					
					strSetsNeeded[1]		= "agency_IT2_01_3b.xml"
					interiorsNeeded[1]		= interior_fib02

					strSetsNeeded[2]		= "agency_IT2_02_3b.xml"
					interiorsNeeded[2]		= interior_fib02
					
					strSetsNeeded[3]		= "agency_IT2_04_3b.xml"
					interiorsNeeded[3]		= interior_fib02
				BREAK
				CASE HASH("V_FIB02_IT2_cor1")
					strSetsNeeded[0]		= "agency_IT2_01_3b.xml"
					interiorsNeeded[0]		= interior_fib02

					strSetsNeeded[1]		= "agency_IT2_02_3b.xml"
					interiorsNeeded[1]		= interior_fib02
					
					strSetsNeeded[2]		= "agency_IT2_04_3b.xml"
					interiorsNeeded[2]		= interior_fib02
				BREAK
				CASE HASH("V_FIB02_IT2_cor2")

					strSetsNeeded[0]		= "agency_IT2_02_3b.xml"
					interiorsNeeded[0]		= interior_fib02
					
					strSetsNeeded[1]		= "agency_IT2_04_3b.xml"
					interiorsNeeded[1]		= interior_fib02
					
					strSetsNeeded[2]		= "agency_IT2_05_3b.xml"
					interiorsNeeded[2]		= interior_fib02
					
					strSetsNeeded[3]		= "agency_IT2_06_3b.xml"
					interiorsNeeded[3]		= interior_fib02
				BREAK
				CASE HASH("V_FIB02_IT2_ser1")
					strSetsNeeded[0]		= "agency_IT2_04_3b.xml"
					interiorsNeeded[0]		= interior_fib02
					
					strSetsNeeded[1]		= "agency_IT2_05_3b.xml"
					interiorsNeeded[1]		= interior_fib02
					
					strSetsNeeded[2]		= "agency_IT2_06_3b.xml"
					interiorsNeeded[2]		= interior_fib02
					
					strSetsNeeded[3]		= "agency_IT2_07_3b.xml"
					interiorsNeeded[3]		= interior_fib02
				BREAK
				CASE HASH("V_FIB02_IT2_ser2")
					strSetsNeeded[0]		= "agency_IT2_05_3b.xml"
					interiorsNeeded[0]		= interior_fib02
					
					strSetsNeeded[1]		= "agency_IT2_06_3b.xml"
					interiorsNeeded[1]		= interior_fib02
					
					strSetsNeeded[2]		= "agency_IT2_07_3b.xml"
					interiorsNeeded[2]		= interior_fib02
				BREAK
				CASE HASH("V_FIB02_IT2_cor3")
					strSetsNeeded[0]		= "agency_IT2_05_3b.xml"
					interiorsNeeded[0]		= interior_fib02
					
					strSetsNeeded[1]		= "agency_IT2_06_3b.xml"
					interiorsNeeded[1]		= interior_fib02
					
					strSetsNeeded[2]		= "agency_IT2_07_3b.xml"
					interiorsNeeded[2]		= interior_fib02
					
					strSetsNeeded[3]		= "agency_IT2_08_3b.xml"
					interiorsNeeded[3]		= interior_fib02
				BREAK
				CASE HASH("V_FIB02_IT2_post")
					strSetsNeeded[0]		= "agency_IT2_07_3b.xml"
					interiorsNeeded[0]		= interior_fib02
					
					strSetsNeeded[1]		= "agency_IT2_08_3b.xml"
					interiorsNeeded[1]		= interior_fib02
					
					strSetsNeeded[2]		= "agency_IT2_09_3b.xml"
					interiorsNeeded[2]		= interior_fib02
				BREAK
				CASE HASH("V_FIB02_IT2_stai")
					strSetsNeeded[0]		= "agency_IT2_07_3b.xml"
					interiorsNeeded[0]		= interior_fib02
					
					strSetsNeeded[1]		= "agency_IT2_08_3b.xml"
					interiorsNeeded[1]		= interior_fib02
					
					strSetsNeeded[2]		= "agency_IT2_09_3b.xml"
					interiorsNeeded[2]		= interior_fib02
				BREAK
				CASE HASH("V_FIB02_str_IT")
					strSetsNeeded[0]		= "agency_IT2_08_3b.xml"
					interiorsNeeded[0]		= interior_fib02
					
					strSetsNeeded[1]		= "agency_IT2_09_3b.xml"
					interiorsNeeded[1]		= interior_fib02
					
					strSetsNeeded[2]		= "agency_IT1_02_3b.xml"
					interiorsNeeded[2]		= interior_fib02
				BREAK
				CASE HASH("V_FIB02_IT1_stai")
					strSetsNeeded[0]		= "agency_IT2_09_3b.xml"
					interiorsNeeded[0]		= interior_fib02
					
					strSetsNeeded[1]		= "agency_IT1_02_3b.xml"
					interiorsNeeded[1]		= interior_fib02
					
					strSetsNeeded[2]		= "agency_IT1_03_3b.xml"
					interiorsNeeded[2]		= interior_fib02
				BREAK
				CASE HASH("V_FIB02_IT1_post")
					strSetsNeeded[0]		= "agency_IT1_02_3b.xml"
					interiorsNeeded[0]		= interior_fib02
					
					strSetsNeeded[1]		= "agency_IT1_03_3b.xml"
					interiorsNeeded[1]		= interior_fib02
				BREAK
				CASE HASH("V_FIB02_IT1_off1")
					strSetsNeeded[0]		= "agency_IT1_02_3b.xml"
					interiorsNeeded[0]		= interior_fib02
					
					strSetsNeeded[1]		= "agency_IT1_03_3b.xml"
					interiorsNeeded[1]		= interior_fib02
				BREAK
				DEFAULT
				BREAK
			ENDSWITCH
			
			// Mark all to clean up
			REPEAT ASSETS_MAX_NUM_MOVIE_MESH_SETS i
				IF sAssetData.sMovieMeshSets[i].status.bRequested
					sAssetData.sMovieMeshSets[i].status.bCleanup = TRUE
				ENDIF
				sAssetData.bStreamingLoopAwake = TRUE
			ENDREPEAT
			
			// Load/unmark for cleanup; the desired meshes
			REPEAT 5 i
				IF IS_VALID_INTERIOR(interiorsNeeded[i])
				AND NOT IS_STRING_NULL_OR_EMPTY(strSetsNeeded[i])
					Load_Asset_Interior(sAssetData, interiorsNeeded[i])
					Load_Asset_Movie_Mesh_Set(sAssetData, strSetsNeeded[i], interiorsNeeded[i])
				ENDIF
			ENDREPEAT
			
			// Update to new room
			mEvents[mef_manage_fire_meshes].iStage = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
		
		ENDIF
		
	ENDIF

ENDPROC

PROC me_manage_smoke_vfx()

	CONST_INT		I_PTFX_EVO_LERP_TIME		1000  // interpolation time in ms

	IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_fib02
	OR GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_fib03
	
		Load_Asset_PTFX(sAssetData)
	
		IF HAS_PTFX_ASSET_LOADED()

			BOOL bPTFXNotNeeded, bHeliRoomAdditionalPTFXOn
			
			FLOAT fDesiredSmokeDensity, fDesiredSmokeStrength, fDesiredCinderDensity, fDesiredDebrisDensity, fDesiredWindSpeed
			BOOL bUseModifier
			
			SWITCH GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
				
			// NO FIRE, MEDIUM DEBRIS FROM DAMAGE, MEDIUM SMOKE, NO WIND
				CASE HASH("V_FIB02_IT2_cor2")			FALLTHRU			// Paramedic corridor
				CASE HASH("V_FIB02_IT2_cser2")			FALLTHRU			// 2nd bit of the comms/server room
				CASE HASH("V_FIB02_IT2_cor3")			FALLTHRU			// corridor after the comms/server room
				CASE HASH("V_FIB02_IT2_post")			FALLTHRU			// immediately after the corridor on exiting the server room 
				CASE HASH("V_FIB02_IT2_stai")								// immediately before the stairs down to the final floor

					bUseModifier			= TRUE
					fDesiredSmokeStrength	= 0.25
					fDesiredSmokeDensity 	= 0.5
					fDesiredCinderDensity	= 0.0
					fDesiredDebrisDensity	= 0.5
					fDesiredWindSpeed		= 0.0
					
				BREAK
				
			// LIGHT FIRE, LIGHT DEBRIS FROM DAMAGE, MEDIUM SMOKE, MEDIUM WIND
				CASE HASH("V_FIB03_IT3_cor1")								// little room inbetween the heli crash and the first burntout bit

					bUseModifier			= TRUE
					fDesiredSmokeStrength	= 0.25
					fDesiredSmokeDensity 	= 0.5
					fDesiredCinderDensity	= 0.25
					fDesiredDebrisDensity	= 0.25
					fDesiredWindSpeed		= 0.5
					bHeliRoomAdditionalPTFXOn = TRUE
				
				BREAK
				
			// HEAVYISH FIRE, HEAVYISH DEBRIS FROM DAMAGE, MEDIUM SMOKE, NO WIND
				CASE HASH("V_FIB03_IT3_hall")								// First burnout section, with the agents in wait and the smashed glass offices
					
					bUseModifier			= TRUE
					fDesiredSmokeStrength	= 0.5
					fDesiredSmokeDensity 	= 0.5
					fDesiredCinderDensity	= 0.75
					fDesiredDebrisDensity	= 0.75
					fDesiredWindSpeed		= 0.0
					
				BREAK
				
			// HEAVYISH FIRE, HEAVYISH DEBRIS FROM DAMAGE, MEDIUM SMOKE, HIGH WIND
				CASE HASH("V_FIB03_IT3_open")								// heli crash room 
				
					bUseModifier			= TRUE
					fDesiredSmokeStrength	= 0.5
					fDesiredSmokeDensity 	= 0.5
					fDesiredCinderDensity	= 0.75
					fDesiredDebrisDensity	= 0.75
					fDesiredWindSpeed		= 1.0
					bHeliRoomAdditionalPTFXOn = TRUE
				
				BREAK
				
			// MEDIUM CINDERS, HEAVYISH DEBRIS FROM DAMAGE, MEDIUM SMOKE, LOW WIND
				CASE HASH("V_FIB02_IT2_elev")								// elevator with the agent banging at it to get out
				
					bUseModifier			= TRUE
					fDesiredSmokeStrength	= 0.5
					fDesiredSmokeDensity 	= 0.5
					fDesiredCinderDensity	= 0.5
					fDesiredDebrisDensity	= 0.75
					fDesiredWindSpeed		= 0.25
					bHeliRoomAdditionalPTFXOn = TRUE
				
				BREAK
			
			// LOW CINDERS, MEDIUM DEBRIS FROM DAMAGE, MEDIUM SMOKE, NO WIND
				CASE HASH("V_FIB02_IT2_cor1")								// corridor just past the elevator with the agent trying to escap
				
					bUseModifier			= TRUE
					fDesiredSmokeStrength	= 0.5
					fDesiredSmokeDensity 	= 0.5
					fDesiredCinderDensity	= 0.25
					fDesiredDebrisDensity	= 0.5
					fDesiredWindSpeed		= 0.0
					
				BREAK
				
			// VERY LOW CINDERS, HEAVYISH DEBRIS, HEAVYISH SMOKE, NO WIND
				CASE HASH("V_FIB02_IT2_cser1")								// first bit of the comms/server room
				
					bUseModifier			= TRUE
					fDesiredSmokeStrength	= 1.0
					fDesiredSmokeDensity 	= 0.75
					fDesiredCinderDensity	= 0.1
					fDesiredDebrisDensity	= 0.75
					fDesiredWindSpeed		= 0.0
				
				BREAK
				
			// NO FIRE, NO DEBRIS, VERY LIGHT SMOKE, NO WIND
				CASE HASH("V_FIB03_IT3_stai")								// room just off the stairs prior to entering first burntout area
				CASE HASH("V_FIB03_IT3_post")								// room just before entering first burntout area
				CASE HASH("V_FIB02_str_IT")									// final set of stairs down to the final rappel floor
				CASE HASH("V_FIB02_IT1_stai")
				CASE HASH("V_FIB02_IT1_post")								// room just before the rappel room	
				CASE HASH("V_FIB02_IT1_off1")								// the rappel room
				DEFAULT
				
					bUseModifier			= FALSE
					fDesiredSmokeStrength	= 0.0
					fDesiredSmokeDensity 	= 0.0
					fDesiredCinderDensity	= 0.0
					fDesiredDebrisDensity	= 0.0
					fDesiredWindSpeed		= 0.0
					
				BREAK
			ENDSWITCH
			
			IF fDesiredSmokeStrength	= 0.0
			AND fDesiredSmokeDensity 	= 0.0
			AND fDesiredCinderDensity 	= 0.0
			AND fDesiredDebrisDensity	= 0.0
				bPTFXNotNeeded = TRUE
			ENDIF
			
			IF NOT bPTFXNotNeeded
				IF NOT IS_AMBIENT_ZONE_ENABLED("AZ_FBI_HEIST_SPRINKLER_FIRES_A_01")
					SET_AMBIENT_ZONE_STATE("AZ_FBI_HEIST_SPRINKLER_FIRES_A_01", TRUE)
				ENDIF
			ELSE
				CLEAR_AMBIENT_ZONE_STATE("AZ_FBI_HEIST_SPRINKLER_FIRES_A_01", TRUE)
			ENDIF
			
			// Create PTFX 
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_playerAttachedSmokeFX)
				
				IF NOT bPTFXNotNeeded
					ptfx_playerAttachedSmokeFX 				= START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_env_agency3b_smoke", PLAYER_PED_ID(), <<0,0,0>>,<<0,0,0>>, 1.0)
					ped_PlayerAttachedSmokeAttachedPed 		= PLAYER_PED_ID()
				ENDIF
				
			// Clean up PTFX
			ELSE
				
				IF PLAYER_PED_ID() != ped_PlayerAttachedSmokeAttachedPed
				OR bPTFXNotNeeded
					STOP_PARTICLE_FX_LOOPED(ptfx_playerAttachedSmokeFX)
					ptfx_playerAttachedSmokeFX 			= NULL
					ped_PlayerAttachedSmokeAttachedPed 	= NULL
				ENDIF
				
			ENDIF
			
			// Interpolate the values
			LERP_PTFX_EVO(s_PlayerAttachedSmokeEvo_SmokeDensity, 	fDesiredSmokeDensity,	I_PTFX_EVO_LERP_TIME)
			LERP_PTFX_EVO(s_PlayerAttachedSmokeEvo_SmokeStrength, 	fDesiredSmokeStrength,	I_PTFX_EVO_LERP_TIME)
			LERP_PTFX_EVO(s_PlayerAttachedSmokeEvo_CinderDensity, 	fDesiredCinderDensity,	I_PTFX_EVO_LERP_TIME)
			LERP_PTFX_EVO(s_PlayerAttachedSmokeEvo_DebrisDensity, 	fDesiredDebrisDensity,	I_PTFX_EVO_LERP_TIME)
			LERP_PTFX_EVO(s_PlayerAttachedSmokeEvo_WindSpeed, 		fDesiredWindSpeed,		I_PTFX_EVO_LERP_TIME)
			
			IF bUseModifier
				IF NOT b_TimeCycleModifierActive
					IF GET_TIMECYCLE_MODIFIER_INDEX() = -1
						SET_TRANSITION_TIMECYCLE_MODIFIER("V_FIB_IT3", I_PTFX_EVO_LERP_TIME/1000)
						b_TimeCycleModifierActive = TRUE
					ENDIF
				ENDIF
			ELSE
				IF b_TimeCycleModifierActive
					IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
						SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(I_PTFX_EVO_LERP_TIME/1000)
					ENDIF
					b_TimeCycleModifierActive = FALSE
				ENDIF
			ENDIF
			
			// Adjust the evo if PTFX is running
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_playerAttachedSmokeFX)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_playerAttachedSmokeFX, "smoke", 			1 - s_PlayerAttachedSmokeEvo_SmokeDensity.fCurrent)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_playerAttachedSmokeFX, "cinder", 			1 - s_PlayerAttachedSmokeEvo_CinderDensity.fCurrent)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_playerAttachedSmokeFX, "debris", 			1 - s_PlayerAttachedSmokeEvo_DebrisDensity.fCurrent)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_playerAttachedSmokeFX, "smoke_strength", 	s_PlayerAttachedSmokeEvo_SmokeStrength.fCurrent)
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_playerAttachedSmokeFX, "speed", 			s_PlayerAttachedSmokeEvo_WindSpeed.fCurrent)
			ENDIF
			
			TEXT_LABEL_23 TL_PTFX_SPARKS	= "ent_amb_fbi_live_wires"
			TEXT_LABEL_23 TL_PTFX_SILT 		= "scr_fbi5b_silt_fall"
			
			CONST_INT	I_PTFX_SILT_FALL_TIME_MIN 		7
			CONST_INT	I_PTFX_SILT_FALL_TIME_MAX 		12

			IF b_HeliRayFireExplosionHappened
				
				// Additional PTFX for the heli crash room	
				IF bHeliRoomAdditionalPTFXOn
				
					IF NOT DOES_PARTICLE_FX_LOOPED_EXIST( ptfxs[ptfx_sparks_wall_right] )
						ptfxs[ptfx_sparks_wall_right] = START_PARTICLE_FX_LOOPED_AT_COORD( TL_PTFX_SPARKS, <<156.2992, -738.6639, 255.4017>>, <<-107.3630, -0.0000, 34.5115>>, 1.0 )
					ENDIF
					IF NOT DOES_PARTICLE_FX_LOOPED_EXIST( ptfxs[ptfx_sparks_ceiling] )
						ptfxs[ptfx_sparks_ceiling] = START_PARTICLE_FX_LOOPED_AT_COORD( TL_PTFX_SPARKS, <<151.8907, -734.0749, 256.0058>>, <<-128.8885, -0.0000, -86.9328>>, 1.0 )
					ENDIF
					
					IF i_Silt_Timer_1 = -1
					OR GET_GAME_TIMER() > i_Silt_Timer_1
						
						START_PARTICLE_FX_NON_LOOPED_AT_COORD( TL_PTFX_SILT, <<154.8069, -735.1069, 256.4583>>, <<-180.0, -0.0000, 147.0874>>, 1.0 )
						i_Silt_Timer_1 = GET_GAME_TIMER() + ( 1000 * GET_RANDOM_INT_IN_RANGE( I_PTFX_SILT_FALL_TIME_MIN, I_PTFX_SILT_FALL_TIME_MAX ) )
						
					ENDIF
					
					IF i_Silt_Timer_2 = -1
					OR GET_GAME_TIMER() > i_Silt_Timer_2
						
						START_PARTICLE_FX_NON_LOOPED_AT_COORD( TL_PTFX_SILT, <<151.4233, -731.1190, 256.5137>>, <<-180.0, -0.0000, -136.6220>>, 1.0 )
						i_Silt_Timer_2 = GET_GAME_TIMER() + ( 1000 * GET_RANDOM_INT_IN_RANGE( I_PTFX_SILT_FALL_TIME_MIN, I_PTFX_SILT_FALL_TIME_MAX ) )
						
					ENDIF
					
					IF i_Silt_Timer_3 = -1
					OR GET_GAME_TIMER() > i_Silt_Timer_3
						
						START_PARTICLE_FX_NON_LOOPED_AT_COORD( TL_PTFX_SILT, <<154.3719, -736.9957, 256.5598>>, <<-180.0, -0.0000, -40.3884>>, 1.0 )
						i_Silt_Timer_3 = GET_GAME_TIMER() + ( 1000 * GET_RANDOM_INT_IN_RANGE( I_PTFX_SILT_FALL_TIME_MIN, I_PTFX_SILT_FALL_TIME_MAX ) )
						
					ENDIF
					
				ELSE
					IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxs[ptfx_sparks_wall_right] )
						STOP_PARTICLE_FX_LOOPED( ptfxs[ptfx_sparks_wall_right] )
					ENDIF
					IF DOES_PARTICLE_FX_LOOPED_EXIST( ptfxs[ptfx_sparks_ceiling] )
						STOP_PARTICLE_FX_LOOPED( ptfxs[ptfx_sparks_ceiling] )
					ENDIF
				ENDIF
			
			ENDIF
			
			
			
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Manages locking and unlocking the doors of the fake rooms once peds have left/entered the room. Additionally it also blips peds as they exit the fake rooms and into the main room.
PROC me_manage_shootout_doors()

	// Determine which door to update this frame
	INT iDoor = mEvents[mef_manage_shootout_doors].iStage
	INT iRoomHash
	BOOL bUnlockDoor
	
	VECTOR vDoorCentre
	FLOAT fDoorClearDist
	
	DOOR_FLAGS eDoorFlagL
	DOOR_FLAGS eDoorFlagR
	
	SWITCH iDoor
		CASE 1
			iRoomHash 		= HASH("V_FIB03_atr_spawn2")
			
			vDoorCentre 	= <<114.849754,-742.408081,0.0>>
			fDoorClearDist 	= 4.0	// 2.0^2
			
			eDoorFlagL		= mdf_topfloor_fake_c
			eDoorFlagR		= mdf_invalid
		BREAK
		CASE 2
			iRoomHash 		= HASH("V_FIB03_atr_spawn1")
			
			vDoorCentre 	= <<122.239670,-758.090698,0.0>>
			fDoorClearDist 	= 4.0 // 2.0^2
			
			eDoorFlagL		= mdf_topfloor_fake_l
			eDoorFlagR		= mdf_invalid
		BREAK
		CASE 3
			iRoomHash 		= HASH("V_FIB03_atr_post")
			
			vDoorCentre 	= <<117.973648,-735.801819,0.0>>
			fDoorClearDist 	= 6.25 // 2.5^2
			
			eDoorFlagL		= mdf_topfloor_main_l
			eDoorFlagR		= mdf_topfloor_main_r
		BREAK
		CASE 4
			iRoomHash 		= HASH("V_FIB03_atr_spawn3")

			vDoorCentre 	= <<139.59, -735.38,0.0>>
			fDoorClearDist 	= 4.0 // 2.0^2
			
			eDoorFlagL		= mdf_topfloor_fake_r
			eDoorFlagR		= mdf_invalid
		BREAK
	ENDSWITCH

	INT x
	REPEAT COUNT_OF(peds) x
	
		IF peds[x].id != NULL
		AND DOES_ENTITY_EXIST(peds[x].id)
		AND NOT IS_PED_INJURED(peds[x].id)
		
			// Check if ped is in room
			IF GET_ROOM_KEY_FROM_ENTITY(peds[x].id) = iRoomHash
				
				peds[x].iCurrentlyInRoom = iDoor
				peds[x].iExitingThisRoom = iDoor	
				bUnlockDoor = TRUE
				CPRINTLN(DEBUG_MIKE, "PED IN ROOM: ", iDoor, " flag to unlock")
			
			// Not in the room check that they are clear of the door
			ELIF peds[x].iCurrentlyInRoom = iDoor
			OR peds[x].iExitingThisRoom = iDoor
			
				peds[x].iCurrentlyInRoom = -1
				
				IF peds[x].iExitingThisRoom = iDoor
			
					VECTOR vPedCoord2D = GET_ENTITY_COORDS(peds[x].id)
					vPedCoord2D.z = 0.0
					
					IF VDIST2(vPedCoord2D, vDoorCentre) < fDoorClearDist
						CPRINTLN(DEBUG_MIKE, "PED NEAR ROOM: ", iDoor, " flag to unlock")
						bUnlockDoor = TRUE
					ELSE
						CPRINTLN(DEBUG_MIKE, "PED CLEAR OF DOOR: ", iDoor, " do not flag to lock")
						peds[x].iExitingThisRoom = -1		// reset as they are now clear of the door
					ENDIF
				
				ENDIF
			
			ENDIF
		ENDIF
	
	ENDREPEAT
	
	IF bUnlockDoor
		IF eDoorFlagL != mdf_invalid	DOORS_Set_State(eDoorFlagL, FALSE, DOORS_KEEP_CURRENT_RATIO)		ENDIF
		IF eDoorFlagR != mdf_invalid	DOORS_Set_State(eDoorFlagR, FALSE, DOORS_KEEP_CURRENT_RATIO)		ENDIF
		CPRINTLN(DEBUG_MIKE, "UNLOCKING ROOM: ", iDoor)
	ELSE
		IF eDoorFlagL != mdf_invalid	DOORS_Set_State(eDoorFlagL, TRUE, 0.0, 500)		ENDIF
		IF eDoorFlagR != mdf_invalid	DOORS_Set_State(eDoorFlagR, TRUE, 0.0, 500)		ENDIF
		CPRINTLN(DEBUG_MIKE, "LOCKING ROOM: ", iDoor)
	ENDIF
	
	mEvents[mef_manage_shootout_doors].iStage++
	IF mEvents[mef_manage_shootout_doors].iStage > 4
		mEvents[mef_manage_shootout_doors].iStage = 1
	ENDIF
	
ENDPROC

PROC me_manage_download_shootout_spawning()

	// Manage enemy counters.
	INT ePed
	FOR ePed = 0 TO NUM_DOWNLOAD_SHOOTOUT_ENEMIES-1
		
		IF s_WavePed[ePed].bSpawned
		AND NOT s_WavePed[ePed].bIsDead
			IF peds[WAVE_PED_START + ePed].id = NULL
			OR NOT DOES_ENTITY_EXIST(peds[WAVE_PED_START + ePed].id)
			OR IS_PED_INJURED(peds[WAVE_PED_START + ePed].id)
				s_WavePed[ePed].bIsDead = TRUE
				i_KillsSinceLastSpawned++
				i_EnemiesAlive--
				
			// not dead, check if in the room and turn blips on
			ELSE
				IF NOT peds[WAVE_PED_START + ePed].bAIBlipsActive
					// Check if the ped is near the skylight
					IF s_SpawnPoints[s_WavePed[ePed].eSpawnPoint].bIsRoofPoint
						IF IS_ENTITY_IN_ANGLED_AREA(peds[WAVE_PED_START + ePed].id, <<123.898270,-744.793518,261.732056>>, <<146.582077,-753.102844,265.107056>>, 18.687500)
							SET_PED_AUTO_BLIPS_ACTIVE(peds[WAVE_PED_START + ePed], TRUE)
						ENDIF
					ELSE
						IF GET_INTERIOR_FROM_ENTITY(peds[WAVE_PED_START + ePed].id) = interior_fib03
							IF peds[WAVE_PED_START + ePed].iCurrentlyInRoom = -1
								SET_PED_AUTO_BLIPS_ACTIVE(peds[WAVE_PED_START + ePed], TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	BOOL bSpawnNewPed
	INT iNewPed = mEvents[mef_manage_download_shootout_spawning].iStage - 1
	
	IF iNewPed < NUM_DOWNLOAD_SHOOTOUT_ENEMIES
	
		// Do checks to see if we should spawn the next ped
		IF s_WavePed[iNewPed].eSpawnPoint != spf_invalid
		AND (NOT s_WavePed[iNewPed].bWaitForGasGrenade OR bHasGasGrenadeGoneOff)
		AND GET_GAME_TIMER() - i_TimeLastSpawned > s_WavePed[iNewPed].iTimeSinceLast 	// minimum amount of time always needed
		AND i_EnemiesAlive < I_NUM_MAX_ENEMIES_SPAWNED									// limit to max amount of enemies
		AND (fDownloadProgress >= s_WavePed[iNewPed].fProgress							// download percentage trigger
		OR  i_KillsSinceLastSpawned >= s_WavePed[iNewPed].iKillsSinceLast)				// kills since last trigger
			bSpawnNewPed = TRUE
		ENDIF
		
		IF bSpawnNewPed
			
			IF HAS_MODEL_LOADED(s_WavePed[iNewPed].eModel)
			
				// Roof ped, put into the roof queue
				IF s_SpawnPoints[s_WavePed[iNewPed].eSpawnPoint].bIsRoofPoint
				
					INT i
					WHILE i < COUNT_OF(iRoofPedQueue)
					AND iRoofPedQueue[i] != -1
					
						i++
					ENDWHILE
					IF i != -1 AND i < COUNT_OF(iRoofPedQueue)
						iRoofPedQueue[i] = iNewPed
					ELSE
						SCRIPT_ASSERT("AGENCY3B: NO ROOM IN ROOF QUEUE!")
					ENDIF
				
				// spawn immediately as normal
				ELSE
			
					SPAWN_WAVE_PED(iNewPed)
				
				ENDIF
				
				mEvents[mef_manage_download_shootout_spawning].iStage++
			ENDIF
		
		ENDIF
		
	// All enemies spawned
	ELSE
		IF i_EnemiesAlive = 0
		AND i_LastPedKilledTimer = -1
			i_LastPedKilledTimer = GET_GAME_TIMER() + 3000
		ENDIF
	ENDIF

ENDPROC

PROC me_manage_download_shootout_rooftop()
	
	
// MANAGE MOVING THE PEDS AROUND ON THE ROOF TOP
//------------------------------------------------------------------------------------------
	CONST_INT MIN_RND_TIME 3000
	CONST_INT MAX_RND_TIME 6000
	
	BOOL bRearLeftFree, bRearRightFree, bLeftSideFree, bRightsideFree
	bRearLeftFree 	= TRUE
	bRearRightFree 	= TRUE
	bLeftSideFree 	= TRUE
	bRightsideFree 	= TRUE
	
	INT j
	FOR j = 0 TO NUM_DOWNLOAD_SHOOTOUT_ENEMIES-1
		IF DOES_ENTITY_EXIST(peds[WAVE_PED_START + j].id)
		AND NOT IS_PED_INJURED(peds[WAVE_PED_START + j].id)
		AND s_WavePed[j].ePreferredDefensiveArea != dsd_invalid
		AND s_ShootOutDefensiveAreas[s_WavePed[j].ePreferredDefensiveArea].bIsRoofPoint
			
			// move defensive areas around

			VECTOR vDefensiveCoord1, vDefensiveCoord2

			vDefensiveCoord2 = s_ShootOutDefensiveAreas[s_WavePed[j].ePreferredDefensiveArea].vCoord
			SWITCH s_WavePed[j].ePreferredDefensiveArea
				CASE dsd_roof_rear_l			
					vDefensiveCoord1 = s_ShootOutDefensiveAreas[dsd_roof_rear_mid_l].vCoord			
					bRearLeftFree = FALSE
				BREAK
				CASE dsd_roof_rear_mid_l		
					vDefensiveCoord1 = s_ShootOutDefensiveAreas[dsd_roof_rear_l].vCoord
					bRearLeftFree = FALSE
				BREAK
				CASE dsd_roof_rear_r			
					vDefensiveCoord1 = s_ShootOutDefensiveAreas[dsd_roof_rear_mid_r].vCoord			
					bRearRightFree 	= FALSE
				BREAK
				CASE dsd_roof_rear_mid_r		
					vDefensiveCoord1 = s_ShootOutDefensiveAreas[dsd_roof_rear_r].vCoord				
					bRearRightFree 	= FALSE
				BREAK
				CASE dsd_roof_rightside_front	
					vDefensiveCoord1 = s_ShootOutDefensiveAreas[dsd_roof_rightside_rear].vCoord		
					bRightsideFree 	= FALSE
				BREAK
				CASE dsd_roof_rightside_rear	
					vDefensiveCoord1 = s_ShootOutDefensiveAreas[dsd_roof_rightside_front].vCoord	
					bRightsideFree 	= FALSE
				BREAK
				CASE dsd_roof_leftside_front	
					vDefensiveCoord1 = s_ShootOutDefensiveAreas[dsd_roof_leftside_rear].vCoord		
					bLeftSideFree 	= FALSE
				BREAK
				CASE dsd_roof_leftside_rear		
					vDefensiveCoord1 = s_ShootOutDefensiveAreas[dsd_roof_leftside_front].vCoord		
					bLeftSideFree 	= FALSE
				BREAK
				DEFAULT							
					vDefensiveCoord1 = <<0,0,0>>	
				BREAK
			ENDSWITCH
			
			IF NOT IS_VECTOR_ZERO(vDefensiveCoord1)
			AND NOT IS_VECTOR_ZERO(vDefensiveCoord2)
			
				CPRINTLN(DEBUG_MIKE, "[DLS:Rooftop] Managing wavePed_", j, "'s defensive area. Stage: ", peds[WAVE_PED_START + j].iGenericFlag)
			
				SWITCH peds[WAVE_PED_START + j].iGenericFlag
					CASE 0
						peds[WAVE_PED_START + j].iGenericFlag = 1
					BREAK
					CASE 1
						IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[WAVE_PED_START + j].id), vDefensiveCoord2, 1.5)
							peds[WAVE_PED_START + j].iGenericFlag++
						ENDIF
					BREAK
					CASE 2
						IF peds[WAVE_PED_START + j].iDeadTimer = 0
							SET_PED_COMBAT_PARAMS(peds[WAVE_PED_START + j], vDefensiveCoord1, 0.75, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 1.0, FALSE, TRUE)
							peds[WAVE_PED_START + j].iDeadTimer = GET_RANDOM_INT_IN_RANGE(MIN_RND_TIME, MAX_RND_TIME)
							peds[WAVE_PED_START + j].iGenericFlag = -1
							CPRINTLN(DEBUG_MIKE, "[DLS:Rooftop] Moving wavePed_", j, "'s defensive area to ", vDefensiveCoord1)
						ELSE
							peds[WAVE_PED_START + j].iDeadTimer = CLAMP_INT(peds[WAVE_PED_START + j].iDeadTimer - ROUND(TIMESTEP()*1000), 0, 999999999)
						ENDIF
					BREAK
					CASE -1
						IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[WAVE_PED_START + j].id), vDefensiveCoord1, 1.5)
							peds[WAVE_PED_START + j].iGenericFlag--
						ENDIF
					BREAK
					CASE -2
						IF peds[WAVE_PED_START + j].iDeadTimer = 0
							SET_PED_COMBAT_PARAMS(peds[WAVE_PED_START + j], vDefensiveCoord2, 0.75, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 1.0, FALSE, TRUE)
							peds[WAVE_PED_START + j].iDeadTimer = GET_RANDOM_INT_IN_RANGE(MIN_RND_TIME, MAX_RND_TIME)
							peds[WAVE_PED_START + j].iGenericFlag = 1
							CPRINTLN(DEBUG_MIKE, "[DLS:Rooftop] Moving wavePed_", j, "'s defensive area to ", vDefensiveCoord2)
						ELSE
							peds[WAVE_PED_START + j].iDeadTimer = CLAMP_INT(peds[WAVE_PED_START + j].iDeadTimer - ROUND(TIMESTEP()*1000), 0, 999999999)
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDFOR
	
// Manage the rooftop queue
//------------------------------------------------------------------------------------------

	IF i_EnemiesAlive > 1									// hold off spawning another while there 1 or less enemies alive
	AND i_EnemiesAlive < I_NUM_MAX_ENEMIES_SPAWNED			// limit to max amount of enemies

		// Check if any of the peds in the queue can be bought in
		INT i
		REPEAT COUNT_OF(iRoofPedQueue) i
			IF iRoofPedQueue[i] != -1
			
				BOOL bReleaseFromQueue
				
				SWITCH s_WavePed[iRoofPedQueue[i]].ePreferredDefensiveArea
					CASE dsd_roof_rear_l			
					CASE dsd_roof_rear_mid_l		
						IF bRearLeftFree
							bReleaseFromQueue = TRUE
						ENDIF
					BREAK
					CASE dsd_roof_rear_r			
					CASE dsd_roof_rear_mid_r		
						IF bRearRightFree
							bReleaseFromQueue = TRUE
						ENDIF
					BREAK
					CASE dsd_roof_rightside_front	
					CASE dsd_roof_rightside_rear	
						IF bRightsideFree
							bReleaseFromQueue = TRUE
						ENDIF
					BREAK
					CASE dsd_roof_leftside_front	
					CASE dsd_roof_leftside_rear		
						IF bLeftSideFree 	
							bReleaseFromQueue = TRUE
						ENDIF
					BREAK
				ENDSWITCH
				
				// Enemy should be release from the queue and spawned
				IF bReleaseFromQueue
					SPAWN_WAVE_PED(iRoofPedQueue[i])
					iRoofPedQueue[i] = -1
				ENDIF

			ENDIF
		ENDREPEAT
	
	ENDIF

ENDPROC

PROC me_kill_sprinklers()

	IF IS_CUTSCENE_PLAYING()
		Event_Pause(mef_kill_sprinklers)
	ELSE
		Event_Resume(mef_kill_sprinklers)

		SWITCH mEvents[mef_kill_sprinklers].iStage
			CASE 1
				bPlayingKeyDialogue = TRUE
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_SPKa", CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
						mEvents[mef_kill_sprinklers].iStage++
					ENDIF
				ENDIF
			BREAK
			CASE 2
				// Select the conversation to play
				IF IS_STRING_NULL_OR_EMPTY(str_dialogue_root)
					SWITCH crewHacker
						CASE CM_HACKER_B_RICKIE_UNLOCK
							SWITCH GET_CREW_MEMBER_SKILL(crewHacker)
								CASE CMSK_BAD
									str_dialogue_root 			= APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_SPKb", TRUE)
									bWillDisableSprinklers 		= FALSE
								BREAK
								CASE CMSK_GOOD
									str_dialogue_root 			= APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_SPKb", TRUE)
									bWillDisableSprinklers 		= TRUE
								BREAK
								CASE CMSK_MEDIUM
									str_dialogue_root 			= APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_SPKb")
									IF GET_RANDOM_BOOL()
										str_dialogue_root 		+= "M"
										bWillDisableSprinklers 	= FALSE
									ELSE
										str_dialogue_root 		+= "G"
										bWillDisableSprinklers 	= TRUE
									ENDIF
								BREAK
							ENDSWITCH
						BREAK
						CASE CM_HACKER_M_CHRIS
							SWITCH GET_CREW_MEMBER_SKILL(crewHacker)
								CASE CMSK_BAD
									str_dialogue_root 			= APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_SPKb", TRUE)
									bWillDisableSprinklers 		= FALSE
								BREAK
								CASE CMSK_GOOD
									str_dialogue_root 			= APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_SPKb", TRUE)
									bWillDisableSprinklers 		= TRUE
								BREAK
								CASE CMSK_MEDIUM
									str_dialogue_root 			= APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_SPKb")
									IF GET_RANDOM_BOOL()
										str_dialogue_root 		+= "B"
										bWillDisableSprinklers 	= FALSE
									ELSE
										str_dialogue_root 		+= "M"
										bWillDisableSprinklers 	= TRUE
									ENDIF
								BREAK
							ENDSWITCH
						BREAK
						CASE CM_HACKER_G_PAIGE
							str_dialogue_root 					= APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_SPKb")
							bWillDisableSprinklers 				= TRUE
						BREAK
					ENDSWITCH
				ENDIF
				
				// Play the selected conversation
				IF NOT IS_STRING_NULL_OR_EMPTY(str_dialogue_root)
					IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("V_FIB03_IT3_cor1")
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							KILL_FACE_TO_FACE_CONVERSATION()
						ENDIF
						str_dialogue_root = ""
						mEvents[mef_kill_sprinklers].iStage++
					ELSE
						IF IS_SAFE_TO_START_CONVERSATION()
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								IF bWillDisableSprinklers
									iDesiredSprinklerState = 0
								ENDIF
								mEvents[mef_kill_sprinklers].iStage++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 3
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("V_FIB03_IT3_cor1")
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
				ENDIF
				
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					bPlayingKeyDialogue = FALSE
					g_replay.iReplayInt[0] = iDesiredSprinklerState+1	// store the sprinkler state
					Event_Finish(mef_kill_sprinklers)
				ENDIF
			BREAK
		ENDSWITCH
	
	ENDIF
ENDPROC



// SWAT comes storming in behind the smoke
PROC me_gas_grenades()
	SWITCH mEvents[mef_gas_grenades].iStage
		CASE 1
			Load_Asset_Model(sAssetData, mod_swat)
			Load_Asset_Model(sAssetData, GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_GRENADE_LAUNCHER_SMOKE))
			Load_Asset_Weapon_Asset(sAssetData, WEAPONTYPE_GRENADELAUNCHER_SMOKE)
			Load_Asset_AnimDict(sAssetData, "MISSHeistFBI3Bsmokegrenade")
			
			IF HAS_MODEL_LOADED(mod_swat)
			AND HAS_MODEL_LOADED(PROP_LD_TEST_01)
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_GRENADELAUNCHER_SMOKE)
			AND HAS_ANIM_DICT_LOADED("MISSHeistFBI3Bsmokegrenade")
				
				CREATE_MISSION_PED(peds[mpf_gas_roof_1], mod_swat, s_SpawnPoints[spf_roof_front_right].vCoord,  s_SpawnPoints[spf_roof_front_right].fHeading, "GAS_1", REL_ENEMY, WEAPONTYPE_GRENADELAUNCHER_SMOKE, WEAPONCOMPONENT_INVALID, 100, 130)
				SET_PED_AUTO_BLIPS_ACTIVE(peds[mpf_gas_roof_1], FALSE)

				mEvents[mef_gas_grenades].iStage++
			ENDIF
		BREAK
		CASE 2
//			IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_SMOKE_GRENADE_LAUNCHER, <<136.043762,-749.235352,257.152191>>, 25.0)
			IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_gas_grenade])
				mEvents[mef_gas_grenades].iStage++
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_gas_grenade])
			OR GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_gas_grenade]) = 1.0
				STOP_SYNCHRONIZED_ENTITY_ANIM(objs[mof_gas_grenade].id, INSTANT_BLEND_OUT, FALSE)
				FREEZE_ENTITY_POSITION(objs[mof_gas_grenade].id, TRUE)
				ADD_EXPLOSION(GET_ENTITY_COORDS(objs[mof_gas_grenade].id), EXP_TAG_SMOKE_GRENADE_LAUNCHER, 1.0, TRUE, FALSE)
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxs[ptfx_gas_grenade_trail])
					STOP_PARTICLE_FX_LOOPED(ptfxs[ptfx_gas_grenade_trail])
				ENDIF
				bHasGasGrenadeGoneOff = TRUE
				mEvents[mef_gas_grenades].iStage++
			ENDIF
		BREAK
	ENDSWITCH
	
	// Gas Ped 1
	IF mEvents[mef_gas_grenades].iStage > 1
		IF DOES_ENTITY_EXIST(peds[mpf_gas_roof_1].id)
		AND NOT IS_PED_INJURED(peds[mpf_gas_roof_1].id)
		
			IF peds[mpf_gas_roof_1].iGenericFlag < 2
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_gas_roof_1].id, SCRIPT_TASK_PERFORM_SEQUENCE)
					SAFE_OPEN_SEQUENCE()
						TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<145.0271, -751.9438, 261.8503>>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 0.0, ENAV_STOP_EXACTLY, 72.9996)
						TASK_AIM_GUN_AT_COORD(null, <<138.8614, -749.8088, 262.3764>>, 1000, FALSE)
						TASK_AIM_GUN_AT_COORD(null, <<138.8614, -749.8088, 262.3764>>, -1, TRUE)
					SAFE_PERFORM_SEQUENCE(peds[mpf_gas_roof_1].id)
				ENDIF
			ENDIF
			
		
			SWITCH peds[mpf_gas_roof_1].iGenericFlag
				CASE 0
					IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_gas_roof_1].id, SCRIPT_TASK_PERFORM_SEQUENCE)
						IF GET_SEQUENCE_PROGRESS(peds[mpf_gas_roof_1].id) = 1
							peds[mpf_gas_roof_1].iGenericFlag++
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF NOT DOES_ENTITY_EXIST(objs[mof_glass_smoke_1].id)
						CPRINTLN(DEBUG_MIKE, "NOT YET GRABBED WINDOW")
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< 141.68, -751.91, 263.08 >>, 0.5, V_ILEV_FIB_ATRGL2)
							objs[mof_glass_smoke_1].id = GET_CLOSEST_OBJECT_OF_TYPE(<< 141.68, -751.91, 263.08>>, 0.5, V_ILEV_FIB_ATRGL2, TRUE)
							CPRINTLN(DEBUG_MIKE, "HAS NOW FOUND WINDOW")
						ELSE
							CPRINTLN(DEBUG_MIKE, "FAILED TO FIND WINDOW PANE")
						ENDIF
					ENDIF
				
					IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_gas_roof_1].id, SCRIPT_TASK_PERFORM_SEQUENCE)
						IF GET_SEQUENCE_PROGRESS(peds[mpf_gas_roof_1].id) = 2
							BREAK_ENTITY_GLASS(objs[mof_glass_smoke_1].id, <<141.68, -751.91, 263.08>>, 0.5, <<0,0,-1.0>> * 1.0, 700, 3, FALSE)
							SET_OBJECT_AS_NO_LONGER_NEEDED(objs[mof_glass_smoke_1].id)
							
							objs[mof_gas_grenade].id = CREATE_OBJECT(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_GRENADE_LAUNCHER_SMOKE), GET_PED_BONE_COORDS(peds[mpf_gas_roof_1].id, BONETAG_PH_L_HAND, <<0,0,0>>))
							ptfxs[ptfx_gas_grenade_trail] = START_PARTICLE_FX_LOOPED_ON_ENTITY("proj_grenade_trail", objs[mof_gas_grenade].id, <<0,0,0>>, <<0,0,0>>, 1.0)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_GRENADE_LAUNCHER_SMOKE))
							syncedScenes[ssf_gas_grenade] = CREATE_SYNCHRONIZED_SCENE(<< 138.201, -746.476, 258.810 >>, << 0.000, 0.000, -124.560 >>)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(objs[mof_gas_grenade].id, syncedScenes[ssf_gas_grenade], "SmokeGrenade", "MISSHeistFBI3Bsmokegrenade", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, enum_to_int(AF_EXTRACT_INITIAL_OFFSET | AF_OVERRIDE_PHYSICS))
							
							//SET_PED_SHOOTS_AT_COORD(peds[mpf_gas_roof_1].id, <<138.8614, -749.8088, 262.3764>>, TRUE) 
							peds[mpf_gas_roof_1].iDeadTimer = GET_GAME_TIMER() + 1000
							peds[mpf_gas_roof_1].iGenericFlag++
						ENDIF
					ENDIF
				BREAK
				CASE 2
					IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_gas_roof_1].id), <<157.2785, -758.9954, 261.8044>>, 2.0)
						DELETE_PED(peds[mpf_gas_roof_1].id)
						peds[mpf_gas_roof_1].iGenericFlag++
					ELSE
						IF GET_GAME_TIMER() > peds[mpf_gas_roof_1].iDeadTimer
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_gas_roof_1].id, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
								TASK_FOLLOW_NAV_MESH_TO_COORD(peds[mpf_gas_roof_1].id, <<157.2785, -758.9954, 261.8044>>, PEDMOVE_RUN)
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		ELSE
			bHasGasGrenadeGoneOff = TRUE
			Event_Finish(mef_gas_grenades)
		ENDIF
	ENDIF
	
	IF bHasGasGrenadeGoneOff
		IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_SHOOTOUT_SMOKE_BOMBS")
			START_AUDIO_SCENE("AH_3B_SHOOTOUT_SMOKE_BOMBS")
		ENDIF
	ENDIF

ENDPROC

PROC me_post_heli_crash_enemies()

	SWITCH mEvents[mef_post_heli_crash_enemies].iStage
		CASE 1
			Load_Asset_Model(sAssetData, mod_fib_agent_1)
			Load_Asset_Model(sAssetData, mod_fib_agent_2)
			Load_Asset_Model(sAssetData, mod_fib_agent_3)
		
			IF HAS_MODEL_LOADED(mod_fib_agent_1)
			AND HAS_MODEL_LOADED(mod_fib_agent_2)
			AND HAS_MODEL_LOADED(mod_fib_agent_3)
				CREATE_MISSION_PED(peds[mpf_post_heli_crash_1], mod_fib_agent_1, <<136.7558, -754.1835, 257.1695>>, 249.1905, "wave_3_1", REL_ENEMY, WEAPONTYPE_PISTOL, 	WEAPONCOMPONENT_INVALID, 5)
				CREATE_MISSION_PED(peds[mpf_post_heli_crash_2], mod_fib_agent_2, <<129.3333, -750.0584, 257.1696>>, 250.7705, "wave_3_2", REL_ENEMY, WEAPONTYPE_SMG,		WEAPONCOMPONENT_INVALID, 5)
				CREATE_MISSION_PED(peds[mpf_post_heli_crash_3], mod_fib_agent_3, <<139.3069, -747.4471, 257.1696>>, 249.2577, "wave_3_3", REL_ENEMY, WEAPONTYPE_PISTOL, 	WEAPONCOMPONENT_INVALID, 5)
				CREATE_MISSION_PED(peds[mpf_post_heli_crash_4], mod_fib_agent_1, <<115.1057, -737.9293, 257.1522>>, 240.4648, "wave_3_4", REL_ENEMY, WEAPONTYPE_PISTOL, 	WEAPONCOMPONENT_INVALID, 5)
				
				SAFE_CREATE_COVERPOINT(peds[mpf_post_heli_crash_1].cov, <<136.7558, -754.1835, 257.1695>>, 249.1905, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
				SAFE_CREATE_COVERPOINT(peds[mpf_post_heli_crash_3].cov, <<139.3069, -747.4471, 257.1696>>, 249.2577, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)
				
				SAFE_OPEN_SEQUENCE()
					TASK_PUT_PED_DIRECTLY_INTO_COVER(null, <<136.7558, -754.1835, 257.1695>>, 500, FALSE, 0, TRUE, FALSE, peds[mpf_post_heli_crash_1].cov, FALSE)
					TASK_COMBAT_HATED_TARGETS_IN_AREA(null, <<135.771698,-749.659546,257.152191>>, 32.0)
				SAFE_PERFORM_SEQUENCE(peds[mpf_post_heli_crash_1].id)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_post_heli_crash_1].id)
				
				TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_post_heli_crash_2].id, <<135.771698,-749.659546,257.152191>>, 32.0)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_post_heli_crash_2].id)
				
				SAFE_OPEN_SEQUENCE()
					TASK_PUT_PED_DIRECTLY_INTO_COVER(null, <<139.3069, -747.4471, 257.1696>>, 750, FALSE, 0, TRUE, FALSE, peds[mpf_post_heli_crash_3].cov, FALSE)
					TASK_COMBAT_HATED_TARGETS_IN_AREA(null, <<135.771698,-749.659546,257.152191>>, 32.0)
				SAFE_PERFORM_SEQUENCE(peds[mpf_post_heli_crash_3].id)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_post_heli_crash_3].id)
				
				SAFE_OPEN_SEQUENCE()
					TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, <<123.2613, -744.4315, 257.1521>>, MIKE_PED_ID(), PEDMOVE_RUN, TRUE, 0.5, 0.5, TRUE, ENAV_DEFAULT, TRUE)
					TASK_COMBAT_HATED_TARGETS_IN_AREA(null, <<135.771698,-749.659546,257.152191>>, 32.0)
				SAFE_PERFORM_SEQUENCE(peds[mpf_post_heli_crash_4].id)
				FORCE_PED_MOTION_STATE(peds[mpf_post_heli_crash_4].id, MS_ON_FOOT_RUN)
				
				i_EnemiesAlive += 4
				
				Event_Next_Stage(mef_post_heli_crash_enemies)
			ENDIF
		BREAK
		CASE 2
			IF peds[mpf_post_heli_crash_1].iGenericFlag != -1
			AND (NOT DOES_ENTITY_EXIST(peds[mpf_post_heli_crash_1].id)
			OR IS_PED_INJURED(peds[mpf_post_heli_crash_1].id))
				peds[mpf_post_heli_crash_1].iGenericFlag = -1
				#IF IS_DEBUG_BUILD
					debug_i_EnemiesManuallyCounted++
				#ENDIF
				i_EnemiesAlive--
			ENDIF
			
			IF peds[mpf_post_heli_crash_2].iGenericFlag != -1
			AND (NOT DOES_ENTITY_EXIST(peds[mpf_post_heli_crash_2].id)
			OR IS_PED_INJURED(peds[mpf_post_heli_crash_2].id))
				peds[mpf_post_heli_crash_2].iGenericFlag = -1
				#IF IS_DEBUG_BUILD
					debug_i_EnemiesManuallyCounted++
				#ENDIF
				i_EnemiesAlive--
			ENDIF
			
			IF peds[mpf_post_heli_crash_3].iGenericFlag != -1
			AND (NOT DOES_ENTITY_EXIST(peds[mpf_post_heli_crash_3].id)
			OR IS_PED_INJURED(peds[mpf_post_heli_crash_3].id))
				peds[mpf_post_heli_crash_3].iGenericFlag = -1
				#IF IS_DEBUG_BUILD
					debug_i_EnemiesManuallyCounted++
				#ENDIF
				i_EnemiesAlive--
			ENDIF
			
			IF peds[mpf_post_heli_crash_4].iGenericFlag != -1
			AND (NOT DOES_ENTITY_EXIST(peds[mpf_post_heli_crash_4].id)
			OR IS_PED_INJURED(peds[mpf_post_heli_crash_4].id))
				peds[mpf_post_heli_crash_4].iGenericFlag = -1
				#IF IS_DEBUG_BUILD
					debug_i_EnemiesManuallyCounted++
				#ENDIF
				i_EnemiesAlive--
			ENDIF
			
			IF peds[mpf_post_heli_crash_1].iGenericFlag = -1
			AND peds[mpf_post_heli_crash_2].iGenericFlag = -1
			AND peds[mpf_post_heli_crash_3].iGenericFlag = -1
			AND peds[mpf_post_heli_crash_4].iGenericFlag = -1
				Event_Finish(mef_post_heli_crash_enemies)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC me_ig2_try_door()

	IF mission_substage >= 3
		mEvents[mef_ig2_try_door].iStage = 6
	ENDIF

	SWITCH mEvents[mef_ig2_try_door].iStage
		CASE 1
			IF GET_IS_WAYPOINT_RECORDING_LOADED(wp_try_door)
				USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE(wp_try_door, TRUE, 1, 0.5)
				Event_Next_Stage(mef_ig2_try_door)
			ENDIF
		BREAK
		CASE 2
			IF HAS_ANIM_DICT_LOADED(animDict_IG2_try_door)
			AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_STICKYBOMB)
				VECTOR vStartCoord
				vStartCoord = GET_ANIM_INITIAL_OFFSET_POSITION(animDict_IG2_try_door, anim_try_door, v_ig2_coord, v_ig2_rot, 0.0)

				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(MIKE_PED_ID()), vStartCoord) <= 0.5
				
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
					
					DISABLE_CELLPHONE(TRUE)
				
					// stop the assisted line
					USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE(wp_try_door, FALSE, 1, 0.5)
				
					// give the ped the c4 ready but hide it for now
					GIVE_WEAPON_TO_PED(MIKE_PED_ID(), WEAPONTYPE_STICKYBOMB, 1, TRUE, TRUE)
					SET_PED_AMMO(MIKE_PED_ID(), WEAPONTYPE_STICKYBOMB, 3)
					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(MIKE_PED_ID(), TRUE)
					
					// start the anim
					syncedScenes[ssf_IG2_try_door] = CREATE_SYNCHRONIZED_SCENE(v_ig2_coord, v_ig2_rot)
					TASK_SYNCHRONIZED_SCENE(MIKE_PED_ID(), syncedScenes[ssf_IG2_try_door], animDict_IG2_try_door, anim_try_door, WALK_BLEND_IN, WALK_BLEND_OUT, SYNCED_SCENE_NONE, RBF_NONE, WALK_BLEND_IN)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(syncedScenes[ssf_IG2_try_door], FALSE)
				
					Event_Next_Stage(mef_ig2_try_door)
				ENDIF
			ENDIF
		BREAK
		CASE 3
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
			IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG2_try_door])
				IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG2_try_door]) >= 0.596
					IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_TRIES", CONV_PRIORITY_HIGH)
						TRIGGER_MUSIC_EVENT("AH3B_LOCKED_DOOR")
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_STICKYBOMB, TRUE)
						HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(MIKE_PED_ID(), FALSE)
						Event_Next_Stage(mef_ig2_try_door)
					ENDIF
				ELSE
					HIDE_HUD_AND_RADAR_THIS_FRAME()
				ENDIF
			ENDIF
		BREAK
		CASE 4
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(MIKE_PED_ID(), SCRIPT_TASK_PLAY_ANIM)
				DISABLE_CELLPHONE(FALSE)
				Event_Next_Stage(mef_ig2_try_door, 10000)
			ELSE
				IF HAS_ANIM_EVENT_FIRED(MIKE_PED_ID(), GET_HASH_KEY("Interrupt"))
				AND (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_LR) OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_UD)
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK2))
					CLEAR_PED_TASKS(MIKE_PED_ID())
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF IS_SAFE_TO_START_CONVERSATION()
			AND mission_substage <= 3
				PRINT_HELP_FOREVER("A3B_ACCHLP")
				Event_Next_Stage(mef_ig2_try_door)
			ENDIF
		BREAK
		CASE 6
			IF mission_substage >= 3
			
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("A3B_ACCHLP")
					CLEAR_HELP()
				ENDIF
				
				Unload_Asset_Anim_Dict(sAssetData, animDict_IG2_try_door)
				Unload_Asset_Waypoint(sAssetData, wp_try_door)
				Unload_Asset_Weapon_Asset(sAssetData, WEAPONTYPE_STICKYBOMB)
			
				Event_Finish(mef_ig2_try_door)
			ENDIF
		BREAK
	ENDSWITCh
ENDPROC

PROC me_ig3_swat_takedown()

	FLOAT fDoorOpenRatio
	BOOL bDoorLockState
	VECTOR vStartCoord
	VECTOR vExactCoord
	TEXT_LABEL_15 strDiaTemp

	SWITCH mEvents[mef_ig3_swat_takedown].iStage
		CASE 1
			Load_Asset_Model(sAssetData, mod_swat)
			Load_Asset_Waypoint(sAssetData, wp_sw_0_frank)
			Load_Asset_Waypoint(sAssetData, wp_sw_0_goon)
			Load_Asset_AnimDict(sAssetData, animDict_IG3_kill_swat)
			
			peds[mpf_stair_1].iGenericFlag = -1
			peds[mpf_stair_2].iGenericFlag = -1
			
			IF GET_IS_WAYPOINT_RECORDING_LOADED(wp_sw_0_frank)
			AND GET_IS_WAYPOINT_RECORDING_LOADED(wp_sw_0_goon)
			AND HAS_ANIM_DICT_LOADED(animDict_IG3_kill_swat)
				
			// Franklin
				REMOVE_PED_DEFENSIVE_AREA(FRANK_PED_ID())
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(FRANK_PED_ID(), TRUE)
				
				SET_CURRENT_PED_WEAPON(FRANK_PED_ID(), WEAPONTYPE_CARBINERIFLE, TRUE)
				
				IF WAYPOINT_RECORDING_GET_COORD(wp_sw_0_frank, 4, vStartCoord)
					SET_ENTITY_COORDS(FRANK_PED_ID(), vStartCoord)
					SET_ENTITY_HEADING(FRANK_PED_ID(), 73.9792)
				ENDIF
				
				vGoToExactlyBuddy = GET_ACTUAL_GROUND_COORD(GET_ANIM_INITIAL_OFFSET_POSITION(animDict_IG3_kill_swat, anim_killswat_frank, v_ig3_coord, v_ig3_rot, 0))
				
				SAFE_OPEN_SEQUENCE()
					TASK_FOLLOW_WAYPOINT_RECORDING(null, wp_sw_0_frank, 5)
					TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, vGoToExactlyBuddy, << 118.2876, -734.7114, 258.5561 >>, PEDMOVE_WALK, FALSE, 0.0, 4.0, TRUE, ENAV_STOP_EXACTLY)
					TASK_AIM_GUN_AT_COORD(null, << 118.2876, -734.7114, 258.5561 >>, -1, FALSE)
				SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
				i_SeqLengthFrank = 3
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(FRANK_PED_ID())
				
			 // Gunman
			 	REMOVE_PED_DEFENSIVE_AREA(peds[mpf_goon].id)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_goon].id, TRUE)
				
				SET_CURRENT_PED_WEAPON(peds[mpf_goon].id, weap_Goon, TRUE)
				
				SET_ENTITY_COORDS(peds[mpf_goon].id, <<146.7726, -756.3869, 257.1695>>)
				SET_ENTITY_HEADING(peds[mpf_goon].id, 68.9933)
				
				RESET_COVERPOINT_PAIR(covPair_gunman)

				// End cover point
				vGoToExactlyGunman = GET_ANIM_INITIAL_OFFSET_POSITION(animDict_IG3_kill_swat, anim_killswat_goon, v_ig3_coord, v_ig3_rot, 0)
				vGoToExactlyGunman.z = 257.1523
				CREATE_COVERPOINT_PAIR(covPair_gunman, <<146.7726, -756.3869, 257.1695>>, 68.9933, COVUSE_WALLTOLEFT, COVHEIGHT_LOW, COVARC_120)	// Starting cover point
				CREATE_COVERPOINT_PAIR(covPair_gunman, vGoToExactlyGunman, 336.6323, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180)				// go to cover point				
				
				SAFE_OPEN_SEQUENCE()
					TASK_PUT_PED_DIRECTLY_INTO_COVER(null, <<146.7726, -756.3869, 257.1695>>, 1000, FALSE, 0, TRUE, FALSE, covPair_gunman.covID[1-covPair_gunman.id], FALSE)
					TASK_EXIT_COVER(null, CORNER_COVER_EXIT, <<0,0,0>>)
					TASK_FOLLOW_WAYPOINT_RECORDING(null, wp_sw_0_goon, 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
					TASK_SEEK_COVER_TO_COVER_POINT(null, covPair_gunman.covID[covPair_gunman.id], << 120.4668, -731.2372, 257.1523 >>, -1, FALSE)
				SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
				peds[mpf_goon].iGenericFlag = GET_GAME_TIMER()
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_goon].id)
			
				bGunmanReadyToMoveOn	= FALSE
				bBuddyReadyToMoveOn 	= FALSE
				Event_Next_Stage(mef_ig3_swat_takedown)				
			ENDIF
		BREAK
		CASE 2
		// BUDDY			
			// get him to the right location
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
				SAFE_OPEN_SEQUENCE()
					i_SeqLengthFrank = 2
					IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ACTUAL_GROUND_COORD(GET_ENTITY_COORDS(BUDDY_PED_ID())), vGoToExactlyBuddy, 3.0)
						TASK_FOLLOW_WAYPOINT_RECORDING(null, wp_sw_0_frank, 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
						i_SeqLengthFrank++
					ENDIF
					TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, vGoToExactlyBuddy, << 118.2876, -734.7114, 258.5561 >>, PEDMOVE_WALK, FALSE, 0.0, 4.0, FALSE, ENAV_STOP_EXACTLY | ENAV_NO_STOPPING)
					TASK_AIM_GUN_AT_COORD(null, << 118.2876, -734.7114, 258.5561 >>, -1, TRUE)
				SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
			ELSE
				IF (i_SeqLengthFrank = 2 AND GET_SEQUENCE_PROGRESS(BUDDY_PED_ID()) = 1)
				OR (i_SeqLengthFrank = 3 AND GET_SEQUENCE_PROGRESS(BUDDY_PED_ID()) = 2)
					bBuddyReadyToMoveOn = TRUE
				ELSE
					bBuddyReadyToMoveOn = FALSE
				ENDIF
			ENDIF
			
		// GUNMAN
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_PERFORM_SEQUENCE)
			AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
				SAFE_OPEN_SEQUENCE()
					IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ACTUAL_GROUND_COORD(GET_ENTITY_COORDS(peds[mpf_goon].id)), vGoToExactlyGunman, 3.0)
						TASK_FOLLOW_WAYPOINT_RECORDING(null, wp_sw_0_goon, 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
					ENDIF
					TASK_SEEK_COVER_TO_COVER_POINT(null, covPair_gunman.covID[covPair_gunman.id], << 120.4668, -731.2372, 257.1523 >>, -1, FALSE)
				SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
				
			ELSE
				IF IS_PED_IN_COVER(peds[mpf_goon].id)
				AND IS_PED_IN_COVER_FACING_LEFT(peds[mpf_goon].id)
					IF NOT bGunmanReadyToMoveOn
						bGunmanReadyToMoveOn = TRUE
						mEvents[mef_ig3_swat_takedown].iTimeStamp = GET_GAME_TIMER() + 500
					ENDIF
				ELSE
					// bug 1895554
					
					IF GET_GAME_TIMER() - peds[mpf_goon].iGenericFlag > 30000
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
							TASK_SEEK_COVER_TO_COVER_POINT(peds[mpf_goon].id, covPair_gunman.covID[covPair_gunman.id], << 120.4668, -731.2372, 257.1523 >>, -1, FALSE)
							peds[mpf_goon].iGenericFlag = GET_GAME_TIMER()
						ENDIF
					ENDIF
				
					bGunmanReadyToMoveOn = FALSE
				ENDIF
			ENDIF
			
			// Ready to start synced scene?
			IF bBuddyReadyToMoveOn AND bGunmanReadyToMoveOn
			AND GET_GAME_TIMER() > mEvents[mef_ig3_swat_takedown].iTimeStamp
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<116.460449,-735.920898,257.152313>>, <<118.937820,-736.834229,259.402313>>, 1.375000)
			AND swState = SWITCH_NOSTATE
			
				IF IS_SAFE_TO_START_CONVERSATION()
			
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<117.209557,-737.216248,257.152435>>, FALSE) <= 11.0
					
						IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_BREACH1", CONV_PRIORITY_HIGH)
						
							vExactCoord = GET_ACTUAL_GROUND_COORD(GET_ANIM_INITIAL_OFFSET_POSITION(animDict_IG3_kill_swat, anim_killswat_goon, v_ig3_coord, v_ig3_rot, 1.0))
							SET_PED_COMBAT_PARAMS(peds[mpf_goon], vExactCoord, 1.0, CM_DEFENSIVE)
							CREATE_COVERPOINT_PAIR(covPair_gunman, vExactCoord, 251.0905, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180)

							// Spawn enemies
							CREATE_MISSION_PED(peds[mpf_stair_1], mod_swat, << 125.5959, -728.5275, 257.1522 >>, 72.9956, "stair_1", REL_ENEMY, WEAPONTYPE_PUMPSHOTGUN, WEAPONCOMPONENT_AT_AR_FLSH, 100, 110)
							STOP_PED_WEAPON_FIRING_WHEN_DROPPED(peds[mpf_stair_1].id)
							REMOVE_PED_FOR_DIALOGUE(sConvo, 6)
							ADD_PED_FOR_DIALOGUE(sConvo, 6, peds[mpf_stair_1].id, "SWAT3")
							peds[mpf_stair_1].iGenericFlag = 0
							
							CREATE_MISSION_PED(peds[mpf_stair_2], mod_swat, << 128.9331, -731.0729, 257.1522 >>, 64.3427, "stair_2", REL_ENEMY, WEAPONTYPE_SMG, WEAPONCOMPONENT_AT_AR_FLSH, 2, 110)
							SET_PED_FLASH_LIGHT(peds[mpf_stair_2], FALSE)
							
							// kick off synced scene IG3 
							syncedScenes[ssf_IG3_killswat] = CREATE_SYNCHRONIZED_SCENE(v_ig3_coord, v_ig3_rot)
							TASK_SYNCHRONIZED_SCENE(BUDDY_PED_ID(), 		syncedScenes[ssf_IG3_killswat], animDict_IG3_kill_swat, anim_killswat_frank,  	SLOW_BLEND_IN, SLOW_BLEND_OUT,			SYNCED_SCENE_USE_PHYSICS, 											RBF_NONE, SLOW_BLEND_IN)
							TASK_SYNCHRONIZED_SCENE(peds[mpf_goon].id, 		syncedScenes[ssf_IG3_killswat], animDict_IG3_kill_swat, anim_killswat_goon, 	SLOW_BLEND_IN, SLOW_BLEND_OUT,			SYNCED_SCENE_USE_PHYSICS,											RBF_NONE, SLOW_BLEND_IN)
							TASK_SYNCHRONIZED_SCENE(peds[mpf_stair_1].id, 	syncedScenes[ssf_IG3_killswat], animDict_IG3_kill_swat, anim_killswat_swat, 	INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 	SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE, 	RBF_PLAYER_IMPACT)
							
							REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							
							SAFE_BLIP_COORD(blip_Objective, << 128.4890, -731.9869, 253.1524 >>)
					
							KILL_FACE_TO_FACE_CONVERSATION()
							Unload_Asset_Waypoint(sAssetData, wp_sw_0_frank)
							Unload_Asset_Waypoint(sAssetData, wp_sw_0_goon)
							Event_Next_Stage(mef_ig3_swat_takedown)
						ENDIF
					ELSE
						IF GET_GAME_TIMER() - i_time_of_last_convo > 7500
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, TRUE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		BREAK
		CASE 3
		
			// If the ped has already been killed jump ahead to stage 6
			// let play out if already past the bit where franklin shoots the guy
			IF peds[mpf_stair_1].iGenericFlag != 7
				IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG3_killswat])
				AND GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG3_killswat]) <= 0.812
				AND (IS_PED_INJURED(peds[mpf_stair_1].id)
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(peds[mpf_stair_1].id, PLAYER_PED_ID()))
					IF peds[mpf_stair_1].iGenericFlag != 602
						peds[mpf_stair_1].iGenericFlag = 602
					ENDIF
					
					IF peds[mpf_stair_2].iGenericFlag < 0
						peds[mpf_stair_2].iGenericFlag = 0
					ENDIF
				ENDIF
			ENDIF
			
			SWITCH peds[mpf_stair_1].iGenericFlag
				CASE 0
					SET_PED_CAPSULE(FRANK_PED_ID(), 0.4)
					IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG3_killswat]) >= 0.110
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_agency3b_wood_splinter", <<117.91, -735.82, 258.18>>, <<0,0,0>>, 1.0)
						DOORS_Set_State(mdf_topfloor_main_l, TRUE, 1.0, 500) 
						DOORS_Set_State(mdf_topfloor_main_r, TRUE, -1.0, 400)
//						DOORS_Set_State(mdf_topfloor_main_l, FALSE, 0.0, 0) 
//						DOORS_Set_State(mdf_topfloor_main_r, FALSE, 0.0, 0)
						peds[mpf_stair_1].iGenericFlag++
					ENDIF
				BREAK
				CASE 1
					SET_PED_CAPSULE(FRANK_PED_ID(), 0.4)
					// Swat breaches the door with shotgun
					IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG3_killswat]) >= 0.236
						SET_PED_SHOOTS_AT_COORD(peds[mpf_stair_1].id, << 123.6343, -728.3350, 258.2463 >>)
						TRIGGER_MUSIC_EVENT("AH3B_STAIRWELL")
						SET_PED_AUTO_BLIPS_ACTIVE(peds[mpf_stair_1], TRUE)
						
						DOORS_Set_State(mdf_topfloor_pre_stairs, FALSE, -0.15, 1000)
						peds[mpf_stair_1].iGenericFlag = 2
					ENDIF
				BREAK
				CASE 2
					// Swat kicks the door fully open
					IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG3_killswat]) >= 0.275
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						DOORS_Set_State(mdf_topfloor_pre_stairs, FALSE, -1.0, 600)
						peds[mpf_stair_1].iGenericFlag++
						
					ELSE HAS_BULLET_IMPACTED_IN_AREA(<<123.59, -728.47, 258.19>>, 0.3, FALSE)
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_agency3b_wood_splinter", <<123.59, -728.47, 258.19>>, <<0,0,0>>, 1.0)
					ENDIF
				BREAK
				CASE 3
					IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_SWAT1", CONV_PRIORITY_HIGH)	
						peds[mpf_stair_1].iGenericFlag ++
					ENDIF
				BREAK
				CASE 4
					// Swat guy drops weapon
					IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG3_killswat]) >= 0.373
//						OBJECT_INDEX objWeap
//						objWeap = GET_WEAPON_OBJECT_FROM_PED(peds[mpf_stair_1].id)
//						SET_OBJECT_AS_NO_LONGER_NEEDED(objWeap)
//						SET_PED_DROPS_WEAPONS_WHEN_DEAD(peds[mpf_stair_1].id, FALSE)
//						SET_CURRENT_PED_WEAPON(peds[mpf_stair_1].id, WEAPONTYPE_UNARMED, TRUE)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						SET_PED_DROPS_WEAPON(peds[mpf_stair_1].id)
						peds[mpf_stair_2].iGenericFlag = 0	// start off second ped moving in and the door closing
						peds[mpf_stair_1].iGenericFlag++
					ENDIF
				BREAK
				CASE 5
					IF IS_SAFE_TO_START_CONVERSATION()
						strDiaTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewGoon, "AH_GUNHIT")
						IF CREATE_CONVERSATION(sConvo, strDialogue, strDiaTemp, CONV_PRIORITY_HIGH)
							peds[mpf_stair_1].iGenericFlag = 501
						ENDIF
					ENDIF
				BREAK
				CASE 501
					// Frank executes swat guy
					IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG3_killswat]) >= 0.789
						IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG3_killswat]) <= 0.8
							SET_PED_ACCURACY(BUDDY_PED_ID(), 100)
							SET_PED_SHOOTS_AT_COORD(BUDDY_PED_ID(), << 121.4192, -726.5958, 257.3294 >>)
						ELSE
							SET_PED_ACCURACY(BUDDY_PED_ID(), 10)
							SET_ENTITY_HEALTH(peds[mpf_stair_1].id, 0)
							peds[mpf_stair_1].iGenericFlag = 502
						ENDIF
					ELSE
						IF i_dialogue_ig3_frank_count < 2
						AND IS_SAFE_TO_START_CONVERSATION()
							IF BUDDY_PED_ID() = MIKE_PED_ID()
								str_dialogue_root = "AH_SHOOT_M"
							ELSE
								str_dialogue_root = "AH_SHOOT_F"
							ENDIF
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_ig3_frank_count++	
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 502
					IF IS_SAFE_TO_START_CONVERSATION()
						IF PLAYER_PED_ID() = MIKE_PED_ID()
							str_dialogue_root = "AH_MICHSHOOT"
						ELSE
							str_dialogue_root = "AH_FRANSHOOT"
						ENDIF
						IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
							peds[mpf_stair_1].iGenericFlag = 601
						ENDIF
					ENDIF
				BREAK
				
			// Lets the scene play out to the end
				CASE 601
					// Set swat as dead and gunman in cover
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG3_killswat])
					OR GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG3_killswat]) = 1.0

						// move frank to the left
						vExactCoord = GET_ACTUAL_GROUND_COORD(GET_ANIM_INITIAL_OFFSET_POSITION(animDict_IG3_kill_swat, anim_killswat_frank, v_ig3_coord, v_ig3_rot, 1.0))
						SET_PED_SPHERE_DEFENSIVE_AREA(BUDDY_PED_ID(), vExactCoord, 1.5)
						TASK_AIM_GUN_AT_COORD(BUDDY_PED_ID(), << 123.6345, -728.0139, 258.3256 >>, -1, TRUE)
						
						// put goon in cover
						TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_goon].id, vExactCoord, -1, FALSE, 0, TRUE, TRUE, covPair_gunman.covID[covPair_gunman.id])
						
						peds[mpf_stair_1].iGenericFlag = 7
					ENDIF
				BREAK
			// interupted scene, task buddy and gunman to their points
				CASE 602
					IF NOT IS_PED_INJURED(peds[mpf_stair_1].id)
						SET_ENTITY_HEALTH(peds[mpf_stair_1].id, 0)
					ENDIF
				
					// move buddy to the left
					vExactCoord = GET_ACTUAL_GROUND_COORD(GET_ANIM_INITIAL_OFFSET_POSITION(animDict_IG3_kill_swat, anim_killswat_frank, v_ig3_coord, v_ig3_rot, 1.0))
					SET_PED_SPHERE_DEFENSIVE_AREA(BUDDY_PED_ID(), vExactCoord, 1.5)
					SAFE_OPEN_SEQUENCE()
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, vExactCoord, << 123.9860, -728.0083, 258.5623 >>, PEDMOVE_WALK, FALSE, 0.2, 1.0)
						TASK_AIM_GUN_AT_COORD(null, << 123.9860, -728.0083, 258.5623 >>, -1, TRUE)
					SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
					
					// put goon in cover
					IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), GET_ENTITY_COORDS(PLAYER_PED_ID()), 1.0)
						TASK_SEEK_COVER_TO_COVER_POINT(peds[mpf_goon].id, covPair_gunman.covID[covPair_gunman.id], << 126.2951, -728.7147, 258.6575 >>, -1, FALSE)
					ELSE
						SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_goon].id, <<120.2220, -729.1161, 257.1519>>, 1.5)
						SAFE_OPEN_SEQUENCE()
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, <<120.2220, -729.1161, 257.1519>>, <<125.5695, -727.5220, 258.4745>>, PEDMOVE_WALK, FALSE, 0.2, 1.0)
							TASK_AIM_GUN_AT_COORD(null, <<125.5695, -727.5220, 258.4745>>, -1, TRUE)
						SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
					ENDIF
					
					peds[mpf_stair_1].iGenericFlag = 7
				BREAK
			ENDSWITCH
			
		 	CDEBUG1LN(DEBUG_MIKE, "STAIR1: ", peds[mpf_stair_1].iGenericFlag)
			
			IF IS_PED_INJURED(peds[mpf_stair_2].id)
				IF peds[mpf_stair_2].iGenericFlag < 99
					peds[mpf_stair_2].iGenericFlag = 99
				ENDIF
			ENDIF
			
			SWITCH peds[mpf_stair_2].iGenericFlag
				CASE 0
					DOORS_Set_State(mdf_topfloor_pre_stairs, FALSE, 0.0, 1500)
					
					SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_stair_2].id,  << 128.5580, -727.9175, 257.1522 >>, 1.5)
					SAFE_OPEN_SEQUENCE()
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, << 128.5580, -727.9175, 257.1522 >>, FRANK_PED_ID(), PEDMOVE_RUN, FALSE)
						TASK_AIM_GUN_AT_COORD(null, << 124.1950, -728.0079, 258.6689 >>, -1, FALSE)
					SAFE_PERFORM_SEQUENCE(peds[mpf_stair_2].id)
					
					SETTIMERA(0)
					ADD_PED_FOR_DIALOGUE(sConvo, 6, peds[mpf_stair_2].id, "SWAT2")
					peds[mpf_stair_2].iGenericFlag++
					
				BREAK
				CASE 1
					IF peds[mpf_stair_1].iGenericFlag = 7
					AND GET_SEQUENCE_PROGRESS(peds[mpf_stair_2].id) = 1
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
						peds[mpf_stair_2].iGenericFlag++
					ENDIF
				BREAK
				CASE 2
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_SWAT2", CONV_PRIORITY_HIGH)	
							peds[mpf_stair_2].iGenericFlag++
						ENDIF
					ENDIF
				BREAK
				CASE 3
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					OR TIMERA() > 1000
						// move 2nd swat to position, shoot through door
						TASK_SHOOT_AT_COORD(peds[mpf_stair_2].id, << 124.1950, -728.0079, 258.6689 >>, 1500, FIRING_TYPE_CONTINUOUS)
						SET_PED_FLASH_LIGHT(peds[mpf_stair_2], TRUE)

						peds[mpf_stair_2].iGenericFlag++
					ENDIF
				BREAK
				CASE 4
					// Slam the door open and lock it if it has been swung open by some other force (ie the enemy shooting it
					DOORS_Get_State(mdf_topfloor_pre_stairs, bDoorLockState, fDoorOpenRatio)
					IF fDoorOpenRatio < 0.3
						DOORS_Set_State(mdf_topfloor_pre_stairs, TRUE, -1.0, 300)
						//TASK_SHOOT_AT_COORD(peds[mpf_stair_2].id, <<123.5139, -728.1011, 258.4227>>, 2500, FIRING_TYPE_CONTINUOUS)
						SETTIMERA(0)
						peds[mpf_stair_2].iGenericFlag++
					ENDIF
				BREAK
				CASE 5
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_stair_2].id, SCRIPT_TASK_SHOOT_AT_COORD)
					AND TIMERA() > 4000
					AND NOT IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), peds[mpf_stair_2].id) 
					AND NOT IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(),peds[mpf_stair_2].id)
					
						IF GET_CREW_MEMBER_SKILL(crewGoon) = CMSK_MEDIUM
							
							SET_PED_COMBAT_ATTRIBUTES(peds[mpf_goon].id, CA_BLIND_FIRE_IN_COVER, FALSE)
							SET_COMBAT_FLOAT(peds[mpf_goon].id, CCF_BLIND_FIRE_CHANCE, 0.0)
							TASK_COMBAT_PED(peds[mpf_goon].id, peds[mpf_stair_2].id)
							
						ELIF GET_CREW_MEMBER_SKILL(crewGoon) = CMSK_GOOD
							
							SET_PED_COMBAT_ATTRIBUTES(peds[mpf_goon].id, CA_BLIND_FIRE_IN_COVER, TRUE)
							SET_COMBAT_FLOAT(peds[mpf_goon].id, CCF_BLIND_FIRE_CHANCE, 1.0)
							SET_COMBAT_FLOAT(peds[mpf_goon].id, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 4.0)
							TASK_COMBAT_PED(peds[mpf_goon].id, peds[mpf_stair_2].id)
							
						ENDIF
						peds[mpf_stair_2].iGenericFlag++
					ENDIF
				BREAK
				CASE 6
					SET_COMBAT_FLOAT(peds[mpf_stair_2].id, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
					SET_COMBAT_FLOAT(peds[mpf_stair_2].id, CCF_WALK_WHEN_STRAFING_CHANCE, 1.0)
					SAFE_OPEN_SEQUENCE()
						TASK_AIM_GUN_AT_COORD(null, << 124.1950, -728.0079, 258.6689 >>, 6000, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, << 128.6558, -728.9380, 257.1522 >>, << 124.1950, -728.0079, 258.6689 >>, PEDMOVE_WALK, FALSE)
						TASK_AIM_GUN_AT_COORD(null, << 124.1950, -728.0079, 258.6689 >>, 3000, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, << 128.5580, -727.9175, 257.1522 >>, << 124.1950, -728.0079, 258.6689 >>, PEDMOVE_WALK, FALSE)
						TASK_AIM_GUN_AT_COORD(null, << 124.1950, -728.0079, 258.6689 >>, 7000, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, << 128.6558, -728.9380, 257.1522 >>, << 124.1950, -728.0079, 258.6689 >>, PEDMOVE_WALK, FALSE)
						TASK_AIM_GUN_AT_COORD(null, << 124.1950, -728.0079, 258.6689 >>, 8000, TRUE)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, << 128.5580, -727.9175, 257.1522 >>, << 124.1950, -728.0079, 258.6689 >>, PEDMOVE_WALK, FALSE)

						SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
					SAFE_PERFORM_SEQUENCE(peds[mpf_stair_2].id)
					
					peds[mpf_stair_2].iGenericFlag++
				BREAK
			ENDSWITCH

			
			IF peds[mpf_stair_2].iGenericFlag >= 0
			AND peds[mpf_stair_2].iGenericFlag <= 7
				IF CAN_PED_SEE_HATED_PED(peds[mpf_stair_2].id, PLAYER_PED_ID())
					
					TASK_COMBAT_PED(peds[mpf_stair_2].id, PLAYER_PED_ID())
					peds[mpf_stair_2].iGenericFlag = 7
				
				ELIF peds[mpf_stair_2].iGenericFlag > 5
					IF CAN_PED_SEE_HATED_PED(peds[mpf_stair_2].id, BUDDY_PED_ID())
					
						TASK_COMBAT_PED(peds[mpf_stair_2].id, BUDDY_PED_ID())
						peds[mpf_stair_2].iGenericFlag = 7
						
					ELIF CAN_PED_SEE_HATED_PED(peds[mpf_stair_2].id, peds[mpf_goon].id)
					
						TASK_COMBAT_PED(peds[mpf_stair_2].id, peds[mpf_goon].id)
						peds[mpf_stair_2].iGenericFlag = 7
					ENDIF
				ENDIF
				
				IF peds[mpf_stair_2].iGenericFlag >= 4
					IF IS_SAFE_TO_START_CONVERSATION()
					AND GET_GAME_TIMER() - i_time_of_last_convo > 3500
					AND NOT IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), peds[mpf_stair_2].id)
					AND NOT IS_PED_SHOOTING(PLAYER_PED_ID())
					AND NOT IS_PED_IN_COMBAT(peds[mpf_goon].id, peds[mpf_stair_2].id)
						GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_CLEAR", FALSE, str_dialogue_root, TRUE)
						IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
						ENDIF
					
					ENDIF
				ENDIF
			ENDIF
			
			CDEBUG1LN(DEBUG_MIKE, "STAIR2: ", peds[mpf_stair_2].iGenericFlag)
			
			// Kill this event once both are dead
			IF IS_PED_INJURED(peds[mpf_stair_1].id)
			AND IS_PED_INJURED(peds[mpf_stair_2].id)
			
				SET_PED_COMBAT_ATTRIBUTES(peds[mpf_goon].id, CA_BLIND_FIRE_IN_COVER, FALSE)
				SET_COMBAT_FLOAT(peds[mpf_goon].id, CCF_BLIND_FIRE_CHANCE, 0.05)
				SET_COMBAT_FLOAT(peds[mpf_goon].id, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 1.25)
			
				Event_Finish(mef_ig3_swat_takedown)
			ENDIF
		BREAK
	ENDSWITCH
	
	
	
	#IF IS_DEBUG_BUILD
		IF bDisplayEventDebug
			DRAW_DEBUG_SPHERE(vGoToExactlyBuddy, 0.025, 255,0,0,100)
		ENDIF
	#ENDIF
ENDPROC

PROC me_ig4_kick_door()
	SWITCH mEvents[mef_ig4_kick_door].iStage
		CASE 1			
			KILL_FACE_TO_FACE_CONVERSATION()
			
			Unload_Asset_Anim_Dict(sAssetData, animDict_IG3_kill_swat)
			Load_Asset_AnimDict(sAssetData, animDict_IG4_sw_door_kick)
			
			CREATE_COVERPOINT_PAIR(covPair_buddy, << 129.7267, -731.5209, 257.1522 >>, 165.6992, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_180)
			CREATE_COVERPOINT_PAIR(covPair_gunman, << 127.6624, -730.7710, 257.1522 >>, 157.5018, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180)

			REMOVE_PED_DEFENSIVE_AREA(BUDDY_PED_ID())
			REMOVE_PED_DEFENSIVE_AREA(peds[mpf_goon].id)
			
			bRetaskBuddy 			= TRUE			
			bRetaskGunman 			= TRUE
			bBuddyReadyToMoveOn 	= FALSE
			bGunmanReadyToMoveOn	= FALSE
			mEvents[mef_ig4_kick_door].iStage++
		BREAK
		CASE 2
			IF IS_SAFE_TO_START_CONVERSATION()
				GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root)
				IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
					str_dialogue_root = ""
					mEvents[mef_ig4_kick_door].iStage++
				ENDIF
			ENDIF	
		BREAK
		CASE 3
		
			SET_PED_RESET_FLAG(peds[mpf_goon].id, PRF_UseKinematicPhysics, TRUE)
			SET_PED_RESET_FLAG(peds[mpf_goon].id, PRF_DisablePedCollisionWithPedEvent, TRUE)
			CPRINTLN(DEBUG_MIKE, "SET_PED_RESET_FLAG(peds[mpf_goon].id, PRF_UseKinematicPhysics, TRUE)")
		
			// BUDDY
			IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
			AND GET_SEQUENCE_PROGRESS(BUDDY_PED_ID()) = 2
			AND ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(BUDDY_PED_ID()), << 129.7267, -731.5209, 257.1522 >>, 2.0)
				bBuddyReadyToMoveOn = TRUE
				
			ELIF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
			OR bRetaskBuddy
			
				bBuddyReadyToMoveOn = FALSE
				
				SET_PED_STEERS_AROUND_PEDS(BUDDY_PED_ID(), FALSE)
				
				SAFE_OPEN_SEQUENCE()
					TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<126.9454, -728.5305, 257.1519>>, PEDMOVE_RUN, DEFAULT, DEFAULT, ENAV_NO_STOPPING)
					TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, <<130.3221, -730.3776, 257.1519>>, <<128.6206, -731.5894, 258.5540>>, PEDMOVE_RUN, FALSE, 0.5, 2.0, TRUE, ENAV_NO_STOPPING)
					TASK_AIM_GUN_AT_COORD(null, <<128.6206, -731.5894, 258.5540>>, -1, FALSE, TRUE)
				SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
					
				//TASK_SEEK_COVER_TO_COVER_POINT(BUDDY_PED_ID(), covPair_buddy.covID[covPair_buddy.id], << 126.5442, -736.1642, 257.5806 >>, -1, FALSE)
				bRetaskBuddy = FALSE
		
			ENDIF
			
			// GUNMAN
			IF IS_PED_IN_COVER(peds[mpf_goon].id)
			AND ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), << 127.6624, -730.7710, 257.1522 >>, 2.0)
			
				bGunmanReadyToMoveOn = TRUE

			ELIF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_PERFORM_SEQUENCE)
			OR bRetaskGunman
			
				bGunmanReadyToMoveOn = FALSE
			
				SAFE_OPEN_SEQUENCE()
					IF IS_PED_IN_COVER(peds[mpf_goon].id)
						TASK_EXIT_COVER(null, CORNER_COVER_EXIT, <<0,0,0>>)
					ENDIF
					TASK_SEEK_COVER_TO_COVER_POINT(null, covPair_gunman.covID[covPair_gunman.id], << 126.5442, -736.1642, 257.5806 >>, -1, FALSE)
				SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
				bRetaskGunman = FALSE
			ENDIF

			// player entered the room
			IF bBuddyReadyToMoveOn AND bGunmanReadyToMoveOn
			
				IF IS_SAFE_TO_START_CONVERSATION()
			
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<124.115028,-727.335327,257.152161>>, <<131.919662,-730.146790,260.818298>>, 4.625000)
						
						IF HAS_ANIM_DICT_LOADED(animDict_IG4_sw_door_kick)
						AND IS_ENTITY_IN_ANGLED_AREA(BUDDY_PED_ID(), <<129.960373,-729.794189,257.152191>>, <<131.794098,-730.491821,260.858673>>, 4.312500)
						
							// start the synced scene
							IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<129.164673,-729.824890,257.152161>>, <<128.612015,-731.494873,260.769989>>, 2.375000)

								str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewGoon, "AH_DOOR")
								IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								
									CREATE_MISSION_PED(peds[mpf_stair_3], mod_swat, << 126.9457, -736.9636, 255.1506 >>, 348.8681, "stair_3", REL_ENEMY, WEAPONTYPE_SMG, WEAPONCOMPONENT_AT_AR_FLSH, 2, 110)
								
									ADD_ENTITY_TO_AUDIO_MIX_GROUP(peds[mpf_goon].id, "SHOOT_THROUGH_DOOR_GROUP", 0)
									START_AUDIO_SCENE("AH_3B_SHOOTTHROUGHDOOR_SCENE")
								
									syncedScenes[ssf_IG4_door_kick_goon] = CREATE_SYNCHRONIZED_SCENE(<< 127.946, -731.132, 257.132 >>, << 0.000, 0.000, -20.0 >>)
									TASK_SYNCHRONIZED_SCENE(peds[mpf_goon].id, syncedScenes[ssf_IG4_door_kick_goon], animDict_IG4_sw_door_kick, anim_goon_door_kick, SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, 1.0)
									TASK_SYNCHRONIZED_SCENE(peds[mpf_stair_3].id, syncedScenes[ssf_IG4_door_kick_goon], animDict_IG4_sw_door_kick, "shoot_opendoor_guard", SLOW_BLEND_IN, NORMAL_BLEND_OUT)
									
									SET_PED_STEERS_AROUND_PEDS(PLAYER_PED_ID(), TRUE)
									SET_PED_STEERS_AROUND_PEDS(BUDDY_PED_ID(), TRUE)

									REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)

									Load_Asset_AnimDict(sAssetData, animDict_IG5_sw_push_door)
									str_dialogue_root = ""
									bRetaskBuddy = TRUE
									mEvents[mef_ig4_kick_door].iStage++
								ENDIF
							
							// dialogue for if the player is standing in the way.
							ELSE
								IF GET_GAME_TIMER() - i_time_of_last_convo > 5000
									GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_DOORM", FALSE, str_dialogue_root, TRUE)
									IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
										str_dialogue_root = ""
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					// tell the player to get a move on
					ELSE
						IF GET_GAME_TIMER() - i_time_of_last_convo > 7500
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, TRUE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
			AND GET_SEQUENCE_PROGRESS(BUDDY_PED_ID()) = 2
				//IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
				IF bRetaskBuddy
					TASK_SEEK_COVER_TO_COVER_POINT(BUDDY_PED_ID(), covPair_buddy.covID[covPair_buddy.id], << 126.5442, -736.1642, 257.5806 >>, -1, FALSE)
					bRetaskBuddy = FALSE
				ENDIF
			ENDIF
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG4_door_kick_goon])
				IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG4_door_kick_goon]) >= 0.34
					SET_PED_ACCURACY(peds[mpf_goon].id, 100)
					SET_PED_SHOOTS_AT_COORD(peds[mpf_goon].id, << 127.9899, -731.2427, 258.1750 >>, TRUE)
					PLAY_SOUND_FROM_ENTITY(-1, "RFL_SINGLESHOT_NPC_MASTER", peds[mpf_goon].id)
					CPRINTLN(DEBUG_MIKE,"RFL_SINGLESHOT_NPC_MASTER sound played")
					mEvents[mef_ig4_kick_door].iStage++
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
			AND GET_SEQUENCE_PROGRESS(BUDDY_PED_ID()) = 2
				//IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
				IF bRetaskBuddy
					TASK_SEEK_COVER_TO_COVER_POINT(BUDDY_PED_ID(), covPair_buddy.covID[covPair_buddy.id], << 126.5442, -736.1642, 257.5806 >>, -1, FALSE)
					bRetaskBuddy = FALSE
				ENDIF
			ENDIF
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG4_door_kick_goon])
				IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG4_door_kick_goon]) >= 0.53
					DOORS_Set_State(mdf_stairwell_53, TRUE, 1.0, 400)
					START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_agency3b_wood_splinter", <<127.95, -731.32, 258.23>>, <<0,0,0>>, 1.0)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(peds[mpf_goon].id, 0)
					STOP_AUDIO_SCENE("AH_3B_SHOOTTHROUGHDOOR_SCENE")
					mEvents[mef_ig4_kick_door].iStage++
				ENDIF
			ENDIF
		BREAK
		CASE 6
			IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
			AND GET_SEQUENCE_PROGRESS(BUDDY_PED_ID()) = 2
				//IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
				IF bRetaskBuddy
					TASK_SEEK_COVER_TO_COVER_POINT(BUDDY_PED_ID(), covPair_buddy.covID[covPair_buddy.id], << 126.5442, -736.1642, 257.5806 >>, -1, FALSE)
					bRetaskBuddy = FALSE
				ENDIF
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG4_door_kick_goon])
				IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG4_door_kick_goon]) >= 0.72
					IF NOT IS_PED_INJURED(peds[mpf_stair_3].id)
						SET_AMMO_IN_CLIP(peds[mpf_goon].id, weap_Goon, 26)
						SET_PED_COMBAT_MOVEMENT(peds[mpf_goon].id, CM_STATIONARY)
						SET_PED_SHOOTS_AT_COORD(peds[mpf_goon].id, GET_ENTITY_COORDS(peds[mpf_stair_3].id), TRUE)
					ELSE
						mEvents[mef_ig4_kick_door].iStage++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 7
			IF IS_SAFE_TO_START_CONVERSATION()
				str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewGoon, "AH_STAIR")
				IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
					str_dialogue_root = ""
					Event_Finish(mef_ig4_kick_door)		
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC me_ig5_push_door()

	SWITCH mEvents[mef_ig5_push_door].iStage
		CASE 1
			Load_Asset_AnimDict(sAssetData, animDict_IG5_sw_push_door)
			
			i_dialogue_stage = 0
			
			IF HAS_ANIM_DICT_LOADED(animDict_IG5_sw_push_door)
				CREATE_MISSION_PED(peds[mpf_stair_4], mod_swat, << 129.45, -728.34, 254.15 >>, 173.95, "stair 4", REL_ENEMY, WEAPONTYPE_SMG, WEAPONCOMPONENT_AT_AR_FLSH, 2, 110)
				SET_ENTITY_HEALTH(peds[mpf_stair_4].id, 110)
				CREATE_MISSION_PED(peds[mpf_stair_5], mod_swat, << 127.0042, -728.3694, 253.1523 >>, 211.8783, "stair 5", REL_ENEMY, WEAPONTYPE_SMG, WEAPONCOMPONENT_AT_AR_FLSH, 2, 110)
				SET_ENTITY_HEALTH(peds[mpf_stair_5].id, 110)
				
				TASK_AIM_GUN_AT_COORD(peds[mpf_stair_4].id, << 126.6600, -736.3271, 253.9823 >>, -1, TRUE)
				TASK_AIM_GUN_AT_COORD(peds[mpf_stair_5].id, << 130.4947, -733.9948, 254.8302 >>, -1, TRUE)
				
				ADD_PED_FOR_DIALOGUE(sConvo, 8, peds[mpf_stair_4].id, "SWAT3")
			
				// Task goon to the next floor down
				REMOVE_PED_DEFENSIVE_AREA(peds[mpf_goon].id)
				SET_PED_COMBAT_PARAMS(peds[mpf_goon], << 0,0,0 >>, 0.0, CM_WILLADVANCE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 1.0)
				SET_PED_ACCURACY(peds[mpf_goon].id, 10)
				
				// Task frank to next floor
				REMOVE_PED_DEFENSIVE_AREA(BUDDY_PED_ID())
				SET_PED_COMBAT_PARAMS(peds[mpf_mike], 	<< 0,0,0 >>, 0.0, CM_WILLADVANCE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 1.0)
				SET_PED_COMBAT_PARAMS(peds[mpf_frank], 	<< 0,0,0 >>, 0.0, CM_WILLADVANCE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 1.0)
				vGoToExactlyGunman = GET_ACTUAL_GROUND_COORD(GET_ANIM_INITIAL_OFFSET_POSITION(animDict_IG5_sw_push_door, anim_frank_push_door, v_ig5_coord, v_ig5_rot, 0.0))
				CREATE_COVERPOINT_PAIR(covPair_gunman, vGoToExactlyGunman, 343.8403, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)

				Unload_Asset_Anim_Dict(sAssetData, animDict_IG4_sw_door_kick)
				
				PREPARE_MUSIC_EVENT("AH3B_DOOR_52")
				
				bRetaskBuddy 	= TRUE
				bRetaskGunman 	= TRUE
				iBuddySeqLength	= 0
				
				mEvents[mef_ig5_push_door].iStage++
			ENDIF
		BREAK
		CASE 2
			bBuddyReadyToMoveOn = FALSE
			bGunmanReadyToMoveOn = FALSE
		
			// BUDDY
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
			OR bRetaskBuddy
				
				iBuddySeqLength = 2
				SAFE_OPEN_SEQUENCE()
					IF IS_PED_IN_COVER(BUDDY_PED_ID())
						TASK_EXIT_COVER(null, CORNER_COVER_EXIT, << 0,0,0 >>)
						iBuddySeqLength++
					ENDIF
					TASK_FOLLOW_NAV_MESH_TO_COORD(null, << 127.3561, -733.6717, 253.1511 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DONT_AVOID_OBJECTS | ENAV_DONT_AVOID_PEDS)
					TASK_AIM_GUN_AT_COORD(null, << 129.1243, -730.1541, 254.6746 >>, -1, FALSE)
				SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())

				bRetaskBuddy = FALSE
			ELSE
				IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
				AND ((iBuddySeqLength = 2 AND GET_SEQUENCE_PROGRESS(BUDDY_PED_ID()) = 1) OR (iBuddySeqLength = 3 AND GET_SEQUENCE_PROGRESS(BUDDY_PED_ID()) = 2))
				AND ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(BUDDY_PED_ID()), << 127.3561, -733.6717, 253.1511 >>, 2.0)
					SET_PED_RESET_FLAG(BUDDY_PED_ID(), PRF_UseKinematicPhysics, TRUE)
					bBuddyReadyToMoveOn = TRUE
				ENDIF
			ENDIF
			
			// GUNMAN			
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_PERFORM_SEQUENCE)
			OR bRetaskGunman
				
				SAFE_OPEN_SEQUENCE()
					TASK_FOLLOW_NAV_MESH_TO_COORD(null, vGoToExactlyGunman, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DONT_AVOID_OBJECTS | ENAV_DONT_AVOID_PEDS)
					TASK_SEEK_COVER_TO_COVER_POINT(null, covPair_gunman.covID[covPair_gunman.id], << 130.3619, -730.3313, 254.5613 >>, -1)
				SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
				
				bRetaskGunman = FALSE
				
			ELSE
				IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), vGoToExactlyGunman, 1.5)
				AND IS_PED_IN_COVER(peds[mpf_goon].id)
					bGunmanReadyToMoveOn = TRUE
				ENDIF
			ENDIF

			IF bBuddyReadyToMoveOn AND bGunmanReadyToMoveOn
			AND i_dialogue_stage > 2

				// Anim: Franklin pushes door open
				syncedScenes[ssf_IG5_push_door_frank] = CREATE_SYNCHRONIZED_SCENE(v_ig5_coord, v_ig5_rot)
				TASK_SYNCHRONIZED_SCENE(peds[mpf_goon].id, syncedScenes[ssf_IG5_push_door_frank], animDict_IG5_sw_push_door, anim_frank_push_door, NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE)
				str_dialogue_root = ""
				mEvents[mef_ig5_push_door].iStage++
			ENDIF
		BREAK
		CASE 3
			IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG5_push_door_frank])
				FLOAT fScenePhase
				fScenePhase = GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG5_push_door_frank])
				IF fScenePhase >= 0.440
					DOORS_Set_State(mdf_stairwell_52, TRUE, -1.0, 700)
					IF PREPARE_MUSIC_EVENT("AH3B_DOOR_52")
						TRIGGER_MUSIC_EVENT("AH3B_DOOR_52")
					ENDIF
					bRetaskBuddy	= TRUE
					bRetaskGunman	= TRUE
					mEvents[mef_ig5_push_door].iStage = 5
				ENDIF
			ENDIF
		BREAK
//		CASE 4
//			IF TIMERB() >= 500	
//				mEvents[mef_ig5_push_door].iStage++
//			ENDIF
//		BREAK
		CASE 5
			IF IS_PED_INJURED(peds[mpf_stair_4].id)
			AND IS_PED_INJURED(peds[mpf_stair_5].id)
				i_dialogue_stage = 0
				Unload_Asset_Anim_Dict(sAssetData, animDict_IG5_sw_push_door)
				Event_Finish(mef_ig5_push_door)
			ELSE
				IF DOES_ENTITY_EXIST(BUDDY_PED_ID()) 
				AND NOT IS_PED_INJURED(BUDDY_PED_ID())
				//AND NOT IS_PED_IN_COMBAT(BUDDY_PED_ID())
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
					OR bRetaskBuddy
						//TASK_COMBAT_PED(BUDDY_PED_ID(), peds[mpf_stair_4].id, COMBAT_PED_DISABLE_AIM_INTRO)
						SAFE_OPEN_SEQUENCE()
							TASK_SHOOT_AT_ENTITY(null, peds[mpf_stair_4].id, 3000, FIRING_TYPE_CONTINUOUS)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(null, 10.0)
						SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
						bRetaskBuddy = FALSE
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(peds[mpf_stair_4].id)
				AND NOT IS_PED_INJURED(peds[mpf_stair_4].id)
				
					//IF NOT IS_PED_IN_COMBAT(peds[mpf_stair_4].id)	
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_stair_4].id, SCRIPT_TASK_PERFORM_SEQUENCE)
						//TASK_COMBAT_PED(peds[mpf_stair_4].id, BUDDY_PED_ID(), COMBAT_PED_DISABLE_AIM_INTRO)
						SAFE_OPEN_SEQUENCE()
							TASK_SHOOT_AT_ENTITY(null, BUDDY_PED_ID(), 2000, FIRING_TYPE_RANDOM_BURSTS)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(null, 10.0)
						SAFE_PERFORM_SEQUENCE(peds[mpf_stair_4].id)
						
					ELIF NOT IS_AMBIENT_SPEECH_PLAYING(peds[mpf_stair_4].id)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_stair_4].id, "AH3b_EYAA", "SWAT2", SPEECH_PARAMS_FORCE)	
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(peds[mpf_stair_5].id)
				AND NOT IS_PED_INJURED(peds[mpf_stair_5].id)
				
					//IF NOT IS_PED_IN_COMBAT(peds[mpf_stair_5].id)
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_stair_5].id, SCRIPT_TASK_PERFORM_SEQUENCE)
						//TASK_COMBAT_PED(peds[mpf_stair_5].id, peds[mpf_goon].id, COMBAT_PED_DISABLE_AIM_INTRO)
						SAFE_OPEN_SEQUENCE()
							TASK_SHOOT_AT_ENTITY(null, peds[mpf_goon].id, 2000, FIRING_TYPE_CONTINUOUS)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(null, 10.0)
						SAFE_PERFORM_SEQUENCE(peds[mpf_stair_5].id)
						
					ELIF NOT IS_AMBIENT_SPEECH_PLAYING(peds[mpf_stair_5].id)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(peds[mpf_stair_5].id, "AH3b_EZAA", "SWAT3", SPEECH_PARAMS_FORCE)
					ENDIF
					
				ENDIF
				
				IF (NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG5_push_door_frank]) OR GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG5_push_door_frank]) = 1.0)
				AND (NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_PERFORM_SEQUENCE) OR bRetaskGunman)
				
					SET_PED_COMBAT_ATTRIBUTES(peds[mpf_goon].id, CA_BLIND_FIRE_IN_COVER, TRUE)
					SET_COMBAT_FLOAT(peds[mpf_goon].id, CCF_BLIND_FIRE_CHANCE, 1.0)
					SET_COMBAT_FLOAT(peds[mpf_goon].id, CCF_BURST_DURATION_IN_COVER, 4.0)
					
					SAFE_OPEN_SEQUENCE()
						TASK_PUT_PED_DIRECTLY_INTO_COVER(null, GET_SCRIPTED_COVER_POINT_COORDS(covPair_gunman.covID[covPair_gunman.id]), 1000, FALSE, 0.5, TRUE, TRUE, covPair_gunman.covID[covPair_gunman.id], FALSE)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(null, 10.0)
					SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
					
					bRetaskGunman = FALSE
				ENDIF
			
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 2000
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_OUT", TRUE, str_dialogue_root)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	SWITCH i_dialogue_stage	
		CASE 0 
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<130.243774,-732.301758,253.249954>>, <<127.556755,-739.092224,257.128967>>, 1.937500)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<125.682320,-738.410034,251.188065>>, <<128.236664,-731.585754,255.151688>>, 2.500000)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				i_dialogue_stage++
			ENDIF
		BREAK
		CASE 1
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_SWATCHAT2", CONV_PRIORITY_HIGH)
					i_dialogue_stage++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_SAFE_TO_START_CONVERSATION()
				str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewGoon, "AH_ODOOR")
				IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
					str_dialogue_root = ""
					i_dialogue_stage++
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

PROC me_ig7_agent_at_elevator()
	// Death check first
	IF DOES_ENTITY_EXIST(peds[mpf_ig7_elevator].id)
	AND IS_PED_INJURED(peds[mpf_ig7_elevator].id)
		TRIGGER_MUSIC_EVENT("AH3B_ENEMY_DOWN")
		Unload_Asset_Anim_Dict(sAssetData, animDict_IG7_elevator)
		Event_Finish(mef_ig7_elevator)
	ELSE
		IF DOES_ENTITY_EXIST(peds[mpf_ig7_elevator].id)
		AND NOT IS_PED_INJURED(peds[mpf_ig7_elevator].id)
			SET_PED_RESET_FLAG(peds[mpf_ig7_elevator].id, PRF_InstantBlendToAim, TRUE)
		ENDIF
		
		SWITCH mEvents[mef_ig7_elevator].iStage
			CASE 1
				Load_Asset_Model(sAssetData, mod_fib_agent_1)
				Load_Asset_AnimDict(sAssetData, animDict_IG7_elevator)
				
				IF HAS_MODEL_LOADED(mod_fib_agent_1)
				AND HAS_ANIM_DICT_LOADED(animDict_IG7_elevator)
					CREATE_MISSION_PED(peds[mpf_ig7_elevator], mod_fib_agent_1, << 136.540, -733.814, 250.189 >>, 158.5, "ig7_elevator", REL_ENEMY, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_INVALID, 5, 130)
					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(peds[mpf_ig7_elevator].id, TRUE)
					SET_PED_ENVEFF_CPV_ADD(peds[mpf_ig7_elevator].id, 0.099)
					SET_PED_ENVEFF_SCALE(peds[mpf_ig7_elevator].id, 1.0)
					SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_ig7_elevator].id, 87,81,68)
					SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_ig7_elevator].id, TRUE)
					ADD_PED_FOR_DIALOGUE(sConvo, 6, peds[mpf_ig7_elevator].id, "SWAT3")
					
					syncedScenes[ssf_IG7_elevator] = CREATE_SYNCHRONIZED_SCENE(v_ig7_coord, v_ig7_rot)
					TASK_SYNCHRONIZED_SCENE(peds[mpf_ig7_elevator].id, syncedScenes[ssf_IG7_elevator], animDict_IG7_elevator, anim_elevator_loop, INSTANT_BLEND_IN, SLOW_BLEND_OUT)
					SET_SYNCHRONIZED_SCENE_LOOPED(syncedScenes[ssf_IG7_elevator], TRUE)
					
					peds[mpf_ig7_elevator].iGenericFlag = -1
					mEvents[mef_ig7_elevator].iStage++
				ENDIF
			BREAK
			CASE 2
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(peds[mpf_ig7_elevator].id)
					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(peds[mpf_ig7_elevator].id, FALSE)
					SET_CURRENT_PED_WEAPON(peds[mpf_ig7_elevator].id, WEAPONTYPE_PISTOL, TRUE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(peds[mpf_ig7_elevator].id, 20.0)
					
					peds[mpf_ig7_elevator].iGenericFlag = 0
					mEvents[mef_ig7_elevator].iStage = 5
					
				ELIF IS_ENTITY_IN_ANGLED_AREA(MIKE_PED_ID(), <<146.778397,-735.523682,249.152115>>, <<131.955978,-730.519348,252.973419>>, 4.562500)
				OR IS_ENTITY_IN_ANGLED_AREA(peds[mpf_frank].id, <<146.778397,-735.523682,249.152115>>, <<131.955978,-730.519348,252.973419>>, 4.562500)
				OR IS_ENTITY_IN_ANGLED_AREA(peds[mpf_goon].id, <<146.778397,-735.523682,249.152115>>, <<131.955978,-730.519348,252.973419>>, 4.562500)
				OR HAS_PED_RECEIVED_EVENT(peds[mpf_ig7_elevator].id, EVENT_SHOT_FIRED)
				OR HAS_PED_RECEIVED_EVENT(peds[mpf_ig7_elevator].id, EVENT_SHOT_FIRED_WHIZZED_BY)
				OR HAS_PED_RECEIVED_EVENT(peds[mpf_ig7_elevator].id, EVENT_SHOT_FIRED_BULLET_IMPACT)
					syncedScenes[ssf_IG7_elevator] = CREATE_SYNCHRONIZED_SCENE(v_ig7_coord, v_ig7_rot)
					TASK_SYNCHRONIZED_SCENE(peds[mpf_ig7_elevator].id, syncedScenes[ssf_IG7_elevator], animDict_IG7_elevator, anim_elevator_exit, SLOW_BLEND_IN, NORMAL_BLEND_OUT)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(syncedScenes[ssf_IG7_elevator], FALSE)
					
					peds[mpf_ig7_elevator].iGenericFlag = 0
					mEvents[mef_ig7_elevator].iStage++
				ELSE
					IF NOT IS_CUTSCENE_PLAYING()
						IF peds[mpf_ig7_elevator].iGenericFlag = -1
							
							peds[mpf_ig7_elevator].iGenericFlag = GET_GAME_TIMER() + 3000
							
						ELIF GET_GAME_TIMER() > peds[mpf_ig7_elevator].iGenericFlag
							
							IF IS_SAFE_TO_START_CONVERSATION()
								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_AGENT_TRY", CONV_PRIORITY_HIGH)
									peds[mpf_ig7_elevator].iGenericFlag = GET_GAME_TIMER() + 10000
								ENDIF
							ENDIF
							
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 3
				IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG7_elevator]) >= 0.353
					HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(peds[mpf_ig7_elevator].id, FALSE)
					SET_CURRENT_PED_WEAPON(peds[mpf_ig7_elevator].id, WEAPONTYPE_PISTOL, TRUE)
					mEvents[mef_ig7_elevator].iStage++
				ENDIF
			BREAK
			CASE 4
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG7_elevator])
				OR GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG7_elevator]) >= 0.812
					SAFE_OPEN_SEQUENCE()
						TASK_AIM_GUN_AT_ENTITY(null, PLAYER_PED_ID(), 500, TRUE)
						TASK_COMBAT_PED_TIMED(null, PLAYER_PED_ID(), 3000)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(null, 20.0)
					SAFE_PERFORM_SEQUENCE(peds[mpf_ig7_elevator].id)
					
					mEvents[mef_ig7_elevator].iStage++
				ENDIF
			BREAK
		ENDSWITCH
		
		IF mEvents[mef_ig7_elevator].iStage >= 3
		AND peds[mpf_ig7_elevator].iGenericFlag != -1
			
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ELIF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_AGENT_SEE", CONV_PRIORITY_HIGH)
					peds[mpf_ig7_elevator].iGenericFlag = -1
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC me_ig8_paramedic()
	
	IF mEvents[mef_ig8_paramedic].iStage > 3
	
		// Manage victim dead/attacked checks
		IF peds[mpf_ig8_victim].iGenericFlag != 77
		AND DOES_ENTITY_EXIST(peds[mpf_ig8_victim].id) 
		AND ( (IS_PED_INJURED(peds[mpf_ig8_victim].id) OR GET_ENTITY_HEALTH(peds[mpf_ig8_victim].id) < 200)		// ... ped is dead or hit
		OR peds[mpf_ig8_paramedic_1].iGenericFlag = 77)		// ... paramedic has stopped doing cpr (dropped out the synced scene; dead or has fled)
		
			// make sure is dead
			IF NOT IS_PED_INJURED(peds[mpf_ig8_victim].id)
				SET_ENTITY_HEALTH(peds[mpf_ig8_victim].id, 0)
			ENDIF
			peds[mpf_ig8_victim].iGenericFlag = 77
			
			IF mEvents[mef_ig8_paramedic].iStage < 3
				mEvents[mef_ig8_paramedic].iStage = 3
			ENDIF
		ENDIF
		
		
		// Manage paramedic dead/attacked checks
		IF peds[mpf_ig8_paramedic_1].iGenericFlag != 77
			IF DOES_ENTITY_EXIST(peds[mpf_ig8_paramedic_1].id) 
			AND ( (IS_PED_INJURED(peds[mpf_ig8_paramedic_1].id) OR GET_ENTITY_HEALTH(peds[mpf_ig8_paramedic_1].id) < 200)	// ... paramedic has been shot
			OR (peds[mpf_ig8_victim].iGenericFlag = 77 AND NOT IS_ENTITY_PLAYING_ANIM(peds[mpf_ig8_paramedic_1].id, "missheistfbi3b_ig8_2", "Cower_Loop_Paramedic")))	// ... OR victim is dead, and not in the cowering loop
			OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(peds[mpf_ig8_paramedic_1].id)) >= 10.0		// player has moved away from the paramedic, let him run off and escape
				
				// .. then flee if alive still
				IF NOT IS_PED_INJURED(peds[mpf_ig8_paramedic_1].id)
					SAFE_OPEN_SEQUENCE()
						TASK_FOLLOW_NAV_MESH_TO_COORD(null, << 133.8307, -731.3259, 249.1521 >>, PEDMOVE_RUN)
						TASK_COWER(null, -1)
					SAFE_PERFORM_SEQUENCE(peds[mpf_ig8_paramedic_1].id)
					SET_PED_AS_NO_LONGER_NEEDED(peds[mpf_ig8_paramedic_1].id)
				ENDIF
				peds[mpf_ig8_paramedic_1].iGenericFlag = 77
				
				IF mEvents[mef_ig8_paramedic].iStage < 3
					mEvents[mef_ig8_paramedic].iStage = 3
				ENDIF
			ENDIF
		ENDIF
		
		// Manage agent 1
		SWITCH peds[mpf_ig8_agent1].iGenericFlag
			CASE 0
				IF NOT IS_PED_INJURED(peds[mpf_ig8_agent1].id)
					IF IS_ENTITY_PLAYING_ANIM(peds[mpf_ig8_agent1].id, "missheistfbi3b_ig8_2", "hallyway_enter_fibagenta")
					AND IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG8_agents])
					
						SET_PED_RESET_FLAG(peds[mpf_ig8_agent1].id, PRF_InstantBlendToAim, TRUE)
					
						IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG8_agents]) >= 0.975

							SAFE_OPEN_SEQUENCE()
								TASK_AIM_GUN_AT_ENTITY(null, PLAYER_PED_ID(), 100, TRUE)
								TASK_COMBAT_PED_TIMED(null, PLAYER_PED_ID(), 3000)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(null, 50.0)
							SAFE_PERFORM_SEQUENCE(peds[mpf_ig8_agent1].id)
							
							//FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_ig8_agent1].id)
							SET_PED_CONFIG_FLAG(peds[mpf_ig8_agent1].id, PCF_ShouldChargeNow, TRUE)
							peds[mpf_ig8_agent1].iGenericFlag++
						ENDIF
					ELSE
					
						SET_PED_RESET_FLAG(peds[mpf_ig8_agent1].id, PRF_InstantBlendToAim, FALSE)
					
						SAFE_OPEN_SEQUENCE()
							TASK_COMBAT_PED_TIMED(null, PLAYER_PED_ID(), 3000)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(null, 50.0)
						SAFE_PERFORM_SEQUENCE(peds[mpf_ig8_agent1].id)
					
						SET_PED_CONFIG_FLAG(peds[mpf_ig8_agent1].id, PCF_ShouldChargeNow, TRUE)
						peds[mpf_ig8_agent1].iGenericFlag++
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		// Manage agent 2
		SWITCH peds[mpf_ig8_agent2].iGenericFlag
			CASE 0
				IF NOT IS_PED_INJURED(peds[mpf_ig8_agent2].id)
					IF IS_ENTITY_PLAYING_ANIM(peds[mpf_ig8_agent2].id, "missheistfbi3b_ig8_2", "hallyway_enter_fibagentb")
					AND IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG8_agents])
					
						SET_PED_RESET_FLAG(peds[mpf_ig8_agent2].id, PRF_InstantBlendToAim, TRUE)
						
						IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG8_agents]) >= 0.975
					
							SAFE_OPEN_SEQUENCE()
								TASK_AIM_GUN_AT_ENTITY(null, PLAYER_PED_ID(), 500, TRUE)
								TASK_COMBAT_PED_TIMED(null, PLAYER_PED_ID(), 3000)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(null, 50.0)
							SAFE_PERFORM_SEQUENCE(peds[mpf_ig8_agent2].id)
							
							//FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_ig8_agent2].id)
							SET_PED_CONFIG_FLAG(peds[mpf_ig8_agent2].id, PCF_ShouldChargeNow, TRUE)
							peds[mpf_ig8_agent2].iGenericFlag++
						ENDIF
					ELSE
					
						SET_PED_RESET_FLAG(peds[mpf_ig8_agent2].id, PRF_InstantBlendToAim, FALSE)
					
						SAFE_OPEN_SEQUENCE()
							TASK_COMBAT_PED_TIMED(null, PLAYER_PED_ID(), 3000)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(null, 50.0)
						SAFE_PERFORM_SEQUENCE(peds[mpf_ig8_agent2].id)
					
						SET_PED_CONFIG_FLAG(peds[mpf_ig8_agent2].id, PCF_ShouldChargeNow, TRUE)
						peds[mpf_ig8_agent2].iGenericFlag++
					
					ENDIF
				ENDIF
				
			BREAK
		ENDSWITCH
	ENDIF
	
	// Main event flow
	SWITCH mEvents[mef_ig8_paramedic].iStage
		CASE 1
			Load_Asset_Model(sAssetData, S_M_M_PARAMEDIC_01)
			Load_Asset_Model(sAssetData, mod_fib_agent_1)
			Load_Asset_Model(sAssetData, mod_fib_agent_2)
			Load_Asset_AnimDict(sAssetData, "missheistfbi3b_ig8_2")
			Load_Asset_AnimDict(sAssetData, "FACIALS@GEN_MALE@BASE")
			
			IF HAS_MODEL_LOADED(mod_fib_agent_1)
			AND HAS_MODEL_LOADED(mod_fib_agent_2)
			AND HAS_MODEL_LOADED(S_M_M_PARAMEDIC_01)
			AND HAS_ANIM_DICT_LOADED("missheistfbi3b_ig8_2")
			AND HAS_ANIM_DICT_LOADED("FACIALS@GEN_MALE@BASE")
			
				// paramedic
				CREATE_MISSION_PED(peds[mpf_ig8_paramedic_1], S_M_M_PARAMEDIC_01, << 129.7995, -741.9392, 249.1520 >>, 18.8529, "IG8_Paramedic", REL_911)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_ig8_paramedic_1].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_ig8_paramedic_1].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_ig8_paramedic_1].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_ig8_paramedic_1].id, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_ig8_paramedic_1].id, TRUE)
				
				CREATE_MISSION_PED(peds[mpf_ig8_victim], mod_fib_agent_1, << 132.1709, -741.9894, 249.1520 >>, 21.9195, "IG8_Victim", REL_911)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_ig8_victim].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_ig8_victim].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_ig8_victim].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_ig8_victim].id, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_ig8_victim].id, TRUE)
				
				INT iPlacementFlags
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
				SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
				//SET_BIT(iPlacementFlags, ENUM_TO_INT(PLACEMENT_FLAG_FIXED))
				pkups[mpuf_health_paramedic] = CREATE_PICKUP(PICKUP_HEALTH_STANDARD, <<131.95, -742.44, 249.15>>, iPlacementFlags)
				SET_PED_CAN_BE_TARGETTED(peds[mpf_ig8_paramedic_1].id, FALSE)
				SET_PED_CAN_BE_TARGETTED(peds[mpf_ig8_victim].id, FALSE)
				Unload_Asset_Model(sAssetData, S_M_M_PARAMEDIC_01)
				
				ADD_PED_FOR_DIALOGUE(sConvo, 8, peds[mpf_ig8_paramedic_1].id, "PARAMEDIC")
				
				syncedScenes[ssf_IG8_paramedic] = CREATE_SYNCHRONIZED_SCENE(v_ig8_coord, v_ig8_rot)
				TASK_SYNCHRONIZED_SCENE(peds[mpf_ig8_paramedic_1].id, 	syncedScenes[ssf_IG8_paramedic], "missheistfbi3b_ig8_2", "CPR_Loop_Paramedic", 	INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
				TASK_SYNCHRONIZED_SCENE(peds[mpf_ig8_victim].id, 		syncedScenes[ssf_IG8_paramedic], "missheistfbi3b_ig8_2", "CPR_Loop_Victim", 	INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
				TASK_PLAY_ANIM(peds[mpf_ig8_victim].id, "FACIALS@GEN_MALE@BASE", "Mood_Knockout_1", INSTANT_BLEND_IN, DEFAULT, -1, AF_LOOPING | AF_SECONDARY)
				SET_SYNCHRONIZED_SCENE_LOOPED(syncedScenes[ssf_IG8_paramedic], TRUE)

				mEvents[mef_ig8_paramedic].iTimeStamp = GET_GAME_TIMER()
				mEvents[mef_ig8_paramedic].iStage++
			ENDIF
		BREAK
		CASE 2	
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<126.714264,-741.724304,249.152023>>, <<137.407394,-745.618958,251.524582>>, 3.437500)
			OR IS_BULLET_IN_ANGLED_AREA(<<116.464325,-737.158081,248.962296>>, <<134.575012,-743.700562,252.652023>>, 4.812500, TRUE)
				
				// Paramedic
				syncedScenes[ssf_IG8_paramedic] = CREATE_SYNCHRONIZED_SCENE(v_ig8_coord, v_ig8_rot)
				IF peds[mpf_ig8_victim].iGenericFlag != 77
					TASK_SYNCHRONIZED_SCENE(peds[mpf_ig8_paramedic_1].id, 	syncedScenes[ssf_IG8_paramedic], "missheistfbi3b_ig8_2", "CPR_To_Cower_Paramedic", 	NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
				ENDIF
				IF peds[mpf_ig8_paramedic_1].iGenericFlag != 77	
					TASK_SYNCHRONIZED_SCENE(peds[mpf_ig8_victim].id, 		syncedScenes[ssf_IG8_paramedic], "missheistfbi3b_ig8_2", "CPR_To_Cower_Victim", 		NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
				ENDIF
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(syncedScenes[ssf_IG8_paramedic], FALSE)
				
				PLAY_PED_AMBIENT_SPEECH(peds[mpf_ig8_paramedic_1].id, "GENERIC_SHOCKED_HIGH", SPEECH_PARAMS_FORCE)
				
				// agents
				CREATE_MISSION_PED(peds[mpf_ig8_agent1], mod_fib_agent_1, << 118.2252, -741.4382, 249.1522 >>, 309.9308, "ig8_agent1", REL_ENEMY, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_INVALID, 5, 150)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_ig8_agent1].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_ig8_agent1].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_ig8_agent1].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_ig8_agent1].id, TRUE)
				SET_PED_COMBAT_MOVEMENT(peds[mpf_ig8_agent1].id, CM_WILLADVANCE)
				SET_COMBAT_FLOAT(peds[mpf_ig8_agent1].id, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
				ADD_PED_FOR_DIALOGUE(sConvo, 6, peds[mpf_ig8_agent1].id, "FBI2AGENT2")
				
				CREATE_MISSION_PED(peds[mpf_ig8_agent2], mod_fib_agent_2, << 118.1177, -742.7543, 249.1522 >>, 153.1093, "ig8_agent2", REL_ENEMY, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_INVALID, 5, 150)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_ig8_agent2].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_ig8_agent2].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_ig8_agent2].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_ig8_agent2].id, TRUE)
				SET_PED_COMBAT_MOVEMENT(peds[mpf_ig8_agent2].id, CM_DEFENSIVE)
				SET_COMBAT_FLOAT(peds[mpf_ig8_agent2].id, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
				ADD_PED_FOR_DIALOGUE(sConvo, 7, peds[mpf_ig8_agent2].id, "FBI2AGENT3")
				
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF

				mEvents[mef_ig8_paramedic].iTimeStamp = GET_GAME_TIMER()
				mEvents[mef_ig8_paramedic].iStage++	
				
			ELSE
				IF NOT IS_PED_INJURED(peds[mpf_ig8_paramedic_1].id)
				AND IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - mEvents[mef_ig8_paramedic].iTimeStamp > 10000
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(peds[mpf_ig8_paramedic_1].id)) < 7.0
						IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_PARAMED", CONV_PRIORITY_HIGH)
							mEvents[mef_ig8_paramedic].iTimeStamp = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_AGENTS1", CONV_PRIORITY_HIGH)
				
					syncedScenes[ssf_IG8_agents] = CREATE_SYNCHRONIZED_SCENE(v_ig8_coord, v_ig8_rot)
					TASK_SYNCHRONIZED_SCENE(peds[mpf_ig8_agent1].id, syncedScenes[ssf_IG8_agents], "missheistfbi3b_ig8_2", "hallyway_enter_fibagenta", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					TASK_SYNCHRONIZED_SCENE(peds[mpf_ig8_agent2].id, syncedScenes[ssf_IG8_agents], "missheistfbi3b_ig8_2", "hallyway_enter_fibagentb", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(syncedScenes[ssf_IG8_agents], FALSE)
				
					mEvents[mef_ig8_paramedic].iStage++
				ENDIF
			ENDIF
		BREAK
		CASE 4
			// Paramedic scene has not been interrupted, progress and begin the loop as normal
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG8_paramedic])
			OR GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG8_paramedic]) >= 1.0
				syncedScenes[ssf_IG8_paramedic] = CREATE_SYNCHRONIZED_SCENE(v_ig8_coord, v_ig8_rot)
				IF peds[mpf_ig8_paramedic_1].iGenericFlag != 77
					TASK_SYNCHRONIZED_SCENE(peds[mpf_ig8_paramedic_1].id, 	syncedScenes[ssf_IG8_paramedic], "missheistfbi3b_ig8_2", "Cower_Loop_Paramedic", 	NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
					TASK_LOOK_AT_ENTITY(peds[mpf_ig8_paramedic_1].id, PLAYER_PED_ID(), -1)
				ENDIF
				IF peds[mpf_ig8_victim].iGenericFlag != 77
					TASK_SYNCHRONIZED_SCENE(peds[mpf_ig8_victim].id, 		syncedScenes[ssf_IG8_paramedic], "missheistfbi3b_ig8_2", "Cower_Loop_Victim", 		NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS, RBF_PLAYER_IMPACT)
				ENDIF
				SET_SYNCHRONIZED_SCENE_LOOPED(syncedScenes[ssf_IG8_paramedic], TRUE)
				
				mEvents[mef_ig8_paramedic].iTimeStamp = GET_GAME_TIMER()
				mEvents[mef_ig8_paramedic].iStage++
			ENDIF
		BREAK
		CASE 5
			IF IS_PED_INJURED(peds[mpf_ig8_paramedic_1].id)
			AND IS_PED_INJURED(peds[mpf_ig8_victim].id)
			AND IS_PED_INJURED(peds[mpf_ig8_agent1].id)
			AND IS_PED_INJURED(peds[mpf_ig8_agent2].id)
				Unload_Asset_Anim_Dict(sAssetData, "missheistfbi3b_ig8_2")
				Unload_Asset_Anim_Dict(sAssetData, "FACIALS@GEN_MALE@BASE")
				Event_Finish(mef_ig8_paramedic)
			ELSE
				IF IS_PED_INJURED(peds[mpf_ig8_agent1].id)
				AND IS_PED_INJURED(peds[mpf_ig8_agent2].id)
					IF NOT IS_PED_INJURED(peds[mpf_ig8_paramedic_1].id)
					AND NOT IS_PED_INJURED(peds[mpf_ig8_victim].id)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(peds[mpf_ig8_paramedic_1].id)) < 7.0
							IF IS_SAFE_TO_START_CONVERSATION()
							AND GET_GAME_TIMER() - mEvents[mef_ig8_paramedic].iTimeStamp > 20000
								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_PARAMED2", CONV_PRIORITY_HIGH)
									mEvents[mef_ig8_paramedic].iTimeStamp = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC me_ig9_rappel_dismount_player()

	SWITCH mEvents[mef_ig9_dismount_player].iStage
		CASE 1
			// move on once everyone is on the ground
			IF PLAYER_PED_ID() = MIKE_PED_ID()
				CANCEL_RAPPEL(s_rappel_mike.sRappelData, 	TRUE, DEFAULT, DEFAULT, DEFAULT, FALSE)
			ELSE
				CANCEL_RAPPEL(s_rappel_frank.sRappelData, 	TRUE, DEFAULT, DEFAULT, DEFAULT, FALSE)
			ENDIF
		
			syncedScenes[ssf_IG9_rappel_dismount_player] 	= CREATE_SYNCHRONIZED_SCENE(v_ig9_coord, v_ig9_rot)
			
			IF PLAYER_PED_ID() = MIKE_PED_ID()
				TASK_SYNCHRONIZED_SCENE(MIKE_PED_ID(),	syncedScenes[ssf_IG9_rappel_dismount_player], 	animDict_IG9_dismount, "Rappel_Dismount_Michael", 		WALK_BLEND_IN, 	SLOW_BLEND_OUT, 	SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS, DEFAULT, WALK_BLEND_IN)
				PLAY_ENTITY_ANIM(s_rappel_mike.sRappelData.pulleyObject, 	"Rappel_Dismount_Michael_Prop", 	animDict_IG9_dismount, WALK_BLEND_IN, FALSE, TRUE)
				bExitStateMichael 	= FALSE
				bWeaponSetMichael	= FALSE
			ELSE
				TASK_SYNCHRONIZED_SCENE(FRANK_PED_ID(), syncedScenes[ssf_IG9_rappel_dismount_player], 	animDict_IG9_dismount, "Rappel_Dismount_Alt_Franklin", 	WALK_BLEND_IN, 	SLOW_BLEND_OUT, 	SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS, DEFAULT, WALK_BLEND_IN)
				PLAY_ENTITY_ANIM(s_rappel_frank.sRappelData.pulleyObject, 	"Rappel_Dismount_Alt_Franklin_Prop", 	animDict_IG9_dismount, WALK_BLEND_IN, FALSE, TRUE)
				bWeaponSetFrank		= FALSE
				bExitStateFrank 	= FALSE
			ENDIF

			REPLAY_RECORD_BACK_FOR_TIME(5.0, 13.0, REPLAY_IMPORTANCE_HIGHEST)

			mEvents[mef_ig9_dismount_player].iStage++
		BREAK
		CASE 2
			IF PLAYER_PED_ID() = MIKE_PED_ID()
				IF s_rappel_mike.bIsRappeling
			
					IF IS_ENTITY_PLAYING_ANIM(MIKE_PED_ID(), animDict_IG9_dismount, "Rappel_Dismount_Michael")
						
						HANDLE_RAPPEL_ROPE(s_rappel_mike.sRappelData)
						IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG9_rappel_dismount_player]) >= 0.059
							CPRINTLN(DEBUG_MIKE, "[MIKE] Detatched from rope\n\n\n")
							//UNPIN_ALL_ROPE_VERTICES_EXCEPT_ANCHOR(s_rappel_mike.sRappelData)
							IF IS_ENTITY_ATTACHED(s_rappel_mike.sRappelData.pulleyObject)
								DETACH_ENTITY(s_rappel_mike.sRappelData.pulleyObject)
							ENDIF
							FREEZE_ENTITY_POSITION(s_rappel_mike.sRappelData.pulleyObject, TRUE)
							s_rappel_mike.bIsRappeling = FALSE
						ENDIF
					
					ENDIF
				ELSE
					IF PLAYER_PED_ID() = MIKE_PED_ID()
						OVERRIDE_PLAYER_GROUND_MATERIAL(GET_HASH_KEY("AM_BASE_GLASS_STRONG"), FALSE)
					ENDIF
				
					// Player controlled ped
					IF IS_ENTITY_PLAYING_ANIM(MIKE_PED_ID(), animDict_IG9_dismount, "Rappel_Dismount_Michael")
					
						IF NOT bWeaponSetMichael
							IF HAS_ANIM_EVENT_FIRED(MIKE_PED_ID(), GET_HASH_KEY("WeaponAppear"))
								SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), WEAPONTYPE_CARBINERIFLE, TRUE)
								CPRINTLN(DEBUG_MIKE, "[MIKE] WeaponAppear: Fired @ ", GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG9_rappel_dismount_player]), "\n\n\n")
								bWeaponSetMichael = TRUE
							ENDIF
						ENDIF
							
						IF NOT bExitStateMichael
							IF HAS_ANIM_EVENT_FIRED(MIKE_PED_ID(), GET_HASH_KEY("interrupt"))
								CPRINTLN(DEBUG_MIKE, "[MIKE] Interrupt: Enabled @ ", GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG9_rappel_dismount_player]), "\n\n\n")
								bExitStateMichael = TRUE
							ENDIF
						ENDIF
						
						IF bExitStateMichael
							IF GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD) != 0
							OR GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) != 0
							OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
							OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)
							OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK2)
								CPRINTLN(DEBUG_MIKE, "[MIKE] Player Interrupted @ ", GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG9_rappel_dismount_player]),"\n\n\n")
								CLEAR_PED_TASKS(MIKE_PED_ID())		
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ELSE
			
				IF s_rappel_frank.bIsRappeling
			
					IF IS_ENTITY_PLAYING_ANIM(FRANK_PED_ID(), animDict_IG9_dismount, "Rappel_Dismount_Alt_Franklin")
					
						HANDLE_RAPPEL_ROPE(s_rappel_frank.sRappelData)
						IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG9_rappel_dismount_player]) >= 0.209
							CPRINTLN(DEBUG_MIKE, "[FRANK] Detatched from rope\n\n\n")
							//UNPIN_ALL_ROPE_VERTICES_EXCEPT_ANCHOR(s_rappel_frank.sRappelData)
							IF IS_ENTITY_ATTACHED(s_rappel_frank.sRappelData.pulleyObject)
								DETACH_ENTITY(s_rappel_frank.sRappelData.pulleyObject)
							ENDIF
							FREEZE_ENTITY_POSITION(s_rappel_frank.sRappelData.pulleyObject, TRUE)
							
							s_rappel_frank.bIsRappeling = FALSE
						ENDIF

					ENDIF
				
				ELSE
				
					IF PLAYER_PED_ID() = FRANK_PED_ID()
						OVERRIDE_PLAYER_GROUND_MATERIAL(GET_HASH_KEY("AM_BASE_GLASS_STRONG"), FALSE)
					ENDIF
				
					// Player controlled ped
					IF IS_ENTITY_PLAYING_ANIM(FRANK_PED_ID(), animDict_IG9_dismount, "Rappel_Dismount_Alt_Franklin")
						
						IF NOT bWeaponSetFrank
							IF HAS_ANIM_EVENT_FIRED(FRANK_PED_ID(), GET_HASH_KEY("WeaponAppear"))
								SET_CURRENT_PED_WEAPON(FRANK_PED_ID(), WEAPONTYPE_CARBINERIFLE, TRUE)
								CPRINTLN(DEBUG_MIKE, "[FRANK] WeaponAppear: Fired\n\n\n")
								bWeaponSetFrank = TRUE
							ENDIF
						ENDIF
							
						IF NOT bExitStateFrank
							IF HAS_ANIM_EVENT_FIRED(FRANK_PED_ID(), GET_HASH_KEY("interrupt"))
								CPRINTLN(DEBUG_MIKE, "[FRANK] Interrupt: Enabled\n\n\n")
								bExitStateFrank = TRUE
							ENDIF
						ENDIF
						
						IF bExitStateFrank
							IF GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_UD) != 0
							OR GET_CONTROL_NORMAL(PLAYER_CONTROL, INPUT_MOVE_LR) != 0
							OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
							OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)
							OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK2)
								CLEAR_PED_TASKS(FRANK_PED_ID())		
								CPRINTLN(DEBUG_MIKE, "[FRANK] Player Interrupted!\n\n\n")
							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
			
			ENDIF
	
			
		BREAK
	ENDSWITCH
	
	
	IF mEvents[mef_ig9_dismount_player].iStage = 2
		// Disable camera controls until player has exited anim
		IF (PLAYER_PED_ID() = MIKE_PED_ID() AND NOT bExitStateMichael)
		OR (PLAYER_PED_ID() = FRANK_PED_ID() AND NOT bExitStateFrank)
		
			IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
				DISPLAY_HUD(TRUE)
				DISPLAY_RADAR(TRUE)
				RENDER_SCRIPT_CAMS(FALSE, TRUE, 3000)
			ENDIF
			
			// Disable messing with the camera while the interp is happening
			IF IS_INTERPOLATING_FROM_SCRIPT_CAMS()
				SET_GAMEPLAY_CAM_WORLD_HEADING(339.1674)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_UD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_LR)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
			ENDIF
		ENDIF
		
		IF (PLAYER_PED_ID() = MIKE_PED_ID()
		AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(MIKE_PED_ID(), SCRIPT_TASK_SYNCHRONIZED_SCENE) 
		AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(MIKE_PED_ID(), SCRIPT_TASK_PLAY_ANIM))
		OR (PLAYER_PED_ID() = FRANK_PED_ID()
		AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(FRANK_PED_ID(), SCRIPT_TASK_SYNCHRONIZED_SCENE) 
		AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(FRANK_PED_ID(), SCRIPT_TASK_PLAY_ANIM))

			Event_Finish(mef_ig9_dismount_player)
		ENDIF
	ENDIF

ENDPROC

PROC me_ig9_rappel_dismount_buddies()

	SWITCH mEvents[mef_ig9_dismount_buddies].iStage
		CASE 1
			// move on once everyone is on the ground
			IF BUDDY_PED_ID() = MIKE_PED_ID()
				CANCEL_RAPPEL(s_rappel_mike.sRappelData, 	FALSE, DEFAULT, DEFAULT, DEFAULT, FALSE)
			ELSE
				CANCEL_RAPPEL(s_rappel_frank.sRappelData, 	FALSE, DEFAULT, DEFAULT, DEFAULT, FALSE)
			ENDIF
			
			CANCEL_RAPPEL(s_rappel_goon.sRappelData, 	FALSE, DEFAULT, DEFAULT, DEFAULT, FALSE)
		
			syncedScenes[ssf_IG9_rappel_dismount_buddies] 	= CREATE_SYNCHRONIZED_SCENE(v_ig9_coord, v_ig9_rot)
			
			IF BUDDY_PED_ID() = MIKE_PED_ID()
				TASK_SYNCHRONIZED_SCENE(MIKE_PED_ID(), 	syncedScenes[ssf_IG9_rappel_dismount_buddies], 	animDict_IG9_dismount, "Rappel_Dismount_Alt_Michael", 	WALK_BLEND_IN, 	SLOW_BLEND_OUT, 	SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS, DEFAULT, WALK_BLEND_IN)
				PLAY_ENTITY_ANIM(s_rappel_mike.sRappelData.pulleyObject, 	"Rappel_Dismount_Alt_Michael_Prop", 	animDict_IG9_dismount, WALK_BLEND_IN, FALSE, TRUE)
				bExitStateMichael 	= FALSE
				bWeaponSetMichael	= FALSE
			ELSE
				TASK_SYNCHRONIZED_SCENE(FRANK_PED_ID(),	syncedScenes[ssf_IG9_rappel_dismount_buddies], 	animDict_IG9_dismount, "Rappel_Dismount_Franklin", 		WALK_BLEND_IN, 	SLOW_BLEND_OUT, 	SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS, DEFAULT, WALK_BLEND_IN)
				PLAY_ENTITY_ANIM(s_rappel_frank.sRappelData.pulleyObject, 	"Rappel_Dismount_Franklin_Prop", 	animDict_IG9_dismount, WALK_BLEND_IN, FALSE, TRUE)
				bWeaponSetFrank		= FALSE
				bExitStateFrank 	= FALSE
			ENDIF
			
			// Gunman dismount
			TASK_SYNCHRONIZED_SCENE(peds[mpf_goon].id, 	syncedScenes[ssf_IG9_rappel_dismount_buddies], 	animDict_IG9_dismount, "Rappel_Dismount_Gunman", 	WALK_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS, DEFAULT, WALK_BLEND_IN)
			PLAY_ENTITY_ANIM(s_rappel_goon.sRappelData.pulleyObject, "Rappel_Dismount_Gunman_Prop", 	animDict_IG9_dismount, WALK_BLEND_IN, FALSE, TRUE)
			
			mEvents[mef_ig9_dismount_buddies].iStage++
		BREAK
		CASE 2
		
			IF BUDDY_PED_ID() = MIKE_PED_ID()

				IF s_rappel_mike.bIsRappeling
				
					IF IS_ENTITY_PLAYING_ANIM(MIKE_PED_ID(), animDict_IG9_dismount, "Rappel_Dismount_Alt_Michael")
					
						HANDLE_RAPPEL_ROPE(s_rappel_mike.sRappelData)
						IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG9_rappel_dismount_buddies]) >= 0.048
							CPRINTLN(DEBUG_MIKE, "[MIKE] Detatched from rope\n\n\n")
							//UNPIN_ALL_ROPE_VERTICES_EXCEPT_ANCHOR(s_rappel_mike.sRappelData)
							IF IS_ENTITY_ATTACHED(s_rappel_mike.sRappelData.pulleyObject)
								DETACH_ENTITY(s_rappel_mike.sRappelData.pulleyObject)
							ENDIF
							FREEZE_ENTITY_POSITION(s_rappel_mike.sRappelData.pulleyObject, TRUE)
							s_rappel_mike.bIsRappeling = FALSE
						ENDIF
					
					ENDIF
				ELSE
				
					IF IS_ENTITY_PLAYING_ANIM(MIKE_PED_ID(), animDict_IG9_dismount, "Rappel_Dismount_Alt_Michael")
					
						IF HAS_ANIM_EVENT_FIRED(MIKE_PED_ID(), GET_HASH_KEY("interrupt"))
							vGoToExactlyBuddy = GET_ACTUAL_GROUND_COORD(GET_ANIM_INITIAL_OFFSET_POSITION(animDict_IG9_dismount, "Rappel_Dismount_Alt_Michael", v_ig9_coord, v_ig9_rot, 1.0))
							Create_ped_Cover_Point(peds[mpf_mike], vGoToExactlyBuddy, 250.8777, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_180)
							SET_PED_COMBAT_PARAMS(peds[mpf_mike], vGoToExactlyBuddy, 2.0, CM_DEFENSIVE, CR_FAR, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.0)
						
							SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), WEAPONTYPE_CARBINERIFLE, TRUE)
							TASK_PUT_PED_DIRECTLY_INTO_COVER(MIKE_PED_ID(), GET_SCRIPTED_COVER_POINT_COORDS(peds[mpf_mike].cov), -1, FALSE, 0.25, TRUE, TRUE, peds[mpf_mike].cov)
							CPRINTLN(DEBUG_MIKE, "[MIKE] Interrupted into cover")
						ENDIF
						
					ENDIF
				ENDIF
				
			ENDIF
			
			IF BUDDY_PED_ID() = FRANK_PED_ID()
				
				IF s_rappel_frank.bIsRappeling
				
					IF IS_ENTITY_PLAYING_ANIM(FRANK_PED_ID(), animDict_IG9_dismount, "Rappel_Dismount_Franklin")
						
						HANDLE_RAPPEL_ROPE(s_rappel_frank.sRappelData)
						IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG9_rappel_dismount_buddies]) >= 0.048
							CPRINTLN(DEBUG_MIKE, "[FRANK] Detatched from rope\n\n\n")
							//UNPIN_ALL_ROPE_VERTICES_EXCEPT_ANCHOR(s_rappel_frank.sRappelData)
							IF IS_ENTITY_ATTACHED(s_rappel_frank.sRappelData.pulleyObject)
								DETACH_ENTITY(s_rappel_frank.sRappelData.pulleyObject)
							ENDIF
							FREEZE_ENTITY_POSITION(s_rappel_frank.sRappelData.pulleyObject, TRUE)
							s_rappel_frank.bIsRappeling = FALSE
						ENDIF
					
					ELIF IS_ENTITY_PLAYING_ANIM(FRANK_PED_ID(), animDict_IG9_dismount, "Rappel_Dismount_Alt_Franklin")
					
						HANDLE_RAPPEL_ROPE(s_rappel_frank.sRappelData)
						IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG9_rappel_dismount_buddies]) >= 0.209
							CPRINTLN(DEBUG_MIKE, "[FRANK] Detatched from rope\n\n\n")
							//UNPIN_ALL_ROPE_VERTICES_EXCEPT_ANCHOR(s_rappel_frank.sRappelData)
							IF IS_ENTITY_ATTACHED(s_rappel_frank.sRappelData.pulleyObject)
								DETACH_ENTITY(s_rappel_frank.sRappelData.pulleyObject)
							ENDIF
							FREEZE_ENTITY_POSITION(s_rappel_frank.sRappelData.pulleyObject, TRUE)
							
							s_rappel_frank.bIsRappeling = FALSE
						ENDIF

					ENDIF
				
				ELSE
				
					IF IS_ENTITY_PLAYING_ANIM(FRANK_PED_ID(), animDict_IG9_dismount, "Rappel_Dismount_Franklin")
				
						IF HAS_ANIM_EVENT_FIRED(FRANK_PED_ID(), GET_HASH_KEY("interrupt"))
							vGoToExactlyBuddy = GET_ACTUAL_GROUND_COORD(GET_ANIM_INITIAL_OFFSET_POSITION(animDict_IG9_dismount, "Rappel_Dismount_Franklin", v_ig9_coord, v_ig9_rot, 1.0))
							Create_ped_Cover_Point(peds[mpf_frank], vGoToExactlyBuddy, 254.8745, COVUSE_WALLTONEITHER, COVHEIGHT_LOW, COVARC_180)
							SET_PED_COMBAT_PARAMS(peds[mpf_frank], vGoToExactlyBuddy, 2.0, CM_DEFENSIVE, CR_FAR, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.0)
						
							SET_CURRENT_PED_WEAPON(FRANK_PED_ID(), WEAPONTYPE_CARBINERIFLE, TRUE)
							TASK_PUT_PED_DIRECTLY_INTO_COVER(FRANK_PED_ID(), GET_SCRIPTED_COVER_POINT_COORDS(peds[mpf_frank].cov), -1, FALSE, 0.25, TRUE, TRUE, peds[mpf_frank].cov)
							CPRINTLN(DEBUG_MIKE, "[FRANK] Interrupted into cover")
						ENDIF
						
					ENDIF
				ENDIF
					
			ENDIF

			IF s_rappel_goon.bIsRappeling
				HANDLE_RAPPEL_ROPE(s_rappel_goon.sRappelData)
				IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG9_rappel_dismount_buddies]) >= 0.082
					CPRINTLN(DEBUG_MIKE, "[GUNMAN] Detached from rope")
					//UNPIN_ALL_ROPE_VERTICES_EXCEPT_ANCHOR(s_rappel_goon.sRappelData)
					IF IS_ENTITY_ATTACHED(s_rappel_goon.sRappelData.pulleyObject)
						DETACH_ENTITY(s_rappel_goon.sRappelData.pulleyObject)
					ENDIF
					FREEZE_ENTITY_POSITION(s_rappel_goon.sRappelData.pulleyObject, TRUE)
					s_rappel_goon.bIsRappeling = FALSE
				ENDIF
			ELSE
				IF IS_ENTITY_PLAYING_ANIM(peds[mpf_goon].id, animDict_IG9_dismount, "Rappel_Dismount_Gunman")
					IF HAS_ANIM_EVENT_FIRED(peds[mpf_goon].id, GET_HASH_KEY("interrupt"))
						vGoToExactlyGunman = GET_ACTUAL_GROUND_COORD(GET_ANIM_INITIAL_OFFSET_POSITION(animDict_IG9_dismount, "Rappel_Dismount_Gunman", v_ig9_coord, v_ig9_rot, 1.0))
						Create_ped_Cover_Point(peds[mpf_goon], vGoToExactlyGunman, 250.8777, COVUSE_WALLTOBOTH, COVHEIGHT_LOW, COVARC_180)
						SET_PED_COMBAT_PARAMS(peds[mpf_goon], vGoToExactlyGunman, 2.0, CM_DEFENSIVE, CR_FAR, TLR_NEVER_LOSE_TARGET, TRUE, 0.5, 0.0)
						
						SET_CURRENT_PED_WEAPON(peds[mpf_goon].id, weap_Goon, TRUE)
						TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_goon].id, GET_SCRIPTED_COVER_POINT_COORDS(peds[mpf_goon].cov), -1, FALSE, 0.5, TRUE, TRUE, peds[mpf_goon].cov)
						
						CPRINTLN(DEBUG_MIKE, "[GUNMAN] Tasked to cover and carbine equipped")
					ENDIF
				ENDIF
			ENDIF
			
			IF (BUDDY_PED_ID() = MIKE_PED_ID()
			AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(MIKE_PED_ID(), SCRIPT_TASK_SYNCHRONIZED_SCENE) 
			AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(MIKE_PED_ID(), SCRIPT_TASK_PLAY_ANIM))
			OR (BUDDY_PED_ID() = FRANK_PED_ID() 
			AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(FRANK_PED_ID(), SCRIPT_TASK_SYNCHRONIZED_SCENE) 
			AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(FRANK_PED_ID(), SCRIPT_TASK_PLAY_ANIM))
			AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_SYNCHRONIZED_SCENE)
				Unload_Asset_Anim_Dict(sAssetData, animDict_IG9_dismount)
				Event_Finish(mef_ig9_dismount_buddies)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC me_pre_crashed_heli_corridor()

	SWITCH mEvents[mef_pre_crashed_heli_corridor].iStage
		CASE 1
			Load_Asset_Model(sAssetData, mod_fib_agent_1)
			Load_Asset_Model(sAssetData, mod_fib_agent_2)
			Load_Asset_Model(sAssetData, mod_fib_agent_3)
		
			IF HAS_MODEL_LOADED(mod_fib_agent_1)
			AND HAS_MODEL_LOADED(mod_fib_agent_2)
			AND HAS_MODEL_LOADED(mod_fib_agent_3)
				CREATE_MISSION_PED(peds[mpf_before_heli_fall_1], mod_fib_agent_1, <<130.2159, -746.9586, 253.1523>>, 49.3587, "b4HeliFall1", REL_ENEMY, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_INVALID, 5)
				SET_PED_COMBAT_PARAMS(peds[mpf_before_heli_fall_1], <<130.2159, -746.9586, 253.1523>>, 1.0, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0, TRUE, FALSE, TRUE)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_before_heli_fall_1].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_before_heli_fall_1].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_before_heli_fall_1].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_before_heli_fall_1].id, TRUE)
				
				CREATE_MISSION_PED(peds[mpf_before_heli_fall_2], mod_fib_agent_2, <<131.2902, -745.9695, 253.1988>>, 42.4347, "b4HeliFall2", REL_ENEMY, WEAPONTYPE_SMG, WEAPONCOMPONENT_INVALID, 5)
				SET_PED_COMBAT_PARAMS(peds[mpf_before_heli_fall_2], <<0,0,0>>, 0.0, CM_WILLADVANCE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0, TRUE, FALSE, TRUE, TRUE)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_before_heli_fall_2].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_before_heli_fall_2].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_before_heli_fall_2].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_before_heli_fall_2].id, TRUE)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_before_heli_fall_2].id, <<131.2902, -745.9695, 253.1988>>, -1, FALSE, 0, TRUE, FALSE, NULL, FALSE)
				
				CREATE_MISSION_PED(peds[mpf_before_heli_fall_3], mod_fib_agent_3, <<139.7227, -744.6555, 253.1821>>, 73.4282, "b4HeliFall3", REL_ENEMY, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_INVALID, 5)
				SET_PED_COMBAT_PARAMS(peds[mpf_before_heli_fall_3], <<139.7227, -744.6555, 253.1821>>, 1.0, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0, TRUE, FALSE, TRUE)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_before_heli_fall_3].id, 0.2)
				SET_PED_ENVEFF_SCALE(peds[mpf_before_heli_fall_3].id, 0.65)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_before_heli_fall_3].id, 74,69,60)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_before_heli_fall_3].id, TRUE)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_before_heli_fall_3].id, <<139.7227, -744.6555, 253.1821>>, -1, FALSE, 0, TRUE, TRUE, NULL, FALSE)
				
				CREATE_MISSION_PED(peds[mpf_before_heli_fall_4], mod_fib_agent_2, <<149.8246, -748.4926, 253.1521>>, 117.3324, "b4HeliFall4", REL_ENEMY, WEAPONTYPE_PUMPSHOTGUN, WEAPONCOMPONENT_INVALID, 1)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_before_heli_fall_4].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_before_heli_fall_4].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_before_heli_fall_4].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_before_heli_fall_4].id, TRUE)
				SET_PED_COMBAT_PARAMS(peds[mpf_before_heli_fall_4], <<0,0,0>>, 0.0, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0, TRUE, FALSE, TRUE)
				
				CREATE_MISSION_PED(peds[mpf_before_heli_fall_5], mod_fib_agent_1, <<153.2197, -741.2181, 253.1521>>, 161.2965, "b4HeliFall5", REL_ENEMY, WEAPONTYPE_SMG, WEAPONCOMPONENT_INVALID, 5)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_before_heli_fall_5].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_before_heli_fall_5].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_before_heli_fall_5].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_before_heli_fall_5].id, TRUE)
				SET_PED_COMBAT_PARAMS(peds[mpf_before_heli_fall_5], <<148.4940, -750.9575, 253.1711>>, 1.0, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0, FALSE, FALSE, FALSE)
				
				mEvents[mef_pre_crashed_heli_corridor].iStage++
			ENDIF
		BREAK
		CASE 2
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), 	<<116.323372,-740.080566,253.152542>>, <<117.880081,-735.824402,256.471313>>, 6.187500)
			OR IS_ENTITY_IN_ANGLED_AREA(BUDDY_PED_ID(), 	<<116.323372,-740.080566,253.152542>>, <<117.880081,-735.824402,256.471313>>, 6.187500)
			OR IS_ENTITY_IN_ANGLED_AREA(peds[mpf_goon].id, 	<<116.323372,-740.080566,253.152542>>, <<117.880081,-735.824402,256.471313>>, 6.187500)
			
				// Enemy AI
				IF NOT IS_PED_INJURED(peds[mpf_before_heli_fall_1].id)
					TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_before_heli_fall_1].id, <<118.7768, -739.8790, 253.1525>>, 30.0)
				ENDIF
				
				IF NOT IS_PED_INJURED(peds[mpf_before_heli_fall_2].id)
					SET_PED_CONFIG_FLAG(peds[mpf_before_heli_fall_2].id, PCF_ShouldChargeNow, TRUE)
					TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_before_heli_fall_2].id, <<118.7768, -739.8790, 253.1525>>, 30.0)
				ENDIF
				
				IF NOT IS_PED_INJURED(peds[mpf_before_heli_fall_3].id)
					TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_before_heli_fall_3].id, <<118.7768, -739.8790, 253.1525>>, 30.0)
				ENDIF
				
				mEvents[mef_pre_crashed_heli_corridor].iStage++
			ENDIF
		BREAK
		CASE 3
			
			IF DOES_ENTITY_EXIST(peds[mpf_before_heli_fall_4].id)
			AND NOT IS_PED_INJURED(peds[mpf_before_heli_fall_4].id)
				SWITCH peds[mpf_before_heli_fall_4].iGenericFlag 
					CASE 0
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<116.323372,-740.080566,253.152542>>, <<117.880081,-735.824402,256.471313>>, 6.187500)
						OR GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("v_fib03_it3_hall")
						
							SET_PED_CONFIG_FLAG(peds[mpf_before_heli_fall_4].id, PCF_ShouldChargeNow, TRUE)
							SET_PED_COMBAT_MOVEMENT(peds[mpf_before_heli_fall_4].id, CM_WILLADVANCE)
							TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_before_heli_fall_4].id, <<118.7768, -739.8790, 253.1525>>, 30.0)
							
							peds[mpf_before_heli_fall_4].iGenericFlag++
							
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
					
			IF DOES_ENTITY_EXIST(peds[mpf_before_heli_fall_5].id)
			AND NOT IS_PED_INJURED(peds[mpf_before_heli_fall_5].id)
				SWITCH peds[mpf_before_heli_fall_5].iGenericFlag
					CASE 0
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<116.323372,-740.080566,253.152542>>, <<117.880081,-735.824402,256.471313>>, 6.187500)
						OR GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("v_fib03_it3_hall")
					
							TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_before_heli_fall_5].id, <<118.7768, -739.8790, 253.1525>>, 30.0)
							peds[mpf_before_heli_fall_5].iGenericFlag++
						
						ENDIF
					BREAK
					CASE 1
						INT iCount
						IF NOT DOES_ENTITY_EXIST(peds[mpf_before_heli_fall_1].id) OR IS_PED_INJURED(peds[mpf_before_heli_fall_1].id)
							iCount++
						ENDIF
						IF NOT DOES_ENTITY_EXIST(peds[mpf_before_heli_fall_2].id) OR IS_PED_INJURED(peds[mpf_before_heli_fall_2].id)
							iCount++
						ENDIF
						IF NOT DOES_ENTITY_EXIST(peds[mpf_before_heli_fall_3].id) OR IS_PED_INJURED(peds[mpf_before_heli_fall_3].id)
							iCount++
						ENDIF
						
						IF iCount >= 2
							SET_PED_COMBAT_PARAMS(peds[mpf_before_heli_fall_5], <<131.2356, -745.8246, 253.2033>>, 1.0, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0, TRUE, FALSE, TRUE)
							peds[mpf_before_heli_fall_5].iGenericFlag++
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF

			// Create a nev mesh blocker the prevent enemies wandering back in to the heli room
			IF i_NavMeshBlockingAreas[3] = -1
			
				BOOL bCreateBlocker 
				bCreateBlocker = TRUE
				MISSION_PED_FLAGS ePed
				FOR ePed = mpf_before_heli_fall_1 TO mpf_before_heli_fall_5
					IF DOES_ENTITY_EXIST(peds[ePed].id)
					AND NOT IS_PED_INJURED(peds[ePed].id)
						IF IS_ENTITY_IN_ANGLED_AREA(peds[ePed].id, <<150.947632,-747.709778,253.152054>>, <<154.239410,-739.635925,256.651917>>, 11.500000)
							bCreateBlocker = FALSE
						ENDIF
					ENDIF
				ENDFOR
				
				IF bCreateBlocker
					i_NavMeshBlockingAreas[3] = ADD_NAVMESH_BLOCKING_OBJECT(<<150.780396,-747.611511,254.152054>>, <<1.000000,1.000000,1.000000>>, 0, FALSE)
				ENDIF
			
			ENDIF
			
			IF 	(NOT DOES_ENTITY_EXIST(peds[mpf_before_heli_fall_1].id) OR IS_PED_INJURED(peds[mpf_before_heli_fall_1].id))
			AND (NOT DOES_ENTITY_EXIST(peds[mpf_before_heli_fall_2].id) OR IS_PED_INJURED(peds[mpf_before_heli_fall_2].id))
			AND (NOT DOES_ENTITY_EXIST(peds[mpf_before_heli_fall_3].id) OR IS_PED_INJURED(peds[mpf_before_heli_fall_3].id))
			AND (NOT DOES_ENTITY_EXIST(peds[mpf_before_heli_fall_4].id) OR IS_PED_INJURED(peds[mpf_before_heli_fall_4].id))
			AND (NOT DOES_ENTITY_EXIST(peds[mpf_before_heli_fall_5].id) OR IS_PED_INJURED(peds[mpf_before_heli_fall_5].id))
				IF i_NavMeshBlockingAreas[3] != -1
					REMOVE_NAVMESH_BLOCKING_OBJECT(i_NavMeshBlockingAreas[3])
					i_NavMeshBlockingAreas[3] = -1
				ENDIF
				Event_Finish(mef_pre_crashed_heli_corridor)
			ENDIF

		BREAK
	ENDSWITCH

ENDPROC

PROC me_comms_2()
	SWITCH mEvents[mef_comms_2].iStage
		CASE 1
			Load_Asset_Model(sAssetData, mod_fib_agent_1)
			Load_Asset_Model(sAssetData, mod_fib_agent_2)
		
			IF HAS_MODEL_LOADED(mod_fib_agent_1)
			AND HAS_MODEL_LOADED(mod_fib_agent_2)
				
				// new peds in the next room
				CREATE_MISSION_PED(peds[mpf_2nd_comms_room_1], mod_fib_agent_2, 	<< 135.0431, -756.7064, 249.1520 >>, 73.7667, "Coms2nd_1", REL_ENEMY, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_INVALID, 5)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_2nd_comms_room_1].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_2nd_comms_room_1].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_2nd_comms_room_1].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_2nd_comms_room_1].id, TRUE)
				CREATE_MISSION_PED(peds[mpf_2nd_comms_room_2], mod_fib_agent_1, 	<< 140.5883, -757.4700, 249.1520 >>, 65.5300, "Coms2nd_2", REL_ENEMY, WEAPONTYPE_PUMPSHOTGUN, WEAPONCOMPONENT_INVALID, 2)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_2nd_comms_room_2].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_2nd_comms_room_2].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_2nd_comms_room_2].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_2nd_comms_room_2].id, TRUE)
				Task_Put_Ped_Directly_Into_Custom_Cover(peds[mpf_2nd_comms_room_1], << 135.0431, -756.7064, 249.1520 >>, 73.766, -1, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_90, FALSE, 0, TRUE, TRUE)
				
				Unload_Asset_Model(sAssetData, mod_fib_agent_2)
				mEvents[mef_comms_2].iStage++
			ENDIF
		BREAK
		CASE 2
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<124.639000,-756.054688,249.152145>>, <<122.306374,-755.039734,251.402206>>, 2.250000)
			OR IS_ENTITY_IN_ANGLED_AREA(BUDDY_PED_ID(), <<124.639000,-756.054688,249.152145>>, <<122.306374,-755.039734,251.402206>>, 2.250000)
				mEvents[mef_comms_2].iStage++
			ENDIF
		BREAK
		CASE 3
			IF DOES_ENTITY_EXIST(peds[mpf_2nd_comms_room_1].id) 
			AND NOT IS_PED_INJURED(peds[mpf_2nd_comms_room_1].id)
				SWITCH peds[mpf_2nd_comms_room_1].iGenericFlag
					CASE 0
						SET_PED_COMBAT_PARAMS(peds[mpf_2nd_comms_room_1], << 135.0431, -756.7064, 249.1520 >>, 1.0, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, FALSE, FALSE, FALSE)
						TASK_COMBAT_PED(peds[mpf_2nd_comms_room_1].id, PLAYER_PED_ID())
						peds[mpf_2nd_comms_room_1].iGenericFlag++
					BREAK
					CASE 1
						// Move ped to charge you once you kill the shotgun guy
						IF NOT DOES_ENTITY_EXIST(peds[mpf_2nd_comms_room_2].id)
						OR IS_PED_INJURED(peds[mpf_2nd_comms_room_2].id)
							REMOVE_PED_DEFENSIVE_AREA(peds[mpf_2nd_comms_room_1].id)
							SET_PED_COMBAT_MOVEMENT(peds[mpf_2nd_comms_room_1].id, CM_WILLADVANCE)
							SET_PED_CONFIG_FLAG(peds[mpf_2nd_comms_room_1].id, PCF_ShouldChargeNow, TRUE)
							peds[mpf_2nd_comms_room_1].iGenericFlag++	
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			
			IF DOES_ENTITY_EXIST(peds[mpf_2nd_comms_room_2].id) 
			AND NOT IS_PED_INJURED(peds[mpf_2nd_comms_room_2].id)
				SWITCH peds[mpf_2nd_comms_room_2].iGenericFlag
					CASE 0
						SET_PED_COMBAT_PARAMS(peds[mpf_2nd_comms_room_2], <<0,0,0>>, 0.0, CM_WILLADVANCE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE, FALSE, TRUE, TRUE)
						TASK_COMBAT_PED(peds[mpf_2nd_comms_room_2].id, PLAYER_PED_ID())
						peds[mpf_2nd_comms_room_2].iGenericFlag++
					BREAK
				ENDSWITCH
			ENDIF
		
			IF IS_PED_INJURED(peds[mpf_2nd_comms_room_1].id)
			AND IS_PED_INJURED(peds[mpf_2nd_comms_room_2].id)
				Event_Finish(mef_comms_2)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC me_rappel_room()
	SWITCH mEvents[mef_rappel_room].iStage
		CASE 1
			Load_Asset_Model(sAssetData, mod_fib_agent_1)
			Load_Asset_Model(sAssetData, mod_fib_agent_3)
			IF HAS_MODEL_LOADED(mod_fib_agent_1)
			AND HAS_MODEL_LOADED(mod_fib_agent_3)
			
				CREATE_MISSION_PED(peds[mpf_final_rappel_room_1], mod_fib_agent_1, <<151.9832, -763.7991, 245.1521>>, 159.3465, "EndRappel_1", REL_ENEMY, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_INVALID, 5)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_final_rappel_room_1].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_final_rappel_room_1].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_final_rappel_room_1].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_final_rappel_room_1].id, TRUE)
				CREATE_PED_COVER_POINT(peds[mpf_final_rappel_room_1], <<152.1283, -763.8553, 245.1521>>, 158.2168, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180)
				SET_PED_COMBAT_PARAMS(peds[mpf_final_rappel_room_1], <<152.1283, -763.8553, 245.1521>>, 1.0, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, FALSE, FALSE, FALSE)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_final_rappel_room_1].id, <<152.1283, -763.8553, 245.1521>>, -1, FALSE, 0, TRUE, TRUE, null, FALSE)
			
				CREATE_MISSION_PED(peds[mpf_final_rappel_room_2], mod_fib_agent_3, <<155.0890, -764.9410, 245.1521>>, 161.2740, "EndRappel_2", REL_ENEMY, WEAPONTYPE_SMG, WEAPONCOMPONENT_INVALID, 5)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_final_rappel_room_2].id, 0.2)
				SET_PED_ENVEFF_SCALE(peds[mpf_final_rappel_room_2].id, 0.65)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_final_rappel_room_2].id, 74,69,60)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_final_rappel_room_2].id, TRUE)
				CREATE_PED_COVER_POINT(peds[mpf_final_rappel_room_2], <<155.0786, -764.9370, 245.1521>>, 158.1851, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_180)
				SET_PED_COMBAT_PARAMS(peds[mpf_final_rappel_room_2], <<155.0786, -764.9370, 245.1521>>, 1.0, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, FALSE, FALSE, FALSE)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_final_rappel_room_2].id, <<155.0786, -764.9370, 245.1521>>, -1, FALSE, 0, TRUE, FALSE, null, FALSE)
				
				mEvents[mef_rappel_room].iStage++
			ENDIF
		BREAK
		CASE 2
			// peds at end of corridor start attacking player
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<146.708191,-771.042236,245.152039>>, <<152.838486,-772.939270,248.050598>>, 7.000000)
				mEvents[mef_rappel_room].iTimeStamp = GET_GAME_TIMER() + 6000
				mEvents[mef_rappel_room].iStage++
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_PED_INJURED(peds[mpf_final_rappel_room_1].id)
				SWITCH peds[mpf_final_rappel_room_1].iGenericFlag
					CASE 0
						TASK_COMBAT_PED(peds[mpf_final_rappel_room_1].id, PLAYER_PED_ID())
						peds[mpf_final_rappel_room_1].iGenericFlag++
					BREAK
					CASE 1
						IF IS_PED_INJURED(peds[mpf_final_rappel_room_2].id)
							SET_PED_COMBAT_PARAMS(peds[mpf_final_rappel_room_1], <<0,0,0>>, 0.0, CM_WILLADVANCE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE, FALSE, TRUE, TRUE)
							peds[mpf_final_rappel_room_2].iGenericFlag++
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			
			IF NOT IS_PED_INJURED(peds[mpf_final_rappel_room_2].id)
				SWITCH peds[mpf_final_rappel_room_2].iGenericFlag
					CASE 0
						TASK_COMBAT_PED(peds[mpf_final_rappel_room_2].id, PLAYER_PED_ID())
						peds[mpf_final_rappel_room_2].iGenericFlag++
					BREAK
					CASE 1
						IF IS_PED_INJURED(peds[mpf_final_rappel_room_1].id)
						OR GET_GAME_TIMER() > mEvents[mef_rappel_room].iTimeStamp
							SET_PED_COMBAT_PARAMS(peds[mpf_final_rappel_room_2], <<0,0,0>>, 0.0, CM_WILLADVANCE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE, FALSE, TRUE, TRUE)
							peds[mpf_final_rappel_room_2].iGenericFlag++
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			
			IF IS_PED_INJURED(peds[mpf_final_rappel_room_1].id)
			AND IS_PED_INJURED(peds[mpf_final_rappel_room_2].id)
				Event_Finish(mef_rappel_room)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC me_bodies()
	SWITCH mEvents[mef_bodies].iStage
		CASE 1
			Load_Asset_Model(sAssetData, mod_fib_agent_1)
			Load_Asset_Model(sAssetData, mod_fib_agent_2)
			Load_Asset_AnimDict(sAssetData, "dead")
			
			IF HAS_MODEL_LOADED(mod_fib_agent_1)
			AND HAS_MODEL_LOADED(mod_fib_agent_2)
			AND HAS_ANIM_DICT_LOADED("dead")
			
				// create dead peds
				CREATE_MISSION_PED(peds[mpf_dead_agent_1], mod_fib_agent_1, << 122.036, -751.384, 250.154 >>, 46.250, "DEAD_PED_1", REL_ENEMY, WEAPONTYPE_INVALID, WEAPONCOMPONENT_INVALID, 1, 101)
				CREATE_MISSION_PED(peds[mpf_dead_agent_2], mod_fib_agent_2, << 148.575, -759.537, 250.137 >>, -92.160, "DEAD_PED_2", REL_ENEMY, WEAPONTYPE_INVALID, WEAPONCOMPONENT_INVALID, 1, 101)
				CREATE_MISSION_PED(peds[mpf_dead_agent_3], mod_fib_agent_1, << 143.100, -770.100, 246.200 >>, -92.160, "DEAD_PED_3", REL_ENEMY, WEAPONTYPE_INVALID, WEAPONCOMPONENT_INVALID, 1, 101)
				peds[mpf_dead_agent_1].bDoNotCleanUp = TRUE
				peds[mpf_dead_agent_2].bDoNotCleanUp = TRUE
				peds[mpf_dead_agent_3].bDoNotCleanUp = TRUE
				SET_PED_CAN_BE_TARGETTED(peds[mpf_dead_agent_1].id, FALSE)
				SET_PED_CAN_BE_TARGETTED(peds[mpf_dead_agent_2].id, FALSE)
				SET_PED_CAN_BE_TARGETTED(peds[mpf_dead_agent_3].id, FALSE)
				TASK_PLAY_ANIM_ADVANCED(peds[mpf_dead_agent_1].id, "dead", "dead_d", << 122.036, -751.384, 250.154 >>, << 0.000, 0.000, 46.250 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE)
				TASK_PLAY_ANIM_ADVANCED(peds[mpf_dead_agent_2].id, "dead", "dead_g", << 148.575, -759.537, 250.137 >>, << 0.000, 0.000, -92.160 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE)
				TASK_PLAY_ANIM_ADVANCED(peds[mpf_dead_agent_3].id, "dead", "dead_e", << 143.100, -770.100, 246.200 >>, << 0.000, 0.000, -92.160 >>, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_ENDS_IN_DEAD_POSE)
				
				Unload_Asset_Anim_Dict(sAssetData, "dead")
				
				mEvents[mef_bodies].iStage++
			ENDIF
		BREAK
		CASE 2
			IF mission_stage >= enum_to_int(msf_9_abseil_down)	
				
				IF DOES_ENTITY_EXIST(peds[mpf_dead_agent_1].id)
					DELETE_PED(peds[mpf_dead_agent_1].id)
				ENDIF
				IF DOES_ENTITY_EXIST(peds[mpf_dead_agent_2].id)
					DELETE_PED(peds[mpf_dead_agent_2].id)
				ENDIF
				IF DOES_ENTITY_EXIST(peds[mpf_dead_agent_3].id)
					DELETE_PED(peds[mpf_dead_agent_3].id)
				ENDIF
				
				Event_Finish(mef_bodies)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC me_leadout_MCS_2()

	SWITCH mEvents[mef_leadout_MCS_2].iStage
		CASE 1			
			
			Load_Asset_AnimDict(sAssetData, "missheistfbi3bleadinoutah_3b_mcs_2")
			Load_Asset_AnimDict(sAssetData, animDict_WaitIdles)
			Load_Asset_Model(sAssetData, P_LD_HEIST_BAG_S)
			
			IF HAS_ANIM_DICT_LOADED("missheistfbi3bleadinoutah_3b_mcs_2")
			AND HAS_MODEL_LOADED(P_LD_HEIST_BAG_S)
				
				REMOVE_WEAPON_FROM_PED(FRANK_PED_ID(), GADGETTYPE_PARACHUTE)
				CLEAR_PED_PARACHUTE_PACK_VARIATION(FRANK_PED_ID())
				REMOVE_WEAPON_FROM_PED(peds[mpf_goon].id, GADGETTYPE_PARACHUTE)
				CLEAR_PED_PARACHUTE_PACK_VARIATION(peds[mpf_goon].id)
				
				SET_CURRENT_PED_WEAPON(peds[mpf_frank].id, WEAPONTYPE_UNARMED, TRUE)
				SET_CURRENT_PED_WEAPON(peds[mpf_goon].id, WEAPONTYPE_UNARMED, TRUE)
				
				SET_PED_CONFIG_FLAG(peds[mpf_frank].id, PCF_RunFromFiresAndExplosions, FALSE)
				SET_PED_CONFIG_FLAG(peds[mpf_goon].id, PCF_RunFromFiresAndExplosions, FALSE)
				
				objs[mof_equipment_bag].id = CREATE_OBJECT(P_LD_HEIST_BAG_S, <<137.1110, -748.9485, 257.1519>>)

				objs[mof_cutscene_weapon_frank].id = CREATE_WEAPON_OBJECT(weap_Frank, ammo_Frank, <<137.1110, -748.9485, 257.1519>>, FALSE)
				GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(objs[mof_cutscene_weapon_frank].id, WEAPONCOMPONENT_AT_AR_FLSH)
				GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(objs[mof_cutscene_weapon_frank].id, WEAPONCOMPONENT_AT_AR_SUPP)

				objs[mof_cutscene_weapon_goon].id = CREATE_WEAPON_OBJECT(weap_Goon, ammo_Goon, <<137.1110, -748.9485, 257.1519>>, FALSE)
				IF weapComp_GoonFlash != WEAPONCOMPONENT_INVALID
					GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(objs[mof_cutscene_weapon_goon].id, weapComp_GoonFlash)
				ENDIF
				IF weapComp_GoonSupp != WEAPONCOMPONENT_INVALID
					GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(objs[mof_cutscene_weapon_goon].id, weapComp_GoonSupp)
				ENDIF
				IF weapComp_GoonClip != WEAPONCOMPONENT_INVALID
					GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(objs[mof_cutscene_weapon_goon].id, weapComp_GoonClip)
				ENDIF
				IF weapComp_GoonScope != WEAPONCOMPONENT_INVALID
					GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(objs[mof_cutscene_weapon_goon].id, weapComp_GoonScope)
				ENDIF
				IF weapComp_GoonGrip != WEAPONCOMPONENT_INVALID
					GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(objs[mof_cutscene_weapon_goon].id, weapComp_GoonGrip)
				ENDIF
				
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(FRANK_PED_ID())
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(peds[mpf_goon].id)
			
				syncedScenes[ssf_leadout_MCS_2] = CREATE_SYNCHRONIZED_SCENE( v_mcs_2_leadout_coord, v_mcs_2_leadout_rot )
				TASK_SYNCHRONIZED_SCENE(FRANK_PED_ID(), syncedScenes[ssf_leadout_MCS_2], 							"missheistfbi3bleadinoutah_3b_mcs_2", "_leadout_fra",	INSTANT_BLEND_IN, NORMAL_BLEND_OUT,		SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE)
				TASK_SYNCHRONIZED_SCENE(peds[mpf_goon].id, syncedScenes[ssf_leadout_MCS_2], 						"missheistfbi3bleadinoutah_3b_mcs_2", "_leadout_hench", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,		SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_ON_ABORT_STOP_SCENE | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(objs[mof_cutscene_weapon_frank].id, syncedScenes[ssf_leadout_MCS_2], 	"_leadout_gun_F", 	"missheistfbi3bleadinoutah_3b_mcs_2", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,		enum_to_int(SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS))
				PLAY_SYNCHRONIZED_ENTITY_ANIM(objs[mof_cutscene_weapon_goon].id, syncedScenes[ssf_leadout_MCS_2], 	"_leadout_gun_H", 	"missheistfbi3bleadinoutah_3b_mcs_2", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,		enum_to_int(SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS))
				PLAY_SYNCHRONIZED_ENTITY_ANIM(objs[mof_equipment_bag].id, syncedScenes[ssf_leadout_MCS_2], 			"_leadout_bag", 	"missheistfbi3bleadinoutah_3b_mcs_2", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,		enum_to_int(SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS))
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(syncedScenes[ssf_leadout_MCS_2], TRUE)
				
				vGoToExactlyBuddy 			= GET_ANIM_INITIAL_OFFSET_POSITION("missheistfbi3bleadinoutah_3b_mcs_2", "_leadout_fra", 	<< 134.7172, -752.7620, 257.1572 >>, << 0.000, 0.000, 159.855225 >>, 1.0)
				vGoToExactlyGunman			= GET_ANIM_INITIAL_OFFSET_POSITION("missheistfbi3bleadinoutah_3b_mcs_2", "_leadout_hench", 	<< 134.7172, -752.7620, 257.1572 >>, << 0.000, 0.000, 159.855225 >>, 1.0)
				vGoToExactlyBuddyRot		= GET_ANIM_INITIAL_OFFSET_ROTATION("missheistfbi3bleadinoutah_3b_mcs_2", "_leadout_fra", 	<< 134.7172, -752.7620, 257.1572 >>, << 0.000, 0.000, 159.855225 >>, 1.0)
				vGoToExactlyGunmanRot		= GET_ANIM_INITIAL_OFFSET_ROTATION("missheistfbi3bleadinoutah_3b_mcs_2", "_leadout_hench", 	<< 134.7172, -752.7620, 257.1572 >>, << 0.000, 0.000, 159.855225 >>, 1.0)
				
				IF IS_SCREEN_FADED_OUT()
					SET_SYNCHRONIZED_SCENE_RATE(syncedScenes[ssf_leadout_MCS_2], 0.0)
				ENDIF
				
				mEvents[mef_leadout_MCS_2].iStage++
			ENDIF
		BREAK
		CASE 2
			IF NOT IS_SCREEN_FADED_OUT()
				SET_SYNCHRONIZED_SCENE_RATE(syncedScenes[ssf_leadout_MCS_2], 1.0)
			ENDIF
		
			IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_leadout_MCS_2]) >= 0.6
			OR HAS_ANIM_EVENT_FIRED(peds[mpf_goon].id, GET_HASH_KEY("CREATE"))
				GIVE_WEAPON_OBJECT_TO_PED(objs[mof_cutscene_weapon_goon].id, peds[mpf_goon].id)
				SET_CURRENT_PED_WEAPON(peds[mpf_goon].id, weap_Goon, TRUE)
				SET_PED_FLASH_LIGHT(peds[mpf_goon], TRUE)
				mEvents[mef_leadout_MCS_2].iStage++
			ENDIF
		BREAK
		CASE 3
			IF GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_leadout_MCS_2]) >= 0.707
			OR HAS_ANIM_EVENT_FIRED(peds[mpf_frank].id, GET_HASH_KEY("CREATE"))
				GIVE_WEAPON_OBJECT_TO_PED(objs[mof_cutscene_weapon_frank].id, peds[mpf_frank].id)
				SET_CURRENT_PED_WEAPON(peds[mpf_frank].id, WEAPONTYPE_CARBINERIFLE, TRUE)
				SET_PED_FLASH_LIGHT(peds[mpf_frank], TRUE)
				Unload_Asset_Weapon_Asset(sAssetData, WEAPONTYPE_CARBINERIFLE)
				Unload_Asset_Weapon_Asset(sAssetData, weap_Goon)
				mEvents[mef_leadout_MCS_2].iStage++
			ENDIF
		BREAK
		CASE 4
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_leadout_MCS_2])
			OR GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_leadout_MCS_2]) = 1.0
			AND HAS_ANIM_DICT_LOADED(animDict_WaitIdles)
			
				SET_PED_CONFIG_FLAG(peds[mpf_frank].id, PCF_RunFromFiresAndExplosions, TRUE)
				SET_PED_CONFIG_FLAG(peds[mpf_goon].id, PCF_RunFromFiresAndExplosions, TRUE)
			
				
				IF NOT IS_PED_INJURED(peds[mpf_frank].id)
					IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(FRANK_PED_ID()), vGoToExactlyBuddy, 0.5)
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_frank].id, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
							TASK_FOLLOW_NAV_MESH_TO_COORD(peds[mpf_frank].id, vGoToExactlyBuddy, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, 0.1, ENAV_STOP_EXACTLY, vGoToExactlyBuddyRot.z)
						ENDIF
					ELSE
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_frank].id, SCRIPT_TASK_PERFORM_SEQUENCE)
							SAFE_OPEN_SEQUENCE()
								TASK_ACHIEVE_HEADING(null, vGoToExactlyBuddyRot.z)
								TASK_PLAY_ANIM(null, animDict_WaitIdles, "idle_f", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
								TASK_PLAY_ANIM(null, animDict_WaitIdles, "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
								TASK_PLAY_ANIM(null, animDict_WaitIdles, "idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
								TASK_PLAY_ANIM(null, animDict_WaitIdles, "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
								TASK_PLAY_ANIM(null, animDict_WaitIdles, "idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
								TASK_PLAY_ANIM(null, animDict_WaitIdles, "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
								SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
							SAFE_PERFORM_SEQUENCE(peds[mpf_frank].id)
							
							TASK_LOOK_AT_ENTITY(peds[mpf_frank].id, MIKE_PED_ID(), -1, SLF_DEFAULT, SLF_LOOKAT_MEDIUM)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(peds[mpf_goon].id)
					IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), vGoToExactlyGunman, 1.0)
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
							TASK_FOLLOW_NAV_MESH_TO_COORD(peds[mpf_goon].id, vGoToExactlyGunman, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, 0.1, ENAV_STOP_EXACTLY, vGoToExactlyGunmanRot.z)
						ENDIF
					ELSE
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_PERFORM_SEQUENCE)
							SAFE_OPEN_SEQUENCE()
								TASK_ACHIEVE_HEADING(null, vGoToExactlyGunmanRot.z)
								TASK_PLAY_ANIM(null, animDict_WaitIdles, "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
								TASK_PLAY_ANIM(null, animDict_WaitIdles, "idle_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
								TASK_PLAY_ANIM(null, animDict_WaitIdles, "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,  AF_USE_KINEMATIC_PHYSICS)
								TASK_PLAY_ANIM(null, animDict_WaitIdles, "idle_e", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
								TASK_PLAY_ANIM(null, animDict_WaitIdles, "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
								TASK_PLAY_ANIM(null, animDict_WaitIdles, "idle_f", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS)
								SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
							SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
							
							TASK_LOOK_AT_ENTITY(peds[mpf_goon].id, MIKE_PED_ID(), -1, SLF_DEFAULT, SLF_LOOKAT_MEDIUM)
						ENDIF
					ENDIF
				ENDIF
				
				syncedScenes[ssf_leadout_MCS_2] = CREATE_SYNCHRONIZED_SCENE(v_mcs_2_leadout_coord, v_mcs_2_leadout_rot)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(objs[mof_equipment_bag].id, syncedScenes[ssf_leadout_MCS_2], "_leadout_bag", "missheistfbi3bleadinoutah_3b_mcs_2", INSTANT_BLEND_IN)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME( syncedScenes[ssf_leadout_MCS_2], TRUE )
				SET_SYNCHRONIZED_SCENE_PHASE( syncedScenes[ssf_leadout_MCS_2], 0.9 )
				
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF mEvents[mef_leadout_MCS_2].iStage > 1
	AND mEvents[mef_leadout_MCS_2].iStage < 4
		
		IF NOT IS_ENTITY_PLAYING_ANIM(FRANK_PED_ID(), "missheistfbi3bleadinoutah_3b_mcs_2", "_leadout_fra", ANIM_SYNCED_SCENE)
		OR IS_PED_RAGDOLL(FRANK_PED_ID())
		OR NOT IS_ENTITY_PLAYING_ANIM(peds[mpf_goon].id, "missheistfbi3bleadinoutah_3b_mcs_2", "_leadout_hench", ANIM_SYNCED_SCENE)
		OR IS_PED_RAGDOLL(peds[mpf_goon].id)
		
			IF GET_INTERIOR_FROM_ENTITY(FRANK_PED_ID()) != interior_fib03
				IF IS_ENTITY_ON_FIRE(peds[mpf_frank].id)
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(peds[mpf_frank].id, PLAYER_PED_ID())
					Mission_Failed(mff_frank_dead)
				ENDIF
			ELIF GET_INTERIOR_FROM_ENTITY(peds[mpf_goon].id) != interior_fib03
				IF IS_ENTITY_ON_FIRE(peds[mpf_goon].id)
				OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(peds[mpf_goon].id, PLAYER_PED_ID())
					Mission_Failed(mff_goon_dead)
				ENDIF
			ELSE
				i_dialogue_substage = 2 // abort conversation
			ENDIF
			
		ENDIF
		
	ENDIF
	
	IF NOT bPlayingKeyDialogue
		SWITCH i_dialogue_substage
			CASE 0
				IF mEvents[mef_leadout_MCS_2].iStage >= 3
					IF IS_SAFE_TO_START_CONVERSATION()
						IF GET_CREW_MEMBER_SKILL(crewGoon) = CMSK_BAD
							str_dialogue_root = "AH_FBILAN"
						ELSE
							str_dialogue_root = "AH_GIVEGUN"
						ENDIF
						IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
							i_dialogue_substage++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF IS_SAFE_TO_START_CONVERSATION()
					str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewGoon, "AH_FBILAN")
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
						i_dialogue_substage++
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF

ENDPROC

 
PROC me_heli_crash_cutscene()
	IF i_crash_cutscene_cam_stage > 1
	AND i_crash_cutscene_cam_stage < 9
		IF IS_SCREEN_FADED_IN()
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
		IF i_crash_cutscene_cam_stage != 99
			i_crash_cutscene_cam_stage 					= 99
			i_crash_cutscene_dialogue 					= 99
			Event_Finish(mef_heli_to_heli_missile)
			STOP_STREAM()
			SET_GAME_PAUSES_FOR_STREAMING(TRUE)
			END_SRL()
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			IF DOES_CAM_EXIST(cams[mcf_helicrash_1]) DESTROY_CAM(cams[mcf_helicrash_1])	ENDIF
			IF DOES_CAM_EXIST(cams[mcf_helicrash_2]) DESTROY_CAM(cams[mcf_helicrash_2])	ENDIF
			IF DOES_CAM_EXIST(cams[mcf_helicrash_3]) DESTROY_CAM(cams[mcf_helicrash_3])	ENDIF
			
			Load_Asset_NewLoadScene_Sphere(sAssetData, GET_ENTITY_COORDS(PLAYER_PED_ID()), 30.0)
			mEvents[mef_heli_crash_cutscene].iStage 	= 4
		ENDIF
	ENDIF

	FLOAT fRecPhaseFriendly
	#IF IS_DEBUG_BUILD
		FLOAT fRecPhaseEnemy
	#ENDIF
	
	IF DOES_ENTITY_EXIST(vehs[mvf_heli].id)
	AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_heli].id)
		fRecPhaseFriendly = GET_TIME_POSITION_IN_RECORDING(vehs[mvf_heli].id)
		#IF IS_DEBUG_BUILD
			f_debug_rec_pos1 = fRecPhaseFriendly
		#ENDIF	
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF DOES_ENTITY_EXIST(vehs[mvf_heli_attack].id)
		AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_heli_attack].id)
			fRecPhaseEnemy = GET_TIME_POSITION_IN_RECORDING(vehs[mvf_heli_attack].id)
			f_debug_rec_pos2 = fRecPhaseEnemy
		ENDIF
	#ENDIF	
	
	
	SWITCH mEvents[mef_heli_crash_cutscene].iStage
		CASE 1
			Load_Asset_Model(sAssetData, mod_heli)
			Load_Asset_Model(sAssetData, mod_pilot)
			Load_Asset_Recording(sAssetData, rec_heli_rescue, str_car_recs)
			Load_Asset_Recording(sAssetData, rec_heli_hover, str_car_recs)

			i_crash_cutscene_cam_stage 	= 0
			i_crash_cutscene_dialogue 	= -1
			
			bStartCrashCutscene 		= FALSE
			bCrashReturnShakeAndPTFX	= FALSE
		
			IF HAS_MODEL_LOADED(mod_heli)
			AND HAS_MODEL_LOADED(mod_pilot)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_heli_rescue, str_car_recs)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_heli_hover, str_car_recs)

				IF DOES_ENTITY_EXIST(vehs[mvf_heli].id)
					IF DOES_ENTITY_EXIST(peds[mpf_pilot].id)
						DELETE_PED(peds[mpf_pilot].id)
					ENDIF
					
					DELETE_VEHICLE(vehs[mvf_heli].id)
				ENDIF
				
				vehs[mvf_heli].id = CREATE_VEHICLE(mod_heli, << 230.8776, -680.0508, 286.7156 >>, 320.1096)
				SET_VEHICLE_AS_RESTRICTED(vehs[mvf_heli].id, 0)
				FREEZE_ENTITY_POSITION(vehs[mvf_heli].id, TRUE)
				SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_heli].id, I_HELI_VARIATION)
				SET_ENTITY_INVINCIBLE(vehs[mvf_heli].id, TRUE)
				SET_ENTITY_LOAD_COLLISION_FLAG(vehs[mvf_heli].id, TRUE)
				
				CREATE_MISSION_PED_IN_VEHICLE(peds[mpf_pilot], mod_pilot, vehs[mvf_heli], VS_DRIVER, "CREA_PILOT", REL_CREW_IGNORE)
				SET_PED_COMPONENT_VARIATION(peds[mpf_pilot].id, PED_COMP_SPECIAL2, 0, 0)
				SET_PED_PROP_INDEX(peds[mpf_pilot].id, ANCHOR_HEAD, 0, 0)
				Unload_Asset_Model(sAssetData, mod_pilot)
				
				FREEZE_ENTITY_POSITION(vehs[mvf_heli].id, FALSE)
				SET_PED_CONFIG_FLAG(peds[mpf_pilot].id, PCF_GetOutBurningVehicle, FALSE)
				SET_PED_CONFIG_FLAG(peds[mpf_pilot].id, PCF_GetOutUndriveableVehicle, FALSE)
				SET_PED_CONFIG_FLAG(peds[mpf_pilot].id, PCF_RunFromFiresAndExplosions, FALSE)
				SET_PED_CONFIG_FLAG(peds[mpf_pilot].id, PCF_DisableExplosionReactions, TRUE)

				START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli].id, rec_heli_rescue, str_car_recs)
				
				//Jump_To_Time_In_Recording(vehs[mvf_heli].id, 23700)
				
				Add_Peds_For_Dialogue()

				SET_HELI_BLADES_FULL_SPEED(vehs[mvf_heli].id)
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				bFailOnDeadPilot = TRUE
				i_crash_cutscene_cam_timer 				= -1
				vehs[mvf_heli].peds[0].iGenericFlag 	= 0
				i_crash_cutscene_dialogue 				= 0
				
				Event_Next_Stage(mef_heli_crash_cutscene)
			ENDIF
		BREAK
		CASE 2
			Load_Asset_Model(sAssetData, mod_heli_attack)
			Load_Asset_Model(sAssetData, mod_pilot_enemy)
			Load_Asset_Model(sAssetData, GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG))
			Load_Asset_Model(sAssetData, PROP_LD_TEST_01)
			Load_Asset_Recording(sAssetData, rec_heli_rocket_hit, str_car_recs)
			Load_Asset_Recording(sAssetData, rec_heli_hit_building, str_car_recs)
			Load_Asset_Recording(sAssetData, rec_gunship_attack_run, str_car_recs)
			Load_Asset_Recording(sAssetData, rec_gunship_flyby, str_car_recs)
			Load_Asset_AnimDict(sAssetData, "missheistfbi3b_helicrash")
			Load_Asset_PTFX(sAssetData)
			Load_Asset_Audiobank(sAssetData, "FBI_Heist_Finale_Heli_Crash")
			PREPARE_MUSIC_EVENT("AH3B_HELI_SHOOTS_HELI")
			LOAD_STREAM("THE_AGENCY_HEIST_HELICOPTER_SHOT_DOWN_MASTER")
			PREFETCH_SRL("ah3bHeliCrash")
			Event_Next_Stage(mef_heli_crash_cutscene)
		BREAK
		CASE 3
			// hover the heli pre cutscene
			SWITCH vehs[mvf_heli].peds[0].iGenericFlag
				CASE 0
					IF fRecPhaseFriendly >= 23785.890
					AND Set_Vehicle_Playback_Speed(vehs[mvf_heli], 0.4, 2000)
						vehs[mvf_heli].peds[0].iGenericFlag++
					ENDIF
				BREAK
				CASE 1
					IF fRecPhaseFriendly >= 28438.890
						vehs[mvf_heli].peds[0].iGenericFlag++
					ENDIF
				BREAK
				CASE 2
					IF Set_Vehicle_Playback_Speed(vehs[mvf_heli], -0.4, 1000)
						vehs[mvf_heli].peds[0].iGenericFlag++
					ENDIF
				BREAK
				CASE 3 
					IF fRecPhaseFriendly <= 23785.890
						vehs[mvf_heli].peds[0].iGenericFlag++
					ENDIF
				BREAK
				CASE 4
					IF Set_Vehicle_Playback_Speed(vehs[mvf_heli], 0.4, 1000)
						vehs[mvf_heli].peds[0].iGenericFlag = 1
					ENDIF
				BREAK
			ENDSWITCH
			
			// timer is set, once everythings loaded kick off the cutscene
			IF bStartCrashCutscene
			
				BOOL bAllLoaded
				bAllLoaded = TRUE
				IF NOT (GET_GAME_TIMER() - i_crash_cutscene_cam_timer >= 3000)
					CPRINTLN(DEBUG_MIKE, "HeliCrashCutscene: AWAITING TIMER")
					bAllLoaded = FALSE
				ENDIF
				IF NOT HAS_MODEL_LOADED(mod_heli_attack)
					CPRINTLN(DEBUG_MIKE, "HeliCrashCutscene: AWAITING heli attack model")
					bAllLoaded = FALSE
				ENDIF
				IF NOT HAS_MODEL_LOADED(mod_pilot_enemy)
					CPRINTLN(DEBUG_MIKE, "HeliCrashCutscene: AWAITING pilot attack model")
					bAllLoaded = FALSE
				ENDIF
				IF NOT HAS_MODEL_LOADED(PROP_LD_TEST_01)
					CPRINTLN(DEBUG_MIKE, "HeliCrashCutscene: AWAITING dummy model")
					bAllLoaded = FALSE
				ENDIF
				IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_gunship_attack_run, str_car_recs)
					CPRINTLN(DEBUG_MIKE, "HeliCrashCutscene:  AWAITING rec_gunship_attack_run")
					bAllLoaded = FALSE
				ENDIF
				IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_heli_hover, str_car_recs)
					CPRINTLN(DEBUG_MIKE, "HeliCrashCutscene:  AWAITING rec_heli_hover")
					bAllLoaded = FALSE
				ENDIF
				IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_heli_rocket_hit, str_car_recs)
					CPRINTLN(DEBUG_MIKE, "HeliCrashCutscene:  AWAITING rec_heli_rocket_hit")
					bAllLoaded = FALSE
				ENDIF
				IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_heli_hit_building, str_car_recs)
					CPRINTLN(DEBUG_MIKE, "HeliCrashCutscene:  AWAITING rec_heli_hit_building")
					bAllLoaded = FALSE
				ENDIF
				IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_gunship_flyby, str_car_recs)
					CPRINTLN(DEBUG_MIKE, "HeliCrashCutscene:  AWAITING rec_gunship_flyby")
					bAllLoaded = FALSE
				ENDIF
				IF NOT LOAD_STREAM("THE_AGENCY_HEIST_HELICOPTER_SHOT_DOWN_MASTER")
					CPRINTLN(DEBUG_MIKE, "HeliCrashCutscene: THE_AGENCY_HEIST_HELICOPTER_SHOT_DOWN_MASTER")
					bAllLoaded = FALSE
				ENDIF
				IF NOT HAS_ANIM_DICT_LOADED("missheistfbi3b_helicrash")
					CPRINTLN(DEBUG_MIKE, "HeliCrashCutscene: AWAITING ANIMDICT missheistfbi3b_helicrash")
					bAllLoaded = FALSE
				ENDIF
				IF NOT IS_SRL_LOADED()
					CPRINTLN(DEBUG_MIKE, "HeliCrashCutscene: AWAITING SRL")
					bAllLoaded = FALSE
				ENDIF
			
				IF bAllLoaded
				AND i_fps_state != DC_PROGRESS_CAM_ENTER AND i_fps_state != DC_CAM_COVER_ENTER
				AND (i_TimeOfCameraStateChange = -1 OR GET_GAME_TIMER() - i_TimeOfCameraStateChange > 2000)
				
					bFailOnDeadPilot = FALSE
					fDownloadSpeedMultiplier = 0.0
					
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), TRUE)
					SET_ENTITY_INVINCIBLE(FRANK_PED_ID(), TRUE)
					SET_ENTITY_INVINCIBLE(peds[mpf_goon].id, TRUE)
					
					CLEAR_HELP()

					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS")
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER")
					
					i_crash_cutscene_cam_stage = 1
					SET_SRL_POST_CUTSCENE_CAMERA(<<151.0806, -755.2610, 259.3411>>, <<145.9581, -753.5327, 258.6601>>-<<151.0806, -755.2610, 259.3411>>)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					Event_Next_Stage(mef_heli_crash_cutscene)
				ENDIF
			ENDIF
		BREAK
		CASE 4
			SET_LIGHTS_CUTOFF_DISTANCE_TWEAK(1500.0)
			
			IF i_crash_cutscene_cam_stage = 99
			OR IS_SCREEN_FADED_OUT()
				
				IF NOT IS_SCREEN_FADED_OUT()
				OR (IS_NEW_LOAD_SCENE_ACTIVE() AND IS_NEW_LOAD_SCENE_LOADED())
			
					IF IS_AUDIO_SCENE_ACTIVE("AH_3B_HELICOPTER_MISSILE_CUTSCENE")
						STOP_AUDIO_SCENE("AH_3B_HELICOPTER_MISSILE_CUTSCENE")
					ENDIF
					IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_SHOOTOUT_CONTINUED")
						START_AUDIO_SCENE("AH_3B_SHOOTOUT_CONTINUED")
					ENDIF
					TRIGGER_MUSIC_EVENT("AH3B_HELI_CRASHED")
					
					IF NOT Is_Event_Triggered(mef_post_heli_crash_enemies)
						Event_Trigger(mef_post_heli_crash_enemies)
					ENDIF
					
					SET_PARTICLE_FX_CAM_INSIDE_NONPLAYER_VEHICLE(vehs[mvf_heli].id, FALSE)
					DELETE_PED(peds[mpf_pilot].id)
					SAFE_DELETE_VEHICLE(vehs[mvf_heli])
					SAFE_DELETE_VEHICLE(vehs[mvf_heli_attack])
					
					IF DOES_ENTITY_EXIST(sRocket.obj)
						DELETE_OBJECT(sRocket.obj)
					ENDIF
					
					IF DOES_PARTICLE_FX_LOOPED_EXIST(sRocket.ptfx)
						STOP_PARTICLE_FX_LOOPED(sRocket.ptfx)
					ENDIF
					Event_Finish(mef_heli_to_heli_missile)
					
					IF NOT bCrashReturnShakeAndPTFX
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_agency_atrium_silt", <<129.60, -750.79, 263.0>>, <<0,0,0>>)
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_agency_atrium_silt", <<130.13, -742.39, 261.7>>, <<0,0,0>>)
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_agency_atrium_silt", <<134.40, -748.22, 265.28>>, <<0,0,0>>)
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_agency_atrium_silt", <<141.66, -756.8, 261.96>>, <<0,0,0>>)
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_agency_atrium_glass", <<130.60,-743.95,262.49>>, <<0,0,0>>)
						SHAKE_GAMEPLAY_CAM("SMALL_EXPLOSION_SHAKE", 0.2)
						bCrashReturnShakeAndPTFX = TRUE
					ENDIF
					
					IF NOT IS_PED_IN_COVER(MIKE_PED_ID(), FALSE)
						TASK_PUT_PED_DIRECTLY_INTO_COVER(MIKE_PED_ID(), v_ShootoutCoverMike, -1, FALSE, 0, TRUE, FALSE, peds[mpf_mike].cov, TRUE)
					ELSE
						IF IS_PED_IN_COVER_FACING_LEFT(MIKE_PED_ID())
							// make the ped face right
						ENDIF
					ENDIF
					
					IF PLAYER_PED_ID() = MIKE_PED_ID()
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(90)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					ELSE
						SET_GAMEPLAY_CAM_LOOK_AT_WORLD_TARGET(<< 121.1928, -744.1429, 258.8839 >>)
					ENDIF	
				
					PLAY_SOUND_FRONTEND(sounds[sfx_heli_crash_exp], "Heli_Crash", "FBI_HEIST_FINALE_CHOPPER")
									
					fDownloadSpeedMultiplier 	= 1.0
					fDownloadProgress			= 0.67
					
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), FALSE)
					SET_ENTITY_INVINCIBLE(FRANK_PED_ID(), FALSE)
					SET_ENTITY_INVINCIBLE(peds[mpf_goon].id, FALSE)
					
					Unload_Asset_Recording(sAssetData, rec_heli_rescue, str_car_recs)
					Unload_Asset_Recording(sAssetData, rec_heli_hover, str_car_recs)
					Unload_Asset_Recording(sAssetData, rec_heli_rocket_hit, str_car_recs)
					Unload_Asset_Recording(sAssetData, rec_heli_hit_building, str_car_recs)
					Unload_Asset_Recording(sAssetData, rec_gunship_attack_run, str_car_recs)
					Unload_Asset_Recording(sAssetData, rec_gunship_flyby, str_car_recs)
					Unload_Asset_Model(sAssetData, mod_heli)
					Unload_Asset_Model(sAssetData, mod_heli_attack)
					
					DISPLAY_RADAR(TRUE)
					DISPLAY_HUD(TRUE)
					
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					IF DOES_CAM_EXIST(cams[mcf_helicrash_1]) DESTROY_CAM(cams[mcf_helicrash_1])	ENDIF
					IF DOES_CAM_EXIST(cams[mcf_helicrash_2]) DESTROY_CAM(cams[mcf_helicrash_2])	ENDIF
					IF DOES_CAM_EXIST(cams[mcf_helicrash_3]) DESTROY_CAM(cams[mcf_helicrash_3])	ENDIF
					
					Event_Resume(mef_manage_switch)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						Unload_Asset_NewLoadScene(sAssetData)
					ENDIF
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
					
					REPLAY_STOP_EVENT()
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					
					SET_GAME_PAUSES_FOR_STREAMING(TRUE)
					END_SRL()
					
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					Event_Next_Stage(mef_heli_crash_cutscene, 3000)
				ENDIF
				
			ENDIF
		BREAK
		CASE 5
			iDesiredSprinklerState = 1
			Event_Finish(mef_heli_crash_cutscene)
		BREAK
	ENDSWITCH
	
	// Control spot light
	IF DOES_ENTITY_EXIST(vehs[mvf_heli].id) AND IS_VEHICLE_DRIVEABLE(vehs[mvf_heli].id)
	AND DOES_ENTITY_EXIST(peds[mpf_pilot].id) AND NOT IS_PED_INJURED(peds[mpf_pilot].id)
	
		IF NOT IS_VEHICLE_SEARCHLIGHT_ON(vehs[mvf_heli].id)
			SET_VEHICLE_SEARCHLIGHT(vehs[mvf_heli].id, TRUE, TRUE)
		ENDIF	
	
		VECTOR vSpotlightTarget
		
		IF i_crash_cutscene_cam_stage = 0
			vSpotlightTarget = <<143.2218, -752.3270, 257.8933>>
		ELSE
			SWITCH i_crash_cutscene_cam_stage
				CASE 1 FALLTHRU
				CASE 2
					VECTOR v1, v2, v3
					FLOAT fAlpha
					IF GET_GAME_TIMER() - i_crash_cutscene_cam_timer <= 2000.0
						v1 = <<128.6734, -758.3740, 262.0997>>
						v2 = GET_CAM_COORD(cams[mcf_helicrash_1]) + <<0,0,3.0>>
						v3 = <<143.2218, -752.3270, 257.8933>>
						fAlpha = CLAMP( (GET_GAME_TIMER() - i_crash_cutscene_cam_timer)/2000.0, 0.0, 1.0 )
						vSpotlightTarget = QUADRATIC_BEZIER_PLOT(v1, v2, v3, fAlpha)
					ELSE
						v1 = <<143.2218, -752.3270, 257.8933>>
						v2 = <<134.9645, -753.0084, 257.6666>>
						fAlpha = CLAMP( (GET_GAME_TIMER() - i_crash_cutscene_cam_timer - 2000.0)/2000.0, 0.0, 1.0 )
						vSpotlightTarget = COSINE_INTERP_VECTOR(v1, v2, fAlpha)
					ENDIF
				BREAK
				CASE 3 FALLTHRU
				CASE 4 FALLTHRU
				CASE 5
					vSpotlightTarget = <<143.2218, -752.3270, 257.8933>>
				BREAK
				CASE 6	FALLTHRU
				CASE 7	FALLTHRU
				CASE 8	FALLTHRU
				CASE 9 	FALLTHRU
				CASE 10
					vSpotlightTarget = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehs[mvf_heli].id, <<-2.5,10.0,-5.0>>)
				BREAK
			ENDSWITCH
		ENDIF

		IF NOT IS_VECTOR_ZERO(vSpotlightTarget)
			IF IS_PED_IN_VEHICLE(peds[mpf_pilot].id, vehs[mvf_heli].id)
				IF IS_VEHICLE_SEARCHLIGHT_ON(vehs[mvf_heli].id)
					IF NOT IS_MOUNTED_WEAPON_TASK_UNDERNEATH_DRIVING_TASK(peds[mpf_pilot].id)
						CPRINTLN(DEBUG_MIKE, "SPOTLIGHT: Start mounted weapon control.")
						CONTROL_MOUNTED_WEAPON(peds[mpf_pilot].id)
					ENDIF
					IF IS_MOUNTED_WEAPON_TASK_UNDERNEATH_DRIVING_TASK(peds[mpf_pilot].id)
						CPRINTLN(DEBUG_MIKE, "SPOTLIGHT: Pointing heli spotlight at ", vSpotlightTarget)
						SET_MOUNTED_WEAPON_TARGET(peds[mpf_pilot].id, null, null, vSpotlightTarget)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	// CAMERAS
	
	SWITCH i_crash_cutscene_cam_stage
	
	// SHOT 1: Rising from the rooftop with the enemy heli coming into view in the background
	// duration: 3 seconds
		CASE 1
			IF PREPARE_MUSIC_EVENT("AH3B_HELI_SHOOTS_HELI")
				TRIGGER_MUSIC_EVENT("AH3B_HELI_SHOOTS_HELI")
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("AH_3B_ENEMIES_ARRIVE")
				STOP_AUDIO_SCENE("AH_3B_ENEMIES_ARRIVE")
			ENDIF
			IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_HELICOPTER_MISSILE_CUTSCENE")
				START_AUDIO_SCENE("AH_3B_HELICOPTER_MISSILE_CUTSCENE")
			ENDIF
			
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_heli].id)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli].id)
			ENDIF
			START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli].id, rec_heli_hover, str_car_recs)
			Set_Vehicle_Playback_Speed(vehs[mvf_heli], 1.0)
			FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_heli].id)

			// Create enemy heli, and play rec
			vehs[mvf_heli_attack].id = CREATE_VEHICLE(mod_heli_attack, << 432.7757, -813.2988, 297.8762 >>, 77.7276)
			CREATE_MISSION_PED_In_Vehicle(vehs[mvf_heli_attack].peds[0], mod_pilot_enemy, vehs[mvf_heli_attack], VS_DRIVER, "CREW_PILOT", REL_ENEMY)
			SET_PED_PROP_INDEX(vehs[mvf_heli_attack].peds[0].id, ANCHOR_HEAD, 0, 0)
			SET_PED_COMPONENT_VARIATION(vehs[mvf_heli_attack].peds[0].id, PED_COMP_SPECIAL2, 1, 0)
			
			START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli_attack].id, rec_gunship_attack_run, str_car_recs)
			FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_heli_attack].id)
			SET_HELI_BLADES_FULL_SPEED(vehs[mvf_heli_attack].id)
			SET_ENTITY_INVINCIBLE(vehs[mvf_heli].id, TRUE)
			
			Unload_Asset_Model(sAssetData, mod_heli_attack)
			Unload_Asset_Model(sAssetData, mod_pilot_enemy)
			
			ADD_PED_FOR_DIALOGUE(sConvo, 6, vehs[mvf_heli_attack].peds[0].id, "FIBPILOT")
			
			SET_PARTICLE_FX_CAM_INSIDE_NONPLAYER_VEHICLE(vehs[mvf_heli].id, TRUE)
		
			CLEAR_ROOM_FOR_GAME_VIEWPORT()
			IF DOES_CAM_EXIST(cams[mcf_helicrash_1])
				DESTROY_CAM(cams[mcf_helicrash_1])
			ENDIF
			cams[mcf_helicrash_1] 				= CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
			syncedScenes[ssf_helicrashCam] 		= CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(vehs[mvf_heli].id), <<0,0,GET_ENTITY_HEADING(vehs[mvf_heli].id)>>)
			SET_CAM_NEAR_CLIP(cams[mcf_helicrash_1], 0.001)
			PLAY_SYNCHRONIZED_CAM_ANIM(cams[mcf_helicrash_1], syncedScenes[ssf_helicrashCam], "establish_camera_cam", "missheistfbi3b_helicrash")
			SET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_helicrashCam], 0.5348)
			
			CLEAR_ROOM_FOR_GAME_VIEWPORT()

			CLEAR_HELP(TRUE)
			CLEAR_PRINTS()
			DISPLAY_HUD(FALSE)
			DISPLAY_RADAR(FALSE)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			CLEAR_ROOM_FOR_GAME_VIEWPORT()
			
			Event_Pause(mef_manage_switch)
			PLAY_STREAM_FRONTEND()
		
			OVERRIDE_LODSCALE_THIS_FRAME(1.0)
			SET_SRL_READAHEAD_TIMES(5, 5, 5, 5)
			BEGIN_SRL()
			SET_GAME_PAUSES_FOR_STREAMING(FALSE)
			i_crash_cutscene_cam_timer = GET_GAME_TIMER()
			i_crash_cutscene_cam_stage++
		BREAK
		
	// SHOT 2: Following the enemy heli from behind
	// duration: 1.5 seconds 
		CASE 2
			//IF NOT IS_CAM_INTERPOLATING(cams[mcf_helicrash_1])
			IF GET_GAME_TIMER() - i_crash_cutscene_cam_timer >= 4000
				Jump_To_Time_In_Recording(vehs[mvf_heli_attack].id, 3500)
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_heli_attack].id)
			
				IF DOES_CAM_EXIST(cams[mcf_helicrash_1])
					DESTROY_CAM(cams[mcf_helicrash_1])
				ENDIF
				cams[mcf_helicrash_1] 				= CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
				SET_CAM_MOTION_BLUR_STRENGTH(cams[mcf_helicrash_1], 0.15)
				
				syncedScenes[ssf_helicrashCam] 		= CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(vehs[mvf_heli_attack].id), GET_ENTITY_ROTATION(vehs[mvf_heli_attack].id))
				PLAY_SYNCHRONIZED_CAM_ANIM(cams[mcf_helicrash_1], syncedScenes[ssf_helicrashCam], "ah3b_attackheli_cam2", "missheistfbi3b_helicrash")
				
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)

				i_crash_cutscene_cam_stage++
			ELSE
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
			ENDIF
		BREAK
		
	// FIRE MISSILE
		CASE 3
			IF GET_GAME_TIMER() - i_crash_cutscene_cam_timer >= 5000
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
				Event_Trigger(mef_heli_to_heli_missile)
				i_crash_cutscene_cam_stage++
			ELSE
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
			ENDIF
		BREAK
		
	// SHOT 3: Cockpit view of pilot, shouts incoming
	// duration: 1 second
		CASE 4
			IF GET_GAME_TIMER() - i_crash_cutscene_cam_timer >= 7500				
			
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_heli].id)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli].id)
				ENDIF
				START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli].id, rec_heli_rocket_hit, str_car_recs)
				Set_Vehicle_Playback_Speed(vehs[mvf_heli], 1.0)
				//Jump_To_Time_In_Recording(vehs[mvf_heli].id, 450.0)
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_heli].id)
				
				SET_ENTITY_COORDS(sRocket.obj, <<83.3058, -748.7635, 271.0>>) // 274.0837>>)
				sRocket.f_RocketSpeed = 60.0 // 55.0
				
				IF DOES_CAM_EXIST(cams[mcf_helicrash_1])
					DESTROY_CAM(cams[mcf_helicrash_1])
				ENDIF
				cams[mcf_helicrash_1] 				= CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
				syncedScenes[ssf_helicrashCam] 		= CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(syncedScenes[ssf_helicrashCam], vehs[mvf_heli].id, -1)// GET_ENTITY_BONE_INDEX_BY_NAME(vehs[mvf_heli].id, "seat_dside_f"))
				PLAY_SYNCHRONIZED_CAM_ANIM(cams[mcf_helicrash_1], syncedScenes[ssf_helicrashCam], "reaction_camera_cam", "missheistfbi3b_helicrash")
				SET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_helicrashCam], 0.0242)
				SET_CAM_MOTION_BLUR_STRENGTH(cams[mcf_helicrash_1], 0.15)
				
				syncedScenes[ssf_helicrash] = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(syncedScenes[ssf_helicrash], vehs[mvf_heli].id, GET_ENTITY_BONE_INDEX_BY_NAME(vehs[mvf_heli].id, "seat_dside_f"))
				TASK_SYNCHRONIZED_SCENE(peds[mpf_pilot].id, syncedScenes[ssf_helicrash], "missheistfbi3b_helicrash", "heli_crash", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
				
				SET_VEHICLE_ENGINE_ON(vehs[mvf_heli].id, TRUE, TRUE)
				SET_HELI_BLADES_FULL_SPEED(vehs[mvf_heli].id)

				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
				i_crash_cutscene_cam_stage++
			ELSE
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
			ENDIF
		BREAK
		
	// SHOT 4: Black hawk down heli style heli spin shot
	// duration: 4 seconds
		CASE 5
			IF GET_GAME_TIMER() - i_crash_cutscene_cam_timer >= 9166
			
				Jump_To_Time_In_Recording(vehs[mvf_heli].id, 1950.0)
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_heli].id)
				
				CLEAR_PED_TASKS_IMMEDIATELY(peds[mpf_pilot].id)
				SET_PED_INTO_VEHICLE(peds[mpf_pilot].id, vehs[mvf_heli].id)
				SET_VEHICLE_ENGINE_ON(vehs[mvf_heli].id, TRUE, TRUE)
				SET_HELI_BLADES_FULL_SPEED(vehs[mvf_heli].id)
			
				IF DOES_CAM_EXIST(cams[mcf_helicrash_1])
					DESTROY_CAM(cams[mcf_helicrash_1])
				ENDIF
				cams[mcf_helicrash_1] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 123.6368, -750.2817, 359.1005 >>, << -89.4995, -0.0000, -20.2171 >>, 15.3997, TRUE)
				SET_CAM_DOF_PLANES(cams[mcf_helicrash_1], 0.0, 0.0, 100.5, 670.000)
				
				IF DOES_CAM_EXIST(cams[mcf_helicrash_2])
					DESTROY_CAM(cams[mcf_helicrash_2])
				ENDIF
				cams[mcf_helicrash_2] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, << 159.3979, -737.8659, 359.3100 >>, << -89.4995, -0.0000, -20.2171 >>, 15.3997)
				SET_CAM_DOF_PLANES(cams[mcf_helicrash_2], 0.0, 0.0, 100.5, 670.000)
				SET_CAM_ACTIVE_WITH_INTERP(cams[mcf_helicrash_2], cams[mcf_helicrash_1], 8000, GRAPH_TYPE_LINEAR)

				SET_USE_HI_DOF()
				OVERRIDE_LODSCALE_THIS_FRAME(3.064)
				
				i_crash_cutscene_cam_stage++
			ELSE
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
			ENDIF
		BREAK
		
	// SHOT 7: 2nd cockpit shot
	// duration: 3 seconds
		CASE 6
			IF GET_GAME_TIMER() - i_crash_cutscene_cam_timer >= 13166
				IF DOES_CAM_EXIST(cams[mcf_helicrash_1])
					DESTROY_CAM(cams[mcf_helicrash_1])
				ENDIF
				
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
				
				cams[mcf_helicrash_1] = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
				syncedScenes[ssf_helicrashCam] = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(syncedScenes[ssf_helicrashCam], vehs[mvf_heli].id, -1)//GET_ENTITY_BONE_INDEX_BY_NAME(vehs[mvf_heli].id, "seat_dside_f"))
				PLAY_SYNCHRONIZED_CAM_ANIM(cams[mcf_helicrash_1], syncedScenes[ssf_helicrashCam], "spinning_camera_cam", "missheistfbi3b_helicrash")
				SET_CAM_MOTION_BLUR_STRENGTH(cams[mcf_helicrash_1], 0.1)
				//SHAKE_CAM(cams[mcf_helicrash_1], "VIBRATE_SHAKE", 1.0)
				
				syncedScenes[ssf_helicrash] = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(syncedScenes[ssf_helicrash], vehs[mvf_heli].id, GET_ENTITY_BONE_INDEX_BY_NAME(vehs[mvf_heli].id, "seat_dside_f"))
				TASK_SYNCHRONIZED_SCENE(peds[mpf_pilot].id, syncedScenes[ssf_helicrash], "missheistfbi3b_helicrash", "heli_crash", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
				SET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_helicrash], 0.2756)
				
				SET_VEHICLE_ENGINE_ON(vehs[mvf_heli].id, TRUE, TRUE)
				SET_HELI_BLADES_FULL_SPEED(vehs[mvf_heli].id)
							
				i_crash_cutscene_cam_stage++
			ELSE
				SET_USE_HI_DOF()
				OVERRIDE_LODSCALE_THIS_FRAME(3.064)
			ENDIF
		BREAK
		
	// SHOT 8: Blackhawk down style panning while crashing
	// duration:  seconds
		CASE 7
			IF GET_GAME_TIMER() - i_crash_cutscene_cam_timer >= 14666
			
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_heli].id)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli].id)
				ENDIF
				START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli].id, rec_heli_hit_building, str_car_recs)
				Set_Vehicle_Playback_Speed(vehs[mvf_heli], 1.0)
				Jump_To_Time_In_Recording(vehs[mvf_heli].id, 3000.0) //2000.0)
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_heli].id)
			
				IF DOES_CAM_EXIST(cams[mcf_helicrash_1])
					DESTROY_CAM(cams[mcf_helicrash_1])
				ENDIF
				cams[mcf_helicrash_1] = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				ATTACH_CAM_TO_ENTITY(cams[mcf_helicrash_1], vehs[mvf_heli].id, <<219.1373, -695.2065, 267.7104>> - <<214.4779, -662.5891, 253.4843>>, FALSE) // LERP_VECTOR(<<219.1373, -695.2065, 267.7104>> - <<214.4779, -662.5891, 253.4843>>, <<229.5684, -655.9583, 252.6958>> - <<202.1229, -677.4445, 249.1550>>, 1000.0/6000.0))
				SET_CAM_FOV(cams[mcf_helicrash_1], 19.9962)
				SET_CAM_MOTION_BLUR_STRENGTH(cams[mcf_helicrash_1], 0.0)
				//SHAKE_CAM(cams[mcf_helicrash_1], "VIBRATE_SHAKE", 1.0)
				// bug 1877359
				SHAKE_CAM(cams[mcf_helicrash_1], "HAND_SHAKE", 1.0)
				

				IF DOES_CAM_EXIST(cams[mcf_helicrash_2])
					DESTROY_CAM(cams[mcf_helicrash_2])
				ENDIF
				cams[mcf_helicrash_2] = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				ATTACH_CAM_TO_ENTITY(cams[mcf_helicrash_2], vehs[mvf_heli].id, <<229.5684, -655.9583, 252.6958>> - <<202.1229, -677.4445, 249.1550>>, FALSE)
				SET_CAM_FOV(cams[mcf_helicrash_2], 19.9962)
				SET_CAM_MOTION_BLUR_STRENGTH(cams[mcf_helicrash_2], 0.0)
				//SHAKE_CAM(cams[mcf_helicrash_2], "VIBRATE_SHAKE", 1.0)
				// bug 1877359
				SHAKE_CAM(cams[mcf_helicrash_2], "HAND_SHAKE", 1.0)
				
				//SET_CAM_ACTIVE_WITH_INTERP(cams[mcf_helicrash_2], cams[mcf_helicrash_1], 5000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				IF DOES_CAM_EXIST(cams[mcf_helicrash_3])
					DESTROY_CAM(cams[mcf_helicrash_3])
				ENDIF
				cams[mcf_helicrash_3] = CREATE_CAMERA(CAMTYPE_SPLINE_SMOOTHED)
				ADD_CAM_SPLINE_NODE_USING_CAMERA(cams[mcf_helicrash_3], cams[mcf_helicrash_1], 0)
				ADD_CAM_SPLINE_NODE_USING_CAMERA(cams[mcf_helicrash_3], cams[mcf_helicrash_2], 1000)
				SET_CAM_SPLINE_DURATION(cams[mcf_helicrash_3], 5000)//6000)
				SET_CAM_SPLINE_SMOOTHING_STYLE(cams[mcf_helicrash_3], CAM_SPLINE_NO_SMOOTH)
				//SHAKE_CAM(cams[mcf_helicrash_3], "VIBRATE_SHAKE", 1.0)
				// bug 1877359
				SHAKE_CAM(cams[mcf_helicrash_3], "HAND_SHAKE", 1.0)
				POINT_CAM_AT_ENTITY(cams[mcf_helicrash_3], vehs[mvf_heli].id, <<0,0,0>>)
				SET_CAM_ACTIVE(cams[mcf_helicrash_3], TRUE)
				SET_CAM_MOTION_BLUR_STRENGTH(cams[mcf_helicrash_3], 0.0)
				
				REMOVE_PARTICLE_FX_IN_RANGE(<<186.7041, -643.1097, 247.5317>>, 500.0)
//				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxs[ptfx_tail_rotor])
//					STOP_PARTICLE_FX_LOOPED(ptfxs[ptfx_tail_rotor])
//				ENDIF

				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
				
				VECTOR vRot
				vRot = GET_ENTITY_ROTATION(vehs[mvf_heli].id)
				vRot.z += LERP_FLOAT(0, -80.2658, (GET_GAME_TIMER() - i_crash_cutscene_cam_timer)/6000.0)
				SET_ENTITY_ROTATION(vehs[mvf_heli].id, vRot)
							
				i_crash_cutscene_cam_stage++
			ELSE
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
			ENDIF
		BREAK
		CASE 8
			IF GET_GAME_TIMER() - i_crash_cutscene_cam_timer >= 17500
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_heli].id)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli].id)
				ENDIF
				SET_ENTITY_COLLISION(vehs[mvf_heli].id, FALSE)
				
				IF DOES_CAM_EXIST(cams[mcf_helicrash_1])
					DESTROY_CAM(cams[mcf_helicrash_1])
				ENDIF
				cams[mcf_helicrash_1] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<156.2, -732.9,280.6>>, <<-81.1,0.0,-3.1>>, 34.0, TRUE)
				SET_CAM_MOTION_BLUR_STRENGTH(cams[mcf_helicrash_1], 0.0)
				SET_CAM_DOF_PLANES(cams[mcf_helicrash_1], 0.0, 0.0, 100.5, 670.000)
				SHAKE_CAM(cams[mcf_helicrash_1], "HAND_SHAKE", 1.0)
				SET_USE_HI_DOF()
				
				DESTROY_CAM(cams[mcf_helicrash_2])
				DESTROY_CAM(cams[mcf_helicrash_3])

				syncedScenes[ssf_helicrash] 	= CREATE_SYNCHRONIZED_SCENE(<< 156.591, -731.486, 249.211 >>, << -0.000, 0.000, 6.840 >>)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(vehs[mvf_heli].id, syncedScenes[ssf_helicrash], "heli_hits_building", "missheistfbi3b_helicrash", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
				SET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_helicrash], 0.15)
				SET_SYNCHRONIZED_SCENE_RATE(syncedScenes[ssf_helicrash], 0.8)
				FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(vehs[mvf_heli].id)

				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()				
				
				Event_Trigger(mef_post_heli_crash_enemies)
				
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
				i_crash_cutscene_cam_stage++
			ELSE
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
				SET_CAM_MOTION_BLUR_STRENGTH(cams[mcf_helicrash_1], 0.15)
				SET_CAM_MOTION_BLUR_STRENGTH(cams[mcf_helicrash_2], 0.15)
				SET_CAM_MOTION_BLUR_STRENGTH(cams[mcf_helicrash_3], 0.15)
				
				VECTOR vRot
				vRot = GET_ENTITY_ROTATION(vehs[mvf_heli].id)
				vRot.z += LERP_FLOAT(0, -80.2658, (GET_GAME_TIMER() - i_crash_cutscene_cam_timer)/6000.0)
				SET_ENTITY_ROTATION(vehs[mvf_heli].id, vRot)
			ENDIF
		BREAK
		CASE 9
			IF GET_GAME_TIMER() - i_crash_cutscene_cam_timer >= 17750
				START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_agency3b_heli_expl", <<154.68, -730.11, 252.50>>, <<-90,0,-28.9>>, 1)
				START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_agency3b_heli_exp_trail", <<154.68, -730.11, 252.49>>, <<-90,0,-28.9>>, 1)
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
				SET_USE_HI_DOF()
				i_crash_cutscene_cam_stage++
			ELSE
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
				SET_USE_HI_DOF()
			ENDIF
		BREAK
		CASE 10
			IF GET_GAME_TIMER() - i_crash_cutscene_cam_timer >= 18500
			
//				SET_ENTITY_COORDS(FRANK_PED_ID(), v_ShootoutCoverFrank)
//				TASK_PUT_PED_DIRECTLY_INTO_COVER(FRANK_PED_ID(), v_ShootoutCoverFrank, 1000, TRUE, 0, TRUE, TRUE, peds[mpf_frank].cov, FALSE)
				SET_GAME_PAUSES_FOR_STREAMING(TRUE)
				END_SRL()
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
				i_crash_cutscene_cam_stage 	= 99
				i_crash_cutscene_dialogue 	= 99
				
			ELSE
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
				SET_USE_HI_DOF()
			
				IF GET_GAME_TIMER() - i_crash_cutscene_cam_timer >= 18170
				
					IF NOT bCrashReturnShakeAndPTFX
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_agency_atrium_silt", <<129.60, -750.79, 263.0>>, <<0,0,0>>)
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_agency_atrium_silt", <<130.13, -742.39, 261.7>>, <<0,0,0>>)
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_agency_atrium_silt", <<134.40, -748.22, 265.28>>, <<0,0,0>>)
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_agency_atrium_silt", <<141.66, -756.8, 261.96>>, <<0,0,0>>)
						START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_agency_atrium_glass", <<130.60,-743.95,262.49>>, <<0,0,0>>)
						SHAKE_GAMEPLAY_CAM("MEDIUM_EXPLOSION_SHAKE", 0.2)
						bCrashReturnShakeAndPTFX = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF i_crash_cutscene_cam_stage > 0
	AND i_crash_cutscene_cam_stage < 99
		SET_SRL_TIME(TO_FLOAT(GET_GAME_TIMER() - i_crash_cutscene_cam_timer))
	ENDIF
	
	// DIALOGUE
	SWITCH i_crash_cutscene_dialogue
		CASE 0
			IF i_crash_cutscene_cam_stage > 1
			AND IS_SAFE_TO_START_CONVERSATION()
			AND CREATE_CONVERSATION(sConvo, strDialogue, "AH_SPOTTED", CONV_PRIORITY_HIGH)
				i_crash_cutscene_dialogue++
			ENDIF
		BREAK
		CASE 1
			IF i_crash_cutscene_cam_stage > 2
			AND IS_SAFE_TO_START_CONVERSATION()
			AND CREATE_CONVERSATION(sConvo, strDialogue, "AH_FLIPSWIT", CONV_PRIORITY_HIGH)
				i_crash_cutscene_dialogue++
			ENDIF
		BREAK
		CASE 2
			IF i_crash_cutscene_cam_stage > 4
			AND IS_SAFE_TO_START_CONVERSATION()
			AND CREATE_CONVERSATION(sConvo, strDialogue, "AH_INCOMING", CONV_PRIORITY_HIGH)
				i_crash_cutscene_dialogue++
			ENDIF
		BREAK
		CASE 3
			IF i_crash_cutscene_cam_stage > 5
			AND IS_SAFE_TO_START_CONVERSATION()
			AND CREATE_CONVERSATION(sConvo, strDialogue, "AH_PILOTHIT", CONV_PRIORITY_HIGH)
				i_crash_cutscene_dialogue++
			ENDIF
		BREAK
		CASE 4
			IF i_crash_cutscene_cam_stage > 6
			AND IS_SAFE_TO_START_CONVERSATION()
			AND CREATE_CONVERSATION(sConvo, strDialogue, "AH_NOCONTROL", CONV_PRIORITY_HIGH)
				i_crash_cutscene_dialogue++
			ENDIF
		BREAK
		CASE 5
			IF i_crash_cutscene_cam_stage > 7
			AND IS_SAFE_TO_START_CONVERSATION()
			AND CREATE_CONVERSATION(sConvo, strDialogue, "AH_UHOH", CONV_PRIORITY_HIGH)
				i_crash_cutscene_dialogue++
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC me_heli_to_heli_missile()
	SWITCH mEvents[mef_heli_to_heli_missile].iStage
		CASE 1
			Load_Asset_Model(sAssetData, GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG))
			Load_Asset_PTFX(sAssetData)
			
			IF HAS_MODEL_LOADED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG))
			AND HAS_PTFX_ASSET_LOADED()
			AND DOES_ENTITY_EXIST(vehs[mvf_heli].id)
				
				sRocket.v_start = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehs[mvf_heli_attack].id, <<1.7354, -1.8334, -0.4724>>)
				sRocket.obj = CREATE_OBJECT_NO_OFFSET(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG), sRocket.v_start)
				sRocket.veh_target = vehs[mvf_heli].id
				sRocket.v_offset = <<0.0453, -7.5946, 1.6338>>
				sRocket.b_reached_target = FALSE
				sRocket.b_add_entity_speed = FALSE
				sRocket.b_rocket_belongs_to_player = FALSE
				sRocket.f_RocketSpeed = 40.0 //50.0
				sRocket.ptfx = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_agency3b_proj_rpg_trail", sRocket.obj, <<0.0, 0.0, 0.0>>,  <<0.0, 0.0, 0.0>>)
								
				SET_ENTITY_LOD_DIST(sRocket.obj, 500)
				SET_ENTITY_NO_COLLISION_ENTITY(sRocket.obj, vehs[mvf_heli_attack].id, FALSE)
				SET_ENTITY_RECORDS_COLLISIONS(sRocket.obj, TRUE)
				Unload_Asset_Model(sAssetData, GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG))
				
				mEvents[mef_heli_to_heli_missile].iStage++
			ENDIF
		BREAK
	ENDSWITCH
	
	IF mEvents[mef_heli_to_heli_missile].iStage >= 2
	AND DOES_ENTITY_EXIST(vehs[mvf_heli].id)

		IF DOES_ENTITY_EXIST(sRocket.obj)
			
			VECTOR v_current_pos
			v_current_pos = GET_ENTITY_COORDS(sRocket.obj)
			
			//Update direction to home into offset, this lets us control how close the missile is to the target.
			IF NOT sRocket.b_reached_target
			AND NOT IS_ENTITY_DEAD(sRocket.veh_target)
				VECTOR v_dest 
				v_dest = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sRocket.veh_target, sRocket.v_offset)
				sRocket.v_dir = (v_dest - v_current_pos) / VMAG(v_dest - v_current_pos)
				
				IF VDIST2(v_current_pos, v_dest) < 25.0					
					sRocket.b_reached_target = TRUE
					sRocket.i_explode_timer = GET_GAME_TIMER()
				ENDIF
				
				sRocket.v_vel = sRocket.v_dir * sRocket.f_RocketSpeed
				
				IF sRocket.b_add_entity_speed
					sRocket.v_vel = sRocket.v_vel + GET_ENTITY_VELOCITY(sRocket.veh_target)
				ENDIF
			ENDIF
		
			//Update rotation to match the direction of movement.
			sRocket.v_rot  = <<0.0, 0.0, 0.0>>
			sRocket.v_rot.x = ATAN2(sRocket.v_dir.z, VMAG(<<sRocket.v_dir.x, sRocket.v_dir.y, 0.0>>))
			sRocket.v_rot.z = ATAN2(sRocket.v_dir.y, sRocket.v_dir.x) - 90.0
		
			//SET_ENTITY_COORDS(sRocket.obj, v_current_pos +@ (sRocket.v_dir * ROCKET_SPEED))
			SET_ENTITY_VELOCITY(sRocket.obj, sRocket.v_vel)
			SET_ENTITY_ROTATION(sRocket.obj, sRocket.v_rot)
			
			//Explode on contact or if scripted to hit
			IF (sRocket.b_reached_target AND GET_GAME_TIMER() - sRocket.i_explode_timer > 3000)
			OR HAS_ENTITY_COLLIDED_WITH_ANYTHING(sRocket.obj)
				IF NOT IS_ENTITY_DEAD(sRocket.veh_target)
					IF IS_ENTITY_TOUCHING_ENTITY(sRocket.obj, sRocket.veh_target)
						//SET_VEHICLE_ENGINE_HEALTH(sRocket.veh_target, 150.0)
						SET_ENTITY_HEALTH(sRocket.veh_target, 1)
					ENDIF
				ENDIF

				ADD_EXPLOSION(v_current_pos, EXP_TAG_ROCKET, 1.0)
				ADD_EXPLOSION(<<129.7160, -744.6680, 263.7435>>, EXP_TAG_ROCKET, 1.0, FALSE, TRUE, 0.0)
				ptfxs[ptfx_tail_rotor] = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_agency3b_shot_chopper", vehs[mvf_heli].id, <<0.0453, -7.5946, 1.6338>>, <<0,0,0>>, 1.0)

				IF DOES_PARTICLE_FX_LOOPED_EXIST(sRocket.ptfx)
					STOP_PARTICLE_FX_LOOPED(sRocket.ptfx)
					sRocket.ptfx = NULL
				ENDIF

				DELETE_OBJECT(sRocket.obj)
				TRIGGER_MUSIC_EVENT("AH3B_HELI_HIT")
				
				Event_Finish(mef_heli_to_heli_missile)
			ENDIF	
		ENDIF
	ENDIF
ENDPROC

PROC me_heli_rappel_flyby()
	SWITCH mEvents[mef_heli_rpl_flyby].iStage
		CASE 1
			Load_Asset_Model(sAssetData, mod_heli_attack)
			Load_Asset_Model(sAssetData, mod_pilot_enemy)
			Load_Asset_Recording(sAssetData, rec_heli_rpl_flyby, str_car_recs)
		
			IF HAS_MODEL_LOADED(mod_pilot_enemy)
			AND HAS_MODEL_LOADED(mod_heli_attack)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_heli_rpl_flyby, str_car_recs)
			//AND s_rappel_mike.vAttachPos.z < -50
				vehs[mvf_heli_attack].id = CREATE_VEHICLE(mod_heli_attack, << 139.0329, -789.1915, 151.4856 >>)
				CREATE_MISSION_PED_In_Vehicle(vehs[mvf_heli_attack].peds[0], mod_pilot_enemy, vehs[mvf_heli_attack], VS_DRIVER, "CREW_PILOT", REL_ENEMY)
				SET_PED_AUTO_BLIPS_ACTIVE(vehs[mvf_heli_attack].peds[0], FALSE)
				SET_PED_PROP_INDEX(vehs[mvf_heli_attack].peds[0].id, ANCHOR_HEAD, 0, 0)
				SET_PED_COMPONENT_VARIATION(vehs[mvf_heli_attack].peds[0].id, PED_COMP_SPECIAL2, 1, 0)
				
				SET_PED_SUFFERS_CRITICAL_HITS(vehs[mvf_heli_attack].peds[0].id, FALSE)
				SET_PED_CONFIG_FLAG(vehs[mvf_heli_attack].peds[0].id, PCF_GetOutBurningVehicle, 		FALSE)
				SET_PED_CONFIG_FLAG(vehs[mvf_heli_attack].peds[0].id, PCF_GetOutUndriveableVehicle, 	FALSE)
				SET_PED_CONFIG_FLAG(vehs[mvf_heli_attack].peds[0].id, PCF_RunFromFiresAndExplosions, 	FALSE)
				SET_PED_CONFIG_FLAG(vehs[mvf_heli_attack].peds[0].id, PCF_DisableExplosionReactions, 	TRUE)
				
				Unload_Asset_Model(sAssetData, mod_heli_attack)
				Unload_Asset_Model(sAssetData, mod_pilot_enemy)

				START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli_attack].id, rec_heli_rpl_flyby, str_car_recs)
				SET_PLAYBACK_SPEED(vehs[mvf_heli_attack].id, 0.75)
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli_attack].id, 5000)
				SET_HELI_BLADES_FULL_SPEED(vehs[mvf_heli_attack].id)
				
				Event_Next_Stage(mef_heli_rpl_flyby)
			ENDIF
		BREAK
		CASE 2
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_heli_attack].id)
			
				SET_GLOBAL_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehs[mvf_heli_attack].id, <<0,0,50.0>>)
			
			ELSE
				DELETE_PED(vehs[mvf_heli_attack].peds[0].id)
				DELETE_VEHICLE(vehs[mvf_heli_attack].id)
				
				Unload_Asset_Recording(sAssetData, rec_heli_rpl_flyby, str_car_recs)
				Event_Finish(mef_heli_rpl_flyby)
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC me_heli_rappel_attack()

	// Check if the heli is dead or the pilot is dead
	IF mEvents[mef_heli_rpl_attack].iStage >= 2
	AND (NOT DOES_ENTITY_EXIST(vehs[mvf_heli_attack].peds[0].id)
	OR IS_PED_INJURED(vehs[mvf_heli_attack].peds[0].id)
	OR NOT DOES_ENTITY_EXIST(vehs[mvf_heli_attack].id)
	OR NOT IS_VEHICLE_DRIVEABLE(vehs[mvf_heli_attack].id))
	
		IF DOES_BLIP_EXIST(vehs[mvf_heli_attack].blip)
		
			IF DOES_ENTITY_EXIST(vehs[mvf_heli_attack].id)
			AND NOT IS_ENTITY_DEAD(vehs[mvf_heli_attack].id)
			
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_heli_attack].id)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli_attack].id)
					CPRINTLN(DEBUG_MIKE, "@@@@@@@ AH3B: STOPPING PLAYBACK ON ATTACK HELI")
				ENDIF
			
				SET_VEHICLE_OUT_OF_CONTROL(vehs[mvf_heli_attack].id, FALSE, TRUE)
				
				// Pilot must be dead if the heli is intact
				PLAY_SOUND_FROM_ENTITY(-1, "Chopper_Goes_Down", vehs[mvf_heli_attack].id, "FBI_HEIST_FIGHT_CHOPPER_SOUNDS")
				
				CPRINTLN(DEBUG_MIKE, "@@@@@@@ AH3B: SETTING ATTACK HELI OUT OF CONTROL")
				
			ELSE
				
				// Heli is already destroyed play destroyed sound and end the event
				PLAY_SOUND_FROM_ENTITY(-1, "Chopper_Destroyed", vehs[mvf_heli_attack].id, "FBI_HEIST_FIGHT_CHOPPER_SOUNDS")
				
				CPRINTLN(DEBUG_MIKE, "@@@@@@@ AH3B: ATTACK HELI DESTROYED INSTANTLY")
				
				IF DOES_ENTITY_EXIST(vehs[mvf_heli_attack].id)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_heli_attack].id)
				ENDIF
				
				Event_Finish(mef_heli_rpl_attack)

			ENDIF
			
			CPRINTLN(DEBUG_MIKE, "@@@@@@@ AH3B: ATTACK HELI BLIP REMOVED")
			TRIGGER_MUSIC_EVENT("AH3B_CHOPPER_DEAD")
			SAFE_REMOVE_BLIP(vehs[mvf_heli_attack].blip)

		// waiting for the final crash to play the sound
		ELSE
		
			IF IS_ENTITY_DEAD(vehs[mvf_heli_attack].id)
			
				PLAY_SOUND_FROM_ENTITY(-1, "Chopper_Destroyed", vehs[mvf_heli_attack].id, "FBI_HEIST_FIGHT_CHOPPER_SOUNDS")
				
				CPRINTLN(DEBUG_MIKE, "@@@@@@@ AH3B: ATTACK HELI CRASHED & DESTROYED")
				
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_heli_attack].id)
				Event_Finish(mef_heli_rpl_attack)
				
			ELSE
				// While crashing push the heli away from the building.
				VECTOR vHeliCoord 	= GET_ENTITY_COORDS(vehs[mvf_heli_attack].id)
				vHeliCoord.z = 0
				VECTOR vPushDir 	= NORMALISE_VECTOR(vHeliCoord - <<135.0158, -749.3968, 0.0>>)
				APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(vehs[mvf_heli_attack].id, APPLY_TYPE_FORCE, vPushDir * 10.0, 0, FALSE, TRUE)
				CPRINTLN(DEBUG_MIKE, "@@@@@@@ AH3B: PUSHING CHOPPER")
			ENDIF

		ENDIF
		
		Unload_Asset_Recording(sAssetData, rec_heli_rpl_attack, str_car_recs)
			
	ELSE
		// Give the driveby task
		IF mEvents[mef_heli_rpl_attack].iStage >= 4
			SCRIPTTASKSTATUS sts
			sts = GET_SCRIPT_TASK_STATUS(vehs[mvf_heli_attack].peds[0].id, SCRIPT_TASK_VEHICLE_MISSION)
			IF sts = PERFORMING_TASK
				IF NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(vehs[mvf_heli_attack].peds[0].id)
				
					TASK_DRIVE_BY(vehs[mvf_heli_attack].peds[0].id, PLAYER_PED_ID(), null, <<0,0,0>>, 2000.0, 100, TRUE, FIRING_PATTERN_BURST_FIRE_HELI)
					CDEBUG1LN(DEBUG_MIKE, "[HELI CONTROLLER] DriveBy Task given, attacking PLAYER_PED()")
				ENDIF
			ENDIF
		ENDIF
		
		INT iNewCoordID[2]
		FLOAT fDistSQR[2]

		SWITCH mEvents[mef_heli_rpl_attack].iStage
			CASE 1
			
				Load_Asset_Model(sAssetData, mod_heli_attack)
				Load_Asset_Model(sAssetData, mod_pilot_enemy)
				Load_Asset_Model(sAssetData, mod_swat)
				Load_Asset_Recording(sAssetData, rec_heli_rpl_attack, str_car_recs)
				Load_Asset_Waypoint(sAssetData, "ah3b_heli_bullets")
			
				IF HAS_MODEL_LOADED(mod_pilot_enemy)
				AND HAS_MODEL_LOADED(mod_heli_attack)
				AND HAS_MODEL_LOADED(mod_swat)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_heli_rpl_attack, str_car_recs)
				AND GET_IS_WAYPOINT_RECORDING_LOADED("ah3b_heli_bullets")
				
					IF DOES_ENTITY_EXIST(vehs[mvf_heli_attack].id)
						SET_ENTITY_COORDS(vehs[mvf_heli_attack].id, <<100.1619, -723.7661, 79.8182>>)
						SET_ENTITY_HEADING(vehs[mvf_heli_attack].id, -72.0956)
					ELSE
						vehs[mvf_heli_attack].id = CREATE_VEHICLE(mod_heli_attack, <<100.1619, -723.7661, 79.8182>>, -72.0956)
					ENDIF
					Event_Finish(mef_heli_rpl_flyby)	// kill the other event thats controlling the heli currently so that it doesn't clean it up
					
					IF NOT DOES_ENTITY_EXIST(vehs[mvf_heli_attack].peds[0].id)
						CREATE_MISSION_PED_In_Vehicle(vehs[mvf_heli_attack].peds[0], mod_pilot_enemy, vehs[mvf_heli_attack], VS_DRIVER, "ATT_PILOT", REL_ENEMY)
					ENDIF
					SET_PED_AUTO_BLIPS_ACTIVE(vehs[mvf_heli_attack].peds[0], FALSE)
					CREATE_MISSION_PED_In_Vehicle(vehs[mvf_heli_attack].peds[1], mod_swat, vehs[mvf_heli_attack], VS_BACK_LEFT, 	"ATT_GUNNER_L", REL_ENEMY, WEAPONTYPE_CARBINERIFLE, WEAPONCOMPONENT_AT_SCOPE_MEDIUM)
					CREATE_MISSION_PED_In_Vehicle(vehs[mvf_heli_attack].peds[2], mod_swat, vehs[mvf_heli_attack], VS_BACK_RIGHT, 	"ATT_GUNNER_R", REL_ENEMY, WEAPONTYPE_CARBINERIFLE, WEAPONCOMPONENT_AT_SCOPE_MEDIUM)
					vehs[mvf_heli_attack].peds[0].bAIBlipsActive = FALSE
					vehs[mvf_heli_attack].peds[1].bAIBlipsActive = FALSE
					vehs[mvf_heli_attack].peds[2].bAIBlipsActive = FALSE
					
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehs[mvf_heli_attack].id, "AH_3B_ENEMY_CHOPPER_GROUP", 0)
					
					SET_PED_PROP_INDEX(vehs[mvf_heli_attack].peds[0].id, ANCHOR_HEAD, 0, 0)
					SET_PED_COMPONENT_VARIATION(vehs[mvf_heli_attack].peds[0].id, PED_COMP_SPECIAL2, 1, 0)
					SET_HELI_BLADES_FULL_SPEED(vehs[mvf_heli_attack].id)
					CONTROL_MOUNTED_WEAPON(vehs[mvf_heli_attack].peds[0].id)
					SET_CURRENT_PED_VEHICLE_WEAPON(vehs[mvf_heli_attack].peds[0].id, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
					SET_PED_CAN_BE_TARGETTED(vehs[mvf_heli_attack].peds[0].id, FALSE)
					SET_VEHICLE_CAN_BE_TARGETTED(vehs[mvf_heli_attack].id, TRUE)
					SET_FORCE_VEHICLE_ENGINE_DAMAGE_BY_BULLET(vehs[mvf_heli_attack].id, TRUE)
					
					Unload_Asset_Model(sAssetData, mod_heli_attack)
					Unload_Asset_Model(sAssetData, mod_pilot_enemy)
					Unload_Asset_Model(sAssetData, mod_swat)
					
					SET_PED_SUFFERS_CRITICAL_HITS(vehs[mvf_heli_attack].peds[0].id, FALSE)
					SET_PED_CONFIG_FLAG(vehs[mvf_heli_attack].peds[0].id, PCF_GetOutBurningVehicle, 		FALSE)
					SET_PED_CONFIG_FLAG(vehs[mvf_heli_attack].peds[0].id, PCF_GetOutUndriveableVehicle, 	FALSE)
					SET_PED_CONFIG_FLAG(vehs[mvf_heli_attack].peds[0].id, PCF_RunFromFiresAndExplosions, 	FALSE)
					SET_PED_CONFIG_FLAG(vehs[mvf_heli_attack].peds[0].id, PCF_DisableExplosionReactions, 	TRUE)
					
					START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli_attack].id, rec_heli_rpl_attack, str_car_recs)
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli_attack].id, 2000)
					SET_GLOBAL_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehs[mvf_heli_attack].id, <<0.0,0.0,2.0>>)
					//SET_ADDITIONAL_ROTATION_FOR_RECORDED_VEHICLE_PLAYBACK(vehs[mvf_heli_attack].id, <<-10.0,0.0,0.0>>)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_heli_attack].id)
					
					SAFE_BLIP_VEHICLE(vehs[mvf_heli_attack].blip, vehs[mvf_heli_attack].id, TRUE)

					Event_Next_Stage(mef_heli_rpl_attack)

				ENDIF
			BREAK
			CASE 2
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_heli_attack].id)
				AND NOT IS_PED_INJURED(vehs[mvf_heli_attack].peds[0].id)
				
					CONST_FLOAT					STRAFE_START_TIME					3908.487
					CONST_FLOAT					STRAFE_END_TIME						11028.560
				
					FLOAT fPlaybackProg
					
					fPlaybackProg = GET_TIME_POSITION_IN_RECORDING(vehs[mvf_heli_attack].id)
					
					#IF IS_DEBUG_BUILD
						f_debug_rec_pos2 = fPlaybackProg
					#ENDIF

					IF fPlaybackProg >= STRAFE_END_TIME
					
						Event_Next_Stage(mef_heli_rpl_attack)
						
					ELIF fPlaybackProg > STRAFE_START_TIME
					
						VECTOR						vTargetCoordOffset					
						vTargetCoordOffset = <<0,0,1.0>>
					
						FLOAT fTotalStrafeTime,	fSectionLength, fCurrentShootTime, fSectionProgressPerc, fV1Time, fV2Time
						INT iNumPoints, iNumSections, iCurrentSection
						VECTOR vTargetCoord, v1, v2
					
						WAYPOINT_RECORDING_GET_NUM_POINTS("ah3b_heli_bullets", iNumPoints)
						iNumSections = iNumPoints-1
						
						fTotalStrafeTime 		= STRAFE_END_TIME - STRAFE_START_TIME
						fSectionLength 			= fTotalStrafeTime/iNumSections
						fCurrentShootTime		= fPlaybackProg - STRAFE_START_TIME
						iCurrentSection 		= CLAMP_INT(CEIL(fCurrentShootTime/fSectionLength), 1, iNumPoints-1)
						fV1Time					= (iCurrentSection-1)*fSectionLength
						fV2Time					= iCurrentSection*fSectionLength
						fSectionProgressPerc 	= CLAMP((fCurrentShootTime-fV1Time)/(fV2Time-fV1Time), 0.0, 1.0)

						IF WAYPOINT_RECORDING_GET_COORD("ah3b_heli_bullets", iCurrentSection-1, v1)
						AND WAYPOINT_RECORDING_GET_COORD("ah3b_heli_bullets", iCurrentSection, v2)
							vTargetCoord = LERP_VECTOR(v1, v2, fSectionProgressPerc) + vTargetCoordOffset
						ELSE 
							vTargetCoord = <<0,0,0>>
						ENDIF
						
						#IF IS_DEBUG_BUILD
						IF bDisplayEventDebug
							TEXT_LABEL_31 strDebug
							strDebug = "fSectionLength: "
							strDebug += ROUND(fSectionLength)
							DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.475, 0.0>>, 255, 255, 255, 255)
							strDebug = "fV1Time: "
							strDebug += ROUND(fV1Time)
							DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.5, 0.0>>, 255, 255, 255, 255)
							strDebug = "fV2Time: "
							strDebug += ROUND(fV2Time)
							DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.525, 0.0>>, 255, 255, 255, 255)
							strDebug = "fPlaybackProg: "
							strDebug += ROUND(fPlaybackProg)
							DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.55, 0.0>>, 255, 255, 255, 255)
							strDebug = "fCurrentShootTime: "
							strDebug += ROUND(fCurrentShootTime)
							DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.575, 0.0>>, 255, 255, 255, 255)
							
							CPRINTLN(DEBUG_MIKE, "@@@@@@@@@@@@@@ [AH3B] Heli strafe,  fSectionProgressPerc: ", fSectionProgressPerc)
							
							DRAW_DEBUG_LINE(GET_ENTITY_COORDS(vehs[mvf_heli_attack].id), vTargetCoord, 255, 0, 0, 255)
						ENDIF
						#ENDIF
					
						IF NOT IS_VECTOR_ZERO(vTargetCoord)

							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_heli_attack].peds[0].id, SCRIPT_TASK_DRIVE_BY)
								
								SET_PED_ACCURACY(vehs[mvf_heli_attack].peds[0].id, 20)
								SET_PED_FIRING_PATTERN(vehs[mvf_heli_attack].peds[0].id, FIRING_PATTERN_FULL_AUTO)
								SET_PED_TARGET_LOSS_RESPONSE(vehs[mvf_heli_attack].peds[0].id, TLR_NEVER_LOSE_TARGET)
								
								TASK_DRIVE_BY(vehs[mvf_heli_attack].peds[0].id, null, null, vTargetCoord, 1000, 100, FALSE, FIRING_PATTERN_FULL_AUTO)
							
							ELIF GET_SCRIPT_TASK_STATUS(vehs[mvf_heli_attack].peds[0].id, SCRIPT_TASK_DRIVE_BY) = PERFORMING_TASK
							
								SET_DRIVEBY_TASK_TARGET(vehs[mvf_heli_attack].peds[0].id, null, null, vTargetCoord)
								
							ENDIF
						
						ENDIF
					
					ENDIF
					
				// heli pilot dead, jump a stage
				ELSE
					Event_Next_Stage(mef_heli_rpl_attack)
				ENDIF
					
			BREAK
			CASE 3				
				// stop recording and drive by, switch to heli_mission attack player
				IF IS_VEHICLE_DRIVEABLE(vehs[mvf_heli_attack].id)
				AND NOT IS_PED_INJURED(vehs[mvf_heli_attack].peds[0].id)
				 	
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_heli_attack].id)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli_attack].id)
					ENDIF
					
					CLEAR_PED_TASKS(vehs[mvf_heli_attack].peds[0].id)
					SET_PED_FIRING_PATTERN(vehs[mvf_heli_attack].peds[0].id, FIRING_PATTERN_BURST_FIRE_HELI)
					SET_PED_ACCURACY(vehs[mvf_heli_attack].peds[0].id, 5)
					SET_PED_SHOOT_RATE(vehs[mvf_heli_attack].peds[0].id, 100)
					
					Unload_Asset_Recording(sAssetData, rec_heli_rpl_attack, str_car_recs)
					Unload_Asset_Waypoint(sAssetData, "ah3b_heli_bullets")
					
					eHeliAIState 		= HELIAI_ATTACK_MOVING 
					eHeliAIStatePrev	= HELIAI_ATTACK_MOVING

					mEvents[mef_heli_rpl_attack].iStage++
				ENDIF
			BREAK
			CASE 4

			// Target update
			
				IF GET_GAME_TIMER() > iHeliAttackTargetUpdateTimer	
				
					FLOAT fDist, fTempDist
					fDist = 999999999
				
					fTempDist = VDIST2(GET_ENTITY_COORDS(vehs[mvf_heli_attack].id), GET_ENTITY_COORDS(PLAYER_PED_ID()))
					IF fTempDist < fDist
						fDist = fTempDist
						pedHeliAttackTarget = PLAYER_PED_ID()
					ENDIF
					
					fTempDist = VDIST2(GET_ENTITY_COORDS(vehs[mvf_heli_attack].id), GET_ENTITY_COORDS(BUDDY_PED_ID()))
					IF fTempDist < fDist
						fDist = fTempDist
						pedHeliAttackTarget = BUDDY_PED_ID()
					ENDIF
					
					fTempDist = VDIST2(GET_ENTITY_COORDS(vehs[mvf_heli_attack].id), GET_ENTITY_COORDS(peds[mpf_goon].id))
					IF fTempDist < fDist
						fDist = fTempDist
						pedHeliAttackTarget = peds[mpf_goon].id
					ENDIF
					
					iHeliAttackTargetUpdateTimer = GET_GAME_TIMER() + 7000
				
				ENDIF
			
			// Ped hanging off the side of the heli
				IF DOES_ENTITY_EXIST(vehs[mvf_heli_attack].peds[1].id)
				AND NOT IS_PED_INJURED(vehs[mvf_heli_attack].peds[1].id)
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_heli_attack].peds[1].id, SCRIPT_TASK_COMBAT)
						TASK_COMBAT_PED(vehs[mvf_heli_attack].peds[1].id, pedHeliAttackTarget)
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(vehs[mvf_heli_attack].peds[2].id)
				AND NOT IS_PED_INJURED(vehs[mvf_heli_attack].peds[2].id)
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_heli_attack].peds[2].id, SCRIPT_TASK_COMBAT)
						TASK_COMBAT_PED(vehs[mvf_heli_attack].peds[2].id, pedHeliAttackTarget)
					ENDIF
				ENDIF
			
				INT i

			// Monitor damage values
				IF eHeliAIState != HELIAI_EVADE
					
					INT iCurrentHealth
					iCurrentHealth = GET_ENTITY_HEALTH(vehs[mvf_heli_attack].peds[0].id)
					IF iCurrentHealth < iHeliAttackPilotHealthLastFrame
						iHeliAttackPilotDamageTaken += iHeliAttackPilotHealthLastFrame - iCurrentHealth
					ENDIF
					iHeliAttackPilotHealthLastFrame = iCurrentHealth
					
					iCurrentHealth = GET_ENTITY_HEALTH(vehs[mvf_heli_attack].id)
					IF iCurrentHealth < iHeliAttackHealthLastFrame
						iHeliAttackDamageTaken += iHeliAttackHealthLastFrame - iCurrentHealth
					ENDIF
					iHeliAttackHealthLastFrame = iCurrentHealth
					
				ELSE
					iHeliAttackPilotDamageTaken 		= 0
					iHeliAttackDamageTaken				= 0
					iHeliAttackPilotHealthLastFrame		= GET_ENTITY_HEALTH(vehs[mvf_heli_attack].peds[0].id)
					iHeliAttackHealthLastFrame 			= GET_ENTITY_HEALTH(vehs[mvf_heli_attack].id)
				ENDIF
			
			// State update
				eHeliAIStatePrev = eHeliAIState
				
				CONST_INT		I_PILOT_DAMAGE_EVADE_THRESHOLD		25
				CONST_INT		I_HELI_DAMAGE_EVADE_THRESHOLD		50
			
				IF eHeliAIState != HELIAI_EVADE
				AND (iHeliAttackPilotDamageTaken > I_PILOT_DAMAGE_EVADE_THRESHOLD
				OR iHeliAttackDamageTaken > I_HELI_DAMAGE_EVADE_THRESHOLD
				OR (DOES_ENTITY_EXIST(vehs[mvf_heli_attack].peds[1].id) AND IS_PED_INJURED(vehs[mvf_heli_attack].peds[1].id)) 
				OR (DOES_ENTITY_EXIST(vehs[mvf_heli_attack].peds[2].id) AND IS_PED_INJURED(vehs[mvf_heli_attack].peds[2].id)) )
				
					// Reset the damage taken values
					iHeliAttackPilotDamageTaken = 0
					iHeliAttackDamageTaken		= 0
					
					IF DOES_ENTITY_EXIST(vehs[mvf_heli_attack].peds[1].id) AND IS_PED_INJURED(vehs[mvf_heli_attack].peds[1].id)
						SET_PED_AS_NO_LONGER_NEEDED(vehs[mvf_heli_attack].peds[1].id)
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehs[mvf_heli_attack].peds[2].id) AND IS_PED_INJURED(vehs[mvf_heli_attack].peds[2].id)
						SET_PED_AS_NO_LONGER_NEEDED(vehs[mvf_heli_attack].peds[2].id)
					ENDIF
					
					// Randomly pick one of the evasion coords which is to either side of the current coord
					IF GET_RANDOM_BOOL()
						IF iHeliAttackSelectedCoord+1 < COUNT_OF(vHeliAttackCoords)
							iHeliAttackSelectedCoord += 2
						ELSE
							iHeliAttackSelectedCoord -= 2
						ENDIF
					ELSE
						IF iHeliAttackSelectedCoord-1 >= 0
							iHeliAttackSelectedCoord -= 2
						ELSE
							iHeliAttackSelectedCoord += 2
						ENDIF
					ENDIF
					
					iHeliAttackSelectedCoord 	= CLAMP_INT(iHeliAttackSelectedCoord, 0, COUNT_OF(vHeliAttackCoords)-1)
					iHeliAttackAITimer 			= GET_GAME_TIMER()
					eHeliAIState 				= HELIAI_EVADE
				ELSE
					SWITCH eHeliAIState
						CASE HELIAI_EVADE
						
							// Finished evading, begin attack from current position
							IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(vehs[mvf_heli_attack].id), vHeliAttackCoords[iHeliAttackSelectedCoord], 5.0)
								iHeliAttackAITimer 	= GET_GAME_TIMER()
								eHeliAIState 		= HELIAI_ATTACK_STATIONARY
							ENDIF
						
						BREAK
						CASE HELIAI_ATTACK_MOVING
						
							// Reached new coord, set to stationary attack
							IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(vehs[mvf_heli_attack].id), vHeliAttackCoords[iHeliAttackSelectedCoord], 5.0)
								iHeliAttackAITimer 	= GET_GAME_TIMER()
								eHeliAIState 		= HELIAI_ATTACK_STATIONARY
							ENDIF
						
						BREAK
						CASE HELIAI_ATTACK_STATIONARY
						
							// Move to another point, use nearest point to player
							IF GET_GAME_TIMER() - iHeliAttackAITimer > 5000
							
								iNewCoordID[0]	= -1
								fDistSQR[0]		= -1
								
								REPEAT COUNT_OF(vHeliAttackCoords) i
									IF i != iHeliAttackSelectedCoord
										FLOAT fDistSQRTemp 
										fDistSQRTemp = VDIST2(GET_ENTITY_COORDS(pedHeliAttackTarget), vHeliAttackCoords[i])
										IF iNewCoordID[0] = -1 
										OR fDistSQR[0] = -1 
										OR fDistSQRTemp < fDistSQR[0]
											fDistSQR[0]	= fDistSQRTemp
											iNewCoordID[0]	= i
										ENDIF
									ENDIF
								ENDREPEAT
							
								IF iNewCoordID[0] != -1
									iHeliAttackSelectedCoord = iNewCoordID[0]
								ENDIF
								
								iHeliAttackAITimer 	= GET_GAME_TIMER()
								eHeliAIState 		= HELIAI_ATTACK_MOVING
							ENDIF
						
						BREAK
					ENDSWITCH
				ENDIF	
				
				CONST_FLOAT 			F_HELI_FACING_ARC				60.0
				CONST_FLOAT 			F_HELI_EVADE_SPEED				40.0
				CONST_FLOAT				F_HELI_MOVE_SPEED				30.0
				VECTOR vTargetsCoords, vHeliCoords
				FLOAT fHeading
			
			// Take update
				SWITCH eHeliAIState
					CASE HELIAI_EVADE
					
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_heli_attack].peds[0].id, SCRIPT_TASK_VEHICLE_MISSION)
						OR eHeliAIStatePrev != eHeliAIState
						
							TASK_HELI_MISSION(vehs[mvf_heli_attack].peds[0].id, vehs[mvf_heli_attack].id, 
								NULL, NULL, vHeliAttackCoords[iHeliAttackSelectedCoord], MISSION_GOTO, F_HELI_EVADE_SPEED, 5, -1, 80, 20, -1)
							
						ENDIF
						
						IF IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(vehs[mvf_heli_attack].peds[0].id)
							CLEAR_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(vehs[mvf_heli_attack].peds[0].id)
						ENDIF
					
					BREAK
					CASE HELIAI_ATTACK_MOVING
					
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_heli_attack].peds[0].id, SCRIPT_TASK_VEHICLE_MISSION)
						OR eHeliAIStatePrev != eHeliAIState
						OR NOT IS_PED_FACING_PED(vehs[mvf_heli_attack].peds[0].id, pedHeliAttackTarget, F_HELI_FACING_ARC)
						
							vTargetsCoords = GET_ENTITY_COORDS(pedHeliAttackTarget)
							vHeliCoords = GET_ENTITY_COORDS(vehs[mvf_heli_attack].id)
							fHeading = GET_HEADING_FROM_VECTOR_2D(vTargetsCoords.x - vHeliCoords.x, vTargetsCoords.y - vHeliCoords.y)
						
							TASK_HELI_MISSION(vehs[mvf_heli_attack].peds[0].id, vehs[mvf_heli_attack].id, 
								NULL, NULL, vHeliAttackCoords[iHeliAttackSelectedCoord], MISSION_GOTO, F_HELI_MOVE_SPEED, 5, fHeading, 80, 20, -1)
								
						ELIF GET_SCRIPT_TASK_STATUS(vehs[mvf_heli_attack].peds[0].id, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
						AND NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(vehs[mvf_heli_attack].peds[0].id)
							
							CONTROL_MOUNTED_WEAPON(vehs[mvf_heli_attack].peds[0].id)
							SET_CURRENT_PED_VEHICLE_WEAPON(vehs[mvf_heli_attack].peds[0].id, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
							TASK_DRIVE_BY(vehs[mvf_heli_attack].peds[0].id, pedHeliAttackTarget, null, <<0,0,0>>, 1000, 100, TRUE, FIRING_PATTERN_BURST_FIRE_HELI)

						ENDIF
					
					BREAK
					CASE HELIAI_ATTACK_STATIONARY
					
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_heli_attack].peds[0].id, SCRIPT_TASK_VEHICLE_MISSION)
						OR eHeliAIStatePrev != eHeliAIState
						OR NOT IS_PED_FACING_PED(vehs[mvf_heli_attack].peds[0].id, pedHeliAttackTarget, F_HELI_FACING_ARC)
						
							vTargetsCoords = GET_ENTITY_COORDS(pedHeliAttackTarget)
							vHeliCoords = GET_ENTITY_COORDS(vehs[mvf_heli_attack].id)
							fHeading = GET_HEADING_FROM_VECTOR_2D(vTargetsCoords.x - vHeliCoords.x, vTargetsCoords.y - vHeliCoords.y)
						
							TASK_HELI_MISSION(vehs[mvf_heli_attack].peds[0].id, vehs[mvf_heli_attack].id, 
								NULL, NULL, vHeliAttackCoords[iHeliAttackSelectedCoord], MISSION_GOTO, F_HELI_MOVE_SPEED, 5, fHeading, 80, 20, -1)
							
						ELIF GET_SCRIPT_TASK_STATUS(vehs[mvf_heli_attack].peds[0].id, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
						AND NOT IS_DRIVEBY_TASK_UNDERNEATH_DRIVING_TASK(vehs[mvf_heli_attack].peds[0].id)
							
							CONTROL_MOUNTED_WEAPON(vehs[mvf_heli_attack].peds[0].id)
							SET_CURRENT_PED_VEHICLE_WEAPON(vehs[mvf_heli_attack].peds[0].id, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
							TASK_DRIVE_BY(vehs[mvf_heli_attack].peds[0].id, pedHeliAttackTarget, null, <<0,0,0>>, 1000, 100, TRUE, FIRING_PATTERN_BURST_FIRE_HELI)
							
						ENDIF
					
					BREAK
				ENDSWITCH
			
				#IF IS_DEBUG_BUILD
					IF bDisplayEventDebug
						TEXT_LABEL_63 strDebug
						
						strDebug = "iHeliAttackSelectedCoord: "
						strDebug += iHeliAttackSelectedCoord
						DRAW_DEBUG_TEXT_ABOVE_COORDS(vHeliAttackCoords[iHeliAttackSelectedCoord], strDebug, -0.5, 255, 255, 255, 255)
						DRAW_DEBUG_SPHERE(vHeliAttackCoords[iHeliAttackSelectedCoord], 0.25, 255, 0, 0, 100)
						
						SWITCH eHeliAIState
							CASE HELIAI_EVADE				strDebug = "HELIAI_EVADE"				BREAK
							CASE HELIAI_ATTACK_MOVING		strDebug = "HELIAI_ATT_MOVING"			BREAk
							CASE HELIAI_ATTACK_STATIONARY	strDebug = "HELIAI_ATT_STATIONARY"		BREAK
						ENDSWITCH
						DRAW_DEBUG_TEXT_ABOVE_ENTITY(vehs[mvf_heli_attack].id, strDebug, -1.0, 255, 255, 255, 255)
					ENDIF
				#ENDIF
				
			BREAK
		ENDSWITCH
	
	ENDIF
	
	IF Is_Event_Complete(mef_heli_rpl_attack)
		Unload_Asset_Model(sAssetData, mod_heli_attack)
		Unload_Asset_Model(sAssetData, mod_pilot_enemy)
	ENDIF
		
ENDPROC

PROC me_heli_news()

	IF mEvents[mef_heli_news].iStage >= 2
	AND (NOT DOES_ENTITY_EXIST(vehs[mvf_heli_news].peds[0].id)
	OR IS_PED_INJURED(vehs[mvf_heli_news].peds[0].id)
	OR NOT DOES_ENTITY_EXIST(vehs[mvf_heli_news].id)
	OR NOT IS_VEHICLE_DRIVEABLE(vehs[mvf_heli_news].id))
	
		IF DOES_ENTITY_EXIST(vehs[mvf_heli_news].id)
			IF NOT IS_ENTITY_DEAD(vehs[mvf_heli_news].id)
				SET_VEHICLE_OUT_OF_CONTROL(vehs[mvf_heli_news].id, FALSE, TRUE)
			ENDIF
			
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_heli_news].id)
		ENDIF
		
		TRIGGER_MUSIC_EVENT("AH3B_CHOPPER_DEAD")
		
		Event_Finish(mef_heli_news)
			
	ELSE

		SWITCH mEvents[mef_heli_news].iStage
			CASE 1
				
				Load_Asset_Model(sAssetData, mod_pilot_enemy)
				Load_Asset_Model(sAssetData, FROGGER)
				
				IF HAS_MODEL_LOADED(FROGGER)
				AND HAS_MODEL_LOADED(mod_pilot_enemy)
				
					// Heli hovering with spot light
					vehs[mvf_heli_news].id = CREATE_VEHICLE(FROGGER, <<298.5472, -819.3707, 141.6706>>, 31.7617)
					SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_heli_news].id, 4)
					SET_VEHICLE_ENGINE_ON(vehs[mvf_heli_news].id, TRUE, TRUE)
					SET_HELI_BLADES_FULL_SPEED(vehs[mvf_heli_news].id)
					SET_HELI_TURBULENCE_SCALAR(vehs[mvf_heli_news].id, 0.3)
					Unload_Asset_Model(sAssetData, FROGGER)
					
					vehs[mvf_heli_news].peds[0].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_heli_news].id, PEDTYPE_MISSION, mod_pilot_enemy, VS_DRIVER)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(vehs[mvf_heli_news].peds[0].id, TRUE)
					Unload_Asset_Model(sAssetData, mod_pilot_enemy)
					
					vehs[mvf_heli_news].peds[0].iGenericFlag = 0
					mEvents[mef_heli_news].iStage++
					
				ENDIF
				
			BREAK
			CASE 2
				IF VDIST(GET_ENTITY_COORDS(vehs[mvf_heli_news].id), <<245.2933, -644.2856, 95.0>>) <= 10.0
					
					vehs[mvf_heli_news].peds[0].iGenericFlag = 0
					mEvents[mef_heli_news].iStage++
					
				ELIF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_heli_news].peds[0].id, SCRIPT_TASK_VEHICLE_MISSION)
				OR vehs[mvf_heli_news].peds[0].iGenericFlag = 0
					
					TASK_HELI_MISSION(vehs[mvf_heli_news].peds[0].id, vehs[mvf_heli_news].id, null, null, <<245.2933, -644.2856, 94.4383>>, MISSION_GOTO, 50.0, 8.0, 109.9122, 95, 20)
					vehs[mvf_heli_news].peds[0].iGenericFlag = 1
					
				ENDIF
			BREAK
			CASE 3 FALLTHRU
			CASE 4
				IF Is_Event_Complete(mef_heli_rpl_attack)
				
					vehs[mvf_heli_news].peds[0].iGenericFlag = 0
					mEvents[mef_heli_news].iStage++
				
				ELIF mEvents[mef_heli_news].iStage = 3
				
					IF VDIST(GET_ENTITY_COORDS(vehs[mvf_heli_news].id), <<214.1013, -622.1078, 95.0>>) <= 10.0
						
						vehs[mvf_heli_news].peds[0].iGenericFlag = 0
						mEvents[mef_heli_news].iStage++
						
					ELIF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_heli_news].peds[0].id, SCRIPT_TASK_VEHICLE_MISSION)
					OR vehs[mvf_heli_news].peds[0].iGenericFlag = 0
						
						TASK_HELI_MISSION(vehs[mvf_heli_news].peds[0].id, vehs[mvf_heli_news].id, null, null, <<214.1013, -622.1078, 95.0>>, MISSION_GOTO, 15.0, 8.0, 94.8032, 95, 20)
						vehs[mvf_heli_news].peds[0].iGenericFlag = 1
						
					ENDIF
			
				ELIF mEvents[mef_heli_news].iStage = 4
				
					IF VDIST(GET_ENTITY_COORDS(vehs[mvf_heli_news].id), <<245.2933, -644.2856, 95.0>>) <= 10.0
						
						vehs[mvf_heli_news].peds[0].iGenericFlag = 0
						mEvents[mef_heli_news].iStage--
						
					ELIF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_heli_news].peds[0].id, SCRIPT_TASK_VEHICLE_MISSION)
					OR vehs[mvf_heli_news].peds[0].iGenericFlag = 0
						
						TASK_HELI_MISSION(vehs[mvf_heli_news].peds[0].id, vehs[mvf_heli_news].id, null, null, <<245.2933, -644.2856, 95.0>>, MISSION_GOTO, 15.0, 8.0, 109.9122, 95, 20)
						vehs[mvf_heli_news].peds[0].iGenericFlag = 1
						
					ENDIF
					
				ENDIF
			BREAK
			CASE 5	
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_heli_news].peds[0].id, SCRIPT_TASK_VEHICLE_MISSION)
				OR vehs[mvf_heli_news].peds[0].iGenericFlag = 0
					
					TASK_HELI_MISSION(vehs[mvf_heli_news].peds[0].id, vehs[mvf_heli_news].id, null, null, <<176.3758, -693.5321, 94.1927>>, MISSION_GOTO, 30.0, 8.0, 84.0092, 130, 20)
					vehs[mvf_heli_news].peds[0].iGenericFlag = 1
					
				ENDIF
			BREAK
			CASE 6
				
			BREAK
			
		ENDSWITCH
		
		// Manage spot light
		IF DOES_ENTITY_EXIST(vehs[mvf_heli_news].id) AND IS_VEHICLE_DRIVEABLE(vehs[mvf_heli_news].id)
		AND DOES_ENTITY_EXIST(vehs[mvf_heli_news].peds[0].id) AND NOT IS_PED_INJURED(vehs[mvf_heli_news].peds[0].id)
		
			IF GET_SCRIPT_TASK_STATUS(vehs[mvf_heli_news].peds[0].id, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
					
				IF NOT IS_VEHICLE_SEARCHLIGHT_ON(vehs[mvf_heli_news].id)
					SET_VEHICLE_SEARCHLIGHT(vehs[mvf_heli_news].id, TRUE, TRUE)
				ENDIF	
				IF NOT IS_MOUNTED_WEAPON_TASK_UNDERNEATH_DRIVING_TASK(vehs[mvf_heli_news].peds[0].id)
					CONTROL_MOUNTED_WEAPON(vehs[mvf_heli_news].peds[0].id)
				ENDIF
				
				SWITCH mEvents[mef_heli_news].iStage
					CASE 2		FALLTHRU
					CASE 3		FALLTHRU
					CASE 4
						IF Is_Event_Triggered(mef_heli_rpl_attack)
						AND DOES_ENTITY_EXIST(vehs[mvf_heli_attack].id)
						AND IS_VEHICLE_DRIVEABLE(vehs[mvf_heli_attack].id)
							SET_MOUNTED_WEAPON_TARGET(vehs[mvf_heli_news].peds[0].id, null, null, GET_ENTITY_COORDS(vehs[mvf_heli_attack].id))
						ELSE
							SET_MOUNTED_WEAPON_TARGET(vehs[mvf_heli_news].peds[0].id, null, null, <<187.76, -672.66, 41.41>>)
						ENDIF
					BREAK
					CASE 5
						IF Is_Event_Running(mef_emergency_services)
						AND mEvents[mef_emergency_services].iStage = 4
							IF DOES_ENTITY_EXIST(vehs[mvf_firetruck_1].id)
								SET_MOUNTED_WEAPON_TARGET(vehs[mvf_heli_news].peds[0].id, null, null, GET_ENTITY_COORDS(vehs[mvf_firetruck_1].peds[0].id))
							ENDIF
						ELSE
							SET_MOUNTED_WEAPON_TARGET(vehs[mvf_heli_news].peds[0].id, null, null, <<187.76, -672.66, 41.41>>)
						ENDIF
					BREAK
					CASE 6 
						
					BREAK
				ENDSWITCH
				
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC


PROC me_ground_agents()
	SWITCH mEvents[mef_ground_agents].iStage
		CASE 1
			Load_Asset_Model(sAssetData, mod_fib_agent_1)
			Load_Asset_Model(sAssetData, mod_fib_agent_3)
			
			IF HAS_MODEL_LOADED(mod_fib_agent_1)
			AND HAS_MODEL_LOADED(mod_fib_agent_3)
				CREATE_MISSION_PED(peds[mpf_ground_pre_ent_1], mod_fib_agent_3, <<118.9352, -723.0794, 46.0770>>, 255.1964, "GND_PRE_1", REL_ENEMY, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_FLSH, 5)
				SET_PED_COMBAT_PARAMS(peds[mpf_ground_pre_ent_1], <<118.9352, -723.0794, 46.0770>>, 1.0, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE, FALSE, TRUE)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_ground_pre_ent_1].id, <<118.9352, -723.0794, 46.0770>>, -1, FALSE, 0, TRUE, TRUE, null, FALSE)
				
				CREATE_MISSION_PED(peds[mpf_ground_pre_ent_2], mod_fib_agent_1, <<94.9189, -735.4648, 44.7587>>, 335.3770, "GND_PRE_2", REL_ENEMY, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_FLSH, 5)
				SET_PED_COMBAT_PARAMS(peds[mpf_ground_pre_ent_2], <<100.5248, -731.0294, 45.4243>>, 3.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE, FALSE, TRUE)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_ground_pre_ent_2].id, <<94.9189, -735.4648, 44.7587>>, -1, FALSE, 0, TRUE, FALSE, null, FALSE)
				
				CREATE_MISSION_PED(peds[mpf_ground_pre_ent_3], mod_fib_agent_3, <<91.8254, -727.5392, 45.6780>>, 330.9851, "GND_PRE_3", REL_ENEMY, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_FLSH, 5)
				SET_PED_COMBAT_PARAMS(peds[mpf_ground_pre_ent_3], <<99.1726, -722.4755, 46.0770>>, 2.5, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE, FALSE, TRUE)
				GIVE_WEAPON_COMPONENT_TO_PED(peds[mpf_ground_pre_ent_3].id, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_FLSH)

				mEvents[mef_ground_agents].iStage++
			ENDIF
		BREAK
		CASE 2
			IF IS_PED_INJURED(peds[mpf_ground_pre_ent_1].id)
			OR IS_PED_INJURED(peds[mpf_ground_pre_ent_2].id)
			OR IS_PED_INJURED(peds[mpf_ground_pre_ent_3].id)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), 	<<142.933334,-693.666748,46.076950>>, <<122.324219,-723.926147,53.555561>>, 23.625000)
			OR IS_ENTITY_IN_ANGLED_AREA(BUDDY_PED_ID(), 	<<142.933334,-693.666748,46.076950>>, <<122.324219,-723.926147,53.555561>>, 23.625000)
			OR IS_ENTITY_IN_ANGLED_AREA(peds[mpf_goon].id, 	<<142.933334,-693.666748,46.076950>>, <<122.324219,-723.926147,53.555561>>, 23.625000)
		
				IF DOES_ENTITY_EXIST(peds[mpf_ground_pre_ent_1].id)
				AND NOT IS_PED_INJURED(peds[mpf_ground_pre_ent_1].id)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(peds[mpf_ground_pre_ent_1].id, 100)
				ENDIF
				
				IF DOES_ENTITY_EXIST(peds[mpf_ground_pre_ent_2].id)
				AND NOT IS_PED_INJURED(peds[mpf_ground_pre_ent_2].id)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(peds[mpf_ground_pre_ent_2].id, 100)
				ENDIF
				
				IF DOES_ENTITY_EXIST(peds[mpf_ground_pre_ent_3].id)
				AND NOT IS_PED_INJURED(peds[mpf_ground_pre_ent_3].id)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(peds[mpf_ground_pre_ent_3].id, 100)
				ENDIF
				
				CREATE_MISSION_PED(peds[mpf_ground_ent_1], mod_fib_agent_1, <<104.7831, -750.0425, 44.7563>>, 1.9668, "GND_ENT_1", REL_ENEMY, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_FLSH, 5)
				SET_PED_COMBAT_PARAMS(peds[mpf_ground_ent_1], <<105.8129, -740.6666, 44.7569>>, 1.0, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE, FALSE, TRUE)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_ground_ent_1].id, <<104.7831, -750.0425, 44.7563>>, -1, FALSE, 0, TRUE, TRUE, null, FALSE)
				
				CREATE_MISSION_PED(peds[mpf_ground_ent_2], mod_fib_agent_1, <<100.6031, -750.2360, 44.7558>>, 5.1231, "GND_ENT_2", REL_ENEMY, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_FLSH, 5)
				SET_PED_COMBAT_PARAMS(peds[mpf_ground_ent_2], <<100.6031, -750.2360, 44.7558>>, 1.0, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE, FALSE, TRUE)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_ground_ent_2].id, <<100.6031, -750.2360, 44.7558>>, -1, FALSE, 0, TRUE, FALSE, null, FALSE)
				
				CREATE_MISSION_PED(peds[mpf_ground_ent_3], mod_fib_agent_3, <<105.9348, -740.6089, 44.7569>>, 343.1414, "GND_ENT_3", REL_ENEMY, WEAPONTYPE_PUMPSHOTGUN, WEAPONCOMPONENT_AT_AR_FLSH, 5)
				SET_PED_COMBAT_PARAMS(peds[mpf_ground_ent_3], <<101.8979, -739.2158, 44.7563>>, 1.0, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE, FALSE, TRUE)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_ground_ent_3].id, <<105.9348, -740.6089, 44.7569>>, -1, FALSE, 0, TRUE, FALSE, null, FALSE)
				
				Unload_Asset_Model(sAssetData, mod_fib_agent_1)
				Unload_Asset_Model(sAssetData, mod_fib_agent_3)
				
				mEvents[mef_ground_agents].iStage++
			ENDIF
		BREAK
		CASE 3
			IF IS_PED_INJURED(peds[mpf_ground_ent_1].id)
			OR IS_PED_INJURED(peds[mpf_ground_ent_2].id)
			OR IS_PED_INJURED(peds[mpf_ground_ent_3].id)
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), 	<<90.718597,-710.785645,42.558598>>, <<118.503433,-726.825256,53.059559>>, 7.687500)
			OR IS_ENTITY_IN_ANGLED_AREA(BUDDY_PED_ID(), 	<<90.718597,-710.785645,42.558598>>, <<118.503433,-726.825256,53.059559>>, 7.687500)
			OR IS_ENTITY_IN_ANGLED_AREA(peds[mpf_goon].id, 	<<90.718597,-710.785645,42.558598>>, <<118.503433,-726.825256,53.059559>>, 7.687500)
		
				IF DOES_ENTITY_EXIST(peds[mpf_ground_ent_1].id)
				AND NOT IS_PED_INJURED(peds[mpf_ground_ent_1].id)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(peds[mpf_ground_ent_1].id, 100)
				ENDIF
				
				IF DOES_ENTITY_EXIST(peds[mpf_ground_ent_2].id)
				AND NOT IS_PED_INJURED(peds[mpf_ground_ent_2].id)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(peds[mpf_ground_ent_2].id, 100)
				ENDIF
				
				IF DOES_ENTITY_EXIST(peds[mpf_ground_ent_3].id)
				AND NOT IS_PED_INJURED(peds[mpf_ground_ent_3].id)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(peds[mpf_ground_ent_3].id, 100)
				ENDIF
				
				mEvents[mef_ground_agents].iStage++

			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

PROC me_getaway_driver_arrival()

// GOOD DRIVER
//----------------------------------------------------------------
	IF GET_DRIVER_SKILL_LEVEL() = CMSK_GOOD
		
		SWITCH mEvents[mef_getaway_driver_arrival].iStage
			CASE 1
				// jump ahead on z skip
				IF mission_stage = enum_to_int(msf_10_get_to_getaway_veh)
					
					bDriverHasArrived 	= TRUE
					bFailChecksOnVan 	= TRUE
					mEvents[mef_getaway_driver_arrival].iStage = 6
					
				ELIF mission_substage >= 6
				
					mEvents[mef_getaway_driver_arrival].iStage++
				
				ENDIF
			BREAK
			CASE 2
				
				Load_Asset_Model(sAssetData, AMBULANCE)
				Load_Asset_Recording(sAssetData, rec_driver_ambulance_pullup, str_car_recs)
			
				IF HAS_MODEL_LOADED(AMBULANCE) 
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_driver_ambulance_pullup, str_car_recs)
				
					// Ambulance
					vehs[mvf_escape].id = CREATE_VEHICLE(AMBULANCE, <<293.6366, -589.3726, 42.1395>>, 339.5197)
					SET_VEHICLE_AS_RESTRICTED(vehs[mvf_escape].id, 1)
					SET_VEHICLE_HAS_STRONG_AXLES(vehs[mvf_escape].id, TRUE)
					SET_VEHICLE_TYRES_CAN_BURST(vehs[mvf_escape].id, FALSE)
					SET_VEHICLE_STRONG(vehs[mvf_escape].id, TRUE)
					SET_VEHICLE_LIGHTS(vehs[mvf_escape].id, FORCE_VEHICLE_LIGHTS_ON)
					SET_VEHICLE_SIREN(vehs[mvf_escape].id, TRUE)
					Unload_Asset_Model(sAssetData, AMBULANCE)
					bFailChecksOnVan = TRUE

					mEvents[mef_getaway_driver_arrival].iStage++
				ENDIF
			BREAK
			CASE 3
				IF CREATE_CREW_MEMBER_IN_VEHICLE(peds[mpf_driver], cmf_driver, vehs[mvf_escape], VS_DRIVER, CREW_OUTFIT_STEALTH, DEFAULT, FALSE, TRUE, FALSE, TRUE)
				
					START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_escape].id, rec_driver_ambulance_pullup, str_car_recs)
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_escape].id, 13259)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_escape].id)
					Add_Peds_For_Dialogue()
					
					GIVE_CREW_MEMBER_WEAPON(cmf_driver, FALSE)
					
					mEvents[mef_getaway_driver_arrival].iStage++
				ENDIF
			BREAK
			CASE 4
				IF DOES_ENTITY_EXIST(vehs[mvf_escape].id)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_escape].id)
						Unload_Asset_Recording(sAssetData, rec_driver_ambulance_pullup, str_car_recs)
						TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(peds[mpf_driver].id, vehs[mvf_escape].id)
						TRIGGER_MUSIC_EVENT("AH3B_VAN_READY")
						iTimeDriverArrived = GET_GAME_TIMER()
						bDriverHasArrived = TRUE
						mEvents[mef_getaway_driver_arrival].iStage++
					ENDIF
				ENDIF
			BREAK
			CASE 5
				IF NOT IS_VEHICLE_SEAT_FREE(vehs[mvf_escape].id, VS_FRONT_RIGHT)
				AND GET_PED_IN_VEHICLE_SEAT(vehs[mvf_escape].id, VS_FRONT_RIGHT) = peds[mpf_driver].id
				AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_driver].id, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
					mEvents[mef_getaway_driver_arrival].iStage++
				ENDIF
			BREAK
			CASE 6
				IF bGetwayPickUpReached
					mEvents[mef_getaway_driver_arrival].iTimeStamp = GET_GAME_TIMER()
					mEvents[mef_getaway_driver_arrival].iStage++
				ENDIF
			BREAK
			CASE 7
				IF (IS_PED_SITTING_IN_VEHICLE(MIKE_PED_ID(), vehs[mvf_escape].id)
				AND IS_PED_SITTING_IN_VEHICLE(FRANK_PED_ID(), vehs[mvf_escape].id)
				AND IS_PED_SITTING_IN_VEHICLE(peds[mpf_goon].id, vehs[mvf_escape].id)
				OR GET_GAME_TIMER() - mEvents[mef_getaway_driver_arrival].iTimeStamp > 30000)
					Event_Trigger(mef_swat_fib_arrival)
					Event_Finish(mef_getaway_driver_arrival)
				ENDIF
			BREAK
			
		ENDSWITCH
	
// BAD DRIVER
//----------------------------------------------------------------
	ELIF GET_DRIVER_SKILL_LEVEL() = CMSK_BAD
	
		SWITCH mEvents[mef_getaway_driver_arrival].iStage
			CASE 1
				// Player hit the blip
				IF bGetwayPickUpReached
					Event_Trigger(mef_swat_fib_arrival)
					mEvents[mef_getaway_driver_arrival].iStage++
				ENDIF
			BREAk
			CASE 2
				IF bSwatHaveArrived
					Load_Asset_Recording(sAssetData, rec_driver_van, str_car_recs)
					Load_Asset_Model(sAssetData, mod_esc_veh)
				
					mEvents[mef_getaway_driver_arrival].iTimeStamp = GET_GAME_TIMER()
					mEvents[mef_getaway_driver_arrival].iStage++
				ENDIF
			BREAK
			CASE 3
				IF GET_GAME_TIMER() - mEvents[mef_getaway_driver_arrival].iTimeStamp > 7000
					IF HAS_MODEL_LOADED(mod_esc_veh)
					AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_driver_van, str_car_recs)
					
						vehs[mvf_escape].id = CREATE_VEHICLE(mod_esc_veh, << -8.0629, -740.6199, 43.1547 >>, 250.6042)
						SET_VEHICLE_AS_RESTRICTED(vehs[mvf_escape].id, 1)
						SET_VEHICLE_CHEAT_POWER_INCREASE(vehs[mvf_escape].id, 0.2)
						SET_VEHICLE_HAS_STRONG_AXLES(vehs[mvf_escape].id, TRUE)
						SET_VEHICLE_STRONG(vehs[mvf_escape].id, TRUE)
						SET_VEHICLE_TYRES_CAN_BURST(vehs[mvf_escape].id, FALSE)
						SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_escape].id, I_ESC_VAN_VARIATION)
						SET_ENTITY_PROOFS(vehs[mvf_escape].id, FALSE, TRUE, TRUE, TRUE, FALSE)
						Unload_Asset_Model(sAssetData, mod_esc_veh)
						bFailChecksOnVan = TRUE
				
						mEvents[mef_getaway_driver_arrival].iStage++
					ENDIF
				ENDIF
			BREAK
			CASE 4
				IF CREATE_CREW_MEMBER_IN_VEHICLE(peds[mpf_driver], cmf_driver, vehs[mvf_escape], VS_DRIVER, CREW_OUTFIT_STEALTH, DEFAULT, FALSE, TRUE, FALSE, FALSE)
					START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_escape].id, rec_driver_van, str_car_recs)
					SET_ENTITY_INVINCIBLE(peds[mpf_driver].id, TRUE)
					GIVE_CREW_MEMBER_WEAPON(cmf_driver, FALSE)
					Add_Peds_For_Dialogue()
					mEvents[mef_getaway_driver_arrival].iStage++
				ENDIF
			BREAK
			CASE 5
				IF DOES_ENTITY_EXIST(vehs[mvf_escape].id)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_escape].id)
						
						Unload_Asset_Recording(sAssetData, rec_driver_van, str_car_recs)

						SET_ENTITY_INVINCIBLE(peds[mpf_driver].id, FALSE)
						SET_ENTITY_PROOFS(vehs[mvf_escape].id, FALSE, FALSE, FALSE, FALSE, FALSE)
						
						TRIGGER_MUSIC_EVENT("AH3B_VAN_READY")
						
						TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(peds[mpf_driver].id, vehs[mvf_escape].id)
						
						iTimeDriverArrived = GET_GAME_TIMER()
						bDriverHasArrived = TRUE
						mEvents[mef_getaway_driver_arrival].iStage++
					ENDIF
				ENDIF
			BREAK
			CASE 6
				IF NOT IS_VEHICLE_SEAT_FREE(vehs[mvf_escape].id, VS_FRONT_RIGHT)
				AND GET_PED_IN_VEHICLE_SEAT(vehs[mvf_escape].id, VS_FRONT_RIGHT) = peds[mpf_driver].id
				AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_driver].id, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
					Event_Finish(mef_getaway_driver_arrival)
				ENDIF
			BREAK
		ENDSWITCH
		
	ENDIF
	
ENDPROC

PROC me_swat_fib_arrival()

	IF mEvents[mef_swat_fib_arrival].iStage > 1
	// SWAT ped AI		
	//--------------------------------------------------------------------------------
		INT iRemainingSwat
		
		INT i, j
		
		FOR i = ENUM_TO_INT(mvf_late_fib_1) TO ENUM_TO_INT(mvf_late_fib_2)
			FOR j = 0 TO 2
		
				IF DOES_ENTITY_EXIST(vehs[i].peds[j].id)
				AND NOT IS_PED_INJURED(vehs[i].peds[j].id)
				
					// Combat the player now
					IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					
						IF vehs[i].peds[j].iGenericFlag = 0
							IF (NOT IS_VEHICLE_DRIVEABLE(vehs[i].id) OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[i].id))
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[i].peds[j].id, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(vehs[i].peds[j].id, 1000.0)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(vehs[i].peds[j].id, FALSE)
									vehs[i].peds[j].iGenericFlag++
								ENDIF
							ENDIF
						ENDIF
					
					// Stack up at the FIB entrance
					ELSE
					
						WEAPON_TYPE eWeap, eWeapBest
						eWeap = WEAPONTYPE_INVALID
						GET_CURRENT_PED_WEAPON(vehs[i].peds[j].id, eWeap)
						eWeapBest = GET_BEST_PED_WEAPON(vehs[i].peds[j].id)
						IF eWeap != eWeapBest
							SET_CURRENT_PED_WEAPON(vehs[i].peds[j].id, eWeapBest, TRUE)
						ENDIF
			
						INT iCovIndex
						iCovIndex = -1
						VECTOR vGoToPoint, vAimAtPoint
						SWITCH int_to_enum(MISSION_VEHICLE_FLAGS, i)
							CASE mvf_late_fib_1
								SWITCH j
									CASE 0
										iCovIndex	= 0
										vGoToPoint 	= GET_SCRIPTED_COVER_POINT_COORDS(cov_FIBEntranceCover[iCovIndex])
										vAimAtPoint = <<0,0,0>>
									BREAK
									CASE 1
										iCovIndex	= 1
										vGoToPoint 	= GET_SCRIPTED_COVER_POINT_COORDS(cov_FIBEntranceCover[iCovIndex])
										vAimAtPoint = <<0,0,0>>
									BREAK
									CASE 2
										vGoToPoint = <<96.1398, -748.0010, 44.7554>>
										vAimAtPoint = <<110.7882, -745.1479, 45.7479>>
									BREAK
								ENDSWITCH
							BREAK
							CASE mvf_late_fib_2
								SWITCH j
									CASE 0
										vGoToPoint = <<96.3826, -737.6090, 44.7563>>
										vAimAtPoint = <<106.9883, -746.0746, 45.8189>>
									BREAK
									CASE 1
										iCovIndex	= 2
										vGoToPoint 	= GET_SCRIPTED_COVER_POINT_COORDS(cov_FIBEntranceCover[iCovIndex])
										vAimAtPoint = <<0,0,0>>
									BREAK
									CASE 2
										vGoToPoint = <<98.9794, -743.5940, 44.7559>>
										vAimAtPoint = <<106.7507, -744.8083, 45.8343>>
									BREAK
								ENDSWITCH
							BREAK
						ENDSWITCH
					
						IF (NOT IS_VEHICLE_DRIVEABLE(vehs[i].id) OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[i].id))
						
							IF NOT IS_VECTOR_ZERO(vGoToPoint)
							
								SET_PED_USING_ACTION_MODE(vehs[i].peds[j].id, TRUE)
								
								IF iCovIndex != -1
								
									IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[i].peds[j].id, SCRIPT_TASK_PERFORM_SEQUENCE)
										SAFE_OPEN_SEQUENCE()
											IF IS_PED_IN_ANY_VEHICLE(vehs[i].peds[j].id)
												TASK_LEAVE_ANY_VEHICLE(null, 0, ECF_DONT_CLOSE_DOOR)
											ENDIF
											TASK_SWAP_WEAPON(null, TRUE)
											TASK_FOLLOW_NAV_MESH_TO_COORD(null, vGoToPoint, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, 15.0)
											TASK_SEEK_COVER_TO_COVER_POINT(null, cov_FIBEntranceCover[iCovIndex], <<117.0291, -751.6413, 47.0793>>, -1, FALSE)
										SAFE_PERFORM_SEQUENCE(vehs[i].peds[j].id)
									ENDIF
									
								ELIF NOT IS_VECTOR_ZERO(vAimAtPoint)
								
									IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[i].peds[j].id, SCRIPT_TASK_PERFORM_SEQUENCE)
										SAFE_OPEN_SEQUENCE()
											IF IS_PED_IN_ANY_VEHICLE(vehs[i].peds[j].id)
												TASK_LEAVE_ANY_VEHICLE(null, 0, ECF_DONT_CLOSE_DOOR)
											ENDIF
											TASK_SWAP_WEAPON(null, TRUE)
											TASK_FOLLOW_NAV_MESH_TO_COORD(null, vGoToPoint, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP)
											TASK_AIM_GUN_AT_COORD(null, vAimAtPoint, -1, FALSE, TRUE)
										SAFE_PERFORM_SEQUENCE(vehs[i].peds[j].id)	
									ENDIF
									
								ENDIF
							
							ENDIF
						ENDIF
	
					
					ENDIF
					
					iRemainingSwat++
				ENDIF
				
			ENDFOR		
		ENDFOR

		
	// Wanted level at the bottom of the building
	//----------------------------------------------------------------------------------------------------
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
		
			IF NOT bBlockSpawningCops
				BOOL bAwareOfPlayer
				
				FOR i = ENUM_TO_INT(mvf_late_fib_1) TO ENUM_TO_INT(mvf_late_fib_2)
					FOR j = 0 TO 2
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehs[i].peds[j].id, MIKE_PED_ID())
						OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehs[i].peds[j].id, FRANK_PED_ID())
						OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehs[i].peds[j].id, peds[mpf_goon].id)
						OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehs[i].peds[j].id, peds[mpf_driver].id)
						OR HAS_PED_RECEIVED_EVENT(vehs[i].peds[j].id, EVENT_SHOT_FIRED)
						OR HAS_PED_RECEIVED_EVENT(vehs[i].peds[j].id, EVENT_SHOT_FIRED_BULLET_IMPACT)
						OR HAS_PED_RECEIVED_EVENT(vehs[i].peds[j].id, EVENT_SHOT_FIRED_WHIZZED_BY)
							bAwareOfPlayer = TRUE
						ENDIF
					ENDFOR
				ENDFOR

				// can the swat guys see the player
				FOR i = ENUM_TO_INT(mvf_late_fib_1) TO ENUM_TO_INT(mvf_late_fib_2)
					FOR j = 0 TO 2
						IF NOT bAwareOfPlayer
							IF DOES_ENTITY_EXIST(vehs[i].peds[j].id)
							AND NOT IS_PED_INJURED(vehs[i].peds[j].id)
								IF CAN_PED_SEE_HATED_PED(vehs[i].peds[j].id, MIKE_PED_ID())
								OR CAN_PED_SEE_HATED_PED(vehs[i].peds[j].id, FRANK_PED_ID())
								OR CAN_PED_SEE_HATED_PED(vehs[i].peds[j].id, peds[mpf_goon].id)
								OR (DOES_ENTITY_EXIST(peds[mpf_driver].id) AND NOT IS_PED_INJURED(peds[mpf_driver].id) AND CAN_PED_SEE_HATED_PED(vehs[i].peds[j].id, peds[mpf_driver].id))
									
									// Can see that they are not an ambulance crew
									IF GET_ENTITY_MODEL(vehs[mvf_escape].id) != AMBULANCE
									OR GET_DISTANCE_BETWEEN_ENTITIES(vehs[i].peds[j].id, vehs[mvf_escape].id) < 10.0
									OR NOT IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehs[mvf_escape].id)
									OR NOT IS_PED_SITTING_IN_VEHICLE(BUDDY_PED_ID(), vehs[mvf_escape].id)
									OR NOT IS_PED_SITTING_IN_VEHICLE(peds[mpf_goon].id, vehs[mvf_escape].id)
									OR NOT IS_PED_SITTING_IN_VEHICLE(peds[mpf_driver].id, vehs[mvf_escape].id)
										bAwareOfPlayer = TRUE
									ENDIF
									bSwatSeenAmbulance = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDFOR
				
				IF bAwareOfPlayer
					
					CPRINTLN(DEBUG_MIKE, "********Agency3B: set blocked wanted level")
					
					// Set real wanted level but block cop spawning til we are ready for code cops to come in
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(4)
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 4)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					//SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), FALSE)
					SET_CREATE_RANDOM_COPS(FALSE)
					ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
					ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
					ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, FALSE) 
					//ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
					ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, FALSE)
					ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
					ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, FALSE)
					ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
					SET_PED_AUTO_BLIPS_ACTIVE(vehs[mvf_late_fib_1].peds[0], FALSE)
					SET_PED_AUTO_BLIPS_ACTIVE(vehs[mvf_late_fib_1].peds[1], FALSE)
					SET_PED_AUTO_BLIPS_ACTIVE(vehs[mvf_late_fib_1].peds[2], FALSE)
					CLEANUP_AI_PED_BLIP(vehs[mvf_late_fib_1].peds[0].ai_blip)
					CLEANUP_AI_PED_BLIP(vehs[mvf_late_fib_1].peds[1].ai_blip)
					CLEANUP_AI_PED_BLIP(vehs[mvf_late_fib_1].peds[2].ai_blip)
					
					SET_PED_AUTO_BLIPS_ACTIVE(vehs[mvf_late_fib_2].peds[0], FALSE)
					SET_PED_AUTO_BLIPS_ACTIVE(vehs[mvf_late_fib_2].peds[1], FALSE)
					SET_PED_AUTO_BLIPS_ACTIVE(vehs[mvf_late_fib_2].peds[2], FALSE)
					CLEANUP_AI_PED_BLIP(vehs[mvf_late_fib_2].peds[0].ai_blip)
					CLEANUP_AI_PED_BLIP(vehs[mvf_late_fib_2].peds[1].ai_blip)
					CLEANUP_AI_PED_BLIP(vehs[mvf_late_fib_2].peds[2].ai_blip)

					bBlockSpawningCops = TRUE
					
				ENDIF
			ENDIF
			
		ELSE

			IF bBlockSpawningCops
				IF (IS_PED_IN_VEHICLE(MIKE_PED_ID(), vehs[mvf_escape].id, TRUE)
				AND IS_PED_IN_VEHICLE(FRANK_PED_ID(), vehs[mvf_escape].id, TRUE)
				AND IS_PED_IN_VEHICLE(peds[mpf_goon].id, vehs[mvf_escape].id, TRUE)
				AND IS_PED_IN_VEHICLE(peds[mpf_driver].id, vehs[mvf_escape].id, TRUE))
				OR (bDriverHasArrived AND (GET_GAME_TIMER() - iTimeDriverArrived > 15000)
					AND (iRemainingSwat <= 2 OR (GET_GAME_TIMER() - iTimeDriverArrived > 30000)))

					CPRINTLN(DEBUG_MIKE, "********Agency3B: unblocked wanted level")
					
					// Reenable cop spawning now, ALL HELL BREAKS LOOSE!
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
					SET_CREATE_RANDOM_COPS(TRUE)
					ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
					ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
					ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, TRUE) 
					ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
					ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, TRUE)
					ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
					ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, TRUE)
					ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
				
					bBlockSpawningCops = FALSE
					Event_Trigger(mef_manage_wanted_level)
					
				ENDIF
			ELSE
				SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
				REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
			ENDIF
			
		ENDIF
	ENDIF
	
	// Main event flow
	//--------------------------------------------------------------------------------------------------------
	SWITCH mEvents[mef_swat_fib_arrival].iStage
		CASE 1
			Load_Asset_Model(sAssetData, FBI2)
			Load_Asset_Model(sAssetData, mod_swat)
			
			Load_Asset_Recording(sAssetData, rec_fib_late_1, str_car_recs)
			Load_Asset_Recording(sAssetData, rec_fib_late_2, str_car_recs)
		
			IF HAS_MODEL_LOADED(FBI2)
			AND HAS_MODEL_LOADED(mod_swat)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_fib_late_1, str_car_recs)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_fib_late_2, str_car_recs)
			
				DISTANT_COP_CAR_SIRENS(TRUE)
			
				vehs[mvf_late_fib_1].id = CREATE_VEHICLE(FBI2, <<150.8472, -585.8458, 42.9832>>, 69.1685)
				SET_VEHICLE_SIREN(vehs[mvf_late_fib_1].id, TRUE)
				START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_late_fib_1].id, rec_fib_late_1, str_car_recs)
				
				CREATE_MISSION_PED_IN_VEHICLE(vehs[mvf_late_fib_1].peds[0], mod_swat, vehs[mvf_late_fib_1], VS_DRIVER, 			"SWATCar1_DVR", 	REL_ENEMY, WEAPONTYPE_PUMPSHOTGUN, 	WEAPONCOMPONENT_AT_AR_FLSH, 5)
				CREATE_MISSION_PED_IN_VEHICLE(vehs[mvf_late_fib_1].peds[1], mod_swat, vehs[mvf_late_fib_1], VS_EXTRA_LEFT_1,	"SWATCar1_LFT", 	REL_ENEMY, WEAPONTYPE_SMG, 			WEAPONCOMPONENT_AT_AR_FLSH, 10)
				CREATE_MISSION_PED_IN_VEHICLE(vehs[mvf_late_fib_1].peds[2], mod_swat, vehs[mvf_late_fib_1], VS_EXTRA_RIGHT_1, 	"SWATCar1_RGT", 	REL_ENEMY, WEAPONTYPE_SMG, 			WEAPONCOMPONENT_AT_AR_FLSH, 10)
				SET_CURRENT_PED_VEHICLE_WEAPON(vehs[mvf_late_fib_1].peds[1].id, WEAPONTYPE_SMG)
				SET_CURRENT_PED_VEHICLE_WEAPON(vehs[mvf_late_fib_1].peds[2].id, WEAPONTYPE_SMG)
				SET_PED_COMBAT_ATTRIBUTES(vehs[mvf_late_fib_1].peds[0].id, CA_USE_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(vehs[mvf_late_fib_1].peds[1].id, CA_USE_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(vehs[mvf_late_fib_1].peds[2].id, CA_USE_VEHICLE, FALSE)
				SET_PED_TARGET_LOSS_RESPONSE(vehs[mvf_late_fib_1].peds[0].id, TLR_NEVER_LOSE_TARGET)
				SET_PED_TARGET_LOSS_RESPONSE(vehs[mvf_late_fib_1].peds[1].id, TLR_NEVER_LOSE_TARGET)
				SET_PED_TARGET_LOSS_RESPONSE(vehs[mvf_late_fib_1].peds[2].id, TLR_NEVER_LOSE_TARGET)
				
				vehs[mvf_late_fib_2].id = CREATE_VEHICLE(FBI2, <<-163.4651, -700.3435, 36.1855>>, 254.9485)
				SET_VEHICLE_SIREN(vehs[mvf_late_fib_2].id, TRUE)
				START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_late_fib_2].id, rec_fib_late_2, str_car_recs)
				
				CREATE_MISSION_PED_IN_VEHICLE(vehs[mvf_late_fib_2].peds[0], mod_swat, vehs[mvf_late_fib_2], VS_DRIVER, 			"SWATCar2_DVR", 	REL_ENEMY, WEAPONTYPE_PUMPSHOTGUN, 		WEAPONCOMPONENT_AT_AR_FLSH, 5)
				CREATE_MISSION_PED_IN_VEHICLE(vehs[mvf_late_fib_2].peds[1], mod_swat, vehs[mvf_late_fib_2], VS_EXTRA_LEFT_1,	"SWATCar2_LFT", 	REL_ENEMY, WEAPONTYPE_SMG, 				WEAPONCOMPONENT_AT_AR_FLSH, 10)
				CREATE_MISSION_PED_IN_VEHICLE(vehs[mvf_late_fib_2].peds[2], mod_swat, vehs[mvf_late_fib_2], VS_EXTRA_RIGHT_1, 	"SWATCar2_RGT", 	REL_ENEMY, WEAPONTYPE_SMG, 				WEAPONCOMPONENT_AT_AR_FLSH, 10)
				SET_CURRENT_PED_VEHICLE_WEAPON(vehs[mvf_late_fib_2].peds[1].id, WEAPONTYPE_SMG)
				SET_CURRENT_PED_VEHICLE_WEAPON(vehs[mvf_late_fib_2].peds[2].id, WEAPONTYPE_SMG)
				SET_PED_COMBAT_ATTRIBUTES(vehs[mvf_late_fib_2].peds[0].id, CA_USE_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(vehs[mvf_late_fib_2].peds[1].id, CA_USE_VEHICLE, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(vehs[mvf_late_fib_2].peds[2].id, CA_USE_VEHICLE, FALSE)
				SET_PED_TARGET_LOSS_RESPONSE(vehs[mvf_late_fib_2].peds[0].id, TLR_NEVER_LOSE_TARGET)
				SET_PED_TARGET_LOSS_RESPONSE(vehs[mvf_late_fib_2].peds[1].id, TLR_NEVER_LOSE_TARGET)
				SET_PED_TARGET_LOSS_RESPONSE(vehs[mvf_late_fib_2].peds[2].id, TLR_NEVER_LOSE_TARGET)
				
				IF cov_FIBEntranceCover[0] = NULL
					cov_FIBEntranceCover[0] = ADD_COVER_POINT(<<99.9129, -737.7625, 44.7573>>, 	249.7556, 	COVUSE_WALLTOLEFT, 	COVHEIGHT_HIGH, 	COVARC_120)
				ENDIF
				IF cov_FIBEntranceCover[1] = NULL
					cov_FIBEntranceCover[1] = ADD_COVER_POINT(<<98.1113, -749.4194, 44.7555>>, 	271.8575, 	COVUSE_WALLTORIGHT, COVHEIGHT_HIGH, 	COVARC_120)
				ENDIF
				IF cov_FIBEntranceCover[2] = NULL
					cov_FIBEntranceCover[2] = ADD_COVER_POINT(<<105.5926, -739.6732, 44.7569>>, 247.3052, 	COVUSE_WALLTOLEFT, 	COVHEIGHT_HIGH, 	COVARC_120)
				ENDIF
				
				Unload_Asset_Model(sAssetData, FBI2)
				Unload_Asset_Model(sAssetData, mod_swat)
				
				bBlockSpawningCops = FALSE
								
				mEvents[mef_swat_fib_arrival].iTimeStamp = GET_GAME_TIMER()
				mEvents[mef_swat_fib_arrival].iStage++
					
			ENDIF
		BREAK
		CASE 2
			IF (NOT IS_VEHICLE_DRIVEABLE(vehs[mvf_late_fib_1].id) OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_late_fib_1].id))
			AND (NOT IS_VEHICLE_DRIVEABLE(vehs[mvf_late_fib_2].id) OR NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_late_fib_2].id))
				bSwatHaveArrived = TRUE
				Unload_Asset_Recording(sAssetData, rec_fib_late_1, str_car_recs)
				Unload_Asset_Recording(sAssetData, rec_fib_late_2, str_car_recs)
				mEvents[mef_swat_fib_arrival].iStage++
			ENDIF
		BREAK
		CASE 3
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vStreetPickupLocation) > 500
			OR (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0 AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vStreetPickupLocation) > 300)
				IF DOES_ENTITY_EXIST(vehs[mvf_late_fib_1].peds[0].id)
					SET_PED_AS_NO_LONGER_NEEDED(vehs[mvf_late_fib_1].peds[0].id)
				ENDIF
				IF DOES_ENTITY_EXIST(vehs[mvf_late_fib_1].peds[1].id)
					SET_PED_AS_NO_LONGER_NEEDED(vehs[mvf_late_fib_1].peds[1].id)
				ENDIF
				IF DOES_ENTITY_EXIST(vehs[mvf_late_fib_1].peds[2].id)
					SET_PED_AS_NO_LONGER_NEEDED(vehs[mvf_late_fib_1].peds[2].id)
				ENDIF
				IF DOES_ENTITY_EXIST(vehs[mvf_late_fib_2].peds[0].id)
					SET_PED_AS_NO_LONGER_NEEDED(vehs[mvf_late_fib_2].peds[0].id)
				ENDIF
				IF DOES_ENTITY_EXIST(vehs[mvf_late_fib_2].peds[1].id)
					SET_PED_AS_NO_LONGER_NEEDED(vehs[mvf_late_fib_2].peds[1].id)
				ENDIF
				IF DOES_ENTITY_EXIST(vehs[mvf_late_fib_2].peds[2].id)
					SET_PED_AS_NO_LONGER_NEEDED(vehs[mvf_late_fib_2].peds[2].id)
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehs[mvf_late_fib_1].id)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_late_fib_1].id)
				ENDIF
				IF DOES_ENTITY_EXIST(vehs[mvf_late_fib_2].id)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_late_fib_2].id)
				ENDIF
				
				Event_Finish(mef_swat_fib_arrival)
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC me_emergency_services()

	SWITCH mEvents[mef_emergency_services].iStage
		CASE 1
			CLEAR_AREA_OF_VEHICLES(<<267.7761, -561.6590, 49.8593>>, 5000)
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			mEvents[mef_emergency_services].iStage++
		BREAK
		CASE 2
			IF NOT IS_CUTSCENE_PLAYING()

				Load_Asset_Model(sAssetData, POLICE3)
				Load_Asset_Model(sAssetData, FIRETRUK)
				Load_Asset_Model(sAssetData, AMBULANCE)
				
				IF GET_DRIVER_SKILL_LEVEL() = CMSK_GOOD
					Load_Asset_Recording(sAssetData, rec_driver_ambulance_arrive, str_car_recs)
					SETUP_EMERGENCY_SERVICE_STRUCT(s_EmergencyServicesSpawner[12], 	mvf_escape, 	AMBULANCE, 		rec_driver_ambulance_arrive, 		13500)
				ELSE
					s_EmergencyServicesSpawner[12].bSpawned = TRUE
				ENDIF
				
				Load_Asset_Recording(sAssetData, rec_police_arrive_1, str_car_recs)
				Load_Asset_Recording(sAssetData, rec_police_arrive_2, str_car_recs)
				Load_Asset_Recording(sAssetData, rec_police_arrive_3, str_car_recs)
				Load_Asset_Recording(sAssetData, rec_police_arrive_4, str_car_recs)
				Load_Asset_Recording(sAssetData, rec_police_arrive_5, str_car_recs)
				Load_Asset_Recording(sAssetData, rec_police_arrive_6, str_car_recs)
				Load_Asset_Recording(sAssetData, rec_police_arrive_7, str_car_recs)
				Load_Asset_Recording(sAssetData, rec_police_arrive_8, str_car_recs)
				Load_Asset_Recording(sAssetData, rec_fireturk_arrive_1, str_car_recs)
				Load_Asset_Recording(sAssetData, rec_fireturk_arrive_2, str_car_recs)
				Load_Asset_Recording(sAssetData, rec_ambulance_arrive_1, str_car_recs)
				Load_Asset_Recording(sAssetData, rec_ambulance_arrive_2, str_car_recs)
				
				SETUP_EMERGENCY_SERVICE_STRUCT(s_EmergencyServicesSpawner[0], 	mvf_police_car_1,	POLICE3, 		rec_police_arrive_1, 		0)
				SETUP_EMERGENCY_SERVICE_STRUCT(s_EmergencyServicesSpawner[1], 	mvf_police_car_2, 	POLICE3, 		rec_police_arrive_2, 		0)
				SETUP_EMERGENCY_SERVICE_STRUCT(s_EmergencyServicesSpawner[2], 	mvf_police_car_3, 	POLICE3, 		rec_police_arrive_3, 		0)
				SETUP_EMERGENCY_SERVICE_STRUCT(s_EmergencyServicesSpawner[3], 	mvf_police_car_4, 	POLICE3, 		rec_police_arrive_4, 		0)
				SETUP_EMERGENCY_SERVICE_STRUCT(s_EmergencyServicesSpawner[4], 	mvf_police_car_5, 	POLICE3, 		rec_police_arrive_5, 		0)
				SETUP_EMERGENCY_SERVICE_STRUCT(s_EmergencyServicesSpawner[5], 	mvf_police_car_6, 	POLICE3, 		rec_police_arrive_6, 		0)
				SETUP_EMERGENCY_SERVICE_STRUCT(s_EmergencyServicesSpawner[6], 	mvf_police_car_7, 	POLICE3, 		rec_police_arrive_7, 		4000)
				SETUP_EMERGENCY_SERVICE_STRUCT(s_EmergencyServicesSpawner[7], 	mvf_police_car_8, 	POLICE3, 		rec_police_arrive_8, 		3000)
				SETUP_EMERGENCY_SERVICE_STRUCT(s_EmergencyServicesSpawner[8], 	mvf_firetruck_1, 	FIRETRUK, 		rec_fireturk_arrive_1, 		0)
				SETUP_EMERGENCY_SERVICE_STRUCT(s_EmergencyServicesSpawner[9], 	mvf_firetruck_2, 	FIRETRUK, 		rec_fireturk_arrive_2, 		2000)
				SETUP_EMERGENCY_SERVICE_STRUCT(s_EmergencyServicesSpawner[10], 	mvf_ambulance_1, 	AMBULANCE, 		rec_ambulance_arrive_1, 	5000, 4000)
				SETUP_EMERGENCY_SERVICE_STRUCT(s_EmergencyServicesSpawner[11], 	mvf_ambulance_2, 	AMBULANCE, 		rec_ambulance_arrive_2, 	5000, 3000)
				
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				
				LOAD_STREAM("Distant_Sirens_Rappel", "FBI_HEIST_FINALE_CHOPPER")
			
				IF HAS_MODEL_LOADED(POLICE3)
				AND HAS_MODEL_LOADED(FIRETRUK)
				AND HAS_MODEL_LOADED(AMBULANCE)
				AND (GET_DRIVER_SKILL_LEVEL() != CMSK_GOOD OR HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_driver_ambulance_arrive, str_car_recs))
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_police_arrive_1, str_car_recs)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_police_arrive_2, str_car_recs)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_police_arrive_3, str_car_recs)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_police_arrive_4, str_car_recs)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_police_arrive_5, str_car_recs)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_police_arrive_6, str_car_recs)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_police_arrive_7, str_car_recs)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_police_arrive_8, str_car_recs)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_fireturk_arrive_1, str_car_recs)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_fireturk_arrive_2, str_car_recs)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_ambulance_arrive_1, str_car_recs)
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_ambulance_arrive_2, str_car_recs)
				AND LOAD_STREAM("Distant_Sirens_Rappel", "FBI_HEIST_FINALE_CHOPPER")
				
					PLAY_STREAM_FROM_POSITION(<<224.5, -627.0, 240.3>>)

					i911SpawnTimer 		= GET_GAME_TIMER()
					mEvents[mef_emergency_services].iStage++
				
				ENDIF
			ENDIF
		BREAK
		CASE 3
		
			BOOL bAllSpawnedAndDeleted
			bAllSpawnedAndDeleted = TRUE
			INT i
			REPEAT COUNT_OF(s_EmergencyServicesSpawner) i

				IF NOT DOES_ENTITY_EXIST(vehs[s_EmergencyServicesSpawner[i].eVeh].id)
				AND NOT s_EmergencyServicesSpawner[i].bSpawned
				
					IF GET_GAME_TIMER() - i911SpawnTimer > s_EmergencyServicesSpawner[i].iStartTime
					
						VECTOR vSpawnLocationCoord
						VECTOR vSpawnLocationRot
						vSpawnLocationCoord 	= GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(s_EmergencyServicesSpawner[i].iRecNumber, 0.0, str_car_recs)
						vSpawnLocationRot		= GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(s_EmergencyServicesSpawner[i].iRecNumber, 0.0, str_car_recs)

						vehs[s_EmergencyServicesSpawner[i].eVeh].id	= CREATE_VEHICLE(s_EmergencyServicesSpawner[i].eModel, vSpawnLocationCoord, vSpawnLocationRot.z)
						SET_VEHICLE_LIGHTS(vehs[s_EmergencyServicesSpawner[i].eVeh].id, FORCE_VEHICLE_LIGHTS_ON)
						SET_VEHICLE_SIREN(vehs[s_EmergencyServicesSpawner[i].eVeh].id, TRUE)
						SET_VEHICLE_HAS_MUTED_SIRENS(vehs[s_EmergencyServicesSpawner[i].eVeh].id, TRUE)
						SET_ENTITY_LOD_DIST(vehs[s_EmergencyServicesSpawner[i].eVeh].id, 5000)
						SET_VEHICLE_LOD_MULTIPLIER(vehs[s_EmergencyServicesSpawner[i].eVeh].id, 10.0)
						
						START_PLAYBACK_RECORDED_VEHICLE(vehs[s_EmergencyServicesSpawner[i].eVeh].id, s_EmergencyServicesSpawner[i].iRecNumber, str_car_recs)
						s_EmergencyServicesSpawner[i].bSpawned = TRUE
						
					ENDIF
				
				ELSE
					IF DOES_ENTITY_EXIST(vehs[s_EmergencyServicesSpawner[i].eVeh].id)
						//IF IS_ENTITY_IN_ANGLED_AREA(vehs[s_EmergencyServicesSpawner[i].eVeh].id, <<181.553619,-600.232971,41.867855>>, <<148.851181,-587.882446,51.923786>>, 31.812500)
						IF IS_ENTITY_AT_COORD(vehs[s_EmergencyServicesSpawner[i].eVeh].id, <<148.023041,-588.078857,48.812019>>,<<8.000000,15.375000,6.812500>>)
							Unload_Asset_Recording(sAssetData, s_EmergencyServicesSpawner[i].iRecNumber, str_car_recs)
							DELETE_VEHICLE(vehs[s_EmergencyServicesSpawner[i].eVeh].id)
						ELSE
						
							IF s_EmergencyServicesSpawner[i].eVeh = mvf_ambulance_1
							OR s_EmergencyServicesSpawner[i].eVeh = mvf_ambulance_2
								SWITCH s_EmergencyServicesSpawner[i].iStage
									CASE 0
										IF IS_ENTITY_IN_ANGLED_AREA(vehs[s_EmergencyServicesSpawner[i].eVeh].id, <<292.140472,-566.492554,41.510773>>, <<273.198792,-613.017334,46.311081>>, 9.875000)
										AND IS_VEHICLE_STOPPED(vehs[s_EmergencyServicesSpawner[i].eVeh].id)
											PAUSE_PLAYBACK_RECORDED_VEHICLE(vehs[s_EmergencyServicesSpawner[i].eVeh].id)
											s_EmergencyServicesSpawner[i].iPauseTimer = GET_GAME_TIMER() + s_EmergencyServicesSpawner[i].iPauseLength
											s_EmergencyServicesSpawner[i].iStage++
										ENDIF
									BREAK
									CASE 1
										IF GET_GAME_TIMER() > s_EmergencyServicesSpawner[i].iPauseTimer
											UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehs[s_EmergencyServicesSpawner[i].eVeh].id)
											s_EmergencyServicesSpawner[i].iStage++
										ENDIF
									BREAK
								ENDSWITCH
							ENDIF
						
							#IF IS_DEBUG_BUILD
								IF g_bTriggerDebugOn
									TEXT_LABEL_15 str_DebugString
									str_DebugString = ""
									str_DebugString += enum_to_int(s_EmergencyServicesSpawner[i].eVeh - mvf_police_car_1)
									DRAW_DEBUG_TEXT(str_DebugString, GET_ENTITY_COORDS(vehs[s_EmergencyServicesSpawner[i].eVeh].id), 0, 255, 0, 255)
								ENDIF
							#ENDIF
						ENDIF
					ENDIF
				
				ENDIF
				
				IF NOT s_EmergencyServicesSpawner[i].bSpawned
				OR DOES_ENTITY_EXIST(vehs[s_EmergencyServicesSpawner[i].eVeh].id)
					bAllSpawnedAndDeleted = FALSE
				ENDIF
			
			ENDREPEAT

			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			
			IF bAllSpawnedAndDeleted
				mEvents[mef_emergency_services].iStage++
			ENDIF

		BREAK
		CASE 4
			SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		
			Load_Asset_Recording(sAssetData, rec_police_car_pull_up_1, str_car_recs)
			Load_Asset_Recording(sAssetData, rec_police_car_pull_up_2, str_car_recs)
			Load_Asset_Recording(sAssetData, rec_firetruck_pullup_1, str_car_recs)
			Load_Asset_Recording(sAssetData, rec_firetruck_pullup_2, str_car_recs)
		
			IF mission_substage >= 6
			AND HAS_MODEL_LOADED(FIRETRUK)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_police_car_pull_up_1, str_car_recs)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_police_car_pull_up_2, str_car_recs)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_firetruck_pullup_1, str_car_recs)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec_firetruck_pullup_2, str_car_recs)
			
				// Cop cars
				vehs[mvf_police_car_1].id = CREATE_VEHICLE(POLICE3, <<200.0189, -735.5240, 32.9126>>, 340.4122)
				vehs[mvf_police_car_2].id = CREATE_VEHICLE(POLICE3, <<205.0903, -740.0325, 32.7917>>, 335.0850)
				vehs[mvf_police_car_1].peds[0].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_police_car_1].id, PEDTYPE_MISSION, S_M_Y_COP_01, VS_DRIVER)
				vehs[mvf_police_car_2].peds[0].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_police_car_2].id, PEDTYPE_MISSION, S_M_Y_COP_01, VS_DRIVER)
				GIVE_WEAPON_TO_PED(vehs[mvf_police_car_1].peds[0].id, WEAPONTYPE_PISTOL, 120, TRUE, TRUE)
				GIVE_WEAPON_TO_PED(vehs[mvf_police_car_2].peds[0].id, WEAPONTYPE_PISTOL, 120, TRUE, TRUE)
				GIVE_WEAPON_COMPONENT_TO_PED(vehs[mvf_police_car_1].peds[0].id, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_FLSH)
				GIVE_WEAPON_COMPONENT_TO_PED(vehs[mvf_police_car_2].peds[0].id, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_FLSH)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(vehs[mvf_police_car_1].peds[0].id, TRUE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(vehs[mvf_police_car_2].peds[0].id, TRUE)
				DISABLE_PED_PAIN_AUDIO(vehs[mvf_police_car_1].peds[0].id, TRUE)
				DISABLE_PED_PAIN_AUDIO(vehs[mvf_police_car_2].peds[0].id, TRUE)
				
				SET_VEHICLE_LIGHTS(vehs[mvf_police_car_1].id, FORCE_VEHICLE_LIGHTS_ON)
				SET_VEHICLE_LIGHTS(vehs[mvf_police_car_2].id, FORCE_VEHICLE_LIGHTS_ON)
				SET_VEHICLE_SIREN(vehs[mvf_police_car_1].id, TRUE)
				SET_VEHICLE_SIREN(vehs[mvf_police_car_2].id, TRUE)
				START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_police_car_1].id, rec_police_car_pull_up_1, str_car_recs)
				START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_police_car_2].id, rec_police_car_pull_up_2, str_car_recs)
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_police_car_1].id, 9035)
				//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_police_car_2].id, 11502)
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_police_car_1].id)
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_police_car_2].id)
		
				// Fire truck 1
				vehs[mvf_firetruck_1].id = CREATE_VEHICLE(FIRETRUK, <<-42.9874, -1008.3702, 28.0347>>, 343.6827)
				vehs[mvf_firetruck_1].peds[0].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_firetruck_1].id, PEDTYPE_MISSION, S_M_Y_FIREMAN_01, VS_DRIVER)
				vehs[mvf_firetruck_1].peds[1].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_firetruck_1].id, PEDTYPE_MISSION, S_M_Y_FIREMAN_01, VS_FRONT_RIGHT)
				vehs[mvf_firetruck_1].peds[2].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_firetruck_1].id, PEDTYPE_MISSION, S_M_Y_FIREMAN_01, VS_BACK_LEFT)
				SET_VEHICLE_LIGHTS(vehs[mvf_firetruck_1].id, FORCE_VEHICLE_LIGHTS_ON)
				SET_VEHICLE_SIREN(vehs[mvf_firetruck_1].id, TRUE)
				START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_firetruck_1].id, rec_firetruck_pullup_1, str_car_recs)
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_firetruck_1].id, 11689)
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_firetruck_1].id)
				DISABLE_PED_PAIN_AUDIO(vehs[mvf_firetruck_1].peds[0].id, TRUE)
				DISABLE_PED_PAIN_AUDIO(vehs[mvf_firetruck_1].peds[1].id, TRUE)
				DISABLE_PED_PAIN_AUDIO(vehs[mvf_firetruck_1].peds[2].id, TRUE)
				
				// Fire truck 2
				vehs[mvf_firetruck_2].id = CREATE_VEHICLE(FIRETRUK, <<121.2887, -564.5789, 42.4048>>, 162.8673)
				vehs[mvf_firetruck_2].peds[0].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_firetruck_2].id, PEDTYPE_MISSION, S_M_Y_FIREMAN_01, VS_DRIVER)
				vehs[mvf_firetruck_2].peds[1].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_firetruck_2].id, PEDTYPE_MISSION, S_M_Y_FIREMAN_01, VS_FRONT_RIGHT)
				SET_VEHICLE_LIGHTS(vehs[mvf_firetruck_2].id, FORCE_VEHICLE_LIGHTS_ON)
				SET_VEHICLE_SIREN(vehs[mvf_firetruck_2].id, TRUE)
				START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_firetruck_2].id, rec_firetruck_pullup_2, str_car_recs)
				DISABLE_PED_PAIN_AUDIO(vehs[mvf_firetruck_2].peds[0].id, TRUE)
				DISABLE_PED_PAIN_AUDIO(vehs[mvf_firetruck_2].peds[1].id, TRUE)
				
				IF GET_DRIVER_SKILL_LEVEL() != CMSK_GOOD
					Unload_Asset_Model(sAssetData, AMBULANCE)
				ENDIF
				Unload_Asset_Model(sAssetData, POLICE3)
				Unload_Asset_Model(sAssetData, FIRETRUK)
		
				mEvents[mef_emergency_services].iStage++
			ENDIF
		BREAK
		CASE 5
		
	// Police 
			IF DOES_ENTITY_EXIST(vehs[mvf_police_car_1].id)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_police_car_1].id)
					Unload_Asset_Recording(sAssetData, rec_police_car_pull_up_1, str_car_recs)
					
					IF DOES_ENTITY_EXIST(vehs[mvf_police_car_1].peds[0].id)
					AND NOT IS_PED_INJURED(vehs[mvf_police_car_1].peds[0].id)
						SWITCH vehs[mvf_police_car_1].peds[0].iGenericFlag
							CASE 0
								
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_police_car_1].peds[0].id, SCRIPT_TASK_PERFORM_SEQUENCE)
								
									SET_PED_USING_ACTION_MODE(vehs[mvf_police_car_1].peds[0].id, TRUE, -1, "DEFAULT_ACTION")
									
									SAFE_OPEN_SEQUENCE()
										IF IS_PED_IN_ANY_VEHICLE(vehs[mvf_police_car_1].peds[0].id)
											TASK_LEAVE_ANY_VEHICLE(null)
										ENDIF
										TASK_SWAP_WEAPON(null, TRUE)
										TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<153.3248, -673.2049, 41.0273>>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 257.5370)
										TASK_AIM_GUN_AT_COORD(null, <<166.6879, -674.2803, 42.9926>>, -1, FALSE)
									SAFE_PERFORM_SEQUENCE(vehs[mvf_police_car_1].peds[0].id)
								
								ENDIF
								
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehs[mvf_police_car_2].id)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_police_car_2].id)
					Unload_Asset_Recording(sAssetData, rec_police_car_pull_up_2, str_car_recs)
					
					IF DOES_ENTITY_EXIST(vehs[mvf_police_car_2].peds[0].id)
					AND NOT IS_PED_INJURED(vehs[mvf_police_car_2].peds[0].id)
						SWITCH vehs[mvf_police_car_2].peds[0].iGenericFlag
							CASE 0
								
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_police_car_2].peds[0].id, SCRIPT_TASK_PERFORM_SEQUENCE)
								
									SET_PED_USING_ACTION_MODE(vehs[mvf_police_car_2].peds[0].id, TRUE, -1, "DEFAULT_ACTION")
									
									SAFE_OPEN_SEQUENCE()
										IF IS_PED_IN_ANY_VEHICLE(vehs[mvf_police_car_2].peds[0].id)
											TASK_LEAVE_ANY_VEHICLE(null)
										ENDIF
										TASK_SWAP_WEAPON(null, TRUE)
										TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<157.0855, -683.5394, 41.0273>>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 315.1326)
										TASK_AIM_GUN_AT_COORD(null, <<166.6879, -674.2803, 42.9926>>, -1, FALSE)
									SAFE_PERFORM_SEQUENCE(vehs[mvf_police_car_2].peds[0].id)
								
								ENDIF
								
							BREAK
						ENDSWITCH
					ENDIF
					
				ENDIF
			ENDIF

		// Fire trucks
		
			IF DOES_ENTITY_EXIST(vehs[mvf_firetruck_1].id)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_firetruck_1].id)
					Unload_Asset_Recording(sAssetData, rec_firetruck_pullup_1, str_car_recs)
					
					IF DOES_ENTITY_EXIST(vehs[mvf_firetruck_1].peds[0].id)
					AND NOT IS_PED_INJURED(vehs[mvf_firetruck_1].peds[0].id)
						SWITCH vehs[mvf_firetruck_1].peds[0].iGenericFlag
							CASE 0
								
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_firetruck_1].peds[0].id, SCRIPT_TASK_PERFORM_SEQUENCE)

									SAFE_OPEN_SEQUENCE()
										IF IS_PED_IN_ANY_VEHICLE(vehs[mvf_firetruck_1].peds[0].id)
											TASK_LEAVE_ANY_VEHICLE(null)
										ENDIF
										TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<167.7315, -678.7234, 42.1430>>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 355.4801)
										TASK_STAND_STILL(null, -1)
									SAFE_PERFORM_SEQUENCE(vehs[mvf_firetruck_1].peds[0].id)
								
								ENDIF
								
							BREAK
						ENDSWITCH
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehs[mvf_firetruck_1].peds[1].id)
					AND NOT IS_PED_INJURED(vehs[mvf_firetruck_1].peds[1].id)
						SWITCH vehs[mvf_firetruck_1].peds[1].iGenericFlag
							CASE 0
								
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_firetruck_1].peds[1].id, SCRIPT_TASK_PERFORM_SEQUENCE)
									
									SAFE_OPEN_SEQUENCE()
										IF IS_PED_IN_ANY_VEHICLE(vehs[mvf_firetruck_1].peds[1].id)
											TASK_LEAVE_ANY_VEHICLE(null)
										ENDIF
										TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<164.1439, -674.0157, 42.2516>>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 269.7370)
										TASK_STAND_STILL(null, -1)
									SAFE_PERFORM_SEQUENCE(vehs[mvf_firetruck_1].peds[1].id)
								
								ENDIF
								
							BREAK
						ENDSWITCH
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehs[mvf_firetruck_1].peds[2].id)
					AND NOT IS_PED_INJURED(vehs[mvf_firetruck_1].peds[2].id)
						SWITCH vehs[mvf_firetruck_1].peds[2].iGenericFlag
							CASE 0
								
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_firetruck_1].peds[2].id, SCRIPT_TASK_PERFORM_SEQUENCE)
									
									SAFE_OPEN_SEQUENCE()
										IF IS_PED_IN_ANY_VEHICLE(vehs[mvf_firetruck_1].peds[2].id)
											TASK_LEAVE_ANY_VEHICLE(null)
										ENDIF
										TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<167.0532, -670.3171, 42.1430>>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 227.9952)
										TASK_STAND_STILL(null, -1)
									SAFE_PERFORM_SEQUENCE(vehs[mvf_firetruck_1].peds[2].id)
								
								ENDIF
								
							BREAK
						ENDSWITCH
					ENDIF

				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(vehs[mvf_firetruck_2].id)
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_firetruck_2].id)
					Unload_Asset_Recording(sAssetData, rec_firetruck_pullup_2, str_car_recs)
					
					IF DOES_ENTITY_EXIST(vehs[mvf_firetruck_2].peds[0].id)
					AND NOT IS_PED_INJURED(vehs[mvf_firetruck_2].peds[0].id)
						SWITCH vehs[mvf_firetruck_2].peds[0].iGenericFlag
							CASE 0
								
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_firetruck_2].peds[0].id, SCRIPT_TASK_PERFORM_SEQUENCE)
									
									SAFE_OPEN_SEQUENCE()
										IF IS_PED_IN_ANY_VEHICLE(vehs[mvf_firetruck_2].peds[0].id)
											TASK_LEAVE_ANY_VEHICLE(null)
										ENDIF
										TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<158.5242, -676.4426, 41.0273>>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 285.7014)
										TASK_STAND_STILL(null, -1)
									SAFE_PERFORM_SEQUENCE(vehs[mvf_firetruck_2].peds[0].id)
								
								ENDIF
								
							BREAK
						ENDSWITCH
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehs[mvf_firetruck_2].peds[1].id)
					AND NOT IS_PED_INJURED(vehs[mvf_firetruck_2].peds[1].id)
						SWITCH vehs[mvf_firetruck_2].peds[1].iGenericFlag
							CASE 0
								
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(vehs[mvf_firetruck_2].peds[1].id, SCRIPT_TASK_PERFORM_SEQUENCE)
									
									SAFE_OPEN_SEQUENCE()
										IF IS_PED_IN_ANY_VEHICLE(vehs[mvf_firetruck_2].peds[1].id)
											TASK_LEAVE_ANY_VEHICLE(null)
										ENDIF
										TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<163.4239, -668.7816, 42.1430>>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 226.5554)
										TASK_STAND_STILL(null, -1)
									SAFE_PERFORM_SEQUENCE(vehs[mvf_firetruck_2].peds[1].id)
								
								ENDIF
								
							BREAK
						ENDSWITCH
					ENDIF
					
				ENDIF
			ENDIF

			// Clean up peds, leave vehicles, they will get cleaned up when arriving at franklins
			IF DOES_ENTITY_EXIST(vehs[mvf_police_car_1].peds[0].id)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vStreetPickupLocation) > 300.0
			
					IF DOES_ENTITY_EXIST(vehs[mvf_police_car_1].peds[0].id)
						DELETE_PED(vehs[mvf_police_car_1].peds[0].id)
					ENDIF
					IF DOES_ENTITY_EXIST(vehs[mvf_police_car_2].peds[0].id)
						DELETE_PED(vehs[mvf_police_car_2].peds[0].id)
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehs[mvf_firetruck_1].peds[0].id)
						DELETE_PED(vehs[mvf_firetruck_1].peds[0].id)
					ENDIF
					IF DOES_ENTITY_EXIST(vehs[mvf_firetruck_1].peds[1].id)
						DELETE_PED(vehs[mvf_firetruck_1].peds[1].id)
					ENDIF
					IF DOES_ENTITY_EXIST(vehs[mvf_firetruck_1].peds[2].id)
						DELETE_PED(vehs[mvf_firetruck_1].peds[2].id)
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehs[mvf_firetruck_2].peds[0].id)
						DELETE_PED(vehs[mvf_firetruck_2].peds[0].id)
					ENDIF
					IF DOES_ENTITY_EXIST(vehs[mvf_firetruck_2].peds[1].id)
						DELETE_PED(vehs[mvf_firetruck_2].peds[1].id)
					ENDIF
					
					Event_Finish(mef_emergency_services)
	
				ENDIF
			ENDIF

		BREAK
	ENDSWITCH
ENDPROC

PROC me_manage_wanted_level()

	IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 3)

		// Manage wanted level
		INT i
		REPEAT Get_Number_Of_Ped_Killed_Events() i
		
			CPRINTLN(DEBUG_MIKE, "Killed Ped Events: ", Get_Number_Of_Ped_Killed_Events())
			
			IF DOES_ENTITY_EXIST(Get_Index_Of_Killed_Ped(i))
			AND IS_ENTITY_A_PED(Get_Index_Of_Killed_Ped(i))
				PED_INDEX pedVictim 
				pedVictim = GET_PED_INDEX_FROM_ENTITY_INDEX(Get_Index_Of_Killed_Ped(i))
				CPRINTLN(DEBUG_MIKE, "	Ped Killed in slot: ", i)
				
				IF IS_ENTITY_DEAD(pedVictim)
					CPRINTLN(DEBUG_MIKE, "	Ped in slot: ", i, " is dead.")
				ELSE
					CPRINTLN(DEBUG_MIKE, "	Error: Killed ped in slot: ", i, " is alive.")
				ENDIF
				
				IF GET_PED_TYPE(pedVictim) = PEDTYPE_COP
				OR GET_PED_TYPE(pedVictim) = PEDTYPE_SWAT
				OR GET_PED_TYPE(pedVictim) = PEDTYPE_ARMY
				
					CPRINTLN(DEBUG_MIKE, "	Ped was a COP/SWAT/ARMY")

					IF DOES_ENTITY_EXIST(GET_PED_SOURCE_OF_DEATH(pedVictim))
					AND IS_ENTITY_A_PED(GET_PED_SOURCE_OF_DEATH(pedVictim))
						PED_INDEX pedSource 
						pedSource = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_PED_SOURCE_OF_DEATH(pedVictim))
						CPRINTLN(DEBUG_MIKE, "	Source of death found")
						IF pedSource = MIKE_PED_ID()
						OR pedSource = FRANK_PED_ID()
						OR pedSource = peds[mpf_goon].id
						OR pedSource = peds[mpf_driver].id
							CPRINTLN(DEBUG_MIKE, "	Source of death was a crew member")
							iKilledCopsCount++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDREPEAT
	
	ENDIF

	SWITCH mEvents[mef_manage_wanted_level].iStage
		CASE 1
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vStreetPickupLocation) > 300
			
				i_EscapeAreaTimer = GET_GAME_TIMER()
				mEvents[mef_manage_wanted_level].iStage++
			ENDIF
		BREAK
		CASE 2
			IF GET_GAME_TIMER() - i_EscapeAreaTimer > 5000
			AND (GET_GAME_TIMER() - i_EscapeAreaTimer > 60000 OR iKilledCopsCount > 10)
				SET_WANTED_LEVEL_MULTIPLIER(0.1)
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				SET_MAX_WANTED_LEVEL(3)
				i_EscapeAreaTimer = GET_GAME_TIMER()
				mEvents[mef_manage_wanted_level].iStage++
			ENDIF
		BREAK
		CASE 3
			IF GET_GAME_TIMER() - i_EscapeAreaTimer > 30000
				ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, FALSE)
				ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
				i_EscapeAreaTimer = GET_GAME_TIMER()
				mEvents[mef_manage_wanted_level].iStage++
			ENDIF
		BREAK
		CASE 4
			IF GET_GAME_TIMER() - i_EscapeAreaTimer > 15000
				ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
				i_EscapeAreaTimer = GET_GAME_TIMER()
				Event_Finish(mef_manage_wanted_level)
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC
					
// Gets the string name of an event
FUNC STRING Get_Event_Name(mission_event_flag mef)	
	STRING result
	SWITCH mef
		CASE 		mef_manage_radar_map						result = "Manage Map"					BREAK
		CASE		mef_manage_sprinklers						result = "Manage Sprinklers"			BREAK
		CASE		mef_manage_parachuting						result = "Parachute: Player"			BREAK
		CASE		mef_manage_parachuting_buddies				result = "Parachute: Buddies"			BREAK
		CASE		mef_manage_parachute_marker					result = "Parachute: Marker"			BREAK
		CASE		mef_manage_switch							result = "Manage SWITCH"				BREAK
		CASE		mef_manage_clock							result = "Manage Clock"					BREAK
		CASE		mef_manage_fire_meshes						result = "Manage fire"					BREAK
		CASE		mef_manage_smoke_vfx						result = "VFX PlayerSmoke"				BREAK
		CASE 		mef_manage_download_shootout_spawning		result = "DLS: spawning"				BREAK
		CASE 		mef_manage_download_shootout_rooftop		result = "DLS: rooftop"					BREAK
		CASE		mef_manage_shootout_doors					result = "DLS: doors"					BREAK
		CASE		mef_manage_wanted_level						result = "Manage WL"					BREAK
		CASE		mef_gas_grenades							result = "DLS: Gas"						BREAK
		CASE 		mef_post_heli_crash_enemies					result = "DLS: Post heli"				BREAK
		CASE		mef_ig2_try_door							result = "IG2: Try the door"			BREAK
		CASE		mef_ig3_swat_takedown						result = "IG3: Swat Takedown"			BREAK
		CASE		mef_ig4_kick_door							result = "IG4: Kick Door"				BREAK
		CASE		mef_ig5_push_door							result = "IG5: Push Door"				BREAK
		CASE		mef_ig7_elevator							result = "IG7: Agent elevator"			BREAK
		CASE		mef_ig8_paramedic							result = "IG8: Paramedic"				BREAK
		CASE		mef_ig9_dismount_player						result = "IG9: Dismount_PLR"			BREAK
		CASE		mef_ig9_dismount_buddies					result = "IG9: Dismount_BUD"			BREAK
		CASE		mef_pre_crashed_heli_corridor				result = "BRNT: PreCrash"				BREAK
		CASE		mef_comms_2									result = "BRNT: Comms2"					BREAK
		CASE		mef_rappel_room								result = "BRNT: RappelRoom"				BREAK
		CASE		mef_bodies									result = "BRNT: Bodies"					BREAK
		CASE		mef_leadout_MCS_2							result = "Leadout: MCS_2"				BREAK
		CASE		mef_heli_crash_cutscene						result = "Heli: Crash cutscene"			BREAK
		CASE		mef_heli_to_heli_missile					result = "Heli: Missile"				BREAK
		CASE		mef_heli_rpl_flyby							result = "Heli: Enemy Flyby"			BREAK
		CASE		mef_heli_rpl_attack							result = "Heli: Fight post rappel"		BREAK
		CASE		mef_heli_news								result = "Heli: News"					BREAK
		CASE		mef_emergency_services						result = "911 Services"					BREAK
		CASE		mef_getaway_driver_arrival					result = "Getaway Arrive"				BREAK
		CASE		mef_swat_fib_arrival						result = "SWAT arrival"					BREAK
		CASE		mef_ground_agents							result = "Ground: Agents"				BREAK
		CASE 		mef_kill_sprinklers							result = "Kill sprinklers"				BREAK
	ENDSWITCH
	RETURN result
ENDFUNC

//PURPOSE: Draws event debug information
PROC Display_Event_Debug_Info()
#IF IS_DEBUG_BUILD
	CONST_FLOAT f_left 			0.05 // 0.1
	CONST_FLOAT f_top 			0.05 // 0.1
	CONST_FLOAT f_spacing		0.05
	CONST_FLOAT	f_txt_scale		0.4
	CONST_INT	i_event_offset	3
	
	CONST_FLOAT f_name_offset	0.03
	CONST_FLOAT f_status_offset	0.17
	CONST_FLOAT	f_stage_offset	0.265
	
	INT i_active_count = 0
	BOOL bShowAllEvents
	IF IS_KEYBOARD_KEY_PRESSED(KEY_END)
		bShowAllEvents = TRUE
	ELSE
		bShowAllEvents = FALSE
	ENDIF	
	
	INT i, i_wants_to_be_active, i_running_count, i_start_from_here
	i_start_from_here = -1
	WHILE i >= 0
		BOOL bIsActive = FALSE
		
		IF bShowAllEvents
			SWITCH mEvents[i].state
				CASE EVENTSTATE_RUNNING		
				FALLTHRU
				CASE EVENTSTATE_DELAYED
					i_running_count++
				FALLTHRU
				CASE EVENTSTATE_READY
				FALLTHRU
				CASE EVENTSTATE_PAUSED
				FALLTHRU
				CASE EVENTSTATE_COMPLETED
				FALLTHRU
				CASE EVENTSTATE_RESET		
					bIsActive = TRUE
				BREAK
			ENDSWITCH
		ELSE
			SWITCH mEvents[i].state
				CASE EVENTSTATE_RUNNING		
				FALLTHRU
				CASE EVENTSTATE_DELAYED
					i_running_count++
				FALLTHRU
				CASE EVENTSTATE_RESET
				FALLTHRU
				CASE EVENTSTATE_PAUSED
					bIsActive = TRUE
				BREAK
				CASE EVENTSTATE_COMPLETED	
					IF((GET_GAME_TIMER() - mEvents[i].iTimeSinceLastRunning) < 10000)
						bIsActive = TRUE
					ENDIF	
				BREAK
			ENDSWITCH
		ENDIF
		
		// store the index of the start for the active event
		IF bIsActive
			i_wants_to_be_active++
			IF i_wants_to_be_active > i_event_page*10 
				IF i_start_from_here = -1
					i_start_from_here = i
				ENDIF
				IF i_active_count < 10
					i_active_count++
				ENDIF
			ENDIF
		ENDIF
		
		i++
		IF i > enum_to_int(mef_num_events)-1
			i = -1
		ENDIF
	ENDWHILE
	
	// If none active then there will be one line in there saying nothing is active
	INT i_additional_line
	IF i_active_count > 0
		i_additional_line = 2
	ELSE
		i_additional_line = 1
	ENDIF
	
	DRAW_RECT_FROM_CORNER(f_left-0.01, f_top-0.01, 0.39, (f_txt_scale * 0.05 * (i_active_count+i_event_offset+i_additional_line)) + 0.02, 0,0,0, 150)
	
	TEXT_LABEL_31 str_debug_info = "Mission Stage: "
	str_debug_info += (mission_stage)
	SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(f_left, f_top, "STRING", str_debug_info)
	
	str_debug_info = "Mission Substage: "
	str_debug_info += (mission_substage)
	SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(f_left + 0.12, f_top, "STRING", str_debug_info)
	
	str_debug_info = "AI: "
	str_debug_info += (iCrewAIStage)
	SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(f_left + 0.26, f_top, "STRING", str_debug_info)
	
	str_debug_info = "Assets: "
	str_debug_info += Get_Num_Assets_Loaded(sAssetData,TRUE)
	str_debug_info += "/"
	str_debug_info += Get_Num_Assets_Loaded(sAssetData,FALSE)
	SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(f_left + 0.30, f_top, "STRING", str_debug_info)
	
	i_active_count = 0
	SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(f_left, 					f_top+((i_event_offset-1)*f_spacing*f_txt_scale), "STRING", "ID")
	SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(f_left+f_name_offset, 		f_top+((i_event_offset-1)*f_spacing*f_txt_scale), "STRING", "Name")
	SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(f_left+f_status_offset, 	f_top+((i_event_offset-1)*f_spacing*f_txt_scale), "STRING", "Status")
	SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
	DISPLAY_TEXT_WITH_LITERAL_STRING(f_left+f_stage_offset, 	f_top+((i_event_offset-1)*f_spacing*f_txt_scale), "STRING", "Stage")
	
	i = i_start_from_here
	WHILE i >= 0
	
		INT debug_draw_col_r, debug_draw_col_g, debug_draw_col_b
		BOOL bDrawThisEvent
		TEXT_LABEL_23 str_debug_status
		bDrawThisEvent = FALSE

		SWITCH mEvents[i].state
			CASE EVENTSTATE_READY
				str_debug_status = "Idle"
				debug_draw_col_r = 77
				debug_draw_col_g = 77
				debug_draw_col_b = 77
				IF bShowAllEvents
					bDrawThisEvent = TRUE
					i_active_count++
				ENDIF
			BREAK
			CASE EVENTSTATE_RUNNING
				str_debug_status = "Running"
				debug_draw_col_r = 255
				debug_draw_col_g = 191
				debug_draw_col_b = 0
				bDrawThisEvent = TRUE
				i_active_count++
			BREAK
			CASE EVENTSTATE_DELAYED
				str_debug_status = "Delayed"
				debug_draw_col_r = 184
				debug_draw_col_g = 184
				debug_draw_col_b = 116
				bDrawThisEvent = TRUE
				i_active_count++
			BREAK
			CASE EVENTSTATE_PAUSED
				str_debug_status = "Paused"
				debug_draw_col_r = 255
				debug_draw_col_g = 0
				debug_draw_col_b = 255
				bDrawThisEvent = TRUE
				i_active_count++
			BREAK
			CASE EVENTSTATE_COMPLETED
				str_debug_status = "Complete"
				debug_draw_col_r = 68
				debug_draw_col_g = 255
				debug_draw_col_b = 40
				IF bShowAllEvents
				OR ((GET_GAME_TIMER() - mEvents[i].iTimeSinceLastRunning) < 10000)
					bDrawThisEvent = TRUE
					i_active_count++
				ENDIF
			BREAK
			CASE EVENTSTATE_RESET
				str_debug_status = "HasReset"
				debug_draw_col_r = 0
				debug_draw_col_g = 191
				debug_draw_col_b = 255
				bDrawThisEvent = TRUE
				i_active_count++
			BREAK
		ENDSWITCH
	
		
		IF bDrawThisEvent
			// ID
			str_debug_info = ""
			str_debug_info += i
			SET_TEXT_COLOUR(debug_draw_col_r,debug_draw_col_g,debug_draw_col_b, 255)
			SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
			DISPLAY_TEXT_WITH_LITERAL_STRING(f_left, f_top+((i_active_count+i_event_offset)*f_spacing*f_txt_scale), "STRING", str_debug_info)
			
			// Name
			str_debug_info = ""
			str_debug_info += Get_Event_Name(int_to_enum(mission_event_flag, i))
			SET_TEXT_COLOUR(debug_draw_col_r,debug_draw_col_g,debug_draw_col_b, 255)
			SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
			DISPLAY_TEXT_WITH_LITERAL_STRING(f_left+f_name_offset, f_top+((i_active_count+i_event_offset)*f_spacing*f_txt_scale), "STRING", str_debug_info)
			
			// Status
			SET_TEXT_COLOUR(debug_draw_col_r,debug_draw_col_g,debug_draw_col_b, 255)
			SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
			DISPLAY_TEXT_WITH_LITERAL_STRING(f_left+f_status_offset, f_top+((i_active_count+i_event_offset)*f_spacing*f_txt_scale), "STRING", str_debug_status)
			
			// Stage
			str_debug_info = ""
			str_debug_info += mEvents[i].iStage
			SET_TEXT_COLOUR(debug_draw_col_r,debug_draw_col_g,debug_draw_col_b, 255)
			SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
			DISPLAY_TEXT_WITH_LITERAL_STRING(f_left+f_stage_offset, f_top+((i_active_count+i_event_offset)*f_spacing*f_txt_scale), "STRING", str_debug_info)
		ENDIF
		
		i++
		IF i > enum_to_int(mef_num_events)-1
		OR i_active_count >= 10
			i = -1
		ENDIF
		
	ENDWHILE
	
	IF i_active_count = 0
		SET_TEXT_COLOUR(163, 163, 163, 255)
		SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
		DISPLAY_TEXT_WITH_LITERAL_STRING(f_left, f_top+(i_event_offset*f_spacing*f_txt_scale), "STRING", "no active mission events")
	ELSE
		str_debug_info = "currently running "
		str_debug_info += i_running_count 
		str_debug_info += " event(s)"
		SET_TEXT_COLOUR(255,255,255, 255)
		SET_TEXT_SCALE(f_txt_scale, f_txt_scale)
		DISPLAY_TEXT_WITH_LITERAL_STRING(f_left, f_top+(((i_active_count+1)+i_event_offset)*f_spacing*f_txt_scale), "STRING", str_debug_info)
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_INSERT)
		i_event_page--
	ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DELETE)
		i_event_page++
	ENDIF
	
	i_event_page = CLAMP_INT(i_event_page, 0, CEIL(i_wants_to_be_active/10.0)-1)
	
#ENDIF
ENDPROC

//PURPOSE: Manages calls to the correct procedures for each triggered event.
PROC Manage_Mission_Events()
	INT i
	FOR i = 0 TO enum_to_int(mef_num_events)-1
	
		SWITCH mEvents[i].state
		
			CASE EVENTSTATE_RUNNING
				// Normal update state
				SWITCH int_to_enum(mission_event_flag, i)
					
					CASE	mef_manage_radar_map						me_manage_radar_map()						BREAK
					CASE	mef_manage_sprinklers						me_manage_sprinklers()						BREAK
					CASE	mef_manage_smoke_vfx						me_manage_smoke_vfx()						BREAK
					CASE	mef_manage_fire_meshes						me_manage_fire_meshes()						BREAK
					CASE	mef_manage_wanted_level						me_manage_wanted_level()					BREAK
					
					// Manage the parachute stage
					CASE	mef_manage_parachuting						me_manage_parachuting()						BREAK
					CASE 	mef_manage_parachuting_buddies				me_manage_parachuting_buddies()				BREAK
					CASE	mef_manage_parachute_marker					me_manage_parachute_marker()				BREAK
					
					CASE 	mef_manage_clock							me_manage_clock()							BREAK
					
					// Manage switching between Michael & Franklin
					CASE	mef_manage_switch							me_manage_switch()							BREAK
					
					// Shoot out events
					CASE	mef_post_heli_crash_enemies					me_post_heli_crash_enemies()				BREAK
					CASE	mef_manage_download_shootout_spawning		me_manage_download_shootout_spawning()		BREAK
					CASE	mef_manage_download_shootout_rooftop		me_manage_download_shootout_rooftop()		BREAK
					CASE	mef_manage_shootout_doors					me_manage_shootout_doors()					BREAK
					
					CASE 	mef_gas_grenades							me_gas_grenades()							BREAK
					
					// synced scene events
					CASE	mef_ig2_try_door							me_ig2_try_door()							BREAK
					CASE 	mef_ig3_swat_takedown						me_ig3_swat_takedown()						BREAK
					CASE	mef_ig4_kick_door							me_ig4_kick_door()							BREAK
					CASE 	mef_ig5_push_door							me_ig5_push_door()							BREAK
					CASE	mef_ig7_elevator							me_ig7_agent_at_elevator()					BREAK
					CASE	mef_ig8_paramedic							me_ig8_paramedic()							BREAK
					CASE	mef_ig9_dismount_player						me_ig9_rappel_dismount_player()				BREAK
					CASE	mef_ig9_dismount_buddies					me_ig9_rappel_dismount_buddies()			BREAK
					
					CASE	mef_pre_crashed_heli_corridor				me_pre_crashed_heli_corridor()				BREAK
					CASE	mef_comms_2									me_comms_2()								BREAK
					CASE	mef_rappel_room								me_rappel_room()							BREAK
					CASE	mef_bodies									me_bodies()									BREAK
					
					CASE 	mef_leadout_MCS_2							me_leadout_MCS_2()							BREAK
					
					// Crash cutscene events
					CASE	mef_heli_crash_cutscene						me_heli_crash_cutscene()					BREAK
					CASE 	mef_heli_to_heli_missile					me_heli_to_heli_missile()					BREAK
					CASE	mef_heli_rpl_flyby							me_heli_rappel_flyby()						BREAK
					CASE 	mef_heli_rpl_attack							me_heli_rappel_attack()						BREAK
					CASE	mef_heli_news								me_heli_news()								BREAK
										
					CASE	mef_emergency_services						me_emergency_services()						BREAK
					CASE 	mef_getaway_driver_arrival					me_getaway_driver_arrival()					BREAK
					CASE 	mef_swat_fib_arrival						me_swat_fib_arrival()						BREAK
					
					CASE 	mef_ground_agents							me_ground_agents()							BREAK
					CASE	mef_kill_sprinklers							me_kill_sprinklers()						BREAK
				
				ENDSWITCH
			BREAK
			CASE EVENTSTATE_DELAYED
				// See if delay is over
				IF (GET_GAME_TIMER() - mEvents[i].iTimeStamp) > mEvents[i].iDelay
					mEvents[i].state = EVENTSTATE_RUNNING
					mEvents[i].iDelay = 0
				ENDIF
			BREAK
			CASE EVENTSTATE_RESET
				IF (GET_GAME_TIMER() - mEvents[i].iTimeSinceLastRunning) > 10000
					mEvents[i].state = EVENTSTATE_READY
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDFOR
	
	#IF IS_DEBUG_BUILD
		// Toggle event debug info on/off
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME)
			IF bDisplayEventDebug
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
			ELSE
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			ENDIF
			bDisplayEventDebug = NOT bDisplayEventDebug
		ENDIF
	
		IF bDisplayEventDebug
			Display_Event_Debug_Info()
		ENDIF
	#ENDIF
ENDPROC

//PURPOSE: Manages the crews AI when the player is not in control of that crew member
PROC Manage_Shootout_AI()

	//>>>>> AI for Michael <<<<<
	//===========================
	IF NOT IS_PED_INJURED(BUDDY_PED_ID())
		
		IF BUDDY_PED_ID() = MIKE_PED_ID()
			//SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), TRUE)

			IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(MIKE_PED_ID(), SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER)
				CDEBUG1LN(DEBUG_MIKE, DEBUG_MIKE, "Putting Michael into cover when not in control of him")
				TASK_PUT_PED_DIRECTLY_INTO_COVER(MIKE_PED_ID(), v_ShootoutCoverMike, -1, FALSE, 0, TRUE, FALSE, peds[mpf_mike].cov, TRUE)
			ENDIF
		ELSE
			//SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), FALSE)
		ENDIF

		//>>>>> AI for Franklin <<<<<
		//===========================
		IF BUDDY_PED_ID() = FRANK_PED_ID()
			//SET_ENTITY_INVINCIBLE(FRANK_PED_ID(), TRUE)
		
			IF i_EnemiesAlive > 0
				
				IF NOT IS_PED_IN_COMBAT(FRANK_PED_ID())
				
					CDEBUG1LN(DEBUG_MIKE, DEBUG_MIKE, "Shootout AI: Tasking Franklin COMBAT")
					SET_PED_COMBAT_PARAMS(peds[mpf_frank], v_ShootoutCoverFrank, 1.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.0, 0.0, FALSE, FALSE)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(FRANK_PED_ID(), 50.0)	
					
				ENDIF

			ELSE
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(FRANK_PED_ID(), SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
				AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(FRANK_PED_ID(), SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER)
				
					CDEBUG1LN(DEBUG_MIKE, DEBUG_MIKE, "Shootout AI: Tasking Franklin back to cover")
					SET_PED_COMBAT_PARAMS(peds[mpf_frank], v_ShootoutCoverFrank, 1.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.0, 0.0, FALSE, FALSE)
					TASK_SEEK_COVER_TO_COVER_POINT(FRANK_PED_ID(), peds[mpf_frank].cov, <<144.7441, -750.0186, 258.5020>>, -1, FALSE)
					
				ENDIF
			ENDIF
		
		// PLAYER IN CONTROL OF FRANKLIN
		ELSE
			//SET_ENTITY_INVINCIBLE(FRANK_PED_ID(), FALSE)
		ENDIF
		
	ENDIF
	
	//>>>>> AI for Gunman <<<<<
	//==========================
	IF i_EnemiesAlive > 0

		IF NOT IS_PED_IN_COMBAT(peds[mpf_goon].id)
		
			CDEBUG1LN(DEBUG_MIKE, DEBUG_MIKE, "Shootout AI: Tasking Gunman COMBAT")
			SET_PED_COMBAT_PARAMS(peds[mpf_goon], v_ShootoutCoverGoon, 1.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.0, 0.0, FALSE, FALSE)
			TASK_COMBAT_HATED_TARGETS_AROUND_PED(peds[mpf_goon].id, 50.0)	
			
		ENDIF
		
	ELSE
	
		IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
		AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER)
		
			CDEBUG1LN(DEBUG_MIKE, DEBUG_MIKE, "Shootout AI: Tasking Gunman back to cover")
			SET_PED_COMBAT_PARAMS(peds[mpf_goon], v_ShootoutCoverGoon, 1.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.0, 0.0, FALSE, FALSE)
			TASK_SEEK_COVER_TO_COVER_POINT(peds[mpf_goon].id, peds[mpf_goon].cov, <<141.8358, -754.4357, 258.6976>>, -1, FALSE)
			
		ENDIF
		
	ENDIF
ENDPROC

//PURPOSE: Processes any change from one stage to another that is not part of the mission flow.
PROC Mission_Stage_Skip()
	// a skip has been made
	IF bDoSkip = TRUE
		
		// Begin the skip if the switching is idle
		IF stageSwitch = STAGESWITCH_IDLE
			
			IF NOT IS_SCREEN_FADED_OUT()
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(1000)
				ENDIF
			ELSE
				Mission_Set_Stage(int_to_enum(mission_stage_flag, iSkipToStage))
			ENDIF

		// needs to be carried out before states own entering stage
		ELIF stageSwitch = STAGESWITCH_ENTERING
			CDEBUG1LN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Starting Skip")
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			CLEAR_PRINTS()
			CLEAR_HELP()
			
			Mission_Reset_Everything()
			IF IS_CUTSCENE_ACTIVE()
				REMOVE_CUTSCENE()
			ENDIF
			
			Start_Skip_Streaming(sAssetData)
			
		// Warping
		// -----------------------------------------------
		
			// must be doing a z skip
			IF NOT IS_REPLAY_BEING_SET_UP()
			
				SET_BUILDING_IPLS( int_to_enum( MISSION_STAGE_FLAG, mission_stage ) )
			
				CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Performing NON-REPLAY skip warping")
				
				VECTOR vWarpCoord
				FLOAT fWarpHeading
				GET_SKIP_STAGE_COORD_AND_HEADING(iSkipToStage, vWarpCoord, fWarpHeading)
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpCoord)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fWarpHeading)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				Load_Asset_NewLoadScene_Sphere(sAssetData, vWarpCoord, 50.0)
				
				CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() NON-REPLAY skip warp coord ", vWarpCoord, " warp heading ", fWarpHeading)
				
			ENDIF
			
			BOOL bIgnoreFade
			
		// Asset loading
		// -----------------------------------------------
			CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Start load")
		
			SWITCH int_to_enum(mission_stage_flag, mission_stage)
				CASE msf_0_intro_cut
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						REQUEST_CUTSCENE("AH_3B_INT")
					ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("AH_3B_INT", CS_SECTION_2)
					ENDIF
					Register_Ped_Prop_For_Cutscene(sCutscenePedVariationRegister, "Lester", ANCHOR_EYES, 0, 0)
				BREAK
				CASE msf_1_go_to_helipad	
					Load_Asset_Model(sAssetData, PROP_CS_LESTER_CRATE)
					Load_Asset_Model(sAssetData, PROP_CS_WALKING_STICK)
					Load_Asset_AnimDict( sAssetData, "missheistfbi3bleadinoutah_3b_int" )
				BREAK
				CASE msf_2_fly_to_jump_zone
					Load_Asset_Model(sAssetData, mod_heli)
					Load_Asset_Model(sAssetData, mod_pilot)
				BREAK
				CASE msf_3_sky_dive_and_land
					Load_Asset_Model(sAssetData, mod_heli)
					Load_Asset_Model(sAssetData, mod_pilot)
					Load_Asset_AnimDict(sAssetData, animDict_IG1_sky_dive)
					Load_Asset_PTFX(sAssetData)
				BREAK
				CASE msf_4_hack_the_computer
					Load_Asset_Waypoint(sAssetData, wp_try_door)
					Load_Asset_AnimDict(sAssetData, animDict_IG2_try_door)
					Load_Asset_Weapon_Asset(sAssetData, WEAPONTYPE_STICKYBOMB)
					Load_Asset_Model(sAssetData, PROP_FIB_SKYLIGHT_PLUG)
					Load_Asset_PTFX(sAssetData)
					Load_Asset_AnimDict(sAssetData, "missheistfbi3bleadinoutah_3b_mcs_2")
					Load_Asset_Interior(sAssetData, interior_fib03)
					LOAD_CREW_WEAPON_ASSETS()
				BREAK
				CASE msf_5_download_shootout
					Load_Asset_Model(sAssetData, mod_fib_agent_1)
					Load_Asset_Model(sAssetData, mod_fib_agent_2)
					Load_Asset_Model(sAssetData, mod_fib_agent_3)
					Load_Asset_Model(sAssetData, mod_swat)
					Load_Asset_Model(sAssetData, mod_hack_phone)
					Load_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B)
					Load_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B_02)
					Load_Asset_Model(sAssetData, PROP_FIB_SKYLIGHT_PLUG)
					Load_Asset_PTFX(sAssetData)
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS")
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER")
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER_B")
					Load_Asset_Audiobank(sAssetData, "FBI_HEIST_3B_SHOOTOUT")
					Load_Asset_Scaleform(sAssetData, "Hacking_PC", sfHacking, TRUE)
					Load_Asset_Interior(sAssetData, interior_fib03)
					Load_Asset_Model( sAssetData, P_LD_HEIST_BAG_S )
					Load_Asset_AnimDict( sAssetData, "missheistfbi3bleadinoutah_3b_mcs_2" )
					LOAD_CREW_WEAPON_ASSETS()
				BREAK
				CASE msf_6_stairwell
					Load_Asset_Model(sAssetData, mod_fib_agent_1)
					Load_Asset_Model(sAssetData, mod_fib_agent_2)
					Load_Asset_Model(sAssetData, mod_fib_agent_3)
					Load_Asset_Model(sAssetData, mod_swat)
					Load_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B)
					Load_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B_02)
					Load_Asset_Model(sAssetData, PROP_FIB_SKYLIGHT_PLUG)
					Load_Asset_Waypoint(sAssetData, wp_sw_0_frank)
					Load_Asset_Waypoint(sAssetData, wp_sw_0_goon)
					Load_Asset_AnimDict(sAssetData, animDict_IG3_kill_swat)
					Load_Asset_PTFX(sAssetData)
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS")
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER")
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER_B")
					Load_Asset_Model( sAssetData, P_LD_HEIST_BAG_S )
					Load_Asset_AnimDict( sAssetData, "missheistfbi3bleadinoutah_3b_mcs_2" )
					LOAD_CREW_WEAPON_ASSETS()
				BREAK
				CASE msf_7_burnt_out_floor_1
					Load_Asset_Model(sAssetData, mod_fib_agent_1)
					Load_Asset_Model(sAssetData, mod_fib_agent_2)
					Load_Asset_Model(sAssetData, mod_fib_agent_3)
					Load_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B)
					Load_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B_02)
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS")
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER")
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER_B")
					Load_Asset_PTFX(sAssetData)
//					Load_Asset_Interior(sAssetData, interior_fib02)
//					Load_Asset_Movie_Mesh_Set(sAssetData, "agency_IT3_01_3b.xml", interior_fib02)
//					Load_Asset_Movie_Mesh_Set(sAssetData, "agency_IT3_02_3b.xml", interior_fib02)
					LOAD_CREW_WEAPON_ASSETS()
				BREAK
				CASE msf_8_burnt_out_floor_2
					Load_Asset_Model(sAssetData, mod_fib_agent_1)
					Load_Asset_Model(sAssetData, mod_fib_agent_2)
					Load_Asset_Model(sAssetData, mod_fib_agent_3)
					Load_Asset_AnimDict(sAssetData, animDict_IG7_elevator)
					Load_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B)
					Load_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B_02)
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS")
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER")
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER_B")
					Load_Asset_PTFX(sAssetData)
//					Load_Asset_Interior(sAssetData, interior_fib02)
//					Load_Asset_Interior(sAssetData, interior_fib03)
//					Load_Asset_Movie_Mesh_Set(sAssetData, "agency_IT3_01_3b.xml", interior_fib03)
//					Load_Asset_Movie_Mesh_Set(sAssetData, "agency_IT3_02_3b.xml", interior_fib03)
//					Load_Asset_Movie_Mesh_Set(sAssetData, "agency_IT3_03_3b.xml", interior_fib03)
//					Load_Asset_Movie_Mesh_Set(sAssetData, "agency_IT2_01_3b.xml", interior_fib02)
//					Load_Asset_Movie_Mesh_Set(sAssetData, "agency_IT2_02_3b.xml", interior_fib02)
					Load_Asset_Audiobank(sAssetData, "SIREN_DISTANT")
					LOAD_CREW_WEAPON_ASSETS()
				BREAK
				CASE msf_9_abseil_down
					Load_Asset_Model(sAssetData, mod_fib_agent_3)
					Load_Asset_Model(sAssetData, mod_swat)
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS")
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER")
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER_B")
					Load_Asset_Audiobank(sAssetData, "SIREN_DISTANT")
					Load_Asset_PTFX(sAssetData)
					LOAD_CREW_WEAPON_ASSETS()
					REQUEST_RAPPEL_ASSETS(TRUE)
				BREAK
				CASE msf_10_get_to_getaway_veh
					IF GET_DRIVER_SKILL_LEVEL() = CMSK_GOOD
						Load_Asset_Model(sAssetData, AMBULANCE)
					ENDIF
					Load_Asset_Model(sAssetData, mod_fib_agent_3)
					Load_Asset_Model(sAssetData, FIRETRUK)
					Load_Asset_Model(sAssetData, POLICE3)
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS")
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER")
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER_B")
					Load_Asset_PTFX(sAssetData)
					LOAD_CREW_WEAPON_ASSETS()
				BREAK
				CASE msf_11_escape_to_franklins
					IF GET_DRIVER_SKILL_LEVEL() = CMSK_GOOD
						Load_Asset_Model(sAssetData, AMBULANCE)
					ELSE
						Load_Asset_Model(sAssetData, FBI2)
						Load_Asset_Model(sAssetData, mod_esc_veh)
					ENDIF
					Load_Asset_Model(sAssetData, FIRETRUK)
					Load_Asset_Model(sAssetData, POLICE3)
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS")
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER")
					Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER_B")
					Load_Asset_PTFX(sAssetData)
					LOAD_CREW_WEAPON_ASSETS()
				BREAK
				CASE msf_12_mission_passed
					Load_Asset_Interior(sAssetData, interior_franklins)
					REQUEST_CUTSCENE("AH_3B_EXT")
					Register_Ped_Outfit_For_Cutscene(sCutscenePedVariationRegister, "Michael", OUTFIT_P0_STEALTH_NO_MASK, PLAYER_ZERO)
					Register_Ped_Outfit_For_Cutscene(sCutscenePedVariationRegister, "Franklin", OUTFIT_P1_STEALTH_NO_MASK, PLAYER_ONE)
				BREAK
			ENDSWITCH
			
			WHILE NOT Update_Skip_Streaming(sAssetData)
				Mission_Checks(TRUE)
				Update_Cutscene_Prestreaming(sCutscenePedVariationRegister)
				SKIP_WAIT(0)
			ENDWHILE
			
			CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Finish load & start setup")
			
			// Set up the mission for that current stage
			SWITCH int_to_enum(mission_stage_flag, mission_stage)
				CASE msf_0_intro_cut
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					
					IS_SKIP_OK_TO_FADE_IN_NOW()
					
					bIgnoreFade = TRUE
				BREAK
				CASE msf_1_go_to_helipad
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
				
					// 1994424
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						Set_Current_Player_Ped(SELECTOR_PED_MICHAEL, TRUE)
					ELSE
						Set_Current_Player_Ped(SELECTOR_PED_FRANKLIN, TRUE)
					ENDIF
					
					WHILE NOT CREATE_NPC_PED_ON_FOOT( peds[mpf_lester].id, CHAR_LESTER, << 713.033, -964.717, 30.399 >>, 42.000 )
						SKIP_WAIT(0)
						Mission_Checks(TRUE)
					ENDWHILE
					SET_PED_PROP_INDEX( peds[mpf_lester].id, ANCHOR_EYES, 0, 0 )
					
					SET_PED_RELATIONSHIP_GROUP_HASH(peds[mpf_lester].id, REL_CREW)
					
					objs[mof_lester_crate].id = CREATE_OBJECT( PROP_CS_LESTER_CRATE, << 713.033, -964.717, 30.399 >> )
					ATTACH_ENTITY_TO_ENTITY(objs[mof_lester_crate].id, peds[mpf_lester].id, GET_PED_BONE_INDEX(peds[mpf_lester].id, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>)
					objs[mof_walking_stick].id = CREATE_OBJECT( PROP_CS_WALKING_STICK, << 713.033, -964.717, 30.399 >> )
					ATTACH_ENTITY_TO_ENTITY(objs[mof_walking_stick].id, peds[mpf_lester].id, GET_PED_BONE_INDEX(peds[mpf_lester].id, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
					TASK_PLAY_ANIM_ADVANCED(peds[mpf_lester].id, "missheistfbi3bleadinoutah_3b_int", "_leadout_b_lester", << 713.033, -964.717, 30.399 >>, << -0.000, 0.000, 42.000 >>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_lester].id)

					Unload_Asset_Model( sAssetData, PROP_CS_LESTER_CRATE )
					Unload_Asset_Model( sAssetData, PROP_CS_WALKING_STICK )

					WHILE NOT CREATE_PLAYER_VEHICLE(vehs[mvf_mikesCar].id, CHAR_MICHAEL, << 718.3592, -983.2876, 23.1379 >>, 270.6242)
						SKIP_WAIT(0)
						Mission_Checks(TRUE)
					ENDWHILE			
					
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						Create_Crew_Member(peds[mpf_frank], cmf_frank, << 717.8079, -971.8441, 25.3313 >>, 183.4215, CREW_OUTFIT_DEFAULT, DEFAULT, TRUE, TRUE)
					ELSE
						Create_Crew_Member(peds[mpf_mike], cmf_mike, << 717.8079, -971.8441, 25.3313 >>, 183.4215, CREW_OUTFIT_DEFAULT, DEFAULT, TRUE, TRUE)
					ENDIF

					IS_SKIP_OK_TO_FADE_IN_NOW()
					IF IS_REPLAY_BEING_SET_UP()
						SETTIMERA(0)
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Start END_REPLAY_SETUP() stating timer A")
						END_REPLAY_SETUP()
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Finish END_REPLAY_SETUP() @ ", TIMERA())
					ENDIF
					
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()

				BREAK
				CASE msf_2_fly_to_jump_zone
					SET_WANTED_LEVEL_MULTIPLIER(1.0)
					SET_MAX_WANTED_LEVEL(5)
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					SET_CURRENT_PLAYER_PED(SELECTOR_PED_MICHAEL, TRUE)
					SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH, FALSE)
					
					vehs[mvf_heli].id = CREATE_VEHICLE(mod_heli, vHeliStartCoord, fHeliStartHeading)
					SET_VEHICLE_AS_RESTRICTED(vehs[mvf_heli].id, 0)
					SET_VEHICLE_ENGINE_ON(vehs[mvf_heli].id, TRUE, TRUE)
					SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_heli].id, I_HELI_VARIATION)
					bFailChecksOnHeli = TRUE
					Unload_Asset_Model(sAssetData, mod_heli)
					
					CREATE_CREW_MEMBER_IN_VEHICLE(peds[mpf_frank], 	cmf_frank, 	vehs[mvf_heli], VS_BACK_RIGHT,	CREW_OUTFIT_STEALTH, DEFAULT, TRUE, TRUE)
					SET_PED_COMP_ITEM_CURRENT_SP(FRANK_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_STEALTH, FALSE)
					SET_PED_COMP_ITEM_CURRENT_SP(FRANK_PED_ID(), COMP_TYPE_SPECIAL, SPECIAL_P1_PARACHUTE, FALSE)
					CREATE_CREW_MEMBER_IN_VEHICLE(peds[mpf_goon], 	cmf_goon,	vehs[mvf_heli], VS_BACK_LEFT, 	CREW_OUTFIT_STEALTH, DEFAULT,TRUE, TRUE)
					CREATE_MISSION_PED_IN_VEHICLE(peds[mpf_pilot], mod_pilot, vehs[mvf_heli], VS_FRONT_RIGHT, "CREW_PILOT", REL_CREW)
					SET_PED_COMPONENT_VARIATION(peds[mpf_pilot].id, PED_COMP_SPECIAL2, 0, 0)
					SET_PED_PROP_INDEX(peds[mpf_pilot].id, ANCHOR_HEAD, 0, 0)
					Unload_Asset_Model(sAssetData, mod_pilot)
					
					SET_PED_COMPONENT_VARIATION(peds[mpf_goon].id, PED_COMP_FEET, 2, 0)
					CLEAR_AREA_OF_PEDS(<<1383.66, -2065.27, 51.00>>, 55.0)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_heli].id)
					
					SET_CLOCK_TIME(iStartHours, 0, 0)
					Event_Trigger(mef_manage_clock)
					
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
					
					IS_SKIP_OK_TO_FADE_IN_NOW()
					IF IS_REPLAY_BEING_SET_UP()
						SETTIMERA(0)
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Start END_REPLAY_SETUP() stating timer A")
						END_REPLAY_SETUP(vehs[mvf_heli].id, VS_DRIVER)
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Finish END_REPLAY_SETUP() @ ", TIMERA())
					ELSE
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehs[mvf_heli].id, VS_DRIVER)
					ENDIF
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
				BREAK
				CASE msf_3_sky_dive_and_land
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
										
					vehs[mvf_heli].id = CREATE_VEHICLE(mod_heli, vHeliJumpCoord, fHeliJumpHeading)
					SET_VEHICLE_AS_RESTRICTED(vehs[mvf_heli].id, 0)
					SET_VEHICLE_ENGINE_ON(vehs[mvf_heli].id, TRUE, TRUE)
					SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_heli].id, I_HELI_VARIATION)
					FREEZE_ENTITY_POSITION(vehs[mvf_heli].id, TRUE)
					Unload_Asset_Model(sAssetData, mod_heli)
					
					SET_CURRENT_PLAYER_PED(SELECTOR_PED_MICHAEL, TRUE)
					SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH, FALSE)
					CREATE_CREW_MEMBER_IN_VEHICLE(peds[mpf_frank], 	cmf_frank, 	vehs[mvf_heli], VS_BACK_RIGHT,	CREW_OUTFIT_STEALTH, DEFAULT, 	TRUE, TRUE)
					CREATE_CREW_MEMBER_IN_VEHICLE(peds[mpf_goon],	cmf_goon,	vehs[mvf_heli], VS_BACK_LEFT, 	CREW_OUTFIT_STEALTH, DEFAULT,	TRUE, TRUE)
					SET_PED_COMPONENT_VARIATION(peds[mpf_goon].id, PED_COMP_FEET, 2, 0)
					
					CREATE_MISSION_PED_IN_VEHICLE(peds[mpf_pilot], mod_pilot, vehs[mvf_heli], VS_DRIVER, "CREW_PILOT", REL_CREW)
					SET_PED_COMPONENT_VARIATION(peds[mpf_pilot].id, PED_COMP_SPECIAL2, 0, 0)
					SET_PED_PROP_INDEX(peds[mpf_pilot].id, ANCHOR_HEAD, 0, 0)
					Unload_Asset_Model(sAssetData, mod_pilot)
										
					SET_CLOCK_TIME(iStartHours, 30, 0)
					Event_Trigger(mef_manage_clock)

					ptfxs[ptfx_sweat_shop] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_agency3b_blding_smoke", vSweatShopSmokeCoord, vSweatShopSmokeRot, fSweatShopSmokeScale, FALSE, FALSE, FALSE)
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_DESTROYED ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_DESTROYED )
					
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
					
					IS_SKIP_OK_TO_FADE_IN_NOW()
					IF IS_REPLAY_BEING_SET_UP()
						SETTIMERA(0)
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Start END_REPLAY_SETUP() stating timer A")
						END_REPLAY_SETUP(vehs[mvf_heli].id, VS_FRONT_RIGHT)
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Finish END_REPLAY_SETUP() @ ", TIMERA())
					ELSE
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehs[mvf_heli].id, VS_FRONT_RIGHT)
					ENDIF
					
					FREEZE_ENTITY_POSITION(vehs[mvf_heli].id, FALSE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
					TRIGGER_MUSIC_EVENT("AH3B_SKYDIVE_RT")
					
					bExitStateMichael = FALSE
					bExitStateCam = FALSE
					
					bIgnoreFade = TRUE
				BREAK
				CASE msf_4_hack_the_computer
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					Set_Current_Player_Ped(SELECTOR_PED_MICHAEL, TRUE)
					SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH, FALSE)
					Create_Crew_Member(peds[mpf_frank], cmf_frank,	v_ShootoutCoverFrank, 74.3802, 	CREW_OUTFIT_STEALTH, DEFAULT, TRUE, TRUE, FALSE)
					Create_Crew_Member(peds[mpf_goon], cmf_goon, 	v_ShootoutCoverGoon, 57.7197, 	CREW_OUTFIT_STEALTH, DEFAULT, TRUE, TRUE, FALSE)
					Mission_Checks(TRUE)
					
					// Equip michael, rest of crew are given the weapons in the lead out
					GIVE_CREW_MEMBER_WEAPON(cmf_mike, TRUE, FALSE)
					SET_PED_FLASH_LIGHT(peds[mpf_mike], TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_frank], TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_goon], TRUE)
					
					SET_COMBAT_FLOAT(FRANK_PED_ID(), CCF_AIM_TURN_THRESHOLD, 0.03)
					SET_COMBAT_FLOAT(peds[mpf_goon].id, CCF_AIM_TURN_THRESHOLD, 0.03)
					
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
					
					IS_SKIP_OK_TO_FADE_IN_NOW()
					IF IS_REPLAY_BEING_SET_UP()
						SETTIMERA(0)
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Start END_REPLAY_SETUP() stating timer A")
						END_REPLAY_SETUP()
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Finish END_REPLAY_SETUP() @ ", TIMERA())
					ENDIF
					
					IF NOT bSwappedGlass
						//IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<139.4704, -745.9282, 263.1190>>, 0.1, V_ILEV_FIB_ATRGLSWAP)
							//CREATE_MODEL_SWAP(<<139.4704, -745.9282, 263.1190>>, 0.1, V_ILEV_FIB_ATRGLSWAP, PROP_FIB_SKYLIGHT_PLUG, TRUE)
							
							SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_ROOF, BUILDINGSTATE_CLEANUP)
							
							bSwappedGlass = TRUE
						//ENDIF
					ENDIF
					
					CLEAR_AREA_OF_OBJECTS(<<151.4373, -766.3517, 258.05>>, 3.0, CLEAROBJ_FLAG_FORCE) // clear the hack room to fix issue with there being 2 monitors left over
					WHILE NOT Setup_Hack_Room()
						SKIP_WAIT(0)
						Mission_Checks(TRUE)
					ENDWHILE
					
					Event_Trigger(mef_leadout_MCS_2)
					WHILE mEvents[mef_leadout_MCS_2].iStage <= 1
						me_leadout_MCS_2()
						wait(0)
					ENDWHILE
					
					SET_CLOCK_TIME(iStartHours+1, 0, 0)
					Event_Trigger(mef_manage_clock)
					
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_DESTROYED ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_DESTROYED )
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()

					IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_BREAK_SERVER_ROOM_DOOR")
						START_AUDIO_SCENE("AH_3B_BREAK_SERVER_ROOM_DOOR")
					ENDIF
					TRIGGER_MUSIC_EVENT("AH3B_HACK_RT")
				BREAK
				CASE msf_5_download_shootout
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					objs[mof_hack_phone].id = CREATE_OBJECT(mod_hack_phone, v_hack_phone_coord)
					SET_ENTITY_COORDS_NO_OFFSET(objs[mof_hack_phone].id, v_hack_phone_coord)
					SET_ENTITY_ROTATION(objs[mof_hack_phone].id, v_hack_phone_rot)
					Unload_Asset_Model(sAssetData, mod_hack_phone)
					
					// Create crew
					Set_Current_Player_Ped(SELECTOR_PED_MICHAEL, TRUE)
					SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH, FALSE)
					Create_Crew_Member(peds[mpf_frank], cmf_frank, v_ShootoutCoverFrank, 74.3802, 	CREW_OUTFIT_STEALTH, 	DEFAULT, 	TRUE, TRUE, FALSE)
					Create_Crew_Member(peds[mpf_goon], 	cmf_goon, v_ShootoutCoverGoon, 57.7197, 	CREW_OUTFIT_STEALTH, 	DEFAULT, 	TRUE, TRUE, FALSE)
					Mission_Checks(TRUE)
					
					ADD_CREW_SHOOTOUT_COVER_POINTS()
					
					//CREATE_MODEL_HIDE(<< 136.1795, -750.7010, 262.0516 >>, 10.0, V_ILEV_FIB_ATRCOL, TRUE)
					SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_ROOF, BUILDINGSTATE_CLEANUP)
					
					// Equip Crew
					GIVE_CREW_MEMBER_WEAPON(cmf_mike, TRUE)
					GIVE_CREW_MEMBER_WEAPON(cmf_frank, TRUE)
					GIVE_CREW_MEMBER_WEAPON(cmf_goon, TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_mike], TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_frank], TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_goon], TRUE)
					SET_CURRENT_PED_WEAPON(FRANK_PED_ID(), weap_Frank, TRUE)
					SET_CURRENT_PED_WEAPON(peds[mpf_goon].id, weap_Goon, TRUE)
					
					SET_PED_USING_ACTION_MODE(MIKE_PED_ID(), 		TRUE)
					SET_PED_USING_ACTION_MODE(FRANK_PED_ID(), 		TRUE)
					SET_PED_USING_ACTION_MODE(peds[mpf_goon].id,	TRUE, -1, "DEFAULT_ACTION")
												
					PLAY_ALARM_SECURITY(TRUE, TRUE, TRUE)
					
					Unload_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B)
					Unload_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B_02)
					DOWNLOAD_Setup_File_Download()
					PLAY_SOUND_FROM_COORD(sounds[sfx_downloading], "HACKING_DOWNLOADING", GET_ENTITY_COORDS(objs[mof_hack_computer].id))
					
					objs[mof_equipment_bag].id = CREATE_OBJECT_NO_OFFSET(P_LD_HEIST_BAG_S, v_mcs_2_leadout_coord )
					syncedScenes[ssf_leadout_MCS_2] = CREATE_SYNCHRONIZED_SCENE(v_mcs_2_leadout_coord, v_mcs_2_leadout_rot)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objs[mof_equipment_bag].id, syncedScenes[ssf_leadout_MCS_2], "_leadout_bag", "missheistfbi3bleadinoutah_3b_mcs_2", INSTANT_BLEND_IN)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME( syncedScenes[ssf_leadout_MCS_2], TRUE )
					SET_SYNCHRONIZED_SCENE_PHASE( syncedScenes[ssf_leadout_MCS_2], 0.9 )
					
					Unload_Asset_Model( sAssetData, P_LD_HEIST_BAG_S )
					
					DOORS_Set_State(mdf_topfloor_pre_stairs, TRUE, 0.0)
					
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)

					IS_SKIP_OK_TO_FADE_IN_NOW()
					IF IS_REPLAY_BEING_SET_UP()
						SETTIMERA(0)
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Start END_REPLAY_SETUP() stating timer A")
						END_REPLAY_SETUP()
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Finish END_REPLAY_SETUP() @ ", TIMERA())
					ENDIF
					
					IF NOT bSwappedGlass
						//IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<139.4704, -745.9282, 263.1190>>, 0.1, V_ILEV_FIB_ATRGLSWAP)
							//CREATE_MODEL_SWAP(<<139.4704, -745.9282, 263.1190>>, 0.1, V_ILEV_FIB_ATRGLSWAP, PROP_FIB_SKYLIGHT_PLUG, TRUE)
							SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_ROOF, BUILDINGSTATE_CLEANUP)
							bSwappedGlass = TRUE
						//ENDIF
					ENDIF
					
					CLEAR_AREA_OF_OBJECTS(<<151.4373, -766.3517, 258.05>>, 3.0, CLEAROBJ_FLAG_FORCE) // clear the hack room to fix issue with there being 2 monitors left over
					WHILE NOT Setup_Hack_Room(TRUE)
						SKIP_WAIT(0)
						Mission_Checks(TRUE)
					ENDWHILE
					
					SET_ENTITY_HEADING(objs[mof_hack_computer].id, -167.160) // make the monitor face the doorway
					
					Setup_Michael_In_Cover(FALSE)
					UPDATE_LOCKED_COVER_PED()
					UPDATE_LOCKED_COVER_CAM_LIMITER()
					
					TASK_PUT_PED_DIRECTLY_INTO_COVER(FRANK_PED_ID(), v_ShootoutCoverFrank, 1000, TRUE, 0, TRUE, TRUE, peds[mpf_frank].cov)
					TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_goon].id, v_ShootoutCoverGoon, 1000, TRUE, 0, TRUE, FALSE, peds[mpf_goon].cov)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(FRANK_PED_ID())
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_goon].id)
					
					SET_GAMEPLAY_CAM_WORLD_HEADING(70.111)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-3.8101)
					
					SET_CLOCK_TIME(iStartHours+1, 30, 0)
					Event_Trigger(mef_manage_clock)
					
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_DESTROYED ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_DESTROYED )
					
					bDownloadStarted 		= TRUE
					bFailOnMonitorDamage	= TRUE
					TRIGGER_MUSIC_EVENT("AH3B_DOWNLOADING_RT")
				BREAK
				CASE msf_6_stairwell
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					IF NOT bSwappedGlass
						//IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<<139.4704, -745.9282, 263.1190>>, 0.1, V_ILEV_FIB_ATRGLSWAP)
							//CREATE_MODEL_SWAP(<<139.4704, -745.9282, 263.1190>>, 0.1, V_ILEV_FIB_ATRGLSWAP, PROP_FIB_SKYLIGHT_PLUG, TRUE)
							SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_ROOF, BUILDINGSTATE_CLEANUP)
							bSwappedGlass = TRUE
						//ENDIF
					ENDIF
					
					VECTOR vStartCoord
					
					Set_Current_Player_Ped(SELECTOR_PED_MICHAEL, TRUE)
					SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH, FALSE)
					WAYPOINT_RECORDING_GET_COORD(wp_sw_0_frank, I_FRANK_WP_START, vStartCoord)
					Create_Crew_Member(peds[mpf_frank], cmf_frank, 	vStartCoord, 74.7167, 	CREW_OUTFIT_STEALTH, 	DEFAULT, TRUE, TRUE, FALSE)
					WAYPOINT_RECORDING_GET_COORD(wp_sw_0_goon, I_GOON_WP_START, vStartCoord)
					Create_Crew_Member(peds[mpf_goon], cmf_goon, 	vStartCoord, 68.4930, 	CREW_OUTFIT_STEALTH, 	DEFAULT, TRUE, TRUE, FALSE)
					Mission_Checks(TRUE)
					
					ADD_CREW_SHOOTOUT_COVER_POINTS()
					
					//CREATE_MODEL_HIDE(<< 136.1795, -750.7010, 262.0516 >>, 10.0, V_ILEV_FIB_ATRCOL, TRUE)
					//SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_ROOF, BUILDINGSTATE_DESTROYED)
					
					// Equip Crew
					GIVE_CREW_MEMBER_WEAPON(cmf_mike, TRUE)
					GIVE_CREW_MEMBER_WEAPON(cmf_frank, TRUE)
					GIVE_CREW_MEMBER_WEAPON(cmf_goon, TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_mike], TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_frank], TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_goon], TRUE)
					SET_CURRENT_PED_WEAPON(FRANK_PED_ID(), weap_Frank, TRUE)
					SET_CURRENT_PED_WEAPON(peds[mpf_goon].id, weap_Goon, TRUE)
					
					SET_PED_USING_ACTION_MODE(MIKE_PED_ID(), 		TRUE)
					SET_PED_USING_ACTION_MODE(FRANK_PED_ID(), 		TRUE)
					SET_PED_USING_ACTION_MODE(peds[mpf_goon].id,	TRUE, -1, "DEFAULT_ACTION")
										
					PLAY_ALARM_SECURITY(TRUE, TRUE, TRUE)
					PLAY_ALARM_FIRE(TRUE, TRUE)
					
					Unload_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B)
					Unload_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B_02)
					
					PREP_HOTSWAP(FALSE, FALSE, TRUE)
					Event_Trigger(mef_manage_switch)
					
					SETUP_FIB_COVERPOINTS(TRUE)
					
					objs[mof_equipment_bag].id = CREATE_OBJECT_NO_OFFSET(P_LD_HEIST_BAG_S, v_mcs_2_leadout_coord )
					syncedScenes[ssf_leadout_MCS_2] = CREATE_SYNCHRONIZED_SCENE(v_mcs_2_leadout_coord, v_mcs_2_leadout_rot)
					PLAY_SYNCHRONIZED_ENTITY_ANIM(objs[mof_equipment_bag].id, syncedScenes[ssf_leadout_MCS_2], "_leadout_bag", "missheistfbi3bleadinoutah_3b_mcs_2", INSTANT_BLEND_IN)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME( syncedScenes[ssf_leadout_MCS_2], TRUE )
					SET_SYNCHRONIZED_SCENE_PHASE( syncedScenes[ssf_leadout_MCS_2], 0.9 )
					
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
					
					IF IS_REPLAY_BEING_SET_UP()
						SETTIMERA(0)
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Start END_REPLAY_SETUP() stating timer A")
						END_REPLAY_SETUP()
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Finish END_REPLAY_SETUP() @ ", TIMERA())
					ENDIF
					IS_SKIP_OK_TO_FADE_IN_NOW()
					
					// Get rid of the door and grab the monitor
					CLEAR_AREA_OF_OBJECTS(<<151.4373, -766.3517, 258.05>>, 3.0, CLEAROBJ_FLAG_FORCE) // clear the hack room to fix issue with there being 2 monitors left over
					WHILE NOT Setup_Hack_Room(TRUE, FALSE, FALSE)
						SKIP_WAIT(0)
						Mission_Checks(TRUE)
					ENDWHILE
					
					SET_CLOCK_TIME(iStartHours+2, 0, 0)
					Event_Trigger(mef_manage_clock)
					
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_DESTROYED ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_DESTROYED )
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					
					Event_Trigger(mef_ig3_swat_takedown)
					TRIGGER_MUSIC_EVENT("AH3B_STAIRWELL_RT")
				BREAK
				CASE msf_7_burnt_out_floor_1
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					Set_Current_Player_Ped(SELECTOR_PED_MICHAEL, TRUE)
					SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH, FALSE)
					Create_Crew_Member(peds[mpf_frank], cmf_frank, 	<< 120.1913, -733.5589, 253.1525 >>, 167.3395, CREW_OUTFIT_STEALTH, DEFAULT, TRUE, TRUE, FALSE)
					SET_PED_COMP_ITEM_CURRENT_SP(FRANK_PED_ID(), COMP_TYPE_OUTFIT, 	OUTFIT_P1_STEALTH, FALSE)
					Create_Crew_Member(peds[mpf_goon], cmf_goon, 	<< 117.5880, -732.5183, 253.1525 >>, 158.8347, CREW_OUTFIT_STEALTH, DEFAULT, TRUE, TRUE, FALSE)
					Mission_Checks(TRUE)
					
					// Equip Crew
					GIVE_CREW_MEMBER_WEAPON(cmf_mike, TRUE)
					GIVE_CREW_MEMBER_WEAPON(cmf_frank, TRUE)
					GIVE_CREW_MEMBER_WEAPON(cmf_goon, TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_mike], TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_frank], TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_goon], TRUE)
					SET_CURRENT_PED_WEAPON(FRANK_PED_ID(), weap_Frank, TRUE)
					SET_CURRENT_PED_WEAPON(peds[mpf_goon].id, weap_Goon, TRUE)
					
					SET_PED_USING_ACTION_MODE(MIKE_PED_ID(), 		TRUE)
					SET_PED_USING_ACTION_MODE(FRANK_PED_ID(), 		TRUE)
					SET_PED_USING_ACTION_MODE(peds[mpf_goon].id,	TRUE, -1, "DEFAULT_ACTION")
					
					PLAY_ALARM_SECURITY(TRUE, TRUE)
					PLAY_ALARM_FIRE(TRUE, TRUE)
					
					Unload_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B)
					Unload_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B_02)
					Event_Trigger(mef_manage_sprinklers)
					iDesiredSprinklerState = 1
					g_replay.iReplayInt[0] = 0
					
					TASK_AIM_GUN_AT_COORD(peds[mpf_frank].id, << 118.0035, -735.9307, 254.6266 >>, -1, TRUE)
					TASK_AIM_GUN_AT_COORD(peds[mpf_goon].id, << 117.4079, -735.4523, 254.5799 >>, -1, TRUE)
					
					//CREATE_MODEL_HIDE(<< 136.1795, -750.7010, 262.0516 >>, 10.0, V_ILEV_FIB_ATRCOL, TRUE)
					SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_ROOF, BUILDINGSTATE_CLEANUP)
					
					DOORS_LockOpenNonAnimDoors()
					
					SETUP_FIB_COVERPOINTS(TRUE)
					
					PREP_HOTSWAP(FALSE, FALSE, TRUE)
					Event_Trigger(mef_manage_switch)
					
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
					
					IS_SKIP_OK_TO_FADE_IN_NOW()
					IF IS_REPLAY_BEING_SET_UP()
						SETTIMERA(0)
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Start END_REPLAY_SETUP() stating timer A")
						END_REPLAY_SETUP()
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Finish END_REPLAY_SETUP() @ ", TIMERA())
					ENDIF
					
					SET_CLOCK_TIME(iStartHours+2, 30, 0)
					Event_Trigger(mef_manage_clock)
					//Event_Trigger(mef_manage_fire_meshes)
					
					ptfxs[ptfx_sweat_shop] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_agency3b_blding_smoke", vSweatShopSmokeCoord, vSweatShopSmokeRot, fSweatShopSmokeScale, FALSE, FALSE, FALSE)
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_DESTROYED ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_DESTROYED )
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					
					Event_Trigger(mef_manage_smoke_vfx)		
					
					PRINT_NOW("A3B_ESCAPE", DEFAULT_GOD_TEXT_TIME, 1)
					SAFE_BLIP_COORD(blip_Objective, << 118.1325, -735.3059, 253.1525 >>) // blip again the objective

					TRIGGER_MUSIC_EVENT("AH3B_BURNTOUT_RT")
				BREAK
				CASE msf_8_burnt_out_floor_2
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					PLAY_SOUND_FROM_COORD(sounds[sfx_distant_sirens_1], "Distant_Sirens_Rubble", <<224.5, -627.0, 240.3>>, "FBI_HEIST_FINALE_CHOPPER")
					
					
					
					WHILE NOT DOES_RAYFIRE_MAP_OBJECT_EXIST(collapsing_floor)
						collapsing_floor = GET_RAYFIRE_MAP_OBJECT(<<152.2996, -736.4700, 253.0175>>, 10, "DES_FIB_Floor")
						SKIP_WAIT(0)
					ENDWHILE
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(collapsing_floor, RFMO_STATE_ENDING)
					
					Set_Current_Player_Ped(SELECTOR_PED_MICHAEL, TRUE)
					SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH, FALSE)
					
					Create_Crew_Member(peds[mpf_frank], cmf_frank, <<151.3366, -728.9139, 249.1526>>, 63.0760, CREW_OUTFIT_STEALTH, DEFAULT, TRUE, TRUE, FALSE)
					Create_Crew_Member(peds[mpf_goon], cmf_goon, <<144.8760, -732.1308, 249.1522>>, 66.7075, CREW_OUTFIT_STEALTH, DEFAULT, TRUE, TRUE, FALSE)
					
					CREATE_COVERPOINT_PAIR(covPair_buddy, <<151.3366, -728.9139, 249.1526>>, 63.0760, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
					SAFE_CREATE_COVERPOINT(covPair_gunman.covID[covPair_gunman.id], <<144.8760, -732.1308, 249.1522>>, 66.7075, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
					
					Mission_Checks(TRUE)
					
					TASK_PUT_PED_DIRECTLY_INTO_COVER(FRANK_PED_ID(), <<151.3366, -728.9139, 249.1526>>, -1, FALSE, 0, TRUE, TRUE, covPair_buddy.covID[covPair_buddy.id])
					TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_goon].id, <<144.8760, -732.1308, 249.1522>>, -1, FALSE, 0, TRUE, TRUE, covPair_gunman.covID[covPair_gunman.id])
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(FRANK_PED_ID(), TRUE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_goon].id, TRUE)
					
					// Equip Crew
					GIVE_CREW_MEMBER_WEAPON(cmf_mike, TRUE)
					GIVE_CREW_MEMBER_WEAPON(cmf_frank, TRUE)
					GIVE_CREW_MEMBER_WEAPON(cmf_goon, TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_mike], TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_frank], TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_goon], TRUE)
					SET_CURRENT_PED_WEAPON(FRANK_PED_ID(), weap_Frank, TRUE)
					SET_CURRENT_PED_WEAPON(peds[mpf_goon].id, weap_Goon, TRUE)
					
					SET_PED_USING_ACTION_MODE(MIKE_PED_ID(), 		TRUE)
					SET_PED_USING_ACTION_MODE(FRANK_PED_ID(), 		TRUE)
					SET_PED_USING_ACTION_MODE(peds[mpf_goon].id,	TRUE, -1, "DEFAULT_ACTION")

					PLAY_ALARM_SECURITY(TRUE, TRUE)
					PLAY_ALARM_FIRE(TRUE, TRUE)
					Unload_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B)
					Unload_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B_02)
					
					SWITCH crewHacker
						CASE CM_HACKER_G_PAIGE
							iDesiredSprinklerState = 0
						BREAK
						CASE CM_HACKER_M_CHRIS
							IF IS_REPLAY_IN_PROGRESS()
							AND g_replay.iReplayInt[0] != 0
								iDesiredSprinklerState = g_replay.iReplayInt[0]-1
							ELSE
								iDesiredSprinklerState = 0
							ENDIF
						BREAK
						CASE CM_HACKER_B_RICKIE_UNLOCK
							IF IS_REPLAY_IN_PROGRESS()
							AND g_replay.iReplayInt[0] != 0
								iDesiredSprinklerState = g_replay.iReplayInt[0]-1
							ELSE
								iDesiredSprinklerState = 1
							ENDIF
						BREAK
					ENDSWITCH
					Event_Trigger(mef_manage_sprinklers)
					
					//CREATE_MODEL_HIDE(<< 136.1795, -750.7010, 262.0516 >>, 10.0, V_ILEV_FIB_ATRCOL, TRUE)
					SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_ROOF, BUILDINGSTATE_CLEANUP)
					
					DOORS_LockOpenNonAnimDoors()
					
					SETUP_FIB_COVERPOINTS(TRUE)
					
					PREP_HOTSWAP(FALSE, FALSE, TRUE)
					Event_Trigger(mef_manage_switch)
					
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)

					IS_SKIP_OK_TO_FADE_IN_NOW()
					IF IS_REPLAY_BEING_SET_UP()
						SETTIMERA(0)
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Start END_REPLAY_SETUP() stating timer A")
						END_REPLAY_SETUP()
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Finish END_REPLAY_SETUP() @ ", TIMERA())
					ENDIF
					
					SET_CLOCK_TIME(iStartHours+3, 0, 0)
					Event_Trigger(mef_manage_clock)
					
					ptfxs[ptfx_sweat_shop] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_agency3b_blding_smoke", vSweatShopSmokeCoord, vSweatShopSmokeRot, fSweatShopSmokeScale, FALSE, FALSE, FALSE)
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_DESTROYED ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_DESTROYED )
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					
					Event_Trigger(mef_manage_smoke_vfx)
					//Event_Trigger(mef_manage_fire_meshes)
					Event_Trigger(mef_ig7_elevator)
					
					PRINT_NOW("A3B_ESCAPE", DEFAULT_GOD_TEXT_TIME, 1)
					
					TRIGGER_MUSIC_EVENT("AH3B_BURNTOUT_TWO_RT")
				BREAK
				CASE msf_9_abseil_down
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					PLAY_SOUND_FROM_COORD(sounds[sfx_distant_sirens_1], "Distant_Sirens_Rubble", <<224.5, -627.0, 240.3>>, "FBI_HEIST_FINALE_CHOPPER")
					DISTANT_COP_CAR_SIRENS(TRUE)
				
					

					Set_Current_Player_Ped(SELECTOR_PED_MICHAEL, TRUE)
					SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH, FALSE)
					IF Create_Crew_Member(peds[mpf_frank], cmf_frank, 	<<166.4286, -761.4565, 245.2417>>, 	260.0782, 	CREW_OUTFIT_STEALTH, 	DEFAULT, TRUE, TRUE, FALSE)
						FREEZE_ENTITY_POSITION(peds[mpf_frank].id, TRUE)
					ENDIF
					IF Create_Crew_Member(peds[mpf_goon], cmf_goon, 	<<165.4856, -763.9792, 245.2417>>, 	267.4155, 	CREW_OUTFIT_STEALTH, 	DEFAULT, TRUE, TRUE, FALSE)
						FREEZE_ENTITY_POSITION(peds[mpf_goon].id, TRUE)
					ENDIF
					SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_HARNESS, FALSE)
					SET_PED_COMP_ITEM_CURRENT_SP(FRANK_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P1_HARNESS, FALSE)
					SET_PED_COMPONENT_VARIATION(peds[mpf_goon].id, PED_COMP_BERD, 0, 0)
					
					Mission_Checks(TRUE)
					
					// Equip Crew
					GIVE_CREW_MEMBER_WEAPON(cmf_mike, TRUE)
					GIVE_CREW_MEMBER_WEAPON(cmf_frank, TRUE)
					GIVE_CREW_MEMBER_WEAPON(cmf_goon, TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_mike], TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_frank], TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_goon], TRUE)
					SET_CURRENT_PED_WEAPON(FRANK_PED_ID(), weap_Frank, TRUE)
					SET_CURRENT_PED_WEAPON(peds[mpf_goon].id, weap_Goon, TRUE)
					
					SET_PED_USING_ACTION_MODE(MIKE_PED_ID(), 		TRUE)
					SET_PED_USING_ACTION_MODE(FRANK_PED_ID(), 		TRUE)
					SET_PED_USING_ACTION_MODE(peds[mpf_goon].id,	TRUE, -1, "DEFAULT_ACTION")
					
					PLAY_ALARM_SECURITY(TRUE, TRUE)
					PLAY_ALARM_FIRE(TRUE, TRUE)
					Unload_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B)
					Unload_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B_02)
					
					PREP_HOTSWAP(FALSE, FALSE, TRUE)
					Event_Trigger(mef_manage_switch)
					
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
					
					IS_SKIP_OK_TO_FADE_IN_NOW()
					IF IS_REPLAY_BEING_SET_UP()
						SETTIMERA(0)
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Start END_REPLAY_SETUP() stating timer A")
						END_REPLAY_SETUP()
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Finish END_REPLAY_SETUP() @ ", TIMERA())
					ENDIF
					
					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
					CLEAR_AREA_OF_VEHICLES(<<267.7761, -561.6590, 49.8593>>, 5000)
					Event_Trigger(mef_emergency_services)
					
					SET_CLOCK_TIME(iStartHours+3, 30, 0)
					Event_Trigger(mef_manage_clock)
					
					ptfxs[ptfx_sweat_shop] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_agency3b_blding_smoke", vSweatShopSmokeCoord, vSweatShopSmokeRot, fSweatShopSmokeScale, FALSE, FALSE, FALSE)
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_DESTROYED ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_DESTROYED )
					
					IF DOES_ENTITY_EXIST(peds[mpf_mike].id)
					AND NOT IS_PED_INJURED(peds[mpf_mike].id)
						FREEZE_ENTITY_POSITION(peds[mpf_mike].id, FALSE)
						CLEAR_ROOM_FOR_ENTITY(peds[mpf_mike].id)
					ENDIF
					IF DOES_ENTITY_EXIST(peds[mpf_frank].id)
					AND NOT IS_PED_INJURED(peds[mpf_frank].id)
						FREEZE_ENTITY_POSITION(peds[mpf_frank].id, FALSE)
						CLEAR_ROOM_FOR_ENTITY(peds[mpf_frank].id)
					ENDIF
					IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
					AND NOT IS_PED_INJURED(peds[mpf_goon].id)
						FREEZE_ENTITY_POSITION(peds[mpf_goon].id, FALSE)
						CLEAR_ROOM_FOR_ENTITY(peds[mpf_goon].id)
					ENDIF
					
					RAPPEL_Manage_1st_Descent_Player()
					RAPPEL_Manage_1st_Descent_Buddies()
					RAPPEL_Manage_Cams_1st_Descent()
					
					TRIGGER_MUSIC_EVENT("AH3B_RAPPEL_RT")
				BREAK
				CASE msf_10_get_to_getaway_veh
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					PLAY_SOUND_FROM_COORD(sounds[sfx_distant_sirens_1], "Distant_Sirens_Rubble", <<224.5, -627.0, 240.3>>, "FBI_HEIST_FINALE_CHOPPER")
					DISTANT_COP_CAR_SIRENS(TRUE)
					
					
					
					Set_Current_Player_Ped(SELECTOR_PED_MICHAEL, TRUE, FALSE)
					SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH, FALSE)
					Create_Crew_Member(peds[mpf_frank], cmf_frank, <<153.2881, -723.7296, 46.0770>>, 	69.0961, 	CREW_OUTFIT_STEALTH, 	DEFAULT, TRUE, FALSE, FALSE)
					Create_Crew_Member(peds[mpf_goon], cmf_goon, 	<<134.3783, -721.2941, 46.0770>>, 	70.2955, 	CREW_OUTFIT_STEALTH, 	DEFAULT, TRUE, FALSE, FALSE)
					SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_HARNESS, FALSE)
					SET_PED_COMP_ITEM_CURRENT_SP(FRANK_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P1_HARNESS, FALSE)
					SET_PED_COMPONENT_VARIATION(peds[mpf_goon].id, PED_COMP_BERD, 0, 0)
					
					IF GET_DRIVER_SKILL_LEVEL() = CMSK_GOOD
						vehs[mvf_escape].id = CREATE_VEHICLE(AMBULANCE, <<60.49, -734.55, 43.88>>, 342.44)
						SET_VEHICLE_CHEAT_POWER_INCREASE(vehs[mvf_escape].id, 0.2)
						SET_VEHICLE_AS_RESTRICTED(vehs[mvf_escape].id, 1)
						SET_VEHICLE_HAS_STRONG_AXLES(vehs[mvf_escape].id, TRUE)
						SET_VEHICLE_TYRES_CAN_BURST(vehs[mvf_escape].id, FALSE)
						SET_VEHICLE_STRONG(vehs[mvf_escape].id, TRUE)
						CREATE_CREW_MEMBER_IN_VEHICLE(peds[mpf_driver], cmf_driver, vehs[mvf_escape], VS_FRONT_RIGHT, CREW_OUTFIT_STEALTH, GET_CREW_MEMBER_ACCURACY(crewDriver), TRUE, TRUE)
						GIVE_CREW_MEMBER_WEAPON(cmf_driver, FALSE)
						SET_VEHICLE_SIREN(vehs[mvf_escape].id, TRUE)
						SET_SIREN_WITH_NO_DRIVER(vehs[mvf_escape].id, TRUE)
						Unload_Asset_Model(sAssetData, AMBULANCE)
						iTimeDriverArrived = GET_GAME_TIMER()
						bDriverHasArrived = TRUE
					ENDIF
					
					Mission_Checks(TRUE)
					
					vehs[mvf_police_car_1].id = CREATE_VEHICLE(POLICE3, <<88.90, -647.90, 43.83>>, 226.86)
					vehs[mvf_police_car_2].id = CREATE_VEHICLE(POLICE3, <<65.3005, -672.6956, 43.2852>>, 192.2619)// <<81.28, -670.21, 43.81>>, 85.27)
					vehs[mvf_firetruck_1].id = CREATE_VEHICLE(FIRETRUK, <<81.48, -661.66, 44.32>>, 132.13)
					vehs[mvf_firetruck_2].id = CREATE_VEHICLE(FIRETRUK, <<78.87, -684.03, 44.18>>, 324.12)
					Unload_Asset_Model(sAssetData, POLICE3)
					Unload_Asset_Model(sAssetData, FIRETRUK)
					SET_VEHICLE_SIREN(vehs[mvf_police_car_1].id, TRUE)
					SET_VEHICLE_SIREN(vehs[mvf_police_car_2].id, TRUE)
					SET_VEHICLE_SIREN(vehs[mvf_firetruck_1].id, TRUE)
					SET_VEHICLE_SIREN(vehs[mvf_firetruck_2].id, TRUE)
					
					// Equip Crew
					GIVE_CREW_MEMBER_WEAPON(cmf_mike, TRUE)
					GIVE_CREW_MEMBER_WEAPON(cmf_frank, TRUE)
					GIVE_CREW_MEMBER_WEAPON(cmf_goon, TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_mike], TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_frank], TRUE)
					SET_PED_FLASH_LIGHT(peds[mpf_goon], TRUE)
					SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), weap_Mike, TRUE)
					SET_CURRENT_PED_WEAPON(FRANK_PED_ID(), weap_Frank, TRUE)
					SET_CURRENT_PED_WEAPON(peds[mpf_goon].id, weap_Goon, TRUE)
					
					SET_PED_USING_ACTION_MODE(MIKE_PED_ID(), 		TRUE)
					SET_PED_USING_ACTION_MODE(FRANK_PED_ID(), 		TRUE)
					SET_PED_USING_ACTION_MODE(peds[mpf_goon].id,	TRUE, -1, "DEFAULT_ACTION")
					
					Disable_Roads_And_Paths_Around_FIB_Building(TRUE)
					
					PLAY_ALARM_SECURITY(TRUE, TRUE)
					PLAY_ALARM_FIRE(TRUE, TRUE)
					Unload_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B)
					Unload_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B_02)
					
					PREP_HOTSWAP(FALSE, FALSE, TRUE)
					Event_Trigger(mef_manage_switch)
					
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)

					IF IS_REPLAY_BEING_SET_UP()
						SETTIMERA(0)
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Start END_REPLAY_SETUP() stating timer A")
						END_REPLAY_SETUP()
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Finish END_REPLAY_SETUP() @ ", TIMERA())
					ENDIF
					IS_SKIP_OK_TO_FADE_IN_NOW()
					
					Create_Ped_Cover_Point(peds[mpf_mike], <<144.6043, -720.4959, 46.0770>>, 71.7143, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
					TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_mike].id, <<144.6043, -720.4959, 46.0770>>, -1, FALSE, 0, TRUE, TRUE, peds[mpf_mike].cov, FALSE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_mike].id, TRUE)
					Create_Ped_Cover_Point(peds[mpf_frank], <<153.2881, -723.7296, 46.0770>>, 69.0961, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
					TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_frank].id, <<153.2881, -723.7296, 46.0770>>, -1, FALSE, 0, TRUE, TRUE, peds[mpf_frank].cov, FALSE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_frank].id, TRUE)
					Create_Ped_Cover_Point(peds[mpf_goon], <<134.3783, -721.2941, 46.0770>>, 70.2955, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120)
					TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_goon].id, <<134.3783, -721.2941, 46.0770>>, -1, FALSE, 0, TRUE, FALSE, peds[mpf_goon].cov, FALSE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_goon].id, TRUE)
					
					SET_CLOCK_TIME(iStartHours+4, 0, 0)
					Event_Trigger(mef_manage_clock)
					
					ptfxs[ptfx_sweat_shop] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_agency3b_blding_smoke", vSweatShopSmokeCoord, vSweatShopSmokeRot, fSweatShopSmokeScale, FALSE, FALSE, FALSE)
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_DESTROYED ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_DESTROYED )
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(-90)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					
					Event_Trigger(mef_ground_agents)
					Event_Trigger(mef_getaway_driver_arrival)
					
					TRIGGER_MUSIC_EVENT("AH3B_GET_TO_VAN_RT")
				BREAK
				CASE msf_11_escape_to_franklins
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					PLAY_SOUND_FROM_COORD(sounds[sfx_distant_sirens_1], "Distant_Sirens_Rubble", <<224.5, -627.0, 240.3>>, "FBI_HEIST_FINALE_CHOPPER")
					DISTANT_COP_CAR_SIRENS(TRUE)
					
					
					
					IF GET_DRIVER_SKILL_LEVEL() = CMSK_GOOD
						vehs[mvf_escape].id = CREATE_VEHICLE(AMBULANCE, <<60.49, -734.55, 43.88>>, 342.44)
						SET_VEHICLE_SIREN(vehs[mvf_escape].id, TRUE)
						SET_SIREN_WITH_NO_DRIVER(vehs[mvf_escape].id, TRUE)
						Unload_Asset_Model(sAssetData, AMBULANCE)
						Event_Trigger(mef_swat_fib_arrival)
						
						TRIGGER_MUSIC_EVENT("AH3B_NO_WANTED_ESCAPE_RT")
						bScorePlaying = FALSE
					ELSE
						vehs[mvf_escape].id = CREATE_VEHICLE(mod_esc_veh, <<60.49, -734.55, 43.88>>, 342.44)
						
						vehs[mvf_late_fib_1].id = CREATE_VEHICLE(FBI2, <<50.184, -755.6543, 43.8408>>, -9.94)
						SET_VEHICLE_DOOR_OPEN(vehs[mvf_late_fib_1].id, SC_DOOR_FRONT_LEFT, TRUE, TRUE)
						SET_VEHICLE_DOOR_OPEN(vehs[mvf_late_fib_1].id, SC_DOOR_FRONT_RIGHT, TRUE, TRUE)
						SET_VEHICLE_DOOR_OPEN(vehs[mvf_late_fib_1].id, SC_DOOR_REAR_LEFT, TRUE, TRUE)
						SET_VEHICLE_DOOR_OPEN(vehs[mvf_late_fib_1].id, SC_DOOR_REAR_RIGHT, TRUE, TRUE)
						
						vehs[mvf_late_fib_2].id = CREATE_VEHICLE(FBI2, <<43.9931, -728.7858, 43.9026>>, -147.94)
						SET_VEHICLE_DOOR_OPEN(vehs[mvf_late_fib_2].id, SC_DOOR_FRONT_LEFT, TRUE, TRUE)
						SET_VEHICLE_DOOR_OPEN(vehs[mvf_late_fib_2].id, SC_DOOR_FRONT_RIGHT, TRUE, TRUE)
						SET_VEHICLE_DOOR_OPEN(vehs[mvf_late_fib_2].id, SC_DOOR_REAR_LEFT, TRUE, TRUE)
						SET_VEHICLE_DOOR_OPEN(vehs[mvf_late_fib_2].id, SC_DOOR_REAR_RIGHT, TRUE, TRUE)
						
						Unload_Asset_Model(sAssetData, FBI2)
						
						SET_WANTED_LEVEL_MULTIPLIER(1.0)
						SET_MAX_WANTED_LEVEL(4)
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 4)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
						SET_CREATE_RANDOM_COPS(TRUE)
						ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, TRUE)
						ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, TRUE)
						ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, TRUE) 
						ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, TRUE)
						ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, TRUE)
						ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, TRUE)
						ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, TRUE)
						ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, TRUE)
						Event_Trigger(mef_manage_wanted_level)
						
						TRIGGER_MUSIC_EVENT("AH3B_EVADE_COPS_RT")
						bScorePlaying = TRUE
					ENDIF
					
					SET_VEHICLE_AS_RESTRICTED(vehs[mvf_escape].id, 1)
					SET_VEHICLE_CHEAT_POWER_INCREASE(vehs[mvf_escape].id, 0.2)
					SET_VEHICLE_HAS_STRONG_AXLES(vehs[mvf_escape].id, TRUE)
					SET_VEHICLE_TYRES_CAN_BURST(vehs[mvf_escape].id, FALSE)
					SET_VEHICLE_STRONG(vehs[mvf_escape].id, TRUE)
					SET_VEHICLE_ENGINE_ON(vehs[mvf_escape].id, TRUE, TRUE)						
					bDriverHasArrived = TRUE
					
					Set_Current_Player_Ped(SELECTOR_PED_MICHAEL, TRUE, FALSE)
					SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH, FALSE)
					CREATE_CREW_MEMBER_IN_VEHICLE(peds[mpf_frank], 	cmf_frank, 	vehs[mvf_escape], 	VS_BACK_LEFT, 	CREW_OUTFIT_STEALTH, 	40, TRUE, FALSE, FALSE)
					CREATE_CREW_MEMBER_IN_VEHICLE(peds[mpf_goon], 	cmf_goon, 	vehs[mvf_escape], 	VS_BACK_RIGHT, 	CREW_OUTFIT_STEALTH, 	40, TRUE, FALSE, FALSE)
					CREATE_CREW_MEMBER_IN_VEHICLE(peds[mpf_driver], cmf_driver, vehs[mvf_escape], 	VS_FRONT_RIGHT, CREW_OUTFIT_STEALTH, 	GET_CREW_MEMBER_ACCURACY(crewDriver), TRUE, FALSE)
					SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P0_HARNESS, FALSE)
					SET_PED_COMP_ITEM_CURRENT_SP(FRANK_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P1_HARNESS, FALSE)
					SET_PED_COMPONENT_VARIATION(peds[mpf_goon].id, PED_COMP_BERD, 0, 0)
					
					Mission_Checks(TRUE)
					
					vehs[mvf_police_car_1].id = CREATE_VEHICLE(POLICE3, <<88.90, -647.90, 43.83>>, 226.86)
					vehs[mvf_police_car_2].id = CREATE_VEHICLE(POLICE3, <<65.3005, -672.6956, 43.2852>>, 192.2619)// <<81.28, -670.21, 43.81>>, 85.27)
					vehs[mvf_firetruck_1].id = CREATE_VEHICLE(FIRETRUK, <<81.48, -661.66, 44.32>>, 132.13)
					vehs[mvf_firetruck_2].id = CREATE_VEHICLE(FIRETRUK, <<78.87, -684.03, 44.18>>, 324.12)
					Unload_Asset_Model(sAssetData, POLICE3)
					Unload_Asset_Model(sAssetData, FIRETRUK)
					SET_VEHICLE_SIREN(vehs[mvf_police_car_1].id, TRUE)
					SET_VEHICLE_SIREN(vehs[mvf_police_car_2].id, TRUE)
					SET_VEHICLE_SIREN(vehs[mvf_firetruck_1].id, TRUE)
					SET_VEHICLE_SIREN(vehs[mvf_firetruck_2].id, TRUE)
					
					// Equip Crew
					GIVE_CREW_MEMBER_WEAPON(cmf_mike, FALSE)
					GIVE_CREW_MEMBER_WEAPON(cmf_frank, FALSE)
					GIVE_CREW_MEMBER_WEAPON(cmf_goon, FALSE)
					GIVE_CREW_MEMBER_WEAPON(cmf_driver, FALSE)
					SET_PED_FLASH_LIGHT(peds[mpf_mike], FALSE)
					SET_PED_FLASH_LIGHT(peds[mpf_frank], FALSE)
					SET_PED_FLASH_LIGHT(peds[mpf_goon], FALSE)
					SET_PED_FLASH_LIGHT(peds[mpf_driver], FALSE)
					SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), weap_Mike, TRUE)
					SET_CURRENT_PED_WEAPON(FRANK_PED_ID(), weap_Frank, TRUE)
					SET_CURRENT_PED_WEAPON(peds[mpf_goon].id, weap_Goon, TRUE)
					SET_CURRENT_PED_WEAPON(peds[mpf_driver].id, weap_Driver, TRUE)
					
					SET_PED_USING_ACTION_MODE(MIKE_PED_ID(), 		TRUE)
					SET_PED_USING_ACTION_MODE(FRANK_PED_ID(), 		TRUE)
					SET_PED_USING_ACTION_MODE(peds[mpf_goon].id,	TRUE, -1, "DEFAULT_ACTION")
					SET_PED_USING_ACTION_MODE(peds[mpf_driver].id,	TRUE, -1, "DEFAULT_ACTION")
					
					Disable_Roads_And_Paths_Around_FIB_Building(TRUE)
					
					PLAY_ALARM_SECURITY(TRUE, TRUE)
					PLAY_ALARM_FIRE(TRUE, TRUE)
					Unload_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B)
					Unload_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B_02)
					
					SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
					
					IF IS_REPLAY_BEING_SET_UP()
						SETTIMERA(0)
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Start END_REPLAY_SETUP() stating timer A")
						END_REPLAY_SETUP(vehs[mvf_escape].id, VS_DRIVER)
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Finish END_REPLAY_SETUP() @ ", TIMERA())
					ELSE
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehs[mvf_escape].id, VS_DRIVER)
					ENDIF
					IS_SKIP_OK_TO_FADE_IN_NOW()
					
					SET_CLOCK_TIME(iStartHours+4, 30, 0)
					Event_Trigger(mef_manage_clock)
					
					ptfxs[ptfx_sweat_shop] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_agency3b_blding_smoke", vSweatShopSmokeCoord, vSweatShopSmokeRot, fSweatShopSmokeScale, FALSE, FALSE, FALSE)
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_DESTROYED ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_DESTROYED )
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				BREAK
				CASE msf_12_mission_passed
					SET_WANTED_LEVEL_MULTIPLIER(0.0)
					SET_MAX_WANTED_LEVEL(0)
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					
					
					Set_Current_Player_Ped(SELECTOR_PED_MICHAEL, TRUE)
					
					IF IS_REPLAY_BEING_SET_UP()
						SETTIMERA(0)
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Start END_REPLAY_SETUP() stating timer A")
						END_REPLAY_SETUP()
						CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Finish END_REPLAY_SETUP() @ ", TIMERA())
					ENDIF
					
					IS_SKIP_OK_TO_FADE_IN_NOW()
					
					SET_CLOCK_TIME(iStartHours+4, 30, 0)

					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
					
					bIgnoreFade = TRUE
				BREAK
			ENDSWITCH
			
			CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Skip finished")
			
			Add_Peds_For_Dialogue()
			
			IF NOT bIgnoreFade
				IF IS_SCREEN_FADED_OUT()
				OR (NOT IS_SCREEN_FADING_IN())
					CPRINTLN(DEBUG_MIKE, "MISSION_STAGE_SKIP() Fading in")
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
			ENDIF
			bDoSkip = FALSE
		ENDIF
#IF IS_DEBUG_BUILD
	// Check is a skip is being asked for, and dont allow skip during setup stage
	ELIF IS_SCREEN_FADED_IN()
	AND NOT IS_CUTSCENE_PLAYING()
	
		INT iCurrentStage
		INT iReturnStage
		
		IF TRANSLATE_TO_Z_MENU(zMenuNames, mission_stage, iCurrentStage)
	
			DONT_DO_J_SKIP(locates_data)
			IF LAUNCH_MISSION_STAGE_MENU(zMenuNames, iReturnStage, iCurrentStage, FALSE, "Agency Heist 3B", FALSE, TRUE, DEFAULT, DEFAULT, DEFAULT, TRUE)
							
				iSkipToStage = TRANSLATE_FROM_Z_MENU(zMenuNames, iReturnStage)
				
				IF IS_CUTSCENE_ACTIVE()
					STOP_CUTSCENE()
					REMOVE_CUTSCENE()
				ENDIF
				
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				bDoSkip = TRUE
			ELSE
				DEBUG_CHECK_FOR_FORCED_PASS_FAIL()
			ENDIF	
			 
		ENDIF
#ENDIF
	ENDIF
	
ENDPROC
	
/*
								 _______  _   ______  ______  _  _______  _______    _______  _       _______  _  _  _ 
								(_______)| | / _____)/ _____)| |(_______)(_______)  (_______)(_)     (_______)(_)(_)(_)
								 _  _  _ | |( (____ ( (____  | | _     _  _     _    _____    _       _     _  _  _  _ 
								| ||_|| || | \____ \ \____ \ | || |   | || |   | |  |  ___)  | |     | |   | || || || |
								| |   | || | _____) )_____) )| || |___| || |   | |  | |      | |_____| |___| || || || |
								|_|   |_||_|(______/(______/ |_| \_____/ |_|   |_|  |_|      |_______)\_____/  \_____/ 
*/


PROC ST_0_Intro_Cutscene()
	SWITCH mission_substage
		CASE STAGE_ENTRY

			IF HAS_CUTSCENE_LOADED()
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
					peds[mpf_lester].id = g_sTriggerSceneAssets.ped[0]
					SET_ENTITY_AS_MISSION_ENTITY(peds[mpf_lester].id, TRUE, TRUE)
					REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_lester].id, "Lester", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(null, "Lester", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_LESTERCREST)
				ENDIF
				
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
					objs[mof_walking_stick].id = g_sTriggerSceneAssets.object[0]
					SET_ENTITY_AS_MISSION_ENTITY(objs[mof_walking_stick].id, TRUE, TRUE)
					IF IS_ENTITY_ATTACHED(objs[mof_walking_stick].id)
						DETACH_ENTITY(objs[mof_walking_stick].id)
					ENDIF
					REGISTER_ENTITY_FOR_CUTSCENE(objs[mof_walking_stick].id, "WalkingStick_Lester", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(null, "WalkingStick_Lester", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PROP_CS_WALKING_STICK)
				ENDIF
				
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[1])
					objs[mof_lester_crate].id = g_sTriggerSceneAssets.object[1]
					SET_ENTITY_AS_MISSION_ENTITY(objs[mof_lester_crate].id, TRUE, TRUE)
					FREEZE_ENTITY_POSITION(objs[mof_lester_crate].id, FALSE)
					REGISTER_ENTITY_FOR_CUTSCENE(objs[mof_lester_crate].id, "Lesters_Crate", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(null, "Lesters_Crate", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PROP_CS_LESTER_CRATE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
					vehs[mvf_mikesCar].id = g_sTriggerSceneAssets.veh[0]
					SET_ENTITY_AS_MISSION_ENTITY(vehs[mvf_mikesCar].id, TRUE, TRUE)
				ENDIF
				
				IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
					
					peds[mpf_mike].id = PLAYER_PED_ID()
					REGISTER_ENTITY_FOR_CUTSCENE(null, "Franklin", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
					
				ELIF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_FRANKLIN
					
					peds[mpf_frank].id = PLAYER_PED_ID()
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
						peds[mpf_mike].id = g_sTriggerSceneAssets.ped[1]
						SET_ENTITY_AS_MISSION_ENTITY(peds[mpf_mike].id, TRUE, TRUE)
						REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_mike].id, "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(null, "Michael", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
					ENDIF
					
				ENDIF
				
				bExitStateMichael	= FALSE
				bExitStateFrank		= FALSE
				bExitStateLester	= FALSE
				bExitStateCam		= FALSE
			
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				IF IS_SCREEN_FADED_OUT()
					SET_CUTSCENE_FADE_VALUES(FALSE, TRUE, FALSE, FALSE)
				ELSE
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
				ENDIF
				
				MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_AGENCY_3B)
				
				Load_Asset_AnimDict(sAssetData, "missheistfbi3bleadinoutah_3b_int")
				
				mission_substage++
			ENDIF
		BREAK
		CASE 1
			IF IS_CUTSCENE_PLAYING()
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				IF IS_GAMEPLAY_HINT_ACTIVE()
					STOP_GAMEPLAY_HINT()
				ENDIF
				
				DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_MICHAEL)
			
				//SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<727.6439, -987.9505, 23.1625>>, 277.8401)
				RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(
					<<750.695496,-983.915283,22.780117>>, <<712.441284,-984.450073,26.998360>>, 16.562500, 
					<<727.6439, -987.9505, 23.1625>>, 277.8401, TRUE, TRUE, TRUE)
			
				CLEAR_AREA(<<715.381592,-964.857117,29.395262>>, 15.062500, TRUE)
				
				IF DOES_ENTITY_EXIST(vehs[mvf_mikesCar].id)
				AND IS_VEHICLE_DRIVEABLE(vehs[mvf_mikesCar].id)
					SET_ENTITY_COORDS(vehs[mvf_mikesCar].id, vStartDriveCarCoord)
					SET_ENTITY_HEADING(vehs[mvf_mikesCar].id, fStartDriveCarHeading)
				ENDIF
				
				CLEAR_PRINTS()
				CLEAR_HELP()
				
				mission_substage++
			ENDIF
		BREAK
		CASE 2
			ENTITY_INDEX tempEntity
			
			IF NOT DOES_ENTITY_EXIST(MIKE_PED_ID())
				tempEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael")
				IF DOES_ENTITY_EXIST(tempEntity)
					peds[mpf_mike].id = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
				ENDIF
			ENDIF

			IF NOT DOES_ENTITY_EXIST(FRANK_PED_ID())
				tempEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklin")
				IF DOES_ENTITY_EXIST(tempEntity)
					peds[mpf_frank].id = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(peds[mpf_lester].id)
				tempEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester")
				IF DOES_ENTITY_EXIST(tempEntity)
					peds[mpf_lester].id = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(objs[mof_walking_stick].id)
				tempEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("WalkingStick_Lester")
				IF DOES_ENTITY_EXIST(tempEntity)
					objs[mof_walking_stick].id = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(tempEntity)
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(objs[mof_lester_crate].id)
				tempEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lesters_Crate")
				IF DOES_ENTITY_EXIST(tempEntity)
					objs[mof_lester_crate].id = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(tempEntity)
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(vehs[mvf_mikesCar].id)
				IF CREATE_PLAYER_VEHICLE(vehs[mvf_mikesCar].id, CHAR_MICHAEL, vStartDriveCarCoord, fStartDriveCarHeading)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(MIKE_PED_ID())
			AND DOES_ENTITY_EXIST(FRANK_PED_ID())
			AND DOES_ENTITY_EXIST(peds[mpf_lester].id)
			AND DOES_ENTITY_EXIST(vehs[mvf_mikesCar].id)
			AND DOES_ENTITY_EXIST(objs[mof_walking_stick].id)
			AND DOES_ENTITY_EXIST(objs[mof_lester_crate].id)
				
				sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] 	= FRANK_PED_ID()
				sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] 	= MIKE_PED_ID()
			
				mission_substage++
			ENDIF
		BREAK
		CASE 3
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lester")				
			
				SET_PED_RELATIONSHIP_GROUP_HASH(peds[mpf_lester].id, REL_CREW)
				ATTACH_ENTITY_TO_ENTITY(objs[mof_lester_crate].id, peds[mpf_lester].id, GET_PED_BONE_INDEX(peds[mpf_lester].id, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>)
				ATTACH_ENTITY_TO_ENTITY(objs[mof_walking_stick].id, peds[mpf_lester].id, GET_PED_BONE_INDEX(peds[mpf_lester].id, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
				TASK_PLAY_ANIM_ADVANCED(peds[mpf_lester].id, "missheistfbi3bleadinoutah_3b_int", "_leadout_b_lester", << 713.033, -964.717, 30.399 >>, << -0.000, 0.000, 42.000 >>, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_lester].id)
			
				bExitStateLester = TRUE
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				
				REPLAY_STOP_EVENT()
				
				SET_ENTITY_COORDS(MIKE_PED_ID(), <<718.1644, -965.1974, 29.3956>>)
				SET_ENTITY_HEADING(MIKE_PED_ID(), 182.4597)
				
				IF PLAYER_PED_ID() = MIKE_PED_ID()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000)
				ELSE
					TASK_ENTER_VEHICLE(MIKE_PED_ID(), vehs[mvf_mikesCar].id, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK)
				ENDIF
				
				FORCE_PED_MOTION_STATE(MIKE_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)

				bExitStateMichael = TRUE
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
			
				SET_ENTITY_COORDS(FRANK_PED_ID(), <<716.7049, -964.2289, 29.3956>>)
				SET_ENTITY_HEADING(FRANK_PED_ID(), 182.7453)
				FORCE_PED_MOTION_STATE(FRANK_PED_ID(), MS_ON_FOOT_IDLE, FALSE, FAUS_CUTSCENE_EXIT)
				
				IF PLAYER_PED_ID() = FRANK_PED_ID()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ELSE
					TASK_ENTER_VEHICLE(FRANK_PED_ID(), vehs[mvf_mikesCar].id, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK)
				ENDIF
				
				bExitStateFrank = TRUE
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				bExitStateCam = TRUE
			ENDIF
			
			IF bExitStateMichael
			AND bExitStateFrank
			AND bExitStateLester
			AND bExitStateCam
			AND HAS_ANIM_DICT_LOADED("missheistfbi3bleadinoutah_3b_int")
			AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			AND HAS_ADDITIONAL_TEXT_LOADED(MINIGAME_TEXT_SLOT)
			AND NOT IS_SCREEN_FADING_OUT()
			
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				Mission_Set_Stage(msf_1_go_to_helipad)
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC 

//PURPOSE: Player travels to the helipad.
PROC ST_1_Go_To_Helipad()

	SWITCH mission_substage
		CASE STAGE_ENTRY
			SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY", 30)
			SET_LOCATES_BUDDIES_TO_GET_INTO_NEAREST_CAR(locates_Data, vehs[mvf_mikesCar].id)
			
			DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_SWEATSHOP_L), DOORSTATE_UNLOCKED)
			DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_SWEATSHOP_R), DOORSTATE_UNLOCKED)
			
			SET_DOOR_STATE(DOORNAME_NOSE_REAR_L, DOORSTATE_LOCKED)
			SET_DOOR_STATE(DOORNAME_NOSE_REAR_L, DOORSTATE_LOCKED)
			DOORS_Set_State(mdf_helipad, TRUE, 0.0)
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "1. Go to Helipad")
			
			Add_Peds_For_Dialogue()
			ADD_PED_FOR_DIALOGUE(sConvo, 3, peds[mpf_lester].id, "Lester")
			
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
			
			i_dialogue_stage = 0
			mission_substage++
		BREAK
		CASE 1
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(sConvo, strDialogue, "AH3B_INTb", CONV_PRIORITY_HIGH)
					mission_substage++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			TEXT_LABEL_15 str_BuddyLeaveBehind
			IF BUDDY_PED_ID() = MIKE_PED_ID()
				str_BuddyLeaveBehind = "CMN_MLEAVE"
			ELSE
				str_BuddyLeaveBehind = "CMN_FLEAVE"
			ENDIF
			
			IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_ANY_MEANS(locates_Data,
					vHeliPadCoord, << LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_ANY_MEANS, LOCATE_SIZE_HEIGHT >>,
					TRUE, BUDDY_PED_ID(), "A3B_HELI1", str_BuddyLeaveBehind, FALSE, TRUE, TRUE)
					
				KILL_FACE_TO_FACE_CONVERSATION()
				
				PRELOAD_OUTFIT(MIKE_PED_ID(), OUTFIT_P0_STEALTH)
				PRELOAD_OUTFIT(FRANK_PED_ID(), OUTFIT_P1_STEALTH)
				PRELOAD_PED_COMP(FRANK_PED_ID(), COMP_TYPE_SPECIAL, SPECIAL_P1_PARACHUTE)
				
				REPLAY_RECORD_BACK_FOR_TIME(10.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
				
				mission_substage++
				
			// Manage everything while on way to locale (goverment building)
			ELSE
			
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND IS_PED_IN_VEHICLE(BUDDY_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				
					IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_GET_TO_FACILITY")
						START_AUDIO_SCENE("AH_3B_GET_TO_FACILITY")
					ENDIF
				
					SWITCH i_dialogue_stage
						CASE 0
							IF IS_SAFE_TO_START_CONVERSATION()
								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_GO", CONV_PRIORITY_HIGH)
									i_dialogue_stage++
								ENDIF
							ENDIF
						BREAK
						CASE 1
							IF IS_SAFE_TO_START_CONVERSATION()
								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_GOING", CONV_PRIORITY_HIGH)
									i_dialogue_stage++
								ENDIF
							ENDIF
						BREAK
						CASE 2
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2557.087891,-459.128021,107.909950>>, <<2601.630371,-596.806519,61.728104>>, 104.312500)
								KILL_FACE_TO_FACE_CONVERSATION()
								i_dialogue_stage++
							ENDIF
						BREAK
						CASE 3
							IF IS_SAFE_TO_START_CONVERSATION()
								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_RADin", CONV_PRIORITY_HIGH)
									str_dialogue_root = ""
									i_dialogue_stage++
								ENDIF
							ENDIF
						BREAK
						CASE 4
							IF IS_SAFE_TO_START_CONVERSATION()
								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_RADa", CONV_PRIORITY_HIGH)
									str_dialogue_root = ""
									i_dialogue_stage++
								ENDIF
							ENDIF
						BREAK
						CASE 5
							str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_RADb")
							IF IS_SAFE_TO_START_CONVERSATION()
								IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
									str_dialogue_root = ""
									i_dialogue_stage++
								ENDIF
							ENDIF
						BREAK
						CASE 6
							str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewGoon, "AH_RADb")
							IF IS_SAFE_TO_START_CONVERSATION()
								IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
									str_dialogue_root = ""
									i_dialogue_stage++
								ENDIF
							ENDIF
						BREAK
						CASE 7
							IF IS_SAFE_TO_START_CONVERSATION()
								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_RADc", CONV_PRIORITY_HIGH)
									i_dialogue_stage++
								ENDIF
							ENDIF
						BREAK
						CASE 8
							str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_RADd")
							IF IS_SAFE_TO_START_CONVERSATION()
								IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
									str_dialogue_root = ""
									i_dialogue_stage++
								ENDIF
							ENDIF
						BREAK
						CASE 9
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vHeliPadCoord) < 30.5
								IF IS_SAFE_TO_START_CONVERSATION()
									IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_ARR_F", CONV_PRIORITY_HIGH)
										i_dialogue_stage++
									ENDIF
								ENDIF
							ENDIF
						BREAK
//						CASE 10
//							IF IS_SAFE_TO_START_CONVERSATION()
//								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_APPROACH", CONV_PRIORITY_HIGH)
//									i_dialogue_stage++
//								ENDIF
//							ENDIF
//						BREAK
					ENDSWITCH
					
					IF i_dialogue_stage > 0
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						ENDIF
					ENDIF
				
				ELSE
					// only pause for the car banter not the radioing the crew
					IF i_dialogue_stage > 0 AND i_dialogue_stage <= 2
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						ENDIF
					ENDIF
				
				ENDIF

			// >>>>>> Crew and heli streaming
			//-----------------------------------
				
				IF NOT bStreamingSceneIn

					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vHeliPadCoord, FALSE) < 500.0		
					
						Load_Asset_Model(sAssetData, mod_heli)
						Load_Asset_Model(sAssetData, mod_esc_veh)
						Load_Asset_Model(sAssetData, mod_news_van)
						Load_Asset_Model(sAssetData, mod_pilot)
						Load_Asset_Model(sAssetData, S_M_M_SECURITY_01)
						Load_Asset_AnimDict(sAssetData, "amb@world_human_leaning@male@wall@back@foot_up@base")
						Load_Asset_AnimDict(sAssetData, "amb@world_human_leaning@male@wall@back@foot_up@idle_a")
						Load_Asset_AnimDict(sAssetData, "amb@world_human_leaning@male@wall@back@foot_up@idle_b")
						
						IF HAS_MODEL_LOADED(mod_heli)
						AND HAS_MODEL_LOADED(mod_esc_veh)
						AND HAS_MODEL_LOADED(mod_news_van)
						AND HAS_MODEL_LOADED(mod_pilot)
						AND HAS_MODEL_LOADED(S_M_M_SECURITY_01)
					
							// Heli
							IF NOT DOES_ENTITY_EXIST(vehs[mvf_heli].id)
								vehs[mvf_heli].id = CREATE_VEHICLE(mod_heli, vHeliStartCoord, fHeliStartHeading)
								SET_VEHICLE_AS_RESTRICTED(vehs[mvf_heli].id, 0)
								SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_heli].id, I_HELI_VARIATION)
								SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_heli].id)
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehs[mvf_heli].id, TRUE)
								SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_heli].id, FALSE)
								SET_VEHICLE_DOORS_LOCKED(vehs[mvf_heli].id, VEHICLELOCK_CANNOT_ENTER)
								bFailChecksOnHeli = TRUE
							ENDIF
							
							// Pilot
							IF NOT DOES_ENTITY_EXIST(peds[mpf_pilot].id)
								CREATE_MISSION_PED(peds[mpf_pilot], mod_pilot, <<2514.4924, -315.0782, 91.9927>>, 96.1190, "CREA_PILOT", REL_CREW)
								SET_PED_COMPONENT_VARIATION(peds[mpf_pilot].id, PED_COMP_SPECIAL2, 0, 0)
								Unload_Asset_Model(sAssetData, mod_pilot)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_pilot].id, TRUE)
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(peds[mpf_pilot].id, TRUE)
								bFailOnDeadPilot 	= TRUE
								bFailAbandonPilot 	= TRUE
							ENDIF
							
							// Gunman
							IF NOT DOES_ENTITY_EXIST(peds[mpf_goon].id)
								IF CREATE_CREW_MEMBER(peds[mpf_goon], cmf_goon, <<2510.9885, -319.4904, 91.9927>>, 46.8987, CREW_OUTFIT_STEALTH, 40)
									SET_PED_COMPONENT_VARIATION(peds[cmf_goon].id, PED_COMP_HAIR, 1, 0)
									SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(peds[mpf_goon].id, TRUE)
									bFailAbandonGunman = TRUE
								ENDIF
							ENDIF
							
							// Create the news van
							IF NOT DOES_ENTITY_EXIST(vehs[mvf_news_van].id)
								vehs[mvf_news_van].id = CREATE_VEHICLE(mod_news_van, <<2481.19, -317.24, 93.01>>, 299.55)
								SET_VEHICLE_AS_RESTRICTED(vehs[mvf_news_van].id, 3)
								SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_news_van].id, I_NEWS_VAN_VARIATION)
								SET_VEHICLE_LIVERY(vehs[mvf_news_van].id, I_NEWS_VAN_LIVERY)
								SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_news_van].id)
								SET_VEHICLE_ENGINE_ON(vehs[mvf_news_van].id, TRUE, FALSE)
								SET_VEHICLE_LIGHTS(vehs[mvf_news_van].id, SET_VEHICLE_LIGHTS_ON)
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehs[mvf_news_van].id, TRUE)
							ENDIF
							
							IF NOT DOES_ENTITY_EXIST(vehs[mvf_escape].id)
								vehs[mvf_escape].id = CREATE_VEHICLE(mod_esc_veh, << 2482.57, -324.40, 92.81 >>, 323.19)
								SET_VEHICLE_AS_RESTRICTED(vehs[mvf_escape].id, 1)
								SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_escape].id, I_ESC_VAN_VARIATION)
								SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_escape].id)
								SET_VEHICLE_ENGINE_ON(vehs[mvf_escape].id, TRUE, FALSE)
								SET_VEHICLE_LIGHTS(vehs[mvf_escape].id, SET_VEHICLE_LIGHTS_ON)
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehs[mvf_escape].id, TRUE)
								bFailChecksOnVan = TRUE
							ENDIF
							
							// Create the hacker
							IF DOES_ENTITY_EXIST(vehs[mvf_news_van].id)
								IF NOT DOES_ENTITY_EXIST(peds[mpf_hacker].id)
									IF CREATE_CREW_MEMBER_IN_VEHICLE(peds[mpf_hacker], cmf_hacker, vehs[mvf_news_van], VS_DRIVER, CREW_OUTFIT_STEALTH, 40)
										SET_PED_COMPONENT_VARIATION(peds[mpf_hacker].id, PED_COMP_HAIR, 1, 0)
									ENDIF
								ENDIF
							ENDIF
							
							// Create the Driver
							IF DOES_ENTITY_EXIST(vehs[mvf_escape].id)
								IF NOT DOES_ENTITY_EXIST(peds[mpf_driver].id)
									IF CREATE_CREW_MEMBER_IN_VEHICLE(peds[mpf_driver], cmf_driver, vehs[mvf_escape], VS_DRIVER, CREW_OUTFIT_STEALTH, 40)
										SET_PED_COMPONENT_VARIATION(peds[mpf_driver].id, PED_COMP_HAIR, 1, 0)
										bFailAbandonDriver = FALSE // we do not want to fail at distance from the driver at this point
									ENDIF
								ENDIF
							ENDIF
							
							IF NOT DOES_ENTITY_EXIST(peds[mpf_sec_guard_1].id)
								peds[mpf_sec_guard_1].id = CREATE_PED(PEDTYPE_MISSION, S_M_M_SECURITY_01, <<2560.93, -601.21, 63.77>>, 0)	
								SET_PED_COMBAT_ATTRIBUTES(peds[mpf_sec_guard_1].id, CA_ALWAYS_FIGHT, TRUE)
							ENDIF
							
							IF NOT DOES_ENTITY_EXIST(peds[mpf_sec_guard_2].id)
								peds[mpf_sec_guard_2].id = CREATE_PED(PEDTYPE_MISSION, S_M_M_SECURITY_01, <<2575.16, -290.00, 92.08>>, 0)	
								SET_PED_COMBAT_ATTRIBUTES(peds[mpf_sec_guard_2].id, CA_ALWAYS_FIGHT, TRUE)
							ENDIF
							
							IF NOT DOES_ENTITY_EXIST(peds[mpf_sec_guard_3].id)
								peds[mpf_sec_guard_3].id = CREATE_PED(PEDTYPE_MISSION, S_M_M_SECURITY_01, <<2492.97, -309.24, 91.99>>, 0)	
								SET_PED_COMBAT_ATTRIBUTES(peds[mpf_sec_guard_3].id, CA_ALWAYS_FIGHT, TRUE)
							ENDIF
							
							IF DOES_ENTITY_EXIST(vehs[mvf_heli].id)
							AND DOES_ENTITY_EXIST(vehs[mvf_escape].id)
							AND DOES_ENTITY_EXIST(vehs[mvf_news_van].id)
							AND DOES_ENTITY_EXIST(peds[mpf_goon].id)
							AND DOES_ENTITY_EXIST(peds[mpf_hacker].id)
							AND DOES_ENTITY_EXIST(peds[mpf_pilot].id)
							AND DOES_ENTITY_EXIST(peds[mpf_driver].id)
							AND DOES_ENTITY_EXIST(peds[mpf_sec_guard_1].id)
							AND DOES_ENTITY_EXIST(peds[mpf_sec_guard_2].id)
							AND DOES_ENTITY_EXIST(peds[mpf_sec_guard_3].id)
								Add_Peds_For_Dialogue()
								bStreamingSceneIn = TRUE
							ENDIF

						ENDIF
					ENDIF
				
				ELIF bStreamingSceneIn
					
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vHeliPadCoord, FALSE) > 550.0
					
						Unload_Asset_Model(sAssetData, mod_heli)
						Unload_Asset_Model(sAssetData, mod_esc_veh)
						Unload_Asset_Model(sAssetData, mod_news_van)
						Unload_Asset_Model(sAssetData, mod_pilot)
						Unload_Asset_Anim_Dict(sAssetData, "amb@world_human_leaning@male@wall@back@foot_up@base")
						Unload_Asset_Anim_Dict(sAssetData, "amb@world_human_leaning@male@wall@back@foot_up@idle_a")
						Unload_Asset_Anim_Dict(sAssetData, "amb@world_human_leaning@male@wall@back@foot_up@idle_b")
						
						// Stream Out
						IF DOES_ENTITY_EXIST(peds[mpf_pilot].id)
							DELETE_PED(peds[mpf_pilot].id)
						ENDIF
						IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
							DELETE_PED(peds[mpf_goon].id)
						ENDIF
						IF DOES_ENTITY_EXIST(peds[mpf_hacker].id)
							DELETE_PED(peds[mpf_hacker].id)
						ENDIF
						IF DOES_ENTITY_EXIST(peds[mpf_driver].id)
							DELETE_PED(peds[mpf_driver].id)
						ENDIF
						
						IF DOES_ENTITY_EXIST(peds[mpf_sec_guard_1].id)
							DELETE_PED(peds[mpf_sec_guard_1].id)
						ENDIF
						IF DOES_ENTITY_EXIST(peds[mpf_sec_guard_2].id)
							DELETE_PED(peds[mpf_sec_guard_2].id)
						ENDIF
						IF DOES_ENTITY_EXIST(peds[mpf_sec_guard_3].id)
							DELETE_PED(peds[mpf_sec_guard_3].id)
						ENDIF
					
						IF DOES_ENTITY_EXIST(vehs[mvf_heli].id)
							DELETE_VEHICLE(vehs[mvf_heli].id)
						ENDIF
						IF DOES_ENTITY_EXIST(vehs[mvf_news_van].id)
							DELETE_VEHICLE(vehs[mvf_news_van].id)
						ENDIF
						IF DOES_ENTITY_EXIST(vehs[mvf_escape].id)
							DELETE_VEHICLE(vehs[mvf_escape].id)
						ENDIF
						
						bStreamingSceneIn = FALSE
						
					ELIF HAS_ANIM_DICT_LOADED("amb@world_human_leaning@male@wall@back@foot_up@base")
					AND HAS_ANIM_DICT_LOADED("amb@world_human_leaning@male@wall@back@foot_up@idle_a")
					AND HAS_ANIM_DICT_LOADED("amb@world_human_leaning@male@wall@back@foot_up@idle_b")
					
						IF DOES_ENTITY_EXIST(peds[mpf_pilot].id)
						AND NOT IS_PED_INJURED(peds[mpf_pilot].id)
							
							IF GET_DISTANCE_BETWEEN_ENTITIES(peds[mpf_pilot].id, PLAYER_PED_ID()) < 20.0
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_pilot].id, SCRIPT_TASK_LOOK_AT_ENTITY)
									TASK_LOOK_AT_ENTITY(peds[mpf_pilot].id, PLAYER_PED_ID(), -1)
								ENDIF
							ELSE
								IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_pilot].id, SCRIPT_TASK_LOOK_AT_ENTITY)
									TASK_CLEAR_LOOK_AT(peds[mpf_pilot].id)
								ENDIF
							ENDIF
						
							SWITCH peds[mpf_pilot].iGenericFlag
								CASE 0
									TASK_START_SCENARIO_AT_POSITION(peds[mpf_pilot].id, "WORLD_HUMAN_GUARD_STAND", <<2514.4924, -315.0782, 91.9927>>, 96.1190, -1, FALSE)
									peds[mpf_pilot].iGenericFlag++
								BREAK
								CASE 1
									IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_pilot].id, SCRIPT_TASK_START_SCENARIO_AT_POSITION)
										TASK_TURN_PED_TO_FACE_ENTITY(peds[mpf_pilot].id, PLAYER_PED_ID(), 5000)
										peds[mpf_pilot].iGenericFlag++
									ENDIF
								BREAK
								CASE 2
									IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_pilot].id, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
									AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_pilot].id, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
										IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_pilot].id), <<2514.4924, -315.0782, 91.9927>>, 1.0)
											peds[mpf_pilot].iGenericFlag++
										ELSE
											TASK_FOLLOW_NAV_MESH_TO_COORD(peds[mpf_pilot].id, <<2514.4924, -315.0782, 91.9927>>, PEDMOVE_WALK, DEFAULT, 0.2, DEFAULT, 41.9451)
										ENDIF
									ENDIF
								BREAK
								CASE 3
									IF IS_ENTITY_OCCLUDED(peds[mpf_pilot].id)
									AND NOT IS_POINT_VISIBLE(<<2514.4924, -315.0782, 91.9927>>, 2.0, 100)
										peds[mpf_pilot].iGenericFlag = 0
									ENDIF
								BREAK
							ENDSWITCH

						ENDIF
						
						IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
						AND NOT IS_PED_INJURED(peds[mpf_goon].id)
							
							IF GET_DISTANCE_BETWEEN_ENTITIES(peds[mpf_goon].id, PLAYER_PED_ID()) < 20.0
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_LOOK_AT_ENTITY)
									TASK_LOOK_AT_ENTITY(peds[mpf_goon].id, PLAYER_PED_ID(), -1)
								ENDIF
							ELSE
								IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_LOOK_AT_ENTITY)
									TASK_CLEAR_LOOK_AT(peds[mpf_goon].id)
								ENDIF
							ENDIF
						
							SWITCH peds[mpf_goon].iGenericFlag
								CASE 0
									SAFE_OPEN_SEQUENCE()
										//TASK_START_SCENARIO_AT_POSITION(peds[mpf_goon].id, "WORLD_HUMAN_LEANING", <<2511.3000, -319.2517, 91.9927>>, 41.9451, -1, FALSE)
										TASK_PLAY_ANIM_ADVANCED(null, "amb@world_human_leaning@male@wall@back@foot_up@base", 	"base", 	<<2511.3000, -319.2517, 91.9927>>, <<0,0, 41.9451>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 2000, AF_OVERRIDE_PHYSICS | AF_LOOPING)
										TASK_PLAY_ANIM_ADVANCED(null, "amb@world_human_leaning@male@wall@back@foot_up@idle_a", 	"idle_a", 	<<2511.3000, -319.2517, 91.9927>>, <<0,0, 41.9451>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
										TASK_PLAY_ANIM_ADVANCED(null, "amb@world_human_leaning@male@wall@back@foot_up@base", 	"base", 	<<2511.3000, -319.2517, 91.9927>>, <<0,0, 41.9451>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 1000, AF_OVERRIDE_PHYSICS | AF_LOOPING)
										TASK_PLAY_ANIM_ADVANCED(null, "amb@world_human_leaning@male@wall@back@foot_up@idle_a", 	"idle_c", 	<<2511.3000, -319.2517, 91.9927>>, <<0,0, 41.9451>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
										TASK_PLAY_ANIM_ADVANCED(null, "amb@world_human_leaning@male@wall@back@foot_up@base", 	"base", 	<<2511.3000, -319.2517, 91.9927>>, <<0,0, 41.9451>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 3000, AF_OVERRIDE_PHYSICS | AF_LOOPING)
										TASK_PLAY_ANIM_ADVANCED(null, "amb@world_human_leaning@male@wall@back@foot_up@idle_b", 	"idle_d", 	<<2511.3000, -319.2517, 91.9927>>, <<0,0, 41.9451>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
										TASK_PLAY_ANIM_ADVANCED(null, "amb@world_human_leaning@male@wall@back@foot_up@base", 	"base", 	<<2511.3000, -319.2517, 91.9927>>, <<0,0, 41.9451>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 2000, AF_OVERRIDE_PHYSICS | AF_LOOPING)
										TASK_PLAY_ANIM_ADVANCED(null, "amb@world_human_leaning@male@wall@back@foot_up@idle_a", 	"idle_c", 	<<2511.3000, -319.2517, 91.9927>>, <<0,0, 41.9451>>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
										SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
									SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
									peds[mpf_goon].iGenericFlag++
								BREAK
								CASE 1
									IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_PERFORM_SEQUENCE)
										TASK_TURN_PED_TO_FACE_ENTITY(peds[mpf_goon].id, PLAYER_PED_ID(), 5000)
										peds[mpf_goon].iGenericFlag++
									ENDIF
								BREAK
								CASE 2
									IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
									AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
										IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), <<2511.3000, -319.2517, 91.9927>>, 1.0)
											peds[mpf_goon].iGenericFlag++
										ELSE
											TASK_FOLLOW_NAV_MESH_TO_COORD(peds[mpf_goon].id, <<2511.3000, -319.2517, 91.9927>>, PEDMOVE_WALK, DEFAULT, 0.2, DEFAULT, 41.9451)
										ENDIF
									ENDIF
								BREAK
								CASE 3
									IF IS_ENTITY_OCCLUDED(peds[mpf_goon].id)
									AND NOT IS_POINT_VISIBLE(<<2511.3000, -319.2517, 91.9927>>, 2.0, 100)
										peds[mpf_goon].iGenericFlag = 0
									ENDIF
								BREAK
							ENDSWITCH
						ENDIF

					ENDIF
				ENDIF
				
			// Manage security guards
				IF bStreamingSceneIn
					IF DOES_ENTITY_EXIST(peds[mpf_sec_guard_1].id)
					AND NOT IS_PED_INJURED(peds[mpf_sec_guard_1].id)
						IF peds[mpf_sec_guard_1].iGenericFlag = 0
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_sec_guard_1].id, SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS, TRUE)
								IF DOES_SCENARIO_EXIST_IN_AREA(<<2560.93, -601.21, 63.77>>, 1.0, FALSE)
									TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(peds[mpf_sec_guard_1].id, <<2560.93, -601.21, 63.77>>, 1.0, -1)
									peds[mpf_sec_guard_1].iGenericFlag++
								ENDIF
							ENDIF
						ELSE
							IF peds[mpf_sec_guard_1].iGenericFlag = 1
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), peds[mpf_sec_guard_1].id) < 10.0
									PLAY_PED_AMBIENT_SPEECH(peds[mpf_sec_guard_1].id, "GENERIC_HI", SPEECH_PARAMS_FORCE)
									peds[mpf_sec_guard_1].iGenericFlag++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(peds[mpf_sec_guard_2].id)
					AND NOT IS_PED_INJURED(peds[mpf_sec_guard_2].id)
						IF peds[mpf_sec_guard_2].iGenericFlag = 0
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_sec_guard_2].id, SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS, TRUE)
								IF DOES_SCENARIO_EXIST_IN_AREA(<<2575.16, -290.00, 92.08>>, 1.0, FALSE)
									TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(peds[mpf_sec_guard_2].id, <<2575.16, -290.00, 92.08>>, 1.0, -1)
									peds[mpf_sec_guard_2].iGenericFlag++
								ENDIF
							ENDIF
						ELSE
							IF peds[mpf_sec_guard_2].iGenericFlag = 1
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), peds[mpf_sec_guard_2].id) < 10.0
									PLAY_PED_AMBIENT_SPEECH(peds[mpf_sec_guard_2].id, "GENERIC_HI", SPEECH_PARAMS_FORCE)
									peds[mpf_sec_guard_2].iGenericFlag++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(peds[mpf_sec_guard_3].id)
					AND NOT IS_PED_INJURED(peds[mpf_sec_guard_3].id)
						IF peds[mpf_sec_guard_3].iGenericFlag = 0
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_sec_guard_3].id, SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS, TRUE)
								IF DOES_SCENARIO_EXIST_IN_AREA(<<2492.97, -309.24, 91.99>>, 1.0, FALSE)
									TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(peds[mpf_sec_guard_3].id, <<2492.97, -309.24, 91.99>>, 1.0, -1)
									peds[mpf_sec_guard_3].iGenericFlag++
								ENDIF
							ENDIF
						ELSE
							IF peds[mpf_sec_guard_3].iGenericFlag = 1
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), peds[mpf_sec_guard_3].id) < 10.0
									PLAY_PED_AMBIENT_SPEECH(peds[mpf_sec_guard_3].id, "GENERIC_HI", SPEECH_PARAMS_FORCE)
									peds[mpf_sec_guard_3].iGenericFlag++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					peds[mpf_sec_guard_1].iGenericFlag = 0
					peds[mpf_sec_guard_2].iGenericFlag = 0
					peds[mpf_sec_guard_3].iGenericFlag = 0
				ENDIF
				
			ENDIF
		BREAK
		CASE 3
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 7.0, 1, 0.0)
				OR IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0)
					TASK_LEAVE_ANY_VEHICLE(BUDDY_PED_ID(), 500)

					mission_substage++
				ENDIF
			ELSE
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				mission_substage++
			ENDIF
			
			IF mission_substage = 4
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
		BREAK
		CASE 4
			IF NOT IS_PED_IN_ANY_VEHICLE(MIKE_PED_ID(), TRUE)
			AND NOT IS_PED_IN_ANY_VEHICLE(FRANK_PED_ID(), TRUE)
			
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()

					KILL_FACE_TO_FACE_CONVERSATION()
					
				ELIF IS_SAFE_TO_START_CONVERSATION()
				
					IF GET_CLOCK_HOURS() > 4 AND GET_CLOCK_HOURS() < 23
						i_TimerA = -1
						str_dialogue_root = "AH_MMGPT"
					ELSE
						i_TimerA = GET_GAME_TIMER() + 1000
						str_dialogue_root = "AH_MMGPW"
					ENDIF
				
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
			
						cams[mcf_tod_1] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2504.2, -290.1, 96.9>>, <<9.2,-0.0, -169.6>>, 47.6, TRUE)
						SHAKE_CAM(cams[mcf_tod_1], "HAND_SHAKE", 0.3)
						cams[mcf_tod_2] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<2503.9, -288.8, 112.8>>, <<-0.2,0.0,-169.6>>, 47.6, FALSE)
						SET_CAM_ACTIVE_WITH_INTERP(cams[mcf_tod_2], cams[mcf_tod_1], 8000)

						DISPLAY_RADAR(FALSE)
						DISPLAY_HUD(FALSE)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
						IF IS_AUDIO_SCENE_ACTIVE("AH_3B_GET_TO_FACILITY")
							STOP_AUDIO_SCENE("AH_3B_GET_TO_FACILITY")
						ENDIF
						
						DOOR_SYSTEM_SET_OPEN_RATIO(enum_to_int(DOORHASH_NOSE_REAR_L), 1.0, TRUE, TRUE)
						DOOR_SYSTEM_SET_OPEN_RATIO(enum_to_int(DOORHASH_NOSE_REAR_R), -1.0, TRUE, TRUE)
						DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_NOSE_REAR_L), DOORSTATE_LOCKED, TRUE, TRUE)
						DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_NOSE_REAR_R), DOORSTATE_LOCKED, TRUE, TRUE)
						
						// 1994460
//						// warp buddy peds into the upper room at the heli pad
//						CLEAR_PED_TASKS_IMMEDIATELY(peds[mpf_goon].id)
//						CLEAR_PED_TASKS_IMMEDIATELY(peds[mpf_pilot].id)
//						SET_ENTITY_COORDS(peds[mpf_goon].id, <<2508.7849, -335.7761, 114.6903>>)
//						SET_ENTITY_COORDS(peds[mpf_pilot].id, <<2508.7849, -335.7761, 114.6903>>)
						CLEAR_PED_TASKS_IMMEDIATELY(peds[mpf_goon].id)
						SET_ENTITY_COORDS(peds[mpf_goon].id, <<2511.3245, -317.7578, 91.9929>>)
						SET_ENTITY_HEADING(peds[mpf_goon].id, 234.4961)
						TASK_GO_STRAIGHT_TO_COORD(peds[mpf_goon].id, <<2520.5002, -327.0908, 91.9980>>, PEDMOVE_WALK)
						FORCE_PED_MOTION_STATE(peds[mpf_goon].id, MS_ON_FOOT_WALK)
						
						CLEAR_PED_TASKS_IMMEDIATELY(peds[mpf_pilot].id)
						SET_ENTITY_COORDS(peds[mpf_pilot].id, <<2511.9336, -315.1837, 91.9929>>)
						SET_ENTITY_HEADING(peds[mpf_pilot].id, 162.0331)
						TASK_GO_STRAIGHT_TO_COORD(peds[mpf_pilot].id, <<2521.0198, -325.2653, 91.9980>>, PEDMOVE_WALK)
						FORCE_PED_MOTION_STATE(peds[mpf_pilot].id, MS_ON_FOOT_WALK)
						
						// see michael walk into the lower room
						CLEAR_PED_TASKS_IMMEDIATELY(MIKE_PED_ID())
						SET_ENTITY_COORDS(MIKE_PED_ID(), <<2509.3838, -314.5894, 91.9926>>)
						SET_ENTITY_HEADING(MIKE_PED_ID(), 229.0784)
						TASK_GO_STRAIGHT_TO_COORD(MIKE_PED_ID(), <<2516.5581, -322.3859, 91.9979>>, PEDMOVE_WALK)
						FORCE_PED_MOTION_STATE(MIKE_PED_ID(), MS_ON_FOOT_WALK)
						
						// see frank walk into the lower room
						CLEAR_PED_TASKS_IMMEDIATELY(FRANK_PED_ID())
						SET_ENTITY_COORDS(FRANK_PED_ID(), <<2510.8154, -316.4493, 91.9926>>)
						SET_ENTITY_HEADING(FRANK_PED_ID(), 227.7634)
						TASK_GO_STRAIGHT_TO_COORD(FRANK_PED_ID(), <<2516.5581, -322.3859, 91.9979>>, PEDMOVE_WALK)
						FORCE_PED_MOTION_STATE(FRANK_PED_ID(), MS_ON_FOOT_WALK)
						
						CLEAR_AREA_OF_PROJECTILES(<<2504.07, -313.17, 91.99>>, 26.0)
						STOP_FIRE_IN_RANGE(<<2504.07, -313.17, 91.99>>, 26.0)
						
						SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), TRUE)
						SET_ENTITY_INVINCIBLE(FRANK_PED_ID(), TRUE)
						SET_ENTITY_INVINCIBLE(peds[mpf_goon].id, TRUE)
						SET_ENTITY_INVINCIBLE(peds[mpf_pilot].id, TRUE)
						
						RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(
							<<2507.712158,-315.131073,91.992859>>, <<2511.952148,-319.157104,98.186020>>, 12.875000, 
							<<2503.3755, -317.5417, 91.9929>>, 261.3922, TRUE, TRUE, TRUE)
					
						CLEAR_AREA(<<2511.90, -319.02, 92.12>>, 7.5, TRUE)
						
						Load_Asset_NewLoadScene_Sphere( sAssetData, <<2505.2742, -338.8715, 116.6902>>, 150.0 )
						mission_substage++
					ENDIF

				ENDIF

			ENDIF
		BREAK
		CASE 5
			IF NOT IS_CAM_INTERPOLATING(cams[mcf_tod_1])
			AND NOT IS_CAM_INTERPOLATING(cams[mcf_tod_2])
			AND HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(MIKE_PED_ID())
			AND HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(FRANK_PED_ID())

				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2510.3096, -336.4757, 114.6900>>)
				SET_ENTITY_COORDS(BUDDY_PED_ID(), <<2511.1599, -336.9917, 114.6900>>)
				
				DOOR_SYSTEM_SET_OPEN_RATIO(enum_to_int(DOORHASH_NOSE_REAR_L), 0.0, TRUE, TRUE)
				DOOR_SYSTEM_SET_OPEN_RATIO(enum_to_int(DOORHASH_NOSE_REAR_R), 0.0, TRUE, TRUE)
				DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_NOSE_REAR_L), DOORSTATE_LOCKED, TRUE, TRUE)
				DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_NOSE_REAR_R), DOORSTATE_LOCKED, TRUE, TRUE)
				DOOR_SYSTEM_SET_OPEN_RATIO(enum_to_int(DOORHASH_NOSE_HELIPAD), 1.0, TRUE, TRUE)
				DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_NOSE_HELIPAD), DOORSTATE_LOCKED, TRUE, TRUE)
				
				SET_PED_PROP_INDEX(peds[mpf_pilot].id, ANCHOR_HEAD, 0, 0)
				SET_PED_COMPONENT_VARIATION(peds[cmf_goon].id, PED_COMP_HAIR, 0, 0)
				SET_PED_COMPONENT_VARIATION(peds[cmf_driver].id, PED_COMP_HAIR, 0, 0)

				SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH, FALSE)
				SET_PED_COMP_ITEM_CURRENT_SP(FRANK_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_STEALTH, FALSE)
				RELEASE_PED_PRELOAD_VARIATION_DATA(MIKE_PED_ID())
				RELEASE_PED_PRELOAD_VARIATION_DATA(FRANK_PED_ID())
				
				mission_substage++
			
			ENDIF
		BREAK
		CASE 6
			BOOL bScriptedCutFinished
		
			IF i_TimerA = -1
				sTimelapse.currentTimeOfDay = GET_CURRENT_TIMEOFDAY()
				IF DO_TIMELAPSE(SP_HEIST_AGENCY_3B, sTimelapse, TRUE, DEFAULT, DEFAULT, TRUE)
					bScriptedCutFinished = TRUE
				ENDIF
			ELSE
				IF GET_GAME_TIMER() > i_TimerA
					bScriptedCutFinished = TRUE
				ENDIF
			ENDIF

			IF bScriptedCutFinished
			AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(MIKE_PED_ID())
			AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(FRANK_PED_ID())
			AND IS_SAFE_TO_START_CONVERSATION()
			AND IS_NEW_LOAD_SCENE_LOADED()
			
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					ANIMPOSTFX_PLAY("CamPushInMichael", 0, FALSE)
				ELSE
					ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
				ENDIF
				PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
			
				i_FlashTimer = GET_GAME_TIMER() + 300
				mission_substage++
			ENDIF
		
		BREAK
		CASE 7
		
			IF GET_GAME_TIMER() > i_FlashTimer 
		
				IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_PCGI", CONV_PRIORITY_HIGH)
				
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
						Prep_Hotswap(FALSE, TRUE, TRUE)
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, FALSE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
					ENDIF
					
					REPLAY_STOP_EVENT()
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_heli].id, TRUE)
					SET_VEHICLE_DOORS_LOCKED(vehs[mvf_heli].id, VEHICLELOCK_UNLOCKED)
					
					SET_ENTITY_COORDS(MIKE_PED_ID(), <<2506.8528, -337.4291, 115.2900>>)
					SET_ENTITY_HEADING(MIKE_PED_ID(), 135.1543)
					FORCE_PED_MOTION_STATE(MIKE_PED_ID(), MS_ON_FOOT_WALK)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(MIKE_PED_ID())
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1500)
					
					SET_ENTITY_COORDS(FRANK_PED_ID(), <<2505.1475, -340.5014, 117.0245>>)
					SET_ENTITY_HEADING(FRANK_PED_ID(), 266.4603)
					TASK_ENTER_VEHICLE(FRANK_PED_ID(), vehs[mvf_heli].id, DEFAULT_TIME_BEFORE_WARP, VS_BACK_RIGHT, PEDMOVE_WALK)
					FORCE_PED_MOTION_STATE(FRANK_PED_ID(), MS_ON_FOOT_WALK)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(FRANK_PED_ID())

					SET_PED_INTO_VEHICLE(peds[mpf_pilot].id, vehs[mvf_heli].id, VS_FRONT_RIGHT)
					SET_PED_INTO_VEHICLE(peds[mpf_goon].id, vehs[mvf_heli].id, VS_BACK_LEFT)
					
					TASK_LOOK_AT_ENTITY(peds[mpf_goon].id, PLAYER_PED_ID(), -1)
					TASK_LOOK_AT_ENTITY(peds[mpf_pilot].id, PLAYER_PED_ID(), -1)
					TASK_LOOK_AT_ENTITY(peds[mpf_frank].id, PLAYER_PED_ID(), -1)
					 
					// Time has passed in the tod, do not need the vans or the driver and hacker for a while
					IF DOES_ENTITY_EXIST(peds[mpf_hacker].id)
						DELETE_PED(peds[mpf_hacker].id)
					ENDIF
					IF DOES_ENTITY_EXIST(peds[mpf_driver].id)
						DELETE_PED(peds[mpf_driver].id)
					ENDIF
					IF DOES_ENTITY_EXIST(vehs[mvf_news_van].id)
						DELETE_VEHICLE(vehs[mvf_news_van].id)
					ENDIF
					IF DOES_ENTITY_EXIST(vehs[mvf_escape].id)
						DELETE_VEHICLE(vehs[mvf_escape].id)
					ENDIF
					
					SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), FALSE)
					SET_ENTITY_INVINCIBLE(FRANK_PED_ID(), FALSE)
					SET_ENTITY_INVINCIBLE(peds[mpf_goon].id, FALSE)
					SET_ENTITY_INVINCIBLE(peds[mpf_pilot].id, FALSE)
					
					Unload_Asset_Model(sAssetData, mod_heli)
					Unload_Asset_Model(sAssetData, mod_esc_veh)
					Unload_Asset_Model(sAssetData, mod_news_van)
					Unload_Asset_Model(sAssetData, mod_pilot)
					Unload_Asset_Anim_Dict(sAssetData, "amb@world_human_leaning@male@wall@back@foot_up@base")
					Unload_Asset_Anim_Dict(sAssetData, "amb@world_human_leaning@male@wall@back@foot_up@idle_a")
					Unload_Asset_Anim_Dict(sAssetData, "amb@world_human_leaning@male@wall@back@foot_up@idle_b")
					Unload_Asset_NewLoadScene( sAssetData )

					// 2043621
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_FOOT ) = CAM_VIEW_MODE_FIRST_PERSON
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						CPRINTLN( DEBUG_MIKE, "POST TOD CAM RESET FOR FIRST PERSON" )
					ELSE
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(164.9536)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-27.9584)
						CPRINTLN( DEBUG_MIKE, "POST TOD CAM RESET FOR THIRD PERSON" )
					ENDIF

					Event_Trigger(mef_manage_clock)
					
					IF i_TimerA = -1
						SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE, FALSE, 0, FALSE, TRUE)
					ELSE
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
					ENDIF
					
					DESTROY_CAM(cams[mcf_tod_1])
					DESTROY_CAM(cams[mcf_tod_2])

					i_TimerA = GET_GAME_TIMER()
					CPRINTLN(DEBUG_MIKE, "AH3B: Hit tod/camera pan finished")
					
					mission_substage++
				ENDIF
			ENDIF
		BREAK
		CASE 8
			IF GET_GAME_TIMER() - i_TimerA > 1000			
			
				DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_NOSE_HELIPAD), DOORSTATE_UNLOCKED, TRUE, TRUE)
				DOORS_Set_State(mdf_helipad, TRUE, 0.0, 500)
				
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				Mission_Set_Stage(msf_2_fly_to_jump_zone)
			ENDIF
		BREAK
		CASE STAGE_EXIT
			SAFE_REMOVE_BLIP(blip_Objective)
			// 1981580
			//CLEAR_PRINTS()
		BREAK
	ENDSWITCH
	
	//Manage deleting Lester
	IF mission_substage > STAGE_ENTRY
		IF DOES_ENTITY_EXIST(peds[mpf_lester].id)
			IF GET_INTERIOR_FROM_ENTITY(MIKE_PED_ID()) != interior_sweatshop
			AND GET_INTERIOR_FROM_ENTITY(FRANK_PED_ID()) != interior_sweatshop
				IF DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_SWEATSHOP_L)) != DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_SWEATSHOP_L), DOORSTATE_LOCKED)
				ENDIF 
				
				IF DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_SWEATSHOP_R)) != DOORSTATE_LOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_SWEATSHOP_R), DOORSTATE_LOCKED)
				ENDIF
				
				IF DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_SWEATSHOP_L)) = DOORSTATE_LOCKED
				AND DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_SWEATSHOP_R)) = DOORSTATE_LOCKED
					IF ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(enum_to_int(DOORHASH_SWEATSHOP_L))) < 0.05
					AND ABSF(DOOR_SYSTEM_GET_OPEN_RATIO(enum_to_int(DOORHASH_SWEATSHOP_R))) < 0.05
						DELETE_PED(peds[mpf_lester].id)
						DELETE_OBJECT(objs[mof_lester_crate].id)
						DELETE_OBJECT(objs[mof_walking_stick].id)
						Unload_Asset_Anim_Dict(sAssetData, "missheistfbi3bleadinoutah_3b_int")
					ENDIF
				ENDIF
			ELSE
				IF DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_SWEATSHOP_L)) != DOORSTATE_UNLOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_SWEATSHOP_L), DOORSTATE_UNLOCKED)
				ENDIF
				
				IF DOOR_SYSTEM_GET_DOOR_STATE(enum_to_int(DOORHASH_SWEATSHOP_R)) != DOORSTATE_UNLOCKED
					DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_SWEATSHOP_R), DOORSTATE_UNLOCKED)
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(peds[mpf_lester].id)
		AND NOT IS_PED_INJURED(peds[mpf_lester].id)
		
			// Lester
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_lester].id, SCRIPT_TASK_PLAY_ANIM)
			OR IS_PED_RAGDOLL(peds[mpf_lester].id)
			OR IS_PED_RUNNING_RAGDOLL_TASK(peds[mpf_lester].id)
				IF IS_ENTITY_ATTACHED(objs[mof_lester_crate].id)
					DETACH_ENTITY(objs[mof_lester_crate].id)
				ENDIF
				IF IS_ENTITY_ATTACHED(objs[mof_walking_stick].id)
					DETACH_ENTITY(objs[mof_walking_stick].id)
				ENDIF
			ELSE
				IF NOT IS_ENTITY_ATTACHED(objs[mof_lester_crate].id)
				AND NOT IS_ENTITY_ATTACHED(objs[mof_walking_stick].id)
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_lester].id, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
						TASK_TURN_PED_TO_FACE_ENTITY(peds[mpf_lester].id, PLAYER_PED_ID(), -1)
						TASK_LOOK_AT_ENTITY(peds[mpf_lester].id, PLAYER_PED_ID(), -1)
					ENDIF
				ENDIF
			ENDIF
			
			// Lester linger conversation
			IF mission_substage > 1
				IF NOT IS_PED_RAGDOLL(peds[mpf_lester].id)
				AND NOT IS_PED_RUNNING_RAGDOLL_TASK(peds[mpf_lester].id)
				AND GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_sweatshop
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), peds[mpf_lester].id) < 7.5
					IF GET_GAME_TIMER() - i_time_of_last_convo > 20000
						IF IS_SAFE_TO_START_CONVERSATION()
							IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_BACK", CONV_PRIORITY_HIGH)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
ENDPROC
 
//PURPOSE: Player flies the helicopter to the jump zone above the FIB building.
PROC ST_2_Fly_To_Jump_Zone()
	SWITCH mission_substage
		CASE STAGE_ENTRY
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_mikesCar].id)
			SET_ENTITY_LOAD_COLLISION_FLAG(vehs[mvf_heli].id, FALSE)
			
			Load_Asset_PTFX(sAssetData)
			
			Add_Peds_For_Dialogue()
			
			TASK_CLEAR_LOOK_AT(FRANK_PED_ID())
			TASK_CLEAR_LOOK_AT(peds[mpf_goon].id)
			TASK_CLEAR_LOOK_AT(peds[mpf_pilot].id)
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_FLY_TO_JUMP_ZONE")
				START_AUDIO_SCENE("AH_3B_FLY_TO_JUMP_ZONE")
			ENDIF

			REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
			
			SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)

			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "2. Fly heli to jump zone")
			i_dialogue_stage 	= 0
			bLiftedOff 			= FALSE
			mission_substage++
		BREAK
		CASE 1
			IF IS_SAFE_TO_DISPLAY_GODTEXT()
				mission_substage++
			ENDIF
		BREAK
		CASE 2
			IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(locates_Data, vHeliJumpBlipCoord, << 160.0, 160.0, 160.0 >>, FALSE, 
						peds[mpf_frank].id, peds[mpf_goon].id, peds[mpf_pilot].id, vehs[mvf_heli].id,
						"A3B_JUMP", 
						"", "", "", "A3B_CREWLB", 						// pickup b1, b2, b3, all buddies
						"CMN_GENGETINHE", "CMN_GENGETBCKHE",	// get in vehicle, get back in vehicle
						FALSE, TRUE, TRUE)
						
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_FOCUS_ON_JUMP_ZONE")
					STOP_AUDIO_SCENE("AH_3B_FOCUS_ON_JUMP_ZONE")
				ENDIF
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				KILL_FACE_TO_FACE_CONVERSATION()
				REPLAY_RECORD_BACK_FOR_TIME(10.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
				mission_substage++
			ELSE
				CONTROL_COORD_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, vHeliJumpBlipCoord)	
				
				IF IS_GAMEPLAY_HINT_ACTIVE()
					IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_FOCUS_ON_JUMP_ZONE")
						START_AUDIO_SCENE("AH_3B_FOCUS_ON_JUMP_ZONE")
					ENDIF
				ELSE
					IF IS_AUDIO_SCENE_ACTIVE("AH_3B_FOCUS_ON_JUMP_ZONE")
						STOP_AUDIO_SCENE("AH_3B_FOCUS_ON_JUMP_ZONE")
					ENDIF
				ENDIF
			ENDIF
			
			CONST_FLOAT		f_jump_dist_threshold		500.0
			CONST_INT		i_jump_marker_alpha			75

			FLOAT 	f_dist 		
			
			VECTOR 	v_PlayerCoord
			v_PlayerCoord	= GET_ENTITY_COORDS(PLAYER_PED_ID())
			f_dist 			= GET_DISTANCE_BETWEEN_COORDS(v_PlayerCoord, vHeliJumpBlipCoord)
			
			IF NOT IS_CUTSCENE_ACTIVE()
				CDEBUG2LN(DEBUG_MIKE, "***************Cutscene not loaded MCS_1, load at 400 - Distance: ", f_dist, " PlayerCoord: ", v_PlayerCoord, " JumpLocCoord: ", vHeliJumpBlipCoord)
				IF f_dist < 400.0
					REQUEST_CUTSCENE("AH_3B_MCS_1")
					Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, FRANK_PED_ID(), 	"Franklin")
					Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, peds[mpf_pilot].id, "Driver_selection")
					Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, peds[mpf_goon].id, 	"gunman_selection_1")
					Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, "gunman_selection_1", PED_COMP_FEET, 1, 0)
					CPRINTLN(DEBUG_MIKE, "***************REQUESTED CUTSCENE: AH_3B_MCS_1")
				ENDIF
			ELSE
				CDEBUG2LN(DEBUG_MIKE, "***************Cutscene loaded MCS_1, unload at 600.0 - Distance: ", f_dist, " PlayerCoord: ", v_PlayerCoord, " JumpLocCoord: ", vHeliJumpBlipCoord)
				IF f_dist > 600.0
					REMOVE_CUTSCENE()
					Reset_Cutscene_Ped_Variation_Streaming(sCutscenePedVariationRegister)
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(locates_Data.LocationBlip)
				INT i_alpha
				i_alpha			= i_jump_marker_alpha
				IF f_dist <= f_jump_dist_threshold
					i_alpha = CLAMP_INT(ROUND(i_alpha * POW((f_dist / f_jump_dist_threshold), 2.0)), 0, i_jump_marker_alpha)
				ENDIF
				INT iColR, iColG, iColB, iColA
				GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iColR, iColG, iColB, iColA)
				DRAW_MARKER(MARKER_RING, vHeliJumpBlipCoord, GET_ENTITY_COORDS(PLAYER_PED_ID())-vHeliJumpBlipCoord, <<0,0,0>>, <<50.0, 50.0, 50.0>>, iColR, iColG, iColB, i_alpha)
			ENDIF
			
			IF NOT bLiftedOff
			
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehs[mvf_heli].id, TRUE)
					IF IS_SAFE_TO_START_CONVERSATION()
						IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), peds[mpf_pilot].id)
						OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), peds[mpf_pilot].id)
							IF GET_GAME_TIMER() - i_time_of_last_convo > 3000
								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_MMACW", CONV_PRIORITY_HIGH)
								ENDIF
							ENDIF
						ELSE
							IF GET_GAME_TIMER() - i_time_of_last_convo > 8000
								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_MDGI", CONV_PRIORITY_HIGH)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				IF DOES_ENTITY_EXIST(vehs[mvf_heli].id)
				AND IS_VEHICLE_DRIVEABLE(vehs[mvf_heli].id)
				AND IS_ENTITY_IN_AIR(vehs[mvf_heli].id)
					TRIGGER_MUSIC_EVENT("AH3B_HELI_LIFT_OFF")
					str_dialogue_line = ""
					str_dialogue_root = ""
					bLiftedOff = TRUE
				ENDIF
			ELSE
			
				IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxs[ptfx_sweat_shop])
				AND HAS_PTFX_ASSET_LOADED()
					ptfxs[ptfx_sweat_shop] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_agency3b_blding_smoke", vSweatShopSmokeCoord, vSweatShopSmokeRot, fSweatShopSmokeScale, FALSE, FALSE, FALSE)
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_NORMAL )
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_NORMAL ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_DESTROYED ) 
					SET_BUILDING_STATE( BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_DESTROYED )
				ENDIF
			
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehs[mvf_heli].id)
					IF NOT IS_STRING_NULL_OR_EMPTY(str_dialogue_root)
					AND NOT IS_STRING_NULL_OR_EMPTY(str_dialogue_line)
						IF IS_SAFE_TO_START_CONVERSATION()
							IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sConvo, strDialogue, str_dialogue_root, str_dialogue_line, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								str_dialogue_line = ""
							ENDIF
						ENDIF
					ELSE
						SWITCH i_dialogue_stage
							CASE 0
								IF DOES_BLIP_EXIST(locates_Data.LocationBlip)
									i_dialogue_stage++
								ENDIF
							BREAK
							CASE 1
								IF IS_SAFE_TO_START_CONVERSATION()
									IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_JUMP", CONV_PRIORITY_HIGH)
										i_dialogue_stage++
									ENDIF
								ENDIF
							BREAK
							CASE 2
								IF GET_GAME_TIMER() - i_time_of_last_convo >= 5000
									IF IS_SAFE_TO_START_CONVERSATION()
										IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_CHAT2", CONV_PRIORITY_HIGH)
											i_dialogue_stage++
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE 3
								IF IS_SAFE_TO_START_CONVERSATION()
									str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewGoon, "AH_REPLY1")
									IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
										str_dialogue_root = ""
										i_dialogue_stage++
									ENDIF
								ENDIF
							BREAK
							CASE 4
								IF IS_SAFE_TO_START_CONVERSATION()
									IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<172.19307, -745.55750, 60.82772>>, FALSE) < 1438.83
										IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_BURN", CONV_PRIORITY_HIGH)
											i_dialogue_stage++
										ENDIF
									ELSE
										i_dialogue_stage = 6
									ENDIF
								ENDIF
							BREAK
							CASE 5
								IF IS_SAFE_TO_START_CONVERSATION()
								AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
									i_dialogue_stage++
								ENDIF
							BREAK
							CASE 6
								IF IS_SAFE_TO_START_CONVERSATION()
									IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_CHAT2_P2", CONV_PRIORITY_HIGH)
										i_dialogue_stage++
									ENDIF
								ENDIF
							BREAK
							CASE 7
								IF IS_SAFE_TO_START_CONVERSATION()
									str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewGoon, "AH_RESP1")
									IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
										str_dialogue_root = ""
										i_dialogue_stage++
									ENDIF
								ENDIF
							BREAK
							CASE 8
								IF IS_SAFE_TO_START_CONVERSATION()
									IF GET_GAME_TIMER() - i_time_of_last_convo >= 10000
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vHeliJumpBlipCoord, FALSE) > 200
											str_dialogue_root = "AH_PIL_KPCL"
											i_dialogue_stage++
										ELSE
											VECTOR vPlayerCoord
											vPlayerCoord = GET_ENTITY_COORDS(PLAYER_PED_ID())
											IF vPlayerCoord.z < vHeliJumpBlipCoord.z - 200
												str_dialogue_root = "AH_PIL_CLIMB"
												i_dialogue_stage++
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							BREAK
							CASE 9
								IF IS_SAFE_TO_START_CONVERSATION()
									IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
										str_dialogue_root = ""
										i_dialogue_stage++
									ENDIF
								ENDIF
							BREAK
							CASE 10
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									i_dialogue_stage = 8
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
				ELSE
					IF IS_SCRIPTED_CONVERSATION_ONGOING()
						str_dialogue_root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						str_dialogue_line = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF HAS_CUTSCENE_LOADED()
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			
				Load_Asset_AnimDict(sAssetData, animDict_IG1_sky_dive)
				
				REGISTER_ENTITY_FOR_CUTSCENE(vehs[mvf_heli].id, 	"Michaels_Heli", 		CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_frank].id, 	"Franklin", 			CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_goon].id, 	"gunman_selection_1", 	CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_pilot].id, 	"Driver_selection", 	CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				
				TRIGGER_MUSIC_EVENT("AH3B_HELI_CS")
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_FLY_TO_JUMP_ZONE")
					STOP_AUDIO_SCENE("AH_3B_FLY_TO_JUMP_ZONE")
				ENDIF
				
				SCRIPT_OVERRIDES_WIND_ELEVATION(TRUE, HASH("WEATHER_TYPES_HIGH_ELEVATION_BASE_JUMP_HELI"))
				PLAY_SOUND_FROM_ENTITY(sounds[sfx_heli_base_jump], "BASEJUMPS_CHOPPER_WIND_WAIT", vehs[mvf_heli].id)
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				
				bExitStateCam = FALSE
				bExitStateMichael = FALSE
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				//SET_CUTSCENE_ORIGIN(vHeliJumpCoord, fHeliJumpHeading, 0)
				START_CUTSCENE()
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehs[mvf_heli].id, TRUE, FALSE, FALSE)
				Mission_Set_Stage(msf_3_sky_dive_and_land)
			ENDIF
		BREAK
		CASE STAGE_EXIT
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE: Player skydives out the helicopter along with their heist team and lands on the FIB building.
PROC ST_3_Skydive_And_Land()

	// Stop player from messing about in the heli
	IF mission_substage < 3
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
	ENDIF

	SWITCH mission_substage
		CASE STAGE_ENTRY
			Load_Asset_Weapon_Asset(sAssetData, GADGETTYPE_PARACHUTE)
			
			IF NOT bExitStateCam
				IF NOT IS_CUTSCENE_ACTIVE()
				OR CAN_SET_EXIT_STATE_FOR_CAMERA()
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
					REPLAY_STOP_EVENT()
					
					i_heli_cam_timer 	= -1
					i_heli_cam_type 	= 0 
					bExitStateCam 		= TRUE
					CPRINTLN(DEBUG_MIKE, "CAM EXT STATE FIRED")
				ENDIF
			ENDIF
			
			IF NOT IS_CUTSCENE_ACTIVE()
			OR CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_Heli")
				
			ENDIF
			
			IF NOT bExitStateMichael
				IF NOT IS_CUTSCENE_ACTIVE()
				OR CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					
					IF i_NavMeshBlockingAreas[5] = -1
						i_NavMeshBlockingAreas[5] = ADD_NAVMESH_BLOCKING_OBJECT(<<135.997, -749.310, -20.1>>, <<17.550, 14.150, 5.350>>, DEG_TO_RAD(-20.1), FALSE)
					ENDIF
				
					TASK_HELI_MISSION(peds[mpf_pilot].id, vehs[mvf_heli].id, null, null, vHeliJumpCoord, MISSION_GOTO, 0.0, 0.0, fHeliJumpHeading, ROUND(vHeliJumpCoord.z), 0)
					SET_HELI_TURBULENCE_SCALAR(vehs[mvf_heli].id, 0.2)
				
					SET_ENTITY_NO_COLLISION_ENTITY(MIKE_PED_ID(), vehs[mvf_heli].id, FALSE)
				
					// Start michaels sky dive loop
					syncedScenes[ssf_IG1_sky_dive] = CREATE_SYNCHRONIZED_SCENE(<<0.0,0.0,0.0>>, <<0.0,0.0,0.0>>)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(syncedScenes[ssf_IG1_sky_dive], vehs[mvf_heli].id, -1)
					FLOAT fBlendIn
					IF IS_SCREEN_FADED_OUT()
						fBlendIn = INSTANT_BLEND_IN
					ELSE
						fBlendIn = SLOW_BLEND_IN
					ENDIF
					TASK_SYNCHRONIZED_SCENE(MIKE_PED_ID(), syncedScenes[ssf_IG1_sky_dive], animDict_IG1_sky_dive, anim_sky_dive_loop, fBlendIn, NORMAL_BLEND_OUT)
					SET_SYNCHRONIZED_SCENE_LOOPED(syncedScenes[ssf_IG1_sky_dive], TRUE)
					
					// REMOVE PLAYER CONTROLL DURING THE CAMERA CUT
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
					bExitStateMichael = TRUE
				ENDIF
			ENDIF
		
			IF bExitStateMichael
			AND bExitStateCam
			AND HAS_WEAPON_ASSET_LOADED(GADGETTYPE_PARACHUTE)
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "3. Skydive onto rooftop")
				
				DISABLE_CELLPHONE(TRUE)
				//SET_PED_COMPONENT_VARIATION(MIKE_PED_ID(), PED_COMP_SPECIAL2, 5, 0)
				SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED_ID(), COMP_TYPE_SPECIAL2, GET_PED_COMP_ITEM_FROM_VARIATIONS(MIKE_PED_ID(), 5, 0, COMP_TYPE_SPECIAL2), FALSE)
				
				// stop player from jumping out early during the cam cuts
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				
				Event_Trigger(mef_manage_parachute_marker)

				// New objective with blips, markers
				SAFE_BLIP_COORD(blip_Objective, vSkyDiveLandingCoord)
				CLEAR_HELP()
				
				REMOVE_PED_FROM_GROUP(BUDDY_PED_ID())
				REMOVE_PED_FROM_GROUP(peds[mpf_goon].id)
				REMOVE_PED_FROM_GROUP(peds[mpf_pilot].id)
				
				IF NOT HAS_PED_GOT_WEAPON(MIKE_PED_ID(), GADGETTYPE_PARACHUTE)
					GIVE_WEAPON_TO_PED(MIKE_PED_ID(), GADGETTYPE_PARACHUTE, 1, TRUE, FALSE)
				ELSE
					GET_PED_PARACHUTE_TINT_INDEX(MIKE_PED_ID(), i_SotredChuteColour_Mike)
				ENDIF
				IF NOT HAS_PED_GOT_WEAPON(FRANK_PED_ID(), GADGETTYPE_PARACHUTE)
					GIVE_WEAPON_TO_PED(FRANK_PED_ID(), GADGETTYPE_PARACHUTE, 1, TRUE, FALSE) 		
				ELSE
					GET_PED_PARACHUTE_TINT_INDEX(FRANK_PED_ID(), i_SotredChuteColour_Frank)
				ENDIF
				
				SET_PED_PARACHUTE_TINT_INDEX(FRANK_PED_ID(), 7)
				SET_PED_PARACHUTE_TINT_INDEX(MIKE_PED_ID(), 3)
				
				// Gunman parachute, give him brightly coloured stupid chute if bad gunman
				GIVE_WEAPON_TO_PED(peds[mpf_goon].id, GADGETTYPE_PARACHUTE, 1, TRUE, FALSE) 
				SWITCH GET_CREW_MEMBER_SKILL(crewGoon) 
					CASE CMSK_BAD				SET_PED_PARACHUTE_TINT_INDEX(peds[mpf_goon].id, 0)			BREAK
					CASE CMSK_MEDIUM			SET_PED_PARACHUTE_TINT_INDEX(peds[mpf_goon].id, 6)			BREAK
					CASE CMSK_GOOD				SET_PED_PARACHUTE_TINT_INDEX(peds[mpf_goon].id, 6)			BREAK
				ENDSWITCH
				
				
				CLEAR_PED_TASKS_IMMEDIATELY(peds[mpf_pilot].id)
				SET_PED_INTO_VEHICLE(peds[mpf_pilot].id, 	vehs[mvf_heli].id, VS_DRIVER)
				SET_HELI_BLADES_FULL_SPEED(vehs[mvf_heli].id)
				
				Event_Trigger(mef_manage_parachuting_buddies)

				SET_WANTED_LEVEL_MULTIPLIER(0.0)
				SET_MAX_WANTED_LEVEL(0)
				ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
				ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_SKYDIVE_PREPARE")
					START_AUDIO_SCENE("AH_3B_SKYDIVE_PREPARE")
				ENDIF
				
				Add_Peds_For_Dialogue()

				i_dialogue_stage 	= 0
				bBailHeliNow		= FALSE
				bFailAbandonPilot 	= FALSE
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				mission_substage++
			ENDIF
		BREAK
		CASE 1
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
		
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RLEFT)
				CPRINTLN(DEBUG_MIKE, "***AH3B: HELI BAIL PRESSED")
				
				bBailHeliNow = TRUE
			ENDIF
		
			// Only allow the jump once all the peds have the parachute
			IF bBailHeliNow
			AND HAS_PED_GOT_WEAPON(MIKE_PED_ID(), GADGETTYPE_PARACHUTE)
			
				CPRINTLN(DEBUG_MIKE, "***AH3B: TRYING TO BAIL NOW")
					
				IF NOT IS_SAFE_TO_START_CONVERSATION()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					CPRINTLN(DEBUG_MIKE, "***AH3B: BAIL - NOT SAFE TO START CONVO")
				ELSE
					CPRINTLN(DEBUG_MIKE, "***AH3B: BAIL - STARTING CONVO")
					IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_NOISES", CONV_PRIORITY_HIGH)
						CPRINTLN(DEBUG_MIKE, "***AH3B: BAIL - CONVO CREATED, BAILING NOW")
						Event_Trigger(mef_manage_parachuting)
						
						SET_ENTITY_NO_COLLISION_ENTITY(MIKE_PED_ID(), vehs[mvf_heli].id, FALSE)
						syncedScenes[ssf_IG1_sky_dive] = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(syncedScenes[ssf_IG1_sky_dive], vehs[mvf_heli].id, -1)
						TASK_SYNCHRONIZED_SCENE(MIKE_PED_ID(), syncedScenes[ssf_IG1_sky_dive], animDict_IG1_sky_dive, anim_sky_dive_jump, SLOW_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_PRESERVE_VELOCITY)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(syncedScenes[ssf_IG1_sky_dive], FALSE)

						Load_Asset_Interior(sAssetData, interior_fib03)
						
						IF IS_AUDIO_SCENE_ACTIVE("AH_3B_SKYDIVE_PREPARE")
							STOP_AUDIO_SCENE("AH_3B_SKYDIVE_PREPARE")
						ENDIF
						IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_SKYDIVE_FREEFALL")
							START_AUDIO_SCENE("AH_3B_SKYDIVE_FREEFALL")
						ENDIF
						
						//STOP_SOUND(sounds[sfx_heli_base_jump])
						//PLAY_SOUND_FROM_ENTITY(sounds[sfx_heli_base_jump], "BASEJUMPS_CHOPPER_WIND", vehs[mvf_heli].id)
						
						i_heli_cam_type = 2
						
						CLEAR_HELP()
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
						mission_substage++
					ENDIF
				ENDIF
				
			// manage updating the idle waiting for jump cam
			ELSE
				IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP_FOREVER("A3B_JHELP")
				ENDIF
			
				// Dialogue between the pilot and michael
				SWITCH i_dialogue_stage
					CASE 0
						IF GET_GAME_TIMER() - i_time_of_last_convo > 10000
							IF IS_SAFE_TO_START_CONVERSATION()
								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_ENCOURAGE", CONV_PRIORITY_HIGH)
									i_dialogue_stage++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF GET_GAME_TIMER() - i_time_of_last_convo > 10000
							IF IS_SAFE_TO_START_CONVERSATION()
								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_WAIT2", CONV_PRIORITY_HIGH)
									i_dialogue_stage = 0
								ENDIF
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
				
				
			ENDIF
		BREAK
		CASE 2
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
		
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG1_sky_dive])
			OR GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG1_sky_dive]) = 1.0
			
				SET_FOLLOW_PED_CAM_THIS_UPDATE("FOLLOW_PED_SKY_DIVING_CAMERA", 3000)
			
				TASK_SKY_DIVE(MIKE_PED_ID())
				FORCE_PED_MOTION_STATE(MIKE_PED_ID(), MS_PARACHUTING, FALSE, FAUS_CUTSCENE_EXIT)
				//SET_ENTITY_VELOCITY(MIKE_PED_ID(), <<1.874333,0.0,-6.243666>>)
				
				SCRIPT_OVERRIDES_WIND_ELEVATION(FALSE, HASH("WEATHER_TYPES_HIGH_ELEVATION_BASE_JUMP_HELI"))

				mission_substage++
			ENDIF
		BREAK
		CASE 3
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
		
			IF IS_PED_IN_PARACHUTE_FREE_FALL(MIKE_PED_ID())
			AND NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
			
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_FOOT ) = CAM_VIEW_MODE_FIRST_PERSON
					ANIMPOSTFX_PLAY( "CamPushInNeutral", 0, FALSE )
					PLAY_SOUND_FRONTEND( -1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET" )
					i_TimerA = GET_GAME_TIMER() + 300	
				ELSE
					i_TimerA = 0
				ENDIF
				
				mission_substage++

			ENDIF
		BREAK
		CASE 4
			IF GET_GAME_TIMER() > i_TimerA
			
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				DISABLE_CELLPHONE(FALSE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				FREEZE_ENTITY_POSITION(vehs[mvf_heli].id, FALSE)
				
				IF DOES_CAM_EXIST(cams[mcf_helijump1])
					DESTROY_CAM(cams[mcf_helijump1])
				ENDIF
				IF DOES_CAM_EXIST(cams[mcf_helijump2])
					DESTROY_CAM(cams[mcf_helijump2])
				ENDIF
				
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(AH3B_FREEFALL_TIME)
				Unload_Asset_Anim_Dict(sAssetData, animDict_IG1_sky_dive)

				// print the objective to land on the building
				PRINT_NOW("A3B_BAIL", DEFAULT_GOD_TEXT_TIME, 1)
				
				TRIGGER_MUSIC_EVENT("AH3B_JUMPED")
				PREPARE_MUSIC_EVENT("AH3B_LANDED")
				
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT( CAM_VIEW_MODE_CONTEXT_ON_FOOT ) = CAM_VIEW_MODE_FIRST_PERSON
					// Snap pitch for first person cam
					SET_GAMEPLAY_CAM_RELATIVE_PITCH( -80.9270 )
				ELSE 
					// dont print help for first person 
					PRINT_HELP("A3B_PARAHLP2")
				ENDIF
	
				i_dialogue_stage = 0
				mission_substage++
			ELSE
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
			ENDIF
		BREAK
		CASE 5
			IF bPlayerLandedSafely
				TRIGGER_MUSIC_EVENT("AH3B_LANDED")
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()

				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_SKYDIVE_PARACHUTE")
					STOP_AUDIO_SCENE("AH_3B_SKYDIVE_PARACHUTE")
				ENDIF
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_FOCUS_ON_LANDING_ZONE")
					STOP_AUDIO_SCENE("AH_3B_FOCUS_ON_LANDING_ZONE")
				ENDIF
				REPLAY_RECORD_BACK_FOR_TIME(8.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				mission_substage++
			ELSE
			
				IF NOT DOES_ENTITY_EXIST(vehs[mvf_heli].id)
				OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehs[mvf_heli].id) > 100
					STOP_SOUND(sounds[sfx_heli_base_jump])
				ENDIF
			
				SWITCH i_dialogue_stage
					CASE 0
						IF GET_CREW_MEMBER_SKILL(crewGoon) != CMSK_BAD
							i_dialogue_stage = -1
						ELSE
							IF GET_PED_PARACHUTE_STATE(peds[mpf_goon].id) = PPS_PARACHUTING
								IF IS_SAFE_TO_START_CONVERSATION()
									IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_CHUTE", CONV_PRIORITY_MEDIUM)
										str_dialogue_root = ""
										i_dialogue_stage++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF IS_SAFE_TO_START_CONVERSATION()
							str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewGoon, "AH_CHUTE")
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_MEDIUM)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					BREAK
					CASE 2
						IF IS_SAFE_TO_START_CONVERSATION()
							IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_CHUTE_FRK", CONV_PRIORITY_MEDIUM)
								i_dialogue_stage = -1
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			
				FLOAT f_Dist 
				f_Dist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vSkyDiveLandingCoord)
				IF NOT IS_CUTSCENE_ACTIVE()
					IF f_Dist < DEFAULT_CUTSCENE_LOAD_DIST
						REQUEST_CUTSCENE("AH_3B_MCS_2")
						Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, FRANK_PED_ID(), "Franklin")
						Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, peds[mpf_goon].id, "gunman_selection_1")
						Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, "gunman_selection_1", PED_COMP_FEET, 1, 0)
					ENDIF
				ELSE
					IF f_Dist > DEFAULT_CUTSCENE_UNLOAD_DIST
						REMOVE_CUTSCENE()
						Reset_Cutscene_Ped_Variation_Streaming(sCutscenePedVariationRegister)
					ENDIF
				ENDIF
				
				IF IS_GAMEPLAY_HINT_ACTIVE()
					IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_FOCUS_ON_LANDING_ZONE")
						START_AUDIO_SCENE("AH_3B_FOCUS_ON_LANDING_ZONE")
					ENDIF
				ELSE
					IF IS_AUDIO_SCENE_ACTIVE("AH_3B_FOCUS_ON_LANDING_ZONE")
						STOP_AUDIO_SCENE("AH_3B_FOCUS_ON_LANDING_ZONE")
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
		CASE 6
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_LANDS", CONV_PRIORITY_HIGH)
					mission_substage++
				ENDIF
			ENDIF
		BREAK
		CASE 7
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				SETTIMERB(0)
				mission_substage++
			ENDIF
		BREAK
		CASE 8
			// Player has landed safely
			IF bPlayerLandedSafely 
			AND NOT IS_PED_CLIMBING(MIKE_PED_ID())
			AND IS_ENTITY_IN_ANGLED_AREA(MIKE_PED_ID(), <<152.377975,-755.480103,261.660736>>, <<120.150558,-743.126526,264.414886>>, 27.750000)
			
			AND bFrankLandedSafely
			AND NOT IS_PED_CLIMBING(FRANK_PED_ID())
			AND IS_ENTITY_IN_ANGLED_AREA(FRANK_PED_ID(), <<140.735123,-736.567627,261.642853>>, <<136.519958,-748.427124,267.655090>>, 27.750000)
			
			AND bGoonLandedSafely
			AND NOT IS_PED_CLIMBING(peds[mpf_goon].id)
			AND IS_ENTITY_IN_ANGLED_AREA(peds[mpf_goon].id, <<140.735123,-736.567627,261.642853>>, <<136.519958,-748.427124,267.655090>>, 27.750000)
			
			AND HAS_CUTSCENE_LOADED()
			AND TIMERB() > 2000
				
				// Begin streaming the assets for michael trying to open the door
				//Load_Asset_Model(sAssetData, PROP_FIB_SKYLIGHT_PLUG)
				Load_Asset_Waypoint(sAssetData, wp_try_door)
				Load_Asset_AnimDict(sAssetData, animDict_IG2_try_door)
				Load_Asset_AnimDict(sAssetData, "missheistfbi3bleadinoutah_3b_mcs_2")
				Load_Asset_Weapon_Asset(sAssetData, WEAPONTYPE_STICKYBOMB)
				LOAD_CREW_WEAPON_ASSETS()
				
				SET_ENTITY_LOAD_COLLISION_FLAG(peds[mpf_frank].id, FALSE) 
				SET_PED_LOD_MULTIPLIER(peds[mpf_frank].id, 1.0)
				
				SET_ENTITY_LOAD_COLLISION_FLAG(peds[mpf_goon].id, FALSE) 
				SET_PED_LOD_MULTIPLIER(peds[mpf_goon].id, 1.0)
				
				REGISTER_ENTITY_FOR_CUTSCENE(FRANK_PED_ID(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_goon].id, "gunman_selection_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				
				SET_PED_STEALTH_MOVEMENT(peds[mpf_frank].id, FALSE)
				SET_PED_STEALTH_MOVEMENT(peds[mpf_goon].id, FALSE, "DEFAULT_ACTION")
				
				TASK_CLEAR_LOOK_AT(peds[mpf_frank].id)
				TASK_CLEAR_LOOK_AT(peds[mpf_goon].id)
				
				CLEAR_AREA_OF_PROJECTILES(<<138.90, -740.16, 263.24>>, 50.0)
				
				CLEAR_PRINTS()
				CLEAR_HELP()
							
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				START_CUTSCENE()
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)

				mission_substage++
			ENDIF
		BREAK
		CASE 9
			IF IS_CUTSCENE_PLAYING()
				CLEAR_AREA_OF_OBJECTS(<<136.09, -749.35, 265.61>>, 40.0)
				SAFE_REMOVE_BLIP(blip_Objective)
				mission_substage++
			ENDIF
		BREAK
		CASE 10
			IF NOT DOES_ENTITY_EXIST(objs[mof_hack_door_fake].id)
			OR NOT DOES_ENTITY_EXIST(objs[mof_hack_computer].id)
			OR NOT DOES_ENTITY_EXIST(objs[mof_hack_chair].id)
				Setup_Hack_Room()
			ENDIF
		
//			IF GET_CUTSCENE_TIME() >= 10778
//				
//			ENDIF
		
			IF GET_CUTSCENE_TIME() >= 14778
				TRIGGER_MUSIC_EVENT("AH3B_ON_FLOOR")
				bExitStateMichael 	= FALSE
				bExitStateFrank 	= FALSE
				bExitStateGoon		= FALSE

				IF NOT bSwappedGlass
					SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_ROOF, BUILDINGSTATE_CLEANUP)
					bSwappedGlass = TRUE						
				ENDIF
							
				mission_substage++
			ENDIF
		BREAK
		CASE 11
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				REPLAY_STOP_EVENT()
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				bExitStateCam = TRUE
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				bExitStateFrank = TRUE
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("gunman_selection_1")
				bExitStateGoon = TRUE
			ENDIF
			
			IF bExitStateFrank AND bExitStateGoon
				IF NOT Is_Event_Triggered(mef_leadout_MCS_2)
					Event_Trigger(mef_leadout_MCS_2)
				ENDIF
			ENDIF
			
			IF bExitStateCam AND NOT bExitStateMichael
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
		
			// Has jumped down into the building
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				
				REMOVE_WEAPON_FROM_PED(MIKE_PED_ID(), GADGETTYPE_PARACHUTE)
				CLEAR_PED_PARACHUTE_PACK_VARIATION(MIKE_PED_ID())
				
				GIVE_CREW_MEMBER_WEAPON(cmf_mike, TRUE, FALSE)
				SET_PED_FLASH_LIGHT(peds[mpf_mike], TRUE)
								
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxs[ptfx_sweat_shop])
					STOP_PARTICLE_FX_LOOPED(ptfxs[ptfx_sweat_shop])
				ENDIF
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_BREAK_SERVER_ROOM_DOOR")
					START_AUDIO_SCENE("AH_3B_BREAK_SERVER_ROOM_DOOR")
				ENDIF

				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				bExitStateMichael = TRUE
			ENDIF
			
			IF bExitStateMichael AND bExitStateFrank AND bExitStateGoon
				Mission_Set_Stage(msf_4_hack_the_computer)
			ENDIF
		BREAK
		CASE STAGE_EXIT
			SET_ENTITY_INVINCIBLE(peds[mpf_frank].id, FALSE)
			SET_ENTITY_INVINCIBLE(peds[mpf_goon].id, FALSE)
			
			DISPLAY_HUD(TRUE)
			DISPLAY_RADAR(TRUE)
		BREAK
	ENDSWITCH
	
	
	IF bExitStateCam
		SWITCH i_heli_cam_type
			CASE 0
				IF i_heli_cam_timer = -1
				OR GET_GAME_TIMER() - i_heli_cam_timer >= i_heli_cam_duration
					CPRINTLN(DEBUG_MIKE, "JUMP CAMS CREATED")
				
					IF NOT DOES_CAM_EXIST(cams[mcf_helijump1])
						cams[mcf_helijump1] = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
						SET_CAM_DEBUG_NAME(cams[mcf_helijump1], "HELI_JUMP_PAN_1")
						ATTACH_CAM_TO_ENTITY(cams[mcf_helijump1], vehs[mvf_heli].id, <<3.5822, 5.3029, -0.6527>>, TRUE)
						//POINT_CAM_AT_ENTITY(cams[mcf_helijump1], vehs[mvf_heli].id, <<2.4670, 2.5709, -0.1120>>, TRUE)
						POINT_CAM_AT_ENTITY(cams[mcf_helijump1], PLAYER_PED_ID(), <<0,0,0>>)//<<2.4670, 2.5709, -0.1120>>, TRUE)
						SET_CAM_FOV(cams[mcf_helijump1], 20.0) 
						//SHAKE_CAM(cams[mcf_helijump1], "HAND_SHAKE", 1.0)
					ENDIF
					
					IF NOT DOES_CAM_EXIST(cams[mcf_helijump2])
						cams[mcf_helijump2] = CREATE_CAMERA(CAMTYPE_SCRIPTED)
						SET_CAM_DEBUG_NAME(cams[mcf_helijump2], "HELI_JUMP_PAN_2")
						ATTACH_CAM_TO_ENTITY(cams[mcf_helijump2], vehs[mvf_heli].id, <<3.2847, 5.4571, -0.4892>>, TRUE)
						//POINT_CAM_AT_ENTITY(cams[mcf_helijump2], vehs[mvf_heli].id, <<2.1195, 2.7240, -0.0741>>, TRUE)
						POINT_CAM_AT_ENTITY(cams[mcf_helijump2], PLAYER_PED_ID(), <<0,0,0>>)// <<2.1195, 2.7240, -0.0741>>, TRUE)
						SET_CAM_FOV(cams[mcf_helijump2], 20.0) 
						//SHAKE_CAM(cams[mcf_helijump2], "HAND_SHAKE", 1.0)
					ENDIF
				
					SET_CAM_ACTIVE_WITH_INTERP(cams[mcf_helijump2], cams[mcf_helijump1], 7000)
					IF NOT IS_SCRIPT_GLOBAL_SHAKING()
						SHAKE_SCRIPT_GLOBAL("HAND_SHAKE", 1.0)
					ENDIF

					i_heli_cam_timer = GET_GAME_TIMER()
					i_heli_cam_duration = 7000
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					i_heli_cam_type++
				ENDIF
			BREAK
			CASE 1	
				IF GET_GAME_TIMER() - i_heli_cam_timer >= i_heli_cam_duration
					SET_CAM_ACTIVE_WITH_INTERP(cams[mcf_helijump1], cams[mcf_helijump2], 7000)
					
					i_heli_cam_timer = GET_GAME_TIMER()
					i_heli_cam_duration = 7000
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					i_heli_cam_type--
				ENDIF
			BREAK
			CASE 2
				IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG1_sky_dive])
				AND GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG1_sky_dive]) >= 0.3
				
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0) 
					SET_FOLLOW_PED_CAM_THIS_UPDATE("FOLLOW_PED_SKY_DIVING_CAMERA", 0)
					
					RENDER_SCRIPT_CAMS(FALSE, TRUE, 3000, FALSE)
					
					IF IS_SCRIPT_GLOBAL_SHAKING()
						STOP_SCRIPT_GLOBAL_SHAKING(FALSE)
					ENDIF
					
					i_heli_cam_timer = GET_GAME_TIMER()
					i_heli_cam_type++
				ENDIF
			BREAK
			CASE 3
				SET_FOLLOW_PED_CAM_THIS_UPDATE("FOLLOW_PED_SKY_DIVING_CAMERA", 0)
				IF IS_PED_IN_PARACHUTE_FREE_FALL(PLAYER_PED_ID())
					i_heli_cam_type++
				ENDIF
			BREAK
			CASE 4
				IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_DEPLOYING
				OR GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					i_heli_cam_type++
					
				ELIF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_SKYDIVING
					
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("A3B_PARAHLP2")
							CLEAR_HELP()
						ENDIF
					ELSE
						CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vSkyDiveLandingCoord, "", HINTTYPE_AGENCY_HEIST_3B_SKY_DIVING, FALSE, FALSE)
					ENDIF
				
				ENDIF
			BREAK
			CASE 5
				IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_PARACHUTING
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("A3B_PARAHLP2")
							CLEAR_HELP()
						ENDIF
					ELSE
						CONTROL_COORD_CHASE_HINT_CAM_ANY_MEANS(localChaseHintCamStruct, vSkyDiveLandingCoord)
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	
		IF (DOES_CAM_EXIST(cams[mcf_helijump1])
		AND IS_CAM_ACTIVE(cams[mcf_helijump1]))
		OR (DOES_CAM_EXIST(cams[mcf_helijump2])
		AND IS_CAM_ACTIVE(cams[mcf_helijump2]))
			HIDE_HUD_AND_RADAR_THIS_FRAME()
		ENDIF
	ENDIF
	
	// If player gets below here fail the mission as they are too low to land on the roof top
	VECTOR vPlayerCoord
	vPlayerCoord = GET_ENTITY_COORDS(MIKE_PED_ID())
	
	IF mission_substage > STAGE_ENTRY
	AND DOES_ENTITY_EXIST(vehs[mvf_heli].id)
	AND GET_DISTANCE_BETWEEN_COORDS(vPlayerCoord, GET_ENTITY_COORDS(vehs[mvf_heli].id)) >= 300
		DELETE_PED(peds[mpf_pilot].id)
		DELETE_VEHICLE(vehs[mvf_heli].id)
	ENDIF
	
ENDPROC

//PURPOSE: Player goes to the office and plays a hacking minigame at the computer, then initialising the document download.
PROC ST_4_Hack_The_Computer()

	IF mission_substage > 1
	AND NOT bFailOnMonitorDamage 
	
		IF iMonitorStartDamageFailCheck = -1
			iMonitorStartDamageFailCheck = GET_GAME_TIMER()
			
		ELIF GET_GAME_TIMER() - iMonitorStartDamageFailCheck > 1500
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(objs[mof_hack_computer].id)
			SET_ENTITY_INVINCIBLE(objs[mof_hack_computer].id, FALSE)
			bFailOnMonitorDamage = TRUE
			CPRINTLN(DEBUG_MIKE, "<<AGENCY3B>> FAIL TURNED ON FOR MONITOR")
		ENDIF
	ENDIF
	
	IF mission_substage >= 1 AND mission_substage <= 5
		DOWNLOAD_Draw_PC_Screen(FALSE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(BUDDY_PED_ID())
	AND NOT IS_PED_INJURED(BUDDY_PED_ID())
		SET_PED_RESET_FLAG(BUDDY_PED_ID(), PRF_BlockCustomAIEntryAnims, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
	AND NOT IS_PED_INJURED(peds[mpf_goon].id)
		SET_PED_RESET_FLAG(peds[mpf_goon].id, PRF_BlockCustomAIEntryAnims, TRUE)
	ENDIF

	SWITCH mission_substage
		CASE STAGE_ENTRY
			IF i_NavMeshBlockingAreas[5] != -1
				REMOVE_NAVMESH_BLOCKING_OBJECT(i_NavMeshBlockingAreas[5])
				i_NavMeshBlockingAreas[5] = -1
			ENDIF
		
			TRIGGER_MUSIC_EVENT("AH3B_INSIDE_BUILDING")

			//Unload_Asset_Interior(sAssetData, interior_fib03)
		
			IF IS_CUTSCENE_ACTIVE()
				REMOVE_CUTSCENE()
			ENDIF
			
			// stop the plume of smoke outside.
			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxs[ptfx_sweat_shop])
				STOP_PARTICLE_FX_LOOPED(ptfxs[ptfx_sweat_shop])
			ENDIF

			REQUEST_CUTSCENE("AH_3B_MCS_3")
			Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, MIKE_PED_ID(), "Michael")
			
			GIVE_WEAPON_TO_PED(MIKE_PED_ID(), WEAPONTYPE_STICKYBOMB, 1, FALSE, FALSE)
			SET_PED_AMMO(MIKE_PED_ID(), WEAPONTYPE_STICKYBOMB, 3)
			
			DOORS_Set_State(mdf_topfloor_main_l, TRUE, 0.0)
			DOORS_Set_State(mdf_topfloor_main_r, TRUE, 0.0)
			DOORS_Set_State(mdf_topfloor_fake_c, TRUE, 0.0)
			DOORS_Set_State(mdf_topfloor_fake_l, TRUE, 0.0)
			DOORS_Set_State(mdf_topfloor_fake_r, TRUE, 0.0)
			DOORS_Set_State(mdf_topfloor_pre_stairs, TRUE, 0.0)
			
			Load_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B)
			Load_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B_02)
			
			Load_Asset_PTFX(sAssetData)
			Load_Asset_Alarm(sAssetData, "AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER_B")
			
			Load_Asset_AnimDict(sAssetData, "missheist_agency3amcs_2")
			Load_Asset_Scaleform(sAssetData, "Hacking_PC", sfHacking, TRUE)
			REQUEST_HACKING_MINI_GAME(FALSE)
			
			// Hide the collision mesh placed over the skylight to prevent players from entering the FIB building
			//CREATE_MODEL_HIDE(<< 136.1795, -750.7010, 262.0516 >>, 10.0, V_ILEV_FIB_ATRCOL, TRUE)
			IF NOT bSwappedGlass			
				SET_BUILDING_STATE(BUILDINGNAME_IPL_FIB_ROOF, BUILDINGSTATE_CLEANUP)
			ENDIF
			
			i_dialogue_stage		= 0
			i_dialogue_substage		= 0
			bAlarmsTriggered 		= FALSE
			bExitStateMichael 		= FALSE
			bExitStateCam 			= FALSE
			bDownloadStarted 		= FALSE
			bFailOnMonitorDamage 	= FALSE
			i_ExplosiveTimer		= -1
			iMonitorStartDamageFailCheck = -1
			
			Event_Trigger(mef_ig2_try_door)
			SET_ENTITY_INVINCIBLE(objs[mof_hack_computer].id, TRUE)
			i_dialogue_stage 	= 0
			i_dialogue_substage = 0
			bPlayingKeyDialogue = TRUE

			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "4. Hack the computer")
			CLEAR_AREA_OF_OBJECTS(<<151.4373, -766.3517, 258.05>>, 3.0, CLEAROBJ_FLAG_FORCE) // clear the hack room to fix issue with there being 2 monitors left over
			
			mission_substage++
		BREAK
		CASE 1
			FLOAT fFragHealth
			fFragHealth = GET_OBJECT_FRAGMENT_DAMAGE_HEALTH(objs[mof_hack_door_fake].id, TRUE)
			
			CPRINTLN(DEBUG_MIKE, "AH3B: Hack door frag health - ", fFragHealth)
			CPRINTLN(DEBUG_MIKE, "AH3B: Hack door normal health - ", GET_ENTITY_HEALTH(objs[mof_hack_door_fake].id))
			
			IF fFragHealth < 1.0
			AND HAS_ANIM_DICT_LOADED("missheist_agency3amcs_2")
				
				START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_agency3b_linger_smoke", <<151.26, -762.57, 258.25>>, <<0,0,0>>, 1.0)
				ptfxs[ptfx_door_haze] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_agency_door_haze",  <<151.26, -762.57, 258.2>>, <<0,0,0>>, 1)
				SET_PED_USING_ACTION_MODE(MIKE_PED_ID(), 		TRUE)
				SET_PED_USING_ACTION_MODE(FRANK_PED_ID(), 		TRUE)
				SET_PED_USING_ACTION_MODE(peds[mpf_goon].id,	TRUE, -1, "DEFAULT_ACTION")
				
				CLEAR_PRINTS()
				CLEAR_HELP()
				PRINT_NOW("A3B_HACK", DEFAULT_GOD_TEXT_TIME, 1)
				IF DOES_BLIP_EXIST(blip_Objective)
					REMOVE_BLIP(blip_Objective)
				ENDIF
				blip_Objective = CREATE_BLIP_FOR_OBJECT(objs[mof_hack_computer].id)
				
				SET_BLIP_NAME_FROM_TEXT_FILE(blip_Objective, "A3B_COMP")
				
				syncedScenes[ssf_doorblownoff] = CREATE_SYNCHRONIZED_SCENE(<< 155.209, -765.206, 257.205 >>, << 0.000, 0.000, -110.000 >>)
				PLAY_SYNCHRONIZED_ENTITY_ANIM(objs[mof_hack_door_fake].id, syncedScenes[ssf_doorblownoff], "explode_doc_door_3b", "missheist_agency3amcs_2", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, enum_to_int(SYNCED_SCENE_DONT_INTERRUPT))
				
				Event_Finish(mef_ig2_try_door)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()

				iAlarmDelay = GET_GAME_TIMER() + 2500
				str_dialogue_root = ""
				mission_substage++
			ELSE
				SWITCH i_dialogue_stage
					CASE 0
						IF IS_SAFE_TO_START_CONVERSATION()
							IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_IAMIN", CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF IS_SAFE_TO_START_CONVERSATION()
							IF crewHacker = CM_HACKER_B_RICKIE_UNLOCK
								str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_SERLO", TRUE)
							ELSE
								str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_SERLO")
							ENDIF
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								SAFE_BLIP_COORD(blip_Objective, <<151.2583, -762.6251, 257.3701>>)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					BREAK
					CASE 2
						IF IS_SAFE_TO_DISPLAY_GODTEXT()
							PRINT_NOW("A3B_ACCESS", DEFAULT_GOD_TEXT_TIME, 1)
							bPlayingKeyDialogue = FALSE
							i_dialogue_stage++
						ENDIF
					BREAK
				ENDSWITCH
			
			
				//... check player still has the means to destroy the door
				IF Get_Ped_Explosive_Ammo_Count(MIKE_PED_ID()) = 0
					IF (NOT IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<112.915855,-741.613037,257.152527>>, <<158.823929,-757.627808,261.177002>>, 29.500000, WEAPONTYPE_STICKYBOMB, TRUE))
					AND (NOT IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<112.915855,-741.613037,257.152527>>, <<158.823929,-757.627808,261.177002>>, 29.500000, WEAPONTYPE_RPG, TRUE))
					AND (NOT IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<112.915855,-741.613037,257.152527>>, <<158.823929,-757.627808,261.177002>>, 29.500000, WEAPONTYPE_GRENADE, TRUE))
					AND (NOT IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<112.915855,-741.613037,257.152527>>, <<158.823929,-757.627808,261.177002>>, 29.500000, WEAPONTYPE_GRENADELAUNCHER, TRUE))
						IF i_ExplosiveTimer = -1
							i_ExplosiveTimer = GET_GAME_TIMER()
						ELIF GET_GAME_TIMER() - i_ExplosiveTimer > 2500
							Mission_Failed(mff_out_of_explosives)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_doorblownoff])
				SET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_doorblownoff], 1.0) 
				Unload_Asset_Anim_Dict(sAssetData, "missheist_agency3amcs_2")
			ENDIF
		
			IF DOES_ENTITY_EXIST(objs[mof_hack_computer].id)
			AND NOT HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(objs[mof_hack_computer].id)
			AND IS_ENTITY_IN_ANGLED_AREA(MIKE_PED_ID(), <<151.348389,-762.138611,257.152039>>, <<150.455841,-764.577698,259.526978>>, 2.312500)
			AND HAS_CUTSCENE_LOADED()
			AND HAS_MODEL_LOADED(V_ILEV_PROP_74_EMR_3B)
			AND HAS_MODEL_LOADED(V_ILEV_PROP_74_EMR_3B_02)
			AND PREPARE_ALARM("AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER_B")
			
				CLEAR_PRINTS()
				CLEAR_HELP()
			
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ELSE
					IF crewHacker = CM_HACKER_B_RICKIE_UNLOCK
						str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_MCS3", TRUE)
					ELSE
						str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_MCS3")
					ENDIF
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						SAFE_REMOVE_BLIP(blip_Objective)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)				
						
						REGISTER_ENTITY_FOR_CUTSCENE(null, "Michaels_phone", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, mod_hack_phone)
						REGISTER_ENTITY_FOR_CUTSCENE(objs[mof_hack_computer].id, "AH_monitor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						
						IF IS_AUDIO_SCENE_ACTIVE("AH_3B_BREAK_SERVER_ROOM_DOOR")
							STOP_AUDIO_SCENE("AH_3B_BREAK_SERVER_ROOM_DOOR")
						ENDIF
						
						IF NOT bAlarmsTriggered
							PLAY_ALARM_SECURITY(TRUE, FALSE, TRUE)
							Unload_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B)
							Unload_Asset_Model(sAssetData, V_ILEV_PROP_74_EMR_3B_02)
							bAlarmsTriggered = TRUE
						ENDIF
						
						REPLAY_RECORD_BACK_FOR_TIME(6.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
						
						START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						Event_Finish(mef_leadout_MCS_2)
						
						str_dialogue_root 		= ""
						bExitStateMichael 		= FALSE
						bExitStateCam 			= FALSE
						mission_substage++
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT bAlarmsTriggered
				IF GET_GAME_TIMER() > iAlarmDelay
				AND HAS_MODEL_LOADED(V_ILEV_PROP_74_EMR_3B)
				AND HAS_MODEL_LOADED(V_ILEV_PROP_74_EMR_3B_02)
				AND PREPARE_ALARM("AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER_B")
					PLAY_ALARM_SECURITY(TRUE, FALSE, TRUE)
					bAlarmsTriggered = TRUE
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF IS_CUTSCENE_PLAYING()
			
				//CLEAR_AREA(<< 151.40, -766.26, 258.05 >>, 1.0, TRUE)
				CLEAR_AREA_OF_PROJECTILES(<<151.308609,-766.122620,258.052216>>, 10.0)
				mission_substage++
			ENDIF
		BREAK
		CASE 4
			IF IS_CUTSCENE_PLAYING()
				IF NOT DOES_ENTITY_EXIST(objs[mof_hack_phone].id)
					ENTITY_INDEX tempEntity 
					tempEntity = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michaels_phone", mod_hack_phone)
					IF DOES_ENTITY_EXIST(tempEntity)
						objs[mof_hack_phone].id = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(tempEntity)
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				REPLAY_STOP_EVENT()
				bExitStateCam = TRUE
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				//sHackingData.b_is_hacking = TRUE
				SET_BIT(sHackingData.bsHacking,BS_IS_HACKING)
				RUN_HACKING_MINIGAME_WITH_PARAMETERS(sHackingData, 5, 5, 50, 10, 8, 0, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE )
				
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE,DEFAULT,FALSE)
				
				SET_ENTITY_COORDS_NO_OFFSET(objs[mof_hack_phone].id, v_hack_phone_coord)
				SET_ENTITY_ROTATION(objs[mof_hack_phone].id, v_hack_phone_rot)

				GIVE_CREW_MEMBER_WEAPON(cmf_mike, TRUE)
				GIVE_CREW_MEMBER_WEAPON(cmf_frank, TRUE)
				GIVE_CREW_MEMBER_WEAPON(cmf_goon, TRUE)
				
				SET_AMMO_IN_CLIP(MIKE_PED_ID(), WEAPONTYPE_CARBINERIFLE, GET_WEAPON_CLIP_SIZE(WEAPONTYPE_CARBINERIFLE))
				
				CLEAR_PED_TASKS(FRANK_PED_ID())
				CLEAR_PED_TASKS(peds[mpf_goon].id)
				
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(AH3B_HACKING_TIME)
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_HACKING_MG")
					START_AUDIO_SCENE("AH_3B_HACKING_MG")
				ENDIF
				
				TRIGGER_MUSIC_EVENT("AH3B_ON_PC")
				i_dialogue_stage 		= 0
				i_dialogue_substage		= 0
				bExitStateMichael = TRUE
			ENDIF
			
			IF bExitStateMichael
				SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
				i_prev_lives = sHackingData.i_lives
				bCheckingForBruteForceMistake = TRUE
				mission_substage++
			ENDIF
		BREAK
		CASE 5		
			
		#IF IS_DEBUG_BUILD
			BOOL bFinishNow
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_T)
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				fDownloadProgress = 0.1
				bFinishNow = TRUE
			ENDIF
		#ENDIF

			IF (HAS_PLAYER_BEAT_HACK_CONNECT(sHackingData) AND HAS_PLAYER_BEAT_BRUTEFORCE(sHackingData)
			AND HAS_DOWNLOAD_WINDOW_BEEN_OPENED(sHackingData))
		#IF IS_DEBUG_BUILD
			OR bFinishNow
		#ENDIF

				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND fDownloadProgress > 0.03
					
					IF HAS_CUTSCENE_LOADED()
					
						IF NOT IS_SAFE_TO_START_CONVERSATION()
							KILL_FACE_TO_FACE_CONVERSATION()
						ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF crewHacker = CM_HACKER_B_RICKIE_UNLOCK
								str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_MCS4", TRUE)
							ELSE
								str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_MCS4")
							ENDIF
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						
								IF IS_MINIGAME_IN_PROGRESS()
									SET_MINIGAME_IN_PROGRESS(FALSE)
								ENDIF
								SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
								
								Load_Asset_Model(sAssetData, mod_fib_agent_1)
								Load_Asset_Model(sAssetData, mod_fib_agent_2)
								Load_Asset_Model(sAssetData, mod_fib_agent_3)
								Load_Asset_Model(sAssetData, mod_swat)
								Load_Asset_Audiobank(sAssetData, "FBI_HEIST_3B_SHOOTOUT")
								Load_Asset_AnimDict(sAssetData, "facials@blend_out@ah_3b_mcs_4@michael@")
								
								CLEAR_HELP(TRUE)
								CLEAR_AREA(<<150.19, -762.98, 257.15>>, 3.25, TRUE)

								TRIGGER_MUSIC_EVENT("AH3B_HACKED_PC")
								
								IF IS_AUDIO_SCENE_ACTIVE("AH_3B_HACKING_MG")
									STOP_AUDIO_SCENE("AH_3B_HACKING_MG")
								ENDIF
								
								ADD_CREW_SHOOTOUT_COVER_POINTS()
								
								SET_ENTITY_COORDS(FRANK_PED_ID(), v_ShootoutCoverFrank)
								TASK_PUT_PED_DIRECTLY_INTO_COVER(FRANK_PED_ID(), v_ShootoutCoverFrank, -1, FALSE, 0, TRUE, TRUE, peds[mpf_frank].cov, FALSE)
								SET_ENTITY_COORDS(peds[mpf_goon].id, v_ShootoutCoverGoon)
								TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_goon].id, v_ShootoutCoverGoon, -1, FALSE, 0, TRUE, FALSE, peds[mpf_goon].cov, FALSE)
								
								REGISTER_ENTITY_FOR_CUTSCENE(objs[mof_hack_phone].id, "Michaels_phone", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								REGISTER_ENTITY_FOR_CUTSCENE(objs[mof_hack_computer].id, "AH_monitor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), WEAPONTYPE_CARBINERIFLE, TRUE)
								objs[mof_cutscene_weapon_mike].id = GET_WEAPON_OBJECT_FROM_PED(MIKE_PED_ID())
								REGISTER_ENTITY_FOR_CUTSCENE(objs[mof_cutscene_weapon_mike].id, "Michaels_2_Handedweapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
								
								IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxs[ptfx_door_haze])
									STOP_PARTICLE_FX_LOOPED(ptfxs[ptfx_door_haze])
								ENDIF
								
								Event_Finish(mef_leadout_MCS_2)
								
								bExitStateMichael 	= FALSE
								bExitStateCam 		= FALSE
								
								START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
								
								REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
								
								mission_substage++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				DOWNLOAD_Manage_File_Download(TRUE)
			ELSE
				IF NOT IS_CUTSCENE_ACTIVE()
					REQUEST_CUTSCENE("AH_3B_MCS_4")
				ENDIF
			
				// Process the hack connect
				IF NOT HAS_PLAYER_BEAT_HACK_CONNECT(sHackingData)
					RUN_HACKING_MINIGAME_WITH_PARAMETERS(sHackingData, 5, 5, 50, 10, 8, 0, FALSE, FALSE, FALSE, TRUE, TRUE, FALSE, TRUE, DEFAULT, FALSE )
					CDEBUG1LN(DEBUG_MIKE, "Running Hack Connect")
				
				// Process the brute force
				ELIF NOT HAS_PLAYER_BEAT_BRUTEFORCE(sHackingData)
					RUN_HACKING_MINIGAME_WITH_PARAMETERS(sHackingData, 5, 5, 77, 10, 8, 0, FALSE, FALSE, FALSE, TRUE, TRUE, FALSE, TRUE, DEFAULT, FALSE )
					CDEBUG1LN(DEBUG_MIKE, "Running Brute Force")
					
				// Process the download window
				ELIF NOT HAS_DOWNLOAD_WINDOW_BEEN_OPENED(sHackingData)
					RUN_HACKING_MINIGAME_WITH_PARAMETERS(sHackingData, 5, 5, 70, 10, 8, 0, FALSE, FALSE, FALSE, TRUE, TRUE, FALSE, TRUE, DEFAULT, FALSE )
				
					CDEBUG1LN(DEBUG_MIKE, "Running Search for download popup")
					
					IF HAS_DOWNLOAD_WINDOW_BEEN_OPENED(sHackingData)
						i_dialogue_stage = 0
						DOWNLOAD_Setup_File_Download()
						DOWNLOAD_Manage_File_Download(TRUE)
						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
						PLAY_SOUND_FROM_COORD(sounds[sfx_downloading], "HACKING_DOWNLOADING", GET_ENTITY_COORDS(objs[mof_hack_computer].id))
					ENDIF
				ENDIF
				
				HACKING_STAGE_ENUM eHackStage
				eHackStage = int_to_enum(HACKING_STAGE_ENUM, iThisHacking)
				
				TEXT_LABEL_15 strTemp
				
				
				
				IF NOT HAS_DOWNLOAD_WINDOW_BEEN_OPENED(sHackingData)
				
					// Hacking dialogue instructions
					SWITCH i_dialogue_stage
						CASE 0
							KILL_FACE_TO_FACE_CONVERSATION()
							i_dialogue_substage = 0
							i_dialogue_stage++
						BREAK
						CASE 1
							IF eHackStage = HACKSTAGE_DESKTOP
								IF IS_SAFE_TO_START_CONVERSATION()

									// Tells player to look in my computer
									IF i_dialogue_substage = 0
										IF crewHacker = CM_HACKER_B_RICKIE_UNLOCK
											strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_COMPa", TRUE)
										ELSE
											strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_COMPa")
										ENDIF
										
										IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
											i_dialogue_substage++
										ENDIF
									ENDIF
									
								ENDIF
								
							ELIF eHackStage = HACKSTAGE_MYCOMP
								KILL_FACE_TO_FACE_CONVERSATION()
								i_dialogue_substage = 0
								i_dialogue_stage++
							ENDIF
						BREAK
						CASE 2
							IF eHackStage = HACKSTAGE_MYCOMP
								IF IS_SAFE_TO_START_CONVERSATION()

									// Tells player to look in hack device
									IF i_dialogue_substage = 0
										IF crewHacker = CM_HACKER_B_RICKIE_UNLOCK
											strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_COMPb", TRUE)
										ELSE
											strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_COMPb")
										ENDIF
										IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
											i_dialogue_substage++
										ENDIF
									ENDIF
									
								ENDIF
								
							ELIF eHackStage = HACKSTAGE_PHONE
								KILL_FACE_TO_FACE_CONVERSATION()
								i_dialogue_substage = 0
								i_dialogue_stage++
							ENDIF
						BREAK
						CASE 3
							IF eHackStage = HACKSTAGE_PHONE

								IF IS_SAFE_TO_START_CONVERSATION()
							
									// Tells player to run the hack connect
									SWITCH i_dialogue_substage
										CASE 0
											IF crewHacker = CM_HACKER_B_RICKIE_UNLOCK
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_HACKa", TRUE)
											ELSE
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_HACKa")
											ENDIF
											IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
												i_dialogue_substage++
											ENDIF
										BREAK
										CASE 1
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_HACKb")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
										CASE 2
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_HACKc")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
									ENDSWITCH
								
								ENDIF
								
							ELIF eHackStage = HACKSTAGE_HCONNECT
								KILL_FACE_TO_FACE_CONVERSATION()
								i_dialogue_substage = 0
								i_dialogue_stage++
							ENDIF
						BREAK
						CASE 4
							IF eHackStage = HACKSTAGE_HCONNECT
							
								IF IS_SAFE_TO_START_CONVERSATION()
								
									// Gives player instructions to do the hack connect
									SWITCH i_dialogue_substage
										CASE 0
											IF crewHacker = CM_HACKER_B_RICKIE_UNLOCK
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_HELPa", TRUE)
											ELSE
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_HELPa")
											ENDIF
											IF DOES_CONVO_EXIST(strTemp)
												IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
													i_dialogue_substage++
												ENDIF
											ELSE
												i_dialogue_substage++
											ENDIF
										BREAK
										CASE 1
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												IF crewHacker = CM_HACKER_B_RICKIE_UNLOCK
													strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_HELPb", TRUE)
												ELSE
													strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_HELPb")
												ENDIF
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF	
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
										CASE 2
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_HELPc")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
										CASE 3
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_HELPd")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF	
												ENDIF
											ENDIF
										BREAK
										CASE 4
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_HELPe")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF	
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
										CASE 5
											IF GET_GAME_TIMER() - i_time_of_last_convo > 4000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_HELPf")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
									ENDSWITCH
								ENDIF
								
							// hack connect has closed....
							ELIF eHackStage != HACKSTAGE_HCONNECT
								
								// only progress if the player has beaten it otherwise wait
								IF HAS_PLAYER_BEAT_HACK_CONNECT( sHackingData )
									KILL_FACE_TO_FACE_CONVERSATION()
									i_dialogue_substage = 0
									i_dialogue_stage++
								ENDIF
								
							ENDIF
						BREAK
						CASE 5
							IF eHackStage = HACKSTAGE_PHONE
							OR eHackStage = HACKSTAGE_MYCOMP
							OR eHackStage = HACKSTAGE_DESKTOP
							OR eHackStage = HACKSTAGE_DELAY
							
								IF IS_SAFE_TO_START_CONVERSATION()
								
									// Tells player to use the brute force app
									SWITCH i_dialogue_substage
										CASE 0
											IF crewHacker = CM_HACKER_B_RICKIE_UNLOCK
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_BRUTa", TRUE)
											ELSE
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_BRUTa")
											ENDIF
											IF DOES_CONVO_EXIST(strTemp)
												IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
													i_dialogue_substage++
												ENDIF	
											ELSE
												i_dialogue_substage++
											ENDIF
										BREAK
										CASE 1
											IF GET_GAME_TIMER() - i_time_of_last_convo > 1000
												IF crewHacker = CM_HACKER_B_RICKIE_UNLOCK
													strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_BRUTb", TRUE)
												ELSE
													strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_BRUTb")
												ENDIF
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
									ENDSWITCH
									
								ENDIF
									
							ELIF eHackStage = HACKSTAGE_HBRUTE
								KILL_FACE_TO_FACE_CONVERSATION()
								i_dialogue_substage = 0
								i_dialogue_stage++
							ENDIF
						BREAK
						CASE 6
							IF eHackStage = HACKSTAGE_HBRUTE
							
								IF IS_SAFE_TO_START_CONVERSATION()
								
									// Gives player instructions for the brute force	
									SWITCH i_dialogue_substage
										CASE 0
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												IF crewHacker = CM_HACKER_B_RICKIE_UNLOCK
													strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_BRUTc", TRUE)
												ELSE
													strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_BRUTc")
												ENDIF
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF	
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
										CASE 1
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_BRUTd")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
										CASE 2
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_BRUTe")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
										CASE 3
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_BRUTf")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
										CASE 4
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_BRUTg")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
										CASE 5
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_BRUTh")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
										CASE 6
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_BRUTi")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
										CASE 7
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_BRUTj")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF	
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
										CASE 8
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_BRUTk")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
										CASE 9
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_BRUTl")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
									ENDSWITCH
								ENDIF
								
							ELIF eHackStage != HACKSTAGE_HBRUTE
								
								// only progress if the player has beaten it
								IF HAS_PLAYER_BEAT_BRUTEFORCE( sHackingData )
									KILL_FACE_TO_FACE_CONVERSATION()
									i_dialogue_substage = 0
									i_dialogue_stage++
								ENDIF
								
							ENDIF
						BREAK
						CASE 7
							IF eHackStage = HACKSTAGE_PHONE
							OR eHackStage = HACKSTAGE_MYCOMP
							OR eHackStage = HACKSTAGE_DESKTOP
							OR eHackStage = HACKSTAGE_DELAY
								
								IF IS_SAFE_TO_START_CONVERSATION()
							
									// Tells player to run the down and out (download)
									SWITCH i_dialogue_substage
										CASE 0
											IF crewHacker = CM_HACKER_B_RICKIE_UNLOCK
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_DWNa", TRUE)
											ELSE
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_DWNa")
											ENDIF
											IF DOES_CONVO_EXIST(strTemp)
												IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
													i_dialogue_substage++
												ENDIF
											ELSE
												i_dialogue_substage++
											ENDIF
										BREAK
										CASE 1
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_DWNb")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
										CASE 2
											IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
												strTemp = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_DWNc")
												IF DOES_CONVO_EXIST(strTemp)
													IF CREATE_CONVERSATION(sConvo, strDialogue, strTemp, CONV_PRIORITY_HIGH)
														i_dialogue_substage++
													ENDIF
												ELSE
													i_dialogue_substage++
												ENDIF
											ENDIF
										BREAK
									ENDSWITCH
								
								ENDIF
							
							ELIF eHackStage = HACKSTAGE_HDOWNLOAD
								KILL_FACE_TO_FACE_CONVERSATION()
								i_dialogue_substage = 0
								i_dialogue_stage++
							ENDIF
						BREAK
						
					ENDSWITCH
				ENDIF
				
				BOOL bMistakeMade
				
				// hack connect check
				IF sHackingData.i_lives != i_prev_lives
					bMistakeMade = TRUE
					i_prev_lives = sHackingData.i_lives
				ENDIF
				
				// Brute force check
				IF bCheckingForBruteForceMistake
					IF IS_BIT_SET(sHackingData.bsHacking,BS_GOT_COLUMN_WRONG)
					//IF sHackingData.bGotColumnWrong 
						bCheckingForBruteForceMistake = FALSE
						bMistakeMade = TRUE
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(sHackingData.bsHacking,BS_GOT_COLUMN_WRONG)
					//IF NOT sHackingData.bGotColumnWrong 
						bCheckingForBruteForceMistake = TRUE
					ENDIF
				ENDIF
				
				IF bMistakeMade
				
					INT i, iPrevValue
					REPEAT g_iMissionStatsBeingTracked i
						IF g_MissionStatTrackingArray[i].target = AH3B_HACKING_ERRORS
							IF iPrevValue = 0
								iPrevValue = g_MissionStatTrackingArray[i].ivalue
							ENDIF
						ENDIF
					ENDREPEAT
				
					INFORM_MISSION_STATS_OF_INCREMENT(AH3B_HACKING_ERRORS)
					CPRINTLN(DEBUG_MIKE, "INFORM_MISSION_STATS_OF_INCREMENT(AH3B_HACKING_ERRORS) from:", iPrevValue, " to: ", iPrevValue+1)
				ENDIF
			ENDIF
		BREAK
		CASE 6
		
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				REPLAY_STOP_EVENT()
				i_CamLimiterInterpStage = -1
				i_CamLimiterInterpTimer = 0
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(90)
				bExitStateCam = TRUE
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_2_Handedweapon")
				GIVE_WEAPON_OBJECT_TO_PED(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(objs[mof_cutscene_weapon_mike].id), MIKE_PED_ID())	
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_phone")
				SET_ENTITY_COORDS_NO_OFFSET(objs[mof_hack_phone].id, v_hack_phone_coord)
				SET_ENTITY_ROTATION(objs[mof_hack_phone].id, v_hack_phone_rot)
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")

				SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), WEAPONTYPE_CARBINERIFLE, TRUE)
				
				TASK_PLAY_ANIM(MIKE_PED_ID(), "facials@blend_out@ah_3b_mcs_4@michael@", "ah_3b_mcs_4_face", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_SECONDARY)
				
				Setup_Michael_In_Cover(FALSE)
				bExitStateMichael = TRUE

			ENDIF
			
			IF bExitStateCam
			AND bExitStateMichael
				Mission_Set_Stage(msf_5_download_shootout)
			ENDIF
			
			DOWNLOAD_Manage_File_Download(FALSE)
		BREAK
	ENDSWITCH
	
	
	IF mission_substage >= 6 OR mission_substage = STAGE_EXIT
		IF bExitStateMichael
			UPDATE_LOCKED_COVER_PED()
			SET_PED_CONFIG_FLAG(MIKE_PED_ID(), PCF_BlockPedFromTurningInCover, TRUE)
		ENDIF

		IF bExitStateCam
		AND NOT bExitStateMichael
			SET_GAMEPLAY_CAM_WORLD_HEADING(70.111)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(-3.8101)
		ENDIF
		
		IF bExitStateCam
		OR bExitStateMichael
			UPDATE_LOCKED_COVER_CAM_LIMITER()
		ENDIF
	ENDIF
	
ENDPROC


//PURPOSE: Player fights off incoming enemies while the documents download.
PROC ST_5_Download_Shootout()

	IF DOES_ENTITY_EXIST(BUDDY_PED_ID())
	AND NOT IS_PED_INJURED(BUDDY_PED_ID())
		SET_PED_RESET_FLAG(BUDDY_PED_ID(), PRF_BlockCustomAIEntryAnims, TRUE)
		MANAGE_AI_BUDDY_WEAPON()
	ENDIF
	
	IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
	AND NOT IS_PED_INJURED(peds[mpf_goon].id)
		SET_PED_RESET_FLAG(peds[mpf_goon].id, PRF_BlockCustomAIEntryAnims, TRUE)
	ENDIF
	
	IF NOT IS_ENTITY_PLAYING_ANIM(MIKE_PED_ID(), "facials@blend_out@ah_3b_mcs_4@michael@", "ah_3b_mcs_4_face")
		Unload_Asset_Anim_Dict(sAssetData, "facials@blend_out@ah_3b_mcs_4@michael@")
	ENDIF

	SWITCH mission_substage
		CASE STAGE_ENTRY
			bFailChecksOnHeli 		= FALSE
			bManageMichaelInCover 	= TRUE
			bManageShootoutAI		= TRUE
			#IF IS_DEBUG_BUILD
			i_debug_EnemiesSpawned	= 0
			#ENDIF
			i_EnemiesAlive			= 0
			i_LastPedKilledTimer	= -1
			bPlayingKeyDialogue		= FALSE
			bHasGasGrenadeGoneOff	= FALSE
			
			PREP_HOTSWAP(FALSE, FALSE, TRUE)
			Event_Trigger(mef_manage_switch)
			
			//B* 2136110: Manually set the post-cutscene blinders off
			SET_MULTIHEAD_SAFE(FALSE)

			SET_ENTITY_INVINCIBLE(peds[mpf_goon].id, TRUE)
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_ENEMIES_ARRIVE")
				START_AUDIO_SCENE("AH_3B_ENEMIES_ARRIVE")
			ENDIF
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING)
						
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "5. Downloading shootout")
			DOORS_Set_State(mdf_boardroom_l, TRUE, 1.0, 0)	// open the glass room door
			
			SETUP_FIB_COVERPOINTS(TRUE)
			
			str_dialogue_root 				= ""			
			i_dialogue_stage 				= 0
			i_shootout_dialogue_selector	= 0
			
			IF i_NavMeshBlockingAreas[0] = -1
				i_NavMeshBlockingAreas[0] = ADD_NAVMESH_BLOCKING_OBJECT(<<148.558, -744.107, 258.190>>, <<10.725, 2.550, 2.175>>, DEG_TO_RAD(69.267), FALSE)
			ENDIF
			IF i_NavMeshBlockingAreas[1] = -1
				i_NavMeshBlockingAreas[1] = ADD_NAVMESH_BLOCKING_OBJECT(<<144.094, -757.919, 258.219>>, <<2.175, 2.550, 2.175>>, DEG_TO_RAD(69.267), FALSE)
			ENDIF
			
			//INCREMENT_HACKER_STATS_DURING_HEIST(HEIST_AGENCY, crewHacker)
			
			mission_substage++
		BREAK
		
//		CASE 1
//		
//			bStartCrashCutscene = TRUE
//			Event_Trigger(mef_heli_crash_cutscene)
//		
//		BREAK

//  TRIGGER VARIOUS PEDS TO STORM IN AT DIFFERENT STAGES
//-----------------------------------------------------------
		CASE 1
		// First wave comes in once player has swapped to franklin
			IF swState = SWITCH_NOSTATE
			AND HAS_MODEL_LOADED(mod_fib_agent_1)

				// kick off the shootout
				Event_Trigger(mef_manage_download_shootout_spawning)
				Event_Trigger(mef_manage_download_shootout_rooftop)
				Event_Trigger(mef_manage_shootout_doors)
				
				INT i
				REPEAT COUNT_OF(iRoofPedQueue) i
					iRoofPedQueue[i] = -1
				ENDREPEAT
				
				mission_substage++
			ENDIF
		BREAK
		CASE 2
			FLOAT fDoorL, fDoorR, fDoorOther
			BOOL bDummyBool

			DOORS_Get_State(mdf_topfloor_main_l, bDummyBool, fDoorL)
			DOORS_Get_State(mdf_topfloor_main_r, bDummyBool, fDoorR)
			DOORS_Get_State(mdf_topfloor_fake_c, bDummyBool, fDoorOther)
			DUMMY_REFERENCE_BOOL(bDummyBool)
		
			// It all kicks off
			IF ABSF(fDoorL) > 0.5
			OR ABSF(fDoorR) > 0.5
			OR ABSF(fDoorOther) > 0.5
			OR IS_PED_SHOOTING(BUDDY_PED_ID())
			OR IS_PED_SHOOTING(peds[mpf_goon].id)
				TRIGGER_MUSIC_EVENT("AH3B_ENEMIES_ARRIVE")
				mission_substage++
			ENDIF
		BREAK
		CASE 3
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_TAKE", CONV_PRIORITY_HIGH)
					mission_substage++
				ENDIF
			ENDIF
		BREAK
		CASE 4
			SWITCH i_dialogue_stage
				CASE 0
					IF GET_GAME_TIMER() - i_time_of_last_convo > 1000
						PLAY_AMBIENT_SPEECH_FROM_POSITION("AH3b_AJAA", "FBI2AGENT2", <<117.2720, -739.3178, 258.6033>>, SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
						i_dialogue_stage++
					ENDIF
				BREAK
				CASE 1
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_HOLDIN_M", CONV_PRIORITY_HIGH)
							mission_substage++
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		BREAK
		CASE 5
			IF IS_SAFE_TO_DISPLAY_GODTEXT()
				PRINT_NOW("A3B_HLDOFF", DEFAULT_GOD_TEXT_TIME, 1)
				KILL_FACE_TO_FACE_CONVERSATION()
				
				bStartCrashCutscene 			= FALSE
				bPlayingKeyDialogue 			= TRUE
				i_dialogue_stage 				= 0
				i_crash_cutscene_cam_timer 		= -1
				mission_substage++
			ENDIF
		BREAK
		CASE 6
			SWITCH i_dialogue_stage
				CASE 0
					IF IS_SAFE_TO_START_CONVERSATION()
					AND CREATE_CONVERSATION(sConvo, strDialogue, "AH_MICRADCP", CONV_PRIORITY_HIGH)
						Event_Trigger(mef_heli_crash_cutscene)
						str_dialogue_root = ""
						i_dialogue_stage++
					ENDIF
				BREAK
				CASE 1
					IF IS_SAFE_TO_START_CONVERSATION()
					AND CREATE_CONVERSATION(sConvo, strDialogue, "AH_PILOTAP", CONV_PRIORITY_HIGH)
						i_dialogue_stage++
					ENDIF
				BREAK
				CASE 2
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						bPlayingKeyDialogue = FALSE
						i_dialogue_stage++
					ENDIF
				BREAK
				CASE 3
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_heli].id)
					AND GET_TIME_POSITION_IN_RECORDING(vehs[mvf_heli].id) > 14000.0
						bPlayingKeyDialogue = TRUE
						KILL_FACE_TO_FACE_CONVERSATION()
						i_dialogue_stage++	
					ENDIF
				BREAK				
				CASE 4
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_FRANKSEE", CONV_PRIORITY_HIGH)
							i_dialogue_stage++	
						ENDIF
					ENDIF
				BREAK
				CASE 5
					IF IS_SAFE_TO_START_CONVERSATION()
					AND CREATE_CONVERSATION(sConvo, strDialogue, "AH_MICSEE", CONV_PRIORITY_HIGH)
						i_dialogue_stage++
					ENDIF
				BREAK
				CASE 6
					IF IS_SAFE_TO_START_CONVERSATION()
					AND CREATE_CONVERSATION(sConvo, strDialogue, "AH_PILOTHOV1", CONV_PRIORITY_HIGH)
						i_dialogue_stage++
					ENDIF
				BREAK
				CASE 7
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						bPlayingKeyDialogue = FALSE
						i_dialogue_timer 	= GET_GAME_TIMER()
						i_dialogue_stage++
					ENDIF
				BREAK
				CASE 8
					IF GET_GAME_TIMER() - i_dialogue_timer > 7000
						IF i_EnemiesAlive <= 3
						// dont bother, not a lot of enemies left
							bPlayingKeyDialogue = FALSE
							i_dialogue_stage = 13
						ELSE
							bPlayingKeyDialogue = TRUE
							i_dialogue_stage++
						ENDIF
					ENDIF
				BREAK
				CASE 9
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_PROGa", CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
							i_dialogue_stage++	
						ENDIF
					ENDIF
				BREAK
				CASE 10	
					IF IS_SAFE_TO_START_CONVERSATION()
						str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker,"AH_STILL")
						IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
							i_dialogue_stage++	
						ENDIF
					ENDIF
				BREAK
				CASE 11
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_PROGc", CONV_PRIORITY_HIGH)
							str_dialogue_root 		= ""
							i_dialogue_stage++	
						ENDIF
					ENDIF
				BREAK
				CASE 12
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_PILOTHOV2", CONV_PRIORITY_HIGH)
							str_dialogue_root 		= ""
							i_dialogue_stage++
						ENDIF
					ENDIF
				BREAK
				CASE 13
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						bPlayingKeyDialogue 	= FALSE
						i_dialogue_timer 		= GET_GAME_TIMER()
						i_dialogue_stage++
					ENDIF
				BREAK
			ENDSWITCH
			
			// Slow down the download as it approaches the halfway point
			IF fDownloadProgress >= 0.4
			AND fDownloadProgress <= 0.47
				FLOAT fAlpha
				fAlpha = (fDownloadProgress - 0.4)/(0.47 - 0.4)
				fDownloadSpeedMultiplier = LERP_FLOAT(1.0, 0.05, fAlpha)
			ENDIF
			
			IF i_EnemiesAlive = 0
			
				Event_Finish(mef_manage_download_shootout_rooftop)
			
				IF i_crash_cutscene_cam_timer = -1
					i_crash_cutscene_cam_timer = GET_GAME_TIMER()
				ENDIF

				IF ((i_dialogue_stage > 6 AND i_dialogue_stage < 9)
				OR i_dialogue_stage > 12)
				AND GET_GAME_TIMER() - i_crash_cutscene_cam_timer > 2000
				AND GET_GAME_TIMER() - i_dialogue_timer > 2000
					bStartCrashCutscene = TRUE
					mission_substage++
				ENDIF
			
			ENDIF
		BREAK
		CASE 7
		// once the heli has crashed trigger the gas grenade
			IF Is_Event_Complete(mef_heli_crash_cutscene)
				Event_Trigger(mef_gas_grenades)
				
				KILL_FACE_TO_FACE_CONVERSATION()
				bPlayingKeyDialogue 				= TRUE
				str_dialogue_root 					= ""
				i_dialogue_stage					= 0
				i_KillsRequiredToFinishShootout 	= -1

				mission_substage++
			ENDIF
		BREAK
		CASE 8
			IF PREPARE_ALARM("AGENCY_HEIST_FIB_TOWER_ALARMS")
			AND PREPARE_ALARM("AGENCY_HEIST_FIB_TOWER_ALARMS_UPPER") 
				PLAY_ALARM_FIRE(TRUE, FALSE) // kick off the fire alarms
			ENDIF
		
			SWITCH i_dialogue_stage
				CASE 0
					IF IS_SAFE_TO_START_CONVERSATION()
						str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_GONE")
						IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
							i_dialogue_stage++
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF IS_SAFE_TO_START_CONVERSATION()
					AND CREATE_CONVERSATION(sConvo, strDialogue, "AH_FRANREACT", CONV_PRIORITY_HIGH)
						i_dialogue_stage++
					ENDIF
				BREAK
				CASE 2
					IF IS_SAFE_TO_START_CONVERSATION()
					AND CREATE_CONVERSATION(sConvo, strDialogue, "AH_BACKUP", CONV_PRIORITY_HIGH)
						bPlayingKeyDialogue 	= FALSE
						i_dialogue_stage++
					ENDIF
				BREAK
				CASE 3
					IF bHasGasGrenadeGoneOff
						bPlayingKeyDialogue 	= TRUE
						IF IS_SAFE_TO_START_CONVERSATION()
							IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_GASS_F", CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 4
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_GASS_M", CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
							i_dialogue_stage++
						ENDIF
					ENDIF
				BREAK
				CASE 5
					IF IS_SAFE_TO_START_CONVERSATION()
						str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_FIRE")
						IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
							i_dialogue_stage++
						ENDIF
					ENDIF
				BREAK
				CASE 6	
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						bPlayingKeyDialogue = FALSE
						i_dialogue_stage++
					ENDIF
				BREAK
				CASE 7
					IF fDownloadProgress = 1.0
						REQUEST_CUTSCENE("AH_3B_MCS_5")
						PREPARE_MUSIC_EVENT("AH3B_DATA_FINISHED")
						Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, MIKE_PED_ID(), "Michael")
						bPlayingKeyDialogue = TRUE
						i_dialogue_timer = GET_GAME_TIMER() + 1000
						str_dialogue_root = ""
						i_dialogue_stage++
					ENDIF
				BREAK
				CASE 8
					IF GET_GAME_TIMER() > i_dialogue_timer
						IF IS_SAFE_TO_START_CONVERSATION()
							str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_DONE")
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)	
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 9
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_REACT", CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
							i_dialogue_timer = GET_GAME_TIMER()
							i_dialogue_stage++
						ENDIF
					ENDIF
				BREAK
				CASE 10
					IF i_EnemiesAlive <= 2
						i_KillsRequiredToFinishShootout = 0
					ELIF i_EnemiesAlive <= 3
						i_KillsRequiredToFinishShootout = 2
					ELSE
						i_KillsRequiredToFinishShootout = i_EnemiesAlive
					ENDIF
					i_KillsFinishShootoutStart = i_EnemiesAlive
					
					str_dialogue_root = ""
					IF i_KillsRequiredToFinishShootout > 0
						i_dialogue_stage++
					ELSE
						i_dialogue_stage = 13
					ENDIF
				BREAK
				CASE 11
					IF i_KillsFinishShootoutStart - i_EnemiesAlive < i_KillsRequiredToFinishShootout 
						IF IS_SAFE_TO_START_CONVERSATION()
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_CLENMY", FALSE, str_dialogue_root, TRUE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								i_dialogue_stage++
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 12
					IF i_KillsFinishShootoutStart - i_EnemiesAlive < i_KillsRequiredToFinishShootout 
						IF IS_SAFE_TO_DISPLAY_GODTEXT()
							PRINT_NOW("A3B_KILLFIB", DEFAULT_GOD_TEXT_TIME, 1)
							i_dialogue_stage++
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			
			// Speed it up toward the end if the enemies remaining are low
			IF i_dialogue_stage >= 6
			AND fDownloadProgress != 1.0
				IF i_EnemiesAlive <= 3
					FLOAT fAlpha 
					fAlpha = 1.0 - fDownloadProgress
					fDownloadSpeedMultiplier = LERP_FLOAT(1.0, 3.0, fAlpha)
				ENDIF
			ENDIF
			
			IF i_dialogue_stage >= 11
			AND i_KillsFinishShootoutStart - i_EnemiesAlive >= i_KillsRequiredToFinishShootout 
			AND GET_GAME_TIMER() - i_LastPedKilledTimer > 2000
			AND GET_GAME_TIMER() - i_dialogue_timer > 2000
			AND HAS_CUTSCENE_LOADED()
			AND swState = SWITCH_NOSTATE
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND PREPARE_MUSIC_EVENT("AH3B_DATA_FINISHED")

				SAFE_REMOVE_BLIP(peds[mpf_mike].objective_blip)

				bDownloadStarted		= FALSE
				bManageShootoutAI		= FALSE
				bManageMichaelInCover	= FALSE
				
				SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), 		FALSE)
				SET_ENTITY_INVINCIBLE(FRANK_PED_ID(), 		FALSE)
				SET_ENTITY_INVINCIBLE(peds[mpf_goon].id, 	FALSE)
				
				Load_Asset_Waypoint(sAssetData, wp_sw_0_frank)
				Load_Asset_Waypoint(sAssetData, wp_sw_0_goon)
				Load_Asset_AnimDict(sAssetData, animDict_IG3_kill_swat)
				
				CLEAR_HELP()
				
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				IF DOES_CAM_EXIST(cams[mcf_dc_progress])
					DESTROY_CAM(cams[mcf_dc_progress])
				ENDIF
				
				TRIGGER_MUSIC_EVENT("AH3B_DATA_FINISHED")
				
				SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), WEAPONTYPE_CARBINERIFLE, TRUE)
				objs[mof_cutscene_weapon_mike].id = GET_WEAPON_OBJECT_FROM_PED(MIKE_PED_ID())
				IF PLAYER_PED_ID() != MIKE_PED_ID()
					REGISTER_ENTITY_FOR_CUTSCENE(MIKE_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				REGISTER_ENTITY_FOR_CUTSCENE(objs[mof_cutscene_weapon_mike].id, "Michaels_2_Handedweapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(objs[mof_hack_phone].id, "Michaels_phone", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(objs[mof_hack_computer].id, "AH_monitor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_SHOOTOUT_CONTINUED")
					STOP_AUDIO_SCENE("AH_3B_SHOOTOUT_CONTINUED")
				ENDIF
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_SHOOTOUT_SMOKE_BOMBS")
					STOP_AUDIO_SCENE("AH_3B_SHOOTOUT_SMOKE_BOMBS")
				ENDIF 
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_COMPUTER_FOCUS_CAM")
					STOP_AUDIO_SCENE("AH_3B_COMPUTER_FOCUS_CAM")
				ENDIF
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				//INFORM_MISSION_STATS_OF_INCREMENT(AH3B_ITEMS_STOLEN)
				
				bExitStateCam 		= FALSE
				bExitStateMichael	= FALSE
				bExitStateFrank		= FALSE
				bExitStateGoon		= FALSE
				
				IF PLAYER_PED_ID() != MIKE_PED_ID()
					IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
					ENDIF
					SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(FRANK_PED_ID())
					SET_PED_RESET_FLAG(FRANK_PED_ID(), PRF_ForceExtraLongBlendInForPedSkipIdleCoverTransition, TRUE)
				ENDIF
				
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WaitingForPlayerControlInterrupt, FALSE)
				
				mission_substage++
				
			ENDIF
		BREAK
		CASE 9
			IF IS_CUTSCENE_PLAYING()
			
				DISABLE_NEAR_CLIP_SCAN_THIS_UPDATE()
			
				// Warp franklin out the way just in case
				SET_ENTITY_COORDS(FRANK_PED_ID(), <<148.3118, -751.8849, 257.1696>>)
				SET_ENTITY_HEADING(FRANK_PED_ID(), 70.3502)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(FRANK_PED_ID(), <<148.3118, -751.8849, 257.1696>>, -1, FALSE, 0, TRUE, TRUE, NULL, FALSE)
				
				DOORS_Set_State(mdf_topfloor_main_l, TRUE, 0.0)
				DOORS_Set_State(mdf_topfloor_main_r, TRUE, 0.0)
				
				INT i
				FOR i = ENUM_TO_INT(mpf_download_1) TO ENUM_TO_INT(mpf_download_24)
					IF DOES_ENTITY_EXIST(peds[i].id)
						DELETE_PED(peds[i].id)
					ENDIF
				ENDFOR
				
				mission_substage++
			ENDIF
		BREAK
		CASE 10
			IF IS_CUTSCENE_PLAYING()
				
				DISABLE_NEAR_CLIP_SCAN_THIS_UPDATE()
			
				IF GET_CUTSCENE_TIME() >= 5300
				AND GET_IS_WAYPOINT_RECORDING_LOADED(wp_sw_0_frank)
				AND GET_IS_WAYPOINT_RECORDING_LOADED(wp_sw_0_goon)
					VECTOR vStartCoord
					
					CLEAR_PED_TASKS(FRANK_PED_ID())
					CLEAR_PED_TASKS(peds[mpf_goon].id)
					
					WAYPOINT_RECORDING_GET_COORD(wp_sw_0_frank, I_FRANK_WP_START, vStartCoord)
					SET_ENTITY_COORDS(FRANK_PED_ID(), vStartCoord)
					SET_ENTITY_HEADING(FRANK_PED_ID(), 74.7167)
					SET_CURRENT_PED_WEAPON(FRANK_PED_ID(), weap_Frank, TRUE)
					SET_PED_AMMO(FRANK_PED_ID(), weap_Frank, ammo_Frank)
					
//					WAYPOINT_RECORDING_GET_COORD(wp_sw_0_goon, I_GOON_WP_START, vStartCoord)
//					SET_ENTITY_COORDS(peds[mpf_goon].id, vStartCoord)
//					SET_ENTITY_HEADING(peds[mpf_goon].id, 68.4930)
					SET_CURRENT_PED_WEAPON(peds[mpf_goon].id, weap_Goon, TRUE)
					SET_PED_AMMO(peds[mpf_goon].id, weap_Frank, ammo_Goon)
				
					Event_Trigger(mef_ig3_swat_takedown)	
					mission_substage++
					
				ELIF GET_CUTSCENE_TIME() >= 1200
					DOWNLOAD_Draw_PC_Screen(FALSE, TRUE)
				ELSE
					DOWNLOAD_Draw_PC_Screen(FALSE, FALSE)
				ENDIF
			ENDIF
		BREAK
		CASE 11
		
			DISABLE_NEAR_CLIP_SCAN_THIS_UPDATE()
			
			IF NOT bExitStateCam
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					REPLAY_STOP_EVENT()
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					bExitStateCam = TRUE
				ENDIF
			ENDIF
			
			IF bExitStateCam
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(MIKE_PED_ID())
			ENDIF
			
			IF NOT bExitStateFrank
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("AH_Monitor")
					bExitStateFrank = TRUE
				ENDIF
			ENDIF

			IF NOT bExitStateGoon
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_2_Handedweapon")
					GIVE_WEAPON_OBJECT_TO_PED(objs[mof_cutscene_weapon_mike].id, MIKE_PED_ID())
					bExitStateGoon = TRUE
				ENDIF
			ENDIF
		
			IF NOT bExitStateMichael
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), WEAPONTYPE_CARBINERIFLE, TRUE)
					
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_RUN, 2000)
					bExitStateMichael = TRUE
				ENDIF
			ENDIF
			
			IF bExitStateMichael
			AND bExitStateCam
			AND bExitStateFrank
			AND bExitStateGoon
			
				SET_OBJECT_AS_NO_LONGER_NEEDED(objs[mof_hack_computer].id)
				SET_OBJECT_AS_NO_LONGER_NEEDED(objs[mof_hack_chair].id)
				
				Unload_Asset_Scaleform(sAssetData, sfHacking)
				RESET_HACKING_DATA(sHackingData, FALSE, TRUE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				Mission_Set_Stage(msf_6_stairwell)
			ENDIF
		BREAK
		CASE STAGE_EXIT
			DESTROY_CAM(cams[mcf_dc_progress])
			
			IF NOT IS_PED_INJURED(peds[mpf_goon].id)
				SET_ENTITY_INVINCIBLE(peds[mpf_goon].id, FALSE)
			ENDIF
		BREAK
	ENDSWITCH
	
	IF mission_substage != STAGE_EXIT

		DOWNLOAD_Manage_File_Download()
		
		IF bManageMichaelInCover		
			UPDATE_LOCKED_COVER_PED()		
			UPDATE_LOCKED_COVER_CAM_LIMITER()
			SET_PED_CONFIG_FLAG(MIKE_PED_ID(), PCF_BlockPedFromTurningInCover, TRUE)
		ELSE
			SET_PED_CONFIG_FLAG(MIKE_PED_ID(), PCF_BlockPedFromTurningInCover, FALSE)
		ENDIF
		
		IF bManageShootoutAI 				
			
			// Manage the peds AI tasks
			Manage_Shootout_AI()		
		
			// Check key dialogue is not playing
			IF NOT bPlayingKeyDialogue
			AND mission_substage > 5
			AND (i_crash_cutscene_cam_stage = 0 OR i_crash_cutscene_cam_stage = 99)
			AND IS_SAFE_TO_START_CONVERSATION()
			
				// Track where all the enemies are during the shootout so we can tell the player via dialogue
				INT i_EnemiesLeft, i_EnemiesRight, i_EnemiesMain, i_EnemiesRoofLeft, i_EnemiesRoofRight, i_EnemiesRoofCentre
				IF GET_GAME_TIMER() > i_EnemyLocationCheckTimer
				
					MISSION_PED_FLAGS ePed
					FOR ePed = mpf_download_1 TO mpf_post_heli_crash_4
						
						IF peds[ePed].id != NULL
						AND DOES_ENTITY_EXIST(peds[ePed].id)
						AND NOT IS_PED_INJURED(peds[ePed].id)
					
							// LEFT GLASS ROOM
							IF GET_ROOM_KEY_FROM_ENTITY(peds[ePed].id) 		= HASH("v_fib03_atr_off2")
								i_EnemiesLeft++
							// RIGHT GLASS ROOM
							ELIF GET_ROOM_KEY_FROM_ENTITY(peds[ePed].id) 	= HASH("v_fib03_atr_off1")
								i_EnemiesRight++
							// MAIN ROOM
							ELIF GET_ROOM_KEY_FROM_ENTITY(peds[ePed].id) 	= HASH("v_fib03_atr_hall")
							OR GET_ROOM_KEY_FROM_ENTITY(peds[ePed].id) 		= HASH("v_fib03_atr_hall001")
							OR GET_ROOM_KEY_FROM_ENTITY(peds[ePed].id) 		= HASH("v_fib03_atr_hall002")
								i_EnemiesMain++
							// ROOFTOP CENTRE
							ELIF IS_ENTITY_IN_ANGLED_AREA(peds[ePed].id, <<123.566696,-751.856262,261.851440>>, <<128.167252,-739.273560,264.589264>>, 6.187500)
								i_EnemiesRoofCentre++
							// ROOFTOP LEFT
							ELIF IS_ENTITY_IN_ANGLED_AREA(peds[ePed].id, <<125.190979,-755.068909,261.605316>>, <<138.043854,-760.011719,264.561401>>, 6.187500)
								i_EnemiesRoofLeft++
							// ROOFTOP RIGHT
							ELIF IS_ENTITY_IN_ANGLED_AREA(peds[ePed].id, <<144.125275,-742.828918,261.599609>>, <<131.941833,-738.044312,264.588745>>, 6.187500)
								i_EnemiesRoofRight++
							ENDIF
							
						ENDIF
						
					ENDFOR
				ENDIF
				
				// Select dialogue to play
				IF IS_STRING_NULL_OR_EMPTY(str_dialogue_root)
				
					SWITCH i_shootout_dialogue_selector
					// Tell player where the enemies are
						CASE 0
							IF GET_GAME_TIMER() - i_time_of_last_convo < 2000 // give it 2 seconds to try and tell the player where the enemies are
							
								IF i_EnemiesAlive > 0
								
									INT iAreaToTellPlayerAbout 
									iAreaToTellPlayerAbout = -1
									
									INT iMostCount
									INT iMostIncreaseCount
									
									IF bUseMostIncrease
										
										IF i_EnemiesLeft - i_EnemiesLeftPrev > iMostIncreaseCount
											iMostIncreaseCount 			= i_EnemiesLeft - i_EnemiesLeftPrev
											iAreaToTellPlayerAbout		= 0
										ENDIF
										IF i_EnemiesRight - i_EnemiesRightPrev > iMostIncreaseCount
											iMostIncreaseCount 			= i_EnemiesRight - i_EnemiesRightPrev
											iAreaToTellPlayerAbout		= 1
										ENDIF
										IF i_EnemiesMain - i_EnemiesMainPrev > iMostIncreaseCount
											iMostIncreaseCount 			= i_EnemiesMain - i_EnemiesMainPrev
											iAreaToTellPlayerAbout		= 2
										ENDIF
										IF i_EnemiesRoofCentre - i_EnemiesRoofCentrePrev > iMostIncreaseCount
											iMostIncreaseCount 			= i_EnemiesRoofCentre - i_EnemiesRoofCentrePrev
											iAreaToTellPlayerAbout		= 3
										ENDIF
										IF i_EnemiesRoofLeft - i_EnemiesRoofLeftPrev > iMostIncreaseCount
											iMostIncreaseCount 			= i_EnemiesRoofLeft - i_EnemiesRoofLeftPrev
											iAreaToTellPlayerAbout		= 4
										ENDIF
										IF i_EnemiesRoofRight - i_EnemiesRoofRightPrev > iMostIncreaseCount
											iMostIncreaseCount 			= i_EnemiesRoofRight - i_EnemiesRoofRightPrev
											iAreaToTellPlayerAbout		= 5
										ENDIF
									
									ENDIF
									
									// if not using the most increate OR if the most increase count is zero then
									// spit lines about which area has the most enemies
									IF NOT bUseMostIncrease
									OR iMostIncreaseCount = 0

										IF i_EnemiesLeft > iMostCount
											iMostCount 					= i_EnemiesLeft
											iAreaToTellPlayerAbout		= 0
										ENDIF
										IF i_EnemiesRight > iMostCount
											iMostCount 					= i_EnemiesRight
											iAreaToTellPlayerAbout		= 1
										ENDIF
										IF i_EnemiesMain > iMostCount
											iMostCount 					= i_EnemiesMain
											iAreaToTellPlayerAbout		= 2
										ENDIF
										IF i_EnemiesRoofCentre > iMostCount
											iMostCount 					= i_EnemiesRoofCentre
											iAreaToTellPlayerAbout		= 3
										ENDIF
										IF i_EnemiesRoofLeft > iMostCount
											iMostCount 					= i_EnemiesRoofLeft
											iAreaToTellPlayerAbout		= 4
										ENDIF
										IF i_EnemiesRoofRight > iMostCount
											iMostCount 					= i_EnemiesRoofRight
											iAreaToTellPlayerAbout		= 5
										ENDIF
										
									ENDIF
									
									SWITCH iAreaToTellPlayerAbout
										CASE 0		GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_LEFT", 	FALSE, str_dialogue_root) 		BREAK
										CASE 1		GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_RIGHT", 	FALSE, str_dialogue_root) 		BREAK
										CASE 2		GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_FLOOD", 	FALSE, str_dialogue_root) 		BREAK
										CASE 3		FALLTHRU
										CASE 4		FALLTHRU
										CASE 5
											IF NOT bSightedOnRoof
												GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_ROOF", 	FALSE, str_dialogue_root, FALSE, TRUE, TRUE, FALSE)
												bSightedOnRoof = TRUE
											ELSE
												SWITCH iAreaToTellPlayerAbout
													CASE 3		GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_ROOF_C", 	FALSE, str_dialogue_root) 		BREAK
													CASE 4 		GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_ROOF_L", 	FALSE, str_dialogue_root) 		BREAK
													CASE 5		GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_ROOF_R", 	FALSE, str_dialogue_root) 		BREAK
												ENDSWITCH
											ENDIF
										BREAK
										DEFAULT		str_dialogue_root = ""													BREAK
									ENDSWITCH
									
									IF NOT IS_STRING_NULL_OR_EMPTY(str_dialogue_root)
										bUseMostIncrease = !bUseMostIncrease
										i_shootout_dialogue_selector++
									ENDIF
									
								ENDIF
								
							ELSE
								str_dialogue_root = ""
								i_shootout_dialogue_selector++
							ENDIF
						BREAK
					// Shout an aggressive shootout line
						CASE 1
							IF ((i_fps_state = DC_PROGRESS_CAM_CURRENT
							AND PLAYER_PED_ID() = MIKE_PED_ID())
							OR i_EnemiesAlive = 0)
							AND fDownloadProgress < 1.0
								str_dialogue_root = "AH_WAITING"
							ELIF i_EnemiesAlive > 0
								GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_ATT", TRUE, str_dialogue_root)	
							ELSE
								str_dialogue_root = ""
							ENDIF
							i_shootout_dialogue_selector++
						BREAK
					ENDSWITCH 
				
				ENDIF
				
				// reset the selector
				IF i_shootout_dialogue_selector > 1
					i_shootout_dialogue_selector = 0
				ENDIF
				
				// play convo if one has been picked
				IF NOT IS_STRING_NULL_OR_EMPTY(str_dialogue_root)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF				
				
				// Store prev enemy count in each area, will be checked for increases
				IF GET_GAME_TIMER() > i_EnemyLocationCheckTimer
					
					i_EnemiesLeftPrev 		= i_EnemiesLeft
					i_EnemiesRightPrev		= i_EnemiesRight
					i_EnemiesMainPrev		= i_EnemiesMain
					i_EnemiesRoofCentrePrev	= i_EnemiesRoofCentre
					i_EnemiesRoofLeftPrev	= i_EnemiesRoofLeft
					i_EnemiesRoofRightPrev	= i_EnemiesRoofRight
					
					i_EnemyLocationCheckTimer = GET_GAME_TIMER() + 3000
				ENDIF

			ENDIF
			
			BOOL bEnemyNearCrew
			MISSION_PED_FLAGS ePed
			FOR ePed = mpf_download_1 TO mpf_post_heli_crash_4
				IF peds[ePed].id != NULL
				AND DOES_ENTITY_EXIST(peds[ePed].id)
				AND NOT IS_PED_INJURED(peds[ePed].id)
					IF GET_ROOM_KEY_FROM_ENTITY(peds[ePed].id) = HASH("V_FIB03_art_hall")
						bEnemyNearCrew = TRUE
					ENDIF
				ENDIF
			ENDFOR

			IF bEnemyNearCrew
				SET_PED_CONFIG_FLAG(BUDDY_PED_ID(), PCF_TreatAsPlayerDuringTargeting, TRUE)
				SET_PED_CONFIG_FLAG(peds[mpf_goon].id, PCF_TreatAsPlayerDuringTargeting, TRUE)
			ELSE
				SET_PED_CONFIG_FLAG(BUDDY_PED_ID(), PCF_TreatAsPlayerDuringTargeting, FALSE)
				SET_PED_CONFIG_FLAG(peds[mpf_goon].id, PCF_TreatAsPlayerDuringTargeting, FALSE)
			ENDIF
			
		ENDIF	
		
	ENDIF

ENDPROC

//PURPOSE: Player fights through and down the building to a lower floor.
PROC ST_6_Stairwell()

	IF DOES_ENTITY_EXIST(BUDDY_PED_ID())
	AND NOT IS_PED_INJURED(BUDDY_PED_ID())
		SET_PED_COMBAT_ATTRIBUTES(BUDDY_PED_ID(), CA_DISABLE_PINNED_DOWN, TRUE)
		MANAGE_AI_BUDDY_WEAPON()
	ENDIF
	
	IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
	AND NOT IS_PED_INJURED(peds[mpf_goon].id)
		SET_PED_COMBAT_ATTRIBUTES(peds[mpf_goon].id, CA_DISABLE_PINNED_DOWN, TRUE)
		SET_PED_RESET_FLAG(peds[mpf_goon].id, PRF_BlockCustomAIEntryAnims, TRUE)
	ENDIF

	SWITCH mission_substage
		CASE STAGE_ENTRY	
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6, "6. Stairwell")
		
			DOORS_Set_State(mdf_stairwell_53, TRUE, 0.0)
			DOORS_Set_State(mdf_stairwell_52, TRUE, 0.0)
			DOORS_Set_State(mdf_stairwell_47, TRUE, 0.0)
			DOORS_Set_State(mdf_topfloor_main_l, TRUE, 0.0)
			DOORS_Set_State(mdf_topfloor_main_r, TRUE, 0.0)
			DOORS_Set_State(mdf_topfloor_pre_stairs, TRUE, 0.0)
			
			Event_Finish(mef_manage_shootout_doors)
			Event_Finish(mef_manage_download_shootout_spawning)
			Event_Finish(mef_manage_download_shootout_rooftop)
			
			DOORS_LockOpenNonAnimDoors()
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
			
			SET_RAGDOLL_BLOCKING_FLAGS(MIKE_PED_ID(), RBF_NONE)
			SET_PED_CONFIG_FLAG(MIKE_PED_ID(), PCF_DisableExplosionReactions, FALSE)
			
			Unload_Asset_Audio_Bank(sAssetData, "FBI_HEIST_3B_SHOOTOUT")
			
			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxs[ptfx_sweat_shop])
				ptfxs[ptfx_sweat_shop] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_agency3b_blding_smoke", vSweatShopSmokeCoord, vSweatShopSmokeRot, fSweatShopSmokeScale, FALSE, FALSE, FALSE)
			ENDIF
			
			ADD_COVER_BLOCKING_AREA( <<128.586212,-731.152893,254.027054>>-<<0.562500,0.812500,1.000000>>, <<128.586212,-731.152893,254.027054>>+<<0.562500,0.812500,1.000000>>, TRUE, TRUE, TRUE, TRUE )
			
			REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
			
			//INCREMENT_GUNMAN_STATS_DURING_HEIST(HEIST_AGENCY, crewGoon)
			str_dialogue_root	= ""	
			i_dialogue_stage	= 0
			i_AudioSceneStage 	= 0
			mission_substage++
		BREAK
		CASE 1
			IF IS_SAFE_TO_DISPLAY_GODTEXT()
				PRINT_NOW("A3B_ESCAPE", DEFAULT_GOD_TEXT_TIME, 1)
				SAFE_BLIP_COORD(blip_Objective, << 117.8241, -735.6005, 257.1526 >>)
				mission_substage++
			ENDIF
		BREAK
		CASE 2
			IF Is_Event_Complete(mef_ig3_swat_takedown)
				bRetaskBuddy = TRUE
				bRetaskGunman = TRUE
				Event_Trigger(mef_ig4_kick_door)
				Load_Asset_AssistedLine(sAssetData, "8_FIBStairs")
				Load_Asset_AssistedLine(sAssetData, "off_stairs1")
				mission_substage++
			ENDIF
		BREAK
		CASE 3
			IF Is_Event_Complete(mef_ig4_kick_door)
				bRetaskBuddy = TRUE
				bRetaskGunman = TRUE
				Event_Trigger(mef_ig5_push_door)
				
				Load_Asset_Interior(sAssetData, interior_fib02)
				Load_Asset_Interior(sAssetData, interior_fib03)
				
				//Event_Trigger(mef_manage_fire_meshes)
				Event_Trigger(mef_manage_sprinklers)
				iDesiredSprinklerState = 1
				
				mission_substage++
			ENDIF
		BREAK
		CASE 4
			IF Is_Event_Complete(mef_ig5_push_door)	
				// move the blip to next bit
				SAFE_BLIP_COORD(blip_Objective, << 118.1325, -735.3059, 253.1525 >>)
				Load_Asset_Model(sAssetData, mod_fib_agent_2)
			
				// Task goon to outside burntout floor
				REMOVE_PED_DEFENSIVE_AREA(peds[mpf_goon].id)
				
				SAFE_OPEN_SEQUENCE()
					TASK_FOLLOW_NAV_MESH_TO_COORD(null, << 117.5880, -732.5183, 253.1525 >>, PEDMOVE_RUN)
					TASK_AIM_GUN_AT_COORD(null, << 117.4079, -735.4523, 254.5799 >>, -1, TRUE)
				SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
				
				// Task frank to outside the burntout floor
				
				SAFE_OPEN_SEQUENCE()
					IF IS_PED_IN_COVER(BUDDY_PED_ID())
						TASK_EXIT_COVER(null, CORNER_COVER_EXIT, <<0,0,0>>)
					ENDIF
					TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<119.8656, -731.6858, 253.1522>>, PEDMOVE_RUN)
					TASK_AIM_GUN_AT_COORD(null, << 118.0035, -735.9307, 254.6266 >>, -1, TRUE)
				SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
				
				SET_PED_COMBAT_ATTRIBUTES(BUDDY_PED_ID(), CA_DISABLE_PINNED_DOWN, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(peds[mpf_goon].id, CA_DISABLE_PINNED_DOWN, FALSE)
				
				mission_substage++
			ENDIF
		BREAK
		CASE 5
			IF IS_SAFE_TO_START_CONVERSATION()
				GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root)
				IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
					Mission_Set_Stage(msf_7_burnt_out_floor_1)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	SWITCH i_AudioSceneStage
		CASE 0
			IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_ESCAPE_START")
				START_AUDIO_SCENE("AH_3B_ESCAPE_START")
			ENDIF
			
			i_AudioSceneStage++
		BREAk
		CASE 1
			IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = GET_HASH_KEY("V_FIB04_st2")
			
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_ESCAPE_START")
					STOP_AUDIO_SCENE("AH_3B_ESCAPE_START")
				ENDIF
				IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_ESCAPE_STAIRWELL_01")
					START_AUDIO_SCENE("AH_3B_ESCAPE_STAIRWELL_01")
				ENDIF
			
				i_AudioSceneStage++
			ENDIF
		BREAK
		CASE 2
			FLOAT fOpenRatio
			BOOL bDummy
			
			DOORS_Get_State(mdf_stairwell_52, bDummy, fOpenRatio)
			IF ABSF(fOpenRatio) >= 0.25
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_ESCAPE_STAIRWELL_01")
					STOP_AUDIO_SCENE("AH_3B_ESCAPE_STAIRWELL_01")
				ENDIF
				IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_ESCAPE_POST_STAIRWELL")
					START_AUDIO_SCENE("AH_3B_ESCAPE_POST_STAIRWELL")
				ENDIF
			
				i_AudioSceneStage++
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE: First half of player making their way through the burnout floor
PROC ST_7_Burntout_Floor()

	IF DOES_ENTITY_EXIST(BUDDY_PED_ID())
	AND NOT IS_PED_INJURED(BUDDY_PED_ID())
		SET_PED_RESET_FLAG(BUDDY_PED_ID(), PRF_BlockCustomAIEntryAnims, TRUE)
		MANAGE_AI_BUDDY_WEAPON()
	ENDIF
	
	IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
	AND NOT IS_PED_INJURED(peds[mpf_goon].id)
		SET_PED_RESET_FLAG(peds[mpf_goon].id, PRF_BlockCustomAIEntryAnims, TRUE)
	ENDIF

	IF mission_substage >= 3 AND mission_substage < 5
		IF NOT DOES_ENTITY_EXIST(objs[mof_crashed_heli].id)
		
			Load_Asset_Model(sAssetData, P_CRAHSED_HELI_S)
			
			IF HAS_MODEL_LOADED(P_CRAHSED_HELI_S)
				REQUEST_CUTSCENE("AH_3B_MCS_6_P1")
			 
				// Create the heli
				objs[mof_crashed_heli].id = CREATE_OBJECT_NO_OFFSET(P_CRAHSED_HELI_S, <<152.9895, -737.3665, 252.8275>>) // << 151.5928, -743.9976, 253.1521 >>
				Unload_Asset_Model(sAssetData, P_CRAHSED_HELI_S)
				SET_ENTITY_ROTATION(objs[mof_crashed_heli].id, <<2.4000, 6.8250, 144.0730>>)

				FREEZE_ENTITY_POSITION(objs[mof_crashed_heli].id, TRUE)
				SET_ENTITY_COLLISION(objs[mof_crashed_heli].id, FALSE)
				SET_ENTITY_INVINCIBLE(objs[mof_crashed_heli].id, TRUE)
			ENDIF
		
		ELSE
			// create the pilot
			IF NOT DOES_ENTITY_EXIST(peds[mpf_pilot].id)
			
				Load_Asset_Model(sAssetData, mod_pilot)
				Load_Asset_AnimDict(sAssetData, animDictMCS6Pilot)
				
				IF HAS_MODEL_LOADED(mod_pilot)
				AND HAS_ANIM_DICT_LOADED(animDictMCS6Pilot)

					CREATE_MISSION_PED( peds[mpf_pilot], mod_pilot, <<152.9895, -737.3665, 252.8275>>, 0.0, "PILOT", REL_CREW)
					SET_ENTITY_INVINCIBLE(peds[mpf_pilot].id, TRUE)
					
					syncedScenes[ssf_MCS6_dead_pilot] = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(syncedScenes[ssf_MCS6_dead_pilot], objs[mof_crashed_heli].id, -1)
					TASK_SYNCHRONIZED_SCENE( peds[mpf_pilot].id, syncedScenes[ssf_MCS6_dead_pilot], animDictMCS6Pilot, "ah_3b_mcs_6_p1_pilot", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
					
				ENDIF
			ENDIF
		ENDIF
		
		// steam the cutscene assets
		IF NOT b_CutsceneStreamed
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", peds[mpf_mike].id)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", peds[mpf_frank].id)
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("gunman_selection_1", peds[mpf_goon].id)
				
				b_CutsceneStreamed = TRUE
			ENDIF
		ENDIF

	ENDIF

	SWITCH mission_substage
		CASE STAGE_ENTRY	
			
			Event_Trigger(mef_manage_radar_map)
			Event_Trigger(mef_kill_sprinklers)
			Event_Trigger(mef_pre_crashed_heli_corridor)
			Event_Trigger(mef_manage_smoke_vfx)
			Load_Asset_Audiobank(sAssetData, "SIREN_DISTANT")
			
			IF DOES_ENTITY_EXIST( objs[mof_equipment_bag].id )
				DELETE_OBJECT( objs[mof_equipment_bag].id )
			ENDIF
			Unload_Asset_Anim_Dict( sAssetData, "missheistfbi3bleadinoutah_3b_mcs_2" )
			
			SET_BUILDING_IPLS( msf_7_burnt_out_floor_1 )
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING)
			
			IF i_NavMeshBlockingAreas[2] = -1
				i_NavMeshBlockingAreas[2] = ADD_NAVMESH_BLOCKING_OBJECT(<<123.521, -739.488, 253.666>>, <<0.975, 3.475, 1.275>>, DEG_TO_RAD(-9.949), FALSE)
			ENDIF

			i_dialogue_stage		= 0
			b_CutsceneStreamed 		= FALSE
			iCrewAIStage 			= 0
			i_AudioSceneStage		= 0
			mission_substage++
		BREAK
		CASE 1
			// Kick of the next lot of fighting
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<117.759895,-736.022949,253.152420>>, <<121.788857,-724.480042,255.933578>>, 5.812500)
			
				SAFE_BLIP_COORD(blip_Objective, << 150.7760, -742.2473, 253.1521 >>)
			
				iCrewAIStage = 1
				mission_substage++
			ELSE
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, TRUE)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<116.323372,-740.080566,253.152542>>, <<117.880081,-735.824402,256.471313>>, 6.187500)
			OR GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = HASH("v_fib03_it3_hall")
				PLAY_SOUND_FROM_COORD(sounds[sfx_distant_sirens_1], "Distant_Sirens_Rubble", <<224.5, -627.0, 240.3>>, "FBI_HEIST_FINALE_CHOPPER")
				TRIGGER_MUSIC_EVENT("AH3B_ENTERED_BURN")
				mission_substage++
			ENDIF
		BREAK
		CASE 3
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<150.88, -747.85, 253.15>>) <= 10.0
				TRIGGER_MUSIC_EVENT("AH3B_HELI_FALLS")
				mission_substage++
			ENDIF
		BREAK
		CASE 4
			IF HAS_CUTSCENE_LOADED()
			
			AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<145.161835,-742.913330,253.235809>>, <<154.859482,-746.203735,256.521362>>, 4.375000)
			AND Is_Event_Complete(mef_pre_crashed_heli_corridor)
				
				SAFE_REMOVE_BLIP(blip_Objective)
				
				Event_Pause(mef_manage_switch)
			
				Load_Asset_AnimDict(sAssetData, animDict_IG7_elevator)
				Load_Asset_AnimDict(sAssetData, animDict_IG6_michael_slide)
				
//				SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), WEAPONTYPE_CARBINERIFLE, TRUE)
//				SET_CURRENT_PED_WEAPON(FRANK_PED_ID(), WEAPONTYPE_CARBINERIFLE, TRUE)
//				SET_CURRENT_PED_WEAPON(peds[mpf_goon].id, weap_Goon, TRUE)
				
				objs[mof_cutscene_weapon_mike].id 	= CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(MIKE_PED_ID(), weap_Mike)
				objs[mof_cutscene_weapon_frank].id 	= CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(FRANK_PED_ID(), weap_Frank)
				objs[mof_cutscene_weapon_goon].id 	= CREATE_WEAPON_OBJECT_FROM_PED_WEAPON_WITH_COMPONENTS(peds[mpf_goon].id, WEAPONTYPE_CARBINERIFLE)
				
//				objs[mof_cutscene_weapon_mike].id 	= GET_WEAPON_OBJECT_FROM_PED(MIKE_PED_ID())
//				objs[mof_cutscene_weapon_frank].id 	= GET_WEAPON_OBJECT_FROM_PED(FRANK_PED_ID())
//				objs[mof_cutscene_weapon_goon].id 	= GET_WEAPON_OBJECT_FROM_PED(peds[mpf_goon].id)
				REGISTER_ENTITY_FOR_CUTSCENE(objs[mof_cutscene_weapon_mike].id, "Michaels_2_Handedweapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(objs[mof_cutscene_weapon_frank].id, "Franklins_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(objs[mof_cutscene_weapon_goon].id, "HC_Gunman_2_Handedweapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				
				IF BUDDY_PED_ID() = FRANK_PED_ID()
					REGISTER_ENTITY_FOR_CUTSCENE(FRANK_PED_ID(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_PRESERVE_BODY_BLOOD_DAMAGE)
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(MIKE_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_PRESERVE_BODY_BLOOD_DAMAGE)
				ENDIF
				REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_goon].id, "gunman_selection_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_PRESERVE_BODY_BLOOD_DAMAGE)
				REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_pilot].id, "heli_pilot", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT)
				REGISTER_ENTITY_FOR_CUTSCENE(objs[mof_crashed_heli].id, "Crashed_Heli", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				bExitStateMichael 	= FALSE
				bExitStateFrank		= FALSE
				bExitStateGoon		= FALSE
				i_dialogue_stage 	= -1
				mission_substage++
			
			ENDIF
		BREAK
		CASE 5
			IF IS_CUTSCENE_PLAYING()
				Unload_Asset_Anim_Dict(sAssetData, animDict_WaitIdles)
				CLEAR_AREA_OF_PROJECTILES(<<148.76, -746.41, 253.29>>, 16.0)
				
				SET_PED_FLASH_LIGHT(peds[mpf_mike], FALSE)
				SET_PED_FLASH_LIGHT(peds[mpf_frank], FALSE)
				SET_PED_FLASH_LIGHT(peds[mpf_goon], FALSE)
				
				mission_substage++
			ENDIF
		BREAK
		CASE 6
			STRING str_Anim
			
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			
			IF NOT b_HeliRayFireExplosionHappened
			AND IS_CUTSCENE_PLAYING()
			
				IF GET_CUTSCENE_TIME() >= 8800
				
					START_PARTICLE_FX_NON_LOOPED_ON_ENTITY( "scr_agency_heli_slide_dust", objs[mof_crashed_heli].id, <<0,0,0>>, <<0,0,0>> )
				
					b_HeliRayFireExplosionHappened = TRUE
				ENDIF
				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				IF DOES_CAM_EXIST(cams[mcf_generic])
					DESTROY_CAM(cams[mcf_generic])
				ENDIF
				cams[mcf_generic] = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<155.594986,-734.012878,250.162979>>,<<23.466677,-6.759744,125.726616>>,50.014545, TRUE)
				POINT_CAM_AT_ENTITY(cams[mcf_generic], PLAYER_PED_ID(), <<0,0,0>>)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)
				Event_Pause(mef_manage_switch)
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				REPLAY_STOP_EVENT()
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG6_rubble_slide])
					syncedScenes[ssf_IG6_rubble_slide] = CREATE_SYNCHRONIZED_SCENE(v_ig6_coord, v_ig6_rot)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(syncedScenes[ssf_IG6_rubble_slide], FALSE)
				ENDIF
				IF PLAYER_PED_ID() = MIKE_PED_ID()
					str_Anim = "Rubble_Slide_Alt_Michael"
				ELSE
					str_Anim = "Rubble_Slide_Michael"
				ENDIF
				TASK_SYNCHRONIZED_SCENE(MIKE_PED_ID(), syncedScenes[ssf_IG6_rubble_slide], animDict_IG6_michael_slide, str_Anim, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), TRUE)
				
				SET_PED_FLASH_LIGHT(peds[mpf_mike], TRUE)
				
				bExitStateMichael = TRUE
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG6_rubble_slide])
					syncedScenes[ssf_IG6_rubble_slide] = CREATE_SYNCHRONIZED_SCENE(v_ig6_coord, v_ig6_rot)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(syncedScenes[ssf_IG6_rubble_slide], FALSE)
				ENDIF
				IF PLAYER_PED_ID() = MIKE_PED_ID()
					str_Anim = "Rubble_Slide_Alt_Franklin"
				ELSE
					str_Anim = "Rubble_Slide_Franklin"
				ENDIF
				TASK_SYNCHRONIZED_SCENE(FRANK_PED_ID(), syncedScenes[ssf_IG6_rubble_slide], animDict_IG6_michael_slide, str_Anim, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				SET_ENTITY_INVINCIBLE(FRANK_PED_ID(), TRUE)
				
				SET_PED_FLASH_LIGHT(peds[mpf_frank], TRUE)
				
				bExitStateFrank = TRUE
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("gunman_selection_1")
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG6_rubble_slide])
					syncedScenes[ssf_IG6_rubble_slide] = CREATE_SYNCHRONIZED_SCENE(v_ig6_coord, v_ig6_rot)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(syncedScenes[ssf_IG6_rubble_slide], FALSE)
				ENDIF
				IF PLAYER_PED_ID() = MIKE_PED_ID()
					str_Anim = "Rubble_Slide_Alt_Gunman"
				ELSE
					str_Anim = "Rubble_Slide_Gunman"
				ENDIF
				TASK_SYNCHRONIZED_SCENE(peds[mpf_goon].id, syncedScenes[ssf_IG6_rubble_slide], animDict_IG6_michael_slide, str_Anim, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_goon].id, TRUE)
				SET_ENTITY_INVINCIBLE(peds[mpf_goon].id, TRUE)
				
				SET_PED_FLASH_LIGHT(peds[mpf_goon], TRUE)

				bExitStateGoon = TRUE
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_2_Handedweapon")
				GIVE_WEAPON_OBJECT_TO_PED(objs[mof_cutscene_weapon_mike].id, MIKE_PED_ID())
				SET_CURRENT_PED_WEAPON(MIKE_PED_ID(), WEAPONTYPE_CARBINERIFLE, TRUE)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklins_weapon")
				GIVE_WEAPON_OBJECT_TO_PED(objs[mof_cutscene_weapon_frank].id, FRANK_PED_ID())
				SET_CURRENT_PED_WEAPON(FRANK_PED_ID(), WEAPONTYPE_CARBINERIFLE, TRUE)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("HC_Gunman_2_Handedweapon")
				GIVE_WEAPON_OBJECT_TO_PED(objs[mof_cutscene_weapon_goon].id, peds[mpf_goon].id)
				SET_CURRENT_PED_WEAPON(peds[mpf_goon].id, weap_Goon, TRUE)
			ENDIF
			
			IF bExitStateMichael OR bExitStateFrank OR bExitStateGoon 
				IF NOT Is_Event_Triggered(mef_ig7_elevator)
					Event_Trigger(mef_ig7_elevator)
				ENDIF
			ENDIF
			
			IF bExitStateMichael AND bExitStateFrank AND bExitStateGoon
			AND DOES_CAM_EXIST(cams[mcf_generic])
			AND IS_CAM_ACTIVE(cams[mcf_generic])
			AND IS_CAM_RENDERING(cams[mcf_generic])
			
				RENDER_SCRIPT_CAMS(FALSE, TRUE, 2000, TRUE)
				DESTROY_CAM(cams[mcf_generic])
			
				TRIGGER_MUSIC_EVENT("AH3B_AFTER_HELI_CS")
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				Mission_Set_Stage(msf_8_burnt_out_floor_2)
			ENDIF
		BREAK
	ENDSWITCH

	IF mission_stage = enum_to_int(msf_7_burnt_out_floor_1)
		SWITCH iCrewAIStage
			CASE 1
				CREATE_COVERPOINT_PAIR(covPair_buddy, 	<<119.2467, -742.4992, 253.1635>>, 239.8784, COVUSE_WALLTORIGHT, COVHEIGHT_LOW, COVARC_120)
				CREATE_COVERPOINT_PAIR(covPair_gunman, 	<<121.0577, -737.6027, 253.2061>>, 249.9036, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120)
				
				SET_PED_COMBAT_PARAMS(peds[PLAYER_MPF()], <<122.3957, -740.7980, 253.1523>>, 1.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.0, 0.0)
				SET_PED_COMBAT_PARAMS(peds[BUDDY_MPF()], <<119.2467, -742.4992, 253.1635>>, 1.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.0, 0.0)
				SET_PED_COMBAT_PARAMS(peds[mpf_goon], <<121.0577, -737.6027, 253.2061>>, 1.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 0.0, 0.0)
				
				bRetaskBuddy = TRUE
				bRetaskGunman = TRUE
				iCrewAIStage++
			BREAK
			CASE 2
				IF NOT IS_PED_INJURED(peds[mpf_before_heli_fall_1].id)
				OR NOT IS_PED_INJURED(peds[mpf_before_heli_fall_2].id)
				OR NOT IS_PED_INJURED(peds[mpf_before_heli_fall_3].id)
				OR NOT IS_PED_INJURED(peds[mpf_before_heli_fall_4].id)
				OR NOT IS_PED_INJURED(peds[mpf_before_heli_fall_5].id)

					// BUDDY
					IF IS_ENTITY_IN_ANGLED_AREA(BUDDY_PED_ID(), <<116.323372,-740.080566,253.152542>>, <<117.880081,-735.824402,256.471313>>, 6.187500)
						SET_COMBAT_FLOAT(BUDDY_PED_ID(), CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
					ENDIF
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
					OR bRetaskBuddy
						TASK_COMBAT_HATED_TARGETS_IN_AREA(BUDDY_PED_ID(), <<131.4281, -744.4481, 254.6485>>, 50.0)
						bRetaskBuddy = FALSE
					ENDIF				
					
					// GUNMAN
					IF IS_ENTITY_IN_ANGLED_AREA(peds[mpf_goon].id, <<116.323372,-740.080566,253.152542>>, <<117.880081,-735.824402,256.471313>>, 6.187500)
						SET_COMBAT_FLOAT(peds[mpf_goon].id, CCF_STRAFE_WHEN_MOVING_CHANCE, 1.0)
					ENDIF
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
					OR bRetaskGunman
						TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_goon].id, <<131.4281, -744.4481, 254.6485>>, 50.0)
						bRetaskGunman = FALSE
					ENDIF
				ELSE
					REMOVE_PED_DEFENSIVE_AREA(BUDDY_PED_ID())
					REMOVE_PED_DEFENSIVE_AREA(peds[mpf_goon].id)
					
					bRetaskBuddy = TRUE
					bRetaskGunman = TRUE
					iCrewAIStage++
				ENDIF
			BREAK
			CASE 3
				IF NOT IS_CUTSCENE_PLAYING()
				
					Load_Asset_AnimDict(sAssetData, animDict_WaitIdles)
				
					bBuddyReadyToMoveOn = FALSE
				
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
					OR bRetaskBuddy
						IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(BUDDY_PED_ID()), <<150.7780, -750.3884, 253.2518>>, 1.0)
							
							TASK_FOLLOW_NAV_MESH_TO_COORD(BUDDY_PED_ID(), <<150.7780, -750.3884, 253.2518>>, PEDMOVE_RUN, DEFAULT, DEFAULT, ENAV_STOP_EXACTLY, 1.4974)
							
						ELIF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
							SAFE_OPEN_SEQUENCE()
								TASK_ACHIEVE_HEADING(null, 1.4974)
								TASK_PLAY_ANIM(null, animDict_WaitIdles, "idle_b", SLOW_BLEND_IN)
								TASK_PLAY_ANIM(null, animDict_WaitIdles, "base")

								SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
							SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
						ENDIF
						bRetaskBuddy = FALSE
					ELSE
						IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(BUDDY_PED_ID()), <<149.4686, -748.3608, 253.1521>>, 2.0)
							bBuddyReadyToMoveOn = TRUE
						ENDIF
					ENDIF
					
					bGunmanReadyToMoveOn = FALSE
					
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
					OR bRetaskGunman
						IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), <<149.4686, -748.3608, 253.1521>>, 1.0)
							TASK_FOLLOW_NAV_MESH_TO_COORD(peds[mpf_goon].id, <<149.4686, -748.3608, 253.1521>>, PEDMOVE_RUN, DEFAULT, DEFAULT, ENAV_STOP_EXACTLY, 355.1978)
						ENDIF
						bRetaskGunman = FALSE
					ELSE
						IF ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), <<149.4686, -748.3608, 253.1521>>, 2.0)
							bGunmanReadyToMoveOn = TRUE
						ENDIF
					ENDIF
				ELSE
					bRetaskBuddy = TRUE
					bRetaskGunman = TRUE
					
					iCrewAIStage++
				ENDIF
			BREAK
		ENDSWITCH
		
		SWITCH i_dialogue_stage
			CASE 0
				IF iCrewAIStage > 1
					i_dialogue_stage++
				ELSE
					IF IS_SAFE_TO_START_CONVERSATION()
					AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
						GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, TRUE)
						IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF iCrewAIStage > 2
					i_dialogue_stage++
				ELSE
					IF IS_SAFE_TO_START_CONVERSATION()
					AND GET_GAME_TIMER() - i_time_of_last_convo > 3500
						GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_OUT", TRUE, str_dialogue_root)
						IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 2
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, TRUE)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
						i_dialogue_stage++
					ENDIF
				ENDIF
			BREAK
			CASE 3
				IF bBuddyReadyToMoveOn
					i_dialogue_stage++
				ELSE
					IF IS_SAFE_TO_START_CONVERSATION()
					AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
						GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, TRUE)
						IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 4
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 5000
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_CRASH", FALSE, str_dialogue_root, TRUE)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
		
		SWITCH i_AudioSceneStage
			CASE 0
				IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_ESCAPE_POST_STAIRWELL")
					START_AUDIO_SCENE("AH_3B_ESCAPE_POST_STAIRWELL")
				ENDIF
				
				i_AudioSceneStage++
			BREAK
			CASE 1
				IF IS_CUTSCENE_PLAYING()
					IF IS_AUDIO_SCENE_ACTIVE("AH_3B_ESCAPE_POST_STAIRWELL")
						STOP_AUDIO_SCENE("AH_3B_ESCAPE_POST_STAIRWELL")
					ENDIF	
					
					i_AudioSceneStage++
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

//PURPOSE: Burntout floor after the crashed heli has fallen from the building
PROC ST_8_Burntout_Floor_2()

	IF DOES_ENTITY_EXIST(BUDDY_PED_ID())
	AND NOT IS_PED_INJURED(BUDDY_PED_ID())
		SET_PED_RESET_FLAG(BUDDY_PED_ID(), PRF_BlockCustomAIEntryAnims, TRUE)
		MANAGE_AI_BUDDY_WEAPON()
	ENDIF
	
	IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
	AND NOT IS_PED_INJURED(peds[mpf_goon].id)
		SET_PED_RESET_FLAG(peds[mpf_goon].id, PRF_BlockCustomAIEntryAnims, TRUE)
	ENDIF

	// Normal mission flow
	SWITCH mission_substage
		CASE STAGE_ENTRY
			SAFE_BLIP_COORD(blip_Objective, << 142.5694, -765.9665, 249.1519 >>)
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7, "8. Burntout floor 2")
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
			
			Event_Trigger(mef_ig8_paramedic)

			SET_PED_COMBAT_MOVEMENT(FRANK_PED_ID(), CM_DEFENSIVE)
			SET_PED_COMBAT_RANGE(FRANK_PED_ID(), CR_MEDIUM)
			
			SET_PED_COMBAT_MOVEMENT(peds[mpf_goon].id, CM_DEFENSIVE)
			SET_PED_COMBAT_RANGE(peds[mpf_goon].id, CR_MEDIUM)
			
			bExitStateMichael 	= FALSE
			bExitStateFrank		= FALSE
			bExitStateGoon		= FALSE
			
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			
			Event_Trigger(mef_bodies)

			str_dialogue_root 	= ""
			i_dialogue_stage 	= 0
			i_AudioSceneStage 	= 0
			iCrewAIStage 		= 0
			mission_substage++
		BREAK
		CASE 1
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG6_rubble_slide])
			OR HAS_ANIM_EVENT_FIRED(PLAYER_PED_ID(), GET_HASH_KEY("interrupt"))

				CLEAR_PED_TASKS(PLAYER_PED_ID())
				
				IF PLAYER_PED_ID() = MIKE_PED_ID()
					bExitStateMichael = TRUE
				ELSE
					bExitStateFrank	= TRUE
				ENDIF
				
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
				
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)

				Unload_Asset_Anim_Dict(sAssetData, "missheistfbi3b_ig6")
				mission_substage++
			ELSE
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			ENDIF
		BREAK
		CASE 2
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<146.778397,-735.523682,249.152115>>, <<131.955978,-730.519348,252.973419>>, 4.562500)
				CREATE_MISSION_PED(peds[mpf_post_heli_fall_1], mod_fib_agent_2, << 134.7801, -743.9953, 249.1520 >>, 334.6092, "postFall_1", REL_ENEMY, WEAPONTYPE_PUMPSHOTGUN, WEAPONCOMPONENT_INVALID, 5)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_post_heli_fall_1].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_post_heli_fall_1].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_post_heli_fall_1].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_post_heli_fall_1].id, TRUE)
				mission_substage++
			ENDIF
		BREAK
		CASE 3
			// Guy comes from other end of corridor
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<138.883682,-732.779663,249.152084>>, <<143.218338,-734.362244,252.527252>>, 4.875000)
				IF NOT IS_PED_INJURED(peds[mpf_post_heli_fall_1].id)
				
					SET_PED_COMBAT_PARAMS(peds[mpf_post_heli_fall_1], <<0,0,0>>, 0.0, CM_WILLADVANCE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE, FALSE, TRUE, TRUE)
				
					SAFE_OPEN_SEQUENCE()
						TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(null, << 136.0926, -744.3270, 249.1520 >>, PLAYER_PED_ID(), PEDMOVE_WALK, TRUE, 1.0)
						TASK_COMBAT_HATED_TARGETS_IN_AREA(null, GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0)
					SAFE_PERFORM_SEQUENCE(peds[mpf_post_heli_fall_1].id)

				ENDIF
				
				mission_substage++
			ENDIF
		BREAK
		CASE 4
			// player reaches end of small corridor, enemies come through large corridor
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<135.248947,-746.241821,249.152008>>, <<136.501892,-743.007813,251.652008>>, 3.750000)
				
			// spawn new peds (in the comms room)
				CREATE_MISSION_PED(peds[mpf_1st_comms_room_1], mod_fib_agent_2, 	<< 117.3914, -750.5050, 249.1522 >>, 343.9231, 	"Coms1st_1", 	REL_ENEMY, WEAPONTYPE_PISTOL, 		WEAPONCOMPONENT_INVALID, 5)
				CREATE_MISSION_PED(peds[mpf_1st_comms_room_2], mod_fib_agent_1, 	<< 126.4687, -756.7318, 249.1521 >>, 57.8205, 	"Coms1st_2", 	REL_ENEMY, WEAPONTYPE_PUMPSHOTGUN, 	WEAPONCOMPONENT_INVALID, 2)
				SET_PED_COMBAT_PARAMS(peds[mpf_1st_comms_room_1], <<118.5374, -741.2036, 249.1521>>,	1.0, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE, FALSE, TRUE)
				SET_PED_COMBAT_PARAMS(peds[mpf_1st_comms_room_2], <<118.5398, -752.3001, 249.1522>>, 	1.0, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, TRUE, FALSE, TRUE)
				Task_Put_Ped_Directly_Into_Custom_Cover(peds[mpf_1st_comms_room_1], << 117.3914, -750.5050, 249.1522 >>, 343.9231, -1, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_90, FALSE, 0, TRUE, TRUE)
				Task_Put_Ped_Directly_Into_Custom_Cover(peds[mpf_1st_comms_room_2], << 124.9857, -756.7640, 249.1522 >>, 71.2150, -1, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120, FALSE, 0, TRUE, FALSE)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_1st_comms_room_1].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_1st_comms_room_1].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_1st_comms_room_1].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_1st_comms_room_1].id, TRUE)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_1st_comms_room_2].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_1st_comms_room_2].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_1st_comms_room_2].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_1st_comms_room_2].id, TRUE)
				
				mission_substage++
			ENDIF
		BREAK
		CASE 5
			// communications room peds attack
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<117.573433,-743.909607,249.152161>>, <<120.301483,-735.992798,251.893295>>, 5.562500)
				
				IF NOT IS_PED_INJURED(peds[mpf_1st_comms_room_1].id)
					REMOVE_PED_DEFENSIVE_AREA(peds[mpf_1st_comms_room_1].id)
					SET_PED_COMBAT_MOVEMENT(peds[mpf_1st_comms_room_1].id, CM_WILLADVANCE)
					SET_PED_CONFIG_FLAG(peds[mpf_1st_comms_room_1].id, PCF_ShouldChargeNow, TRUE)
					TASK_COMBAT_PED(peds[mpf_1st_comms_room_1].id, PLAYER_PED_ID())
				ENDIF
				
				Event_Trigger(mef_comms_2)
				
				mission_substage++
			ENDIF
		BREAK
		CASE 6
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<117.771576,-747.283508,249.152161>>, <<112.505775,-745.617859,252.367432>>, 3.000000)
			
				IF NOT IS_PED_INJURED(peds[mpf_1st_comms_room_2].id)
					REMOVE_PED_DEFENSIVE_AREA(peds[mpf_1st_comms_room_2].id)
					SET_PED_COMBAT_MOVEMENT(peds[mpf_1st_comms_room_2].id, CM_WILLADVANCE)
					SET_PED_CONFIG_FLAG(peds[mpf_1st_comms_room_2].id, PCF_ShouldChargeNow, TRUE)
					TASK_COMBAT_PED(peds[mpf_1st_comms_room_2].id, PLAYER_PED_ID())
				ENDIF
				
				mission_substage++
			ENDIF
		BREAK
		CASE 7
			// player steps in to next room
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<124.639000,-756.054688,249.152145>>, <<122.306374,-755.039734,251.402206>>, 2.250000)
			OR IS_ENTITY_IN_ANGLED_AREA(BUDDY_PED_ID(), <<124.639000,-756.054688,249.152145>>, <<122.306374,-755.039734,251.402206>>, 2.250000)
				
				// create peds in next corridor
				CREATE_MISSION_PED(peds[mpf_pre_final_stairwell_1], mod_fib_agent_1, << 153.6167, -763.6597, 249.1519 >>, 72.7809, "preFinalStair1", REL_ENEMY, WEAPONTYPE_SMG, WEAPONCOMPONENT_INVALID, 5)
				SET_PED_COMBAT_PARAMS(peds[mpf_pre_final_stairwell_1],<<0,0,0>>, 0.0, CM_WILLADVANCE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0, TRUE, FALSE, TRUE, TRUE)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_pre_final_stairwell_1].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_pre_final_stairwell_1].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_pre_final_stairwell_1].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_pre_final_stairwell_1].id, TRUE)
				
				CREATE_MISSION_PED(peds[mpf_pre_final_stairwell_2], mod_fib_agent_1, <<148.4886, -769.8999, 249.1519>>, 341.6490, "preFinalStair2", REL_ENEMY, WEAPONTYPE_PISTOL, WEAPONCOMPONENT_INVALID, 5, 150)
				SET_PED_COMBAT_PARAMS(peds[mpf_pre_final_stairwell_2], <<148.4886, -769.8999, 249.1519>>, 1.0, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0, FALSE, FALSE, FALSE)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_pre_final_stairwell_2].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_pre_final_stairwell_2].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_pre_final_stairwell_2].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_pre_final_stairwell_2].id, TRUE)
				TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_pre_final_stairwell_2].id, <<148.4886, -769.8999, 249.1519>>, -1, FALSE, 0, TRUE, FALSE, NULL, FALSE)
				
				CREATE_MISSION_PED(peds[mpf_pre_final_stairwell_3], mod_fib_agent_1, <<145.9628, -770.7472, 249.1519>>, 255.5830, "preFinalStair3", REL_ENEMY, WEAPONTYPE_PUMPSHOTGUN, WEAPONCOMPONENT_INVALID, 2, 110)
				SET_PED_COMBAT_PARAMS(peds[mpf_pre_final_stairwell_3], <<150.9992, -772.7845, 249.1519>>, 1.5, CM_DEFENSIVE, CR_NEAR, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0, TRUE, FALSE, TRUE)
				SET_PED_ENVEFF_CPV_ADD(peds[mpf_pre_final_stairwell_3].id, 0.099)
				SET_PED_ENVEFF_SCALE(peds[mpf_pre_final_stairwell_3].id, 1.0)
				SET_PED_ENVEFF_COLOR_MODULATOR(peds[mpf_pre_final_stairwell_3].id, 87,81,68)
				SET_ENABLE_PED_ENVEFF_SCALE(peds[mpf_pre_final_stairwell_3].id, TRUE)
				
				mission_substage++
			ENDIF
		BREAK
		CASE 8
			// Player steps out into corridor and gets attacked
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<140.271912,-757.679382,249.152008>>, <<141.748810,-753.510010,251.569305>>, 6.750000)
				IF NOT IS_PED_INJURED(peds[mpf_pre_final_stairwell_1].id)
					TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_pre_final_stairwell_1].id, GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0)
				ENDIF
				mission_substage++
			ENDIF
		BREAK
		CASE 9
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<153.429642,-760.386780,249.151947>>, <<157.335526,-761.752991,252.842697>>, 5.062500)
				IF NOT IS_PED_INJURED(peds[mpf_pre_final_stairwell_2].id)
					TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_pre_final_stairwell_2].id, GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0)
				ENDIF
				IF NOT IS_PED_INJURED(peds[mpf_pre_final_stairwell_3].id)
					TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_pre_final_stairwell_3].id, GET_ENTITY_COORDS(PLAYER_PED_ID()), 50.0)
				ENDIF
				mission_substage++
			ENDIF
		BREAK
		CASE 10
			// new blip as player walks through the door
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<142.269363,-767.053833,249.151886>>, <<142.907104,-765.348816,251.401886>>, 1.750000)
			
				Load_Asset_Model(sAssetData, mod_rubble_blocker)
				REQUEST_RAPPEL_ASSETS()
				REQUEST_CUTSCENE("ah_3b_mcs_7")
				SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_ON)
				IF BUDDY_MPF() = mpf_frank
					Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, MIKE_PED_ID(), "Michael")
				ELIF BUDDY_MPF() = mpf_mike
					Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, FRANK_PED_ID(), "Franklin")
				ENDIF
				Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, peds[mpf_goon].id, "gunman_selection_1")
				
				SAFE_BLIP_COORD(blip_Objective, << 161.9050, -763.6213, 245.1519 >>)
				
				Event_Trigger(mef_rappel_room)
				
				mission_substage++
			ENDIF
		BREAK
		CASE 11
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<142.502716,-767.184265,245.152084>>, <<141.227097,-770.819031,248.200287>>, 2.500000)
				IF NOT DOES_ENTITY_EXIST(objs[mof_rubble_blocker].id)
				AND HAS_MODEL_LOADED(mod_rubble_blocker)
					objs[mof_rubble_blocker].id = CREATE_OBJECT(mod_rubble_blocker, <<163.03, -756.43, 245.14>>)
					objs[mof_rubble_blocker].bCustom = TRUE
					SET_ENTITY_HEADING(objs[mof_rubble_blocker].id, -1.92)
					Unload_Asset_Model(sAssetData, mod_rubble_blocker)
					DISTANT_COP_CAR_SIRENS(TRUE)
					mission_substage++
				ENDIF
			ENDIF
		BREAK
		CASE 12
			// Remove blip....
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<158.070847,-759.965271,245.152222>>, <<163.792160,-761.811401,248.026840>>, 25.187500)
			AND IS_PED_INJURED(peds[mpf_final_rappel_room_1].id)
			AND IS_PED_INJURED(peds[mpf_final_rappel_room_2].id)
			AND HAS_CUTSCENE_LOADED()
			
				IF NOT IS_SAFE_TO_START_CONVERSATION()
					KILL_FACE_TO_FACE_CONVERSATION()
				ELIF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF crewHacker = CM_HACKER_B_RICKIE_UNLOCK
						str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_MCS7", TRUE)
					ELSE
						str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_MCS7")
					ENDIF
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						SAFE_REMOVE_BLIP(blip_Objective)
						
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_frank].id, TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_goon].id, TRUE)
						
						IF PLAYER_PED_ID() = MIKE_PED_ID()
							REGISTER_ENTITY_FOR_CUTSCENE(FRANK_PED_ID(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_PRESERVE_BODY_BLOOD_DAMAGE)
						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(MIKE_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_PRESERVE_BODY_BLOOD_DAMAGE)
						ENDIF
						REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_goon].id, "gunman_selection_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_PRESERVE_BODY_BLOOD_DAMAGE)
						
						TRIGGER_MUSIC_EVENT("AH3B_RAPPEL_CS")
						
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						bExitStateMichael	= FALSE
						bExitStateFrank		= FALSE
						bExitStateGoon		= FALSE
						bExitStateCam		= FALSE
						
						Event_Trigger(mef_emergency_services)
						
						str_dialogue_root = ""
						
						Mission_Set_Stage(msf_9_abseil_down)
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	BOOL bInstantAim
	SWITCH iCrewAIStage
		CASE 0
			IF bExitStateMichael
			AND bExitStateFrank
			AND bExitStateGoon
			
				IF Is_Event_Triggered(mef_manage_switch)
					Event_Resume(mef_manage_switch)
				ENDIF
				
				Unload_Asset_Anim_Dict(sAssetData, animDict_IG6_michael_slide)

				i_dialogue_stage = 0
				bRetaskBuddy	= TRUE
				bRetaskGunman 	= TRUE
				iCrewAIStage++
			ELSE
			
				SET_PED_SPHERE_DEFENSIVE_AREA(PLAYER_PED_ID(), <<147.0101, -733.1375, 249.1522>>, 2.0)
			
			// BUDDY
				SET_PED_SPHERE_DEFENSIVE_AREA(BUDDY_PED_ID(), << 146.5043, -734.9260, 249.1521 >>, 2.0)
			
				IF BUDDY_PED_ID() = MIKE_PED_ID()
				AND NOT bExitStateMichael
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG6_rubble_slide])
					OR HAS_ANIM_EVENT_FIRED(MIKE_PED_ID(), GET_HASH_KEY("interrupt"))
						CREATE_COVERPOINT_PAIR(covPair_buddy, <<151.3366, -728.9139, 249.1526>>, 63.0760, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
						TASK_PUT_PED_DIRECTLY_INTO_COVER(MIKE_PED_ID(), <<151.3366, -728.9139, 249.1526>>, -1, FALSE, 0.5, TRUE, TRUE, covPair_buddy.covID[covPair_buddy.id], FALSE)
						
						SET_ENTITY_INVINCIBLE(MIKE_PED_ID(), FALSE)
						bExitStateMichael = TRUE
						bRetaskBuddy = TRUE
					ENDIF
					
				ELIF BUDDY_PED_ID() = FRANK_PED_ID()
				AND NOT bExitStateFrank 
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG6_rubble_slide])
					OR HAS_ANIM_EVENT_FIRED(FRANK_PED_ID(), GET_HASH_KEY("interrupt"))
						CREATE_COVERPOINT_PAIR(covPair_buddy, <<151.3366, -728.9139, 249.1526>>, 63.0760, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
						TASK_PUT_PED_DIRECTLY_INTO_COVER(FRANK_PED_ID(), <<151.3366, -728.9139, 249.1526>>, -1, FALSE, 0.5, TRUE, TRUE, covPair_buddy.covID[covPair_buddy.id], FALSE)
					
						SET_ENTITY_INVINCIBLE(FRANK_PED_ID(), FALSE)
						bExitStateFrank = TRUE
						bRetaskBuddy = TRUE
					ENDIF
				ELSE
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER)
						//IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(),SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
						IF bRetaskBuddy
							CREATE_COVERPOINT_PAIR(covPair_buddy, <<151.3366, -728.9139, 249.1526>>, 63.0760, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
							TASK_SEEK_COVER_TO_COVER_POINT(BUDDY_PED_ID(), covPair_buddy.covID[covPair_buddy.id], << 139.5157, -733.0037, 250.3622 >>, -1, FALSE)
							bRetaskBuddy = FALSE
						ENDIF
					ENDIF
				ENDIF
				
			// GUNMAN
				SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_goon].id, << 144.4086, -732.6505, 249.1522 >>, 2.5)
			
				IF NOT bExitStateGoon
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG6_rubble_slide])
					OR HAS_ANIM_EVENT_FIRED(peds[mpf_goon].id, GET_HASH_KEY("interrupt"))
						SAFE_CREATE_COVERPOINT( covPair_gunman.covID[covPair_gunman.id], <<144.8760, -732.1308, 249.1522>>, 66.7075, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
						TASK_PUT_PED_DIRECTLY_INTO_COVER(peds[mpf_goon].id, <<144.8760, -732.1308, 249.1522>>, -1, FALSE, 0.5, TRUE, TRUE, covPair_gunman.covID[covPair_gunman.id], FALSE)
					
						SET_ENTITY_INVINCIBLE(peds[mpf_goon].id, FALSE)
						bExitStateGoon 	= TRUE
						bRetaskGunman	= TRUE
					ELSE
						IF i_dialogue_stage = 0
							IF crewGoon = CM_GUNMAN_G_PACKIE_UNLOCK
								i_dialogue_stage++
							ELSE
								IF IS_SAFE_TO_START_CONVERSATION()
									str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewGoon, "AH_SLIDE")
									IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
										str_dialogue_root = ""
										CPRINTLN(DEBUG_MIKE, "PLAYED CONVERSATION AH_SLIDE")
										i_dialogue_stage++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_PUT_PED_DIRECTLY_INTO_COVER)
						IF bRetaskGunman
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id,SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
								TASK_SEEK_COVER_TO_COVER_POINT(peds[mpf_goon].id, covPair_gunman.covID[covPair_gunman.id], << 139.5157, -733.0037, 250.3622 >>, -1, FALSE)
								bRetaskGunman = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
		CASE 1
			IF peds[mpf_ig7_elevator].bHasBeenKilled
				CREATE_COVERPOINT_PAIR(covPair_gunman, <<142.9039, -732.4354, 249.1522>>, 63.1978, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180)
				CREATE_COVERPOINT_PAIR(covPair_buddy, << 138.1126, -734.5737, 249.1521 >>, 163.6320, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
				
				bRetaskBuddy	= TRUE
				bRetaskGunman 	= TRUE
				i_dialogue_stage = 0
				iCrewAIStage++
				
			ELIF IS_PED_IN_COMBAT(peds[mpf_ig7_elevator].id)
			OR mEvents[mef_ig7_elevator].iStage > 3
			
				// BUDDY
				IF NOT IS_PED_IN_COMBAT(BUDDY_PED_ID())
					TASK_COMBAT_PED(BUDDY_PED_ID(), peds[mpf_ig7_elevator].id)
				ENDIF
				
				// GUNMAN
				IF NOT IS_PED_IN_COMBAT(peds[mpf_goon].id)
					TASK_COMBAT_PED(peds[mpf_goon].id, peds[mpf_ig7_elevator].id)
				ENDIF
				
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_OUT", TRUE, str_dialogue_root)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
				
			ELSE
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_POINT", FALSE, str_dialogue_root, TRUE)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF (DOES_ENTITY_EXIST(peds[mpf_post_heli_fall_1].id)
			AND NOT IS_PED_INJURED(peds[mpf_post_heli_fall_1].id)
			AND IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_post_heli_fall_1].id, SCRIPT_TASK_PERFORM_SEQUENCE))
			OR peds[mpf_post_heli_fall_1].bHasBeenKilled
			
				SET_PED_SPHERE_DEFENSIVE_AREA(BUDDY_PED_ID(), << 141.3472, -733.1163, 249.1521 >>, 1.5)
				SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_goon].id, << 138.1126, -734.5737, 249.1521 >>, 1.5)
			
				bRetaskBuddy	= TRUE
				bRetaskGunman 	= TRUE
				iCrewAIStage++
			ELSE
				// BUDDY
				//IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
				IF bRetaskBuddy
					TASK_SEEK_COVER_TO_COVER_POINT(BUDDY_PED_ID(), covPair_buddy.covID[covPair_buddy.id], << 138.7686, -737.5693, 250.4563 >>, -1, FALSE)
					bRetaskBuddy = FALSE
				ENDIF
				
				// GUNMAN
				//IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
				IF bRetaskGunman
					TASK_SEEK_COVER_TO_COVER_POINT(peds[mpf_goon].id, covPair_gunman.covID[covPair_gunman.id], << 138.7686, -737.5693, 250.4563 >>, -1, FALSE)
					bRetaskGunman = FALSE
				ENDIF
				
				SWITCH i_dialogue_stage
					CASE 0
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, FALSE, FALSE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_POINT", FALSE, str_dialogue_root, TRUE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
				
			ENDIF
		BREAK
		CASE 3
			IF peds[mpf_post_heli_fall_1].bHasBeenKilled
				REMOVE_COVER_POINT(covPair_buddy.covID[covPair_buddy.id])
				CREATE_COVERPOINT_PAIR(covPair_gunman, << 136.2379, -741.0781, 249.1520 >>, 74.5267, COVUSE_WALLTOBOTH, COVHEIGHT_TOOHIGH, COVARC_180)
			
				i_dialogue_stage = 0
				bRetaskBuddy	= TRUE
				bRetaskGunman 	= TRUE
				iCrewAIStage++
			ELSE
				// BUDDY
				IF NOT IS_PED_IN_COMBAT(BUDDY_PED_ID())
					TASK_COMBAT_PED(BUDDY_PED_ID(), peds[mpf_post_heli_fall_1].id)
				ENDIF
				// GUNMAN
				IF NOT IS_PED_IN_COMBAT(peds[mpf_goon].id)
					TASK_COMBAT_PED(peds[mpf_goon].id, peds[mpf_post_heli_fall_1].id)
				ENDIF
				
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_OUT", TRUE, str_dialogue_root)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF Is_Event_Running(mef_ig8_paramedic)
			AND mEvents[mef_ig8_paramedic].iStage >= 3
				SET_PED_SPHERE_DEFENSIVE_AREA(BUDDY_PED_ID(), <<135.7272, -744.9222, 249.1521>>, 1.5)
				SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_goon].id, <<131.3241, -743.5495, 249.1521>>, 1.5)
				bRetaskBuddy	= TRUE
				bRetaskGunman 	= TRUE
				iCrewAIStage++
			ELSE
				// BUDDY
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
				OR bRetaskBuddy
					SAFE_OPEN_SEQUENCE()
						IF IS_PED_IN_COVER(BUDDY_PED_ID(), TRUE)
							TASK_EXIT_COVER(null, AIMING_COVER_EXIT, << 135.9303, -742.9735, 250.6711 >>)		
						ENDIF
						IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), << 136.2379, -741.0781, 249.1520 >>, 2.0)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, << 138.3504, -742.0092, 249.2518 >>, << 135.9303, -742.9735, 250.6711 >>, PEDMOVE_RUN, FALSE, 0.5, 2.0, TRUE, ENAV_STOP_EXACTLY)
							bInstantAim = TRUE
						ENDIF
						TASK_AIM_GUN_AT_COORD(null, << 135.9303, -742.9735, 250.6711 >>, -1, bInstantAim)
					SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
					bRetaskBuddy = FALSE
				ENDIF
				//GUNMAN
				//IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_PERFORM_SEQUENCE)
				IF bRetaskGunman
					SAFE_OPEN_SEQUENCE()
						IF IS_PED_IN_COVER(peds[mpf_goon].id, TRUE)
							TASK_EXIT_COVER(null, AIMING_COVER_EXIT, << 135.5610, -743.4017, 250.4345 >>)
						ENDIF
						IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), << 136.2379, -741.0781, 249.1520 >>, 2.0)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, << 136.2379, -741.0781, 249.1520 >>, << 135.5610, -743.4017, 250.4345 >>, PEDMOVE_RUN, FALSE, 1.0, 2.0)
						ENDIF
						TASK_SEEK_COVER_TO_COVER_POINT(null, covPair_gunman.covID[covPair_gunman.id], << 131.0666, -742.8386, 250.6242 >>, -1)
					SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
					bRetaskGunman = FALSE
				ENDIF
				
				SWITCH i_dialogue_stage
					CASE 0
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, FALSE, FALSE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_POINT", FALSE, str_dialogue_root, TRUE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 5
			IF peds[mpf_ig8_agent1].bHasBeenKilled
			AND peds[mpf_ig8_agent2].bHasBeenKilled
			
				CREATE_COVERPOINT_PAIR(covPair_buddy, << 123.1685, -741.6398, 249.1520 >>, 159.9832, COVUSE_WALLTOBOTH, COVHEIGHT_TOOHIGH, COVARC_180)
				REMOVE_COVER_POINT(covPair_gunman.covID[covPair_gunman.id])
				
				i_dialogue_stage = 0
				bRetaskBuddy	= TRUE
				bRetaskGunman 	= TRUE
				iCrewAIStage++
			ELSE
				// BUDDY
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
					TASK_COMBAT_HATED_TARGETS_IN_AREA(BUDDY_PED_ID(), << 121.9407, -739.3134, 249.1520 >>, 4.0)
				ENDIF
				
				// GUNMAN
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
					TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_goon].id, << 121.9407, -739.3134, 249.1520 >>, 4.0)
				ENDIF
				
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_OUT", TRUE, str_dialogue_root)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 6
			IF (DOES_ENTITY_EXIST(peds[mpf_1st_comms_room_1].id)
			AND (IS_PED_INJURED(peds[mpf_1st_comms_room_1].id)
			OR IS_PED_SHOOTING(peds[mpf_1st_comms_room_1].id)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(peds[mpf_1st_comms_room_1].id)))
			OR peds[mpf_1st_comms_room_1].bHasBeenKilled
			
				SET_PED_SPHERE_DEFENSIVE_AREA(BUDDY_PED_ID(), << 119.9149, -742.9847, 249.1522 >>, 1.5)
				SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_goon].id, << 116.9745, -742.2205, 249.1522 >>, 1.5)
			
				bRetaskBuddy	= TRUE
				bRetaskGunman 	= TRUE
				iCrewAIStage++
			ELSE
				// BUDDY
				//IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
				IF bRetaskBuddy
					SAFE_OPEN_SEQUENCE()
						IF IS_PED_IN_COVER(BUDDY_PED_ID(), TRUE)
							TASK_EXIT_COVER(null, AIMING_COVER_EXIT, << 119.4113, -739.8093, 250.4530 >>)
						ENDIF
						IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(BUDDY_PED_ID()), << 123.1685, -741.6398, 249.1520 >>, 3.0)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, << 123.1685, -741.6398, 249.1520 >>, << 119.4113, -739.8093, 250.4530 >>, PEDMOVE_RUN, FALSE, 2.0, 0.5)
						ENDIF
						TASK_SEEK_COVER_TO_COVER_POINT(null, covPair_buddy.covID[covPair_buddy.id], << 118.2874, -742.6687, 250.3579 >>, -1, FALSE)
					SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
					bRetaskBuddy = FALSE
				ENDIF
				
				// GUNMAN
				//IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_PERFORM_SEQUENCE)	
				IF bRetaskGunman
					SAFE_OPEN_SEQUENCE()
						IF IS_PED_IN_COVER(peds[mpf_goon].id, TRUE)
							TASK_EXIT_COVER(null, AIMING_COVER_EXIT, << 119.4113, -739.8093, 250.4530 >>)
						ENDIF
						IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), << 122.0048, -737.8095, 249.1520 >>, 2.0)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, << 122.0048, -737.8095, 249.1520 >>, << 119.4113, -739.8093, 250.4530 >>, PEDMOVE_RUN, FALSE, 0.5, 1.5)
							bInstantAim = TRUE
						ENDIF
						TASK_AIM_GUN_AT_COORD(null, << 119.4113, -739.8093, 250.4530 >>, -1, bInstantAim)
					SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
					bRetaskGunman = FALSE
				ENDIF
				
				SWITCH i_dialogue_stage
					CASE 0
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, FALSE, FALSE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_POINT", FALSE, str_dialogue_root, TRUE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 7
			IF peds[mpf_1st_comms_room_1].bHasBeenKilled
				CREATE_COVERPOINT_PAIR(covPair_buddy, << 117.5014, -747.2892, 249.1522 >>, 249.4468, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_180)
			
				i_dialogue_stage = 0
				bRetaskBuddy	= TRUE
				bRetaskGunman 	= TRUE
				iCrewAIStage++
			ELSE
				SET_PED_RESET_FLAG(peds[mpf_1st_comms_room_1].id, PRF_ConsiderAsPlayerCoverThreatWithoutLOS, TRUE)
			
				// BUDDY
				IF NOT IS_PED_IN_COMBAT(BUDDY_PED_ID())
					TASK_COMBAT_PED(BUDDY_PED_ID(),peds[mpf_1st_comms_room_1].id)
				ENDIF
				
				// GUNMAN
				IF NOT IS_PED_IN_COMBAT(peds[mpf_goon].id)
					TASK_COMBAT_PED(peds[mpf_goon].id,peds[mpf_1st_comms_room_1].id)
				ENDIF
				
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_OUT", TRUE, str_dialogue_root)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 8
			IF NOT DOES_ENTITY_EXIST(peds[mpf_1st_comms_room_2].id)
			OR IS_PED_INJURED(peds[mpf_1st_comms_room_2].id)
			OR peds[mpf_1st_comms_room_2].bHasBeenKilled
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(peds[mpf_1st_comms_room_2].id)
			OR IS_PED_SHOOTING(peds[mpf_1st_comms_room_2].id)
			
				SET_PED_SPHERE_DEFENSIVE_AREA(BUDDY_PED_ID(), << 117.5887, -752.5247, 249.1522 >>, 1.5)
				SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_goon].id, << 114.4728, -748.7062, 249.1522 >>, 1.5)
			
				bRetaskBuddy	= TRUE
				bRetaskGunman 	= TRUE
				iCrewAIStage++
			ELSE
				// BUDDY
				//IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
				IF bRetaskBuddy
					TASK_SEEK_COVER_TO_COVER_POINT(BUDDY_PED_ID(), covPair_buddy.covID[covPair_buddy.id], << 121.2903, -752.4714, 250.2806 >>, -1, FALSE)
					bRetaskBuddy = FALSE
				ENDIF
				
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_PERFORM_SEQUENCE)
				OR bRetaskGunman
					SAFE_OPEN_SEQUENCE()
						IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), << 114.1068, -749.8569, 249.1522 >>, 1.0)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, << 114.1068, -749.8569, 249.1522 >>, << 124.2414, -755.4163, 250.6170 >>, PEDMOVE_RUN, FALSE, 0.2, 2.5)
							bInstantAim = TRUE
						ENDIF
						TASK_AIM_GUN_AT_COORD(null, << 124.2414, -755.4163, 250.6170 >>, -1, bInstantAim)
					SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
					bRetaskGunman = FALSE
				ENDIF
				
				SWITCH i_dialogue_stage
					CASE 0
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, FALSE, FALSE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_POINT", FALSE, str_dialogue_root, TRUE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 9
			IF peds[mpf_1st_comms_room_2].bHasBeenKilled
				CREATE_COVERPOINT_PAIR(covPair_buddy, << 124.8241, -754.5803, 249.1522 >>, 250.7867, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120)
			
				i_dialogue_stage = 0
				bRetaskBuddy	= TRUE
				bRetaskGunman 	= TRUE
				iCrewAIStage++
			ELSE
				// BUDDY
				IF NOT IS_PED_IN_COMBAT(BUDDY_PED_ID())
					TASK_COMBAT_PED(BUDDY_PED_ID(),peds[mpf_1st_comms_room_2].id)
				ENDIF
				
				// GUNMAN
				IF NOT IS_PED_IN_COMBAT(peds[mpf_goon].id)
					TASK_COMBAT_PED(peds[mpf_goon].id,peds[mpf_1st_comms_room_2].id)
				ENDIF
				
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_OUT", TRUE, str_dialogue_root)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 10
			IF peds[mpf_2nd_comms_room_1].bHasBeenKilled
			OR (DOES_ENTITY_EXIST(peds[mpf_2nd_comms_room_1].id)
			AND (IS_PED_INJURED(peds[mpf_2nd_comms_room_1].id)
			OR IS_PED_SHOOTING(peds[mpf_2nd_comms_room_1].id)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(peds[mpf_2nd_comms_room_1].id) ) )
			
			OR peds[mpf_2nd_comms_room_2].bHasBeenKilled
			OR ( DOES_ENTITY_EXIST(peds[mpf_2nd_comms_room_2].id)
			AND ( IS_PED_INJURED(peds[mpf_2nd_comms_room_2].id)
			OR IS_PED_SHOOTING(peds[mpf_2nd_comms_room_2].id)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(peds[mpf_2nd_comms_room_2].id) ) )
			
				SET_PED_SPHERE_DEFENSIVE_AREA(BUDDY_PED_ID(), << 124.8241, -754.5803, 249.1522 >>, 1.5)
				SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_goon].id, << 120.6721, -754.4247, 249.1522 >>, 1.0)
			
				bRetaskBuddy	= TRUE
				bRetaskGunman 	= TRUE
				iCrewAIStage++
			ELSE
				// BUDDY
				//IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
				IF bRetaskBuddy
					SAFE_OPEN_SEQUENCE()
						IF IS_PED_IN_COVER(BUDDY_PED_ID(), TRUE)
							TASK_EXIT_COVER(null, AIMING_COVER_EXIT, << 125.1018, -755.8192, 250.6866 >>)
						ENDIF
						IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(BUDDY_PED_ID()), << 124.8241, -754.5803, 249.1522 >>, 3.0)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, << 124.8241, -754.5803, 249.1522 >>, << 125.1018, -755.8192, 250.6866 >>, PEDMOVE_RUN, FALSE, 2.0, 0.5)
						ENDIF
						TASK_SEEK_COVER_TO_COVER_POINT(null, covPair_buddy.covID[covPair_buddy.id], << 127.4873, -756.7517, 250.5525 >>, -1, FALSE)
					SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
					bRetaskBuddy = FALSE
				ENDIF
				
				// GUNMAN
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_PERFORM_SEQUENCE)
				OR bRetaskGunman
					SAFE_OPEN_SEQUENCE()
						IF IS_PED_IN_COVER(peds[mpf_goon].id, TRUE)
							TASK_EXIT_COVER(null, AIMING_COVER_EXIT, << 126.6842, -756.2921, 250.6685 >>)
						ENDIF
						IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), << 121.0429, -755.6022, 249.1522 >>, 3.0)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, << 121.0429, -755.6022, 249.1522 >>, << 126.6842, -756.2921, 250.6685 >>, PEDMOVE_RUN, FALSE, 2.0, 0.5)
							bInstantAim = TRUE
						ENDIF
						TASK_AIM_GUN_AT_COORD(null, << 126.6842, -756.2921, 250.6685 >>, -1, bInstantAim)
					SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
					bRetaskGunman = FALSE
				ENDIF
				
				SWITCH i_dialogue_stage
					CASE 0
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, FALSE, FALSE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_POINT", FALSE, str_dialogue_root, TRUE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 11
			IF peds[mpf_2nd_comms_room_2].bHasBeenKilled
			AND peds[mpf_2nd_comms_room_1].bHasBeenKilled
				REMOVE_COVER_POINT(covPair_buddy.covID[covPair_buddy.id])
				CREATE_COVERPOINT_PAIR(covPair_gunman, << 137.1121, -757.1174, 249.1519 >>, 342.2234, COVUSE_WALLTOBOTH, COVHEIGHT_TOOHIGH, COVARC_180)
				
				// BUDDY
				SAFE_OPEN_SEQUENCE()
					IF IS_PED_IN_COVER(BUDDY_PED_ID(), TRUE)
						TASK_EXIT_COVER(null, CORNER_COVER_EXIT, << 139.2458, -758.1196, 250.6395 >>)
					ENDIF
					IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(BUDDY_PED_ID()), << 132.5270, -759.2299, 249.1519 >>, 2.0)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, << 132.5270, -759.2299, 249.1519 >>, << 139.2458, -758.1196, 250.6395 >>, PEDMOVE_RUN, FALSE, 0.5, 1.0)
						bInstantAim = TRUE
					ENDIF
					TASK_AIM_GUN_AT_COORD(null, << 139.2458, -758.1196, 250.6395 >>, -1, bInstantAim)
				SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
				
				// GUNMAN
				SAFE_OPEN_SEQUENCE()
					IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), << 137.1121, -757.1174, 249.1519 >>, 3.0)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, << 137.1121, -757.1174, 249.1519 >>, << 141.4854, -757.6714, 250.3407 >>, PEDMOVE_RUN, FALSE, 2.0, 0.5)
					ENDIF
					TASK_SEEK_COVER_TO_COVER_POINT(null, covPair_gunman.covID[covPair_gunman.id], << 140.9996, -756.3683, 250.1622 >>, -1, FALSE)
				SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
				
				i_dialogue_stage = 0
				bRetaskBuddy	= FALSE
				bRetaskGunman 	= FALSE
				iCrewAIStage++
			ELSE
				// BUDDY
				IF NOT IS_PED_IN_COMBAT(BUDDY_PED_ID())
					TASK_COMBAT_HATED_TARGETS_IN_AREA(BUDDY_PED_ID(), << 137.1840, -758.6304, 250.4749 >>, 5.0)
				ENDIF
				
				// GUNMAN
				IF NOT IS_PED_IN_COMBAT(peds[mpf_goon].id)
					TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_goon].id, << 137.1840, -758.6304, 250.4749 >>, 5.0)
				ENDIF
				
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_OUT", TRUE, str_dialogue_root)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 12
			IF IS_PED_IN_COMBAT(peds[mpf_pre_final_stairwell_1].id)
			OR peds[mpf_pre_final_stairwell_1].bHasBeenKilled
				CREATE_COVERPOINT_PAIR(covPair_gunman, << 144.7374, -758.4955, 249.3018 >>, 254.1820, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
				
				// BUDDY
				SAFE_OPEN_SEQUENCE()
					IF IS_PED_IN_COVER(BUDDY_PED_ID(), TRUE)
						TASK_EXIT_COVER(null, CORNER_COVER_EXIT, << 152.6020, -760.4814, 250.5413 >>)
					ENDIF
					IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(BUDDY_PED_ID()), << 144.2511, -755.1204, 249.1521 >>, 2.0)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, << 144.2511, -755.1204, 249.1521 >>, << 152.6020, -760.4814, 250.5413 >>, PEDMOVE_RUN, FALSE, 0.5, 1.0)
						bInstantAim = TRUE
					ENDIF
					TASK_AIM_GUN_AT_COORD(null, << 152.6020, -760.4814, 250.5413 >>, -1, bInstantAim)
				SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
				bRetaskBuddy = FALSE
				
			
				// GUNMAN
				SAFE_OPEN_SEQUENCE()
					IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), << 144.7374, -758.4955, 249.3018 >>, 3.0)
						TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(null, << 144.7374, -758.4955, 249.3018 >>, << 153.9947, -760.6960, 250.5582 >>, PEDMOVE_RUN, FALSE, 2.0, 0.5)
					ENDIF
					TASK_SEEK_COVER_TO_COVER_POINT(null, covPair_gunman.covID[covPair_gunman.id], << 153.9947, -760.6960, 250.5582 >>, -1, FALSE)
				SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
				bRetaskGunman = FALSE
				
			
				i_dialogue_stage = 0
				iCrewAIStage++
			ELSE 				
				SWITCH i_dialogue_stage
					CASE 0
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, FALSE, FALSE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_POINT", FALSE, str_dialogue_root, TRUE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 13
			IF peds[mpf_pre_final_stairwell_1].bHasBeenKilled
			OR ( DOES_ENTITY_EXIST(peds[mpf_pre_final_stairwell_1].id)
			AND ( IS_PED_SHOOTING(peds[mpf_pre_final_stairwell_1].id)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(peds[mpf_pre_final_stairwell_1].id) ) )

				SET_PED_SPHERE_DEFENSIVE_AREA(BUDDY_PED_ID(), <<141.8383, -754.6273, 249.1520>>, 1.5)
				SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_goon].id, << 144.7374, -758.4955, 249.3018 >>, 1.0)
				
				bRetaskBuddy	= TRUE
				bRetaskGunman 	= TRUE
				iCrewAIStage++
			ELSE
				
				SWITCH i_dialogue_stage
					CASE 0
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, FALSE, FALSE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_POINT", FALSE, str_dialogue_root, TRUE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 14
			IF peds[mpf_pre_final_stairwell_1].bHasBeenKilled
				CREATE_COVERPOINT_PAIR(covPair_gunman, << 152.5575, -761.7089, 249.1519 >>, 158.6884, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180)
				
				// BUDDY
				SAFE_OPEN_SEQUENCE()
					IF IS_PED_IN_COVER(BUDDY_PED_ID(), TRUE)
						TASK_EXIT_COVER(null, CORNER_COVER_EXIT, << 154.6000, -762.7058, 250.6459 >>)
					ENDIF
					IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(BUDDY_PED_ID()), << 155.9827, -761.1984, 249.1520 >>, 1.0)
						TASK_FOLLOW_NAV_MESH_TO_COORD(null, << 155.9827, -761.1984, 249.1520 >>, PEDMOVE_RUN)
					ENDIF
					TASK_AIM_GUN_AT_COORD(null, << 154.6000, -762.7058, 250.6459 >>, -1, FALSE)
				SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
				bRetaskBuddy = FALSE
				
				// GUNMAN
				SAFE_OPEN_SEQUENCE()
					IF IS_PED_IN_COVER(peds[mpf_goon].id, TRUE)
						TASK_EXIT_COVER(null, CORNER_COVER_EXIT, << 153.9947, -760.6960, 250.5582 >>)
					ENDIF
					TASK_SEEK_COVER_TO_COVER_POINT(null, covPair_gunman.covID[covPair_gunman.id], << 151.3350, -767.8516, 250.8267 >>, -1, FALSE)
				SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
				bRetaskGunman = FALSE
				
				
				i_dialogue_stage = 0
				iCrewAIStage++
			ELSE
				// BUDDY
				IF NOT IS_PED_IN_COMBAT(BUDDY_PED_ID())
					TASK_COMBAT_PED(BUDDY_PED_ID(), peds[mpf_pre_final_stairwell_1].id)
				ENDIF
				
				// GUNMAN
				IF NOT IS_PED_IN_COMBAT(peds[mpf_goon].id)
					TASK_COMBAT_PED(peds[mpf_goon].id, peds[mpf_pre_final_stairwell_1].id)
				ENDIF
				
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_OUT", TRUE, str_dialogue_root)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 15
			IF IS_PED_IN_COMBAT(peds[mpf_pre_final_stairwell_2].id)
			OR IS_PED_IN_COMBAT(peds[mpf_pre_final_stairwell_3].id)
			OR peds[mpf_pre_final_stairwell_2].bHasBeenKilled
			OR peds[mpf_pre_final_stairwell_3].bHasBeenKilled
			
				SET_PED_SPHERE_DEFENSIVE_AREA(BUDDY_PED_ID(), <<154.2395, -765.4312, 249.1519>>, 1.0)
				SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_goon].id, <<151.6309, -765.1901, 249.1519>>, 1.0)
			
				bRetaskBuddy	= TRUE
				bRetaskGunman	= TRUE
				iCrewAIStage++
			ELSE
				
				SWITCH i_dialogue_stage
					CASE 0
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, FALSE, FALSE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_POINT", FALSE, str_dialogue_root, TRUE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 16
			IF peds[mpf_pre_final_stairwell_2].bHasBeenKilled
			AND peds[mpf_pre_final_stairwell_3].bHasBeenKilled
				CREATE_COVERPOINT_PAIR(covPair_buddy, << 142.9844, -768.0378, 249.1519 >>, 344.5135, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
				CREATE_COVERPOINT_PAIR(covPair_gunman, << 140.8836, -767.2733, 249.1519 >>, 339.8291, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120)
				
				SAFE_OPEN_SEQUENCE()
					IF IS_PED_IN_COVER(BUDDY_PED_ID(), TRUE)
						TASK_EXIT_COVER(null, CORNER_COVER_EXIT, <<0,0,0>>)
					ENDIF
					IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(BUDDY_PED_ID()), << 142.9844, -768.0378, 249.1519 >>, 3.0)
						TASK_FOLLOW_NAV_MESH_TO_COORD(null, << 142.9844, -768.0378, 249.1519 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 2.0)
					ENDIF
					TASK_SEEK_COVER_TO_COVER_POINT(null, covPair_buddy.covID[covPair_buddy.id], << 142.9097, -765.1097, 250.6145 >>, -1, FALSE)
				SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
				bRetaskBuddy = FALSE
				
				SAFE_OPEN_SEQUENCE()
					IF IS_PED_IN_COVER(peds[mpf_goon].id, TRUE)
						TASK_EXIT_COVER(null, CORNER_COVER_EXIT, <<0,0,0>>)
					ENDIF
					IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), << 140.8836, -767.2733, 249.1519 >>, 3.0)
						TASK_FOLLOW_NAV_MESH_TO_COORD(null, << 140.8836, -767.2733, 249.1519 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 2.0)
					ENDIF
					TASK_SEEK_COVER_TO_COVER_POINT(null, covPair_gunman.covID[covPair_gunman.id], << 142.9097, -765.1097, 250.6145 >>, -1, FALSE)
				SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
				bRetaskGunman = FALSE
				
				
				i_dialogue_stage = 0
				
				iCrewAIStage++
			ELSE
				// BUDDY
				IF NOT IS_PED_IN_COMBAT(BUDDY_PED_ID())
					TASK_COMBAT_HATED_TARGETS_IN_AREA(BUDDY_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()), 30.0)
				ENDIF
				
				// GUNMAN
				IF NOT IS_PED_IN_COMBAT(peds[mpf_goon].id)
					TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_goon].id, GET_ENTITY_COORDS(PLAYER_PED_ID()), 30.0)
				ENDIF
				
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_OUT", TRUE, str_dialogue_root)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 17
			IF (DOES_ENTITY_EXIST(peds[mpf_final_rappel_room_1].id) OR peds[mpf_final_rappel_room_1].bHasBeenKilled)
			AND (DOES_ENTITY_EXIST(peds[mpf_final_rappel_room_2].id) OR peds[mpf_final_rappel_room_2].bHasBeenKilled)
				CREATE_COVERPOINT_PAIR(covPair_buddy, << 146.7832, -770.0289, 245.1520 >>, 252.9230, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_120)
				
				SAFE_OPEN_SEQUENCE()
					IF IS_PED_IN_COVER(BUDDY_PED_ID(), TRUE)
						TASK_EXIT_COVER(null, CORNER_COVER_EXIT, << 146.9380, -771.2317, 246.5175 >>)
					ENDIF
					IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(BUDDY_PED_ID()), << 146.7832, -770.0289, 245.1520 >>, 3.0)
						TASK_FOLLOW_NAV_MESH_TO_COORD(null, << 146.7832, -770.0289, 245.1520 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 2.0)
					ENDIF
					TASK_SEEK_COVER_TO_COVER_POINT(null, covPair_buddy.covID[covPair_buddy.id], << 150.1991, -771.1047, 246.6587 >>, -1, FALSE)
				SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
				bRetaskBuddy = FALSE
				
				bRetaskGunman 	= TRUE
				SETTIMERA(0)
				i_dialogue_stage = 0
				iCrewAIStage++
			ELSE
				bBuddyReadyToMoveOn = FALSE
				
				IF IS_PED_IN_COVER(BUDDY_PED_ID())
					bBuddyReadyToMoveOn = TRUE
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(peds[mpf_goon].id, <<151.801895,-761.862305,249.199417>>, <<156.684357,-763.542053,251.856689>>, 4.812500)
					SET_PED_CAPSULE(peds[mpf_goon].id, 0.325)
				ENDIF
				
				SWITCH i_dialogue_stage
					CASE 0
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, FALSE, FALSE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF bBuddyReadyToMoveOn
							IF IS_SAFE_TO_START_CONVERSATION()
							AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
								GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_DOWNST", FALSE, str_dialogue_root, TRUE)
								IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
									str_dialogue_root = ""
									i_dialogue_stage++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 2
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_POINT", FALSE, str_dialogue_root, TRUE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					BREAK
					CASE 3
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_DOWNST", FALSE, str_dialogue_root, TRUE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage--
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH

			ENDIF
		BREAK
		CASE 18
			IF peds[mpf_final_rappel_room_1].bHasBeenKilled
			AND ( DOES_ENTITY_EXIST(peds[mpf_final_rappel_room_1].id)
			OR ( IS_PED_SHOOTING(peds[mpf_final_rappel_room_1].id)
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(peds[mpf_final_rappel_room_1].id) ) )
			OR peds[mpf_final_rappel_room_2].bHasBeenKilled
			AND ( DOES_ENTITY_EXIST(peds[mpf_final_rappel_room_1].id)
			OR ( HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(peds[mpf_final_rappel_room_2].id) 
			OR IS_PED_SHOOTING(peds[mpf_final_rappel_room_2].id) ) )

				SET_PED_SPHERE_DEFENSIVE_AREA(BUDDY_PED_ID(), << 148.4145, -769.9975, 245.1520 >>, 2.0)
				SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_goon].id, << 151.8461, -772.6965, 245.1520 >>, 2.0)
			
				iCrewAIStage++
			ELSE
				
				IF TIMERA() > 1000
					// GUNMAN
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_PERFORM_SEQUENCE)
					OR bRetaskGunman
						SAFE_OPEN_SEQUENCE()
							IF IS_PED_IN_COVER(peds[mpf_goon].id, TRUE)
								TASK_EXIT_COVER(null, CORNER_COVER_EXIT, << 146.9380, -771.2317, 246.5175 >>)
							ENDIF
							IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), << 142.1409, -770.7808, 245.1520 >>, 3.0)
								TASK_FOLLOW_NAV_MESH_TO_COORD(null, << 142.1409, -770.7808, 245.1520 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 2.0)
							ENDIF
							TASK_AIM_GUN_AT_COORD(null, << 146.9380, -771.2317, 246.5175 >>, -1, FALSE)
						SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
						bRetaskGunman = FALSE
					ENDIF
				ENDIF
				
				SWITCH i_dialogue_stage
					CASE 0
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, FALSE, FALSE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_POINT", FALSE, str_dialogue_root, TRUE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 19			
			IF peds[mpf_final_rappel_room_1].bHasBeenKilled
			AND peds[mpf_final_rappel_room_2].bHasBeenKilled
				
				// BUDDY
				SAFE_OPEN_SEQUENCE()
					IF IS_PED_IN_COVER(BUDDY_PED_ID(), TRUE)
						TASK_EXIT_COVER(null, CORNER_COVER_EXIT, <<0,0,0>>)
					ENDIF
					IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(BUDDY_PED_ID()), << 154.2747, -761.4401, 245.1521 >>, 3.0)
						TASK_FOLLOW_NAV_MESH_TO_COORD(null, << 154.2747, -761.4401, 245.1521 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 2.0)
					ENDIF
					TASK_AIM_GUN_AT_COORD(null, << 161.1281, -762.9623, 246.3683 >>, -1, FALSE)
				SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
				bRetaskBuddy = FALSE
				
				// GUNMAN
				SAFE_OPEN_SEQUENCE()
					IF IS_PED_IN_COVER(peds[mpf_goon].id, TRUE)
						TASK_EXIT_COVER(null, CORNER_COVER_EXIT, <<0,0,0>>)
					ENDIF
					IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(peds[mpf_goon].id), << 152.8098, -764.4324, 245.1520 >>, 3.0)
						TASK_FOLLOW_NAV_MESH_TO_COORD(null, << 152.8098, -764.4324, 245.1520 >>, PEDMOVE_RUN, DEFAULT_TIME_BEFORE_WARP, 2.0)
					ENDIF
					TASK_AIM_GUN_AT_COORD(null, << 161.1281, -762.9623, 246.3683 >>, -1, FALSE)
				SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
				bRetaskBuddy = FALSE
				
				i_dialogue_stage = 0
				iCrewAIStage++
			ELSE
				// BUDDY
				IF NOT IS_PED_IN_COMBAT(BUDDY_PED_ID())
					TASK_COMBAT_HATED_TARGETS_IN_AREA(BUDDY_PED_ID(), << 154.1767, -762.4851, 245.1521 >>, 5.0)
				ENDIF
				
				// GUNMAN
				IF NOT IS_PED_IN_COMBAT(peds[mpf_goon].id)
					TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_goon].id, << 154.1767, -762.4851, 245.1521 >>, 5.0)
				ENDIF
				
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_OUT", TRUE, str_dialogue_root)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 20
			IF IS_CUTSCENE_PLAYING()
				iCrewAIStage++
			ELSE
				SWITCH i_dialogue_stage
					CASE 0
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root, FALSE, FALSE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF IS_SAFE_TO_START_CONVERSATION()
							IF GET_ROOM_KEY_FROM_ENTITY(MIKE_PED_ID()) = HASH("V_FIB02_IT1_off1")
								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_OPENING_M", CONV_PRIORITY_HIGH)
									i_dialogue_stage++
								ENDIF
							ELIF GET_ROOM_KEY_FROM_ENTITY(FRANK_PED_ID()) = HASH("V_FIB02_IT1_off1")
								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_OPENING_F", CONV_PRIORITY_HIGH)
									i_dialogue_stage++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 2
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_POINT", FALSE, str_dialogue_root, TRUE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
	ENDSWITCH
	
	SWITCH i_AudioSceneStage
		CASE 0
			IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_ESCAPE_CONTINUED_DOWN_RUBBLE")
				START_AUDIO_SCENE("AH_3B_ESCAPE_CONTINUED_DOWN_RUBBLE")
			ENDIF
			
			i_AudioSceneStage++
		BREAK
		CASE 1
			IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = GET_HASH_KEY("V_FIB02_str_IT")
				
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_ESCAPE_CONTINUED_DOWN_RUBBLE")
					STOP_AUDIO_SCENE("AH_3B_ESCAPE_CONTINUED_DOWN_RUBBLE")
				ENDIF	
				IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_ESCAPE_CONTINUED_STAIRWELL_02")
					START_AUDIO_SCENE("AH_3B_ESCAPE_CONTINUED_STAIRWELL_02")
				ENDIF
				
				i_AudioSceneStage++
			ENDIF
		BREAK
		CASE 2
			IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = GET_HASH_KEY("V_FIB02_IT1_post")
				
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_ESCAPE_CONTINUED_STAIRWELL_02")
					STOP_AUDIO_SCENE("AH_3B_ESCAPE_CONTINUED_STAIRWELL_02")
				ENDIF	
				IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_ESCAPE_TO_RAPPEL_POINT")
					START_AUDIO_SCENE("AH_3B_ESCAPE_TO_RAPPEL_POINT")
				ENDIF
				
				i_AudioSceneStage++
			ENDIF
		BREAK
		CASE 3
			IF IS_CUTSCENE_PLAYING()
			
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_ESCAPE_TO_RAPPEL_POINT")
					STOP_AUDIO_SCENE("AH_3B_ESCAPE_TO_RAPPEL_POINT")
				ENDIF
			
				i_AudioSceneStage++
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

//PURPOSE: Player abseils down the side of the building.
PROC ST_9_Abseil_Down()

	IF DOES_ENTITY_EXIST(BUDDY_PED_ID())
	AND NOT IS_PED_INJURED(BUDDY_PED_ID())
		SET_PED_RESET_FLAG(BUDDY_PED_ID(), PRF_BlockCustomAIEntryAnims, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
	AND NOT IS_PED_INJURED(peds[mpf_goon].id)
		SET_PED_RESET_FLAG(peds[mpf_goon].id, PRF_BlockCustomAIEntryAnims, TRUE)
	ENDIF

	IF NOT IS_PED_INJURED(FRANK_PED_ID())
		IF HAS_PED_GOT_WEAPON(FRANK_PED_ID(), GADGETTYPE_PARACHUTE)
			REMOVE_WEAPON_FROM_PED(FRANK_PED_ID(), GADGETTYPE_PARACHUTE)
			CLEAR_PED_PARACHUTE_PACK_VARIATION(FRANK_PED_ID())
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(MIKE_PED_ID())
		IF HAS_PED_GOT_WEAPON(MIKE_PED_ID(), GADGETTYPE_PARACHUTE)
			REMOVE_WEAPON_FROM_PED(MIKE_PED_ID(), GADGETTYPE_PARACHUTE)
			CLEAR_PED_PARACHUTE_PACK_VARIATION(MIKE_PED_ID())
		ENDIF
	ENDIF
		
	CONST_FLOAT	RAP_CAM_Y_DIST_START		-2.46
	CONST_FLOAT	RAP_CAM_Z_DIST_START		4.18
	
	CONST_FLOAT	RAP_CAM_Y_DIST_END			-3.745
	CONST_FLOAT	RAP_CAM_Z_DIST_END			1.945
	
	SWITCH mission_substage
		CASE STAGE_ENTRY
		
			i_dialogue_stage = 0
			ROPE_LOAD_TEXTURES()
			
			IF NOT bExitStateMichael
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael") 
				OR HAS_CUTSCENE_FINISHED()
					REPLAY_STOP_EVENT()
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					IF NOT IS_ENTITY_ATTACHED( MIKE_PED_ID() )
						FREEZE_ENTITY_POSITION(MIKE_PED_ID(), TRUE)
					ENDIF
					SET_PED_DROPS_WEAPONS_WHEN_DEAD(MIKE_PED_ID(), FALSE)
					bExitStateMichael = TRUE
				ENDIF
			ENDIF
			
			IF NOT bExitStateFrank
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin") 
				OR HAS_CUTSCENE_FINISHED()
					IF NOT IS_ENTITY_ATTACHED( FRANK_PED_ID() )
						FREEZE_ENTITY_POSITION(FRANK_PED_ID(), TRUE)
					ENDIF
					SET_PED_COMP_ITEM_CURRENT_SP(FRANK_PED_ID(), COMP_TYPE_SPECIAL2, SPECIAL2_P1_HARNESS, FALSE)
					SET_PED_DROPS_WEAPONS_WHEN_DEAD(FRANK_PED_ID(), FALSE)
					bExitStateFrank = TRUE
				ENDIF
			ENDIF
			
			IF NOT bExitStateGoon
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("gunman_selection_1") 
				OR HAS_CUTSCENE_FINISHED()
					IF NOT IS_ENTITY_ATTACHED( peds[mpf_goon].id )
						FREEZE_ENTITY_POSITION(peds[mpf_goon].id, TRUE)
					ENDIF
					bExitStateGoon = TRUE
				ENDIF
			ENDIF
			
			IF NOT bExitStateCam
				IF CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE)
				OR HAS_CUTSCENE_FINISHED()
					bExitStateCam = TRUE
				ENDIF
			ENDIF
			
			IF bExitStateMichael
			AND bExitStateFrank
			AND bExitStateGoon
			AND bExitStateCam
			AND HAVE_RAPPEL_ASSETS_LOADED()
			
				SAFE_REMOVE_BLIP(blip_Objective)
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(8, "9. Rappel out of building")

				Event_Finish(mef_manage_sprinklers)
				Unload_Asset_Model(sAssetData, V_ILEV_FIB_SPRKLR)
				Unload_Asset_Model(sAssetData, V_ILEV_FIB_SPRKLR_OFF)
				Unload_Asset_Model(sAssetData, V_ILEV_FIB_SPRKLR_ON)
				Unload_Asset_Interior(sAssetData, interior_fib02)
				Unload_Asset_Interior(sAssetData, interior_fib03)
				Unload_Asset_AssistedLine(sAssetData, "8_FIBStairs")
				Unload_Asset_AssistedLine(sAssetData, "off_stairs1")
				
				Event_Finish(mef_manage_radar_map)
				Event_Finish(mef_manage_smoke_vfx)
				
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_playerAttachedSmokeFX)
					STOP_PARTICLE_FX_LOOPED(ptfx_playerAttachedSmokeFX)
				ENDIF
				IF GET_TIMECYCLE_MODIFIER_INDEX() != -1
					CLEAR_TIMECYCLE_MODIFIER()
				ENDIF
				b_TimeCycleModifierActive = FALSE
				ped_PlayerAttachedSmokeAttachedPed 	= NULL
				
				iCrewAIStage = 1
				
				IF NOT IS_ENTITY_ATTACHED( MIKE_PED_ID() )
					FREEZE_ENTITY_POSITION(MIKE_PED_ID(), FALSE)
				ENDIF
				IF NOT IS_ENTITY_ATTACHED( FRANK_PED_ID() )
					FREEZE_ENTITY_POSITION(FRANK_PED_ID(), FALSE)
				ENDIF
				IF NOT IS_ENTITY_ATTACHED( peds[mpf_goon].id )
					FREEZE_ENTITY_POSITION(peds[mpf_goon].id, FALSE)
				ENDIF
				
				bHalfWayMusic = FALSE
				RAPPEL_Manage_1st_Descent_Player()
				RAPPEL_Manage_1st_Descent_Buddies()
				RAPPEL_Manage_Cams_1st_Descent()
				IF PLAYER_PED_ID() = MIKE_PED_ID()
					DO_RAPPEL_CAM(s_rappel_mike.sRappelData, TRUE, 0, 0, FALSE)
				ELIF PLAYER_PED_ID() = FRANK_PED_ID()
					DO_RAPPEL_CAM(s_rappel_frank.sRappelData, TRUE, 0, 0, FALSE)
				ENDIF

				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_ALLOW_PLAYER_DAMAGE)
				
				Load_Asset_AnimDict(sAssetData, animDict_IG9_dismount)
				Disable_Roads_And_Paths_Around_FIB_Building(TRUE)
				
				SETUP_FIB_COVERPOINTS(FALSE)
				
				Event_Trigger(mef_getaway_driver_arrival)
				Event_Trigger(mef_heli_rpl_flyby)
				Event_Trigger(mef_heli_news)
				
				i_dialogue_stage = 1
				
				TRIGGER_MUSIC_EVENT("AH3B_RAPPEL_STARTS")
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_RAPPEL_01")
					START_AUDIO_SCENE("AH_3B_RAPPEL_01")
				ENDIF
				
				Event_Pause(mef_manage_switch)
				
				Unload_Asset_Model(sAssetData, mod_fib_agent_2)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				
				i_time_of_last_convo = GET_GAME_TIMER()
				mission_substage++
			ENDIF
		BREAK
		CASE 1
			IF NOT Is_Event_Triggered(mef_ig9_dismount_player)
				// Show the rappel help
				IF NOT bRappelShownHelp
				AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					PRINT_HELP ("A3B_RPLHLP")
					bRappelShownHelp = TRUE
				ENDIF
				
				RAPPEL_Manage_1st_Descent_Player()
				RAPPEL_Manage_Cams_1st_Descent()
			
				IF (s_rappel_mike.bReachedEnd AND PLAYER_PED_ID() = MIKE_PED_ID())
				OR (s_rappel_frank.bReachedEnd AND PLAYER_PED_ID() = FRANK_PED_ID())
					Event_Trigger(mef_ig9_dismount_player)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					CLEAR_HELP()
				ENDIF
			ENDIF
		
			// Trigger the heli attack once both buddies are down
			// and the player is within range
			IF NOT Is_Event_Triggered(mef_ig9_dismount_buddies)
			
				RAPPEL_Manage_1st_Descent_Buddies()
			
				IF ((s_rappel_mike.bReachedEnd AND BUDDY_PED_ID() = MIKE_PED_ID() 	AND s_rappel_frank.sRappelData.vAttachPos.z < -171.317)
				OR (s_rappel_frank.bReachedEnd AND BUDDY_PED_ID() = FRANK_PED_ID()) AND s_rappel_mike.sRappelData.vAttachPos.z < -171.317 )
				AND s_rappel_goon.bReachedEnd
					
					IF IS_AUDIO_SCENE_ACTIVE("AH_3B_RAPPEL_01")
						STOP_AUDIO_SCENE("AH_3B_RAPPEL_01")
					ENDIF
					IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_FIGHT_CHOPPER")
						START_AUDIO_SCENE("AH_3B_FIGHT_CHOPPER")
					ENDIF
					
					TRIGGER_MUSIC_EVENT("AH3B_CHOPPER_APPEARS")

					Event_Trigger(mef_ig9_dismount_buddies)
					Event_Trigger(mef_heli_rpl_attack)
					
					Load_Asset_AnimDict(sAssetData, "missprologuewait_impatient")
				ENDIF
			ENDIF
			
			IF Is_Event_Triggered(mef_ig9_dismount_player)
			AND Is_Event_Complete(mef_ig9_dismount_buddies)
				mission_substage++
			ENDIF
		BREAK
		CASE 2
			IF IS_SAFE_TO_DISPLAY_GODTEXT()
			AND Is_Event_Complete(mef_ig9_dismount_buddies)
				
				Load_Asset_Model(sAssetData, P_CS_15M_ROPE_S)
				Load_Asset_AnimDict(sAssetData, animDict_IG10_rappel_over)
				PREPARE_SYNCHRONIZED_AUDIO_EVENT("AH_3B_IG_10", 0)
			
				PRINT_NOW("A3B_ATTHELI", DEFAULT_GOD_TEXT_TIME, 1)
				
				Event_Resume(mef_manage_switch)
				
				mission_substage++
			ENDIF
		BREAK
		CASE 3
			IF NOT DOES_BLIP_EXIST(vehs[mvf_heli_attack].blip)
				
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_FIGHT_CHOPPER")
					STOP_AUDIO_SCENE("AH_3B_FIGHT_CHOPPER")
				ENDIF
				IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_GET_TO_RAPPEL_POINT_02")
					START_AUDIO_SCENE("AH_3B_GET_TO_RAPPEL_POINT_02")
				ENDIF
				
				PLAY_SOUND_FROM_COORD(sounds[sfx_distant_sirens_fight], "Distant_Sirens_Fight", <<68.4, -779.1, 47.8>>, "FBI_HEIST_FINALE_CHOPPER")
				
				CLEAR_PRINTS()
				mission_substage++
				
			ELSE
				IF DOES_ENTITY_EXIST(BUDDY_PED_ID())
				AND NOT IS_PED_INJURED(BUDDY_PED_ID())
					MANAGE_AI_BUDDY_WEAPON()

					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_SYNCHRONIZED_SCENE) AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PLAY_ANIM)
						
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_COMBAT)
						
							PED_INDEX pedTarget
						
							IF NOT IS_PED_INJURED(vehs[mvf_heli_attack].peds[1].id)
								pedTarget = vehs[mvf_heli_attack].peds[1].id
							ELIF NOT IS_PED_INJURED(vehs[mvf_heli_attack].peds[2].id)
								pedTarget = vehs[mvf_heli_attack].peds[2].id
							ELSE
								pedTarget = vehs[mvf_heli_attack].peds[0].id
							ENDIF
							
							IF DOES_ENTITY_EXIST(pedTarget)
								TASK_COMBAT_PED(BUDDY_PED_ID(), pedTarget, COMBAT_PED_PREVENT_CHANGING_TARGET)
							ENDIF
						ELSE
							SET_PED_SPHERE_DEFENSIVE_AREA(BUDDY_PED_ID(), vHeliAttackBuddyDefensiveArea[iHeliAttackSelectedCoord], 3.0)
							SET_PED_ACCURACY(BUDDY_PED_ID(), 10)
						ENDIF
						
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
				AND NOT IS_PED_INJURED(peds[mpf_goon].id)
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_SYNCHRONIZED_SCENE)

						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_COMBAT)
						
							PED_INDEX pedTarget
						
							IF NOT IS_PED_INJURED(vehs[mvf_heli_attack].peds[1].id)
								pedTarget = vehs[mvf_heli_attack].peds[1].id
							ELIF NOT IS_PED_INJURED(vehs[mvf_heli_attack].peds[2].id)
								pedTarget = vehs[mvf_heli_attack].peds[2].id
							ELSE
								pedTarget = vehs[mvf_heli_attack].peds[0].id
							ENDIF
							
							IF DOES_ENTITY_EXIST(pedTarget)
								TASK_COMBAT_PED(peds[mpf_goon].id, pedTarget, COMBAT_PED_PREVENT_CHANGING_TARGET)
							ENDIF
						ELSE
							SET_PED_SPHERE_DEFENSIVE_AREA(peds[mpf_goon].id, vHeliAttackGoonDefensiveArea[iHeliAttackSelectedCoord], 3.0)
							SET_PED_ACCURACY(peds[mpf_goon].id, 10)
						ENDIF

					ENDIF
				ENDIF
			
			ENDIF
		BREAK
		CASE 4
			BOOL bMikeInPos, bFrankInPos
			BOOL bMikeCorona, bFrankCorona
			
			IF PLAYER_PED_ID() = MIKE_PED_ID()
				bMikeCorona = TRUE
			ELSE
				bFrankCorona = TRUE
			ENDIF
			
			LOAD_STREAM("Rappel_Foley_02", "FBI_HEIST_RAID")
			
			IF DOES_ENTITY_EXIST(MIKE_PED_ID())
			AND NOT IS_PED_INJURED(MIKE_PED_ID())
				IF IS_ENTITY_IN_ANGLED_AREA(MIKE_PED_ID(), <<150.212189,-722.344971,73.155525>>, <<147.916367,-721.499695,75.280525>>, 1.687500, FALSE)
					bMikeInPos = TRUE
				ELSE
					IF bMikeCorona
						IF DOES_BLIP_EXIST(blip_Objective)
							IF NOT ARE_VECTORS_EQUAL(GET_BLIP_COORDS(blip_Objective), <<149.01, -722.10, 73.16>>)
								SET_BLIP_COORDS(blip_Objective, <<149.01, -722.10, 73.16>>)
							ENDIF
						ELSE
							SAFE_BLIP_COORD(blip_Objective, <<149.01, -722.10, 73.16>>)
						ENDIF
						IS_ENTITY_AT_COORD(MIKE_PED_ID(), <<149.01, -722.10, 73.16>>, <<0.1,0.1,LOCATE_SIZE_HEIGHT>>, TRUE)
					ENDIF
					bMikeInPos = FALSE
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(FRANK_PED_ID())
			AND NOT IS_PED_INJURED(FRANK_PED_ID())
				IF IS_ENTITY_IN_ANGLED_AREA(FRANK_PED_ID(), <<156.640900,-725.267761,73.155525>>, <<158.715881,-726.532227,75.280525>>, 1.687500, FALSE)
					bFrankInPos = TRUE
				ELSE
					IF bFrankCorona
						IF DOES_BLIP_EXIST(blip_Objective)
							IF NOT ARE_VECTORS_EQUAL(GET_BLIP_COORDS(blip_Objective), <<156.640900,-725.267761,73.155525>>)
								SET_BLIP_COORDS(blip_Objective, <<156.640900,-725.267761,73.155525>>)
							ENDIF
						ELSE
							SAFE_BLIP_COORD(blip_Objective, <<156.640900,-725.267761,73.155525>>)
						ENDIF
						IS_ENTITY_AT_COORD(FRANK_PED_ID(), <<156.640900,-725.267761,73.155525>>, <<0.1,0.1,LOCATE_SIZE_HEIGHT>>, TRUE)
					ENDIF
					bFrankInPos = FALSE
				ENDIF
			ENDIF
		
			IF DOES_ENTITY_EXIST(MIKE_PED_ID()) AND NOT IS_PED_INJURED(MIKE_PED_ID())
			AND DOES_ENTITY_EXIST(FRANK_PED_ID()) AND NOT IS_PED_INJURED(FRANK_PED_ID())
				IF ((bMikeInPos AND MIKE_PED_ID() = PLAYER_PED_ID())
				OR (bFrankInPos AND FRANK_PED_ID() = PLAYER_PED_ID()))
				AND HAS_MODEL_LOADED(P_CS_15M_ROPE_S)
				AND HAS_ANIM_DICT_LOADED(animDict_IG10_rappel_over)
				AND PREPARE_SYNCHRONIZED_AUDIO_EVENT("AH_3B_IG_10", 0)
					
				// Michael's rappel
					IF MIKE_PED_ID() = PLAYER_PED_ID()
					
						cams[mcf_rappel_cam] = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
						objs[mof_rappel_rope].id = CREATE_OBJECT(P_CS_15M_ROPE_S, << 149.350, -721.591, 74.266 >>)
					
						syncedScenes[ssf_IG10_rappel_small] = CREATE_SYNCHRONIZED_SCENE(<< 149.350, -721.591, 74.266 >>, << 0.000, 0.000, 161.000 >>)
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), syncedScenes[ssf_IG10_rappel_small], animDict_IG10_rappel_over, "start_rappel_m_michael", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(objs[mof_rappel_rope].id, syncedScenes[ssf_IG10_rappel_small], "start_rappel_m_rope", animDict_IG10_rappel_over, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_CAM_ANIM(cams[mcf_rappel_cam], syncedScenes[ssf_IG10_rappel_small], "start_rappel_m_cam", animDict_IG10_rappel_over)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objs[mof_rappel_rope].id)

				// Franklin's rappel
					ELSE
					
						cams[mcf_rappel_cam] = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
						objs[mof_rappel_rope].id = CREATE_OBJECT(P_CS_15M_ROPE_S, << 157.975, -725.503, 74.266 >>)
					
						syncedScenes[ssf_IG10_rappel_small] = CREATE_SYNCHRONIZED_SCENE(<< 157.975, -725.503, 74.266 >>, << 0.000, 0.000, 148.250 >>)
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), syncedScenes[ssf_IG10_rappel_small], animDict_IG10_rappel_over, "start_rappel_m_michael", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(objs[mof_rappel_rope].id, syncedScenes[ssf_IG10_rappel_small], "start_rappel_m_rope", animDict_IG10_rappel_over, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						PLAY_SYNCHRONIZED_CAM_ANIM(cams[mcf_rappel_cam], syncedScenes[ssf_IG10_rappel_small], "start_rappel_m_cam", animDict_IG10_rappel_over)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(objs[mof_rappel_rope].id)
					
					ENDIF
					
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WaitingForPlayerControlInterrupt, FALSE)
					
					IF IS_AUDIO_SCENE_ACTIVE("AH_3B_GET_TO_RAPPEL_POINT_02")
						STOP_AUDIO_SCENE("AH_3B_GET_TO_RAPPEL_POINT_02")
					ENDIF
					IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_RAPPEL_02")
						START_AUDIO_SCENE("AH_3B_RAPPEL_02")
					ENDIF
					PLAY_SYNCHRONIZED_AUDIO_EVENT(syncedScenes[ssf_IG10_rappel_small])

					IF LOAD_STREAM("Rappel_Foley_02", "FBI_HEIST_RAID")
						PLAY_STREAM_FROM_PED(PLAYER_PED_ID())
					ENDIF
					
					Unload_Asset_Model(sAssetData, P_CS_15M_ROPE_S)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					
					Event_Pause(mef_manage_switch)
					
					SAFE_REMOVE_BLIP(blip_Objective)
					
					DISPLAY_RADAR(FALSE)
					DISPLAY_HUD(FALSE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					Unload_Asset_Anim_Dict(sAssetData, "missprologuewait_impatient")
					mission_substage++
				ELSE
				
				// get all crew into position
					IF PLAYER_PED_ID() != MIKE_PED_ID()
					
						IF NOT bMikeInPos
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(MIKE_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
								TASK_FOLLOW_NAV_MESH_TO_COORD(MIKE_PED_ID(), <<165.6748, -733.6248, 74.1555>>, PEDMOVE_RUN,  DEFAULT_TIME_BEFORE_WARP, 0.1, ENAV_STOP_EXACTLY, 312.1074)
							ENDIF
						ELSE
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(MIKE_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
								SAFE_OPEN_SEQUENCE()
									TASK_PLAY_ANIM(null, "missprologuewait_impatient", "trevor_waiting_impatient_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
									TASK_PLAY_ANIM(null, "missprologuewait_impatient", "trevor_waiting_impatient_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
									TASK_PLAY_ANIM(null, "missprologuewait_impatient", "trevor_waiting_impatient_c", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
									SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
								SAFE_PERFORM_SEQUENCE(MIKE_PED_ID())
							ENDIF
						ENDIF
					ENDIF
					
					IF PLAYER_PED_ID() != FRANK_PED_ID()
						IF NOT bFrankInPos
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(FRANK_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
								TASK_FOLLOW_NAV_MESH_TO_COORD(FRANK_PED_ID(), <<157.6914, -725.8945, 74.1555>>, PEDMOVE_RUN,  DEFAULT_TIME_BEFORE_WARP, 0.1, ENAV_STOP_EXACTLY, 312.1074)
							ENDIF
						ELSE
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(FRANK_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
								SAFE_OPEN_SEQUENCE()
									TASK_PLAY_ANIM(null, "missprologuewait_impatient", "trevor_waiting_impatient_c", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
									TASK_PLAY_ANIM(null, "missprologuewait_impatient", "trevor_waiting_impatient_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
									TASK_PLAY_ANIM(null, "missprologuewait_impatient", "trevor_waiting_impatient_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
									SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
								SAFE_PERFORM_SEQUENCE(FRANK_PED_ID())
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_IN_ANGLED_AREA(peds[mpf_goon].id, <<166.662521,-734.699158,73.155525>>, <<164.989883,-732.404602,75.280525>>, 1.687500)
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
							TASK_FOLLOW_NAV_MESH_TO_COORD(peds[mpf_goon].id, <<165.6748, -733.6248, 74.1555>>, PEDMOVE_RUN,  DEFAULT_TIME_BEFORE_WARP, 0.1, ENAV_STOP_EXACTLY, 312.1074)
						ENDIF
					ELSE
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_PERFORM_SEQUENCE)
							SAFE_OPEN_SEQUENCE()
								TASK_PLAY_ANIM(null, "missprologuewait_impatient", "trevor_waiting_impatient_b", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
								TASK_PLAY_ANIM(null, "missprologuewait_impatient", "trevor_waiting_impatient_c", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
								TASK_PLAY_ANIM(null, "missprologuewait_impatient", "trevor_waiting_impatient_a", SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS)
								SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
							SAFE_PERFORM_SEQUENCE(peds[mpf_goon].id)
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		BREAK
		CASE 5
			// Cut back to gameplay
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncedScenes[ssf_IG10_rappel_small])
			OR GET_SYNCHRONIZED_SCENE_PHASE(syncedScenes[ssf_IG10_rappel_small]) = 1.0
			
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				STOP_SYNCHRONIZED_ENTITY_ANIM(objs[mof_rappel_rope].id, INSTANT_BLEND_OUT, FALSE)
				DELETE_OBJECT(objs[mof_rappel_rope].id)
				
				CLEANUP_RAPPEL(s_rappel_mike.sRappelData)
				CLEANUP_RAPPEL(s_rappel_frank.sRappelData)
				CLEANUP_RAPPEL(s_rappel_goon.sRappelData)

				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				
			// >>>>>>>>> Player ped rappel stuff <<<<<<<<<<<<<<
				INIT_RAPPEL_WITH_ROPE(s_rappel_mike2.sRappelData, 	NULL, 	
					<<147.918, -720.178, 74.186>>, <<0.0000, 0.0000, 159.7800>>, 	
					-5.420, -(fRopeLength2) + fZLimitOffset, fRopeLength2, TRUE, FALSE, FALSE, MIKE_PED_ID(), TRUE, TRUE, DEFAULT_ROPE_WEIGHT, fCreateRopeLength2, TRUE,	RAPPEL_STATE_JUMPING_DOWN, 0.39)
					
				INIT_RAPPEL_WITH_ROPE(s_rappel_frank2.sRappelData, 	NULL, 	
					<<158.3680, -724.7775, 74.1858>>, <<0.0000, 0.0000, 149.5340>>, 	
					-5.420, -(fRopeLength2) + fZLimitOffset, fRopeLength2, TRUE, FALSE, FALSE, FRANK_PED_ID(), TRUE, TRUE, DEFAULT_ROPE_WEIGHT, fCreateRopeLength2, TRUE, 	RAPPEL_STATE_JUMPING_DOWN, 0.39)
				SET_RAPPEL_MATERIAL(s_rappel_mike2.sRappelData, RAPPEL_MATERIAL_GLASS)
				SET_RAPPEL_MATERIAL(s_rappel_frank2.sRappelData, RAPPEL_MATERIAL_GLASS)
				SET_RAPPEL_ALLOW_SMALL_JUMPS(s_rappel_mike2.sRappelData, TRUE)
				SET_RAPPEL_ALLOW_BIG_JUMPS(s_rappel_mike2.sRappelData, FALSE)
				SET_RAPPEL_BIG_JUMP_CONTROL_AS_NORMAL(s_rappel_mike2.sRappelData, TRUE)
				SET_RAPPEL_ALLOW_SMALL_JUMPS(s_rappel_frank2.sRappelData, TRUE)
				SET_RAPPEL_ALLOW_BIG_JUMPS(s_rappel_frank2.sRappelData, FALSE)
				SET_RAPPEL_BIG_JUMP_CONTROL_AS_NORMAL(s_rappel_frank2.sRappelData, TRUE)
				
				s_rappel_mike2.sRappelData.bAllowRopeMovement = TRUE
				s_rappel_mike2.sRappelData.vOverrideHand = <<150.4321, -719.2129, 69.6497>>
				s_rappel_mike2.sRappelData.iLastRopeVertex = 5
				s_rappel_mike2.sRappelData.iNextRopeVertex = 6
				//s_rappel_mike2.fSlowDownByLimitRange = 0.3
				HANDLE_RAPPEL_ROPE(s_rappel_mike2.sRappelData, FALSE, TRUE, TRUE)
				
				s_rappel_frank2.sRappelData.bAllowRopeMovement = TRUE
				s_rappel_frank2.sRappelData.vOverrideHand = <<150.4321, -719.2129, 69.6497>>
				s_rappel_frank2.sRappelData.iLastRopeVertex = 5
				s_rappel_frank2.sRappelData.iNextRopeVertex = 6
				//s_rappel_frank2.fSlowDownByLimitRange = 0.3
				HANDLE_RAPPEL_ROPE(s_rappel_frank2.sRappelData, FALSE, TRUE, TRUE)
				
				s_rappel_frank2.bIsRappeling 	= TRUE
				s_rappel_mike2.bIsRappeling 	= TRUE

				// Cameras
				DEFINE_RAPPEL_CAM_PROPERTIES(s_rappel_mike2.sRappelData, 
					-82.865, -4.735, 1.3, 5.4, 70.625, 2.0, 1.820, 0.04, 3.950, -2.890, 1.0, 35.640)
				DEFINE_RAPPEL_CAM_PROPERTIES(s_rappel_frank2.sRappelData, 
					-82.865, -4.735, 1.3, 8.625, 14.225, 2.0, 1.820, 0.04, 1.8, -1.4, 1.0, 35.640) 		// needs looking at
									
				SET_CAM_NEAR_DOF(s_rappel_mike2.sRappelData.cam, 2.0)
            	SET_CAM_FAR_DOF(s_rappel_mike2.sRappelData.cam, 18.0)
                SET_CAM_DOF_STRENGTH(s_rappel_mike2.sRappelData.cam, 0.2)
                  
                SET_CAM_NEAR_DOF(s_rappel_frank2.sRappelData.cam, 2.0)
            	SET_CAM_FAR_DOF(s_rappel_frank2.sRappelData.cam, 18.0)
                SET_CAM_DOF_STRENGTH(s_rappel_frank2.sRappelData.cam, 0.2)
				
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(2.0)
				CASCADE_SHADOWS_SET_ENTITY_TRACKER_SCALE(2.1)

				IF PLAYER_PED_ID() = MIKE_PED_ID()
					SET_CAM_ACTIVE(s_rappel_mike2.sRappelData.cam, TRUE)
				ELSE
					SET_CAM_ACTIVE(s_rappel_frank2.sRappelData.cam, TRUE)
				ENDIF
				DESTROY_CAM(cams[mcf_rappel_cam])

			// Gunman
				INIT_RAPPEL_WITH_ROPE(s_rappel_goon2.sRappelData, null, <<167.0205, -732.6820, 74.1858>>, <<0.0000, 0.0000, 126.9090>>, 	-5.420, -(fRopeLength2) + fZLimitOffset, fRopeLength2, FALSE, FALSE, FALSE, peds[mpf_goon].id, TRUE, TRUE, DEFAULT_ROPE_WEIGHT, fCreateRopeLength2, TRUE, 	RAPPEL_STATE_JUMPING_DOWN, 0.445)
				SET_RAPPEL_ALLOW_SMALL_JUMPS(s_rappel_goon2.sRappelData, TRUE)
				SET_RAPPEL_ALLOW_BIG_JUMPS(s_rappel_goon2.sRappelData, FALSE)
				s_rappel_goon2.bIsRappeling = TRUE

				#IF IS_DEBUG_BUILD
					
					v_debug_offset_m2 	= GET_ENTITY_COORDS(s_rappel_mike2.sRappelData.anchorObject)
					f_debug_heading_m2 	= GET_ENTITY_HEADING(s_rappel_mike2.sRappelData.anchorObject)
					
					v_debug_offset_f2 	= GET_ENTITY_COORDS(s_rappel_frank2.sRappelData.anchorObject)
					f_debug_heading_f2 	= GET_ENTITY_HEADING(s_rappel_frank2.sRappelData.anchorObject)
					
					v_debug_offset_g2 	= GET_ENTITY_COORDS(s_rappel_goon2.sRappelData.anchorObject)
					f_debug_heading_g2 	= GET_ENTITY_HEADING(s_rappel_goon2.sRappelData.anchorObject)
					
				#ENDIF
				
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				RAPPEL_Manage_2nd_Descent()
				
				STOP_STREAM()
			
				mission_substage++
			ENDIF
		BREAK
		CASE 6
			// Manage the peds rappels
			RAPPEL_Manage_2nd_Descent()
			
			IF NOT s_rappel_mike2.bIsRappeling
				IF BUDDY_PED_ID() = MIKE_PED_ID()
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(MIKE_PED_ID(), SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
						TASK_SEEK_COVER_TO_COVER_POINT(MIKE_PED_ID(), peds[mpf_mike].cov, <<138.8757, -718.3909, 47.5149>>, -1, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT s_rappel_frank2.bIsRappeling
				IF BUDDY_PED_ID() = FRANK_PED_ID()
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(FRANK_PED_ID(), SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
						TASK_SEEK_COVER_TO_COVER_POINT(FRANK_PED_ID(), peds[mpf_frank].cov, <<149.1408, -721.7725, 47.5948>>, -1, FALSE)
					ENDIF
				ENDIF
			ENDIF
			IF NOT s_rappel_goon2.bIsRappeling
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
					TASK_SEEK_COVER_TO_COVER_POINT(peds[mpf_goon].id, peds[mpf_goon].cov, <<130.2284, -719.3832, 47.3941>>, -1, FALSE)
				ENDIF
			ENDIF
				
			// move on once everyone is on the ground
			IF NOT s_rappel_mike2.bIsRappeling
			AND NOT s_rappel_frank2.bIsRappeling 
			AND NOT s_rappel_goon2.bIsRappeling
				Event_Resume(mef_manage_switch)
				Event_Trigger(mef_ground_agents)
				Mission_Set_Stage(msf_10_get_to_getaway_veh)
			ENDIF
		BREAK
	ENDSWITCH
	
// DIALOGUE
	SWITCH i_dialogue_stage
		CASE 1
			IF IS_SAFE_TO_START_CONVERSATION()
				IF GET_GAME_TIMER() - i_time_of_last_convo > 2000
					i_dialogue_stage++
				ELSE
					RAPPEL_Do_Random_Dialogue()
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_SERVICES", CONV_PRIORITY_HIGH)
					str_dialogue_root = ""
					i_dialogue_stage++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF IS_SAFE_TO_START_CONVERSATION()
			AND GET_GAME_TIMER() - i_time_of_last_convo > 500
				IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_SERVICEM1", CONV_PRIORITY_HIGH)
					str_dialogue_root = ""
					i_dialogue_stage++
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF IS_SAFE_TO_START_CONVERSATION()
			AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
				str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewHacker, "AH_CHOP")
				IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
					str_dialogue_root = ""
					i_dialogue_stage++
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_MICSEEHEL", CONV_PRIORITY_HIGH)
					str_dialogue_root = ""
					i_dialogue_stage++
				ENDIF
			ENDIF
		BREAK
		CASE 6
			IF Is_Event_Triggered(mef_heli_rpl_attack)
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_DOWN", CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
						i_dialogue_stage++
					ENDIF
				ENDIF
			ELSE
				IF (BUDDY_PED_ID() = MIKE_PED_ID() 	AND s_rappel_mike.sRappelData.vAttachPos.z > -165)
				OR (BUDDY_PED_ID() = FRANK_PED_ID() AND s_rappel_frank.sRappelData.vAttachPos.z > -165)
					RAPPEL_Do_Random_Dialogue()
				ENDIF
			ENDIF
		BREAK
		CASE 7 	
			IF IS_SAFE_TO_START_CONVERSATION()
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_NEEDSHOOT", CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
						i_dialogue_stage++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 8	FALLTHRU
		CASE 9
			IF mission_substage > 3
				str_dialogue_root = ""
				i_dialogue_stage = 10
			ELSE
				IF GET_GAME_TIMER() - i_time_of_last_convo > 1500
					
					IF IS_STRING_NULL_OR_EMPTY(str_dialogue_root)
						IF i_dialogue_stage = 8
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_OUT", TRUE, str_dialogue_root)
							i_dialogue_stage++
						ELSE
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_COWER", TRUE, str_dialogue_root)
							i_dialogue_stage = 8
						ENDIF
					ENDIF
				
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 10
			IF mission_substage >  4
				str_dialogue_root = ""
				i_dialogue_stage = 12
			ELSE
				IF IS_SAFE_TO_START_CONVERSATION()
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_RESPOND", FALSE, str_dialogue_root, TRUE)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						REPLAY_RECORD_BACK_FOR_TIME(8.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
						str_dialogue_root = ""
						i_dialogue_stage++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 11
			IF mission_substage >  4
				str_dialogue_root = ""
				i_dialogue_stage++
			ELSE
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_REPEL", FALSE, str_dialogue_root, TRUE)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
						i_dialogue_stage++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 12
			IF mission_substage >  4
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_RIG", CONV_PRIORITY_HIGH)
						REPLAY_RECORD_BACK_FOR_TIME(8.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
						i_dialogue_stage++
					ENDIF
				ENDIF
			ELSE
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_REPEL", FALSE, str_dialogue_root, TRUE)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 13
			RAPPEL_Do_Random_Dialogue()
		BREAK
	ENDSWITCH
ENDPROC

//PURPOSE: Player gets to an escape vehicle on the ground and drives away to lose wanted level.
PROC ST_10_Get_To_The_Getaway_Vehicle()

	IF DOES_ENTITY_EXIST(BUDDY_PED_ID())
	AND NOT IS_PED_INJURED(BUDDY_PED_ID())
		SET_PED_RESET_FLAG(BUDDY_PED_ID(), PRF_BlockCustomAIEntryAnims, TRUE)
		MANAGE_AI_BUDDY_WEAPON()
	ENDIF
	
	IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
	AND NOT IS_PED_INJURED(peds[mpf_goon].id)
		SET_PED_RESET_FLAG(peds[mpf_goon].id, PRF_BlockCustomAIEntryAnims, TRUE)
	ENDIF

	// Unload rappel over anim dictionary
	IF NOT s_rappel_mike2.bIsRappeling AND NOT s_rappel_frank2.bIsRappeling AND NOT s_rappel_goon2.bIsRappeling
		Unload_Asset_Anim_Dict(sAssetData, animDict_IG10_rappel_over)
		Unload_Asset_Model(sAssetData, GET_RAPPEL_PULLEY_MODEL_NAME())
		SET_RAPPEL_ASSETS_AS_NO_LONGER_NEEDED()
	ENDIF

	SWITCH mission_substage
		CASE STAGE_ENTRY
			// "go to the street for pickup"
			SAFE_BLIP_COORD(blip_Objective, vStreetPickupLocation)
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(9, "10. Get to the getaway vehicle")
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_SHOOTING)
			
			Add_Peds_For_Dialogue()
			
			PLAY_ALARM_FIRE(FALSE, FALSE)
			PLAY_ALARM_SECURITY(FALSE, FALSE)
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_GET_TO_VEHICLE")
				START_AUDIO_SCENE("AH_3B_GET_TO_VEHICLE")
			ENDIF
			
			i_dialogue_stage 				= 0
			str_dialogue_root 				= ""
			bGetwayPickUpReached			= FALSE
			bSwatHaveArrived				= FALSE
			iCrewAIStage					= 0
			bExitStateMichael				= FALSE
			bExitStateFrank					= FALSE
			bRetaskBuddy 					= TRUE
			bRetaskGunman 					= TRUE
			mission_substage++
		BREAK
		CASE 1
			IF IS_SAFE_TO_START_CONVERSATION()
				GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_GROUN", FALSE, str_dialogue_root, TRUE)
				IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
					REPLAY_RECORD_BACK_FOR_TIME(8.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
					str_dialogue_root = ""
					mission_substage++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_SAFE_TO_DISPLAY_GODTEXT()
				PRINT_NOW("A3B_ESCPKUP", DEFAULT_GOD_TEXT_TIME, 1)
				mission_substage++
			ENDIF
		BREAK
		CASE 3
			IF DOES_BLIP_EXIST(blip_Objective)
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_BLIP_COORDS(blip_Objective)) < 30.0
				SAFE_REMOVE_BLIP(blip_Objective)
				mission_substage++
			ENDIF
		BREAK
		CASE 4
			SWITCH i_dialogue_stage
				CASE 0
					// Michael/franklin question where the driver is
					IF IS_SAFE_TO_START_CONVERSATION()
						GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_AMB_1", FALSE, str_dialogue_root)
						IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
							bGetwayPickUpReached = TRUE
							str_dialogue_root = ""
							i_dialogue_stage++
						ENDIF
					ENDIF
				BREAK
				CASE 1
					// driver signals them from the ambulance
					IF GET_DRIVER_SKILL_LEVEL() = CMSK_GOOD
						IF IS_SAFE_TO_START_CONVERSATION()
							str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewDriver, "AH_AMB")
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					ELSE
						IF IS_SAFE_TO_START_CONVERSATION()
							str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewDriver, "AH_NOT")
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 2
					i_dialogue_stage = 0
					mission_substage++
				BREAK
			ENDSWITCH
		BREAK
		// Waiting for the bad driver to arrive
		CASE 5
			IF bDriverHasArrived
				SAFE_BLIP_VEHICLE(vehs[mvf_escape].blip, vehs[mvf_escape].id, FALSE)
				
				//INCREMENT_DRIVER_STATS_DURING_HEIST(HEIST_AGENCY, crewDriver)
				
				i_dialogue_stage = 0
				mission_substage++
			ELSE
				SWITCH i_dialogue_stage
					CASE 0
						IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
							IF IS_SAFE_TO_START_CONVERSATION()
								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_SWAT_KD", CONV_PRIORITY_HIGH)
									i_dialogue_stage++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF IS_SAFE_TO_START_CONVERSATION()
							IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_DISC_KD", CONV_PRIORITY_HIGH)
								i_dialogue_stage++
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
		BREAK
		CASE 6
			IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehs[mvf_escape].id)
				SAFE_REMOVE_BLIP(vehs[mvf_escape].blip)
				mission_substage++
			ELSE
				SWITCH i_dialogue_stage
					CASE 0
						// michael/franklin see the driver in the ambulance
						IF GET_DRIVER_SKILL_LEVEL() = CMSK_GOOD
							IF IS_SAFE_TO_START_CONVERSATION()
								GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_AMB_2", FALSE, str_dialogue_root)
								IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
									str_dialogue_root = ""
									i_dialogue_stage++
								ENDIF
							ENDIF
						ELSE
							IF IS_SAFE_TO_START_CONVERSATION()
								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_TURNUP_KD", CONV_PRIORITY_HIGH)
									REPLAY_RECORD_BACK_FOR_TIME(8.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
									str_dialogue_root = ""
									i_dialogue_stage++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 1
						// tell the player to get in
						IF IS_SAFE_TO_DISPLAY_GODTEXT()
							IF GET_ENTITY_MODEL(vehs[mvf_escape].id) = AMBULANCE
								PRINT_NOW("A3B_ESCAMB1", DEFAULT_GOD_TEXT_TIME, 1)
							ELSE
								PRINT_NOW("A3B_ESCVAN1", DEFAULT_GOD_TEXT_TIME, 1)
							ENDIF
							mission_substage++
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF
			IF mission_substage > 6
				i_dialogue_stage = 0
			ENDIF
		BREAK
		CASE 7
			
			IF Is_Event_Triggered(mef_manage_switch)
			AND swState = SWITCH_NOSTATE
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehs[mvf_escape].id, TRUE)
				OR IS_PED_IN_VEHICLE(BUDDY_PED_ID(), vehs[mvf_escape].id, TRUE)
				OR GET_DISTANCE_BETWEEN_ENTITIES(BUDDY_PED_ID(), vehs[mvf_escape].id) < 5.0
					Event_Finish(mef_manage_switch)
				ENDIF
			ENDIF
		
			IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehs[mvf_escape].id)
				
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_GET_TO_VEHICLE")
					STOP_AUDIO_SCENE("AH_3B_GET_TO_VEHICLE")
				ENDIF
				IF NOT IS_AUDIO_SCENE_ACTIVE("AH_3B_GET_TO_FRANKLINS")
					START_AUDIO_SCENE("AH_3B_GET_TO_FRANKLINS")
				ENDIF
				
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					TRIGGER_MUSIC_EVENT("AH3B_VAN_ENTERED_WANTED")
					bScorePlaying = TRUE
				ELSE
					TRIGGER_MUSIC_EVENT("AH3B_VAN_ENTERED")
					bScorePlaying = FALSE
				ENDIF
				
				CLEAR_AREA_OF_VEHICLES(vEndAtFranklinsCoord, 20.0)
				SAFE_REMOVE_BLIP(vehs[mvf_escape].blip)

				REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)

				Mission_Set_Stage(msf_11_escape_to_franklins)
			ELSE
				// Tell the player to get in the ambulance
				SWITCH i_dialogue_stage
					CASE 0	
						IF GET_DRIVER_SKILL_LEVEL() = CMSK_GOOD
							IF IS_SAFE_TO_START_CONVERSATION()			
								GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_AMB_3", FALSE, str_dialogue_root)
								IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
									str_dialogue_root = ""
									i_dialogue_stage++
								ENDIF
							ENDIF
						ELSE
							IF IS_SAFE_TO_START_CONVERSATION()			
								IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_UDRV_KD", CONV_PRIORITY_HIGH)
									bGetwayPickUpReached = TRUE
									str_dialogue_root = ""
									i_dialogue_stage++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_time_of_last_convo > 3000
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_DGI", FALSE, str_dialogue_root, TRUE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
				
			ENDIF
		BREAK
	ENDSWITCH
	
	SWITCH iCrewAIStage 
		CASE 0
			IF mEvents[mef_ground_agents].iStage > 2
				bRetaskBuddy 	= TRUE
				bRetaskGunman 	= TRUE
				iCrewAIStage++
			ELSE
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_PED_INJURED(peds[mpf_ground_pre_ent_1].id)
			OR NOT IS_PED_INJURED(peds[mpf_ground_pre_ent_2].id)
			OR NOT IS_PED_INJURED(peds[mpf_ground_pre_ent_3].id)
				
				IF (BUDDY_MPF() = mpf_frank AND NOT s_rappel_frank2.bIsRappeling)
				OR (BUDDY_MPF() = mpf_mike AND NOT s_rappel_mike2.bIsRappeling)
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
					OR bRetaskBuddy
					
						SET_PED_COMBAT_PARAMS(peds[BUDDY_MPF()], <<121.4464, -709.9032, 46.0770>>, 3.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, FALSE, FALSE, FALSE)
						TASK_COMBAT_HATED_TARGETS_IN_AREA(BUDDY_PED_ID(), <<105.33, -725.83, 46.08>>, 21.250)
						bRetaskBuddy = FALSE
						
					ENDIF
				ENDIF
				
				IF NOT s_rappel_goon2.bIsRappeling
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
					OR bRetaskGunman
					
						SET_PED_COMBAT_PARAMS(peds[mpf_goon], <<130.2455, -719.3640, 46.0770>>, 3.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, FALSE, FALSE, FALSE)
						TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_goon].id, <<105.33, -725.83, 46.08>>, 21.250)
						bRetaskGunman = FALSE
						
					ENDIF
				ENDIF
				
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_OUT", TRUE, str_dialogue_root)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
				
			ELSE
				bRetaskBuddy 	= TRUE
				bRetaskGunman 	= TRUE
				iCrewAIStage++
			ENDIF
		BREAK
		CASE 2
			IF mEvents[mef_ground_agents].iStage > 3 
				bRetaskBuddy 	= TRUE
				bRetaskGunman 	= TRUE
				iCrewAIStage++
			ELSE
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 7500
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_MOVEON", TRUE, str_dialogue_root)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF NOT IS_PED_INJURED(peds[mpf_ground_ent_1].id)
			OR NOT IS_PED_INJURED(peds[mpf_ground_ent_2].id)
			OR NOT IS_PED_INJURED(peds[mpf_ground_ent_3].id)
			
				IF (BUDDY_MPF() = mpf_frank AND NOT s_rappel_frank2.bIsRappeling)
				OR (BUDDY_MPF() = mpf_mike AND NOT s_rappel_mike2.bIsRappeling)
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
					OR bRetaskBuddy
					
						SET_PED_COMBAT_PARAMS(peds[BUDDY_MPF()], <<98.6859, -723.5784, 46.0770>>, 3.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, FALSE, FALSE, FALSE)
						TASK_COMBAT_HATED_TARGETS_IN_AREA(BUDDY_PED_ID(), <<101.24, -740.07, 44.76>>, 21.250)
						bRetaskBuddy = FALSE
						
					ENDIF
				ENDIF
				
				IF NOT s_rappel_goon2.bIsRappeling
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
					OR bRetaskGunman
					
						SET_PED_COMBAT_PARAMS(peds[mpf_goon], <<111.4017, -727.0502, 46.0770>>, 3.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, FALSE, FALSE, FALSE)
						TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_goon].id, <<101.24, -740.07, 44.76>>, 21.250)
						bRetaskGunman = FALSE
						
					ENDIF
				ENDIF
				
				IF IS_SAFE_TO_START_CONVERSATION()
				AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
					GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_OUT", TRUE, str_dialogue_root)
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
					ENDIF
				ENDIF
				
			ELSE
				CREATE_COVERPOINT_PAIR(covPair_buddy, <<95.7508, -735.1851, 44.7604>>, 66.2413, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
				CREATE_COVERPOINT_PAIR(covPair_gunman, <<101.1608, -749.3187, 44.7559>>, 90.6467, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_120)
				
				bRetaskBuddy 	= TRUE
				bRetaskGunman 	= TRUE
				iCrewAIStage++
			ENDIF
		BREAK
		CASE 4		
			IF bGetwayPickUpReached
			AND bDriverHasArrived
				bRetaskBuddy 	= TRUE
				bRetaskGunman 	= TRUE
				iCrewAIStage++
			ELSE
			
				SET_PED_COMBAT_PARAMS(peds[BUDDY_MPF()], <<95.7508, -735.1851, 44.7604>>, 1.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, FALSE, FALSE, FALSE)
				SET_PED_COMBAT_PARAMS(peds[mpf_goon], <<101.1608, -749.3187, 44.7559>>, 1.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, FALSE, FALSE, FALSE)
			
				IF bSwatHaveArrived
				AND (NOT IS_PED_INJURED(vehs[mvf_late_fib_1].peds[0].id)
				OR NOT IS_PED_INJURED(vehs[mvf_late_fib_1].peds[1].id)
				OR NOT IS_PED_INJURED(vehs[mvf_late_fib_1].peds[2].id)
				OR NOT IS_PED_INJURED(vehs[mvf_late_fib_2].peds[0].id)
				OR NOT IS_PED_INJURED(vehs[mvf_late_fib_2].peds[1].id)
				OR NOT IS_PED_INJURED(vehs[mvf_late_fib_2].peds[2].id))
				
					// FIGHT THE SWAT GUYS
					IF (BUDDY_MPF() = mpf_frank AND NOT s_rappel_frank2.bIsRappeling)
					OR (BUDDY_MPF() = mpf_mike AND NOT s_rappel_mike2.bIsRappeling)

						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
						OR bRetaskBuddy
							TASK_COMBAT_HATED_TARGETS_IN_AREA(BUDDY_PED_ID(), <<62.23, -734.86, 43.07>>, 45.0)
							bRetaskBuddy = FALSE
						ENDIF
						
					ENDIF
					
					IF NOT s_rappel_goon2.bIsRappeling
					
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
						OR bRetaskGunman
							TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_goon].id, <<62.23, -734.86, 43.07>>, 45.0)
							bRetaskGunman = FALSE
						ENDIF

					ENDIF
					
					IF IS_SAFE_TO_START_CONVERSATION()
					AND GET_GAME_TIMER() - i_time_of_last_convo > 1000
						GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_OUT", TRUE, str_dialogue_root)
						IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
						ENDIF
					ENDIF
				
				// HIDE IN COVER UNTIL GETAWAY DRIVER ARRIVES
				ELSE

					IF (BUDDY_MPF() = mpf_frank AND NOT s_rappel_frank2.bIsRappeling)
					OR (BUDDY_MPF() = mpf_mike AND NOT s_rappel_mike2.bIsRappeling)

						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
						OR bRetaskBuddy
							TASK_SEEK_COVER_TO_COVER_POINT(BUDDY_PED_ID(), covPair_buddy.covID[covPair_buddy.id], <<65.5344, -736.5551, 45.4244>>, -1, FALSE)
							bRetaskBuddy = FALSE
						ENDIF
						
					ENDIF
					
					IF NOT s_rappel_goon2.bIsRappeling
					
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_SEEK_COVER_TO_COVER_POINT)
						OR bRetaskGunman
							TASK_SEEK_COVER_TO_COVER_POINT(peds[mpf_goon].id, covPair_gunman.covID[covPair_gunman.id], <<65.5344, -736.5551, 45.4244>>, -1, FALSE)
							bRetaskGunman = FALSE
						ENDIF

					ENDIF
				
				ENDIF
			ENDIF
		BREAK
		CASE 5
			ENTER_EXIT_VEHICLE_FLAGS eVehEnterFlags
			BOOL bIsWanted
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				eVehEnterFlags = ECF_RESUME_IF_INTERRUPTED | ECF_DONT_CLOSE_DOOR
				bIsWanted = TRUE
			ELSE
				eVehEnterFlags = ECF_RESUME_IF_INTERRUPTED
				bIsWanted = FALSE
			ENDIF
		
			IF ((BUDDY_MPF() = mpf_frank AND NOT s_rappel_frank2.bIsRappeling)
			OR (BUDDY_MPF() = mpf_mike AND NOT s_rappel_mike2.bIsRappeling))
			AND DOES_ENTITY_EXIST(peds[BUDDY_MPF()].id)
			AND NOT IS_PED_INJURED(peds[BUDDY_MPF()].id)
				
				IF IS_PED_IN_VEHICLE(peds[BUDDY_MPF()].id, vehs[mvf_escape].id, FALSE)
				
					IF bIsWanted
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[BUDDY_MPF()].id, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)
						OR bRetaskBuddy
							SET_PED_COMBAT_ATTRIBUTES(peds[BUDDY_MPF()].id, CA_LEAVE_VEHICLES, FALSE) 
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(peds[BUDDY_MPF()].id, 100)
							bRetaskBuddy = FALSE
						ENDIF
					ENDIF
			
				ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(BUDDY_PED_ID()), GET_ENTITY_COORDS(vehs[mvf_escape].id)) < 10.0
				
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_ENTER_VEHICLE)
					OR bRetaskBuddy
						TASK_ENTER_VEHICLE(BUDDY_PED_ID(), vehs[mvf_escape].id, DEFAULT_TIME_NEVER_WARP, VS_BACK_LEFT, PEDMOVE_RUN, eVehEnterFlags)
						bRetaskBuddy = FALSE
					ENDIF
				
				
				// GO TO THE VAN (combating OR running)
				ELSE
				
					SET_PED_COMBAT_PARAMS(peds[BUDDY_MPF()], <<0,0,0>>, 0.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, FALSE, FALSE, FALSE)
					SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(peds[BUDDY_MPF()].id, vehs[mvf_escape].id, <<0,0,0>>, 5.0, FALSE)
					
					IF NOT IS_PED_INJURED(vehs[mvf_late_fib_1].peds[0].id)
					OR NOT IS_PED_INJURED(vehs[mvf_late_fib_1].peds[1].id)
					OR NOT IS_PED_INJURED(vehs[mvf_late_fib_1].peds[2].id)
					OR NOT IS_PED_INJURED(vehs[mvf_late_fib_2].peds[0].id)
					OR NOT IS_PED_INJURED(vehs[mvf_late_fib_2].peds[1].id)
					OR NOT IS_PED_INJURED(vehs[mvf_late_fib_2].peds[2].id)
					
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
						OR bRetaskBuddy
							TASK_COMBAT_HATED_TARGETS_IN_AREA(BUDDY_PED_ID(), <<62.23, -734.86, 43.07>>, 100.0)
							bRetaskBuddy = FALSE
						ENDIF
						
					ELSE
						
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
						OR bRetaskBuddy						
							TASK_FOLLOW_NAV_MESH_TO_COORD(BUDDY_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehs[mvf_escape].id, <<0,-2.0,0>>), PEDMOVE_RUN)
							bRetaskBuddy = FALSE
						ENDIF

					ENDIF
				ENDIF
				
			ENDIF
			
			
			IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
			AND NOT IS_PED_INJURED(peds[mpf_goon].id)
			AND NOT s_rappel_goon2.bIsRappeling
			
				IF IS_PED_IN_VEHICLE(peds[mpf_goon].id, vehs[mvf_escape].id, FALSE)
				
					IF bIsWanted
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)
						OR bRetaskGunman
							SET_PED_COMBAT_ATTRIBUTES(peds[mpf_goon].id, CA_LEAVE_VEHICLES, FALSE)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(peds[mpf_goon].id, 100)
							bRetaskGunman = FALSE
						ENDIF
					ENDIF
			
				ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(peds[mpf_goon].id), GET_ENTITY_COORDS(vehs[mvf_escape].id)) < 10.0
				OR NOT bIsWanted
				
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_ENTER_VEHICLE)
					OR bRetaskGunman
						TASK_ENTER_VEHICLE(peds[mpf_goon].id, vehs[mvf_escape].id, DEFAULT_TIME_NEVER_WARP, VS_BACK_RIGHT, PEDMOVE_RUN, eVehEnterFlags)
						bRetaskGunman = FALSE
					ENDIF
					
				// GO TO THE VAN (combat OR run)
				ELSE
				
					SET_PED_COMBAT_PARAMS(peds[mpf_goon], <<0,0,0>>, 0.0, CM_DEFENSIVE, CR_MEDIUM, TLR_NEVER_LOSE_TARGET, TRUE, 1.0, 0.0, FALSE, FALSE, FALSE)
					SET_PED_DEFENSIVE_SPHERE_ATTACHED_TO_VEHICLE(peds[mpf_goon].id, vehs[mvf_escape].id, <<0,0,0>>, 5.0, FALSE)
				
					IF NOT IS_PED_INJURED(vehs[mvf_late_fib_1].peds[0].id)
					OR NOT IS_PED_INJURED(vehs[mvf_late_fib_1].peds[1].id)
					OR NOT IS_PED_INJURED(vehs[mvf_late_fib_1].peds[2].id)
					OR NOT IS_PED_INJURED(vehs[mvf_late_fib_2].peds[0].id)
					OR NOT IS_PED_INJURED(vehs[mvf_late_fib_2].peds[1].id)
					OR NOT IS_PED_INJURED(vehs[mvf_late_fib_2].peds[2].id)
				
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_COMBAT_HATED_TARGETS_IN_AREA)
						OR bRetaskGunman
							TASK_COMBAT_HATED_TARGETS_IN_AREA(peds[mpf_goon].id, <<62.23, -734.86, 43.07>>, 45.0)
							bRetaskGunman = FALSE
						ENDIF
						
					ELSE
					
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
						OR bRetaskGunman						
							TASK_FOLLOW_NAV_MESH_TO_COORD(peds[mpf_goon].id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehs[mvf_escape].id, <<0,-2.0,0>>), PEDMOVE_RUN)
							bRetaskGunman = FALSE
						ENDIF
					
					ENDIF
				ENDIF

			ENDIF
			
			IF DOES_ENTITY_EXIST(peds[mpf_driver].id)
			AND NOT IS_PED_INJURED(peds[mpf_driver].id)
				
				IF IS_PED_IN_VEHICLE(peds[mpf_driver].id, vehs[mvf_escape].id, FALSE)
					IF GET_SEAT_PED_IS_IN(peds[mpf_driver].id) = VS_DRIVER
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_driver].id, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
							TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(peds[mpf_driver].id, vehs[mvf_escape].id)
						ENDIF
					ELSE
						IF bIsWanted
							IF NOT IS_PED_INJURED(vehs[mvf_late_fib_1].peds[0].id)
							OR NOT IS_PED_INJURED(vehs[mvf_late_fib_1].peds[1].id)
							OR NOT IS_PED_INJURED(vehs[mvf_late_fib_1].peds[2].id)
							OR NOT IS_PED_INJURED(vehs[mvf_late_fib_2].peds[0].id)
							OR NOT IS_PED_INJURED(vehs[mvf_late_fib_2].peds[1].id)
							OR NOT IS_PED_INJURED(vehs[mvf_late_fib_2].peds[2].id)
							
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_driver].id, SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)
									SET_PED_COMBAT_ATTRIBUTES(peds[mpf_driver].id, CA_LEAVE_VEHICLES, FALSE)
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(peds[mpf_driver].id, 100)
								ENDIF
								
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
			ENDIF
		
		BREAK
	ENDSWITCH
	
ENDPROC

PROC ST_11_Escape_To_Franklins()

	TEXT_LABEL_15 str_DontLeaveBuddyLabel
	IF BUDDY_PED_ID() = MIKE_PED_ID()
		str_DontLeaveBuddyLabel = "CMN_MLEAVE"
	ELSE
		str_DontLeaveBuddyLabel = "CMN_FLEAVE"
	ENDIF
	IF NOT IS_PED_INJURED(BUDDY_PED_ID())
		MANAGE_AI_BUDDY_WEAPON()
	ENDIF

	SWITCH mission_substage
		CASE STAGE_ENTRY
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(10, "11. Escape to Franklin's", TRUE)
			INCREMENT_ALL_CREW_MEMBER_STATS_DURING_HEIST(HEIST_AGENCY, TRUE)
			
			SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
			
			CLEANUP_RAPPEL(s_rappel_mike2.sRappelData)
			CLEANUP_RAPPEL(s_rappel_frank2.sRappelData)
			CLEANUP_RAPPEL(s_rappel_goon2.sRappelData)
			
			Unload_Asset_Model(sAssetData, mod_fib_agent_1)
			Unload_Asset_Model(sAssetData, mod_fib_agent_2)
			Unload_Asset_Model(sAssetData, mod_fib_agent_3)
			
			SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE, TRUE)
			
			IF i_NavMeshBlockingAreas[4] = -1
				i_NavMeshBlockingAreas[4] = ADD_NAVMESH_BLOCKING_OBJECT(<<15.650, 542.705,176.25>>, <<2.075,2.0,2.6>>, 23.0, FALSE)
			ENDIF
			
			// block the roads around franklins house.
			SET_ROADS_IN_AREA(<<4.020949,550.573975,170.556335>> - <<153.125000,32.000000,43.187500>>, <<4.020949,550.573975,170.556335>> + <<153.125000,32.000000,43.187500>>, FALSE)
			SET_ROADS_IN_ANGLED_AREA(<<291.467987,735.869751,133.466675>>, <<197.176666,723.254272,201.064758>>, 59.812500, FALSE, FALSE)
			SET_ROADS_IN_ANGLED_AREA(<<234.179382,713.081238,180.183334>>, <<168.204880,547.548340,200.742401>>, 59.812500, FALSE, FALSE)
			sbi_franklins = ADD_SCENARIO_BLOCKING_AREA(<<83.879822,647.615906,179.093460>>-<<216.937500,155.250000,43.187500>>, <<83.879822,647.615906,179.093460>>+<<216.937500,155.250000,43.187500>>)
			
			Add_Peds_For_Dialogue()
			
			SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_MICHAEL, 	SAVEHOUSE_FRANKLIN_VH, TRUE)
			SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_FRANKLIN, 	SAVEHOUSE_FRANKLIN_VH, TRUE)
			
			bReachedSafeDistanceFromCrimeScene = FALSE
			iCrewAIStage 			= 0
			i_dialogue_stage 		= 0
			i_dialogue_substage 	= 0
			bPrintLoseCops			= FALSE
			bPickupBuddys 			= FALSE
			mission_substage++
		BREAK
		CASE 1
			MANAGE_BUDDIES_ATTACKING_BASED_OFF_WANTED()
			
			SAFE_REMOVE_BLIP(peds[BUDDY_MPF()].objective_blip)
			SAFE_REMOVE_BLIP(peds[mpf_goon].objective_blip)
			SAFE_REMOVE_BLIP(peds[mpf_driver].objective_blip)
			SAFE_REMOVE_BLIP(vehs[mvf_escape].blip)
			SAFE_REMOVE_BLIP(blip_Objective)
		
			IS_PLAYER_AT_ANGLED_AREA_WITH_BUDDIES_IN_VEHICLE(locates_Data, vEndAtFranklinsCoord, 
					<<6.664714,543.694641,173.973328>>, <<19.389061,550.205933,177.437775>>, 6.250000, TRUE, 
					peds[mpf_driver].id, BUDDY_PED_ID(), peds[mpf_goon].id, vehs[mvf_escape].id,
					"A3B_ESCFIN", "A3B_CREWLB", str_DontLeaveBuddyLabel, "A3B_CREWLB", "", "", "",
					FALSE, TRUE, TRUE)
					
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehs[mvf_escape].id)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(), FALSE)
			ENDIF
		
			IF DOES_ENTITY_EXIST(BUDDY_PED_ID()) AND IS_PED_IN_VEHICLE(BUDDY_PED_ID(), vehs[mvf_escape].id)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(BUDDY_PED_ID(), FALSE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(peds[mpf_goon].id) AND IS_PED_IN_VEHICLE(peds[mpf_goon].id, vehs[mvf_escape].id)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_goon].id, FALSE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(peds[mpf_driver].id) AND IS_PED_IN_VEHICLE(peds[mpf_driver].id, vehs[mvf_escape].id)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_driver].id, FALSE)
			ENDIF

			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehs[mvf_escape].id)
			AND DOES_ENTITY_EXIST(BUDDY_PED_ID()) AND IS_PED_IN_VEHICLE(BUDDY_PED_ID(), vehs[mvf_escape].id)
			AND DOES_ENTITY_EXIST(peds[mpf_goon].id) AND IS_PED_IN_VEHICLE(peds[mpf_goon].id, vehs[mvf_escape].id)
			AND DOES_ENTITY_EXIST(peds[mpf_driver].id) AND IS_PED_IN_VEHICLE(peds[mpf_driver].id, vehs[mvf_escape].id)
				CLEAR_MISSION_LOCATE_STUFF(locates_Data)
				SAFE_BLIP_COORD(blip_Objective, vEndAtFranklinsCoord, TRUE)
				
				SET_GROUP_SEPARATION_RANGE(PLAYER_GROUP_ID(), 50.0)
				SET_PED_AS_GROUP_MEMBER(BUDDY_PED_ID(), PLAYER_GROUP_ID())
				SET_PED_AS_GROUP_MEMBER(peds[mpf_goon].id, PLAYER_GROUP_ID())
				SET_PED_AS_GROUP_MEMBER(peds[mpf_driver].id, PLAYER_GROUP_ID())
				
				SET_PED_CONFIG_FLAG(BUDDY_PED_ID(), PCF_RunFromFiresAndExplosions, FALSE)
				SET_PED_CONFIG_FLAG(peds[mpf_goon].id, PCF_RunFromFiresAndExplosions, FALSE)
				SET_PED_CONFIG_FLAG(peds[mpf_driver].id, PCF_RunFromFiresAndExplosions, FALSE)
				
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				AND GET_DRIVER_SKILL_LEVEL() = CMSK_BAD
					PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_AH_3B_01", 0.0)
				ENDIF
				
				mission_substage++	
			ENDIF
		BREAK
		CASE 2
			MANAGE_BUDDIES_ATTACKING_BASED_OFF_WANTED()
			
			// Update grouping
			IF DOES_ENTITY_EXIST(BUDDY_PED_ID())
				IF GET_PED_GROUP_INDEX(BUDDY_PED_ID()) != PLAYER_GROUP_ID()
					SET_PED_AS_GROUP_MEMBER(BUDDY_PED_ID(), PLAYER_GROUP_ID())
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
				IF GET_PED_GROUP_INDEX(peds[mpf_goon].id) != PLAYER_GROUP_ID()
					SET_PED_AS_GROUP_MEMBER(peds[mpf_goon].id, PLAYER_GROUP_ID())
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(peds[mpf_driver].id)
				IF GET_PED_GROUP_INDEX(peds[mpf_driver].id) != PLAYER_GROUP_ID()
					SET_PED_AS_GROUP_MEMBER(peds[mpf_driver].id, PLAYER_GROUP_ID())
				ENDIF
			ENDIF

			// Left buddy at least 1 buddy behind
			IF ( DOES_ENTITY_EXIST(BUDDY_PED_ID()) AND NOT IS_PED_IN_GROUP(BUDDY_PED_ID()) )
			OR ( DOES_ENTITY_EXIST(peds[mpf_goon].id) AND NOT IS_PED_IN_GROUP(peds[mpf_goon].id) )
			OR ( DOES_ENTITY_EXIST(peds[mpf_driver].id) AND NOT IS_PED_IN_GROUP(peds[mpf_driver].id) )
			
				IF NOT IS_PED_IN_GROUP(BUDDY_PED_ID())
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), BUDDY_PED_ID()) < 20.0
						SET_PED_AS_GROUP_MEMBER(BUDDY_PED_ID(), PLAYER_GROUP_ID())
						SAFE_REMOVE_BLIP(peds[BUDDY_MPF()].objective_blip)
					ELSE
						SAFE_BLIP_PED(peds[BUDDY_MPF()].objective_blip, peds[BUDDY_MPF()].id)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_IN_GROUP(peds[mpf_goon].id)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), peds[mpf_goon].id) < 20.0
						SET_PED_AS_GROUP_MEMBER(peds[mpf_goon].id, PLAYER_GROUP_ID())
						SAFE_REMOVE_BLIP(peds[mpf_goon].objective_blip)
					ELSE
						SAFE_BLIP_PED(peds[mpf_goon].objective_blip, peds[mpf_goon].id)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_IN_GROUP(peds[mpf_driver].id)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), peds[mpf_driver].id) < 20.0
						SET_PED_AS_GROUP_MEMBER(peds[mpf_driver].id, PLAYER_GROUP_ID())
						SAFE_REMOVE_BLIP(peds[mpf_driver].objective_blip)
					ELSE
						SAFE_BLIP_PED(peds[mpf_driver].objective_blip, peds[mpf_driver].id)
					ENDIF
				ENDIF
				
				IF NOT bPickupBuddys
					PRINT_NOW("A3B_CREWLB", DEFAULT_GOD_TEXT_TIME, 1)
					bPickupBuddys = TRUE
				ENDIF
				SAFE_REMOVE_BLIP(blip_Objective)

			// Has a wanted level
			ELIF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			
				bPickupBuddys = TRUE
				IF NOT bPrintLoseCops
					IF IS_SAFE_TO_DISPLAY_GODTEXT()
					AND i_dialogue_stage >= 3
						PRINT_NOW("LOSE_WANTED", DEFAULT_GOD_TEXT_TIME, 1)
						bPrintLoseCops = TRUE
					ENDIF
				ENDIF
				SAFE_REMOVE_BLIP(blip_Objective)
				
				SAFE_BLIP_PED(peds[BUDDY_MPF()].objective_blip, peds[BUDDY_MPF()].id)
				SAFE_BLIP_PED(peds[mpf_goon].objective_blip, peds[mpf_goon].id)
				SAFE_BLIP_PED(peds[mpf_driver].objective_blip, peds[mpf_driver].id)
				
			ELSE
			
				IF bPrintLoseCops
				OR bPickupBuddys
					bPrintLoseCops 	= FALSE
					bPickupBuddys	= FALSE
					IF IS_MESSAGE_BEING_DISPLAYED()
						CLEAR_PRINTS()
					ENDIF
				ENDIF
				
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				OR ( DOES_ENTITY_EXIST(BUDDY_PED_ID()) AND NOT IS_PED_IN_VEHICLE(BUDDY_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) )
					SAFE_BLIP_PED(peds[BUDDY_MPF()].objective_blip, peds[BUDDY_MPF()].id)
				ELSE
					SAFE_REMOVE_BLIP(peds[BUDDY_MPF()].objective_blip)
				ENDIF
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				OR ( DOES_ENTITY_EXIST(peds[mpf_goon].id) AND NOT IS_PED_IN_VEHICLE(peds[mpf_goon].id, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) )
					SAFE_BLIP_PED(peds[mpf_goon].objective_blip, peds[mpf_goon].id)
				ELSE
					SAFE_REMOVE_BLIP(peds[mpf_goon].objective_blip)
				ENDIF
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				OR ( DOES_ENTITY_EXIST(peds[mpf_driver].id) AND NOT IS_PED_IN_VEHICLE(peds[mpf_driver].id, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) )
					SAFE_BLIP_PED(peds[mpf_driver].objective_blip, peds[mpf_driver].id)
				ELSE
					SAFE_REMOVE_BLIP(peds[mpf_driver].objective_blip)
				ENDIF
				
				IF NOT IS_CUTSCENE_ACTIVE()
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( PLAYER_PED_ID(), vEndAtFranklinsCoord ) < 100
					
						REQUEST_CUTSCENE("AH_3B_EXT")
						Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, MIKE_PED_ID(), "Michael")
						Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, "Michael", PED_COMP_SPECIAL2, 0, 0)	// take off harness
						Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, "Michael", PED_COMP_SPECIAL, 0, 0)	// take off bala
						Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, "Michael", PED_COMP_HAIR,			// take off bala, restore hair
							g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iDrawableVariation[PED_COMP_HAIR], 
							g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iTextureVariation[PED_COMP_HAIR])
						Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, "Michael", PED_COMP_BERD,			// take off bala, restore beard
							g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iDrawableVariation[PED_COMP_BERD], 
							g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iTextureVariation[PED_COMP_BERD])
						
						Register_Ped_Variation_For_Cutscene_From_Ped(sCutscenePedVariationRegister, FRANK_PED_ID(), "Franklin")
						Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, "Franklin", PED_COMP_SPECIAL2, 0, 0)	// take off harness
						Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, "Franklin", PED_COMP_HAIR,			// take off bala, restore hair
							g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_FRANKLIN].iDrawableVariation[PED_COMP_HAIR], 
							g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_FRANKLIN].iTextureVariation[PED_COMP_HAIR])
						Register_Ped_Variation_For_Cutscene(sCutscenePedVariationRegister, "Franklin", PED_COMP_BERD,			// take off bala, restore hair
							g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_FRANKLIN].iDrawableVariation[PED_COMP_BERD], 
							g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_FRANKLIN].iTextureVariation[PED_COMP_BERD])
					
					ENDIF
					
				ELIF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( PLAYER_PED_ID(), vEndAtFranklinsCoord ) > 120
				
					REMOVE_CUTSCENE()
					
				ENDIF
			
				// Has arrived at the end locate
				IF IS_CUTSCENE_ACTIVE()
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<5.699973,544.465515,173.675751>>, <<16.315777,550.831604,178.259506>>, 12.687500)
					
					Load_Asset_Interior(sAssetData, interior_franklins)

					CLEAR_MISSION_LOCATE_STUFF(locates_Data)
					
					SET_PED_USING_ACTION_MODE(MIKE_PED_ID(), 		FALSE)
					SET_PED_USING_ACTION_MODE(FRANK_PED_ID(), 		FALSE)
					IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
						SET_PED_USING_ACTION_MODE(peds[mpf_goon].id,	FALSE)
					ENDIF
					
					IF DOES_ENTITY_EXIST(BUDDY_PED_ID())
						REMOVE_PED_FROM_GROUP(BUDDY_PED_ID())
					ENDIF
					IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
						REMOVE_PED_FROM_GROUP(peds[mpf_goon].id)
					ENDIF
					IF DOES_ENTITY_EXIST(peds[mpf_driver].id)
						REMOVE_PED_FROM_GROUP(peds[mpf_driver].id)
					ENDIF
					
					STOP_SCRIPTED_CONVERSATION(TRUE)
					
					Event_Finish(mef_manage_switch)
					
					mission_substage++
				
				ELSE
				
					IF NOT DOES_BLIP_EXIST(blip_Objective)
						SAFE_BLIP_COORD(blip_Objective, vEndAtFranklinsCoord, TRUE)
					ENDIF
					IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vEndAtFranklinsCoord, <<0.1, 0.1, LOCATE_SIZE_HEIGHT>>, TRUE)
					
				ENDIF

			ENDIF

			IF NOT bReachedSafeDistanceFromCrimeScene
				// player has reached safe distance
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vStreetPickupLocation) > 300
					bReachedSafeDistanceFromCrimeScene = TRUE
				ENDIF
				
				// wanted level gained before safe distance was reached.
				IF NOT bScorePlaying
					IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
						TRIGGER_MUSIC_EVENT("AH3B_COPS")
						bScorePlaying = TRUE
					ENDIF
				ENDIF	
			ENDIF

			IF bReachedSafeDistanceFromCrimeScene
			AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)

				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) != AMBULANCE
					IF bScorePlaying
						TRIGGER_MUSIC_EVENT("AH3B_LOST_COPS_VEH")
					ENDIF
				ELSE
					TRIGGER_MUSIC_EVENT("AH3B_LOST_COPS")
				ENDIF
				DISTANT_COP_CAR_SIRENS(FALSE)
				
				bScorePlaying = FALSE
			
			ENDIF
			
			// clean up emergency vehicles
			IF DOES_ENTITY_EXIST(vehs[mvf_police_car_1].id)
			OR DOES_ENTITY_EXIST(vehs[mvf_police_car_2].id)
			OR DOES_ENTITY_EXIST(vehs[mvf_firetruck_1].id)
			OR DOES_ENTITY_EXIST(vehs[mvf_firetruck_2].id)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vStreetPickupLocation) > 400.0
					IF DOES_ENTITY_EXIST(vehs[mvf_police_car_1].id)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_police_car_1].id)
					ENDIF
					IF DOES_ENTITY_EXIST(vehs[mvf_police_car_2].id)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_police_car_2].id)
					ENDIF
					IF DOES_ENTITY_EXIST(vehs[mvf_firetruck_1].id)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_firetruck_1].id)
					ENDIF
					IF DOES_ENTITY_EXIST(vehs[mvf_firetruck_2].id)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_firetruck_2].id)
					ENDIF
				ENDIF
			ENDIF
		
		// CREW AI
		//-------------------------------------------------------------------------------
		
			IF DOES_ENTITY_EXIST(BUDDY_PED_ID())
			AND NOT IS_PED_INJURED(BUDDY_PED_ID())

				IF IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE)
					CLEAR_PED_TASKS(BUDDY_PED_ID())
				ENDIF				
				
			ENDIF

		BREAK
		CASE 3
			IF IS_PED_IN_ANY_VEHICLE( PLAYER_PED_ID() )
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), DEFAULT_VEH_STOPPING_DISTANCE, 1, 0.1)
					mission_substage++
				ENDIF
			ELSE
				mission_substage++
			ENDIF
			
			IF mission_substage > 3
			
				// Set crew state
				CrewMemberStatus cmStatus

				// HACKER : FINE
				SET_HEIST_END_SCREEN_CREW_STATUS(HEIST_AGENCY, crewHacker, CMST_FINE)
				
				// GUNMAN: ?
				IF GET_ENTITY_HEALTH(peds[mpf_goon].id) < 200.0
					cmStatus = CMST_INJURED
				ELSE
					cmStatus = CMST_FINE
				ENDIF
				SET_HEIST_END_SCREEN_CREW_STATUS(HEIST_AGENCY, crewGoon, cmStatus)
				
				// DRIVER: ?
				IF GET_ENTITY_HEALTH(peds[mpf_driver].id) < 200.0
					cmStatus = CMST_INJURED
				ELSE
					cmStatus = CMST_FINE
				ENDIF
				SET_HEIST_END_SCREEN_CREW_STATUS(HEIST_AGENCY, crewDriver, cmStatus)

				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				
				IF DOES_ENTITY_EXIST(vehs[mvf_escape].id)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_escape].id, FALSE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(GET_PLAYERS_LAST_VEHICLE(), FALSE)
				ENDIF
				
				// bug 2051588, update the vehicle ID so that the AI will do all the stuff they need to drive it off after the player gets out
				IF GET_PLAYERS_LAST_VEHICLE() != vehs[mvf_escape].id
					SET_VEHICLE_AS_NO_LONGER_NEEDED( vehs[mvf_escape].id )
					vehs[mvf_escape].id = GET_PLAYERS_LAST_VEHICLE()
					SET_ENTITY_AS_MISSION_ENTITY( vehs[mvf_escape].id, TRUE, TRUE )
				ENDIF
				
				CLEAR_MISSION_LOCATE_STUFF(locates_Data)
				
				SAFE_BLIP_COORD(blip_Objective, <<8.5330, 540.1049, 175.0272>>)
				
				bRetaskBuddy 		= TRUE
				//i_dialogue_stage	= 0
				iCrewAIStage		= 1
				
				bFailAbandonGunman 	= FALSE
				bFailAbandonDriver	= FALSE
				bFailAbandonPilot	= FALSE
				bFailChecksOnVan	= FALSE
				
			ENDIF
		BREAK
		CASE 4
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<8.5330, 540.1049, 175.0272>>, <<LOCATE_SIZE_ON_FOOT_ONLY, LOCATE_SIZE_ON_FOOT_ONLY, LOCATE_SIZE_HEIGHT>>, TRUE)
				SAFE_BLIP_COORD(blip_Objective, <<5.6705, 532.1223, 174.3446>>)
				mission_substage++
			ELSE
			
			// BUDDY AI
			//--------------------------------
				IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(BUDDY_PED_ID()), <<5.6705, 532.1223, 174.3446>>, 1.0)
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
					OR bRetaskBuddy
						SAFE_OPEN_SEQUENCE()
							IF IS_PED_IN_ANY_VEHICLE(BUDDY_PED_ID())
								TASK_LEAVE_ANY_VEHICLE(null)
							ENDIF
							TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<5.6705, 532.1223, 174.3446>>, PEDMOVE_WALK)
						SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
						bRetaskBuddy = FALSE
					ENDIF
				ELSE
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
					OR bRetaskBuddy
						TASK_TURN_PED_TO_FACE_ENTITY(BUDDY_PED_ID(), PLAYER_PED_ID(), -1)
						TASK_LOOK_AT_ENTITY(BUDDY_PED_ID(), PLAYER_PED_ID(), -1, SLF_DEFAULT, SLF_LOOKAT_HIGH)
						bRetaskBuddy = FALSE
					ENDIF
				ENDIF

			ENDIF
		BREAK
		CASE 5
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<5.6705, 532.1223, 174.3446>>, <<LOCATE_SIZE_ON_FOOT_ONLY, LOCATE_SIZE_ON_FOOT_ONLY, LOCATE_SIZE_HEIGHT>>, FALSE)
				SAFE_REMOVE_BLIP(blip_Objective)
				Mission_Set_Stage(msf_12_mission_passed)
			ELSE
				IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(BUDDY_PED_ID()), <<5.6705, 532.1223, 174.3446>>, 1.0)
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_PERFORM_SEQUENCE)
					OR bRetaskBuddy
						SAFE_OPEN_SEQUENCE()
							IF IS_PED_IN_ANY_VEHICLE(BUDDY_PED_ID())
								TASK_LEAVE_ANY_VEHICLE(null)
							ENDIF
							TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<5.6705, 532.1223, 174.3446>>, PEDMOVE_WALK)
						SAFE_PERFORM_SEQUENCE(BUDDY_PED_ID())
						bRetaskBuddy = TRUE
					ENDIF
				ELSE
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(BUDDY_PED_ID(), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
					OR bRetaskBuddy
						TASK_TURN_PED_TO_FACE_ENTITY(BUDDY_PED_ID(), PLAYER_PED_ID(), -1)
						TASK_LOOK_AT_ENTITY(BUDDY_PED_ID(), PLAYER_PED_ID(), -1, SLF_DEFAULT, SLF_LOOKAT_HIGH)
						bRetaskBuddy = FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	SWITCH iCrewAIStage
		CASE 1
		
			// Does the escape vehicle exist
			IF DOES_ENTITY_EXIST(vehs[mvf_escape].id)
			AND IS_VEHICLE_DRIVEABLE(vehs[mvf_escape].id)
				
				// Is the escape vehicle the player's last vehicle
				IF GET_PLAYERS_LAST_VEHICLE() = vehs[mvf_escape].id
				AND GET_DISTANCE_BETWEEN_ENTITIES(peds[mpf_driver].id, vehs[mvf_escape].id) < 100.0
					
				// DRIVER AI
				//----------------------------
					IF NOT IS_PED_IN_VEHICLE(peds[mpf_driver].id, vehs[mvf_escape].id)
					
						// Is the driver in the vehicle, get him back in there
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_driver].id, SCRIPT_TASK_ENTER_VEHICLE)
							
							IF IS_VEHICLE_SEAT_FREE(vehs[mvf_escape].id, VS_DRIVER)
								TASK_ENTER_VEHICLE(peds[mpf_driver].id, vehs[mvf_escape].id, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_RUN)
							ELSE
								TASK_ENTER_VEHICLE(peds[mpf_driver].id, vehs[mvf_escape].id, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_RUN)
							ENDIF
							
						ENDIF
						
					ELSE
					
						// Make sure driver is in the right seat
						IF GET_SEAT_PED_IS_IN(peds[mpf_driver].id) != VS_DRIVER
						
							IF IS_VEHICLE_SEAT_FREE(vehs[mvf_escape].id, VS_DRIVER)
						
								IF GET_SEAT_PED_IS_IN(peds[mpf_driver].id) = VS_FRONT_RIGHT
																	
									IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_driver].id, SCRIPT_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
										TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(peds[mpf_driver].id, vehs[mvf_escape].id)
									ENDIF

								ELSE
								
									IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_driver].id, SCRIPT_TASK_PERFORM_SEQUENCE)
										SAFE_OPEN_SEQUENCE()
											TASK_LEAVE_ANY_VEHICLE(null)
											TASK_ENTER_VEHICLE(null, vehs[mvf_escape].id, DEFAULT, VS_DRIVER)
										SAFE_PERFORM_SEQUENCE(peds[mpf_driver].id)
									ENDIF
								
								ENDIF
								
							ENDIF
						
						// Wait on the gunman to get in and then drive off
						ELIF IS_PED_IN_VEHICLE(peds[mpf_goon].id, vehs[mvf_escape].id, FALSE)
						AND GET_DISTANCE_BETWEEN_ENTITIES( vehs[mvf_escape].id, BUDDY_PED_ID() ) > 8.0
						AND i_dialogue_stage > 15
						
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_driver].id, SCRIPT_TASK_VEHICLE_MISSION)
								TASK_VEHICLE_MISSION_COORS_TARGET(peds[mpf_driver].id, vehs[mvf_escape].id, vEndAtFranklinsCoord, MISSION_FLEE, 15.0, DRIVINGMODE_STOPFORCARS_STRICT | DF_SteerAroundPeds, 1000.0, 10.0, FALSE)
							ENDIF
						
						ENDIF
					
					ENDIF
					
				// GUNMAN AI
				//----------------------------
					IF NOT IS_PED_IN_VEHICLE(peds[mpf_goon].id, vehs[mvf_escape].id)
						
						// Is the gunman in the vehicle, get him back in there
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_goon].id, SCRIPT_TASK_ENTER_VEHICLE)
							TASK_ENTER_VEHICLE(peds[mpf_goon].id, vehs[mvf_escape].id, DEFAULT_TIME_BEFORE_WARP, VS_ANY_PASSENGER, PEDMOVE_RUN)
						ENDIF
						
					ENDIF
				
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
// DIALOGUE
//-------------------------------------------------------------------------------

	IF mission_substage > 1

		// Nearly at franklins, kill the conversation
		IF i_dialogue_stage < 13
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), vEndAtFranklinsCoord) < 70
			OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<128.991684,572.449951,187.785645>>, <<-96.151588,537.560974,148.521515>>, 54.937500)
				IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					KILL_FACE_TO_FACE_CONVERSATION()
				ENDIF
				i_dialogue_stage = 13
			ENDIF
		ENDIF
		
		
		IF GET_DRIVER_SKILL_LEVEL() = CMSK_GOOD

			SWITCH i_dialogue_stage
				// Driver says play it cool
				CASE 0
					IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					
						str_dialogue_root = ""
						i_dialogue_substage = -1	// signifies that the "play it cool" was never played
						i_dialogue_stage++
					
					ELIF bSwatSeenAmbulance
					OR bSwatHaveArrived
					OR (DOES_ENTITY_EXIST(vehs[mvf_late_fib_1].id) AND IS_VEHICLE_DRIVEABLE(vehs[mvf_late_fib_1].id) AND IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_late_fib_1].id, <<25.299328,-787.341797,41.940556>>, <<108.733849,-560.708984,48.660660>>, 42.750000))
					OR (DOES_ENTITY_EXIST(vehs[mvf_late_fib_1].id) AND IS_VEHICLE_DRIVEABLE(vehs[mvf_late_fib_1].id) AND IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_late_fib_2].id, <<25.299328,-787.341797,41.940556>>, <<108.733849,-560.708984,48.660660>>, 42.750000))
						IF IS_SAFE_TO_START_CONVERSATION()
							str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewDriver, "AH_PLAY")
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_substage = 0
								i_dialogue_stage++
							ENDIF		
						ENDIF
					ENDIF
				BREAK
				CASE 1
					// Get away safely without alerting the cops
					IF bReachedSafeDistanceFromCrimeScene 
					AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					AND NOT bScorePlaying
					
						str_dialogue_root = ""
						i_dialogue_stage = 4
												
					// Swat get alerted
					ELIF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				
						IF IS_SAFE_TO_START_CONVERSATION()
							GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_ONTOT", FALSE, str_dialogue_root, FALSE, TRUE, TRUE, FALSE)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF		
						ENDIF
					
					// Gunman says they might be getting away with it
					ELSE
					
						SWITCH i_dialogue_substage
							CASE 0
								IF IS_SAFE_TO_START_CONVERSATION()
								AND (crewGoon != CM_GUNMAN_G_GUSTAV OR (bSwatHaveArrived AND GET_GAME_TIMER() - i_time_of_last_convo > 2000))
									str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewGoon, "AH_COOL")
									IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
										str_dialogue_root = ""
										i_dialogue_substage++
									ENDIF		
								ENDIF
							BREAK
						ENDSWITCH					
					
					ENDIF
				
				BREAK		
				// Driver moans about ruining the stealth escape
				CASE 2
					IF i_dialogue_substage = -1
						str_dialogue_root = ""
						i_dialogue_stage++
					ELSE
						IF IS_SAFE_TO_START_CONVERSATION()
							str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewDriver, "AH_WANT")
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
								i_dialogue_stage++
							ENDIF		
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
		// bad drive jump straight to shared get away lines
		ELSE
			IF i_dialogue_stage < 3
				i_dialogue_stage = 3
			ENDIF
		ENDIF
			
		// Shared dialogue
		SWITCH i_dialogue_stage
			CASE 3
				// Get away safely from the cops
				IF bReachedSafeDistanceFromCrimeScene 
				AND NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				AND NOT bScorePlaying

					str_dialogue_root = ""
					i_dialogue_stage++
					
				// Lines to play while escaping the wanted level
				ELSE
					IF NOT ARE_PLAYER_STARS_GREYED_OUT(PLAYER_ID())
					AND IS_SAFE_TO_START_CONVERSATION()
					AND GET_GAME_TIMER() - i_time_of_last_convo > 15000
						
						IF IS_STRING_NULL_OR_EMPTY(str_dialogue_root)
							SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
								CASE 0
									str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewDriver, "AH_LOSE")
								BREAK
								CASE 1
									str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewGoon, "AH_LOSE")
								BREAK
								CASE 2
									GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_SHOOT2", FALSE, str_dialogue_root, TRUE)
								BREAK
							ENDSWITCH
						ENDIF
						
						IF NOT IS_STRING_NULL_OR_EMPTY(str_dialogue_root)
							IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
								str_dialogue_root = ""
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 4
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_CLEAR", CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
						i_dialogue_stage++
					ENDIF		
				ENDIF
			BREAK
			CASE 5
				IF GET_DRIVER_SKILL_LEVEL() = CMSK_GOOD
					IF IS_SAFE_TO_START_CONVERSATION()
					AND GET_GAME_TIMER() - i_time_of_last_convo > 2000
						str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewDriver, "AH_WHERE")
						IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
							i_dialogue_stage++
						ENDIF		
					ENDIF
				ELSE
					IF IS_SAFE_TO_START_CONVERSATION()
					AND GET_GAME_TIMER() - i_time_of_last_convo > 2000
						IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_MOAN", CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
							i_dialogue_stage++
						ENDIF		
					ENDIF
				ENDIF
			BREAK
			CASE 6
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_TALK", CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
						i_dialogue_stage++
					ENDIF		
				ENDIF
			BREAK
			CASE 7
				IF IS_SAFE_TO_START_CONVERSATION()
					str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewDriver, "AH_PAID")
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
						i_dialogue_stage++
					ENDIF		
				ENDIF
			BREAK
			CASE 8
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_TALK2", CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
						i_dialogue_stage++
					ENDIF		
				ENDIF
			BREAK
			CASE 9
				IF crewGoon = CM_GUNMAN_G_GUSTAV
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_SEG", CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
							i_dialogue_stage++
						ENDIF		
					ENDIF
				ELSE
					i_dialogue_stage++
				ENDIF
			BREAK
			CASE 10
				IF IS_SAFE_TO_START_CONVERSATION()
					str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewGoon, "AH_PAID")
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
						i_dialogue_stage++
					ENDIF		
				ENDIF
			BREAK
			CASE 11
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION(sConvo, strDialogue, "AH_TALK3", CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
						i_dialogue_stage++
					ENDIF		
				ENDIF
			BREAK
			CASE 12
				// BLANK CASE
			BREAK
			CASE 13
				IF IS_SAFE_TO_START_CONVERSATION()
					str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewDriver, "AH_BYEM")
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						REPLAY_RECORD_BACK_FOR_TIME(8.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						str_dialogue_root = ""
						i_dialogue_stage++
					ENDIF		
				ENDIF
			BREAK
			CASE 14
				IF IS_SAFE_TO_START_CONVERSATION()
					str_dialogue_root = APPEND_CREW_ID_TO_CONV_ROOT(crewDriver, "AH_BYE")
					IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
						str_dialogue_root = ""
						i_dialogue_stage++
					ENDIF		
				ENDIF
			BREAK
			CASE 15
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					i_dialogue_stage++
				ENDIF
			BREAK
			CASE 16
				IF GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()) = interior_franklins
				OR GET_INTERIOR_FROM_ENTITY(BUDDY_PED_ID()) = interior_franklins
					IF IS_SAFE_TO_START_CONVERSATION()
						GET_CONVERSATION_ROOT_FOR_RANDOM_CREW_MEMBER("AH_HOUSE", FALSE, str_dialogue_root, TRUE)
						IF CREATE_CONVERSATION(sConvo, strDialogue, str_dialogue_root, CONV_PRIORITY_HIGH)
							str_dialogue_root = ""
							i_dialogue_stage++
						ENDIF		
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH

		
	ENDIF
	
ENDPROC

PROC ST_12_Mission_Passed()
	SWITCH mission_substage
		CASE STAGE_ENTRY
			IF HAS_CUTSCENE_LOADED()
			AND IS_INTERIOR_READY(interior_franklins)
			// 1978151
			//AND IS_SAFE_TO_START_CONVERSATION()

				IF BUDDY_PED_ID() != NULL
					IF BUDDY_PED_ID() = MIKE_PED_ID()
						REGISTER_ENTITY_FOR_CUTSCENE(BUDDY_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(BUDDY_PED_ID(), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("AH_3B_GET_TO_FRANKLINS")
					STOP_AUDIO_SCENE("AH_3B_GET_TO_FRANKLINS")
				ENDIF
				
				Unload_Asset_PTFX(sAssetData)
				IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxs[ptfx_sweat_shop])
					STOP_PARTICLE_FX_LOOPED(ptfxs[ptfx_sweat_shop])
				ENDIF
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				
				SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)
				
				bExitStateFrank 		= FALSE
				bExitStateMichael 		= FALSE
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				iCrewAIStage = -1
				START_CUTSCENE()
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				mission_substage++
			ENDIF
		BREAK
		CASE 1
			IF IS_CUTSCENE_PLAYING()
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
			
				IF DOES_ENTITY_EXIST(peds[mpf_goon].id)
					DELETE_PED(peds[mpf_goon].id)
				ENDIF
				
				IF DOES_ENTITY_EXIST(peds[mpf_driver].id)
					DELETE_PED(peds[mpf_driver].id)
				ENDIF
			
				IF DOES_ENTITY_EXIST(vehs[mvf_escape].id)
					DELETE_VEHICLE(vehs[mvf_escape].id)
				ENDIF
				
				IF DOES_CAM_EXIST(cams[mcf_generic])
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					DESTROY_CAM(cams[mcf_generic])
				ENDIF
				
				PRELOAD_STORED_PLAYER_PED_VARIATION(MIKE_PED_ID())
				
				mission_substage++
			ENDIF
		BREAk
		CASE 2
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				REPLAY_STOP_EVENT()
				CPRINTLN(DEBUG_MIKE, "AH_3B_EXT: Franklin Exited")
				bExitStateFrank = TRUE
			ENDIF
		
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			
				RESTORE_PLAYER_PED_VARIATIONS(MIKE_PED_ID())
			
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
					ENDIF
				ENDIF
				
				SET_ENTITY_COORDS(MIKE_PED_ID(), << 8.8201, 541.1979, 175.0280 >>)
				SET_ENTITY_HEADING(MIKE_PED_ID(), 332.6756)

				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				TASK_GO_STRAIGHT_TO_COORD(MIKE_PED_ID(), << 9.9220, 543.4659, 174.7219 >>, PEDMOVE_WALK)
				FORCE_PED_MOTION_STATE(MIKE_PED_ID(), MS_ON_FOOT_WALK)
				
				SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_MICHAEL, 	SAVEHOUSE_FRANKLIN_VH, FALSE)
				DOOR_SYSTEM_SET_OPEN_RATIO(enum_to_int(DOORHASH_F_HOUSE_VH_F), 0.0, FALSE, TRUE)
				DOOR_SYSTEM_SET_DOOR_STATE(enum_to_int(DOORHASH_F_HOUSE_VH_F), DOORSTATE_LOCKED, FALSE, TRUE)
				
				CPRINTLN(DEBUG_MIKE, "AH_3B_EXT: Michael Exited")
				bExitStateMichael = TRUE
			ENDIF
			
			IF bExitStateMichael
			AND (bExitStateFrank OR NOT DOES_ENTITY_EXIST(peds[mpf_frank].id))
				
				IF DOES_ENTITY_EXIST(peds[mpf_frank].id)
					DELETE_PED(peds[mpf_frank].id)
				ENDIF
				Unload_Asset_Interior(sAssetData, interior_franklins)
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				CPRINTLN(DEBUG_MIKE, "AH_3B_EXT: Michael Exited")
				Mission_Passed()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

// Handles the mission flow based on the mission_stage
PROC Mission_Flow()
	SWITCH int_to_enum(MISSION_STAGE_FLAG, mission_stage)
	
		CASE 	msf_0_intro_cut					ST_0_Intro_Cutscene()					BREAK
		CASE	msf_1_go_to_helipad				ST_1_Go_To_Helipad()					BREAK
		CASE	msf_2_fly_to_jump_zone			ST_2_Fly_To_Jump_Zone()					BREAK
		CASE	msf_3_sky_dive_and_land			ST_3_Skydive_And_Land()					BREAK
		CASE	msf_4_hack_the_computer			ST_4_Hack_The_Computer()				BREAK
		CASE	msf_5_download_shootout			ST_5_Download_Shootout()				BREAK
		CASE 	msf_6_stairwell 				ST_6_Stairwell()						BREAK
		CASE	msf_7_burnt_out_floor_1			ST_7_Burntout_Floor()					BREAK
		CASE	msf_8_burnt_out_floor_2			ST_8_Burntout_Floor_2()					BREAK
		CASE	msf_9_abseil_down				ST_9_Abseil_Down()						BREAK
		CASE	msf_10_get_to_getaway_veh		ST_10_Get_To_The_Getaway_Vehicle()		BREAK
		CASE 	msf_11_escape_to_franklins		ST_11_Escape_To_Franklins()				BREAK
		CASE 	msf_12_mission_passed			ST_12_Mission_Passed()					BREAK
		
	ENDSWITCH

	
	#IF IS_DEBUG_BUILD
		Mission_Debug()
	#ENDIF
ENDPROC


/*
									  ______  _______  ______   _  ______  _______    _       _______  _______  ______  
									 / _____)(_______)(_____ \ | |(_____ \(_______)  (_)     (_______)(_______)(_____ \ 
									( (____   _        _____) )| | _____) )   _       _       _     _  _     _  _____) )
									 \____ \ | |      |  __  / | ||  ____/   | |     | |     | |   | || |   | ||  ____/ 
									 _____) )| |_____ | |  \ \ | || |        | |     | |_____| |___| || |___| || |      
									(______/  \______)|_|   |_||_||_|        |_|     |_______)\_____/  \_____/ |_|                                                                                         
*/

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED()	
		Mission_Failed(mff_default)
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	// Do everything to prepare the mission
	Mission_Setup()
	
	// Main loop
	WHILE (TRUE)
	
		Process_Streaming(sAssetData)		// Deals with loading any assets and keeps track of what has been loaded
		Update_Cutscene_Prestreaming(sCutscenePedVariationRegister)	// deals with streaming ped variations
		
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			i_time_of_last_convo = -1
		ELSE
			IF i_time_of_last_convo = -1
				i_time_of_last_convo = GET_GAME_TIMER()
			ENDIF
		ENDIF
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_AH3B")
		
		Mission_Stage_Management()			// manage any stage switching stuff
		Mission_Stage_Skip()				// processes any stage skipping that is required
		Mission_Checks(bDoSkip)				// Check if any of the mission critical entities (peds, vehicles, objects) are dead and fail accordingly
		IF NOT bDoSkip
		#IF IS_DEBUG_BUILD
			AND NOT debug_b_PTFXDebugIsRunning
		#ENDIF
			Manage_Mission_Events()			// manage any events triggered
			DOORS_Manage_Door_System()		// manages the opening and closing of doors
		ENDIF
		Mission_Flow()						// process the mission flow
		Process_Fail()
	
		WAIT(0)
		
	ENDWHILE
	
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
