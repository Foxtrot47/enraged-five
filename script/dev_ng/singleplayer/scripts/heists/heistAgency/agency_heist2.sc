// killing architect
// Paul Davies - 29/01/2010

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "script_blips.sch"
USING "select_mission_stage.sch"
USING "player_ped_public.sch"
USING "dialogue_public.sch"
USING "chase_hint_cam.sch"
USING "commands_interiors.sch"
USING "replay_public.sch"
USING "selector_public.sch"
USING "CompletionPercentage_public.sch"
USING "taxi_functions.sch"
USING "net_fps_cam.sch"
USING "mission_stat_public.sch"
USING "flow_public_core.sch"
USING "player_scene_private.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
	using "shared_debug.sch"
	CONST_INT NUM_OF_STAGES 4
	INT iStageHolder
	MissionStageMenuTextStruct skipMenuOptions[NUM_OF_STAGES]
	WIDGET_GROUP_ID hat_widget
#ENDIF

//____________________________________ VARIABLES __________________________________________

//Blips
BLIP_INDEX blipArchitect
BLIP_INDEX blipSafeHouse
BLIP_INDEX blipPlans
BLIP_INDEX blipLift
BLIP_INDEX blipParkUp
BLIP_INDEX blipLifts[4]
BLIP_INDEX blipOffice

PED_INDEX pedArchitect

VEHICLE_INDEX vehPlayerCar
VEHICLE_INDEX vehArchitectCar
VEHICLE_INDEX vehHeli
VEHICLE_INDEX vehMike

OBJECT_INDEX objPlans
OBJECT_INDEX objChair
OBJECT_INDEX objBox
OBJECT_INDEX objHat
OBJECT_INDEX objBarrier

CAMERA_INDEX cutCam

SCENARIO_BLOCKING_INDEX sbiSite

SEQUENCE_INDEX seqTurnTask

INTERIOR_INSTANCE_INDEX interiorGarmentFactory

//SET_CAN_CLIMB_ON_ENTITY to FALSE for elevators

REL_GROUP_HASH rgConstruction
REL_GROUP_HASH rgBuddies

VECTOR vPlayerCarStart = <<-70.4456, -1015.1096, 28.2250>>
FLOAT fPlayerCarHeading = 162.098038

VECTOR vArchCarStart = << -109.9041, -1032.1838, 26.2752 >>
FLOAT fArchCarStart = 184.7313

VECTOR vTopFloorLiftApproach[4]
VECTOR vTopFloorLiftStandPos[4]

VECTOR vReplayCoords
FLOAT fReplayHeading

//areas
VECTOR vMuggingAreaPos1 = <<-151.785461,-996.356934,113.135284>>
VECTOR vMuggingAreaPos2 = <<-134.473282,-958.601074,118.384758>>
FLOAT fMuggingAreaWidth = 12.250000

VECTOR vPlayerCoords //cheaper to grab these once each frame
VECTOR vOffice = <<711.1870, -964.4080, 29.3953>>

BOOL bSkip = FALSE
BOOL bHeliOneGo = FALSE
BOOL bHeliOneDone = FALSE
BOOL bHelmetHiddenInVeh = FALSE
BOOL bArchCowering = FALSE
BOOL bMugged = FALSE
BOOL bTopFloorCower[2]
BOOL bPlayerhasCase = FALSE
BOOL bCaseClipsetApplied = FALSE
BOOL bElevatorMusicStarted
BOOL bArchIsSecure = FALSE
BOOL bLastLookRight
BOOL bLeavePrinted
BOOL bPreHatSHout
BOOL bStealthKillConv = FALSE
BOOL bCarStopped = FALSE
//BOOL bCamSwitched = FALSE
BOOL bWarningGiven = FALSE
BOOL bEndCutsceneRequested
BOOL bSecondLineDone = FALSE
BOOL bSiteCleanedUp = FALSE
BOOL bBigScream = FALSE
BOOL bBigSplat = FALSE
BOOL bMethodDone = FALSE
BOOL bThanks = FALSE

BOOL bTwoStarWantedLevelGiven = FALSE
BOOL bThreeStarWantedLevelGiven = FALSE
BOOL bPlayerHasLeftTopFloor = FALSE
BOOL bTwoStarWantedLevelAttained = FALSE
BOOL bThreeStarWantedLevelAttained = FALSE
INT iTimeLastWantedLevelApplied = 0

BOOL bArchCanSeePlayer = FALSE
INT iArchCanSeePlayerTimer
FLOAT fTimeArmed = 0.0
FLOAT fTotalLurkingTime = 0.0
FLOAT fLostPlansTime = 0.0

INT iCurrentWorkerLosCheck
INT iLastLosCheck
INT iLookAtPlansStage
INT iLastLookCheck
INT iLastLookAtTask
INT iLiftLookAt
INT iTimeCasePickup
INT iPostHatSlowdown = 0

INT iStageProgress = 0
INT iArchitectSightTimer = 0
INT iMugFailCheckTImer = 0
INT iTimeOfTouch = 0
INT iTimeOfLastArchChat = 0
INT iTimeOfLastChaseChat = 0
INT iArchCurrentPlanChat = 1
INT iLastWeaponForce = 0
INT iArchStage = 0
INT iSaveStage = -1
INT iTimeOnSite = -1
INT iTimeArchFirstFloor = -1
INT iChatStage
INT iHatThrowStage

STRING sArchConvLabel[5]
STRING sArchDeskAnims[5]

INT iWorkerScene
VECTOR vWorkerScenePosition = << -174.583, -1002.130, 113.130 >>
VECTOR vWorkerSceneRotation = << 0.000, 0.000, 163.080 >>

INT iArchScene
VECTOR vArchAtDeskScenePosition = << -135.997, -966.943, 113.135 >>
VECTOR vArchAtDeskSceneRotation = << 0.000, 0.000, -124.000 >>
VECTOR vArchSceneAnimStart

//INT iCowerStage
INT iLastCower
STRING sMuggingDict1 = "missfbi5ig_21"
STRING sMuggingDict2 = "missheist_agency2ahands_up"

STRING sHandsUpBase = "hand_up_scientist"
STRING sHandsUpAnxious = "handsup_anxious"
STRING sHandsUpReact2 = "hands_up_shocked_scientist"

#IF IS_DEBUG_BUILD
	BOOL bDebugInitialised
#ENDIF

STRING sBlockID = "FB3aAUD"

//____________________________________ FUNCTIONS __________________________________________

ENUM MISSION_STAGE_FLAG
	STAGE_GROUND_FLOOR,
	STAGE_FIRST_FLOOR,
	STAGE_ESCAPE,
	STAGE_DRIVE_BACK,
	STAGE_LOSE_COPS,
	STAGE_PLAYER_SPOTTED,
	STAGE_COVER_BLOWN,
	STAGE_TEST
ENDENUM

MISSION_STAGE_FLAG mission_stage = STAGE_GROUND_FLOOR
MISSION_STAGE_FLAG eSavedStage

PROC GO_TO_STAGE(MISSION_STAGE_FLAG stage)
	mission_stage = stage
	IF DOES_BLIP_EXIST(blipParkUp)
		REMOVE_BLIP(blipParkUp)
	ENDIF
	IF DOES_BLIP_EXIST(blipOffice)
		REMOVE_BLIP(blipOffice)
	ENDIF
	iStageProgress = 0
	SETTIMERA(0)
ENDPROC

ENUM SITE_STATE
	AH2_SS_ALL_QUIET = 0,
	AH2_SS_SPOTTED_PLAYER,
	AH2_SS_ATTACKING_PLAYER,
	AH2_SS_FLEEING_PLAYER
ENDENUM

SITE_STATE eSiteState = AH2_SS_ALL_QUIET

ENUM AMBIENT_STAGE
	AH2_AS_SETUP_GROUNDFLOOR1 = 0,
	AH2_AS_RUN_GROUNDFLOOR,
	AH2_AS_SETUP_TOPFLOOR,
	AH2_AS_RUN_TOP_FLOOR,
	AH2_AS_SETUP_GROUNDFLOOR2,
	AH2_AS_RUN_GROUNDFLOOR2
ENDENUM

AMBIENT_STAGE eAmbientSiteStage = AH2_AS_SETUP_GROUNDFLOOR1

ENUM AH2_LEADING_STAGE
	AH2_LS_WAIT_FOR_ACTIVATION = 0,
	AH2_LS_AWAIT_INTERIOR,
	AH2_LS_WAIT,
	AH2_UNLOAD
ENDENUM

AH2_LEADING_STAGE eLeadinStage

PROC RESET_ALL_VARIABLES()

	bHeliOneGo = FALSE
	bHeliOneDone = FALSE
	bHelmetHiddenInVeh = FALSE
	bArchCowering = FALSE
	bMugged = FALSE
	bTopFloorCower[0] = FALSE
	bTopFloorCower[1] = FALSE
	bPlayerhasCase = FALSE
	bElevatorMusicStarted = FALSE
	bArchIsSecure = FALSE
	bLastLookRight = FALSE
	bLeavePrinted = FALSE
	bPreHatSHout = FALSE
	bStealthKillConv = FALSE
	bCarStopped = FALSE
//	bCamSwitched = FALSE
	bWarningGiven = FALSE
//	bLiftStairsCollisionLoaded = FALSE
	bSiteCleanedUp = FALSE
	
	fTimeArmed = 0.0
	
	iCurrentWorkerLosCheck = 0
	iLastLosCheck = 0
	iLookAtPlansStage = 0
	iLastLookCheck = 0
	iLastLookAtTask = 0
	iLiftLookAt = 0
	fTotalLurkingTime = 0
	iStageProgress = 0
	iArchitectSightTimer = 0
	iMugFailCheckTImer = 0
	iTimeOfTouch = 0
	iTimeOfLastArchChat = 0
	iTimeOfLastChaseChat = 0
	iArchCurrentPlanChat = 1
	iLastWeaponForce = 0
	iArchStage = 0
	iSaveStage = -1
	iTimeOnSite = -1
	iTimeArchFirstFloor = -1
	iChatStage = 0
	iHatThrowStage = 0
	bSiteCleanedUp = FALSE
	bBigScream = FALSE
	bBigSplat = FALSE
	
	bTwoStarWantedLevelGiven = FALSE
	bThreeStarWantedLevelGiven = FALSE
	bPlayerHasLeftTopFloor = FALSE
	bTwoStarWantedLevelAttained = FALSE
	bThreeStarWantedLevelAttained = FALSE
	
ENDPROC

PROC RESET_ALL_GENERIC_VARIABLES()

	bHelmetHiddenInVeh = FALSE
	bArchCowering = FALSE
	bTopFloorCower[0] = FALSE
	bTopFloorCower[1] = FALSE
	bPlayerhasCase = FALSE
	bArchIsSecure = FALSE
	bLeavePrinted = FALSE
	bStealthKillConv = FALSE
	bWarningGiven = FALSE
	bSiteCleanedUp = FALSE
	
	fTimeArmed = 0.0
	
	iCurrentWorkerLosCheck = 0
	iLastLosCheck = 0
	fTotalLurkingTime = 0
	iArchitectSightTimer = 0
	iMugFailCheckTImer = 0
	iTimeOfTouch = 0
	iTimeOfLastArchChat = 0
	iTimeOfLastChaseChat = 0
	iArchCurrentPlanChat = 1
	iLastWeaponForce = 0
	iSaveStage = -1
	
ENDPROC

PROC RESET_ALL_TOP_FLOOR_VARIABLES()
	bHelmetHiddenInVeh = FALSE
	bMugged = FALSE
	bTopFloorCower[0] = FALSE
	bTopFloorCower[1] = FALSE
	bElevatorMusicStarted = FALSE
	bBigSplat = FALSE
	
	iLookAtPlansStage = 0
	iArchitectSightTimer = 0
	iMugFailCheckTImer = 0
	iTimeOfTouch = 0
	iTimeOfLastArchChat = 0
	iTimeOfLastChaseChat = 0
	iArchCurrentPlanChat = 1
	iArchStage = 0
ENDPROC

PROC RESET_ALL_GROUNDFLOOR_VARIABLES()
	bHeliOneGo = FALSE
	bHeliOneDone = FALSE
	bLastLookRight = FALSE
	bPreHatSHout = FALSE
	bCarStopped = FALSE
//	bCamSwitched = FALSE
//	bLiftStairsCollisionLoaded = FALSE
	
	iChatStage = 0
	iHatThrowStage = 0
	iLastLookCheck = 0
	iLastLookAtTask = 0
	iLiftLookAt = 0
	iTimeOnSite = -1
	iTimeArchFirstFloor = -1
ENDPROC

structPedsForConversation sSpeech

FUNC BOOL IS_PED_IN_GARMENT_FACTORY(PED_INDEX ped)
	IF DOES_ENTITY_EXIST(ped)
	AND NOT IS_PED_INJURED(ped)
		RETURN IS_ENTITY_IN_ANGLED_AREA(ped, <<696.425964,-961.024719,22.882015>>, <<739.275208,-961.294128,35.816986>>, 30.500000)
		AND IS_VALID_INTERIOR(GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID()))
	ENDIF
	RETURN FALSE
ENDFUNC

STRUCT TEXT_HOLDER
	STRING label
	BOOL played
ENDSTRUCT

TEXT_HOLDER gtPickup
TEXT_HOLDER gtPark
TEXT_HOLDER gtGetOut
TEXT_HOLDER htUseLift
TEXT_HOLDER htMug
TEXT_HOLDER dhBanter1
TEXT_HOLDER htUseCam

FUNC FLOAT MIN(FLOAT a, FLOAT b)
	IF a<b
		RETURN a
	ELSE
		RETURN b
	ENDIF
ENDFUNC

FUNC FLOAT MAX(FLOAT a, FLOAT b)
	IF a>b
		RETURN a
	ELSE
		RETURN b
	ENDIF
ENDFUNC

FUNC BOOL DO_LINES_INTERSECT2(VECTOR p1, VECTOR p2, VECTOR p3, VECTOR p4, VECTOR &vIntersectPoint)
	// Store the values for fast access and easy
	// equations-to-code conversion
	FLOAT x1 = p1.x
	FLOAT x2 = p2.x
	FLOAT x3 = p3.x
	FLOAT x4 = p4.x
	FLOAT y1 = p1.y
	FLOAT y2 = p2.y
	FLOAT y3 = p3.y
	FLOAT y4 = p4.y
	
	float d = (x1 - x2) * (y3 - y4) - (y1 - y2) * (x3 - x4)
	// If d is zero, there is no intersection
	IF d = 0 
		RETURN FALSE 
	ENDIF
	
	// Get the x and y
	FLOAT fPre = (x1*y2 - y1*x2)
	FLOAT fPost = (x3*y4 - y3*x4)
	float x = ( fPre * (x3 - x4) - (x1 - x2) * fPost ) / d
	float y = ( fPre * (y3 - y4) - (y1 - y2) * fPost ) / d
	 
	// Check if the x and y coordinates are within both lines
	IF x < min(x1, x2) 
	OR x > max(x1, x2) 
	OR x < min(x3, x4)
	OR x > max(x3, x4) 
		RETURN FALSE
	ENDIF
	
	IF y < min(y1, y2) 
	OR y > max(y1, y2)
	OR y < min(y3, y4) 
	OR y > max(y3, y4)
		RETURN FALSE
	ENDIF
	
	// Return the point of intersection
	vIntersectPoint.x = x
	vIntersectPoint.y = y
	vIntersectPoint.z = p1.z
	return TRUE
	
ENDFUNC

FUNC TEXT_HOLDER CREATE_TEXT_HOLDER(STRING textlabel)
	TEXT_HOLDER retText
	retText.label = textLabel
	retText.played = FALSE
	RETURN retText
ENDFUNC

PROC INIT_TEXT()
	gtPickup		= CREATE_TEXT_HOLDER("S3A_PICKUP")
	gtPark			= CREATE_TEXT_HOLDER("S3A_PARK")
	gtGetOut		= CREATE_TEXT_HOLDER("S3A_GETOUT")
	dhBanter1 		= CREATE_TEXT_HOLDER("F3A_ARCHAT")
	htUseLift		= CREATE_TEXT_HOLDER("S3A_LFTHLP")
	htMug			= CREATE_TEXT_HOLDER("S3A_MUGHLP")
	IF IS_STRING_NULL_OR_EMPTY(htUseCam.label)
		htUseCam		= CREATE_TEXT_HOLDER("S3A_CAMHELP")	
	ENDIF
ENDPROC

PROC PRINT_GOD_TEXT(TEXT_HOLDER &text)
	IF NOT text.played
		PRINT_NOW(text.label, DEFAULT_GOD_TEXT_TIME, 1)
		text.played = TRUE
	ENDIF
ENDPROC

PROC PRINT_HELP_TEXT(TEXT_HOLDER &text)
	IF NOT text.played
		CLEAR_HELP()
		PRINT_HELP(text.label)
		text.played = TRUE
	ENDIF
ENDPROC

FUNC BOOL PLAY_DIALOGUE(TEXT_HOLDER &text)
	IF NOT text.played
		IF CREATE_CONVERSATION(sSpeech, sBlockID, text.label, CONV_PRIORITY_HIGH)
			text.played = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC WEAPON_TYPE GET_PLAYER_WEAPON()
	WEAPON_TYPE playerWep
	GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), playerWep, FALSE)
	RETURN playerWep
ENDFUNC

ENUM LIFT_NAMES
	LS_SOUTH_A = 0,
	LS_SOUTH_B,
	LS_WEST_A,
	LS_WEST_B
ENDENUM

STRUCT LIFT_STRUCT

	OBJECT_INDEX obj
	OBJECT_INDEX button
	CAMERA_INDEX cam
	BLIP_INDEX blip
	
	VECTOR vPos1			//lift position one 
	VECTOR vPos2			//lift position two
	VECTOR vRot				//rotation of the lift
	VECTOR vCameraPos
	VECTOR vCameraRot
	
	VECTOR vVolOffset1		//first vector for volume check
	VECTOR vVolOffset2		//second vector for volume check
	FLOAT fVolWidth			//width of volume
		
	BOOL bInTransit			//a flag marked true if the lift is in motion
	BOOL bActive			//Is the lift available for use by the player
	BOOL bCamYChange
	BOOL bLiftCamHelpCleared
	
	FLOAT fCurrentState 	//value for progress between position 1 and 2
	FLOAT fTargetState		//1 is pos 2, 0 is pos 1
	
	PED_INDEX passenger		//a holder for the passenger using the lift
	
	INT iSoundID			//sound for the lift
	INT iEntryStage 		//stage to hold task progress for playing anim and activating lift.
	INT iButtonScene 		// a synced scene index to hold the sync scene for the button.
	
ENDSTRUCT

LIFT_STRUCT constructionLifts[4]

STRUCT CONSTRUCTION_WORKER_STRUCT

	PED_INDEX ped
	OBJECT_INDEX board
	BLIP_INDEX blip
	
	VECTOR vStartPosition
	FLOAT fStartHeading
	
	BOOL bCanSeePlayer
	BOOL bSpottedPlayer
	FLOAT fSpotTime
	
	BOOL bTasked
	BOOL bInitialised
	BOOL bCombat
	BOOL bFlee
	
ENDSTRUCT

CONSTRUCTION_WORKER_STRUCT sConstructionWorkers[15]

#IF IS_DEBUG_BUILD
	BOOL bCanSeePlayer_widge
	BOOL bSpottedPlayer_widge
	FLOAT fSpotTime_widge
	
	BOOL bTasked_widge
	BOOL bInitialised_widge
	BOOL bCombat_widge
	BOOL bFlee_widge
#ENDIF

PROC PLAY_LIFT_SOUND(LIFT_STRUCT &lift)
	IF lift.iSoundID = -1
		lift.iSoundID = GET_SOUND_ID()
		IF DOES_ENTITY_EXIST(lift.obj)
			PLAY_SOUND_FROM_ENTITY(lift.iSoundID, "FREIGHT_ELEVATOR_02_MOTOR", lift.obj)
		ENDIF
	ENDIF
ENDPROC

PROC STOP_LIFT_SOUND(LIFT_STRUCT &lift)
	IF lift.iSoundID <> -1
		STOP_SOUND(lift.iSoundID)
		RELEASE_SOUND_ID(lift.iSoundID)
		lift.iSoundID = -1
	ENDIF
ENDPROC

PROC INIT_LIFT(LIFT_STRUCT &lift, VECTOR pos1, VECTOR pos2, VECTOR liftrotation, BOOL startAtTop = FALSE, BOOL setActive = TRUE, BOOL invertYforCam = FALSE)
	lift.vPos1 = pos1
	lift.vPos2 = pos2
	IF lift.vPos1.z > lift.vPos2.z
		lift.vPos1 = pos2
		lift.vPos2 = pos1
	ENDIF
	IF startAtTop
		lift.fCurrentState = 1
		lift.fTargetState = 0
	ELSE
		lift.fCurrentState = 0
		lift.fTargetState = 1
	ENDIF
	lift.vRot = liftrotation
	lift.bInTransit = FALSE
	lift.bActive = setActive
	lift.bCamYChange = invertYforCam
	lift.bLiftCamHelpCleared = FALSE
	lift.vVolOffset1 = <<-2.48, -2, -2.6>>
	lift.vVolOffset2 = <<2.48, -2, 0.4>>
	lift.fVolWidth = 3.0	
	lift.passenger = NULL
	lift.iSoundID = -1
	lift.iEntryStage = 0
ENDPROC

PROC WARP_LIFT_TO_STATE(LIFT_STRUCT &lift, FLOAT fState)
	lift.fCurrentState = CLAMP(fState, 0, 1)
	lift.bInTransit = FALSE
	IF lift.fCurrentState = 1
		lift.fTargetState = 0
	ELIF lift.fCurrentState = 0
		lift.fTargetState = 1
	ENDIF
	STOP_LIFT_SOUND(lift)
	SET_ENTITY_COORDS(lift.obj, COSINE_INTERP_VECTOR(lift.vPos1, lift.vPos2, lift.fCurrentState))
ENDPROC

VECTOR vButtonOffset = <<-0.469401, -0.863015, -0.6957>>
VECTOR vButtonPressAnimOffset = <<-0.369, -1.705, -1.329>>

FUNC BOOL CREATE_LIFT(LIFT_STRUCT &lift)
	IF NOT DOES_ENTITY_EXIST(lift.obj)
		REQUEST_MODEL(PROP_CONSLIFT_LIFT)
		REQUEST_MODEL(PROP_SUB_RELEASE)
		REQUEST_ANIM_DICT("missagency_heist_2a")
		IF HAS_MODEL_LOADED(PROP_CONSLIFT_LIFT)
		AND HAS_MODEL_LOADED(PROP_SUB_RELEASE)
		AND HAS_ANIM_DICT_LOADED("missagency_heist_2a")
			lift.obj = CREATE_OBJECT(PROP_CONSLIFT_LIFT, lift.vPos1)
			SET_CAN_CLIMB_ON_ENTITY(lift.obj, FALSE)
			IF lift.fCurrentState = 1
				SET_ENTITY_COORDS(lift.obj, lift.vPos2)
			ELIF lift.fCurrentState = 0
				SET_ENTITY_COORDS(lift.obj, lift.vPos1)
			ELSE
				SCRIPT_ASSERT("Cannot create lift between floors.")
			ENDIF
			SET_ENTITY_ROTATION(lift.obj, lift.vRot)
			FREEZE_ENTITY_POSITION(lift.obj, TRUE)
			lift.button = CREATE_OBJECT(PROP_SUB_RELEASE, lift.vPos1)
			ATTACH_ENTITY_TO_ENTITY(lift.button, lift.obj, 0, vButtonOffset, <<0,0,0>>)
//			IF NOT DOES_CAM_EXIST(lift.cam)
//				lift.cam = CREATE_CAMERA()
//				ATTACH_CAM_TO_ENTITY(lift.cam, lift.obj, lift.vCameraPos)
//				SET_CAM_ROT(lift.cam, lift.vCameraRot)
//				SET_CAM_FOV(lift.cam, 43.5)
//			ENDIF
		ENDIF
	ENDIF
	RETURN DOES_ENTITY_EXIST(lift.obj) AND DOES_ENTITY_EXIST(lift.button)
ENDFUNC

PROC PREPARE_LIFT_FOR_PED_TRANSIT(LIFT_STRUCT &lift, PED_INDEX ped)
	IF NOT IS_PED_INJURED(ped)
	AND DOES_ENTITY_EXIST(lift.obj)
		lift.passenger = ped
		lift.iEntryStage = 1
	ENDIF
ENDPROC

FUNC VECTOR GET_LIMITED_ROTATION(VECTOR rot)
	VECTOR vResult = rot
	WHILE vResult.x >  180 vResult.x -= 360 ENDWHILE
	WHILE vResult.x < -180 vResult.x += 360 ENDWHILE
	WHILE vResult.y >  180 vResult.y -= 360 ENDWHILE
	WHILE vResult.y < -180 vResult.y += 360 ENDWHILE
	WHILE vResult.z >  180 vResult.z -= 360 ENDWHILE
	WHILE vResult.z < -180 vResult.z += 360 ENDWHILE
	RETURN vResult
ENDFUNC

FUNC BOOL IS_ENTITY_IN_LIFT(LIFT_STRUCT &lift, ENTITY_INDEX ent)
	IF DOES_ENTITY_EXIST(ent)
	AND DOES_ENTITY_EXIST(lift.obj)
		IF IS_ENTITY_IN_ANGLED_AREA(ent, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(lift.obj, lift.vVolOffset1), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(lift.obj, lift.vVolOffset2), lift.fVolWidth)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_POSITION_NEAR_LIFT(INT iLiftIndex, VECTOR vPos)
	IF DOES_ENTITY_EXIST(constructionLifts[iLiftIndex].obj)
		IF IS_POINT_IN_ANGLED_AREA(vPos, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(constructionLifts[iLiftIndex].obj, constructionLifts[iLiftIndex].vVolOffset1 - <<5,0,0>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(constructionLifts[iLiftIndex].obj, constructionLifts[iLiftIndex].vVolOffset2 + <<5,0,0>>), constructionLifts[iLiftIndex].fVolWidth+4)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_ENTITY_IN_ANY_LIFT(ENTITY_INDEX ent)
	INT iTemp
	BOOL bReturn = FALSE
	REPEAT COUNT_OF(constructionLifts) iTemp
		IF DOES_ENTITY_EXIST(ent)
		AND DOES_ENTITY_EXIST(constructionLifts[iTemp].obj)
		AND NOT bReturn
			IF IS_ENTITY_IN_LIFT(constructionLifts[iTemp], ent)
				bReturn = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN bReturn
ENDFUNC

FUNC INT GET_LIFT_INDEX_ENTITY_IS_IN(ENTITY_INDEX ent)
	INT iTemp
	INT iReturn = -1
	REPEAT COUNT_OF(constructionLifts) iTemp
		IF DOES_ENTITY_EXIST(ent)
		AND DOES_ENTITY_EXIST(constructionLifts[iTemp].obj)
		AND iReturn = -1
			IF IS_ENTITY_IN_LIFT(constructionLifts[iTemp], ent)
				iReturn = iTemp
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN iReturn
ENDFUNC

PROC LOOK_AROUND_IN_LIFT()
	IF GET_GAME_TIMER() - iLastLookAtTask > GET_RANDOM_INT_IN_RANGE(3000, 5000)
		SWITCH iLiftLookAt
			CASE 0 
				TASK_LOOK_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<5, 5, 0>>), 5000, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
				iLiftLookAt=GET_RANDOM_INT_IN_RANGE(1, 4)
			BREAK
			CASE 1
				TASK_LOOK_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<-5, 5, 0>>), 5000, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
				iLiftLookAt=GET_RANDOM_INT_IN_RANGE(2, 4)
			BREAK
			CASE 2
				TASK_LOOK_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0, 5, 10>>), 5000, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
				iLiftLookAt++
			BREAK
			CASE 3
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), constructionLifts[LS_SOUTH_A].button, 5000, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
				iLiftLookAt = GET_RANDOM_INT_IN_RANGE(0, 3)
			BREAK
		ENDSWITCH
		iLastLookAtTask = GET_GAME_TIMER()
	ENDIF
ENDPROC

INT iStartWantedLevel

FUNC BOOL IS_LIFT_CLEAR(LIFT_STRUCT &lift)

	IF DOES_ENTITY_EXIST(pedArchitect)
		IF IS_ENTITY_IN_LIFT(lift, pedArchitect)
			RETURN FALSE
		ENDIF
	ENDIF
	
	INT iTemp
	REPEAT COUNT_OF(constructionLifts) iTemp
		IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
			IF IS_ENTITY_IN_LIFT(lift, sConstructionWorkers[iTemp].ped)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	RETURN TRUE
	
ENDFUNC

FUNC BOOL IS_PED_UNDER_LIFT(PED_INDEX ped, LIFT_STRUCT &lift)
	IF NOT IS_PED_INJURED(ped)
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(lift.obj, lift.vVolOffset1 + <<-1.0,0,-1.5>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(lift.obj, lift.vVolOffset2 + <<1,0,-1.5>>), lift.fVolWidth + 1.0)	
		AND NOT IS_ENTITY_IN_LIFT(lift, PLAYER_PED_ID())
		AND VDIST2(GET_ENTITY_COORDS(lift.obj), lift.vPos1) < 2*2
		AND IS_ENTITY_TOUCHING_ENTITY(lift.obj, PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC UPDATE_LIFT(LIFT_STRUCT &lift)
	IF NOT lift.bInTransit
		IF lift.bActive
			SWITCH lift.iEntryStage
				CASE 0
					IF IS_ENTITY_IN_LIFT(lift, PLAYER_PED_ID())
					AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
						IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
							IF IS_LIFT_CLEAR(lift)
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)
								IF NOT bElevatorMusicStarted
									TRIGGER_MUSIC_EVENT("AH2A_MISSION_START")
								ENDIF
								IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
									iStartWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
								ENDIF
								PREPARE_LIFT_FOR_PED_TRANSIT(lift, PLAYER_PED_ID())
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE 1
					IF IS_PED_A_PLAYER(lift.passenger)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						VECTOR vTemp
						vTemp = GET_ENTITY_COORDS(lift.button) - GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(lift.obj, vButtonPressAnimOffset)
						
						FLOAT fHead
						fHead = GET_HEADING_FROM_VECTOR_2D(vTemp.x, vTemp.y)
						
						TASK_GO_STRAIGHT_TO_COORD(lift.passenger, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(lift.obj, vButtonPressAnimOffset), PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, fHead) 
						lift.iEntryStage++	
					ELSE
						IF IS_ENTITY_IN_LIFT(lift, lift.passenger)
							lift.iEntryStage = 5
						ENDIF
					ENDIF
				BREAK
				CASE 2
					VECTOR vPedToButton
					vPedToButton = GET_ENTITY_COORDS(lift.button) - GET_ENTITY_COORDS(lift.passenger)
					VECTOR vPedForwards
					vPedForwards = GET_ENTITY_FORWARD_VECTOR(lift.passenger)
					FLOAT fAngle
					fAngle = ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vPedToButton.x, vPedToButton.y, vPedForwards.x, vPedForwards.y))
					IF GET_SCRIPT_TASK_STATUS(lift.passenger, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
					OR fAngle < 15
						lift.iEntryStage++
					ENDIF
				BREAK
				CASE 3
					IF GET_SCRIPT_TASK_STATUS(lift.passenger, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
						VECTOR vScenePos
						vScenePos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(lift.obj, vButtonPressAnimOffset)
						vScenePos.z = vPlayerCoords.z
						lift.iButtonScene = CREATE_SYNCHRONIZED_SCENE(vScenePos, GET_ENTITY_ROTATION(lift.obj))
						TASK_SYNCHRONIZED_SCENE(lift.passenger, lift.iButtonScene, "missagency_heist_2a", "push_button", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, SLOW_BLEND_IN)
						lift.iEntryStage++
					ENDIF
				BREAK
				CASE 4
					IF IS_SYNCHRONIZED_SCENE_RUNNING(lift.iButtonScene)
					AND GET_SYNCHRONIZED_SCENE_PHASE(lift.iButtonScene) > 0.95
						lift.iEntryStage++
					ENDIF
				BREAK
				CASE 5
					IF lift.fCurrentState = 0
						lift.fTargetState = 1
					ELSE
						lift.fTargetState = 0
					ENDIF
					lift.bInTransit = TRUE
					lift.iEntryStage = 0
					CLEAR_PED_TASKS(lift.passenger)
					VECTOR vAttach
					vAttach = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(lift.obj, GET_ENTITY_COORDS(lift.passenger))
					vAttach.z = -1.3024
					ATTACH_ENTITY_TO_ENTITY(lift.passenger, lift.obj, 0, vAttach, <<0,0,GET_ENTITY_HEADING(lift.passenger)-GET_ENTITY_HEADING(lift.obj)>>, FALSE, FALSE, TRUE, TRUE)
					SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), TRUE)
					lift.iEntryStage = 0
				BREAK
			ENDSWITCH
		ENDIF
		
	ELSE
	
		IF DOES_ENTITY_EXIST(lift.obj)
		
			IF lift.iSoundID = -1
				PLAY_LIFT_SOUND(lift)
			ENDIF
			
			IF lift.fTargetState = 1
				lift.fCurrentState = CLAMP(lift.fCurrentState + 0.003, 0, 1)
			ELSE
				lift.fCurrentState = CLAMP(lift.fCurrentState - 0.003, 0, 1)
			ENDIF
			
			SET_ENTITY_COORDS(lift.obj, COSINE_INTERP_VECTOR(lift.vPos1, lift.vPos2, lift.fCurrentState))
			
			IF lift.passenger = PLAYER_PED_ID()
			
//				IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
//					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//				ENDIF
//			
//				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
//				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
//				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
//				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
//				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
//				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
//				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
//				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)
//				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
//				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
//				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)			
//				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)				
//				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
//				
				SET_FOLLOW_CAM_IGNORE_ATTACH_PARENT_MOVEMENT_THIS_UPDATE()
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				IF iStartWantedLevel > 0
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), iStartWantedLevel)
				ENDIF
				LOOK_AROUND_IN_LIFT()
				
			ELSE
				IF IS_PED_UNDER_LIFT(PLAYER_PED_ID(), lift)
					IF NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
						SET_PED_TO_RAGDOLL(PLAYER_PED_ID(), 5000, 5000, TASK_RELAX, FALSE, FALSE)
					ENDIF
				ENDIF
				IF lift.bInTransit
					IF lift.passenger = pedArchitect
					AND IS_ENTITY_ATTACHED_TO_ENTITY(pedArchitect, lift.obj)
						IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_ROCKET, GET_ENTITY_COORDS(pedArchitect,FALSE), 10)
						OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADE, GET_ENTITY_COORDS(pedArchitect,FALSE), 10)
						OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADELAUNCHER, GET_ENTITY_COORDS(pedArchitect,FALSE), 10)
						OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_STICKYBOMB, GET_ENTITY_COORDS(pedArchitect,FALSE), 10)
							lift.bInTransit = TRUE
							lift.fTargetState = 0.0
							DETACH_ENTITY(pedArchitect, TRUE, FALSE)
							SET_ENTITY_HEALTH(pedArchitect, 0)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF ABSF(lift.fTargetState - lift.fCurrentState) = 0
				IF IS_PED_UNDER_LIFT(PLAYER_PED_ID(), lift)
					SET_ENTITY_HEALTH(PLAYER_PED_ID(), 0)
				ENDIF
				IF lift.passenger<>NULL
					IF NOT IS_PED_INJURED(lift.passenger)
						DETACH_ENTITY(lift.passenger, FALSE, FALSE)
					ENDIF
					IF lift.passenger = PLAYER_PED_ID()
						htUseCam.played = FALSE
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, FALSE)
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE) 
					ENDIF
					lift.passenger = NULL
					STOP_LIFT_SOUND(lift)
					iStartWantedLevel = 0
				ENDIF
				PRINTLN(VDIST2(GET_ENTITY_COORDS(lift.obj), lift.vPos1))
				WARP_LIFT_TO_STATE(lift, lift.fCurrentState)
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

PROC GIVE_THE_CASE_TO_PED(PED_INDEX ped)
	IF DOES_ENTITY_EXIST(objPlans)
		DELETE_OBJECT(objPlans)
	ENDIF
	IF NOT HAS_PED_GOT_WEAPON(ped, WEAPONTYPE_BRIEFCASE_02)
		GIVE_WEAPON_TO_PED(ped, WEAPONTYPE_BRIEFCASE_02, INFINITE_AMMO, TRUE, TRUE)
	ENDIF
	SET_CURRENT_PED_WEAPON(ped, WEAPONTYPE_BRIEFCASE_02, TRUE)
ENDPROC

PROC DROP_THE_CASE(PED_INDEX ped)
	IF DOES_ENTITY_EXIST(objPlans)
		DELETE_OBJECT(objPlans)
	ENDIF
// 	We could ask programmers to get a DoDeathCheck for the command GET_WEAPON_OBJECT_FROM_PED as we have for GET_CURRENT_PED_WEAPON, but it's just an ignorable assert.
	objPlans = GET_WEAPON_OBJECT_FROM_PED(ped, FALSE)
	IF DOES_ENTITY_EXIST(objPlans)
		ACTIVATE_PHYSICS(objPlans)
	ELSE
		objPlans = CREATE_WEAPON_OBJECT(WEAPONTYPE_BRIEFCASE_02, INFINITE_AMMO, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehArchitectCar , <<-1,0,0>>), TRUE)
		ACTIVATE_PHYSICS(objPlans)
	ENDIF
	REMOVE_WEAPON_FROM_PED(ped, WEAPONTYPE_BRIEFCASE_02)
ENDPROC

ENUM CASE_STATE
	AH2_CS_WITH_ARCHITECT = 0,
	AH2_CS_DROPPING_CASE,
	AH2_CS_ON_GROUND,
	AH2_CS_IN_LIFT,
	AH2_CS_WITH_PLAYER
ENDENUM
CASE_STATE eCaseState

ENUM AH2_MUGGING_STAGE_ENUM

	AH2_MSE_LOAD_ANIMS = 0,
	AH2_MSE_AREA_CHECK,
	AH2_MSE_WAIT_FOR_PLAYER_TO_START_MUGGING,
	AH2_MSE_TURN_TO_FACE_THE_PLAYER,
	AH2_MSE_DO_HANDS_UP,
	AH2_MSE_DROP_CASE,
	AH2_MSE_DO_COWERING,
	AH2_MSE_MUGGING_BLOWN,
	AH2_MSE_MUGGING_OVER
	
ENDENUM
AH2_MUGGING_STAGE_ENUM eMugStage

ENUM AH2_MUGGING_COWER_STAGE_ENUM

	AH2_MCS_HANDS_UP = 0,
	AH2_MCS_HANDS_UP_ANXIOUS,
	AH2_MCS_SHOCKED,
	AH2_MCS_OUT_OF_ANIM
	
ENDENUM
AH2_MUGGING_COWER_STAGE_ENUM eCowerStage

ENUM AH2_RUNAWAY_STATE
	AH2_RS_FOLLOW_PATH_TOP_FLOOR = 0,
	AH2_RS_NEAR_LIFT,
	AH2_RS_IN_LIFT,
	AH2_RS_GOING_DOWN,
	AH2_RS_FOLLOW_PATH1,
	AH2_RS_FOLLOW_PATH2,
	AH2_RS_FLEE
ENDENUM
AH2_RUNAWAY_STATE eRunawayState = AH2_RS_FOLLOW_PATH_TOP_FLOOR

PROC RESET_MUGGING(AH2_MUGGING_STAGE_ENUM paramMugStage = AH2_MSE_LOAD_ANIMS)
	eMugStage = paramMugStage
	eCowerStage = AH2_MCS_HANDS_UP
//	iCowerStage = 0
	bSecondLineDone = FALSE
ENDPROC

FUNC BOOL HAS_PLAYER_PICKED_UP_CASE()
	IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(objPlans)) < 1.5*1.5
	AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
	AND GET_ENTITY_SPEED(objPlans) < 0.1
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		PLAY_SOUND_FROM_ENTITY(-1, "FBI_HEIST_H2_ARCHITECT_GRAB_CASE", objPlans)
		IF DOES_BLIP_EXIST(blipPlans)
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
			REMOVE_BLIP(blipPlans)
		ENDIF
		PRINTLN("PLAYER HAS CASE")
		bPlayerHasCase = TRUE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD

	FUNC STRING GET_STRING_FROM_CASE_STATE(CASE_STATE eState)
		SWITCH eState
			CASE AH2_CS_WITH_ARCHITECT	RETURN "AH2_CS_WITH_ARCHITECT" 	BREAK
			CASE AH2_CS_DROPPING_CASE	RETURN "AH2_CS_DROPPING_CASE"	BREAK
			CASE AH2_CS_ON_GROUND		RETURN "AH2_CS_ON_GROUND"		BREAK
			CASE AH2_CS_IN_LIFT			RETURN "AH2_CS_IN_LIFT"			BREAK
			CASE AH2_CS_WITH_PLAYER		RETURN "AH2_CS_WITH_PLAYER"		BREAK
		ENDSWITCH
		RETURN ""
	ENDFUNC
	
	FUNC STRING GET_STRING_FROM_MUGGING_STATE(AH2_MUGGING_STAGE_ENUM paramMugStage)
		SWITCH paramMugStage
			CASE AH2_MSE_LOAD_ANIMS							RETURN "AH2_MSE_LOAD_ANIMS						" 	BREAK
			CASE AH2_MSE_AREA_CHECK							RETURN "AH2_MSE_AREA_CHECK						"	BREAK
			CASE AH2_MSE_WAIT_FOR_PLAYER_TO_START_MUGGING	RETURN "AH2_MSE_WAIT_FOR_PLAYER_TO_START_MUGGING"	BREAK
			CASE AH2_MSE_TURN_TO_FACE_THE_PLAYER			RETURN "AH2_MSE_TURN_TO_FACE_THE_PLAYER			"	BREAK
			CASE AH2_MSE_DO_HANDS_UP						RETURN "AH2_MSE_DO_HANDS_UP						"	BREAK
			CASE AH2_MSE_DROP_CASE							RETURN "AH2_MSE_DROP_CASE						"	BREAK
			CASE AH2_MSE_DO_COWERING						RETURN "AH2_MSE_DO_COWERING						"	BREAK
			CASE AH2_MSE_MUGGING_BLOWN						RETURN "AH2_MSE_MUGGING_BLOWN					"	BREAK
			CASE AH2_MSE_MUGGING_OVER						RETURN "AH2_MSE_MUGGING_OVER					"	BREAK
		ENDSWITCH
		RETURN ""
	ENDFUNC
	
	FUNC STRING GET_STRING_FROM_FLEE_STATE(AH2_RUNAWAY_STATE eFleeState)
		SWITCH eFleeState
			CASE AH2_RS_FOLLOW_PATH_TOP_FLOOR	RETURN "AH2_RS_FOLLOW_PATH_TOP_FLOOR"	BREAK
			CASE AH2_RS_NEAR_LIFT				RETURN "AH2_RS_NEAR_LIFT			"	BREAK
			CASE AH2_RS_IN_LIFT					RETURN "AH2_RS_IN_LIFT				"	BREAK
			CASE AH2_RS_GOING_DOWN				RETURN "AH2_RS_GOING_DOWN			"	BREAK
			CASE AH2_RS_FOLLOW_PATH1			RETURN "AH2_RS_FOLLOW_PATH1			"	BREAK
			CASE AH2_RS_FOLLOW_PATH2			RETURN "AH2_RS_FOLLOW_PATH2			"	BREAK
			CASE AH2_RS_FLEE					RETURN "AH2_RS_FLEE					"	BREAK
		ENDSWITCH
		RETURN "Invalid flee state"
	ENDFUNC
	
	FUNC STRING GET_STRING_FROM_SITE_STATE(SITE_STATE paramSiteState)
		SWITCH paramSiteState
			CASE AH2_SS_ALL_QUIET				RETURN "AH2_SS_ALL_QUIET		"	BREAK
			CASE AH2_SS_SPOTTED_PLAYER			RETURN "AH2_SS_SPOTTED_PLAYER	"	BREAK
			CASE AH2_SS_ATTACKING_PLAYER		RETURN "AH2_SS_ATTACKING_PLAYER	"	BREAK
			CASE AH2_SS_FLEEING_PLAYER			RETURN "AH2_SS_FLEEING_PLAYER	"	BREAK
		ENDSWITCH
		RETURN "Invalid site state"
	ENDFUNC
	
	FUNC STRING GET_STRING_FROM_AMBIENT_STATE(AMBIENT_STAGE paramAmbeintStage)
		SWITCH paramAmbeintStage
			CASE AH2_AS_SETUP_GROUNDFLOOR1		RETURN "AH2_AS_SETUP_GROUNDFLOOR1	"	BREAK
			CASE AH2_AS_RUN_GROUNDFLOOR			RETURN "AH2_AS_RUN_GROUNDFLOOR		"	BREAK
			CASE AH2_AS_SETUP_TOPFLOOR			RETURN "AH2_AS_SETUP_TOPFLOOR		"	BREAK
			CASE AH2_AS_RUN_TOP_FLOOR			RETURN "AH2_AS_RUN_TOP_FLOOR		"	BREAK
			CASE AH2_AS_SETUP_GROUNDFLOOR2		RETURN "AH2_AS_SETUP_GROUNDFLOOR2	"	BREAK
			CASE AH2_AS_RUN_GROUNDFLOOR2		RETURN "AH2_AS_RUN_GROUNDFLOOR2		"	BREAK
		ENDSWITCH
		RETURN "Invalid ambient state"
	ENDFUNC
	
	FUNC STRING GET_STRING_FROM_MISSION_STAGE(MISSION_STAGE_FLAG stage)
		SWITCH stage
			CASE STAGE_GROUND_FLOOR			RETURN "STAGE_GROUND_FLOOR	"	BREAK
			CASE STAGE_FIRST_FLOOR			RETURN "STAGE_FIRST_FLOOR	"	BREAK
			CASE STAGE_ESCAPE				RETURN "STAGE_ESCAPE		"	BREAK
			CASE STAGE_DRIVE_BACK			RETURN "STAGE_DRIVE_BACK	"	BREAK
			CASE STAGE_LOSE_COPS			RETURN "STAGE_LOSE_COPS		"	BREAK
			CASE STAGE_PLAYER_SPOTTED		RETURN "STAGE_PLAYER_SPOTTED"	BREAK
			CASE STAGE_COVER_BLOWN			RETURN "STAGE_COVER_BLOWN	"	BREAK
			CASE STAGE_TEST					RETURN "STAGE_TEST			"	BREAK
		ENDSWITCH
		RETURN "Invalid mission stage"
	ENDFUNC

	FUNC STRING GET_STRING_FROM_LEADIN_STAGE(AH2_LEADING_STAGE stage)
		SWITCH stage
			CASE AH2_LS_WAIT_FOR_ACTIVATION	RETURN "AH2_LS_WAIT_FOR_ACTIVATION"	BREAK
			CASE AH2_LS_AWAIT_INTERIOR		RETURN "AH2_LS_AWAIT_INTERIOR"		BREAK
			CASE AH2_LS_WAIT				RETURN "AH2_LS_WAIT"				BREAK
			CASE AH2_UNLOAD					RETURN "AH2_UNLOAD"					BREAK
		ENDSWITCH
		RETURN "Invalid Lead in stage"
	ENDFUNC
	
#ENDIF

///case state stuff

INT iTimeOfCaseDrop = -1
INT iTimeOfCaseText = -1
INT iCaseLift = -1

FUNC BOOL IS_CASE_TEXT_SHOWING()
	RETURN GET_GAME_TIMER() - iTimeOfCaseText < 6000
ENDFUNC

PROC MANAGE_CASE()

	SWITCH eCaseState
	
		CASE AH2_CS_WITH_ARCHITECT
		
			IF DOES_ENTITY_EXIST(pedArchitect)
			
				WEAPON_TYPE wepArch
				wepArch = WEAPONTYPE_INVALID
			
				IF GET_GAME_TIMER() - iLastWeaponForce > 1000
					GET_CURRENT_PED_WEAPON(pedArchitect, wepArch, FALSE)
					iLastWeaponForce = GET_GAME_TIMER()
				ENDIF
				
				IF NOT IS_PED_INJURED(pedArchitect)
					IF NOT HAS_PED_GOT_WEAPON(pedArchitect, WEAPONTYPE_BRIEFCASE_02)
						GIVE_WEAPON_TO_PED(pedArchitect, WEAPONTYPE_BRIEFCASE_02, INFINITE_AMMO, TRUE, TRUE)
					ELSE
						IF wepArch <> WEAPONTYPE_BRIEFCASE_02
							GIVE_THE_CASE_TO_PED(pedArchitect)
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehArchitectCar)
					IF IS_PED_IN_VEHICLE(pedArchitect, vehArchitectCar, TRUE)
						IF IS_ENTITY_DEAD(pedArchitect)
						OR IS_PED_INJURED(pedArchitect)
						OR IS_ENTITY_DEAD(vehArchitectCar)
						OR IS_PED_RAGDOLL(pedArchitect)
							REQUEST_WEAPON_ASSET(WEAPONTYPE_BRIEFCASE_02, ENUM_TO_INT(WRF_REQUEST_MOTION_ANIMS))
							PRINTLN("CREATING BRIEFCASE OBJECT HERE")
							IF HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_BRIEFCASE_02)
								PRINTLN("CREATING BRIEFCASE OBJECT HERE")
								REMOVE_WEAPON_FROM_PED(pedArchitect, WEAPONTYPE_BRIEFCASE_02)
								objPlans = CREATE_WEAPON_OBJECT(WEAPONTYPE_BRIEFCASE_02, INFINITE_AMMO, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehArchitectCar , <<-1,0,0>>), TRUE)						
								eCaseState = AH2_CS_ON_GROUND
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF wepArch = WEAPONTYPE_BRIEFCASE_02
					IF IS_PED_INJURED(pedArchitect)
					OR IS_ENTITY_DEAD(pedArchitect)
					OR (NOT IS_PED_INJURED(pedArchitect) AND IS_PED_RAGDOLL(pedArchitect))
						PRINTLN("DROPPING CASE 2")
						DROP_THE_CASE(pedArchitect)
						eCaseState = AH2_CS_DROPPING_CASE
					ENDIF
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE AH2_CS_DROPPING_CASE
		
			IF NOT bMethodDone
				IF DOES_ENTITY_EXIST(pedArchitect)
					IF NOT IS_ENTITY_DEAD(pedArchitect)
						IF bMugged
							INFORM_MISSION_STATS_OF_INCREMENT(AH2_METHOD, 1, TRUE)
							bMethodDone = TRUE
							PRINTLN("\n\n\n ah2 method - mugged")
						ELIF IS_PED_BEING_STUNNED(pedArchitect)
							INFORM_MISSION_STATS_OF_INCREMENT(AH2_METHOD, 2, TRUE)
							bMethodDone = TRUE
							PRINTLN("\n\n\n ah2 method - stunned")
						ELIF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_MELEE)
						OR NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE | WF_INCLUDE_MELEE)
							INFORM_MISSION_STATS_OF_INCREMENT(AH2_METHOD, 0, TRUE)
							bMethodDone = TRUE
							PRINTLN("\n\n\n ah2 method - meleed")
						ENDIF
					ELSE
						IF IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID())
						AND NOT WAS_PED_KILLED_BY_TAKEDOWN(pedArchitect)
						AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) > 4
							INFORM_MISSION_STATS_OF_INCREMENT(AH2_METHOD, 3, TRUE)
							bMethodDone = TRUE
							PRINTLN("\n\n\n ah2 method - silenced")
						ELSE
							INFORM_MISSION_STATS_OF_INCREMENT(AH2_METHOD, -1, TRUE)
							bMethodDone = TRUE
							PRINTLN("\n\n\n ah2 method - other")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_PED_GOT_WEAPON(pedArchitect, WEAPONTYPE_BRIEFCASE_02)
				REMOVE_WEAPON_FROM_PED(pedArchitect, WEAPONTYPE_BRIEFCASE_02)
			ENDIF
			
			IF DOES_ENTITY_EXIST(objPlans)
			
				IF IS_ENTITY_IN_ANY_LIFT(objPlans)
				AND constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(objPlans)].bInTransit
				
					eCaseState = AH2_CS_ON_GROUND
					
				ELSE
				
					IF iTimeOfCaseDrop = -1
						iTimeOfCaseDrop = GET_GAME_TIMER()
					ELSE
						IF GET_GAME_TIMER() - iTimeOfCaseDrop > 1000
						AND GET_ENTITY_SPEED(objPlans) < 0.1
							eCaseState = AH2_CS_ON_GROUND
						ENDIF
					ENDIF
					
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE AH2_CS_ON_GROUND
		
			IF NOT bMethodDone
				INFORM_MISSION_STATS_OF_INCREMENT(AH2_METHOD, -1, TRUE)
				PRINTLN("\n\n\n ah2 method - other")
				bMethodDone = TRUE
			ENDIF
		
			IF HAS_PED_GOT_WEAPON(pedArchitect, WEAPONTYPE_BRIEFCASE_02)
				REMOVE_WEAPON_FROM_PED(pedArchitect, WEAPONTYPE_BRIEFCASE_02)
			ENDIF
			
			IF DOES_BLIP_EXIST(blipArchitect)
				REMOVE_BLIP(blipArchitect)
			ENDIF
			
			IF IS_ENTITY_IN_ANY_LIFT(objPlans)
			AND constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(objPlans)].bInTransit
			AND DOES_ENTITY_EXIST(constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(objPlans)].obj)
				IF NOT IS_ENTITY_ATTACHED(objPlans)
					IF IS_ENTITY_OCCLUDED(objPlans)
					OR GET_ENTITY_SPEED(objPlans) > 0
						ATTACH_ENTITY_TO_ENTITY(objPlans, constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(objPlans)].obj, 0, <<0.450814, -2.446289, -2.240097>>, <<-90.000000,90.177399,-0.609694>>)
					ELSE
						VECTOR vTemp
						vTemp = GET_ENTITY_COORDS(objPlans) - GET_ENTITY_COORDS(constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(objPlans)].obj)
						ATTACH_ENTITY_TO_ENTITY(objPlans, constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(objPlans)].obj, 0, <<vTemp.x, vTemp.y, -2.240097>>, <<-90.000000,90.177399,-0.609694>>)
					ENDIF
					iCaseLift = GET_LIFT_INDEX_ENTITY_IS_IN(objPlans)
				ENDIF
			ELSE
				IF iCaseLift > 0 and iCaseLift < 4
					IF NOT constructionLifts[iCaseLift].bInTransit
						IF IS_ENTITY_ATTACHED(objPlans)
							PRINTLN("DETACHING CASE FORM LIFT ")
							DETACH_ENTITY(objPlans, TRUE, FALSE)
							APPLY_FORCE_TO_ENTITY(objPlans, APPLY_TYPE_FORCE, <<0,0,0.01>>, <<0,0,0>>, 0, TRUE, TRUE, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE AH2_CS_WITH_PLAYER
			IF GET_GAME_TIMER() - iTimeCasePickup < 1000
				SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableActionMode, TRUE)
			ENDIF
			IF NOT bPlayerhasCase
				bPlayerhasCase = TRUE
			ENDIF
			IF GET_PLAYER_WEAPON() = WEAPONTYPE_BRIEFCASE_02
				IF NOT bCaseClipsetApplied
					REQUEST_ANIM_SET("MOVE_P_M_ONE_BRIEFCASE")
					IF HAS_ANIM_SET_LOADED("MOVE_P_M_ONE_BRIEFCASE")
						SET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID(), "MOVE_P_M_ONE_BRIEFCASE")
						bCaseClipsetApplied = TRUE
					ENDIF
				ENDIF
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
			ELSE
				IF bCaseClipsetApplied
					REQUEST_ANIM_SET("MOVE_P_M_ONE")
					IF HAS_ANIM_SET_LOADED("MOVE_P_M_ONE")
						SET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID(), "MOVE_P_M_ONE")
						bCaseClipsetApplied = FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF DOES_ENTITY_EXIST(objPlans)
		IF eCaseState = AH2_CS_ON_GROUND
			IF iTimeCasePickup != 0
				iTimeCasePickup = 0
			ENDIF
			IF NOT DOES_BLIP_EXIST(blipPlans)
				blipPlans = CREATE_BLIP_FOR_OBJECT(objPlans)
			ELSE
				IF (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					IF NOT gtPickup.played
						PRINT_NOW("S3A_PICKUP", DEFAULT_GOD_TEXT_TIME, 1)
						gtPickup.played = TRUE
						iTimeOfCaseText = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
			IF HAS_PLAYER_PICKED_UP_CASE()
				CLEAR_PRINTS()
				GO_TO_STAGE(STAGE_ESCAPE)
				GIVE_THE_CASE_TO_PED(PLAYER_PED_ID())
				iTimeCasePickup = GET_GAME_TIMER()
				eCaseState = AH2_CS_WITH_PLAYER
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bSkip
		IF eCaseState != AH2_CS_WITH_ARCHITECT
		AND (mission_stage = STAGE_GROUND_FLOOR OR mission_stage = STAGE_FIRST_FLOOR OR mission_stage = STAGE_PLAYER_SPOTTED)
			IF eCaseState = AH2_CS_WITH_PLAYER
				GO_TO_STAGE(STAGE_ESCAPE)
			ELSE
				GO_TO_STAGE(STAGE_COVER_BLOWN)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

STRUCT LINESTRUCT
	VECTOR p1
	VECTOR p2
ENDSTRUCT

FUNC LINESTRUCT CREATE_LINE(VECTOR v1, VECTOR v2)
	LINESTRUCT line
	line.p1 = v1
	line.p2 = v2
	RETURN line
ENDFUNC

FUNC BOOL IS_POINT_BEHIND_LINE(VECTOR O, LINESTRUCT &line)
	FLOAT test_val
	test_val = (line.p2.x - line.p1.x) * (O.y - line.p1.y) - (line.p2.y - line.p1.y) * (O.x - line.p1.x)
	RETURN test_val >= 0
ENDFUNC

LINESTRUCT siteBoundary[7]
PROC INIT_SITE_BOUNDARY()

	siteBoundary[0] = CREATE_LINE(<<-58.59, -961.48, 28.37>>, <<-159.73, -924.63, 28.40>>)
	siteBoundary[1] = CREATE_LINE(<<-159.73, -924.63, 28.40>>, <<-230.83, -1120.06, 21.99>>)
	siteBoundary[2] = CREATE_LINE(<<-230.83, -1120.06, 21.99>>, <<-136.78, -1119.10, 24.00>>)
	siteBoundary[3] = CREATE_LINE(<<-136.78, -1119.10, 24.00>>, <<-124.51, -1114.89, 24.68>>)
	siteBoundary[4] = CREATE_LINE(<<-124.51, -1114.89, 24.68>>, <<-112.83, -1098.48, 24.81>>)
	siteBoundary[5] = CREATE_LINE(<<-112.83, -1098.48, 24.81>>, <<-102.21, -1077.07, 25.84>>)
	siteBoundary[6] = CREATE_LINE(<<-102.21, -1077.07, 25.84>>, <<-58.59, -961.48, 28.37>>)
	
ENDPROC

FUNC BOOL IS_PED_ON_SITE(PED_INDEX ped)

	VECTOR point 
	IF DOES_ENTITY_EXIST(ped)
		point = GET_ENTITY_COORDS(ped, FALSE)
	ENDIF
	BOOL bReturn = TRUE
	INT iTemp
	REPEAT COUNT_OF(siteBoundary) iTemp
		IF NOT IS_POINT_BEHIND_LINE(point, siteBoundary[iTemp])
			bReturn = FALSE
		ENDIF
	ENDREPEAT
	#IF IS_DEBUG_BUILD
		REPEAT COUNT_OF(siteBoundary) iTemp
			IF bReturn
				DRAW_DEBUG_LINE(siteBoundary[iTemp].p1, siteBoundary[iTemp].p2, 255,0,0)
			ELSE
				DRAW_DEBUG_LINE(siteBoundary[iTemp].p1, siteBoundary[iTemp].p2, 0,255,0)
			ENDIF
		ENDREPEAT
	#ENDIF

	RETURN bReturn
ENDFUNC

FUNC BOOL CAN_ARCHITECT_SEE_PLAYER()
	RETURN bArchCanSeePlayer
ENDFUNC

PROC UPDATE_ARCHITECT_CAN_SEE_PLAYER()

	IF (GET_GAME_TIMER() - iArchCanSeePlayerTimer) > 500
		IF DOES_ENTITY_EXIST(pedArchitect)
		AND NOT IS_PED_INJURED(pedArchitect)
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			bArchCanSeePlayer = HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(pedArchitect, PLAYER_PED_ID()) 
			AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 10 * 10
			iArchCanSeePlayerTimer = GET_GAME_TIMER()
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_ON_TOP_FLOOR()
	RETURN IS_PED_ON_SITE(PLAYER_PED_ID()) AND vPlayerCoords.z > 75
ENDFUNC

FUNC INT GET_CLOSEST_CONSTRUCTION_WORKER(CONSTRUCTION_WORKER_STRUCT &sWorker[], BOOL bClosestSpotted = FALSE)
	INT iTemp
	FLOAT fClosestDistance = 999999
	INT iClosestPed = -1
	REPEAT COUNT_OF(sWorker) iTemp
		IF DOES_ENTITY_EXIST(sWorker[iTemp].ped)
		AND NOT IS_PED_INJURED(sWorker[iTemp].ped)
			IF bClosestSpotted AND sWorker[iTemp].bSpottedPlayer
			OR NOT bClosestSpotted 
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sWorker[iTemp].ped)) < fClosestDistance
					fClosestDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sWorker[iTemp].ped))
					iClosestPed = iTemp
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN iClosestPed
ENDFUNC

FUNC BOOL DO_ARCHITECT_FLEEING_LINE(STRING sArchRoot, STRING sWorkerRoot)

	BOOL bArchPriority
	BOOL bTooFarAway = TRUE
	
	IF DOES_ENTITY_EXIST(pedArchitect)
	AND NOT IS_ENTITY_DEAD(pedArchitect)
	AND NOT IS_PED_INJURED(pedArchitect)
	AND NOT IS_PED_BEING_STEALTH_KILLED(pedArchitect)
	AND NOT IS_PED_BEING_STUNNED(pedArchitect)
	AND NOT bBigScream
		bArchPriority = TRUE
	ENDIF
	
	INT iClosest
	iClosest = GET_CLOSEST_CONSTRUCTION_WORKER(sConstructionWorkers)
	
	IF bArchPriority
	AND iClosest <> -1
	AND VDIST2(GET_ENTITY_COORDS(sConstructionWorkers[iClosest].ped), GET_ENTITY_COORDS(PLAYER_PED_ID())) < VDIST2(GET_ENTITY_COORDS(pedArchitect), GET_ENTITY_COORDS(PLAYER_PED_ID()))
		bArchPriority = FALSE
	ENDIF
	
	IF bArchPriority
		IF VDIST2(GET_ENTITY_COORDS(pedArchitect), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 20*20
			bTooFarAway = FALSE
			IF CREATE_CONVERSATION(sSpeech, sBlockID, sArchRoot, CONV_PRIORITY_HIGH)
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF iClosest <> -1
		AND NOT IS_STRING_NULL_OR_EMPTY(sWorkerRoot)
			IF VDIST2(GET_ENTITY_COORDS(sConstructionWorkers[iClosest].ped), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 20*20
				IF NOT IS_PED_INJURED(sConstructionWorkers[iClosest].ped)
					bTooFarAway = FALSE
					REMOVE_PED_FOR_DIALOGUE(sSpeech, 2)
					ADD_PED_FOR_DIALOGUE(sSpeech, 2, sConstructionWorkers[iClosest].ped, "FIBConstruction")
					IF ARE_STRINGS_EQUAL(sWorkerRoot, "F3A_CONFLEE")
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(sConstructionWorkers[iClosest].ped, "SHOUT_THREATEN", "FIBConstruction", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
					ELIF ARE_STRINGS_EQUAL(sWorkerRoot, "F3A_CONFLEE")
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(sConstructionWorkers[iClosest].ped, "FIGHT_RUN", "FIBConstruction", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
					ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bTooFarAway
		#IF IS_DEBUG_BUILD
			PRINTLN("Nobody nearby to say line, closest worker is ", iClosest, " worker string is ", sWorkerRoot)
		#ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC UPDATE_WORKERS_LINE_OF_SIGHT()

	IF DOES_ENTITY_EXIST(sConstructionWorkers[iCurrentWorkerLosCheck].ped)
	AND NOT IS_PED_INJURED(sConstructionWorkers[iCurrentWorkerLosCheck].ped)
	
		IF GET_GAME_TIMER() - iLastLosCheck > 100
		
			VECTOR vPedToPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID()) - GET_ENTITY_COORDS(sConstructionWorkers[iCurrentWorkerLosCheck].ped)
			VECTOR vPedForward = GET_ENTITY_FORWARD_VECTOR(sConstructionWorkers[iCurrentWorkerLosCheck].ped)
			
			FLOAT fAngle = ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vPedForward.x, vPedForward.y, vPedToPlayer.x, vPedToPlayer.y))
			FLOAT fDist = VDIST2(vPlayerCoords, GET_ENTITY_COORDS(sConstructionWorkers[iCurrentWorkerLosCheck].ped))
			
			IF iCurrentWorkerLosCheck >= 0
			AND iCurrentWorkerLosCheck < COUNT_OF(sConstructionWorkers)
			AND DOES_ENTITY_EXIST(sConstructionWorkers[iCurrentWorkerLosCheck].ped)
			AND NOT IS_PED_INJURED(sConstructionWorkers[iCurrentWorkerLosCheck].ped)
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND fAngle < 50
			AND fDist < 400
			AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(sConstructionWorkers[iCurrentWorkerLosCheck].ped, PLAYER_PED_ID())
				IF NOT sConstructionWorkers[iCurrentWorkerLosCheck].bCanSeePlayer
					TASK_LOOK_AT_ENTITY(sConstructionWorkers[iCurrentWorkerLosCheck].ped, PLAYER_PED_ID(), -1)
				ENDIF
				sConstructionWorkers[iCurrentWorkerLosCheck].bCanSeePlayer = TRUE
			ELSE
				IF sConstructionWorkers[iCurrentWorkerLosCheck].bCanSeePlayer
					TASK_CLEAR_LOOK_AT(sConstructionWorkers[iCurrentWorkerLosCheck].ped)
				ENDIF
				sConstructionWorkers[iCurrentWorkerLosCheck].bCanSeePlayer = FALSE
			ENDIF
			
			iLastLosCheck = GET_GAME_TIMER()
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
			INT i
			REPEAT COUNT_OF(sConstructionWorkers) i
				IF DOES_ENTITY_EXIST(sConstructionWorkers[i].ped)
				AND NOT IS_PED_INJURED(sConstructionWorkers[i].ped)
					IF sConstructionWorkers[i].bCanSeePlayer
						DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(sConstructionWorkers[i].ped)+<<0,0,1>>, 0.1, 0,255,0)
					ELSE
						DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(sConstructionWorkers[i].ped)+<<0,0,1>>, 0.1, 255,0,0)
					ENDIF
				ENDIF
			ENDREPEAT
		#ENDIF
		
		iCurrentWorkerLosCheck++
		IF iCurrentWorkerLosCheck >= COUNT_OF(sConstructionWorkers)
			iCurrentWorkerLosCheck = 0
		ENDIF
		
	ELSE
	
		iCurrentWorkerLosCheck++
		IF iCurrentWorkerLosCheck >= COUNT_OF(sConstructionWorkers)
			iCurrentWorkerLosCheck = 0
		ENDIF
		
	ENDIF
	
ENDPROC

PROC REMOVE_ALL_WORKER_BLIPS()
	INT iTemp
	REPEAT COUNT_OF(sConstructionWorkers) iTemp
		IF DOES_BLIP_EXIST(sConstructionWorkers[iTemp].blip)
			REMOVE_BLIP(sConstructionWorkers[iTemp].blip)
		ENDIF
	ENDREPEAT
ENDPROC

INT iCurrentVar1,iCurrentVar2
INT iMaxVar1 = 4
INT iMaxVar2 = 7

PROC SET_CONSTRUCTION_WORKER_VARIATION(PED_INDEX PedIndex)

	IF GET_ENTITY_MODEL(PedIndex) = S_M_Y_CONSTRUCT_01
	
		IF iCurrentVar1 > iMaxVar1
			iCurrentVar1 = 0
		ENDIF
		
		SWITCH(iCurrentVar1)
		
			CASE 0
				//Hispanic guy jeans yellow vest
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
			BREAK
			
			CASE 1
				//Hispanic guy checkered shirt yellow vest
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 2, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
			BREAK
			
			CASE 2
				//Hispanic guy short shirt over long sleeves, orange shirt
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,8), 0, 1, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
			BREAK
			
			CASE 3
				//Hispanic guy in red with orange vest
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,8), 0, 1, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
			BREAK
			
			CASE 4
				//Hispanic guy mostly in black no vest
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
			BREAK
			
		ENDSWITCH
		
		iCurrentVar1++
		
	ELIF GET_ENTITY_MODEL(PedIndex) = S_M_Y_CONSTRUCT_02
	
		IF iCurrentVar2 > iMaxVar2
			iCurrentVar2 = 0
		ENDIF
		
		SWITCH(iCurrentVar2)
		
			CASE 0
				//black guy shorts orange vest
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 0, 2, 0) //(head)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
			BREAK
			
			CASE 1  
				//white guy shorts orange vest
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
			BREAK
			
			CASE 2  
				//white guy, blonde, shorts, orange vest
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
			BREAK
			
			CASE 3  
				//white gut boiler suit orange
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 1, 2, 0) //(head)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,8), 2, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,10), 1, 0, 0) //(decl)
			BREAK
			
			CASE 4  
				//black guy polo shirt no vest
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,8), 2, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
			BREAK
			
			CASE 5  
				//black guy blue boiler orange vest
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,10), 1, 0, 0) //(decl)
			BREAK
			
			CASE 6  
				//black guy shorts yellow vest
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,8), 0, 1, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
			BREAK
			
			CASE 7  
				//white guy blue shirt no vest
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,4), 0, 2, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,8), 2, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(PedIndex, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
			BREAK
			
		ENDSWITCH
		
		iCurrentVar2++
		
	ENDIF
	
ENDPROC

PROC CREATE_CONSTRUCTION_WORKER(CONSTRUCTION_WORKER_STRUCT &worker, MODEL_NAMES model)
	IF worker.bInitialised
		worker.ped = CREATE_PED(PEDTYPE_MISSION, model, worker.vStartPosition, worker.fStartHeading)
		SET_CONSTRUCTION_WORKER_VARIATION(worker.ped)
		IF model = S_M_Y_CONSTRUCT_01
			SET_PED_PROP_INDEX(worker.ped, ANCHOR_HEAD, 1, 2)
		ELSE
			SET_PED_PROP_INDEX(worker.ped, ANCHOR_HEAD, 0, 1)
		ENDIF
		SET_PED_KEEP_TASK(worker.ped, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(worker.ped, rgConstruction)
		SET_PED_COMBAT_ATTRIBUTES(worker.ped, CA_ALWAYS_FIGHT, TRUE)
		SET_PED_COMBAT_ATTRIBUTES(worker.ped, CA_USE_COVER, FALSE)
		SET_PED_FLEE_ATTRIBUTES(worker.ped, FA_NEVER_FLEE, TRUE)
		SET_PED_COMBAT_RANGE(worker.ped, CR_FAR)
		SET_ENTITY_HEALTH(worker.ped, GET_RANDOM_INT_IN_RANGE(125, 150))
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(worker.ped, TRUE)
	ENDIF
ENDPROC

PROC DESTROY_CONSTRUCTION_WORKER(CONSTRUCTION_WORKER_STRUCT &worker, BOOL bForced = FALSE)
	IF DOES_ENTITY_EXIST(worker.ped)
		IF bForced
			DELETE_PED(worker.ped)
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(worker.ped)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(worker.board)
		IF bForced
			DELETE_OBJECT(worker.board)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(worker.board)
		ENDIF
	ENDIF
ENDPROC

PROC DESTROY_ALL_WORKERS(BOOL bForced = FALSE)
	INT iTemp
	REPEAT COUNT_OF(sConstructionWorkers) iTemp
		DESTROY_CONSTRUCTION_WORKER(sConstructionWorkers[iTemp], bForced)
		sConstructionWorkers[iTemp].bInitialised = FALSE
		sConstructionWorkers[iTemp].bFlee = FALSE
		sConstructionWorkers[iTemp].bCombat = FALSE
	ENDREPEAT
ENDPROC

PROC INIT_CONSTRUCTION_WORKER(CONSTRUCTION_WORKER_STRUCT &worker, VECTOR vPos, FLOAT fHeading)
	DESTROY_CONSTRUCTION_WORKER(worker)
	worker.vStartPosition = vPos
	worker.fStartHeading = fHeading
	worker.bInitialised = TRUE
	worker.bSpottedPlayer = FALSE
	worker.fSpotTime = 0.0
	worker.bCombat = FALSE
	worker.bFlee = FALSE
ENDPROC

PROC FADE_IN(INT iTime = 500)
	IF NOT IS_SCREEN_FADED_IN()
		DO_SCREEN_FADE_IN(iTime)
		WHILE NOT IS_SCREEN_FADED_IN()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC FADE_OUT(INT iTime = 500)
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(iTime)
		WHILE NOT IS_SCREEN_FADED_OUT()
			WAIT(0)
		ENDWHILE
	ENDIF
ENDPROC

PROC REMOVE_BLIPS()
	IF DOES_BLIP_EXIST(blipArchitect)
		REMOVE_BLIP(blipArchitect)
	ENDIF
	IF DOES_BLIP_EXIST(blipSafeHouse)
		REMOVE_BLIP(blipSafeHouse)
	ENDIF
	IF DOES_BLIP_EXIST(blipPlans)
		REMOVE_BLIP(blipPlans)
	ENDIF
	IF DOES_BLIP_EXIST(blipLift)
		REMOVE_BLIP(blipLift)
	ENDIF
	IF DOES_BLIP_EXIST(blipParkUp)
		REMOVE_BLIP(blipParkUp)
	ENDIF
	INT iTemp
	REPEAT COUNT_OF(blipLifts) iTemp
		IF DOES_BLIP_EXIST(blipLifts[iTemp])
			REMOVE_BLIP(blipLifts[iTemp])
		ENDIF
	ENDREPEAT
ENDPROC

PROC RESET_VARS()
	INT iTemp
	REPEAT COUNT_OF(constructionLifts) iTemp
		constructionLifts[iTemp].iSoundID = -1
	ENDREPEAT
ENDPROC

PROC DO_FULL_RESET_FOR_SKIP()

	iStageProgress = 0
	REMOVE_BLIPS()
	DELETE_PED(pedArchitect)
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehPlayerCar)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehPlayerCar, <<2,0,0>>))
		ENDIF
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_ENTITY_ATTACHED(PLAYER_PED_ID())
		DETACH_ENTITY(PLAYER_PED_ID())
	ENDIF
	
	IF DOES_BLIP_EXIST(blipParkUp)
		REMOVE_BLIP(blipParkUp)
	ENDIF
	
	IF DOES_BLIP_EXIST(blipOffice)
		REMOVE_BLIP(blipOffice)
	ENDIF
	
	DESTROY_ALL_WORKERS(TRUE)
	
	INT iTemp
	REPEAT COUNT_OF(sConstructionWorkers) iTemp
		sConstructionWorkers[iTemp].bTasked = FALSE
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(objPlans)
		DELETE_OBJECT(objPlans)
	ENDIF
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	DELETE_VEHICLE(vehArchitectCar)
	IF DOES_ENTITY_EXIST(vehPlayerCar)
		IF IS_ENTITY_A_MISSION_ENTITY(vehPlayerCar)
			IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehPlayerCar)
				SET_ENTITY_AS_MISSION_ENTITY(vehPlayerCar, TRUE, TRUE)
			ENDIF
			DELETE_VEHICLE(vehPlayerCar)
		ENDIF
	ENDIF
	CLEAR_PRINTS()
	
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	KILL_ANY_CONVERSATION()
	HANG_UP_AND_PUT_AWAY_PHONE()
	
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
ENDPROC

//Cleanup
PROC MISSION_CLEANUP()

	IF NOT IS_PED_INJURED(pedArchitect)
		IF NOT IS_PED_FLEEING(pedArchitect)
			CLEAR_PED_TASKS(pedArchitect)
		ENDIF
	ENDIF
	
	SET_PED_AS_NO_LONGER_NEEDED(pedArchitect)
	IF DOES_ENTITY_EXIST(objPlans)
		DELETE_OBJECT(objPlans)
	ENDIF
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND IS_ENTITY_ATTACHED(PLAYER_PED_ID())
			DETACH_ENTITY(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbiSite)
	DISABLE_TAXI_HAILING(FALSE)
	
	INT iTemp
	REPEAT COUNT_OF(sConstructionWorkers) iTemp
		DESTROY_CONSTRUCTION_WORKER(sConstructionWorkers[iTemp])
	ENDREPEAT
	
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(hat_widget)
			DELETE_WIDGET_GROUP(hat_widget)
		ENDIF
	#ENDIF
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-107.924973,-1037.519409,28.776199>>+<<9.000000,14.250000,4.000000>>, <<-107.924973,-1037.519409,28.776199>>-<<9.000000,14.250000,4.000000>>, TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-219.009720,-1113.939819,24.486897>>+<<4.500000,4.500000,4.000000>>, <<-219.009720,-1113.939819,24.486897>>-<<4.500000,4.500000,4.000000>>, TRUE)
	RENDER_SCRIPT_CAMS(FALSE, TRUE)
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_stealth_kill_a"), TRUE)
	ACTION_MANAGER_ENABLE_ACTION(GET_HASH_KEY("ACT_stealth_kill_b"), TRUE)
	TRIGGER_MUSIC_EVENT("AH2A_MISSION_FAIL")
	REMOVE_RELATIONSHIP_GROUP(rgBuddies)
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_GAMEPLAY_HINT_ACTIVE()
		STOP_GAMEPLAY_HINT()
	ENDIF
	
	STOP_STREAM()
	
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_L), DOORSTATE_UNLOCKED, FALSE, TRUE)
	DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_R), DOORSTATE_UNLOCKED, FALSE, TRUE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_CAR), FALSE)
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC Mission_Passed()

	//Bind the player's car to global index so the heist controller can clean it up.
	g_sTriggerSceneAssets.veh[0] = vehPlayerCar
	
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	KILL_ANY_CONVERSATION()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	IF NOT IS_REPEAT_PLAY_ACTIVE()
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_PRIME_BOARD_TRANSITION, TRUE)
		Mission_Flow_Mission_Passed(TRUE)
	ELSE
		Mission_Flow_Mission_Passed()
	ENDIF
	
	Mission_Cleanup()
	
ENDPROC

//Mission Failed
PROC Mission_Failed(STRING reason)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		CLEAR_PED_TASKS(PLAYER_PED_ID())
	ENDIF
	
	IF DOES_ENTITY_EXIST(objHat)
		IF IS_ENTITY_ATTACHED(objHat)
			DETACH_ENTITY(objHat)
		ENDIF
	ENDIF
	
	IF IS_GAMEPLAY_HINT_ACTIVE()
		STOP_GAMEPLAY_HINT()
	ENDIF
	
	MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<706.9233, -965.4952, 29.4179>>, 282.8027)
	SET_REPLAY_REJECTED_CHARACTER(CHAR_MICHAEL)
	
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(reason)
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		INT iCountLifts	
		REPEAT COUNT_OF(constructionLifts) iCountLifts
			UPDATE_LIFT(constructionLifts[iCountLifts])
		ENDREPEAT
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		WAIT(0)
	ENDWHILE
	
	IF DOES_ENTITY_EXIST(vehPlayerCar)
		IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehPlayerCar, FALSE)
			SET_ENTITY_AS_MISSION_ENTITY(vehPlayerCar, TRUE, TRUE)
		ENDIF
		DELETE_VEHICLE(vehPlayerCar)
	ENDIF
	
	IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_BRIEFCASE_02)
		REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), WEAPONTYPE_BRIEFCASE_02)
	ENDIF
	
	CLEANUP_MISSION_VEHICLE_GEN_VEHICLE()
	
	IF IS_SCREEN_FADED_OUT()
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-76.3515, -1021.1342, 27.5053>>, 97.8560)
	ENDIF
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	
	DELETE_PED(pedArchitect)
	
	IF bPlayerhasCase
	AND mission_stage = STAGE_DRIVE_BACK
		IF DOES_ENTITY_EXIST(vehArchitectCar)
		AND IS_ENTITY_DEAD(vehArchitectCar)
			PRINTLN("Architects car grabbed")
			g_replay.iReplayInt[0] = 1	
		ENDIF
	ENDIF
	
	SET_VEHICLE_AS_NO_LONGER_NEEDED(vehArchitectCar)
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_ENTITY_ATTACHED(PLAYER_PED_ID())
		DETACH_ENTITY(PLAYER_PED_ID())
	ENDIF
	
	INT iTemp
	
	REPEAT COUNT_OF(sConstructionWorkers) iTemp
		IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
			DELETE_PED(sConstructionWorkers[iTemp].ped)
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(sConstructionWorkers) iTemp
		IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
			DELETE_PED(sConstructionWorkers[iTemp].ped)
		ENDIF
	ENDREPEAT
	
	CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
	
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_PRIME_BOARD_TRANSITION, FALSE)
	
	Mission_Cleanup()
	
ENDPROC

BOOL bTopFloorPedsAlerted = FALSE
BOOL bTopFloorPedsFlee = FALSE
INT iTimeTopFloorAlerted = 0

FUNC BOOL CHECK_TOP_FLOOR_SITE_FAILS()
	INT iWorkersAlive = 0
	INT iTemp
	
	REPEAT COUNT_OF(sConstructionWorkers) iTemp
		IF NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
			IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
				IF NOT IS_ENTITY_DEAD(sConstructionWorkers[iTemp].ped)
				AND NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
					iWorkersAlive++
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	PRINTLN("Workers alive ", iWorkersAlive)
	
	IF NOT bTopFloorPedsAlerted
	
		IF iWorkersAlive > 0
			REPEAT COUNT_OF(sConstructionWorkers) iTemp
				IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
				AND IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
					PRINTLN("TOP FLOOR SITE FAIL 1")
					bTopFloorPedsAlerted = TRUE
				ENDIF
				IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
				AND IS_ENTITY_DEAD(sConstructionWorkers[iTemp].ped)
					PRINTLN("TOP FLOOR SITE FAIL 2")
					bTopFloorPedsAlerted = TRUE
				ENDIF
				IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
				AND NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
					IF IS_EXPLOSION_IN_AREA(EXP_TAG_BZGAS, GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped) + <<5,5,3>>, GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped) - <<5,5,3>>)
					OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped) + <<5,5,3>>, GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped) - <<5,5,3>>, WEAPONTYPE_BZGAS)
					OR IS_EXPLOSION_IN_AREA(EXP_TAG_SMOKE_GRENADE, GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped) + <<5,5,3>>, GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped) - <<5,5,3>>)
					OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped) + <<5,5,3>>, GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped) - <<5,5,3>>, WEAPONTYPE_SMOKEGRENADE)
						PRINTLN("SITE FAIL 2.5")
						bTopFloorPedsAlerted = TRUE
					ENDIF
					IF sConstructionWorkers[iTemp].bCombat
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sConstructionWorkers[iTemp].ped, PLAYER_PED_ID())
						PRINTLN("SITE FAIL 3")
						bTopFloorPedsAlerted = TRUE
					ENDIF
					IF IS_PED_BEING_STEALTH_KILLED(sConstructionWorkers[iTemp].ped)
						PRINTLN("SITE FAIL 4")
						bTopFloorPedsAlerted = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF (IS_PED_SHOOTING(PLAYER_PED_ID()) AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE) AND GET_PLAYER_WEAPON() <> WEAPONTYPE_STUNGUN  AND NOT IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID()))
			OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADELAUNCHER, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<300,300,300>>, GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<300,300,300>>)
			OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADE, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<300,300,300>>, GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<300,300,300>>)
			OR IS_EXPLOSION_IN_AREA(EXP_TAG_ROCKET, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<300,300,300>>, GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<300,300,300>>)
			OR IS_EXPLOSION_IN_AREA(EXP_TAG_MOLOTOV, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<300,300,300>>, GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<300,300,300>>)
			OR IS_EXPLOSION_IN_AREA(EXP_TAG_STICKYBOMB, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<300,300,300>>, GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<300,300,300>>)
				PRINTLN("TOP FLOOR SITE FAIL 6")
				bTopFloorPedsAlerted = TRUE
				bTopFloorPedsFlee = TRUE
			ENDIF
			
			IF bTopFloorPedsAlerted
				IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_PROJECTILE | WF_INCLUDE_GUN)
					bTopFloorPedsFlee = TRUE
				ENDIF
			ENDIF
			
		ENDIF
		
	ELSE
	
		REPEAT COUNT_OF(sConstructionWorkers) iTemp
			IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
			AND NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
				IF NOT bTopFloorPedsFlee
					IF NOT IS_PED_IN_COMBAT(sConstructionWorkers[iTemp].ped)
						TASK_COMBAT_PED(sConstructionWorkers[iTemp].ped, PLAYER_PED_ID())
					ENDIF
				ELSE
					IF NOT IS_PED_FLEEING(sConstructionWorkers[iTemp].ped)
						TASK_REACT_AND_FLEE_PED(sConstructionWorkers[iTemp].ped, PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		
		IF iWorkersAlive > 0
			IF iTimeTopFloorAlerted  = 0
				iTimeTopFloorAlerted = GET_GAME_TIMER()
			ELIF GET_GAME_TIMER() - iTimeTopFloorAlerted > 2500
				RETURN TRUE
			ENDIF
		ENDIF
		
		IF iTimeTopFloorAlerted != 0
			PRINTLN("Time since alert", iTimeTopFloorAlerted = GET_GAME_TIMER())
		ENDIF
		
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL CHECK_SITE_FAILS(BOOL &bFlee)
	INT iTemp
	BOOL bFail
	REPEAT COUNT_OF(sConstructionWorkers) iTemp
		IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
		AND IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
			PRINTLN("SITE FAIL 1")
			bFail = TRUE
		ENDIF
		IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
		AND IS_ENTITY_DEAD(sConstructionWorkers[iTemp].ped)
			PRINTLN("SITE FAIL 2")
			bFail = TRUE
		ENDIF
		IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
		AND NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
			IF IS_EXPLOSION_IN_AREA(EXP_TAG_BZGAS, GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped) + <<5,5,3>>, GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped) - <<5,5,3>>)
			OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped) + <<5,5,3>>, GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped) - <<5,5,3>>, WEAPONTYPE_BZGAS)
			OR IS_EXPLOSION_IN_AREA(EXP_TAG_SMOKE_GRENADE, GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped) + <<5,5,3>>, GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped) - <<5,5,3>>)
			OR IS_PROJECTILE_TYPE_IN_AREA(GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped) + <<5,5,3>>, GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped) - <<5,5,3>>, WEAPONTYPE_SMOKEGRENADE)
				PRINTLN("SITE FAIL 2.5")
				bFail = TRUE
			ENDIF
			IF sConstructionWorkers[iTemp].bCombat
			OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sConstructionWorkers[iTemp].ped, PLAYER_PED_ID())
				PRINTLN("SITE FAIL 3")
				bFail = TRUE
			ENDIF
			IF IS_PED_BEING_STEALTH_KILLED(sConstructionWorkers[iTemp].ped)
				PRINTLN("SITE FAIL 4")
				bFail = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-116.823143,-1049.830933,22.229935>>, <<-186.375580,-1024.101563,37.391541>>, 23.250000)
		PRINTLN("SITE FAIL 5")
		bFail = TRUE
	ENDIF
	
	IF (IS_PED_SHOOTING(PLAYER_PED_ID()) AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE) AND GET_PLAYER_WEAPON() <> WEAPONTYPE_STUNGUN  AND NOT IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID()))
	OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADELAUNCHER, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<300,300,300>>, GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<300,300,300>>)
	OR IS_EXPLOSION_IN_AREA(EXP_TAG_GRENADE, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<300,300,300>>, GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<300,300,300>>)
	OR IS_EXPLOSION_IN_AREA(EXP_TAG_ROCKET, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<300,300,300>>, GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<300,300,300>>)
	OR IS_EXPLOSION_IN_AREA(EXP_TAG_MOLOTOV, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<300,300,300>>, GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<300,300,300>>)
	OR IS_EXPLOSION_IN_AREA(EXP_TAG_STICKYBOMB, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<300,300,300>>, GET_ENTITY_COORDS(PLAYER_PED_ID()) - <<300,300,300>>)
		PRINTLN("SITE FAIL 6")
		bFlee = TRUE
		bFail = TRUE
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehArchitectCar)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehArchitectCar)
			PRINTLN("SITE FAIL 7")
			bFail = TRUE
		ENDIF
		IF IS_VEHICLE_ALARM_ACTIVATED(vehArchitectCar)
		AND (VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vArchCarStart) < 100 OR VDIST2(GET_ENTITY_COORDS(vehPlayerCar), vArchCarStart) < 100)
			bFail = TRUE
		ENDIF
	ENDIF
	
	IF IS_PED_ON_SITE(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
		OR IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			IF vPlayerPos.z < 250
				PRINTLN("SITE FAIL 8")
				bFail = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bFail
	
ENDFUNC

FLOAT fLostTime

PROC HAS_PLAYER_LOST_ARCHITECT()
	IF DOES_ENTITY_EXIST(pedArchitect)
	AND NOT IS_PED_INJURED(pedArchitect)
		FLOAT fDist = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect))
		IF (fDist > 100*100 AND IS_ENTITY_OCCLUDED(pedArchitect))
		OR fDist > 250*250
			fLostTime += GET_FRAME_TIME()
		ELSE
			fLostTime = CLAMP(fLostTime - GET_FRAME_TIME()*2, 0, 5.0)
		ENDIF
		IF fLostTime > 3.0
			CLEAR_PRINTS()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			PRINTLN("\n\n\n FAIL HERE FOR LOSING \n\n\n")
			Mission_Failed("S3A_LOST")
		ENDIF
	ENDIF
	IF mission_stage = STAGE_GROUND_FLOOR
		IF NOT IS_PED_INJURED(pedArchitect)
			IF iTimeArchFirstFloor = -1
				VECTOR vArchHeightCheck = GET_ENTITY_COORDS(pedArchitect)
				IF vArchHeightCheck.z > 100
				AND NOT constructionLifts[LS_SOUTH_B].bInTransit
					iTimeArchFirstFloor = GET_GAME_TIMER()
				ENDIF
			ELSE
				IF (GET_GAME_TIMER() - iTimeArchFirstFloor) > 10000
				AND (NOT IS_ENTITY_IN_ANY_LIFT(PLAYER_PED_ID()) OR (IS_ENTITY_IN_ANY_LIFT(PLAYER_PED_ID()) AND NOT constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(PLAYER_PED_ID())].bInTransit))
					Mission_Failed("S3A_LOST")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_ARCHITECT_SEEN_PLAYER(BOOL bshowmessage = TRUE)

	BOOL bReturn
	
	IF DOES_ENTITY_EXIST(pedArchitect)
	AND NOT IS_PED_INJURED(pedArchitect)
	
		FLOAT minDist = 1.0
		IF CAN_ARCHITECT_SEE_PLAYER()
			minDist = 4.0
		ENDIF
		FLOAT fMAXLurk = 5.0
		
		FLOAT distFromJan = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect))
		
		BOOL bInSeperateLifts
		bInSeperateLifts = 	(IS_ENTITY_IN_ANY_LIFT(pedArchitect)AND NOT IS_ENTITY_IN_ANY_LIFT(PLAYER_PED_ID()))
							OR (IS_ENTITY_IN_ANY_LIFT(PLAYER_PED_ID()) AND IS_ENTITY_IN_ANY_LIFT(pedArchitect) AND GET_LIFT_INDEX_ENTITY_IS_IN(pedArchitect) <> GET_LIFT_INDEX_ENTITY_IS_IN(PLAYER_PED_ID()))
							
		IF distFromJan < minDist
		AND NOT bInSeperateLifts
			IF bshowmessage
				IF NOT IS_PED_PERFORMING_MELEE_ACTION(PLAYER_PED_ID())
					PRINT_NOW("S3A_BACKUP", 1000, 1)
				ENDIF
			ENDIF
			fTotalLurkingTime += GET_FRAME_TIME()
		ELSE
			fTotalLurkingTime = 0
		ENDIF
		
		IF IS_ENTITY_IN_ANY_LIFT(pedArchitect)
		AND IS_ENTITY_IN_ANY_LIFT(PLAYER_PED_ID())
		AND GET_LIFT_INDEX_ENTITY_IS_IN(PLAYER_PED_ID()) = GET_LIFT_INDEX_ENTITY_IS_IN(pedArchitect)
			constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(pedArchitect)].bInTransit = FALSE
			bReturn = TRUE
		ENDIF
		
		IF IS_ENTITY_AT_COORD(pedArchitect, GET_ENTITY_COORDS(PLAYER_PED_ID()), <<3,3,3>>)
			IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedArchitect)
				bReturn = TRUE
			ENDIF
		ENDIF
		
		IF fTotalLurkingTime > fMAXLurk
			PRINTLN("LURKING!!")
			bReturn = TRUE
		ENDIF
		
	ENDIF
	
	RETURN bReturn
	
ENDFUNC

INT iLOSSpookCheck

FUNC BOOL HAS_PLAYER_SPOOKED_ARCHITECT()

	BOOL bReturn = FALSE
	
	IF NOT bSkip
	
		INT iTemp
		
		IF NOT IS_PED_INJURED(pedArchitect)
		AND NOT IS_ENTITY_DEAD(pedArchitect)
			REPEAT COUNT_OF(sConstructionWorkers) iTemp
				IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
					IF IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
					OR IS_ENTITY_DEAD(sConstructionWorkers[iTemp].ped)
					OR (NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped) AND IS_PED_FLEEING(sConstructionWorkers[iTemp].ped))
						IF VDIST2(GET_ENTITY_COORDS(pedArchitect), GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped)) < 30*30
							IF GET_GAME_TIMER() - iLOSSpookCheck > 500
								IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(pedArchitect, sConstructionWorkers[iTemp].ped)
									bReturn = TRUE
								ENDIF
							ENDIF
							iLOSSpookCheck = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedArchitect, PLAYER_PED_ID())
				bReturn = TRUE
			ENDIF
			
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 3*3
				IF IS_PED_RAGDOLL(pedArchitect)
					bReturn = TRUE
				ENDIF
			ENDIF
			
		ENDIF
		
//		IF IS_ENTITY_DEAD(pedArchitect)
//		OR IS_PED_INJURED(pedArchitect)
//			GO_TO_STAGE(STAGE_COVER_BLOWN)
//		ENDIF
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bReturn
			PRINTLN("\n\nSPOOKED ARCHITECT\n\n")
		ENDIF
	#ENDIF
	RETURN bReturn
	
ENDFUNC

FUNC BOOL CREATE_ARCHITECT(VECTOR position, FLOAT heading)

	IF NOT DOES_ENTITY_EXIST(pedArchitect)
	OR IS_PED_INJURED(pedArchitect)
	
		IF IS_PED_INJURED(pedArchitect)
			DELETE_PED(pedArchitect)
		ENDIF
		
		REQUEST_MODEL(u_m_m_fibarchitect)
		REQUEST_MODEL(PROP_HARD_HAT_01)
		IF HAS_MODEL_LOADED(u_m_m_fibarchitect)
		AND HAS_MODEL_LOADED(PROP_HARD_HAT_01)
			pedArchitect = CREATE_PED(PEDTYPE_MISSION, u_m_m_fibarchitect, position, heading)
			ADD_PED_FOR_DIALOGUE(sSpeech, 1, pedArchitect, "FIBArchitect")
		ENDIF
		
	ENDIF
	
	IF NOT IS_PED_INJURED(pedArchitect)
	
		IF IS_ENTITY_ATTACHED(pedArchitect)
			DETACH_ENTITY(pedArchitect)
		ENDIF
		
		SET_ENTITY_COORDS(pedArchitect, position)
		SET_ENTITY_HEADING(pedArchitect, heading)
		
		SET_PED_COMBAT_ATTRIBUTES(pedArchitect, CA_ALWAYS_FLEE, TRUE)
		SET_PED_FLEE_ATTRIBUTES(pedArchitect, FA_USE_VEHICLE, FALSE)
		SET_PED_FLEE_ATTRIBUTES(pedArchitect, FA_PREFER_PAVEMENTS, TRUE)
		SET_PED_FLEE_ATTRIBUTES(pedArchitect, FA_DISABLE_HANDS_UP, TRUE)
		SET_PED_FLEE_ATTRIBUTES(pedArchitect, FA_WANDER_AT_END, TRUE)
		
		SET_PED_COMPONENT_VARIATION(pedArchitect, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedArchitect, INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedArchitect, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedArchitect, INT_TO_ENUM(PED_COMPONENT,4), 1, 2, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedArchitect, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
		
		STOP_PED_SPEAKING(pedArchitect, TRUE)
		SET_PED_PROP_INDEX(pedArchitect, ANCHOR_HEAD, 0, 0)
		SET_PED_PROP_INDEX(pedArchitect, ANCHOR_EYES, 0, 0)
		SET_PED_HEARING_RANGE(pedArchitect, 5)
		SET_ENTITY_IS_TARGET_PRIORITY(pedArchitect, TRUE)
		SET_ENTITY_HEALTH(pedArchitect, 110)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedArchitect, TRUE)
		SET_PED_DROPS_WEAPONS_WHEN_DEAD(pedArchitect, FALSE)
		SET_PED_DROPS_WEAPON(pedArchitect)
		SET_PED_VISUAL_FIELD_MIN_ANGLE(pedArchitect, -60)
		SET_PED_VISUAL_FIELD_MAX_ANGLE(pedArchitect, 60)
		SET_ENTITY_LOAD_COLLISION_FLAG(pedArchitect, TRUE)
		SET_PED_LOD_MULTIPLIER(pedArchitect, 2.0)
		
	ENDIF
	
	RETURN DOES_ENTITY_EXIST(pedArchitect) AND NOT IS_PED_INJURED(pedArchitect)
	
ENDFUNC

PROC LOSE_THE_COPS()

	IF mission_stage <> STAGE_LOSE_COPS
		eSavedStage = mission_stage
		mission_stage = STAGE_LOSE_COPS
		CLEAR_PRINTS()
		KILL_ANY_CONVERSATION()
		REMOVE_BLIP(blipOffice)
		SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
		mission_stage = eSavedStage
		CLEAR_PRINTS()
	ENDIF
	
ENDPROC

BOOL bMikeAdded = FALSE

PROC CHECK_DIALOGUE_PEDS()
	IF NOT DOES_ENTITY_EXIST(sSpeech.PedInfo[0].Index)
		ADD_PED_FOR_DIALOGUE(sSpeech, 0, PLAYER_PED_ID(), "FRANKLIN")
	ENDIF
	IF NOT bMikeAdded
		ADD_PED_FOR_DIALOGUE(sSpeech, 3, NULL, "MICHAEL")
		bMikeAdded = TRUE
	ENDIF
	IF NOT DOES_ENTITY_EXIST(sSpeech.PedInfo[1].Index)
	AND NOT IS_PED_INJURED(pedArchitect)
		ADD_PED_FOR_DIALOGUE(sSpeech, 1, pedArchitect, "FIBArchitect")
	ENDIF
ENDPROC

PROC SET_ALL_CONSTRUCTION_SCENARIOS_BLOCKED()
	sbiSite = ADD_SCENARIO_BLOCKING_AREA(<<-151.703125,-1025.596558,73.762939>> + <<131.500000,148.750000,61.500000>>, <<-151.703125,-1025.596558,73.762939>> - <<131.500000,148.750000,61.500000>>)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-107.924973,-1037.519409,28.776199>>+<<20.000000,24.250000,10.000000>>, <<-107.924973,-1037.519409,28.776199>>-<<20.000000,24.250000,10.000000>>, FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-219.009720,-1113.939819,24.486897>>+<<4.500000,4.500000,4.000000>>, <<-219.009720,-1113.939819,24.486897>>-<<4.500000,4.500000,4.000000>>, FALSE)
ENDPROC

PROC UNBLOCK_ALL_CONSTRUCTION_SCENARIOS()
	REMOVE_SCENARIO_BLOCKING_AREA(sbiSite)
	SET_SCENARIO_PEDS_SPAWN_IN_SPHERE_AREA(vArchCarStart, 200, 30)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-107.924973,-1037.519409,28.776199>>+<<20.000000,24.250000,10.000000>>, <<-107.924973,-1037.519409,28.776199>>-<<20.000000,24.250000,10.000000>>, TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-219.009720,-1113.939819,24.486897>>+<<4.500000,4.500000,4.000000>>, <<-219.009720,-1113.939819,24.486897>>-<<4.500000,4.500000,4.000000>>, TRUE)
ENDPROC

PROC DO_ARCHITECT_CONTINUALLY_FACES_PLAYER()
	IF NOT IS_PED_INJURED(pedArchitect)
		IF NOT IS_PED_FACING_PED(pedArchitect, PLAYER_PED_ID(), 60)
			IF GET_SCRIPT_TASK_STATUS(pedArchitect, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
				TASK_LOOK_AT_ENTITY(pedArchitect, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
				OPEN_SEQUENCE_TASK(seqTurnTask)
				TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
				TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
				CLOSE_SEQUENCE_TASK(seqTurnTask)
				TASK_PERFORM_SEQUENCE(pedArchitect, seqTurnTask)
				CLEAR_SEQUENCE_TASK(seqTurnTask)
			ENDIF
		ELSE
			IF GET_SCRIPT_TASK_STATUS(pedArchitect, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
				IF NOT IS_PED_INJURED(pedArchitect)
					TASK_LOOK_AT_ENTITY(pedArchitect, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


#IF IS_DEBUG_BUILD
	VECTOR vTarget
	FLOAT fTraject
	FLOAT u
#ENDIF
FLOAT G = 9.8
FLOAT t

PROC SET_VECTOR_MAGNITUDE(VECTOR &v, FLOAT m)
	VECTOR n = NORMALISE_VECTOR(v)
	n.x = n.x * m
	n.y = n.y * m
	n.z = n.z * m
	v = n
ENDPROC

FUNC FLOAT GET_ANGLE_OF_TRAJECTORY(VECTOR origin, VECTOR target, FLOAT v, BOOL bSmallestAngle = TRUE)
	VECTOR vDiff = target - origin
	FLOAT y = vDiff.z
	FLOAT x = SQRT((vDiff.x*vDiff.x) + (vDiff.y*vDiff.y))
	FLOAT s = (v * v * v * v) - G * (G * (x * x) + (2 * y * (v * v))) //substitution
	FLOAT o
	IF bSmallestAngle
		o = ATAN(  (((v * v) - SQRT(s)) / (G * x))   ) // launch angle
	ELSE
		o = ATAN(  (((v * v) + SQRT(s)) / (G * x))   ) // launch angle
	ENDIF
	RETURN o
ENDFUNC

PROC LAUNCH_HAT(PED_INDEX pedThrower, VECTOR vTargetCoord, FLOAT &flightTime)
	VECTOR vOriginCoord = GET_PED_BONE_COORDS(pedThrower, BONETAG_PH_R_HAND, <<0,0,0>>)
	VECTOR vDiff
	vDiff = vTargetCoord - vOriginCoord
	FLOAT initialVel = SQRT( VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedThrower))*10)
	IF vDiff.z > 0
		initialVel += absf(vDiff.z)
	ENDIF
	FLOAT fTrajectory = GET_ANGLE_OF_TRAJECTORY(vOriginCoord, vTargetCoord, initialVel)
	vDiff.z = 0
	VECTOR vHatVel
	vHatVel = vDiff
	SET_VECTOR_MAGNITUDE(vHatVel, initialVel*COS(fTrajectory))
	vHatVel.z = initialVel*SIN(fTrajectory)
	flightTime = VMAG(vDiff)/(initialVel*cos(fTrajectory))
	DETACH_ENTITY(objHat, FALSE, TRUE)
	SET_OBJECT_PHYSICS_PARAMS(objHat, 1.0, 1.0, <<0,0,0>>, <<0,0,0>>)
	SET_DAMPING(objHat, PHYSICS_DAMPING_LINEAR_C, 0.0)
	SET_DAMPING(objHat, PHYSICS_DAMPING_LINEAR_V, 0.0)
	SET_DAMPING(objHat, PHYSICS_DAMPING_LINEAR_V2, 0.0)
	SET_DAMPING(objHat, PHYSICS_DAMPING_ANGULAR_C, 0.0)
	SET_DAMPING(objHat, PHYSICS_DAMPING_ANGULAR_V, 0.0)
	SET_DAMPING(objHat, PHYSICS_DAMPING_ANGULAR_V2, 0.0)
	SET_ENTITY_VELOCITY(objHat, vHatVel)
ENDPROC

FUNC BOOL HAS_HAT_BEEN_CAUGHT(FLOAT flightTime, /*VECTOR vTargetCoords,*/ INT iCurrentTime)
//	VECTOR vThisPos
//	VECTOR vNextPos
//	vThisPos = GET_ENTITY_COORDS(objHat)
//	vNextPos = vThisPos + (GET_ENTITY_VELOCITY(objHat) * GET_FRAME_TIME())
	IF iCurrentTime > (flightTime*1000)+200
	//OR (iCurrentTime > (flightTime*1000)/2 AND vNextPos.z < vTargetCoords.z - 0.2 AND vNextPos.z < vThisPos.z)
		ATTACH_ENTITY_TO_ENTITY(objHat, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

INT iHatThrowScene
VECTOR vHatScenePosition = << -93.075, -1031.653, 27.075 >>
VECTOR vHatSceneRotation = << -0.000, 0.000, 13.680 >>


//**********************************************************************************************************************************************

/// ARCHITECT FLEEING THE SITE

//**********************************************************************************************************************************************

VECTOR vTopFLoorRunPath[8]
VECTOR vGroundFLoorRunPath1[8]
VECTOR vGroundFLoorRunPath2[6]
INT iTargetNode
INT iCanForceArchTask = 0

VECTOR vLift1AreaTopFloorPos1 = <<-186.167801,-1010.032349,113.349190>>
VECTOR vLift1AreaTopFloorPos2 = <<-177.510452,-1013.212952,117.388771>> 
FLOAT fLift1AreaWidth = 16.500000

VECTOR vLift2AreaTopFloorPos1 = <<-157.975586,-948.552307,112.698914>>
VECTOR vLift2AreaTopFloorPos2 = <<-154.218765,-938.141968,117.188484>> 
FLOAT fLift2AreaWidth = 17.750000

SEQUENCE_INDEX seqEnterLift
INT iTargetLift = -1
BOOL bIgnoreLiftAreaCheck = FALSE

PROC INIT_FLEE_ROUTES()
	
	vGroundFLoorRunPath1[0] = <<-129.73230, -944.42224, 28.29400>>
	vGroundFLoorRunPath1[1] = <<-158.76277, -934.15765, 28.29401>>
	vGroundFLoorRunPath1[2] = <<-162.97496, -938.78796, 28.28928>>
	vGroundFLoorRunPath1[3] = <<-164.24478, -942.71362, 28.28928>>
	vGroundFLoorRunPath1[4] = <<-189.44942, -1018.85925, 28.28928>>
	vGroundFLoorRunPath1[5] = <<-185.72169, -1020.86487, 28.28928>>
	vGroundFLoorRunPath1[6] = <<-182.14116, -1022.12451, 28.28928>>
	vGroundFLoorRunPath1[7] = <<-187.19696, -1035.60522, 26.15204>>
	
	vGroundFLoorRunPath2[0] = <<-78.36512, -1023.82281, 27.41733>>
	vGroundFLoorRunPath2[1] = <<-94.58283, -1030.61694, 26.58587>>
	vGroundFLoorRunPath2[2] = <<-128.50769, -1044.73572, 26.27405>>
	vGroundFLoorRunPath2[3] = <<-175.81627, -1035.86560, 26.27405>>
	vGroundFLoorRunPath2[4] = <<-187.62428, -1043.72839, 25.48483>>
	vGroundFLoorRunPath2[5] = <<-222.12471, -1132.41418, 22.01625>>
	
	vTopFLoorRunPath[0] = <<-180.32860, -1009.96014, 113.13877>>
	vTopFLoorRunPath[1] = <<-176.04013, -998.02661, 113.13877>>
	vTopFLoorRunPath[2] = <<-155.29431, -998.74628, 113.13877>>
	vTopFLoorRunPath[3] = <<-147.73753, -985.44556, 113.13877>>
	vTopFLoorRunPath[4] = <<-139.04646, -970.24915, 113.13877>>
	vTopFLoorRunPath[5] = <<-148.79703, -958.01074, 113.13877>>
	vTopFLoorRunPath[6] = <<-147.45317, -949.20422, 113.60802>>
	vTopFLoorRunPath[7] = <<-153.55850, -944.00726, 113.15311>>

ENDPROC

PROC RESET_FLEE_STATE()

	CLEAR_SEQUENCE_TASK(seqEnterLift)
	CLEAR_PED_TASKS(pedArchitect)
	iTargetLift = -1
	iCanForceArchTask = -3
	bIgnoreLiftAreaCheck = TRUE
	bArchCowering = FALSE
	
ENDPROC

PROC REVERSE_PATH(VECTOR &vPath[])

	INT iCountOfPath = COUNT_OF(vPath)
	INT i
	INT i_max
	
	IF i_max % 2 != 0
		i_max = ROUND(TO_FLOAT(iCountOfPath)/2.0)-1
	ELSE
		i_max = ROUND(TO_FLOAT(iCountOfPath)/2.0)
	ENDIF
	
	FOR i = 0 TO i_max-1
	
		VECTOR vTemp = vPath[i]
		vPath[i] = vPath[iCountOfPath-1-i]
		vPath[iCountOfPath-1-i] = vTemp
		
	ENDFOR
	
	IF NOT IS_PED_INJURED(pedArchitect)
		CLEAR_PED_TASKS(pedArchitect)
	ENDIF
	
ENDPROC

FUNC INT GET_CLOSEST_PATH_SECTION(VECTOR point, VECTOR &vPath[])
	INT iTemp
	INT iClosest
	FLOAT closest_distance = 99999
	REPEAT COUNT_OF(vPath)-1 iTemp
		IF GET_DISTANCE_BETWEEN_COORDS(GET_CLOSEST_POINT_ON_LINE(point, vPath[iTemp], vPath[iTemp+1]), point) < closest_distance
			iClosest = itemp
			closest_distance = VDIST(GET_CLOSEST_POINT_ON_LINE(point, vPath[iTemp], vPath[iTemp+1]), point)
		ENDIF
	ENDREPEAT
	RETURN iClosest
ENDFUNC

FUNC FLOAT GET_PROGRESS_ALONG_PATH(VECTOR vPos, VECTOR &vPath[])
	INT iNearNode = GET_CLOSEST_PATH_SECTION(vPos, vPath)
	IF iNearNode >= 0 AND iNearNode < COUNT_OF(vPath)
		FLOAT fProg = TO_FLOAT(iNearNode)
		IF iNearNode < (COUNT_OF(vPath)-1)
			fProg += GET_RATIO_OF_CLOSEST_POINT_ON_LINE(vPos, vPath[iNearNode], vPath[iNearNode+1])
		ENDIF
		RETURN fProg
	ENDIF
	RETURN -1.0
ENDFUNC

FUNC VECTOR GET_CLOSEST_PATH_NODE_POSITION(VECTOR &vPath[])
	FLOAT fClosestDist = 9999999
	INT iClosestIndex = 0
	INT i
	REPEAT COUNT_OF(vPath) i
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPath[i]) < fClosestDist
			iClosestIndex = i
			fClosestDist = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPath[i])
		ENDIF
	ENDREPEAT
	RETURN vPath[iClosestIndex]
ENDFUNC

PROC FOLLOW_PATH(VECTOR &vPath[])

	IF NOT IS_PED_INJURED(pedArchitect)
	
		INT iLastNode = COUNT_OF(vPath)-1
		
		IF GET_SCRIPT_TASK_STATUS(pedArchitect, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
		AND GET_SCRIPT_TASK_STATUS(pedArchitect, SCRIPT_TASK_GO_STRAIGHT_TO_COORD) != PERFORMING_TASK
			INT iNextNode = GET_CLOSEST_PATH_SECTION(GET_ENTITY_COORDS(pedArchitect), vPath) + 1
			IF iNextNode >= 0 AND iNextNode < COUNT_OF(vPath)
				iTargetNode = -1
				IF VDIST2(GET_ENTITY_COORDS(pedArchitect), vPath[iNextNode]) > 4*4
					iTargetNode = iNextNode
				ELSE
					IF iNextNode < (iLastNode)
						iTargetNode = iNextNode+1
					ENDIF
				ENDIF
				IF iTargetNode >= 0
					IF iCanForceArchTask >= 0
						IF IS_ENTITY_IN_ANY_LIFT(pedArchitect)
							iTargetNode = -1
							TASK_GO_STRAIGHT_TO_COORD(pedArchitect, GET_CLOSEST_PATH_NODE_POSITION(vPath), PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP)
						ELSE
							TASK_FOLLOW_NAV_MESH_TO_COORD(pedArchitect, vPath[iTargetNode], PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP, 0.05, ENAV_NO_STOPPING)
						ENDIF
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedArchitect)
						iCanForceArchTask = -2
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF iTargetNode >= 0 AND iTargetNode < COUNT_OF(vPath)
				IF VDIST2(GET_ENTITY_COORDS(pedArchitect), vPath[iTargetNode]) < 3*3
					CLEAR_PED_TASKS(pedArchitect)
				ENDIF
			ENDIF
		ENDIF
		
		IF iCanForceArchTask < 0
			iCanForceArchTask++
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC INT GET_CLOSEST_LIFT_INDEX(VECTOR vPos)
	FLOAT fClosest = 9999999
	INT iClosest = -1
	INT iTemp
	REPEAT COUNT_OF(constructionLifts) iTemp
		IF constructionLifts[iTemp].bInTransit = FALSE
		AND constructionLifts[iTemp].fCurrentState = 1.0
			FLOAT fDist = VDIST2(GET_ENTITY_COORDS(constructionLifts[iTemp].obj), vPos)
			IF fDist < fClosest 
				fClosest = fDist
				iClosest = iTemp
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN iClosest
ENDFUNC

#IF IS_DEBUG_BUILD

	PROC DRAW_PATH(VECTOR &vPath[], BOOL bAddCLosestPoints)
	
		INT iDrawDebugLines
		REPEAT COUNT_OF(vPath) iDrawDebugLines
			IF iDrawDebugLines < COUNT_OF(vPath) - 1
				DRAW_DEBUG_LINE(vPath[iDrawDebugLines] + <<0,0,1>>, vPath[iDrawDebugLines+1] + <<0,0,1>>, 0,0,255)
			ENDIF
			DRAW_DEBUG_TEXT(GET_STRING_FROM_INT(iDrawDebugLines), vPath[iDrawDebugLines] + <<0,0,1.5>>, 0,0,255)
			DRAW_DEBUG_LINE(vPath[iDrawDebugLines] + <<0,0,1>>, vPath[iDrawDebugLines] + <<0,0,1.5>>, 0,0,255)
		ENDREPEAT
		
		IF bAddCLosestPoints
		
			INT iClosestPath = GET_CLOSEST_PATH_SECTION(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPath)
			VECTOR vClosestPoint = GET_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vPath[iClosestPath], vPath[iClosestPath+1])
			DRAW_DEBUG_SPHERE(vClosestPoint + <<0,0,1>>, 0.1, 255,0,0)
			DRAW_DEBUG_TEXT("Player's path progress", vClosestPoint + <<0,0,1.23>>, 255,0,0)
			DRAW_DEBUG_LINE(vClosestPoint + <<0,0,1>>, vClosestPoint + <<0,0,1.3>>, 255,0,0)
			
			iClosestPath = GET_CLOSEST_PATH_SECTION(GET_ENTITY_COORDS(pedArchitect), vPath)
			vClosestPoint = GET_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS(pedArchitect), vPath[iClosestPath], vPath[iClosestPath+1])
			DRAW_DEBUG_SPHERE(vClosestPoint + <<0,0,1>>, 0.1, 0,255,0)
			DRAW_DEBUG_TEXT("Architect's path progress", vClosestPoint + <<0,0,1.2>>, 0,255,0)
			DRAW_DEBUG_LINE(vClosestPoint + <<0,0,1>>, vClosestPoint + <<0,0,1.3>>, 0,255,0)
			
			IF iTargetNode <> -1
			AND iTargetNode < COUNT_OF(vPath)
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(pedArchitect), vPath[iTargetNode] + <<0,0,1.05>>, 0,255,0)
				DRAW_DEBUG_SPHERE(vPath[iTargetNode] + <<0,0,1.05>>, 0.05, 0,255,0)
			ENDIF
			
		ENDIF
		
	ENDPROC
	
#ENDIF

PROC DO_ARCH_CRIES_FOR_HELP()
	IF GET_GAME_TIMER() - iTimeOfLastChaseChat > 3000
	AND NOT IS_MESSAGE_BEING_DISPLAYED()
		IF DO_ARCHITECT_FLEEING_LINE("F3A_JANRUN", "")
			iTimeOfLastChaseChat = GET_GAME_TIMER()
		ENDIF
	ENDIF
ENDPROC

PROC ALERT_WHOLE_SITE(BOOL bFlee = FALSE)
	IF eSiteState != AH2_SS_FLEEING_PLAYER
	AND eSiteState != AH2_SS_ATTACKING_PLAYER
		IF NOT bFlee
			PRINTLN("SETTING TO ATTACK PLAYER")
			eSiteState = AH2_SS_ATTACKING_PLAYER
		ELSE
			PRINTLN("SETTING TO FLEE PLAYER")
			eSiteState = AH2_SS_FLEEING_PLAYER
		ENDIF
	ENDIF
	IF NOT bPlayerhasCase
		GO_TO_STAGE(STAGE_COVER_BLOWN)
	ENDIF
	PRINTLN("ALERTING WHOLE SITE!")
ENDPROC

PROC MANAGE_FLEEING_ARCHITECT()

	IF NOT IS_PED_INJURED(pedArchitect)
	
		IF bMugged
		
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) > 30*30
				eMugStage = AH2_MSE_MUGGING_BLOWN
				bMugged = FALSE
			ENDIF
			
		ELSE
		
			IF IS_PED_ON_SITE(pedArchitect)
				VECTOR vTemp = GET_ENTITY_COORDS(pedArchitect)
				IF eRunawayState < AH2_RS_GOING_DOWN
					IF vTemp.z < 50
						eRunawayState = AH2_RS_GOING_DOWN
					ENDIF
				ELIF eRunawayState > AH2_RS_GOING_DOWN
					IF vTemp.z > 50
					AND NOT IS_ENTITY_IN_ANY_LIFT(pedArchitect)
						eRunawayState = AH2_RS_FOLLOW_PATH_TOP_FLOOR
					ENDIF
				ENDIF
			ENDIF
			
			IF eSiteState = AH2_SS_ALL_QUIET
				INT iTemp
				REPEAT COUNT_OF(sConstructionWorkers) iTemp
					IF NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
					AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped)) < 30*30
					AND DO_ARCHITECT_FLEEING_LINE("F3A_JANRUN", "")
						ALERT_WHOLE_SITE()
					ENDIF
				ENDREPEAT
			ENDIF
			
			SWITCH eRunawayState
			
				CASE AH2_RS_FOLLOW_PATH_TOP_FLOOR
				
					IF GET_PROGRESS_ALONG_PATH(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTopFLoorRunPath) >= GET_PROGRESS_ALONG_PATH(GET_ENTITY_COORDS(pedArchitect), vTopFLoorRunPath)
					AND GET_PROGRESS_ALONG_PATH(GET_ENTITY_COORDS(pedArchitect), vTopFLoorRunPath) > 0
						REVERSE_PATH(vTopFLoorRunPath)				
					ENDIF
					
					FOLLOW_PATH(vTopFLoorRunPath)
					
					#IF IS_DEBUG_BUILD
						DRAW_PATH(vTopFLoorRunPath, TRUE)
					#ENDIF
					
					IF NOT bIgnoreLiftAreaCheck
						IF IS_ENTITY_IN_ANGLED_AREA(pedArchitect, vLift1AreaTopFloorPos1, vLift1AreaTopFloorPos2, fLift1AreaWidth)
						OR IS_ENTITY_IN_ANGLED_AREA(pedArchitect, vLift2AreaTopFloorPos1, vLift2AreaTopFloorPos2, fLift2AreaWidth)
							eRunawayState = AH2_RS_NEAR_LIFT
							iTargetLift = -1
						ENDIF
					ELSE
						IF NOT IS_ENTITY_IN_ANGLED_AREA(pedArchitect, vLift1AreaTopFloorPos1, vLift1AreaTopFloorPos2, fLift1AreaWidth)
						AND NOT IS_ENTITY_IN_ANGLED_AREA(pedArchitect, vLift2AreaTopFloorPos1, vLift2AreaTopFloorPos2, fLift2AreaWidth)
							IF NOT IS_ENTITY_IN_ANY_LIFT(pedArchitect)
								bIgnoreLiftAreaCheck = FALSE
							ENDIF
						ELSE
							IF GET_PROGRESS_ALONG_PATH(GET_ENTITY_COORDS(PLAYER_PED_ID()), vTopFLoorRunPath) > GET_PROGRESS_ALONG_PATH(GET_ENTITY_COORDS(pedArchitect), vTopFLoorRunPath)
								bIgnoreLiftAreaCheck = FALSE
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE AH2_RS_NEAR_LIFT
					IF NOT IS_ENTITY_IN_ANY_LIFT(pedArchitect)
						IF GET_SCRIPT_TASK_STATUS(pedArchitect, SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
							iTargetLift = GET_CLOSEST_LIFT_INDEX(GET_ENTITY_COORDS(pedArchitect))
							IF iTargetLift <> -1
								OPEN_SEQUENCE_TASK(seqEnterLift)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vTopFloorLiftApproach[iTargetLift], PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT, ENAV_NO_STOPPING)
									TASK_GO_STRAIGHT_TO_COORD(NULL, vTopFloorLiftStandPos[iTargetLift], PEDMOVEBLENDRATIO_RUN, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_FINAL_HEADING, 1.5)
									TASK_TURN_PED_TO_FACE_COORD(NULL,  <<-157.0022, -941.1864, 113.2076>>, 0)
								CLOSE_SEQUENCE_TASK(seqEnterLift)
								TASK_PERFORM_SEQUENCE(pedArchitect, seqEnterLift)
								CLEAR_SEQUENCE_TASK(seqEnterLift)
							ENDIF
						ELSE
							IF iTargetLift <> -1
								IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(constructionLifts[iTargetLift].obj)) >  VDIST2(GET_ENTITY_COORDS(pedArchitect), GET_ENTITY_COORDS(constructionLifts[iTargetLift].obj))
									IF IS_ENTITY_IN_LIFT(constructionLifts[iTargetLift],  PLAYER_PED_ID())
										RESET_FLEE_STATE()
									ENDIF
								ELSE
									eRunawayState = AH2_RS_FOLLOW_PATH_TOP_FLOOR
								ENDIF
							ENDIF
						ENDIF
					ELSE
						eRunawayState = AH2_RS_IN_LIFT
					ENDIF
				BREAK
				
				CASE AH2_RS_IN_LIFT
					IF IS_ENTITY_IN_ANY_LIFT(pedArchitect)
					AND GET_SCRIPT_TASK_STATUS(pedArchitect, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
						IF NOT IS_ENTITY_IN_LIFT(constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(pedArchitect)], PLAYER_PED_ID())
						AND NOT IS_POSITION_NEAR_LIFT(GET_LIFT_INDEX_ENTITY_IS_IN(pedArchitect), GET_ENTITY_COORDS(PLAYER_PED_ID()))
							IF NOT constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(pedArchitect)].bInTransit
								bArchCowering = FALSE
								CLEAR_PED_TASKS(pedArchitect)
								constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(pedArchitect)].bActive = TRUE
								PREPARE_LIFT_FOR_PED_TRANSIT(constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(pedArchitect)], pedArchitect)
								eRunawayState = AH2_RS_GOING_DOWN
							ENDIF
						ELSE
							IF IS_ENTITY_IN_LIFT(constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(pedArchitect)], PLAYER_PED_ID())					
								RESET_FLEE_STATE()
								eRunawayState = AH2_RS_FOLLOW_PATH_TOP_FLOOR
							ELIF IS_POSITION_NEAR_LIFT(GET_LIFT_INDEX_ENTITY_IS_IN(pedArchitect), GET_ENTITY_COORDS(PLAYER_PED_ID()))
								IF NOT bArchCowering
									TASK_COWER(pedArchitect, -1)
									DROP_THE_CASE(pedArchitect)
									eCaseState = AH2_CS_DROPPING_CASE
									bArchCowering = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE AH2_RS_GOING_DOWN
					VECTOR vArchCoords
					vArchCoords = GET_ENTITY_COORDS(pedArchitect)
					IF vArchCoords.z < 50
					OR (IS_ENTITY_IN_ANY_LIFT(pedArchitect) AND constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(pedArchitect)].fCurrentState < 0.2)
					OR NOT IS_ENTITY_IN_ANY_LIFT(pedArchitect)
						RESET_FLEE_STATE()
						eRunawayState = AH2_RS_FOLLOW_PATH1
					ENDIF
				BREAK
				
				CASE AH2_RS_FOLLOW_PATH1
				
					#IF IS_DEBUG_BUILD
						DRAW_PATH(vGroundFLoorRunPath1, TRUE)
					#ENDIF
					
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
					AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 25*25
						IF GET_PROGRESS_ALONG_PATH(GET_ENTITY_COORDS(PLAYER_PED_ID()), vGroundFLoorRunPath1) >= GET_PROGRESS_ALONG_PATH(GET_ENTITY_COORDS(pedArchitect), vGroundFLoorRunPath1)
						AND GET_PROGRESS_ALONG_PATH(GET_ENTITY_COORDS(pedArchitect), vGroundFLoorRunPath1) > 0
							REVERSE_PATH(vGroundFLoorRunPath1)				
						ENDIF
					ENDIF
					
					IF NOT bArchCowering
						IF IS_ENTITY_IN_ANGLED_AREA(pedArchitect, <<-127.729340,-944.406738,30.352045>>, <<-138.307159,-940.593445,35.794006>>, 16.500000)
						AND GET_PROGRESS_ALONG_PATH(GET_ENTITY_COORDS(PLAYER_PED_ID()), vGroundFLoorRunPath1) < GET_PROGRESS_ALONG_PATH(GET_ENTITY_COORDS(pedArchitect), vGroundFLoorRunPath1)
							TASK_COWER(pedArchitect)
							DROP_THE_CASE(pedArchitect)
							eCaseState = AH2_CS_DROPPING_CASE
							bArchCowering = TRUE
						ENDIF
					ENDIF
					
					FOLLOW_PATH(vGroundFLoorRunPath1)
					
					IF IS_ENTITY_IN_ANY_LIFT(pedArchitect)
						IF IS_ENTITY_OCCLUDED(pedArchitect)
							IF IS_ENTITY_ATTACHED(pedArchitect)
								DETACH_ENTITY(pedArchitect)
							ENDIF
							IF NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(pedArchitect), 2)
							AND NOT IS_SPHERE_VISIBLE(GET_CLOSEST_PATH_NODE_POSITION(vGroundFLoorRunPath1), 2)
								SET_ENTITY_COORDS(pedArchitect, GET_CLOSEST_PATH_NODE_POSITION(vGroundFLoorRunPath1))
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_IN_ANGLED_AREA(pedArchitect, <<-177.884125,-1028.465576,26.168293>>, <<-141.839127,-931.335938,35.794006>>, 42.250000)
					AND NOT IS_ENTITY_IN_ANY_LIFT(pedArchitect)
						iTargetNode = -1
						PRINTLN("\n\n GOING TO PATH 2")
						RESET_FLEE_STATE()
						eRunawayState = AH2_RS_FOLLOW_PATH2
					ENDIF
					
				BREAK
				
				CASE AH2_RS_FOLLOW_PATH2
				
					#IF IS_DEBUG_BUILD
						DRAW_PATH(vGroundFLoorRunPath2, TRUE)
					#ENDIF
					
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
					AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 25*25
						IF GET_PROGRESS_ALONG_PATH(GET_ENTITY_COORDS(PLAYER_PED_ID()), vGroundFLoorRunPath2) >= GET_PROGRESS_ALONG_PATH(GET_ENTITY_COORDS(pedArchitect), vGroundFLoorRunPath2)
						AND GET_PROGRESS_ALONG_PATH(GET_ENTITY_COORDS(pedArchitect), vGroundFLoorRunPath2) > 0
							REVERSE_PATH(vGroundFLoorRunPath2)				
						ENDIF
					ENDIF
					
					FOLLOW_PATH(vGroundFLoorRunPath2)
					
					IF NOT IS_PED_ON_SITE(pedArchitect)
						eRunawayState = AH2_RS_FLEE
					ENDIF
					
				BREAK
				
				CASE AH2_RS_FLEE
					IF NOT IS_PED_FLEEING(pedArchitect)
						TASK_SMART_FLEE_PED(pedArchitect, PLAYER_PED_ID(), 5000, -1, TRUE)
					ENDIF
					IF IS_PED_ON_SITE(pedArchitect)
						eRunawayState = AH2_RS_FOLLOW_PATH2
					ENDIF
				BREAK
				
			ENDSWITCH
			
			IF eSiteState < AH2_SS_ATTACKING_PLAYER
				DO_ARCH_CRIES_FOR_HELP()
			ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

//**********************************************************************************************************************************************

//**********************************************************************************************************************************************

FUNC BOOL SETUP_GROUNDFLOOR_CONSTRUCTION()

	REQUEST_MODEL(S_M_Y_CONSTRUCT_01)
	REQUEST_MODEL(S_M_Y_CONSTRUCT_02)
	REQUEST_MODEL(PROP_TOOL_HARDHAT)
	REQUEST_MODEL(P_CS_CLIPBOARD)
	REQUEST_ANIM_DICT("missheist_agency2aig_8")
	REQUEST_ANIM_DICT("missheist_agency2aig_3")
	
	IF HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)
	AND HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_02)
	AND HAS_MODEL_LOADED(PROP_TOOL_HARDHAT)
	AND HAS_MODEL_LOADED(P_CS_CLIPBOARD)
	AND HAS_ANIM_DICT_LOADED("missheist_agency2aig_8")
	AND HAS_ANIM_DICT_LOADED("missheist_agency2aig_3")
		DESTROY_ALL_WORKERS(TRUE)		
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[0], <<-109.0560, -1027.7017, 26.2740>>, 258.7900)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[1], << -92.5795, -1034.5360, 26.9519 >>, 4.1899)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[2], <<-108.0088, -1051.8103, 26.2730>>, -0.000000)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[3], <<-109.0365, -1051.4375, 26.2730>>, -50.420292)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[4], <<-108.1822, -1050.1962, 26.2730>>, 164.621384)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[5], <<-134.8451, -1053.2484, 26.2570>>, 8.021409)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[6], <<-135.8568, -1053.0006, 26.2570>>, -53.857956)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[7], <<-150.6152, -1031.5042, 27.2799>>, 155.454041)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[8], <<-151.8469, -1030.7598, 27.2798>>, -169.595505)
		INT iTemp
		REPEAT COUNT_OF(sConstructionWorkers) iTemp
			IF NOT DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
				IF sConstructionWorkers[iTemp].bInitialised
					IF iTemp % 2 > 0
						CREATE_CONSTRUCTION_WORKER(sConstructionWorkers[iTemp],S_M_Y_CONSTRUCT_01)
					ELSE
						CREATE_CONSTRUCTION_WORKER(sConstructionWorkers[iTemp],S_M_Y_CONSTRUCT_02)
					ENDIF
					IF iTemp = 7
						sConstructionWorkers[iTemp].board = CREATE_OBJECT(P_CS_CLIPBOARD, GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped))
						ATTACH_ENTITY_TO_ENTITY(sConstructionWorkers[iTemp].board, sConstructionWorkers[iTemp].ped, GET_PED_BONE_INDEX(sConstructionWorkers[iTemp].ped, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>)
					ENDIF
					TEXT_LABEL strTemp
					strTemp = "worker "
					strTemp += iTemp
					SET_PED_NAME_DEBUG(sConstructionWorkers[iTemp].ped, strTemp)
					println("CREATING GROUNDFLOOR WORKER")
				ENDIF
			ENDIF
		ENDREPEAT
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

STRING sScenarios[15]

FUNC BOOL SETUP_GROUNDFLOOR_CONSTRUCTION_PART_TWO()

	REQUEST_MODEL(S_M_Y_CONSTRUCT_01)
	REQUEST_MODEL(S_M_Y_CONSTRUCT_02)
	IF HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)
	AND HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_02)
	
		DESTROY_ALL_WORKERS(TRUE)
		
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[0]		, 	<<-164.8137, -1029.0441, 26.2758>>, 110.8465)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[1]		, 	<<-166.5433, -1028.5907, 26.2758>>, 220.45072)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[2]		, 	<<-165.3987, -1030.3521, 26.2758>>, 4.9405)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[3]		, 	<<-106.8050, -1063.7751, 26.2988>>, 21.1665)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[4]		, 	<<-175.26, -1039.31, 27.24>>, 68.6232 )
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[5]		, 	<<-93.1512, -1015.1375, 26.2751>>, 175.4839)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[6]		, 	<<-94.7600, -1016.1201, 26.2751>>, 264.7611)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[7]		, 	<<-110.2056, -956.3950, 26.2504>>, 301.2692)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[8]		, 	<<-109.4497, -954.1309, 26.5774>>, 163.2955)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[9]		, 	<<-218.0016, -1107.6976, 21.9060>>, 155.2912)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[10]		, 	<<-219.6230, -1108.0063, 21.9094>>, 202.6225)
		
		sScenarios[0] = "WORLD_HUMAN_SMOKING"
		sScenarios[1] = "WORLD_HUMAN_CLIPBOARD"
		sScenarios[2] = "WORLD_HUMAN_HANG_OUT_STREET"
		sScenarios[3] = "WORLD_HUMAN_CONST_DRILL"
		sScenarios[4] = "WORLD_HUMAN_SEAT_WALL_EATING"
		sScenarios[5] = "WORLD_HUMAN_CLIPBOARD"
		sScenarios[6] = "WORLD_HUMAN_SMOKING"
		sScenarios[7] = "WORLD_HUMAN_SMOKING"
		sScenarios[8] = "WORLD_HUMAN_HANG_OUT_STREET"
		sScenarios[9] = "WORLD_HUMAN_HANG_OUT_STREET"
		sScenarios[10] = "WORLD_HUMAN_SMOKING"
		
		INT iTemp
		REPEAT COUNT_OF(sConstructionWorkers) iTemp
			IF NOT DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
				IF sConstructionWorkers[iTemp].bInitialised
					IF iTemp % 2 > 0
						CREATE_CONSTRUCTION_WORKER(sConstructionWorkers[iTemp],S_M_Y_CONSTRUCT_01)
					ELSE
						CREATE_CONSTRUCTION_WORKER(sConstructionWorkers[iTemp],S_M_Y_CONSTRUCT_02)
					ENDIF
					TEXT_LABEL strTemp
					strTemp = "worker "
					strTemp += iTemp
					SET_PED_NAME_DEBUG(sConstructionWorkers[iTemp].ped, strTemp)
					IF NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
						TASK_START_SCENARIO_AT_POSITION(sConstructionWorkers[iTemp].ped, sScenarios[iTemp], sConstructionWorkers[iTemp].vStartPosition, sConstructionWorkers[iTemp].fStartHeading, 0)
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
		RETURN TRUE
		
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC PRE_INIT_FIRST_FLOOR()
	REQUEST_MODEL(S_M_Y_CONSTRUCT_01)
	REQUEST_MODEL(S_M_Y_CONSTRUCT_02)
	REQUEST_ANIM_DICT("missheist_agency2aig_4")
ENDPROC

FUNC BOOL SETUP_TOPFLOOR_CONSTRUCTION()

	REQUEST_MODEL(S_M_Y_CONSTRUCT_01)
	REQUEST_MODEL(S_M_Y_CONSTRUCT_02)
	REQUEST_ANIM_DICT("missheist_agency2aig_4")
	
	IF HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)
	AND HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_02)
	AND HAS_ANIM_DICT_LOADED("missheist_agency2aig_4")
		DESTROY_ALL_WORKERS(TRUE)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[0], 	<< -175.8960, -1002.3189, 113.1376 >>, 286.7573)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[1], 	<< -174.1983, -1000.6993, 113.1376 >>, 146.8856)
		INT iTemp
		REPEAT COUNT_OF(sConstructionWorkers) iTemp
			IF NOT DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
				IF sConstructionWorkers[iTemp].bInitialised
					IF iTemp % 2 > 0
						CREATE_CONSTRUCTION_WORKER(sConstructionWorkers[iTemp],S_M_Y_CONSTRUCT_01)
					ELSE
						CREATE_CONSTRUCTION_WORKER(sConstructionWorkers[iTemp],S_M_Y_CONSTRUCT_02)
					ENDIF
					TEXT_LABEL strTemp
					strTemp = "worker "
					strTemp += iTemp
					SET_PED_NAME_DEBUG(sConstructionWorkers[iTemp].ped, strTemp)
				ENDIF
			ENDIF
		ENDREPEAT
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iWorkerScene)
			IF DOES_ENTITY_EXIST(sConstructionWorkers[0].ped)
			AND DOES_ENTITY_EXIST(sConstructionWorkers[1].ped)
				IF NOT IS_PED_INJURED(sConstructionWorkers[0].ped)
				AND NOT IS_PED_INJURED(sConstructionWorkers[1].ped)	
					iWorkerScene = CREATE_SYNCHRONIZED_SCENE(vWorkerScenePosition, vWorkerSceneRotation)
					TASK_SYNCHRONIZED_SCENE(sConstructionWorkers[0].ped, iWorkerScene, "missheist_agency2aig_4", "look_plan_base_worker1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					TASK_SYNCHRONIZED_SCENE(sConstructionWorkers[1].ped, iWorkerScene, "missheist_agency2aig_4", "look_plan_base_worker2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)	
					SET_SYNCHRONIZED_SCENE_LOOPED(iWorkerScene, TRUE)
				ENDIF
			ENDIF
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

INT iTImeGrab1, iTimeGrab2
FLOAT iPhaseGrab1, iPhaseGrab2
FLOAT fThousandthOfTotal

VECTOR vChatSceneStart
VECTOR vGroundFloorChatScenePosition = << -148.683, -1034.987, 26.256 >>
VECTOR vGroundFloorChatSceneRotation = << 0.000, 0.000, -104.800 >>
INT iGroundFloorChatScene

VECTOR vCatchLineP1 = <<-91.29668, -1027.77026, 26.78870>>
VECTOR vCatchLineP2 = <<-91.35906, -1019.10797, 26.49537>>

VECTOR vStandTarget

PROC RUN_GROUND_FLOOR_AMBIENCE()

	//hat on at 0.390
	#IF IS_DEBUG_BUILD
		VECTOR vIntersectTest
		DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,20,0>>))
		IF DO_LINES_INTERSECT2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,20,0>>), vCatchLineP1, vCatchLineP2, vIntersectTest)
			DRAW_DEBUG_SPHERE(vIntersectTest, 0.2, 255,0,0)
		ENDIF
		DRAW_DEBUG_LINE(<<-91.29668, -1027.77026, 26.78870>>, <<-91.35906, -1019.10797, 26.49537>>)
		DRAW_DEBUG_SPHERE(GET_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCatchLineP1, vCatchLineP2, TRUE), 0.2, 255, 0, 0)
	#ENDIF
	
	IF iHatThrowStage > 0
	AND iHatThrowStage < 5
	AND IS_SYNCHRONIZED_SCENE_RUNNING(iHatThrowScene)
	
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CELLPHONE_UP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		
	ENDIF
	
	IF NOT IS_PED_INJURED(sConstructionWorkers[1].ped)
	
		SWITCH iHatThrowStage
		
			CASE 0
				
				REQUEST_ANIM_DICT("missheist_agency2aig_8")
				REQUEST_MODEL(PROP_HARD_HAT_01)
				IF HAS_ANIM_DICT_LOADED("missheist_agency2aig_8")
				AND HAS_MODEL_LOADED(PROP_HARD_HAT_01)
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iHatThrowScene)
						iHatThrowScene = CREATE_SYNCHRONIZED_SCENE(vHatScenePosition, vHatSceneRotation)
						TASK_SYNCHRONIZED_SCENE(sConstructionWorkers[1].ped, iHatThrowScene, "missheist_agency2aig_8", "start_loop_foreman", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
						SET_SYNCHRONIZED_SCENE_LOOPED(iHatThrowScene, TRUE)
						objHat = CREATE_OBJECT(PROP_HARD_HAT_01, GET_ENTITY_COORDS(sConstructionWorkers[1].ped))
						ATTACH_ENTITY_TO_ENTITY(objHat, sConstructionWorkers[1].ped, GET_PED_BONE_INDEX(sConstructionWorkers[1].ped, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
						SET_ENTITY_VISIBLE(objHat, FALSE)
					ENDIF
					IF IS_PED_ON_SITE(PLAYER_PED_ID())
					AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-79.437965,-1023.789551,25.909868>>, <<-92.449219,-1019.016541,32.396622>>, 24.500000)
					AND (IS_ENTITY_OCCLUDED(sConstructionWorkers[1].ped) OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-150.450333,-1044.423950,46.905464>>, <<-186.544632,-1144.930786,18.087076>>, 121.000000))
						IF DOES_ENTITY_EXIST(objHat)
							DELETE_OBJECT(objHat)
						ENDIF
						CLEAR_PED_TASKS_IMMEDIATELY(sConstructionWorkers[1].ped)
						SET_ENTITY_COORDS(sConstructionWorkers[1].ped, <<-90.1453, -1030.2339, 26.9782>>)
						SET_ENTITY_HEADING(sConstructionWorkers[1].ped, 12.4764)
						TASK_START_SCENARIO_IN_PLACE(sConstructionWorkers[1].ped, "WORLD_HUMAN_HANG_OUT_STREET", 0, TRUE)
						PRINTLN("Player on site away from hat thrower")
						iHatThrowStage = 7
					ENDIF
					
					IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-92.268478,-1031.527344,30.506458>>, <<-94.062271,-1014.746094,26.023346>>, 10.500000)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-95.892853,-1017.194153,25.275059>>, <<-79.711021,-1014.680115,30.696949>>, 6.250000)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-85.237518,-1031.640381,26.199564>>, <<-91.327530,-1026.263428,29.989769>>, 6.250000))
					AND IS_SYNCHRONIZED_SCENE_RUNNING(iHatThrowScene)
					AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
					AND NOT IS_ANY_VEHICLE_NEAR_POINT(GET_ENTITY_COORDS(sConstructionWorkers[1].ped), 5)
						REMOVE_PED_FOR_DIALOGUE(sSpeech, 6)
						ADD_PED_FOR_DIALOGUE(sSpeech, 6, sConstructionWorkers[1].ped, "FIBConstruction2")
						
						SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
						IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						ELSE
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						ENDIF
						
						//find the optimum point to walk to
						VECTOR vIdeal
						VECTOR vToArchitect
						VECTOR vClosest
						
						BOOL bIdeal
						BOOL bToArchitect
						
						vClosest = GET_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCatchLineP1, vCatchLineP2)
						bIdeal = DO_LINES_INTERSECT2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,20,0>>), vCatchLineP1, vCatchLineP2, vIdeal)
						IF NOT IS_PED_INJURED(pedArchitect)
							bToArchitect = DO_LINES_INTERSECT2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect), vCatchLineP1, vCatchLineP2, vToArchitect)
						ENDIF
						
						IF bIdeal
							#IF IS_DEBUG_BUILD
								PRINTLN("Ideal path intersects")
							#ENDIF
							IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vIdeal) < 7
								vStandTarget = vIdeal							
							ELSE
								#IF IS_DEBUG_BUILD
									PRINTLN("Ideal intersection is too far away")
								#ENDIF
								bIdeal = FALSE
							ENDIF
						ENDIF
						
						IF NOT bIdeal
						AND bToArchitect
							#IF IS_DEBUG_BUILD
								PRINTLN("Path to architect intersects")
							#ENDIF
							IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vToArchitect) < 7
								vStandTarget = vToArchitect							
							ELSE
								#IF IS_DEBUG_BUILD
									PRINTLN("Path to architect intersection is too far away")
								#ENDIF
								bToArchitect = FALSE
							ENDIF
						ENDIF
						
						IF NOT bIdeal
						AND NOT bToArchitect
							#IF IS_DEBUG_BUILD
								PRINTLN("Going for closest point")
							#ENDIF
							vStandTarget = vClosest
						ENDIF
						
						SEQUENCE_INDEX seqTemp
						OPEN_SEQUENCE_TASK(seqTemp)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vStandTarget, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, sConstructionWorkers[1].ped)
						CLOSE_SEQUENCE_TASK(seqTemp)
						TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqTemp)
						CLEAR_SEQUENCE_TASK(seqTemp)
						
						RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sConstructionWorkers[1].ped, -1, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_HIGH)
						SET_GAMEPLAY_ENTITY_HINT(sConstructionWorkers[1].ped, <<0,0,1>>, TRUE, 20000)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						SET_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID(), ANCHOR_HEAD, 7)
						
						SET_ENTITY_VISIBLE(objHat, TRUE)
						
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE, DEFAULT, FALSE)
						
						iTImeGrab1 = -1
						iTimeGrab2 = -1
						bThanks = FALSE
						iHatThrowStage++
						
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), vStandTarget) < 3
					CLEAR_PED_TASKS_IMMEDIATELY(sConstructionWorkers[1].ped)
					iHatThrowScene = CREATE_SYNCHRONIZED_SCENE(vHatScenePosition, vHatSceneRotation)
					TASK_SYNCHRONIZED_SCENE(sConstructionWorkers[1].ped, iHatThrowScene, "missheist_agency2aig_8", "throw_helmet_foreman", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sConstructionWorkers[1].ped, TRUE)
					REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					iHatThrowStage++
				ENDIF
			BREAK
			
			CASE 2
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iHatThrowScene)
					IF iTImeGrab1 = -1
						iTImeGrab1 = GET_GAME_TIMER()
						iPhaseGrab1 = GET_SYNCHRONIZED_SCENE_PHASE(iHatThrowScene)
					ELIF iTimeGrab2 = -1
						iTImeGrab2 = GET_GAME_TIMER()
						iPhaseGrab2 = GET_SYNCHRONIZED_SCENE_PHASE(iHatThrowScene)
					ELSE
						IF iTImeGrab1 <> -2
							fThousandthOfTotal= ((iTimeGrab2 - iTImeGrab1)/(iPhaseGrab2 - iPhaseGrab1))/1000
							iTImeGrab1 = -2
						ENDIF
					ENDIF
					IF GET_SYNCHRONIZED_SCENE_PHASE(iHatThrowScene) > 0.05
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_HAT", CONV_PRIORITY_HIGH)
						iHatThrowStage++
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iHatThrowScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iHatThrowScene) > 0.232
						LAUNCH_HAT(sConstructionWorkers[1].ped, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.0356, 0.3167, 0.3579>>), t)
						STOP_GAMEPLAY_HINT()
						IF DOES_ENTITY_EXIST(objHat)
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), objHat, -1, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_HIGH)
						ENDIF
						SETTIMERA(0)
						iHatThrowStage++
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
			
				#IF IS_DEBUG_BUILD
					TEXT_LABEL_63 sTemp
					sTemp = GET_STRING_FROM_INT(TIMERA())
					sTemp += " \ "
					sTemp += GET_STRING_FROM_FLOAT(t*1000)
					DRAW_DEBUG_TEXT_2D(sTemp, <<0.1,0.1,0>>)
					DRAW_DEBUG_SPHERE(vTarget, 0.1, 255, 0, 255)
				#ENDIF
				
				IF DOES_ENTITY_EXIST(objHat)
				
					IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missheist_agency2aig_8", "throw_helmet_player1")
						IF ((t*1000) - TIMERA()) < 1000
							IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
								TASK_PLAY_ANIM(PLAYER_PED_ID(), "missheist_agency2aig_8", "throw_helmet_player1", INSTANT_BLEND_IN, SLOW_BLEND_OUT, 5000, AF_UPPERBODY|AF_SECONDARY, 0.278 - (((t*1000)-TIMERA())/fThousandthOfTotal/1000))
							ELSE
								TASK_PLAY_ANIM(PLAYER_PED_ID(), "missheist_agency2aig_8", "throw_helmet_player1", INSTANT_BLEND_IN, SLOW_BLEND_OUT, 5000, AF_DEFAULT, 0.278 - (((t*1000)-TIMERA())/fThousandthOfTotal/1000))
							ENDIF
							TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_ATTACHED(objHat)
						IF (t*1000) - TIMERA() <= 0
							ATTACH_ENTITY_TO_ENTITY(objHat, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, FALSE)
							IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
								SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							ENDIF
							//TASK_LOOK_AT_ENTITY(sConstructionWorkers[1].ped, PLAYER_PED_ID(), -1)
						ELSE
							IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
								IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
									SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
								ELSE
									SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missheist_agency2aig_8", "throw_helmet_player1")
				AND GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "missheist_agency2aig_8", "throw_helmet_player1") > 0.39
				AND HAS_PED_PRELOAD_PROP_DATA_FINISHED(PLAYER_PED_ID())
					SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD, 7)
					RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
					iPostHatSlowdown = GET_GAME_TIMER()
					iHatThrowStage++
				ENDIF
				
			BREAK
			
			CASE 5
				IF DOES_ENTITY_EXIST(objHat)
					DELETE_OBJECT(objHat)
				ENDIF
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missheist_agency2aig_8", "throw_helmet_player1")
					IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "missheist_agency2aig_8", "throw_helmet_player1") > 0.40
						iHatThrowStage++
					ENDIF
				ENDIF
			BREAK
			
			CASE 6
				IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missheist_agency2aig_8", "throw_helmet_player1")
					IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "missheist_agency2aig_8", "throw_helmet_player1") > 0.45
						IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_2_FOLLOW_ARCHITECT_1")
							START_AUDIO_SCENE("AGENCY_H_2_FOLLOW_ARCHITECT_1")
						ENDIF
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sConstructionWorkers[0].ped, 5000, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
					ENDIF
				ENDIF
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iHatThrowScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iHatThrowScene) > 0.597
						CLEAR_PED_TASKS(sConstructionWorkers[1].ped)
//						iHatThrowScene = CREATE_SYNCHRONIZED_SCENE(vHatScenePosition, vHatSceneRotation)
//						TASK_SYNCHRONIZED_SCENE(sConstructionWorkers[1].ped, iHatThrowScene, "missheist_agency2aig_8", "end_loop_foreman", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
//						FORCE_PED_AI_AND_ANIMATION_UPDATE(sConstructionWorkers[1].ped)
						TASK_START_SCENARIO_IN_PLACE(sConstructionWorkers[1].ped, "WORLD_HUMAN_HANG_OUT_STREET", 0, TRUE)
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedArchitect, -1, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_HIGH)
						iHatThrowStage++
					ENDIF
				ENDIF
			BREAK
			
		ENDSWITCH
		
		IF NOT bThanks
			IF iHatThrowStage > 3
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_THANKS", CONV_PRIORITY_HIGH)
					bThanks = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF iHatThrowStage > 0
		AND iHatThrowStage < 7
			sConstructionWorkers[1].bCanSeePlayer = FALSE
		ENDIF
		
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missheist_agency2aig_8", "throw_helmet_player1")
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iHatThrowScene)
				DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iHatThrowScene)), <<0.02, 0.5, 0>>)
			ENDIF
		#ENDIF
		
		IF mission_stage = STAGE_GROUND_FLOOR
		AND GET_GAME_TIMER() - iPostHatSlowdown < 2500
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
		ENDIF
		
	ENDIF
	
	IF iStageProgress < 100
	
		IF NOT IS_PED_INJURED(sConstructionWorkers[2].ped)
			IF NOT IS_PED_USING_SCENARIO(sConstructionWorkers[2].ped, "WORLD_HUMAN_CLIPBOARD")
				TASK_START_SCENARIO_IN_PLACE(sConstructionWorkers[2].ped, "WORLD_HUMAN_CLIPBOARD")
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(sConstructionWorkers[3].ped)
			IF NOT IS_PED_USING_SCENARIO(sConstructionWorkers[3].ped, "WORLD_HUMAN_HANG_OUT_STREET")
				TASK_START_SCENARIO_IN_PLACE(sConstructionWorkers[3].ped, "WORLD_HUMAN_HANG_OUT_STREET")
				ADD_PED_FOR_DIALOGUE(sSpeech, 3, sConstructionWorkers[3].ped, "FIBConstruction")
			ENDIF
			IF NOT sConstructionWorkers[3].bTasked
				IF GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD) != 7
				AND IS_PED_ON_SITE(PLAYER_PED_ID())
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sConstructionWorkers[3].ped)) < POW(18,2)
//						IF GET_RANDOM_INT_IN_RANGE(1,3) < 2
							IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_HARD", CONV_PRIORITY_HIGH)
								sConstructionWorkers[3].bTasked = TRUE
							ENDIF
//						ELSE
//							IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_HARD2", CONV_PRIORITY_HIGH)
//								sConstructionWorkers[3].bTasked = TRUE
//							ENDIF
//						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(sConstructionWorkers[4].ped)
			IF NOT IS_PED_USING_SCENARIO(sConstructionWorkers[4].ped, "WORLD_HUMAN_SMOKING")
				TASK_START_SCENARIO_IN_PLACE(sConstructionWorkers[4].ped, "WORLD_HUMAN_SMOKING")
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(sConstructionWorkers[5].ped)
			IF NOT IS_PED_USING_SCENARIO(sConstructionWorkers[5].ped, "WORLD_HUMAN_CLIPBOARD")
				TASK_START_SCENARIO_IN_PLACE(sConstructionWorkers[5].ped, "WORLD_HUMAN_CLIPBOARD")
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(sConstructionWorkers[6].ped)
			IF NOT IS_PED_USING_SCENARIO(sConstructionWorkers[6].ped, "WORLD_HUMAN_HANG_OUT_STREET")
				TASK_START_SCENARIO_IN_PLACE(sConstructionWorkers[6].ped, "WORLD_HUMAN_HANG_OUT_STREET")
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(sConstructionWorkers[7].ped)
	AND NOT IS_PED_INJURED(sConstructionWorkers[8].ped)
	AND NOT IS_PED_INJURED(pedArchitect)
		SWITCH iChatStage
			CASE 0
				REQUEST_ANIM_DICT("missheist_agency2aig_3")
				IF HAS_ANIM_DICT_LOADED("missheist_agency2aig_3")
					iGroundFloorChatScene = CREATE_SYNCHRONIZED_SCENE(vGroundFloorChatScenePosition, vGroundFloorChatSceneRotation)
					TASK_SYNCHRONIZED_SCENE(sConstructionWorkers[7].ped, iGroundFLoorChatScene, "missheist_agency2aig_3", "chat_a_worker1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					TASK_SYNCHRONIZED_SCENE(sConstructionWorkers[8].ped, iGroundFLoorChatScene, "missheist_agency2aig_3", "chat_a_worker2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					SET_SYNCHRONIZED_SCENE_LOOPED(iGroundFloorChatScene, TRUE)
					iChatStage++
				ENDIF
			BREAK
			CASE 1
				IF IS_ENTITY_IN_ANGLED_AREA(pedArchitect, <<-149.548264,-1036.483765,29.025150>>, <<-143.127014,-1039.089233,26.025150>>, 10.5)
					vChatSceneStart = GET_ANIM_INITIAL_OFFSET_POSITION("missheist_agency2aig_3", "walk_by_chat_architect", vGroundFloorChatScenePosition, vGroundFloorChatSceneRotation, 0)
					iChatStage++
				ENDIF
			BREAK
			CASE 2
				IF VDIST(GET_ENTITY_COORDS(pedArchitect), vChatSceneStart) < 0.3
					IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 20
						REMOVE_PED_FOR_DIALOGUE(sSpeech, 6)
						ADD_PED_FOR_DIALOGUE(sSpeech, 6, sConstructionWorkers[7].ped, "FIBConstruction2")
						PLAY_DIALOGUE(dhBanter1)
					ENDIF
					CLEAR_PED_TASKS(sConstructionWorkers[7].ped)
					CLEAR_PED_TASKS(sConstructionWorkers[8].ped)
					sConstructionWorkers[7].bTasked = TRUE
					iGroundFloorChatScene = CREATE_SYNCHRONIZED_SCENE(vGroundFloorChatScenePosition, vGroundFloorChatSceneRotation)
					TASK_SYNCHRONIZED_SCENE(sConstructionWorkers[7].ped, iGroundFloorChatScene, "missheist_agency2aig_3", "walk_by_chat_worker1", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					TASK_SYNCHRONIZED_SCENE(sConstructionWorkers[8].ped, iGroundFloorChatScene, "missheist_agency2aig_3", "walk_by_chat_worker2", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sConstructionWorkers[7].ped)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sConstructionWorkers[8].ped)
					TASK_LOOK_AT_ENTITY(pedArchitect, sConstructionWorkers[7].ped, 30000)
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					iChatStage++
				ENDIF
			BREAK
			CASE 3
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iGroundFloorChatScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iGroundFloorChatScene) > 0.9999
						TASK_CLEAR_LOOK_AT(pedArchitect)
						CLEAR_PED_TASKS(sConstructionWorkers[7].ped)
						CLEAR_PED_TASKS(sConstructionWorkers[8].ped)
						iGroundFloorChatScene = CREATE_SYNCHRONIZED_SCENE(vGroundFloorChatScenePosition, vGroundFloorChatSceneRotation)
						TASK_SYNCHRONIZED_SCENE(sConstructionWorkers[7].ped, iGroundFloorChatScene, "missheist_agency2aig_3", "chat_b_worker1", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
						TASK_SYNCHRONIZED_SCENE(sConstructionWorkers[8].ped, iGroundFloorChatScene, "missheist_agency2aig_3", "chat_b_worker2", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sConstructionWorkers[7].ped)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sConstructionWorkers[8].ped)
						SET_SYNCHRONIZED_SCENE_LOOPED(iGroundFloorChatScene, TRUE)
						iChatStage++
					ENDIF
				ENDIF
			BREAK
			CASE 4
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iGroundFloorChatScene)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(sConstructionWorkers[7].ped), <<10,10,5>>)
						IF NOT sConstructionWorkers[6].bTasked
							REMOVE_PED_FOR_DIALOGUE(sSpeech, 6)
							REMOVE_PED_FOR_DIALOGUE(sSpeech, 3)
							ADD_PED_FOR_DIALOGUE(sSpeech, 6, sConstructionWorkers[7].ped, "FIBConstruction2")
							ADD_PED_FOR_DIALOGUE(sSpeech, 3, sConstructionWorkers[8].ped, "FIBConstruction")
							IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_WRKCHAT", CONV_PRIORITY_HIGH, DO_NOT_DISPLAY_SUBTITLES, DO_NOT_ADD_TO_BRIEF_SCREEN)
								sConstructionWorkers[6].bTasked = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
ENDPROC

PROC SET_SITE_STATE(SITE_STATE eStage = AH2_SS_ALL_QUIET)
	#IF IS_DEBUG_BUILD PRINTLN("Setting site state to ", GET_STRING_FROM_SITE_STATE(eStage)) #ENDIF
	eSiteState = eStage
ENDPROC

PROC ALERT_NEARBY_WORKERS(INT iSourceWorker)
	INT iTemp
	REPEAT COUNT_OF(sConstructionWorkers) iTemp
		IF NOT sConstructionWorkers[iTemp].bSpottedPlayer
			IF NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
			AND VDIST2(GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped), GET_ENTITY_COORDS(sConstructionWorkers[iSourceWorker].ped)) < 5*5
				sConstructionWorkers[iTemp].bSpottedPlayer = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	IF MISSION_STAGE = STAGE_FIRST_FLOOR
	OR mission_stage = STAGE_GROUND_FLOOR
		IF NOT IS_PED_INJURED(pedArchitect)
			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 3*3
			AND NOT IS_ENTITY_IN_ANY_LIFT(pedArchitect)
				GO_TO_STAGE(STAGE_PLAYER_SPOTTED)	
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_WORKER_BLIPS()
	INT iWatchConsBlips
	REPEAT COUNT_OF(sConstructionWorkers) iWatchConsBlips
		IF DOES_ENTITY_EXIST(sConstructionWorkers[iWatchConsBlips].ped)
			IF DOES_BLIP_EXIST(sConstructionWorkers[iWatchConsBlips].blip)
				IF IS_PED_INJURED(sConstructionWorkers[iWatchConsBlips].ped)
				OR IS_ENTITY_DEAD(sConstructionWorkers[iWatchConsBlips].ped)
				OR (NOT IS_PED_INJURED(sConstructionWorkers[iWatchConsBlips].ped) AND NOT sConstructionWorkers[iWatchConsBlips].bCombat)
				OR (NOT IS_PED_INJURED(sConstructionWorkers[iWatchConsBlips].ped) AND VDIST2(GET_ENTITY_COORDS(sConstructionWorkers[iWatchConsBlips].ped), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 55*55)
				OR DOES_BLIP_EXIST(blipArchitect)
				OR DOES_BLIP_EXIST(blipPlans)
				OR VDIST2(GET_ENTITY_COORDS(sConstructionWorkers[iWatchConsBlips].ped), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 30*30
				OR eCaseState = AH2_CS_DROPPING_CASE
				OR eCaseState = AH2_CS_ON_GROUND
					REMOVE_BLIP(sConstructionWorkers[iWatchConsBlips].blip)
				ENDIF
			ELSE
				IF sConstructionWorkers[iWatchConsBlips].bCombat
				AND NOT sConstructionWorkers[iWatchConsBlips].bFlee
				AND NOT DOES_BLIP_EXIST(blipArchitect)
				AND NOT DOES_BLIP_EXIST(blipPlans)
				AND eCaseState != AH2_CS_DROPPING_CASE
				AND eCaseState != AH2_CS_ON_GROUND
					IF NOT IS_PED_INJURED(sConstructionWorkers[iWatchConsBlips].ped)
					AND VDIST2(GET_ENTITY_COORDS(sConstructionWorkers[iWatchConsBlips].ped), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 20*20
						sConstructionWorkers[iWatchConsBlips].blip = CREATE_BLIP_FOR_PED(sConstructionWorkers[iWatchConsBlips].ped, TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEANUP_WORKERS(BOOL bSiteCheckPed, BOOL bSiteCheckPlayer, BOOL bLockUpWhenDone, FLOAT fDist = 30.0)
	INT iTemp
	BOOL bAllWorkersDoneOrDead = TRUE
	IF NOT bSiteCleanedUp
		REPEAT COUNT_OF(sConstructionWorkers) iTemp
			IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
				IF (NOT IS_PED_ON_SITE(PLAYER_PED_ID()) OR NOT bSiteCheckPlayer)
				AND (NOT IS_PED_ON_SITE(sConstructionWorkers[iTemp].ped) OR NOT bSiteCheckPed) 
					IF IS_ENTITY_OCCLUDED(sConstructionWorkers[iTemp].ped)
					AND VDIST2(GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped, FALSE), vPlayerCoords) > fDist*fDist
						IF NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
							DELETE_PED(sConstructionWorkers[iTemp].ped)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
				IF NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
					bAllWorkersDoneOrDead = FALSE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	IF bAllWorkersDoneOrDead
	AND bLockUpWhenDone
	AND NOT bSiteCleanedUp
		REMOVE_SCENARIO_BLOCKING_AREA(sbiSite)
		bSiteCleanedUp = TRUE
	ENDIF
ENDPROC

BOOL bReportDone

PROC MANAGE_SITE()

	UPDATE_WORKERS_LINE_OF_SIGHT()
	MANAGE_WORKER_BLIPS()
	
	IF eSiteState = AH2_SS_ATTACKING_PLAYER		
		IF NOT bTwoStarWantedLevelGiven
		AND NOT bThreeStarWantedLevelGiven
		AND (VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vArchCarStart) < 4000 OR IS_PED_ON_SITE(PLAYER_PED_ID()))
		
			SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 2)
			SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
			IF IS_PLAYER_ON_TOP_FLOOR()
			OR IS_PED_ON_SITE(PLAYER_PED_ID())
				bPlayerHasLeftTopFloor = FALSE
			ENDIF
			iTimeLastWantedLevelApplied = GET_GAME_TIMER()
			bTwoStarWantedLevelGiven = TRUE
		ENDIF
	ENDIF
	
	IF eSiteState = AH2_SS_FLEEING_PLAYER
		IF NOT bthreeStarWantedLevelGiven
		AND (VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vArchCarStart) < 4000 OR IS_PED_ON_SITE(PLAYER_PED_ID()))
			SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 3)
			SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
			IF IS_PLAYER_ON_TOP_FLOOR()
			OR IS_PED_ON_SITE(PLAYER_PED_ID())
				bPlayerHasLeftTopFloor = FALSE
			ENDIF
			iTimeLastWantedLevelApplied = GET_GAME_TIMER()
			bthreeStarWantedLevelGiven = TRUE
		ENDIF
	ENDIF
	
	IF eSiteState = AH2_SS_FLEEING_PLAYER
	OR eSiteState = AH2_SS_ATTACKING_PLAYER
	
		IF bThreeStarWantedLevelGiven
		OR bTwoStarWantedLevelGiven
			IF bThreeStarWantedLevelGiven
				IF NOT bThreeStarWantedLevelAttained
					IF GET_GAME_TIMER() - iTimeLastWantedLevelApplied > 3000
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 3
							SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 3)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							iTimeLastWantedLevelApplied = GET_GAME_TIMER()
						ENDIF
					ENDIF
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) >= 3
						bThreeStarWantedLevelAttained = TRUE
					ENDIF
				ELSE
					IF NOT bPlayerHasLeftTopFloor
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 3
							SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 3)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						ENDIF
					ENDIF
				ENDIF
			ELIF bTwoStarWantedLevelGiven
				IF NOT bTwoStarWantedLevelAttained
					IF GET_GAME_TIMER() - iTimeLastWantedLevelApplied > 3000
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
							SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 2)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							iTimeLastWantedLevelApplied = GET_GAME_TIMER()
						ENDIF
					ENDIF
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) >= 2
						bTwoStarWantedLevelAttained = TRUE
					ENDIF
				ELSE
					IF NOT bPlayerHasLeftTopFloor
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
							SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 2)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			SET_PLAYER_WANTED_CENTRE_POSITION(PLAYER_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
		ENDIF
		
		IF bTwoStarWantedLevelAttained
		OR bThreeStarWantedLevelAttained
			IF NOT bReportDone
				PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_AH_MUGGING_01", 0.0)
				PRINTLN("SCRIPTED_SCANNER_REPORT_AH_MUGGING_01 played")
				bReportDone = TRUE
			ENDIF
		ENDIF
		
		IF NOT bPlayerHasLeftTopFloor
			IF NOT IS_PLAYER_ON_TOP_FLOOR()
			OR NOT IS_PED_ON_SITE(PLAYER_PED_ID())
				bPlayerHasLeftTopFloor = TRUE
			ENDIF
		ENDIF
		
	ENDIF
	
	INT iTemp
	INT iClosest
	FLOAT fDistToPlayer
	
	SWITCH eAmbientSiteStage
		CASE AH2_AS_SETUP_GROUNDFLOOR1
			IF SETUP_GROUNDFLOOR_CONSTRUCTION()
				eAmbientSiteStage = AH2_AS_RUN_GROUNDFLOOR
			ENDIF
		BREAK
		CASE AH2_AS_RUN_GROUNDFLOOR
			IF eSiteState = AH2_SS_ALL_QUIET
				RUN_GROUND_FLOOR_AMBIENCE()
			ENDIF
			IF IS_PLAYER_ON_TOP_FLOOR()
				eAmbientSiteStage = AH2_AS_SETUP_TOPFLOOR
			ENDIF
		BREAK
		CASE AH2_AS_SETUP_TOPFLOOR
			IF SETUP_TOPFLOOR_CONSTRUCTION()
				eAmbientSiteStage = AH2_AS_RUN_TOP_FLOOR
			ENDIF
		BREAK
		CASE AH2_AS_RUN_TOP_FLOOR
			IF NOT IS_PLAYER_ON_TOP_FLOOR()
				eAmbientSiteStage = AH2_AS_SETUP_GROUNDFLOOR2
			ENDIF
		BREAK
		CASE AH2_AS_SETUP_GROUNDFLOOR2
			IF SETUP_GROUNDFLOOR_CONSTRUCTION_PART_TWO()
				eAmbientSiteStage = AH2_AS_RUN_GROUNDFLOOR2
			ENDIF
		BREAK
		CASE AH2_AS_RUN_GROUNDFLOOR2
			CLEANUP_WORKERS(TRUE, TRUE, TRUE, 100)
		BREAK
	ENDSWITCH
	
	SWITCH eSiteState
	
		CASE AH2_SS_ALL_QUIET
		
			REPEAT COUNT_OF(sConstructionWorkers) iTemp
			
				IF eSiteState = AH2_SS_ALL_QUIET	
				
					IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
					AND NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
					
						fDistToPlayer = VDIST(vPlayerCoords, GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped))
						
						IF fDistToPlayer < 5
						
							IF sConstructionWorkers[iTemp].bCanSeePlayer
								sConstructionWorkers[iTemp].fSpotTime += GET_FRAME_TIME()
							ELSE
								sConstructionWorkers[iTemp].fSpotTime = 0
							ENDIF
							
						ENDIF
						
						IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
						
							IF fDistToPlayer < 20
							
								IF sConstructionWorkers[iTemp].bCanSeePlayer
									sConstructionWorkers[iTemp].fSpotTime += GET_FRAME_TIME()*3
								ELSE
									sConstructionWorkers[iTemp].fSpotTime = 0
								ENDIF
								
							ENDIF
							
						ENDIF
						
						IF sConstructionWorkers[iTemp].fSpotTime > 3.0
							sConstructionWorkers[iTemp].bSpottedPlayer = TRUE
						ENDIF
						
						IF fDistToPlayer < 2
							IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), sConstructionWorkers[iTemp].ped)
								IF iTemp = 1
									IF NOT IS_ENTITY_PLAYING_ANIM(sConstructionWorkers[iTemp].ped, "missheist_agency2aig_8", "start_loop_foreman")
										sConstructionWorkers[iTemp].bSpottedPlayer = TRUE
									ENDIF
								ELSE
									sConstructionWorkers[iTemp].bSpottedPlayer = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						IF sConstructionWorkers[iTemp].bSpottedPlayer
						AND (iHatThrowStage < 1 OR iHatThrowStage > 6)
						
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							CLEAR_PRINTS()
							CLEAR_PED_TASKS(sConstructionWorkers[iTemp].ped)
							TASK_TURN_PED_TO_FACE_ENTITY(sConstructionWorkers[iTemp].ped, PLAYER_PED_ID())
							REMOVE_PED_FOR_DIALOGUE(sSpeech, 4)
							ADD_PED_FOR_DIALOGUE(sSpeech, 4, sConstructionWorkers[iTemp].ped, "FIBConstruction2")
							
							IF NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
								IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_MAKE", CONV_PRIORITY_HIGH)
									REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
									ALERT_NEARBY_WORKERS(iTemp)
									eSiteState = AH2_SS_SPOTTED_PLAYER
								ENDIF
							ELSE
								PRINTLN("ALERT WHOLE SITE 1")
								ALERT_WHOLE_SITE(TRUE)
							ENDIF
							
						ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDREPEAT
			
		BREAK
		
		CASE AH2_SS_SPOTTED_PLAYER
		
			iClosest = GET_CLOSEST_CONSTRUCTION_WORKER(sConstructionWorkers)
			BOOL bPlayerCanBeSeen
			
			REPEAT COUNT_OF(sConstructionWorkers) iTemp
				IF NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
					fDistToPlayer = VDIST(vPlayerCoords, GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped))
					IF sConstructionWorkers[iTemp].bCanSeePlayer
						IF NOT sConstructionWorkers[iTemp].bSpottedPlayer
							IF fDistToPlayer < 5
								sConstructionWorkers[iTemp].bSpottedPlayer = TRUE
							ENDIF
						ENDIF
						IF sConstructionWorkers[iTemp].bSpottedPlayer
						AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
						AND fDistToPlayer < 5
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							REMOVE_PED_FOR_DIALOGUE(sSpeech, 2)
							ADD_PED_FOR_DIALOGUE(sSpeech, 2, sConstructionWorkers[iTemp].ped, "FIBConstruction")
							IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_GUN", CONV_PRIORITY_HIGH)
								PRINTLN("ALERT WHOLE SITE 2")
								ALERT_WHOLE_SITE(TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF sConstructionWorkers[iTemp].bSpottedPlayer
						IF NOT IS_PED_FACING_PED(sConstructionWorkers[iTemp].ped, PLAYER_PED_ID(), 60)
							IF GET_SCRIPT_TASK_STATUS(sConstructionWorkers[iTemp].ped, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
								CLEAR_PED_TASKS(sConstructionWorkers[iTemp].ped)
								TASK_LOOK_AT_ENTITY(sConstructionWorkers[iTemp].ped, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
								OPEN_SEQUENCE_TASK(seqTurnTask)
								TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
								TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT)
								CLOSE_SEQUENCE_TASK(seqTurnTask)
								TASK_PERFORM_SEQUENCE(sConstructionWorkers[iTemp].ped, seqTurnTask)
								CLEAR_SEQUENCE_TASK(seqTurnTask)
							ENDIF
						ENDIF
						bPlayerCanBeSeen = TRUE
					ENDIF
					
				ENDIF
				
			ENDREPEAT
			
			IF bPlayerCanBeSeen
			
				iclosest = GET_CLOSEST_CONSTRUCTION_WORKER(sConstructionWorkers, TRUE)
				IF iClosest <> -1
					IF NOT bWarningGiven
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND NOT IS_CASE_TEXT_SHOWING()
							IF VDIST2(vPlayerCoords, GET_ENTITY_COORDS(sConstructionWorkers[iClosest].ped)) < 20*20
							AND sConstructionWorkers[iClosest].bCanSeePlayer
								REMOVE_PED_FOR_DIALOGUE(sSpeech, 2)
								ADD_PED_FOR_DIALOGUE(sSpeech, 2, sConstructionWorkers[iClosest].ped, "FIBConstruction")
								IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_CONSPOT", CONV_PRIORITY_HIGH)
									ALERT_NEARBY_WORKERS(iClosest)
									bWarningGiven = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF VDIST2(vPlayerCoords, GET_ENTITY_COORDS(sConstructionWorkers[iClosest].ped)) < 20*20
						AND sConstructionWorkers[iClosest].bCanSeePlayer
//							IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_CONRUN", CONV_PRIORITY_HIGH)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(sConstructionWorkers[iClosest].ped, "SHOUT_THREATEN", "FIBConstruction", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
							PRINTLN("ALERT WHOLE SITE 3")
							ALERT_WHOLE_SITE()
//							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE AH2_SS_ATTACKING_PLAYER
		
			REPEAT COUNT_OF(sConstructionWorkers) iTemp
			
				IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
				AND NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
					IF sConstructionWorkers[iTemp].bCanSeePlayer
					AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
						SET_SITE_STATE(AH2_SS_FLEEING_PLAYER)
					ENDIF
				ENDIF
				
				IF NOT sConstructionWorkers[iTemp].bCombat
				
					IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
					AND NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
						CLEAR_PED_TASKS(sConstructionWorkers[iTemp].ped)
						IF IS_PED_USING_ANY_SCENARIO(sConstructionWorkers[iTemp].ped)
							SET_PED_PANIC_EXIT_SCENARIO(sConstructionWorkers[iTemp].ped, GET_ENTITY_COORDS(PLAYER_PED_ID()))
						ENDIF
						
						SET_PED_RELATIONSHIP_GROUP_HASH(sConstructionWorkers[iTemp].ped, RELGROUPHASH_HATES_PLAYER)
						SET_PED_COMBAT_ATTRIBUTES(sConstructionWorkers[iTemp].ped, CA_ALWAYS_FIGHT, TRUE)
						SET_PED_FLEE_ATTRIBUTES(sConstructionWorkers[iTemp].ped, FA_NEVER_FLEE, TRUE)
						SET_PED_COMBAT_RANGE(sConstructionWorkers[iTemp].ped, CR_FAR)
						TASK_COMBAT_PED(sConstructionWorkers[iTemp].ped, PLAYER_PED_ID())
						sConstructionWorkers[iTemp].bCombat = TRUE
					ENDIF
					
				ENDIF
				
			ENDREPEAT
			
			CLEANUP_WORKERS(TRUE, TRUE, FALSE, 30)
			
			IF GET_GAME_TIMER() - iTimeOfLastChaseChat > 3000
			AND NOT IS_MESSAGE_BEING_DISPLAYED()
				IF DO_ARCHITECT_FLEEING_LINE("F3A_JANRUN", "F3A_CONRUN")
					iTimeOfLastChaseChat = GET_GAME_TIMER()
				ENDIF
			ENDIF
			
		BREAK
		
		CASE AH2_SS_FLEEING_PLAYER
			//flee the player
			REPEAT COUNT_OF(sConstructionWorkers) iTemp
			
				IF NOT sConstructionWorkers[iTemp].bFlee
				
					IF IS_PLAYER_ON_TOP_FLOOR()
						IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
						AND NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
							CLEAR_PED_TASKS(sConstructionWorkers[iTemp].ped)
							SET_PED_RELATIONSHIP_GROUP_HASH(sConstructionWorkers[iTemp].ped, RELGROUPHASH_HATES_PLAYER)
							SET_PED_COMBAT_ATTRIBUTES(sConstructionWorkers[iTemp].ped, CA_ALWAYS_FIGHT, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(sConstructionWorkers[iTemp].ped, CA_ALWAYS_FLEE, TRUE)
							SET_PED_FLEE_ATTRIBUTES(sConstructionWorkers[iTemp].ped, FA_NEVER_FLEE, FALSE)
							SET_PED_COMBAT_RANGE(sConstructionWorkers[iTemp].ped, CR_FAR)
							sConstructionWorkers[iTemp].bFlee = TRUE
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
						AND NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
							CLEAR_PED_TASKS(sConstructionWorkers[iTemp].ped)
							IF IS_PED_USING_ANY_SCENARIO(sConstructionWorkers[iTemp].ped)
								SET_PED_PANIC_EXIT_SCENARIO(sConstructionWorkers[iTemp].ped, GET_ENTITY_COORDS(PLAYER_PED_ID()))
							ENDIF
							SET_PED_RELATIONSHIP_GROUP_HASH(sConstructionWorkers[iTemp].ped, RELGROUPHASH_HATES_PLAYER)
							SET_PED_COMBAT_ATTRIBUTES(sConstructionWorkers[iTemp].ped, CA_ALWAYS_FIGHT, FALSE)
							SET_PED_COMBAT_ATTRIBUTES(sConstructionWorkers[iTemp].ped, CA_ALWAYS_FLEE, TRUE)
							SET_PED_FLEE_ATTRIBUTES(sConstructionWorkers[iTemp].ped, FA_NEVER_FLEE, FALSE)
							SET_PED_COMBAT_RANGE(sConstructionWorkers[iTemp].ped, CR_FAR)
							IF iTemp % 3 = 0
								TASK_REACT_AND_FLEE_PED(sConstructionWorkers[iTemp].ped, PLAYER_PED_ID())
							ELSE
								TASK_SMART_FLEE_PED(sConstructionWorkers[iTemp].ped, PLAYER_PED_ID(), 500, -1)
							ENDIF
							SET_CURRENT_PED_WEAPON(sConstructionWorkers[iTemp].ped, WEAPONTYPE_UNARMED, TRUE)
							sConstructionWorkers[iTemp].bFlee = TRUE
						ENDIF
					ENDIF
					
				ELSE
				
					IF IS_PLAYER_ON_TOP_FLOOR()
						IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
						AND NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
							IF NOT IS_ENTITY_AT_COORD(sConstructionWorkers[iTemp].ped, <<-159.4073, -1024.2125, 113.3052>>, <<4,4,4>>)
								IF GET_SCRIPT_TASK_STATUS(sConstructionWorkers[iTemp].ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK
								AND GET_SCRIPT_TASK_STATUS(sConstructionWorkers[iTemp].ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
								AND GET_SCRIPT_TASK_STATUS(sConstructionWorkers[iTemp].ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> DORMANT_TASK
									TASK_FOLLOW_NAV_MESH_TO_COORD(sConstructionWorkers[iTemp].ped, <<-159.4073, -1024.2125, 113.3052>>, PEDMOVEBLENDRATIO_SPRINT, DEFAULT_TIME_NEVER_WARP, 3.0- TO_FLOAT(iTemp))
								ENDIF
							ELSE
								IF NOT bTopFloorCower[iTemp]
									TASK_COWER(sConstructionWorkers[iTemp].ped)
									bTopFloorCower[iTemp] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
			ENDREPEAT
			
			CLEANUP_WORKERS(TRUE, TRUE, FALSE, 30)
			
			IF GET_GAME_TIMER() - iTimeOfLastChaseChat > 3000
			AND NOT IS_MESSAGE_BEING_DISPLAYED()
				IF DO_ARCHITECT_FLEEING_LINE("F3A_JANRUN", "F3A_CONFLEE")
					iTimeOfLastChaseChat = GET_GAME_TIMER()
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	IF eSiteState = AH2_SS_ALL_QUIET
	OR eSiteState = AH2_SS_SPOTTED_PLAYER
		REPEAT COUNT_OF(sConstructionWorkers) iTemp
			IF DOES_ENTITY_EXIST(pedArchitect)
			AND (IS_ENTITY_DEAD(pedArchitect) OR IS_PED_INJURED(pedArchitect))
				IF NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
				AND VDIST2(GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped), GET_ENTITY_COORDS(pedArchitect)) < 20*20
					IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(sConstructionWorkers[iTemp].ped, pedArchitect)
						IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
						AND GET_PLAYER_WEAPON() <> WEAPONTYPE_STUNGUN
							eSiteState = AH2_SS_FLEEING_PLAYER
						ELSE
							PRINTLN("\n\n\n Dead or injured architect seen by worker \n\n\n")
							eSiteState = AH2_SS_ATTACKING_PLAYER
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF eSiteState < AH2_SS_ATTACKING_PLAYER
		BOOL bFleeCheck
		IF IS_PLAYER_ON_TOP_FLOOR()
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			IF CHECK_TOP_FLOOR_SITE_FAILS()
				PRINTLN("ALERT WHOLE SITE 4")
				ALERT_WHOLE_SITE(bTopFloorPedsFlee)
			ENDIF
		ELSE
			IF CHECK_SITE_FAILS(bFleeCheck)
				PRINTLN("ALERT WHOLE SITE 5")
				ALERT_WHOLE_SITE(bFleeCheck)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC FORCE_SITE_TO_STAGE(SITE_STATE sState, AMBIENT_STAGE eAmbStage)

	DESTROY_ALL_WORKERS(TRUE)
	
	SWITCH eAmbStage
		CASE AH2_AS_SETUP_GROUNDFLOOR1
		CASE AH2_AS_RUN_GROUNDFLOOR
			eAmbientSiteStage = eAmbStage
		BREAK
		CASE AH2_AS_SETUP_TOPFLOOR
		CASE AH2_AS_RUN_TOP_FLOOR
			eAmbientSiteStage = eAmbStage
		BREAK
		CASE AH2_AS_SETUP_GROUNDFLOOR2
		CASE AH2_AS_RUN_GROUNDFLOOR2
			eAmbientSiteStage = eAmbStage
		BREAK
	ENDSWITCH
	
	eSiteState = sState
	
ENDPROC

PROC initialiseMission()

	SET_ALL_CONSTRUCTION_SCENARIOS_BLOCKED()
	
	#IF IS_DEBUG_BUILD
		skipMenuOptions[0].sTxtLabel = "Ground floor"
		skipMenuOptions[1].sTxtLabel = "First floor"
		skipMenuOptions[2].sTxtLabel = "Flee site"
		skipMenuOptions[3].sTxtLabel = "Call Mike"
	#ENDIF
	
	REQUEST_MODEL(PROP_LD_CASE_01)
	REQUEST_MODEL(PROP_CHAIR_06)
	REQUEST_MODEL(PROP_HARD_HAT_01)
	REQUEST_MODEL(PROP_TOOL_BOX_05)
	REQUEST_MODEL(U_M_M_FIBARCHITECT)
	REQUEST_ADDITIONAL_TEXT("H1SET3A", MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT(sBlockID, MISSION_DIALOGUE_TEXT_SLOT)
	REQUEST_WEAPON_ASSET(WEAPONTYPE_BRIEFCASE_02, ENUM_TO_INT(WRF_REQUEST_MOTION_ANIMS))
	
	WHILE NOT HAS_MODEL_LOADED(PROP_LD_CASE_01)
	OR NOT HAS_MODEL_LOADED(PROP_CHAIR_06)
	OR NOT HAS_MODEL_LOADED(PROP_HARD_HAT_01)
	OR NOT HAS_MODEL_LOADED(PROP_TOOL_BOX_05)
	OR NOT HAS_MODEL_LOADED(U_M_M_FIBARCHITECT)
	OR NOT REQUEST_SCRIPT_AUDIO_BANK("Freight_Elevator")
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
	OR NOT LOAD_STREAM("Construction_Site_Stream", "FBI_HEIST_SOUNDSET")
		WAIT(0)
	ENDWHILE
	
	sArchDeskAnims[0] = "look_at_plan_a"
	sArchDeskAnims[1] = "look_at_plan_b"
	sArchDeskAnims[2] = "look_at_plan_c"
	sArchDeskAnims[3] = "look_at_plan_d"
	sArchDeskAnims[4] = "look_at_plan_e"
	
	sArchConvLabel[0] = "F3A_ARCH"
	sArchConvLabel[1] = "F3A_ARCH2"
	sArchConvLabel[2] = "F3A_ARCH3"
	sArchConvLabel[3] = "F3A_ARCH4"
	sArchConvLabel[4] = "F3A_ARCH5"
	
	PLAY_STREAM_FROM_POSITION(<<-146,-990,33>>)
	
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
	
		SET_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID(), ANCHOR_HEAD, 7)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		IF IS_REPLAY_IN_PROGRESS()
			PRINTLN("FOUND REPLAY")
			REMOVE_CUTSCENE()
			FADE_OUT()
			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)
			
			INT iStage = GET_REPLAY_MID_MISSION_STAGE()
			
			IF g_bShitskipAccepted = TRUE
				iStage++
			ENDIF
			
			PRINTLN("REPLAY STAGE IS ", iStage)
			
			IF iStage > 3
			
				START_REPLAY_SETUP(vOffice, 122.1115)
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
				END_REPLAY_SETUP()
				Mission_Passed()
				
			ELSE
			
				SWITCH iStage
				
					CASE 0
						vReplayCoords = << -139.9581, -971.2977, 113.1330 >>
						fReplayHeading = 332.4719
						mission_stage = STAGE_GROUND_FLOOR
					BREAK
					
					CASE 1
						vReplayCoords = <<-180.1966, -1017.0390, 113.2072>>
						fReplayHeading = 340.0900
						mission_stage = STAGE_FIRST_FLOOR
						TRIGGER_MUSIC_EVENT("AH2A_FIRST_FLOOR_RESTART")
					BREAK
					
					CASE 2
						vReplayCoords = << -139.9581, -971.2977, 113.1330 >>
						fReplayHeading = 332.4719
						mission_stage = STAGE_ESCAPE
						SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "Architect mugged", FALSE)
					BREAK
					
					CASE 3
						IF NOT g_bShitskipAccepted
							IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
								PRINTLN("-*- Grabbed global arch car")
								SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[1], TRUE, TRUE)
								vehArchitectCar = g_sTriggerSceneAssets.veh[1]
								g_sTriggerSceneAssets.veh[1] = NULL
							ELSE
								PRINTLN("-*- vehicle not present")
							ENDIF
						ENDIF
						vReplayCoords = << -77.7099, -1019.9009, 27.5262 >>
						fReplayHeading = 315.6337 
						mission_stage = STAGE_DRIVE_BACK
					BREAK
					
				ENDSWITCH
				
				bSkip = TRUE
				
				START_REPLAY_SETUP(vReplayCoords, fReplayHeading)
				
			ENDIF
			
		ENDIF
		
	ENDIF
	
	IF IS_REPEAT_PLAY_ACTIVE()
		SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_DESTROYED)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_DESTROYED) 
		SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_DESTROYED) 
		SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_NORMAL) 
		SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_NORMAL)
		SET_DOOR_STATE(DOORNAME_SWEATSHOP_L, DOORSTATE_UNLOCKED)
		SET_DOOR_STATE(DOORNAME_SWEATSHOP_R, DOORSTATE_UNLOCKED)
	ENDIF
	
	SET_ROADS_IN_AREA(<<-19.16, -969.91, 28.30>>, <<-111.44, -1114.63, 24.77>>, FALSE)
	CLEAR_AREA(vArchCarStart, 500, TRUE)
	WASH_DECALS_IN_RANGE(vArchCarStart, 1000, 1.0)
	
	WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_FRANKLIN)
		WAIT(0)
	ENDWHILE
	
	objChair = CREATE_OBJECT(PROP_CHAIR_06, <<-90.8550, -1031.8630, 27.0969>>)
	SET_ENTITY_QUATERNION(objChair, -0.0000, 0.0000, 0.7268, -0.6868)
	FREEZE_ENTITY_POSITION(objChair, TRUE)
	
	objBox = CREATE_OBJECT(PROP_TOOL_BOX_05, <<-92.0505, -1032.0500, 27.1212>>)
	SET_ENTITY_QUATERNION(objBox, 0.0000, 0.0000, 0.7665, 0.6423)
	FREEZE_ENTITY_POSITION(objBox, TRUE)
	
	ADD_PED_FOR_DIALOGUE(sSpeech, 0, PLAYER_PED_ID(), "FRANKLIN")
	ADD_PED_FOR_DIALOGUE(sSpeech, 3, NULL, "MICHAEL")
	DISABLE_TAXI_HAILING(TRUE)
	
	vTopFloorLiftApproach[0] = <<-179.2060, -1013.2731, 113.1372>>
	vTopFloorLiftApproach[1] = <<-182.7186, -1011.8274, 113.1388>>
	vTopFloorLiftApproach[2] = <<-154.6769, -941.7866, 113.1388>>
	vTopFloorLiftApproach[3] = <<-155.5882, -945.6394, 113.1388>>
	
	vTopFloorLiftStandPos[0] = <<-180.2197, -1016.7516, 113.2066>>
	vTopFloorLiftStandPos[1] = <<-183.6155, -1015.3174, 113.2066>>
	vTopFloorLiftStandPos[2] = <<-157.7097, -940.4479, 113.2066>>
	vTopFloorLiftStandPos[3] = <<-159.8848, -943.6281, 113.2066>>
	
	INIT_FLEE_ROUTES()
	
	ADD_RELATIONSHIP_GROUP("construction workers", rgConstruction)
	ADD_RELATIONSHIP_GROUP("agency2setupbg", rgBuddies)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgConstruction, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rgBuddies, RELGROUPHASH_PLAYER)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_CAR), TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BUFFALO, TRUE)
	
	
	bSkip = TRUE
	
ENDPROC

PROC INIT_LIFT_VARIABLES()
	INIT_LIFT(constructionLifts[LS_SOUTH_A	], <<-182.19, -1016.54, 31.36>>, 	<<-182.19, -1016.19, 115.51>>, 	<<-0.0, 0.0,   70.09>>, TRUE, TRUE, FALSE)
	INIT_LIFT(constructionLifts[LS_SOUTH_B	], <<-182.19, -1016.54, 31.36>>, 	<<-182.19, -1016.19, 115.51>>, 	<<-0.0, 0.0, -110.09>>, TRUE, TRUE, TRUE)
	INIT_LIFT(constructionLifts[LS_WEST_A	], <<-158.78, -942.25, 31.36>>, 	<<-158.78, -942.25, 115.51>>, 	<<-0.0, 0.0,  160.09>>, TRUE, TRUE, TRUE)
	INIT_LIFT(constructionLifts[LS_WEST_B	], <<-158.78, -942.25, 31.36>>, 	<<-158.78, -942.25, 115.51>>, 	<<-0.0, 0.0,  -20.09>>, TRUE, TRUE, FALSE)
	constructionLifts[0].vCameraPos = <<-0.8000, -1.7200, -0.8000>>
	constructionLifts[0].vCameraRot = <<-1.4400, 0.0000, 166.3200>>
	constructionLifts[1].vCameraPos = <<0.8000, -1.7200, -0.8000>>
	constructionLifts[1].vCameraRot = <<-1.4400, 0.0000, 153.3600>>
	constructionLifts[2].vCameraPos = <<0.8000, -1.7200, -0.8000>>
	constructionLifts[2].vCameraRot = <<-1.4400, 0.0000, 64.4400>>
	constructionLifts[3].vCameraPos = <<-0.8000, -1.7200, -0.8000>>
	constructionLifts[3].vCameraRot = <<-1.4400, 0.0000, 75.6000>>
ENDPROC

#IF IS_DEBUG_BUILD
	
	PROC jSkip()
		IF mission_stage = STAGE_GROUND_FLOOR
		AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), constructionLifts[LS_SOUTH_B].vPos1) > 50
			PRINTLN("SKIP ONE")
			IF NOT IS_PED_INJURED(pedArchitect)
				IF DOES_CAM_EXIST(cutCam)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_CAM_ACTIVE(cutCam, FALSE)
					DESTROY_CAM(cutCam)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF
				CLEAR_PRINTS()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				KILL_ANY_CONVERSATION()
				HANG_UP_AND_PUT_AWAY_PHONE()
				SET_ENTITY_COORDS(pedArchitect, << -184.8003, -1018.6793, 29.0599 >>)
				SET_ENTITY_HEADING(pedArchitect, 340.0000)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -186.5275, -1034.6732, 26.1941 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 321.9427)
				iStageProgress = 4
				REMOVE_BLIP(blipArchitect)
				WARP_LIFT_TO_STATE(constructionLifts[LS_SOUTH_A], 0)
				WARP_LIFT_TO_STATE(constructionLifts[LS_SOUTH_B], 0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
		ELIF mission_stage < STAGE_ESCAPE
			PRINTLN("SKIP TWO")
			DO_FULL_RESET_FOR_SKIP()
			mission_stage = INT_TO_ENUM(MISSION_STAGE_FLAG, ENUM_TO_INT(mission_stage)+1)
			iStageProgress = 0
			bSkip = TRUE
		ELIF mission_stage = STAGE_ESCAPE
			PRINTLN("SKIP THREE")
			DO_FULL_RESET_FOR_SKIP()
			mission_stage = INT_TO_ENUM(MISSION_STAGE_FLAG, STAGE_DRIVE_BACK)
			iStageProgress = 0
			bSkip = TRUE
		ENDIF
	ENDPROC
	
	PROC pSkip()
		IF mission_stage = STAGE_GROUND_FLOOR
			DO_FULL_RESET_FOR_SKIP()
			mission_stage = STAGE_GROUND_FLOOR
			iStageProgress = 0
			bSkip = TRUE
		ENDIF
		IF mission_stage > STAGE_GROUND_FLOOR
			DO_FULL_RESET_FOR_SKIP()
			mission_stage = INT_TO_ENUM(MISSION_STAGE_FLAG, ENUM_TO_INT(mission_stage)-1)
			iStageProgress = 0
			bSkip = TRUE
		ENDIF
	ENDPROC
	
	PROC debugProcedures()
	
		IF bDebugInitialised = FALSE
			//Init debug stuff.			
			bDebugInitialised = TRUE
		ENDIF
		
		IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
			IF NOT IS_PED_INJURED(pedArchitect)
				CLEAR_PED_TASKS_IMMEDIATELY(pedArchitect)
			ENDIF
			MISSION_PASSED()
		ENDIF
		
		IF IS_KEYBOARD_KEY_PRESSED(KEY_f)	
			MISSION_FAILED("S3A_SCARED")
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
			jSkip()	
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P) 
			pSkip()
		ENDIF
		
		IF LAUNCH_MISSION_STAGE_MENU(skipMenuOptions, iStageHolder)
			DO_FULL_RESET_FOR_SKIP()
			mission_stage = INT_TO_ENUM(MISSION_STAGE_FLAG, iStageHolder)
			bSkip = TRUE
		ENDIF
		
		IF bSkip
			INT iTemp
			REPEAT COUNT_OF(constructionLifts) iTemp
				constructionLifts[iTemp].passenger = NULL
				STOP_LIFT_SOUND(constructionLifts[iTemp])
			ENDREPEAT
			TRIGGER_MUSIC_EVENT("AH2A_MISSION_FAIL")
			SET_WIDESCREEN_BORDERS(FALSE, 0)
		ENDIF
		
	ENDPROC
	
#ENDIF

FUNC BOOL SETUP_PLAYER_CAR(VECTOR coords, FLOAT heading)
	IF NOT DOES_ENTITY_EXIST(vehPlayerCar)
		REQUEST_MODEL(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_CAR))
		IF HAS_MODEL_LOADED(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_CAR))
			CREATE_PLAYER_VEHICLE(vehPlayerCar, CHAR_FRANKLIN, coords, heading, TRUE, VEHICLE_TYPE_CAR)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehPlayerCar)
	AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		SET_ENTITY_COORDS(vehPlayerCar, coords)
		SET_ENTITY_HEADING(vehPlayerCar, heading)
		SET_ENTITY_QUATERNION(vehPlayerCar, -0.0256, -0.0077, 0.9857, 0.1664)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_ARCHITECT_CAR(VECTOR coords, FLOAT heading)
	IF NOT DOES_ENTITY_EXIST(vehArchitectCar)
		REQUEST_MODEL(ZION2)
		IF HAS_MODEL_LOADED(ZION2)
			vehArchitectCar = CREATE_VEHICLE(ZION2, coords, heading)
			SET_VEHICLE_COLOURS(vehArchitectCar, 33,0)
			SET_VEHICLE_EXTRA_COLOURS(vehArchitectCar, 69, 0)
			SET_DISABLE_PRETEND_OCCUPANTS(vehArchitectCar, FALSE)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehArchitectCar)
	AND IS_VEHICLE_DRIVEABLE(vehArchitectCar)
		SET_ENTITY_COORDS(vehArchitectCar, coords)
		SET_ENTITY_HEADING(vehArchitectCar, heading)
		SET_VEHICLE_ALARM(vehArchitectCar, TRUE)
		println("ARCH CAR DONE")
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

STRING sHeliRecString = "theagencytwo"
INT iHeliRecNum = 1

PROC DO_HELI_FLY_BY()
	IF NOT DOES_ENTITY_EXIST(vehHeli)
	AND NOT bHeliOneDone
		REQUEST_MODEL(FROGGER)
		REQUEST_VEHICLE_RECORDING(iHeliRecNum, sHeliRecString)
		IF HAS_MODEL_LOADED(FROGGER)
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iHeliRecNum, sHeliRecString)
			vehHeli = CREATE_VEHICLE(FROGGER, GET_ENTITY_COORDS(PLAYER_PED_ID()) + <<0,0,800>>)
			START_PLAYBACK_RECORDED_VEHICLE(vehHeli, iHeliRecNum, sHeliRecString)
			SET_PLAYBACK_SPEED(vehHeli, 0.75)
			PRINTLN("Heli created")
		ENDIF
	ELSE
		IF IS_VEHICLE_DRIVEABLE(vehHeli)
			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehHeli)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehHeli)
				DELETE_VEHICLE(vehHeli)
				SET_MODEL_AS_NO_LONGER_NEEDED(FROGGER)
				REMOVE_VEHICLE_RECORDING(iHeliRecNum, sHeliRecString)
				PRINTLN("Heli destroyed")
				bHeliOneDone = TRUE
			ELSE
				SET_HELI_BLADES_FULL_SPEED(vehHeli)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

INT iTakeOffStage
FUNC BOOL DO_HAT_TAKE_OFF()
	SWITCH iTakeOffStage
		CASE 0
			IF GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD) = 7
				REQUEST_MODEL(PROP_HARD_HAT_01)
				REQUEST_ANIM_DICT("veh@bike@common@Front@base")
				IF HAS_ANIM_DICT_LOADED("veh@bike@common@Front@base")
				AND HAS_MODEL_LOADED(PROP_HARD_HAT_01)
					IF DOES_ENTITY_EXIST(objHat)
						DELETE_OBJECT(objHat)
					ENDIF
					TASK_PLAY_ANIM(PLAYER_PED_ID(), "veh@bike@common@Front@base", "take_off_helmet_walk", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
					iTakeOffStage++
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
		CASE 1
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "veh@bike@common@Front@base", "take_off_helmet_walk")
				IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "veh@bike@common@Front@base", "take_off_helmet_walk") > 0.7
					objHat = CREATE_OBJECT(PROP_HARD_HAT_01, GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_PH_L_HAND, <<0,0,0>>))
					ATTACH_ENTITY_TO_ENTITY(objHat, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>)
					CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
					iTakeOffStage++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "veh@bike@common@Front@base", "take_off_helmet_walk")
				IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "veh@bike@common@Front@base", "take_off_helmet_walk") > 0.95
					IF IS_ENTITY_ATTACHED(objHat)
						DETACH_ENTITY(objHat)
						SET_OBJECT_AS_NO_LONGER_NEEDED(objHat)
						iTakeOffStage=0
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	#IF IS_DEBUG_BUILD
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "veh@bike@common@Front@base", "take_off_helmet_walk")
			DRAW_DEBUG_TEXT(GET_STRING_FROM_FLOAT(GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "veh@bike@common@Front@base", "take_off_helmet_walk")), GET_ENTITY_COORDS(PLAYER_PED_ID()))
		ENDIF
	#ENDIF
	RETURN FALSE
ENDFUNC

PROC PLAYER_SPOTTED_BY_ARCHITECT()

	SWITCH iStageProgress
	
		CASE 0
			SETTIMERB(0)
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedArchitect)
				CLEAR_PED_TASKS(pedArchitect)
			ENDIF
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iGroundFloorChatScene)
				IF NOT IS_PED_INJURED(sConstructionWorkers[7].ped)
					CLEAR_PED_TASKS(sConstructionWorkers[7].ped)
				ENDIF
				IF NOT IS_PED_INJURED(sConstructionWorkers[8].ped)
					CLEAR_PED_TASKS(sConstructionWorkers[8].ped)
				ENDIF
			ENDIF
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iArchScene)
				IF NOT IS_PED_INJURED(sConstructionWorkers[0].ped)
					CLEAR_PED_TASKS(sConstructionWorkers[0].ped)
				ENDIF
				IF NOT IS_PED_INJURED(sConstructionWorkers[1].ped)
					CLEAR_PED_TASKS(sConstructionWorkers[1].ped)
				ENDIF
			ENDIF
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			REPLAY_RECORD_BACK_FOR_TIME(3)
			iStageProgress++
		BREAK
		
		CASE 1
			DO_ARCHITECT_CONTINUALLY_FACES_PLAYER()
			IF HAS_PLAYER_SPOOKED_ARCHITECT()
				GO_TO_STAGE(STAGE_COVER_BLOWN)
			ELSE
				IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_WHORU", CONV_PRIORITY_HIGH)
					SETTIMERB(0)
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
				fTimeArmed += GET_FRAME_TIME()
			ENDIF
			IF TIMERB() > 3000
			OR fTimeArmed > 1.5
			OR  HAS_PLAYER_SPOOKED_ARCHITECT()
				IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_SECURITY", CONV_PRIORITY_HIGH)
					PRINTLN("ALERT WHOLE SITE 6")
					ALERT_WHOLE_SITE(fTimeArmed > 1.5)
					GO_TO_STAGE(STAGE_COVER_BLOWN)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC COVER_BLOWN()

	IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_2_COVER_BLOWN")
		START_AUDIO_SCENE("AGENCY_H_2_COVER_BLOWN")
	ENDIF
	
	SWITCH iStageProgress
	
		CASE 0
		
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				AND NOT IS_ENTITY_IN_ANY_LIFT(PLAYER_PED_ID())
					PRINTLN("TRUNING PLAYER CONTROL ON")
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, FALSE)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			IF IS_GAMEPLAY_HINT_ACTIVE()
				PRINTLN("KILLING HINT CAM")
				STOP_GAMEPLAY_HINT()
			ENDIF
			
			IF DOES_ENTITY_EXIST(objHat)
				IF IS_ENTITY_ATTACHED(objHat)
					DETACH_ENTITY(objHat)
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(sConstructionWorkers[0].ped)
			AND DOES_ENTITY_EXIST(sConstructionWorkers[0].board)
			AND IS_ENTITY_ATTACHED(sConstructionWorkers[0].board)
				DETACH_ENTITY(sConstructionWorkers[0].board)
			ENDIF
			
			IF NOT IS_PED_INJURED(sConstructionWorkers[7].ped)
			AND DOES_ENTITY_EXIST(sConstructionWorkers[7].board)
			AND IS_ENTITY_ATTACHED(sConstructionWorkers[7].board)
				DETACH_ENTITY(sConstructionWorkers[7].board)
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iGroundFloorChatScene)
				IF NOT IS_PED_INJURED(sConstructionWorkers[7].ped)
					CLEAR_PED_TASKS(sConstructionWorkers[7].ped)
				ENDIF
				IF NOT IS_PED_INJURED(sConstructionWorkers[8].ped)
					CLEAR_PED_TASKS(sConstructionWorkers[8].ped)
				ENDIF
				IF NOT IS_PED_INJURED(pedArchitect)
					CLEAR_PED_TASKS(pedArchitect)
				ENDIF
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iArchScene)
				IF NOT IS_PED_INJURED(sConstructionWorkers[0].ped)
					CLEAR_PED_TASKS(sConstructionWorkers[0].ped)
				ENDIF
				IF NOT IS_PED_INJURED(sConstructionWorkers[1].ped)
					CLEAR_PED_TASKS(sConstructionWorkers[1].ped)
				ENDIF
				IF NOT IS_PED_INJURED(pedArchitect)
					CLEAR_PED_TASKS(pedArchitect)
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iWorkerScene)
				IF NOT IS_PED_INJURED(sConstructionWorkers[0].ped)
					CLEAR_PED_TASKS(sConstructionWorkers[0].ped)
				ENDIF
				IF NOT IS_PED_INJURED(sConstructionWorkers[1].ped)
					CLEAR_PED_TASKS(sConstructionWorkers[1].ped)
				ENDIF
				IF NOT IS_PED_INJURED(pedArchitect)
					CLEAR_PED_TASKS(pedArchitect)
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			ENDIF
			
		BREAK
		
		CASE 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND NOT IS_CASE_TEXT_SHOWING()
				IF DO_ARCHITECT_FLEEING_LINE("F3A_JANRUN", "")
					iStageProgress++
				ENDIF
			ELSE
				IF TIMERA() > 5000
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
		
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND NOT IS_CASE_TEXT_SHOWING()
				IF NOT IS_PED_INJURED(pedArchitect)
				AND NOT IS_PED_BEING_STEALTH_KILLED(pedArchitect)
				AND NOT IS_PED_BEING_STUNNED(pedArchitect)
				AND DOES_ENTITY_EXIST(objPlans)
				AND IS_ENTITY_ATTACHED_TO_ENTITY(objPlans, pedArchitect)
					PRINTLN("TAKEDOWN 1")
					PRINT("S3A_TAKEDOWN", DEFAULT_GOD_TEXT_TIME, 1) 
				ENDIF
				SETTIMERA(0)
				iStageProgress++
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	IF DOES_BLIP_EXIST(blipArchitect)
		IF NOT DOES_BLIP_EXIST(blipPlans)
			IF GET_BLIP_HUD_COLOUR(blipArchitect) <> HUD_COLOUR_ENEMY
				REMOVE_BLIP(blipArchitect)
				IF NOT IS_PED_INJURED(pedArchitect)
					blipArchitect = CREATE_BLIP_FOR_PED(pedArchitect, TRUE)
				ENDIF
			ENDIF
		ELSE
			REMOVE_BLIP(blipPlans)
		ENDIF
	ENDIF
	
	IF DOES_BLIP_EXIST(blipPlans)
		IF DOES_BLIP_EXIST(blipArchitect)
			REMOVE_BLIP(blipArchitect)
		ENDIF
	ENDIF
	
//	IF DOES_BLIP_EXIST(blipArchitect)
//		PRINTLN("blip architect exists")
//		IF DOES_BLIP_EXIST(blipPlans)
//			PRINTLN("blip plans exists")
//			REMOVE_BLIP(blipArchitect)
//		ENDIF
//	ENDIF
	
	MANAGE_FLEEING_ARCHITECT()
	
	IF DOES_ENTITY_EXIST(objPlans)
	AND NOT IS_ENTITY_ATTACHED(objPlans)
		BOOL bLostPlot = FALSE
		FLOAT fDistToPlans = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(objPlans))
		IF IS_ENTITY_OCCLUDED(objPlans)
			IF fDistToPlans > 100.0
				bLostPlot = TRUE
			ENDIF
			IF fDistToPlans > 5.0
				fLostPlansTime += GET_FRAME_TIME()
			ENDIF
			IF fLostPlansTime > 20.0
				bLostPlot = TRUE
			ENDIF
		ELSE
			IF fLostPlansTime > 0.0
				fLostPlansTime -= GET_FRAME_TIME()*2
			ELSE
				fLostPlansTime = 0.0
			ENDIF
		ENDIF
		
		IF bLostPlot
			IF DOES_BLIP_EXIST(blipPlans)
				REMOVE_BLIP(blipPlans)
			ENDIF
			Mission_Failed("S3A_LFTCASE")
		ENDIF
			
	ELSE
		HAS_PLAYER_LOST_ARCHITECT()
	ENDIF
	
ENDPROC

FUNC BOOL HAS_PLAYER_BLOWN_MUGGING()
	IF IS_PED_INJURED(pedArchitect)
	OR IS_ENTITY_DEAD(pedArchitect)
	OR IS_PED_BEING_STEALTH_KILLED(pedArchitect)
		RETURN FALSE
	ENDIF
	IF (GET_GAME_TIMER() - iMugFailCheckTImer) > 500
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 10 * 10
			IF CAN_ARCHITECT_SEE_PLAYER()
				iArchitectSightTimer++
			ELSE
				iArchitectSightTimer = 0
			ENDIF
			iMugFailCheckTImer = GET_GAME_TIMER()
		ENDIF
	ENDIF
	IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 2*2
		IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedArchitect)
			iTimeOfTouch = GET_GAME_TIMER()
		ELSE
			iTimeOfTouch = 0
		ENDIF
	ELSE
		iTimeOfTouch = 0
	ENDIF
	RETURN iArchitectSightTimer > 1 
	OR (iTimeOfTouch <> 0 AND (GET_GAME_TIMER() - iTimeOfTouch) > 1000)
ENDFUNC


BOOL bArchLine = FALSE

PROC GROUND_FLOOR_SECTION()

	IF IS_ENTITY_IN_ANY_LIFT(PLAYER_PED_ID())
	AND constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(PLAYER_PED_ID())].bInTransit
	AND constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(PLAYER_PED_ID())].passenger = PLAYER_PED_ID()
		IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_2_FOLLOW_ARCHITECT_1")
			STOP_AUDIO_SCENE("AGENCY_H_2_FOLLOW_ARCHITECT_1")
		ENDIF
		IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_2_USE_ELEVATOR")
			START_AUDIO_SCENE("AGENCY_H_2_USE_ELEVATOR")
		ENDIF
	ENDIF
	
	//Dopey little lookabout thing
	IF NOT IS_ENTITY_IN_LIFT(constructionLifts[LS_SOUTH_A], PLAYER_PED_ID())
		IF GET_GAME_TIMER() - iLastLookAtTask > GET_RANDOM_INT_IN_RANGE(1000, 2000)
			IF DOES_ENTITY_EXIST(objPlans)
			AND NOT IS_GAMEPLAY_HINT_ACTIVE()
				IF IS_ENTITY_ATTACHED(objPlans)
					IF NOT IS_PED_INJURED(pedArchitect)
					AND VDIST2(GET_ENTITY_COORDS(pedArchitect), GET_ENTITY_COORDS(PLAYER_PED_ID())) < POW(15,2)
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedArchitect, 3000, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
						iLastLookAtTask = GET_GAME_TIMER()
					ELIF DOES_ENTITY_EXIST(constructionLifts[LS_SOUTH_A].obj)
					AND VDIST2(GET_ENTITY_COORDS(constructionLifts[LS_SOUTH_A].obj), GET_ENTITY_COORDS(PLAYER_PED_ID())) < POW(10,2)
						TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), constructionLifts[LS_SOUTH_A].obj, 3000, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
						iLastLookAtTask = GET_GAME_TIMER()
					ELSE
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), constructionLifts[LS_SOUTH_A].vPos1) < 20*20
							IF bLastLookRight
								TASK_LOOK_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<-5, 5, 0>>), 3000, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
							ELSE
								TASK_LOOK_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<5, 5, 0>>), 3000, SLF_DEFAULT, SLF_LOOKAT_VERY_HIGH)
							ENDIF
							bLastLookRight = !bLastLookRight
							iLastLookAtTask = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehArchitectCar)
	AND NOT IS_VEHICLE_ALARM_ACTIVATED(vehArchitectCar)
	
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehArchitectCar)) < 25
		AND IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), vehArchitectCar)
		AND IS_PED_RAGDOLL(PLAYER_PED_ID())
			START_VEHICLE_ALARM(vehArchitectCar)
		ENDIF
		
		VEHICLE_INDEX vehTemp
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
				vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())		
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
				vehTemp = GET_PLAYERS_LAST_VEHICLE()
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehTemp)
			IF VDIST2(GET_ENTITY_COORDS(vehTemp), GET_ENTITY_COORDS(vehArchitectCar)) < 25
				IF IS_ENTITY_TOUCHING_ENTITY(vehTemp, vehArchitectCar)
					START_VEHICLE_ALARM(vehArchitectCar)
				ENDIF
			ENDIF
		ENDIF
		
		vehTemp = NULL
		
	ENDIF
	
	SWITCH iStageProgress
	
		CASE 0
		
			REMOVE_BLIPS()
			INIT_TEXT()
			
			RESET_ALL_GROUNDFLOOR_VARIABLES()
			RESET_MUGGING()
			
			REQUEST_WAYPOINT_RECORDING("agency2Aground")
			REQUEST_ANIM_DICT("missheist_agency2aig_2")
			REQUEST_ANIM_DICT("missheist_agency2aig_1")
			REQUEST_SCRIPT_AUDIO_BANK("SCRIPT\\FBI_Heist_Mug_Architect")
			
			bPreHatSHout = FALSE
			bArchLine = FALSE
			
			IF bSkip
			
				eRunawayState = AH2_RS_FOLLOW_PATH_TOP_FLOOR
				
				RESET_ALL_VARIABLES()
				SET_SITE_STATE()
				FORCE_SITE_TO_STAGE(AH2_SS_ALL_QUIET, AH2_AS_SETUP_GROUNDFLOOR1)
				
				IF DOES_ENTITY_EXIST(objHat)
					DELETE_OBJECT(objHat)
				ENDIF
				
				IF DOES_ENTITY_EXIST(sConstructionWorkers[1].ped)
					IF NOT IS_PED_INJURED(sConstructionWorkers[1].ped)
						CLEAR_PED_TASKS(sConstructionWorkers[1].ped)
					ENDIF
				ENDIF
				
				WHILE NOT SETUP_ARCHITECT_CAR(vArchCarStart, fArchCarStart)
				OR NOT CREATE_ARCHITECT(vArchCarStart, 0)
				OR NOT HAS_ANIM_DICT_LOADED("missheist_agency2aig_2")
					println("LOADING")
					WAIT(0)
				ENDWHILE
				
				IF IS_REPLAY_BEING_SET_UP()
				OR IS_REPEAT_PLAY_ACTIVE()
				
					IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
						PRINTLN("Creating player car 1")
						WHILE NOT SETUP_PLAYER_CAR(vPlayerCarStart, fPlayerCarHeading)
							WAIT(0)
						ENDWHILE
					ELSE
						IF DOES_ENTITY_EXIST(g_vPlayerVeh[CHAR_FRANKLIN])
						AND VDIST2(GET_ENTITY_COORDS(g_vPlayerVeh[CHAR_FRANKLIN]), vPlayerCarStart) < 50*50
							PRINTLN("Grabbed global vehicle")
							SET_ENTITY_AS_MISSION_ENTITY(g_vPlayerVeh[CHAR_FRANKLIN], TRUE, TRUE)
							vehPlayerCar = g_vPlayerVeh[CHAR_FRANKLIN]
							g_vPlayerVeh[CHAR_FRANKLIN] = NULL
						ELSE
							IF NOT DOES_ENTITY_EXIST(g_vPlayerVeh[CHAR_FRANKLIN])
								PRINTLN("didn't grab global vehicle as it doesn't exist")
							ELSE
								PRINTLN("didn't grab global vehicle as it's too far")
							ENDIF
							VEHICLE_INDEX vehTemp
							vehTemp = GET_CLOSEST_VEHICLE(vPlayerCarStart, 100, GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN, VEHICLE_TYPE_CAR), VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES | VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES )
							IF DOES_ENTITY_EXIST(vehTemp)
							AND VDIST2(GET_ENTITY_COORDS(vehTemp), vPlayerCarStart) < 50*50
								PRINTLN("Grabbed random vehicle")
								SET_ENTITY_AS_MISSION_ENTITY(vehTemp, TRUE, TRUE)
								vehPlayerCar = vehTemp
								vehTemp = NULL
							ELSE
								IF NOT DOES_ENTITY_EXIST(vehTemp)
									PRINTLN("didn't grab random vehicle as it doesn't exist")
								ELSE
									PRINTLN("didn't grab random vehicle as it's too far")
								ENDIF
							ENDIF
							
							IF NOT DOES_ENTITY_EXIST(vehPlayerCar)
								CLEAR_AREA_OF_VEHICLES(vPlayerCarStart, 5, FALSE)
								WHILE NOT SETUP_PLAYER_CAR(vPlayerCarStart, fPlayerCarHeading)
									WAIT(0)
								ENDWHILE
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCar)
					SET_LAST_DRIVEN_VEHICLE(vehPlayerCar)
				ENDIF
				
				IF NOT IS_PED_INJURED(pedArchitect)
				AND IS_VEHICLE_DRIVEABLE(vehArchitectCar)
					SET_PED_INTO_VEHICLE(pedArchitect, vehArchitectCar)
					TASK_PLAY_ANIM(pedArchitect, "missheist_agency2aig_2", "look_at_phone_c", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				ENDIF
				
				eCaseState = AH2_CS_WITH_ARCHITECT
				
				WARP_LIFT_TO_STATE(constructionLifts[LS_SOUTH_A], 1.0)
				WARP_LIFT_TO_STATE(constructionLifts[LS_SOUTH_B], 1.0)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				
				IF NOT IS_PED_INJURED(sConstructionWorkers[1].ped)
					CLEAR_PED_TASKS(sConstructionWorkers[1].ped)
				ENDIF
				
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "Ground Floor", FALSE)
				
				IF IS_REPEAT_PLAY_ACTIVE()
				AND NOT IS_REPLAY_BEING_SET_UP()
					PRINTLN("DOING PLAYER WARP FOR REPEAT PLAY")
					DO_PLAYER_MAP_WARP_WITH_LOAD(<<-78.4023, -1019.2347, 27.5447>>, 109.0206, FALSE, 12000, 200)
				ENDIF
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP()
				ENDIF
				
				CLEAR_AREA(vArchCarStart, 500.0, TRUE)
				
				WHILE eAmbientSiteStage != AH2_AS_RUN_GROUNDFLOOR
					MANAGE_SITE()
					WAIT(0)
				ENDWHILE
				
				bSkip = FALSE
				
			ENDIF
			
			RESET_VARS()
			
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-78.4023, -1019.2347, 27.5447>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 109.0206)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
			TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedArchitect, 15000)
			constructionLifts[LS_SOUTH_A].bActive = FALSE
			constructionLifts[LS_SOUTH_B].bActive = FALSE
			SETTIMERA(0)
			SETTIMERB(0)
			
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			blipArchitect = CREATE_BLIP_FOR_VEHICLE(vehArchitectCar)
			PRINTLN("Created architect blip 1")
			iStageProgress++
			FADE_IN()
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
				ar_ALLOW_PLAYER_SWITCH_OUTRO()
			ENDIF
			
		BREAK
		
		CASE 1
			IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			AND IS_GAMEPLAY_CAM_RENDERING()
			AND NOT Is_Player_Timetable_Scene_In_Progress()
				SETTIMERA(0)
				SETTIMERB(0)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				PRINT_NOW("S3A_FOLLOW", DEFAULT_GOD_TEXT_TIME, 1)
				FADE_IN()
				iStageProgress = 3
			ENDIF
		BREAK
		
		CASE 3
			IF iTimeOnSite = -1
				IF IS_PED_ON_SITE(PLAYER_PED_ID())
				AND IS_VEHICLE_DRIVEABLE(vehArchitectCar)
					iTimeOnSite = GET_GAME_TIMER()
				ENDIF
			ELSE
				IF(GET_GAME_TIMER() - iTimeOnSite) > 1500
				AND NOT IS_PED_INJURED(pedArchitect)
				AND IS_VEHICLE_DRIVEABLE(vehArchitectCar)
//				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT IS_CASE_TEXT_SHOWING()
				AND NOT IS_GAMEPLAY_HINT_ACTIVE()
				AND (iHatThrowStage > 3 OR VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vArchCarStart) < 15*15)
					IF IS_PED_IN_ANY_VEHICLE(pedArchitect)
						TASK_LEAVE_ANY_VEHICLE(pedArchitect)
						SETTIMERA(0)
						REPLAY_RECORD_BACK_FOR_TIME(3.0, 4.0)
						iStageProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF NOT IS_PED_INJURED(pedArchitect)
			AND GET_IS_WAYPOINT_RECORDING_LOADED("agency2Aground")
			AND NOT IS_PED_IN_ANY_VEHICLE(pedArchitect, TRUE)
			AND TIMERA() > 2000
				IF DOES_ENTITY_EXIST(vehArchitectCar)
				AND IS_VEHICLE_DRIVEABLE(vehArchitectCar)
					SET_VEHICLE_DOORS_LOCKED(vehArchitectCar, VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
					SET_VEHICLE_ALARM(vehArchitectCar, TRUE)
				ENDIF
				REMOVE_BLIP(blipArchitect)
				TASK_FOLLOW_WAYPOINT_RECORDING(pedArchitect, "agency2aground", 0, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 5
			IF NOT IS_PED_INJURED(pedArchitect)
				IF VDIST(GET_ENTITY_COORDS(pedArchitect), GET_ENTITY_COORDS(constructionLifts[LS_SOUTH_B].obj)) < 5
				AND IS_ENTITY_OCCLUDED(pedArchitect)
				AND NOT IS_ENTITY_IN_LIFT(constructionLifts[LS_SOUTH_B], pedArchitect)
				AND NOT constructionLifts[LS_SOUTH_B].bInTransit
					SET_ENTITY_COORDS(pedArchitect, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(constructionLifts[LS_SOUTH_B].obj, <<0, -1.93, -2.4>>))
					CLEAR_PED_TASKS(pedArchitect)
				ENDIF
				IF GET_DISTANCE_BETWEEN_COORDS(constructionLifts[LS_SOUTH_B].vPos1, GET_ENTITY_COORDS(pedArchitect)) < 30
					IF constructionLifts[LS_SOUTH_B].fCurrentState = 1
						constructionLifts[LS_SOUTH_B].fTargetState = 0
						constructionLifts[LS_SOUTH_B].bInTransit = TRUE
					ENDIF
				ENDIF
				IF GET_DISTANCE_BETWEEN_COORDS(constructionLifts[LS_SOUTH_B].vPos1, GET_ENTITY_COORDS(pedArchitect)) < 28
					IF constructionLifts[LS_SOUTH_A].fCurrentState = 1
						constructionLifts[LS_SOUTH_A].fTargetState = 0
						constructionLifts[LS_SOUTH_A].bInTransit = TRUE
					ENDIF
				ENDIF
				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedArchitect)
				AND IS_ENTITY_IN_LIFT(constructionLifts[LS_SOUTH_B], pedArchitect)
				AND NOT IS_ENTITY_IN_LIFT(constructionLifts[LS_SOUTH_B], PLAYER_PED_ID())
				AND (VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) > 4 OR (IS_ENTITY_IN_ANY_LIFT(PLAYER_PED_ID()) AND GET_LIFT_INDEX_ENTITY_IS_IN(pedArchitect) != GET_LIFT_INDEX_ENTITY_IS_IN(PLAYER_PED_ID()) ))
					constructionLifts[LS_SOUTH_A].bActive = TRUE
					IF NOT constructionLifts[LS_SOUTH_B].bActive
						constructionLifts[LS_SOUTH_B].bActive = TRUE
						PREPARE_LIFT_FOR_PED_TRANSIT(constructionLifts[LS_SOUTH_B], pedArchitect)
						iStageProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF NOT bHeliOneGo
				IF IS_ENTITY_IN_LIFT(constructionLifts[LS_SOUTH_A], PLAYER_PED_ID())
				AND constructionLifts[LS_SOUTH_A].bInTransit
				AND constructionLifts[LS_SOUTH_A].passenger = PLAYER_PED_ID()
					PRE_INIT_FIRST_FLOOR()
					bHeliOneGo = TRUE
				ENDIF
			ENDIF
			IF constructionLifts[LS_SOUTH_B].fCurrentState > 0.5
			AND constructionLifts[LS_SOUTH_A].fCurrentState > 0.5
				GO_TO_STAGE(STAGE_FIRST_FLOOR)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF NOT bArchLine
		IF NOT IS_PED_INJURED(pedArchitect)
		AND NOT IS_PED_IN_ANY_VEHICLE(pedArchitect)
		AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND VDIST2(GET_ENTITY_COORDS(pedArchitect), vArchCarStart) < 400
		AND VDIST2(GET_ENTITY_COORDS(pedArchitect), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 900
			IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_SITE", CONV_PRIORITY_HIGH)
				bArchLine = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD)<>7
	AND IS_PED_ON_SITE(PLAYER_PED_ID())
	AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCatchLineP1) < 25*25
	AND iHatThrowStage < 7
		IF VDIST2(GET_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCatchLineP1, vCatchLineP2), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 7*7
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
		ENDIF
		IF NOT bPreHatSHout
		AND NOT IS_PED_INJURED(sConstructionWorkers[1].ped)
		AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF VDIST2(GET_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID()), vCatchLineP1, vCatchLineP2), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 5*5
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//						REMOVE_PED_FOR_DIALOGUE(sSpeech, 6)
//						ADD_PED_FOR_DIALOGUE(sSpeech, 6, sConstructionWorkers[1].ped, "FIBConstruction2")
//						IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_PREHAT", CONV_PRIORITY_HIGH)
//							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), sConstructionWorkers[1].ped, 10000, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_HIGH)
						bPreHatSHout = TRUE
//						ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedArchitect)
	
		IF IS_ENTITY_AT_COORD(pedArchitect, <<-183.187332,-1022.582825,28.039276>>,<<6.000000,10.000000,3.000000>>)
			//IF NOT bLiftStairsCollisionLoaded 
				REQUEST_COLLISION_AT_COORD(<<-184.81052, -1019.51874, 28.28928>>)
				REQUEST_ADDITIONAL_COLLISION_AT_COORD(<<-184.81052, -1019.51874, 28.28928>>)
				//PRINTLN("LOADING COLLISION FOR LIFT AREA")
				//IF HAS_COLLISION_LOADED_AROUND_ENTITY(constructionLifts[LS_SOUTH_B].obj)
				//	PRINTLN("LOADED COLLISION FOR LIFT AREA")
				//	bLiftStairsCollisionLoaded = TRUE
				//ENDIF
			//ENDIF
		ENDIF
		
		IF IS_GAMEPLAY_HINT_ACTIVE()
		OR DOES_BLIP_EXIST(blipParkUp)
			SET_PED_MAX_MOVE_BLEND_RATIO(pedArchitect, 0.3)
		ENDIF
		
	ENDIF
	
	IF constructionLifts[LS_SOUTH_A].bActive = TRUE
		IF IS_ENTITY_IN_LIFT(constructionLifts[LS_SOUTH_A], PLAYER_PED_ID())
			IF DOES_BLIP_EXIST(blipLift)
				CLEAR_PRINTS()
				REMOVE_BLIP(blipLift)
			ENDIF
			PRINT_HELP_TEXT(htUseLift)
			IF constructionLifts[LS_SOUTH_A].bInTransit
			AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(htUseLift.label)
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(htUseLift.label)
	AND NOT IS_ENTITY_IN_ANY_LIFT(PLAYER_PED_ID())
		CLEAR_HELP()
		htUseLift.played = FALSE
	ENDIF
	
	IF IS_ENTITY_IN_LIFT(constructionLifts[LS_SOUTH_B], pedArchitect)
	AND constructionLifts[LS_SOUTH_B].fCurrentState = 1
	AND NOT IS_ENTITY_ATTACHED(pedArchitect)
		SET_ENTITY_COORDS(pedArchitect, << -183.1782, -1012.1372, 113.1390 >>)
		SET_ENTITY_HEADING(pedArchitect, 341.0595)
	ENDIF
	
	IF NOT IS_PED_INJURED(sConstructionWorkers[0].ped)
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-110.548759,-1025.629761,26.275150>>, <<-96.838722,-1052.730469,36.325695>>, 15)
		AND IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) <> vehArchitectCar
			IF NOT bCarStopped
				IF NOT DOES_BLIP_EXIST(blipParkUp)
					REMOVE_PED_FOR_DIALOGUE(sSpeech, 2)
					ADD_PED_FOR_DIALOGUE(sSpeech, 2, sConstructionWorkers[0].ped, "FIBConstruction")
					IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_SIGN", CONV_PRIORITY_HIGH)
						SET_GAMEPLAY_ENTITY_HINT(sConstructionWorkers[0].ped, <<0,0,0.5>>)
						SEQUENCE_INDEX seqTemp
						OPEN_SEQUENCE_TASK(seqTemp)
						TASK_STAND_STILL(NULL, 2000)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
						TASK_PLAY_ANIM(NULL, "missheist_agency2aig_1", "direct_traffic_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_CLIPBOARD")
						CLOSE_SEQUENCE_TASK(seqTemp)
						TASK_PERFORM_SEQUENCE(sConstructionWorkers[0].ped, seqTemp)
						CLEAR_SEQUENCE_TASK(seqTemp)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						SETTIMERB(0)
						bCarStopped = TRUE
					ENDIF
				ENDIF
			ELSE
				IF TIMERB() > 3000
					IF NOT DOES_BLIP_EXIST(blipParkUp)
						CLEAR_PRINTS()
						blipParkUp = CREATE_BLIP_FOR_COORD(<< -99.0973, -1014.7451, 26.2727 >>)
					ELSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND NOT IS_CASE_TEXT_SHOWING()
							PRINT_GOD_TEXT(gtPark)
						ENDIF
					ENDIF
					IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_USING_SCENARIO(sConstructionWorkers[0].ped, "WORLD_HUMAN_CLIPBOARD")
			AND NOT sConstructionWorkers[0].bTasked
			AND NOT IS_ENTITY_IN_ANY_LIFT(PLAYER_PED_ID())
				TASK_START_SCENARIO_IN_PLACE(sConstructionWorkers[0].ped, "WORLD_HUMAN_CLIPBOARD")
				sConstructionWorkers[0].bTasked = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_BLIP_EXIST(blipParkUp)
		IF DOES_BLIP_EXIST(blipArchitect)
			REMOVE_BLIP(blipArchitect)
		ENDIF
		IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
		AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -99.0973, -1014.7451, 26.2727 >>, g_vAnyMeansLocate)
			CLEAR_PRINTS()
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			WAIT(1000)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			REMOVE_BLIP(blipParkUp)
			PRINT_GOD_TEXT(gtGetOut)
		ENDIF
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			CLEAR_PRINTS()
			IF NOT IS_PED_INJURED(sConstructionWorkers[0].ped)
				PLAY_PED_AMBIENT_SPEECH(sConstructionWorkers[0].ped, "GENERIC_INSULT_MED", SPEECH_PARAMS_FORCE_NORMAL)
			ENDIF
			REMOVE_BLIP(blipParkUp)
		ENDIF
	ELSE
		IF NOT DOES_BLIP_EXIST(blipArchitect)
		AND NOT IS_PED_INJURED(pedArchitect)
			blipArchitect = CREATE_BLIP_FOR_PED(pedArchitect)
			PRINTLN("Created architect blip 2")
			SET_BLIP_NAME_FROM_TEXT_FILE(blipArchitect, "S3A_ARCLABEL")
		ENDIF
	ENDIF
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND gtGetOut.played
		gtGetOut.played = FALSE
		CLEAR_PRINTS()
	ENDIF
	
	IF NOT bSkip
	
		IF (HAS_ARCHITECT_SEEN_PLAYER() AND NOT IS_ENTITY_IN_ANY_LIFT(pedArchitect))
			GO_TO_STAGE(STAGE_PLAYER_SPOTTED)
		ENDIF
		
		IF IS_ENTITY_IN_LIFT(constructionLifts[LS_SOUTH_B], PLAYER_PED_ID())
		AND IS_ENTITY_IN_ANGLED_AREA(pedArchitect, <<-182.838486,-1013.135315,27.625643>>, <<-186.095551,-1021.540344,32.539276>>, 4.250000)
			GO_TO_STAGE(STAGE_PLAYER_SPOTTED)
		ENDIF
		
	ENDIF
	
	IF DOES_CAM_EXIST(cutCam)
		IF IS_CAM_ACTIVE(cutCam)
			HIDE_HUD_AND_RADAR_THIS_FRAME()
		ENDIF
	ENDIF
	
	IF HAS_PLAYER_SPOOKED_ARCHITECT()
		GO_TO_STAGE(STAGE_COVER_BLOWN)
	ENDIF
	
	HAS_PLAYER_LOST_ARCHITECT()
	
ENDPROC


PROC RUN_ARCH_LOOK_AT_PLANS()
	IF NOT IS_PED_INJURED(pedArchitect)
	AND NOT IS_ENTITY_DEAD(pedArchitect)
	AND NOT IS_PED_DEAD_OR_DYING(pedArchitect, TRUE)
		SWITCH iLookAtPlansStage
			CASE 0
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iArchScene)
					iArchScene = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(pedArchitect), <<0, 0, 334.8835>>)
					TASK_SYNCHRONIZED_SCENE(pedArchitect, iArchScene, "missheist_agency2aig_12", "look_at_plan_base", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					SET_SYNCHRONIZED_SCENE_LOOPED(iArchScene, TRUE)
					iLookAtPlansStage++
				ENDIF
			BREAK
			CASE 1
				IF (GET_GAME_TIMER() - iTimeOfLastArchChat) > 5000
				AND iArchCurrentPlanChat < COUNT_OF(sArchDeskAnims)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iArchScene)
						CLEAR_PED_TASKS(pedArchitect)
					ENDIF
					iArchScene = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(pedArchitect), <<0, 0, 334.8835>>)
					TASK_SYNCHRONIZED_SCENE(pedArchitect, iArchScene, "missheist_agency2aig_12", sArchDeskAnims[iArchCurrentPlanChat], SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedArchitect)
					iLookAtPlansStage++
				ENDIF
			BREAK
			CASE 2
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iArchScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iArchScene) > 0.2
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 30*30
							CREATE_CONVERSATION(sSpeech, sBlockID, sArchConvLabel[iArchCurrentPlanChat], CONV_PRIORITY_HIGH)
						ENDIF
						iArchCurrentPlanChat++
						iLookAtPlansStage++
					ENDIF
				ENDIF
			BREAK
			CASE 3
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iArchScene)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iArchScene) > 0.957
						CLEAR_PED_TASKS(pedArchitect)
						iArchScene = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(pedArchitect), <<0, 0, 334.8835>>)
						TASK_SYNCHRONIZED_SCENE(pedArchitect, iArchScene, "missheist_agency2aig_12", "look_at_plan_base", SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedArchitect)
						SET_SYNCHRONIZED_SCENE_LOOPED(iArchScene, TRUE)
						iTimeOfLastArchChat = GET_GAME_TIMER()
						iLookAtPlansStage=1
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC RUN_MUGGING()

	IF eMugStage != AH2_MSE_MUGGING_OVER
	AND eMugStage != AH2_MSE_MUGGING_BLOWN
		IF IS_PED_SHOOTING(PLAYER_PED_ID())
		AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
		AND ( NOT IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID()) OR ( NOT IS_PED_INJURED(pedArchitect) AND IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedArchitect), 3)) )
			IF eMugStage != AH2_MSE_DO_COWERING
				eMugStage = AH2_MSE_MUGGING_BLOWN
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(pedArchitect)
			IF IS_PED_INJURED(pedArchitect)
			OR IS_PED_RAGDOLL(pedArchitect)
			OR IS_ENTITY_DEAD(pedArchitect)
				IF IS_PED_INJURED(pedArchitect)
					PRINTLN("\n\n\n\n\n\n\n\n ARCHITECT HAS BEEN INJURED \n\n\n\n\n\n\n\n")
				ENDIF
				IF IS_PED_RAGDOLL(pedArchitect)
					PRINTLN("\n\n\n\n\n\n\n\n ARCHITECT HAS BEEN RAGDOLLING \n\n\n\n\n\n\n\n")			
				ENDIF
				eMugStage = AH2_MSE_MUGGING_BLOWN
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(pedArchitect)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedArchitect, PLAYER_PED_ID())
				eMugStage = AH2_MSE_MUGGING_BLOWN
			ENDIF
		ENDIF
		IF eMugStage > AH2_MSE_WAIT_FOR_PLAYER_TO_START_MUGGING
			IF NOT IS_ENTITY_IN_ANGLED_AREA(pedArchitect, vMuggingAreaPos1, vMuggingAreaPos2, fMuggingAreaWidth)
				eMugStage = AH2_MSE_MUGGING_BLOWN
			ENDIF
		ENDIF
//		IF eMugStage < AH2_MSE_DO_COWERING
//		AND eCaseState >= AH2_CS_ON_GROUND
//		ENDIF
	ENDIF
	
	SWITCH eMugStage
	
		CASE AH2_MSE_LOAD_ANIMS
			REQUEST_ANIM_DICT(sMuggingDict1)
			REQUEST_ANIM_DICT(sMuggingDict2)
			IF HAS_ANIM_DICT_LOADED(sMuggingDict1)
			AND HAS_ANIM_DICT_LOADED(sMuggingDict2)
				bSecondLineDone = FALSE
				eMugStage = AH2_MSE_AREA_CHECK
			ENDIF
		BREAK
		
		CASE AH2_MSE_AREA_CHECK
			IF NOT IS_PED_INJURED(pedArchitect)
				IF IS_ENTITY_IN_ANGLED_AREA(pedArchitect, vMuggingAreaPos1, vMuggingAreaPos2, fMuggingAreaWidth)
					eMugStage = AH2_MSE_WAIT_FOR_PLAYER_TO_START_MUGGING
				ENDIF
			ENDIF
		BREAK
		
		CASE AH2_MSE_WAIT_FOR_PLAYER_TO_START_MUGGING
			IF NOT IS_PED_INJURED(pedArchitect)
				IF IS_ENTITY_IN_ANGLED_AREA(pedArchitect, vMuggingAreaPos1, vMuggingAreaPos2, fMuggingAreaWidth)
					IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
						PRINT_HELP_TEXT(htMug)
						IF (IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedArchitect) OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedArchitect))
						AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 15
							CLEAR_HELP()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_FMUG", CONV_PRIORITY_HIGH)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedArchitect, TRUE)
								eMugStage = AH2_MSE_DO_HANDS_UP
							ENDIF
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vMuggingAreaPos1, vMuggingAreaPos2, fMuggingAreaWidth)
					AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(htMug.label)
						CLEAR_HELP()
						htMug.played = FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE AH2_MSE_TURN_TO_FACE_THE_PLAYER
			IF NOT IS_PED_INJURED(pedArchitect)
				IF NOT IS_PED_FACING_PED(pedArchitect, PLAYER_PED_ID(), 20)
					IF GET_SCRIPT_TASK_STATUS(pedArchitect, SCRIPT_TASK_HANDS_UP) != PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(pedArchitect, SCRIPT_TASK_HANDS_UP) != DORMANT_TASK
						TASK_HANDS_UP(pedArchitect, 10000, PLAYER_PED_ID(), -1, HANDS_UP_NOTHING)
					ENDIF
				ELSE
					IF GET_SCRIPT_TASK_STATUS(pedArchitect, SCRIPT_TASK_HANDS_UP) = PERFORMING_TASK
					OR GET_SCRIPT_TASK_STATUS(pedArchitect, SCRIPT_TASK_HANDS_UP) = DORMANT_TASK
						eMugStage = AH2_MSE_DO_COWERING
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE AH2_MSE_DO_HANDS_UP
			TASK_LOOK_AT_ENTITY(pedArchitect, PLAYER_PED_ID(), -1, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_VERY_HIGH)
			SEQUENCE_INDEX seqHandsUp
			OPEN_SEQUENCE_TASK(seqHandsUp)
			IF GET_ENTITY_SPEED(pedArchitect) > 1
				TASK_GO_STRAIGHT_TO_COORD(NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedArchitect, <<0, 1, 0>>), PEDMOVEBLENDRATIO_WALK)
				TASK_STAND_STILL(NULL, 500)
			ELSE
				TASK_STAND_STILL(NULL, 1000)
			ENDIF
			TASK_PLAY_ANIM(NULL, sMuggingDict2, "handsup_enter", SLOW_BLEND_IN, NORMAL_BLEND_OUT)
			TASK_PLAY_ANIM(NULL, sMuggingDict2, "handsup_loop", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
			CLOSE_SEQUENCE_TASK(seqHandsUp)
			TASK_PERFORM_SEQUENCE(pedArchitect, seqHandsUp)
			CLEAR_SEQUENCE_TASK(seqHandsUp)
			//iTurnTime = GET_GAME_TIMER()
			eMugStage = AH2_MSE_DROP_CASE
		BREAK
		
		CASE AH2_MSE_DROP_CASE
			IF NOT IS_PED_INJURED(pedArchitect)
				IF IS_ENTITY_PLAYING_ANIM(pedArchitect, sMuggingDict2, "handsup_loop")
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_MUGG", CONV_PRIORITY_HIGH)
							//CLEAR_PED_TASKS(pedArchitect)
							TASK_PLAY_ANIM(pedArchitect, sMuggingDict2, "handsup_exit", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
						ENDIF
					ENDIF
				ELSE
					IF eCaseState < AH2_CS_DROPPING_CASE
						IF IS_ENTITY_PLAYING_ANIM(pedArchitect, sMuggingDict2, "handsup_exit")
							#IF IS_DEBUG_BUILD
								PRINTLN(GET_STRING_FROM_FLOAT(GET_ENTITY_ANIM_CURRENT_TIME(pedArchitect, sMuggingDict2, "handsup_exit")))
							#ENDIF
							IF NOT bMugged
								IF GET_ENTITY_ANIM_CURRENT_TIME(pedArchitect, sMuggingDict2, "handsup_exit") > 0.255
									PRINTLN("DROPPING CASE 1")
									DROP_THE_CASE(pedArchitect)
									eCaseState = AH2_CS_DROPPING_CASE
									SET_PED_CONFIG_FLAG(pedArchitect, PCF_ActivateRagdollFromMinorPlayerContact, FALSE)
									SET_PED_COMBAT_ATTRIBUTES(pedArchitect,  CA_PLAY_REACTION_ANIMS, FALSE)
									eMugStage = AH2_MSE_DO_COWERING
									bMugged = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE AH2_MSE_DO_COWERING
		
			IF NOT IS_PED_INJURED(pedArchitect)
			AND NOT IS_ENTITY_DEAD(pedArchitect)
			AND bMugged
			
				IF eCowerStage != AH2_MCS_SHOCKED
				
					IF IS_PED_SHOOTING(PLAYER_PED_ID())
					AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
					AND (NOT IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID()) OR CAN_ARCHITECT_SEE_PLAYER())
					AND IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedArchitect), 3)
						eCowerStage = AH2_MCS_SHOCKED
						TASK_PLAY_ANIM(pedArchitect, sMuggingDict1, sHandsUpReact2, SLOW_BLEND_IN, SLOW_BLEND_OUT)
					ENDIF
					
					IF NOT bSecondLineDone
					AND GET_GAME_TIMER() - iTimeOfCaseText < 1000
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND NOT IS_CASE_TEXT_SHOWING()
							IF NOT bPlayerhasCase
								IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_BRIEF", CONV_PRIORITY_HIGH)
									bSecondLineDone = TRUE
								ENDIF
							ELSE
								bSecondLineDone = TRUE
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
				SWITCH eCowerStage
				
					CASE AH2_MCS_HANDS_UP
					
						IF IS_ENTITY_PLAYING_ANIM(pedArchitect, sMuggingDict2, "handsup_exit")
							IF GET_ENTITY_ANIM_CURRENT_TIME(pedArchitect, sMuggingDict2, "handsup_exit") > 0.9
								TASK_PLAY_ANIM(pedArchitect, sMuggingDict1, sHandsUpBase, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
							ENDIF
						ELSE
							IF NOT IS_ENTITY_PLAYING_ANIM(pedArchitect, sMuggingDict1, sHandsUpBase)
								TASK_PLAY_ANIM(pedArchitect, sMuggingDict1, sHandsUpBase, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
							ENDIF
							IF GET_GAME_TIMER() - iLastCower > 1000
								IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 15
									IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
									AND ( IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedArchitect) OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedArchitect))
									AND CAN_ARCHITECT_SEE_PLAYER()
										TASK_PLAY_ANIM(pedArchitect, sMuggingDict2, sHandsUpAnxious, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY | AF_SECONDARY)
										iLastCower = GET_GAME_TIMER()
										eCowerStage = AH2_MCS_HANDS_UP_ANXIOUS
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					BREAK
					
					CASE AH2_MCS_HANDS_UP_ANXIOUS
					
						IF NOT IS_ENTITY_PLAYING_ANIM(pedArchitect, sMuggingDict1, sHandsUpBase)
							TASK_PLAY_ANIM(pedArchitect, sMuggingDict1, sHandsUpBase, NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
						ENDIF
						
						IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
						AND CAN_ARCHITECT_SEE_PLAYER()
							iLastCower = GET_GAME_TIMER()
						ENDIF
						
						IF GET_GAME_TIMER() - iLastCower > 3000
							IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) > 15
							OR NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
							OR NOT (IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedArchitect) OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedArchitect))
							OR NOT CAN_ARCHITECT_SEE_PLAYER()
								CLEAR_PED_TASKS(pedArchitect)
								TASK_PLAY_ANIM(pedArchitect, sMuggingDict1, sHandsUpBase, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
								eCowerStage = AH2_MCS_HANDS_UP
							ENDIF
						ENDIF
						
					BREAK
					
					CASE AH2_MCS_SHOCKED
					
						IF IS_ENTITY_PLAYING_ANIM(pedArchitect, sMuggingDict1, sHandsUpReact2)
						AND GET_ENTITY_ANIM_CURRENT_TIME(pedArchitect, sMuggingDict1, sHandsUpReact2)>0.9
							eCowerStage = AH2_MCS_HANDS_UP
						ENDIF
						
					BREAK
					
				ENDSWITCH
				
			ENDIF
			
		BREAK
		
		CASE AH2_MSE_MUGGING_BLOWN
			IF NOT IS_PED_INJURED(pedArchitect)
				CLEAR_PED_TASKS(pedArchitect)
				eMugStage = AH2_MSE_MUGGING_OVER
				bMugged = FALSE
			ENDIF
			IF mission_stage = STAGE_FIRST_FLOOR
				GO_TO_STAGE(STAGE_COVER_BLOWN)
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC FIRST_FLOOR_SECTION()

	INT iTemp
		
	SWITCH iStageProgress
	
		CASE 0
		
			RESET_ALL_TOP_FLOOR_VARIABLES()
			
			bTopFloorPedsAlerted = FALSE
			bTopFloorPedsFlee = FALSE
			iTimeTopFloorAlerted = 0
			
			REMOVE_BLIPS()
			INIT_TEXT()
			
			REQUEST_WAYPOINT_RECORDING("agency2Afloor")
			REQUEST_ANIM_DICT("missheistdockssetup1ig_10@idle_d")
			REQUEST_MISSION_AUDIO_BANK("FBI_Heist_Mug_Architect")
			REQUEST_ANIM_DICT("missheist_agency2aig_4")
			REQUEST_ANIM_DICT("missheist_agency2aig_12")
			
			IF bSkip
			
				CREATE_ARCHITECT(<<-182.3432, -1011.5896, 113.1388>>, 347.4715)
				RESET_MUGGING()
				SET_SITE_STATE()
				FORCE_SITE_TO_STAGE(AH2_SS_ALL_QUIET, AH2_AS_SETUP_TOPFLOOR)
				
				FADE_OUT()
				IF DOES_ENTITY_EXIST(objHat)
					DELETE_OBJECT(objHat)
				ENDIF
				
				SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD, 7)
				RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
				PRINTLN("FADED OUT")
				WARP_LIFT_TO_STATE(constructionLifts[LS_SOUTH_A], 1.0)
				WARP_LIFT_TO_STATE(constructionLifts[LS_SOUTH_B], 1.0)
				WHILE NOT SETUP_ARCHITECT_CAR(vArchCarStart, fArchCarStart)
				OR NOT SETUP_PLAYER_CAR(vPlayerCarStart, fPlayerCarHeading)
				OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("agency2Afloor")
					#IF IS_DEBUG_BUILD
						PRINTLN("SETTING UP STAGE")
					#ENDIF
					WAIT(0)
				ENDWHILE
				eCaseState = AH2_CS_WITH_ARCHITECT
				bSkip = FALSE
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				REPEAT COUNT_OF(constructionLifts) iTemp
					constructionLifts[iTemp].bActive = TRUE
				ENDREPEAT
				IF NOT IS_REPLAY_BEING_SET_UP()
					DO_PLAYER_MAP_WARP_WITH_LOAD(<<-180.1966, -1017.0389, 113.2076>>, 340.0900, FALSE)
				ENDIF
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-180.1966, -1017.0390, 113.2072>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 340.0900)
				REQUEST_ANIM_DICT("missheist_agency2aig_4")
				REMOVE_PED_FOR_DIALOGUE(sSpeech, 1)
				ADD_PED_FOR_DIALOGUE(sSpeech, 1, pedArchitect, "FIBArchitect")
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP()
				ENDIF
				
			ENDIF
						
			SETTIMERA(0)
			SETTIMERB(0)
			iTimeOfLastArchChat = 0
			iArchCurrentPlanChat = 1
			iArchitectSightTimer = 0
			iMugFailCheckTImer = 0
			iTimeOfTouch = 0
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "Top floor", FALSE)
			iStageProgress++
			
		BREAK
		
		CASE 1
		
			IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_2_USE_ELEVATOR")
				IF NOT IS_ENTITY_IN_ANY_LIFT(PLAYER_PED_ID())
				OR (IS_ENTITY_IN_ANY_LIFT(PLAYER_PED_ID()) AND NOT constructionLifts[GET_LIFT_INDEX_ENTITY_IS_IN(PLAYER_PED_ID())].bInTransit)
					STOP_AUDIO_SCENE("AGENCY_H_2_USE_ELEVATOR")
				ENDIF
			ELSE
				IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_2_FOLLOW_ARCHITECT_2")
					START_AUDIO_SCENE("AGENCY_H_2_FOLLOW_ARCHITECT_2")
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(pedArchitect)
			AND iArchStage < 200
				IF NOT DOES_BLIP_EXIST(blipArchitect)
					blipArchitect = CREATE_BLIP_FOR_PED(pedArchitect)
					PRINTLN("Created architect blip 3")
					SET_BLIP_NAME_FROM_TEXT_FILE(blipArchitect, "S3A_ARCLABEL")
				ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA(pedArchitect, vMuggingAreaPos1, vMuggingAreaPos2, fMuggingAreaWidth)
					PRINT_NOW("S3A_KILL", DEFAULT_GOD_TEXT_TIME, 1)
					SETTIMERA(0)
					SET_BLIP_AS_FRIENDLY(blipArchitect, FALSE)
					IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_2_FOLLOW_ARCHITECT_2")
						STOP_AUDIO_SCENE("AGENCY_H_2_FOLLOW_ARCHITECT_2")
					ENDIF
					iStageProgress++
				ENDIF
				IF HAS_PLAYER_SPOOKED_ARCHITECT()
					GO_TO_STAGE(STAGE_COVER_BLOWN)
				ELSE
					IF HAS_ARCHITECT_SEEN_PLAYER()
						GO_TO_STAGE(STAGE_PLAYER_SPOTTED)
					ENDIF
				ENDIF
			ELSE
				CLEAR_PRINTS()
				iStageProgress = 2
			ENDIF
			
		BREAK
		
		CASE 2
			
			IF HAS_PLAYER_SPOOKED_ARCHITECT()
				GO_TO_STAGE(STAGE_COVER_BLOWN)
			ELSE
				IF CAN_ARCHITECT_SEE_PLAYER()
				AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 49
					GO_TO_STAGE(STAGE_PLAYER_SPOTTED)
				ENDIF
			ENDIF
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_2_TAKE_OUT_ARCHITECT")
				START_AUDIO_SCENE("AGENCY_H_2_TAKE_OUT_ARCHITECT")
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	SWITCH iArchStage
	
		CASE 0
			IF vPlayerCoords.z > 100
				IF NOT IS_PED_INJURED(sConstructionWorkers[0].ped)
				AND NOT IS_PED_INJURED(sConstructionWorkers[1].ped)
				AND DOES_ENTITY_EXIST(pedArchitect)
					REQUEST_ANIM_DICT("missheist_agency2aig_4")
					PRINTLN("LOADING TOP FLOOR STUFF")
					IF HAS_ANIM_DICT_LOADED("missheist_agency2aig_4")
					AND NOT IS_ENTITY_ATTACHED(pedArchitect)
					AND NOT IS_REPLAY_BEING_SET_UP()
						TASK_GO_STRAIGHT_TO_COORD(pedArchitect, <<-177.6619, -1002.1149, 113.1388>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, 302.3083)
//						TASK_FOLLOW_WAYPOINT_RECORDING(pedArchitect, "agency2Afloor", 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
						FADE_IN()
						iArchStage++						
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF VDIST(GET_ENTITY_COORDS(pedArchitect), <<-175.36372, -1002.18530, 113.13877>>) < 10.5
				TASK_FOLLOW_NAV_MESH_TO_COORD(pedArchitect, <<-177.6619, -1002.1149, 113.1388>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, 302.3083)
				ADD_PED_FOR_DIALOGUE(sSpeech, 2, sConstructionWorkers[0].ped, "FIBConstruction")
				CLEAR_PED_TASKS(sConstructionWorkers[0].ped)
				CLEAR_PED_TASKS(sConstructionWorkers[1].ped)
				iWorkerScene = CREATE_SYNCHRONIZED_SCENE(vWorkerScenePosition, vWorkerSceneRotation)
				TASK_SYNCHRONIZED_SCENE(sConstructionWorkers[0].ped, iWorkerScene,  "missheist_agency2aig_4", "walk_by_worker1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				TASK_SYNCHRONIZED_SCENE(sConstructionWorkers[1].ped, iWorkerScene,  "missheist_agency2aig_4", "walk_by_worker2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
//				TASK_SYNCHRONIZED_SCENE(pedArchitect, iWorkerScene,  "missheist_agency2aig_4", "walk_by_architect", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
				IF NOT IS_PED_INJURED(sConstructionWorkers[0].ped)
				AND NOT IS_PED_INJURED(sConstructionWorkers[1].ped)
					TASK_LOOK_AT_ENTITY(pedArchitect, sConstructionWorkers[0].ped, 10000)
					TASK_LOOK_AT_ENTITY(sConstructionWorkers[0].ped, pedArchitect, 10000)
					TASK_LOOK_AT_ENTITY(sConstructionWorkers[1].ped, pedArchitect, 10000)
				ENDIF
				IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 25
					CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_WCHAT2", CONV_PRIORITY_HIGH)
				ENDIF
				SET_SYNCHRONIZED_SCENE_LOOPED(iWorkerScene, FALSE)
				
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0)
				
				iArchStage++
			ENDIF
		BREAK
		
		CASE 2
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iWorkerScene)
//				PRINTLN("SCENE PHASE ", GET_SYNCHRONIZED_SCENE_PHASE(iWorkerScene))
				IF GET_SYNCHRONIZED_SCENE_PHASE(iWorkerScene) > 0.65
//					CLEAR_PED_TASKS(pedArchitect)
					TASK_FOLLOW_WAYPOINT_RECORDING(pedArchitect, "agency2Afloor", 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedArchitect)
//					FORCE_PED_MOTION_STATE(pedArchitect, MS_ON_FOOT_WALK)
					iArchStage++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_PED_INJURED(sConstructionWorkers[0].ped)
			AND NOT IS_PED_INJURED(sConstructionWorkers[1].ped)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iWorkerScene)
					PRINTLN("SCENE PHASE ", GET_SYNCHRONIZED_SCENE_PHASE(iWorkerScene))
					IF GET_SYNCHRONIZED_SCENE_PHASE(iWorkerScene) > 0.95
						CLEAR_PED_TASKS(sConstructionWorkers[0].ped)
						CLEAR_PED_TASKS(sConstructionWorkers[1].ped)
						iWorkerScene = CREATE_SYNCHRONIZED_SCENE(vWorkerScenePosition, vWorkerSceneRotation)
						TASK_SYNCHRONIZED_SCENE(sConstructionWorkers[0].ped, iWorkerScene, "missheist_agency2aig_4", "look_plan_a_worker1", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
						TASK_SYNCHRONIZED_SCENE(sConstructionWorkers[1].ped, iWorkerScene, "missheist_agency2aig_4", "look_plan_a_worker2", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)	
						SET_SYNCHRONIZED_SCENE_LOOPED(iWorkerScene, TRUE)
						iArchStage++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			vArchSceneAnimStart = <<-137.2063, -968.6782, 113.1362>>
			iArchStage++
		BREAK
		
		CASE 5
			IF NOT IS_PED_INJURED(pedArchitect)
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedArchitect)
				AND VDIST(GET_ENTITY_COORDS(pedArchitect), vArchSceneAnimStart) < 3
					TASK_FOLLOW_NAV_MESH_TO_COORD(pedArchitect, vArchSceneAnimStart, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, 0.1, ENAV_STOP_EXACTLY)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedArchitect)
					iArchStage++
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF NOT IS_PED_INJURED(pedArchitect)
				FLOAT fHead
				fHead = GET_ENTITY_HEADING(pedArchitect)
				IF eMugStage <= AH2_MSE_WAIT_FOR_PLAYER_TO_START_MUGGING
					IF VDIST(GET_ENTITY_COORDS(pedArchitect), vArchSceneAnimStart) < 1.5
					AND (ABSF(fHead -  334.8835) < 10 OR  ABSF(fHead -  -26.8835) < 10)
						RUN_ARCH_LOOK_AT_PLANS()
					ELSE
						IF iLookAtPlansStage <> 0
							iLookAtPlansStage = 0
						ENDIF
						IF VDIST(GET_ENTITY_COORDS(pedArchitect), vArchSceneAnimStart) > 1
							IF GET_SCRIPT_TASK_STATUS(pedArchitect, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedArchitect, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK
								TASK_FOLLOW_NAV_MESH_TO_COORD(pedArchitect, vArchSceneAnimStart, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, 0.1, ENAV_STOP_EXACTLY, 334.8835)
							ENDIF
						ELSE
							IF GET_SCRIPT_TASK_STATUS(pedArchitect, SCRIPT_TASK_ACHIEVE_HEADING) <> PERFORMING_TASK
							AND GET_SCRIPT_TASK_STATUS(pedArchitect, SCRIPT_TASK_ACHIEVE_HEADING) <> WAITING_TO_START_TASK
								TASK_ACHIEVE_HEADING(pedArchitect, 334.8835)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iArchScene)
				IF IS_ENTITY_ATTACHED(objPlans)
				AND GET_SYNCHRONIZED_SCENE_PHASE(iArchScene) > 0.651
					DETACH_ENTITY(objPlans, FALSE)
				ENDIF
				IF GET_SYNCHRONIZED_SCENE_PHASE(iArchScene) > 0.95
					CLEAR_PED_TASKS(pedArchitect)
					iArchScene = CREATE_SYNCHRONIZED_SCENE(vArchAtDeskScenePosition, vArchAtDeskSceneRotation)
//					TASK_SYNCHRONIZED_SCENE(pedArchitect, iArchScene, "missheist_agency2aig_5", "look_plan_base", NORMAL_BLEND_IN, NORMAL_BLEND_IN, SYNCED_SCENE_USE_PHYSICS)
					SET_SYNCHRONIZED_SCENE_LOOPED(iArchScene, TRUE)
					iArchStage++
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			IF NOT IS_PED_INJURED(pedArchitect)
				IF iArchCurrentPlanChat < 5
					IF (GET_GAME_TIMER() - iTimeOfLastArchChat) > 7500
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 30*30
							IF CREATE_CONVERSATION(sSpeech, sBlockID, sArchConvLabel[iArchCurrentPlanChat], CONV_PRIORITY_HIGH)
								iTimeOfLastArchChat = GET_GAME_TIMER()
								iArchCurrentPlanChat++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE -100
			IF NOT IS_PED_INJURED(pedArchitect)
				CLEAR_PED_TASKS(pedArchitect)
				VECTOR vLookTemp
				vLookTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
				SEQUENCE_INDEX seqTemp
				OPEN_SEQUENCE_TASK(seqTemp)
				TASK_STAND_STILL(NULL, 1500)
				TASK_TURN_PED_TO_FACE_COORD(NULL, vLookTemp)
				CLOSE_SEQUENCE_TASK(seqTemp)
				TASK_PERFORM_SEQUENCE(pedArchitect, seqTemp)
				CLEAR_SEQUENCE_TASK(seqTemp)
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 20*20
					CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_WASSAT", CONV_PRIORITY_HIGH)
				ENDIF
				iArchStage = -101
			ENDIF
		BREAK
		
		CASE -101
			IF (GET_GAME_TIMER() - iLastLookCheck) > 5000
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 20*20
					CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_NOTHING", CONV_PRIORITY_HIGH)
				ENDIF
				iArchStage = iSaveStage
				IF iArchStage <= 5
					TASK_FOLLOW_WAYPOINT_RECORDING(pedArchitect, "agency2Afloor", 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF iArchStage < 100
		IF NOT IS_PED_INJURED(pedArchitect)
			IF IS_ENTITY_IN_ANGLED_AREA(pedArchitect, <<-135.457855,-965.983276,116.111168>>, <<-148.984299,-988.099487,112.386230>>, 3.500000)
				IF eMugStage <= AH2_MSE_WAIT_FOR_PLAYER_TO_START_MUGGING
				AND mission_stage != STAGE_COVER_BLOWN
					IF iArchStage > -100
					AND (GET_GAME_TIMER() - iLastLookCheck) > 8000
					AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedArchitect, <<2,2,3>>)
						IF CAN_PED_HEAR_PLAYER(PLAYER_ID(), pedArchitect)
							iSaveStage = iArchStage
							iArchStage = -100
							iLastLookCheck = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bStealthKillConv
	AND NOT IS_PED_INJURED(pedArchitect)
		IF IS_PED_BEING_STEALTH_KILLED(pedArchitect)
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			IF CREATE_CONVERSATION(sSpeech, sBlockID, "F3A_JSTEALTH", CONV_PRIORITY_HIGH)
				bStealthKillConv = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedArchitect)
		VECTOR vFloorCheck = GET_ENTITY_COORDS(PLAYER_PED_ID())
		IF vFloorCheck.z < 40
			#IF IS_DEBUG_BUILD
				PRINTLN("FALLEN FAIL")
			#ENDIF
			Mission_Failed("S3A_LOST")
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iArchStage), <<0.1,0.5,0>>)
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iStageProgress), <<0.1,0.52,0>>)
	#ENDIF
	
ENDPROC

FUNC BOOL HAS_PLAYER_LOST_CONSTRUCTION_WORKERS()
	INT iTemp
	BOOL bLost = TRUE
	REPEAT COUNT_OF(sConstructionWorkers) iTemp
		IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
		AND NOT IS_PED_INJURED(sConstructionWorkers[iTemp].ped)
			IF NOT IS_PED_FLEEING(sConstructionWorkers[iTemp].ped)
				IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(sConstructionWorkers[iTemp].ped)) < 50
				OR NOT IS_ENTITY_OCCLUDED(sConstructionWorkers[iTemp].ped)
					bLost = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN bLost
ENDFUNC

PROC CREATE_GROUNDFLOOR_ANGRY_PEDS(BOOL bFirstLift)
	IF bFirstLift
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[13], <<-185.7923, -1008.4628, 28.2893>>, 197.0187)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[14], <<-185.7570, -1008.4086, 28.2893>>, 151.2708)
	ELSE
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[13], <<-169.6047, -956.9946, 28.2893>>, 348.6088)
		INIT_CONSTRUCTION_WORKER(sConstructionWorkers[14], <<-171.1050, -960.8823, 28.2893>>, 339.4783)
	ENDIF
	CREATE_CONSTRUCTION_WORKER(sConstructionWorkers[13], S_M_Y_CONSTRUCT_01)
	CREATE_CONSTRUCTION_WORKER(sConstructionWorkers[14], S_M_Y_CONSTRUCT_02)
ENDPROC

PROC LEAVE_CONSTRUCTION_SITE()

	INT iTemp
	
	IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_2_TAKE_OUT_ARCHITECT")
		STOP_AUDIO_SCENE("AGENCY_H_2_TAKE_OUT_ARCHITECT")
	ENDIF
	
	SWITCH iStageProgress
	
		CASE 0
			REMOVE_BLIPS()
			INIT_TEXT()
			
			IF bSkip
			
				RESET_ALL_GENERIC_VARIABLES()
				RESET_MUGGING(AH2_MSE_DO_COWERING)
				FADE_OUT()
				FORCE_SITE_TO_STAGE(AH2_SS_ALL_QUIET, AH2_AS_SETUP_TOPFLOOR)
				
				IF DOES_ENTITY_EXIST(objHat)
					DELETE_OBJECT(objHat)
				ENDIF
				
				WHILE NOT SETUP_ARCHITECT_CAR(vArchCarStart, fArchCarStart)
				OR NOT SETUP_PLAYER_CAR(vPlayerCarStart, fPlayerCarHeading)
					WAIT(0)
				ENDWHILE
				
				DELETE_PED(pedArchitect)
				CREATE_ARCHITECT(<< -136.7302, -967.6271, 113.1347 >>, 312.9988)
				
				TASK_COWER(pedArchitect)
				bMugged = TRUE
				
				DELETE_OBJECT(objPlans)
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -139.9581, -971.2977, 113.1330 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 332.4719)
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				htUseLift.played = FALSE
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				bPlayerhasCase = TRUE
				GIVE_THE_CASE_TO_PED(PLAYER_PED_ID())
				eCaseState = AH2_CS_WITH_PLAYER
				
				WAIT(2000)
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP()
				ENDIF
				
				FADE_IN()
				bSkip = FALSE
				
			ENDIF
			
			INT iLifts 
			REPEAT COUNT_OF(constructionLifts) iLifts
				constructionLifts[iLifts].bActive = TRUE	
			ENDREPEAT
			
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(AH2_QUICK_GETAWAY)
			TRIGGER_MUSIC_EVENT("AH2A_FLEE_SITE")
			
			CLEAR_PRINTS()
			iStageProgress++
			
		BREAK
		
		CASE 1
		
			BOOL bInAnyLift
			bInAnyLift = FALSE
			
			IF TIMERB() > 2000
			AND NOT bLeavePrinted
			AND IS_PED_ON_SITE(PLAYER_PED_ID())
			AND (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
				PRINT_NOW("S3A_ESCAPE", DEFAULT_GOD_TEXT_TIME, 1)
				bLeavePrinted = TRUE
			ENDIF
			
			IF NOT IS_PED_ON_SITE(PLAYER_PED_ID())
			AND GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_INVALID
			AND (HAS_PLAYER_LOST_CONSTRUCTION_WORKERS() OR eSiteState = AH2_SS_ALL_QUIET)
				CLEAR_PRINTS()
				GO_TO_STAGE(STAGE_DRIVE_BACK)
			ENDIF
			
			REPEAT COUNT_OF(constructionLifts) iTemp
				IF constructionLifts[iTemp].bActive = TRUE
				AND IS_LIFT_CLEAR(constructionLifts[iTemp])
					IF IS_ENTITY_IN_LIFT(constructionLifts[iTemp], PLAYER_PED_ID())
						bInAnyLift = TRUE
						IF NOT constructionLifts[iTemp].bInTransit
							PRINT_HELP_TEXT(htUseLift)
						ENDIF
						IF constructionLifts[iTemp].bInTransit
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(htUseLift.label)
								CLEAR_HELP()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			IF NOT bInAnyLift
				IF htUseLift.played
					htUseLift.played = FALSE
				ENDIF
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(htUseLift.label)
					CLEAR_HELP()
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	IF DOES_ENTITY_EXIST(pedArchitect)
		IF NOT IS_PED_INJURED(pedArchitect)
			MANAGE_FLEEING_ARCHITECT()
			IF VDIST(GET_ENTITY_COORDS(pedArchitect), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 50
			AND IS_ENTITY_OCCLUDED(pedArchitect)
				DELETE_PED(pedArchitect)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

BOOL bBriefCaseForced
BOOL bBriefCaseShouldForceOnExitVehicle

PROC DRIVE_BACK()

	IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vOffice) < 100*100
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedArchitect)
		IF NOT IS_PED_INJURED(pedArchitect)
			MANAGE_FLEEING_ARCHITECT()
			IF VDIST(GET_ENTITY_COORDS(pedArchitect), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 50
			AND IS_ENTITY_OCCLUDED(pedArchitect)
				DELETE_PED(pedArchitect)
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH iStageProgress
	
		CASE 0
		
			REMOVE_PED_FOR_DIALOGUE(sSpeech, 3)
			ADD_PED_FOR_DIALOGUE(sSpeech, 3, NULL, "MICHAEL")
			RELEASE_MISSION_AUDIO_BANK()
			TRIGGER_MUSIC_EVENT("AH2A_EXIT_SITE")
			REMOVE_ALL_WORKER_BLIPS()
			INIT_TEXT()
						
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE)
			
			IF bSkip
			
				RESET_ALL_GENERIC_VARIABLES()
				FADE_OUT()
				FORCE_SITE_TO_STAGE(AH2_SS_ALL_QUIET, AH2_AS_SETUP_GROUNDFLOOR2)
				RESET_MUGGING()
				bPlayerhasCase = TRUE
				
				IF DOES_ENTITY_EXIST(objHat)
					DELETE_OBJECT(objHat)
				ENDIF
				
				IF g_replay.iReplayInt[0] != 1
					WHILE NOT SETUP_ARCHITECT_CAR(vArchCarStart, fArchCarStart)
						WAIT(0)
					ENDWHILE
				ENDIF
				
				WHILE NOT SETUP_PLAYER_CAR(vPlayerCarStart, fPlayerCarHeading)
					WAIT(0)
				ENDWHILE
				
				DELETE_OBJECT(objPlans)
				
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -77.7099, -1019.9009, 27.5262 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 315.6337)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP()
				ENDIF
				
				DO_SCREEN_FADE_IN(500)
				
				bSkip = FALSE
				
			ELSE
			
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					iStageProgress = -1
				ELSE
					iStageProgress++
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE -1
			PRINT_NOW("S3A_LOSECOPS", DEFAULT_GOD_TEXT_TIME, 1)
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
			iStageProgress--
		BREAK
		
		CASE -2
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				CLEAR_PRINTS()
				iStageProgress = 1
			ENDIF
		BREAK
		
		CASE 1
		
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			SET_MAX_WANTED_LEVEL(0)
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "Drive back", TRUE)
			
			IF GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD) = 7
			
				REQUEST_ANIM_DICT("veh@bike@common@Front@base")
				
				IF HAS_ANIM_DICT_LOADED("veh@bike@common@Front@base")
				AND NOT IS_PED_RAGDOLL(PLAYER_PED_ID())
				
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					
//						SET_PED_CURRENT_WEAPON_VISIBLE(PLAYER_PED_ID(), FALSE)
						SET_PLAYER_CAN_USE_COVER(PLAYER_ID(), FALSE)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)	
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)	
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
						
						IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "veh@bike@common@Front@base", "take_off_helmet_walk")
							TASK_PLAY_ANIM(PLAYER_PED_ID(), "veh@bike@common@Front@base", "take_off_helmet_walk", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY | AF_SECONDARY)
						ELSE
							SETTIMERA(0)
							iStageProgress++
						ENDIF
						
					ELSE
					
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
							bHelmetHiddenInVeh = FALSE
							iStageProgress++
						ENDIF
						
					ENDIF
				ENDIF
				
			ELSE
			
				bHelmetHiddenInVeh = FALSE
				iStageProgress = 3
				
			ENDIF
			
		BREAK
		
		CASE 2
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			SET_MAX_WANTED_LEVEL(0)
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "veh@bike@common@Front@base", "take_off_helmet_walk")
				SET_PLAYER_CAN_USE_COVER(PLAYER_ID(), FALSE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)	
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)	
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
				PRINTLN(GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "veh@bike@common@Front@base", "take_off_helmet_walk"))
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
				IF NOT DOES_ENTITY_EXIST(objHat)
					IF GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD) = 7
						IF GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "veh@bike@common@Front@base", "take_off_helmet_walk") > 0.53990
							CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
							objHat = CREATE_OBJECT(PROP_HARD_HAT_01, GET_ENTITY_COORDS(PLAYER_PED_ID()))
							ATTACH_ENTITY_TO_ENTITY(objHat, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_L_HAND), <<0,0,0>>, <<0,90,0>>)
							PRINTLN("OBJECT CREATED AND ATTACHED")
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(objHat)
					IF IS_ENTITY_ATTACHED(objHat)
						PRINTLN("OBJECT DETACHED")
						DETACH_ENTITY(objHat)
						SET_ENTITY_COLLISION(objHat, TRUE)
						APPLY_FORCE_TO_ENTITY(objHat, APPLY_TYPE_FORCE, <<0,0,-0.1>>, <<0,0,0>>, 0, TRUE, TRUE, FALSE)
						SET_OBJECT_AS_NO_LONGER_NEEDED(objHat)
					ENDIF
				ENDIF
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 3
			IF TIMERA() > 1000
				PRINT_NOW("S3A_GOHOME", DEFAULT_GOD_TEXT_TIME, 1)
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 4
		
			IF NOT DOES_BLIP_EXIST(blipOffice)
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				blipOffice = CREATE_BLIP_FOR_COORD(vOffice, TRUE)
			ENDIF
			
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				LOSE_THE_COPS()
			ENDIF
			
			IF NOT IS_REPEAT_PLAY_ACTIVE()
				//Start the exit cutscene loading when 75m from the office.
				IF NOT bEndCutsceneRequested
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vOffice) < DEFAULT_CUTSCENE_LOAD_DIST*DEFAULT_CUTSCENE_LOAD_DIST
						SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_PRIME_BOARD_TRANSITION, TRUE)
						bEndCutsceneRequested = TRUE
					ENDIF
				ELSE
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vOffice) > DEFAULT_CUTSCENE_UNLOAD_DIST*DEFAULT_CUTSCENE_UNLOAD_DIST
						SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_PRIME_BOARD_TRANSITION, FALSE)
						bEndCutsceneRequested = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vOffice, g_vAnyMeansLocate, TRUE)
			
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<710.964172,-975.392273,29.263268>>, <<710.697571,-958.356934,32.594238>>, 2.250000)
				IF DOES_BLIP_EXIST(blipOffice)
					REMOVE_BLIP(blipOffice)
				ENDIF
				REPLAY_RECORD_BACK_FOR_TIME(7.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				Mission_Passed()
			ENDIF
			
		BREAK
		
	ENDSWITCH
	
	SET_PED_CAN_SWITCH_WEAPON(PLAYER_PED_ID(), NOT bBriefCaseForced)
	
	IF bBriefCaseForced
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<741.510010,-984.745178,23.692511>>, <<677.498474,-984.195007,33.708466>>, 17.250000)
		AND NOT IS_PED_IN_GARMENT_FACTORY(PLAYER_PED_ID())
			bBriefCaseForced = FALSE
			bBriefCaseShouldForceOnExitVehicle = FALSE
		ENDIF
	ELSE
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<741.510010,-984.745178,23.692511>>, <<677.498474,-984.195007,33.708466>>, 17.250000)
		OR IS_PED_IN_GARMENT_FACTORY(PLAYER_PED_ID())
			WEAPON_TYPE wep
			GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), wep, FALSE)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				IF NOT bBriefCaseShouldForceOnExitVehicle
					bBriefCaseShouldForceOnExitVehicle = TRUE
				ENDIF
			ELSE
				IF wep != WEAPONTYPE_BRIEFCASE_02
					IF IS_PED_IN_GARMENT_FACTORY(PLAYER_PED_ID())
					OR bBriefCaseShouldForceOnExitVehicle
						IF NOT IS_PHONE_ONSCREEN()
							GIVE_THE_CASE_TO_PED(PLAYER_PED_ID())
						ENDIF
					ENDIF
				ELSE
					bBriefCaseForced = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD

	CONST_INT NUMBER_OF_POS 200
	
	VECTOR vFireBase = << -91.2291, -994.3994, 103.2579 >>
	VECTOR vTargetBase = vFireBase + <<10,0,0>>
	VECTOR vFireAdd = <<0,0,1>>
	VECTOR vTargetAdd = <<0,0,1>>
	INT iCurrentTimeSection
	VECTOR vPositions[NUMBER_OF_POS]
	BOOL bFire = FALSE
	BOOL bReset = FALSE
	BOOL bLowAngle = FALSE
	
	TEXT_WIDGET_ID twLeadinStage
	TEXT_WIDGET_ID twArchFleeState
	TEXT_WIDGET_ID twCaseState
	TEXT_WIDGET_ID twSiteState
	TEXT_WIDGET_ID twAmbientState
	TEXT_WIDGET_ID twMugStage
	TEXT_WIDGET_ID twMissionStage
	TEXT_WIDGET_ID twCurrentWorker
	
	INT iFocusWorker = -1
	
	PROC BUILD_WIDGETS()
		hat_widget = START_WIDGET_GROUP("Agency 2A")
		
		twLeadinStage 	= ADD_TEXT_WIDGET("Leadin stage")
		twArchFleeState = ADD_TEXT_WIDGET("Architect flee state")
		twCaseState 	= ADD_TEXT_WIDGET("Current Case state")
		twSiteState 	= ADD_TEXT_WIDGET("Current site state")
		twAmbientState 	= ADD_TEXT_WIDGET("Current ambient state")
		twMissionStage 	= ADD_TEXT_WIDGET("Current mission Stage")
		
		ADD_WIDGET_INT_READ_ONLY("Stage progress", iStageProgress)
		ADD_WIDGET_INT_READ_ONLY("Architect progress", iArchStage)
		
		twMugStage 	= ADD_TEXT_WIDGET("Current mug Stage")
		
		twCurrentWorker = ADD_TEXT_WIDGET("Selected worker")
		
		ADD_WIDGET_BOOL("Can see player", 				bCanSeePlayer_widge)
		ADD_WIDGET_BOOL("Has spotted player", 			bSpottedPlayer_widge)
		ADD_WIDGET_FLOAT_READ_ONLY("Time spotted player", 	fSpotTime_widge)
		ADD_WIDGET_BOOL("Ambience tasked ", 			bTasked_widge)
		ADD_WIDGET_BOOL("Initialised", 					bInitialised_widge)
		ADD_WIDGET_BOOL("In combat", 					bCombat_widge)
		ADD_WIDGET_BOOL("Set to flee", 					bFlee_widge)
		ADD_WIDGET_BOOL("2 star wanted level given", 	bTwoStarWantedLevelGiven)
		ADD_WIDGET_BOOL("3 star wanted level given", 	bThreeStarWantedLevelGiven)
		ADD_WIDGET_BOOL("Player hit bottom floor",	 	bPlayerHasLeftTopFloor)
		
		
		STOP_WIDGET_GROUP()
		
	ENDPROC
	
	PROC RUN_WIDGETS()
	
		SET_CONTENTS_OF_TEXT_WIDGET(twArchFleeState , GET_STRING_FROM_FLEE_STATE(eRunawayState))
		SET_CONTENTS_OF_TEXT_WIDGET(twCaseState 	, GET_STRING_FROM_CASE_STATE(eCaseState))
		SET_CONTENTS_OF_TEXT_WIDGET(twSiteState 	, GET_STRING_FROM_SITE_STATE(eSiteState))
		SET_CONTENTS_OF_TEXT_WIDGET(twAmbientState 	, GET_STRING_FROM_AMBIENT_STATE(eAmbientSiteStage))
		SET_CONTENTS_OF_TEXT_WIDGET(twMissionStage 	, GET_STRING_FROM_MISSION_STAGE(mission_stage))
		SET_CONTENTS_OF_TEXT_WIDGET(twMugStage 		, GET_STRING_FROM_MUGGING_STATE(eMugStage))
		SET_CONTENTS_OF_TEXT_WIDGET(twLeadinStage	, GET_STRING_FROM_LEADIN_STAGE(eLeadinStage))
		
//		BOOL bWorkerSelected = FALSE
//		IF DOES_ENTITY_EXIST(GET_FOCUS_ENTITY_INDEX())
//			IF IS_ENTITY_A_PED(GET_FOCUS_ENTITY_INDEX())
//			AND IS_ENTITY_A_MISSION_ENTITY(GET_FOCUS_ENTITY_INDEX())
//				INT iTemp
//				REPEAT COUNT_OF(sConstructionWorkers) iTemp
//					IF DOES_ENTITY_EXIST(sConstructionWorkers[iTemp].ped)
//						IF sConstructionWorkers[iTemp].ped = GET_PED_INDEX_FROM_ENTITY_INDEX( GET_FOCUS_ENTITY_INDEX())
//							bWorkerSelected = TRUE
//							iFocusWorker = iTemp
//						ENDIF
//					ENDIF
//				ENDREPEAT
//			ENDIF
//		ENDIF
//		
//		IF NOT bWorkerSelected
//			iFocusWorker = -1
//		ENDIF
		
		IF iFocusWorker >= 0
			
			TEXT_LABEL_31 sTemp = "Worker "
			sTemp += iFocusWorker		
		
			SET_CONTENTS_OF_TEXT_WIDGET(twCurrentWorker, sTemp)
			
			bCanSeePlayer_widge			= sConstructionWorkers[iFocusWorker].bCanSeePlayer
			bSpottedPlayer_widge		= sConstructionWorkers[iFocusWorker].bSpottedPlayer
			fSpotTime_widge				= sConstructionWorkers[iFocusWorker].fSpotTime
			bTasked_widge				= sConstructionWorkers[iFocusWorker].bTasked
			bInitialised_widge			= sConstructionWorkers[iFocusWorker].bInitialised
			bCombat_widge				= sConstructionWorkers[iFocusWorker].bCombat
			bFlee_widge					= sConstructionWorkers[iFocusWorker].bFlee
			
		ENDIF

	ENDPROC
	
	PROC THROW_TEST()
		INT iTemp
		SWITCH iStageProgress
			CASE 0
				REQUEST_MODEL(PROP_HARD_HAT_01)
				IF HAS_MODEL_LOADED(PROP_HARD_HAT_01)
					DO_PLAYER_MAP_WARP_WITH_LOAD(<< -96.9235, -987.2451, 103.2579 >>, 239.3236)
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
					SET_WIND(0)
					iStageProgress++
				ENDIF
			BREAK
			CASE 1
				IF NOT DOES_ENTITY_EXIST(objHat)
					REPEAT COUNT_OF(vPositions) iTemp
						vPositions[iTemp] = <<0,0,0>>
					ENDREPEAT
					objHat = CREATE_OBJECT(PROP_HARD_HAT_01, vFireBase + vFireAdd)
					FREEZE_ENTITY_POSITION(objHat, TRUE)
				ELSE
					iStageProgress++
				ENDIF
			BREAK
			CASE 2
			
				SET_ENTITY_COORDS(objHat, vFireBase + vFireAdd)
				
				IF bReset bReset = FALSE ENDIF
				
				fTraject = GET_ANGLE_OF_TRAJECTORY(vFireBase + vFireAdd, vTargetBase + vTargetAdd, u, bLowAngle)
				
				VECTOR vDiff
				vDiff = (vTargetBase + vTargetAdd) - (vFireBase + vFireAdd)				
				vDiff.z = 0
				VECTOR vHatVel
				vHatVel = vDiff
				SET_VECTOR_MAGNITUDE(vHatVel, u*cos(fTraject))
				vHatVel.z = u*sin(fTraject)
				
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(objHat), GET_ENTITY_COORDS(objHat) + vHatVel)
				DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(vDiff.z), <<0.9, 0.02, 0>>)
				DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(fTraject), <<0.9, 0.04, 0>>)
				
				WHILE NOT HAS_COLLISION_FOR_MODEL_LOADED(PROP_HARD_HAT_01)
					WAIT(0)
				ENDWHILE
				
				SET_OBJECT_PHYSICS_PARAMS(objHat, 1.0, 1.0, <<0,0,0>>, <<0,0,0>>)				
				SET_DAMPING(objHat, PHYSICS_DAMPING_LINEAR_C, 0.0)
				SET_DAMPING(objHat, PHYSICS_DAMPING_LINEAR_V, 0.0)
				SET_DAMPING(objHat, PHYSICS_DAMPING_LINEAR_V2, 0.0)
				SET_DAMPING(objHat, PHYSICS_DAMPING_ANGULAR_C, 0.0)
				SET_DAMPING(objHat, PHYSICS_DAMPING_ANGULAR_V, 0.0)
				SET_DAMPING(objHat, PHYSICS_DAMPING_ANGULAR_V2, 0.0)
				
				IF bFire
					FREEZE_ENTITY_POSITION(objHat, FALSE)
					WAIT(0)
					SET_ENTITY_VELOCITY(objHat, vHatVel)
					vPositions[0] = GET_ENTITY_COORDS(objHat)
					iCurrentTimeSection = 1
					SETTIMERA(0)
					iStageProgress++
				ENDIF
				
			BREAK
			
			CASE 3
				IF bFire bFire = FALSE ENDIF
				
				IF iCurrentTimeSection < 100
					vPositions[iCurrentTimeSection] = GET_ENTITY_COORDS(objHat)
					iCurrentTimeSection++
				ENDIF
				
				REPEAT COUNT_OF(vPositions) iTemp
					IF iTemp >0
						IF NOT ARE_VECTORS_EQUAL(vPositions[iTemp], <<0,0,0>>)
							IF iTemp = 49
							OR iTemp = 50
								DRAW_DEBUG_SPHERE(vPositions[iTemp], 0.05, 255, 0, 0)
							ELSE
								DRAW_DEBUG_SPHERE(vPositions[iTemp], 0.05)
							ENDIF
							DRAW_DEBUG_LINE(vPositions[iTemp], vPositions[iTemp-1])
						ENDIF
					ENDIF
				ENDREPEAT
				
				DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(timera()), <<0.02,0.04,0>>)
				DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(t*1000), <<0.02,0.06,0>>)
				
				IF bReset
					DELETE_OBJECT(objHat)
					iStageProgress=1
					bReset = FALSE
					bFire = FALSE
				ENDIF
			BREAK
		ENDSWITCH
		
		DRAW_DEBUG_SPHERE(vFireBase+vFireAdd, 0.25, 0, 255, 0, 50)
		DRAW_DEBUG_SPHERE(vTargetBase+vTargetAdd, 0.25, 0, 255, 0, 50)
		
		VECTOR vP1 = vFireBase + vFireAdd
		VECTOR vP2 = vTargetBase + vTargetAdd
		FLOAT yDiff = vP2.z - vP1.z
		
		vP1.z = 0
		vP2.z = 0
		FLOAT xDiff = VDIST(vP1, vP2)
		
		INT iCountPathNodes = 100
		
		FOR iTemp = 0 TO iCountPathNodes
			FLOAT x
			FLOAT y
			FLOAT iStep
			iStep = (iTemp) * (1.0 / iCountPathNodes)
			FLOAT tStep = xDiff/(u*cos(fTraject))
			tStep *= iStep
			x = u*cos(fTraject) * tStep
			y = (u*sin(fTraject) * tStep)  - 0.5 * g * (tStep*tStep)
			VECTOR vDistx
			vDistx = (vTargetBase+vTargetAdd) - (vFireBase+vFireAdd)
			vDistx.z = 0
			SET_VECTOR_MAGNITUDE(vDistx, x)
			VECTOR vStart
			vStart = vFireBase + vFireAdd
			DRAW_DEBUG_SPHERE(vStart + vDistx + <<0, 0, y>>, 0.1, 255, 0, 0)
		ENDFOR
		
		TEXT_LABEL_63 sTemp
		sTemp = "X Val: "
		sTemp += GET_STRING_FROM_FLOAT(xDiff)
		DRAW_DEBUG_TEXT_2D(sTemp, <<0.8, 0.1, 0>>)
		sTemp = "Y Val: "
		sTemp += GET_STRING_FROM_FLOAT(yDiff)
		DRAW_DEBUG_TEXT_2D(sTemp, <<0.8, 0.12, 0>>)
		
	ENDPROC
	
#ENDIF

OBJECT_INDEX objGateHolder[4]
VECTOR vOpenRotations[4]
VECTOR vGrabPositions[4]
INT iLastGateCheck

PROC RESET_GATE_MONITOR()

	vOpenRotations[0] = <<0.0000, 0.0001, 64.0457>>
	vOpenRotations[1] = <<0.0000, 0.0002, -99.6494>>
	vOpenRotations[2] = <<-0.0002, 0.0003, -60.1200>>
	vOpenRotations[3] = <<-0.0014, 0.0000, -179.0270>>
	
	vGrabPositions[0] = <<-83.0655, -1029.2957, 28.8274>>
	vGrabPositions[1] = <<-79.1160, -1017.8572, 29.2833>>
	vGrabPositions[2] = <<-226.1044, -1122.3464, 23.6851>>
	vGrabPositions[3] = <<-213.9111, -1122.3636, 23.6257>>
	
	INT iTemp
	REPEAT COUNT_OF(objGateHolder) iTemp
		IF DOES_ENTITY_EXIST(objGateHolder[iTemp])
			SET_OBJECT_AS_NO_LONGER_NEEDED(objGateHolder[iTemp])
		ENDIF
	ENDREPEAT
	
	iLastGateCheck = 0
	
ENDPROC

PROC MONITOR_GATES()
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
		IF GET_GAME_TIMER() - iLastGateCheck > 1000
			INT iTemp
			REPEAT COUNT_OF(objGateHolder) iTemp
				IF NOT DOES_ENTITY_EXIST(objGateHolder[iTemp])
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vGrabPositions[iTemp]) < 100*100
					AND NOT IS_SPHERE_VISIBLE(vGrabPositions[iTemp], 10)
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(vGrabPositions[iTemp], 1, PROP_FNCLINK_03GATE4)
							objGateHolder[iTemp] = GET_CLOSEST_OBJECT_OF_TYPE(vGrabPositions[iTemp], 2, PROP_FNCLINK_03GATE4)
							SET_ENTITY_AS_MISSION_ENTITY(objGateHolder[iTemp], TRUE, TRUE)
							SET_ENTITY_ROTATION(objGateHolder[iTemp], vOpenRotations[iTemp])
							FREEZE_ENTITY_POSITION(objGateHolder[iTemp], TRUE)
						ENDIF
					ENDIF
				ELSE
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vGrabPositions[iTemp]) > 150*150
						FREEZE_ENTITY_POSITION(objGateHolder[iTemp], FALSE)
						SET_OBJECT_AS_NO_LONGER_NEEDED(objGateHolder[iTemp])
					ENDIF
				ENDIF
			ENDREPEAT
			iLastGateCheck = GET_GAME_TIMER()
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_LEADIN()

	IF NOT IS_REPEAT_PLAY_ACTIVE()
	
		SWITCH eLeadinStage
		
			CASE AH2_LS_WAIT_FOR_ACTIVATION
						
				IF (mission_stage = STAGE_DRIVE_BACK OR mission_stage = STAGE_LOSE_COPS)
				AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vOffice) < 300*300
				
					IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
					OR NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
					OR NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
					OR NOT DOES_ENTITY_EXIST(vehMike)
					
						REQUEST_ANIM_DICT("MISSAH_2_EXT_ALTLEADINOUT")
						REQUEST_MODEL(PROP_CS_WALKING_STICK)
						REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_LESTER))
						REQUEST_MODEL(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
						REQUEST_MODEL(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
						
						IF 	HAS_ANIM_DICT_LOADED("MISSAH_2_EXT_ALTLEADINOUT")
						AND HAS_MODEL_LOADED(PROP_CS_WALKING_STICK)
						AND HAS_MODEL_LOADED(GET_NPC_PED_MODEL(CHAR_LESTER))
						AND HAS_MODEL_LOADED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
						AND HAS_MODEL_LOADED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
						
							interiorGarmentFactory = GET_INTERIOR_AT_COORDS(<<714.0795, -964.6153, 29.3953>>)
							PIN_INTERIOR_IN_MEMORY(interiorGarmentFactory)
							
							//Create Lester at his computer.
							CREATE_NPC_PED_ON_FOOT(g_sTriggerSceneAssets.ped[0], CHAR_LESTER, << 707.350, -967.2, 30.400 >>)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_LESTER))
							TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[0], rgBuddies)
							TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[0], "MISSAH_2_EXT_ALTLEADINOUT", "HACK_LOOP", << 707.350, -967.2, 30.400 >>, << 0.000, 0.000, 175.000 >>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_USE_KINEMATIC_PHYSICS)
							
							//Create Lester's walking stick on the table next to him.
							g_sTriggerSceneAssets.object[0] = CREATE_OBJECT(PROP_CS_WALKING_STICK, <<706.987366,-967.094543,30.400000>>)
							SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_WALKING_STICK)
							SET_ENTITY_COORDS (g_sTriggerSceneAssets.object[0], <<706.987366,-967.094543,30.400000>>)
							SET_ENTITY_ROTATION (g_sTriggerSceneAssets.object[0], <<90.00, 0.00, 160.00>>)
							FREEZE_ENTITY_POSITION(g_sTriggerSceneAssets.object[0], TRUE)
							
							//Create Michael
							CREATE_PLAYER_PED_ON_FOOT(g_sTriggerSceneAssets.ped[1], CHAR_MICHAEL, << 705.431, -965.364, 30.415 >>, 0.0, FALSE)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
							TRIGGER_SCENE_SETUP_FRIENDLY_PED(g_sTriggerSceneAssets.ped[1], rgBuddies)
							TASK_PLAY_ANIM_ADVANCED(g_sTriggerSceneAssets.ped[1], "MISSAH_2_EXT_ALTLEADINOUT", "SOFA_LOOP", << 705.431, -965.364, 30.415 >>, << 0.000, 0.000, -75.960 >>, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_USE_KINEMATIC_PHYSICS)
							
							CLEAR_AREA_OF_VEHICLES(<<719.9169, -982.8645, 23.1393>>, 7.5)
							IF NOT IS_ENTITY_DEAD(vehMike)
								IF NOT IS_ENTITY_AT_ENTITY(vehMike, PLAYER_PED_ID(), << 150, 150, 150 >>)
									DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MISSION_VEH)
									CREATE_PLAYER_VEHICLE(vehMike, CHAR_MICHAEL, <<706.6290, -979.8341, 23.1355>>, 45.3224)
									SET_MISSION_VEHICLE_GEN_VEHICLE(vehMike, <<706.6290, -979.8341, 23.1355>>, 45.3224)
								ENDIF
							ELSE
								DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_MISSION_VEH)
								CREATE_PLAYER_VEHICLE(vehMike, CHAR_MICHAEL, <<706.6290, -979.8341, 23.1355>>, 45.3224)
								SET_MISSION_VEHICLE_GEN_VEHICLE(vehMike, <<706.6290, -979.8341, 23.1355>>, 45.3224)
							ENDIF
							
							//Bind the player's car to global index so the heist controller can clean it up.
							g_sTriggerSceneAssets.veh[0] = vehPlayerCar
							
							#IF IS_DEBUG_BUILD
								PRINTLN("LEADOUT SCENE CREATED!!!!")
							#ENDIF
							
							eLeadinStage = AH2_LS_WAIT
							
						ENDIF
						
					ENDIF
					
				ENDIF
				
			BREAK
			
			CASE AH2_LS_AWAIT_INTERIOR
			
				IF IS_INTERIOR_READY(interiorGarmentFactory)
					PIN_INTERIOR_IN_MEMORY(interiorGarmentFactory)
					SET_INTERIOR_ACTIVE(interiorGarmentFactory, TRUE)
					eLeadinStage = AH2_LS_WAIT
				ENDIF
				
			BREAK
			
			CASE AH2_LS_WAIT
			
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vOffice) > 150*150
				OR (mission_stage != STAGE_DRIVE_BACK AND mission_stage != STAGE_LOSE_COPS)
					IF IS_INTERIOR_READY(interiorGarmentFactory)
						UNPIN_INTERIOR(interiorGarmentFactory)
						SET_INTERIOR_ACTIVE(interiorGarmentFactory, FALSE)
					ENDIF
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
						DELETE_PED(g_sTriggerSceneAssets.ped[0])
					ENDIF
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
						DELETE_PED(g_sTriggerSceneAssets.ped[1])
					ENDIF
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
						DELETE_OBJECT(g_sTriggerSceneAssets.object[0])
					ENDIF
					REMOVE_ANIM_DICT("MISSAH_2_EXT_ALTLEADINOUT")
					SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_WALKING_STICK)
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_LESTER))
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
					eLeadinStage = AH2_LS_WAIT_FOR_ACTIVATION
				ENDIF
				
			BREAK
			
		ENDSWITCH
		
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0 
	OR eLeadinStage != AH2_LS_WAIT
	
		IF NOT IS_REPEAT_PLAY_ACTIVE()
		AND NOT IS_PED_IN_GARMENT_FACTORY(PLAYER_PED_ID())
		
			IF DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_L)) != 0
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_L), 0)
			ENDIF
			
			IF DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_R)) != 0
				DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_R), 0)
			ENDIF
			
			IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_L)) != DOORSTATE_LOCKED
				SET_DOOR_STATE(DOORNAME_SWEATSHOP_L, DOORSTATE_LOCKED)
			ENDIF
			
			IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_R)) != DOORSTATE_LOCKED
				SET_DOOR_STATE(DOORNAME_SWEATSHOP_R, DOORSTATE_LOCKED)
			ENDIF
			
		ENDIF
		
	ELSE
		
		IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_L)) != DOORSTATE_UNLOCKED
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_L, DOORSTATE_UNLOCKED)
		ENDIF
		
		IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_R)) != DOORSTATE_UNLOCKED
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_R, DOORSTATE_UNLOCKED)
		ENDIF
		
	ENDIF
	
	IF (mission_stage = STAGE_DRIVE_BACK AND (iStageProgress > 4 OR iStageProgress < 0 ))
	OR (mission_stage = STAGE_LOSE_COPS)
		IF IS_PED_IN_GARMENT_FACTORY(PLAYER_PED_ID())
			SET_WANTED_LEVEL_MULTIPLIER(0.1)
			SET_MAX_WANTED_LEVEL(0)
		ELSE
			SET_WANTED_LEVEL_MULTIPLIER(1)
			SET_MAX_WANTED_LEVEL(5)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
		OR IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
			Mission_Failed("CMN_LDIED")
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
		IF IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
		OR IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[1])
			Mission_Failed("CMN_MDIED")
		ENDIF
	ENDIF
	
ENDPROC

// ____________________________________ MISSION SCRIPT _____________________________________

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED()
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<706.9233, -965.4952, 29.4179>>, 282.8027)
		SET_REPLAY_REJECTED_CHARACTER(CHAR_MICHAEL)
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_PRIME_BOARD_TRANSITION, FALSE)
		Mission_Flow_Mission_Force_Cleanup()
		MISSION_CLEANUP()
	ENDIF
	
	//____________________________________ MISSION LOOP _______________________________________
	
	SET_MISSION_FLAG(TRUE)
	INCRAMENT_Pause_Outro_Count()
	initialiseMission()
	RESET_GATE_MONITOR()
	INIT_SITE_BOUNDARY()
	INIT_LIFT_VARIABLES()
	
	WHILE NOT CREATE_LIFT(constructionLifts[LS_SOUTH_A])
	OR NOT CREATE_LIFT(constructionLifts[LS_SOUTH_B])
	OR NOT CREATE_LIFT(constructionLifts[LS_WEST_A])
	OR NOT CREATE_LIFT(constructionLifts[LS_WEST_B])
		WAIT(0)
	ENDWHILE
	
	#IF IS_DEBUG_BUILD
		BUILD_WIDGETS()
	#ENDIF
	
	WHILE TRUE
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_BR2")
		
		vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
//		IF NOT IS_REPEAT_PLAY_ACTIVE()
			MANAGE_LEADIN()
//		ENDIF
		
		MONITOR_GATES()
		UPDATE_ARCHITECT_CAN_SEE_PLAYER()
		RUN_MUGGING()
		
		SWITCH mission_stage
		
			CASE STAGE_GROUND_FLOOR
				GROUND_FLOOR_SECTION()
			BREAK
			
			CASE STAGE_FIRST_FLOOR
				FIRST_FLOOR_SECTION()
			BREAK
			
			CASE STAGE_ESCAPE
				LEAVE_CONSTRUCTION_SITE()
			BREAK
			
			CASE STAGE_DRIVE_BACK
				DRIVE_BACK()
			BREAK
			
			CASE STAGE_PLAYER_SPOTTED
				PLAYER_SPOTTED_BY_ARCHITECT()
			BREAK
			
			CASE STAGE_COVER_BLOWN
				COVER_BLOWN()
			BREAK
			
			CASE STAGE_LOSE_COPS
				LOSE_THE_COPS()
			BREAK
			
			CASE STAGE_TEST
			
				#IF IS_DEBUG_BUILD
				
					SWITCH iStageProgress
						CASE 0
							CLEAR_AREA(<<-138.4194, -968.3976, 113.1382>>, 3000, TRUE)
							WHILE NOT CREATE_ARCHITECT(<<-138.4194, -968.3976, 113.1382>>, 327.3889)
								WAIT(0)
							ENDWHILE
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedArchitect, TRUE)
							SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-141.5894, -974.9841, 113.1382>>)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 355.4739)
							bskip = FALSE
							iStageProgress++
						BREAK
						CASE 1
						
						BREAK
					ENDSWITCH
					
				#ENDIF
				
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("Unrecognised Mission Stage!")
			BREAK
			
		ENDSWITCH
		
		INT iCountLifts	
		
		REPEAT COUNT_OF(constructionLifts) iCountLifts
			UPDATE_LIFT(constructionLifts[iCountLifts])
		ENDREPEAT
		
		CHECK_DIALOGUE_PEDS()
		
		#IF IS_DEBUG_BUILD
			debugProcedures()
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
				IF NOT IS_PED_INJURED(pedArchitect)
//					SET_PED_TO_RAGDOLL(pedArchitect, 3000, 3000, TASK_RELAX, FALSE, FALSE)
					EXPLODE_PED_HEAD(pedArchitect)
				ENDIF
			ENDIF
			RUN_WIDGETS()
		#ENDIF
		
		IF bHeliOneGo
			DO_HELI_FLY_BY()
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD) = 7
				IF mission_stage != STAGE_DRIVE_BACK
					bHelmetHiddenInVeh = TRUE
				ENDIF
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
			ENDIF
		ELSE
			IF GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD) <> 7
			AND bHelmetHiddenInVeh
				bHelmetHiddenInVeh = FALSE
				SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD, 7)
			ENDIF
		ENDIF
		
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID())
		
		IF NOT bSkip
			MANAGE_SITE()
		ENDIF
		
		IF NOT IS_PED_INJURED(pedArchitect)
			IF IS_ENTITY_AT_COORD(pedArchitect, <<-183.187332,-1022.582825,28.039276>>,<<6.000000,10.000000,3.000000>>)
				SET_PED_RESET_FLAG(pedArchitect, PRF_UseProbeSlopeStairsDetection, TRUE)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedArchitect)
			IF NOT bBigScream
				IF NOT IS_PED_INJURED(pedArchitect)
				AND (IS_ENTITY_AT_COORD(pedArchitect, <<-160.438492,-938.426514,115.434189>>, <<10,10,10>>) OR IS_ENTITY_AT_COORD(pedArchitect, <<-186.056900,-1017.814758,115.429977>>, <<10,10,10>>))
					IF NOT IS_ENTITY_IN_ANY_LIFT(pedArchitect)
					AND (IS_ENTITY_IN_ANGLED_AREA(pedArchitect, <<-160.438492,-938.426514,115.434189>>, <<-162.479965,-944.109070,108.255920>>, 9.250000) 
							OR IS_ENTITY_IN_ANGLED_AREA( pedArchitect, <<-186.056900,-1017.814758,115.429977>>, <<-180.341431,-1019.817810,108.076027>>, 4.750000))
						PLAY_PAIN(pedArchitect, AUD_DAMAGE_REASON_SUPER_FALLING, 100)
						bBigScream = TRUE
					ENDIF
				ENDIF
			ELSE
				IF NOT bBigSplat
					IS_ENTITY_DEAD(pedArchitect) //added to avoid assert
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(pedArchitect)) < 900
						IF GET_ENTITY_HEIGHT_ABOVE_GROUND(pedArchitect) < 2
						AND (IS_PED_INJURED(pedArchitect) OR IS_ENTITY_DEAD(pedArchitect)) 
							PLAY_SOUND_FROM_ENTITY(-1, "Architect_Fall", pedArchitect, "FBI_HEIST_SOUNDSET")
							PRINTLN("\n\n\n SPLAT!! \n\n\n")
							bBigSplat = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_2_TAKE_OUT_ARCHITECT")
			IF DOES_ENTITY_EXIST(pedArchitect)
				IF IS_PED_INJURED(pedArchitect)
				OR IS_PED_IN_WRITHE(pedArchitect)
				OR bArchIsSecure
				OR bArchCowering
					STOP_AUDIO_SCENE("AGENCY_H_2_TAKE_OUT_ARCHITECT")				
				ENDIF
			ENDIF
		ENDIF
		
		MANAGE_CASE()
				
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-147.622147,-1033.464478,38.502144>>) < 250*250
			IF NOT DOES_ENTITY_EXIST(objBarrier)
				REQUEST_MODEL(PROP_CONST_FENCE02B)
				IF HAS_MODEL_LOADED(PROP_CONST_FENCE02B)
					objBarrier = CREATE_OBJECT(PROP_CONST_FENCE02B, <<-147.622147,-1033.464478,38.402144>>)
					SET_ENTITY_ROTATION(objBarrier, <<-0.000000,0.000000,-17.761690>>)
					FREEZE_ENTITY_POSITION(objBarrier, TRUE)
					SET_ENTITY_CAN_BE_DAMAGED(objBarrier, FALSE)
					SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CONST_FENCE02B)
				ENDIF
			ENDIF
		ELIF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-147.622147,-1033.464478,38.502144>>) > 300*300
			IF DOES_ENTITY_EXIST(objBarrier)
				DELETE_OBJECT(objBarrier)
			ENDIF
		ENDIF
		
		IF bSkip
		
			IF IS_GAMEPLAY_HINT_ACTIVE()
				STOP_GAMEPLAY_HINT(TRUE)
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_2_FOLLOW_ARCHITECT_1")
				STOP_AUDIO_SCENE("AGENCY_H_2_FOLLOW_ARCHITECT_1")
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_2_USE_ELEVATOR")
				STOP_AUDIO_SCENE("AGENCY_H_2_USE_ELEVATOR")
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_2_FOLLOW_ARCHITECT_2")
				STOP_AUDIO_SCENE("AGENCY_H_2_FOLLOW_ARCHITECT_2")
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_2_TAKE_OUT_ARCHITECT")
				STOP_AUDIO_SCENE("AGENCY_H_2_TAKE_OUT_ARCHITECT")
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_2_LEAVE_SITE")
				STOP_AUDIO_SCENE("AGENCY_H_2_LEAVE_SITE")
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_2_COVER_BLOWN")
				STOP_AUDIO_SCENE("AGENCY_H_2_COVER_BLOWN")
			ENDIF
			
			fTotalLurkingTime = 0
			
		ENDIF
		
		WAIT(0)
		
	ENDWHILE
	
ENDSCRIPT
