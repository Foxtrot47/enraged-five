
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "script_blips.sch"
USING "locates_public.sch"
USING "battlebuddy_public.sch"
USING "replay_public.sch"
USING "friendActivity_private.sch"
USING "friendUtil_dialogue_private.sch"
USING "commands_recording.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	agency_prep1.sc
//		AUTHOR			:	Paul D
//		DESCRIPTION		:	Take a firetruck to the garment factory
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

CONST_INT  AGENCY_P1_NUMBER_OF_FIREMEN 	8

BLIP_INDEX blipDest
BLIP_INDEX blipVeh
VEHICLE_INDEX vehFiretruck
VEHICLE_INDEX vehTowTruck
PED_INDEX pedFiremen[AGENCY_P1_NUMBER_OF_FIREMEN]
REL_GROUP_HASH rgFireMen

structPedsForConversation sSpeech

INT iPrepProgress = 0
INT iLoseCopsPrintTime
VECTOR vDest = <<693.11584, -1012.41815, 21.72802>>

BOOL bGetInPrinted = FALSE
BOOL bGoHomePrinted = FALSE
BOOL bDestCleared
BOOL bStatsStarted
BOOL bWentToFireStation = FALSE
BOOL bLoseCopsPrinted = FALSE
BOOL bGetOut = FALSE
BOOL bEmergecyCallStatGive = FALSE
BOOL bCurrentlyTowingTruck = FALSE
BOOL bReportFired = FALSE

/// PURPOSE:
///    Checks if the player is in the fire station
/// RETURNS:
///    TRUE if in the fire station. FALSE otherwise
FUNC BOOL IS_PLAYER_IN_FIRE_STATION()
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<226.676544,-1668.942139,25.460825>>, <<185.644638,-1635.835938,39.291840>>, 57.750000)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

ENUM AHP_BB_STATE
	AHPBB_SCAN_FOR_DRIVING_BUDDIES = 0,
	AHPBB_BUDDY_IS_DRIVING,
	AHPBB_RELEASE_BUDDY
ENDENUM

AHP_BB_STATE buddyState
enumCharacterList eDriver

VECTOR vTunnel1 = <<692.8695, -998.7867, 22.3201>>
VECTOR vTunnel2 = <<693.3784, -1025.6710, 21.5060>>
VECTOR vLoadMin, vLoadMax

ENUM PREP_CONVERSATIONS
	PREP_CONV_GET_LABELS,
	PREP_CONV_START_CONV,
	PREP_CONV_PLAYING,
	PREP_CONV_FINISHED
ENDENUM

PREP_CONVERSATIONS ePrepConv
TEXT_LABEL sBlock
TEXT_LABEL sRoot

BOOL bConvOnHold
BOOL bFirstConvStarted
TEXT_LABEL_23 sResumeLabel
INT iConvFinished
INT iBuddyDriveHomeStage

PROC MANAGE_BATTLEBUDDY_DIALOGUE()

	BOOL bCanTalk
	bCanTalk = FALSE
	
	IF NOT IS_PED_INJURED(GET_BATTLEBUDDY_PED(eDriver))
	AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
		IF DOES_ENTITY_EXIST(vehFiretruck) AND IS_VEHICLE_DRIVEABLE(vehFiretruck) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehFiretruck) AND IS_PED_IN_VEHICLE(GET_BATTLEBUDDY_PED(eDriver), vehFiretruck)
		OR DOES_ENTITY_EXIST(vehTowTruck) AND IS_VEHICLE_DRIVEABLE(vehTowTruck) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTowTruck) AND IS_PED_IN_VEHICLE(GET_BATTLEBUDDY_PED(eDriver), vehTowTruck)
			bCanTalk = TRUE
		ENDIF
	ENDIF
	
	SWITCH ePrepConv
		CASE PREP_CONV_GET_LABELS
			INT iCond
			iCond = Private_GetFriendChatConditions()
			IF Private_GetFriendChat(CHAR_MICHAEL, CHAR_FRANKLIN, NO_CHARACTER, FCHAT_TypeFull, iCond, sBlock, sRoot, TRUE)
				REMOVE_PED_FOR_DIALOGUE(sSpeech, 0)
				REMOVE_PED_FOR_DIALOGUE(sSpeech, 1)
				REMOVE_PED_FOR_DIALOGUE(sSpeech, 2)
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					ADD_PED_FOR_DIALOGUE(sSpeech, 0, GET_BATTLEBUDDY_PED(CHAR_MICHAEL), "MICHAEL")
					ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "FRANKLIN")
				ELSE
					ADD_PED_FOR_DIALOGUE(sSpeech, 1, GET_BATTLEBUDDY_PED(CHAR_FRANKLIN), "FRANKLIN")
					ADD_PED_FOR_DIALOGUE(sSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
				ENDIF
				IF NOT bFirstConvStarted
					iConvFinished = GET_GAME_TIMER()
				ENDIF
				ePrepConv = PREP_CONV_START_CONV
			ENDIF
		BREAK
		CASE PREP_CONV_START_CONV
			IF bCanTalk
				IF GET_GAME_TIMER() - iConvFinished > 5000
				OR bFirstConvStarted
					IF CREATE_CONVERSATION(sSpeech, sBlock, sRoot, CONV_PRIORITY_HIGH)
						bFirstConvStarted = TRUE
						ePrepConv = PREP_CONV_PLAYING
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE PREP_CONV_PLAYING
			IF NOT bConvOnHold
				IF NOT bCanTalk
					sResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
					KILL_ANY_CONVERSATION()
					bConvOnHold = TRUE
				ENDIF
			ELSE
				IF bCanTalk
					IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, sBlock, sRoot, sResumeLabel, CONV_PRIORITY_HIGH)
						bConvOnHold = FALSE
					ENDIF
				ENDIF
			ENDIF
			IF IS_PLAYER_SWITCH_IN_PROGRESS()
			OR (NOT bConvOnHold AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED())
				KILL_FACE_TO_FACE_CONVERSATION()
				ePrepConv = PREP_CONV_FINISHED
				iConvFinished = GET_GAME_TIMER()
			ENDIF
		BREAK
		CASE PREP_CONV_FINISHED
			IF GET_GAME_TIMER() - iConvFinished > 10000
				ePrepConv = PREP_CONV_GET_LABELS				
			ENDIF			
		BREAK
	ENDSWITCH
ENDPROC

PROC MANAGE_BATTLE_BUDDIES_DRIVING()

	VEHICLE_INDEX vehTemp
	IF bCurrentlyTowingTruck
		vehTemp = vehTowTruck
	ELSE
		vehTemp = vehFiretruck
	ENDIF
	
	IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
	
		SWITCH buddyState
		
			CASE AHPBB_SCAN_FOR_DRIVING_BUDDIES
				IF IS_VEHICLE_DRIVEABLE(vehTemp)
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTemp)
				AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					enumCharacterList eChar
					REPEAT MAX_BATTLE_BUDDIES eChar
						PED_INDEX hBuddy
						hBuddy = GET_BATTLEBUDDY_PED(eChar)
						IF NOT IS_PED_INJURED(hBuddy)
						AND IS_PED_IN_VEHICLE(hBuddy, vehTemp)
						AND GET_PED_IN_VEHICLE_SEAT(vehTemp) = hBuddy
							IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddy)
							AND IS_BATTLEBUDDY_AVAILABLE(hBuddy, FALSE)
								IF OVERRIDE_BATTLEBUDDY(hBuddy, FALSE)
									iBuddyDriveHomeStage = 0
									eDriver = eChar
									buddyState = AHPBB_BUDDY_IS_DRIVING
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
			BREAK
			
			CASE AHPBB_BUDDY_IS_DRIVING
				IF IS_VEHICLE_DRIVEABLE(vehTemp)
					IF IS_PED_INJURED(GET_BATTLEBUDDY_PED(eDriver))
					OR GET_PED_IN_VEHICLE_SEAT(vehTemp) != GET_BATTLEBUDDY_PED(eDriver)
					OR NOT IS_PED_IN_VEHICLE(GET_BATTLEBUDDY_PED(eDriver), vehTemp)
					OR NOT IS_BATTLEBUDDY_OVERRIDDEN(GET_BATTLEBUDDY_PED(eDriver))
					OR NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTemp)
						RELEASE_BATTLEBUDDY(GET_BATTLEBUDDY_PED(eDriver))
						buddyState = AHPBB_SCAN_FOR_DRIVING_BUDDIES
					ELSE
						SWITCH iBuddyDriveHomeStage
							CASE 0
//								FLOAT fDist
//								fDist = VDIST(GET_ENTITY_COORDS(vehFiretruck), vDest)
//								IF fDist > 280
//									vTruckPos = GET_ENTITY_COORDS(vehFiretruck)
//									vMidpoint = vTruckPos + (vDest - vTruckPos)/2.0
//									vLoadMax = vMidpoint + <<fDist + 100, fDist + 100, 0>>
//									vLoadMin = vMidpoint - <<fDist + 100, fDist + 100, 0>>
//									LOAD_PATH_NODES_IN_AREA(vLoadMin.x, vLoadMin.y, vLoadMax.x, vLoadMax.y)
//									iBuddyDriveHomeStage++
//								ELSE
									iBuddyDriveHomeStage=2
//								ENDIF
							BREAK
							CASE 1
								IF ARE_NODES_LOADED_FOR_AREA(vLoadMin.x, vLoadMin.y, vLoadMax.x, vLoadMax.y)
									iBuddyDriveHomeStage++
								ENDIF
							BREAK
							CASE 2
								TASK_VEHICLE_DRIVE_TO_COORD(GET_BATTLEBUDDY_PED(eDriver), vehTemp, vDest, 15, DRIVINGSTYLE_NORMAL, FIRETRUK, DRIVINGMODE_AVOIDCARS_RECKLESS | DF_SteerAroundPeds, 5, 5)
								IF VDIST(GET_ENTITY_COORDS(vehTemp), vDest) > 250
									SET_VEHICLE_CUSTOM_PATH_NODE_STREAMING_RADIUS(vehTemp, VDIST(GET_ENTITY_COORDS(vehFiretruck), vDest))
								ENDIF
								iBuddyDriveHomeStage++
							BREAK
							CASE 3
								IF IS_ENTITY_IN_ANGLED_AREA(vehTemp, <<697.688416,-1013.363770,27.496796>>, <<688.348206,-1013.179138,22.329676>>, 29.250000)
									IF VDIST(GET_ENTITY_COORDS(vehTemp), vTunnel1) < VDIST(GET_ENTITY_COORDS(vehTemp), vTunnel2)
										IF bCurrentlyTowingTruck
											TASK_VEHICLE_DRIVE_TO_COORD(GET_BATTLEBUDDY_PED(eDriver), vehTemp, vTunnel2, 5, DRIVINGSTYLE_ACCURATE, GET_ENTITY_MODEL(vehTemp), DRIVINGMODE_PLOUGHTHROUGH, 2, 2)
										ELSE
											TASK_VEHICLE_PARK(GET_BATTLEBUDDY_PED(eDriver), vehTemp, vDest, 180, PARK_TYPE_PERPENDICULAR_NOSE_IN, 15)
										ENDIF
									ELSE
										IF bCurrentlyTowingTruck
											TASK_VEHICLE_DRIVE_TO_COORD(GET_BATTLEBUDDY_PED(eDriver), vehTemp, vTunnel1, 5, DRIVINGSTYLE_ACCURATE, GET_ENTITY_MODEL(vehTemp), DRIVINGMODE_PLOUGHTHROUGH, 2, 2)
										ELSE
											TASK_VEHICLE_PARK(GET_BATTLEBUDDY_PED(eDriver), vehTemp, vDest, 0, PARK_TYPE_PERPENDICULAR_NOSE_IN, 15)
										ENDIF
									ENDIF
									iBuddyDriveHomeStage++
								ENDIF
							BREAK
							CASE 4
								//this should sit until the buddy parks
							BREAK
						ENDSWITCH
					ENDIF
				ENDIF
				
				MANAGE_BATTLEBUDDY_DIALOGUE()
				
			BREAK
			
		ENDSWITCH
				
	ELSE
	
		IF buddyState != AHPBB_SCAN_FOR_DRIVING_BUDDIES
			IF RELEASE_BATTLEBUDDY(GET_BATTLEBUDDY_PED(eDriver))
				buddyState = AHPBB_SCAN_FOR_DRIVING_BUDDIES
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

FUNC BOOL CHECK_FOR_NEW_FIRETRUCK_AND_TOW_TRUCK()
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
	AND GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) != vehFiretruck
	AND GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()) != vehTowTruck
		IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), FIRETRUK)
			vehFiretruck = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			SET_ENTITY_AS_MISSION_ENTITY(vehFiretruck, TRUE, TRUE)
			bStatsStarted = FALSE
			RETURN TRUE
		ENDIF
		IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TOWTRUCK)
		OR IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TOWTRUCK2)
			IF DOES_ENTITY_EXIST(GET_ENTITY_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
				IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))))
					vehTowTruck = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					SET_ENTITY_AS_MISSION_ENTITY(vehTowTruck, TRUE, TRUE)
					IF GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO_TOW_TRUCK(vehTowTruck)) != vehFiretruck
						vehFiretruck = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX( GET_ENTITY_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
						bStatsStarted = FALSE
						SET_ENTITY_AS_MISSION_ENTITY(vehFiretruck, TRUE, TRUE)
					ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

INT iGrabNo

FUNC BOOL IS_PED_IN_ARRAY(PED_INDEX ped, PED_INDEX &pedArray[])
	IF DOES_ENTITY_EXIST(ped)
		INT iTemp
		REPEAT COUNT_OF(pedArray) iTemp
			IF DOES_ENTITY_EXIST(pedArray[iTemp])
				IF ped = pedArray[iTemp]
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	RETURN FALSE
ENDFUNC

PROC GRAB_NEARBY_FIREMEN_AND_MAKE_HOSTILE()

	BOOL bGotOne = FALSE
	
	IF iGrabNo < AGENCY_P1_NUMBER_OF_FIREMEN
	
		IF iGrabNo < 4
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[iGrabNo])
				IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[iGrabNo])
					IF VDIST2(GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[iGrabNo]), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 30*30
						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[iGrabNo], TRUE, TRUE)
						pedFiremen[iGrabNo] = g_sTriggerSceneAssets.ped[iGrabNo]
						bGotOne = TRUE
						PRINTLN("Grabbed trigger scene fireman")
						g_sTriggerSceneAssets.ped[iGrabNo] = NULL
					ELSE
						IF VDIST2(GET_ENTITY_COORDS(g_sTriggerSceneAssets.ped[iGrabNo]), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 50*50
						AND IS_ENTITY_OCCLUDED(g_sTriggerSceneAssets.ped[iGrabNo])
							SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[iGrabNo], TRUE, TRUE)
							PRINTLN("Deleted trigger scene fireman")
							DELETE_PED(g_sTriggerSceneAssets.ped[iGrabNo])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bGotOne
		
			PED_INDEX pedTemp
			
			IF GET_CLOSEST_PED(GET_ENTITY_COORDS(PLAYER_PED_ID()), 30, TRUE, TRUE, pedTemp, FALSE, TRUE, PEDTYPE_FIRE)
				IF DOES_ENTITY_EXIST(pedTemp)
				AND NOT IS_PED_INJURED(pedTemp)
				AND NOT IS_PED_IN_ARRAY(pedTemp, pedFiremen)
					SET_ENTITY_AS_MISSION_ENTITY(pedTemp, TRUE, TRUE)
					pedFiremen[iGrabNo] = pedTemp
					PRINTLN("Grabbed gen pop fireman")
					bGotone = TRUE
				ENDIF
			ENDIF
			
		ENDIF
		
		IF bGotOne
		
			IF NOT IS_PED_INJURED(pedFiremen[iGrabNo])
			
				#IF IS_DEBUG_BUILD
				
					TEXT_LABEL_63 sTemp
					sTemp = "fireman "
					sTemp += iGrabNo
					
					SET_PED_NAME_DEBUG(pedFiremen[iGrabNo], sTemp)
					
				#ENDIF
				
				SET_PED_RELATIONSHIP_GROUP_HASH(pedFiremen[iGrabNo], rgFireMen)
				SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 1)
				SET_PED_COMBAT_ATTRIBUTES(pedFiremen[iGrabNo], CA_ALWAYS_FLEE, FALSE)
				SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
				
			ENDIF
			
			iGrabNo++
			
		ENDIF
		
		
	ENDIF
	
ENDPROC

PROC MANAGE_BLIPS(VEHICLE_INDEX veh)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			IF IS_VEHICLE_DRIVEABLE(veh)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh)
					IF DOES_BLIP_EXIST(blipVeh)
						REMOVE_BLIP(blipVeh)
					ENDIF
				ELSE
					IF NOT DOES_BLIP_EXIST(blipVeh)
						blipVeh = CREATE_BLIP_FOR_VEHICLE(veh)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh)
				IF DOES_BLIP_EXIST(blipVeh)
					REMOVE_BLIP(blipVeh)
				ENDIF
				IF NOT DOES_BLIP_EXIST(blipDest)
					CLEAR_PRINTS()
					IF NOT bGoHomePrinted
						PRINT("AHP_DROPOFF", DEFAULT_GOD_TEXT_TIME, 1)
						bGoHomePrinted = TRUE
					ENDIF
					blipDest = CREATE_BLIP_FOR_COORD(vDest, TRUE)
					REPLAY_RECORD_BACK_FOR_TIME(3)
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(blipDest)
					REMOVE_BLIP(blipDest)
				ENDIF
				IF NOT DOES_BLIP_EXIST(blipVeh)
					CLEAR_PRINTS()
					IF NOT bGetInPrinted
						PRINT("AHP_GETBAKIN", DEFAULT_GOD_TEXT_TIME, 1)
						bGetInPrinted = TRUE
					ENDIF
					blipVeh = CREATE_BLIP_FOR_VEHICLE(veh)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(blipVeh)
			REMOVE_BLIP(blipVeh)
		ENDIF
		IF DOES_BLIP_EXIST(blipDest)
			REMOVE_BLIP(blipDest)
		ENDIF
	ENDIF
ENDPROC

SCENARIO_BLOCKING_INDEX sbiFireStation

PROC initialiseMission()

	PRINTLN("aprep1 - Check1")
	
	//check if the player is in a firetruck
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
	AND IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sTriggerSceneAssets.veh[0])
		SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
		vehFiretruck = g_sTriggerSceneAssets.veh[0]
		g_sTriggerSceneAssets.veh[0] = NULL
		PRINTLN("Grabbed firetruck at mission init")
	ENDIF
	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
	AND IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[1])
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sTriggerSceneAssets.veh[1])
		IF DOES_ENTITY_EXIST(GET_ENTITY_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
		AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))))
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[1], TRUE, TRUE)
			vehTowTruck = g_sTriggerSceneAssets.veh[1]
			g_sTriggerSceneAssets.veh[1] = NULL
			PRINTLN("Grabbed towtruck at mission init")
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
			vehFiretruck = g_sTriggerSceneAssets.veh[0]
			g_sTriggerSceneAssets.veh[0] = NULL
			PRINTLN("Grabbed firetruck at mission init attached to towtruck")
			bCurrentlyTowingTruck = TRUE
		ENDIF
	ENDIF
	
	REQUEST_ADDITIONAL_TEXT("AHPREP1", MISSION_TEXT_SLOT)
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		WAIT(0)
	ENDWHILE
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	
	ADD_RELATIONSHIP_GROUP("FireMen", rgFireMen)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, rgFireMen)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFireMen, RELGROUPHASH_PLAYER)
	
	IF IS_REPEAT_PLAY_ACTIVE()
	
		REQUEST_MODEL(S_M_Y_FIREMAN_01)
		WHILE NOT HAS_MODEL_LOADED(S_M_Y_FIREMAN_01)
			WAIT(0)
		ENDWHILE
		
		VECTOR vFiretruckPos = <<202.0504, -1655.7734, 28.8031>>
		sbiFireStation = ADD_SCENARIO_BLOCKING_AREA(vFiretruckPos - <<30,30,10>>, vFiretruckPos + <<30,30,10>>)
		CLEAR_AREA(vFiretruckPos, 20.0, TRUE)
		
		pedFiremen[0] = CREATE_PED(PEDTYPE_FIRE, S_M_Y_FIREMAN_01, <<215.7186, -1644.6219, 28.7719>>, 357.5736)
		pedFiremen[1] = CREATE_PED(PEDTYPE_FIRE, S_M_Y_FIREMAN_01, <<214.4446, -1643.7203, 28.7776>>, 279.1055)
		pedFiremen[2] = CREATE_PED(PEDTYPE_FIRE, S_M_Y_FIREMAN_01, <<217.0826, -1644.1172, 28.7155>>, 72.8262)
		
		INT iTemp
		FOR iTemp = 0 TO COUNT_OF(pedFiremen)-1
			IF NOT IS_PED_INJURED(pedFiremen[iTemp])
				TASK_START_SCENARIO_IN_PLACE(pedFiremen[iTemp], "WORLD_HUMAN_HANG_OUT_STREET")
				SET_PED_COMBAT_ATTRIBUTES(pedFiremen[iTemp], CA_ALWAYS_FLEE, FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(pedFiremen[iTemp], rgFireMen)
			ENDIF
		ENDFOR
		
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, RELGROUPHASH_PLAYER, rgFireMen)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE, rgFireMen, RELGROUPHASH_PLAYER)
		
		#IF IS_DEBUG_BUILD
			PRINTLN(GET_THIS_SCRIPT_NAME(), " - trigger scene created for replay")
		#ENDIF
		
		iPrepProgress = -1
		
		IF IS_REPLAY_IN_PROGRESS()
			START_REPLAY_SETUP(<<354.3055, -1722.2062, 28.2590>>, 109.6154)
			END_REPLAY_SETUP()
		ENDIF
		
	ENDIF
	
	IF NOT IS_SCREEN_FADED_IN()
	AND NOT IS_SCREEN_FADING_IN()	
		DO_SCREEN_FADE_IN(500)
	ENDIF
	
	// check if player went to fire station for the truck
	IF IS_PLAYER_IN_FIRE_STATION()
		bWentToFireStation = TRUE
		CPRINTLN(DEBUG_MISSION, "Player is in fire station")
	ELSE
		CPRINTLN(DEBUG_MISSION, "Player not in fire station")
	ENDIF
	
	IF NOT IS_REPEAT_PLAY_ACTIVE()
		TRIGGER_MUSIC_EVENT("AHP1_START")
	ENDIF
	
	bStatsStarted = FALSE
	INIT_BATTLEBUDDY_BEHAVIOUR_FOR_MISSION(SP_HEIST_AGENCY_PREP_1)
	
ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Cleanup()
	PRINTLN("Agency Heist Prep 1 cleaned up")
	RESET_ALL_BATTLEBUDDY_BEHAVIOUR_REQUESTS()
	IF DOES_ENTITY_EXIST(vehFiretruck)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(vehFiretruck)
	ENDIF
	REMOVE_SCENARIO_BLOCKING_AREA(sbiFireStation)
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Passed()
	PRINTLN("Agency Heist Prep 1 passed")
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	Mission_Flow_Mission_Passed()
	Mission_Cleanup()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    If the player has been in the fire station we set his replay warp pos to nearby
PROC SET_REPLAY_ACCEPTED_WARP_LOCATION()
	IF bWentToFireStation = TRUE
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<354.3055, -1722.2062, 28.2590>>, 109.6154)
	ENDIF
ENDPROC

PROC Mission_Failed(STRING reason)
	PRINTLN("Agency Heist Prep 1 failed")
	
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(reason)
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		WAIT(0)
	ENDWHILE
	
	IF DOES_ENTITY_EXIST(vehFiretruck)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehFiretruck)
			DELETE_VEHICLE(vehFiretruck)
		ENDIF
	ENDIF
	
	INT iTemp
	
	REPEAT COUNT_OF(pedFiremen) iTemp
		IF DOES_ENTITY_EXIST(pedFiremen[iTemp])
			DELETE_PED(pedFiremen[iTemp])
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(g_sTriggerSceneAssets.ped) iTemp
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[iTemp])
			DELETE_PED(g_sTriggerSceneAssets.ped[iTemp])
		ENDIF
	ENDREPEAT
	
	CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100, TRUE, TRUE)
	
	TRIGGER_MUSIC_EVENT("AHP1_FAIL")
	
	// check if we need to respawn the player in a different position, 
	IF HAS_PLAYER_ACCEPTED_REPLAY()
		SET_REPLAY_ACCEPTED_WARP_LOCATION()
	ELSE
		// if he rejects the replay only warp him to just outside station if he failed inside it
		IF IS_PLAYER_IN_FIRE_STATION()
			// failed in fire station, warp outside
			MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<229.1676, -1614.9794, 28.2892>>, 142.5156)
		ENDIF
	ENDIF
	
	MISSION_CLEANUP() // must only take 1 frame and terminate the thread
	
ENDPROC

PROC UPDATE_TOW_STATUS()
	IF bCurrentlyTowingTruck
		IF NOT IS_VEHICLE_DRIVEABLE(vehTowTruck)
		OR (IS_VEHICLE_DRIVEABLE(vehTowTruck) AND NOT IS_ENTITY_ATTACHED_TO_ENTITY(vehFiretruck, vehTowTruck))
		OR (IS_VEHICLE_DRIVEABLE(vehFiretruck) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehFiretruck))
		OR (DOES_ENTITY_EXIST(vehTowTruck) AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehTowTruck)) > 250)
			bCurrentlyTowingTruck = FALSE
		ENDIF
	ELSE
		IF IS_VEHICLE_DRIVEABLE(vehTowTruck)
		AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTowTruck)
		AND IS_ENTITY_ATTACHED_TO_ENTITY(vehFiretruck, vehTowTruck)
			bCurrentlyTowingTruck = TRUE
		ENDIF
	ENDIF
ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

//CAMERA_INDEX camCut

SCRIPT

	PRINTLN("...Agency Heist Prep 1 Launched")
	IF (HAS_FORCE_CLEANUP_OCCURRED())
		SET_REPLAY_ACCEPTED_WARP_LOCATION()
		Mission_Flow_Mission_Force_Cleanup()
		Mission_Cleanup()
	ENDIF
	
	initialiseMission()
	SET_MISSION_FLAG(TRUE)
		
	WHILE (TRUE)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TheAgencyHeistPrep1")
		
		IF NOT bDestCleared
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDest, <<200, 200, 200>>)
			AND NOT IS_SPHERE_VISIBLE(vDest, 50)
				CLEAR_AREA(vDest, 50, TRUE)
				bDestCleared = TRUE
			ENDIF
		ELSE
			IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDest, <<200, 200, 200>>)
				bDestCleared = FALSE
			ENDIF
		ENDIF
		
		IF NOT bEmergecyCallStatGive
			IF g_sTriggerSceneAssets.id = GET_HASH_KEY("AHP1_TRUCKCALLED")
			//OR if the player has called 911 before the text was received
			OR g_bCalled911BeforeText
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(AHP1_TRUCKCALLED)
				g_sTriggerSceneAssets.id = 0
				
				bEmergecyCallStatGive = TRUE
			ENDIF
		ENDIF
		
		UPDATE_TOW_STATUS()
		
		INT iTemp
		FOR iTemp = 0 TO AGENCY_P1_NUMBER_OF_FIREMEN -1
			IF DOES_ENTITY_EXIST(pedFiremen[iTemp])
			AND NOT IS_PED_INJURED(pedFiremen[iTemp])
			AND DOES_ENTITY_EXIST(vehFiretruck)
				IF VDIST2(GET_ENTITY_COORDS(pedFiremen[iTemp]), GET_ENTITY_COORDS(PLAYER_PED_ID())) > 50*50
					SET_PED_KEEP_TASK(pedFiremen[iTemp], TRUE)
					TASK_SMART_FLEE_PED(pedFiremen[iTemp], PLAYER_PED_ID(), 300, 20000, TRUE)
					SET_PED_AS_NO_LONGER_NEEDED(pedFiremen[iTemp])
				ENDIF
			ENDIF
		ENDFOR
		
		IF iGrabNo < AGENCY_P1_NUMBER_OF_FIREMEN
		AND iPrepProgress >= 0
			GRAB_NEARBY_FIREMEN_AND_MAKE_HOSTILE()
		ENDIF
		
		SWITCH iPrepProgress
		
			CASE -1 //for repeat play only
			
				INT iFiremen
				REPEAT COUNT_OF(pedFiremen) iFiremen
					IF DOES_ENTITY_EXIST(pedFiremen[iFiremen])
						IF DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()))
							VEHICLE_INDEX vehTemp
							vehTemp = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())
							IF IS_VEHICLE_DRIVEABLE(vehTemp)
							AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehTemp)) < 30
							AND IS_VEHICLE_MODEL(vehTemp, FIRETRUK)
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, rgFireMen)
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rgFireMen, RELGROUPHASH_PLAYER)
								SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 1)
								SET_WANTED_LEVEL_DIFFICULTY(PLAYER_ID(), 0)
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
				//Trigger the mission if the player gets in any firetruck.
				IF CHECK_FOR_NEW_FIRETRUCK_AND_TOW_TRUCK()
					REPLAY_RECORD_BACK_FOR_TIME(15.0, 15.0, REPLAY_IMPORTANCE_HIGHEST)
					TRIGGER_MUSIC_EVENT("AHP1_START")
					iPrepProgress++
				ENDIF
				
			BREAK
			
			CASE 0
			
				IF NOT DOES_ENTITY_EXIST(vehFiretruck)
				
					PRINTLN("AWAITING GRABBING FIRE TRUCK")
					
					CHECK_FOR_NEW_FIRETRUCK_AND_TOW_TRUCK()
					
					IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
					AND IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
					AND GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()) = FIRETRUK
						vehFiretruck = GET_PLAYERS_LAST_VEHICLE()
						SET_ENTITY_AS_MISSION_ENTITY(vehFiretruck, TRUE, TRUE)
					ENDIF
					
				ELSE
				
					MANAGE_BATTLE_BUDDIES_DRIVING()
					
					IF NOT bStatsStarted
					AND IS_VEHICLE_DRIVEABLE(vehFiretruck)
						INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehFiretruck)
						INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehFiretruck)
						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(AH1P_FACTORY_TIME)
						bStatsStarted = TRUE
					ENDIF
					
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), vDest) < POW(100,2)
						SET_IGNORE_NO_GPS_FLAG(TRUE)
					ENDIF
					
					CHECK_FOR_NEW_FIRETRUCK_AND_TOW_TRUCK()
					
					IF bCurrentlyTowingTruck
					
						IF IS_VEHICLE_DRIVEABLE(vehFiretruck)
						AND IS_VEHICLE_DRIVEABLE(vehTowTruck)
						
							MANAGE_BLIPS(vehTowTruck)
							
							IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehTowTruck)
							AND IS_ENTITY_AT_COORD(vehFiretruck, vDest, g_vAnyMeansLocate, TRUE)
								IF DOES_BLIP_EXIST(blipDest)
									REMOVE_BLIP(blipDest)
								ENDIF
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 15.0, REPLAY_IMPORTANCE_HIGHEST)
								iPrepProgress++
							ENDIF
							
						ENDIF
						
					ELSE
						
						IF IS_VEHICLE_DRIVEABLE(vehFiretruck)
						
							MANAGE_BLIPS(vehFiretruck)
							
							IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehFiretruck)
							AND IS_ENTITY_AT_COORD(vehFiretruck, vDest, g_vAnyMeansLocate, TRUE)
								IF DOES_BLIP_EXIST(blipDest)
									REMOVE_BLIP(blipDest)
								ENDIF
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 15.0, REPLAY_IMPORTANCE_HIGHEST)
								iPrepProgress++
							ENDIF
							
						ENDIF
						
					ENDIF
					
				ENDIF
				
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					iPrepProgress = 100
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(vehFiretruck)
					Mission_Failed("AHP_LOSTTRUCK")
				ELSE
					IF IS_VEHICLE_DRIVEABLE(vehFiretruck)
						IF (VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehFiretruck)) > 150*150 AND IS_ENTITY_OCCLUDED(vehFiretruck))
						OR VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehFiretruck)) > 300*300
							Mission_Failed("AHP_LOSTTRUCK")
						ENDIF
					ELSE
						Mission_Failed("AHP_DEADTRUCK")
					ENDIF
				ENDIF
				
			BREAK
			
			CASE 1
				IF IS_VEHICLE_DRIVEABLE(vehFiretruck)
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehFiretruck)
						IF NOT bGetOut
							PRINT_NOW("AHP_GETOUT", DEFAULT_GOD_TEXT_TIME, 1)
							bGetOut = TRUE
						ENDIF
						TRIGGER_MUSIC_EVENT("AHP1_STOP")
						REPLAY_RECORD_BACK_FOR_TIME(10.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
						iPrepProgress++
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF IS_VEHICLE_DRIVEABLE(vehFiretruck)
					IF NOT IS_ENTITY_AT_COORD(vehFiretruck, vDest, g_vAnyMeansLocate+<<2,2,2>>, TRUE)
						iPrepProgress=0
					ELSE
						IF bCurrentlyTowingTruck
							IF DOES_ENTITY_EXIST(vehTowTruck)
								IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(vehTowTruck, vehFiretruck)
									SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehFiretruck, FALSE)
									IF IS_VEHICLE_SIREN_ON(vehFiretruck)
										SET_VEHICLE_SIREN(vehFiretruck, FALSE)
									ENDIF
									SET_VEHICLE_DOORS_LOCKED(vehFiretruck, VEHICLELOCK_LOCKED)
									CLEAR_PRINTS()
									SETTIMERA(0)
									iPrepProgress++
								ELSE
									DETACH_VEHICLE_FROM_TOW_TRUCK(vehTowTruck, vehFiretruck)
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehFiretruck)
								IF IS_VEHICLE_SIREN_ON(vehFiretruck)
									SET_VEHICLE_SIREN(vehFiretruck, FALSE)
								ENDIF
								SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
								SET_VEHICLE_DOORS_LOCKED(vehFiretruck, VEHICLELOCK_LOCKED)
								CLEAR_PRINTS()
								SETTIMERA(0)
								iPrepProgress++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF IS_VEHICLE_DRIVEABLE(vehFiretruck)
					IF TIMERA() > 3000
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						Mission_Passed()
					ENDIF
				ENDIF
			BREAK
			
			CASE 100
				IF DOES_BLIP_EXIST(blipDest)
					REMOVE_BLIP(blipDest)
				ENDIF
				IF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_FIRETRUCK_GOT_WANTED)
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_FIRETRUCK_GOT_WANTED, TRUE)
				ENDIF
				
				iPrepProgress++
			BREAK
			
			CASE 101
			
				CHECK_FOR_NEW_FIRETRUCK_AND_TOW_TRUCK()
				
				IF bCurrentlyTowingTruck
					MANAGE_BLIPS(vehTowTruck)
				ELSE
					MANAGE_BLIPS(vehFiretruck)
				ENDIF
				
				IF NOT bReportFired
					IF (IS_VEHICLE_DRIVEABLE(vehFiretruck) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehFiretruck))
					OR (IS_VEHICLE_DRIVEABLE(vehTowTruck) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTowTruck))
						IF IS_ENTITY_IN_ZONE(PLAYER_PED_ID(), "DAVIS")
						AND GET_ENTITY_SPEED(PLAYER_PED_ID()) > 5
						AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
							IF GET_RANDOM_BOOL()
								PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_AH_PREP_01", 0.0)
							ELSE
								PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_AH_PREP_02", 0.0)
							ENDIF
							bReportFired = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(vehFiretruck)
					Mission_Failed("AHP_LOSTTRUCK")
				ELSE
					IF IS_VEHICLE_DRIVEABLE(vehFiretruck)
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							CLEAR_PRINTS()
							iPrepProgress = 0
						ELSE
							IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehFiretruck)
								IF NOT bLoseCopsPrinted
									PRINT_NOW("AHP_LOSECOPS", DEFAULT_GOD_TEXT_TIME, 1)
									bLoseCopsPrinted = TRUE
									iLoseCopsPrintTime = GET_GAME_TIMER()
								ENDIF
							ELSE
								IF bLoseCopsPrinted
								AND GET_GAME_TIMER() - iLoseCopsPrintTime < DEFAULT_GOD_TEXT_TIME
								AND IS_MESSAGE_BEING_DISPLAYED()
									CLEAR_PRINTS()
								ENDIF
							ENDIF
						ENDIF
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehFiretruck)) > 150*150
							Mission_Failed("AHP_LOSTTRUCK")
						ENDIF
					ELSE
						Mission_Failed("AHP_DEADTRUCK")
					ENDIF
				ENDIF
				
			BREAK
			
		ENDSWITCH
		
		IF DOES_ENTITY_EXIST(vehFiretruck)
			IF NOT IS_VEHICLE_RESTRICTED(vehFiretruck)
				SET_VEHICLE_AS_RESTRICTED(vehFiretruck, 0)
			ENDIF			
		ENDIF
		
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iPrepProgress), <<0,0.02,0>>)
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
				Mission_Failed("AHP_DEADTRUCK")
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
				Mission_Passed()
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
				PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_AH_PREP_01", 0.0)
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
				PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_AH_PREP_02", 0.0)
			ENDIF
			
		#ENDIF
		
		WAIT(0)
		
	ENDWHILE
	
ENDSCRIPT
