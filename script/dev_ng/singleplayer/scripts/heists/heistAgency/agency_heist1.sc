// bank_fbi_setup1.sc
// Kill the Janitor
// Paul Davies - 29/01/2010

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//____________________________________ INCLUDES __________________________________________

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "script_blips.sch"
USING "script_heist.sch"
USING "select_mission_stage.sch"
USING "player_ped_public.sch"
USING "dialogue_public.sch"
USING "chase_hint_cam.sch"
USING "commands_interiors.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "cellphone_public.sch"
USING "cam_recording_public.sch"
USING "cutscene_public.sch"
USING "building_control_public.sch"
USING "CompletionPercentage_public.sch"
USING "taxi_functions.sch"
USING "clearmissionarea.sch"
USING "mission_stat_public.sch"
USING "locates_public.sch"
USING "Ambient_Common.sch"
USING "net_fps_cam.sch"
USING "buddy_head_track_public.sch"
USING "fake_cellphone_public.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
#ENDIF

//____________________________________ VARIABLES __________________________________________

CONST_FLOAT	FOLLOW_FAIL_DISTANCE			300.0
CONST_FLOAT	BACK_OFF_DISTANCE				7.0
CONST_INT VISIBLE_ANGLE						60
CONST_INT MAX_ROUTE_NODES 					200

ENUM MISSION_STAGE_FLAG
	STAGE_INTRO_CUT = 0,
	STAGE_GO_TO_FIB,
	STAGE_WAIT_FOR_JANITOR,
	STAGE_TAIL_THE_JANITOR,
	STAGE_FOLLOW_JANITOR_ON_FOOT,
	STAGE_JANITOR_CUT,
	STAGE_DRIVE_BACK_TO_SAFEHOUSE,
	STAGE_UBER_RECORD,
	STAGE_RECORD_SET_PIECE,
	STAGE_SWITCH_CUT,
	STAGE_TEST,
	STAGE_RECORD_CAMERA_CARS,
	STAGE_LOSE_COPS,
	STAGE_JANITOR_SPOOKED_ON_FOOT,
	STAGE_PLAYBACK_CAMERA_CARS
ENDENUM

MISSION_STAGE_FLAG mission_stage = STAGE_INTRO_CUT
MISSION_STAGE_FLAG eSavedStage

BOOL bSkip = FALSE

BOOL bBlockFleeCheckForFail = FALSE

BOOL bSpeechOnFootFail = TRUE

structPedsForConversation sSpeech

VECTOR v_janitorFrontDoorCoords = <<-107.54, -9.03, 70.67>>
VECTOR v_OfficeCoords = <<71.4087, -702.7785, 31.2297>>

VECTOR v_parkingSpace = <<-70.5294, -45.0289, 62.1148>>
VECTOR vLicensePlateOffset = <<0.0, -2.3, -0.350>>
VECTOR v_apartment_interior =  << -109.2446, -8.7791, 70 >>
VECTOR v_parking_spot_for_janitor = <<-84.4707, -22.6341, 65.3215>>
VECTOR vPlayersFaceOffset = <<0.0, 0.0, 0.7>>
VECTOR vEndPoint = << 719.09, -983.13, 23.14 >>
VECTOR vCamOffset = << 0.080, 0.070, -0.005 >>//<<0.02, 0.200, 0.654>>
VECTOR vCamRot = <<0.0,-0.051185,-20.336971>>

VECTOR vPlayerStartPosition = <<715.2437, -978.9654, 23.1143>>
FLOAT fPlayerStartHeading	= 99.3712

VECTOR vLesterStartPosition = << 709.4145, -980.8687, 23.1044 >>
FLOAT fLesterStartHeading	= 280.8127

VECTOR vHassleScenePosition = << -78.025, -17.575, 65.318 >>
VECTOR vHassleSceneRotation = << -0.000, 0.3, -18.560 >>
INT iHassleSceneId

VECTOR path1[MAX_ROUTE_NODES]

VECTOR v_Stakeout_area_Pos1 =  <<111.0968, -583.6760, 29.400103>>
VECTOR v_Stakeout_area_Pos2 =  <<69.211121,-698.345825,34.402485>> 
FLOAT f_Stakeout_area_Width =  27.000000

VECTOR vLargerStakeoutAreaPos1 = <<68.085724,-701.327881,30.653330>>
VECTOR vLargerStakeoutAreaPos2 = <<118.560150,-562.427673,38.156513>> 
FLOAT fLargerStakeoutAreaWidth = 27.000000

VECTOR v_janitors_street_Pos1 = <<-66.013786,-59.915276,59.154297>>
VECTOR v_janitors_street_Pos2 = <<-48.974030,10.519518,76.134163>> 
FLOAT f_janitors_street_Width = 60

//starting positions and headings for peds and vehs
VECTOR vPlayerCarCoords = << 710.9962, -979.7601, 23.1001 >>
FLOAT fPlayerCarHeading = 223.3430

FLOAT fZoomReached = 50//44.178452
FLOAT f_office_heading = 340.9176
FLOAT f_park_heading = 167.842
FLOAT f_parking_spot_for_janitor = 69.6488

BOOL b_JanitorHome = FALSE
BOOL b_help_used = FALSE
BOOL b_janitor_spotted = FALSE
BOOL b_key_areas_cleared = FALSE
BOOL b_janitor_rage = FALSE
BOOL bRequestNextStage = FALSE
BOOL bLeftTurn[3]
BOOL bRightTurn
BOOL bFollowAchievment
BOOL bTyresUnpoppable = FALSE

INT	janitor_wait_time
INT current_car
INT iStageProgress
INT iNumberOfDecoys
INT iCurrentStakeoutSpeech = 0
INT colours[8]
INT iJanSeqProg = 0
INT iConvStage

STRING lpJanitor = "83QSL722"
STRING sConvBlock = "FBS1AUD"
STRING sParkWaypointRec = "agencysetup1park2"

MODEL_NAMES car_models[4]
MODEL_NAMES driver_model = A_M_Y_BUSINESS_01
MODEL_NAMES modelJanitorVeh = STANIER

VECTOR vJanLPOffset = <<0, -2.6829, 0.0781>>
VECTOR vLPOffsets[4]

CHASE_HINT_CAM_STRUCT localChaseHintCamStruct

FIRST_PERSON_CAM_STRUCT fpCam

REL_GROUP_HASH neutrals

FAKE_CELLPHONE_DATA sfLestPhoneScreen

VECTOR vUnlockDoorScenePosition = << -107.492, -9.034, 69.524  >>
VECTOR vUnlockDoorSceneRotation = << 0.000, 0.000, -178.000 >>

BOOL bInfrontDone = FALSE
//INT iTimeInfrontDone = 0

// Indeces
BLIP_INDEX blipJanitor
BLIP_INDEX blipOfficeFBI
BLIP_INDEX blipParkingSpace
BLIP_INDEX blipPlayerCar

CAMERA_INDEX zoomCam

PED_INDEX pedMarilyn
PED_INDEX pedLester
PED_INDEX pedJanitor
PED_INDEX pedFranklin


//used for the biggest hack in the world
PED_INDEX pedDummy
VEHICLE_INDEX vehDummy

VEHICLE_INDEX vehJanitor
VEHICLE_INDEX vehPlayerCar
VEHICLE_INDEX vehFranklin
VEHICLE_INDEX carHotGirl

VEHICLE_INDEX CarParkDummies[5]
VEHICLE_INDEX vehDave

INTERIOR_INSTANCE_INDEX apartment

OBJECT_INDEX objLestPhone
OBJECT_INDEX objBottle
OBJECT_INDEX objBag

SCENARIO_BLOCKING_INDEX sbiGarmentFactory
SCENARIO_BLOCKING_INDEX sbiAlley

STRING sCutsceneName = "ah_1_mcs_1"
STRING sIntroName = "ah_1_int"

OBJECT_INDEX objStick, objChair

REL_GROUP_HASH rghBuddy

#IF IS_DEBUG_BUILD

//	FLOAT fCarDOFFarIn = 50
//	FLOAT fCarDOFFarOut = 75 
//	FLOAT fCarDOFNearOut = 5
//	FLOAT fCarDOFNearIn = 6
//	
	VECTOR vWidgetCamOffset = vCamOffset
	
	CONST_INT MAX_SKIP_MENU_LENGTH 7
	INT iStageHolder
	MissionStageMenuTextStruct skipMenuOptions[MAX_SKIP_MENU_LENGTH]
	
	WIDGET_GROUP_ID stick_widget
	
	PROC BUILD_WIDGETS()
		stick_widget = START_WIDGET_GROUP("Agency Setup 1 - DOF test")
//		ADD_WIDGET_FLOAT_SLIDER("Near out", fCarDOFNearOut, 0, 100, 0.1)
//		ADD_WIDGET_FLOAT_SLIDER("Near in", fCarDOFNearIn, 0, 100, 0.1)
//		ADD_WIDGET_FLOAT_SLIDER("Far in", fCarDOFFarIn, 0, 100, 0.1) 
//		ADD_WIDGET_FLOAT_SLIDER("Far out", fCarDOFFarOut, 0, 100, 0.1)
		ADD_WIDGET_VECTOR_SLIDER("Cam offset", vWidgetCamOffset, -2, 2, 0.01)
		STOP_WIDGET_GROUP()
	ENDPROC
	
#ENDIF

//functions to fade in and out safely
PROC FADE_IN(INT iTime = 500)
	IF NOT IS_SCREEN_FADED_IN()
	AND NOT IS_SCREEN_FADING_IN()
		DO_SCREEN_FADE_IN(iTime)
	ENDIF
ENDPROC

PROC FADE_OUT(INT iTime = 500)
	IF NOT IS_SCREEN_FADED_OUT()
	AND NOT IS_SCREEN_FADING_OUT()
		DO_SCREEN_FADE_OUT(iTime)
	ENDIF
ENDPROC

STRUCT TEXT_HOLDER
	STRING label
	BOOL played
ENDSTRUCT

TEXT_HOLDER gGetBackInCar
TEXT_HOLDER gWaitForJanitor
TEXT_HOLDER gFollowJanitor
TEXT_HOLDER gGoToFIB
TEXT_HOLDER gGoHome
TEXT_HOLDER	gGoBackToJan
TEXT_HOLDER gtLoseCops
TEXT_HOLDER gtAbandoned

//TEXT_HOLDER dToFIBChat1
TEXT_HOLDER dTail1
TEXT_HOLDER dTail2
TEXT_HOLDER dToHome1

FUNC TEXT_HOLDER CREATE_TEXT_HOLDER(STRING textlabel)
	TEXT_HOLDER retText
	retText.label = textLabel
	retText.played = FALSE
	RETURN retText
ENDFUNC

PROC INIT_TEXT()

	gGetBackInCar			= CREATE_TEXT_HOLDER("S1_BKINCAR")
	gWaitForJanitor			= CREATE_TEXT_HOLDER("S1_WAIT")
	gFollowJanitor			= CREATE_TEXT_HOLDER("S1_FOLLOW")
	gGoToFIB				= CREATE_TEXT_HOLDER("S1_GETTOBANK")
	gGoHome					= CREATE_TEXT_HOLDER("S1_GOTOSH")
	gGoBackToJan			= CREATE_TEXT_HOLDER("S1_BKTOAPT")
	gtLoseCops				= CREATE_TEXT_HOLDER("S1_LOSECOPS")
	gtAbandoned				= CREATE_TEXT_HOLDER("S1_ABAND")
	
//	dToFIBChat1				= CREATE_TEXT_HOLDER("FBS1_TOFIB")
	dTail1					= CREATE_TEXT_HOLDER("FBS1_TAIL1")
	dTail2					= CREATE_TEXT_HOLDER("FBS1_TAIL2")
	dToHome1				= CREATE_TEXT_HOLDER("FBS1_HOME1")
	
ENDPROC

PROC PRINT_GOD_TEXT(TEXT_HOLDER &text)
	IF NOT text.played
		PRINT_NOW(text.label, DEFAULT_GOD_TEXT_TIME, 1)
		text.played = TRUE
	ENDIF
ENDPROC

PROC PRINT_HELP_TEXT(TEXT_HOLDER &text)
	IF NOT text.played
		CLEAR_HELP()
		PRINT_HELP(text.label)
		text.played = TRUE
	ENDIF
ENDPROC

FUNC BOOL PLAY_DIALOGUE(TEXT_HOLDER &text)
	IF NOT text.played
		IF CREATE_CONVERSATION(sSpeech, "FBS1AUD", text.label, CONV_PRIORITY_HIGH)
			text.played = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

BOOL in_car_conv_started = FALSE
BOOL in_car_conv_on_hold = FALSE
TEXT_LABEL_23 ConvResumeLabel = ""

PROC MANAGE_VEHICLE_CONVERSATION(STRING label, VEHICLE_INDEX veh)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh)
			IF NOT in_car_conv_started
				IF CREATE_CONVERSATION(sSpeech, "FBS1AUD", label, CONV_PRIORITY_HIGH)
					in_car_conv_on_hold = FALSE
					in_car_conv_started = TRUE
				ENDIF
			ENDIF
			IF in_car_conv_on_hold
				IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, "FBS1AUD", label, ConvResumeLabel, CONV_PRIORITY_HIGH)
					in_car_conv_on_hold = FALSE
				ENDIF
			ENDIF
		ENDIF
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh)
			IF NOT in_car_conv_on_hold
				ConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
				KILL_FACE_TO_FACE_CONVERSATION()
				in_car_conv_on_hold = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC INTERP_VEHICLE_TO_POSITION(VEHICLE_INDEX veh, VECTOR vStartPos, VECTOR vFinalPos, FLOAT &fQuatStart[], FLOAT &fQuatEnd[], FLOAT &fInterp)
	SET_ENTITY_COORDS(veh, LERP_VECTOR(vStartPos, vFinalPos, fInterp))
	FLOAT fQuatCurrent[4]
	SLERP_NEAR_QUATERNION(fInterp, fQuatStart[0], fQuatStart[1], fQuatStart[2], fQuatStart[3], fQuatEnd[0], fQuatEnd[1], fQuatEnd[2], fQuatEnd[3], fQuatCurrent[0], fQuatCurrent[1], fQuatCurrent[2], fQuatCurrent[3])
	SET_ENTITY_QUATERNION(veh, fQuatCurrent[0], fQuatCurrent[1], fQuatCurrent[2], fQuatCurrent[3])
ENDPROC

SCENARIO_BLOCKING_INDEX sbiGarage
VECTOR vGarageBlockAreaPos1 = <<62.325294,-662.977234,34.627426>> + <<49.250000,91.500000,5.000000>>
VECTOR vGarageBlockAreaPos2 = <<62.325294,-662.977234,34.627426>> - <<49.250000,91.500000,5.000000>>

SCENARIO_BLOCKING_INDEX sbiApartment
VECTOR vApartmentBlockAreaPos1 = << -81.1729, -94.4853, -160.9970 >>
VECTOR vApartmentBlockAreaPos2 = << -34.0891, 28.3163, 192.1070 >>

SCENARIO_BLOCKING_INDEX sbiDrilling

PROC FULLY_BLOCK_AREA(SCENARIO_BLOCKING_INDEX sBlockID, VECTOR vPos1, VECTOR vPos2)
	SET_PED_PATHS_IN_AREA(vPos1, vPos2, FALSE)
	SET_ROADS_IN_AREA(vPos1, vPos2, FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vPos1, vPos2, FALSE)
	REMOVE_SCENARIO_BLOCKING_AREA(sBlockId)
	sBlockID = ADD_SCENARIO_BLOCKING_AREA(vPos1, vPos2)	
	PRINTLN("ADDED BLOCKING AREA")
ENDPROC

PROC UNBLOCK_AREA(SCENARIO_BLOCKING_INDEX sBlockID, VECTOR vPos1, VECTOR vPos2)
	REMOVE_SCENARIO_BLOCKING_AREA(sBlockID)
	SET_ROADS_BACK_TO_ORIGINAL(vPos1, vPos2)
	SET_PED_PATHS_BACK_TO_ORIGINAL(vPos1, vPos2)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(vPos1, vPos2, TRUE)
	PRINTLN("REMOVED BLOCKING AREA")
ENDPROC

STRUCT JANITOR_CAR
	PED_INDEX ped
	VEHICLE_INDEX veh
ENDSTRUCT

JANITOR_CAR decoy_cars[4]

FUNC BOOL CREATE_DECOY_CAR(JANITOR_CAR &JC, MODEL_NAMES model, MODEL_NAMES driver, INT rec, INT col1=-1, INT col2=-1)
	IF NOT DOES_ENTITY_EXIST(JC.veh)
	OR NOT DOES_ENTITY_EXIST(JC.ped)
		REQUEST_MODEL(model)	
		REQUEST_MODEL(driver)
		REQUEST_VEHICLE_RECORDING(rec, "FBIs1")
		IF HAS_MODEL_LOADED(model)
		AND HAS_MODEL_LOADED(driver)
		AND HAS_VEHICLE_RECORDING_BEEN_LOADED(rec, "FBIs1")
			IF NOT DOES_ENTITY_EXIST(JC.veh)
				JC.veh = CREATE_VEHICLE(model,<<99.8736, -721.1680, 32.1399>>, 342.0303)
				IF col1 <> -1
				AND col2 <> -1
					SET_VEHICLE_COLOURS(JC.veh, col1, col2)
				ENDIF
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(JC.veh)
				JC.ped = CREATE_PED_INSIDE_VEHICLE(JC.veh, PEDTYPE_MISSION, driver)
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(JC.veh)
			AND NOT IS_PED_INJURED(JC.ped)
				START_PLAYBACK_RECORDED_VEHICLE_USING_AI(JC.veh, rec, "FBIs1", 10, DRIVINGMODE_AVOIDCARS | DF_SteerAroundStationaryCars)
			ENDIF
		ENDIF
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC

PROC FORCE_DELETE_DECOY_CARS()
	INT iTemp
	REPEAT COUNT_OF(decoy_cars) iTemp
		IF DOES_ENTITY_EXIST(decoy_cars[iTemp].ped)
			DELETE_PED(decoy_cars[iTemp].ped)
		ENDIF
		IF DOES_ENTITY_EXIST(decoy_cars[iTemp].veh)
			DELETE_VEHICLE(decoy_cars[iTemp].veh)
		ENDIF
	ENDREPEAT
ENDPROC

FUNC BOOL CREATE_JANITOR_IN_CAR(VECTOR coords, FLOAT heading)

	REQUEST_MODEL(modelJanitorVeh)
	IF HAS_MODEL_LOADED(modelJanitorVeh)
		IF NOT DOES_ENTITY_EXIST(vehJanitor)
			vehJanitor = CREATE_VEHICLE(modelJanitorVeh, coords, heading)
			SET_VEHICLE_NUMBER_PLATE_TEXT(vehJanitor, lpJanitor)
			SET_VEHICLE_NAME_DEBUG(vehJanitor, "JANITOR CAR")
			SET_VEHICLE_COLOUR_COMBINATION(vehJanitor, 9)
			SET_VEHICLE_AS_RESTRICTED(vehJanitor, COUNT_OF(CarParkDummies))
			APPLY_FORCE_TO_ENTITY(vehJanitor, APPLY_TYPE_EXTERNAL_FORCE, <<0,0,-1>>, <<0,0,0>>, 0, TRUE, TRUE, FALSE)
			SET_VEHICLE_LOD_MULTIPLIER(vehJanitor, 2)
			SET_VEHICLE_DOORS_LOCKED(vehJanitor, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
		ENDIF
	ENDIF
	
	REQUEST_MODEL(S_M_M_JANITOR)
	IF HAS_MODEL_LOADED(S_M_M_JANITOR)
		IF IS_VEHICLE_DRIVEABLE(vehJanitor)
		AND NOT DOES_ENTITY_EXIST(pedJanitor)
			pedJanitor = CREATE_PED_INSIDE_VEHICLE(vehJanitor, PEDTYPE_MISSION, S_M_M_JANITOR)
			SET_PED_COMPONENT_VARIATION(pedJanitor, PED_COMP_HEAD, 0, 0, 0) //(head)
			SET_PED_COMPONENT_VARIATION(pedJanitor, PED_COMP_TORSO, 0, 0, 0) //(uppr)janitorscar
			SET_PED_COMPONENT_VARIATION(pedJanitor, PED_COMP_LEG, 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(pedJanitor, PED_COMP_HAND, 0, 0, 0) //(hand)
			SET_PED_COMPONENT_VARIATION(pedJanitor, PED_COMP_SPECIAL2, 1, 0, 0) //(task)
			SET_PED_COMBAT_ATTRIBUTES(pedJanitor, CA_ALWAYS_FLEE, TRUE)
//			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedJanitor, TRUE)
			SET_PED_ID_RANGE(pedJanitor, 20.0)
			SET_PED_SEEING_RANGE(pedJanitor, 20.0)
			SET_PED_CAN_BE_TARGETTED(pedJanitor, FALSE)
			SET_PED_NAME_DEBUG(pedJanitor, "JANITOR")
			SET_PED_LOD_MULTIPLIER(pedJanitor, 3)
			ADD_PED_FOR_DIALOGUE(sSpeech, 1, pedJanitor, "AGENCYJANITOR")
			SET_AMBIENT_VOICE_NAME(pedJanitor, "AGENCYJANITOR")
		ENDIF
	ENDIF
	
	RETURN DOES_ENTITY_EXIST(pedJanitor) AND DOES_ENTITY_EXIST(vehJanitor)
	
ENDFUNC

PROC FORCE_DELETE_JANITOR()
	IF DOES_ENTITY_EXIST(pedJanitor)
		DELETE_PED(pedJanitor)
	ENDIF
	IF DOES_ENTITY_EXIST(vehJanitor)
		DELETE_VEHICLE(vehJanitor)
	ENDIF
ENDPROC

PROC SET_JANITORS_DOOR_LOCKED(BOOL bLock = TRUE)
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_JANITORS_APARTMENT))
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_JANITORS_APARTMENT), 0.0, FALSE, FALSE)
		IF bLock
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_JANITORS_APARTMENT), DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_JANITORS_APARTMENT), DOORSTATE_LOCKED, FALSE, FALSE)
			#IF IS_DEBUG_BUILD PRINTLN("Janitors door debug - locked") #ENDIF
		ELSE
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_JANITORS_APARTMENT), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, FALSE)
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_JANITORS_APARTMENT), DOORSTATE_UNLOCKED, FALSE, FALSE)
			#IF IS_DEBUG_BUILD PRINTLN("Janitors door debug - unlocked") #ENDIF
		ENDIF
   	ELSE
		PRINTLN("Janitors door debug - DOOR NOT REGISTERED")
	ENDIF
ENDPROC

PROC CHECK_PEDS_FOR_DIALOGUE()

	//jaintor
	IF NOT DOES_ENTITY_EXIST(sSpeech.PedInfo[1].Index)
	AND DOES_ENTITY_EXIST(pedJanitor)
		IF NOT IS_PED_INJURED(pedJanitor)
			ADD_PED_FOR_DIALOGUE(sSpeech, 1, pedJanitor, "AGENCYJANITOR")
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(sSpeech.PedInfo[1].Index)
		AND NOT DOES_ENTITY_EXIST(pedJanitor)
			REMOVE_PED_FOR_DIALOGUE(sSpeech, 1)
		ENDIF
	ENDIF
	
	//Lester
	IF NOT DOES_ENTITY_EXIST(sSpeech.PedInfo[2].Index)
	AND DOES_ENTITY_EXIST(pedLester)
		IF NOT IS_PED_INJURED(pedLester)
			ADD_PED_FOR_DIALOGUE(sSpeech, 2, pedLester, "LESTER")
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(sSpeech.PedInfo[2].Index)
		AND NOT DOES_ENTITY_EXIST(pedLester)
			REMOVE_PED_FOR_DIALOGUE(sSpeech, 2)
		ENDIF
	ENDIF
		
ENDPROC

FUNC BOOL IS_PED_IN_JANITORS_APARTMENT(PED_INDEX ped)
	IF DOES_ENTITY_EXIST(ped)
	AND NOT IS_PED_INJURED(ped)
	AND IS_ENTITY_IN_ANGLED_AREA(ped, <<-112.686089,-14.275766,69.539253>>, <<-109.549889,-5.690081,72.782700>>, 6.000000)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


//____________________________________ FUNCTIONS __________________________________________

//Cleanup
PROC MISSION_CLEANUP()
	KILL_ANY_CONVERSATION()
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	IF IS_VALID_INTERIOR(apartment)
		UNPIN_INTERIOR(apartment)
	ENDIF
	REMOVE_WAYPOINT_RECORDING("janitor_route")
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
	ENDIF
	ASSISTED_MOVEMENT_REMOVE_ROUTE("Aprtmnt_1")
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	DISABLE_TAXI_HAILING(FALSE)
	UNBLOCK_AREA(sbiGarage, vGarageBlockAreaPos1, vGarageBlockAreaPos2)
	UNBLOCK_AREA(sbiApartment, vApartmentBlockAreaPos1, vApartmentBlockAreaPos2)
	REMOVE_SCENARIO_BLOCKING_AREA(sbiDrilling)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-83.2189, -23.8057, 65.3210>> + <<5,5,3>>, <<-83.2189, -23.8057, 65.3210>> - <<5,5,3>>, TRUE)
	
	IF IS_RADAR_HIDDEN()
		DISPLAY_RADAR(TRUE)
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		SET_VEHICLE_TYRES_CAN_BURST(vehPlayerCar, TRUE)
	ENDIF
	
	REMOVE_MODEL_HIDE(<<-107.5401, -9.0258, 70.6696>>, 1.0, V_ILEV_JANITOR_FRONTDOOR)
	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_JANITORS_APARTMENT), 0, FALSE, TRUE)
	
	RELEASE_SCRIPT_AUDIO_BANK()
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(modelJanitorVeh, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(INTRUDER, FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(S_M_M_JANITOR, FALSE)
	
	IF DOES_ENTITY_EXIST(objLestPhone)
		DELETE_OBJECT(objLestPhone)
	ENDIF
	RELEASE_FAKE_CELLPHONE_MOVIE(sfLestPhoneScreen)
	RELEASE_NPC_PHONE_RENDERTARGET()
	
	IF DOES_ENTITY_EXIST(pedFranklin)
		DELETE_PED(pedFranklin)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedDummy)
		DELETE_PED(pedDummy)
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehDummy)
		DELETE_VEHICLE(vehDummy)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF DOES_WIDGET_GROUP_EXIST(stick_widget)
			DELETE_WIDGET_GROUP(stick_widget)
		ENDIF
	#ENDIF
	 
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	
	DELETE_TEXT_MESSAGE_BY_LABEL_FROM_CURRENT_PLAYER("S1_LPTEXT")
	
	REMOVE_SCENARIO_BLOCKING_AREA(sbiGarmentFactory)
	REMOVE_SCENARIO_BLOCKING_AREA(sbiAlley)
	REMOVE_SCENARIO_BLOCKING_AREA(sbiDrilling)
	
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)
	INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL)
	
	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_R), 0)
	SET_DOOR_STATE(DOORNAME_SWEATSHOP_R, DOORSTATE_UNLOCKED)
	DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_L), 0)
	SET_DOOR_STATE(DOORNAME_SWEATSHOP_L, DOORSTATE_UNLOCKED)
	
	TERMINATE_THIS_THREAD()
ENDPROC

PROC Mission_Passed()
	//Make sure the first few items are displaying on the planning 
	//board as we pass the mission. -BenR
	SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_AGENCY, PBDG_0, TRUE)
	
	Mission_Flow_Mission_Passed()
	Mission_Cleanup()
ENDPROC

//Mission Failed
PROC Mission_Failed(STRING reason)

	TRIGGER_MUSIC_EVENT("AH1_FAIL")

	IF IS_PLAYER_PLAYING(PLAYER_ID())
		IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
			IF DOES_CAM_EXIST(fpCam.theCam)
			AND IS_CAM_RENDERING(fpCam.theCam)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			IF DOES_CAM_EXIST(zoomCam)
			AND IS_CAM_RENDERING(zoomCam)
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(reason)
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off).
		PRINTLN("GET_MISSION_FLOW_SAFE_TO_CLEANUP() returned FALSE")
		WAIT(0)
	ENDWHILE
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		IF IS_PED_IN_JANITORS_APARTMENT(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -105.0312, -8.6584, 69.5201 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 259.2958)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_JANITORS_APARTMENT), 0)
			DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_JANITORS_APARTMENT), DOORSTATE_LOCKED)
		ENDIF
	ENDIF
	
	IF ARE_STRINGS_EQUAL(reason, "S1_JSPOOK")
	AND DOES_ENTITY_EXIST(pedJanitor)
	AND (IS_PED_INJURED(pedJanitor) OR IS_ENTITY_DEAD(pedJanitor))
		reason = "S1_FAILED_K"
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedLester)
		DELETE_PED(pedLester)
	ENDIF
	IF DOES_ENTITY_EXIST(pedMarilyn)
		DELETE_PED(pedMarilyn)
	ENDIF
	IF DOES_ENTITY_EXIST(pedFranklin)
		DELETE_PED(pedFranklin)
	ENDIF	
	IF DOES_ENTITY_EXIST(pedJanitor)
		DELETE_PED(pedJanitor)
	ENDIF
	IF DOES_ENTITY_EXIST(vehDave)
		DELETE_VEHICLE(vehDave)
	ENDIF
	
	//Make sure the first few items aren't displaying on the planning 
	//board as we fail the mission. -BenR
	SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_AGENCY, PBDG_0, FALSE)
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	
	Mission_Cleanup() // must only take 1 frame and terminate the thread
	
ENDPROC

FUNC FLOAT GET_VECTOR_MAGNITUDE(VECTOR vVector)

	FLOAT magnitude
	
	magnitude = SQRT((vVector.x * vVector.x) + (vVector.y * vVector.y) + (vVector.z * vVector.z))
	
	RETURN magnitude
	
ENDFUNC

FUNC VECTOR GET_NORMALISED_VERSION_OF_VECTOR(VECTOR vVector)

	FLOAT magnitude = GET_VECTOR_MAGNITUDE(vVector)
	
	IF vVector.x <> 0
		vVector.x = (vVector.x / magnitude)
	ELSE
		vVector.x = 0
	ENDIF
	
	IF vVector.y <> 0
		vVector.y = (vVector.y / magnitude)
	ELSE
		vVector.y = 0
	ENDIF
	
	IF vVector.z <> 0
		vVector.z = (vVector.z /magnitude)
	ELSE
		vVector.z = 0
	ENDIF
	
	RETURN vVector
	
ENDFUNC

FUNC FLOAT GET_ANGLE_BETWEEN_TWO_VECTORS(VECTOR vVectorA, VECTOR vVectorB)

	FLOAT return_angle
	
	vVectorA = GET_NORMALISED_VERSION_OF_VECTOR(vVectorA)
	vVectorB = GET_NORMALISED_VERSION_OF_VECTOR(vVectorB)
	
	//atan2(v2.y,v2.x) - atan2(v1.y,v1.x)
	return_angle = (ATAN2(vVectorB.y, vVectorB.x) - ATAN2(vVectorA.y, vVectorA.x))
	
	IF return_angle > 180
		return_angle = (return_angle - 360)
	ELIF return_angle < -180
		return_angle = (return_angle + 360)
	ENDIF
	
	RETURN return_angle
	
ENDFUNC

PROC SET_VECTOR_MAGNITUDE(VECTOR &vVector, FLOAT fMag)
	vVector = GET_NORMALISED_VERSION_OF_VECTOR(vVector)
	vVector.x = vVector.x * fMag
	vVector.y = vVector.y * fMag
	vVector.z = vVector.z * fMag
ENDPROC

FUNC VECTOR GET_FOCUS_CAMERA_POSITION(VECTOR H, VECTOR V)

	VECTOR R
	VECTOR shift
	
	shift = V - H
	
	SET_VECTOR_MAGNITUDE(shift, 0.3)
	
	R = H + shift
	
	RETURN R
	
ENDFUNC

PROC BLIP_MANAGER_FOR_ENTITY(VEHICLE_INDEX &veh, BLIP_INDEX &vehBlip, ENTITY_INDEX target, BLIP_INDEX &entBlip)

	IF IS_VEHICLE_DRIVEABLE(veh)
	AND DOES_ENTITY_EXIST(veh)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh)
			IF NOT DOES_BLIP_EXIST(vehBlip)
				CLEAR_PRINTS()
				PRINT_GOD_TEXT(gGetBackInCar)
				vehBlip = CREATE_BLIP_FOR_VEHICLE(veh)
			ENDIF
			IF DOES_BLIP_EXIST(entBlip)
				REMOVE_BLIP(entBlip)
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(entBlip)
				IF IS_ENTITY_A_VEHICLE(target)
					entBlip = CREATE_BLIP_FOR_VEHICLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(target))
				ELIF IS_ENTITY_A_PED(target)
					entBlip = CREATE_BLIP_FOR_PED(GET_PED_INDEX_FROM_ENTITY_INDEX(target))
				ENDIF
			ENDIF
			IF DOES_BLIP_EXIST(vehBlip)
				CLEAR_PRINTS()
				REMOVE_BLIP(vehBlip)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC BLIP_MANAGER_FOR_COORD(VEHICLE_INDEX &veh, BLIP_INDEX &vehBlip, VECTOR coord, BLIP_INDEX &coordBlip)
	IF IS_VEHICLE_DRIVEABLE(veh)
	AND DOES_ENTITY_EXIST(veh)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh)
			IF NOT DOES_BLIP_EXIST(vehBlip)
				CLEAR_PRINTS()
				PRINT_GOD_TEXT(gGetBackInCar)
				vehBlip = CREATE_BLIP_FOR_VEHICLE(veh)
			ENDIF
			IF DOES_BLIP_EXIST(coordBlip)
				REMOVE_BLIP(coordBlip)
			ENDIF
		ELSE
			IF NOT DOES_BLIP_EXIST(coordBlip)
				coordBlip = CREATE_BLIP_FOR_COORD(coord, TRUE)
			ENDIF
			IF DOES_BLIP_EXIST(vehBlip)
				CLEAR_PRINTS()
				REMOVE_BLIP(vehBlip)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_LOOKING_AT_LICENSE_PLATE(VEHICLE_INDEX thisCar, BOOL draw_lines = FALSE, BOOL print_info = FALSE)

	IF IS_VEHICLE_DRIVEABLE(thisCar)
	
		//position in world coords of the current car's license plate
		VECTOR v_license_plate_position = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisCar, vLicensePlateOffset)
		//position of players head
		VECTOR v_player_head_position = GET_FOCUS_CAMERA_POSITION( GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), vPlayersFaceOffset), v_license_plate_position)
		//position in world coords of a point directly perpendicular to the current car's license plate
		VECTOR v_license_plate_normal_position = v_license_plate_position - GET_ENTITY_FORWARD_VECTOR(thisCar)
		//vector between camera position and license plate with world rotation
		VECTOR v_license_plate_to_head = v_player_head_position - v_license_plate_position
		//vector between the license plate and the point just in front of license plate
		VECTOR v_license_plate_to_vehicle_forward_heading = v_license_plate_normal_position - v_license_plate_position
		
		FLOAT ftemp = GET_ANGLE_BETWEEN_TWO_VECTORS(v_license_plate_to_vehicle_forward_heading, v_license_plate_to_head)
		
		IF print_info
			#IF IS_DEBUG_BUILD
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("Angle between car and player ")
				SAVE_FLOAT_TO_DEBUG_FILE(ftemp)
				SAVE_NEWLINE_TO_DEBUG_FILE()
			#ENDIF
		ENDIF
		
		IF draw_lines
			#IF IS_DEBUG_BUILD
				VECTOR v_2D_normalised_license_to_player_position
				TEXT_LABEL_63 temp
				v_2D_normalised_license_to_player_position = v_license_plate_position + GET_NORMALISED_VERSION_OF_VECTOR(v_license_plate_to_head)
				v_2D_normalised_license_to_player_position.z = v_license_plate_position.z
				
				DRAW_DEBUG_LINE(v_player_head_position, v_license_plate_position)
				
				temp = ROUND(ftemp)
				DRAW_DEBUG_TEXT(temp, v_2D_normalised_license_to_player_position)
				
				temp = ("0")
				DRAW_DEBUG_TEXT(temp, v_license_plate_normal_position)
				
				IF ABSF(fTemp) < VISIBLE_ANGLE
					DRAW_DEBUG_LINE(v_license_plate_normal_position, v_license_plate_position)
					DRAW_DEBUG_LINE(v_2D_normalised_license_to_player_position, v_license_plate_position)
				ELSE
					DRAW_DEBUG_LINE(v_license_plate_normal_position, v_license_plate_position, 255, 0, 0)
					DRAW_DEBUG_LINE(v_2D_normalised_license_to_player_position, v_license_plate_position, 255, 0, 0)
				ENDIF
			#ENDIF
		ENDIF
		
		IF ABSF(fTemp) < VISIBLE_ANGLE
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC


VECTOR vJanitorRoute[8]
INT iTargetNode = -1

PROC INIT_JANITOR_ROUTE()
	vJanitorRoute[0] = <<103.17112, -599.22192, 30.64482>>
	vJanitorRoute[1] = <<69.96421, -552.62140, 31.93536>>
	vJanitorRoute[2] = <<-15.68856, -544.88452, 38.05373>>
	vJanitorRoute[3] = <<-39.99245, -493.27765, 39.46600>>
	vJanitorRoute[4] = <<114.25062, -65.17094, 65.38992>>
	vJanitorRoute[5] = <<82.67328, -8.04467, 67.46969>>
	vJanitorRoute[6] = <<-10.82436, 26.30634, 70.45963>>
	vJanitorRoute[7] = <<-78.5120, -89.0248, 56.8283>>
	iTargetNode = -1
ENDPROC

FUNC INT GET_CLOSEST_PATH_SECTION(VECTOR point, VECTOR &path[])
	INT iTemp
	INT iClosest
	FLOAT closest_distance = 99999
	REPEAT COUNT_OF(path)-1 iTemp
		IF GET_DISTANCE_BETWEEN_COORDS(GET_CLOSEST_POINT_ON_LINE(point, path[iTemp], path[iTemp+1]), point) < closest_distance
			iClosest = itemp
			closest_distance = VDIST(GET_CLOSEST_POINT_ON_LINE(point, path[iTemp], path[iTemp+1]), point)
		ENDIF
	ENDREPEAT
	RETURN iClosest
ENDFUNC

//BOOL bParked = FALSE
INT iParkStage = 0

PROC DO_JANITOR_ROUTE_HOME()

	IF NOT IS_PED_INJURED(pedJanitor)
	AND IS_VEHICLE_DRIVEABLE(vehJanitor)
		IF NOT IS_ENTITY_IN_ANGLED_AREA(vehJanitor, v_janitors_street_Pos1, v_janitors_street_Pos2, f_janitors_street_Width)
			IF GET_SCRIPT_TASK_STATUS(pedJanitor, SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) != PERFORMING_TASK
				INT iNextNode = GET_CLOSEST_PATH_SECTION(GET_ENTITY_COORDS(pedJanitor, FALSE), vJanitorRoute) + 1
				#IF IS_DEBUG_BUILD
					PRINTLN("Janitor drive debug - Next node is ", iNextNode)
				#ENDIF
				IF iNextNode >= 0 AND iNextNode < COUNT_OF(vJanitorRoute)
					IF VDIST2(GET_ENTITY_COORDS(vehJanitor, FALSE), vJanitorRoute[iNextNode]) > 25*25
					OR iNextNode = 7
						#IF IS_DEBUG_BUILD
							PRINTLN("Janitor drive debug - Driving to next node.")
						#ENDIF
						iTargetNode = iNextNode
					ELSE
						IF iNextNode < (COUNT_OF(vJanitorRoute)-1)
							#IF IS_DEBUG_BUILD
								PRINTLN("Janitor drive debug - Driving to node after next.")
							#ENDIF
							iTargetNode = iNextNode+1
						ENDIF
					ENDIF
					IF iTargetNode >= 0
						iParkStage = 0
						IF iTargetNode < 2
							TASK_VEHICLE_DRIVE_TO_COORD(pedJanitor, vehJanitor, vJanitorRoute[iTargetNode], 15, DRIVINGSTYLE_NORMAL, STANIER, DRIVINGMODE_STOPFORCARS_IGNORELIGHTS, 3, 0)
						ELSE
							TASK_VEHICLE_DRIVE_TO_COORD(pedJanitor, vehJanitor, vJanitorRoute[iTargetNode], 15, DRIVINGSTYLE_NORMAL, STANIER, DRIVINGMODE_STOPFORCARS | DF_StopAtLights, 3, 0)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF iTargetNode >= 0 AND iTargetNode < COUNT_OF(vJanitorRoute)
					IF VDIST2(GET_ENTITY_COORDS(vehJanitor, FALSE), vJanitorRoute[iTargetNode]) < 20*20
						PRINTLN("Clearing drive task")
						CLEAR_PED_TASKS(pedJanitor)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			SWITCH iParkStage
				CASE 0
					REQUEST_WAYPOINT_RECORDING(sParkWaypointRec)
					IF GET_IS_WAYPOINT_RECORDING_LOADED(sParkWaypointRec)
						TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(pedJanitor, vehJanitor, sParkWaypointRec, DRIVINGMODE_AVOIDCARS, 0, EWAYPOINT_START_FROM_CLOSEST_POINT, -1, 10)
						iParkStage++
					ENDIF
//					SEQUENCE_INDEX seqPark
//					OPEN_SEQUENCE_TASK(seqPark)
//					TASK_VEHICLE_MISSION_COORS_TARGET(NULL, vehJanitor, <<-62.8874, -22.0113, 66.5737>>, MISSION_GOTO, 5, DRIVINGMODE_STOPFORCARS | DF_StopForPeds, 2, 5, TRUE)
//					TASK_VEHICLE_MISSION_COORS_TARGET(NULL, vehJanitor, <<-76.1369, -25.8275, 65.1061>>, MISSION_GOTO, 5, DRIVINGMODE_STOPFORCARS | DF_StopForPeds, 2, 2, TRUE)
//					TASK_VEHICLE_PARK(NULL, vehJanitor, v_parking_spot_for_janitor, f_parking_spot_for_janitor, PARK_TYPE_PERPENDICULAR_NOSE_IN, 100)
//					CLOSE_SEQUENCE_TASK(seqPark)
//					TASK_PERFORM_SEQUENCE(pedJanitor, seqPark)
//					iParkStage++
				BREAK
				CASE 1
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJanitor)
						iParkStage++
					ENDIF
//					IF GET_SCRIPT_TASK_STATUS(pedJanitor, SCRIPT_TASK_PERFORM_SEQUENCE) = FINISHED_TASK
//						iParkStage++
//					ENDIF
				BREAK
				CASE 2
					IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJanitor)
						iParkStage++
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		#IF IS_DEBUG_BUILD
			INT iDrawDebugLines
			REPEAT COUNT_OF(vJanitorRoute) iDrawDebugLines
				IF iDrawDebugLines < COUNT_OF(vJanitorRoute) - 1
					IF iDrawDebugLines = GET_CLOSEST_PATH_SECTION(GET_ENTITY_COORDS(vehJanitor, FALSE), vJanitorRoute)
						DRAW_DEBUG_LINE(vJanitorRoute[iDrawDebugLines] + <<0,0,1>>, vJanitorRoute[iDrawDebugLines+1] + <<0,0,1>>, 255,0,0)
					ELSE
						DRAW_DEBUG_LINE(vJanitorRoute[iDrawDebugLines] + <<0,0,1>>, vJanitorRoute[iDrawDebugLines+1] + <<0,0,1>>, 0,255,0)
					ENDIF
				ENDIF
			ENDREPEAT
		#ENDIF
	ENDIF
ENDPROC

FUNC FLOAT GET_PROGRESS_ALONG_ROUTE_HOME(VECTOR vPos)
	INT iNearNode = GET_CLOSEST_PATH_SECTION(vPos, vJanitorRoute)
	IF iNearNode >= 0 AND iNearNode < COUNT_OF(vJanitorRoute)
		FLOAT fProg = TO_FLOAT(iNearNode)
		IF iNearNode < (COUNT_OF(vJanitorRoute)-1)
			fProg += GET_RATIO_OF_CLOSEST_POINT_ON_LINE(vPos, vJanitorRoute[iNearNode], vJanitorRoute[iNearNode+1])
		ENDIF
		RETURN fProg
	ENDIF
	RETURN -1.0
ENDFUNC

///// PURPOSE: Given a position this will return the time along a recording of the closest position to the recording from the input
  ///    /
  /// PARAMS: 
  ///    position - position passed for query
  ///    FileNumber - file number of the recording
  ///    pRecordingName - name of the recording
  ///    recording_path - An array of vectors forming the recording nodes
  /// RETURNS: Float representing time along recoridng
  ///    NOTE: the recording must already be loaded and the recording path built

FUNC FLOAT GET_TIME_IN_RECORDING_FROM_POSITION(VECTOR position, INT FileNumber, STRING pRecordingName, VECTOR &recording_path[])
	FLOAT segment_time
	INT segments_passed
	FLOAT current_segment_distance
	VECTOR closest_point_on_segment
	FLOAT current_segment_progress_distance
	FLOAT current_segment_progress_in_time_step
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(FileNumber, pRecordingName)
		segment_time = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(FileNumber, pRecordingName)/MAX_ROUTE_NODES
		segments_passed = GET_CLOSEST_PATH_SECTION(position, recording_path)
		current_segment_distance = GET_DISTANCE_BETWEEN_COORDS(recording_path[segments_passed], recording_path[segments_passed+1])
		closest_point_on_segment = GET_CLOSEST_POINT_ON_LINE(position, recording_path[segments_passed], recording_path[segments_passed+1])
		current_segment_progress_distance = GET_DISTANCE_BETWEEN_COORDS(recording_path[segments_passed], closest_point_on_segment)		
		current_segment_progress_in_time_step = (current_segment_progress_distance/current_segment_distance)*segment_time
	ENDIF
	RETURN current_segment_progress_in_time_step + (segments_passed*segment_time)
ENDFUNC

PROC BUILD_UBER_ROUTE_DEBUG_PATH(VECTOR &path[], INT FileNumber, STRING pRecordingName)
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(FileNumber, pRecordingName)
		FLOAT duration = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(FileNumber, pRecordingName)
		FLOAT route_segment = duration/(MAX_ROUTE_NODES-1)
		INT iTemp
		path[MAX_ROUTE_NODES-1] = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(FileNumber, duration, pRecordingName)
		REPEAT COUNT_OF(path) iTemp
			path[iTemp] = GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(FileNumber, route_segment*iTemp, pRecordingName)
		ENDREPEAT
	ENDIF
ENDPROC

PROC INIT_STAKEOUT_CAM(CAMERA_INDEX &cam, BOOL start_active)
	IF NOT DOES_CAM_EXIST(cam)
		cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
		SET_CAM_ACTIVE(cam, start_active)
		SHAKE_CAM(cam, "HAND_SHAKE", 0.15)
	ENDIF
	STOP_CAM_POINTING(cam)
	IF IS_VEHICLE_DRIVEABLE(vehDummy)
	AND NOT IS_PED_INJURED(pedDummy)
		SET_CAM_ROT(cam, vCamRot)
		SET_CAM_COORD(cam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_PED_BONE_COORDS(pedDummy, BONETAG_HEAD, <<0.05,0,0>>), 339.9642, vCamOffset))
		IF cam = fpCam.theCam
			fpCam.vInitCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_PED_BONE_COORDS(pedDummy, BONETAG_HEAD, <<0.05,0,0>>), 339.9642, vCamOffset)
		ENDIF
		
		FLOAT fNearClip = 0.2
		IF (IS_PC_VERSION() AND GET_ASPECT_RATIO(TRUE) > 1.8)
			// Reduce near clip for triple head since camera is clipping car window
			fNearClip = 0.075
		ENDIF
		SET_CAM_NEAR_CLIP(cam, fNearClip)
		SET_CAM_FAR_CLIP(cam, 600)
		//SET_CAM_DOF_PLANES(cam, fCarDOFNearOut, fCarDOFNearIn, fCarDOFFarIn, fCarDOFFarOut)
		SET_CAM_USE_SHALLOW_DOF_MODE(cam, TRUE)
		SET_CAM_IS_INSIDE_VEHICLE(cam, TRUE)
	ENDIF
	SET_CAM_FOV(cam, 50)//44.178452)
	IF start_active
		IF NOT IS_CAM_ACTIVE(cam)
			SET_CAM_ACTIVE(cam, TRUE)
		ENDIF
//		RENDER_SCRIPT_CAMS(TRUE, TRUE, 3000)
//		SET_CAM_ACTIVE_WITH_INTERP(cam, GET_RENDERING_CAM(), 1500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_SIN_ACCEL_DECEL)
	ENDIF
ENDPROC

PROC DESTROY_STAKEOUT_CAM(CAMERA_INDEX &cam, BOOL bInterp = FALSE)
	IF DOES_CAM_EXIST(cam)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		RENDER_SCRIPT_CAMS(FALSE, bInterp)
		SET_CAM_ACTIVE(cam, FALSE)
		DESTROY_CAM(cam)
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
	PROC RUN_WIDGETS()
		IF DOES_CAM_EXIST(fpCam.theCam)
		AND IS_CAM_RENDERING(fpCam.theCam)
			IF NOT ARE_VECTORS_EQUAL(vWidgetCamOffset, vCamOffset)
				vCamOffset = vWidgetCamOffset
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				DESTROY_CAM(fpCam.theCam)
				INIT_STAKEOUT_CAM(fpCam.theCam, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				PRINTLN("Changing camera offset planes")
			ENDIF
		ENDIF
	ENDPROC
#ENDIF

PROC TRANSITION_BACK_TO_GAME()
	IF DOES_CAM_EXIST(zoomCam)
		SET_CAM_ACTIVE(zoomCam, FALSE)
		DESTROY_CAM(zoomCam)
	ENDIF
	IF DOES_CAM_EXIST(fpCam.theCam)
		SET_CAM_ACTIVE(fpCam.theCam, FALSE)
		DESTROY_CAM(fpCam.theCam)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	ENDIF
	IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	ENDIF
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
ENDPROC
//
//PROC ACTIVATE_ZOOM_CAM(BOOL bActivate)
//
//	IF bActivate
//		VECTOR tempRot = GET_CAM_ROT(fpCam.theCam)
//		SET_CAM_ROT(zoomCam, tempRot)
//		SET_CAM_ACTIVE(zoomCam, TRUE)
//		SET_CAM_ACTIVE(fpCam.theCam, FALSE)
//		SET_CAM_ROT(fpCam.theCam, vCamRot)
//		SET_CAM_FOV(zoomCam, fZoomReached)
//		SHAKE_CAM(zoomCam, "HAND_SHAKE", 0.15)
//	ELSE
//		IF DOES_CAM_EXIST(fpCam.theCam)
//			SET_CAM_ACTIVE(fpCam.theCam, TRUE)
//			SET_CAM_ROT(fpCam.theCam, vCamRot)
//			fpCam.vInitCamRot = vCamRot
//			SET_CAM_ROT(zoomCam, vCamRot)
//			SET_CAM_FOV(zoomCam, fZoomReached)
//			SHAKE_CAM(fpCam.theCam, "HAND_SHAKE", 0.15)
//		ELSE
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//		ENDIF
//		SET_CAM_ACTIVE(zoomCam, FALSE)
//	ENDIF
//	
//ENDPROC

CHASE_HINT_CAM_STRUCT focuscam
//OBJECT_INDEX objLookAt

//FUNC BOOL UPDATE_LOOK_AT_LICENSE_PLATE(VEHICLE_INDEX veh)
//	REQUEST_MODEL(PROP_LD_TEST_01)
//	IF HAS_MODEL_LOADED(PROP_LD_TEST_01)
//		IF DOES_ENTITY_EXIST(veh)
//		AND IS_VEHICLE_DRIVEABLE(veh)
//			IF VDIST2(v_OfficeCoords, GET_ENTITY_COORDS(veh)) < POW(124.36,2)
//			AND IS_ENTITY_IN_ANGLED_AREA(veh, vLargerStakeoutAreaPos1, vLargerStakeoutAreaPos2, fLargerStakeoutAreaWidth)
//			AND SHOULD_CONTROL_CHASE_HINT_CAM(focuscam, FALSE)
//				IF NOT DOES_ENTITY_EXIST(objLookAt)
//					objLookAt = CREATE_OBJECT(PROP_LD_TEST_01, GET_ENTITY_COORDS(veh))
//				ENDIF
//				SET_ENTITY_COORDS(objLookAt, GET_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(veh) + <<0,0,1>>, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, vLicensePlateOffset)))
//				IF NOT DOES_CAM_EXIST(zoomCam)
//				AND IS_CAM_ACTIVE(fpCam.theCam)
//				AND IS_CAM_RENDERING(fpCam.theCam)
//					zoomCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, FALSE)
//					SET_CAM_ROT(zoomCam, GET_CAM_ROT(fpCam.theCam))
//					POINT_CAM_AT_ENTITY(zoomCam, objLookAt, <<0,0,0>>)
//					SET_CAM_FOV(zoomCam, 3.5)
//					SET_CAM_ACTIVE_WITH_INTERP(zoomCam, fpCam.theCam, 10000, GRAPH_TYPE_SIN_ACCEL_DECEL)
//					CPRINTLN(DEBUG_RANDOM_EVENTS, "Camera created")
//				ELSE
//					CPRINTLN(DEBUG_RANDOM_EVENTS, "B")
//					IF NOT IS_CAM_INTERPOLATING(zoomCam)
//						IF IS_PLAYER_LOOKING_AT_LICENSE_PLATE(veh, FALSE)
//						AND IS_ENTITY_IN_ANGLED_AREA(veh, v_Stakeout_area_Pos1, v_Stakeout_area_Pos2, f_Stakeout_area_Width)
//							RETURN TRUE
//						ENDIF
//					ELSE
//						CPRINTLN(DEBUG_RANDOM_EVENTS, "Camera interping")
//					ENDIF
//				ENDIF
//			ELSE
//				IF DOES_CAM_EXIST(zoomCam)
//				AND DOES_CAM_EXIST(fpCam.theCam)
//					SET_CAM_ACTIVE(fpCam.theCam, TRUE)
//					DESTROY_CAM(zoomCam)
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	RETURN FALSE
//ENDFUNC

ENUM AH1_LP_SPOTTING_ENUM
	AH1LP_INACTIVE = 0,
	AH1LP_CREATE_ZOOM_CAM,
	AH1LP_INTERP_TO_ZOOM_CAM,
	AH1LP_ZOOM_IN,
	AH1LP_CANCELLED
ENDENUM

AH1_LP_SPOTTING_ENUM eLPstage = AH1LP_INACTIVE

#IF IS_DEBUG_BUILD
	FUNC STRING GET_STRING_FROM_LP_ENUM(AH1_LP_SPOTTING_ENUM paramLPStage)
		SWITCH paramLPStage
			CASE AH1LP_INACTIVE				RETURN "AH1LP_INACTIVE"				BREAK
			CASE AH1LP_CREATE_ZOOM_CAM		RETURN "AH1LP_CREATE_ZOOM_CAM"		BREAK
			CASE AH1LP_INTERP_TO_ZOOM_CAM	RETURN "AH1LP_INTERP_TO_ZOOM_CAM"	BREAK
			CASE AH1LP_ZOOM_IN				RETURN "AH1LP_ZOOM_IN"				BREAK
			CASE AH1LP_CANCELLED			RETURN "AH1LP_CANCELLED"			BREAK
		ENDSWITCH
		RETURN ""
	ENDFUNC
#ENDIF

FUNC BOOL HAS_PLAYER_SPOTTED_PLATE(VEHICLE_INDEX veh, VECTOR paramOffsetForLp)

	BOOL bSpotThisFrame = FALSE
	
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_LP_ENUM(eLPstage), <<0.8, 0.02, 0>>, 255, 0, 0)
	#ENDIF
	
	IF DOES_CAM_EXIST(fpcam.theCam)
	
		IF DOES_ENTITY_EXIST(veh)
		AND NOT IS_ENTITY_DEAD(veh)
		AND VDIST(v_OfficeCoords, GET_ENTITY_COORDS(veh, FALSE)) < 130.36
		AND IS_ENTITY_IN_ANGLED_AREA(veh, vLargerStakeoutAreaPos1, vLargerStakeoutAreaPos2, fLargerStakeoutAreaWidth)
		AND SHOULD_CONTROL_CHASE_HINT_CAM(focuscam, FALSE)
			IF eLPstage = AH1LP_INACTIVE
				eLPstage = AH1LP_CREATE_ZOOM_CAM
			ENDIF
		ELSE
			IF eLPstage != AH1LP_INACTIVE
			AND eLPstage != AH1LP_CANCELLED
				eLPstage = AH1LP_CANCELLED
			ENDIF
		ENDIF
		
		FLOAT fNearClip = 0.5
		SWITCH eLPstage
		
			CASE AH1LP_CREATE_ZOOM_CAM
			
				IF NOT DOES_CAM_EXIST(zoomCam)
					zoomCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, GET_CAM_COORD(fPcam.theCam), <<0,0,0>>, 3.5 /*GET_CAM_FOV(fpCam.theCam)*/, FALSE)
//					SET_CAM_FOV(zoomCam, 10)
				ENDIF
				
				IF DOES_CAM_EXIST(zoomCam)
					IF (IS_PC_VERSION() AND GET_ASPECT_RATIO(TRUE) > 1.8)
						// Reduce near clip for triple head since camera is clipping car window
						fNearClip = 0.25
					ENDIF
				
					SET_CAM_NEAR_CLIP(zoomCam, fNearClip)
					SET_CAM_FOV(zoomCam, 3.5)
					SET_CAM_IS_INSIDE_VEHICLE(zoomCam, TRUE)
					POINT_CAM_AT_ENTITY(zoomCam, veh, paramOffsetForLp)
//					SET_CAM_FOV(zoomCam, 10)
					SET_CAM_ACTIVE_WITH_INTERP(zoomCam, fpCam.theCam, 1500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_SIN_ACCEL_DECEL)
					fZoomReached  = 3.5
					eLPstage = AH1LP_INTERP_TO_ZOOM_CAM
				ENDIF
				
			BREAK
			
			CASE AH1LP_INTERP_TO_ZOOM_CAM
			
				IF NOT IS_CAM_INTERPOLATING(zoomCam)
					eLPstage = AH1LP_ZOOM_IN
				ELSE
					PRINTLN("interping")
//					IF fZoomReached > 3.5
//						fZoomReached = fZoomReached +@ (-fZoomReached*2)
//					ENDIF
//					SET_CAM_FOV(zoomCam, fZoomReached)
				ENDIF
				
			BREAK
			
			CASE AH1LP_ZOOM_IN
			
				IF fZoomReached > 3.5
					fZoomReached = fZoomReached +@ (-fZoomReached*2)
				ENDIF
				
				SET_CAM_FOV(zoomCam, fZoomReached)
				
				IF fZoomReached < 5.0
					IF IS_PLAYER_LOOKING_AT_LICENSE_PLATE(veh, FALSE)
					AND IS_ENTITY_IN_ANGLED_AREA(veh, v_Stakeout_area_Pos1, v_Stakeout_area_Pos2, f_Stakeout_area_Width)
						bSpotThisFrame = TRUE
					ENDIF
				ENDIF
				
			BREAK
			
			CASE AH1LP_CANCELLED
			
				IF DOES_CAM_EXIST(zoomCam)
				
					IF NOT IS_CAM_RENDERING(fpCam.theCam)
					AND IS_CAM_ACTIVE(zoomCam)
						SET_CAM_FOV(fpCam.theCam, 50)//44.178452)
						SET_CAM_ACTIVE(zoomCam, FALSE)
						SET_CAM_ACTIVE(fpCam.theCam, TRUE)
					ENDIF
					
					IF IS_CAM_ACTIVE(fpCam.theCam)
					AND IS_CAM_RENDERING(fpCam.theCam)
						DESTROY_CAM(zoomCam)
					ENDIF
					
					KILL_CHASE_HINT_CAM(focuscam)
					
				ELSE
				
					eLPstage = AH1LP_INACTIVE
					
				ENDIF
				
			BREAK
			
		ENDSWITCH
		
		#IF IS_DEBUG_BUILD
			IF DOES_ENTITY_EXIST(veh)
				PRINTLN("distance to vehicle:", VDIST(v_OfficeCoords, GET_ENTITY_COORDS(veh, FALSE)))
			ENDIF
		#ENDIF
		
	ENDIF
	
	RETURN bSpotThisFrame
		
ENDFUNC
//
//FUNC BOOL UPDATE_LOOK_AT_LICENSE_PLATE(VEHICLE_INDEX veh, VECTOR paramOffsetForLp)
//		//Progressively zoom in.
//	IF NOT IS_ENTITY_DEAD(veh)
//	AND veh <> NULL
//	AND VDIST2(v_OfficeCoords, GET_ENTITY_COORDS(veh)) < POW(124.36,2)
//	AND IS_ENTITY_IN_ANGLED_AREA(veh, vLargerStakeoutAreaPos1, vLargerStakeoutAreaPos2, fLargerStakeoutAreaWidth)
//	AND SHOULD_CONTROL_CHASE_HINT_CAM(focuscam, FALSE)
//		IF DOES_CAM_EXIST(zoomCam)
//			IF NOT IS_CAM_ACTIVE(zoomCam)
//				IF NOT b_help_used
//					b_help_used = TRUE
//				ENDIF
//				ACTIVATE_ZOOM_CAM(TRUE)
//			ENDIF
//			//Point it at car number plate area.
//			VECTOR focus_point = GET_CLOSEST_POINT_ON_LINE(GET_CAM_COORD(zoomCam), GET_ENTITY_COORDS(veh) + <<0,0,1>>, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(veh, paramOffsetForLp))
//			POINT_CAM_AT_COORD(zoomCam, focus_point)
//			SET_CAM_FOV(zoomCam, fZoomReached)
//			SET_CAM_ACTIVE(zoomCam, TRUE)
//			IF fZoomReached > 3.5
//				fZoomReached = fZoomReached +@ (-fZoomReached*2)
//			ENDIF
//			IF fZoomReached < 5.0
//				IF IS_PLAYER_LOOKING_AT_LICENSE_PLATE(veh, FALSE)
//				AND IS_ENTITY_IN_ANGLED_AREA(veh, v_Stakeout_area_Pos1, v_Stakeout_area_Pos2, f_Stakeout_area_Width)
//					RETURN TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ELSE
//		IF DOES_CAM_EXIST(zoomCam)
//			IF IS_CAM_ACTIVE(zoomCam)
//				PRINTLN("KILLING ZOOM CAM")
//				IF DOES_CAM_EXIST(fpCam.theCam)
//					SET_CAM_FOV(fpCam.theCam, fZoomReached)
//					PRINTLN("SETTING fpCam FOV to fZoomReached")
//				ENDIF
//				fZoomReached  = 44.178452
//				ACTIVATE_ZOOM_CAM(FALSE)
//			ELSE
//				IF DOES_CAM_EXIST(fpCam.theCam)
//					IF GET_CAM_FOV(fpCam.theCam) < 44.178452
//						PRINTLN("SETTING_CAM_FOV TO 44.178452 INCREMENTALLY")
//						SET_CAM_FOV(fpCam.theCam, GET_CAM_FOV(fpCam.theCam) +@ (GET_CAM_FOV(fpCam.theCam)*1.3))
//					ELIF GET_CAM_FOV(fpCam.theCam) <> 44.178452
//						PRINTLN("SETTING_CAM_FOV TO 44.178452 INSTANTLY")
//						SET_CAM_FOV(fpCam.theCam, 44.178452)
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	RETURN FALSE
//	
//ENDFUNC

PROC SET_PED_AS_BUDDY(PED_INDEX ped)
	SET_PED_CAN_BE_DRAGGED_OUT(ped, FALSE)
	SET_PED_CAN_BE_TARGETTED(ped, FALSE)
	SET_PED_KEEP_TASK(ped, TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(ped, rghBuddy)
	SET_PED_CONFIG_FLAG(ped, PCF_GetOutUndriveableVehicle, FALSE)
	SET_PED_CONFIG_FLAG(ped, PCF_GetOutBurningVehicle, FALSE)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, TRUE)
	SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(ped, FALSE)
	SET_PED_CAN_PLAY_AMBIENT_ANIMS(ped, FALSE)
	
ENDPROC

FUNC BOOL CREATE_BUDDY_ON_FOOT(PED_INDEX &ped, enumCharacterList char_buddy, VECTOR vCoords, FLOAT fHeading = 0.0)
	REQUEST_MODEL(GET_NPC_PED_MODEL(char_buddy))
	IF HAS_MODEL_LOADED(GET_NPC_PED_MODEL(char_buddy))
		IF NOT DOES_ENTITY_EXIST(ped)
			CREATE_NPC_PED_ON_FOOT(ped, char_buddy, vCoords, fHeading)
		ENDIF
		IF NOT IS_PED_INJURED(ped)
			SET_ENTITY_COORDS(ped, vCoords)
			SET_ENTITY_HEADING(ped, fHeading)
			IF char_buddy = CHAR_LESTER
				SET_PED_PROP_INDEX(ped, ANCHOR_EYES, 0, 0)
			ENDIF
			SET_PED_AS_BUDDY(ped)
			RETURN TRUE
		ENDIF
		#IF IS_DEBUG_BUILD
			IF char_buddy = CHAR_LESTER
				SET_PED_NAME_DEBUG(ped, "LESTER")
			ELIF char_buddy = CHAR_DAVE
				SET_PED_NAME_DEBUG(ped, "DAVE")
			ELIF char_buddy = CHAR_STEVE
				SET_PED_NAME_DEBUG(ped, "STEVE")
			ENDIF
		#ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL CREATE_BUDDY_IN_VEHICLE(PED_INDEX &ped, VEHICLE_INDEX veh, enumCharacterList char_buddy, VEHICLE_SEAT vehSeat = VS_FRONT_RIGHT)
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF CREATE_NPC_PED_INSIDE_VEHICLE(ped, char_buddy, vehPlayerCar, vehSeat)
			SET_PED_AS_BUDDY(ped)
			IF char_buddy = CHAR_LESTER
				SET_PED_PROP_INDEX(ped, ANCHOR_EYES, 0, 0)
			ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC FORCE_RESET_LESTER()
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		DELETE_PED(pedLester)
		CHECK_PEDS_FOR_DIALOGUE()
		WHILE NOT CREATE_NPC_PED_INSIDE_VEHICLE(pedLester, CHAR_LESTER, vehPlayerCar, VS_FRONT_RIGHT)
			WAIT(0)
		ENDWHILE
		SET_PED_PROP_INDEX(pedLester, ANCHOR_EYES, 0, 0)
		SET_PED_AS_BUDDY(pedLester)		
		CHECK_PEDS_FOR_DIALOGUE()
	ENDIF
ENDPROC

FUNC BOOL SAFELY_CREATE_PLAYER_VEHICLE(VECTOR position, FLOAT heading, BOOL playerInside = FALSE, BOOL lesterInside = FALSE)

	IF NOT DOES_ENTITY_EXIST(vehPlayerCar)
		CREATE_PLAYER_VEHICLE(vehPlayerCar, CHAR_MICHAEL, position, heading)
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		IF LesterInside
			IF NOT IS_PED_INJURED(pedLester)
				IF NOT IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
					SET_PED_INTO_VEHICLE(pedLester, vehPlayerCar, VS_FRONT_RIGHT)
				ENDIF
			ENDIF
		ENDIF
		IF playerInside
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayerCar, VS_DRIVER)
			ENDIF
		ENDIF
		SET_ENTITY_COORDS(vehPlayerCar, position)
		SET_ENTITY_HEADING(vehPlayerCar, heading)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCar)
		APPLY_FORCE_TO_ENTITY(vehPlayerCar, APPLY_TYPE_EXTERNAL_FORCE, <<0,0,-0.1>>, <<0,0,0>>, 0, TRUE, TRUE, FALSE)
	ENDIF
	
	RETURN DOES_ENTITY_EXIST(vehPlayerCar)
	
ENDFUNC

PROC MISSION_VEHICLE_FAIL_CHECKS()
	IF NOT bTyresUnpoppable
		IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			SET_VEHICLE_TYRES_CAN_BURST(vehPlayerCar, FALSE)
			bTyresUnpoppable = TRUE
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehPlayerCar)
		IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			IF mission_stage = STAGE_DRIVE_BACK_TO_SAFEHOUSE
				IF NOT IS_VEHICLE_EMPTY(vehPlayerCar)
				OR IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
					IF IS_VEHICLE_STUCK_TIMER_UP(vehPlayerCar, VEH_STUCK_ON_ROOF, ROOF_TIME)
					OR IS_VEHICLE_STUCK_TIMER_UP(vehPlayerCar, VEH_STUCK_JAMMED, JAMMED_TIME)
					OR IS_VEHICLE_STUCK_TIMER_UP(vehPlayerCar, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
					OR IS_VEHICLE_STUCK_TIMER_UP(vehPlayerCar, VEH_STUCK_ON_SIDE, SIDE_TIME)
						Mission_Failed("S1_VEHSTUCK")
					ENDIF
				ENDIF
			ELSE
				IF IS_VEHICLE_STUCK_TIMER_UP(vehPlayerCar, VEH_STUCK_ON_ROOF, ROOF_TIME)
				OR IS_VEHICLE_STUCK_TIMER_UP(vehPlayerCar, VEH_STUCK_JAMMED, JAMMED_TIME)
				OR IS_VEHICLE_STUCK_TIMER_UP(vehPlayerCar, VEH_STUCK_HUNG_UP, HUNG_UP_TIME)
				OR IS_VEHICLE_STUCK_TIMER_UP(vehPlayerCar, VEH_STUCK_ON_SIDE, SIDE_TIME)
					Mission_Failed("S1_VEHSTUCK")
				ENDIF
			ENDIF
		ELSE
			IF mission_stage < STAGE_DRIVE_BACK_TO_SAFEHOUSE
				Mission_Failed("CMN_GENDEST")
			ELSE
				IF NOT IS_VEHICLE_EMPTY(vehPlayerCar)
				OR IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
					Mission_Failed("CMN_GENDEST")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC CLEAR_BLIPS()
	REMOVE_BLIP(blipPlayerCar)
	REMOVE_BLIP(blipOfficeFBI)
	REMOVE_BLIP(blipParkingSpace)
ENDPROC

PROC CLEAR_ALL()
	b_JanitorHome = FALSE
	b_help_used = FALSE
	b_janitor_spotted = FALSE
	bRequestNextStage = FALSE
	in_car_conv_started = FALSE
	in_car_conv_on_hold = FALSE
	iJanSeqProg = 0
	ConvResumeLabel = ""
	current_car = 0
	iStageProgress = 0
ENDPROC

PROC SETUP_FIRST_PERSON()
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayerCar, VS_DRIVER)
		ENDIF
		SET_ENTITY_COORDS_NO_OFFSET(vehPlayerCar, <<72.6414, -702.4333, 31.1800>>)
		SET_ENTITY_QUATERNION(vehPlayerCar, 0.0019, 0.0174, -0.1740, 0.9846)
		SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCar)
	ENDIF
	
	FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
	HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), TRUE)
	HANG_UP_AND_PUT_AWAY_PHONE()
	SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
	
ENDPROC

INT iGirlIdleScene

PROC CREATE_HOT_GIRL()

	REQUEST_MODEL(A_F_Y_Hipster_01)
	REQUEST_MODEL(DILETTANTE)
	REQUEST_ANIM_DICT("missheistfbisetup1")
	PRINTLN("LOADING HOT GIRL ASSETS")
	
	IF NOT DOES_ENTITY_EXIST(pedMarilyn)
	AND NOT DOES_ENTITY_EXIST(carHotGirl)
		IF HAS_MODEL_LOADED(A_F_Y_Hipster_01)
		AND HAS_MODEL_LOADED(DILETTANTE)
		AND HAS_ANIM_DICT_LOADED("missheistfbisetup1")
		
			carHotGirl = CREATE_VEHICLE(DILETTANTE, <<-81.150002,-18.851000,65.820000>>, 67.682190)
			SET_ENTITY_COORDS_NO_OFFSET(carHotGirl, <<-81.1500, -18.8510, 65.8200>>)
			SET_ENTITY_ROTATION(carHotGirl, <<-0.0000, 0.0000, 67.6822>>)
			SET_ENTITY_QUATERNION(carHotGirl, 0.0000, 0.0000, 0.5569, 0.8306)
			SET_VEHICLE_ON_GROUND_PROPERLY(carHotGirl)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, neutrals, RELGROUPHASH_PLAYER)
			SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_PLAYER, neutrals)
			SET_VEHICLE_COLOUR_COMBINATION(carHotGirl, 2)
			SET_VEHICLE_ALARM(carHotGirl, TRUE)
			SET_VEHICLE_DOORS_LOCKED(carHotGirl, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			
			pedMarilyn = CREATE_PED(PEDTYPE_CIVFEMALE, A_F_Y_Hipster_01, <<-81.93, -19.81, 65.32>>, -18.56)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedMarilyn, neutrals)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMarilyn, TRUE)
			SET_PED_COMBAT_ATTRIBUTES(pedMarilyn, CA_ALWAYS_FLEE, TRUE)
			SET_PED_COMPONENT_VARIATION(pedMarilyn, PED_COMP_HEAD, 1, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedMarilyn, PED_COMP_HAIR, 0, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedMarilyn, PED_COMP_TORSO, 0, 2, 0)
			SET_PED_COMPONENT_VARIATION(pedMarilyn, PED_COMP_LEG, 0, 2, 0)
			SET_PED_COMPONENT_VARIATION(pedMarilyn, PED_COMP_SPECIAL, 0, 1, 0)
			SET_PED_LOD_MULTIPLIER(pedMarilyn, 2.0)
			
			iGirlIdleScene = CREATE_SYNCHRONIZED_SCENE(vHassleScenePosition, vHassleSceneRotation)
			TASK_SYNCHRONIZED_SCENE(pedMarilyn, iGirlIdleScene, "missheistfbisetup1", "hassle_intro_loop_f", INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			SET_SYNCHRONIZED_SCENE_LOOPED(iGirlIdleScene, TRUE)
			ADD_PED_FOR_DIALOGUE(sSpeech, 5, pedMarilyn, "AH_NEIGHBOUR")
			
			SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_HIPPIE_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(DILETTANTE)
			
		ENDIF
	ENDIF
ENDPROC

PROC FORCE_DELETE_HOT_GIRL()
	IF DOES_ENTITY_EXIST(pedMarilyn)
		DELETE_PED(pedMarilyn)
	ENDIF
	IF DOES_ENTITY_EXIST(carHotGirl)
		DELETE_VEHICLE(carHotGirl)
	ENDIF
ENDPROC

FUNC BOOL IS_MODEL_NAME_IN_ARRAY(MODEL_NAMES model, MODEL_NAMES &array[])
	INT iTemp
	BOOL bfound = FALSE
	REPEAT COUNT_OF(array) iTemp
		IF NOT bfound
			IF model = array[iTemp]
				bfound = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	RETURN bfound
ENDFUNC

FUNC BOOL REQUEST_ASSETS_FOR_STAGE(MISSION_STAGE_FLAG stage)

	PRINTLN("Requesting assets for stage: ", ENUM_TO_INT(stage))

	MODEL_NAMES scriptModels[11], stageModels[11]
	
	scriptModels[0] = S_M_M_JANITOR
	scriptModels[1] = GET_NPC_PED_MODEL(CHAR_LESTER)
	scriptModels[2] = GET_NPC_PED_MODEL(CHAR_STEVE)
	scriptModels[3] = GET_NPC_PED_MODEL(CHAR_DAVE)
	scriptModels[4] = TAILGATER
	scriptModels[5] = modelJanitorVeh
	scriptModels[6] = car_models[0]
	scriptModels[7] = car_models[1]
	scriptModels[8] = car_models[2]
	scriptModels[9] = driver_model
	scriptModels[10] = A_F_Y_HIPSTER_03
	
	INT iTemp
	
	REPEAT COUNT_OF(stageModels) iTemp
		stageModels[iTemp] = DUMMY_MODEL_FOR_SCRIPT
	ENDREPEAT
	
	SWITCH stage
		CASE STAGE_GO_TO_FIB
			stageModels[0] = GET_NPC_PED_MODEL(CHAR_LESTER)
			stageModels[1] = GET_NPC_PED_MODEL(CHAR_STEVE)
			stageModels[2] = GET_NPC_PED_MODEL(CHAR_DAVE)
			stageModels[3] = GET_NPC_VEH_MODEL(CHAR_DAVE)
			stageModels[4] = TAILGATER
		BREAK
		CASE STAGE_TAIL_THE_JANITOR
			stageModels[0] = GET_NPC_PED_MODEL(CHAR_LESTER)
			stageModels[1] = TAILGATER
			stageModels[2] = S_M_M_JANITOR
			stageModels[3] = modelJanitorVeh
			REQUEST_VEHICLE_RECORDING(1, "FBIs1UBER")
			REQUEST_WAYPOINT_RECORDING(sParkWaypointRec)
		BREAK
		CASE STAGE_FOLLOW_JANITOR_ON_FOOT
			stageModels[0] = GET_NPC_PED_MODEL(CHAR_LESTER)
			stageModels[1] = TAILGATER
			stageModels[2] = S_M_M_JANITOR
			stageModels[3] = modelJanitorVeh
		BREAK
		CASE STAGE_JANITOR_CUT
			stageModels[0] = GET_NPC_PED_MODEL(CHAR_LESTER)
			stageModels[1] = TAILGATER
			stageModels[2] = S_M_M_JANITOR
			stageModels[3] = modelJanitorVeh
		BREAK
		CASE STAGE_DRIVE_BACK_TO_SAFEHOUSE
			stageModels[0] = modelJanitorVeh
			stageModels[1] = GET_NPC_PED_MODEL(CHAR_LESTER)
			stageModels[2] = TAILGATER
		BREAK
	ENDSWITCH
	
	BOOL bAllLoaded = TRUE
	
	REPEAT COUNT_OF(stageModels) iTemp
		IF stageModels[iTemp] <> DUMMY_MODEL_FOR_SCRIPT
			REQUEST_MODEL(stageModels[iTemp])
			IF NOT HAS_MODEL_LOADED(stageModels[iTemp])
				#IF IS_DEBUG_BUILD
					PRINTLN("Not loaded ", GET_MODEL_NAME_FOR_DEBUG(stageModels[iTemp]))
				#ENDIF
				bAllLoaded = FALSE
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF stage = STAGE_TAIL_THE_JANITOR
		IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "FBIs1UBER")
		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED(sParkWaypointRec)
			PRINTLN("Not loaded uber rec")
			bAllLoaded = FALSE
		ENDIF
	ENDIF
	
	REPEAT COUNT_OF(scriptModels) iTemp
		IF NOT IS_MODEL_NAME_IN_ARRAY(scriptModels[iTemp], stageModels)
			SET_MODEL_AS_NO_LONGER_NEEDED(scriptModels[iTemp])
		ENDIF
	ENDREPEAT
	
	PRINTLN("LOADING ASSETS FOR STAGE 2")
	
	RETURN bAllLoaded
	
ENDFUNC

PROC GO_TO_STAGE(MISSION_STAGE_FLAG stage)
	
	PRINTLN("Going to mission stage ", ENUM_TO_INT(stage))
	
	IF NOT IS_PED_INJURED(pedLester)
		
		IF stage = STAGE_WAIT_FOR_JANITOR
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "STAGE_WAIT_FOR_JANITOR", FALSE, FALSE, pedLester, TRUE)
		ELIF stage = STAGE_TAIL_THE_JANITOR
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "STAGE_TAIL_THE_JANITOR", FALSE, FALSE, pedLester, TRUE)
		ELIF stage >= STAGE_FOLLOW_JANITOR_ON_FOOT AND stage < STAGE_DRIVE_BACK_TO_SAFEHOUSE
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "STAGE_FOLLOW_JANITOR_ON_FOOT", FALSE, FALSE, pedLester, TRUE)
		ELIF stage = STAGE_DRIVE_BACK_TO_SAFEHOUSE
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "STAGE_DRIVE_BACK_TO_SAFEHOUSE", TRUE, FALSE, pedLester, TRUE)
		ENDIF
		
	ELSE
		
		IF stage = STAGE_WAIT_FOR_JANITOR
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "STAGE_WAIT_FOR_JANITOR", FALSE, FALSE, NULL, TRUE)
		ELIF stage = STAGE_TAIL_THE_JANITOR
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "STAGE_TAIL_THE_JANITOR", FALSE, FALSE, NULL, TRUE)
		ELIF stage >= STAGE_FOLLOW_JANITOR_ON_FOOT AND stage < STAGE_DRIVE_BACK_TO_SAFEHOUSE
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "STAGE_FOLLOW_JANITOR_ON_FOOT", FALSE, FALSE, NULL, TRUE)
		ELIF stage = STAGE_DRIVE_BACK_TO_SAFEHOUSE
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "STAGE_DRIVE_BACK_TO_SAFEHOUSE", TRUE, FALSE, NULL, TRUE)
		ENDIF
	
	ENDIF
	
	SET_DOOR_STATE(DOORNAME_JANITORS_APARTMENT, DOORSTATE_LOCKED)
	
	mission_stage = stage
	
	PRINTLN("Mission stage is now ", ENUM_TO_INT(stage))
	
	IF IS_CUTSCENE_ACTIVE()
	AND IS_CUTSCENE_PLAYING()
		STOP_CUTSCENE()
		REMOVE_CUTSCENE()
		WHILE IS_CUTSCENE_ACTIVE()
			PRINTSTRING("REMOVING CUTSCENE\n")
			WAIT(0)
		ENDWHILE
	ENDIF
	
	IF IS_RADAR_HIDDEN()
		DISPLAY_RADAR(TRUE)
	ENDIF
	
	CLEAR_ALL()
	CLEAR_BLIPS()
	
	IF stage <> STAGE_TAIL_THE_JANITOR
		REMOVE_BLIP(blipJanitor)
	ENDIF
	
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	IF NOT IS_ENTITY_DEAD(vehPlayerCar)
		SET_VEHICLE_LIGHTS(vehPlayerCar, NO_VEHICLE_LIGHT_OVERRIDE)
	ENDIF
	INIT_TEXT()
	
	CLEAR_HELP()
	CLEAR_PRINTS()
	
	IF stage <> STAGE_WAIT_FOR_JANITOR
	AND NOT IS_REPLAY_BEING_SET_UP()
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	ENDIF
	IF NOT IS_ENTITY_VISIBLE(PLAYER_PED_ID())
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	IF bSkip
		IF DOES_ENTITY_EXIST(objBottle)
			DELETE_OBJECT(objBottle)
		ENDIF
		IF DOES_ENTITY_EXIST(objBag)
			DELETE_OBJECT(objBag)
		ENDIF
		FADE_OUT()
		IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			FREEZE_ENTITY_POSITION(vehPlayerCar, FALSE)
			SET_VEHICLE_DOORS_SHUT(vehPlayerCar)
		ENDIF
		FORCE_DELETE_DECOY_CARS()
		FORCE_DELETE_JANITOR()
		FORCE_DELETE_HOT_GIRL()
		IF mission_stage <> STAGE_TAIL_THE_JANITOR
			DESTROY_STAKEOUT_CAM(zoomCam)
			DESTROY_STAKEOUT_CAM(fpCam.theCam)
		ENDIF
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		KILL_ANY_CONVERSATION()
		WHILE NOT REQUEST_ASSETS_FOR_STAGE(stage)
			PRINTSTRING("LOADING ASSETS FOR STAGE 1\n")
			WAIT(0)
		ENDWHILE
		IF DOES_ENTITY_EXIST(vehFranklin)
			DELETE_VEHICLE(vehFranklin)
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_GET_TO_FIB")
			STOP_AUDIO_SCENE("AGENCY_H_1_GET_TO_FIB")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_WAIT_FOR_CARS")
			STOP_AUDIO_SCENE("AGENCY_H_1_WAIT_FOR_CARS")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_NON_TARGET_CARS")
			STOP_AUDIO_SCENE("AGENCY_H_1_NON_TARGET_CARS")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_JANITORS_CAR")
			STOP_AUDIO_SCENE("AGENCY_H_1_JANITORS_CAR")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_FOCUS_ON_PLATES")
			STOP_AUDIO_SCENE("AGENCY_H_1_FOCUS_ON_PLATES")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_FOLLOW_JANITOR")
			STOP_AUDIO_SCENE("AGENCY_H_1_FOLLOW_JANITOR")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_FOCUS_ON_JANITOR")
			STOP_AUDIO_SCENE("AGENCY_H_1_FOCUS_ON_JANITOR")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_FOLLOW_ON_FOOT")
			STOP_AUDIO_SCENE("AGENCY_H_1_FOLLOW_ON_FOOT")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_DRIVE_TO_FACTORY")
			STOP_AUDIO_SCENE("AGENCY_H_1_DRIVE_TO_FACTORY")
		ENDIF
	ENDIF
	
	IF DOES_CAM_EXIST(fpCam.theCam)
		DESTROY_CAM(fpCam.theCam)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	ENDIF
	
	IF DOES_CAM_EXIST(zoomCam)
		DESTROY_CAM(zoomCam)
	ENDIF
	
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	
	INT iTemp
	
	REPEAT COUNT_OF(CarParkDummies) iTemp
		IF DOES_ENTITY_EXIST(CarParkDummies[iTemp])
			DELETE_VEHICLE(CarParkDummies[iTemp])
		ENDIF
	ENDREPEAT
	
	IF mission_stage <> STAGE_DRIVE_BACK_TO_SAFEHOUSE
		SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
	ENDIF
	
	PRINTSTRING("LEAVING GO TO STAGE SECTION\n")
	
	iStageProgress = 0
	
ENDPROC

BOOL bLoseCopsConvDone

PROC LOSE_THE_COPS()

	IF mission_stage <> STAGE_LOSE_COPS
	
		eSavedStage = mission_stage
		mission_stage = STAGE_LOSE_COPS
		CLEAR_PRINTS()
		CLEAR_BLIPS()
		gtLoseCops.played = FALSE
		bLoseCopsConvDone = FALSE
		
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND in_car_conv_started
		AND NOT in_car_conv_on_hold
			in_car_conv_on_hold = TRUE
			ConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
		ENDIF
		
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		
	ENDIF
	
	IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	OR (GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES)) = 0
		PRINT_GOD_TEXT(gtLoseCops)
	ENDIF
	
	IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vPlayerCarCoords) > 75
		IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_R)) != DOORSTATE_LOCKED
		AND DOOR_SYSTEM_GET_DOOR_PENDING_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_R)) != DOORSTATE_LOCKED
			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_R), 0)
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_R, DOORSTATE_LOCKED)
		ENDIF
		IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_L)) != DOORSTATE_LOCKED
		AND DOOR_SYSTEM_GET_DOOR_PENDING_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_L)) != DOORSTATE_LOCKED
			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_L), 0)
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_L, DOORSTATE_LOCKED)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
			IF DOES_BLIP_EXIST(blipPlayerCar)
				REMOVE_BLIP(blipPlayerCar)
			ENDIF
			IF NOT bLoseCopsConvDone
				IF eSavedStage = STAGE_GO_TO_FIB
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
					OR (GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES)) = 0
						IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FBS1_WANT", CONV_PRIORITY_HIGH)
							bLoseCopsConvDone = TRUE
						ENDIF
					ENDIF
				ELSE
					bLoseCopsConvDone = TRUE
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(pedLester)
			AND IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
				IF NOT DOES_BLIP_EXIST(blipPlayerCar)	
					blipPlayerCar = CREATE_BLIP_FOR_VEHICLE(vehPlayerCar)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
		IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_R)) = DOORSTATE_LOCKED
		OR DOOR_SYSTEM_GET_DOOR_PENDING_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_R)) = DOORSTATE_LOCKED
			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_R), 0)
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_R, DOORSTATE_UNLOCKED)
		ENDIF
		IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_L)) = DOORSTATE_LOCKED
		OR DOOR_SYSTEM_GET_DOOR_PENDING_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_L)) = DOORSTATE_LOCKED
			DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_L), 0)
			SET_DOOR_STATE(DOORNAME_SWEATSHOP_L, DOORSTATE_UNLOCKED)
		ENDIF
		mission_stage = eSavedStage
		CLEAR_PRINTS()
	ENDIF
	
ENDPROC

FUNC BOOL HAS_PED_BEEN_ABANDONED(PED_INDEX ped)
	IF DOES_ENTITY_EXIST(ped)
		IF NOT IS_PED_INJURED(ped)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(ped, FALSE)) > 150
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CHECK_WALKING_ROUTE_FOR_OBSTRUCTIONS()
	IF GET_RANDOM_VEHICLE_IN_SPHERE(<<-86.58, -17.64, 65.77>>, 12, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES) <> NULL
	OR GET_RANDOM_VEHICLE_BACK_BUMPER_IN_SPHERE(<<-86.58, -17.64, 65.77>>, 12, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES) <> NULL
	OR GET_RANDOM_VEHICLE_FRONT_BUMPER_IN_SPHERE(<<-86.58, -17.64, 65.77>>, 12, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES) <> NULL	
		TASK_SMART_FLEE_PED(pedJanitor, PLAYER_PED_ID(), 200, 20000, TRUE)
		Mission_Failed("S1_JSPOOK")
	ENDIF
ENDPROC

INT timeOfLastFrame
FLOAT fTotalLurkingTime = 0

FUNC BOOL HAS_PLAYER_SPOOKED_JANITOR_ON_FOOT()
	
	IF DOES_ENTITY_EXIST(pedJanitor)
	
		FLOAT minLurkTime = 1.0
		FLOAT maxLurkTime = 5.0
		
		FLOAT minDist = 5.0
		FLOAT fDistToJan = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedJanitor, FALSE))
		
		IF NOT IS_PED_INJURED(pedJanitor)
			
			IF NOT IS_PED_IN_JANITORS_APARTMENT(pedJanitor)
				IF fDistToJan < minDist
					PRINT_NOW("S1_BACKUP", 1000, 1)
					IF timeOfLastFrame <> 0
						fTotalLurkingTime += GET_FRAME_TIME()
					ENDIF
					timeOfLastFrame = GET_GAME_TIMER()
				ELSE
					timeOfLastFrame = 0
				ENDIF
				
				IF fDistToJan < minDist
				AND fTotalLurkingTime > LERP_FLOAT(minLurkTime, maxLurkTime, fDistToJan/minDist)
					RETURN TRUE
				ENDIF
				
			ENDIF
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF IS_ENTITY_IN_ANGLED_AREA(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), <<-74.949501,-20.331139,64.585152>>, <<-101.633774,-10.822943,70.845352>>, 13.000000)
					AND fDistToJan < 5.0
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF fDistToJan < 10
			AND IS_ENTITY_IN_ANGLED_AREA(pedJanitor, <<-77.885201,-15.151772,65.368904>>, <<-106.713531,-5.299530,72.150627>>, 7.000000)
			AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(pedJanitor, PLAYER_PED_ID())
				RETURN TRUE
			ENDIF
			
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedJanitor, PLAYER_PED_ID())
				bSpeechOnFootFail = FALSE
				RETURN TRUE
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedMarilyn)
			
				BOOL bTouchingSomeHow
				
				IF NOT IS_PED_INJURED(pedMarilyn)
				AND VDIST2(GET_ENTITY_COORDS(pedMarilyn, FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < 5*5
					IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), pedMarilyn)
						bTouchingSomeHow = TRUE
					ENDIF
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					AND IS_ENTITY_TOUCHING_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), pedMarilyn)
						bTouchingSomeHow = TRUE
					ENDIF
					
					IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
					AND NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
					AND VDIST2(GET_ENTITY_COORDS(pedMarilyn, FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < 5*5
					AND IS_ENTITY_TOUCHING_ENTITY(GET_PLAYERS_LAST_VEHICLE(), pedMarilyn)
						bTouchingSomeHow = TRUE
					ENDIF
				
				ENDIF
				
				IF DOES_ENTITY_EXIST(carHotGirl)
				
					IF IS_ENTITY_DEAD(pedMarilyn)
					OR IS_PED_INJURED(pedMarilyn)
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedMarilyn, PLAYER_PED_ID())
					OR bTouchingSomeHow
					OR IS_PED_FLEEING(pedMarilyn)
					OR (IS_VEHICLE_DRIVEABLE(carHotGirl) AND IS_VEHICLE_ALARM_ACTIVATED(carHotGirl))
					
						IF NOT IS_PED_INJURED(pedMarilyn)
							IF NOT IS_PED_FLEEING(pedMarilyn)
								TASK_REACT_AND_FLEE_PED(pedMarilyn, PLAYER_PED_ID())
							ENDIF
						ENDIF
						
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iHassleSceneId)
						OR VDIST2(GET_ENTITY_COORDS(pedJanitor, FALSE), GET_ENTITY_COORDS(pedMarilyn, FALSE)) < 10*10
						OR (IS_PED_IN_ANY_VEHICLE(pedJanitor) AND VDIST(GET_ENTITY_COORDS(pedJanitor, FALSE), v_parking_spot_for_janitor) < 7)
							IF NOT IS_PED_IN_ANY_VEHICLE(pedJanitor)
								bSpeechOnFootFail = TRUE
							ELSE
								bSpeechOnFootFail = FALSE
							ENDIF
							RETURN TRUE
						ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDIF
			
			IF IS_PED_FLEEING(pedJanitor)
				bSpeechOnFootFail = FALSE
				RETURN TRUE
			ENDIF

			IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADE, 					GET_ENTITY_COORDS(pedJanitor, FALSE), 10)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_GRENADELAUNCHER, 			GET_ENTITY_COORDS(pedJanitor, FALSE), 10)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_STICKYBOMB, 				GET_ENTITY_COORDS(pedJanitor, FALSE), 10)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_MOLOTOV, 					GET_ENTITY_COORDS(pedJanitor, FALSE), 10)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_ROCKET, 					GET_ENTITY_COORDS(pedJanitor, FALSE), 10)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_TANKSHELL, 				GET_ENTITY_COORDS(pedJanitor, FALSE), 10)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_BZGAS, 					GET_ENTITY_COORDS(pedJanitor, FALSE), 10)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_SMOKE_GRENADE, 			GET_ENTITY_COORDS(pedJanitor, FALSE), 10)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_SMOKE_GRENADE_LAUNCHER, 	GET_ENTITY_COORDS(pedJanitor, FALSE), 10)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_PROGRAMMABLEAR, 			GET_ENTITY_COORDS(pedJanitor, FALSE), 10)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_CAR, 						GET_ENTITY_COORDS(pedJanitor, FALSE), 10)
			OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_BIKE, 					GET_ENTITY_COORDS(pedJanitor, FALSE), 10)
				bSpeechOnFootFail = FALSE
				RETURN TRUE
			ENDIF
			
			IF IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedJanitor, FALSE), 5)
				bSpeechOnFootFail = FALSE
				RETURN TRUE
			ENDIF
			
			VEHICLE_INDEX vehTemp
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
				AND IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
					vehTemp = GET_PLAYERS_LAST_VEHICLE()
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(vehTemp)
				IF IS_ENTITY_IN_ANGLED_AREA(vehTemp, <<-74.647758,-15.944033,69.250465>>, <<-98.175003,-7.711004,65.129463>>, 4.250000)
					bSpeechOnFootFail = TRUE
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(pedJanitor)
				RETURN TRUE
			ENDIF
			
		ELSE
		
			bSpeechOnFootFail = FALSE
			RETURN TRUE
			
		ENDIF
		
		
		IF fDistToJan < 50
		AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_PROJECTILE|WF_INCLUDE_GUN)
		AND IS_PED_SHOOTING(PLAYER_PED_ID())
			IF NOT IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID())
				IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(pedJanitor, PLAYER_PED_ID())
					bSpeechOnFootFail = FALSE
					RETURN TRUE
				ENDIF 
			ELSE
				bSpeechOnFootFail = FALSE
				RETURN TRUE
			ENDIF
		ENDIF
		
		CHECK_WALKING_ROUTE_FOR_OBSTRUCTIONS()
	
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HAS_PLAYER_DRIVEN_ERRATICALLY()
	BOOL bReturn = FALSE
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
	AND IS_VEHICLE_DRIVEABLE(vehJanitor)
	AND NOT IS_PED_INJURED(pedJanitor)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
			IF VDIST2(GET_ENTITY_COORDS(vehPlayerCar, FALSE), GET_ENTITY_COORDS(vehJanitor, FALSE)) < 100*100
				IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehPlayerCar)
					bReturn = TRUE
				ENDIF
				IF GET_ENTITY_SPEED(vehPlayerCar) > 60
					bReturn = TRUE
				ENDIF
				IF IS_PED_SHOOTING(PLAYER_PED_ID())
					bReturn = TRUE
				ENDIF
				IF IS_PLAYER_PRESSING_HORN(PLAYER_ID())
					bReturn = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN bReturn
ENDFUNC

BOOL follow_conv_on_hold = FALSE
INT iTimeOfLastWarning = 0
INT iMinWarningTime = 15000
TEXT_LABEL_23 followResumeLabel = ""
TEXT_LABEL_23 tempRoot, tempLabel
BOOL bFollowReminderDue = FALSE
INT iTimeFollowReminder

FUNC BOOL IS_PLAYER_INFRONT_OF_JANITOR()
	BOOL bRet = FALSE
	IF IS_VEHICLE_DRIVEABLE(vehJanitor)
	AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		IF IS_ENTITY_IN_ANGLED_AREA(vehPlayerCar,
					GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehJanitor, <<0,2,20>>), 
					GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehJanitor, <<0,100,-20>>),
					30, TRUE)
					bRet = TRUE
		ENDIF
	ENDIF
	RETURN bRet
ENDFUNC


VECTOR vLeftTurn1Pos1 = <<110.111351,-584.371094,30.352879>>
VECTOR vLeftTurn1Pos2 = <<104.696365,-567.518005,35.669853>>
FLOAT fLeftTurn1Width = 12.000000

VECTOR vLeftTurn2Pos1 = <<124.082230,-38.153778,66.361267>>
VECTOR vLeftTurn2Pos2 = <<100.992325,-11.908267,71.929619>>
FLOAT fLeftTurn2Width = 17.000000

VECTOR vLeftTurn3Pos1 = <<-28.279661,32.393726,70.740288>>
VECTOR vLeftTurn3Pos2 = <<-51.275455,24.574474,76.143532>>
FLOAT fLeftTurn3Width = 17.000000

VECTOR vRightTurnPos1 = <<90.541054,-482.089081,33.695477>>
VECTOR vRightTurnPos2 = <<-58.224949,-524.614990,44.399258>>
FLOAT fRightTurnWidth = 17.000000


FUNC BOOL IS_FOLLOW_REMINDER_NEEDED(TEXT_LABEL_23 &root, TEXT_LABEL_23 &label, BOOL bPlayerInFront = FALSE)

	BOOL bReturn
	
	IF IS_VEHICLE_DRIVEABLE(vehJanitor)
	AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
	
		FLOAT fClose = 7
		FLOAT fFar = 100
		FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehPlayerCar, FALSE), GET_ENTITY_COORDS(vehJanitor, FALSE))
		root = ""
		label = ""
		
//		IF bInfrontDone
//		AND (GET_GAME_TIMER()-iTimeInfrontDone) > 20000
//			bInfrontDone = FALSE
//		ENDIF
		
		IF bPlayerInFront
		AND NOT bInFrontDone
			bReturn = TRUE
			root = "FBS1_FRONT"
			bInfrontDone = TRUE
//			iTimeInfrontDone = GET_GAME_TIMER()
		ELIF HAS_PLAYER_DRIVEN_ERRATICALLY()
			bReturn = TRUE
			root = "FBS1_CALM"
		ELIF fDist < fClose
		AND NOT bPlayerInFront
			bReturn = TRUE
			root = "FBS1_CLOSE"
		ELIF fDist > fFar
		AND NOT bPlayerInFront
			bReturn = TRUE
			root = "FBS1_FAR"
		ELSE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF fDist < 100
				AND fDist > 40
				AND NOT IS_ENTITY_OCCLUDED(vehJanitor)
					IF NOT bLeftTurn[0]
						IF IS_ENTITY_IN_ANGLED_AREA(vehJanitor, vLeftTurn1Pos1, vLeftTurn1Pos2, fLeftTurn1Width)
							root = "FBS1_LTURN"
							label = "FBS1_LTURN_1"
							bReturn = TRUE
						ENDIF
					ENDIF
					IF NOT bLeftTurn[1]
						IF IS_ENTITY_IN_ANGLED_AREA(vehJanitor, vLeftTurn2Pos1, vLeftTurn2Pos2, fLeftTurn2Width)
							root = "FBS1_LTURN"
							label = "FBS1_LTURN_2"
							bReturn = TRUE
						ENDIF
					ENDIF
					IF NOT bLeftTurn[2]
						IF IS_ENTITY_IN_ANGLED_AREA(vehJanitor, vLeftTurn3Pos1, vLeftTurn3Pos2, fLeftTurn3Width)
							root = "FBS1_LTURN"
							label = "FBS1_LTURN_3"
							bReturn = TRUE
						ENDIF
					ENDIF
					IF NOT bRightTurn
						IF IS_ENTITY_IN_ANGLED_AREA(vehJanitor, vRightTurnPos1, vRightTurnPos2, fRightTurnWidth)
							root = "FBS1_RTURN"
							label = "FBS1_RTURN_1"
							bReturn = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN bReturn
	
ENDFUNC


PROC MANAGE_CONVERSATION_FOR_CHASE(STRING sLabel, BOOL bDoReply = FALSE)

	IF IS_VEHICLE_DRIVEABLE(vehJanitor)
	AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
	AND NOT IS_PED_INJURED(pedJanitor)
	AND NOT IS_PED_INJURED(pedLester)
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "FBIs1UBER")
	
		FLOAT fPlayerProg = GET_PROGRESS_ALONG_ROUTE_HOME(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))					//GET_TIME_IN_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(vehPlayerCar), 1, "FBIs1UBER", path1)
		FLOAT fJanitorProg = GET_PROGRESS_ALONG_ROUTE_HOME(GET_ENTITY_COORDS(vehJanitor, FALSE))					//GET_TIME_IN_RECORDING_FROM_POSITION(GET_ENTITY_COORDS(vehJanitor), 1, "FBIs1UBER", path1)
		BOOL bPlayerInFront = fPlayerProg > fJanitorProg
		
		#IF IS_DEBUG_BUILD
			IF bPlayerInFront
				DRAW_DEBUG_TEXT_2D("PLAYER IN FRONT", <<0.85, 0.2, 0>>)
			ELSE
				DRAW_DEBUG_TEXT_2D("PLAYER BEHIND", <<0.85, 0.2, 0>>)
			ENDIF
		#ENDIF
		
		IF NOT in_car_conv_started
		AND NOT bDoReply
		AND NOT IS_STRING_NULL_OR_EMPTY(sLabel)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				IF CREATE_CONVERSATION(sSpeech, "FBS1AUD", sLabel, CONV_PRIORITY_HIGH)
					PRINTLN("IN CAR CONV STARTED")
					in_car_conv_started = TRUE
					follow_conv_on_hold = FALSE
				ENDIF
			ENDIF
		ELSE
			IF NOT follow_conv_on_hold
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						followResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
						follow_conv_on_hold = TRUE
						bFollowReminderDue = FALSE
						KILL_FACE_TO_FACE_CONVERSATION()
						PRINTLN("IN CAR CONV STOPPED OUT OF VEHICLE")
					ENDIF
				ELSE
					IF NOT bFollowReminderDue
						IF IS_FOLLOW_REMINDER_NEEDED(tempRoot, tempLabel, bPlayerInFront)
						AND (GET_GAME_TIMER() - iTimeOfLastWarning) > iMinWarningTime
							bFollowReminderDue = TRUE
							iTimeFollowReminder = GET_GAME_TIMER()
							PRINTLN("FOLLOW REMINDER DUE")
						ENDIF
					ENDIF
					IF bFollowReminderDue 
						IF GET_GAME_TIMER() - iTimeFollowReminder < 3000
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								followResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
								KILL_FACE_TO_FACE_CONVERSATION()
								PRINTLN("KILLING CONVERSATION FOR FOLLOW REMINDER")
							ENDIF
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF bFollowReminderDue
									IF NOT ARE_STRINGS_EQUAL(tempRoot, "FBS1_LTURN")
									AND NOT ARE_STRINGS_EQUAL(tempRoot, "FBS1_RTURN")
									AND NOT IS_STRING_NULL(tempRoot)								
										IF bDoReply
											IF CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(sSpeech, "FBS1AUD", tempRoot, tempRoot, "FBS1_MRESP", "FBS1_MRESP", CONV_PRIORITY_HIGH)
											
												IF ARE_STRINGS_EQUAL(tempRoot, "FBS1_FRONT")
												OR ARE_STRINGS_EQUAL(tempRoot, "FBS1_CLOSE")
													bFollowAchievment = FALSE
												ENDIF
												
												follow_conv_on_hold = TRUE
												bFollowReminderDue = FALSE
												iTimeOfLastWarning = GET_GAME_TIMER()
											ENDIF
										ELSE
											IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "FBS1AUD", tempRoot, tempLabel, CONV_PRIORITY_VERY_HIGH)
											
												IF ARE_STRINGS_EQUAL(tempRoot, "FBS1_FRONT")
												OR ARE_STRINGS_EQUAL(tempRoot, "FBS1_CLOSE")
													bFollowAchievment = FALSE
												ENDIF
											
												follow_conv_on_hold = TRUE
												bFollowReminderDue = FALSE
												iTimeOfLastWarning = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ELSE
										IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "FBS1AUD", tempRoot, tempLabel, CONV_PRIORITY_VERY_HIGH)
											//A quick check to see if Lester is shouting directions as I only want him to do this once per turn
											IF ARE_STRINGS_EQUAL(tempLabel, "FBS1_LTURN_1")
												bLeftTurn[0] = TRUE
											ENDIF
											IF ARE_STRINGS_EQUAL(tempLabel, "FBS1_LTURN_2")
												bLeftTurn[1] = TRUE
											ENDIF
											IF ARE_STRINGS_EQUAL(tempLabel, "FBS1_LTURN_3")
												bLeftTurn[2] = TRUE
											ENDIF
											IF ARE_STRINGS_EQUAL(tempLabel, "FBS1_RTURN_1")
												bRightTurn = TRUE
											ENDIF
											follow_conv_on_hold = TRUE
											bFollowReminderDue = FALSE
											iTimeOfLastWarning = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							bFollowReminderDue = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF follow_conv_on_hold
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					IF NOT bDoReply
						IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, "FBS1AUD", sLabel, followResumeLabel, CONV_PRIORITY_HIGH)
							PRINTLN("RESUMING CONVERSATION AFTER REMINDER")
							follow_conv_on_hold = FALSE
						ENDIF
					ELSE
						follow_conv_on_hold = FALSE
					ENDIF
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF follow_conv_on_hold
					DRAW_DEBUG_TEXT_2D("CONVERSATIONS HELD", <<0.1,0.7,0>>)
				ELSE
					DRAW_DEBUG_TEXT_2D("CONVERSATIONS FREE", <<0.1,0.7,0>>)
				ENDIF
			#ENDIF
			
		ENDIF
		
	ENDIF
	
ENDPROC

FLOAT fAlarmtime

PROC DO_PARKED_CAR_THING()

	INT iTemp
	
	IF NOT DOES_ENTITY_EXIST(CarParkDummies[0])
	OR NOT DOES_ENTITY_EXIST(CarParkDummies[1])
	OR NOT DOES_ENTITY_EXIST(CarParkDummies[2])
	OR NOT DOES_ENTITY_EXIST(CarParkDummies[3])
	OR NOT DOES_ENTITY_EXIST(CarParkDummies[4])
	
		VECTOR vSpaces[5]
		FLOAT fSpaces[5]
		
		vSpaces[0] = << 159.2625, -683.4668, 32.1298 >>
		vSpaces[1] = << 174.8298, -706.0013, 32.1304 >>
		vSpaces[2] = << 115.1112, -734.7641, 32.1331 >>
		vSpaces[3] = << 130.1378, -702.7861, 32.1246 >>
		vSpaces[4] = << 118.7168, -698.4962, 32.1258 >>
		
		fSpaces[0] = 336.4295
		fSpaces[1] = 247.5462
		fSpaces[2] = 339.0851
		fSpaces[3] = 160.7991
		fSpaces[4] = 342.2677
		
		REPEAT COUNT_OF(CarParkDummies) iTemp
		
			IF NOT DOES_ENTITY_EXIST(CarParkDummies[iTemp])
			
				MODEL_NAMES mnTemp
				
				IF iTemp < 4
					mnTemp = car_models[iTemp]
				ELSE
					mnTemp = modelJanitorVeh
				ENDIF
				
				REQUEST_MODEL(mnTemp)
				
				IF HAS_MODEL_LOADED(mnTemp)
				
					CarParkDummies[iTemp] = CREATE_VEHICLE(mnTemp, vSpaces[iTemp], fSpaces[iTemp])
					SET_VEHICLE_ON_GROUND_PROPERLY(CarParkDummies[iTemp])
					SET_VEHICLE_ALARM(CarParkDummies[iTemp], TRUE)
					SET_VEHICLE_DOORS_LOCKED(CarParkDummies[iTemp], VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
					SET_VEHICLE_AUTOMATICALLY_ATTACHES(CarParkDummies[iTemp], FALSE)
					
					IF iTemp = 0
						GET_VEHICLE_COLOURS(CarParkDummies[iTemp], colours[0], colours[1])
					ELIF iTemp = 1
						GET_VEHICLE_COLOURS(CarParkDummies[iTemp], colours[2], colours[3])
					ELIF iTemp = 2
						GET_VEHICLE_COLOURS(CarParkDummies[iTemp], colours[4], colours[5])
					ELIF iTemp = 3
						GET_VEHICLE_COLOURS(CarParkDummies[iTemp], colours[6], colours[7])
					ENDIF
					
					IF iTemp = 4
						SET_VEHICLE_COLOURS(CarParkDummies[iTemp], 35, 35)
						SET_VEHICLE_EXTRA_COLOURS(CarParkDummies[iTemp], 33, 33)
						SET_VEHICLE_NUMBER_PLATE_TEXT(CarParkDummies[iTemp], lpJanitor)
						SET_VEHICLE_AS_RESTRICTED(CarParkDummies[iTemp], iTemp)
					ENDIF
					
				ENDIF
				
			ENDIF
			
		ENDREPEAT
		
	ENDIF
	
	REPEAT COUNT_OF(CarParkDummies) iTemp
		IF DOES_ENTITY_EXIST(CarParkDummies[iTemp])
			IF IS_VEHICLE_DRIVEABLE(CarParkDummies[iTemp])
			
				IF IS_VEHICLE_ALARM_ACTIVATED(CarParkDummies[iTemp])
					fAlarmtime += GET_FRAME_TIME()
				ENDIF
				
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(CarParkDummies[iTemp], FALSE) + <<0,0,1.5>>, 0, 255, 0)
				IF NOT IS_VEHICLE_DRIVEABLE(CarParkDummies[iTemp])
				OR fAlarmtime > 3.0
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(CarParkDummies[iTemp], PLAYER_PED_ID())
						PRINTLN("Car park dummy damaged by player", iTemp)
					ENDIF
					
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(CarParkDummies[iTemp])
						PRINTLN("Car park dummy damaged by vehicle", iTemp)
					ENDIF
					
					IF IS_VEHICLE_ALARM_ACTIVATED(CarParkDummies[iTemp])
						PRINTLN("Car park dummy alarm active ", iTemp)
					ENDIF
					
					Mission_Failed("S1_SECFAIL")
				ENDIF
			ELSE
				PRINTLN("Car park dummy broken", iTemp)
				Mission_Failed("S1_SECFAIL")
			ENDIF
		ENDIF
	ENDREPEAT
	
ENDPROC

VECTOR vReplayCoords
FLOAT fReplayHeading

PROC PRE_INIT()

	#IF IS_DEBUG_BUILD
		skipMenuOptions[0].sTxtLabel = "ah_1_int"
		skipMenuOptions[1].sTxtLabel = "Go to FBI office"
		skipMenuOptions[2].sTxtLabel = "Stakeout garage"
		skipMenuOptions[3].sTxtLabel = "Follow Janitor in car"
		skipMenuOptions[4].sTxtLabel = "Follow Janitor on foot"
		skipMenuOptions[5].sTxtLabel = "ah_1_mcs_1"
		skipMenuOptions[6].sTxtLabel = "Get back in the car"
	#ENDIF
	
	IF IS_RADAR_HIDDEN()
		DISPLAY_RADAR(TRUE)
	ENDIF
	
	sbiGarmentFactory = ADD_SCENARIO_BLOCKING_AREA(<<714.3340, -963.6519, 29.3953>> - <<20, 20, 0>>, <<714.3340, -963.6519, 29.3953>> + <<20, 20, 10>>)
	
	REQUEST_ADDITIONAL_TEXT("FBS1AUD", MISSION_DIALOGUE_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("H1SET1", MISSION_TEXT_SLOT)
	
	ADD_PED_FOR_DIALOGUE(sSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
	
	//couple of pre-requests to speed up inital kick off
	REQUEST_MODEL(PROP_LD_TEST_01)
	REQUEST_MODEL(TAILGATER)
	
	REQUEST_SCRIPT_AUDIO_BANK("FBI_Heist_1_Tail_Janitor_01")
	
	IF Is_Replay_In_Progress()
	
		DISPLAY_RADAR(TRUE)
		FADE_OUT()
		
		WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
			WAIT(0)
		ENDWHILE
		
		INIT_TEXT()
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)+<<0,0,2>>)
		ENDIF
		
		INT iStage = GET_REPLAY_MID_MISSION_STAGE()
		IF g_bShitskipAccepted = TRUE
			iStage++
        ENDIF
				
		IF iStage > 4
			vReplayCoords = <<714.4953, -964.8629, 29.3953>>
			fReplayHeading = 188.1813
			SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
			WHILE NOT HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())	
				WAIT(0)
			ENDWHILE
			SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
			RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
			GO_TO_STAGE(STAGE_DRIVE_BACK_TO_SAFEHOUSE)
			iStageProgress = 10
			REQUEST_CUTSCENE("ah_1_ext_t6")
		ELSE
			bSkip = TRUE
			IF iStage = 0
				vReplayCoords = vPlayerStartPosition
				fReplayHeading = fPlayerStartHeading
				CREATE_VEHICLE_FOR_REPLAY(vehPlayerCar, vPlayerCarCoords, fPlayerCarHeading, FALSE, FALSE, FALSE, FALSE, TRUE)
				GO_TO_STAGE(STAGE_GO_TO_FIB)
			ENDIF
			IF iStage = 1
				vReplayCoords = v_OfficeCoords
				fReplayHeading = f_office_heading
				CREATE_VEHICLE_FOR_REPLAY(vehPlayerCar, v_OfficeCoords, f_office_heading, FALSE, FALSE, FALSE, FALSE, TRUE)
				GO_TO_STAGE(STAGE_WAIT_FOR_JANITOR)
			ENDIF
			IF iStage = 2
				vReplayCoords = v_OfficeCoords
				fReplayHeading = f_office_heading
				CREATE_VEHICLE_FOR_REPLAY(vehPlayerCar, v_OfficeCoords, f_office_heading, FALSE, FALSE, FALSE, FALSE, TRUE)
				GO_TO_STAGE(STAGE_TAIL_THE_JANITOR)
			ENDIF
			IF iStage = 3
				vReplayCoords = << -53.0454, 18.6296, 71.1259 >>
				fReplayHeading = 167.0193
				GO_TO_STAGE(STAGE_FOLLOW_JANITOR_ON_FOOT)
			ENDIF
			IF iStage = 4
				IF g_bShitskipAccepted
					vReplayCoords = << -105.0312, -8.6584, 69.5201 >>
					fReplayHeading = 259.2958
					PRINTLN("Going to janitor scene as shitskip is accepted")
					GO_TO_STAGE(STAGE_JANITOR_CUT)	
				ELSE
					vReplayCoords = << -105.0312, -8.6584, 69.5201 >>
					fReplayHeading = 259.2958
					GO_TO_STAGE(STAGE_DRIVE_BACK_TO_SAFEHOUSE)
				ENDIF
			ENDIF
		ENDIF
		
		PRINTLN("REPLAY SETUP: Started replay setup, position - ", vReplayCoords, " heading - ", fReplayHeading)
		
		START_REPLAY_SETUP(vReplayCoords, fReplayHeading)
		
		CLEAR_AREA(vReplayCoords, 1000, TRUE)
		WASH_DECALS_IN_RANGE(vReplayCoords, 1000, 1)
		CLEAR_PED_WETNESS(PLAYER_PED_ID())
		
	ENDIF
	
	IF IS_REPEAT_PLAY_ACTIVE()
		SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR, BUILDINGSTATE_DESTROYED)
		SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR, BUILDINGSTATE_DESTROYED) 
		SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR, BUILDINGSTATE_DESTROYED) 
		SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_BURNT, BUILDINGSTATE_NORMAL) 
		SET_BUILDING_STATE(BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS, BUILDINGSTATE_NORMAL)
		SET_DOOR_STATE(DOORNAME_SWEATSHOP_L, DOORSTATE_UNLOCKED)
		SET_DOOR_STATE(DOORNAME_SWEATSHOP_R, DOORSTATE_UNLOCKED)
	ENDIF
	
	car_models[0] = INGOT
	car_models[1] = INTRUDER
	car_models[2] = PICADOR
	car_models[3] = ASTEROPE
	
	vLPOffsets[0] = <<0, -2.3123, 0.0297>>
	vLPOffsets[1] = <<0, -2.4781, -0.1779>>
	vLPOffsets[2] = <<0, -2.7685, -0.3293>>
	vLPOffsets[3] = <<0, -2.3374, 0.2962>>
	
	INIT_JANITOR_ROUTE()
	
	INT iTemp
	REPEAT COUNT_OF(colours) iTemp
		colours[iTemp] = -1
	ENDREPEAT
	
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	
	ASSISTED_MOVEMENT_REQUEST_ROUTE("Aprtmnt_1")
	ADD_RELATIONSHIP_GROUP("NEUTRAL PEDS", neutrals)
	ADD_RELATIONSHIP_GROUP("BUDDIES", rghBuddy)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, rghBuddy, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE, RELGROUPHASH_COP, rghBuddy)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rghBuddy, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, rghBuddy)
	
	DISABLE_TAXI_HAILING(TRUE)
	
	VEHICLE_INDEX vehTemp
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	ELSE
		vehTemp = GET_LAST_DRIVEN_VEHICLE()
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehTemp)
	AND IS_VEHICLE_DRIVEABLE(vehTemp)
		IF NOT IS_VEHICLE_MODEL(vehTemp, TAXI)
			IF VDIST(GET_ENTITY_COORDS(vehTemp, FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) < 50
			AND NOT IS_VEHICLE_MODEL_ON_BLACKLIST(GET_ENTITY_MODEL(vehTemp))
				SET_ENTITY_AS_MISSION_ENTITY(vehTemp, TRUE, TRUE)
				vehPlayerCar = vehTemp
				vehTemp = NULL
				SET_ENTITY_COORDS(vehPlayerCar, vPlayerCarCoords)
				SET_ENTITY_HEADING(vehPlayerCar, fPlayerCarHeading)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCar)
			ENDIF
		ENDIF
	ENDIF
	
	CLEAR_AREA(vApartmentBlockAreaPos1, 100, TRUE)
	CLEAR_AREA(vGarageBlockAreaPos1, 100, TRUE)
	SET_NUMBER_OF_PARKED_VEHICLES(0)
	FULLY_BLOCK_AREA(sbiGarage, vGarageBlockAreaPos1, vGarageBlockAreaPos2)
	FULLY_BLOCK_AREA(sbiApartment, vApartmentBlockAreaPos1, vApartmentBlockAreaPos2)
	
	sbiDrilling = ADD_SCENARIO_BLOCKING_AREA(<<58.578835,-702.402710,32.253922>> - <<5.500000,9.750000,3.000000>>, <<58.578835,-702.402710,32.253922>>+<<5.500000,9.750000,3.000000>>)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-83.2189, -23.8057, 65.3210>> + <<5,5,3>>, <<-83.2189, -23.8057, 65.3210>> - <<5,5,3>>, FALSE)
	
	sbiAlley =  ADD_SCENARIO_BLOCKING_AREA(<<-85.141922,-11.761818,66.616379>> - <<16.250000,6.500000, 0.0000>>, <<-85.141922,-11.761818,66.616379>> + <<16.250000,6.500000, 2.0000>>, FALSE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(modelJanitorVeh, TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(INTRUDER, TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(S_M_M_JANITOR, TRUE)
	
	REMOVE_PARTICLE_FX_IN_RANGE(vPlayerCarCoords, 40)
	
	IF NOT Is_Replay_In_Progress()
	AND mission_stage <> STAGE_RECORD_CAMERA_CARS
	AND mission_stage <> STAGE_UBER_RECORD
	AND mission_stage <> STAGE_RECORD_SET_PIECE
	AND mission_stage <> STAGE_TEST
		REQUEST_ASSETS_FOR_STAGE(STAGE_GO_TO_FIB)
		GO_TO_STAGE(STAGE_INTRO_CUT)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
		SET_DEBUG_TEXT_VISIBLE(FALSE)
	#ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD

	PROC jSkip()
	
		IF IS_CUTSCENE_ACTIVE()
		AND IS_CUTSCENE_PLAYING()
			STOP_CUTSCENE()
			REMOVE_CUTSCENE()
			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
			bSkip = TRUE
		ENDIF
		
		IF mission_stage = STAGE_DRIVE_BACK_TO_SAFEHOUSE
			IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
			ENDIF
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			SAFELY_CREATE_PLAYER_VEHICLE(vEndPoint, 87.7990)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()			
		ELSE
			IF mission_stage < STAGE_JANITOR_CUT
				KILL_ANY_CONVERSATION()
				CLEAR_PRINTS()
				bSkip = TRUE
				GO_TO_STAGE(INT_TO_ENUM(MISSION_STAGE_FLAG, ENUM_TO_INT(mission_stage)+1))
			ENDIF
		ENDIF
	ENDPROC
	
	//Skip to Previous stage
	
	PROC pSkip()
	
		IF IS_CUTSCENE_ACTIVE()
		AND IS_CUTSCENE_PLAYING()
			STOP_CUTSCENE()
			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
			REMOVE_CUTSCENE()
		ENDIF
		
		IF mission_stage <> STAGE_FOLLOW_JANITOR_ON_FOOT
		AND mission_stage <> STAGE_GO_TO_FIB
			KILL_ANY_CONVERSATION()
			CLEAR_PRINTS()
			bSkip = TRUE
			GO_TO_STAGE(INT_TO_ENUM(MISSION_STAGE_FLAG, ENUM_TO_INT(mission_stage)-1))
		ENDIF
		
		IF MISSION_STAGE = STAGE_DRIVE_BACK_TO_SAFEHOUSE
			KILL_ANY_CONVERSATION()
			CLEAR_PRINTS()
			bSkip = TRUE
			GO_TO_STAGE(STAGE_FOLLOW_JANITOR_ON_FOOT)
		ELIF MISSION_STAGE = STAGE_FOLLOW_JANITOR_ON_FOOT
			KILL_ANY_CONVERSATION()
			CLEAR_PRINTS()
			bSkip = TRUE
			GO_TO_STAGE(STAGE_TAIL_THE_JANITOR)
		ELIF MISSION_STAGE = STAGE_GO_TO_FIB
			KILL_ANY_CONVERSATION()
			CLEAR_PRINTS()
			bSkip = TRUE
			GO_TO_STAGE(STAGE_GO_TO_FIB)
		ENDIF
		
	ENDPROC
	
	PROC DEBUG_CUTSCENE_CONTROL()
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		//OR IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
			STOP_CUTSCENE()
			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			STOP_CUTSCENE()
			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
		ENDIF
		
	ENDPROC
	
#ENDIF

BOOL bCompsGrabbed = FALSE
BOOL bGetin

PROC DO_INTRO()

	IF DOES_ENTITY_EXIST(vehPlayerCar)
	AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		REQUEST_VEHICLE_HIGH_DETAIL_MODEL(vehPlayerCar)
	ENDIF
	
	IF NOT bGetin
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
	ENDIF
	
	SWITCH iStageProgress
	
		CASE 0
		
//			REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_STEVE))
//			REQUEST_MODEL(GET_NPC_PED_MODEL(CHAR_DAVE))
//			REQUEST_MODEL(GET_NPC_VEH_MODEL(CHAR_DAVE))
			
			IF bSkip
			OR IS_REPEAT_PLAY_ACTIVE()
			
				WHILE NOT SAFELY_CREATE_PLAYER_VEHICLE(vPlayerCarCoords, fPlayerCarHeading)
					PRINTLN("Creating player vehicle")
					WAIT(0)
				ENDWHILE
				
				DO_PLAYER_MAP_WARP_WITH_LOAD(vPlayerCarCoords, fPlayerStartHeading, FALSE)
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerCarCoords)
				FORCE_RESET_LESTER()
				bSkip = FALSE
				iStageProgress = -1
				
			ELSE
			
				IF NOT DOES_ENTITY_EXIST(objChair)
				OR NOT DOES_ENTITY_EXIST(objStick)
				OR NOT DOES_ENTITY_EXIST(pedLester)
				
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
					AND NOT DOES_ENTITY_EXIST(pedLester)
						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
						pedLester = g_sTriggerSceneAssets.ped[0]
						g_sTriggerSceneAssets.ped[0] = NULL
						PRINTLN("GRABBED LESTER PED")
					ENDIF
					
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
					AND NOT DOES_ENTITY_EXIST(objChair)
						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.object[0], TRUE, TRUE)
						objChair = g_sTriggerSceneAssets.object[0]
						g_sTriggerSceneAssets.object[0] = NULL
						PRINTLN("GRABBED CHAIR")
					ENDIF
					
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[1])
					AND NOT DOES_ENTITY_EXIST(objStick)
						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.object[1], TRUE, TRUE)
						objStick = g_sTriggerSceneAssets.object[1]
						g_sTriggerSceneAssets.object[1] = NULL
						PRINTLN("GRABBED STICK")
					ENDIF
										
				ELSE
				
					MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_AGENCY_1)
					iStageProgress++
					
				ENDIF
				
			ENDIF
						
		BREAK
		
		CASE -1
					
			iStageProgress = 1
			
		BREAK
		
		CASE 1
		
			IF GET_PED_PARACHUTE_STATE(PLAYER_PED_ID()) = PPS_INVALID
				REQUEST_CUTSCENE(sIntroName)
				IF NOT bCompsGrabbed
					IF DOES_ENTITY_EXIST(pedLester)
					AND NOT IS_PED_INJURED(pedLester)
						IF IS_CUTSCENE_ACTIVE()
							IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
								SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Lester", pedLester)
								bCompsGrabbed = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			
				IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE(sIntroName)
				
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					
						IF NOT DOES_ENTITY_EXIST(pedLester)
							REGISTER_ENTITY_FOR_CUTSCENE(pedLester, "Lester", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_NPC_PED_MODEL(CHAR_LESTER))
						ELIF NOT IS_PED_INJURED(pedLester)
							REGISTER_ENTITY_FOR_CUTSCENE(pedLester, "Lester", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
												
						START_CUTSCENE()
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						REMOVE_ANIM_DICT("missheistfbisetup1leadinoutah_1_int")
						iStageProgress++
						
					ELSE
					
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_LEAVE_ANY_VEHICLE) != PERFORMING_TASK
							TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
						ENDIF
						
					ENDIF
					
				ELSE
				
					REMOVE_CUTSCENE()
					
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 2
			INT iTemp
			REPEAT COUNT_OF(g_sTriggerSceneAssets.object) iTemp
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[iTemp])
					SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.object[iTemp], TRUE, TRUE)
					DELETE_OBJECT(g_sTriggerSceneAssets.object[iTemp])
				ENDIF
			ENDREPEAT
			IF DOES_ENTITY_EXIST(objChair)
				DELETE_OBJECT(objChair)
			ENDIF
			IF DOES_ENTITY_EXIST(objStick)
				DELETE_OBJECT(objStick)
			ENDIF
			//CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10, TRUE)
			iStageProgress++
		BREAK
		
		CASE 3
		
			IF IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(500)
			ENDIF
			
			STOP_GAMEPLAY_HINT()
			
			IF NOT DOES_ENTITY_EXIST(vehPlayerCar)
				PRINTLN("\n\n\n\n\n\n\n\n\n\n\n\n\n RESOLVING PLAYER VEH \n\n\n\n\n\n\n\n\n\n\n")
				IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
				AND IS_VEHICLE_DRIVEABLE(GET_PLAYERS_LAST_VEHICLE())
					VEHICLE_INDEX vehTemp
					vehTemp = GET_PLAYERS_LAST_VEHICLE()
					SET_ENTITY_AS_MISSION_ENTITY(vehTemp, TRUE, TRUE)
					
					BOOL bPlayerTookTaxiRide
					
					IF IS_VEHICLE_A_TAXI_MODEL(vehTemp)
						IF GET_PED_IN_VEHICLE_SEAT(vehTemp, VS_DRIVER) != PLAYER_PED_ID()
						AND GET_PED_IN_VEHICLE_SEAT(vehTemp, VS_DRIVER) != NULL
							bPlayerTookTaxiRide = TRUE
						ENDIF
					ENDIF
					
					IF NOT IS_BIG_VEHICLE(vehTemp)
					AND NOT bPlayerTookTaxiRide
						SET_MISSION_VEHICLE_GEN_VEHICLE(vehTemp, <<702.5282, -980.0775, 23.1412>>, 225.0234)
					ENDIF
					DELETE_VEHICLE(vehTemp)
				ENDIF
				CLEAR_AREA(vPlayerCarCoords, 100, TRUE)
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MISSION_VEH)
				WHILE NOT SAFELY_CREATE_PLAYER_VEHICLE(vPlayerCarCoords, fPlayerCarHeading)
					WAIT(0)
				ENDWHILE
			ELSE
				CLEAR_AREA(vPlayerCarCoords, 100, TRUE)
			ENDIF
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SKIP_CUTSCENE)
			
			IF NOT DOES_ENTITY_EXIST(pedLester)
				PRINTLN("Lester DOESN'T EXIST")
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester"))
					pedLester = GET_PED_INDEX_FROM_ENTITY_INDEX((GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester")))
					SET_PED_AS_BUDDY(pedLester)
				ELSE
					PRINTLN("Cutscene Lester DOESN'T EXIST")
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedLester)
			AND DOES_ENTITY_EXIST(vehPlayerCar)
				iStageProgress++
			ENDIF
			
		BREAK
		
		CASE 4
		
			IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			
				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(vehPlayerCar, FALSE) + <<0,0,2>>, 0, 255, 0)
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					PRINTLN("Exit state hit for Mike and camera")
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 900)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lester")
					PRINTLN("Exit state hit for Lester")
					IF NOT IS_PED_INJURED(pedLester)
						PRINTLN("Lester is not injured")
						SET_PED_INTO_VEHICLE(pedLester, vehPlayerCar, VS_FRONT_RIGHT)
						SET_PED_AS_BUDDY(pedLester)
					ENDIF
				ENDIF
				
			ELSE
			
				PRINTLN("PLAYER CAR BROKEN.")
				
			ENDIF
			
			IF WAS_CUTSCENE_SKIPPED()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			
			
			IF NOT IS_CUTSCENE_ACTIVE()
				CLEAR_AREA(vApartmentBlockAreaPos1, 300, TRUE)
				CLEAR_AREA(vGarageBlockAreaPos1, 300, TRUE)
				SET_NUMBER_OF_PARKED_VEHICLES(0)
				IF NOT IS_PED_INJURED(pedLester)
				AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
					IF NOT IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
						SET_PED_INTO_VEHICLE(pedLester, vehPlayerCar, VS_FRONT_RIGHT)
						SET_PED_AS_BUDDY(pedLester)	
					ENDIF
				ENDIF
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				FADE_IN()
				CHECK_PEDS_FOR_DIALOGUE()
				SETTIMERA(0)
				REPLAY_STOP_EVENT()
				GO_TO_STAGE(STAGE_GO_TO_FIB)
			ENDIF
			
		BREAK
		
	ENDSWITCH
		
ENDPROC


BOOL bTurnRound
INT iTurnRoundLine
BOOL bArriveConv

PROC GO_TO_FIB()

	SET_PARKED_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
	
	IF DOES_ENTITY_EXIST(vehPlayerCar)
	
		IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
		AND iStageProgress < 2
			PRINTLN("car detail debug - SETTING DETAIL ON MODEL")
			REQUEST_VEHICLE_HIGH_DETAIL_MODEL(vehPlayerCar)
		ENDIF
		
	ENDIF
	
	IF NOT bGetin
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
	ENDIF
	
	SWITCH iStageProgress
	
		CASE 0
		
			IF bSkip
			
				IF NOT IS_REPLAY_BEING_SET_UP()
					DO_PLAYER_MAP_WARP_WITH_LOAD(vPlayerStartPosition, fPlayerStartHeading, FALSE)
				ENDIF
				
				WHILE NOT SAFELY_CREATE_PLAYER_VEHICLE(vPlayerCarCoords, fPlayerCarHeading)
					WAIT(0)
				ENDWHILE
				
				SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCar)
				IF NOT DOES_ENTITY_EXIST(pedLester)
					WHILE NOT CREATE_BUDDY_ON_FOOT (pedLester, CHAR_LESTER, vLesterStartPosition, fLesterStartHeading)
						WAIT(0)
					ENDWHILE
				ENDIF
				IF NOT IS_PED_INJURED(pedLester)
					SET_PED_INTO_VEHICLE(pedLester, vehPlayerCar, VS_FRONT_RIGHT)
				ENDIF
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP()
				ENDIF
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerStartPosition)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fPlayerStartHeading)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVEBLENDRATIO_WALK, 900)
				
//				TASK_ENTER_VEHICLE(PLAYER_PED_ID(), vehPlayerCar, DEFAULT_TIME_NEVER_WARP, VS_DRIVER, PEDMOVE_WALK)
				
				
				CHECK_PEDS_FOR_DIALOGUE()
				SET_PED_AS_BUDDY(pedLester)
				
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
				SET_PLAYERS_LAST_VEHICLE(vehPlayerCar)
			ENDIF
			
			blipPlayerCar = CREATE_BLIP_FOR_VEHICLE(vehPlayerCar)
//			bDaveFlee = FALSE
			bGetin = FALSE
			b_key_areas_cleared = FALSE
			bArriveConv = FALSE
			iConvStage = 0
			
			IF NOT IS_PED_INJURED(pedLester)
			AND IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
				PRINTLN("Setting replay stage to 0")
//				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "STAGE_GO_TO_FIB", FALSE, FALSE, pedLester, TRUE)
				Store_Game_State_Snapshot_For_Stage(0, "STAGE_GO_TO_FIB", pedLester, TRUE)
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
			
			iStageProgress++
			
		BREAK
		
		CASE 1
		
			FADE_IN()
			
			IF NOT IS_PED_INJURED(pedLester)
			AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					IF IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
						IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_GET_TO_FIB")
							START_AUDIO_SCENE("AGENCY_H_1_GET_TO_FIB")
						ENDIF
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						IF CREATE_CONVERSATION(sSpeech, "FBS1AUD", "FBS1_INTRO", CONV_PRIORITY_HIGH)
							SETTIMERA(0)
							iStageProgress++
						ENDIF
					ENDIF
				ELSE
					IF NOT bGetin
						PRINT_NOW("CMN_GENGETINY", DEFAULT_GOD_TEXT_TIME, 1)
						bGetin = TRUE
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 2
		
			IF TIMERA() > 7500
				MANAGE_VEHICLE_CONVERSATION("FBS1_TOFIB", vehPlayerCar)
			ENDIF
			
			SWITCH iConvStage
				CASE 0
					IF in_car_conv_started
					AND NOT in_car_conv_on_hold
						TEXT_LABEL_23 sConv
						sConv = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND ARE_STRINGS_EQUAL(sConv, "FBS1_TOFIB")
							iConvstage++
						ENDIF
					ENDIF
				BREAK
				CASE 1
					REQUEST_MODEL(PROP_NPC_PHONE)
					REQUEST_FAKE_CELLPHONE_MOVIE(sfLestPhoneScreen)
					REQUEST_ANIM_DICT("missheist_agency2aig_2")
					IF HAS_MODEL_LOADED(PROP_NPC_PHONE)
					AND HAS_FAKE_CELLPHONE_MOVIE_LOADED(sfLestPhoneScreen)
					AND HAS_ANIM_DICT_LOADED("missheist_agency2aig_2")
					AND NOT IS_PED_INJURED(pedLester)
						IF NOT DOES_ENTITY_EXIST(objLestPhone)
							objLestPhone = CREATE_OBJECT(PROP_NPC_PHONE, GET_PED_BONE_COORDS(pedLester, BONETAG_PH_L_HAND, <<0,0,0>>))
							DRAW_FAKE_CELLPHONE_SCREEN(sfLestPhoneScreen, FALSE, FAKE_CELLPHONE_SCREEN_BADGER_MENU)
							ATTACH_ENTITY_TO_ENTITY(objLestPhone, pedLester, GET_PED_BONE_INDEX(pedLester, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, TRUE)
							TASK_PLAY_ANIM(pedLester, "missheist_agency2aig_2", "look_at_phone_a", REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_UPPERBODY|AF_SECONDARY|AF_LOOPING)
							TASK_LOOK_AT_ENTITY(pedLester, objLestPhone, -1)
							iConvStage++
						ENDIF
					ENDIF
				BREAK
				CASE 2
					IF in_car_conv_started
					AND NOT in_car_conv_on_hold
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							iConvstage++
						ENDIF
					ENDIF
				BREAK
				CASE 3
					IF NOT IS_PED_INJURED(pedLester)
						IF IS_ENTITY_PLAYING_ANIM(pedLester, "missheist_agency2aig_2", "look_at_phone_a")
							FLOAT fTemp
							fTemp = GET_ENTITY_ANIM_CURRENT_TIME(pedLester, "missheist_agency2aig_2", "look_at_phone_a")
							CLEAR_PED_TASKS(pedLester)
							TASK_PLAY_ANIM(pedLester, "missheist_agency2aig_2", "look_at_phone_a", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_UPPERBODY|AF_SECONDARY, fTemp)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(pedLester)
							TASK_CLEAR_LOOK_AT(pedLester)
							SETTIMERA(0)
							iConvStage++
						ENDIF
					ENDIF
				BREAK
				CASE 4
					IF NOT IS_PED_INJURED(pedLester)
					AND NOT IS_ENTITY_PLAYING_ANIM(pedLester, "missheist_agency2aig_2", "look_at_phone_a")
					AND DOES_ENTITY_EXIST(objLestPhone)
					AND TIMERA() > 1000
						SETTIMERA(0)
						DELETE_OBJECT(objLestPhone)
						RELEASE_FAKE_CELLPHONE_MOVIE(sfLestPhoneScreen)
						RELEASE_NPC_PHONE_RENDERTARGET()
						iConvStage++
					ENDIF
				BREAK
			ENDSWITCH
			
			BLIP_MANAGER_FOR_COORD(vehPlayerCar, blipPlayerCar, v_OfficeCoords-0.5, blipOfficeFBI)
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
				IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
					IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
						PRINT_GOD_TEXT(gGoToFIB)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(pedLester)
			AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
			AND IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
			
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<72.25, -703.94, 30.53>>, <<15,15,g_vAnyMeansLocate.z>>, TRUE)
				
					VECTOR vParkForward
					VECTOR vPlayerForward
					
					vParkForward = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v_OfficeCoords, f_office_heading, <<0,1,0>>) - v_OfficeCoords
					vPlayerForward = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_ENTITY_COORDS(vehPlayerCar, FALSE), GET_ENTITY_HEADING(vehPlayerCar), <<0,1,0>>) - GET_ENTITY_COORDS(vehPlayerCar, FALSE)
					PRINTLN(GET_ANGLE_BETWEEN_2D_VECTORS(vParkForward.x, vParkForward.y, vPlayerForward.x, vPlayerForward.y))
					DRAW_DEBUG_LINE(v_OfficeCoords, v_OfficeCoords + vParkForward)
					DRAW_DEBUG_LINE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE) + vPlayerForward)
					
					IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<74.095825,-698.990295,30.530674>>, <<70.899979,-707.910522,32.529118>>, 5.250000, FALSE)
						IF NOT IS_ENTITY_UPSIDEDOWN(vehPlayerCar)
						AND CAN_PLAYER_START_CUTSCENE()
							IF ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vParkForward.x, vParkForward.y, vPlayerForward.x, vPlayerForward.y)) < 30
								REMOVE_BLIP(blipOfficeFBI)
								KILL_FACE_TO_FACE_CONVERSATION()
								iStageProgress++
							ELSE
								IF NOT bTurnRound
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FBS1_TURNRND", CONV_PRIORITY_HIGH)
											iTurnRoundLine = GET_GAME_TIMER()
											bTurnRound = TRUE
										ENDIF
									ENDIF
								ELSE
									IF (GET_GAME_TIMER() - iTurnRoundLine) > 15000
										bTurnRound = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 3
			IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayerCar, 5, 5)
					IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
						SETTIMERA(0)
						iStageProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF bArriveConv
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 5
			IF TIMERA() > 2000
				GO_TO_STAGE(STAGE_WAIT_FOR_JANITOR)
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF NOT bArriveConv
		IF iStageProgress > 2
			IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FBS1_ATFIB", CONV_PRIORITY_HIGH)
				REPLAY_RECORD_BACK_FOR_TIME(10.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				bArriveConv = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(vehPlayerCar)
		SAFELY_CREATE_PLAYER_VEHICLE(vPlayerCarCoords, fPlayerCarHeading)
	ENDIF
	
//	IF NOT IS_PED_INJURED(pedDave)
//	AND NOT IS_PED_INJURED(pedSteve)
//	AND IS_VEHICLE_DRIVEABLE(vehDave)
//	
//		IF NOT bDaveFlee
//			IF GET_SCRIPT_TASK_STATUS(pedDave, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
//			AND NOT IS_PED_FLEEING(pedDave)
//				SEQUENCE_INDEX seqTemp
//				OPEN_SEQUENCE_TASK(seqTemp)
//				TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehDave, <<757.2346, -978.3004, 24.2398>>, 10, DRIVINGSTYLE_NORMAL, GET_NPC_VEH_MODEL(CHAR_DAVE), DRIVINGMODE_STOPFORCARS, 3, 10)
//				TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehDave, <<764.5475, -961.9715, 24.9256>>, 10, DRIVINGSTYLE_NORMAL, GET_NPC_VEH_MODEL(CHAR_DAVE), DRIVINGMODE_STOPFORCARS, 10, 10)
//				TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehDave, <<777.6909, -921.1370, 24.4691>>, 10, DRIVINGSTYLE_NORMAL, GET_NPC_VEH_MODEL(CHAR_DAVE), DRIVINGMODE_STOPFORCARS, 10, 10)
//				TASK_VEHICLE_DRIVE_TO_COORD(NULL, vehDave, <<779.3174, -737.0959, 26.7278>>, 10, DRIVINGSTYLE_NORMAL, GET_NPC_VEH_MODEL(CHAR_DAVE), DRIVINGMODE_STOPFORCARS, 10, 10)
//				TASK_VEHICLE_DRIVE_WANDER(NULL, vehDave, 10, DRIVINGMODE_AVOIDCARS_OBEYLIGHTS)
//				CLOSE_SEQUENCE_TASK(seqTemp)
//				TASK_PERFORM_SEQUENCE(pedDave, seqTemp)
//				CLEAR_SEQUENCE_TASK(seqTemp)
//				SET_PED_AS_BUDDY(pedDave)
//				SET_PED_AS_BUDDY(pedSteve)			
//			ENDIF
//			
//			IF DOES_ENTITY_EXIST(vehDave)
//			AND DOES_ENTITY_EXIST(pedDave)
//			AND NOT IS_PED_INJURED(pedDave)
//				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehDave, PLAYER_PED_ID())
//					SET_PED_FLEE_ATTRIBUTES(pedDave, FA_USE_VEHICLE, TRUE)
//					CLEAR_PED_TASKS(pedDave)
//					TASK_SMART_FLEE_PED(pedDave, PLAYER_PED_ID(), 500, -1)
//					TASK_SMART_FLEE_PED(pedSteve, PLAYER_PED_ID(), 500, -1)
//					PRINTLN("IS FLEEING")
//					bDaveFlee = TRUE
//				ENDIF
//			ENDIF
//			
//		ENDIF
//		
//		
//		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(vehDave)) > POW(100, 2)
//		AND IS_ENTITY_OCCLUDED(vehDave)
//			DESTROY_DAVE_AND_STEVE_AND_THEIR_STUPID_LITTLE_CAR()
//		ENDIF
//	ENDIF
	
	DO_PARKED_CAR_THING()
	
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), v_OfficeCoords) < 300
		IF NOT b_key_areas_cleared
			CLEAR_AREA(v_OfficeCoords, 100, TRUE)
			CLEAR_ANGLED_AREA_OF_VEHICLES(v_Stakeout_area_Pos1, v_Stakeout_area_Pos2, f_Stakeout_area_Width)
			b_key_areas_cleared = TRUE
		ENDIF
	ELSE
		IF b_key_areas_cleared
			b_key_areas_cleared = FALSE
		ENDIF
	ENDIF
	
	IF NOT bRequestNextStage
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), v_OfficeCoords) < 100
			REQUEST_ASSETS_FOR_STAGE(STAGE_WAIT_FOR_JANITOR)
			bRequestNextStage = TRUE
		ENDIF
	ENDIF
	
ENDPROC

INT iDecoySpots = 0

ENUM STAKEOUT_PROG
	AHS1_SO_WAIT_FOR_CAR = 0,
	AHS1_SO_SPOT_CAR,
	AHS1_SO_IDENT_CAR,
	AHS1_SO_SUCCESS,
	AHS1_SO_FAIL,
	AHS1_SO_CONV,
	AHS1_SO_WAIT_FOR_END
ENDENUM

STAKEOUT_PROG iCarProgress = AHS1_SO_WAIT_FOR_CAR
BOOL bLockVehicleControls = FALSE
//BOOL bGameplayCamGrabbed = FALSE
CAM_VIEW_MODE eCamMode
BOOL bExitTopRampSound
BOOL bExitBottomRampSound

PROC WATCH_DECOYS()

	INT iTemp
	
	SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
	OR iStageProgress = 0
	
		FLOAT fNearClip = 0.2
		SWITCH iStageProgress
		
			CASE 0
			
				iCarProgress = AHS1_SO_WAIT_FOR_CAR
//				bGameplayCamGrabbed = FALSE				
				REQUEST_VEHICLE_RECORDING(1, "FBIs1UBER")
				
				IF bSkip
				
					REQUEST_COLLISION_AT_COORD(v_OfficeCoords)
					
					WHILE NOT SAFELY_CREATE_PLAYER_VEHICLE(v_OfficeCoords, f_office_heading, TRUE, TRUE)
						WAIT(0)
					ENDWHILE
					
					IF NOT IS_REPLAY_BEING_SET_UP()
						SET_FOCUS_POS_AND_VEL(v_OfficeCoords, <<0,0,0>>)
						NEW_LOAD_SCENE_START(v_OfficeCoords, <<COS(f_office_heading+90), SIN(f_office_heading+90), 0>>, 25)
						SETTIMERA(0)
						WHILE NOT IS_NEW_LOAD_SCENE_LOADED() AND TIMERA() < 10000
							WAIT(0)
						ENDWHILE
						CLEAR_FOCUS()
						NEW_LOAD_SCENE_STOP()
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
						SET_ENTITY_COORDS_NO_OFFSET(vehPlayerCar, <<72.6414, -702.4333, 31.1800>>)
						SET_ENTITY_QUATERNION(vehPlayerCar, 0.0019, 0.0174, -0.1740, 0.9846)
						SET_VEHICLE_ENGINE_ON(vehPlayerCar, TRUE, TRUE)
					ENDIF
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP(vehPlayerCar)
					ENDIF
					
					FORCE_RESET_LESTER()
					
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
				
					vehDummy = CREATE_VEHICLE(GET_ENTITY_MODEL(vehPlayerCar), <<72.6414, -702.4333, 31.1800>>)
					SET_ENTITY_QUATERNION(vehDummy, 0.0019, 0.0174, -0.1740, 0.9846)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehDummy)
					
					pedDummy = CREATE_PED_INSIDE_VEHICLE(vehDummy, PEDTYPE_MISSION, GET_PLAYER_PED_MODEL(CHAR_MICHAEL), VS_DRIVER)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedDummy, TRUE)
					
					FREEZE_ENTITY_POSITION(vehDummy, TRUE)
					SET_ENTITY_COLLISION(vehDummy, FALSE)
					SET_ENTITY_VISIBLE(vehDummy, FALSE)
					
					IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_GET_TO_FIB")
						STOP_AUDIO_SCENE("AGENCY_H_1_GET_TO_FIB")
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_WAIT_FOR_CARS")
						START_AUDIO_SCENE("AGENCY_H_1_WAIT_FOR_CARS")
					ENDIF
					
				ENDIF
				
				current_car = 0
				iDecoySpots = 0
				janitor_wait_time = 10000
				iCurrentStakeoutSpeech = 1
				iNumberOfDecoys = GET_RANDOM_INT_IN_RANGE(3,5)
				CHECK_PEDS_FOR_DIALOGUE()
				bLockVehicleControls = TRUE
				
				TRIGGER_MUSIC_EVENT("AH1_START")
				
				SETTIMERA(0)
				
				iStageProgress++
				
			BREAK
			
			CASE 1
			
				IF TIMERA() > 100
				
					IF NOT IS_PED_INJURED(pedDummy)
					
						eCamMode = GET_FOLLOW_VEHICLE_CAM_VIEW_MODE()
						PRINTLN("Grabbed cam mode as ", ENUM_TO_INT(eCamMode))
						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
							FADE_OUT()
							WHILE NOT IS_SCREEN_FADED_OUT()
								WAIT(0)
							ENDWHILE
						ENDIF
						SETUP_FIRST_PERSON()
						INIT_FIRST_PERSON_CAMERA(fpCam, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_PED_BONE_COORDS(pedDummy, BONETAG_HEAD, <<0,0,0>>), 339.9642, vCamOffset), vCamRot, 50)//44.178452)
						INIT_STAKEOUT_CAM(fpCam.theCam, TRUE)
						INIT_STAKEOUT_CAM(zoomCam, FALSE)
						IF DOES_CAM_EXIST(zoomCam)
							IF (IS_PC_VERSION() AND GET_ASPECT_RATIO(TRUE) > 1.8)
								// Reduce near clip for triple head since camera is clipping car window
								fNearClip = 0.075
							ENDIF						
							SET_CAM_NEAR_CLIP(zoomCam, fNearClip)
						ENDIF
						
						IF NOT IS_PED_INJURED(pedLester)
						AND IS_ENTITY_PLAYING_ANIM(pedLester, "missheist_agency2aig_2", "look_at_phone_a")
						AND DOES_ENTITY_EXIST(objLestPhone)
							DELETE_OBJECT(objLestPhone)
							RELEASE_FAKE_CELLPHONE_MOVIE(sfLestPhoneScreen)
							RELEASE_NPC_PHONE_RENDERTARGET()
							CLEAR_PED_TASKS(pedLester)
							TASK_CLEAR_LOOK_AT(pedLester)
							SETTIMERA(0)
							iConvStage++
						ENDIF
						
						SET_WIDESCREEN_BORDERS(TRUE, 0)
						CLEAR_AREA(v_OfficeCoords, 200, TRUE)
						
						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON
							WAIT(500)
							FADE_IN()
							WHILE NOT IS_SCREEN_FADED_IN()
								WAIT(0)
							ENDWHILE
						ENDIF
						
						SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON_NEAR)
					ENDIF
					
					DO_SCREEN_FADE_IN(500)
					PRINT_GOD_TEXT(gWaitForJanitor)
					iStageProgress++
					
				ENDIF
				
//				IF eCamMode != CAM_VIEW_MODE_THIRD_PERSON_MEDIUM
//					bGameplayCamGrabbed = TRUE
//				ENDIF
				
			BREAK
			
			CASE 2
			
				IF DOES_CAM_EXIST(fpCam.theCam) 
				AND IS_CAM_ACTIVE(fpCam.theCam)
				AND IS_CAM_RENDERING(fpCam.theCam)
//					IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_THIRD_PERSON_MEDIUM
//						SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(eCamMode)
//					ENDIF
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ENDIF
				
				SET_VEHICLE_LIGHTS(vehPlayerCar, FORCE_VEHICLE_LIGHTS_OFF)
				
				IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF
				
				
				SWITCH iCarProgress
				
					CASE AHS1_SO_WAIT_FOR_CAR
						IF current_car < iNumberOfDecoys
							HAS_PLAYER_SPOTTED_PLATE(decoy_cars[current_car].veh, vLPOffsets[current_car])
						ENDIF
						IF TIMERA() > janitor_wait_time
						OR current_car = 0
							IF current_car < iNumberOfDecoys
								INT iRec
								IF current_car = 3
									iRec = 102
								ELSE
									iRec = 101+current_car
								ENDIF
								IF CREATE_DECOY_CAR(decoy_cars[current_car], car_models[current_car], driver_model, iRec, colours[2*current_car], colours[(2*current_car)+1])
									bExitTopRampSound = FALSE
									bExitBottomRampSound = FALSE
									iCarProgress = AHS1_SO_SPOT_CAR
									janitor_wait_time = GET_RANDOM_INT_IN_RANGE(12000, 22000)
									//janitor_wait_time = 0
									IF IS_VEHICLE_DRIVEABLE(decoy_cars[current_car].veh)
										ADD_ENTITY_TO_AUDIO_MIX_GROUP(decoy_cars[current_car].veh, "AGENCY_H_1_NON_TARGET_CARS_GROUP")
									ENDIF
									SETTIMERA(0)
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE AHS1_SO_SPOT_CAR
						HAS_PLAYER_SPOTTED_PLATE(decoy_cars[current_car].veh, vLPOffsets[current_car])
						IF IS_ENTITY_IN_ANGLED_AREA(decoy_cars[current_car].veh, v_Stakeout_area_Pos1, v_Stakeout_area_Pos2, f_Stakeout_area_Width)
							IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_NON_TARGET_CARS")
								START_AUDIO_SCENE("AGENCY_H_1_NON_TARGET_CARS")
							ENDIF
							IF NOT b_help_used
								PRINT_HELP_FOREVER("S1_FOCUS")
							ENDIF
							IF CREATE_CONVERSATION(sSpeech, "FBS1AUD", "FBS1_SPOT", CONV_PRIORITY_HIGH)
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								iCarProgress = AHS1_SO_IDENT_CAR
							ENDIF
						ENDIF
					BREAK
					
					CASE AHS1_SO_IDENT_CAR
						IF HAS_PLAYER_SPOTTED_PLATE(decoy_cars[current_car].veh, vLPOffsets[current_car])
							iDecoySpots++
							iCarProgress = AHS1_SO_SUCCESS
						ELSE
							IF NOT IS_ENTITY_IN_ANGLED_AREA(decoy_cars[current_car].veh, v_Stakeout_area_Pos1, v_Stakeout_area_Pos2, f_Stakeout_area_Width)
								iCarProgress = AHS1_SO_FAIL
							ENDIF
						ENDIF
					BREAK
					
					CASE AHS1_SO_SUCCESS
						HAS_PLAYER_SPOTTED_PLATE(decoy_cars[current_car].veh, vLPOffsets[current_car])
						IF CREATE_CONVERSATION(sSpeech, "FBS1AUD", "FBS1_IDENT", CONV_PRIORITY_HIGH)
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							iCarProgress = AHS1_SO_CONV
						ENDIF
					BREAK
					
					CASE AHS1_SO_FAIL
						HAS_PLAYER_SPOTTED_PLATE(decoy_cars[current_car].veh, vLPOffsets[current_car])
						IF CREATE_CONVERSATION(sSpeech, "FBS1AUD", "FBS1_MISS", CONV_PRIORITY_HIGH)
							iCarProgress = AHS1_SO_CONV
						ENDIF
					BREAK
					
					CASE AHS1_SO_CONV
						HAS_PLAYER_SPOTTED_PLATE(decoy_cars[current_car].veh, vLPOffsets[current_car])
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF iCurrentStakeoutSpeech < 4
								TEXT_LABEL_23 sTemp
								sTemp  = "FBS1_SOC"
								sTemp += iCurrentStakeoutSpeech
								IF CREATE_CONVERSATION(sSpeech, "FBS1AUD", sTemp, CONV_PRIORITY_HIGH)
									iCarProgress = AHS1_SO_WAIT_FOR_END
									iCurrentStakeoutSpeech++
								ENDIF
							ELSE
								iCarProgress = AHS1_SO_WAIT_FOR_END
							ENDIF
						ENDIF
					BREAK
					
					CASE AHS1_SO_WAIT_FOR_END
						HAS_PLAYER_SPOTTED_PLATE(decoy_cars[current_car].veh, vLPOffsets[current_car])
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF (DOES_ENTITY_EXIST(decoy_cars[current_car].veh)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(decoy_cars[current_car].veh, vLargerStakeoutAreaPos1, vLargerStakeoutAreaPos2, fLargerStakeoutAreaWidth))
							OR NOT DOES_ENTITY_EXIST(decoy_cars[current_car].veh)
								current_car++
								PRINTLN("Going to car number ", current_car)
								janitor_wait_time = GET_RANDOM_INT_IN_RANGE(1000, 5000)
								IF current_car >= iNumberOfDecoys
									REQUEST_ASSETS_FOR_STAGE(STAGE_TAIL_THE_JANITOR)
									IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_NON_TARGET_CARS")
										STOP_AUDIO_SCENE("AGENCY_H_1_NON_TARGET_CARS")
									ENDIF
									IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_JANITORS_CAR")
										START_AUDIO_SCENE("AGENCY_H_1_JANITORS_CAR")
									ELSE
										IF DOES_ENTITY_EXIST(vehJanitor)
											ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehJanitor, "AGENCY_H_1_JANITORS_CAR_GROUP")
										ENDIF
									ENDIF
									PRINTLN("GOING TO JANITOR CAR")
									bExitTopRampSound = FALSE
									bExitBottomRampSound = FALSE
									iStageProgress++
								ELSE
									iCarProgress = AHS1_SO_WAIT_FOR_CAR
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
				ENDSWITCH
				
				IF current_car < iNumberOfDecoys
				AND current_car >= 0
				
					IF DOES_ENTITY_EXIST(decoy_cars[current_car].veh)
					AND IS_VEHICLE_DRIVEABLE(decoy_cars[current_car].veh)
					
						IF NOT bExitTopRampSound
							IF IS_ENTITY_IN_ANGLED_AREA(decoy_cars[current_car].ped, <<94.876869,-703.077332,31.523407>>, <<98.584084,-692.756653,36.082397>>, 9.0000)
								PLAY_SOUND_FROM_ENTITY(-1, "FBI_HEIST_SETUP_1_Car_Park_Skid_CR", decoy_cars[current_car].veh)
								bExitTopRampSound = TRUE
							ENDIF
						ENDIF
						
						IF NOT bExitBottomRampSound
							IF IS_ENTITY_IN_ANGLED_AREA(decoy_cars[current_car].ped, <<84.498009,-699.107910,30.427471>>, <<88.141838,-689.035278,35.591557>>, 4.000000)
								IF current_car = 0
									PLAY_SOUND_FROM_ENTITY(-1, "FBI_HEIST_SETUP_1_Car_Park_tyre_bump_CR", decoy_cars[current_car].veh)
									PRINTLN("Playing FBI_HEIST_SETUP_1_Car_Park_tyre_bump_CR")
								ELIF current_car = 1
									PLAY_SOUND_FROM_ENTITY(-1, "FBI_HEIST_SETUP_1_Car_Park_tyre_bump_creak_CR", decoy_cars[current_car].veh)
									PRINTLN("Playing FBI_HEIST_SETUP_1_Car_Park_tyre_bump_creak_CR")
								ELIF current_car = 2
									PLAY_SOUND_FROM_ENTITY(-1, "FBI_HEIST_SETUP_1_Car_Park_tyre_bump_no_skid_CR", decoy_cars[current_car].veh)
									PRINTLN("Playing FBI_HEIST_SETUP_1_Car_Park_tyre_bump_no_skid_CR")
								ELSE
									PLAY_SOUND_FROM_ENTITY(-1, "FBI_HEIST_SETUP_1_Car_Park_tyre_bump_scraped_CR", decoy_cars[current_car].veh)
									PRINTLN("Playing FBI_HEIST_SETUP_1_Car_Park_tyre_bump_scraped_CR")
								ENDIF
								bExitBottomRampSound = TRUE
							ENDIF
						ENDIF
						
					ENDIF
					
				ENDIF
				
				//check for Lester spotting a car
				REPEAT COUNT_OF(decoy_cars) iTemp
					IF IS_VEHICLE_DRIVEABLE(decoy_cars[iTemp].veh)
					AND NOT IS_PED_INJURED(decoy_cars[iTemp].ped)
						//once outside range for spotting and identifying wander off
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(decoy_cars[iTemp].veh)
							TASK_VEHICLE_DRIVE_WANDER(decoy_cars[iTemp].ped, decoy_cars[iTemp].veh, 20, DRIVINGMODE_STOPFORCARS_STRICT | DF_StopForPeds | DF_StopAtLights)
							IF IS_ENTITY_OCCLUDED(decoy_cars[iTemp].veh)
								PRINTLN("DELETING OLD DECOY")
								REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(decoy_cars[iTemp].veh)
								DELETE_PED(decoy_cars[iTemp].ped)
								DELETE_VEHICLE(decoy_cars[iTemp].veh)
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
				
			BREAK
			
			CASE 3
			
				IF IS_VEHICLE_DRIVEABLE(vehJanitor)
				
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "FBIs1UBER")
					AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJanitor)
						IF IS_VEHICLE_DRIVEABLE(vehJanitor)
							START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehJanitor, 1, "FBIs1UBER", 5, DRIVINGMODE_PLOUGHTHROUGH)
							SETTIMERA(0)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehJanitor)
					AND IS_VEHICLE_DRIVEABLE(vehJanitor)
					
						IF NOT bExitTopRampSound
							IF IS_ENTITY_IN_ANGLED_AREA(vehJanitor, <<94.876869,-703.077332,31.523407>>, <<98.584084,-692.756653,36.082397>>, 4.750000)
								PLAY_SOUND_FROM_ENTITY(-1, "FBI_HEIST_SETUP_1_Car_Park_Skid_CR", vehJanitor)
								bExitTopRampSound = TRUE
							ENDIF
						ENDIF
						
						IF NOT bExitBottomRampSound
							IF IS_ENTITY_IN_ANGLED_AREA(vehJanitor, <<84.498009,-699.107910,30.427471>>, <<88.141838,-689.035278,35.591557>>, 4.000000)
								PLAY_SOUND_FROM_ENTITY(-1, "FBI_HEIST_SETUP_1_Car_Park_tyre_bump_CR", vehJanitor)
								PRINTLN("Playing FBI_HEIST_SETUP_1_Car_Park_tyre_bump_CR")
								bExitBottomRampSound = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF b_janitor_spotted
						IF DOES_CAM_EXIST(fpcam.theCam)
							SET_CAM_ACTIVE(fpcam.theCam, FALSE)
							DESTROY_CAM(fpCam.theCam)
							SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
						ENDIF
						IF NOT SHOULD_CONTROL_CHASE_HINT_CAM(focuscam, FALSE)
						OR TIMERA() > 3000
							SETTIMERA(0)
							REPEAT COUNT_OF(decoy_cars) iTemp
								IF DOES_ENTITY_EXIST(decoy_cars[iTemp].ped)
								AND DOES_ENTITY_EXIST(decoy_cars[iTemp].veh)
								AND IS_ENTITY_OCCLUDED(decoy_cars[iTemp].veh)
									SET_PED_AS_NO_LONGER_NEEDED(decoy_cars[iTemp].ped)
									SET_VEHICLE_AS_NO_LONGER_NEEDED(decoy_cars[iTemp].veh)
								ENDIF
							ENDREPEAT
							bLockVehicleControls = FALSE
							KILL_CHASE_HINT_CAM(focuscam)
							//INSTANTLY_FILL_VEHICLE_POPULATION()
							FREEZE_ENTITY_POSITION(vehPlayerCar, FALSE)
							SETTIMERA(0)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehJanitor)
							START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehJanitor, 1, "FBIs1UBER", 5, DRIVINGMODE_STOPFORCARS_STRICT | DF_StopForPeds | DF_StopAtLights)
							IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_JANITORS_CAR")	
								STOP_AUDIO_SCENE("AGENCY_H_1_JANITORS_CAR")
							ENDIF
//							IF bGameplayCamGrabbed
								SET_GAMEPLAY_CAM_RELATIVE_HEADING()
								SET_GAMEPLAY_CAM_RELATIVE_PITCH()
								SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(eCamMode)
//							ENDIF
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							IF DOES_CAM_EXIST(zoomCam)
								DESTROY_CAM(zoomCam)
							ENDIF
							iStageProgress++
						ENDIF
					ELSE
						IF IS_ENTITY_IN_ANGLED_AREA(vehJanitor, v_Stakeout_area_Pos1, v_Stakeout_area_Pos2, f_Stakeout_area_Width)
							IF NOT b_help_used
								PRINT_HELP_FOREVER("S1_FOCUS")
							ENDIF
						ENDIF
						IF HAS_PLAYER_SPOTTED_PLATE(vehJanitor, vJanLPOffset)
							PRINTLN("CAN SEE JANITOR LP")
							IF NOT b_janitor_spotted
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								IF CREATE_CONVERSATION(sSpeech, "FBS1AUD", "FBS1_JANID", CONV_PRIORITY_HIGH)
									IF iDecoySpots = iNumberOfDecoys
										INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(AH1_EAGLE_EYE)
									ENDIF
									REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
									b_janitor_spotted = TRUE
									b_janitor_rage = FALSE
									SETTIMERA(0)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//fail if janitor slips away
					IF NOT b_janitor_spotted
					AND NOT IS_ENTITY_IN_ANGLED_AREA(vehJanitor, v_Stakeout_area_Pos1, v_Stakeout_area_Pos2, f_Stakeout_area_Width)
					AND NOT IS_ENTITY_IN_ANGLED_AREA(vehJanitor, vLargerStakeoutAreaPos1, vLargerStakeoutAreaPos2, fLargerStakeoutAreaWidth)
					AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(vehJanitor, FALSE)) > 75
						SETTIMERA(0)
						iStageProgress = 100
					ENDIF
					
					REPEAT COUNT_OF(decoy_cars) iTemp
						IF DOES_ENTITY_EXIST(decoy_cars[iTemp].ped)
						AND DOES_ENTITY_EXIST(decoy_cars[iTemp].veh)
						AND IS_ENTITY_OCCLUDED(decoy_cars[iTemp].veh)
							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(decoy_cars[iTemp].veh)
							DELETE_PED(decoy_cars[iTemp].ped)
							DELETE_VEHICLE(decoy_cars[iTemp].veh)
							IF iTemp = COUNT_OF(decoy_cars) - 1
								IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_NON_TARGET_CARS")
									STOP_AUDIO_SCENE("AGENCY_H_1_NON_TARGET_CARS")
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
				ENDIF
				
			BREAK
			
			CASE 4
				TRANSITION_BACK_TO_GAME()
				IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_FOLLOW_JANITOR")
					START_AUDIO_SCENE("AGENCY_H_1_FOLLOW_JANITOR")
				ENDIF
				IF TIMERA() > 500
					IF CREATE_CONVERSATION(sSpeech, "FBS1AUD", "FBS1_FOLLOW", CONV_PRIORITY_HIGH)
						GO_TO_STAGE(STAGE_TAIL_THE_JANITOR)
						blipJanitor = CREATE_BLIP_FOR_VEHICLE(vehJanitor)
						HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
						REPEAT COUNT_OF(decoy_cars) iTemp
							DELETE_PED(decoy_cars[iTemp].ped)
							DELETE_VEHICLE(decoy_cars[iTemp].veh)
							SET_MODEL_AS_NO_LONGER_NEEDED(car_models[iTemp])
						ENDREPEAT
						SET_MODEL_AS_NO_LONGER_NEEDED(driver_model)
						CLEAR_HELP()
						SETTIMERA(0)
					ENDIF
				ENDIF
			BREAK
			
			CASE 100
				IF TIMERA() > 3000
					IF NOT IS_ENTITY_DEAD(vehJanitor)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehJanitor)
						START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehJanitor, 1, "FBIs1UBER", 5, DRIVINGMODE_STOPFORCARS_STRICT | DF_StopForPeds | DF_StopAtLights)
					ENDIF
					WAIT(0)
					Mission_Failed("S1_FAILED")
				ENDIF
			BREAK
			
		ENDSWITCH
		
	ENDIF
	
	IF NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
		IF DOES_CAM_EXIST(fpCam.theCam)
			UPDATE_FIRST_PERSON_CAMERA(fpCam, FALSE)
			IF DOES_CAM_EXIST(zoomCam)
			AND IS_CAM_ACTIVE(zoomCam)
				SET_CAM_ROT(fpCam.theCam, vCamRot)
			ENDIF
		ENDIF
	ENDIF
	
	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "FBIs1UBER")
		CREATE_JANITOR_IN_CAR(<< 98.6395, -728.7055, 32.1340 >>, 347.8271)
		IF IS_VECTOR_ZERO(path1[0])
			BUILD_UBER_ROUTE_DEBUG_PATH(path1, 1, "FBIs1UBER")
		ENDIF
	ENDIF
	
	IF DOES_CAM_EXIST(zoomCam)
	AND IS_CAM_RENDERING(zoomCam)
		IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_FOCUS_ON_PLATES")
			START_AUDIO_SCENE("AGENCY_H_1_FOCUS_ON_PLATES")
		ENDIF
	ELSE
		IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_FOCUS_ON_PLATES")
			STOP_AUDIO_SCENE("AGENCY_H_1_FOCUS_ON_PLATES")
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iStageProgress), <<0.01, 0.5, 0>>)
	#ENDIF
	
	IF NOT b_help_used
	
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("S1_FOCUS")
			BOOL bNeeded = FALSE
			REPEAT COUNT_OF(decoy_cars) iTemp
				IF DOES_ENTITY_EXIST(decoy_cars[iTemp].veh)
					IF IS_ENTITY_IN_ANGLED_AREA(decoy_cars[iTemp].veh, v_Stakeout_area_Pos1, v_Stakeout_area_Pos2, f_Stakeout_area_Width)
						bNeeded = TRUE
					ENDIF
				ENDIF
			ENDREPEAT
			IF DOES_ENTITY_EXIST(vehJanitor)
				IF IS_ENTITY_IN_ANGLED_AREA(vehJanitor, v_Stakeout_area_Pos1, v_Stakeout_area_Pos2, f_Stakeout_area_Width)
					bNeeded = TRUE
				ENDIF
			ENDIF
			IF NOT bNeeded
				CLEAR_HELP()
			ENDIF
		ENDIF
		
		IF DOES_CAM_EXIST(zoomCam)
			IF IS_CAM_RENDERING(zoomCam)
			AND GET_CAM_FOV(zoomCam) < 20
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("S1_FOCUS")
					CLEAR_HELP()
					b_help_used = TRUE
				ENDIF							
			ENDIF
		ENDIF
		
	ENDIF
	
	IF bLockVehicleControls
	
		IF (DOES_CAM_EXIST(fpCam.theCam) AND IS_CAM_RENDERING(fpCam.theCam))
		OR (DOES_CAM_EXIST(zoomCam) AND IS_CAM_RENDERING(zoomCam))
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
		ENDIF
		
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_1)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_2)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SAVING_GAME)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
		HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
		
		IF NOT IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_MOVE_LR)
		ENDIF
		
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_UP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_FRONTEND_DOWN)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_BRAKE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HANDBRAKE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PHONE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SCRIPT_SELECT)
//		IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_THIRD_PERSON_MEDIUM
//			SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON_MEDIUM) 
//		ENDIF
		DISABLE_SELECTOR_THIS_FRAME()
		
	ENDIF
		
ENDPROC

FUNC VECTOR GET_VECTOR_FROM_DIRECTION_AND_MAGNITUDE(VECTOR v, FLOAT m)
	VECTOR n = NORMALISE_VECTOR(v)
	n.x = n.x * m
	n.y = n.y * m
	n.z = n.z * m
	RETURN n
ENDFUNC


FLOAT fLostTime = 0
FUNC BOOL HAS_PLAYER_LOST_SIGHT_OF_JANITOR(FLOAT minLostTime = 3.0, FLOAT maxLostTime = 10.0, FLOAT minDist = 100.0, FLOAT maxDist = 250.0)
	FLOAT fRun = maxDist - minDist
	FLOAT fDistToJan = VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(vehJanitor, FALSE))
	IF IS_ENTITY_OCCLUDED(vehJanitor)
	AND fDistToJan > (minDist)
		fLostTime += GET_FRAME_TIME()
	ELSE
		fLostTime = 0
	ENDIF
	IF fDistToJan > minDist
	AND fLostTime > LERP_FLOAT(minLostTime, maxLostTime, 1 - (CLAMP(fDistToJan - minDist, 0, fRun)/fRun))
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FLOAT fBlowTime

FUNC BOOL HAS_PLAYER_BLOWN_FOLLOW()

	IF IS_VEHICLE_DRIVEABLE(vehJanitor)
	
		BOOL bIncrementFailTime
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
			VEHICLE_INDEX vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF IS_VEHICLE_DRIVEABLE(vehTemp)
			
				VECTOR vDimMin, vDimMax, vDim
				GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(vehTemp), vDimMin, vDimMax)
				vDim = vDimMax - vDimMin
				
				VECTOR vCarPoints[4]
				vCarPoints[0] = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehJanitor, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTemp, <<0, vDim.y, 0>>))
				vCarPoints[1] = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehJanitor, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTemp, <<0, -vDim.y, 0>>))				
				vCarPoints[2] = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehJanitor, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTemp, <<vDim.x, 0, 0>>))				
				vCarPoints[3] = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehJanitor, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTemp, <<-vDim.x, 0, 0>>))				
				
				INT iTemp
				REPEAT COUNT_OF(vCarPoints) iTemp
					IF vCarPoints[iTemp].y > -1
					AND vCarPoints[iTemp].y < 7
					AND ABSF(vCarPoints[iTemp].x) < 3.5
						#IF IS_DEBUG_BUILD
							DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(vehJanitor, FALSE) + <<0,0,2>>, 0.3, 255, 0, 0)
							DRAW_DEBUG_LINE(GET_ENTITY_COORDS(vehJanitor, FALSE), GET_ENTITY_COORDS(vehJanitor, FALSE) + <<0,0,2>>, 255, 0, 0)
						#ENDIF
						bIncrementFailTime = TRUE
					ENDIF
				ENDREPEAT
				
			ENDIF
			
		ENDIF
		
		IF bIncrementFailTime
			IF IS_PLAYER_PRESSING_HORN(PLAYER_ID())
				fBlowTime += GET_FRAME_TIME()*100.0
			ELSE
				fBlowTime += GET_FRAME_TIME()
			ENDIF
		ELSE
			IF fBlowTime > 0
				fBlowTime -= GET_FRAME_TIME() * 2
			ENDIF
		ENDIF
		
		IF fBlowTime > 15.0
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC TAIL_THE_JANITOR_USING_AI()

	IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
	OR iStageProgress = 0
	
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.85)
		
		SWITCH iStageProgress
		
			CASE 0
			
				bFollowAchievment = TRUE
				fLostTime = 0
				b_janitor_rage = FALSE
				fBlowTime = 0
				iParkStage = 0
				
				REQUEST_ANIM_DICT("cellphone@")
				REQUEST_MODEL(PROP_NPC_PHONE)
				
				SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
				
				IF bSkip
				
					FORCE_DELETE_DECOY_CARS()
					DELETE_PED(pedJanitor)
					DELETE_VEHICLE(vehJanitor)
					
					WHILE NOT SAFELY_CREATE_PLAYER_VEHICLE(v_OfficeCoords, f_office_heading, TRUE, TRUE)	
						WAIT(0)
					ENDWHILE
					
					IF NOT IS_REPLAY_BEING_SET_UP()
					
						REQUEST_COLLISION_AT_COORD(v_OfficeCoords)
						
						WHILE NOT HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID())
							WAIT(0)
						ENDWHILE
						
						SET_FOCUS_POS_AND_VEL(v_OfficeCoords, <<0,0,0>>)
						NEW_LOAD_SCENE_START(v_OfficeCoords, <<COS(f_office_heading+90), SIN(f_office_heading+90), 0>>, 25)
						SETTIMERA(0)
						
						WHILE NOT IS_NEW_LOAD_SCENE_LOADED() 
						AND TIMERA() < 10000
							WAIT(0)
						ENDWHILE
						
						CLEAR_FOCUS()
						NEW_LOAD_SCENE_STOP()
						
					ENDIF
					
					FORCE_RESET_LESTER()
					
					WHILE NOT CREATE_JANITOR_IN_CAR(<< 80.8118, -661.5384, 30.6444 >>, 339.4735)
						WAIT(0)
					ENDWHILE
					
					IF IS_VEHICLE_DRIVEABLE(vehJanitor)
						SET_ENTITY_COORDS_NO_OFFSET(vehJanitor, <<94.7542, -621.0219, 31.2438>>)
						SET_ENTITY_QUATERNION(vehJanitor, -0.0002, -0.0024, -0.1767, 0.9843)
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehJanitor, "AGENCY_H_1_JANITORS_CAR_GROUP")							
					ENDIF
					
					REQUEST_VEHICLE_RECORDING(1, "FBIs1UBER")
					WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "FBIs1UBER")
						WAIT(0)
					ENDWHILE
					
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "FBIs1UBER")
						IF IS_VECTOR_ZERO(path1[0])
							BUILD_UBER_ROUTE_DEBUG_PATH(path1, 1, "FBIs1UBER")
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehJanitor)
						SET_VEHICLE_FORWARD_SPEED(vehJanitor, 7)
					ENDIF
					
//					iTimeInfrontDone = 0
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP(vehPlayerCar)
					ENDIF
					
					TRIGGER_MUSIC_EVENT("AH1_RESTART")
					
					gFollowJanitor.played = FALSE
					
				ENDIF
				
				SET_ROADS_IN_ANGLED_AREA(<<199.830154,182.740646,64.701622>>, <<124.547417,-22.546419,115.231438>>, 17.500000, FALSE, FALSE)
				
				IF IS_VEHICLE_DRIVEABLE(vehJanitor)
				AND NOT IS_PED_INJURED(pedJanitor)
				
					SET_ENTITY_LOAD_COLLISION_FLAG(vehJanitor, TRUE)
					SET_ENTITY_LOAD_COLLISION_FLAG(pedJanitor, TRUE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehJanitor, FALSE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedJanitor, FALSE)
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJanitor)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehJanitor)
					ENDIF
					
					IF NOT DOES_BLIP_EXIST(blipJanitor)
						blipJanitor = CREATE_BLIP_FOR_VEHICLE(vehJanitor)
					ENDIF
					
				ENDIF
				
				bLeftTurn[0] = FALSE
				bLeftTurn[1] = FALSE
				bLeftTurn[2] = FALSE
				bRightTurn = FALSE
				iConvStage = 0
				
				follow_conv_on_hold = FALSE
				
				SETTIMERA(0)
				SETTIMERB(0)
				
				CHECK_PEDS_FOR_DIALOGUE()
				
				IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_WAIT_FOR_CARS")
					STOP_AUDIO_SCENE("AGENCY_H_1_WAIT_FOR_CARS")
				ENDIF
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_FOLLOW_JANITOR")
					START_AUDIO_SCENE("AGENCY_H_1_FOLLOW_JANITOR")
				ENDIF
				
				bInfrontDone = TRUE
				
				iStageProgress++
				
			BREAK
			
			CASE 1
			
				FADE_IN()
			
				IF DOES_BLIP_EXIST(blipJanitor)
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRINT_GOD_TEXT(gFollowJanitor)
				ENDIF
				
				SWITCH iConvStage
				
					CASE 0
						IF TIMERA() > 7500
							MANAGE_CONVERSATION_FOR_CHASE(dTail1.label)
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
								IF in_car_conv_started
								AND NOT in_car_conv_on_hold
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									SETTIMERA(0)
									in_car_conv_started = FALSE
									in_car_conv_on_hold = FALSE
									ConvResumeLabel = ""
									iConvStage++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 1
						MANAGE_CONVERSATION_FOR_CHASE("", TRUE)
						IF TIMERA() > 8000
							iConvStage++
						ENDIF
					BREAK
					
					CASE 2
					
						IF NOT DOES_ENTITY_EXIST(objLestPhone)
							REQUEST_MODEL(PROP_NPC_PHONE)
							REQUEST_FAKE_CELLPHONE_MOVIE(sfLestPhoneScreen)
							REQUEST_ANIM_DICT("missheist_agency2aig_2")
							IF HAS_MODEL_LOADED(PROP_NPC_PHONE)
							AND HAS_FAKE_CELLPHONE_MOVIE_LOADED(sfLestPhoneScreen)
							AND HAS_ANIM_DICT_LOADED("missheist_agency2aig_2")
							AND NOT IS_PED_INJURED(pedLester)
								IF NOT DOES_ENTITY_EXIST(objLestPhone)
									objLestPhone = CREATE_OBJECT(PROP_NPC_PHONE, GET_PED_BONE_COORDS(pedLester, BONETAG_PH_L_HAND, <<0,0,0>>))
									DRAW_FAKE_CELLPHONE_SCREEN(sfLestPhoneScreen, FALSE, FAKE_CELLPHONE_SCREEN_BADGER_MENU)
									ATTACH_ENTITY_TO_ENTITY(objLestPhone, pedLester, GET_PED_BONE_INDEX(pedLester, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, TRUE)
									TASK_PLAY_ANIM(pedLester, "missheist_agency2aig_2", "look_at_phone_a", REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_UPPERBODY|AF_SECONDARY|AF_LOOPING)
									TASK_LOOK_AT_ENTITY(pedLester, objLestPhone, -1)
									SETTIMERA(0)
								ENDIF
							ENDIF
						ELSE
							IF TIMERA() > 3000
								MANAGE_CONVERSATION_FOR_CHASE(dTail2.label)
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
									IF in_car_conv_started
									AND NOT in_car_conv_on_hold
									AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										SETTIMERA(0)
										in_car_conv_started = FALSE
										in_car_conv_on_hold = FALSE
										ConvResumeLabel = ""
										iConvStage++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					BREAK
					
					CASE 3
						IF NOT IS_PED_INJURED(pedLester)
							IF IS_ENTITY_PLAYING_ANIM(pedLester, "missheist_agency2aig_2", "look_at_phone_a")
								FLOAT fTemp
								fTemp = GET_ENTITY_ANIM_CURRENT_TIME(pedLester, "missheist_agency2aig_2", "look_at_phone_a")
								CLEAR_PED_TASKS(pedLester)
								TASK_PLAY_ANIM(pedLester, "missheist_agency2aig_2", "look_at_phone_a", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1, AF_UPPERBODY|AF_SECONDARY, fTemp)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(pedLester)
								TASK_CLEAR_LOOK_AT(pedLester)
								SETTIMERA(0)
								iConvStage++
							ENDIF
						ENDIF
					BREAK
					
					CASE 4
						IF NOT IS_PED_INJURED(pedLester)
						AND NOT IS_ENTITY_PLAYING_ANIM(pedLester, "missheist_agency2aig_2", "look_at_phone_a")
						AND DOES_ENTITY_EXIST(objLestPhone)
						AND TIMERA() > 1000
							SETTIMERA(0)
							DELETE_OBJECT(objLestPhone)
							RELEASE_FAKE_CELLPHONE_MOVIE(sfLestPhoneScreen)
							RELEASE_NPC_PHONE_RENDERTARGET()
							iConvStage++
						ENDIF
					BREAK
					
					CASE 5
						MANAGE_CONVERSATION_FOR_CHASE("", TRUE)
						IF TIMERA() > 4000
							iConvStage++
						ENDIF
					BREAK
					
					CASE 6
						IF TIMERA() > 3000
							MANAGE_CONVERSATION_FOR_CHASE("FBS1_CASH")
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
								IF in_car_conv_started
								AND NOT in_car_conv_on_hold
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									SETTIMERA(0)
									in_car_conv_started = FALSE
									in_car_conv_on_hold = FALSE
									ConvResumeLabel = ""
									iConvStage++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 7
						MANAGE_CONVERSATION_FOR_CHASE("", TRUE)
						IF TIMERA() > 10000
							iConvStage++
						ENDIF
					BREAK
					
					CASE 8
						MANAGE_CONVERSATION_FOR_CHASE("FBS1_TAIL3")
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
							IF in_car_conv_started
							AND NOT in_car_conv_on_hold
							AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								SETTIMERA(0)
								in_car_conv_started = FALSE
								in_car_conv_on_hold = FALSE
								ConvResumeLabel = ""
								iConvStage++
							ENDIF
						ENDIF
					BREAK
					
					CASE 9
						MANAGE_CONVERSATION_FOR_CHASE("", TRUE)
					BREAK
					
				ENDSWITCH
				
				IF IS_VEHICLE_DRIVEABLE(vehJanitor)
				
					IF IS_VEHICLE_STOPPED_AT_TRAFFIC_LIGHTS(vehJanitor)
						PRINTLN("WAITING AT RED LIGHT")
					ENDIF
					
					IF NOT IS_PED_INJURED(pedJanitor)
					AND IS_PED_IN_VEHICLE(pedJanitor,vehJanitor)
					//AND NOT IS_PLAYBACK_USING_AI_GOING_ON_FOR_VEHICLE(vehJanitor)
						DO_JANITOR_ROUTE_HOME()
						//START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehJanitor, 1, "FBIs1UBER", 5, DRIVINGMODE_STOPFORCARS_STRICT | DF_StopForPeds | DF_StopAtLights)
					ENDIF
					
					CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, vehJanitor)
					
					VEHICLE_INDEX vehTemp
			
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF NOT IS_ENTITY_DEAD(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
							vehTemp = GET_PLAYERS_LAST_VEHICLE()
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehTemp)
					
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehJanitor, vehtemp)
						OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehJanitor, PLAYER_PED_ID())
							IF NOT IS_PED_INJURED(pedJanitor)
								SET_PED_FLEE_ATTRIBUTES(pedJanitor, FA_USE_VEHICLE, IS_VEHICLE_DRIVEABLE(vehJanitor))
								TASK_SMART_FLEE_PED(pedJanitor, PLAYER_PED_ID(), 200, 20000)				
							ENDIF
							IF IS_PED_IN_VEHICLE(pedLester, vehTemp)
								iStageProgress = 2
							ELSE
								iStageProgress = 20
							ENDIF
							bBlockFleeCheckForFail = TRUE
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
						
					ENDIF
					
					IF HAS_PLAYER_LOST_SIGHT_OF_JANITOR()
						IF DOES_BLIP_EXIST(blipJanitor)
							REMOVE_BLIP(blipJanitor)
						ENDIF
						iStageProgress = 3
						bBlockFleeCheckForFail = TRUE
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					ENDIF
					
					IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehJanitor, <<10,10,3>>)
						IF IS_PED_JACKING(PLAYER_PED_ID())
						AND GET_VEHICLE_PED_IS_TRYING_TO_ENTER(PLAYER_PED_ID()) != 0
						AND GET_VEHICLE_PED_IS_TRYING_TO_ENTER(PLAYER_PED_ID()) != 0
						AND NOT IS_VEHICLE_EMPTY(vehJanitor)
							IF NOT IS_PED_INJURED(pedJanitor)
								SET_PED_FLEE_ATTRIBUTES(pedJanitor, FA_USE_VEHICLE, IS_VEHICLE_DRIVEABLE(vehJanitor))
								TASK_SMART_FLEE_PED(pedJanitor, PLAYER_PED_ID(), 200, -1)
							ENDIF
							SETTIMERA(1500)
							iStageProgress = 20
							bBlockFleeCheckForFail = TRUE
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						ENDIF
					ENDIF
					
					IF (VDIST2(GET_ENTITY_COORDS(vehPlayerCar, FALSE), GET_ENTITY_COORDS(vehJanitor, FALSE)) < 10*10 AND IS_ENTITY_TOUCHING_ENTITY(vehPlayerCar, vehJanitor))
					OR (DOES_ENTITY_EXIST(vehTemp) AND VDIST2(GET_ENTITY_COORDS(vehTemp, FALSE), GET_ENTITY_COORDS(vehTemp, FALSE)) < 10*10 AND IS_ENTITY_TOUCHING_ENTITY(vehTemp, vehJanitor))
					OR HAS_PLAYER_BLOWN_FOLLOW()
						IF NOT b_janitor_rage
							IF IS_VEHICLE_DRIVEABLE(vehJanitor)
							AND NOT IS_PED_INJURED(pedJanitor)
							AND IS_PED_IN_VEHICLE(pedJanitor,vehJanitor)
								STOP_PLAYBACK_RECORDED_VEHICLE(vehJanitor)
								SET_PED_FLEE_ATTRIBUTES(pedJanitor, FA_USE_VEHICLE, IS_VEHICLE_DRIVEABLE(vehJanitor))
								TASK_SMART_FLEE_PED(pedJanitor, PLAYER_PED_ID(), 200, 20000)
								bBlockFleeCheckForFail = TRUE
								iStageProgress = 2
							ENDIF
						ENDIF
					ENDIF
					
					vehTemp = NULL
					
					IF IS_ENTITY_IN_ANGLED_AREA(vehJanitor, vLeftTurn1Pos1, vLeftTurn1Pos2, fLeftTurn1Width)
					OR IS_ENTITY_IN_ANGLED_AREA(vehJanitor, vLeftTurn2Pos1, vLeftTurn2Pos2, fLeftTurn2Width)
					OR IS_ENTITY_IN_ANGLED_AREA(vehJanitor, vLeftTurn3Pos1, vLeftTurn3Pos2, fLeftTurn3Width)
						SET_VEHICLE_INDICATOR_LIGHTS(vehJanitor, TRUE, TRUE)
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA(vehJanitor, vRightTurnPos1, vRightTurnPos2, fRightTurnWidth)
						SET_VEHICLE_INDICATOR_LIGHTS(vehJanitor, FALSE, TRUE)
					ENDIF
					
					IF NOT IS_ENTITY_IN_ANGLED_AREA(vehJanitor, <<110.141418,-578.823120,29.459078>>, <<104.086945,-594.876648,34.626968>>, 7.250000)
					AND NOT IS_ENTITY_IN_ANGLED_AREA(vehJanitor,  <<125.536919,-35.082371,64.561394>>, <<115.333183,-62.903217,71.670616>>, 9.750000)
					AND NOT IS_ENTITY_IN_ANGLED_AREA(vehJanitor, <<-4.227216,24.248606,68.124977>>, <<-33.933033,34.761368,77.058029>>, 9.750000)
					AND NOT IS_ENTITY_IN_ANGLED_AREA(vehJanitor, <<-45.326366,-542.104004,37.184109>>, <<-10.149111,-544.482361,43.567471>>, 9.750000)
						SET_VEHICLE_INDICATOR_LIGHTS(vehJanitor, FALSE, FALSE)
					ENDIF
					
				ENDIF
				
				BLIP_MANAGER_FOR_ENTITY(vehPlayerCar, blipPlayerCar, vehJanitor, blipJanitor)
				
			BREAK
			
			CASE 2
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					PRINTLN("TRYING TO CREATE CONVERSATION FOR SPOOK FAIL")
					IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FBS1_SPKFAIL", CONV_PRIORITY_HIGH)
						SETTIMERA(0)
						iStageProgress = 20
					ENDIF
				ELSE
					iStageProgress = 20
				ENDIF
			BREAK
			
			CASE 20
				IF TIMERA() > 3000
					Mission_Failed("S1_JSPOOK")
				ENDIF
			BREAK
			
			CASE 3
				IF DOES_BLIP_EXIST(blipJanitor)
					REMOVE_BLIP(blipJanitor)
				ENDIF
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					IF CREATE_CONVERSATION(sSpeech, sConvBlock, "FBS1_LOSFAIL", CONV_PRIORITY_HIGH)
						SETTIMERA(0)
						iStageProgress = 30
					ENDIF
				ELSE
					iStageProgress = 30
				ENDIF
			BREAK
			
			CASE 30
				IF TIMERA() > 3000
					Mission_Failed("S1_FAILED")
				ENDIF
			BREAK
			
		ENDSWITCH
		
		//manage playback, uber playback and cam playback
		IF IS_VEHICLE_DRIVEABLE(vehJanitor)
		AND NOT IS_PED_INJURED(pedJanitor)
		AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
		
			//check for completion
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), v_janitors_street_Pos1, v_janitors_street_Pos2, f_janitors_street_Width)
			AND IS_ENTITY_IN_ANGLED_AREA(pedJanitor, v_janitors_street_Pos1, v_janitors_street_Pos2, f_janitors_street_Width)
				IF IS_GAMEPLAY_HINT_ACTIVE()
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				ENDIF
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPlayerCar)
//					STOP_PLAYBACK_RECORDED_VEHICLE(vehPlayerCar)
//				ENDIF
				IF iStageProgress = 1
					//INFORM_STAT_AGENCY_HEIST_ONE_SNUCK_TO_JANITOR_AREA_SUCCESSFULLY()
				ENDIF
				IF bFollowAchievment
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(AH1_MISSED_A_SPOT)
				ENDIF
				GO_TO_STAGE(STAGE_FOLLOW_JANITOR_ON_FOOT)
			ENDIF
			
		ENDIF
		
		IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), v_janitors_street_Pos1) < 200*200
			IF NOT DOES_ENTITY_EXIST(pedMarilyn)
				CREATE_HOT_GIRL()
			ENDIF
			IF NOT bRequestNextStage
				REQUEST_ASSETS_FOR_STAGE(STAGE_FOLLOW_JANITOR_ON_FOOT)
				bRequestNextStage = TRUE
			ENDIF
		ENDIF
		
		IF IS_GAMEPLAY_HINT_ACTIVE()
			IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_FOCUS_ON_JANITOR")
				START_AUDIO_SCENE("AGENCY_H_1_FOCUS_ON_JANITOR")
			ENDIF
		ELSE
			IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_FOCUS_ON_JANITOR")
				STOP_AUDIO_SCENE("AGENCY_H_1_FOCUS_ON_JANITOR")
			ENDIF
		ENDIF
		
	ENDIF
	
	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)
	
ENDPROC

//FLOAT fInterpProg
//FLOAT fJanitorQuatStart[4]
//FLOAT fJanitorQuatEnd[4]
//VECTOR vJanitorStartPos
//VECTOR vJanitorEndPos = <<-84.7900, -22.5400, 65.8062>>
INT iUnlockScenes[3]
INT iLeadinScene
VECTOR vLeadinPosition = << -110.732, -6.756, 70.538 >>
VECTOR vLeadinRotation = << 0.000, 0.000, -105.840 >>
BOOL bParkUpDialogue = FALSE

PROC stageJanitorFleesOnFoot()

	IF NOT IS_PED_INJURED(pedJanitor)
	
		SWITCH iStageProgress
		
			CASE 0
			
				IF NOT IS_PED_INJURED(pedMarilyn)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedMarilyn, RELGROUPHASH_HATES_PLAYER)
					CLEAR_PED_TASKS(pedMarilyn)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMarilyn, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(pedMarilyn, CA_ALWAYS_FLEE, TRUE)
				ENDIF
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()	
				CLEAR_PRINTS()
				//SET_PED_RELATIONSHIP_GROUP_HASH(pedJanitor, RELGROUPHASH_HATES_PLAYER)
				CLEAR_PED_TASKS(pedJanitor)
				
				SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
				
				IF IS_PED_IN_JANITORS_APARTMENT(pedJanitor)
				AND NOT IS_PED_IN_JANITORS_APARTMENT(PLAYER_PED_ID())
//				AND DOES_ENTITY_EXIST(objDoorGrab)
				AND VDIST(GET_ENTITY_COORDS(pedJanitor, FALSE), v_janitorFrontDoorCoords) > 1.0
					TASK_COWER(pedJanitor, -1)
				ELIF IS_ENTITY_IN_ANGLED_AREA(pedJanitor, <<-99.239250,-10.321966,65.751968>>, <<-107.062050,-7.839327,72.583641>>, 2.750000)
				AND VDIST(GET_ENTITY_COORDS(pedJanitor, FALSE), v_janitorFrontDoorCoords) < VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), v_janitorFrontDoorCoords)
					SET_DOOR_STATE(DOORNAME_JANITORS_APARTMENT, DOORSTATE_UNLOCKED)
					SEQUENCE_INDEX seqRun
					OPEN_SEQUENCE_TASK(seqRun)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-111.5814, -11.7526, 69.5196>>, PEDMOVEBLENDRATIO_SPRINT, DEFAULT_TIME_NEVER_WARP)
						TASK_COWER(NULL)
					CLOSE_SEQUENCE_TASK(seqRun)
					TASK_PERFORM_SEQUENCE(pedJanitor, seqRun)
					CLEAR_SEQUENCE_TASK(seqRun)
				ELSE
					IF bSpeechOnFootFail
						
					ENDIF
					TASK_REACT_AND_FLEE_PED(pedJanitor, PLAYER_PED_ID())
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedJanitor, FALSE)
					SET_PED_COMBAT_ATTRIBUTES(pedJanitor, CA_ALWAYS_FLEE, TRUE)
				ENDIF
				
				SETTIMERA(0)
				iStageProgress++
				PRINTLN("FAIL _@@")
				
			BREAK
			
			CASE 1
				PRINTLN("FAILING _@@")
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() 
				AND TIMERA() > 1500
					Mission_Failed("S1_JSPOOK")
				ENDIF
			BREAK
			
		ENDSWITCH
		
	ELSE
	
		Mission_Failed("S1_FAILED_K")
		
	ENDIF
	
ENDPROC

BOOL bFollowIntoAptPrinted = FALSE
BOOL bBlipIsVeh

PROC stageFollowJanitorOnFoot()

	IF NOT IS_PED_INJURED(pedJanitor)
	AND IS_VEHICLE_DRIVEABLE(vehPlayerCar)
	AND IS_VEHICLE_DRIVEABLE(vehJanitor)
	
		IF NOT DOES_ENTITY_EXIST(objBottle)
			REQUEST_MODEL(PROP_CS_BEER_BOT_01)
			IF HAS_MODEL_LOADED(PROP_CS_BEER_BOT_01)
				objBottle = CREATE_OBJECT_NO_OFFSET(PROP_CS_BEER_BOT_01, <<-109.9875, -6.9250, 70.5236>>)				
				SET_ENTITY_ROTATION(objBottle, <<-0.0000, -0.0000, -91.3911>>)
				SET_ENTITY_VISIBLE(objBottle, FALSE)
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(objBag)
			REQUEST_MODEL(P_LD_HEIST_BAG_S_2)
			IF HAS_MODEL_LOADED(P_LD_HEIST_BAG_S_2)
				objBag = CREATE_OBJECT_NO_OFFSET(P_LD_HEIST_BAG_S_2, <<-109.5757, -6.4225, 70.3903>>)
				SET_ENTITY_ROTATION(objBag, <<0.0000, 0.0000, 153.0623>>)
				SET_ENTITY_VISIBLE(objBag, FALSE)
			ENDIF
		ENDIF
		
		IF NOT DOES_BLIP_EXIST(blipJanitor)
			IF NOT DOES_BLIP_EXIST(blipParkingSpace)
				IF NOT IS_PED_IN_ANY_VEHICLE(pedJanitor)
					blipJanitor = CREATE_BLIP_FOR_PED(pedJanitor)
					bBlipIsVeh = FALSE
					PRINTLN("creating ped blip")
				ELSE
					blipJanitor = CREATE_BLIP_FOR_PED(pedJanitor)
					bBlipIsVeh = TRUE
					SET_BLIP_SCALE(blipJanitor, BLIP_SIZE_VEHICLE)
					PRINTLN("creating veh blip")
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(blipParkingSpace)
			OR (bBlipIsVeh AND NOT IS_PED_IN_ANY_VEHICLE(pedJanitor))
			OR (NOT bBlipIsVeh AND IS_PED_IN_ANY_VEHICLE(pedJanitor))
				PRINTLN("removing janitor blip")
				REMOVE_BLIP(blipJanitor)
			ENDIF
		ENDIF
		
		SWITCH iJanSeqProg
			CASE 0
				iUnlockScenes[0] = -1
				iUnlockScenes[1] = -1
				iUnlockScenes[2] = -1
				iLeadinScene = -1
				iParkStage = 0
				bFollowIntoAptPrinted = FALSE
				SET_JANITORS_DOOR_LOCKED()
				DO_JANITOR_ROUTE_HOME()
				IF NOT DOES_ENTITY_EXIST(pedMarilyn)
					CREATE_HOT_GIRL()
				ELSE
					REPLAY_RECORD_BACK_FOR_TIME(0)
					iJanSeqProg++
				ENDIF
			BREAK
			CASE 1
				DO_JANITOR_ROUTE_HOME()
				IF iParkStage > 2
					iJanSeqProg++
				ENDIF
			BREAK
			CASE 2
				DO_JANITOR_ROUTE_HOME()
				IF (GET_SCRIPT_TASK_STATUS(pedJanitor, SCRIPT_TASK_VEHICLE_PARK) <> PERFORMING_TASK AND GET_SCRIPT_TASK_STATUS(pedJanitor, SCRIPT_TASK_VEHICLE_PARK) <> WAITING_TO_START_TASK)
				OR VDIST2(GET_ENTITY_COORDS(vehJanitor, FALSE), v_parking_spot_for_janitor) < 0.2
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehJanitor)
					iJanSeqProg++
				ENDIF
			BREAK
			CASE 3
				iJanSeqProg++
			BREAK
			CASE 4
				IF NOT IS_PED_INJURED(pedMarilyn)
					IF REQUEST_SCRIPT_AUDIO_BANK("FBI_Heist_1_Tail_Janitor_01")
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-80.867424,-25.283297,76.676826>>, <<-43.429420,-35.593742,56.315414>>, 22.500000)
						OR (NOT IS_ENTITY_OCCLUDED(pedMarilyn) OR NOT IS_ENTITY_OCCLUDED(pedJanitor))
						OR TIMERA() > 3000
							SET_VEHICLE_ENGINE_ON(vehJanitor, FALSE, FALSE)
							CLEAR_PED_TASKS(pedMarilyn)
							iHassleSceneId = CREATE_SYNCHRONIZED_SCENE(vHassleScenePosition, vHassleSceneRotation)
							TASK_SYNCHRONIZED_SCENE(pedMarilyn, iHassleSceneId, "missheistfbisetup1", "hassle_f", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_TAG_SYNC_OUT | SYNCED_SCENE_ABORT_ON_WEAPON_DAMAGE | SYNCED_SCENE_ON_ABORT_STOP_SCENE)
							TASK_LOOK_AT_ENTITY(pedMarilyn, pedJanitor, -1, SLF_WIDEST_YAW_LIMIT)
							SEQUENCE_INDEX seqJan
							OPEN_SEQUENCE_TASK(seqJan)
								TASK_STAND_STILL(NULL, 2000)
								TASK_LEAVE_VEHICLE(NULL, vehJanitor)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-81.9327, -25.7698, 65.3204>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, 282.6097)
							CLOSE_SEQUENCE_TASK(seqJan)
							TASK_PERFORM_SEQUENCE(pedJanitor, seqJan)
							TASK_LOOK_AT_ENTITY(pedJanitor, pedMarilyn, -1, SLF_WIDEST_YAW_LIMIT)
							CLEAR_SEQUENCE_TASK(seqJan)
							iJanSeqProg++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 5
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iHassleSceneId)
					IF GET_SYNCHRONIZED_SCENE_PHASE(iHassleSceneId) > 0.25
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedJanitor, FALSE)) < 40*40
							IF CREATE_CONVERSATION(sSpeech, "FBS1AUD", "FBS1_DAMN", CONV_PRIORITY_MEDIUM)
								iJanSeqProg++
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(sSpeech, "FBS1AUD", "FBS1_DAMN", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES, DO_NOT_ADD_TO_BRIEF_SCREEN)
								iJanSeqProg++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE 6
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iHassleSceneId) 
				AND GET_SYNCHRONIZED_SCENE_PHASE(iHassleSceneId) > 0.44
				AND NOT IS_PED_INJURED(pedMarilyn)
					CLEAR_PED_TASKS(pedMarilyn)
					SEQUENCE_INDEX tempseq
					OPEN_SEQUENCE_TASK(tempseq)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -101.9519, -82.8542, 56.3310 >>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
						TASK_WANDER_STANDARD(NULL)
					CLOSE_SEQUENCE_TASK(tempseq)
					TASK_PERFORM_SEQUENCE(pedMarilyn, tempseq)
					CLEAR_SEQUENCE_TASK(tempseq)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMarilyn, FALSE)
					SETTIMERA(0)
					REQUEST_ANIM_DICT("missheistfbisetup1leadinoutah_1_mcs_1")
					iJanSeqProg++
				ENDIF
			BREAK
			CASE 7
				IF TIMERA() > 2500
					TASK_FOLLOW_NAV_MESH_TO_COORD(pedJanitor, <<-77.0063, -17.4465, 65.4398>>, PEDMOVEBLENDRATIO_WALK)
					iJanSeqProg++
				ENDIF
			BREAK
			CASE 8
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedJanitor)
					IF GET_PED_WAYPOINT_PROGRESS(pedJanitor) > 20
						CLEAR_PED_TASKS(pedJanitor)
						TASK_FOLLOW_NAV_MESH_TO_COORD(pedJanitor,
						GET_ANIM_INITIAL_OFFSET_POSITION("missheistfbisetup1", "unlock_enter_janitor", vUnlockDoorScenePosition, vUnlockDoorSceneRotation)
						, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, 0.1,	ENAV_STOP_EXACTLY, 92.34)
						iJanSeqProg++
					ENDIF
				ELSE
					IF IS_ENTITY_AT_COORD(pedJanitor, <<-77.0063, -17.4465, 65.4398>>, <<2,2,2>>)
						CLEAR_PED_TASKS(pedJanitor)
						TASK_FOLLOW_WAYPOINT_RECORDING(pedJanitor, "janitor_route", 0, EWAYPOINT_START_FROM_CLOSEST_POINT)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJanitor)
					ENDIF
				ENDIF
			BREAK
			CASE 9
				#IF IS_DEBUG_BUILD
					DRAW_DEBUG_LINE(GET_ENTITY_COORDS(pedJanitor, FALSE), GET_ANIM_INITIAL_OFFSET_POSITION("missheistfbisetup1", "unlock_enter_janitor", vUnlockDoorScenePosition, vUnlockDoorSceneRotation), 255, 0, 0)
					DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(VDIST(GET_ENTITY_COORDS(pedJanitor, FALSE), GET_ANIM_INITIAL_OFFSET_POSITION("missheistfbisetup1", "unlock_enter_janitor", vUnlockDoorScenePosition, vUnlockDoorSceneRotation))), <<0.1,0.5,0>>)
				#ENDIF
				IF VDIST(GET_ENTITY_COORDS(pedJanitor, FALSE), GET_ANIM_INITIAL_OFFSET_POSITION("missheistfbisetup1", "unlock_enter_janitor", vUnlockDoorScenePosition, vUnlockDoorSceneRotation)) < 0.7
					iUnlockScenes[0] = CREATE_SYNCHRONIZED_SCENE(vUnlockDoorScenePosition, vUnlockDoorSceneRotation)
					TASK_SYNCHRONIZED_SCENE(pedJanitor, iUnlockScenes[0], "missheistfbisetup1", "unlock_enter_janitor", SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, SLOW_BLEND_IN)
					SETTIMERB(0)
					iJanSeqProg++
				ENDIF
			BREAK
			CASE 10
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iUnlockScenes[0])
				AND GET_SYNCHRONIZED_SCENE_PHASE(iUnlockScenes[0])>0.9
					CLEAR_PED_TASKS(pedJanitor)
					iUnlockScenes[0] = -1
					iUnlockScenes[1] = CREATE_SYNCHRONIZED_SCENE(vUnlockDoorScenePosition, vUnlockDoorSceneRotation)
					TASK_SYNCHRONIZED_SCENE(pedJanitor, iUnlockScenes[1], "missheistfbisetup1", "unlock_loop_janitor", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					SET_SYNCHRONIZED_SCENE_LOOPED(iUnlockScenes[1], TRUE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedJanitor)
					iJanSeqProg++
				ENDIF
			BREAK
			CASE 11
				IF (VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedJanitor, FALSE)) < 12 OR NOT IS_ENTITY_OCCLUDED(pedJanitor))
				AND IS_SYNCHRONIZED_SCENE_RUNNING(iUnlockScenes[1])
					CLEAR_PED_TASKS(pedJanitor)
					iUnlockScenes[1] = -1
					iUnlockScenes[2] = CREATE_SYNCHRONIZED_SCENE(vUnlockDoorScenePosition, vUnlockDoorSceneRotation)
					TASK_SYNCHRONIZED_SCENE(pedJanitor, iUnlockScenes[2], "missheistfbisetup1", "unlock_exit_janitor", NORMAL_BLEND_IN, SLOW_BLEND_OUT)
					IF DOES_ENTITY_EXIST(GET_CLOSEST_OBJECT_OF_TYPE(<<-107.5401, -9.0258, 70.6696>>, 2, V_ILEV_JANITOR_FRONTDOOR, FALSE))
						FREEZE_ENTITY_POSITION(GET_CLOSEST_OBJECT_OF_TYPE(<<-107.5401, -9.0258, 70.6696>>, 2, V_ILEV_JANITOR_FRONTDOOR, FALSE), FALSE)
						PLAY_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_OBJECT_OF_TYPE(<<-107.5401, -9.0258, 70.6696>>, 2, V_ILEV_JANITOR_FRONTDOOR, FALSE), iUnlockScenes[2], "unlock_exit_door", "missheistfbisetup1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
					ENDIF
					iJanSeqProg++
				ENDIF
			BREAK
			CASE 12
				IF IS_SYNCHRONIZED_SCENE_RUNNING(iUnlockScenes[2])
					IF GET_SYNCHRONIZED_SCENE_PHASE(iUnlockScenes[2]) > 0.9
						CLEAR_PED_TASKS(pedJanitor)
						IF DOES_ENTITY_EXIST(GET_CLOSEST_OBJECT_OF_TYPE(<<-107.5401, -9.0258, 70.6696>>, 2, V_ILEV_JANITOR_FRONTDOOR, FALSE))
							STOP_SYNCHRONIZED_ENTITY_ANIM(GET_CLOSEST_OBJECT_OF_TYPE(<<-107.5401, -9.0258, 70.6696>>, 2, V_ILEV_JANITOR_FRONTDOOR, FALSE), NORMAL_BLEND_OUT, TRUE)
							SET_JANITORS_DOOR_LOCKED(FALSE)
						ENDIF
						iUnlockScenes[2] = -1
						SET_DOOR_STATE(DOORNAME_JANITORS_APARTMENT, DOORSTATE_UNLOCKED)
						PRINTLN("\n\n\n\n Anims done \n")
						TASK_FOLLOW_NAV_MESH_TO_COORD(pedJanitor, <<-111.4550, -7.0131, 69.5283>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 283)
						FORCE_PED_MOTION_STATE(pedJanitor, MS_ON_FOOT_WALK)
						SET_ENTITY_COLLISION(pedJanitor, TRUE)
						SETTIMERA(0)
						iJanSeqProg++
					ENDIF
				ENDIF
			BREAK
			CASE 13
				IF TIMERA() > 1000
					iJanSeqProg++
				ENDIF
			BREAK
			CASE 14
				IF GET_SCRIPT_TASK_STATUS(pedJanitor, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
				AND GET_SCRIPT_TASK_STATUS(pedJanitor, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
				AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(iLeadinScene)
					IF IS_ENTITY_IN_ANGLED_AREA(pedJanitor, <<-113.705902,-8.827343,69.576927>>, <<-108.326988,-10.887757,72.769081>>, 8.750000)
						IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedJanitor)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedJanitor, TRUE)
							SEQUENCE_INDEX seqTemp
							OPEN_SEQUENCE_TASK(seqTemp)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vLeadinPosition, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP, 0.1, ENAV_STOP_EXACTLY)
							TASK_ACHIEVE_HEADING(NULL, vLeadinRotation.z)
							CLOSE_SEQUENCE_TASK(seqTemp)
							TASK_PERFORM_SEQUENCE(pedJanitor, seqTemp)
							CLEAR_SEQUENCE_TASK(seqTemp)
						ENDIF
					ENDIF
				ENDIF
				
				REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2223492
				
				IF (IS_ENTITY_OCCLUDED(pedJanitor) AND IS_PED_IN_JANITORS_APARTMENT(pedJanitor))
				OR (IS_PED_IN_JANITORS_APARTMENT(pedJanitor) AND NOT IS_PED_IN_JANITORS_APARTMENT(PLAYER_PED_ID()) AND DOOR_SYSTEM_GET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_JANITORS_APARTMENT)) < 0.05)
				OR (VDIST(GET_ENTITY_COORDS(pedJanitor, FALSE), vLeadinPosition) < 0.4 AND  (ABSF(GET_ENTITY_HEADING(pedJanitor)-vLeadinRotation.z)) < 10 OR ABSF(GET_ENTITY_HEADING(pedJanitor) - vLeadinRotation.z - 360) < 10)
					CLEAR_PED_TASKS(pedJanitor)
					PRINTLN("\n\n\n warping to anim scene")
					iLeadinScene = CREATE_SYNCHRONIZED_SCENE(vLeadinPosition, vLeadinRotation)
					TASK_SYNCHRONIZED_SCENE(pedJanitor, iLeadinScene, "missheistfbisetup1leadinoutah_1_mcs_1", "leadin_janitor_idle_01", SLOW_BLEND_IN, NORMAL_BLEND_OUT)
					SET_SYNCHRONIZED_SCENE_LOOPED(iLeadinScene, TRUE)
					
					//Make the bag and beer visible, and remove the bag from the janitor.
					IF DOES_ENTITY_EXIST(objBag)
						SET_ENTITY_VISIBLE(objBag, TRUE)
					ENDIF
					
					IF DOES_ENTITY_EXIST(objBottle)
						SET_ENTITY_VISIBLE(objBottle, TRUE)
					ENDIF
					
					SET_PED_COMPONENT_VARIATION(pedJanitor, PED_COMP_SPECIAL2, 0, 0, 0) //(task)
					
					iJanSeqProg++
				ENDIF
			BREAK
			CASE 15
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-113.705902,-8.827343,69.576927>>, <<-108.326988,-10.887757,72.769081>>, 8.750000)
					CLEAR_PED_TASKS(pedJanitor)
					iLeadinScene = CREATE_SYNCHRONIZED_SCENE(vLeadinPosition, vLeadinRotation)
					TASK_SYNCHRONIZED_SCENE(pedJanitor, iLeadinScene, "missheistfbisetup1leadinoutah_1_mcs_1", "leadin_janitor_idle_action", NORMAL_BLEND_IN, INSTANT_BLEND_OUT)
					iJanSeqProg++
				ENDIF
			BREAK
		ENDSWITCH
		
	ENDIF
	
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iJanSeqProg), <<0.1, 0.2, 0>>)
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iHassleSceneId)
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(iHassleSceneId)), <<0.1, 0.22, 0>>)
		ENDIF
	#ENDIF
	
	IF NOT bFollowIntoAptPrinted
		IF IS_PED_IN_JANITORS_APARTMENT(pedJanitor)
		AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), v_janitorFrontDoorCoords) > 2*2
			PRINT("S1_FOLLOW2", DEFAULT_GOD_TEXT_TIME, 1)
			bFollowIntoAptPrinted = TRUE
		ENDIF
	ENDIF
	
	IF (NOT IS_PED_INJURED(pedJanitor) AND IS_VEHICLE_DRIVEABLE(vehJanitor) AND IS_VEHICLE_DRIVEABLE(vehPlayerCar))
	OR iStageProgress = 0
	
		SWITCH iStageProgress
		
			CASE 0
			
				REQUEST_WAYPOINT_RECORDING("janitor_route")
				REQUEST_MODEL(A_F_Y_Hipster_03)
				
				SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 1, 1)
				
				SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
				
				IF bSkip
				
					REQUEST_VEHICLE_RECORDING(1, "FBIs1UBER")
					REQUEST_WAYPOINT_RECORDING(sParkWaypointRec)
					
					IF NOT IS_REPLAY_BEING_SET_UP()
					
						SET_FOCUS_POS_AND_VEL(<< -53.0454, 18.6296, 71.1259 >>, <<0,0,0>>)
						NEW_LOAD_SCENE_START(<< -53.0454, 18.6296, 71.1259 >>, <<COS(167.0193+90), SIN(167.0193+90), 0>>, 125)
						
						INT iTimeNow
						iTimeNow = GET_GAME_TIMER()
						
						WHILE NOT IS_NEW_LOAD_SCENE_LOADED() AND GET_GAME_TIMER() - iTimeNow < 10000 
							WAIT(0)
						ENDWHILE
						
						CLEAR_FOCUS()
						NEW_LOAD_SCENE_STOP()
						
					ENDIF
					
					IF IS_REPLAY_IN_PROGRESS()
					AND IS_REPLAY_BEING_SET_UP()
						CREATE_VEHICLE_FOR_REPLAY(vehPlayerCar, vPlayerCarCoords, fPlayerCarHeading, FALSE, FALSE, FALSE, FALSE, TRUE)
					ENDIF
					
					WHILE NOT SAFELY_CREATE_PLAYER_VEHICLE(<< -53.0454, 18.6296, 71.1259 >>, 167.0193, TRUE, TRUE)
						WAIT(0)
					ENDWHILE
					
					WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED(sParkWaypointRec)
						WAIT(0)
					ENDWHILE
					
					// reset camera behind player
					SET_GAMEPLAY_CAM_RELATIVE_PITCH() 
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					
					CLEAR_AREA(<<-83.2189, -23.8057, 65.3210>>, 50, TRUE)
					
					WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "FBIs1UBER")
					OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("janitor_route")
					OR NOT HAS_MODEL_LOADED(A_F_Y_Hipster_03)
						WAIT(0)
					ENDWHILE
					
					WHILE NOT DOES_ENTITY_EXIST(pedMarilyn)
						CREATE_HOT_GIRL()
						WAIT(0)
					ENDWHILE
					
					FORCE_RESET_LESTER()
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP(vehPlayerCar)
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST(pedJanitor)
					OR NOT IS_VEHICLE_DRIVEABLE(vehJanitor)
						WHILE NOT CREATE_JANITOR_IN_CAR(<<-59.3559, -7.0657, 69.2725>>, 165.8312)
							WAIT(0)
						ENDWHILE
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehJanitor)
					AND NOT IS_PED_INJURED(pedJanitor)
					AND IS_PED_IN_VEHICLE(pedJanitor,vehJanitor)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehJanitor)
						SET_VEHICLE_ENGINE_ON(vehJanitor, TRUE, TRUE)
						SET_VEHICLE_FORWARD_SPEED(vehJanitor, 5)
					ENDIF
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_FOLLOW_JANITOR")
						START_AUDIO_SCENE("AGENCY_H_1_FOLLOW_JANITOR")
					ENDIF
					
					TRIGGER_MUSIC_EVENT("AH1_RESTART")
					
				ENDIF
				
				IF NOT IS_PED_INJURED(pedJanitor)
					TASK_LOOK_AT_ENTITY(pedLester, pedJanitor, -1, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_HIGH)
					TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedJanitor, -1, SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_HIGH)
				ENDIF
				
				apartment = GET_INTERIOR_AT_COORDS(v_apartment_interior)
				PIN_INTERIOR_IN_MEMORY(apartment)
				SET_INTERIOR_ACTIVE(apartment, TRUE)
				CAP_INTERIOR(apartment, FALSE)
				
				iJanSeqProg = 0
				CHECK_PEDS_FOR_DIALOGUE()
				bParkUpDialogue = FALSE
				SET_WIDESCREEN_BORDERS(FALSE, 0)
				iStageProgress++
				
			BREAK
			
			CASE 1
			
				IF NOT IS_SCREEN_FADED_IN()
				AND NOT IS_SCREEN_FADING_IN()
					SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCar)
					SET_VEHICLE_ENGINE_ON(vehPlayerCar, TRUE, TRUE)
					PRINTLN("SETTING FORWARD SPEED")
					SET_VEHICLE_FORWARD_SPEED(vehPlayerCar, 12)
					DO_SCREEN_FADE_IN(500)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
				
					BLIP_MANAGER_FOR_COORD(vehPlayerCar, blipPlayerCar, v_parkingSpace, blipParkingSpace)
					
					IF NOT bParkUpDialogue
					AND NOT IS_PED_INJURED(pedJanitor)
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedJanitor, FALSE)) < POW(50,2)
						AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY(PLAYER_PED_ID(), vehJanitor)
						AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
							IF CREATE_CONVERSATION(sSpeech, "FBS1AUD", "FBS1_ARRIVE", CONV_PRIORITY_HIGH)
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								bParkUpDialogue = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
						IF (IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_parkingSpace, g_vAnyMeansLocate) OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), v_parkingSpace-<<0,0,0.05>>, <<1.0, 1.0, 2.5>>, TRUE))
						AND NOT IS_ENTITY_UPSIDEDOWN(vehPlayerCar)
						AND CAN_PLAYER_START_CUTSCENE()
							CLEAR_PRINTS()
							IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayerCar)
								SETTIMERB(0)
								REMOVE_BLIP(blipParkingSpace)
								iStageProgress++
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						CLEAR_PRINTS()
						REMOVE_BLIP(blipParkingSpace)
						REMOVE_BLIP(blipPlayerCar)
						iStageProgress++
					ENDIF
					
				ENDIF
				
			BREAK
			
			CASE 2
				IF iJanSeqProg > 1
					SETTIMERB(0)
					REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 3
				IF TIMERB() > 1500
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
						IF CREATE_CONVERSATION(sSpeech, "FBS1AUD", "FBS1_OUT", CONV_PRIORITY_VERY_HIGH)
							SETTIMERB(0)
							iStageProgress++
						ENDIF
					ELSE
						SETTIMERB(1500)
						iStageProgress++
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
				IF TIMERB() > 1500
					iStageProgress++
				ENDIF
			BREAK
			
			CASE 5
			
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedJanitor)
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(pedJanitor, 1.0)
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-73.216858,-16.197964,65.364883>>, <<-104.949959,-4.713087,69.378258>>, 6.00000)
					IF NOT IS_CUTSCENE_ACTIVE()
						REQUEST_CUTSCENE(sCutsceneName)
					ENDIF
				ENDIF
				
				IF IS_PED_IN_JANITORS_APARTMENT(PLAYER_PED_ID())
				AND IS_PED_IN_JANITORS_APARTMENT(pedJanitor)
				AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())
					IF IS_VALID_INTERIOR(apartment)
						UNPIN_INTERIOR(apartment)
					ENDIF
					IF DOES_ENTITY_EXIST(pedMarilyn)
						DELETE_PED(pedMarilyn)
					ENDIF
					SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_HIPSTER_01)
					GO_TO_STAGE(STAGE_JANITOR_CUT)
				ENDIF
				
			BREAK
			
		ENDSWITCH
		
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), v_janitorFrontDoorCoords) < 15
			IF NOT bRequestNextStage
				REQUEST_ASSETS_FOR_STAGE(STAGE_JANITOR_CUT)
				bRequestNextStage = TRUE
			ENDIF
			IF NOT IS_CUTSCENE_ACTIVE()
				REQUEST_CUTSCENE(sCutsceneName)
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(pedJanitor)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedJanitor, FALSE)) > 100
				PRINT_GOD_TEXT(gGoBackToJan)
			ENDIF
		ENDIF
		
		IF mission_stage <> STAGE_FOLLOW_JANITOR_ON_FOOT
			SET_DOOR_STATE(DOORNAME_JANITORS_APARTMENT, DOORSTATE_FORCE_CLOSED_THIS_FRAME)
			SET_DOOR_STATE(DOORNAME_JANITORS_APARTMENT, DOORSTATE_LOCKED)
		ENDIF
		
		// end
		
		IF IS_VEHICLE_DRIVEABLE(vehJanitor)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehJanitor)
				VEHICLE_INDEX vehCurrent
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehCurrent = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				ELSE
					vehCurrent = GET_PLAYERS_LAST_VEHICLE()
				ENDIF
				IF DOES_ENTITY_EXIST(vehCurrent)
				AND IS_VEHICLE_DRIVEABLE(vehCurrent)
					IF IS_ENTITY_TOUCHING_ENTITY(vehCurrent, vehJanitor)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehJanitor)
						TASK_SMART_FLEE_PED(pedJanitor, PLAYER_PED_ID(), 200, 10000, TRUE)
						KILL_ANY_CONVERSATION()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						Mission_Failed("S1_JSPOOK")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_FOLLOW_ON_FOOT")
				START_AUDIO_SCENE("AGENCY_H_1_FOLLOW_ON_FOOT")
			ENDIF
		ENDIF
		
		IF IS_GAMEPLAY_HINT_ACTIVE()
			IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_FOCUS_ON_JANITOR")
				START_AUDIO_SCENE("AGENCY_H_1_FOCUS_ON_JANITOR")
			ENDIF
		ELSE
			IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_FOCUS_ON_JANITOR")
				STOP_AUDIO_SCENE("AGENCY_H_1_FOCUS_ON_JANITOR")
			ENDIF
		ENDIF
		
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(pedJanitor, FALSE)) > 200
			CLEAR_PRINTS()
			KILL_ANY_CONVERSATION()
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			Mission_Failed("S1_FAILLV")
		ENDIF
		
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iStageProgress), <<0.02, 0.02, 0>>)
			DRAW_DEBUG_TEXT_2D(GET_STRING_FROM_INT(iJanSeqProg), <<0.04, 0.02, 0>>)
		#ENDIF
		
	ENDIF
	
	// all fails for spooking the janitor
	IF NOT IS_PED_INJURED(pedJanitor)
	
		IF IS_VEHICLE_DRIVEABLE(vehJanitor)
		AND (IS_PED_IN_VEHICLE(pedJanitor, vehJanitor) OR VDIST2(GET_ENTITY_COORDS(pedJanitor, FALSE), GET_ENTITY_COORDS(vehJanitor, FALSE)) < 5*5)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), vehJanitor)
					GO_TO_STAGE(STAGE_JANITOR_SPOOKED_ON_FOOT)
				ENDIF
			ENDIF
		ENDIF
	
		IF HAS_PLAYER_SPOOKED_JANITOR_ON_FOOT()
			GO_TO_STAGE(STAGE_JANITOR_SPOOKED_ON_FOOT)
		ENDIF
		
		IF iJanSeqProg < 7 
		AND IS_ENTITY_IN_ANGLED_AREA(pedJanitor, <<-87.364937,-31.341928,63.480988>>, <<-80.962273,-15.885396,70.972168>>, 11.750000)
		AND (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-87.364937,-31.341928,63.480988>>, <<-80.962273,-15.885396,70.972168>>, 11.750000) 
				OR IS_ENTITY_IN_ANGLED_AREA(vehPlayerCar, <<-87.364937,-31.341928,63.480988>>, <<-80.962273,-15.885396,70.972168>>, 11.750000))
			GO_TO_STAGE(STAGE_JANITOR_SPOOKED_ON_FOOT)
		ENDIF
		
	ELIF IS_PED_INJURED(pedJanitor)
		IF NOT IS_PED_INJURED(pedMarilyn)
			CLEAR_PED_TASKS(pedMarilyn)
			TASK_SMART_FLEE_PED(pedMarilyn, PLAYER_PED_ID(), 1000, -1)
		ENDIF
		Mission_Failed("S1_FAILED_K")
	ENDIF
	
ENDPROC

BOOL bBagSet = FALSE

PROC DO_JANITOR_SCENE()

	SWITCH iStageProgress
	
		CASE 0
		
			b_JanitorHome = TRUE
			
			SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
			
			IF bSkip
				SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 1, 1)
				IF NOT IS_REPLAY_BEING_SET_UP()
					DO_PLAYER_MAP_WARP_WITH_LOAD(v_janitorFrontDoorCoords + <<0.5, 1, 0>>)
				ENDIF
				SAFELY_CREATE_PLAYER_VEHICLE(v_parkingSpace, f_park_heading, FALSE, TRUE)
				SET_ENTITY_QUATERNION(vehPlayerCar, -0.0341, -0.1053, 0.9890, 0.0985)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCar)
				WHILE NOT DOES_ENTITY_EXIST(carHotGirl)
					CREATE_HOT_GIRL()
					WAIT(0)
				ENDWHILE
				IF DOES_ENTITY_EXIST(pedMarilyn)
					DELETE_PED(pedMarilyn)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(pedJanitor)
					REQUEST_MODEL(S_M_M_JANITOR)
					WHILE NOT HAS_MODEL_LOADED(S_M_M_JANITOR)
						WAIT(0)
					ENDWHILE
					WHILE NOT CREATE_JANITOR_IN_CAR(v_parking_spot_for_janitor, f_parking_spot_for_janitor)
						WAIT(0)
					ENDWHILE
					FORCE_RESET_LESTER()
					IF NOT IS_PED_INJURED(pedJanitor)
						SET_ENTITY_COORDS(pedJanitor, v_janitorFrontDoorCoords)
					ENDIF
				ENDIF
				END_REPLAY_SETUP(NULL)
			ENDIF
			bBagSet = FALSE
			TRIGGER_MUSIC_EVENT("AH1_STOP")
			REMOVE_ANIM_DICT("missheistfbisetup1leadinoutah_1_mcs_1")
			IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_FOLLOW_ON_FOOT")
				STOP_AUDIO_SCENE("AGENCY_H_1_FOLLOW_ON_FOOT")
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(8.0, 00.0, REPLAY_IMPORTANCE_HIGHEST)
			
			CHECK_PEDS_FOR_DIALOGUE()
			iStageProgress++
		BREAK
		
		CASE 1
			IF NOT IS_CUTSCENE_ACTIVE()
				REQUEST_CUTSCENE(sCutsceneName)
				PRINTLN("\n\n\n\n\n\n\n\nCUTSCENE WAS REQUESTED \n\n\n\n\n\n\n\n")
			ENDIF
			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE(sCutsceneName)
				//AND DOES_ENTITY_EXIST(objDoorGrab)
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					IF NOT IS_PED_INJURED(pedJanitor)
						REGISTER_ENTITY_FOR_CUTSCENE(pedJanitor, "Janitor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					START_CUTSCENE()
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					REQUEST_ASSETS_FOR_STAGE(STAGE_DRIVE_BACK_TO_SAFEHOUSE)
					iStageProgress++
				ELSE
					REMOVE_CUTSCENE()
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			
			DO_SCREEN_FADE_IN(500)
			SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
			
			IF DOES_ENTITY_EXIST(objBag)
				DELETE_OBJECT(objBag)
			ENDIF
			
			IF DOES_ENTITY_EXIST(objBottle)
				DELETE_OBJECT(objBottle)
			ENDIF
			
			CLEAR_AREA(<< -98.8604, -10.5318, 65.5199 >>, 100, TRUE)
			CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
			
			iStageProgress++
			
		BREAK
		
		CASE 3
		
			IF NOT bBagSet
				SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 1, 1)
			
				IF GET_CUTSCENE_TIME() > 27500
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 1, 1)
					RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
					bBagSet = TRUE
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael", PLAYER_ZERO)
				IF NOT bBagSet
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 1, 1)
					RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
					bBagSet = TRUE
				ENDIF
				
				//Set appropriate heading for Michael in first person
				if GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 251.0110)
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1000)
				ELSE
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 287.4864)
					TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), << -98.8604, -10.5318, 65.5199 >>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				REPLAY_STOP_EVENT()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			
			IF NOT IS_CUTSCENE_ACTIVE()
//				IF DOES_ENTITY_EXIST(objDoorGrab)
//					SET_ENTITY_VISIBLE(objDoorGrab, TRUE)
//					FREEZE_ENTITY_POSITION(objDoorGrab, TRUE)
//				ENDIF
				IF DOES_ENTITY_EXIST(pedJanitor)
					DELETE_PED(pedJanitor)
				ENDIF
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				//Set appropriate heading for Michael in first person
				if GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 251.0110)
				ELSE
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 287.4864)
				ENDIF
				
				GO_TO_STAGE(STAGE_DRIVE_BACK_TO_SAFEHOUSE)
			ENDIF
			#IF IS_DEBUG_BUILD
				//DEBUG_CUTSCENE_CONTROL()
			#ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC

BOOL bGoUpstairs = FALSE

FUNC BOOL CREATE_FRANKLIN_FOR_OUTRO()
	IF CREATE_PLAYER_PED_ON_FOOT(pedFranklin, CHAR_FRANKLIN, <<705.4539, -965.5662, 29.3953>>, 273.3178)
		SET_PED_COMP_ITEM_CURRENT_SP(pedFranklin, COMP_TYPE_OUTFIT, OUTFIT_P1_HOODIE_AND_JEANS_2, FALSE)
		TASK_START_SCENARIO_AT_POSITION(pedFranklin, "PROP_HUMAN_SEAT_CHAIR", <<704.85626, -965.38519, 29.82450>>, 269.2285)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DRIVE_BACK_TO_SAFEHOUSE()

	IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vPlayerCarCoords) < 1000
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0)
	ENDIF
	
	IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_R)) = DOORSTATE_LOCKED
	OR DOOR_SYSTEM_GET_DOOR_PENDING_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_R)) = DOORSTATE_LOCKED
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_R), 0)
		SET_DOOR_STATE(DOORNAME_SWEATSHOP_R, DOORSTATE_UNLOCKED)
	ENDIF
	
	IF DOOR_SYSTEM_GET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_L)) = DOORSTATE_LOCKED
	OR DOOR_SYSTEM_GET_DOOR_PENDING_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_L)) = DOORSTATE_LOCKED
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_L), 0)
		SET_DOOR_STATE(DOORNAME_SWEATSHOP_L, DOORSTATE_UNLOCKED)
	ENDIF
	
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<716.190491,-967.218445,31.585453>>,<<8.000000,9.000000,2.500000>>)
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
	ENDIF
	
	SWITCH iStageProgress
	
		CASE 0
		
			gGetBackInCar.played = FALSE
			bGoUpstairs = FALSE
			
			SET_JANITORS_DOOR_LOCKED()
			
			SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
			
			IF bSkip
			
				SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 1, 1)
				REQUEST_COLLISION_AT_COORD(v_janitorFrontDoorCoords)
				
				IF IS_VALID_INTERIOR(apartment)
					UNPIN_INTERIOR(apartment)
					CAP_INTERIOR(apartment, TRUE)
				ENDIF
				
				IF IS_REPLAY_BEING_SET_UP()
				AND IS_REPLAY_IN_PROGRESS()
					CREATE_VEHICLE_FOR_REPLAY(vehPlayerCar, v_parkingSpace, f_park_heading, FALSE, FALSE, FALSE, FALSE, TRUE)
				ENDIF
				
				WHILE NOT SAFELY_CREATE_PLAYER_VEHICLE(v_parkingSpace, f_park_heading, FALSE, TRUE)
					WAIT(0)
				ENDWHILE
				
				WHILE NOT DOES_ENTITY_EXIST(carHotGirl)
					CREATE_HOT_GIRL()
					WAIT(0)
				ENDWHILE
				
				IF DOES_ENTITY_EXIST(pedMarilyn)
					DELETE_PED(pedMarilyn)
				ENDIF
				
				IF NOT IS_REPLAY_BEING_SET_UP()
				
					WHILE NOT HAS_COLLISION_LOADED_AROUND_ENTITY(PLAYER_PED_ID())
						WAIT(0)
					ENDWHILE
					
					SET_FOCUS_POS_AND_VEL(<< -105.0312, -8.6584, 69.5201 >>, <<0,0,0>>)
					NEW_LOAD_SCENE_START(<< -105.0312, -8.6584, 69.5201 >>, <<COS(259.2958+90), SIN(259.2958+90), 0>>, 25)
					SETTIMERA(0)
					WHILE NOT IS_NEW_LOAD_SCENE_LOADED() AND TIMERA() < 10000
						WAIT(0)
					ENDWHILE
					CLEAR_FOCUS()
					NEW_LOAD_SCENE_STOP()
					
				ENDIF
				
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 1, 1)
				RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << -105.0312, -8.6584, 69.5201 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 259.2958)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				FORCE_RESET_LESTER()
				SET_ENTITY_COORDS_NO_OFFSET(vehPlayerCar, v_parkingSpace)
				SET_ENTITY_ROTATION(vehPlayerCar, <<-11.3149, 1.4111, 167.8415>>)
				SET_ENTITY_QUATERNION(vehPlayerCar, -0.0226, -0.0967, 0.9893, 0.1066)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayerCar)
				
				WHILE NOT CREATE_JANITOR_IN_CAR(v_parking_spot_for_janitor, f_parking_spot_for_janitor)
					WAIT(0)
				ENDWHILE
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP()
				ENDIF
				IF NOT IS_PED_INJURED(pedLester)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedLester, FALSE)
				ENDIF
				TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), << -98.8604, -10.5318, 65.5199 >>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
				
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
			
			IF DOES_ENTITY_EXIST(pedJanitor)
				DELETE_PED(pedJanitor)
			ENDIF
			CHECK_PEDS_FOR_DIALOGUE()
			iStageProgress++
			FADE_IN()
			SETTIMERA(0)
			
		BREAK
		
		CASE -1
			IF DOES_ENTITY_EXIST(pedJanitor)
				DELETE_PED(pedJanitor)
			ENDIF
			CHECK_PEDS_FOR_DIALOGUE()
			iStageProgress++
			FADE_IN()
			SETTIMERA(0)
			iStageProgress = 1
		BREAK
		
		CASE 1
		
			IF NOT IS_SCREEN_FADED_IN()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(500)
			ENDIF
			
			IF TIMERA() > 1000
				IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = PERFORMING_TASK
					CLEAR_PED_TASKS(PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
				IF NOT DOES_BLIP_EXIST(blipPlayerCar)
					blipPlayerCar = CREATE_BLIP_FOR_VEHICLE(vehPlayerCar)
					PRINT_NOW("S1_BKINCAR", DEFAULT_GOD_TEXT_TIME, 1)
				ENDIF
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					IF NOT IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_DRIVE_TO_FACTORY")
						START_AUDIO_SCENE("AGENCY_H_1_DRIVE_TO_FACTORY")
					ENDIF
					TRIGGER_MUSIC_EVENT("AH1_BACK_IN_CAR")
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0)
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					iStageProgress++
					SETTIMERA(0)
				ELSE
					IF NOT IS_PED_DEAD_OR_DYING(PLAYER_PED_ID())
						IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), GET_ENTITY_COORDS(vehPlayerCar, FALSE))<20*20
							IF NOT IS_PED_DEAD_OR_DYING(pedLester)
								IF NOT IS_PED_HEADTRACKING_PED(pedLester, PLAYER_PED_ID())
									TASK_LOOK_AT_ENTITY(pedLester, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV | SLF_EXTEND_YAW_LIMIT, SLF_LOOKAT_HIGH)
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_PED_DEAD_OR_DYING(pedLester)
								IF IS_PED_HEADTRACKING_PED(pedLester, PLAYER_PED_ID())
									TASK_CLEAR_LOOK_AT(pedLester)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 2
			IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			AND NOT IS_PED_INJURED(pedLester)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					IF IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
						IF DOES_BLIP_EXIST(blipPlayerCar)
							REMOVE_BLIP(blipPlayerCar)
						ENDIF
						IF TIMERA() > 500
							IF CREATE_CONVERSATION(sSpeech, "FBS1AUD", "FBS1_PKGE", CONV_PRIORITY_VERY_HIGH)
								SETTIMERA(0)
								iStageProgress++
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT DOES_BLIP_EXIST(blipPlayerCar)
						blipPlayerCar = CREATE_BLIP_FOR_VEHICLE(vehPlayerCar)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			AND NOT IS_PED_INJURED(pedLester)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					IF IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
						IF DOES_BLIP_EXIST(blipPlayerCar)
							REMOVE_BLIP(blipPlayerCar)
						ENDIF
						IF CREATE_CONVERSATION(sSpeech, "FBS1AUD", "FBS1_GOHOME", CONV_PRIORITY_VERY_HIGH)
							SETTIMERA(0)
							iStageProgress++
						ENDIF
					ENDIF
				ELSE
					IF NOT DOES_BLIP_EXIST(blipPlayerCar)
						blipPlayerCar = CREATE_BLIP_FOR_VEHICLE(vehPlayerCar)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
			AND NOT IS_PED_INJURED(pedLester)
				IF TIMERA() > 1000
					BLIP_MANAGER_FOR_COORD(vehPlayerCar, blipPlayerCar,  vEndPoint, blipOfficeFBI)
				ENDIF
				IF TIMERA() > 3000
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				AND (NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED() OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0)
					IF NOT gGoHome.played
						PRINT_GOD_TEXT(gGoHome)
					ENDIF
				ENDIF
				IF TIMERA() > 11000
					MANAGE_VEHICLE_CONVERSATION(dToHome1.label, vehPlayerCar)
				ENDIF
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vEndPoint) < 200*200
					IF NOT DOES_ENTITY_EXIST(vehFranklin)
						IF CREATE_PLAYER_VEHICLE(vehFranklin, CHAR_FRANKLIN,<< 711.2252, -979.7821, 23.1003 >>, 43.2293, TRUE, VEHICLE_TYPE_CAR)
							SET_VEHICLE_ON_GROUND_PROPERLY(vehFranklin)
							APPLY_FORCE_TO_ENTITY(vehFranklin, APPLY_TYPE_FORCE, <<0,0,-0.01>>, <<0,0,0>>, 0, TRUE, TRUE, FALSE)
							SET_VEHICLE_DOORS_LOCKED(vehFranklin, VEHICLELOCK_LOCKED)
						ENDIF
					ENDIF
				ENDIF
				IF DOES_BLIP_EXIST(blipOfficeFBI)
				AND IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
				AND NOT IS_ENTITY_UPSIDEDOWN(vehPlayerCar)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vEndPoint, g_vAnyMeansLocate, TRUE)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<723.490540,-983.816345,19.161726>>, <<710.227783,-984.953735,26.114151>>, 12.250000)
						iStageProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
				REQUEST_CLIP_SET("move_lester_CaneUp")
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehPlayerCar, 8, 5)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON)
					SETTIMERA(0)
					REMOVE_BLIP(blipOfficeFBI)
					KILL_FACE_TO_FACE_CONVERSATION()
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION( sSpeech, sConvBlock,  "FBS1_HOME2", CONV_PRIORITY_HIGH)
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
					iStageProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
				IF GET_ENTITY_SPEED(vehPlayerCar) <0.5
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
						TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), vehPlayerCar)
					ENDIF
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					blipOfficeFBI = CREATE_BLIP_FOR_COORD(<<706.5483, -965.2577, 29.4179>>)
					iStageProgress++
					SETTIMERA(0)
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			IF TIMERA() > 250
			AND HAS_CLIP_SET_LOADED("move_lester_CaneUp")
				IF IS_VEHICLE_DRIVEABLE(vehPlayerCar)
					IF NOT IS_PED_INJURED(pedLester)
					AND IS_PED_IN_VEHICLE(pedLester, vehPlayerCar)
						SET_PED_WEAPON_MOVEMENT_CLIPSET(pedLester, "move_lester_CaneUp")
						TASK_LOOK_AT_ENTITY(pedLester, PLAYER_PED_ID(), -1)
						SEQUENCE_INDEX seqTemp
						OPEN_SEQUENCE_TASK(seqTemp)
						TASK_LEAVE_VEHICLE(NULL, vehPlayerCar)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<713.0986, -962.1581, 29.3953>>, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_NEVER_WARP)
						TASK_ACHIEVE_HEADING(NULL, 185.6443)
						CLOSE_SEQUENCE_TASK(seqTemp)
						TASK_PERFORM_SEQUENCE(pedLester, seqTemp)
						CLEAR_SEQUENCE_TASK(seqTemp)
					ENDIF
				ENDIF
				REQUEST_SCRIPT("lesterHandler")
				
				//Make sure the first few items are displaying on the planning 
				//board as we approach the end of the mission. -BenR
				SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_AGENCY, PBDG_0, TRUE)
				
				PRINTLN("\n\n\n\n\n\n\n\nCUTSCENE WAS REQUESTED \n\n\n\n\n\n\n\n")
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 9
			IF NOT IS_PED_INJURED(pedLester)
				SET_PED_MOVE_RATE_OVERRIDE(pedLester, 0.8)
			ENDIF
			IF NOT DOES_BLIP_EXIST(blipOfficeFBI)
				blipOfficeFBI = CREATE_BLIP_FOR_COORD(<<706.5483, -965.2577, 29.4179>>)
			ENDIF
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<710.971741,-964.134949,30.645330>>,<<1.000000,5.500000,1.250000>>)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						WAIT(0)
					ENDWHILE
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(), 0)
				ENDIF
				IF DOES_BLIP_EXIST(blipOfficeFBI)
					REMOVE_BLIP(blipOfficeFBI)
				ENDIF
				CLEAR_PRINTS()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 10
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP()
			ENDIF
			IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				IF NOT IS_PED_INJURED(pedLester)
					REGISTER_ENTITY_FOR_CUTSCENE(pedLester, "Lester", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF NOT IS_PED_INJURED(pedFranklin)
					REGISTER_ENTITY_FOR_CUTSCENE(pedFranklin, "Franklin", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				START_CUTSCENE()
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				iStageProgress=100
			ENDIF
		BREAK
		
		CASE 100
			CLEAR_AREA(<<710.971741,-964.134949,30.645330>>, 20, TRUE)
			DO_SCREEN_FADE_IN(500)
			iStageProgress++
		BREAK
		
		CASE 101
			IF DOES_ENTITY_EXIST(objStick)
				DELETE_OBJECT(objStick)
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("AGENCY_H_1_DRIVE_TO_FACTORY")
				STOP_AUDIO_SCENE("AGENCY_H_1_DRIVE_TO_FACTORY")
			ENDIF
			IF DOES_ENTITY_EXIST(vehFranklin)
				DELETE_VEHICLE(vehFranklin)
			ENDIF
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lester")
			AND NOT IS_PED_INJURED(pedLester)
				IF HAS_SCRIPT_LOADED("lesterHandler")
					StructLesterHandover sLesterHandover
					sLesterHandover.pedHandover = pedLester
					START_NEW_SCRIPT_WITH_ARGS("lesterHandler", sLesterHandover, SIZE_OF(sLesterHandover), DEFAULT_STACK_SIZE)
					SET_SCRIPT_AS_NO_LONGER_NEEDED("lesterHandler")
					println("LESTER HANDED OVER TO HANDLER")
				ENDIF
			ENDIF
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				REPLAY_STOP_EVENT()
				PRINTLN("MIKE EXIT")
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
			ENDIF
			SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
				SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000)
			ENDIF
			IF NOT IS_CUTSCENE_ACTIVE()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				SETTIMERA(0)
				iStageProgress++
			ENDIF
		BREAK
		
		CASE 102
			IF TIMERA() < 1500
			AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<715.869629,-964.413025,31.395329>>,<<4.000000,6.000000,2.000000>>)
				SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
			ENDIF
			IF TIMERA() > 3000
				Mission_Passed()
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF DOES_BLIP_EXIST(blipOfficeFBI)
	AND NOT bGoUpstairs
	AND iStageProgress > 7
	AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		PRINT("S1_UPSTAIRS", DEFAULT_GOD_TEXT_TIME, 1)
		bGoUpstairs = TRUE
	ENDIF
	
	IF NOT IS_PED_INJURED(pedLester)
		IF NOT DOES_ENTITY_EXIST(objStick)
			IF iStageProgress < 100
				IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vEndPoint) < 200*200
					REQUEST_MODEL(PROP_CS_WALKING_STICK)
					IF HAS_MODEL_LOADED(PROP_CS_WALKING_STICK)
						objStick = CREATE_OBJECT(PROP_CS_WALKING_STICK, GET_ENTITY_COORDS(pedLester, FALSE))
						SET_ENTITY_VISIBLE(objStick, FALSE)
						ATTACH_ENTITY_TO_ENTITY(objStick, pedLester, GET_PED_BONE_INDEX(pedLester, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_ENTITY_VISIBLE(objStick)
				IF IS_PED_IN_ANY_VEHICLE(pedLester)
					SET_ENTITY_VISIBLE(objStick, FALSE)
				ENDIF
			ELSE
				IF NOT IS_PED_IN_ANY_VEHICLE(pedLester)
					SET_ENTITY_VISIBLE(objStick, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
//	IF iStageProgress >= 1
//		IF DOES_ENTITY_EXIST(objDoorGrab)
//			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_janitorFrontDoorCoords) > POW(100,2)
//				RESET_ENTITY_ALPHA(objDoorGrab)
//				SET_OBJECT_AS_NO_LONGER_NEEDED(objDoorGrab)
//			ENDIF
//		ELSE
//			IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), v_janitorFrontDoorCoords) < POW(90,2)
//				IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(v_janitorFrontDoorCoords, 2, V_ILEV_JANITOR_FRONTDOOR, TRUE)
//					objDoorGrab = GET_CLOSEST_OBJECT_OF_TYPE(v_janitorFrontDoorCoords, 2, V_ILEV_JANITOR_FRONTDOOR, TRUE)
//					FREEZE_ENTITY_POSITION(objDoorGrab, TRUE)
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	
	IF iStageProgress < 100
		IF NOT DOES_ENTITY_EXIST(pedFranklin)
		AND VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vEndPoint) < DEFAULT_CUTSCENE_LOAD_DIST
			CREATE_FRANKLIN_FOR_OUTRO()
			REQUEST_CUTSCENE("ah_1_ext_t6")
			IF IS_CUTSCENE_ACTIVE()
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					SET_CUTSCENE_PED_OUTFIT("Franklin", PLAYER_ONE, OUTFIT_P1_HOODIE_AND_JEANS_2)
				ENDIF
			ENDIF
		ELIF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE), vEndPoint) > DEFAULT_CUTSCENE_UNLOAD_DIST
			IF DOES_ENTITY_EXIST(pedFranklin)
				DELETE_PED(pedFranklin)
			ENDIF
			IF IS_CUTSCENE_ACTIVE()
				REMOVE_CUTSCENE()
			ENDIF
		ENDIF
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<709.9813, -963.5311, 30.5453>>, <<100, 100, 50>>)
			DOOR_SYSTEM_SET_OPEN_RATIO(	ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE), 0.0, FALSE, TRUE)
		ENDIF
	ENDIF
	
ENDPROC

PROC JANITOR_AND_CAR_FAIL_CHECK()

	IF DOES_ENTITY_EXIST(pedJanitor)
	
		IF IS_PED_INJURED(pedJanitor)
		AND mission_stage < STAGE_DRIVE_BACK_TO_SAFEHOUSE
			Mission_Failed("S1_FAILED_K")
		ELSE
			IF IS_EXPLOSION_IN_AREA(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(pedJanitor, FALSE) + <<3,3,3>>, GET_ENTITY_COORDS(pedJanitor, FALSE) - <<3,3,3>>)
			OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(pedJanitor, FALSE), 6)
			OR (IS_PED_FLEEING(pedJanitor) AND NOT bBlockFleeCheckForFail )
			OR (IS_PED_JACKING(PLAYER_PED_ID()) AND IS_PED_BEING_JACKED(pedJanitor))
				Mission_Failed("S1_JSPOOK")
			ENDIF
			IF (DOES_ENTITY_EXIST(GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())) AND vehJanitor = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()))
				PRINTLN("FAILED FOR TRYING TO ENTER CAR!")
				Mission_Failed("S1_JSPOOK")
			ENDIF
		ENDIF
		
		IF NOT b_JanitorHome
			IF DOES_ENTITY_EXIST(vehJanitor)
				IF NOT IS_VEHICLE_DRIVEABLE(vehJanitor)
					Mission_Failed("S1_FAILED_C")
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD

	PROC DRAW_UBER_ROUTE_DEBUG_PATH(VECTOR &path[], INT red = 0, INT green = 0, INT blue = 255)
		INT iTemp
		REPEAT COUNT_OF(path) iTemp
			DRAW_DEBUG_SPHERE(path[iTemp], 0.2, red, green, blue, 170)
			IF iTemp > 0
				DRAW_DEBUG_LINE(path[iTemp], path[iTemp-1], red, green, blue, 170)
			ENDIF
		ENDREPEAT
	ENDPROC
	
	PROC debugProcedures()
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)		
			MISSION_PASSED()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			Mission_Failed("S1_FAILED_K")
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) 
			jSkip()	
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P) 
			pSkip()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD3)
			KILL_CHASE_HINT_CAM(focuscam)
		ENDIF
		
		IF LAUNCH_MISSION_STAGE_MENU(skipMenuOptions, iStageHolder)
			bSkip = TRUE
			GO_TO_STAGE(INT_TO_ENUM(MISSION_STAGE_FLAG, iStageHolder))
		ENDIF
		
	ENDPROC
	
#ENDIF

PROC SET_PED_CANT_DANCE(PED_INDEX ped)
	IF NOT IS_PED_INJURED(ped)
	AND IS_PED_IN_ANY_VEHICLE(ped)
		SET_PED_CAN_PLAY_AMBIENT_ANIMS(ped, FALSE)
		SET_PED_CAN_PLAY_AMBIENT_BASE_ANIMS(ped, FALSE)
	ENDIF
ENDPROC

// ____________________________________ MISSION SCRIPT _____________________________________

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED()	
		Mission_Flow_Mission_Force_Cleanup()
		IF DOES_ENTITY_EXIST(pedLester)
			DELETE_PED(pedLester)
		ENDIF
		MISSION_CLEANUP()
	ENDIF
	
	//\____________________________________// MISSION LOOP \\_______________________________________/\\
	
	SET_MISSION_FLAG(TRUE)
	
	IF mission_stage <> STAGE_TEST
		PRE_INIT()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		BUILD_WIDGETS()
	#ENDIF
	
	WHILE TRUE
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_BR1")
		
		SWITCH mission_stage
		
			CASE STAGE_INTRO_CUT
				DO_INTRO()
			BREAK
			
			CASE STAGE_GO_TO_FIB
				GO_TO_FIB()
			BREAK
			
			CASE STAGE_WAIT_FOR_JANITOR
				WATCH_DECOYS()
			BREAK
			
			CASE STAGE_TAIL_THE_JANITOR
				TAIL_THE_JANITOR_USING_AI()
			BREAK
			
			CASE STAGE_FOLLOW_JANITOR_ON_FOOT
				stageFollowJanitorOnFoot()
			BREAK
			
			CASE STAGE_JANITOR_CUT
				DO_JANITOR_SCENE()
			BREAK
			
			CASE STAGE_DRIVE_BACK_TO_SAFEHOUSE
				DRIVE_BACK_TO_SAFEHOUSE()
			BREAK
			
			CASE STAGE_LOSE_COPS
				LOSE_THE_COPS()
			BREAK
			
			CASE STAGE_JANITOR_SPOOKED_ON_FOOT
				stageJanitorFleesOnFoot()
			BREAK
			
			CASE STAGE_TEST
				#IF IS_DEBUG_BUILD
				#ENDIF
			BREAK
			
			DEFAULT
				SCRIPT_ASSERT("Unrecognised Mission Stage!")
			BREAK
			
		ENDSWITCH
		
		IF GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2) = 1
			SET_PLAYER_CLOTH_PIN_FRAMES(PLAYER_ID(), 1)
		ENDIF
		
		SET_PED_CANT_DANCE(pedLester)
		
		IF HAS_PED_BEEN_ABANDONED(pedLester)
		AND NOT bSkip
		AND NOT IS_CUTSCENE_PLAYING()
			MISSION_FAILED(gtAbandoned.label)
		ENDIF
		
		IF bSkip
			CHECK_PEDS_FOR_DIALOGUE()
			bSkip = FALSE
		ENDIF
				
		IF mission_stage > STAGE_FOLLOW_JANITOR_ON_FOOT
		AND mission_stage != STAGE_JANITOR_SPOOKED_ON_FOOT
			IF DOES_ENTITY_EXIST(pedMarilyn)
				IF VDIST2(GET_ENTITY_COORDS(pedMarilyn, FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 1000
				AND IS_ENTITY_OCCLUDED(pedMarilyn)
					DELETE_PED(pedMarilyn)
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(carHotGirl)
				IF VDIST2(GET_ENTITY_COORDS(carHotGirl, FALSE), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 1000
				AND IS_ENTITY_OCCLUDED(carHotGirl)
					DELETE_VEHICLE(carHotGirl)
				ENDIF
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		
			debugProcedures()
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD4)
				IF NOT IS_PED_INJURED(pedJanitor)
					EXPLODE_PED_HEAD(pedJanitor)
				ENDIF
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
				SET_JANITORS_DOOR_LOCKED()
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
				SET_JANITORS_DOOR_LOCKED(FALSE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				DRAW_DEBUG_TEXT_2D(GET_NAME_OF_ZONE(GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)), <<0.1, 0.02, 0>>)
			ENDIF
			
			RUN_WIDGETS()
			
		#ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), AH1_CAR_DAMAGE)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), AH1_MAX_SPEED)
		ELSE
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, AH1_CAR_DAMAGE)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL, AH1_MAX_SPEED)
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedLester)
			IF IS_PED_INJURED(pedLester)
			OR IS_ENTITY_DEAD(pedLester)
				Mission_Failed("S1_LESFAIL")
			ELSE
				IF DOES_ENTITY_EXIST(objLestPhone)
					SET_PED_CAN_PLAY_GESTURE_ANIMS(pedLester, FALSE)
					PRINTLN("@@@@@@@@@@ SET_PED_CAN_PLAY_GESTURE_ANIMS(pedLester, FALSE) @@@@@@@@@@")
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedFranklin)
			IF IS_PED_INJURED(pedFranklin)
			OR IS_PED_INJURED(pedFranklin)
				Mission_Failed("CMN_FDIED")
			ENDIF
		ENDIF
		
		IF MISSION_STAGE < STAGE_FOLLOW_JANITOR_ON_FOOT
			JANITOR_AND_CAR_FAIL_CHECK()
		ENDIF
		
		MISSION_VEHICLE_FAIL_CHECKS()
		HANDLE_BUDDY_HEAD_TRACK_WHILE_ENTERING_VEHICLE()
		
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			IF mission_stage = STAGE_GO_TO_FIB
			OR mission_stage = STAGE_DRIVE_BACK_TO_SAFEHOUSE
				LOSE_THE_COPS()
			ELSE
				IF mission_stage = STAGE_TAIL_THE_JANITOR
				OR mission_stage = STAGE_FOLLOW_JANITOR_ON_FOOT
					IF NOT IS_PED_INJURED(pedJanitor)
						IF IS_VEHICLE_DRIVEABLE(vehJanitor)
							IF IS_PED_IN_VEHICLE(pedJanitor, vehJanitor)
								TASK_VEHICLE_MISSION_PED_TARGET(pedJanitor, vehJanitor, PLAYER_PED_ID(), MISSION_FLEE, 40, DRIVINGMODE_PLOUGHTHROUGH, 20, 20)
							ENDIF
						ENDIF
						IF NOT IS_PED_IN_ANY_VEHICLE(pedJanitor)
							TASK_SMART_FLEE_PED(pedJanitor, PLAYER_PED_ID(), 200, 20000)
						ENDIF
					ENDIF
					Mission_Failed("S1_JSPOOK")
				ENDIF
			ENDIF
		ENDIF
				
		WAIT(0)
		
	ENDWHILE
	
ENDSCRIPT
