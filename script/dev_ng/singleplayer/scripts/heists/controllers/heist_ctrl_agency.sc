//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 27/05/10			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│							  FBI Heist Control Script							│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

USING "rage_builtins.sch"
USING "globals.sch"

USING "heist_board_data_agency.sch"
USING "heist_planning_board.sch"
USING "heist_ctrl_generic.sch"
USING "flow_public_core_override.sch"
USING "cutscene_public.sch"
USING "selector_public.sch"
USING "clearmissionarea.sch"
USING "script_misc.sch"

//═══════════════════════════════╡ Constants ╞═════════════════════════════════

CONST_FLOAT 	BOARD_LOAD_DIST				40.0
CONST_FLOAT		BOARD_UNLOAD_DIST			50.0

//State flags.
CONST_INT		BIT_CREATE_MIKE_VEH_APPROACH		0
CONST_INT		BIT_BOARD_VIEW_ACTIVATED			1
CONST_INT		BIT_CANDIDATE_ID_SECURED			2
CONST_INT		BIT_BLUEPRINTS_PUT_UP				3
CONST_INT		BIT_LESTER_FRANKLIN_HIDDEN			4
CONST_INT		BIT_MICHAEL_HIDDEN					5
CONST_INT		BIT_LOADING_INTRO_CUTSCENE			6
CONST_INT		BIT_LOADING_EXIT_CUTSCENE			7
CONST_INT		BIT_REPOSITIONED_JANITOR_ITEMS_A	8
CONST_INT		BIT_REPOSITIONED_JANITOR_ITEMS_B	9
CONST_INT		BIT_GRAB_LESTER_ENTITY				10
CONST_INT		BIT_GRAB_STICK_ENTITY				11
CONST_INT		BIT_GRAB_MICHAEL_ENTITY				12
CONST_INT		BIT_FADE_IN_FROM_SHITSKIP			13
CONST_INT		BIT_CUTSCENE_SKIPPED				14
CONST_INT		BIT_SWITCHED_TO_MICHAEL_FOR_REPEAT	15

//═══════════════════════════════╡ Variables ╞═════════════════════════════════

INT		m_iState					= 0
INT 	m_iMissionCandidateID	 	= NO_CANDIDATE_ID
INT		m_iMichaelBlendoutStart 	= -1
INT 	m_iCutsceneStartTime		= -1

PED_INDEX 				m_pedFranklin
PED_INDEX 				m_pedLester
OBJECT_INDEX			m_objLesterStick
OBJECT_INDEX 			m_objLaptop
SELECTOR_PED_STRUCT 	sSelector
SCENARIO_BLOCKING_INDEX m_scenarioBlock
INT						m_navmeshBlock = -1

//═══════════════════════════════╡ Structures ╞════════════════════════════════

PlanningBoard m_sAgencyBoard

//══════════════════════════════╡ Procedures ╞═════════════════════════════════

PROC Terminate_Heist_Controller()
	
	//Ensure the planning board has been cleaned up before the control script is terminated.
	WHILE NOT Cleanup_Planning_Board(m_sAgencyBoard)
		WAIT(0)
	ENDWHILE
	
	//Cleanup flow flag states.
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_PRIME_BOARD_TRANSITION, FALSE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_DO_BOARD_EXIT_CUTSCENE, FALSE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_LOAD_BOARD_EXIT_CUTSCENE, FALSE)
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, HEIST_AGENCY, FALSE)
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_TOGGLE_BOARD, HEIST_AGENCY, FALSE)
	
	//Remove area blocks and restore wanted levels.
	REMOVE_SCENARIO_BLOCKING_AREA(m_scenarioBlock)
	IF m_navmeshBlock != -1
		IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(m_navmeshBlock)
			REMOVE_NAVMESH_BLOCKING_OBJECT(m_navmeshBlock)
		ENDIF
	ENDIF
	CLEAR_PED_NON_CREATION_AREA()
	SET_PED_PATHS_IN_AREA(m_sAgencyBoard.sData.vAreaBlockCenter-m_sAgencyBoard.sData.vAreaBlockDimensions, m_sAgencyBoard.sData.vAreaBlockCenter+m_sAgencyBoard.sData.vAreaBlockDimensions, TRUE)
	SET_MAX_WANTED_LEVEL(5)
	
	//Clean up any cutscenes we have loaded.
	REMOVE_CUTSCENE()
	
	//Release the mission candidate ID if we have one.
	IF m_iMissionCandidateID != NO_CANDIDATE_ID
		Mission_Over(m_iMissionCandidateID)
	ENDIF
	
	CPRINTLN(DEBUG_HEIST, "Agency heist controller terminated.")
	TERMINATE_THIS_THREAD()
	
ENDPROC


PROC Set_Laptop_Visibile(BOOL paramVisible)
	IF NOT paramVisible
		CPRINTLN(DEBUG_HEIST, "Setting sweatshop laptop hidden.")
		IF DOES_ENTITY_EXIST(m_objLaptop)
			DELETE_OBJECT(m_objLaptop)
		ENDIF
		CREATE_MODEL_HIDE(<<707.3041, -967.6456, 30.3760>>, 1.00, PROP_LAPTOP_01A, FALSE)
	ELSE
		CPRINTLN(DEBUG_HEIST, "Setting sweatshop laptop visible.")
		REMOVE_MODEL_HIDE(<<707.3041, -967.6456, 30.3760>>, 1.00, PROP_LAPTOP_01A, FALSE)
		IF HAS_MODEL_LOADED(PROP_LAPTOP_01A)
			m_objLaptop = CREATE_OBJECT(PROP_LAPTOP_01A, <<707.3041, -967.6456, 30.3760>>)
			SET_ENTITY_HEADING(m_objLaptop, 183.14)
			FREEZE_ENTITY_POSITION(m_objLaptop, TRUE)
		ENDIF
	ENDIF
ENDPROC


PROC Switch_On_Planning_Board_View()
	//Seamless transition into board view. Toggle board on and disable board exit.
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, HEIST_AGENCY, TRUE)
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_TOGGLE_BOARD, HEIST_AGENCY, TRUE)
	Manage_Board_View_Auto_Toggling(m_sAgencyBoard)
	Manage_Board_Viewing(m_sAgencyBoard)
ENDPROC


PROC Manage_Agency_Heist_Board_Intro_Cutscene()

	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_PRIME_BOARD_TRANSITION)	
		IF NOT IS_REPEAT_PLAY_ACTIVE()
		
			//Normal playthrough, play the intro cutscene leading in to the planning board interactions.
			IF NOT IS_BIT_SET(m_iState, BIT_LOADING_INTRO_CUTSCENE)
				CPRINTLN(DEBUG_HEIST, "Requesting agency heist board intro cutscene.")
				REQUEST_CUTSCENE("AH_2_EXT_ALT")
				REQUEST_MODEL(PROP_LAPTOP_01A)

				//Reposition the Janitor items on the board.
				m_sAgencyBoard.sData.sStaticItemPos[2].x = 0
				m_sAgencyBoard.sData.sStaticItemPos[2].y = 0
				m_sAgencyBoard.sData.sStaticItemPos[3].x = 0
				m_sAgencyBoard.sData.sStaticItemPos[3].y = 0
				m_sAgencyBoard.sData.sStaticItemPos[7].x = 0
				m_sAgencyBoard.sData.sStaticItemPos[7].y = 0
				m_sAgencyBoard.sData.sStaticItemPos[8].x = 0
				m_sAgencyBoard.sData.sStaticItemPos[8].y = 0

				SET_BIT(m_iState, BIT_LOADING_INTRO_CUTSCENE)
				
			ELIF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_PROP_VARIATION("LESTER", ANCHOR_EYES, 0, 0)
			ENDIF
			
			IF GET_MISSION_COMPLETE_STATE(SP_HEIST_AGENCY_2)
				IF NOT IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY)
					IF Request_Mission_Launch(m_iMissionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
						CPRINTLN(DEBUG_HEIST, "Agency Heist control thread secured the mission flag.")
						
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						CLEAR_AUTOSAVE_REQUESTS()
						
						//Stop the player getting a wanted level for the duration of this planning board sequence.
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
							SET_MAX_WANTED_LEVEL(0)
						ENDIF
						
						//Setup area blocks around the board.
						m_scenarioBlock = ADD_SCENARIO_BLOCKING_AREA(m_sAgencyBoard.sData.vAreaBlockCenter-m_sAgencyBoard.sData.vAreaBlockDimensions, m_sAgencyBoard.sData.vAreaBlockCenter+m_sAgencyBoard.sData.vAreaBlockDimensions)
						m_navmeshBlock = ADD_NAVMESH_BLOCKING_OBJECT(m_sAgencyBoard.sData.vAreaBlockCenter, m_sAgencyBoard.sData.vAreaBlockDimensions*1.5, 0.0)
						SET_PED_NON_CREATION_AREA(m_sAgencyBoard.sData.vAreaBlockCenter-m_sAgencyBoard.sData.vAreaBlockDimensions, m_sAgencyBoard.sData.vAreaBlockCenter+m_sAgencyBoard.sData.vAreaBlockDimensions)
						SET_PED_PATHS_IN_AREA(m_sAgencyBoard.sData.vAreaBlockCenter-m_sAgencyBoard.sData.vAreaBlockDimensions, m_sAgencyBoard.sData.vAreaBlockCenter+m_sAgencyBoard.sData.vAreaBlockDimensions, FALSE)
						
						SET_BIT(m_iState, BIT_CANDIDATE_ID_SECURED)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(m_iState, BIT_CANDIDATE_ID_SECURED)
				IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE("AH_2_EXT_ALT")
				AND HAS_MODEL_LOADED(PROP_LAPTOP_01A)
				
					IF NOT IS_CUTSCENE_PLAYING()
						CPRINTLN(DEBUG_HEIST, "Starting Agency Heist board intro cutscene.")

						//Register Lester.
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
						AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
							m_pedLester = g_sTriggerSceneAssets.ped[0]
							SET_ENTITY_AS_MISSION_ENTITY(m_pedLester, FALSE, TRUE)
							REGISTER_ENTITY_FOR_CUTSCENE(m_pedLester, 	"LESTER", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, IG_LESTERCREST)
							CPRINTLN(DEBUG_HEIST, "Grabbed Lester from trigger scene.")
						ENDIF
						
						//Register Lester's stick.
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
						AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.object[0])
							m_objLesterStick = g_sTriggerSceneAssets.object[0]
							SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.object[0], FALSE, TRUE)
							REGISTER_ENTITY_FOR_CUTSCENE(m_objLesterStick, "WALKINGSTICK_LESTER", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, PROP_CS_WALKING_STICK)
							CPRINTLN(DEBUG_HEIST, "Grabbed Lester's stick from trigger scene.")
						ENDIF
						
						//Register Michael.
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
						AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[1])
							sSelector.pedID[SELECTOR_PED_MICHAEL] = g_sTriggerSceneAssets.ped[1]
							SET_ENTITY_AS_MISSION_ENTITY(sSelector.pedID[SELECTOR_PED_MICHAEL], FALSE, TRUE)
							REGISTER_ENTITY_FOR_CUTSCENE(sSelector.pedID[SELECTOR_PED_MICHAEL], 	"MICHAEL", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
							CPRINTLN(DEBUG_HEIST, "Grabbed Michael from trigger scene.")
						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(sSelector.pedID[SELECTOR_PED_MICHAEL], 	"MICHAEL", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PLAYER_ZERO)
							SET_BIT(m_iState, BIT_GRAB_MICHAEL_ENTITY)
							CPRINTLN(DEBUG_HEIST, "Registering Michael to be created by the cutscene.")
						ENDIF
						
						//Register Franklin.
						IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
						AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
							m_pedFranklin = PLAYER_PED_ID()
							REGISTER_ENTITY_FOR_CUTSCENE(m_pedFranklin, "FRANKLIN", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, PLAYER_ONE)
						ENDIF
						
						ADD_PED_FOR_DIALOGUE(m_sAgencyBoard.sPedsForConversation, 0, NULL, "MICHAEL", 	TRUE)
						ADD_PED_FOR_DIALOGUE(m_sAgencyBoard.sPedsForConversation, 1, NULL, "FRANKLIN", TRUE)
						ADD_PED_FOR_DIALOGUE(m_sAgencyBoard.sPedsForConversation, 3, NULL, "LESTER", 	TRUE)
					
						DISABLE_CELLPHONE(TRUE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						Set_Laptop_Visibile(FALSE)
						
						DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME)
						DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE_STORE), 0)
						
						START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						m_iCutsceneStartTime = GET_GAME_TIMER()
					ELSE
						CPRINTLN(DEBUG_HEIST, "Cutscene time: ", GET_GAME_TIMER() - m_iCutsceneStartTime)
						
						// #1966073 Prevent replay recording while a planning board cutscene is active.
						REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
						
						//Clean up whatever car Franklin arrived in.
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
						AND NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[0])
							PRINTLN("\n\n\nREMOVING FRANKLIN'S CAR\n\n\n")
							SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], FALSE, TRUE)							
							SEND_VEHICLE_DATA_TO_IMPOUND_USING_VEHICLE(g_sTriggerSceneAssets.veh[0], FALSE, CHAR_FRANKLIN) // Impound this to Franklin's lot
							DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])							
						ENDIF
						
						IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
							PRINTLN("\n\n\nREMOVING LAST CAR\n\n\n")
							g_sTriggerSceneAssets.veh[0] = GET_PLAYERS_LAST_VEHICLE()
							SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], FALSE, TRUE)
//							if IS_PED_in_VEHICLE(player_ped_id(),g_sTriggerSceneAssets.veh[0])
//								SET_ENTITY_COORDS(player_ped_id(),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(g_sTriggerSceneAssets.veh[0],<<0,0,1>>))
//							endif
							SEND_VEHICLE_DATA_TO_IMPOUND_USING_VEHICLE(g_sTriggerSceneAssets.veh[0], FALSE, GET_CURRENT_PLAYER_PED_ENUM())
							DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
						ENDIF
						
						IF GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD) = 7
							CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
						ENDIF
						
						IF NOT IS_BIT_SET(m_iState, BIT_FADE_IN_FROM_SHITSKIP)
							IF NOT IS_SCREEN_FADED_IN()
							AND NOT IS_SCREEN_FADING_IN()
								DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
							ENDIF
							SET_BIT(m_iState, BIT_FADE_IN_FROM_SHITSKIP)
						ENDIF
						
						IF WAS_CUTSCENE_SKIPPED()
							SET_BIT(m_iState, BIT_CUTSCENE_SKIPPED)
						ENDIF
						
						IF IS_BIT_SET(m_iState, BIT_CUTSCENE_SKIPPED)
							IF IS_SCREEN_FADED_OUT()
								IF NOT IS_BIT_SET(m_iState, BIT_BOARD_VIEW_ACTIVATED)
									CPRINTLN(DEBUG_HEIST, "Activating Agency Heist planning board due to cutscene skip.")
									Switch_On_Planning_Board_View()
									SET_BIT(m_iState, BIT_BOARD_VIEW_ACTIVATED)
								ENDIF
								IF NOT IS_BIT_SET(m_iState, BIT_BLUEPRINTS_PUT_UP)
									CPRINTLN(DEBUG_HEIST, "Blueprints toggled on as cutscene is skipped.")
									SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_AGENCY, PBDG_3, TRUE)
									SET_BIT(m_iState, BIT_BLUEPRINTS_PUT_UP)
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(m_iState, BIT_GRAB_MICHAEL_ENTITY)
							IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MICHAEL", PLAYER_ZERO))
								CPRINTLN(DEBUG_HEIST, "Grabbed Michael entity created by cutscene.")
								sSelector.pedID[SELECTOR_PED_MICHAEL] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MICHAEL", PLAYER_ZERO))
								CLEAR_BIT(m_iState, BIT_GRAB_MICHAEL_ENTITY)
							ENDIF
						ENDIF

						IF NOT IS_BIT_SET(m_iState, BIT_BLUEPRINTS_PUT_UP)
							IF (GET_GAME_TIMER() - m_iCutsceneStartTime) > 15848
								CPRINTLN(DEBUG_HEIST, "Blueprints toggled on mid cutscene.")
								SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_AGENCY, PBDG_3, TRUE)
								SET_BIT(m_iState, BIT_BLUEPRINTS_PUT_UP)
							ENDIF
						ENDIF
						
						IF NOT IS_BIT_SET(m_iState, BIT_BOARD_VIEW_ACTIVATED)
							IF (GET_GAME_TIMER() - m_iCutsceneStartTime) > 5000//65000
								CPRINTLN(DEBUG_HEIST, "Activating Agency Heist planning board during cutscene.")
								Switch_On_Planning_Board_View()
								SET_BIT(m_iState, BIT_BOARD_VIEW_ACTIVATED)
							ENDIF
						ENDIF
						
						IF NOT IS_BIT_SET(m_iState, BIT_LESTER_FRANKLIN_HIDDEN)
							IF (GET_GAME_TIMER() - m_iCutsceneStartTime) > 71550
								IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("FRANKLIN"))
									IF NOT IS_ENTITY_DEAD(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("FRANKLIN"))
								   		SET_ENTITY_VISIBLE(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("FRANKLIN"), FALSE)
										CPRINTLN(DEBUG_HEIST, "Franklin hidden during cutscene.")
									ENDIF
								ENDIF
								IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("LESTER"))
									IF NOT IS_ENTITY_DEAD(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("LESTER"))
									   	SET_ENTITY_VISIBLE(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("LESTER"), FALSE)
										CPRINTLN(DEBUG_HEIST, "Lester hidden during cutscene.")
									ENDIF
								ENDIF
								IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("WALKINGSTICK_LESTER"))
									IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("WALKINGSTICK_LESTER"))
										SET_ENTITY_VISIBLE(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("WALKINGSTICK_LESTER"), FALSE)
										CPRINTLN(DEBUG_HEIST, "Stick hidden during cutscene.")
									ENDIF
								ENDIF
								SET_BIT(m_iState, BIT_LESTER_FRANKLIN_HIDDEN)
							ENDIF
						ENDIF
						IF NOT IS_BIT_SET(m_iState, BIT_MICHAEL_HIDDEN)
							IF (GET_GAME_TIMER() - m_iCutsceneStartTime) > 73900
								IF DOES_ENTITY_EXIST(sSelector.pedID[SELECTOR_PED_MICHAEL])
									SET_ENTITY_VISIBLE(sSelector.pedID[SELECTOR_PED_MICHAEL], FALSE)
									CPRINTLN(DEBUG_HEIST, "Michael hidden during cutscene.")
								ENDIF
								SET_BIT(m_iState, BIT_MICHAEL_HIDDEN)
							ENDIF
						ENDIF
						
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MICHAEL", PLAYER_ZERO)
							CPRINTLN(DEBUG_HEIST, "Agency Heist board intro cutscene ready to set exit state.")
							IF NOT IS_ENTITY_DEAD(sSelector.pedID[SELECTOR_PED_MICHAEL])
								SET_PED_CONFIG_FLAG(sSelector.pedID[SELECTOR_PED_MICHAEL], PCF_DisableExplosionReactions, TRUE)
								SET_PED_CONFIG_FLAG(sSelector.pedID[SELECTOR_PED_MICHAEL], PCF_RunFromFiresAndExplosions, FALSE)
								SET_PED_CONFIG_FLAG(sSelector.pedID[SELECTOR_PED_MICHAEL], PCF_ListensToSoundEvents, FALSE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelector.pedID[SELECTOR_PED_MICHAEL], TRUE)
							ENDIF
							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
								CPRINTLN(DEBUG_HEIST, "Switching player to Michael.")
								MAKE_SELECTOR_PED_SELECTION(sSelector, SELECTOR_PED_MICHAEL)
								TAKE_CONTROL_OF_SELECTOR_PED(sSelector, FALSE, FALSE)
								SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ZERO)
								ADD_PED_FOR_DIALOGUE(m_sAgencyBoard.sPedsForConversation, 0, PLAYER_PED_ID(), "MICHAEL", TRUE)
							ENDIF
							IF NOT IS_BIT_SET(m_iState, BIT_BOARD_VIEW_ACTIVATED)
								CPRINTLN(DEBUG_HEIST, "Activating Agency Heist planning board view behind cutscene.")
								Switch_On_Planning_Board_View()
								SET_BIT(m_iState, BIT_BOARD_VIEW_ACTIVATED)
							ENDIF
							SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_PRIME_BOARD_TRANSITION, FALSE)
						ENDIF

						IF CAN_SET_EXIT_STATE_FOR_CAMERA()
						
							Set_Laptop_Visibile(TRUE)
							IF NOT IS_BIT_SET(m_iState, BIT_LESTER_FRANKLIN_HIDDEN)
								IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("FRANKLIN"))
									IF NOT IS_ENTITY_DEAD(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("FRANKLIN"))
								   		SET_ENTITY_VISIBLE(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("FRANKLIN"), FALSE)
										CPRINTLN(DEBUG_HEIST, "Franklin hidden at cutscene end.")
									ENDIF
								ENDIF
								IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("LESTER"))
									IF NOT IS_ENTITY_DEAD(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("LESTER"))
									   	SET_ENTITY_VISIBLE(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("LESTER"), FALSE)
										CPRINTLN(DEBUG_HEIST, "Lester hidden at cutscene end.")
									ENDIF
								ENDIF
								IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("WALKINGSTICK_LESTER"))
									IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("WALKINGSTICK_LESTER"))
										SET_ENTITY_VISIBLE(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("WALKINGSTICK_LESTER"), FALSE)
										CPRINTLN(DEBUG_HEIST, "Stick hidden at cutscene end.")
									ENDIF
								ENDIF
								SET_BIT(m_iState, BIT_LESTER_FRANKLIN_HIDDEN)
							ENDIF
							IF NOT IS_BIT_SET(m_iState, BIT_MICHAEL_HIDDEN)
								IF DOES_ENTITY_EXIST(sSelector.pedID[SELECTOR_PED_MICHAEL])
									SET_ENTITY_VISIBLE(sSelector.pedID[SELECTOR_PED_MICHAEL], FALSE)
									CPRINTLN(DEBUG_HEIST, "Michael hidden at cutscene end.")
								ENDIF
								SET_BIT(m_iState, BIT_MICHAEL_HIDDEN)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
				IF Request_Mission_Launch(m_iMissionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
					CPRINTLN(DEBUG_HEIST, "Agency Heist control thread secured the mission flag.")
					SET_BIT(m_iState, BIT_CANDIDATE_ID_SECURED)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(m_iState, BIT_CANDIDATE_ID_SECURED)
			
				//Repeat play, jump straight into the board.
				ADD_PED_FOR_DIALOGUE(m_sAgencyBoard.sPedsForConversation, 0, NULL, "MICHAEL", 	TRUE)
				ADD_PED_FOR_DIALOGUE(m_sAgencyBoard.sPedsForConversation, 1, NULL, "FRANKLIN", TRUE)
				ADD_PED_FOR_DIALOGUE(m_sAgencyBoard.sPedsForConversation, 3, NULL, "LESTER", 	TRUE)
					
				CPRINTLN(DEBUG_HEIST, "Activating Agency Heist planning board for repeat play.")
				Switch_On_Planning_Board_View()
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_PRIME_BOARD_TRANSITION, FALSE)
			ENDIF
		ENDIF
	ELSE
		IF HAS_THIS_CUTSCENE_LOADED("AH_2_EXT_ALT")
			CPRINTLN(DEBUG_HEIST, "Cleaning up Agency Heist planning board intro cutscene.")
			REMOVE_CUTSCENE()
		ENDIF
		IF IS_BIT_SET(m_iState, BIT_LOADING_INTRO_CUTSCENE)
			CLEAR_BIT(m_iState, BIT_LOADING_INTRO_CUTSCENE)
		ENDIF
	ENDIF

ENDPROC


PROC Manage_Agency_Heist_Board_Exit_Cutscene()

	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_LOAD_BOARD_EXIT_CUTSCENE)
		IF NOT IS_BIT_SET(m_iState, BIT_LOADING_EXIT_CUTSCENE)
			REQUEST_CUTSCENE("AH_2_EXT_P4")
			REQUEST_SCRIPT("lesterHandler")
			REQUEST_MODEL(PROP_LAPTOP_01A)
			SET_BIT(m_iState, BIT_LOADING_EXIT_CUTSCENE)
		ELIF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_PROP_VARIATION("LESTER", ANCHOR_EYES, 0, 0)
		ENDIF
	ENDIF
	
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_DO_BOARD_EXIT_CUTSCENE)
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_LOAD_BOARD_EXIT_CUTSCENE)
			IF HAS_THIS_CUTSCENE_LOADED("AH_2_EXT_P4")
			AND HAS_SCRIPT_LOADED("lesterHandler")
			AND HAS_MODEL_LOADED(PROP_LAPTOP_01A)
				
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					IF NOT IS_REPEAT_PLAY_ACTIVE() OR GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MICHAEL", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
					ELSE
						CPRINTLN(DEBUG_HEIST, "Registed Michael as a selector ped so we can switch to him for repeat play.")
						REGISTER_ENTITY_FOR_CUTSCENE(sSelector.pedID[SELECTOR_PED_MICHAEL], "MICHAEL", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PLAYER_ZERO)
						SET_BIT(m_iState, BIT_GRAB_MICHAEL_ENTITY)
					ENDIF
				ENDIF
				
				IF IS_REPEAT_PLAY_ACTIVE()
					// url:bugstar:2112221
					IF DOES_ENTITY_EXIST(m_pedFranklin)
						REGISTER_ENTITY_FOR_CUTSCENE(m_pedFranklin, "FRANKLIN", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, PLAYER_ONE)
					ENDIF
				ENDIF
					
				REGISTER_ENTITY_FOR_CUTSCENE(m_pedLester, "LESTER", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_LESTERCREST)

				SET_BIT(m_iState, BIT_GRAB_LESTER_ENTITY)
				Set_Laptop_Visibile(FALSE)

				PAUSE_FLOW_HELP_QUEUE(TRUE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				DISABLE_CELLPHONE(TRUE)
				
				//If we've just picked Rickie make sure to deactivate his nag phonecall.
				INT iCrewIndex
				REPEAT GET_HEIST_CREW_SIZE(HEIST_AGENCY) iCrewIndex
					IF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_AGENCY, iCrewIndex) = CM_HACKER_B_RICKIE_UNLOCK
						CANCEL_COMMUNICATION(CALL_HACKER_3)
					ENDIF
				ENDREPEAT
				
				START_CUTSCENE(CUTSCENE_PLAYER_FP_FLASH_MICHAEL)
				
				IF IS_REPEAT_PLAY_ACTIVE()
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
				ENDIF
				
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_LOAD_BOARD_EXIT_CUTSCENE, FALSE)
				m_iCutsceneStartTime = GET_GAME_TIMER()
			ENDIF
		ELSE
			IF IS_CUTSCENE_PLAYING()
			
				// #1966073 Prevent replay recording while a planning board cutscene is active.
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
				
				//Unhide entities as the exit cutscene starts.
				IF IS_BIT_SET(m_iState, BIT_MICHAEL_HIDDEN)
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MICHAEL", PLAYER_ZERO))
						CPRINTLN(DEBUG_HEIST, "Setting Michael visible.")
						SET_ENTITY_VISIBLE(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MICHAEL", PLAYER_ZERO), TRUE)
					ENDIF
					CLEAR_BIT(m_iState, BIT_MICHAEL_HIDDEN)
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					IF IS_BIT_SET(m_iState, BIT_GRAB_MICHAEL_ENTITY)
						IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MICHAEL", PLAYER_ZERO))
							CPRINTLN(DEBUG_HEIST, "Grabbed Michael's entity from the cutscene.")
							sSelector.pedID[SELECTOR_PED_MICHAEL] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MICHAEL", PLAYER_ZERO))
							CLEAR_BIT(m_iState, BIT_GRAB_MICHAEL_ENTITY)
						ENDIF
					ENDIF
				ENDIF

				IF IS_BIT_SET(m_iState, BIT_GRAB_LESTER_ENTITY)
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("LESTER", IG_LESTERCREST))
						CPRINTLN(DEBUG_HEIST, "Grabbed Lester entity created by cutscene.")
						m_pedLester = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("LESTER", IG_LESTERCREST))
						CLEAR_BIT(m_iState, BIT_GRAB_LESTER_ENTITY)
					ENDIF
				ENDIF
			
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MICHAEL")
				
					// This will only get hit when doing a repeat play.
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
						// Grab reference to Franklin before the switch
						m_pedFranklin = PLAYER_PED_ID()
						CPRINTLN(DEBUG_HEIST, "Switching player to Michael.")
						MAKE_SELECTOR_PED_SELECTION(sSelector, SELECTOR_PED_MICHAEL)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelector, TRUE, FALSE)
						SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_ZERO)
					ENDIF
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
						SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
						
						//If in first person have ped walk forward b*2022412
						if GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
							SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000)
						ENDIF
						
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableExplosionReactions, FALSE)
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_RunFromFiresAndExplosions, TRUE)
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ListensToSoundEvents, TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PLAYER_PED_ID(), FALSE)
						m_iMichaelBlendoutStart = GET_GAME_TIMER()
						CPRINTLN(DEBUG_HEIST, "Michael's exit state blend out started.")
					ENDIF
				ENDIF
				
				SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("LESTER", IG_LESTERCREST)
					IF NOT IS_ENTITY_DEAD(m_pedLester)
						StructLesterHandover sLesterHandover
						sLesterHandover.pedHandover = m_pedLester
						START_NEW_SCRIPT_WITH_ARGS("lesterHandler", sLesterHandover, SIZE_OF(sLesterHandover), DEFAULT_STACK_SIZE)
						SET_SCRIPT_AS_NO_LONGER_NEEDED("lesterHandler")
						CPRINTLN(DEBUG_HEIST, "Lester handed over to his handler script.")
					ENDIF
					CPRINTLN(DEBUG_HEIST, "Lester's exit state set.")
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					Set_Laptop_Visibile(TRUE)
				ENDIF
			ELSE
				CPRINTLN(DEBUG_HEIST, "Exit cutscene ending cutscene.")
				IF IS_REPEAT_PLAY_ACTIVE() // fade out for repeat play
					IF DOES_ENTITY_EXIST(m_pedFranklin)
						DELETE_PED(m_pedFranklin)
					ENDIF
					IF DOES_ENTITY_EXIST(m_pedLester)
						DELETE_PED(m_pedLester)
					ENDIF
					DO_SCREEN_FADE_OUT(0)
				ELSE
					g_bMissionStatSystemBlocker = FALSE
					g_sAutosaveData.bFlushAutosaves = FALSE
					MAKE_AUTOSAVE_REQUEST()
				ENDIF
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				DISABLE_CELLPHONE(FALSE)
				ADD_GLOBAL_COMMUNICATION_DELAY(20000)
				ResetLastKnownPedInfo(g_savedGlobals.sPlayerData.sInfo, SP_HEIST_AGENCY_2)
				
				UpdateLastKnownPedInfoPostMission(g_savedGlobals.sPlayerData.sInfo, SP_HEIST_AGENCY_2)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LAPTOP_01A)
				Mission_Over(m_iMissionCandidateID)
				CLEAR_BIT(m_iState, BIT_CANDIDATE_ID_SECURED)
				SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVEBLENDRATIO_WALK)
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000)
				ENDIF
				//Remove area blocks and restore wanted levels.
				REMOVE_SCENARIO_BLOCKING_AREA(m_scenarioBlock)
				IF m_navmeshBlock != -1
					IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(m_navmeshBlock)
						REMOVE_NAVMESH_BLOCKING_OBJECT(m_navmeshBlock)
					ENDIF
				ENDIF
				CLEAR_PED_NON_CREATION_AREA()
				SET_PED_PATHS_IN_AREA(m_sAgencyBoard.sData.vAreaBlockCenter-m_sAgencyBoard.sData.vAreaBlockDimensions, m_sAgencyBoard.sData.vAreaBlockCenter+m_sAgencyBoard.sData.vAreaBlockDimensions, TRUE)
				SET_MAX_WANTED_LEVEL(5)
				
				#IF FEATURE_SP_DLC_DIRECTOR_MODE
					UNLOCK_DIRECTOR_HEIST_CHARS_FROM_HEIST(HEIST_AGENCY)
				#ENDIF
			
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_DO_BOARD_EXIT_CUTSCENE, FALSE)
			ENDIF
		ENDIF
	ELIF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_LOAD_BOARD_EXIT_CUTSCENE)
		//Clean up the cutscene if neither flow flag is set.
		IF HAS_THIS_CUTSCENE_LOADED("AH_2_EXT_P4")
			REMOVE_CUTSCENE()
		ENDIF
	ENDIF
	
	IF m_iMichaelBlendoutStart != -1
		IF (GET_GAME_TIMER() - m_iMichaelBlendoutStart) > 2000
			SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
			m_iMichaelBlendoutStart = -1
			CPRINTLN(DEBUG_HEIST, "Michael's exit state blend out finished.")
		ENDIF
	ENDIF
	
ENDPROC


PROC Maintain_Agency_3_Trigger_Building_State()
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	
	IF IS_PLAYER_PED_PLAYABLE(ePlayer)
		IF NOT IS_CURRENTLY_ON_MISSION_TO_TYPE(MISSION_TYPE_STORY)
			IF NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_AGENCY_3A) 
			AND NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_AGENCY_3B)
				IF IS_MISSION_AVAILABLE(SP_HEIST_AGENCY_3A)
				OR IS_MISSION_AVAILABLE(SP_HEIST_AGENCY_3B)
					IF g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR] != BUILDINGSTATE_DESTROYED
						CPRINTLN(DEBUG_HEIST, "Modding Agency Heist sweatshop building state: BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR")
						g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR] = BUILDINGSTATE_DESTROYED
						g_bBuildingStateUpdateDelay[BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR] = FALSE
						g_bUpdateBuildingState[BUILDINGNAME_IPL_SWEATSHOP_NO_INTERIOR] = TRUE
					ENDIF
					IF g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR] != BUILDINGSTATE_CLEANUP
						CPRINTLN(DEBUG_HEIST, "Modding Agency Heist sweatshop building state: BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR")
						g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR] = BUILDINGSTATE_CLEANUP
						g_bBuildingStateUpdateDelay[BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR] = FALSE
						g_bUpdateBuildingState[BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR] = TRUE
					ENDIF
					IF g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR] != BUILDINGSTATE_DESTROYED
						CPRINTLN(DEBUG_HEIST, "Modding Agency Heist sweatshop building state: BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR")
						g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR] = BUILDINGSTATE_DESTROYED
						g_bBuildingStateUpdateDelay[BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR] = FALSE
						g_bUpdateBuildingState[BUILDINGNAME_IPL_SWEATSHOP_LOD_DOOR] = TRUE
					ENDIF
					IF g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_SWEATSHOP_BURNT] != BUILDINGSTATE_NORMAL
						CPRINTLN(DEBUG_HEIST, "Modding Agency Heist sweatshop building state: BUILDINGNAME_IPL_SWEATSHOP_BURNT")
						g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_SWEATSHOP_BURNT] = BUILDINGSTATE_NORMAL
						g_bBuildingStateUpdateDelay[BUILDINGNAME_IPL_SWEATSHOP_BURNT] = FALSE
						g_bUpdateBuildingState[BUILDINGNAME_IPL_SWEATSHOP_BURNT] = TRUE
					ENDIF
					IF g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS] != BUILDINGSTATE_NORMAL
						CPRINTLN(DEBUG_HEIST, "Modding Agency Heist sweatshop building state: BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS")
						g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS] = BUILDINGSTATE_NORMAL
						g_bBuildingStateUpdateDelay[BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS] = FALSE
						g_bUpdateBuildingState[BUILDINGNAME_IPL_SWEATSHOP_WINDOW_LIGHTS] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


//══════════════════════════════╡ Control Loop ╞═══════════════════════════════

SCRIPT
	CPRINTLN(DEBUG_HEIST, "Agency heist controller started.")
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU | FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		Terminate_Heist_Controller()
	ENDIF
	
	//Register script to be saved as running.
	Register_Script_To_Relaunch_List(LAUNCH_BIT_HEIST_CTRL_AGENCY)
	
	//Setup the planning board for this heist.
	Setup_Agency_Heist_Board_Data(m_sAgencyBoard.sData, IMAX(g_sHeistChoiceData[HEIST_CHOICE_AGENCY_FIRETRUCK].iCrewSize, g_sHeistChoiceData[HEIST_CHOICE_AGENCY_HELICOPTER].iCrewSize))
	Setup_Planning_Board(m_sAgencyBoard, HEIST_AGENCY)

	WHILE NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINISHED_AGENCY)
		
		IF NOT g_flowUnsaved.bUpdatingGameflow
			#IF IS_DEBUG_BUILD
				Debug_Run_Planning_Board_Widget(m_sAgencyBoard)
			#ENDIF
			
			Maintain_Agency_3_Trigger_Building_State()
			
			//Overrides the position of the Janitor items if they aren't in their default positions.
			IF Is_Heist_Display_Group_Active(HEIST_AGENCY, PBDG_0)
				IF NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_AGENCY_1)
					IF NOT IS_BIT_SET(m_iState, BIT_REPOSITIONED_JANITOR_ITEMS_A)
						m_sAgencyBoard.sData.sStaticItemPos[0].x = 302
						m_sAgencyBoard.sData.sStaticItemPos[0].y = 191
						m_sAgencyBoard.sData.sStaticItemPos[1].x = 309
						m_sAgencyBoard.sData.sStaticItemPos[1].y = 109
						m_sAgencyBoard.sData.sStaticItemPos[2].x = 196
						m_sAgencyBoard.sData.sStaticItemPos[2].y = 318
						m_sAgencyBoard.sData.sStaticItemPos[3].x = 220
						m_sAgencyBoard.sData.sStaticItemPos[3].y = 227
						CPRINTLN(DEBUG_HEIST, "Repositoned Janitor items while on Agency Heist 1.")
						SET_BIT(m_iState, BIT_REPOSITIONED_JANITOR_ITEMS_A)
					ENDIF
				ELSE
					IF NOT IS_BIT_SET(m_iState, BIT_REPOSITIONED_JANITOR_ITEMS_B)
						m_sAgencyBoard.sData.sStaticItemPos[0].x = 0
						m_sAgencyBoard.sData.sStaticItemPos[0].y = 0
						m_sAgencyBoard.sData.sStaticItemPos[1].x = 0
						m_sAgencyBoard.sData.sStaticItemPos[1].y = 0
						m_sAgencyBoard.sData.sStaticItemPos[2].x = 0
						m_sAgencyBoard.sData.sStaticItemPos[2].y = 0
						m_sAgencyBoard.sData.sStaticItemPos[3].x = 0
						m_sAgencyBoard.sData.sStaticItemPos[3].y = 0
						CPRINTLN(DEBUG_HEIST, "Repositoned Janitor items after having passed Agency Heist 1.")
						SET_BIT(m_iState, BIT_REPOSITIONED_JANITOR_ITEMS_B)
					ENDIF
				ENDIF
			ENDIF
			
			Is_Heist_Display_Group_Active(HEIST_AGENCY, PBDG_0)
			
			//Only draw the board while the Sweatshop hasn't been cleared out to be burned.
			IF g_savedGlobals.sBuildingData.eBuildingState[BUILDINGNAME_IPL_SWEATSHOP_WITH_INTERIOR] = BUILDINGSTATE_DESTROYED
			AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
			
				//Stream the board in and out when necessary.
				Manage_Board_Streaming(m_sAgencyBoard, BOARD_LOAD_DIST, BOARD_UNLOAD_DIST)
				
				//Check for the flow auto toggling the board.
				Manage_Board_View_Auto_Toggling(m_sAgencyBoard)
			
				//Load and run the planning board intro cutscene when requested.
				//Loading happens during mission Agency Heist 3.
				Manage_Agency_Heist_Board_Intro_Cutscene()
				
				//Load and run the planning board exit cutscene when requested.
				Manage_Agency_Heist_Board_Exit_Cutscene()
			ELSE
				//Clean up the board if it exists and the sweatshop has been burned.
				IF IS_BIT_SET(m_sAgencyBoard.iState, BIT_BOARD_LOADED)
					CPRINTLN(DEBUG_HEIST, "Cleaning up agency heist planning board as the planning location has been destroyed.")
					Cleanup_Planning_Board(m_sAgencyBoard)
				ENDIF
			ENDIF

			IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
			OR IS_BIT_SET(m_sAgencyBoard.iState, BIT_BOARD_VIEW_INITIALISED)
				//Allow the player to interact with the board if it is created.
				Manage_Board_Viewing(m_sAgencyBoard)
				
				//Check for flow requesting the board switches mode.
				Manage_Board_Mode_Switching(m_sAgencyBoard)
				
				//Ensure that changes to boards display groups are kept up-to-date.
				Update_Board_Display_Groups(m_sAgencyBoard)
			ENDIF
		ENDIF
		
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("agency_heist3A")) > 0)
		OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("agency_heist3B")) > 0)
			HEIST_TIMER_UPDATE()
		ENDIF
		
		// Update the Getaway Vehicle TODO list item based on the missions completion flowflag.
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_AGENCY_PREP_2_DONE)
			IF NOT IS_REPEAT_PLAY_ACTIVE()
				IF NOT IS_BIT_SET(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_AGENCY], ENUM_TO_INT(PBDG_8))
					SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_AGENCY, PBDG_8, TRUE)
				ENDIF
			ELSE
				IF IS_BIT_SET(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_AGENCY], ENUM_TO_INT(PBDG_8))
					SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_AGENCY, PBDG_8, FALSE)
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_AGENCY], ENUM_TO_INT(PBDG_8))
				SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_AGENCY, PBDG_8, FALSE)
			ENDIF
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	Remove_Script_From_Relaunch_List(LAUNCH_BIT_HEIST_CTRL_AGENCY)
	Terminate_Heist_Controller()
	
ENDSCRIPT
