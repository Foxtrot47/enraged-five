//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 01/06/10			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						    Rural Bank Heist Control Script						│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_interiors.sch"

USING "heist_board_data_rural.sch"
USING "heist_planning_board.sch"
USING "heist_ctrl_generic.sch"
USING "flow_public_core_override.sch"
USING "script_misc.sch"
USING "cutscene_public.sch"
USING "selector_public.sch"

//═══════════════════════════════╡ Constants ╞═════════════════════════════════

CONST_FLOAT 	BOARD_LOAD_DIST		32.0
CONST_FLOAT		BOARD_UNLOAD_DIST	35.0

//═══════════════════════════════╡ Variables ╞═════════════════════════════════

//State flags.
CONST_INT		BIT_BOARD_VIEW_ACTIVATED		0
CONST_INT		BIT_CANDIDATE_ID_SECURED		1
CONST_INT		BIT_GRAB_TREVOR_ENTITY			2
CONST_INT		BIT_GRAB_MICHAEL_ENTITY			3
CONST_INT		BIT_LESTER_PUT_UP_BOARD			4
CONST_INT		BIT_PLAYER_ARRIVED_IN_VEHICLE	5
CONST_INT		BIT_PLAYER_VEHCILE_MOVED		6
CONST_INT		BIT_SETUP_TREVOR_SWITCH			7
CONST_INT		BIT_LOADING_MIKE_WIN_CUT		8
CONST_INT		BIT_LOADING_TREV_WIN_CUT		9
CONST_INT		BIT_LOADING_EXIT_CUTSCENE		10
CONST_INT		BIT_TREVOR_PUKING				11
CONST_INT		BIT_TREVOR_PUKE_AUDIO_PLAYING	12
CONST_INT		BIT_TREVOR_PUKE_PART_A_RUNNING	13
CONST_INT		BIT_TREVOR_PUKE_PART_B_RUNNING	14
CONST_INT		BIT_TREVOR_PUKE_PART_C_RUNNING	15
CONST_INT		BIT_TREVOR_PUKE_PART_D_RUNNING	16
CONST_INT		BIT_TREVOR_COMMENTING			17
CONST_INT		BIT_CHARACTERS_HIDDEN			18
CONST_INT		BIT_FADE_IN_FROM_SHITSKIP		19
CONST_INT		BIT_CUTSCENE_SKIPPED			20


//═══════════════════════════════╡ Variables ╞═════════════════════════════════

//General state tracking.
INT m_iState						= 0
INT m_iTimer 						= -1
INT m_iUpperBodyAnimTimer			= -1
INT m_iMissionCandidateID	 		= NO_CANDIDATE_ID

//Cutscene load requests.
INT m_iIntroCutsceneRequestID = NULL_OFFMISSION_CUTSCENE_REQUEST

//Player vehicle tracking.
VEHICLE_INDEX			m_vehPlayer
VEHICLE_SETUP_STRUCT 	m_sPlayerVehicle
SCENARIO_BLOCKING_INDEX	m_scenarioBlock
INT 					m_navmeshBlock = -1
CAMERA_INDEX			m_vomitCam

PED_INDEX				pedLester
PED_INDEX 				pedMichael
PTFX_ID 				ptfxTrevorPuke
BOOL 					bStartedExitCutsceneAsTrevor

SELECTOR_PED_STRUCT 	sSelector


//═══════════════════════════════╡ Structures ╞════════════════════════════════

PlanningBoard m_sRuralBankBoard

//══════════════════════════════╡ Procedures ╞═════════════════════════════════


PROC Store_Last_Player_Vehicle()
	//Vehicle.
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			m_vehPlayer = GET_PLAYERS_LAST_VEHICLE()
			IF IS_VEHICLE_DRIVEABLE(m_vehPlayer)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(m_vehPlayer), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 30.0
					GET_VEHICLE_SETUP(m_vehPlayer, m_sPlayerVehicle)
					SET_BIT(m_iState, BIT_PLAYER_ARRIVED_IN_VEHICLE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

 
PROC Recreate_Player_Vehicle_In_Safe_Location(VECTOR paramPosition, FLOAT paramHeading)
	IF IS_BIT_SET(m_iState, BIT_PLAYER_ARRIVED_IN_VEHICLE)
		IF NOT IS_BIT_SET(m_iState, BIT_PLAYER_VEHCILE_MOVED)
			REQUEST_MODEL(m_sPlayerVehicle.eModel)
			IF HAS_MODEL_LOADED(m_sPlayerVehicle.eModel)
				CLEAR_AREA_OF_VEHICLES(paramPosition, 50.0, TRUE)
				m_vehPlayer = CREATE_VEHICLE(m_sPlayerVehicle.eModel, paramPosition, paramHeading)
				SET_VEHICLE_SETUP(m_vehPlayer, m_sPlayerVehicle)
				SET_MISSION_VEHICLE_GEN_VEHICLE(m_vehPlayer, paramPosition, paramHeading)
				SET_MODEL_AS_NO_LONGER_NEEDED(m_sPlayerVehicle.eModel)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(m_vehPlayer)
				SET_BIT(m_iState, BIT_PLAYER_VEHCILE_MOVED)
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC Switch_On_Planning_Board_View()

	//Seamless transition into board view. Toggle board on and disable board exit.
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, HEIST_RURAL_BANK, TRUE)
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_TOGGLE_BOARD, HEIST_RURAL_BANK, TRUE)
	Manage_Board_View_Auto_Toggling(m_sRuralBankBoard)
	Manage_Board_Viewing(m_sRuralBankBoard)
	
ENDPROC

PROC Terminate_Heist_Controller()
	
	//Ensure the planning board has been cleaned up before the control script is terminated.
	WHILE NOT Cleanup_Planning_Board(m_sRuralBankBoard)
		WAIT(0)
	ENDWHILE
	
	//Cleanup flow flag states.
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING, FALSE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING, FALSE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_DO_BOARD_EXIT_CUTSCENE, FALSE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_BOARD_EXIT_CUTSCENE, FALSE)
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, HEIST_RURAL_BANK, FALSE)
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_TOGGLE_BOARD, HEIST_RURAL_BANK, FALSE)
	
	//Remove area blocks and restore wanted levels.
	REMOVE_SCENARIO_BLOCKING_AREA(m_scenarioBlock)
	IF m_navmeshBlock != -1
		IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(m_navmeshBlock)
			REMOVE_NAVMESH_BLOCKING_OBJECT(m_navmeshBlock)
		ENDIF
	ENDIF
	CLEAR_PED_NON_CREATION_AREA()
	SET_PED_PATHS_IN_AREA(m_sRuralBankBoard.sData.vAreaBlockCenter-m_sRuralBankBoard.sData.vAreaBlockDimensions, m_sRuralBankBoard.sData.vAreaBlockCenter+m_sRuralBankBoard.sData.vAreaBlockDimensions, TRUE)
	SET_MAX_WANTED_LEVEL(5)
	
	//Clean up any cutscenes we have loaded.
	REMOVE_CUTSCENE()
	
	//Release the mission candidate ID if we have one.
	IF m_iMissionCandidateID != NO_CANDIDATE_ID
		Mission_Over(m_iMissionCandidateID)
	ENDIF
	
	CPRINTLN(DEBUG_HEIST, "Rural bank heist controller terminated.")
	TERMINATE_THIS_THREAD()
	
ENDPROC


PROC Manage_Rural_Heist_Board_Intro_Cutscene()

	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING)
	OR GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING)
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			//Normal playthrough, play the intro cutscene leading in to the planning board interactions.
			//Pick the correct sections based on who won the race at the end of the setup mission.
			IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING)
				IF NOT IS_BIT_SET(m_iState, BIT_LOADING_MIKE_WIN_CUT)
					CPRINTLN(DEBUG_HEIST, "planning board intro scene - BIT_LOADING_MIKE_WIN_CUT not set, setting now.")
					IF IS_BIT_SET(m_iState, BIT_LOADING_TREV_WIN_CUT)
						CPRINTLN(DEBUG_HEIST, "planning board intro scene - BIT_LOADING_TREV_WIN_CUT is set, clearing and removing cutscene now.")
						REMOVE_CUTSCENE()
						CLEAR_BIT(m_iState, BIT_LOADING_TREV_WIN_CUT)
					ENDIF
					CPRINTLN(DEBUG_HEIST, "planning board intro scene - loading mike win cut.")
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("RBHS_MCS_3", CS_SECTION_1|CS_SECTION_3)
					SET_BIT(m_iState, BIT_LOADING_MIKE_WIN_CUT)
				ENDIF
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					CPRINTLN(DEBUG_HEIST, "planning board intro scene - setting Trevor prop variation in mike win scene.")
					SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_HEAD, 		0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_HAIR, 		0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_TORSO, 		0, 1)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_LEG, 		1, 1)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_HAND, 		0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_TEETH, 		0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_SPECIAL, 	1, 0)
					SET_CUTSCENE_PED_PROP_VARIATION("LESTER",ANCHOR_EYES, 0, 0)
				ENDIF
			ELSE
				IF NOT IS_BIT_SET(m_iState, BIT_LOADING_TREV_WIN_CUT)
					CPRINTLN(DEBUG_HEIST, "planning board intro scene - BIT_LOADING_TREV_WIN_CUT not set, setting now.")
					IF IS_BIT_SET(m_iState, BIT_LOADING_MIKE_WIN_CUT)
						CPRINTLN(DEBUG_HEIST, "planning board intro scene - BIT_LOADING_MIKE_WIN_CUT is set, clearing and removing cutscene now.")
						REMOVE_CUTSCENE()
						CLEAR_BIT(m_iState, BIT_LOADING_MIKE_WIN_CUT)
					ENDIF
					CPRINTLN(DEBUG_HEIST, "planning board intro scene - loading trev win cut.")
					REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("RBHS_MCS_3", CS_SECTION_2|CS_SECTION_3)
					SET_BIT(m_iState, BIT_LOADING_TREV_WIN_CUT)
				ENDIF
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					CPRINTLN(DEBUG_HEIST, "planning board intro scene - setting Trevor prop variation in Trev win scene.")
					SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_HEAD, 		0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_HAIR, 		0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_TORSO, 		0, 1)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_LEG, 		1, 1)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_HAND, 		0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_TEETH, 		0, 0)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_SPECIAL, 	1, 0)
					SET_CUTSCENE_PED_PROP_VARIATION("LESTER",ANCHOR_EYES, 0, 0)
				ENDIF
			ENDIF
			
			IF GET_MISSION_COMPLETE_STATE(SP_HEIST_RURAL_1)
				IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
					IF Request_Mission_Launch(m_iMissionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
						CPRINTLN(DEBUG_HEIST, "Rural Heist control thread secured the mission flag.")
						
						CLEAR_AUTOSAVE_REQUESTS()
						
						//Setup area blocks around the board.
						m_scenarioBlock = ADD_SCENARIO_BLOCKING_AREA(m_sRuralBankBoard.sData.vAreaBlockCenter-m_sRuralBankBoard.sData.vAreaBlockDimensions, m_sRuralBankBoard.sData.vAreaBlockCenter+m_sRuralBankBoard.sData.vAreaBlockDimensions)
						m_navmeshBlock = ADD_NAVMESH_BLOCKING_OBJECT(m_sRuralBankBoard.sData.vAreaBlockCenter, m_sRuralBankBoard.sData.vAreaBlockDimensions*1.5, 0.0)
						SET_PED_NON_CREATION_AREA(m_sRuralBankBoard.sData.vAreaBlockCenter-m_sRuralBankBoard.sData.vAreaBlockDimensions, m_sRuralBankBoard.sData.vAreaBlockCenter+m_sRuralBankBoard.sData.vAreaBlockDimensions)
						SET_PED_PATHS_IN_AREA(m_sRuralBankBoard.sData.vAreaBlockCenter-m_sRuralBankBoard.sData.vAreaBlockDimensions, m_sRuralBankBoard.sData.vAreaBlockCenter+m_sRuralBankBoard.sData.vAreaBlockDimensions, FALSE)
						
						//Stop the player getting a wanted level for the duration of this planning board sequence.
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
							SET_MAX_WANTED_LEVEL(0)
						ENDIF
						
						SET_BIT(m_iState, BIT_CANDIDATE_ID_SECURED)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(m_iState, BIT_CANDIDATE_ID_SECURED)

				IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE("RBHS_MCS_3")
				
					IF NOT IS_CUTSCENE_PLAYING()

					
						CPRINTLN(DEBUG_HEIST, "Starting Rural Heist board intro cutscene.")
						DISABLE_CELLPHONE(TRUE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						IF DOES_ENTITY_EXIST(m_vehPlayer)
							IF IS_VEHICLE_DRIVEABLE(m_vehPlayer)
								SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(m_vehPlayer, SC_DOOR_FRONT_LEFT, FALSE)
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
							SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
							pedLester = g_sTriggerSceneAssets.ped[0]
							g_sTriggerSceneAssets.ped[0] = NULL
							CPRINTLN(DEBUG_HEIST, "Lester grabbed for cutscene.")
							//DELETE_PED(pedLester)
							//REGISTER_ENTITY_FOR_CUTSCENE(pedLester, "LESTER", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "MICHAEL", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
							ENDIF
							IF DOES_ENTITY_EXIST(sSelector.pedID[SELECTOR_PED_TREVOR])
							AND NOT IS_ENTITY_DEAD(sSelector.pedID[SELECTOR_PED_TREVOR])
								CPRINTLN(DEBUG_HEIST, "Trevor already exists.")
								REGISTER_ENTITY_FOR_CUTSCENE(sSelector.pedID[SELECTOR_PED_TREVOR], 	"TREVOR", 		CU_ANIMATE_EXISTING_SCRIPT_ENTITY, 			PLAYER_TWO)
							ELSE
								CPRINTLN(DEBUG_HEIST, "Trevor to be made by cutscene.")
								REGISTER_ENTITY_FOR_CUTSCENE(sSelector.pedID[SELECTOR_PED_TREVOR], 	"TREVOR", 		CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,	PLAYER_TWO)
								SET_BIT(m_iState, BIT_GRAB_TREVOR_ENTITY)
							ENDIF
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "TREVOR", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
							ENDIF
							IF DOES_ENTITY_EXIST(pedMichael)
							AND NOT IS_ENTITY_DEAD(pedMichael)
								CPRINTLN(DEBUG_HEIST, "Michael already exists.")
								REGISTER_ENTITY_FOR_CUTSCENE(pedMichael, "MICHAEL", 		CU_ANIMATE_EXISTING_SCRIPT_ENTITY, 			PLAYER_ZERO)
							ELSE
								CPRINTLN(DEBUG_HEIST, "Michael to be made by cutscene.")
								REGISTER_ENTITY_FOR_CUTSCENE(pedMichael, "MICHAEL", 		CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,	PLAYER_ZERO)
								SET_BIT(m_iState, BIT_GRAB_MICHAEL_ENTITY)
							ENDIF
						ENDIF
						
						ADD_PED_FOR_DIALOGUE(m_sRuralBankBoard.sPedsForConversation, 0, NULL, "MICHAEL", TRUE)
						ADD_PED_FOR_DIALOGUE(m_sRuralBankBoard.sPedsForConversation, 2, PLAYER_PED_ID(), "TREVOR", TRUE)
						ADD_PED_FOR_DIALOGUE(m_sRuralBankBoard.sPedsForConversation, 3, NULL, "LESTER", TRUE)												
							
						m_iTimer = GET_GAME_TIMER()
						
						START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
						
					ELSE
					
						// #1966073 Prevent replay recording while a planning board cutscene is active.
						REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
					
						// this is to force a fade in after cutscene cams are rendering if the pllayer has skipped the last stage of paleto setup - PLEASE DO NOT REMOVE
						IF NOT IS_BIT_SET(m_iState, BIT_FADE_IN_FROM_SHITSKIP)
							IF NOT IS_SCREEN_FADED_IN()
							AND NOT IS_SCREEN_FADING_IN()
								DO_SCREEN_FADE_IN(500)
							ENDIF
							SET_BIT(m_iState, BIT_FADE_IN_FROM_SHITSKIP)
						ENDIF
					
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
							SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
							DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
						ENDIF
						
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
							SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[1], TRUE, TRUE)
							DELETE_VEHICLE(g_sTriggerSceneAssets.veh[1])
						ENDIF
						
						IF DOES_ENTITY_EXIST(pedLester)
							DELETE_PED(pedLester)
						ENDIF
						
						IF IS_BIT_SET(m_iState, BIT_GRAB_TREVOR_ENTITY)
							IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor", PLAYER_TWO))
								CPRINTLN(DEBUG_HEIST, "Grabbing Trevor's entity.")
								sSelector.pedID[SELECTOR_PED_TREVOR] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor", PLAYER_TWO))
								CLEAR_BIT(m_iState, BIT_GRAB_TREVOR_ENTITY)
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(m_iState, BIT_GRAB_MICHAEL_ENTITY)
							IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael", PLAYER_ZERO))
								CPRINTLN(DEBUG_HEIST, "Grabbing Michael's entity.")
								pedMichael = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael", PLAYER_ZERO))
								CLEAR_BIT(m_iState, BIT_GRAB_MICHAEL_ENTITY)
							ENDIF
						ENDIF
						
						IF NOT IS_BIT_SET(m_iState, BIT_LESTER_PUT_UP_BOARD)
							IF (GET_CUTSCENE_TIME()) > 43160 AND GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING)
							OR (GET_CUTSCENE_TIME()) > 42740 AND GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING)
								CPRINTLN(DEBUG_HEIST, "Lester put up board mid cutscene.")
								SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_RURAL_BANK, PBDG_0, TRUE)
								SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_RURAL_BANK, PBDG_1, TRUE)
								SET_BIT(m_iState, BIT_LESTER_PUT_UP_BOARD)
							ENDIF
						ENDIF
						
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("TREVOR", PLAYER_TWO)
								CPRINTLN(DEBUG_HEIST, "Setting exit state of Trevor.")
								IF NOT DOES_ENTITY_EXIST(sSelector.pedID[SELECTOR_PED_TREVOR])
									CPRINTLN(DEBUG_HEIST, "Trevor doesn't exist.")
								   	sSelector.pedID[SELECTOR_PED_TREVOR] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor", PLAYER_TWO))
									CLEAR_BIT(m_iState, BIT_GRAB_TREVOR_ENTITY)
								ENDIF
								IF NOT IS_ENTITY_DEAD(sSelector.pedID[SELECTOR_PED_TREVOR])
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelector.pedID[SELECTOR_PED_TREVOR], TRUE)
									SET_PED_CONFIG_FLAG(sSelector.pedID[SELECTOR_PED_TREVOR], PCF_DisableExplosionReactions, TRUE)
									SET_PED_CONFIG_FLAG(sSelector.pedID[SELECTOR_PED_TREVOR], PCF_RunFromFiresAndExplosions, FALSE)
									SET_PED_CONFIG_FLAG(sSelector.pedID[SELECTOR_PED_TREVOR], PCF_ListensToSoundEvents, FALSE)
								ENDIF
							ENDIF
						ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MICHAEL", PLAYER_ZERO)
								CPRINTLN(DEBUG_HEIST, "Setting exit state of Michael.")
								IF NOT DOES_ENTITY_EXIST(pedMichael)
									CPRINTLN(DEBUG_HEIST, "Michael doesn't exist.")
								   	pedMichael = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael", PLAYER_ZERO))
									CLEAR_BIT(m_iState, BIT_GRAB_MICHAEL_ENTITY)
								ENDIF
								IF NOT IS_ENTITY_DEAD(pedMichael)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMichael, TRUE)
									SET_PED_CONFIG_FLAG(pedMichael, PCF_DisableExplosionReactions, TRUE)
									SET_PED_CONFIG_FLAG(pedMichael, PCF_RunFromFiresAndExplosions, FALSE)
									SET_PED_CONFIG_FLAG(pedMichael, PCF_ListensToSoundEvents, FALSE)
								ENDIF
							ENDIF
						ENDIF
						
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("LESTER")
							CPRINTLN(DEBUG_HEIST, "Setting exit state of Michael.")
							IF DOES_ENTITY_EXIST(pedLester)
								SET_ENTITY_COORDS(pedLester,  <<1398.1981, 3605.5999, 37.9419>>)
								SET_ENTITY_HEADING(pedLester, 194.2789)
								DELETE_PED(pedLester)
							ENDIF
						ENDIF
						
						IF CAN_SET_EXIT_STATE_FOR_CAMERA()
							IF NOT IS_BIT_SET(m_iState, BIT_BOARD_VIEW_ACTIVATED)
								CPRINTLN(DEBUG_HEIST, "Activating Rural Heist planning board view behind cutscene.")
								Switch_On_Planning_Board_View()
								SET_BIT(m_iState, BIT_BOARD_VIEW_ACTIVATED)
							ENDIF
							CPRINTLN(DEBUG_HEIST, "Rural Heist board intro cutscene set camera exit state.")
							CLEAR_AREA(<<1397.8835, 3594.6270, 33.9271>>, 50.0, TRUE)
							
							CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MISSION_VEH)
							SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING, FALSE)
							SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING, FALSE)
							
							IF NOT IS_BIT_SET(m_iState, BIT_LESTER_PUT_UP_BOARD)
								CPRINTLN(DEBUG_HEIST, "Lester put up board as skipping cutscene.")
								SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_RURAL_BANK, PBDG_0, TRUE)
								SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_RURAL_BANK, PBDG_1, TRUE)
								Delete_Planning_Board(m_sRuralBankBoard)
								SET_BIT(m_iState, BIT_LESTER_PUT_UP_BOARD)
							ENDIF
							
							IF NOT IS_BIT_SET(m_iState, BIT_CHARACTERS_HIDDEN)
								IF NOT IS_ENTITY_DEAD(pedMichael)
									SET_ENTITY_VISIBLE(pedMichael, FALSE)
								ENDIF
								IF NOT IS_ENTITY_DEAD(pedLester)
									SET_ENTITY_VISIBLE(pedLester, FALSE)
								ENDIF
								IF DOES_ENTITY_EXIST(sSelector.pedID[SELECTOR_PED_TREVOR])
									SET_ENTITY_VISIBLE(sSelector.pedID[SELECTOR_PED_TREVOR], FALSE)
								ENDIF
								SET_BIT(m_iState, BIT_CHARACTERS_HIDDEN)
							ENDIF
						ENDIF

						IF WAS_CUTSCENE_SKIPPED()
							SET_BIT(m_iState, BIT_CUTSCENE_SKIPPED)
						ELSE
							IF NOT IS_SCREEN_FADED_IN()
							AND NOT IS_SCREEN_FADING_IN()
								DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(m_iState, BIT_CUTSCENE_SKIPPED)
							IF IS_SCREEN_FADED_OUT()
							
								IF NOT IS_BIT_SET(m_iState, BIT_BOARD_VIEW_ACTIVATED)
									CPRINTLN(DEBUG_HEIST, "Activating Rural Heist planning board view due to cutscene skip.")
									Switch_On_Planning_Board_View()
									SET_BIT(m_iState, BIT_BOARD_VIEW_ACTIVATED)
								ENDIF

								IF IS_BIT_SET(m_iState, BIT_LOADING_MIKE_WIN_CUT)
									CPRINTLN(DEBUG_HEIST, "Clearing BIT_LOADING_MIKE_WIN_CUT as cutscene is over.")
									CLEAR_BIT(m_iState, BIT_LOADING_MIKE_WIN_CUT)
								ENDIF
								IF IS_BIT_SET(m_iState, BIT_LOADING_TREV_WIN_CUT)
									CPRINTLN(DEBUG_HEIST, "Clearing BIT_LOADING_TREV_WIN_CUT as cutscene is over.")
									CLEAR_BIT(m_iState, BIT_LOADING_TREV_WIN_CUT)
								ENDIF
								
								SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING, FALSE)
								SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING, FALSE)
								
								IF NOT IS_BIT_SET(m_iState, BIT_LESTER_PUT_UP_BOARD)
									CPRINTLN(DEBUG_HEIST, "Lester put up board as cutscene was skipped.")
									SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_RURAL_BANK, PBDG_0, TRUE)
									SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_RURAL_BANK, PBDG_1, TRUE)
									SET_BIT(m_iState, BIT_LESTER_PUT_UP_BOARD)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
				IF Request_Mission_Launch(m_iMissionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
					CPRINTLN(DEBUG_HEIST, "Rural Heist control thread secured the mission flag.")
					SET_BIT(m_iState, BIT_CANDIDATE_ID_SECURED)
				ENDIF
			ENDIF
			IF IS_BIT_SET(m_iState, BIT_CANDIDATE_ID_SECURED)
				//Repeat play, jump straight into the board.
				ADD_PED_FOR_DIALOGUE(m_sRuralBankBoard.sPedsForConversation, 0, NULL, "MICHAEL", TRUE)
				ADD_PED_FOR_DIALOGUE(m_sRuralBankBoard.sPedsForConversation, 2, PLAYER_PED_ID(), "TREVOR", TRUE)
				ADD_PED_FOR_DIALOGUE(m_sRuralBankBoard.sPedsForConversation, 3, NULL, "LESTER", TRUE)							
				
				CPRINTLN(DEBUG_HEIST, "Activating Rural Heist planning board for repeat play.")
				Switch_On_Planning_Board_View()
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_MIKE_WIN_ENDING, FALSE)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_TREV_WIN_ENDING, FALSE)
			ENDIF
		ENDIF
		
	ELSE
		IF m_iIntroCutsceneRequestID != NULL_OFFMISSION_CUTSCENE_REQUEST
			END_OFFMISSION_CUTSCENE_REQUEST(m_iIntroCutsceneRequestID)
		ENDIF
		IF HAS_THIS_CUTSCENE_LOADED("RBHS_MCS_3")
			CPRINTLN(DEBUG_HEIST, "Cleaning up Rural Heist planning board intro cutscene.")
			REMOVE_CUTSCENE()
		ENDIF
		IF IS_BIT_SET(m_iState, BIT_LOADING_MIKE_WIN_CUT)
			CPRINTLN(DEBUG_HEIST, "Clearing BIT_LOADING_MIKE_WIN_CUT as flow flag is no longer set.")
			CLEAR_BIT(m_iState, BIT_LOADING_MIKE_WIN_CUT)
		ENDIF
		IF IS_BIT_SET(m_iState, BIT_LOADING_TREV_WIN_CUT)
			CPRINTLN(DEBUG_HEIST, "Clearing BIT_LOADING_TREV_WIN_CUT as flow flag is no longer set.")
			CLEAR_BIT(m_iState, BIT_LOADING_TREV_WIN_CUT)
		ENDIF
	ENDIF

ENDPROC


PROC Manage_Rural_Heist_Board_Exit_Cutscene()

	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_BOARD_EXIT_CUTSCENE)
		IF NOT IS_BIT_SET(m_iState, BIT_LOADING_EXIT_CUTSCENE)
			REQUEST_CUTSCENE("RBHS_MSC_3_P3")
			REQUEST_MODEL(PLAYER_TWO)
			REQUEST_ANIM_DICT("MISSHEISTPALETOSCORE1LEADINOUT")
			REQUEST_PTFX_ASSET()
			SET_BIT(m_iState, BIT_LOADING_EXIT_CUTSCENE)
		ELIF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_HEAD, 		0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_HAIR, 		0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_TORSO, 		0, 1)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_LEG, 		1, 1)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_HAND, 		0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_TEETH, 		0, 0)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("LESTER", PED_COMP_SPECIAL, 	1, 0)
			SET_CUTSCENE_PED_PROP_VARIATION("LESTER",ANCHOR_EYES, 0, 0)
		ENDIF
	ENDIF
	
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_DO_BOARD_EXIT_CUTSCENE)
	
		//Check if the cutscene has loaded.
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_BOARD_EXIT_CUTSCENE)
			IF HAS_CUTSCENE_LOADED()
			AND HAS_MODEL_LOADED(PLAYER_TWO)
			AND HAS_ANIM_DICT_LOADED("MISSHEISTPALETOSCORE1LEADINOUT")
			AND HAS_PTFX_ASSET_LOADED()
				CPRINTLN(DEBUG_HEIST, "Exit cutscene for Rural Heist board loaded. Running.")
				IF NOT IS_ENTITY_DEAD(pedLester)
					REGISTER_ENTITY_FOR_CUTSCENE(pedLester, "LESTER", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				IF NOT IS_ENTITY_DEAD(pedMichael)
					REGISTER_ENTITY_FOR_CUTSCENE(pedMichael, "MICHAEL", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
				ENDIF
				IF NOT IS_ENTITY_DEAD(sSelector.pedID[SELECTOR_PED_TREVOR])
					REGISTER_ENTITY_FOR_CUTSCENE(sSelector.pedID[SELECTOR_PED_TREVOR], "TREVOR", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
				ENDIF
				PAUSE_FLOW_HELP_QUEUE(TRUE)
				DISABLE_CELLPHONE(TRUE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				m_iTimer = -1
				
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					bStartedExitCutsceneAsTrevor = TRUE
				ELSE
					bStartedExitCutsceneAsTrevor = FALSE
				ENDIF

				START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)	
				
				IF IS_REPEAT_PLAY_ACTIVE()
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
				ENDIF				
				
				REQUEST_SCRIPT_AUDIO_BANK("Taxi_vomit")
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_BOARD_EXIT_CUTSCENE, FALSE)
			ENDIF
		ELSE
			IF IS_CUTSCENE_PLAYING()
			
				// #1966073 Prevent replay recording while a planning board cutscene is active.
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
				IF IS_BIT_SET(m_iState, BIT_CHARACTERS_HIDDEN)
					IF NOT IS_ENTITY_DEAD(pedMichael)
						SET_ENTITY_VISIBLE(pedMichael, TRUE)
					ENDIF
					IF NOT IS_ENTITY_DEAD(pedLester)
						SET_ENTITY_VISIBLE(pedLester, TRUE)
					ENDIF
					IF DOES_ENTITY_EXIST(sSelector.pedID[SELECTOR_PED_TREVOR])
						SET_ENTITY_VISIBLE(sSelector.pedID[SELECTOR_PED_TREVOR], TRUE)
					ENDIF
					CLEAR_BIT(m_iState, BIT_CHARACTERS_HIDDEN)
				ENDIF
				
				IF NOT DOES_CAM_EXIST(m_vomitCam)
					m_vomitCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<1398.1670, 3605.4375, 39.6443>>, <<-8.2882, 0.0000, 126.8884>>, 50, TRUE)
				ENDIF
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				//Try and switch player to Trevor during cutscene.
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor"))
					IF NOT IS_BIT_SET(m_iState, BIT_SETUP_TREVOR_SWITCH)
						CPRINTLN(DEBUG_HEIST, "Setup Trevor index for player switch.")
						sSelector.pedID[SELECTOR_PED_TREVOR] = GET_PED_INDEX_FROM_ENTITY_INDEX((GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor")))
						SET_BIT(m_iState, BIT_SETUP_TREVOR_SWITCH)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
					CPRINTLN(DEBUG_HEIST, "Attempting switch to Trevor.")
					IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_TREVOR
						IF MAKE_SELECTOR_PED_SELECTION(sSelector, SELECTOR_PED_TREVOR)
							TAKE_CONTROL_OF_SELECTOR_PED(sSelector, FALSE, FALSE)
						ENDIF
					ENDIF
					
					CPRINTLN(DEBUG_HEIST, "Switching player to Trevor on cutscene exit if required.")
					SET_MODEL_AS_NO_LONGER_NEEDED(PLAYER_TWO)
					
					IF NOT IS_REPEAT_PLAY_ACTIVE()
						START_AUDIO_SCENE("PS_1_TREVOR_PUKING")
						VECTOR vTrevPosition = <<1391.8099, 3614.6062, 37.9700>>
						GET_GROUND_Z_FOR_3D_COORD(vTrevPosition, vTrevPosition.Z)
						SEQUENCE_INDEX seqTrevorVomit
						OPEN_SEQUENCE_TASK(seqTrevorVomit)
							TASK_PLAY_ANIM_ADVANCED(NULL, "MISSHEISTPALETOSCORE1LEADINOUT", "TRV_PUKING_LEADOUT", << 1396.911, 3603.942, 38.925 >>, << 0.000, 0.000, 108.000 >>, INSTANT_BLEND_IN, SLOW_BLEND_OUT)
						CLOSE_SEQUENCE_TASK(seqTrevorVomit)
						TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqTrevorVomit)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						SET_BIT(m_iState, BIT_TREVOR_PUKING)
						ptfxTrevorPuke = NULL
					ELSE
						//No lead-out when repeat playing.
						CLEAR_BIT(m_iState, BIT_TREVOR_PUKING)
						CLEAR_BIT(m_iState, BIT_TREVOR_COMMENTING)
					ENDIF
					CPRINTLN(DEBUG_HEIST, "Trevor starting puking.")
					
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE)
					IF NOT IS_REPEAT_PLAY_ACTIVE()
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
					ENDIF
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					CPRINTLN(DEBUG_HEIST, "Camera exit frame for rbhs_mcs_3_p3")
					IF DOES_ENTITY_EXIST(pedLester)
						DELETE_PED(pedLester)
					ENDIF
					IF DOES_ENTITY_EXIST(pedMichael)
						DELETE_PED(pedMichael)
					ENDIF
					ADD_GLOBAL_COMMUNICATION_DELAY(15000)

					RESET_ADAPTATION()
				ENDIF
				
			ELIF IS_BIT_SET(m_iState, BIT_TREVOR_PUKING)
			
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_CHARACTER_WHEEL)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_SPECIAL_ABILITY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_SPECIAL_ABILITY_SECONDARY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_SPECIAL_ABILITY_PC)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_DUCK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_SELECT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_COVER)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_RELOAD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_DIVE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_SPRINT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_ENTER)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_JUMP)
				
				//Block player movement in first person mode until Trevor has taken his hand off the wall.
				IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
					DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_MOVE_UD)
					DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_MOVE_LR)
					DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_UD)
					DISABLE_CONTROL_ACTION (PLAYER_CONTROL, INPUT_LOOK_LR)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-56)
				ENDIF
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				
					IF NOT IS_BIT_SET(m_iState, BIT_TREVOR_PUKE_AUDIO_PLAYING)
						ADD_PED_FOR_DIALOGUE(m_sRuralBankBoard.sPedsForConversation, 2, PLAYER_PED_ID(), "TREVOR", TRUE)
						IF CREATE_CONVERSATION(m_sRuralBankBoard.sPedsForConversation, "RBS1AUD", "RBS1_VOMIT", CONV_PRIORITY_HIGH)
							SET_BIT(m_iState, BIT_TREVOR_PUKE_AUDIO_PLAYING)
						ENDIF
					ENDIF
					
					IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "MISSHEISTPALETOSCORE1LEADINOUT", "TRV_PUKING_LEADOUT")					
						FLOAT fAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "MISSHEISTPALETOSCORE1LEADINOUT", "TRV_PUKING_LEADOUT")				
						CPRINTLN(DEBUG_HEIST, "Puke anim time: ", fAnimTime)
						IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						ENDIF
						IF fAnimTime < 1.0
						AND fAnimTime >= 0.0
							IF fAnimTime > 0.0920 AND fAnimTime < 0.1840
								IF NOT IS_BIT_SET(m_iState, BIT_TREVOR_PUKE_PART_A_RUNNING)
									IF ptfxTrevorPuke = NULL
										ptfxTrevorPuke = START_PARTICLE_FX_LOOPED_ON_PED_BONE("SCR_TREV_PUKE", PLAYER_PED_ID(), <<0,0,0>>, <<0,0,0>>, BONETAG_HEAD, 1)
										SET_BIT(m_iState, BIT_TREVOR_PUKE_PART_A_RUNNING)
										
										IF NOT bStartedExitCutsceneAsTrevor		
											SET_FIRST_PERSON_FLASH_EFFECT_TYPE(FLASH_TYPE_TREVOR) 
										ENDIF
										
										RENDER_SCRIPT_CAMS(FALSE, TRUE, 1000)
										IF DOES_CAM_EXIST(m_vomitCam)
											DESTROY_CAM(m_vomitCam)
										ENDIF
									ENDIF
								ENDIF
							ELIF fAnimTime > 0.2200 AND fAnimTime < 0.3000
								IF NOT IS_BIT_SET(m_iState, BIT_TREVOR_PUKE_PART_B_RUNNING)
									IF ptfxTrevorPuke = NULL
										ptfxTrevorPuke = START_PARTICLE_FX_LOOPED_ON_PED_BONE("SCR_TREV_PUKE", PLAYER_PED_ID(), <<0,0,0>>, <<0,0,0>>, BONETAG_HEAD, 1)
										SET_BIT(m_iState, BIT_TREVOR_PUKE_PART_B_RUNNING)
									ENDIF
								ENDIF
							ELIF fAnimTime > 0.3400 AND fAnimTime < 0.3600
								IF NOT IS_BIT_SET(m_iState, BIT_TREVOR_PUKE_PART_C_RUNNING)
									IF ptfxTrevorPuke = NULL
										ptfxTrevorPuke = START_PARTICLE_FX_LOOPED_ON_PED_BONE("SCR_TREV_PUKE", PLAYER_PED_ID(), <<0,0,0>>, <<0,0,0>>, BONETAG_HEAD, 1)
										SET_BIT(m_iState, BIT_TREVOR_PUKE_PART_C_RUNNING)
									ENDIF
								ENDIF
							ELIF fAnimTime > 0.4222 AND fAnimTime < 0.5160
								IF NOT IS_BIT_SET(m_iState, BIT_TREVOR_PUKE_PART_D_RUNNING)
									IF ptfxTrevorPuke = NULL
										ptfxTrevorPuke = START_PARTICLE_FX_LOOPED_ON_PED_BONE("SCR_TREV_PUKE", PLAYER_PED_ID(), <<0,0,0>>, <<0,0,0>>, BONETAG_HEAD, 1)
										SET_BIT(m_iState, BIT_TREVOR_PUKE_PART_D_RUNNING)
									ENDIF
								ENDIF
							ELIF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxTrevorPuke)
								STOP_PARTICLE_FX_LOOPED(ptfxTrevorPuke)
								ptfxTrevorPuke = NULL
							ENDIF
							IF fAnimTime > 0.98
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								//B* - 2069349
								IF GET_FOLLOW_PED_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
									TASK_PLAY_ANIM(PLAYER_PED_ID(), "MISSHEISTPALETOSCORE1LEADINOUT", "TRV_UPPERBODY_LeadOut_FPS", SLOW_BLEND_IN, WALK_BLEND_OUT, -1, AF_UPPERBODY|AF_SECONDARY)
								ELSE
									TASK_PLAY_ANIM(PLAYER_PED_ID(), "MISSHEISTPALETOSCORE1LEADINOUT", "TRV_UPPERBODY_LEADOUT", SLOW_BLEND_IN, WALK_BLEND_OUT, -1, AF_UPPERBODY|AF_SECONDARY)
								ENDIF
								IF IS_AUDIO_SCENE_ACTIVE("PS_1_TREVOR_PUKING")
									STOP_AUDIO_SCENE("PS_1_TREVOR_PUKING")
								ENDIF
							ENDIF
						ENDIF
					ELIF IS_BIT_SET(m_iState, BIT_TREVOR_PUKE_PART_D_RUNNING)
						CLEAR_BIT(m_iState, BIT_TREVOR_PUKING)
					ENDIF
				ENDIF
			ELIF m_iTimer = -1
				CPRINTLN(DEBUG_HEIST, "Trevor leadout ended.")
				//Cutscene has ended. Clean up.
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				REMOVE_ANIM_DICT("MISSHEISTPALETOSCORE1LEADINOUT")
				REMOVE_PTFX_ASSET()
				
				IF IS_REPEAT_PLAY_ACTIVE() // no delay in repeat play
					DO_SCREEN_FADE_OUT(0)
					m_iTimer = 0
				ELSE
					m_iTimer = GET_GAME_TIMER() + 1000
				ENDIF
				
			ELIF GET_GAME_TIMER() > m_iTimer
				IF NOT IS_REPEAT_PLAY_ACTIVE()
					g_bMissionStatSystemBlocker = FALSE
					g_sAutosaveData.bFlushAutosaves = FALSE
					MAKE_AUTOSAVE_REQUEST()
				ENDIF
			
				//Pause for pacing has passed.
				DISABLE_CELLPHONE(FALSE)
				ResetLastKnownPedInfo(g_savedGlobals.sPlayerData.sInfo, SP_HEIST_RURAL_1)
				
				UpdateLastKnownPedInfoPostMission(g_savedGlobals.sPlayerData.sInfo, SP_HEIST_RURAL_1)
				
				IF DOES_ENTITY_EXIST(m_vehPlayer)
					IF IS_VEHICLE_DRIVEABLE(m_vehPlayer)
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(m_vehPlayer, SC_DOOR_FRONT_LEFT, TRUE)
					ENDIF
				ENDIF
				
				Mission_Over(m_iMissionCandidateID)
				CLEAR_BIT(m_iState, BIT_CANDIDATE_ID_SECURED)
				
				//Remove area blocks and restore wanted levels.
				REMOVE_SCENARIO_BLOCKING_AREA(m_scenarioBlock)
				IF m_navmeshBlock != -1
					IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(m_navmeshBlock)
						REMOVE_NAVMESH_BLOCKING_OBJECT(m_navmeshBlock)
					ENDIF
				ENDIF
				CLEAR_PED_NON_CREATION_AREA()
				SET_PED_PATHS_IN_AREA(m_sRuralBankBoard.sData.vAreaBlockCenter-m_sRuralBankBoard.sData.vAreaBlockDimensions, m_sRuralBankBoard.sData.vAreaBlockCenter+m_sRuralBankBoard.sData.vAreaBlockDimensions, TRUE)
				SET_MAX_WANTED_LEVEL(5)
				
				#IF FEATURE_SP_DLC_DIRECTOR_MODE
					UNLOCK_DIRECTOR_HEIST_CHARS_FROM_HEIST(HEIST_RURAL_BANK)
				#ENDIF
				
				CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MISSION_VEH)
				
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_DO_BOARD_EXIT_CUTSCENE, FALSE)
			ENDIF
		ENDIF
	ELIF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_RURAL_LOAD_BOARD_EXIT_CUTSCENE)
		//Clean up the cutscene if neither flow flag is set.
		IF HAS_THIS_CUTSCENE_LOADED("RBHS_MSC_3_P3")
			REMOVE_CUTSCENE()
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//B* - 2069349
		IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "MISSHEISTPALETOSCORE1LEADINOUT", "TRV_UPPERBODY_LEADOUT")
		OR IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "MISSHEISTPALETOSCORE1LEADINOUT", "TRV_UPPERBODY_LeadOut_FPS")
			m_iUpperBodyAnimTimer = GET_GAME_TIMER() + 4000
		ENDIF
	ENDIF
	
	IF GET_GAME_TIMER() < m_iUpperBodyAnimTimer
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_CHARACTER_WHEEL)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_SPECIAL_ABILITY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_SPECIAL_ABILITY_SECONDARY)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_SPECIAL_ABILITY_PC)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_DUCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_SELECT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_COVER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_RELOAD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_DIVE)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_SPRINT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_ENTER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, 	INPUT_ATTACK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL,	INPUT_JUMP)
	ENDIF
	
ENDPROC

//══════════════════════════════╡ Control Loop ╞═══════════════════════════════

SCRIPT

	#IF IS_DEBUG_BUILD
		CPRINTLN(DEBUG_HEIST, "Rural bank heist controller started.")
	#ENDIF
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU | FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		Terminate_Heist_Controller()
	ENDIF
	
	//Register script to be saved as running.
	Register_Script_To_Relaunch_List(LAUNCH_BIT_HEIST_CTRL_RURAL)
	
	//Setup the planning board for this heist.
	Setup_Rural_Bank_Heist_Board_Data(m_sRuralBankBoard.sData, g_sHeistChoiceData[HEIST_CHOICE_RURAL_NO_TANK].iCrewSize)
	Setup_Planning_Board(m_sRuralBankBoard, HEIST_RURAL_BANK)
	
	//Hardcode the Paleto Score heist choice when this controller starts. 
	//There is only one option so always set it.
	IF g_savedGlobals.sFlow.controls.intIDs[FLOWINT_HEIST_CHOICE_RURAL] != HEIST_CHOICE_RURAL_NO_TANK
		g_savedGlobals.sFlow.controls.intIDs[FLOWINT_HEIST_CHOICE_RURAL] = HEIST_CHOICE_RURAL_NO_TANK
	ENDIF
	
	//Set up a dialogue struct to play conversation from.

	
	WHILE NOT Get_Mission_Flow_Flag_State(FLOWFLAG_HEIST_FINISHED_RURAL_BANK)
	
		IF NOT g_flowUnsaved.bUpdatingGameflow
		AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
		
			//Load and run the planning board intro cutscene when requested.
			//Loading happens during mission Rural Bank Setup.
			Manage_Rural_Heist_Board_Intro_Cutscene()
		
			#IF IS_DEBUG_BUILD
				Debug_Run_Planning_Board_Widget(m_sRuralBankBoard)
			#ENDIF
			
			//Stream the board in and out when necessary.
			Manage_Board_Streaming(m_sRuralBankBoard, BOARD_LOAD_DIST, BOARD_UNLOAD_DIST)
			
			//Check for the flow auto toggling the board.
			Manage_Board_View_Auto_Toggling(m_sRuralBankBoard)
			
			//Load and run the planning board exit cutscene when requested.
			Manage_Rural_Heist_Board_Exit_Cutscene()
			
			IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
			OR IS_BIT_SET(m_sRuralBankBoard.iState, BIT_BOARD_VIEW_INITIALISED)
			
				//Allow the player to interact with the board if it is created.
				Manage_Board_Viewing(m_sRuralBankBoard)
				
				//Check for flow requesting the board switches mode.
				Manage_Board_Mode_Switching(m_sRuralBankBoard)
				
				//Ensure that changes to boards display groups are kept up-to-date.
				Update_Board_Display_Groups(m_sRuralBankBoard)
			ENDIF
		ENDIF
		
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("rural_bank_heist")) > 0)
			HEIST_TIMER_UPDATE()
		ENDIF
		
		WAIT(0)
		
	ENDWHILE
	
	Remove_Script_From_Relaunch_List(LAUNCH_BIT_HEIST_CTRL_RURAL)
	Terminate_Heist_Controller()
	
ENDSCRIPT
