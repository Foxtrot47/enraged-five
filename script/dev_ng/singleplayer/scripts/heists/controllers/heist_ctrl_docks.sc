//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 22/07/10			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						  Docks Heist Control Script							│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_interiors.sch"
USING "commands_cutscene.sch"

USING "heist_board_data_docks.sch"
USING "heist_planning_board.sch"
USING "heist_ctrl_generic.sch"
USING "flow_public_core_override.sch"
USING "script_misc.sch"
USING "cutscene_public.sch"
USING "clearmissionarea.sch"

//═══════════════════════════════╡ Constants ╞═════════════════════════════════

CONST_FLOAT 	BOARD_LOAD_DIST		24.0
CONST_FLOAT		BOARD_UNLOAD_DIST	30.0

//═════════════════════════════════╡ ENUMS ╞═══════════════════════════════════

ENUM CutDocksBoard
	INIT_CUTSCENE,
	BOARD_PAN,
	END_CUTSCENE
ENDENUM

//═══════════════════════════════╡ Variables ╞═════════════════════════════════

CONST_INT	BIT_BOARD_VIEW_ACTIVE			0
CONST_INT	BIT_RENDER_BOARD				1
CONST_INT	BIT_CANDIDATE_ID_SECURED		2
CONST_INT	BIT_CHARACTERS_HIDDEN			3
CONST_INT	BIT_LOADING_INTRO_CUTSCENE		4
CONST_INT	BIT_LOADING_EXIT_CUTSCENE		5
CONST_INT	BIT_CUTSCENE_SKIPPED			6
CONST_INT	BIT_REPOSITIONED_VEHICLE		7
CONST_INT	BIT_DOING_TIMELAPSE				8
CONST_INT	BIT_CREATE_SYNC_SCENE			9
CONST_INT	BIT_DOING_LEADOUT				10
CONST_INT	BIT_FADE_IN_FROM_SHITSKIP		11
CONST_INT	BIT_CLEANUP						12


INT m_iState = 0

//Cutscene load requests.
INT m_iMissionCandidateID 	= NO_CANDIDATE_ID

//Leadout scene variables.
INT								m_iLeadoutScene
INT								m_iSelectorBlockTimer
CAMERA_INDEX					m_camLeadoutScene
OBJECT_INDEX					m_oRemote
PED_INDEX						m_pedFloyd

SCENARIO_BLOCKING_INDEX			m_scenarioBlock
INT								m_navmeshBlock = -1

STREAMVOL_ID 					houseVolume
BOOL bPrestreamingExterior
BOOL bPrestreamingInterior
BOOL bMonitorForUnwantedLine


//═══════════════════════════════╡ Structures ╞════════════════════════════════

PlanningBoard m_sDocksBoard
structTimelapse	sToD

//══════════════════════════════╡ Procedures ╞═════════════════════════════════

PROC Terminate_Heist_Controller()
	
	NEW_LOAD_SCENE_STOP()
	IF STREAMVOL_IS_VALID(houseVolume)
		STREAMVOL_DELETE(houseVolume)
	ENDIF	
	
	//Ensure the planning board has been cleaned up before the control script is terminated.
	WHILE NOT Cleanup_Planning_Board(m_sDocksBoard)
		WAIT(0)
	ENDWHILE
	
	//Cleanup flow flag states.
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_PRIME_BOARD_TRANSITION, FALSE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_LOAD_BOARD_EXIT_CUTSCENE, FALSE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_DO_BOARD_EXIT_CUTSCENE, FALSE)
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, HEIST_DOCKS, FALSE)
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_TOGGLE_BOARD, HEIST_DOCKS, FALSE)
	
	//Remove area blocks and restore wanted levels.
	REMOVE_SCENARIO_BLOCKING_AREA(m_scenarioBlock)
	IF m_navmeshBlock != -1
		IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(m_navmeshBlock)
			REMOVE_NAVMESH_BLOCKING_OBJECT(m_navmeshBlock)
		ENDIF
	ENDIF
	CLEAR_PED_NON_CREATION_AREA()
	SET_PED_PATHS_IN_AREA(m_sDocksBoard.sData.vAreaBlockCenter-m_sDocksBoard.sData.vAreaBlockDimensions, m_sDocksBoard.sData.vAreaBlockCenter+m_sDocksBoard.sData.vAreaBlockDimensions, TRUE)
	SET_MAX_WANTED_LEVEL(5)

	//Clean up any cutscenes we have loaded.
	REMOVE_CUTSCENE()
	
	SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL, -1)
	
	//Release the mission candidate ID if we have one.
	IF m_iMissionCandidateID != NO_CANDIDATE_ID
		Mission_Over(m_iMissionCandidateID)
	ENDIF
	
	CPRINTLN(DEBUG_HEIST, "Docks heist controller terminated.")
	TERMINATE_THIS_THREAD()
	
ENDPROC


PROC Switch_On_Planning_Board_View()

	//Seamless transition into board view. Toggle board on and disable board exit.
	IF NOT IS_BIT_SET(m_sDocksBoard.iState, BIT_BOARD_VIEW_INITIALISED)
		//Turn on board view.
		SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, HEIST_DOCKS, TRUE)
		SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_TOGGLE_BOARD, HEIST_DOCKS, TRUE)
		Manage_Board_View_Auto_Toggling(m_sDocksBoard)
		Manage_Board_Viewing(m_sDocksBoard)
	ENDIF
				
ENDPROC


PROC Manage_Docks_Heist_Board_Intro_Cutscene()

	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_PRIME_BOARD_TRANSITION)	
	
		IF NOT IS_REPEAT_PLAY_ACTIVE()
		
			//Normal playthrough, play the intro cutscene leading in to the planning board interactions.
			IF NOT IS_BIT_SET(m_iState, BIT_LOADING_INTRO_CUTSCENE)
				CPRINTLN(DEBUG_HEIST, "Requesting docks heist board intro cutscene.")
				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("LSDHS_MCS_3_P1_CONCAT", CS_SECTION_2|CS_SECTION_3)
				CLEAR_AUTOSAVE_REQUESTS()
				
				//Don't let Kenneth's system pull in Trevor's hat and glasses.
				SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(CHAR_TREVOR)], NUM_PED_COMPONENTS + ENUM_TO_INT(ANCHOR_HEAD))
				SET_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(CHAR_TREVOR)], NUM_PED_COMPONENTS + ENUM_TO_INT(ANCHOR_EYES))
				
				SET_BIT(m_iState, BIT_LOADING_INTRO_CUTSCENE)
			ELIF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				CPRINTLN(DEBUG_HEIST, "Set Wade's variations for board intro cutscene.")
				SET_CUTSCENE_PED_COMPONENT_VARIATION("WADE", PED_COMP_HEAD, 0, 1)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("WADE", PED_COMP_TORSO, 1, 1)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("WADE", PED_COMP_LEG, 1, 1)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("WADE", PED_COMP_HAND, 1, 1)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("WADE", PED_COMP_HAND, 1, 1)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("WADE", PED_COMP_DECL, 1, 0)
			ENDIF
		
			IF GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_1)
				IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
					IF Request_Mission_Launch(m_iMissionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
						CPRINTLN(DEBUG_HEIST, "Docks Heist control thread secured the mission flag.")
						
						IF GET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_PICTURE) != BUILDINGSTATE_DESTROYED
							SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_PICTURE, BUILDINGSTATE_DESTROYED)
						ENDIF
						IF GET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT) != BUILDINGSTATE_DESTROYED
							SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT, BUILDINGSTATE_DESTROYED)
						ENDIF
		
						//Setup area blocks around the board.
						m_scenarioBlock = ADD_SCENARIO_BLOCKING_AREA(m_sDocksBoard.sData.vAreaBlockCenter-m_sDocksBoard.sData.vAreaBlockDimensions, m_sDocksBoard.sData.vAreaBlockCenter+m_sDocksBoard.sData.vAreaBlockDimensions)
						m_navmeshBlock = ADD_NAVMESH_BLOCKING_OBJECT(m_sDocksBoard.sData.vAreaBlockCenter, m_sDocksBoard.sData.vAreaBlockDimensions*1.5, 0.0)
						SET_PED_NON_CREATION_AREA(m_sDocksBoard.sData.vAreaBlockCenter-m_sDocksBoard.sData.vAreaBlockDimensions, m_sDocksBoard.sData.vAreaBlockCenter+m_sDocksBoard.sData.vAreaBlockDimensions)
						SET_PED_PATHS_IN_AREA(m_sDocksBoard.sData.vAreaBlockCenter-m_sDocksBoard.sData.vAreaBlockDimensions, m_sDocksBoard.sData.vAreaBlockCenter+m_sDocksBoard.sData.vAreaBlockDimensions, FALSE)
						
						//Stop the player getting a wanted level for the duration of this planning board sequence.
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
							SET_MAX_WANTED_LEVEL(0)
						ENDIF
						
						SET_BIT(m_iState, BIT_CANDIDATE_ID_SECURED)
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(m_iState, BIT_CANDIDATE_ID_SECURED)
				IF HAS_THIS_CUTSCENE_LOADED_WITH_FAILSAFE("LSDHS_MCS_3_P1_CONCAT")
					IF NOT IS_CUTSCENE_PLAYING()
						CPRINTLN(DEBUG_HEIST, "Starting Docks Heist board intro cutscene.")

						
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
							SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], FALSE, TRUE)
							REGISTER_ENTITY_FOR_CUTSCENE(g_sTriggerSceneAssets.ped[0], "WADE", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, IG_WADE)
						ENDIF
						
						REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "TREVOR", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
						DISABLE_CELLPHONE(TRUE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						CLEAR_BIT(m_iState, BIT_RENDER_BOARD)
						
						IF GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_2)
							CPRINTLN(DEBUG_HEIST, "Setting board exit dialogue for having completed FBI2 and met Franklin.")
							g_sHeistChoiceData[HEIST_CHOICE_DOCKS_BLOW_UP_BOAT].tChoiceDialogueLabels[HCDS_EXIT] = "DHP8A"
							g_sHeistChoiceData[HEIST_CHOICE_DOCKS_DEEP_SEA].tChoiceDialogueLabels[HCDS_EXIT] = "DHP8A"
						ELSE
							CPRINTLN(DEBUG_HEIST, "Setting board exit dialogue for not having completed FBI2 and not having met Franklin.")
							g_sHeistChoiceData[HEIST_CHOICE_DOCKS_BLOW_UP_BOAT].tChoiceDialogueLabels[HCDS_EXIT] = "DHP8B"
							g_sHeistChoiceData[HEIST_CHOICE_DOCKS_DEEP_SEA].tChoiceDialogueLabels[HCDS_EXIT] = "DHP8B"
						ENDIF
						
						ADD_PED_FOR_DIALOGUE(m_sDocksBoard.sPedsForConversation, 2, NULL, 	"TREVOR", 	TRUE)
						ADD_PED_FOR_DIALOGUE(m_sDocksBoard.sPedsForConversation, 3, NULL, 	"WADE", 	TRUE)	
						
						bMonitorForUnwantedLine = TRUE
						START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
						
					ELSE
						CPRINTLN(DEBUG_HEIST, "Cutscene time: ", GET_CUTSCENE_TIME())
						
						// #1966073 Prevent replay recording while a planning board cutscene is active.
						REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
						
						IF NOT IS_BIT_SET(m_iState, BIT_FADE_IN_FROM_SHITSKIP)
							IF NOT IS_SCREEN_FADED_IN()
							AND NOT IS_SCREEN_FADING_IN()
								DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
							ENDIF
							SET_BIT(m_iState, BIT_FADE_IN_FROM_SHITSKIP)
						ENDIF

						IF WAS_CUTSCENE_SKIPPED()
							SET_BIT(m_iState, BIT_CUTSCENE_SKIPPED)
						ENDIF
						
						IF IS_BIT_SET(m_iState, BIT_CUTSCENE_SKIPPED)
							IF IS_SCREEN_FADED_OUT()
								IF NOT IS_BIT_SET(m_iState, BIT_RENDER_BOARD)
									CPRINTLN(DEBUG_HEIST, "Turning on board display groups as cutscene is skipped.")
									SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_DOCKS, PBDG_0, TRUE)
									SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_DOCKS, PBDG_1, TRUE)
									SET_BIT(m_iState, BIT_RENDER_BOARD)
								ENDIF
								IF NOT IS_BIT_SET(m_iState, BIT_BOARD_VIEW_ACTIVE)
									CPRINTLN(DEBUG_HEIST, "Activating Docks Heist planning board as cutscene is skipped.")
									SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_DOCKS, PBDG_1, TRUE)
									Switch_On_Planning_Board_View()
									SET_BIT(m_iState, BIT_BOARD_VIEW_ACTIVE)
								ENDIF
								SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_PRIME_BOARD_TRANSITION, FALSE)
							ENDIF
						ENDIF
						
						IF NOT IS_BIT_SET(m_iState, BIT_REPOSITIONED_VEHICLE)
							/*
							CPRINTLN(DEBUG_HEIST, "Repositioning player vehicle.")
							VEHICLE_INDEX vehLastPlayer = GET_PLAYERS_LAST_VEHICLE()
							IF DOES_ENTITY_EXIST(vehLastPlayer)
								IF IS_VEHICLE_DRIVEABLE(vehLastPlayer)
									SET_ENTITY_COORDS(vehLastPlayer, <<-1156.0468, -1522.3109, 3.3348>>)
									SET_ENTITY_HEADING(vehLastPlayer, 36.8708)
									SET_MISSION_VEHICLE_GEN_VEHICLE(vehLastPlayer, <<-1156.0468, -1522.3109, 3.3348>>, 36.8708)
								ENDIF
							ENDIF
							*/
							SET_BIT(m_iState, BIT_REPOSITIONED_VEHICLE)
						ENDIF
						
						IF NOT IS_BIT_SET(m_iState, BIT_RENDER_BOARD)
							IF GET_CUTSCENE_TIME() >= 60671
								CPRINTLN(DEBUG_HEIST, "Turning on board display groups.")
								SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_DOCKS, PBDG_0, TRUE)
								SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_DOCKS, PBDG_1, TRUE)
								SET_BIT(m_iState, BIT_RENDER_BOARD)
							ENDIF
						ENDIF
						
						IF NOT IS_BIT_SET(m_iState, BIT_CHARACTERS_HIDDEN)
							IF GET_CUTSCENE_TIME() >= 53044
								CPRINTLN(DEBUG_HEIST, "Hiding Trevor.")
								//IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								//	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
								//ENDIF
								SET_BIT(m_iState, BIT_CHARACTERS_HIDDEN)
							ENDIF
						ENDIF
						
						IF CAN_SET_EXIT_STATE_FOR_CAMERA()
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
							ENDIF		
							
							CPRINTLN(DEBUG_HEIST, "Docks Heist board intro cutscene ready to set camera exit state.")
							IF NOT IS_BIT_SET(m_iState, BIT_BOARD_VIEW_ACTIVE)
								CPRINTLN(DEBUG_HEIST, "Activating Docks Heist planning board view behind cutscene.")
								SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_DOCKS, PBDG_1, TRUE)
								Switch_On_Planning_Board_View()
								SET_BIT(m_iState, BIT_BOARD_VIEW_ACTIVE)
							ENDIF
							SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_PRIME_BOARD_TRANSITION, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
				IF Request_Mission_Launch(m_iMissionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
					CPRINTLN(DEBUG_HEIST, "Docks Heist control thread secured the mission flag.")
					SET_BIT(m_iState, BIT_CANDIDATE_ID_SECURED)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(m_iState, BIT_CANDIDATE_ID_SECURED)
				//Repeat play, jump straight into the board.
				ADD_PED_FOR_DIALOGUE(m_sDocksBoard.sPedsForConversation, 2, NULL, 	"TREVOR", 	TRUE)
				ADD_PED_FOR_DIALOGUE(m_sDocksBoard.sPedsForConversation, 3, NULL, 	"WADE", 	TRUE)							
				
				CPRINTLN(DEBUG_HEIST, "Activating Docks Heist planning board for repeat play.")
				Switch_On_Planning_Board_View()
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_PRIME_BOARD_TRANSITION, FALSE)
			ENDIF
		ENDIF
	ELSE
		IF HAS_THIS_CUTSCENE_LOADED("LSDHS_MCS_3_P1_CONCAT")
			CPRINTLN(DEBUG_HEIST, "Cleaning up Docks Heist planning board intro cutscene.")
			REMOVE_CUTSCENE()
		ENDIF
		IF IS_BIT_SET(m_iState, BIT_LOADING_INTRO_CUTSCENE)
			CLEAR_BIT(m_iState, BIT_LOADING_INTRO_CUTSCENE)
		ENDIF
	ENDIF
ENDPROC

FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR vRot)
	RETURN <<-SIN(vRot.z) * COS(vRot.x), COS(vRot.z) * COS(vRot.x), SIN(vRot.x)>>
ENDFUNC

PROC Manage_Docks_Heist_Board_Exit_Cutscene()
	IF bMonitorForUnwantedLine
		IF GET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_DOCKS) = HEIST_CHOICE_DOCKS_BLOW_UP_BOAT
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				TEXT_LABEL_23 tCurrentLabel 
				tCurrentLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
				IF ARE_STRINGS_EQUAL(tCurrentLabel, "DHP8B_1")		
					KILL_ANY_CONVERSATION()
					bMonitorForUnwantedLine = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_LOAD_BOARD_EXIT_CUTSCENE)
		IF NOT IS_BIT_SET(m_iState, BIT_LOADING_EXIT_CUTSCENE)
			REQUEST_CUTSCENE("LSDHS_MCS_3_P2")
			REQUEST_AMBIENT_AUDIO_BANK("TIME_LAPSE")
			REQUEST_MODEL(IG_FLOYD)
			SET_BIT(m_iState, BIT_LOADING_EXIT_CUTSCENE)
		ENDIF
	ENDIF
	
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_DO_BOARD_EXIT_CUTSCENE)
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_LOAD_BOARD_EXIT_CUTSCENE)
			
			IF HAS_CUTSCENE_LOADED()
			AND (IS_BIT_SET(g_sHeistChoiceData[HEIST_CHOICE_DOCKS_DEEP_SEA].iBitsetChoiceDialoguePlayed, ENUM_TO_INT(HCDS_EXIT))
			OR IS_BIT_SET(g_sHeistChoiceData[HEIST_CHOICE_DOCKS_BLOW_UP_BOAT].iBitsetChoiceDialoguePlayed, ENUM_TO_INT(HCDS_EXIT)))	
			AND REQUEST_AMBIENT_AUDIO_BANK("TIME_LAPSE")
			AND HAS_MODEL_LOADED(IG_FLOYD)
			AND m_sDocksBoard.iNoConvQueued = 0
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "TREVOR", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
				ENDIF
				
				//Create, damage and register Floyd.
				CPRINTLN(DEBUG_HEIST, "Smashing up Floyd's face.")
				CREATE_NPC_PED_ON_FOOT(m_pedFloyd, CHAR_FLOYD, <<-1146.4182, -1518.0624, 10.2716>>, 0.0, TRUE)
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.554, 0.421, 0.010, 0.831, 1.000,  1,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.554, 0.542, 0.010, 0.861, 0.900,  1,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.554, 0.542, 0.010, 0.861, 1.000,  5,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.554, 0.542, 0.010, 0.861, 1.000,  2,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.554, 0.542, 0.010, 0.861, 1.000,  1,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.557, 0.576, 0.000, 0.866, 1.000,  1,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.557, 0.576, 0.000, 0.866, 0.900,  4,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.557, 0.576, 0.000, 0.866, 1.000,  1,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.557, 0.576, 0.000, 0.866, 1.000,  1,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.557, 0.576, 0.000, 0.866, 0.900,  2,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.557, 0.576, 0.000, 0.866, 1.000,  1,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.557, 0.576, 0.000, 0.866, 1.000,  5,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.557, 0.576, 0.000, 0.866, 1.000,  3,   TRUE, "bruise")
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.577, 0.536, 0.000, 0.866, 1.000,  3,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.577, 0.536, 0.000, 0.866, 1.000,  3,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.577, 0.536, 0.000, 0.866, 0.900,  4,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.577, 0.536, 0.000, 0.866, 0.900,  4,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.577, 0.536, 0.000, 0.866, 1.000,  1,   TRUE, "bruise")
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.411, 0.485, 0.000, 1.000, 1.000,  0,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.411, 0.485, 0.000, 1.000, 1.000,  0,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.411, 0.485, 0.000, 1.000, 1.000,  2,   TRUE, "bruise") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.411, 0.485, 0.000, 1.000, 1.000,  5,   TRUE, "bruise") 
				
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.414, 0.542, 0.030, 0.988, 1.000,  5,   TRUE, "scar") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.414, 0.542, 0.030, 0.988, 1.000, 11,   TRUE, "scar") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.504, 0.752, 0.030, 0.988, 1.000, 10,   TRUE, "scar") 
				APPLY_PED_DAMAGE_DECAL(m_pedFloyd, PDZ_HEAD, 0.504, 0.752, 0.030, 0.988, 1.000,  3,   TRUE, "scar")
				REGISTER_ENTITY_FOR_CUTSCENE(m_pedFloyd, "Floyd", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, CS_FLOYD, CEO_CLONE_DAMAGE_TO_CS_MODEL|CEO_PRESERVE_FACE_BLOOD_DAMAGE)
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_FLOYD)
				
				PAUSE_FLOW_HELP_QUEUE(TRUE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				DISABLE_CELLPHONE(TRUE)
				START_CUTSCENE()
				
				bMonitorForUnwantedLine = FALSE
				
				IF IS_REPEAT_PLAY_ACTIVE()
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
				ENDIF
				
				IF IS_BIT_SET(m_iState, BIT_CHARACTERS_HIDDEN)
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
					ENDIF
					CLEAR_BIT(m_iState, BIT_CHARACTERS_HIDDEN)
				ENDIF
				Set_Mission_Flow_Flag_State(FLOWFLAG_HEIST_DOCKS_LOAD_BOARD_EXIT_CUTSCENE, FALSE)
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
				IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_NONE, "Skip cutscene")
					IF GET_CUTSCENE_TIME() >= 1500
						STOP_CUTSCENE()
					ENDIF
				ENDIF
			#ENDIF
			
			IF IS_CUTSCENE_PLAYING()
			
				// #1966073 Prevent replay recording while a planning board cutscene is active.
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
				IF NOT bPrestreamingExterior
					IF GET_CUTSCENE_TIME() >= 35000
						houseVolume = STREAMVOL_CREATE_FRUSTUM(<<-1193, -1525, 4.4>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<9.4, 0.0, -78.8>>), 1000, FLAG_MAPDATA)
						bPrestreamingExterior = TRUE
					ENDIF
				ENDIF
			
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					IF NOT IS_REPEAT_PLAY_ACTIVE()
						//SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_DEFAULT, FALSE)
						RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
						REQUEST_ANIM_DICT("SWITCH@TREVOR@WATCHING_TV")
						REQUEST_MODEL(PROP_CS_REMOTE_01)
						REGISTER_SCRIPT_WITH_AUDIO()
						REQUEST_AMBIENT_AUDIO_BANK("SAFEHOUSE_FRANKLIN_SOFA")
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
						SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						DO_TIMELAPSE(SP_HEIST_DOCKS_1, sToD, TRUE, TRUE, FALSE, TRUE)
						
						SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL, 0)
						
						SET_BIT(m_iState, BIT_DOING_TIMELAPSE)
						CPRINTLN(DEBUG_HEIST, "Flagging to start timelapse.")
					ELSE
						SET_BIT(m_iState, BIT_CLEANUP)
					ENDIF
				ENDIF
			ENDIF

			//Timelapse cutscene running.
			IF IS_BIT_SET(m_iState, BIT_DOING_TIMELAPSE)
				IF NOT bPrestreamingInterior
					IF STREAMVOL_IS_VALID(houseVolume)
						STREAMVOL_DELETE(houseVolume)
					ENDIF	
					NEW_LOAD_SCENE_STOP()
					//houseVolume = STREAMVOL_CREATE_FRUSTUM(<<-1157.9, -1519.7, 10.7>>, CONVERT_ROTATION_TO_DIRECTION_VECTOR(<<-2.8, 0.0, 128.3>>), 100, FLAG_MAPDATA)
					NEW_LOAD_SCENE_START_SPHERE(<<-1159.2729, -1522.5040, 9.6340>>, 15.0)
					bPrestreamingInterior = TRUE
				ENDIF
				
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				IF NOT IS_BIT_SET(m_iState, BIT_CREATE_SYNC_SCENE)
				
					IF DO_TIMELAPSE(SP_HEIST_DOCKS_1, sToD, TRUE, FALSE, FALSE, TRUE)
						//Create lead-in scene during timelapse.
						IF HAS_ANIM_DICT_LOADED("SWITCH@TREVOR@WATCHING_TV")
						AND HAS_MODEL_LOADED(PROP_CS_REMOTE_01)
							m_iLeadoutScene = CREATE_SYNCHRONIZED_SCENE(<<-1159.2729, -1522.5040, 9.6340>>, << -0.000, 0.000, -147.0000 >>)
							m_oRemote = CREATE_OBJECT(PROP_CS_REMOTE_01, <<-1159.2729, -1522.5040, 9.6340>>)
							SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_REMOTE_01)
							ATTACH_ENTITY_TO_ENTITY(m_oRemote, PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
			
							SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(m_iLeadoutScene, FALSE)
							SET_SYNCHRONIZED_SCENE_LOOPED(m_iLeadoutScene, TRUE)
							TASK_SYNCHRONIZED_SCENE (PLAYER_PED_ID(), m_iLeadoutScene, "SWITCH@TREVOR@WATCHING_TV", "LOOP", INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
								CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_EYES)
							ENDIF
						
							SET_BIT(m_iState, BIT_CREATE_SYNC_SCENE)
						ENDIF
					ENDIF
					
				ELIF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID()) 
					CPRINTLN(DEBUG_HEIST, "Starting to play Trevor's leadout.")
					
					INTERIOR_INSTANCE_INDEX interiorFloydFlat = GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-1160.3545, -1521.7078, 9.6327>>, "V_TREVORS")
					RETAIN_ENTITY_IN_INTERIOR(PLAYER_PED_ID(), interiorFloydFlat)
					RETAIN_ENTITY_IN_INTERIOR(m_oRemote, interiorFloydFlat)
					
					SET_TODS_CUTSCENE_RUNNING(sToD, FALSE)
					
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(m_iLeadoutScene, FALSE)
					SET_SYNCHRONIZED_SCENE_LOOPED(m_iLeadoutScene, FALSE)
					SET_SYNCHRONIZED_SCENE_PHASE(m_iLeadoutScene, 0.0)
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), m_iLeadoutScene, "SWITCH@TREVOR@WATCHING_TV", "EXIT", INSTANT_BLEND_IN, WALK_BLEND_OUT)
					
					m_camLeadoutScene = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
					PLAY_SYNCHRONIZED_CAM_ANIM(m_camLeadoutScene, m_iLeadoutScene, "EXIT_CAM", "SWITCH@TREVOR@WATCHING_TV")
					RENDER_SCRIPT_CAMS(TRUE, FALSE)			
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(),TRUE)
					
					ADD_GLOBAL_COMMUNICATION_DELAY(15000)

					CLEAR_BIT(m_iState, BIT_DOING_TIMELAPSE)
					SET_BIT(m_iState, BIT_DOING_LEADOUT)
				ENDIF
			
			//Leadout animations running.
			ELIF IS_BIT_SET(m_iState, BIT_DOING_LEADOUT)
			
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CHARACTER_WHEEL)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_PC)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DIVE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				
				NEW_LOAD_SCENE_STOP()
				IF STREAMVOL_IS_VALID(houseVolume)
					STREAMVOL_DELETE(houseVolume)
				ENDIF	

				IF IS_SYNCHRONIZED_SCENE_RUNNING(m_iLeadoutScene)
					FLOAT fStartPhase
					FLOAT fTemp
					
					IF DOES_ENTITY_EXIST(m_oRemote)
						IF IS_ENTITY_ATTACHED(m_oRemote)
							IF NOT FIND_ANIM_EVENT_PHASE("SWITCH@TREVOR@WATCHING_TV", "EXIT", "OBJECTRELEASE", fStartPhase, fTemp)
								fStartPhase = 0.4206
							ENDIF
							IF GET_SYNCHRONIZED_SCENE_PHASE(m_iLeadoutScene) >= fStartPhase
								CPRINTLN(DEBUG_HEIST, "Detaching remote.")
								DETACH_ENTITY(m_oRemote)
								SET_OBJECT_AS_NO_LONGER_NEEDED(m_oRemote)
							ENDIF
						ENDIF
					ENDIF
					IF NOT FIND_ANIM_EVENT_PHASE("SWITCH@TREVOR@WATCHING_TV", "EXIT", "WALKINTERRUPTABLE", fStartPhase, fTemp)
						fStartPhase = 0.9
					ENDIF
					IF GET_SYNCHRONIZED_SCENE_PHASE(m_iLeadoutScene) >= fStartPhase
						CPRINTLN(DEBUG_HEIST, "Trevor's leadout finished playing.")
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, TRUE)
						STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
						REMOVE_ANIM_DICT("SWITCH@TREVOR@WATCHING_TV")
						RELEASE_AMBIENT_AUDIO_BANK()
						UNREGISTER_SCRIPT_WITH_AUDIO()
						CLEAR_BIT(m_iState, BIT_DOING_LEADOUT)
						SET_BIT(m_iState, BIT_CLEANUP)
					ENDIF
				ENDIF
			
			//Cleanup
			ELIF IS_BIT_SET(m_iState, BIT_CLEANUP)
				IF IS_REPEAT_PLAY_ACTIVE() // fade out for repeat play
					DO_SCREEN_FADE_OUT(0)
				ELSE
					g_bMissionStatSystemBlocker = FALSE
					m_iSelectorBlockTimer = GET_GAME_TIMER() + 5000
					g_sAutosaveData.bFlushAutosaves = FALSE
					MAKE_AUTOSAVE_REQUEST()
				ENDIF
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				DISABLE_CELLPHONE(FALSE)
				ADD_GLOBAL_COMMUNICATION_DELAY(20000)
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_DO_BOARD_EXIT_CUTSCENE, FALSE)
				ResetLastKnownPedInfo(g_savedGlobals.sPlayerData.sInfo, SP_HEIST_DOCKS_1)
				
				Set_Current_Event_For_FamilyMember(FM_TREVOR_1_FLOYD, FE_T1_FLOYD_with_wade_post_docks1)
				Set_Current_Event_For_FamilyMember(FM_TREVOR_1_WADE, FE_T1_FLOYD_with_wade_post_docks1)
				
				UpdateLastKnownPedInfoPostMission(g_savedGlobals.sPlayerData.sInfo, SP_HEIST_DOCKS_1)
				
				IF STREAMVOL_IS_VALID(houseVolume)
					STREAMVOL_DELETE(houseVolume)
				ENDIF		
				NEW_LOAD_SCENE_STOP()
				
				SET_CONTROL_SHAKE_SUPPRESSED_ID(PLAYER_CONTROL, -1)
				
				Mission_Over(m_iMissionCandidateID)
				CLEAR_BIT(m_iState, BIT_CANDIDATE_ID_SECURED)
				CPRINTLN(DEBUG_HEIST, "Docks Heist controller released the mission flag.")
				
				//Remove area blocks and restore wanted levels.
				REMOVE_SCENARIO_BLOCKING_AREA(m_scenarioBlock)
				IF m_navmeshBlock != -1
					IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(m_navmeshBlock)
						REMOVE_NAVMESH_BLOCKING_OBJECT(m_navmeshBlock)
					ENDIF
				ENDIF
				CLEAR_PED_NON_CREATION_AREA()
				SET_PED_PATHS_IN_AREA(m_sDocksBoard.sData.vAreaBlockCenter-m_sDocksBoard.sData.vAreaBlockDimensions, m_sDocksBoard.sData.vAreaBlockCenter+m_sDocksBoard.sData.vAreaBlockDimensions, TRUE)
				SET_MAX_WANTED_LEVEL(5)
				
				//B* 1766600: Clear Hats and Glasses blocking bits
				CLEAR_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(CHAR_TREVOR)], NUM_PED_COMPONENTS + ENUM_TO_INT(ANCHOR_HEAD))
				CLEAR_BIT(g_iFlowBitsetBlockPlayerCutsceneVariations[ENUM_TO_INT(CHAR_TREVOR)], NUM_PED_COMPONENTS + ENUM_TO_INT(ANCHOR_EYES))
			ENDIF
		ENDIF
	ELIF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_LOAD_BOARD_EXIT_CUTSCENE)
		//Clean up the cutscene if neither flow flag is set.
		IF HAS_THIS_CUTSCENE_LOADED("LSDHS_MCS_3_P2")
			REMOVE_CUTSCENE()
		ENDIF
	ENDIF
	
ENDPROC


//══════════════════════════════╡ Control Loop ╞═══════════════════════════════

SCRIPT
	CPRINTLN(DEBUG_HEIST, "Docks heist controller started.")
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU | FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		Terminate_Heist_Controller()
	ENDIF
	
	//Register script to be saved as running.
	Register_Script_To_Relaunch_List(LAUNCH_BIT_HEIST_CTRL_DOCKS)
	
	//Setup the planning board for this heist.
	Setup_Docks_Heist_Board_Data(m_sDocksBoard.sData, IMAX(g_sHeistChoiceData[HEIST_CHOICE_DOCKS_BLOW_UP_BOAT].iCrewSize, g_sHeistChoiceData[HEIST_CHOICE_DOCKS_DEEP_SEA].iCrewSize))
	Setup_Planning_Board(m_sDocksBoard, HEIST_DOCKS)

	WHILE NOT Get_Mission_Flow_Flag_State(FLOWFLAG_HEIST_FINISHED_DOCKS)
		
		IF NOT g_flowUnsaved.bUpdatingGameflow
			#IF IS_DEBUG_BUILD
				Debug_Run_Planning_Board_Widget(m_sDocksBoard)
			#ENDIF

			//Stream the board in and out when necessary.
			IF NOT IS_MISSION_AVAILABLE(SP_HEIST_DOCKS_1)
			AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
				Manage_Board_Streaming(m_sDocksBoard, BOARD_LOAD_DIST, BOARD_UNLOAD_DIST)
			ELIF IS_BIT_SET(m_sDocksBoard.iState, BIT_BOARD_LOADED)
				CPRINTLN(DEBUG_HEIST, "Cleaning up docks heist planning board as the setup mission is available.")
				Cleanup_Planning_Board(m_sDocksBoard)
			ENDIF
			
			//Check for the flow auto toggling the board.
			Manage_Board_View_Auto_Toggling(m_sDocksBoard)
			
			//Load and run the planning board intro cutscene when requested.
			//Loading happens during mission Docks Setup.
			Manage_Docks_Heist_Board_Intro_Cutscene()
			
			//Load and run the planning board exit cutscene when requested.
			Manage_Docks_Heist_Board_Exit_Cutscene()

			IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
			OR IS_BIT_SET(m_sDocksBoard.iState, BIT_BOARD_VIEW_INITIALISED)
				//Allow the player to interact with the board if it is created.
				Manage_Board_Viewing(m_sDocksBoard)
				
				//Check for flow requesting the board switches mode.
				Manage_Board_Mode_Switching(m_sDocksBoard)
				
				//Ensure that changes to boards display groups are kept up-to-date.
				Update_Board_Display_Groups(m_sDocksBoard)
			ENDIF
		ENDIF
		
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("docks_heistA")) > 0)
		OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("docks_heistB")) > 0)
			HEIST_TIMER_UPDATE()
		ENDIF
		
		IF GET_GAME_TIMER() < m_iSelectorBlockTimer
			CDEBUG3LN(DEBUG_HEIST, "Docks heist controller disabling selector this frame.")
			DISABLE_SELECTOR_THIS_FRAME()
		ENDIF
		
		WAIT(0)
		
	ENDWHILE
	
	Remove_Script_From_Relaunch_List(LAUNCH_BIT_HEIST_CTRL_DOCKS)
	Terminate_Heist_Controller()
	
ENDSCRIPT
