//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 22/07/10			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						  Finale Heist Control Script							│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "heist_board_data_finale.sch"
USING "heist_planning_board.sch"
USING "heist_ctrl_generic.sch"
USING "flow_public_core_override.sch"
USING "script_misc.sch"
USING "cutscene_public.sch"
USING "TimeLapse.sch"
USING "selector_public.sch"
USING "clearmissionarea.sch"

//TEMP FOR FUDGE.
USING "stripclub_public.sch"

//═══════════════════════════════╡ Constants ╞═════════════════════════════════

CONST_FLOAT 	BOARD_LOAD_DIST		10.0
CONST_FLOAT		BOARD_UNLOAD_DIST	18.0

//═══════════════════════════════╡ Variables ╞═════════════════════════════════

//General state tracking.
CONST_INT	BIT_PLAYER_ARRIVED_IN_VEHICLE	0
CONST_INT	BIT_PLAYER_VEHICLE_MOVED		1
CONST_INT	BIT_BOARD_VIEW_ACTIVATED		2
CONST_INT	BIT_CANDIDATE_ID_SECURED		3
CONST_INT	BIT_SWITCH_TO_MICHAEL			4
CONST_INT	BIT_CUT_ENTITIES_HIDDEN			5
CONST_INT	BIT_CLEARED_WADE				6

STRING	m_strExitCutsceneName		= ""
INT		m_iState					= 0
INT 	m_iCutsceneStartTime		= -1
INT 	m_iDelayTimer				= -1
INT 	m_iMissionCandidateID	 	= NO_CANDIDATE_ID
BOOL	bStartedExitCutAsMichael

//Cutscene load requests.
INT m_iIntroCutsceneRequestID = NULL_OFFMISSION_CUTSCENE_REQUEST
INT m_iHeistChoiceLastFrame = -1


ENUM CREATE_MICHAEL_CAR_STATE 
	CREATE_MICHAEL_CAR_STATE_NOT_STARTED = 0,
	CREATE_MICHAEL_CAR_STATE_WAITING,
	CREATE_MICHAEL_CAR_STATE_DONE
ENDENUM

VEHICLE_SETUP_STRUCT 	m_sPlayerVehicle
VEHICLE_INDEX			m_vehPlayer
VEHICLE_INDEX 			michaelCar
PED_INDEX				m_pedMichael
PED_INDEX 				m_pedFranklin
PED_INDEX 				m_pedTrevor
PED_INDEX 				m_pedLester
OBJECT_INDEX			m_objLesterStick
SELECTOR_PED_STRUCT 	sSelector
SCENARIO_BLOCKING_INDEX m_scenarioBlock
INT						m_navmeshBlock = -1
CREATE_MICHAEL_CAR_STATE createMichaelCarState

//═══════════════════════════════╡ Structures ╞════════════════════════════════

PlanningBoard m_sFinaleBoard

//══════════════════════════════╡ Procedures ╞═════════════════════════════════

PROC Terminate_Heist_Controller()
	
	//Ensure the planning board has been cleaned up before the control script is terminated.
	WHILE NOT Cleanup_Planning_Board(m_sFinaleBoard)
		WAIT(0)
	ENDWHILE
	
	//Cleanup flow flag states.
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PRIME_BOARD_TRANSITION, FALSE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_LOAD_BOARD_EXIT_CUTSCENE, FALSE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_DO_BOARD_EXIT_CUTSCENE, FALSE)
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, HEIST_FINALE, FALSE)
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_TOGGLE_BOARD, HEIST_FINALE, FALSE)
	
	//Remove area blocks and restore wanted levels.
	REMOVE_SCENARIO_BLOCKING_AREA(m_scenarioBlock)
	IF m_navmeshBlock != -1
		IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(m_navmeshBlock)
			REMOVE_NAVMESH_BLOCKING_OBJECT(m_navmeshBlock)
		ENDIF
	ENDIF
	CLEAR_PED_NON_CREATION_AREA()
	SET_PED_PATHS_IN_AREA(m_sFinaleBoard.sData.vAreaBlockCenter-m_sFinaleBoard.sData.vAreaBlockDimensions, m_sFinaleBoard.sData.vAreaBlockCenter+m_sFinaleBoard.sData.vAreaBlockDimensions, TRUE)
	SET_MAX_WANTED_LEVEL(5)
	
	//Clean up any cutscenes we have loaded.
	IF m_iIntroCutsceneRequestID != NULL_OFFMISSION_CUTSCENE_REQUEST
		END_OFFMISSION_CUTSCENE_REQUEST(m_iIntroCutsceneRequestID)
	ENDIF
	
	CPRINTLN(DEBUG_HEIST, "Unloading planning board owned cutscene.")
	REMOVE_CUTSCENE()
	
	//Release the mission candidate ID if we have one.
	IF m_iMissionCandidateID != NO_CANDIDATE_ID
		Mission_Over(m_iMissionCandidateID)
	ENDIF
	
	CPRINTLN(DEBUG_HEIST, "Finale heist controller terminated.")
	TERMINATE_THIS_THREAD()
	
ENDPROC

PROC Store_Last_Player_Vehicle()

	enumCharacterList ePed = GET_PLAYER_PED_ENUM(PLAYER_PED_ID())
	STORE_VEH_DATA_FROM_PED(PLAYER_PED_ID(),g_sPlayerLastVeh[ePed],g_vPlayerLastVehCoord[ePed],g_fPlayerLastVehHead[ePed],g_ePlayerLastVehState[ePed],g_ePlayerLastVehGen[ePed])
										
ENDPROC


PROC Recreate_Player_Vehicle_In_Safe_Location(VECTOR paramPosition, FLOAT paramHeading)
	IF IS_BIT_SET(m_iState, BIT_PLAYER_ARRIVED_IN_VEHICLE)
		IF NOT IS_BIT_SET(m_iState, BIT_PLAYER_VEHICLE_MOVED)
			REQUEST_MODEL(m_sPlayerVehicle.eModel)
			IF HAS_MODEL_LOADED(m_sPlayerVehicle.eModel)
				CLEAR_AREA_OF_VEHICLES(paramPosition, 50.0, TRUE)
				m_vehPlayer = CREATE_VEHICLE(m_sPlayerVehicle.eModel, paramPosition, paramHeading)
				SET_VEHICLE_SETUP(m_vehPlayer, m_sPlayerVehicle)
				SET_MODEL_AS_NO_LONGER_NEEDED(m_sPlayerVehicle.eModel)
			ENDIF
			SET_BIT(m_iState, BIT_PLAYER_VEHICLE_MOVED)
		ENDIF
	ENDIF
ENDPROC


PROC Switch_On_Planning_Board_View()
	//Seamless transition into board view. Toggle board on and disable board exit.
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, HEIST_FINALE, TRUE)
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_TOGGLE_BOARD, HEIST_FINALE, TRUE)
	Manage_Board_View_Auto_Toggling(m_sFinaleBoard)
	Manage_Board_Viewing(m_sFinaleBoard)
ENDPROC


PROC Manage_Finale_Heist_Board_Intro_Cutscene()

	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PRIME_BOARD_TRANSITION)	
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			IF NOT IS_BIT_SET(m_iState, BIT_CANDIDATE_ID_SECURED)
				IF GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_2_INTRO)
					IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
						IF Request_Mission_Launch(m_iMissionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
							CPRINTLN(DEBUG_HEIST, "Finale Heist control thread secured the mission flag.")
							SET_BIT(m_iState, BIT_CANDIDATE_ID_SECURED)
							
							CLEAR_AUTOSAVE_REQUESTS()
							
							//Setup area blocks around the board.
							m_scenarioBlock = ADD_SCENARIO_BLOCKING_AREA(m_sFinaleBoard.sData.vAreaBlockCenter-m_sFinaleBoard.sData.vAreaBlockDimensions, m_sFinaleBoard.sData.vAreaBlockCenter+m_sFinaleBoard.sData.vAreaBlockDimensions)
							m_navmeshBlock = ADD_NAVMESH_BLOCKING_OBJECT(m_sFinaleBoard.sData.vAreaBlockCenter, m_sFinaleBoard.sData.vAreaBlockDimensions*1.5, 0.0)
							SET_PED_NON_CREATION_AREA(m_sFinaleBoard.sData.vAreaBlockCenter-m_sFinaleBoard.sData.vAreaBlockDimensions, m_sFinaleBoard.sData.vAreaBlockCenter+m_sFinaleBoard.sData.vAreaBlockDimensions)
							SET_PED_PATHS_IN_AREA(m_sFinaleBoard.sData.vAreaBlockCenter-m_sFinaleBoard.sData.vAreaBlockDimensions, m_sFinaleBoard.sData.vAreaBlockCenter+m_sFinaleBoard.sData.vAreaBlockDimensions, FALSE)
							
							//Stop the player getting a wanted level for the duration of this planning board sequence.
							IF IS_PLAYER_PLAYING(PLAYER_ID())
								CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
								SET_MAX_WANTED_LEVEL(0)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF IS_BIT_SET(m_iState, BIT_CANDIDATE_ID_SECURED)
				IF HAS_THIS_CUTSCENE_LOADED("BS_2A_2B_INT")
					IF NOT IS_CUTSCENE_PLAYING()
						CPRINTLN(DEBUG_HEIST, "Starting Finale Heist board intro cutscene.")
						Store_Last_Player_Vehicle()
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
							m_pedLester = g_sTriggerSceneAssets.ped[0]
														
							SET_ENTITY_AS_MISSION_ENTITY(m_pedLester, FALSE, TRUE)
							//Delete ped
							DELETE_PED(m_pedLester)
							REGISTER_ENTITY_FOR_CUTSCENE(m_pedLester, 	"LESTER", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_LESTERCREST)
							CPRINTLN(DEBUG_HEIST, "Grabbed Lester from trigger scene.")
						ELSE
							REGISTER_ENTITY_FOR_CUTSCENE(m_pedLester, 	"LESTER", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_LESTERCREST)
							CPRINTLN(DEBUG_HEIST, "Created new Lester for cutscene.")
						ENDIF
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
							m_objLesterStick = g_sTriggerSceneAssets.object[0]
							SET_ENTITY_AS_MISSION_ENTITY(m_objLesterStick, FALSE, TRUE)
							IF IS_ENTITY_ATTACHED_TO_ANY_PED(m_objLesterStick)
								DETACH_ENTITY(m_objLesterStick)
							ENDIF
							REGISTER_ENTITY_FOR_CUTSCENE(m_objLesterStick, 	"WALKINGSTICK_LESTER", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, IG_LESTERCREST)
						ENDIF
						SWITCH GET_CURRENT_PLAYER_PED_ENUM()
							CASE CHAR_MICHAEL
								CPRINTLN(DEBUG_HEIST, "Player is Michael.")
								m_pedMichael = PLAYER_PED_ID()
								REGISTER_ENTITY_FOR_CUTSCENE(m_pedMichael, 		"MICHAEL", 	CU_ANIMATE_EXISTING_SCRIPT_ENTITY, 			PLAYER_ZERO)
								REGISTER_ENTITY_FOR_CUTSCENE(m_pedFranklin, 	"FRANKLIN", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, 	PLAYER_ONE)
								REGISTER_ENTITY_FOR_CUTSCENE(m_pedTrevor, 		"TREVOR",	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, 	PLAYER_TWO)
								
							BREAK
							CASE CHAR_FRANKLIN
								CPRINTLN(DEBUG_HEIST, "Player is Franklin.")
								m_pedFranklin = PLAYER_PED_ID()
								REGISTER_ENTITY_FOR_CUTSCENE(m_pedMichael, 		"MICHAEL", 	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,	PLAYER_ZERO)
								REGISTER_ENTITY_FOR_CUTSCENE(m_pedFranklin, 	"FRANKLIN", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, 			PLAYER_ONE)
								REGISTER_ENTITY_FOR_CUTSCENE(m_pedTrevor, 		"TREVOR", 	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,	PLAYER_TWO)
								SET_BIT(m_iState, BIT_SWITCH_TO_MICHAEL)
							BREAK
							CASE CHAR_TREVOR
								CPRINTLN(DEBUG_HEIST, "Player is Trevor.")
								m_pedTrevor = PLAYER_PED_ID()
								REGISTER_ENTITY_FOR_CUTSCENE(m_pedMichael, 		"MICHAEL", 	CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,	PLAYER_ZERO)
								REGISTER_ENTITY_FOR_CUTSCENE(m_pedFranklin, 	"FRANKLIN", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,	PLAYER_ONE)
								REGISTER_ENTITY_FOR_CUTSCENE(m_pedTrevor, 		"TREVOR", 	CU_ANIMATE_EXISTING_SCRIPT_ENTITY, 			PLAYER_TWO)
								SET_BIT(m_iState, BIT_SWITCH_TO_MICHAEL)
							BREAK
						ENDSWITCH
						REQUEST_MODEL(PLAYER_ZERO) //Ensure Micahel model is in memory so we can switch player to him.
						DISABLE_CELLPHONE(TRUE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_FINALE_2_INTRO)
												
						//Hide the extra office chair.
						CREATE_MODEL_HIDE(<<95.1900, -1293.4994, 28.2672>>, 1.50, V_CLUB_OFFICECHAIR, FALSE)
						
						ADD_PED_FOR_DIALOGUE(m_sFinaleBoard.sPedsForConversation, 0, NULL, 	"MICHAEL", 	TRUE)
						ADD_PED_FOR_DIALOGUE(m_sFinaleBoard.sPedsForConversation, 1, NULL, 	"FRANKLIN", TRUE)
						ADD_PED_FOR_DIALOGUE(m_sFinaleBoard.sPedsForConversation, 2, NULL, 	"TREVOR", 	TRUE)
						ADD_PED_FOR_DIALOGUE(m_sFinaleBoard.sPedsForConversation, 3, NULL, 	"LESTER", 	TRUE)
						
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
							bStartedExitCutAsMichael = TRUE
						ELSE
							bStartedExitCutAsMichael = FALSE
						ENDIF
						
						START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
					ELSE
					
						// #1966073 Prevent replay recording while a planning board cutscene is active.
						REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
					
						IF NOT IS_BIT_SET(m_iState, BIT_CLEARED_WADE)
							CLEAR_AREA(<<124.0681, -1294.7621, 28.2695>>, 1.5, FALSE)
							SET_BIT(m_iState, BIT_CLEARED_WADE)
						ENDIF
					
						SWITCH createMichaelCarState
							CASE CREATE_MICHAEL_CAR_STATE_NOT_STARTED
								DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_FRANKLIN)
								DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_TREVOR)
								
								IF NOT IS_ENTITY_DEAD(m_vehPlayer)
									IF IS_VEHICLE_A_PLAYER_PERSONAL_VEHICLE(m_vehPlayer, CHAR_MICHAEL)
										createMichaelCarState = CREATE_MICHAEL_CAR_STATE_DONE
									ENDIF
								ELSE
									IF DOES_ENTITY_EXIST(m_vehPlayer)
										DELETE_VEHICLE(m_vehPlayer)
									ENDIF
								ENDIF
								
								IF createMichaelCarState <> CREATE_MICHAEL_CAR_STATE_DONE
									REQUEST_MODEL(TAILGATER)
									createMichaelCarState = CREATE_MICHAEL_CAR_STATE_WAITING
								ENDIF
							BREAK
							
							CASE CREATE_MICHAEL_CAR_STATE_WAITING
								IF HAS_MODEL_LOADED(TAILGATER)
									DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_MICHAEL)
									RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<133.95317, -1311.40369, 28.08922>>, <<127.84701, -1315.54187, 32.11665>>, 3.3, <<124.97, -1321.87, 28.00>>, 37.74, GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR(), TRUE, FALSE, TRUE)
									CREATE_PLAYER_VEHICLE(michaelCar, CHAR_MICHAEL, <<130.56, -1313.66, 28.74>>, 124.66, FALSE)
									SET_VEHICLE_ON_GROUND_PROPERLY(michaelCar)
									SET_MODEL_AS_NO_LONGER_NEEDED(TAILGATER)
									SET_MISSION_VEHICLE_GEN_VEHICLE(michaelCar, <<0,0,0>>, 0)
									createMichaelCarState = CREATE_MICHAEL_CAR_STATE_DONE
								ENDIF
							BREAK
						ENDSWITCH
					
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MICHAEL", PLAYER_ZERO)
							CPRINTLN(DEBUG_HEIST, "Setting exit state of Michael.")
							IF NOT IS_ENTITY_DEAD(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MICHAEL"))
							   	m_pedMichael = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("MICHAEL"))
							   	SET_PED_CONFIG_FLAG(m_pedMichael, PCF_DisableExplosionReactions, TRUE)
								SET_PED_CONFIG_FLAG(m_pedMichael, PCF_RunFromFiresAndExplosions, FALSE)
								SET_PED_CONFIG_FLAG(m_pedMichael, PCF_ListensToSoundEvents, FALSE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_pedMichael, TRUE)
								SET_ENTITY_COORDS(m_pedMichael, << 96.4600, -1291.4402, 28.2688 >>)
							   	SET_ENTITY_HEADING(m_pedMichael, 299.3742)
							  	ADD_PED_FOR_DIALOGUE(m_sFinaleBoard.sPedsForConversation, 0, m_pedMichael, "MICHAEL", TRUE)
								
								IF IS_BIT_SET(m_iState, BIT_SWITCH_TO_MICHAEL)
									CPRINTLN(DEBUG_HEIST, "Switching to Michael behind cutscene.")
									sSelector.pedID[SELECTOR_PED_MICHAEL] = m_pedMichael
									MAKE_SELECTOR_PED_SELECTION(sSelector, SELECTOR_PED_MICHAEL)
									TAKE_CONTROL_OF_SELECTOR_PED(sSelector, FALSE, FALSE)
								ENDIF
							ENDIF
						ENDIF
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("FRANKLIN", PLAYER_ONE)
							CPRINTLN(DEBUG_HEIST, "Setting exit state of Franklin.")
							IF NOT IS_ENTITY_DEAD(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("FRANKLIN"))
								m_pedFranklin = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("FRANKLIN"))
							  	SET_PED_CONFIG_FLAG(m_pedFranklin, PCF_DisableExplosionReactions, TRUE)
								SET_PED_CONFIG_FLAG(m_pedFranklin, PCF_RunFromFiresAndExplosions, FALSE)
								SET_PED_CONFIG_FLAG(m_pedFranklin, PCF_ListensToSoundEvents, FALSE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_pedFranklin, TRUE)
								SET_ENTITY_COORDS(m_pedFranklin, << 97.1160, -1288.5883, 28.2688 >>)
								SET_ENTITY_HEADING(m_pedFranklin, 246.1162)
								ADD_PED_FOR_DIALOGUE(m_sFinaleBoard.sPedsForConversation, 1, m_pedFranklin, 	"FRANKLIN", TRUE)
							ENDIF
						ENDIF
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("TREVOR", PLAYER_TWO)
							CPRINTLN(DEBUG_HEIST, "Setting exit state of Trevor.")
							IF NOT IS_ENTITY_DEAD(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("TREVOR"))
								m_pedTrevor = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("TREVOR"))
								SET_PED_CONFIG_FLAG(m_pedTrevor, PCF_DisableExplosionReactions, TRUE)
								SET_PED_CONFIG_FLAG(m_pedTrevor, PCF_RunFromFiresAndExplosions, FALSE)
								SET_PED_CONFIG_FLAG(m_pedTrevor, PCF_ListensToSoundEvents, FALSE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_pedTrevor, TRUE)
								SET_ENTITY_COORDS(m_pedTrevor, << 99.1774, -1292.0686, 28.2688 >>)
								SET_ENTITY_HEADING(m_pedTrevor, 20.8497)
								ADD_PED_FOR_DIALOGUE(m_sFinaleBoard.sPedsForConversation, 2, m_pedTrevor, 	"TREVOR", TRUE)
							ENDIF
						ENDIF
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("LESTER", IG_LESTERCREST)
							CPRINTLN(DEBUG_HEIST, "Setting exit state of Lester.")
							IF NOT IS_ENTITY_DEAD(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("LESTER"))
								m_pedLester = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("LESTER"))
								SET_PED_CONFIG_FLAG(m_pedLester, PCF_DisableExplosionReactions, TRUE)
								SET_PED_CONFIG_FLAG(m_pedLester, PCF_RunFromFiresAndExplosions, FALSE)
								SET_PED_CONFIG_FLAG(m_pedLester, PCF_ListensToSoundEvents, FALSE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(m_pedLester, TRUE)
								SET_ENTITY_COORDS(m_pedLester, << 96.2331, -1289.7440, 28.2688 >>)
								SET_ENTITY_HEADING(m_pedLester, 223.2882)
								ADD_PED_FOR_DIALOGUE(m_sFinaleBoard.sPedsForConversation, 3, m_pedLester, "LESTER", TRUE)
							ENDIF
						ENDIF
						IF CAN_SET_EXIT_STATE_FOR_CAMERA()
							IF NOT IS_BIT_SET(m_iState, BIT_BOARD_VIEW_ACTIVATED)
								CPRINTLN(DEBUG_HEIST, "Activating Finale Heist planning board view behind cutscene.")
								Switch_On_Planning_Board_View()
								SET_BIT(m_iState, BIT_BOARD_VIEW_ACTIVATED)
							ENDIF
							
							IF createMichaelCarState = CREATE_MICHAEL_CAR_STATE_WAITING
								SET_MODEL_AS_NO_LONGER_NEEDED(TAILGATER)
							ENDIF

							//Unhide the extra office chair.
							REMOVE_MODEL_HIDE(<<95.1900, -1293.4994, 28.2672>>, 1.50, V_CLUB_OFFICECHAIR, FALSE)
							
							CPRINTLN(DEBUG_HEIST, "Finale Heist board intro cutscene set camera exit state.")
							SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PRIME_BOARD_TRANSITION, FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_BIT_SET(m_iState, BIT_CANDIDATE_ID_SECURED)
				IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
					IF Request_Mission_Launch(m_iMissionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
						CPRINTLN(DEBUG_HEIST, "Finale Heist control thread secured the mission flag.")
						SET_BIT(m_iState, BIT_CANDIDATE_ID_SECURED)
					ENDIF
				ENDIF
			ELSE
				//Repeat play, jump straight into the board.
				ADD_PED_FOR_DIALOGUE(m_sFinaleBoard.sPedsForConversation, 0, NULL, 	"MICHAEL", TRUE)
				ADD_PED_FOR_DIALOGUE(m_sFinaleBoard.sPedsForConversation, 1, NULL, 	"FRANKLIN", TRUE)
				ADD_PED_FOR_DIALOGUE(m_sFinaleBoard.sPedsForConversation, 2, NULL, 	"TREVOR", TRUE)
				ADD_PED_FOR_DIALOGUE(m_sFinaleBoard.sPedsForConversation, 3, NULL, 	"LESTER", TRUE)
				
				CPRINTLN(DEBUG_HEIST, "Activating Finale Heist planning board for repeat play.")
				Switch_On_Planning_Board_View()
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PRIME_BOARD_TRANSITION, FALSE)
			ENDIF
		ENDIF
		
	ELSE
		IF GET_MISSION_COMPLETE_STATE(SP_HEIST_FINALE_2_INTRO)
		OR IS_REPEAT_PLAY_ACTIVE()
			IF HAS_THIS_CUTSCENE_LOADED("BS_2A_2B_INT")
				CPRINTLN(DEBUG_HEIST, "Unloading Finale Heist planning board intro cutscene.")
				REMOVE_CUTSCENE()
			ENDIF
			IF m_iIntroCutsceneRequestID != NULL_OFFMISSION_CUTSCENE_REQUEST
				CPRINTLN(DEBUG_HEIST, "Ending Finale Heist planning board intro cutscene request.")
				END_OFFMISSION_CUTSCENE_REQUEST(m_iIntroCutsceneRequestID)
			ENDIF
		ENDIF
	ENDIF

ENDPROC


PROC Manage_Finale_Heist_Board_Exit_Cutscene()

	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_LOAD_BOARD_EXIT_CUTSCENE)
		INT iHeistChoiceThisFrame = GET_MISSION_FLOW_INT_VALUE(Get_Heist_Choice_FlowInt_For_Heist(HEIST_FINALE))
		IF iHeistChoiceThisFrame != m_iHeistChoiceLastFrame
			CPRINTLN(DEBUG_HEIST, "New heist choice made this frame. Restarting preloading of exit cutscene.")
			REMOVE_CUTSCENE()
			SWITCH iHeistChoiceThisFrame
				CASE HEIST_CHOICE_FINALE_TRAFFCONT
					m_strExitCutsceneName = "BS_2A_INT"
				BREAK
				CASE HEIST_CHOICE_FINALE_HELI
					m_strExitCutsceneName = "BS_2B_INT"
				BREAK
			ENDSWITCH
			m_iHeistChoiceLastFrame = iHeistChoiceThisFrame
		ENDIF
		IF m_iHeistChoiceLastFrame != -1
			REQUEST_CUTSCENE(m_strExitCutsceneName)
		ENDIF
		
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_PROP_VARIATION("LESTER", ANCHOR_EYES, 0, 0)
		ENDIF
	ENDIF
	
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_DO_BOARD_EXIT_CUTSCENE)
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_LOAD_BOARD_EXIT_CUTSCENE)
			IF HAS_THIS_CUTSCENE_LOADED(m_strExitCutsceneName)
				CPRINTLN(DEBUG_HEIST, "Exit cutscene for Finale Heist board loaded. Running.")
				IF NOT IS_REPEAT_PLAY_ACTIVE()
					// This causes an assert in repeat play (url:bugstar:2107518)
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						REGISTER_ENTITY_FOR_CUTSCENE(m_pedMichael, "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
					ENDIF
				ELSE
					// Alternate check used to avoid repeat play assert (url:bugstar:2107518)
					IF NOT IS_ENTITY_DEAD(m_pedMichael)
						REGISTER_ENTITY_FOR_CUTSCENE(m_pedMichael, "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(m_pedLester)
					REGISTER_ENTITY_FOR_CUTSCENE(m_pedLester, "Lester", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, IG_LESTERCREST)
				ENDIF
				IF NOT IS_ENTITY_DEAD(m_pedFranklin)
					REGISTER_ENTITY_FOR_CUTSCENE(m_pedFranklin, "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ONE)
				ENDIF
				IF NOT IS_ENTITY_DEAD(m_pedTrevor)
					REGISTER_ENTITY_FOR_CUTSCENE(m_pedTrevor, "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
				ENDIF
				
				DISABLE_CELLPHONE(TRUE)
				PAUSE_FLOW_HELP_QUEUE(TRUE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				//If we've just picked Rickie make sure to deactivate his nag phonecall.
				INT iCrewIndex
				REPEAT GET_HEIST_CREW_SIZE(HEIST_FINALE) iCrewIndex 
					IF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_FINALE, iCrewIndex) = CM_HACKER_B_RICKIE_UNLOCK
						CANCEL_COMMUNICATION(CALL_HACKER_4)
					ENDIF
				ENDREPEAT
				
				CUTSCENE_OPTION_FLAGS flags 
				IF bStartedExitCutAsMichael
					flags = CUTSCENE_NO_OPTIONS
				ELSE
					flags = CUTSCENE_PLAYER_FP_FLASH_MICHAEL
				ENDIF
				
				START_CUTSCENE(flags)
				
				IF IS_REPEAT_PLAY_ACTIVE()
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
				ENDIF				
				
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_LOAD_BOARD_EXIT_CUTSCENE, FALSE)
				
				m_iCutsceneStartTime = GET_GAME_TIMER()
			ENDIF
		ELSE
			//Grab Lester's entity from the cutscene while it plays.
			IF IS_CUTSCENE_PLAYING()
				m_iCutsceneStartTime = m_iCutsceneStartTime	//Release fix
				#IF IS_DEBUG_BUILD
					IF IS_DEBUG_KEY_JUST_PRESSED(KEY_J, KEYBOARD_MODIFIER_NONE, "Skip cutscene")
						IF (GET_GAME_TIMER() - m_iCutsceneStartTime) > 1500
							STOP_CUTSCENE()
						ENDIF
					ENDIF
				#ENDIF
				
				// #1966073 Prevent replay recording while a planning board cutscene is active.
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
				
				//Recreate_Player_Vehicle_In_Safe_Location(<< 725.6306, -984.5936, 23.1741 >>, 88.7642)
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("LESTER")
					IF DOES_ENTITY_EXIST(m_pedLester)
						DELETE_PED(m_pedLester)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("MICHAEL")
					IF NOT IS_PED_INJURED(m_pedMichael)
						FORCE_PED_MOTION_STATE(m_pedMichael, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
						IF IS_PLAYER_PLAYING(PLAYER_ID())
							SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK)
						ENDIF
					ENDIF
					DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("FRANKLIN")
					IF DOES_ENTITY_EXIST(m_pedFranklin)
						DELETE_PED(m_pedFranklin)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("TREVOR")
					IF DOES_ENTITY_EXIST(m_pedTrevor)
						DELETE_PED(m_pedTrevor)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ENDIF
			ELSE
				IF m_iDelayTimer = -1
				AND NOT IS_REPEAT_PLAY_ACTIVE() // no delay in repeat play
					m_iDelayTimer = GET_GAME_TIMER() + 3500
				ELSE
					IF GET_GAME_TIMER() > m_iDelayTimer
					OR IS_REPEAT_PLAY_ACTIVE()
						//Cutscene has ended. Clean up.
						IF IS_REPEAT_PLAY_ACTIVE() // fade out for repeat play
							DO_SCREEN_FADE_OUT(0)
						ELSE
							g_bMissionStatSystemBlocker = FALSE
							g_sAutosaveData.bFlushAutosaves = FALSE
							MAKE_AUTOSAVE_REQUEST()
						ENDIF
						
						DISABLE_CELLPHONE(FALSE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
						ENABLE_STRIP_CLUBS()
						ResetLastKnownPedInfo(g_savedGlobals.sPlayerData.sInfo, SP_HEIST_FINALE_2_INTRO)
						
						UpdateLastKnownPedInfoPostMission(g_savedGlobals.sPlayerData.sInfo, SP_HEIST_FINALE_2_INTRO)
						
						Mission_Over(m_iMissionCandidateID)
						CLEAR_BIT(m_iState, BIT_CANDIDATE_ID_SECURED)
						SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_DO_BOARD_EXIT_CUTSCENE, FALSE)
						
						//Remove area blocks and restore wanted levels.
						REMOVE_SCENARIO_BLOCKING_AREA(m_scenarioBlock)
						IF m_navmeshBlock != -1
							IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(m_navmeshBlock)
								REMOVE_NAVMESH_BLOCKING_OBJECT(m_navmeshBlock)
							ENDIF
						ENDIF
						CLEAR_PED_NON_CREATION_AREA()
						SET_PED_PATHS_IN_AREA(m_sFinaleBoard.sData.vAreaBlockCenter-m_sFinaleBoard.sData.vAreaBlockDimensions, m_sFinaleBoard.sData.vAreaBlockCenter+m_sFinaleBoard.sData.vAreaBlockDimensions, TRUE)
						SET_MAX_WANTED_LEVEL(5)
						
						#IF FEATURE_SP_DLC_DIRECTOR_MODE
							UNLOCK_DIRECTOR_HEIST_CHARS_FROM_HEIST(HEIST_FINALE)
						#ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_LOAD_BOARD_EXIT_CUTSCENE)
		//Clean up the cutscene if neither flow flag is set.
		IF HAS_THIS_CUTSCENE_LOADED(m_strExitCutsceneName)
			REMOVE_CUTSCENE()
		ENDIF
	ENDIF
	
ENDPROC


//══════════════════════════════╡ Control Loop ╞═══════════════════════════════

SCRIPT
	CPRINTLN(DEBUG_HEIST, "Finale heist controller started.")
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU | FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		Terminate_Heist_Controller()
	ENDIF
	
	//Register script to be saved as running.
	Register_Script_To_Relaunch_List(LAUNCH_BIT_HEIST_CTRL_FINALE)
	
	//Setup the planning board for this heist.
	Setup_Finale_Heist_Board_Data(m_sFinaleBoard.sData, IMAX(g_sHeistChoiceData[HEIST_CHOICE_FINALE_TRAFFCONT].iCrewSize, g_sHeistChoiceData[HEIST_CHOICE_FINALE_HELI].iCrewSize))
	Setup_Planning_Board(m_sFinaleBoard, HEIST_FINALE)
	SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_FINALE, PBDG_0, TRUE)
		
	//Temporary fudge to stop strippers approaching the player while using the planning board.
	//NEEDS REMOVING EVENTUALLY!
	SET_STRIPPER_APPROACH_ENABLED(FALSE)

	WHILE NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINISHED_FINALE)
		
		IF NOT g_flowUnsaved.bUpdatingGameflow
		AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
			#IF IS_DEBUG_BUILD
				Debug_Run_Planning_Board_Widget(m_sFinaleBoard)
			#ENDIF
		
			//Stream the board in and out when necessary.
			Manage_Board_Streaming(m_sFinaleBoard, BOARD_LOAD_DIST, BOARD_UNLOAD_DIST)
			
			//Check for the flow auto toggling the board.
			Manage_Board_View_Auto_Toggling(m_sFinaleBoard)
			
			//Load and run the planning board intro cutscene when requested.
			//Loading happens during mission Finale Heist 2 Intro.
			Manage_Finale_Heist_Board_Intro_Cutscene()
			
			//Load and run the planning board exit cutscene when requested.
			Manage_Finale_Heist_Board_Exit_Cutscene()

			IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
			OR IS_BIT_SET(m_sFinaleBoard.iState, BIT_BOARD_VIEW_INITIALISED)
			
				//Allow the player to interact with the board if it is created.
				Manage_Board_Viewing(m_sFinaleBoard)
				
				//Check for flow requesting the board switches mode.
				Manage_Board_Mode_Switching(m_sFinaleBoard)
				
				//Ensure that changes to boards display groups are kept up-to-date.
				Update_Board_Display_Groups(m_sFinaleBoard)
			ENDIF
		ENDIF
		
		IF (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("finale_heist2A")) > 0)
		OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("finale_heist2B")) > 0)
			HEIST_TIMER_UPDATE()
		ENDIF
		
		// Update the Getaway Vehicle TODO list item based on the missions completion flowflag.
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPE_DONE)
			IF NOT IS_REPEAT_PLAY_ACTIVE()
				IF NOT IS_BIT_SET(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_FINALE], ENUM_TO_INT(PBDG_9))
					SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_FINALE, PBDG_9, TRUE)
				ENDIF
			ELSE
				IF IS_BIT_SET(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_FINALE], ENUM_TO_INT(PBDG_9))
					SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_FINALE, PBDG_9, FALSE)
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(g_savedGlobals.sHeistData.iDisplayGroupVisibleBitset[HEIST_FINALE], ENUM_TO_INT(PBDG_9))
				SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_FINALE, PBDG_9, FALSE)
			ENDIF
		ENDIF
		
		WAIT(0)
		

	ENDWHILE
	
	Remove_Script_From_Relaunch_List(LAUNCH_BIT_HEIST_CTRL_FINALE)
	Terminate_Heist_Controller()
	
ENDSCRIPT
