//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Ben Rollinson					Date: 26/05/10			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│						  Jewelery Heist Control Script							│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛


//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_interiors.sch"
USING "commands_cutscene.sch"

USING "heist_board_data_jewel.sch"
USING "heist_planning_board.sch"
USING "heist_ctrl_generic.sch"
USING "flow_public_core_override.sch"
USING "script_misc.sch"
USING "player_ped_public.sch"
USING "dialogue_public.sch"
USING "script_heist.sch"
USING "blip_control_public.sch"
USING "cutscene_public.sch"
USING "selector_public.sch"
USING "trigger_scene_private.sch"
USING "clearmissionarea.sch"

//═══════════════════════════════╡ Constants ╞═════════════════════════════════

CONST_FLOAT 	BOARD_LOAD_DIST		32.0
CONST_FLOAT		BOARD_UNLOAD_DIST	35.0

//═══════════════════════════════╡ Variables ╞═════════════════════════════════

CONST_INT	BIT_BOARD_VIEW_ACTIVE			0
CONST_INT	BIT_CANDIDATE_ID_SECURED		1
CONST_INT	BIT_LEADIN_CUTSCENE_PLAYING		2
CONST_INT	BIT_LEADIN_PLAYER_LEFT_CAR		3
CONST_INT	BIT_LEADIN_LESTER_LEFT_CAR		4
CONST_INT	BIT_LEADIN_CLEANED_UP			5
CONST_INT	BIT_REPOSITIONED_VEHICLE		6
CONST_INT	BIT_CHARACTERS_HIDDEN			7
CONST_INT	BIT_GRAB_STICK_ENTITY			8
CONST_INT	BIT_GRAB_LESTER_ENTITY			9
CONST_INT	BIT_LOADING_INTRO_CUTSCENE		10
CONST_INT	BIT_LOADING_EXIT_CUTSCENE		11
CONST_INT	BIT_BOARD_ITEMS_SET1_ON			12
CONST_INT	BIT_BOARD_ITEMS_SET2_ON			13
CONST_INT	BIT_BOARD_ITEMS_SET3_ON			14
CONST_INT	BIT_BOARD_ITEMS_SET4_ON			15
CONST_INT	BIT_CUTSCENE_SKIPPED			16
CONST_INT	BIT_FADE_IN_FROM_SHITSKIP		17
CONST_INT	BIT_DISABLE_LOOK_BEHIND			18

INT iState						= 0
INT m_iLeadingCutsceneStage		= 0
INT m_iCutsceneTimer			= -1
INT m_iCutsceneFrameCount		= 0
INT m_iMissionCandidateID	 	= NO_CANDIDATE_ID

//═══════════════════════════════╡ Structures ╞════════════════════════════════

PlanningBoard m_sJewelBoard

//════════════════════════════════╡ Indexes ╞══════════════════════════════════

PED_INDEX 				pedLester
PED_INDEX 				pedMichael
VEHICLE_INDEX			vehMichael
OBJECT_INDEX			objLesterStick
SCENARIO_BLOCKING_INDEX scenarioBlock
CAMERA_INDEX			camLeadinStart
CAMERA_INDEX			camLeadinEnd
SEQUENCE_INDEX			seqMichaelExitCar
SEQUENCE_INDEX			seqLesterExitCar
INT						navmeshBlock = -1
INT 					iDisableLookBehindTime

//═════════════════════════╡ Planning Board Procs ╞════════════════════════════


PROC Terminate_Heist_Controller()
	//Clean up any cutscenes we have loaded.
	IF HAS_THIS_CUTSCENE_LOADED("JH_1_MCS_4P2")
	OR HAS_THIS_CUTSCENE_LOADED("JH_1_MCS_4_P1_ALT1")
		REMOVE_CUTSCENE()
	ENDIF
	
	//Cleanup flow flag states.
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, FALSE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_LOAD_BOARD_EXIT_CUTSCENE, FALSE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_DO_BOARD_EXIT_CUTSCENE, FALSE)
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, HEIST_JEWEL, FALSE)
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_TOGGLE_BOARD, HEIST_JEWEL, FALSE)
	
	//Remove area blocks.
	REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlock)
	IF navmeshBlock != -1
		IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(navmeshBlock)
			REMOVE_NAVMESH_BLOCKING_OBJECT(navmeshBlock)
		ENDIF
	ENDIF
	CLEAR_PED_NON_CREATION_AREA()
	SET_PED_PATHS_IN_AREA(m_sJewelBoard.sData.vAreaBlockCenter-m_sJewelBoard.sData.vAreaBlockDimensions, m_sJewelBoard.sData.vAreaBlockCenter+m_sJewelBoard.sData.vAreaBlockDimensions, TRUE)
	SET_MAX_WANTED_LEVEL(5)
						
	//Release the mission candidate ID if we have one.
	IF m_iMissionCandidateID != NO_CANDIDATE_ID
		Mission_Over(m_iMissionCandidateID)
	ENDIF
	
	//Ensure the planning board has been cleaned up before the control script is terminated.
	WHILE NOT Cleanup_Planning_Board(m_sJewelBoard)
		WAIT(0)
	ENDWHILE
	
	CPRINTLN(DEBUG_HEIST, "Jewellery heist controller terminated.")
	TERMINATE_THIS_THREAD()
ENDPROC


PROC Switch_On_Planning_Board_View()
	//Seamless transition into board view. Toggle board on and disable board exit.
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_DISABLE_BOARD_EXIT, HEIST_JEWEL, TRUE)
	SET_MISSION_FLOW_BITSET_BIT_STATE(FLOWBITSET_HEIST_TOGGLE_BOARD, HEIST_JEWEL, TRUE)
	Manage_Board_View_Auto_Toggling(m_sJewelBoard)
	Manage_Board_Viewing(m_sJewelBoard)
ENDPROC


PROC Manage_Jewel_Heist_Board_Intro_Cutscene()

	//Fix for b*2007445
	ENABLE_EXTERIOR_CULL_MODEL_THIS_FRAME(INT_TO_ENUM( MODEL_NAMES, HASH( "ID2_Emissive_LOD" )))
	
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION)		
		IF NOT IS_REPEAT_PLAY_ACTIVE()
			//Normal playthrough, play the intro cutscene leading in to the planning board interactions.
			IF NOT IS_BIT_SET(iState, BIT_LOADING_INTRO_CUTSCENE)
				REQUEST_CUTSCENE("JH_1_MCS_4_P1_ALT1")
				CLEAR_AUTOSAVE_REQUESTS()
				
				SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_JEWEL, PBDG_0, TRUE)
				
				//Setup area blocks around the board.
				scenarioBlock = ADD_SCENARIO_BLOCKING_AREA(m_sJewelBoard.sData.vAreaBlockCenter-m_sJewelBoard.sData.vAreaBlockDimensions, m_sJewelBoard.sData.vAreaBlockCenter+m_sJewelBoard.sData.vAreaBlockDimensions)
				navmeshBlock = ADD_NAVMESH_BLOCKING_OBJECT(m_sJewelBoard.sData.vAreaBlockCenter, m_sJewelBoard.sData.vAreaBlockDimensions*1.5, 0.0)
				SET_PED_NON_CREATION_AREA(m_sJewelBoard.sData.vAreaBlockCenter-m_sJewelBoard.sData.vAreaBlockDimensions, m_sJewelBoard.sData.vAreaBlockCenter+m_sJewelBoard.sData.vAreaBlockDimensions)
				SET_PED_PATHS_IN_AREA(m_sJewelBoard.sData.vAreaBlockCenter-m_sJewelBoard.sData.vAreaBlockDimensions, m_sJewelBoard.sData.vAreaBlockCenter+m_sJewelBoard.sData.vAreaBlockDimensions, FALSE)
				
				SET_BIT(iState, BIT_LOADING_INTRO_CUTSCENE)
				
			ELIF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				SET_CUTSCENE_PED_PROP_VARIATION("LESTER",ANCHOR_EYES, 0, 0)
			ENDIF
			
			IF GET_MISSION_COMPLETE_STATE(SP_HEIST_JEWELRY_1)
				IF NOT IS_BIT_SET(iState, BIT_CANDIDATE_ID_SECURED)
					IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
						IF Request_Mission_Launch(m_iMissionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
							//Activate the multihead blinders early
							SET_MULTIHEAD_SAFE(TRUE,FALSE)
							
							//Stop the player getting a wanted level for the duration of this planning board sequence.
							IF IS_PLAYER_PLAYING(PLAYER_ID())
								CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
								SET_MAX_WANTED_LEVEL(0)
							ENDIF
						
							CPRINTLN(DEBUG_HEIST, "Jewel Heist control thread secured the mission flag.")
							SET_BIT(iState, BIT_CANDIDATE_ID_SECURED)
							SET_BIT(iState, BIT_LEADIN_CUTSCENE_PLAYING)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(iState, BIT_LEADIN_CUTSCENE_PLAYING)
				SWITCH m_iLeadingCutsceneStage
					CASE 0 
						IF DOES_CAM_EXIST(camLeadinStart)
				         	DESTROY_CAM(camLeadinStart)
				    	ENDIF
				        IF DOES_CAM_EXIST(camLeadinEnd)
				        	DESTROY_CAM(camLeadinEnd)
				        ENDIF
						camLeadinStart = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<724.468201,-992.068420,24.251366>>,<<5.259225,0.000715,25.889177>>,44.131210)
						camLeadinEnd = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<724.372742,-991.320496,24.433039>>,<<31.499252,0.000715,-4.206256>>,44.131210)
						
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
							IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.ped[0])
								pedLester = g_sTriggerSceneAssets.ped[0]
								SET_ENTITY_AS_MISSION_ENTITY(pedLester, FALSE, TRUE)
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
							IF NOT IS_ENTITY_DEAD(g_sTriggerSceneAssets.veh[0])
								vehMichael = g_sTriggerSceneAssets.veh[0]
								//SET_ENTITY_AS_MISSION_ENTITY(vehMichael, FALSE, TRUE)
							ENDIF
						ENDIF
						
						m_iLeadingCutsceneStage++
					BREAK
					
					CASE 1
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,FALSE)
				        DISPLAY_RADAR(FALSE)
				        DISPLAY_HUD(FALSE)
				                            
				        IF DOES_ENTITY_EXIST(vehMichael)
		                    IF IS_VEHICLE_DRIVEABLE(vehMichael)
		                    	POINT_CAM_AT_ENTITY(camLeadinStart, vehMichael, <<0,0,0>>)
		                   	ENDIF
				        ENDIF
				                            
				        SET_CAM_ACTIVE_WITH_INTERP(camLeadinEnd, camLeadinStart, 5000)
				        RENDER_SCRIPT_CAMS(TRUE, FALSE)
				        m_iCutsceneTimer = GET_GAME_TIMER()
				        m_iLeadingCutsceneStage++
					BREAK
					
					CASE 2
						IF (GET_GAME_TIMER() - m_iCutsceneTimer) > 1200
							IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
								IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	                            	IF IS_VEHICLE_DRIVEABLE(vehMichael)  
	                            		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehMichael)
											CPRINTLN(DEBUG_HEIST, "Tasking Michael to leave the vehicle.")
											OPEN_SEQUENCE_TASK(seqMichaelExitCar)
	                            				TASK_LEAVE_VEHICLE(NULL, vehMichael)
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<718.1777, -976.1260, 23.9148>>, 1.0)
											CLOSE_SEQUENCE_TASK(seqMichaelExitCar)
											CLEAR_PED_TASKS(PLAYER_PED_ID())
											TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqMichaelExitCar)
	                                    ENDIF
	                                ENDIF
	                            ENDIF
							ENDIF
							m_iLeadingCutsceneStage++
						ENDIF
						
					BREAK
					
					CASE 3
						IF (GET_GAME_TIMER() - m_iCutsceneTimer) > 2400
							IF NOT IS_ENTITY_DEAD(pedLester)
								IF DOES_ENTITY_EXIST(vehMichael)
	                            	IF IS_VEHICLE_DRIVEABLE(vehMichael)  
	                            		IF IS_PED_IN_VEHICLE(pedLester, vehMichael)
											CPRINTLN(DEBUG_HEIST, "Tasking Lester to leave the vehicle.")
	                            			OPEN_SEQUENCE_TASK(seqLesterExitCar)
	                            				TASK_LEAVE_VEHICLE(NULL, vehMichael)
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<718.1777, -976.1260, 23.9148>>, 1.0)
											CLOSE_SEQUENCE_TASK(seqLesterExitCar)
											CLEAR_PED_TASKS(pedLester)
											TASK_PERFORM_SEQUENCE(pedLester, seqLesterExitCar)
	                                    ENDIF
	                                ENDIF
	                            ENDIF
							ENDIF
							m_iLeadingCutsceneStage++
						ENDIF
					BREAK
					
					CASE 4
						IF (GET_GAME_TIMER() - m_iCutsceneTimer) > 5000
							CLEAR_BIT(iState, BIT_LEADIN_CUTSCENE_PLAYING)
						ENDIF
					BREAK
				ENDSWITCH
			ENDIF

			IF IS_BIT_SET(iState, BIT_CANDIDATE_ID_SECURED)
			AND NOT IS_BIT_SET(iState, BIT_LEADIN_CUTSCENE_PLAYING)
				IF HAS_THIS_CUTSCENE_LOADED("JH_1_MCS_4_P1_ALT1")
					IF NOT IS_CUTSCENE_PLAYING()
						CPRINTLN(DEBUG_HEIST, "Starting Jewel Heist board intro cutscene.")
									
						pedMichael = PLAYER_PED_ID()
						IF NOT IS_ENTITY_DEAD(pedMichael)//1581468
							REGISTER_ENTITY_FOR_CUTSCENE(pedMichael, "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
						ENDIF
						CPRINTLN(DEBUG_HEIST, "Lester to be made by cutscene.")
						IF NOT IS_ENTITY_DEAD(pedLester)//1581468
							REGISTER_ENTITY_FOR_CUTSCENE(pedLester, "Lester", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, IG_LESTERCREST)
						ENDIF
						SET_BIT(iState, BIT_GRAB_LESTER_ENTITY)
						
						CPRINTLN(DEBUG_HEIST, "Walking stick to be made by cutscene.")
						REGISTER_ENTITY_FOR_CUTSCENE(objLesterStick, "WalkingStick_Lester", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PROP_CS_WALKING_STICK)
						SET_BIT(iState, BIT_GRAB_STICK_ENTITY)
						
						ADD_PED_FOR_DIALOGUE(m_sJewelBoard.sPedsForConversation, 0, NULL, "MICHAEL", 	TRUE)
						ADD_PED_FOR_DIALOGUE(m_sJewelBoard.sPedsForConversation, 3, NULL, "LESTER", 	TRUE)							
							
						DISABLE_CELLPHONE(TRUE)
						m_iCutsceneTimer = GET_GAME_TIMER()
						START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)						
					ELSE
						//CPRINTLN(DEBUG_HEIST, "Cutscene time: ", (GET_GAME_TIMER() - m_iCutsceneTimer),".")
						
						// #1966073 Prevent replay recording while a planning board cutscene is active.
						REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
						
						IF NOT IS_BIT_SET(iState, BIT_FADE_IN_FROM_SHITSKIP)
							IF NOT IS_SCREEN_FADED_IN()
							AND NOT IS_SCREEN_FADING_IN()
								DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
							ENDIF
							SET_BIT(iState, BIT_FADE_IN_FROM_SHITSKIP)
						ENDIF
					
						IF WAS_CUTSCENE_SKIPPED()
							SET_BIT(iState, BIT_CUTSCENE_SKIPPED)
						ENDIF
						
						IF IS_BIT_SET(iState, BIT_CUTSCENE_SKIPPED)
							IF IS_SCREEN_FADED_OUT()
								IF NOT IS_BIT_SET(iState, BIT_BOARD_ITEMS_SET1_ON)
									CPRINTLN(DEBUG_HEIST, "Turning on board items set 1 as cutscene is skipped.")
									SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_JEWEL, PBDG_1, TRUE)
									SET_BIT(iState, BIT_BOARD_ITEMS_SET1_ON)
								ENDIF
								IF NOT IS_BIT_SET(iState, BIT_BOARD_ITEMS_SET2_ON)
									CPRINTLN(DEBUG_HEIST, "Turning on board items set 2 as cutscene is skipped.")
									SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_JEWEL, PBDG_2, TRUE)
									SET_BIT(iState, BIT_BOARD_ITEMS_SET2_ON)
								ENDIF
								IF NOT IS_BIT_SET(iState, BIT_BOARD_ITEMS_SET3_ON)
									CPRINTLN(DEBUG_HEIST, "Turning on board items set 3 as cutscene is skipped.")
									SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_JEWEL, PBDG_3, TRUE)
									SET_BIT(iState, BIT_BOARD_ITEMS_SET3_ON)
								ENDIF
								IF NOT IS_BIT_SET(iState, BIT_BOARD_ITEMS_SET4_ON)
									CPRINTLN(DEBUG_HEIST, "Turning on board items set 4 as cutscene is skipped.")
									SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_JEWEL, PBDG_4, TRUE)
									SET_BIT(iState, BIT_BOARD_ITEMS_SET4_ON)
								ENDIF
								Delete_Planning_Board(m_sJewelBoard)
								IF NOT IS_BIT_SET(iState, BIT_BOARD_VIEW_ACTIVE)
									CPRINTLN(DEBUG_HEIST, "Activating Jewelry Heist planning board due to cutscene skip.")
									Switch_On_Planning_Board_View()
									SET_BIT(iState, BIT_BOARD_VIEW_ACTIVE)
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT IS_BIT_SET(iState, BIT_LEADIN_CLEANED_UP)
							CPRINTLN(DEBUG_HEIST, "Cleaned up lead-in cutscene.")
							IF DOES_CAM_EXIST(camLeadinStart)
								SET_CAM_ACTIVE(camLeadinStart, FALSE)
							ENDIF
							IF DOES_CAM_EXIST(camLeadinEnd)
								SET_CAM_ACTIVE(camLeadinEnd, FALSE)
							ENDIF
							RENDER_SCRIPT_CAMS(FALSE, FALSE)
							IF DOES_CAM_EXIST(camLeadinEnd)
								DESTROY_CAM(camLeadinEnd, FALSE)
							ENDIF
							IF DOES_CAM_EXIST(camLeadinEnd)
								DESTROY_CAM(camLeadinEnd, FALSE)
							ENDIF
							IF DOES_ENTITY_EXIST(pedLester)
								DELETE_PED(pedLester)
							ENDIF
							SET_BIT(iState, BIT_LEADIN_CLEANED_UP)
						ENDIF

						IF NOT IS_BIT_SET(iState, BIT_REPOSITIONED_VEHICLE)
							IF m_iCutsceneFrameCount > 0
								CPRINTLN(DEBUG_HEIST, "Repositioning player vehicle.")
								/*
								IF IS_VEHICLE_DRIVEABLE(vehMichael)
									SET_ENTITY_COORDS(vehMichael, <<711.4032, -980.1462, 23.1115>>)
									SET_ENTITY_HEADING(vehMichael, 46.6031)
									SET_MISSION_VEHICLE_GEN_VEHICLE(vehMichael, <<711.4032, -980.1462, 23.1115>>, 46.6031)
								ELSE
									RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<678.103760,-985.749939,21.547447>>, <<743.436096,-985.848816,26.214418>>, 19.250000, 
																		<<719.9169, -982.8645, 23.1393>>, 93.1483,
																		TRUE, FALSE, TRUE)
								ENDIF
								*/
								SET_BIT(iState, BIT_REPOSITIONED_VEHICLE)
							ENDIF
						ENDIF
						
						IF NOT IS_BIT_SET(iState, BIT_BOARD_ITEMS_SET1_ON)
							IF (GET_GAME_TIMER() - m_iCutsceneTimer) > 14058
								CPRINTLN(DEBUG_HEIST, "Turning on board items set 1.")
								SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_JEWEL, PBDG_1, TRUE)
								SET_BIT(iState, BIT_BOARD_ITEMS_SET1_ON)
							ENDIF
						ELIF NOT IS_BIT_SET(iState, BIT_BOARD_ITEMS_SET2_ON)
							IF (GET_GAME_TIMER() - m_iCutsceneTimer) > 22887
								CPRINTLN(DEBUG_HEIST, "Turning on board items set 2.")
								SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_JEWEL, PBDG_2, TRUE)
								SET_BIT(iState, BIT_BOARD_ITEMS_SET2_ON)
							ENDIF
						ELIF NOT IS_BIT_SET(iState, BIT_BOARD_ITEMS_SET3_ON)
							IF (GET_GAME_TIMER() - m_iCutsceneTimer) > 27988
								CPRINTLN(DEBUG_HEIST, "Turning on board items set 3.")
								SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_JEWEL, PBDG_3, TRUE)
								Delete_Planning_Board(m_sJewelBoard)
								SET_BIT(iState, BIT_BOARD_ITEMS_SET3_ON)
							ENDIF
						ELIF NOT IS_BIT_SET(iState, BIT_BOARD_ITEMS_SET4_ON)
							IF (GET_GAME_TIMER() - m_iCutsceneTimer) > 42084
								CPRINTLN(DEBUG_HEIST, "Turning on board items set 4.")
								SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_JEWEL, PBDG_4, TRUE)
								SET_BIT(iState, BIT_BOARD_ITEMS_SET4_ON)
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(iState, BIT_GRAB_STICK_ENTITY)
							IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("WalkingStick_Lester", PROP_CS_WALKING_STICK))
								CPRINTLN(DEBUG_HEIST, "Grabbed walking stick entity created by cutscene.")
								objLesterStick = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("WalkingStick_Lester", PROP_CS_WALKING_STICK))
								CLEAR_BIT(iState, BIT_GRAB_STICK_ENTITY)
							ENDIF
						ENDIF
						
						IF IS_BIT_SET(iState, BIT_GRAB_LESTER_ENTITY)
							IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester", IG_LESTERCREST))
								CPRINTLN(DEBUG_HEIST, "Grabbed Lester entity created by cutscene.")
								pedLester = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester", IG_LESTERCREST))
								CLEAR_BIT(iState, BIT_GRAB_LESTER_ENTITY)
							ENDIF
						ENDIF
						
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
							IF NOT IS_ENTITY_DEAD(pedMichael)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedMichael, TRUE)
								SET_PED_CONFIG_FLAG(pedMichael, PCF_DisableExplosionReactions, TRUE)
								SET_PED_CONFIG_FLAG(pedMichael, PCF_RunFromFiresAndExplosions, FALSE)
								SET_PED_CONFIG_FLAG(pedMichael, PCF_ListensToSoundEvents, FALSE)
								TASK_GO_STRAIGHT_TO_COORD(pedMichael, <<708.8129, -966.3621, 29.3956>>, 1.0)
														

							ENDIF
						ENDIF
						
						IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lester")
							IF NOT IS_ENTITY_DEAD(pedLester)
								IF DOES_ENTITY_EXIST(objLesterStick)
									ATTACH_ENTITY_TO_ENTITY(objLesterStick, pedLester, GET_PED_BONE_INDEX(pedLester, BONETAG_PH_R_HAND), <<0,0,0>>, <<0,0,0>>)
								ENDIF
							ENDIF
							IF NOT IS_ENTITY_DEAD(pedLester)								
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedLester, TRUE)
								SET_PED_CONFIG_FLAG(pedLester, PCF_DisableExplosionReactions, TRUE)
								SET_PED_CONFIG_FLAG(pedLester, PCF_RunFromFiresAndExplosions, FALSE)
								SET_PED_CONFIG_FLAG(pedLester, PCF_ListensToSoundEvents, FALSE)
								TASK_GO_STRAIGHT_TO_COORD(pedLester, <<708.3643, -963.9194, 29.4181>>, 1.0)
							ENDIF
						ENDIF

						IF CAN_SET_EXIT_STATE_FOR_CAMERA()
							IF NOT IS_BIT_SET(iState, BIT_BOARD_VIEW_ACTIVE)
								CPRINTLN(DEBUG_HEIST, "Activating Jewelry Heist planning board at end of cutscene.")
								Switch_On_Planning_Board_View()
								SET_BIT(iState, BIT_BOARD_VIEW_ACTIVE)
							ENDIF
							IF NOT IS_BIT_SET(iState, BIT_CHARACTERS_HIDDEN)
								IF NOT IS_ENTITY_DEAD(pedMichael)
									SET_ENTITY_VISIBLE(pedMichael, FALSE)
								ENDIF
								IF NOT IS_ENTITY_DEAD(pedLester)
									SET_ENTITY_VISIBLE(pedLester, FALSE)
								ENDIF
								IF DOES_ENTITY_EXIST(objLesterStick)
									SET_ENTITY_VISIBLE(objLesterStick, FALSE)
								ENDIF
								SET_BIT(iState, BIT_CHARACTERS_HIDDEN)
							ENDIF

							SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, FALSE)
						ENDIF
						
						m_iCutsceneFrameCount++
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//Repeat play, jump straight into the board.
			ADD_PED_FOR_DIALOGUE(m_sJewelBoard.sPedsForConversation, 0, NULL, "MICHAEL", 	TRUE)
			ADD_PED_FOR_DIALOGUE(m_sJewelBoard.sPedsForConversation, 3, NULL, "LESTER", 	TRUE)							
							
			IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
				IF Request_Mission_Launch(m_iMissionCandidateID, MCTID_MUST_LAUNCH, MISSION_TYPE_STORY) = MCRET_ACCEPTED
					CPRINTLN(DEBUG_HEIST, "Jewel Heist control thread secured the mission flag.")
					SET_BIT(iState, BIT_CANDIDATE_ID_SECURED)
				ENDIF
			ENDIF
			IF IS_BIT_SET(iState, BIT_CANDIDATE_ID_SECURED)		
				CPRINTLN(DEBUG_HEIST, "Activating Jewelry Heist planning board for repeat play.")
				Switch_On_Planning_Board_View()
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, FALSE)
			ENDIF
		ENDIF
	ELSE 
		IF HAS_THIS_CUTSCENE_LOADED("JH_1_MCS_4_P1_ALT1")
			CPRINTLN(DEBUG_HEIST, "Cleaning up Jewelry Heist planning board intro cutscene.")
			REMOVE_CUTSCENE()
		ENDIF
		IF IS_BIT_SET(iState, BIT_LOADING_INTRO_CUTSCENE)
			CLEAR_BIT(iState, BIT_LOADING_INTRO_CUTSCENE)
		ENDIF
	ENDIF

ENDPROC


PROC Manage_Jewel_Heist_Board_Exit_Cutscene()

	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_LOAD_BOARD_EXIT_CUTSCENE)
		IF NOT IS_BIT_SET(iState, BIT_LOADING_EXIT_CUTSCENE)
			REQUEST_CUTSCENE("JH_1_MCS_4P2")
			REQUEST_SCRIPT("lesterHandler")
			REQUEST_ADDITIONAL_TEXT("JHS1AUD", MISSION_DIALOGUE_TEXT_SLOT)
			SET_BIT(iState, BIT_LOADING_EXIT_CUTSCENE)
		ELIF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_PROP_VARIATION("LESTER", ANCHOR_EYES, 0, 0)
		ENDIF
	ENDIF
	
	IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_DO_BOARD_EXIT_CUTSCENE)
		IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_LOAD_BOARD_EXIT_CUTSCENE)
			IF HAS_THIS_CUTSCENE_LOADED("JH_1_MCS_4P2")
			AND HAS_SCRIPT_LOADED("lesterHandler")
			AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
				CPRINTLN(DEBUG_HEIST, "Exit cutscene for Jewel Heist board loaded. Running.")
				IF NOT IS_ENTITY_DEAD(pedLester)
					REGISTER_ENTITY_FOR_CUTSCENE(pedLester, "Lester", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, CS_LESTERCREST)
				ELSE 
					REGISTER_ENTITY_FOR_CUTSCENE(pedLester, "Lester", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, CS_LESTERCREST)
				ENDIF
				IF DOES_ENTITY_EXIST(objLesterStick)
					IF IS_ENTITY_ATTACHED_TO_ANY_PED(objLesterStick)
						DETACH_ENTITY(objLesterStick)
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
				ENDIF
				IF DOES_ENTITY_EXIST(objLesterStick)
					REGISTER_ENTITY_FOR_CUTSCENE(objLesterStick, "WalkingStick_Lester", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, PROP_CS_WALKING_STICK)
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE(objLesterStick, "WalkingStick_Lester", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PROP_CS_WALKING_STICK)
				ENDIF
				
				REQUEST_MODEL(PROP_CS_WALKING_STICK)
				REQUEST_MODEL(PROP_LAPTOP_LESTER2)
				
				DISABLE_CELLPHONE(TRUE)
				PAUSE_FLOW_HELP_QUEUE(TRUE)
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				m_iCutsceneTimer = -1
				START_CUTSCENE()
				
				IF IS_REPEAT_PLAY_ACTIVE()
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
				ENDIF				
				
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_LOAD_BOARD_EXIT_CUTSCENE, FALSE)
			ENDIF
		ELSE
			//Grab Lester's entity from the cutscene while it plays.
			IF IS_CUTSCENE_PLAYING()
			
				// #1966073 Prevent replay recording while a planning board cutscene is active.
				REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			
				IF IS_BIT_SET(iState, BIT_CHARACTERS_HIDDEN)
					IF DOES_ENTITY_EXIST(objLesterStick)
						SET_ENTITY_VISIBLE(objLesterStick, TRUE)
					ENDIF
					IF DOES_ENTITY_EXIST(pedMichael)
						SET_ENTITY_VISIBLE(pedMichael, TRUE)
					ENDIF
					IF DOES_ENTITY_EXIST(pedLester)
						SET_ENTITY_VISIBLE(pedLester, TRUE)
					ENDIF
					CLEAR_BIT(iState, BIT_CHARACTERS_HIDDEN)
				ENDIF

				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lester")
					IF NOT IS_PED_INJURED(pedLester)
						StructLesterHandover sLesterHandover
						sLesterHandover.pedHandover = pedLester
						START_NEW_SCRIPT_WITH_ARGS("lesterHandler", sLesterHandover, SIZE_OF(sLesterHandover), DEFAULT_STACK_SIZE)
						SET_SCRIPT_AS_NO_LONGER_NEEDED("lesterHandler")
						CPRINTLN(DEBUG_HEIST, "Lester handed over to his handler script.")
						iDisableLookBehindTime = GET_GAME_TIMER()
						SET_BIT(iState, BIT_DISABLE_LOOK_BEHIND)
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						VECTOR vMichaelPos = << 709.3766, -964.2602, 29.3954 >>
						GET_GROUND_Z_FOR_3D_COORD(vMichaelPos, vMichaelPos.Z)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							SET_ENTITY_COORDS(PLAYER_PED_ID(),vMichaelPos)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 261.7831)
							IF IS_PLAYER_PLAYING(PLAYER_ID())
								SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK)
							ENDIF
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
						ENDIF
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					ENDIF
				ENDIF

				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
				ENDIF
			ELSE
				IF m_iCutsceneTimer = -1
				AND NOT IS_REPEAT_PLAY_ACTIVE()// no delay in repeat play
					m_iCutsceneTimer = GET_GAME_TIMER() + 1500
				ELSE
					IF GET_GAME_TIMER() > m_iCutsceneTimer
					OR IS_REPEAT_PLAY_ACTIVE()
						//Cutscene has ended. Clean up.
						IF IS_REPEAT_PLAY_ACTIVE() // fade out for repeat play
							DO_SCREEN_FADE_OUT(0)
						ELSE
							g_bMissionStatSystemBlocker = FALSE
							g_sAutosaveData.bFlushAutosaves = FALSE
							MAKE_AUTOSAVE_REQUEST()
						ENDIF
						
						SET_MODEL_AS_NO_LONGER_NEEDED(PROP_CS_WALKING_STICK)
						SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LAPTOP_LESTER2)						
						//MISSION_FLOW_PLAY_END_OF_MISSION_MUSIC(FALSE)
						DISABLE_CELLPHONE(FALSE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
						ResetLastKnownPedInfo(g_savedGlobals.sPlayerData.sInfo, SP_HEIST_JEWELRY_1)
						
						UpdateLastKnownPedInfoPostMission(g_savedGlobals.sPlayerData.sInfo, SP_HEIST_JEWELRY_1)
						
						Mission_Over(m_iMissionCandidateID)
						CLEAR_BIT(iState, BIT_CANDIDATE_ID_SECURED)
						
						//Remove area blocks.
						REMOVE_SCENARIO_BLOCKING_AREA(scenarioBlock)
						IF navmeshBlock != -1
							IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(navmeshBlock)
								REMOVE_NAVMESH_BLOCKING_OBJECT(navmeshBlock)
							ENDIF
						ENDIF
						CLEAR_PED_NON_CREATION_AREA()
						SET_PED_PATHS_IN_AREA(m_sJewelBoard.sData.vAreaBlockCenter-m_sJewelBoard.sData.vAreaBlockDimensions, m_sJewelBoard.sData.vAreaBlockCenter+m_sJewelBoard.sData.vAreaBlockDimensions, TRUE)
						SET_MAX_WANTED_LEVEL(5)
						
						SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_DO_BOARD_EXIT_CUTSCENE, FALSE)
						
						#IF FEATURE_SP_DLC_DIRECTOR_MODE
							UNLOCK_DIRECTOR_HEIST_CHARS_FROM_HEIST(HEIST_JEWEL)
						#ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELIF NOT GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_LOAD_BOARD_EXIT_CUTSCENE)
		//Clean up the cutscene if neither flow flag is set.
		IF HAS_THIS_CUTSCENE_LOADED("JH_1_MCS_4P2")
			REMOVE_CUTSCENE()
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(iState, BIT_DISABLE_LOOK_BEHIND)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_LOOK_BEHIND)
		IF GET_GAME_TIMER() > iDisableLookBehindTime + 1000
			CLEAR_BIT(iState, BIT_DISABLE_LOOK_BEHIND)
		ENDIF
	ENDIF
	
ENDPROC


//══════════════════════════════╡ Control Loop ╞═══════════════════════════════

SCRIPT
	CPRINTLN(DEBUG_HEIST, "Jewelry heist controller started.")

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		Terminate_Heist_Controller()
	ENDIF
	
	//Register script to be saved as running.
	Register_Script_To_Relaunch_List(LAUNCH_BIT_HEIST_CTRL_JEWEL)
	
	//Setup the planning board for the heist.
	Setup_Jewel_Heist_Board_Data(m_sJewelBoard.sData, IMAX(g_sHeistChoiceData[HEIST_CHOICE_JEWEL_STEALTH].iCrewSize, g_sHeistChoiceData[HEIST_CHOICE_JEWEL_HIGH_IMPACT].iCrewSize))
	Setup_Planning_Board(m_sJewelBoard, HEIST_JEWEL)
	
	WHILE NOT Get_Mission_Flow_Flag_State(FLOWFLAG_HEIST_FINISHED_JEWEL)
	
		IF NOT g_flowUnsaved.bUpdatingGameflow
			#IF IS_DEBUG_BUILD
				Debug_Run_Planning_Board_Widget(m_sJewelBoard)
			#ENDIF
			
			//Stream the board in and out when necessary.
			IF NOT IS_MISSION_AVAILABLE(SP_HEIST_JEWELRY_1)
			AND NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
				Manage_Board_Streaming(m_sJewelBoard, BOARD_LOAD_DIST, BOARD_UNLOAD_DIST)
			ELIF IS_BIT_SET(m_sJewelBoard.iState, BIT_BOARD_LOADED)
				CPRINTLN(DEBUG_HEIST, "Cleaning up jewlery heist planning board as the setup mission is available.")
				Cleanup_Planning_Board(m_sJewelBoard)
			ENDIF
			
			IF IS_BIT_SET(m_sJewelBoard.iState, BIT_BOARD_LOADED)
				IF MISSION_FLOW_GET_RUNNING_MISSION() = SP_HEIST_JEWELRY_1
					CPRINTLN(DEBUG_HEIST, "Cleaning up loaded but unused planning board assets during HEIST_JEWELRY_1.")
					WHILE NOT Cleanup_Planning_Board(m_sJewelBoard)
						WAIT(0)
					ENDWHILE
				ENDIF
			ENDIF
			
			//Check to see if the planning board view is being told to auto toggle on or off.
			Manage_Board_View_Auto_Toggling(m_sJewelBoard)
			
			//Load and run the planning board intro cutscene when requested.
			//Loading happens during mission Jewelry Setup 1.
			Manage_Jewel_Heist_Board_Intro_Cutscene()
			
			//Load and run the planning board exit cutscene when requested.
			Manage_Jewel_Heist_Board_Exit_Cutscene()
				
			IF CAN_MISSION_TYPE_START_AGAINST_CURRENT_TYPE(MISSION_TYPE_STORY)
			OR IS_BIT_SET(m_sJewelBoard.iState, BIT_BOARD_VIEW_INITIALISED)
				//Allow the player to interact with the board if it is created.
				Manage_Board_Viewing(m_sJewelBoard)
				
				//Check for flow requesting the board switches mode.
				Manage_Board_Mode_Switching(m_sJewelBoard)
				
				//Ensure that changes to boards display groups are kept up-to-date.
				Update_Board_Display_Groups(m_sJewelBoard)
			ENDIF
		ENDIF
		
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("jewelry_heist")) > 0
			HEIST_TIMER_UPDATE()
		ENDIF
		
		WAIT(0)
	ENDWHILE
	
	Remove_Script_From_Relaunch_List(LAUNCH_BIT_HEIST_CTRL_JEWEL)
	Terminate_Heist_Controller()
	
ENDSCRIPT
