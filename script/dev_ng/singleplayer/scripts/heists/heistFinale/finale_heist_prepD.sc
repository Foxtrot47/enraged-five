//Big Score Prep D.sc
//Ross Wallace 20/06/2012

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "commands_clock.sch"
USING "globals.sch"
USING "player_ped_public.sch"
USING "dialogue_public.sch"
USING "blip_control_public.sch"
// ____________________________________ INCLUDES ___________________________________________
USING "commands_script.sch"
USING "CompletionPercentage_public.sch"
using "commands_pad.sch"
using "commands_misc.sch"
using "commands_player.sch"
USING "Commands_streaming.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "commands_interiors.sch"
USING "script_MISC.sch"
USING "commands_object.sch"
USING "script_ped.sch"
USING "chase_hint_cam.sch"
USING "script_blips.sch" 
USING "locates_public.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "script_maths.sch"
USING "mission_stat_public.sch"
USING "cutscene_public.sch"
using "selector_public.sch"
USING "taxi_functions.sch"
USING "timeLapse.sch"
USING "shop_public.sch"
USING "rappel_public.sch"
USING "Shared_hud_Displays.sch"
USING "mission_stat_public.sch"
USING "clearMissionArea.sch"
USING "script_heist.sch"
USING "commands_recording.sch"
USING "commands_vehicle.sch"

CAMERA_INDEX initialCam
CAMERA_INDEX destinationCam

VEHICLE_INDEX viSkyLift

CONST_INT REPLAY_PLAYER_STARTED_AS  	0

#IF IS_DEBUG_BUILD 
	USING "select_mission_stage.sch"
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

#IF IS_DEBUG_BUILD 

	WIDGET_GROUP_ID bsPrepWidgetGroup
	
	INT iDebugMissionStage
	BOOL bDebugInitialised
	BOOL bIsSuperDebugEnabled
	BOOL bDebugOn = TRUE
	
	CONST_INT MAX_SKIP_MENU_LENGTH 13                      // number of stages in mission + 2 (for menu )
	INT iReturnStage                                       // mission stage to jump to
	MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]      // struct containing the debug menu 
	
#ENDIF

VEHICLE_INDEX viTrainIndex
//PED_INDEX piTrainDriver

SELECTOR_PED_STRUCT pedSelector
//SELECTOR_CAM_STRUCT sCamDetails

structPedsForConversation myScriptedSpeech
INT i_current_event

LOCATES_HEADER_DATA sLocatesData

// ____________________________________ FUNCTIONS __________________________________________

ENUM MISSION_STAGE_FLAG 

	STAGE_TIME_LAPSE,						//0
	STAGE_INITIALISE,						//1
	STAGE_GET_TO_TRAIN_SWITCH,				//2
	STAGE_WAIT_FOR_TRAIN,					//3
	STAGE_TRAIN_COMING_INTO_SIDINGS_CUTSCENE,		//4
	STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING,	//5
	STAGE_FLY_AWAY_WITH_CARRIAGE,			//6
	STAGE_MISSION_PASSED,					//7
	STAGE_DEBUG								//8

ENDENUM


MISSION_STAGE_FLAG mission_stage =  STAGE_INITIALISE //STAGE_INITIALISE //STAGE_TIME_LAPSE


//PURPOSE: Starts or stops a cutscene. If TRUE is passed TIMERA() is set to Zero
PROC SET_CUTSCENE_RUNNING(BOOL isRunning, BOOL doGameCamInterp = FALSE, INT durationFromInterp = 2000, BOOL turnOffGadgets = TRUE)

//	SET_USE_HIGHDOF(isRunning)removed
	SET_WIDESCREEN_BORDERS(isRunning,0)
	IF turnOffGadgets = TRUE
		SET_PLAYER_CONTROL(PLAYER_ID(), (NOT isRunning),  SPC_DEACTIVATE_GADGETS)
	ELSE
		SET_PLAYER_CONTROL(PLAYER_ID(), (NOT isRunning))
	ENDIF
	
	IF doGameCamInterp
		//STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
		RENDER_SCRIPT_CAMS(isRunning, doGameCamInterp, durationFromInterp)
	ELSE
		RENDER_SCRIPT_CAMS(isRunning, doGameCamInterp, durationFromInterp)
	ENDIF
	CLEAR_HELP(TRUE)
	CLEAR_PRINTS()
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(isRunning)
		
	DISABLE_CELLPHONE(isRunning)
	DISPLAY_HUD(NOT isRunning)
	DISPLAY_RADAR(NOT isRunning)
ENDPROC

PROC FADE_IN_IF_NEEDED()

	IF IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
	OR NOT IS_SCREEN_FADING_IN()
		DO_SCREEN_FADE_IN(500)
	ENDIF

ENDPROC

PED_INDEX piTruckDriver1
VEHICLE_INDEX viTrailerDropOff1Cab
VEHICLE_INDEX viTrailerDropOff1

PED_INDEX piTruckDriver2
VEHICLE_INDEX viTrailerDropOff2Cab
VEHICLE_INDEX viTrailerDropOff2


//Cleanup
PROC MISSION_CLEANUP()

	//For replays
	
	TRIGGER_MUSIC_EVENT("FHPRD_FAIL")
	
	#IF IS_DEBUG_BUILD
		DELETE_WIDGET_GROUP(bsPrepWidgetGroup)
		STOP_CUTSCENE(TRUE)
	#ENDIF
	
	UPDATE_DYNAMIC_VEHICLE_GEN_POSITION(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY, <<0,0,0>>, 0.0)
	
	//STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(<<2632.059,2934.051,43.705>>, 1.0, p_rail_controller_s, NORMAL_BLEND_DURATION)
	
		
	SET_CUTSCENE_RUNNING(FALSE)
	//SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
	HANG_UP_AND_PUT_AWAY_PHONE() 
		
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	SET_BUILDING_STATE(BUILDINGNAME_IPL_BIG_SCORE_RAILS_1,	BUILDINGSTATE_NORMAL)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_BIG_SCORE_RAILS_2,	BUILDINGSTATE_NORMAL)
	
	IF NOT IS_ENTITY_DEAD(viTrailerDropOff1Cab)
		SET_ENTITY_PROOFS(viTrailerDropOff1Cab, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
	ENDIF
	IF NOT IS_ENTITY_DEAD(viTrailerDropOff2Cab)
		SET_ENTITY_PROOFS(viTrailerDropOff2Cab, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
	ENDIF
	IF NOT IS_ENTITY_DEAD(viTrailerDropOff1)
		SET_ENTITY_PROOFS(viTrailerDropOff1, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
	ENDIF
	IF NOT IS_ENTITY_DEAD(viTrailerDropOff2)
		SET_ENTITY_PROOFS(viTrailerDropOff2, FALSE, FALSE, FALSE, FALSE, FALSE, FALSE)
	ENDIF
	
	SET_REDUCE_VEHICLE_MODEL_BUDGET(FALSE)
	SET_REDUCE_PED_MODEL_BUDGET(FALSE)
	
	DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
	
	SET_BUILDING_STATE(BUILDINGNAME_IPL_BIG_SCORE_RAIL_TANKCAR, BUILDINGSTATE_NORMAL, TRUE)
	
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_MAX_WANTED_LEVEL(5)
	//SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)

	CLEAR_TIMECYCLE_MODIFIER()
	SET_TIME_SCALE(1.0)		
	
ENDPROC


PROC Mission_Passed()	
	
	FADE_IN_IF_NEEDED()
	
	INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(FALSE, FINPD_TIME)
	
	#IF IS_DEBUG_BUILD
		
	#ENDIF
	SET_CUTSCENE_RUNNING(FALSE)
	Mission_Flow_Mission_Passed()
	CLEAR_PRINTS () 	
	Mission_Cleanup()
	TERMINATE_THIS_THREAD()
		
ENDPROC

//Mission Failed
PROC Mission_Failed()	
	
	KILL_ANY_CONVERSATION()
	
	TRIGGER_MUSIC_EVENT("FHPRD_FAIL")

	IF NOT IS_ENTITY_DEAD(viSkyLift)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viSkyLift)
			MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<1722.4844, 3254.0852, 40.1525>>, 281.7393)
		ENDIF
	ENDIF

	Mission_Flow_Mission_Failed()
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		PRINTLN("Waiting for GET_MISSION_FLOW_SAFE_TO_CLEANUP to be TRUE") 
		WAIT(0)
	ENDWHILE
	
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
	ENDIF

	IF DOES_ENTITY_EXIST(viTrainIndex)
		DELETE_MISSION_TRAIN(viTrainIndex)
	ENDIF

	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	
	Mission_Cleanup() // must only take 1 frame and terminate the thread
	
	TERMINATE_THIS_THREAD()
		
ENDPROC

PROC DELETE_ARRAY_OF_PEDS(PED_INDEX &thesePeds[], BOOL bSetAsNoLongerNeeded = FALSE, INT iMaxIndex = -1)

	INT i
	
	INT iArrayLength 
	
	IF iMaxIndex <> -1
		iArrayLength = iMaxIndex
	ELSE
		iArrayLength = COUNT_OF(thesePeds) -1
	ENDIF
	
	FOR i = 0 TO iArrayLength
		IF DOES_ENTITY_EXIST(thesePeds[i])
			IF NOT bSetAsNoLongerNeeded
				DELETE_PED(thesePeds[i])
				PRINTLN("deleted Ped in array at: ", i)
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(thesePeds[i])
				PRINTLN("set Ped as no longer needed in array at: ", i)
			ENDIF
			
		ENDIF
	ENDFOR
	
ENDPROC

FUNC BOOL IS_COORD_IN_PLAYER_SNIPER_SCOPE(VECTOR vPos, FLOAT fGameplayCamFOV = 12.0, FLOAT fXTolerance = 0.23, FLOAT fYTolerance = 0.47)

      IF IS_PLAYER_PLAYING(PLAYER_ID())
            IF IS_PLAYER_FREE_AIMING(PLAYER_ID())
                  IF NOT IS_PED_INJURED(PLAYER_PED_ID())
            
                        WEAPON_TYPE CurrentPlayerWeapon

                        IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), CurrentPlayerWeapon)
                              IF ( CurrentPlayerWeapon = WEAPONTYPE_SNIPERRIFLE OR CurrentPlayerWeapon = WEAPONTYPE_HEAVYSNIPER )

                                   // IF DOES_ENTITY_EXIST(vPos)
                                         // IF NOT IS_ENTITY_DEAD(vPos)
                                                
                                                //IF IS_ENTITY_ON_SCREEN(vPos)
                                                      IF IS_SPHERE_VISIBLE(vPos, 0.001)
                                                            IF GET_GAMEPLAY_CAM_FOV() < fGameplayCamFOV
                                                            
                                                                  FLOAT fXPosition, fYPosition
                                                            
                                                                  GET_SCREEN_COORD_FROM_WORLD_COORD(vPos, fXPosition, fYPosition)
                                                                                                                        
                                                                  IF    (fXPosition > 0.5 - fXTolerance) AND (fXPosition < 0.5 + fXTolerance)
                                                                  AND (fYPosition > 0.5 - fYTolerance) AND (fYPosition < 0.5 + fYTolerance)

                                                                        RETURN TRUE
                                                                        
                                                                  ENDIF
                                                                  
                                                            ENDIF
                                                      ENDIF
                                               // ENDIF
                                                
                                         // ENDIF
                                   // ENDIF
                                    
                              ENDIF
                        ENDIF
                  ENDIF
            ENDIF       
      ENDIF

      RETURN FALSE

ENDFUNC



structTimelapse sTimelapse
BOOL bRequestCutInTimeLapse

PROC timeLapseCutscene()


	IF NOT IS_REPLAY_IN_PROGRESS()
		IF DO_TIMELAPSE(SP_MISSION_MICHAEL_4, sTimelapse, FALSE, FALSE)
		
			mission_stage = STAGE_INITIALISE	
			IF bRequestCutInTimeLapse = FALSE
				
				bRequestCutInTimeLapse = TRUE
			ENDIF				
		ENDIF				
	ELSE
		mission_stage = STAGE_INITIALISE		
	ENDIF
	
ENDPROC

/// PURPOSE: Requests assets for next stage also sets any mission variable states such as wanted level etc.
///    
/// PARAMS: MISSION_STAGE_FLAG stageAssetsToLoad
///    stageAssetsToLoad - 
///    
///    
PROC REQUEST_STAGE_ASSETS(MISSION_STAGE_FLAG stageAssetsToLoad)

	SWITCH stageAssetsToLoad
	
		CASE STAGE_GET_TO_TRAIN_SWITCH
			REQUEST_MODEL(S_M_Y_CONSTRUCT_01)
			REQUEST_ANIM_DICT("missbigscoreprep")
			REQUEST_MODEL(Prop_LD_Rail_01)
			REQUEST_MODEL(Prop_LD_Rail_02)
		BREAK
		
		CASE STAGE_WAIT_FOR_TRAIN
			REQUEST_MODEL(S_M_Y_CONSTRUCT_01)
			REQUEST_MODEL(FREIGHT)
			REQUEST_MODEL(FREIGHTCONT1)
			REQUEST_MODEL(FREIGHTCONT2)
			//REQUEST_MODEL(FREIGHTCRATE)
			REQUEST_MODEL(FREIGHTGRAIN)
			REQUEST_MODEL(TANKERCAR)
			REQUEST_MODEL(FREIGHTCAR)
			REQUEST_ANIM_DICT("missbigscoreprepd")
			REQUEST_AMBIENT_AUDIO_BANK("BIG_SCORE_PREP_D")
		BREAK
		
		CASE STAGE_TRAIN_COMING_INTO_SIDINGS_CUTSCENE
			REQUEST_MODEL(S_M_Y_CONSTRUCT_01)
			REQUEST_VEHICLE_RECORDING(1, "BSPrepheli")
			REQUEST_MODEL(SKYLIFT)
			REQUEST_MODEL(FREIGHT)
			REQUEST_MODEL(FREIGHTCONT1)
			REQUEST_MODEL(FREIGHTCONT2)
			//REQUEST_MODEL(FREIGHTCRATE)
			REQUEST_MODEL(FREIGHTGRAIN)
			REQUEST_MODEL(TANKERCAR)
			REQUEST_MODEL(FREIGHTCAR)
		BREAK
		
		CASE STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING
			REQUEST_MODEL(S_M_Y_CONSTRUCT_01)
			REQUEST_MODEL(SKYLIFT)
			REQUEST_VEHICLE_RECORDING(1, "BSPrepheli")
			REQUEST_MODEL(FREIGHT)
			REQUEST_MODEL(FREIGHTCONT1)
			REQUEST_MODEL(FREIGHTCONT2)
			//REQUEST_MODEL(FREIGHTCRATE)
			REQUEST_MODEL(FREIGHTGRAIN)
			REQUEST_MODEL(TANKERCAR)
			REQUEST_MODEL(FREIGHTCAR)
		BREAK
		
		CASE STAGE_FLY_AWAY_WITH_CARRIAGE
			REQUEST_MODEL(S_M_Y_CONSTRUCT_01)
			REQUEST_MODEL(SKYLIFT)
			REQUEST_MODEL(FREIGHT)
			REQUEST_MODEL(FREIGHTCONT1)
			REQUEST_MODEL(FREIGHTCONT2)
			//REQUEST_MODEL(FREIGHTCRATE)
			REQUEST_MODEL(FREIGHTGRAIN)
			REQUEST_MODEL(TANKERCAR)
			REQUEST_MODEL(FREIGHTCAR)
			
			REQUEST_MODEL(FREIGHTTRAILER)
			REQUEST_MODEL(PHANTOM)
			
			REQUEST_MODEL_FOR_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_FINALE, 0))
					
		BREAK
					
	ENDSWITCH
		
ENDPROC

FUNC BOOL HAVE_STAGE_ASSETS_LOADED(MISSION_STAGE_FLAG stageAssetsToLoad)

	SWITCH stageAssetsToLoad
	
		CASE STAGE_GET_TO_TRAIN_SWITCH
			IF HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)
			AND HAS_ANIM_DICT_LOADED("missbigscoreprep")
			AND HAS_MODEL_LOADED(Prop_LD_Rail_01)
			AND HAS_MODEL_LOADED(Prop_LD_Rail_02)
				RETURN TRUE
			ENDIF
		BREAK
	
		CASE STAGE_WAIT_FOR_TRAIN
			IF HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)
			AND HAS_MODEL_LOADED(FREIGHT)
			AND HAS_MODEL_LOADED(FREIGHTCONT1)
			AND HAS_MODEL_LOADED(FREIGHTCONT2)
			//AND HAS_MODEL_LOADED(FREIGHTCRATE)
			AND HAS_MODEL_LOADED(FREIGHTGRAIN)
			AND HAS_MODEL_LOADED(TANKERCAR)
			AND HAS_MODEL_LOADED(FREIGHTCAR)			
			AND REQUEST_AMBIENT_AUDIO_BANK("BIG_SCORE_PREP_D")
			AND HAS_ANIM_DICT_LOADED("missbigscoreprepd")
				RETURN TRUE
			ENDIF		
		BREAK
		
		CASE STAGE_TRAIN_COMING_INTO_SIDINGS_CUTSCENE
			IF HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)
			AND HAS_MODEL_LOADED(SKYLIFT)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "BSPrepheli")
			AND HAS_MODEL_LOADED(FREIGHT)
			AND HAS_MODEL_LOADED(FREIGHTCONT1)
			AND HAS_MODEL_LOADED(FREIGHTCONT2)
			//AND HAS_MODEL_LOADED(FREIGHTCRATE)
			AND HAS_MODEL_LOADED(FREIGHTGRAIN)
			AND HAS_MODEL_LOADED(TANKERCAR)
			AND HAS_MODEL_LOADED(FREIGHTCAR)
				RETURN TRUE
			ENDIF			
		BREAK
		
		CASE STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING
			IF HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)
			AND HAS_MODEL_LOADED(SKYLIFT)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "BSPrepheli")
			AND HAS_MODEL_LOADED(FREIGHT)
			AND HAS_MODEL_LOADED(FREIGHTCONT1)
			AND HAS_MODEL_LOADED(FREIGHTCONT2)
			//AND HAS_MODEL_LOADED(FREIGHTCRATE)
			AND HAS_MODEL_LOADED(FREIGHTGRAIN)
			AND HAS_MODEL_LOADED(TANKERCAR)
			AND HAS_MODEL_LOADED(FREIGHTCAR)
				RETURN TRUE
			ENDIF			
		BREAK
	
		CASE STAGE_FLY_AWAY_WITH_CARRIAGE
			IF HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)
			AND HAS_MODEL_LOADED(SKYLIFT)
			AND HAS_MODEL_LOADED(FREIGHT)
			AND HAS_MODEL_LOADED(FREIGHTCONT1)
			AND HAS_MODEL_LOADED(FREIGHTCONT2)
			//AND HAS_MODEL_LOADED(FREIGHTCRATE)
			AND HAS_MODEL_LOADED(FREIGHTGRAIN)
			AND HAS_MODEL_LOADED(TANKERCAR)
			AND HAS_MODEL_LOADED(FREIGHTCAR)
			AND HAS_MODEL_LOADED(FREIGHTTRAILER)
			AND HAS_MODEL_LOADED(PHANTOM)
			AND HAS_MODEL_LOADED_FOR_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_FINALE, 0))
				RETURN TRUE
			ENDIF			
		BREAK
	
	ENDSWITCH

	RETURN FALSE
	
ENDFUNC


PROC CREATE_STAGE_ASSETS(MISSION_STAGE_FLAG stageAssetsToLoad)

	SWITCH stageAssetsToLoad

		
	ENDSWITCH

ENDPROC


PROC RESET_GAME_CAMERA(FLOAT fHeading = 0.0)

	SET_GAMEPLAY_CAM_RELATIVE_HEADING(fHeading)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(fHeading)

ENDPROC

PROC SET_SKYLIFT_BIG_MAGNET(BOOL bOnOrOff)

	SET_VEHICLE_EXTRA(viSkylift, 10, NOT bOnOrOff)
		
ENDPROC

PROC SET_SKYLIFT_FREIGHT_COLLISION_BOX(BOOL bOnOrOff)
	
	SET_VEHICLE_EXTRA(viSkylift, 11, NOT bOnOrOff)

ENDPROC

PROC SET_SKYLIFT_FLATBED_COLLISION_BOX(BOOL bOnOrOff)

	SET_VEHICLE_EXTRA(viSkylift, 12, NOT bOnOrOff)
	
ENDPROC

PROC CREATE_SKYLIFT()

	REQUEST_COLLISION_AT_COORD(<<1770.4459, 3240.6953, 41.1108>>)
	viSkylift = CREATE_VEHICLE(SKYLIFT, <<1770.4459, 3240.6953, 41.1108>>, 281.6867)//GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(1, 3000.0, "BsPrepHeli"))
	//START_PLAYBACK_RECORDED_VEHICLE(viSkylift, 1, "BsPrepHeli")
	//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(viSkylift, 7000.0)
	SET_VEHICLE_ENGINE_ON(viSkylift, TRUE, TRUE)
	SET_HELI_BLADES_FULL_SPEED(viSkylift)
	SET_PLAYBACK_SPEED(viSkylift, 0.6)

	SET_VEHICLE_STRONG(viSkyLift, TRUE)

	SET_ENTITY_HEALTH(viSkylift, 3000)
	
	SET_VEHICLE_ENGINE_CAN_DEGRADE(viSkylift, FALSE)

//	SET_VEHICLE_ON_GROUND_PROPERLY(viSkyLift)
//	FREEZE_ENTITY_POSITION(viSkyLift, TRUE)


	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(viSkylift, TRUE)

	SET_ENTITY_LOD_DIST(viSkylift, 3000)
	SET_VEHICLE_LOD_MULTIPLIER(viSkylift, 100.0)
	
	SET_ENTITY_LOAD_COLLISION_FLAG(viSkyLift, TRUE)
	
	SET_VEHICLE_IGNORED_BY_QUICK_SAVE(viSkyLift)

	SET_SKYLIFT_FREIGHT_COLLISION_BOX(FALSE)
	SET_SKYLIFT_BIG_MAGNET(FALSE)
	SET_SKYLIFT_FLATBED_COLLISION_BOX(FALSE)
	
	SET_VEHICLE_AS_RESTRICTED(viSkylift, 0)
	
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(viSkylift)
	INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(viSkylift)
	
ENDPROC

PROC CREATE_TRAIN_AND_DRIVER(VECTOR vPos, FLOAT fCruiseSpeed = 30.5)

	viTrainIndex = CREATE_MISSION_TRAIN(7,  vPos,  FALSE)
			
	SET_TRAIN_CRUISE_SPEED(viTrainIndex, fCruiseSpeed)
	SET_TRAIN_SPEED(viTrainIndex, fCruiseSpeed)
		
	SET_ENTITY_LOD_DIST(viTrainIndex, 3000)
	SET_VEHICLE_LOD_MULTIPLIER(viTrainIndex, 100.0)
		
ENDPROC

// <<2773.7463, 2828.9158, 36.5910>> //, 49.4780
FLOAT fDistanceToEnd
BLIP_INDEX blipEngine
BLIP_INDEX blipViSkylift
VEHICLE_INDEX viTempTrainCarriage[9]
INT iMagnetBuzzEffects[9]
VECTOR vTempPosition[9]

PROC CREATE_SEPARATE_TRAIN_MODELS_FROM_TRAIN()
	IF NOT IS_ENTITY_DEAD(viTrainIndex)
	
		vTempPosition[0] = GET_ENTITY_COORDS(GET_TRAIN_CARRIAGE(viTrainIndex, 0))
		viTempTrainCarriage[0] = CREATE_VEHICLE(FREIGHT, vTempPosition[0], 0.0)
		SET_ENTITY_COORDS_NO_OFFSET(viTempTrainCarriage[0], vTempPosition[0])
		SET_ENTITY_ROTATION(viTempTrainCarriage[0], GET_ENTITY_ROTATION(GET_TRAIN_CARRIAGE(viTrainIndex, 0)))
						
		vTempPosition[1] = GET_ENTITY_COORDS(GET_TRAIN_CARRIAGE(viTrainIndex, 1))
		viTempTrainCarriage[1] = CREATE_VEHICLE(FREIGHTCAR, vTempPosition[1], 0.0)
		SET_ENTITY_COORDS_NO_OFFSET(viTempTrainCarriage[1], vTempPosition[1])
		SET_ENTITY_ROTATION(viTempTrainCarriage[1], GET_ENTITY_ROTATION(GET_TRAIN_CARRIAGE(viTrainIndex, 1)))
		
		vTempPosition[2] = GET_ENTITY_COORDS(GET_TRAIN_CARRIAGE(viTrainIndex, 2))
		viTempTrainCarriage[2] = CREATE_VEHICLE(FREIGHTCAR, vTempPosition[2], 0.0)
		SET_ENTITY_COORDS_NO_OFFSET(viTempTrainCarriage[2], vTempPosition[2])
		SET_ENTITY_ROTATION(viTempTrainCarriage[2], GET_ENTITY_ROTATION(GET_TRAIN_CARRIAGE(viTrainIndex, 2)))
		
		vTempPosition[3] = GET_ENTITY_COORDS(GET_TRAIN_CARRIAGE(viTrainIndex, 3))
		viTempTrainCarriage[3] = CREATE_VEHICLE(FREIGHTCAR, vTempPosition[3], 0.0)
		SET_ENTITY_COORDS_NO_OFFSET(viTempTrainCarriage[3], vTempPosition[3])
		SET_ENTITY_ROTATION(viTempTrainCarriage[3], GET_ENTITY_ROTATION(GET_TRAIN_CARRIAGE(viTrainIndex, 3)))

		vTempPosition[4] = GET_ENTITY_COORDS(GET_TRAIN_CARRIAGE(viTrainIndex, 4))
		viTempTrainCarriage[4] = CREATE_VEHICLE(FREIGHTCONT2, vTempPosition[4], 0.0)
		SET_ENTITY_COORDS_NO_OFFSET(viTempTrainCarriage[4], vTempPosition[4])
		SET_ENTITY_ROTATION(viTempTrainCarriage[4], GET_ENTITY_ROTATION(GET_TRAIN_CARRIAGE(viTrainIndex, 4)))
		
		vTempPosition[5] = GET_ENTITY_COORDS(GET_TRAIN_CARRIAGE(viTrainIndex, 5))
		viTempTrainCarriage[5] = CREATE_VEHICLE(FREIGHTCONT2, vTempPosition[5], 0.0)
		SET_ENTITY_COORDS_NO_OFFSET(viTempTrainCarriage[5], vTempPosition[5])
		SET_ENTITY_ROTATION(viTempTrainCarriage[5], GET_ENTITY_ROTATION(GET_TRAIN_CARRIAGE(viTrainIndex, 5)))
		
		vTempPosition[6] = GET_ENTITY_COORDS(GET_TRAIN_CARRIAGE(viTrainIndex, 6))
		viTempTrainCarriage[6] = CREATE_VEHICLE(FREIGHTCONT2, vTempPosition[6], 0.0)
		SET_ENTITY_COORDS_NO_OFFSET(viTempTrainCarriage[6], vTempPosition[6])
		SET_ENTITY_ROTATION(viTempTrainCarriage[6], GET_ENTITY_ROTATION(GET_TRAIN_CARRIAGE(viTrainIndex, 6)))
		
		vTempPosition[7] = GET_ENTITY_COORDS(GET_TRAIN_CARRIAGE(viTrainIndex, 7))
		viTempTrainCarriage[7] = CREATE_VEHICLE(FREIGHTCAR, vTempPosition[7], 0.0)
		SET_ENTITY_COORDS_NO_OFFSET(viTempTrainCarriage[7], vTempPosition[7])
		SET_ENTITY_ROTATION(viTempTrainCarriage[7], GET_ENTITY_ROTATION(GET_TRAIN_CARRIAGE(viTrainIndex, 7)))
		
		vTempPosition[8] = GET_ENTITY_COORDS(GET_TRAIN_CARRIAGE(viTrainIndex, 8))
		viTempTrainCarriage[8] = CREATE_VEHICLE(FREIGHTCAR, vTempPosition[8], 0.0)
		SET_ENTITY_COORDS_NO_OFFSET(viTempTrainCarriage[8], vTempPosition[8])
		SET_ENTITY_ROTATION(viTempTrainCarriage[8], GET_ENTITY_ROTATION(GET_TRAIN_CARRIAGE(viTrainIndex, 8)))
		DELETE_MISSION_TRAIN(viTrainIndex)
		
	ENDIF
	
	FREEZE_ENTITY_POSITION(viTempTrainCarriage[0], TRUE)
	FREEZE_ENTITY_POSITION(viTempTrainCarriage[1], TRUE)
	FREEZE_ENTITY_POSITION(viTempTrainCarriage[2], TRUE)
	FREEZE_ENTITY_POSITION(viTempTrainCarriage[3], TRUE)
	FREEZE_ENTITY_POSITION(viTempTrainCarriage[4], TRUE)
	FREEZE_ENTITY_POSITION(viTempTrainCarriage[5], TRUE)
	FREEZE_ENTITY_POSITION(viTempTrainCarriage[6], TRUE)
	FREEZE_ENTITY_POSITION(viTempTrainCarriage[7], TRUE)
	FREEZE_ENTITY_POSITION(viTempTrainCarriage[8], TRUE)
	
ENDPROC


//Author: Ross Wallace
//PURPOSE: Jumps to an absolute time in a vehicle recording
PROC SET_VEHICLE_RECORDING_PLAYBACK_POSITION(VEHICLE_INDEX thisVehicle, FLOAT positionToSet)

	IF NOT IS_ENTITY_DEAD(thisVehicle)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(thisVehicle)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(thisVehicle, (GET_TIME_POSITION_IN_RECORDING(thisVehicle) * -1 ) + positionToSet )
		ELSE
			SCRIPT_ASSERT("SET_VEHICLE_RECORDING_PLAYBACK_POSITION: Playback not going on for Vehicle!")
		ENDIF
	ELSE
		//SCRIPT_ASSERT("SET_VEHICLE_RECORDING_PLAYBACK_POSITION: Vehicle Dead!")
	ENDIF
	
ENDPROC


//Author: Ross Wallace
//PURPOSE: Jumps to the percentage progress point of a car recording...
PROC SET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(VEHICLE_INDEX thisVehicle, FLOAT percentage, INT recordingNumber, STRING RecName)

	IF HAS_VEHICLE_RECORDING_BEEN_LOADED(recordingNumber, RecName)
		IF percentage >= 0.0 AND percentage <= 100.00
			SET_VEHICLE_RECORDING_PLAYBACK_POSITION(thisVehicle,  ( GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(recordingNumber, RecName) / 100.00 )  * percentage  )
		#IF IS_DEBUG_BUILD
		ELSE
			SCRIPT_ASSERT("SET_VEHICLE_RECORDING_PLAYBACK_POSITION: Supplied Percentage out of range!!")
			PRINTSTRING("SET_VEHICLE_RECORDING_PLAYBACK_POSITION: Supplied Percentage out of range!!")
		#ENDIF
		ENDIF
	ELSE
		SCRIPT_ASSERT("Car rec not loaded")
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_MICHAEL()

	RETURN GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
	
ENDFUNC

FUNC BOOL IS_PLAYER_FRANKLIN()

	RETURN GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
	
ENDFUNC

FUNC BOOL IS_PLAYER_TREVOR()

	RETURN GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
	
ENDFUNC



FUNC BOOL IS_PLAYER_MICHAEL_OR_FRANKLIN()

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
	OR GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

VECTOR vEndOfTracks = <<2767.2104, 2824.7864, 36.5910>>

PROC MANAGE_SKIP(MISSION_STAGE_FLAG to_this_mission_stage, BOOL bIsReplay)
	
	#IF IS_DEBUG_BUILD
		iReturnStage = ENUM_TO_INT(to_this_mission_stage)
	#ENDIF
	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	PRINTLN("-----------------------------------------")
	PRINTLN("BS PREP D MANAGE_SKIP selecting:",  iReturnStage)
	PRINTLN("-----------------------------------------")
	
	//Do additional work when selecting stage
	SWITCH to_this_mission_stage
			
		CASE STAGE_GET_TO_TRAIN_SWITCH
			IF bIsReplay
				START_REPLAY_SETUP(<<2610.2693, 2912.1846, 39.4156>>, 322.0601)		
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2610.2693, 2912.1846, 39.4156>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 322.0601)
			ENDIF
			
			TRIGGER_MUSIC_EVENT("FHPRD_RESTART_1")
			
			END_REPLAY_SETUP()
		BREAK
		
		CASE STAGE_WAIT_FOR_TRAIN
			IF bIsReplay
				START_REPLAY_SETUP( <<2632.8030, 2933.4268, 43.7442>>, 52.5638)		
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2632.8030, 2933.4268, 43.7442>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 52.5638)
			ENDIF
			
			TRIGGER_MUSIC_EVENT("FHPRD_RESTART_2")
			
			END_REPLAY_SETUP()
		BREAK
		
		CASE STAGE_TRAIN_COMING_INTO_SIDINGS_CUTSCENE
			IF bIsReplay
				START_REPLAY_SETUP( <<2632.8030, 2933.4268, 43.7442>>, 52.5638)		
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2632.8030, 2933.4268, 43.7442>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 52.5638)
			ENDIF
			END_REPLAY_SETUP()
			
			TRIGGER_MUSIC_EVENT("FHPRD_RESTART_2")
			
		BREAK

		CASE STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING
		
			IF bIsReplay
				START_REPLAY_SETUP(<<2628.1152, 2924.5671, 39.4265>>,  154.8777)		
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2628.1152, 2924.5671, 39.4265>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 154.8777)
			ENDIF
			
			REQUEST_STAGE_ASSETS(STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING)
			WHILE NOT HAVE_STAGE_ASSETS_LOADED(STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING)
				WAIT(0)
			ENDWHILE
			
			CREATE_SKYLIFT()
			CREATE_TRAIN_AND_DRIVER(<<2617.8606, 2934.2769, 38.8511>>)
			
			IF	IS_PLAYER_MICHAEL_OR_FRANKLIN()
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(pedSelector.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(1, 3000.0, "BsPrepHeli"))
					WAIT(0)
				ENDWHILE
				IF NOT IS_ENTITY_DEAD(viSkyLift)			
					SET_PED_INTO_VEHICLE(pedSelector.pedID[SELECTOR_PED_TREVOR], viSkylift)
				ENDIF
			ELSE
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(pedSelector.pedID[pedSelector.ePreviousSelectorPed], CHAR_MICHAEL, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(1, 3000.0, "BsPrepHeli"))
					WAIT(0)
				ENDWHILE
				IF NOT IS_ENTITY_DEAD(viSkyLift)			
					SET_PED_INTO_VEHICLE(pedSelector.pedID[pedSelector.ePreviousSelectorPed], viSkylift)
				ENDIF
			ENDIF
			
			END_REPLAY_SETUP()	
			
			
			SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_RUN, 3000, 0.0, TRUE, TRUE)
			FORCE_PED_MOTION_STATE(player_ped_id(), MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT)
					
			TRIGGER_MUSIC_EVENT("FHPRD_RESTART_2")
		BREAK
		
		
		CASE STAGE_FLY_AWAY_WITH_CARRIAGE
		
			IF g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 0
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					WAIT(0)
				ENDWHILE
			ELIF g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 1
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					WAIT(0)
				ENDWHILE
			ELIF g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 2
				WHILE NOT SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
					WAIT(0)
				ENDWHILE
			ENDIF
			
		
			IF bIsReplay
				START_REPLAY_SETUP( <<1773.3335, 3243.5676, 41.1441>>, 282.4375)		
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1773.3335, 3243.5676, 41.1441>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(),  282.4375)
			ENDIF
			
			CLEAR_AREA(<<1773.3335, 3243.5676, 41.1441>>, 40.0, TRUE)
			
			REQUEST_STAGE_ASSETS(STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING)
			WHILE NOT HAVE_STAGE_ASSETS_LOADED(STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING)
				WAIT(0)
			ENDWHILE
			
			CREATE_SKYLIFT()
			CREATE_TRAIN_AND_DRIVER(<<2761.5403, 2821.7625, 35.5180>>, 0.0)
		
			SET_TRAIN_CRUISE_SPEED(viTrainIndex, 0.0)
			SET_TRAIN_SPEED(viTrainIndex, 0.0)	
			
			IF	IS_PLAYER_MICHAEL_OR_FRANKLIN()
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(pedSelector.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(1, 3000.0, "BsPrepHeli"))
					WAIT(0)
				ENDWHILE
			ELSE
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(pedSelector.pedID[pedSelector.ePreviousSelectorPed], CHAR_MICHAEL, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(1, 3000.0, "BsPrepHeli"))
					WAIT(0)
				ENDWHILE				
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(viSkyLift)			
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), viSkylift)
				//SET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(viSkylift, 95.0, 1, "BsPrepHeli")
				//SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(viSkylift)
			ENDIF
			
					
			//CREATE_TRAIN_AND_DRIVER(vEndOfTracks)
			CREATE_SEPARATE_TRAIN_MODELS_FROM_TRAIN()
			
			TRIGGER_MUSIC_EVENT("FHPRD_RESTART_3")
			
			END_REPLAY_SETUP(viSkyLift)			
		BREAK
		
		DEFAULT
			
		BREAK
		
	ENDSWITCH
	
	
	
	RESET_GAME_CAMERA()
	
ENDPROC

MISSION_STAGE_FLAG skip_mission_stage	//For debug and replays.

REL_GROUP_HASH relGroupRailWay

PROC initialiseMission()
	
	IF IS_REPEAT_PLAY_ACTIVE()
	
		SET_MISSION_FLOW_INT_VALUE(FLOWINT_HEIST_CHOICE_FINALE, HEIST_CHOICE_FINALE_HELI)
	
		g_savedGlobals.sHeistData.eSelectedCrew[HEIST_CHOICE_FINALE_HELI][0] = CM_DRIVER_B_KARIM
		g_savedGlobals.sHeistData.eSelectedCrew[HEIST_CHOICE_FINALE_HELI][1] = CM_DRIVER_G_EDDIE
	ENDIF
		
	INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FINPD_TIME)
	
	SET_PLAYER_PED_AVAILABLE(CHAR_MICHAEL, TRUE)
	SET_PLAYER_PED_AVAILABLE(CHAR_FRANKLIN, TRUE)
	SET_PLAYER_PED_AVAILABLE(CHAR_TREVOR, TRUE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_M, TRUE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_F, TRUE)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_PLAYER_PED_INTRODUCED_T, TRUE)

	//SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
	
	
	UPDATE_DYNAMIC_VEHICLE_GEN_POSITION(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY, <<1702.9482, 3272.0217, 40.1539>>, 247.3247)
	DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY)
	CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY)
	
//	Took this out to test the mission triggering seamlessly. -BenR
//	SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2610.2693, 2912.1846, 39.4156>>)
//	SET_ENTITY_HEADING(PLAYER_PED_ID(), 322.0601)
	
	ADD_SCENARIO_BLOCKING_AREA(<<2595.1243, 2807.5581, 30.7274>>, <<2649.7778, 2988.8291, 53.4642>>)
	SET_PED_NON_CREATION_AREA(<<2595.1243, 2807.5581, 30.7274>>, <<2649.7778, 2988.8291, 53.4642>>)
	
	CLEAR_AREA_OF_PEDS(<<2627.9658, 2941.1003, 39.4282>>, 30.0)
	
	IF IS_PLAYER_MICHAEL_OR_FRANKLIN()
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	ELSE
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P2_HEADSET)
	ENDIF
	
	SET_BUILDING_STATE(BUILDINGNAME_IPL_BIG_SCORE_RAIL_TANKCAR, BUILDINGSTATE_DESTROYED)
	SET_RANDOM_TRAINS(FALSE) 
		
	REMOVE_RELATIONSHIP_GROUP(relGroupRailWay)
	ADD_RELATIONSHIP_GROUP("RAILWAY", relGroupRailWay)

	SET_PED_RELATIONSHIP_GROUP_HASH(player_ped_id(), RELGROUPHASH_PLAYER)

	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_PLAYER, relGroupRailWay)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupRailWay, RELGROUPHASH_PLAYER)
	
	SET_WANTED_LEVEL_MULTIPLIER(0.35)
		
		
	DISABLE_TAXI_HAILING(TRUE)
	//SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
	
	REQUEST_ADDITIONAL_TEXT("BSPREP", MISSION_TEXT_SLOT)
	REQUEST_ADDITIONAL_TEXT("BSPRP", MISSION_DIALOGUE_TEXT_SLOT)
		
	DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
	
	//SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
	
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
		
	DISABLE_TAXI_HAILING(TRUE)
	
	IF IS_PLAYER_MICHAEL()
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, NULL, "TREVOR")
	ELIF IS_PLAYER_FRANKLIN()
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PLAYER_PED_ID(), "FRANKLIN")
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, NULL, "TREVOR")
	ELIF IS_PLAYER_TREVOR()
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, PLAYER_PED_ID(), "TREVOR")
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, NULL, "MICHAEL")
	ENDIF
	
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID()) 
	
	mission_stage = STAGE_GET_TO_TRAIN_SWITCH
	
	IF (Is_Replay_In_Progress())
		
		MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_FINALE_PREP_D)
		
		//ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
		
		//SET_CLOCK_TIME(16, 00, 00)
			
        // Your mission is being replayed
        INT myStage = Get_Replay_Mid_Mission_Stage() 
        
		//Use the myStage variable to restart your mission at the correct stage
        //Warp the player to appropriate start coordinates (because the player won’t be at the contact point)
    	//Fade In (the game gets faded out when a replay is selected)
		
		SWITCH myStage
		
		
			CASE 0
				skip_mission_stage = STAGE_GET_TO_TRAIN_SWITCH
			BREAK		
		
			CASE 1
				skip_mission_stage = STAGE_WAIT_FOR_TRAIN			
			BREAK		
			
			CASE 2
				skip_mission_stage = STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING
			BREAK		
			
			CASE 3
				skip_mission_stage = STAGE_FLY_AWAY_WITH_CARRIAGE
			BREAK		
				
		ENDSWITCH
			
		//skip_mission_stage = INT_TO_ENUM(MISSION_STAGE_FLAG, myStage)
		
		IF g_bShitskipAccepted = TRUE
            // player chose to shitskip
            // you need to skip to stage after Get_Replay_Mid_Mission_Stage()
			IF  skip_mission_stage = STAGE_GET_TO_TRAIN_SWITCH
				skip_mission_stage = STAGE_WAIT_FOR_TRAIN
			ELIF skip_mission_stage = STAGE_WAIT_FOR_TRAIN
				skip_mission_stage = STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING
			ELIF skip_mission_stage = STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING
				skip_mission_stage = STAGE_FLY_AWAY_WITH_CARRIAGE
			ELIF skip_mission_stage = STAGE_FLY_AWAY_WITH_CARRIAGE
				Mission_Passed()
			ENDIF
        ENDIF
		
		MANAGE_SKIP(skip_mission_stage, TRUE)
		mission_stage = skip_mission_stage
		DO_SCREEN_FADE_IN(500)
	ELSE
		IF IS_PLAYER_MICHAEL()
			g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 0
		ELIF IS_PLAYER_FRANKLIN()
			g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 1
		ELIF IS_PLAYER_TREVOR()
			g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 2
		ENDIF
        // Your mission is being played normally, not being replayed
    	//Set_Replay_Mid_Mission_Stage(0) 
	ENDIF
		
ENDPROC

#IF IS_DEBUG_BUILD

#ENDIF



#IF IS_DEBUG_BUILD

PROC debugRoutines()

			
ENDPROC

#ENDIF

PROC REMOVE_BLIP_AND_CHECK_IF_EXISTS(BLIP_INDEX &thisBlip)

	IF DOES_BLIP_EXIST(thisBlip)
		REMOVE_BLIP(thisBlip)
	ENDIF

ENDPROC

FUNC INT GET_REPLAY_STAGE_FROM_MISSION_STAGE(MISSION_STAGE_FLAG thisStage)
	
	INT iReplayNumber
	
	iReplayNumber = ENUM_TO_INT(thisStage) - ENUM_TO_INT(STAGE_GET_TO_TRAIN_SWITCH)
	
	IF iReplayNumber < 0
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("Sol5 - trying to set replay to a negative value")
		#ENDIF
		iReplayNumber = 0
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("****** --- GET_REPLAY_STAGE_FROM_MISSION_STAGE: ", iReplayNumber)
	#ENDIF
	
	RETURN iReplayNumber

ENDFUNC


//***************************************************************
//****************** MISSION STAGES *****************************
//***************************************************************

BLIP_INDEX blipviTrainIndex

FUNC BOOL HAS_PED_BEEN_TAZERED_BY_PED(PED_INDEX pedToTazer, PED_INDEX pedWithTazer)
	
	IF NOT IS_ENTITY_DEAD(pedToTazer)
		IF NOT IS_ENTITY_DEAD(pedWithTazer)
			WEAPON_TYPE wPed
			IF GET_CURRENT_PED_WEAPON(pedWithTazer, wPed)
				IF wPed = WEAPONTYPE_STUNGUN 
				
					if HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(pedToTazer, WEAPONTYPE_STUNGUN)
					and HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedToTazer, pedWithTazer)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

BLIP_INDEX blippedStation1
BLIP_INDEX blippedStation2

PED_INDEX pedStation1
PED_INDEX pedStation2
INT iControlped1
INT iControlped2

BOOL bWasPedJustKilled = TRUE
BOOL bWasPedJustKilled2 = TRUE
//SEQUENCE_INDEX seqGetIntoSmokingPosition

PROC stageGetToTrainSwitch()
		
	SWITCH i_current_event
		
		CASE 0		
		
			IF IS_PLAYER_MICHAEL()
				g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 0
			ELIF IS_PLAYER_FRANKLIN()
				g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 1
			ELIF IS_PLAYER_TREVOR()
				g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 2
			ENDIF
			
			IF IS_PLAYER_MICHAEL()
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, NULL, "TREVOR")
			ELIF IS_PLAYER_FRANKLIN()
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PLAYER_PED_ID(), "FRANKLIN")
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, NULL, "TREVOR")
			ELIF IS_PLAYER_TREVOR()
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, PLAYER_PED_ID(), "TREVOR")
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, NULL, "MICHAEL")
			ENDIF
			
			REQUEST_STAGE_ASSETS(STAGE_GET_TO_TRAIN_SWITCH)
			SET_RANDOM_TRAINS(FALSE) 
			i_current_event++
		BREAK
				
		CASE 1		
			IF HAVE_STAGE_ASSETS_LOADED(STAGE_GET_TO_TRAIN_SWITCH)
//				chopperArrival = CREATE_VEHICLE(BUZZARD, <<1699.3177, 3250.4324, 39.9546>>, 189.6397)
//				blipChopperArrival = CREATE_BLIP_FOR_VEHICLE(chopperArrival)

				IF NOT IS_AUDIO_SCENE_ACTIVE("BS_P_D_STEALTH")
					START_AUDIO_SCENE("BS_P_D_STEALTH")
				ENDIF
			
				IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
					pedStation1 = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<2628.9084, 2947.6255, 40.4280>>, 338.9562)//<<2628.9084, 2947.6255, 39.4280>>, 347.0879)
				ELSE
					pedStation1 = g_sTriggerSceneAssets.ped[0]
					SET_ENTITY_AS_MISSION_ENTITY(pedStation1, TRUE, TRUE)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
					pedStation2 = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<2632.8145, 2933.4819, 43.7436>>, 53.5715)
				ELSE
					pedStation2 = g_sTriggerSceneAssets.ped[1]
					SET_ENTITY_AS_MISSION_ENTITY(pedStation2, TRUE, TRUE)
				ENDIF
				
				MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_FINALE_PREP_D)
				
				IF NOT IS_ENTITY_DEAD(pedStation1)
					SET_ENTITY_HEALTH(pedStation1, 150)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedStation1, relGroupRailWay)
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, pedStation1, "CONSTRUCTION1")
					TASK_START_SCENARIO_AT_POSITION(pedstation1, "WORLD_HUMAN_SMOKING", <<2628.9084, 2947.6255, 40.4280>>, 347.0879)
					SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(pedStation1)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedStation1, TRUE)
					blippedStation1 = CREATE_BLIP_FOR_PED(pedStation1, TRUE)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(pedStation2)
					SET_ENTITY_HEALTH(pedStation2, 150)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedStation2, relGroupRailWay)
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedStation2, "CONSTRUCTION2")
					TASK_PLAY_ANIM(pedStation2, "missbigscoreprep", "idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedStation2, TRUE)
					blippedStation2 = CREATE_BLIP_FOR_PED(pedStation2, TRUE)
				ENDIF
				
				iControlped1 = 0
				iControlped2 = 0
				FADE_IN_IF_NEEDED()
				i_current_event++
			ENDIF
		BREAK
		
		CASE 2
			
			IF NOT IS_REPLAY_IN_PROGRESS()
				IF IS_PLAYER_MICHAEL()
					IF PLAYER_CALL_CHAR_CELLPHONE(myScriptedSpeech, CHAR_TREVOR, "BSPRP", "BSPRP_KILLG", CONV_PRIORITY_VERY_HIGH, TRUE,  DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, FALSE)
						REPLAY_RECORD_BACK_FOR_TIME(2.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
						i_current_event++
					ENDIF
				ELIF IS_PLAYER_FRANKLIN()
					IF PLAYER_CALL_CHAR_CELLPHONE(myScriptedSpeech, CHAR_TREVOR, "BSPRP", "BSPRP_KILLGF", CONV_PRIORITY_VERY_HIGH, TRUE,  DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, FALSE)
						REPLAY_RECORD_BACK_FOR_TIME(2.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
						i_current_event++
					ENDIF
				ELIF IS_PLAYER_TREVOR()
					IF PLAYER_CALL_CHAR_CELLPHONE(myScriptedSpeech, CHAR_MICHAEL, "BSPRP", "BSPRP_KILLGT", CONV_PRIORITY_VERY_HIGH, TRUE,  DISPLAY_SUBTITLES, DO_ADD_TO_BRIEF_SCREEN, FALSE)
						REPLAY_RECORD_BACK_FOR_TIME(2.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
						i_current_event++
					ENDIF
				ENDIF
			ELSE
				i_current_event++
			ENDIF
		
//			IF CREATE_CONVERSATION(myScriptedSpeech, "BSPRP", "BSPRP_KILLG", CONV_PRIORITY_VERY_HIGH)
//				i_current_event++
//			ENDIF
		BREAK
		
		CASE 3
		CASE 4
		
			IF i_current_event = 3
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										
					TRIGGER_MUSIC_EVENT("FHPRD_START")
					
					PRINT_NOW("KILLGUARDS", DEFAULT_GOD_TEXT_TIME, 1)
					i_current_event = 4
				ENDIF
			ENDIF
						
			IF IS_PED_INJURED(pedStation1)
			OR HAS_PED_BEEN_TAZERED_BY_PED(pedStation1, PLAYER_PED_ID())
				
				IF DOES_BLIP_EXIST(blippedStation1)
					REMOVE_BLIP_AND_CHECK_IF_EXISTS(blippedStation1)
					KILL_FACE_TO_FACE_CONVERSATION()
				
					IF NOT IS_PED_INJURED(pedStation1)
						APPLY_DAMAGE_TO_PED(pedStation1, 100, TRUE)
					ELSE
						INFORM_MISSION_STATS_OF_INCREMENT(FINPD_KILLS)
						IF WAS_PED_KILLED_BY_STEALTH(pedStation1)
						OR HAS_PED_BEEN_TAZERED_BY_PED(pedStation1, PLAYER_PED_ID())
							
							IF bWasPedJustKilled = FALSE
								REPLAY_RECORD_BACK_FOR_TIME(2.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
								bWasPedJustKilled = TRUE
							ENDIF
							
							INFORM_MISSION_STATS_OF_INCREMENT(FINPD_STEALTH_KILLS)
						ENDIF
					ENDIF
				ENDIF
			ELSE
			
				SWITCH iControlped1
				
					CASE 0
//						IF NOT IS_PED_INJURED(pedStation1)
//							ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, pedStation1, "CONSTRUCTION1")
//						ENDIF
						
						IF HAS_PED_RECEIVED_EVENT(pedStation1, EVENT_SHOT_FIRED)
							iControlped1 = 101
						ENDIF
					
						IF CAN_PED_SEE_HATED_PED(pedStation1, PLAYER_PED_ID())				
						OR (GET_DISTANCE_BETWEEN_ENTITIES(pedStation1, PLAYER_PED_ID()) < 10.5 AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID()))
							IF CREATE_CONVERSATION(myScriptedSpeech, "SOL1AUD", "SOL1_HEY", CONV_PRIORITY_VERY_HIGH)
								SETTIMERB(0)
								CLEAR_PED_TASKS(pedStation1)
								TASK_LOOK_AT_ENTITY(pedStation1, PLAYER_PED_ID(), INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
								TASK_TURN_PED_TO_FACE_ENTITY(pedStation1, PLAYER_PED_ID())
								iControlped1 = 1
							ENDIF
						ENDIF
					BREAK
				
					CASE 1
					
						IF HAS_PED_RECEIVED_EVENT(pedStation1, EVENT_SHOT_FIRED)
							iControlped1 = 101
						ENDIF
					
						IF TIMERB() > 5000
						AND CAN_PED_SEE_HATED_PED(pedStation1, PLAYER_PED_ID())
							TASK_USE_MOBILE_PHONE(pedStation1, TRUE)
							iControlped1++
						ENDIF
					BREAK
					
					CASE 2						
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL1AUD", "SOL1_COPS1", CONV_PRIORITY_VERY_HIGH)
							iControlped1++
						ENDIF
					BREAK
					
					CASE 3
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							SETTIMERB(0)
							iControlped1++
						ENDIF
					BREAK
					
					CASE 4
						
						IF TIMERB() > 2000
							MISSION_FLOW_SET_FAIL_REASON("FAILGUARD")
							mission_Failed()
						ENDIF
					BREAK
					
					CASE 101
						CLEAR_PED_TASKS(pedStation1)						
						TASK_SMART_FLEE_PED(pedStation1, PLAYER_PED_ID(), 50.0, INFINITE_TASK_TIME)
						iControlped1++
					BREAK
					
					CASE 102
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL1AUD", "SOL1_COPSIN1", CONV_PRIORITY_VERY_HIGH)
							iControlped1++
						ENDIF
					BREAK
				
					CASE 103
						IF NOT IS_ENTITY_AT_ENTITY(pedStation1, PLAYER_PED_ID(), <<30.0, 30.0, 30>>)
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							WAIT(2000)
							MISSION_FLOW_SET_FAIL_REASON("FAILGUARD")
							mission_Failed()
						ENDIF
					BREAK
				BREAK
				ENDSWITCH
					
			ENDIF
			
			IF IS_PED_INJURED(pedStation2)
			OR HAS_PED_BEEN_TAZERED_BY_PED(pedStation2, PLAYER_PED_ID())
				IF DOES_BLIP_EXIST(blippedStation2)
					REMOVE_BLIP_AND_CHECK_IF_EXISTS(blippedStation2)
					IF NOT IS_PED_INJURED(pedStation2)
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						APPLY_DAMAGE_TO_PED(pedStation2, 100, TRUE)
					ELSE
					
						IF bWasPedJustKilled2 = FALSE
							REPLAY_RECORD_BACK_FOR_TIME(2.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
							bWasPedJustKilled2 = TRUE
						ENDIF
					
						INFORM_MISSION_STATS_OF_INCREMENT(FINPD_KILLS) 
						IF WAS_PED_KILLED_BY_STEALTH(pedStation2)
						OR HAS_PED_BEEN_TAZERED_BY_PED(pedStation2, PLAYER_PED_ID())
							INFORM_MISSION_STATS_OF_INCREMENT(FINPD_STEALTH_KILLS)
						ENDIF
					ENDIF
				ENDIF
			ELSE
			
				SWITCH iControlped2
				
					CASE 0
						IF HAS_PED_RECEIVED_EVENT(pedStation2, EVENT_SHOT_FIRED)
						//OR HAS_PED_RECEIVED_EVENT(pedStation2, EVENT_FOOT_STEP_HEARD)
							iControlped2 = 101
						ENDIF
						
//						IF NOT IS_PED_INJURED(pedStation2)
//							ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 3, pedStation2, "CONSTRUCTION2")
//						ENDIF
						
						IF (CAN_PED_SEE_HATED_PED(pedStation2, PLAYER_PED_ID()) OR CAN_PED_HEAR_PLAYER(PLAYER_ID(), pedStation2))
						AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2629.506348,2935.470947,43.470905>>, <<2635.961670,2931.989746,47.003082>>, 4.500000)

						//AND GET_DISTANCE_BETWEEN_ENTITIES(pedStation2, PLAYER_PED_ID()) < 3.5
							IF CREATE_CONVERSATION(myScriptedSpeech, "SOL1AUD", "SOL1_CALL2", CONV_PRIORITY_VERY_HIGH)
								SETTIMERB(0)
								CLEAR_PED_TASKS(pedStation2)
								TASK_LOOK_AT_ENTITY(pedStation2, PLAYER_PED_ID(), INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
								TASK_TURN_PED_TO_FACE_ENTITY(pedStation2, PLAYER_PED_ID())
						
								TASK_USE_MOBILE_PHONE(pedStation2, TRUE)
								iControlped2 = 1
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF TIMERB() > 1000
							iControlped2 = 2
						ENDIF
					BREAK
					
					CASE 2
					
						IF HAS_PED_RECEIVED_EVENT(pedStation2, EVENT_SHOT_FIRED)
							iControlped2 = 101
						ENDIF
					
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						AND TIMERB() > 3000
							MISSION_FLOW_SET_FAIL_REASON("FAILGUARD")
							mission_Failed()
						ENDIF
					BREAK
					
					CASE 101
						CLEAR_PED_TASKS(pedStation2)
						TASK_SMART_FLEE_PED(pedSTation2, PLAYER_PED_ID(), 50.0, INFINITE_TASK_TIME)
						iControlped2++
					BREAK
				
					CASE 102
						IF CREATE_CONVERSATION(myScriptedSpeech, "SOL1AUD", "SOL1_COPSIN2", CONV_PRIORITY_VERY_HIGH)
							iControlped2++
						ENDIF
					BREAK
				
					CASE 103
						IF NOT IS_ENTITY_AT_ENTITY(pedSTation2, PLAYER_PED_ID(), <<30.0, 30.0, 30>>)
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							WAIT(2000)
							MISSION_FLOW_SET_FAIL_REASON("FAILGUARD")
							mission_Failed()
						ENDIF
					BREAK
				
				ENDSWITCH
				
			ENDIF
			
			IF (IS_PED_INJURED(pedStation1) OR HAS_PED_BEEN_TAZERED_BY_PED(pedStation1, PLAYER_PED_ID()))
			AND (IS_PED_INJURED(pedStation2) OR HAS_PED_BEEN_TAZERED_BY_PED(pedStation2, PLAYER_PED_ID()))
			
				REMOVE_BLIP(blippedStation1)
				REMOVE_BLIP(blippedStation2)
				
				REMOVE_ANIM_DICT("missbigscoreprep")
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_CONSTRUCT_01)
				SET_PED_AS_NO_LONGER_NEEDED(pedStation1)
				SET_PED_AS_NO_LONGER_NEEDED(pedStation2)
				
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				
				i_current_event = 0
				mission_stage = STAGE_WAIT_FOR_TRAIN
			ENDIF
		BREAK
				
	ENDSWITCH
	
ENDPROC


FUNC BOOL IS_TRAIN_OVER_SIDINGS()

	IF DOES_ENTITY_EXIST(viTrainIndex)
		IF DOES_ENTITY_EXIST(GET_TRAIN_CARRIAGE(viTrainIndex, 0))
		AND DOES_ENTITY_EXIST(GET_TRAIN_CARRIAGE(viTrainIndex, 1))
		AND DOES_ENTITY_EXIST(GET_TRAIN_CARRIAGE(viTrainIndex, 2))
		AND DOES_ENTITY_EXIST(GET_TRAIN_CARRIAGE(viTrainIndex, 3))
		AND DOES_ENTITY_EXIST(GET_TRAIN_CARRIAGE(viTrainIndex, 4))
			IF IS_ENTITY_AT_COORD(viTrainIndex, <<2644.0293, 2974.4802, 39.5441>>, <<10.0, 10.0, 10.0>>)
			OR IS_ENTITY_AT_COORD(GET_TRAIN_CARRIAGE(viTrainIndex, 0), <<2644.0293, 2974.4802, 39.5441>>, <<20.0, 20.0, 10.0>>)
			OR IS_ENTITY_AT_COORD(GET_TRAIN_CARRIAGE(viTrainIndex, 1), <<2644.0293, 2974.4802, 39.5441>>, <<20.0, 20.0, 10.0>>)
			OR IS_ENTITY_AT_COORD(GET_TRAIN_CARRIAGE(viTrainIndex, 2), <<2644.0293, 2974.4802, 39.5441>>, <<20.0, 20.0, 10.0>>)
			OR IS_ENTITY_AT_COORD(GET_TRAIN_CARRIAGE(viTrainIndex, 3), <<2644.0293, 2974.4802, 39.5441>>, <<20.0, 20.0, 10.0>>)
			OR IS_ENTITY_AT_COORD(GET_TRAIN_CARRIAGE(viTrainIndex, 4), <<2644.0293, 2974.4802, 39.5441>>, <<50.0, 50.0, 10.0>>)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL CREATE_CONVO_FOR_PLAYER_ON_GROUND(STRING strMichael, STRING strFranklin, STRING strTrevor)

	IF pedSelector.ePreviousSelectorPed = SELECTOR_PED_MICHAEL
		RETURN CREATE_CONVERSATION(myScriptedSpeech, "BSPRP", strMichael, CONV_PRIORITY_VERY_HIGH)
	ELIF pedSelector.ePreviousSelectorPed = SELECTOR_PED_FRANKLIN
		RETURN CREATE_CONVERSATION(myScriptedSpeech, "BSPRP", strFranklin, CONV_PRIORITY_VERY_HIGH)
	ELIF pedSelector.ePreviousSelectorPed = SELECTOR_PED_TREVOR
		RETURN CREATE_CONVERSATION(myScriptedSpeech, "BSPRP", strTrevor, CONV_PRIORITY_VERY_HIGH)
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL CREATE_CONVO_FOR_PLAYER_CURRENT(STRING strMichael, STRING strFranklin, STRING strTrevor)

	IF IS_PLAYER_MICHAEL()
		RETURN CREATE_CONVERSATION(myScriptedSpeech, "BSPRP", strMichael, CONV_PRIORITY_VERY_HIGH)
	ELIF IS_PLAYER_FRANKLIN()
		RETURN CREATE_CONVERSATION(myScriptedSpeech, "BSPRP", strFranklin, CONV_PRIORITY_VERY_HIGH)
	ELIF IS_PLAYER_TREVOR()
		RETURN CREATE_CONVERSATION(myScriptedSpeech, "BSPRP", strTrevor, CONV_PRIORITY_VERY_HIGH)
	ENDIF

	RETURN FALSE

ENDFUNC

BLIP_INDEX blipTrain
BOOL bSidingSwitched = FALSE
BOOL bSidingDialoguePlayed
INT iQuickSidingsCut

OBJECT_INDEX oiRail01
OBJECT_INDEX oiRail02
VECTOR scenePositionLever
VECTOR sceneRotationLever
INT sceneIdLever
INT sceneIdLeverObject

FLOAT fRailHeading

INT iTrainInterval = 15000

PROC stageWaitForTrain()

	SWITCH i_current_event
		
		CASE 0
			
		
			IF iControlped1 = 0
			AND iControlped2 = 0
				//FINPD_UNDETECTED 
				INFORM_MISSION_STATS_OF_INCREMENT(FINPD_UNDETECTED, 1)
			ENDIF
		
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "STAGE_WAIT_FOR_TRAIN")
			IF IS_PLAYER_MICHAEL()
				g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 0
			ELIF IS_PLAYER_FRANKLIN()
				g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 1
			ELIF IS_PLAYER_TREVOR()
				g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 2
			ENDIF
			
			IF IS_PLAYER_MICHAEL()
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, NULL, "TREVOR")
			ELIF IS_PLAYER_FRANKLIN()
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PLAYER_PED_ID(), "FRANKLIN")
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, NULL, "TREVOR")
			ELIF IS_PLAYER_TREVOR()
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, PLAYER_PED_ID(), "TREVOR")
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, NULL, "MICHAEL")
			ENDIF
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("BS_P_D_STEALTH")
				START_AUDIO_SCENE("BS_P_D_STEALTH")
			ENDIF
			iQuickSidingsCut = 0
			REQUEST_STAGE_ASSETS(STAGE_WAIT_FOR_TRAIN)
			bSidingDialoguePlayed = FALSE
			bSidingSwitched = FALSE
			i_current_event++
		BREAK
		
		CASE 1
			IF HAVE_STAGE_ASSETS_LOADED(STAGE_WAIT_FOR_TRAIN)				
				/* START SYNCHRONIZED SCENE -  */
				scenePositionLever = <<2632.0591,2934.0509,43.7052>>//<<2631.9919, 2934.0911, 43.6900>>//<< 2631.992, 2934.091, 43.690 >>
				sceneRotationLever = << 0.000, -0.000, 58.53>>
				sceneIdLeverObject = CREATE_SYNCHRONIZED_SCENE(scenePositionLever, sceneRotationLever)
				SET_SYNCHRONIZED_SCENE_LOOPED(sceneIdLeverObject, FALSE)
				SET_SYNCHRONIZED_SCENE_PHASE(sceneIdLeverObject, 0.0)
				SET_SYNCHRONIZED_SCENE_RATE(sceneIdLeverObject, 0.0)
				PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<2632.0591,2934.0509,43.7052>>, 1.0, p_rail_controller_s , sceneIdLeverObject, "pull_level_panel", "missbigscoreprepd", INSTANT_BLEND_IN)
				
				FADE_IN_IF_NEEDED()
				i_current_event++			
			ENDIF
		BREAK
		
		CASE 2
			//IF CREATE_CONVERSATION(myScriptedSpeech, "BSPRP", "BSPRP_DEAD", CONV_PRIORITY_VERY_HIGH)
			IF CREATE_CONVO_FOR_PLAYER_CURRENT("BSPRP_DEAD", "BSPRP_DEADF", "BSPRP_DEADT")
				i_current_event++
			ENDIF
		BREAK
		
		CASE 3
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				//IF CREATE_CONVO_FOR_PLAYER_CURRENT("BSPRP_TINCOM", "BSPRP_TINCOM", "BSPRP_COME")
					i_current_event++
				//ENDIF
			ENDIF
		BREAK
			
		CASE 4
			IF IS_PLAYER_AT_LOCATION_ON_FOOT(sLocatesData, <<2631.9299, 2934.0813, 45.7436>>, <<1.5, 1.5, 2.5>>, FALSE, "GOTOBOX")
				i_current_event++	
			ENDIF
		BREAK
		
		CASE 5
		CASE 6
		
			//Trains spawn every 30 seconds...			
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND TIMERA() > iTrainInterval //30000
			AND NOT IS_COORD_IN_PLAYER_SNIPER_SCOPE(<<2742.8098, 3121.8733, 44.8242>>)
			AND GET_DISTANCE_BETWEEN_COORDS(<<2742.8098, 3121.8733, 44.8242>>, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 80.0
			//AND NOT IS_SPHERE_VISIBLE(<<2742.8098, 3121.8733, 44.8242>>, 2.0)
			AND bSidingSwitched
			AND i_current_event = 5
			
				REQUEST_STAGE_ASSETS(STAGE_TRAIN_COMING_INTO_SIDINGS_CUTSCENE)
			
				iTrainInterval = GET_RANDOM_INT_IN_RANGE(30000, 40000)
			
				SET_RANDOM_TRAINS(FALSE) 
				DELETE_MISSION_TRAIN(viTrainIndex)
				viTrainIndex = CREATE_MISSION_TRAIN(7,  <<2928.8259, 3572.7749, 54.0699>>, TRUE)//<<2742.8098, 3121.8733, 44.8242>>,  TRUE)	
				SET_TRAIN_CRUISE_SPEED(viTrainIndex, 27.0)
				SET_TRAIN_SPEED(viTrainIndex, 27.0)
				
				SET_ENTITY_LOD_DIST(viTrainIndex, 3000)
				SET_VEHICLE_LOD_MULTIPLIER(viTrainIndex, 100.0)
				
				blipTrain = CREATE_BLIP_FOR_VEHICLE(viTrainIndex, FALSE)
				//SET_BLIP_COLOUR(blipTrain, BLIP_COLOUR_GREEN)
				
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), viTrainIndex, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
				
				i_current_event = 6				
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(viTrainIndex)
				IF IS_ENTITY_AT_COORD(viTrainIndex, <<2644.0293, 2974.4802, 39.5441>>, <<10.0, 10.0, 10.0>>)
				AND i_current_event = 6
				
					IF bSidingSwitched
						//If player has switch lines
						i_current_event = 7
					ELSE
						//SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(viTrainIndex)
						REMOVE_BLIP(blipTrain)
						SETTIMERA(0)
						i_current_event = 5
					ENDIF
				ENDIF
			ENDIF
		BREAK
				
		CASE 7
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)				
				i_current_event = 0
				mission_stage = STAGE_TRAIN_COMING_INTO_SIDINGS_CUTSCENE
			ENDIF
		BREAK
		
	ENDSWITCH
	
	IF i_current_event >= 3
			
		IF bSidingSwitched = FALSE
			IF IS_PLAYER_AT_LOCATION_ON_FOOT(sLocatesData, <<2631.9299, 2935.0813, 45.7436>>, <<0.1, 0.1, 1.1>>, FALSE, "GOTOBOX")
			OR (DOES_BLIP_EXIST(sLocatesData.LocationBlip) AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<2632.733154,2934.567627,43.719173>>, <<2631.923340,2933.234863,45.469173>>, 1.900000))			
			
				IF CAN_PLAYER_START_CUTSCENE()
				AND NOT IS_ENTITY_ON_FIRE(PLAYER_PED_ID())
				
					IF IS_MESSAGE_BEING_DISPLAYED()
					AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						CLEAR_PRINTS()
					ENDIF
					
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP("FLIPSWITCH")
					ENDIF
									
					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
						IF IS_TRAIN_OVER_SIDINGS()
							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
								PRINT_HELP("NOFLIP")
							ENDIF
						ELSE
							CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
							SET_CONTROL_SHAKE(PLAYER_CONTROL, 500, 200)
							bSidingSwitched = TRUE
						ENDIF
					ENDIF
				ELSE
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
				ENDIF
			ELSE
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
				ENDIF
			ENDIF
		ENDIF
	
		
		SWITCH iQuickSidingsCut
		
			CASE 0
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
				REPLAY_START_EVENT()
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				iQuickSidingsCut++
			BREAK
		
			CASE 1
				IF bSidingSwitched = TRUE
					
					DESTROY_ALL_CAMS()
					initialCam= CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
					destinationCam= CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
					
					SET_CAM_COORD(initialCam,<<2632.582275,2935.789063,44.805943>>)
					SET_CAM_ROT(initialCam,<<8.405854,-1.193160,174.702866>>)
					SET_CAM_FOV(initialCam,26.778191)

					SET_CAM_COORD(destinationCam,<<2632.622314,2936.221191,44.741833>>)
					SET_CAM_ROT(destinationCam,<<8.405854,-1.193160,174.702866>>)
					SET_CAM_FOV(destinationCam,26.778191)

					SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 4000, GRAPH_TYPE_ACCEL, GRAPH_TYPE_LINEAR)
					
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE) 
					
					CLEAR_AREA_OF_VEHICLES(<< 2631.992, 2934.091, 43.690 >>, 5.0)
					CLEAR_AREA_OF_PROJECTILES(<< 2631.992, 2934.091, 43.690 >>, 5.0)
					STOP_FIRE_IN_RANGE(<< 2631.992, 2934.091, 43.690 >>, 8)			
					
					SET_CUTSCENE_RUNNING(TRUE)
					
					/* START SYNCHRONIZED SCENE -  */
					scenePositionLever = <<2632.0591,2934.0509,43.7052>>
					sceneRotationLever = << 0.000, -0.000, 58.53>>
					sceneIdLever = CREATE_SYNCHRONIZED_SCENE(scenePositionLever, sceneRotationLever)
					
					TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sceneIdLever, "missbigscoreprepd", "pull_level_michael", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_NONE, RBF_NONE)
					SET_SYNCHRONIZED_SCENE_LOOPED(sceneIdLever, FALSE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					
					SET_SYNCHRONIZED_SCENE_RATE(sceneIdLeverObject, 1.0)
					
					iQuickSidingsCut++
				ENDIF
			BREAK
		
			CASE 2
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIdLever) >= 0.8
				
					sceneIdLeverObject = CREATE_SYNCHRONIZED_SCENE(scenePositionLever, sceneRotationLever)
					
					SET_SYNCHRONIZED_SCENE_LOOPED(sceneIdLeverObject, TRUE)
					PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<2632.059,2934.051,43.705>>, 1.0, p_rail_controller_s, sceneIdLeverObject, "control_panel_loop_panel", "missbigscoreprepd", INSTANT_BLEND_IN)
					
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					iQuickSidingsCut++
				ENDIF
			BREAK
		
			CASE 3
				IF bSidingSwitched = TRUE	
				
					SET_BUILDING_STATE(BUILDINGNAME_IPL_BIG_SCORE_RAILS_1,	BUILDINGSTATE_DESTROYED)
					SET_BUILDING_STATE(BUILDINGNAME_IPL_BIG_SCORE_RAILS_2,	BUILDINGSTATE_DESTROYED)
					
					//to remove and BUILDINGSTATE_NORMAL to reset.
				
				
					oiRail01 = CREATE_OBJECT_NO_OFFSET(Prop_LD_Rail_01, <<2626.24854, 2948.61646, 39.25363>>)
					oiRail02 = CREATE_OBJECT_NO_OFFSET(Prop_LD_Rail_02, <<2625.44604, 2949.19409, 39.25363>>)
					
					FREEZE_ENTITY_POSITION(oiRail01, TRUE)
					FREEZE_ENTITY_POSITION(oiRail02, TRUE)
					
					IF NOT IS_AUDIO_SCENE_ACTIVE("BS_P_D_SWITCH_TRACKS")
						START_AUDIO_SCENE("BS_P_D_SWITCH_TRACKS")
					ENDIF
										
					SET_CAM_COORD(initialCam,<<2622.918213,2948.420898,40.631676>>)
					SET_CAM_ROT(initialCam,<<-19.427151,0.000000,-64.464088>>)
					SET_CAM_FOV(initialCam,41.755051)

					SET_CAM_COORD(destinationCam,<<2623.316650,2948.611572,40.475819>>)
					SET_CAM_ROT(destinationCam,<<-19.427151,0.000000,-64.464088>>)
					SET_CAM_FOV(destinationCam,41.755051)

										
					SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 4000, GRAPH_TYPE_ACCEL, GRAPH_TYPE_LINEAR)
					SETTIMERA(0)
					SETTIMERB(0)
					
					PLAY_SOUND_FROM_ENTITY(-1, "TRAIN_SWITCH_TRACKS_MASTER", oiRail01)
					
					STOP_FIRE_IN_RANGE(<<2622.918213,2948.420898,40.631676>>, 100.0)
					
					iQuickSidingsCut++
				ENDIF
			BREAK
			
			CASE 4
			CASE 5
				
				IF TIMERB() > 500
					IF fRailHeading < 3.0
						fRailHeading = fRailHeading +@ 1.375//2.75
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(oiRail01)
					SET_ENTITY_HEADING(oiRail01, fRailHeading)
				ENDIF
				IF DOES_ENTITY_EXIST(oiRail02)
					SET_ENTITY_HEADING(oiRail02, fRailHeading)
				ENDIF
					
				IF iQuickSidingsCut = 4
					IF PREPARE_MUSIC_EVENT("FHPRD_SIDINGS")
						TRIGGER_MUSIC_EVENT("FHPRD_SIDINGS")
						iQuickSidingsCut =5
					ENDIF
				ENDIF
							
				IF TIMERB() > 4000
					iQuickSidingsCut = 6
				ENDIF
				
			BREAK
			
			CASE 6
				IF IS_AUDIO_SCENE_ACTIVE("BS_P_D_SWITCH_TRACKS")
					STOP_AUDIO_SCENE("BS_P_D_SWITCH_TRACKS")
				ENDIF
				SET_CUTSCENE_RUNNING(FALSE)
				REPLAY_STOP_EVENT()
				iQuickSidingsCut++
			BREAK
			
			CASE 7
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)	
				AND bSidingDialoguePlayed = TRUE
					PRINT_NOW("BSPD_WAIT", DEFAULT_GOD_TEXT_TIME, 1)
				
					iQuickSidingsCut++
				ENDIF
			BREAK
			
		
		ENDSWITCH
	
			
		IF bSidingSwitched = TRUE
		AND bSidingDialoguePlayed = FALSE
			//IF CREATE_CONVERSATION(myScriptedSpeech, "BSPRP", "BSPRP_FLIPED", CONV_PRIORITY_VERY_HIGH)
			IF CREATE_CONVO_FOR_PLAYER_CURRENT("BSPRP_FLIPED", "BSPRP_FLIPF", "BSPRP_FLIPT")
				bSidingDialoguePlayed = TRUE
			ENDIF
		ENDIF
		
	ENDIF
	
	
ENDPROC


//SEQUENCE_INDEX seqUnHookTrain
//VECTOR vTrainDecouplePoint1 = <<2758.0913, 2821.3860, 35.4878>>
//VECTOR vTrainDecouplePoint2 = <<2747.9753, 2820.1262, 35.3281>>
PROC stageTrainComingIntoSiding()

	SWITCH i_current_event
		
		CASE 0
		
			IF NOT IS_AUDIO_SCENE_ACTIVE("BS_P_D_STEALTH")
				START_AUDIO_SCENE("BS_P_D_STEALTH")
			ENDIF
			REQUEST_STAGE_ASSETS(STAGE_TRAIN_COMING_INTO_SIDINGS_CUTSCENE)
						
			NEW_LOAD_SCENE_START(<<2384.677002,2237.905762,89.195038>>,	NORMALISE_VECTOR(<<2384.677002,2237.905762,89.195038>> - <<2618.867676,2959.865479,39.891586>>), 1000.0)
			i_current_event++
		BREAK
		
		CASE 1
			IF HAVE_STAGE_ASSETS_LOADED(STAGE_TRAIN_COMING_INTO_SIDINGS_CUTSCENE)				
				i_current_event++			
			ENDIF
		BREAK
		
		CASE 2
			
			//Camera Position = <<599.2651, 3165.6875, 43.4924>>, <<6.4827, 0.0000, 60.4199>>
			//Camera FOV = 43.1942
			DESTROY_ALL_CAMS()
			initialCam= CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
			destinationCam= CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
				
			IF NOT IS_AUDIO_SCENE_ACTIVE("BS_P_D_TRAIN_ARRIVES")
				START_AUDIO_SCENE("BS_P_D_TRAIN_ARRIVES")
			ENDIF
			
				SET_CAM_COORD(initialCam,<<2624.928467,2947.425049,39.319672>>)
				SET_CAM_ROT(initialCam,<<14.969305,-0.237041,-31.326054>>)
				SET_CAM_FOV(initialCam,45.000000)

				SET_CAM_COORD(destinationCam,<<2624.928467,2947.425049,39.319672>>)
				SET_CAM_ROT(destinationCam,<<18.496492,-0.237040,-31.326050>>)
				SET_CAM_FOV(destinationCam,45.000000)

			SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 1000, GRAPH_TYPE_ACCEL, GRAPH_TYPE_LINEAR)
			
			CREATE_SKYLIFT()
			
			IF	IS_PLAYER_MICHAEL_OR_FRANKLIN()
			
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(pedSelector.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(1, 3000.0, "BsPrepHeli"))
					WAIT(0)
				ENDWHILE
				IF NOT IS_ENTITY_DEAD(viSkyLift)			
					SET_PED_INTO_VEHICLE(pedSelector.pedID[SELECTOR_PED_TREVOR], viSkylift)
					GIVE_WEAPON_TO_PED(pedSelector.pedID[SELECTOR_PED_TREVOR], GADGETTYPE_PARACHUTE, 1)
				ENDIF
			ELSE
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(pedSelector.pedID[pedSelector.ePreviousSelectorPed], CHAR_MICHAEL, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(1, 3000.0, "BsPrepHeli"))
					WAIT(0)
				ENDWHILE
				IF NOT IS_ENTITY_DEAD(viSkyLift)			
					SET_PED_INTO_VEHICLE(pedSelector.pedID[pedSelector.ePreviousSelectorPed], viSkylift)
					GIVE_WEAPON_TO_PED(pedSelector.pedID[pedSelector.ePreviousSelectorPed], GADGETTYPE_PARACHUTE, 1)
				ENDIF
			ENDIF
			
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2628.1152, 2924.5671, 39.4265>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 154.8777)
//			
			//TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<2624.5552, 2917.9424, 35.9037>>, PEDMOVE_RUN)
			
			CLEAR_AREA_OF_VEHICLES(<<2624.928467,2947.425049,39.319672>>, 100.0)
			
			SET_CUTSCENE_RUNNING(TRUE)
			SETTIMERA(0)
			
			FADE_IN_IF_NEEDED()
			i_current_event++	
		BREAK
		//1st shot

		
		CASE 3
			IF TIMERA() > 1000
			
				REMOVE_BLIP(blipTrain)
		
				SET_RANDOM_TRAINS(FALSE) 
				DELETE_MISSION_TRAIN(viTrainIndex)
				CREATE_TRAIN_AND_DRIVER(<<2617.8606, 2934.2769, 38.8511>>)	
				
				SET_CAM_COORD(initialCam,<<2628.826416,2972.152588,44.413403>>)
				SET_CAM_ROT(initialCam,<<1.527947,0.000000,156.912994>>)
				SET_CAM_FOV(initialCam,18.322863)

				SET_CAM_COORD(destinationCam,<<2624.9,2963.6,46.0>>)
				SET_CAM_ROT(destinationCam,<<0.8303, 0.0000, 163.9873>>)
				SET_CAM_FOV(destinationCam,18.322863)

				
				SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 3000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
				SETTIMERA(0)
				i_current_event++	
			ENDIF
		BREAK
		
		CASE 4
			IF TIMERA() > 3000
						
//				IF NOT IS_ENTITY_DEAD(viSkylift)
//					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(viSkylift, -000.0)
//					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(viSkylift)
//				ENDIF
				
//				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2700.4109, 2864.6602, 36.5997>>)
//				SET_ENTITY_HEADING(PLAYER_PED_ID(), 244.1306)
//				
//				TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), <<2766.5916, 2829.8635, 35.2409>>, PEDMOVE_RUN)
				
//				
//				SET_CAM_COORD(destinationCam,<<2482.851563,2717.850098,75.083817>>)
//				SET_CAM_ROT(destinationCam,<<-3.971118,-0.000002,-46.108959>>)
//				SET_CAM_FOV(destinationCam,36.403961)

				IF NOT IS_ENTITY_DEAD(viTrainIndex)
					SET_TRAIN_CRUISE_SPEED(viTrainIndex, 22.5)
					SET_TRAIN_SPEED(viTrainIndex, 22.5)
				ENDIF
//				SET_CAM_ACTIVE(destinationCam, TRUE)
				SETTIMERA(0)
			
				i_current_event++	
			ENDIF
		BREAK
//		
//		CASE 5
//			IF TIMERA() > 3000
//				i_current_event++	
//			ENDIF
//		BREAK
		
		CASE 5
			IF IS_AUDIO_SCENE_ACTIVE("BS_P_D_TRAIN_ARRIVES")
				STOP_AUDIO_SCENE("BS_P_D_TRAIN_ARRIVES")
			ENDIF
			
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				NEW_LOAD_SCENE_STOP()
			ENDIF
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<2628.1152, 2924.5671, 39.4265>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 154.8777)
						
			SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_RUN, 3000, 0.0, TRUE, TRUE)
			FORCE_PED_MOTION_STATE(player_ped_id(), MS_ON_FOOT_RUN, FALSE, FAUS_CUTSCENE_EXIT)
			
								
//			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, NULL, "MICHAEL")
//			ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, PLAYER_PED_ID(), "TREVOR")
			
			//SCRIPT_ASSERT("boom")
			
			//1778178
			
			REPLAY_STOP_EVENT()
			
			IF IS_PLAYER_MICHAEL()
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, NULL, "TREVOR")
			ELIF IS_PLAYER_FRANKLIN()
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PLAYER_PED_ID(), "FRANKLIN")
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, NULL, "TREVOR")
			ELIF IS_PLAYER_TREVOR()
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, PLAYER_PED_ID(), "TREVOR")
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, NULL, "MICHAEL")
			ENDIF
			
			RESET_GAME_CAMERA()  
			SET_CUTSCENE_RUNNING(FALSE)//, (NOT IS_SCREEN_FADED_OUT()), 5000)
			i_current_event = 0
			mission_stage = STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING
			FADE_IN_IF_NEEDED()
		BREAK
		
				
	ENDSWITCH
	
	IF i_current_event > 3
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
			REPLAY_CANCEL_EVENT()
			DO_SCREEN_FADE_OUT(500)
			WHILE IS_SCREEN_FADING_OUT()
				WAIT(0)
			ENDWHILE
			i_current_event = 5
				
		ENDIF
	ENDIF
	

ENDPROC

FUNC BOOL IS_PLAYER_PUSHING_ANALOGUE_STICKS()

	INT padLX, padLy, padRX, padRy

	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(padLX, padLy, padRX, padRy)
	
	IF ABSI(padLX) > 75
	OR ABSI(padLY) > 75
	OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_AIM)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

INT iSwitchNowTimer
INT iSwitchNowDialogue

PROC PROCESS_SWITCH_TO_ME_DIALOGUE()

	SWITCH iSwitchNowDialogue
	
		CASE 0
			iSwitchNowTimer = GET_GAME_TIMER()
			iSwitchNowDialogue++
		BREAK
	
		CASE 1
			IF GET_GAME_TIMER() - iSwitchNowTimer > 7000
				iSwitchNowDialogue++
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVO_FOR_PLAYER_CURRENT("BSPRP_TAKET", "BSPRP_TAKET", "BSPRP_TAKEM")
					iSwitchNowTimer = GET_GAME_TIMER()
					iSwitchNowDialogue = 1
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC


PROC CONTROL_TRAIN_SLOWING_DOWN()

	IF NOT IS_ENTITY_DEAD(viTrainIndex)
		IF fDistanceToEnd < 60.0
			
			IF IS_PC_VERSION()
				IF NOT IS_ENTITY_ON_SCREEN(viTrainIndex)
					SET_TRAIN_CRUISE_SPEED(viTrainIndex, 30.0)
					SET_TRAIN_SPEED(viTrainIndex, 30.0)	
					PRINTLN("Speed:", 30.0)
				ELSE
					SET_TRAIN_CRUISE_SPEED(viTrainIndex, LERP_FLOAT(30.0, 0.0, 1.0/fDistanceToEnd))
					SET_TRAIN_SPEED(viTrainIndex, LERP_FLOAT(30.0, 0.0, 1.0/fDistanceToEnd))	
					PRINTLN("Speed PC version: ", LERP_FLOAT(30.0, 0.0, 1.0/fDistanceToEnd))
					
					IF fDistanceToEnd < 3.0
						SET_TRAIN_CRUISE_SPEED(viTrainIndex,0.0)
						SET_TRAIN_SPEED(viTrainIndex, 0.0)		
					ENDIF
				ENDIF
				
			ELSE
				IF NOT IS_ENTITY_ON_SCREEN(viTrainIndex)
					SET_TRAIN_CRUISE_SPEED(viTrainIndex, 30.0)
					SET_TRAIN_SPEED(viTrainIndex, 30.0)	
					PRINTLN("Speed:", 30.0)
				ELSE
					//This was working fine on consoles so lets leave this alone.				
					SET_TRAIN_CRUISE_SPEED(viTrainIndex, 0.33 * fDistanceToEnd)
					SET_TRAIN_SPEED(viTrainIndex, 0.33 * fDistanceToEnd)	
					PRINTLN("Speed CONSOLE Version:", 0.33 * fDistanceToEnd)
				ENDIF
			ENDIF
			
			
		ENDIF
	ENDIF

ENDPROC

SELECTOR_CAM_STRUCT sCamDetails

PROC  stageWaitForTrainToStopInSiding()

	SWITCH i_current_event
		
		CASE 0
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING")
			IF IS_PLAYER_MICHAEL()
				g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 0
			ELIF IS_PLAYER_FRANKLIN()
				g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 1
			ELIF IS_PLAYER_TREVOR()
				g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 2
			ENDIF
			IF NOT IS_AUDIO_SCENE_ACTIVE("BS_P_D_STEALTH")
				START_AUDIO_SCENE("BS_P_D_STEALTH")
			ENDIF
			
		
			IF IS_PLAYER_MICHAEL_OR_FRANKLIN()
				//PRINT_NOW("BSPD_SW_T", DEFAULT_GOD_TEXT_TIME, 1)
				SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_TREVOR, FALSE)
				SET_SELECTOR_PED_HINT(pedSelector, SELECTOR_PED_TREVOR, TRUE)
			ELSE
				//PRINT_NOW("BSPD_SW_M", DEFAULT_GOD_TEXT_TIME, 1)							
				SET_SELECTOR_PED_BLOCKED(pedSelector, SELECTOR_PED_MICHAEL, FALSE)
				SET_SELECTOR_PED_HINT(pedSelector, SELECTOR_PED_MICHAEL, TRUE)
			ENDIF
		
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
		
			i_current_event++
		BREAK
		
		CASE 1
			IF CREATE_CONVO_FOR_PLAYER_CURRENT("BSPRP_STOPM", "BSPRP_STOPF", "BSPRP_STOPT")
				i_current_event++
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF IS_PLAYER_MICHAEL_OR_FRANKLIN()
					PRINT_NOW("BSPD_SW_T", DEFAULT_GOD_TEXT_TIME, 1)
				ELSE
					PRINT_NOW("BSPD_SW_M", DEFAULT_GOD_TEXT_TIME, 1)							
				ENDIF	
				i_current_event++
			ENDIF
		BREAK
		
		CASE 3
		
			UPDATE_SELECTOR_HUD(pedSelector)
			
			PROCESS_SWITCH_TO_ME_DIALOGUE()
			
			IF IS_PLAYER_MICHAEL_OR_FRANKLIN()
				IF HAS_SELECTOR_PED_BEEN_SELECTED(pedSelector, SELECTOR_PED_TREVOR)
				OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedSelector.pedID[SELECTOR_PED_TREVOR]) < 200.0
					MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_TREVOR)
					sCamDetails.pedTo = pedSelector.pedID[SELECTOR_PED_TREVOR]
					CLEAR_PRINTS()
					i_current_event++
				ENDIF
			ELSE					
				IF HAS_SELECTOR_PED_BEEN_SELECTED(pedSelector, SELECTOR_PED_MICHAEL)
				OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedSelector.pedID[pedSelector.ePreviousSelectorPed]) < 200.0
					MAKE_SELECTOR_PED_SELECTION(pedSelector, pedSelector.ePreviousSelectorPed)
					sCamDetails.pedTo = pedSelector.pedID[pedSelector.ePreviousSelectorPed]
					CLEAR_PRINTS()
					i_current_event++
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 4
			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_LONG_RANGE(sCamDetails)
				IF sCamDetails.bOKToSwitchPed
					IF NOT sCamDetails.bPedSwitched
						IF TAKE_CONTROL_OF_SELECTOR_PED(pedSelector, TRUE, FALSE)          
						
							IF NOT IS_ENTITY_DEAD(viSkyLift)								
								FREEZE_ENTITY_POSITION(viSkyLift, FALSE)
							ENDIF						
							sCamDetails.bPedSwitched = TRUE
						ENDIF
					ENDIF 
				ENDIF
			ELSE
				i_current_event++
			ENDIF
		BREAK
		
		CASE 5						
			//IF CREATE_CONVO_FOR_PLAYER_ON_GROUND("BSPRP_SWITM", "BSPRP_SWITF", "BSPRP_SWIM")
				
//				IF NOT IS_PED_INJURED(pedSelector.pedID[pedSelector.ePreviousSelectorPed])
//					SET_ENTITY_LOD_DIST(pedSelector.pedID[pedSelector.ePreviousSelectorPed], 300)
//					SET_PED_LOD_MULTIPLIER(pedSelector.pedID[pedSelector.ePreviousSelectorPed], 3.0)
//									
//					GIVE_WEAPON_TO_PED(pedSelector.pedID[pedSelector.ePreviousSelectorPed], WEAPONTYPE_PISTOL, 100, TRUE, TRUE)
//					
//					OPEN_SEQUENCE_TASK(seqUnHookTrain)				
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vTrainDecouplePoint1, PEDMOVE_RUN)
//						TASK_PAUSE(NULL, 2000)
//					CLOSE_SEQUENCE_TASK(seqUnHookTrain)
//					TASK_PERFORM_SEQUENCE(pedSelector.pedID[pedSelector.ePreviousSelectorPed], seqUnHookTrain)
//					CLEAR_SEQUENCE_TASK(seqUnHookTrain)
//				ENDIF
						
				FADE_IN_IF_NEEDED()
				
				REQUEST_STAGE_ASSETS(STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING)
				i_current_event++
			//ENDIF
		BREAK
		
		CASE 6
			IF HAVE_STAGE_ASSETS_LOADED(STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING)
				FADE_IN_IF_NEEDED()
				
				IF IS_PLAYER_MICHAEL_OR_FRANKLIN()
					MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_TREVOR)
				ELSE
					MAKE_SELECTOR_PED_SELECTION(pedSelector, SELECTOR_PED_MICHAEL)
				ENDIF
				
				IF IS_AUDIO_SCENE_ACTIVE("BS_P_D_STEALTH")
					STOP_AUDIO_SCENE("BS_P_D_STEALTH")
				ENDIF
				IF NOT IS_AUDIO_SCENE_ACTIVE("BS_P_D_FLY_CHOPPER")
					START_AUDIO_SCENE("BS_P_D_FLY_CHOPPER")
				ENDIF
				i_current_event = 0//7
				mission_stage = STAGE_FLY_AWAY_WITH_CARRIAGE
			ENDIF
		BREAK
		

	
//		CASE 7
//		CASE 8
//		CASE 9	
//		CASE 10
//		CASE 11
//			
//			IF NOT IS_ENTITY_DEAD(viTempTrainCarriage[0])
//			AND NOT DOES_BLIP_EXIST(blipEngine)
//				blipEngine = CREATE_BLIP_FOR_VEHICLE(viTempTrainCarriage[0])
//			ENDIF
//			
//		
//			IF i_current_event = 7
//			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
//				//IF CREATE_CONVERSATION(myScriptedSpeech, "BSPRP", "BSPRP_MUNHK", CONV_PRIORITY_VERY_HIGH)
//				IF CREATE_CONVO_FOR_PLAYER_ON_GROUND("BSPRP_MUNHK", "BSPRP_MUNHF", "BSPRP_MUNHT")
//					i_current_event = 8
//				ENDIF
//			ENDIF
//			
//			IF i_current_event = 8
//			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
//			AND NOT IS_ENTITY_DEAD(pedSelector.pedID[pedSelector.ePreviousSelectorPed])
//			AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(pedSelector.pedID[pedSelector.ePreviousSelectorPed])) < 200.00
//				IF CREATE_CONVO_FOR_PLAYER_ON_GROUND("BSPRP_PICKUP", "BSPRP_PICKF", "BSPRP_PICKT")
//					i_current_event = 9
//				ENDIF
//			ENDIF
//						
//			IF i_current_event = 9
//			AND NOT IS_ENTITY_DEAD(viTempTrainCarriage[0])
//				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
//					PRINT_NOW("GETENGINE", DEFAULT_GOD_TEXT_TIME, 1)
//					REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipviTrainIndex)
//					i_current_event = 0//2
//					mission_stage = STAGE_FLY_AWAY_WITH_CARRIAGE
//				ENDIF
//			ENDIF
//		
//		BREAK
		
	ENDSWITCH
	
	PRINTLN("Dist:", fDistanceToEnd)

	IF NOT IS_ENTITY_DEAD(viTrainIndex)

		fDistanceToEnd = GET_DISTANCE_BETWEEN_COORDS(vEndOfTracks, GET_ENTITY_COORDS(viTrainIndex))
		IF fDistanceToEnd < 1.5
			SET_TRAIN_CRUISE_SPEED(viTrainIndex, 0.0)
			SET_TRAIN_SPEED(viTrainIndex, 0.0)	
			
				
			//viTempTrainCarriage[0] = GET_TRAIN_CARRIAGE(viTrainIndex, 0)
			
			CREATE_SEPARATE_TRAIN_MODELS_FROM_TRAIN()
						
		ELSE
		
			CONTROL_TRAIN_SLOWING_DOWN()
			
		ENDIF
	ELSE
		pRINTLN("train dead or DELETED")
	ENDIF
	
ENDPROC

FUNC BOOL IS_CARRIAGE_ATTACHED()

	INT i
	
	FOR i = 0 TO 8
		IF NOT IS_ENTITY_DEAD(viTempTrainCarriage[i])
			//RETURN IS_ENTITY_ATTACHED(viTempTrainCarriage[i])
			
			IF IS_ENTITY_ATTACHED_TO_ENTITY(viTempTrainCarriage[i], viSkyLift)
			OR IS_ENTITY_ATTACHED_TO_ENTITY(viSkyLift, viTempTrainCarriage[i])
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR

	RETURN FALSE

ENDFUNC

INT iMetalStressAudio
VECTOR vSkyliftRotation
VECTOR vSkyliftRotationPrev
//INT iStressAudioTimer
INT iMetalStressSound = GET_SOUND_ID() // Rob - 2131368

PROC PROCESS_METAL_STRESS_AUDIO()

	
	SWITCH iMetalStressAudio
	
		CASE 0
			IF REQUEST_SCRIPT_AUDIO_BANK("BIG_SCORE_PREP_D_01")
				iMetalStressAudio++
			ENDIF
		BREAK
		
		CASE 1
			
			IF NOT IS_ENTITY_DEAD(viSkyLift)
			//AND GET_GAME_TIMER() - iStressAudioTimer > 1000
			AND IS_CARRIAGE_ATTACHED()
			
				vSkyliftRotation = GET_ENTITY_ROTATION(viSkyLift)
				
				IF VDIST(vSkyliftRotationPrev, vSkyliftRotation) > 25.5
				AND HAS_SOUND_FINISHED(iMetalStressSound)
					//iMetalStressSound = GET_SOUND_ID()
					PLAY_SOUND_FROM_ENTITY(iMetalStressSound, "HELI_TURNING_MASTER", viSkyLift)
					//iStressAudioTimer = GET_GAME_TIMER()
					vSkyliftRotationPrev = vSkyliftRotation	
				ENDIF
				
			ENDIF
			
		BREAK

		
	ENDSWITCH
	
	

ENDPROC

VECTOR vInitialOffset
VECTOR vInitialRotation
VECTOR vDifference
VECTOR vLerpRotation
VECTOR vTrainAttachOffset = <<0.0, -5.8, -2.85>>

VECTOR vFlatbedAttachOffset = <<0.0, -7.6, -1.25>>
VECTOR vFlatbedAttachRotation = <<0.0, 0.0, 0.0>>

VECTOR vFlatbedAttachOffset180 = <<0.0, -7.6, -1.75>>
VECTOR vFlatbedAttachRotation180 = <<-3.0, 0.0, 180.0>>


VECTOR vTrailerAttachOffset = <<0.0, 0.0, 0.0>>
VECTOR vTrailerAttachRotation
FLOAT fLerpValue

BOOL bPlayImpactNoise = FALSE

PROC LERP_ATTACHMENT_FROM_HELI_TO_TRAILER(FLOAT fSpeed = 2.5)

	vDifference = LERP_VECTOR(vInitialOffset, vTrailerAttachOffset, fLerpValue)
	vLerpRotation = LERP_VECTOR(vInitialRotation, vTrailerAttachRotation, fLerpValue)
	
	IF fLerpValue < 1.0
		fLerpValue = fLerpValue +@ fSpeed
		PRINTLN("vDifference", vDifference, " vTrainAttachOffset:", vTrainAttachOffset, " vInitialOffset:", vInitialOffset)
		PRINTLN()
		PRINTLN("vLerpRotation", vLerpRotation, " vTrailerAttachRotation:", vTrailerAttachRotation, " vInitialRotation:", vInitialRotation)
		PRINTLN("vLerpRotation", vLerpRotation)
	ELSE
		IF bPlayImpactNoise = FALSE
			PLAY_SOUND_FROM_ENTITY(-1, "TRAIN_PICK_UP_IMPACT_MASTER", viSkyLift)
			
			REPLAY_RECORD_BACK_FOR_TIME(3.5, 4.0, REPLAY_IMPORTANCE_HIGHEST)
			
			bPlayImpactNoise = TRUE
		ENDIF
		fLerpValue = 1.0
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(viTempTrainCarriage[0])
	AND NOT IS_ENTITY_DEAD(viTrailerDropOff1)
		ATTACH_ENTITY_TO_ENTITY(viTempTrainCarriage[0], viTrailerDropOff1, 0, vDifference, vLerpRotation, TRUE, FALSE, TRUE)
	ENDIF
	
ENDPROC



INT iProcessCarefUlDialogue
INT iCarefulDialogueTimer

PROC PROCESS_CAREFUL_DIALOGUE(VEHICLE_INDEX viThisCarriage)

	SWITCH iProcessCarefUlDialogue
	
		CASE 0
			IF GET_GAME_TIMER() - iCarefulDialogueTimer > 7000
				IF IS_ENTITY_TOUCHING_ENTITY(viSkyLift, viThisCarriage)
					iProcessCarefUlDialogue++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF CREATE_CONVO_FOR_PLAYER_ON_GROUND("BSPRP_FLATBE", "BSPRP_FLATBF", "BSPRP_FLATBT")
				iCarefulDialogueTimer = GET_GAME_TIMER()
				iProcessCarefUlDialogue = 0 
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC

BLIP_INDEX blipTempTrainCarriage[9]

VECTOR vCarriageRot
VECTOR vTrailerRot

VECTOR vTrailerAttachOffset2 = <<0.0, -0.52, -0.65>>


INT iMagnetBuzz

BOOL bMagnetOn

VECTOR vDropCoords = <<1759.6031, 3270.7751, 41.2680>>
INT iPickedUpCarriage

BLIP_INDEX blipTrailer1
//BLIP_INDEX blipTrailer2
BOOL bOnlyPrintGodTextOnce1 = FALSE
BOOL bOnlyPrintGodTextOnce2 = FALSE
BOOL bToldToLower = FALSE


BOOL bIsAligned = FALSE
BOOL bIsAligned180 = FALSE

PROC stageFlyAwayWithCarriage()

	PRINTLN("@@@@@ Dist:", fDistanceToEnd)

	IF NOT IS_ENTITY_DEAD(viTrainIndex)

		fDistanceToEnd = GET_DISTANCE_BETWEEN_COORDS(vEndOfTracks, GET_ENTITY_COORDS(viTrainIndex))
		IF fDistanceToEnd < 1.5
			SET_TRAIN_CRUISE_SPEED(viTrainIndex, 0.0)
			SET_TRAIN_SPEED(viTrainIndex, 0.0)	
			
				
			//viTempTrainCarriage[0] = GET_TRAIN_CARRIAGE(viTrainIndex, 0)
			
			CREATE_SEPARATE_TRAIN_MODELS_FROM_TRAIN()
						
		ELSE
		
			CONTROL_TRAIN_SLOWING_DOWN()
			
		ENDIF
	ELSE
		pRINTLN("@@@@@ train dead or DELETED")
	ENDIF

	PROCESS_METAL_STRESS_AUDIO()

	//VECTOR vCarriagePosition
	INT i, j
	
	SWITCH i_current_event
	
		CASE 0
		CASE 1
		CASE 2
			
			IF NOT IS_ENTITY_DEAD(viTempTrainCarriage[0])
			AND NOT DOES_BLIP_EXIST(blipEngine)
			
				IF (DOES_ENTITY_EXIST(pedSelector.pedID[pedSelector.ePreviousSelectorPed]))
				OR (DOES_ENTITY_EXIST(pedSelector.pedID[SELECTOR_PED_TREVOR]))
					IF IS_PLAYER_MICHAEL_OR_FRANKLIN()
						IF NOT IS_PED_INJURED(pedSelector.pedID[SELECTOR_PED_TREVOR])
							CLEAR_PED_TASKS_IMMEDIATELY(pedSelector.pedID[SELECTOR_PED_TREVOR])
							SET_ENTITY_COORDS(pedSelector.pedID[SELECTOR_PED_TREVOR], <<2757.7615, 2805.0862, 40.6190>>)
							SET_ENTITY_HEADING(pedSelector.pedID[SELECTOR_PED_TREVOR], 24.1320)
							TASK_LOOK_AT_ENTITY(pedSelector.pedID[SELECTOR_PED_TREVOR], viSkyLift, INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
					
							pedSelector.ePreviousSelectorPed = SELECTOR_PED_TREVOR
					
						ENDIF
					ELSE
						IF NOT IS_PED_INJURED(pedSelector.pedID[pedSelector.ePreviousSelectorPed])
							CLEAR_PED_TASKS_IMMEDIATELY(pedSelector.pedID[pedSelector.ePreviousSelectorPed])
							SET_ENTITY_COORDS(pedSelector.pedID[pedSelector.ePreviousSelectorPed], <<2757.7615, 2805.0862, 40.6190>>)
							SET_ENTITY_HEADING(pedSelector.pedID[pedSelector.ePreviousSelectorPed], 24.1320)
							TASK_LOOK_AT_ENTITY(pedSelector.pedID[pedSelector.ePreviousSelectorPed], viSkyLift,  INFINITE_TASK_TIME,  SLF_WHILE_NOT_IN_FOV)
						ENDIF
					ENDIF
				ENDIF
			
				IF IS_PLAYER_MICHAEL()
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, PLAYER_PED_ID(), "MICHAEL")
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, NULL, "TREVOR")
				ELIF IS_PLAYER_TREVOR()
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 2, PLAYER_PED_ID(), "TREVOR")
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 0, NULL, "MICHAEL")
				ENDIF
			
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "STAGE_FLY_AWAY_WITH_CARRIAGE")
				
//				IF IS_PLAYER_MICHAEL()
//					g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 0
//				ELIF IS_PLAYER_FRANKLIN()
//					g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 1
//				ELIF IS_PLAYER_TREVOR()
//					g_replay.iReplayInt[REPLAY_PLAYER_STARTED_AS] = 2
//				ENDIF
				
				
				REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				
				REQUEST_STAGE_ASSETS(STAGE_FLY_AWAY_WITH_CARRIAGE)
			
				FADE_IN_IF_NEEDED()		
			
				blipEngine = CREATE_BLIP_FOR_VEHICLE(viTempTrainCarriage[0])
			ENDIF
			
		
			IF i_current_event = 0
			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				//IF CREATE_CONVERSATION(myScriptedSpeech, "BSPRP", "BSPRP_MUNHK", CONV_PRIORITY_VERY_HIGH)
				IF CREATE_CONVO_FOR_PLAYER_ON_GROUND("BSPRP_MUNHK", "BSPRP_MUNHKF", "BSPRP_MUNKHT")
					i_current_event = 1
				ENDIF
			ENDIF
			
			IF i_current_event = 1
			AND NOT IS_ENTITY_DEAD(viTempTrainCarriage[0])
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
					PRINT_NOW("GETENGINE", DEFAULT_GOD_TEXT_TIME, 1)
					REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipviTrainIndex)
					i_current_event = 2					
				ENDIF
			ENDIF
			
			IF i_current_event = 2
			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			AND NOT IS_ENTITY_DEAD(pedSelector.pedID[pedSelector.ePreviousSelectorPed])
			AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(pedSelector.pedID[pedSelector.ePreviousSelectorPed])) < 200.00
//				IF CREATE_CONVO_FOR_PLAYER_ON_GROUND("BSPRP_PICKUP", "BSPRP_PICKF", "BSPRP_PICKT")
					i_current_event = 3
//				ENDIF
			ENDIF
					
		BREAK
			
		CASE 3
			IF HAVE_STAGE_ASSETS_LOADED(STAGE_FLY_AWAY_WITH_CARRIAGE)		
			AND REQUEST_AMBIENT_AUDIO_BANK("BIG_SCORE_PREP_D")
				IF NOT IS_ENTITY_DEAD(viTempTrainCarriage[0])
					iMagnetBuzz = GET_SOUND_ID()
					PLAY_SOUND_FROM_ENTITY(iMagnetBuzz, "ELECTRO_MAGNET_DRONE_MASTER", viTempTrainCarriage[0])
				ENDIF		
				
				bOnlyPrintGodTextOnce1 = FALSE
				bOnlyPrintGodTextOnce2 = FALSE
				bMagnetOn = FALSE
				
				
				SETTIMERA(0)
				
				i_current_event++			
			ENDIF
		BREAK
						
		CASE 4		
					
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viSkyLift)
				REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipEngine)
				REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipviTrainIndex)
				IF NOT DOES_BLIP_EXIST(blipViSkylift)
					
					IF NOT HAS_SOUND_FINISHED(iMagnetBuzz)
						STOP_SOUND(iMagnetBuzz)
					ENDIF				
					blipViSkylift = CREATE_BLIP_FOR_VEHICLE(viSkyLift)
					CLEAR_PRINTS()
					IF bOnlyPrintGodTextOnce1 = FALSE
						PRINT_NOW("BSD_GETINCHOP", DEFAULT_GOD_TEXT_TIME, 1)
						bOnlyPrintGodTextOnce1 = TRUE
					ENDIF
					
				ENDIF
			ELSE
				REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipViSkylift)
				IF NOT IS_ENTITY_DEAD(viTempTrainCarriage[0])
					IF NOT DOES_BLIP_EXIST(blipEngine)
						IF HAS_SOUND_FINISHED(iMagnetBuzz)
							PLAY_SOUND_FROM_ENTITY(iMagnetBuzz, "ELECTRO_MAGNET_DRONE_MASTER", viTempTrainCarriage[0])
						ENDIF
						blipEngine = CREATE_BLIP_FOR_VEHICLE(viTempTrainCarriage[0])
						CLEAR_PRINTS()
						IF bOnlyPrintGodTextOnce2 = FALSE
							PRINT_NOW("GETENGINE", DEFAULT_GOD_TEXT_TIME, 1)						
							bOnlyPrintGodTextOnce2 = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			PROCESS_CAREFUL_DIALOGUE(viTempTrainCarriage[0])
		
			IF IS_ENTITY_AT_ENTITY(viSkyLift, viTempTrainCarriage[0], <<6.25, 6.25, 6.75>>)
			AND IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(viSkyLift), GET_ENTITY_HEADING(viTempTrainCarriage[0]), 30.0)
		
				IF bMagnetOn = FALSE
				//AND IS_ENTITY_AT_ENTITY(viSkyLift, viTempTrainCarriage[0], <<4.9, 4.9, 6.0>>)
					PRINT_HELP("MAGNETON")
					bMagnetOn = TRUE
				ENDIF
				
				IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK) 
		
					REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
		
					//Create trailer 1
										
					viTrailerDropOff1 = CREATE_VEHICLE(FREIGHTTRAILER,<<1776.7172, 3252.0356, 40.9069>>, 268.4944)
					//FREEZE_ENTITY_POSITION(viTrailerDropOff1, TRUE)
					SET_ENTITY_INVINCIBLE(viTrailerDropOff1, TRUE)	
				
					viTrailerDropOff1Cab = CREATE_VEHICLE(PHANTOM, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viTrailerDropOff1, <<0.0, 11.0, -1.2>>), GET_ENTITY_HEADING(viTrailerDropOff1) + 5.0)
					SET_ENTITY_COORDS_NO_OFFSET(viTrailerDropOff1Cab, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viTrailerDropOff1, <<0.0, 11.0, -1.2>>))
					//FREEZE_ENTITY_POSITION(viTrailerDropOff1Cab, TRUE)
					SET_VEHICLE_DOORS_LOCKED(viTrailerDropOff1Cab, VEHICLELOCK_LOCKED)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viTrailerDropOff1Cab, FALSE)
					SET_ENTITY_PROOFS(viTrailerDropOff1Cab, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE)
					SET_ENTITY_PROOFS(viTrailerDropOff1, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE)
					
					piTruckDriver1  = CREATE_HEIST_CREW_MEMBER_AT_INDEX(HEIST_FINALE, 0, <<1776.7172, 3252.0356, 40.9069>>, 0.0)
					SET_PED_INTO_VEHICLE(piTruckDriver1, viTrailerDropOff1Cab)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piTruckDriver1, TRUE)
					
					ATTACH_VEHICLE_TO_TRAILER(viTrailerDropOff1Cab, viTrailerDropOff1)
		
					SET_RANDOM_TRAINS(FALSE) 
										
					vInitialOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(viSkyLift, GET_ENTITY_COORDS(viTempTrainCarriage[0]))
					vInitialRotation = GET_ENTITY_ROTATION(viTempTrainCarriage[0]) - GET_ENTITY_ROTATION(viSkyLift)
					ATTACH_ENTITY_TO_ENTITY(viTempTrainCarriage[0], viSkyLift, 0, vInitialOffset, vInitialRotation, TRUE, FALSE, TRUE)		
					PRINTLN("vDifference", vDifference, " vTrainAttachOffset:", vTrainAttachOffset, " vInitialOffset:", vInitialOffset)
					
					DETACH_ENTITY(viSkyLift)
					//DELETE_VEHICLE(viTempTrainCarriage[0])
					REMOVE_BLIP(blipEngine)
					//PRINT_NOW("TAKEENGINE", DEFAULT_GOD_TEXT_TIME, 1)
					
					SET_CONTROL_SHAKE(PLAYER_CONTROL, 250, 125)
					
					bMagnetOn = TRUE
					bPlayImpactNoise = FALSE
					sLocatesData.LocationBlip = CREATE_BLIP_FOR_COORD(vDropCoords)
					
					TRIGGER_MUSIC_EVENT("FHPRD_TRAIN")
					
					SET_HELI_CONTROL_LAGGING_RATE_SCALAR(viSkyLift, 0.5)	
					STOP_SOUND(iMagnetBuzz)
					
					PLAY_SOUND_FROM_ENTITY(-1, "TRAIN_PICK_UP_MASTER", viSkyLift)
					
					SET_SKYLIFT_FREIGHT_COLLISION_BOX(TRUE)
					SET_SKYLIFT_FLATBED_COLLISION_BOX(FALSE)
					
					FREEZE_ENTITY_POSITION(viTempTrainCarriage[0], FALSE)
					SET_ENTITY_COLLISION(viTempTrainCarriage[0], FALSE)
					
					SET_ENTITY_NO_COLLISION_ENTITY(viSkyLift, viTempTrainCarriage[1], TRUE)
					
					SETTIMERA(0)
					
					i_current_event++				
					
				ENDIF
			ELSE
				
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				AND NOT IS_ENTITY_AT_ENTITY(viSkyLift, viTempTrainCarriage[0], <<6.4, 6.4, 9.0>>)
					bMagnetOn = FALSE
					CLEAR_HELP()
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
		CASE 6
		CASE 7
		
					
			IF i_current_event = 5
			AND TIMERA() > 2000
				//IF CREATE_CONVERSATION(myScriptedSpeech, "BSPRP", "BSPRP_TRAINC", CONV_PRIORITY_VERY_HIGH)
					i_current_event= 6
				//ENDIF
			ENDIF
				
		
			IF i_current_event = 6
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					//IF CREATE_CONVERSATION(myScriptedSpeech, "BSPRP", "BSPRP_TAKEBA", CONV_PRIORITY_VERY_HIGH)
					IF CREATE_CONVO_FOR_PLAYER_ON_GROUND("BSPRP_PICKUP", "BSPRP_PICKF", "BSPRP_PICKT")
						i_current_event = 7
					ENDIF
				ENDIF
			ENDIF
		
			vDifference = LERP_VECTOR(vInitialOffset, vTrainAttachOffset, fLerpValue)
			vLerpRotation = LERP_VECTOR(vInitialRotation, <<0.0, 0.0, 0.0>>, fLerpValue)
			
			IF fLerpValue < 1.0
				fLerpValue = fLerpValue +@ 2.0
				PRINTLN("vDifference", vDifference, " vTrainAttachOffset:", vTrainAttachOffset, " vInitialOffset:", vInitialOffset)
				PRINTLN("vLerpRotation", vLerpRotation)
			ELSE
				IF bPlayImpactNoise = FALSE
					PLAY_SOUND_FROM_ENTITY(-1, "TRAIN_PICK_UP_IMPACT_MASTER", viTempTrainCarriage[0])
					bPlayImpactNoise = TRUE
				ENDIF
				fLerpValue = 1.0
			ENDIF
							
			ATTACH_ENTITY_TO_ENTITY(viTempTrainCarriage[0], viSkyLift, 0, vDifference, vLerpRotation, TRUE, FALSE, TRUE)
			
			//IF IS_ENTITY_IN_ANGLED_AREA(viSkyLift, <<1735.198608,3297.529053,39.473721>>, <<1784.188477,3245.093994,100.154278>>, 93.000000)
			IF IS_ENTITY_IN_ANGLED_AREA(viSkyLift, <<1784.349487,3178.338623,37.757511>>, <<1703.870728,3326.288574,90.465927>>, 174.750000)
				IF bToldToLower = FALSE
					
					IF NOT IS_ENTITY_DEAD(viTrailerDropOff1)
						FREEZE_ENTITY_POSITION(viTrailerDropOff1, TRUE)
					ENDIF
					IF NOT IS_ENTITY_DEAD(viTrailerDropOff1Cab)
						FREEZE_ENTITY_POSITION(viTrailerDropOff1Cab, TRUE)
					ENDIF
				
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
					blipTrailer1 = CREATE_BLIP_FOR_VEHICLE(viTrailerDropOff1, FALSE, FALSE)
					PRINT_NOW("TAKEENGINE2", DEFAULT_GOD_TEXT_TIME, 1)
					bToldToLower = TRUE
				ENDIF
				
				//IF IS_ENTITY_AT_ENTITY(viSkyLift, viTrailerDropOff1, <<6.0, 6.0, 6.0>>)
				IF IS_ENTITY_IN_ANGLED_AREA(viTempTrainCarriage[0], <<1765.117432,3250.880127,40.718864>>, <<1788.481934,3251.799805,50.444584>>, 7.750000)
				AND NOT IS_ENTITY_DEAD(viTrailerDropOff1)
				AND VMAG(GET_ENTITY_VELOCITY(viSkyLift)) < 3.0
					IF (IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(viSkyLift), GET_ENTITY_HEADING(viTrailerDropOff1), 20)
						OR IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(viSkyLift), GET_ENTITY_HEADING(viTrailerDropOff1) - 180.0, 20))
						CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
					
						IF bMagnetOn
							PRINT_HELP("MAGNETOFF")
							IF IS_MESSAGE_BEING_DISPLAYED()
								CLEAR_PRINTS()
							ENDIF
							bMagnetOn = FALSE
						ENDIF
						
						IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK) 
							SET_HELI_CONTROL_LAGGING_RATE_SCALAR(viSkyLift, 1.0)
							DETACH_ENTITY(viTempTrainCarriage[0], FALSE)
							SET_ENTITY_COLLISION(viTempTrainCarriage[0], FALSE)
							DETACH_ENTITY(viSkyLift)
							SETTIMERA(0)
							
							SET_SKYLIFT_FLATBED_COLLISION_BOX(FALSE)
							SET_SKYLIFT_FREIGHT_COLLISION_BOX(FALSE)
							SET_SKYLIFT_BIG_MAGNET(FALSE)
							
							CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
							bPlayImpactNoise = FALSE
							fLerpValue = 0.0
							vInitialOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(viTrailerDropOff1, GET_ENTITY_COORDS(viTempTrainCarriage[0]))
							vInitialRotation = GET_ENTITY_ROTATION(viTempTrainCarriage[0]) - GET_ENTITY_ROTATION(viTrailerDropOff1)
							
							vCarriageRot = GET_ENTITY_ROTATION(viTempTrainCarriage[0])
							vTrailerRot = GET_ENTITY_ROTATION(viTrailerDropOff1)
							
							PRINTLN(vCarriageRot.z, "-", vTrailerRot.z, ": ", "ABSF(vCarriageRot.z - vTrailerRot.z):", ABSF(vCarriageRot.z - vTrailerRot.z))
							
							IF (vCarriageRot.z - vTrailerRot.z) > 90.0
							AND (vCarriageRot.z - vTrailerRot.z) < 270.0
								vTrailerAttachRotation.z = 180.0							
							ELSE
								vTrailerAttachRotation.z = 0.0							
							ENDIF
				
							IF IS_HELP_MESSAGE_BEING_DISPLAYED()
								CLEAR_HELP()
							ENDIF
							
							IF DOES_BLIP_EXIST(blipTrailer1)
								REMOVE_BLIP(blipTrailer1)
							ENDIF
							
							TRIGGER_MUSIC_EVENT("FHPRD_STOP")
							CLEAR_HELP()
							i_current_event++	
						ENDIF
					ELSE
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP()
							bMagnetOn = TRUE
						ENDIF
					ENDIF	
				ELSE
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
						bMagnetOn = TRUE
					ENDIF
				ENDIF
			ELSE
			
				IF bToldToLower = TRUE
					CLEAR_PRINTS()
					bToldToLower = FALSE
					IF DOES_BLIP_EXIST(blipTrailer1)
						REMOVE_BLIP(blipTrailer1)
					ENDIF
					
					SET_BIT(sLocatesData.iLocatesBitSet, BS_INITIAL_GOD_TEXT_PRINTED)
					
				ENDIF
				//Draw corona		
				
				bMagnetOn = TRUE
				
				IF bToldToLower = FALSE
					IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, vDropCoords, <<0.1, 0.1, 3.0>>, FALSE, viSkyLift, "TAKEENGINE", "BSD_GETINCHOP", "BSD_GETINCHOP")
				ELSE
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
				ENDIF
				
			ENDIF

		BREAK
		
		CASE 8
					
			LERP_ATTACHMENT_FROM_HELI_TO_TRAILER(2.75)
		
			IF TIMERA() > 0
				SET_SKYLIFT_FREIGHT_COLLISION_BOX(FALSE)
				SET_ENTITY_COLLISION(viTempTrainCarriage[0], TRUE)
				
				FOR i = 0 TO 8
					IF GET_ENTITY_MODEL(viTempTrainCarriage[i]) = FREIGHTCAR
						blipTempTrainCarriage[i] = CREATE_BLIP_FOR_VEHICLE(viTempTrainCarriage[i])
					ENDIF
				ENDFOR
				
				CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
				
				SETTIMERA(0)
				i_current_event++			
			ENDIF
		BREAK
		
		CASE 9
		
			LERP_ATTACHMENT_FROM_HELI_TO_TRAILER(2.75)
				
			//IF CREATE_CONVERSATION(myScriptedSpeech, "BSPRP", "BSPRP_COMINB", CONV_PRIORITY_VERY_HIGH)
			IF CREATE_CONVO_FOR_PLAYER_CURRENT("BSPRP_COMINM", "", "BSPRP_COMINB")
				i_current_event++
			ENDIF
		BREAK
		
		CASE 10
		
			LERP_ATTACHMENT_FROM_HELI_TO_TRAILER(2.75)
		
		
			IF TIMERA() > 2000
			AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				PRINT_NOW("GETCARRIAGE", DEFAULT_GOD_TEXT_TIME, 1)
				iMagnetBuzz = 0
				vTrailerAttachRotation.z = 0.0	
				i_current_event++			
			ENDIF
		BREAK
		
		CASE 11
		
			IF NOT IS_PED_INJURED(piTruckDriver1)
			AND NOT IS_ENTITY_DEAD(viTrailerDropOff1Cab)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), piTruckDriver1) > 25.00
					IF GET_SCRIPT_TASK_STATUS(piTruckDriver1, SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) <> PERFORMING_TASK
						IF NOT IS_ENTITY_DEAD(viTrailerDropOff1Cab)
							FREEZE_ENTITY_POSITION(viTrailerDropOff1, FALSE)
							FREEZE_ENTITY_POSITION(viTrailerDropOff1Cab, FALSE)
							SET_PED_KEEP_TASK(piTruckDriver1, TRUE)
							
							SET_VEHICLE_IMPATIENCE_TIMER(viTrailerDropOff1Cab, 15000)
							
							TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(piTruckDriver1, viTrailerDropOff1Cab, <<2598.5564, 1639.3530, 27.0148>>, 15.0, DRIVINGMODE_AVOIDCARS, 5.0)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// url:bugstar:2078692
			BOOL bAligned
			bAligned = FALSE
		
			FOR i = 0 TO 8
				IF GET_ENTITY_MODEL(viTempTrainCarriage[i]) = FREIGHTCAR
					
					IF IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(viSkyLift), GET_ENTITY_HEADING(viTempTrainCarriage[i]), 22.0)	
						bIsAligned = TRUE
					ELSE
						bIsAligned = FALSE
					ENDIF
					IF IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(viSkyLift) + 180.00, GET_ENTITY_HEADING(viTempTrainCarriage[i]), 22.0)
						bIsAligned180 = TRUE
					ELSE
						bIsAligned180 = FALSE
					ENDIF													
																		
					IF IS_ENTITY_AT_ENTITY(viSkyLift, viTempTrainCarriage[i], <<6.1, 6.1, 8.0>>)
					AND (bIsAligned180 OR bIsAligned)
						
						bAligned = TRUE
						
						IF bMagnetOn = FALSE
							PRINT_HELP("MAGNETON")
							bMagnetOn = TRUE
						ENDIF
						
						PROCESS_CAREFUL_DIALOGUE(viTempTrainCarriage[i])
						
						IF iMagnetBuzzEffects[i] = 0
							iMagnetBuzzEffects[i] = GET_SOUND_ID()
							PLAY_SOUND_FROM_ENTITY(iMagnetBuzzEffects[i], "ELECTRO_MAGNET_DRONE_MASTER", viTempTrainCarriage[i])
						ENDIF
												
						IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK) 
													
							IF bIsAligned180
								vFlatbedAttachOffset = vFlatbedAttachOffset180
								vFlatbedAttachRotation = vFlatbedAttachRotation180						
							ELSE
								vFlatbedAttachOffset = vFlatbedAttachOffset
								vFlatbedAttachRotation = vFlatbedAttachRotation							
							ENDIF
							
							vInitialOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(viSkyLift, GET_ENTITY_COORDS(viTempTrainCarriage[i]))
							vInitialRotation = GET_ENTITY_ROTATION(viSkyLift) - GET_ENTITY_ROTATION(viTempTrainCarriage[i])
							
							vDifference = LERP_VECTOR(vInitialOffset, vFlatbedAttachOffset, 0.0)
							vLerpRotation = LERP_VECTOR(vInitialRotation, vFlatbedAttachRotation, 0.0)
							
							FREEZE_ENTITY_POSITION(viTempTrainCarriage[i], FALSE)
							
							ATTACH_ENTITY_TO_ENTITY(viTempTrainCarriage[i], viSkyLift, 0, vDifference, vLerpRotation, TRUE, FALSE, TRUE)
						
							SET_SKYLIFT_BIG_MAGNET(TRUE)
							SET_SKYLIFT_FREIGHT_COLLISION_BOX(FALSE)
							SET_SKYLIFT_FLATBED_COLLISION_BOX(TRUE)
							
							//Dont collide with the trailer behind or in front until clear.
							IF i < 8
								SET_ENTITY_NO_COLLISION_ENTITY(viTempTrainCarriage[i+1], viSkyLift, TRUE)
							ENDIF							
							IF i >= 1
								SET_ENTITY_NO_COLLISION_ENTITY(viTempTrainCarriage[i-1], viSkyLift, TRUE)
							ENDIF
							
							SET_ENTITY_COLLISION(viTempTrainCarriage[i], FALSE)
							bPlayImpactNoise = FALSE							
							bToldToLower = FALSE
							FOR j = 0 TO 8
								REMOVE_BLIP(blipTempTrainCarriage[j])
							ENDFOR
							
							sLocatesData.LocationBlip = CREATE_BLIP_FOR_COORD(vDropCoords)
							
							SET_HELI_CONTROL_LAGGING_RATE_SCALAR(viSkyLift, 0.5)	
							STOP_SOUND(iMagnetBuzzEffects[i])
							PLAY_SOUND_FROM_ENTITY(-1, "TRAIN_PICK_UP_MASTER", viSkyLift)
																			
							iPickedUpCarriage = i
																	
							viTrailerDropOff2 = CREATE_VEHICLE(FREIGHTTRAILER,<<1784.2673, 3280.5579, 40.8560>>, 265.7362 )
							SET_ENTITY_PROOFS(viTrailerDropOff2, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE)
							
							//Create trailer to put flatbed trailer on.. yeah sounds weird.
							viTrailerDropOff2Cab = CREATE_VEHICLE(PHANTOM,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viTrailerDropOff2, <<0.0, 11.0, -1.2>>), GET_ENTITY_HEADING(viTrailerDropOff2) + 5.0)
							SET_ENTITY_COORDS_NO_OFFSET(viTrailerDropOff2Cab, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(viTrailerDropOff2, <<0.0, 11.0, -1.2>>))
							SET_VEHICLE_ON_GROUND_PROPERLY(viTrailerDropOff2Cab)
							SET_ENTITY_PROOFS(viTrailerDropOff2Cab, FALSE, FALSE, FALSE, TRUE, FALSE, FALSE)
							
							piTruckDriver2  = CREATE_HEIST_CREW_MEMBER_AT_INDEX(HEIST_FINALE, 1, <<1776.7172, 3252.0356, 40.9069>>, 0.0)
							SET_PED_INTO_VEHICLE(piTruckDriver2, viTrailerDropOff2Cab)
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piTruckDriver2, TRUE)
							
							SET_VEHICLE_DOORS_LOCKED(viTrailerDropOff2Cab, VEHICLELOCK_LOCKED)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(viTrailerDropOff2Cab, FALSE)
							
							ATTACH_VEHICLE_TO_TRAILER(viTrailerDropOff2Cab, viTrailerDropOff2)
							
							fLerpValue = 0.0
							i_current_event++									
						ENDIF
					ELSE					
						iMagnetBuzzEffects[i] = 0
						STOP_SOUND(iMagnetBuzzEffects[i])
					ENDIF
				ENDIF
			ENDFOR
			
			IF (NOT IS_ENTITY_AT_ENTITY(viSkyLift, viTempTrainCarriage[0], <<6.1, 6.1, 8.0>>)
			AND NOT IS_ENTITY_AT_ENTITY(viSkyLift, viTempTrainCarriage[1], <<6.1, 6.1, 8.0>>)
			AND NOT IS_ENTITY_AT_ENTITY(viSkyLift, viTempTrainCarriage[2], <<6.1, 6.1, 8.0>>)
			AND NOT IS_ENTITY_AT_ENTITY(viSkyLift, viTempTrainCarriage[3], <<6.1, 6.1, 8.0>>)
			AND NOT IS_ENTITY_AT_ENTITY(viSkyLift, viTempTrainCarriage[4], <<6.1, 6.1, 8.0>>)
			AND NOT IS_ENTITY_AT_ENTITY(viSkyLift, viTempTrainCarriage[5], <<6.1, 6.1, 8.0>>)
			AND NOT IS_ENTITY_AT_ENTITY(viSkyLift, viTempTrainCarriage[6], <<6.1, 6.1, 8.0>>)
			AND NOT IS_ENTITY_AT_ENTITY(viSkyLift, viTempTrainCarriage[7], <<6.1, 6.1, 8.0>>)			
			AND NOT IS_ENTITY_AT_ENTITY(viSkyLift, viTempTrainCarriage[8], <<6.1, 6.1, 8.0>>))			
			OR NOT (bAligned)
												
				IF bMagnetOn = TRUE
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
						bMagnetOn = FALSE
					ENDIF
				ENDIF
				
			ENDIF
			
		BREAK
		
		CASE 12
		CASE 13
		CASE 14
		
			IF i_current_event = 12
				//IF CREATE_CONVERSATION(myScriptedSpeech, "BSPRP", "BSPRP_FLATGO", CONV_PRIORITY_VERY_HIGH)
				IF CREATE_CONVO_FOR_PLAYER_ON_GROUND("BSPRP_FLATGO", "BSPRP_FLATGO", "BSPRP_FLATM")
					i_current_event = 13
				ENDIF
			ENDIF
		
			IF i_current_event = 13
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
					PRINT_NOW("TAKEFLATBED", DEFAULT_GOD_TEXT_TIME, 1)		
					i_current_event = 14
				ENDIF
			ENDIF
			
		
			vDifference = LERP_VECTOR(vInitialOffset, vFlatbedAttachOffset, fLerpValue)
			vLerpRotation = LERP_VECTOR(vInitialRotation, vFlatbedAttachRotation, fLerpValue)
			
			IF fLerpValue < 1.0
				fLerpValue = fLerpValue +@ 2.5
				PRINTLN("vDifference", vDifference, " vTrainAttachOffset:", vFlatbedAttachRotation, " vInitialOffset:", vInitialOffset)
				PRINTLN("vLerpRotation", vLerpRotation)
			ELSE
				IF bPlayImpactNoise = FALSE
					PLAY_SOUND_FROM_ENTITY(-1, "TRAIN_PICK_UP_IMPACT_MASTER", viSkyLift)
					REPLAY_RECORD_BACK_FOR_TIME(3.5, 4.0, REPLAY_IMPORTANCE_HIGHEST)
					bPlayImpactNoise = TRUE
				ENDIF
				fLerpValue = 1.0
			ENDIF
									
			ATTACH_ENTITY_TO_ENTITY(viTempTrainCarriage[iPickedUpCarriage], viSkyLift, 0, vDifference, vLerpRotation, TRUE, FALSE, TRUE)
			
			//IF IS_ENTITY_IN_ANGLED_AREA(viSkyLift, <<1735.198608,3297.529053,39.473721>>, <<1784.188477,3245.093994,100.154278>>, 93.000000)
			
			
			IF IS_ENTITY_IN_ANGLED_AREA(viSkyLift, <<1784.349487,3178.338623,37.757511>>, <<1703.870728,3326.288574,90.465927>>, 174.750000)
			
				IF bToldToLower = FALSE
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
					blipTrailer1 = CREATE_BLIP_FOR_VEHICLE(viTrailerDropOff2, FALSE, FALSE)
					
					IF NOT IS_ENTITY_DEAD(viTrailerDropOff2)
						FREEZE_ENTITY_POSITION(viTrailerDropOff2, TRUE)
					ENDIF
					IF NOT IS_ENTITY_DEAD(viTrailerDropOff2Cab)
						FREEZE_ENTITY_POSITION(viTrailerDropOff2Cab, TRUE)
					ENDIF
					bToldToLower = TRUE
				ENDIF
				
				//IF IS_ENTITY_AT_ENTITY(viSkyLift, viTrailerDropOff2, <<6.0, 6.0, 6.0>>)
				//IF IS_ENTITY_IN_ANGLED_AREA(viSkyLift, <<1795.957275,3279.801758,41.671764>>, <<1772.338013,3281.307373,48.468468>>, 6.000000)
				IF IS_ENTITY_IN_ANGLED_AREA(viTempTrainCarriage[iPickedUpCarriage], <<1773.061523,3279.320557,40.500504>>, <<1795.292114,3279.465576,50.554737>>, 7.750000)
				AND NOT IS_ENTITY_DEAD(viTrailerDropOff2)	
				AND (IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(viSkyLift), GET_ENTITY_HEADING(viTrailerDropOff2), 20)
					OR IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(viSkyLift), GET_ENTITY_HEADING(viTrailerDropOff2) - 180.0, 20))
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
				
					IF bMagnetOn
						PRINT_HELP("MAGNETOFF")
						IF IS_MESSAGE_BEING_DISPLAYED()
							CLEAR_PRINTS()
						ENDIF
						bMagnetOn = FALSE
					ENDIF
				
					IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK) 
						
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
						
						SET_HELI_CONTROL_LAGGING_RATE_SCALAR(viSkyLift, 1.0)	
						
						DETACH_ENTITY(viTempTrainCarriage[iPickedUpCarriage], FALSE)
						DETACH_ENTITY(viSkyLift)
						CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
											
						
						SET_SKYLIFT_FLATBED_COLLISION_BOX(FALSE)
						SETTIMERA(0)
						
						TRIGGER_MUSIC_EVENT("FHPRD_STOP")
						bPlayImpactNoise = FALSE
						fLerpValue = 0.0
						vInitialOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(viTrailerDropOff2, GET_ENTITY_COORDS(viTempTrainCarriage[iPickedUpCarriage]))
						vInitialRotation = GET_ENTITY_ROTATION(viTempTrainCarriage[iPickedUpCarriage]) - GET_ENTITY_ROTATION(viTrailerDropOff2)
						
						vCarriageRot = GET_ENTITY_ROTATION(viTempTrainCarriage[iPickedUpCarriage])
						vTrailerRot = GET_ENTITY_ROTATION(viTrailerDropOff2)
						
						IF (vCarriageRot.z - vTrailerRot.z) > 90.0
						AND (vCarriageRot.z - vTrailerRot.z) < 270.0
							vTrailerAttachRotation.z = 180.0							
						ELSE
							vTrailerAttachRotation.z = 0.0							
						ENDIF
																	
						i_current_event = 15
					ENDIF
				ELSE					
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
						bMagnetOn = TRUE
					ENDIF	
				ENDIF
			
			ELSE
				
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
					bMagnetOn = TRUE
				ENDIF	
				
				IF bToldToLower = TRUE
					CLEAR_PRINTS()
					bToldToLower = FALSE
					IF DOES_BLIP_EXIST(blipTrailer1)
						REMOVE_BLIP(blipTrailer1)
					ENDIF
					SET_BIT(sLocatesData.iLocatesBitSet, BS_INITIAL_GOD_TEXT_PRINTED)
				ENDIF
				//Draw corona		
				
				bMagnetOn = TRUE
				
				IF bToldToLower = FALSE
					IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, vDropCoords, <<0.1, 0.1, 3.0>>, FALSE, viSkyLift, "TAKEFLATBED", "BSD_GETINCHOP", "BSD_GETINCHOP")
				ELSE
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
				ENDIF
				
			ENDIF
		BREAK
		
		CASE 15
		
			vDifference = LERP_VECTOR(vInitialOffset, vTrailerAttachOffset2, fLerpValue)
			vLerpRotation = LERP_VECTOR(vInitialRotation, vTrailerAttachRotation, fLerpValue)
			IF fLerpValue < 1.0
				fLerpValue = fLerpValue +@ 1.75
				PRINTLN("vDifference", vDifference, " vTrainAttachOffset:", vTrailerAttachRotation, " vInitialOffset:", vInitialOffset)
				PRINTLN("vLerpRotation", vLerpRotation)
			ELSE
				IF bPlayImpactNoise = FALSE
					PLAY_SOUND_FROM_ENTITY(-1, "TRAIN_PICK_UP_IMPACT_MASTER", viTempTrainCarriage[iPickedUpCarriage])
					
					SET_SKYLIFT_FREIGHT_COLLISION_BOX(FALSE)
					SET_SKYLIFT_FLATBED_COLLISION_BOX(FALSE)
					SETTIMERA(0)
					SET_ENTITY_COLLISION(viTempTrainCarriage[iPickedUpCarriage], TRUE)
					REMOVE_BLIP(blipTrailer1)
					
					bPlayImpactNoise = TRUE
				ENDIF
				fLerpValue = 1.0
			ENDIF		
			
			IF NOT IS_ENTITY_DEAD(viTempTrainCarriage[iPickedUpCarriage])
			AND NOT IS_ENTITY_DEAD(viTrailerDropOff2)
				ATTACH_ENTITY_TO_ENTITY(viTempTrainCarriage[iPickedUpCarriage], viTrailerDropOff2, 0, vDifference, vLerpRotation, TRUE, FALSE, TRUE)
			ENDIF
			
			
			IF NOT IS_PED_INJURED(piTruckDriver2)
			AND NOT IS_ENTITY_DEAD(viTrailerDropOff2Cab)
			AND bPlayImpactNoise = TRUE
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), piTruckDriver2) > 15.00
				OR TIMERA() > 15000
					IF GET_SCRIPT_TASK_STATUS(piTruckDriver2, SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) <> PERFORMING_TASK
						FREEZE_ENTITY_POSITION(viTrailerDropOff2, FALSE)
						FREEZE_ENTITY_POSITION(viTrailerDropOff2Cab, FALSE)
						
						SET_VEHICLE_IMPATIENCE_TIMER(viTrailerDropOff2Cab, 15000)
						
						TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(piTruckDriver2, viTrailerDropOff2Cab, <<2598.5564, 1639.3530, 27.0148>>, 15.0, DRIVINGMODE_AVOIDCARS, 5.0)
						SET_PED_KEEP_TASK(piTruckDriver2, TRUE)
						TRIGGER_MUSIC_EVENT("FHPRD_END")
		
						IF IS_AUDIO_SCENE_ACTIVE("BS_P_D_FLY_CHOPPER")
							STOP_AUDIO_SCENE("BS_P_D_FLY_CHOPPER")
						ENDIF
						i_current_event = 0
						mission_stage = STAGE_MISSION_PASSED					
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
		
ENDPROC


PROC DELETE_ALL_ENTITIES()

	fRailHeading = 0.0

	DELETE_OBJECT(oiRail01)
	DELETE_OBJECT(oiRail02)

	REMOVE_CUTSCENE()
	STOP_AUDIO_SCENES()

	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blippedStation1)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blippedStation2)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipTrain)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipviTrainIndex)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipViSkylift)
		
	DELETE_PED(pedStation1)
	DELETE_PED(pedStation2)

	DELETE_VEHICLE(viSkylift)
	
	DELETE_MISSION_TRAIN(viTrainIndex)
	
	DELETE_VEHICLE(viTempTrainCarriage[0])
	DELETE_VEHICLE(viTempTrainCarriage[1])
	DELETE_VEHICLE(viTempTrainCarriage[2])
	DELETE_VEHICLE(viTempTrainCarriage[3])
	DELETE_VEHICLE(viTempTrainCarriage[4])
	DELETE_VEHICLE(viTempTrainCarriage[5])
	DELETE_VEHICLE(viTempTrainCarriage[6])
	DELETE_VEHICLE(viTempTrainCarriage[7])
	DELETE_VEHICLE(viTempTrainCarriage[8])
	
	DELETE_VEHICLE(viTrailerDropOff1Cab)
	DELETE_VEHICLE(viTrailerDropOff1)
	DELETE_VEHICLE(viTrailerDropOff2Cab)
	DELETE_VEHICLE(viTrailerDropOff2)
	
	
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL	
		DELETE_PED(pedSelector.pedID[pedSelector.ePreviousSelectorPed])
	ENDIF

	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR	
		DELETE_PED(pedSelector.pedID[SELECTOR_PED_TREVOR])
	ENDIF
	
	PIN_HEIST_BOARD_IN_MEMORY(HEIST_RURAL_BANK, FALSE)
		
ENDPROC


#IF IS_DEBUG_BUILD

BOOL bDebugBoxes
BOOL bFreightBox
BOOL bFlatbedBox
BOOL bMagnetExtra

PROC debugProcedures()

	iDebugMissionStage = ENUM_TO_INT(mission_stage)

	IF bDebugInitialised = FALSE
		//Init debug stuff.
		
		bsPrepWidgetGroup = START_WIDGET_GROUP("Big Score Prep Test")
			
			ADD_WIDGET_BOOL("bIsSuperDebugEnabled", bIsSuperDebugEnabled)
			ADD_WIDGET_BOOL("Enable Debug Processing", bDebugOn)
			
			ADD_WIDGET_INT_READ_ONLY("iTimelapseCut", sTimelapse.iTimelapseCut)
			ADD_WIDGET_INT_READ_ONLY("i_current_event", i_current_event)
			ADD_WIDGET_INT_READ_ONLY("mission_stage", iDebugMissionStage)
			
			ADD_WIDGET_FLOAT_SLIDER("fRailHeading", fRailHeading, -360.0, 360.0, 0.1)
						
			ADD_WIDGET_VECTOR_SLIDER("vTrainAttachOffset", vTrainAttachOffset, -100.0, 100.0, 0.1)
			
			ADD_WIDGET_VECTOR_SLIDER("vFlatbedAttachRotation", vFlatbedAttachRotation, -360.0, 360.0, 1.0)
				
			ADD_WIDGET_BOOL("bDebugBoxes", bDebugBoxes)	
			ADD_WIDGET_BOOL("bFreightBox", bFreightBox)	
			ADD_WIDGET_BOOL("bFlatbedBox", bFlatbedBox)
			ADD_WIDGET_BOOL("bMagnetExtra", bMagnetExtra)
			
			ADD_WIDGET_BOOL("bIsAligned", bIsAligned)
			ADD_WIDGET_BOOL("bIsAligned180", bIsAligned180)
			
		STOP_WIDGET_GROUP()		
		
		SET_LOCATES_HEADER_WIDGET_GROUP(bsPrepWidgetGroup)
	
		SkipMenuStruct[ENUM_TO_INT(STAGE_TIME_LAPSE)].bSelectable = FALSE
		SkipMenuStruct[ENUM_TO_INT(STAGE_INITIALISE)].bSelectable = FALSE
				
		SkipMenuStruct[ENUM_TO_INT(STAGE_GET_TO_TRAIN_SWITCH)].sTxtLabel 			= "Take out the guards /Get Switch"              
		SkipMenuStruct[ENUM_TO_INT(STAGE_WAIT_FOR_TRAIN)].sTxtLabel 				= "Wait for train"              
		SkipMenuStruct[ENUM_TO_INT(STAGE_TRAIN_COMING_INTO_SIDINGS_CUTSCENE)].sTxtLabel 	= "Train Switch Cutscene"              
		SkipMenuStruct[ENUM_TO_INT(STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING)].sTxtLabel 	= "Wait for train to stop in siding"              
		SkipMenuStruct[ENUM_TO_INT(STAGE_FLY_AWAY_WITH_CARRIAGE)].sTxtLabel 		= "Fly away with carriage"        
		SkipMenuStruct[ENUM_TO_INT(STAGE_MISSION_PASSED)].sTxtLabel 				= "STAGE_MISSION_PASSED" 				
		SkipMenuStruct[ENUM_TO_INT(STAGE_DEBUG)].sTxtLabel 							= "STAGE_DEBUG"                
				
		bDebugInitialised = TRUE
	ENDIF
	
	IF bDebugBoxes
		SET_SKYLIFT_FLATBED_COLLISION_BOX(bFreightBox)
		SET_SKYLIFT_FREIGHT_COLLISION_BOX(bFlatbedBox)
		SET_SKYLIFT_BIG_MAGNET(bMagnetExtra)
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)

		FLOAT fX, fY, fZ, fW
		VECTOR vCoords
		
		ENTITY_INDEX thisCar = GET_FOCUS_ENTITY_INDEX()
		IF DOES_ENTITY_EXIST(thisCar)
			GET_ENTITY_QUATERNION(thisCar, fX, fY, fZ, fW)
			vCoords = GET_ENTITY_COORDS(thiscar)
		
			SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_QUATERNION(thisCar,") SAVE_FLOAT_TO_DEBUG_FILE(fX)
			SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fY)
			SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fZ)
			SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fW)
			SAVE_STRING_TO_DEBUG_FILE(")")
			
			
			SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_COORDS(thisCar,") SAVE_VECTOR_TO_DEBUG_FILE(vCoords) 
			SAVE_STRING_TO_DEBUG_FILE(")")
		ENDIF
		
	ENDIF
		
	
	IF bDebugOn
		IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, ENUM_TO_INT(mission_stage), TRUE)
												
			STOP_CUTSCENE(TRUE)
			WHILE NOT HAS_CUTSCENE_FINISHED()
				WAIT(0)
			ENDWHILE
//			WHILE IS_CUTSCENE_ACTIVE()
//				WAIT(0)
//			ENDWHILE
			REMOVE_CUTSCENE()
			
			DO_SCREEN_FADE_OUT(500)
			
			WHILE NOT IS_SCREEN_FADED_OUT()
				WAIT(0)
			ENDWHILE
			
			REMOVE_CUTSCENE()
			
			DESTROY_CAM(sTimelapse.splineCamera)
			
			NEW_LOAD_SCENE_STOP()
			
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			
			DELETE_ALL_ENTITIES()
			
			KILL_ANY_CONVERSATION()
			
			SET_CUTSCENE_RUNNING(FALSE)
		
			i_current_event = 0
			
			CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
			
			CLEAR_ALL_FLOATING_HELP()
		
			//Set stage enum to required stage
			skip_mission_stage = INT_TO_ENUM(MISSION_STAGE_FLAG, iReturnStage)
						
			SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
				
			//Do additional work when selecting stagePl
			MANAGE_SKIP(skip_mission_stage, FALSE)
			mission_stage = skip_mission_stage
			
			RESET_GAME_CAMERA()
			
			//When a stage has been selected from the menu LAUNCH_MISSION_STAGE_MENU() returns true it passes back the stage that has been selected as an INT iReturnStage
			//This INT can then be used to change the current mission stage using your J/P skip functions.
			// iReturnStage = 0 is returned if STAGE_1 is selected from the menu.
		ENDIF
	

		IF IS_KEYBOARD_KEY_PRESSED(KEY_S)		
			MISSION_PASSED()
		ENDIF
	
		IF IS_KEYBOARD_KEY_PRESSED(KEY_f)	
			Mission_Failed()
		ENDIF
			
	ENDIF

ENDPROC

#ENDIF	

VECTOR vHeliRotation 

//PURPOSE:		Checks if the user has failed a level stage
FUNC BOOL HAS_MISSION_FAILED()
		
	SWITCH mission_stage

		CASE STAGE_TIME_LAPSE
		BREAK
		
		CASE STAGE_GET_TO_TRAIN_SWITCH
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<2628.9084, 2947.6255, 39.4280>>) > 270.0
				MISSION_FLOW_SET_FAIL_REASON("BSPD_ABANDON")
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING
		CASE STAGE_FLY_AWAY_WITH_CARRIAGE
		
			IF DOES_ENTITY_EXIST(viSkyLift)
				IF NOT IS_VEHICLE_DRIVEABLE(viSkyLift)
				//OR IS_VEHICLE_PERMANENTLY_STUCK(viSkyLift)
					MISSION_FLOW_SET_FAIL_REASON("FAILHELI")
					RETURN TRUE
				ELSE
				
					IF VMAG(GET_ENTITY_VELOCITY(viSkyLift)) < 0.5
						
						PRINTLN("vHeliRotation", vHeliRotation)
							
						vHeliRotation = GET_ENTITY_ROTATION(viSkyLift)
		
						IF (vHeliRotation.y > 80.0 AND vHeliRotation.y < 280.0)
						OR (vHeliRotation.x > 80.0 AND vHeliRotation.x < 280.0)
							MISSION_FLOW_SET_FAIL_REASON("FAILHELI")
							RETURN TRUE
						ENDIF
					ELSE
						PRINTLN("TOO FAST")
					ENDIF	
				
				ENDIF
			ENDIF	
			
			IF DOES_ENTITY_EXIST(viTrainIndex)
				IF IS_ENTITY_DEAD(viTrainIndex)
				OR IS_ENTITY_IN_WATER(viTrainIndex)
					MISSION_FLOW_SET_FAIL_REASON("FAILTRAIN")
					RETURN TRUE
				ENDIF
			ENDIF	
						
			IF IS_PLAYER_MICHAEL_OR_FRANKLIN()
				IF IS_PED_INJURED(pedSelector.pedID[SELECTOR_PED_TREVOR])
					MISSION_FLOW_SET_FAIL_REASON("CMN_TDIED")
					RETURN TRUE
				ENDIF
			ELSE
				IF IS_PED_INJURED(pedSelector.pedID[pedSelector.ePreviousSelectorPed])
					MISSION_FLOW_SET_FAIL_REASON("CMN_MDIED")
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(viTrailerDropOff1Cab)	
				IF IS_ENTITY_DEAD(viTrailerDropOff1Cab)
					MISSION_FLOW_SET_FAIL_REASON("BSPD_TRUCKDEAD")
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(piTruckDriver1)
				IF IS_PED_INJURED(piTruckDriver1)
					MISSION_FLOW_SET_FAIL_REASON("BSPD_CREWDEAD")
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(piTruckDriver2)
				IF IS_PED_INJURED(piTruckDriver2)
					MISSION_FLOW_SET_FAIL_REASON("BSPD_CREWDEAD")
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(viTrailerDropOff2Cab)
				IF IS_ENTITY_DEAD(viTrailerDropOff2Cab)
					MISSION_FLOW_SET_FAIL_REASON("BSPD_TRUCKDEAD")
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(viTrailerDropOff1)
				IF IS_ENTITY_DEAD(viTrailerDropOff1)
					MISSION_FLOW_SET_FAIL_REASON("BSPD_TRUCKDEAD")
					RETURN TRUE
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(viTrailerDropOff2)
				IF IS_ENTITY_DEAD(viTrailerDropOff2)
					MISSION_FLOW_SET_FAIL_REASON("BSPD_TRUCKDEAD")
					RETURN TRUE
				ENDIF
			ENDIF
			
			
		BREAK
			
	ENDSWITCH
	
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
		WAIT(2000)
		MISSION_FLOW_SET_FAIL_REASON("FAILGUARD")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

// ____________________________________ MISSION SCRIPT _____________________________________

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED()	
        Mission_Flow_Mission_Force_Cleanup()
		MISSION_CLEANUP()  
		TERMINATE_THIS_THREAD()
	ENDIF
	
	// ____________________________________ MISSION LOOP _______________________________________
	
	
	
	SET_MISSION_FLAG(TRUE)
		
	WHILE TRUE	
	
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_SDTRCK")
				
		#IF IS_DEBUG_BUILD
			debugProcedures()
			//mission_stage = STAGE_DEBUG	//Comment out...
		#ENDIF
					
		SWITCH mission_stage 
		
			#IF IS_DEBUG_BUILD
		
				CASE STAGE_DEBUG
					debugRoutines()
				BREAK
			
			#ENDIF
		
			//Stage 0		
			
			CASE STAGE_TIME_LAPSE
				timeLapseCutscene()
			BREAK
			
			CASE STAGE_INITIALISE	
				initialiseMission()		
			BREAK		
						
			CASE STAGE_GET_TO_TRAIN_SWITCH
				stageGetToTrainSwitch()		
			BREAK		
			
			CASE STAGE_WAIT_FOR_TRAIN
				stageWaitForTrain()
			BREAK
						
			CASE STAGE_TRAIN_COMING_INTO_SIDINGS_CUTSCENE
				stageTrainComingIntoSiding()
			BREAK	
			
			CASE STAGE_WAIT_FOR_TRAIN_TO_STOP_IN_SIDING
				stageWaitForTrainToStopInSiding()
			BREAK
			
			CASE STAGE_FLY_AWAY_WITH_CARRIAGE
				stageFlyAwayWithCarriage()
			BREAK	
			
			CASE STAGE_MISSION_PASSED
				mission_Passed()		
			BREAK	
			
			DEFAULT
				SCRIPT_ASSERT("Unrecognised Mission Stage!")
			BREAK
			
		ENDSWITCH	
		
		IF HAS_MISSION_FAILED()
			IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
				Mission_Failed()
			ENDIF
		ENDIF
				
		WAIT(0)
		
	ENDWHILE
	
	vFlatbedAttachRotation = vFlatbedAttachRotation	//release build???
	
	//	should never reach here - always ends by going through the cleanup function

ENDSCRIPT					



