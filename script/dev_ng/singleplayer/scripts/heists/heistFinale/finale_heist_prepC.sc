
//Compile out Title Update changes to header functions.
//Must be before includes.
CONST_INT 	USE_TU_CHANGES	1

USING "rage_builtins.sch"
USING "globals.sch"

USING "clearmissionarea.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_vehicle.sch"
USING "commands_path.sch"
USING "commands_ped.sch"
USING "commands_physics.sch"
USING "commands_script.sch"
USING "commands_task.sch"

USING "CompletionPercentage_public.sch"

using "dialogue_public.sch"

USING "flow_public_core_override.sch"
USING "flow_public_GAME.sch"

using "ped_component_public.sch"
USING "player_ped_public.sch"
USING "replay_public.sch"

USING "script_blips.sch"
USING "script_heist.sch"
USING "script_ped.sch"

USING "battlebuddy_public.sch"

USING "flow_special_event_checks.sch"

BOOL bJumpSkip = FALSE					//flag for if current MISSION state should clean up and move to the next state
USING "prep_mission_common.sch"
USING "taxi_functions.sch"
USING "event_public.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
	USING "shared_debug.sch"

	CONST_INT MAX_SKIP_MENU_LENGTH 2
	INT i_debug_jump_stage
	MissionStageMenuTextStruct s_skip_menu[MAX_SKIP_MENU_LENGTH]
#ENDIF

// ***************************************************************************************** 
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Template.sc
//		AUTHOR			:	Ste Kerrigan
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//----------------------
//    CHECKPOINTS
//----------------------
CONST_INT CP_AFTER_MOCAP                0 //following beverly down the road

//-----------------------
//		CONSTANTS
//-----------------------

/// --------------------------------------------------
///	ENUMS
/// -------------------------------------------

/// PURPOSE: Mission states
ENUM MISSION_STATE
	MS_SET_UP = 0,				//0
	MS_RETURN_WITH_THING,		//1
	MS_BUY_MODS,				//2
	MS_TAKE_CAR_TO_GARAGE,		//3
	MS_LEAVE_AREA,				//4
	MS_FAILED					//5
ENDENUM

/// PURPOSE: 
///    Internal state machine states for mission state
ENUM STAGE_STATES
	SS_INIT,
	SS_ACTIVE,
	SS_CLEANUP,
	SS_SKIPPED
ENDENUM

/// PURPOSE: Mission requirements used for loading 
///    and creating mission assets
ENUM MISSION_REQ
	RQ_NONE,
	RQ_TEXT,
	RQ_STEAL_CAR
ENDENUM

/// PURPOSE: Fail reason enums for picking correct fail reason
ENUM FAILED_REASONS
	FR_NONE,
	FR_WRECKED_STEAL_CAR,
	FR_ABAN_CAR,
	FR_STUCK_CAR,
	FR_NO_CASH,
	FR_NO_CASH_REPAIRS,
	FR_WANTED
ENDENUM

ENUM DOOR_STATE
	DS_CLOSED,
	DS_OPENING,
	DS_OPEN,
	DS_CLOSING
ENDENUM


///MISSION PED STATES
MISSION_STATE eMissionState = MS_SET_UP										//track what MISSION stage we are at
STAGE_STATES eState = SS_INIT												//Internal state tracking for mission stages
COP_MONITOR eCopMonitor = CM_MONITER
IN_STEAL_CAR_MONITOR eStealCarState = ISCM_NOT_IN_VEHICLE

//*****************************************************************************
//								:STRUCTS:
//*****************************************************************************

/// PURPOSE: Holds data for creating a 
///    vehicle in an encounter
STRUCT MYVEHICLE
	VEHICLE_INDEX 		id
	VECTOR 				vPos
	FLOAT 				fDir
	INT 				iColour
	MODEL_NAMES 		Mod = DUMMY_MODEL_FOR_SCRIPT
ENDSTRUCT

STRUCT GARAGE_DOOR
	INT 		id
	FLOAT 		fOpen
	
	VECTOR		vCarControlAreaPosA
	VECTOR		vCarControlAreaPosB
	
	VECTOR		vFootControlAreaPosA
	VECTOR		vFootControlAreaPosB
	
	FLOAT		fDoorWidth	
	
	VECTOR 		vBlocking[2]
	DOOR_STATE 	eDoorState = DS_CLOSED
ENDSTRUCT

GARAGE_DOOR mLeftDoor
GARAGE_DOOR mRightDoor


//****************************************************************************************************
//								: MISSION FLOW VARIABLES :
//****************************************************************************************************

//mission flow
INT iMissionState = 0					//Used in skips and checkpoints

//Fail vars
STRING sFailReason = NULL				//String to display when mission is failed

//GOD TEXT
STRING sGodText = "FINPRC"
BOOL bObjectiveShown = FALSE			//Has an objective been shown

//BLIPs
BLIP_INDEX biBlip						//Mission blip - mainly used for go to objectives
BLIP_INDEX biVehicleBlip
INT iStartSpentOnMods = -1
INT iCurrentSpendOnMods = -1
INT iSpentOnMods = 0


//ScriptCamera

VECTOR vSafeVec = <<0,0,0>>				//safe vector used when a proper position isnt needed 

VECTOR vModShop = <<-1154.0862, -2006.2974, 12.1803>>
VECTOR vGarage

VECTOR vGaragePosA = <<-442.56024, -2184.60547, 14.55648>> 
VECTOR vGaragePosB = <<-442.66803, -2172.49561, 9.39933>>
FLOAT fGarage = 13.47
BOOL bTurnedOnDoorSound = FALSE

///MISSION PED VARS SCRIPT AI

BOOL bDamageSpeedStatTurnedOn = FALSE
BOOL bGarageTimeStatTriggered = FALSE

BOOL bModsBought = FALSE

BOOL bDeletedBlip = FALSE

BOOL bForceStop = FALSE

BOOL bViewedGoldMenu = FALSE

//CHASE

///==============| GOD TEXT BOOLS |============
BOOL bExpireReturnVehWanted = FALSE
BOOL bExpireReturnVeh = FALSE


///===============| models |===================


/// ===============| START VECTORS |==============


/// ==============| START HEADINGS |===============

/// ==============| PED INDICES |================

/// ==============| VEHICLE INDICES |===========
VEHICLE_INDEX viStealCar
VEHICLE_INDEX viAnyValidTruck
MYVEHICLE mMissionCar

VEHICLE_INDEX viPastCars[3]
BOOL bCreatedPastVehicle[3]
VECTOR vPastSpawnPosition[3]
FLOAT fPastSpawnDir[3]
/// ===============| GROUPS |========================

/// ===============| DIALOGUE |======================

/// PURPOSE:
///    Resets all variables used in flow
PROC RESET_ALL()
	bExpireReturnVehWanted = FALSE
	bExpireReturnVeh = FALSE

	bDamageSpeedStatTurnedOn = FALSE
	bGarageTimeStatTriggered = FALSE
	
	eCopMonitor = CM_MONITER
	eStealCarState = ISCM_NOT_IN_VEHICLE
	bCreatedPastVehicle[0] = FALSE
	bCreatedPastVehicle[1] = FALSE
	bCreatedPastVehicle[2] = FALSE
ENDPROC

///
///    
///    DEBUG ONLY MISSION STUFF ------------------------------------------------------------
///    
///    
#IF IS_DEBUG_BUILD

	BOOL bShowDebugText = TRUE
	WIDGET_GROUP_ID widgetGroup
	BOOL bForceFail = FALSE
	/// PURPOSE:
	///    Prints a string to a TTY Channel
	/// PARAMS:
	///    s - The string to print
	///    ddc - The debug channel to print to
	PROC SK_PRINT(String s, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Prints a string and an int to a TTY Channel
	/// PARAMS:
	///    s - The string to print
	///    i - the int to print
	///    ddc - the debug channel to print to
	PROC SK_PRINT_INT(String s, INT i, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s,i)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Prints a string and a Float to a TTY Channel
	/// PARAMS:
	///    s - the string to print
	///    f - the float to print
	///    ddc - the debug channel 
	PROC SK_PRINT_FLOAT(String s, FLOAT f, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s,f)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Sets up a widget for this mission
	PROC SETUP_FOR_RAGE_WIDGETS()
		widgetGroup = START_WIDGET_GROUP("CURRENT Mission Widgets")
			START_WIDGET_GROUP("Debug")
				ADD_WIDGET_BOOL("Toggle Debug spew", bShowDebugText)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Force Mission Fail")
				ADD_WIDGET_BOOL("Force Fail", bForceFail)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		
	ENDPROC
	
	/// PURPOSE:
	///    Deletes the mission widget
	PROC CLEANUP_OBJECT_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
	ENDPROC 
	
	/// PURPOSE:
	///    Checks for any updates needed for the widgets
	PROC UPDATE_RAG_WIDGETS()
	
		IF bForceFail
			eMissionState = MS_FAILED
			eState = SS_INIT
			bForceFail = FALSE
		ENDIF	
	ENDPROC
	
	/// PURPOSE:
	///    Draws an angled area
	/// PARAMS:
	///    vec1 - first point of the area
	///    vec2 - second point of the area
	///    width - the width of the area
//	PROC DRAW_DEBUG_LOCATE_SPECIAL(VECTOR vec1, VECTOR vec2, FLOAT width)
//
//		VECTOR vBottom[2]
//		VECTOR vTop[2]
//		
//		vBottom[0] = vec1
//		vBottom[1] = vec2
//		
//		vTop[0] = vec1
//		vTop[1] = vec2
//		
//		IF vec1.z > vec2.z
//			vBottom[0].z = vec2.z
//			vBottom[1].z = vec2.z
//			vTop[0].z = vec1.z
//			vTop[1].z = vec1.z
//		ELSE
//			vBottom[0].z = vec1.z
//			vBottom[1].z = vec1.z
//			vTop[0].z = vec2.z
//			vTop[1].z = vec2.z
//		ENDIF
//
//		VECTOR fwd = NORMALISE_VECTOR(vBottom[1] - vBottom[0])      // normalize to get distance 
//		VECTOR side = <<-fwd.y, fwd.x, fwd.z>>
//		VECTOR w = side * (width / 2.0)
//
//		// Bottom points
//		VECTOR c1 = vBottom[0] - w  // base left
//		VECTOR c2 = vBottom[0] + w  // base right              
//		VECTOR c3 = vBottom[1] + w  // top rt
//		VECTOR c4 = vBottom[1] - w  // top lt
//
//		// Top points
//		VECTOR d1 = vTop[0] - w  // base left
//		VECTOR d2 = vTop[0] + w  // base right              
//		VECTOR d3 = vTop[1] + w  // top rt
//		VECTOR d4 = vTop[1] - w  // top lt
//
//		// Draw bottom lines
//		DRAW_DEBUG_LINE(c1, c2, 128, 0, 128)
//		DRAW_DEBUG_LINE(c2, c3, 128, 0, 128)
//		DRAW_DEBUG_LINE(c3, c4, 128, 0, 128)
//		DRAW_DEBUG_LINE(c4, c1, 128, 0, 128)
//		// Draw top lines
//		DRAW_DEBUG_LINE(d1, d2, 128, 0, 128)
//		DRAW_DEBUG_LINE(d2, d3, 128, 0, 128)
//		DRAW_DEBUG_LINE(d3, d4, 128, 0, 128)
//		DRAW_DEBUG_LINE(d4, d1, 128, 0, 128)
//		// Draw uprights
//		DRAW_DEBUG_LINE(c1, d1, 128, 0, 128)
//		DRAW_DEBUG_LINE(c2, d2, 128, 0, 128)
//		DRAW_DEBUG_LINE(c3, d3, 128, 0, 128)
//		DRAW_DEBUG_LINE(c4, d4, 128, 0, 128)
//		
//	ENDPROC
	
#ENDIF

					///
					///    
					///    GENERAL HELP FUNCTIONS
					///    
					///    


/// PURPOSE:
///    Set a peds position and heading safely ie check its ok then move it and set its heading
/// PARAMS:
///    index - The ped to move
///    pos - The position to move it to
///    dir - The Heading to set
PROC SET_PED_POS(PED_INDEX index, VECTOR pos, FLOAT dir)
	IF IS_PED_UNINJURED(index)
		CLEAR_PED_TASKS(index)
	ENDIF

	SAFE_TELEPORT_ENTITY(index, pos, dir)
ENDPROC


/// PURPOSE:
///    Requests a model and waits for it to load
/// PARAMS:
///    _modname - The name of the model to load
///    _debugstring - used to debug 
///    i - used to debug
/// RETURNS:
///    TRUE when the model is loaded
FUNC BOOL REQUEST_AND_CHECK_MODEL(MODEL_NAMES _modname, STRING _debugstring, INT i = 0)


	REQUEST_MODEL(_modname)
	IF NOT Is_String_Null_Or_Empty(_debugstring)
	AND i <> -1
		#IF IS_DEBUG_BUILD
		  SK_PRINT_INT(_debugstring, i)
		#ENDIF
	ENDIF
	
	IF HAS_MODEL_LOADED(_modname)
		#IF IS_DEBUG_BUILD
			SK_PRINT("MODEL LOADED")
		#ENDIF
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Unloads a model  - Sets it as no longer needed
/// PARAMS:
///    _modname - The model to unload
PROC UNLOAD_MODEL(MODEL_NAMES _modname, BOOL bCheckLoaded = TRUE)
	IF bCheckLoaded
		IF HAS_MODEL_LOADED(_modname)
			SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
		ENDIF
	ELSE
		SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
	ENDIF
ENDPROC

/// PURPOSE:
///    Spawns a ped and returns true if it was succesful. Requests the model and unloads it 
/// PARAMS:
///    pedindex - The PED_INDEX to write the newly create ped to 
///    model - The model to load and use to create the ped
///    pos - The position the ped should be created
///    dir - The heading the ped should have when created
///    bUnloadAfterSpawn - If true we unload the model after spawning the ped
///    bFreeze - if true we freeze the ped after spawning
///    bTempEvents - if true we block temp events
///    bTargetable - if false we set the ped to not be targetted
/// RETURNS:
///    TRUE if the ped was created
FUNC BOOL SPAWN_PED(PED_INDEX &pedindex, MODEL_NAMES model, VECTOR pos, FLOAT dir,   BOOL bUnloadAfterSpawn = TRUE, BOOL bFreeze = FALSE, BOOL bTempEvents = TRUE, BOOL bTargetable = TRUE)
	IF NOT DOES_ENTITY_EXIST(pedindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			pedindex = CREATE_PED(PEDTYPE_MISSION, model, pos, dir)
			//SET_PED_DEFAULT_COMPONENT_VARIATION(pedindex)
			IF IS_PED_UNINJURED(pedindex)
				#IF IS_DEBUG_BUILD SK_PRINT("PED CREATED !") #ENDIF
				IF bFreeze
					FREEZE_ENTITY_POSITION(pedindex, TRUE)
				ENDIF
				
				IF bTempEvents
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedindex, bTempEvents)
				ENDIF
				
				IF NOT bTargetable
					SET_PED_CAN_BE_TARGETTED(pedindex, bTargetable)
				ENDIF
				

				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF IS_PED_UNINJURED(pedindex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Spawns a vehicle and returns true if it was succesful - why does the is mission entity check return true
///    no matter what the out come of said check?
/// PARAMS:
///    vehicleindex - The VEHICLE_INDEX to write the newly create vehicle to 
///    model - The model to load and use to create the vehicle
///    pos - The position the vehicle should be created
///    dir - The heading the vehicle should have when created
/// RETURNS:
///    TRUE if the vehicle was created
FUNC BOOL SPAWN_VEHICLE(VEHICLE_INDEX &vehicleindex, MODEL_NAMES model, VECTOR pos, FLOAT dir, BOOL bUnloadAfterSpawn = TRUE)
	IF NOT DOES_ENTITY_EXIST(vehicleindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			vehicleindex = CREATE_VEHICLE(model, pos, dir)
			
			IF DOES_ENTITY_EXIST(vehicleindex)
				IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicleindex)
					SET_ENTITY_AS_MISSION_ENTITY(vehicleindex)
				ENDIF
				SET_VEHICLE_ON_GROUND_PROPERLY(vehicleindex)
				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
				
			ENDIF
		ENDIF
	ELSE
		IF IS_VEHICLE_OK(vehicleindex)
			SET_ENTITY_COORDS(vehicleindex, pos)
			SET_ENTITY_HEADING(vehicleindex, dir)
			SET_VEHICLE_ON_GROUND_PROPERLY(vehicleindex)
		ENDIF
		IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicleindex)
			SET_ENTITY_AS_MISSION_ENTITY(vehicleindex)
			RETURN TRUE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Spawns a ped inside a vehicle
/// PARAMS:
///    pedindex - The index to write the newly created ped back to (ref)
///    Car - The vehicle to create the ped in
///    modelName - The model of the ped to create
///    seat - the seat to create the ped in 
///    bUnloadAfterSpawn - should we unload the ped model after creation
/// RETURNS:
///    TRUE if the ped was spawned correctly
FUNC BOOL SPAWN_PED_IN_VEHICLE(PED_INDEX &pedindex, VEHICLE_INDEX Car, MODEL_NAMES modelName, VEHICLE_SEAT seat = VS_DRIVER, BOOL bUnloadAfterSpawn = TRUE)
	IF IS_VEHICLE_OK(Car)
		IF NOT DOES_ENTITY_EXIST(pedindex)
			IF REQUEST_AND_CHECK_MODEL(modelName,"Loading")
				pedindex = CREATE_PED_INSIDE_VEHICLE(Car, PEDTYPE_MISSION, modelName, seat)
				
				IF DOES_ENTITY_EXIST(pedindex)
					IF bUnloadAfterSpawn
						UNLOAD_MODEL(modelName)
					ENDIF
					RETURN TRUE
				ENDIF

			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Print a warning God text string and expires a bool when the warning has been shown
/// PARAMS:
///    WarnObj - the text key of the wanrning
///    expire - the bool to change
PROC PRINT_WARNING_OBJ(STRING WarnObj, BOOL &expire)
	IF NOT expire
		PRINT_NOW(WarnObj, DEFAULT_GOD_TEXT_TIME,0)
		expire = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Gets the heading between to vectors
/// PARAMS:
///    V1 - First vector
///    V2 - Second vector
/// RETURNS:
///    FLOAT -  the heading between the two
FUNC FLOAT GET_HEADING_BETWEEN_VECTORS(VECTOR V1, VECTOR V2)
	RETURN GET_HEADING_FROM_VECTOR_2D(V2.x-V1.x,V2.y-V1.y)
ENDFUNC


///-----------------------------------------------------------------------------------
///    						MISSION FUNCTIONS
///-----------------------------------------------------------------------------------

/// PURPOSE:
///    Checks that 2 entities are at the same height - within
///    1.5m
/// PARAMS:
///    e1 - first entity
///    e2 - second entity
/// RETURNS:
///    TRUE if the entities are at the same height
FUNC BOOL ARE_ENTITYS_AT_SAME_HEIGHT(ENTITY_INDEX e1, ENTITY_INDEX e2)
	VECTOR vE1 = GET_ENTITY_COORDS(e1)
	VECTOR vE2 = GET_ENTITY_COORDS(e2)
	FLOAT fDiff = ABSF(vE1.z - vE2.z) //want the absolute difference so it doesnt matter which entitys height was taken away first 
	IF fDiff <= 1.5
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Checks that the player is with in range of the hunter used when playing dialogue
/// PARAMS:
///    distance - The distance the player must be with in - defaults to 10.0
/// RETURNS:
///    TRUE if the player is with in the distance 
FUNC BOOL IS_IN_CONV_DISTANCE(PED_INDEX ped, FLOAT distance = 10.0)
	IF ped = NULL
		RETURN TRUE
	ENDIF
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
	AND IS_PED_UNINJURED(ped)
//		#IF IS_DEBUG_BUILD SK_PRINT_FLOAT(" DISTANCE BETWEEN CONVO  === ", GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ped, FALSE))#ENDIF
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ped, FALSE) <= distance
		AND ARE_ENTITYS_AT_SAME_HEIGHT(ped, PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC ENUM_MISSION_STATS GET_MISSION_MAPPED_STAT()
	SP_MISSIONS mis = MISSION_FLOW_GET_RUNNING_MISSION()
	ENUM_MISSION_STATS stat

	IF  mis = SP_HEIST_FINALE_PREP_C1
		stat = FINPC1_MAPPED
	ELIF  mis = SP_HEIST_FINALE_PREP_C2
		stat = FINPC2_MAPPED
	ELIF  mis = SP_HEIST_FINALE_PREP_C3
		stat = FINPC3_MAPPED
	ENDIF

	RETURN stat
ENDFUNC

FUNC ENUM_MISSION_STATS GET_MISSION_DAMAGED_STAT()
	SP_MISSIONS mis = MISSION_FLOW_GET_RUNNING_MISSION()
	ENUM_MISSION_STATS stat

	IF  mis = SP_HEIST_FINALE_PREP_C1
		stat = FINPC1_CAR_DAMAGE
	ELIF  mis = SP_HEIST_FINALE_PREP_C2
		stat = FINPC2_CAR_DAMAGE
	ELIF  mis = SP_HEIST_FINALE_PREP_C3
		stat = FINPC3_CAR_DAMAGE
	ENDIF

	RETURN stat
ENDFUNC

FUNC ENUM_MISSION_STATS GET_MISSION_TIME_STAT()
	SP_MISSIONS mis = MISSION_FLOW_GET_RUNNING_MISSION()
	ENUM_MISSION_STATS stat

	IF  mis = SP_HEIST_FINALE_PREP_C1
		stat = FINPC1_TIME_TO_GARAGE
	ELIF  mis = SP_HEIST_FINALE_PREP_C2
		stat = FINPC2_TIME_TO_GARAGE
	ELIF  mis = SP_HEIST_FINALE_PREP_C3
		stat = FINPC3_TIME_TO_GARAGE
	ENDIF

	RETURN stat
ENDFUNC

FUNC INT GET_CURRENT_PLAYER_TOTAL_SPENT_ON_MODS()

	IF GET_CURRENT_PROPERTY_OWNER(PROPERTY_CAR_MOD_SHOP) = GET_CURRENT_PLAYER_PED_ENUM()
		RETURN g_iCarmodItemsPurchasedThisSession
	ENDIF
	
	INT i
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
			CASE CHAR_MICHAEL
				STAT_GET_INT(SP0_MONEY_SPENT_CAR_MODS , i)
			BREAK

			CASE CHAR_FRANKLIN
				STAT_GET_INT(SP1_MONEY_SPENT_CAR_MODS , i)
			BREAK

			CASE CHAR_TREVOR
				STAT_GET_INT(SP2_MONEY_SPENT_CAR_MODS , i)
			BREAK
		
		ENDSWITCH
	
	RETURN i
ENDFUNC

PROC MONITOR_PLAYER_SPENT_ON_MODS()
	IF iStartSpentOnMods != -1
		iStartSpentOnMods = GET_CURRENT_PLAYER_TOTAL_SPENT_ON_MODS()

		IF iCurrentSpendOnMods != iStartSpentOnMods
			iSpentOnMods += (iStartSpentOnMods - iCurrentSpendOnMods)
			CPRINTLN(DEBUG_MISSION, "iSpentOnMods updated BY = ", (iStartSpentOnMods - iCurrentSpendOnMods))
			CPRINTLN(DEBUG_MISSION, "iSpentOnMods = ", iSpentOnMods)
			iCurrentSpendOnMods = iStartSpentOnMods
		ENDIF
	ENDIF
ENDPROC

PROC SET_STATS_WATCH_OFF()
	INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL) 
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL, GET_MISSION_DAMAGED_STAT()) 
ENDPROC

FUNC BOOL IS_PLAYER_BROWSING_IN_ANY_MOD_SHOP()
// Car Mod - AMB1 (v_carmod)
	IF IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_01_AP)
	// Car Mod - AMB2 (v_lockup)
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_05_ID2)
	// Car Mod - AMB3 (v_carmod)
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_06_BT1)
	// Car Mod - AMB4 (v_carmod3)
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_07_CS1)
	// Car Mod - AMB5 (v_carmod3)
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_08_CS6)
	// Car Mod - AMB6 (lr_supermod_int)
	OR IS_PLAYER_BROWSING_ITEMS_IN_SHOP(CARMOD_SHOP_SUPERMOD)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC



PROC MANAGE_PLAYER_VEHICLE_STATS()
	IF NOT bDamageSpeedStatTurnedOn
		IF IS_VEHICLE_OK(viStealCar)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viStealCar)
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(viStealCar) 
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(viStealCar, GET_MISSION_DAMAGED_STAT())
				CPRINTLN(DEBUG_MISSION, "Set damage stat watcher to steal car")
				bDamageSpeedStatTurnedOn = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bGarageTimeStatTriggered
		IF eMissionState >= MS_TAKE_CAR_TO_GARAGE
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(GET_MISSION_TIME_STAT())
			CPRINTLN(DEBUG_MISSION, "opened get to garage stat window")
			bGarageTimeStatTriggered = TRUE
		ENDIF
	ENDIF
	
	IF eMissionState >= MS_BUY_MODS
		IF IS_PLAYER_BROWSING_IN_ANY_MOD_SHOP()
		AND eStealCarState = ISCM_IN_VEHICLE
		OR	eStealCarState = ISCM_IN_ANY_VEHICLE
			MONITOR_PLAYER_SPENT_ON_MODS()
		ENDIF
	ENDIF

ENDPROC

PROC MONITER_STATS()
	MANAGE_PLAYER_VEHICLE_STATS()
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Called when the player has failed 
///    deletes blips and clears the screen of text and conversations are ended
/// PARAMS:
///    fail - An Enum of the fail reason used in a switch statment to pick the correct text to display
PROC MISSION_FAILED(FAILED_REASONS fail = FR_NONE)
	
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()

	SWITCH fail
		CASE FR_NONE

		BREAK
		
		CASE FR_WRECKED_STEAL_CAR
			sFailReason = "PRF_FWRECK"
		BREAK
		
		CASE FR_ABAN_CAR
			sFailReason = "PRF_FFAR"
		BREAK
		
		CASE FR_STUCK_CAR
			sFailReason = "PRF_FSTUCK"
		BREAK
		
		CASE FR_NO_CASH
			sFailReason = "PRF_CASH"
		BREAK
		
		CASE FR_WANTED
			sFailReason = "PRF_FKICKOFF"
		BREAK
		
		CASE FR_NO_CASH_REPAIRS
			sFailReason = "PRF_CASH"
		BREAK
	ENDSWITCH
	
	eMissionState = MS_FAILED
	eState = SS_INIT
	
ENDPROC

///-----------------------------------------------------------------------------------
///    						STATE MACHINES
///-----------------------------------------------------------------------------------

/// PURPOSE:
///    Sets the mission check point 
PROC SET_CHECKPOINT()

ENDPROC

/// PURPOSE:
///    Turn off the ambient services 
PROC SERVICES_TOGGLE(BOOL bOn)

	//Wanted
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, bOn)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, bOn)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, bOn)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, bOn)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, bOn)

	IF bOn
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_MAX_WANTED_LEVEL(5)
	ELSE	
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0)
	ENDIF
ENDPROC

/// PURPOSE:
///    Loads a scene around a point used to the player 
///    seeing the map stream in when starting a replay or debug skipping
/// PARAMS:
///    pos - the position to stream the scene around at
///    rad - the radius of the scene to load
PROC LOAD_SCENE_SKIP(VECTOR pos, FLOAT rad)
	NEW_LOAD_SCENE_START_SPHERE(pos, rad)
	INT i_load_scene_timer = GET_GAME_TIMER()
	WHILE (NOT IS_NEW_LOAD_SCENE_LOADED()) AND (GET_GAME_TIMER() - i_load_scene_timer < 12000)
		WAIT(0)
	ENDWHILE
	NEW_LOAD_SCENE_STOP()
ENDPROC

/// PURPOSE:
///    Adds peds for dialogue based off the Mission state
PROC ADD_PEDS_FOR_DIALOGUE()

ENDPROC

FUNC INT GET_TRIGGER_VEHICLE_FOR_PREP()
	SWITCH MISSION_FLOW_GET_RUNNING_MISSION()
		CASE SP_HEIST_FINALE_PREP_C1
			CPRINTLN(DEBUG_MISSION, "Returning vehicle 0 for ", GET_THIS_SCRIPT_NAME())
			RETURN 0
		BREAK
		CASE SP_HEIST_FINALE_PREP_C2
			CPRINTLN(DEBUG_MISSION, "Returning vehicle 1 for ", GET_THIS_SCRIPT_NAME())
			RETURN 1
		BREAK
		CASE SP_HEIST_FINALE_PREP_C3
			CPRINTLN(DEBUG_MISSION, "Returning vehicle 2 for ", GET_THIS_SCRIPT_NAME())
			RETURN 2
		BREAK
	ENDSWITCH
	
	CPRINTLN(DEBUG_MISSION, "Trigger invalid for Prep returning safe value")
	RETURN 0
ENDFUNC


FUNC BOOL IS_PLAYER_IN_CORRECT_MODEL_ON_START()
	
	IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP()])
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP()])
			g_replay.iReplayInt[0] = -1
			RETURN FALSE
		ENDIF
	ENDIF

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF IS_VEHICLE_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), GAUNTLET)
			SET_ENTITY_AS_MISSION_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), TRUE, TRUE)
			viStealCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			SET_VEHICLE_AS_RESTRICTED(viStealCar, 0)
			IF IS_VEHICLE_OK(viStealCar)
				g_replay.iReplayInt[0] = 1
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PICK_REPLY_POS()
	VECTOR vPos
	FLOAT fHead
	
	SWITCH MISSION_FLOW_GET_RUNNING_MISSION()
		CASE SP_HEIST_FINALE_PREP_C1
			vPos = <<-311.1739, -771.6993, 52.2467>>
			fHead = 182.1060
			CPRINTLN(DEBUG_MISSION, " vPos fHead SP_HEIST_FINALE_PREP_C1")
		BREAK
		CASE SP_HEIST_FINALE_PREP_C2
			vPos = <<-657.8753, -272.2921, 34.7628>> 
			fHead = 30.6575
			CPRINTLN(DEBUG_MISSION, " vPos fHead SP_HEIST_FINALE_PREP_C2")
		BREAK
		CASE SP_HEIST_FINALE_PREP_C3
			vPos = <<307.1230, -1084.8005, 28.3597>>
			fHead = 299.7018
			CPRINTLN(DEBUG_MISSION, " vPos fHead SP_HEIST_FINALE_PREP_C3")
		BREAK
	ENDSWITCH
	IF g_replay.iReplayInt[0] != 1
		IF NOT IS_REPLAY_BEING_SET_UP()
		AND NOT IS_REPLAY_IN_PROGRESS()
		AND NOT IS_REPEAT_PLAY_ACTIVE()
			SET_PED_POS(PLAYER_PED_ID(), vPos, fHead)
		ELSE
			START_REPLAY_SETUP(vPos, fHead)
		ENDIF
	ELSE
		GET_MISSION_START_POS_AND_HEADING(vpos, fHead)
		IF NOT IS_REPLAY_BEING_SET_UP()
		AND NOT IS_REPLAY_IN_PROGRESS()
		AND NOT IS_REPEAT_PLAY_ACTIVE()
			SET_PED_POS(PLAYER_PED_ID(), vPos, fHead)
		ELSE
			START_REPLAY_SETUP(vPos, fHead)
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Sets up a stage requirment via a switch using an ENUM 
///    Stage requirements include the hunter the saleform or the mission text etc.
/// PARAMS:
///    missionReq - The Enum of the required mission element e.g. RQ_TEXT
///    pos - The position the thing is spawned at - if no position is required the use vSafeVec
///    		 If spawning multiple things at once then have the postions already in the switch statement
///    		 as calling this func multiple times wont work as well with the other function that calls it 
///    dir - This is the heading or direction you want the thing to face when spawned. Defaults to 
///    		 0.0 
/// RETURNS:
///    TRUE when the thing required is created/loaded/setup or whatever.
///    
FUNC BOOL SETUP_STAGE_REQUIREMENTS(MISSION_REQ missionReq,VECTOR pos, FLOAT dir=0.0)
	SWITCH missionReq
		CASE RQ_NONE
			IF ARE_VECTORS_ALMOST_EQUAL(pos, vSafeVec)
			AND dir = 0.0
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE RQ_TEXT
			REQUEST_ADDITIONAL_TEXT(sGodText, MISSION_TEXT_SLOT)
			
			IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_TEXT")
				#ENDIF
				RETURN TRUE
			ENDIF
			#IF IS_DEBUG_BUILD
				SK_PRINT("TEXT FAILED")
			#ENDIF
		BREAK
		
		CASE RQ_STEAL_CAR		
			IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP()])
			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP()], TRUE, TRUE)
				mMissionCar.id = g_sTriggerSceneAssets.veh[GET_TRIGGER_VEHICLE_FOR_PREP()]
				
				IF IS_VEHICLE_OK(mMissionCar.id)
					#IF IS_DEBUG_BUILD
						SK_PRINT("g_sTriggerSceneAssets - RQ_STEAL_CAR")
					#ENDIF
					viStealCar = mMissionCar.id
					SET_VEHICLE_AS_RESTRICTED(mMissionCar.id, 0)
					SET_VEHICLE_HAS_STRONG_AXLES(mMissionCar.id, TRUE)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, TRUE)
					RETURN TRUE
				ENDIF
			ELSE
				IF g_replay.iReplayInt[0] != 1
				OR IS_REPEAT_PLAY_ACTIVE()
					IF SPAWN_VEHICLE(mMissionCar.id, mMissionCar.Mod, mMissionCar.vPos, mMissionCar.fDir)
						#IF IS_DEBUG_BUILD
							SK_PRINT("RQ_STEAL_CAR")
						#ENDIF
						SET_VEHICLE_COLOUR_COMBINATION(mMissionCar.id, mMissionCar.iColour)
						SET_VEHICLE_AS_RESTRICTED(mMissionCar.id, 0)
						SET_VEHICLE_HAS_STRONG_AXLES(mMissionCar.id, TRUE)
						SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, TRUE)
						RETURN TRUE
					ENDIF
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD
				SK_PRINT("RQ_STEAL_CAR FAILED")
			#ENDIF
		BREAK
		
	ENDSWITCH
	RETURN FALSE
ENDFUNC

PROC CREATE_AMBIENT_REPLAY_CAR()
	IF g_replay.iReplayInt[0] = 1
		VECTOR vpos
		FLOAT fHed
		GET_MISSION_START_POS_AND_HEADING(vpos, fHed)
		IF NOT IS_VEHICLE_OK(mMissionCar.id)
			CREATE_VEHICLE_FOR_REPLAY(mMissionCar.id, vpos, fHed)
		ENDIF
		SET_VEHICLE_AS_RESTRICTED(mMissionCar.id, 0)
		SET_VEHICLE_HAS_STRONG_AXLES(mMissionCar.id, TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, TRUE)
		IF NOT IS_REPLAY_IN_PROGRESS()
			WAIT_FOR_WORLD_TO_LOAD(vpos, 80)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Used to setup a mission stage(or state if you call it that) Uses a switch statement to pick which 
///    set of SETUP_STAGE_REQUIREMENTS() to call. It checks to see if all the stage requirements are 
///    setup and then does any other setup needed. such as setting the players position or switching a
///    bool to true or false etc.
///    Handles setting up stuff needed after a Z or p skip first then the normal setup takes place
/// PARAMS:
///    eStage - The mission state/stage that needs setting up
///    bJumped - Wether or not the state/stage has been jumped to using Z or P skips
/// RETURNS:
///    TRUE if everything required for a stage is loaded properly
FUNC BOOL SETUP_MISSION_STAGE(MISSION_STATE eStage, BOOL bJumped = FALSE)
	SWITCH eStage
		CASE MS_SET_UP
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
				RC_END_Z_SKIP()
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_TEXT, vSafeVec)
					IF NOT IS_REPLAY_IN_PROGRESS()
						IF IS_PLAYER_IN_CORRECT_MODEL_ON_START()
							MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(MISSION_FLOW_GET_RUNNING_MISSION())
							//SET_VEHICLE_MODEL_IS_SUPPRESSED(TRASH, TRUE)
							//SET_VEHICLE_MODEL_IS_SUPPRESSED(TOWTRUCK, TRUE)
							RETURN TRUE
						ELSE
							IF SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR, vSafeVec)
								MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(MISSION_FLOW_GET_RUNNING_MISSION())
								//SET_VEHICLE_MODEL_IS_SUPPRESSED(TRASH, TRUE)
								//SET_VEHICLE_MODEL_IS_SUPPRESSED(TOWTRUCK, TRUE)
								RETURN TRUE //all mission stage requirements set up return true and activate stage
							ENDIF
						ENDIF
					ELSE
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_SET_UP, FAILED") 
			#ENDIF
		BREAK

		CASE MS_RETURN_WITH_THING
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				IF SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR, vSafeVec)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, TRUE)
					RESET_ALL()
					CREATE_AMBIENT_REPLAY_CAR()
					IF IS_VEHICLE_OK(mMissionCar.id)
						viStealCar = mMissionCar.id
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), mMissionCar.id)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(mMissionCar.id)
						IF IS_REPEAT_PLAY_ACTIVE()
							IF NOT DECOR_EXIST_ON(mMissionCar.id, "MapGauntlet")
								DECOR_SET_INT(mMissionCar.id, "MapGauntlet", 0)
							ENDIF
						ENDIF
					ENDIF

					IF NOT IS_REPLAY_IN_PROGRESS()
						PICK_REPLY_POS()
						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					ENDIF

					IF NOT IS_REPLAY_BEING_SET_UP()
						bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
						RC_END_Z_SKIP()
					ELSE
						END_REPLAY_SETUP()
					ENDIF
				ENDIF
			ELSE
				RETURN TRUE //all mission stage requirements set up return true and activate stage
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_RETURN_WITH_THING, FAILED") 
			#ENDIF
		BREAK

		CASE MS_BUY_MODS
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				IF SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR, vModShop, 134.0846)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, TRUE)
					CREATE_AMBIENT_REPLAY_CAR()
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), mMissionCar.id)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()					
					RESET_ALL()
					bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
					WAIT_FOR_WORLD_TO_LOAD(vModShop)
					RC_END_Z_SKIP()
				ENDIF
			ELSE
				RETURN TRUE //all mission stage requirements set up return true and activate stage
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_RETURN_WITH_THING, FAILED") 
			#ENDIF
		BREAK
		
		CASE MS_TAKE_CAR_TO_GARAGE
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				IF SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR, vModShop, 134.0846)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, TRUE)
					CREATE_AMBIENT_REPLAY_CAR()
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), mMissionCar.id)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					RESET_ALL()
					WAIT_FOR_WORLD_TO_LOAD(vModShop)
					bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
					RC_END_Z_SKIP()
				ENDIF
			ELSE
				RETURN TRUE //all mission stage requirements set up return true and activate stage
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_TAKE_CAR_TO_GARAGE, FAILED") 
			#ENDIF
		BREAK

		CASE MS_LEAVE_AREA
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				IF SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR, vGarage, 181.3192)
					CREATE_AMBIENT_REPLAY_CAR()
					SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, TRUE)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), mMissionCar.id)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					RESET_ALL()
					WAIT_FOR_WORLD_TO_LOAD(vModShop)
					bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
					RC_END_Z_SKIP()
				ENDIF
			ELSE
				RETURN TRUE //all mission stage requirements set up return true and activate stage
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_LEAVE_AREA, FAILED") 
			#ENDIF
		BREAK

	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sits in the NEXT_STAGE() function and moniters for assest that
///    should be unloaded based on the mission state
/// PARAMS:
///    state - the current state we should evaluate
PROC MONITER_UNLOAD_ASSETS(MISSION_STATE state)
	SWITCH state
		CASE MS_SET_UP
			
		BREAK
		
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Unloads all models
PROC UNLOAD_ALL_MODELS()
	SET_MODEL_AS_NO_LONGER_NEEDED(mMissionCar.Mod)
ENDPROC

/// PURPOSE:
///    Unloads all waypoints 
PROC UNLOAD_ALL_WAYPOINTS()
ENDPROC

/// PURPOSE:
///    Unloads all anims
PROC UNLOAD_ANIMS()
ENDPROC

/// PURPOSE:
///    Unload all vehicle recordings 
PROC UNLOAD_ALL_CAR_RECS()
ENDPROC

/// PURPOSE:
///    Deletes any blips that are valid
PROC REMOVE_BLIPS()
	SAFE_REMOVE_BLIP(biBlip)
	SAFE_REMOVE_BLIP(biVehicleBlip)
ENDPROC

PROC CREATE_PAST_GAUNTLET_CAR(INT i, SP_MISSIONS sp)
	IF NOT bCreatedPastVehicle[i]
		IF GET_MISSION_COMPLETE_STATE(sp)
			IF SPAWN_VEHICLE(viPastCars[i], GAUNTLET, vPastSpawnPosition[i], fPastSpawnDir[i])
				bCreatedPastVehicle[i] = TRUE
				SET_VEHICLE_SETUP(viPastCars[i],  g_savedGlobals.sVehicleGenData.sHeistPrepVehicles[i])
				SET_VEHICLE_DOORS_LOCKED(viPastCars[i], VEHICLELOCK_LOCKED)
				#IF IS_DEBUG_BUILD SK_PRINT_INT("Created past vehicle = ", i) #ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC RELEASE_PAST_GAUNTLET_CAR(INT i)
	IF bCreatedPastVehicle[i]
		IF IS_VEHICLE_OK(viPastCars[i])
			#IF IS_DEBUG_BUILD SK_PRINT_INT("Releasing past vehicle = ", i) #ENDIF
			SAFE_DELETE_VEHICLE(viPastCars[i])
		ENDIF
	ENDIF
ENDPROC

PROC MONITOR_CREATE_PAST_GAUNTLETS()
	IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vGarage, 100)
		CREATE_PAST_GAUNTLET_CAR(0, SP_HEIST_FINALE_PREP_C1)
		CREATE_PAST_GAUNTLET_CAR(1, SP_HEIST_FINALE_PREP_C2)
		CREATE_PAST_GAUNTLET_CAR(2, SP_HEIST_FINALE_PREP_C3)
	ELIF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vGarage, 120)
		RELEASE_PAST_GAUNTLET_CAR(0)
		RELEASE_PAST_GAUNTLET_CAR(1)
		RELEASE_PAST_GAUNTLET_CAR(2)
	ENDIF
ENDPROC


/// PURPOSE:
///    Standard delete all function used the wait for fail state
///    Safe deletes all peds, props and vehicles
PROC DELETE_ALL()
	SAFE_DELETE_VEHICLE(viStealCar)
	SAFE_DELETE_VEHICLE(mMissionCar.id)
	RELEASE_PAST_GAUNTLET_CAR(0)
	RELEASE_PAST_GAUNTLET_CAR(1)
	RELEASE_PAST_GAUNTLET_CAR(2)
ENDPROC

PROC RELEASE_ALL()
	SAFE_RELEASE_VEHICLE(viStealCar)
	SAFE_RELEASE_VEHICLE(mMissionCar.id)
	RELEASE_PAST_GAUNTLET_CAR(0)
	RELEASE_PAST_GAUNTLET_CAR(1)
	RELEASE_PAST_GAUNTLET_CAR(2)
ENDPROC

/// PURPOSE:
///    Deletes all mission entities and any other clean up
///    This is used to clear the mission when P or Z skipping
PROC CLEANUP(BOOL bDelAll = TRUE)
	IF bDelAll
		#IF IS_DEBUG_BUILD SK_PRINT("CLEANUP = DEL ALL") #ENDIF
	ELSE
		#IF IS_DEBUG_BUILD SK_PRINT("CLEANUP = RELEASE ") #ENDIF
	ENDIF

	KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	CLEAR_PRINTS()
	WAIT_FOR_CUTSCENE_TO_STOP()

	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_LEANING", TRUE)

	REMOVE_BLIPS()
	UNLOAD_ALL_MODELS()
	UNLOAD_ALL_WAYPOINTS()
	UNLOAD_ANIMS()
	UNLOAD_ALL_CAR_RECS()
	IF bDelAll
		IF DOES_ENTITY_EXIST(viStealCar)
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viStealCar)
						VECTOR vPos = GET_ENTITY_COORDS(PLAYER_PED_ID()) // get out of vehicle
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		DELETE_ALL()
	ELSE
		RELEASE_ALL()
	ENDIF
	SET_GPS_MULTI_ROUTE_RENDER(FALSE)

ENDPROC

/// PURPOSE:
///    Cleans up mission entities, releases the entity to be cleaned up by population
///    and will give a suitable task to the peds before clean up
PROC Script_Cleanup()

	SERVICES_TOGGLE(TRUE)
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)

	IF IS_DOOR_REGISTERED_WITH_SYSTEM(mRightDoor.id)
		SET_SCRIPT_UPDATE_DOOR_AUDIO(mRightDoor.id, FALSE)
	ENDIF

	IF IS_DOOR_REGISTERED_WITH_SYSTEM(mLeftDoor.id)
		SET_SCRIPT_UPDATE_DOOR_AUDIO(mLeftDoor.id, FALSE)
	ENDIF
	SET_STATS_WATCH_OFF()
	RESET_ALL_BATTLEBUDDY_BEHAVIOUR_REQUESTS()
	SET_MODEL_IS_SUPPRESSED_FOR_DURATION(GAUNTLET, TRUE, TRUE, 90000)
	UNREGISTER_SCRIPT_WITH_AUDIO()
	DISABLE_TAXI_HAILING(FALSE)
	CLEANUP(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//***************************************
//			:MISSION FLOW FUNC:
//***************************************
//PURPOSE: Advances or reverses the mission stage  
PROC NEXT_STAGE( BOOL bReverse = FALSE)
	MONITER_UNLOAD_ASSETS(eMissionState)
	iMissionState = ENUM_TO_INT(eMissionState)
	IF NOT bReverse
		eMissionState = INT_TO_ENUM(MISSION_STATE, (iMissionState + 1))
	ELSE
		IF iMissionState > 0
			eMissionState = INT_TO_ENUM(MISSION_STATE, (iMissionState - 1))		
		ENDIF
	ENDIF
	bObjectiveShown = FALSE
	eState = SS_INIT	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------



/// PURPOSE:
///    Pass function calls cleanup and termination 
///    if the player debug passes this function warps him to the end 
///    of the chasem, it will then complete
PROC Script_Passed()
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()
	
	DOOR_SYSTEM_SET_DOOR_STATE(mLeftDoor.id, DOORSTATE_FORCE_LOCKED_UNTIL_OUT_OF_AREA, TRUE, TRUE)
	DOOR_SYSTEM_SET_DOOR_STATE(mRightDoor.id, DOORSTATE_FORCE_LOCKED_UNTIL_OUT_OF_AREA, TRUE, TRUE)
	INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE, GET_MISSION_TIME_STAT())
	IF IS_VEHICLE_OK(viStealCar)
		IF DECOR_EXIST_ON(viStealCar, "MapGauntlet")
			INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(GET_MISSION_MAPPED_STAT())
		ELSE
			IF IS_REPLAY_IN_PROGRESS()
				IF g_replay.iReplayInt[0] = -1
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(GET_MISSION_MAPPED_STAT())
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	INFORM_MISSION_STATS_OF_FINANCIAL_DAMAGE(iSpentOnMods)
	SAFE_DELETE_VEHICLE(viStealCar)
	Mission_Flow_Mission_Passed()
	Script_Cleanup()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Waits for the screen to fade out then updates failed reason
PROC FAILED_WAIT_FOR_FADE()
	SWITCH eState
		CASE SS_INIT
			CLEAR_PRINTS()
			CLEAR_HELP()
			REMOVE_BLIPS()
			
			/* (not supported for story missions atm)
			// set if we want to delay the fade
			BOOL bDelayFade
			bDelayFade = FALSE
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sFailReason)
				bDelayFade = TRUE // delay the fade if we failed for one of these reasons
			ENDIF
			*/
			TRIGGER_MUSIC_EVENT("FHPRC_FAIL")

			IF NOT IS_STRING_NULL_OR_EMPTY(sFailReason)
				MISSION_FLOW_MISSION_FAILED_WITH_REASON(sFailReason)  
			ELSE
				MISSION_FLOW_MISSION_FAILED()
			ENDIF
			
			
			eState = SS_ACTIVE
		BREAK

		CASE SS_ACTIVE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				// Do a check here to see if we need to warp the player at all
				// (only set the fail warp locations if we can't leave the player where he was)
				//
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(mLeftDoor.id)
					CPRINTLN(DEBUG_MISSION, "shutting LEFT garage == ", mRightDoor.id)
					DOOR_SYSTEM_SET_OPEN_RATIO(mLeftDoor.id, 0.0)
					DOOR_SYSTEM_SET_DOOR_STATE(mLeftDoor.id, DOORSTATE_LOCKED, TRUE, TRUE)
				ENDIF
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(mRightDoor.id)
					CPRINTLN(DEBUG_MISSION, "shutting right garage == ", mRightDoor.id)
					DOOR_SYSTEM_SET_OPEN_RATIO(mRightDoor.id, 0.0)
					DOOR_SYSTEM_SET_DOOR_STATE(mRightDoor.id, DOORSTATE_LOCKED, TRUE, TRUE)
				ENDIF
				
				DELETE_ALL()
				Script_Cleanup()
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


//------------------------------------------------------------------------------------
//							MISSION STATES
//------------------------------------------------------------------------------------

/// PURPOSE:
///    Jumps the mission to a specific stage
/// PARAMS:
///    stage - The state to jump to 
PROC JUMP_TO_STAGE(MISSION_STATE stage)
	RC_START_Z_SKIP()
	bJumpSkip = TRUE //Tells the mission stage setup function that we have just jumped and special setup is required
	eMissionState = stage 
	IF eMissionState = MS_SET_UP
		#IF IS_DEBUG_BUILD SK_PRINT("eMission state = MSS_SETUP GOING TO INTRO ") #ENDIF
		eMissionState = MS_SET_UP
	ENDIF
	bObjectiveShown = FALSE
	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	eState = SS_INIT
	CLEANUP() //delete everything
ENDPROC

/// PURPOSE:
///    Fills a new MYVEHICLE
/// PARAMS:
///    pos - the position the vehicle is spawned at
///    dir - the heading it faces
///    mod - the model used to create it
/// RETURNS:
///    A newly initialised MYVEHICLE
FUNC MYVEHICLE FILL_VEHICLE(VECTOR pos, FLOAT dir, MODEL_NAMES mod, INT iColour)
	MYVEHICLE mTempVeh
	
	mTempVeh.vPos = pos
	mTempVeh.fDir = dir
	mTempVeh.Mod = mod
	mTempVeh.iColour = iColour
	
	RETURN mTempVeh 
ENDFUNC


PROC POPULATE_VEHICLES()
///GAUNTLETS NEED TO BE INITIALISED WITH DIFFERENT COORDS 
///BASED OFF THE MISSION WE ARE TRIGGERING

	SWITCH MISSION_FLOW_GET_RUNNING_MISSION()
		CASE SP_HEIST_FINALE_PREP_C1
			mMissionCar = FILL_VEHICLE(<<-311.1739, -771.6993, 52.2467>>, 182.1060, GAUNTLET, 13)
			vGarage = <<-442.18896, -2183.60278, 9.31981>>
			CPRINTLN(DEBUG_MISSION, "SP_HEIST_FINALE_PREP_C1")
		BREAK
		CASE SP_HEIST_FINALE_PREP_C2
			mMissionCar = FILL_VEHICLE(<<-657.8753, -272.2921, 34.7628>>, 30.6575, GAUNTLET, 4)
			vGarage = <<-442.18896, -2183.60278, 9.31981>>
			CPRINTLN(DEBUG_MISSION, "SP_HEIST_FINALE_PREP_C2")
		BREAK
		CASE SP_HEIST_FINALE_PREP_C3
			mMissionCar = FILL_VEHICLE(<<307.1230, -1084.8005, 28.3597>>, 299.7018, GAUNTLET, 0)
			vGarage = <<-442.18896, -2183.60278, 9.31981>>
			CPRINTLN(DEBUG_MISSION, "SP_HEIST_FINALE_PREP_C3")
		BREAK
		
	ENDSWITCH
	
	bCreatedPastVehicle[0] = FALSE
	bCreatedPastVehicle[1] = FALSE
	bCreatedPastVehicle[2] = FALSE

	vPastSpawnPosition[0] = <<-438.2076, -2181.5149, 9.3253>>
	vPastSpawnPosition[1] = <<-442.1008, -2181.2903, 9.3195>>
	vPastSpawnPosition[2] = <<-445.3427, -2181.1318, 9.3183>>

	fPastSpawnDir[0] = 181.2117
	fPastSpawnDir[1] = 180.0560
	fPastSpawnDir[2] = 181.0495
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    id - 
///    CarControlArea1 - 
///    CarControlArea2 - 
///    FootControlArea1 - 
///    FootControlArea2 - 
///    DoorWidth - 
///    GarageArea1 - 
///    GarageArea2 - 
///    GarageWidth - 
/// RETURNS:
///    
FUNC GARAGE_DOOR FILL_GARAGE_DOOR(INT id, VECTOR CarControlArea1, VECTOR CarControlArea2, VECTOR FootControlArea1, VECTOR FootControlArea2, VECTOR blocking, VECTOR blockinga)
	GARAGE_DOOR G
	
	G.id = id
	
	G.fOpen = 0.0
	G.vCarControlAreaPosA = CarControlArea1
	G.vCarControlAreaPosB = CarControlArea2
	
	G.vFootControlAreaPosA = FootControlArea1
	G.vFootControlAreaPosB = FootControlArea2

	G.fDoorWidth = 4.86
	
	G.vBlocking[0] = blocking
	G.vBlocking[1] = blockinga
	
	
	RETURN G
ENDFUNC

PROC POPULATE_GATE()
	
	mLeftDoor = FILL_GARAGE_DOOR(HASH("LeftLockup"), 
								<<-440.03522, -2172.50195, 9.39933>>, 
								<<-440.12354, -2166.84106, 12.31810>>, 
								<<-440.14792, -2166.36401, 9.31810>>, 
								<<-439.90460, -2174.04077, 12.39933>>,
								<<-442.25977, -2169.64331, 9.31843>>,
								<<-438.06152, -2173.34888, 13.31699>>)

	mRightDoor = FILL_GARAGE_DOOR(HASH("RightLockup"), 
								<<-445.36987, -2172.47021, 9.39933>>, 
								<<-445.32858, -2166.68848, 12.31802>>, 
								<<-445.49197, -2166.23169, 9.31686>>, 
								<<-445.21893, -2174.17310, 12.39933>>,
								<<-447.36536, -2169.83643, 9.30682>>,
								<<-443.38004, -2173.16504, 12.31856>>)
ENDPROC


PROC POP_CASH_SPENT_ON_MODS()
	iStartSpentOnMods = GET_CURRENT_PLAYER_TOTAL_SPENT_ON_MODS()
	iCurrentSpendOnMods = iStartSpentOnMods
ENDPROC


/// PURPOSE:
///    Initialises all variables and structs
PROC POPULATE_STUFF()
	POPULATE_VEHICLES()
	POPULATE_GATE()
ENDPROC


/// PURPOSE:
///    Sets the Game world time after a replay/shit skip
PROC SET_TIME_FOR_REPLAY()
	IF g_bShitskipAccepted
	ELSE
	ENDIF
ENDPROC


///PURPOSE: 
///    Initiate the mission and load the things needed 
///    for the immediate gameplay
///    The skip menu is initialsed here
///    And if a replay is being done then we init and load assests for the check point
PROC INITMISSION()
	SWITCH eState
		CASE SS_INIT
			//DO_SCREEN_FADE_OUT(0)
//			#IF IS_DEBUG_BUILD
//				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE (TRUE)
//			#ENDIF

			#IF IS_DEBUG_BUILD  SK_PRINT("INIT MISSION - THIS WILL LOOP")  #ENDIF

			IF SETUP_MISSION_STAGE(eMissionState) 			
				#IF IS_DEBUG_BUILD
					s_skip_menu[0].sTxtLabel = "MS_STEAL"
					s_skip_menu[1].sTxtLabel = "MS_RETURN_WITH_THING"
//					s_skip_menu[2].sTxtLabel = "MS_LEAVE_VEHICLE_AND_SHOP"
					
				#ENDIF

				SERVICES_TOGGLE(FALSE)
				SET_MAX_WANTED_LEVEL(1)
				SET_WANTED_LEVEL_MULTIPLIER(0.5)
				MARK_PREP_START_CAR_AS_VEH_GEN()
				SWITCH MISSION_FLOW_GET_RUNNING_MISSION()
					CASE SP_HEIST_FINALE_PREP_C1
						INIT_BATTLEBUDDY_BEHAVIOUR_FOR_MISSION(SP_HEIST_FINALE_PREP_C1)
						CPRINTLN(DEBUG_MISSION, "INIT_BATTLEBUDDY_BEHAVIOUR_FOR_MISSION SP_HEIST_FINALE_PREP_C1")
					BREAK
					CASE SP_HEIST_FINALE_PREP_C2
						INIT_BATTLEBUDDY_BEHAVIOUR_FOR_MISSION(SP_HEIST_FINALE_PREP_C2)
						CPRINTLN(DEBUG_MISSION, "INIT_BATTLEBUDDY_BEHAVIOUR_FOR_MISSION SP_HEIST_FINALE_PREP_C2")
					BREAK
					CASE SP_HEIST_FINALE_PREP_C3
						INIT_BATTLEBUDDY_BEHAVIOUR_FOR_MISSION(SP_HEIST_FINALE_PREP_C3)
						CPRINTLN(DEBUG_MISSION, "INIT_BATTLEBUDDY_BEHAVIOUR_FOR_MISSION SP_HEIST_FINALE_PREP_C3")
					BREAK
				ENDSWITCH
				
				IF IS_REPLAY_IN_PROGRESS()
					JUMP_TO_STAGE(MS_RETURN_WITH_THING)
				ELSE
					IF IS_REPEAT_PLAY_ACTIVE()
						PICK_REPLY_POS()
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), mMissionCar.id)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						GET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						IF NOT DECOR_EXIST_ON(mMissionCar.id, "MapGauntlet")
							DECOR_SET_INT(mMissionCar.id, "MapGauntlet", 0)
						ENDIF
						SAFE_FADE_SCREEN_IN_FROM_BLACK()
					ENDIF
					eState = SS_CLEANUP
				ENDIF
			ENDIF
		BREAK

		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP INIT MISSION")  #ENDIF
			NEXT_STAGE()
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL DOES_BUDDY_NEED_TO_DRIVE_TRUCK(PED_INDEX hPed)

	IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
	
		IF DOES_ENTITY_EXIST(viStealCar)
		AND IS_VEHICLE_DRIVEABLE(viStealCar)
		AND NOT IS_ENTITY_AT_COORD(viStealCar, vModShop, <<2,2,2>>)
					
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viStealCar)
			AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			
				IF IS_PED_IN_VEHICLE(hPed, viStealCar)
				AND GET_PED_IN_VEHICLE_SEAT(viStealCar, VS_DRIVER) = hPed

					RETURN TRUE
				
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MONITOR_BATTLE_BUDDIES()
	// For each playable character...
	enumCharacterList eChar
	REPEAT MAX_BATTLE_BUDDIES eChar
		
		PED_INDEX hBuddy = GET_BATTLEBUDDY_PED(eChar)
		
		IF NOT IS_PED_INJURED(hBuddy)
			IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddy)
			
				// Does script need to take control of buddy and drive to dest?
				IF IS_BATTLEBUDDY_AVAILABLE(hBuddy, FALSE)
					IF DOES_BUDDY_NEED_TO_DRIVE_TRUCK(hBuddy)
						IF OVERRIDE_BATTLEBUDDY(hBuddy, FALSE)
							SET_ENTITY_AS_MISSION_ENTITY(hBuddy, TRUE, TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddy, TRUE)
							CLEAR_PED_TASKS(hBuddy)
						ENDIF
					ENDIF
				ENDIF
			
			ELSE

				// Does script still need to take control of buddy and drive to dest?
				IF DOES_BUDDY_NEED_TO_DRIVE_TRUCK(hBuddy)
				AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddy)
					
					// Buddy drives truck to destination
					IF  GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
					AND GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
						TASK_VEHICLE_MISSION_COORS_TARGET(hBuddy, GET_VEHICLE_PED_IS_IN(hBuddy), vModShop, MISSION_GOTO, 20.0, DRIVINGMODE_AVOIDCARS, 2.0, 10.0)
					ENDIF
				
				ELSE
					RELEASE_BATTLEBUDDY(hBuddy)
				ENDIF
			
			ENDIF
		ENDIF
	
	ENDREPEAT
ENDPROC

PROC RELEASE_BATTLE_BUDDIES()
	// For each playable character...
	enumCharacterList eChar
	REPEAT MAX_BATTLE_BUDDIES eChar
		
		PED_INDEX hBuddy = GET_BATTLEBUDDY_PED(eChar)
		
		IF NOT IS_PED_INJURED(hBuddy)
			IF IS_BATTLEBUDDY_OVERRIDDEN(hBuddy)
				RELEASE_BATTLEBUDDY(hBuddy)
			ENDIF
		ENDIF
	
	ENDREPEAT
ENDPROC

FUNC STRING GET_MUSIC_EVENT_NAME(BOOL bStart)
	SP_MISSIONS mission = MISSION_FLOW_GET_RUNNING_MISSION()
	STRING sReturn
	IF bStart
		IF mission = SP_HEIST_FINALE_PREP_C1
			sReturn = "FHPRC1_START"
		ELIF mission = SP_HEIST_FINALE_PREP_C2
			sReturn = "FHPRC2_START"
		ELIF mission = SP_HEIST_FINALE_PREP_C3
			sReturn = "FHPRC3_START"
		ENDIF
	ELSE
		IF mission = SP_HEIST_FINALE_PREP_C1
			sReturn = "FHPRC1_END"
		ELIF mission = SP_HEIST_FINALE_PREP_C2
			sReturn = "FHPRC2_END"
		ELIF mission = SP_HEIST_FINALE_PREP_C3
			sReturn = "FHPRC3_END"
		ENDIF
	ENDIF

	RETURN sReturn
ENDFUNC

PROC RETURN_THING()
	MONITOR_IN_STEAL_CAR(viStealCar, viAnyValidTruck, eStealCarState, biVehicleBlip, biBlip, vModShop)
	SWITCH eState
		CASE SS_INIT
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				#IF IS_DEBUG_BUILD  SK_PRINT("INIT TAKE_VAN_BACK")  #ENDIF
				REPLAY_RECORD_BACK_FOR_TIME(5.0)

				IF DOES_VEHICLE_HAVE_GOLD_PREP_SETUP(viStealCar)
					SAFE_REMOVE_BLIP(biBlip)
					SAFE_REMOVE_BLIP(biVehicleBlip)
					bObjectiveShown = FALSE
					eState = SS_INIT
					eMissionState = MS_TAKE_CAR_TO_GARAGE
					bModsBought = TRUE
					EXIT
				ENDIF

				ADD_BLIP_LOCATION(biBlip, vModShop)
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0 
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRINT_OBJ("PRF_TAKBACK", bObjectiveShown)
				ENDIF
				
				REPLAY_RECORD_BACK_FOR_TIME(6.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				
				SERVICES_TOGGLE(TRUE)
				eState = SS_ACTIVE
			ENDIF
		BREAK

		CASE SS_ACTIVE
			IF IS_THIS_PRINT_BEING_DISPLAYED("PRF_TAKBACK")
				TRIGGER_MUSIC_EVENT(GET_MUSIC_EVENT_NAME(TRUE))
			ENDIF
			MONITER_PLAYER_WANTED(biBlip, vModShop, eCopMonitor, eStealCarState, bObjectiveShown, bExpireReturnVehWanted)
			MONITOR_BATTLE_BUDDIES()
			IF DOES_VEHICLE_HAVE_GOLD_PREP_SETUP(viStealCar)
				SAFE_REMOVE_BLIP(biBlip)
				SAFE_REMOVE_BLIP(biVehicleBlip)
				bObjectiveShown = FALSE
				eState = SS_INIT
				eMissionState = MS_TAKE_CAR_TO_GARAGE
				bModsBought = TRUE
				#IF IS_DEBUG_BUILD  SK_PRINT("jumping state state to take car to lock up")  #ENDIF
				EXIT
			ENDIF

			IF RETURN_STOLEN_VEHICLE(viStealCar, vModShop, biBlip, eCopMonitor, eStealCarState, bObjectiveShown, bExpireReturnVeh, FALSE, TRUE)
			OR IS_PLAYER_BROWSING_IN_ANY_MOD_SHOP()
				REPLAY_RECORD_BACK_FOR_TIME(10.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				eState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP TAKE_VAN_BACK")  #ENDIF
			SAFE_REMOVE_BLIP(biBlip)
			SAFE_REMOVE_BLIP(biVehicleBlip)
			IF STOP_MY_VEHICLE()
				IF IS_VEHICLE_OK(viStealCar)
					NEXT_STAGE()
				ENDIF				
			ENDIF
		BREAK
		
		CASE SS_SKIPPED
			IF IS_VEHICLE_OK(viStealCar)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viStealCar)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), viStealCar)
				ELSE
					SAFE_TELEPORT_ENTITY(viStealCar, vModShop, 146.5443)
					RC_END_Z_SKIP()
					eState = SS_ACTIVE
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

FUNC BOOL HAS_PLAYER_AQUIRED_MODS()
	IF GET_CURRENT_CARMOD_SHOP_MENU() != CMM_GOLD_PREP
		IF DOES_VEHICLE_HAVE_GOLD_PREP_SETUP(viStealCar)
			IF bViewedGoldMenu
				#IF IS_DEBUG_BUILD  SK_PRINT("Has prep mods")  #ENDIF
				RETURN TRUE
			ENDIF
		ELSE
			IF bViewedGoldMenu
				bViewedGoldMenu = FALSE
			ENDIF
		ENDIF
	ELSE
		IF NOT bViewedGoldMenu
			#IF IS_DEBUG_BUILD  SK_PRINT("In Gold prep menu")  #ENDIF
			bViewedGoldMenu = TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MONITOR_PLAYER_BUYING_MODS()
	IF IS_PLAYER_BROWSING_IN_ANY_MOD_SHOP()
	AND (eStealCarState = ISCM_IN_VEHICLE OR eStealCarState = ISCM_IN_ANY_VEHICLE)
		SAFE_REMOVE_BLIP(biBlip)
		IF HAS_PLAYER_AQUIRED_MODS()
			PRINT_OBJ("PRF_LEV", bObjectiveShown)
			bModsBought = TRUE
		ENDIF
	ENDIF
	
ENDPROC

PROC BUY_MODS()
	MONITOR_IN_STEAL_CAR(viStealCar, viAnyValidTruck, eStealCarState, biVehicleBlip, biBlip, vModShop)
	SWITCH eState
		CASE SS_INIT	
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				#IF IS_DEBUG_BUILD  SK_PRINT("INIT BUY()")  #ENDIF
				PRINT_OBJ("PRF_BUYPREP", bObjectiveShown)
				bObjectiveShown = FALSE
				bExpireReturnVeh = FALSE
				POP_CASH_SPENT_ON_MODS()
				eState = SS_ACTIVE
			ENDIF
		BREAK

		CASE SS_ACTIVE
//			MONITER_PLAYER_WANTED(biBlip, vModShop, eCopMonitor, eStealCarState, bObjectiveShown, bExpireReturnVehWanted)
				IF bModsBought											
					eState = SS_CLEANUP
				ELSE
//					IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vModShop, 75)
					IF eStealCarState = ISCM_NOT_IN_VEHICLE
						IF NOT bExpireReturnVeh
							IF IS_THIS_PRINT_BEING_DISPLAYED(GET_PREP_TAKE_VEHICLE_BACK_OBJ(MISSION_FLOW_GET_RUNNING_MISSION()))
								CLEAR_THIS_PRINT(GET_PREP_TAKE_VEHICLE_BACK_OBJ(MISSION_FLOW_GET_RUNNING_MISSION()))
							ENDIF
							PRINT_NOW(GET_PREP_RETURN_TO_VEHICLE_OBJ(MISSION_FLOW_GET_RUNNING_MISSION()), DEFAULT_GOD_TEXT_TIME, 0)
							
							bExpireReturnVeh = TRUE
						ENDIF
					ELSE
						IF NOT bDeletedBlip
							SAFE_REMOVE_BLIP(biBlip)
							bDeletedBlip = TRUE
						ENDIF
						IF IS_THIS_PRINT_BEING_DISPLAYED("PRF_LEV")
							CLEAR_THIS_PRINT("PRF_LEV")
						ENDIF
						ADD_BLIP_LOCATION(biBlip, vModShop, TRUE)
//					ENDIF
					ENDIF
				ENDIF
		BREAK
		
		CASE SS_CLEANUP
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)	
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP BUY()")  #ENDIF
			SAFE_REMOVE_BLIP(biBlip)
			SET_STATS_WATCH_OFF()
			NEXT_STAGE()
		BREAK
		
		CASE SS_SKIPPED
			RC_END_Z_SKIP()
			eState = SS_CLEANUP
		BREAK
	ENDSWITCH
ENDPROC

PROC OPEN_GATE(GARAGE_DOOR &Door)
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(Door.id)
		IF Door.eDoorState != DS_OPEN
			Door.eDoorState = DS_OPENING
			DOOR_SYSTEM_SET_OPEN_RATIO(Door.id, Door.fOpen, TRUE, TRUE)
			Door.fOpen = Door.fOpen +@ 0.2
//			#IF IS_DEBUG_BUILD
//				IF bLeft
//					SK_PRINT_FLOAT("Opening Left door  Door.fOpen = ", Door.fOpen)
//				ELSE
//					SK_PRINT_FLOAT("Opening Right door Door.fOpen = ", Door.fOpen)
//				ENDIF
//			#ENDIF
			IF Door.fOpen >= 1.0
				Door.fOpen = 1.0
				Door.eDoorState = DS_OPEN
			ENDIF
		ENDIF	
	ENDIF
ENDPROC

FUNC BOOL IS_GARAGE_DOOR_BLOCKED(GARAGE_DOOR Door) //, BOOL bLeft)
	IF IS_VEHICLE_OK(viStealCar)
		IF IS_AREA_OCCUPIED(Door.vBlocking[0], Door.vBlocking[1], FALSE, TRUE, TRUE, FALSE, FALSE)
//			#IF IS_DEBUG_BUILD
//				IF bLeft
//					IF IS_AREA_OCCUPIED(Door.vBlocking[0], Door.vBlocking[1],  FALSE, FALSE, TRUE, FALSE, FALSE)
//						SK_PRINT_FLOAT("Blocking Left door  PED Door.fOpen = ", Door.fOpen)
//						DRAW_DEBUG_SPHERE(Door.vBlocking[0], 2)
//					ELIF IS_AREA_OCCUPIED(Door.vBlocking[0], Door.vBlocking[1], FALSE, TRUE, FALSE, FALSE, FALSE)
//						SK_PRINT_FLOAT("Blocking Left door VEHICLE  Door.fOpen = ", Door.fOpen)
//						DRAW_DEBUG_SPHERE(Door.vBlocking[1], 2, 0, 255, 0)
//					ENDIF
//				ELSE
//					IF IS_AREA_OCCUPIED(Door.vBlocking[0], Door.vBlocking[1], FALSE, FALSE, TRUE, FALSE, FALSE)
//						SK_PRINT_FLOAT("Blocking Right door PED Door.fOpen = ", Door.fOpen)
//						DRAW_DEBUG_SPHERE(Door.vBlocking[0], 2, 255, 0, 0)
//					ELIF IS_AREA_OCCUPIED(Door.vBlocking[0], Door.vBlocking[1], FALSE, TRUE, FALSE, FALSE, FALSE)
//						SK_PRINT_FLOAT("Blocking Right door VEHICLE  Door.fOpen = ", Door.fOpen)
//						DRAW_DEBUG_SPHERE(Door.vBlocking[1], 2, 255, 255)
//					ENDIF
//
//				ENDIF
//			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CLOSE_GATE(GARAGE_DOOR &Door) //, BOOL bLeft)
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(Door.id)
		IF Door.eDoorState != DS_CLOSED
			IF NOT IS_GARAGE_DOOR_BLOCKED(Door)
				Door.eDoorState = DS_CLOSING
				DOOR_SYSTEM_SET_OPEN_RATIO(Door.id, Door.fOpen, TRUE, TRUE)
				Door.fOpen = Door.fOpen -@ 0.2

//				#IF IS_DEBUG_BUILD
//					IF bLeft
//						SK_PRINT_FLOAT("Closing Left door  Door.fOpen = ", Door.fOpen)
//					ELSE
//						SK_PRINT_FLOAT("Closing Right door Door.fOpen = ", Door.fOpen)
//					ENDIF
//				#ENDIF

				IF Door.fOpen <= 0
				OR (eMissionState = MS_LEAVE_AREA 
				AND NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vGarage, 100))
					#IF IS_DEBUG_BUILD
						IF (eMissionState = MS_LEAVE_AREA 
						AND NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vGarage, 100))
							SK_PRINT_FLOAT("PLAYER out of range ", Door.fOpen)
						ENDIF
					#ENDIF
					DOOR_SYSTEM_SET_DOOR_STATE(Door.id, DOORSTATE_LOCKED, TRUE, TRUE)
					Door.fOpen = 0
					DOOR_SYSTEM_SET_OPEN_RATIO(Door.id, Door.fOpen, TRUE, TRUE)
					Door.eDoorState = DS_CLOSED
				ENDIF
			ELSE
				IF (eMissionState = MS_LEAVE_AREA 
				AND NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vGarage, 100))
					#IF IS_DEBUG_BUILD
						IF (eMissionState = MS_LEAVE_AREA 
						AND NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vGarage, 100))
							SK_PRINT_FLOAT("PLAYER out of range ", Door.fOpen)
						ENDIF
					#ENDIF
					CLEAR_AREA(<<-442.9902, -2168.6230, 9.3184>>, 5, FALSE)
					DOOR_SYSTEM_SET_DOOR_STATE(Door.id, DOORSTATE_LOCKED, TRUE, TRUE)
					Door.fOpen = 0
					DOOR_SYSTEM_SET_OPEN_RATIO(Door.id, Door.fOpen, TRUE, TRUE)
					Door.eDoorState = DS_CLOSED
				ELSE
					OPEN_GATE(Door)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


FUNC BOOL IS_IN_CAR_AREA_WITH_CAR(GARAGE_DOOR Door)
	IF IS_VEHICLE_OK(viStealCar)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viStealCar)
		OR IS_ENTITY_IN_ANGLED_AREA(viStealCar, vGaragePosA, vGaragePosB, fGarage)
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), Door.vCarControlAreaPosA, Door.vCarControlAreaPosB, Door.fDoorWidth)
//				#IF IS_DEBUG_BUILD  SK_PRINT("Player is inside the CAR area")  #ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_OUT_FOOT_AREA_ON_FOOT(GARAGE_DOOR Door)
	IF IS_VEHICLE_OK(viStealCar)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viStealCar)
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), Door.vFootControlAreaPosA, Door.vFootControlAreaPosB, Door.fDoorWidth)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
//	#IF IS_DEBUG_BUILD  SK_PRINT("Player is outside the foot area")  #ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_GARAGE()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vGaragePosA, vGaragePosB, fGarage)
//		#IF IS_DEBUG_BUILD  SK_PRINT("Player is INSIDE the GARAGE")  #ENDIF
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC GARAGE_DOOR_STATE(GARAGE_DOOR &Door, BOOL bLeft = FALSE)
	IF eCopMonitor = CM_MONITER
		IF eMissionState = MS_LEAVE_AREA
			IF IS_VEHICLE_OK(viStealCar)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viStealCar)
					IF IS_IN_CAR_AREA_WITH_CAR(Door)
					OR IS_PLAYER_IN_GARAGE()
						IF bLeft
							IF mRightDoor.eDoorState != DS_OPENING
								OPEN_GATE(Door)
							ENDIF
						ELSE
							IF mLeftDoor.eDoorState != DS_OPENING
								OPEN_GATE(Door)
							ENDIF
						ENDIF
					ELSE
						CLOSE_GATE(Door)
					ENDIF
				ELSE
					IF IS_OUT_FOOT_AREA_ON_FOOT(Door)
					AND NOT IS_PLAYER_IN_GARAGE()
						CLOSE_GATE(Door)
					ELSE IS_PLAYER_IN_GARAGE()
						IF bLeft
							IF mRightDoor.eDoorState != DS_OPENING
								OPEN_GATE(Door)
							ENDIF
						ELSE
							IF mLeftDoor.eDoorState != DS_OPENING
								OPEN_GATE(Door)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_IN_CAR_AREA_WITH_CAR(Door)
			OR IS_PLAYER_IN_GARAGE()
				IF bLeft
					IF mRightDoor.eDoorState != DS_OPENING
						OPEN_GATE(Door)
					ENDIF
				ELSE
					IF mLeftDoor.eDoorState != DS_OPENING
						OPEN_GATE(Door)
					ENDIF
				ENDIF
				ELSE
				CLOSE_GATE(Door)
			ENDIF
		ENDIF
	ELSE
		IF IS_PLAYER_IN_GARAGE()
			IF bLeft
				IF mRightDoor.eDoorState != DS_OPENING
					OPEN_GATE(Door)
				ENDIF
			ELSE
				IF mLeftDoor.eDoorState != DS_OPENING
					OPEN_GATE(Door)
				ENDIF
			ENDIF
		ELSE
			CLOSE_GATE(Door)
		ENDIF
	ENDIF
ENDPROC

PROC MONITOR_GARAGE_DOORS()
	IF IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vGarage, 50)
		IF NOT bTurnedOnDoorSound
			SET_SCRIPT_UPDATE_DOOR_AUDIO(mLeftDoor.id, TRUE)
			SET_SCRIPT_UPDATE_DOOR_AUDIO(mRightDoor.id, TRUE)
			START_AUDIO_SCENE("BIG_SCORE_PREP_C_GARAGE_DOOR")
			bTurnedOnDoorSound = TRUE
		ENDIF
	ELIF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vGarage, 70)
		IF bTurnedOnDoorSound
			SET_SCRIPT_UPDATE_DOOR_AUDIO(mLeftDoor.id, FALSE)
			SET_SCRIPT_UPDATE_DOOR_AUDIO(mRightDoor.id, FALSE)
			STOP_AUDIO_SCENE("BIG_SCORE_PREP_C_GARAGE_DOOR")
			bTurnedOnDoorSound = FALSE
		ENDIF
	ENDIF
	GARAGE_DOOR_STATE(mLeftDoor, TRUE)
	GARAGE_DOOR_STATE(mRightDoor)
ENDPROC


PROC TAKE_CAR_TO_GARAGE()
	MONITOR_IN_STEAL_CAR(viStealCar, viAnyValidTruck, eStealCarState, biVehicleBlip, biBlip, vGarage, NULL, TRUE)
	MONITOR_GARAGE_DOORS()
	MONITOR_CREATE_PAST_GAUNTLETS()
	SWITCH eState
		CASE SS_INIT
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				#IF IS_DEBUG_BUILD  SK_PRINT("INIT TAKE_VAN_BACK")  #ENDIF
				ADD_BLIP_LOCATION(biBlip, vGarage)
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0 
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND NOT IS_PLAYER_BROWSING_IN_ANY_MOD_SHOP()
					PRINT_OBJ("PRF_GARAGE", bObjectiveShown)
				ENDIF
				bForceStop = FALSE
				SERVICES_TOGGLE(TRUE)
				SET_SCENARIO_TYPE_ENABLED("WORLD_HUMAN_LEANING", FALSE)

				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(mLeftDoor.id)
					CPRINTLN(DEBUG_MISSION, "MISSION, Adding door Left with hash == ", mLeftDoor.id)
					ADD_DOOR_TO_SYSTEM(mLeftDoor.id, PROP_COM_GAR_DOOR_01, <<-440.0606, -2171.8267, 11.3672>>)
					DOOR_SYSTEM_SET_OPEN_RATIO(mLeftDoor.id, 0.0)
					DOOR_SYSTEM_SET_DOOR_STATE(mLeftDoor.id, DOORSTATE_LOCKED, TRUE, TRUE)
				ENDIF

				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(mRightDoor.id)
					CPRINTLN(DEBUG_MISSION, "MISSION, Adding door RIGHT with hash == ", mRightDoor.id)
					ADD_DOOR_TO_SYSTEM(mRightDoor.id, PROP_COM_GAR_DOOR_01, <<-445.3054, -2171.8267, 11.3672>>)
					DOOR_SYSTEM_SET_OPEN_RATIO(mRightDoor.id, 0.0)
					DOOR_SYSTEM_SET_DOOR_STATE(mRightDoor.id, DOORSTATE_LOCKED, TRUE, TRUE)
				ENDIF
				
				REPLAY_RECORD_BACK_FOR_TIME(2.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
				
				DISABLE_TAXI_HAILING()
				eState = SS_ACTIVE
			ENDIF
		BREAK

		CASE SS_ACTIVE
			MONITER_PLAYER_WANTED(biBlip, vGarage, eCopMonitor, eStealCarState, bObjectiveShown, bExpireReturnVehWanted)
			MONITOR_BATTLE_BUDDIES()
			
			IF NOT DOES_VEHICLE_HAVE_GOLD_PREP_SETUP(viStealCar)
			AND NOT IS_PLAYER_BROWSING_IN_ANY_MOD_SHOP()
				SAFE_REMOVE_BLIP(biBlip)
				SAFE_REMOVE_BLIP(biVehicleBlip)
				bObjectiveShown = FALSE
				eState = SS_INIT
				eMissionState = MS_RETURN_WITH_THING
				#IF IS_DEBUG_BUILD  SK_PRINT("Resetting state to return with thing")  #ENDIF
				bModsBought = FALSE
				EXIT
			ENDIF
			
			
			IF RETURN_STOLEN_VEHICLE(viStealCar, vGarage, biBlip, eCopMonitor, eStealCarState, bObjectiveShown, bExpireReturnVeh)
			AND IS_PLAYER_IN_GARAGE()
				eState = SS_CLEANUP
			ENDIF
		BREAK

		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP TAKE_VAN_BACK")  #ENDIF
			RELEASE_BATTLE_BUDDIES()
			SAFE_REMOVE_BLIP(biBlip)
			SAFE_REMOVE_BLIP(biVehicleBlip)
			IF STOP_MY_VEHICLE()
				IF IS_VEHICLE_OK(viStealCar)
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					NEXT_STAGE()
				ENDIF				
			ENDIF
		BREAK

		CASE SS_SKIPPED
			IF IS_VEHICLE_OK(viStealCar)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viStealCar)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), viStealCar)
				ELSE
					SAFE_TELEPORT_ENTITY(viStealCar, vGarage, 173.6840)
					RC_END_Z_SKIP()
					eState = SS_ACTIVE
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC


PROC LEAVE_AREA()
	MONITOR_IN_STEAL_CAR(viStealCar, viAnyValidTruck, eStealCarState, biVehicleBlip, biBlip, vGarage)
	MONITOR_GARAGE_DOORS()
	SWITCH eState
		CASE SS_INIT
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				#IF IS_DEBUG_BUILD  SK_PRINT("INIT LEAVE_AREA")  #ENDIF
				bExpireReturnVeh = FALSE
				bForceStop = FALSE
				eState = SS_ACTIVE
				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(mLeftDoor.id)
					ADD_DOOR_TO_SYSTEM(mLeftDoor.id, PROP_COM_GAR_DOOR_01, <<-440.0606, -2171.8267, 11.3672>>)
					DOOR_SYSTEM_SET_DOOR_STATE(mLeftDoor.id, DOORSTATE_LOCKED, TRUE, TRUE)
				ENDIF

				IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(mRightDoor.id)
					ADD_DOOR_TO_SYSTEM(mRightDoor.id, PROP_COM_GAR_DOOR_01, <<-445.3054, -2171.8267, 11.3672>>)
					DOOR_SYSTEM_SET_DOOR_STATE(mRightDoor.id, DOORSTATE_LOCKED, TRUE, TRUE)
				ENDIF
				TRIGGER_MUSIC_EVENT(GET_MUSIC_EVENT_NAME(FALSE))
				DISABLE_TAXI_HAILING()
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
			ENDIF
		BREAK

		CASE SS_ACTIVE
			INT iCallTimerDelay
			BOOL bCallTimer
			iCallTimerDelay = -1
			bCallTimer = FALSE
			MONITER_PLAYER_WANTED(biBlip, vGarage, eCopMonitor, eStealCarState, bObjectiveShown, bExpireReturnVehWanted)
			
			IF LEAVE_VEHICLE_STEAL(viStealCar, vGarage, biBlip, eStealCarState, eCopMonitor, bForceStop, bExpireReturnVeh, iCallTimerDelay, bCallTimer, bObjectiveShown, 7)
				SET_VEHICLE_HAS_BEEN_DRIVEN_FLAG(viStealCar, FALSE) 
                SET_VEHICLE_EXTENDED_REMOVAL_RANGE(viStealCar, 200)
				IF mLeftDoor.eDoorState = DS_CLOSED 
				AND mRightDoor.eDoorState = DS_CLOSED 
				AND NOT IS_PLAYER_IN_GARAGE()
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					DISABLE_TAXI_HAILING(FALSE)
					SET_STATS_WATCH_OFF()
					Script_Passed()
				ENDIF
			ENDIF

		BREAK

		CASE SS_SKIPPED
			IF IS_VEHICLE_OK(viStealCar)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), viStealCar)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), viStealCar)
				ELSE
					SAFE_TELEPORT_ENTITY(viStealCar, vGarage, 126.0705)
					RC_END_Z_SKIP()
					eState = SS_ACTIVE
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC


///DEBUG KEYS
#IF IS_DEBUG_BUILD

	/// PURPOSE: Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()

		// Check for Pass
			IF eState = SS_ACTIVE
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
					Script_Passed()
				ENDIF

				// Check for Fail
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
					MISSION_FAILED(FR_NONE)
				ENDIF
					
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)) 
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
						
					RC_START_Z_SKIP()
					eState = SS_SKIPPED
				ENDIF	
				
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)) 
				AND eMissionState <> MS_SET_UP 
					//Work out which stage we want to reach based on the current stage
					iMissionState = ENUM_TO_INT(eMissionState)
					
					IF iMissionState > 0	
						MISSION_STATE e_stage = INT_TO_ENUM(MISSION_STATE, iMissionState - 1)
						JUMP_TO_STAGE(e_stage)
					ENDIF
				ENDIF
			
			    IF LAUNCH_MISSION_STAGE_MENU(s_skip_menu, i_debug_jump_stage)
					#IF IS_DEBUG_BUILD SK_PRINT_INT("Z DEBUG Initial pick = ", i_debug_jump_stage) #ENDIF
					i_debug_jump_stage++
					SK_PRINT_INT("Z DEBUG ACTUAL STATE = ", i_debug_jump_stage) 

			        MISSION_STATE e_stage = INT_TO_ENUM(MISSION_STATE, i_debug_jump_stage)
			        JUMP_TO_STAGE(e_stage)
			    ENDIF
			ENDIF		
	ENDPROC
#ENDIF

PROC MONITER_STEAL_CAR()	
	IF NOT IS_VEHICLE_OK(viStealCar)
		IF NOT IS_VEHICLE_OK(viAnyValidTruck) 
		AND NOT IS_VEHICLE_OK(mMissionCar.id)
			#IF IS_DEBUG_BUILD  SK_PRINT("FAIL 1")  #ENDIF
			MISSION_FAILED(FR_WRECKED_STEAL_CAR)
			EXIT
		ENDIF
		
		IF IS_VEHICLE_OK(mMissionCar.id)
		AND NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mMissionCar.id, 500)
			#IF IS_DEBUG_BUILD  SK_PRINT("FAIL 2")  #ENDIF
			MISSION_FAILED(FR_ABAN_CAR)
		ENDIF
	ELSE
		INT iCountOutOfRange = 0 
		IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), viStealCar, 500)
			iCountOutOfRange++
		ENDIF
		
		IF IS_VEHICLE_OK(viAnyValidTruck)
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), viAnyValidTruck, 500)
				iCountOutOfRange++
			ENDIF
		ELSE
			iCountOutOfRange++
		ENDIF
		
		IF IS_VEHICLE_OK(mMissionCar.id)
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mMissionCar.id, 500)
				iCountOutOfRange++
			ENDIF
		ELSE
			iCountOutOfRange++
		ENDIF

		IF iCountOutOfRange >= 3
			#IF IS_DEBUG_BUILD SK_PRINT_INT("All 3 out of range ", iCountOutOfRange) #ENDIF
			MISSION_FAILED(FR_ABAN_CAR)
			EXIT
		ENDIF


		//stuck
		iCountOutOfRange = 0
		
		
		IF IS_CAR_STUCK(viStealCar)
			iCountOutOfRange++
		ENDIF

		IF IS_VEHICLE_OK(viAnyValidTruck)
			IF IS_CAR_STUCK(viAnyValidTruck)
				#IF IS_DEBUG_BUILD SK_PRINT_INT("vehicle stuck  viAnyValidTruck", iCountOutOfRange) #ENDIF
				iCountOutOfRange++
			ENDIF
		ELSE
			iCountOutOfRange++
		ENDIF

		IF IS_VEHICLE_OK(mMissionCar.id)
			IF IS_CAR_STUCK(mMissionCar.id)
				#IF IS_DEBUG_BUILD SK_PRINT_INT("vehicle stuck  mMissionCar" , iCountOutOfRange) #ENDIF
				iCountOutOfRange++
			ENDIF
		ELSE
			iCountOutOfRange++
		ENDIF
		
		IF iCountOutOfRange >= 3
			#IF IS_DEBUG_BUILD SK_PRINT_INT("vehicle stuck All ", iCountOutOfRange) #ENDIF
			MISSION_FAILED(FR_STUCK_CAR)
			EXIT
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_REPAIR_COST_TOO_HIGH()
	IF IS_VEHICLE_OK(viStealCar)
		enumBankAccountName	ePlayerAccount
		SWITCH GET_CURRENT_PLAYER_PED_ENUM()
			CASE CHAR_MICHAEL
				ePlayerAccount = BANK_ACCOUNT_MICHAEL
			BREAK
			
			CASE CHAR_FRANKLIN
				ePlayerAccount = BANK_ACCOUNT_FRANKLIN
			BREAK
			
			CASE CHAR_TREVOR
				ePlayerAccount = BANK_ACCOUNT_TREVOR
			BREAK
		ENDSWITCH
		
		IF (GET_ACCOUNT_BALANCE(ePlayerAccount) - GET_CARMOD_REPAIR_COST(viStealCar)) < 11000
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Holds functions that moniter for the player failing
PROC CHECK_FOR_FAIL()
	IF NOT bJumpSkip
	AND eMissionState <> MS_FAILED
		IF eMissionState > MS_SET_UP
			MONITER_STEAL_CAR()
		
			IF eMissionState <= MS_BUY_MODS
			AND GET_CURRENT_CARMOD_SHOP_MENU() != CMM_GOLD_PREP
				IF NOT DOES_PLAYER_HAVE_THE_CASH(bModsBought , 11000)
					CPRINTLN(DEBUG_MISSION, "No cash")
					MISSION_FAILED(FR_NO_CASH)
				ENDIF

				IF NOT bModsBought
				AND IS_REPAIR_COST_TOO_HIGH()
					CPRINTLN(DEBUG_MISSION, "IS_REPAIR_COST_TOO_HIGH")
					MISSION_FAILED(FR_NO_CASH_REPAIRS)
				ENDIF

				IF IS_PLAYER_KICKING_OFF_IN_SHOP(CARMOD_SHOP_01_AP)
					MISSION_FAILED(FR_WANTED)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC DO_REPLAY_CHECK_EVENT()
	SWITCH MISSION_FLOW_GET_RUNNING_MISSION()
		CASE SP_HEIST_FINALE_PREP_C1
			REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TheBigScorePrepC1")
		BREAK
		CASE SP_HEIST_FINALE_PREP_C2
			REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TheBigScorePrepC2")
		BREAK
		CASE SP_HEIST_FINALE_PREP_C3
			REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TheBigScorePrepC3")
		BREAK
	ENDSWITCH

ENDPROC

SCRIPT

	SET_MISSION_FLAG(TRUE)
	
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINTSTRING("FORCE CLEAN UP") PRINTNL()
		sFailReason = NULL
		Mission_Flow_Mission_Force_Cleanup()
        Script_Cleanup()
	ENDIF

	#IF IS_DEBUG_BUILD
		SETUP_FOR_RAGE_WIDGETS()
	#ENDIF
	POPULATE_STUFF()
	IF IS_REPLAY_IN_PROGRESS()
		PICK_REPLY_POS()
	ENDIF

	SET_VEHICLE_MODEL_IS_SUPPRESSED(GAUNTLET, TRUE)

	REGISTER_SCRIPT_WITH_AUDIO()
	IF IS_REPEAT_PLAY_ACTIVE()
		CREDIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_CASH_DEPOSIT, 25000, FALSE, FALSE)
	ENDIF
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		DO_REPLAY_CHECK_EVENT()
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			MONITOR_PLAYER_BUYING_MODS()
			CHECK_FOR_FAIL()
			MONITER_STATS()
			SWITCH eMissionState
			
				CASE MS_SET_UP
					INITMISSION()
				BREAK

				CASE MS_RETURN_WITH_THING
					RETURN_THING()
				BREAK
				
				CASE MS_BUY_MODS
					BUY_MODS()
				BREAK
				
				CASE MS_TAKE_CAR_TO_GARAGE
					TAKE_CAR_TO_GARAGE()
				BREAK
				
				CASE MS_LEAVE_AREA
					LEAVE_AREA()
				BREAK
				
				CASE MS_FAILED
					FAILED_WAIT_FOR_FADE()
				BREAK
			ENDSWITCH

			IF eMissionState <> MS_FAILED
				IF eMissionState >= MS_SET_UP
				AND NOT bJumpSkip
				ENDIF

				#IF IS_DEBUG_BUILD
					// Check debug completion/failure
					DEBUG_Check_Debug_Keys()
					UPDATE_RAG_WIDGETS()
				#ENDIF
			ENDIF
		ENDIF

		WAIT(0)

	ENDWHILE
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
