
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "replay_public.sch"
USING "locates_public.sch"
USING "script_Ped.sch"
USING "commands_recording.sch"



// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Setup Big Score 2b
//		AUTHOR			:	Ben Barclay
//		DESCRIPTION		:	Player has to steal a driller for the finale heist.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************


/////══════════════════════════════════════╡ DEBUG VARIABLES  ╞════════════════════════════════════════
#IF IS_DEBUG_BUILD

//For z menu
USING "select_mission_stage.sch"

CONST_INT MAX_SKIP_MENU_LENGTH 4                       // number of stages in mission + 2 (for menu )
INT iReturnStage                                       // mission stage to jump to

MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]      // struct containing the debug menu 

#ENDIF

// ___________________________________ ENUMS ______________________________________________
ENUM MISSION_FLOW
	STAGE_OPENING_CUTSCENE,
	STAGE_INIT_MISSION,
	STAGE_GET_TO_DRILLER,
	STAGE_STEAL_DRILLER,
	STAGE_TAKE_DRILLER_HOME,
	STAGE_CLOSING_CUTSCENE,
	STAGE_MISSION_FAILED
ENDENUM
MISSION_FLOW missionStage = STAGE_OPENING_CUTSCENE

ENUM WORKERS_REACTION_TO_PLAYER
	IGNORES_PLAYER,
	CURIOUS_OF_PLAYER,
	DISLIKES_PLAYER,
	HATES_PLAYER,
	SCARED_OF_PLAYER,
	CHASE_PLAYER_IN_VEHICLE
ENDENUM
WORKERS_REACTION_TO_PLAYER eWorkerReaction = IGNORES_PLAYER

ENUM FAIL_REASON
	FAIL_DEFAULT,
	FAIL_TRAILER_DESTROYED,
	FAIL_TRUCK_STUCK,
	FAIL_TRUCK_DESTROYED,
	FAIL_CUTTER_DESTROYED,
	FAIL_CUTTER_ABANDONED,
	FAIL_ENDGUARD_KILLED
ENDENUM
FAIL_REASON eFailReason = FAIL_DEFAULT

ENUM GUARD_REACTION_TO_PLAYER
	NOT_BOTHERED_BY_PLAYER,
	WARN_PLAYER,
	HATE_PLAYER
ENDENUM
GUARD_REACTION_TO_PLAYER eYardGuardMood = NOT_BOTHERED_BY_PLAYER

// ===========================================================================================================
//		VARIABLES
// ===========================================================================================================

CONST_INT SafeDistance 20 

//Vehicles
VEHICLE_INDEX DrillerTrailer
//VEHICLE_INDEX Driller
VEHICLE_INDEX DrillerTruck
VEHICLE_INDEX playerStartCar

//Peds
PED_INDEX EnemyPed[7]
PED_INDEX YardGuard[2]
PED_INDEX hBuddyMichael, hBuddyFranklin, hBuddyTrevor
PED_INDEX EndGuard
PED_INDEX NearestCop

//Cameras

//Scenario blocking areas
SCENARIO_BLOCKING_INDEX endMissionWorkersArea
SCENARIO_BLOCKING_INDEX endMissionWorkersArea2
SCENARIO_BLOCKING_INDEX YardWorkersArea1
SCENARIO_BLOCKING_INDEX YardWorkersArea2
SCENARIO_BLOCKING_INDEX YardWorkersArea3
SCENARIO_BLOCKING_INDEX YardWorkersArea4

//Sequences
SEQUENCE_INDEX seq

//Blips
BLIP_INDEX DrillerBlip
BLIP_INDEX HomeBlip
BLIP_INDEX YardBlip
//BLIP_INDEX EnemyPedBlip[7]
BLIP_INDEX TrailerBlip


//Integers
INT iControlFlag 	= 0
INT icount, icount2, icount3, icount5
INT iCaseStage 		= 0
INT iWeaponTimer
INT iDislikeTimer[7]
//INT iApproachTimer[7]
INT iRandomScreamTimer
INT ichat12Timer
INT iWantedLevelTimer
INT iweaponSpotCount
INT iGuardsCombatTimer
INT iChat13Timer
INT iChat14Timer
INT iFightCHat
INT iFightCHat2
INT iChat15Timer
INT iChat16Timer
INT iChat17Timer
INT iChat18Timer
INT imeleeTimer 
INT iCanSeeTimer
INT iMoveTimer				
INT iworker6Timer
INT icountChat24
INT ichat24timer
INT iPedCanSeePlayerTimer[2]
INT iCombatSeeTimer[2]
INT iTurnPedTaskTimer[2]
INT iScenarioExistsTimer[2]
INT iChat27Timer
INT iattachTextTimer
INT iLoseCopsTimer
INT iStartPhoneTimer
INT iSee6ChatTimer
INT iEndGuardTaskTimer
						
//Barriers
INT iLeftBarrier
INT iRightBarrier
											
//Vector
VECTOR vHomeCoords = <<27.7189, -608.7927, 30.6293>>
VECTOR vplayerCoords
VECTOR vbuddyCoords
VECTOR vYardGuardCoords[2]
VECTOR vMiddleOfYard = <<903.1, -1548.8, 29.8>>

//Floats
FLOAT fOpenRatio = 0

//Strings
STRING sFailReason

//Structs
structPedsForConversation MyLocalPedStruct

//Groups
REL_GROUP_HASH workerMainGroup
REL_GROUP_HASH GuardMainGroup

//Flags
BOOL MissionStageBeingSkippedTo
BOOL doneGodText
BOOL FleeTaskGiven[7]
BOOL doneLoseCopsText
BOOL doneGetBackInText
//BOOL blipsHaveBeenAdded
BOOL doneLoseWorkersText
BOOL WantedLevelFlagSet
BOOL buddy1NotAvailable 		
BOOL buddy2NotAvailable 	
BOOL buddyReleasedAndOutTruck
//BOOL PedDriveByTaskGiven 
BOOL weaponTimerStarted 
BOOL dislikeTimerStarted[7]
BOOL pedIsFarAway[7]
BOOL AllWorkersAreFarAway	
BOOL WorkerLookTaskGiven[7]
BOOL WorkerApproachTaskGiven[7]
BOOL blockedEventsForHate[7]
BOOL attackPlayerTaskGiven[7]
BOOL SmartFleeTaskGiven[7]
BOOL handsUpTaskGiven[7]
//BOOL blockedEvents[7]
BOOL randomScreamTimerSet
BOOL startedChat12
BOOL doneChat10		
BOOL PlayerIsStealingTruck
BOOL DoneChat8
BOOL DoneCuriousChat
//BOOL DoneChat7	
BOOL wantedLevelSet
BOOL wantedLevelTimerStarted
BOOL DoneGod6Text
BOOL missionCanFail
BOOL playerWarnedChat
BOOL GuardsShouldCombat
BOOL YardGuard0Closest
BOOL YardGuard1Closest
BOOL Chat16Done
BOOL Chat15Done
BOOL doneChat13
BOOL yardguardIsAlive[2]
BOOL playerHasBeenWarnedBefore[2]
BOOL doneChat17
BOOL doneChat18
//BOOL Player1StarGiven
BOOL Player2StarGiven
BOOL meleeTimerSet
BOOL GuardsCombatTimerSet
BOOL taskForPed4Given
BOOL doneChat19
BOOL doneLeaveYardText
BOOL doneChat20
BOOL doneChat21
BOOL doneChat22
BOOL lookAtTaskGiven
BOOL gatesLocked
BOOL moveTimerSet
BOOL worker6TaskGiven
BOOL worker6TimerStarted
BOOL worker6Task2Given
BOOL WorkerSpottedPlayerFirst
BOOL playerSpottedOnRoof
BOOL doneChat23
BOOL doneChat24
BOOL GuardsAlerted
BOOL doneChat25
BOOL TaskLookAtGiven[2]
BOOL TaskGoToEntity[2]
BOOL TaskNavmeshToPed[2]
BOOL combatTaskGiven[2]
BOOL combatSeeTimerSet[2]
BOOL followNavForCombat[2]
BOOL turnPedTask[2]
BOOL GuardsFightingTalkStarted
//BOOL doneChat26
BOOL doneChat27
BOOL endguardTaskGiven
BOOL BarriersAddedToSystem
BOOL copsHaveArrived
BOOL TaskToGetBackToGuardCoord[2]
BOOL StandGuardTaskGiven[2]
BOOL doneGod1
BOOL ped5CombatTaskGiven
BOOL ped5PhonePolice
BOOL ped0TaskGiven
BOOL musicChanged
BOOL buddyMichaelDrivingTruck
BOOL buddyTrevorDrivingTruck
BOOL buddyFranklinDrivingTruck
BOOL BuddyMichaelReadyToBeReleased
BOOL BuddyFranklinReadyToBeReleased
BOOL BuddyTrevorReadyToBeReleased
BOOL MichaelDriveTaskGiven
BOOL TrevorDriveTaskGiven
BOOL FranklinDriveTaskGiven
BOOL MichaelPoliceDriveTaskGiven
BOOL TrevorPoliceDriveTaskGiven
BOOL FranklinPoliceDriveTaskGiven
BOOL deadBodyFound
BOOL PlayerSpotted
BOOL GuardFacingPlayer[2]
BOOL chatPed0

// ===========================================================================================================
//		Termination
// ===========================================================================================================

//PURPOSE: Checks if players buddy is in the driver seat of a vehicle the player is in.
FUNC BOOL IS_BUDDY_IN_DRIVER_SEAT(PED_INDEX hPed)
		
	//Check if player is in any vehicle and if his buddy is in that vehicle's driving seat.			
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(hPed)
			IF IS_PED_IN_VEHICLE(hPed, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			AND GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_DRIVER) = hPed
				
				//Check if player and buddy are in the helicopter
				IF DOES_ENTITY_EXIST(DrillerTruck)
					IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
						IF IS_PED_IN_VEHICLE(hPed, DrillerTruck)
						AND GET_PED_IN_VEHICLE_SEAT(DrillerTruck, VS_DRIVER) = hPed	
						AND NOT IS_ENTITY_AT_COORD(DrillerTruck, vHomeCoords, <<3,3,3>>)
							
							IF hPed = hBuddyMichael
								buddyMichaelDrivingTruck = TRUE
							ENDIF
							IF hPed = hBuddyTrevor
								BuddyTrevorDrivingTruck = TRUE
							ENDIF
							IF hPed = hBuddyFranklin
								BuddyFranklinDrivingTruck = TRUE
							ENDIF
							
							RETURN TRUE
							
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	IF hPed = hBuddyMichael
		buddyMichaelDrivingTruck = FALSE
	ENDIF
	IF hPed = hBuddyTrevor
		BuddyTrevorDrivingTruck = FALSE
	ENDIF
	IF hPed = hBuddyFranklin
		BuddyFranklinDrivingTruck = FALSE
	ENDIF	

	RETURN FALSE
ENDFUNC

//Handles the battle buddies 
PROC MONITOR_BATTLE_BUDDIES()

	hBuddyMichael = GET_BATTLEBUDDY_PED(CHAR_MICHAEL)
	hBuddyFranklin = GET_BATTLEBUDDY_PED(CHAR_FRANKLIN)
	hBuddyTrevor = GET_BATTLEBUDDY_PED(CHAR_TREVOR)	
	
	//Handle Battle Buddy Michael if he's available
	IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
		IF NOT IS_PED_INJURED(hBuddyMichael)
			IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddyMichael)	
				//Dont take control of battle buddy until switch cam has finished.
				IF NOT IS_SELECTOR_CAM_ACTIVE()

					// Is buddy driving the players vehicle
					IF IS_BUDDY_IN_DRIVER_SEAT(hBuddyMichael)
						PRINTSTRING("IS_BUDDY_IN_DRIVER_SEAT = TRUE") PRINTNL()
						IF OVERRIDE_BATTLEBUDDY(hBuddyMichael, FALSE)
							PRINTSTRING("OVERRIDE_BATTLEBUDDY has been called") PRINTNL()
							SET_ENTITY_AS_MISSION_ENTITY(hBuddyMichael, TRUE, TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyMichael, TRUE)
							CLEAR_PED_TASKS(hBuddyMichael)
							
							//Flags
							BuddyMichaelReadyToBeReleased = FALSE
							MichaelDriveTaskGiven = FALSE
							MichaelPoliceDriveTaskGiven = FALSE
							
						ENDIF
					ENDIF
					
				ENDIF
			
			ELSE
				
				//GENERAL RULES THAT SHOULD ALWAYS ALLOW BUDDY TO BE RELEASED
				
				//If player is not in the buddy's vehicle
				IF BuddyMichaelReadyToBeReleased = FALSE
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						PRINTSTRING("BuddyMichaelReadyToBeReleased 0 = TRUE") PRINTNL()
						BuddyMichaelReadyToBeReleased = TRUE
					ELSE
						//Check to make sure the player is in the same vehicle as the buddy, if not release the battle buddy.
						IF IS_PED_IN_ANY_VEHICLE(hBuddyMichael)
							IF NOT IS_PED_IN_VEHICLE(hBuddyMichael, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								PRINTSTRING("BuddyMichaelReadyToBeReleased 1 = TRUE") PRINTNL()
								BuddyMichaelReadyToBeReleased = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
				
				//If player switches then release all battle buddies
				IF BuddyMichaelReadyToBeReleased = FALSE
					IF IS_SELECTOR_CAM_ACTIVE()
						PRINTSTRING("BuddyMichaelReadyToBeReleased 2 = TRUE") PRINTNL()
						BuddyMichaelReadyToBeReleased = TRUE
					ENDIF
				ENDIF	
				
				
				//GIVE THE BUDDY TASKS NOW DEPENDING ON WHAT THEY ARE DOING	
				
				//If buddy is drivin the truck
				IF buddyMichaelDrivingTruck = TRUE
					// Does script still need to take control of buddy and drive to dest?
					IF IS_BUDDY_IN_DRIVER_SEAT(hBuddyMichael)
					AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddyMichael)
					AND BuddyMichaelReadyToBeReleased = FALSE				
						//Give buddy task to drive the car to the heli 	
						vbuddyCoords = GET_ENTITY_COORDS(hBuddyMichael)
						REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vbuddyCoords.x, vbuddyCoords.y, vHomeCoords.x, vHomeCoords.y)
						//Check for wanted level
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							IF MichaelDriveTaskGiven = FALSE
								IF ARE_NODES_LOADED_FOR_AREA(vbuddyCoords.x, vbuddyCoords.y, vHomeCoords.x, vHomeCoords.y)
									TASK_VEHICLE_MISSION_COORS_TARGET(hBuddyMichael, GET_VEHICLE_PED_IS_IN(hBuddyMichael), vHomeCoords, MISSION_GOTO, 30.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 3.0, 15.0, FALSE)
									MichaelPoliceDriveTaskGiven = FALSE
									MichaelDriveTaskGiven = TRUE
								ENDIF
							ENDIF
						ELSE
							IF DOES_ENTITY_EXIST(GET_CLOSEST_VEHICLE(vbuddyCoords, 200, POLICE3, VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL))	
								IF MichaelPoliceDriveTaskGiven = FALSE
									TASK_VEHICLE_MISSION(hBuddyMichael, GET_VEHICLE_PED_IS_IN(hBuddyMichael), GET_CLOSEST_VEHICLE(vbuddyCoords, 200, POLICE3, VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL), MISSION_FLEE, 40, DRIVINGMODE_PLOUGHTHROUGH, 300, 1, TRUE)
									MichaelDriveTaskGiven = FALSE
									MichaelPoliceDriveTaskGiven = TRUE
								ENDIF
							ELSE
								IF MichaelDriveTaskGiven = FALSE
									IF ARE_NODES_LOADED_FOR_AREA(vbuddyCoords.x, vbuddyCoords.y, vHomeCoords.x, vHomeCoords.y)
										TASK_VEHICLE_MISSION_COORS_TARGET(hBuddyMichael, GET_VEHICLE_PED_IS_IN(hBuddyMichael), vHomeCoords, MISSION_GOTO, 30.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 3.0, 15.0, FALSE)
										MichaelPoliceDriveTaskGiven = FALSE
										MichaelDriveTaskGiven = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						//Release buddy if he's not driving the truck anymore
						BuddyMichaelReadyToBeReleased = TRUE
						PRINTSTRING("BuddyMichaelReadyToBeReleased 3 = TRUE") PRINTNL()
					ENDIF
				ENDIF
				
				//Release Michael if this flag gets set to TRUE for any of the reasons above.
				IF BuddyMichaelReadyToBeReleased = TRUE
					IF RELEASE_BATTLEBUDDY(hBuddyMichael)
						BuddyMichaelReadyToBeReleased = FALSE
					ENDIF
				ENDIF
				
			ENDIF	
		ENDIF
	ENDIF

	//Handle Battle Buddy Trevor if he's available
	IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
		IF NOT IS_PED_INJURED(hBuddyTrevor)
			IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddyTrevor)	
				//Dont take control of battle buddy until switch cam has finished.
				IF NOT IS_SELECTOR_CAM_ACTIVE()

					// Is buddy driving the players vehicle
					IF IS_BUDDY_IN_DRIVER_SEAT(hBuddyTrevor)
						PRINTSTRING("IS_BUDDY_IN_DRIVER_SEAT = TRUE") PRINTNL()
						IF OVERRIDE_BATTLEBUDDY(hBuddyTrevor, FALSE)
							PRINTSTRING("OVERRIDE_BATTLEBUDDY has been called") PRINTNL()
							SET_ENTITY_AS_MISSION_ENTITY(hBuddyTrevor, TRUE, TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyTrevor, TRUE)
							CLEAR_PED_TASKS(hBuddyTrevor)
							
							//Flags
							BuddyTrevorReadyToBeReleased = FALSE
							TrevorDriveTaskGiven = FALSE
							TrevorPoliceDriveTaskGiven = FALSE
							
						ENDIF
					ENDIF
					
				ENDIF
			
			ELSE
				
				//GENERAL RULES THAT SHOULD ALWAYS ALLOW BUDDY TO BE RELEASED
				
				//If player is not in the buddy's vehicle
				IF BuddyTrevorReadyToBeReleased = FALSE
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						PRINTSTRING("BuddyTrevorReadyToBeReleased 0 = TRUE") PRINTNL()
						BuddyTrevorReadyToBeReleased = TRUE
					ELSE
						//Check to make sure the player is in the same vehicle as the buddy, if not release the battle buddy.
						IF IS_PED_IN_ANY_VEHICLE(hBuddyTrevor)
							IF NOT IS_PED_IN_VEHICLE(hBuddyTrevor, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								PRINTSTRING("BuddyTrevorReadyToBeReleased 1 = TRUE") PRINTNL()
								BuddyTrevorReadyToBeReleased = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
				
				//If player switches then release all battle buddies
				IF BuddyTrevorReadyToBeReleased = FALSE
					IF IS_SELECTOR_CAM_ACTIVE()
						PRINTSTRING("BuddyTrevorReadyToBeReleased 2 = TRUE") PRINTNL()
						BuddyTrevorReadyToBeReleased = TRUE
					ENDIF
				ENDIF	
				
				
				//GIVE THE BUDDY TASKS NOW DEPENDING ON WHAT THEY ARE DOING	
				
				//If buddy is drivin the truck
				IF buddyTrevorDrivingTruck = TRUE
					// Does script still need to take control of buddy and drive to dest?
					IF IS_BUDDY_IN_DRIVER_SEAT(hBuddyTrevor)
					AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddyTrevor)
					AND BuddyTrevorReadyToBeReleased = FALSE				
						//Give buddy task to drive the car to the heli 	
						vbuddyCoords = GET_ENTITY_COORDS(hBuddyTrevor)
						REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vbuddyCoords.x, vbuddyCoords.y, vHomeCoords.x, vHomeCoords.y)
						//Check for wanted level
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							IF TrevorDriveTaskGiven = FALSE
								IF ARE_NODES_LOADED_FOR_AREA(vbuddyCoords.x, vbuddyCoords.y, vHomeCoords.x, vHomeCoords.y)
									TASK_VEHICLE_MISSION_COORS_TARGET(hBuddyTrevor, GET_VEHICLE_PED_IS_IN(hBuddyTrevor), vHomeCoords, MISSION_GOTO, 30.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 3.0, 15.0, FALSE)
									TrevorPoliceDriveTaskGiven = FALSE
									TrevorDriveTaskGiven = TRUE
								ENDIF
							ENDIF
						ELSE
							IF DOES_ENTITY_EXIST(GET_CLOSEST_VEHICLE(vbuddyCoords, 200, POLICE3, VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL))	
								IF TrevorPoliceDriveTaskGiven = FALSE
									TASK_VEHICLE_MISSION(hBuddyTrevor, GET_VEHICLE_PED_IS_IN(hBuddyTrevor), GET_CLOSEST_VEHICLE(vbuddyCoords, 200, POLICE3, VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL), MISSION_FLEE, 40, DRIVINGMODE_PLOUGHTHROUGH, 300, 1, TRUE)
									TrevorDriveTaskGiven = FALSE
									TrevorPoliceDriveTaskGiven = TRUE
								ENDIF
							ELSE
								IF TrevorDriveTaskGiven = FALSE
									IF ARE_NODES_LOADED_FOR_AREA(vbuddyCoords.x, vbuddyCoords.y, vHomeCoords.x, vHomeCoords.y)
										TASK_VEHICLE_MISSION_COORS_TARGET(hBuddyTrevor, GET_VEHICLE_PED_IS_IN(hBuddyTrevor), vHomeCoords, MISSION_GOTO, 30.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 3.0, 15.0, FALSE)
										TrevorPoliceDriveTaskGiven = FALSE
										TrevorDriveTaskGiven = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						//Release buddy if he's not driving the truck anymore
						BuddyTrevorReadyToBeReleased = TRUE
						PRINTSTRING("BuddyTrevorReadyToBeReleased 3 = TRUE") PRINTNL()
					ENDIF
				ENDIF
				
				//Release Michael if this flag gets set to TRUE for any of the reasons above.
				IF BuddyTrevorReadyToBeReleased = TRUE
					IF RELEASE_BATTLEBUDDY(hBuddyTrevor)
						BuddyTrevorReadyToBeReleased = FALSE
					ENDIF
				ENDIF
				
			ENDIF	
		ENDIF
	ENDIF
	
	//Handle Battle Buddy Franklin if he's available
	IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
		IF NOT IS_PED_INJURED(hBuddyFranklin)
			IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddyFranklin)	
				//Dont take control of battle buddy until switch cam has finished.
				IF NOT IS_SELECTOR_CAM_ACTIVE()

					// Is buddy driving the players vehicle
					IF IS_BUDDY_IN_DRIVER_SEAT(hBuddyFranklin)
						PRINTSTRING("IS_BUDDY_IN_DRIVER_SEAT = TRUE") PRINTNL()
						IF OVERRIDE_BATTLEBUDDY(hBuddyFranklin, FALSE)
							PRINTSTRING("OVERRIDE_BATTLEBUDDY has been called") PRINTNL()
							SET_ENTITY_AS_MISSION_ENTITY(hBuddyFranklin, TRUE, TRUE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyFranklin, TRUE)
							CLEAR_PED_TASKS(hBuddyFranklin)
							
							//Flags
							BuddyFranklinReadyToBeReleased = FALSE
							FranklinDriveTaskGiven = FALSE
							FranklinPoliceDriveTaskGiven = FALSE
							
						ENDIF
					ENDIF
					
				ENDIF
			
			ELSE
				
				//GENERAL RULES THAT SHOULD ALWAYS ALLOW BUDDY TO BE RELEASED
				
				//If player is not in the buddy's vehicle
				IF BuddyFranklinReadyToBeReleased = FALSE
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						PRINTSTRING("BuddyFranklinReadyToBeReleased 0 = TRUE") PRINTNL()
						BuddyFranklinReadyToBeReleased = TRUE
					ELSE
						//Check to make sure the player is in the same vehicle as the buddy, if not release the battle buddy.
						IF IS_PED_IN_ANY_VEHICLE(hBuddyFranklin)
							IF NOT IS_PED_IN_VEHICLE(hBuddyFranklin, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								PRINTSTRING("BuddyFranklinReadyToBeReleased 1 = TRUE") PRINTNL()
								BuddyFranklinReadyToBeReleased = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
				
				//If player switches then release all battle buddies
				IF BuddyFranklinReadyToBeReleased = FALSE
					IF IS_SELECTOR_CAM_ACTIVE()
						PRINTSTRING("BuddyFranklinReadyToBeReleased 2 = TRUE") PRINTNL()
						BuddyFranklinReadyToBeReleased = TRUE
					ENDIF
				ENDIF	
				
				
				//GIVE THE BUDDY TASKS NOW DEPENDING ON WHAT THEY ARE DOING	
				
				//If buddy is drivin the truck
				IF buddyFranklinDrivingTruck = TRUE
					// Does script still need to take control of buddy and drive to dest?
					IF IS_BUDDY_IN_DRIVER_SEAT(hBuddyFranklin)
					AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddyFranklin)
					AND BuddyFranklinReadyToBeReleased = FALSE				
						//Give buddy task to drive the car to the heli 	
						vbuddyCoords = GET_ENTITY_COORDS(hBuddyFranklin)
						REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vbuddyCoords.x, vbuddyCoords.y, vHomeCoords.x, vHomeCoords.y)
						//Check for wanted level
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							IF FranklinDriveTaskGiven = FALSE
								IF ARE_NODES_LOADED_FOR_AREA(vbuddyCoords.x, vbuddyCoords.y, vHomeCoords.x, vHomeCoords.y)
									TASK_VEHICLE_MISSION_COORS_TARGET(hBuddyFranklin, GET_VEHICLE_PED_IS_IN(hBuddyFranklin), vHomeCoords, MISSION_GOTO, 30.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 3.0, 15.0, FALSE)
									FranklinPoliceDriveTaskGiven = FALSE
									FranklinDriveTaskGiven = TRUE
								ENDIF
							ENDIF
						ELSE
							IF DOES_ENTITY_EXIST(GET_CLOSEST_VEHICLE(vbuddyCoords, 200, POLICE3, VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL))	
								IF FranklinPoliceDriveTaskGiven = FALSE
									TASK_VEHICLE_MISSION(hBuddyFranklin, GET_VEHICLE_PED_IS_IN(hBuddyFranklin), GET_CLOSEST_VEHICLE(vbuddyCoords, 200, POLICE3, VEHICLE_SEARCH_FLAG_ALLOW_LAW_ENFORCER_VEHICLES_WITH_WANTED_LEVEL), MISSION_FLEE, 40, DRIVINGMODE_PLOUGHTHROUGH, 300, 1, TRUE)
									FranklinDriveTaskGiven = FALSE
									FranklinPoliceDriveTaskGiven = TRUE
								ENDIF
							ELSE
								IF FranklinDriveTaskGiven = FALSE
									IF ARE_NODES_LOADED_FOR_AREA(vbuddyCoords.x, vbuddyCoords.y, vHomeCoords.x, vHomeCoords.y)
										TASK_VEHICLE_MISSION_COORS_TARGET(hBuddyFranklin, GET_VEHICLE_PED_IS_IN(hBuddyFranklin), vHomeCoords, MISSION_GOTO, 30.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 3.0, 15.0, FALSE)
										FranklinPoliceDriveTaskGiven = FALSE
										FranklinDriveTaskGiven = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						//Release buddy if he's not driving the truck anymore
						BuddyFranklinReadyToBeReleased = TRUE
						PRINTSTRING("BuddyFranklinReadyToBeReleased 3 = TRUE") PRINTNL()
					ENDIF
				ENDIF
				
				//Release Michael if this flag gets set to TRUE for any of the reasons above.
				IF BuddyFranklinReadyToBeReleased = TRUE
					IF RELEASE_BATTLEBUDDY(hBuddyFranklin)
						BuddyFranklinReadyToBeReleased = FALSE
					ENDIF
				ENDIF
				
			ENDIF	
		ENDIF
	ENDIF	

ENDPROC

//PURPOSE: Handles all mission specific stats
PROC STATS_CONTROLLER()

	//FHPB_CAR_DAMAGE - Total damage taken in the truck.
	//This will be handled by calling INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(ENTITY_INDEX in) when the truck is created.
	
	//FHPB_MAX_SPEED - Fastest speed reached in the truck
	//This will be handled by calling INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(ENTITY_INDEX in) when the truck is created.
	
	//FHPB_INNOCENTS_KILLED - How many innocent peds are killed, this is handled outwith my script
	
	//FHPB_TRUCK_TIME - Time taken to get to the destination in the truck
	//This is started when player gets into the truck and stopped when player gets to destination in truck.
	
	//Steal the Cutter without being detected
	IF missionStage = STAGE_CLOSING_CUTSCENE
		IF PlayerSpotted = FALSE
			INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FHPB_SNEAK_THIEF)
		ENDIF
	ENDIF
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Cleanup()

	//Flags
	SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), FALSE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(PACKER, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(PHANTOM, FALSE)
	
	//Audio
	CANCEL_MUSIC_EVENT("FHPRB_START")
	CANCEL_MUSIC_EVENT("FHPRB_TRUCK")
	CANCEL_MUSIC_EVENT("FHPRB_COPS")
	CANCEL_MUSIC_EVENT("FHPRB_LOST")
	CANCEL_MUSIC_EVENT("FHPRB_STOP")
	
	REMOVE_SCENARIO_BLOCKING_AREA(endMissionWorkersArea)
	REMOVE_SCENARIO_BLOCKING_AREA(endMissionWorkersArea2)
	REMOVE_SCENARIO_BLOCKING_AREA(YardWorkersArea1)
	REMOVE_SCENARIO_BLOCKING_AREA(YardWorkersArea2)
	REMOVE_SCENARIO_BLOCKING_AREA(YardWorkersArea3)
	REMOVE_SCENARIO_BLOCKING_AREA(YardWorkersArea4)

	IF DOES_SCENARIO_GROUP_EXIST("SCRAP_SECURITY") 
		IF IS_SCENARIO_GROUP_ENABLED("SCRAP_SECURITY") 
			SET_SCENARIO_GROUP_ENABLED("SCRAP_SECURITY",FALSE)
		ENDIF
	ENDIF
	
	SET_MAX_WANTED_LEVEL(5)
	
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iLeftBarrier)
		REMOVE_DOOR_FROM_SYSTEM(iLeftBarrier)
	ENDIF
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iRightBarrier)
		REMOVE_DOOR_FROM_SYSTEM(iRightBarrier)
	ENDIF
	
	PRINTSTRING("Finale Heist Prep B Mission Cleanup") PRINTNL()
	
	TERMINATE_THIS_THREAD()
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Passed()

	PRINTSTRING("...Placeholder Mission Passed")
	PRINTNL()
	
	Mission_Flow_Mission_Passed()
	Mission_Cleanup()
	
ENDPROC



// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------



/// PURPOSE:
///    Sets the fail reason, updates the mission stage and resets iControlFlag
///    (If we've already failed the mission, it only edits the fail reason
///    - so this can be used to update fail reason after mission has faded out)
/// PARAMS:
///    eFail - the reason the player has failed the mission
PROC SET_MISSION_FAILED(FAIL_REASON eFail)
	eFailReason = eFail
	IF missionStage <> STAGE_MISSION_FAILED
		missionStage = STAGE_MISSION_FAILED
		iControlFlag = 0
	ENDIF
ENDPROC

//PURPOSE: Handle's all cases where the mission can fail
PROC FAIL_CHECKS()

	//Fail mission if both CARGOBOBs are destroyed
//	IF DOES_ENTITY_EXIST(Driller)
//		IF NOT IS_VEHICLE_DRIVEABLE(Driller)
//			SET_MISSION_FAILED(FAIL_CUTTER_DESTROYED)
//			EXIT		
//		ENDIF
//	ENDIF
	IF DOES_ENTITY_EXIST(DrillerTruck)
		IF NOT IS_VEHICLE_DRIVEABLE(DrillerTruck)
			SET_MISSION_FAILED(FAIL_TRUCK_DESTROYED)
			EXIT		
		ELSE
			IF IS_VEHICLE_PERMANENTLY_STUCK(DrillerTruck)
				SET_MISSION_FAILED(FAIL_TRUCK_STUCK)
				PRINTSTRING("Mission Failed = The Truck is permanently stuck") PRINTNL()
				EXIT	
			ENDIF
			IF DOES_ENTITY_EXIST(DrillerTrailer)
				IF NOT IS_VEHICLE_DRIVEABLE(DrillerTrailer)
					SET_MISSION_FAILED(FAIL_TRAILER_DESTROYED)
					EXIT		
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(DrillerTrailer)
					IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(DrillerTruck)
						PRINTSTRING("trailer is not attached") PRINTNL()
						IF IS_VEHICLE_STUCK_ON_ROOF(DrillerTrailer)
						OR IS_VEHICLE_FUCKED(DrillerTrailer)
						OR IS_VEHICLE_PERMANENTLY_STUCK(DrillerTrailer)
							SET_MISSION_FAILED(FAIL_TRAILER_DESTROYED)
							PRINTSTRING("trailer is not attached and is either stuck on roof/is fucked/is permanently stuck") PRINTNL()
							EXIT							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF missionStage = STAGE_STEAL_DRILLER
	OR missionStage = STAGE_TAKE_DRILLER_HOME
		IF DOES_ENTITY_EXIST(DrillerTrailer)
			IF IS_VEHICLE_DRIVEABLE(DrillerTrailer)
				IF GET_DISTANCE_BETWEEN_ENTITIES(DrillerTrailer, PLAYER_PED_ID()) > 600
					SET_MISSION_FAILED(FAIL_CUTTER_ABANDONED)
					EXIT
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Fail mission if end guard is killed.
	IF DOES_ENTITY_EXIST(EndGuard)
		IF IS_PED_INJURED(EndGuard)
			SET_MISSION_FAILED(FAIL_ENDGUARD_KILLED)
			PRINTSTRING("MISSION FAILED end guard has been killed") PRINTNL()
			EXIT
		ENDIF
	ENDIF
	
ENDPROC


//═════════════════════════════════╡ DEBUG ╞═══════════════════════════════════
//DEBUG STAGE SELECTOR NAMING
#IF IS_DEBUG_BUILD
PROC SET_DEBUG_STAGE_NAMES()

	SkipMenuStruct[0].sTxtLabel = "START_MISSION - GET TO DRILLER"  						// Stage 0 Name missionStage = STAGE_GET_TO_DRILLER
	SkipMenuStruct[1].sTxtLabel = "STAGE_STEAL_DRILLER"  									// Stage 1 Name missionStage = STAGE_STEAL_DRILLER
	SkipMenuStruct[2].sTxtLabel = "STAGE_TAKE_DRILLER_TO_LOCATION"  						// Stage 2 Name missionStage = STAGE_TAKE_DRILLER_TO_LOCATION
	SkipMenuStruct[3].sTxtLabel = "STAGE_END_MISSION"  										// Stage 3 Name missionStage = STAGE_END_CUTSCENE
	
ENDPROC
#ENDIF

#IF IS_DEBUG_BUILD
PROC DEBUG_KEYS()

		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			Mission_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			SET_MISSION_FAILED(FAIL_DEFAULT)
			EXIT
		ENDIF
		
		// Check for J-skip
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
		
			IF NOT IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_OUT(0)
			ENDIF		
		
			SWITCH missionStage
			
				CASE STAGE_GET_TO_DRILLER
					
					iControlFlag = 0
					MissionStageBeingSkippedTo = TRUE
					missionStage = STAGE_STEAL_DRILLER
					
				BREAK	
		
				CASE STAGE_STEAL_DRILLER
					
					iControlFlag = 0
					MissionStageBeingSkippedTo = TRUE
					missionStage = STAGE_TAKE_DRILLER_HOME
					
				BREAK	
				
				CASE STAGE_TAKE_DRILLER_HOME
					
					iControlFlag = 0
					MissionStageBeingSkippedTo = TRUE
					missionStage = STAGE_CLOSING_CUTSCENE
					
				BREAK	
			
			ENDSWITCH
			
		ENDIF	
		
		// Check for P-skip
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
		
			IF NOT IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_OUT(0)
			ENDIF		
		
			SWITCH missionStage
			
				CASE STAGE_STEAL_DRILLER
					
					iControlFlag = 0
					MissionStageBeingSkippedTo = TRUE
					missionStage = STAGE_STEAL_DRILLER
					
				BREAK	
		
				CASE STAGE_TAKE_DRILLER_HOME
					
					iControlFlag = 0
					MissionStageBeingSkippedTo = TRUE
					missionStage = STAGE_STEAL_DRILLER
					
				BREAK	
				
				CASE STAGE_CLOSING_CUTSCENE
					
					iControlFlag = 0
					MissionStageBeingSkippedTo = TRUE
					missionStage = STAGE_TAKE_DRILLER_HOME
					
				BREAK	
			
			ENDSWITCH
			
		ENDIF	
ENDPROC	
#ENDIF

//PURPOSE: Sets the reaction state for the worker towards the player
PROC SET_WORKER_REACTION_STATE(WORKERS_REACTION_TO_PLAYER e_new_state)
	eWorkerReaction 	= e_new_state
	iCaseStage 			= 0 
ENDPROC

//PURPOSE: Checks if player is aiming at any of the peds.
FUNC BOOL IS_PLAYER_AIMING_AT_PED(PED_INDEX ePed, INT iWeaponFlags)

	IF DOES_ENTITY_EXIST(ePed)
	AND NOT IS_PED_INJURED(ePed)
		
		IF IS_PED_ARMED(PLAYER_PED_ID(), iWeaponFlags)
			IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), ePed)
			OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), ePed)
		
				RETURN TRUE
			
			ENDIF
		ENDIF
		
	ENDIF

	RETURN FALSE
ENDFUNC

//PURPOSE: Updates the workers attitude if it is changed.
PROC UPDATE_WORKERS_ATTITUDE(PED_INDEX worker)
	
//	PRINTSTRING("iCaseStage = ") PRINTINT(iCaseStage) PRINTNL()
	//Handle workers looking at the player regardless of stage
	FOR icount3 = 0 TO 6
		IF worker = EnemyPed[icount3]
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), worker) < SafeDistance	
			AND CAN_PED_SEE_HATED_PED(worker, PLAYER_PED_ID())
				IF WorkerLookTaskGiven[icount3] = FALSE
					TASK_LOOK_AT_ENTITY(worker, PLAYER_PED_ID(), -1, SLF_USE_TORSO|SLF_WIDEST_PITCH_LIMIT|SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_HIGH)
					WorkerLookTaskGiven[icount3] = TRUE
				ENDIF
			ELSE
				IF WorkerLookTaskGiven[icount3] = TRUE
					TASK_LOOK_AT_ENTITY(worker, PLAYER_PED_ID(), 1, SLF_USE_TORSO|SLF_WIDEST_PITCH_LIMIT|SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_HIGH)
					WorkerLookTaskGiven[icount3] = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	SWITCH eWorkerReaction
		CASE IGNORES_PLAYER
//			PRINTSTRING("IGNORES_PLAYER") PRINTNL()
			IF iCaseStage = 0
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(worker, TRUE)	
				//Set the workers back to their original postions and give them their scenarios to play/
				IF worker = EnemyPed[0]
					IF DOES_SCENARIO_EXIST_IN_AREA(<<912.2, -1542.5, 29.8>>, 3, FALSE)
						IF GET_SCRIPT_TASK_STATUS(EnemyPed[0], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(EnemyPed[0], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
							TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[0], <<912.2, -1542.5, 29.8>>, 20, 0)
						ENDIF
					ENDIF
				ENDIF
				IF worker = EnemyPed[1]
					IF DOES_SCENARIO_EXIST_IN_AREA(<<917.4, -1517.4, 30>>, 3, FALSE)
						IF GET_SCRIPT_TASK_STATUS(EnemyPed[1], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(EnemyPed[1], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
							TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[1], <<917.4, -1517.4, 30>>, 20, 0)
						ENDIF
					ENDIF
				ENDIF
				IF worker = EnemyPed[2]
					IF DOES_SCENARIO_EXIST_IN_AREA(<<869.8, -1541.2, 29.4>>, 3, FALSE)
						IF GET_SCRIPT_TASK_STATUS(EnemyPed[2], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(EnemyPed[2], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
							TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[2], <<869.8, -1541.2, 29.4>>, 20, 0)
						ENDIF
					ENDIF
				ENDIF
				IF worker = EnemyPed[3]
					IF DOES_SCENARIO_EXIST_IN_AREA(<<884.2, -1574.1, 30>>, 3, FALSE)
						IF GET_SCRIPT_TASK_STATUS(EnemyPed[3], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(EnemyPed[3], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
							TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[3], <<884.2, -1574.1, 30>>, 20, 0)
						ENDIF
					ENDIF
				ENDIF
				IF worker = EnemyPed[4]
					IF DOES_SCENARIO_EXIST_IN_AREA(<<904, -1575, 30>>, 3, FALSE)
						IF GET_SCRIPT_TASK_STATUS(EnemyPed[4], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(EnemyPed[4], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
							TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[4], <<904, -1575, 30>>, 20, 0)
						ENDIF
					ENDIF
				ENDIF
				IF worker = EnemyPed[5]
					IF DOES_SCENARIO_EXIST_IN_AREA(<<905.9, -1574.8, 29.9>>, 3, FALSE)
						IF GET_SCRIPT_TASK_STATUS(EnemyPed[5], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(EnemyPed[5], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
							TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[5], <<905.9, -1574.8, 29.9>>, 20, 0)
						ENDIF
					ENDIF
				ENDIF
				IF worker = EnemyPed[6]
					IF DOES_SCENARIO_EXIST_IN_AREA(<<889.5, -1562.0, 29.7>>, 3, FALSE)
						IF GET_SCRIPT_TASK_STATUS(EnemyPed[6], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(EnemyPed[6], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
							TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[6], <<889.5, -1562.0, 29.7>>, 20, 0)
						ENDIF
					ENDIF
				ENDIF
				iCaseStage++
			ENDIF
			IF iCaseStage = 1
				
				//Make the worker 4 move to a new location to allow for the player to time it right and steal the truck undetected
//				IF worker = EnemyPed[4]
				IF DOES_ENTITY_EXIST(EnemyPed[4])
					IF NOT IS_PED_INJURED(EnemyPed[4])
						IF DOES_ENTITY_EXIST(EnemyPed[5])
							IF NOT IS_PED_INJURED(EnemyPed[5])
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed[4]) < 25
									IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<926.455017,-1582.384399,29.327585>>, <<909.632202,-1581.374268,31.589067>>, 9.500000)
									OR vplayerCoords.y > -1573
										IF doneChat19 = FALSE
											ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, EnemyPed[4], "CONSTRUCTION2")
											ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, EnemyPed[5], "CONSTRUCTION3")
											TASK_LOOK_AT_ENTITY(EnemyPed[4], EnemyPed[5], -1)
											TASK_LOOK_AT_ENTITY(EnemyPed[5], EnemyPed[4], -1)
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												IF NOT IS_MESSAGE_BEING_DISPLAYED()
												OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
													IF CREATE_CONVERSATION(MyLocalPedStruct, "SOL1AUD", "SOL1_TALK", CONV_PRIORITY_LOW, DO_NOT_DISPLAY_SUBTITLES, DO_NOT_ADD_TO_BRIEF_SCREEN)
														//When we going to be finished with that metro tunnel?
														//...
														//Don't worry - I will.
														doneChat19 = TRUE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
									
								//If player is near the truck have a 4 second wait before the ped will move off. so player has to stay hidden for a few seconds.
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<921.9, -1556.5, 30>>, <<5,5,5>>, FALSE, TRUE, TM_ON_FOOT)
									IF moveTimerSet = FALSE
										iMoveTimer = GET_GAME_TIMER()
										moveTimerSet = TRUE
									ENDIF
									IF moveTimerSet = TRUE 
										IF GET_GAME_TIMER() > (iMoveTimer + 4000)
											IF doneChat19 = FALSE
												ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, EnemyPed[4], "CONSTRUCTION2")
												ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, EnemyPed[5], "CONSTRUCTION3")
												TASK_LOOK_AT_ENTITY(EnemyPed[4], EnemyPed[5], -1)
												TASK_LOOK_AT_ENTITY(EnemyPed[5], EnemyPed[4], -1)												
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													IF NOT IS_MESSAGE_BEING_DISPLAYED()
													OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
														IF CREATE_CONVERSATION(MyLocalPedStruct, "SOL1AUD", "SOL1_TALK", CONV_PRIORITY_LOW, DO_NOT_DISPLAY_SUBTITLES, DO_NOT_ADD_TO_BRIEF_SCREEN)
															//When we going to be finished with that metro tunnel?
															//...
															//Don't worry - I will.
															doneChat19 = TRUE
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								//Make ped 4 walk off after the chat is finished.
								IF doneChat19 = TRUE
								AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF taskForPed4Given = FALSE
										IF DOES_SCENARIO_EXIST_IN_AREA(<<865, -1558.1, 29.5>>, 3, FALSE)
											TASK_CLEAR_LOOK_AT(EnemyPed[4])
											TASK_CLEAR_LOOK_AT(EnemyPed[5])
											TASK_USE_NEAREST_SCENARIO_TO_COORD(EnemyPed[4], <<865, -1558.1, 29.5>>, 3, 0)
											taskForPed4Given = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Make worker 6 take a walk around a couple of scenarios
				IF worker = EnemyPed[6]
					IF worker6TaskGiven = FALSE
						IF worker6TimerStarted = FALSE
							iworker6Timer = GET_GAME_TIMER()
							worker6TimerStarted = TRUE
						ENDIF
						IF GET_GAME_TIMER() > (iworker6Timer + 30000)
							IF worker6TaskGiven = FALSE
								IF DOES_SCENARIO_EXIST_IN_AREA(<<925, -1561, 30>>, 3, FALSE)
									TASK_USE_NEAREST_SCENARIO_TO_COORD(EnemyPed[6], <<925, -1561, 30>>, 3, 0)
									worker6TaskGiven = TRUE
									worker6TimerStarted = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF worker6TaskGiven = TRUE
						IF IS_ENTITY_AT_COORD(EnemyPed[6], <<925, -1561, 30>>, <<3,3,3>>)
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed[6]) < 15
							AND vplayerCoords.z < 40
								IF worker6Task2Given = FALSE
									IF DOES_SCENARIO_EXIST_IN_AREA(<<909.5, -1515.5, 30>>, 3, FALSE)
										TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[6], <<909.5, -1515.5, 30>>, 50, 0)
	//									OPEN_SEQUENCE_TASK(seq)
	//										TASK_GO_STRAIGHT_TO_COORD(NULL, <<925.5, -1535.6, 30>>, PEDMOVEBLENDRATIO_WALK, -1)
	//										TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(NULL, <<909.5, -1515.5, 30>>, 50, 0)
	//									CLOSE_SEQUENCE_TASK(seq)
	//									TASK_PERFORM_SEQUENCE(EnemyPed[6], seq)
	//									CLEAR_SEQUENCE_TASK(seq)
										worker6Task2Given = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
		BREAK
		CASE CURIOUS_OF_PLAYER
//			PRINTSTRING("CURIOUS_OF_PLAYER") PRINTNL()
			
			IF iCaseStage = 0
				FOR icount3 = 0 TO 6
//					IF WorkerLookTaskGiven[icount3] = FALSE
//						PRINTSTRING("WorkerLookTaskGiven ") PRINTINT(icount3 )PRINTSTRING("= FALSE ")PRINTNL()
//					ELSE
//						PRINTSTRING("WorkerLookTaskGiven ") PRINTINT(icount3 )PRINTSTRING("= TRUE ")PRINTNL()
//					ENDIF				
	
					//Only task the workers to go to the player if the guards are not coming for the player.
					IF eYardGuardMood = NOT_BOTHERED_BY_PLAYER
						IF worker = EnemyPed[icount3]
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), worker) < SafeDistance	
								//Set the workers to be curious of the player for 30 seconds and if the player doesn't leave then make them approach him
								IF WorkerApproachTaskGiven[icount3] = FALSE
									IF CAN_PED_SEE_HATED_PED(worker, PLAYER_PED_ID())
									OR CAN_PED_HEAR_PLAYER(PLAYER_ID(), worker)
										IF NOT IS_ENTITY_ON_SCREEN(worker)
											CLEAR_PED_TASKS_IMMEDIATELY(worker)
										ENDIF
										IF IS_ENTITY_ON_SCREEN(worker)
											CLEAR_PED_TASKS(worker)
										ENDIF
										OPEN_SEQUENCE_TASK(seq)
											TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), -1, 2, PEDMOVEBLENDRATIO_WALK)
											TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(worker, seq)
										CLEAR_SEQUENCE_TASK(seq)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(worker, TRUE)
										WorkerLookTaskGiven[icount3] = FALSE
										WorkerApproachTaskGiven[icount3] = TRUE
									ENDIF
								ENDIF
								IF WorkerApproachTaskGiven[icount3] = TRUE
									IF NOT IS_PED_FACING_PED(worker, PLAYER_PED_ID(), 45)
										TASK_TURN_PED_TO_FACE_ENTITY(worker, PLAYER_PED_ID())
										WorkerApproachTaskGiven[icount3] = FALSE
									ENDIF	
								ENDIF
							ELSE
								//Reset flags
								IF WorkerApproachTaskGiven[icount3] = TRUE
									WorkerApproachTaskGiven[icount3] = FALSE
								ENDIF
								//Set the workers back to their original postions and give them their scenarios to play/
								IF worker = EnemyPed[0]
									IF DOES_SCENARIO_EXIST_IN_AREA(<<912.2, -1542.5, 29.8>>, 3, FALSE)
										IF GET_SCRIPT_TASK_STATUS(EnemyPed[0], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(EnemyPed[0], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
											TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[0], <<912.2, -1542.5, 29.8>>, 20, 0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[0], TRUE)
										ENDIF
									ENDIF
								ENDIF
								IF worker = EnemyPed[1]
									IF DOES_SCENARIO_EXIST_IN_AREA(<<917.4, -1517.4, 30>>, 3, FALSE)
										IF GET_SCRIPT_TASK_STATUS(EnemyPed[1], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(EnemyPed[1], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
											TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[1], <<917.4, -1517.4, 30>>, 20, 0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[1], TRUE)
										ENDIF
									ENDIF
								ENDIF
								IF worker = EnemyPed[2]
									IF DOES_SCENARIO_EXIST_IN_AREA(<<869.8, -1541.2, 29.4>>, 3, FALSE)
										IF GET_SCRIPT_TASK_STATUS(EnemyPed[2], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(EnemyPed[2], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
											TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[2], <<869.8, -1541.2, 29.4>>, 20, 0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[2], TRUE)
										ENDIF
									ENDIF
								ENDIF
								IF worker = EnemyPed[3]
									IF DOES_SCENARIO_EXIST_IN_AREA(<<884.2, -1574.1, 30>>, 3, FALSE)
										IF GET_SCRIPT_TASK_STATUS(EnemyPed[3], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(EnemyPed[3], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
											TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[3], <<884.2, -1574.1, 30>>, 20, 0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[3], TRUE)
										ENDIF
									ENDIF
								ENDIF
								IF worker = EnemyPed[4]
									IF DOES_SCENARIO_EXIST_IN_AREA(<<904, -1575, 30>>, 3, FALSE)
										IF GET_SCRIPT_TASK_STATUS(EnemyPed[4], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(EnemyPed[4], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
											TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[4], <<904, -1575, 30>>, 20, 0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[4], TRUE)
										ENDIF
									ENDIF
								ENDIF
								IF worker = EnemyPed[5]
									IF DOES_SCENARIO_EXIST_IN_AREA(<<905.9, -1574.8, 29.9>>, 3, FALSE)
										IF GET_SCRIPT_TASK_STATUS(EnemyPed[5], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(EnemyPed[5], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
											TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[5], <<905.9, -1574.8, 29.9>>, 20, 0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[5], TRUE)
										ENDIF
									ENDIF
								ENDIF
								IF worker = EnemyPed[6]
									IF DOES_SCENARIO_EXIST_IN_AREA(<<889.5, -1562.0, 29.7>>, 3, FALSE)
										IF GET_SCRIPT_TASK_STATUS(EnemyPed[6], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(EnemyPed[6], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
											TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[6], <<889.5, -1562.0, 29.7>>, 20, 0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[6], TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						//Just task them to look at the player and possibly play some pointing anims as if pointing him out to the guards
						IF worker = EnemyPed[icount3]
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), worker) < SafeDistance	
								//Set the workers to be curious of the player for 30 seconds and if the player doesn't leave then make them approach him
								IF WorkerApproachTaskGiven[icount3] = FALSE
									IF CAN_PED_SEE_HATED_PED(worker, PLAYER_PED_ID())
									OR CAN_PED_HEAR_PLAYER(PLAYER_ID(), worker)
										IF NOT IS_ENTITY_ON_SCREEN(worker)
											CLEAR_PED_TASKS_IMMEDIATELY(worker)
										ENDIF
										IF IS_ENTITY_ON_SCREEN(worker)
											CLEAR_PED_TASKS(worker)
										ENDIF
										OPEN_SEQUENCE_TASK(seq)
											TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
											TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1, SLF_USE_TORSO|SLF_WIDEST_PITCH_LIMIT|SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_HIGH)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(worker, seq)
										CLEAR_SEQUENCE_TASK(seq)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(worker, TRUE)
										WorkerLookTaskGiven[icount3] = FALSE
										WorkerApproachTaskGiven[icount3] = TRUE
									ENDIF
								ENDIF
								IF WorkerApproachTaskGiven[icount3] = TRUE
									IF NOT IS_PED_FACING_PED(worker, PLAYER_PED_ID(), 45)
										TASK_TURN_PED_TO_FACE_ENTITY(worker, PLAYER_PED_ID())
										WorkerApproachTaskGiven[icount3] = FALSE
									ENDIF	
								ENDIF
							ELSE
								//Reset flags
								IF WorkerApproachTaskGiven[icount3] = TRUE
									WorkerApproachTaskGiven[icount3] = FALSE
								ENDIF
								//Set the workers back to their original postions and give them their scenarios to play/
								IF worker = EnemyPed[0]
									IF DOES_SCENARIO_EXIST_IN_AREA(<<912.2, -1542.5, 29.8>>, 3, FALSE)
										IF GET_SCRIPT_TASK_STATUS(EnemyPed[0], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(EnemyPed[0], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
											TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[0], <<912.2, -1542.5, 29.8>>, 20, 0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[0], TRUE)
										ENDIF
									ENDIF
								ENDIF
								IF worker = EnemyPed[1]
									IF DOES_SCENARIO_EXIST_IN_AREA(<<917.4, -1517.4, 30>>, 3, FALSE)
										IF GET_SCRIPT_TASK_STATUS(EnemyPed[1], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(EnemyPed[1], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
											TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[1], <<917.4, -1517.4, 30>>, 20, 0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[1], TRUE)
										ENDIF
									ENDIF
								ENDIF
								IF worker = EnemyPed[2]
									IF DOES_SCENARIO_EXIST_IN_AREA(<<869.8, -1541.2, 29.4>>, 3, FALSE)
										IF GET_SCRIPT_TASK_STATUS(EnemyPed[2], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(EnemyPed[2], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
											TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[2], <<869.8, -1541.2, 29.4>>, 20, 0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[2], TRUE)
										ENDIF
									ENDIF
								ENDIF
								IF worker = EnemyPed[3]
									IF DOES_SCENARIO_EXIST_IN_AREA(<<884.2, -1574.1, 30>>, 3, FALSE)
										IF GET_SCRIPT_TASK_STATUS(EnemyPed[3], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(EnemyPed[3], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
											TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[3], <<884.2, -1574.1, 30>>, 20, 0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[3], TRUE)
										ENDIF
									ENDIF
								ENDIF
								IF worker = EnemyPed[4]
									IF DOES_SCENARIO_EXIST_IN_AREA(<<904, -1575, 30>>, 3, FALSE)
										IF GET_SCRIPT_TASK_STATUS(EnemyPed[4], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(EnemyPed[4], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
											TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[4], <<904, -1575, 30>>, 20, 0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[4], TRUE)
										ENDIF
									ENDIF
								ENDIF
								IF worker = EnemyPed[5]
									IF DOES_SCENARIO_EXIST_IN_AREA(<<905.9, -1574.8, 29.9>>, 3, FALSE)
										IF GET_SCRIPT_TASK_STATUS(EnemyPed[5], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(EnemyPed[5], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
											TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[5], <<905.9, -1574.8, 29.9>>, 20, 0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[5], TRUE)
										ENDIF
									ENDIF
								ENDIF
								IF worker = EnemyPed[6]
									IF DOES_SCENARIO_EXIST_IN_AREA(<<889.5, -1562.0, 29.7>>, 3, FALSE)
										IF GET_SCRIPT_TASK_STATUS(EnemyPed[6], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
										AND GET_SCRIPT_TASK_STATUS(EnemyPed[6], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
											TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[6], <<889.5, -1562.0, 29.7>>, 20, 0)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[6], TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF						
					ENDIF
				ENDFOR
			ENDIF
		BREAK
		CASE DISLIKES_PLAYER
//			PRINTSTRING("DISLIKES_PLAYER") PRINTNL()
			//Flag for stat should not be set as player has been spotted.
			IF PlayerSpotted = FALSE
				PlayerSpotted = TRUE
			ENDIF

			IF iCaseStage = 0
				//Set the group to dislike player now but only have them react if they are close enough to the player
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, workerMainGroup, RELGROUPHASH_PLAYER)
				iCaseStage++
			ENDIF
			IF iCaseStage = 1
				FOR icount3 = 0 TO 6
					IF worker = EnemyPed[icount3]
						IF GET_DISTANCE_BETWEEN_ENTITIES(worker, PLAYER_PED_ID()) < 15
							IF GET_DISTANCE_BETWEEN_ENTITIES(worker, PLAYER_PED_ID()) > 3
								IF GET_SCRIPT_TASK_STATUS(worker, SCRIPT_TASK_PERFORM_SEQUENCE) <> WAITING_TO_START_TASK
								OR GET_SCRIPT_TASK_STATUS(worker, SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
									IF NOT IS_ENTITY_ON_SCREEN(worker)
										CLEAR_PED_TASKS_IMMEDIATELY(worker)
									ENDIF
									IF IS_ENTITY_ON_SCREEN(worker)
										CLEAR_PED_TASKS(worker)
									ENDIF
									OPEN_SEQUENCE_TASK(seq)
										TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), -1, 1, PEDMOVEBLENDRATIO_WALK)
										TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(worker, seq)
									CLEAR_SEQUENCE_TASK(seq)
									WorkerLookTaskGiven[icount3] = FALSE
								ENDIF
							ENDIF
						ELSE
							//Set the workers back to their original postions and give them their scenarios to play/
							IF worker = EnemyPed[0]
								IF DOES_SCENARIO_EXIST_IN_AREA(<<912.2, -1542.5, 29.8>>, 3, FALSE)
									IF GET_SCRIPT_TASK_STATUS(EnemyPed[0], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(EnemyPed[0], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
										TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[0], <<912.2, -1542.5, 29.8>>, 20, 0)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[0], TRUE)
									ENDIF
								ENDIF
							ENDIF
							IF worker = EnemyPed[1]
								IF DOES_SCENARIO_EXIST_IN_AREA(<<917.4, -1517.4, 30>>, 3, FALSE)
									IF GET_SCRIPT_TASK_STATUS(EnemyPed[1], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(EnemyPed[1], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
										TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[1], <<917.4, -1517.4, 30>>, 20, 0)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[1], TRUE)
									ENDIF
								ENDIF
							ENDIF
							IF worker = EnemyPed[2]
								IF DOES_SCENARIO_EXIST_IN_AREA(<<869.8, -1541.2, 29.4>>, 3, FALSE)
									IF GET_SCRIPT_TASK_STATUS(EnemyPed[2], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(EnemyPed[2], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
										TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[2], <<869.8, -1541.2, 29.4>>, 20, 0)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[2], TRUE)
									ENDIF
								ENDIF
							ENDIF
							IF worker = EnemyPed[3]
								IF DOES_SCENARIO_EXIST_IN_AREA(<<884.2, -1574.1, 30>>, 3, FALSE)
									IF GET_SCRIPT_TASK_STATUS(EnemyPed[3], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(EnemyPed[3], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
										TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[3], <<884.2, -1574.1, 30>>, 20, 0)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[3], TRUE)
									ENDIF
								ENDIF
							ENDIF
							IF worker = EnemyPed[4]
								IF DOES_SCENARIO_EXIST_IN_AREA(<<904, -1575, 30>>, 3, FALSE)
									IF GET_SCRIPT_TASK_STATUS(EnemyPed[4], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(EnemyPed[4], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
										TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[4], <<904, -1575, 30>>, 20, 0)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[4], TRUE)
									ENDIF
								ENDIF
							ENDIF
							IF worker = EnemyPed[5]
								IF DOES_SCENARIO_EXIST_IN_AREA(<<905.9, -1574.8, 29.9>>, 3, FALSE)
									IF GET_SCRIPT_TASK_STATUS(EnemyPed[5], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(EnemyPed[5], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
										TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[5], <<905.9, -1574.8, 29.9>>, 20, 0)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[5], TRUE)
									ENDIF
								ENDIF
							ENDIF
							IF worker = EnemyPed[6]
								IF DOES_SCENARIO_EXIST_IN_AREA(<<889.5, -1562.0, 29.7>>, 3, FALSE)
									IF GET_SCRIPT_TASK_STATUS(EnemyPed[6], SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
									AND GET_SCRIPT_TASK_STATUS(EnemyPed[6], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
										TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(EnemyPed[6], <<889.5, -1562.0, 29.7>>, 20, 0)
										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[6], TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF		
		BREAK
		CASE HATES_PLAYER
//			PRINTSTRING("HATES_PLAYER") PRINTNL()
			//Flag for stat should not be set as player has been spotted.
			IF PlayerSpotted = FALSE
				PlayerSpotted = TRUE
			ENDIF

			//Give the player a wanted level after 20seconds
			IF wantedLevelSet = FALSE
				IF wantedLevelTimerStarted = FALSE
					iWantedLevelTimer = GET_GAME_TIMER()
					wantedLevelTimerStarted = TRUE
				ENDIF
				IF wantedLevelTimerStarted = TRUE
					IF GET_GAME_TIMER() > (iWantedLevelTimer + 9000)
						SET_MAX_WANTED_LEVEL(5)
						SET_WANTED_LEVEL_MULTIPLIER(1)
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						PRINTSTRING("WANTED LEVEL BEING SET TO 2 AS TIMER IS OVER 15SECONDS") PRINTNL()
						wantedLevelSet = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF iCaseStage = 0
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, workerMainGroup, RELGROUPHASH_PLAYER)
				iCaseStage++
			ENDIF
			IF iCaseStage = 1
				IF missionStage = STAGE_STEAL_DRILLER
					
					FOR icount3 = 0 TO 6
						IF worker = EnemyPed[icount3]
							IF GET_DISTANCE_BETWEEN_ENTITIES(worker, PLAYER_PED_ID()) < 100
								IF blockedEventsForHate[icount3] = FALSE
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(worker, TRUE)
									blockedEventsForHate[icount3] = TRUE
								ENDIF
								IF attackPlayerTaskGiven[icount3] = FALSE
									IF NOT IS_ENTITY_ON_SCREEN(worker)
										CLEAR_PED_TASKS_IMMEDIATELY(worker)
									ENDIF
									IF IS_ENTITY_ON_SCREEN(worker)
										CLEAR_PED_TASKS(worker)
									ENDIF
									TASK_COMBAT_HATED_TARGETS_AROUND_PED(worker, 100)
									WorkerLookTaskGiven[icount3] = FALSE
									attackPlayerTaskGiven[icount3] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				
					//Make ped 0 run infront of the truck if the player is getting in or is in it and hasn't drove off yet.
					IF ped0TaskGiven = FALSE
						IF DOES_ENTITY_EXIST(EnemyPed[0])
							IF NOT IS_PED_INJURED(EnemyPed[0])
								IF DOES_ENTITY_EXIST(DrillerTruck)
									IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
										IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DrillerTruck, TRUE)
										AND IS_ENTITY_AT_COORD(DrillerTruck, <<919.2, -1554.4, 30>>, <<4,4,4>>)
											IF NOT IS_ENTITY_ON_SCREEN(EnemyPed[0])
												CLEAR_PED_TASKS_IMMEDIATELY(EnemyPed[0])
											ENDIF
											IF IS_ENTITY_ON_SCREEN(EnemyPed[0])
												CLEAR_PED_TASKS(EnemyPed[0])
											ENDIF
											OPEN_SEQUENCE_TASK(seq)
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<917.8, -1561, 29>>, PEDMOVEBLENDRATIO_SPRINT, -1)
												TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID())
												TASK_PLAY_ANIM(NULL, "misscarsteal4@director_grip","mcs_2_loop_grip1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(EnemyPed[0], seq)
											CLEAR_SEQUENCE_TASK(seq)
											ped0TaskGiven = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					//Have him shouting at the player
					IF ped0TaskGiven = TRUE
					AND chatPed0 = FALSE
						IF DOES_ENTITY_EXIST(EnemyPed[0])
							IF NOT IS_PED_INJURED(EnemyPed[0])
								IF IS_ENTITY_AT_COORD(EnemyPed[0], <<917.8, -1561, 29>>, <<2,2,2>>)
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, EnemyPed[0], "FHPrepBWorker")
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_SHOUT", CONV_PRIORITY_MEDIUM)
//									IF PLAY_AMBIENT_DIALOGUE_LINE(MyLocalPedStruct, EnemyPed[0], "FHPBAUD", "FHPB_SHOUT")
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(EnemyPed[0], "FHPB_BBAA", "FHPrepBWorker", SPEECH_PARAMS_FORCE_FRONTEND)
									//Woah
									chatPed0 = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
									
//					//Make ped 5 phone police
					IF ped5PhonePolice = FALSE
						IF GET_GAME_TIMER() > (iWantedLevelTimer + 3000)
							IF DOES_ENTITY_EXIST(EnemyPed[5])
								IF NOT IS_PED_INJURED(EnemyPed[5])
									TASK_PLAY_ANIM(EnemyPed[5],"cellphone@str","cellphone_call_listen_c",SLOW_BLEND_IN,Normal_BLEND_OUT,-1,AF_LOOPING | AF_SECONDARY | AF_UPPERBODY)
									iStartPhoneTimer = GET_GAME_TIMER()
									ped5PhonePolice = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF ped5CombatTaskGiven = FALSE
							IF GET_GAME_TIMER() > (iStartPhoneTimer + 5000)
								IF DOES_ENTITY_EXIST(EnemyPed[5])
									IF NOT IS_PED_INJURED(EnemyPed[5])
										CLEAR_PED_TASKS(EnemyPed[5])
										TASK_COMBAT_PED(EnemyPed[5], PLAYER_PED_ID())
										ped5CombatTaskGiven = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				
				ENDIF
					
				//Make them give up once the player gets more than 200metres away from them if they are in a car, and 100m if on foot
				FOR icount3 = 0 TO 6
					IF FleeTaskGiven[icount3] = FALSE
						IF worker = EnemyPed[icount3]
							IF DOES_ENTITY_EXIST(worker)
								IF NOT IS_PED_INJURED(worker)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(worker, TRUE)
									IF IS_PED_IN_ANY_VEHICLE(worker)
										IF GET_DISTANCE_BETWEEN_ENTITIES(worker, PLAYER_PED_ID()) > 200
											IF NOT IS_ENTITY_ON_SCREEN(worker)
												CLEAR_PED_TASKS_IMMEDIATELY(worker)
											ENDIF
											IF IS_ENTITY_ON_SCREEN(worker)
												CLEAR_PED_TASKS(worker)
											ENDIF
											TASK_SMART_FLEE_PED(worker, PLAYER_PED_ID(), 1000, -1)
											FleeTaskGiven[icount3] = TRUE
										ENDIF
									ELSE
										IF GET_DISTANCE_BETWEEN_ENTITIES(worker, PLAYER_PED_ID()) > 100
											IF NOT IS_ENTITY_ON_SCREEN(worker)
												CLEAR_PED_TASKS_IMMEDIATELY(worker)
											ENDIF
											IF IS_ENTITY_ON_SCREEN(worker)
												CLEAR_PED_TASKS(worker)
											ENDIF
											TASK_SMART_FLEE_PED(worker, PLAYER_PED_ID(), 1000, -1)
											FleeTaskGiven[icount3] = TRUE
										ENDIF	
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
			IF missionStage = STAGE_TAKE_DRILLER_HOME
			
				//Make them give up once the player gets more than 200metres away from them if they are in a car, and 100m if on foot
				FOR icount3 = 0 TO 6
					IF FleeTaskGiven[icount3] = FALSE
						IF worker = EnemyPed[icount3]
							IF DOES_ENTITY_EXIST(worker)
								IF NOT IS_PED_INJURED(worker)
									IF NOT IS_ENTITY_ON_SCREEN(worker)
										CLEAR_PED_TASKS_IMMEDIATELY(worker)
									ENDIF
									IF IS_ENTITY_ON_SCREEN(worker)
										CLEAR_PED_TASKS(worker)
									ENDIF
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(worker, TRUE)
									TASK_SMART_FLEE_PED(worker, PLAYER_PED_ID(), 1000, -1)
									SET_PED_KEEP_TASK(worker, TRUE)
									SET_PED_AS_NO_LONGER_NEEDED(worker)
									FleeTaskGiven[icount3] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR				
			
			ENDIF
		BREAK
		CASE SCARED_OF_PLAYER
//			PRINTSTRING("SCARED_OF_PLAYER") PRINTNL()
			//Flag for stat should not be set as player has been spotted.
			IF PlayerSpotted = FALSE
				PlayerSpotted = TRUE
			ENDIF			
			
			//Give the player a wanted level after 20seconds
			IF wantedLevelSet = FALSE
				IF wantedLevelTimerStarted = FALSE
					iWantedLevelTimer = GET_GAME_TIMER()
					wantedLevelTimerStarted = TRUE
				ENDIF
				IF wantedLevelTimerStarted = TRUE
					IF GET_GAME_TIMER() > (iWantedLevelTimer + 6000)
						SET_MAX_WANTED_LEVEL(5)
						SET_WANTED_LEVEL_MULTIPLIER(1)
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						PRINTSTRING("WANTED LEVEL BEING SET TO 2 AS TIMER IS OVER 6SECONDS") PRINTNL()
						wantedLevelSet = TRUE
					ENDIF
				ENDIF
			ENDIF

			IF iCaseStage = 0
				//Set the group to hate the player now but manually task them to flee apart from the 2 who should get in the car and chase you
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, workerMainGroup, RELGROUPHASH_PLAYER)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(worker, TRUE)
				iCaseStage++
			ENDIF
			
			IF iCaseStage = 1
				
				FOR icount3 = 0 TO 6
					IF DOES_ENTITY_EXIST(EnemyPed[icount3])
						IF NOT IS_PED_INJURED(EnemyPed[icount3])
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed[icount3]) < 15
								IF IS_PLAYER_AIMING_AT_PED(EnemyPed[icount3], WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
									IF CAN_PED_SEE_HATED_PED(EnemyPed[icount3], PLAYER_PED_ID())
										IF handsUpTaskGiven[icount3] = FALSE
											IF NOT IS_ENTITY_ON_SCREEN(EnemyPed[icount3])
												CLEAR_PED_TASKS_IMMEDIATELY(EnemyPed[icount3])
											ENDIF
											IF IS_ENTITY_ON_SCREEN(EnemyPed[icount3])
												CLEAR_PED_TASKS(EnemyPed[icount3])
											ENDIF
											TASK_HANDS_UP(EnemyPed[icount3], -1, PLAYER_PED_ID(), -1)
											handsUpTaskGiven[icount3] = TRUE
											SmartFleeTaskGiven[icount3] = FALSE
											WorkerLookTaskGiven[icount3] = FALSE
											iCanSeeTimer = GET_GAME_TIMER()
										ENDIF
									ELSE
										IF GET_GAME_TIMER() > (iCanSeeTimer + 300)
											SmartFleeTaskGiven[icount3] = FALSE
										ENDIF
									ENDIF	
								ELSE
									IF SmartFleeTaskGiven[icount3] = FALSE
										IF NOT IS_ENTITY_ON_SCREEN(EnemyPed[icount3])
											CLEAR_PED_TASKS_IMMEDIATELY(EnemyPed[icount3])
										ENDIF
										IF IS_ENTITY_ON_SCREEN(EnemyPed[icount3])
											CLEAR_PED_TASKS(EnemyPed[icount3])
										ENDIF
										TASK_SMART_FLEE_PED(EnemyPed[icount3], PLAYER_PED_ID(), 250, -1)
										SmartFleeTaskGiven[icount3] = TRUE
										handsUpTaskGiven[icount3] = FALSE
									ENDIF
								ENDIF
							ELSE
								IF SmartFleeTaskGiven[icount3] = FALSE
									IF NOT IS_ENTITY_ON_SCREEN(EnemyPed[icount3])
										CLEAR_PED_TASKS_IMMEDIATELY(EnemyPed[icount3])
									ENDIF
									IF IS_ENTITY_ON_SCREEN(EnemyPed[icount3])
										CLEAR_PED_TASKS(EnemyPed[icount3])
									ENDIF
									TASK_SMART_FLEE_PED(EnemyPed[icount3], PLAYER_PED_ID(), 250, -1)
									SmartFleeTaskGiven[icount3] = TRUE
									handsUpTaskGiven[icount3] = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
				
//				//Make peds 4 and 5 get in the car and chase the player if the player is in the truck
//				IF worker = EnemyPed[4]
//				OR worker = EnemyPed[5]
//					IF DOES_ENTITY_EXIST(DrillerTruck)
//						IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
//							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DrillerTruck)
//								IF DOES_ENTITY_EXIST(EnemyCar)
//									IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//										IF worker = EnemyPed[4]
//											IF drivingPed4TaskGiven = FALSE
//												IF DOES_ENTITY_EXIST(worker)
//													IF NOT IS_PED_INJURED(worker)
//														IF NOT IS_PED_IN_ANY_VEHICLE(worker)
//															TASK_ENTER_VEHICLE(worker, EnemyCar, -1, VS_FRONT_RIGHT)
//															drivingPed4TaskGiven = TRUE
//														ENDIF
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF
//										IF worker = EnemyPed[5]
//											IF drivingPed5TaskGiven = FALSE
//												IF DOES_ENTITY_EXIST(worker)
//													IF NOT IS_PED_INJURED(worker)
//														IF NOT IS_PED_IN_ANY_VEHICLE(worker)
//															TASK_ENTER_VEHICLE(worker, EnemyCar, -1)
//															drivingPed5TaskGiven = TRUE
//														ENDIF
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF	
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				//Give any worker the task to chase the player if they are in the driver seat of the enemycar.
//				IF PedDriveTaskGiven = FALSE
//					IF DOES_ENTITY_EXIST(EnemyCar)
//						IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//							IF DOES_ENTITY_EXIST(worker)
//								IF NOT IS_PED_INJURED(worker)
//									IF IS_PED_IN_VEHICLE(worker, EnemyCar)
//										IF IS_PED_SITTING_IN_VEHICLE_SEAT(worker, EnemyCar, VS_DRIVER)
//											TASK_VEHICLE_CHASE(worker, PLAYER_PED_ID())
//											PedDriveTaskGiven = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
				
//				//Give the passenger a drive by task if sitting in passenger seat
//				IF PedDriveByTaskGiven = FALSE
//					IF DOES_ENTITY_EXIST(EnemyCar)
//						IF IS_VEHICLE_DRIVEABLE(EnemyCar)
//							IF DOES_ENTITY_EXIST(worker)
//								IF NOT IS_PED_INJURED(worker)
//									IF IS_PED_IN_VEHICLE(worker, EnemyCar)
//										IF IS_PED_SITTING_IN_VEHICLE_SEAT(worker, EnemyCar, VS_FRONT_RIGHT)
//											GIVE_WEAPON_TO_PED(worker, WEAPONTYPE_PISTOL, 200, TRUE)
//											TASK_DRIVE_BY(worker, PLAYER_PED_ID(), NULL, <<0,0,0>>, 200, 100)
//											PedDriveByTaskGiven = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF						
	
			ENDIF		
		BREAK

	ENDSWITCH

ENDPROC

//PURPOSE: Returns TRUE once a worker has been added to the chat
FUNC BOOL IS_WORKER_ADDED_TO_DIALOGUE()
	PED_INDEX ClosestPed
	
	FOR icount5 = 0 TO 6
		IF DOES_ENTITY_EXIST(EnemyPed[icount5])
//			PRINTSTRING("EnemyPed ") PRINTINT(icount5) PRINTSTRING(" exists")PRINTNL()
			IF NOT IS_PED_INJURED(EnemyPed[icount5])
//				PRINTSTRING("EnemyPed ") PRINTINT(icount5) PRINTSTRING(" Is alive")PRINTNL()
				//Determine who is closest to the player for the chat
//				GET_CLOSEST_PED(GET_ENTITY_COORDS(PLAYER_PED_ID()), 30, FALSE, TRUE, ClosestPed, FALSE, TRUE)
				ClosestPed = GET_PED_CLOSEST_PED_FROM_RELATIONSHIP_GROUP(PLAYER_PED_ID(), workerMainGroup, 0, FALSE, PEDTYPE_INVALID)
				IF DOES_ENTITY_EXIST(closestPed)
//					PRINTSTRING("ClosestPed ") PRINTINT(icount5) PRINTSTRING(" exists")PRINTNL()
					IF NOT IS_PED_INJURED(closestPed)
//						PRINTSTRING("ClosestPed ") PRINTINT(icount5) PRINTSTRING(" Is alive")PRINTNL()
					ELSE
//						PRINTSTRING("ClosestPed ") PRINTINT(icount5) PRINTSTRING(" Is dead")PRINTNL()
					ENDIF
				ELSE
//					PRINTSTRING("ClosestPed ") PRINTINT(icount5) PRINTSTRING(" doesn't exist")PRINTNL()
				ENDIF
				IF ClosestPed = EnemyPed[icount5]
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ClosestPed) < 15
//					PRINTSTRING("EnemyPed ") PRINTINT(icount5) PRINTSTRING(" is the closest ped  RETURNING TRUE")PRINTNL()
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, EnemyPed[icount5], "CONSTRUCTION3")
					RETURN TRUE
				ENDIF
			ENDIF
		ELSE
//			PRINTSTRING("EnemyPed ") PRINTINT(icount5) PRINTSTRING("doesn't exist")PRINTNL()
		ENDIF
	ENDFOR
	
	RETURN FALSE

ENDFUNC

//PURPOSE: Returns TRUE once the player has been added for dialogue
FUNC BOOL IS_PLAYER_ADDED_TO_DIALOGUE()

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL	
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
		RETURN TRUE
	ENDIF	
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN	
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
		RETURN TRUE
	ENDIF

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR	
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, PLAYER_PED_ID(), "TREVOR")
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC


//PURPOSE: Creates and handles everything for the guard at the car park
PROC MONITOR_END_GUARD()

	//Create the guard when player is within range of the car park
	IF NOT DOES_ENTITY_EXIST(EndGuard)
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<45, -616.2, 30.8>>) < 150
			REQUEST_MODEL(S_M_M_Security_01)
			IF HAS_MODEL_LOADED(S_M_M_Security_01)
				EndGuard = CREATE_PED(PEDTYPE_MISSION, S_M_M_Security_01, <<64.9040, -617.9949, 30.7028>>, 223.5550)
				SET_PED_CONFIG_FLAG(EndGuard, PCF_DontBehaveLikeLaw, TRUE)
			ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_Security_01)
		ENDIF
	ENDIF
	
	//Give the guard the look at the player task if close enough
	IF DOES_ENTITY_EXIST(EndGuard)
		IF NOT IS_PED_INJURED(EndGuard)	
			//Keep him playing the guard scenario				
			IF DOES_SCENARIO_EXIST_IN_AREA(<<65.2, -617.2, 30.8>>, 5, FALSE)
//				PRINTSTRING("scenario exists") PRINTNL()
				IF endguardTaskGiven = FALSE
//				IF GET_SCRIPT_TASK_STATUS(EndGuard, SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
//				AND GET_SCRIPT_TASK_STATUS(EndGuard, SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> WAITING_TO_START_TASK
					IF IS_ENTITY_ON_SCREEN(EndGuard)
						CLEAR_PED_TASKS(EndGuard)
					ELSE
						CLEAR_PED_TASKS_IMMEDIATELY(EndGuard)
					ENDIF
					PRINTSTRING("TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD should be getting called now") PRINTNL()
					TASK_USE_NEAREST_SCENARIO_TO_COORD(EndGuard, <<65.2, -617.2, 31>>, 2)
					SET_PED_KEEP_TASK(EndGuard, TRUE)
					iEndGuardTaskTimer = GET_GAME_TIMER()
					endguardTaskGiven = TRUE
				ELSE	
//					IF GET_SCRIPT_TASK_STATUS(EndGuard, SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS) <> PERFORMING_TASK
//						endguardTaskGiven = FALSE
//					ENDIF
					IF GET_GAME_TIMER() > (iEndGuardTaskTimer + 5000)
						IF NOT IS_PED_USING_ANY_SCENARIO(EndGuard)
							PRINTSTRING("endguardTaskGiven being set to false as EndGuard is not using any scenario") PRINTNL()
							endguardTaskGiven = FALSE
						ENDIF
					ENDIF
				ENDIF
			ELSE
//				PRINTSTRING("scenario doesn't exist") PRINTNL()
			ENDIF
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EndGuard) < 20
				IF lookAtTaskGiven = FALSE
					TASK_LOOK_AT_ENTITY(EndGuard, PLAYER_PED_ID(), -1, SLF_USE_TORSO|SLF_WIDEST_PITCH_LIMIT|SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_HIGH)
					lookAtTaskGiven = TRUE
				ENDIF
			ELSE
				IF lookAtTaskGiven = TRUE
					TASK_LOOK_AT_ENTITY(EndGuard, PLAYER_PED_ID(), 1)
					lookAtTaskGiven = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_9)
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<76.5746, -633.6944, 30.6189>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 55.0265)	
		ENDIF
	#ENDIF
	
	//Now handle the guard for when the player arrives.
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<45, -616.2, 30.8>>) < 50
		
		IF BarriersAddedToSystem = FALSE
			iLeftBarrier = HASH("LeftBarrier")
			iRightBarrier = HASH("RightBarrier")

			//Register the door
			ADD_DOOR_TO_SYSTEM(iLeftBarrier, PROP_SEC_BARRIER_LD_01A, <<61, -633, 32>>)	
			ADD_DOOR_TO_SYSTEM(iRightBarrier, PROP_SEC_BARRIER_LD_01A, <<66, -618, 32>>)
			BarriersAddedToSystem = TRUE
		ENDIF
	
		//Open and close the barriers
		IF BarriersAddedToSystem = TRUE
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
//				PRINTSTRING("fOpenRatio = ") PRINTFLOAT(fOpenRatio) PRINTNL()
				//Open the barriers
				IF vplayerCoords.x > 35
					IF fOpenRatio < 1 
						fOpenRatio = fOpenRatio + 0.02
					ELSE
						fOpenRatio = 1
					ENDIF
					IF fOpenRatio <= 1
	//					PRINTSTRING("SET_STATE_OF_CLOSEST_DOOR_OF_TYPE IS BEING CALLED") PRINTNL()
						DOOR_SYSTEM_SET_OPEN_RATIO(iLeftBarrier, fOpenRatio, FALSE, TRUE)
						DOOR_SYSTEM_SET_OPEN_RATIO(iRightBarrier, fOpenRatio, FALSE, TRUE)
						DOOR_SYSTEM_SET_DOOR_STATE(iLeftBarrier, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
						DOOR_SYSTEM_SET_DOOR_STATE(iRightBarrier, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
//						SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_SEC_BARRIER_LD_01A, <<61, -633, 32>>, TRUE, fOpenRatio)
//						SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_SEC_BARRIER_LD_01A, <<66, -618, 32>>, TRUE, fOpenRatio)
						gatesLocked = FALSE
					ENDIF
				ENDIF
				//Close the barriers
				IF vplayerCoords.x < 35
					IF fOpenRatio > 0 
						fOpenRatio = fOpenRatio - 0.02
					ELSE
						fOpenRatio = 0
					ENDIF
					IF fOpenRatio >= 0
						DOOR_SYSTEM_SET_OPEN_RATIO(iLeftBarrier, fOpenRatio, FALSE, TRUE)
						DOOR_SYSTEM_SET_OPEN_RATIO(iRightBarrier, fOpenRatio, FALSE, TRUE)
						DOOR_SYSTEM_SET_DOOR_STATE(iLeftBarrier, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
						DOOR_SYSTEM_SET_DOOR_STATE(iRightBarrier, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
//						SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_SEC_BARRIER_LD_01A, <<61, -633, 32>>, TRUE, fOpenRatio)
//						SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_SEC_BARRIER_LD_01A, <<66, -618, 32>>, TRUE, fOpenRatio)
						gatesLocked = FALSE
					ENDIF	
				ENDIF
			ELSE
				//Lock the gates if player has wanted level
				IF gatesLocked = FALSE
					//Close the barriers
					IF vplayerCoords.x < 35
						IF fOpenRatio > 0 
							fOpenRatio = fOpenRatio - 0.02
						ELSE
							fOpenRatio = 0
						ENDIF
						IF fOpenRatio >= 0
							DOOR_SYSTEM_SET_OPEN_RATIO(iLeftBarrier, fOpenRatio, FALSE, TRUE)
							DOOR_SYSTEM_SET_OPEN_RATIO(iRightBarrier, fOpenRatio, FALSE, TRUE)
							DOOR_SYSTEM_SET_DOOR_STATE(iLeftBarrier, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
							DOOR_SYSTEM_SET_DOOR_STATE(iRightBarrier, DOORSTATE_FORCE_LOCKED_THIS_FRAME, FALSE, TRUE)
//							SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_SEC_BARRIER_LD_01A, <<61, -633, 32>>, TRUE, fOpenRatio)
//							SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_SEC_BARRIER_LD_01A, <<66, -618, 32>>, TRUE, fOpenRatio)
							gatesLocked = TRUE
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<62.901512,-638.632874,29.918798>>, <<70.789192,-617.133789,36.917885>>, 10.750000)					
			IF DOES_ENTITY_EXIST(EndGuard)
				IF NOT IS_PED_INJURED(EndGuard)		
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						IF doneChat21 = FALSE
							IF doneChat20 = FALSE
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, EndGuard, "FHPBSecGuard")
								IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT20", CONV_PRIORITY_MEDIUM)
									//Hey, you must be Lester's acquaintance.
									//Just park her up over there.
									doneChat20 = TRUE
								ENDIF
							ENDIF
						ELSE
							IF doneChat22 = FALSE
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, EndGuard, "FHPBSecGuard")
								IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT22", CONV_PRIORITY_MEDIUM)
									//That's better, you must be Lester's aqcuaintance. Park her up over there.
									doneChat22 = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF doneChat21 = FALSE
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, EndGuard, "FHPBSecGuard")
							IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT21", CONV_PRIORITY_MEDIUM)
								//Hey, lose the heat first dude. What the fuck.
								doneChat21 = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

//PURPOSE: Handles all the mission dialogue
PROC MONITOR_DIALOGUE()
	
	//Do chat between worker and player for when they first enter the yard.
	IF NOT IS_MESSAGE_BEING_DISPLAYED()
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
			IF missionStage = STAGE_STEAL_DRILLER
				
				//Only allow the chat if the guards haven't seen the player
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				AND eYardGuardMood = NOT_BOTHERED_BY_PLAYER
					//Do specific chat depending on who the player character is
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL	
						IF eWorkerReaction = CURIOUS_OF_PLAYER
							IF DoneCuriousChat = FALSE
								IF IS_WORKER_ADDED_TO_DIALOGUE()
								AND IS_PLAYER_ADDED_TO_DIALOGUE()
									IF CREATE_CONVERSATION(MyLocalPedStruct, "SOL1AUD", "SOL1_SEE5", CONV_PRIORITY_MEDIUM)//CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT4", CONV_PRIORITY_MEDIUM)
										//Excuse me! You can't be here!
										//Hello! This is restricted access!
										//Whoa! What you doing here!
										iSee6ChatTimer = GET_GAME_TIMER()
										DoneCuriousChat = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						IF eWorkerReaction = CURIOUS_OF_PLAYER
							IF DoneCuriousChat = TRUE
							AND GET_GAME_TIMER() > (iSee6ChatTimer + 7000)
								IF IS_WORKER_ADDED_TO_DIALOGUE()
								AND IS_PLAYER_ADDED_TO_DIALOGUE()
									IF CREATE_CONVERSATION(MyLocalPedStruct, "SOL1AUD", "SOL1_SEE6", CONV_PRIORITY_MEDIUM)//CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT4", CONV_PRIORITY_MEDIUM)
										//You got to leave - now. 
										//Turn around and leave, pal! 
										//Off you go - like, right now. 
										iSee6ChatTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
//					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN	
//						IF eWorkerReaction = CURIOUS_OF_PLAYER
//							IF DoneCuriousChat = FALSE
//								IF IS_WORKER_ADDED_TO_DIALOGUE()
//								AND IS_PLAYER_ADDED_TO_DIALOGUE()
//									IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT6", CONV_PRIORITY_MEDIUM)
//										//Can I help you? 
//										//I'm just looking for something.
//										//Well you can't be in here so you'll need to leave. 
//										DoneCuriousChat = TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR	
//						IF eWorkerReaction = CURIOUS_OF_PLAYER
//							IF DoneCuriousChat = FALSE
//								IF IS_WORKER_ADDED_TO_DIALOGUE()
//								AND IS_PLAYER_ADDED_TO_DIALOGUE()
//									IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT5", CONV_PRIORITY_MEDIUM)
//										//Can I help you? 
//										//Fuck off!
//										//Screw you man, get the fuck out of here before I call the cops.
//										DoneCuriousChat = TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
					
					IF eWorkerReaction = DISLIKES_PLAYER
						IF doneChat24 = FALSE
							IF IS_WORKER_ADDED_TO_DIALOGUE()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "SOL1AUD", "SOL1_SEC3", CONV_PRIORITY_MEDIUM)//CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT24", CONV_PRIORITY_MEDIUM)
									//Security! Come here and do your job.
									//Security! There's a guy over here!
									//Get the security guards - this guy's an asshole!
									ichat24timer = GET_GAME_TIMER()
									doneChat24 = TRUE
								ENDIF
							ENDIF
						ENDIF											
					ENDIF
					
				ELSE
					//Have some random dialogue from the workers if the guards are chasing after the player and they can see him.
					IF eYardGuardMood = HATE_PLAYER
					AND eWorkerReaction = CURIOUS_OF_PLAYER
						IF doneChat27 = FALSE
							IF IS_WORKER_ADDED_TO_DIALOGUE()
							AND IS_PLAYER_ADDED_TO_DIALOGUE()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "SOL1AUD", "SOL1_SEE6", CONV_PRIORITY_MEDIUM)
									//You got to leave - now. 
									//Turn around and leave, pal! 
									//Off you go - like, right now. 
									iChat27Timer = GET_GAME_TIMER()
									doneChat27 = TRUE
								ENDIF
							ENDIF
						ELSE
							IF GET_GAME_TIMER() > (iChat27Timer + 8000)
								IF IS_WORKER_ADDED_TO_DIALOGUE()
								AND IS_PLAYER_ADDED_TO_DIALOGUE()
									IF CREATE_CONVERSATION(MyLocalPedStruct, "SOL1AUD", "SOL1_SEE6", CONV_PRIORITY_MEDIUM)
										//You got to leave - now. 
										//Turn around and leave, pal! 
										//Off you go - like, right now. 
										iChat27Timer = GET_GAME_TIMER()
									ENDIF									
								ENDIF
							ENDIF
						ENDIF						
					ENDIF
				ENDIF
				
//				//The following can be said regardless of who the player is
//				IF eWorkerReaction = DISLIKES_PLAYER
//					IF DoneCuriousChat = TRUE
//						IF DoneChat7 = FALSE
//							IF IS_WORKER_ADDED_TO_DIALOGUE()
//								IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT7", CONV_PRIORITY_MEDIUM)
//									//Hey I said you need to leave. I'll call the cops if you don't.
//									DoneChat7 = TRUE
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
				
				IF eWorkerReaction = SCARED_OF_PLAYER
					IF DoneChat8 = FALSE
						KILL_ANY_CONVERSATION()
						IF IS_WORKER_ADDED_TO_DIALOGUE()
						
							WEAPON_TYPE weap
							GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), weap)
							
							IF weap != WEAPONTYPE_UNARMED
							AND weap != WEAPONTYPE_INVALID
								IF CREATE_CONVERSATION(MyLocalPedStruct, "SOL1AUD", "SOL1_ARM3", CONV_PRIORITY_MEDIUM)
									//Weapon!  Look out!
									//This crazy's armed!
									//He's got a weapon!
									iRandomScreamTimer = GET_GAME_TIMER()
									randomScreamTimerSet = TRUE								
									DoneChat8 = TRUE
								ENDIF
							ELSE
								//Set flags without doing conversation
								iRandomScreamTimer = GET_GAME_TIMER()
								randomScreamTimerSet = TRUE								
								DoneChat8 = TRUE
							ENDIF
						ENDIF
					ENDIF
					IF DoneChat8 = TRUE
						IF randomScreamTimerSet = FALSE
							iRandomScreamTimer = GET_GAME_TIMER()
							randomScreamTimerSet = TRUE
						ENDIF
						IF randomScreamTimerSet = TRUE
							IF GET_GAME_TIMER() > (iRandomScreamTimer + 9000)
								IF IS_WORKER_ADDED_TO_DIALOGUE()
									IF CREATE_CONVERSATION(MyLocalPedStruct, "SOL1AUD", "SOL1_PAN3", CONV_PRIORITY_MEDIUM)
										//Shit!!!
										//Oh, crap!!!
										//Help me!!!
										randomScreamTimerSet = FALSE
									ENDIF
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF eWorkerReaction = HATES_PLAYER
					
					//If the guards haven't seen the player play this line but then have the guards set to hate player.
					IF WorkerSpottedPlayerFirst = TRUE
						IF doneChat10 = FALSE
							IF PlayerIsStealingTruck = TRUE
								IF IS_WORKER_ADDED_TO_DIALOGUE()
									IF CREATE_CONVERSATION(MyLocalPedStruct, "SOL1AUD", "SOL1_STEAL3", CONV_PRIORITY_MEDIUM)
										//Look out - the rig!
										//The rig, guys!  He's taking it!
										//This guy ain't meant to be in the rig!
										doneChat10 = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF startedChat12 = FALSE
						IF DOES_ENTITY_EXIST(DrillerTruck)
							IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DrillerTruck)					
									IF IS_WORKER_ADDED_TO_DIALOGUE()
										IF CREATE_CONVERSATION(MyLocalPedStruct, "SOL1AUD", "SOL1_OUT3", CONV_PRIORITY_MEDIUM)
											//Pull him out on his ass!
											//Drag his ass out of there!
											//Get him out! Come on!
											ichat12Timer = GET_GAME_TIMER()
											doneChat10 = TRUE
										ENDIF
									ENDIF	
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF GET_GAME_TIMER() > (ichat12Timer + 10000)
						IF DOES_ENTITY_EXIST(DrillerTruck)
							IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DrillerTruck)	
									IF IS_WORKER_ADDED_TO_DIALOGUE()
										IF CREATE_CONVERSATION(MyLocalPedStruct, "SOL1AUD", "SOL1_OUT3", CONV_PRIORITY_MEDIUM)
											//Pull him out on his ass!
											//Drag his ass out of there!
											//Get him out! Come on!
											ichat12Timer = GET_GAME_TIMER()
										ENDIF
									ENDIF	
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF playerSpottedOnRoof = TRUE
						IF doneChat23 = FALSE
							IF IS_WORKER_ADDED_TO_DIALOGUE()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "SOL1AUD", "SOL1_SEC3", CONV_PRIORITY_MEDIUM)
									//Security! Come here and do your job.
									//Security! There's a guy over here!
									//Get the security guards - this guy's an asshole!
									doneChat23 = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
				
//				//Do shouting from the guys if they are chasing the player in the van and are away from the yard
//				IF eWorkerReaction = SCARED_OF_PLAYER
//				OR eWorkerReaction = HATES_PLAYER
//					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -1915.7711, -635.8125, 9.4879 >>) > 100
//						IF startedChaseChat = FALSE
//							IF IS_WORKER_ADDED_TO_DIALOGUE()
//								IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT11", CONV_PRIORITY_MEDIUM)
//									//Pull over asshole!
//									//You think you can outrun us in that thing?
//									//Stop the truck and we won't kill you.
//									//Give it up you bastard!
//									//The police will get you scumbag!
//									//You've fucked with the wrong guys!
//									ichat11Timer = GET_GAME_TIMER()
//									startedChaseChat = TRUE
//								ENDIF	
//							ENDIF
//						ENDIF
//						IF GET_GAME_TIMER() > (ichat11Timer + 10000)
//							IF IS_WORKER_ADDED_TO_DIALOGUE()
//								IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT11", CONV_PRIORITY_MEDIUM)
//									//Pull over asshole!
//									//You think you can outrun us in that thing?
//									//Stop the truck and we won't kill you.
//									//Give it up you bastard!
//									//The police will get you scumbag!
//									//You've fucked with the wrong guys!
//									ichat11Timer = GET_GAME_TIMER()
//								ENDIF	
//							ENDIF	
//						ENDIF
//					ENDIF
//				ENDIF
				
			ENDIF
		ENDIF
	ENDIF

ENDPROC 

//PURPOSE: Returns true if player has been spotted by any workers or guards
FUNC BOOL PlayerSpottedCarryingWeapon()

	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())

		FOR iweaponSpotCount = 0 TO 6
			IF DOES_ENTITY_EXIST(EnemyPed[iweaponSpotCount])
				IF NOT IS_PED_INJURED(EnemyPed[iweaponSpotCount])
					IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
					AND NOT IS_PHONE_ONSCREEN()
						IF CAN_PED_SEE_HATED_PED(EnemyPed[iweaponSpotCount], PLAYER_PED_ID())
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
		
		IF DOES_ENTITY_EXIST(YardGuard[0])
			IF NOT IS_PED_INJURED(YardGuard[0])
				IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
				AND NOT IS_PHONE_ONSCREEN()
					IF CAN_PED_SEE_HATED_PED(YardGuard[0], PLAYER_PED_ID())
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
		
		IF DOES_ENTITY_EXIST(YardGuard[1])
			IF NOT IS_PED_INJURED(YardGuard[1])
				IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
				AND NOT IS_PHONE_ONSCREEN()
					IF CAN_PED_SEE_HATED_PED(YardGuard[1], PLAYER_PED_ID())
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	ENDIF
	
	RETURN FALSE

ENDFUNC

//PURPOSE:
FUNC BOOL CAN_PED_SEE_DEAD_PED(PED_INDEX alivePed, PED_INDEX deadPed)
	
	IF GET_DISTANCE_BETWEEN_ENTITIES(alivePed, deadPed) < SafeDistance
	AND IS_PED_FACING_PED(alivePed, deadPed, 90)
	AND NOT IS_PED_INJURED(alivePed)
	AND IS_PED_INJURED(deadPed)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

//PURPOSE: Handles how the worker peds react to the player
PROC MONITOR_WORKER_PEDS()
	
	//Control Flags
	
	//Workout if all workers are far enough away from player.
	FOR icount2 = 0 TO 6
		IF DOES_ENTITY_EXIST(EnemyPed[icount2])
			IF NOT IS_PED_INJURED(EnemyPed[icount2])
				IF pedIsFarAway[icount2] = FALSE
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed[icount2]) > SafeDistance
						pedIsFarAway[icount2] = TRUE
					ENDIF
				ENDIF
				IF pedIsFarAway[icount2] = TRUE
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed[icount2]) < SafeDistance
						pedIsFarAway[icount2] = FALSE
					ENDIF
				ENDIF
			ELSE
				IF pedIsFarAway[icount2] = FALSE
					pedIsFarAway[icount2] = TRUE
				ENDIF
			ENDIF
		ELSE
			IF pedIsFarAway[icount2] = FALSE
				pedIsFarAway[icount2] = TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	//Set the main flag for if all workers are far away or not
	IF pedIsFarAway[0] = TRUE
	AND pedIsFarAway[1] = TRUE
	AND pedIsFarAway[2] = TRUE
	AND pedIsFarAway[3] = TRUE
	AND pedIsFarAway[4] = TRUE
	AND pedIsFarAway[5] = TRUE
		AllWorkersAreFarAway = TRUE
	ELSE
		AllWorkersAreFarAway = FALSE
	ENDIF
	
	//Ped specific checks
//	//Check if ped 5 can see any dead peds
//	IF DOES_ENTITY_EXIST(EnemyPed[5])
//		IF NOT IS_PED_INJURED(EnemyPed[5])
//			IF DOES_ENTITY_EXIST(EnemyPed[4])
//				IF GET_DISTANCE_BETWEEN_ENTITIES(EnemyPed[4], EnemyPed[5]) < 20
//					IF IS_PED_FACING_PED(EnemyPed[5], EnemyPed[4], 170)
//						IF IS_PED_INJURED(EnemyPed[4])
//							IF eWorkerReaction <> SCARED_OF_PLAYER
//								SET_WORKER_REACTION_STATE(SCARED_OF_PLAYER)
//								PRINTSTRING("SET_WORKER_REACTION_STATE(SCARED_OF_PLAYER) 5") PRINTNL()
//								PRINTSTRING("EnemyPed 5 can see dead ped 4") PRINTNL()
//								deadBodyFound = TRUE
//							ENDIF	
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	
	//Go through every possible scenario here and make the workers respond accordingly by updating their eWorkerReaction
	FOR icount = 0 TO 6
		IF DOES_ENTITY_EXIST(EnemyPed[icount])
			IF NOT IS_PED_INJURED(EnemyPed[icount])
			
				UPDATE_WORKERS_ATTITUDE(EnemyPed[icount])
			
				IF missionStage = STAGE_STEAL_DRILLER
				OR missionStage = STAGE_TAKE_DRILLER_HOME
				
					//Basic rule if the guards hate the player the workers should not be set to Ignore player
					IF eYardGuardMood = HATE_PLAYER
					AND eWorkerReaction = IGNORES_PLAYER
						SET_WORKER_REACTION_STATE(CURIOUS_OF_PLAYER)
						IF doneChat19 = TRUE
							KILL_ANY_CONVERSATION()
						ENDIF
					ENDIF
				
					//If a worker ped spots a dead body
					IF HAS_PED_RECEIVED_EVENT(EnemyPed[icount], EVENT_DEAD_PED_FOUND)
						IF eWorkerReaction <> SCARED_OF_PLAYER
							SET_WORKER_REACTION_STATE(SCARED_OF_PLAYER)
							IF doneChat19 = TRUE
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
							PRINTSTRING("SET_WORKER_REACTION_STATE(SCARED_OF_PLAYER) 4") PRINTNL()
							PRINTSTRING("EnemyPed ") PRINTINT(icount ) PRINTSTRING("has found a dead body") PRINTNL()
							deadBodyFound = TRUE
						ENDIF	
					ENDIF	
					
					IF eWorkerReaction <> SCARED_OF_PLAYER
						IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, <<892.9, -1552.4, 30>>, 40)
							SET_WORKER_REACTION_STATE(SCARED_OF_PLAYER)
							IF doneChat19 = TRUE
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
							PRINTSTRING("SET_WORKER_REACTION_STATE(SCARED_OF_PLAYER) 6") PRINTNL()
						ENDIF	
					ENDIF
						
					IF CAN_PED_SEE_DEAD_PED(EnemyPed[icount], EnemyPed[0])
					OR CAN_PED_SEE_DEAD_PED(EnemyPed[icount], EnemyPed[1])
					OR CAN_PED_SEE_DEAD_PED(EnemyPed[icount], EnemyPed[2])
					OR CAN_PED_SEE_DEAD_PED(EnemyPed[icount], EnemyPed[3])
					OR CAN_PED_SEE_DEAD_PED(EnemyPed[icount], EnemyPed[4])
					OR CAN_PED_SEE_DEAD_PED(EnemyPed[icount], EnemyPed[5])
					OR CAN_PED_SEE_DEAD_PED(EnemyPed[icount], EnemyPed[6])
					OR CAN_PED_SEE_DEAD_PED(EnemyPed[icount], YardGuard[0])
					OR CAN_PED_SEE_DEAD_PED(EnemyPed[icount], YardGuard[1])
						IF eWorkerReaction <> SCARED_OF_PLAYER
							SET_WORKER_REACTION_STATE(SCARED_OF_PLAYER)
							IF doneChat19 = TRUE
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ENDIF
							PRINTSTRING("SET_WORKER_REACTION_STATE(SCARED_OF_PLAYER) 5") PRINTNL()
							PRINTSTRING("EnemyPed ") PRINTINT(icount ) PRINTSTRING("has found a dead body") PRINTNL()
							deadBodyFound = TRUE
						ENDIF
					ENDIF
				
					//If player has no weapon out and has caused no problems to any of the workers do the following
					IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_UNARMED
					OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
					OR IS_PHONE_ONSCREEN()
						IF GuardsShouldCombat = FALSE
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed[icount]) > SafeDistance
								//Reset timer flags if player is far enough away from workers.
								IF dislikeTimerStarted[icount] = TRUE
									dislikeTimerStarted[icount] = FALSE
								ENDIF
								IF AllWorkersAreFarAway = TRUE
								AND eWorkerReaction <> IGNORES_PLAYER
								AND eWorkerReaction <> DISLIKES_PLAYER
								AND eWorkerReaction <> HATES_PLAYER
								AND eWorkerReaction <> SCARED_OF_PLAYER
								AND eYardGuardMood <> HATE_PLAYER
									SET_WORKER_REACTION_STATE(IGNORES_PLAYER)
									PRINTSTRING("SET_WORKER_REACTION_STATE(IGNORES_PLAYER) 1") PRINTNL()
								ENDIF
							ENDIF
							//Set the workers group to be curious of the player if he is less than 20m
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed[icount]) < SafeDistance
							AND CAN_PED_SEE_HATED_PED(EnemyPed[icount], PLAYER_PED_ID())

								IF eWorkerReaction <> CURIOUS_OF_PLAYER
								AND eWorkerReaction <> DISLIKES_PLAYER
								AND eWorkerReaction <> HATES_PLAYER
								AND eWorkerReaction <> SCARED_OF_PLAYER
									IF GuardsAlerted = FALSE
									AND GuardsShouldCombat = FALSE
										SET_WORKER_REACTION_STATE(CURIOUS_OF_PLAYER)
										IF doneChat19 = TRUE
											KILL_ANY_CONVERSATION()
										ENDIF
										PRINTSTRING("SET_WORKER_REACTION_STATE(CURIOUS_OF_PLAYER) 1") PRINTNL()
									ELSE
										SET_WORKER_REACTION_STATE(HATES_PLAYER)
										IF doneChat19 = TRUE
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										ENDIF
										PRINTSTRING("SET_WORKER_REACTION_STATE(HATES_PLAYER) 1") PRINTNL()
									ENDIF
								ENDIF

							ENDIF
							//Set them to dislike the player if he doesn't move away in under 30 seconds
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed[icount]) < SafeDistance
							AND CAN_PED_SEE_HATED_PED(EnemyPed[icount], PLAYER_PED_ID())
								IF dislikeTimerStarted[icount] = FALSE
	//								PRINTSTRING("dislikeTimerStarted ") PRINTINT(icount) PRINTNL()
									iDislikeTimer[icount] = GET_GAME_TIMER()
									dislikeTimerStarted[icount] = TRUE
								ENDIF
								//Set the workers to be curious of the player for 30 seconds and if the player doesn't leave then make them dislike him
								IF dislikeTimerStarted[icount] = TRUE
	//								PRINTSTRING("dislikeTimerStarted ") PRINTINT(icount) PRINTSTRING("= TRUE ")PRINTNL()
									IF GET_GAME_TIMER() > (iDislikeTimer[icount] + 24000)
										IF eWorkerReaction <> DISLIKES_PLAYER
										AND eWorkerReaction <> HATES_PLAYER
										AND eWorkerReaction <> SCARED_OF_PLAYER 
											SET_WORKER_REACTION_STATE(DISLIKES_PLAYER)
											IF doneChat19 = TRUE
												KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											ENDIF
											PRINTSTRING("SET_WORKER_REACTION_STATE(DISLIKES_PLAYER) 1") PRINTNL()
										ENDIF
									ELSE
	//									PRINTSTRING("GAMETIMER IS LESS THAN  iDislikeTimer ") PRINTINT(icount) PRINTNL()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						//If the guards are combatting player then the workers should be set to hate the player
						IF GuardsShouldCombat = TRUE
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed[icount]) < SafeDistance
							AND CAN_PED_SEE_HATED_PED(EnemyPed[icount], PLAYER_PED_ID())
								IF eWorkerReaction <> HATES_PLAYER
								AND eWorkerReaction <> SCARED_OF_PLAYER
									SET_WORKER_REACTION_STATE(HATES_PLAYER)
									IF doneChat19 = TRUE
										KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									ENDIF
									PRINTSTRING("SET_WORKER_REACTION_STATE(HATES_PLAYER) 7") PRINTNL()
								ENDIF
							ENDIF							
						ENDIF
						
						//If player gets into a fight then everyone should hate him
						IF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
							IF meleeTimerSet = FALSE
								imeleeTimer = GET_GAME_TIMER()
								meleeTimerSet = TRUE
							ELSE
								IF GET_GAME_TIMER() > (imeleeTimer + 3000)
									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed[icount]) < SafeDistance
									AND CAN_PED_SEE_HATED_PED(EnemyPed[icount], PLAYER_PED_ID())
										IF eWorkerReaction <> HATES_PLAYER
										AND eWorkerReaction <> SCARED_OF_PLAYER
											SET_WORKER_REACTION_STATE(HATES_PLAYER)
											IF doneChat19 = TRUE
												KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											ENDIF
											PRINTSTRING("SET_WORKER_REACTION_STATE(HATES_PLAYER) 2") PRINTNL()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF meleeTimerSet = TRUE
								meleeTimerSet = FALSE
							ENDIF
						ENDIF
						//If the player is getting into the truck without a weapon set everyone to hate him
						IF DOES_ENTITY_EXIST(DrillerTruck)
							IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DrillerTruck, TRUE)
									IF CAN_PED_SEE_HATED_PED(EnemyPed[icount], PLAYER_PED_ID())
										IF eWorkerReaction <> HATES_PLAYER
										AND eWorkerReaction <> SCARED_OF_PLAYER
											PlayerIsStealingTruck = TRUE
											SET_WORKER_REACTION_STATE(HATES_PLAYER)
											IF doneChat19 = TRUE
												KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											ENDIF
											PRINTSTRING("SET_WORKER_REACTION_STATE(HATES_PLAYER) 3") PRINTNL()
											PRINTSTRING("enemyped ") PRINTINT(icount) PRINTSTRING("can see player") PRINTNL()
											PRINTSTRING("Setting reaction state to hate the player as the player is inside the drillerTruck ") PRINTNL()
										ENDIF
									ENDIF
								ENDIF
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), DrillerTruck) < 5
								AND IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
									IF CAN_PED_SEE_HATED_PED(EnemyPed[icount], PLAYER_PED_ID())
										IF eWorkerReaction <> HATES_PLAYER
										AND eWorkerReaction <> SCARED_OF_PLAYER
											PlayerIsStealingTruck = TRUE
											SET_WORKER_REACTION_STATE(HATES_PLAYER)
											IF doneChat19 = TRUE
												KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											ENDIF
											PRINTSTRING("SET_WORKER_REACTION_STATE(HATES_PLAYER) 4") PRINTNL()
											PRINTSTRING("enemyped ") PRINTINT(icount) PRINTSTRING("can see player") PRINTNL()
											PRINTSTRING("Setting reaction state to hate the player as the player is getting into the drillertruck ") PRINTNL()
										ENDIF
									ENDIF
									//Set peds to hate player if he gets in the passenger side of the truck while ped 0 is at his scenario point.
									IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<917.343628,-1554.419800,29.266108>>, <<916.877380,-1556.440674,33.006607>>, 2.250000)
										IF DOES_ENTITY_EXIST(EnemyPed[0])
											IF NOT IS_PED_INJURED(EnemyPed[0])
												IF IS_ENTITY_AT_COORD(EnemyPed[0], <<912.3, -1542.6, 30>>, <<3,3,3>>)
													IF eWorkerReaction <> HATES_PLAYER
													AND eWorkerReaction <> SCARED_OF_PLAYER
														PlayerIsStealingTruck = TRUE
														SET_WORKER_REACTION_STATE(HATES_PLAYER)
														IF doneChat19 = TRUE
															KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
														ENDIF
														PRINTSTRING("SET_WORKER_REACTION_STATE(HATES_PLAYER) 5") PRINTNL()
														PRINTSTRING("enemyped 0") PRINTSTRING("can see player") PRINTNL()
														PRINTSTRING("Setting reaction state to hate the player as the player is getting into the drillertrucks passenger door and has been seen by ped 0 ") PRINTNL()
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						//Check for player being spotted behind the truck by worker 6 as he walks round
						IF DOES_ENTITY_EXIST(EnemyPed[6])
							IF NOT IS_PED_INJURED(EnemyPed[6])
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed[6]) < SafeDistance
									IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<928.716309,-1546.671021,27.346033>>, <<921.897095,-1549.751709,34.047241>>, 6.250000)
									OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<931.867981,-1545.185059,27.365303>>, <<925.885681,-1547.303101,34.072052>>, 3.500000)
									OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<929.490845,-1524.828613,32.840981>>, <<929.369629,-1545.288452,37.342575>>, 5.000000)
										//Check players height, if above 31 he will be on a roof so should be instant guard alert and hate
										IF CAN_PED_SEE_HATED_PED(EnemyPed[6], PLAYER_PED_ID())	
											IF vplayerCoords.z > 31
												IF eWorkerReaction <> HATES_PLAYER
												AND eWorkerReaction <> SCARED_OF_PLAYER
													playerSpottedOnRoof = TRUE
													SET_WORKER_REACTION_STATE(HATES_PLAYER)
													IF doneChat19 = TRUE
														KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
													ENDIF
													PRINTSTRING("SET_WORKER_REACTION_STATE(HATES_PLAYER) 6") PRINTNL()
													PRINTSTRING("enemyped 6") PRINTSTRING("can see player") PRINTNL()
												ENDIF											
											ELSE
												//If on the ground, should just be normal approach
												IF eWorkerReaction <> CURIOUS_OF_PLAYER
												AND eWorkerReaction <> DISLIKES_PLAYER
												AND eWorkerReaction <> HATES_PLAYER
												AND eWorkerReaction <> SCARED_OF_PLAYER
													SET_WORKER_REACTION_STATE(CURIOUS_OF_PLAYER)
													IF doneChat19 = TRUE
														KILL_ANY_CONVERSATION()
													ENDIF
													PRINTSTRING("SET_WORKER_REACTION_STATE(CURIOUS_OF_PLAYER) 2") PRINTNL()
													PRINTSTRING("SET_WORKER_REACTION_STATE to CURIOS OF PLAYER as ped 6 can see him") PRINTNL()
												ENDIF												
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						//Allow 4 seconds for the player to unequip their weapon before the workers will react to it unless they already are set to hate him then they should react to it straight away
						IF weaponTimerStarted = FALSE
							iWeaponTimer = GET_GAME_TIMER()
							weaponTimerStarted = TRUE
						ENDIF
						IF weaponTimerStarted = TRUE
							IF GET_GAME_TIMER() > (iWeaponTimer + 4000)
							OR eWorkerReaction = HATES_PLAYER
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed[icount]) < SafeDistance
									IF CAN_PED_SEE_HATED_PED(EnemyPed[icount], PLAYER_PED_ID())
										IF eWorkerReaction <> SCARED_OF_PLAYER
											SET_WORKER_REACTION_STATE(SCARED_OF_PLAYER)
											IF doneChat19 = TRUE
												KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
											ENDIF
											PRINTSTRING("SET_WORKER_REACTION_STATE(SCARED_OF_PLAYER) 2") PRINTNL()
											PRINTSTRING("EnemyPed ") PRINTINT(icount ) PRINTSTRING("can see player") PRINTNL()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						//Set the workers to scared if the player starts shooting
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed[icount]) < 40
							IF IS_PED_SHOOTING(PLAYER_PED_ID())
								IF CAN_PED_SEE_HATED_PED(EnemyPed[icount], PLAYER_PED_ID())
								OR CAN_PED_HEAR_PLAYER(player_id(), EnemyPed[icount])
									IF eWorkerReaction <> SCARED_OF_PLAYER
										SET_WORKER_REACTION_STATE(SCARED_OF_PLAYER)
										IF doneChat19 = TRUE
											KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
										ENDIF
										PRINTSTRING("SET_WORKER_REACTION_STATE(SCARED_OF_PLAYER) 3") PRINTNL()
										PRINTSTRING("Setting reaction state to scared because player is shooting") PRINTNL()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
ENDPROC

//Updates the iYardGuardMood depending on the players actions and whereabouts.
PROC UPDATE_YARD_GUARD_MOOD()
	
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			vplayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(YardGuard[0])
		IF NOT IS_PED_INJURED(YardGuard[0])
			vYardGuardCoords[0] = GET_ENTITY_COORDS(YardGuard[0])
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(YardGuard[1])
		IF NOT IS_PED_INJURED(YardGuard[1])
			vYardGuardCoords[1] = GET_ENTITY_COORDS(YardGuard[1])	
		ENDIF
	ENDIF
	
	//Set flags for guards deathchecks
	IF DOES_ENTITY_EXIST(YardGuard[0])
		IF NOT IS_PED_INJURED(YardGuard[0])
			yardguardIsAlive[0] = TRUE
		ELSE
			yardguardIsAlive[0] = FALSE
		ENDIF
	ELSE
		yardguardIsAlive[0] = FALSE
	ENDIF
	IF DOES_ENTITY_EXIST(YardGuard[1])
		IF NOT IS_PED_INJURED(YardGuard[1])
			yardguardIsAlive[1] = TRUE
		ELSE
			yardguardIsAlive[1] = FALSE
		ENDIF
	ELSE
		yardguardIsAlive[1] = FALSE
	ENDIF
	
	//Everycase that can set the mood to NOT_BOTHERED_BY_PLAYER 
	//Only way it can come back to this stage is if the guards are only on warn player and the player backs off.
	IF eYardGuardMood <> NOT_BOTHERED_BY_PLAYER
		IF eYardGuardMood = WARN_PLAYER
		
			//Check which guard the player is closer too.
			IF yardguardIsAlive[0] = TRUE
				IF yardguardIsAlive[1] = TRUE
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1])
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < SafeDistance
							IF vplayerCoords.x < vYardGuardCoords[0].x
							AND NOT IS_PED_FACING_PED(PLAYER_PED_ID(), YardGuard[0], 90)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<868.561707,-1579.476440,33.304478>>, <<866.563538,-1560.984375,28.904823>>, 8.250000)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<855.459290,-1548.228394,28.277218>>, <<863.176453,-1563.287842,33.313747>>, 4.750000)
								PRINTSTRING("eYardGuardMood = NOT_BOTHERED_BY_PLAYER [1]") PRINTNL()
								eYardGuardMood = NOT_BOTHERED_BY_PLAYER
								TaskToGetBackToGuardCoord[0] = FALSE
								StandGuardTaskGiven[0] = FALSE
								TaskToGetBackToGuardCoord[1] = FALSE
								StandGuardTaskGiven[1] = FALSE
								iScenarioExistsTimer[0] = GET_GAME_TIMER()
								iScenarioExistsTimer[1] = GET_GAME_TIMER()
							ENDIF
						ELSE
							IF vplayerCoords.x < vYardGuardCoords[0].x	
								PRINTSTRING("eYardGuardMood = NOT_BOTHERED_BY_PLAYER [2]") PRINTNL()
								eYardGuardMood = NOT_BOTHERED_BY_PLAYER
								TaskToGetBackToGuardCoord[0] = FALSE
								StandGuardTaskGiven[0] = FALSE
								TaskToGetBackToGuardCoord[1] = FALSE
								StandGuardTaskGiven[1] = FALSE
								iScenarioExistsTimer[0] = GET_GAME_TIMER()
								iScenarioExistsTimer[1] = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ELSE
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1]) < 15
							IF vplayerCoords.x > vYardGuardCoords[1].x
							AND NOT IS_PED_FACING_PED(PLAYER_PED_ID(), YardGuard[1], 90)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<936.784912,-1569.337402,31.496609>>, <<936.355286,-1581.973389,27.778118>>, 8.250000)
								PRINTSTRING("eYardGuardMood = NOT_BOTHERED_BY_PLAYER [3]") PRINTNL()
								eYardGuardMood = NOT_BOTHERED_BY_PLAYER
								TaskToGetBackToGuardCoord[0] = FALSE
								StandGuardTaskGiven[0] = FALSE
								TaskToGetBackToGuardCoord[1] = FALSE
								StandGuardTaskGiven[1] = FALSE
								iScenarioExistsTimer[0] = GET_GAME_TIMER()
								iScenarioExistsTimer[1] = GET_GAME_TIMER()
							ENDIF
						ELSE
							IF vplayerCoords.x > vYardGuardCoords[1].x	
								PRINTSTRING("eYardGuardMood = NOT_BOTHERED_BY_PLAYER [4]") PRINTNL()
								eYardGuardMood = NOT_BOTHERED_BY_PLAYER
								TaskToGetBackToGuardCoord[0] = FALSE
								StandGuardTaskGiven[0] = FALSE
								TaskToGetBackToGuardCoord[1] = FALSE
								StandGuardTaskGiven[1] = FALSE
								iScenarioExistsTimer[0] = GET_GAME_TIMER()
								iScenarioExistsTimer[1] = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1])
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < 15
							IF vplayerCoords.x < vYardGuardCoords[0].x
							AND NOT IS_PED_FACING_PED(PLAYER_PED_ID(), YardGuard[0], 90)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<868.561707,-1579.476440,33.304478>>, <<866.563538,-1560.984375,28.904823>>, 8.250000)
								PRINTSTRING("eYardGuardMood = NOT_BOTHERED_BY_PLAYER [5]") PRINTNL()
								eYardGuardMood = NOT_BOTHERED_BY_PLAYER
								TaskToGetBackToGuardCoord[0] = FALSE
								StandGuardTaskGiven[0] = FALSE
								TaskToGetBackToGuardCoord[1] = FALSE
								StandGuardTaskGiven[1] = FALSE
								iScenarioExistsTimer[0] = GET_GAME_TIMER()
								iScenarioExistsTimer[1] = GET_GAME_TIMER()
							ENDIF
						ELSE
							IF vplayerCoords.x < vYardGuardCoords[0].x	
								PRINTSTRING("eYardGuardMood = NOT_BOTHERED_BY_PLAYER [6]") PRINTNL()
								eYardGuardMood = NOT_BOTHERED_BY_PLAYER
								TaskToGetBackToGuardCoord[0] = FALSE
								StandGuardTaskGiven[0] = FALSE
								TaskToGetBackToGuardCoord[1] = FALSE
								StandGuardTaskGiven[1] = FALSE
								iScenarioExistsTimer[0] = GET_GAME_TIMER()
								iScenarioExistsTimer[1] = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF yardguardIsAlive[1] = TRUE
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1]) < 15
						IF vplayerCoords.x > vYardGuardCoords[1].x
						AND NOT IS_PED_FACING_PED(PLAYER_PED_ID(), YardGuard[1], 90)
						AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<936.784912,-1569.337402,31.496609>>, <<936.355286,-1581.973389,27.778118>>, 8.250000)
							PRINTSTRING("eYardGuardMood = NOT_BOTHERED_BY_PLAYER [7]") PRINTNL()
							eYardGuardMood = NOT_BOTHERED_BY_PLAYER
							TaskToGetBackToGuardCoord[0] = FALSE
							StandGuardTaskGiven[0] = FALSE
							TaskToGetBackToGuardCoord[1] = FALSE
							StandGuardTaskGiven[1] = FALSE
							iScenarioExistsTimer[0] = GET_GAME_TIMER()
							iScenarioExistsTimer[1] = GET_GAME_TIMER()
						ENDIF
					ELSE
						IF vplayerCoords.x > vYardGuardCoords[1].x	
							PRINTSTRING("eYardGuardMood = NOT_BOTHERED_BY_PLAYER [8]") PRINTNL()
							eYardGuardMood = NOT_BOTHERED_BY_PLAYER
							TaskToGetBackToGuardCoord[0] = FALSE
							StandGuardTaskGiven[0] = FALSE
							TaskToGetBackToGuardCoord[1] = FALSE
							StandGuardTaskGiven[1] = FALSE
							iScenarioExistsTimer[0] = GET_GAME_TIMER()
							iScenarioExistsTimer[1] = GET_GAME_TIMER()
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
			
		ENDIF
		
		//Chill everyone out if player leaves area before timer runs out.
		IF eYardGuardMood = HATE_PLAYER
			IF GuardsShouldCombat = FALSE
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<936.784912,-1569.337402,31.496609>>, <<936.355286,-1581.973389,27.778118>>, 8.250000)
				OR vplayerCoords.x > 942
					IF NOT IS_PED_INJURED(YardGuard[1])
						IF vplayerCoords.x > vYardGuardCoords[1].x
							IF NOT IS_PED_FACING_PED(PLAYER_PED_ID(), YardGuard[1], 90)
								PRINTSTRING("eYardGuardMood = NOT_BOTHERED_BY_PLAYER [9]") PRINTNL()
								eYardGuardMood = NOT_BOTHERED_BY_PLAYER
								TaskToGetBackToGuardCoord[0] = FALSE
								StandGuardTaskGiven[0] = FALSE
								TaskToGetBackToGuardCoord[1] = FALSE
								StandGuardTaskGiven[1] = FALSE
								iScenarioExistsTimer[0] = GET_GAME_TIMER()
								iScenarioExistsTimer[1] = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ELSE
						IF vplayerCoords.x > 942
							IF NOT IS_PED_FACING_PED(PLAYER_PED_ID(), YardGuard[1], 90)
								PRINTSTRING("eYardGuardMood = NOT_BOTHERED_BY_PLAYER [12]") PRINTNL()
								eYardGuardMood = NOT_BOTHERED_BY_PLAYER
								TaskToGetBackToGuardCoord[0] = FALSE
								StandGuardTaskGiven[0] = FALSE
								TaskToGetBackToGuardCoord[1] = FALSE
								StandGuardTaskGiven[1] = FALSE
								iScenarioExistsTimer[0] = GET_GAME_TIMER()
								iScenarioExistsTimer[1] = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<868.561707,-1579.476440,33.304478>>, <<866.563538,-1560.984375,28.904823>>, 8.250000)
				OR vplayerCoords.x < 860 
					IF NOT IS_PED_INJURED(YardGuard[0])
						IF vplayerCoords.x < vYardGuardCoords[0].x
							IF NOT IS_PED_FACING_PED(PLAYER_PED_ID(), YardGuard[0], 90)
								PRINTSTRING("eYardGuardMood = NOT_BOTHERED_BY_PLAYER [10]") PRINTNL()
								eYardGuardMood = NOT_BOTHERED_BY_PLAYER
								TaskToGetBackToGuardCoord[0] = FALSE
								StandGuardTaskGiven[0] = FALSE
								TaskToGetBackToGuardCoord[1] = FALSE
								StandGuardTaskGiven[1] = FALSE
								iScenarioExistsTimer[0] = GET_GAME_TIMER()
								iScenarioExistsTimer[1] = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ELSE
						IF vplayerCoords.x < 860 
							IF NOT IS_PED_FACING_PED(PLAYER_PED_ID(), YardGuard[0], 90)
								PRINTSTRING("eYardGuardMood = NOT_BOTHERED_BY_PLAYER [11]") PRINTNL()
								eYardGuardMood = NOT_BOTHERED_BY_PLAYER
								TaskToGetBackToGuardCoord[0] = FALSE
								StandGuardTaskGiven[0] = FALSE
								TaskToGetBackToGuardCoord[1] = FALSE
								StandGuardTaskGiven[1] = FALSE
								iScenarioExistsTimer[0] = GET_GAME_TIMER()
								iScenarioExistsTimer[1] = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF

	//Run checks for putting the guards mood to WARN_PLAYER
	IF eYardGuardMood <> WARN_PLAYER
		//Should only be able to go to warn player if they have not been at Hate_player
		IF eYardGuardMood <> HATE_PLAYER
			IF yardguardIsAlive[0] = TRUE
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < SafeDistance
					IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_UNARMED
					OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					OR IS_PHONE_ONSCREEN()
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<860.460571,-1563.580444,28.817883>>, <<865.080688,-1575.088379,30.933239>>, 5.750000)
							IF vplayerCoords.x > vYardGuardCoords[0].x
							OR IS_PED_FACING_PED(PLAYER_PED_ID(), YardGuard[0], 90)
								PRINTSTRING("eYardGuardMood = WARN_PLAYER [1]") PRINTNL()
								eYardGuardMood = WARN_PLAYER
							ENDIF
						ENDIF
						//Check for player climbing over wall next to guard
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<855.459290,-1548.228394,28.277218>>, <<863.176453,-1563.287842,33.313747>>, 4.750000)
							PRINTSTRING("eYardGuardMood = WARN_PLAYER [3]") PRINTNL()
							eYardGuardMood = WARN_PLAYER
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF yardguardIsAlive[1] = TRUE
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1]) < 15
					IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_UNARMED
					OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					OR IS_PHONE_ONSCREEN()
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<939.886169,-1575.635376,28.597832>>, <<952.925415,-1575.483521,33.194588>>, 13.750000)
							IF vplayerCoords.x < vYardGuardCoords[1].x
							OR IS_PED_FACING_PED(PLAYER_PED_ID(), YardGuard[1], 90)							
								PRINTSTRING("eYardGuardMood = WARN_PLAYER [2]") PRINTNL()
								eYardGuardMood = WARN_PLAYER
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
		ENDIF
		
	ENDIF
	
	//Run checks for putting the guards mood to HATE_PLAYER
	IF eYardGuardMood <> HATE_PLAYER
		
		IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, <<892.9, -1552.4, 30>>, 40)
			PRINTSTRING("eYardGuardMood = HATE_PLAYER [14]") PRINTNL()
			eYardGuardMood = HATE_PLAYER
			GuardsShouldCombat = TRUE
		ENDIF
		
		IF deadBodyFound = TRUE
			PRINTSTRING("eYardGuardMood = HATE_PLAYER [13]") PRINTNL()
			eYardGuardMood = HATE_PLAYER
			GuardsShouldCombat = TRUE		
		ENDIF
		
		//Set the guards to hate the player if he has a wanted level 
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <> 0
			IF DOES_ENTITY_EXIST(YardGuard[0])
				IF NOT IS_PED_INJURED(YardGuard[0])
					IF CAN_PED_SEE_HATED_PED(YardGuard[0], PLAYER_PED_ID())
						PRINTSTRING("eYardGuardMood = HATE_PLAYER [11]") PRINTNL()
						eYardGuardMood = HATE_PLAYER
						GuardsShouldCombat = TRUE
					ENDIF
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(YardGuard[1])
				IF NOT IS_PED_INJURED(YardGuard[1])
					IF CAN_PED_SEE_HATED_PED(YardGuard[1], PLAYER_PED_ID())
						PRINTSTRING("eYardGuardMood = HATE_PLAYER [12]") PRINTNL()
						eYardGuardMood = HATE_PLAYER
						GuardsShouldCombat = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//If chat 24 is being played worker mood has been set to dislike and guards have been shouted for assistance
		IF GuardsAlerted = FALSE
			IF doneChat24 = TRUE
				IF GET_GAME_TIMER() > (ichat24timer + 4000)
					FOR icountChat24 = 0 TO 6
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF DOES_ENTITY_EXIST(enemyped[icountChat24])
								IF NOT IS_PED_INJURED(EnemyPed[icountChat24])
									IF IS_PED_IN_CURRENT_CONVERSATION(EnemyPed[icountChat24])
										PRINTSTRING("eYardGuardMood = HATE_PLAYER [10]") PRINTNL()
										eYardGuardMood = HATE_PLAYER
										GuardsAlerted = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		ENDIF
		
		//If the workers are scared or hate the player then the guards should be too.
		IF eWorkerReaction = SCARED_OF_PLAYER
		OR eWorkerReaction = HATES_PLAYER
		OR doneChat24 = TRUE
			PRINTSTRING("eYardGuardMood = HATE_PLAYER [1]") PRINTNL()
			eYardGuardMood = HATE_PLAYER
			WorkerSpottedPlayerFirst = TRUE
			GuardsShouldCombat = TRUE
		ENDIF
		
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF yardguardIsAlive[0] = TRUE
				IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
				AND NOT IS_PHONE_ONSCREEN()
					IF CAN_PED_SEE_HATED_PED(YardGuard[0], PLAYER_PED_ID())
						PRINTSTRING("eYardGuardMood = HATE_PLAYER [2]") PRINTNL()
						eYardGuardMood = HATE_PLAYER
					ENDIF
				ENDIF
			ENDIF
			IF yardguardIsAlive[1] = TRUE
				IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
				AND NOT IS_PHONE_ONSCREEN()
					IF CAN_PED_SEE_HATED_PED(YardGuard[1], PLAYER_PED_ID())
						PRINTSTRING("eYardGuardMood = HATE_PLAYER [3]") PRINTNL()
						eYardGuardMood = HATE_PLAYER
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_DISTANCE_BETWEEN_COORDS(vplayerCoords, vMiddleOfYard) < 36
			IF yardguardIsAlive[0] = TRUE
				IF CAN_PED_SEE_HATED_PED(YardGuard[0], PLAYER_PED_ID())
					PRINTSTRING("eYardGuardMood = HATE_PLAYER [4]") PRINTNL()
					eYardGuardMood = HATE_PLAYER
				ENDIF
				IF GET_DISTANCE_BETWEEN_ENTITIES(YardGuard[0], PLAYER_PED_ID()) < SafeDistance
					IF CAN_PED_HEAR_PLAYER(PLAYER_ID(), YardGuard[0])
						PRINTSTRING("eYardGuardMood = HATE_PLAYER [9]") PRINTNL()
						eYardGuardMood = HATE_PLAYER
					ENDIF
				ENDIF
			ENDIF
			IF yardguardIsAlive[1] = TRUE
				IF vplayerCoords.z < 36
				OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF CAN_PED_SEE_HATED_PED(YardGuard[1], PLAYER_PED_ID())
						PRINTSTRING("eYardGuardMood = HATE_PLAYER [8]") PRINTNL()
						eYardGuardMood = HATE_PLAYER
					ENDIF
					IF GET_DISTANCE_BETWEEN_ENTITIES(YardGuard[1], PLAYER_PED_ID()) < SafeDistance
						IF CAN_PED_HEAR_PLAYER(PLAYER_ID(), YardGuard[1])
							PRINTSTRING("eYardGuardMood = HATE_PLAYER [5]") PRINTNL()
							eYardGuardMood = HATE_PLAYER
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		//Check if the player just goes straight passed the guards
		IF yardguardIsAlive[0] = TRUE
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < SafeDistance
				IF vplayerCoords.x > vYardGuardCoords[0].x
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<868.561707,-1579.476440,33.304478>>, <<866.563538,-1560.984375,28.904823>>, 8.250000)
					IF CAN_PED_SEE_HATED_PED(YardGuard[0], PLAYER_PED_ID())
					OR CAN_PED_HEAR_PLAYER(PLAYER_ID(), YardGuard[0])
						PRINTSTRING("eYardGuardMood = HATE_PLAYER [6]") PRINTNL()
						eYardGuardMood = HATE_PLAYER
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF yardguardIsAlive[1] = TRUE
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1]) < SafeDistance
				IF vplayerCoords.x < vYardGuardCoords[1].x
				AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<936.784912,-1569.337402,31.496609>>, <<936.355286,-1581.973389,27.778118>>, 8.250000)
					IF CAN_PED_SEE_HATED_PED(YardGuard[1], PLAYER_PED_ID())
					OR CAN_PED_HEAR_PLAYER(PLAYER_ID(), YardGuard[1])					
						PRINTSTRING("eYardGuardMood = HATE_PLAYER [7]") PRINTNL()
						eYardGuardMood = HATE_PLAYER
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	
	ENDIF
	
ENDPROC

//PURPOSE: Resets all task flags. Should be called everytime the peds tasks are cleared by script to ensure he will never be left with no tasks.
PROC RESET_ALL_TASK_FLAGS(PED_INDEX pedToBeReset)
	
	IF pedToBeReset = YardGuard[0]
		TaskLookAtGiven[0] = FALSE
		TaskToGetBackToGuardCoord[0] = FALSE
		StandGuardTaskGiven[0] = FALSE
		turnPedTask[0] = FALSE
		combatTaskGiven[0] = FALSE
		followNavForCombat[0] = FALSE
		TaskGoToEntity[0] = FALSE
		TaskNavmeshToPed[0] = FALSE
		GuardFacingPlayer[0] = FALSE
	ENDIF

	IF pedToBeReset = YardGuard[1]
		TaskLookAtGiven[1] = FALSE
		TaskToGetBackToGuardCoord[1] = FALSE
		StandGuardTaskGiven[1] = FALSE
		turnPedTask[1] = FALSE
		combatTaskGiven[1] = FALSE
		followNavForCombat[1] = FALSE
		TaskGoToEntity[1] = FALSE
		TaskNavmeshToPed[1] = FALSE
		GuardFacingPlayer[1] = FALSE
	ENDIF

ENDPROC
//Purpose: Returns the closest Security guard
FUNC PED_INDEX GET_NEAREST_SECURITY_GAURD()
	
	IF DOES_ENTITY_EXIST(YardGuard[0])
		IF NOT IS_PED_INJURED(YardGuard[0])
			IF DOES_ENTITY_EXIST(YardGuard[1])
				IF NOT IS_PED_INJURED(YardGuard[1])
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1])
						RETURN YardGuard[0]
					ELSE
						RETURN YardGuard[1]
					ENDIF
				ELSE
					RETURN YardGuard[0]
				ENDIF
			ELSE
				RETURN YardGuard[0]
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(YardGuard[1])
				IF NOT IS_PED_INJURED(YardGuard[1])
					RETURN YardGuard[1]
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(YardGuard[1])
			IF NOT IS_PED_INJURED(YardGuard[1])
				RETURN YardGuard[1]
			ENDIF
		ENDIF
	ENDIF
	
	RETURN NULL

ENDFUNC

//PURPOSE: Controls the guards around the yard
PROC MONITOR_YARD_GUARDS()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID())	
		AND IS_PED_SHOOTING(PLAYER_PED_ID())
			SET_WANTED_LEVEL_MULTIPLIER(0)
		ELSE
			SET_WANTED_LEVEL_MULTIPLIER(1)
		ENDIF
	ENDIF
			
	UPDATE_YARD_GUARD_MOOD()		
			
	IF yardguardIsAlive[0]
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), yardguard[0]) < SafeDistance
		AND CAN_PED_SEE_HATED_PED(yardguard[0], PLAYER_PED_ID())
			IF TaskLookAtGiven[0] = FALSE
				TASK_LOOK_AT_ENTITY(yardguard[0], PLAYER_PED_ID(), -1, SLF_USE_TORSO|SLF_WIDEST_PITCH_LIMIT|SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_HIGH)
				TaskLookAtGiven[0] = TRUE
			ENDIF
			IF TaskLookAtGiven[0] = TRUE
				IF GET_SCRIPT_TASK_STATUS(yardguard[0], SCRIPT_TASK_LOOK_AT_ENTITY) <> WAITING_TO_START_TASK
				OR GET_SCRIPT_TASK_STATUS(yardguard[0], SCRIPT_TASK_LOOK_AT_ENTITY) <> PERFORMING_TASK
					TaskLookAtGiven[0] = FALSE
				ENDIF
			ENDIF
		ELSE
			IF TaskLookAtGiven[0] = TRUE
				TASK_LOOK_AT_ENTITY(yardguard[0], PLAYER_PED_ID(), 1)
				TaskLookAtGiven[0] = FALSE
			ENDIF
		ENDIF
	ENDIF	
	IF yardguardIsAlive[1]
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), yardguard[1]) < SafeDistance
		AND CAN_PED_SEE_HATED_PED(yardguard[1], PLAYER_PED_ID())
			IF TaskLookAtGiven[1] = FALSE
				TASK_LOOK_AT_ENTITY(yardguard[1], PLAYER_PED_ID(), -1, SLF_USE_TORSO|SLF_WIDEST_PITCH_LIMIT|SLF_WIDEST_YAW_LIMIT, SLF_LOOKAT_HIGH)
				TaskLookAtGiven[1] = TRUE
			ENDIF
			IF TaskLookAtGiven[1] = TRUE
				IF GET_SCRIPT_TASK_STATUS(yardguard[1], SCRIPT_TASK_LOOK_AT_ENTITY) <> WAITING_TO_START_TASK
				OR GET_SCRIPT_TASK_STATUS(yardguard[1], SCRIPT_TASK_LOOK_AT_ENTITY) <> PERFORMING_TASK
					TaskLookAtGiven[1] = FALSE
				ENDIF
			ENDIF
		ELSE
			IF TaskLookAtGiven[1] = TRUE
				TASK_LOOK_AT_ENTITY(yardguard[1], PLAYER_PED_ID(), 1)
				TaskLookAtGiven[1] = FALSE
			ENDIF
		ENDIF
	ENDIF
			
	//Check who is closest for dialogue
	IF yardguardIsAlive[0]
	AND yardguardIsAlive[1]
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1])
			YardGuard0Closest = TRUE
			YardGuard1Closest = FALSE
		ELSE
			YardGuard0Closest = FALSE
			YardGuard1Closest = TRUE
		ENDIF		
	ELSE
		IF yardguardIsAlive[0] 
		AND NOT yardguardIsAlive[1]
			YardGuard0Closest = TRUE
			YardGuard1Closest = FALSE
		ENDIF
		IF yardguardIsAlive[1] 
		AND NOT yardguardIsAlive[0]
			YardGuard0Closest = FALSE
			YardGuard1Closest = TRUE
		ENDIF
	ENDIF
			
	SWITCH eYardGuardMood
		
		CASE NOT_BOTHERED_BY_PLAYER
//			PRINTSTRING("eYardGuardMood = NOT_BOTHERED_BY_PLAYER") PRINTNL()			
			
			//Put the guards back to their posts to stand guard
			IF DOES_SCENARIO_GROUP_EXIST("SCRAP_SECURITY") 
				IF NOT IS_SCENARIO_GROUP_ENABLED("SCRAP_SECURITY") 
					SET_SCENARIO_GROUP_ENABLED("SCRAP_SECURITY",TRUE)
				ENDIF
				IF IS_SCENARIO_GROUP_ENABLED("SCRAP_SECURITY") 
					IF DOES_ENTITY_EXIST(YardGuard[0])
						IF NOT IS_PED_INJURED(YardGuard[0])
							IF NOT IS_PED_BEING_STEALTH_KILLED(YardGuard[0])
								IF DOES_SCENARIO_EXIST_IN_AREA(<<863.1551, -1564.5721, 29.3231>>, 5, TRUE)
									iScenarioExistsTimer[0] = GET_GAME_TIMER()
									IF NOT IS_PED_ACTIVE_IN_SCENARIO(YardGuard[0])
										IF StandGuardTaskGiven[0] = FALSE
											TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(YardGuard[0], <<863.1551, -1564.5721, 29.3231>>, 5, 0)
											PRINTSTRING("TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(YardGuard[0], <<863.1551, -1564.5721, 29.3231>>, 5, 0)") PRINTNL()
											TaskToGetBackToGuardCoord[0] = FALSE
											StandGuardTaskGiven[0] = TRUE
										ENDIF
									ENDIF
								ELSE
									IF GET_GAME_TIMER() > (iScenarioExistsTimer[0] + 2000)
										IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(YardGuard[0]), <<863.1551, -1564.5721, 29.3231>>) > 3
											IF TaskToGetBackToGuardCoord[0] = FALSE
												TASK_FOLLOW_NAV_MESH_TO_COORD(YardGuard[0], <<863.1551, -1564.5721, 29.3231>>, PEDMOVEBLENDRATIO_WALK, -1)
												PRINTSTRING("YardGuard[0], <<863.1551, -1564.5721, 29.3231>>, PEDMOVEBLENDRATIO_WALK, -1") PRINTNL()
												StandGuardTaskGiven[0] = FALSE
												TaskToGetBackToGuardCoord[0] = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(YardGuard[1])
						IF NOT IS_PED_INJURED(YardGuard[1])	
							IF NOT IS_PED_BEING_STEALTH_KILLED(YardGuard[1])
//								IF DOES_SCENARIO_EXIST_IN_AREA(<<940.2881, -1573.8774, 29.3866>>, 5, TRUE)
//									IF GET_SCRIPT_TASK_STATUS(YardGuard[1], SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS) <> PERFORMING_TASK
//										TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(YardGuard[1], <<940.2881, -1573.8774, 29.3866>>, 5, 0)
//										PRINTSTRING("TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(YardGuard[1], <<940.2881, -1573.8774, 29.3866>>, 5, 0)") PRINTNL()
//									ENDIF
//								ELSE
//									IF GET_SCRIPT_TASK_STATUS(YardGuard[1], SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK
//									OR GET_SCRIPT_TASK_STATUS(YardGuard[1], SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> WAITING_TO_START_TASK
//										TASK_FOLLOW_NAV_MESH_TO_COORD(YardGuard[1], <<940.2881, -1573.8774, 29.3866>>, PEDMOVEBLENDRATIO_WALK, -1)
//										PRINTSTRING("YardGuard[1], <<940.2881, -1573.8774, 29.3866>>, PEDMOVEBLENDRATIO_WALK, -1") PRINTNL()
//									ENDIF
//								ENDIF
								IF DOES_SCENARIO_EXIST_IN_AREA(<<940.2881, -1573.8774, 29.3866>>, 5, TRUE)
									iScenarioExistsTimer[1] = GET_GAME_TIMER()
									IF NOT IS_PED_ACTIVE_IN_SCENARIO(YardGuard[1])
										IF StandGuardTaskGiven[1] = FALSE
											TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(YardGuard[1], <<940.2881, -1573.8774, 29.3866>>, 5, 0)
											PRINTSTRING("TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(YardGuard[1], <<863.1551, -1564.5721, 29.3231>>, 5, 0)") PRINTNL()
											TaskToGetBackToGuardCoord[1] = FALSE
											StandGuardTaskGiven[1] = TRUE
										ENDIF
									ENDIF
								ELSE
									IF GET_GAME_TIMER() > (iScenarioExistsTimer[1] + 2000)
										IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(YardGuard[1]), <<940.2881, -1573.8774, 29.3866>>) > 3
											IF TaskToGetBackToGuardCoord[1] = FALSE
												TASK_FOLLOW_NAV_MESH_TO_COORD(YardGuard[1], <<940.2881, -1573.8774, 29.3866>>, PEDMOVEBLENDRATIO_WALK, -1)
												PRINTSTRING("YardGuard[1], <<940.2881, -1573.8774, 29.3866>>, PEDMOVEBLENDRATIO_WALK, -1") PRINTNL()
												StandGuardTaskGiven[1] = FALSE
												TaskToGetBackToGuardCoord[1] = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
			
		BREAK
		
		CASE WARN_PLAYER
		
//			PRINTSTRING("eYardGuardMood = WARN_PLAYER") PRINTNL()
		
			//Check for player entering the front or back gates.
			IF DOES_ENTITY_EXIST(YardGuard[0])
				IF NOT IS_PED_INJURED(YardGuard[0])				
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < SafeDistance
						IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_UNARMED
						OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						OR IS_PHONE_ONSCREEN()
							IF IS_PED_FACING_PED(PLAYER_PED_ID(), YardGuard[0], 100)
							OR CAN_PED_SEE_HATED_PED(YardGuard[0], PLAYER_PED_ID())
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<860.460571,-1563.580444,28.817883>>, <<865.080688,-1575.088379,30.933239>>, 5.750000)
								OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<855.459290,-1548.228394,28.277218>>, <<863.176453,-1563.287842,33.313747>>, 4.750000)
									IF turnPedTask[0] = FALSE
										IF NOT IS_PED_FACING_PED(YardGuard[0], PLAYER_PED_ID(), 45)
											IF NOT IS_ENTITY_ON_SCREEN(YardGuard[0])
												CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[0])
												PRINTNL()PRINTSTRING("CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[0]) 0.4")PRINTNL()
											ELSE
												CLEAR_PED_TASKS(YardGuard[0])
												PRINTNL()PRINTSTRING("CLEAR_PED_TASKS(YardGuard[0]) 0.4")PRINTNL()
											ENDIF
											PRINTSTRING("TASK_TURN_PED_TO_FACE_ENTITY for YardGuard[0] is being called") PRINTNL()
											TASK_TURN_PED_TO_FACE_ENTITY(YardGuard[0], PLAYER_PED_ID(), 5000)
											PRINTSTRING("TASK_TURN_PED_TO_FACE_ENTITY(YardGuard[0], PLAYER_PED_ID(), 5000)") PRINTNL()
											iTurnPedTaskTimer[0] = GET_GAME_TIMER()
											RESET_ALL_TASK_FLAGS(YardGuard[0])
											turnPedTask[0] = TRUE
										ENDIF
									ENDIF
									IF turnPedTask[0] = TRUE
										IF GET_GAME_TIMER() > (iTurnPedTaskTimer[0] + 5000)
											turnPedTask[0] = FALSE
										ENDIF
									ENDIF
									
									//Check if player has been warned before.
									IF playerHasBeenWarnedBefore[0] = FALSE
									AND playerHasBeenWarnedBefore[1] = FALSE
										IF doneChat13 = FALSE
//											PLAY_PED_AMBIENT_SPEECH(YardGuard[0], "PROVOKE_TRESSPASS", SPEECH_PARAMS_FORCE_NORMAL)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[0], "PROVOKE_TRESPASS", "S_M_M_GENERICSECURITY_01_LATINO_MINI_01", SPEECH_PARAMS_FORCE_NORMAL)
											PRINTSTRING("01 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
											iChat13Timer = GET_GAME_TIMER()
											doneChat13 = TRUE	
//											ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[0], "FHPBSecGuard")
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT13", CONV_PRIORITY_MEDIUM)
//												//Sorry no entry. Private grounds.
//												//Hey. You can't come in here.
//												//Move along please, no entry.
//												iChat13Timer = GET_GAME_TIMER()
//												doneChat13 = TRUE
//											ENDIF
										ELSE
											IF GET_GAME_TIMER() > (iChat13Timer + 15000)
//												PLAY_PED_AMBIENT_SPEECH(YardGuard[0], "PROVOKE_TRESSPASS", SPEECH_PARAMS_FORCE_SHOUTED)
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[0], "PROVOKE_TRESPASS", "S_M_M_GENERICSECURITY_01_LATINO_MINI_01", SPEECH_PARAMS_FORCE_SHOUTED)
												PRINTSTRING("02 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
												iChat13Timer = GET_GAME_TIMER()
											ENDIF
//												ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[0], "FHPBSecGuard")
//												IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT13", CONV_PRIORITY_MEDIUM)
//													//Sorry no entry. Private grounds.
//													//Hey. You can't come in here.
//													//Move along please, no entry.
//													iChat13Timer = GET_GAME_TIMER()
//												ENDIF						
//											ENDIF
										ENDIF
									ELSE
										IF playerHasBeenWarnedBefore[0] = TRUE
											IF doneChat17 = FALSE
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[0], "PROVOKE_GEBERIC", "S_M_M_GENERICSECURITY_01_LATINO_MINI_01", SPEECH_PARAMS_FORCE_SHOUTED)
												PRINTSTRING("03 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
												iChat17Timer = GET_GAME_TIMER()
												doneChat17 = TRUE
//												ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[0], "FHPBSecGuard")
//												IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT17", CONV_PRIORITY_MEDIUM)
//													//Hey, I thought I told you to get out of here.
//													//What is your fucking problem?
//													//Just fuck off before I call the cops.
//													iChat17Timer = GET_GAME_TIMER()
//													doneChat17 = TRUE
//												ENDIF
											ELSE
												IF GET_GAME_TIMER() > (iChat17Timer + 15000)
													PRINTSTRING("04 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[0], "PROVOKE_GENERIC", "S_M_M_GENERICSECURITY_01_LATINO_MINI_01", SPEECH_PARAMS_FORCE_SHOUTED)
													iChat17Timer = GET_GAME_TIMER()
//													ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[0], "FHPBSecGuard")
//													IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT17", CONV_PRIORITY_MEDIUM)
//														//Hey, I thought I told you to get out of here.
//														//What is your fucking problem?
//														//Just fuck off before I call the cops.
//														iChat17Timer = GET_GAME_TIMER()
//													ENDIF						
												ENDIF
											ENDIF
										ENDIF
										IF playerHasBeenWarnedBefore[1] = TRUE
											IF doneChat18 = FALSE
												PRINTSTRING("05 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[0], "PROVOKE_GENERIC", "S_M_M_GENERICSECURITY_01_LATINO_MINI_01", SPEECH_PARAMS_FORCE_SHOUTED)
												iChat18Timer = GET_GAME_TIMER()
												doneChat18 = TRUE
//												ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[0], "FHPBSecGuard")
//												IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT18", CONV_PRIORITY_MEDIUM)
//													//Hey my collegue's already told you leave.
//													//Go on, fuck off. Private property.
//													//If you don't leave the police will be called.
//													iChat18Timer = GET_GAME_TIMER()
//													doneChat18 = TRUE
//												ENDIF
											ELSE
												IF GET_GAME_TIMER() > (iChat18Timer + 15000)
													PRINTSTRING("06 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[0], "PROVOKE_GENERIC", "S_M_M_GENERICSECURITY_01_LATINO_MINI_01", SPEECH_PARAMS_FORCE_SHOUTED)
													iChat18Timer = GET_GAME_TIMER()
//													ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[0], "FHPBSecGuard")
//													IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT18", CONV_PRIORITY_MEDIUM)
//														//Hey my collegue's already told you leave.
//														//Go on, fuck off. Private property.
//														//If you don't leave the police will be called.
//														iChat18Timer = GET_GAME_TIMER()
//													ENDIF						
												ENDIF
											ENDIF
										ENDIF									
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(YardGuard[1])
				IF NOT IS_PED_INJURED(YardGuard[1])
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1]) < 15
						IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_UNARMED
						OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						OR IS_PHONE_ONSCREEN()
							IF IS_PED_FACING_PED(PLAYER_PED_ID(), YardGuard[1], 100)
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<939.886169,-1575.635376,28.597832>>, <<952.925415,-1575.483521,33.194588>>, 13.750000)
									IF turnPedTask[1] = FALSE
										IF NOT IS_PED_FACING_PED(YardGuard[1], PLAYER_PED_ID(), 45)
											IF NOT IS_ENTITY_ON_SCREEN(YardGuard[1])
												CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[1])
											ELSE
												CLEAR_PED_TASKS(YardGuard[1])
											ENDIF
											PRINTSTRING("TASK_TURN_PED_TO_FACE_ENTITY for YardGuard[1] is being called") PRINTNL()
											TASK_TURN_PED_TO_FACE_ENTITY(YardGuard[1], PLAYER_PED_ID(), 5000)
											PRINTSTRING("TASK_TURN_PED_TO_FACE_ENTITY(YardGuard[1], PLAYER_PED_ID(), 5000)") PRINTNL()
											iTurnPedTaskTimer[1] = GET_GAME_TIMER()
											RESET_ALL_TASK_FLAGS(YardGuard[1])
											turnPedTask[1] = TRUE
										ENDIF
									ENDIF
									IF turnPedTask[1] = TRUE
										IF GET_GAME_TIMER() > (iTurnPedTaskTimer[1] + 5000)
											turnPedTask[1] = FALSE
										ENDIF
									ENDIF
									//Check if player has been warned before.
									IF playerHasBeenWarnedBefore[0] = FALSE
									AND playerHasBeenWarnedBefore[1] = FALSE
										IF doneChat13 = FALSE
//											PLAY_PED_AMBIENT_SPEECH(YardGuard[1], "PROVOKE_TRESSPASS", SPEECH_PARAMS_FORCE_NORMAL)
											PRINTSTRING("07 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[1], "PROVOKE_TRESPASS", "S_M_M_GENERICSECURITY_01_LATINO_MINI_02", SPEECH_PARAMS_FORCE_NORMAL)
											iChat13Timer = GET_GAME_TIMER()
											doneChat13 = TRUE	
//											ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[1], "FHPBSecGuard")
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT13", CONV_PRIORITY_MEDIUM)
//												//Sorry no entry. Private grounds.
//												//Hey. You can't come in here.
//												//Move along please, no entry.
//												iChat13Timer = GET_GAME_TIMER()
//												doneChat13 = TRUE
//											ENDIF
										ELSE
											IF GET_GAME_TIMER() > (iChat13Timer + 15000)
//												PLAY_PED_AMBIENT_SPEECH(YardGuard[1], "PROVOKE_TRESSPASS", SPEECH_PARAMS_FORCE_SHOUTED)
												PRINTSTRING("08 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[1], "PROVOKE_TRESPASS", "S_M_M_GENERICSECURITY_01_LATINO_MINI_02", SPEECH_PARAMS_FORCE_SHOUTED)
												iChat13Timer = GET_GAME_TIMER()
											ENDIF
//												ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[1], "FHPBSecGuard")
//												IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT13", CONV_PRIORITY_MEDIUM)
//													//Sorry no entry. Private grounds.
//													//Hey. You can't come in here.
//													//Move along please, no entry.
//													iChat13Timer = GET_GAME_TIMER()
//												ENDIF						
//											ENDIF
										ENDIF
									ELSE
										IF playerHasBeenWarnedBefore[1] = TRUE
											IF doneChat17 = FALSE
												PRINTSTRING("09 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[1], "PROVOKE_GENERIC", "S_M_M_GENERICSECURITY_01_LATINO_MINI_02", SPEECH_PARAMS_FORCE_NORMAL)
												iChat17Timer = GET_GAME_TIMER()
												doneChat17 = TRUE
//												ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[1], "FHPBSecGuard")
//												IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT17", CONV_PRIORITY_MEDIUM)
//													//Hey, I thought I told you to get out of here.
//													//What is your fucking problem?
//													//Just fuck off before I call the cops.
//													iChat17Timer = GET_GAME_TIMER()
//													doneChat17 = TRUE
//												ENDIF
											ELSE
												IF GET_GAME_TIMER() > (iChat17Timer + 15000)
													PRINTSTRING("10 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[1], "PROVOKE_GENERIC", "S_M_M_GENERICSECURITY_01_LATINO_MINI_02", SPEECH_PARAMS_FORCE_SHOUTED)
													iChat17Timer = GET_GAME_TIMER()
//													ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[1], "FHPBSecGuard")
//													IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT17", CONV_PRIORITY_MEDIUM)
//														//Hey, I thought I told you to get out of here.
//														//What is your fucking problem?
//														//Just fuck off before I call the cops.
//														iChat17Timer = GET_GAME_TIMER()
//													ENDIF						
												ENDIF
											ENDIF
										ENDIF
										IF playerHasBeenWarnedBefore[0] = TRUE
											IF doneChat18 = FALSE
												PRINTSTRING("11 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[1], "PROVOKE_GENERIC", "S_M_M_GENERICSECURITY_01_LATINO_MINI_02", SPEECH_PARAMS_FORCE_NORMAL)
												iChat18Timer = GET_GAME_TIMER()
												doneChat18 = TRUE
//												ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[1], "FHPBSecGuard")
//												IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT18", CONV_PRIORITY_MEDIUM)
//													//Hey my collegue's already told you leave.
//													//Go on, fuck off. Private property.
//													//If you don't leave the police will be called.
//													iChat18Timer = GET_GAME_TIMER()
//													doneChat18 = TRUE
//												ENDIF
											ELSE
												IF GET_GAME_TIMER() > (iChat18Timer + 15000)
													PRINTSTRING("12 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[1], "PROVOKE_GENERIC", "S_M_M_GENERICSECURITY_01_LATINO_MINI_02", SPEECH_PARAMS_FORCE_SHOUTED)
													iChat18Timer = GET_GAME_TIMER()
//													ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[1], "FHPBSecGuard")
//													IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT18", CONV_PRIORITY_MEDIUM)
//														//Hey my collegue's already told you leave.
//														//Go on, fuck off. Private property.
//														//If you don't leave the police will be called.
//														iChat18Timer = GET_GAME_TIMER()
//													ENDIF						
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

		BREAK
		
		CASE HATE_PLAYER
			
//			PRINTSTRING("eYardGuardMood = HATE_PLAYER") PRINTNL()
			
			//Flag for stat should not be set as player has been spotted.
			IF PlayerSpotted = FALSE
				PlayerSpotted = TRUE
			ENDIF			
			
			//Have peds act accordingly if player has been seen with a weapon			
			IF PlayerSpottedCarryingWeapon()
			OR GuardsShouldCombat = TRUE
				IF DOES_ENTITY_EXIST(YardGuard[0])
					IF NOT IS_PED_INJURED(YardGuard[0])
						IF CAN_PED_SEE_HATED_PED(YardGuard[0], PLAYER_PED_ID())
//							//Set the players wanted level to 2 again if they have got rid of it and come back and been re-spotted
//							IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
//								SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
//								SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//							ENDIF
							iCombatSeeTimer[0] = GET_GAME_TIMER()
							combatSeeTimerSet[0] = TRUE
							IF combatTaskGiven[0] = FALSE
								IF IS_ENTITY_ON_SCREEN(YardGuard[0])
									CLEAR_PED_TASKS(YardGuard[0])
									PRINTNL()PRINTSTRING("CLEAR_PED_TASKS(YardGuard[0]) 0.5")PRINTNL()
								ELSE
									CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[0])
									PRINTNL()PRINTSTRING("CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[0]) 0.5")PRINTNL()
								ENDIF
								SET_PED_COMBAT_ATTRIBUTES(YardGuard[0], CA_CAN_CHARGE, TRUE)
								TASK_COMBAT_PED(YardGuard[0], PLAYER_PED_ID())
								PRINTSTRING("TASK_COMBAT_PED(YardGuard[0], PLAYER_PED_ID())") PRINTNL()
								RESET_ALL_TASK_FLAGS(YardGuard[0])
								combatTaskGiven[0] = TRUE
							ENDIF
						ELSE
							IF followNavForCombat[0] = FALSE
								IF combatSeeTimerSet[0] = FALSE
									IF IS_ENTITY_ON_SCREEN(YardGuard[0])
										CLEAR_PED_TASKS(YardGuard[0])
										PRINTNL()PRINTSTRING("CLEAR_PED_TASKS(YardGuard[0]) 0.6")PRINTNL()
									ELSE
										CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[0])
										PRINTNL()PRINTSTRING("CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[0]) 0.6")PRINTNL()
									ENDIF
									TASK_FOLLOW_NAV_MESH_TO_COORD(YardGuard[0], GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVEBLENDRATIO_SPRINT, -1)
									RESET_ALL_TASK_FLAGS(YardGuard[0])
									followNavForCombat[0] = TRUE
								ENDIF
								IF combatSeeTimerSet[0] = TRUE
									IF GET_GAME_TIMER() > (iCombatSeeTimer[0] + 500)	
										IF IS_ENTITY_ON_SCREEN(YardGuard[0])
											CLEAR_PED_TASKS(YardGuard[0])
											PRINTNL()PRINTSTRING("CLEAR_PED_TASKS(YardGuard[0]) 0.7")PRINTNL()
										ELSE
											CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[0])
											PRINTNL()PRINTSTRING("CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[0]) 0.7")PRINTNL()
										ENDIF
										TASK_FOLLOW_NAV_MESH_TO_COORD(YardGuard[0], GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVEBLENDRATIO_SPRINT, -1)
										RESET_ALL_TASK_FLAGS(YardGuard[0])
										followNavForCombat[0] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(YardGuard[1])
					IF NOT IS_PED_INJURED(YardGuard[1])
						IF CAN_PED_SEE_HATED_PED(YardGuard[1], PLAYER_PED_ID())
//							//Set the players wanted level to 2 again if they have got rid of it and come back and been re-spotted
//							IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
//								SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
//								SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//							ENDIF
							iCombatSeeTimer[1] = GET_GAME_TIMER()
							combatSeeTimerSet[1] = TRUE
							IF combatTaskGiven[1] = FALSE
								IF IS_ENTITY_ON_SCREEN(YardGuard[1])
									CLEAR_PED_TASKS(YardGuard[1])
								ELSE
									CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[1])
								ENDIF
								SET_PED_COMBAT_ATTRIBUTES(YardGuard[1], CA_CAN_CHARGE, TRUE)
								TASK_COMBAT_PED(YardGuard[1], PLAYER_PED_ID())
								PRINTSTRING("TASK_COMBAT_PED(YardGuard[1], PLAYER_PED_ID())") PRINTNL()
								RESET_ALL_TASK_FLAGS(YardGuard[1])
								combatTaskGiven[1] = TRUE
							ENDIF
						ELSE
							IF followNavForCombat[1] = FALSE
								IF combatSeeTimerSet[1] = FALSE
									IF IS_ENTITY_ON_SCREEN(YardGuard[1])
										CLEAR_PED_TASKS(YardGuard[1])
									ELSE
										CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[1])
									ENDIF
									TASK_FOLLOW_NAV_MESH_TO_COORD(YardGuard[1], GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVEBLENDRATIO_SPRINT, -1)
									RESET_ALL_TASK_FLAGS(YardGuard[1])
									followNavForCombat[1] = TRUE
								ENDIF
								IF combatSeeTimerSet[1] = TRUE
									IF GET_GAME_TIMER() > (iCombatSeeTimer[1] + 500)	
										IF IS_ENTITY_ON_SCREEN(YardGuard[1])
											CLEAR_PED_TASKS(YardGuard[1])
										ELSE
											CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[1])
										ENDIF
										TASK_FOLLOW_NAV_MESH_TO_COORD(YardGuard[1], GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVEBLENDRATIO_SPRINT, -1)
										RESET_ALL_TASK_FLAGS(YardGuard[1])
										followNavForCombat[1] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
//				
//				//Give player 2 star wanted level if guards see player in truck
//				IF DOES_ENTITY_EXIST(DrillerTruck)
//					IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
//						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DrillerTruck)
//							IF CAN_PED_SEE_HATED_PED(YardGuard[0], PLAYER_PED_ID())
//							OR CAN_PED_SEE_HATED_PED(YardGuard[1], PLAYER_PED_ID())
//								IF Player2StarGiven = FALSE
//									IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
//										SET_WANTED_LEVEL_MULTIPLIER(1)
//										SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
//										SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//										Player2StarGiven = TRUE
//									ENDIF
//								ENDIF	
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
				
				//Have some aggressive dialogue now. Alter it depending on wether or not player has been seen with a weapon
				IF PlayerSpottedCarryingWeapon()
					//Give the player a 2 star wanted level now if they don't already have one.
					IF Player2StarGiven = FALSE
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
							SET_MAX_WANTED_LEVEL(5)
							SET_WANTED_LEVEL_MULTIPLIER(1)
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							PRINTSTRING("WANTED LEVEL BEING SET TO 2 AS PLAYER SPOTTED CARRYING A WEAPON = TRUE") PRINTNL()
							Player2StarGiven = TRUE
						ENDIF
					ENDIF
					//Have the guards shouting at the player telling him to leave immediately
					IF YardGuard0Closest = TRUE
						IF Chat16Done = FALSE
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < SafeDistance
								IF NOT IS_AMBIENT_SPEECH_PLAYING(YardGuard[0])
									PRINTSTRING("13 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[0], "GUN_COOL", "S_M_M_GENERICSECURITY_01_LATINO_MINI_01", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
									iChat16Timer = GET_GAME_TIMER()	
									Chat16Done = TRUE
//										ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[0], "FHPBSecGuard")
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT16", CONV_PRIORITY_MEDIUM)
//											//Put the weapon down asshole.
//											//Careful he's got a gun.
//											//If we don't get you the cops will.
//											iChat16Timer = GET_GAME_TIMER()
//											Chat16Done = TRUE
//										ENDIF
								ENDIF
							ENDIF
						ELSE
							IF GET_GAME_TIMER() > (iChat16Timer + 7000)
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < SafeDistance
									IF NOT IS_AMBIENT_SPEECH_PLAYING(YardGuard[0])
										PRINTSTRING("14 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[0], "GUN_COOL", "S_M_M_GENERICSECURITY_01_LATINO_MINI_01", SPEECH_PARAMS_FORCE_SHOUTED)
										iChat16Timer = GET_GAME_TIMER()	
//											ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[0], "FHPBSecGuard")
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT16", CONV_PRIORITY_MEDIUM)
//												//Put the weapon down asshole.
//												//Careful he's got a gun.
//												//If we don't get you the cops will.
//												iChat16Timer = GET_GAME_TIMER()
//											ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF YardGuard1Closest = TRUE
						IF Chat16Done = FALSE
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1]) < SafeDistance
								IF NOT IS_AMBIENT_SPEECH_PLAYING(YardGuard[1])
									PRINTSTRING("15 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[1], "GUN_COOL", "S_M_M_GENERICSECURITY_01_LATINO_MINI_02", SPEECH_PARAMS_FORCE_SHOUTED)
									iChat16Timer = GET_GAME_TIMER()
									Chat16Done = TRUE
//										ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[1], "FHPBSecGuard")
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT16", CONV_PRIORITY_MEDIUM)
//											//Put the weapon down asshole.
//											//Careful he's got a gun.
//											//If we don't get you the cops will.
//											iChat16Timer = GET_GAME_TIMER()
//											Chat16Done = TRUE
//										ENDIF
								ENDIF
							ENDIF
						ELSE
							IF GET_GAME_TIMER() > (iChat16Timer + 7000)
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1]) < SafeDistance
									IF NOT IS_AMBIENT_SPEECH_PLAYING(YardGuard[1])
										PRINTSTRING("16 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[1], "GUN_COOL", "S_M_M_GENERICSECURITY_01_LATINO_MINI_02", SPEECH_PARAMS_FORCE_SHOUTED)
										iChat16Timer = GET_GAME_TIMER()
//											ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[1], "FHPBSecGuard")
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT16", CONV_PRIORITY_MEDIUM)
//												//Put the weapon down asshole.
//												//Careful he's got a gun.
//												//If we don't get you the cops will.
//												iChat16Timer = GET_GAME_TIMER()
//											ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//Give the player a 1 star wanted level now
//					IF Player1StarGiven = FALSE
//						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 1
//							SET_WANTED_LEVEL_MULTIPLIER(1)
//							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 1)
//							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//							Player1StarGiven = TRUE
//						ENDIF
//					ENDIF
					
					IF copsHaveArrived = FALSE
						NearestCop = GET_PED_CLOSEST_PED_FROM_RELATIONSHIP_GROUP(PLAYER_PED_ID(), RELGROUPHASH_COP, 0, FALSE)
						IF NearestCop <> NULL
							IF DOES_ENTITY_EXIST(NearestCop)
								IF NOT IS_PED_INJURED(NearestCop)
									PRINTSTRING("NearestCop exists and is not injured") PRINTNL()
									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), NearestCop) < 35
										copsHaveArrived = TRUE
										PRINTSTRING("copsHaveArrived = TRUE") PRINTNL()
									ENDIF
								ENDIF
							ENDIF
						ELSE
							PRINTSTRING("NearestCop = NULL") PRINTNL()
						ENDIF
					ENDIF
					
					//Have the guards shouting at the player telling him to leave immediately
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					AND copsHaveArrived = FALSE 
						IF YardGuard0Closest = TRUE
							IF Chat15Done = FALSE
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < SafeDistance
									IF NOT IS_AMBIENT_SPEECH_PLAYING(YardGuard[0])
										PRINTSTRING("17 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[0], "GENERIC_INSULT_HIGH", "S_M_M_GENERICSECURITY_01_LATINO_MINI_01", SPEECH_PARAMS_FORCE_SHOUTED)
										iChat15Timer = GET_GAME_TIMER()
										Chat15Done = TRUE
//											ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[0], "FHPBSecGuard")
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT15", CONV_PRIORITY_MEDIUM)
//												//The cops are on their way asshole.
//												//You're fucked now. Cops will be here any second.
//												//Enjoy spending a night in the cells.
//												iChat15Timer = GET_GAME_TIMER()
//												Chat15Done = TRUE
//											ENDIF
									ENDIF
								ENDIF
							ELSE
								IF GET_GAME_TIMER() > (iChat15Timer + 7000)
									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < SafeDistance
										IF NOT IS_AMBIENT_SPEECH_PLAYING(YardGuard[0])
											PRINTSTRING("18 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[0], "GENERIC_CURSE_HIGH", "S_M_M_GENERICSECURITY_01_LATINO_MINI_01", SPEECH_PARAMS_FORCE_SHOUTED)
											iChat15Timer = GET_GAME_TIMER()
//											IF NOT IS_MESSAGE_BEING_DISPLAYED()
//											OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//												ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[0], "FHPBSecGuard")
//												IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT15", CONV_PRIORITY_MEDIUM)
//													//The cops are on their way asshole.
//													//You're fucked now. Cops will be here any second.
//													//Enjoy spending a night in the cells.
//													iChat15Timer = GET_GAME_TIMER()
//												ENDIF
//											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						IF YardGuard1Closest = TRUE
							IF Chat15Done = FALSE
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1]) < SafeDistance
									IF NOT IS_AMBIENT_SPEECH_PLAYING(YardGuard[1])
										PRINTSTRING("19 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[1], "GENERIC_INSULT_HIGH", "S_M_M_GENERICSECURITY_01_LATINO_MINI_02", SPEECH_PARAMS_FORCE_SHOUTED)
										iChat15Timer = GET_GAME_TIMER()
										Chat15Done = TRUE
//											ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[1], "FHPBSecGuard")
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT15", CONV_PRIORITY_MEDIUM)
//												//The cops are on their way asshole.
//												//You're fucked now. Cops will be here any second.
//												//Enjoy spending a night in the cells.
//												iChat15Timer = GET_GAME_TIMER()
//												Chat15Done = TRUE
//											ENDIF
									ENDIF
								ENDIF
							ELSE
								IF GET_GAME_TIMER() > (iChat15Timer + 7000)
									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1]) < SafeDistance
										IF NOT IS_AMBIENT_SPEECH_PLAYING(YardGuard[1])
											PRINTSTRING("20 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[1], "GENERIC_CURSE_HIGH", "S_M_M_GENERICSECURITY_01_LATINO_MINI_02", SPEECH_PARAMS_FORCE_SHOUTED)
											iChat15Timer = GET_GAME_TIMER()
//												ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[1], "FHPBSecGuard")
//												IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT15", CONV_PRIORITY_MEDIUM)
//													//The cops are on their way asshole.
//													//You're fucked now. Cops will be here any second.
//													//Enjoy spending a night in the cells.
//													iChat15Timer = GET_GAME_TIMER()
//												ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT PlayerSpottedCarryingWeapon()
			
				//For bug 1794959 Run a check for player being in middle of yard with guards looking at him if so then start the timer for guards to attack
				IF GuardsCombatTimerSet = FALSE
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<893.2, -1556.5, 30>>) < 18
						IF DOES_ENTITY_EXIST(YardGuard[0])
							IF NOT IS_PED_INJURED(YardGuard[0])
								IF CAN_PED_SEE_HATED_PED(YardGuard[0], PLAYER_PED_ID())
									iGuardsCombatTimer = GET_GAME_TIMER()
									GuardsCombatTimerSet = TRUE
									PRINTNL() PRINTSTRING("Guard 0 can see player who is in middle of yard combat timer being started") PRINTNL()
								ENDIF
							ENDIF
						ENDIF
						IF DOES_ENTITY_EXIST(YardGuard[1])
							IF NOT IS_PED_INJURED(YardGuard[1])
								IF CAN_PED_SEE_HATED_PED(YardGuard[1], PLAYER_PED_ID())
									iGuardsCombatTimer = GET_GAME_TIMER()
									GuardsCombatTimerSet = TRUE
									PRINTNL() PRINTSTRING("Guard 1 can see player who is in middle of yard combat timer being started") PRINTNL()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF			
				
				//Make the 2 guards come to the player 
				IF GuardsShouldCombat = FALSE
					IF DOES_ENTITY_EXIST(YardGuard[0])
						IF NOT IS_PED_INJURED(YardGuard[0])
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) > 4
								IF CAN_PED_SEE_HATED_PED(YardGuard[0], PLAYER_PED_ID())
									IF TaskGoToEntity[0] = FALSE
										IF IS_ENTITY_ON_SCREEN(YardGuard[0])
											CLEAR_PED_TASKS(YardGuard[0])
											PRINTNL()PRINTSTRING("CLEAR_PED_TASKS(YardGuard[0]) 0.1")PRINTNL()
										ELSE
											CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[0])
											PRINTNL()PRINTSTRING("CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[0]) 0.1")PRINTNL()
										ENDIF
										TASK_GO_TO_ENTITY(YardGuard[0], PLAYER_PED_ID(), -1)
										PRINTSTRING("TASK_GO_TO_ENTITY(YardGuard[0], PLAYER_PED_ID(), -1)") PRINTNL()
										RESET_ALL_TASK_FLAGS(YardGuard[0])
										TaskGoToEntity[0] = TRUE
									ENDIF
									iPedCanSeePlayerTimer[0] = GET_GAME_TIMER()
								ELSE	
									IF GET_GAME_TIMER() > (iPedCanSeePlayerTimer[0] + 500)
										IF TaskNavmeshToPed[0] = FALSE
											IF IS_ENTITY_ON_SCREEN(YardGuard[0])
												CLEAR_PED_TASKS(YardGuard[0])
												PRINTNL()PRINTSTRING("CLEAR_PED_TASKS(YardGuard[0]) 0.2")PRINTNL()
											ELSE
												CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[0])
												PRINTNL()PRINTSTRING("CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[0]) 0.2")PRINTNL()
											ENDIF
											TASK_FOLLOW_NAV_MESH_TO_COORD(YardGuard[0], GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVEBLENDRATIO_RUN, -1)
											PRINTSTRING("TASK_FOLLOW_NAV_MESH_TO_COORD(YardGuard[0], GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVEBLENDRATIO_RUN, -1)") PRINTNL()
											RESET_ALL_TASK_FLAGS(YardGuard[0])
											TaskNavmeshToPed[0] = TRUE
										ENDIF										
									ENDIF
								ENDIF
							ELSE
								IF GuardFacingPlayer[0] = FALSE
									IF IS_ENTITY_ON_SCREEN(YardGuard[0])
										CLEAR_PED_TASKS(YardGuard[0])
										PRINTNL()PRINTSTRING("CLEAR_PED_TASKS(YardGuard[0]) 0.3")PRINTNL()
									ELSE
										CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[0])
										PRINTNL()PRINTSTRING("CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[0]) 0.3")PRINTNL()
									ENDIF
									OPEN_SEQUENCE_TASK(seq)
										TASK_TURN_PED_TO_FACE_ENTITY(null, PLAYER_PED_ID())
										TASK_STAND_STILL(null, 2000)
										SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(YardGuard[0], seq)
									CLEAR_SEQUENCE_TASK(seq)	
									RESET_ALL_TASK_FLAGS(YardGuard[0])
									GuardFacingPlayer[0] = TRUE
									PRINTSTRING("TASK_PERFORM_SEQUENCE(YardGuard[0], seq)") PRINTNL()
								ENDIF
							ENDIF
							
							//Have the guards shouting at the player telling him to leave immediately
							IF GuardsFightingTalkStarted = FALSE
								IF GuardsAlerted = FALSE
								OR doneChat13 = TRUE
									IF playerWarnedChat = FALSE
										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < 10
											IF NOT IS_AMBIENT_SPEECH_PLAYING(YardGuard[0])
												PRINTSTRING("21 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[0], "PROVOKE_TRESPASS", "S_M_M_GENERICSECURITY_01_LATINO_MINI_01", SPEECH_PARAMS_ALLOW_REPEAT)
												iChat14Timer = GET_GAME_TIMER()
												iFightCHat = GET_GAME_TIMER()
												iGuardsCombatTimer = GET_GAME_TIMER()
												GuardsCombatTimerSet = TRUE
												playerWarnedChat = TRUE
												playerHasBeenWarnedBefore[0] = TRUE
	//												ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[0], "FHPBSecGuard")
	//												IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT14", CONV_PRIORITY_MEDIUM)
	//													//Where the hell do you think you're going?
	//													//Are you deaf? I said no entry. 
	//													//Leave the yard immediately.
	//													iChat14Timer = GET_GAME_TIMER()
	//													iGuardsCombatTimer = GET_GAME_TIMER()
	//													GuardsCombatTimerSet = TRUE
	//													playerWarnedChat = TRUE
	//													playerHasBeenWarnedBefore[0] = TRUE
	//												ENDIF
											ENDIF
										ENDIF
									ELSE
										IF GET_GAME_TIMER() > (iChat14Timer + 7000)
											IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < 10
												IF NOT IS_AMBIENT_SPEECH_PLAYING(YardGuard[0])
													PRINTSTRING("22 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[0], "PROVOKE_TRESPASS", "S_M_M_GENERICSECURITY_01_LATINO_MINI_01", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
													iChat14Timer = GET_GAME_TIMER()
	//													ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[0], "FHPBSecGuard")
	//													IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT14", CONV_PRIORITY_MEDIUM)
	//														//Where the hell do you think you're going?
	//														//Are you deaf? I said no entry. 
	//														//Leave the yard immediately.
	//														iChat14Timer = GET_GAME_TIMER()
	//														playerWarnedChat = TRUE
	//													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE	
									IF doneChat13 = FALSE
										IF doneChat25 = FALSE
											IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < 10
												IF NOT IS_AMBIENT_SPEECH_PLAYING(YardGuard[0])
	//											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
	//												IF NOT IS_MESSAGE_BEING_DISPLAYED()
	//												OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
														PRINTSTRING("23 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
														PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[0], "PROVOKE_TRESPASS", "S_M_M_GENERICSECURITY_01_LATINO_MINI_01", SPEECH_PARAMS_FORCE_NORMAL_CRITICAL)
														playerWarnedChat = TRUE
														doneChat25 = TRUE
	//													ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[0], "FHPBSecGuard")
	//													IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT25", CONV_PRIORITY_MEDIUM)
	//														//Hey, how the hell did you get in here? This is private property.
	//														//Come on, get out of here before I call the cops.
	//														playerWarnedChat = TRUE
	//														doneChat25 = TRUE
	//													ENDIF
	//												ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF GET_GAME_TIMER() > (iFightCHat + 7000)
									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[0]) < 15
										IF NOT IS_AMBIENT_SPEECH_PLAYING(YardGuard[0])
											PRINTSTRING("30 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[0], "FIGHT", "S_M_M_GENERICSECURITY_01_LATINO_MINI_01", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
											iFightCHat = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(YardGuard[1])
						IF NOT IS_PED_INJURED(YardGuard[1])
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1]) > 4
								IF CAN_PED_SEE_HATED_PED(YardGuard[1], PLAYER_PED_ID())
									IF TaskGoToEntity[1] = FALSE
										IF IS_ENTITY_ON_SCREEN(YardGuard[1])
											CLEAR_PED_TASKS(YardGuard[1])
										ELSE
											CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[1])
										ENDIF
										TASK_GO_TO_ENTITY(YardGuard[1], PLAYER_PED_ID(), -1)
										PRINTSTRING("TASK_GO_TO_ENTITY(YardGuard[1], PLAYER_PED_ID(), -1)") PRINTNL()
										RESET_ALL_TASK_FLAGS(YardGuard[1])
										TaskGoToEntity[1] = TRUE
									ENDIF
									iPedCanSeePlayerTimer[1] = GET_GAME_TIMER()
								ELSE	
									IF GET_GAME_TIMER() > (iPedCanSeePlayerTimer[1] + 500)
										IF TaskNavmeshToPed[1] = FALSE
											IF IS_ENTITY_ON_SCREEN(YardGuard[1])
												CLEAR_PED_TASKS(YardGuard[1])
											ELSE
												CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[1])
											ENDIF
											TASK_FOLLOW_NAV_MESH_TO_COORD(YardGuard[1], GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVEBLENDRATIO_RUN, -1)
											PRINTSTRING("TASK_FOLLOW_NAV_MESH_TO_COORD(YardGuard[1], GET_ENTITY_COORDS(PLAYER_PED_ID()), PEDMOVEBLENDRATIO_RUN, -1)") PRINTNL()
											RESET_ALL_TASK_FLAGS(YardGuard[1])
											TaskNavmeshToPed[1] = TRUE
										ENDIF										
									ENDIF
								ENDIF
							ELSE
								IF GuardFacingPlayer[1] = FALSE
									IF IS_ENTITY_ON_SCREEN(YardGuard[1])
										CLEAR_PED_TASKS(YardGuard[1])
									ELSE
										CLEAR_PED_TASKS_IMMEDIATELY(YardGuard[1])
									ENDIF
									OPEN_SEQUENCE_TASK(seq)
										TASK_TURN_PED_TO_FACE_ENTITY(null, PLAYER_PED_ID())
										TASK_STAND_STILL(null, 2000)
										SET_SEQUENCE_TO_REPEAT(seq, REPEAT_FOREVER)
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(YardGuard[1], seq)
									CLEAR_SEQUENCE_TASK(seq)								
									RESET_ALL_TASK_FLAGS(YardGuard[1])
									GuardFacingPlayer[1] = TRUE
									PRINTSTRING("TASK_PERFORM_SEQUENCE(YardGuard[1], seq)") PRINTNL()
								ENDIF	
							ENDIF
							//Have the guards shouting at the player telling him to leave immediately
							IF GuardsFightingTalkStarted = FALSE
								IF GuardsAlerted = FALSE
								OR doneChat13 = TRUE
									IF playerWarnedChat = FALSE
										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1]) < 10
											IF NOT IS_AMBIENT_SPEECH_PLAYING(YardGuard[1])
												PRINTSTRING("24 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[1], "PROVOKE_TRESPASS", "S_M_M_GENERICSECURITY_01_LATINO_MINI_02", SPEECH_PARAMS_FORCE_SHOUTED)
												iChat14Timer = GET_GAME_TIMER()
												iFightCHat2 = GET_GAME_TIMER()
												iGuardsCombatTimer = GET_GAME_TIMER()
												GuardsCombatTimerSet = TRUE
												playerWarnedChat = TRUE
												playerHasBeenWarnedBefore[1] = TRUE
	//												ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[1], "FHPBSecGuard")
	//												IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT14", CONV_PRIORITY_MEDIUM)
	//													//Where the hell do you think you're going?
	//													//Are you deaf? I said no entry. 
	//													//Leave the yard immediately.
	//													iChat14Timer = GET_GAME_TIMER()
	//													iGuardsCombatTimer = GET_GAME_TIMER()
	//													GuardsCombatTimerSet = TRUE
	//													playerWarnedChat = TRUE
	//													playerHasBeenWarnedBefore[1] = TRUE
	//												ENDIF
											ENDIF
										ENDIF
									ELSE
										IF GET_GAME_TIMER() > (iChat14Timer + 7000)
											IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1]) < 10
												IF NOT IS_AMBIENT_SPEECH_PLAYING(YardGuard[1])
													PRINTSTRING("25 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[1], "PROVOKE_TRESPASS", "S_M_M_GENERICSECURITY_01_LATINO_MINI_02", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
													iChat14Timer = GET_GAME_TIMER()
	//													ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[1], "FHPBSecGuard")
	//													IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT14", CONV_PRIORITY_MEDIUM)
	//														//Where the hell do you think you're going?
	//														//Are you deaf? I said no entry. 
	//														//Leave the yard immediately.
	//														iChat14Timer = GET_GAME_TIMER()
	//														playerWarnedChat = TRUE
	//													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE	
									IF doneChat13 = FALSE
										IF doneChat25 = FALSE
											IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1]) < 10
												IF NOT IS_AMBIENT_SPEECH_PLAYING(YardGuard[1])
													PRINTSTRING("26 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[1], "PROVOKE_TRESPASS", "S_M_M_GENERICSECURITY_01_LATINO_MINI_02", SPEECH_PARAMS_FORCE_NORMAL_CRITICAL)
													playerWarnedChat = TRUE
													doneChat25 = TRUE
	//													ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, YardGuard[1], "FHPBSecGuard")
	//													IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT25", CONV_PRIORITY_MEDIUM)
	//														//Hey, how the hell did you get in here? This is private property.
	//														//Come on, get out of here before I call the cops.
	//														playerWarnedChat = TRUE
	//														doneChat25 = TRUE
	//													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF GET_GAME_TIMER() > (iFightCHat2 + 7000)
									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), YardGuard[1]) < 10
										IF NOT IS_AMBIENT_SPEECH_PLAYING(YardGuard[1])
											PRINTSTRING("31 PLAY_PED_AMBIENT_SPEECH_WITH_VOICE")
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(YardGuard[1], "FIGHT", "S_M_M_GENERICSECURITY_01_LATINO_MINI_02", SPEECH_PARAMS_FORCE_SHOUTED_CRITICAL)
											iFightCHat2 = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				
					//Allow 22 seconds before the guards should start shouting fighting talk at the player
					IF GuardsCombatTimerSet = TRUE
						IF GuardsFightingTalkStarted = FALSE
							IF GET_GAME_TIMER() > (iGuardsCombatTimer + 22000)
								PRINTNL() PRINTSTRING("FLAG CHANGE ... GuardsFightingTalkStarted = TRUE") PRINTNL()
								GuardsFightingTalkStarted = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					//Allow 30 seconds for the player to leave or set the guards to combat
					IF GuardsCombatTimerSet = TRUE
						IF GuardsShouldCombat = FALSE
							PRINTNL() PRINTSTRING("FLAG CHANGE GuardsCombatTimerSet = TRUE Countdown 30 seconds has begun before guards will open fire") PRINTNL()
							IF GET_GAME_TIMER() > (iGuardsCombatTimer + 30000)
								SET_PLAYER_WANTED_LEVEL(player_id(), 2)
								GuardsShouldCombat = TRUE
								PRINTNL() PRINTSTRING("FLAG CHANGE ... GuardsShouldCombat = TRUE") PRINTNL()
							ENDIF
						ENDIF
					ENDIF
					
					//Set guards into combat mode if they see the player getting into the truck
					IF DOES_ENTITY_EXIST(DrillerTruck)
						IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DrillerTruck, TRUE)
								IF CAN_PED_SEE_HATED_PED(YardGuard[0], PLAYER_PED_ID())
								OR CAN_PED_SEE_HATED_PED(YardGuard[1], PLAYER_PED_ID())
									GuardsShouldCombat = TRUE
								ENDIF
							ENDIF
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), DrillerTruck) < 5
							AND IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
								IF CAN_PED_SEE_HATED_PED(YardGuard[0], PLAYER_PED_ID())
								OR CAN_PED_SEE_HATED_PED(YardGuard[1], PLAYER_PED_ID())
									GuardsShouldCombat = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					//Set guards into combat mode if they see the player fighting anyone
					IF DOES_ENTITY_EXIST(YardGuard[0])
						IF NOT IS_PED_INJURED(YardGuard[0])
							IF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
								IF IS_PED_PERFORMING_MELEE_ACTION(PLAYER_PED_ID())
									IF CAN_PED_SEE_HATED_PED(YardGuard[0], PLAYER_PED_ID())
										GuardsShouldCombat = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(YardGuard[1])
						IF NOT IS_PED_INJURED(YardGuard[1])
							IF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
								IF IS_PED_PERFORMING_MELEE_ACTION(PLAYER_PED_ID())
									IF CAN_PED_SEE_HATED_PED(YardGuard[1], PLAYER_PED_ID())
										GuardsShouldCombat = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF	
			ENDIF
			
		BREAK
		
	ENDSWITCH
					
ENDPROC


//PURPOSE:Sets up the truck and the 2 cutters.
PROC SETUP_TRUCK_AND_CUTTER()

//	REQUEST_MODEL(CUTTER)
	REQUEST_MODEL(ARMYTRAILER2)
	REQUEST_MODEL(PACKER)
	REQUEST_MODEL(S_M_Y_CONSTRUCT_01)
	REQUEST_MODEL(S_M_M_SECURITY_01)
	REQUEST_MODEL(BISON2)
	
//	WHILE NOT HAS_MODEL_LOADED(CUTTER)
	WHILE NOT HAS_MODEL_LOADED(ARMYTRAILER2)
	OR NOT HAS_MODEL_LOADED(PACKER)
	OR NOT HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)
	OR NOT HAS_MODEL_LOADED(S_M_M_SECURITY_01)
	OR NOT HAS_MODEL_LOADED(BISON2)
		PRINTSTRING("Waiting for models to load") PRINTNL()
		WAIT(0)
	ENDWHILE

	//Grab the vehicles from the trigger scene 
//	IF NOT DOES_ENTITY_EXIST(Driller)
//		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
//			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
//			Driller = g_sTriggerSceneAssets.veh[0]
//			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(Driller, FALSE)
//		ELSE
//			Driller = CREATE_VEHICLE(CUTTER, << -1915.7711, -635.8125, 9.4879 >>, 33.6688)
//			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(Driller, FALSE)
//		ENDIF
//	ENDIF
	IF NOT DOES_ENTITY_EXIST(DrillerTrailer)
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[1], TRUE, TRUE)
			DrillerTrailer = g_sTriggerSceneAssets.veh[1] 
			ADD_VEHICLE_UPSIDEDOWN_CHECK(DrillerTrailer)
		ELSE
			DrillerTrailer = CREATE_VEHICLE(ARMYTRAILER2, <<919.3030, -1553.8795, 29.7789>>, 167.1917)
			ADD_VEHICLE_UPSIDEDOWN_CHECK(DrillerTrailer)
		ENDIF
//		SET_VEHICLE_EXTRA(DrillerTrailer, 8, FALSE)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(DrillerTruck)
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[2])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[2], TRUE, TRUE)
			DrillerTruck = g_sTriggerSceneAssets.veh[2] 
			SET_VEHICLE_HAS_STRONG_AXLES(DrillerTruck, TRUE)
			SET_VEHICLE_DOORS_LOCKED(DrillerTruck, VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(DrillerTruck)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(DrillerTruck)
		ELSE
			DrillerTruck = CREATE_VEHICLE(PACKER, <<919.3030, -1553.8795, 29.7789>>, 167.1917)
			SET_VEHICLE_HAS_STRONG_AXLES(DrillerTruck, TRUE)
			SET_VEHICLE_DOORS_LOCKED(DrillerTruck, VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(DrillerTruck)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(DrillerTruck)
		ENDIF
	ENDIF	
	
	ATTACH_VEHICLE_TO_TRAILER(DrillerTruck, DrillerTrailer, 0.5)
//	ATTACH_ENTITY_TO_ENTITY(Driller, DrillerTrailer, 0, <<0, -1.3, -0.6>>, <<0,0,0>>, FALSE, FALSE, TRUE)		

//	SET_MODEL_AS_NO_LONGER_NEEDED(CUTTER)
	SET_MODEL_AS_NO_LONGER_NEEDED(ARMYTRAILER2)
	SET_MODEL_AS_NO_LONGER_NEEDED(PACKER)
	SET_MODEL_AS_NO_LONGER_NEEDED(BISON2)
	
	//Grab the peds from the trigger scene 
	IF NOT DOES_ENTITY_EXIST(EnemyPed[0])
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
			EnemyPed[0] = g_sTriggerSceneAssets.ped[0] 
			SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed[0], workerMainGroup)
			SET_PED_VISUAL_FIELD_PROPERTIES(EnemyPed[0], 30)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[0], TRUE)
		ELSE
			EnemyPed[0] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<912.3400, -1543.2975, 29.6469>>, 16.1690)
			SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed[0], workerMainGroup)
			IF DOES_SCENARIO_EXIST_IN_AREA(<<912.3, -1542.6, 30.4>>, 3, FALSE)
				TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(EnemyPed[0], <<912.3, -1542.6, 30.4>>, 5, 0)
			ENDIF
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[0], TRUE)
			SET_PED_VISUAL_FIELD_PROPERTIES(EnemyPed[0], 30)
		ENDIF
	ENDIF	
	IF NOT DOES_ENTITY_EXIST(EnemyPed[1])
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[1], TRUE, TRUE)
			EnemyPed[1] = g_sTriggerSceneAssets.ped[1] 
			SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed[1], workerMainGroup)
			SET_PED_VISUAL_FIELD_PROPERTIES(EnemyPed[1], 30)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[1], TRUE)
		ELSE
			EnemyPed[1] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<917.5028, -1517.4009, 29.9673>>, 158.5738)
			SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed[1], workerMainGroup)
			IF DOES_SCENARIO_EXIST_IN_AREA(<<917.5028, -1517.4009, 29.9673>>, 3, FALSE)
				TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(EnemyPed[1], <<917.5028, -1517.4009, 29.9673>>, 5, 0)
			ENDIF
			SET_PED_VISUAL_FIELD_PROPERTIES(EnemyPed[1], 30)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[1], TRUE)
		ENDIF
	ENDIF	
	IF NOT DOES_ENTITY_EXIST(EnemyPed[2])
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[2], TRUE, TRUE)
			EnemyPed[2] = g_sTriggerSceneAssets.ped[2] 
			SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed[2], workerMainGroup)
			SET_PED_VISUAL_FIELD_PROPERTIES(EnemyPed[2], 30)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[2], TRUE)
		ELSE
			EnemyPed[2] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<869.6423, -1541.4226, 29.2516>>, 346.9848)
			SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed[2], workerMainGroup)
			IF DOES_SCENARIO_EXIST_IN_AREA(<<869.6423, -1541.4226, 29.2516>>, 3, FALSE)
				TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(EnemyPed[2], <<869.6423, -1541.4226, 29.2516>>, 5, 0)
			ENDIF
			SET_PED_VISUAL_FIELD_PROPERTIES(EnemyPed[2], 30)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[2], TRUE)
		ENDIF
	ENDIF	
	IF NOT DOES_ENTITY_EXIST(EnemyPed[3])
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[3], TRUE, TRUE)
			EnemyPed[3] = g_sTriggerSceneAssets.ped[3] 
			SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed[3], workerMainGroup)
			SET_PED_VISUAL_FIELD_PROPERTIES(EnemyPed[3], 30)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[3], TRUE)
		ELSE
			EnemyPed[3] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<884.3046, -1573.1875, 29.8247>>, 182.9722)
			SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed[3], workerMainGroup)
			IF DOES_SCENARIO_EXIST_IN_AREA(<<884.3046, -1573.1875, 29.8247>>, 3, FALSE)
				TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(EnemyPed[3], <<884.3046, -1573.1875, 29.8247>>, 5, 0)
			ENDIF
			SET_PED_VISUAL_FIELD_PROPERTIES(EnemyPed[3], 30)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[3], TRUE)
		ENDIF
	ENDIF	
	IF NOT DOES_ENTITY_EXIST(EnemyPed[4])
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[4])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[4], TRUE, TRUE)
			EnemyPed[4] = g_sTriggerSceneAssets.ped[4] 
			SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed[4], workerMainGroup)
			SET_PED_VISUAL_FIELD_PROPERTIES(EnemyPed[4], 30)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[4], TRUE)
		ELSE
			EnemyPed[4] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<903.8805, -1575.0199, 29.8327>>, 308.1952)
			SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed[4], workerMainGroup)
			IF DOES_SCENARIO_EXIST_IN_AREA(<<903.8805, -1575.0199, 29.8327>>, 3, FALSE)
				TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(EnemyPed[4], <<903.8805, -1575.0199, 29.8327>>, 5, 0)
			ENDIF
			SET_PED_VISUAL_FIELD_PROPERTIES(EnemyPed[4], 30)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[4], TRUE)
		ENDIF
	ENDIF	
	IF NOT DOES_ENTITY_EXIST(EnemyPed[5])
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[5])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[5], TRUE, TRUE)
			EnemyPed[5] = g_sTriggerSceneAssets.ped[5] 
			SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed[5], workerMainGroup)
			SET_PED_VISUAL_FIELD_PROPERTIES(EnemyPed[5], 30)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[5], TRUE)
		ELSE
			EnemyPed[5] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<906.2186, -1575.1078, 29.8125>>, 55.9906)
			SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed[5], workerMainGroup)
			IF DOES_SCENARIO_EXIST_IN_AREA(<<906.2186, -1575.1078, 29.8125>>, 3, FALSE)
				TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(EnemyPed[5], <<906.2186, -1575.1078, 29.8125>>, 5, 0)
			ENDIF
			SET_PED_VISUAL_FIELD_PROPERTIES(EnemyPed[5], 30)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[5], TRUE)
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(EnemyPed[6])
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[8])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[8], TRUE, TRUE)
			EnemyPed[6] = g_sTriggerSceneAssets.ped[8] 
			SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed[6], workerMainGroup)
			SET_PED_VISUAL_FIELD_PROPERTIES(EnemyPed[6], 20, 5, 120, -45, 45)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[6], TRUE)
		ELSE
			EnemyPed[6] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<889.2850, -1561.4847, 29.6539>>, 22.2456)
			SET_PED_RELATIONSHIP_GROUP_HASH(EnemyPed[6], workerMainGroup)
			IF DOES_SCENARIO_EXIST_IN_AREA(<<889.2850, -1561.4847, 29.6539>>, 3, FALSE)
				TASK_USE_NEAREST_SCENARIO_TO_COORD_WARP(EnemyPed[6], <<889.2850, -1561.4847, 29.6539>>, 5, 0)
			ENDIF
			SET_PED_VISUAL_FIELD_PROPERTIES(EnemyPed[6], 20, 5, 120, -45, 45)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyPed[6], TRUE)
		ENDIF
	ENDIF	
	//The guards
	IF NOT DOES_ENTITY_EXIST(YardGuard[0])
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[6])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[6], TRUE, TRUE)
			YardGuard[0] = g_sTriggerSceneAssets.ped[6] 
			SET_PED_RELATIONSHIP_GROUP_HASH(YardGuard[0], GuardMainGroup)
			SET_PED_VISUAL_FIELD_PROPERTIES(YardGuard[0], 40)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(YardGuard[0], TRUE)
			GIVE_WEAPON_TO_PED(YardGuard[0], WEAPONTYPE_PISTOL, 100, FALSE, FALSE)
			SET_AMBIENT_VOICE_NAME(YardGuard[0], "S_M_M_GENERICSECURITY_01_LATINO_MINI_01")
		ELSE
			YardGuard[0] = CREATE_PED(PEDTYPE_MISSION, S_M_M_SECURITY_01, <<863.1551, -1564.5721, 29.3231>>, 130.7946)
			SET_PED_RELATIONSHIP_GROUP_HASH(YardGuard[0], GuardMainGroup)
			SET_PED_VISUAL_FIELD_PROPERTIES(YardGuard[0], 40)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(YardGuard[0], TRUE)
			GIVE_WEAPON_TO_PED(YardGuard[0], WEAPONTYPE_PISTOL, 100, FALSE, FALSE)
			SET_AMBIENT_VOICE_NAME(YardGuard[0], "S_M_M_GENERICSECURITY_01_LATINO_MINI_01")
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(YardGuard[1])
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[7])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[7], TRUE, TRUE)
			YardGuard[1] = g_sTriggerSceneAssets.ped[7] 
			SET_PED_RELATIONSHIP_GROUP_HASH(YardGuard[1], GuardMainGroup)
			SET_PED_VISUAL_FIELD_PROPERTIES(YardGuard[1], 40)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(YardGuard[1], TRUE)
			GIVE_WEAPON_TO_PED(YardGuard[1], WEAPONTYPE_PISTOL, 100, FALSE, FALSE)
			SET_AMBIENT_VOICE_NAME(YardGuard[1], "S_M_M_GENERICSECURITY_01_LATINO_MINI_02")
		ELSE
			YardGuard[1] = CREATE_PED(PEDTYPE_MISSION, S_M_M_SECURITY_01, <<940.2881, -1573.8774, 29.3866>>, 269.1856)
			SET_PED_RELATIONSHIP_GROUP_HASH(YardGuard[1], GuardMainGroup)
			SET_PED_VISUAL_FIELD_PROPERTIES(YardGuard[1], 40)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(YardGuard[1], TRUE)
			GIVE_WEAPON_TO_PED(YardGuard[1], WEAPONTYPE_PISTOL, 100, FALSE, FALSE)
			SET_AMBIENT_VOICE_NAME(YardGuard[1], "S_M_M_GENERICSECURITY_01_LATINO_MINI_02")
		ENDIF
	ENDIF

	#IF IS_DEBUG_BUILD
		IF DOES_ENTITY_EXIST(EnemyPed[0])
			TEXT_LABEL tDebugName = "PedID 0"
			SET_PED_NAME_DEBUG(EnemyPed[0], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(EnemyPed[1])
			TEXT_LABEL tDebugName = "PedID 1"
			SET_PED_NAME_DEBUG(EnemyPed[1], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(EnemyPed[2])
			TEXT_LABEL tDebugName = "PedID 2"
			SET_PED_NAME_DEBUG(EnemyPed[2], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(EnemyPed[3])
			TEXT_LABEL tDebugName = "PedID 3"
			SET_PED_NAME_DEBUG(EnemyPed[3], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(EnemyPed[4])
			TEXT_LABEL tDebugName = "PedID 4"
			SET_PED_NAME_DEBUG(EnemyPed[4], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(EnemyPed[5])
			TEXT_LABEL tDebugName = "PedID 5"
			SET_PED_NAME_DEBUG(EnemyPed[5], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(EnemyPed[6])
			TEXT_LABEL tDebugName = "PedID 6"
			SET_PED_NAME_DEBUG(EnemyPed[6], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(YardGuard[0])
			TEXT_LABEL tDebugName = "YardGuard 0"
			SET_PED_NAME_DEBUG(YardGuard[0], tDebugName)
		ENDIF
		IF DOES_ENTITY_EXIST(YardGuard[1])
			TEXT_LABEL tDebugName = "YardGuard 1"
			SET_PED_NAME_DEBUG(YardGuard[1], tDebugName)
		ENDIF
	#ENDIF	
	
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_CONSTRUCT_01)
	SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_SECURITY_01)
	
ENDPROC

//PURPOSE:Removes all blips for skipping
PROC REMOVE_BLIPS()

	IF DOES_BLIP_EXIST(DrillerBlip)
		REMOVE_BLIP(DrillerBlip)
	ENDIF
	IF DOES_BLIP_EXIST(HomeBlip)
		REMOVE_BLIP(HomeBlip)
	ENDIF
	IF DOES_BLIP_EXIST(YardBlip)
		REMOVE_BLIP(YardBlip)
	ENDIF	
	IF DOES_BLIP_EXIST(TrailerBlip)
		REMOVE_BLIP(TrailerBlip)
	ENDIF	
//	FOR icount = 0 TO 6
//		IF DOES_BLIP_EXIST(EnemyPedBlip[icount])
//			REMOVE_BLIP(EnemyPedBlip[icount])
//		ENDIF
//	ENDFOR
	
ENDPROC

//PURPOSE: Delete's all mission entities for skipping
PROC DELETE_ALL_ENTITIES()	

	//Peds
	FOR icount = 0 TO 6
		IF DOES_ENTITY_EXIST(EnemyPed[icount])
		AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(EnemyPed[icount])
			DELETE_PED(EnemyPed[icount])
		ENDIF		
	ENDFOR
	IF DOES_ENTITY_EXIST(YardGuard[0])
	AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(YardGuard[0])
		DELETE_PED(YardGuard[0])
	ENDIF
	IF DOES_ENTITY_EXIST(YardGuard[1])
	AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(YardGuard[1])
		DELETE_PED(YardGuard[1])
	ENDIF
	IF DOES_ENTITY_EXIST(hBuddyMichael)
	AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddyMichael)
		DELETE_PED(hBuddyMichael)
	ENDIF	
	IF DOES_ENTITY_EXIST(hBuddyFranklin)
	AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddyFranklin)
		DELETE_PED(hBuddyFranklin)
	ENDIF	
	IF DOES_ENTITY_EXIST(hBuddyTrevor)
	AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddyTrevor)
		DELETE_PED(hBuddyTrevor)
	ENDIF
	IF DOES_ENTITY_EXIST(EndGuard)
	AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(EndGuard)
		DELETE_PED(EndGuard)
	ENDIF	
	
	//Move player here so he is not in any of the vehicles before they are deleted
	IF NOT IS_REPLAY_BEING_SET_UP()
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<519.1906, -2980.9941, 5.0443>>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(), 270.1911)
	ENDIF
	
	//Vehicles
	IF DOES_ENTITY_EXIST(DrillerTrailer)
	AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(DrillerTrailer)
		DELETE_VEHICLE(DrillerTrailer)
	ENDIF	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[3])
		SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[3])
		DELETE_VEHICLE(g_sTriggerSceneAssets.veh[3])
	ENDIF	
	IF DOES_ENTITY_EXIST(DrillerTruck)
	AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(DrillerTruck)
		DELETE_VEHICLE(DrillerTruck)
	ENDIF	
	
//	REMOVE_SCENARIO_BLOCKING_AREA(YardWorkersArea1)
//	REMOVE_SCENARIO_BLOCKING_AREA(YardWorkersArea2)
//	REMOVE_SCENARIO_BLOCKING_AREA(YardWorkersArea3)
//	REMOVE_SCENARIO_BLOCKING_AREA(YardWorkersArea4)
//	REMOVE_SCENARIO_BLOCKING_AREA(endMissionWorkersArea)
//	REMOVE_SCENARIO_BLOCKING_AREA(endMissionWorkersArea2)
	
	REMOVE_ANIM_DICT("cellphone@str")
	REMOVE_ANIM_DICT("misscarsteal4@director_grip")
	
	CANCEL_MUSIC_EVENT("FHPRB_START")
	CANCEL_MUSIC_EVENT("FHPRB_TRUCK")
	CANCEL_MUSIC_EVENT("FHPRB_COPS")
	CANCEL_MUSIC_EVENT("FHPRB_LOST")
	CANCEL_MUSIC_EVENT("FHPRB_STOP")	
	
	CLEAR_AREA(<<889.5, -1553.8, 30>>, 150, TRUE)
	
ENDPROC

//Creates everything required for specific mission stage for skipping
PROC CREATE_ENTITIES_REQUIRED()

	SWITCH missionStage
	
		CASE STAGE_STEAL_DRILLER
		
			doneChat20 = FALSE
			doneChat21 = FALSE
			doneChat22 = FALSE	 	
		
			SETUP_TRUCK_AND_CUTTER()
			
			IF IS_REPLAY_START_VEHICLE_AVAILABLE()
			AND IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(<<7,7,7>>, TRUE)
				REQUEST_REPLAY_START_VEHICLE_MODEL()
				WHILE NOT HAS_REPLAY_START_VEHICLE_LOADED()
					PRINTSTRING("Waiting for replay vehicle model loading") PRINTNL()
					WAIT(0)
				ENDWHILE
				playerStartCar = CREATE_REPLAY_START_VEHICLE(<<818.8139, -1610.4055, 30.7951>>, 264.4120)
			ENDIF
			
			SET_WANTED_LEVEL_MULTIPLIER(0.01)
			
		BREAK
		
		CASE STAGE_TAKE_DRILLER_HOME
		
			doneChat20 = FALSE
			doneChat21 = FALSE
			doneChat22 = FALSE			
			
			SETUP_TRUCK_AND_CUTTER()
			
			IF DOES_ENTITY_EXIST(DrillerTruck)
				IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
					SET_ENTITY_COORDS(DrillerTruck, <<89.2131, -596.9456, 30.6224>>)
					SET_ENTITY_HEADING(DrillerTruck, 161.1163)
				ENDIF
			ENDIF
			
			CLEAR_AREA(<<41, -615, 30>>, 50, TRUE, TRUE)
			
		BREAK
		
		CASE STAGE_CLOSING_CUTSCENE
			
			doneChat20 = TRUE
			doneChat21 = TRUE
			doneChat22 = TRUE			
			
			SETUP_TRUCK_AND_CUTTER()
			
			CLEAR_AREA(<<41, -615, 30>>, 50, TRUE, TRUE)
			
			IF DOES_ENTITY_EXIST(DrillerTruck)
				IF IS_VEHICLE_DRIVEABLE(DrillerTruck)			
					SET_ENTITY_COORDS(DrillerTruck, <<28.4065, -608.9371, 30.6293>>)
					SET_ENTITY_HEADING(DrillerTruck, 70.7778)
				ENDIF
			ENDIF
			//Create the guard when player is within range of the car park
			REQUEST_MODEL(S_M_M_Security_01)
			WHILE NOT HAS_MODEL_LOADED(S_M_M_Security_01)
				PRINTSTRING("waiting for model S_M_M_Security_01 loading") PRINTNL()
				WAIT(0)
			ENDWHILE
			IF NOT DOES_ENTITY_EXIST(EndGuard)
				EndGuard = CREATE_PED(PEDTYPE_MISSION, S_M_M_Security_01, <<64.9040, -617.9949, 30.7028>>, 223.5550)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_Security_01)
				SET_PED_CONFIG_FLAG(EndGuard, PCF_DontBehaveLikeLaw, TRUE)
			ENDIF
			
		BREAK
		
	ENDSWITCH	

ENDPROC

//PURPOSE: Sets up the player at correct coords for skipping
PROC SET_PLAYER_COORDS()	

	SWITCH missionStage
	
		CASE STAGE_GET_TO_DRILLER
		
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<856.9501, -1573.1766, 29.4751>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 288.3586)		
		
		BREAK	
	
		CASE STAGE_STEAL_DRILLER
		
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<856.9501, -1573.1766, 29.4751>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 288.3586)
		
		BREAK
		
		CASE STAGE_TAKE_DRILLER_HOME
		
			IF DOES_ENTITY_EXIST(DrillerTruck)
				IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DrillerTruck)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), DrillerTruck)
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE STAGE_CLOSING_CUTSCENE
		
			IF DOES_ENTITY_EXIST(DrillerTruck)
				IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DrillerTruck)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), DrillerTruck)
					ENDIF
				ENDIF
			ENDIF		
		
		BREAK
		
	ENDSWITCH

ENDPROC

//Handles the skipping to stages.
PROC STAGE_SELECTOR_MISSION_SETUP()

	missionCanFail = FALSE
	endguardTaskGiven = FALSE

	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())

	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(0)
	ENDIF

	KILL_ANY_CONVERSATION()
	
	CLEAR_PRINTS()
	
	REMOVE_BLIPS()
	
	DELETE_ALL_ENTITIES()	
	
	CREATE_ENTITIES_REQUIRED()
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		SET_PLAYER_COORDS()	
	ENDIF
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)

ENDPROC

//PURPOSE: Does opening cutscene
PROC DO_STAGE_OPENING_CUTSCENE()

	//No cutscene at the moment so skip to next stage
	IF NOT IS_REPLAY_IN_PROGRESS()
		//DO OPENING CUTSCENE
		missionStage = STAGE_INIT_MISSION
	ELSE
		missionStage = STAGE_INIT_MISSION
	ENDIF

ENDPROC

//PURPOSE: Initialises mission 
PROC DO_STAGE_INIT_MISSION()
	
	SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
	SET_WANTED_LEVEL_MULTIPLIER(0.01)
	
	YardWorkersArea1 = ADD_SCENARIO_BLOCKING_AREA(<<861.6, -1599.7, 27.9>>, <<961.8, -1476.4, 49.7>>)
	YardWorkersArea2 = ADD_SCENARIO_BLOCKING_AREA(<<851.2, -1598.2, 29.7>>, <<855.7, -1587.1, 33>>)	
	YardWorkersArea3 = ADD_SCENARIO_BLOCKING_AREA(<<859.4, -1569.6, 32.3>>, <<865.4, -1561.0, 28.7>>)	
	YardWorkersArea4 = ADD_SCENARIO_BLOCKING_AREA(<<939.0, -1576.8, 33>>, <<943.8, -1571.3, 28.9>>)	
	endMissionWorkersArea = ADD_SCENARIO_BLOCKING_AREA(<<5.4, -588, 39.1>>, <<57.4, -678.8, 25.4>>)
	endMissionWorkersArea2 = ADD_SCENARIO_BLOCKING_AREA(<<50.9, -641.6, 37.7>>, <<81.1, -608.4, 29.5>>)		
	
	//Check if mission is on replay. If so is it being shit skipped or not. Move mission to correct checkpoint. If not on replay initialise mission and start from 1st stage.
	IF IS_REPLAY_IN_PROGRESS()
		//Check if player chose to shitskip
        IF g_bShitskipAccepted = TRUE
			START_REPLAY_SETUP(<<28.4065, -608.9371, 30.6293>>, 70.7778)
			PlayerSpotted = TRUE
			MissionStageBeingSkippedTo = TRUE
			iControlFlag = 0
			missionStage = STAGE_CLOSING_CUTSCENE
        ELSE
			IF GET_REPLAY_MID_MISSION_STAGE() = 0
				IF IS_REPLAY_START_VEHICLE_AVAILABLE()
				AND IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(<<7,7,7>>, TRUE)
					START_REPLAY_SETUP(<<818.8139, -1610.4055, 30.7951>>, 264.4120)
				ELSE
					START_REPLAY_SETUP(<<825.5603, -1605.8247, 30.9548>>, 312.5221)
				ENDIF
				MissionStageBeingSkippedTo = TRUE
				PlayerSpotted = TRUE
				iControlFlag = 0
				missionStage = STAGE_STEAL_DRILLER
			ENDIF			
		ENDIF
	ELSE
		IF IS_REPEAT_PLAY_ACTIVE()
			MissionStageBeingSkippedTo = TRUE
			PlayerSpotted = TRUE
			iControlFlag = 0
			missionStage = STAGE_STEAL_DRILLER	
		ELSE
			//Initialise mission for start
			iControlFlag = 0
			missionStage = STAGE_GET_TO_DRILLER
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Handles the stage where the player has to get to the driller
PROC DO_STAGE_GET_TO_DRILLER()

	IF iControlFlag = 0
		
		IF MissionStageBeingSkippedTo = TRUE
			STAGE_SELECTOR_MISSION_SETUP()
			MissionStageBeingSkippedTo = FALSE
		ENDIF				
		
		SET_START_CHECKPOINT_AS_FINAL()
		
		SETUP_TRUCK_AND_CUTTER()
		
		IF NOT IS_SCREEN_FADED_IN()
			LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		//Set the players start car as a car gen.
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[3])
			PRINTSTRING("g_sTriggerSceneAssets.veh[3] exists") PRINTNL()
			IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[3])
				IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(g_sTriggerSceneAssets.veh[3])) 
					SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[3], TRUE, TRUE)
					SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<1.0095, -631.9655, 34.7259>>, 349.0681)
					PRINTSTRING("SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN being called as start car is a heli") PRINTNL()
				ENDIF
				IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(g_sTriggerSceneAssets.veh[3]))
				OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(g_sTriggerSceneAssets.veh[3]))
				OR IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(g_sTriggerSceneAssets.veh[3]))
				OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(g_sTriggerSceneAssets.veh[3]))
					SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[3], TRUE, TRUE)
					SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<49.0052, -599.5534, 30.6299>>, 158.5544)
					PRINTSTRING("SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN being called for start car") PRINTNL()
				ENDIF
			ENDIF
		ELSE
			PRINTSTRING("g_sTriggerSceneAssets.veh[2] Doesn't exist") PRINTNL()
		ENDIF
		
		MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_FINALE_PREP_B)
		
		missionCanFail = TRUE
		
		iControlFlag = 1
	
	ENDIF
	
	IF iControlFlag = 1
		
		iControlFlag = 0
		missionStage = STAGE_STEAL_DRILLER
	
	ENDIF
	
ENDPROC

//PROC HANDLE_ALL_ENEMY_BLIPS()
//
//	IF DOES_ENTITY_EXIST(DrillerTruck)
//		IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
//			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DrillerTruck)
//				IF IS_VEHICLE_ATTACHED_TO_TRAILER(DrillerTruck)
//					FOR icount = 0 TO 6
//						IF DOES_ENTITY_EXIST(EnemyPed[icount])
//							IF NOT IS_PED_INJURED(EnemyPed[icount])
//								IF IS_PED_IN_ANY_VEHICLE(EnemyPed[icount])
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed[icount]) < 200
//										IF NOT DOES_BLIP_EXIST(EnemyPedBlip[icount])
//											EnemyPedBlip[icount] = CREATE_BLIP_FOR_PED(EnemyPed[icount], TRUE)
//										ENDIF
//									ELSE
//										IF DOES_BLIP_EXIST(EnemyPedBlip[icount])
//											REMOVE_BLIP(EnemyPedBlip[icount])
////											PRINTSTRING("removing blip ") PRINTINT(icount) PRINTNL()
//										ENDIF
//									ENDIF
//								ELSE
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyPed[icount]) < 100
//										IF NOT DOES_BLIP_EXIST(EnemyPedBlip[icount])
//											EnemyPedBlip[icount] = CREATE_BLIP_FOR_PED(EnemyPed[icount], TRUE)
//										ENDIF
//									ELSE
//										IF DOES_BLIP_EXIST(EnemyPedBlip[icount])
//											REMOVE_BLIP(EnemyPedBlip[icount])
////											PRINTSTRING("removing blip ") PRINTINT(icount) PRINTNL()
//										ENDIF
//									ENDIF
//								ENDIF
//							ELSE
//								IF DOES_BLIP_EXIST(EnemyPedBlip[icount])
//									REMOVE_BLIP(EnemyPedBlip[icount])
////									PRINTSTRING("removing blip ") PRINTINT(icount) PRINTNL()
//								ENDIF
//							ENDIF
//						ELSE
//							IF DOES_BLIP_EXIST(EnemyPedBlip[icount])
//								REMOVE_BLIP(EnemyPedBlip[icount])
////								PRINTSTRING("removing blip ") PRINTINT(icount) PRINTNL()
//							ENDIF
//						ENDIF
//					ENDFOR
//					IF blipsHaveBeenAdded = FALSE
//						blipsHaveBeenAdded = TRUE
//					ENDIF
//				ELSE
//					FOR icount = 0 TO 6
//						IF DOES_BLIP_EXIST(EnemyPedBlip[icount])
//							REMOVE_BLIP(EnemyPedBlip[icount])
////							PRINTSTRING("removing blip ") PRINTINT(icount) PRINTNL()
//						ENDIF
//					ENDFOR
//					IF blipsHaveBeenAdded = TRUE
//						blipsHaveBeenAdded = FALSE
//					ENDIF
//				ENDIF
//			ELSE
//				FOR icount = 0 TO 6
//					IF DOES_BLIP_EXIST(EnemyPedBlip[icount])
//						REMOVE_BLIP(EnemyPedBlip[icount])
////						PRINTSTRING("removing blip ") PRINTINT(icount) PRINTNL()
//					ENDIF
//				ENDFOR
//				IF blipsHaveBeenAdded = TRUE
//					blipsHaveBeenAdded = FALSE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
//ENDPROC

PROC HANDLE_ALL_OTHER_BLIPS()

	IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DrillerTruck)
			IF IS_VEHICLE_ATTACHED_TO_TRAILER(DrillerTruck)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_1")
					CLEAR_HELP()
				ENDIF
				IF DOES_BLIP_EXIST(DrillerBlip)
					REMOVE_BLIP(DrillerBlip)
				ENDIF
				IF DOES_BLIP_EXIST(TrailerBlip)
					IF DoneGod6Text = TRUE
						IF GET_GAME_TIMER() < (iattachTextTimer + DEFAULT_GOD_TEXT_TIME)
							CLEAR_PRINTS()
						ENDIF
					ENDIF
					REMOVE_BLIP(TrailerBlip)
				ENDIF
//				//Blip enemies that are close enough
//				IF missionStage = STAGE_STEAL_DRILLER
//					IF blipsHaveBeenAdded = TRUE
//						IF DOES_BLIP_EXIST(EnemyPedBlip[0])
//						OR DOES_BLIP_EXIST(EnemyPedBlip[1])
//						OR DOES_BLIP_EXIST(EnemyPedBlip[2])
//						OR DOES_BLIP_EXIST(EnemyPedBlip[3])
//						OR DOES_BLIP_EXIST(EnemyPedBlip[4])
//						OR DOES_BLIP_EXIST(EnemyPedBlip[5])
//							//Update god text
//							IF doneLoseWorkersText = FALSE
//								PRINT_NOW("GOD_3", DEFAULT_GOD_TEXT_TIME, -1)//~s~Lose the ~r~workers.
//								doneLoseWorkersText = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
				//Blip the bank car park
				IF missionStage = STAGE_TAKE_DRILLER_HOME
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						IF musicChanged = FALSE
							CANCEL_MUSIC_EVENT("FHPRB_COPS")
							TRIGGER_MUSIC_EVENT("FHPRB_LOST")
							musicChanged = TRUE
						ENDIF
						IF doneLoseCopsText = TRUE
							IF GET_GAME_TIMER() < (iLoseCopsTimer + DEFAULT_GOD_TEXT_TIME)
								CLEAR_PRINTS()
							ENDIF
							doneLoseCopsText = FALSE
						ENDIF
						IF iControlFlag > 3
							IF NOT DOES_BLIP_EXIST(HomeBlip)
								HomeBlip = CREATE_BLIP_FOR_COORD(vHomeCoords, TRUE)
							ENDIF
							IF doneGodText = FALSE
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
									PRINT_NOW("GOD_2", DEFAULT_GOD_TEXT_TIME, 1)//~s~Go to the ~y~parking lot.~s~
									doneGodText = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(HomeBlip)
							REMOVE_BLIP(HomeBlip)
						ENDIF
						IF doneLoseCopsText = FALSE
							CLEAR_PRINTS()
							PRINT_NOW("GOD_4", DEFAULT_GOD_TEXT_TIME, -1)//~s~Lose the cops.~s~
							TRIGGER_MUSIC_EVENT("FHPRB_COPS")
							iLoseCopsTimer = GET_GAME_TIMER()
							doneLoseCopsText = TRUE
							musicChanged = FALSE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(DrillerBlip)
					REMOVE_BLIP(DrillerBlip)
				ENDIF
				IF DOES_BLIP_EXIST(HomeBlip)
					REMOVE_BLIP(HomeBlip)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(DrillerTrailer)
					IF NOT DOES_BLIP_EXIST(TrailerBlip)
						TrailerBlip = CREATE_BLIP_FOR_ENTITY(DrillerTrailer)
						SET_BLIP_AS_FRIENDLY(TrailerBlip, TRUE)
						IF IS_MESSAGE_BEING_DISPLAYED()
							CLEAR_PRINTS()
						ENDIF
						IF DoneGod6Text = FALSE
							PRINT_NOW("GOD_6", DEFAULT_GOD_TEXT_TIME, -1)//~s~Re-attach the ~b~trailer.
							iattachTextTimer = GET_GAME_TIMER()
							DoneGod6Text = TRUE
						ENDIF
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_1")
							PRINT_HELP_FOREVER("HELP_1")
							//~s~Line up the back of the truck to the front of the trailer to re-attach.
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(TrailerBlip)
				REMOVE_BLIP(TrailerBlip)
			ENDIF
			IF DOES_BLIP_EXIST(HomeBlip)
				REMOVE_BLIP(HomeBlip)
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
				IF NOT DOES_BLIP_EXIST(DrillerBlip)
					DrillerBlip = CREATE_BLIP_FOR_ENTITY(DrillerTruck)
					SET_BLIP_AS_FRIENDLY(DrillerBlip, TRUE)
				ENDIF
			ENDIF		
			IF doneGetBackInText = FALSE
				CLEAR_PRINTS()
				PRINT_NOW("GOD_5", DEFAULT_GOD_TEXT_TIME, -1)//~s~Get back in the ~b~truck.
				doneGetBackInText = TRUE
			ENDIF
		ENDIF
	ENDIF

ENDPROC

//PURPOSE: Handles stage where the player has to steal the driller.
PROC DO_STAGE_STEAL_DRILLER()
	
//	IF eWorkerReaction <> IGNORES_PLAYER
//	OR eYardGuardMood <> NOT_BOTHERED_BY_PLAYER
//	OR blipsHaveBeenAdded = TRUE
//		HANDLE_ALL_ENEMY_BLIPS()
//	ENDIF
	
	IF iControlFlag > 1
		HANDLE_ALL_OTHER_BLIPS()
	ENDIF
	
	IF iControlFlag = 0
	
		IF MissionStageBeingSkippedTo = TRUE
			STAGE_SELECTOR_MISSION_SETUP()
			MissionStageBeingSkippedTo = FALSE
		ENDIF		
	
		//Flags
		playerWarnedChat 			= FALSE
		GuardsShouldCombat			= FALSE
		YardGuard0Closest			= FALSE
		YardGuard1Closest			= FALSE
		Chat16Done 					= FALSE
		Chat15Done					= FALSE
		doneChat13					= FALSE
		yardguardIsAlive[0]			= FALSE
		yardguardIsAlive[1]			= FALSE
		playerHasBeenWarnedBefore[0]= FALSE
		playerHasBeenWarnedBefore[1]= FALSE
		doneChat17 					= FALSE
		doneChat18					= FALSE
		Player2StarGiven 			= FALSE
		meleeTimerSet 				= FALSE
		GuardsCombatTimerSet 		= FALSE
		taskForPed4Given			= FALSE
		doneChat19					= FALSE
		moveTimerSet 				= FALSE
		worker6TaskGiven			= FALSE
		worker6TimerStarted			= FALSE
		worker6Task2Given			= FALSE
		WorkerSpottedPlayerFirst	= FALSE
		playerSpottedOnRoof			= FALSE
		doneChat23 					= FALSE
		doneChat24					= FALSE
		GuardsAlerted				= FALSE
		doneChat25	 				= FALSE
		TaskLookAtGiven[0]			= FALSE
		TaskLookAtGiven[1]			= FALSE
		TaskGoToEntity[0]			= FALSE
		TaskGoToEntity[1]			= FALSE
		TaskNavmeshToPed[0]			= FALSE	
		TaskNavmeshToPed[1]			= FALSE	
		combatTaskGiven[0]			= FALSE
		combatTaskGiven[1]			= FALSE
		combatSeeTimerSet[0]		= FALSE
		combatSeeTimerSet[1]		= FALSE
		followNavForCombat[0]		= FALSE	
		followNavForCombat[1]		= FALSE	
		turnPedTask[0]				= FALSE
		turnPedTask[1]				= FALSE
		doneChat27 					= FALSE
		copsHaveArrived 			= FALSE
		TaskToGetBackToGuardCoord[0]= FALSE
		StandGuardTaskGiven[0]		= FALSE
		TaskToGetBackToGuardCoord[1]= FALSE
		StandGuardTaskGiven[1]		= FALSE
		ped5CombatTaskGiven 		= FALSE
		ped5PhonePolice 			= FALSE
		ped0TaskGiven				= FALSE
		deadBodyFound				= FALSE
		
		doneGetBackInText 			= FALSE
		doneLoseWorkersText	 		= FALSE
		weaponTimerStarted 			= FALSE
		randomScreamTimerSet 		= FALSE
		startedChat12 				= FALSE
		doneChat10 					= FALSE	
		PlayerIsStealingTruck		= FALSE	
		DoneChat8 					= FALSE
		DoneCuriousChat 			= FALSE
//		DoneChat7 					= FALSE
		wantedLevelSet 				= FALSE
		wantedLevelTimerStarted 	= FALSE
		DoneGod6Text 				= FALSE
		doneLeaveYardText 			= FALSE
//		doneChat26 					= FALSE
		doneGod1 					= FALSE
		PlayerSpotted				= FALSE
		chatPed0 					= FALSE
		GuardsFightingTalkStarted 	= FALSE
		iattachTextTimer			= 0
		iLoseCopsTimer				= 0
		
		FOR icount = 0 TO 6
	 		FleeTaskGiven[icount] 			= FALSE
//			blockedEvents[icount]			= FALSE
			blockedEventsForHate[icount]	= FALSE
			SmartFleeTaskGiven[icount]		= FALSE
			handsUpTaskGiven[icount]		= FALSE
			attackPlayerTaskGiven[icount]	= FALSE
		ENDFOR
		
		REQUEST_ANIM_DICT("cellphone@str")
		REQUEST_ANIM_DICT("misscarsteal4@director_grip")		
		
		IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
			IF NOT DOES_BLIP_EXIST(DrillerBlip)
				DrillerBlip = CREATE_BLIP_FOR_ENTITY(DrillerTruck)
//				SET_BLIP_SPRITE(DrillerBlip, RADAR_TRACE_DRILL)
				SET_BLIP_AS_FRIENDLY(DrillerBlip, TRUE)
			ENDIF
		ENDIF
		
		//don't allow the player to be able to get awanted level just now. so I can control when he gets one.
		SET_MAX_WANTED_LEVEL(0)
		
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			IF DOES_ENTITY_EXIST(playerStartCar)
				END_REPLAY_SETUP(playerStartCar)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(playerStartCar)
			ELSE
				END_REPLAY_SETUP()
			ENDIF
		ENDIF
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		TRIGGER_MUSIC_EVENT("FHPRB_START")
		
		CLEAR_AREA(<<43.5, -615.8, 32.4>>, 50, TRUE)	
		
		missionCanFail = TRUE
		iControlFlag = 1
		
	ENDIF
	
	IF iControlFlag = 1
	
		IF doneGod1 = FALSE
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
				PRINT_NOW("GOD_1", DEFAULT_GOD_TEXT_TIME, 1)//~s~Steal the ~b~cutter.~s~
				doneGod1 = TRUE
			ENDIF	
		ENDIF
			
		IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DrillerTruck)
				TRIGGER_MUSIC_EVENT("FHPRB_TRUCK")
				IF DOES_BLIP_EXIST(DrillerBlip)
					REMOVE_BLIP(DrillerBlip)
				ENDIF
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FHPB_TRUCK_TIME) 
				SET_WANTED_LEVEL_MULTIPLIER(1)
				iControlFlag = 2
			ENDIF
		ENDIF	

	ENDIF
	
	IF iControlFlag = 2
		
		IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DrillerTruck)
				IF IS_VEHICLE_ATTACHED_TO_TRAILER(DrillerTruck)
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_1")
						CLEAR_HELP()
					ENDIF
					IF DOES_BLIP_EXIST(DrillerBlip)
						REMOVE_BLIP(DrillerBlip)
					ENDIF
					IF DOES_BLIP_EXIST(TrailerBlip)
						REMOVE_BLIP(TrailerBlip)
					ENDIF
					//Move stage on once all the enemy blips have been removed
//					IF blipsHaveBeenAdded = TRUE
//						IF NOT DOES_BLIP_EXIST(EnemyPedBlip[0])
//						AND NOT DOES_BLIP_EXIST(EnemyPedBlip[1])
//						AND NOT DOES_BLIP_EXIST(EnemyPedBlip[2])
//						AND NOT DOES_BLIP_EXIST(EnemyPedBlip[3])
//						AND NOT DOES_BLIP_EXIST(EnemyPedBlip[4])
//						AND NOT DOES_BLIP_EXIST(EnemyPedBlip[5])
//						AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
//							iControlFlag = 0
//							missionStage = STAGE_TAKE_DRILLER_HOME
//						ELSE
//							//Update god text
//							IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <> 0
//								IF doneLoseWorkersText = FALSE
//									PRINT_NOW("GOD_4", DEFAULT_GOD_TEXT_TIME, -1)//~s~Lose the cops.~s~
//									doneLoseWorkersText = TRUE
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
					IF GET_DISTANCE_BETWEEN_COORDS(vplayerCoords, vMiddleOfYard) < 100
						//Update god text
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0	
							IF doneLoseWorkersText = TRUE
								IF GET_GAME_TIMER() < (iLoseCopsTimer + DEFAULT_GOD_TEXT_TIME)
									CLEAR_PRINTS()
									doneLoseWorkersText = FALSE
								ENDIF
							ENDIF
							IF doneLeaveYardText = FALSE
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
								PRINT_NOW("GOD_7", DEFAULT_GOD_TEXT_TIME, -1)//~s~Get the driller out of the yard.
								doneLeaveYardText = TRUE
							ELSE
//								IF doneChat26 = FALSE
//									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//										IF NOT IS_MESSAGE_BEING_DISPLAYED()
//										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//											IF IS_WORKER_ADDED_TO_DIALOGUE()
//												IF CREATE_CONVERSATION(MyLocalPedStruct, "FHPBAUD", "FHPB_CHAT26", CONV_PRIORITY_MEDIUM)
//													//That's the drill getting taken away already.
//													doneChat26 = TRUE
//												ENDIF
//											ELSE
//												PRINTSTRING("Worker not available for chat") PRINTNL()
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
							ENDIF
						ELSE
							IF doneLoseWorkersText = FALSE
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
								PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_BS_PREP_B_01", 0.0)
								iLoseCopsTimer = GET_GAME_TIMER()
								PRINT_NOW("GOD_4", DEFAULT_GOD_TEXT_TIME, -1)//~s~Lose the cops.~s~
								doneLoseWorkersText = TRUE
							ENDIF
						ENDIF
					ELSE
						//move stage on if player is out of the yard and no wanted level
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0	
							iControlFlag = 0
							missionStage = STAGE_TAKE_DRILLER_HOME
						ENDIF
					ENDIF
//					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(DrillerBlip)
						REMOVE_BLIP(DrillerBlip)
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(DrillerTrailer)
						IF NOT DOES_BLIP_EXIST(TrailerBlip)
							TrailerBlip = CREATE_BLIP_FOR_ENTITY(DrillerTrailer)
							SET_BLIP_AS_FRIENDLY(TrailerBlip, TRUE)
							IF IS_MESSAGE_BEING_DISPLAYED()
								CLEAR_PRINTS()
							ENDIF
							IF DoneGod6Text = FALSE
								PRINT_NOW("GOD_6", DEFAULT_GOD_TEXT_TIME, -1)//~s~Re-attach the ~b~trailer.
								DoneGod6Text = TRUE
							ENDIF
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP_1")
								PRINT_HELP_FOREVER("HELP_1")
								//~s~Line up the back of the truck to the front of the trailer to re-attach.
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(TrailerBlip)
					REMOVE_BLIP(TrailerBlip)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
					IF NOT DOES_BLIP_EXIST(DrillerBlip)
						DrillerBlip = CREATE_BLIP_FOR_ENTITY(DrillerTruck)
						SET_BLIP_AS_FRIENDLY(DrillerBlip, TRUE)
					ENDIF
				ENDIF		
				IF doneGetBackInText = FALSE
					CLEAR_PRINTS()
					PRINT_NOW("GOD_5", DEFAULT_GOD_TEXT_TIME, -1)//~s~Get back in the ~b~truck.
					doneGetBackInText = TRUE
				ENDIF
			ENDIF
		ENDIF
					
	ENDIF

ENDPROC

//PURPOSE: Handles stage where the player has to take the driller to location
PROC DO_STAGE_TAKE_DRILLER_HOME()

	HANDLE_ALL_OTHER_BLIPS()

	IF iControlFlag = 0
	
		IF MissionStageBeingSkippedTo = TRUE
			STAGE_SELECTOR_MISSION_SETUP()
			MissionStageBeingSkippedTo 	= FALSE
		ENDIF		
		
		//Flags
		doneGodText 				= FALSE
		doneLoseCopsText 			= FALSE
		musicChanged 				= FALSE
		iattachTextTimer			= 0
		iLoseCopsTimer				= 0
		
		SET_MAX_WANTED_LEVEL(5)
		
		REMOVE_ANIM_DICT("cellphone@str")
		REMOVE_ANIM_DICT("misscarsteal4@director_grip")
		
		IF NOT IS_SCREEN_FADED_IN()
			LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		SET_WANTED_LEVEL_MULTIPLIER(1)
		
		//Add in a video recording of the last 10 seconds of action involving the player getting into the heli.
		REPLAY_RECORD_BACK_FOR_TIME(10.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)		
		
		missionCanFail = TRUE
		iControlFlag = 1
		
	ENDIF
	
	IF iControlFlag = 1
		
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "LESTER")
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
		ENDIF
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
		ENDIF
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF
		
		iControlFlag = 2
	ENDIF
	
	IF iControlFlag = 2
		
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_LESTER, "FHPBAUD", "FHPB_CHAT1", CONV_PRIORITY_MEDIUM)
					//Talk to me.
					//We've got the drill, now what?
					//Good, take it to the car park at the bank. I know a guy that will open the gates for you.
					//Ok, will call you once we're there.					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)									
					SETTIMERA(0)
					iControlFlag = 3
				ENDIF
			ENDIF
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				IF PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_LESTER, "FHPBAUD", "FHPB_CHAT3", CONV_PRIORITY_MEDIUM)
					//Talk to me.
					//I've got the drill, where am I taking it?
					//Good, take it to the car park at the bank, I know a guy that will open the gates for you.
					//Ok, call you when we get there.	
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)		
					SETTIMERA(0)
					iControlFlag = 3
				ENDIF
			ENDIF
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				IF PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_LESTER, "FHPBAUD", "FHPB_CHAT2", CONV_PRIORITY_MEDIUM)
					//Talk to me.
					//We got the drill, what the fuck now?
					//Good, take it to the car park at the bank, I know a guy that will open the gates for you.
					//You know a guy? Bla Bla... I'll open the gates myself if I have to.	
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)		
					SETTIMERA(0)
					iControlFlag = 3
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	IF iControlFlag = 3
		
//		PRINTSTRING("timer a = ") PRINTINT(TIMERA()) PRINTNL()
		IF NOT DOES_BLIP_EXIST(HomeBlip)
			IF NOT HAS_CELLPHONE_CALL_FINISHED()
				IF TIMERA() > 10500
					HomeBlip = CREATE_BLIP_FOR_COORD(vHomeCoords, TRUE)
				ENDIF
			ENDIF
		ENDIF
		
		IF HAS_CELLPHONE_CALL_FINISHED()
			iControlFlag = 4
		ENDIF
	
	ENDIF
	
	IF iControlFlag = 4
		
		//Move stage on once they arrive at the bank car park
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DrillerTruck)
					IF IS_VEHICLE_ATTACHED_TO_TRAILER(DrillerTruck)
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vHomeCoords, <<4,4,LOCATE_SIZE_HEIGHT>>, TRUE, TRUE, TM_IN_VEHICLE)
							IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 4, 3, 0.5)
								TRIGGER_MUSIC_EVENT("FHPRB_STOP")
								IF DOES_BLIP_EXIST(HomeBlip)
									REMOVE_BLIP(HomeBlip)
								ENDIF	
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)		
								INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
								iControlFlag = 0
								missionStage = STAGE_CLOSING_CUTSCENE
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

//PURPOSE: Handles the closing cutscene for the mission.
PROC DO_STAGE_CLOSING_CUTSCENE()

	MONITOR_END_GUARD()

	IF iControlFlag = 0
	
		//For Stage Selector
		IF MissionStageBeingSkippedTo = TRUE
			STAGE_SELECTOR_MISSION_SETUP()
			MissionStageBeingSkippedTo = FALSE
		ENDIF
		
		//Flags
		buddy1NotAvailable 				= FALSE
		buddy2NotAvailable 				= FALSE
		buddyReleasedAndOutTruck 		= FALSE
		
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
		//Take control of the battle buddies 
		IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//			PRINTSTRING("hBuddyMichael is avialable") PRINTNL()
			IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddyMichael)
//				PRINTSTRING("hBuddyMichael is not overriden") PRINTNL()
				WHILE NOT OVERRIDE_BATTLEBUDDY(hBuddyMichael, FALSE)
//					PRINTSTRING("Waiting on overriding hBuddyMichael buddy") PRINTNL()
					WAIT(0)
				ENDWHILE
			ENDIF
		ENDIF
		IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//			PRINTSTRING("hBuddyTrevor is avialable") PRINTNL()
			IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddyTrevor)
//				PRINTSTRING("hBuddyTrevor is not overriden") PRINTNL()
				WHILE NOT OVERRIDE_BATTLEBUDDY(hBuddyTrevor, FALSE)
//					PRINTSTRING("Waiting on overriding hBuddyTrevor buddy") PRINTNL()
					WAIT(0)
				ENDWHILE
			ENDIF
		ENDIF
		IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//			PRINTSTRING("hBuddyFranklin is avialable") PRINTNL()
			IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddyFranklin)
//				PRINTSTRING("hBuddyFranklin is not overriden") PRINTNL()
				WHILE NOT OVERRIDE_BATTLEBUDDY(hBuddyFranklin, FALSE)
//					PRINTSTRING("Waiting on overriding hBuddyFranklin buddy") PRINTNL()
					WAIT(0)
				ENDWHILE
			ENDIF
		ENDIF		
		
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			END_REPLAY_SETUP(DrillerTruck)
		ENDIF
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		missionCanFail = TRUE
		iControlFlag = 1	
	
	ENDIF
	
//	IF iControlFlag = 1
//	
//		IF NOT DOES_CAM_EXIST(EndSceneCam)
//			EndSceneCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//			SET_CAM_PARAMS(EndSceneCam,<<28.783087,-597.357056,42.201782>>,<<-23.232971,-0.000000,178.175674>>,41.958347)
//		ENDIF
//		
//		DISPLAY_RADAR(FALSE)
//		DISPLAY_HUD(FALSE)
//		RENDER_SCRIPT_CAMS(TRUE, FALSE)	
//		iControlFlag = 2
//	
//	ENDIF
	
	IF iControlFlag = 1
	
		//Give the peds in the heli tasks to leave the heli
		IF DOES_ENTITY_EXIST(DrillerTruck)
			IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
				//Make buddies exit vehicle
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
						IF IS_PED_IN_VEHICLE(hBuddyTrevor, DrillerTruck)
							CLEAR_PED_TASKS(hBuddyTrevor)
							REMOVE_PED_FROM_GROUP(hBuddyTrevor)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyTrevor, TRUE)
							TASK_LEAVE_VEHICLE(hBuddyTrevor, DrillerTruck)
							PRINTSTRING("TASK_LEAVE_VEHICLE(hBuddyTrevor, HeliToSteal)") PRINTNL()
						ENDIF
					ENDIF
					IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
						IF IS_PED_IN_VEHICLE(hBuddyFranklin, DrillerTruck)
							CLEAR_PED_TASKS(hBuddyFranklin)
							REMOVE_PED_FROM_GROUP(hBuddyFranklin)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyFranklin, TRUE)
							TASK_LEAVE_VEHICLE(hBuddyFranklin, DrillerTruck)
							PRINTSTRING("TASK_LEAVE_VEHICLE(hBuddyFranklin, HeliToSteal)") PRINTNL()
						ENDIF
					ENDIF
				ENDIF
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
						IF IS_PED_IN_VEHICLE(hBuddyMichael, DrillerTruck)
							CLEAR_PED_TASKS(hBuddyMichael)
							REMOVE_PED_FROM_GROUP(hBuddyMichael)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyMichael, TRUE)
							TASK_LEAVE_VEHICLE(hBuddyMichael, DrillerTruck)
							PRINTSTRING("TASK_LEAVE_VEHICLE(hBuddyMichael, HeliToSteal)") PRINTNL()
						ENDIF
					ENDIF
					IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
						IF IS_PED_IN_VEHICLE(hBuddyFranklin, DrillerTruck)
							CLEAR_PED_TASKS(hBuddyFranklin)
							REMOVE_PED_FROM_GROUP(hBuddyFranklin)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyFranklin, TRUE)
							TASK_LEAVE_VEHICLE(hBuddyFranklin, DrillerTruck)
							PRINTSTRING("TASK_LEAVE_VEHICLE(hBuddyFranklin, HeliToSteal)") PRINTNL()
						ENDIF
					ENDIF
				ENDIF	
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
						IF IS_PED_IN_VEHICLE(hBuddyMichael, DrillerTruck)
							CLEAR_PED_TASKS(hBuddyMichael)
							REMOVE_PED_FROM_GROUP(hBuddyMichael)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyMichael, TRUE)
							TASK_LEAVE_VEHICLE(hBuddyMichael, DrillerTruck)
							PRINTSTRING("TASK_LEAVE_VEHICLE(hBuddyMichael, HeliToSteal)") PRINTNL()
						ENDIF
					ENDIF
					IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
						IF IS_PED_IN_VEHICLE(hBuddyTrevor, DrillerTruck)
							CLEAR_PED_TASKS(hBuddyTrevor)
							REMOVE_PED_FROM_GROUP(hBuddyTrevor)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyTrevor, TRUE)
							TASK_LEAVE_VEHICLE(hBuddyTrevor, DrillerTruck)
							PRINTSTRING("TASK_LEAVE_VEHICLE(hBuddyTrevor, HeliToSteal)") PRINTNL()
						ENDIF
					ENDIF
				ENDIF	
				//Make player exit vehicle
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DrillerTruck)
					TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), DrillerTruck)
					PRINTSTRING("TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), HeliToSteal)") PRINTNL()
				ENDIF
			ENDIF
		ENDIF
		
		SETTIMERB(0)
		icontrolFlag = 2		
	
	ENDIF
	
	IF icontrolFlag = 2
	
		//Move scene on and give player control once both player and buddy are out of the truck
		IF DOES_ENTITY_EXIST(DrillerTruck)
			IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
				//Set flag once buddy is out of vehicle 
				IF buddyReleasedAndOutTruck = FALSE
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
							IF NOT IS_PED_IN_VEHICLE(hBuddyTrevor, DrillerTruck)
								IF IS_BATTLEBUDDY_OVERRIDDEN(hBuddyTrevor)
									IF RELEASE_BATTLEBUDDY(hBuddyTrevor)	
										buddyReleasedAndOutTruck = TRUE
									ENDIF
								ELSE
									buddyReleasedAndOutTruck = TRUE
								ENDIF
							ENDIF
						ELSE
							buddy1NotAvailable = TRUE
						ENDIF
						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
							IF NOT IS_PED_IN_VEHICLE(hBuddyFranklin, DrillerTruck)
								IF IS_BATTLEBUDDY_OVERRIDDEN(hBuddyFranklin)
									IF RELEASE_BATTLEBUDDY(hBuddyFranklin)	
										buddyReleasedAndOutTruck = TRUE
									ENDIF
								ELSE
									buddyReleasedAndOutTruck = TRUE
								ENDIF
							ENDIF
						ELSE
							buddy2NotAvailable = TRUE
						ENDIF
					ENDIF
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
							IF NOT IS_PED_IN_VEHICLE(hBuddyMichael, DrillerTruck)
								IF IS_BATTLEBUDDY_OVERRIDDEN(hBuddyMichael)
									IF RELEASE_BATTLEBUDDY(hBuddyMichael)	
										buddyReleasedAndOutTruck = TRUE
									ENDIF
								ELSE
									buddyReleasedAndOutTruck = TRUE
								ENDIF
							ENDIF
						ELSE
							buddy1NotAvailable = TRUE
						ENDIF
						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
							IF NOT IS_PED_IN_VEHICLE(hBuddyFranklin, DrillerTruck)
								IF IS_BATTLEBUDDY_OVERRIDDEN(hBuddyFranklin)
									IF RELEASE_BATTLEBUDDY(hBuddyFranklin)	
										buddyReleasedAndOutTruck = TRUE
									ENDIF
								ELSE
									buddyReleasedAndOutTruck = TRUE
								ENDIF
							ENDIF
						ELSE
							buddy2NotAvailable = TRUE
						ENDIF
					ENDIF	
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
							IF NOT IS_PED_IN_VEHICLE(hBuddyMichael, DrillerTruck)
								IF IS_BATTLEBUDDY_OVERRIDDEN(hBuddyMichael)
									IF RELEASE_BATTLEBUDDY(hBuddyMichael)	
										buddyReleasedAndOutTruck = TRUE
									ENDIF
								ELSE
									buddyReleasedAndOutTruck = TRUE
								ENDIF
							ENDIF
						ELSE
							buddy1NotAvailable = TRUE
						ENDIF
						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
							IF NOT IS_PED_IN_VEHICLE(hBuddyTrevor, DrillerTruck)
								IF IS_BATTLEBUDDY_OVERRIDDEN(hBuddyTrevor)
									IF RELEASE_BATTLEBUDDY(hBuddyTrevor)	
										buddyReleasedAndOutTruck = TRUE
									ENDIF
								ELSE
									buddyReleasedAndOutTruck = TRUE
								ENDIF
							ENDIF
						ELSE
							buddy2NotAvailable = TRUE
						ENDIF
					ENDIF	
					
					//Set flag to true if no buddies exist
					IF buddy1NotAvailable = TRUE
					AND buddy2NotAvailable = TRUE
						buddyReleasedAndOutTruck = TRUE
					ENDIF
				ELSE
					//Make player exit vehicle
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), DrillerTruck, TRUE)
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						SETTIMERB(0)
						icontrolFlag = 3
					ENDIF
				ENDIF
			ENDIF			
		ENDIF	
	
	ENDIF
	
	IF icontrolFlag = 3		
			
		IF DOES_ENTITY_EXIST(DrillerTruck)
			IF IS_VEHICLE_DRIVEABLE(DrillerTruck)
				SET_VEHICLE_DOORS_SHUT(DrillerTruck, FALSE)
				SET_VEHICLE_DOORS_LOCKED(DrillerTruck, VEHICLELOCK_LOCKED)
				FREEZE_ENTITY_POSITION(DrillerTruck, TRUE)
			ENDIF
		ENDIF			
		IF DOES_ENTITY_EXIST(DrillerTrailer)
			IF IS_VEHICLE_DRIVEABLE(DrillerTrailer)
				FREEZE_ENTITY_POSITION(DrillerTrailer, TRUE)
			ENDIF
		ENDIF					
		
		iControlFlag = 4
		
	ENDIF
	
	IF iControlFlag = 4
	
		Mission_Passed()
	
	ENDIF

ENDPROC

//PURPOSE: Handles the mission if it fails for any reason.
PROC DO_STAGE_MISSION_FAILED()

	SWITCH iControlFlag 
	
		CASE 0 // init
			CPRINTLN(DEBUG_MISSION, "Init DO_STAGE_MISSION_FAILED")
			CLEAR_PRINTS()
			CLEAR_HELP()
			// remove all blips here
			
			//Trigger fail music 
			TRIGGER_MUSIC_EVENT("FHPRA_FAIL")
			
			// set fail reason
			SWITCH eFailReason
				CASE FAIL_DEFAULT
					// no fail reason
				BREAK
				CASE FAIL_CUTTER_DESTROYED
					sFailReason = "FPB_FAIL1"//~s~The driller was destroyed.
				BREAK
				CASE FAIL_TRUCK_DESTROYED
					sFailReason = "FPB_FAIL2"//~s~The truck was destroyed.
				BREAK
				CASE FAIL_TRAILER_DESTROYED
					sFailReason = "FPB_FAIL3"//~s~The trailer was destroyed.
				BREAK
				CASE FAIL_TRUCK_STUCK
					sFailReason = "FPB_FAIL10"//~s~The truck is stuck.
				BREAK
				CASE FAIL_CUTTER_ABANDONED
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						sFailReason = "FPB_FAIL7"//~s~Michael abandoned the Driller.
					ENDIF
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						sFailReason = "FPB_FAIL8"//~s~Trevor abandoned the Driller.
					ENDIF
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						sFailReason = "FPB_FAIL9"//~s~Franklin abandoned the Driller.
					ENDIF
				BREAK
				CASE FAIL_ENDGUARD_KILLED
					sFailReason = "FPB_FAIL11"//~s~Lester's contact was killed.
				BREAK
			ENDSWITCH
			
			// call appropriate mission failed function
			IF eFailReason = FAIL_DEFAULT
				MISSION_FLOW_MISSION_FAILED()
			ELSE
				MISSION_FLOW_MISSION_FAILED_WITH_REASON(sFailReason)
			ENDIF	

			iControlFlag = 1
		BREAK
		
		CASE 1 // wait for fail fade to finish
		
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				
				// check if we need to respawn the player in a different position, 
				// if replay accepted, warp to short distance away from the blip position
				// (far enough away so that the mission gets blipped)
				IF HAS_PLAYER_ACCEPTED_REPLAY()
//					SET_REPLAY_ACCEPTED_WARP_LOCATION()
				ELSE
					// if he rejects the replay only warp him to just outside military base if he failed inside it
					// (This is a restricted area so is already handled by the replay controller)
				ENDIF
				
				MISSION_CLEANUP() // must only take 1 frame and terminate the thread
			ELSE
				// fade not finished, you may want to handle dialogue or other behaviour here
			ENDIF
		BREAK
	ENDSWITCH

ENDPROC

// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	PRINTSTRING("...Placeholder Mission Launched")
	PRINTNL()

	IF (HAS_FORCE_CLEANUP_OCCURRED())
		TRIGGER_MUSIC_EVENT("FHPRB_STOP")
		PRINTSTRING("...Placeholder Mission Force Cleanup")
		PRINTNL()
		Mission_Flow_Mission_Force_Cleanup()
		Mission_Cleanup()
	ENDIF
	
	IF IS_REPEAT_PLAY_ACTIVE()
		REQUEST_ADDITIONAL_TEXT("FINPRB", MISSION_TEXT_SLOT)
	ENDIF
	
	ADD_RELATIONSHIP_GROUP("WorkerPedMainGroup", workerMainGroup)
	ADD_RELATIONSHIP_GROUP("GuardMainGroup", GuardMainGroup)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(PACKER, TRUE)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, GuardMainGroup, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, workerMainGroup, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, GuardMainGroup, workerMainGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, workerMainGroup, GuardMainGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, GuardMainGroup, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, GuardMainGroup)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, workerMainGroup, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, workerMainGroup)	
	
	//SET_MISSION_FLAG(TRUE) //don't do this, flow should be setting this flag for you
	//as part of candidate launch process, if it isn't please add a bug 
	
	#IF IS_DEBUG_BUILD
		SET_DEBUG_STAGE_NAMES()
	#ENDIF
	
	WHILE (TRUE)
		
		#IF IS_DEBUG_BUILD
			DEBUG_KEYS()
		#ENDIF
		
		//For video recorder
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_FINPRB") 			
		
		//Needs calling every frame to stop player being allowed to detach the trailer
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_AllowedToDetachTrailer, FALSE)
		ENDIF
		
		//Handles all the mission stats
		STATS_CONTROLLER()
		
		//Update this flag if player gets a wanted level at all throughout the mission, it will effect the end of mission phonecall
		IF WantedLevelFlagSet = FALSE
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREP_GOT_WANTED, TRUE)
				WantedLevelFlagSet = TRUE
			ENDIF
		ENDIF
		
		//Run fail checks throughout mission only if the mission is allowed to fail.
		IF missionCanFail = TRUE
			FAIL_CHECKS()
		ENDIF		
		
		//Controls battle buddies
		MONITOR_BATTLE_BUDDIES()
		
		//Control the worker peds and how they treat the player 
		MONITOR_WORKER_PEDS()
		
		//Control the guards
		MONITOR_YARD_GUARDS()		
		
		//Dialogue controller
		MONITOR_DIALOGUE()
		
		//Controls the guard at the car park
		MONITOR_END_GUARD()
		
		SWITCH missionStage
		
			CASE STAGE_OPENING_CUTSCENE
				DO_STAGE_OPENING_CUTSCENE()
			BREAK

			CASE STAGE_INIT_MISSION
				DO_STAGE_INIT_MISSION()
			BREAK

			CASE STAGE_GET_TO_DRILLER
				DO_STAGE_GET_TO_DRILLER()
			BREAK
			
			CASE STAGE_STEAL_DRILLER
				DO_STAGE_STEAL_DRILLER()
			BREAK	
			
			CASE STAGE_TAKE_DRILLER_HOME
				DO_STAGE_TAKE_DRILLER_HOME()
			BREAK
			
			CASE STAGE_CLOSING_CUTSCENE
				DO_STAGE_CLOSING_CUTSCENE()
			BREAK
			
			CASE STAGE_MISSION_FAILED
				DO_STAGE_MISSION_FAILED()
			BREAK		
			
		ENDSWITCH
	
		#IF IS_DEBUG_BUILD
			//Checks for debug keys being pressed. J-skip, S-Pass, F-Fail, P-Previous
			IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, 0) = TRUE
				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(0)
				ENDIF
				iControlFlag = 0
				IF iReturnStage = 0
					MissionStageBeingSkippedTo = TRUE
					missionStage = STAGE_GET_TO_DRILLER
				ENDIF
				IF iReturnStage = 1
					MissionStageBeingSkippedTo = TRUE
					missionStage = STAGE_STEAL_DRILLER
				ENDIF
				IF iReturnStage = 2
					MissionStageBeingSkippedTo = TRUE
					missionStage = STAGE_TAKE_DRILLER_HOME
				ENDIF
				IF iReturnStage = 3
					MissionStageBeingSkippedTo = TRUE
					missionStage = STAGE_CLOSING_CUTSCENE
				ENDIF
			ENDIF
		#ENDIF		
		
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
