// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	finale_heist1.sc
//		AUTHOR			:	Craig Vincent /
//		DESCRIPTION		:	finale bank heist setup. Staking the place out for entrances and 
//							any other means of easier access to the banks vault to retrieve 
//							the gold.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//FH1_TOOCLOSE

//New dialogue to be implemented for finh1.dstar
//
//New roots:
//FH1_WHERE
//FH1_TIMERb
//FH1_TIMERc
//FH1_WANTED
//FH1_LOSE
//FH1_SEES
//FH1_SHOUT1
//FH1_TALK4
//FH1_COMEON
//FH1_LESWH
//FH1_PAINT
//FH1_SEECON
//FH1_GOTCON
//FH1_MESS
//FH1_SEEAIR
//FH1_BACK
//FH1_LESBACK
//FH1_AROUND
//FH1_ALERT
//FH1_TIME
//FH1_UP4
//
//FH1_TOOCLOSE
//FH1_LOSTEM
//FH1_ALERTED
//FH1_HOLEBACK
//FH1_ABOVE
//
//FH1_OKAY
//
//Please implement in script, thanks. 

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"

USING "script_blips.sch"
Using "Locates_public.sch"
Using "select_mission_stage.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"
USING "chase_hint_cam.sch"                            
using "mission_stat_public.sch"
using "cutscene_public.sch"
using "clearMissionArea.sch"
USING "replay_public.sch"
using "completionpercentage_public.sch"
USING "taxi_functions.sch"
USING "timeLapse.sch"
using "stripclub_public.sch"
using "shared_hud_displays.sch"
USING "family_public.sch"
//USING "Overlay_Effects.sch"
using "RC_helper_functions.sch" 
using "asset_management_public.sch"
USING "spline_cam_edit.sch"
USING "building_control_public.sch"
USING "weapons_public.sch"
USING "commands_recording.sch"

//USING "traffic_default_values.sch" // this must be included before traffic.sch 
// total should not exceed 225
CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS					150
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS					25	
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS				40	

CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK 		11  
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK		1					

// this should be fine
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK		10
USING "traffic.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

//-----------------------------------------------------------------------------------------------------------
//		ENUMS
//-----------------------------------------------------------------------------------------------------------
ENUM MPF_MISSION_PED_FLAGS	
	//main player peds
	mpf_michael,
	mpf_franklin,	
	mpf_trevor,
	mpf_lester,
	mpf_guard,
	mpf_guardF,
	mpf_securityDriver,
	
	mpf_worker1,
	mpf_worker2,
	mpf_worker3,
	mpf_worker4,
	
	//bank security peds
	MPF_NUM_OF_PEDS
ENDENUM
ENUM MVF_MISSION_VEHICLE_FLAGS
	mvf_Michael_veh,
	mvf_trevor_veh,
	mvf_helicopter,
	mvf_security_truck,
	mvf_security_truck_fol,	
	mvf_bulldozer1,
	mvf_bulldozer2,	
	mvf_pol1,
	mvf_pol2,
	
	MVF_NUM_OF_VEH
ENDENUM
ENUM MSF_MISSION_STAGE_FLAGS
	msf_0_strip_club,
	msf_1_font_of_bank,
	msf_2_back_of_bank,
	msf_3_get_in_position,
	msf_4_heli_follow_truck,
	msf_5_CONSTRUCTION_HOLE,
	msf_6_drop_off_point,
	msf_7_passed,
	
	CST_INIT,
	CST_MCS_2,
	CST_MCS_3,
	
	MSF_NUM_OF_STAGES	
ENDENUM
ENUM MFF_MISSION_FAIL_FLAGS

	mff_debug_fail,
	mff_franklin_dead_fail,
	mff_trevor_dead_fail,
	mff_lester_dead_fail,
	mff_mike_dead_fail,
	mff_too_close_fail,
	mff_lost_truck,
	mff_Mike_car_des,
	mff_trev_car_des,
	mff_heli_des,
	mff_bank_knows,
	mff_left_franklin,
	mff_left_lester,
	mff_left_mission,
	mff_time,
	
	mff_default,	
	MFF_NUM_OF_FAILS
ENDENUM
Enum STAGE_SWITCHSTATE
	STAGESWITCH_IDLE,
	STAGESWITCH_REQUESTED,
	STAGESWITCH_EXITING,
	STAGESWITCH_ENTERING	
ENDENUM
ENUM UBERSTATE
	UBER_INIT,
	UBER_PLAY,
	UBER_UPDATE
ENDENUM

// Hot swap status
enum SWITCHSTATE
	SWITCH_NOSTATE,
	SWITCH_COMPLETE,
	SWITCH_IN_PROGRESS
ENDENUM
enum HELISTAGE
	TO_HELI,
	TO_TRUCKS
endenum
enum DIALOGUE_STATE_MIKE
	DI_START_CONDITION_MIKE,
	DI_TRIGGER_DIALOGUE_MIKE,
	DI_RUNNING_DIALOGUE_MIKE,
	DI_EXIT_DIALOGUE_MIKE
endenum
enum DIALOGUE_STATE_TREVOR
	DI_START_CONDITION_TREVOR,
	DI_TRIGGER_DIALOGUE_TREVOR,
	DI_RUNNING_DIALOGUE_TREVOR,
	DI_EXIT_DIALOGUE_TREVOR
endenum
enum INTERRUPT_ENUM
	INTERRUPT_NONE,
	INTERRUPT_STORE_CURRENT,
	INTERRUPT_KILL_CURRENT,
	INTERRUPTOR_SAFE,
	INTERRUPT_RESTART_READY,
	INTERRUPT_RESTART_PlAYING
endenum
enum BOTH_DIALOGUE_ENUM
	BOTH_TIME_INFO,
	BOTH_WARN1,
	BOTH_WARN2,
	BOTH_WARN3,
	BOTH_WARN4,
	BOTH_WARN5
endenum
enum MIKE_DIALOGUE_ENUM
	M_di_TO_FRONT,
	M_di_TO_BACK,
	M_di_END,
	M_di_NONE
endenum
enum TREV_DIALOGUE_ENUM	
	T_di_FILL1,
	T_di_FILL2,
	T_di_BRAD,
	T_di_THINKING,
	T_di_END,
	T_di_NONE
endenum
enum ENUM_HOLE
	HOLE_OBJECTIVE,
	HOLE_UNSEEN,
	HOLE_SEEN,
	HOLE_STABLE
	
endenum
enum ENUM_LEST_SIDE
	LEST_SLIDE, 
	LESTER_LEFT,
	LESTER_RIGHT
ENDENUM
ENUM SWITCH_TYPE_STAGE
	S_TYPE_INIT,
	S_TYPE_SHORT,
	S_TYPE_AUTO
ENDENUM	

//-----------------------------------------------------------------------------------------------------------
//		STRUCTS
//-----------------------------------------------------------------------------------------------------------
STRUCT VEHICLE_STRUCT
	VEHICLE_INDEX 		id
	BLIP_INDEX			blip
endstruct
Struct PEDS_STRUCT
	PED_INDEX					id	
	BLIP_INDEX					blip
	SEQUENCE_INDEX				seq
	SCENARIO_BLOCKING_INDEX		scenario_index
ENDSTRUCT

//-----------------------------------------------------------------------------------------------------------
//		CONSTANTS
//-----------------------------------------------------------------------------------------------------------
CONST_INT				STAGE_ENTRY		0
CONST_INT				STAGE_EXIT		-1 
CONST_INT				MAX_SPLINE_CAM_NODES 15		//used to create splines

// easier naming methods
CONST_INT		MIKE_AT_SCOPE_POINT 200
CONST_INT		TREV_PREP_HELI_STAGE 100

VECTOR		vFRONT_BANK 	= <<-7.8156, -741.8263, 43.1570>>
FLOAT		fHEADING_FRONT	= 69.4048
VECTOR		vBACK_BANK		= <<-110.60891, -636.62433, 35.10708>>
FLOAT		fHEADING_BACK	= 343.3254
VECTOR		vAIRFIELD		= <<1768.99805, 3279.52417, 40.37643>>
Vector		vTRUCK_START	= <<1147.39539, -1718.47021, 34.71170>>
Vector		vHOLE			= <<25.30480, -635.79169, 30.30575>>
//-----------------------------------------------------------------------------------------------------------
//		VARIABLES
//-----------------------------------------------------------------------------------------------------------
CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
VEHICLE_STRUCT				Vehs[MVF_NUM_OF_VEH]	//holds all of the vehicles for tis level
PEDS_STRUCT					peds[MPF_NUM_OF_PEDS]	//holds all of the peds for this level
structPedsForConversation 	convo_struct			//holds peds in the conversation
LOCATES_HEADER_DATA 		sLocatesData
BLIP_INDEX					blip_objective			//blip for mission objective
Camera_index				cameraIndex				//in game cut scene camera
camera_index				camfollow
CAMERA_INDEX				Camtarget
SEQUENCE_INDEX				seq						//used to create AI sequence
STREAMVOL_ID				vol_construction 

OBJECT_INDEX				objdummy
object_index				objCam

HELISTAGE					heli_switch

INTERIOR_INSTANCE_INDEX 	interior_tunnel			// interiors
INTERIOR_INSTANCE_INDEX 	interior_constuction
INTERIOR_INSTANCE_INDEX 	interior_carpark

//relationship
REL_GROUP_HASH	rel_buddy
REL_GROUP_HASH 	rel_guard

//time of day variables 
structTimelapse	sTimelapse

//-------------------------------------------hot swap stuff--------------------------------------------------------
SELECTOR_PED_STRUCT			sSelectorPeds						//struct to hold the peds for hotswap		
SELECTOR_CAM_STRUCT 		sCamDetails							//camera for hotswap
SWITCH_TYPE_STAGE 			eSwitchTypeStage
INT							iLastSwapTimer
bool						bHotSwap	= false   				//Toggle HotSwap
float 						fSwitchTimeRec
bool						bLongSpline
int 						iUnfreezeDelay
int 						idescentstage

bool						bcustomswitch_mike_to_trev_inCar 	= false
bool						bcustomswitch_mike_to_trev_inHeli 	= false
bool						bcustomswitch_trev_to_mike	 		= false

//-----------------------------------------------------------------------------------------------------------------
//---------------------------Bank CS / timings--------------------------------
int 					iWindowTimer	
bool					bHeliStageLoaded =  false

bool					bFCSanimsLoaded  = false

//---------------------------trucks--------------------------------
bool 					bTruckSkipForward	= false
int 					iInFailTimer
int						iCountdownAudioEvent
int						iGetCloser
bool					bFailTimerStarted	= true
int						iSPOTTEDTIME

bool					wSkippedUber
bool					wSkip

INT 			iRoadBlock_1
INT 			iRoadBlock_2
INT 			iRoadBlock_3
INT 			iRoadBlock_4
INT 			iRoadBlock_5
INT 			iRoadBlock_6
INT 			iRoadBlock_7
INT 			iRoadBlock_8

//--------------------------- hole --------------------------------
int 					iHoleTimer
int						iHoleStage			// film hole
int 					iONSCREENTIME
int 					iCloserTimer
int						iHOLE_INFO_STAGE = 0
BOOL                    b_get_near_help = FALSE
BOOL                    b_film_hole_help_reminder = FALSE

//--------------------------helper variable-----------------------
int						i
int 					iDelay
float					fDist									//distance between two points
float 					wantedMultiplier
bool					b_cutscene_loaded		= false
bool					bMissedBackCS 			= false
bool					bsetEndCam				= false
//------------------------------stat helpers-------------------------
int 					i_tunnle_check

//-------------------------------cam---------------------------------
float					fshakeAmp
bool					bstopshake = false
int 					ishake
int 					iFrontCS_stage		// font of bank cs switch
CAM_VIEW_MODE 			eSavedMode = NUM_CAM_VIEW_MODES
enumCharacterList		eSavedChar

CHASE_HINT_CAM_STRUCT	sHintCamStruct
bool					bHint_camOn = false

SCALEFORM_INDEX			sfMov
int 					iSFstage = 0
//---------------------------lester cam footage--------------------------
bool					bLesterCam = false
int 					iCamTimer
int 					iLESTERCAM_stage
int 					hintCheckTimer
int						ihintstage
float					camPrevFOV 
int 					iFaceHintTimer
INT						iFrontBankDelay = -1

ENUM_LEST_SIDE			eLESTER_side		= LESTER_LEFT

			//camera attache bool
bool					battachedCam

//------------------------------- Audio ---------------------------------
int 					camhum 	= GET_SOUND_ID()
int						camzoom = GET_SOUND_ID()
bool					bAUDIOStreamLoaded 	= false
bool					bplayingstream 		= false
//--------------------text related----------------------------
//bool					bPrintDisplayed 		= false
bool					bfirstwarningCL			= true
bool					b_displayed_get_heli	= false
bool					bheliPrintDisplayed 	= false
bool					bLeftbuddyprint			= false
bool					bLeftArea_printed		= false

//---------------------- Camera Crosshair --------------------
//FLOAT fCross1HairHeight = 0.0020
//FLOAT fCross1HairWidth = 0.0300
//FLOAT fCross2HairHeight = 0.0440
//FLOAT fCross2HairWidth = 0.0020
//
//FLOAT fCursorHeight = 0.013
//FLOAT fCursorWidth = 0.013
//FLOAT fCursorX1 = 0.4600
//FLOAT fCursorY1 = 0.4570
//FLOAT fCursorX2 = 0.5400
//FLOAT fCursorY2 = 0.4570
//FLOAT fCursorX3 = 0.4600
//FLOAT fCursorY3 = 0.5480
//FLOAT fCursorX4 = 0.5400
//FLOAT fCursorY4 = 0.5480
//
//INT iRedBlink = 0

//--------------------Dialogue related----------------------------
DIALOGUE_STATE_MIKE		eDialogue_stateM = DI_START_CONDITION_MIKE
DIALOGUE_STATE_TREVOR	eDialogue_stateT = DI_START_CONDITION_TREVOR
BOTH_DIALOGUE_ENUM		eBOTH_dialogue
TREV_DIALOGUE_ENUM		eTREV_Dialogue
MIKE_DIALOGUE_ENUM		eMike_dialogue

INTERRUPT_ENUM			eInterruptstage
TEXT_LABEL_23			lbl_Interrupt 	= ""
TEXT_LABEL_23			lbl_root 		= ""
int 					iInterrupt_restart_delay 
const_int 				INTERRUPT_RESTART_DELAY 2000


TEXT_LABEL_23			lbl_TREVSwitchInterrupt 	= ""
TEXT_LABEL_23			lbl_TREVSwitchroot 			= ""
bool					b_TREVSpeechSwitchStopped 	= false
TEXT_LABEL_23			lbl_MIKESwitchInterrupt 	= ""
TEXT_LABEL_23			lbl_MIKESwitchroot 			= ""
bool					b_MIKESpeechSwitchStopped 	= false


int						iDialogueTimer
bool					bShowObjective = false

bool					bDialoguePlayed			= false
bool					bPrisonWarning 			= false
bool 					b_convo_paused			= false
bool					bFailedConvo			= false

//--------------------FIND HOLE STAGE----------------------------
ENUM_HOLE			eholestage = HOLE_UNSEEN 
bool				bFrankHELP1			= false 
bool				b_create_workers	= true
//bool				bworkersloaded 		= false
bool 				bCloserLest = true
//----------------------- flow helpers ----------------------------

bool	bheliset			= false
bool	btimeReset			= false
bool	bTunnel				= false
bool	bWrongSite			= false
bool	bSeenHole			= false

//----------------------- cs ----------------------------
bool 	bcs_mike	= false
bool	bcs_mikecar	= false
bool	bcs_trev	= false
bool 	bcs_frank	= false
bool	bcs_lester	= false
bool	bcs_cam		= false
bool	bcs_sec		= false

bool	bSwitchedToMike	= false
//---------------ANIM ------

//ACHIEVEMENTS
BOOL bBridge = FALSE

int iSyncScene

//uber recording
UBERSTATE		uber_state
Float  			fCurrentPlaybackTime
FLOAT 			fPlaybackSpeed 			
//int 			iRecordingProgress 		= 0 // used for recording the uber stuff only temp


//============================== streaming ============================== 
ASSET_MANAGEMENT_DATA	sAssetData
CUTSCENE_PED_VARIATION 	sCutscenePedVariationRegister[MPF_NUM_OF_PEDS]

bool		bloadDozer 			= false
bool		bcreateDozers 		= false
bool		bloadGuards 		= false
bool		bcreateGuards 		= false

//============================== ============================== ============================== 

//Stage Management and skip stuff
STAGE_SWITCHSTATE		stageswitch				//current switching status
INT						mission_stage			//current stage
INT						mission_substage		//current substage
INT						requestedStage			//the mission stage requested by a mission_stage switch
INT						iStageTimer				//timer used for debug
Bool					bDoSkip					//trigger the skip
INT						iSkipToStage			//the stage to skip to
bool					b_zskipped 		= false

#if IS_DEBUG_BUILD
	MissionStageMenuTextStruct 	zMenuNames[MSF_NUM_OF_STAGES]
	WIDGET_GROUP_ID 			widget_debug_cv
	vector 						vdebugcamOFfset = <<-6.32,20,6>>
	int 						iheliWidget = enum_to_int(heli_switch)
	int 						wDialoguestage = enum_to_int(eDialogue_stateM)
	int							wBOTH_dialogue = ENUM_TO_INT(eBOTH_dialogue)
	int							wTREV_Dialogue = ENUM_TO_INT(eTREV_Dialogue)
	int							wMike_dialogue = ENUM_TO_INT(eMike_dialogue)
	
	bool 						wb_TREVSpeechSwitchStopped	= b_TREVSpeechSwitchStopped
	bool						wb_MIKESpeechSwitchStopped	= b_MIKESpeechSwitchStopped
#endif


//SWITCH CAM VARIABLES AND PROCEDURES

ENUM FINAL_HEIST_SWITCH_CAM_STATE
	SWITCH_CAM_IDLE,
	SWITCH_CAM_REQUEST_ASSETS,
	SWITCH_CAM_SETUP_GAMEPLAY,
	SWITCH_CAM_SETUP_SPLINE_1,
	SWITCH_CAM_PLAYING_SPLINE_1,
	SWITCH_CAM_SHUTDOWN_SPLINE_1,
	SWITCH_CAM_SETUP_SPLINE_2,
	SWITCH_CAM_PLAYING_CODE_CAM,
	SWITCH_CAM_PLAYING_SPLINE_2,
	SWITCH_CAM_SHUTDOWN_SPLINE_2,
	SWITCH_CAM_RETURN_TO_GAMEPLAY
ENDENUM
FINAL_HEIST_SWITCH_CAM_STATE eSwitchCamState = SWITCH_CAM_IDLE

SWITCH_CAM_STRUCT scsSwitchCamMichaelToTrevorInTruck
SWITCH_CAM_STRUCT scsSwitchCamMichaelToTrevorInHeli
SWITCH_CAM_STRUCT scsSwitchCamTrevorInHeliToMichael

#IF IS_DEBUG_BUILD
//BOOL bSwitchCamDebugScenarioEnabled = TRUE
//BOOL bResetDebugScenario = FALSE
#ENDIF

FLOAT fGameplayCamHeadingMichaelToTrevorInCar = 12.0//-44.0
FLOAT fGameplayCamPitchMichaelToTrevorInCar = 3.0

VECTOR vTrevPreSwitchLoc

FLOAT fGameplayCamHeadingMichaelToTrevorInHeli = 0.0
FLOAT fGameplayCamPitchMichaelToTrevorInHeli = 0.0

VECTOR vMichaelToTrevorTruck_TrevLocation = <<1865.341, 2265.841, 55.65>>
FLOAT fMichaelToTrevorTruck_TrevHeading = 80.0

INT iMichealToTrevorTruck_PeeLoopDuration = 1400

INT iSyncSceneTrevPiss

VECTOR vMichaelToTrevorTruck_TruckLocation = (<<1877.9, 2257.5, 54.6>>)
FLOAT fMichaelToTrevorTruck_TruckHeading = 6.0

BOOL bRestoreHighLOD = FALSE
BOOL bMichaelCarToTrevTruck_DestGeoRequested

FLOAT fMichaelToTrevorTruck_SwitchCamLoadSphereRadius = 15.0
BOOL bMichaelCarToTrevTruck_TrevorPeeAnimHappening
BOOL bMichaelCarToTrevTruck_TrevorTransitionedToStand

FLOAT fMikeToTrevTruck_CharSwitchPhase = 0.18
BOOL bPushInToFirstPerson

FLOAT fPlaySwitchConvoMike = 0.055
FLOAT fPlaySwitchConvoTrev = 0.27

BOOL bHasMikeSwitchConvoPlayed
BOOL bHasTrevSwitchConvoPlayed
BOOL bTrevor_PeeFXHasStarted
BOOL bTrevor_PeeHasStopped
PTFX_ID PTfx_Trevor_Pee
FLOAT fTrevor_PeeFX_Stop_Phase = 0.33
VECTOR vTrevor_PeeFX_BoneOffset = <<0.15, 0.17, 0.0>>
VECTOR vTrevor_PeeFX_BoneRot = <<-90.0, 0.0, 0.0>>
FLOAT fMikeCarToTrevTruck_TransitionToPlayerLocoPhase = 0.65

BOOL bTrevHeliMichaelCar_DestGeoRequestEnabled = TRUE
BOOL bTrevHeliToMichaelCar_DestGeoRequested

BOOL bTrevHeliMichaelCar_EarlyFocusPedSwitchEnabled = TRUE
FLOAT fTrevHeliToMichaelCar_DestGeoRequested_Phase =  0.4
FLOAT fTrevHeliToMichaelCar_SwitchCamLoadSphereRadius = 7.0
VECTOR vTrevHeliToMichaelCar_SwitchCamLoadSphereLocation = <<-110.9, -636.6, 35.4>>

#if IS_DEBUG_BUILD
BOOL bTrevHeliToMichaelCar_TeleportToPos
BOOL bDEBUG_IsInIsolatedDebugMode = FALSE //[MF] This should always be set to FALSE on a submitted file.
#ENDIF

BOOL bMichaelCarToTrevHeli_UseCustomSwitch
BOOL bMichaelCarToTrevHeli_AnimsStarted
BOOL bCustomSwitchCamRendering_HeliToCar
FLOAT fMichaelCarToTrevHeli_Low_LOD_Level = 0.6
FLOAT fMichaelCarToTrevHeli_SwitchCamLowLodFrustrumDist = 2500.0
STREAMVOL_ID sid_MichaelToTrevHeli_LowLODFrustrum
STREAMVOL_ID sid_TrevHeliToMichael_BankRear
BOOL bTrevHeliToMichaelCar_StopRequestGeo
TAKE_CONTROL_OF_PED_FLAGS PlayerSwitchTakeControlFlag = TCF_NONE //[MF] This is to fix a nasty animation pop when we do the custom switch from Trevor in the heli to Michael

STRING sSwitchAnimLibraryCarToTruck_PissAnims = "missbigscore1Switch_Trevor_Piss"
STRING sSwitchAnimLibrary_MikeAndFrankAnims = "missbigscore1leadinoutbss_1_mcs_2"
STRING sSwitchAnimLibraryTrevToCar_HeliAnims = "missswitch"


//INT iWooshIn_ID = -1
//INT iWooshOut_ID = -1
//BOOL bWooshOut = FALSE

BOOL bPlayerControlGiven

BOOL bIgnoreNoPlayerControl

//[MF] Debug Booleans
#IF IS_DEBUG_BUILD
BOOL bSetupTrevorToMichaelHeliSwitch
BOOL bSetupMichaelToTrevorHeliSwitch
BOOL bDEBUG_SwitchCamIsActive
#ENDIF

Func bool Do_forced_hotswap(SELECTOR_SLOTS_ENUM targetPed)
	if MAKE_SELECTOR_PED_SELECTION(sSelectorPeds,targetPed)
		sCamDetails.pedTo = sSelectorPeds.pedID[targetPed]
		sCamDetails.bRun  = TRUE
		return true
	endif
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_CONVERSATION_PLAYING(STRING sRoot)
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		TEXT_LABEL txtRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		
		IF ARE_STRINGS_EQUAL(sRoot, txtRoot)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_TREVOR_IN_TRUCK(SWITCH_CAM_STRUCT &thisSwitchCam, PED_INDEX &piMichael, PED_INDEX &piTrevor)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_TREVOR_IN_TRUCK_OUTRO")
	
	IF NOT thisSwitchCam.bInitialized

		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<-2.3136, 2.2717, 0.7501>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[0].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[0].vNodeDir = <<-3.7170, -0.0056, -143.2630>>
		thisSwitchCam.nodes[0].bPointAtEntity = FALSE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].fNodeFOV = 30.0000
		thisSwitchCam.nodes[0].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].iNodeToClone = 0
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[0].fNodeCamShake = 0.3000
		thisSwitchCam.nodes[0].iCamEaseType = 1
		thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 3500
		thisSwitchCam.nodes[1].vNodePos = <<-1.6297, 1.5037, 0.6884>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[1].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[1].vNodeDir = <<-3.7170, -0.0056, -143.2630>>
		thisSwitchCam.nodes[1].bPointAtEntity = FALSE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].fNodeFOV = 30.0000
		thisSwitchCam.nodes[1].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].iNodeToClone = 0
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[1].fNodeCamShake = 0.3000
		thisSwitchCam.nodes[1].iCamEaseType = 2
		thisSwitchCam.nodes[1].fCamEaseScaler = 0.6000
		thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = TRUE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_SwitchShortMichaelIn
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 1.0000
		thisSwitchCam.nodes[1].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 600
		thisSwitchCam.nodes[2].vNodePos = <<-4.6983, -2.6296, 0.6944>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[2].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[2].vNodeDir = <<-3.7170, -0.0056, -143.2630>>
		thisSwitchCam.nodes[2].bPointAtEntity = FALSE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].fNodeFOV = 30.0000
		thisSwitchCam.nodes[2].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].iNodeToClone = 0
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 0.4190
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[2].fNodeCamShake = 0.1000
		thisSwitchCam.nodes[2].iCamEaseType = 0
		thisSwitchCam.nodes[2].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[2].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 1.0000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bFlashEnabled = TRUE
		thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_SwitchShortTrevorMid
		thisSwitchCam.nodes[2].fMinExposure = 0.0000
		thisSwitchCam.nodes[2].fMaxExposure = 0.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 0
		thisSwitchCam.nodes[2].iRampDownDuration = 0
		thisSwitchCam.nodes[2].iHoldDuration = 0
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.2000
		thisSwitchCam.nodes[2].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[3].bIsCamCutNode = TRUE

		thisSwitchCam.nodes[4].SwitchCamType = SWITCH_CAM_WORLD_POS
		thisSwitchCam.nodes[4].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[4].iNodeTime = 0
		thisSwitchCam.nodes[4].vNodePos = <<1866.3777, 2262.2773, 55.9814>>
		thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[4].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[4].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[4].vNodeDir = <<-6.0926, 0.0006, 57.3604>>
		thisSwitchCam.nodes[4].bPointAtEntity = FALSE
		thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].fNodeFOV = 40.0000
		thisSwitchCam.nodes[4].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].iNodeToClone = 0
		thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[4].fNodeMotionBlur = 0.3000
		thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[4].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[4].iCamEaseType = 1
		thisSwitchCam.nodes[4].fCamEaseScaler = 0.3000
		thisSwitchCam.nodes[4].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[4].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[4].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[4].fTimeScale = 1.0000
		thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[4].bFlashEnabled = FALSE
		thisSwitchCam.nodes[4].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[4].fMinExposure = 0.0000
		thisSwitchCam.nodes[4].fMaxExposure = 0.0000
		thisSwitchCam.nodes[4].iRampUpDuration = 0
		thisSwitchCam.nodes[4].iRampDownDuration = 0
		thisSwitchCam.nodes[4].iHoldDuration = 0
		thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[4].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[5].SwitchCamType = SWITCH_CAM_WORLD_POS
		thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[5].iNodeTime = 600
		thisSwitchCam.nodes[5].vNodePos = <<1867.2435, 2263.7412, 55.9814>>
		thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[5].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[5].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[5].vNodeDir = <<-6.0926, 0.0006, 57.3604>>
		thisSwitchCam.nodes[5].bPointAtEntity = FALSE
		thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].fNodeFOV = 40.0000
		thisSwitchCam.nodes[5].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].iNodeToClone = 0
		thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[5].fNodeMotionBlur = 0.3310
		thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[5].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[5].iCamEaseType = 0
		thisSwitchCam.nodes[5].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[5].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[5].fTimeScale = 1.0000
		thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[5].bFlashEnabled = FALSE
		thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[5].fMinExposure = 0.0000
		thisSwitchCam.nodes[5].fMaxExposure = 0.0000
		thisSwitchCam.nodes[5].iRampUpDuration = 0
		thisSwitchCam.nodes[5].iRampDownDuration = 0
		thisSwitchCam.nodes[5].iHoldDuration = 0
		thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[5].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[6].SwitchCamType = SWITCH_CAM_WORLD_POS
		thisSwitchCam.nodes[6].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[6].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[6].iNodeTime = 5000
		thisSwitchCam.nodes[6].vNodePos = <<1867.4435, 2263.9412, 55.9814>>
		thisSwitchCam.nodes[6].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[6].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[6].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[6].vNodeDir = <<-6.0926, 0.0006, 57.3604>>
		thisSwitchCam.nodes[6].bPointAtEntity = FALSE
		thisSwitchCam.nodes[6].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[6].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[6].fNodeFOV = 40.0000
		thisSwitchCam.nodes[6].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].iNodeToClone = 0
		thisSwitchCam.nodes[6].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[6].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[6].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[6].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[6].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[6].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[6].iCamEaseType = 0
		thisSwitchCam.nodes[6].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[6].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[6].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[6].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[6].fTimeScale = 1.0000
		thisSwitchCam.nodes[6].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[6].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[6].bFlashEnabled = FALSE
		thisSwitchCam.nodes[6].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[6].fMinExposure = 0.0000
		thisSwitchCam.nodes[6].fMaxExposure = 0.0000
		thisSwitchCam.nodes[6].iRampUpDuration = 0
		thisSwitchCam.nodes[6].iRampDownDuration = 0
		thisSwitchCam.nodes[6].iHoldDuration = 0
		thisSwitchCam.nodes[6].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[6].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[6].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[7].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[7].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[7].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[7].iNodeTime = 1400
		thisSwitchCam.nodes[7].vNodePos = <<1.3125, -2.2925, 0.5904>>
		thisSwitchCam.nodes[7].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[7].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[7].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[7].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[7].vNodeDir = <<-8.1857, 0.0000, 23.3637>>
		thisSwitchCam.nodes[7].bPointAtEntity = FALSE
		thisSwitchCam.nodes[7].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[7].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[7].fNodeFOV = 40.0000
		thisSwitchCam.nodes[7].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[7].iNodeToClone = 0
		thisSwitchCam.nodes[7].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[7].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[7].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[7].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[7].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[7].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[7].iCamEaseType = 0
		thisSwitchCam.nodes[7].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[7].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[7].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[7].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[7].fTimeScale = 1.0000
		thisSwitchCam.nodes[7].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[7].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[7].bFlashEnabled = FALSE
		thisSwitchCam.nodes[7].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[7].fMinExposure = 0.0000
		thisSwitchCam.nodes[7].fMaxExposure = 0.0000
		thisSwitchCam.nodes[7].iRampUpDuration = 0
		thisSwitchCam.nodes[7].iRampDownDuration = 0
		thisSwitchCam.nodes[7].iHoldDuration = 0
		thisSwitchCam.nodes[7].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[7].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[7].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_EffectStrength = 0.0000
		
		thisSwitchCam.nodes[8].bIsCamCutNode = TRUE

		thisSwitchCam.nodes[9].SwitchCamType = SWITCH_CAM_GAMEPLAY_CAM_COPY
		thisSwitchCam.nodes[9].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[9].bIsGameplayCamCopy = TRUE
		thisSwitchCam.nodes[9].iNodeTime = 0
		thisSwitchCam.nodes[9].vNodePos = <<-1.6713, 0.3223, 0.3105>>
		thisSwitchCam.nodes[9].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[9].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[9].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[9].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[9].vNodeDir = <<0.8921, 0.0000, -112.2359>>
		thisSwitchCam.nodes[9].bPointAtEntity = FALSE
		thisSwitchCam.nodes[9].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[9].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[9].fNodeFOV = 50.0000
		thisSwitchCam.nodes[9].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[9].iNodeToClone = 0
		thisSwitchCam.nodes[9].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[9].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[9].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[9].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[9].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[9].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[9].iCamEaseType = 0
		thisSwitchCam.nodes[9].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[9].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[9].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[9].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[9].fTimeScale = 1.0000
		thisSwitchCam.nodes[9].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[9].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[9].bFlashEnabled = FALSE
		thisSwitchCam.nodes[9].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[9].fMinExposure = 0.0000
		thisSwitchCam.nodes[9].fMaxExposure = 0.0000
		thisSwitchCam.nodes[9].iRampUpDuration = 0
		thisSwitchCam.nodes[9].iRampDownDuration = 0
		thisSwitchCam.nodes[9].iHoldDuration = 0
		thisSwitchCam.nodes[9].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[9].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[9].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[9].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[9].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[9].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[10].SwitchCamType = SWITCH_CAM_GAMEPLAY_CAM_COPY
		thisSwitchCam.nodes[10].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[10].bIsGameplayCamCopy = TRUE
		thisSwitchCam.nodes[10].iNodeTime = 4500
		thisSwitchCam.nodes[10].vNodePos = <<-1.5291, 0.3435, 0.4191>>
		thisSwitchCam.nodes[10].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[10].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[10].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[10].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[10].vNodeDir = <<-3.1276, -0.0015, -106.0949>>
		thisSwitchCam.nodes[10].bPointAtEntity = FALSE
		thisSwitchCam.nodes[10].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[10].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[10].fNodeFOV = 50.0000
		thisSwitchCam.nodes[10].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[10].iNodeToClone = 0
		thisSwitchCam.nodes[10].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[10].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[10].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[10].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[10].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[10].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[10].iCamEaseType = 0
		thisSwitchCam.nodes[10].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[10].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[10].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[10].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[10].fTimeScale = 1.0000
		thisSwitchCam.nodes[10].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[10].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[10].bFlashEnabled = FALSE
		thisSwitchCam.nodes[10].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[10].fMinExposure = 0.0000
		thisSwitchCam.nodes[10].fMaxExposure = 0.0000
		thisSwitchCam.nodes[10].iRampUpDuration = 0
		thisSwitchCam.nodes[10].iRampDownDuration = 0
		thisSwitchCam.nodes[10].iHoldDuration = 0
		thisSwitchCam.nodes[10].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[10].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[10].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[10].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[10].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[10].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[11].SwitchCamType = SWITCH_CAM_GAMEPLAY_CAM_COPY
		thisSwitchCam.nodes[11].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[11].bIsGameplayCamCopy = TRUE
		thisSwitchCam.nodes[11].iNodeTime = 5100
		thisSwitchCam.nodes[11].vNodePos = <<-1.5291, 0.3435, 0.4191>>
		thisSwitchCam.nodes[11].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[11].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[11].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[11].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[11].vNodeDir = <<-3.1276, -0.0015, -106.0949>>
		thisSwitchCam.nodes[11].bPointAtEntity = FALSE
		thisSwitchCam.nodes[11].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[11].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[11].fNodeFOV = 50.0000
		thisSwitchCam.nodes[11].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[11].iNodeToClone = 0
		thisSwitchCam.nodes[11].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[11].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[11].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[11].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[11].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[11].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[11].iCamEaseType = 0
		thisSwitchCam.nodes[11].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[11].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[11].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[11].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[11].fTimeScale = 1.0000
		thisSwitchCam.nodes[11].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[11].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[11].bFlashEnabled = FALSE
		thisSwitchCam.nodes[11].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[11].fMinExposure = 0.0000
		thisSwitchCam.nodes[11].fMaxExposure = 0.0000
		thisSwitchCam.nodes[11].iRampUpDuration = 0
		thisSwitchCam.nodes[11].iRampDownDuration = 0
		thisSwitchCam.nodes[11].iHoldDuration = 0
		thisSwitchCam.nodes[11].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[11].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[11].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[11].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[11].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[11].NodeDOF_Info.fDOF_EffectStrength = 0.0000



		thisSwitchCam.iNumNodes = 12
		thisSwitchCam.iCamSwitchFocusNode = 3
		thisSwitchCam.fSwitchSoundAudioStartPhase = 0.1700
		thisSwitchCam.fSwitchSoundAudioEndPhase = 0.2020
		thisSwitchCam.bSwitchSoundPlayOpeningPulse = TRUE
		thisSwitchCam.bSwitchSoundPlayMoveLoop = TRUE
		thisSwitchCam.bSwitchSoundPlayExitPulse = FALSE
		thisSwitchCam.bSplineNoSmoothing = TRUE
		thisSwitchCam.bAddGameplayCamAsLastNode = FALSE
		thisSwitchCam.iGameplayNodeBlendDuration = 0


		//--- End of Cam Data ---


		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_BigScore_MichaelToTrevorInCar.txt"	
		thisSwitchCam.strXMLFileName = "CameraInfo_BigScore_MichaelToTrevorInCar.xml"
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.piPeds[0] = piMichael
	thisSwitchCam.piPeds[1] = piTrevor
	
		
ENDPROC

/// PURPOSE:
///    Handle the playback of the switch cam.  The will setup, play, shut down and cleanup the switch cam spline
/// PARAMS:
///    thisSwitchCam - Switch camera to playback
///    bReturnTrueOnce - If you only want this function to return TRUE once upon completion. Set to FALSE to have this function to return true every time this is called once the camera is finished playback.
/// RETURNS:
///    TRUE when camera is complete, FALSE if playback is still active, or if the camera has completed playback and bReturnTrueOnce is set to TRUE.
FUNC BOOL HANDLE_SWITCH_CAM_MICHAEL_TO_TREVOR_IN_TRUCK(SWITCH_CAM_STRUCT &thisSwitchCam_1, BOOL bReturnTrueOnce = TRUE)
	
	INT iCurrentNode
	
	SWITCH eSwitchCamState

		CASE SWITCH_CAM_IDLE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")
		BREAK
		
		CASE SWITCH_CAM_REQUEST_ASSETS
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_REQUEST_ASSETS")

			NEW_LOAD_SCENE_START_SPHERE((<<1865.1, 2263.8, 56.3>>), fMichaelToTrevorTruck_SwitchCamLoadSphereRadius, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
			
		BREAK
		
		CASE SWITCH_CAM_SETUP_SPLINE_1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_SETUP_SPLINE_1")
			
			SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_TREVOR_IN_TRUCK(thisSwitchCam_1, peds[mpf_michael].id,  peds[mpf_trevor].id)

			IF NOT IS_PED_INJURED(peds[mpf_michael].id)
				TASK_PLAY_ANIM(peds[mpf_michael].id, sSwitchAnimLibrary_MikeAndFrankAnims, "bss_1_mcs_2_leadout_mic", INSTANT_BLEND_IN, DEFAULT, DEFAULT, AF_HOLD_LAST_FRAME)
			ENDIF
			IF NOT IS_PED_INJURED(peds[mpf_franklin].id)
				TASK_PLAY_ANIM(peds[mpf_franklin].id, sSwitchAnimLibrary_MikeAndFrankAnims, "bss_1_mcs_2_leadout_fra", INSTANT_BLEND_IN, DEFAULT, DEFAULT, AF_HOLD_LAST_FRAME)
			ENDIF
			
			DELETE_ALL_CARS_IN_UBER_PLAYBACK()
			SET_RENDER_HD_ONLY(FALSE)
			REQUEST_PTFX_ASSET()
			
			SET_PED_POPULATION_BUDGET(0)
			SET_VEHICLE_POPULATION_BUDGET(0)
			SET_REDUCE_PED_MODEL_BUDGET(TRUE)
			SET_REDUCE_VEHICLE_MODEL_BUDGET(TRUE)

			//[MF] If this switch cam exists, delete it and we'll create a fresh one.
			IF DOES_SWITCH_CAM_EXIST(thisSwitchCam_1)
				DESTROY_SWITCH_CAM(thisSwitchCam_1)
			ENDIF		
			
			CREATE_SPLINE_CAM(thisSwitchCam_1)
			
			SET_CAM_ACTIVE(thisSwitchCam_1.ciSpline, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)

			bRestoreHighLOD = FALSE
			
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
			eSavedMode = GET_FOLLOW_PED_CAM_VIEW_MODE()
			SET_FOLLOW_PED_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON_NEAR)
			
			bPlayerControlGiven = FALSE
			bTrevor_PeeHasStopped = FALSE
			bTrevor_PeeFXHasStarted = FALSE
			bHasTrevSwitchConvoPlayed = FALSE
			bHasMikeSwitchConvoPlayed = FALSE
			bMichaelCarToTrevTruck_TrevorTransitionedToStand = FALSE
			bPushInToFirstPerson = FALSE
			
			thisSwitchCam_1.bIsSplineCamFinishedPlaying = FALSE
			bMichaelCarToTrevTruck_DestGeoRequested = FALSE
			bMichaelCarToTrevTruck_TrevorPeeAnimHappening = FALSE	
							
			if not IS_PED_INJURED(peds[mpf_trevor].id)
				CLEAR_PED_TASKS_IMMEDIATELY(peds[mpf_trevor].id)
				CLEAR_PED_SECONDARY_TASK(peds[mpf_trevor].id)	
				SET_ENTITY_COORDS_NO_OFFSET(peds[mpf_trevor].id, vMichaelToTrevorTruck_TrevLocation)
				SET_ENTITY_HEADING(peds[mpf_trevor].id, fMichaelToTrevorTruck_TrevHeading)				
			endif
			
			IF NOT IS_ENTITY_DEAD(vehs[mvf_trevor_veh].id)
				SET_ENTITY_COORDS_NO_OFFSET(vehs[mvf_trevor_veh].id, vMichaelToTrevorTruck_TruckLocation)
				SET_ENTITY_HEADING(vehs[mvf_trevor_veh].id, fMichaelToTrevorTruck_TruckHeading)
				SET_ENTITY_LOAD_COLLISION_FLAG(vehs[mvf_trevor_veh].id,TRUE)
				REQUEST_COLLISION_AT_COORD(vMichaelToTrevorTruck_TruckLocation)
				FREEZE_ENTITY_POSITION(vehs[mvf_trevor_veh].id,TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehs[mvf_trevor_veh].id, TRUE)
			ENDIF
			
			//[MF] rolling down windows so they don't block player's view of characters
			ROLL_DOWN_WINDOW(vehs[mvf_Michael_veh].id, SC_WINDOW_FRONT_LEFT)
			ROLL_DOWN_WINDOW(vehs[mvf_Michael_veh].id, SC_WINDOW_FRONT_RIGHT)				
			
			IF NOT IS_PED_INJURED(peds[mpf_lester].id)
				IF IS_VEHICLE_DRIVEABLE(vehs[mvf_trevor_veh].id)
					IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(peds[mpf_lester].id,vehs[mvf_trevor_veh].id,VS_FRONT_RIGHT)
						SET_PED_INTO_VEHICLE(peds[mpf_lester].id,vehs[mvf_trevor_veh].id,VS_FRONT_RIGHT)
					ENDIF
				ENDIF
			ENDIF
			
			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE_1

		BREAK
		
		CASE SWITCH_CAM_PLAYING_SPLINE_1
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE() 
			
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE_1")
			CDEBUG3LN(DEBUG_MISSION, GET_CAM_SPLINE_PHASE(thisSwitchCam_1.ciSpline))
			
			iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam_1)
			
			IF IS_CAM_ACTIVE(thisSwitchCam_1.ciSpline)
				OVERRIDE_LODSCALE_THIS_FRAME(1.0)
				
				
				//[MF] Fix for Lester falling through the world
				IF iCurrentNode <= thisSwitchCam_1.iCamSwitchFocusNode + 1
					IF NOT IS_ENTITY_DEAD(vehs[mvf_trevor_veh].id)
						SET_ENTITY_COORDS_NO_OFFSET(vehs[mvf_trevor_veh].id, vMichaelToTrevorTruck_TruckLocation)
						SET_ENTITY_HEADING(vehs[mvf_trevor_veh].id, fMichaelToTrevorTruck_TruckHeading)
					ENDIF
				ENDIF
				
				//[MF] Handle streaming after we switch to Trevor and maintin gameplay camera direction
				IF GET_CAM_SPLINE_PHASE(thisSwitchCam_1.ciSpline) >= fMikeToTrevTruck_CharSwitchPhase
					IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(fGameplayCamHeadingMichaelToTrevorInCar)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(fGameplayCamPitchMichaelToTrevorInCar)
						IF NOT sCamDetails.bOKToSwitchPed
							sCamDetails.bOKToSwitchPed = TRUE
							SET_GAME_PAUSES_FOR_STREAMING(FALSE)
							SET_VEHICLE_POPULATION_BUDGET(2)
						ENDIF
					ENDIF
				ENDIF
				
				//[MF] Handle Trevor Pee PFX
				IF NOT bTrevor_PeeFXHasStarted
//					IF iCurrentNode >= thisSwitchCam_1.iCamSwitchFocusNode -3
//						IF bMichaelCarToTrevTruck_TrevorPeeAnimHappening
						IF GET_CAM_SPLINE_PHASE(thisSwitchCam_1.ciSpline) >= 0.05
							IF NOT IS_PED_INJURED(peds[mpf_trevor].id)
								PRINTLN("@@@@@@@@@@ START_PARTICLE_FX_LOOPED_ON_PED_BONE @@@@@@@@@@@")
								PTfx_Trevor_Pee = START_PARTICLE_FX_LOOPED_ON_PED_BONE("scr_bigscore_peeing", peds[mpf_trevor].id, vTrevor_PeeFX_BoneOffset, vTrevor_PeeFX_BoneRot, BONETAG_PELVIS)
								bTrevor_PeeFXHasStarted = TRUE
							ENDIF
						ENDIF
//						ENDIF
//					ENDIF
				ENDIF
	
				IF iCurrentNode >= thisSwitchCam_1.iCamSwitchFocusNode -1
				AND iCurrentNode < thisSwitchCam_1.iCamSwitchFocusNode + 2
					SET_RENDER_HD_ONLY(TRUE) //[MF] This is to prevent some nasty LOD confusion on the wall that Trevor is standing next to.
				ENDIF

				
				//[MF] Load the geo in for where we are advancing Trevor to.
				IF iCurrentNode >= 0
				AND GET_CAM_SPLINE_NODE_PHASE(thisSwitchCam_1.ciSpline) >= 0.0
					IF NOT bMichaelCarToTrevTruck_DestGeoRequested
						NEW_LOAD_SCENE_START_SPHERE((<<1865.1, 2263.8, 56.3>>), fMichaelToTrevorTruck_SwitchCamLoadSphereRadius, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE|NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
						bMichaelCarToTrevTruck_DestGeoRequested = TRUE
					ENDIF
				ENDIF

				//[MF]After we've switched to Trevor monitor to ensure he's playing his anim. If animation is interrupted by the gameplay systems this will restart it.
				IF GET_CAM_SPLINE_PHASE(thisSwitchCam_1.ciSpline) >= fMikeToTrevTruck_CharSwitchPhase
					IF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
						IF NOT bMichaelCarToTrevTruck_TrevorPeeAnimHappening
							IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
								IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneTrevPiss)
									IF NOT IS_PED_INJURED(peds[mpf_trevor].id)
										CLEAR_PED_TASKS_IMMEDIATELY(peds[mpf_trevor].id)
										CLEAR_PED_SECONDARY_TASK(peds[mpf_trevor].id)
										SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
										FREEZE_ENTITY_POSITION(peds[mpf_trevor].id, FALSE)
										
										//Additional warp
										IF IS_VEHICLE_DRIVEABLE(vehs[mvf_trevor_veh].id)
											SET_ENTITY_COORDS_NO_OFFSET(vehs[mvf_trevor_veh].id, <<1877.90, 2257.49, 54.50>>)
											SET_ENTITY_HEADING(vehs[mvf_trevor_veh].id, 6.90)
											SET_ENTITY_LOAD_COLLISION_FLAG(vehs[mvf_trevor_veh].id,TRUE)
											REQUEST_COLLISION_AT_COORD(<<1877.59, 2256.80, 53.51>>)
											SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehs[mvf_trevor_veh].id,FALSE)
											SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_trevor_veh].id)
											FREEZE_ENTITY_POSITION(vehs[mvf_trevor_veh].id,FALSE)
										ENDIF
										
										IF NOT IS_PED_INJURED(peds[mpf_lester].id)
											IF IS_VEHICLE_DRIVEABLE(vehs[mvf_trevor_veh].id)
												IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(peds[mpf_lester].id,vehs[mvf_trevor_veh].id,VS_FRONT_RIGHT)
													SET_PED_INTO_VEHICLE(peds[mpf_lester].id,vehs[mvf_trevor_veh].id,VS_FRONT_RIGHT)
												ENDIF
											ENDIF
										ENDIF

										SEQUENCE_INDEX siMakeTrevorPee
										OPEN_SEQUENCE_TASK(siMakeTrevorPee)
											TASK_PLAY_ANIM(NULL, sSwitchAnimLibraryCarToTruck_PissAnims, "PISS_LOOP", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, iMichealToTrevorTruck_PeeLoopDuration, AF_LOOPING)
											TASK_PLAY_ANIM(NULL, sSwitchAnimLibraryCarToTruck_PissAnims, "PISS_OUTRO", NORMAL_BLEND_IN, WALK_BLEND_OUT, -1, AF_TAG_SYNC_OUT)
										CLOSE_SEQUENCE_TASK(siMakeTrevorPee)
										TASK_PERFORM_SEQUENCE(peds[mpf_trevor].id, siMakeTrevorPee)
										
										bMichaelCarToTrevTruck_TrevorPeeAnimHappening = TRUE

										CDEBUG3LN(DEBUG_MISSION, "Playing Sync Scene")
									ENDIF
								ENDIF
								
								//[MF] Load geo behind Trevor
								NEW_LOAD_SCENE_STOP()
								NEW_LOAD_SCENE_START_SPHERE((<<1905.9, 2226.5, 71.1>>), 150.0)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bMichaelCarToTrevTruck_TrevorTransitionedToStand
					IF bMichaelCarToTrevTruck_TrevorPeeAnimHappening
						IF GET_CAM_SPLINE_PHASE(thisSwitchCam_1.ciSpline) >= fMikeCarToTrevTruck_TransitionToPlayerLocoPhase
							CPRINTLN(DEBUG_MISSION,"Telling Player To End In Stand Pose...")
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_DEFAULT, FALSE) 
							SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 500) 
							CLEAR_PED_TASKS(peds[mpf_trevor].id)
							bMichaelCarToTrevTruck_TrevorTransitionedToStand = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF bTrevor_PeeFXHasStarted
					IF NOT bTrevor_PeeHasStopped
						IF bMichaelCarToTrevTruck_TrevorPeeAnimHappening
							IF GET_CAM_SPLINE_PHASE(thisSwitchCam_1.ciSpline) >= fTrevor_PeeFX_Stop_Phase
								STOP_PARTICLE_FX_LOOPED(PTfx_Trevor_Pee)
								bTrevor_PeeHasStopped = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//[MF] Restore high detail LOD and ped/vehicle population data
				IF NOT bRestoreHighLOD
					IF iCurrentNode >= thisSwitchCam_1.iCamSwitchFocusNode + 2
						CDEBUG3LN(DEBUG_MISSION, "Restoring everything to high detail")
						SET_PED_POPULATION_BUDGET(3)
						SET_VEHICLE_POPULATION_BUDGET(3)
						SET_REDUCE_PED_MODEL_BUDGET(FALSE)
						SET_REDUCE_VEHICLE_MODEL_BUDGET(FALSE)

						FREEZE_ENTITY_POSITION(peds[mpf_trevor].id, FALSE)
						bRestoreHighLOD = true
					ENDIF
				ENDIF
				
				IF NOT bHasMikeSwitchConvoPlayed
					IF GET_CAM_SPLINE_PHASE(thisSwitchCam_1.ciSpline) > fPlaySwitchConvoMike
						
						CDEBUG3LN(DEBUG_MISSION, "Playing Michael's Dialogue: FH1_MCS2_LO")
						ADD_PED_FOR_DIALOGUE(convo_struct, 1, peds[mpf_michael].id, "MICHAEL", true)
						PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct, "FH1AUD","FH1_MCS2_LO", "FH1_MCS2_LO_1", CONV_PRIORITY_HIGH)
						
						bHasMikeSwitchConvoPlayed = TRUE
					ENDIF
				ENDIF
				
				IF NOT bHasTrevSwitchConvoPlayed
					IF GET_CAM_SPLINE_PHASE(thisSwitchCam_1.ciSpline) > fPlaySwitchConvoTrev
						CDEBUG3LN(DEBUG_MISSION, "Playing Trevor's Dialogue: FH1_TMBB")
						CREATE_CONVERSATION_FROM_SPECIFIC_LINE(convo_struct, "FH1AUD","FH1_TMBB", "FH1_TMBB_2", CONV_PRIORITY_HIGH)
						bHasTrevSwitchConvoPlayed = TRUE
					ENDIF
				ENDIF
				
				//[MF] End Cases if player presses a direction on an analogue stick
				IF GET_CAM_SPLINE_NODE_INDEX(thisSwitchCam_1.ciSpline) >= (thisSwitchCam_1.iNumNodes - 3)
				AND eSavedMode != CAM_VIEW_MODE_FIRST_PERSON
					SET_RENDER_HD_ONLY(FALSE)
					IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
						INT iLeftX, iLeftY, iRightX, iRightY
						GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
						IF iLeftX <> 0 OR iLeftY <> 0 OR iRightX <> 0 OR iRightY <> 0
						OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_AIM)	
						OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)	
							SET_PLAYER_CONTROL(PLAYER_ID(), true)
						
							CLEAR_PED_TASKS(peds[mpf_trevor].id)
							SET_FOLLOW_PED_CAM_VIEW_MODE(eSavedMode)
							RENDER_SCRIPT_CAMS(FALSE, TRUE, 1500)
							
							eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY	
						ENDIF
					ENDIF
				ENDIF
				IF NOT bPushInToFirstPerson
					IF (GET_CAM_SPLINE_PHASE(thisSwitchCam_1.ciSpline) >= 0.55 AND eSavedMode = CAM_VIEW_MODE_FIRST_PERSON)
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						bPushInToFirstPerson = TRUE
					ENDIF
				ENDIF
				//[MF] Once cam is at the end, reset the gameplay cam positon and advance to next state.
				IF (GET_CAM_SPLINE_PHASE(thisSwitchCam_1.ciSpline) >= 1.00 AND eSavedMode != CAM_VIEW_MODE_FIRST_PERSON)
				OR (GET_CAM_SPLINE_PHASE(thisSwitchCam_1.ciSpline) >= 0.57 AND eSavedMode = CAM_VIEW_MODE_FIRST_PERSON)
					CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")
					IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(fGameplayCamHeadingMichaelToTrevorInCar)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(fGameplayCamPitchMichaelToTrevorInCar)
					ENDIF
					SET_FOLLOW_PED_CAM_VIEW_MODE(eSavedMode)
					IF eSavedMode != CAM_VIEW_MODE_FIRST_PERSON
						RENDER_SCRIPT_CAMS(FALSE, TRUE, 2000)
					ELSE
						RENDER_SCRIPT_CAMS(FALSE, TRUE, 0)
					ENDIF
					eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY
				ENDIF
			ENDIF
			
		BREAK

		CASE SWITCH_CAM_RETURN_TO_GAMEPLAY

			SET_TIME_SCALE(1.0)

			IF DOES_CAM_EXIST(thisSwitchCam_1.ciSpline)
				SET_CAM_ACTIVE(thisSwitchCam_1.ciSpline, FALSE)
			ENDIF
			
			IF NOT IS_PED_INJURED(peds[mpf_lester].id )
				TASK_LOOK_AT_ENTITY(peds[mpf_lester].id ,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV,SLF_LOOKAT_HIGH)
			ENDIF

			IF DOES_SWITCH_CAM_EXIST(thisSwitchCam_1)
				DESTROY_SWITCH_CAM(thisSwitchCam_1)
			ENDIF

			SETTIMERB(0)
			
			IF NOT bPlayerControlGiven
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				bPlayerControlGiven = TRUE
			ENDIF

			SETTIMERA(0)
			
			REMOVE_ANIM_DICT(sSwitchAnimLibraryCarToTruck_PissAnims)
			REMOVE_ANIM_DICT(sSwitchAnimLibrary_MikeAndFrankAnims)
			REMOVE_ANIM_DICT("shake_cam_all@")
			
			//[MF] rolling down windows so they don't block player's view of characters
			IF NOT IS_ENTITY_DEAD(vehs[mvf_Michael_veh].id)
				ROLL_UP_WINDOW(vehs[mvf_Michael_veh].id, SC_WINDOW_FRONT_LEFT)
				ROLL_UP_WINDOW(vehs[mvf_Michael_veh].id, SC_WINDOW_FRONT_RIGHT)
			ENDIF
			
			thisSwitchCam_1.bIsSplineCamFinishedPlaying = TRUE
			sCamDetails.brun = FALSE
			
			SET_RENDER_HD_ONLY(FALSE)
			
			NEW_LOAD_SCENE_STOP()
			SET_GAME_PAUSES_FOR_STREAMING(TRUE)
			
			eSwitchCamState = SWITCH_CAM_IDLE
			
			SET_WANTED_LEVEL_MULTIPLIER(1.0)
			
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	IF bReturnTrueOnce
		RETURN FALSE
	ELSE
		RETURN thisSwitchCam_1.bIsSplineCamFinishedPlaying
	ENDIF
ENDFUNC


/// PURPOSE:
///    Setup the individual spline node cam data for each node in the scripted switch cam
/// PARAMS:
///    thisSwitchCam - Switch cam to setup the spline node data for
///    piMichael - Michael's current ped index
///    piTrevor - Trevor's current ped index
PROC SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_TREVOR_IN_HELI(SWITCH_CAM_STRUCT &thisSwitchCam, PED_INDEX &piMichael, PED_INDEX &piTrevor)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_TREVOR_IN_HELI")
	
	IF NOT thisSwitchCam.bInitialized


		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<-1.6973, 1.5316, 0.6956>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[0].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[0].vNodeDir = <<-3.7170, -0.0056, -143.2630>>
		thisSwitchCam.nodes[0].bPointAtEntity = FALSE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].fNodeFOV = 30.0000
		thisSwitchCam.nodes[0].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].iNodeToClone = 0
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[0].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 1500
		thisSwitchCam.nodes[1].vNodePos = <<-1.6297, 1.5037, 0.6884>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[1].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[1].vNodeDir = <<-3.7170, -0.0056, -143.2630>>
		thisSwitchCam.nodes[1].bPointAtEntity = FALSE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].fNodeFOV = 30.0000
		thisSwitchCam.nodes[1].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].iNodeToClone = 0
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[1].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[1].iCamEaseType = 2
		thisSwitchCam.nodes[1].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = TRUE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_SwitchShortMichaelIn
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.8
		thisSwitchCam.nodes[1].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 600
		thisSwitchCam.nodes[2].vNodePos = <<-0.0425, 0.2776, 31.0641>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[2].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[2].vNodeDir = <<-3.7170, -0.0060, -143.2626>>
		thisSwitchCam.nodes[2].bPointAtEntity = FALSE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].fNodeFOV = 30.0000
		thisSwitchCam.nodes[2].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].iNodeToClone = 0
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 0.3000
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[2].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[2].iCamEaseType = 1
		thisSwitchCam.nodes[2].fCamEaseScaler = 0.4000
		thisSwitchCam.nodes[2].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 1.0000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bFlashEnabled = TRUE
		thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_SwitchShortTrevorMid
		thisSwitchCam.nodes[2].fMinExposure = 0.0000
		thisSwitchCam.nodes[2].fMaxExposure = 0.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 0
		thisSwitchCam.nodes[2].iRampDownDuration = 0
		thisSwitchCam.nodes[2].iHoldDuration = 0
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.5500
		thisSwitchCam.nodes[2].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[3].bIsCamCutNode = TRUE

		thisSwitchCam.nodes[4].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[4].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[4].iNodeTime = 0
		thisSwitchCam.nodes[4].vNodePos = <<-5.9937, 5.3838, -9.1104>>
		thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[4].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[4].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[4].vNodeDir = <<-0.4838, -2.0393, -1.6781>>
		thisSwitchCam.nodes[4].bPointAtEntity = TRUE
		thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[4].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[4].fNodeFOV = 35.0000
		thisSwitchCam.nodes[4].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].iNodeToClone = 0
		thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[4].fNodeMotionBlur = 0.3000
		thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[4].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[4].iCamEaseType = 1
		thisSwitchCam.nodes[4].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[4].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[4].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[4].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[4].fTimeScale = 1.0000
		thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[4].bFlashEnabled = FALSE
		thisSwitchCam.nodes[4].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[4].fMinExposure = 0.0000
		thisSwitchCam.nodes[4].fMaxExposure = 0.0000
		thisSwitchCam.nodes[4].iRampUpDuration = 0
		thisSwitchCam.nodes[4].iRampDownDuration = 0
		thisSwitchCam.nodes[4].iHoldDuration = 0
		thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[4].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[5].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[5].iNodeTime = 900
		thisSwitchCam.nodes[5].vNodePos = <<-1.2508, 1.3896, 0.2707>>
		thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[5].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[5].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[5].vNodeDir = <<-0.0555, 0.0160, 0.4621>>
		thisSwitchCam.nodes[5].bPointAtEntity = TRUE
		thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[5].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[5].fNodeFOV = 35.0000
		thisSwitchCam.nodes[5].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].iNodeToClone = 0
		thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[5].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[5].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[5].iCamEaseType = 0
		thisSwitchCam.nodes[5].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[5].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[5].fTimeScale = 1.0000
		thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[5].bFlashEnabled = FALSE
		thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[5].fMinExposure = 0.0000
		thisSwitchCam.nodes[5].fMaxExposure = 0.0000
		thisSwitchCam.nodes[5].iRampUpDuration = 0
		thisSwitchCam.nodes[5].iRampDownDuration = 0
		thisSwitchCam.nodes[5].iHoldDuration = 0
		thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[5].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[6].SwitchCamType = SWITCH_ENTITY_ATTACHED_CAM
		thisSwitchCam.nodes[6].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[6].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[6].iNodeTime = 4500
		thisSwitchCam.nodes[6].vNodePos = <<-1.0556, 1.4287, 0.1626>>
		thisSwitchCam.nodes[6].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[6].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[6].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[6].vNodeDir = <<-0.0441, 0.0195, 0.3618>>
		thisSwitchCam.nodes[6].bPointAtEntity = TRUE
		thisSwitchCam.nodes[6].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[6].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[6].fNodeFOV = 35.0000
		thisSwitchCam.nodes[6].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].iNodeToClone = 0
		thisSwitchCam.nodes[6].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[6].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[6].fNodeTimePostFXTimeOffset = 0.9800
		thisSwitchCam.nodes[6].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[6].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[6].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[6].iCamEaseType = 0
		thisSwitchCam.nodes[6].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[6].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[6].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[6].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[6].fTimeScale = 1.0000
		thisSwitchCam.nodes[6].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[6].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[6].bFlashEnabled = FALSE
		thisSwitchCam.nodes[6].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[6].fMinExposure = 0.0000
		thisSwitchCam.nodes[6].fMaxExposure = 0.0000
		thisSwitchCam.nodes[6].iRampUpDuration = 0
		thisSwitchCam.nodes[6].iRampDownDuration = 0
		thisSwitchCam.nodes[6].iHoldDuration = 0
		thisSwitchCam.nodes[6].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[6].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[6].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.iNumNodes = 7
		thisSwitchCam.iCamSwitchFocusNode = 3
		thisSwitchCam.fSwitchSoundAudioStartPhase = 0.170
		thisSwitchCam.fSwitchSoundAudioEndPhase = 0.28
		thisSwitchCam.bSwitchSoundPlayOpeningPulse = TRUE
		thisSwitchCam.bSwitchSoundPlayMoveLoop = TRUE
		thisSwitchCam.bSwitchSoundPlayExitPulse = FALSE
		thisSwitchCam.bSplineNoSmoothing = TRUE
		thisSwitchCam.bAddGameplayCamAsLastNode = TRUE
		thisSwitchCam.iGameplayNodeBlendDuration = 0


		//--- End of Cam Data ---


		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_BigScore_MichaelToTrevorInHeli.txt"		
		thisSwitchCam.strXMLFileName = "CameraInfo_BigScore_MichaelToTrevorInHeli.xml"
		
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.piPeds[0] = piMichael
	thisSwitchCam.piPeds[1] = piTrevor
	
		
ENDPROC


/// PURPOSE:
///    Handle the playback of the switch cam.  The will setup, play, shut down and cleanup the switch cam spline
/// PARAMS:
///    thisSwitchCam - Switch camera to playback
///    bReturnTrueOnce - If you only want this function to return TRUE once upon completion. Set to FALSE to have this function to return true every time this is called once the camera is finished playback.
/// RETURNS:
///    TRUE when camera is complete, FALSE if playback is still active, or if the camera has completed playback and bReturnTrueOnce is set to TRUE.
FUNC BOOL HANDLE_SWITCH_CAM_MICHAEL_TO_TREVOR_IN_HELI(SWITCH_CAM_STRUCT &thisSwitchCam, BOOL bReturnTrueOnce = TRUE)
	
	INT iCurrentNode
	VECTOR vHeliPos

	SWITCH eSwitchCamState

		CASE SWITCH_CAM_IDLE
			CDEBUG3LN(DEBUG_MISSION, "HANDLE_SWITCH_CAM_MICHAEL_TO_TREVOR_IN_HELI: eSwitchCamState = SWITCH_CAM_IDLE")
		BREAK

		CASE SWITCH_CAM_SETUP_SPLINE_1
			CDEBUG3LN(DEBUG_MISSION, "HANDLE_SWITCH_CAM_MICHAEL_TO_TREVOR_IN_HELI: eSwitchCamState = SWITCH_CAM_SETUP_SPLINE_1")
			eSavedMode = GET_FOLLOW_VEHICLE_CAM_VIEW_MODE()
			//[MF] If this switch cam exists, delete it and we'll create a fresh one.
			IF DOES_SWITCH_CAM_EXIST(scsSwitchCamMichaelToTrevorInHeli)
				DESTROY_SWITCH_CAM(scsSwitchCamMichaelToTrevorInHeli)
			ENDIF
			
			//[MF] Destroying previous switch cam since we delay destroying it until the cutscene begins
			IF DOES_SWITCH_CAM_EXIST(scsSwitchCamTrevorInHeliToMichael)
				DESTROY_SWITCH_CAM(scsSwitchCamTrevorInHeliToMichael)
			ENDIF
			
			TASK_PLAY_ANIM(peds[mpf_franklin].id, sSwitchAnimLibrary_MikeAndFrankAnims, "bss_1_mcs_2_leadout_fra", INSTANT_BLEND_IN, DEFAULT, -1, AF_NOT_INTERRUPTABLE)
			TASK_PLAY_ANIM(peds[mpf_michael].id, sSwitchAnimLibrary_MikeAndFrankAnims, "bss_1_mcs_2_leadout_mic", INSTANT_BLEND_IN, DEFAULT, -1, AF_NOT_INTERRUPTABLE)							
			FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_michael].id, TRUE)
			
			SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_TREVOR_IN_HELI(thisSwitchCam, peds[mpf_michael].id,  peds[mpf_trevor].id)
			
			CREATE_SPLINE_CAM(thisSwitchCam)

			DELETE_ALL_CARS_IN_UBER_PLAYBACK()
			
			SET_CAM_ACTIVE(thisSwitchCam.ciSpline, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			SET_FORCE_HD_VEHICLE(vehs[mvf_helicopter].id, TRUE)
			
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)

			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			bPlayerControlGiven = FALSE
			
			SET_RENDER_HD_ONLY(FALSE)
			
			FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id, FALSE)

			thisSwitchCam.bIsSplineCamFinishedPlaying = FALSE

			bMichaelCarToTrevHeli_AnimsStarted = FALSE
			bPushInToFirstPerson = FALSE
			
			vHeliPos = GET_ENTITY_COORDS(vehs[mvf_helicopter].id)
			IF vHeliPos.Z < 40.0
				vHeliPos.Z += 70.0
				SET_ENTITY_COORDS(vehs[mvf_helicopter].id, vHeliPos)
			ENDIF

			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE_1
		BREAK
		
		CASE SWITCH_CAM_PLAYING_SPLINE_1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE_1 ", GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline))
			
			iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam)			
			
			IF IS_CAM_ACTIVE(thisSwitchCam.ciSpline)
			
				IF iCurrentNode >= thisSwitchCam.iCamSwitchFocusNode
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(fGameplayCamHeadingMichaelToTrevorInHeli)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(fGameplayCamPitchMichaelToTrevorInHeli)
				ENDIF				
				
				IF iCurrentNode >= thisSwitchCam.iCamSwitchFocusNode
					OVERRIDE_LODSCALE_THIS_FRAME(fMichaelCarToTrevHeli_Low_LOD_Level)
					IF NOT sCamDetails.bOKToSwitchPed
						PRINTLN("Switching Player...")
						sCamDetails.bOKToSwitchPed = TRUE
					
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_helicopter].id)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_helicopter].id)
						ENDIF
						
						STREAMVOL_DELETE(sid_TrevHeliToMichael_BankRear)
						
						//[MF] Load low LOD mountain details
						sid_MichaelToTrevHeli_LowLODFrustrum = STREAMVOL_CREATE_FRUSTUM(GET_ENTITY_COORDS(peds[mpf_trevor].id), GET_ENTITY_FORWARD_VECTOR(peds[mpf_trevor].id), fMichaelCarToTrevHeli_SwitchCamLowLodFrustrumDist, FLAG_MAPDATA, STREAMVOL_LOD_FLAG_LOW)
						SET_RENDER_HD_ONLY(FALSE)
						
					ENDIF
				ENDIF
				
				IF NOT bMichaelCarToTrevHeli_AnimsStarted
					IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_TREVOR
						CPRINTLN(DEBUG_MISSION,"Playing Trevor Switch Cam Anim")
						TASK_PLAY_ANIM(peds[mpf_trevor].id, sSwitchAnimLibraryTrevToCar_HeliAnims, "mid_mission_inside_helicopter_trevor", DEFAULT, REALLY_SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
						TASK_PLAY_ANIM(peds[mpf_lester].id, sSwitchAnimLibraryTrevToCar_HeliAnims, "mid_mission_inside_helicopter_lester", DEFAULT, SLOW_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)				
						FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_trevor].id)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_lester].id)
						bMichaelCarToTrevHeli_AnimsStarted = TRUE
					ENDIF
				ENDIF
				
				IF NOT bPushInToFirstPerson
					IF (GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= 0.7 AND eSavedMode = CAM_VIEW_MODE_FIRST_PERSON)
						PRINTLN("@@@@@@@@@@@ ANIMPOSTFX_PLAY - CamPushInNeutral @@@@@@@@@@@")
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						bPushInToFirstPerson = TRUE
					ENDIF
				ENDIF
				
				IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= 1.00
				OR (STREAMVOL_HAS_LOADED(sid_MichaelToTrevHeli_LowLODFrustrum) AND GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= 0.8)
					CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(fGameplayCamHeadingMichaelToTrevorInHeli)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(fGameplayCamPitchMichaelToTrevorInHeli)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_RENDER_HD_ONLY(FALSE)
					eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY
				ENDIF
			ENDIF
			
		BREAK
		
		CASE SWITCH_CAM_RETURN_TO_GAMEPLAY
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")
			
			SET_TIME_SCALE(1.0)
			
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = DESTROY_CAM(thisSwitchCam.ciSpline)")
			IF DOES_CAM_EXIST(thisSwitchCam.ciSpline)
				SET_CAM_ACTIVE(thisSwitchCam.ciSpline, FALSE)
				DESTROY_CAM(thisSwitchCam.ciSpline)
			ENDIF
			DESTROY_ALL_CAMS()
	
			IF NOT bPlayerControlGiven
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				bPlayerControlGiven = TRUE
			ENDIF
			
			CLEAR_PED_TASKS(peds[mpf_trevor].id)
			
			REMOVE_ANIM_DICT(sSwitchAnimLibraryTrevToCar_HeliAnims)
			REMOVE_ANIM_DICT(sSwitchAnimLibrary_MikeAndFrankAnims)
			UNLOAD_ALL_CAM_SHAKE_ANIM_LIBRARIES()
			
			SET_TO_DEFAULT_DETAIL_RENDERING(DEFAULT, 2)
			SET_FORCE_HD_VEHICLE(vehs[mvf_helicopter].id, FALSE)

			SETTIMERA(0)

			STREAMVOL_DELETE(sid_MichaelToTrevHeli_LowLODFrustrum)
			eSwitchCamState = SWITCH_CAM_IDLE
			thisSwitchCam.bIsSplineCamFinishedPlaying = TRUE
			
			ROLL_UP_WINDOW(vehs[mvf_Michael_veh].id, SC_WINDOW_FRONT_LEFT)
			ROLL_UP_WINDOW(vehs[mvf_Michael_veh].id, SC_WINDOW_FRONT_RIGHT)

			RETURN TRUE
			
		BREAK
		
	ENDSWITCH
	
	IF bReturnTrueOnce
		RETURN FALSE
	ELSE
		RETURN thisSwitchCam.bIsSplineCamFinishedPlaying
	ENDIF
ENDFUNC


/// PURPOSE:
///    Setup the individual spline node cam data for each node in the scripted switch cam
/// PARAMS:
///    thisSwitchCam - Switch cam to setup the spline node data for
///    piMichael - Michael's current ped index
///    piTrevor - Trevor's current ped index
PROC SETUP_SPLINE_CAM_NODE_ARRAY_TREVOR_IN_HELI_TO_MICHAEL(SWITCH_CAM_STRUCT &thisSwitchCam, PED_INDEX &piTrevor, PED_INDEX &piMichael)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_TREVOR_IN_HELI_TO_MICHAEL")
	
	IF NOT thisSwitchCam.bInitialized

		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<-1.3574, 1.8668, -0.2382>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[0].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[0].vNodeDir = <<0.0874, 0.1445, 0.2509>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].fNodeFOV = 30.0000
		thisSwitchCam.nodes[0].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].iNodeToClone = 0
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[0].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[0].bUseCustomDOF = TRUE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0600
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 10.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 1.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 1500
		thisSwitchCam.nodes[1].vNodePos = <<-1.4130, 1.5631, 0.1155>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[1].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[1].vNodeDir = <<-0.0956, 0.0073, 0.4327>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].fNodeFOV = 30.0000
		thisSwitchCam.nodes[1].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].iNodeToClone = 0
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[1].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[1].iCamEaseType = 2
		thisSwitchCam.nodes[1].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = TRUE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_SwitchShortTrevorIn
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.9900
		thisSwitchCam.nodes[1].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[1].bUseCustomDOF = TRUE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.1000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 16.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 1.0000

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 600
		thisSwitchCam.nodes[2].vNodePos = <<-1.7076, 2.1871, 2.7493>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[2].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[2].vNodeDir = <<-0.4081, 0.4474, 3.1630>>
		thisSwitchCam.nodes[2].bPointAtEntity = TRUE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].fNodeFOV = 30.0000
		thisSwitchCam.nodes[2].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].iNodeToClone = 0
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 0.3000
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[2].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[2].iCamEaseType = 1
		thisSwitchCam.nodes[2].fCamEaseScaler = 0.5000
		thisSwitchCam.nodes[2].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 1.0000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bFlashEnabled = TRUE
		thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_SwitchShortMichaelMid
		thisSwitchCam.nodes[2].fMinExposure = 0.0000
		thisSwitchCam.nodes[2].fMaxExposure = 0.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 0
		thisSwitchCam.nodes[2].iRampDownDuration = 0
		thisSwitchCam.nodes[2].iHoldDuration = 0
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.4000
		thisSwitchCam.nodes[2].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[2].bUseCustomDOF = TRUE
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.1700
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 20.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 1.0000

		thisSwitchCam.nodes[3].bIsCamCutNode = TRUE

		thisSwitchCam.nodes[4].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[4].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[4].iNodeTime = 0
		thisSwitchCam.nodes[4].vNodePos = <<2.6577, 0.8505, -0.0030>>
		thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[4].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[4].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[4].vNodeDir = <<-4.2795, 0.0000, 113.5217>>
		thisSwitchCam.nodes[4].bPointAtEntity = FALSE
		thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].fNodeFOV = 40.0000
		thisSwitchCam.nodes[4].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].iNodeToClone = 0
		thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[4].fNodeMotionBlur = 0.3000
		thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[4].fNodeCamShake = 0.1500
		thisSwitchCam.nodes[4].iCamEaseType = 1
		thisSwitchCam.nodes[4].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[4].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[4].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[4].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[4].fTimeScale = 1.0000
		thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[4].bFlashEnabled = FALSE
		thisSwitchCam.nodes[4].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[4].fMinExposure = 0.0000
		thisSwitchCam.nodes[4].fMaxExposure = 0.0000
		thisSwitchCam.nodes[4].iRampUpDuration = 0
		thisSwitchCam.nodes[4].iRampDownDuration = 0
		thisSwitchCam.nodes[4].iHoldDuration = 0
		thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[4].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[5].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[5].iNodeTime = 500
		thisSwitchCam.nodes[5].vNodePos = <<2.6138, 0.8314, 0.6365>>
		thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[5].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[5].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[5].vNodeDir = <<-4.2795, 0.0000, 113.5217>>
		thisSwitchCam.nodes[5].bPointAtEntity = FALSE
		thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].fNodeFOV = 40.0000
		thisSwitchCam.nodes[5].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].iNodeToClone = 0
		thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[5].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[5].fNodeCamShake = 0.1500
		thisSwitchCam.nodes[5].iCamEaseType = 1
		thisSwitchCam.nodes[5].fCamEaseScaler = 0.9000
		thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[5].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[5].fTimeScale = 1.0000
		thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[5].bFlashEnabled = FALSE
		thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[5].fMinExposure = 0.0000
		thisSwitchCam.nodes[5].fMaxExposure = 0.0000
		thisSwitchCam.nodes[5].iRampUpDuration = 0
		thisSwitchCam.nodes[5].iRampDownDuration = 0
		thisSwitchCam.nodes[5].iHoldDuration = 0
		thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[5].bIsLowDetailNode = TRUE
		thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[6].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[6].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[6].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[6].iNodeTime = 1000
		thisSwitchCam.nodes[6].vNodePos = <<2.5496, 0.8035, 0.6313>>
		thisSwitchCam.nodes[6].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[6].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[6].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[6].vNodeDir = <<-4.2795, 0.0000, 113.5214>>
		thisSwitchCam.nodes[6].bPointAtEntity = FALSE
		thisSwitchCam.nodes[6].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[6].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[6].fNodeFOV = 40.0000
		thisSwitchCam.nodes[6].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].iNodeToClone = 0
		thisSwitchCam.nodes[6].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[6].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[6].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[6].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[6].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[6].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[6].iCamEaseType = 0
		thisSwitchCam.nodes[6].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[6].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[6].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[6].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[6].fTimeScale = 1.0000
		thisSwitchCam.nodes[6].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[6].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[6].bFlashEnabled = FALSE
		thisSwitchCam.nodes[6].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[6].fMinExposure = 0.0000
		thisSwitchCam.nodes[6].fMaxExposure = 0.0000
		thisSwitchCam.nodes[6].iRampUpDuration = 0
		thisSwitchCam.nodes[6].iRampDownDuration = 0
		thisSwitchCam.nodes[6].iHoldDuration = 0
		thisSwitchCam.nodes[6].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[6].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[6].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[7].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[7].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[7].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[7].iNodeTime = 0
		thisSwitchCam.nodes[7].vNodePos = <<3.6208, 1.1263, 0.6699>>
		thisSwitchCam.nodes[7].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[7].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[7].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[7].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[7].vNodeDir = <<-10.0052, 0.0000, 104.9732>>
		thisSwitchCam.nodes[7].bPointAtEntity = FALSE
		thisSwitchCam.nodes[7].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[7].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[7].fNodeFOV = 50.0000
		thisSwitchCam.nodes[7].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[7].iNodeToClone = 0
		thisSwitchCam.nodes[7].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[7].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[7].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[7].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[7].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[7].fNodeCamShake = 0.1500
		thisSwitchCam.nodes[7].iCamEaseType = 0
		thisSwitchCam.nodes[7].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[7].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[7].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[7].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[7].fTimeScale = 1.0000
		thisSwitchCam.nodes[7].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[7].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[7].bFlashEnabled = FALSE
		thisSwitchCam.nodes[7].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[7].fMinExposure = 0.0000
		thisSwitchCam.nodes[7].fMaxExposure = 0.0000
		thisSwitchCam.nodes[7].iRampUpDuration = 0
		thisSwitchCam.nodes[7].iRampDownDuration = 0
		thisSwitchCam.nodes[7].iHoldDuration = 0
		thisSwitchCam.nodes[7].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[7].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[7].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_EffectStrength = 0.0000



		thisSwitchCam.iNumNodes = 8
		thisSwitchCam.iCamSwitchFocusNode = 4
		thisSwitchCam.fSwitchSoundAudioStartPhase = 0.5200
		thisSwitchCam.fSwitchSoundAudioEndPhase = 0.6600
		thisSwitchCam.bSwitchSoundPlayOpeningPulse = TRUE
		thisSwitchCam.bSwitchSoundPlayMoveLoop = TRUE
		thisSwitchCam.bSwitchSoundPlayExitPulse = FALSE
		thisSwitchCam.bSplineNoSmoothing = TRUE
		thisSwitchCam.bAddGameplayCamAsLastNode = FALSE
		thisSwitchCam.iGameplayNodeBlendDuration = 2000


		//--- End of Cam Data ---


		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_BigScore_TrevorInHeliToMichael.txt"		
		thisSwitchCam.strXMLFileName = "CameraInfo_BigScore_TrevorInHeliToMichael.xml"
		
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.piPeds[0] = piTrevor
	thisSwitchCam.piPeds[1] = piMichael
	
		
ENDPROC


/// PURPOSE:
///    Handle the playback of the switch cam.  The will setup, play, shut down and cleanup the switch cam spline
/// PARAMS:
///    thisSwitchCam - Switch camera to playback
///    bReturnTrueOnce - If you only want this function to return TRUE once upon completion. Set to FALSE to have this function to return true every time this is called once the camera is finished playback.
/// RETURNS:
///    TRUE when camera is complete, FALSE if playback is still active, or if the camera has completed playback and bReturnTrueOnce is set to TRUE.
FUNC BOOL HANDLE_SWITCH_CAM_TREVOR_IN_HELI_TO_MICHAEL(SWITCH_CAM_STRUCT &thisSwitchCam, BOOL bReturnTrueOnce = TRUE)
	
	INT iCurrentNode

	SWITCH eSwitchCamState

		CASE SWITCH_CAM_IDLE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")
		BREAK
		
		CASE SWITCH_CAM_REQUEST_ASSETS
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_REQUEST_ASSETS")

			NEW_LOAD_SCENE_START_SPHERE(vTrevHeliToMichaelCar_SwitchCamLoadSphereLocation, fTrevHeliToMichaelCar_SwitchCamLoadSphereRadius)

		BREAK
		
		CASE SWITCH_CAM_SETUP_SPLINE_1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_SETUP_SPLINE_1")
				
			DESTROY_ALL_CAMS()	
			
			SETUP_SPLINE_CAM_NODE_ARRAY_TREVOR_IN_HELI_TO_MICHAEL(thisSwitchCam, peds[mpf_trevor].id, peds[mpf_michael].id)
		
			NEW_LOAD_SCENE_START_SPHERE(vTrevHeliToMichaelCar_SwitchCamLoadSphereLocation, fTrevHeliToMichaelCar_SwitchCamLoadSphereRadius, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
			
			TASK_PLAY_ANIM(peds[mpf_trevor].id, sSwitchAnimLibraryTrevToCar_HeliAnims, "mid_mission_inside_helicopter_trevor", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			TASK_PLAY_ANIM(peds[mpf_lester].id, sSwitchAnimLibraryTrevToCar_HeliAnims, "mid_mission_inside_helicopter_lester", INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_lester].id, FALSE)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(peds[mpf_trevor].id, FALSE)
			
			//[MF] rolling down windows so they don't block player's view of characters
			ROLL_DOWN_WINDOW(vehs[mvf_Michael_veh].id, SC_WINDOW_FRONT_LEFT)
			ROLL_DOWN_WINDOW(vehs[mvf_Michael_veh].id, SC_WINDOW_FRONT_RIGHT)
			
			SET_ENTITY_COORDS(vehs[mvf_helicopter].id, GET_ENTITY_COORDS(vehs[mvf_helicopter].id) + (<<0.0, 0.0, 10.0>>))
			SET_HELI_BLADES_FULL_SPEED(vehs[mvf_helicopter].id)

			CREATE_SPLINE_CAM(thisSwitchCam)
			
			SET_CAM_ACTIVE(thisSwitchCam.ciSpline, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			bPlayerControlGiven = FALSE
			
			bTrevHeliToMichaelCar_DestGeoRequested = FALSE
			thisSwitchCam.bIsSplineCamFinishedPlaying = FALSE
			bTrevHeliToMichaelCar_StopRequestGeo = FALSE
			bCustomSwitchCamRendering_HeliToCar = TRUE

			//[MF] ensure Michael's vehicle is in the proper posiiton
			SET_ENTITY_COORDS(vehs[mvf_Michael_veh].id,vBACK_BANK)
			SET_ENTITY_HEADING(vehs[mvf_Michael_veh].id,fHEADING_BACK)				
			
			SET_GAME_PAUSES_FOR_STREAMING(FALSE)
			SET_FORCE_HD_VEHICLE(vehs[mvf_helicopter].id, TRUE)
			SET_VEHICLE_RADIO_ENABLED(vehs[mvf_Michael_veh].id, FALSE)
			
			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE_1

		BREAK
		
		CASE SWITCH_CAM_PLAYING_SPLINE_1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE_1", GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline))
			
			iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam)	
			IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Michael_veh].id)
				SET_ENTITY_COORDS(vehs[mvf_Michael_veh].id,vBACK_BANK)
				SET_ENTITY_HEADING(vehs[mvf_Michael_veh].id,fHEADING_BACK)
			ENDIF
			IF IS_CAM_ACTIVE(thisSwitchCam.ciSpline)
				IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) < fTrevHeliToMichaelCar_DestGeoRequested_Phase
					OVERRIDE_LODSCALE_THIS_FRAME(0.2)
				ENDIF	
				
				IF NOT bTrevHeliToMichaelCar_DestGeoRequested
					IF bTrevHeliMichaelCar_DestGeoRequestEnabled
						NEW_LOAD_SCENE_START_SPHERE(vTrevHeliToMichaelCar_SwitchCamLoadSphereLocation, fTrevHeliToMichaelCar_SwitchCamLoadSphereRadius, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
						bTrevHeliToMichaelCar_DestGeoRequested = TRUE
					ENDIF
				ENDIF
				
				IF bTrevHeliMichaelCar_EarlyFocusPedSwitchEnabled
					IF NOT sCamDetails.bOKToSwitchPed
						IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= fTrevHeliToMichaelCar_DestGeoRequested_Phase
							sCamDetails.bOKToSwitchPed = TRUE
							FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id, TRUE)
							PRINTLN("Setting: PlayerSwitchTakeControlFlag = TCF_CLEAR_TASK_INTERRUPT_CHECKS")
							PlayerSwitchTakeControlFlag = TCF_CLEAR_TASK_INTERRUPT_CHECKS
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_PLAYER_PED_ENUM(PLAYER_PED_ID()) = CHAR_MICHAEL
					IF PlayerSwitchTakeControlFlag = TCF_CLEAR_TASK_INTERRUPT_CHECKS
						PRINTLN("Setting: PlayerSwitchTakeControlFlag = TCF_NONE")
						//[MF] Restoring player clear task interrupt check for player switches
						PlayerSwitchTakeControlFlag = TCF_NONE
					ENDIF
				ENDIF
				
				
				IF iCurrentNode >= thisSwitchCam.iCamSwitchFocusNode - 1
					IF NOT IS_PED_INJURED(peds[mpf_franklin].id)
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_franklin].id, SCRIPT_TASK_PLAY_ANIM)
							TASK_PLAY_ANIM(peds[mpf_franklin].id, sSwitchAnimLibrary_MikeAndFrankAnims, "bss_1_mcs_2_leadin_fra", INSTANT_BLEND_IN, DEFAULT, DEFAULT, AF_LOOPING)
						ENDIF
					ENDIF
					IF NOT IS_PED_INJURED(peds[mpf_michael].id)
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_michael].id, SCRIPT_TASK_PLAY_ANIM)
							TASK_PLAY_ANIM(peds[mpf_michael].id, sSwitchAnimLibrary_MikeAndFrankAnims, "bss_1_mcs_2_leadin_mic", INSTANT_BLEND_IN, DEFAULT, DEFAULT, AF_LOOPING)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bTrevHeliToMichaelCar_StopRequestGeo
					
					IF iCurrentNode >= thisSwitchCam.iCamSwitchFocusNode
						NEW_LOAD_SCENE_STOP()
						WAIT(0)
						sid_TrevHeliToMichael_BankRear = STREAMVOL_CREATE_FRUSTUM((<<-111.2, -638.1, 36.5>>), (<<0.9, -1.6, 0.0>>), 350.0, FLAG_MAPDATA)

						OVERRIDE_LODSCALE_THIS_FRAME(1.0)//[MF] Forces a faster loading of high detail art.
						IF IS_VEHICLE_DRIVEABLE(vehs[mvf_helicopter].id)
							SET_FORCE_HD_VEHICLE(vehs[mvf_helicopter].id, FALSE)
						ENDIF

						bTrevHeliToMichaelCar_StopRequestGeo = TRUE
					ENDIF
				ENDIF
				
				IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= 1.00
					CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")
					eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY
				ENDIF
			ENDIF
			
		BREAK
		
		CASE SWITCH_CAM_RETURN_TO_GAMEPLAY
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")
			
			SET_TIME_SCALE(1.0)
			
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = DESTROY_CAM(thisSwitchCam.ciSpline)")

			SETTIMERA(0)

			STREAMVOL_DELETE(sid_TrevHeliToMichael_BankRear)
			
			FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id, FALSE)
			
			eSwitchCamState = SWITCH_CAM_IDLE
			
			thisSwitchCam.bIsSplineCamFinishedPlaying = TRUE
			
			sCamDetails.bRun = FALSE
			
			SET_RENDER_HD_ONLY(FALSE)
			SET_GAME_PAUSES_FOR_STREAMING(TRUE)
			SET_TO_DEFAULT_DETAIL_RENDERING()
			
			SET_VEHICLE_RADIO_ENABLED(vehs[mvf_Michael_veh].id, TRUE)

			//[MF] so we're not stuck on Michael and Franklin talking.
			KILL_ANY_CONVERSATION()
			
			PlayerSwitchTakeControlFlag = TCF_NONE
			
			//[MF] Safety to ensure we're going to a cutscene after this. If we aren't abort the cam so player isn't stuck in it.
			IF NOT HAS_CUTSCENE_LOADED()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				IF DOES_SWITCH_CAM_EXIST(thisSwitchCam)
					DESTROY_SWITCH_CAM(thisSwitchCam)
					REMOVE_ANIM_DICT(sSwitchAnimLibraryTrevToCar_HeliAnims)
					REMOVE_ANIM_DICT(sSwitchAnimLibrary_MikeAndFrankAnims)
					UNLOAD_ALL_CAM_SHAKE_ANIM_LIBRARIES()
				ENDIF
			ENDIF
			
			bMichaelCarToTrevHeli_UseCustomSwitch = TRUE

			RETURN TRUE
			
		BREAK
		
	ENDSWITCH
	
	IF bReturnTrueOnce
		RETURN FALSE
	ELSE
		RETURN thisSwitchCam.bIsSplineCamFinishedPlaying
	ENDIF
ENDFUNC



#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Create the debug widgets for working with the scripted character switch cam.
PROC CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
	CDEBUG3LN(DEBUG_MISSION, "CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS")

	START_WIDGET_GROUP("Custom Switch Cameras - Extra Tunables")
		
		START_WIDGET_GROUP("Michael Car To Trevor Truck")
			ADD_WIDGET_FLOAT_SLIDER("Dialogue Michael Start Phase", fPlaySwitchConvoMike, 0.0, 1.0, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Dialogue Trevor Start Phase", fPlaySwitchConvoTrev, 0.0, 1.0, 0.01)	
			START_WIDGET_GROUP("Trevor Tuner")	
				START_WIDGET_GROUP("Trevor Gameplay Cam Tuner")
					ADD_WIDGET_FLOAT_SLIDER("Gameplay Cam Heading Trevor", fGameplayCamHeadingMichaelToTrevorInCar, -180.0, 180.0, 0.25)
					ADD_WIDGET_FLOAT_SLIDER("Gameplay Cam Pitch Trevor", fGameplayCamPitchMichaelToTrevorInCar, -180.0, 180.0, 0.25)
				STOP_WIDGET_GROUP()
				ADD_WIDGET_VECTOR_SLIDER("Trevor Location", vMichaelToTrevorTruck_TrevLocation, -10000.0, 10000.0, 0.25)
				ADD_WIDGET_FLOAT_SLIDER("Trevor Heading", fMichaelToTrevorTruck_TrevHeading, -180.0, 180.0, 0.25)
				ADD_WIDGET_INT_SLIDER("Pee Anim Loop Duration", iMichealToTrevorTruck_PeeLoopDuration, 0, 5000, 500)
				ADD_WIDGET_FLOAT_SLIDER("Transition To Player Loco Phase", fMikeCarToTrevTruck_TransitionToPlayerLocoPhase, 0.0, 1.0, 0.1)
				START_WIDGET_GROUP("Trevor Pee Tuner")
					ADD_WIDGET_VECTOR_SLIDER("Pee Effect Bone Offset", vTrevor_PeeFX_BoneOffset, -1.0, 1.0, 0.01)
					ADD_WIDGET_VECTOR_SLIDER("Pee Effect Bone Rotation", vTrevor_PeeFX_BoneRot, -360.0, 360.0, 1.0)
					ADD_WIDGET_FLOAT_SLIDER("Pee Effect Stop Phase", fTrevor_PeeFX_Stop_Phase, 0.0, 1.0, 0.05)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Streaming Tuner")
				ADD_WIDGET_FLOAT_SLIDER("Streaming Sphere Radius", fMichaelToTrevorTruck_SwitchCamLoadSphereRadius, 0.0, 100.0, 0.1) 
				ADD_WIDGET_FLOAT_SLIDER("Character Switch Spline Phase", fMikeToTrevTruck_CharSwitchPhase, 0.0, 1.0, 0.1)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Trevor Heli To Michael Car")
			START_WIDGET_GROUP("Streaming Tuner")
				ADD_WIDGET_BOOL("Enable Early Switch Focus Ped Phase", bTrevHeliMichaelCar_EarlyFocusPedSwitchEnabled)
				ADD_WIDGET_BOOL("Enable Early Geo Streaming Request", bTrevHeliMichaelCar_DestGeoRequestEnabled)
				ADD_WIDGET_FLOAT_SLIDER("Switch Focus Ped Phase", fTrevHeliToMichaelCar_DestGeoRequested_Phase, 0.0, 1.0, 0.05)
				ADD_WIDGET_FLOAT_SLIDER("Streaming Sphere Radius", fTrevHeliToMichaelCar_SwitchCamLoadSphereRadius, 0.0, 10.0, 0.1) 
				ADD_WIDGET_VECTOR_SLIDER("Streaming Sphere Location", vTrevHeliToMichaelCar_SwitchCamLoadSphereLocation, -10000.0, 10000.0, 0.25) 
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_BOOL("Warp To Heliport", bTrevHeliToMichaelCar_TeleportToPos)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Michael Car To Trevor Heli")
			START_WIDGET_GROUP("Streaming Tuner")
				ADD_WIDGET_FLOAT_SLIDER("Low LOD Scale", fMichaelCarToTrevHeli_Low_LOD_Level, 0.01, 10.0, 0.25)
				ADD_WIDGET_FLOAT_SLIDER("Low LOD Load Frustrum Distance", fMichaelCarToTrevHeli_SwitchCamLowLodFrustrumDist, 0.0, 10000, 250)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Force Switch Debug Tools")
			ADD_WIDGET_BOOL("Enable dedicated debug mode", bDEBUG_IsInIsolatedDebugMode)
			ADD_WIDGET_BOOL("Setup Trevor Heli to Michael Car Switch", bSetupTrevorToMichaelHeliSwitch)
			ADD_WIDGET_BOOL("Setup Michael Car to Trevor Heli Switch", bSetupMichaelToTrevorHeliSwitch)
			ADD_WIDGET_BOOL("Start Switch Cam", bDEBUG_SwitchCamIsActive)
		STOP_WIDGET_GROUP()
		
		
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC P_DEBUG_FORCE_CHARACTER_SELECT(SELECTOR_SLOTS_ENUM selectorSlotChar, MPF_MISSION_PED_FLAGS missionPedFlag)
	
	sSelectorPeds.pedID[selectorSlotChar] = peds[missionPedFlag].id
	MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, selectorSlotChar)
	TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)

ENDPROC

PROC DEBUG_SETUP_TREVOR_TO_MICHAEL_HELI_SWITCH(BOOL bStartingOnTrevor)
	REQUEST_MODEL(FROGGER)
	
	WHILE NOT HAS_MODEL_LOADED(FROGGER)
		WAIT(0)
		CPRINTLN(DEBUG_MISSION,"Loading Helicopter Model: FROGGER")
	ENDWHILE

	IF bStartingOnTrevor
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
		
			P_DEBUG_FORCE_CHARACTER_SELECT(SELECTOR_PED_TREVOR, mpf_trevor)

		ENDIF
	
		sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = peds[mpf_michael].id
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
	ELSE
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
		
			P_DEBUG_FORCE_CHARACTER_SELECT(SELECTOR_PED_MICHAEL, mpf_michael)

		ENDIF
	
		sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = peds[mpf_trevor].id
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_TREVOR, FALSE)
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(peds[mpf_trevor].id)
		SET_VEHICLE_ON_GROUND_PROPERLY(GET_VEHICLE_PED_IS_IN(peds[mpf_trevor].id))
		WAIT(0)
		TASK_LEAVE_VEHICLE(peds[mpf_trevor].id, GET_VEHICLE_PED_IS_IN(peds[mpf_trevor].id), ECF_WARP_PED)
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(peds[mpf_lester].id)
		SET_VEHICLE_ON_GROUND_PROPERLY(GET_VEHICLE_PED_IS_IN(peds[mpf_lester].id))
		WAIT(0)
		TASK_LEAVE_VEHICLE(peds[mpf_lester].id, GET_VEHICLE_PED_IS_IN(peds[mpf_lester].id), ECF_WARP_PED)
	ENDIF
		
	IF NOT DOES_ENTITY_EXIST(vehs[mvf_helicopter].id)
	OR IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
		vehs[mvf_helicopter].id = CREATE_VEHICLE(frogger, << 1758.3090, 3284.5278, 40.7 >>, 133.1852 )
		SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_helicopter].id,false)
	ELSE
		IF NOT bDEBUG_SwitchCamIsActive //[MF] Only reset the helicopter's positon if we aren't about to view a switch cam
			SET_ENTITY_COORDS(vehs[mvf_helicopter].id, << 1758.3090, 3284.5278, 40.7 >>)
			SET_ENTITY_HEADING(vehs[mvf_helicopter].id, 133.1852)
		ENDIF
	ENDIF
		
	SET_PED_INTO_VEHICLE(peds[mpf_trevor].id, vehs[mvf_helicopter].id)
	SET_PED_INTO_VEHICLE(peds[mpf_lester].id, vehs[mvf_helicopter].id, VS_BACK_LEFT)
	
	IF IS_PED_IN_ANY_VEHICLE(peds[mpf_michael].id)
		SET_VEHICLE_ON_GROUND_PROPERLY(GET_VEHICLE_PED_IS_IN(peds[mpf_michael].id))
		WAIT(0)
		TASK_LEAVE_VEHICLE(peds[mpf_trevor].id, GET_VEHICLE_PED_IS_IN(peds[mpf_michael].id), ECF_WARP_PED)
	ENDIF
		
	IF NOT DOES_ENTITY_EXIST(vehs[mvf_Michael_veh].id)
	OR IS_ENTITY_DEAD(vehs[mvf_Michael_veh].id)
		WHILE NOT CREATE_PLAYER_VEHICLE(vehs[mvf_Michael_veh].id,CHAR_MICHAEL,vBACK_BANK, fHEADING_BACK)
			CPRINTLN(DEBUG_MISSION,"Creating Michael's vehicle...")
			WAIT(0)
		ENDWHILE
	ELSE
		SET_ENTITY_COORDS(vehs[mvf_Michael_veh].id, vBACK_BANK)
		SET_ENTITY_HEADING(vehs[mvf_Michael_veh].id, fHEADING_BACK)
	ENDIF
		
	SET_PED_INTO_VEHICLE(peds[mpf_michael].id, vehs[mvf_Michael_veh].id)	
	
ENDPROC


PROC HANDLE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()

	IF bTrevHeliToMichaelCar_TeleportToPos
		SET_PED_COORDS_KEEP_VEHICLE(peds[mpf_trevor].id, (<<1771.9, 3276.0, 41.4>>))
		bTrevHeliToMichaelCar_TeleportToPos = FALSE
	ENDIF
	
	IF bSetupTrevorToMichaelHeliSwitch
	
		DEBUG_SETUP_TREVOR_TO_MICHAEL_HELI_SWITCH(TRUE)
		bSetupTrevorToMichaelHeliSwitch = FALSE
	ENDIF
	
	IF bSetupMichaelToTrevorHeliSwitch
		DEBUG_SETUP_TREVOR_TO_MICHAEL_HELI_SWITCH(FALSE)
		bSetupMichaelToTrevorHeliSwitch = FALSE
	ENDIF
	
	IF bDEBUG_IsInIsolatedDebugMode
		IF IS_BUTTON_JUST_PRESSED(PAD1, DPADLEFT)
			bDEBUG_SwitchCamIsActive = TRUE
		ENDIF
	ENDIF
	
	IF bDEBUG_SwitchCamIsActive
	
		BOOL bDEBUG_StartingSwitchOnTrevor
		
		
		IF eSwitchCamState = SWITCH_CAM_IDLE
	
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				bDEBUG_StartingSwitchOnTrevor = TRUE
			ELSE
				bDEBUG_StartingSwitchOnTrevor = FALSE
			ENDIF

			DEBUG_SETUP_TREVOR_TO_MICHAEL_HELI_SWITCH(bDEBUG_StartingSwitchOnTrevor)
			
			//[MF] Popping Trevor's Helicopter Into The Air
			IF NOT bDEBUG_StartingSwitchOnTrevor
				SET_ENTITY_COORDS(vehs[mvf_helicopter].id, << 1758.3090, 3284.5278, 80.7 >>)
			ENDIF
			eSwitchCamState = SWITCH_CAM_SETUP_SPLINE_1
		ENDIF
		
		WAIT(5000)
		sCamDetails.bOKToSwitchPed = FALSE
		
		IF bDEBUG_StartingSwitchOnTrevor 
			WHILE NOT HANDLE_SWITCH_CAM_TREVOR_IN_HELI_TO_MICHAEL(scsSwitchCamTrevorInHeliToMichael)
				WAIT(0)
				
				UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCamMichaelToTrevorInHeli)
				UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCamTrevorInHeliToMichael)				
				
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					IF sCamDetails.bOKToSwitchPed = TRUE
						P_DEBUG_FORCE_CHARACTER_SELECT(SELECTOR_PED_MICHAEL, mpf_michael)
						sCamDetails.bOKToSwitchPed = FALSE
					ENDIF
				ENDIF
			ENDWHILE
		ELSE
			WHILE NOT HANDLE_SWITCH_CAM_MICHAEL_TO_TREVOR_IN_HELI(scsSwitchCamMichaelToTrevorInHeli)
				WAIT(0)
				
				UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCamMichaelToTrevorInHeli)
				UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCamTrevorInHeliToMichael)				
				
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
					IF sCamDetails.bOKToSwitchPed = TRUE
						P_DEBUG_FORCE_CHARACTER_SELECT(SELECTOR_PED_TREVOR, mpf_trevor)
						sCamDetails.bOKToSwitchPed = FALSE
					ENDIF
				ENDIF
			ENDWHILE
		ENDIF
		
		sCamDetails.bOKToSwitchPed = FALSE
		bDEBUG_SwitchCamIsActive = FALSE		
	ENDIF


ENDPROC

#ENDIF
// ===========================================================================================================
//		Termination

// ===========================================================================================================
// -----------------------------------------------------------------------------------------------------------
//		Mission Helpers
// -----------------------------------------------------------------------------------------------------------
PROC GET_SKIP_STAGE_COORD_AND_HEADING(MSF_MISSION_STAGE_FLAGS eStage, VECTOR &vCoord, FLOAT &fHeading,int iChar = 0)
	if iChar = 0 //mike
		SWITCH eStage
			CASE msf_0_strip_club			vCoord = <<92.0847, -1280.7062, 28.1447 >>		fHeading = 73.3971			BREAK
			CASE msf_1_font_of_bank			vCoord = 	vFRONT_BANK 						fHeading = fHEADING_FRONT	BREAK
			CASE msf_2_back_of_bank			vCoord = 	vBACK_BANK							fHeading = fHEADING_BACK	BREAK
			CASE msf_3_get_in_position		vCoord = 	vBACK_BANK							fHeading = fHEADING_BACK	BREAK
			CASE msf_4_heli_follow_truck	vCoord = <<1095.7894, -1421.1515, 37.5>> 		fHeading = -168				BREAK
			//CASE msf_4_heli_follow_truck	vCoord = <<1095.7894, -1421.1515,102.4156>> 	fHeading = -168				BREAK	
			CASE msf_5_CONSTRUCTION_HOLE	vCoord = <<-76.4238,  -528.6735, 80	 >>			fHeading = 176.7705			BREAK	//87.5000
			CASE msf_6_drop_off_point		vCoord = << 7.0467, -614.5734, 61.0681 >>		fHeading = 187.9638			BREAK
			CASE msf_7_passed				vCoord = <<19.2811, 534.3147, 173.6274>> 		fHeading = 	189.0861		BREAK
		ENDSWITCH
	elif iChar = 1 //trev
		SWITCH eStage
			CASE msf_0_strip_club			vCoord = <<92.0847, -1280.7062, 28.1447 >>		fHeading = 73.3971			BREAK
			CASE msf_1_font_of_bank			vCoord = <<1681.2954, 1320.6780, 86.0397 >>		fHeading = 346.5002 		BREAK
			CASE msf_2_back_of_bank			vCoord = <<1993.6239, 2541.7837, 53.6037>>		fHeading = 318.9926	  		BREAK
			CASE msf_3_get_in_position		vCoord = <<1758.3090, 3284.5278, 40.7 >> 		fHeading = 133.1852			BREAK	
			CASE msf_4_heli_follow_truck	vCoord = <<1095.7894, -1421.1515, 37.5>> 		fHeading = -168				BREAK
			//CASE msf_4_heli_follow_truck	vCoord = <<1095.7894, -1421.1515,102.4156>> 	fHeading = -168				BREAK	
			CASE msf_5_CONSTRUCTION_HOLE	vCoord = <<-76.4238, -528.6735, 80 >>			fHeading = 176.7705			BREAK	//87.5
			CASE msf_6_drop_off_point		vCoord = << 7.0467, -614.5734, 61.0681 >>		fHeading = 187.9638			BREAK
			case msf_7_Passed				vCoord = <<1759.5400, 3290.2327, 40.1387>>  	fHeading = 259.1121			BREAK
		ENDSWITCH
	endif
	CPRINTLN(DEBUG_MISSION,"vCoord: ", vCoord, " fheading: ", fHeading, " iChar: ", iChar)
ENDPROC
FUNC BOOL IsEntityAlive(ENTITY_INDEX mEntity)
  	If DOES_ENTITY_EXIST(mEntity)
		if IS_ENTITY_A_VEHICLE(mEntity)
			if IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(mEntity))
				return TRUE 
			endif
		elif IS_ENTITY_A_PED(mEntity)
			if not IS_PED_INJURED(GET_PED_INDEX_FROM_ENTITY_INDEX(mEntity))
	          	return TRUE 
	    	ENDIF	
		endif           
	ENDIF
      return FALSE
ENDFUNC
FUNC BOOL INIT_CAM()
	switch iSFstage
		case 0
			sfmov = REQUEST_SCALEFORM_MOVIE("SECURITY_CAM")
			iSFstage++
		break
		case 1
			IF HAS_SCALEFORM_MOVIE_LOADED(sfmov)
				BEGIN_SCALEFORM_MOVIE_METHOD(sfmov, "SET_LAYOUT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
				END_SCALEFORM_MOVIE_METHOD()
				BEGIN_SCALEFORM_MOVIE_METHOD(sfmov, "SET_LOCATION")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("FH1_REC")
				END_SCALEFORM_MOVIE_METHOD()
				iSFstage++
				RETURN TRUE
			ENDIF
		break
		case 2
			RETURN TRUE
		break
	endswitch	
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Handles all the drawing of the camera UI 
PROC DO_CAM_UI()
	
	BEGIN_SCALEFORM_MOVIE_METHOD(sfmov, "SET_TIME")
		IF GET_CLOCK_HOURS() >= 0 AND GET_CLOCK_HOURS() <= 12
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_HOURS())
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_HOURS() - 12)
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(GET_CLOCK_MINUTES())
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(00)
		IF GET_CLOCK_HOURS() >= 0 AND GET_CLOCK_HOURS() < 12
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LSH_TIMEAM")
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("LSH_TIMEPM")
		ENDIF
	END_SCALEFORM_MOVIE_METHOD()	
	
	DRAW_SCALEFORM_MOVIE_FULLSCREEN(sfmov,255,255,255,0)
	SET_TIMECYCLE_MODIFIER("scanline_cam_cheap")
	
	DISABLE_CONTROL_ACTION(player_control,INPUT_VEH_EXIT)
	
	if HAS_SOUND_FINISHED(camhum)
		PLAY_SOUND_FRONTEND(camhum,"Camera_Hum","BIG_SCORE_SETUP_SOUNDS")
	endif
	
ENDPROC 
proc stop_mission_audio_scenes(int exclude = 0)
	
	if IS_AUDIO_SCENE_ACTIVE("BS_1_drive_START")
	and exclude != 1
		STOP_AUDIO_SCENE("BS_1_drive_START")
	endif	
	
	if IS_AUDIO_SCENE_ACTIVE("BS_1_BANK_CHECK_OUT")
	and exclude != 2
		STOP_AUDIO_SCENE("BS_1_BANK_CHECK_OUT")
	endif	
	
	if IS_AUDIO_SCENE_ACTIVE("BS_1_BANK_HINT_CAM")
	and exclude != 3
		STOP_AUDIO_SCENE("BS_1_BANK_HINT_CAM")
	endif
	
	if IS_AUDIO_SCENE_ACTIVE("BS_1_DRIVE_AROUND_BACK")
	and exclude != 4
		STOP_AUDIO_SCENE("BS_1_DRIVE_AROUND_BACK")
	endif
	
	if IS_AUDIO_SCENE_ACTIVE("BS_1_SWITCH_TO_TREVOR")
	and exclude != 5
		STOP_AUDIO_SCENE("BS_1_SWITCH_TO_TREVOR")
	endif
	
	if IS_AUDIO_SCENE_ACTIVE("BS_1_FLY_TO_TRUCKS")
	and exclude != 6
		STOP_AUDIO_SCENE("BS_1_FLY_TO_TRUCKS")
	endif	
	
	if IS_AUDIO_SCENE_ACTIVE("BS_1_TRUCKS_SPOTTED")
	and exclude != 7
		STOP_AUDIO_SCENE("BS_1_TRUCKS_SPOTTED")
	endif			
	
	if IS_AUDIO_SCENE_ACTIVE("BS_1_TRUCKS_HINT_CAM")
	and exclude != 8
		STOP_AUDIO_SCENE("BS_1_TRUCKS_HINT_CAM")
	endif
	
	if IS_AUDIO_SCENE_ACTIVE("BS_1_TRUCKS_ENTER_TUNNEL")
	and exclude != 9
		STOP_AUDIO_SCENE("BS_1_TRUCKS_ENTER_TUNNEL")
	endif	
	
	if IS_AUDIO_SCENE_ACTIVE("BS_1_SEARCH_FOR_ENTRANCE")
	and exclude != 10
		STOP_AUDIO_SCENE("BS_1_SEARCH_FOR_ENTRANCE")
	endif		
	
	if IS_AUDIO_SCENE_ACTIVE("BS_1_VIEW_RECORDING")
	and exclude != 11
		STOP_AUDIO_SCENE("BS_1_VIEW_RECORDING")
	endif	
	
	if IS_AUDIO_SCENE_ACTIVE("BS_1_HELI_RETURN_TO_AIRSTRIP")
	and exclude != 12
		STOP_AUDIO_SCENE("BS_1_HELI_RETURN_TO_AIRSTRIP")
	endif
	
	if IS_AUDIO_SCENE_ACTIVE("BS_1_CAR_RETURN_TO_FRANKLINS")
	and exclude != 13
		STOP_AUDIO_SCENE("BS_1_CAR_RETURN_TO_FRANKLINS")
	endif
endproc
proc Security_gate_setup(BUILDING_STATE_ENUM bstate = BUILDINGSTATE_NORMAL)		
	SET_BUILDING_STATE(BUILDINGNAME_ES_BANK_CAR_PARK_SHUTTERS,bstate)
endproc
PROC REQUEST_TRAFFIC_IN_ADVANCE(FLOAT fCutoffTime)
	IF fCurrentPlaybackTime < fCutoffTime
		INT iuber = 0
		REPEAT COUNT_OF(TrafficCarID) iuber
			IF TrafficCarRecording[iuber] != 0
				IF TrafficCarStartime[iuber] < fCutoffTime
					REQUEST_MODEL(TrafficCarModel[iuber])
					REQUEST_VEHICLE_RECORDING(TrafficCarRecording[iuber], "FH1UBER")
				ENDIF
			ENDIF
		ENDREPEAT
		
		REPEAT COUNT_OF(SetPieceCarID) iuber
			IF SetPieceCarRecording[iuber] != 0
				IF SetPieceCarStartime[iuber] < fCutoffTime
					IF NOT DOES_ENTITY_EXIST(SetPieceCarID[iuber])
						REQUEST_MODEL(SetPieceCarModel[iuber])
					ENDIF
					REQUEST_VEHICLE_RECORDING(SetPieceCarRecording[iuber], "FH1UBER")
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC
PROC LOAD_UBER_CHASE_DATA()
	TrafficCarPos[0] = <<1094.9021, -1724.9916, 28.7513>>
	TrafficCarQuatX[0] = 0.0003
	TrafficCarQuatY[0] = -0.0020
	TrafficCarQuatZ[0] = 0.9865
	TrafficCarQuatW[0] = -0.1636
	TrafficCarRecording[0] = 1
	TrafficCarStartime[0] = 2970.0000
	TrafficCarModel[0] = premier//stratum//penumbra

	TrafficCarPos[1] = <<1124.5498, -1741.1417, 28.9311>>
	TrafficCarQuatX[1] = 0.0008
	TrafficCarQuatY[1] = -0.0002
	TrafficCarQuatZ[1] = 0.1767
	TrafficCarQuatW[1] = 0.9843
	TrafficCarRecording[1] = 2
	TrafficCarStartime[1] = 3102.0000
	TrafficCarModel[1] = serrano//rancherxl 

	TrafficCarPos[2] = <<1098.3792, -1748.8092, 35.1966>>
	TrafficCarQuatX[2] = -0.0009
	TrafficCarQuatY[2] = 0.0009
	TrafficCarQuatZ[2] = -0.5723
	TrafficCarQuatW[2] = 0.8200
	TrafficCarRecording[2] = 3
	TrafficCarStartime[2] = 3564.0000
	TrafficCarModel[2] = premier 

	TrafficCarPos[3] = <<1079.2882, -1712.6084, 29.0574>>
	TrafficCarQuatX[3] = 0.0000
	TrafficCarQuatY[3] = -0.0001
	TrafficCarQuatZ[3] = 0.9870
	TrafficCarQuatW[3] = -0.1608
	TrafficCarRecording[3] = 4
	TrafficCarStartime[3] = 4092.0000
	TrafficCarModel[3] = serrano//rancherxl

	TrafficCarPos[4] = <<1084.3353, -1748.2080, 35.4187>>
	TrafficCarQuatX[4] = 0.0002
	TrafficCarQuatY[4] = -0.0001
	TrafficCarQuatZ[4] = -0.5776
	TrafficCarQuatW[4] = 0.8163
	TrafficCarRecording[4] = 5
	TrafficCarStartime[4] = 4488.0000
	TrafficCarModel[4] = serrano//utillitruck3 

	TrafficCarPos[5] = <<1059.9586, -1730.4474, 35.2553>>
	TrafficCarQuatX[5] = -0.0013
	TrafficCarQuatY[5] = 0.0083
	TrafficCarQuatZ[5] = 0.9843
	TrafficCarQuatW[5] = -0.1765
	TrafficCarRecording[5] = 6
	TrafficCarStartime[5] = 5610.0000
	TrafficCarModel[5] = serrano//rancherxl

	TrafficCarPos[6] = <<1057.1438, -1722.0253, 35.1296>>
	TrafficCarQuatX[6] = -0.0015
	TrafficCarQuatY[6] = 0.0094
	TrafficCarQuatZ[6] = 0.9877
	TrafficCarQuatW[6] = -0.1560
	TrafficCarRecording[6] = 7
	TrafficCarStartime[6] = 5742.0000
	TrafficCarModel[6] = serrano//utillitruck3

	TrafficCarPos[7] = <<1066.2435, -1754.8478, 35.3732>>
	TrafficCarQuatX[7] = -0.0011
	TrafficCarQuatY[7] = 0.0006
	TrafficCarQuatZ[7] = -0.5739
	TrafficCarQuatW[7] = 0.8189
	TrafficCarRecording[7] = 8
	TrafficCarStartime[7] = 5874.0000
	TrafficCarModel[7] = serrano//rancherxl

	TrafficCarPos[8] = <<1059.5776, -1759.7831, 35.0615>>
	TrafficCarQuatX[8] = 0.0005
	TrafficCarQuatY[8] = 0.0000
	TrafficCarQuatZ[8] = -0.4766
	TrafficCarQuatW[8] = 0.8791
	TrafficCarRecording[8] = 9
	TrafficCarStartime[8] = 6402.0000
	TrafficCarModel[8] = premier//peyote 

	TrafficCarPos[9] = <<1057.0295, -1757.4359, 35.1548>>
	TrafficCarQuatX[9] = -0.0005
	TrafficCarQuatY[9] = 0.0007
	TrafficCarQuatZ[9] = -0.5842
	TrafficCarQuatW[9] = 0.8116
	TrafficCarRecording[9] = 10
	TrafficCarStartime[9] = 6468.0000
	TrafficCarModel[9] = premier//stratum 

	TrafficCarPos[10] = <<1027.8516, -1770.6709, 35.2252>>
	TrafficCarQuatX[10] = 0.0414
	TrafficCarQuatY[10] = -0.0311
	TrafficCarQuatZ[10] = -0.6375
	TrafficCarQuatW[10] = 0.7687
	TrafficCarRecording[10] = 11
	TrafficCarStartime[10] = 8250.0000
	TrafficCarModel[10] = premier//manana 

	TrafficCarPos[11] = <<867.5558, -1748.6525, 29.4237>>
	TrafficCarQuatX[11] = -0.0075
	TrafficCarQuatY[11] = -0.0064
	TrafficCarQuatZ[11] = 0.6388
	TrafficCarQuatW[11] = 0.7693
	TrafficCarRecording[11] = 12
	TrafficCarStartime[11] = 17754.0000
	TrafficCarModel[11] = serrano//rancherxl

	TrafficCarPos[12] = <<860.6240, -1747.1852, 28.9555>>
	TrafficCarQuatX[12] = -0.0082
	TrafficCarQuatY[12] = -0.0064
	TrafficCarQuatZ[12] = 0.6768
	TrafficCarQuatW[12] = 0.7361
	TrafficCarRecording[12] = 13
	TrafficCarStartime[12] = 18216.0000
	TrafficCarModel[12] = premier//stratum//peyote

	TrafficCarPos[13] = <<860.9755, -1742.0430, 29.0624>>
	TrafficCarQuatX[13] = -0.0066
	TrafficCarQuatY[13] = -0.0052
	TrafficCarQuatZ[13] = 0.6624
	TrafficCarQuatW[13] = 0.7491
	TrafficCarRecording[13] = 14
	TrafficCarStartime[13] = 18216.0000
	TrafficCarModel[13] = premier//penumbra

	TrafficCarPos[14] = <<845.2426, -1762.8329, 28.6199>>
	TrafficCarQuatX[14] = 0.0017
	TrafficCarQuatY[14] = -0.0013
	TrafficCarQuatZ[14] = -0.6127
	TrafficCarQuatW[14] = 0.7903
	TrafficCarRecording[14] = 15
	TrafficCarStartime[14] = 19668.0000
	TrafficCarModel[14] = premier//stratum

	TrafficCarPos[15] = <<837.1574, -1723.8857, 28.7141>>
	TrafficCarQuatX[15] = -0.0005
	TrafficCarQuatY[15] = 0.0000
	TrafficCarQuatZ[15] = -0.0404
	TrafficCarQuatW[15] = 0.9992
	TrafficCarRecording[15] = 16
	TrafficCarStartime[15] = 19932.0000
	TrafficCarModel[15] = premier//stratum

	TrafficCarPos[16] = <<801.6107, -1752.7430, 28.7486>>
	TrafficCarQuatX[16] = 0.0028
	TrafficCarQuatY[16] = -0.0031
	TrafficCarQuatZ[16] = 0.7449
	TrafficCarQuatW[16] = -0.6671
	TrafficCarRecording[16] = 17
	TrafficCarStartime[16] = 21780.0000
	TrafficCarModel[16] = premier//manana

	TrafficCarPos[17] = <<789.3914, -1751.3726, 28.8925>>
	TrafficCarQuatX[17] = 0.0032
	TrafficCarQuatY[17] = -0.0037
	TrafficCarQuatZ[17] = 0.7530
	TrafficCarQuatW[17] = -0.6580
	TrafficCarRecording[17] = 18
	TrafficCarStartime[17] = 22506.0000
	TrafficCarModel[17] = premier

	TrafficCarPos[18] = <<768.2957, -1744.7550, 29.0428>>
	TrafficCarQuatX[18] = 0.0008
	TrafficCarQuatY[18] = -0.0008
	TrafficCarQuatZ[18] = -0.6955
	TrafficCarQuatW[18] = 0.7185
	TrafficCarRecording[18] = 19
	TrafficCarStartime[18] = 24420.0000
	TrafficCarModel[18] = premier//stratum//manana

	TrafficCarPos[19] = <<848.1189, -1592.2108, 31.3671>>
	TrafficCarQuatX[19] = -0.0098
	TrafficCarQuatY[19] = -0.0003
	TrafficCarQuatZ[19] = 0.0308
	TrafficCarQuatW[19] = 0.9995
	TrafficCarRecording[19] = 20
	TrafficCarStartime[19] = 29766.0000
	TrafficCarModel[19] = serrano//rancherxl//tiptruck 

	TrafficCarPos[20] = <<789.3643, -1395.0581, 26.5083>>
	TrafficCarQuatX[20] = -0.0005
	TrafficCarQuatY[20] = 0.0083
	TrafficCarQuatZ[20] = 0.9999
	TrafficCarQuatW[20] = -0.0085
	TrafficCarRecording[20] = 21
	TrafficCarStartime[20] = 41008.0000
	TrafficCarModel[20] = premier//penumbra

	ParkedCarPos[0] = <<707.8363, -1395.9775, 25.8513>>
	ParkedCarQuatX[0] = -0.0024
	ParkedCarQuatY[0] = 0.0013
	ParkedCarQuatZ[0] = 0.8060
	ParkedCarQuatW[0] = 0.5919
	ParkedCarModel[0] = premier//stratum//penumbra

	ParkedCarPos[1] = <<709.5156, -1401.8848, 25.9322>>
	ParkedCarQuatX[1] = -0.0006
	ParkedCarQuatY[1] = -0.0007
	ParkedCarQuatZ[1] = 0.7992
	ParkedCarQuatW[1] = 0.6011
	ParkedCarModel[1] = premier

	TrafficCarPos[21] = <<656.9852, -1438.7355, 30.5204>>
	TrafficCarQuatX[21] = -0.0061
	TrafficCarQuatY[21] = 0.0032
	TrafficCarQuatZ[21] = 0.7272
	TrafficCarQuatW[21] = -0.6864
	TrafficCarRecording[21] = 22
	TrafficCarStartime[21] = 52624.0000
	TrafficCarModel[21] = serrano//rancherxl

	TrafficCarPos[22] = <<649.5815, -1443.6371, 30.0296>>
	TrafficCarQuatX[22] = -0.0048
	TrafficCarQuatY[22] = 0.0041
	TrafficCarQuatZ[22] = 0.7389
	TrafficCarQuatW[22] = -0.6738
	TrafficCarRecording[22] = 23
	TrafficCarStartime[22] = 53812.0000
	TrafficCarModel[22] = premier//penumbra

	TrafficCarPos[23] = <<538.8035, -1418.0469, 28.6650>>
	TrafficCarQuatX[23] = 0.0003
	TrafficCarQuatY[23] = 0.0000
	TrafficCarQuatZ[23] = 0.0253
	TrafficCarQuatW[23] = 0.9997
	TrafficCarRecording[23] = 24
	TrafficCarStartime[23] = 59290.0000
	TrafficCarModel[23] = premier//stratum//peyote

	TrafficCarPos[24] = <<533.0499, -1416.2839, 28.9904>>
	TrafficCarQuatX[24] = 0.0000
	TrafficCarQuatY[24] = 0.0002
	TrafficCarQuatZ[24] = 1.0000
	TrafficCarQuatW[24] = 0.0034
	TrafficCarRecording[24] = 25
	TrafficCarStartime[24] = 59620.0000
	TrafficCarModel[24] = serrano//rancherxl

	TrafficCarPos[25] = <<484.8600, -1444.9907, 28.7354>>
	TrafficCarQuatX[25] = 0.0004
	TrafficCarQuatY[25] = 0.0002
	TrafficCarQuatZ[25] = -0.7033
	TrafficCarQuatW[25] = 0.7109
	TrafficCarRecording[25] = 26
	TrafficCarStartime[25] = 61996.0000
	TrafficCarModel[25] = premier//penumbra

	TrafficCarPos[26] = <<471.0496, -1415.1317, 28.9925>>
	TrafficCarQuatX[26] = -0.0002
	TrafficCarQuatY[26] = -0.0002
	TrafficCarQuatZ[26] = 0.5543
	TrafficCarQuatW[26] = 0.8323
	TrafficCarRecording[26] = 27
	TrafficCarStartime[26] = 62788.0000
	TrafficCarModel[26] = premier//stratum//ingot 

	ParkedCarPos[2] = <<501.9684, -1337.3604, 28.7765>>
	ParkedCarQuatX[2] = -0.0015
	ParkedCarQuatY[2] = 0.0014
	ParkedCarQuatZ[2] = -0.4395
	ParkedCarQuatW[2] = 0.8982
	ParkedCarModel[2] = premier//penumbra

	ParkedCarPos[3] = <<503.8632, -1340.4531, 28.8254>>
	ParkedCarQuatX[3] = 0.0006
	ParkedCarQuatY[3] = 0.0004
	ParkedCarQuatZ[3] = -0.3988
	ParkedCarQuatW[3] = 0.9170
	ParkedCarModel[3] = premier 

	TrafficCarPos[27] = <<521.3064, -1324.2755, 28.8192>>
	TrafficCarQuatX[27] = 0.0012
	TrafficCarQuatY[27] = 0.0014
	TrafficCarQuatZ[27] = 0.3395
	TrafficCarQuatW[27] = 0.9406
	TrafficCarRecording[27] = 28
	TrafficCarStartime[27] = 66154.0000
	TrafficCarModel[27] = premier//stratum//manana
//
//	ParkedCarPos[4] = <<519.8814, -1343.1223, 28.8116>>
//	ParkedCarQuatX[4] = -0.0018
//	ParkedCarQuatY[4] = 0.0088
//	ParkedCarQuatZ[4] = 0.2345
//	ParkedCarQuatW[4] = 0.9721
//	ParkedCarModel[4] = penumbra
//
//	ParkedCarPos[5] = <<515.7321, -1344.2231, 28.8422>>
//	ParkedCarQuatX[5] = -0.0079
//	ParkedCarQuatY[5] = -0.0082
//	ParkedCarQuatZ[5] = 0.0867
//	ParkedCarQuatW[5] = 0.9962
//	ParkedCarModel[5] = premier

	TrafficCarPos[28] = <<493.0157, -1260.1964, 28.8755>>
	TrafficCarQuatX[28] = 0.0022
	TrafficCarQuatY[28] = -0.0021
	TrafficCarQuatZ[28] = 0.7097
	TrafficCarQuatW[28] = -0.7045
	TrafficCarRecording[28] = 29
	TrafficCarStartime[28] = 70444.0000
	TrafficCarModel[28] = premier//manana

	TrafficCarPos[29] = <<485.2056, -1193.5140, 41.2846>>
	TrafficCarQuatX[29] = -0.0029
	TrafficCarQuatY[29] = -0.0028
	TrafficCarQuatZ[29] = 0.6941
	TrafficCarQuatW[29] = 0.7199
	TrafficCarRecording[29] = 30
	TrafficCarStartime[29] = 75130.0000
	TrafficCarModel[29] = premier//stratum//penumbra

	TrafficCarPos[30] = <<504.7741, -1176.6111, 28.8833>>
	TrafficCarQuatX[30] = -0.0020
	TrafficCarQuatY[30] = -0.0000
	TrafficCarQuatZ[30] = 0.0099
	TrafficCarQuatW[30] = 0.9999
	TrafficCarRecording[30] = 31
	TrafficCarStartime[30] = 75526.0000
	TrafficCarModel[30] = premier//ingot

	TrafficCarPos[31] = <<500.8426, -1132.7350, 29.0899>>
	TrafficCarQuatX[31] = -0.0001
	TrafficCarQuatY[31] = 0.0000
	TrafficCarQuatZ[31] = -0.3028
	TrafficCarQuatW[31] = 0.9531
	TrafficCarRecording[31] = 32
	TrafficCarStartime[31] = 78298.0000
	TrafficCarModel[31] = serrano//rancherxl

	TrafficCarPos[32] = <<481.2516, -1134.4500, 28.9857>>
	TrafficCarQuatX[32] = -0.0001
	TrafficCarQuatY[32] = 0.0001
	TrafficCarQuatZ[32] = 0.7071
	TrafficCarQuatW[32] = -0.7071
	TrafficCarRecording[32] = 33
	TrafficCarStartime[32] = 78430.0000
	TrafficCarModel[32] = premier

	TrafficCarPos[33] = <<492.8828, -1128.5598, 28.9559>>
	TrafficCarQuatX[33] = 0.0010
	TrafficCarQuatY[33] = 0.0010
	TrafficCarQuatZ[33] = 0.7238
	TrafficCarQuatW[33] = 0.6900
	TrafficCarRecording[33] = 34
	TrafficCarStartime[33] = 78562.0000
	TrafficCarModel[33] = premier

	TrafficCarPos[34] = <<497.8196, -1104.1888, 28.8836>>
	TrafficCarQuatX[34] = -0.0000
	TrafficCarQuatY[34] = 0.0061
	TrafficCarQuatZ[34] = 1.0000
	TrafficCarQuatW[34] = -0.0014
	TrafficCarRecording[34] = 35
	TrafficCarStartime[34] = 80344.0000
	TrafficCarModel[34] = serrano//rancherxl

	TrafficCarPos[35] = <<470.4125, -1032.5781, 33.3936>>
	TrafficCarQuatX[35] = -0.0252
	TrafficCarQuatY[35] = -0.0286
	TrafficCarQuatZ[35] = 0.7504
	TrafficCarQuatW[35] = 0.6599
	TrafficCarRecording[35] = 36
	TrafficCarStartime[35] = 88858.0000
	TrafficCarModel[35] = DUMMY_MODEL_FOR_SCRIPT //manana

	TrafficCarPos[36] = <<380.9980, -1134.3085, 28.9309>>
	TrafficCarQuatX[36] = 0.0024
	TrafficCarQuatY[36] = 0.0025
	TrafficCarQuatZ[36] = 0.7109
	TrafficCarQuatW[36] = -0.7032
	TrafficCarRecording[36] = 37
	TrafficCarStartime[36] = 90574.0000
	TrafficCarModel[36] = premier//stratum//manana

	ParkedCarPos[6] = <<372.9772, -1136.5481, 29.1298>>
	ParkedCarQuatX[6] = -0.0186
	ParkedCarQuatY[6] = -0.0177
	ParkedCarQuatZ[6] = 0.7270
	ParkedCarQuatW[6] = -0.6861
	ParkedCarModel[6] = serrano//burrito 

	TrafficCarPos[37] = <<328.8199, -1134.4833, 28.9543>>
	TrafficCarQuatX[37] = -0.0001
	TrafficCarQuatY[37] = 0.0001
	TrafficCarQuatZ[37] = -0.7043
	TrafficCarQuatW[37] = 0.7099
	TrafficCarRecording[37] = 38
	TrafficCarStartime[37] = 95194.0000
	TrafficCarModel[37] = premier

	TrafficCarPos[38] = <<321.5484, -1134.5587, 29.0881>>
	TrafficCarQuatX[38] = -0.0002
	TrafficCarQuatY[38] = 0.0002
	TrafficCarQuatZ[38] = -0.7034
	TrafficCarQuatW[38] = 0.7108
	TrafficCarRecording[38] = 39
	TrafficCarStartime[38] = 96052.0000
	TrafficCarModel[38] = DUMMY_MODEL_FOR_SCRIPT //rancherxl

	TrafficCarPos[39] = <<374.7384, -1053.8479, 28.9763>>
	TrafficCarQuatX[39] = 0.0007
	TrafficCarQuatY[39] = -0.0006
	TrafficCarQuatZ[39] = -0.6988
	TrafficCarQuatW[39] = 0.7153
	TrafficCarRecording[39] = 40
	TrafficCarStartime[39] = 100936.0000
	TrafficCarModel[39] = premier//ruiner

	TrafficCarPos[40] = <<382.6972, -1053.6361, 28.8843>>
	TrafficCarQuatX[40] = -0.0011
	TrafficCarQuatY[40] = 0.0010
	TrafficCarQuatZ[40] = -0.7014
	TrafficCarQuatW[40] = 0.7128
	TrafficCarRecording[40] = 41
	TrafficCarStartime[40] = 100936.0000
	TrafficCarModel[40] = serrano//rancherxl

	TrafficCarPos[41] = <<396.9267, -978.9899, 29.0242>>
	TrafficCarQuatX[41] = -0.0000
	TrafficCarQuatY[41] = -0.0009
	TrafficCarQuatZ[41] = 0.9998
	TrafficCarQuatW[41] = 0.0211
	TrafficCarRecording[41] = 42
	TrafficCarStartime[41] = 111232.0000
	TrafficCarModel[41] = serrano//rancherxl

	TrafficCarPos[42] = <<387.2838, -957.0242, 29.0587>>
	TrafficCarQuatX[42] = 0.0002
	TrafficCarQuatY[42] = -0.0004
	TrafficCarQuatZ[42] = 0.7264
	TrafficCarQuatW[42] = -0.6873
	TrafficCarRecording[42] = 43
	TrafficCarStartime[42] = 117370.0000
	TrafficCarModel[42] = serrano//rancherxl

	TrafficCarPos[43] = <<378.7138, -956.7831, 28.9829>>
	TrafficCarQuatX[43] = 0.0000
	TrafficCarQuatY[43] = -0.0000
	TrafficCarQuatZ[43] = 0.7138
	TrafficCarQuatW[43] = -0.7004
	TrafficCarRecording[43] = 44
	TrafficCarStartime[43] = 117436.0000
	TrafficCarModel[43] = serrano//rebel2

	TrafficCarPos[44] = <<400.5379, -936.7937, 28.9378>>
	TrafficCarQuatX[44] = -0.0000
	TrafficCarQuatY[44] = -0.0006
	TrafficCarQuatZ[44] = 0.9998
	TrafficCarQuatW[44] = 0.0187
	TrafficCarRecording[44] = 45
	TrafficCarStartime[44] = 119086.0000
	TrafficCarModel[44] = premier

	TrafficCarPos[45] = <<408.7719, -877.9841, 28.9611>>
	TrafficCarQuatX[45] = 0.0036
	TrafficCarQuatY[45] = -0.0001
	TrafficCarQuatZ[45] = -0.0204
	TrafficCarQuatW[45] = 0.9998
	TrafficCarRecording[45] = 46
	TrafficCarStartime[45] = 122650.0000
	TrafficCarModel[45] = premier//stratum//ingot

	TrafficCarPos[46] = <<421.5898, -849.4219, 29.2132>>
	TrafficCarQuatX[46] = -0.0565
	TrafficCarQuatY[46] = -0.0543
	TrafficCarQuatZ[46] = 0.7197
	TrafficCarQuatW[46] = 0.6898
	TrafficCarRecording[46] = 47
	TrafficCarStartime[46] = 124432.0000
	TrafficCarModel[46] = premier//manana

	TrafficCarPos[47] = <<422.5729, -821.7974, 28.8209>>
	TrafficCarQuatX[47] = 0.0192
	TrafficCarQuatY[47] = 0.0192
	TrafficCarQuatZ[47] = 0.7070
	TrafficCarQuatW[47] = 0.7067
	TrafficCarRecording[47] = 48
	TrafficCarStartime[47] = 125818.0000
	TrafficCarModel[47] = serrano//rancherxl

	TrafficCarPos[48] = <<403.5500, -810.9785, 28.8325>>
	TrafficCarQuatX[48] = -0.0000
	TrafficCarQuatY[48] = 0.0000
	TrafficCarQuatZ[48] = 1.0000
	TrafficCarQuatW[48] = -0.0000
	TrafficCarRecording[48] = 49
	TrafficCarStartime[48] = 126346.0000
	TrafficCarModel[48] = premier

	TrafficCarPos[49] = <<408.7001, -694.7044, 28.9669>>
	TrafficCarQuatX[49] = 0.0076
	TrafficCarQuatY[49] = -0.0000
	TrafficCarQuatZ[49] = 0.0000
	TrafficCarQuatW[49] = 1.0000
	TrafficCarRecording[49] = 50
	TrafficCarStartime[49] = 144757.0000
	TrafficCarModel[49] = serrano//rancherxl

	TrafficCarPos[50] = <<399.3801, -674.1376, 28.8158>>
	TrafficCarQuatX[50] = 0.0007
	TrafficCarQuatY[50] = 0.0007
	TrafficCarQuatZ[50] = 0.6916
	TrafficCarQuatW[50] = 0.7223
	TrafficCarRecording[50] = 51
	TrafficCarStartime[50] = 145813.0000
	TrafficCarModel[50] = premier

	ParkedCarPos[7] = <<409.8649, -655.0555, 28.0074>>
	ParkedCarQuatX[7] = 0.0000
	ParkedCarQuatY[7] = 0.0000
	ParkedCarQuatZ[7] = 0.7213
	ParkedCarQuatW[7] = -0.6926
	ParkedCarModel[7] = premier//stratum//dilettante

	ParkedCarPos[8] = <<392.8911, -643.9622, 28.0134>>
	ParkedCarQuatX[8] = -0.0001
	ParkedCarQuatY[8] = -0.0000
	ParkedCarQuatZ[8] = 0.7089
	ParkedCarQuatW[8] = 0.7053
	ParkedCarModel[8] = premier//dilettante

	ParkedCarPos[9] = <<429.7155, -638.2184, 28.5253>>
	ParkedCarQuatX[9] = -0.0003
	ParkedCarQuatY[9] = 0.0000
	ParkedCarQuatZ[9] = -0.0189
	ParkedCarQuatW[9] = 0.9998
	ParkedCarModel[9] = bus 

	TrafficCarPos[51] = <<333.5482, -662.7706, 28.9943>>
	TrafficCarQuatX[51] = 0.0000
	TrafficCarQuatY[51] = -0.0000
	TrafficCarQuatZ[51] = 0.7987
	TrafficCarQuatW[51] = -0.6017
	TrafficCarRecording[51] = 52
	TrafficCarStartime[51] = 152347.0000
	TrafficCarModel[51] = premier//stratum//ingot

	TrafficCarPos[52] = <<327.5386, -660.2631, 28.7691>>
	TrafficCarQuatX[52] = -0.0000
	TrafficCarQuatY[52] = 0.0001
	TrafficCarQuatZ[52] = 0.8171
	TrafficCarQuatW[52] = -0.5764
	TrafficCarRecording[52] = 53
	TrafficCarStartime[52] = 152347.0000
	TrafficCarModel[52] = premier//penumbra

	ParkedCarPos[10] = <<257.7682, -626.4667, 40.9463>>
	ParkedCarQuatX[10] = 0.0462
	ParkedCarQuatY[10] = 0.0124
	ParkedCarQuatZ[10] = -0.1716
	ParkedCarQuatW[10] = 0.9840
	ParkedCarModel[10] = serrano//rancherxl

	TrafficCarPos[53] = <<127.3681, -577.9399, 31.2777>>
	TrafficCarQuatX[53] = 0.0008
	TrafficCarQuatY[53] = -0.0012
	TrafficCarQuatZ[53] = 0.8310
	TrafficCarQuatW[53] = -0.5562
	TrafficCarRecording[53] = 54
	TrafficCarStartime[53] = 161917.0000
	TrafficCarModel[53] = premier//stratum//ingot

	TrafficCarPos[54] = <<99.9988, -544.2546, 33.5235>>
	TrafficCarQuatX[54] = -0.0017
	TrafficCarQuatY[54] = -0.0009
	TrafficCarQuatZ[54] = 0.7134
	TrafficCarQuatW[54] = -0.7008
	TrafficCarRecording[54] = 55
	TrafficCarStartime[54] = 163699.0000
	TrafficCarModel[54] = premier//ingot

//	TrafficCarPos[55] = <<102.0943, -561.2410, 43.1619>>
//	TrafficCarQuatX[55] = 0.0015
//	TrafficCarQuatY[55] = 0.0063
//	TrafficCarQuatZ[55] = 0.9840
//	TrafficCarQuatW[55] = 0.1779
//	TrafficCarRecording[55] = 56
//	TrafficCarStartime[55] = 163765.0000
//	TrafficCarModel[55] = ingot

	TrafficCarPos[56] = <<108.3187, -581.6124, 31.0713>>
	TrafficCarQuatX[56] = 0.0009
	TrafficCarQuatY[56] = -0.0004
	TrafficCarQuatZ[56] = -0.1665
	TrafficCarQuatW[56] = 0.9860
	TrafficCarRecording[56] = 57
	TrafficCarStartime[56] = 163765.0000
	TrafficCarModel[56] = premier//stratum//penumbra

//	TrafficCarPos[57] = <<97.7940, -557.4409, 43.1147>>
//	TrafficCarQuatX[57] = -0.0002
//	TrafficCarQuatY[57] = 0.0054
//	TrafficCarQuatZ[57] = 0.9843
//	TrafficCarQuatW[57] = 0.1767
//	TrafficCarRecording[57] = 58
//	TrafficCarStartime[57] = 163963.0000
//	TrafficCarModel[57] = ingot

	TrafficCarPos[58] = <<-124.8340, -690.5775, 34.6172>>
	TrafficCarQuatX[58] = -0.0011
	TrafficCarQuatY[58] = -0.0065
	TrafficCarQuatZ[58] = 0.9848
	TrafficCarQuatW[58] = 0.1733
	TrafficCarRecording[58] = 59
	TrafficCarStartime[58] = 186261.0000
	TrafficCarModel[58] = premier//ruiner

	TrafficCarPos[59] = <<-112.3217, -727.3997, 34.2949>>
	TrafficCarQuatX[59] = 0.0026
	TrafficCarQuatY[59] = -0.0020
	TrafficCarQuatZ[59] = -0.5987
	TrafficCarQuatW[59] = 0.8009
	TrafficCarRecording[59] = 60
	TrafficCarStartime[59] = 188307.0000
	TrafficCarModel[59] = premier//stratum//manana

	TrafficCarPos[60] = <<-94.2945, -712.0757, 34.2515>>
	TrafficCarQuatX[60] = -0.0092
	TrafficCarQuatY[60] = 0.0142
	TrafficCarQuatZ[60] = 0.5341
	TrafficCarQuatW[60] = 0.8453
	TrafficCarRecording[60] = 61
	TrafficCarStartime[60] = 188505.0000
	TrafficCarModel[60] = premier//manana

	TrafficCarPos[61] = <<-127.4751, -746.0173, 33.8412>>
	TrafficCarQuatX[61] = 0.0199
	TrafficCarQuatY[61] = -0.0038
	TrafficCarQuatZ[61] = -0.1870
	TrafficCarQuatW[61] = 0.9822
	TrafficCarRecording[61] = 62
	TrafficCarStartime[61] = 189957.0000
	TrafficCarModel[61] = premier//stratum//dilettante

	TrafficCarPos[62] = <<-129.9522, -753.0590, 33.7323>>
	TrafficCarQuatX[62] = 0.0233
	TrafficCarQuatY[62] = -0.0065
	TrafficCarQuatZ[62] = -0.2697
	TrafficCarQuatW[62] = 0.9626
	TrafficCarRecording[62] = 63
	TrafficCarStartime[62] = 190815.0000
	TrafficCarModel[62] = serrano//rancherxl

	TrafficCarPos[63] = <<-37.9436, -752.9625, 32.3440>>
	TrafficCarQuatX[63] = -0.0028
	TrafficCarQuatY[63] = -0.0054
	TrafficCarQuatZ[63] = 0.8178
	TrafficCarQuatW[63] = -0.5754
	TrafficCarRecording[63] = 64
	TrafficCarStartime[63] = 195171.0000
	TrafficCarModel[63] = premier//serrano 

	TrafficCarPos[64] = <<-16.8790, -743.6087, 31.9734>>
	TrafficCarQuatX[64] = 0.0066
	TrafficCarQuatY[64] = 0.0088
	TrafficCarQuatZ[64] = 0.5713
	TrafficCarQuatW[64] = 0.8206
	TrafficCarRecording[64] = 65
	TrafficCarStartime[64] = 195765.0000
	TrafficCarModel[64] = serrano

	ParkedCarPos[11] = <<48.7516, -687.4161, 43.5806>>
	ParkedCarQuatX[11] = -0.0083
	ParkedCarQuatY[11] = 0.0015
	ParkedCarQuatZ[11] = 0.9857
	ParkedCarQuatW[11] = 0.1680
	ParkedCarModel[11] = premier//stratum//dilettante

	TrafficCarPos[65] = <<57.4123, -677.5038, 31.1767>>
	TrafficCarQuatX[65] = -0.0001
	TrafficCarQuatY[65] = -0.0006
	TrafficCarQuatZ[65] = 0.9843
	TrafficCarQuatW[65] = 0.1765
	TrafficCarRecording[65] = 66
	TrafficCarStartime[65] = 203421.0000
	TrafficCarModel[65] = serrano//rebel2

	TrafficCarPos[66] = <<69.8850, -690.3309, 31.2761>>
	TrafficCarQuatX[66] = -0.0000
	TrafficCarQuatY[66] = 0.0000
	TrafficCarQuatZ[66] = -0.1711
	TrafficCarQuatW[66] = 0.9853
	TrafficCarRecording[66] = 67
	TrafficCarStartime[66] = 204147.0000
	TrafficCarModel[66] = serrano//rancherxl
	
	SetPieceCarPos[0] = <<1225.3907, -1685.3101, 39.4209>>
	SetPieceCarQuatX[0] = -0.0236
	SetPieceCarQuatY[0] = -0.0719
	SetPieceCarQuatZ[0] = 0.8377
	SetPieceCarQuatW[0] = 0.5409
	SetPieceCarRecording[0] = 101
	SetPieceCarStartime[0] = 0.0000
	SetPieceCarRecordingSpeed[0] = 1.0000
	SetPieceCarModel[0] = stockade
	vehs[mvf_security_truck_fol].id = SetPieceCarID[0]
	
ENDPROC
Proc Manage_UBER_rec(VEHICLE_INDEX veh, string recName)

      SWITCH uber_state
          // **** INITIALISE UBER PLAYBACK ****
        CASE UBER_INIT
	       	INITIALISE_UBER_PLAYBACK(recName,001,FALSE)
			SET_FORCE_UBER_PLAYBACK_TO_USE_DEFAULT_PED_MODEL(TRUE)
			SET_UBER_PLAYBACK_TO_CLEANUP_DEFAULT_PED_MODEL(TRUE)
			CREATE_ALL_WAITING_UBER_CARS()
			LOAD_UBER_CHASE_DATA()
			bPlayTrafficRecordingEvenIfPlayerIsAheadOfChase = true
			fUberPlaybackDensitySwitchOffRange = 700
			fUberPlaybackMinCreationDistance = 400	
			fCurrentPlaybackTime = 0.0
			//roads
			bCarsAreOn = false
			SWITCH_ALL_RANDOM_CARS_ON()
			
			SET_ROADS_IN_AREA(<<1184.57813, -1801.74841, 25>>,<<780.50098, -1454.51013, 37>>,false)			
			SET_ROADS_IN_AREA(<<859.14117, -1563.11743, 24>>,<<242.78966, -829.41626, 33>>,false)
			SET_ROADS_IN_AREA(<<535.71149, -552.73450, 24>>,<<35.41522, -1156.01208, 33>>,false)
			SET_ROADS_IN_AREA(<<-159.27182, -539.10400, 28>>,<<416.92444, -832.99457, 41.5>>,false)
			iRoadBlock_1 =	ADD_ROAD_NODE_SPEED_ZONE(<<824.89844, -1748.84338, 28.48285>>,29,0)
			iRoadBlock_2 =	ADD_ROAD_NODE_SPEED_ZONE(<<796.60425, -1435.00281, 26.20493>>,23.8,0) 
			iRoadBlock_3 =	ADD_ROAD_NODE_SPEED_ZONE(<<541.35822, -1431.70422, 28.34233>>,21,0)
			iRoadBlock_4 =	ADD_ROAD_NODE_SPEED_ZONE(<<499.58060, -1132.15405, 28.45514>>,18,0) // after bridge
			iRoadBlock_5 =	ADD_ROAD_NODE_SPEED_ZONE(<<398.24036, -1048.06909, 28.44339>>,22,0) 
			iRoadBlock_6 =	ADD_ROAD_NODE_SPEED_ZONE(<<403.37341, -955.11713, 28.44834>>,20,0)
			iRoadBlock_7 =	ADD_ROAD_NODE_SPEED_ZONE(<<406.04382, -853.28693, 28.33914>>,25,0)// problem zone
			iRoadBlock_8 =	ADD_ROAD_NODE_SPEED_ZONE(<<354.64478, -667.05859, 28.33901>>,27,0)//tunnel enterance		
			
	        uber_state = UBER_PLAY		
	   	BREAK
          // **** START PLAYBACK OF MAIN CAR ****
		  
	    CASE UBER_PLAY 	  
			REQUEST_TRAFFIC_IN_ADVANCE(10000)
			fCurrentPlaybackTime = 0.0
            START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(veh, 001,recName, 4, 0, DRIVINGMODE_AVOIDCARS_RECKLESS)
			SET_UBER_PLAYBACK_TO_TIME_NOW(veh,7000)
			CREATE_ALL_WAITING_UBER_CARS()
	    	uber_state = UBER_UPDATE			
	    BREAK
	   
           // **** UPDATE UBER PLAYBACK ****
	    CASE UBER_UPDATE		 
	        IF IS_VEHICLE_DRIVEABLE(veh)	
		    	IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(veh)
					fCurrentPlaybackTime = GET_TIME_POSITION_IN_RECORDING(veh)
					if not DOES_ENTITY_EXIST(vehs[mvf_security_truck_fol].id)
						vehs[mvf_security_truck_fol].id = SetPieceCarID[0]				
					endif 
	                SET_PLAYBACK_SPEED(veh, fPlaybackSpeed)
					REQUEST_TRAFFIC_IN_ADVANCE(5000)			
				 	UPDATE_UBER_PLAYBACK(veh, fPlaybackSpeed)	
									
		     	ELSE
					CLEANUP_UBER_PLAYBACK()
					REMOVE_ROAD_NODE_SPEED_ZONE(iRoadBlock_1)
					REMOVE_ROAD_NODE_SPEED_ZONE(iRoadBlock_2)
					REMOVE_ROAD_NODE_SPEED_ZONE(iRoadBlock_3)
					REMOVE_ROAD_NODE_SPEED_ZONE(iRoadBlock_4)
					REMOVE_ROAD_NODE_SPEED_ZONE(iRoadBlock_5)
					REMOVE_ROAD_NODE_SPEED_ZONE(iRoadBlock_6)
					REMOVE_ROAD_NODE_SPEED_ZONE(iRoadBlock_7)
					REMOVE_ROAD_NODE_SPEED_ZONE(iRoadBlock_8)
		     	ENDIF
			ENDIF
	    BREAK
	ENDSWITCH
endproc

PROC Point_Gameplay_cam_at_coord(float targetHeading)	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(targetHeading - GET_ENTITY_HEADING(Player_ped_id()))
endproc
func float get_pitch_for_Entity(Entity_index ent)
	
      vector vec_ba
      vector direction_vec
	  
      if isentityalive(ent)
	  and isentityalive(PLAYER_PED_ID())
     	 vec_ba = get_entity_coords(ent) - get_entity_coords(player_ped_id())
      endif
	  
      direction_vec = normalise_vector(vec_ba)

      return (atan2(direction_vec.z, vmag(<<direction_vec.x, direction_vec.y, 0.0>>)))

endfunc
FUNC ped_index TREV()
	return peds[mpf_trevor].id
ENDFUNC
FUNC ped_index MIKE()
	return peds[mpf_michael].id
endfunc
FUNC ped_index LESTER()	
	return peds[mpf_lester].id 
endfunc
FUNC ped_index FRANK()
	return peds[mpf_franklin].id
endfunc
Func bool Is_clock_time_less_than_or_equal(int h,int m)
	if GET_CLOCK_HOURS() < h
		return true
	elif GET_CLOCK_HOURS() = h
		if GET_CLOCK_MINUTES() < m
		or GET_CLOCK_MINUTES() = m
			return true
		else		
			return false 
		endif
	elif GET_CLOCK_HOURS() > h
		return false
	endif
	
	return false
endfunc

PROC countdownSound()
	INT iHours, iMinutes, iSeconds
	iHours = GET_CLOCK_HOURS()
	iMinutes = GET_CLOCK_MINUTES()
	iSeconds = GET_CLOCK_SECONDS()

	IF iCountdownAudioEvent = 0
		IF iMinutes = 25 AND iSeconds >= 30
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 1
		IF iMinutes = 26 AND iSeconds >= 0
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 2
		IF iMinutes = 26 AND iSeconds >= 30
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 3
		IF iMinutes = 27 AND iSeconds >= 0
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 4
		IF iMinutes = 27 AND iSeconds >= 30
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 5
		IF iMinutes = 28 AND iSeconds >= 0
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 6
		IF iMinutes = 28 AND iSeconds >= 15
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 7
		IF iMinutes = 28 AND iSeconds >= 30
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 8
		IF iMinutes = 28 AND iSeconds >= 45
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 9
		IF iMinutes = 29 AND iSeconds >= 0
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 10
		IF iMinutes = 29 AND iSeconds >= 15
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 11
		IF iMinutes = 29 AND iSeconds >= 30
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 12
		IF iMinutes = 29 AND iSeconds >= 45
			PLAY_SOUND_FRONTEND(-1, "10_SEC_WARNING", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ELIF iCountdownAudioEvent = 13
		IF iMinutes >= 30 AND iHours = 15
			PLAY_SOUND_FRONTEND(-1, "TIMER_STOP", "HUD_MINI_GAME_SOUNDSET")
			iCountdownAudioEvent++
		ENDIF
	ENDIF
//	PRINTLN("@@@@@@@@@@@@@@@ iCountdownAudioEvent:", iCountdownAudioEvent, " iHours:", iHours, " iMinutes:", iMinutes, " iSeconds:", iSeconds, " @@@@@@@@@@@@@@@@@@@")

ENDPROC

BOOL bWentInBridge1
BOOL bWentOutBridge1
BOOL bWentInBridge2
BOOL bWentOutBridge2
BOOl bSetUnderTheBridgeStat 

PROC manage_bridges_achievement()
		
	//INT iTemp
	VECTOR vClosestNode, vHeliCoords
	FLOAT fHeliMaxRange_X, fHeliMinRange_X, fHeliMaxRange_Y, fHeliMinRange_Y
	FLOAT fCurDistSq1 
	FLOAT fCurDistSq2	
	
	IF IS_VEHICLE_DRIVEABLE(vehs[mvf_helicopter].id)
		IF bBridge = FALSE
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehs[mvf_helicopter].id)
			AND NOT IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_helicopter].id, <<335.038849,-659.229736,40.802753>>, <<79.029335,-561.329224,28.069180>>, 34.687500)
//				REPEAT 32 iTemp
//					IF IS_BIT_SET(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags, iTemp)
//						PRINTSTRING("BRIDGE 1")PRINTNL()
//						bBridge = TRUE
//					ENDIF
//					IF IS_BIT_SET(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags2, iTemp)
//						PRINTSTRING("BRIDGE 2")PRINTNL()
//						bBridge = TRUE
//					ENDIF
//					IF IS_BIT_SET(g_savedGlobals.sAmbient.iBridgesFlownUnderFlags3, iTemp)
//						PRINTSTRING("BRIDGE 3")PRINTNL()
//						bBridge = TRUE
//					ENDIF
//				ENDREPEAT
				fCurDistSq1 = VDIST2(<<636.83197, -1428.86707, 8.60403>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) 
				IF fCurDistSq1 < 10000.0 //This is 100m
					IF IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_helicopter].id, <<599.864319,-1452.673584,7.386136>>, <<688.002136,-1451.465210,27.684410>>, 10.750000)
						bWentInBridge1 = TRUE
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA( vehs[mvf_helicopter].id, <<678.311707,-1413.951538,7.958761>>, <<589.502563,-1416.228882,27.585014>>, 10.750000)
						bWentOutBridge1 = TRUE
					ENDIF
				ENDIF
				
				fCurDistSq2 =VDIST2(<<592.38153, -1180.68628, 8.87134>>,GET_ENTITY_COORDS(PLAYER_PED_ID()))//This is 200m
				IF fCurDistSq2 < 20000.0 //This is 100m
					IF IS_ENTITY_IN_ANGLED_AREA( vehs[mvf_helicopter].id, <<491.187988,-1233.989258,6.291784>>, <<687.082397,-1250.511841,40.418732>>, 14.250000)
						bWentInBridge2 = TRUE
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_helicopter].id, <<489.855194,-1178.115356,6.291779>>, <<696.421570,-1149.358276,40.812756>>, 14.250000)
						bWentOutBridge2 = TRUE
					ENDIF
				ENDIF
				
				IF bWentInBridge1 = TRUE
				AND bWentOutBridge1 = TRUE
					bBridge = TRUE
				ENDIF
				
				IF bWentInBridge2 = TRUE
				AND bWentOutBridge2 = TRUE
					bBridge = TRUE
				ENDIF

				//Check to see if there is a vehicle node above the player. This will suggest that the player is flying under a road.
				IF GET_CLOSEST_VEHICLE_NODE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehs[mvf_helicopter].id, <<0,0,5>>), vClosestNode)
					vHeliCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
					
					fHeliMaxRange_X = (vHeliCoords.x + 4)
					fHeliMinRange_X = (vHeliCoords.x - 4)

					fHeliMaxRange_Y = (vHeliCoords.y + 4)
					fHeliMinRange_Y = (vHeliCoords.y - 4)	
					
					IF VclosestNode.z > vHeliCoords.z
						IF VclosestNode.x <= fHeliMaxRange_X
						AND VclosestNode.x >= fHeliMinRange_X
						AND VclosestNode.y <= fHeliMaxRange_Y
						AND VclosestNode.y >= fHeliMinRange_Y							 
							PRINTSTRING("BRIDGE 4")PRINTNL()
							bBridge = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			if bBridge
				IF NOT bSetUnderTheBridgeStat 
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FH1_UNDER_BRIDGE)
					bSetUnderTheBridgeStat = TRUE
					//INFORM_MISSION_STATS_OF_INCREMENT(FH1_UNDER_BRIDGE)
				ENDIF
			endif
		ENDIF
	ENDIF	
ENDPROC
proc set_peds_CS_OUTFITS()

	IF ISENTITYALIVE(MIKE())
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael",MIKE())	
	ENDIF
	IF ISENTITYALIVE(FRANK())
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin",FRANK())	
	ENDIF
	IF ISENTITYALIVE(TREV())
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor",TREV())	
	ENDIF
	if IsEntityAlive(LESTER())
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Lester",LESTER())
	endif
	if IsEntityAlive(peds[mpf_guard].id)
		SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("security_guard",peds[mpf_guard].id)
	endif

endproc
proc DO_SHAKE_INTERP()
	if bstopshake		
		if GET_GAME_TIMER()- ishake > 250
			fshakeAmp = fshakeAmp - 0.05 
			ishake = get_game_timer()
		endif
		
		If fshakeAmp > 1.0
			fshakeAmp = 1.0
		ENDIF
		
		IF fshakeAmp <= 0
			fshakeAmp = 0
			bstopshake = false
		endif
		SET_GAMEPLAY_CAM_SHAKE_AMPLITUDE(fshakeAmp)
	endif
	
endproc
Proc Do_timelapse_BS1()
	sTimelapse.currentTimeOfDay = GET_CURRENT_TIMEOFDAY()
	
	IF sTimelapse.iTimelapseCut = 0
		if 	GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("bs_1_int", CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_4 | CS_SECTION_5 | CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8 )
		elif GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("bs_1_int", CS_SECTION_4 | CS_SECTION_5 | CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8)
		elif GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("bs_1_int", CS_SECTION_4 | CS_SECTION_5 | CS_SECTION_6 | CS_SECTION_7 | CS_SECTION_8)
		endif		
	ENDIF
		
	while not DO_TIMELAPSE(SP_HEIST_FINALE_1,sTimelapse)		
		DISPLAY_RADAR(false)
		DISPLAY_HUD(false)
		wait(0)
	endwhile
endproc

proc clear_players_task_on_control_input(script_task_name task_name)

	if get_script_task_status(player_ped_id(), task_name) = performing_task
			
		int left_stick_x
		int left_stick_y
		int right_stick_x
		int right_stick_y
		int stick_dead_zone = 28
	
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(left_stick_x, left_stick_y, right_stick_x, right_stick_y)

		IF NOT IS_LOOK_INVERTED()
			right_stick_y *= -1
		ENDIF
		
		// invert the vertical
		IF (left_stick_y > STICK_DEAD_ZONE)
		OR (left_stick_y < (STICK_DEAD_ZONE * -1))	
		or (left_stick_x > STICK_DEAD_ZONE)
		OR (left_stick_x < (STICK_DEAD_ZONE * -1))
		//or is_control_pressed(player_control, input_sprint) 
		
			clear_ped_tasks(player_ped_id())
			
		endif 		
	endif 
endproc
Proc Create_guard(MPF_MISSION_PED_FLAGS guard,vector pos,float heading)
	
	peds[guard].id = create_ped(PEDTYPE_MISSION,S_M_M_ARMOURED_01,pos,heading)
		if guard = mpf_guard
			SET_PED_COMPONENT_VARIATION(peds[guard].id,PED_COMP_HEAD,0,1)
			SET_PED_PROP_INDEX(peds[guard].id,ANCHOR_HEAD,1,0)
		else
			SET_PED_COMPONENT_VARIATION(peds[guard].id,PED_COMP_HEAD,2,0)
		endif
		
			SET_PED_CONFIG_FLAG(peds[guard].id,PCF_RunFromFiresAndExplosions,false)
			SET_PED_HIGHLY_PERCEPTIVE(peds[guard].id,true)
			GIVE_WEAPON_TO_PED(peds[guard].id,WEAPONTYPE_ADVANCEDRIFLE,INFINITE_AMMO,true,true)			
			SET_CURRENT_PED_WEAPON(peds[guard].id,WEAPONTYPE_ADVANCEDRIFLE,true)
			SET_PED_LEG_IK_MODE(peds[guard].id,LEG_IK_PARTIAL)
			
			OPEN_SEQUENCE_TASK(peds[guard].seq)
				if guard = mpf_guard
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<-77.96, -677.87, 33.47 >>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,90)
				else
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<9.47284, -710.34497, 45.01460>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,189.8286)
				endif
				TASK_PLAY_ANIM(null,"missbigscore1guard_wait_rifle","wait_base",slow_BLEND_IN,INSTANT_BLEND_OUT)	
				TASK_PLAY_ANIM(null,"missbigscore1guard_wait_rifle","wait_a",INSTANT_BLEND_in,INSTANT_BLEND_OUT)				
				TASK_PLAY_ANIM(null,"missbigscore1guard_wait_rifle","wait_b",INSTANT_BLEND_in,INSTANT_BLEND_OUT)				
				TASK_PLAY_ANIM(null,"missbigscore1guard_wait_rifle","wait_c",INSTANT_BLEND_in,SLOW_BLEND_OUT)		
				
				SET_SEQUENCE_TO_REPEAT(peds[guard].seq,REPEAT_FOREVER)
			CLOSE_SEQUENCE_TASK(peds[guard].seq)
			TASK_PERFORM_SEQUENCE(peds[guard].id,peds[guard].seq)
			
			SET_PED_RELATIONSHIP_GROUP_HASH(peds[guard].id,rel_guard)			
			if guard = mpf_guardF
				peds[guard].scenario_index = ADD_SCENARIO_BLOCKING_AREA(<<-3.84928, -728.36700, 44.09243>>,<<5.56493, -705.19385, 47.59827>>)
			else
				peds[guard].scenario_index = ADD_SCENARIO_BLOCKING_AREA(<<-93.01785, -670.31049, 34.46116>>,<<-65.72590, -686.62421, 31.73092>>)
			endif
			CLEAR_AREA_OF_PEDS(pos,100)
endproc
Proc give_all_peds_earpiece(bool bremove = false)
	if DOES_ENTITY_EXIST(mike())
	and not IS_PED_INJURED(mike())
		if not bremove
			SET_PED_COMP_ITEM_CURRENT_SP(mike(), COMP_TYPE_PROPS, PROPS_P0_HEADSET)
		else
			REMOVE_PED_COMP_ITEM_SP(mike(), COMP_TYPE_PROPS, PROPS_P0_HEADSET)
		endif
	endif
	if DOES_ENTITY_EXIST(frank())
	and not IS_PED_INJURED(frank())
		if not bremove
			SET_PED_COMP_ITEM_CURRENT_SP(frank(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
		else
			REMOVE_PED_COMP_ITEM_SP(frank(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
		endif
	endif
	if does_entity_exist(trev())
	and not IS_PED_INJURED(trev())
		if not bremove
			SET_PED_COMP_ITEM_CURRENT_SP(trev(), COMP_TYPE_PROPS, PROPS_P2_HEADSET)
		else
			REMOVE_PED_COMP_ITEM_SP(trev(), COMP_TYPE_PROPS, PROPS_P2_HEADSET)
		endif
	endif
	
endproc

// -----------------------------------------------------------------------------------------------------------
//		HOT SWAP
// -----------------------------------------------------------------------------------------------------------
Proc Hotswap_Menu(BOOL bBlockMike = FALSE, BOOL bBlockFrank = FALSE, BOOL bBlockTrev = FALSE,
					BOOL bHintMike = FALSE, BOOL bHintFrank = FALSE, BOOL bHintTrev = FALSE)
	//Enable or disable selecabilty in the hot swap
	
	if DOES_ENTITY_EXIST(mike())	
		sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = peds[mpf_michael].id
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_MICHAEL,bBlockMike) 
		SET_SELECTOR_PED_HINT(sSelectorPeds,SELECTOR_PED_MICHAEL,bHintMike)
	ELSE
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_MICHAEL,true)
	ENDIF
	
	if DOES_ENTITY_EXIST(frank())
		sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = peds[mpf_franklin].id
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,bBlockFrank) 
		SET_SELECTOR_PED_HINT(sSelectorPeds,SELECTOR_PED_FRANKLIN,bHintFrank)
	ELSE
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,true)
	ENDIF
	
	if DOES_ENTITY_EXIST(trev())
		sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = peds[mpf_trevor].id 
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,bBlockTrev) 
		SET_SELECTOR_PED_HINT(sSelectorPeds,SELECTOR_PED_TREVOR,bHintTrev)
	ELSE
		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,true)
	ENDIF
	
	
endproc

FUNC BOOL Display_Selector_HUD()
	IF bIgnoreNoPlayerControl
		IF UPDATE_SELECTOR_HUD(sSelectorPeds,FALSE)     // Returns TRUE when the player has made a selection
			IF NOT HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MULTIPLAYER)
				sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]					
				return TRUE
			ENDIF
		ENDIF
	ELSE	
		IF UPDATE_SELECTOR_HUD(sSelectorPeds)     // Returns TRUE when the player has made a selection
			IF NOT HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MULTIPLAYER)
				sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]					
				return TRUE
			ENDIF
		ENDIF	
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: Carries out a swap using the sCamDetail struct for cam interpolation
FUNC SWITCHSTATE Do_Hotswap()

	IF NOT sCamDetails.bRun
			return SWITCH_NOSTATE
	elif sCamDetails.bRun	

		if bcustomswitch_trev_to_mike
		CPRINTLN(DEBUG_MISSION,"bcustomswitch_trev_to_mike")
			if HANDLE_SWITCH_CAM_TREVOR_IN_HELI_TO_MICHAEL(scsSwitchCamTrevorInHeliToMichael)
				sCamDetails.bRun  = FALSE
				return SWITCH_COMPLETE				
			else
				return SWITCH_IN_PROGRESS
			endif		
		elif bcustomswitch_mike_to_trev_inHeli
		CPRINTLN(DEBUG_MISSION,"bcustomswitch_mike_to_trev_inHeli")
			if HANDLE_SWITCH_CAM_MICHAEL_TO_TREVOR_IN_HELI(scsSwitchCamMichaelToTrevorInHeli)				
				sCamDetails.bRun  = FALSE
				return SWITCH_COMPLETE				
			else
				return SWITCH_IN_PROGRESS
			endif						
		elif bcustomswitch_mike_to_trev_inCar
		CPRINTLN(DEBUG_MISSION,"bcustomswitch_mike_to_trev_inCar")
			if HANDLE_SWITCH_CAM_MICHAEL_TO_TREVOR_IN_TRUCK(scsSwitchCamMichaelToTrevorInTruck)
				sCamDetails.bRun  = FALSE
				return SWITCH_COMPLETE				
			else
				return SWITCH_IN_PROGRESS
			endif
		else	
			int iswitchflag = 0
			
			if bLongSpline
			and DOES_CAM_EXIST(sCamDetails.camTo)
				IF RUN_SWITCH_CAM_FROM_PLAYER_TO_CAM(sCamDetails,sCamDetails.camTo,SWITCH_TYPE_AUTO,iswitchflag)	
					return SWITCH_IN_PROGRESS
				ELSE			
					sCamDetails.bRun  = FALSE
					return SWITCH_COMPLETE
				ENDIF
			else	
				switch eSwitchTypeStage
					CASE S_TYPE_INIT
						IF GET_DISTANCE_BETWEEN_ENTITIES(TREV(),MIKE()) < 100
							eSwitchTypeStage = S_TYPE_SHORT							
						else
							eSwitchTypeStage = S_TYPE_AUTO							
						endif
					BREAK
					CASE S_TYPE_SHORT
						IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails,SWITCH_TYPE_SHORT,iswitchflag)		
							return SWITCH_IN_PROGRESS
						ELSE			
							sCamDetails.bRun  = FALSE
							eSwitchTypeStage = S_TYPE_INIT
							return SWITCH_COMPLETE
						ENDIF
					BREAK
					CASE S_TYPE_AUTO
						IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sCamDetails,SWITCH_TYPE_AUTO,iswitchflag)		
							return SWITCH_IN_PROGRESS
						ELSE			
							sCamDetails.bRun  = FALSE
							eSwitchTypeStage = S_TYPE_INIT
							return SWITCH_COMPLETE
						ENDIF
					BREAK
				endswitch
			endif	
		endif
			
	endif
	return SWITCH_NOSTATE	
ENDFUNC
FUNC BOOL Set_Current_Player_Ped(SELECTOR_SLOTS_ENUM pedChar, BOOL bWait = FALSE, BOOL bCleanUpModel = TRUE)

	IF bWait
		WHILE NOT SET_CURRENT_SELECTOR_PED(pedChar, bCleanUpModel)	
			WAIT(0)
		ENDWHILE
	ELSE
		IF NOT SET_CURRENT_SELECTOR_PED(pedChar, bCleanUpModel)
			RETURN FALSE
		ENDIF
	ENDIF
			
	// Additional stuff to set up ped for use in mission
	SWITCH pedChar
		CASE SELECTOR_PED_MICHAEL
			peds[mpf_michael].id = PLAYER_PED_ID()			
			ADD_PED_FOR_DIALOGUE(convo_struct, 1, mike() , "MICHAEL")		
		BREAK
		CASE SELECTOR_PED_FRANKLIN
			peds[mpf_franklin].id = PLAYER_PED_ID()
			ADD_PED_FOR_DIALOGUE(convo_struct, 2, frank(), "FRANKLIN")	
		BREAK
		CASE SELECTOR_PED_TREVOR
			peds[mpf_trevor].id = PLAYER_PED_ID()
			ADD_PED_FOR_DIALOGUE(convo_struct, 0,trev(), "TREVOR")
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
Proc Manage_hotswap()
vector pos
	//hot swap checks-------------------------------------------------
SWITCHSTATE hsStatus		
	if bHotSwap
		if player_ped_id() = MIKE()
			SET_SELECTOR_PED_HINT(sSelectorPeds,SELECTOR_PED_TREVOR,false)
			SET_SELECTOR_PED_PRIORITY(sSelectorPeds,SELECTOR_PED_TREVOR,SELECTOR_PED_MICHAEL,SELECTOR_PED_FRANKLIN)
		elif player_ped_id() = TREV()
			SET_SELECTOR_PED_HINT(sSelectorPeds,SELECTOR_PED_MICHAEL,false)
			SET_SELECTOR_PED_PRIORITY(sSelectorPeds,SELECTOR_PED_MICHAEL,SELECTOR_PED_TREVOR,SELECTOR_PED_FRANKLIN)
		elif player_ped_id() = frank()
			SET_SELECTOR_PED_HINT(sSelectorPeds,SELECTOR_PED_MICHAEL,false)
			SET_SELECTOR_PED_PRIORITY(sSelectorPeds,SELECTOR_PED_MICHAEL,SELECTOR_PED_TREVOR,SELECTOR_PED_FRANKLIN)	
		endif
		if not sCamDetails.brun
		and not bCustomSwitchCamRendering_HeliToCar
			IF GET_PLAYER_WANTED_LEVEL(player_id()) = 0
				if Display_Selector_HUD()
				
					INFORM_MISSION_STATS_OF_INCREMENT(FH1_SWITCHES)				
	//====================================================== UI NEW PED CHOSEN ============================================================			
					if IS_MESSAGE_BEING_DISPLAYED()
						CLEAR_PRINTS()
					endif
					if IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					endif	
					
					CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
					PAUSE_CLOCK(true)
					if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mike()),GET_ENTITY_COORDS(trev())) > 70
					and not IS_ENTITY_ON_SCREEN(sCamDetails.pedTo)					
						bLongSpline = true
					else
						bLongSpline = false						
					endif
					eSwitchTypeStage = S_TYPE_INIT
					//=======================================  Safety =============================================	
						if IsEntityAlive(mike())
							SET_ENTITY_INVINCIBLE(MIKE(),true)
						endif
						if IsEntityAlive(lester())
							SET_ENTITY_INVINCIBLE(LESTER(),true)
						endif
						if IsEntityAlive(trev())
							SET_ENTITY_INVINCIBLE(TREV(),true)
						endif
						if IsEntityAlive(frank())
							SET_ENTITY_INVINCIBLE(FRANK(),true)
						endif
						if IsEntityAlive(vehs[mvf_helicopter].id)
							SET_ENTITY_INVINCIBLE(vehs[mvf_helicopter].id,true)	
						endif
						if IsEntityAlive(vehs[mvf_trevor_veh].id)
							SET_ENTITY_INVINCIBLE(vehs[mvf_trevor_veh].id,true)
						endif
						if IsEntityAlive(vehs[mvf_Michael_veh].id)
							SET_ENTITY_INVINCIBLE(vehs[mvf_Michael_veh].id,true)
						endif
			//========================================= MICHAEL PICKED ===============================================			
					if sSelectorPeds.eNewSelectorPed = SELECTOR_PED_MICHAEL	
						pos = GET_ENTITY_COORDS(mike())		
						//SWITCH INTERRUPT From Trev	
						if eDialogue_stateT = DI_RUNNING_DIALOGUE_TREVOR
						and eInterruptstage = INTERRUPT_NONE
						and IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							if eTREV_Dialogue = T_di_FILL1
							or eTREV_Dialogue = T_di_FILL2
							or eTREV_Dialogue = T_di_END
								//store line too allow the convo to continue on re-switch
								lbl_TREVSwitchInterrupt 	= GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()					
								lbl_TREVSwitchroot 			= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT() 
								b_TREVSpeechSwitchStopped 	= true								
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								PRINTLN("@@@@@@@@@@@@@ KILL TREVOR CONVO NOW AND SAVE ROOT: ", lbl_TREVSwitchroot, " AND LABEL: ", lbl_TREVSwitchInterrupt, " @@@@@@@@@@@@@@@@@@")
							endif
						endif		
						//=============================== STAGE 0 ====================================
						if mission_stage = enum_to_int(msf_0_strip_club)
							if bLongSpline								
								if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_Michael_veh].id)
									STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id)
								endif									
								START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,1,"FH1RECMIKE")
								fSwitchTimeRec = GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(pos,1,"FH1RECMIKE",20)										
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,fSwitchTimeRec)
								SET_PLAYBACK_SPEED(vehs[mvf_Michael_veh].id,0)
								FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_Michael_veh].id)									
								SET_ENTITY_COORDS(MIKE(),GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(1,"FH1RECMIKE"),fSwitchTimeRec))

								if not IS_PED_IN_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
									SET_PED_INTO_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(MIKE())
								endif
								if not IS_PED_IN_VEHICLE(FRANK(),vehs[mvf_Michael_veh].id)
									SET_PED_INTO_VEHICLE(FRANK(),vehs[mvf_Michael_veh].id,VS_FRONT_RIGHT)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(FRANK())
								endif
								sCamDetails.camTo = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,
										(GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(1,"FH1RECMIKE"),fSwitchTimeRec)+ <<0,0,30>>),
										 GET_ENTITY_ROTATION(sCamDetails.pedTo))
							endif
						endif
						//=============================== STAGE 1 ====================================
						if mission_stage = enum_to_int(msf_1_font_of_bank)
							if mission_substage = STAGE_ENTRY
								if bLongSpline							
									if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_Michael_veh].id)
										STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id)
									endif												
									START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,1,"FH1RECMIKE")
									fSwitchTimeRec = GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(vFRONT_BANK,1,"FH1RECMIKE",20) - 10000
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,fSwitchTimeRec)
									SET_PLAYBACK_SPEED(vehs[mvf_Michael_veh].id,0)
									FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_Michael_veh].id)									
									SET_ENTITY_COORDS(MIKE(),GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(1,"FH1RECMIKE"),fSwitchTimeRec))

									if not IS_PED_IN_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
										SET_PED_INTO_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(MIKE())
									endif
									if not IS_PED_IN_VEHICLE(FRANK(),vehs[mvf_Michael_veh].id)
										SET_PED_INTO_VEHICLE(FRANK(),vehs[mvf_Michael_veh].id,VS_FRONT_RIGHT)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(FRANK())
									endif
									sCamDetails.camTo = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,
											(GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(1,"FH1RECMIKE"),fSwitchTimeRec)+ <<0,0,30>>),
											 GET_ENTITY_ROTATION(sCamDetails.pedTo))
								endif	
							else
								if bLongSpline	
									if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_Michael_veh].id)
										STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id)
									endif									
									START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,2,"FH1RECMIKE")
									fSwitchTimeRec = GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(pos,2,"FH1RECMIKE")
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,fSwitchTimeRec)
									SET_PLAYBACK_SPEED(vehs[mvf_Michael_veh].id,0)
									FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_Michael_veh].id)									
									SET_ENTITY_COORDS(MIKE(),GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(2,"FH1RECMIKE"),fSwitchTimeRec))

									if not IS_PED_IN_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
										SET_PED_INTO_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(MIKE())
									endif
									if not IS_PED_IN_VEHICLE(FRANK(),vehs[mvf_Michael_veh].id)
										SET_PED_INTO_VEHICLE(FRANK(),vehs[mvf_Michael_veh].id,VS_FRONT_RIGHT)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(FRANK())
									endif
									sCamDetails.camTo = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,
											(GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(2,"FH1RECMIKE"),fSwitchTimeRec)+ <<0,0,30>>),
											 GET_ENTITY_ROTATION(sCamDetails.pedTo))
								endif
							endif
						//=============================== STAGE 2 ====================================
						elif mission_stage = enum_to_int(msf_2_back_of_bank)
						or mission_stage = enum_to_int(msf_3_get_in_position)
							if mission_substage = STAGE_ENTRY
								REQUEST_CUTSCENE("BSS_1_MCS_2")
								SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_OFF) //#BUG:1542524
								lbl_MIKESwitchInterrupt 		= ""				
								lbl_MIKESwitchroot 				= ""
								b_MIKESpeechSwitchStopped 		= false
								eMike_dialogue = M_di_NONE
								if bLongSpline
									if DOES_ENTITY_EXIST(vehs[mvf_Michael_veh].id)									
										SET_ENTITY_COORDS(vehs[mvf_Michael_veh].id,vBACK_BANK)
										SET_ENTITY_HEADING(vehs[mvf_Michael_veh].id,fHEADING_BACK)
										SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_Michael_veh].id)
										clear_area(<< -114.0502, -652.3067, 34.9012 >>,40,true)
									endif
								endif
							endif
							
							if DOES_ENTITY_EXIST(vehs[mvf_helicopter].id)		
								IF IS_PED_IN_VEHICLE(peds[mpf_trevor].id, vehs[mvf_helicopter].id)
									eSwitchCamState = SWITCH_CAM_REQUEST_ASSETS
									HANDLE_SWITCH_CAM_TREVOR_IN_HELI_TO_MICHAEL(scsSwitchCamTrevorInHeliToMichael)
									
									bcustomswitch_trev_to_mike	= true
									eSwitchCamState = SWITCH_CAM_SETUP_SPLINE_1
								ENDIF	
							ENDIF

						elif mission_Stage = enum_to_int(msf_6_drop_off_point)
							if bLongSpline	
								if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_Michael_veh].id)
									STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id)
								endif									
								START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,3,"FH1RECMIKE")
								fSwitchTimeRec = GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(pos,3,"FH1RECMIKE")
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,fSwitchTimeRec)
								SET_PLAYBACK_SPEED(vehs[mvf_Michael_veh].id,0)
								FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_Michael_veh].id)									
								SET_ENTITY_COORDS(MIKE(),GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(3,"FH1RECMIKE"),fSwitchTimeRec))
								if not IS_PED_IN_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
									SET_PED_INTO_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(MIKE())
								endif
								if not IS_PED_IN_VEHICLE(FRANK(),vehs[mvf_Michael_veh].id)
									SET_PED_INTO_VEHICLE(FRANK(),vehs[mvf_Michael_veh].id,VS_FRONT_RIGHT)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(FRANK())
								endif
								sCamDetails.camTo = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,
										(GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(3,"FH1RECMIKE"),fSwitchTimeRec)+ <<0,0,30>>),
										 GET_ENTITY_ROTATION(sCamDetails.pedTo))

							endif

						endif 
						//=============================== ALWAYS ====================================
							if IsEntityAlive(vehs[mvf_Michael_veh].id)
								SET_ENTITY_LOAD_COLLISION_FLAG(vehs[mvf_Michael_veh].id,TRUE)
							endif
							//set_other party invincible
							//on
							if IsEntityAlive(TREV())
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(TREV(),true)
							ENDIF
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(lester(),true)
							if IsEntityAlive(vehs[mvf_helicopter].id)
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_helicopter].id,true)
							endif
							if IsEntityAlive(vehs[mvf_trevor_veh].id)
								SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_trevor_veh].id,true)
							endif
							//off
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(MIKE(),false)
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(frank(),false)
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_Michael_veh].id,false)
							
							
			//========================================= TREVOR PICKED ===============================================								
					elif sSelectorPeds.eNewSelectorPed = SELECTOR_PED_TREVOR
						//Switch interrupt from mike
						if eDialogue_stateM = DI_RUNNING_DIALOGUE_MIKE
						and eInterruptstage = INTERRUPT_NONE
						and IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							if eMike_dialogue = M_di_TO_FRONT
							or eMike_dialogue = M_di_TO_BACK
							or eMike_dialogue = M_di_END
								//store line too allow the convo to continue on re-switch
								lbl_MIKESwitchInterrupt 		= GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()					
								lbl_MIKESwitchroot 				= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
								b_MIKESpeechSwitchStopped 		= true
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								PRINTLN("@@@@@@@@@@@@@ KILL MIKE CONVO NOW AND SAVE ROOT: ", lbl_MIKESwitchroot, " AND LABEL: ", lbl_MIKESwitchInterrupt, " @@@@@@@@@@@@@@@@@@")
							endif
						endif
						//=============================== TREVOR POSITION ====================================
						if bLongSpline
								
							//if been 4 second put trev on recording of cloest point
							pos = GET_ENTITY_COORDS(trev())						
							if mission_stage < ENUM_TO_INT(msf_4_heli_follow_truck)
							 	if heli_switch = TO_HELI
									if IsEntityAlive(vehs[mvf_trevor_veh].id)
		
										if Is_clock_time_less_than_or_equal(14,45)
										and bcustomswitch_mike_to_trev_inCar
											eSwitchCamState = SWITCH_CAM_SETUP_SPLINE_1											
											if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_trevor_veh].id)
												STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_veh].id)
											endif											

										ELSE
											if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_trevor_veh].id)
												STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_veh].id)
											endif									
											START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_veh].id,3,"FH1REC")
											fSwitchTimeRec = GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(pos,3,"FH1REC")
											if fSwitchTimeRec <= 10000
												fSwitchTimeRec = 10000
											endif
											SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_veh].id,fSwitchTimeRec)
											SET_PLAYBACK_SPEED(vehs[mvf_trevor_veh].id,0)
											FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_trevor_veh].id)									
											SET_ENTITY_COORDS(TREV(),GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(3,"FH1REC"),fSwitchTimeRec))
											if not IS_PED_IN_VEHICLE(TREV(),vehs[mvf_trevor_veh].id)
												SET_PED_INTO_VEHICLE(TREV(),vehs[mvf_trevor_veh].id)
												FORCE_PED_AI_AND_ANIMATION_UPDATE(TREV())
											endif
											if not IS_PED_IN_VEHICLE(lester(),vehs[mvf_trevor_veh].id)
												SET_PED_INTO_VEHICLE(lester(),vehs[mvf_trevor_veh].id,VS_FRONT_RIGHT)
												FORCE_PED_AI_AND_ANIMATION_UPDATE(lester())
											endif					

										endif
										sCamDetails.camTo = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,
												(GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(3,"FH1REC"),fSwitchTimeRec)+ <<0,0,30>>),
												 GET_ENTITY_ROTATION(sCamDetails.pedTo))	
									endif
								elif heli_switch = TO_TRUCKS
								AND IS_PED_IN_VEHICLE( peds[mpf_trevor].id, Vehs[mvf_helicopter].id)
									if IsEntityAlive(vehs[mvf_helicopter].id)
																	
										if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_helicopter].id)
											STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_helicopter].id)
										endif
										START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_helicopter].id,4,"FH1REC")
										fSwitchTimeRec = GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(pos,4,"FH1REC")
										SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_helicopter].id,fSwitchTimeRec)
										SET_PLAYBACK_SPEED(vehs[mvf_helicopter].id,0)
										FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_helicopter].id)
										SET_ENTITY_COORDS(TREV(),GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(4,"FH1REC"),fSwitchTimeRec))
										if not IS_PED_IN_VEHICLE(TREV(),vehs[mvf_helicopter].id)
											SET_PED_INTO_VEHICLE(TREV(),vehs[mvf_helicopter].id)
											FORCE_PED_AI_AND_ANIMATION_UPDATE(TREV())
										endif
										if not IS_PED_IN_VEHICLE(lester(),vehs[mvf_helicopter].id)
											SET_PED_INTO_VEHICLE(lester(),vehs[mvf_helicopter].id,VS_BACK_LEFT)
											FORCE_PED_AI_AND_ANIMATION_UPDATE(lester())
										endif		

										bcustomswitch_mike_to_trev_inHeli	= true
										eSwitchCamState = SWITCH_CAM_SETUP_SPLINE_1	
									endif
								endif
							endif
							if mission_stage = ENUM_TO_INT(msf_6_drop_off_point)
								if IsEntityAlive(vehs[mvf_helicopter].id)
									
									START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_helicopter].id,5,"FH1REC")								
									fSwitchTimeRec = GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(pos,5,"FH1REC")
									SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_helicopter].id,fSwitchTimeRec)
									SET_PLAYBACK_SPEED(vehs[mvf_helicopter].id,0)
									FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_helicopter].id)
									SET_ENTITY_COORDS(TREV(),GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(5,"FH1REC"),fSwitchTimeRec))
									if not IS_PED_IN_VEHICLE(TREV(),vehs[mvf_helicopter].id)
										SET_PED_INTO_VEHICLE(TREV(),vehs[mvf_helicopter].id)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(TREV())
									endif
									if not IS_PED_IN_VEHICLE(lester(),vehs[mvf_helicopter].id)
										SET_PED_INTO_VEHICLE(lester(),vehs[mvf_helicopter].id,VS_BACK_LEFT)
										FORCE_PED_AI_AND_ANIMATION_UPDATE(lester())
									endif					

									sCamDetails.camTo = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,
												(GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(5,"FH1REC"),fSwitchTimeRec)+ <<0,0,30>>),
												GET_ENTITY_ROTATION(sCamDetails.pedTo))	
								endif	
							endif		
							
							
						endif
							
						//=============================== ALWAYS ====================================
						if IS_PED_IN_ANY_VEHICLE(TREV())
							IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_PED_IS_IN(TREV()))
								SET_ENTITY_LOAD_COLLISION_FLAG(GET_VEHICLE_PED_IS_IN(TREV()),TRUE)
							ENDIF
						endif
						//set_other party invincible
						//on
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(TREV(),false)
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(lester(),false)
						if IsEntityAlive(vehs[mvf_helicopter].id)
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_helicopter].id,false)
						endif
						if IsEntityAlive(vehs[mvf_trevor_veh].id)
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_trevor_veh].id,false)
						endif
						//off
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(MIKE(),true)
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(frank(),true)
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_Michael_veh].id,true)					
					endif
					//anything that was waiting to resume leave to the switch system to pick up.
//					lbl_Interrupt = ""
//					lbl_root = ""					
//					eInterruptstage = INTERRUPT_NONE
					
					idescentstage = 0
					sCamDetails.bRun  = TRUE
					sCamDetails.bPedSwitched = false
				endif
			ENDIF
		endif
//====================================================== Start SWITCH ============================================================					
		hsStatus = Do_Hotswap()			
//======================================================    RUNNING   ============================================================		
		if hsStatus = SWITCH_IN_PROGRESS
			IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Michael_veh].id)
				SET_ENTITY_LOAD_COLLISION_FLAG(vehs[mvf_Michael_veh].id,TRUE)
			ENDIF
				
			IF sCamDetails.bOKToSwitchPed		
				IF NOT sCamDetails.bPedSwitched						
					IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, PlayerSwitchTakeControlFlag)
						clear_area(GET_ENTITY_COORDS(sCamDetails.pedTo),10,true)
						sCamDetails.bPedSwitched = TRUE
				//=======================================  SWITCHED  =============================================		
						//===========================   Trev   ===========================
						if DOES_ENTITY_EXIST(trev())		
							if sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = NULL
								peds[mpf_trevor].id = PLAYER_PED_ID()		
								iUnfreezeDelay = get_game_timer()								
								if IS_PED_IN_ANY_VEHICLE(trev())
									FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(trev()),false)
								endif								
							ELSE
								peds[mpf_trevor].id = sSelectorPeds.pedID[SELECTOR_PED_TREVOR] 	
								SET_PED_RELATIONSHIP_GROUP_HASH(trev(),rel_buddy)
							ENDIF
							IF NOT IS_PED_INJURED(trev())
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(trev(), TRUE)
							ENDIF
						ENDIF
						//===========================   Mike   ===========================
						if DOES_ENTITY_EXIST(mike())		
							if sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = NULL
								peds[mpf_michael].id = PLAYER_PED_ID()	
								iUnfreezeDelay = get_game_timer()
								FREEZE_ENTITY_POSITION(vehs[mvf_Michael_veh].id ,false)	
							ELSE
								peds[mpf_michael].id = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
								SET_PED_RELATIONSHIP_GROUP_HASH(mike(),rel_buddy)
							ENDIF
							IF NOT IS_PED_INJURED(mike())
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mike(), TRUE)
							ENDIF
						ENDIF	
						//===========================   FRANK   ===========================
						if DOES_ENTITY_EXIST(Frank())		
							if sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = NULL
								peds[mpf_franklin].id = PLAYER_PED_ID()	
								iUnfreezeDelay = get_game_timer()
							ELSE
								peds[mpf_franklin].id = sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
								SET_PED_RELATIONSHIP_GROUP_HASH(FRANK(),rel_buddy)
							ENDIF
							IF NOT IS_PED_INJURED(frank())
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(frank(), TRUE)
							ENDIF
						ENDIF
					ENDIF
				endif
			ENDIF		
			//=========================== Keep Tasks so they don't just stop =================================
			
			if get_game_timer() - iUnfreezeDelay > 2000
			and bLongSpline
				if IS_PED_IN_ANY_VEHICLE(sCamDetails.pedTo)
					FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(sCamDetails.pedTo) ,false)
				endif
			endif			
			
			if mission_stage = enum_to_int(msf_0_strip_club)		
				
				//===========================  TREV TASKS   ===========================
				if not bcustomswitch_mike_to_trev_inCar
					if heli_switch = TO_HELI
						IF NOT IS_PED_INJURED(trev())
						AND NOT IS_ENTITY_DEAD(vehs[mvf_trevor_veh].id)
							if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) != PERFORMING_TASK		
								TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(trev(),vehs[mvf_trevor_veh].id,vAIRFIELD,50,DRIVINGMODE_AVOIDCARS | DF_ForceJoinInRoadDirection ,5)												
							endif
						ENDIF
					elif heli_switch = TO_TRUCKS
						IF NOT IS_PED_INJURED(trev())
						AND NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
							if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
								TASK_VEHICLE_MISSION_COORS_TARGET(trev(),vehs[mvf_helicopter].id,<< 1527.6018, -591.3962, 336.5655 >>,MISSION_CIRCLE,10,DRIVINGMODE_AVOIDCARS,4,-1)					
							endif
						ENDIF
					endif	
				endif
				//===========================   MIKE TASKS   ===========================
					IF NOT IS_PED_INJURED(mike())
					AND NOT IS_ENTITY_DEAD(vehs[mvf_Michael_veh].id)
						if GET_SCRIPT_TASK_STATUS(mike(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) != PERFORMING_TASK
							TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(mike(),vehs[mvf_Michael_veh].id,vFRONT_BANK,15,DRIVINGMODE_AVOIDCARS | DF_ForceJoinInRoadDirection ,5)																
						endif
					ENDIF
			elif mission_stage = enum_to_int(msf_1_font_of_bank)	
				//===========================   TREV TASKS  ===========================	
					if not bcustomswitch_mike_to_trev_inCar
						if heli_switch = TO_HELI 
							IF NOT IS_PED_INJURED(trev())
							AND NOT IS_ENTITY_DEAD(vehs[mvf_trevor_veh].id)
								if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) != PERFORMING_TASK		
									TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(trev(),vehs[mvf_trevor_veh].id,vAIRFIELD,50,DRIVINGMODE_AVOIDCARS | DF_ForceJoinInRoadDirection ,5)												
								endif
							ENDIF
						elif heli_switch = TO_TRUCKS
							IF NOT IS_PED_INJURED(trev())
							AND NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
								if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
									TASK_VEHICLE_MISSION_COORS_TARGET(trev(),vehs[mvf_helicopter].id,<< 1527.6018, -591.3962, 336.5655 >>,MISSION_CIRCLE,10,DRIVINGMODE_AVOIDCARS,4,-1)					
								endif
							ENDIF
						endif
					endif
				//===========================   MIKE TASKS   ===========================					
					if mission_substage = STAGE_ENTRY
						if sCamDetails.pedTo = MIKE()
							IF NOT IS_PED_INJURED(mike())
							AND NOT IS_ENTITY_DEAD(vehs[mvf_Michael_veh].id)
								if GET_SCRIPT_TASK_STATUS(mike(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) != PERFORMING_TASK
									TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(mike(),vehs[mvf_Michael_veh].id,vFRONT_BANK,10,DRIVINGMODE_AVOIDCARS,6)																
								endif 
							ENDIF
						endif
					elif mission_substage > STAGE_ENTRY
						IF NOT IS_PED_INJURED(mike())
						AND NOT IS_ENTITY_DEAD(vehs[mvf_Michael_veh].id)
							if GET_SCRIPT_TASK_STATUS(MIKE(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) != PERFORMING_TASK
								TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(MIKE(),vehs[mvf_Michael_veh].id,vBACK_BANK,13,DRIVINGMODE_AVOIDCARS | DF_ForceJoinInRoadDirection ,3)
							endif
						ENDIF
					endif
					
			elif mission_stage = enum_to_int(msf_2_back_of_bank)
					//===========================   TREV TASKS   ===========================
					if not bcustomswitch_mike_to_trev_inCar
						if heli_switch = TO_HELI
							IF NOT IS_PED_INJURED(trev())
							AND NOT IS_ENTITY_DEAD(vehs[mvf_trevor_veh].id)
								if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) != PERFORMING_TASK		
									TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(trev(),vehs[mvf_trevor_veh].id,<< 1799.9630, 3321.9102, 40.8868 >>,50,DRIVINGMODE_AVOIDCARS | DF_ForceJoinInRoadDirection ,5)												
								endif
							ENDIF
						elif heli_switch = TO_TRUCKS
							IF NOT IS_PED_INJURED(trev())
							AND NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
								if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
									TASK_VEHICLE_MISSION_COORS_TARGET(trev(),vehs[mvf_helicopter].id,<< 1527.6018, -591.3962, 336.5655 >>,MISSION_CIRCLE,10,DRIVINGMODE_AVOIDCARS,4,-1)					
								endif
							ENDIF
						endif
					endif
					//===========================   MIKE TASKS   ===========================
					if mission_substage = STAGE_ENTRY				
						if bLongSpline
							if DOES_ENTITY_EXIST(vehs[mvf_Michael_veh].id)
								SET_ENTITY_COORDS(vehs[mvf_Michael_veh].id,vBACK_BANK)
								SET_ENTITY_HEADING(vehs[mvf_Michael_veh].id,fHEADING_BACK)
								SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_Michael_veh].id)
							endif
						endif
					endif
					
			elif mission_stage = enum_to_int(msf_6_drop_off_point)					
				//===========================   TREV TASKS   ===========================
				IF NOT IS_PED_INJURED(trev())
				AND NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
					if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
						TASK_VEHICLE_MISSION_COORS_TARGET(trev(),vehs[mvf_helicopter].id,<< 1728.8278, 3126.1279,106.3001 >>,MISSION_CIRCLE,10,DRIVINGMODE_AVOIDCARS,4,-1)					
					endif	
				ENDIF
				//===========================   MIKE TASKS   ===========================
				IF NOT IS_PED_INJURED(MIKE())
				AND NOT IS_ENTITY_DEAD(vehs[mvf_Michael_veh].id)
					if GET_SCRIPT_TASK_STATUS(MIKE(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) != PERFORMING_TASK
						TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(MIKE(),vehs[mvf_Michael_veh].id, << 11.2359, 547.3271, 174.8878 >>,10,DRIVINGMODE_AVOIDCARS | DF_ForceJoinInRoadDirection ,3)
					endif
				ENDIF
			endif	
			
			//=======================================  DESCENT  =============================================	
			if not bcustomswitch_mike_to_trev_inCar
			and not bcustomswitch_mike_to_trev_inHeli
			and not bcustomswitch_trev_to_mike
				if bLongSpline
				
					if GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_LONG
					or GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_MEDIUM	

						if GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_DESCENT

							if GET_PLAYER_SWITCH_JUMP_CUT_INDEX() > 0
								CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(sCamDetails.pedTo), 100.0)	
								if 	sCamDetails.pedTo = MIKE()
									SET_VEHICLE_USE_MORE_RESTRICTIVE_SPAWN_CHECKS(vehs[mvf_Michael_veh].id,true)
									SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vehs[mvf_Michael_veh].id,true)
								elif sCamDetails.pedTo = trev()
									if IsEntityAlive(vehs[mvf_trevor_veh].id)
										SET_VEHICLE_USE_MORE_RESTRICTIVE_SPAWN_CHECKS(vehs[mvf_trevor_veh].id,true)
										SET_VEHICLE_WILL_FORCE_OTHER_VEHICLES_TO_STOP(vehs[mvf_trevor_veh].id,true)
									endif
								endif
							endif			
							
							switch idescentstage
								case 0
									if GET_PLAYER_SWITCH_JUMP_CUT_INDEX() = 1
										if 	sCamDetails.pedTo = MIKE()
											if IsEntityAlive(vehs[mvf_Michael_veh].id)
											and IS_PED_IN_VEHICLE(sCamDetails.pedTo,vehs[mvf_Michael_veh].id)
												if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_Michael_veh].id)
													SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,(fSwitchTimeRec - 5000) - GET_TIME_POSITION_IN_RECORDING(vehs[mvf_Michael_veh].id))

												else												
													if mission_stage = ENUM_TO_INT(msf_0_strip_club)		

														START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,1,"FH1RECMIKE")
													elif mission_stage = ENUM_TO_INT(msf_1_font_of_bank)
														if mission_substage = STAGE_ENTRY

															START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,1,"FH1RECMIKE")
														else

															START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,2,"FH1RECMIKE")
														endif
													elif mission_Stage = enum_to_int(msf_6_drop_off_point)

														START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,3,"FH1RECMIKE")
													endif
													IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_Michael_veh].id)	
														SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,(fSwitchTimeRec - 5000))
													ENDIF
												endif
												IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_Michael_veh].id)	
													SET_PLAYBACK_SPEED(vehs[mvf_Michael_veh].id,0.5)
													FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_Michael_veh].id)
												ENDIF
											endif
										elif sCamDetails.pedTo = trev()
											if IsEntityAlive(vehs[mvf_trevor_veh].id)
											and IS_PED_IN_VEHICLE(sCamDetails.pedTo,vehs[mvf_trevor_veh].id)
												if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_trevor_veh].id)
													SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_veh].id,(fSwitchTimeRec - 5000) - GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_veh].id))

												else	
													START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_veh].id,3,"FH1REC")										
													SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_veh].id,(fSwitchTimeRec - 5000))

												endif	
												SET_PLAYBACK_SPEED(vehs[mvf_trevor_veh].id,0.5)
												FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_trevor_veh].id)
											endif									
											if IsEntityAlive(vehs[mvf_helicopter].id)
											and IS_PED_IN_VEHICLE(sCamDetails.pedTo,vehs[mvf_helicopter].id)
												if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_helicopter].id)
													SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_helicopter].id,(fSwitchTimeRec - 5000) - GET_TIME_POSITION_IN_RECORDING(vehs[mvf_helicopter].id))

												else
													if mission_stage = ENUM_TO_INT(msf_6_drop_off_point)												
														START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_helicopter].id,5,"FH1REC")
													else													
														START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_helicopter].id,4,"FH1REC")
													endif
													SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_helicopter].id,(fSwitchTimeRec - 5000))

												endif
																					
												SET_PLAYBACK_SPEED(vehs[mvf_helicopter].id,0.5)
												FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_helicopter].id)											
											endif
										endif
										idescentstage++
									elif GET_PLAYER_SWITCH_JUMP_CUT_INDEX() = 0
										idescentstage++
									endif
								break
								case 1
									if GET_PLAYER_SWITCH_JUMP_CUT_INDEX() = 0
										if sCamDetails.pedTo = MIKE()
											if IsEntityAlive(vehs[mvf_Michael_veh].id)
											and IS_PED_IN_VEHICLE(sCamDetails.pedTo,vehs[mvf_Michael_veh].id)
												if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_Michael_veh].id)
													SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,(fSwitchTimeRec) - GET_TIME_POSITION_IN_RECORDING(vehs[mvf_Michael_veh].id))

												else
													if mission_stage = ENUM_TO_INT(msf_0_strip_club)													
														START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,1,"FH1RECMIKE")

													elif mission_stage = ENUM_TO_INT(msf_1_font_of_bank)
														if mission_substage = STAGE_ENTRY
															START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,1,"FH1RECMIKE")

														else
															START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,2,"FH1RECMIKE")

														endif
													elif mission_Stage = enum_to_int(msf_6_drop_off_point)

														START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,3,"FH1RECMIKE")
													endif
													IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_Michael_veh].id)
														SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id,(fSwitchTimeRec))
													ENDIF
												endif
												IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_Michael_veh].id)
													SET_PLAYBACK_SPEED(vehs[mvf_Michael_veh].id,1.2)
													FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_Michael_veh].id)
												ENDIF
											endif
										elif sCamDetails.pedTo = trev()
											if IsEntityAlive(vehs[mvf_trevor_veh].id)
											and IS_PED_IN_VEHICLE(sCamDetails.pedTo,vehs[mvf_trevor_veh].id)
												if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_trevor_veh].id)
													SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_veh].id,fSwitchTimeRec - GET_TIME_POSITION_IN_RECORDING(vehs[mvf_trevor_veh].id))

												else	
													START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_veh].id,3,"FH1REC")														
													SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_veh].id,fSwitchTimeRec)

												endif	
												SET_PLAYBACK_SPEED(vehs[mvf_trevor_veh].id,0.5)
												FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_trevor_veh].id)
											endif									
											if IsEntityAlive(vehs[mvf_helicopter].id)
											and IS_PED_IN_VEHICLE(sCamDetails.pedTo,vehs[mvf_helicopter].id)
												if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_helicopter].id)
													SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_helicopter].id,fSwitchTimeRec - GET_TIME_POSITION_IN_RECORDING(vehs[mvf_helicopter].id))
												else
													if mission_stage = ENUM_TO_INT(msf_6_drop_off_point)
														START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_helicopter].id,5,"FH1REC")
													else
														START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_helicopter].id,4,"FH1REC")
													endif
													SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_helicopter].id,fSwitchTimeRec)
												endif
																					
												SET_PLAYBACK_SPEED(vehs[mvf_helicopter].id,0.5)
												FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehs[mvf_helicopter].id)											
											endif
										endif
										idescentstage++
									endif
								break
								case 2
									if GET_PLAYER_SWITCH_JUMP_CUT_INDEX() != 0
										if sCamDetails.pedTo = MIKE()
											if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_Michael_veh].id)
												SET_PLAYBACK_SPEED(vehs[mvf_Michael_veh].id,1)

											endif
										elif sCamDetails.pedTo = trev()
											if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_helicopter].id)
												SET_PLAYBACK_SPEED(vehs[mvf_helicopter].id,1)

											elif IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_trevor_veh].id)
												SET_PLAYBACK_SPEED(vehs[mvf_trevor_veh].id,1)

											endif
										endif
									endif
								break
							endswitch				
						endif	
					endif
				else
					if GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_LONG
					or GET_PLAYER_SWITCH_TYPE() = SWITCH_TYPE_MEDIUM
						if GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_DESCENT
						or GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_OUTRO_HOLD
						or GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_OUTRO_SWOOP
						or GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_OUTRO
						or GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_FINISHED

							if IS_PED_IN_ANY_VEHICLE(sCamDetails.pedTo)
								FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(sCamDetails.pedTo) ,false)
								if not IS_PED_IN_FLYING_VEHICLE(sCamDetails.pedTo)
									SET_VEHICLE_ON_GROUND_PROPERLY(GET_VEHICLE_PED_IS_IN(sCamDetails.pedTo))
								endif
							endif
						else

							if IS_PED_IN_ANY_VEHICLE(sCamDetails.pedTo)
								FREEZE_ENTITY_POSITION(GET_VEHICLE_PED_IS_IN(sCamDetails.pedTo) ,true)								
							endif
						endif
					endif
				endif
			endif
			if NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
				if IS_HELI_PART_BROKEN(vehs[mvf_helicopter].id,true,true,true)
					SET_VEHICLE_FIXED(vehs[mvf_helicopter].id)
				ENDIF 
			endif
		endif
//======================================================    DONE   ============================================================		
		IF hsStatus = SWITCH_COMPLETE
			
			if IsEntityAlive(vehs[mvf_trevor_veh].id)
				if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_trevor_veh].id)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_trevor_veh].id)
				endif		
			endif
			if IsEntityAlive(vehs[mvf_helicopter].id)
				if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_helicopter].id)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_helicopter].id)
				endif		
			endif
			if IsEntityAlive(vehs[mvf_Michael_veh].id)
				if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_Michael_veh].id)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_Michael_veh].id)
				endif		
			endif			
		
			if not bcustomswitch_mike_to_trev_inCar
			and not bcustomswitch_mike_to_trev_inHeli
			and not bcustomswitch_trev_to_mike
				SET_PLAYER_CONTROL(player_id(),true)
				CLEAR_PED_TASKS(player_ped_id())
			else
				//[MF] Only clear Michael's task if we aren't currently displaying the switch cam from Michael that transitions directly to a cutscene.		
				IF DOES_CAM_EXIST(scsSwitchCamTrevorInHeliToMichael.ciSpline)
					IF NOT IS_CAM_ACTIVE(scsSwitchCamTrevorInHeliToMichael.ciSpline)
						SET_PLAYER_CONTROL(player_id(),true)
						CLEAR_PED_TASKS(player_ped_id())
						bCustomSwitchCamRendering_HeliToCar = FALSE
					ENDIF
				ENDIF	
				
			endif
			
			bcustomswitch_mike_to_trev_inCar 	= false
			bcustomswitch_mike_to_trev_inHeli	= false
			bcustomswitch_trev_to_mike			= false
			
			//[MF] Only stop redering the script cam if we aren't currently displaying the switch cam from Michael that transitions directly to a cutscene.		
			IF DOES_CAM_EXIST(scsSwitchCamTrevorInHeliToMichael.ciSpline)
				IF NOT IS_CAM_ACTIVE(scsSwitchCamTrevorInHeliToMichael.ciSpline)
					RENDER_SCRIPT_CAMS(false,true)			
				ENDIF
			ELSE
				RENDER_SCRIPT_CAMS(false,true)
			ENDIF
			
			SET_FRONTEND_RADIO_ACTIVE(true)
			iLastSwapTimer = get_game_timer() //so that if the player quickly goes back and forth they wont see an unrealistic leap
			PAUSE_CLOCK(false)			
								
			//cleanup stuff			
			CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
			CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
			CLEANUP_SELECTOR_CAM(sCamDetails)
			stop_mission_audio_scenes()
						
//			if eDialogue_stateM != DI_EXIT_DIALOGUE_MIKE
//				eDialogue_stateM = DI_START_CONDITION_MIKE
//			endif
//			if eDialogue_stateT != DI_EXIT_DIALOGUE_TREVOR
//				eDialogue_stateT = DI_START_CONDITION_TREVOR
//			endif
			
			if PLAYER_PED_ID() = TREV()
				if mission_stage < enum_to_int(msf_4_heli_follow_truck)
					if heli_switch = TO_HELI
						if not IS_AUDIO_SCENE_ACTIVE("BS_1_SWITCH_TO_TREVOR")
							START_AUDIO_SCENE("BS_1_SWITCH_TO_TREVOR")
						endif
					elif heli_switch = TO_TRUCKS
						if not IS_AUDIO_SCENE_ACTIVE("BS_1_FLY_TO_TRUCKS")
							START_AUDIO_SCENE("BS_1_FLY_TO_TRUCKS")
						endif
					endif				
				endif
				if IsEntityAlive(vehs[mvf_helicopter].id)
					SET_ENTITY_INVINCIBLE(vehs[mvf_helicopter].id,false)
				endif
			else
			//move heli to safe place and restore at least half health
				if IsEntityAlive(vehs[mvf_helicopter].id)
				and IsEntityAlive(TREV())
				and IsEntityAlive(LESTER())
					if GET_ENTITY_HEALTH(vehs[mvf_helicopter].id) < 700
						SET_ENTITY_HEALTH(vehs[mvf_helicopter].id,700)
					endif
					if IS_HELI_PART_BROKEN(vehs[mvf_helicopter].id,true,true,true)
						SET_VEHICLE_FIXED(vehs[mvf_helicopter].id)
					ENDIF 
					if is_ped_in_vehicle(TREV(),vehs[mvf_helicopter].id)
					and is_ped_in_vehicle(LESTER(),vehs[mvf_helicopter].id)
						VECTOR currenthelipos = GET_ENTITY_COORDS(vehs[mvf_helicopter].id)						
						if IS_ENTITY_OCCLUDED(vehs[mvf_helicopter].id)
						and not IS_ENTITY_ON_SCREEN(vehs[mvf_helicopter].id)
						and currenthelipos.z < 300
							SET_ENTITY_COORDS(vehs[mvf_helicopter].id,(currenthelipos + <<0,0,200>>),true,true)
							SET_HELI_BLADES_FULL_SPEED(vehs[mvf_helicopter].id)
							SET_VEHICLE_ENGINE_ON(vehs[mvf_helicopter].id,true,true)
						endif
					endif
					SET_ENTITY_INVINCIBLE(vehs[mvf_helicopter].id,true)
				endif
			endif 
			//=======================================  unSafety =============================================	
			if IsEntityAlive(mike())
				SET_ENTITY_INVINCIBLE(MIKE(),false)
			endif
			if IsEntityAlive(lester())
				SET_ENTITY_INVINCIBLE(LESTER(),false)
			endif
			if IsEntityAlive(trev())
				SET_ENTITY_INVINCIBLE(TREV(),false)
			endif
			if IsEntityAlive(frank())
				SET_ENTITY_INVINCIBLE(FRANK(),false)
			endif
			if IsEntityAlive(vehs[mvf_helicopter].id)
				SET_ENTITY_INVINCIBLE(vehs[mvf_helicopter].id,false)
			endif
			if IsEntityAlive(vehs[mvf_trevor_veh].id)
				SET_ENTITY_INVINCIBLE(vehs[mvf_trevor_veh].id,false)
			endif
			if IsEntityAlive(vehs[mvf_Michael_veh].id)
				SET_ENTITY_INVINCIBLE(vehs[mvf_Michael_veh].id,false)		
			endif		
		endif
	endif
endproc
// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Cleanup(bool bMissionPassed = false)

		if GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_MICHAEL
			g_replay.iReplayInt[2] = 0
		elif GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_TREVOR			
			g_replay.iReplayInt[2] = 1
		endif
		
		IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Michael_veh].id)
			SET_VEHICLE_RADIO_ENABLED(vehs[mvf_Michael_veh].id,true)
		ENDIF
		
		UPDATE_DYNAMIC_VEHICLE_GEN_POSITION(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY, <<0,0,0>>, 0.0)
		
		SET_BUILDING_STATE( BUILDINGNAME_IPL_BIG_SCORE_HOLE_COVER, BUILDINGSTATE_NORMAL) 
		SET_BUILDING_STATE( BUILDINGNAME_IPL_JEWEL_HEIST_BIKE_TUNNELS, BUILDINGSTATE_NORMAL) 
		
		IF IS_CUTSCENE_ACTIVE()			
			STOP_CUTSCENE_IMMEDIATELY()
			REMOVE_CUTSCENE()		
		ENDIF
		
		if DOES_ENTITY_EXIST(TREV())
		AND NOT IS_ENTITY_DEAD(TREV())
			IF NOT IS_ENTITY_VISIBLE(TREV())
				SET_ENTITY_VISIBLE(TREV(),true)
			ENDIF
		endif
		if NOT IS_ENTITY_DEAD(lester())
			IF NOT IS_ENTITY_VISIBLE(lester())
				SET_ENTITY_VISIBLE(lester(),true)
			ENDIF
		endif
		if NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
			IF NOT IS_ENTITY_VISIBLE(vehs[mvf_helicopter].id)
				SET_ENTITY_VISIBLE(vehs[mvf_helicopter].id,true)
			ENDIF
		endif
		
		SET_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY, TRUE)
		RENDER_SCRIPT_CAMS(false,false)
		DISPLAY_RADAR(true)
		DISPLAY_HUD(true)
		STOP_SOUND(camZoom)
		STOP_SOUND(camhum)
		CLEAR_TIMECYCLE_MODIFIER()
//========================PEDS VEHS======================
		for i = 0 TO Enum_to_int(MPF_NUM_OF_PEDS)-1
			if DOES_ENTITY_EXIST(peds[i].id)
			AND (NOT IS_PED_INJURED(peds[i].id))				
				if peds[i].id != player_ped_id()
					if peds[i].id = peds[mpf_franklin].id
					or peds[i].id = peds[mpf_lester].id
					or peds[i].id = peds[mpf_michael].id
					or peds[i].id = peds[mpf_trevor].id
						SAFE_DELETE_PED(peds[i].id)
					else
						SAFE_RELEASE_PED(peds[i].id)
					endif
				endif
				if DOES_BLIP_EXIST(peds[i].blip)
					REMOVE_BLIP(peds[i].blip)
				endif
				REMOVE_SCENARIO_BLOCKING_AREA(peds[i].scenario_index)
			endif
		endfor	
		//all vehicles are now no longer needed
		for i = 0 TO Enum_to_int(MVF_NUM_OF_VEH)-1
			if DOES_ENTITY_EXIST(vehs[i].id)	
				SAFE_RELEASE_VEHICLE(vehs[i].id)
			endif
		endfor
		
		//objects
		if DOES_ENTITY_EXIST(objdummy)
			DELETE_OBJECT(objdummy)
		endif
		if DOES_ENTITY_EXIST(objcam)
			if IS_ENTITY_ATTACHED(objcam)
				DETACH_ENTITY(objcam)
			endif
			SAFE_DELETE_OBJECT(objcam)
		endif
//===========Destroys============
		
		give_all_peds_earpiece(true) //bremove true
		
		DESTROY_ALL_CAMS()	
		CLEAR_FOCUS()	
		CLEANUP_SELECTOR_CAM(sCamDetails)
		
		TRIGGER_MUSIC_EVENT("FH1_FAIL")
		
		
		stop_mission_audio_scenes()
		CLEAR_PRINTS()
		if IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
		ENDIF
		remove_cutscene()
		STREAMVOL_DELETE(vol_construction)
		NEW_LOAD_SCENE_STOP()
		
		RELEASE_SOUND_ID(camzoom)
		RELEASE_SOUND_ID(camhum)
		
		if interior_carpark != null
			UNPIN_INTERIOR(interior_carpark)
		endif
		if interior_tunnel != null
			UNPIN_INTERIOR(interior_tunnel)
		endif
		if interior_constuction != null
			UNPIN_INTERIOR(interior_constuction)
		endif
		
		KILL_ANY_CONVERSATION()
		
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
		CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
			
		if DOES_BLIP_EXIST(blip_objective)
			remove_blip(blip_objective)
		endif
		
		SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfmov)
		SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_ARMOURED_01)
		SET_MODEL_AS_NO_LONGER_NEEDED(BULLDOZER)
		REMOVE_ANIM_DICT("missbigscore1guard_wait_rifle")
	//===============================
	
//============Resets=============

		If is_screen_faded_out()
		and bMissionPassed
			DO_SCREEN_FADE_IN(2000)
		ENDIF		
		
		SET_PLAYER_CONTROL(player_id(),true)
		
		//cam
		Display_hud(true)
		DISPLAY_RADAR(true)
		RENDER_SCRIPT_CAMS(false,false)	
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		STOP_GAMEPLAY_HINT(true)
		SET_CINEMATIC_BUTTON_ACTIVE(true)
		
		//uber
		CLEANUP_UBER_PLAYBACK()
		SET_VEHICLE_POPULATION_BUDGET(3)
		SET_PED_POPULATION_BUDGET(3)
		uber_state = UBER_INIT		
		
		//tasks
		if not IS_PED_INJURED(player_ped_id())
			CLEAR_PED_TASKS(player_ped_id())
		endif
		CLEAR_SEQUENCE_TASK(seq)
		CLEAR_SEQUENCE_TASK(peds[mpf_guard].seq)
		CLEAR_SEQUENCE_TASK(peds[mpf_guardf].seq)
		
		//off mission
		SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER,false)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(BODHI2,false)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE,true)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING,true)
		ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER,true)
		SET_ALL_RANDOM_PEDS_FLEE(player_id(),false)
		SET_MAX_WANTED_LEVEL(5)
		
		SET_ROADS_BACK_TO_ORIGINAL(<< 534.7286, -1144.5100, 8.0146 >>,<< 243.1961, -560.9398, 65.8140 >>)		
		SET_WANTED_LEVEL_MULTIPLIER(1)
		ENABLE_STRIP_CLUBS() 				
		REMOVE_SCENARIO_BLOCKING_AREAS()
		CLEAR_WEATHER_TYPE_PERSIST()

		disable_taxi_hailing(false)
	//===============================

	PRINTSTRING("...finale heist 1 Mission Cleanup")
	PRINTNL()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Passed()
	stop_mission_audio_scenes()
	PRINTSTRING("...finale hesit 1 Mission Passed")
	PRINTNL()	

	Mission_Flow_Mission_Passed()
	Mission_Cleanup(true)
	
	// B*1409141 - Ensure Chop is present at Franklin's apartment
	REACTIVATE_NAMED_WORLD_BRAINS_WAITING_TILL_OUT_OF_RANGE("chop")
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Failed(MFF_MISSION_FAIL_FLAGS fail_condition = mff_default)
if not sCamDetails.bRun
	TRIGGER_MUSIC_EVENT("FH1_FAIL")
	KILL_ANY_CONVERSATION()
	STRING strReason = ""
		
	//show fail message
	SWITCH fail_condition
		CASE mff_debug_fail
			strReason = "FH1_FAILDB"
		BREAK	
		case mff_franklin_dead_fail
			strReason = "CMN_FDIED"
		break
		case mff_lester_dead_fail
			strReason = "FH1_FAILLD"
		break
		case mff_lost_truck
			strReason = "FH1_FAILLT"
		break
		case mff_mike_dead_fail
			strReason = "CMN_MDIED"
		break
		case mff_too_close_fail
			strReason = "FH1_FAILTC"
		break
		case mff_trevor_dead_fail
			strReason = "CMN_TDIED"
		break
		case mff_Mike_car_des
			strReason = "CMN_MDEST"
		break
		case mff_trev_car_des
			strReason = "CMN_TDEST"
		break
		case mff_heli_des
			strReason = "FH1_FAILHD"
		break
		case mff_bank_knows
			strReason = "FH1_FAILBK"
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),2)
		break
		case mff_left_franklin
			strReason = "CMN_FLEFT"
		break
		case mff_left_lester
			strReason = "FH1_FAILLES"
		break
		case mff_time		
			
			strReason = "FH1_FAILTI"
		break
		case mff_left_mission
			strreason = "FH1_FMIKEL"
		break
		DEFAULT
			strReason = "FH1_FAILDF"
		BREAk
	ENDSWITCH
	
	
	//if in the custom lester cam clean this up before fail screen 
	if NOT IS_ENTITY_DEAD(TREV())
		IF NOT IS_ENTITY_VISIBLE(TREV())
			SET_ENTITY_VISIBLE(TREV(),true)
		ENDIF
	endif
	if NOT IS_ENTITY_DEAD(lester())
		IF NOT IS_ENTITY_VISIBLE(lester())
			SET_ENTITY_VISIBLE(lester(),true)
		ENDIF
	endif
	if NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
		IF NOT IS_ENTITY_VISIBLE(vehs[mvf_helicopter].id)
			SET_ENTITY_VISIBLE(vehs[mvf_helicopter].id,true)
		ENDIF
	endif
	RENDER_SCRIPT_CAMS(false,false)
	DISPLAY_RADAR(true)
	DISPLAY_HUD(true)
	TRIGGER_MUSIC_EVENT("FH1_FAIL")
	STOP_SOUND(camZoom)
	STOP_SOUND(camhum)
	CLEAR_TIMECYCLE_MODIFIER()
	
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(strReason)
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		KILL_ANY_CONVERSATION()
		if fail_condition = mff_default
		and not bFailedConvo
			if IsEntityAlive(LESTER())
				ADD_PED_FOR_DIALOGUE(convo_struct,3,LESTER(),"Lester")
				if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_FAILTME",CONV_PRIORITY_MEDIUM)
					bFailedConvo = true
				endif
			endif
		endif
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 		
		WAIT(0)
	ENDWHILE
	
	// Call RemovePlayerFromRestrictedVehicle() here if you need to
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<92.0847, -1280.7062, 28.1447 >>, 73.3971)
//	SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<-1574.9478, 5169.6572, 18.5775>>, 163.3193)
	
	MISSION_CLEANUP() // must only take 1 frame and terminate the thread
	TERMINATE_THIS_THREAD()
endif
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		MISSION STAGE MANAGEMENT
// -----------------------------------------------------------------------------------------------------------

PROC Mission_stage_management()
	SWITCH stageswitch
		CASE STAGESWITCH_REQUESTED
			CPRINTLN(DEBUG_MISSION,"[stageManagement] mission_stage switch requested from mission_stage: ", mission_stage, " to mission_stage ", requestedStage)
			stageswitch = STAGESWITCH_EXITING
			mission_substage = STAGE_EXIT
		BREAK
		CASE STAGESWITCH_EXITING
			CPRINTLN(DEBUG_MISSION,"[StageManagement] Exiting mission_stage: ", mission_stage)
			stageSwitch = STAGESWITCH_ENTERING
			mission_substage = STAGE_ENTRY
			mission_stage = requestedStage
		BREAK
		CASE STAGESWITCH_ENTERING
			CPRINTLN(DEBUG_MISSION,"[StageManagement] Entered mission_stage: ", mission_stage)
			requestedStage = -1
			stageSwitch = STAGESWITCH_IDLE
		BREAK
		CASE STAGESWITCH_IDLE
			IF (GET_GAME_TIMER() - iStageTimer) > 2500
			CPRINTLN(DEBUG_MISSION,"[StageManagement] mission_stage: ", mission_stage, " mission_substage: ", mission_substage)
				iStageTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
FUNC BOOL Mission_Set_Stage(MSF_MISSION_STAGE_FLAGS newStage)
	If stageswitch = STAGESWITCH_IDLE
		requestedstage = ENUM_TO_INT(newStage)
		stageswitch = STAGESWITCH_REQUESTED		
		return True
	else
		return false
	endif
ENDFUNC
PROC unload_uber_chase() 	
	for i = 1 to 67			
		if HAS_VEHICLE_RECORDING_BEEN_LOADED(i,"FH1UBER")			
			CPRINTLN(DEBUG_MISSION,"remove rec: ",i)	
			REMOVE_VEHICLE_RECORDING(i,"FH1UBER")
		endif
	endfor
endproc
Proc Load_asset_Stage(MSF_MISSION_STAGE_FLAGS newMissionStage,bool Breset = false)
			
			
	PRINTLN("Load_asset_Stage : ", ENUM_TO_INT(newMissionStage))
	//asset loading
	switch newMissionStage
		case msf_0_strip_club					
			Load_Asset_Model(sAssetData,frogger2)	//helicopter 
		break
		case msf_1_font_of_bank						
			Load_Asset_AnimDict(sAssetData,"missbigscore1guard_wait_rifle")
			Load_Asset_Model(sAssetData,frogger2)		//helicopter 
			Load_Asset_Recording(sAssetData,001,"FH1RECMIKE")
			Load_Asset_Recording(sAssetData,002,"FH1RECMIKE")
			Load_Asset_Recording(sAssetData,003,"FH1REC")
			Load_Asset_Recording(sAssetData,004,"FH1REC")
			
			//[MF] Switch Cam Assets
			Load_Asset_AnimDict(sAssetData, sSwitchAnimLibrary_MikeAndFrankAnims)
			Load_Asset_AnimDict(sAssetData, sSwitchAnimLibraryCarToTruck_PissAnims)
			Load_Asset_AnimDict(sAssetData, sSwitchAnimLibraryTrevToCar_HeliAnims)
			Load_Asset_AnimDict(sAssetData, "shake_cam_all@")
		break
		case msf_2_back_of_bank	
		case msf_3_get_in_position
			if bReset 									
				Load_Asset_Model(sAssetData,frogger2)	//helicopter 
				Load_Asset_AnimDict(sAssetData,"missbigscore1guard_wait_rifle")
				Load_Asset_Recording(sAssetData,003,"FH1REC")
				Load_Asset_Recording(sAssetData,004,"FH1REC")
				
				//[MF] Switch Cam Assets
				Load_Asset_AnimDict(sAssetData, sSwitchAnimLibrary_MikeAndFrankAnims)
				Load_Asset_AnimDict(sAssetData, sSwitchAnimLibraryCarToTruck_PissAnims)
				Load_Asset_AnimDict(sAssetData, sSwitchAnimLibraryTrevToCar_HeliAnims)
				Load_Asset_AnimDict(sAssetData, "shake_cam_all@")
			endif
			REQUEST_CUTSCENE("bss_1_mcs_2")	
			SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_OFF) //#BUG:1542524
		break
		case msf_4_heli_follow_truck			
			if bReset 
//				Load_Asset_Model(IG_LESTERCREST)//Lester									
				Load_Asset_Model(sAssetData,frogger2)	//helicopter 
				Load_Asset_AnimDict(sAssetData,"missbigscore1guard_wait_rifle")
			endif
			
			Load_Asset_Model(sAssetData,S_M_M_ARMOURED_01)
			Load_Asset_Model(sAssetData,STOCKADE) // security truck
			load_asset_model(sAssetData,PROP_PAP_CAMERA_01)
			Load_Asset_Recording(sAssetData,001,"FH1UBER")			
			Load_Asset_AnimDict(sAssetData, "missheist_the_big_score_setup_1@camera@idle_a")
			
		break
		case msf_5_CONSTRUCTION_HOLE
			if bReset 			
//				Load_Asset_Model(IG_LESTERCREST)//Lester								
				Load_Asset_Model(sAssetData,frogger2)	//helicopter 
				Load_Asset_AnimDict(sAssetData,"missbigscore1guard_wait_rifle")
				Load_Asset_AnimDict(sAssetData, "missheist_the_big_score_setup_1@camera@idle_a")
				load_asset_model(sAssetData,PROP_PAP_CAMERA_01)				
			endif
			load_asset_model(sAssetData,PROP_LD_TEST_01)
		break
		case msf_6_drop_off_point
			if bReset 	
//				Load_Asset_Model(IG_LESTERCREST)//Lester									
				Load_Asset_Model(sAssetData,frogger2)	//helicopter 
				Load_Asset_AnimDict(sAssetData,"missbigscore1guard_wait_rifle")	
			endif
			Load_Asset_AnimDict(sAssetData,"missheist_the_big_score_setup_1@heli_exit")
			Load_Asset_Recording(sAssetData,005,"FH1REC")
			Load_Asset_Recording(sAssetData,003,"FH1RECMIKE")
		break
		case msf_7_passed
			Load_Asset_Model(sAssetData,frogger2)
		break
	endswitch
	
	
endproc

PROC RESET_EVERYTHING()
	
	IF IS_CUTSCENE_ACTIVE()			
		STOP_CUTSCENE_IMMEDIATELY()
		REMOVE_CUTSCENE()
		WHILE IS_CUTSCENE_ACTIVE()
			WAIT(0)
		ENDWHILE		
	ENDIF
	
	if not IS_REPLAY_BEING_SET_UP()
		NEW_LOAD_SCENE_STOP()
		CLEAR_FOCUS()
		IF STREAMVOL_IS_VALID(vol_construction)
			STREAMVOL_DELETE(vol_construction)	
		endif
	endif
	
	//==============PEDS==============
	for i = 0 TO Enum_to_int(MPF_NUM_OF_PEDS)-1
		if DOES_ENTITY_EXIST(peds[i].id)
		AND (NOT IS_PED_INJURED(peds[i].id))	
			if IS_PED_IN_ANY_VEHICLE(peds[i].id)				
				SET_PED_COORDS_NO_GANG(peds[i].id,(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(peds[i].id))+<<0,-2,0>>))				
			endif
			if peds[i].id != player_ped_id()
				DELETE_PED(peds[i].id)			
			endif				
		endif
	endfor
	//================================
	
	//==============VEHS==============
	for i = 0 TO Enum_to_int(MVF_NUM_OF_VEH)-1
		if DOES_ENTITY_EXIST(vehs[i].id)		
		 	DELETE_VEHICLE(vehs[i].id)
		endif
	endfor	
	//================================	
	//objects
	if DOES_ENTITY_EXIST(objcam)
		if IS_ENTITY_ATTACHED(objcam)
			DETACH_ENTITY(objcam)
		endif
		SAFE_DELETE_OBJECT(objcam)
	endif
	if DOES_ENTITY_EXIST(objdummy)
		DELETE_OBJECT(objdummy)
	endif
	//-----------RESETS--------------
	CLEANUP_UBER_PLAYBACK()
	SWITCH_ALL_ROADS_BACK_TO_ORIGINAL()
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_PED_POPULATION_BUDGET(3)
	uber_state = UBER_INIT
	
	if not IS_PED_INJURED(player_ped_id())
		CLEAR_PED_TASKS(player_ped_id())
		SET_ENTITY_VISIBLE(player_ped_id(),true)
	endif
	
	SET_WANTED_LEVEL_MULTIPLIER(0.8)
	CLEAR_PLAYER_WANTED_LEVEL(player_id())
	
	Display_hud(true)
	DISPLAY_RADAR(true)
	RENDER_SCRIPT_CAMS(false,false)	
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	CLEAR_SEQUENCE_TASK(seq)
	CLEAR_SEQUENCE_TASK(peds[mpf_guard].seq)
	CLEAR_SEQUENCE_TASK(peds[mpf_guardf].seq)
	
	iHoleTimer = get_Game_timer()
	iLastSwapTimer = get_game_timer()
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	STOP_GAMEPLAY_HINT(true)
	SET_CINEMATIC_BUTTON_ACTIVE(true)
	
	//swaps
	bHotSwap 							= false	
	bcustomswitch_mike_to_trev_inCar 	= false
	bcustomswitch_mike_to_trev_inHeli	= false
	bcustomswitch_trev_to_mike			= false
	
	bLeftbuddyprint		= false 
	bheliPrintDisplayed = false
	bLeftArea_printed	= false
	
	bTruckSkipForward	= false
	bheliset 			= false
	btimeReset 	 		= false	
	
	bfirstwarningCL		= true
	bFailTimerStarted	= true
	bMissedBackCS 		= false
	
	b_displayed_get_heli= false
	bHeliStageLoaded 	= false
	bFCSanimsLoaded 	= false
	bAUDIOStreamLoaded 	= false
	bplayingstream		= false
	b_cutscene_loaded	= false
	bIgnoreNoPlayerControl = FALSE
	
	CLEANUP_SELECTOR_CAM(sCamDetails)
	//----------- Dialogue ---------
	
	eDialogue_stateM = DI_START_CONDITION_MIKE
	eDialogue_stateT = DI_START_CONDITION_TREVOR
	eInterruptstage = INTERRUPT_NONE
	iDialogueTimer	= get_game_timer()
	bDialoguePlayed = false
	bFailedConvo	= false
	bPrisonWarning	= false
	lbl_Interrupt 	= ""
	lbl_root	  	= ""
	lbl_MikeswitchInterrupt 	= ""
	lbl_mikeswitchroot 			= ""		
	b_mikeSpeechSwitchStopped 	= false
	lbl_trevswitchInterrupt 	= ""
	lbl_trevswitchroot 			= ""		
	b_trevSpeechSwitchStopped 	= false
	

	//------------CS--------------	
	bcs_mike	= false
	bcs_trev	= false
	bcs_frank	= false
	bcs_lester	= false
	bcs_sec		= false
	bcs_mikecar = false
	bcs_cam		= false
	
	bSwitchedToMike = false
	//------------ streaming assets --------------	
	bloadDozer 			= false
	bcreateDozers 		= false
	bloadGuards 		= false
	bcreateGuards 		= false
	iSFstage 			= 0
	//----------DESTROYS-------------	
		
	TRIGGER_MUSIC_EVENT("FH1_FAIL")
	stop_mission_audio_scenes()
	STOP_SOUND(camZoom)
	STOP_SOUND(camhum)
	DESTROY_ALL_CAMS()	
	CLEAR_TIMECYCLE_MODIFIER()
	
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfmov)
	
	CLEAR_PRINTS()	
	REMOVE_SCENARIO_BLOCKING_AREAS()
	if interior_constuction != null
		UNPIN_INTERIOR(interior_constuction)
	endif
	if interior_Tunnel != null
		UNPIN_INTERIOR(interior_tunnel)
	endif
	if interior_carpark != null
		UNPIN_INTERIOR(interior_carpark)
	endif
	KILL_ANY_CONVERSATION()
	
	//locates stuff
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
		
	if DOES_BLIP_EXIST(blip_objective)
		remove_blip(blip_objective)
	endif
		
	remove_PED_FOR_DIALOGUE(convo_struct,0)
	remove_PED_FOR_DIALOGUE(convo_struct,1)
	remove_PED_FOR_DIALOGUE(convo_struct,2)
	remove_PED_FOR_DIALOGUE(convo_struct,3)	
	
	//-------------------------------
	
ENDPROC
PROC SU_GENERAL()

	SET_MAX_WANTED_LEVEL(2)

	if DOES_ENTITY_EXIST(trev())
		if player_ped_id() != trev()
			SET_PED_RELATIONSHIP_GROUP_HASH(trev(),rel_buddy)	
		endif
	endif	
	if DOES_ENTITY_EXIST(mike())		
		if player_ped_id() != mike()
			SET_PED_RELATIONSHIP_GROUP_HASH(mike(),rel_buddy)		
		endif		
	endif
	if DOES_ENTITY_EXIST(frank())
		SET_PED_RELATIONSHIP_GROUP_HASH(frank(),rel_buddy)
	endif
	
	if IsEntityAlive(lester())
		SET_PED_RELATIONSHIP_GROUP_HASH(lester(),rel_buddy)
		SET_PED_MOVEMENT_CLIPSET(lester(),"move_lester_CaneUp")	
		SET_PED_WEAPON_MOVEMENT_CLIPSET(lester(),"move_lester_CaneUp")	
	endif	
	
	
	if IsEntityAlive(vehs[mvf_Michael_veh].id)
		SET_ENTITY_COLLISION(vehs[mvf_Michael_veh].id,true)	
		SET_VEHICLE_HAS_STRONG_AXLES(vehs[mvf_Michael_veh].id,true)
	endif
	if IsEntityAlive(vehs[mvf_trevor_veh].id)
		SET_ENTITY_COLLISION(vehs[mvf_trevor_veh].id,true)
		SET_VEHICLE_HAS_STRONG_AXLES(vehs[mvf_trevor_veh].id,true)
	endif
	
	
	if player_ped_id() = mike()
		//on
		if IsEntityAlive(TREV())
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(TREV(),true)
		endif				
		if IsEntityAlive(lester())
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(lester(),true)
		endif
		if IsEntityAlive(vehs[mvf_helicopter].id)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_helicopter].id,true)
		endif
		if IsEntityAlive(vehs[mvf_trevor_veh].id)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_trevor_veh].id,true)
		endif
		//off
		if IsEntityAlive(mike())
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(MIKE(),false)
		endif
		if IsEntityAlive(FRANK())
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(frank(),false)
		endif
		if IsEntityAlive(vehs[mvf_Michael_veh].id)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_Michael_veh].id,false)
		endif

	elif player_ped_id() = trev()
		//on
		if IsEntityAlive(TREV())
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(TREV(),false)
		endif				
		if IsEntityAlive(lester())
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(lester(),false)
		endif
		if IsEntityAlive(vehs[mvf_helicopter].id)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_helicopter].id,false)
		endif
		if IsEntityAlive(vehs[mvf_trevor_veh].id)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_trevor_veh].id,false)
		endif
		//off
		if IsEntityAlive(mike())
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(MIKE(),true)
		endif
		if IsEntityAlive(FRANK())
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(frank(),true)
		endif
		if IsEntityAlive(vehs[mvf_Michael_veh].id)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_Michael_veh].id,true)
		endif			
	endif
	
	//ready for checks
	btimeReset = true		
endproc
PROC SU_0_STRIP_CLUB()
	SET_CLOCK_TIME(12,0,0)
	//michael start	
	Set_Current_Player_Ped(SELECTOR_PED_MICHAEL,true)
	peds[mpf_michael].id = player_ped_id()	
	
	REQUEST_CLIP_SET("move_lester_CaneUp")
	while not HAS_CLIP_SET_LOADED("move_lester_CaneUp")
		wait(0)
	endwhile		
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_Michael_veh].id,CHAR_MICHAEL,<< 90.3864, -1278.8374, 28.0896 >>, 98.4832)
		wait(0)
	endwhile
	while not CREATE_PLAYER_PED_ON_FOOT(peds[mpf_franklin].id,CHAR_FRANKLIN,<< 94.5331, -1277.5521, 28.1446 >>,15.2302)
		wait(0)
	endwhile
	
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_trevor_veh].id,CHAR_TREVOR,<< 83.4733, -1281.3232, 29.1518 >>, 96.9356)
		wait(0)
	endwhile
	SET_ENTITY_COLLISION(vehs[mvf_trevor_veh].id,true)
	while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_trevor].id,CHAR_TREVOR,vehs[mvf_trevor_veh].id)
		wait(0)
	endwhile
	while not CREATE_NPC_PED_INSIDE_VEHICLE(peds[mpf_lester].id,CHAR_LESTER,vehs[mvf_trevor_veh].id,VS_FRONT_RIGHT)
		wait(0)
	endwhile
	SET_PED_PROP_INDEX(peds[mpf_lester].id, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), false)
	endif
	
	btimeReset 			= true
	heli_switch 		= TO_HELI
	eTREV_Dialogue 		= T_di_FILL1
	eMike_dialogue 		= M_di_TO_FRONT
	eBOTH_dialogue 		= BOTH_TIME_INFO	
	CLEAR_AREA_OF_PEDS(<< 97.6772, -1290.7390, 28.2688 >>,300)	
	Point_Gameplay_cam_at_coord(get_entity_heading(player_ped_id()))	
	
ENDPROC
PROC SU_1_FONT_OF_BANK()
		
	//michael		
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_Michael_veh].id,CHAR_MICHAEL,vFRONT_BANK, fHEADING_FRONT )		
		wait(0)
	endwhile
	//franklin
	while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_franklin].id,CHAR_FRANKLIN,vehs[mvf_Michael_veh].id,VS_FRONT_RIGHT)
		wait(0)
	endwhile	
	//trev veh
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_trevor_veh].id,CHAR_TREVOR,<< 1681.2954, 1320.6780, 86.0397 >>, 346.5002)		
		wait(0)							
	endwhile
	//lester
	while not CREATE_NPC_PED_INSIDE_VEHICLE(peds[mpf_lester].id,CHAR_LESTER,vehs[mvf_trevor_veh].id,VS_FRONT_RIGHT)
		wait(0)
	endwhile
	SET_PED_PROP_INDEX(peds[mpf_lester].id, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
	REQUEST_CLIP_SET("move_lester_CaneUp")
	while not HAS_CLIP_SET_LOADED("move_lester_CaneUp")
		wait(0)
	endwhile

	if g_replay.iReplayInt[2] = 0	
		Set_Current_Player_Ped(SELECTOR_PED_MICHAEL,true)
		peds[mpf_michael].id = player_ped_id()	
		//trev
		while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_trevor].id,CHAR_TREVOR,vehs[mvf_trevor_veh].id)
			wait(0)
		endwhile
		if IsEntityAlive(mike())
		and IsEntityAlive(vehs[mvf_Michael_veh].id)
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP(vehs[mvf_Michael_veh].id)
			Else
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
				SET_PED_INTO_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
			endif
		endif
						
	elif g_replay.iReplayInt[2] = 1
		Set_Current_Player_Ped(SELECTOR_PED_TREVOR,true)
		peds[mpf_trevor].id = player_ped_id()
		//trev
		while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_michael].id,CHAR_MICHAEL,vehs[mvf_Michael_veh].id)
			wait(0)
		endwhile
		if IsEntityAlive(TREV())
		and IsEntityAlive(vehs[mvf_trevor_veh].id)
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP(vehs[mvf_trevor_veh].id)
			Else
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
				SET_PED_INTO_VEHICLE(TREV(),vehs[mvf_trevor_veh].id)
			endif
			SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_trevor_veh].id)	
			SET_VEHICLE_FORWARD_SPEED(vehs[mvf_trevor_veh].id,25)
			SET_VEHICLE_ENGINE_ON(vehs[mvf_trevor_veh].id,true,true)
		endif	
		if not IS_AUDIO_SCENE_ACTIVE("BS_1_SWITCH_TO_TREVOR")
			START_AUDIO_SCENE("BS_1_SWITCH_TO_TREVOR")
		endif
	endif
	
	ADD_PED_FOR_DIALOGUE(convo_struct,0,trev(),"TREVOR",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,1,mike(),"MICHAEL",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,2,frank(),"FRANKLIN",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,3,lester(),"LESTER",true)
	
	//heli
	vehs[mvf_helicopter].id = CREATE_VEHICLE(frogger2,<< 1758.3090, 3284.5278, 40.7 >>, 133.1852)
	SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_helicopter].id,false)
	SET_ENTITY_ALWAYS_PRERENDER(vehs[mvf_helicopter].id,true)
	SET_VEHICLE_LIVERY(vehs[mvf_helicopter].id,1)
	SET_ENTITY_COLLISION(vehs[mvf_helicopter].id,true)
	FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id,true)
	SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_helicopter].id,false)
	Unload_Asset_Model(sAssetData,frogger2)
	give_all_peds_earpiece()	
	
	if IS_REPLAY_IN_PROGRESS()
	and g_replayMissionStage = 1
	and g_replay.iReplayInt[0] = 1
	and not g_bShitskipAccepted
	and not b_zskipped
		SET_CLOCK_TIME(13,15,0)
	else
		SET_CLOCK_TIME(12,45,0)
	endif
	
	btimeReset = true		
	Hotswap_Menu(false,true,false)
	bHotSwap = true	
	
	heli_switch = TO_HELI
	eTREV_Dialogue = T_di_FILL1
	eMike_dialogue = M_di_TO_BACK
	eBOTH_dialogue = BOTH_WARN1
	
	clear_area(GET_ENTITY_COORDS(player_ped_id()),50,true)	
	Point_Gameplay_cam_at_coord(get_entity_heading(player_ped_id()))
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Stage 1: Front of bank")
	
ENDPROC
PROC SU_2_BACK_OF_BANK()
		
	// Set the position you want the vehicle to spawn at.
	UPDATE_DYNAMIC_VEHICLE_GEN_POSITION(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY, <<1702.9482, 3272.0217, 40.1539>>, 247.3247)

	// Delete the vehicle gen and create your version of the frogger here.
	DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY)

	// Force the current one to respawn at new location
	CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY)
	
	//heli
	vehs[mvf_helicopter].id = CREATE_VEHICLE(frogger2,<< 1758.3090, 3284.5278, 40.7 >>, 133.1852)
	SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_helicopter].id,false)
	SET_ENTITY_ALWAYS_PRERENDER(vehs[mvf_helicopter].id,true)
	SET_VEHICLE_LIVERY(vehs[mvf_helicopter].id,1)
	SET_ENTITY_COLLISION(vehs[mvf_helicopter].id,true)
	FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id,true)
	Unload_Asset_Model(sAssetData,frogger2)
	//michael
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_Michael_veh].id,CHAR_MICHAEL,vBACK_BANK,fHEADING_BACK)	
		wait(0)
	endwhile					
	//franklin
	while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_franklin].id,CHAR_FRANKLIN,vehs[mvf_Michael_veh].id,VS_FRONT_RIGHT)
		wait(0)
	endwhile
	//trev veh
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_trevor_veh].id,CHAR_TREVOR,<<1993.6239, 2541.7837, 53.6037>>,318.9926)		
		wait(0)							
	endwhile
	REQUEST_CLIP_SET("move_lester_CaneUp")
	while not HAS_CLIP_SET_LOADED("move_lester_CaneUp")
		wait(0)
	endwhile
	
	if g_replay.iReplayInt[2] = 0
		Set_Current_Player_Ped(SELECTOR_PED_MICHAEL,true)
		peds[mpf_michael].id = player_ped_id()		
		//trev
		while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_trevor].id,CHAR_TREVOR,vehs[mvf_trevor_veh].id)
			wait(0)
		endwhile	
		//lester
		while not CREATE_NPC_PED_INSIDE_VEHICLE(peds[mpf_lester].id,CHAR_LESTER,vehs[mvf_trevor_veh].id,VS_FRONT_RIGHT)
			wait(0)
		endwhile
		SET_PED_PROP_INDEX(peds[mpf_lester].id, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
		
		if IsEntityAlive(vehs[mvf_Michael_veh].id)
		and IsEntityAlive(mike())
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP(vehs[mvf_Michael_veh].id)
			Else
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
				SET_PED_INTO_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
			endif
		endif
		SET_PLAYER_CONTROL(player_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON)
		Hotswap_Menu(false,true,false)
		bHotSwap = false	
		
	elif g_replay.iReplayInt[2] = 1	
		Set_Current_Player_Ped(SELECTOR_PED_TREVOR,true)
		peds[mpf_trevor].id = player_ped_id()
		if not IS_AUDIO_SCENE_ACTIVE("BS_1_SWITCH_TO_TREVOR")
			START_AUDIO_SCENE("BS_1_SWITCH_TO_TREVOR")
		endif
		//mike
		while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_michael].id,CHAR_MICHAEL,vehs[mvf_Michael_veh].id)
			wait(0)
		endwhile	
		//lester
		while not CREATE_NPC_PED_INSIDE_VEHICLE(peds[mpf_lester].id,CHAR_LESTER,vehs[mvf_trevor_veh].id,VS_FRONT_RIGHT)
			wait(0)
		endwhile
		SET_PED_PROP_INDEX(peds[mpf_lester].id, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
		//trev
		if IsEntityAlive(vehs[mvf_trevor_veh].id)
		and IsEntityAlive(TREV())
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP(vehs[mvf_trevor_veh].id)
			Else
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
				SET_PED_INTO_VEHICLE(TREV(),vehs[mvf_trevor_veh].id)
			endif	
			SET_VEHICLE_FORWARD_SPEED(vehs[mvf_trevor_veh].id,25)
			SET_VEHICLE_ENGINE_ON(vehs[mvf_trevor_veh].id,true,true)	
		endif	
		Hotswap_Menu(false,true,false)
		bHotSwap = true	
	endif
	
	if g_replay.iReplayInt[1] = 1		  
		if IS_REPLAY_IN_PROGRESS()
		and g_bShitskipAccepted
		and(  g_replayMissionStage = 3 or  g_replayMissionStage = 2)
			g_replay.iReplayInt[2] = 1	
			bHotSwap 		= false
		endif
		SET_CLOCK_TIME(14,00,0)		
		bMissedBackCS 	= true
		eBOTH_dialogue 	= BOTH_WARN3
	else
		eBOTH_dialogue 	= BOTH_WARN2
		SET_CLOCK_TIME(13,45,0)
	endif
	
	btimeReset = true
	
	give_all_peds_earpiece()
	ADD_PED_FOR_DIALOGUE(convo_struct,0,trev(),"TREVOR",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,1,mike(),"MICHAEL",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,2,frank(),"FRANKLIN",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,3,lester(),"LESTER",true)	
		
	heli_switch 	= TO_HELI
	eTREV_Dialogue 	= T_di_THINKING
	eMike_dialogue 	= M_di_TO_BACK
	
	clear_area(GET_ENTITY_COORDS(player_ped_id()),50,true)
	Point_Gameplay_cam_at_coord(get_entity_heading(player_ped_id()))	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Stage 2: Back of bank")
	
ENDPROC
PROC SU_3_GET_IN_POSITION()

	SET_CLOCK_TIME(14,00,0)
	
	//heli
	vehs[mvf_helicopter].id = CREATE_VEHICLE(frogger2,<< 1758.3090, 3284.5278, 40.7 >>, 133.1852)
	SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_helicopter].id,false)
	SET_ENTITY_ALWAYS_PRERENDER(vehs[mvf_helicopter].id,true)
	SET_VEHICLE_LIVERY(vehs[mvf_helicopter].id,1)
	SET_ENTITY_COLLISION(vehs[mvf_helicopter].id,true)
	FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id,true)
	Unload_Asset_Model(sAssetData,frogger2)
	bHasTrevSwitchConvoPlayed = TRUE
	//michael
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_Michael_veh].id,CHAR_MICHAEL,vBACK_BANK,fHEADING_BACK)	
		wait(0)
	endwhile					
	//franklin
	while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_franklin].id,CHAR_FRANKLIN,vehs[mvf_Michael_veh].id,VS_FRONT_RIGHT)
		wait(0)
	endwhile		
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_trevor_veh].id,CHAR_TREVOR,<< 1768.0714, 3293.4478, 40.2011 >>,305.7980)		
		wait(0)							
	endwhile
	REQUEST_CLIP_SET("move_lester_CaneUp")
	while not HAS_CLIP_SET_LOADED("move_lester_CaneUp")
		wait(0)
	endwhile

	if g_replay.iReplayInt[2] = 0
		Set_Current_Player_Ped(SELECTOR_PED_MICHAEL,true)
		peds[mpf_michael].id = player_ped_id()		
		
		while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_trevor].id,CHAR_TREVOR,vehs[mvf_helicopter].id)
			wait(0)
		endwhile
		//lester
		while not CREATE_NPC_PED_INSIDE_VEHICLE(peds[mpf_lester].id,CHAR_LESTER,vehs[mvf_helicopter].id,VS_BACK_LEFT)
			wait(0)
		endwhile		
		SET_PED_PROP_INDEX(peds[mpf_lester].id, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
		
		if IsEntityAlive(vehs[mvf_Michael_veh].id)
		and IsEntityAlive(mike())
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP(vehs[mvf_Michael_veh].id)
			Else	
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
				SET_PED_INTO_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
			endif
		endif
		SET_PLAYER_CONTROL(player_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON)
		Hotswap_Menu(false,true,false)
		bHotSwap = false	
		
	elif g_replay.iReplayInt[2] = 1
		Set_Current_Player_Ped(SELECTOR_PED_TREVOR,true)
		peds[mpf_trevor].id = player_ped_id()
		if not IS_AUDIO_SCENE_ACTIVE("BS_1_FLY_TO_TRUCKS")
			START_AUDIO_SCENE("BS_1_FLY_TO_TRUCKS")
		endif
		//mike
		while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_michael].id,CHAR_MICHAEL,vehs[mvf_Michael_veh].id)
			wait(0)
		endwhile
		while not CREATE_NPC_PED_INSIDE_VEHICLE(peds[mpf_lester].id,CHAR_LESTER,vehs[mvf_helicopter].id,VS_BACK_LEFT)
			wait(0)
		endwhile		
		SET_PED_PROP_INDEX(peds[mpf_lester].id, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
		//trev
		if IsEntityAlive(vehs[mvf_helicopter].id)
		and IsEntityAlive(TREV())
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP(vehs[mvf_helicopter].id)
			Else
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
				SET_PED_INTO_VEHICLE(TREV(),vehs[mvf_helicopter].id)
			endif					
		endif	
				
		Hotswap_Menu(false,true,false)
		bHotSwap = true	
	endif
	
	if g_replay.iReplayInt[1] = 1		  
		if IS_REPLAY_IN_PROGRESS()
		and g_bShitskipAccepted
		and(  g_replayMissionStage = 3 or  g_replayMissionStage = 2)
			g_replay.iReplayInt[2] = 1	
			bHotSwap 		= false
		endif
		SET_CLOCK_TIME(14,15,0)	
		eTREV_Dialogue  = T_di_THINKING	
		bMissedBackCS 	= true
		eBOTH_dialogue 	= BOTH_WARN3
	else
		SET_CLOCK_TIME(14,00,0)
		eTREV_Dialogue  = T_di_BRAD
		eBOTH_dialogue 	= BOTH_WARN2
	endif
	
	btimeReset = true
	give_all_peds_earpiece()
	ADD_PED_FOR_DIALOGUE(convo_struct,0,trev(),"TREVOR",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,1,mike(),"MICHAEL",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,2,frank(),"FRANKLIN",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,3,lester(),"LESTER",true)
	
	heli_switch 	= TO_TRUCKS	
	eMike_dialogue  = M_di_TO_BACK
	eBOTH_dialogue  = BOTH_WARN3
	
	clear_area(GET_ENTITY_COORDS(player_ped_id()),50,true)	
	FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id,false)
	Point_Gameplay_cam_at_coord(get_entity_heading(player_ped_id()))
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3,"Stage 3: Get in Position")
	
ENDPROC
PROC SU_4_HELI_FOLLOW_TRUCK()

	Set_Current_Player_Ped(SELECTOR_PED_TREVOR,true)
	peds[mpf_trevor].id = player_ped_id()
		
	//create helicopter					
	vehs[mvf_helicopter].id = CREATE_VEHICLE(frogger2, <<1095.7894, -1421.1515,102.4156>>, 183.9259)	
	Unload_Asset_Model(sAssetData,frogger2)
	SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_helicopter].id,false)
	SET_ENTITY_ALWAYS_PRERENDER(vehs[mvf_helicopter].id,true)
	SET_VEHICLE_ENGINE_ON(vehs[mvf_helicopter].id,true,true)
	SET_HELI_BLADES_FULL_SPEED(vehs[mvf_helicopter].id)				
	SET_VEHICLE_LIVERY(vehs[mvf_helicopter].id,1)
	FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id, true)
	
	NEW_LOAD_SCENE_START_SPHERE(<<1079.2714, -1998.5048, 46.6193>>, 100)
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
			PRINTLN("@@@@@@@@ WHILE NOT IS_NEW_LOAD_SCENE_LOADED() @@@@@@@@@@@")
			WAIT(0)
		ENDWHILE
	ENDIF
	REQUEST_VEHICLE_RECORDING(001,"FH1UBER")
	while not HAS_VEHICLE_RECORDING_BEEN_LOADED(001,"FH1UBER")
		wait(0)
	endwhile
	//place trevor in the correct place ready for hot swap
	
	//create security truck and driver
	vehs[mvf_security_truck].id = CREATE_VEHICLE(STOCKADE,<< 1025.5339, -1756.0054, 35.2748 >>, 81.9993)					
	peds[mpf_securityDriver].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_security_truck].id,PEDTYPE_MISSION,S_M_M_ARMOURED_01)
	SET_PED_COMPONENT_VARIATION(peds[mpf_securityDriver].id, PED_COMP_HEAD, 0, 0)
	//place lester in helicopter 					
	while not CREATE_NPC_PED_INSIDE_VEHICLE(peds[mpf_lester].id,CHAR_LESTER,vehs[mvf_helicopter].id,VS_BACK_LEFT)
		wait(0)
	endwhile
	SET_PED_PROP_INDEX(peds[mpf_lester].id, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
	//michael
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_Michael_veh].id,CHAR_MICHAEL,vBACK_BANK,fHEADING_BACK)	
		wait(0)
	endwhile	
	while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_michael].id,CHAR_MICHAEL,vehs[mvf_Michael_veh].id)
		wait(0)
	endwhile
	//franklin
	while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_franklin].id,CHAR_FRANKLIN,vehs[mvf_Michael_veh].id,VS_FRONT_RIGHT)
		wait(0)
	endwhile
	REQUEST_CLIP_SET("move_lester_CaneUp")
	while not HAS_CLIP_SET_LOADED("move_lester_CaneUp")
		wait(0)
	endwhile
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP(vehs[mvf_helicopter].id)
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
		SET_PED_INTO_VEHICLE(TREV(),vehs[mvf_helicopter].id)
	endif
	FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id,false)
	uber_state = UBER_INIT
	bTruckSkipForward = true
	give_all_peds_earpiece()
	print_now("FH1_TRUCK",DEFAULT_GOD_TEXT_TIME,1)
	ADD_PED_FOR_DIALOGUE(convo_struct,0,trev(),"TREVOR",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,1,mike(),"MICHAEL",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,2,frank(),"FRANKLIN",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,3,lester(),"LESTER",true)	
	clear_area(<< 1041.8492, -1773.0105,110.6172 >>,300,true)			
ENDPROC
PROC SU_5_CONSTRUCTION_HOLE()
	Set_Current_Player_Ped(SELECTOR_PED_TREVOR,true)
	peds[mpf_trevor].id = player_ped_id()					
		
	//create helicopter
	vehs[mvf_helicopter].id = CREATE_VEHICLE(frogger2,<<-76.4238, -528.6735, 87.5 >>,176.7705)
	Unload_Asset_Model(sAssetData,frogger2)
	SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_helicopter].id,false)
	SET_ENTITY_ALWAYS_PRERENDER(vehs[mvf_helicopter].id,true)
	SET_VEHICLE_ENGINE_ON(vehs[mvf_helicopter].id,true,true)
	SET_HELI_BLADES_FULL_SPEED(vehs[mvf_helicopter].id)
	SET_VEHICLE_LIVERY(vehs[mvf_helicopter].id,1)
	FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id, true)
	
	
	interior_constuction = GET_INTERIOR_AT_COORDS(<< 12.8829, -634.9265, 15.0884>>)			
	IF IS_VALID_INTERIOR(interior_constuction)
		PIN_INTERIOR_IN_MEMORY(interior_constuction)
	endif
	while not IS_INTERIOR_READY(interior_constuction)
		wait(0)
	endwhile 
	//place lester in helicopter 
	while not CREATE_NPC_PED_INSIDE_VEHICLE(peds[mpf_lester].id,CHAR_LESTER,vehs[mvf_helicopter].id,VS_BACK_LEFT)
		wait(0)
	endwhile
	SET_PED_PROP_INDEX(peds[mpf_lester].id, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
	//michael
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_Michael_veh].id,CHAR_MICHAEL,vBACK_BANK,fHEADING_BACK)		
		wait(0)
	endwhile					
	while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_michael].id,CHAR_MICHAEL,vehs[mvf_Michael_veh].id)
		wait(0)
	endwhile
	//franklin
	while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_franklin].id,CHAR_FRANKLIN,vehs[mvf_Michael_veh].id,VS_FRONT_RIGHT)
		wait(0)
	endwhile
	REQUEST_CLIP_SET("move_lester_CaneUp")
	while not HAS_CLIP_SET_LOADED("move_lester_CaneUp")
		wait(0)
	endwhile
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP(vehs[mvf_helicopter].id)
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
		SET_PED_INTO_VEHICLE(TREV(),vehs[mvf_helicopter].id)
	endif
	
	FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id,false)
	
	give_all_peds_earpiece()
	ADD_PED_FOR_DIALOGUE(convo_struct,0,trev(),"TREVOR",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,1,mike(),"MICHAEL",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,2,frank(),"FRANKLIN",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,3,lester(),"LESTER",true)
					
	iSFstage 			= 0							
								
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	CLEAR_AREA(<< -77.3855, -591.1019, 94.5943 >>,300,true)
						
ENDPROC
PROC SU_6_DROP_OFF_POINT()
	Set_Current_Player_Ped(SELECTOR_PED_TREVOR,true)
	peds[mpf_trevor].id = player_ped_id()
		
	interior_constuction = GET_INTERIOR_AT_COORDS(<< 12.8829, -634.9265, 15.0884  >>)			
	IF IS_VALID_INTERIOR(interior_constuction)
		PIN_INTERIOR_IN_MEMORY(interior_constuction)
	endif
	
	//create helicopter
	vehs[mvf_helicopter].id = CREATE_VEHICLE(frogger2,<< 7.0467, -614.5734, 61.0681 >>,187.9638)
	Unload_Asset_Model(sAssetData,frogger2)
	SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_helicopter].id,false)
	SET_ENTITY_ALWAYS_PRERENDER(vehs[mvf_helicopter].id,true)
	SET_VEHICLE_ENGINE_ON(vehs[mvf_helicopter].id,true,true)
	SET_HELI_BLADES_FULL_SPEED(vehs[mvf_helicopter].id)		
	SET_VEHICLE_LIVERY(vehs[mvf_helicopter].id,1)             
	FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id,true)
	
	//place lester in helicopter 
	while not CREATE_NPC_PED_INSIDE_VEHICLE(peds[mpf_lester].id,CHAR_LESTER,vehs[mvf_helicopter].id,VS_BACK_LEFT)
		wait(0)
	endwhile				
	SET_PED_PROP_INDEX(peds[mpf_lester].id, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
	//michael
	while not CREATE_PLAYER_VEHICLE(vehs[mvf_Michael_veh].id,CHAR_MICHAEL,vBACK_BANK,fHEADING_BACK)	
		wait(0)
	endwhile				
	while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_michael].id,CHAR_MICHAEL,vehs[mvf_Michael_veh].id)
		wait(0)
	endwhile
	//franklin
	while not CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_franklin].id,CHAR_FRANKLIN,vehs[mvf_Michael_veh].id,VS_FRONT_RIGHT)
		wait(0)
	endwhile
	REQUEST_CLIP_SET("move_lester_CaneUp")
	while not HAS_CLIP_SET_LOADED("move_lester_CaneUp")
		wait(0)
	endwhile
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP(vehs[mvf_helicopter].id)
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
		IF IS_VEHICLE_DRIVEABLE(vehs[mvf_helicopter].id)
			SET_PED_INTO_VEHICLE(TREV(),vehs[mvf_helicopter].id)
		ENDIF
	endif
	
	eTREV_Dialogue 	= T_di_END
	eMike_dialogue 	= M_di_END	
	give_all_peds_earpiece()
	ADD_PED_FOR_DIALOGUE(convo_struct,0,trev(),"TREVOR",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,1,mike(),"MICHAEL",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,2,frank(),"FRANKLIN",true)
	ADD_PED_FOR_DIALOGUE(convo_struct,3,lester(),"LESTER",true)
	
	FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id,false)
	CLEAR_AREA(<< 59.2427, -572.2118, 72.9076 >>,300,true)		
	Point_Gameplay_cam_at_coord(get_entity_heading(player_ped_id()))
	
ENDPROC
PROC SU_PASSED()
	if DOES_ENTITY_EXIST(player_ped_id())
		if g_replay.iReplayInt[2] = 1
			while not CREATE_PLAYER_VEHICLE(vehs[mvf_trevor_veh].id,CHAR_TREVOR,<<1759.5400, 3290.2327, 40.1387>>, 259.1121)
				wait(0)
			endwhile
			
			vehs[mvf_helicopter].id = CREATE_VEHICLE(frogger2,<<1744.3944, 3269.2964, 40.2207>>, 359.9513)
			Unload_Asset_Model(sAssetData,frogger2)
			SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_helicopter].id,false)
			SET_ENTITY_ALWAYS_PRERENDER(vehs[mvf_helicopter].id,true)
			SET_VEHICLE_LIVERY(vehs[mvf_helicopter].id,1)
			
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP(vehs[mvf_helicopter].id)
			else
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
				SET_PED_INTO_VEHICLE(trev(),vehs[mvf_helicopter].id)
			ENDIF			
		else
			Set_Current_Player_Ped(SELECTOR_PED_FRANKLIN,true)
			peds[mpf_franklin].id = player_ped_id()		
			IF IS_REPLAY_BEING_SET_UP()
				END_REPLAY_SETUP()
			else
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
			endif
		endif
	endif
	mission_passed()
endPROC
PROC MISSION_STAGE_SKIP()
//a skip has been made
	IF bDoSkip = TRUE
		
		//begin the skip if the switching is idle
		if stageSwitch = STAGESWITCH_IDLE
			if not IS_SCREEN_FADED_OUT()
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(1000)
				ENDIF
			else
				Mission_Set_Stage(INT_TO_ENUM(MSF_MISSION_STAGE_FLAGS, iSkipToStage))
			endif
			
		//Needs to be carried out before states own entering stage
		ELIF stageSwitch = STAGESWITCH_ENTERING
			
			RENDER_SCRIPT_CAMS(false,false)
			SET_PLAYER_CONTROL(player_id(), true)
			
			RESET_EVERYTHING()
			
			Start_Skip_Streaming(sAssetData)
			DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY)
			CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY)

			
			// -------------------------- Not a Replay ----------------------------
			IF NOT IS_REPLAY_BEING_SET_UP()					
				VECTOR vWarpCoord
				FLOAT fWarpHeading
				GET_SKIP_STAGE_COORD_AND_HEADING(int_to_enum(msf_mission_stage_flags, iSkipToStage), vWarpCoord, fWarpHeading,g_replay.iReplayInt[2])				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpCoord)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fWarpHeading)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				Load_Asset_NewLoadScene_Sphere(sAssetData, vWarpCoord, 25.0)
			ENDIF
			// --------------------------------------------------------------------
		
			Load_asset_Stage(int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage),true)	
		
			PRINTLN("ENTERING LOOP FOR SKIP STREAMING...")
			WHILE NOT Update_Skip_Streaming(sAssetData)
				WAIT(0)				
			ENDWHILE
			PRINTLN("...EXITED LOOP FOR SKIP STREAMING")
			
			//set up stage
			SWITCH int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage)			
				CASE msf_0_strip_club			SU_0_STRIP_CLUB() 			break				
				CASE msf_1_font_of_bank			SU_1_FONT_OF_BANK()			BREAK				
				CASE msf_2_back_of_bank			SU_2_BACK_OF_BANK()			BREAK				
				case msf_3_get_in_position		SU_3_GET_IN_POSITION()		break				
				CASE msf_4_heli_follow_truck	SU_4_HELI_FOLLOW_TRUCK()	BREAK				
				CASE msf_5_CONSTRUCTION_HOLE	SU_5_CONSTRUCTION_HOLE()	break				
				CASE msf_6_drop_off_point		SU_6_DROP_OFF_POINT()		break
				CASE msf_7_passed				SU_PASSED()					break
			ENDSWITCH
			
			SU_GENERAL()
			bDoSkip = FALSE
			IF NOT IS_REPLAY_BEING_SET_UP()	
				NEW_LOAD_SCENE_STOP()
			endif
		ENDIF
#if Is_debug_build
	//Check is a skip being asked for, dont allow skip during setup stage
	ELIF LAUNCH_MISSION_STAGE_MENU(zMenuNames, iSkipToStage, mission_stage, TRUE)
		b_zskipped = true
		g_replay.iReplayInt[0] = 0
		g_replay.iReplayInt[1] = 0
		if GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_MICHAEL
			g_replay.iReplayInt[2] = 0
		elif GET_PLAYER_PED_ENUM(player_ped_id()) = CHAR_TREVOR			
			g_replay.iReplayInt[2] = 1
		endif
		IF iSkipToStage > ENUM_TO_INT(msf_6_drop_off_point)
			MISSION_PASSED()
		ELSE
			iSkipToStage = CLAMP_INT(iSkipToStage, 0, ENUM_TO_INT(MSF_NUM_OF_STAGES)-1)
			if IS_SCREEN_FADED_IN()
				DO_SCREEN_FADE_OUT(1000)
				bDoSkip = true
			endif
		endif
#endif
	ENDIF
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		MISSION CHECKS
// -----------------------------------------------------------------------------------------------------------
BOOL bShowHelpOnce = FALSE
func bool Front_bank_CS()

	IF iFrontCS_stage > 0
		IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Michael_veh].id)
			IF NOT IS_PED_IN_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
			OR GET_IS_TASK_ACTIVE(MIKE(),CODE_TASK_EXIT_VEHICLE)
				CLEAR_PED_SECONDARY_TASK(MIKE())
				IF IS_ENTITY_PLAYING_ANIM(MIKE(),"missbigscore1_mcs1","michael_bss_1_mcs_1")
					STOP_ANIM_TASK(MIKE(),"missbigscore1_mcs1","michael_bss_1_mcs_1")
				ENDIF
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(FRANK(),vehs[mvf_Michael_veh].id)
			OR GET_IS_TASK_ACTIVE(FRANK(),CODE_TASK_EXIT_VEHICLE)
				CLEAR_PED_SECONDARY_TASK(FRANK())
				IF IS_ENTITY_PLAYING_ANIM(FRANK(),"missbigscore1_mcs1","franklin_bss_1_mcs_1")
					STOP_ANIM_TASK(FRANK(),"missbigscore1_mcs1","franklin_bss_1_mcs_1")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	switch iFrontCS_stage
		case 0			
			if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_MCS1",CONV_PRIORITY_VERY_HIGH)
				IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Michael_veh].id)
					IF IS_PED_IN_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
						TASK_PLAY_ANIM(MIKE(),"missbigscore1_mcs1","michael_bss_1_mcs_1",NORMAL_BLEND_IN,WALK_BLEND_OUT,-1,AF_SECONDARY)
						TASK_PLAY_ANIM(FRANK(),"missbigscore1_mcs1","franklin_bss_1_mcs_1",NORMAL_BLEND_IN,WALK_BLEND_OUT,-1,AF_SECONDARY)
						
						IF GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_THIRD_PERSON_NEAR
						AND GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_THIRD_PERSON_FAR
						AND GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_THIRD_PERSON_MEDIUM
						AND GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
							eSavedMode = GET_FOLLOW_VEHICLE_CAM_VIEW_MODE()
							SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON_MEDIUM)
						ELSE
							eSavedMode = NUM_CAM_VIEW_MODES
						ENDIF
						
						REPLAY_RECORD_BACK_FOR_TIME(0.0, 12.0, REPLAY_IMPORTANCE_HIGHEST)
						
						cameraIndex = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<0.2,-733.7,45.3>>,<<10.6,0,-14.2>>,40)fshakeAmp = 0.35
						SHAKE_CAM(cameraIndex,"HAND_SHAKE",fshakeAmp)
						SET_CAM_ACTIVE(cameraIndex,true)
						ihintstage = 0
						SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
						bHint_camOn = false	
						eMike_dialogue = M_di_NONE
						eDialogue_stateM = DI_START_CONDITION_MIKE
						bShowHelpOnce = FALSE
						iFrontCS_stage++
					ENDIF
				ENDIF
			endif
		break
		case 1	
			IF NOT IS_PED_IN_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
				CLEAR_PED_SECONDARY_TASK(MIKE())
				CLEAR_PED_SECONDARY_TASK(FRANK())
			ENDIF
			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
				CLEAR_PED_SECONDARY_TASK(MIKE())
				CLEAR_PED_SECONDARY_TASK(FRANK())
				RENDER_SCRIPT_CAMS(false,false)				
				CLEAR_HELP(false)
				iFrontCS_stage++	
			else
			
			 	if IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_Michael_veh].id,<<-2.55928, -743.97137, 45>>,<<-14.34047, -739.44629, 43.15934>>,4.46)	
				and IS_PED_IN_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
				and not GET_IS_TASK_ACTIVE(MIKE(),CODE_TASK_EXIT_VEHICLE)
					
					if not bHint_camOn
						if IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_Michael_veh].id,<<-2.55928, -743.97137, 45>>,<<-14.34047, -739.44629, 43.15934>>,4.46)
							IF bShowHelpOnce = FALSE
								PRINT_HELP("FH1_FBHELP")
								bShowHelpOnce = TRUE
							ENDIF
						ENDIF
					endif
					
					if (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) != CAM_VIEW_MODE_FIRST_PERSON 
					AND SHOULD_CONTROL_CHASE_HINT_CAM(sHintCamStruct, false))
					OR (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON 
					AND NOT bHint_camOn 
					AND IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
					AND GET_GAME_TIMER() > iFrontBankDelay)
						clear_Area(<<-3.72364, -751.49194, 44.46628>>,5.5,true)
						if not bHint_camOn 
							if IS_AUDIO_SCENE_ACTIVE("BS_1_BANK_CHECK_OUT")						
								STOP_AUDIO_SCENE("BS_1_BANK_CHECK_OUT")						
							endif
							if not IS_AUDIO_SCENE_ACTIVE("BS_1_BANK_HINT_CAM")						
								START_AUDIO_SCENE("BS_1_BANK_HINT_CAM")						
							endif				
							
							//Add delay for turning on / off camera
							iFrontBankDelay = GET_GAME_TIMER() + 500
							
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)

							PRINTLN("FRONT BANK CAM ON!")
							
//							SET_PLAYER_CONTROL(player_id(),false,SPC_DONT_STOP_OTHER_CARS_AROUND_PLAYER)
							RENDER_SCRIPT_CAMS(true,false)
							DISPLAY_RADAR(false)
							display_hud(false)						
							if IS_HELP_MESSAGE_BEING_DISPLAYED()
							and not IS_HELP_MESSAGE_FADING_OUT()
								CLEAR_HELP(false)
							endif	
							bHint_camOn = true
						endif
					elif (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) != CAM_VIEW_MODE_FIRST_PERSON 
					AND not SHOULD_CONTROL_CHASE_HINT_CAM(sHintCamStruct, false))
					OR ( GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON 
					AND bHint_camOn 
					AND IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_VEH_CIN_CAM) 
					AND GET_GAME_TIMER() > iFrontBankDelay)
						if bHint_camOn							
							if not IS_AUDIO_SCENE_ACTIVE("BS_1_BANK_CHECK_OUT")						
								START_AUDIO_SCENE("BS_1_BANK_CHECK_OUT")						
							endif
							if IS_AUDIO_SCENE_ACTIVE("BS_1_BANK_HINT_CAM")						
								STOP_AUDIO_SCENE("BS_1_BANK_HINT_CAM")						
							endif
							
							PRINTLN("FRONT BANK CAM OFF!")
							
							//Add delay for turning on / off camera
							iFrontBankDelay = GET_GAME_TIMER() + 500
							
							RENDER_SCRIPT_CAMS(false,false)
							DISPLAY_RADAR(true)
							display_hud(true)
//							SET_PLAYER_CONTROL(player_id(),true)
							bHint_camOn = false
						endif
					endif
				else
					if IS_HELP_MESSAGE_BEING_DISPLAYED()
					and not IS_HELP_MESSAGE_FADING_OUT()
						CLEAR_HELP(false)
					endif
					RENDER_SCRIPT_CAMS(false,false)
					DISPLAY_RADAR(true)
					display_hud(true)
					SET_PLAYER_CONTROL(player_id(),true)
				endif
			endif
		break
		case 2	
			
		break		 
	endswitch		
	
	if iFrontCS_stage = 2	
	
		IF eSavedMode != NUM_CAM_VIEW_MODES
			IF NOT localChaseHintCamStruct.bHintInterpingBack
			AND NOT IS_GAMEPLAY_HINT_ACTIVE()
				SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(eSavedMode)
				eSavedMode = NUM_CAM_VIEW_MODES
			ENDIF
		ENDIF
		
		RENDER_SCRIPT_CAMS(false,false)
		DISPLAY_RADAR(true)
		display_hud(true)
		SET_CINEMATIC_BUTTON_ACTIVE(true)
		SET_PLAYER_CONTROL(player_id(),true)		
		Unload_Asset_Anim_Dict(sAssetData,"missbigscore1_mcs1")
		return true
	endif
	
	IF DOES_ENTITY_EXIST(peds[mpf_guardF].id)
		if not IS_PED_HEADTRACKING_PED(MIKE(),peds[mpf_guardF].id)
			TASK_LOOK_AT_ENTITY(MIKE(),peds[mpf_guardF].id,-1, SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_YAW_LIMIT)
		endif
		if not IS_PED_HEADTRACKING_PED(FRANK(),peds[mpf_guardF].id)
			TASK_LOOK_AT_ENTITY(FRANK(),peds[mpf_guardF].id,-1, SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_YAW_LIMIT)
		endif
	ENDIF
				
	
return false
endfunc
Proc AUDIO_Interrupt()	
	switch eInterruptstage
		case INTERRUPT_NONE
			
		break
		
		case INTERRUPT_STORE_CURRENT
			if IS_SCRIPTED_CONVERSATION_ONGOING()
				eSavedChar = GET_CURRENT_PLAYER_PED_ENUM()
				lbl_Interrupt = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
				lbl_root = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				CPRINTLN(DEBUG_MISSION,"***** eSavedChar:", eSavedChar, " *****INTERRUPT_STORE_CURRENT****************: ",lbl_Interrupt," ",lbl_root)
				if not IS_STRING_NULL_OR_EMPTY(lbl_Interrupt) 
				and not IS_STRING_NULL_OR_EMPTY(lbl_root)
				AND NOT ARE_STRINGS_EQUAL(lbl_Interrupt, "NULL")
				AND NOT ARE_STRINGS_EQUAL(lbl_root, "NULL")
					CPRINTLN(DEBUG_MISSION,"INTERRUPT_KILL_CURRENT")
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()//KILL_ANY_CONVERSATION()
					eInterruptstage = INTERRUPT_KILL_CURRENT
				endif
			else
				eInterruptstage = INTERRUPTOR_SAFE
			endif
		break
		
		case INTERRUPT_KILL_CURRENT
			if IS_SAFE_TO_DISPLAY_GODTEXT()
				CPRINTLN(DEBUG_MISSION,"****************INTERRUPT_KILL_CURRENT*******************: ",lbl_Interrupt," ",lbl_root)
				eInterruptstage = INTERRUPTOR_SAFE			
			endif
		break	
		
		case INTERRUPTOR_SAFE
			CPRINTLN(DEBUG_MISSION,"*******************INTERRUPTOR_SAFE********************: ",lbl_Interrupt," ",lbl_root)
			//used for safe to start interrupting convo
		break
		
		case INTERRUPT_RESTART_READY
			CPRINTLN(DEBUG_MISSION,"***** eSavedChar:", eSavedChar, " ******** GET_CURRENT_PLAYER_PED_ENUM: ", GET_CURRENT_PLAYER_PED_ENUM()," ***********INTERRUPT_COMING*********************: ",lbl_Interrupt," ",lbl_root)
			IF eSavedChar = GET_CURRENT_PLAYER_PED_ENUM()
			AND NOT IS_PLAYER_SWITCH_IN_PROGRESS()
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				if not IS_STRING_NULL_OR_EMPTY(lbl_Interrupt) 
				and not IS_STRING_NULL_OR_EMPTY(lbl_root)
				AND NOT ARE_STRINGS_EQUAL(lbl_Interrupt, "NULL")
				AND NOT ARE_STRINGS_EQUAL(lbl_root, "NULL")
					if IS_SAFE_TO_DISPLAY_GODTEXT()	
					and not IS_SCRIPTED_CONVERSATION_ONGOING()
						if get_Game_timer() - iInterrupt_restart_delay > INTERRUPT_RESTART_DELAY
							CPRINTLN(DEBUG_MISSION,"************************INTERRUPT_RESTART_GO*********************: ",lbl_Interrupt," ",lbl_root)
							if CREATE_CONVERSATION_FROM_SPECIFIC_LINE(convo_struct,"FH1AUD",lbl_root,lbl_Interrupt,CONV_PRIORITY_MEDIUM)
								lbl_Interrupt = ""
								lbl_root = ""					
								eInterruptstage = INTERRUPT_RESTART_PlAYING
							endif
						endif
					else
						iInterrupt_restart_delay = get_game_timer()
					endif	
				else
					lbl_Interrupt = ""
					lbl_root = ""					
					eInterruptstage = INTERRUPT_NONE
				endif
			ELSE
				if IS_STRING_NULL_OR_EMPTY(lbl_Interrupt)
				OR IS_STRING_NULL_OR_EMPTY(lbl_root)
				OR ARE_STRINGS_EQUAL(lbl_Interrupt, "NULL")
				OR ARE_STRINGS_EQUAL(lbl_root, "NULL")
					lbl_Interrupt = ""
					lbl_root = ""
					eInterruptstage = INTERRUPT_NONE
				endif
			ENDIF
		break
		case INTERRUPT_RESTART_PlAYING
			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				eInterruptstage = INTERRUPT_NONE
			endif
		break
	endswitch
endproc
proc AUDIO_manange_dialogue()
	if not sCamDetails.bRun
//╞═══════════════════════════════════════════════════════════ TREV ══════════════════════════════════════════════════════════════╡
		if PLAYER_PED_ID() = TREV()
			switch eTREV_Dialogue
			
		//╞═══════════════════════════════════════ FILL 1 ═════════════════════════════════════════════╡
				case T_di_FILL1
					switch eDialogue_stateT
						
						case DI_START_CONDITION_TREVOR
							if b_TREVSpeechSwitchStopped
								if not IS_MESSAGE_BEING_DISPLAYED()
								and eInterruptstage = INTERRUPT_NONE
									if CREATE_CONVERSATION_FROM_SPECIFIC_LINE(convo_struct,"FH1AUD",lbl_TREVSwitchroot,lbl_TREVSWITCHInterrupt,CONV_PRIORITY_MEDIUM)
										b_convo_paused = false
										lbl_TREVswitchInterrupt = ""
										lbl_TREVswitchroot = ""		
										b_TREVSpeechSwitchStopped = false
										eDialogue_stateT = DI_RUNNING_DIALOGUE_TREVOR										
									endif
								endif
							else
								if not IS_MESSAGE_BEING_DISPLAYED()
								and not Is_clock_time_less_than_or_equal(12,15)
									b_convo_paused = false
									iDialogueTimer = GET_GAME_TIMER()
									eDialogue_stateT = DI_TRIGGER_DIALOGUE_TREVOR								
								endif
							endif
						break
						
						case DI_TRIGGER_DIALOGUE_TREVOR						
							if IS_PED_IN_ANY_VEHICLE(trev())
								if IS_PED_IN_VEHICLE(lester(),GET_VEHICLE_PED_IS_IN(trev()))									
									if GET_GAME_TIMER() - iDialogueTimer > 3000									
									and IS_SAFE_TO_DISPLAY_GODTEXT()
									and eInterruptstage = INTERRUPT_NONE
										if CREATE_CONVERSATION(convo_struct, "FH1AUD", "FH1_TFILL1", CONV_PRIORITY_MEDIUM)												
											eDialogue_stateT = DI_RUNNING_DIALOGUE_TREVOR
										endif
									endif
								endif
							endif
						break
						
						case DI_RUNNING_DIALOGUE_TREVOR
							if eInterruptstage = INTERRUPT_NONE
								if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							
									if IS_PED_IN_ANY_VEHICLE(trev())
										if IS_PED_IN_VEHICLE(lester(),GET_VEHICLE_PED_IS_IN(trev()))
											if b_convo_paused
												PAUSE_FACE_TO_FACE_CONVERSATION(false)	
												b_convo_paused = false
											endif
										else
											if not b_convo_paused
												PAUSE_FACE_TO_FACE_CONVERSATION(true)
												b_convo_paused = true
											endif
										endif
									else
										if not b_convo_paused
											PAUSE_FACE_TO_FACE_CONVERSATION(true)
											b_convo_paused = true
										endif
									endif								
								else								
									eDialogue_stateT = DI_EXIT_DIALOGUE_TREVOR								
								endif
							endif
						break
						
						case DI_EXIT_DIALOGUE_TREVOR
							lbl_TREVswitchInterrupt = ""
							lbl_TREVswitchroot = ""		
							b_TREVSpeechSwitchStopped = false
							eTREV_Dialogue = T_di_FILL2
							eDialogue_stateT = DI_START_CONDITION_TREVOR
							iDialogueTimer = GET_GAME_TIMER()
						break
						
					endswitch
				break
		//╞═══════════════════════════════════════ FILL 2 ═════════════════════════════════════════════╡				
				case T_di_FILL2		
					switch eDialogue_stateT
					
						case DI_START_CONDITION_TREVOR
							PRINTLN("@@@@@@@@@@@@ DI_START_CONDITION @@@@@@@@@@@@")
							if b_TREVSpeechSwitchStopped
								if not IS_MESSAGE_BEING_DISPLAYED()
								and eInterruptstage = INTERRUPT_NONE
									if CREATE_CONVERSATION_FROM_SPECIFIC_LINE(convo_struct,"FH1AUD",lbl_TREVSwitchroot,lbl_TREVSWITCHInterrupt,CONV_PRIORITY_MEDIUM)
										b_convo_paused = false
										lbl_TREVswitchInterrupt = ""
										lbl_TREVswitchroot = ""		
										b_TREVSpeechSwitchStopped = false
										eDialogue_stateT = DI_RUNNING_DIALOGUE_TREVOR										
									endif
								endif
							else
								if not IS_MESSAGE_BEING_DISPLAYED()
								and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									b_convo_paused = false
									iDialogueTimer = GET_GAME_TIMER()
									eDialogue_stateT = DI_TRIGGER_DIALOGUE_TREVOR
								endif
							endif
						break
						
						case DI_TRIGGER_DIALOGUE_TREVOR
							PRINTLN("@@@@@@@@@@@@ DI_TRIGGER_DIALOGUE @@@@@@@@@@@@")
							if IS_PED_IN_ANY_VEHICLE(trev())
								if IS_PED_IN_VEHICLE(lester(),GET_VEHICLE_PED_IS_IN(trev()))
									if not IS_MESSAGE_BEING_DISPLAYED()
									and eInterruptstage = INTERRUPT_NONE
										if CREATE_CONVERSATION(convo_struct, "FH1AUD", "FH1_TALK4", CONV_PRIORITY_MEDIUM)
											eDialogue_stateT = DI_RUNNING_DIALOGUE_TREVOR
										endif
									endif
								ENDIF
							endif
						break
						case DI_RUNNING_DIALOGUE_TREVOR
							PRINTLN("@@@@@@@@@@@@ DI_RUNNING_DIALOGUE @@@@@@@@@@@@")
							if eInterruptstage = INTERRUPT_NONE
								if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							
									if IS_PED_IN_ANY_VEHICLE(trev())
										if IS_PED_IN_VEHICLE(lester(),GET_VEHICLE_PED_IS_IN(trev()))
											if b_convo_paused
												PAUSE_FACE_TO_FACE_CONVERSATION(false)	
												b_convo_paused = false
											endif
										else
											if not b_convo_paused
												PAUSE_FACE_TO_FACE_CONVERSATION(true)
												b_convo_paused = true
											endif
										endif
									else
										if not b_convo_paused
											PAUSE_FACE_TO_FACE_CONVERSATION(true)
											b_convo_paused = true
										endif
									endif								
								else
									eDialogue_stateT = DI_EXIT_DIALOGUE_TREVOR
								endif
							endif
						break
						
						case DI_EXIT_DIALOGUE_TREVOR
							PRINTLN("@@@@@@@@@@@@ DI_EXIT_DIALOGUE @@@@@@@@@@@@")
							lbl_TREVswitchInterrupt = ""
							lbl_TREVswitchroot = ""		
							b_TREVSpeechSwitchStopped = false
							eTREV_Dialogue = T_di_THINKING
							eDialogue_stateT = DI_START_CONDITION_TREVOR
							iDialogueTimer = GET_GAME_TIMER()							
						break						
					endswitch
				break
			//╞═══════════════════════════════════════ BRAD ═════════════════════════════════════════════╡			
				case T_di_BRAD
					switch eDialogue_stateT
						case DI_START_CONDITION_TREVOR
							if b_TREVSpeechSwitchStopped
								if not IS_MESSAGE_BEING_DISPLAYED()
								and eInterruptstage = INTERRUPT_NONE
									if CREATE_CONVERSATION_FROM_SPECIFIC_LINE(convo_struct,"FH1AUD",lbl_TREVSwitchroot,lbl_TREVSWITCHInterrupt,CONV_PRIORITY_MEDIUM)
										b_convo_paused = false
										lbl_TREVswitchInterrupt = ""
										lbl_TREVswitchroot = ""		
										b_TREVSpeechSwitchStopped = false
										eDialogue_stateT = DI_RUNNING_DIALOGUE_TREVOR									
									endif
								endif
							else
								if not IS_MESSAGE_BEING_DISPLAYED()
									if not Is_clock_time_less_than_or_equal(14,00)
									or g_replay.iReplayInt[1] = 1
										b_convo_paused = false
										iDialogueTimer = GET_GAME_TIMER()
										eDialogue_stateT = DI_TRIGGER_DIALOGUE_TREVOR
									endif
								endif
							endif
						break
						
						case DI_TRIGGER_DIALOGUE_TREVOR
								//TRIGGER BRAD STUFF
							if IS_PED_IN_ANY_VEHICLE(trev())
								if IS_PED_IN_VEHICLE(lester(),GET_VEHICLE_PED_IS_IN(trev()))	
									if GET_GAME_TIMER() - iDialogueTimer > 3000										
									and not IS_MESSAGE_BEING_DISPLAYED()
									and eInterruptstage = INTERRUPT_NONE
										if CREATE_CONVERSATION(convo_struct, "FH1AUD", "FH1_TALK4b", CONV_PRIORITY_MEDIUM)
											eDialogue_stateT = DI_RUNNING_DIALOGUE_TREVOR
										endif
									endif
								endif
							ENDIF
						break
						
						case DI_RUNNING_DIALOGUE_TREVOR
							if eInterruptstage = INTERRUPT_NONE
								if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							
									if IS_PED_IN_ANY_VEHICLE(trev())
										if IS_PED_IN_VEHICLE(lester(),GET_VEHICLE_PED_IS_IN(trev()))
											if b_convo_paused
												PAUSE_FACE_TO_FACE_CONVERSATION(false)	
												b_convo_paused = false
											endif
										else
											if not b_convo_paused
												PAUSE_FACE_TO_FACE_CONVERSATION(true)
												b_convo_paused = true
											endif
										endif
									else
										if not b_convo_paused
											PAUSE_FACE_TO_FACE_CONVERSATION(true)
											b_convo_paused = true
										endif
									endif								
								else
									eDialogue_stateT = DI_EXIT_DIALOGUE_TREVOR
								endif
							endif
						break
						
						case DI_EXIT_DIALOGUE_TREVOR
							lbl_TREVswitchInterrupt = ""
							lbl_TREVswitchroot = ""		
							b_TREVSpeechSwitchStopped = false
							eTREV_Dialogue = T_di_THINKING
							eDialogue_stateT = DI_START_CONDITION_TREVOR
							iDialogueTimer = GET_GAME_TIMER()		
						break
					endswitch
				break
			
			//╞═══════════════════════════════════════ WHAT YOU THINKING  ═════════════════════════════════════════════╡			
				case T_di_THINKING
					switch eDialogue_stateT
						case DI_START_CONDITION_TREVOR
							if b_TREVSpeechSwitchStopped
								if not IS_MESSAGE_BEING_DISPLAYED()
								and eInterruptstage = INTERRUPT_NONE
									if CREATE_CONVERSATION_FROM_SPECIFIC_LINE(convo_struct,"FH1AUD",lbl_TREVSwitchroot,lbl_TREVSWITCHInterrupt,CONV_PRIORITY_MEDIUM)
										b_convo_paused = false
										lbl_TREVswitchInterrupt = ""
										lbl_TREVswitchroot = ""		
										b_TREVSpeechSwitchStopped = false
										eDialogue_stateT = DI_RUNNING_DIALOGUE_TREVOR										
									endif
								endif
							else
								if not IS_MESSAGE_BEING_DISPLAYED()
									if g_replay.iReplayInt[1] = 1
										b_convo_paused = false
										iDialogueTimer = GET_GAME_TIMER()
										eDialogue_stateT = DI_TRIGGER_DIALOGUE_TREVOR
									endif
								endif
							endif
						break
						
						case DI_TRIGGER_DIALOGUE_TREVOR
							IF DOES_ENTITY_EXIST(vehs[mvf_helicopter].id)
								if IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
									if IS_PED_IN_VEHICLE(lester(),GET_VEHICLE_PED_IS_IN(trev()))
										if GET_GAME_TIMER() - iDialogueTimer > 3000
										and not IS_MESSAGE_BEING_DISPLAYED()
										and eInterruptstage = INTERRUPT_NONE
										AND b_displayed_get_heli
											if CREATE_CONVERSATION(convo_struct, "FH1AUD", "FH1_FILL3", CONV_PRIORITY_MEDIUM)
												eDialogue_stateT = DI_RUNNING_DIALOGUE_TREVOR
											endif
										endif
									endif
								ENDIF
							ENDIF
						break
						
						case DI_RUNNING_DIALOGUE_TREVOR
							if eInterruptstage = INTERRUPT_NONE
								if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							
									if IS_PED_IN_ANY_VEHICLE(trev())
										if IS_PED_IN_VEHICLE(lester(),GET_VEHICLE_PED_IS_IN(trev()))
											if b_convo_paused
												PAUSE_FACE_TO_FACE_CONVERSATION(false)	
												b_convo_paused = false
											endif
										else
											if not b_convo_paused
												PAUSE_FACE_TO_FACE_CONVERSATION(true)
												b_convo_paused = true
											endif
										endif
									else
										if not b_convo_paused
											PAUSE_FACE_TO_FACE_CONVERSATION(true)
											b_convo_paused = true
										endif
									endif								
								else
									eDialogue_stateT = DI_EXIT_DIALOGUE_TREVOR
								endif
							endif
						break
						
						case DI_EXIT_DIALOGUE_TREVOR
							lbl_TREVswitchInterrupt = ""
							lbl_TREVswitchroot = ""		
							b_TREVSpeechSwitchStopped = false
							eTREV_Dialogue = T_di_NONE
							eDialogue_stateT = DI_START_CONDITION_TREVOR
							iDialogueTimer = GET_GAME_TIMER()		
						break
					endswitch
				break
			
				//╞═══════════════════════════════════════ END ═════════════════════════════════════════════╡			
				case T_di_END
					switch eDialogue_stateT
						case DI_START_CONDITION_TREVOR
							if b_TREVSpeechSwitchStopped
								if not IS_MESSAGE_BEING_DISPLAYED()
								and eInterruptstage = INTERRUPT_NONE
									IF bShowObjective
										if CREATE_CONVERSATION_FROM_SPECIFIC_LINE(convo_struct,"FH1AUD",lbl_TREVSwitchroot,lbl_TREVSWITCHInterrupt,CONV_PRIORITY_MEDIUM)
											b_convo_paused = false
											lbl_TREVswitchInterrupt = ""
											lbl_TREVswitchroot = ""		
											b_TREVSpeechSwitchStopped = false
											eDialogue_stateT = DI_RUNNING_DIALOGUE_TREVOR									
										endif
									ENDIF
								endif
							else
								if mission_stage = enum_to_int(msf_6_drop_off_point) 
								and mission_substage >= 1
									if not IS_MESSAGE_BEING_DISPLAYED()
										b_convo_paused = false
										iDialogueTimer = GET_GAME_TIMER()
										eDialogue_stateT = DI_TRIGGER_DIALOGUE_TREVOR
									endif
								endif
							endif
						break
						
						case DI_TRIGGER_DIALOGUE_TREVOR							
							if IsEntityAlive(vehs[mvf_helicopter].id)
								if IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
								and IS_PED_IN_VEHICLE(lester(),vehs[mvf_helicopter].id)
									if not IS_MESSAGE_BEING_DISPLAYED()
									and get_game_timer() - iDialogueTimer > 5000
									and eInterruptstage = INTERRUPT_NONE
										if CREATE_CONVERSATION(convo_struct, "FH1AUD", "FH1_ENDT", CONV_PRIORITY_MEDIUM)								
											eDialogue_stateT = DI_RUNNING_DIALOGUE_TREVOR
										endif
									endif
								endif
							endif							
						break
						
						case DI_RUNNING_DIALOGUE_TREVOR
							if eInterruptstage = INTERRUPT_NONE
								if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									if IsEntityAlive(vehs[mvf_helicopter].id)
										if IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
										and IS_PED_IN_VEHICLE(lester(),vehs[mvf_helicopter].id)
											if b_convo_paused
												PAUSE_FACE_TO_FACE_CONVERSATION(false)	
												b_convo_paused = false
											endif
										else
											if not b_convo_paused
												PAUSE_FACE_TO_FACE_CONVERSATION(true)
												b_convo_paused = true
											endif
										endif
									endif
								else
									eDialogue_stateT = DI_EXIT_DIALOGUE_TREVOR
								endif
							endif
						break
						
						case DI_EXIT_DIALOGUE_TREVOR
							lbl_TREVswitchInterrupt = ""
							lbl_TREVswitchroot = ""		
							b_TREVSpeechSwitchStopped = false
							eTREV_Dialogue = T_di_NONE
							eDialogue_stateT = DI_START_CONDITION_TREVOR
							iDialogueTimer = GET_GAME_TIMER()							
						break
					endswitch
				break
				//╞═══════════════════════════════════════ NONE ═════════════════════════════════════════════╡			
				case T_di_NONE
					switch eDialogue_stateT
						case DI_START_CONDITION_TREVOR
							
						break
					endswitch
				break
			endswitch	
//╞═══════════════════════════════════════════════════════════ MIKE ══════════════════════════════════════════════════════════════╡		
		elif player_ped_id() = MIKE()
			switch eMike_dialogue						
			//╞═══════════════════════════════════════ FRONT ═════════════════════════════════════════════╡			
				case M_di_TO_FRONT
					switch eDialogue_stateM
						case DI_START_CONDITION_MIKE
							if b_mikeSpeechSwitchStopped
								if not IS_MESSAGE_BEING_DISPLAYED()
								and eInterruptstage = INTERRUPT_NONE
									if CREATE_CONVERSATION_FROM_SPECIFIC_LINE(convo_struct,"FH1AUD",lbl_mikeSwitchroot,lbl_mikeSWITCHInterrupt,CONV_PRIORITY_MEDIUM)
										b_convo_paused = false
										lbl_MikeswitchInterrupt = ""
										lbl_mikeswitchroot = ""		
										b_mikeSpeechSwitchStopped = false
										eDialogue_stateM = DI_RUNNING_DIALOGUE_MIKE										
									endif
								endif
							else
								if mission_stage = ENUM_TO_INT(msf_0_strip_club)
								and mission_substage = 7
								and not IS_MESSAGE_BEING_DISPLAYED()
									b_convo_paused = false
									iDialogueTimer = GET_GAME_TIMER()
									eDialogue_stateM = DI_TRIGGER_DIALOGUE_MIKE								
								endif
							endif
						break
						case DI_TRIGGER_DIALOGUE_MIKE
							if IS_PED_IN_VEHICLE(mike(),vehs[mvf_Michael_veh].id)
							and IS_PED_IN_VEHICLE(frank(),vehs[mvf_Michael_veh].id)	
								if GET_GAME_TIMER() - iDialogueTimer > 4000								
								and not IS_MESSAGE_BEING_DISPLAYED()
								and eInterruptstage = INTERRUPT_NONE
								and not DOES_CAM_EXIST(scsSwitchCamTrevorInHeliToMichael.ciSpline) //[MF] Make sure we're not in the switch cam that's supposed to transition to a cutscene
									if CREATE_CONVERSATION(convo_struct, "FH1AUD", "FH1_FILL4", CONV_PRIORITY_MEDIUM)
										eDialogue_stateM = DI_RUNNING_DIALOGUE_MIKE
									endif
								endif
							endif
						break
						case DI_RUNNING_DIALOGUE_MIKE
							if eInterruptstage = INTERRUPT_NONE
								if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									if IS_PED_IN_VEHICLE(mike(),vehs[mvf_Michael_veh].id)
									and IS_PED_IN_VEHICLE(frank(),vehs[mvf_Michael_veh].id)
										if b_convo_paused
											PAUSE_FACE_TO_FACE_CONVERSATION(false)	
											b_convo_paused = false
										endif
									else
										if not b_convo_paused
											PAUSE_FACE_TO_FACE_CONVERSATION(true)
											b_convo_paused = true
										endif
									endif
								else
									eDialogue_stateM = DI_EXIT_DIALOGUE_MIKE								
								endif
							endif	
						break
						case DI_EXIT_DIALOGUE_MIKE
							lbl_MikeswitchInterrupt = ""
							lbl_mikeswitchroot = ""		
							b_mikeSpeechSwitchStopped = false
							eMike_dialogue = M_di_TO_BACK
							eDialogue_stateM = DI_START_CONDITION_MIKE
							iDialogueTimer = GET_GAME_TIMER()
						break
					endswitch
				break
			//╞═══════════════════════════════════════ BACK ═════════════════════════════════════════════╡					
				case M_di_TO_BACK
					switch eDialogue_stateM
						case DI_START_CONDITION_MIKE						
							if b_mikeSpeechSwitchStopped
								if not IS_MESSAGE_BEING_DISPLAYED()
								and eInterruptstage = INTERRUPT_NONE
									if CREATE_CONVERSATION_FROM_SPECIFIC_LINE(convo_struct,"FH1AUD",lbl_mikeSwitchroot,lbl_mikeSWITCHInterrupt,CONV_PRIORITY_MEDIUM)
										b_convo_paused = false
										lbl_MikeswitchInterrupt = ""
										lbl_mikeswitchroot = ""		
										b_mikeSpeechSwitchStopped = false
										eDialogue_stateM = DI_RUNNING_DIALOGUE_MIKE										
									endif
								endif
							else
								if mission_stage = enum_to_int(msf_1_font_of_bank)
								and mission_substage = 4
								and not IS_MESSAGE_BEING_DISPLAYED()
									b_convo_paused = false
									iDialogueTimer = GET_GAME_TIMER()
									eDialogue_stateM = DI_TRIGGER_DIALOGUE_MIKE									
								endif
							endif
						break
						case DI_TRIGGER_DIALOGUE_MIKE
							if IS_PED_IN_VEHICLE(mike(),vehs[mvf_Michael_veh].id)
							and IS_PED_IN_VEHICLE(frank(),vehs[mvf_Michael_veh].id)	
								if GET_GAME_TIMER() - iDialogueTimer > 1000							
								and not IS_MESSAGE_BEING_DISPLAYED()
								and eInterruptstage = INTERRUPT_NONE
								and not DOES_CAM_EXIST(scsSwitchCamTrevorInHeliToMichael.ciSpline)
									if CREATE_CONVERSATION(convo_struct, "FH1AUD", "FH1_FILL5", CONV_PRIORITY_MEDIUM)								
										eDialogue_stateM = DI_RUNNING_DIALOGUE_MIKE
									endif
								endif
							endif
						break
						case DI_RUNNING_DIALOGUE_MIKE
							if eInterruptstage = INTERRUPT_NONE
								if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									if IS_PED_IN_VEHICLE(mike(),vehs[mvf_Michael_veh].id)
									and IS_PED_IN_VEHICLE(frank(),vehs[mvf_Michael_veh].id)
										if b_convo_paused
											PAUSE_FACE_TO_FACE_CONVERSATION(false)	
											b_convo_paused = false
										endif
									else
										if not b_convo_paused
											PAUSE_FACE_TO_FACE_CONVERSATION(true)
											b_convo_paused = true
										endif
									endif
								else
									eDialogue_stateM = DI_EXIT_DIALOGUE_MIKE
								endif
							endif
						break
						case DI_EXIT_DIALOGUE_MIKE
							lbl_MikeswitchInterrupt = ""
							lbl_mikeswitchroot = ""		
							b_mikeSpeechSwitchStopped = false
							eMike_dialogue 	= M_di_NONE
							eDialogue_stateM = DI_START_CONDITION_MIKE
							iDialogueTimer = GET_GAME_TIMER()
						break
					endswitch
				break
				//╞═══════════════════════════════════════ END ═════════════════════════════════════════════╡			
				case M_di_END
					switch eDialogue_stateM
						case DI_START_CONDITION_MIKE
							if b_mikeSpeechSwitchStopped
								if not IS_MESSAGE_BEING_DISPLAYED()
								and eInterruptstage = INTERRUPT_NONE
									if CREATE_CONVERSATION_FROM_SPECIFIC_LINE(convo_struct,"FH1AUD",lbl_mikeSwitchroot,lbl_mikeSWITCHInterrupt,CONV_PRIORITY_MEDIUM)
										b_convo_paused = false
										lbl_MikeswitchInterrupt = ""
										lbl_mikeswitchroot = ""		
										b_mikeSpeechSwitchStopped = false
										eDialogue_stateM = DI_RUNNING_DIALOGUE_MIKE										
									endif
								endif
							else
								if mission_stage = enum_to_int(msf_6_drop_off_point) 
								and mission_substage >= 1
									if not IS_MESSAGE_BEING_DISPLAYED()
										b_convo_paused = false
										iDialogueTimer = GET_GAME_TIMER()
										eDialogue_stateM = DI_TRIGGER_DIALOGUE_MIKE
									endif
								endif
							endif
						break
						
						case DI_TRIGGER_DIALOGUE_MIKE
							if IsEntityAlive(vehs[mvf_Michael_veh].id)
								if IS_PED_IN_VEHICLE(mike(),vehs[mvf_Michael_veh].id)
								and IS_PED_IN_VEHICLE(frank(),vehs[mvf_Michael_veh].id)	
									if not IS_MESSAGE_BEING_DISPLAYED()
									and eInterruptstage = INTERRUPT_NONE
										if CREATE_CONVERSATION(convo_struct, "FH1AUD", "FH1_ENDM", CONV_PRIORITY_MEDIUM)								
											eDialogue_stateM = DI_RUNNING_DIALOGUE_MIKE
										endif
									endif
								endif
							endif	
						break
						
						case DI_RUNNING_DIALOGUE_MIKE
							if eInterruptstage = INTERRUPT_NONE
								if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									if IsEntityAlive(vehs[mvf_Michael_veh].id)
										if IS_PED_IN_VEHICLE(mike(),vehs[mvf_Michael_veh].id)
										and IS_PED_IN_VEHICLE(frank(),vehs[mvf_Michael_veh].id)
											if b_convo_paused
												PAUSE_FACE_TO_FACE_CONVERSATION(false)	
												b_convo_paused = false
											endif
										else
											if not b_convo_paused
												PAUSE_FACE_TO_FACE_CONVERSATION(true)
												b_convo_paused = true
											endif
										endif
									endif
								else
									eDialogue_stateM = DI_EXIT_DIALOGUE_MIKE
								endif
							endif
						break
						
						case DI_EXIT_DIALOGUE_MIKE
							lbl_MikeswitchInterrupt = ""
							lbl_mikeswitchroot = ""		
							b_mikeSpeechSwitchStopped = false
							eMike_dialogue 	= M_di_NONE
							eDialogue_stateM = DI_START_CONDITION_MIKE
							iDialogueTimer 	= GET_GAME_TIMER()
							bShowObjective 	= true
						break
					endswitch
				break
				//╞═══════════════════════════════════════ NONE ═════════════════════════════════════════════╡			
				case M_di_NONE
					switch eDialogue_stateM
						case DI_START_CONDITION_MIKE
							
						break
					endswitch
				break
			endswitch		
		endif
//╞═══════════════════════════════════════════════════════════ BOTH ══════════════════════════════════════════════════════════════╡		
		if mission_stage < enum_to_int(msf_4_heli_follow_truck)
			switch eBOTH_dialogue
				case BOTH_TIME_INFO
					if mission_stage = ENUM_TO_INT(msf_0_strip_club)
					and mission_substage = 7
						if eInterruptstage = INTERRUPT_NONE
							eInterruptstage = INTERRUPT_STORE_CURRENT
						endif
						if eInterruptstage = INTERRUPTOR_SAFE		
						and not IS_MESSAGE_BEING_DISPLAYED()
							if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_TIMER",CONV_PRIORITY_MEDIUM)
								Hotswap_Menu(false,true,false)
								bHotSwap = true	
								eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 1")
								eBOTH_dialogue = BOTH_WARN1
							endif
						endif
					endif
				break
				case BOTH_WARN1					
					eBOTH_dialogue = BOTH_WARN2					
				break
				case BOTH_WARN2		
					IF NOT IS_PED_INJURED(TREV())
						if( not Is_clock_time_less_than_or_equal(13,30)	and Is_clock_time_less_than_or_equal(13,45)	)
							if eInterruptstage = INTERRUPT_NONE
								eInterruptstage = INTERRUPT_STORE_CURRENT
							endif
							if eInterruptstage = INTERRUPTOR_SAFE
							and not IS_MESSAGE_BEING_DISPLAYED()
							 	if player_ped_id() = trev()
									IF DOES_ENTITY_EXIST(vehs[mvf_helicopter].id)
										IF IS_VEHICLE_DRIVEABLE(vehs[mvf_helicopter].id)
											IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehs[mvf_helicopter].id)
												if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_UPDATE_P",CONV_PRIORITY_MEDIUM)
													eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 2")	
													eBOTH_dialogue = BOTH_WARN3
												endif
											ELSE
												if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_UPDATE",CONV_PRIORITY_MEDIUM)
													eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 3")	
													eBOTH_dialogue = BOTH_WARN3
												endif
											ENDIF
										ENDIF
									ELSE
										if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_UPDATE",CONV_PRIORITY_MEDIUM)
											eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 4")	
											eBOTH_dialogue = BOTH_WARN3
										endif
									ENDIF
								else
									if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_UPDATE_P",CONV_PRIORITY_MEDIUM)
										eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 5")	
										eBOTH_dialogue = BOTH_WARN3
									endif
								endif
							endif
						else
							
						endif
					ENDIF
				break
				case BOTH_WARN3					
					eBOTH_dialogue = BOTH_WARN4
				break
				case BOTH_WARN4
					if not Is_clock_time_less_than_or_equal(15,00)
						if eInterruptstage = INTERRUPT_NONE
							eInterruptstage = INTERRUPT_STORE_CURRENT
						endif
						if eInterruptstage = INTERRUPTOR_SAFE
						and not IS_MESSAGE_BEING_DISPLAYED()
							if player_ped_id() = trev()
								IF IS_VEHICLE_DRIVEABLE(vehs[mvf_helicopter].id)
									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehs[mvf_helicopter].id)
										if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_300_P",CONV_PRIORITY_MEDIUM)
											eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 6")
											eBOTH_dialogue = BOTH_WARN5
										endif
									ELSE
										if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_300",CONV_PRIORITY_MEDIUM)
											eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 7")
											eBOTH_dialogue = BOTH_WARN5
										endif
									ENDIF
								ELSE
									if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_300",CONV_PRIORITY_MEDIUM)
										eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 8")
										eBOTH_dialogue = BOTH_WARN5
									endif
								ENDIF
							else 
								if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_300_P",CONV_PRIORITY_MEDIUM)
									eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 9")
									eBOTH_dialogue = BOTH_WARN5
								endif
							endif
						endif
					endif
				break
			endswitch
		endif
	endif	
	AUDIO_Interrupt()
	
endproc

PROC update_lester_anims(vector vTarget)
	vector voffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(lester(),vTarget)

	switch eLESTER_side
		CASE LEST_SLIDE 
			if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(lester(),script_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
			and not IS_ENTITY_PLAYING_ANIM(lester(),"missheist_the_big_score_setup_1@camera@idle_a","idle_a")
			and not IS_ENTITY_PLAYING_ANIM(lester(),"missheist_the_big_score_setup_1@camera@idle_a","idle_c")
				if GET_PED_VEHICLE_SEAT(lester(),vehs[mvf_helicopter].id) = VS_BACK_LEFT
					TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(lester(),vehs[mvf_helicopter].id)
					eLESTER_side = LESTER_RIGHT
					
				elif GET_PED_VEHICLE_SEAT(lester(),vehs[mvf_helicopter].id) = VS_BACK_RIGHT
					TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(lester(),vehs[mvf_helicopter].id)
					eLESTER_side = LESTER_lEFT

				endif
			endif
		BREAK
		case LESTER_LEFT
			IF GET_PED_VEHICLE_SEAT(lester(),vehs[mvf_helicopter].id) = VS_BACK_LEFT
			and not IS_SCRIPT_TASK_RUNNING_OR_STARTING(lester(),script_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
				if not IS_ENTITY_PLAYING_ANIM(lester(),"missheist_the_big_score_setup_1@camera@idle_a","idle_a")
					TASK_PLAY_ANIM(lester(),"missheist_the_big_score_setup_1@camera@idle_a","idle_a",WALK_BLEND_IN,WALK_BLEND_OUT,-1,AF_LOOPING | AF_SECONDARY| AF_NOT_INTERRUPTABLE)
				endif
				
				if voffset.x >= 1.2
					CLEAR_PED_TASKS(lester())
					eLESTER_side = LEST_SLIDE
				endif
			ENDIF
		break
		case LESTER_RIGHT
			IF GET_PED_VEHICLE_SEAT(lester(),vehs[mvf_helicopter].id) = VS_BACK_RIGHT
			and not IS_SCRIPT_TASK_RUNNING_OR_STARTING(lester(),script_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
				if not IS_ENTITY_PLAYING_ANIM(lester(),"missheist_the_big_score_setup_1@camera@idle_a","idle_c")
					TASK_PLAY_ANIM(lester(),"missheist_the_big_score_setup_1@camera@idle_a","idle_c",WALK_BLEND_IN,WALK_BLEND_OUT,-1,AF_LOOPING | AF_UPPERBODY| AF_SECONDARY| AF_NOT_INTERRUPTABLE)
				endif
				
				if voffset.x <= -1.2	
					CLEAR_PED_TASKS(lester())
					eLESTER_side = LEST_SLIDE
				endif
			ENDIF
		break
	endswitch
ENDPROC
PROC manage_lester_Cam()

if mission_stage = enum_to_int(msf_4_heli_follow_truck)
and mission_substage > 11
	if DOES_CAM_EXIST(cameraIndex)
		DESTROY_CAM(cameraIndex)
	endif
	if DOES_CAM_EXIST(camfollow)
		DESTROY_CAM(camfollow)
	endif
	if DOES_CAM_EXIST(Camtarget)
		DESTROY_CAM(Camtarget)
	endif
	IF NOT IS_ENTITY_DEAD(TREV())
		IF NOT IS_ENTITY_VISIBLE(TREV())
			SET_ENTITY_VISIBLE(TREV(),true)
		ENDIF
	ENDIF
	IF NOT IS_ENTITY_DEAD(LESTER())
		IF NOT IS_ENTITY_VISIBLE(lester())
			SET_ENTITY_VISIBLE(lester(),true)
		ENDIF
	ENDIF
	IF NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
		IF NOT IS_ENTITY_VISIBLE(vehs[mvf_helicopter].id)
			SET_ENTITY_VISIBLE(vehs[mvf_helicopter].id,true)
		ENDIF
	ENDIF
	RENDER_SCRIPT_CAMS(false,false)
	DISPLAY_RADAR(true)
	DISPLAY_HUD(true)
	
	STOP_SOUND(camZoom)
	STOP_SOUND(camhum)
	
elif mission_stage = enum_to_int(msf_4_heli_follow_truck)
and mission_substage > 3
and INIT_CAM()

	switch iLESTERCAM_stage	
		//create cams
		case 0
			SET_CINEMATIC_BUTTON_ACTIVE(false)		
			
			cameraIndex = CREATE_CAMERA(CAMTYPE_SCRIPTED)
			ATTACH_CAM_TO_ENTITY(cameraIndex,vehs[mvf_helicopter].id,<<0,0,0>>)
			POINT_CAM_AT_ENTITY(cameraIndex,vehs[mvf_security_truck].id,<<0,-12.5,0>>)	
			SET_CAM_FOV(cameraIndex,35)
			SHAKE_CAM(cameraIndex,"hand_shake",1)
			SET_CAM_ACTIVE(cameraIndex,true)
			
			camfollow = CREATE_CAMERA(CAMTYPE_SCRIPTED)
			ATTACH_CAM_TO_ENTITY(camfollow,vehs[mvf_helicopter].id,<<0,0,0>>)		
			SHAKE_CAM(camfollow,"hand_shake",1)	
			
			Camtarget = CREATE_CAMERA(CAMTYPE_SCRIPTED)
			ATTACH_CAM_TO_ENTITY(Camtarget,vehs[mvf_helicopter].id,<<0,0,0>>)		
			SHAKE_CAM(Camtarget,"hand_shake",1)
			iLESTERCAM_stage++
		break
		
		
		//loop between vans
		
		case 1 //return as index
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if (fCurrentPlaybackTime > 30000 and fCurrentPlaybackTime < 31000)
				or (fCurrentPlaybackTime > 85000 and fCurrentPlaybackTime < 86000)
				or (fCurrentPlaybackTime > 133000 and fCurrentPlaybackTime < 134000)	
				or (fCurrentPlaybackTime > 175000 and fCurrentPlaybackTime < 176000)
					POINT_CAM_AT_ENTITY(camfollow,vehs[mvf_security_truck_fol].id,<<0,0,0>>)
					SET_CAM_FOV(camfollow,45)
					SET_CAM_ACTIVE_WITH_INTERP(camfollow,cameraIndex,3000)
					iLESTERCAM_stage++
				endif				
			endif
		break
		case 2 // return as follow
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if (fCurrentPlaybackTime > 34000 and fCurrentPlaybackTime < 35000)
				or (fCurrentPlaybackTime > 90000 and fCurrentPlaybackTime < 91000)
				or (fCurrentPlaybackTime > 138000 and fCurrentPlaybackTime < 139000)
				or (fCurrentPlaybackTime > 180000 and fCurrentPlaybackTime < 181000)
					POINT_CAM_AT_ENTITY(cameraIndex,vehs[mvf_security_truck].id,<<-2,-5,0>>)	
					SET_CAM_FOV(cameraIndex,42)
					SET_CAM_ACTIVE_WITH_INTERP(cameraIndex,camfollow,4000)
					iLESTERCAM_stage++
				endif
			endif
		break
		case 3
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if (fCurrentPlaybackTime > 40000 and fCurrentPlaybackTime < 41000)
				or (fCurrentPlaybackTime > 99000 and fCurrentPlaybackTime < 100000)
				or (fCurrentPlaybackTime > 144000 and fCurrentPlaybackTime < 145000)
				or (fCurrentPlaybackTime > 186000 and fCurrentPlaybackTime < 187000)	
					POINT_CAM_AT_ENTITY(camfollow,vehs[mvf_security_truck_fol].id,<<2,-7,3>>)	
					SET_CAM_FOV(camfollow,32)
					SET_CAM_ACTIVE_WITH_INTERP(camfollow,cameraIndex,2000)
					iLESTERCAM_stage++
				endif
			endif
		break
		case 4
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if (fCurrentPlaybackTime > 48000 and fCurrentPlaybackTime < 49000)
				or (fCurrentPlaybackTime > 105000 and fCurrentPlaybackTime < 106000)
				or (fCurrentPlaybackTime > 148000 and fCurrentPlaybackTime < 149000)
				or (fCurrentPlaybackTime > 190000 and fCurrentPlaybackTime < 191000)	
					POINT_CAM_AT_ENTITY(cameraIndex,vehs[mvf_security_truck].id,<<5,3,1>>)	
					SET_CAM_FOV(cameraIndex,35)
					SET_CAM_ACTIVE_WITH_INTERP(cameraIndex,camfollow,3000)
					iLESTERCAM_stage++
				endif
			endif
		break
		case 5
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if (fCurrentPlaybackTime > 52000 and fCurrentPlaybackTime < 53000)
				or (fCurrentPlaybackTime > 114000 and fCurrentPlaybackTime < 1150000)
				or (fCurrentPlaybackTime > 153000 and fCurrentPlaybackTime < 154000)
				or (fCurrentPlaybackTime > 194000 and fCurrentPlaybackTime < 195000)	
					POINT_CAM_AT_ENTITY(camfollow,vehs[mvf_security_truck].id,<<1,-12.5,2>>)	
					SET_CAM_FOV(camfollow,40)
					SET_CAM_ACTIVE_WITH_INTERP(camfollow,cameraIndex,2000)
					iLESTERCAM_stage++
				endif
			endif
		break
		case 6
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if (fCurrentPlaybackTime > 60000 and fCurrentPlaybackTime < 61000)
				or (fCurrentPlaybackTime > 122000 and fCurrentPlaybackTime < 123000)
				or (fCurrentPlaybackTime > 157000 and fCurrentPlaybackTime < 158000)
				or (fCurrentPlaybackTime > 197500 and fCurrentPlaybackTime < 199000)	
					POINT_CAM_AT_ENTITY(cameraIndex,vehs[mvf_security_truck].id,<<-3,-12.5,0>>)	
					SET_CAM_FOV(cameraIndex,43)
					SET_CAM_ACTIVE_WITH_INTERP(cameraIndex,camfollow,2000)
					iLESTERCAM_stage++				
				endif
			endif
		break
		case 7
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if (fCurrentPlaybackTime > 72000 and fCurrentPlaybackTime < 73000)
				or (fCurrentPlaybackTime > 124000 and fCurrentPlaybackTime < 125000)
				or (fCurrentPlaybackTime > 160000 and fCurrentPlaybackTime < 161000)
					iLESTERCAM_stage = 1				
				endif
			endif
		break
		// point of interest 1 - bridge
		case 8	
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if (fCurrentPlaybackTime > 75800 and fCurrentPlaybackTime < 80000)

					POINT_CAM_AT_COORD(Camtarget,<<522.83740, -1233.52051, 34.04720>>)	
					SET_CAM_FOV(Camtarget,45)
					SET_CAM_ACTIVE_WITH_INTERP(Camtarget,cameraIndex,2000)
					iLESTERCAM_stage = 9
				endif
			endif
		break
		case 9
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if (fCurrentPlaybackTime > 80000 and fCurrentPlaybackTime < 82000)
					POINT_CAM_AT_ENTITY(cameraIndex,vehs[mvf_security_truck].id,<<0,0,0>>)	
					SET_CAM_FOV(cameraIndex,40)
					SET_CAM_ACTIVE_WITH_INTERP(cameraIndex,Camtarget,2000)
					iLESTERCAM_stage = 1
				endif
			endif
		break
		// point of interest 2 - police station 
		case 10
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if (fCurrentPlaybackTime > 125600 and fCurrentPlaybackTime < 127000)

					POINT_CAM_AT_COORD(Camtarget,<<408.45987, -985.92719, 29.09093>>)	
					SET_CAM_FOV(Camtarget,45)
					SET_CAM_ACTIVE_WITH_INTERP(Camtarget,cameraIndex,3000)
					iLESTERCAM_stage = 11	
				endif	
			endif
		break
		case 11
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if (fCurrentPlaybackTime > 130000 and fCurrentPlaybackTime < 131000)				
					POINT_CAM_AT_ENTITY(cameraIndex,vehs[mvf_security_truck].id,<<0,0,0>>)	
					SET_CAM_FOV(cameraIndex,35)
					SET_CAM_ACTIVE_WITH_INTERP(cameraIndex,Camtarget,3000)
					iLESTERCAM_stage = 1
				endif
			endif
		break
		// point of interest 3 - tunnel exit
		case 12
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if (fCurrentPlaybackTime > 160000 and fCurrentPlaybackTime < 162000)

					POINT_CAM_AT_COORD(Camtarget,<<79.28018, -559.11414, 31.16214>>)	
					SET_CAM_FOV(Camtarget,45)
					SET_CAM_ACTIVE_WITH_INTERP(Camtarget,cameraIndex,4000)
					iLESTERCAM_stage = 13							
				endif
			endif
		break
		case 13
			if not bTunnel	
				if IS_CAM_ACTIVE(Camtarget)
				and not IS_CAM_INTERPOLATING(Camtarget)					
					POINT_CAM_AT_ENTITY(cameraIndex,vehs[mvf_security_truck].id,<<0,0,0>>)	
					SET_CAM_FOV(cameraIndex,35)
					SET_CAM_ACTIVE_WITH_INTERP(cameraIndex,Camtarget,2000)
					iLESTERCAM_stage = 1
				endif
			endif
		break
	endswitch
endif

if mission_stage = enum_to_int(msf_5_CONSTRUCTION_HOLE)
and INIT_CAM()
	if mission_substage < 5	
		switch iHoleStage
			case 0		
				SET_CINEMATIC_BUTTON_ACTIVE(false)			
				//cams on hole
				cameraIndex = CREATE_CAMERA(CAMTYPE_SCRIPTED)
				ATTACH_CAM_TO_ENTITY(cameraIndex,vehs[mvf_helicopter].id,<<0,0,0>>)
				POINT_CAM_AT_COORD(cameraIndex,vHOLE)
				SET_CAM_FOV(cameraIndex,60)
				SHAKE_CAM(cameraIndex,"hand_shake",1)
				SET_CAM_ACTIVE(cameraIndex,true)
				
				camfollow = CREATE_CAMERA(CAMTYPE_SCRIPTED)
				ATTACH_CAM_TO_ENTITY(camfollow,vehs[mvf_helicopter].id,<<0,0,0>>)
				SHAKE_CAM(camfollow,"hand_shake",1)
				iCamTimer = get_Game_timer()	
				iHoleStage++
				
			break
			case 1 //interp to follow
				if get_game_timer() - icamTimer > 2000	
					if DOES_CAM_EXIST(cameraIndex)
					and DOES_CAM_EXIST(camfollow)
						POINT_CAM_AT_COORD(camfollow,<<24.51892, -634.02692, 15.08808>>)
						SET_CAM_FOV(camfollow,55)
						SET_CAM_ACTIVE_WITH_INTERP(camfollow,cameraIndex,3000)
						iCamTimer = get_Game_timer()
						iHoleStage++		
					endif
				endif
			break
			case 2 // set and start iterp to camindex
				if get_game_timer() - icamTimer > 4000
					if DOES_CAM_EXIST(cameraIndex)
					and DOES_CAM_EXIST(camfollow)
						POINT_CAM_AT_COORD(cameraIndex,<<15.07439, -641.73126,15.08808>>)
						SET_CAM_FOV(cameraIndex,56)
						SET_CAM_ACTIVE_WITH_INTERP(cameraIndex,camfollow,5000)
						iCamTimer = get_Game_timer()
						iHoleStage++	
					endif
				endif
			break
			case 3
				if get_game_timer() - icamTimer > 8000
					if DOES_CAM_EXIST(cameraIndex)
					and DOES_CAM_EXIST(camfollow)
						POINT_CAM_AT_COORD(camfollow,<<17.36475, -645.56958, 15.08808>>)
						SET_CAM_FOV(camfollow,54)
						SET_CAM_ACTIVE_WITH_INTERP(camfollow,cameraIndex,4000)
						iCamTimer = get_Game_timer()
						iHoleStage++		
					endif
				endif
			break
			case 4
				if get_game_timer() - icamTimer > 4500	
					if DOES_CAM_EXIST(cameraIndex)
					and DOES_CAM_EXIST(camfollow)
						POINT_CAM_AT_COORD(cameraIndex,<<17.36475, -645.56958, 15.08808>>)
						SET_CAM_FOV(cameraIndex,52)
						SET_CAM_ACTIVE_WITH_INTERP(cameraIndex,camfollow,1000)
						iCamTimer = get_Game_timer()
						iHoleStage++		
					endif
				endif
			break
			case 5
				if get_game_timer() - icamTimer > 5000
					if DOES_CAM_EXIST(cameraIndex)
					and DOES_CAM_EXIST(camfollow)
						POINT_CAM_AT_COORD(camfollow,<<19.90802, -641.74707, 15.08808>>)
						SET_CAM_FOV(camfollow,55)
						SET_CAM_ACTIVE_WITH_INTERP(camfollow,cameraIndex,1500)
						iCamTimer = get_Game_timer()
						iHoleStage++	
					endif	
				endif
			break
			case 6
				if get_game_timer() - icamTimer > 3000
					if DOES_CAM_EXIST(cameraIndex)
					and DOES_CAM_EXIST(camfollow)
						POINT_CAM_AT_COORD(cameraIndex,<<17.18857, -635.11615, 15.08808>>)
						SET_CAM_FOV(cameraIndex,59)
						SET_CAM_ACTIVE_WITH_INTERP(cameraIndex,camfollow,3000)
						iCamTimer = get_Game_timer()
						iHoleStage++			
					endif
				endif
			break
			case 7
				if get_game_timer() - icamTimer > 5000
					if DOES_CAM_EXIST(cameraIndex)
					and DOES_CAM_EXIST(camfollow)
						POINT_CAM_AT_COORD(camfollow,<<21.87010, -628.49554,15.08808>>)
						SET_CAM_FOV(camfollow,60)
						SET_CAM_ACTIVE_WITH_INTERP(camfollow,cameraIndex,4000)
						iCamTimer = get_Game_timer()
						iHoleStage++			
					endif
				endif
			break
			case 8
				if get_game_timer() - icamTimer > 6000	
					if DOES_CAM_EXIST(cameraIndex)
					and DOES_CAM_EXIST(camfollow)
						POINT_CAM_AT_COORD(cameraIndex,<<24.27826, -644.17523,15.08808>>)
						SET_CAM_FOV(cameraIndex,57)
						SET_CAM_ACTIVE_WITH_INTERP(cameraIndex,camfollow,2000)
						iCamTimer = get_Game_timer()
						iHoleStage++	
					endif
				endif
			break
			case 9
				if get_game_timer() - icamTimer > 6000	
					iCamTimer = get_Game_timer()
					iHoleStage = 1
				endif
			break
			case 10
				
			break
		endswitch
	else
		IF NOT IS_ENTITY_DEAD(TREV())
			IF NOT IS_ENTITY_VISIBLE(TREV())
				SET_ENTITY_VISIBLE(TREV(),true)
			ENDIF
		ENDIF
		IF NOT IS_ENTITY_DEAD(LESTER())
			IF NOT IS_ENTITY_VISIBLE(lester())
				SET_ENTITY_VISIBLE(lester(),true)
			ENDIF
		ENDIF
		IF NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
			IF NOT IS_ENTITY_VISIBLE(vehs[mvf_helicopter].id)
				SET_ENTITY_VISIBLE(vehs[mvf_helicopter].id,true)
			ENDIF
		ENDIF
		RENDER_SCRIPT_CAMS(false,false)
		DISPLAY_RADAR(true)
		DISPLAY_HUD(true)
		DESTROY_ALL_CAMS()
		STOP_SOUND(camZoom)
		STOP_SOUND(camhum)
	endif
endif		

	//-------------------------turn film cam on and off------------------------
	if bLesterCam
	
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
		
		REMAP_LODSCALE_RANGE_THIS_FRAME(1.7,4.7,1.0,1.8)  
		//SUPPRESS_HD_MAP_STREAMING_THIS_FRAME()
		SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_HELICAM)
		DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
		DISABLE_SELECTOR_THIS_FRAME()

		if mission_stage = enum_to_int(msf_4_heli_follow_truck)
		and not bTunnel 	
			stop_mission_audio_scenes(8)
			if not IS_AUDIO_SCENE_ACTIVE("BS_1_TRUCKS_HINT_CAM")
				START_AUDIO_SCENE("BS_1_TRUCKS_HINT_CAM")
			endif
		elif mission_stage = enum_to_int(msf_5_CONSTRUCTION_HOLE)
			stop_mission_audio_scenes(11)
			if not IS_AUDIO_SCENE_ACTIVE("BS_1_VIEW_RECORDING")
				START_AUDIO_SCENE("BS_1_VIEW_RECORDING")
			endif
		endif		
		if mission_stage = enum_to_int(msf_4_heli_follow_truck)
			if get_game_timer() - iFaceHintTimer > 1500
				SET_ENTITY_FACING(vehs[mvf_helicopter].id,GET_ENTITY_COORDS(vehs[mvf_security_truck].id))
			endif
		endif		
	else
		iFaceHintTimer = GET_GAME_TIMER()
		
		if mission_stage = enum_to_int(msf_4_heli_follow_truck)
		and not bTunnel 
		 	stop_mission_audio_scenes(7)
			if not IS_AUDIO_SCENE_ACTIVE("BS_1_TRUCKS_SPOTTED")
				START_AUDIO_SCENE("BS_1_TRUCKS_SPOTTED")
			endif
		elif mission_stage = enum_to_int(msf_5_CONSTRUCTION_HOLE)
			stop_mission_audio_scenes(10)
			if not IS_AUDIO_SCENE_ACTIVE("BS_1_SEARCH_FOR_ENTRANCE")
				START_AUDIO_SCENE("BS_1_SEARCH_FOR_ENTRANCE")
			endif
		endif
	endif
	
	if DOES_CAM_EXIST(cameraIndex)
	and DOES_CAM_EXIST(camfollow)	
		BOOL bLesterCamLastFrame = bLesterCam
		switch ihintstage
			case 0 //off
				bLesterCam = false
				IF IS_VEHICLE_DRIVEABLE(vehs[mvf_helicopter].id)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehs[mvf_helicopter].id)
						if IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_VEH_CIN_CAM)
							hintCheckTimer = get_game_timer()
							bLesterCam = true
							ihintstage = 1			
						endif
					ENDIF
				ENDIF
				
				//Toggle the multihead blinders
				IF bLesterCamLastFrame <> bLesterCam
					SET_MULTIHEAD_SAFE(bLesterCam,TRUE)
				ENDIF
			break
			
			case 1 // check			
				// button was tapped toggle
				if get_game_timer() - hintCheckTimer <= 500 //toggle
					if not IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_VEH_CIN_CAM)
						hintCheckTimer = get_game_timer()
						ihintstage = 3
					endif
				else				
				//held down check if no longer held
					ihintstage = 2
				endif				
			break
			
			case 2 // held
				if not IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_VEH_CIN_CAM)
					ihintstage = 0
				endif
			break
			
			case 3 // toggle
//				bLesterCam = true
				if get_game_timer() - hintCheckTimer > 500 
					IF IS_VEHICLE_DRIVEABLE(vehs[mvf_helicopter].id)
						if IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,INPUT_VEH_CIN_CAM)
						OR NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehs[mvf_helicopter].id)
							ihintstage = 0
						endif
					ENDIF
				endif
			break
			
		endswitch
		
		if bLesterCam // ON
			IF NOT IS_CUTSCENE_PLAYING()
				if IS_CAM_ACTIVE(camfollow)
				or IS_CAM_ACTIVE(cameraIndex)				
					SET_ENTITY_VISIBLE(TREV(),false)
					SET_ENTITY_VISIBLE(lester(),false)
					SET_ENTITY_VISIBLE(vehs[mvf_helicopter].id,false)
					RENDER_SCRIPT_CAMS(true,false)			
					DISPLAY_RADAR(false)
					DISPLAY_HUD(false)
					if IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					endif	
					
					DO_CAM_UI()
					
					
					if DOES_CAM_EXIST(GET_RENDERING_CAM())
						if GET_CAM_FOV(GET_RENDERING_CAM()) != camPrevFOV 
							PLAY_SOUND_FRONTEND(camzoom,"Camera_Zoom","BIG_SCORE_SETUP_SOUNDS")
						elif GET_CAM_FOV(GET_RENDERING_CAM()) = camPrevFOV
							STOP_SOUND(camzoom)
						endif
						camPrevFOV = GET_CAM_FOV(GET_RENDERING_CAM())
					endif
					
				endif
			ENDIF
		else // OFF
			IF NOT IS_ENTITY_DEAD(TREV())
				IF NOT IS_ENTITY_VISIBLE(TREV())
					SET_ENTITY_VISIBLE(TREV(),true)
					CLEAR_TIMECYCLE_MODIFIER()
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD(LESTER())
				IF NOT IS_ENTITY_VISIBLE(lester())
					SET_ENTITY_VISIBLE(lester(),true)
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
				IF NOT IS_ENTITY_VISIBLE(vehs[mvf_helicopter].id)
					SET_ENTITY_VISIBLE(vehs[mvf_helicopter].id,true)
				ENDIF
			ENDIF
			RENDER_SCRIPT_CAMS(false,false)
			DISPLAY_RADAR(true)
			DISPLAY_HUD(true)
			STOP_SOUND(camZoom)
			STOP_SOUND(camhum)
		endif	
	endif
endproc
FUNC bool HOLE_INFO_DONE()
	switch iHOLE_INFO_STAGE
		case 0
			if IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
			and IS_PED_IN_VEHICLE(lester(),vehs[mvf_helicopter].id)
				if not IS_MESSAGE_BEING_DISPLAYED()
				and get_game_timer() - iHoleTimer > 1000
					if CREATE_CONVERSATION(convo_struct, "FH1AUD", "FH1_HOLECAM", CONV_PRIORITY_MEDIUM)
						iHoleTimer = get_game_timer()
						iHOLE_INFO_STAGE++
					endif
				endif
			endif
		break
		case 1
			if IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
			and IS_PED_IN_VEHICLE(lester(),vehs[mvf_helicopter].id)
				if not IS_MESSAGE_BEING_DISPLAYED()
				and get_game_timer() - iHoleTimer > 3000
					if CREATE_CONVERSATION(convo_struct, "FH1AUD", "FH1_HOLECAM", CONV_PRIORITY_MEDIUM)
						iHoleTimer = get_game_timer()
						iHOLE_INFO_STAGE++
					endif
				endif
			endif
		break
		case 2
			if IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
			and IS_PED_IN_VEHICLE(lester(),vehs[mvf_helicopter].id)
				if not IS_MESSAGE_BEING_DISPLAYED()
				and get_game_timer() - iHoleTimer > 1000
					if CREATE_CONVERSATION(convo_struct, "FH1AUD", "FH1_HOLECAM", CONV_PRIORITY_MEDIUM)
						iHoleTimer = get_game_timer()
						iHOLE_INFO_STAGE++
					endif
				endif
			endif
		break
		case 3
			if IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
			and IS_PED_IN_VEHICLE(lester(),vehs[mvf_helicopter].id)
				if not IS_MESSAGE_BEING_DISPLAYED()
				and get_game_timer() - iHoleTimer > 2500
					if CREATE_CONVERSATION(convo_struct, "FH1AUD", "FH1_HOLECAM", CONV_PRIORITY_MEDIUM)
						iHoleTimer = get_game_timer()
						iHOLE_INFO_STAGE++
					endif
				endif
			endif
		break
		case 4
			return true
		break
	endswitch
return false	
ENDFUNC
FUNC bool Filmed_hole()
	// --------------------------- In position check ---------------------------	
	
	if IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_helicopter].id,<<30.85060, -616.74359, 30>>,<<19.54452, -648.75940, 48>>,24.0)
	and IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
	and not GET_IS_TASK_ACTIVE(trev(),CODE_TASK_EXIT_VEHICLE)
		//----------------------Can Resume Lester's comments---------------------------
					
		if DOES_BLIP_EXIST(blip_objective)
			remove_blip(blip_objective)
		endif
		
		bDialoguePlayed = false //reset for lester to say get back to the hole
		
		//filming finished
		if not IS_SCRIPTED_CONVERSATION_ONGOING() 
		and get_game_timer() - iHoleTimer > 3000		
		and HOLE_INFO_DONE()
			if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_HOLE2",CONV_PRIORITY_VERY_HIGH)
				IF NOT IS_ENTITY_DEAD(TREV())
					IF NOT IS_ENTITY_VISIBLE(TREV())
						SET_ENTITY_VISIBLE(TREV(),true)
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(LESTER())
					IF NOT IS_ENTITY_VISIBLE(lester())
						SET_ENTITY_VISIBLE(lester(),true)
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
					IF NOT IS_ENTITY_VISIBLE(vehs[mvf_helicopter].id)
						SET_ENTITY_VISIBLE(vehs[mvf_helicopter].id,true)
					ENDIF
				ENDIF
				RENDER_SCRIPT_CAMS(false,false)
				DISPLAY_RADAR(true)
				DISPLAY_HUD(true)
				SET_PLAYER_CONTROL(player_id(),true)	
				STOP_SOUND(camZoom)
				STOP_SOUND(camhum)
				return true
			endif	
		endif
		
	else // left filming area
	
		// lester's comments are interrupted and then tells player to go back(Only once does he tell the player to go back)
			
		if not IS_MESSAGE_BEING_DISPLAYED()
		and not bDialoguePlayed
			if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_HOLEBK",CONV_PRIORITY_MEDIUM)
				bDialoguePlayed = true
			endif
		endif
		
		// re blip position
		if not DOES_BLIP_EXIST(blip_objective)
			blip_objective = CREATE_BLIP_FOR_COORD(<< 25.33051, -637.68353, 15.08808 >>)
		endif	
		iHoleTimer = get_Game_timer()
		SET_PLAYER_CONTROL(player_id(),true)
		IF NOT IS_PED_INJURED(trev())
			if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
				CLEAR_PED_TASKS(trev())
			endif
		ENDIF
	endif		
	
	if bLesterCam
		SET_PLAYER_CONTROL(player_id(),false)
		IF NOT IS_PED_INJURED(trev())
			if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
				TASK_HELI_MISSION(trev(),vehs[mvf_helicopter].id,null,null,GET_ENTITY_COORDS(vehs[mvf_helicopter].id),MISSION_GOTO,0.01,-1,-1,-1,-1)
			endif
		ENDIF
	else
		SET_PLAYER_CONTROL(player_id(),true)
		IF NOT IS_PED_INJURED(trev())
			if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
				CLEAR_PED_TASKS(trev())
			endif
		ENDIF
	endif
	
	return false
ENDFUNC
proc find_hole_dialogue()
	switch eholestage
	
		case HOLE_OBJECTIVE
			if IS_SAFE_TO_DISPLAY_GODTEXT()
			and IS_PED_IN_VEHICLE(TREV(),vehs[mvf_helicopter].id)
				bFrankHELP1			= false 
				eholestage = HOLE_UNSEEN
			endif
		break
		
		case HOLE_UNSEEN
		
			// can see the hole as its on screen for 2.5 secs
			if not bSeenHole
				
					//timer before franklin tells the player where exactly to look
				if timera() > 90000
				and mission_substage > STAGE_ENTRY
				and mission_substage < 3
					if not bDialoguePlayed
						if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_TIMEH",CONV_PRIORITY_VERY_HIGH)
							bDialoguePlayed = true
						endif
					endif
					if not DOES_BLIP_EXIST(blip_objective)
					and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						blip_objective = CREATE_BLIP_FOR_COORD(vHOLE)
					endif
				endif
			
				// at the wrong construction site
				if not bWrongSite
				and GET_DISTANCE_BETWEEN_COORDS(<<73.15453, -374.87485, 38.92091>>,GET_ENTITY_COORDS(TREV())) < 130	
					if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_WRCH",CONV_PRIORITY_VERY_HIGH)
						bWrongSite = true
					endif
				endif
				
				if bFrankHELP1 
					iONSCREENTIME  = 800
				else
					iONSCREENTIME  = 2000
				endif
				
				if (IS_POINT_VISIBLE(vHOLE,2,200)and not IS_ENTITY_OCCLUDED(objdummy))
				or ( GET_DISTANCE_BETWEEN_COORDS(vHOLE,GET_ENTITY_COORDS(TREV())) < 50)
					if get_game_timer() - iHoleTimer > iONSCREENTIME
						if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_HOLE1",CONV_PRIORITY_VERY_HIGH)					 
							bSeenHole = true							
							if not DOES_BLIP_EXIST(blip_objective)
								blip_objective = CREATE_BLIP_FOR_COORD(vHOLE)
							endif
							if DOES_ENTITY_EXIST(objdummy)
								DELETE_OBJECT(objdummy)
							endif
							bCloserLest = true
							eholestage = HOLE_SEEN
						endif
					endif
				else
					iHoleTimer = get_game_timer()
				endif
			endif
			
			
		break
		case HOLE_SEEN
			
			if IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_helicopter].id,<<30.85060, -616.74359, 30>>,<<19.54452, -648.75940, 48>>,20)
				if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_FINDH",CONV_PRIORITY_VERY_HIGH)	
						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
						eholestage = HOLE_STABLE
					endif
				else
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				endif				
			else
				if get_game_timer() - iCloserTimer > 30000
					if bCloserLest
						if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_ABOVE",CONV_PRIORITY_VERY_HIGH)	
							bCloserLest = false
							iCloserTimer = get_game_timer()
						endif
					else
						if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_HOLEV",CONV_PRIORITY_VERY_HIGH)	
							bCloserLest = true
							iCloserTimer = get_game_timer()
						endif
					endif					
				endif
			endif
		
		break
		case HOLE_STABLE
			
		break
	endswitch
endproc

proc draw_TimeLeft_meter()
	if not IS_PAUSE_MENU_ACTIVE()
		DRAW_CLOCK("FH1_TIME",true,14,45,TIMER_STYLE_DONTUSEMILLISECONDS)
	endif
endproc
proc Checks_Area_Assets()

	if not bloadGuards
		if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vFRONT_BANK) < 200
		or GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vBACK_BANK) < 200
		or (mission_stage = enum_to_int(msf_4_heli_follow_truck) and mission_substage > 10)
			REQUEST_MODEL(S_M_M_ARMOURED_01)
			REQUEST_ANIM_DICT("missbigscore1guard_wait_rifle")
			bloadGuards = true
			bcreateGuards = true
		endif
	else
		if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vFRONT_BANK) < 200
		or GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vBACK_BANK)  < 200	
		or (mission_stage = enum_to_int(msf_4_heli_follow_truck) and mission_substage > 10)
			if HAS_MODEL_LOADED(S_M_M_ARMOURED_01)
			and HAS_ANIM_DICT_LOADED("missbigscore1guard_wait_rifle")
			and bcreateGuards
				if not IsEntityAlive(peds[mpf_guard].id)
					Create_guard(mpf_guard,<<-77.96, -677.87, 33.47 >>, 64.7755)	
				endif
				if not IsEntityAlive(peds[mpf_guardf].id)
					Create_guard(mpf_guardf,<<9.47284, -710.34497, 45.01460>>, 209.3389)
					CLEAR_AREA(<<3.41664, -710.20435, 44.97406>>,4,true)
				endif
				bcreateGuards = false
			endif
		elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vFRONT_BANK) > 230
		and  GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vBACK_BANK) 	> 230
			if mission_stage != enum_to_int(msf_4_heli_follow_truck) 
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_ARMOURED_01)				
			endif
			SAFE_RELEASE_PED(peds[mpf_guard].id)
			SAFE_RELEASE_PED(peds[mpf_guardf].id)
			REMOVE_ANIM_DICT("missbigscore1guard_wait_rifle")
			bloadGuards = false
		endif
	endif
	
	if mission_stage >= ENUM_TO_INT(msf_4_heli_follow_truck)
		if not bloadDozer
			if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vHOLE) < 300
				REQUEST_MODEL(BULLDOZER)
				bloadDozer = true
				bcreateDozers = true
			endif
		else
			if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vHOLE) < 300
				if HAS_MODEL_LOADED(BULLDOZER)
				and bcreateDozers
					if not IsEntityAlive(vehs[mvf_bulldozer1].id)
						vehs[mvf_bulldozer1].id = CREATE_VEHICLE(bulldozer,<<26.76431, -609.32452, 30.62795>>, 252.2286)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_bulldozer1].id,false)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_bulldozer1].id)					
				
					endif
					if not IsEntityAlive(vehs[mvf_bulldozer2].id)
						vehs[mvf_bulldozer2].id = CREATE_VEHICLE(bulldozer,<<36.85886, -648.69897, 30.62794>>, 140.3838)				
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_bulldozer2].id,false)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_bulldozer2].id)
					endif
					bcreateDozers = false
				endif
			elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vHOLE) > 330
				SAFE_RELEASE_VEHICLE(vehs[mvf_bulldozer1].id)		
				SAFE_RELEASE_VEHICLE(vehs[mvf_bulldozer1].id)	
				SET_MODEL_AS_NO_LONGER_NEEDED(BULLDOZER)
				bloadDozer = false
			endif
		endif
	endif
endproc
proc Check_Death_Fail()
//=============================== death checks ==========================================
	
	for i = 0 to enum_to_int(MPF_NUM_OF_PEDS) -1
		if DOES_ENTITY_EXIST(peds[i].id)
			if IS_PED_INJURED(peds[i].id)				
				if peds[i].id = mike()
					Mission_Failed(mff_mike_dead_fail)
				elif peds[i].id = trev()
					Mission_Failed(mff_trevor_dead_fail)
				elif peds[i].id = frank()
					Mission_Failed(mff_franklin_dead_fail)
				elif peds[i].id = lester()
					Mission_Failed(mff_lester_dead_fail)
				elif peds[i].id = peds[mpf_guard].id
				or 	 peds[i].id = peds[mpf_guardf].id
				or 	 peds[i].id = peds[mpf_worker1].id
				or   peds[i].id = peds[mpf_worker2].id
				or 	 peds[i].id = peds[mpf_worker3].id
					Mission_Failed(mff_bank_knows)
				endif
				SAFE_RELEASE_PED(peds[i].id)
			ENDIF
		ENDIF
	ENDFOR
	for i = 0 to enum_to_int(MVF_NUM_OF_VEH) -1
		if DOES_ENTITY_EXIST(vehs[i].id)		
			if vehs[i].id = vehs[mvf_Michael_veh].id
			and IS_VEHICLE_DRIVEABLE(vehs[i].id)
				if IS_ENTITY_ON_FIRE(vehs[mvf_Michael_veh].id)
					Mission_Failed(mff_Mike_car_des)
				endif				
				if DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(vehs[mvf_Michael_veh].id)
					if IS_VEHICLE_STUCK_ON_ROOF(vehs[mvf_Michael_veh].id)	
						if PLAYER_PED_ID() = MIKE()
							Mission_Failed(mff_Mike_car_des)
						endif
					endif
				else					
					ADD_VEHICLE_UPSIDEDOWN_CHECK(vehs[mvf_Michael_veh].id)
				endif
			endif
			if vehs[i].id = vehs[mvf_trevor_veh].id
			and IS_VEHICLE_DRIVEABLE(vehs[i].id)
				if IS_ENTITY_ON_FIRE(vehs[mvf_trevor_veh].id)
					Mission_Failed(mff_trev_car_des)
				endif				
				if DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(vehs[mvf_trevor_veh].id)
					if IS_VEHICLE_STUCK_ON_ROOF(vehs[mvf_trevor_veh].id)	
						if PLAYER_PED_ID() = trev()
							Mission_Failed(mff_trev_car_des)
						endif
					endif
				else					
					ADD_VEHICLE_UPSIDEDOWN_CHECK(vehs[mvf_trevor_veh].id)
				endif
			endif
			if not IS_VEHICLE_DRIVEABLE(vehs[i].id)				
				if vehs[i].id = vehs[mvf_Michael_veh].id
					Mission_Failed(mff_Mike_car_des)
				endif
				if vehs[i].id = vehs[mvf_trevor_veh].id
					
					Mission_Failed(mff_Trev_car_des)
				endif
				if vehs[i].id = vehs[mvf_helicopter].id
					Mission_Failed(mff_heli_des)
				endif
				SAFE_RELEASE_VEHICLE(vehs[i].id)
			ELSE
				IF NOT IS_CUTSCENE_PLAYING()
					if vehs[i].id = vehs[mvf_helicopter].id
						IF IS_ENTITY_IN_AIR(vehs[mvf_helicopter].id)
						AND NOT IS_PED_IN_VEHICLE(TREV(), vehs[mvf_helicopter].id)
							VECTOR currenthelipos = GET_ENTITY_COORDS(vehs[mvf_helicopter].id)
							IF currenthelipos.z > 200
								Mission_Failed(mff_left_lester)//mff_left_mission
								SAFE_RELEASE_VEHICLE(vehs[i].id)
							ENDIF
						ENDIF
					endif
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
//=============================== fail check ==========================================	
	//mike left bank check
	if btimeReset
		
		if not Is_clock_time_less_than_or_equal(15,24)
		and mission_stage < enum_To_int(msf_4_heli_follow_truck)	
			countdownSound()
		ENDIF
		
		if not Is_clock_time_less_than_or_equal(15,30)
		and mission_stage < enum_To_int(msf_4_heli_follow_truck)	
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			Mission_Failed(mff_time)
		endif
		
		if mission_stage < enum_to_int(msf_6_drop_off_point)
			if IsEntityAlive(MIKE())			
				if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(MIKE(),<<7.03093, -710.37170, 45.01460>>) > 2000
				and player_ped_id() = mike()
					Mission_Failed(mff_left_mission)
				elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(MIKE(),<<7.03093, -710.37170, 45.01460>>) > 1900
				and player_ped_id() = mike()
					if IS_SAFE_TO_DISPLAY_GODTEXT()
					and not bLeftArea_printed
						print_now("FH1_MIKERT",DEFAULT_GOD_TEXT_TIME,1)
						bLeftArea_printed	= true
					endif
				else
					bLeftArea_printed	= false
				endif
			endif
		elif mission_stage = enum_to_int(msf_6_drop_off_point)
			if IsEntityAlive(MIKE())			
				if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(MIKE(),<<1743.80652, 3270.31860, 40.20966>>) < 600
				and player_ped_id() = mike()
					Mission_Failed(mff_left_mission)
				elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(MIKE(),<<1743.80652, 3270.31860, 40.20966>>) < 700
				and player_ped_id() = mike()
					if IS_SAFE_TO_DISPLAY_GODTEXT()
					and not bLeftArea_printed
						print_now("FH1_MISSRT",DEFAULT_GOD_TEXT_TIME,1)
						bLeftArea_printed	= true
					endif
				else
					bLeftArea_printed	= false
				endif
			endif
		endif
		
	//fail check for leaving buddy
		if not IS_CUTSCENE_PLAYING()
			if player_ped_id() = mike()
				if IsEntityAlive(frank())
				fDist = vdist(GET_ENTITY_COORDS(mike()),GET_ENTITY_COORDS(frank()))	
					if fdist > 90
						if not bLeftbuddyprint
							PRINT_NOW("FH1_HEADBKF",DEFAULT_GOD_TEXT_TIME,1)
							bLeftbuddyprint = true
						endif
						if fdist > 120
							mission_failed(mff_left_franklin)
						endif
					else
						bLeftbuddyprint = false
					endif
				endif
			elif player_ped_id() = trev()
				if IsEntityAlive(lester())
					if GET_DISTANCE_BETWEEN_ENTITIES(trev(),lester()) > 90
					AND eSwitchCamState = SWITCH_CAM_IDLE 
						if not bLeftbuddyprint
							PRINT_NOW("FH1_HEADBKL",DEFAULT_GOD_TEXT_TIME,1)
							bLeftbuddyprint = true
						endif
						if GET_DISTANCE_BETWEEN_ENTITIES(trev(),lester()) > 120
							mission_failed(mff_left_lester)
						endif
					else
						bLeftbuddyprint = false
					endif
				endif
			endif
		endif
		
		//check if the player is close to the bank to set wanted multiplier
		
		if IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 4.5757, -707.3064, 46.2527 >>,<<150,150,60>>)
			if wantedMultiplier < 1
				wantedMultiplier = 1
				SET_WANTED_LEVEL_MULTIPLIER(wantedMultiplier)
			endif	
			if GET_PLAYER_WANTED_LEVEL(player_id()) > 0
			OR IS_EXPLOSION_IN_AREA(EXP_TAG_DONTCARE, <<-51.41013, -753.00482, 28.25766>>, <<80.25236, -683.17981, 100.06847>>)
				Mission_Failed(mff_bank_knows)
			endif
		else
			if wantedMultiplier > 0
				wantedMultiplier = 0
				SET_WANTED_LEVEL_MULTIPLIER(wantedMultiplier)
			endif
		endif
		
	// guards interaction fail
		if DOES_ENTITY_EXIST(peds[mpf_guardF].id)
			If HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(peds[mpf_guardF].id,player_ped_id(),true)		
			or IS_PED_IN_COMBAT(peds[mpf_guardf].id,player_ped_id())
			or IS_PED_BEING_STEALTH_KILLED(peds[mpf_guardf].id)
			or IS_PROJECTILE_IN_AREA((GET_ENTITY_COORDS(peds[mpf_guardf].id)- <<15,15,5>>),(GET_ENTITY_COORDS(peds[mpf_guardf].id)+ <<15,15,5>>),true)
				SET_PLAYER_WANTED_LEVEL(player_id(),1)	
				if GET_SCRIPT_TASK_STATUS(peds[mpf_guardF].id,SCRIPT_TASK_AIM_GUN_AT_ENTITY) != PERFORMING_TASK
					TASK_AIM_GUN_AT_ENTITY(peds[mpf_guardF].id,player_ped_id(),-1)
				endif
				Mission_Failed(mff_bank_knows)
			endif
		endif
		if DOES_ENTITY_EXIST(peds[mpf_guard].id)
			if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(peds[mpf_guard].id,player_ped_id(),true)
			or IS_PED_IN_COMBAT(peds[mpf_guard].id,player_ped_id())
			or IS_PROJECTILE_IN_AREA((GET_ENTITY_COORDS(peds[mpf_guard].id)- <<15,15,5>>),(GET_ENTITY_COORDS(peds[mpf_guard].id)+ <<15,15,5>>),true)
				SET_PLAYER_WANTED_LEVEL(player_id(),1)
				if GET_SCRIPT_TASK_STATUS(peds[mpf_guard].id,SCRIPT_TASK_AIM_GUN_AT_ENTITY) != PERFORMING_TASK
					TASK_AIM_GUN_AT_ENTITY(peds[mpf_guard].id,player_ped_id(),-1)
				endif
				Mission_Failed(mff_bank_knows)
			endif
		endif
		
		if IsEntityAlive(player_ped_id())
			if IS_PED_IN_ANY_VEHICLE(player_ped_id())
				if IS_PED_IN_ANY_HELI(player_ped_id())
					if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),<<-98.69806, -671.90033, 34.46694>>,<<-85.72156, -676.68573, 40.92188>>,25)
					or IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),<<4.47685, -705.70099, 58.89827>>,<<17.18902, -732.90491, 43.21600>>,47)
						if IsEntityAlive(peds[mpf_guard].id)
							if GET_SCRIPT_TASK_STATUS(peds[mpf_guard].id,SCRIPT_TASK_AIM_GUN_AT_ENTITY) != PERFORMING_TASK
								TASK_AIM_GUN_AT_ENTITY(peds[mpf_guard].id,player_ped_id(),-1)
							endif
						endif
						if IsEntityAlive(peds[mpf_guardF].id)
							if GET_SCRIPT_TASK_STATUS(peds[mpf_guardF].id,SCRIPT_TASK_AIM_GUN_AT_ENTITY) != PERFORMING_TASK
								TASK_AIM_GUN_AT_ENTITY(peds[mpf_guardF].id,player_ped_id(),-1)
							endif
						endif
						Mission_Failed(mff_bank_knows)
					endif
				else
					if IS_ENTITY_AT_COORD(player_ped_id(),<<8.12, -713.66, 44.57>>,<<12,12,5>>)			
					or IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),<<30.85060, -616.74359, 15>>,<<19.54452, -648.75940, 29>>,20)
						if IsEntityAlive(peds[mpf_guard].id)
							if GET_SCRIPT_TASK_STATUS(peds[mpf_guard].id,SCRIPT_TASK_AIM_GUN_AT_ENTITY) != PERFORMING_TASK
								TASK_AIM_GUN_AT_ENTITY(peds[mpf_guard].id,player_ped_id(),-1)
							endif
						endif
						if IsEntityAlive(peds[mpf_guardF].id)
							if GET_SCRIPT_TASK_STATUS(peds[mpf_guardF].id,SCRIPT_TASK_AIM_GUN_AT_ENTITY) != PERFORMING_TASK
								TASK_AIM_GUN_AT_ENTITY(peds[mpf_guardF].id,player_ped_id(),-1)
							endif
						endif
						Mission_Failed(mff_bank_knows)
					endif
				endif
			endif
			if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),<<-83.04369, -678.85724, 38>>,<<-69.75829, -683.59113, 32.18783>>,14)
				if IsEntityAlive(peds[mpf_guard].id)
					if GET_SCRIPT_TASK_STATUS(peds[mpf_guard].id,SCRIPT_TASK_AIM_GUN_AT_ENTITY) != PERFORMING_TASK
						TASK_AIM_GUN_AT_ENTITY(peds[mpf_guard].id,player_ped_id(),-1)
					endif
				endif
				Mission_Failed(mff_bank_knows)
			endif			
		endif
		
	endif
	
endproc
proc CHECKS_heli()
	//heli
	if IsEntityAlive(vehs[mvf_helicopter].id)
		if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehs[mvf_helicopter].id),GET_ENTITY_COORDS(player_ped_id())) < 200	
		and not bheliset
			SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_helicopter].id)
			FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id,false)
			bheliset = true
		endif
		if player_ped_id() = MIKE()
			SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_helicopter].id,false)
			if GET_VEHICLE_ENGINE_HEALTH(vehs[mvf_helicopter].id) < 300
				SET_VEHICLE_ENGINE_HEALTH(vehs[mvf_helicopter].id,550)				
			endif
			set_vehicle_is_considered_by_player(vehs[mvf_helicopter].id,false)
		elif player_ped_id() = TREV()	
			SET_VEHICLE_ENGINE_CAN_DEGRADE(vehs[mvf_helicopter].id,false)
			set_vehicle_is_considered_by_player(vehs[mvf_helicopter].id,true)
		endif		
	else
		if DOES_ENTITY_EXIST(TREV())
			if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(TREV(),vAIRFIELD) < 300
				DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY)
				if HAS_MODEL_LOADED(frogger2)
					vehs[mvf_helicopter].id = CREATE_VEHICLE(frogger2,<< 1758.3090, 3284.5278, 40.7 >>, 133.1852 )
					SET_ENTITY_ALWAYS_PRERENDER(vehs[mvf_helicopter].id,true)
					SET_VEHICLE_LIVERY(vehs[mvf_helicopter].id,1)
					SET_ENTITY_COLLISION(vehs[mvf_helicopter].id,true)
					FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id,true)
					Unload_Asset_Model(sAssetData,frogger2)
				endif
			elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(TREV(),vAIRFIELD) < 350
				Load_Asset_Model(sAssetData,frogger2)
			endif
		endif
	endif
	//	VEH's Check for trevor in the heli
	if mission_stage = ENUM_TO_INT(msf_4_heli_follow_truck)
	or mission_stage = ENUM_TO_INT(msf_5_CONSTRUCTION_HOLE)
		if DOES_ENTITY_EXIST(vehs[mvf_helicopter].id)
		and DOES_ENTITY_EXIST(trev())
		and not IS_CUTSCENE_PLAYING()
			if not IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
				if bheliPrintDisplayed = false
					PRINT_NOW("FH1_BKHELI",DEFAULT_GOD_TEXT_TIME,1)
					bheliPrintDisplayed = true
				endif
				if DOES_BLIP_EXIST(blip_objective)
					SET_BLIP_DISPLAY(blip_objective,DISPLAY_NOTHING)
				endif
				if not DOES_BLIP_EXIST(vehs[mvf_helicopter].blip)
					vehs[mvf_helicopter].blip = CREATE_BLIP_FOR_VEHICLE(vehs[mvf_helicopter].id,false)
				endif
			else
				bheliPrintDisplayed = false
				if DOES_BLIP_EXIST(blip_objective)
					SET_BLIP_DISPLAY(blip_objective,DISPLAY_BLIP)
				endif
				if DOES_BLIP_EXIST(vehs[mvf_helicopter].blip)
					remove_blip(vehs[mvf_helicopter].blip)
				endif
			endif
		endif
	else
		if DOES_BLIP_EXIST(vehs[mvf_helicopter].blip)
			remove_blip(vehs[mvf_helicopter].blip)
		endif
	endif
	
	//trevor pos check 
	if player_ped_id() != TREV()
		if IsEntityAlive(vehs[mvf_trevor_veh].id)
		and IsEntityAlive(TREV())
		and IsEntityAlive(LESTER())
		and heli_switch = TO_HELI
			if GET_DISTANCE_BETWEEN_COORDS(vAIRFIELD,GET_ENTITY_COORDS(vehs[mvf_trevor_veh].id)) < 60	
				if DOES_ENTITY_EXIST(vehs[mvf_helicopter].id)
				and IS_VEHICLE_DRIVEABLE(vehs[mvf_helicopter].id)				
					FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id,false)
					SET_PED_INTO_VEHICLE(lester(),vehs[mvf_helicopter].id,VS_BACK_LEFT)
					heli_switch = TO_TRUCKS
				endif
			endif
		endif
	endif
endproc
proc CHECKS_lester()
	//used to attach the camera to lester 
	if DOES_ENTITY_EXIST(objcam)
	and IsEntityAlive(lester())
		if not battachedCam
			if DOES_ENTITY_HAVE_PHYSICS(objCam)
			and DOES_ENTITY_HAVE_PHYSICS(lester())
				ATTACH_ENTITY_TO_ENTITY(objcam,lester(),GET_PED_BONE_INDEX(lester(),BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>)
				IF IsEntityAlive(vehs[mvf_helicopter].id)
					SET_ENTITY_NO_COLLISION_ENTITY(objcam,vehs[mvf_helicopter].id,FALSE)
				ENDIF
				battachedCam = true
			endif
		endif
	endif	
	//stop lester sprinting
	if IsEntityAlive(lester())
		SET_PED_MAX_MOVE_BLEND_RATIO(lester(),PEDMOVEBLENDRATIO_WALK)
	endif
	//Lester looks at trev
	if mission_stage < enum_to_int(msf_4_heli_follow_truck)
		if IsEntityAlive(LESTER())
		and IsEntityAlive(vehs[mvf_helicopter].id)
		and IsEntityAlive(TREV())
			if IS_PED_IN_VEHICLE(lester(),vehs[mvf_helicopter].id)
				if not IS_PED_HEADTRACKING_ENTITY(lester(),trev())
					TASK_LOOK_AT_ENTITY(lester(),TREV(),-1,SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_YAW_LIMIT|SLF_WIDEST_PITCH_LIMIT)
				endif
			endif
		endif
	endif
endproc
proc CHECKS_hole()
	//construction hole		
	if mission_stage > enum_to_int(msf_4_heli_follow_truck)
		if IsEntityAlive(player_ped_id())
			//construction audio
			if not bAUDIOStreamLoaded	
				if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vHOLE) < 250
					if LOAD_STREAM("Construction_Stream","BIG_SCORE_SETUP_SOUNDS")
						bAUDIOStreamLoaded = true
					endif
				endif
			else
				if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vHOLE) > 250
					STOP_STREAM()
					bAUDIOStreamLoaded = false
					bplayingstream = false
				else
					if not bplayingstream
					and bAUDIOStreamLoaded
						PLAY_STREAM_FROM_POSITION(<<26,-636, 17>>)
						bplayingstream = true
					endif
				endif
			endif
			//peds in hole
			if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vHOLE) < 300
				if b_create_workers
					/*if bworkersloaded
						if not IsEntityAlive(peds[mpf_worker1].id)
							peds[mpf_worker1].id = create_ped(PEDTYPE_MISSION,S_M_Y_CONSTRUCT_01,<<5.91112, -655.18097, 15.09623>>,173.28)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_worker1].id,true)
							SET_ENTITY_LOAD_COLLISION_FLAG(peds[mpf_worker1].id,true)
							TASK_START_SCENARIO_AT_POSITION(peds[mpf_worker1].id,"WORLD_HUMAN_HAMMERING",<<5.91112, -655.18097, 15.09623>>,173.28)					
						endif
						if not IsEntityAlive(peds[mpf_worker2].id)
							peds[mpf_worker2].id = create_ped(PEDTYPE_MISSION,S_M_Y_CONSTRUCT_01,<<14.64482, -647.54327, 15.08808>>,117.52)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_worker2].id,true)
							TASK_START_SCENARIO_AT_POSITION(peds[mpf_worker2].id,"WORLD_HUMAN_WELDING",<<14.64482, -647.54327, 15.08808>>,117.52)
						endif
						if not IsEntityAlive(peds[mpf_worker3].id)
							peds[mpf_worker3].id = create_ped(PEDTYPE_MISSION,S_M_Y_CONSTRUCT_01,<<10.53172, -626.45038, 14.68462>>,-176.69)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_worker3].id,true)
							TASK_START_SCENARIO_AT_POSITION(peds[mpf_worker3].id,"WORLD_HUMAN_CONST_DRILL",<<10.53172, -626.45038, 14.68462>>,-176.69)
						endif
						if not IsEntityAlive(peds[mpf_worker4].id)
							peds[mpf_worker4].id = create_ped(PEDTYPE_MISSION,S_M_Y_CONSTRUCT_01,<<37.45826, -668.00366, 15.48347>>,-63.09)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_worker4].id,true)
							OPEN_SEQUENCE_TASK(seq)
								TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<-7.47202, -610.15527, 14.60039>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
								TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<37.10248, -667.52417, 15.48347>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
								TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<7.00026, -654.31519, 15.09687>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
								TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<38.43104, -643.26019, 15.08808>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)						
								SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
							CLOSE_SEQUENCE_TASK(seq)					
							TASK_PERFORM_SEQUENCE(peds[mpf_worker4].id,seq)
							CLEAR_SEQUENCE_TASK(seq)
						endif
						SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_CONSTRUCT_01)
						b_create_workers 	= false
						bworkersloaded 		= false
					else
						REQUEST_MODEL(S_M_Y_CONSTRUCT_01)
						if HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)
							bworkersloaded = true
						endif
					endif*/
				endif
			elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vHOLE) > 330
				b_create_workers = true
				if DOES_ENTITY_EXIST(peds[mpf_worker1].id)
					safe_delete_ped(peds[mpf_worker1].id)
				endif
				if DOES_ENTITY_EXIST(peds[mpf_worker2].id)
					SAFE_DELETE_PED(peds[mpf_worker2].id)
				endif
				if DOES_ENTITY_EXIST(peds[mpf_worker3].id)
					SAFE_DELETE_PED(peds[mpf_worker3].id)
				endif
				if DOES_ENTITY_EXIST(peds[mpf_worker4].id)
					SAFE_DELETE_PED(peds[mpf_worker4].id)
				endif
			endif
		endif
	endif
endproc
PROC MISSION_CHECKS()

	if btimeReset
		if mission_stage < enum_to_int(msf_4_heli_follow_truck)		
			AUDIO_manange_dialogue()
			draw_TimeLeft_meter()
		endif
	endif
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF DOES_ENTITY_EXIST(vehs[mvf_helicopter].id)
		AND DOES_ENTITY_EXIST(trev())
			IF IS_VEHICLE_DRIVEABLE(vehs[mvf_helicopter].id)
				IF IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL,FH1_CAR_DAMAGE)
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehs[mvf_helicopter].id,FH1_HELI_DAMAGE)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehs[mvf_trevor_veh].id)
					IF IS_PED_IN_VEHICLE(trev(),vehs[mvf_trevor_veh].id)
						INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehs[mvf_trevor_veh].id,FH1_CAR_DAMAGE)
					ENDIF
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(vehs[mvf_trevor_veh].id)
					IF IS_PED_IN_VEHICLE(trev(),vehs[mvf_trevor_veh].id)
						INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehs[mvf_trevor_veh].id,FH1_CAR_DAMAGE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(vehs[mvf_Michael_veh].id)
		AND DOES_ENTITY_EXIST(MIKE())
			IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Michael_veh].id)
				IF IS_PED_IN_VEHICLE(mike(),vehs[mvf_Michael_veh].id)
					PRINTSTRING("Checking damage on Mikes car")PRINTNL()
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehs[mvf_Michael_veh].id,FH1_CAR_DAMAGE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	Manage_hotswap()
	DO_SHAKE_INTERP()
	Checks_Area_Assets()
	Check_Death_Fail()
	CHECKS_heli()
	CHECKS_lester()
	CHECKS_hole()
	
	//stats
	if DOES_ENTITY_EXIST(player_ped_id())
	and not IS_PED_INJURED(player_ped_id())
		if IS_PED_IN_ANY_VEHICLE(player_ped_id())
		
			IF NOT IS_PED_IN_ANY_BOAT(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_PLANE(PLAYER_PED_ID())
			AND NOT IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(player_ped_id()), FH1_MAX_SPEED)
				PRINTLN("Setting FH1_MAX_SPEED to watch player veh")
			ELSE
				PRINTLN("Setting FH1_MAX_SPEED to null")
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(null, FH1_MAX_SPEED)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehs[mvf_helicopter].id)
			AND DOES_ENTITY_EXIST(trev())
				IF IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
					PRINTLN("Setting FH1_MAX_HELI_SPEED to watch player veh")
					INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(player_ped_id()), FH1_MAX_HELI_SPEED)
				ENDIF
			ENDIF
			
		else
			PRINTLN("Setting all speed watch to null")
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(null)
		endif
	endif
	
#IF IS_DEBUG_BUILD
	iheliWidget = enum_to_int(heli_switch)
	wDialoguestage = enum_to_int(eDialogue_stateM)
	wBOTH_dialogue = ENUM_TO_INT(eBOTH_dialogue)
	wTREV_Dialogue = ENUM_TO_INT(eTREV_Dialogue)
	wMike_dialogue = ENUM_TO_INT(eMike_dialogue)
	wb_TREVSpeechSwitchStopped	= b_TREVSpeechSwitchStopped
	wb_MIKESpeechSwitchStopped	= b_MIKESpeechSwitchStopped
#endif

ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		MISSION SETUP
// -----------------------------------------------------------------------------------------------------------

PROC MISSION_SETUP()
	bHotSwap = false	
	
	camhum 	= GET_SOUND_ID()
	camzoom = GET_SOUND_ID()
	
	SET_ALL_RANDOM_PEDS_FLEE(player_id(),true)
	REMOVE_RELATIONSHIP_GROUP(rel_buddy)
	REMOVE_RELATIONSHIP_GROUP(rel_guard)
	ADD_RELATIONSHIP_GROUP("BUDDY", rel_buddy)
	ADD_RELATIONSHIP_GROUP("GUARD",rel_guard)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_buddy,rel_buddy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_PLAYER,rel_buddy)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_buddy,RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_guard,rel_guard)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE,RELGROUPHASH_PLAYER,rel_guard)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE,rel_guard,RELGROUPHASH_PLAYER)
	
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE,false)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING,false)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER ,false)	
	
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY, FALSE)
	SET_BUILDING_STATE( BUILDINGNAME_IPL_BIG_SCORE_HOLE_COVER, BUILDINGSTATE_DESTROYED) 
	SET_BUILDING_STATE( BUILDINGNAME_IPL_JEWEL_HEIST_BIKE_TUNNELS, BUILDINGSTATE_DESTROYED) 
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER,true)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BODHI2,true)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(bus,true)
	disable_taxi_hailing(true)
	
	SET_MAX_WANTED_LEVEL(2)
	SET_WEATHER_TYPE_PERSIST("EXTRASUNNY")
	
	SET_BUILDING_STATE(BUILDINGNAME_ES_BANK_CAR_PARK_SHUTTERS,BUILDINGSTATE_NORMAL)
	
	SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<78.4684, -1295.3275, 28.1690>>, 29.1546)
	RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<77.12664, -1261.75354, 28.25048>>,<<104.40543, -1305.11694, 39.38937>>,40,<<78.4684, -1295.3275, 28.1690>>, 29.1546)
	//Dan H delete player's vehicle if not franklin
	DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_MICHAEL | CHAR_TREVOR)
	
	btimeReset = false
	IF Is_Replay_In_Progress()
	or IS_REPEAT_PLAY_ACTIVE()
		
		PRINTLN("REPLAY IS IN PROGRESS!")
		
		IF Is_Replay_In_Progress()
			iSkipToStage = Get_Replay_Mid_Mission_Stage()
			if g_bShitskipAccepted 
				iSkipToStage++
				if iskipToStage >= 7				
					iskipToStage = 7
				endif
			endif
			b_zskipped 	= false
		elif IS_REPEAT_PLAY_ACTIVE()
			iSkipToStage = 0
		endif		
		// New replay streaming 
		IF IS_REPLAY_IN_PROGRESS()
			VECTOR vSkipToCoord
			FLOAT fSkipToHeading
			GET_SKIP_STAGE_COORD_AND_HEADING(int_to_enum(MSF_MISSION_STAGE_FLAGS,iSkipToStage), vSkipToCoord, fSkipToHeading,g_replay.iReplayInt[2])
			START_REPLAY_SETUP(vSkipToCoord, fSkipToHeading)
		ENDIF	
		
		bDoSkip = TRUE	
	ELSE
		//Grab trev
		IF NOT DOES_ENTITY_EXIST(peds[mpf_trevor].id)		
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
				peds[mpf_trevor].id = g_sTriggerSceneAssets.ped[0]
			ENDIF
		ENDIF
		//Grab mike
		IF NOT DOES_ENTITY_EXIST(peds[mpf_michael].id)		
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
				SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[1], TRUE, TRUE)
				peds[mpf_michael].id = g_sTriggerSceneAssets.ped[1]
			ENDIF
		ENDIF
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0,"Stage 0: Strip club")
		mission_stage = enum_to_int(msf_0_strip_club)					
		Load_asset_Stage(msf_0_strip_club)
		g_replay.iReplayInt[0] = 0
		g_replay.iReplayInt[1] = 0
		g_replay.iReplayInt[2] = 0
	endif
	
	mission_substage = STAGE_ENTRY
	
	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
	ENDIF		
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Initial Cutscene
// -----------------------------------------------------------------------------------------------------------
Proc Run_Init_CutScene()

	#IF IS_DEBUG_BUILD
		IF IS_CUTSCENE_ACTIVE()
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				STOP_CUTSCENE()
				REMOVE_CUTSCENE()
				WHILE IS_CUTSCENE_ACTIVE()
					WAIT(0)
				ENDWHILE
			ENDIF
		ENDIF
	#ENDIF
	
	IF NOT b_cutscene_loaded
		if NOT IS_CUTSCENE_ACTIVE()
		
			if 	GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("bs_1_int", CS_SECTION_1 | CS_SECTION_2  | CS_SECTION_3  | CS_SECTION_4  | CS_SECTION_5  | CS_SECTION_6  | CS_SECTION_7  | CS_SECTION_8 |
																CS_SECTION_9)
			elif GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("bs_1_int",  CS_SECTION_3  | CS_SECTION_4  | CS_SECTION_5  | CS_SECTION_6  | CS_SECTION_7  | CS_SECTION_8 |
																CS_SECTION_9)
			elif GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("bs_1_int",  CS_SECTION_4  | CS_SECTION_5  | CS_SECTION_6  | CS_SECTION_7  | CS_SECTION_8 |
																CS_SECTION_9)
			endif		
			set_peds_CS_OUTFITS()
			WAIT(0)
		endif
		IF HAS_CUTSCENE_LOADED()
			//pre mission setup			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)		
					
			if GET_CURRENT_PLAYER_PED_ENUM()  != CHAR_TREVOR
				if IsEntityAlive(peds[mpf_trevor].id)
					REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_trevor].id,"Trevor",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				else
					REGISTER_ENTITY_FOR_CUTSCENE(null,"Trevor",CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,GET_PLAYER_PED_MODEL(CHAR_TREVOR))
				endif
			endif			
			if GET_CURRENT_PLAYER_PED_ENUM()  != CHAR_MICHAEL
				if IsEntityAlive(peds[mpf_michael].id)
					REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_michael].id,"Michael",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				else
					REGISTER_ENTITY_FOR_CUTSCENE(null,"Michael",CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				endif
			endif			
			if GET_CURRENT_PLAYER_PED_ENUM()  != CHAR_FRANKLIN
				if IsEntityAlive(peds[mpf_franklin].id)
					REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_franklin].id,"Franklin",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				else
					REGISTER_ENTITY_FOR_CUTSCENE(null,"Franklin",CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,GET_PLAYER_PED_MODEL(CHAR_FRANKLIN))
				endif
			endif
			
			REGISTER_ENTITY_FOR_CUTSCENE(null,"Lester",CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,GET_NPC_PED_MODEL(CHAR_LESTER))
			
			if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
				if IS_ENTITY_ATTACHED(g_sTriggerSceneAssets.object[0])
					DETACH_ENTITY(g_sTriggerSceneAssets.object[0],false)
				endif
				REGISTER_ENTITY_FOR_CUTSCENE(g_sTriggerSceneAssets.object[0],"BS_Beer_Bottle",CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
			endif
			
//			if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[1])
//				REGISTER_ENTITY_FOR_CUTSCENE(g_sTriggerSceneAssets.object[1],"stripclub_fridge_door",CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
//			endif
//			
//			if DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[2])
//				REGISTER_ENTITY_FOR_CUTSCENE(g_sTriggerSceneAssets.object[2],"stripclub_fridge",CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
//			endif
			if 	GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
				START_CUTSCENE(CUTSCENE_PLAYER_FP_FLASH_MICHAEL)
			elif GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
				START_CUTSCENE()
			elif GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
				START_CUTSCENE(CUTSCENE_PLAYER_FP_FLASH_MICHAEL)
			endif
			b_cutscene_loaded = TRUE			
		ENDIF	
	ENDIF
	
endproc

// -----------------------------------------------------------------------------------------------------------
//		Mission stages
// -----------------------------------------------------------------------------------------------------------
PROC ST_0_STRIP_CLUB()
Switch mission_substage
	case STAGE_ENTRY
		if IS_REPLAY_IN_PROGRESS()
			mission_substage = 3
		else
			Run_Init_CutScene()
			IF b_cutscene_loaded				
				mission_substage++
			endif
		endif
	break
	case 1
		if IS_CUTSCENE_PLAYING()		
			if IS_REPEAT_PLAY_ACTIVE()
			and IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			endif
			STOP_GAMEPLAY_HINT(TRUE)
			SET_TODS_CUTSCENE_RUNNING(sTimelapse, false,false,2000,true,false)
			SET_CLOCK_TIME(12,0,0)
			btimeReset = true			
			CLEAR_AREA(<< 90.3864, -1278.8374, 28.0896 >>,200,true)
			
			DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<< 90.3864, -1278.8374, 28.0896 >>,20)
			DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_TREVOR_SAVEHOUSE_STRIPCLUB)	
			DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY)
			CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_WEB_HELIPAD_TREVOR_COUNTRY)

			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)

			mission_substage++
		endif
	break
	case 2
		if IS_CUTSCENE_PLAYING()
			if not IsEntityAlive(vehs[mvf_Michael_veh].id)
				CREATE_PLAYER_VEHICLE(vehs[mvf_Michael_veh].id,CHAR_MICHAEL,<< 90.3864, -1278.8374, 28.0896 >>, 98.4832)

			elif not IsEntityAlive(vehs[mvf_trevor_veh].id)
				if CREATE_PLAYER_VEHICLE(vehs[mvf_trevor_veh].id,CHAR_TREVOR,<< 83.4733, -1281.3232, 29.1518 >>, 96.9356)
					SET_ENTITY_COLLISION(vehs[mvf_trevor_veh].id,true)
				endif

			elif not IsEntityAlive(TREV())
			and GET_CURRENT_PLAYER_PED_ENUM()  != CHAR_TREVOR
				if DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor"))
					peds[mpf_trevor].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor"))
				endif		

			elif not IsEntityAlive(MIKE())
			and GET_CURRENT_PLAYER_PED_ENUM()  != CHAR_MICHAEL
				if DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael"))
					peds[mpf_michael].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael"))
				endif		

			elif not IsEntityAlive(FRANK())
			and GET_CURRENT_PLAYER_PED_ENUM()  != CHAR_FRANKLIN
				if DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklin"))
					peds[mpf_franklin].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklin"))
				endif

			elif not IsEntityAlive(LESTER())
				if DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester"))
					peds[mpf_lester].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester"))
				endif

			else
				Hotswap_Menu(false,true,true)
				bcs_mike	= false
				bcs_trev	= false
				bcs_frank	= false
				bcs_lester	= false
				bcs_cam		= false		
				bSwitchedToMike = false
				mission_substage++
			endif
		endif
	break
	case 3		
		if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
		OR NOT IS_CUTSCENE_PLAYING()
			REPLAY_STOP_EVENT()
						
			if GET_CURRENT_PLAYER_PED_ENUM()  != CHAR_MICHAEL
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds,SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
				peds[mpf_trevor].id 	= sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
				peds[mpf_franklin].id 	= sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
				bSwitchedToMike = true
			endif
			peds[mpf_michael].id = player_ped_id()
			SET_ENTITY_COORDS(mike(),<<92.8082, -1282.6401, 28.2537>> )
			set_entity_heading(mike(),38.2535)		
			//TASK_FOLLOW_NAV_MESH_TO_COORD(MIKE(),<<90.8650, -1280.3580, 28.1095>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
			
			SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 800, 38.2535, FALSE)
			FORCE_PED_MOTION_STATE(MIKE(),MS_ON_FOOT_WALK,false,FAUS_CUTSCENE_EXIT)
			

			SET_GAMEPLAY_CAM_RELATIVE_HEADING()	
			bcs_mike = true			
		endif
		if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
		OR NOT IS_CUTSCENE_PLAYING()
			SET_PED_INTO_VEHICLE(TREV(),vehs[mvf_trevor_veh].id)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(trev())

			bcs_trev = true
		endif
		if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")	
		OR NOT IS_CUTSCENE_PLAYING()
			SET_ENTITY_COORDS(frank(),<< 94.5331, -1277.5521, 28.1446 >>)
			SET_ENTITY_HEADING(FRANK(), 15.2302)
			TASK_ENTER_VEHICLE(frank(),vehs[mvf_Michael_veh].id,DEFAULT_TIME_NEVER_WARP,VS_FRONT_RIGHT,PEDMOVEBLENDRATIO_WALK)
			SET_PED_MIN_MOVE_BLEND_RATIO(Frank(),PEDMOVEBLENDRATIO_WALK)
			FORCE_PED_MOTION_STATE(Frank(),MS_ON_FOOT_WALK,false,FAUS_CUTSCENE_EXIT)

			bcs_frank = true
		endif
		if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lester")
		OR NOT IS_CUTSCENE_PLAYING()
			SET_PED_INTO_VEHICLE(lester(),vehs[mvf_trevor_veh].id,VS_FRONT_RIGHT)
			SET_PED_CONFIG_FLAG(LESTER(),PCF_ForcedToUseSpecificGroupSeatIndex,true)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(lester())
			REQUEST_CLIP_SET("move_lester_CaneUp")

			bcs_lester = true
		endif		
		if CAN_SET_EXIT_STATE_FOR_CAMERA()
		OR NOT IS_CUTSCENE_PLAYING()

			bcs_cam = true
		endif
		
		if (bcs_mike and bcs_trev and bcs_frank and bcs_lester and bcs_cam)
		or IS_REPLAY_IN_PROGRESS()
		or b_zskipped					
			//This needs to be here as we swap mission triggers around this mission.
			//Without it the game will memory fault as we pass this mission. 100% unavoidable PT.
			MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_FINALE_1) 			
			
			//Load_Asset_Recording(sAssetData,001,"FH1RECMIKE")
			//Load_Asset_Recording(sAssetData,003,"FH1REC")
			//Load_Asset_Recording(sAssetData,004,"FH1REC")	
			REQUEST_VEHICLE_RECORDING(1,"FH1RECMIKE")
			REQUEST_VEHICLE_RECORDING(4,"FH1REC")
			REQUEST_VEHICLE_RECORDING(3,"FH1REC")
			//michael start			
			CLEAR_AREA(<< 90.3864, -1278.8374, 28.0896 >>,20,true)
			
			IF NOT IS_SCREEN_FADED_OUT()
				SET_PLAYER_CONTROL(player_id(),true)	
			ENDIF
			PAUSE_CLOCK(false)
			DISPLAY_RADAR(true)
			display_hud(true)
			b_zskipped 	= false
			mission_substage++	
		endif
	break	
	case 4 
		if HAS_VEHICLE_RECORDING_BEEN_LOADED(3,"FH1REC")	
			//convo setup	
			ADD_PED_FOR_DIALOGUE(convo_struct,0,trev(),"TREVOR",true)
			ADD_PED_FOR_DIALOGUE(convo_struct,1,mike(),"MICHAEL",true)
			ADD_PED_FOR_DIALOGUE(convo_struct,2,frank(),"FRANKLIN",true)
			ADD_PED_FOR_DIALOGUE(convo_struct,3,lester(),"LESTER",true)
			give_all_peds_earpiece()			

			SET_PED_MOVEMENT_CLIPSET(lester(),"move_lester_CaneUp")
			SET_PED_WEAPON_MOVEMENT_CLIPSET(lester(),"move_lester_CaneUp")
			SET_PED_CONFIG_FLAG(frank(),PCF_GetOutBurningVehicle,false)	
			SET_PED_CONFIG_FLAG(lester(),PCF_GetOutBurningVehicle,false)	
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(TREV(),true)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(lester(),true)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_trevor_veh].id,true)
			
			//relationship
			if DOES_ENTITY_EXIST(trev())
				SET_PED_RELATIONSHIP_GROUP_HASH(trev(),rel_buddy)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(trev(),true)
				
			endif
			if DOES_ENTITY_EXIST(frank())
				SET_PED_RELATIONSHIP_GROUP_HASH(frank(),rel_buddy)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(frank(),true)
			endif
			if DOES_ENTITY_EXIST(lester())
				SET_PED_RELATIONSHIP_GROUP_HASH(lester(),rel_buddy)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(lester(),true)
			endif
			SET_WANTED_LEVEL_MULTIPLIER(0.7)
			if bSwitchedToMike
				ANIMPOSTFX_PLAY("SwitchSceneMichael", 1000, FALSE)
				PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
			endif
			mission_substage++
		endif
	break
	case 5	
		IF IS_SCREEN_FADED_OUT()
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			SET_PED_MIN_MOVE_BLEND_RATIO(mike(),PEDMOVE_WALK)
			FORCE_PED_MOTION_STATE(mike(),MS_ON_FOOT_WALK,true,FAUS_CUTSCENE_EXIT)
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			
		ENDIF
		if IS_SCREEN_FADED_IN()			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			idelay = get_game_timer()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)
			mission_substage++
		endif
		
		
		#IF IS_DEBUG_BUILD
		IF bDEBUG_IsInIsolatedDebugMode
			SCRIPT_ASSERT("[MF] In Isolated Switch Cam Debug Mode. All Gameplay Logic is disabled.  YOU SHOULD NOT BE SEEING THIS ON ANY SUBMITTED SCRIPTS.")
		ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		WHILE bDEBUG_IsInIsolatedDebugMode
			//CPRINTLN(DEBUG_MISSION,"SCRIPT IS BLOCKED!")
			HANDLE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
			UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCamMichaelToTrevorInHeli)
			UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCamTrevorInHeliToMichael)
			WAIT(0)
		ENDWHILE
		#ENDIF
	break
	case 6
		
		//---------next stage load--------
				Load_asset_Stage(msf_1_font_of_bank) //load next stage
		//--------------------------------	
		iDelay = GET_GAME_TIMER()
		mission_substage++						
	
	break	
	case 7	
		if not sCamDetails.bRun 
			if player_ped_id() = mike()		
				//Sort out trev and lester
				IF NOT IS_PED_INJURED(trev())
				AND NOT IS_ENTITY_DEAD(vehs[mvf_trevor_veh].id)
					if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) != PERFORMING_TASK		
						TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(trev(),vehs[mvf_trevor_veh].id,<< 1799.9630, 3321.9102, 40.8868 >>,55,DRIVINGMODE_AVOIDCARS | DF_ForceJoinInRoadDirection ,5)												
					endif
				ENDIF
				
				if not IS_AUDIO_SCENE_ACTIVE("BS_1_drive_START")
					if IS_PED_IN_ANY_VEHICLE(player_ped_id())
						START_AUDIO_SCENE("BS_1_drive_START")
					endif
				endif
								
				if IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData,<<-9.34495, -741.32111, 43.15848>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,true,frank(),NULL,NULL,vehs[mvf_Michael_veh].id,"FH1_BANK","FH1_FRCAR","","","","CMN_GENGETINY","CMN_GENGETBCKY",FALSE,true)										
				or (DOES_BLIP_EXIST(sLocatesData.LocationBlip) And IS_ENTITY_IN_ANGLED_AREA(MIKE(),<<-2.55928, -743.97137, 45>>,<<-14.34047, -739.44629, 43.15934>>,4.46))
				and not GET_IS_TASK_ACTIVE(MIKE(),CODE_TASK_EXIT_VEHICLE)	
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehs[mvf_Michael_veh].id)	
					if IS_AUDIO_SCENE_ACTIVE("BS_1_drive_START")						
						STOP_AUDIO_SCENE("BS_1_drive_START")						
					endif
					bDialoguePlayed = false
					bHotSwap = false	
					IF IS_THIS_PRINT_BEING_DISPLAYED("FH1_BANK")
						CLEAR_PRINTS()
					ENDIF
					mission_substage = MIKE_AT_SCOPE_POINT
				endif
				
			elif player_ped_id() = trev()
				
				IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData,vAIRFIELD,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,true,lester(),NULL,NULL,vehs[mvf_trevor_veh].id,"","","","","","","",FALSE,true,true)															
					
				if not b_displayed_get_heli	
				and DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				and IS_SAFE_TO_DISPLAY_GODTEXT()
					print_now("FH1_AIR",DEFAULT_GOD_TEXT_TIME,1)
					b_displayed_get_heli = true
				endif
				
				IF NOT IS_PED_INJURED(mike())
				AND NOT IS_ENTITY_DEAD(vehs[mvf_Michael_veh].id)
					if GET_SCRIPT_TASK_STATUS(mike(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) != PERFORMING_TASK
						TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(mike(),vehs[mvf_Michael_veh].id,vFRONT_BANK,8,DRIVINGMODE_AVOIDCARS,2)																						
					endif
				ENDIF
								
				//mike is at the front of the bank
				if IS_ENTITY_AT_COORD(mike(),vFRONT_BANK,<<4,4,2>>)	
				or (not Is_clock_time_less_than_or_equal(12,35)					
				and not IS_ENTITY_ON_SCREEN(vehs[mvf_Michael_veh].id)
				and IS_ENTITY_OCCLUDED(vehs[mvf_Michael_veh].id)
				and GET_GAME_TIMER() - iWindowTimer > 15000
				and GET_GAME_TIMER() - iLastSwapTimer > 10000)
				
					if eInterruptstage = INTERRUPT_NONE
						eInterruptstage = INTERRUPT_STORE_CURRENT
					endif
					if eInterruptstage = INTERRUPTOR_SAFE
						if not IS_MESSAGE_BEING_DISPLAYED()
							if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_TRVFBS",CONV_PRIORITY_MEDIUM)
								eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 10")
//								if not IS_ENTITY_ON_SCREEN(vehs[mvf_Michael_veh].id)
//								and IS_ENTITY_OCCLUDED(vehs[mvf_Michael_veh].id)
//									SET_ENTITY_COORDS(vehs[mvf_Michael_veh].id,vFRONT_BANK)
//									SET_ENTITY_HEADING(vehs[mvf_Michael_veh].id,fHEADING_FRONT)
//									SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_Michael_veh].id)	
//								endif				
								SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Stage 1: Front of bank")
								mission_substage = STAGE_ENTRY
					 			mission_set_stage(msf_1_font_of_bank)
								iWindowTimer = get_game_timer()
							endif
						endif
					endif
				endif
				
			endif
		endif
	break
	
	// mike front of bank run front of bank
	case MIKE_AT_SCOPE_POINT
		IF IS_THIS_PRINT_BEING_DISPLAYED("FH1_BANK")
			CLEAR_PRINTS()
		ENDIF
		if IS_VEHICLE_ALMOST_STOPPED(vehs[mvf_Michael_veh].id)	
		and IS_PED_IN_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
		and not GET_IS_TASK_ACTIVE(trev(),CODE_TASK_EXIT_VEHICLE)
			IF IS_THIS_PRINT_BEING_DISPLAYED("FH1_BANK")
				CLEAR_PRINTS()
			ENDIF
			CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)	
			KILL_FACE_TO_FACE_CONVERSATION()			
			mission_substage = STAGE_ENTRY
	 		mission_set_stage(msf_1_font_of_bank)	
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Stage 1: Front of bank")
		else
			IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData,<<-9.34495, -741.32111, 43.15848>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,true,frank(),NULL,NULL,vehs[mvf_Michael_veh].id,"","FH1_FRCAR","","","","CMN_GENGETINY","CMN_GENGETBCKY",FALSE,true)										
			BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehs[mvf_Michael_veh].id)
		endif
	break
endswitch
ENDPROC
PROC ST_1_FRONT_OF_BANK()
	
	IF mission_substage > 0
		if player_ped_id() = mike()
//			DISABLE_CINEMATIC_BONNET_CAMERA_THIS_UPDATE()
		ENDIF
	ENDIF
	
	switch mission_substage

		case stage_entry	
			if IS_SCREEN_FADED_OUT()		
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			endif
			if not sCamDetails.bRun
				if player_ped_id() = TREV()
									
					IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData,vAIRFIELD,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,true,lester(),NULL,NULL,vehs[mvf_trevor_veh].id,"","","","","","","",FALSE,true,true)															
					
					if not b_displayed_get_heli	
					and not IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
					and DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					and IS_SAFE_TO_DISPLAY_GODTEXT()
						print_now("FH1_AIR",DEFAULT_GOD_TEXT_TIME,1)
						b_displayed_get_heli = true
					endif
					
					//mike and frank are at the front of the bank whilst player is still trev *they missed it*
					if not Is_clock_time_less_than_or_equal(13,15)
					and GET_GAME_TIMER() - iWindowTimer > 15000
					and GET_GAME_TIMER() - iLastSwapTimer > 5000
						if eInterruptstage = INTERRUPT_NONE
							eInterruptstage = INTERRUPT_STORE_CURRENT
						endif
						if eInterruptstage = INTERRUPTOR_SAFE
							if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_TRVFBE",CONV_PRIORITY_MEDIUM)
								eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 11")
								//---------next stage load--------
									Load_asset_Stage(msf_2_back_of_bank) //load next stage
								//--------------------------------								
								g_replay.iReplayInt[0] = 1
								mission_substage = 4
								iWindowTimer = get_game_timer()
							endif
						endif
					endif		
					
				elif player_ped_id() = mike()	
					if IS_REPLAY_IN_PROGRESS()
					and g_replayMissionStage = 1
					and g_replay.iReplayInt[0] = 1
					and not g_bShitskipAccepted
					and not b_zskipped
						// ------------PLAYER HAS RESET FROM THIS STAGE------------
						iFrontCS_stage = 2
						mission_substage = 3// jump to after front of bank CS	
						REMOVE_VEHICLE_RECORDING(001,"FH1RECMIKE")
					Else
						//***switch needs to set car in the correct place***		
												
						if not bFCSanimsLoaded
							if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(MIKE(),vFRONT_BANK) < 60
								Load_Asset_AnimDict(sAssetData,"missbigscore1_mcs1")
								bFCSanimsLoaded = true
							endif
						else
							if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(MIKE(),vFRONT_BANK) > 90 
								Unload_Asset_Anim_Dict(sAssetData,"missbigscore1_mcs1")
								bFCSanimsLoaded = false
							endif
						endif
						
						if IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData,<<-9.34495, -741.32111, 43.15848>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,true,frank(),NULL,NULL,vehs[mvf_Michael_veh].id,"FH1_BANK","FH1_FRCAR","","","","CMN_GENGETINY","CMN_GENGETBCKY",FALSE,true)										
							CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
							BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehs[mvf_Michael_veh].id)	
							KILL_FACE_TO_FACE_CONVERSATION()
							if IS_AUDIO_SCENE_ACTIVE("BS_1_drive_START")						
								STOP_AUDIO_SCENE("BS_1_drive_START")						
							endif
							
							IF IS_THIS_PRINT_BEING_DISPLAYED("FH1_BANK")
								CLEAR_PRINTS()
							ENDIF
							
							bDialoguePlayed = false
							bHotSwap = false
							mission_substage++
						ELSE
							IF (DOES_BLIP_EXIST(sLocatesData.LocationBlip) 
							And IS_ENTITY_IN_ANGLED_AREA(MIKE(),<<-2.55928, -743.97137, 45>>,<<-14.34047, -739.44629, 43.15934>>,4.46))
							and not GET_IS_TASK_ACTIVE(MIKE(),CODE_TASK_EXIT_VEHICLE)	
								CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
								BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehs[mvf_Michael_veh].id)	
								KILL_FACE_TO_FACE_CONVERSATION()
								if IS_AUDIO_SCENE_ACTIVE("BS_1_drive_START")						
									STOP_AUDIO_SCENE("BS_1_drive_START")						
								endif
								
								IF IS_THIS_PRINT_BEING_DISPLAYED("FH1_BANK")
									CLEAR_PRINTS()
								ENDIF
								
								bDialoguePlayed = false
								bHotSwap = false
								mission_substage++
							endif
						ENDIF
					endif
				endif	
			endif
		break
		case 1	
			IF IS_THIS_PRINT_BEING_DISPLAYED("FH1_BANK")
				CLEAR_PRINTS()
			ENDIF
			if IS_VEHICLE_ALMOST_STOPPED(vehs[mvf_Michael_veh].id)	
			and IS_PED_IN_VEHICLE(MIKE(),vehs[mvf_Michael_veh].id)
			and not GET_IS_TASK_ACTIVE(trev(),CODE_TASK_EXIT_VEHICLE)
			and HAS_ANIM_DICT_LOADED("missbigscore1_mcs1")
				CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)	
				if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()		
					
					
					
					START_AUDIO_SCENE("BS_1_BANK_CHECK_OUT")
					REMOVE_VEHICLE_RECORDING(001,"FH1RECMIKE")
					mission_substage++
				else	
					KILL_FACE_TO_FACE_CONVERSATION()
				endif
				SET_PLAYER_CONTROL(player_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON | SPC_DONT_STOP_OTHER_CARS_AROUND_PLAYER)
			else
				IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData,<<-9.34495, -741.32111, 43.15848>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,true,frank(),NULL,NULL,vehs[mvf_Michael_veh].id,"","FH1_FRCAR","","","","CMN_GENGETINY","CMN_GENGETBCKY",FALSE,true)										
				BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehs[mvf_Michael_veh].id)
			endif
		break
		case 2
			SET_VEHICLE_RADIO_ENABLED(vehs[mvf_Michael_veh].id,false)
			STOP_FIRE_IN_RANGE(vFRONT_BANK,100)
			SPECIAL_ABILITY_DEACTIVATE(player_id())				
			//misc			
			iDelay = GET_GAME_TIMER()
			iFrontCS_stage = 0
			mission_substage++			
		break		
		case 3
			if Front_bank_CS()	
				if IS_AUDIO_SCENE_ACTIVE("BS_1_BANK_CHECK_OUT")						
					STOP_AUDIO_SCENE("BS_1_BANK_CHECK_OUT")						
				endif
				if IS_AUDIO_SCENE_ACTIVE("BS_1_BANK_HINT_CAM")						
					STOP_AUDIO_SCENE("BS_1_BANK_HINT_CAM")						
				endif
				//player
				SET_PLAYER_CONTROL(player_id(),true)	
				SET_VEH_RADIO_STATION(vehs[mvf_Michael_veh].id, "OFF") 
				SET_VEHICLE_RADIO_ENABLED(vehs[mvf_Michael_veh].id,true)
				//---------next stage load--------
					Load_asset_Stage(msf_2_back_of_bank) //load next stage
				//--------------------------------
				g_replay.iReplayInt[0] = 1
				bstopshake = true
				iDelay = GET_GAME_TIMER()
				Hotswap_Menu(false,true,false)
				bHotSwap = true	
				eMike_dialogue = M_di_TO_BACK
				eDialogue_stateM = DI_START_CONDITION_MIKE
				iDialogueTimer = GET_GAME_TIMER()
				mission_substage++				
			endif			
		break
		case 4 //* dialogue dependent on stage number *
			if not sCamDetails.bRun 
				if player_ped_id() = TREV()				
					if heli_switch = TO_HELI					
						IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData,vAIRFIELD,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,true,lester(),NULL,NULL,vehs[mvf_trevor_veh].id,"","","","","","","",FALSE,true,true)															
						if not b_displayed_get_heli	
						and DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						and IS_SAFE_TO_DISPLAY_GODTEXT()
							print_now("FH1_AIR",DEFAULT_GOD_TEXT_TIME,1)
							b_displayed_get_heli = true
						endif
					elif heli_switch = TO_TRUCKS
						IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData,vTRUCK_START,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,FALSE,NULL,lester(),NULL,vehs[mvf_helicopter].id,"","","","","","FH1_HELI","FH1_BKHELI",FALSE,true)										
					endif
					
					IF NOT IS_PED_INJURED(mike())
					AND NOT IS_ENTITY_DEAD(vehs[mvf_Michael_veh].id)
						if GET_SCRIPT_TASK_STATUS(mike(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) != PERFORMING_TASK
							TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(mike(),vehs[mvf_Michael_veh].id,vBACK_BANK,8,DRIVINGMODE_AVOIDCARS,2)																
						endif
					ENDIF
					
			// if mike at bank | (time over & not on screen) | (player at airfield) & no message on screen
					if IS_ENTITY_AT_COORD(mike(),vBACK_BANK,<<3,3,2>>)					
					
					or (not Is_clock_time_less_than_or_equal(13,45)	
					and not IS_ENTITY_ON_SCREEN(vehs[mvf_Michael_veh].id) 
					and IS_ENTITY_OCCLUDED(vehs[mvf_Michael_veh].id)
					and GET_GAME_TIMER() - iLastSwapTimer > 9000 and GET_GAME_TIMER() - iWindowTimer > 15000)					
					
					or (DOES_BLIP_EXIST(sLocatesData.LocationBlip)And GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(TREV(),vAIRFIELD) < 100 and GET_GAME_TIMER() - iLastSwapTimer > 9000)
					
						if eInterruptstage = INTERRUPT_NONE
							eInterruptstage = INTERRUPT_STORE_CURRENT
						endif
						if eInterruptstage = INTERRUPTOR_SAFE				
							if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_TRVBB",CONV_PRIORITY_MEDIUM)
								eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 12")
								if not IS_ENTITY_ON_SCREEN(vehs[mvf_Michael_veh].id)
								and IS_ENTITY_OCCLUDED(vehs[mvf_Michael_veh].id)
									SET_ENTITY_COORDS(vehs[mvf_Michael_veh].id,vBACK_BANK)
									SET_ENTITY_HEADING(vehs[mvf_Michael_veh].id,fHEADING_BACK)
									SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_Michael_veh].id)	
								endif
								REQUEST_CUTSCENE("BSS_1_MCS_2")
								SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_OFF) //#BUG:1542524
									
								mission_substage = stage_entry
								mission_set_Stage(msf_2_back_of_bank)
								SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Stage 2: Back of bank")
							endif
						endif
					endif 
					
				elif player_ped_id() = mike()		
				
					//Sort out trev and lester
					if heli_switch = TO_HELI
						IF NOT IS_PED_INJURED(TREV())
						AND NOT IS_ENTITY_DEAD(vehs[mvf_trevor_veh].id)
							if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) != PERFORMING_TASK			
								TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(trev(),vehs[mvf_trevor_veh].id,<< 1799.9630, 3321.9102, 40.8868 >>,70,DRIVINGMODE_AVOIDCARS,5)								
							endif
						ENDIF
					elif heli_switch = TO_TRUCKS	
						IF NOT IS_PED_INJURED(TREV())
						AND NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
							if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
							and IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
							and IS_PED_IN_VEHICLE(lester(),vehs[mvf_helicopter].id)
								TASK_VEHICLE_MISSION_COORS_TARGET(trev(),vehs[mvf_helicopter].id,<< 1527.6018, -591.3962, 336.5655 >>,MISSION_CIRCLE,10,DRIVINGMODE_AVOIDCARS,4,-1)					
							endif
						ENDIF
					endif
					if not IS_AUDIO_SCENE_ACTIVE("BS_1_DRIVE_AROUND_BACK")
						START_AUDIO_SCENE("BS_1_DRIVE_AROUND_BACK")
					endif					
					
					if IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData,vBACK_BANK,<<2.5,2.5,LOCATE_SIZE_HEIGHT>>,true,frank(),NULL,NULL,vehs[mvf_Michael_veh].id,"FH1_BENTR","FH1_FRCAR","","","","","FH1_BKCAR",FALSE,true)										
						BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehs[mvf_Michael_veh].id)				
						bHotSwap = false
						mission_substage = MIKE_AT_SCOPE_POINT
					endif
											
				endif
			endif
		break
				
		case MIKE_AT_SCOPE_POINT
			if IS_VEHICLE_ALMOST_STOPPED(vehs[mvf_Michael_veh].id)
			
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 2.0, REPLAY_IMPORTANCE_HIGHEST)
			
				SET_PLAYER_CONTROL(player_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON)
				mission_substage = stage_entry
				mission_set_Stage(msf_2_back_of_bank)
				KILL_ANY_CONVERSATION()
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Stage 2: Back of bank")
			endif
		break
				
	endswitch	
	
ENDPROC

PROC ST_2_BACK_OF_BANK()
	switch mission_substage
		case STAGE_ENTRY
			if IS_SCREEN_FADED_OUT()			
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			endif
			if not sCamDetails.bRun 
				if player_ped_id() = trev()
					
					Mission_Set_Stage(msf_3_get_in_position)
					mission_substage= STAGE_ENTRY					
					
				elif player_ped_id() = mike()	
//					if g_replay.iReplayInt[1] = 1					
//						//skip cutscene as it's replay and has played before
//						Point_Gameplay_cam_at_coord(238)			
//						SET_PLAYER_CONTROL(player_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON)	
//						bIgnoreNoPlayerControl = TRUE
//						SET_PED_INTO_VEHICLE(mike(),vehs[mvf_Michael_veh].id)
//						SET_PED_INTO_VEHICLE(frank(),vehs[mvf_Michael_veh].id,VS_FRONT_RIGHT)
//						FORCE_PED_AI_AND_ANIMATION_UPDATE(mike())
//						FORCE_PED_AI_AND_ANIMATION_UPDATE(frank())
//						//guard 
//						SET_CURRENT_PED_WEAPON(peds[mpf_guard].id,WEAPONTYPE_ADVANCEDRIFLE,true)
//						//vehs
//						SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_Michael_veh].id)
//						//interior 
//						
//						if interior_carpark != null
//							UNPIN_INTERIOR(interior_carpark)
//						endif
//						
//						mission_substage = 4					
//					else										
						interior_carpark = GET_INTERIOR_AT_COORDS(<<-77.30131, -678.88495, 33.32175>>)	
						IF IS_VALID_INTERIOR(interior_carpark)
							PIN_INTERIOR_IN_MEMORY(interior_carpark)
						endif
						if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							bHotSwap = false
//							Load_Asset_Recording(001,"FH1UBER")			
							//start security Guard anim.
							IF DOES_ENTITY_EXIST(peds[mpf_guard].id)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_guard].id,true)
							ENDIF
							SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
							bcs_mike	= false
							bcs_trev	= false
							bcs_frank	= false
							bcs_cam 	= false
							SET_INTERIOR_CAPPED(INTERIOR_DT1_03_CARPARK, FALSE) 
							mission_substage++
						endif
//					endif					
				endif
			endif
		break
		case 1
			if HAS_CUTSCENE_LOADED()
				if interior_carpark = null
				or IS_INTERIOR_READY(interior_carpark)
//					KILL_ANY_CONVERSATION()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					SET_VEHICLE_RADIO_ENABLED(vehs[mvf_Michael_veh].id,false)
					
					if IS_AUDIO_SCENE_ACTIVE("BS_1_DRIVE_AROUND_BACK")
						STOP_AUDIO_SCENE("BS_1_DRIVE_AROUND_BACK")
					endif
					
					REGISTER_ENTITY_FOR_CUTSCENE(vehs[mvf_Michael_veh].id,"Michaels_car",CU_ANIMATE_EXISTING_SCRIPT_ENTITY) //mike car
					REGISTER_ENTITY_FOR_CUTSCENE(frank(),"Franklin",CU_ANIMATE_EXISTING_SCRIPT_ENTITY) //frank
					REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_guard].id,"security_guard",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)//guard				
					
					set_peds_CS_OUTFITS()
					CLEAR_TIMECYCLE_MODIFIER()
					STOP_SOUND(camZoom)
					STOP_SOUND(camhum)
					
					IF DOES_SWITCH_CAM_EXIST(scsSwitchCamTrevorInHeliToMichael)
						DESTROY_SWITCH_CAM(scsSwitchCamTrevorInHeliToMichael)
					ENDIF	
					
					//[MF] making sure that the assets for the switch cam are requested properly
					if heli_switch = TO_HELI
					and vTrevPreSwitchLoc.y < 2600 //[MF] ensure we're not too far past the overpass where Trevor takes a tinkle.
						eSwitchCamState = SWITCH_CAM_REQUEST_ASSETS
						HANDLE_SWITCH_CAM_MICHAEL_TO_TREVOR_IN_TRUCK(scsSwitchCamMichaelToTrevorInTruck)
					ENDIF

					START_CUTSCENE(CUTSCENE_SUPPRESS_FP_TRANSITION_FLASH)
										
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					mission_substage++
				endif
			endif
		break
		case 2
			if IS_CUTSCENE_PLAYING()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				REMOVE_VEHICLE_WINDOW(vehs[mvf_Michael_veh].id,SC_WINDOW_FRONT_RIGHT)
				CLEAR_AREA(vBACK_BANK,500,true)
				bcs_mike	= false
				bcs_trev	= false
				bcs_frank	= false
				bcs_sec		= false
				bcs_mikecar = false
				bcs_cam		= false
				bCustomSwitchCamRendering_HeliToCar = FALSE
				Point_Gameplay_cam_at_coord(238)
				mission_substage++
			endif
		break
		case 3		
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael",PLAYER_ZERO)
				SET_PED_INTO_VEHICLE(mike(),vehs[mvf_Michael_veh].id)				
				
				bcs_mike = true
			else
				Point_Gameplay_cam_at_coord(238)
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin",PLAYER_ONE)
				SET_PED_INTO_VEHICLE(frank(),vehs[mvf_Michael_veh].id,VS_FRONT_RIGHT)

				bcs_frank = true
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("security_guard",S_M_M_ARMOURED_01)

				bcs_sec = true
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_car",TAILGATER)
				SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehs[mvf_Michael_veh].id)

				bcs_mikecar = true
			endif
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
			
			ENDIF
			
			if bcs_mike
			and bcs_frank
			and bcs_sec
			and bcs_mikecar			
			
				REPLAY_STOP_EVENT()
				//REPLAY_RECORD_BACK_FOR_TIME(0.0, 5.0, REPLAY_IMPORTANCE_HIGHEST) - stop recording trevor peeing
			
				SET_PLAYER_CONTROL(player_id(),false, SPC_DONT_STOP_OTHER_CARS_AROUND_PLAYER)						
				eMike_dialogue = M_di_NONE
				eDialogue_stateM = DI_START_CONDITION_MIKE
				//guard 
				SET_CURRENT_PED_WEAPON(peds[mpf_guard].id,WEAPONTYPE_ADVANCEDRIFLE,true)
				//interior 
				if interior_carpark != null
					UNPIN_INTERIOR(interior_carpark)
				endif
				SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_DT1_03_CARPARK, TRUE)

				g_replay.iReplayInt[1] = 1	
				vTrevPreSwitchLoc = GET_ENTITY_COORDS(peds[mpf_trevor].id)
				
				if eTREV_Dialogue = T_di_FILL1
					PRINTLN("@@@@@@@@@@@@@ if eTREV_Dialogue = T_di_FILL1 @@@@@@@@@@@@@@@@@@")
					b_convo_paused = false
					lbl_TREVswitchInterrupt = ""
					lbl_TREVswitchroot = ""		
					b_TREVSpeechSwitchStopped = false
					eTREV_Dialogue = T_di_BRAD
				endif
				
				if heli_switch = TO_HELI
				and vTrevPreSwitchLoc.y < 2600 //[MF] ensure we're not too far past the overpass where Trevor takes a tinkle.
					bcustomswitch_mike_to_trev_inCar = true					
					eSwitchCamState = SWITCH_CAM_SETUP_SPLINE_1
					HANDLE_SWITCH_CAM_MICHAEL_TO_TREVOR_IN_TRUCK(scsSwitchCamMichaelToTrevorInTruck)
				ELIF heli_switch = TO_TRUCKS
				AND IS_PED_IN_VEHICLE( peds[mpf_trevor].id, Vehs[mvf_helicopter].id)
				AND bMichaelCarToTrevHeli_UseCustomSwitch = TRUE
					bcustomswitch_mike_to_trev_inHeli = TRUE
					eSwitchCamState = SWITCH_CAM_SETUP_SPLINE_1
					HANDLE_SWITCH_CAM_MICHAEL_TO_TREVOR_IN_HELI(scsSwitchCamMichaelToTrevorInHeli)
					bMichaelCarToTrevHeli_UseCustomSwitch = FALSE
				endif
				IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Michael_veh].id)
					SET_VEHICLE_RADIO_ENABLED(vehs[mvf_Michael_veh].id,true)
				ENDIF
				sCamDetails.bRun = TRUE
				Do_forced_hotswap(SELECTOR_PED_TREVOR)	
				
				mission_substage++				
			endif
		break
		case 4
			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				bcs_mike	= false
				bcs_trev	= false
				bcs_frank	= false
				bcs_sec		= false
				bcs_mikecar = false
				bcs_cam		= false
				Hotswap_Menu(false,true,false)
				bHotSwap = true
				REMOVE_CUTSCENE()
				mission_substage++	
			endif
		break
		case 5
			if not sCamDetails.bRun 
				if player_ped_id() = trev()				
					SET_MULTIHEAD_SAFE(FALSE)	//B* 2166335: Disable multihead blinders
					bHotSwap = false												
					Mission_Set_Stage(msf_3_get_in_position)
					mission_substage = STAGE_ENTRY	
					if not IS_PLAYER_CONTROL_ON(player_id())
						SET_PLAYER_CONTROL(player_id(),true)
					endif
					bIgnoreNoPlayerControl = FALSE
//				elif player_ped_id() = mike()  //[MF] Switch now happens automatically so the below logic is no longer needed.	
				
//					bcustomswitch_mike_to_trev_inHeli = TRUE
//					eSwitchCamState = SWITCH_CAM_SETUP_SPLINE_1
//					HANDLE_SWITCH_CAM_MICHAEL_TO_TREVOR_IN_HELI(scsSwitchCamMichaelToTrevorInHeli)
//					sCamDetails.bRun = TRUE
//					Do_forced_hotswap(SELECTOR_PED_TREVOR)				
//					if IS_PLAYER_CONTROL_ON(player_id())
//						SET_PLAYER_CONTROL(player_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON | SPC_DONT_STOP_OTHER_CARS_AROUND_PLAYER)
//					endif
//					
//					if IS_SCREEN_FADED_IN()
//					and not IS_CUTSCENE_PLAYING()
//					and IS_SAFE_TO_DISPLAY_GODTEXT()				
//						Print_now("FH1_SWITCH",DEFAULT_GOD_TEXT_TIME,1)
//					endif
				endif
			endif			
		break	
	endswitch		
ENDPROC
PROC ST_3_GET_IN_POSITION()
	
	if	bHotSwap = true
		if IsEntityAlive(vehs[mvf_helicopter].id)
			if (DOES_BLIP_EXIST(sLocatesData.LocationBlip) And GET_DISTANCE_BETWEEN_COORDS(vTRUCK_START,GET_ENTITY_COORDS(vehs[mvf_helicopter].id)) < 1000)	
				bMissedBackCS = true
			endif
		endif	
		
		if (not Is_clock_time_less_than_or_equal(14,30) and GET_GAME_TIMER() - iLastSwapTimer > 29000 and GET_GAME_TIMER() - iWindowTimer > 20000)
		or bMissedBackCS
			if not sCamDetails.bRun 
			and player_ped_id() = trev()
				if eInterruptstage = INTERRUPT_NONE
					eInterruptstage = INTERRUPT_STORE_CURRENT
				endif
				if eInterruptstage = INTERRUPTOR_SAFE
					IF IS_VEHICLE_DRIVEABLE(vehs[mvf_trevor_veh].id)
						IF IS_PED_IN_VEHICLE(TREV(),vehs[mvf_trevor_veh].id)
						OR bHasTrevSwitchConvoPlayed
							g_replay.iReplayInt[1] = 1
							eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 13")
							
							REMOVE_CUTSCENE()
							REMOVE_VEHICLE_RECORDING(3,"FH1REC")
							REMOVE_VEHICLE_RECORDING(4,"FH1REC")
							bHotSwap = false
						//endif
						ELSE
							if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_TMBB_H",CONV_PRIORITY_MEDIUM)
								g_replay.iReplayInt[1] = 1
								eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 17")
								
								REMOVE_CUTSCENE()
								REMOVE_VEHICLE_RECORDING(3,"FH1REC")
								REMOVE_VEHICLE_RECORDING(4,"FH1REC")
								bHotSwap = false
							endif
						ENDIF
					ENDIF
				endif
			endif
		endif
	endif
	
	IF NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
	AND NOT IS_PED_INJURED(trev())
	AND NOT IS_PED_INJURED(LESTER())
		IF IS_THIS_CONVERSATION_PLAYING("FH1_TALK4")
		and IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
		and IS_PED_IN_VEHICLE(LESTER(),vehs[mvf_helicopter].id)
			PRINTLN("@@@@@@@@@ KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE - FH1_TALK4 @@@@@@@@@")
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		ENDIF
	ENDIF
	
	switch mission_substage
		case STAGE_ENTRY
			if IS_SCREEN_FADED_OUT()			
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			endif		
			if not IS_PLAYER_CONTROL_ON(player_id())
				SET_PLAYER_CONTROL(player_id(),true)
			endif
			
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
			
			mission_substage++
		break
		case 1
			if not sCamDetails.bRun 
				if player_ped_id() = MIKE()
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					mission_substage= STAGE_ENTRY					
					Mission_Set_Stage(msf_2_back_of_bank)
					
				elif player_ped_id() = TREV()
					switch heli_switch
						case TO_HELI
						
							if not b_displayed_get_heli	
							and IS_SAFE_TO_DISPLAY_GODTEXT()
							and DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								print_now("FH1_AIR",DEFAULT_GOD_TEXT_TIME,1)
								b_displayed_get_heli = true
							endif
							
							if	IS_PLAYER_AT_LOCATION_WITH_BUDDIES_ANY_MEANS(sLocatesData,vAIRFIELD,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,true,lester(),NULL,NULL,"","","","","",FALSE,true,true)															
							or (DOES_BLIP_EXIST(sLocatesData.LocationBlip)And  GET_DISTANCE_BETWEEN_COORDS(vAIRFIELD,GET_ENTITY_COORDS(TREV())) < 15)						
								if IS_PED_IN_VEHICLE(TREV(),vehs[mvf_trevor_veh].id)
									BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehs[mvf_trevor_veh].id)
								endif
								
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
								
								CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
								CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)								
								FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id,false)
								SET_MAX_WANTED_LEVEL(2)
								SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3,"Stage 3: Get in Position")
								heli_switch = TO_TRUCKS
								b_displayed_get_heli = false
								bDialoguePlayed = false
								bPrisonWarning 	= false
							ENDIF
							
						break
						case TO_TRUCKS
							if IsEntityAlive(vehs[mvf_helicopter].id)
								
							
								if IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
									g_replay.iReplayInt[1] = 1
									stop_mission_audio_scenes(6)//6 excludes stopping on fly to trucks
									if not IS_AUDIO_SCENE_ACTIVE("BS_1_FLY_TO_TRUCKS")
										START_AUDIO_SCENE("BS_1_FLY_TO_TRUCKS")
										
										REPLAY_RECORD_BACK_FOR_TIME(2.0, 10.0, REPLAY_IMPORTANCE_HIGH)
										
									endif
								endif	
								
								if not bPrisonWarning
								and IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
								and IS_PED_IN_VEHICLE(LESTER(),vehs[mvf_helicopter].id)
								and GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(trev(),<<1692.16467, 2615.41187, 58.89342>>) < 400
									IF IS_POINT_VISIBLE(<<1692.16467, 2615.41187, 58.89342>>,50,500)
										if eInterruptstage = INTERRUPT_NONE
											eInterruptstage = INTERRUPT_STORE_CURRENT
										endif
										if eInterruptstage = INTERRUPTOR_SAFE
											if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_PRISWRN",CONV_PRIORITY_MEDIUM)
												eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 14")
												bPrisonWarning = true
											endif
										endif
									ENDIF
								endif
								
								if not b_displayed_get_heli	
								and IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id,true)
								and IS_PED_IN_VEHICLE(LESTER(),vehs[mvf_helicopter].id,true)
								and DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								and IS_SAFE_TO_DISPLAY_GODTEXT()
									if eInterruptstage = INTERRUPT_NONE
										eInterruptstage = INTERRUPT_STORE_CURRENT
									endif
									if eInterruptstage = INTERRUPTOR_SAFE
										print_now("FH1_FLY",DEFAULT_GOD_TEXT_TIME,1)
										eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 15")
										b_displayed_get_heli = true								
									endif
								endif
								
								if not bDialoguePlayed
								and not IS_PED_IN_VEHICLE(trev(),vehs[mvf_trevor_veh].id)
								and not IS_PED_IN_VEHICLE(LESTER(),vehs[mvf_trevor_veh].id)
								and not IS_PED_IN_VEHICLE(LESTER(),vehs[mvf_helicopter].id)
								and GET_DISTANCE_BETWEEN_ENTITIES(trev(),vehs[mvf_helicopter].id) < GET_DISTANCE_BETWEEN_ENTITIES(trev(),vehs[mvf_trevor_veh].id)
								and GET_DISTANCE_BETWEEN_ENTITIES(trev(),LESTER()) < 20
									if eInterruptstage = INTERRUPT_NONE
										eInterruptstage = INTERRUPT_STORE_CURRENT
									endif
									if eInterruptstage = INTERRUPTOR_SAFE
										if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_LESSLOW",CONV_PRIORITY_MEDIUM)
											TASK_LOOK_AT_ENTITY(TREV(),LESTER(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_YAW_LIMIT)
											eInterruptstage = INTERRUPT_RESTART_READY PRINTLN("@@@@@@@@@@@@ eInterruptstage = INTERRUPT_RESTART_READY 16")
											bDialoguePlayed = true
										endif
									endif
								endif
								
								if not bHeliStageLoaded
								And GET_DISTANCE_BETWEEN_COORDS(vTRUCK_START,GET_ENTITY_COORDS(vehs[mvf_helicopter].id)) < 1000
									if g_replay.iReplayInt[1] = 1
										//---------next stage load--------
											Load_asset_Stage(msf_4_heli_follow_truck) //load next stage
										//--------------------------------
										bHeliStageLoaded =  true
									endif
								endif							
							
								if IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData,vTRUCK_START,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,FALSE,NULL,lester(),NULL,vehs[mvf_helicopter].id,"","","","","","FH1_HELI","FH1_BKHELI",FALSE,true)										
								or (DOES_BLIP_EXIST(sLocatesData.LocationBlip)And GET_DISTANCE_BETWEEN_COORDS(vTRUCK_START,GET_ENTITY_COORDS(vehs[mvf_helicopter].id)) < 380)				
									IF HAS_MODEL_LOADED(stockade)
									AND HAS_MODEL_LOADED(S_M_M_ARMOURED_01)
										vehs[mvf_security_truck].id = CREATE_VEHICLE(stockade,<< 1025.5339, -1756.0054, 35.2748 >>, 81.9993)				
										peds[mpf_securityDriver].id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_security_truck].id,PEDTYPE_MISSION,S_M_M_ARMOURED_01)
										SET_PED_COMPONENT_VARIATION(peds[mpf_securityDriver].id, PED_COMP_HEAD, 0, 0)
										SET_ENTITY_LOD_DIST(vehs[mvf_security_truck].id,350)									
										CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
										CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)										
										bTruckSkipForward = false
										mission_substage = stage_entry
										DISPLAY_RADAR(false)
										DISPLAY_HUD(false)
										
										REPLAY_RECORD_BACK_FOR_TIME(2.0, 10.0, REPLAY_IMPORTANCE_HIGH)
																				
										mission_set_stage(msf_4_heli_follow_truck)
									ENDIF
								endif
							endif
						break
					endswitch
				endif
			endif
		break
	endswitch
ENDPROC


PROC ST_4_HELI_FOLLOW_TRUCK()
	
	OBJECT_INDEX objWeapon

	switch mission_substage
		case stage_entry
			bWentInBridge1 = FALSE
			bWentOutBridge1 = FALSE
			bWentInBridge2 = FALSE
			bWentOutBridge2 = FALSE
			bSetUnderTheBridgeStat = FALSE
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4,"Stage 4: Follow truck")		
			
			ADD_PED_FOR_DIALOGUE(convo_struct,0,trev(),"TREVOR")	
			ADD_PED_FOR_DIALOGUE(convo_struct,3,lester(),"LESTER")		
			Unload_Asset_Anim_Dict(sAssetData,"missbigscore1guard_wait_rifle")
			REMOVE_VEHICLE_RECORDING(003,"FH1REC")
			REMOVE_VEHICLE_RECORDING(004,"FH1REC")
			REMOVE_VEHICLE_RECORDING(002,"FH1RECMIKE")
			SAFE_RELEASE_VEHICLE(vehs[mvf_trevor_veh].id)
			REMOVE_PTFX_ASSET()
			SET_MODEL_AS_NO_LONGER_NEEDED(BODHI2)
			give_all_peds_earpiece()

			SET_ENTITY_INVINCIBLE(vehs[mvf_security_truck].id,true)
			SET_VEHICLE_RADIO_ENABLED(vehs[mvf_security_truck].id,false)	
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_securityDriver].id,true)
			
			SET_ROADS_IN_AREA(<<1184.57813, -1801.74841, 25>>,<<780.50098, -1454.51013, 37>>,false)			
			SET_ROADS_IN_AREA(<<859.14117, -1563.11743, 24>>,<<242.78966, -829.41626, 33>>,false)
			SET_ROADS_IN_AREA(<<535.71149, -552.73450, 24>>,<<35.41522, -1156.01208, 33>>,false)
			SET_ROADS_IN_AREA(<<-159.27182, -539.10400, 28>>,<<416.92444, -832.99457, 41.5>>,false)
			iRoadBlock_1 =	ADD_ROAD_NODE_SPEED_ZONE(<<824.89844, -1748.84338, 28.48285>>,29,0)
			iRoadBlock_2 =	ADD_ROAD_NODE_SPEED_ZONE(<<796.60425, -1435.00281, 26.20493>>,23.8,0) 
			iRoadBlock_3 =	ADD_ROAD_NODE_SPEED_ZONE(<<541.35822, -1431.70422, 28.34233>>,21,0)
			iRoadBlock_4 =	ADD_ROAD_NODE_SPEED_ZONE(<<499.58060, -1132.15405, 28.45514>>,18,0) // after bridge
			iRoadBlock_5 =	ADD_ROAD_NODE_SPEED_ZONE(<<398.24036, -1048.06909, 28.44339>>,22,0) 
			iRoadBlock_6 =	ADD_ROAD_NODE_SPEED_ZONE(<<403.37341, -955.11713, 28.44834>>,20,0)
			iRoadBlock_7 =	ADD_ROAD_NODE_SPEED_ZONE(<<406.04382, -853.28693, 28.33914>>,25,0)// problem zone
			iRoadBlock_8 =	ADD_ROAD_NODE_SPEED_ZONE(<<354.64478, -667.05859, 28.33901>>,27,0)//tunnel enterance		
				
			bfirstwarningCL = true
			bFailTimerStarted = false
			
			//set truck to full speed recording
			fPlaybackSpeed = 1
			
			SET_VEHICLE_POPULATION_BUDGET(1)
			SET_PED_POPULATION_BUDGET(1)
			
			SET_CINEMATIC_BUTTON_ACTIVE(false)
			
			bLesterCam = false
			if not DOES_ENTITY_EXIST(objCam)
				objcam = CREATE_OBJECT(PROP_PAP_CAMERA_01,GET_ENTITY_COORDS(lester()))
				IF IsEntityAlive(vehs[mvf_helicopter].id)
					SET_ENTITY_NO_COLLISION_ENTITY(objcam,vehs[mvf_helicopter].id,FALSE)
				ENDIF
				battachedCam = false
			endif
			IF NOT IS_ENTITY_DEAD(TREV())
				IF NOT IS_ENTITY_VISIBLE(TREV())
					SET_ENTITY_VISIBLE(TREV(),true)
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD(LESTER())
				IF NOT IS_ENTITY_VISIBLE(lester())
					SET_ENTITY_VISIBLE(lester(),true)
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
				IF NOT IS_ENTITY_VISIBLE(vehs[mvf_helicopter].id)
					SET_ENTITY_VISIBLE(vehs[mvf_helicopter].id,true)
				ENDIF
			ENDIF
			RENDER_SCRIPT_CAMS(false,false)
			REPLAY_RECORD_BACK_FOR_TIME(0, 5, REPLAY_IMPORTANCE_HIGHEST)
			//DISPLAY_RADAR(true)
			//DISPLAY_HUD(true)
			iSFstage 			= 0
			i_tunnle_check 		= 0 	//stat helper
			ihintstage 			= 0 	// hint cam 
			mission_substage++			
		BREAK
		case 1			
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)	
			and IsEntityAlive(vehs[mvf_security_truck].id)
			and IsEntityAlive(vehs[mvf_security_truck_fol].id)	
				add_entity_to_audio_MIX_GROUP(vehs[mvf_security_truck].id,"BS_1_TRUCKS_Group")
				add_entity_to_audio_MIX_GROUP(vehs[mvf_security_truck_fol].id,"BS_1_TRUCKS_Group")
				
				//Ensure security trucks are high detail
				IF NOT IS_VEHICLE_HIGH_DETAIL( vehs[mvf_security_truck].id )
				AND NOT IS_VEHICLE_HIGH_DETAIL( vehs[mvf_security_truck_fol].id )
					REQUEST_VEHICLE_HIGH_DETAIL_MODEL( vehs[mvf_security_truck].id )
					REQUEST_VEHICLE_HIGH_DETAIL_MODEL( vehs[mvf_security_truck_fol].id )
				ENDIF
				
				Unload_Asset_Model(sAssetData,STOCKADE)					
				IF IS_SCREEN_FADED_OUT()			
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ENDIF
				cameraIndex = CREATE_CAMERA()
				ATTACH_CAM_TO_ENTITY(cameraIndex,vehs[mvf_helicopter].id,<<-6.32,15,6>>,false)
				POINT_CAM_AT_ENTITY(cameraIndex,vehs[mvf_security_truck].id,<<0,-15,0>>)
				
				SET_CAM_ACTIVE(cameraIndex,true)
				if bTruckSkipForward
					SET_CAM_FOV(cameraIndex,31)
					RENDER_SCRIPT_CAMS(true,false)
					SET_PLAYER_CONTROL(player_id(),false)
					SET_ENTITY_VELOCITY(vehs[mvf_helicopter].id,<<0,-50,0>>)
					TASK_HELI_CHASE(TREV(),vehs[mvf_security_truck].id,<<0,0,80>>)	
					iSPOTTEDTIME = 4000
				else
					SET_CAM_FOV(cameraIndex,27)
					RENDER_SCRIPT_CAMS(true,true,4000,false)
					vector vhelipos
					vhelipos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehs[mvf_security_truck].id,<<0,0,45>>)
					TASK_HELI_MISSION(TREV(),vehs[mvf_helicopter].id,null,null,vhelipos,MISSION_GOTO,35,-1,-1,-1,-1,-1)
					iSPOTTEDTIME = 5200
				endif
				iDelay = GET_GAME_TIMER()
				DISPLAY_RADAR(false)
				DISPLAY_HUD(false)
				TRIGGER_MUSIC_EVENT("FH1_TRUCKS")
				bDialoguePlayed = false
				bPushInToFirstPerson = FALSE
				mission_substage++					
				HANG_UP_AND_PUT_AWAY_PHONE()	
				if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
					KILL_ANY_CONVERSATION()
				endif
			endif	
		break
		case 2			
			if not bDialoguePlayed 
			and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_GOTCON",CONV_PRIORITY_LOW)	
					stop_mission_audio_scenes()
					if not IS_AUDIO_SCENE_ACTIVE("BS_1_TRUCKS_SPOTTED")
						START_AUDIO_SCENE("BS_1_TRUCKS_SPOTTED")
					endif
					bDialoguePlayed = true
				endif
			endif
			IF GET_GAME_TIMER() - IDELAY > ISPOTTEDTIME - 300
			AND NOT bPushInToFirstPerson
			AND GET_FOLLOW_VEHICLE_CAM_VIEW_MODE() = CAM_VIEW_MODE_FIRST_PERSON
				ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
				PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
				bPushInToFirstPerson = TRUE
			ENDIF
			if get_game_timer() - iDelay > iSPOTTEDTIME
				SET_PLAYER_CONTROL(player_id(),true)
				CLEAR_PED_TASKS(TREV())
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				RENDER_SCRIPT_CAMS(false,true,4000,false)			// make sure both timings change together	
				DISPLAY_RADAR(true)
				DISPLAY_HUD(true)
				SET_PLAYER_CONTROL(player_id(),true)		
				iLESTERCAM_stage = 0
				iDelay = GET_GAME_TIMER()
				mission_substage++				
			else
				SUPPRESS_HD_MAP_STREAMING_THIS_FRAME()
				OVERRIDE_LODSCALE_THIS_FRAME(2.0)
				REMAP_LODSCALE_RANGE_THIS_FRAME(1.7,4.7,1.0,1.8)  
			endif
		break
		case 3
			DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()
			if get_game_timer() - idelay > 4000					// make sure both timings change together	
				if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_ROUTE",CONV_PRIORITY_LOW)					
					PRINT_HELP("FH1_FILM")
					Unload_Asset_Model(sAssetData,STOCKADE)
					Load_Asset_Model(sAssetData,police3)
					mission_substage++				
				endif
			endif
		break
		case 4	//* control hint dependant on stage number*		
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if GET_TIME_POSITION_IN_RECORDING(vehs[mvf_security_truck].id) > 20500	
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_FILL",CONV_PRIORITY_LOW)
							vehs[mvf_pol1].id = CREATE_VEHICLE(police3,<<407.8519, -984.2310, 28.2662>>, 230.4461)
							vehs[mvf_pol2].id = CREATE_VEHICLE(police3,<<408.2209, -997.6844, 28.2664>>, 229.8704)
							Unload_Asset_Model(sAssetData,police3)
							mission_substage++			
						endif
					else
						KILL_ANY_CONVERSATION()
					endif
				endif				
			endif	
		break
		case 5
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if GET_TIME_POSITION_IN_RECORDING(vehs[mvf_security_truck].id) > 69700 
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						if 	CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_FILL2",CONV_PRIORITY_LOW)
							iLESTERCAM_stage = 8
							mission_substage++
						endif
					else
						KILL_ANY_CONVERSATION()
					endif
				endif				
			endif
		break 
		case 6
			if GET_TIME_POSITION_IN_RECORDING(vehs[mvf_security_truck].id) > 118000
				CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_CHATTER",CONV_PRIORITY_LOW)
					//tunnel interior pinning
				interior_tunnel = GET_INTERIOR_AT_COORDS(<< 224.4949, -608.8183, 27.8671 >>)
				IF IS_VALID_INTERIOR(interior_tunnel)
					PIN_INTERIOR_IN_MEMORY(interior_tunnel)					
				endif
				SAFE_RELEASE_VEHICLE(vehs[mvf_pol1].id)
				SAFE_RELEASE_VEHICLE(vehs[mvf_pol2].id)
				mission_substage++
			endif
		break
		case 7
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if(fCurrentPlaybackTime > 124000 and fCurrentPlaybackTime < 125000)
					iLESTERCAM_stage = 10
				endif
			endif
			
			if IS_ENTITY_AT_COORD(peds[mpf_securityDriver].id,<< 292.5266, -643.8066, 28.3005 >>,<<20,20,20>>)	
				
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
				
				CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_Tunnel",CONV_PRIORITY_VERY_HIGH)	
				bTunnel = true
				iLESTERCAM_stage = 12
				if IS_GAMEPLAY_HINT_ACTIVE()
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
					SET_GAMEPLAY_COORD_HINT(<<79.28018, -559.11414, 31.16214>>,-1)
				endif
				stop_mission_audio_scenes()
				if not IS_AUDIO_SCENE_ACTIVE("BS_1_TRUCKS_ENTER_TUNNEL")
					START_AUDIO_SCENE("BS_1_TRUCKS_ENTER_TUNNEL")
				endif
				SET_ENTITY_COORDS(vehs[mvf_Michael_veh].id,vBACK_BANK)
				SET_ENTITY_HEADING(vehs[mvf_Michael_veh].id,fHEADING_BACK)
				REMOVE_CUTSCENE()
				mission_substage++
			endif
		break
		case 8 
			if IS_ENTITY_AT_COORD(peds[mpf_securityDriver].id,<< 89.9054, -563.3521, 30.6528 >>,<<20,20,20>>)	
			
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
			
				//out of the tunnel	
				bTunnel = false
				request_cutscene("BSS_1_MCS_3")
				//unpin interior
				if interior_tunnel != null
					UNPIN_INTERIOR(interior_tunnel)
				endif
				interior_carpark = GET_INTERIOR_AT_COORDS(<<-73.4359, -680.0825, 32.7495>>)	
				IF IS_VALID_INTERIOR(interior_carpark)
					PIN_INTERIOR_IN_MEMORY(interior_carpark)
				endif
				stop_mission_audio_scenes()
				if bLesterCam
					if not IS_AUDIO_SCENE_ACTIVE("BS_1_TRUCKS_HINT_CAM")
						START_AUDIO_SCENE("BS_1_TRUCKS_HINT_CAM")
					endif
				else
					if not IS_AUDIO_SCENE_ACTIVE("BS_1_TRUCKS_SPOTTED")
						START_AUDIO_SCENE("BS_1_TRUCKS_SPOTTED")
					endif
				endif
				bDialoguePlayed = false
				mission_substage++
			endif
		break
		case 9	//*using to dialogue on re-appearing trucks*	
			CREATE_FORCED_OBJECT(<< -84.3858, -670.8411, 35.1694 >>,5,PROP_BOLLARD_02A,true)
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if GET_TIME_POSITION_IN_RECORDING(vehs[mvf_security_truck].id) >185500
					if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_SECCHECK",CONV_PRIORITY_VERY_HIGH)						
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
						mission_substage++
					endif
				endif
			endif
		break
		case 10	//van at gate//*being used to kill the cam*	
			if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
				if GET_TIME_POSITION_IN_RECORDING(vehs[mvf_security_truck].id) >198000		
					bLesterCam = false
					CLEAR_PED_SECONDARY_TASK(lester())
					IF NOT IS_ENTITY_DEAD(TREV())
						IF NOT IS_ENTITY_VISIBLE(TREV())
							SET_ENTITY_VISIBLE(TREV(),true)
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_DEAD(LESTER())
						IF NOT IS_ENTITY_VISIBLE(lester())
							SET_ENTITY_VISIBLE(lester(),true)
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
						IF NOT IS_ENTITY_VISIBLE(vehs[mvf_helicopter].id)
							SET_ENTITY_VISIBLE(vehs[mvf_helicopter].id,true)
						ENDIF
					ENDIF
					RENDER_SCRIPT_CAMS(false,false)
					DISPLAY_RADAR(true)
					DISPLAY_HUD(true)
					STOP_SOUND(camZoom)
					STOP_SOUND(camhum)
					CLEAR_TIMECYCLE_MODIFIER()
					SET_INTERIOR_CAPPED(INTERIOR_DT1_03_CARPARK, FALSE)
					fPlaybackSpeed = 0.1
					SET_VEHICLE_POPULATION_BUDGET(3)
					SET_PED_POPULATION_BUDGET(3)
					SWITCH_ALL_ROADS_BACK_TO_ORIGINAL()
					//---------next stage load--------
							load_Asset_Model(sAssetData,S_M_M_ARMOURED_01)
					//--------------------------------	
					mission_substage++
				endif
			endif
		break
		case 11
			bool debugtest1
			bool debugtest2
			bool debugtest3
			bool debugtest4
			bool debugtest5
			
			debugtest1 = false
			debugtest2 = false
			debugtest3 = false
			debugtest4 = false
			debugtest5 = false
			
			if HAS_CUTSCENE_LOADED()
				debugtest1 = true 
				CPRINTLN(DEBUG_MISSION,"HAS_CUTSCENE_LOADED true")
			endif			
			if IS_INTERIOR_READY(interior_carpark)
				debugtest2 = true
				CPRINTLN(DEBUG_MISSION,"IS_INTERIOR_READY true")
			endif
			if HAS_MODEL_LOADED(S_M_M_ARMOURED_01)
				debugtest3 = true
				CPRINTLN(DEBUG_MISSION,"HAS_MODEL_LOADED true")
			endif			
			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				debugtest4 = true
				CPRINTLN(DEBUG_MISSION,"not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED true")
			endif
			
			IF IS_VEHICLE_DRIVEABLE(vehs[mvf_helicopter].id)
				IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), vehs[mvf_helicopter].id)
					debugtest5 = true
				ENDIF
			ENDIF
			
			if  debugtest1
			and debugtest2
			and debugtest3
			and debugtest4
			and debugtest5
		//---------next stage load--------
				Load_asset_Stage(msf_5_CONSTRUCTION_HOLE) //load next stage
				load_Asset_Model(sAssetData,S_M_M_ARMOURED_01)
				

		//--------------------------------					
				//delete entities
				objWeapon = CREATE_WEAPON_OBJECT_FROM_CURRENT_PED_WEAPON_WITH_COMPONENTS(PEDS[mpf_guard].id)
//				CREATE_WEAPON_OBJECT_FROM_CURRENT_PED_WEAPON_WITH_COMPONENTS
				REGISTER_ENTITY_FOR_CUTSCENE(objWeapon, "security_guard_gun", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(PEDS[mpf_guard].id,"security_guard",CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(peds[mpf_securityDriver].id,"Casey",CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(vehs[mvf_security_truck].id,"security_truck",CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				//REGISTER_ENTITY_FOR_CUTSCENE(vehs[mvf_security_truck_fol].id,"security_truck_fol",CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
							
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehs[mvf_security_truck].id)		
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehs[mvf_security_truck_fol].id)	
				
				Security_gate_setup(BUILDINGSTATE_DESTROYED)
				HANG_UP_AND_PUT_AWAY_PHONE()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(true)
				set_peds_CS_OUTFITS()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				start_cutscene()
				SET_FRONTEND_RADIO_ACTIVE(FALSE)
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				mission_substage++
			endif
		break
		case 12		
			IF IS_VEHICLE_DRIVEABLE(vehs[mvf_helicopter].id)
				SET_FORCE_HD_VEHICLE(vehs[mvf_helicopter].id, TRUE)
			ENDIF
			
			if IS_CUTSCENE_PLAYING()
				stop_mission_audio_scenes()
				TRIGGER_MUSIC_EVENT("FH1_ONION86")
				
				IF NOT IS_PED_INJURED(peds[mpf_securityDriver].id)
					PRINTSTRING("set casey collision ") PRINTVECTOR(GET_ENTITY_COORDS(peds[mpf_securityDriver].id)) PRINTNL()
					SET_ENTITY_COLLISION(peds[mpf_securityDriver].id, FALSE)
				ENDIF				
				
				if DOES_ENTITY_EXIST(vehs[mvf_security_truck_fol].id)
					DELETE_VEHICLE(vehs[mvf_security_truck_fol].id)
				endif	
				clear_area(<< -84.3858, -670.8411, 35.1694 >>,200,true)
				//heli	
				SET_ENTITY_COORDS(vehs[mvf_helicopter].id,<<-76.4238, -528.6735, 87.5 >>)
				SET_ENTITY_HEADING(vehs[mvf_helicopter].id, 176.7705)				
				FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id,true)	
				SET_HELI_BLADES_FULL_SPEED(vehs[mvf_helicopter].id)
				SET_PED_INTO_VEHICLE(trev(),vehs[mvf_helicopter].id)
				SET_PED_INTO_VEHICLE(lester(),vehs[mvf_helicopter].id,VS_BACK_LEFT)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(lester())		
				interior_constuction = GET_INTERIOR_AT_COORDS(<< 12.8829, -634.9265, 15.0884  >>)			
				IF IS_VALID_INTERIOR(interior_constuction)
					PIN_INTERIOR_IN_MEMORY(interior_constuction)
				endif				
				KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				mission_substage++
			endif
		break	
		case 13	
			IF IS_VEHICLE_DRIVEABLE(vehs[mvf_helicopter].id)
				SET_FORCE_HD_VEHICLE(vehs[mvf_helicopter].id, TRUE)
			ENDIF
			
			if CAN_SET_EXIT_STATE_FOR_CAMERA()
				bcs_cam = true
			endif
			
//			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("security_guard_gun")
//				GIVE_WEAPON_OBJECT_TO_PED(objWeapon, PEDS[mpf_guard].id)
//			ENDIF

			if bcs_cam
			
				REPLAY_STOP_EVENT()
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(false)
				//frank and mike
				SET_PED_INTO_VEHICLE(mike(),vehs[mvf_Michael_veh].id)
				set_ped_into_vehicle(frank(),vehs[mvf_Michael_veh].id,VS_FRONT_RIGHT)
				force_peD_ai_and_animation_update(mike())
				FORCE_PED_AI_AND_ANIMATION_UPDATE(frank())
				SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_Michael_veh].id)
				//heli
				FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id,false)
				SET_HELI_BLADES_FULL_SPEED(vehs[mvf_helicopter].id)
				SET_VEHICLE_FORWARD_SPEED(vehs[mvf_helicopter].id,1)
				SET_PED_INTO_VEHICLE(lester(),vehs[mvf_helicopter].id,VS_BACK_LEFT)
				SET_PED_INTO_VEHICLE(trev(),vehs[mvf_helicopter].id)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(trev())				
								
				//gate
				Security_gate_setup()
				Point_Gameplay_cam_at_coord(get_entity_heading(player_ped_id()))
				if interior_carpark != null
					UNPIN_INTERIOR(interior_carpark)
				endif
				NEW_LOAD_SCENE_STOP()
				TRIGGER_MUSIC_EVENT("FH1_TRUCKS_2")
				mission_substage++								
			endif				
		break
		case 14
			IF IS_VEHICLE_DRIVEABLE(vehs[mvf_helicopter].id)
				SET_FORCE_HD_VEHICLE(vehs[mvf_helicopter].id, TRUE)
			ENDIF
			
			IF NOT IS_CUTSCENE_ACTIVE()
				SET_FRONTEND_RADIO_ACTIVE(TRUE)
				SET_INTERIOR_CAPPED_ON_EXIT(INTERIOR_DT1_03_CARPARK, TRUE)
				mission_substage = STAGE_ENTRY
				mission_set_stage(msf_5_CONSTRUCTION_HOLE)
			ENDIF
		break	
	endswitch
	
	clear_players_task_on_control_input(script_task_vehicle_MISSION)
	
	//speed up stopped behind traffic
	if mission_substage > STAGE_ENTRY and mission_substage < 9  //van at gate
		if fCurrentPlaybackTime < 10000 //start
			fPlaybackSpeed = 0.75
			CREATE_ALL_WAITING_UBER_CARS()
		elif fCurrentPlaybackTime > 10000 and fCurrentPlaybackTime < 20000	
			fPlaybackSpeed = 0.85		
		elif fCurrentPlaybackTime > 20000 and fCurrentPlaybackTime < 30000	
			fPlaybackSpeed = 1.0		
		elif fCurrentPlaybackTime > 107000 and fCurrentPlaybackTime < 116000	
			fPlaybackSpeed = 1.4
		elif fCurrentPlaybackTime > 131500 and fCurrentPlaybackTime < 139000
			fPlaybackSpeed = 1.4
		elif fCurrentPlaybackTime > 152000 and fCurrentPlaybackTime < 162000
			fPlaybackSpeed = 0.8
		elif fCurrentPlaybackTime > 162000 and fCurrentPlaybackTime < 168000
			vector vTrucksOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehs[mvf_security_truck].id, GET_ENTITY_COORDS(vehs[mvf_helicopter].id))
			if vtrucksOffset.y < -1.0
			and vtrucksOffset.z > 10
				fPlaybackSpeed = 0.2
			else
				fPlaybackSpeed = 1.0
			endif	
		elif fCurrentPlaybackTime > 168000
			fPlaybackSpeed = 1.0
		else
			fPlaybackSpeed = 1.2
		endif
		
	endif
	
	SET_VEHICLE_CEILING_HEIGHT(vehs[mvf_helicopter].id,420.0) //#bug:1498268
	
	if wSkip
		SET_UBER_PLAYBACK_TO_TIME_NOW(vehs[mvf_security_truck].id,165000)
		SET_ENTITY_COORDS(vehs[mvf_helicopter].id,<< 89.9054, -563.3521, 100.6528 >>)
		wSkip = false
		wSkippedUber = true
		mission_substage = 8
	endif
	
	//update UBER recording
	Manage_UBER_rec(vehs[mvf_security_truck].id,"FH1UBER")	
	//trucks only damaged by the player.
	if DOES_ENTITY_EXIST(vehs[mvf_security_truck].id)
	 if IS_VEHICLE_DRIVEABLE(vehs[mvf_security_truck].id)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_security_truck].id,true)
	 endif
	endif
	if DOES_ENTITY_EXIST(vehs[mvf_security_truck_fol].id)
	 if IS_VEHICLE_DRIVEABLE(vehs[mvf_security_truck_fol].id)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_security_truck_fol].id,true)
	 endif
	endif
	
	if mission_substage < 11
		if IsEntityAlive(lester())
		and IsEntityAlive(vehs[mvf_security_truck].id)
			update_lester_anims(GET_ENTITY_COORDS(vehs[mvf_security_truck].id))
		endif
		manage_lester_Cam()	
	endif

	//dialgoue for coming back out the tunnel
	if mission_substage = 9 
	and IS_ENTITY_ON_SCREEN(vehs[mvf_security_truck_fol].id)
	and not IS_ENTITY_OCCLUDED(vehs[mvf_security_truck_fol].id)
	and not bDialoguePlayed
	and fCurrentPlaybackTime > 171500
		if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_Tunnel2",CONV_PRIORITY_MEDIUM)
			REPLAY_RECORD_BACK_FOR_TIME(4.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
			bDialoguePlayed	= true 
		endif	
	endif
	
//distance check			
	
	if mission_substage < 11
	and mission_stage = enum_to_int(msf_4_heli_follow_truck)
	and DOES_ENTITY_EXIST(peds[mpf_securityDriver].id)
	
		fDist = vdist(GET_ENTITY_COORDS(peds[mpf_securityDriver].id),GET_ENTITY_COORDS(player_ped_id()))	
		float fDistMaxLimit = 550		
		if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
			float fcurrentTime = GET_TIME_POSITION_IN_RECORDING(vehs[mvf_security_truck].id)
			if  fcurrentTime  <=  20000
				fDistMaxLimit = 550
				
			elif fcurrentTime  >  20000 and fcurrentTime <= 150000 //tunnel
				fDistMaxLimit = 450
				
			elif fcurrentTime > 150000
				fDistMaxLimit = 350
			endif
			
			float heightDif
			vector vtruckCoord
			vector vheliCoord
			 vtruckCoord = GET_ENTITY_COORDS(vehs[mvf_security_truck].id)
			 vheliCoord = GET_ENTITY_COORDS(vehs[mvf_helicopter].id)
			 
			heightDif = vtruckCoord.z - vheliCoord.z
			if heightDif < 0
				heightDif *= -1
			endif
			if not wSkippedUber			
				//------------too far away-----------------
				if fdist > fDistMaxLimit	
					if not bFailTimerStarted
						INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FH1_PERFECT_DISTANCE)
						PRINT_NOW("FH1_FRWARN",DEFAULT_GOD_TEXT_TIME,1)
						bFailTimerStarted = true	
					endif	
									
					if get_game_timer() - iInFailTimer > 10500
					and not IS_AMBIENT_SPEECH_PLAYING(LESTER())
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(LESTER(),"FINH1_BDAA","Lester",SPEECH_PARAMS_FORCE)
						mission_failed(mff_lost_truck)						
					elif get_game_timer() - iInFailTimer > 3000
						if not IS_AMBIENT_SPEECH_PLAYING(LESTER())
						and get_game_timer() - iGetCloser > 5000
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(LESTER(),"FINH1_BFAA","Lester",SPEECH_PARAMS_FORCE)
							iGetCloser = get_Game_timer()
						endif
					endif
				//------------too close-------------------	
				elif fdist < 30 
					if bTunnel = false 		
					or heightDif < 9
						if fCurrentPlaybackTime < 162000
						or fCurrentPlaybackTime > 171500
						or heightDif < 9
							if not IS_AMBIENT_SPEECH_PLAYING(LESTER())
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(LESTER(),"FINH1_BCAA","Lester",SPEECH_PARAMS_FORCE)	
								if IsEntityAlive(vehs[mvf_security_truck].id)
									if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck].id)
										STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_security_truck].id)
									endif
								endif
								if IsEntityAlive(vehs[mvf_security_truck].id)
									if IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_security_truck_fol].id)
										STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_security_truck_fol].id)
									endif
								endif
								mission_failed(mff_too_close_fail)
							endif
						endif
					endif
				elif fdist < 60
					if bTunnel = false
						if fCurrentPlaybackTime < 162000
						or fCurrentPlaybackTime > 169000
							if bfirstwarningCL
								INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FH1_PERFECT_DISTANCE)
								PRINT_NOW("FH1_CLWARN",DEFAULT_GOD_TEXT_TIME,1)
								iInFailTimer = GET_GAME_TIMER()
								bfirstwarningCL = false			
							else
								if get_game_timer() - iInFailTimer > 10000
								and not IS_AMBIENT_SPEECH_PLAYING(LESTER())
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(LESTER(),"FINH1_BEAA","Lester",SPEECH_PARAMS_FORCE)
									iInFailTimer = GET_GAME_TIMER()
								endif
							endif	
						endif
					endif
				//------------ NORMAL ------------	
				else
					iInFailTimer = GET_GAME_TIMER()
					bFailTimerStarted = false
					bfirstwarningCL = true	
				endif
			endif				
		endif		
		
		if DOES_ENTITY_EXIST(vehs[mvf_security_truck].id)
			CLEAR_ROOM_FOR_ENTITY(vehs[mvf_security_truck].id)
			SET_ENTITY_LOD_DIST(vehs[mvf_security_truck].id,ROUND(fDistMaxLimit))
		endif
		if DOES_ENTITY_EXIST(vehs[mvf_security_truck_fol].id)
			CLEAR_ROOM_FOR_ENTITY(vehs[mvf_security_truck_fol].id)
			SET_ENTITY_LOD_DIST(vehs[mvf_security_truck_fol].id,ROUND(fDistMaxLimit))					
		endif
	endif		
	
	//head tracking 
	if IsEntityAlive(TREV())
	and IsEntityAlive(LESTER())
	and IsEntityAlive(vehs[mvf_security_truck].id)
		if not IS_PED_HEADTRACKING_ENTITY(trev(),vehs[mvf_security_truck].id)
			TASK_LOOK_AT_ENTITY(TREV(),vehs[mvf_security_truck].id,-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)	
		endif
		if not IS_PED_HEADTRACKING_ENTITY(lester(),vehs[mvf_security_truck].id)
			TASK_LOOK_AT_ENTITY(lester(),vehs[mvf_security_truck].id,-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)	
		endif
	endif
	
	
	if IsEntityAlive(vehs[mvf_helicopter].id)
		switch i_tunnle_check
		case 0
			if IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_helicopter].id,<<291.67627, -653.67816, 28.37552>>,<<297.37866, -639.35156, 33.76371>>,10)
				i_tunnle_check++
			endif
		break
		case 1
			if IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_helicopter].id,<<192.95865, -607.28772, 28.54596>>,<<202.67386, -592.18152, 33.89706>>,10)
				i_tunnle_check++
			endif
		break
		case 2
			if IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_helicopter].id,<<124.56654, -583.90997, 30.61664>>,<<130.59349, -568.08618, 37.65457>>,10)
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(FH1_THROUGH_TUNNEL)
				i_tunnle_check++
			endif
		break
		endswitch
	endif
	
	MANAGE_BRIDGES_ACHIEVEMENT()
	
ENDPROC

BOOL bFindHoleGodText
BOOL bBeenToHole

PROC ST_5_CONSTRUCTION_HOLE()
	switch mission_substage
		case stage_entry
			SETTIMERA(0)
			IF IS_SCREEN_FADED_OUT()
				TRIGGER_MUSIC_EVENT("AH1_HOLE_RESTART")
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			SET_CINEMATIC_BUTTON_ACTIVE(false)			
			KILL_ANY_CONVERSATION()								
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5,"Stage 5: Construction hole")			
			
			bBeenToHole         = FALSE
			bFindHoleGodText	= FALSE
			bDialoguePlayed 	= false
			bWrongSite			= false
			bSeenHole			= false
			bAUDIOStreamLoaded 	= false
			lbl_Interrupt 		= ""
			lbl_root 			= ""		
			
			eInterruptstage = INTERRUPT_NONE
			
			vol_construction = STREAMVOL_CREATE_SPHERE(<<12.29, -638.08, 15.09>>,20,FLAG_MAPDATA)
			iDelay = GET_GAME_TIMER()
			
			IF IS_VEHICLE_DRIVEABLE(vehs[mvf_helicopter].id)
				SET_FORCE_HD_VEHICLE(vehs[mvf_helicopter].id, TRUE)
			ENDIF			
			
			objdummy = CREATE_OBJECT(PROP_LD_TEST_01,<<25.30480, -635.79169, 30.30575>>)
			if not DOES_ENTITY_EXIST(objCam)
				objcam = CREATE_OBJECT(PROP_PAP_CAMERA_01,GET_ENTITY_COORDS(lester()))
				IF IsEntityAlive(vehs[mvf_helicopter].id)
					SET_ENTITY_NO_COLLISION_ENTITY(objcam,vehs[mvf_helicopter].id,FALSE)
				ENDIF
				battachedCam = false
			endif
			Unload_Asset_Model(sAssetData,PROP_LD_TEST_01)
			FREEZE_ENTITY_POSITION(objdummy,true)
			ADD_SCENARIO_BLOCKING_AREA(<<71.25517, -613.30609, 10.67887>>,<<2.50692, -654.65991, 39.92294>>)
			CLEAR_AREA(vHOLE,30,true)
			DESTROY_ALL_CAMS()
			iHoleStage 		 = 0
			bLesterCam 		 = false
			ihintstage 		 = 0 // hint cam 
			iHOLE_INFO_STAGE = 0
			b_create_workers = true
			b_get_near_help = FALSE
			b_film_hole_help_reminder = FALSE
			IF NOT IS_ENTITY_DEAD(TREV())
				IF NOT IS_ENTITY_VISIBLE(TREV())
					SET_ENTITY_VISIBLE(TREV(),true)
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD(LESTER())
				IF NOT IS_ENTITY_VISIBLE(lester())
					SET_ENTITY_VISIBLE(lester(),true)
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
				IF NOT IS_ENTITY_VISIBLE(vehs[mvf_helicopter].id)
					SET_ENTITY_VISIBLE(vehs[mvf_helicopter].id,true)
				ENDIF
			ENDIF
			RENDER_SCRIPT_CAMS(false,false)
			DISPLAY_RADAR(true)
			DISPLAY_HUD(true)
			STOP_SOUND(camZoom)
			STOP_SOUND(camhum)
			stop_mission_audio_scenes()			
			if not IS_AUDIO_SCENE_ACTIVE("BS_1_SEARCH_FOR_ENTRANCE")
				START_AUDIO_SCENE("BS_1_SEARCH_FOR_ENTRANCE")
			endif
			
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
			
			mission_substage++			
		break
		case 1
			if GET_GAME_TIMER() - iDelay > 500
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_INFO",CONV_PRIORITY_MEDIUM)
						eholestage = HOLE_OBJECTIVE
						INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FH1_FIND_HOLE_TIME)
						mission_substage++
					endif
				ENDIF
			endif
		break		
		case 2
			IF NOT bFindHoleGodText
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
					Print_now("FH1_HOLE",DEFAULT_GOD_TEXT_TIME,1)	
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FH1_FIND_HOLE_TIME) 
					bFindHoleGodText = TRUE
				ENDIF
			ELSE
				find_hole_dialogue()
			ENDIF
			
			IF NOT bBeenToHole
				IF IS_VEHICLE_DRIVEABLE(vehs[mvf_helicopter].id)
					IF IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_helicopter].id, <<16.890251,-659.901367,29.972874>>, <<50.351219,-569.759949,87.376694>>, 66.750000)
					AND IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
					AND NOT GET_IS_TASK_ACTIVE(trev(),CODE_TASK_EXIT_VEHICLE)
//						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
							Print_now("FH1_HOVERN",DEFAULT_GOD_TEXT_TIME,1)	
							bBeenToHole = TRUE
							IF NOT bFindHoleGodText
								REPLAY_RECORD_BACK_FOR_TIME(4.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
								INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FH1_FIND_HOLE_TIME) 
								bFindHoleGodText = TRUE
							ENDIF
//						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT b_get_near_help
					IF NOT IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_helicopter].id, <<16.890251,-659.901367,29.972874>>, <<50.351219,-569.759949,87.376694>>, 66.750000)
					AND IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
					AND NOT GET_IS_TASK_ACTIVE(trev(),CODE_TASK_EXIT_VEHICLE)
						PRINT_HELP("FH1_CSHELP2")
						b_get_near_help = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			
			if IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_helicopter].id,<<17.52435, -655.95490, 30.76982>>,<<34.11578, -609.88947, 54.92456>>,48)
			and IS_PED_IN_VEHICLE(trev(),vehs[mvf_helicopter].id)
			and not GET_IS_TASK_ACTIVE(trev(),CODE_TASK_EXIT_VEHICLE)
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
				if eholestage = HOLE_STABLE
				//---------next stage load--------
					Load_asset_Stage(msf_6_drop_off_point) //load next stage
					SET_OBJECT_AS_NO_LONGER_NEEDED(objdummy)
					
				//--------------------------------
					if DOES_BLIP_EXIST(blip_objective)
						remove_blip(blip_objective)
					endif	
					iDelay = get_game_timer()	
					

					mission_substage++
				endif
			endif
		break	
		case 3
			IF bLesterCam = FALSE
				IF b_film_hole_help_reminder = FALSE
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP("FH1_FILM")
						b_film_hole_help_reminder = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
				iHOLE_INFO_STAGE = 0
				bDialoguePlayed = false					
				iHoleTimer = GET_GAME_TIMER()
				mission_substage++
			else
				if bLesterCam
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FH1_FILM")
						CLEAR_HELP()
					ENDIF
					SET_PLAYER_CONTROL(player_id(),false)
					IF NOT IS_PED_INJURED(TREV())
						if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
							//TASK_HELI_MISSION(trev(),vehs[mvf_helicopter].id,null,null,GET_ENTITY_COORDS(vehs[mvf_helicopter].id),MISSION_GOTO,-1,-1,-1,-1,-1)
							TASK_HELI_MISSION(trev(),vehs[mvf_helicopter].id,null,null,GET_ENTITY_COORDS(vehs[mvf_helicopter].id),MISSION_GOTO,0,-1,-1,-1,-1)
						endif
					ENDIF
				else
					SET_PLAYER_CONTROL(player_id(),true)
					IF NOT IS_PED_INJURED(TREV())
						if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
							CLEAR_PED_TASKS(trev())
						endif
					ENDIF
				endif
			endif
		break
		case 4
			IF bLesterCam = FALSE
				IF b_film_hole_help_reminder = FALSE
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP("FH1_FILM")
						b_film_hole_help_reminder = TRUE
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FH1_FILM")
					CLEAR_HELP()
				ENDIF
			ENDIF
			
			if filmed_hole()					
			
				REPLAY_RECORD_BACK_FOR_TIME(7.0, 7.0, REPLAY_IMPORTANCE_HIGHEST)
				//Quick disable multihead fade if lester cam still active
				IF bLesterCam
					SET_MULTIHEAD_SAFE(FALSE,TRUE)
				ENDIF			
				bLesterCam = false
				IF NOT IS_ENTITY_DEAD(TREV())
					IF NOT IS_ENTITY_VISIBLE(TREV())
						SET_ENTITY_VISIBLE(TREV(),true)
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(LESTER())
					IF NOT IS_ENTITY_VISIBLE(lester())
						SET_ENTITY_VISIBLE(lester(),true)
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
					IF NOT IS_ENTITY_VISIBLE(vehs[mvf_helicopter].id)
						SET_ENTITY_VISIBLE(vehs[mvf_helicopter].id,true)
					ENDIF
				ENDIF
				RENDER_SCRIPT_CAMS(false,false)
				CLEAR_TIMECYCLE_MODIFIER()
				DISPLAY_RADAR(true)
				DISPLAY_HUD(true)
				STOP_SOUND(camZoom)
				STOP_SOUND(camhum)
				IF NOT IS_PED_INJURED(TREV())
					if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
						CLEAR_PED_TASKS(trev())
					endif
				ENDIF
				TRIGGER_MUSIC_EVENT("FH1_END")
				mission_substage++
			endif	
		break
		case 5
			IF bLesterCam = FALSE
				IF b_film_hole_help_reminder = FALSE
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP("FH1_FILM")
						b_film_hole_help_reminder = TRUE
					ENDIF
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("FH1_FILM")
					CLEAR_HELP()
				ENDIF
			ENDIF
			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfmov)
				DESTROY_ALL_CAMS()
				SET_CINEMATIC_BUTTON_ACTIVE(true)
				clear_help(false)		
				STOP_SOUND(camZoom)
				STOP_SOUND(camhum)
				//lester back into his seat
				if GET_PED_VEHICLE_SEAT(lester(),vehs[mvf_helicopter].id) = VS_BACK_RIGHT
				and not IS_SCRIPT_TASK_RUNNING_OR_STARTING(lester(),script_TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT)
					TASK_SHUFFLE_TO_NEXT_VEHICLE_SEAT(lester(),vehs[mvf_helicopter].id)
				endif
				//cam stuff									
				REPLAY_RECORD_BACK_FOR_TIME(2.0, 7.0, REPLAY_IMPORTANCE_HIGHEST)
				mission_substage = STAGE_ENTRY
				mission_set_stage(msf_6_drop_off_point)
			endif 
		break
	endswitch	
	
	if mission_substage < 5
		manage_lester_Cam()
		if IsEntityAlive(lester())
			update_lester_anims(vHOLE)		
		endif
	endif
	
	
	
	if mission_substage < 4
		if IS_PED_IN_VEHICLE(TREV(),vehs[mvf_helicopter].id)
		and IS_PED_IN_VEHICLE(Lester(),vehs[mvf_helicopter].id)
			if b_convo_paused
			and IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				PAUSE_FACE_TO_FACE_CONVERSATION(false)
				b_convo_paused = false					
			endif
		else
			if not b_convo_paused
				PAUSE_FACE_TO_FACE_CONVERSATION(true)							
				b_convo_paused = true
			endif
		endif
	endif	
ENDPROC
PROC ST_6_DROP_OFF_POINT()

	switch mission_substage
		case stage_entry
			
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				Point_Gameplay_cam_at_coord(187.9638)
			ENDIF			
					
			if DOES_ENTITY_EXIST(vehs[mvf_helicopter].id)
				FREEZE_ENTITY_POSITION(vehs[mvf_helicopter].id,false)
			endif
						
			Hotswap_Menu(False,true,false)
			bHotSwap = true
						
			eMike_dialogue 	= M_di_END
			eDialogue_stateM = DI_START_CONDITION_MIKE
			eInterruptstage = INTERRUPT_NONE
					
			if DOES_ENTITY_EXIST(objcam)
				if IS_ENTITY_ATTACHED(objcam)
					DETACH_ENTITY(objcam)
				endif
				SAFE_DELETE_OBJECT(objcam)
			endif
			IF NOT IS_PED_INJURED(LESTER())
				CLEAR_PED_SECONDARY_TASK(lester())
			ENDIF
			
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGH)
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6,"Stage 6: Drop off point",true)
			bShowObjective = false
			mission_substage++
		break
		case 1	//*using to trigger dialogue in the manager*			
			if not sCamDetails.bRun				
				if 	player_ped_id() = mike()
					
					if IS_STREAMVOL_ACTIVE()
						STREAMVOL_DELETE(vol_construction)
					endif	
					if interior_constuction != null
						UNPIN_INTERIOR(interior_constuction)
					endif
					
					IF NOT IS_PED_INJURED(TREV())
					AND NOT IS_ENTITY_DEAD(vehs[mvf_helicopter].id)
						if GET_SCRIPT_TASK_STATUS(trev(),SCRIPT_TASK_VEHICLE_MISSION) != PERFORMING_TASK
							TASK_VEHICLE_MISSION_COORS_TARGET(trev(),vehs[mvf_helicopter].id,<< 1728.8278, 3126.1279,106.3001 >>,MISSION_CIRCLE,30,DRIVINGMODE_AVOIDCARS,4,-1)					
						endif
					ENDIF
				
					//on
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(TREV(),true)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(lester(),true)
					if IsEntityAlive(vehs[mvf_helicopter].id)
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_helicopter].id,true)
					endif
					if IsEntityAlive(vehs[mvf_trevor_veh].id)
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_trevor_veh].id,true)
					endif
					//off
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(MIKE(),false)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(frank(),false)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_Michael_veh].id,false)
					
					stop_mission_audio_scenes(13)
					if not IS_AUDIO_SCENE_ACTIVE("BS_1_CAR_RETURN_TO_FRANKLINS")
						START_AUDIO_SCENE("BS_1_CAR_RETURN_TO_FRANKLINS")
					endif			
										
					if IS_SAFE_TO_DISPLAY_GODTEXT()
						if bShowObjective						
							print_now("FH1_End2",DEFAULT_GOD_TEXT_TIME,1)
							bShowObjective = false
						endif
					endif
					
					if IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData,<<17.56, 547.32, 175.11>>,<<7,3,LOCATE_SIZE_HEIGHT>>,true,frank(),null,null,vehs[mvf_Michael_veh].id,"","FH1_FRCAR","","","","FH1_CAR","FH1_BKCAR")
						if IS_VEHICLE_ALMOST_STOPPED(vehs[mvf_Michael_veh].id)
							
							REPLAY_RECORD_BACK_FOR_TIME(6.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
							
							SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_MICHAEL, SAVEHOUSE_FRANKLIN_VH, FALSE)
							DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_F_HOUSE_VH_F), DOORSTATE_UNLOCKED, FALSE, TRUE)
							SET_PED_CONFIG_FLAG(frank(), PCF_OpenDoorArmIK, TRUE)
							
							OPEN_SEQUENCE_TASK(seq)
								TASK_LEAVE_ANY_VEHICLE(null,0,ECF_DONT_WAIT_FOR_VEHICLE_TO_STOP)
								TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<6.4282, 535.5682, 175.0280>>,PEDMOVE_WALK,DEFAULT_TIME_BEFORE_WARP,1,ENAV_DEFAULT,DEFAULT_NAVMESH_FINAL_HEADING)	
//								TASK_GO_STRAIGHT_TO_COORD(null,<<5.7777, 535.8402, 175.0280>>,PEDMOVE_WALK)				
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(frank(),seq)
							CLEAR_SEQUENCE_TASK(seq)
							SET_PLAYER_CONTROL(PLAYER_ID(),false,SPC_LEAVE_CAMERA_CONTROL_ON)
							//cam 						
							DESTROY_ALL_CAMS()
							HANG_UP_AND_PUT_AWAY_PHONE()
							CLEAR_AREA(<< 17.8857, 572.9736, 180.1063 >>,50,true)				
							cameraIndex = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED,<<7.5984, 539.8885, 175.7547>>, <<7.1088, 0.0000, -38.9600>>,45.5983)
							SET_CAM_ACTIVE(cameraIndex,true)
							SET_CAM_PARAMS(cameraIndex,<<7.6983, 540.0698, 175.7795>>, <<6.0492, 0.0000, -38.9600>>,45.5983,7000)
							RENDER_SCRIPT_CAMS(true,false)
							fshakeAmp = 0.1
							SHAKE_CAM(cameraIndex,"HAND_SHAKE",fshakeAmp)
							DISPLAY_RADAR(false)
							DISPLAY_HUD(false)						
							SET_GAMEPLAY_COORD_HINT(<<8.17981, 539.19104, 176.37321>>,10000,0)
							bsetEndCam = False
							bHotSwap = false
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							eMike_dialogue = M_di_NONE
							bDialoguePlayed = false
							iDelay = get_game_timer()
							mission_substage++
						else
							BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehs[mvf_Michael_veh].id,4,1)	
						endif
					endif
				elif player_ped_id() = TREV()
					
					IF NOT IS_PED_INJURED(MIKE())
					AND NOT IS_ENTITY_DEAD(vehs[mvf_Michael_veh].id)
						if GET_SCRIPT_TASK_STATUS(MIKE(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE) != PERFORMING_TASK
							TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(MIKE(),vehs[mvf_Michael_veh].id, << 11.2359, 547.3271, 174.8878 >>,10,DRIVINGMODE_AVOIDCARS_OBEYLIGHTS,2)
						endif
					ENDIF
					
					//Michael party					
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(mike(),true)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(frank(),true)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_Michael_veh].id,true)
					//Trevor lot invincible					
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(trev(),false)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(lester(),false)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehs[mvf_helicopter].id,false)
					
					if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id()),vHOLE) > 200
						if IS_STREAMVOL_ACTIVE()
							STREAMVOL_DELETE(vol_construction)
						endif	
						if interior_constuction != null
							UNPIN_INTERIOR(interior_constuction)
						endif	
					endif
					
					if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id()),<<1753.27808, 3262.48755, 40.32073>>) < 200
						if not IsEntityAlive(vehs[mvf_trevor_veh].id)
							CREATE_PLAYER_VEHICLE(vehs[mvf_trevor_veh].id,CHAR_TREVOR,<< 1768.0714, 3293.4478, 40.2011 >>, 305.7980)			
						else
							SET_ENTITY_COORDS(vehs[mvf_trevor_veh].id,<< 1768.0714, 3293.4478, 40.2011 >>)
							SET_ENTITY_HEADING(vehs[mvf_trevor_veh].id,305.7980)
							SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_trevor_veh].id)
							FREEZE_ENTITY_POSITION(vehs[mvf_trevor_veh].id,false)
						endif					
					endif
					
					stop_mission_audio_scenes(12)
					if not IS_AUDIO_SCENE_ACTIVE("BS_1_HELI_RETURN_TO_AIRSTRIP")
						START_AUDIO_SCENE("BS_1_HELI_RETURN_TO_AIRSTRIP")
					endif	
					
					if IS_SAFE_TO_DISPLAY_GODTEXT()
					and DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						if not bShowObjective						
							print_now("FH1_AIR2",DEFAULT_GOD_TEXT_TIME,1)
							eTREV_Dialogue 	= T_di_END
							bShowObjective = true							
						endif
					endif
					
					if IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData,<<1754.60693, 3268.81152, 40.24541>>,<<15,15,LOCATE_SIZE_HEIGHT>>,true,lester(),NULL,NULL,vehs[mvf_helicopter].id,"","","","","","FH1_HELI","FH1_BKHELI",FALSE,true)										
					and IS_VEHICLE_ALMOST_STOPPED(vehs[mvf_helicopter].id)	
					
						SET_PLAYER_CONTROL(player_id(),false)
//						SET_VEHICLE_ENGINE_ON(vehs[mvf_helicopter].id,false,false)
						BRING_VEHICLE_TO_HALT(vehs[mvf_helicopter].id,5,1)
							
						
						if IsEntityAlive(vehs[mvf_trevor_veh].id)							
							SET_ENTITY_COORDS(vehs[mvf_trevor_veh].id,<< 1768.0714, 3293.4478, 40.2011 >>)
							SET_ENTITY_HEADING(vehs[mvf_trevor_veh].id,305.7980)
						endif
						CLEAR_AREA(<<1754.60693, 3268.81152, 40.24541>>,20,true)
						//tasks
						//trev
						TASK_LOOK_AT_COORD(TREV(),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TREV(),<<-3.30,3,0>>),-1,SLF_WIDEST_YAW_LIMIT | SLF_USE_TORSO)
						//lester	
						CLEAR_PED_TASKS_IMMEDIATELY(LESTER())
						FORCE_PED_AI_AND_ANIMATION_UPDATE(LESTER())
						iSyncScene = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>,<<0,0,0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(iSyncScene,vehs[mvf_helicopter].id,GET_ENTITY_BONE_INDEX_BY_NAME(vehs[mvf_helicopter].id,"seat_dside_r"))
						TASK_SYNCHRONIZED_SCENE(lester(),iSyncScene,"missheist_the_big_score_setup_1@heli_exit","lester_exit_heli",INSTANT_BLEND_IN,WALK_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT)
					
						//cams
						DESTROY_ALL_CAMS()
						cameraIndex = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
						PLAY_SYNCHRONIZED_CAM_ANIM(cameraIndex,iSyncScene,"lester_exit_heli_CAM","missheist_the_big_score_setup_1@heli_exit")
						RENDER_SCRIPT_CAMS(true,false)
						
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 15.0, REPLAY_IMPORTANCE_HIGHEST)
						
						HANG_UP_AND_PUT_AWAY_PHONE()
						DISPLAY_RADAR(false)
						DISPLAY_HUD(false)
						bHotSwap = false
						iDelay = get_game_timer()		
						
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						eTREV_Dialogue = T_di_NONE
						bDialoguePlayed = false
						mission_substage++
					endif
					
				endif
			endif
		break		
		case 2 
			if player_ped_id() = TREV()
				if not sCamDetails.bRun	
					if IS_SAFE_TO_DISPLAY_GODTEXT()
					and not bDialoguePlayed
						if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_FINT",CONV_PRIORITY_MEDIUM)
							bDialoguePlayed = true
						endif
					endif
					
					if IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScene)
					and GET_SYNCHRONIZED_SCENE_PHASE(iSyncScene) >= 1
						TASK_FOLLOW_NAV_MESH_TO_COORD(lester(),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(lester(),<<0,10,0>>),PEDMOVE_WALK)					
						TASK_LOOK_AT_ENTITY(TREV(),LESTER(),15000,SLF_WIDEST_YAW_LIMIT | SLF_WHILE_NOT_IN_FOV)
						iDelay = get_game_timer()
						mission_substage++
					endif					
				endif
			ELSE
				if player_ped_id() = mike()
					if IS_SAFE_TO_DISPLAY_GODTEXT()
					and not bDialoguePlayed
						if CREATE_CONVERSATION(convo_struct,"FH1AUD","FH1_FINM",CONV_PRIORITY_MEDIUM)
							bDialoguePlayed = true
						endif
					endif
					if not IS_PED_IN_ANY_VEHICLE(FRANK())
						SET_PED_RESET_FLAG(FRANK(), PRF_SearchForClosestDoor, TRUE)
						IF get_game_timer() - iDelay > 4000				
							if not bsetEndCam 
								RENDER_SCRIPT_CAMS(false,false)	
								
								SET_ENTITY_COORDS(FRANK(), <<8.7834, 540.8622, 175.0277>>,true,true)
								SET_ENTITY_HEADING(FRANK(), 154.3967)
								/*SET_ENTITY_COORDS(FRANK(),<<8.91393, 540.71735, 175.02716>>,true,true)
								SET_ENTITY_HEADING(FRANK(),154.30)*/
//								SET_PLAYER_CONTROL(PLAYER_ID(),true)
								STOP_GAMEPLAY_HINT()
								bsetEndCam = true
							endif
							IF get_game_timer() - iDelay > 3500
								SET_PLAYER_CONTROL(PLAYER_ID(),true)
							ENDIF
//							IF IS_ENTITY_IN_ANGLED_AREA(FRANK(), <<6.609171,536.303101,174.528046>>, <<8.059766,538.846924,177.778122>>, 3.000000)
//								IF NOT IS_ENTITY_ON_SCREEN(FRANK())
//								IF NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(FRANK(), FALSE), 1)
								IF GET_SCRIPT_TASK_STATUS(FRANK(), SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK
									PRINTLN("@@@@@@@@@@@@@@@@@@@ DELETE_PED(peds[mpf_franklin].id) @@@@@@@@@@@@@@@@@@@")
									DELETE_PED(peds[mpf_franklin].id)
									DISPLAY_HUD(true)
									mission_substage++
								ENDIF
//							ENDIF
						ENDIF
					else
						iDelay = get_game_timer()
					endif
				ENDIF
			ENDIF
		
		break
		case 3
			if player_ped_id() = MIKE()
				mission_substage++
			elif player_ped_id() = trev()
				if not sCamDetails.bRun	
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						bstopshake = true				
						RENDER_SCRIPT_CAMS(false,false)				
						DISPLAY_RADAR(true)
						DISPLAY_HUD(true)
						DESTROY_ALL_CAMS()
						if IsEntityAlive(LESTER())
							delete_ped(peds[mpf_lester].id)
						endif
						mission_substage++
					endif
				ENDIF
			endif
		break
		case 4			
			if player_ped_id() = MIKE()		
				IF IS_VEHICLE_DRIVEABLE(vehs[mvf_Michael_veh].id)
					SET_VEHICLE_RADIO_ENABLED(vehs[mvf_Michael_veh].id,true)
				ENDIF
				SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_MICHAEL, SAVEHOUSE_FRANKLIN_VH,FALSE)
				SET_PLAYER_CONTROL(PLAYER_ID(),true)			
				Mission_Passed()
			elif player_ped_id() = trev()				
				SET_PLAYER_CONTROL(PLAYER_ID(),true)					
				Mission_Passed()
			endif			
		break		
	endswitch
	
	AUDIO_manange_dialogue()

ENDPROC
PROC ST_7_PASSED()
	switch mission_substage
		case STAGE_ENTRY
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			endif
			mission_substage++
		break
		case 1
			Mission_Passed()
		break
	endswitch
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		MISSION FLOW
// -----------------------------------------------------------------------------------------------------------
PROC mission_flow()
	Switch	int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage)
		
		case msf_0_strip_club			ST_0_STRIP_CLUB() 			break
		case msf_1_font_of_bank			ST_1_FRONT_OF_BANK()		break
		case msf_2_back_of_bank			ST_2_BACK_OF_BANK()			break
		case msf_3_get_in_position		ST_3_GET_IN_POSITION()		break
		case msf_4_heli_follow_truck	ST_4_HELI_FOLLOW_TRUCK()	break
		case msf_5_CONSTRUCTION_HOLE	ST_5_CONSTRUCTION_HOLE() 	break
		case msf_6_drop_off_point		ST_6_DROP_OFF_POINT()		break
		case msf_7_passed				ST_7_PASSED()				break
		
	endswitch
ENDPROC
#IF IS_DEBUG_BUILD			
	PROC DO_DEBUG()
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_PASSED()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			MISSION_FAILED(mff_debug_fail)		
		ENDIF
	ENDPROC
#endif
// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	PRINTSTRING("...finale heist setup Mission Launched")
	PRINTNL()
	
	
	
	IF (HAS_FORCE_CLEANUP_OCCURRED())		
		if GET_CAUSE_OF_MOST_RECENT_FORCE_CLEANUP() = FORCE_CLEANUP_FLAG_SP_TO_MP
			PRINTSTRING("...finale heist setup Mission Force Cleanup Single player to Multiplayer")
			PRINTNL()
			Mission_Flow_Mission_Force_Cleanup()	
			Mission_Cleanup()
			TERMINATE_THIS_THREAD()
		else
			if not sCamDetails.bRun
				PRINTSTRING("...finale heist setup Mission Force Cleanup Single player default")
				PRINTNL()
				Mission_Flow_Mission_Force_Cleanup()
				if IS_CUTSCENE_ACTIVE()
					SET_CUTSCENE_FADE_VALUES()	
				endif
//				WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
//			    	WAIT(0)
//			    ENDWHILE
				Mission_Cleanup()
				TERMINATE_THIS_THREAD()
			endif
		endif
	ENDIF	
	
	DISABLE_STRIP_CLUBS()
	SET_MISSION_FLAG(TRUE)	
		
#if IS_DEBUG_BUILD
	//z menu for skipping stages
	zMenuNames[msf_0_strip_club].sTxtLabel 			=	"Stage 0: Strip club"
	zMenuNames[msf_1_font_of_bank].sTxtLabel 		=	"Stage 1: TREV: TRUCK 1 / MIKE: Front Bank"
	zMenuNames[msf_2_back_of_bank].sTxtLabel 		=	"Stage 2: TREV: TRUCK 2 / MIKE: Back Bank"
	zMenuNames[msf_3_get_in_position].sTxtLabel 	=	"Stage 3: TREV: HELI / MIKE: Back Bank"
	zMenuNames[msf_4_heli_follow_truck].sTxtLabel 	=	"Stage 4: Follow truck"
	zMenuNames[msf_5_construction_hole].sTxtLabel 	=	"Stage 5: Construction hole"
	zMenuNames[msf_6_drop_off_point].sTxtLabel 		=	"Stage 6: Drop off point"
	zMenuNames[msf_7_passed].sTxtLabel 				=	"--------- PASSED -----------"
	
	zMenuNames[CST_INIT].sTxtLabel    	= "Initial Cutscene"
	zMenuNames[CST_INIT].bSelectable  	= false
	zMenuNames[CST_MCS_2].sTxtLabel    	= "Stage 2: MCS_2"
	zMenuNames[CST_MCS_2].bSelectable  	= false
	zMenuNames[CST_MCS_3].sTxtLabel   	= "Stage 4: MCS_3"
	zMenuNames[CST_MCS_3].bSelectable 	= false
	
	widget_debug_cv = START_WIDGET_GROUP("Finale heist setup Menu")
	//widget_debug_cv = START_WIDGET_GROUP("MISSION DEBUG")
	
		SET_LOCATES_HEADER_WIDGET_GROUP(widget_debug_cv)
		//uber recording widget
		SET_UBER_PARENT_WIDGET_GROUP(widget_debug_cv) 
		ADD_WIDGET_VECTOR_SLIDER("Cam offset: ",vdebugcamOFfset,-50,50,0.01)
		
		ADD_WIDGET_INT_READ_ONLY("mission stage:", mission_stage)
		ADD_WIDGET_INT_READ_ONLY("mission substage:", mission_substage)	
		
		ADD_WIDGET_INT_READ_ONLY("Heli stage: ", 					iheliWidget)
		
		START_WIDGET_GROUP("Dialogue:")
			ADD_WIDGET_INT_READ_ONLY("Mike:f=0|b=1: ", 			wMike_dialogue)
			ADD_WIDGET_INT_READ_ONLY("Trev:f1=0|f2=1: ", 		wTREV_Dialogue)
			ADD_WIDGET_INT_READ_ONLY("SC=0|T=1|R=2|E=3: ", 		wDialoguestage)
			ADD_WIDGET_INT_READ_ONLY("both: ", 					wBOTH_dialogue)
			ADD_WIDGET_BOOL("wb_TREVSpeechSwitchStopped: ", 	wb_TREVSpeechSwitchStopped)
			ADD_WIDGET_BOOL("wb_MIKESpeechSwitchStopped: ", 	wb_MIKESpeechSwitchStopped)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Follow trucks debug skip")
			ADD_WIDGET_BOOL("skip to after tunnel:",wSkip)
		STOP_WIDGET_GROUP()
		CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
	STOP_WIDGET_GROUP()
	
	SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_TREVOR_IN_TRUCK(scsSwitchCamMichaelToTrevorInTruck, peds[mpf_michael].id,  peds[mpf_trevor].id)
	SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_TREVOR_IN_HELI(scsSwitchCamMichaelToTrevorInHeli, peds[mpf_michael].id,  peds[mpf_trevor].id)
	SETUP_SPLINE_CAM_NODE_ARRAY_TREVOR_IN_HELI_TO_MICHAEL(scsSwitchCamTrevorInHeliToMichael,  peds[mpf_trevor].id, peds[mpf_michael].id)	
	
	CREATE_SPLINE_CAM_WIDGETS(scsSwitchCamMichaelToTrevorInTruck, "Michael Car", "Trevor Truck", widget_debug_cv)
	CREATE_SPLINE_CAM_WIDGETS(scsSwitchCamMichaelToTrevorInHeli, "Michael Car", "Trevor Heli", widget_debug_cv)
	CREATE_SPLINE_CAM_WIDGETS(scsSwitchCamTrevorInHeliToMichael, "Trevor Heli", "Michael Car", widget_debug_cv)

#endif

	REQUEST_ADDITIONAL_TEXT("FINH1",MISSION_TEXT_SLOT)
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		WAIT(0)
	ENDWHILE

	MISSION_SETUP()	
	
	WHILE (TRUE)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TheBigScoreSetup")
		
		#IF IS_DEBUG_BUILD
			UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCamMichaelToTrevorInTruck)
			UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCamMichaelToTrevorInHeli)
			UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCamTrevorInHeliToMichael)
		#ENDIF		
			
		//prestreaming loop
		Update_Asset_Management_System(sAssetData)		// Deals with loading any assets and keeps track of what has been loaded
		Update_Cutscene_Prestreaming(sCutscenePedVariationRegister)	
		//main loop
		Mission_stage_management()		
		MISSION_STAGE_SKIP()
		
		if not bDoSkip
			MISSION_CHECKS()
			mission_flow()
		endif 		

		#IF IS_DEBUG_BUILD
			DO_DEBUG()
			HANDLE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
		#ENDIF	
	
		WAIT(0)
	
	ENDWHILE
ENDSCRIPT
