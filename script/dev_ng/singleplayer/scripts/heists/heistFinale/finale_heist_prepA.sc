
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "clearmissionarea.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_vehicle.sch"
USING "commands_path.sch"
USING "commands_ped.sch"
USING "commands_physics.sch"
USING "commands_script.sch"
USING "commands_task.sch"

USING "CompletionPercentage_public.sch"

using "dialogue_public.sch"
USING "flow_public_core_override.sch"
USING "flow_public_GAME.sch"

using "ped_component_public.sch"
USING "player_ped_public.sch"
USING "replay_public.sch"

USING "script_blips.sch"
USING "script_heist.sch"
USING "script_ped.sch"

USING "flow_special_event_checks.sch"

BOOL bJumpSkip = FALSE					//flag for if current MISSION state should clean up and move to the next state
USING "prep_mission_common.sch"

USING "event_public.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
	USING "select_mission_stage.sch"
	USING "shared_debug.sch"

	CONST_INT MAX_SKIP_MENU_LENGTH 2
	INT i_debug_jump_stage
	MissionStageMenuTextStruct s_skip_menu[MAX_SKIP_MENU_LENGTH]
#ENDIF
// ***************************************************************************************** 
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	finale_heist_prepA.sc
//		AUTHOR			:	Ste Kerrigan
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//----------------------
//    CHECKPOINTS
//----------------------
CONST_INT CP_AFTER_MOCAP                0 //following beverly down the road

//-----------------------
//		CONSTANTS
//-----------------------

//POLY TEST CONST
CONST_INT MAX_POLY_TEST_VERTS		8		//poly test
//Ambient encounter speech
CONST_INT MAX_AMBIENT_CONV_LINES	8

CONST_INT MAX_COPS					3
CONST_INT COP_1						0
CONST_INT COP_2						1
CONST_INT COP_3						2
CONST_INT MAX_COP_CARS				3

CONST_INT MAX_CLIMB_POINTS			2
CONST_INT MAX_RESTRICTED_AREAS		6
CONST_INT COP_ALLEY					0
CONST_INT COP_COMP					1

//Reaction consts
CONST_INT MAX_COP_VIEWCONE			140
CONST_INT DEFAULT_COP_REPEAT_DELAY	7500

/// --------------------------------------------------
///	ENUMS
/// -------------------------------------------

/// PURPOSE: Mission states
ENUM MISSION_STATE
	MS_SET_UP = 0,				//0
	MS_STEAL,					//1
	MS_RETURN_WITH_THING,		//2
	MS_LEAVE_VEHICLE_RING_LEST,	//3
	MS_FAILED					//4
ENDENUM

/// PURPOSE: 
///    Internal state machine states for mission state
ENUM STAGE_STATES
	SS_INIT,
	SS_ACTIVE,
	SS_CLEANUP,
	SS_SKIPPED
ENDENUM

/// PURPOSE: Mission requirements used for loading 
///    and creating mission assets
ENUM MISSION_REQ
	RQ_NONE,
	RQ_TEXT,
	RQ_STEAL_CAR,
	RQ_COPS,
	RQ_COP_CARS,
	RQ_STINGERS
ENDENUM

/// PURPOSE: Fail reason enums for picking correct fail reason
ENUM FAILED_REASONS
	FR_NONE,
	FR_WRECKED_STEAL_CAR,
	FR_ROOFSTUCK_STEAL_CAR,
	FR_LEFT_COPS,
	FR_LEFT_VAN
ENDENUM

ENUM REACTIONS
	REAC_PLAYER_WANTED = 0,					//0
	REAC_SEEN_IN_RESTRICTED_ARREST_AREA,	//1
	REAC_SEEN_GUN,							//2
	REAC_BUMP_RAGDOLL,						//3
	REAC_SEEN_IN_LONE_COP_AREA,				//4
	REAC_BUMP,								//5
	REAC_SEEN_IN_WARNING_AREA,				//6
	REAC_PLAYER_LEFT_VEHICLE_IN_ALLEY,		//7
	REAC_PLAYER_IN_VEHICLE,					//8
	REAC_PLAYER_IN_POLICE_TYPE_VEH,			//9
	REAC_SEEN_IN_COP_AREA,					//10
	REAC_HEARD_PLAYER,						//11
	REAC_SEEN_UNI,							//12
	REAC_SEEN_SNEAKING,						//13
	REAC_SEEN_IN_ALLEY,						//14
	NUM_REACT								//15
ENDENUM

ENUM RESTRICTED_AREA_ID
	RAI_RESTRICTED_ARREST_AREA = 0,
	RAI_FRONT_DESK_COPS_AREA,
	RAI_WARNING_AREA,
	RAI_BACK_ALLEY_AREA,
	RAI_VEHICLE_AREA,
	RAI_LONE_COP_AREA
ENDENUM

ENUM COP_BEHAVIOUR_STATES
	CBS_NULL,								///0
	CBS_NOT_ALERTED,
	CBS_QUESTIONING,
	CBS_REACTING
ENDENUM

ENUM COP_QUESTIONING_STATES
	CQS_NULL,								///0
	CQS_QUESTION_WATCH_PLAYER_COP_ZONE,		///1
	CQS_QUESTION_WATCH_PLAYER_ZONE_A,		///2
	CQS_QUESTION_WATCH_PLAYER_ZONE_B,		///3
	CQS_QUESTION_WATCH_PLAYER_STEALTH,		///4
	CQS_QUESTION_WATCH_PLAYER_BUMPED,		///5
	CQS_QUESTION_PLAYER_WITH_GUN,			///6
	CQS_TURN_TO_FACE_NOISE					///7  				  
ENDENUM

ENUM COP_REACTION_STATES
	CRS_NULL,
	CRS_WATCHING_PLAYER,					///0
	CRS_WARN_PLAYER_WITH_GUN,				///1
	CRS_PLAYER_IN_VEHICLE_IN_AREA,			///2
	CRS_PLAYER_VEHICLE_IN_AREA_NO_PLAYER,	///3
	CRS_REACT_WATCH_PLAYER_BUMPED_RAGDOLL,	///4
	CRS_WAIT_REACT_ARREST,					///5
	CRS_ARREST								///6
ENDENUM

ENUM COP_NOT_ALERTED_STATES
	CNAS_USING_SCENARIO, 						///0
	CNAS_RETURN_BEHAVIOUR						///1
ENDENUM

ENUM ABM_CONV_STATES
	ACS_START_LINE,
	ACS_WAIT_LINE_FIN,
	ACS_PICK_NEXT_LINE,
	ACS_FIN
ENDENUM

ENUM BATTLE_BUDDY_OVERRIDE_STATES
	BBOS_WAITING,
	BBOS_GO_TO_POSITION,
	BBOS_JUMPING_FENCE,
	BBOS_DRIVING_TO_PLACE,
	BBOS_END
ENDENUM

ENUM BATTLE_BUDDY_BEHAVIOUR
	BBB_CONTROL,
	BBB_CLIMB,
	BBB_DRIVE
ENDENUM

///MISSION PED STATES
MISSION_STATE eMissionState = MS_SET_UP										//track what MISSION stage we are at
STAGE_STATES eState = SS_INIT												//Internal state tracking for mission stages
IN_STEAL_CAR_MONITOR eStealCarState = ISCM_NOT_IN_VEHICLE
BATTLE_BUDDY_OVERRIDE_STATES eBattleBuddyState[MAX_BATTLE_BUDDIES] 
BATTLE_BUDDY_BEHAVIOUR eBattleBuddyBehaviour[MAX_BATTLE_BUDDIES] 
//COP_STATES ePrevCopState[MAX_COPS]

COP_MONITOR eCopMonitor = CM_MONITER

//INT iCopReactionTimer[MAX_COPS]
//BOOL bCopReactionTimerStarted[MAX_COPS]

//*****************************************************************************
//								:STRUCTS:
//*****************************************************************************

/// PURPOSE: Holds data used to create a ped 
///    Contains ped specific variables for the
///    perception system
STRUCT MYPED
	PED_INDEX 				id
	VECTOR 					vPos
	FLOAT 					fDir
	MODEL_NAMES 			Mod = DUMMY_MODEL_FOR_SCRIPT
	AI_BLIP_STRUCT			mAIBlip
	INT						iReactTimer = -1
	INT 					iReaction = 0
	BOOL					bCanSeePlayer = FALSE
	BOOL					bUpdateReaction = FALSE
	COP_BEHAVIOUR_STATES	eBehaviour = CBS_NULL
	COP_QUESTIONING_STATES	eQuestioning = CQS_NULL
	COP_REACTION_STATES		eReacting = CRS_NULL
	COP_NOT_ALERTED_STATES	eNotAlerted = CNAS_USING_SCENARIO
ENDSTRUCT

/// PURPOSE: Holds data for creating a 
///    vehicle in an encounter
STRUCT MYVEHICLE
	VEHICLE_INDEX 		id
	VECTOR 				vPos
	FLOAT 				fDir
	MODEL_NAMES 		Mod = DUMMY_MODEL_FOR_SCRIPT
ENDSTRUCT

STRUCT AMBIENT_DIALOGUE_LINE
	STRING sConvLine
	PED_INDEX ped
	STRING sVoice
	INT iStartTime = 0
	INT iLineTime = 0
	BOOL bDone = FALSE
ENDSTRUCT

STRUCT AMBIENT_DIALOGUE_CONV
	AMBIENT_DIALOGUE_LINE mConvLine[MAX_AMBIENT_CONV_LINES]
	INT iIndex = 0
	ABM_CONV_STATES eAmbConv = ACS_START_LINE
	BOOL bIsRandom = FALSE //Doesnt do anything yet 
ENDSTRUCT

/// PURPOSE: Holds a string and a int 
///    String is used to play a conversation
///    the int is used to expire a bit in a bit field
///    
STRUCT DIALOGUE_HOLDER
	STRING 	sConv
	INT 	iExpireBit
ENDSTRUCT


STRUCT CLIMB_POINT
	VECTOR vPos
	FLOAT fDir
ENDSTRUCT

STRUCT RESTRICTED_AREA
	VECTOR	vAreaPos1
	VECTOR 	vAreaPos2
	FLOAT 	fAreaWidth
ENDSTRUCT

TEST_POLY mCopConvoCheck1	//Poly area 1
TEST_POLY mCopConvoCheck2	//Poly area 1
TEST_POLY mAreaCheck
TEST_POLY areaCopStation


CLIMB_POINT mClimbPoint[MAX_CLIMB_POINTS]
RESTRICTED_AREA mRestrictedArea[MAX_RESTRICTED_AREAS]
//****************************************************************************************************
//								: MISSION FLOW VARIABLES :
//****************************************************************************************************

//mission flow
INT iMissionState = 0					//Used in skips and checkpoints

//Fail vars
STRING sFailReason = NULL				//String to display when mission is failed

//GOD TEXT
STRING sGodText = "FINPRA"
BOOL bObjectiveShown = FALSE			//Has an objective been shown

//BLIPs
BLIP_INDEX biBlip						//Mission blip - mainly used for go to objectives
BLIP_INDEX biVehicleBlip

//INT iCompoundTimer = 0
//BOOL bCompoundTimerStarted = FALSE

OBJECT_INDEX oiStingers

//ScriptCamera
CAMERA_INDEX camMain					//Script camera used in place holder cutscenes

SCENARIO_BLOCKING_INDEX	siCopStation

VECTOR vSafeVec = <<0,0,0>>				//safe vector used when a proper position isnt needed 

VECTOR vDropOffLoc = <<134.6909, -1255.3782, 28.4875>>
///MISSION PED VARS SCRIPT AI

INT iUpsideDownTimer = 0 
BOOL bOnRoofTimerStarted = FALSE
// GATES
INT iSideGate

FLOAT fFrontGateOpenRatio = 0.0
INT iNavBlocker

//COP REACTION STUFF
BOOL bIsPlayerWearingCopUniform = FALSE

BOOL bPlayerInPoliceTypeVeh = TRUE


BOOL bIsPlayerWanted = FALSE

BOOL bActivatedWanted = FALSE
INT iAreaWarningTimer
INT iTimesSeenWithGun = 0
INT iVehicleWarnings = 0

INT iCallTimerDelay = 0
BOOL bCallTimer = FALSE

INT iHearingTimer = 0
BOOL bHearingDelay = FALSE

BOOL bDamageSpeedStatTurnedOn = FALSE

///==============| GOD TEXT BOOLS |============

BOOL bExpireReturnVehWanted = FALSE
BOOL bExpireReturnVeh = FALSE

BOOL bDeletedCops = FALSE

BOOL bAreaWarning = FALSE
BOOL bVanWarning = FALSE
///===============| models |===================


/// ===============| START VECTORS |==============

/// ==============| START HEADINGS |===============

/// ==============| PED INDICES |================
MYPED mCop[MAX_COPS]


/// ==============| VEHICLE INDICES |===========
MYVEHICLE mStealCar
MYVEHICLE mCopCars[MAX_COP_CARS]


/// ===============| GROUPS |========================
REL_GROUP_HASH HASH_ENEMIES			//Relationship hash for all ranchers

/// ===============| DIALOGUE |======================
STRING sTextBlock = "BSPRAAU"			//The Dialogue block for the mission
structPedsForConversation s_conversation_peds		//conversation struct
//BOOL bLestCallDone = FALSE
//BOOL bExpireCopDispatch = FALSE
//BOOL bExpireCopResponse = FALSE 

BOOL bCopConvExpire[MAX_COPS]
STRING sCopConvs[MAX_COPS]
BOOL bACopConvOnGoing
INT iCopConvID = 0
BOOL bExpireCopShoutAfterPlayer = FALSE
BOOL bExpireArrestLine = FALSE
INT iWantedLevelResetTimer = -1
BOOL bExpireStealth = FALSE

BOOL bMonitorWantedStatFlow = TRUE

BOOL bForceStop = FALSE

BOOL bPlayNewReport = FALSE

INT iClimbPoint = -1

//Used for tracking an interupted conversation
TEXT_LABEL_23 sResumeRoot = "NONE"
TEXT_LABEL_23 sResumeLine = "NONE"

PROC RESET_COP_REACTION_FLAGS()
	INT i, c
	FOR c=0 TO (MAX_COPS-1)
		FOR i=0 TO (ENUM_TO_INT(NUM_REACT)-1)
			CLEAR_BIT(mCop[c].iReaction, i)
		ENDFOR
		
		mCop[c].bUpdateReaction = FALSE
		mCop[c].bCanSeePlayer = FALSE
	ENDFOR

	iTimesSeenWithGun = 0
	iVehicleWarnings = 0
	bActivatedWanted = FALSE
	iWantedLevelResetTimer = -1
	bExpireCopShoutAfterPlayer = FALSE
	bIsPlayerWanted = FALSE
	bExpireStealth = FALSE
ENDPROC

/// PURPOSE:
///    Resets all variables used in flow
PROC RESET_ALL()
	INT i 
	sResumeRoot = "NONE"
	sResumeLine = "NONE"

	bExpireReturnVehWanted = FALSE
	
	bExpireCopShoutAfterPlayer = FALSE
	bExpireReturnVeh = FALSE
	bExpireArrestLine = FALSE
	
//	bLestCallDone = FALSE
	
	iCallTimerDelay = 0
	bCallTimer = FALSE
	eCopMonitor = CM_MONITER
	
	RESET_COP_REACTION_FLAGS()

	FOR i=0 TO (MAX_COPS-1)
		mCop[i].eBehaviour = CBS_NULL
		mCop[i].eQuestioning = CQS_NULL
		mCop[i].eReacting = CRS_NULL
		bCopConvExpire[i] = FALSE
	ENDFOR
	bACopConvOnGoing = FALSE
	iCopConvID = 0
	
	FOR i=0 TO (MAX_BATTLE_BUDDIES-1)
		eBattleBuddyState[i] = BBOS_WAITING
		eBattleBuddyBehaviour[i] = BBB_CONTROL
	ENDFOR
	
	bMonitorWantedStatFlow = TRUE
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREP_GOT_WANTED, FALSE)
	DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID, DOORSTATE_FORCE_LOCKED_THIS_FRAME)
	
	bForceStop = FALSE
ENDPROC

///
///    
///    DEBUG ONLY MISSION STUFF ------------------------------------------------------------
///    
///    

#IF IS_DEBUG_BUILD

	BOOL bShowDebugText = TRUE
	BOOL bTogglePoly = FALSE
	WIDGET_GROUP_ID widgetGroup
	BOOL bForceFail = FALSE
	/// PURPOSE:
	///    Prints a string to a TTY Channel
	/// PARAMS:
	///    s - The string to print
	///    ddc - The debug channel to print to
	PROC SK_PRINT(String s, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s)
			PRINTNL()
			PRINTNL()
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Prints a string and an int to a TTY Channel
	/// PARAMS:
	///    s - The string to print
	///    i - the int to print
	///    ddc - the debug channel to print to
	PROC SK_PRINT_INT(String s, INT i, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s,i)
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Prints a string and a Float to a TTY Channel
	/// PARAMS:
	///    s - the string to print
	///    f - the float to print
	///    ddc - the debug channel 
	PROC SK_PRINT_FLOAT(String s, FLOAT f, DEBUG_CHANNELS ddc = DEBUG_MISSION)
		IF bShowDebugText
			CPRINTLN(ddc, s,f)
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Sets up a widget for this mission
	PROC SETUP_FOR_RAGE_WIDGETS()
		widgetGroup = START_WIDGET_GROUP("CURRENT Mission Widgets")
			START_WIDGET_GROUP("Debug")
				ADD_WIDGET_BOOL("Toggle Debug spew", bShowDebugText)
				ADD_WIDGET_BOOL("Toggle Drawing poly", bTogglePoly)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Force Mission Fail")
				ADD_WIDGET_BOOL("Force Fail", bForceFail)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Cops reactions")
				ADD_BIT_FIELD_WIDGET("Cop 1", mCop[COP_1].iReaction)
				ADD_BIT_FIELD_WIDGET("Cop 2", mCop[COP_2].iReaction)
				ADD_BIT_FIELD_WIDGET("Cop 3", mCop[COP_3].iReaction)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
	ENDPROC
	
	/// PURPOSE:
	///    Deletes the mission widget
	PROC CLEANUP_OBJECT_WIDGETS()
		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
	ENDPROC 
	
	/// PURPOSE:
	///    Checks for any updates needed for the widgets
	PROC UPDATE_RAG_WIDGETS()
	
		IF bForceFail
			eMissionState = MS_FAILED
			eState = SS_INIT
			bForceFail = FALSE
		ENDIF	

		IF bTogglePoly
			DISPLAY_POLY(mCopConvoCheck1, 255,0,0)
			DISPLAY_POLY(mCopConvoCheck2, 0,255,0)
			DISPLAY_POLY(areaCopStation, 0,0,255)
			DISPLAY_POLY(mAreaCheck, 255,255,255)
		ENDIF
	ENDPROC
	
	/// PURPOSE:
	///    Draws an angled area
	/// PARAMS:
	///    vec1 - first point of the area
	///    vec2 - second point of the area
	///    width - the width of the area
//	PROC DRAW_DEBUG_LOCATE_SPECIAL(VECTOR vec1, VECTOR vec2, FLOAT width)
//
//		VECTOR vBottom[2]
//		VECTOR vTop[2]
//		
//		vBottom[0] = vec1
//		vBottom[1] = vec2
//		
//		vTop[0] = vec1
//		vTop[1] = vec2
//		
//		IF vec1.z > vec2.z
//			vBottom[0].z = vec2.z
//			vBottom[1].z = vec2.z
//			vTop[0].z = vec1.z
//			vTop[1].z = vec1.z
//		ELSE
//			vBottom[0].z = vec1.z
//			vBottom[1].z = vec1.z
//			vTop[0].z = vec2.z
//			vTop[1].z = vec2.z
//		ENDIF
//
//		VECTOR fwd = NORMALISE_VECTOR(vBottom[1] - vBottom[0])      // normalize to get distance 
//		VECTOR side = <<-fwd.y, fwd.x, fwd.z>>
//		VECTOR w = side * (width / 2.0)
//
//		// Bottom points
//		VECTOR c1 = vBottom[0] - w  // base left
//		VECTOR c2 = vBottom[0] + w  // base right              
//		VECTOR c3 = vBottom[1] + w  // top rt
//		VECTOR c4 = vBottom[1] - w  // top lt
//
//		// Top points
//		VECTOR d1 = vTop[0] - w  // base left
//		VECTOR d2 = vTop[0] + w  // base right              
//		VECTOR d3 = vTop[1] + w  // top rt
//		VECTOR d4 = vTop[1] - w  // top lt
//
//		// Draw bottom lines
//		DRAW_DEBUG_LINE(c1, c2, 128, 0, 128)
//		DRAW_DEBUG_LINE(c2, c3, 128, 0, 128)
//		DRAW_DEBUG_LINE(c3, c4, 128, 0, 128)
//		DRAW_DEBUG_LINE(c4, c1, 128, 0, 128)
//		// Draw top lines
//		DRAW_DEBUG_LINE(d1, d2, 128, 0, 128)
//		DRAW_DEBUG_LINE(d2, d3, 128, 0, 128)
//		DRAW_DEBUG_LINE(d3, d4, 128, 0, 128)
//		DRAW_DEBUG_LINE(d4, d1, 128, 0, 128)
//		// Draw uprights
//		DRAW_DEBUG_LINE(c1, d1, 128, 0, 128)
//		DRAW_DEBUG_LINE(c2, d2, 128, 0, 128)
//		DRAW_DEBUG_LINE(c3, d3, 128, 0, 128)
//		DRAW_DEBUG_LINE(c4, d4, 128, 0, 128)
//		
//	ENDPROC
	
#ENDIF

					///
					///    
					///    GENERAL HELP FUNCTIONS
					///    
					///    


/// PURPOSE:
///    Set a peds position and heading safely ie check its ok then move it and set its heading
/// PARAMS:
///    index - The ped to move
///    pos - The position to move it to
///    dir - The Heading to set
PROC SET_PED_POS(PED_INDEX index, VECTOR pos, FLOAT dir)
	IF IS_PED_UNINJURED(index)
		CLEAR_PED_TASKS(index)
	ENDIF

	SAFE_TELEPORT_ENTITY(index, pos, dir)
ENDPROC


/// PURPOSE:
///    Requests a model and waits for it to load
/// PARAMS:
///    _modname - The name of the model to load
///    _debugstring - used to debug 
///    i - used to debug
/// RETURNS:
///    TRUE when the model is loaded
FUNC BOOL REQUEST_AND_CHECK_MODEL(MODEL_NAMES _modname, STRING _debugstring, INT i = 0)


	REQUEST_MODEL(_modname)
	IF NOT Is_String_Null_Or_Empty(_debugstring)
	AND i <> -1
		#IF IS_DEBUG_BUILD
		  SK_PRINT_INT(_debugstring, i)
		#ENDIF
	ENDIF
	
	IF HAS_MODEL_LOADED(_modname)
		#IF IS_DEBUG_BUILD
			SK_PRINT("MODEL LOADED")
		#ENDIF
		RETURN TRUE
	ENDIF
		
	RETURN FALSE
ENDFUNC
/// PURPOSE:
///    Unloads a model  - Sets it as no longer needed
/// PARAMS:
///    _modname - The model to unload
PROC UNLOAD_MODEL(MODEL_NAMES _modname, BOOL bCheckLoaded = TRUE)
	IF bCheckLoaded
		IF HAS_MODEL_LOADED(_modname)
			SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
		ENDIF
	ELSE
		SET_MODEL_AS_NO_LONGER_NEEDED(_modname)
	ENDIF
ENDPROC

/// PURPOSE:
///    Spawns a ped and returns true if it was succesful. Requests the model and unloads it 
/// PARAMS:
///    pedindex - The PED_INDEX to write the newly create ped to 
///    model - The model to load and use to create the ped
///    pos - The position the ped should be created
///    dir - The heading the ped should have when created
///    bUnloadAfterSpawn - If true we unload the model after spawning the ped
///    bFreeze - if true we freeze the ped after spawning
///    bTempEvents - if true we block temp events
///    bTargetable - if false we set the ped to not be targetted
/// RETURNS:
///    TRUE if the ped was created
FUNC BOOL SPAWN_PED(PED_INDEX &pedindex, MODEL_NAMES model, VECTOR pos, FLOAT dir,   BOOL bUnloadAfterSpawn = TRUE, BOOL bFreeze = FALSE, BOOL bTempEvents = TRUE, BOOL bTargetable = TRUE)
	IF NOT DOES_ENTITY_EXIST(pedindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			pedindex = CREATE_PED(PEDTYPE_MISSION, model, pos, dir)
			//SET_PED_DEFAULT_COMPONENT_VARIATION(pedindex)
			IF IS_PED_UNINJURED(pedindex)
				#IF IS_DEBUG_BUILD SK_PRINT("PED CREATED !") #ENDIF
				IF bFreeze
					FREEZE_ENTITY_POSITION(pedindex, TRUE)
				ENDIF
				
				IF bTempEvents
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedindex, bTempEvents)
				ENDIF
				
				IF NOT bTargetable
					SET_PED_CAN_BE_TARGETTED(pedindex, bTargetable)
				ENDIF
				

				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Spawns a vehicle and returns true if it was succesful - why does the is mission entity check return true
///    no matter what the out come of said check?
/// PARAMS:
///    vehicleindex - The VEHICLE_INDEX to write the newly create vehicle to 
///    model - The model to load and use to create the vehicle
///    pos - The position the vehicle should be created
///    dir - The heading the vehicle should have when created
/// RETURNS:
///    TRUE if the vehicle was created
FUNC BOOL SPAWN_VEHICLE(VEHICLE_INDEX &vehicleindex, MODEL_NAMES model, VECTOR pos, FLOAT dir, BOOL bUnloadAfterSpawn = TRUE)
	IF NOT DOES_ENTITY_EXIST(vehicleindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			vehicleindex = CREATE_VEHICLE(model, pos, dir)
			
			IF DOES_ENTITY_EXIST(vehicleindex)
				IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicleindex)
					SET_ENTITY_AS_MISSION_ENTITY(vehicleindex)
				ENDIF
				SET_VEHICLE_ON_GROUND_PROPERLY(vehicleindex)
				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
				
			ENDIF
		ENDIF
	ELSE
		IF IS_VEHICLE_OK(vehicleindex)
			SET_ENTITY_COORDS(vehicleindex, pos)
			SET_ENTITY_HEADING(vehicleindex, dir)
		ENDIF
		IF NOT IS_ENTITY_A_MISSION_ENTITY(vehicleindex)
			SET_ENTITY_AS_MISSION_ENTITY(vehicleindex)
			RETURN TRUE
		ELSE
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Spawns a ped inside a vehicle
/// PARAMS:
///    pedindex - The index to write the newly created ped back to (ref)
///    Car - The vehicle to create the ped in
///    modelName - The model of the ped to create
///    seat - the seat to create the ped in 
///    bUnloadAfterSpawn - should we unload the ped model after creation
/// RETURNS:
///    TRUE if the ped was spawned correctly
FUNC BOOL SPAWN_PED_IN_VEHICLE(PED_INDEX &pedindex, VEHICLE_INDEX Car, MODEL_NAMES modelName, VEHICLE_SEAT seat = VS_DRIVER, BOOL bUnloadAfterSpawn = TRUE)
	IF IS_VEHICLE_OK(Car)
		IF NOT DOES_ENTITY_EXIST(pedindex)
			IF REQUEST_AND_CHECK_MODEL(modelName,"Loading")
				pedindex = CREATE_PED_INSIDE_VEHICLE(Car, PEDTYPE_MISSION, modelName, seat)
				
				IF DOES_ENTITY_EXIST(pedindex)
					IF bUnloadAfterSpawn
						UNLOAD_MODEL(modelName)
					ENDIF
					RETURN TRUE
				ENDIF

			ENDIF
		ELSE
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Spawns an object. And activates its physics
/// PARAMS:
///    objectindex - The OBJECT_INDEX to write the newly create object to 
///    model - The model to load and use to create the object
///    pos - The position the object should be created
///    dir - The heading the object should have when created
/// RETURNS:
///    TRUE if the object was created
FUNC BOOL SPAWN_OBJECT(OBJECT_INDEX &objectindex, MODEL_NAMES model, VECTOR pos, FLOAT dir = 0.0, BOOL bUnloadAfterSpawn = TRUE)
	IF NOT DOES_ENTITY_EXIST(objectindex)
		IF REQUEST_AND_CHECK_MODEL(model,"Loading")
			objectindex = CREATE_OBJECT(model, pos)
			
			IF DOES_ENTITY_EXIST(objectindex)
				SET_ENTITY_HEADING(objectindex, dir)
				ACTIVATE_PHYSICS(objectindex)
				IF bUnloadAfterSpawn
					UNLOAD_MODEL(model)
				ENDIF
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


//PURPOSE: 
/// PURPOSE:
///    Checkes to see if a ped is in a given vehicle, also does alive checks. 
///    Returns TRUE if the given ped is in the given vehicle and they are all alive and well
/// PARAMS:
///    ped - Ped to check
///    veh - The vehicle to check they are in
/// RETURNS:
///    TRUE if the ped is in the vehicle and they are both alive and well
FUNC BOOL IS_SAFE_PED_IN_VEHICLE(PED_INDEX ped, VEHICLE_INDEX veh)
	IF veh != NULL
		IF IS_VEHICLE_OK(veh)
			IF IS_PED_UNINJURED(ped)
				IF IS_PED_IN_VEHICLE(ped,veh)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Print a warning God text string and expires a bool when the warning has been shown
/// PARAMS:
///    WarnObj - the text key of the wanrning
///    expire - the bool to change
PROC PRINT_WARNING_OBJ(STRING WarnObj, BOOL &expire)
	IF NOT expire
		PRINT_NOW(WarnObj, DEFAULT_GOD_TEXT_TIME,0)
		expire = TRUE
	ENDIF
ENDPROC

/// PURPOSE:
///    Spawns and sets up the ped with a debug name
/// PARAMS:
///    index - The PED_INDEX to write the newly created ped to 
///    pos - The postion to create the ped at
///    fDir - The heading to give the new ped
///    modelName - The model to create the ped with
///    i - Debug number for Debug name
///    _name - The Debug name 
/// RETURNS:
///    TRUE if the ped was created
FUNC BOOL SETUP_PED(PED_INDEX &index, VECTOR pos, FLOAT fDir, MODEL_NAMES modelName, INT i, STRING _name) 
	
	IF SPAWN_PED(index, modelName, pos, fDir)
		#IF IS_DEBUG_BUILD
			SK_PRINT("SPAWNED")
		#ENDIF
		
		IF IS_PED_UNINJURED(index)
			TEXT_LABEL tDebugName = _name
			tDebugName += i
			#IF IS_DEBUG_BUILD
				SK_PRINT(tDebugName)
			#ENDIF
			SET_PED_NAME_DEBUG(index, tDebugName)
			RETURN TRUE
		ENDIF
	ENDIF	

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the heading between to vectors
/// PARAMS:
///    V1 - First vector
///    V2 - Second vector
/// RETURNS:
///    FLOAT -  the heading between the two
FUNC FLOAT GET_HEADING_BETWEEN_VECTORS(VECTOR V1, VECTOR V2)
	RETURN GET_HEADING_FROM_VECTOR_2D(V2.x-V1.x,V2.y-V1.y)
ENDFUNC


///-----------------------------------------------------------------------------------
///    						MISSION FUNCTIONS
///-----------------------------------------------------------------------------------

/// PURPOSE:
///    Tells a ped to leave a specified vehicle 
///    performs dead checks on the ped and vehicle
/// PARAMS:
///    ped - The ped to leave the vechicle
///    veh - The vehicle said ped is leaving
PROC GIVE_LEAVE_VEHICLE_ORDER(PED_INDEX ped, VEHICLE_INDEX veh)
	IF IS_VEHICLE_OK(veh)
		IF IS_PED_UNINJURED(ped)
			TASK_LEAVE_VEHICLE(ped, veh)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Tells a ped to enter a specified vehicle 
///    performs dead checks on the ped and vehicle
/// PARAMS:
///    ped - The ped to leave the vechicle
///    veh - The vehicle said ped is leaving
///    seat - Seate the ped should enter the car from
PROC GIVE_ENTER_VEHICLE_ORDER(PED_INDEX ped, VEHICLE_INDEX veh, VEHICLE_SEAT seat = VS_DRIVER)
	IF IS_VEHICLE_OK(veh)
		IF IS_PED_UNINJURED(ped)
			TASK_ENTER_VEHICLE(ped, veh, DEFAULT_TIME_BEFORE_WARP, seat)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Gives a ped a waypoint task
/// PARAMS:
///    ped - The ped to give the task to 
///    waypoint - The name of the waypoint rec to give the ped
PROC GIVE_WAYPOINT_TASK(PED_INDEX ped, STRING waypoint, INT iStartPoint = 0 , EWAYPOINT_FOLLOW_FLAGS eFollow = EWAYPOINT_DEFAULT)
	IF IS_PED_UNINJURED(ped)
		FREEZE_ENTITY_POSITION(ped, FALSE)
		CLEAR_PED_TASKS(ped)
		TASK_FOLLOW_WAYPOINT_RECORDING(ped, waypoint, iStartPoint, eFollow)
	ENDIF
ENDPROC

/// PURPOSE:
///    Gives a ped an attack order after the player has been spotted
///    Only called when the player fails the mission for getting spotted or attacking a ped
///    and possibly when the player has completed the sex scene section
/// PARAMS:
///    ped - The ped to give the order to
PROC GIVE_PED_ATTACK_ORDER(PED_INDEX ped)
	IF IS_PED_UNINJURED(ped)
		IF NOT IsPedPerformingTask(ped, SCRIPT_TASK_COMBAT)
			FREEZE_ENTITY_POSITION(ped, FALSE)
			CLEAR_PED_TASKS(ped)
			TASK_COMBAT_PED(ped, PLAYER_PED_ID())
			SET_PED_KEEP_TASK(ped, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Tells a ped to smart flee 200m from the player
///    Checks to see if the task is already being performed
///    Checks the ped is alive
/// PARAMS:
///    ped - the ped to flee
PROC GIVE_PED_FLEE_ORDER(PED_INDEX ped, BOOL bClearTasks = TRUE)
	IF IS_PED_UNINJURED(ped)
		IF NOT IsPedPerformingTask(ped, SCRIPT_TASK_SMART_FLEE_PED) 
			FREEZE_ENTITY_POSITION(ped, FALSE)
			IF bClearTasks
				CLEAR_PED_TASKS(ped)
			ENDIF
			TASK_SMART_FLEE_PED(ped, PLAYER_PED_ID(), 200, -1)
			SET_PED_KEEP_TASK(ped, TRUE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks that 2 entities are at the same height - within
///    1.5m
/// PARAMS:
///    e1 - first entity
///    e2 - second entity
/// RETURNS:
///    TRUE if the entities are at the same height
FUNC BOOL ARE_ENTITYS_AT_SAME_HEIGHT(ENTITY_INDEX e1, ENTITY_INDEX e2)
	VECTOR vE1 = GET_ENTITY_COORDS(e1)
	VECTOR vE2 = GET_ENTITY_COORDS(e2)
	FLOAT fDiff = ABSF(vE1.z - vE2.z) //want the absolute difference so it doesnt matter which entitys height was taken away first 
	IF fDiff <= 1.5
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Checks that the player is with in range of the hunter used when playing dialogue
/// PARAMS:
///    distance - The distance the player must be with in - defaults to 10.0
/// RETURNS:
///    TRUE if the player is with in the distance 
FUNC BOOL IS_IN_CONV_DISTANCE(PED_INDEX ped, FLOAT distance = 10.0)
	IF ped = NULL
		RETURN TRUE
	ENDIF
	
	IF IS_PED_UNINJURED(PLAYER_PED_ID())
	AND IS_PED_UNINJURED(ped)
//		#IF IS_DEBUG_BUILD SK_PRINT_FLOAT(" DISTANCE BETWEEN CONVO  === ", GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ped, FALSE))#ENDIF
		IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ped, FALSE) <= distance
//		AND ARE_ENTITYS_AT_SAME_HEIGHT(ped, PLAYER_PED_ID())
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Creates a conversation and then displays an objective
///    Expires a bool when the conversation has been created
///    assume the player is talking to another ped
///    kills the conversation if its started and the peds are too far away
/// PARAMS:
///    ped - the ped the conversation is with - pass in NULL if i doesnt matter where the other ped is in relation to the player 
///    bExpire - The bool to expire when the conversation has been created (ref)
///    sConvo - the string of the conversation to play
///    sObj - the text key for the obj
///    distance - the distance the player and ped need to be in before the convostation happens
PROC DO_CONVO_WITH_OBJ(PED_INDEX ped, BOOL &bExpire, STRING sConvo, STRING sObj, FLOAT distance = 10.0)
	
	IF NOT bObjectiveShown
//		#IF IS_DEBUG_BUILD SK_PRINT(sConvo) #ENDIF
//		#IF IS_DEBUG_BUILD SK_PRINT(" NOT bObjectiveShown") #ENDIF
//
		IF NOT bExpire
//			#IF IS_DEBUG_BUILD SK_PRINT("NOT bExpire") #ENDIF
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND IS_SCREEN_FADED_IN()
				IF IS_IN_CONV_DISTANCE(ped, distance)
					bExpire = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, sConvo, CONV_PRIORITY_MEDIUM)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_IN_CONV_DISTANCE(ped, distance)
				KILL_FACE_TO_FACE_CONVERSATION()
			ENDIF
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
				PRINT_OBJ(sObj, bObjectiveShown) 
		 	ENDIF
		ENDIF
	ENDIF	
	
ENDPROC

/// PURPOSE:
///    Creates a conversation and then displays an objective
///    And then creates another conversation
///    Expires a bool when each of the conversation have been created
///    assume the player is talking to another ped
///    kills the conversation if its started and the peds are too far away
/// PARAMS:
///    ped - the ped the conversation is with - pass in NULL if i doesnt matter where the other ped is in relation to the player 
///    bExpire - The expiry bool for the first conversation (ref)
///    bExpireAfterConv - bool for the second conversation (ref)
///    sConvo - the string of the 1st conversation to play
///    sConvToPlayAfterObj - the string 2nd of the conversation to play
///    sObj - the text key for the obj
///    distance - the distance the player and ped need to be in before the convostation happens
PROC DO_TWO_CONVO_WITH_OBJ(PED_INDEX ped, BOOL &bExpire, BOOL &bExpireAfterConv, STRING sConvo, STRING sConvToPlayAfterObj, STRING sObj, FLOAT distance = 10.0)
	
	IF NOT bObjectiveShown
//		#IF IS_DEBUG_BUILD SK_PRINT(" NOT bObjectiveShown") #ENDIF
		
		IF NOT bExpire
//			#IF IS_DEBUG_BUILD SK_PRINT(sConvo) #ENDIF
//			#IF IS_DEBUG_BUILD SK_PRINT("NOT bExpire") #ENDIF
			
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			AND IS_SCREEN_FADED_IN()
				IF IS_IN_CONV_DISTANCE(ped, distance)
					bExpire = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, sConvo, CONV_PRIORITY_MEDIUM)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_IN_CONV_DISTANCE(ped, distance)
				KILL_FACE_TO_FACE_CONVERSATION()
			ENDIF

			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
				PRINT_OBJ(sObj, bObjectiveShown) 
		 	ENDIF
		ENDIF
	ELSE
		IF NOT IS_THIS_PRINT_BEING_DISPLAYED(sObj)
		OR GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES) = 0
			IF NOT IS_STRING_NULL_OR_EMPTY(sConvToPlayAfterObj)
				IF NOT bExpireAfterConv
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()							
						IF IS_IN_CONV_DISTANCE(ped, distance)
							bExpireAfterConv = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, sConvToPlayAfterObj, CONV_PRIORITY_MEDIUM)
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_IN_CONV_DISTANCE(ped, distance)
						KILL_FACE_TO_FACE_CONVERSATION()
					ENDIF
				ENDIF
			ELSE
				bExpireAfterConv = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Creates a conversation then expires a bool when the conversation has been created
/// PARAMS:
///    ped - the ped the conversation is with - pass in NULL if i doesnt matter where the other ped is in relation to the player 
///    bExpire - The expiry bool for the first conversation (ref)
///    sConvo - the string of the conversation to play
///    distance - the distance the player and ped need to be in before the convostation happens
/// RETURNS:
///    true when the bool is expired
FUNC BOOL DO_CONVO(PED_INDEX ped, BOOL &bExpire, STRING sConvo, FLOAT distance = 10.0, enumConversationPriority priority = CONV_PRIORITY_MEDIUM)
	
	IF NOT bExpire
		
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND IS_SCREEN_FADED_IN()
			IF IS_IN_CONV_DISTANCE(ped, distance)
			AND NOT IS_THIS_PRINT_BEING_DISPLAYED("PR_TAKBACK")
				bExpire = CREATE_CONVERSATION(s_conversation_peds, sTextBlock, sConvo, priority)
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_IN_CONV_DISTANCE(ped, distance)
			KILL_FACE_TO_FACE_CONVERSATION()
		ENDIF
	ENDIF
	RETURN bExpire
ENDFUNC


PROC CONTROL_ABM_CONV(AMBIENT_DIALOGUE_CONV &conv)
	SWITCH conv.eAmbConv
		CASE ACS_START_LINE
			IF IS_PED_UNINJURED(conv.mConvLine[conv.iIndex].ped)
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(conv.mConvLine[conv.iIndex].ped, conv.mConvLine[conv.iIndex].sConvLine, conv.mConvLine[conv.iIndex].sVoice, "SPEECH_PARAMS_STANDARD")
				conv.mConvLine[conv.iIndex].iStartTime = GET_GAME_TIMER()
				#IF IS_DEBUG_BUILD SK_PRINT("ACS_START_LINE === ") #ENDIF
				#IF IS_DEBUG_BUILD SK_PRINT(conv.mConvLine[conv.iIndex].sConvLine) #ENDIF
				
				conv.eAmbConv = ACS_WAIT_LINE_FIN
			ENDIF
		BREAK
		
		CASE ACS_WAIT_LINE_FIN
			IF (GET_GAME_TIMER() - conv.mConvLine[conv.iIndex].iStartTime) > conv.mConvLine[conv.iIndex].iLineTime
				conv.mConvLine[conv.iIndex].bDone = TRUE
				conv.iIndex++
				#IF IS_DEBUG_BUILD SK_PRINT_INT("NEXT INDEX ===", conv.iIndex) #ENDIF
				conv.eAmbConv = ACS_PICK_NEXT_LINE
			ENDIF
		BREAK
		
		CASE ACS_PICK_NEXT_LINE
			#IF IS_DEBUG_BUILD SK_PRINT("ACS_PICK_NEXT_LINE") #ENDIF
			IF NOT conv.bIsRandom
				IF NOT ARE_STRINGS_EQUAL("END", conv.mConvLine[conv.iIndex].sConvLine)
					conv.eAmbConv = ACS_START_LINE
				ELSE
					conv.eAmbConv = ACS_FIN
				ENDIF
			ELSE
				//not implemented 
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Checks to see if the player has killed innocents then increaments the mission stat.
/// PARAMS:
///    bIsAfterOtherFail - Flag to see if the player has already failed the mission when this 
///    returns true and changes the mission fail reason accordingly.
PROC CHECK_FOR_MURDER()
	PED_INDEX mPed
	INT i = 0
	IF  Has_Ped_Been_Killed()
		REPEAT Get_Number_Of_Ped_Killed_Events() i
			IF DOES_ENTITY_EXIST(Get_Index_Of_Killed_Ped(i))
            	mPed = GET_PED_INDEX_FROM_ENTITY_INDEX(Get_Index_Of_Killed_Ped(i))
				IF mPed <> PLAYER_PED_ID()
					IF IS_ENTITY_A_PED(mPed)
						IF NOT IS_ENTITY_A_MISSION_ENTITY(mPed)
						AND IS_PED_HUMAN(mPed)
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mPed, PLAYER_PED_ID())
								INFORM_MISSION_STATS_OF_INCREMENT(JH1P_INNOCENTS_KILLED)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF	
ENDPROC

PROC SET_STATS_WATCH_OFF()
	#IF IS_DEBUG_BUILD SK_PRINT("Closing time window ") #ENDIF
	INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE, FHPA_ESCAPE_TIME)
	INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL) 
ENDPROC

PROC MANAGE_PLAYER_VEHICLE_STATS()
	IF NOT bDamageSpeedStatTurnedOn
		IF eStealCarState = ISCM_IN_VEHICLE
		OR eStealCarState = ISCM_IN_ANY_VEHICLE
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(mStealCar.id, FHPA_DAMAGE_TO_WAGON) 
			#IF IS_DEBUG_BUILD SK_PRINT("Opening time window ") #ENDIF
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(FHPA_ESCAPE_TIME)
			bDamageSpeedStatTurnedOn = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC MONITER_STATS()
//	CHECK_FOR_MURDER()
	MANAGE_PLAYER_VEHICLE_STATS()
	IF bMonitorWantedStatFlow
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREP_GOT_WANTED, TRUE)
			bMonitorWantedStatFlow = FALSE
		ENDIF
	ENDIF
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Called when the player has failed 
///    deletes blips and clears the screen of text and conversations are ended
/// PARAMS:
///    fail - An Enum of the fail reason used in a switch statment to pick the correct text to display
PROC MISSION_FAILED(FAILED_REASONS fail = FR_NONE)
	
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()
	TRIGGER_MUSIC_EVENT("FHPRA_FAIL")

	SWITCH fail		
		CASE FR_NONE
			
		BREAK
		
		CASE FR_WRECKED_STEAL_CAR
			sFailReason = "PR_FWRECK"
		BREAK
		
		CASE FR_ROOFSTUCK_STEAL_CAR
			sFailReason = "PR_FSTUCK"
		BREAK
		
		CASE FR_LEFT_COPS
			sFailReason = "PR_FLEFT"
		BREAK
		
		CASE FR_LEFT_VAN
			sFailReason = "PR_FLEFTV"
		BREAK
	ENDSWITCH
	
	eMissionState = MS_FAILED
	eState = SS_INIT
	
ENDPROC

///-----------------------------------------------------------------------------------
///    						STATE MACHINES
///-----------------------------------------------------------------------------------

PROC CREATE_RELATIONSHIP_GROUP()
	ADD_RELATIONSHIP_GROUP("COPS", HASH_ENEMIES)
ENDPROC


/// PURPOSE:
///    Sets the mission check point 
PROC SET_CHECKPOINT()

ENDPROC

/// PURPOSE:
///    Loads a scene around a point used to the player 
///    seeing the map stream in when starting a replay or debug skipping
/// PARAMS:
///    pos - the position to stream the scene around at
///    rad - the radius of the scene to load
PROC LOAD_SCENE_SKIP(VECTOR pos, FLOAT rad)
	NEW_LOAD_SCENE_START_SPHERE(pos, rad)
	INT i_load_scene_timer = GET_GAME_TIMER()
	WHILE (NOT IS_NEW_LOAD_SCENE_LOADED()) AND (GET_GAME_TIMER() - i_load_scene_timer < 12000)
		WAIT(0)
	ENDWHILE
	NEW_LOAD_SCENE_STOP()
ENDPROC

/// PURPOSE:
///    Adds peds for dialogue based off the Mission state
PROC ADD_PEDS_FOR_DIALOGUE()
	INT iVoice
	STRING sVoice
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	IF ePlayer = CHAR_MICHAEL
		iVoice = 0
		sVoice = "MICHAEL"
	ELIF ePlayer = CHAR_FRANKLIN
		iVoice = 1
		sVoice = "FRANKLIN"
	ELIF ePlayer = CHAR_TREVOR
		iVoice = 2
		sVoice = "TREVOR"
	ENDIF

	ADD_PED_FOR_DIALOGUE(s_conversation_peds, iVoice, PLAYER_PED_ID(), sVoice)
	ADD_PED_FOR_DIALOGUE(s_conversation_peds, 6, mCop[0].id, "BSPRACopBreak1")
	ADD_PED_FOR_DIALOGUE(s_conversation_peds, 7, mCop[1].id, "BSPRACopBreak2")
	ADD_PED_FOR_DIALOGUE(s_conversation_peds, 5, mCop[2].id, "BSPRACopBreak3")
ENDPROC

PROC ACTIVATE_COPS()
	INT i
	FOR i=0 TO (MAX_COPS-1)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mCop[i].id, FALSE)
		
		IF NOT IsPedPerformingTask(mCop[i].id, SCRIPT_TASK_START_SCENARIO_AT_POSITION)
			SWITCH i
				CASE 0
				FALLTHRU
				CASE 1
					TASK_START_SCENARIO_AT_POSITION(mCop[i].id, "WORLD_HUMAN_HANG_OUT_STREET", mCop[i].vPos, mCop[i].fDir, 0, FALSE)
				BREAK

				CASE 2
					TASK_USE_MOBILE_PHONE(mCop[i].id, TRUE)
				BREAK
				
			ENDSWITCH
		ENDIF
		GIVE_WEAPON_TO_PED(mCop[i].id, WEAPONTYPE_PISTOL, INFINITE_AMMO, FALSE, FALSE)
		SET_PED_RELATIONSHIP_GROUP_HASH(mCop[i].id, HASH_ENEMIES)
		
//		IF i = COP_3
//			FORCE_PED_AI_AND_ANIMATION_UPDATE(mCop[i].id)
//		ENDIF
		mCop[i].eBehaviour = CBS_NOT_ALERTED
		mCop[i].eNotAlerted = CNAS_USING_SCENARIO
	ENDFOR
ENDPROC

PROC SET_COP_VARIATION(INT i)
	IF IS_PED_UNINJURED(mCop[i].id)
		IF i=0
			SET_PED_COMPONENT_VARIATION(mCop[i].id, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(mCop[i].id, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(mCop[i].id, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(mCop[i].id, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(mCop[i].id, INT_TO_ENUM(PED_COMPONENT,9), 1, 0, 0) //(task)
			SET_PED_COMPONENT_VARIATION(mCop[i].id, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
		ELSE
			SET_PED_COMPONENT_VARIATION(mCop[i].id, INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(mCop[i].id, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(mCop[i].id, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(mCop[i].id, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
			SET_PED_COMPONENT_VARIATION(mCop[i].id, INT_TO_ENUM(PED_COMPONENT,9), 2, 0, 0) //(task)
			SET_PED_COMPONENT_VARIATION(mCop[i].id, INT_TO_ENUM(PED_COMPONENT,10), 0, 1, 0) //(decl)	
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Sets up a stage requirment via a switch using an ENUM 
///    Stage requirements include the hunter the saleform or the mission text etc.
/// PARAMS:
///    missionReq - The Enum of the required mission element e.g. RQ_TEXT
///    pos - The position the thing is spawned at - if no position is required the use vSafeVec
///    		 If spawning multiple things at once then have the postions already in the switch statement
///    		 as calling this func multiple times wont work as well with the other function that calls it 
///    dir - This is the heading or direction you want the thing to face when spawned. Defaults to 
///    		 0.0 
/// RETURNS:
///    TRUE when the thing required is created/loaded/setup or whatever.
///    
FUNC BOOL SETUP_STAGE_REQUIREMENTS(MISSION_REQ missionReq,VECTOR pos, FLOAT dir=0.0)
	SWITCH missionReq
		CASE RQ_NONE
			IF ARE_VECTORS_ALMOST_EQUAL(pos, vSafeVec)
			AND dir = 0.0
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE RQ_TEXT
			REQUEST_ADDITIONAL_TEXT(sGodText, MISSION_TEXT_SLOT)
			REQUEST_ADDITIONAL_TEXT(sTextBlock, MISSION_DIALOGUE_TEXT_SLOT)
			
			IF HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
			AND HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
				#IF IS_DEBUG_BUILD
					SK_PRINT("RQ_TEXT")
				#ENDIF
				RETURN TRUE
			ENDIF
			#IF IS_DEBUG_BUILD
				SK_PRINT("TEXT FAILED")
			#ENDIF
		BREAK
		
		CASE RQ_STEAL_CAR		
			IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[0])
			    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
				mStealCar.id = g_sTriggerSceneAssets.veh[0]
				
				IF IS_VEHICLE_OK(mStealCar.id)
					IF NOT DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(mStealCar.id)
						ADD_VEHICLE_UPSIDEDOWN_CHECK(mStealCar.id)
					ENDIF
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mStealCar.id, SC_DOOR_REAR_LEFT, FALSE)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mStealCar.id, SC_DOOR_REAR_RIGHT, FALSE)
					SET_VEHICLE_AS_RESTRICTED(mStealCar.id, 0)
					SET_VEHICLE_HAS_STRONG_AXLES(mStealCar.id, TRUE)
					SET_VEHICLE_DOORS_LOCKED(mStealCar.id, VEHICLELOCK_UNLOCKED)
					RETURN TRUE
				ENDIF
			ELSE
				IF SPAWN_VEHICLE(mStealCar.id, mStealCar.Mod, mStealCar.vPos, mStealCar.fDir)
					#IF IS_DEBUG_BUILD
						SK_PRINT("RQ_STEAL_CAR")
					#ENDIF
					IF NOT DOES_VEHICLE_HAVE_STUCK_VEHICLE_CHECK(mStealCar.id)
						ADD_VEHICLE_UPSIDEDOWN_CHECK(mStealCar.id)
					ENDIF
					SET_VEHICLE_AS_RESTRICTED(mStealCar.id, 0)
					SET_VEHICLE_HAS_STRONG_AXLES(mStealCar.id, TRUE)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mStealCar.id, SC_DOOR_REAR_LEFT, FALSE)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(mStealCar.id, SC_DOOR_REAR_RIGHT, FALSE)
					SET_VEHICLE_DOORS_LOCKED(mStealCar.id, VEHICLELOCK_UNLOCKED)
					RETURN TRUE
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD
				SK_PRINT("RQ_STEAL_CAR FAILED")
			#ENDIF		
		BREAK
		
		CASE RQ_COPS
			INT i
			INT iCount
			iCount = 0
			FOR i=0 TO (MAX_COPS-1)
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[i])
					IF NOT IS_PED_DEAD_OR_DYING(g_sTriggerSceneAssets.ped[i])
					    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[i], TRUE, TRUE)
						mCop[i].id = g_sTriggerSceneAssets.ped[i]

						IF IS_PED_UNINJURED(mCop[i].id)
							SET_PED_TARGET_LOSS_RESPONSE(mCop[i].id, TLR_SEARCH_FOR_TARGET)
							STOP_PED_SPEAKING(mCop[i].id, TRUE)
							SET_COP_VARIATION(i)
							iCount++
						ENDIF
					ELSE
						iCount++
					ENDIF
				ELSE
					IF SPAWN_PED(mCop[i].id, mCop[i].Mod, mCop[i].vPos, mCop[i].fDir, FALSE, FALSE, FALSE)
						SET_COP_VARIATION(i)
						STOP_PED_SPEAKING(mCop[i].id, TRUE)
						SET_PED_TARGET_LOSS_RESPONSE(mCop[i].id, TLR_SEARCH_FOR_TARGET)

						TEXT_LABEL tDebugName 
						tDebugName = "COP "
						tDebugName += i
						#IF IS_DEBUG_BUILD
							SK_PRINT(tDebugName)
						#ENDIF
						SET_PED_NAME_DEBUG(mCop[i].id, tDebugName)

						iCount++
					ENDIF
				ENDIF
			ENDFOR

			IF iCount = MAX_COPS
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE RQ_COP_CARS
			INT c
			INT p
			p = 0
			INT iCarCount
			iCarCount = 0
			
			FOR c=1 TO (MAX_COP_CARS)
				IF IS_VEHICLE_OK(g_sTriggerSceneAssets.veh[c])
				    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[c], TRUE, TRUE)
					mCopCars[p].id = g_sTriggerSceneAssets.veh[c]
					
					IF IS_VEHICLE_OK(mCopCars[p].id)
						iCarCount++
					ENDIF
				ELSE
					IF SPAWN_VEHICLE(mCopCars[p].id, mCopCars[p].Mod, mCopCars[p].vPos, mCopCars[p].fDir)
						iCarCount++
					ENDIF
				ENDIF
				p++
			ENDFOR
			IF iCarCount = MAX_COP_CARS
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE RQ_STINGERS
			IF SPAWN_OBJECT(oiStingers, prop_box_guncase_03a, <<354.6938, -1634.7828, 22.7849>>, 299.1508)
				IF IS_VEHICLE_OK(mStealCar.id)
					ATTACH_ENTITY_TO_ENTITY(oiStingers, mStealCar.id, 0, <<0, -1.85, 0.05>>, GET_ENTITY_ROTATION(mStealCar.id))
					IF IS_ENTITY_ATTACHED_TO_ENTITY(mStealCar.id, oiStingers)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Used to setup a mission stage(or state if you call it that) Uses a switch statement to pick which 
///    set of SETUP_STAGE_REQUIREMENTS() to call. It checks to see if all the stage requirements are 
///    setup and then does any other setup needed. such as setting the players position or switching a
///    bool to true or false etc.
///    Handles setting up stuff needed after a Z or p skip first then the normal setup takes place
/// PARAMS:
///    eStage - The mission state/stage that needs setting up
///    bJumped - Wether or not the state/stage has been jumped to using Z or P skips
/// RETURNS:
///    TRUE if everything required for a stage is loaded properly
FUNC BOOL SETUP_MISSION_STAGE(MISSION_STATE eStage, BOOL bJumped = FALSE)
	SWITCH eStage
		CASE MS_SET_UP
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
				RC_END_Z_SKIP()
			ELSE
				IF SETUP_STAGE_REQUIREMENTS(RQ_TEXT, vSafeVec)
					IF SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR, mStealCar.vPos, mStealCar.fDir)
					AND SETUP_STAGE_REQUIREMENTS(RQ_COP_CARS, vSafeVec)
					AND SETUP_STAGE_REQUIREMENTS(RQ_COPS, vSafeVec)
					AND SETUP_STAGE_REQUIREMENTS(RQ_STINGERS, vSafeVec)
						iNavBlocker = ADD_NAVMESH_BLOCKING_OBJECT(<<400.86282, -1609.73865, 28.29278>>, <<7.50, 2, 9.50>>, DEG_TO_RAD(229.3746))
						IF GET_BLIP_FROM_ENTITY(mStealCar.id) != NULL
							BLIP_INDEX blip 
							blip = GET_BLIP_FROM_ENTITY(mStealCar.id)
							SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(TRUE)
							SAFE_REMOVE_BLIP(blip)
							SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(FALSE)
						ENDIF
						ADD_SAFE_BLIP_TO_VEHICLE(biBlip, mStealCar.id, TRUE)
						
						ACTIVATE_COPS()
						ADD_PEDS_FOR_DIALOGUE()
						RETURN TRUE //all mission stage requirements set up return true and activate stage
					ENDIF
				ENDIF
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_SET_UP, FAILED") 
			#ENDIF
		BREAK

		CASE MS_STEAL
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				IF SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR, mStealCar.vPos, mStealCar.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_COP_CARS, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_COPS, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_STINGERS, vSafeVec)
					SET_PED_POS(PLAYER_PED_ID(), <<408.3364, -1567.4294, 28.2723>>, 128.2512)
					RESET_ALL()
					CLEAR_AREA_OF_PEDS(<<369.0115, -1609.1163, 28.2928>>, 5)
					CLEAR_AREA_OF_PEDS(mCop[0].vPos, 5)
					ACTIVATE_COPS()
					ADD_PEDS_FOR_DIALOGUE()
					bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
					RC_END_Z_SKIP()
				ENDIF
			ELSE
				RETURN TRUE //all mission stage requirements set up return true and activate stage
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_STEAL, FAILED") 
			#ENDIF
		BREAK

		CASE MS_RETURN_WITH_THING
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				IF SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR, mStealCar.vPos, mStealCar.fDir)
				AND SETUP_STAGE_REQUIREMENTS(RQ_COP_CARS, vSafeVec)
				AND SETUP_STAGE_REQUIREMENTS(RQ_COPS, vSafeVec)
					RESET_ALL()
					CLEAR_AREA_OF_PEDS(<<369.0115, -1609.1163, 28.2928>>, 5)
					CLEAR_AREA_OF_PEDS(mCop[0].vPos, 5)
					ACTIVATE_COPS()
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), mStealCar.id)
					ADD_PEDS_FOR_DIALOGUE()
					bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
					RC_END_Z_SKIP()
				ENDIF
			ELSE
				RETURN TRUE //all mission stage requirements set up return true and activate stage
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_RETURN_WITH_THING, FAILED") 
			#ENDIF
		BREAK
		
		CASE MS_LEAVE_VEHICLE_RING_LEST
			IF bJumped //If we have jumped to this stage set up everything that might of been cleared buy the jump to funtions 
				IF SETUP_STAGE_REQUIREMENTS(RQ_STEAL_CAR, vDropOffLoc, mStealCar.fDir)
					RESET_ALL()
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), mStealCar.id)
					ADD_PEDS_FOR_DIALOGUE()
					bJumpSkip = FALSE // everything has been setup that is not key to this stage but is required to carry the mission on from this stage so reset the jump flag
					RC_END_Z_SKIP()
				ENDIF
			ELSE
				RETURN TRUE //all mission stage requirements set up return true and activate stage
			ENDIF
			#IF IS_DEBUG_BUILD  
			SK_PRINT("MS_RETURN_WITH_THING, FAILED") 
			#ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Sits in the NEXT_STAGE() function and moniters for assest that
///    should be unloaded based on the mission state
/// PARAMS:
///    state - the current state we should evaluate
PROC MONITER_UNLOAD_ASSETS(MISSION_STATE state)
	SWITCH state
		CASE MS_SET_UP
			
		BREAK
		
	ENDSWITCH
ENDPROC

// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Unloads all models
PROC UNLOAD_ALL_MODELS()
	SET_MODEL_AS_NO_LONGER_NEEDED(mStealCar.Mod)
	INT c
	FOR c=0 TO (MAX_COP_CARS-1)
		SET_MODEL_AS_NO_LONGER_NEEDED(mCopCars[c].Mod)
	ENDFOR
ENDPROC

/// PURPOSE:
///    Unloads all waypoints 
PROC UNLOAD_ALL_WAYPOINTS()
ENDPROC

/// PURPOSE:
///    Unloads all anims
PROC UNLOAD_ANIMS()
ENDPROC

/// PURPOSE:
///    Unload all vehicle recordings 
PROC UNLOAD_ALL_CAR_RECS()
ENDPROC

/// PURPOSE:
///    Deletes any blips that are valid
PROC REMOVE_BLIPS()
	SAFE_REMOVE_BLIP(biBlip)
	SAFE_REMOVE_BLIP(biVehicleBlip)
ENDPROC


/// PURPOSE:
///    Standard delete all function used the wait for fail state
///    Safe deletes all peds, props and vehicles
PROC DELETE_ALL()
	REMOVE_VEHICLE_UPSIDEDOWN_CHECK(mStealCar.id)
	SAFE_DELETE_VEHICLE(mStealCar.id)
	SAFE_DELETE_OBJECT(oiStingers)
	INT i
	FOR  i=0 TO (MAX_COPS-1)
		SAFE_DELETE_PED(mCop[i].id)
	ENDFOR
	FOR i=0 TO (MAX_COP_CARS-1)
		SAFE_DELETE_VEHICLE(mCopCars[i].id)
	ENDFOR
ENDPROC

PROC RELEASE_ALL()
	REMOVE_VEHICLE_UPSIDEDOWN_CHECK(mStealCar.id)
	SAFE_RELEASE_VEHICLE(mStealCar.id)
	SAFE_RELEASE_OBJECT(oiStingers)
	INT i
	FOR  i=0 TO (MAX_COPS-1)
		SAFE_RELEASE_PED(mCop[i].id)
	ENDFOR
	
	FOR i=0 TO (MAX_COP_CARS-1)
		SAFE_RELEASE_VEHICLE(mCopCars[i].id)
	ENDFOR
ENDPROC

PROC RELEASE_BATTLE_BUDDIES()
	// For each playable character...
	enumCharacterList eChar
	REPEAT MAX_BATTLE_BUDDIES eChar
		
		PED_INDEX hBuddy = GET_BATTLEBUDDY_PED(eChar)
		
		IF NOT IS_PED_INJURED(hBuddy)
			IF IS_BATTLEBUDDY_OVERRIDDEN(hBuddy)
				RELEASE_BATTLEBUDDY(hBuddy)
			ENDIF
		ENDIF
	
	ENDREPEAT
ENDPROC


/// PURPOSE:
///    Deletes all mission entities and any other clean up
///    This is used to clear the mission when P or Z skipping
PROC CLEANUP(BOOL bDelAll = TRUE)
	IF bDelAll
		CLEAR_AREA(<<372.9868, -1623.5313, 28.2928>>, 100, TRUE)
		#IF IS_DEBUG_BUILD SK_PRINT("CLEANUP = DEL ALL") #ENDIF
	ELSE
		#IF IS_DEBUG_BUILD SK_PRINT("CLEANUP = RELEASE ") #ENDIF
	ENDIF

	KILL_FACE_TO_FACE_CONVERSATION()
	CLEAR_PRINTS()
	WAIT_FOR_CUTSCENE_TO_STOP()

	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	RELEASE_BATTLE_BUDDIES()
	REMOVE_BLIPS()
	UNLOAD_ALL_MODELS()
	UNLOAD_ALL_WAYPOINTS()
	UNLOAD_ANIMS()
	UNLOAD_ALL_CAR_RECS()
	IF bDelAll
		IF DOES_ENTITY_EXIST(mStealCar.id)
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), mStealCar.id)
						VECTOR vPos = GET_ENTITY_COORDS(PLAYER_PED_ID()) // get out of vehicle
						SET_ENTITY_COORDS(PLAYER_PED_ID(), vPos)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		DELETE_ALL()
	ELSE
		RELEASE_ALL()
	ENDIF
	SET_GPS_MULTI_ROUTE_RENDER(FALSE)

	IF DOES_CAM_EXIST(camMain)
		RENDER_SCRIPT_CAMS(FALSE,FALSE)
		DESTROY_CAM(camMain)
	ENDIF
ENDPROC


/// PURPOSE:
///    Cleans up mission entities, releases the entity to be cleaned up by population
///    and will give a suitable task to the peds before clean up
PROC Script_Cleanup()
//	CLEAR_ADDITIONAL_TEXT(MISSION_DIALOGUE_TEXT_SLOT,TRUE)
	REMOVE_RELATIONSHIP_GROUP(HASH_ENEMIES)
//	SERVICES_TOGGLE(TRUE)
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)

	IF IS_DOOR_REGISTERED_WITH_SYSTEM(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID)
		SET_SCRIPT_UPDATE_DOOR_AUDIO(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID, FALSE)
	ENDIF

	DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID, 0, TRUE, TRUE)
	DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID, DOORSTATE_FORCE_LOCKED_THIS_FRAME, TRUE, TRUE)
	
	DOOR_SYSTEM_SET_DOOR_STATE(iSideGate, DOORSTATE_UNLOCKED)
	REMOVE_DOOR_FROM_SYSTEM(iSideGate)

	REMOVE_SCENARIO_BLOCKING_AREA(siCopStation)
	SET_STATS_WATCH_OFF()

	SET_MAX_WANTED_LEVEL(5)
	SET_WANTED_LEVEL_MULTIPLIER(1)
	RESET_ALL_BATTLEBUDDY_BEHAVIOUR_REQUESTS()
	
	UNREGISTER_SCRIPT_WITH_AUDIO()
	
	CLEANUP(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

//***************************************
//			:MISSION FLOW FUNC:
//***************************************
//PURPOSE: Advances or reverses the mission stage  
PROC NEXT_STAGE( BOOL bReverse = FALSE)
	MONITER_UNLOAD_ASSETS(eMissionState)
	iMissionState = ENUM_TO_INT(eMissionState)
	IF NOT bReverse
		eMissionState = INT_TO_ENUM(MISSION_STATE, (iMissionState + 1))
	ELSE
		IF iMissionState > 0
			eMissionState = INT_TO_ENUM(MISSION_STATE, (iMissionState - 1))		
		ENDIF
	ENDIF
	bObjectiveShown = FALSE
	eState = SS_INIT	
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Script Pass
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Pass function calls cleanup and termination 
///    if the player debug passes this function warps him to the end 
///    of the chasem, it will then complete
PROC Script_Passed()
	CLEAR_PRINTS()
	KILL_ANY_CONVERSATION()

	Mission_Flow_Mission_Passed()
	Script_Cleanup()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Script Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets where the player will be warped to if they accept the replay
PROC SET_REPLAY_ACCEPTED_WARP_LOCATION()
	MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<475.3364, -1409.7823, 28.3108>>, 123.1614)
	SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<471.5946, -1412.7371, 28.1562>>, 68.5531)
ENDPROC


/// PURPOSE:
///    Waits for the screen to fade out then updates failed reason
PROC FAILED_WAIT_FOR_FADE()
	SWITCH eState
		CASE SS_INIT

			CLEAR_PRINTS()
			CLEAR_HELP()
			REMOVE_BLIPS()
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sFailReason)
				MISSION_FLOW_MISSION_FAILED_WITH_REASON(sFailReason)  
			ELSE
				MISSION_FLOW_MISSION_FAILED()
			ENDIF
			
			eState = SS_ACTIVE
		BREAK

		CASE SS_ACTIVE
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				
				// if replay accepted, warp to short distance away from the police station
				// (far enough away so that the mission gets blipped)
				IF HAS_PLAYER_ACCEPTED_REPLAY()
					SET_REPLAY_ACCEPTED_WARP_LOCATION()
				ELSE
					// if he rejects the replay only warp him to just outside station if he failed inside it
					IF IS_POINT_IN_POLY_2D(mAreaCheck, GET_ENTITY_COORDS(PLAYER_PED_ID()))
						// failed in police station, warp outside
						MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<408.3364, -1567.4294, 28.2723>>, 128.2512)
						SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<471.5946, -1412.7371, 28.1562>>, 68.5531)
					ENDIF
				ENDIF
				CLEAR_AREA(<<372.9868, -1623.5313, 28.2928>>, 100, TRUE)
				DELETE_ALL()
				Script_Cleanup() // must only take 1 frame and terminate the thread
			ELSE
				// not finished fading out
				// you may want to handle dialogue etc here.
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


//------------------------------------------------------------------------------------
//							MISSION STATES
//------------------------------------------------------------------------------------

FUNC BOOL DOES_BUDDY_NEED_TO_JUMP_FENCE(INT i, PED_INDEX hPed)

	IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
		IF IS_ENTITY_IN_ANGLED_AREA(hPed, <<344.98444, -1604.01135, 28.29278>>, <<339.72116, -1599.48352, 31.29056>>, 3.63)
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<345.15421, -1604.17322, 28.29278>>, <<360.88577, -1617.22986, 31.29278>>, 3.38)
			iClimbPoint = COP_ALLEY
			eBattleBuddyBehaviour[i] = BBB_CLIMB
			RETURN TRUE
		ELIF IS_ENTITY_IN_ANGLED_AREA(hPed, <<345.15421, -1604.17322, 28.29278>>, <<360.88577, -1617.22986, 31.29278>>, 3.63)
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<344.98444, -1604.01135, 28.29278>>, <<339.72116, -1599.48352, 31.29056>>, 3.38)
			iClimbPoint = COP_COMP
			eBattleBuddyBehaviour[i] = BBB_CLIMB
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL BATTLE_BUDDIES_CLIMB(INT i, PED_INDEX ped)
	SWITCH eBattleBuddyState[i]
		CASE BBOS_WAITING
			IF NOT IS_POSITION_OCCUPIED(mClimbPoint[iClimbPoint].vPos, 1.5, FALSE, FALSE, TRUE, FALSE, FALSE, ped)
				FLOAT fSpeed
				IF IS_ENTITY_IN_RANGE_COORDS(ped, mClimbPoint[iClimbPoint].vPos, 5)
					fSpeed = PEDMOVEBLENDRATIO_WALK
				ELSE
					fSpeed = PEDMOVEBLENDRATIO_RUN
				ENDIF
				
				TASK_FOLLOW_NAV_MESH_TO_COORD(ped, mClimbPoint[iClimbPoint].vPos, fSpeed, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, mClimbPoint[iClimbPoint].fDir )
				eBattleBuddyState[i] = BBOS_GO_TO_POSITION
			ENDIF
		BREAK
		
		CASE BBOS_GO_TO_POSITION
			IF NOT IsPedPerformingTask(ped, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
				TASK_CLIMB(ped, FALSE)
				eBattleBuddyState[i] = BBOS_JUMPING_FENCE
			ENDIF
		BREAK
		
		CASE BBOS_JUMPING_FENCE
			IF NOT IsPedPerformingTask(ped, SCRIPT_TASK_CLIMB)
				eBattleBuddyState[i] = BBOS_END
			ENDIF
		BREAK
		
		CASE BBOS_END
			eBattleBuddyState[i] = BBOS_WAITING
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_BUDDY_NEED_TO_DRIVE_TRUCK(INT i, PED_INDEX hPed)

	IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()
	
		IF DOES_ENTITY_EXIST(mStealCar.id)
		AND IS_VEHICLE_DRIVEABLE(mStealCar.id)
		AND NOT IS_ENTITY_AT_COORD(mStealCar.id, vDropOffLoc, <<2,2,2>>)
					
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), mStealCar.id)
			AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			
				IF IS_PED_IN_VEHICLE(hPed, mStealCar.id)
				AND GET_PED_IN_VEHICLE_SEAT(mStealCar.id, VS_DRIVER) = hPed
					eBattleBuddyBehaviour[i] = BBB_DRIVE
					RETURN TRUE
				
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC


FUNC BOOL BATTLE_BUDDIES_DRIVE(INT i, PED_INDEX ped)
	SWITCH eBattleBuddyState[i]
		CASE BBOS_WAITING
			TASK_VEHICLE_MISSION_COORS_TARGET(ped, GET_VEHICLE_PED_IS_IN(ped), vDropOffLoc, MISSION_GOTO, 20.0, DRIVINGMODE_AVOIDCARS, 3.5, 5.0)
			eBattleBuddyState[i] = BBOS_DRIVING_TO_PLACE
		BREAK
		
		CASE BBOS_DRIVING_TO_PLACE
			IF NOT DOES_BUDDY_NEED_TO_DRIVE_TRUCK(i, ped)
			OR IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), vDropOffLoc, 2)
				eBattleBuddyState[i] = BBOS_END
			ENDIF
		BREAK
		
		CASE BBOS_END
			eBattleBuddyState[i] = BBOS_WAITING
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC MONITOR_BATTLE_BUDDIES()
	// For each playable character...
	enumCharacterList eChar
	REPEAT MAX_BATTLE_BUDDIES eChar
		
		PED_INDEX hBuddy = GET_BATTLEBUDDY_PED(eChar)
		
		IF NOT IS_PED_INJURED(hBuddy)
			SWITCH eBattleBuddyBehaviour[ENUM_TO_INT(eChar)]
				CASE BBB_CONTROL
					IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddy)
						
						// Does script need to take control of buddy and drive to dest?
						IF IS_BATTLEBUDDY_AVAILABLE(hBuddy, FALSE)
							IF DOES_BUDDY_NEED_TO_JUMP_FENCE(ENUM_TO_INT(eChar), hBuddy)
							OR DOES_BUDDY_NEED_TO_DRIVE_TRUCK(ENUM_TO_INT(eChar), hBuddy)
								IF OVERRIDE_BATTLEBUDDY(hBuddy, FALSE)
									SET_ENTITY_AS_MISSION_ENTITY(hBuddy, TRUE, TRUE)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddy, TRUE)
									CLEAR_PED_TASKS(hBuddy)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK

				CASE BBB_CLIMB
					IF BATTLE_BUDDIES_CLIMB(ENUM_TO_INT(eChar), hBuddy)
					AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddy)
						// Buddy drives truck to destination
						eBattleBuddyBehaviour[ENUM_TO_INT(eChar)] = BBB_CONTROL
						RELEASE_BATTLEBUDDY(hBuddy)
					ENDIF
				BREAK
				
				CASE BBB_DRIVE
					IF BATTLE_BUDDIES_DRIVE(ENUM_TO_INT(eChar), hBuddy)
					AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddy)
						// Buddy drives truck to destination
						eBattleBuddyBehaviour[ENUM_TO_INT(eChar)] = BBB_CONTROL
						RELEASE_BATTLEBUDDY(hBuddy)
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
	
	ENDREPEAT
ENDPROC

PROC OPEN_GATE(INT iGate, FLOAT &fOpen)
	IF fOpen != 1.0
		DOOR_SYSTEM_SET_OPEN_RATIO(iGate, fOpen, TRUE, TRUE)
		fOpen = fOpen +@ 0.2
		IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST( iNavBlocker )
			REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlocker)
		ENDIF
//		#IF IS_DEBUG_BUILD
//			SK_PRINT_FLOAT("fFrontGateOpenRatio = ", fFrontGateOpenRatio)
//		#ENDIF

		IF fOpen >= 1.0
			fOpen = 1.0
		ENDIF
	ENDIF
ENDPROC

PROC CLOSE_GATE(INT iGate, FLOAT &fOpen)
	IF IS_DOOR_REGISTERED_WITH_SYSTEM(iGate)
		IF fOpen != 0.0
			IF NOT DOES_NAVMESH_BLOCKING_OBJECT_EXIST( iNavBlocker )
				iNavBlocker = ADD_NAVMESH_BLOCKING_OBJECT(<<400.86282, -1609.73865, 28.29278>>, <<7.50, 2, 9.50>>, DEG_TO_RAD(229.3746))
			ENDIF
			DOOR_SYSTEM_SET_OPEN_RATIO(iGate, fOpen, TRUE, TRUE)
			fOpen = fOpen -@ 0.2

	//		#IF IS_DEBUG_BUILD
	//			SK_PRINT_FLOAT("fFrontGateOpenRatio = ", fFrontGateOpenRatio)
	//		#ENDIF

			IF fOpen <= 0.0
				DOOR_SYSTEM_SET_DOOR_STATE(iGate, DOORSTATE_LOCKED)
				REMOVE_NAVMESH_BLOCKING_OBJECT(iNavBlocker)
				fOpen = 0.0
			ENDIF
		ENDIF
	ELSE
		SCRIPT_ASSERT("DOOR isnt registered")
	ENDIF
ENDPROC

PROC MONITOR_SIDE_GATE()
	IF IS_COP_PED_IN_AREA_3D(<<385.85281, -1629.94482, 35.29278>>, <<398.80630, -1640.86536, 28.0>>)
		DOOR_SYSTEM_SET_DOOR_STATE(iSideGate, DOORSTATE_UNLOCKED, TRUE, TRUE)
	ELSE
		DOOR_SYSTEM_SET_DOOR_STATE(iSideGate, DOORSTATE_LOCKED, TRUE, TRUE)
	ENDIF
ENDPROC

PROC MONITOR_FRONT_GATE()
	IF bIsPlayerWanted
		IF IS_COP_PED_IN_AREA_3D(<<387.00583, -1641.53748, 20.489498>>, <<404.14117, -1580.30176, 40.34303>>)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<390.49271, -1621.06958, 28.29278>>, <<406.0266, -1604.3584, 35.2086>>, 15)
			IF IS_POINT_IN_POLY_2D(mAreaCheck, GET_ENTITY_COORDS(PLAYER_PED_ID()))
				OPEN_GATE(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID, fFrontGateOpenRatio)
				EXIT
			ENDIF
		ELSE
			CLOSE_GATE(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID, fFrontGateOpenRatio)
		ENDIF
	ELSE
		IF IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID())
		OR eStealCarState = ISCM_IN_VEHICLE
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<390.49271, -1621.06958, 28.29278>>, <<406.0266, -1604.3584, 35.2086>>, 15)
			OR IS_COP_PED_IN_AREA_3D(<<387.00583, -1641.53748, 20.489498>>, <<404.14117, -1580.30176, 40.34303>>)
				OPEN_GATE(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID, fFrontGateOpenRatio)
			ELSE
				CLOSE_GATE(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID, fFrontGateOpenRatio)
			ENDIF
		ELSE
			IF NOT IS_COP_PED_IN_AREA_3D(<<387.00583, -1641.53748, 20.489498>>, <<404.14117, -1580.30176, 40.34303>>)
				CLOSE_GATE(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID, fFrontGateOpenRatio)
			ELSE
				OPEN_GATE(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID, fFrontGateOpenRatio)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MONITOR_GATE()
	MONITOR_FRONT_GATE()
	MONITOR_SIDE_GATE()
ENDPROC


PROC GRAB_CURRENT_CONVO_TO_RESUME()
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		TEXT_LABEL_23 sCheckConv = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()

		#IF IS_DEBUG_BUILD 
			SK_PRINT("Convo playing when before kllling no last line ===== ")
			SK_PRINT(sCheckConv)
		#ENDIF

		IF ARE_STRINGS_EQUAL(sCheckConv, "PRA_COPTLK1")
		OR ARE_STRINGS_EQUAL(sCheckConv, "PRA_COPTLK2")
			sResumeRoot = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
			sResumeLine = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
		ENDIF
		IF bACopConvOnGoing
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			bACopConvOnGoing = FALSE
		ELSE
			KILL_FACE_TO_FACE_CONVERSATION()
		ENDIF
	ENDIF
ENDPROC

PROC RESUME_CONVERSATION()
	IF IS_POINT_IN_POLY_2D(mCopConvoCheck1, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF NOT ARE_STRINGS_EQUAL(sResumeRoot, "NONE")
			AND NOT ARE_STRINGS_EQUAL(sResumeLine, "NONE")
			AND NOT IS_THIS_PRINT_BEING_DISPLAYED("PR_TAKBACK")
				IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(s_conversation_peds, sTextBlock, sResumeRoot, sResumeLine, CONV_PRIORITY_MEDIUM)
					sResumeRoot = "NONE"
					sResumeLine = "NONE"
					bACopConvOnGoing = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_THE_OTHER_COP(INT i)
	INT n = COP_1 //Is the other cop
	IF i = COP_1
		n = COP_2
	ELSE
		n = COP_1
	ENDIF
	
	RETURN n

ENDFUNC

FUNC BOOL IS_THE_OTHER_COP_REACTING(INT i)
	IF mCop[GET_THE_OTHER_COP(i)].eBehaviour > CBS_NOT_ALERTED
	AND mCop[GET_THE_OTHER_COP(i)].eReacting != CRS_WATCHING_PLAYER
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_COP_REACTING(INT i)
	RETURN mCop[i].eBehaviour > CBS_NOT_ALERTED
ENDFUNC

PROC DO_REACTION_LINE(INT i, STRING sConv, BOOL bArea = FALSE, BOOL bCheckUniform = FALSE)
	IF bCheckUniform
		IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_UNI))
			EXIT
		ENDIF
	ENDIF

	TEXT_LABEL_23 sCheckConv = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	IF IS_STRING_NULL_OR_EMPTY(sConv)
	OR IS_STRING_NULL_OR_EMPTY(sCheckConv)
		ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(s_conversation_peds, sTextBlock, sConv, CONV_PRIORITY_MEDIUM)
		IF bArea
			iAreaWarningTimer = GET_GAME_TIMER()
		ENDIF
		EXIT
	ENDIF

	IF NOT ARE_STRINGS_EQUAL(sCheckConv, sConv)
	AND NOT IS_THIS_PRINT_BEING_DISPLAYED("PR_TAKBACK")
	AND NOT IS_THIS_PRINT_BEING_DISPLAYED("PR_GOVEH")
		GRAB_CURRENT_CONVO_TO_RESUME()
		ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(s_conversation_peds, sTextBlock, sConv, CONV_PRIORITY_MEDIUM)
		IF bArea
			iAreaWarningTimer = GET_GAME_TIMER()
		ENDIF
	ENDIF
ENDPROC

PROC DO_PED_TURN_FOR_REACTION(INT i, COP_QUESTIONING_STATES state, BOOL bCheckUniform = FALSE)
	IF bCheckUniform
		IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_UNI))
			EXIT
		ENDIF
	ENDIF
	
	IF NOT IsPedPerformingTask(mCop[i].id, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
		SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(mCop[i].id)
		TASK_TURN_PED_TO_FACE_ENTITY(mCop[i].id, PLAYER_PED_ID())
	ENDIF
	mCop[i].iReactTimer = GET_GAME_TIMER()

	//The other cop
	mCop[GET_THE_OTHER_COP(i)].iReactTimer = GET_GAME_TIMER()
	mCop[GET_THE_OTHER_COP(i)].eBehaviour= CBS_REACTING
	mCop[GET_THE_OTHER_COP(i)].eReacting = CRS_WATCHING_PLAYER


	#IF IS_DEBUG_BUILD 
		SK_PRINT_INT("CHANGING COP STATE TO ==== ", ENUM_TO_INT(state))
		SK_PRINT_INT("COP THAT HAS CHANGED STATE ==== ", i)
	#ENDIF

	mCop[i].eQuestioning = state
	mCop[i].eBehaviour = CBS_QUESTIONING
ENDPROC

PROC TELL_COP_TO_QUESTION_WATCH_PLAYER(INT i, COP_QUESTIONING_STATES state, BOOL bDiableOtherPedReactCheck = FALSE)
	IF NOT IS_THE_OTHER_COP_REACTING(i)
	OR bDiableOtherPedReactCheck
		STRING sConv, sVoice
		IF state = CQS_QUESTION_WATCH_PLAYER_COP_ZONE
			//Disable constant chatter
			IF mCop[i].eQuestioning <> CQS_QUESTION_WATCH_PLAYER_COP_ZONE
			OR (GET_GAME_TIMER() - iAreaWarningTimer) > DEFAULT_COP_REPEAT_DELAY
				#IF IS_DEBUG_BUILD SK_PRINT("GREET") #ENDIF
				IF NOT bIsPlayerWearingCopUniform
					IF i = 0
						sConv = "PRA_COPPRT1"
					ELIF i = 1
						sConv = "PRA_COPPRT2"
					ENDIF
				ELSE
					IF i = 0
						sConv = "PRA_CPLYCOP1"
					ELIF i = 1
						sConv = "PRA_CPLYCOP2"
					ELIF i = 2
						sConv = "PRA_CPLYCOP3"
					ENDIF
				ENDIF
				DO_REACTION_LINE(i, sConv, TRUE, TRUE)
				DO_PED_TURN_FOR_REACTION(i, state, TRUE)
			ELSE
				#IF IS_DEBUG_BUILD SK_PRINT("eCopState[i] <> CS_QUESTION_WATCH_PLAYER_COP_ZONE") #ENDIF
			ENDIF
		ELIF state = CQS_QUESTION_WATCH_PLAYER_ZONE_A
			#IF IS_DEBUG_BUILD SK_PRINT("NO GREET") #ENDIF
			//Disable constant chatter
			IF mCop[i].eQuestioning <> CQS_QUESTION_WATCH_PLAYER_ZONE_A
			OR (GET_GAME_TIMER() - iAreaWarningTimer) > DEFAULT_COP_REPEAT_DELAY
				IF i = 0
					sConv = "PRA_COPPLY1"
				ELIF i = 1
					sConv = "PRA_COPPLY2"
				ENDIF
				DO_REACTION_LINE(i, sConv, TRUE, TRUE)
				DO_PED_TURN_FOR_REACTION(i, state, TRUE)
			ELSE
				#IF IS_DEBUG_BUILD SK_PRINT("eCopState[i] <> CS_QUESTION_WATCH_PLAYER_ZONE_A") #ENDIF
			ENDIF
		ELIF state =  CQS_QUESTION_WATCH_PLAYER_STEALTH
			IF mCop[i].eQuestioning <> CQS_QUESTION_WATCH_PLAYER_STEALTH
			OR (GET_GAME_TIMER() - iAreaWarningTimer) > DEFAULT_COP_REPEAT_DELAY
				IF i = 0
					sConv = "PRA_COPST1"
				ELIF i = 1
					sConv = "PRA_COPST2"
				ENDIF
				DO_REACTION_LINE(i, sConv, TRUE)
				DO_PED_TURN_FOR_REACTION(i, state)
			ELSE
				#IF IS_DEBUG_BUILD SK_PRINT("eCopState[i] <> CS_QUESTION_WATCH_PLAYER_STEALTH") #ENDIF
			ENDIF
		ELIF state =  CQS_QUESTION_PLAYER_WITH_GUN
			IF mCop[i].eQuestioning <> CQS_QUESTION_PLAYER_WITH_GUN
				IF i = 0
					sConv = "PRA_COPGUNR1"
				ELIF i = 1
					sConv = "PRA_COPGUNR2"
				ENDIF
				DO_REACTION_LINE(i, sConv, FALSE, TRUE)
				DO_PED_TURN_FOR_REACTION(i, state, TRUE)
			ELSE
				#IF IS_DEBUG_BUILD SK_PRINT("eCopState[i] <> CS_QUESTION_PLAYER_WITH_GUN") #ENDIF
			ENDIF
		ELIF state =  CQS_QUESTION_WATCH_PLAYER_BUMPED
			IF mCop[i].eQuestioning <> CQS_QUESTION_WATCH_PLAYER_BUMPED
				IF i = 0
//					sConv = "PRA_COPBMP1"
					sVoice = "S_M_Y_COP_01_WHITE_MINI_03"
				ELIF i = 1
//					sConv = "PRA_COPBMP2"
					sVoice = "S_M_Y_COP_01_BLACK_MINI_04"
				ENDIF
//				DO_REACTION_LINE(i, sConv)
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(mCop[i].id, "BUMP", sVoice, "SPEECH_PARAMS_FORCE")
				DO_PED_TURN_FOR_REACTION(i, state)
			ELSE
				#IF IS_DEBUG_BUILD SK_PRINT("eCopState[i] <> CS_QUESTION_WATCH_PLAYER_BUMPED") #ENDIF
			ENDIF
		ELIF state =  CQS_TURN_TO_FACE_NOISE
			IF mCop[i].eQuestioning <> CQS_TURN_TO_FACE_NOISE
				
				IF i = 0
					sConv = "PRA_CHP1"
				ELIF i = 1
					sConv = "PRA_CHP2"
				ELIF i = 2
					sConv = "PRA_CHP3"
				ENDIF
				
				DO_REACTION_LINE(i, sConv, TRUE)
				DO_PED_TURN_FOR_REACTION(i, state)
			ELSE
				#IF IS_DEBUG_BUILD SK_PRINT("eCopState[i] <> CQS_TURN_TO_FACE_NOISE") #ENDIF
			ENDIF
		ENDIF

		IF bIsPlayerWearingCopUniform
			IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_UNI))
				SET_BIT(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_UNI))
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC TELL_COP_REACT_TO_PLAYER(INT i, COP_REACTION_STATES state, BOOL bDisableOtherPedReact = FALSE)
	IF NOT IS_THE_OTHER_COP_REACTING(i)
	OR bDisableOtherPedReact
		STRING sConv, sVoice
		IF state =  CRS_WARN_PLAYER_WITH_GUN
			IF mCop[GET_THE_OTHER_COP(i)].eReacting <> CRS_WARN_PLAYER_WITH_GUN
				IF i = 0
					sConv = "PRA_COPGUNW1"
				ELIF i = 1
					sConv = "PRA_COPGUNW2"
				ENDIF
				DO_REACTION_LINE(i, sConv)
				iTimesSeenWithGun++
			ELSE
				#IF IS_DEBUG_BUILD SK_PRINT("eCopState[i] <> CS_WARN_PLAYER_WITH_GUN") #ENDIF
			ENDIF
		ELIF state =  CRS_PLAYER_IN_VEHICLE_IN_AREA
			IF mCop[GET_THE_OTHER_COP(i)].eReacting <> CRS_PLAYER_IN_VEHICLE_IN_AREA
			OR (GET_GAME_TIMER() - iAreaWarningTimer) > DEFAULT_COP_REPEAT_DELAY
				IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_PLAYER_IN_POLICE_TYPE_VEH))
					IF i = 0
						sConv = "PRA_CWCAR1"
					ELIF i = 1
						sConv = "PRA_CWCAR2"
					ENDIF
				ELSE
					IF i = 0
						sConv = "PRA_CCWCAR1"
					ELIF i = 1
						sConv = "PRA_CCWCAR2"
					ENDIF
				ENDIF
				IF iVehicleWarnings < 2
					DO_REACTION_LINE(i, sConv, TRUE)
				ENDIF
				iVehicleWarnings++
			ELSE
				#IF IS_DEBUG_BUILD SK_PRINT("eCopState[i] <> CS_PLAYER_IN_VEHICLE_IN_AREA") #ENDIF
			ENDIF
		ELIF state =  CRS_PLAYER_VEHICLE_IN_AREA_NO_PLAYER
			IF mCop[GET_THE_OTHER_COP(i)].eReacting <> CRS_PLAYER_VEHICLE_IN_AREA_NO_PLAYER
			OR (GET_GAME_TIMER() - iAreaWarningTimer) > DEFAULT_COP_REPEAT_DELAY
				
				IF NOT bPlayerInPoliceTypeVeh
					IF i = 0
						sConv = "PRA_CWCARNP1"
					ELIF i = 1
						sConv = "PRA_CWCARNP2"
					ENDIF
				ELSE
					IF NOT bIsPlayerWearingCopUniform
						IF i = 0
							sConv = "PRA_CCWCNP1"
						ELIF i = 1
							sConv = "PRA_CCWCNP2"
						ENDIF
					ELSE
						IF i = 0
							sConv = "PRA_CWCARNP1"
						ELIF i = 1
							sConv = "PRA_CWCARNP2"
						ENDIF
					ENDIF
				ENDIF
				
				IF iVehicleWarnings < 2
					DO_REACTION_LINE(i, sConv, TRUE)
				ENDIF
				iVehicleWarnings++
			ELSE
				#IF IS_DEBUG_BUILD SK_PRINT("eCopState[i] <> CS_PLAYER_VEHICLE_IN_AREA_NO_PLAYER") #ENDIF
			ENDIF
		ELIF state =  CRS_REACT_WATCH_PLAYER_BUMPED_RAGDOLL
			IF mCop[GET_THE_OTHER_COP(i)].eReacting <> CRS_REACT_WATCH_PLAYER_BUMPED_RAGDOLL
				IF i = 0
					sVoice = "S_M_Y_COP_01_WHITE_MINI_03"
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(mCop[i].id, "CHALLENGE_THREATEN", sVoice, "SPEECH_PARAMS_FORCE")
//					sConv = "PRA_COPBMPR1"
				ELIF i = 1
					sVoice = "S_M_Y_COP_01_BLACK_MINI_04"
				PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(mCop[i].id, "CHALLENGE_THREATEN", sVoice, "SPEECH_PARAMS_FORCE")
//					sConv = "PRA_COPBMPR2"
				ELIF i = 2
					sConv = "PRA_COPSARR3"
					DO_REACTION_LINE(i, sConv)
				ENDIF
				
			ELSE
				#IF IS_DEBUG_BUILD SK_PRINT("eCopState[i] <> CS_QUESTION_WATCH_PLAYER_BUMPED_RAGDOLL") #ENDIF
			ENDIF
		ENDIF

		IF NOT IsPedPerformingTask(mCop[i].id, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
			SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(mCop[i].id)
			TASK_TURN_PED_TO_FACE_ENTITY(mCop[i].id, PLAYER_PED_ID())
		ENDIF
		mCop[i].iReactTimer = GET_GAME_TIMER()
		
		//The other cop
		mCop[GET_THE_OTHER_COP(i)].iReactTimer = GET_GAME_TIMER()
		mCop[GET_THE_OTHER_COP(i)].eBehaviour = CBS_REACTING
		mCop[GET_THE_OTHER_COP(i)].eReacting = CRS_WATCHING_PLAYER

		#IF IS_DEBUG_BUILD 
			SK_PRINT_INT("changing cop state to cop_reaction_states ==== ", ENUM_TO_INT(state))
			SK_PRINT_INT("COP THAT HAS CHANGED STATE ==== ", i)
		#ENDIF

		mCop[i].eBehaviour = CBS_REACTING
		mCop[i].eReacting = state
	ENDIF
ENDPROC

FUNC BOOL CAN_RETURN(INT c, REACTIONS ExclusionReaction)
	INT i
	FOR i=0 TO ENUM_TO_INT(REAC_SEEN_IN_ALLEY)
		IF i != ENUM_TO_INT(ExclusionReaction)
			IF IS_BIT_SET(mCop[c].iReaction, i)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR

	IF bIsPlayerWanted
		RETURN FALSE
	ENDIF
	RETURN TRUE
ENDFUNC


PROC COP_DIALOGUE_MANAGER()
	IF mCop[COP_1].eBehaviour = CBS_NOT_ALERTED
	AND mCop[COP_2].eBehaviour = CBS_NOT_ALERTED
	AND mCop[COP_3].eBehaviour = CBS_NOT_ALERTED
		IF IS_PED_UNINJURED(mCop[COP_1].id)
		AND IS_PED_UNINJURED(mCop[COP_2].id)
		AND IS_POINT_IN_POLY_2D(mCopConvoCheck1, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		AND IS_ENTITY_IN_RANGE_COORDS( mCop[COP_1].id, mCop[COP_1].vPos, 3)
		AND IS_ENTITY_IN_RANGE_COORDS( mCop[COP_2].id, mCop[COP_2].vPos, 3)
			IF CAN_RETURN(COP_1,  NUM_REACT)
			AND CAN_RETURN(COP_2,  NUM_REACT)
			AND NOT bCopConvExpire[iCopConvID]
				IF DO_CONVO(NULL, bCopConvExpire[iCopConvID], sCopConvs[iCopConvID], 10, CONV_PRIORITY_AMBIENT_HIGH)
					#IF IS_DEBUG_BUILD SK_PRINT("Doing conversation") #ENDIF
					iCopConvID++
					IF iCopConvID = 2
						iCopConvID = 0
					ENDIF
					bACopConvOnGoing = TRUE
				ENDIF
			ENDIF
		ELIF IS_PED_UNINJURED(mCop[COP_3].id)
		AND IS_POINT_IN_POLY_2D(mCopConvoCheck2, GET_ENTITY_COORDS(PLAYER_PED_ID()))
		AND IS_ENTITY_IN_RANGE_COORDS( mCop[COP_3].id, mCop[COP_3].vPos, 3)
			IF NOT IS_BIT_SET(mCop[COP_3].iReaction, ENUM_TO_INT(REAC_SEEN_GUN)) 
			AND NOT IS_BIT_SET(mCop[COP_3].iReaction, ENUM_TO_INT(REAC_SEEN_IN_LONE_COP_AREA))  
				IF DO_CONVO(NULL, bCopConvExpire[2], "PRA_COPTLK3", 10, CONV_PRIORITY_AMBIENT_HIGH)
					bACopConvOnGoing = TRUE
				ENDIF
			ENDIF
		ELSE
			IF bACopConvOnGoing
			AND IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				KILL_ANY_CONVERSATION()
				bACopConvOnGoing = FALSE
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC



PROC PLAY_ARREST_LINE(INT i)
	IF eMissionState <> MS_RETURN_WITH_THING
	AND NOT bExpireArrestLine
	AND NOT IS_ENTITY_DEAD(mCop[i].id)
		#IF IS_DEBUG_BUILD 
			SK_PRINT_INT("Trying to play arrest line with cop == ", i)
			IF IS_THIS_COP_REACTING(i)
				SK_PRINT_INT("Trying to play arrest line with cop he is reacting number =  ", i)
			ELSE
				SK_PRINT_INT("Trying to play arrest line with cop he is NOT reacting number =   ", i)
			ENDIF
		#ENDIF
		IF IS_THIS_COP_REACTING(i)
		AND IS_IN_CONV_DISTANCE(mCop[i].id, 30)
			#IF IS_DEBUG_BUILD 
				SK_PRINT("Trying to play arrest line ") 
			#ENDIF
			STRING sVoice
			
			IF i = COP_1
				sVoice = "S_M_Y_COP_01_WHITE_MINI_03"
//				sConv = "PRA_COPSARR1"
			ELIF i = COP_2
				sVoice = "S_M_Y_COP_01_BLACK_MINI_04"
//				sConv = "PRA_COPSARR2"
			ELIF i = COP_3
				sVoice = "PRA_COPSARR3"
			ENDIF

			IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_BUMP_RAGDOLL)) 
				#IF IS_DEBUG_BUILD 
					SK_PRINT("NOT play_arrest_line") 
					IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_BUMP_RAGDOLL)) 
						SK_PRINT("bPlayerKnockedCopOver") 
					ELSE
						SK_PRINT("NOT bPlayerKnockedCopOver") 
					ENDIF
				#ENDIF
				bExpireArrestLine = TRUE	
			ELSE
				IF NOT CAN_RETURN(i, NUM_REACT)
				OR IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<362.635864,-1581.255615,31.547737>>, <<358.107361,-1586.614990,34.297737>>, 2.000000)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					bACopConvOnGoing = FALSE
					IF i = COP_3
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(s_conversation_peds, sTextBlock, sVoice, CONV_PRIORITY_MEDIUM)
					ELSE
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(mCop[i].id, "ARREST_PLAYER", sVoice, "SPEECH_PARAMS_FORCE")
					ENDIF
					#IF IS_DEBUG_BUILD SK_PRINT("play_arrest_line") #ENDIF
					bExpireArrestLine = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SET_WANTED()
	IF NOT bActivatedWanted
		#IF IS_DEBUG_BUILD SK_PRINT("SET_WANTED() ") #ENDIF
		iWantedLevelResetTimer = GET_GAME_TIMER()
		IF NOT bExpireArrestLine
			KILL_FACE_TO_FACE_CONVERSATION()
		ENDIF
		
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 1)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		
		bActivatedWanted = TRUE
	ENDIF
ENDPROC

PROC SET_COPS_TO_WAIT_REACT()
	SET_WANTED()
	INT i
	FOR i=0 TO (MAX_COPS-1)
		IF mCop[i].eReacting <> CRS_WAIT_REACT_ARREST
		AND mCop[i].eReacting <> CRS_ARREST
			#IF IS_DEBUG_BUILD SK_PRINT_INT("SET_COPS_TO_WAIT_REACT === ", i) #ENDIF
			CLEANUP_AI_PED_BLIP(mCop[i].mAIBlip)
			mCop[i].iReactTimer = GET_GAME_TIMER()
			mCop[i].eBehaviour = CBS_REACTING
			mCop[i].eReacting = CRS_WAIT_REACT_ARREST
		ENDIF
	ENDFOR
ENDPROC

PROC SWITCH_COP_TO_AI(INT i)
	IF mCop[i].eReacting != CRS_ARREST
		IF IS_PED_UNINJURED(mCop[i].id)
			IF IsPedPerformingTask(mCop[i].id, SCRIPT_TASK_PERFORM_SEQUENCE)
			OR IS_PED_RUNNING_MOBILE_PHONE_TASK(mCop[i].id)
				#IF IS_DEBUG_BUILD SK_PRINT_INT("Clear ped tasks for AI cop behaviour = ", i) #ENDIF
				CLEAR_PED_TASKS(mCop[i].id)
			ELIF IsPedPerformingTask(mCop[i].id, SCRIPT_TASK_START_SCENARIO_AT_POSITION)
			OR IsPedPerformingTask(mCop[i].id, SCRIPT_TASK_START_SCENARIO_IN_PLACE)
				#IF IS_DEBUG_BUILD SK_PRINT_INT("Set leave scenario immediate = ", i) #ENDIF
				SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(mCop[i].id)
			ENDIF

			SET_PED_AS_COP(mCop[i].id)
			TASK_ARREST_PED(mCop[i].id, PLAYER_PED_ID())
			GIVE_WEAPON_TO_PED(mCop[i].id, WEAPONTYPE_PISTOL, INFINITE_AMMO)
			#IF IS_DEBUG_BUILD SK_PRINT_INT("SWITCH_COP_TO_AI === ", i) #ENDIF
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mCop[i].id)
			UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
			PLAY_ARREST_LINE(i)
			mCop[i].eBehaviour = CBS_REACTING
			mCop[i].eReacting = CRS_ARREST
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_BUMPED_COP(INT i, BOOL &bRagdoll)
	IF IS_ENTITY_TOUCHING_ENTITY(mCop[i].id, PLAYER_PED_ID())
		IF IS_PED_RAGDOLL(mCop[i].id)
			bRagdoll = TRUE
		ELSE
			bRagdoll = FALSE
		ENDIF
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_BEHIND_SIDE_COP(INT i)
	IF i = COP_3
		RETURN FALSE
	ENDIF
	
	IF mCop[i].eReacting != CRS_ARREST
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mCop[i].id, <<1,-1,0>>), <<1,1,2>>)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PICK_REACTION(INT i, REACTIONS r)

	SWITCH r

		CASE REAC_PLAYER_WANTED
			#IF IS_DEBUG_BUILD SK_PRINT("Pick reaction  REAC_PLAYER_WANTED") #ENDIF
			SWITCH_COP_TO_AI(i)
			SET_COPS_TO_WAIT_REACT()
		BREAK
		
		CASE REAC_SEEN_GUN
			#IF IS_DEBUG_BUILD SK_PRINT_INT("Pick reaction  REAC_SEEN_GUN, iTimesSeenWithGun < 0 ", iTimesSeenWithGun) #ENDIF
			TELL_COP_TO_QUESTION_WATCH_PLAYER(i, CQS_QUESTION_PLAYER_WITH_GUN)
		BREAK
		
		CASE REAC_BUMP_RAGDOLL
			#IF IS_DEBUG_BUILD SK_PRINT("Pick reaction  REAC_BUMP_RAGDOLL") #ENDIF
			SWITCH_COP_TO_AI(i)
			SET_COPS_TO_WAIT_REACT()
		BREAK
		
		CASE REAC_SEEN_IN_LONE_COP_AREA
			#IF IS_DEBUG_BUILD SK_PRINT("Pick reaction  REAC_SEEN_IN_LONE_COP_AREA") #ENDIF
			SWITCH_COP_TO_AI(i)
			SET_COPS_TO_WAIT_REACT()
		BREAK
		
		CASE REAC_SEEN_IN_RESTRICTED_ARREST_AREA
			#IF IS_DEBUG_BUILD SK_PRINT("Pick reaction  REAC_SEEN_IN_RESTRICTED_ARREST_AREA") #ENDIF
			SWITCH_COP_TO_AI(i)
			SET_COPS_TO_WAIT_REACT()
		BREAK
		
		CASE REAC_SEEN_IN_WARNING_AREA
			#IF IS_DEBUG_BUILD SK_PRINT("Pick reaction  REAC_SEEN_IN_WARNING_AREA") #ENDIF
			TELL_COP_TO_QUESTION_WATCH_PLAYER(i, CQS_QUESTION_WATCH_PLAYER_ZONE_A)
		BREAK
		
		CASE REAC_BUMP
			#IF IS_DEBUG_BUILD SK_PRINT("Pick reaction  REAC_BUMP") #ENDIF
			TELL_COP_TO_QUESTION_WATCH_PLAYER(i, CQS_QUESTION_WATCH_PLAYER_BUMPED)
		BREAK
		
		CASE REAC_PLAYER_IN_VEHICLE
			IF iVehicleWarnings < 2
				#IF IS_DEBUG_BUILD SK_PRINT_INT("Pick reaction  REAC_PLAYER_IN_VEHICLE, iVehicleWarnings < 2 ", iVehicleWarnings) #ENDIF
				TELL_COP_REACT_TO_PLAYER(i, CRS_PLAYER_IN_VEHICLE_IN_AREA)
			ELSE
				#IF IS_DEBUG_BUILD SK_PRINT_INT("Pick reaction  REAC_PLAYER_IN_VEHICLE, iVehicleWarnings > 2 ", iVehicleWarnings) #ENDIF
				SWITCH_COP_TO_AI(i)
				SET_COPS_TO_WAIT_REACT()
			ENDIF
		BREAK
		
		CASE REAC_PLAYER_LEFT_VEHICLE_IN_ALLEY
			#IF IS_DEBUG_BUILD SK_PRINT("Pick reaction  REAC_PLAYER_LEFT_VEHICLE_IN_ALLEY") #ENDIF
			TELL_COP_REACT_TO_PLAYER(i, CRS_PLAYER_VEHICLE_IN_AREA_NO_PLAYER)
			
		BREAK
		
		CASE REAC_SEEN_IN_COP_AREA
			#IF IS_DEBUG_BUILD SK_PRINT("Pick reaction  REAC_SEEN_IN_COP_AREA") #ENDIF
			TELL_COP_TO_QUESTION_WATCH_PLAYER(i, CQS_QUESTION_WATCH_PLAYER_COP_ZONE)
		BREAK
		
		CASE REAC_SEEN_SNEAKING
			#IF IS_DEBUG_BUILD SK_PRINT("Pick reaction  REAC_SEEN_SNEAKING") #ENDIF
			TELL_COP_TO_QUESTION_WATCH_PLAYER(i, CQS_QUESTION_WATCH_PLAYER_STEALTH)
		BREAK
		
		CASE REAC_HEARD_PLAYER
			#IF IS_DEBUG_BUILD SK_PRINT("Pick reaction  REAC_HEARD_PLAYER") #ENDIF
			TELL_COP_TO_QUESTION_WATCH_PLAYER(i, CQS_TURN_TO_FACE_NOISE)
		BREAK
	ENDSWITCH
ENDPROC

PROC RETURN_COPS(INT i)
	IF mCop[i].eBehaviour <> CBS_NOT_ALERTED
	AND NOT IsPedPerformingTask(mCop[i].id, SCRIPT_TASK_PERFORM_SEQUENCE)
		CLEAR_PED_TASKS(mCop[i].id)
		SET_PED_RELATIONSHIP_GROUP_HASH(mCop[i].id, HASH_ENEMIES)
		SEQUENCE_INDEX iSeq
		IF i = COP_3
			OPEN_SEQUENCE_TASK(iSeq)
				IF NOT IS_ENTITY_IN_RANGE_COORDS(mCop[i].id, mCop[i].vPos, 3)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, mCop[i].vPos, PEDMOVEBLENDRATIO_WALK, 120000, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, mCop[i].fDir)
				ELSE
					TASK_ACHIEVE_HEADING(NULL, mCop[i].fDir)
				ENDIF
				IF bCopConvExpire[COP_3]
				AND IS_STRING_NULL_OR_EMPTY("sResumeRoot")
					TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
				ELSE
					TASK_USE_MOBILE_PHONE(NULL, TRUE)
				ENDIF
			CLOSE_SEQUENCE_TASK(iSeq)
			TASK_PERFORM_SEQUENCE(mCop[i].id, iSeq)
			CLEAR_SEQUENCE_TASK(iSeq)
		ELSE
		
			SWITCH GET_RANDOM_INT_IN_RANGE(0, MAX_COPS-1)
				CASE 0
					OPEN_SEQUENCE_TASK(iSeq)
						IF NOT IS_ENTITY_IN_RANGE_COORDS(mCop[i].id, mCop[i].vPos, 3)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, mCop[i].vPos, PEDMOVEBLENDRATIO_WALK, 120000, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, mCop[i].fDir)
						ELSE
							TASK_ACHIEVE_HEADING(NULL, mCop[i].fDir)
						ENDIF
						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_HANG_OUT_STREET", 0, TRUE)
					CLOSE_SEQUENCE_TASK(iSeq)
					TASK_PERFORM_SEQUENCE(mCop[i].id, iSeq)
					CLEAR_SEQUENCE_TASK(iSeq)
				BREAK

				CASE 1
					OPEN_SEQUENCE_TASK(iSeq)
						IF NOT IS_ENTITY_IN_RANGE_COORDS(mCop[i].id, mCop[i].vPos, 3)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, mCop[i].vPos, PEDMOVEBLENDRATIO_WALK, 120000, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, mCop[i].fDir)
						ELSE
							TASK_ACHIEVE_HEADING(NULL, mCop[i].fDir)
						ENDIF
						TASK_START_SCENARIO_IN_PLACE(NULL, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
					CLOSE_SEQUENCE_TASK(iSeq)
					TASK_PERFORM_SEQUENCE(mCop[i].id, iSeq)
					CLEAR_SEQUENCE_TASK(iSeq)
				BREAK
			ENDSWITCH
		ENDIF
		RESUME_CONVERSATION()
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mCop[i].id, FALSE)
		mCop[i].eBehaviour = CBS_NOT_ALERTED
		mCop[i].eNotAlerted = CNAS_USING_SCENARIO

		mCop[i].eQuestioning = CQS_NULL
		mCop[i].eReacting = CRS_NULL
		#IF IS_DEBUG_BUILD SK_PRINT_INT("RETURN COP ==== ", i) #ENDIF
	ENDIF
ENDPROC

PROC COP_DECISION_MAKER(INT i)
//	IF CAN_RETURN(i, NUM_REACT)
//		#IF IS_DEBUG_BUILD SK_PRINT_INT("RETURN_COPS(i) COP_DECISION_MAKER", i) #ENDIF
//		RESET_COP_REACTION_FLAGS()
//		RETURN_COPS(i)
//		EXIT
//	ENDIF

	INT r
	IF NOT IS_THE_OTHER_COP_REACTING(i)
	AND mCop[i].eReacting != CRS_ARREST
	AND mCop[i].eReacting != CRS_WAIT_REACT_ARREST
	AND NOT bIsPlayerWanted
	AND mCop[i].bUpdateReaction
		FOR r=0 TO (ENUM_TO_INT(REAC_HEARD_PLAYER))	
			IF IS_BIT_SET(mCop[i].iReaction, r)
				PICK_REACTION(i, INT_TO_ENUM(REACTIONS, r))
				mCop[i].bUpdateReaction = FALSE
				EXIT
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC SET_UPDATE_REACTION(INT i, REACTIONS react)
	IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(react))
		#IF IS_DEBUG_BUILD SK_PRINT_INT("BIT set = ", ENUM_TO_INT(react)) #ENDIF
		SET_BIT(mCop[i].iReaction, ENUM_TO_INT(react))
		mCop[i].bUpdateReaction = TRUE
	ENDIF
ENDPROC

PROC CLEAR_UPDATE_REACTION(INT i, REACTIONS react)
	IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(react))
		#IF IS_DEBUG_BUILD SK_PRINT_INT("Cleared bit set", ENUM_TO_INT(react)) #ENDIF
		CLEAR_BIT(mCop[i].iReaction, ENUM_TO_INT(react))
		mCop[i].bUpdateReaction = TRUE
	ENDIF
ENDPROC

PROC PLAYER_ARMED_CHECKS(INT i)
	BOOL bIsArmed = IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN)
	///PLAYER WITH A GUN - START
	IF mCop[i].eReacting <> CRS_WAIT_REACT_ARREST
	AND mCop[i].eReacting <> CRS_ARREST
		IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mCop[i].id, 12)
			IF bIsArmed
				IF mCop[i].bCanSeePlayer
					IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_GUN)) 
						#IF IS_DEBUG_BUILD SK_PRINT("PLAYER SEEN ARMED") #ENDIF
						SET_UPDATE_REACTION(i, REAC_SEEN_GUN)
						#IF IS_DEBUG_BUILD SK_PRINT("bPlayerSeenWithGun = TRUE set") #ENDIF
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD   
					IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_GUN))
						SK_PRINT("bPlayerSeenWithGun = FALSE set not bIsArmed") 
					ENDIF
				#ENDIF
				CLEAR_UPDATE_REACTION(i, REAC_SEEN_GUN)
			ENDIF
		ELSE
			IF bIsArmed
				IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_COP_AREA))
					IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_GUN)) 
						SET_UPDATE_REACTION(i, REAC_SEEN_GUN)
						#IF IS_DEBUG_BUILD
							SK_PRINT("bPlayerSeenWithGun = TRUE set =  bIsArmed out of range in cop zone or restricted zone") 
						#ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD   
						IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_GUN))
							SK_PRINT("bPlayerSeenWithGun = FALSE set = bIsArmed ") 
						ENDIF
					#ENDIF
					CLEAR_UPDATE_REACTION(i, REAC_SEEN_GUN)
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD   
					IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_GUN))
						SK_PRINT("bPlayerSeenWithGun = FALSE set = not bIsArmed out of range as well") 
					ENDIF
				#ENDIF
				CLEAR_UPDATE_REACTION(i, REAC_SEEN_GUN)
			ENDIF
		ENDIF
		///PLAYER WITH A GUN - END
	ENDIF
ENDPROC

PROC PLAYER_BUMPED_COP_CHECKS(INT i)
	IF mCop[i].eQuestioning != CQS_QUESTION_WATCH_PLAYER_BUMPED
	AND mCop[i].eReacting <> CRS_WAIT_REACT_ARREST
	AND mCop[i].eReacting <> CRS_ARREST
		BOOL bRagdollCop = FALSE
		IF IS_ENTITY_IN_RANGE_ENTITY(mCop[i].id, PLAYER_PED_ID(), 5)
		AND NOT IS_PED_BEING_STEALTH_KILLED(mCop[i].id)
		AND NOT WAS_PED_KILLED_BY_STEALTH(mCop[i].id)
		AND NOT WAS_PED_KILLED_BY_TAKEDOWN(mCop[i].id)
			IF HAS_PLAYER_BUMPED_COP(i, bRagdollCop)
			OR IS_PLAYER_BEHIND_SIDE_COP(i)
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF bRagdollCop
						CPRINTLN(DEBUG_MISSION, "player bumped cop with rag doll")
						SET_UPDATE_REACTION(i, REAC_BUMP_RAGDOLL)
					ELSE
						IF i = COP_3
							CPRINTLN(DEBUG_MISSION, "player bumped lone cop he kicks off no matter what")
							SET_UPDATE_REACTION(i, REAC_BUMP_RAGDOLL)
						ELSE
							CPRINTLN(DEBUG_MISSION, "player bumped cop withOUT rag doll")
							SET_UPDATE_REACTION(i, REAC_BUMP)
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_MISSION, "player bumped in car")
					SET_UPDATE_REACTION(i, REAC_BUMP_RAGDOLL)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_ENTITY_IN_RANGE_ENTITY(mCop[i].id, PLAYER_PED_ID(), 5.5)
			CLEAR_UPDATE_REACTION(i, REAC_BUMP)
			CLEAR_UPDATE_REACTION(i, REAC_BUMP_RAGDOLL)
//			CLEAR_BIT(mCop[i].iReaction, ENUM_TO_INT(REAC_BUMP))
		ENDIF
	ENDIF
ENDPROC

PROC PLAYER_IN_STEALTH_MODE_CHECKS(INT i)
	/// STEALTH MOVEMENT - START
	IF NOT bExpireStealth
		BOOL bStealth = GET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID())
		IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_SNEAKING))
			IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_COP_AREA))
				IF mCop[i].bCanSeePlayer
					IF bStealth
						#IF IS_DEBUG_BUILD SK_PRINT("PLAYER SEEN SNEAKING") #ENDIF
						SET_UPDATE_REACTION(i, REAC_SEEN_SNEAKING)
						#IF IS_DEBUG_BUILD SK_PRINT("PLAYER_IN_STEALTH_MODE_CHECKS(), CS_WARN_PLAYER_WITH_GUN") #ENDIF
					ELSE
						CLEAR_UPDATE_REACTION(i, REAC_SEEN_SNEAKING)
//						CLEAR_BIT(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_SNEAKING))
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_THIS_COP_REACTING(i)
				IF bStealth
					IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mCop[i].id, 10)
						IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_COP_AREA)) 
						AND NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_WARNING_AREA)) 
							#IF IS_DEBUG_BUILD SK_PRINT("NOT IS_ENTITY_IN_RANGE_ENTITY, bPlayerSeenSneaking = FALSE") #ENDIF
							#IF IS_DEBUG_BUILD SK_PRINT("NOT bPlayerSeenCopZone + NOT bPlayerSeenInRestrictedZoneA, bPlayerSeenSneaking = FALSE") #ENDIF
							CLEAR_BIT(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_SNEAKING))
						ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD SK_PRINT("NOT bStealth, bPlayerSeenSneaking = FALSE") #ENDIF
					CLEAR_UPDATE_REACTION(i, REAC_SEEN_SNEAKING)
//					CLEAR_BIT(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_SNEAKING))
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	/// STEALTH MOVEMENT - end

ENDPROC

PROC CAN_COP_SEE_PED(INT i)
	IF IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mCop[i].id, 30)
		IF CAN_PED_SEE_PED(mCop[i].id, PLAYER_PED_ID(), MAX_COP_VIEWCONE)
			mCop[i].bCanSeePlayer = TRUE
		ELSE
			mCop[i].bCanSeePlayer = FALSE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_BEHIND_UNARMED_STEALTH(INT i)
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mCop[i].id, <<0,-2,0>>), <<2,3,2>>)
	AND NOT IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC PLAYER_IN_AREA_SET_BIT(INT i, RESTRICTED_AREA_ID areaSeenPlayer)
	SWITCH areaSeenPlayer
		CASE RAI_FRONT_DESK_COPS_AREA
			IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_WARNING_AREA))
			AND NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_RESTRICTED_ARREST_AREA))
			AND NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_PLAYER_IN_POLICE_TYPE_VEH))
				IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_COP_AREA)) 
					SET_UPDATE_REACTION(i, REAC_SEEN_IN_COP_AREA)
					#IF IS_DEBUG_BUILD  
						SK_PRINT("Player seen in the front desk area ")
					#ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE RAI_WARNING_AREA
			IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_RESTRICTED_ARREST_AREA))
				IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_WARNING_AREA)) 
					#IF IS_DEBUG_BUILD  
						SK_PRINT("Player seen in the warning area ")
					#ENDIF
					SET_UPDATE_REACTION(i, REAC_SEEN_IN_WARNING_AREA)
				ENDIF
			ENDIF
		BREAK

		CASE RAI_RESTRICTED_ARREST_AREA
			IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_RESTRICTED_ARREST_AREA)) 
				#IF IS_DEBUG_BUILD  
					SK_PRINT("Player seen in restricted zone arrest")
				#ENDIF
				SET_UPDATE_REACTION(i, REAC_SEEN_IN_RESTRICTED_ARREST_AREA)
			ENDIF
		BREAK

		CASE RAI_BACK_ALLEY_AREA
			IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_ALLEY)) 
				#IF IS_DEBUG_BUILD  
					SK_PRINT("Player seen in alley")
				#ENDIF
				SET_UPDATE_REACTION(i, REAC_SEEN_IN_ALLEY)
			ENDIF
		BREAK

		CASE RAI_VEHICLE_AREA
			IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_PLAYER_IN_VEHICLE))
				IF IS_PED_IN_ANY_POLICE_VEHICLE(PLAYER_PED_ID())
					#IF IS_DEBUG_BUILD  
						IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_PLAYER_IN_POLICE_TYPE_VEH)) 
							SK_PRINT("Player seen in police vehicle")
							SK_PRINT("Player seen in area in vehicle")
						ENDIF
					#ENDIF
					SET_BIT(mCop[i].iReaction, ENUM_TO_INT(REAC_PLAYER_IN_POLICE_TYPE_VEH))
					SET_UPDATE_REACTION(i, REAC_PLAYER_IN_VEHICLE)
				ELSE
					#IF IS_DEBUG_BUILD  
						IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_PLAYER_IN_VEHICLE)) 
							SK_PRINT("Player not in police vehicle")
							SK_PRINT("Player seen in area in vehicle")
						ENDIF
					#ENDIF
					CLEAR_BIT(mCop[i].iReaction, ENUM_TO_INT(REAC_PLAYER_IN_POLICE_TYPE_VEH))
					SET_UPDATE_REACTION(i, REAC_PLAYER_IN_VEHICLE)
				ENDIF
			ENDIF
		BREAK

		CASE RAI_LONE_COP_AREA
			IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_LONE_COP_AREA)) 
				IF NOT IS_PED_BEING_STEALTH_KILLED(mCop[i].id)
				AND NOT WAS_PED_KILLED_BY_STEALTH(mCop[i].id)
				AND NOT WAS_PED_KILLED_BY_TAKEDOWN(mCop[i].id)
					#IF IS_DEBUG_BUILD  
						SK_PRINT("Player seen in lone cop area")
					#ENDIF
					SET_UPDATE_REACTION(i, REAC_SEEN_IN_LONE_COP_AREA)
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC CLEAR_AREA_REACTION_BIT(INT i, RESTRICTED_AREA_ID areaSeenPlayer)
	SWITCH areaSeenPlayer
		CASE RAI_FRONT_DESK_COPS_AREA
			#IF IS_DEBUG_BUILD  
				IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_COP_AREA)) 
					SK_PRINT("Clearing seen in cop area bit ")
				ENDIF
			#ENDIF
			CLEAR_UPDATE_REACTION(i, REAC_SEEN_IN_COP_AREA)
		BREAK

		CASE RAI_WARNING_AREA
			#IF IS_DEBUG_BUILD  
				IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_WARNING_AREA)) 
					SK_PRINT("Clearing seen in warning area bit ") 
				ENDIF
			#ENDIF
			CLEAR_UPDATE_REACTION(i, REAC_SEEN_IN_WARNING_AREA)
		BREAK

		CASE RAI_RESTRICTED_ARREST_AREA
			IF NOT bIsPlayerWanted
				#IF IS_DEBUG_BUILD  
					IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_RESTRICTED_ARREST_AREA)) 
						SK_PRINT("Clearing seen in restricted area bit") 
					ENDIF
				#ENDIF
				CLEAR_UPDATE_REACTION(i, REAC_SEEN_IN_RESTRICTED_ARREST_AREA)
			ENDIF
		BREAK

		CASE RAI_BACK_ALLEY_AREA
			#IF IS_DEBUG_BUILD  
				IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_ALLEY))
					SK_PRINT("Clearing seen in back alley") 
				ENDIF
			#ENDIF
			CLEAR_UPDATE_REACTION(i, REAC_SEEN_IN_ALLEY)
		BREAK

		CASE RAI_VEHICLE_AREA
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX viLastCar 
				viLastCar = GET_PLAYERS_LAST_VEHICLE()
				
				IF IS_VEHICLE_OK(viLastCar)
					IF IS_MODEL_POLICE_VEHICLE(GET_ENTITY_MODEL(viLastCar))
						bPlayerInPoliceTypeVeh = TRUE
					ELSE
						bPlayerInPoliceTypeVeh = FALSE
					ENDIF

					IF NOT IS_ENTITY_IN_ANGLED_AREA(viLastCar, mRestrictedArea[RAI_VEHICLE_AREA].vAreaPos1, mRestrictedArea[RAI_VEHICLE_AREA].vAreaPos2, mRestrictedArea[RAI_VEHICLE_AREA].fAreaWidth)
						#IF IS_DEBUG_BUILD  
							IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_PLAYER_IN_VEHICLE)) 
								SK_PRINT("PLAYER NOT IN VEHICLE VEHICLE NOT IN AREA")
							ENDIF
						#ENDIF
						CLEAR_UPDATE_REACTION(i, REAC_PLAYER_IN_VEHICLE)
						CLEAR_UPDATE_REACTION(i, REAC_PLAYER_LEFT_VEHICLE_IN_ALLEY)
					ELSE
						IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_PLAYER_LEFT_VEHICLE_IN_ALLEY))
							#IF IS_DEBUG_BUILD  
								IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_PLAYER_IN_VEHICLE)) 
									SK_PRINT("PLAYER NOT IN VEHICLE BUT LEFT VEHICLE IN ALLEY")
								ENDIF
							#ENDIF
							SET_UPDATE_REACTION(i, REAC_PLAYER_LEFT_VEHICLE_IN_ALLEY)
						ENDIF
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD  
						IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_PLAYER_IN_VEHICLE)) 
							SK_PRINT("PLAYER NOT IN VEHICLE AND LAST CAR IS DEAD")
						ENDIF
					#ENDIF
					CLEAR_UPDATE_REACTION(i, REAC_PLAYER_IN_VEHICLE)
					CLEAR_UPDATE_REACTION(i, REAC_PLAYER_LEFT_VEHICLE_IN_ALLEY)
				ENDIF
			ENDIF
//
			
//			#IF IS_DEBUG_BUILD  
//				IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_PLAYER_IN_POLICE_TYPE_VEH)) 
//					SK_PRINT("Clearing seen in police vehicle 1 bit") 
//				ENDIF
//			#ENDIF
//			CLEAR_UPDATE_REACTION(i, REAC_PLAYER_IN_POLICE_TYPE_VEH)
//
//			#IF IS_DEBUG_BUILD  
//				IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_PLAYER_IN_VEHICLE)) 
//					SK_PRINT("Clearing seen in Vehicle bit")
//				ENDIF
//			#ENDIF
//
//			CLEAR_UPDATE_REACTION(i, REAC_PLAYER_IN_VEHICLE)
		BREAK

		CASE RAI_LONE_COP_AREA
			#IF IS_DEBUG_BUILD  
				IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_LONE_COP_AREA)) 
					SK_PRINT("Clearing seen in lone cop area bit")
				ENDIF
			#ENDIF
			CLEAR_UPDATE_REACTION(i, REAC_SEEN_IN_LONE_COP_AREA)
		BREAK
	ENDSWITCH
ENDPROC


PROC MONITOR_PLAYER_SEEN_IN_AN_AREA(INT i)
	IF mCop[i].eReacting <> CRS_WAIT_REACT_ARREST
	AND mCop[i].eReacting <> CRS_ARREST
		INT a
		FOR a=0 TO (MAX_RESTRICTED_AREAS-1)
			IF mCop[i].bCanSeePlayer
				IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_UNI))
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), mRestrictedArea[a].vAreaPos1, mRestrictedArea[a].vAreaPos2, mRestrictedArea[a].fAreaWidth)
						bHearingDelay = TRUE
						iHearingTimer = GET_GAME_TIMER()

						IF a = ENUM_TO_INT(RAI_VEHICLE_AREA)
							IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
								PLAYER_IN_AREA_SET_BIT(i, RAI_VEHICLE_AREA)
							ELSE
								CLEAR_AREA_REACTION_BIT(i, RAI_VEHICLE_AREA)
							ENDIF
						ELSE
							PLAYER_IN_AREA_SET_BIT(i, INT_TO_ENUM(RESTRICTED_AREA_ID, a))
						ENDIF
					ELSE
						CLEAR_AREA_REACTION_BIT(i, INT_TO_ENUM(RESTRICTED_AREA_ID, a))
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), mRestrictedArea[a].vAreaPos1, mRestrictedArea[a].vAreaPos2, mRestrictedArea[a].fAreaWidth)
				OR IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_UNI))
					CLEAR_AREA_REACTION_BIT(i, INT_TO_ENUM(RESTRICTED_AREA_ID, a))
				ENDIF
			ENDIF
		ENDFOR
	ELSE
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), mRestrictedArea[RAI_BACK_ALLEY_AREA].vAreaPos1, mRestrictedArea[RAI_BACK_ALLEY_AREA].vAreaPos2, mRestrictedArea[RAI_BACK_ALLEY_AREA].fAreaWidth)
			PLAYER_IN_AREA_SET_BIT(i, RAI_BACK_ALLEY_AREA)
		ELSE
			CLEAR_AREA_REACTION_BIT(i, RAI_BACK_ALLEY_AREA)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_THREATENED_PARTNER(INT i)
	IF IS_PED_UNINJURED(mCop[i].id)
		FLOAT fTargettingDist
		IF IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_GUN|WF_INCLUDE_PROJECTILE)
			fTargettingDist = 20.0 // ranged distance
		ELSE
			fTargettingDist = 3.0 // melee distance
		ENDIF

		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), mCop[i].id, <<fTargettingDist, fTargettingDist, fTargettingDist>>, FALSE) // player is nearby
			IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), mCop[i].id)
			OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), mCop[i].id) 
				// player is targetting ped, set targetting dist based on whether he is using melee or ranged attack
				IF mCop[i].bCanSeePlayer
					#IF IS_DEBUG_BUILD SK_PRINT_INT("HAS_PLAYER_THREATENED_PARTNER TRUE  ", i) #ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


PROC MONITOR_COPS_THREATENED(INT i)
	IF NOT IS_PLAYER_BEHIND_UNARMED_STEALTH(i)
		IF bIsPlayerWanted
			SET_COPS_TO_WAIT_REACT()
		ELIF HAS_PLAYER_THREATENED_PED(mCop[i].id, TRUE, 70, 150, FALSE, TRUE, FALSE, TRUE)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(mCop[i].id), 100)
			#IF IS_DEBUG_BUILD SK_PRINT_INT("Cop has been threatened = ", i) #ENDIF
			SET_COPS_TO_WAIT_REACT()
		ELIF HAS_PLAYER_THREATENED_PARTNER(GET_THE_OTHER_COP(i))
			#IF IS_DEBUG_BUILD SK_PRINT_INT("Cop Partner has been threatened = ", GET_THE_OTHER_COP(i)) #ENDIF
			SET_COPS_TO_WAIT_REACT()
		ENDIF
	ENDIF
ENDPROC

PROC MONITER_COP_SIGHT(INT i)
	CAN_COP_SEE_PED(i)
	MONITOR_COPS_THREATENED(i)
	MONITOR_PLAYER_SEEN_IN_AN_AREA(i)
	/// RESTRICTED ZONE B = If the player is seen in here they get a wanted level
	IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_RESTRICTED_ARREST_AREA))
		IF mCop[i].eReacting <> CRS_WAIT_REACT_ARREST
		AND mCop[i].eReacting <> CRS_ARREST
		AND NOT bIsPlayerWanted

			IF NOT bIsPlayerWearingCopUniform
			AND IS_ENTITY_IN_RANGE_COORDS(mCop[i].id, mCop[i].vPos, 6)
				PLAYER_ARMED_CHECKS(i)
			ENDIF

			PLAYER_BUMPED_COP_CHECKS(i)
			PLAYER_IN_STEALTH_MODE_CHECKS(i)
		ENDIF
	ENDIF

ENDPROC


PROC MONITOR_COP_HEARING(INT i)
	IF NOT IS_THE_OTHER_COP_REACTING(i)
	AND mCop[i].eReacting <> CRS_WAIT_REACT_ARREST
	AND mCop[i].eReacting <> CRS_ARREST

		FLOAT fNoise = GET_PLAYER_CURRENT_STEALTH_NOISE(PLAYER_ID())
		IF i = 0
		OR i = 1
			IF NOT bHearingDelay
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<340.07581, -1598.25537, 28.29322>>, <<346.49390, -1603.68433, 38.29278>>, 6.22)
					IF IS_PED_FALLING(PLAYER_PED_ID())
					OR IS_PED_CLIMBING(PLAYER_PED_ID())
						bHearingDelay = TRUE
						iHearingTimer = GET_GAME_TIMER()
						EXIT
					ELSE
						IF NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_HEARD_PLAYER))
							IF fNoise > 16
							AND NOT mCop[i].bCanSeePlayer
							AND NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_COP_AREA))
							AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<344.44824, -1603.47290, 28.29278>>, <<345.62222, -1604.47839, 32.29278>>, 3.46) //gate makes a noise when walked in to or jumped over in certain places
							AND IS_ENTITY_IN_ANGLED_AREA(mCop[i].id, mRestrictedArea[RAI_FRONT_DESK_COPS_AREA].vAreaPos1, mRestrictedArea[RAI_FRONT_DESK_COPS_AREA].vAreaPos2, mRestrictedArea[RAI_FRONT_DESK_COPS_AREA].fAreaWidth)
								#IF IS_DEBUG_BUILD SK_PRINT_FLOAT("PLAYER NOISE ==== ", fNoise) #ENDIF
								SET_UPDATE_REACTION(i, REAC_HEARD_PLAYER)
								#IF IS_DEBUG_BUILD SK_PRINT("fNoise > 12  CQS_TURN_TO_FACE_NOISE") #ENDIF
								EXIT
							ENDIF
						ELSE
						ENDIF
					ENDIF
				ENDIF

				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), mRestrictedArea[RAI_FRONT_DESK_COPS_AREA].vAreaPos1, mRestrictedArea[RAI_FRONT_DESK_COPS_AREA].vAreaPos2, mRestrictedArea[RAI_FRONT_DESK_COPS_AREA].fAreaWidth)
				AND IS_ENTITY_IN_ANGLED_AREA(mCop[i].id, mRestrictedArea[RAI_FRONT_DESK_COPS_AREA].vAreaPos1, mRestrictedArea[RAI_FRONT_DESK_COPS_AREA].vAreaPos2, mRestrictedArea[RAI_FRONT_DESK_COPS_AREA].fAreaWidth)
				AND NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_COP_AREA))
				AND NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_HEARD_PLAYER))
					IF fNoise > 12
					AND NOT mCop[i].bCanSeePlayer
					AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<344.44824, -1603.47290, 28.29278>>, <<345.62222, -1604.47839, 32.29278>>, 3.46)//gate makes a noise when walked in to or jumped over in certain places
						SET_UPDATE_REACTION(i, REAC_HEARD_PLAYER)
						#IF IS_DEBUG_BUILD SK_PRINT_FLOAT("PLAYER NOISE ==== ", fNoise) #ENDIF
						#IF IS_DEBUG_BUILD SK_PRINT("fNoise > 8.5  CQS_TURN_TO_FACE_NOISE") #ENDIF
					ENDIF
				ELSE
				ENDIF
			ELSE
				IF (GET_GAME_TIMER() - iHearingTimer) > 4000
					#IF IS_DEBUG_BUILD SK_PRINT_FLOAT("bHearingDelay = FALSE PLAYER NOISE ==== ", GET_PLAYER_CURRENT_STEALTH_NOISE(PLAYER_ID())) #ENDIF
					bHearingDelay = FALSE
				ENDIF
			ENDIF
		ELSE
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), mRestrictedArea[RAI_LONE_COP_AREA].vAreaPos1, mRestrictedArea[RAI_LONE_COP_AREA].vAreaPos2, mRestrictedArea[RAI_LONE_COP_AREA].fAreaWidth)
			AND IS_ENTITY_IN_ANGLED_AREA(mCop[i].id, mRestrictedArea[RAI_LONE_COP_AREA].vAreaPos1, mRestrictedArea[RAI_LONE_COP_AREA].vAreaPos2, mRestrictedArea[RAI_LONE_COP_AREA].fAreaWidth)

				IF fNoise > 10
				AND NOT mCop[i].bCanSeePlayer
					#IF IS_DEBUG_BUILD SK_PRINT_FLOAT("PLAYER NOISE ==== ", fNoise) #ENDIF
					SET_UPDATE_REACTION(i, REAC_HEARD_PLAYER)
				ELSE
					CLEAR_UPDATE_REACTION(i, REAC_HEARD_PLAYER)
				ENDIF
			ELSE
			ENDIF
		ENDIF
	ENDIF

ENDPROC

PROC MONITER_COP_PERCEPTIONS(INT i)
	MONITER_COP_SIGHT(i)
	MONITOR_COP_HEARING(i)
	COP_DECISION_MAKER(i)
ENDPROC

PROC NOT_ALERTED_STATES(INT i)
	SWITCH mCop[i].eNotAlerted
		CASE CNAS_USING_SCENARIO
			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF IS_PED_BEING_STEALTH_KILLED(mCop[i].id)
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				ENDIF
			ELSE
				IF i = COP_3
					IF bCopConvExpire[COP_3]
					AND NOT IS_PED_IN_CURRENT_CONVERSATION(mCop[i].id)
						#IF IS_DEBUG_BUILD SK_PRINT("Telling lone cop to use scenario") #ENDIF
						CLEAR_PED_TASKS(mCop[i].id)
						TASK_START_SCENARIO_IN_PLACE(mCop[i].id, "WORLD_HUMAN_STAND_MOBILE", 0, TRUE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE CNAS_RETURN_BEHAVIOUR
		BREAK
	ENDSWITCH
ENDPROC

PROC QUESTIONING_STATES(INT i)
	SWITCH mCop[i].eQuestioning
		CASE CQS_QUESTION_WATCH_PLAYER_COP_ZONE
			IF (GET_GAME_TIMER() - mCop[i].iReactTimer) > GET_RANDOM_INT_IN_RANGE(2500, 3000)
				
				IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<362.635864,-1581.255615,31.547737>>, <<358.107361,-1586.614990,34.297737>>, 2.000000)
					#IF IS_DEBUG_BUILD SK_PRINT("SET_COPS_TO_WAIT_REACT(), Player on awening") #ENDIF
					SWITCH_COP_TO_AI(i)
					SET_COPS_TO_WAIT_REACT()
					EXIT
				ENDIF

				IF CAN_RETURN(i, REAC_SEEN_UNI)
					#IF IS_DEBUG_BUILD SK_PRINT_INT("RETURN_COPS(i) CQS_QUESTION_WATCH_PLAYER_COP_ZONE", i) #ENDIF
					RETURN_COPS(i)
					EXIT
				ELSE
					IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_SNEAKING))
					AND (GET_GAME_TIMER() - iAreaWarningTimer) > 2500
					AND NOT bExpireStealth
						STRING sConv
						IF i = 0
							sConv = "PRA_COPST1"
						ELIF i = 1
							sConv = "PRA_COPST2"
						ENDIF
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(s_conversation_peds, sTextBlock, sConv, CONV_PRIORITY_MEDIUM)
						CLEAR_BIT(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_SNEAKING))
						CLEAR_BIT(mCop[GET_THE_OTHER_COP(i)].iReaction, ENUM_TO_INT(REAC_SEEN_SNEAKING))
						bExpireStealth = TRUE
					ENDIF
					#IF IS_DEBUG_BUILD SK_PRINT_INT("player still in cop zone", i) #ENDIF
					mCop[i].bUpdateReaction = TRUE
				ENDIF
			ENDIF
		BREAK
		CASE CQS_QUESTION_WATCH_PLAYER_ZONE_A
			IF (GET_GAME_TIMER() - mCop[i].iReactTimer) > GET_RANDOM_INT_IN_RANGE(1500, 2000)
				IF CAN_RETURN(i, REAC_SEEN_UNI)
					#IF IS_DEBUG_BUILD SK_PRINT_INT("RETURN_COPS(i) CQS_QUESTION_WATCH_PLAYER_ZONE_A", i) #ENDIF
					RETURN_COPS(i)
					EXIT
				ELSE
					#IF IS_DEBUG_BUILD SK_PRINT_INT("player still in Zone A ", i) #ENDIF
					mCop[i].bUpdateReaction = TRUE
				ENDIF
			ENDIF
		BREAK
		CASE CQS_QUESTION_WATCH_PLAYER_STEALTH
			IF (GET_GAME_TIMER() - mCop[i].iReactTimer) > GET_RANDOM_INT_IN_RANGE(2500, 3000)
				IF CAN_RETURN(i, REAC_SEEN_UNI)
					#IF IS_DEBUG_BUILD SK_PRINT_INT("RETURN_COPS(i) CQS_QUESTION_WATCH_PLAYER_STEALTH", i) #ENDIF
					RETURN_COPS(i)
					EXIT
				ELSE
					#IF IS_DEBUG_BUILD SK_PRINT_INT("player still in stealth", i) #ENDIF
					mCop[i].bUpdateReaction = TRUE
				ENDIF
			ENDIF
		BREAK
		CASE CQS_QUESTION_WATCH_PLAYER_BUMPED
			IF (GET_GAME_TIMER() - mCop[i].iReactTimer) > GET_RANDOM_INT_IN_RANGE(2500, 3000)
				IF NOT IS_ENTITY_IN_RANGE_ENTITY(mCop[i].id, PLAYER_PED_ID(), 1.5)
					CLEAR_BIT(mCop[i].iReaction, ENUM_TO_INT(REAC_BUMP))
				ENDIF
				
				IF CAN_RETURN(i, REAC_SEEN_UNI)
					#IF IS_DEBUG_BUILD SK_PRINT_INT("RETURN_COPS(i) CQS_QUESTION_WATCH_PLAYER_BUMPED", i) #ENDIF
					RETURN_COPS(i)
					EXIT
				ELSE
					#IF IS_DEBUG_BUILD SK_PRINT_INT("player still near cop after bump", i) #ENDIF
					mCop[i].bUpdateReaction = TRUE
				ENDIF

			ENDIF
		BREAK
		CASE CQS_QUESTION_PLAYER_WITH_GUN
			IF (GET_GAME_TIMER() - mCop[i].iReactTimer) > GET_RANDOM_INT_IN_RANGE(5000, 6000)
				
				IF CAN_RETURN(i, REAC_SEEN_UNI)
					#IF IS_DEBUG_BUILD SK_PRINT_INT("RETURN_COPS(i) CQS_QUESTION_PLAYER_WITH_GUN", i) #ENDIF
					RETURN_COPS(i)
					EXIT
				ELSE
					IF iTimesSeenWithGun > 0 
						#IF IS_DEBUG_BUILD SK_PRINT_INT("Pick reaction  REAC_SEEN_GUN, iTimesSeenWithGun > 0 ", iTimesSeenWithGun) #ENDIF
						SWITCH_COP_TO_AI(i)
						SET_COPS_TO_WAIT_REACT()
					ENDIF
				ENDIF

			ENDIF
		BREAK
		CASE CQS_TURN_TO_FACE_NOISE
			IF (GET_GAME_TIMER() - mCop[i].iReactTimer) > GET_RANDOM_INT_IN_RANGE(2500, 3000)
				FLOAT fNoise
				fNoise = GET_PLAYER_CURRENT_STEALTH_NOISE(PLAYER_ID())
				#IF IS_DEBUG_BUILD SK_PRINT_FLOAT("PLAYER NOISE ==== ", fNoise) #ENDIF

				IF fNoise < 12
				AND NOT mCop[i].bCanSeePlayer
				AND NOT IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_COP_AREA))
				AND NOT IsPedPerformingTask(mCop[i].id, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
					#IF IS_DEBUG_BUILD SK_PRINT_INT("player still making noise = ", i) #ENDIF
					CLEAR_BIT(mCop[i].iReaction, ENUM_TO_INT(REAC_HEARD_PLAYER))
				ENDIF

				IF CAN_RETURN(i, REAC_SEEN_UNI)
					#IF IS_DEBUG_BUILD SK_PRINT_INT("RETURN_COPS(i) CQS_TURN_TO_FACE_NOISE", i) #ENDIF
					RETURN_COPS(i)
					EXIT
				ELSE
					#IF IS_DEBUG_BUILD SK_PRINT_INT("player still making noise", i) #ENDIF
					mCop[i].bUpdateReaction = TRUE
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
ENDPROC

PROC RESET_WANTED(INT iWantedlevel = -1)
	SET_MAX_WANTED_LEVEL(5)
	SET_WANTED_LEVEL_MULTIPLIER(1)
	iWantedLevelResetTimer = -1
	IF IS_PED_UNINJURED(mCop[COP_1].id)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mCop[COP_1].id, FALSE)
	ENDIF
	
	IF IS_PED_UNINJURED(mCop[COP_2].id)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mCop[COP_2].id, FALSE)
	ENDIF
	
	IF IS_PED_UNINJURED(mCop[COP_3].id)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mCop[COP_3].id, FALSE)
	ENDIF

	IF iWantedlevel != -1
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), iWantedlevel)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	ENDIF
ENDPROC

PROC REACTING_STATES(INT i)
	SWITCH mCop[i].eReacting
		CASE CRS_WATCHING_PLAYER
			IF IS_THE_OTHER_COP_REACTING(i)
			AND IS_ENTITY_IN_RANGE_COORDS(mCop[i].id, mCop[i].vPos, 3)
				IF (GET_GAME_TIMER() - mCop[i].iReactTimer) > GET_RANDOM_INT_IN_RANGE(1000, 1500)
					IF NOT IsPedPerformingTask(mCop[i].id, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
						SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(mCop[i].id)
						TASK_TURN_PED_TO_FACE_ENTITY(mCop[i].id, PLAYER_PED_ID())
						#IF IS_DEBUG_BUILD SK_PRINT_INT("Turning to watch player  = ", i) #ENDIF
					ENDIF
					mCop[i].iReactTimer = GET_GAME_TIMER()
				ENDIF
			ELSE
				RETURN_COPS(i)
			ENDIF
		BREAK

		CASE CRS_WARN_PLAYER_WITH_GUN
			IF (GET_GAME_TIMER() - mCop[i].iReactTimer) > GET_RANDOM_INT_IN_RANGE(5000, 6000)

				IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_GUN))
					#IF IS_DEBUG_BUILD SK_PRINT("SET_COPS_TO_WAIT_REACT(), CS_WARN_PLAYER_WITH_GUN") #ENDIF
					SWITCH_COP_TO_AI(i)
					SET_COPS_TO_WAIT_REACT()
					EXIT
				ELSE
					IF CAN_RETURN(i, NUM_REACT)
						#IF IS_DEBUG_BUILD SK_PRINT_INT("RETURN_COPS(i) CRS_WARN_PLAYER_WITH_GUN", i) #ENDIF
						RETURN_COPS(i)
						EXIT
					ELSE
						mCop[i].bUpdateReaction = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE CRS_PLAYER_IN_VEHICLE_IN_AREA
			IF (GET_GAME_TIMER() - mCop[i].iReactTimer) > GET_RANDOM_INT_IN_RANGE(2500, 3000)
				IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_PLAYER_IN_VEHICLE))
					mCop[i].bUpdateReaction = TRUE
				ELSE
					iVehicleWarnings = 0
					RETURN_COPS(i)
				ENDIF
			ENDIF
		BREAK
		CASE CRS_PLAYER_VEHICLE_IN_AREA_NO_PLAYER
			IF (GET_GAME_TIMER() - mCop[i].iReactTimer) > GET_RANDOM_INT_IN_RANGE(2500, 3000)
				IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_PLAYER_IN_VEHICLE))
					IF iVehicleWarnings >= 3
						#IF IS_DEBUG_BUILD SK_PRINT("SET_COPS_TO_WAIT_REACT(), CS_PLAYER_VEHICLE_IN_AREA_NO_PLAYER") #ENDIF
						SWITCH_COP_TO_AI(i)
						SET_COPS_TO_WAIT_REACT()
						EXIT
					ELIF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_PLAYER_IN_POLICE_TYPE_VEH))
					AND NOT bIsPlayerWearingCopUniform
						#IF IS_DEBUG_BUILD SK_PRINT("SET_COPS_TO_WAIT_REACT(), bPlayerInPoliceTypeVeh bIsPlayerWearingCopUniform CS_PLAYER_VEHICLE_IN_AREA_NO_PLAYER") #ENDIF
						SWITCH_COP_TO_AI(i)
						SET_COPS_TO_WAIT_REACT()
						EXIT
					ELSE
						mCop[i].bUpdateReaction = TRUE
					ENDIF
				ELSE
					iVehicleWarnings = 0 
					RETURN_COPS(i)
				ENDIF
			ENDIF
		BREAK
		CASE CRS_WAIT_REACT_ARREST
			INT iWaitTime
			
			IF i=0
				iWaitTime = 350
			ELSE
				iWaitTime = 750
			ENDIF
			
			IF (GET_GAME_TIMER() - mCop[i].iReactTimer) > iWaitTime
				IF NOT IS_PED_BEING_STUNNED(mCop[i].id)
				AND NOT IS_PED_IN_WRITHE(mCop[i].id)
					mCop[i].iReactTimer = GET_GAME_TIMER()
					SWITCH_COP_TO_AI(i)
				ENDIF
			ENDIF
		BREAK
		CASE CRS_REACT_WATCH_PLAYER_BUMPED_RAGDOLL
			IF (GET_GAME_TIMER() - mCop[i].iReactTimer) > GET_RANDOM_INT_IN_RANGE(1000, 3000)
				#IF IS_DEBUG_BUILD SK_PRINT("SET_COPS_TO_WAIT_REACT(), CS_QUESTION_WATCH_PLAYER_BUMPED_RAGDOLL") #ENDIF
				SET_COPS_TO_WAIT_REACT()
				EXIT
			ENDIF
		BREAK

		CASE CRS_ARREST
			IF (GET_GAME_TIMER() - mCop[i].iReactTimer) > 350
			
				IF NOT bIsPlayerWanted
				AND CAN_RETURN(i, NUM_REACT)
					RESET_COP_REACTION_FLAGS()
					RETURN_COPS(i)
					REMOVE_COP_BLIP_FROM_PED(mCop[i].id)
					#IF IS_DEBUG_BUILD SK_PRINT_INT("RESET_COP_REACTION_FLAGS in arrest state = ", i) #ENDIF
					EXIT
				ELSE
					
					IF iWantedLevelResetTimer != -1
						ENTITY_INDEX ent
						IF (GET_GAME_TIMER() - iWantedLevelResetTimer) > 12000
							#IF IS_DEBUG_BUILD SK_PRINT("Resetting wanted levels") #ENDIF
							RESET_WANTED()
						ELIF IS_PED_SHOOTING(PLAYER_PED_ID())
						OR IS_PED_CLIMBING(PLAYER_PED_ID())
						OR IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
							#IF IS_DEBUG_BUILD SK_PRINT("Resetting wanted levels and setting player wanted higher as he did something bad") #ENDIF
							RESET_WANTED(2)
						ELIF GET_ENTITY_PLAYER_IS_FREE_AIMING_AT(PLAYER_ID(), ent)
							IF IS_ENTITY_A_PED(ent)
								PED_INDEX ped
								ped = GET_PED_INDEX_FROM_ENTITY_INDEX(ent)
								IF ped = mCop[COP_1].id
								OR ped = mCop[COP_2].id
								OR ped = mCop[COP_3].id
									#IF IS_DEBUG_BUILD SK_PRINT("Resetting wanted levels and setting player wanted higher as he aimed at a cop") #ENDIF
									RESET_WANTED(2)
								ENDIF
							ENDIF
						ENDIF
					ENDIF

					IF mCop[i].bCanSeePlayer
						UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
					ENDIF
				
					IF (IsPedPerformingTask(mCop[i].id, SCRIPT_TASK_INVALID)
						OR IsPedPerformingTask(mCop[i].id, SCRIPT_TASK_STAND_STILL))
					AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 2
						IF NOT IsPedPerformingTask(mCop[i].id, SCRIPT_TASK_ARREST_PED)
							#IF IS_DEBUG_BUILD SK_PRINT_INT("Cop is performing invalid task = ", i) #ENDIF
							TASK_ARREST_PED(mCop[i].id, PLAYER_PED_ID())
						ENDIF
					ENDIF

				ENDIF
			ENDIF

			IF NOT bExpireCopShoutAfterPlayer
				IF IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_RESTRICTED_ARREST_AREA))
				AND IS_BIT_SET(mCop[i].iReaction, ENUM_TO_INT(REAC_SEEN_IN_ALLEY))
					#IF IS_DEBUG_BUILD SK_PRINT("CRS_ARREST") #ENDIF

					STRING sConv
					IF i = 0
						sConv = "PRA_COPSHT1"
					ELSE
						sConv = "PRA_COPSHT2"
					ENDIF

					KILL_ANY_CONVERSATION()
					ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(s_conversation_peds, sTextBlock, sConv, CONV_PRIORITY_MEDIUM)
					bExpireCopShoutAfterPlayer = TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC COPS_STATE()

	COP_DIALOGUE_MANAGER()
	INT i
	FOR i=COP_1 TO COP_2
		IF mCop[i].eBehaviour <> CBS_REACTING
		AND	mCop[i].eReacting <> CRS_ARREST
			UPDATE_AI_PED_BLIP(mCop[i].id, mCop[i].mAIBlip)
		ELSE
			CLEANUP_AI_PED_BLIP(mCop[i].mAIBlip)
		ENDIF

		IF IS_PED_UNINJURED(mCop[i].id)
			MONITER_COP_PERCEPTIONS(i)
			SWITCH mCop[i].eBehaviour
				CASE CBS_NULL
				BREAK

				CASE CBS_NOT_ALERTED
					NOT_ALERTED_STATES(i)
				BREAK

				CASE CBS_QUESTIONING
					QUESTIONING_STATES(i)
				BREAK

				CASE CBS_REACTING
					REACTING_STATES(i)
				BREAK
			ENDSWITCH
		ELSE
			IF DOES_ENTITY_EXIST(mCop[i].id)
				IF IS_VEHICLE_OK(GET_PLAYERS_LAST_VEHICLE())
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mCop[i].id, GET_PLAYERS_LAST_VEHICLE())
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mCop[i].id)
						#IF IS_DEBUG_BUILD SK_PRINT_INT("Cop Damaged by player vehicle id = ", i) #ENDIF
						SET_COPS_TO_WAIT_REACT()
					ENDIF
				ENDIF

				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mCop[i].id, PLAYER_PED_ID())
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mCop[i].id)
					#IF IS_DEBUG_BUILD SK_PRINT_INT("Cop Damaged by player id = ", i) #ENDIF
					SET_COPS_TO_WAIT_REACT()
				ENDIF
				mCop[i].eBehaviour = CBS_NOT_ALERTED
				mCop[i].eNotAlerted = CNAS_USING_SCENARIO
			ENDIF

			SAFE_REMOVE_BLIP(mCop[i].mAIBlip.BlipID)
			SAFE_RELEASE_PED(mCop[i].id)
		ENDIF
		
	ENDFOR
ENDPROC
PROC MONITER_LONE_COP_SIGHT(INT i)
	CAN_COP_SEE_PED(i)
	MONITOR_PLAYER_SEEN_IN_AN_AREA(i)
	PLAYER_BUMPED_COP_CHECKS(i)
	MONITOR_COP_HEARING(i)
	IF mCop[i].eReacting <> CRS_ARREST
	OR mCop[i].eReacting <> CRS_WAIT_REACT_ARREST
		IF NOT bIsPlayerWearingCopUniform 
		AND NOT IS_PED_BEING_STEALTH_KILLED(mCop[i].id)
		AND NOT WAS_PED_KILLED_BY_STEALTH(mCop[i].id)
		AND NOT WAS_PED_KILLED_BY_TAKEDOWN(mCop[i].id)
			COP_DECISION_MAKER(i)
		ENDIF
	ENDIF
ENDPROC

PROC LONE_COP_PERCEPTIONS(INT i)

	MONITER_LONE_COP_SIGHT(i)
	IF NOT IS_PLAYER_BEHIND_UNARMED_STEALTH(i)
		IF HAS_PLAYER_THREATENED_PED(mCop[i].id, TRUE, 50, 140, FALSE, TRUE, TRUE, TRUE)
		OR IS_EXPLOSION_IN_SPHERE(EXP_TAG_DONTCARE, GET_ENTITY_COORDS(mCop[i].id), 100)
			IF NOT IS_PED_BEING_STEALTH_KILLED(mCop[i].id)
			AND NOT WAS_PED_KILLED_BY_STEALTH(mCop[i].id)
			AND NOT WAS_PED_KILLED_BY_TAKEDOWN(mCop[i].id)
				#IF IS_DEBUG_BUILD SK_PRINT_INT("Lone cop threatened = ", i) #ENDIF
				SET_COPS_TO_WAIT_REACT()
			ELSE
				IF NOT bIsPlayerWanted
					SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_STEALTH_KILL_COP)
					SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_KILL_COP)
					SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_HIT_COP)
					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				ENDIF
			ENDIF		
		ENDIF
	ELSE
//		#IF IS_DEBUG_BUILD SK_PRINT("SUPPRESS_CRIME_THIS_FRAME ") #ENDIF
		IF IS_ENTITY_IN_RANGE_ENTITY(mCop[i].id, PLAYER_PED_ID(), 5)
			IF NOT bIsPlayerWanted
				SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_STEALTH_KILL_COP)
				SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_KILL_COP)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC LONE_COP_STATE()
	INT i = COP_3

	IF mCop[i].eBehaviour <> CBS_REACTING
	AND	mCop[i].eReacting <> CRS_ARREST
		UPDATE_AI_PED_BLIP(mCop[i].id, mCop[i].mAIBlip)
	ELSE
		CLEANUP_AI_PED_BLIP(mCop[i].mAIBlip)
	ENDIF

	IF IS_PED_UNINJURED(mCop[i].id)
		LONE_COP_PERCEPTIONS(i)
		SWITCH mCop[i].eBehaviour
			CASE CBS_NULL
			BREAK

			CASE CBS_NOT_ALERTED
				NOT_ALERTED_STATES(i)
			BREAK

			CASE CBS_REACTING
				REACTING_STATES(i)
			BREAK
		ENDSWITCH
	ELSE
		IF DOES_ENTITY_EXIST(mCop[i].id)
		AND NOT IS_PED_INJURED(mCop[i].id)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mCop[i].id, PLAYER_PED_ID())
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(mCop[i].id)
				#IF IS_DEBUG_BUILD SK_PRINT_INT("Cop Damaged by player id = ", i) #ENDIF
				IF IS_PED_CURRENT_WEAPON_SILENCED(PLAYER_PED_ID())
				OR IS_PED_BEING_STEALTH_KILLED(mCop[i].id)
				OR WAS_PED_KILLED_BY_STEALTH(mCop[i].id)
				OR WAS_PED_KILLED_BY_TAKEDOWN(mCop[i].id)
					IF NOT bIsPlayerWanted
						SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_STEALTH_KILL_COP)
						SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_KILL_COP)
						SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
						SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
					ENDIF
				ENDIF
			ENDIF
			mCop[i].eBehaviour = CBS_NOT_ALERTED
			mCop[i].eNotAlerted = CNAS_USING_SCENARIO
		ENDIF
		SAFE_REMOVE_BLIP(mCop[i].mAIBlip.BlipID)
		SAFE_RELEASE_PED(mCop[i].id)
	ENDIF
ENDPROC

PROC COMPOUND_COP_STATES()
	IF NOT bJumpSkip
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) >= 1
			bIsPlayerWanted = TRUE
		ELSE
			SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_THROW_GRENADE)
			SUPPRESS_CRIME_THIS_FRAME(PLAYER_ID(), CRIME_MOLOTOV)
			bIsPlayerWanted = FALSE
		ENDIF
		IF eMissionState = MS_RETURN_WITH_THING
			IF NOT bDeletedCops
				IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), mStealCar.vPos, 180)
					INT i
					FOR i=0 TO (MAX_COPS-1)
						SAFE_RELEASE_PED(mCop[i].id)
					ENDFOR

					bDeletedCops = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		COPS_STATE()
		LONE_COP_STATE()
	ENDIF
ENDPROC

//PROC 
//ENDPROC

/// PURPOSE:
///    Jumps the mission to a specific stage
/// PARAMS:
///    stage - The state to jump to 
PROC JUMP_TO_STAGE(MISSION_STATE stage)
	RC_START_Z_SKIP()
	bJumpSkip = TRUE //Tells the mission stage setup function that we have just jumped and special setup is required
	eMissionState = stage 
	IF eMissionState = MS_SET_UP
		#IF IS_DEBUG_BUILD SK_PRINT("eMission state = MSS_SETUP GOING TO INTRO ") #ENDIF
		eMissionState = MS_SET_UP
	ENDIF
	SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
	SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//	bLoadingFinCutscene = FALSE
	bObjectiveShown = FALSE
	eState = SS_INIT
	CLEANUP() //delete everything
ENDPROC

/// PURPOSE:
///    Fills an new MYPED
/// PARAMS:
///    pos - position the ped is spawned at
///    dir - the heading it faces
///    mod - the model used to create it
/// RETURNS:
///    a newly initialised MYPED
FUNC MYPED FILL_PED(VECTOR pos, FLOAT dir, MODEL_NAMES mod)
	MYPED mTempPed
	
	mTempPed.vPos = pos
	mTempPed.fDir = dir
	mTempPed.Mod = mod
	
	RETURN mTempPed 
ENDFUNC

/// PURPOSE:
///    Fills a new MYVEHICLE
/// PARAMS:
///    pos - the position the vehicle is spawned at
///    dir - the heading it faces
///    mod - the model used to create it
/// RETURNS:
///    A newly initialised MYVEHICLE
FUNC MYVEHICLE FILL_VEHICLE(VECTOR pos, FLOAT dir, MODEL_NAMES mod)
	MYVEHICLE mTempVeh
	
	mTempVeh.vPos = pos
	mTempVeh.fDir = dir
	mTempVeh.Mod = mod
	
	RETURN mTempVeh 
ENDFUNC

/// PURPOSE:
///    Adds points to a poly 
/// PARAMS:
///    polys - The points to be added to the poly 
///    count - the number of points to be added to the poly 
PROC POPULATE_POLY_CHECKS(TEST_POLY &Area, VECTOR &polys[], INT count = MAX_POLY_TEST_VERTS)

	OPEN_TEST_POLY(Area)
	INT i
	
	FOR i = 0 TO (count-1)
		ADD_TEST_POLY_VERT(Area, polys[i])
	ENDFOR
	CLOSE_TEST_POLY(Area)

ENDPROC

/// PURPOSE:
///    Creates a poly check area using a local array
PROC CREATE_POLY_CHECKS()
	VECTOR polys[MAX_POLY_TEST_VERTS]
	polys[0] = <<331.76779, -1596.39563, 31.53864>> // top right
	polys[1] = <<348.49341, -1576.65662, 31.83304>> //right alley
	polys[2] = <<356.26553, -1567.39648, 28.29265>> //right alley
	polys[3] = <<368.48047, -1576.27393, 28.26307>> //right alley
	polys[4] = <<364.82422, -1580.73743, 28.29278>> //left alley 
	polys[5] = <<350.28958, -1606.61414, 28.29278>>  //top left
	polys[6] = <<346.13925, -1608.52283, 31.53354>> //in the corridor
	POPULATE_POLY_CHECKS(mCopConvoCheck1 ,polys, 7)

	polys[0] = <<353.65701, -1613.42139, 28.29278>> //top corridoor
	polys[1] = <<391.31555, -1601.63318, 28.29278>> //right of the gate - the side it moves to open
	polys[2] = <<397.66696, -1607.16772, 28.29278>> //right of the gate next to the post - the side it moves to open
	polys[3] = <<376.64902, -1633.09558, 26.96452>> //Top bu the underground carpark
	POPULATE_POLY_CHECKS(mCopConvoCheck2 ,polys, 4)


	polys[0] = << 344.0985, -1605.1539, 28.2928 >>
	polys[1] = << 365.6985, -1580.4351, 28.2928 >>
	polys[2] = << 407.9943, -1616.2589, 28.2928 >>
	polys[3] = << 387.3322, -1641.2583, 29.0912 >>
	POPULATE_POLY_CHECKS(mAreaCheck, polys, 4)
	
	OPEN_TEST_POLY(areaCopStation)
		ADD_TEST_POLY_VERT(areaCopStation, <<408.8245, -1616.0111, 31.7537>>)
		ADD_TEST_POLY_VERT(areaCopStation, <<376.3118, -1588.0358, 36.7534>>)
		ADD_TEST_POLY_VERT(areaCopStation, <<354.1272, -1614.8190, 31.5335>>)
		ADD_TEST_POLY_VERT(areaCopStation, <<387.2971, -1641.9429, 31.8330>>)
	CLOSE_TEST_POLY(areaCopStation)
	
ENDPROC

/// PURPOSE:
///    Fill in a new dialogue holder
/// PARAMS:
///    sConv - the coversation lable to store in this holder
///    iExpireBit - the bit that will be used to expire the conversation
/// RETURNS:
///    a newly initialised DIALOGUE_HOLDER
FUNC DIALOGUE_HOLDER FILL_DIALOGUE(STRING sConv, INT iExpireBit)
	DIALOGUE_HOLDER TempDial
	
	TempDial.sConv = sConv
	TempDial.iExpireBit = iExpireBit
	
	RETURN TempDial
	
ENDFUNC

PROC POPULATE_VEHICLES()
	mStealCar = FILL_VEHICLE(<<372.9868, -1623.5313, 28.2928>>, 321.7075, POLICET)
	
	mCopCars[0] = FILL_VEHICLE(<<400.5771, -1618.9274, 28.2928>>, 48.8115, POLICE3)
	mCopCars[1] = FILL_VEHICLE(<<396.6843, -1623.2220, 28.2928>>, 231.3392, POLICE3)
	mCopCars[2] = FILL_VEHICLE(<<394.3527, -1625.3374, 28.2928>>, 49.3732, POLICE3)	
	
ENDPROC

PROC POPULATE_PEDS()
	MODEL_NAMES mnCopMod = S_M_Y_Cop_01
	mCop[COP_1] = FILL_PED(<<353.1191, -1589.6061, 28.2928>>, 270.0968, mnCopMod)
	mCop[COP_2] = FILL_PED(<<354.5570, -1590.5488, 28.2928>>, 16.3702, 	mnCopMod)
	mCop[COP_3] = FILL_PED(<<369.8186, -1611.0293, 28.3928>>, 9.0837, 	mnCopMod)
	INT c, r
	FOR c=0 TO (MAX_COPS-1)
		mCop[c].eBehaviour = CBS_NULL
		mCop[c].eQuestioning = CQS_NULL
		mCop[c].eReacting = CRS_NULL
		mCop[c].bUpdateReaction = FALSE
		mCop[c].bCanSeePlayer = FALSE
		FOR r=0 TO (ENUM_TO_INT(NUM_REACT)-1)
			CLEAR_BIT(mCop[c].iReaction, r)
		ENDFOR
	ENDFOR
	
	bACopConvOnGoing = FALSE
	iCopConvID = 0

	sCopConvs[0] = "PRA_COPTLK1"
	sCopConvs[1] = "PRA_COPTLK2"
ENDPROC

PROC POPULATE_GATES()
	iSideGate 	= HASH("SIDE_GATE")

//	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID)
//		ADD_DOOR_TO_SYSTEM(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID, PROP_FACGATE_07B, <<397.8851, -1607.3861, 28.3417>>)
//	ENDIF
	
	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(iSideGate)
		ADD_DOOR_TO_SYSTEM(iSideGate, PROP_FNCLINK_03GATE5, <<391.8602, -1636.0702, 29.9744>>)
	ENDIF
	
	DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID, DOORSTATE_LOCKED)
	DOOR_SYSTEM_SET_DOOR_STATE(iSideGate, DOORSTATE_LOCKED)
ENDPROC

PROC POPULATE_CLIMB_POINTS()
	INT i
	FOR i=0 TO (MAX_BATTLE_BUDDIES-1)
		eBattleBuddyState[i] = BBOS_WAITING
		eBattleBuddyBehaviour[i] = BBB_CONTROL
	ENDFOR

	mClimbPoint[COP_ALLEY].vPos = <<344.4447, -1603.4282, 28.2928>>
	mClimbPoint[COP_ALLEY].fDir = 228.9052

	mClimbPoint[COP_COMP].vPos = <<345.9821, -1604.8014, 28.2928>>
	mClimbPoint[COP_COMP].fDir = 40.2137
ENDPROC

FUNC RESTRICTED_AREA FILL_RESTRICTED_AREA(VECTOR vPos1, VECTOR vPos2, FLOAT fWidth)
	RESTRICTED_AREA temp
	
	temp.vAreaPos1 = vPos1
	temp.vAreaPos2 = vPos2
	temp.fAreaWidth = fWidth
	
	RETURN temp
ENDFUNC

PROC POPULATE_RESTRICTED_AREAS()
	mRestrictedArea[RAI_FRONT_DESK_COPS_AREA] = FILL_RESTRICTED_AREA(<<359.52383, -1581.44470, 28.29278>>, <<341.13434, -1603.05249, 40.37049>>, 7.3)
	mRestrictedArea[RAI_WARNING_AREA] = FILL_RESTRICTED_AREA(<<352.23120, -1590.47070, 28.29277>>, <<341.13434, -1603.05249, 40.37049>>, 6.78)
	mRestrictedArea[RAI_RESTRICTED_ARREST_AREA] = FILL_RESTRICTED_AREA(<<344.58505, -1601.56238, 28.29278>>, <<342.42746, -1604.07141, 40.29278>>, 3.79)
	mRestrictedArea[RAI_BACK_ALLEY_AREA] = FILL_RESTRICTED_AREA(<<345.01022, -1604.09558, 40.67350>>, <<361.09140, -1617.45959, 28.29278>>, 3.33)
	mRestrictedArea[RAI_VEHICLE_AREA] = FILL_RESTRICTED_AREA(<<362.79855, -1577.08093, 28.29278>>, <<341.13434, -1603.05249, 40.37049>>, 6.78)
	mRestrictedArea[RAI_LONE_COP_AREA] = FILL_RESTRICTED_AREA(<<365.97729, -1611.55603, 28.29277>>, <<373.12787, -1617.80640, 40.37049>>, 19.19)
ENDPROC

/// PURPOSE:
///    Initialises all variables and structs
PROC POPULATE_STUFF()
	POPULATE_VEHICLES()
	POPULATE_PEDS()
	CREATE_POLY_CHECKS()
	POPULATE_GATES()
	POPULATE_CLIMB_POINTS()
	POPULATE_RESTRICTED_AREAS()
ENDPROC

/// PURPOSE:
///    Sets the Game world time after a replay/shit skip
PROC SET_TIME_FOR_REPLAY()
	IF g_bShitskipAccepted
	ELSE
	ENDIF

ENDPROC

///PURPOSE: 
///    Initiate the mission and load the things needed 
///    for the immediate gameplay
///    The skip menu is initialsed here
///    And if a replay is being done then we init and load assests for the check point
PROC INITMISSION()
	SWITCH eState
		CASE SS_INIT
			//DO_SCREEN_FADE_OUT(0)
			#IF IS_DEBUG_BUILD
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE (TRUE)
			#ENDIF
			
			
			#IF IS_DEBUG_BUILD  SK_PRINT("INIT MISSION - THIS WILL LOOP")  #ENDIF
			
			IF SETUP_MISSION_STAGE(eMissionState) 			
				MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_FINALE_PREP_A)
				#IF IS_DEBUG_BUILD
					s_skip_menu[0].sTxtLabel = "MS_STEAL"
					s_skip_menu[1].sTxtLabel = "MS_RETURN_WITH_THING"
				#ENDIF				

//				REGISTER_SCRIPT_WITH_AUDIO()
				
				SET_MAX_WANTED_LEVEL(1)
				SET_WANTED_LEVEL_MULTIPLIER(0.5)
				CLEAR_AREA_OF_PEDS(<<369.0115, -1609.1163, 28.2928>>, 5)
				CLEAR_AREA_OF_PEDS(mCop[0].vPos, 5)
				IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_HIGHWAY_PATROL )
				OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_HIGHWAY_PATROL )
					#IF IS_DEBUG_BUILD  SK_PRINT("Player wearing uniform")  #ENDIF
					bIsPlayerWearingCopUniform = TRUE
				ENDIF
				MARK_PREP_START_CAR_AS_VEH_GEN()
				INIT_BATTLEBUDDY_BEHAVIOUR_FOR_MISSION(SP_HEIST_FINALE_PREP_A)
				IF IS_REPLAY_IN_PROGRESS()
					END_REPLAY_SETUP()
					SAFE_FADE_SCREEN_IN_FROM_BLACK()
					eState = SS_CLEANUP
				ELSE
					IF IS_REPEAT_PLAY_ACTIVE()
						SET_PED_POS(PLAYER_PED_ID(),  <<408.3364, -1567.4294, 28.2723>>, 128.2512)
						WAIT_FOR_WORLD_TO_LOAD(<<408.3364, -1567.4294, 28.2723>>, 50)
						SAFE_FADE_SCREEN_IN_FROM_BLACK()
					ENDIF
					eState = SS_CLEANUP
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP INIT MISSION")  #ENDIF
			NEXT_STAGE()
		BREAK
	ENDSWITCH
ENDPROC

BOOL bRecordClimbingFlag

PROC STEAL()
	VEHICLE_INDEX viNullAnyCar
	MONITOR_IN_STEAL_CAR(mStealCar.id, viNullAnyCar, eStealCarState, biVehicleBlip, biBlip, vDropOffLoc)
	COMPOUND_COP_STATES()
	MONITOR_BATTLE_BUDDIES()
	SWITCH eState
		CASE SS_INIT
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				#IF IS_DEBUG_BUILD  SK_PRINT("INIT STEAL_VAN")  #ENDIF
				ADD_SAFE_BLIP_TO_VEHICLE(biBlip, mStealCar.id, TRUE)
				PRINT_OBJ("PR_GOVEH", bObjectiveShown)
				TRIGGER_MUSIC_EVENT("FHPRA_START")
//				iCompoundTimer = GET_GAME_TIMER()
//				bCompoundTimerStarted = TRUE
				IF IS_DOOR_REGISTERED_WITH_SYSTEM(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID)
					SET_SCRIPT_UPDATE_DOOR_AUDIO(g_sAutoDoorData[AUTODOOR_SC1_COP_CARPARK].doorID, TRUE)
				ENDIF
				bRecordClimbingFlag = FALSE
				REPLAY_RECORD_BACK_FOR_TIME(2.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				
				eState = SS_ACTIVE
			ENDIF
		BREAK

		CASE SS_ACTIVE
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) < 3
				IF IS_PED_UNINJURED(mCop[COP_3].id)
					IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<371.93933, -1604.25183, 45.60300>>, <<399.83713, -1627.06506, 31.75287>>, 32.73, FALSE, TRUE, TM_IN_VEHICLE)
						OR IS_PED_SHOOTING(PLAYER_PED_ID())	
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3) //player is trying to steal the vehicle in a cargo bob and the lone cop is still alive to he sees/hears it 
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF IS_PED_CLIMBING(PLAYER_PED_ID())
			AND bRecordClimbingFlag = FALSE
			AND GET_DISTANCE_BETWEEN_ENTITIES(mStealCar.id, PLAYER_PED_ID()) < 100.0
				REPLAY_RECORD_BACK_FOR_TIME(7.0, 7.0, REPLAY_IMPORTANCE_HIGHEST)
				bRecordClimbingFlag = TRUE
			ENDIF

			IF IS_VEHICLE_OK(mStealCar.id)
				IF eStealCarState = ISCM_IN_VEHICLE
				OR eStealCarState = ISCM_IN_ANY_VEHICLE
				OR eStealCarState = ISCM_IS_TOWING_VEHICLE
					REPLAY_RECORD_BACK_FOR_TIME(10.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
					eState = SS_CLEANUP
				ENDIF

				IF bIsPlayerWanted
					IF IS_POINT_IN_POLY_2D(mAreaCheck, GET_ENTITY_COORDS(PLAYER_PED_ID()))
						SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP STEAL_VAN")  #ENDIF
			SAFE_REMOVE_BLIP(biBlip)
			NEXT_STAGE()
		BREAK
		
		CASE SS_SKIPPED
			IF IS_VEHICLE_OK(mStealCar.id)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), mStealCar.id)
			ENDIF
			RC_END_Z_SKIP()
			eState = SS_ACTIVE
		BREAK
	ENDSWITCH
ENDPROC

PROC RETURN_THING()
	VEHICLE_INDEX viNullAnyCar
	MONITOR_IN_STEAL_CAR(mStealCar.id, viNullAnyCar, eStealCarState, biVehicleBlip, biBlip, vDropOffLoc)
	COMPOUND_COP_STATES()
	MONITOR_BATTLE_BUDDIES()
	SWITCH eState
		CASE SS_INIT
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				IF PREPARE_MUSIC_EVENT("FHPRA_VAN")
					#IF IS_DEBUG_BUILD  SK_PRINT("INIT TAKE_VAN_BACK")  #ENDIF
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0 
						IF eStealCarState = ISCM_IN_VEHICLE
						OR eStealCarState = ISCM_IN_ANY_VEHICLE
							KILL_FACE_TO_FACE_CONVERSATION()
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								PRINT_OBJ("PR_TAKBACK", bObjectiveShown)
							ENDIF
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
							TRIGGER_MUSIC_EVENT("FHPRA_VAN")
							ADD_BLIP_LOCATION(biBlip, vDropOffLoc)
						ENDIF
					ENDIF
		//				bCompoundTimerStarted = FALSE
					bPlayNewReport = FALSE
		
					eState = SS_ACTIVE
				ENDIF
			ENDIF
		BREAK

		CASE SS_ACTIVE
			MONITER_PLAYER_WANTED(biBlip, vDropOffLoc, eCopMonitor, eStealCarState, bObjectiveShown, bExpireReturnVehWanted)
			IF eCopMonitor = CM_LOSING_WANTED
				IF IS_POINT_IN_POLY_2D(mAreaCheck, GET_ENTITY_COORDS(PLAYER_PED_ID()))
					SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
				ENDIF

				IF NOT bPlayNewReport
					PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_BS_PREP_A_01", 0.0)
					bPlayNewReport = TRUE
				ENDIF
			ENDIF
			IF RETURN_STOLEN_VEHICLE(mStealCar.id, vDropOffLoc, biBlip, eCopMonitor, eStealCarState, bObjectiveShown, bExpireReturnVeh)
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
				eState = SS_CLEANUP
			ENDIF
		BREAK
		
		CASE SS_CLEANUP
			#IF IS_DEBUG_BUILD  SK_PRINT("CLEANEDUP TAKE_VAN_BACK")  #ENDIF
			SAFE_REMOVE_BLIP(biBlip)	
			STOP_MY_VEHICLE()
			NEXT_STAGE()			
		BREAK
		
		CASE SS_SKIPPED
			IF IS_VEHICLE_OK(mStealCar.id)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), mStealCar.id)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), mStealCar.id)
				ELSE
					SAFE_TELEPORT_ENTITY(mStealCar.id, vDropOffLoc, 126.0705)
					RC_END_Z_SKIP()
					eState = SS_ACTIVE
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

PROC LEAVE_RING_LESTER()
	VEHICLE_INDEX viNullAnyCar
	MONITOR_IN_STEAL_CAR(mStealCar.id, viNullAnyCar, eStealCarState, biVehicleBlip, biBlip, vDropOffLoc)
	SWITCH eState
		CASE SS_INIT
			IF SETUP_MISSION_STAGE(eMissionState, bJumpSkip)
				IF PREPARE_MUSIC_EVENT("FHPRA_STOP")
					#IF IS_DEBUG_BUILD  SK_PRINT("INIT LEAVE_RING_LESTER")  #ENDIF
					bExpireReturnVeh = FALSE
					bForceStop = FALSE
					eState = SS_ACTIVE
					TRIGGER_MUSIC_EVENT("FHPRA_STOP")
				ENDIF
			ENDIF
		BREAK

		CASE SS_ACTIVE
			MONITER_PLAYER_WANTED(biBlip, vDropOffLoc, eCopMonitor, eStealCarState, bObjectiveShown, bExpireReturnVehWanted)
			IF eCopMonitor = CM_LOSING_WANTED
				IF IS_POINT_IN_POLY_2D(mAreaCheck, GET_ENTITY_COORDS(PLAYER_PED_ID()))
					SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
				ENDIF
			ENDIF
			IF LEAVE_VEHICLE_STEAL(mStealCar.id, vDropOffLoc, biBlip, eStealCarState, eCopMonitor, bForceStop, bExpireReturnVeh, iCallTimerDelay, bCallTimer, bObjectiveShown)
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
				SET_STATS_WATCH_OFF()
				Script_Passed()
			ENDIF
		BREAK
		
		CASE SS_SKIPPED
			IF IS_VEHICLE_OK(mStealCar.id)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), mStealCar.id)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), mStealCar.id)
				ELSE
					SAFE_TELEPORT_ENTITY(mStealCar.id, vDropOffLoc, 126.0705)
					RC_END_Z_SKIP()
					eState = SS_ACTIVE
				ENDIF
			ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC

///DEBUG KEYS
#IF IS_DEBUG_BUILD

	/// PURPOSE: Check for Forced Pass or Fail
	PROC DEBUG_Check_Debug_Keys()

		// Check for Pass
			IF eState = SS_ACTIVE
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
					Script_Passed()
				ENDIF

				// Check for Fail
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
					MISSION_FAILED(FR_NONE)
				ENDIF
					
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)) 
					KILL_ANY_CONVERSATION()
					CLEAR_PRINTS()
					WAIT_FOR_CUTSCENE_TO_STOP()
						
					RC_START_Z_SKIP()
					eState = SS_SKIPPED
				ENDIF	
				
				IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)) 
				AND eMissionState <> MS_SET_UP 
					//Work out which stage we want to reach based on the current stage
					iMissionState = ENUM_TO_INT(eMissionState)
					
					IF iMissionState > 0	
						MISSION_STATE e_stage = INT_TO_ENUM(MISSION_STATE, iMissionState - 1)
						JUMP_TO_STAGE(e_stage)
					ENDIF
				ENDIF
			
			    IF LAUNCH_MISSION_STAGE_MENU(s_skip_menu, i_debug_jump_stage)
					#IF IS_DEBUG_BUILD SK_PRINT_INT("Z DEBUG Initial pick = ", i_debug_jump_stage) #ENDIF
					i_debug_jump_stage++
					
					SK_PRINT_INT("Z DEBUG ACTUAL STATE = ", i_debug_jump_stage) 

			        MISSION_STATE e_stage = INT_TO_ENUM(MISSION_STATE, i_debug_jump_stage)
			        JUMP_TO_STAGE(e_stage)
			    ENDIF
			ENDIF		
	ENDPROC
#ENDIF

PROC CHECK_STEAL_CAR_UPSIDEDOWN()
	IF IS_VEHICLE_STUCK_ON_ROOF(mStealCar.id)
		IF bOnRoofTimerStarted
			IF (GET_GAME_TIMER()- iUpsideDownTimer ) > 5000
				MISSION_FAILED(FR_ROOFSTUCK_STEAL_CAR)
				EXIT
			ENDIF
		ELSE
			iUpsideDownTimer = GET_GAME_TIMER()
			bOnRoofTimerStarted = TRUE
		ENDIF
	ELSE
		bOnRoofTimerStarted = FALSE
	ENDIF
	
	IF IS_CAR_STUCK(mStealCar.id)
		MISSION_FAILED(FR_ROOFSTUCK_STEAL_CAR)
	ENDIF
	
ENDPROC


PROC MONITER_STEAL_CAR()
	IF NOT IS_VEHICLE_OK(mStealCar.id) 
		MISSION_FAILED(FR_WRECKED_STEAL_CAR)
	ELSE
		CHECK_STEAL_CAR_UPSIDEDOWN()
		IF eMissionState = MS_RETURN_WITH_THING
		OR eMissionState = MS_LEAVE_VEHICLE_RING_LEST
			IF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mStealCar.id, 50)
			AND NOT bVanWarning
				PRINT_WARNING_OBJ("PR_VANWARN", bVanWarning)
			ELIF NOT IS_ENTITY_IN_RANGE_ENTITY(PLAYER_PED_ID(), mStealCar.id, 100)
			AND bVanWarning
				MISSION_FAILED(FR_LEFT_VAN)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MONITOR_PLAYER_DISTANCE_TO_COMPOUND()
	IF eMissionState <> MS_RETURN_WITH_THING
	AND eMissionState <> MS_LEAVE_VEHICLE_RING_LEST
		IF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), <<372.0053, -1604.0758, 36.7536>>, 100)
		AND NOT bAreaWarning
			PRINT_WARNING_OBJ("PR_AREAWARN", bAreaWarning)
		ELIF NOT IS_ENTITY_IN_RANGE_COORDS(PLAYER_PED_ID(), <<372.0053, -1604.0758, 36.7536>>, 170)
		AND bAreaWarning
			MISSION_FAILED(FR_LEFT_COPS)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Holds functions that moniter for the player failing
PROC CHECK_FOR_FAIL()
	IF NOT bJumpSkip
	AND eMissionState <> MS_FAILED
		IF eMissionState > MS_SET_UP
			MONITER_STEAL_CAR()
			MONITOR_PLAYER_DISTANCE_TO_COMPOUND()
		ENDIF
	ENDIF
ENDPROC


SCRIPT
	SET_MISSION_FLAG(TRUE)
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS|FORCE_CLEANUP_FLAG_DEBUG_MENU))
		PRINTSTRING("FORCE CLEAN UP") PRINTNL()
		SET_REPLAY_ACCEPTED_WARP_LOCATION()
		sFailReason = NULL
		TRIGGER_MUSIC_EVENT("FHPRA_FAIL")
		Mission_Flow_Mission_Force_Cleanup()
        Script_Cleanup()
	ENDIF

	#IF IS_DEBUG_BUILD
		SETUP_FOR_RAGE_WIDGETS()
		SET_PROFILING_OF_THIS_SCRIPT(TRUE)
	#ENDIF
	CREATE_RELATIONSHIP_GROUP()
	POPULATE_STUFF()
	siCopStation = ADD_SCENARIO_BLOCKING_AREA(<<402.9022, -1633.6042, 25.0472>>, <<315.6288, -1558.8143, 38.7931>>)
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREP_GOT_WANTED, FALSE)
//	
//	WHILE NOT SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_HIGHWAY_PATROL, FALSE)
//		WAIT(0)
//	ENDWHILE

	IF IS_REPLAY_IN_PROGRESS()
		START_REPLAY_SETUP(<<409.6954, -1567.0455, 28.2916>>, 138.7093)
	ENDIF
	
	REGISTER_SCRIPT_WITH_AUDIO()
	
	// Loop within here until the mission passes or fails
	WHILE(TRUE)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_STS")
		IF IS_PED_UNINJURED(PLAYER_PED_ID())
			CHECK_FOR_FAIL()
			MONITER_STATS()
			MONITOR_GATE()
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			SWITCH eMissionState
				CASE MS_SET_UP
					INITMISSION()
				BREAK

				CASE MS_STEAL
					STEAL()
				BREAK

				CASE MS_RETURN_WITH_THING
					RETURN_THING()
				BREAK

				CASE MS_LEAVE_VEHICLE_RING_LEST
					LEAVE_RING_LESTER()
				BREAK

				CASE MS_FAILED
					FAILED_WAIT_FOR_FADE()
				BREAK
			ENDSWITCH

			IF eMissionState <> MS_FAILED
				IF eMissionState >= MS_SET_UP
				AND NOT bJumpSkip
				ENDIF

				#IF IS_DEBUG_BUILD
					// Check debug completion/failure
					DEBUG_Check_Debug_Keys()
					UPDATE_RAG_WIDGETS()
				#ENDIF
			ENDIF
		ENDIF

		WAIT(0)

	ENDWHILE
// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
