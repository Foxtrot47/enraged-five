//╒═════════════════════════════════════════════════════════════════════════════╕
//│			  Author:  Ben Rollinson					Date: 18/05/11			│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│								Finale Heist 2 Intro							│
//│																				│
//│			Plays cutscenes that lead into the planning board sequence			│
//│			of this heist.														│
//│																				│
//╘═════════════════════════════════════════════════════════════════════════════╛

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_script.sch"
USING "commands_cutscene.sch"
USING "flow_public_core.sch"
USING "flow_public_game.sch"
USING "stripclub_public.sch"
USING "clearmissionarea.sch"

PROC Mission_Passed()
	//Always ensure this flowflag is set TRUE no matter how this script ends.
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PRIME_BOARD_TRANSITION, TRUE)
	MISSION_FLOW_MISSION_PASSED(TRUE)
	TERMINATE_THIS_THREAD()
ENDPROC


PROC Load_Cutscene()
	enumCharacterList ePlayer = GET_CURRENT_PLAYER_PED_ENUM()
	SWITCH ePlayer
		CASE CHAR_MICHAEL
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("BS_2A_2B_INT", CS_SECTION_3|CS_SECTION_4)
		BREAK
		CASE CHAR_FRANKLIN
			REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("BS_2A_2B_INT", CS_SECTION_2|CS_SECTION_3|CS_SECTION_4)
		BREAK
		CASE CHAR_TREVOR
			REQUEST_CUTSCENE("BS_2A_2B_INT")
		BREAK
	ENDSWITCH

	WHILE NOT HAS_THIS_CUTSCENE_LOADED("BS_2A_2B_INT")
		WAIT(0)
	ENDWHILE
ENDPROC


// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Mission_Passed()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
	
	Load_Cutscene()
	
	//Find the finale heist control thread.
	THREADID threadHeistController
	SCRIPT_THREAD_ITERATOR_RESET()
	BOOL bFoundController
	WHILE NOT bFoundController
		threadHeistController = SCRIPT_THREAD_ITERATOR_GET_NEXT_THREAD_ID()
		IF ARE_STRINGS_EQUAL(GET_NAME_OF_SCRIPT_WITH_THIS_ID(threadHeistController), "heist_ctrl_finale")
			bFoundController = TRUE
		ENDIF
	ENDWHILE
	//Allow the heist controller to play the preloaded cutscene.
	SET_SCRIPT_CAN_START_CUTSCENE(threadHeistController)
	DISABLE_STRIP_CLUBS()
	CPRINTLN(DEBUG_FLOW, GET_THIS_SCRIPT_NAME(), " handed over cutscene BS_2A_2B_INT to heist_ctrl_finale.")

	//B* 1834405: Clear all vehicles that may be near the planning board (motorcycles)
	RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<96.7,-1288.1,28.1>>,<<104.5,-1301,30>>,2,<<118.3,-1309.7,29>>,120)

	Mission_Passed()
ENDSCRIPT
