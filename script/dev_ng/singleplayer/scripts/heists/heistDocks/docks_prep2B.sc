//// *****************************************************************************************
//// *****************************************************************************************
//// *****************************************************************************************
////
////		MISSION NAME	:	Docks Prep 2b
////		AUTHOR			:	Ben Barclay
////		DESCRIPTION		:	Michael has to steal a CargoBob heli.
////
//// *****************************************************************************************
//// *****************************************************************************************
//// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"
USING "commands_fire.sch"
USING "replay_public.sch"
USING "selector_public.sch"
USING "mission_stat_public.sch"
USING "cutscene_public.sch"
USING "commands_cutscene.sch"
USING "script_player.sch"
USING "CompletionPercentage_public.sch"
USING "flow_public_GAME.sch"
USING "Clearmissionarea.sch"
USING "script_ped.sch"
USING "commands_object.sch"
USING "building_control_public.sch"
USING "locates_public.sch"
USING "area_checks.sch"
USING "battlebuddy_public.sch"
USING "commands_recording.sch"

/////══════════════════════════════════════╡ DEBUG VARIABLES  ╞════════════════════════════════════════
#IF IS_DEBUG_BUILD

USING "select_mission_stage.sch"

CONST_INT MAX_SKIP_MENU_LENGTH 4                       // number of stages in mission + 2 (for menu )
INT iReturnStage                                       // mission stage to jump to

MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]      // struct containing the debug menu 

#ENDIF

// ___________________________________ ENUMS ______________________________________________
ENUM MISSION_FLOW
	STAGE_OPENING_CUTSCENE,
	STAGE_INIT_MISSION,
	STAGE_STEAL_CARGOBOB,
	STAGE_ESCAPE_MILITARY,
	STAGE_FLY_CARGOBOB_TO_HANGER,
	STAGE_END_CUTSCENE,
	STAGE_MISSION_FAILED
ENDENUM
MISSION_FLOW missionStage = STAGE_OPENING_CUTSCENE

ENUM FAIL_REASON
	FAIL_DEFAULT,
	FAIL_HELI_DESTROYED,
	FAIL_HELI_ABANDONED,
	FAIL_HELI_STUCK
ENDENUM
FAIL_REASON eFailReason = FAIL_DEFAULT

//ENUM ROCKET_LINE_UP_STATE
//	LINE_UP_DONT_FIRE_TOO_FAR_AWAY,
//	LINE_UP_DONT_FIRE_BAD_ANGLE,
//	LINE_UP_FIRE_MISS_LEFT_SIDE,
//	LINE_UP_FIRE_MISS_RIGHT_SIDE,
//	LINE_UP_FIRE_LEFT_SIDE,
//	LINE_UP_FIRE_RIGHT_SIDE
//ENDENUM
//
//ENUM ROCKET_ANIM_STATE
//	ROCKET_ANIM_USING_AI,
//	ROCKET_ANIM_GET_LAUNCHER_OUT_THEN_AIM,
//	ROCKET_ANIM_PUT_LAUNCHER_AWAY,
//	ROCKET_ANIM_GET_CLOSER_THEN_AIM,
//	ROCKET_ANIM_RELOAD_THEN_AIM,
//	ROCKET_ANIM_AIMING
//ENDENUM
//ROCKET_ANIM_STATE e_rocket_anim_state 	= ROCKET_ANIM_USING_AI
//
ENUM ENEMY_HELI_ATTACK_STATE
	ENEMY_HELI_ATTACK_PLAYER_PED_ON_FOOT,
	ENEMY_HELI_ATTACK_PLAYER_VEHICLE_LOW,
	ENEMY_HELI_ATTACK_PLAYER_VEHICLE_HIGH,
	ENEMY_HELI_BACK_OFF_PLAYER,
	ENEMY_HELI_FLEE
ENDENUM
ENEMY_HELI_ATTACK_STATE EnemyHeliState 

//ENUM ENEMY_PED_ATTACK_STATE
//	PLAYER_CLOSE_RANGE,
//	PLAYER_FAR_RANGE,
//	PLAYER_NEAR_HELI_PEDS_FAR,
//	PLAYER_NEAR_HELI_PEDS_NEAR
//ENDENUM
//ENEMY_PED_ATTACK_STATE EnemyPedState[13] 

//STRUCT ROCKET_DATA
//      OBJECT_INDEX obj
//      ENTITY_INDEX entity_owner
//      VEHICLE_INDEX veh_target
//      VECTOR v_dir
//      VECTOR v_start
//      VECTOR v_rot
//      VECTOR v_offset
//      VECTOR v_vel
//      VECTOR v_prev_pos
//      BOOL b_reached_target
//      BOOL b_add_entity_speed
//      BOOL b_rocket_belongs_to_player
//      INT i_explode_timer
//      FLOAT f_speed_multiplier
//      PTFX_ID ptfx
//ENDSTRUCT
//
//ROCKET_DATA s_rockets[4]

// ===========================================================================================================
//		Variables
// ===========================================================================================================

//TEST PROCEDURE
//VEHICLE_INDEX testCargoBob
//CAMERA_INDEX testCam

//INDEXES
//Peds
PED_INDEX EnemyHeliPilot[1]
PED_INDEX MarineCop[13]
//PED_INDEX hBuddyMichael, hBuddyFranklin, hBuddyTrevor
//PED_INDEX InvisiblePed

//Vehicles
VEHICLE_INDEX buddyCar
VEHICLE_INDEX HeliToSteal
VEHICLE_INDEX EnemyHeli[1]
VEHICLE_INDEX GetAwayCar[1]
VEHICLE_INDEX ArmyJet1, ArmyJet2
VEHICLE_INDEX PlayerCar

//Cameras
CAMERA_INDEX RocketCam
CAMERA_INDEX EndSceneCam

//Objects
OBJECT_INDEX obj_lesters_rpg
OBJECT_INDEX obj_single_rocket

//Seats
//VEHICLE_SEAT VS_buddyMichael_seat, VS_buddyFranklin_seat, VS_buddyTrevor_seat

//Groups
REL_GROUP_HASH MARINE_GROUP_HASH

//Blips
BLIP_INDEX HeliToStealBlip
BLIP_INDEX HangerBlip
//BLIP_INDEX BuddyBlip
BLIP_INDEX EnemyHeliPilotBlip[1]
BLIP_INDEX ArmyJet1Blip, ArmyJet2Blip

//Strings
//STRING str_anim_rockets					= "missbigscore2big_5"
//STRING str_anim_rocket_reload			= "missbigscore2big_6"
//STRING str_anim_rocket_signal			= "missbigscore2big_7_p2"

SCENARIO_BLOCKING_INDEX RemoveHeliBlockingArea, RemoveHeliBlockingArea2, RemoveHeliBlockingArea3 //RemoveFrontGuardBlockingArea

AI_BLIP_STRUCT MarineCopBlip[13]

//Integers
INT iControlFlag 				= 0
INT icount
INT iArmyChatTimer
INT iCutsceneSkipStage
INT iArmyPedCount2
INT iArmyPedCount
INT iRandomInt2
//INT iRocketControlFlag
//INT iRocketFired
//INT i_player_rocket_event 		= 0
//INT i_lester_rocket_anim_event	= 0
//INT i_current_lester_target		= 0
//INT i_rocket_timer				
//INT i_rocket_line_up_dialogue_timer
//INT i_hBuddy_rocket_sync_scene
//INT iPlayerNotInAnyVehicleTimer = 0
INT iAlarmTimer
INT iGodTextTimer   			= 0
INT iAlarmStartCount			= 0
INT iFireCount					= 0
INT iChat58Timer				= 0
INT iChat57Timer				= 0
INT iEnemyPilotAccuracyTimer	= 0
INT iGetBackinTimer				= 0
INT iGetTohangerTextTimer		= 0
INT iBecomeHostileTimer 		= 0
INT iGuardCount
INT iGuard2Count
INT iWeaponStillEquipedTimer
INT iRandomInt
//INT iAlarmChatTimer


//Flags
BOOL MissionStageBeingSkippedTo
BOOL missionCanFail
BOOL DoneStealCargobobText 
BOOL DoneGetToHangerText
BOOL HeliToStealDestroyed
//BOOL LongDistanceCamRequired
//BOOL MichaelAITaskGiven
//BOOL TrevorAITaskGiven
//BOOL FranklinAITaskGiven
BOOL SetupMilitaryCops
//BOOL MichaelSwitchAvailable
//BOOL TrevorSwitchAvailable
//BOOL FranklinSwitchAvailable
BOOL EnemyHeliPilotTaskGiven[2]
BOOL Heli0RocketsEquipped
//BOOL Heli1RocketsEquipped
//BOOL FranklinIsNeeded
//BOOL TrevorIsNeeded
//BOOL MichaelIsNeeded
BOOL EnemyHeliDown[2]
BOOL godTextUpdtaed
//BOOL militaryFriendlyTaskGiven
BOOL playerIsInBase
//BOOL BuddyMichaelDrivingHeli
//BOOL BuddyTrevorDrivingHeli
//BOOL BuddyFranklinDrivingHeli
//BOOL BuddyMichaelDrivingAnyVehicle
//BOOL BuddyTrevorDrivingAnyVehicle
//BOOL BuddyFranklinDrivingAnyVehicle
//BOOL BuddyMichaelReadyToBeReleased
//BOOL BuddyTrevorReadyToBeReleased
//BOOL BuddyFranklinReadyToBeReleased
//BOOL TaskToStopVehicle
//BOOL nodesRequested
//BOOL RocketReadyToBeFired
BOOL AlarmStarted
//BOOL BuddyMichaelReadyToFireRockets
//BOOL BuddyTrevorReadyToFireRockets
//BOOL BuddyFranklinReadyToFireRockets
//BOOL BuddyMichaelRocketOveride
//BOOL BuddyTrevorRocketOveride
//BOOL BuddyFranklinRocketOveride
//BOOL BuddyMichaelDriverOveride
//BOOL BuddyTrevorDriverOveride
//BOOL BuddyFranklinDriverOveride
//BOOL PlayerNotInAVehicleTimerStarted
//BOOL BuddyMichaelReadyToDriveBy
//BOOL BuddyTrevorReadyToDriveBy
//BOOL BuddyFranklinReadyToDriveBy
BOOL playerIsNearHeli
//BOOL buddyMichaelCombatFlagSet
//BOOL buddyTrevorCombatFlagSet
//BOOL buddyFranklinCombatFlagSet
//BOOL FranklinAddedForChat
//BOOL MichaelAddedForChat
BOOL TrevorAddedForChat
//BOOL doneChat[50]
//BOOL EnemyHeliSpottedChatDone
//BOOL FoundRocketChatDone
//BOOL TakingAShotChatDone
//BOOL ReloadingChatDone
//BOOL RocketMissChatDone
//BOOL RocketHitChatDone
BOOL EndCarsCreated
//BOOL DoneEndCutChat
BOOL TaskNeedsUpdated[1]
//BOOL relGroupsSetup
BOOL militaryPNHPNTasksGiven[13]
BOOL militaryPNHPFTasksGiven[13]
BOOL militaryPCRTasksGiven[13]	
BOOL militaryPFRTasksGiven[13]
//BOOL NoBuddysAvailable
BOOL doneChat53
BOOL doneChat54
BOOL MarineCopSetup[13]
BOOL playerSpottedFiringAiming
BOOL doneChat55
BOOL doneChat56
BOOL AimTaskGiven[2]
BOOL TaskToSlowDownGiven
BOOL Accuracy0
BOOL Accuracy100
BOOL FiringTypeBullet
BOOL FiringTypeRocket
BOOL doneGetBackInText
BOOL cutsceneSkipped
BOOL playerWaitedTooLongToLeaveGate
BOOL hostileTimerSet
BOOL doneChat59
BOOL doneHelp4
BOOL TimerResetOnce
BOOL weaponTimerStarted
BOOL checkpointSet
BOOL donePhoneCall
BOOL doneArmyWrn1aChat
	


//Vectors
VECTOR vHeliCoords = << -2145.4856, 3018.2944, 31.8100 >>
VECTOR vHanger = << 1744.3083, 3270.6731, 40.2076 >>
//VECTOR vMilitaryBaseEntrance = <<-1560.8121, 2768.4988, 16.4692>>
//VECTOR vMichaelsCoords
//VECTOR vFranklinsCoords
//VECTOR vTrevorsCoords
VECTOR vEnemyHeli0Coords
//VECTOR vEnemyHeli1Coords
VECTOR vPlayerCoords
//VECTOR vDriveToHeliCoords
//VECTOR vbuddyCoords
//VECTOR RocketCamCoords

//Floats
FLOAT fHeliHeading = 330.4836
//FLOAT fMilitaryBaseEntranceHeading = 45.0943 
//FLOAT fGroundzCoord
//FLOAT fCamNewDirectionZCoord
//FLOAT fCamNewDirectionYCoord
//FLOAT fEnemyHeliDistance[2]

//Strings
STRING sFailReason

//Groups
//REL_GROUP_HASH invisibleGroup
//REL_GROUP_HASH SecGuardGroup

//Structs
SELECTOR_PED_STRUCT sSelectorPeds
SELECTOR_CAM_STRUCT sCamDetails
structPedsForConversation MyLocalPedStruct

//Camera Index
CAMERA_INDEX NodeCam[3]

//Sequences
SEQUENCE_INDEX seq


//PURPOSE: Stops dispatch types from spawning.
PROC ALLOW_DISPATCH(DISPATCH_TYPE dispatchType, BOOL bAllow)

      BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(dispatchType, (NOT bAllow))
      enable_dispatch_service(dispatchType, bAllow)

ENDPROC

//
//PROC SET_LESTER_ROCKET_ANIM_STATE(ROCKET_ANIM_STATE e_new_state)
//	e_rocket_anim_state = e_new_state
//	i_lester_rocket_anim_event = 0
//ENDPROC
//
////PURPOSE: Creates a rocket aimed at a vehicle target
//FUNC BOOL FIRE_ROCKET(VECTOR v_start, VEHICLE_INDEX veh_target, ENTITY_INDEX entity_owner, VECTOR v_offset, BOOL b_enemy_rocket = FALSE, BOOL b_track_target_speed = FALSE, FLOAT f_rocket_speed_multiplier = 1.0)
//	INT i = 0
//	INT i_rocket_index = -1
//	
//	REPEAT COUNT_OF(s_rockets) i
//		IF NOT DOES_ENTITY_EXIST(s_rockets[i].obj)
//			i_rocket_index = i
//		ENDIF
//	ENDREPEAT
//	
//	REQUEST_MODEL(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG))
//	REQUEST_PTFX_ASSET()
//	
//	IF HAS_MODEL_LOADED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG))
//	AND HAS_PTFX_ASSET_LOADED()
//	AND i_rocket_index > -1
//		s_rockets[i_rocket_index].obj = CREATE_OBJECT_NO_OFFSET(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG), v_start)
//		s_rockets[i_rocket_index].entity_owner = entity_owner
//		s_rockets[i_rocket_index].v_start = v_start
//		s_rockets[i_rocket_index].veh_target = veh_target
//		s_rockets[i_rocket_index].v_offset = v_offset
//		s_rockets[i_rocket_index].v_prev_pos = <<0.0, 0.0, 0.0>>
//		s_rockets[i_rocket_index].b_reached_target = FALSE
//		s_rockets[i_rocket_index].b_add_entity_speed = b_track_target_speed
//		s_rockets[i_rocket_index].b_rocket_belongs_to_player = NOT b_enemy_rocket
//		s_rockets[i_rocket_index].f_speed_multiplier = f_rocket_speed_multiplier
//		s_rockets[i_rocket_index].ptfx = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_bigscore_rpg_trail", s_rockets[i_rocket_index].obj, <<0.0, 0.0, 0.0>>,  <<0.0, 0.0, 0.0>>)
//		
//		SET_ENTITY_LOD_DIST(s_rockets[i_rocket_index].obj, 500)
//		SET_ENTITY_RECORDS_COLLISIONS(s_rockets[i_rocket_index].obj, TRUE)
//		SET_ENTITY_LOAD_COLLISION_FLAG(s_rockets[i_rocket_index].obj, TRUE)
//		SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG))
//		
////		IF NOT b_enemy_rocket
////			i_rocket_timer = 0 //When the timer = 0 no more rockets are allowed for the player.
////		ENDIF
//		
//		RETURN TRUE
//	ENDIF
//	
//	RETURN FALSE
//ENDFUNC
//
//
////PURPOSE: This goes through my array of rockets and updates them each frame.
//PROC UPDATE_ROCKETS()
//	CONST_FLOAT ROCKET_SPEED				60.0
//	CONST_FLOAT MAX_ROCKET_SPEED			79.0
//
//	INT i = 0
//	REPEAT COUNT_OF(s_rockets) i
//		IF DOES_ENTITY_EXIST(s_rockets[i].obj)
//			BOOL b_force_blow_up = FALSE
//			VECTOR v_current_pos = GET_ENTITY_COORDS(s_rockets[i].obj)
//			
//			//Update direction to home into offset, this lets us control how close the missile is to the target.
//			IF NOT s_rockets[i].b_reached_target
//			AND NOT IS_ENTITY_DEAD(s_rockets[i].veh_target)
//				VECTOR v_dest = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_rockets[i].veh_target, s_rockets[i].v_offset)
//				s_rockets[i].v_dir = (v_dest - v_current_pos) / VMAG(v_dest - v_current_pos)
//				
//				//Detect when rockets hit (or when they've missed and are flying away).
//				IF s_rockets[i].v_prev_pos.x != 0.0
//					FLOAT f_dist_from_target = VDIST2(v_current_pos, v_dest)
//					//FLOAT f_prev_dist_from_target = VDIST2(s_rockets[i].v_prev_pos, v_dest)
//					
//					IF s_rockets[i].v_offset.x = 0.0 AND s_rockets[i].v_offset.y = 0.0 s_rockets[i].v_offset.z = 0.0
//						//The rocket is meant to be on target, once it gets close blow it up
//						IF f_dist_from_target < 25.0
//							b_force_blow_up = TRUE
//							s_rockets[i].b_reached_target = TRUE
//						ENDIF
//					ELSE
//						IF f_dist_from_target < 100.0
//							//IF f_prev_dist_from_target < f_dist_from_target //The rocket is moving away from the target.
//								s_rockets[i].b_reached_target = TRUE
//							
//								//If the missed rocket was one the player fired then play some near-miss dialogue.
//								IF s_rockets[i].b_rocket_belongs_to_player
//									//i_time_since_last_rocket_missed = GET_GAME_TIMER()
//								ENDIF
//							//ENDIF
//						ENDIF
//					ENDIF
//					
//					IF s_rockets[i].b_reached_target
//						s_rockets[i].i_explode_timer = GET_GAME_TIMER()
//						
//						//Start the timer for the player's next rocket.
//						IF i_rocket_timer = 0
//						AND s_rockets[i].b_rocket_belongs_to_player
//							//b_played_line_up_dialogue = FALSE
//							i_rocket_timer = GET_GAME_TIMER()
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				s_rockets[i].v_vel = s_rockets[i].v_dir * ROCKET_SPEED * s_rockets[i].f_speed_multiplier
//				
//				IF s_rockets[i].b_add_entity_speed
//					s_rockets[i].v_vel = s_rockets[i].v_vel + GET_ENTITY_VELOCITY(s_rockets[i].veh_target)
//				ENDIF
//				
//				//Fix for B*550927: check the velocity to make sure it cannot go over the max amount.
//				IF VMAG(s_rockets[i].v_vel) > MAX_ROCKET_SPEED
//					s_rockets[i].v_vel = (s_rockets[i].v_vel / VMAG(s_rockets[i].v_vel)) * MAX_ROCKET_SPEED
//				ENDIF
//			ENDIF
//		
//			//Update rotation to match the direction of movement.
//			s_rockets[i].v_rot  = <<0.0, 0.0, 0.0>>
//			s_rockets[i].v_rot.x = ATAN2(s_rockets[i].v_dir.z, VMAG(<<s_rockets[i].v_dir.x, s_rockets[i].v_dir.y, 0.0>>))
//			s_rockets[i].v_rot.z = ATAN2(s_rockets[i].v_dir.y, s_rockets[i].v_dir.x) - 90.0
//		
//			//SET_ENTITY_COORDS(s_rockets[i].obj, v_current_pos +@ (s_rockets[i].v_dir * ROCKET_SPEED))
//			SET_ENTITY_VELOCITY(s_rockets[i].obj, s_rockets[i].v_vel)
//			SET_ENTITY_ROTATION(s_rockets[i].obj, s_rockets[i].v_rot)
//			
////			PRINTSTRING("Rocket velocity = ") PRINTVECTOR(s_rockets[i].v_vel) PRINTNL()
//			
////			#IF IS_DEBUG_BUILD
//////				IF b_debug_draw_debug
////					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
////					DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(s_rockets[i].obj), 0.3)
//////				ENDIF
////			#ENDIF
//			
//			//Explode on contact or if scripted to hit
//			IF (s_rockets[i].b_reached_target AND GET_GAME_TIMER() - s_rockets[i].i_explode_timer > 3000)
//			OR HAS_ENTITY_COLLIDED_WITH_ANYTHING(s_rockets[i].obj) AND NOT IS_ENTITY_TOUCHING_ENTITY(s_rockets[i].obj, s_rockets[i].entity_owner)
//			OR b_force_blow_up
//				IF NOT IS_ENTITY_DEAD(s_rockets[i].veh_target)
//					IF IS_ENTITY_TOUCHING_ENTITY(s_rockets[i].obj, s_rockets[i].veh_target)
//					OR b_force_blow_up
//						
//						EXPLODE_VEHICLE(s_rockets[i].veh_target)
//						SET_VEHICLE_PETROL_TANK_HEALTH(s_rockets[i].veh_target, -100.0)
//						//i_time_since_last_chopper_exploded = GET_GAME_TIMER()
//					ENDIF
//				ENDIF
//			
//				ADD_EXPLOSION(v_current_pos, EXP_TAG_ROCKET, 1.0)
//
//				IF s_rockets[i].ptfx != NULL
//					STOP_PARTICLE_FX_LOOPED(s_rockets[i].ptfx)
//					s_rockets[i].ptfx = NULL
//				ENDIF
//
//				DELETE_OBJECT(s_rockets[i].obj)
//			ENDIF	
//			
//			s_rockets[i].v_prev_pos = v_current_pos
//		ENDIF
//	ENDREPEAT
//ENDPROC
//
////PURPOSE: Updates rocket anims during the chase: Buddy has a number of different states he can be in, these states link together different anims.
//PROC UPDATE_BUDDY_ROCKET_ANIMS(PED_INDEX hbuddy)
//	REQUEST_ANIM_DICT(str_anim_rockets)
//	REQUEST_ANIM_DICT(str_anim_rocket_signal)
//	REQUEST_ANIM_DICT(str_anim_rocket_reload)
//
//	IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//	AND NOT IS_PED_INJURED(hbuddy)
//	AND HAS_ANIM_DICT_LOADED(str_anim_rockets)
//	AND HAS_ANIM_DICT_LOADED(str_anim_rocket_signal)
//	AND HAS_ANIM_DICT_LOADED(str_anim_rocket_reload)
//		IF NOT DOES_ENTITY_EXIST(obj_lesters_rpg)
//			REQUEST_MODEL(GET_WEAPONTYPE_MODEL(WEAPONTYPE_RPG))
//			REQUEST_MODEL(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG))
//			
//			IF HAS_MODEL_LOADED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_RPG))
//			AND HAS_MODEL_LOADED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG))
//				obj_lesters_rpg = CREATE_OBJECT(GET_WEAPONTYPE_MODEL(WEAPONTYPE_RPG), GET_ENTITY_COORDS(hBuddy))
//				ATTACH_ENTITY_TO_ENTITY(obj_lesters_rpg, hBuddy, GET_PED_BONE_INDEX(hBuddy, BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//				SET_ENTITY_VISIBLE(obj_lesters_rpg, FALSE)
//				
//				obj_single_rocket = CREATE_OBJECT(GET_WEAPONTYPE_MODEL(WEAPONTYPE_RPG), GET_ENTITY_COORDS(hBuddy))
//				ATTACH_ENTITY_TO_ENTITY(obj_single_rocket, hBuddy, GET_PED_BONE_INDEX(hBuddy, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//				SET_ENTITY_VISIBLE(obj_single_rocket, FALSE)
//				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(obj_single_rocket, FALSE)
//				
//				SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_RPG))
//				SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG))
//			ENDIF
//		ENDIF
//		
//		SWITCH e_rocket_anim_state
//			CASE ROCKET_ANIM_USING_AI
////				PRINTSTRING("e_rocket_anim_state = ROCKET_ANIM_USING_AI") PRINTNL()
//			BREAK
//			CASE ROCKET_ANIM_AIMING	
////				PRINTSTRING("e_rocket_anim_state = ROCKET_ANIM_AIMING") PRINTNL()
//			BREAK
//			CASE ROCKET_ANIM_GET_LAUNCHER_OUT_THEN_AIM
////				PRINTSTRING("e_rocket_anim_state = ROCKET_ANIM_GET_LAUNCHER_OUT_THEN_AIM") PRINTNL()
//				
//				IF DOES_ENTITY_EXIST(obj_lesters_rpg)
//					IF NOT IS_ENTITY_VISIBLE(obj_lesters_rpg)
//						SET_ENTITY_VISIBLE(obj_lesters_rpg, TRUE)
//					ENDIF
//				ENDIF
//			
//				IF i_lester_rocket_anim_event = 0
//					IF IS_ENTITY_ATTACHED(hBuddy)
//						CLEAR_PED_TASKS_IMMEDIATELY(hBuddy)
//						DETACH_ENTITY(hBuddy)
//					ENDIF
//					
//					i_hBuddy_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_hBuddy_rocket_sync_scene, HeliToSteal, GET_ENTITY_BONE_INDEX_BY_NAME(HeliToSteal, "seat_dside_r"))
//					TASK_SYNCHRONIZED_SCENE(hBuddy, i_hBuddy_rocket_sync_scene, str_anim_rockets, "idle_to_leansit", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, 
//												SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
//					SET_SYNCHRONIZED_SCENE_LOOPED(i_hBuddy_rocket_sync_scene, FALSE)
//					
//					i_lester_rocket_anim_event++
//				ELIF i_lester_rocket_anim_event = 1 
//					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_hBuddy_rocket_sync_scene)
//					AND GET_SYNCHRONIZED_SCENE_PHASE(i_hBuddy_rocket_sync_scene) > 0.99
//						i_hBuddy_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_hBuddy_rocket_sync_scene, HeliToSteal, GET_ENTITY_BONE_INDEX_BY_NAME(HeliToSteal, "seat_dside_r"))
//						TASK_SYNCHRONIZED_SCENE(hBuddy, i_hBuddy_rocket_sync_scene, str_anim_rockets, "intro_90r", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
//												SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
//						SET_SYNCHRONIZED_SCENE_LOOPED(i_hBuddy_rocket_sync_scene, FALSE)
//						
//						i_lester_rocket_anim_event++
//					ENDIF
//				ELIF i_lester_rocket_anim_event = 2
//					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_hBuddy_rocket_sync_scene)
//					AND GET_SYNCHRONIZED_SCENE_PHASE(i_hBuddy_rocket_sync_scene) > 0.99
//						i_hBuddy_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_hBuddy_rocket_sync_scene, HeliToSteal, GET_ENTITY_BONE_INDEX_BY_NAME(HeliToSteal, "seat_dside_r"))
//						TASK_SYNCHRONIZED_SCENE(hBuddy, i_hBuddy_rocket_sync_scene, str_anim_rockets, "sweep_medium", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
//												SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
//						SET_SYNCHRONIZED_SCENE_LOOPED(i_hBuddy_rocket_sync_scene, TRUE)
//						SET_SYNCHRONIZED_SCENE_PHASE(i_hBuddy_rocket_sync_scene, 0.98)
//						SET_SYNCHRONIZED_SCENE_RATE(i_hBuddy_rocket_sync_scene, 0.0)
//						
//						e_rocket_anim_state = ROCKET_ANIM_AIMING
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE ROCKET_ANIM_PUT_LAUNCHER_AWAY
////				PRINTSTRING("e_rocket_anim_state = ROCKET_ANIM_PUT_LAUNCHER_AWAY") PRINTNL()
//				IF i_lester_rocket_anim_event = 0
//					IF IS_ENTITY_ATTACHED(hBuddy)
//						CLEAR_PED_TASKS_IMMEDIATELY(hBuddy)
//						DETACH_ENTITY(hBuddy)
//					ENDIF
//					
//					i_hBuddy_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_hBuddy_rocket_sync_scene, HeliToSteal, GET_ENTITY_BONE_INDEX_BY_NAME(HeliToSteal, "seat_dside_r"))
//					TASK_SYNCHRONIZED_SCENE(hBuddy, i_hBuddy_rocket_sync_scene, str_anim_rockets, "outro_90r", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
//											SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
//					SET_SYNCHRONIZED_SCENE_LOOPED(i_hBuddy_rocket_sync_scene, FALSE)
//					
//					i_lester_rocket_anim_event++
//				ELIF i_lester_rocket_anim_event = 1 
//					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_hBuddy_rocket_sync_scene)
//					AND GET_SYNCHRONIZED_SCENE_PHASE(i_hBuddy_rocket_sync_scene) > 0.99
//						i_hBuddy_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_hBuddy_rocket_sync_scene, HeliToSteal, GET_ENTITY_BONE_INDEX_BY_NAME(HeliToSteal, "seat_dside_r"))
//						TASK_SYNCHRONIZED_SCENE(hBuddy, i_hBuddy_rocket_sync_scene, str_anim_rockets, "leansit_to_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
//												SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
//						SET_SYNCHRONIZED_SCENE_LOOPED(i_hBuddy_rocket_sync_scene, FALSE)
//						
//						i_lester_rocket_anim_event++
//					ENDIF
//				ELIF i_lester_rocket_anim_event = 2
//					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_hBuddy_rocket_sync_scene)
//					AND GET_SYNCHRONIZED_SCENE_PHASE(i_hBuddy_rocket_sync_scene) > 0.99
//						IF DOES_ENTITY_EXIST(obj_lesters_rpg)
//							SET_ENTITY_VISIBLE(obj_lesters_rpg, FALSE)
//						ENDIF
//					
//						i_hBuddy_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_hBuddy_rocket_sync_scene, HeliToSteal, GET_ENTITY_BONE_INDEX_BY_NAME(HeliToSteal, "seat_dside_r"))
//						TASK_SYNCHRONIZED_SCENE(hBuddy, i_hBuddy_rocket_sync_scene, str_anim_rockets, "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
//												SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
//						SET_SYNCHRONIZED_SCENE_LOOPED(i_hBuddy_rocket_sync_scene, TRUE)
//						
//						i_lester_rocket_anim_event++
//					ENDIF
//				ELIF i_lester_rocket_anim_event = 3
//					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_hBuddy_rocket_sync_scene)
//					AND GET_SYNCHRONIZED_SCENE_PHASE(i_hBuddy_rocket_sync_scene) > 0.99
//						STOP_SYNCHRONIZED_ENTITY_ANIM(hBuddy, NORMAL_BLEND_OUT, TRUE)
//						CLEAR_PED_TASKS_IMMEDIATELY(hBuddy)
//						
//						IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//							SET_PED_INTO_VEHICLE(hBuddy, HeliToSteal, VS_BACK_LEFT)
//						ENDIF
//						
//						i_lester_rocket_anim_event++
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE ROCKET_ANIM_GET_CLOSER_THEN_AIM
////				PRINTSTRING("e_rocket_anim_state = ROCKET_ANIM_GET_CLOSER_THEN_AIM") PRINTNL()
//				IF i_lester_rocket_anim_event = 0
//					IF IS_ENTITY_ATTACHED(hBuddy)
//						CLEAR_PED_TASKS_IMMEDIATELY(hBuddy)
//						DETACH_ENTITY(hBuddy)
//					ENDIF
//					
//					i_hBuddy_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_hBuddy_rocket_sync_scene, HeliToSteal, GET_ENTITY_BONE_INDEX_BY_NAME(HeliToSteal, "seat_dside_r"))
//					TASK_SYNCHRONIZED_SCENE(hBuddy, i_hBuddy_rocket_sync_scene, str_anim_rockets, "outro_90r", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
//											SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
//					SET_SYNCHRONIZED_SCENE_LOOPED(i_hBuddy_rocket_sync_scene, FALSE)
//					
//					i_lester_rocket_anim_event++
//				ELIF i_lester_rocket_anim_event = 1 
//					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_hBuddy_rocket_sync_scene)
//					AND GET_SYNCHRONIZED_SCENE_PHASE(i_hBuddy_rocket_sync_scene) > 0.99
//						i_hBuddy_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_hBuddy_rocket_sync_scene, HeliToSteal, GET_ENTITY_BONE_INDEX_BY_NAME(HeliToSteal, "seat_dside_r"))
//						TASK_SYNCHRONIZED_SCENE(hBuddy, i_hBuddy_rocket_sync_scene, str_anim_rocket_signal, "signal_forward", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
//												SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
//						SET_SYNCHRONIZED_SCENE_LOOPED(i_hBuddy_rocket_sync_scene, FALSE)
//						
//						i_lester_rocket_anim_event++
//					ENDIF
//				ELIF i_lester_rocket_anim_event = 2
//					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_hBuddy_rocket_sync_scene)
//					AND GET_SYNCHRONIZED_SCENE_PHASE(i_hBuddy_rocket_sync_scene) > 0.99
//						i_hBuddy_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_hBuddy_rocket_sync_scene, HeliToSteal, GET_ENTITY_BONE_INDEX_BY_NAME(HeliToSteal, "seat_dside_r"))
//						TASK_SYNCHRONIZED_SCENE(hBuddy, i_hBuddy_rocket_sync_scene, str_anim_rockets, "intro_90r", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
//												SYNCED_SCENE_DONT_INTERRUPT,  RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
//						SET_SYNCHRONIZED_SCENE_LOOPED(i_hBuddy_rocket_sync_scene, FALSE)
//						
//						i_lester_rocket_anim_event++
//					ENDIF
//				ELIF i_lester_rocket_anim_event = 3
//					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_hBuddy_rocket_sync_scene)
//					AND GET_SYNCHRONIZED_SCENE_PHASE(i_hBuddy_rocket_sync_scene) > 0.99
//						i_hBuddy_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_hBuddy_rocket_sync_scene, HeliToSteal, GET_ENTITY_BONE_INDEX_BY_NAME(HeliToSteal, "seat_dside_r"))
//						TASK_SYNCHRONIZED_SCENE(hBuddy, i_hBuddy_rocket_sync_scene, str_anim_rockets, "sweep_medium", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
//												SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
//						SET_SYNCHRONIZED_SCENE_LOOPED(i_hBuddy_rocket_sync_scene, TRUE)
//						SET_SYNCHRONIZED_SCENE_PHASE(i_hBuddy_rocket_sync_scene, 0.98)
//						SET_SYNCHRONIZED_SCENE_RATE(i_hBuddy_rocket_sync_scene, 0.0)
//						
//						e_rocket_anim_state = ROCKET_ANIM_AIMING
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE ROCKET_ANIM_RELOAD_THEN_AIM
////				PRINTSTRING("e_rocket_anim_state = ROCKET_ANIM_RELOAD_THEN_AIM") PRINTNL()
//				IF i_lester_rocket_anim_event = 0
//					IF IS_ENTITY_ATTACHED(hBuddy)
//						CLEAR_PED_TASKS_IMMEDIATELY(hBuddy)
//						DETACH_ENTITY(hBuddy)
//					ENDIF
//					
//					IF DOES_ENTITY_EXIST(obj_single_rocket)
//						SET_ENTITY_VISIBLE(obj_single_rocket, TRUE)
//					ENDIF
//					
//					i_hBuddy_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_hBuddy_rocket_sync_scene, HeliToSteal, GET_ENTITY_BONE_INDEX_BY_NAME(HeliToSteal, "seat_dside_r"))
//					TASK_SYNCHRONIZED_SCENE(hBuddy, i_hBuddy_rocket_sync_scene, str_anim_rockets, "outro_90r", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
//											SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
//					SET_SYNCHRONIZED_SCENE_LOOPED(i_hBuddy_rocket_sync_scene, FALSE)
//					
//					i_lester_rocket_anim_event++
//				ELIF i_lester_rocket_anim_event = 1 
//					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_hBuddy_rocket_sync_scene)
//					AND GET_SYNCHRONIZED_SCENE_PHASE(i_hBuddy_rocket_sync_scene) > 0.99
//						i_hBuddy_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_hBuddy_rocket_sync_scene, HeliToSteal, GET_ENTITY_BONE_INDEX_BY_NAME(HeliToSteal, "seat_dside_r"))
//						TASK_SYNCHRONIZED_SCENE(hBuddy, i_hBuddy_rocket_sync_scene, str_anim_rocket_reload, "reload_rocket_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
//												SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
//						SET_SYNCHRONIZED_SCENE_LOOPED(i_hBuddy_rocket_sync_scene, FALSE)
//						
//						i_lester_rocket_anim_event++
//					ENDIF
//				ELIF i_lester_rocket_anim_event = 2
//					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_hBuddy_rocket_sync_scene)
//					AND GET_SYNCHRONIZED_SCENE_PHASE(i_hBuddy_rocket_sync_scene) > 0.99
//						i_hBuddy_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_hBuddy_rocket_sync_scene, HeliToSteal, GET_ENTITY_BONE_INDEX_BY_NAME(HeliToSteal, "seat_dside_r"))
//						TASK_SYNCHRONIZED_SCENE(hBuddy, i_hBuddy_rocket_sync_scene, str_anim_rockets, "intro_90r", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
//												SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
//						SET_SYNCHRONIZED_SCENE_LOOPED(i_hBuddy_rocket_sync_scene, FALSE)
//						
//						i_lester_rocket_anim_event++
//					ENDIF
//				ELIF i_lester_rocket_anim_event = 3
//					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_hBuddy_rocket_sync_scene)
//					AND GET_SYNCHRONIZED_SCENE_PHASE(i_hBuddy_rocket_sync_scene) > 0.99
//						i_hBuddy_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
//						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_hBuddy_rocket_sync_scene, HeliToSteal, GET_ENTITY_BONE_INDEX_BY_NAME(HeliToSteal, "seat_dside_r"))
//						TASK_SYNCHRONIZED_SCENE(hBuddy, i_hBuddy_rocket_sync_scene, str_anim_rockets, "sweep_medium", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
//												SYNCED_SCENE_DONT_INTERRUPT, RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
//						SET_SYNCHRONIZED_SCENE_LOOPED(i_hBuddy_rocket_sync_scene, TRUE)
//						SET_SYNCHRONIZED_SCENE_PHASE(i_hBuddy_rocket_sync_scene, 0.98)
//						SET_SYNCHRONIZED_SCENE_RATE(i_hBuddy_rocket_sync_scene, 0.0)
//						
//						IF DOES_ENTITY_EXIST(obj_single_rocket)
//							SET_ENTITY_VISIBLE(obj_single_rocket, FALSE)
//						ENDIF
//						
//						e_rocket_anim_state = ROCKET_ANIM_AIMING
//					ENDIF
//				ENDIF
//			BREAK
//		ENDSWITCH
//	ENDIF
//ENDPROC
//
////PURPOSE: Checks if players buddy is in the driver seat of a vehicle the player is in.
//FUNC BOOL IS_BUDDY_IN_DRIVER_SEAT(PED_INDEX hPed)
//		
//	//Check if player is in any vehicle and if his buddy is in that vehicle's driving seat.			
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//		IF NOT IS_PED_INJURED(hPed)
//			IF IS_PED_IN_VEHICLE(hPed, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//			AND GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_DRIVER) = hPed
//				
//				//Check if player and buddy are in the helicopter
//				IF DOES_ENTITY_EXIST(HeliToSteal)
//					IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//						IF IS_PED_IN_VEHICLE(hPed, HeliToSteal)
//						AND GET_PED_IN_VEHICLE_SEAT(HeliToSteal, VS_DRIVER) = hPed	
//						AND NOT IS_ENTITY_AT_COORD(HeliToSteal, vHanger, <<8,8,4>>)
//							
//							IF hPed = hBuddyMichael
//								buddyMichaelDrivingHeli = TRUE
//								buddyMichaelDrivingAnyVehicle = FALSE
//							ENDIF
//							IF hPed = hBuddyTrevor
//								BuddyTrevorDrivingHeli = TRUE
//								BuddyTrevorDrivingAnyVehicle = FALSE
//							ENDIF
//							IF hPed = hBuddyFranklin
//								BuddyFranklinDrivingHeli = TRUE
//								BuddyFranklinDrivingAnyVehicle = FALSE
//							ENDIF
//							
//							RETURN TRUE
//							
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				//Check if player and buddy are in any other vehicle
//				IF GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) <> CARGOBOB
//					IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
//					OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
//					OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())))
//						
//						IF hPed = hBuddyMichael
//							BuddyMichaelDrivingHeli = FALSE
//							BuddyMichaelDrivingAnyVehicle = TRUE	
//						ENDIF
//						IF hPed = hBuddyTrevor
//							BuddyTrevorDrivingHeli = FALSE
//							BuddyTrevorDrivingAnyVehicle = TRUE	
//						ENDIF
//						IF hPed = hBuddyFranklin
//							BuddyFranklinDrivingHeli = FALSE
//							BuddyFranklinDrivingAnyVehicle = TRUE	
//						ENDIF							
//					
//						RETURN TRUE
//							
//					ENDIF	
//				ENDIF
//				
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	IF hPed = hBuddyMichael
//		BuddyMichaelDrivingHeli = FALSE
//		BuddyMichaelDrivingAnyVehicle = FALSE	
//	ENDIF
//	IF hPed = hBuddyTrevor
//		BuddyTrevorDrivingHeli = FALSE
//		BuddyTrevorDrivingAnyVehicle = FALSE	
//	ENDIF
//	IF hPed = hBuddyFranklin
//		BuddyFranklinDrivingHeli = FALSE
//		BuddyFranklinDrivingAnyVehicle = FALSE	
//	ENDIF	
//
//	RETURN FALSE
//ENDFUNC
//
////PURPOSE: Checks if players buddy is in the driver seat of a vehicle the player is in.
//FUNC BOOL IS_BUDDY_IN_BACK_SEAT(PED_INDEX hPed)
//		
//	//Check if player is in any vehicle and if his buddy is in that vehicle's driving seat.			
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//	AND IS_VEHICLE_DRIVEABLE(HeliToSteal)
//		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//			IF NOT IS_PED_INJURED(hPed)
//				IF IS_PED_IN_VEHICLE(hPed, HeliToSteal)
//					IF GET_PED_IN_VEHICLE_SEAT(HeliToSteal, VS_BACK_LEFT) = hPed
//						IF GET_ENTITY_HEIGHT_ABOVE_GROUND(HeliToSteal) > 20
//							IF hPed = hBuddyMichael
//								VS_buddyMichael_seat = VS_BACK_LEFT
//							ENDIF
//							IF hPed = hBuddyTrevor
//								VS_buddyTrevor_seat = VS_BACK_LEFT
//							ENDIF
//							IF hPed = hBuddyFranklin
//								VS_buddyFranklin_seat = VS_BACK_LEFT
//							ENDIF
//							RETURN TRUE
//						ENDIF
//					ENDIF
//					IF GET_PED_IN_VEHICLE_SEAT(HeliToSteal, VS_BACK_RIGHT) = hPed
//						IF GET_ENTITY_HEIGHT_ABOVE_GROUND(HeliToSteal) > 20
//							IF hPed = hBuddyMichael
//								VS_buddyMichael_seat = VS_BACK_RIGHT
//							ENDIF
//							IF hPed = hBuddyTrevor
//								VS_buddyTrevor_seat = VS_BACK_RIGHT
//							ENDIF
//							IF hPed = hBuddyFranklin
//								VS_buddyFranklin_seat = VS_BACK_RIGHT
//							ENDIF
//							RETURN TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//
//	RETURN FALSE
//ENDFUNC
//
////PURPOSE: Handles the player firing rockets from the heli rear passenger seats
//PROC DO_PLAYER_ROCKET_FIRE_FROM_HELI_SIDE()
//
//	IF iRocketControlFlag = 0
//		
//		RocketCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
//		REQUEST_MODEL(PROP_LD_W_LR_RPG)
//		
//		IF DOES_ENTITY_EXIST(HeliToSteal)
//			IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//				IF DOES_CAM_EXIST(RocketCam)
//					ATTACH_CAM_TO_ENTITY(RocketCam, HeliToSteal, << -1.40739, 4.40528, 0.236747 >>)
//					POINT_CAM_AT_COORD(RocketCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(HeliToSteal, << -10, 2.25834, -0.21304 >>))
//					SET_CAM_ACTIVE(RocketCam, TRUE)
//					SET_CAM_FOV(RocketCam, 50)
//					DISPLAY_RADAR(FALSE)
//					RENDER_SCRIPT_CAMS(TRUE, FALSE)
////					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
//					fCamNewDirectionZCoord = -0.21304
//					fCamNewDirectionYCoord = 2.25834
//					RocketReadyToBeFired = TRUE
//					iRocketControlFlag = 1
//				ENDIF
//			ENDIF
//		ENDIF
//		
//	ENDIF
//
//	IF iRocketControlFlag = 1
//
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
//		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
//		
////		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
//		//Get the hash INT for the RPG weapon through RAG -> UI -> HUD widget -> Reticle Override
//		SET_RETICULE_OVERRIDE_THIS_FRAME(-1312131151)
////		DISPLAY_SNIPER_SCOPE_THIS_FRAME()
//
//		// UP AND DOWN CONTROL CAMERA MOVEMENT
//		IF (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_UP_ONLY) AND NOT IS_LOOK_INVERTED())
//		OR (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_DOWN_ONLY) AND IS_LOOK_INVERTED())
//			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
//				fCamNewDirectionZCoord = (fCamNewDirectionZCoord + 0.25)
//			ELSE
//				fCamNewDirectionZCoord = (fCamNewDirectionZCoord + 0.5)
//			ENDIF
//		ENDIF
//		
//		IF (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_DOWN_ONLY) AND NOT IS_LOOK_INVERTED())
//		OR (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_UP_ONLY) AND IS_LOOK_INVERTED())
//			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
//				fCamNewDirectionZCoord = (fCamNewDirectionZCoord - 0.25)
//			ELSE
//				fCamNewDirectionZCoord = (fCamNewDirectionZCoord - 0.5)
//			ENDIF
//		ENDIF
//
//		IF fCamNewDirectionZCoord > 4.5
//			fCamNewDirectionZCoord = 4.5
//		ENDIF
//		IF fCamNewDirectionZCoord < -4.5
//			fCamNewDirectionZCoord = -4.5
//		ENDIF		
//
//
//		// LEFT AND RIGHT CONTROL CAMERA MOVEMENT		
//		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_LEFT_ONLY)
//			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
//				fCamNewDirectionYCoord = (fCamNewDirectionYCoord - 0.75)
//			ELSE
//				fCamNewDirectionYCoord = (fCamNewDirectionYCoord - 1.5)
//			ENDIF
//		ENDIF	
//		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_RIGHT_ONLY)
//			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
//				fCamNewDirectionYCoord = (fCamNewDirectionYCoord + 0.75)
//			ELSE
//				fCamNewDirectionYCoord = (fCamNewDirectionYCoord + 1.5)
//			ENDIF
//		ENDIF	
//	
//		IF fCamNewDirectionYCoord > 30
//			fCamNewDirectionYCoord = 30
//		ENDIF
//		IF fCamNewDirectionYCoord < -30
//			fCamNewDirectionYCoord = -30
//		ENDIF	
//		
//		POINT_CAM_AT_COORD(RocketCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(HeliToSteal, << -10, fCamNewDirectionYCoord, fCamNewDirectionZCoord >>))
//	
//		//Zoom in if aim is pressed.
//		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
//			SET_CAM_FOV(RocketCam, 37)
//		ELSE
//			SET_CAM_FOV(RocketCam, 50)
//		ENDIF
//		
//		RocketCamCoords = GET_CAM_COORD(RocketCam)
//		
////		PRINTSTRING("RocketCamCoords = ") PRINTVECTOR(RocketCamCoords) PRINTNL()
////		PRINTSTRING("VecEndCoords = ") PRINTVECTOR(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(HeliToSteal, << -10, fCamNewDirectionYCoord, fCamNewDirectionZCoord >>)) PRINTNL()
//		
//		//Fire rocket if control is pressed/
//		IF RocketReadyToBeFired = TRUE
//			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)
//				//Have a safety check as a temp fix for bug 1151740 until propper camera is implemented
//				IF RocketCamCoords.x > 3000
//					RocketCamCoords.x = 3000
//				ENDIF
//				IF RocketCamCoords.y > 3000
//					RocketCamCoords.y = 3000
//				ENDIF
//				IF RocketCamCoords.z > 800
//					RocketCamCoords.z = 800
//				ENDIF
//				SHOOT_SINGLE_BULLET_BETWEEN_COORDS(RocketCamCoords, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(HeliToSteal, << -10, fCamNewDirectionYCoord, fCamNewDirectionZCoord >>), 50, FALSE, WEAPONTYPE_RPG)
//				iRocketFired = GET_GAME_TIMER()
//				RocketReadyToBeFired = FALSE
//			ENDIF
//		ENDIF
//		
//		IF RocketReadyToBeFired = FALSE
//			IF GET_GAME_TIMER() > iRocketFired + 5000
//				RocketReadyToBeFired = TRUE
//			ENDIF
//		ENDIF
//	
////		IF HAS_ANY_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds)
////			DISPLAY_RADAR(TRUE)
////			RENDER_SCRIPT_CAMS(FALSE, FALSE)				
////		ENDIF
//		
//	ENDIF
//
//ENDPROC

//PURPOSE: Handles the mission stats system
PROC STATS_CONTROL()
	
	
ENDPROC

//
//FUNC ROCKET_LINE_UP_STATE GET_ROCKET_LINE_UP_STATE(VEHICLE_INDEX veh_target, VEHICLE_INDEX veh_player)
//	VECTOR v_offset
//	FLOAT f_vertical_angle, f_horizontal_angle, f_dist
//	ROCKET_LINE_UP_STATE e_rocket_state = LINE_UP_DONT_FIRE_TOO_FAR_AWAY
//	
//	IF NOT IS_ENTITY_DEAD(veh_target)
//	AND NOT IS_ENTITY_DEAD(veh_player)
//		//Rules:
//		// - If the player is too far away, don't fire at all.
//		// - If the player is on the wrong side, don't fire at all.
//		// - If the the player is on the right side but the angle is too tight, then fire but miss.
//		f_dist = VDIST2(GET_ENTITY_COORDS(veh_target), GET_ENTITY_COORDS(veh_player))
//		
//		IF f_dist < 10000.0
//			v_offset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(veh_player, GET_ENTITY_COORDS(veh_target))
//			f_vertical_angle = ABSF(ATAN2(ABSF(v_offset.x), ABSF(v_offset.z)))
//			f_horizontal_angle = ABSF(ATAN2(ABSF(v_offset.x), ABSF(v_offset.y)))
//			
//			IF f_vertical_angle > 20.0 AND f_horizontal_angle > 5.0
//				IF v_offset.x < 0.0
//					e_rocket_state = LINE_UP_FIRE_LEFT_SIDE
////					PRINTSTRING("e_rocket_state = LINE_UP_FIRE_LEFT_SIDE") PRINTNL()
//				ELSE
////					PRINTSTRING("e_rocket_state = LINE_UP_FIRE_RIGHT_SIDE") PRINTNL()
//					e_rocket_state = LINE_UP_FIRE_RIGHT_SIDE
//				ENDIF
//			ELSE
////				PRINTSTRING("e_rocket_state = LINE_UP_DONT_FIRE_BAD_ANGLE") PRINTNL()
//				e_rocket_state = LINE_UP_DONT_FIRE_BAD_ANGLE
//				/*IF v_offset.x < 0.0
//					e_rocket_state = LINE_UP_FIRE_MISS_LEFT_SIDE
//				ELSE
//					e_rocket_state = LINE_UP_FIRE_MISS_RIGHT_SIDE
//				ENDIF*/
//			ENDIF
//		ELSE
//			e_rocket_state = LINE_UP_DONT_FIRE_TOO_FAR_AWAY
////			PRINTSTRING("e_rocket_state = LINE_UP_DONT_FIRE_TOO_FAR_AWAY") PRINTNL()
//		ENDIF
//	ENDIF
//	
//	#IF IS_DEBUG_BUILD
//		//PRINTLN(f_dist, " ", v_offset, " ", f_vertical_angle, " ", f_horizontal_angle)
//	#ENDIF
//	
//	RETURN e_rocket_state
//ENDFUNC
//
//PROC UPDATE_TARGET_HELI()
//	
//	//Change target if current target is destroyed
//	IF i_current_lester_target = 0
//		IF DOES_ENTITY_EXIST(enemyheli[0])
//			IF NOT IS_VEHICLE_DRIVEABLE(enemyheli[0])
//				IF DOES_ENTITY_EXIST(enemyheli[1])
//					IF IS_VEHICLE_DRIVEABLE(enemyheli[1])
//						i_current_lester_target = 1
//					ENDIF
//				ENDIF
//			ENDIF
//		ELSE
//			IF DOES_ENTITY_EXIST(enemyheli[1])
//				IF IS_VEHICLE_DRIVEABLE(enemyheli[1])
//					i_current_lester_target = 1
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//
//	IF i_current_lester_target = 1
//		IF DOES_ENTITY_EXIST(enemyheli[1])
//			IF NOT IS_VEHICLE_DRIVEABLE(enemyheli[1])
//				IF DOES_ENTITY_EXIST(enemyheli[0])
//					IF IS_VEHICLE_DRIVEABLE(enemyheli[0])
//						i_current_lester_target = 0
//					ENDIF
//				ENDIF
//			ENDIF
//		ELSE
//			IF DOES_ENTITY_EXIST(enemyheli[0])
//				IF IS_VEHICLE_DRIVEABLE(enemyheli[0])
//					i_current_lester_target = 0
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//
//ENDPROC
//
//PROC HANDLE_BUDDY_FIRING_ROCKETS(PED_INDEX hBuddyPed)
//
////	PRINTSTRING("current heli target  = ") PRINTINT(i_current_lester_target) PRINTNL()
////	PRINTSTRING("rocket timer  = ") PRINTINT(i_rocket_timer) PRINTNL()
//	
//	//Lester rockets: don't do any of this stuff if we're in the military choppers.
//	ROCKET_LINE_UP_STATE e_rocket_state
//	FLOAT f_speed_multiplier = 1
//		
//	//Rockets: come from buddies, and later on enemies.
//	IF DOES_BLIP_EXIST(EnemyHeliPilotBlip[0])
//	OR DOES_BLIP_EXIST(EnemyHeliPilotBlip[1])
//	
//	
//		UPDATE_BUDDY_ROCKET_ANIMS(hBuddyPed)
//		
//		UPDATE_ROCKETS()
//		
////		PRINTSTRING("i_player_rocket_event = ") PRINTINT(i_player_rocket_event) PRINTNL()
//		
//		//Sequence of events for buddy rockets:
//		// - Waits for the player to get into position.
//		// - Fires the rocket.
//		// - Reloads.
//		// - If the rocket missed then repeat, otherwise pick a new target.
//		SWITCH i_player_rocket_event
//			CASE 0 //Lester gets the rocket launcher out.
//				SET_LESTER_ROCKET_ANIM_STATE(ROCKET_ANIM_USING_AI)
//				
//				//Move stage on if either heli is close to hbuddy
//				fEnemyHeliDistance[0] = GET_DISTANCE_BETWEEN_ENTITIES(EnemyHeli[0], hBuddyPed)
//				fEnemyHeliDistance[1] = GET_DISTANCE_BETWEEN_ENTITIES(EnemyHeli[1], hBuddyPed)
//				
//				//Check if heli 0 is closer than heli 1 and is under 300m's from hbuddy
//				IF fEnemyHeliDistance[0] < fEnemyHeliDistance[1]
//				AND fEnemyHeliDistance[0] < 300
////					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
////						IF CREATE_CONVERSATION(s_conversation_peds, str_dialogue_block, "BS2B_TREVOR", CONV_PRIORITY_MEDIUM)
//							SET_LESTER_ROCKET_ANIM_STATE(ROCKET_ANIM_GET_LAUNCHER_OUT_THEN_AIM)
////							PRINTSTRING("SET_LESTER_ROCKET_ANIM_STATE(ROCKET_ANIM_GET_LAUNCHER_OUT_THEN_AIM) ***** i_current_lester_target = 0") PRINTNL()
//							
//							i_current_lester_target = 0
//							i_player_rocket_event++
////						ENDIF
////					ENDIF
//				ENDIF
//				
//				//Check if heli 1 is closer than heli 0 and is under 300m's from hbuddy
//				IF fEnemyHeliDistance[1] < fEnemyHeliDistance[0]
//				AND fEnemyHeliDistance[1] < 300
////					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
////						IF CREATE_CONVERSATION(s_conversation_peds, str_dialogue_block, "BS2B_TREVOR", CONV_PRIORITY_MEDIUM)
//							SET_LESTER_ROCKET_ANIM_STATE(ROCKET_ANIM_GET_LAUNCHER_OUT_THEN_AIM)
////							PRINTSTRING("SET_LESTER_ROCKET_ANIM_STATE(ROCKET_ANIM_GET_LAUNCHER_OUT_THEN_AIM) ***** i_current_lester_target = 1") PRINTNL()
//							
//							i_current_lester_target = 1
//							i_player_rocket_event++
////						ENDIF
////					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 1 //Lester waits for the player to get into a good position, then fires.
//				
//				//Do help text to tell player ~s~Help Lester target the choppers by flying close to Merryweather and keeping level.~s~
////				IF NOT HAS_LABEL_BEEN_TRIGGERED("F3B_ROCHELP")
////					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(s_locates_data)
////						PRINT_HELP("F3B_ROCHELP")
////						SET_LABEL_AS_TRIGGERED("F3B_ROCHELP", TRUE)
////					ENDIF
////				ENDIF
//				
////				//Play reloading dialogue.
//				IF e_rocket_anim_state = ROCKET_ANIM_RELOAD_THEN_AIM
//					IF ReloadingChatDone = FALSE
//						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//							IF NOT IS_MESSAGE_BEING_DISPLAYED()
//							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//								IF FoundRocketChatDone = TRUE
//									//Check which buddy is firing the rockets for the correct line of dialogue
//									IF BuddyFranklinReadyToFireRockets = TRUE
//										IF FranklinAddedForChat = TRUE
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT41", CONV_PRIORITY_MEDIUM)
//												//Reloading!
//												ReloadingChatDone = TRUE
//											ENDIF
//										ENDIF
//									ELIF BuddyMichaelReadyToFireRockets = TRUE
//										IF MichaelAddedForChat = TRUE
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT40", CONV_PRIORITY_MEDIUM)
//												//Reloading!
//												ReloadingChatDone = TRUE
//											ENDIF
//										ENDIF
//									ELIF BuddyTrevorReadyToFireRockets = TRUE
//										IF TrevorAddedForChat = TRUE
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT42", CONV_PRIORITY_MEDIUM)
//												//Reloading!
//												ReloadingChatDone = TRUE
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF						
//					ENDIF	
//				ENDIF
//			
////				IF f_playback_time > f_min_time_to_shoot AND f_playback_time < f_max_time_to_shoot
//					
//					//Update the target heli here
//					UPDATE_TARGET_HELI()
//	
//					e_rocket_state = GET_ROCKET_LINE_UP_STATE(EnemyHeli[i_current_lester_target], HeliToSteal)
//					
//					IF GET_GAME_TIMER() - i_rocket_timer > 3000 //Guarantee some time between rockets.
//						IF e_rocket_state = LINE_UP_DONT_FIRE_TOO_FAR_AWAY
//							IF GET_GAME_TIMER() - i_rocket_line_up_dialogue_timer > 0
//								//Do some random dialogue for being too far away
//								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//									IF NOT IS_MESSAGE_BEING_DISPLAYED()
//									OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//										IF FoundRocketChatDone = TRUE
//											//Check which buddy is firing the rockets for the correct line of dialogue
//											IF BuddyFranklinReadyToFireRockets = TRUE
//												IF FranklinAddedForChat = TRUE
//													IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT26", CONV_PRIORITY_MEDIUM)
//														SET_LESTER_ROCKET_ANIM_STATE(ROCKET_ANIM_GET_CLOSER_THEN_AIM)
//														i_rocket_line_up_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 10000)
//														//I'm not hitting anything from this distance!
//														//You need to get closer, I can't hit anything from here!
//													ENDIF
//												ENDIF
//											ELIF BuddyMichaelReadyToFireRockets = TRUE
//												IF MichaelAddedForChat = TRUE
//													IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT25", CONV_PRIORITY_MEDIUM)
//														SET_LESTER_ROCKET_ANIM_STATE(ROCKET_ANIM_GET_CLOSER_THEN_AIM)
//														i_rocket_line_up_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 10000)
//														//I'm not hitting anything from this distance!
//														//You need to get closer, I can't hit anything from here!
//													ENDIF
//												ENDIF
//											ELIF BuddyTrevorReadyToFireRockets = TRUE
//												IF TrevorAddedForChat = TRUE
//													IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT27", CONV_PRIORITY_MEDIUM)
//														SET_LESTER_ROCKET_ANIM_STATE(ROCKET_ANIM_GET_CLOSER_THEN_AIM)
//														i_rocket_line_up_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 10000)
//														//You need to get closer, I can't hit anything from here!
//														//I'm not hitting anything from this distance!
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ELIF e_rocket_state = LINE_UP_DONT_FIRE_BAD_ANGLE
//							IF GET_GAME_TIMER() - i_rocket_line_up_dialogue_timer > 0
//								//Do some random dialogue for being too far away
//								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//									IF NOT IS_MESSAGE_BEING_DISPLAYED()
//									OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//										IF FoundRocketChatDone = TRUE
//											//Check which buddy is firing the rockets for the correct line of dialogue
//											IF BuddyFranklinReadyToFireRockets = TRUE
//												IF FranklinAddedForChat = TRUE
//													IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT29", CONV_PRIORITY_MEDIUM)
//														SET_LESTER_ROCKET_ANIM_STATE(ROCKET_ANIM_GET_CLOSER_THEN_AIM)
//														i_rocket_line_up_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 10000)
//														//The angle's too tight, I can't get a shot!
//														//Move over, I need a better angle!
//													ENDIF
//												ENDIF
//											ELIF BuddyMichaelReadyToFireRockets = TRUE
//												IF MichaelAddedForChat = TRUE
//													IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT28", CONV_PRIORITY_MEDIUM)
//														SET_LESTER_ROCKET_ANIM_STATE(ROCKET_ANIM_GET_CLOSER_THEN_AIM)
//														i_rocket_line_up_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 10000)
//														//The angle's too tight, I can't get a shot!
//														//Move over, I need a better angle!
//													ENDIF
//												ENDIF
//											ELIF BuddyTrevorReadyToFireRockets = TRUE
//												IF TrevorAddedForChat = TRUE
//													IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT30", CONV_PRIORITY_MEDIUM)
//														SET_LESTER_ROCKET_ANIM_STATE(ROCKET_ANIM_GET_CLOSER_THEN_AIM)
//														i_rocket_line_up_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 10000)
//														//The angle's too tight, I can't get a shot!
//														//Move over, I need a better angle!
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ELSE
//							IF NOT IS_ENTITY_DEAD(EnemyHeli[i_current_lester_target])
//							AND e_rocket_anim_state = ROCKET_ANIM_AIMING
//							
//								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//									IF TakingAShotChatDone = FALSE
//										IF NOT IS_MESSAGE_BEING_DISPLAYED()
//										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//											IF FoundRocketChatDone = TRUE
//												//Check which buddy is firing the rockets for the correct line of dialogue
//												IF BuddyFranklinReadyToFireRockets = TRUE
//													IF FranklinAddedForChat = TRUE
//														IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT32", CONV_PRIORITY_MEDIUM)
//															//I'm going for it, keep us steady!
//															//Going for a shot!
//															TakingAShotChatDone = TRUE
//														ENDIF
//													ENDIF
//												ELIF BuddyMichaelReadyToFireRockets = TRUE
//													IF MichaelAddedForChat = TRUE
//														IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT31", CONV_PRIORITY_MEDIUM)
//															//I'm going for it, keep us steady!
//															//Going for a shot!
//															TakingAShotChatDone = TRUE
//														ENDIF
//													ENDIF
//												ELIF BuddyTrevorReadyToFireRockets = TRUE
//													IF TrevorAddedForChat = TRUE
//														IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT33", CONV_PRIORITY_MEDIUM)
//															//I'm going for it, keep us steady!
//															//Going for a shot!
//															TakingAShotChatDone = TRUE
//														ENDIF
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							
//	//									PRINTSTRING("e_rocket_state = ROCKET_ANIM_AIMING") PRINTNL()
//								VECTOR v_start_pos, v_end_offset
//								
//								IF e_rocket_state = LINE_UP_FIRE_LEFT_SIDE
//									v_start_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(HeliToSteal, <<-1.2, 2.0, 1.2>>)
//									v_end_offset = <<0.0, 0.0, 0.0>>
//								ELIF e_rocket_state = LINE_UP_FIRE_RIGHT_SIDE
//									v_start_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(HeliToSteal, <<1.2, 2.0, 1.2>>)
//									v_end_offset = <<0.0, 0.0, 0.0>>
//								ELIF e_rocket_state = LINE_UP_FIRE_MISS_LEFT_SIDE
//									v_start_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(HeliToSteal, <<-1.2, 2.0, 1.2>>)
//									v_end_offset = <<5.0, -5.0, -5.0>>
//								ELIF e_rocket_state = LINE_UP_FIRE_MISS_RIGHT_SIDE
//									v_start_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(HeliToSteal,<<1.2, 2.0, 1.2>>)
//									v_end_offset = <<5.0, -5.0, -5.0>>
//								ENDIF
//								
//								IF FIRE_ROCKET(v_start_pos, EnemyHeli[i_current_lester_target], HeliToSteal, v_end_offset, FALSE, FALSE, f_speed_multiplier)
//	//										PRINTSTRING("FIRE_ROCKET RETURNING TRUE") PRINTNL()
//	//										SET_LABEL_AS_TRIGGERED("BS2B_RELOAD", FALSE)
//									SET_LESTER_ROCKET_ANIM_STATE(ROCKET_ANIM_RELOAD_THEN_AIM)
//									i_player_rocket_event++
//								ELSE
//	//										PRINTSTRING("FIRE_ROCKET RETURNING FALSE") PRINTNL()
//								ENDIF
//							ELSE
//								IF IS_ENTITY_DEAD(EnemyHeli[i_current_lester_target])
//									i_player_rocket_event++
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
////				ENDIF
//			BREAK
//			
//			CASE 2 //Reload and update target if they were hit.
//				IF i_rocket_timer != 0
//					IF NOT IS_VEHICLE_DRIVEABLE(EnemyHeli[i_current_lester_target]) //Chopper was blown up.
//						//Do some chat for hitting the heli
//						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//							IF RocketHitChatDone = FALSE
//								IF NOT IS_MESSAGE_BEING_DISPLAYED()
//								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//									IF FoundRocketChatDone = TRUE
//										//Check which buddy is firing the rockets for the correct line of dialogue
//										IF BuddyFranklinReadyToFireRockets = TRUE
//											IF FranklinAddedForChat = TRUE
//												IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT38", CONV_PRIORITY_MEDIUM)
//													//Gotcha!
//													//Boom!
//													//Hell yeah!
//													//Burn you fuckers!
//													RocketHitChatDone = TRUE
//												ENDIF
//											ENDIF
//										ELIF BuddyMichaelReadyToFireRockets = TRUE
//											IF MichaelAddedForChat = TRUE
//												IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT37", CONV_PRIORITY_MEDIUM)
//													//Gotcha!
//													//Boom!
//													//Hell yeah!
//													//Burn you fuckers!
//													RocketHitChatDone = TRUE
//												ENDIF
//											ENDIF
//										ELIF BuddyTrevorReadyToFireRockets = TRUE
//											IF TrevorAddedForChat = TRUE
//												IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT39", CONV_PRIORITY_MEDIUM)
//													//Gotcha!
//													//Boom!
//													//Hell yeah!
//													//Burn you fuckers!
//													RocketHitChatDone = TRUE
//												ENDIF
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF					
//
////						PRINTSTRING("CHOPPER WAS BLOWN UP") PRINTNL()
//						i_rocket_timer = GET_GAME_TIMER()
//						
//						//Change targets if the other heli is still alive and go back a stage
//						IF i_current_lester_target = 0
//							IF DOES_BLIP_EXIST(EnemyHeliPilotBlip[1])
//								i_current_lester_target = 1
//								i_player_rocket_event--
//							ELSE
//								i_player_rocket_event = 100 //All targets are dead.
//							ENDIF
//						ELIF i_current_lester_target = 1
//							IF DOES_BLIP_EXIST(EnemyHeliPilotBlip[0])
//								i_current_lester_target = 0
//								i_player_rocket_event--
//							ELSE
//								i_player_rocket_event = 100 //All targets are dead.
//							ENDIF
//						ENDIF 
//					ELSE
////						PRINTSTRING("CHOPPER WASN'T BLOWN UP") PRINTNL()
//
//						//Do some chat for missing the heli
//						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//							IF RocketMissChatDone = FALSE
//								IF NOT IS_MESSAGE_BEING_DISPLAYED()
//								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//									IF FoundRocketChatDone = TRUE
//										//Check which buddy is firing the rockets for the correct line of dialogue
//										IF BuddyFranklinReadyToFireRockets = TRUE
//											IF FranklinAddedForChat = TRUE
//												IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT35", CONV_PRIORITY_MEDIUM)
//													//Dammit!
//													//Shit!
//													//Fuck!
//													RocketMissChatDone = TRUE
//												ENDIF
//											ENDIF
//										ELIF BuddyMichaelReadyToFireRockets = TRUE
//											IF MichaelAddedForChat = TRUE
//												IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT34", CONV_PRIORITY_MEDIUM)
//													//Dammit!
//													//Shit!
//													//Fuck!
//													RocketMissChatDone = TRUE
//												ENDIF
//											ENDIF
//										ELIF BuddyTrevorReadyToFireRockets = TRUE
//											IF TrevorAddedForChat = TRUE
//												IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT36", CONV_PRIORITY_MEDIUM)
//													//Dammit!
//													//Shit!
//													//Fuck!
//													RocketMissChatDone = TRUE
//												ENDIF
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//						
//						i_rocket_timer = GET_GAME_TIMER()
//						i_player_rocket_event--						
//					ENDIF
//				ELSE
////					PRINTSTRING("STUCK COS i_rocket_timer = 0") PRINTNL()
//				ENDIF
//				IF i_player_rocket_event != 2
//					TakingAShotChatDone = FALSE
//					RocketMissChatDone = FALSE
//					RocketHitChatDone = FALSE
//				ENDIF
//			BREAK
//		ENDSWITCH
//		
//	ELSE
////		PRINTSTRING("BuddyReadyToFireRockets = FALSE") PRINTNL()
//		BuddyMichaelReadyToFireRockets = FALSE
//		BuddyTrevorReadyToFireRockets = FALSE
//		BuddyFranklinReadyToFireRockets = FALSE
//	ENDIF
//
//ENDPROC
//
////PURPOSE: Reset's or deletes everything if no battle buddy is overridden.
//PROC RESET_OR_DELETE_EVERYTHING_FOR_BATTLEBUDDY()
//	
//	//Delete the rocket 1st person cam if active
//	IF DOES_CAM_EXIST(RocketCam)
//		DISPLAY_RADAR(TRUE)
//		RENDER_SCRIPT_CAMS(FALSE, FALSE)
//		DESTROY_CAM(RocketCam)
//		iRocketControlFlag = 0
//	ENDIF
//			
//	//Flags
//	PlayerNotInAVehicleTimerStarted = FALSE
//	
//	iRocketControlFlag = 0
//		
//	//release the rode nodes if they were requested	
//	IF nodesRequested = TRUE
//		//RELEASE_PATH_NODES()
//		nodesRequested = FALSE
//	ENDIF					
//					
//ENDPROC


////Handles the battle buddies 
//PROC MONITOR_BATTLE_BUDDIES()
//		
//	hBuddyMichael = GET_BATTLEBUDDY_PED(CHAR_MICHAEL)
//	hBuddyFranklin = GET_BATTLEBUDDY_PED(CHAR_FRANKLIN)
//	hBuddyTrevor = GET_BATTLEBUDDY_PED(CHAR_TREVOR)
//	
//	//Handle Battle Buddy Michael if he's available
//	IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//		IF NOT IS_PED_INJURED(hBuddyMichael)
//			IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddyMichael)
//				
////				PRINTSTRING("IS_BATTLEBUDDY_OVERRIDDEN = FALSE") PRINTNL()
//				
//				//Reset delete anything that shouldn't be there if no buddies are overridden
//				RESET_OR_DELETE_EVERYTHING_FOR_BATTLEBUDDY()
//				
//				//Set buddy to take cover at the cargobob if near it and player is on foot
//				IF missionStage = STAGE_STEAL_CARGOBOB
//					IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//						IF GET_DISTANCE_BETWEEN_ENTITIES(hBuddyMichael, HeliToSteal) < 20
//							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//								IF IS_BATTLEBUDDY_BEHAVIOUR_REQUESTED(BBF_CombatDefendCargobobArea)
//									IF buddyMichaelCombatFlagSet = FALSE
//										SET_BATTLEBUDDY_BEHAVIOUR_REQUEST(BBF_CombatDefendCargobobArea)
//										buddyMichaelCombatFlagSet = TRUE
//									ENDIF
//								ENDIF
//							ELSE
//								IF buddyMichaelCombatFlagSet = TRUE
//									CLEAR_BATTLEBUDDY_BEHAVIOUR_REQUEST(BBF_CombatDefendCargobobArea)
//									buddyMichaelCombatFlagSet = FALSE
//								ENDIF
//							ENDIF
//						ELSE
//							IF buddyMichaelCombatFlagSet = TRUE
//								CLEAR_BATTLEBUDDY_BEHAVIOUR_REQUEST(BBF_CombatDefendCargobobArea)
//								buddyMichaelCombatFlagSet = FALSE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				//Dont take control of battle buddy until switch cam has finished.
//				IF NOT IS_SELECTOR_CAM_ACTIVE()
//					
//					//HAVE CHECKS FOR CASES WHEREBY BATTLE BUDDY WILL NEED TO BE OVERRIDDEN HERE
//					
//					// Is buddy driving the players vehicle
//					IF IS_BUDDY_IN_DRIVER_SEAT(hBuddyMichael)
////						PRINTSTRING("IS_BUDDY_IN_DRIVER_SEAT = TRUE") PRINTNL()
//						IF OVERRIDE_BATTLEBUDDY(hBuddyMichael, FALSE)
////							PRINTSTRING("OVERRIDE_BATTLEBUDDY has been called") PRINTNL()
//							SET_ENTITY_AS_MISSION_ENTITY(hBuddyMichael, TRUE, TRUE)
//							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyMichael, TRUE)
//							CLEAR_PED_TASKS(hBuddyMichael)
//							BuddyMichaelReadyToBeReleased = FALSE
//							BuddyMichaelRocketOveride = FALSE
//							BuddyMichaelDriverOveride = TRUE
//							BuddyMichaelReadyToDriveBy = FALSE
//							BuddyMichaelReadyToFireRockets = FALSE
//						ENDIF
//					ENDIF
//					
//					// Does script need to take control of buddy to fire rockets at enemychoppers
//					IF missionStage = STAGE_ESCAPE_MILITARY
//						IF EnemyHeliDown[0] = FALSE
//						OR EnemyHeliDown[1] = FALSE
//							IF IS_BUDDY_IN_BACK_SEAT(hBuddyMichael)
//								IF OVERRIDE_BATTLEBUDDY(hBuddyMichael, FALSE)
////									PRINTSTRING("OVERRIDE_BATTLEBUDDY has been called") PRINTNL()
//									SET_ENTITY_AS_MISSION_ENTITY(hBuddyMichael, TRUE, TRUE)
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyMichael, TRUE)
//									CLEAR_PED_TASKS(hBuddyMichael)
//									IF VS_buddyMichael_seat = VS_BACK_LEFT
//										i_player_rocket_event = 0
//										BuddyMichaelReadyToFireRockets = TRUE
//										BuddyMichaelRocketOveride = TRUE
//										BuddyMichaelReadyToDriveBy = FALSE
//									ENDIF
//									IF VS_buddyMichael_seat = VS_BACK_RIGHT
//										BuddyMichaelReadyToDriveBy = TRUE
//										BuddyMichaelRocketOveride = FALSE
//										BuddyMichaelReadyToFireRockets = FALSE
//									ENDIF
//									BuddyMichaelReadyToBeReleased = FALSE
//									BuddyMichaelDriverOveride = FALSE
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF		
//					
//				ENDIF
//				
//			ELSE
//				
////				PRINTSTRING("IS_BATTLEBUDDY_OVERRIDDEN = TRUE") PRINTNL()
//				
//				//GENERAL RULES THAT SHOULD ALWAYS ALLOW BUDDY TO BE RELEASED
//				//If player is not in the buddy's vehicle after 5 seconds release battle buddy
//				IF BuddyMichaelReadyToBeReleased = FALSE
//					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						IF PlayerNotInAVehicleTimerStarted = FALSE
//							iPlayerNotInAnyVehicleTimer = GET_GAME_TIMER()
//							PlayerNotInAVehicleTimerStarted = TRUE
//						ELSE
//							IF GET_GAME_TIMER() > (iPlayerNotInAnyVehicleTimer + 5000)
////								PRINTSTRING("BuddyMichaelReadyToBeReleased 1 = TRUE") PRINTNL()
//								BuddyMichaelReadyToBeReleased = TRUE
//							ENDIF
//						ENDIF
//					ELSE
//						//Check to make sure the player is in the same vehicle as the buddy, if not release the battle buddy.
//						IF IS_PED_IN_ANY_VEHICLE(hBuddyMichael)
//							IF IS_PED_IN_VEHICLE(hBuddyMichael, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//								//Reset flag for if player gets back in the vehicle under 5 seconds
//								PlayerNotInAVehicleTimerStarted = FALSE
//							ELSE
//								BuddyMichaelReadyToBeReleased = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				//If player switches then release all battle buddies
//				IF BuddyMichaelReadyToBeReleased = FALSE
//					IF IS_SELECTOR_CAM_ACTIVE()
////						PRINTSTRING("BuddyMichaelReadyToBeReleased 2 = TRUE") PRINTNL()
//						BuddyMichaelReadyToBeReleased = TRUE
//					ENDIF
//				ENDIF
//				
//				//Handle everything if buddy is firing rockets from side of heli, with player driving
//				IF BuddyMichaelRocketOveride = TRUE
//					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddyMichael)
//					AND BuddyMichaelReadyToBeReleased = FALSE
//						IF BuddyMichaelReadyToFireRockets = TRUE
//							HANDLE_BUDDY_FIRING_ROCKETS(hBuddyMichael)
//						ELSE
////							PRINTSTRING("BuddyMichaelReadyToBeReleased 4 = TRUE") PRINTNL()
//							BuddyMichaelReadyToBeReleased = TRUE
//						ENDIF
//					ELSE
//						IF BuddyMichaelReadyToBeReleased = TRUE
//							IF DOES_ENTITY_EXIST(obj_lesters_rpg)
//								DELETE_OBJECT(obj_lesters_rpg)
//							ENDIF
//							IF DOES_ENTITY_EXIST(obj_single_rocket)
//								DELETE_OBJECT(obj_single_rocket)
//							ENDIF
//							CLEAR_PED_TASKS_IMMEDIATELY(hBuddyMichael)
//							IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//								IF NOT IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//									SET_PED_INTO_VEHICLE(hBuddyMichael, HeliToSteal, VS_buddyMichael_seat)
//								ENDIF
//							ENDIF
//							IF RELEASE_BATTLEBUDDY(hBuddyMichael)
//								BuddyMichaelRocketOveride = FALSE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF					
//				
//				//Handle everything if buddy is ready to drive by in the heli's back right seat.
//				IF BuddyMichaelReadyToDriveBy = TRUE
//					// Does script still need to take control of buddy to do drive by?
//					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddyMichael)
//					AND BuddyMichaelReadyToBeReleased = FALSE	
//						IF  GET_SCRIPT_TASK_STATUS(hBuddyMichael, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//						AND GET_SCRIPT_TASK_STATUS(hBuddyMichael, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK	
//							IF DOES_ENTITY_EXIST(EnemyHeli[0])
//								IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
//									TASK_DRIVE_BY(hBuddyMichael, NULL, EnemyHeli[0], <<0,0,0>>, 10000, 100)
//								ELSE
//									IF DOES_ENTITY_EXIST(EnemyHeli[1])
//										IF IS_VEHICLE_DRIVEABLE(EnemyHeli[1])
//											TASK_DRIVE_BY(hBuddyMichael, NULL, EnemyHeli[1], <<0,0,0>>, 10000, 100)
//										ENDIF	
//									ENDIF
//								ENDIF
//							ELSE
//								IF DOES_ENTITY_EXIST(EnemyHeli[1])
//									IF IS_VEHICLE_DRIVEABLE(EnemyHeli[1])
//										TASK_DRIVE_BY(hBuddyMichael, NULL, EnemyHeli[1], <<0,0,0>>, 10000, 100)
//									ENDIF	
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						IF RELEASE_BATTLEBUDDY(hBuddyMichael)
//							BuddyMichaelReadyToDriveBy = FALSE
//						ENDIF	
//					ENDIF
//				ENDIF
//				
//				//Handle everything if buddy is driving heli with player firing rockets from the side.
//				IF BuddyMichaelDriverOveride = TRUE
//					
////					PRINTSTRING("BuddyMichaelDriverOveride = TRUE") PRINTNL()
//					
//					//Handle player firing rockets from side of heli here as buddy will be overridden to fly the heli at this point.
//					IF DOES_ENTITY_EXIST(HeliToSteal)
//						IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//								IF GET_PED_VEHICLE_SEAT(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = VS_BACK_LEFT
//									DO_PLAYER_ROCKET_FIRE_FROM_HELI_SIDE()
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				
//					// Does script still need to take control of buddy and drive to dest?
//					IF IS_BUDDY_IN_DRIVER_SEAT(hBuddyMichael)
//					AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddyMichael)
//					AND BuddyMichaelReadyToBeReleased = FALSE	
////						PRINTSTRING("IS_BUDDY_IN_DRIVER_SEAT = TRUE...AND..DOES_ENTITY_BELONG_TO_THIS_SCRIPT = TRUE") PRINTNL()
//						
//						// Give buddy task to fly heli to hanger
//						IF BuddyMichaelDrivingHeli = TRUE
//							vbuddyCoords = GET_ENTITY_COORDS(hBuddyMichael)
//							IF  GET_SCRIPT_TASK_STATUS(hBuddyMichael, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//							AND GET_SCRIPT_TASK_STATUS(hBuddyMichael, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
//								IF IS_VEHICLE_DRIVEABLE(HeliToSteal)	
//									OPEN_SEQUENCE_TASK(seq)
//										IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//											TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vbuddyCoords.x,vbuddyCoords.y, (vbuddyCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//										ENDIF
//										TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 20, -1, 240, 50)
//										TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 5, 1, -1, 0, 0)
//									CLOSE_SEQUENCE_TASK(seq)
//									TASK_PERFORM_SEQUENCE(hBuddyMichael, seq)
//									CLEAR_SEQUENCE_TASK(seq)	
//								ENDIF
//							ENDIF
//						ENDIF
//							
//							
//						//Give buddy task to drive the car to the heli 	
//						IF BuddyMichaelDrivingAnyVehicle = TRUE
//							vDriveToHeliCoords = GET_ENTITY_COORDS(heliToSteal)
//							vbuddyCoords = GET_ENTITY_COORDS(hBuddyMichael)
//							REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vbuddyCoords.x, vbuddyCoords.y, vDriveToHeliCoords.x, vDriveToHeliCoords.y)
////							PRINTSTRING("buddyDrivingAnyVehicle = TRUE") PRINTNL()
//							IF  GET_SCRIPT_TASK_STATUS(hBuddyMichael, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//							AND GET_SCRIPT_TASK_STATUS(hBuddyMichael, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
//								IF ARE_NODES_LOADED_FOR_AREA(vbuddyCoords.x, vbuddyCoords.y, vDriveToHeliCoords.x, vDriveToHeliCoords.y)
//									TASK_VEHICLE_MISSION_COORS_TARGET(hBuddyMichael, GET_VEHICLE_PED_IS_IN(hBuddyMichael), vDriveToHeliCoords, MISSION_GOTO, 40.0, DRIVINGMODE_PLOUGHTHROUGH|DRIVINGMODE_AVOIDCARS_RECKLESS, 10.0, 10.0)
//								ENDIF
//							ENDIF
//						ELSE
////							PRINTSTRING("buddyDrivingAnyVehicle = FALSE") PRINTNL()
//						ENDIF
//						
//						//Run a check for player being driven to the heli by a buddy. Dont allow release of buddy till player is out of the car.
//						//Bring the car to a halt once close enough to the helicopter.
////							IF IS_ENTITY_IN_ANGLED_AREA(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), <<-2115.419434,3038.640137,30.809973>>, <<-2144.749512,3055.938232,36.309971>>, 16.750000)
////							PRINTSTRING("Players vehicle is in the area where it should be brought to a halt") PRINTNL()
//						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(heliToSteal), GET_ENTITY_COORDS(hBuddyMichael)) < 15
//							IF NOT IS_VEHICLE_STOPPED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
////								PRINTSTRING("Vehicle is not stopped") PRINTNL()
//								IF IS_PED_IN_VEHICLE(hBuddyMichael, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//									IF NOT IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
////										PRINTSTRING("BRING_VEHICLE_TO_HALT is being called") PRINTNL()
//										BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 7, 10)
//									ENDIF
//								ENDIF
//							ELSE
//								TaskToStopVehicle = TRUE
//							ENDIF
//						ELSE
////							PRINTSTRING("Players vehicle is NOT in the area where it should be brought to a halt") PRINTNL()
//						ENDIF
//						
//						IF TaskToStopVehicle = TRUE
//							IF IS_PED_IN_VEHICLE(hBuddyMichael, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//								IF IS_VEHICLE_STOPPED(GET_VEHICLE_PED_IS_IN(hBuddyMichael))
//									CLEAR_PED_TASKS(hBuddyMichael)
//									REMOVE_PED_FROM_GROUP(hBuddyMichael)
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyMichael, TRUE)
//									SET_PED_SPHERE_DEFENSIVE_AREA(hBuddyMichael, GET_ENTITY_COORDS(hBuddyMichael), 15)
//									OPEN_SEQUENCE_TASK(seq)
//										TASK_LEAVE_ANY_VEHICLE(NULL)
//										TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 50)
//									CLOSE_SEQUENCE_TASK(seq)
//									TASK_PERFORM_SEQUENCE(hBuddyMichael, seq)
//									CLEAR_SEQUENCE_TASK(seq)
//									SET_PED_COMBAT_ATTRIBUTES(hBuddyMichael, CA_USE_VEHICLE, FALSE) 
//									SET_PED_COMBAT_ATTRIBUTES(hBuddyMichael, CA_LEAVE_VEHICLES, TRUE)
////									PRINTSTRING("Sequence task given to buddy") PRINTNL()
//									TaskToStopVehicle = FALSE
//								ENDIF
//							ENDIF					
//						ENDIF	
//						
//					ELSE
//						IF BuddyMichaelReadyToBeReleased = TRUE
////							PRINTSTRING("BuddyReadyToBeReleased = TRUE so RELEASE_BATTLEBUDY BEING CALLED") PRINTNL()
//							RELEASE_BATTLEBUDDY(hBuddyMichael)
//							BuddyMichaelDriverOveride = FALSE
//						ELSE
////							PRINTSTRING("BuddyReadyToBeReleased = FALSE") PRINTNL()
//						ENDIF
//					ENDIF
//				ENDIF
//				
////					//Some debug testing
////					IF IS_BUDDY_IN_DRIVER_SEAT(hBuddy)
////						PRINTSTRING("Buddy is in the driver seat") PRINTNL()
////					ELSE
////						PRINTSTRING("Buddy is NOT in the driver seat") PRINTNL()
////					ENDIF
////					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddy)
////						PRINTSTRING("Buddy belongs to this script") PRINTNL()
////					ELSE
////						PRINTSTRING("Buddy does not belong to this script") PRINTNL()
////					ENDIF
//				
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	//Handle Battle Buddy Trevor if he's available
//	IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//		IF NOT IS_PED_INJURED(hBuddyTrevor)
//			IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddyTrevor)
//				
////				PRINTSTRING("IS_BATTLEBUDDY_OVERRIDDEN = FALSE") PRINTNL()
//				
//				//Reset delete anything that shouldn't be there if no buddies are overridden
//				RESET_OR_DELETE_EVERYTHING_FOR_BATTLEBUDDY()
//				
//				//Set buddy to take cover at the cargobob if near it and player is on foot
//				IF missionStage = STAGE_STEAL_CARGOBOB
//					IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//						IF GET_DISTANCE_BETWEEN_ENTITIES(hBuddyMichael, HeliToSteal) < 20
//							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//								IF IS_BATTLEBUDDY_BEHAVIOUR_REQUESTED(BBF_CombatDefendCargobobArea)
//									IF buddyTrevorCombatFlagSet = FALSE
//										SET_BATTLEBUDDY_BEHAVIOUR_REQUEST(BBF_CombatDefendCargobobArea)
//										buddyTrevorCombatFlagSet = TRUE
//									ENDIF
//								ENDIF
//							ELSE
//								IF buddyTrevorCombatFlagSet = TRUE
//									CLEAR_BATTLEBUDDY_BEHAVIOUR_REQUEST(BBF_CombatDefendCargobobArea)
//									buddyTrevorCombatFlagSet = FALSE
//								ENDIF
//							ENDIF
//						ELSE
//							IF buddyTrevorCombatFlagSet = TRUE
//								CLEAR_BATTLEBUDDY_BEHAVIOUR_REQUEST(BBF_CombatDefendCargobobArea)
//								buddyTrevorCombatFlagSet = FALSE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				//Dont take control of battle buddy until switch cam has finished.
//				IF NOT IS_SELECTOR_CAM_ACTIVE()
//					
//					//HAVE CHECKS FOR CASES WHEREBY BATTLE BUDDY WILL NEED TO BE OVERRIDDEN HERE
//					
//					// Is buddy driving the players vehicle
//					IF IS_BUDDY_IN_DRIVER_SEAT(hBuddyTrevor)
////						PRINTSTRING("IS_BUDDY_IN_DRIVER_SEAT = TRUE") PRINTNL()
//						IF OVERRIDE_BATTLEBUDDY(hBuddyTrevor, FALSE)
////							PRINTSTRING("OVERRIDE_BATTLEBUDDY has been called") PRINTNL()
//							SET_ENTITY_AS_MISSION_ENTITY(hBuddyTrevor, TRUE, TRUE)
//							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyTrevor, TRUE)
//							CLEAR_PED_TASKS(hBuddyTrevor)
//							BuddyTrevorReadyToBeReleased = FALSE
//							BuddyTrevorRocketOveride = FALSE
//							BuddyTrevorDriverOveride = TRUE
//							BuddyTrevorReadyToDriveBy = FALSE
//							BuddyTrevorReadyToFireRockets = FALSE
//						ENDIF
//					ENDIF
//					
//					// Does script need to take control of buddy to fire rockets at enemychoppers
//					IF missionStage = STAGE_ESCAPE_MILITARY
//						IF EnemyHeliDown[0] = FALSE
//						OR EnemyHeliDown[1] = FALSE
//							IF IS_BUDDY_IN_BACK_SEAT(hBuddyTrevor)
//								IF OVERRIDE_BATTLEBUDDY(hBuddyTrevor, FALSE)
////									PRINTSTRING("OVERRIDE_BATTLEBUDDY has been called") PRINTNL()
//									SET_ENTITY_AS_MISSION_ENTITY(hBuddyTrevor, TRUE, TRUE)
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyTrevor, TRUE)
//									CLEAR_PED_TASKS(hBuddyTrevor)
//									IF VS_buddyTrevor_seat = VS_BACK_LEFT
//										i_player_rocket_event = 0
//										BuddyTrevorReadyToFireRockets = TRUE
//										BuddyTrevorRocketOveride = TRUE
//										BuddyTrevorReadyToDriveBy = FALSE
//									ENDIF
//									IF VS_buddyTrevor_seat = VS_BACK_RIGHT
//										BuddyTrevorReadyToDriveBy = TRUE
//										BuddyTrevorRocketOveride = FALSE
//										BuddyTrevorReadyToFireRockets = FALSE
//									ENDIF
//									BuddyTrevorReadyToBeReleased = FALSE
//									BuddyTrevorDriverOveride = FALSE
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF		
//					
//				ENDIF
//				
//			ELSE
//				
////				PRINTSTRING("IS_BATTLEBUDDY_OVERRIDDEN = TRUE") PRINTNL()
//				
//				//GENERAL RULES THAT SHOULD ALWAYS ALLOW BUDDY TO BE RELEASED
//				//If player is not in the buddy's vehicle after 5 seconds release battle buddy
//				IF BuddyTrevorReadyToBeReleased = FALSE
//					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						IF PlayerNotInAVehicleTimerStarted = FALSE
//							iPlayerNotInAnyVehicleTimer = GET_GAME_TIMER()
//							PlayerNotInAVehicleTimerStarted = TRUE
//						ELSE
//							IF GET_GAME_TIMER() > (iPlayerNotInAnyVehicleTimer + 5000)
////								PRINTSTRING("BuddyTrevorReadyToBeReleased 1 = TRUE") PRINTNL()
//								BuddyTrevorReadyToBeReleased = TRUE
//							ENDIF
//						ENDIF
//					ELSE
//						//Check to make sure the player is in the same vehicle as the buddy, if not release the battle buddy.
//						IF IS_PED_IN_ANY_VEHICLE(hBuddyTrevor)
//							IF IS_PED_IN_VEHICLE(hBuddyTrevor, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//								//Reset flag for if player gets back in the vehicle under 5 seconds
//								PlayerNotInAVehicleTimerStarted = FALSE
//							ELSE
//								BuddyTrevorReadyToBeReleased = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				//If player switches then release all battle buddies
//				IF BuddyTrevorReadyToBeReleased = FALSE
//					IF IS_SELECTOR_CAM_ACTIVE()
////						PRINTSTRING("BuddyTrevorReadyToBeReleased 2 = TRUE") PRINTNL()
//						BuddyTrevorReadyToBeReleased = TRUE
//					ENDIF
//				ENDIF
//				
//				//Handle everything if buddy is firing rockets from side of heli, with player driving
//				IF BuddyTrevorRocketOveride = TRUE
//					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddyTrevor)
//					AND BuddyTrevorReadyToBeReleased = FALSE
//						IF BuddyTrevorReadyToFireRockets = TRUE
//							HANDLE_BUDDY_FIRING_ROCKETS(hBuddyTrevor)
//						ELSE
////							PRINTSTRING("BuddyTrevorReadyToBeReleased 4 = TRUE") PRINTNL()
//							BuddyTrevorReadyToBeReleased = TRUE
//						ENDIF
//					ELSE
//						IF BuddyTrevorReadyToBeReleased = TRUE
//							IF DOES_ENTITY_EXIST(obj_lesters_rpg)
//								DELETE_OBJECT(obj_lesters_rpg)
//							ENDIF
//							IF DOES_ENTITY_EXIST(obj_single_rocket)
//								DELETE_OBJECT(obj_single_rocket)
//							ENDIF
//							CLEAR_PED_TASKS_IMMEDIATELY(hBuddyTrevor)
//							IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//								IF NOT IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//									SET_PED_INTO_VEHICLE(hBuddyTrevor, HeliToSteal, VS_buddyTrevor_seat)
//								ENDIF
//							ENDIF
//							IF RELEASE_BATTLEBUDDY(hBuddyTrevor)
//								BuddyTrevorRocketOveride = FALSE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF					
//				
//				//Handle everything if buddy is ready to drive by in the heli's back right seat.
//				IF BuddyTrevorReadyToDriveBy = TRUE
//					// Does script still need to take control of buddy to do drive by?
//					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddyTrevor)
//					AND BuddyTrevorReadyToBeReleased = FALSE	
//						IF  GET_SCRIPT_TASK_STATUS(hBuddyTrevor, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//						AND GET_SCRIPT_TASK_STATUS(hBuddyTrevor, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK	
//							IF DOES_ENTITY_EXIST(EnemyHeli[0])
//								IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
//									TASK_DRIVE_BY(hBuddyTrevor, NULL, EnemyHeli[0], <<0,0,0>>, 10000, 100)
//								ELSE
//									IF DOES_ENTITY_EXIST(EnemyHeli[1])
//										IF IS_VEHICLE_DRIVEABLE(EnemyHeli[1])
//											TASK_DRIVE_BY(hBuddyTrevor, NULL, EnemyHeli[1], <<0,0,0>>, 10000, 100)
//										ENDIF	
//									ENDIF
//								ENDIF
//							ELSE
//								IF DOES_ENTITY_EXIST(EnemyHeli[1])
//									IF IS_VEHICLE_DRIVEABLE(EnemyHeli[1])
//										TASK_DRIVE_BY(hBuddyTrevor, NULL, EnemyHeli[1], <<0,0,0>>, 10000, 100)
//									ENDIF	
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						IF RELEASE_BATTLEBUDDY(hBuddyTrevor)
//							BuddyTrevorReadyToDriveBy = FALSE
//						ENDIF	
//					ENDIF
//				ENDIF
//				
//				//Handle everything if buddy is driving heli with player firing rockets from the side.
//				IF BuddyTrevorDriverOveride = TRUE
//					
////					PRINTSTRING("BuddyTrevorDriverOveride = TRUE") PRINTNL()
//					
//					//Handle player firing rockets from side of heli here as buddy will be overridden to fly the heli at this point.
//					IF DOES_ENTITY_EXIST(HeliToSteal)
//						IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//								IF GET_PED_VEHICLE_SEAT(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = VS_BACK_LEFT
//									DO_PLAYER_ROCKET_FIRE_FROM_HELI_SIDE()
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				
//					// Does script still need to take control of buddy and drive to dest?
//					IF IS_BUDDY_IN_DRIVER_SEAT(hBuddyTrevor)
//					AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddyTrevor)
//					AND BuddyTrevorReadyToBeReleased = FALSE	
////						PRINTSTRING("IS_BUDDY_IN_DRIVER_SEAT = TRUE...AND..DOES_ENTITY_BELONG_TO_THIS_SCRIPT = TRUE") PRINTNL()
//						
//						// Give buddy task to fly heli to hanger
//						IF BuddyTrevorDrivingHeli = TRUE
//							vbuddyCoords = GET_ENTITY_COORDS(hBuddyTrevor)
//							IF  GET_SCRIPT_TASK_STATUS(hBuddyTrevor, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//							AND GET_SCRIPT_TASK_STATUS(hBuddyTrevor, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
//								IF IS_VEHICLE_DRIVEABLE(HeliToSteal)	
//									OPEN_SEQUENCE_TASK(seq)
//										IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//											TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vbuddyCoords.x,vbuddyCoords.y, (vbuddyCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//										ENDIF
//										TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 20, -1, 240, 50)
//										TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 5, 1, -1, 0, 0)
//									CLOSE_SEQUENCE_TASK(seq)
//									TASK_PERFORM_SEQUENCE(hBuddyTrevor, seq)
//									CLEAR_SEQUENCE_TASK(seq)	
//								ENDIF
//							ENDIF
//						ENDIF
//							
//							
//						//Give buddy task to drive the car to the heli 	
//						IF BuddyTrevorDrivingAnyVehicle = TRUE
//							vDriveToHeliCoords = GET_ENTITY_COORDS(heliToSteal)
//							vbuddyCoords = GET_ENTITY_COORDS(hBuddyTrevor)
//							REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vbuddyCoords.x, vbuddyCoords.y, vDriveToHeliCoords.x, vDriveToHeliCoords.y)
////							PRINTSTRING("buddyDrivingAnyVehicle = TRUE") PRINTNL()
//							IF  GET_SCRIPT_TASK_STATUS(hBuddyTrevor, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//							AND GET_SCRIPT_TASK_STATUS(hBuddyTrevor, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
//								IF ARE_NODES_LOADED_FOR_AREA(vbuddyCoords.x, vbuddyCoords.y, vDriveToHeliCoords.x, vDriveToHeliCoords.y)
//									TASK_VEHICLE_MISSION_COORS_TARGET(hBuddyTrevor, GET_VEHICLE_PED_IS_IN(hBuddyTrevor), vDriveToHeliCoords, MISSION_GOTO, 40.0, DRIVINGMODE_PLOUGHTHROUGH|DRIVINGMODE_AVOIDCARS_RECKLESS, 10.0, 10.0)
//								ENDIF
//							ENDIF
//						ELSE
////							PRINTSTRING("buddyDrivingAnyVehicle = FALSE") PRINTNL()
//						ENDIF
//						
//						//Run a check for player being driven to the heli by a buddy. Dont allow release of buddy till player is out of the car.
//						//Bring the car to a halt once close enough to the helicopter.
////							IF IS_ENTITY_IN_ANGLED_AREA(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), <<-2115.419434,3038.640137,30.809973>>, <<-2144.749512,3055.938232,36.309971>>, 16.750000)
////							PRINTSTRING("Players vehicle is in the area where it should be brought to a halt") PRINTNL()
//						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(heliToSteal), GET_ENTITY_COORDS(hBuddyTrevor)) < 15
//							IF NOT IS_VEHICLE_STOPPED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
////								PRINTSTRING("Vehicle is not stopped") PRINTNL()
//								IF IS_PED_IN_VEHICLE(hBuddyTrevor, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//									IF NOT IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
////										PRINTSTRING("BRING_VEHICLE_TO_HALT is being called") PRINTNL()
//										BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 7, 10)
//									ENDIF
//								ENDIF
//							ELSE
//								TaskToStopVehicle = TRUE
//							ENDIF
//						ELSE
////							PRINTSTRING("Players vehicle is NOT in the area where it should be brought to a halt") PRINTNL()
//						ENDIF
//						
//						IF TaskToStopVehicle = TRUE
//							IF IS_PED_IN_VEHICLE(hBuddyTrevor, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//								IF IS_VEHICLE_STOPPED(GET_VEHICLE_PED_IS_IN(hBuddyTrevor))
//									CLEAR_PED_TASKS(hBuddyTrevor)
//									REMOVE_PED_FROM_GROUP(hBuddyTrevor)
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyTrevor, TRUE)
//									SET_PED_SPHERE_DEFENSIVE_AREA(hBuddyTrevor, GET_ENTITY_COORDS(hBuddyTrevor), 15)
//									OPEN_SEQUENCE_TASK(seq)
//										TASK_LEAVE_ANY_VEHICLE(NULL)
//										TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 50)
//									CLOSE_SEQUENCE_TASK(seq)
//									TASK_PERFORM_SEQUENCE(hBuddyTrevor, seq)
//									CLEAR_SEQUENCE_TASK(seq)
//									SET_PED_COMBAT_ATTRIBUTES(hBuddyTrevor, CA_USE_VEHICLE, FALSE) 
//									SET_PED_COMBAT_ATTRIBUTES(hBuddyTrevor, CA_LEAVE_VEHICLES, TRUE)
////									PRINTSTRING("Sequence task given to buddy") PRINTNL()
//									TaskToStopVehicle = FALSE
//								ENDIF
//							ENDIF					
//						ENDIF	
//						
//					ELSE
//						IF BuddyTrevorReadyToBeReleased = TRUE
////							PRINTSTRING("BuddyReadyToBeReleased = TRUE so RELEASE_BATTLEBUDY BEING CALLED") PRINTNL()
//							RELEASE_BATTLEBUDDY(hBuddyTrevor)
//							BuddyTrevorDriverOveride = FALSE
//						ELSE
////							PRINTSTRING("BuddyReadyToBeReleased = FALSE") PRINTNL()
//						ENDIF
//					ENDIF
//				ENDIF
//				
////					//Some debug testing
////					IF IS_BUDDY_IN_DRIVER_SEAT(hBuddy)
////						PRINTSTRING("Buddy is in the driver seat") PRINTNL()
////					ELSE
////						PRINTSTRING("Buddy is NOT in the driver seat") PRINTNL()
////					ENDIF
////					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddy)
////						PRINTSTRING("Buddy belongs to this script") PRINTNL()
////					ELSE
////						PRINTSTRING("Buddy does not belong to this script") PRINTNL()
////					ENDIF
//				
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	//Handle Battle Buddy Franklin if he's available
//	IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//		IF NOT IS_PED_INJURED(hBuddyFranklin)
//			IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddyFranklin)
//				
////				PRINTSTRING("IS_BATTLEBUDDY_OVERRIDDEN = FALSE") PRINTNL()
//				
//				//Reset delete anything that shouldn't be there if no buddies are overridden
//				RESET_OR_DELETE_EVERYTHING_FOR_BATTLEBUDDY()
//				
//				//Set buddy to take cover at the cargobob if near it and player is on foot
//				IF missionStage = STAGE_STEAL_CARGOBOB
//					IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//						IF GET_DISTANCE_BETWEEN_ENTITIES(hBuddyMichael, HeliToSteal) < 20
//							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//								IF IS_BATTLEBUDDY_BEHAVIOUR_REQUESTED(BBF_CombatDefendCargobobArea)
//									IF buddyFranklinCombatFlagSet = FALSE
//										SET_BATTLEBUDDY_BEHAVIOUR_REQUEST(BBF_CombatDefendCargobobArea)
//										buddyFranklinCombatFlagSet = TRUE
//									ENDIF
//								ENDIF
//							ELSE
//								IF buddyFranklinCombatFlagSet = TRUE
//									CLEAR_BATTLEBUDDY_BEHAVIOUR_REQUEST(BBF_CombatDefendCargobobArea)
//									buddyFranklinCombatFlagSet = FALSE
//								ENDIF
//							ENDIF
//						ELSE
//							IF buddyFranklinCombatFlagSet = TRUE
//								CLEAR_BATTLEBUDDY_BEHAVIOUR_REQUEST(BBF_CombatDefendCargobobArea)
//								buddyFranklinCombatFlagSet = FALSE
//							ENDIF
//						ENDIF
//					ENDIF	
//				ENDIF
//				
//				//Dont take control of battle buddy until switch cam has finished.
//				IF NOT IS_SELECTOR_CAM_ACTIVE()
//					
//					//HAVE CHECKS FOR CASES WHEREBY BATTLE BUDDY WILL NEED TO BE OVERRIDDEN HERE
//					
//					// Is buddy driving the players vehicle
//					IF IS_BUDDY_IN_DRIVER_SEAT(hBuddyFranklin)
////						PRINTSTRING("IS_BUDDY_IN_DRIVER_SEAT = TRUE") PRINTNL()
//						IF OVERRIDE_BATTLEBUDDY(hBuddyFranklin, FALSE)
////							PRINTSTRING("OVERRIDE_BATTLEBUDDY has been called") PRINTNL()
//							SET_ENTITY_AS_MISSION_ENTITY(hBuddyFranklin, TRUE, TRUE)
//							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyFranklin, TRUE)
//							CLEAR_PED_TASKS(hBuddyFranklin)
//							BuddyFranklinReadyToBeReleased = FALSE
//							BuddyFranklinRocketOveride = FALSE
//							BuddyFranklinDriverOveride = TRUE
//							BuddyFranklinReadyToDriveBy = FALSE
//							BuddyFranklinReadyToFireRockets = FALSE
//						ENDIF
//					ENDIF
//					
//					// Does script need to take control of buddy to fire rockets at enemychoppers
//					IF missionStage = STAGE_ESCAPE_MILITARY
//						IF EnemyHeliDown[0] = FALSE
//						OR EnemyHeliDown[1] = FALSE
//							IF IS_BUDDY_IN_BACK_SEAT(hBuddyFranklin)
//								IF OVERRIDE_BATTLEBUDDY(hBuddyFranklin, FALSE)
////									PRINTSTRING("OVERRIDE_BATTLEBUDDY has been called") PRINTNL()
//									SET_ENTITY_AS_MISSION_ENTITY(hBuddyFranklin, TRUE, TRUE)
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyFranklin, TRUE)
//									CLEAR_PED_TASKS(hBuddyFranklin)
//									IF VS_buddyFranklin_seat = VS_BACK_LEFT
//										i_player_rocket_event = 0
//										BuddyFranklinReadyToFireRockets = TRUE
//										BuddyFranklinRocketOveride = TRUE
//										BuddyFranklinReadyToDriveBy = FALSE
//									ENDIF
//									IF VS_buddyFranklin_seat = VS_BACK_RIGHT
//										BuddyFranklinReadyToDriveBy = TRUE
//										BuddyFranklinRocketOveride = FALSE
//										BuddyFranklinReadyToFireRockets = FALSE
//									ENDIF
//									BuddyFranklinReadyToBeReleased = FALSE
//									BuddyFranklinDriverOveride = FALSE
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF		
//					
//				ENDIF
//				
//			ELSE
//				
////				PRINTSTRING("IS_BATTLEBUDDY_OVERRIDDEN = TRUE") PRINTNL()
//				
//				//GENERAL RULES THAT SHOULD ALWAYS ALLOW BUDDY TO BE RELEASED
//				//If player is not in the buddy's vehicle after 5 seconds release battle buddy
//				IF BuddyFranklinReadyToBeReleased = FALSE
//					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						IF PlayerNotInAVehicleTimerStarted = FALSE
//							iPlayerNotInAnyVehicleTimer = GET_GAME_TIMER()
//							PlayerNotInAVehicleTimerStarted = TRUE
//						ELSE
//							IF GET_GAME_TIMER() > (iPlayerNotInAnyVehicleTimer + 5000)
////								PRINTSTRING("BuddyFranklinReadyToBeReleased 1 = TRUE") PRINTNL()
//								BuddyFranklinReadyToBeReleased = TRUE
//							ENDIF
//						ENDIF
//					ELSE
//						//Check to make sure the player is in the same vehicle as the buddy, if not release the battle buddy.
//						IF IS_PED_IN_ANY_VEHICLE(hBuddyFranklin)
//							IF IS_PED_IN_VEHICLE(hBuddyFranklin, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//								//Reset flag for if player gets back in the vehicle under 5 seconds
//								PlayerNotInAVehicleTimerStarted = FALSE
//							ELSE
//								BuddyFranklinReadyToBeReleased = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				//If player switches then release all battle buddies
//				IF BuddyFranklinReadyToBeReleased = FALSE
//					IF IS_SELECTOR_CAM_ACTIVE()
////						PRINTSTRING("BuddyFranklinReadyToBeReleased 2 = TRUE") PRINTNL()
//						BuddyFranklinReadyToBeReleased = TRUE
//					ENDIF
//				ENDIF
//				
//				//Handle everything if buddy is firing rockets from side of heli, with player driving
//				IF BuddyFranklinRocketOveride = TRUE
//					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddyFranklin)
//					AND BuddyFranklinReadyToBeReleased = FALSE
//						IF BuddyFranklinReadyToFireRockets = TRUE
//							HANDLE_BUDDY_FIRING_ROCKETS(hBuddyFranklin)
//						ELSE
////							PRINTSTRING("BuddyFranklinReadyToBeReleased 4 = TRUE") PRINTNL()
//							BuddyFranklinReadyToBeReleased = TRUE
//						ENDIF
//					ELSE
//						IF BuddyFranklinReadyToBeReleased = TRUE
//							IF DOES_ENTITY_EXIST(obj_lesters_rpg)
//								DELETE_OBJECT(obj_lesters_rpg)
//							ENDIF
//							IF DOES_ENTITY_EXIST(obj_single_rocket)
//								DELETE_OBJECT(obj_single_rocket)
//							ENDIF
//							CLEAR_PED_TASKS_IMMEDIATELY(hBuddyFranklin)
//							IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//								IF NOT IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//									SET_PED_INTO_VEHICLE(hBuddyFranklin, HeliToSteal, VS_buddyFranklin_seat)
//								ENDIF
//							ENDIF
//							IF RELEASE_BATTLEBUDDY(hBuddyFranklin)
//								BuddyFranklinRocketOveride = FALSE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF					
//				
//				//Handle everything if buddy is ready to drive by in the heli's back right seat.
//				IF BuddyFranklinReadyToDriveBy = TRUE
//					// Does script still need to take control of buddy to do drive by?
//					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddyFranklin)
//					AND BuddyFranklinReadyToBeReleased = FALSE	
//						IF  GET_SCRIPT_TASK_STATUS(hBuddyFranklin, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//						AND GET_SCRIPT_TASK_STATUS(hBuddyFranklin, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK	
//							IF DOES_ENTITY_EXIST(EnemyHeli[0])
//								IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
//									TASK_DRIVE_BY(hBuddyFranklin, NULL, EnemyHeli[0], <<0,0,0>>, 10000, 100)
//								ELSE
//									IF DOES_ENTITY_EXIST(EnemyHeli[1])
//										IF IS_VEHICLE_DRIVEABLE(EnemyHeli[1])
//											TASK_DRIVE_BY(hBuddyFranklin, NULL, EnemyHeli[1], <<0,0,0>>, 10000, 100)
//										ENDIF	
//									ENDIF
//								ENDIF
//							ELSE
//								IF DOES_ENTITY_EXIST(EnemyHeli[1])
//									IF IS_VEHICLE_DRIVEABLE(EnemyHeli[1])
//										TASK_DRIVE_BY(hBuddyFranklin, NULL, EnemyHeli[1], <<0,0,0>>, 10000, 100)
//									ENDIF	
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						IF RELEASE_BATTLEBUDDY(hBuddyFranklin)
//							BuddyFranklinReadyToDriveBy = FALSE
//						ENDIF	
//					ENDIF
//				ENDIF
//				
//				//Handle everything if buddy is driving heli with player firing rockets from the side.
//				IF BuddyFranklinDriverOveride = TRUE
//					
//////					PRINTSTRING("BuddyFranklinDriverOveride = TRUE") PRINTNL()
//					
//					//Handle player firing rockets from side of heli here as buddy will be overridden to fly the heli at this point.
//					IF DOES_ENTITY_EXIST(HeliToSteal)
//						IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//								IF GET_PED_VEHICLE_SEAT(PLAYER_PED_ID(), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = VS_BACK_LEFT
//									DO_PLAYER_ROCKET_FIRE_FROM_HELI_SIDE()
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				
//					// Does script still need to take control of buddy and drive to dest?
//					IF IS_BUDDY_IN_DRIVER_SEAT(hBuddyFranklin)
//					AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddyFranklin)
//					AND BuddyFranklinReadyToBeReleased = FALSE	
////						PRINTSTRING("IS_BUDDY_IN_DRIVER_SEAT = TRUE...AND..DOES_ENTITY_BELONG_TO_THIS_SCRIPT = TRUE") PRINTNL()
//						
//						// Give buddy task to fly heli to hanger
//						IF BuddyFranklinDrivingHeli = TRUE
//							vbuddyCoords = GET_ENTITY_COORDS(hBuddyFranklin)
//							IF  GET_SCRIPT_TASK_STATUS(hBuddyFranklin, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//							AND GET_SCRIPT_TASK_STATUS(hBuddyFranklin, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
//								IF IS_VEHICLE_DRIVEABLE(HeliToSteal)	
//									OPEN_SEQUENCE_TASK(seq)
//										IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//											TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vbuddyCoords.x,vbuddyCoords.y, (vbuddyCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//										ENDIF
//										TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 20, -1, 240, 50)
//										TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 5, 1, -1, 0, 0)
//									CLOSE_SEQUENCE_TASK(seq)
//									TASK_PERFORM_SEQUENCE(hBuddyFranklin, seq)
//									CLEAR_SEQUENCE_TASK(seq)	
//								ENDIF
//							ENDIF
//						ENDIF
//							
//							
//						//Give buddy task to drive the car to the heli 	
//						IF BuddyFranklinDrivingAnyVehicle = TRUE
//							vDriveToHeliCoords = GET_ENTITY_COORDS(heliToSteal)
//							vbuddyCoords = GET_ENTITY_COORDS(hBuddyFranklin)
//							REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(vbuddyCoords.x, vbuddyCoords.y, vDriveToHeliCoords.x, vDriveToHeliCoords.y)
////							PRINTSTRING("buddyDrivingAnyVehicle = TRUE") PRINTNL()
//							IF  GET_SCRIPT_TASK_STATUS(hBuddyFranklin, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
//							AND GET_SCRIPT_TASK_STATUS(hBuddyFranklin, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
//								IF ARE_NODES_LOADED_FOR_AREA(vbuddyCoords.x, vbuddyCoords.y, vDriveToHeliCoords.x, vDriveToHeliCoords.y)
//									TASK_VEHICLE_MISSION_COORS_TARGET(hBuddyFranklin, GET_VEHICLE_PED_IS_IN(hBuddyFranklin), vDriveToHeliCoords, MISSION_GOTO, 40.0, DRIVINGMODE_PLOUGHTHROUGH|DRIVINGMODE_AVOIDCARS_RECKLESS, 10.0, 10.0)
//								ENDIF
//							ENDIF
//						ELSE
////							PRINTSTRING("buddyDrivingAnyVehicle = FALSE") PRINTNL()
//						ENDIF
//						
//						//Run a check for player being driven to the heli by a buddy. Dont allow release of buddy till player is out of the car.
//						//Bring the car to a halt once close enough to the helicopter.
////							IF IS_ENTITY_IN_ANGLED_AREA(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), <<-2115.419434,3038.640137,30.809973>>, <<-2144.749512,3055.938232,36.309971>>, 16.750000)
////							PRINTSTRING("Players vehicle is in the area where it should be brought to a halt") PRINTNL()
//						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(heliToSteal), GET_ENTITY_COORDS(hBuddyFranklin)) < 15
//							IF NOT IS_VEHICLE_STOPPED(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
////								PRINTSTRING("Vehicle is not stopped") PRINTNL()
//								IF IS_PED_IN_VEHICLE(hBuddyFranklin, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//									IF NOT IS_PLAYER_IN_VEH_SEAT(PLAYER_ID(), VS_DRIVER)
////										PRINTSTRING("BRING_VEHICLE_TO_HALT is being called") PRINTNL()
//										BRING_VEHICLE_TO_HALT(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 7, 10)
//									ENDIF
//								ENDIF
//							ELSE
//								TaskToStopVehicle = TRUE
//							ENDIF
//						ELSE
////							PRINTSTRING("Players vehicle is NOT in the area where it should be brought to a halt") PRINTNL()
//						ENDIF
//						
//						IF TaskToStopVehicle = TRUE
//							IF IS_PED_IN_VEHICLE(hBuddyFranklin, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
//								IF IS_VEHICLE_STOPPED(GET_VEHICLE_PED_IS_IN(hBuddyFranklin))
//									CLEAR_PED_TASKS(hBuddyFranklin)
//									REMOVE_PED_FROM_GROUP(hBuddyFranklin)
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyFranklin, TRUE)
//									SET_PED_SPHERE_DEFENSIVE_AREA(hBuddyFranklin, GET_ENTITY_COORDS(hBuddyFranklin), 15)
//									OPEN_SEQUENCE_TASK(seq)
//										TASK_LEAVE_ANY_VEHICLE(NULL)
//										TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 50)
//									CLOSE_SEQUENCE_TASK(seq)
//									TASK_PERFORM_SEQUENCE(hBuddyFranklin, seq)
//									CLEAR_SEQUENCE_TASK(seq)
//									SET_PED_COMBAT_ATTRIBUTES(hBuddyFranklin, CA_USE_VEHICLE, FALSE) 
//									SET_PED_COMBAT_ATTRIBUTES(hBuddyFranklin, CA_LEAVE_VEHICLES, TRUE)
////									PRINTSTRING("Sequence task given to buddy") PRINTNL()
//									TaskToStopVehicle = FALSE
//								ENDIF
//							ENDIF					
//						ENDIF	
//						
//					ELSE
//						IF BuddyFranklinReadyToBeReleased = TRUE
////							PRINTSTRING("BuddyReadyToBeReleased = TRUE so RELEASE_BATTLEBUDY BEING CALLED") PRINTNL()
//							RELEASE_BATTLEBUDDY(hBuddyFranklin)
//							BuddyFranklinDriverOveride = FALSE
//						ELSE
////							PRINTSTRING("BuddyReadyToBeReleased = FALSE") PRINTNL()
//						ENDIF
//					ENDIF
//				ENDIF
//				
////					//Some debug testing
////					IF IS_BUDDY_IN_DRIVER_SEAT(hBuddy)
////						PRINTSTRING("Buddy is in the driver seat") PRINTNL()
////					ELSE
////						PRINTSTRING("Buddy is NOT in the driver seat") PRINTNL()
////					ENDIF
////					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddy)
////						PRINTSTRING("Buddy belongs to this script") PRINTNL()
////					ELSE
////						PRINTSTRING("Buddy does not belong to this script") PRINTNL()
////					ENDIF
//				
//			ENDIF
//		ENDIF
//	ENDIF	
//	
//ENDPROC

//PURPOSE: Controls any scenarios that need switched on/off throughout the mission
PROC SCENARIO_CONTROLLER()

	//Switch off ARMY_HELI group to stop other cargobobs spawning around the military base
	IF DOES_SCENARIO_GROUP_EXIST("ARMY_HELI") 
		IF IS_SCENARIO_GROUP_ENABLED("ARMY_HELI") 
			SET_SCENARIO_GROUP_ENABLED("ARMY_HELI",FALSE)
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Checks if player is aiming at any of the peds.
FUNC BOOL IS_PLAYER_AIMING_AT_PED(PED_INDEX ePed, INT iWeaponFlags)

	IF DOES_ENTITY_EXIST(ePed)
	AND NOT IS_PED_INJURED(ePed)
		
		IF IS_PED_ARMED(PLAYER_PED_ID(), iWeaponFlags)
			IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), ePed)
			OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), ePed)
		
				RETURN TRUE
			
			ENDIF
		ENDIF
		
	ENDIF

	RETURN FALSE
ENDFUNC

//PURPOSE: Adds the players to dialogue by checking who the player is in control of.
FUNC BOOL ARE_ARMY_PEDS_ADDED_FOR_DIALOGUE()

	FOR icount = 0 TO 9
		IF DOES_ENTITY_EXIST(MarineCop[icount])
			IF NOT IS_PED_INJURED(MarineCop[icount])
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[icount]) < 25
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, MarineCop[icount], "ArmyPed")
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns True when an army ped is less that 25m's from player and is alive
FUNC BOOL GET_NEAREST_ARMY_PED_READY_FOR_CHAT()

	FOR iArmyPedCount = 0 TO 9
		IF DOES_ENTITY_EXIST(MarineCop[iArmyPedCount])
			IF NOT IS_PED_INJURED(MarineCop[iArmyPedCount])		
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[iArmyPedCount]) < 25
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

//Purpose: Returns the closest Army ped
FUNC PED_INDEX GET_NEAREST_ARMY_PED()
	
	PED_INDEX ClosestPed
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		GET_CLOSEST_PED(GET_ENTITY_COORDS(PLAYER_PED_ID()), 30, FALSE, TRUE, ClosestPed, FALSE, TRUE)
	ENDIF
	
	FOR iArmyPedCount2 = 0 TO 9
		IF DOES_ENTITY_EXIST(MarineCop[iArmyPedCount2])
			IF NOT IS_PED_INJURED(MarineCop[iArmyPedCount2])		
				IF ClosestPed = MarineCop[iArmyPedCount2]
					RETURN MarineCop[iArmyPedCount2]
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN NULL

ENDFUNC

//PURPOSE: Handles all mission dialogue
PROC MISSION_DIALOGUE_CONTROL()
			
//	//Do alarm tannoy dialogue once the alarm has been started
//	IF AlarmStarted = TRUE
//	AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-2144, 3021, 34>>) < 600
//		IF GET_GAME_TIMER() > (iAlarmChatTimer + 10000)
//			IF NOT IS_MESSAGE_BEING_DISPLAYED()
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 8, NULL, "ARMY_ANNOUNCER")
//					IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_ANN", CONV_PRIORITY_MEDIUM, DO_NOT_DISPLAY_SUBTITLES)
//						//This is a code red! I repeat, this is a code red.
//						//All units, we have a confirmed security breach.
//						//Security has been compromised, eliminate the target immediately.
//						//Attention, we have an intruder on the base.
//						//All units authorized to use lethal force!
//						//All units mobilize immediately!
//						//Any units still not mobilized, engage immediately.  
//						//We are under attack, I repeat, we are under attack.
//						iAlarmChatTimer = GET_GAME_TIMER()
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
	
	//Have a control for has the player fired his weapon or aimed at anyone and has been seen or heard doing so
	IF playerSpottedFiringAiming = FALSE
		FOR iFireCount = 0 TO 12
			IF DOES_ENTITY_EXIST(MarineCop[iFireCount])
				IF NOT IS_PED_INJURED(MarineCop[iFireCount])
					IF CAN_PED_SEE_HATED_PED(MarineCop[iFireCount], PLAYER_PED_ID())
						IF GET_DISTANCE_BETWEEN_ENTITIES(MarineCop[iFireCount], PLAYER_PED_ID()) < 40
							IF IS_PED_SHOOTING(PLAYER_PED_ID())
								playerSpottedFiringAiming = TRUE
							ENDIF
						ENDIF
						IF GET_DISTANCE_BETWEEN_ENTITIES(MarineCop[iFireCount], PLAYER_PED_ID()) < 25
							IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
								IF IS_PLAYER_AIMING_AT_PED(MarineCop[iFireCount], WF_INCLUDE_GUN | WF_INCLUDE_PROJECTILE)
									playerSpottedFiringAiming = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
	
	//Add Trevor for dialogue
	IF TrevorAddedForChat = FALSE
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, PLAYER_PED_ID(), "TREVOR")	
			TrevorAddedForChat = TRUE
		ENDIF
	ENDIF
	
	//have some dialogue from the guards at front gates if they spot the player
	IF playerIsInBase = FALSE
		FOR iGuardCount = 0 To 1
			
			IF DOES_ENTITY_EXIST(MarineCop[iGuardCount])
				IF NOT IS_PED_INJURED(MarineCop[iGuardCount])
				
					//Do opening chat at front gates. Dependant on if player has weapon equipped or not
					//With weapon
					IF doneChat55 = FALSE
					AND doneChat56 = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[iGuardCount]) < 25
									IF CAN_PED_SEE_HATED_PED(MarineCop[iGuardCount], PLAYER_PED_ID())	
										IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										AND NOT IS_PHONE_ONSCREEN()
											IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED	
												IF DOES_ENTITY_EXIST(MarineCop[0])
													IF NOT IS_PED_INJURED(MarineCop[0])
														IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[0]) < 30
															ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, MarineCop[0], "ARMY_Guard01")
														ENDIF
													ENDIF
												ENDIF
												IF DOES_ENTITY_EXIST(MarineCop[1])
													IF NOT IS_PED_INJURED(MarineCop[1])
														IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[1]) < 30
															ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, MarineCop[1], "ARMY_Guard01")
														ENDIF
													ENDIF
												ENDIF
												IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMYAUD", "ARMY_WRN1", CONV_PRIORITY_MEDIUM)
													//This is a restricted area, please leave immediately!
													iChat57Timer = GET_GAME_TIMER()
													iChat58Timer = GET_GAME_TIMER()
													doneChat55 = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					//Without weapon
					IF doneChat56 = FALSE
					AND doneChat55 = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[iGuardCount]) < 25
									IF CAN_PED_SEE_HATED_PED(MarineCop[iGuardCount], PLAYER_PED_ID())	
										IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
											IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_UNARMED	
											OR IS_PHONE_ONSCREEN()
												IF DOES_ENTITY_EXIST(MarineCop[0])
													IF NOT IS_PED_INJURED(MarineCop[0])
														IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[0]) < 30
															ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, MarineCop[0], "ARMY_Guard01")
														ENDIF
													ENDIF
												ENDIF
												IF DOES_ENTITY_EXIST(MarineCop[1])
													IF NOT IS_PED_INJURED(MarineCop[1])
														IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[1]) < 30
															ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, MarineCop[1], "ARMY_Guard01")
														ENDIF
													ENDIF
												ENDIF
												IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMYAUD", "ARMY_WRN1", CONV_PRIORITY_MEDIUM)
													//This is a restricted area, please leave immediately!
													iChat57Timer = GET_GAME_TIMER()
													iChat58Timer = GET_GAME_TIMER()
													doneChat56 = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF				
					
					//Do some random dialogue now until the player either aims, shoots or enters the base
					IF doneChat55 = TRUE
					OR doneChat56 = TRUE
						IF playerSpottedFiringAiming = FALSE
						AND playerIsInBase = FALSE
							IF doneArmyWrn1aChat = FALSE
								IF GET_GAME_TIMER() > (iChat58Timer + 8000)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
											IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[iGuardCount]) < 25	
												IF CAN_PED_SEE_HATED_PED(MarineCop[iGuardCount], PLAYER_PED_ID())	
													IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
													AND NOT IS_PHONE_ONSCREEN()
														IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED	
															IF DOES_ENTITY_EXIST(MarineCop[0])
																IF NOT IS_PED_INJURED(MarineCop[0])
																	IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[0]) < 30
																		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, MarineCop[0], "ARMY_Guard01")
																	ENDIF
																ENDIF
															ENDIF
															IF DOES_ENTITY_EXIST(MarineCop[1])
																IF NOT IS_PED_INJURED(MarineCop[1])
																	IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[1]) < 30
																		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, MarineCop[1], "ARMY_Guard01")
																	ENDIF
																ENDIF
															ENDIF
															IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMYAUD", "ARMY_WRN1a", CONV_PRIORITY_MEDIUM)
																//I'm not going to say this again. Leave now!
																iChat58Timer = GET_GAME_TIMER()
																doneArmyWrn1aChat = TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							IF GET_GAME_TIMER() > (iChat57Timer + 8000)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_MESSAGE_BEING_DISPLAYED()
									OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[iGuardCount]) < 25	
											IF CAN_PED_SEE_HATED_PED(MarineCop[iGuardCount], PLAYER_PED_ID())	
												IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
													IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) = WEAPONTYPE_UNARMED	
													OR IS_PHONE_ONSCREEN()
														IF DOES_ENTITY_EXIST(MarineCop[0])
															IF NOT IS_PED_INJURED(MarineCop[0])
																IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[0]) < 30
																	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, MarineCop[0], "ARMY_Guard01")
																ENDIF
															ENDIF
														ENDIF
														IF DOES_ENTITY_EXIST(MarineCop[1])
															IF NOT IS_PED_INJURED(MarineCop[1])
																IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[1]) < 30
																	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, MarineCop[1], "ARMY_Guard01")
																ENDIF
															ENDIF
														ENDIF
														IF doneArmyWrn1aChat = FALSE
															IF CREATE_CONVERSATION(MyLocalPedStruct, "ARMYAUD", "ARMY_WRN1a", CONV_PRIORITY_MEDIUM)
																//I'm not going to say this again. Leave now!
																iChat57Timer = GET_GAME_TIMER()
																doneArmyWrn1aChat = TRUE
															ENDIF
														ENDIF
														IF hostileTimerSet = FALSE
															iBecomeHostileTimer = GET_GAME_TIMER()
															hostileTimerSet = TRUE
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDFOR
	ENDIF
	
	//Dialogue from Military if player has entered the base or if he's aimed at a guard or has been seen firing his weapon 
	IF GET_GAME_TIMER() > (iArmyChatTimer + 12000)
		IF playerIsInBase = TRUE
		OR playerSpottedFiringAiming = TRUE
			IF NOT IS_MESSAGE_BEING_DISPLAYED()
			OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					IF ARE_ARMY_PEDS_ADDED_FOR_DIALOGUE()
//						IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT4", CONV_PRIORITY_AMBIENT_MEDIUM)	
//							//Die motherfucker.
//							//You wont get out of here alive.
//							//Take this guy down.
//							//Shoot to kill.
//							//Get this guy.
//							//Don't let him escape.
//							iArmyChatTimer = GET_GAME_TIMER()
//						ENDIF
//					ENDIF
					IF GET_NEAREST_ARMY_PED_READY_FOR_CHAT()
						iRandomInt2 = GET_RANDOM_INT_IN_RANGE(0, 6)
						IF iRandomInt2 = 0
							PLAY_PED_AMBIENT_SPEECH(GET_NEAREST_ARMY_PED(), "OVER_THERE", SPEECH_PARAMS_FORCE_SHOUTED)
							iArmyChatTimer = GET_GAME_TIMER()
						ELIF iRandomInt2 = 1
							PLAY_PED_AMBIENT_SPEECH(GET_NEAREST_ARMY_PED(), "PROVOKE_TRESSPASS", SPEECH_PARAMS_FORCE_SHOUTED)
							iArmyChatTimer = GET_GAME_TIMER()
						ELIF iRandomInt2 = 2
							PLAY_PED_AMBIENT_SPEECH(GET_NEAREST_ARMY_PED(), "UP_THERE", SPEECH_PARAMS_FORCE_SHOUTED)
							iArmyChatTimer = GET_GAME_TIMER()
						ELIF iRandomInt2 = 3
							PLAY_PED_AMBIENT_SPEECH(GET_NEAREST_ARMY_PED(), "suspect_spotted", SPEECH_PARAMS_FORCE_SHOUTED)
							iArmyChatTimer = GET_GAME_TIMER()
						ELIF iRandomInt2 = 4
							PLAY_PED_AMBIENT_SPEECH(GET_NEAREST_ARMY_PED(), "GENERIC_INSULT_HIGH", SPEECH_PARAMS_FORCE_SHOUTED)
							iArmyChatTimer = GET_GAME_TIMER()
						ELIF iRandomInt2 = 5
							PLAY_PED_AMBIENT_SPEECH(GET_NEAREST_ARMY_PED(), "GENERIC_INSULT_MED", SPEECH_PARAMS_FORCE_SHOUTED)
							iArmyChatTimer = GET_GAME_TIMER()
						ENDIF 
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
//	//Reset flags and re-add peds for chat if player switches.
//	IF IS_SELECTOR_CAM_ACTIVE()
//		MichaelAddedForChat = FALSE
//		FranklinAddedForChat = FALSE
//		TrevorAddedForChat = FALSE
//	ENDIF
//
//	//Add the players to the dialogue system if they are available.
//	IF MichaelAddedForChat = FALSE
//		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")	
//			MichaelAddedForChat = TRUE
//		ELSE
//			IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//				IF NOT IS_PED_INJURED(hBuddyMichael)
//					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, hBuddyMichael, "MICHAEL")
//					MichaelAddedForChat = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	IF FranklinAddedForChat = FALSE
//		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")	
//			FranklinAddedForChat = TRUE
//		ELSE
//			IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//				IF NOT IS_PED_INJURED(hBuddyFranklin)
//					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, hBuddyFranklin, "FRANKLIN")
//					FranklinAddedForChat = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF

//	SWITCH missionStage
//	
//		CASE STAGE_STEAL_CARGOBOB
//			
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				IF NOT IS_MESSAGE_BEING_DISPLAYED()
//				
//					//If Michael and Trevor exists but no Franklin 
//					IF TrevorAddedForChat = TRUE
//					AND MichaelAddedForChat = TRUE
//					AND NOT IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, TRUE)
//						IF doneChat[1] = FALSE
//							IF playerIsInBase = FALSE
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyMichael) < 15
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT1", CONV_PRIORITY_MEDIUM)
//											//Trevor, my man, I need your help to break into the military base.
//											//Now we're talking. What you up to?
//											//Just need to borrow one of those big army choppers. 
//											//Sounds like my kind of job. Let's go.
//											doneChat[1] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyTrevor) < 15
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT1", CONV_PRIORITY_MEDIUM)
//											//Trevor, my man, I need your help to break into the military base.
//											//Now we're talking. What you up to?
//											//Just need to borrow one of those big army choppers. 
//											//Sounds like my kind of job. Let's go.
//											doneChat[1] = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//						
//						//Do some chat between Trevor and Michael once they're inside the base
//						IF doneChat[3] = FALSE
//							IF playerIsInBase = TRUE
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyMichael) < 15
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT3", CONV_PRIORITY_MEDIUM)
//											//Okay, here we go, stay alert. This is going to get messy.
//											//You don't need to worry about me.
//											doneChat[3] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyTrevor) < 15
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT3", CONV_PRIORITY_MEDIUM)
//											//Okay, here we go, stay alert. This is going to get messy.
//											//You don't need to worry about me.
//											doneChat[3] = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF						
//						
//						//Do some chat between Trevor and Michael once they're approaching the heli
//						IF doneChat[2] = FALSE
//							IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), HeliToSteal) < 100
//								AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyMichael) < 15
//									IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT2", CONV_PRIORITY_MEDIUM)
//										//There she is.
//										//Think I should do the flying.
//										//Whatever, let's just get it and get the fuck out of here.
//										doneChat[2] = TRUE
//									ENDIF
//								ENDIF
//							ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), HeliToSteal) < 100
//								AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyTrevor) < 15
//									IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT2", CONV_PRIORITY_MEDIUM)
//										//There she is.
//										//Think I should do the flying.
//										//Whatever, let's just get it and get the fuck out of here.
//										doneChat[2] = TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//					
//					//If Michael and Franklin exists but no Trevor 
//					IF MichaelAddedForChat = TRUE
//					AND FranklinAddedForChat = TRUE
//					AND NOT IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, TRUE)
//						IF doneChat[7] = FALSE
//							IF playerIsInBase = FALSE
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyMichael) < 15
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT7", CONV_PRIORITY_MEDIUM)
//											//Hey man we need to get a big chopper out the base. You game?
//											//Yeah sure, let's do it.
//											doneChat[7] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyFranklin) < 15
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT7", CONV_PRIORITY_MEDIUM)
//											//Hey man we need to get a big chopper out the base. You game?
//											//Yeah sure, let's do it.
//											doneChat[7] = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//						
//						//Some chat as they enter the base
//						IF doneChat[5] = FALSE
//							IF playerIsInBase = TRUE
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyMichael) < 15
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT5", CONV_PRIORITY_MEDIUM)
//											//Okay here we go. Stay alert.
//											//Let's fucking do this.
//											doneChat[5] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyFranklin) < 15
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT5", CONV_PRIORITY_MEDIUM)
//											//Okay here we go. Stay alert.
//											//Let's fucking do this.
//											doneChat[5] = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF						
//						
//						//Do some chat as they spot the heli
//						IF doneChat[10] = FALSE
//							IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), HeliToSteal) < 100
//								AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyMichael) < 15
//									IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT10", CONV_PRIORITY_MEDIUM)
//										//There she is, let's get in it and get the fuck out of here.
//										//Cool, one of us should ride up back incase any fucker needs taken out.
//										doneChat[10] = TRUE
//									ENDIF
//								ENDIF
//							ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), HeliToSteal) < 100
//								AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyFranklin) < 15
//									IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT10", CONV_PRIORITY_MEDIUM)
//										//There she is, let's get in it and get the fuck out of here.
//										//Cool, one of us should ride up back incase any fucker needs taken out.
//										doneChat[10] = TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF													
//					ENDIF					
//					
//					//If Trevor and Franklin exists but no Michael 
//					IF TrevorAddedForChat = TRUE
//					AND FranklinAddedForChat = TRUE
//					AND NOT IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, TRUE)
//						IF doneChat[11] = FALSE
//							IF playerIsInBase = FALSE
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyTrevor) < 15
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT11", CONV_PRIORITY_MEDIUM)
//											//Right you, we need to get a big fucking chopper from the base. Don't slow me down.
//											//Cut me some slack. I'm here ain't I?
//											//Maybe i will if you don't fuck this up.
//											doneChat[11] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyFranklin) < 15
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT11", CONV_PRIORITY_MEDIUM)
//											//Right you, we need to get a big fucking chopper from the base. Don't slow me down.
//											//Cut me some slack. I'm here ain't I?
//											//Maybe i will if you don't fuck this up.
//											doneChat[11] = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//						
//						//Some chat as they enter the base
//						IF doneChat[6] = FALSE
//							IF playerIsInBase = TRUE
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyTrevor) < 15
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT6", CONV_PRIORITY_MEDIUM)
//											//Fucking alarm. Stay close and take out any mother fucker that moves.
//											//This is crazy. We're fucked.
//											//Shut up, come on. 
//											doneChat[6] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyFranklin) < 15
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT6", CONV_PRIORITY_MEDIUM)
//											//Fucking alarm. Stay close and take out any mother fucker that moves.
//											//This is crazy. We're fucked.
//											//Shut up, come on. 
//											doneChat[6] = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF						
//						
//						//Do some chat as they spot the heli
//						IF doneChat[12] = FALSE
//							IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), HeliToSteal) < 100
//								AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyTrevor) < 15
//									IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT12", CONV_PRIORITY_MEDIUM)
//										//There's the fucker. C'mon let's get it and get the fuck out of here.
//										//Shit that's one big chopper. 
//										doneChat[12] = TRUE
//									ENDIF
//								ENDIF
//							ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), HeliToSteal) < 100
//								AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyFranklin) < 15
//									IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT12", CONV_PRIORITY_MEDIUM)
//										//There's the fucker. C'mon let's get it and get the fuck out of here.
//										//Shit that's one big chopper. 
//										doneChat[12] = TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF													
//					ENDIF						
//					
//					//If all 3 characters are there
//					IF TrevorAddedForChat = TRUE
//					AND FranklinAddedForChat = TRUE
//					AND MichaelAddedForChat = TRUE
//						IF doneChat[8] = FALSE
//							IF playerIsInBase = FALSE
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyTrevor) < 20
//									AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyMichael) < 20
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT8", CONV_PRIORITY_MEDIUM)
//											//Ok guys we're going to borrow a big chopper from these military mother fuckers.
//											//I take it they don't know anything about this. 
//											//I always wanted a shot in one of those big army choppers.
//											//You won't be getting a shot in anything bitch. I'm the pilot round here.
//											//Fuck you Trevor. I'll be flying it.
//											//Guys, enough. Just focus for fuck sake.
//											doneChat[8] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyFranklin) < 20
//									AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyMichael) < 20	
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT8", CONV_PRIORITY_MEDIUM)
//											//Ok guys we're going to borrow a big chopper from these military mother fuckers.
//											//I take it they don't know anything about this. 
//											//I always wanted a shot in one of those big army choppers.
//											//You won't be getting a shot in anything bitch. I'm the pilot round here.
//											//Fuck you Trevor. I'll be flying it.
//											//Guys, enough. Just focus for fuck sake.
//											doneChat[8] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyFranklin) < 20
//									AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyTrevor) < 20	
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT8", CONV_PRIORITY_MEDIUM)
//											//Ok guys we're going to borrow a big chopper from these military mother fuckers.
//											//I take it they don't know anything about this. 
//											//I always wanted a shot in one of those big army choppers.
//											//You won't be getting a shot in anything bitch. I'm the pilot round here.
//											//Fuck you Trevor. I'll be flying it.
//											//Guys, enough. Just focus for fuck sake.
//											doneChat[8] = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//						
//						//Some chat as they see the heli
//						IF doneChat[9] = FALSE
//							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), HeliToSteal) < 100
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyTrevor) < 20
//									AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyMichael) < 20
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT9", CONV_PRIORITY_MEDIUM)
//											//There she is guys. Let's get in it and get the fuck out of here.
//											//Two of us should sit in the back incase and fucker needs taken out.
//											//You need taken out.
//											//Enough!
//											doneChat[9] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyFranklin) < 20
//									AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyMichael) < 20	
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT9", CONV_PRIORITY_MEDIUM)
//											//There she is guys. Let's get in it and get the fuck out of here.
//											//Two of us should sit in the back incase and fucker needs taken out.
//											//You need taken out.
//											//Enough!
//											doneChat[9] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyFranklin) < 20
//									AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), hBuddyTrevor) < 20	
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT9", CONV_PRIORITY_MEDIUM)
//											//There she is guys. Let's get in it and get the fuck out of here.
//											//Two of us should sit in the back incase and fucker needs taken out.
//											//You need taken out.
//											//Enough!
//											doneChat[9] = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF																		
//					ENDIF						
//					
//				ENDIF
//			ENDIF
//			
//		BREAK
//	
//		CASE STAGE_ESCAPE_MILITARY
//		
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				IF NOT IS_MESSAGE_BEING_DISPLAYED()
//				
//					//If Michael and Trevor exists but no Franklin 
//					IF TrevorAddedForChat = TRUE
//					AND MichaelAddedForChat = TRUE
//					AND NOT IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, TRUE)
//						//Do some chat once they're both in the heli
//						IF doneChat[16] = FALSE
//							IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT16", CONV_PRIORITY_MEDIUM)
//											//Ok this is bigger than i expected. 
//											//This is what i'm talking about. Haha
//											doneChat[16] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT16", CONV_PRIORITY_MEDIUM)
//											//Ok this is bigger than i expected. 
//											//This is what i'm talking about. Haha
//											doneChat[16] = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF	
//					
//					//If Michael and Franklin exists but no Trevor 
//					IF FranklinAddedForChat = TRUE
//					AND MichaelAddedForChat = TRUE
//					AND NOT IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, TRUE)
//						//Do some chat once they're both in the heli
//						IF doneChat[17] = FALSE
//							IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT17", CONV_PRIORITY_MEDIUM)
//											//This is bigger than i expected.
//											//Fucking huge, just hope we don't crash.
//											doneChat[17] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT17", CONV_PRIORITY_MEDIUM)
//											//This is bigger than i expected.
//											//Fucking huge, just hope we don't crash.
//											doneChat[17] = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//					
//					//If Trevor and Franklin exists but no Michael 
//					IF FranklinAddedForChat = TRUE
//					AND TrevorAddedForChat = TRUE
//					AND NOT IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, TRUE)
//						//Do some chat once they're both in the heli
//						IF doneChat[18] = FALSE
//							IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT18", CONV_PRIORITY_MEDIUM)
//											//Damn, this thing's huge.
//											//This is a real machine, now let's see what she's made of.
//											doneChat[18] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT18", CONV_PRIORITY_MEDIUM)
//											//Damn, this thing's huge.
//											//This is a real machine, now let's see what she's made of.
//											doneChat[18] = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF	
//					
//					//Have some chat if all 3 characters are in the heli depending on who's flying.
//					IF FranklinAddedForChat = TRUE
//					AND TrevorAddedForChat = TRUE
//					AND MichaelAddedForChat = TRUE	
//						//If Michael is pilot
//						IF doneChat[13] = FALSE
//							IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//										IF IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(), HeliToSteal, VS_DRIVER)
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT13", CONV_PRIORITY_MEDIUM)
//												//Can you even fly?
//												//We'll soon find out.
//												//How come i don't get to fly?
//												//I'm the fucking pilot here, you're going to kill us all.
//												doneChat[13] = TRUE
//											ENDIF
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//										IF IS_PED_SITTING_IN_VEHICLE_SEAT(hBuddyMichael, HeliToSteal, VS_DRIVER)
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT13", CONV_PRIORITY_MEDIUM)
//												//Can you even fly?
//												//We'll soon find out.
//												//How come i don't get to fly?
//												//I'm the fucking pilot here, you're going to kill us all.
//												doneChat[13] = TRUE
//											ENDIF
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//										IF IS_PED_SITTING_IN_VEHICLE_SEAT(hBuddyMichael, HeliToSteal, VS_DRIVER)
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT13", CONV_PRIORITY_MEDIUM)
//												//Can you even fly?
//												//We'll soon find out.
//												//How come i don't get to fly?
//												//I'm the fucking pilot here, you're going to kill us all.
//												doneChat[13] = TRUE
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//
//						//If Franklin is pilot
//						IF doneChat[15] = FALSE
//							IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//										IF IS_PED_SITTING_IN_VEHICLE_SEAT(hBuddyFranklin, HeliToSteal, VS_DRIVER)
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT15", CONV_PRIORITY_MEDIUM)
//												//Woohoo here we fucking go. 
//												//What the fuck is this idiot getting to fly for? You got a death wish?
//												//Just take it easy, c'mon let's get out of here.
//												doneChat[15] = TRUE
//											ENDIF
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//										IF IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(), HeliToSteal, VS_DRIVER)
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT15", CONV_PRIORITY_MEDIUM)
//												//Woohoo here we fucking go. 
//												//What the fuck is this idiot getting to fly for? You got a death wish?
//												//Just take it easy, c'mon let's get out of here.
//												doneChat[15] = TRUE
//											ENDIF
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//										IF IS_PED_SITTING_IN_VEHICLE_SEAT(hBuddyFranklin, HeliToSteal, VS_DRIVER)
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT15", CONV_PRIORITY_MEDIUM)
//												//Woohoo here we fucking go. 
//												//What the fuck is this idiot getting to fly for? You got a death wish?
//												//Just take it easy, c'mon let's get out of here.
//												doneChat[15] = TRUE
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//						
//						//If Trevor is pilot
//						IF doneChat[14] = FALSE
//							IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//										IF IS_PED_SITTING_IN_VEHICLE_SEAT(hBuddyTrevor, HeliToSteal, VS_DRIVER)
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT14", CONV_PRIORITY_MEDIUM)
//												//Let's get the fuck out of here.
//												//I wanted to fly.
//												//Trevor's the best man for the job.
//												//Yeah, Don't be a little bitch about it.
//												doneChat[14] = TRUE
//											ENDIF
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//										IF IS_PED_SITTING_IN_VEHICLE_SEAT(hBuddyTrevor, HeliToSteal, VS_DRIVER)
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT14", CONV_PRIORITY_MEDIUM)
//												//Let's get the fuck out of here.
//												//I wanted to fly.
//												//Trevor's the best man for the job.
//												//Yeah, Don't be a little bitch about it.
//												doneChat[14] = TRUE
//											ENDIF
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//										IF IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(), HeliToSteal, VS_DRIVER)
//											IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT14", CONV_PRIORITY_MEDIUM)
//												//Let's get the fuck out of here.
//												//I wanted to fly.
//												//Trevor's the best man for the job.
//												//Yeah, Don't be a little bitch about it.
//												doneChat[14] = TRUE
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF	
//					ENDIF
//					
//					//Do some chat for the enemy heli's being spotted make sure at least 1 buddy is with the player and who ever is flying should say this chat
//					IF EnemyHeliSpottedChatDone = FALSE
//						//If Michael is flying with at least 1 buddy
//						IF MichaelAddedForChat = TRUE
//							IF TrevorAddedForChat = TRUE
//							OR FranklinAddedForChat = TRUE	
//								IF IS_VEHICLE_DRIVEABLE(HeliToSteal)	
//									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//										IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//											IF IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(), HeliToSteal, VS_DRIVER)									
//												IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//													IF IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//														IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT22", CONV_PRIORITY_MEDIUM)
//															//Fuck I see heli's taking off. Shit shit shit.
//															EnemyHeliSpottedChatDone = TRUE
//														ENDIF	
//													ENDIF
//												ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)	
//													IF IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//														IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT22", CONV_PRIORITY_MEDIUM)
//															//Fuck I see heli's taking off. Shit shit shit.
//															EnemyHeliSpottedChatDone = TRUE
//														ENDIF	
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF
//									ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//										IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//											IF IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//												IF IS_PED_SITTING_IN_VEHICLE_SEAT(hBuddyMichael, HeliToSteal, VS_DRIVER)									
//													IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//														IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT22", CONV_PRIORITY_MEDIUM)
//															//Fuck I see heli's taking off. Shit shit shit.
//															EnemyHeliSpottedChatDone = TRUE
//														ENDIF	
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF
//									ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//										IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//											IF IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//												IF IS_PED_SITTING_IN_VEHICLE_SEAT(hBuddyMichael, HeliToSteal, VS_DRIVER)									
//													IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//														IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT22", CONV_PRIORITY_MEDIUM)
//															//Fuck I see heli's taking off. Shit shit shit.
//															EnemyHeliSpottedChatDone = TRUE
//														ENDIF	
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF
//									ENDIF										
//								ENDIF
//							ENDIF
//						ENDIF
//					
//						//If Trevor is flying with at least 1 buddy
//						IF TrevorAddedForChat = TRUE
//							IF MichaelAddedForChat = TRUE
//							OR FranklinAddedForChat = TRUE	
//								IF IS_VEHICLE_DRIVEABLE(HeliToSteal)	
//									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//										IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//											IF IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(), HeliToSteal, VS_DRIVER)									
//												IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//													IF IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//														IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT23", CONV_PRIORITY_MEDIUM)
//															//Fuck there's choppers taking off, how the fuck are we meant to lose them in this heap of shit.
//															EnemyHeliSpottedChatDone = TRUE
//														ENDIF	
//													ENDIF
//												ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)	
//													IF IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//														IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT23", CONV_PRIORITY_MEDIUM)
//															//Fuck there's choppers taking off, how the fuck are we meant to lose them in this heap of shit.
//															EnemyHeliSpottedChatDone = TRUE
//														ENDIF	
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF
//									ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//										IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//											IF IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//												IF IS_PED_SITTING_IN_VEHICLE_SEAT(hBuddyTrevor, HeliToSteal, VS_DRIVER)									
//													IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//														IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT23", CONV_PRIORITY_MEDIUM)
//															//Fuck there's choppers taking off, how the fuck are we meant to lose them in this heap of shit.
//															EnemyHeliSpottedChatDone = TRUE
//														ENDIF	
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF
//									ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//										IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//											IF IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//												IF IS_PED_SITTING_IN_VEHICLE_SEAT(hBuddyTrevor, HeliToSteal, VS_DRIVER)									
//													IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//														IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT23", CONV_PRIORITY_MEDIUM)
//															//Fuck there's choppers taking off, how the fuck are we meant to lose them in this heap of shit.
//															EnemyHeliSpottedChatDone = TRUE
//														ENDIF	
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF
//									ENDIF	
//								ENDIF
//							ENDIF
//						ENDIF					
//			
//						//If Franklin is flying with at least 1 buddy
//						IF FranklinAddedForChat = TRUE
//							IF MichaelAddedForChat = TRUE
//							OR TrevorAddedForChat = TRUE	
//								IF IS_VEHICLE_DRIVEABLE(HeliToSteal)	
//									IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//										IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//											IF IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(), HeliToSteal, VS_DRIVER)									
//												IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//													IF IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//														IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT24", CONV_PRIORITY_MEDIUM)
//															//Shit we got more company, an army chopper taking off. We're fucked!
//															EnemyHeliSpottedChatDone = TRUE
//														ENDIF	
//													ENDIF
//												ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)	
//													IF IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//														IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT24", CONV_PRIORITY_MEDIUM)
//															//Shit we got more company, an army chopper taking off. We're fucked!
//															EnemyHeliSpottedChatDone = TRUE
//														ENDIF	
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF
//									ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//										IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//											IF IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//												IF IS_PED_SITTING_IN_VEHICLE_SEAT(hBuddyFranklin, HeliToSteal, VS_DRIVER)									
//													IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//														IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT24", CONV_PRIORITY_MEDIUM)
//															//Shit we got more company, an army chopper taking off. We're fucked!
//															EnemyHeliSpottedChatDone = TRUE
//														ENDIF	
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF
//									ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//										IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//											IF IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//												IF IS_PED_SITTING_IN_VEHICLE_SEAT(hBuddyFranklin, HeliToSteal, VS_DRIVER)									
//													IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//														IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT24", CONV_PRIORITY_MEDIUM)
//															//Shit we got more company, an army chopper taking off. We're fucked!
//															EnemyHeliSpottedChatDone = TRUE
//														ENDIF	
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF
//									ENDIF	
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF					
//					
//					//Do some dialogue about finding the rocket launcher in the back. Dialogue should be spoken by the ped who is in back left seat.
//					IF EnemyHeliSpottedChatDone = TRUE
//						IF FoundRocketChatDone = FALSE
//							//If Franklin is in left back seat in heli.
//							IF FranklinAddedForChat = TRUE	
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//									IF BuddyFranklinReadyToFireRockets = TRUE
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT21", CONV_PRIORITY_MEDIUM)
//											//Hey there's a rocket launcher back here. Let's see how they like this.
//											FoundRocketChatDone = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//							//If Trevor is in left back seat in heli.
//							IF TrevorAddedForChat = TRUE	
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//									IF BuddyTrevorReadyToFireRockets = TRUE
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT20", CONV_PRIORITY_MEDIUM)
//											//Now we're fucking talking, rocket launcher in the back here. Get some.
//											FoundRocketChatDone = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//							//If Michael is in left back seat in heli.
//							IF MichaelAddedForChat = TRUE	
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF BuddyMichaelReadyToFireRockets = TRUE
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT19", CONV_PRIORITY_MEDIUM)
//											//Well what have we got here? Just found a rocket launcher in the back, this should help us.
//											FoundRocketChatDone = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//					
//				ENDIF
//			ENDIF
//			
//		BREAK
//		
//		CASE STAGE_FLY_CARGOBOB_TO_HANGER
//		
//			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				IF NOT IS_MESSAGE_BEING_DISPLAYED()
//				
//					//If Michael and Trevor exists but no Franklin 
//					IF TrevorAddedForChat = TRUE
//					AND MichaelAddedForChat = TRUE
//					AND NOT IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, TRUE)			
//						
//						//Do some chat between Trevor and Michael once they've taken down the enemy heli's.
//						IF doneChat[45] = FALSE
//							IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT45", CONV_PRIORITY_MEDIUM)
//											//Think that was the last of them.
//											//Damn i was enjoying watching them blow the fuck up.
//											//Why doesn't that surprise me. Let's get this bad boy back to the hanger
//											doneChat[45] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT45", CONV_PRIORITY_MEDIUM)
//											//Think that was the last of them.
//											//Damn i was enjoying watching them blow the fuck up.
//											//Why doesn't that surprise me. Let's get this bad boy back to the hanger
//											doneChat[45] = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF	
//					
//					ENDIF
//					
//					//If Michael and Franklin exists but no Trevor 
//					IF FranklinAddedForChat = TRUE
//					AND MichaelAddedForChat = TRUE
//					AND NOT IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, TRUE)			
//						
//						//Do some chat between Trevor and Michael once they've taken down the enemy heli's.
//						IF doneChat[44] = FALSE
//							IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT44", CONV_PRIORITY_MEDIUM)
//											//Think that was the last of them. Let's get this back to the hanger.
//											//Yeah thank fuck for that rocket launcher or we would've been screwed.
//											doneChat[44] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT44", CONV_PRIORITY_MEDIUM)
//											//Think that was the last of them. Let's get this back to the hanger.
//											//Yeah thank fuck for that rocket launcher or we would've been screwed.
//											doneChat[44] = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//						
//					ENDIF
//
//					//If Trevor and Franklin exists but no Michael 
//					IF FranklinAddedForChat = TRUE
//					AND TrevorAddedForChat = TRUE
//					AND NOT IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, TRUE)			
//						
//						//Do some chat between Trevor and Michael once they've taken down the enemy heli's.
//						IF doneChat[46] = FALSE
//							IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT46", CONV_PRIORITY_MEDIUM)
//											//Think that was the last of them.
//											//Fucking shame, I was enjoying that!
//											//At least one of us was.
//											//Ah well let's get this beast home.
//											doneChat[46] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT46", CONV_PRIORITY_MEDIUM)
//											//Think that was the last of them.
//											//Fucking shame, I was enjoying that!
//											//At least one of us was.
//											//Ah well let's get this beast home.
//											doneChat[46] = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//						
//					ENDIF
//					
//					//If all 3 characters are there
//					IF TrevorAddedForChat = TRUE
//					AND FranklinAddedForChat = TRUE
//					AND MichaelAddedForChat = TRUE
//						IF doneChat[43] = FALSE
//							IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//								IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT43", CONV_PRIORITY_MEDIUM)
//											//And relax. Think that's the last of them. 
//											//Fucking eh.
//											//Thank fuck for the rocket launcher.
//											//Yeah, let's just get this bad boy back to the hanger.
//											doneChat[43] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT43", CONV_PRIORITY_MEDIUM)
//											//And relax. Think that's the last of them. 
//											//Fucking eh.
//											//Thank fuck for the rocket launcher.
//											//Yeah, let's just get this bad boy back to the hanger.
//											doneChat[43] = TRUE
//										ENDIF
//									ENDIF
//								ELIF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//								AND IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
//									AND IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
//										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT43", CONV_PRIORITY_MEDIUM)
//											//And relax. Think that's the last of them. 
//											//Fucking eh.
//											//Thank fuck for the rocket launcher.
//											//Yeah, let's just get this bad boy back to the hanger.
//											doneChat[43] = TRUE
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF					
//					
//				ENDIF
//			ENDIF
//			
//		BREAK
//		
//		CASE STAGE_END_CUTSCENE
//			
//			//If all 3 are there do chat depending on who the player is
//			IF DoneEndCutChat = FALSE
//				IF TrevorAddedForChat = TRUE
//				AND FranklinAddedForChat = TRUE
//				AND MichaelAddedForChat = TRUE
//					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//						AND IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//							IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT48", CONV_PRIORITY_AMBIENT_MEDIUM)	
//								//Well here we are. 
//								//Yeah you boys done well, even you Franklin.
//								//You feeling ok?
//								//Ah fuck you, i'm outta here.
//								//Fuck you too. Michael I gotta be somewhere, laters.
//								DoneEndCutChat = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//						AND IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//							IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT47", CONV_PRIORITY_AMBIENT_MEDIUM)	
//								//Well here we are. 
//								//Yeah you boys done well, even you Franklin.
//								//You feeling ok?
//								//Anyway catch up with you two later.
//								//Yeah I'm out of here too. 
//								DoneEndCutChat = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//						AND IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//							IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT49", CONV_PRIORITY_AMBIENT_MEDIUM)	
//								//Well here we are. 
//								//Yeah you boys done well, even you Franklin.
//								//You feeling ok?
//								//Ah get the fuck out of here.
//								//Guy's i'll leave you to it. It's been fun.
//								//I'm not staying here with this asshole.
//								DoneEndCutChat = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF	
//				
//		BREAK
//	ENDSWITCH
	
ENDPROC


////PURPOSE: Controls the players buddy's AI
//PROC HANDLE_PLAYER_CHARACTERS()
//
//	//Control Michael and Franklins AI if playing as Trev
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//				
//		//Give Michaels AI task
//		IF MichaelAITaskGiven = FALSE
//			IF DOES_ENTITY_EXIST(HeliToSteal)	
//				IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//					
//					//Check if Michael is inside the heli
//					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])					
//							IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], HeliToSteal)
//								
//								//Run a check to see what seat Michael is in.
//								//If Michael is flying give him flying tasks.
//								IF GET_PED_VEHICLE_SEAT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], HeliToSteal) = VS_DRIVER
//											
//									//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)									
//											
//										//Check if Franklin exists and if he is in the heli.
//										IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])	
//												IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal)									
//										
//													//Michael is flying the heli and everyone is in the heli, give him flying tasks.
//													vMichaelsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//													SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//													CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//													OPEN_SEQUENCE_TASK(seq)
//														IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vMichaelsCoords.x,vMichaelsCoords.y, (vMichaelsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//														ENDIF
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//													CLOSE_SEQUENCE_TASK(seq)
//													TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seq)
//													CLEAR_SEQUENCE_TASK(seq)
//													//Put Michael in players group
//													IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//														SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//													ENDIF
//													MichaelAITaskGiven = TRUE
//													
//												ELSE
//													
//													//Michael and Trevor are in the heli, but Franklin is not. IF they are on the ground they need to wait for Franklin to get in. Give Michael waiting task. If in the air they can go with out Franklin.
//													SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//													CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//													OPEN_SEQUENCE_TASK(seq)
//														IF IS_ENTITY_IN_AIR(HeliToSteal)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//														ENDIF
//													CLOSE_SEQUENCE_TASK(seq)
//													TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seq)
//													CLEAR_SEQUENCE_TASK(seq)
//													//Put Michael in players group
//													IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//														SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//													ENDIF
//													MichaelAITaskGiven = TRUE
//												
//												ENDIF
//												
//											ELSE
//												
//												//If Franklin does exist but is dead
//												vMichaelsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//												SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//												CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//												OPEN_SEQUENCE_TASK(seq)
//													IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vMichaelsCoords.x,vMichaelsCoords.y, (vMichaelsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//													ENDIF
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//												CLOSE_SEQUENCE_TASK(seq)
//												TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seq)
//												CLEAR_SEQUENCE_TASK(seq)
//												//Put Michael in players group
//												IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//													SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//												ENDIF
//												MichaelAITaskGiven = TRUE
//											ENDIF
//											
//										ELSE
//											
//											//If Franklin doesn't exist
//											vMichaelsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//											OPEN_SEQUENCE_TASK(seq)
//												IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vMichaelsCoords.x,vMichaelsCoords.y, (vMichaelsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//												ENDIF
//												TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//												TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//											CLOSE_SEQUENCE_TASK(seq)
//											TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seq)
//											CLEAR_SEQUENCE_TASK(seq)
//											//Put Michael in players group
//											IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//												SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//											ENDIF
//											MichaelAITaskGiven = TRUE
//										ENDIF
//									
//									ELSE
//										
//										//If player is not in the heli, check to see how far away the player is.
//										//If player is close by less than 15m Michael should just wait for the player inside the heli. 
//										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]) < 15
//										
//											//Remove Michael from players group
//											IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//												REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											ENDIF										
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//											//If heli is in the air tell Michael to land it
//											IF IS_ENTITY_IN_AIR(HeliToSteal)
//												vMichaelsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//												GET_GROUND_Z_FOR_3D_COORD(vMichaelsCoords, fGroundzCoord)
//												TASK_HELI_MISSION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], HeliToSteal, NULL, NULL, <<vMichaelsCoords.x,vMichaelsCoords.y, fGroundzCoord>>, MISSION_GOTO, 10, 1, -1, 0, 0)
//											ENDIF
//											MichaelAITaskGiven = TRUE
//											
//										ELSE
//											
//											//If player is further than 15metres tell Michael to get out the heli and go to the player and help him out.
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//											OPEN_SEQUENCE_TASK(seq)
//												IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//													vMichaelsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//													GET_GROUND_Z_FOR_3D_COORD(vMichaelsCoords, fGroundzCoord)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vMichaelsCoords.x,vMichaelsCoords.y, fGroundzCoord>>, MISSION_GOTO, 10, 1, -1, 0, 0)
//												ENDIF
//												TASK_LEAVE_VEHICLE(NULL, HeliToSteal, ECF_DONT_CLOSE_DOOR)
//												TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID())
//											CLOSE_SEQUENCE_TASK(seq)
//											TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seq)
//											CLEAR_SEQUENCE_TASK(seq)	
//											//Put Michael in players group
//											IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//												SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//											ENDIF
//											MichaelAITaskGiven = TRUE
//											
//										ENDIF
//									ENDIF
//								
//								ELSE
//									
//									//If Michael is not the driver of the heli sort out his tasks. No need to check if Franklin is alive or in the heli here.
//									//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)									
//																			
//										//Michael is not flying the heli and everyone is in the heli, give him rocket tasks.
//										SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//										CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//										
//										//*********GIVE MICHAEL ROCKET TASKS HERE*********
//										
//										//Put Michael in players group
//										IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//											SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//										ENDIF
//										MichaelAITaskGiven = TRUE
//									
//									ELSE
//										
//										//If player is not in the heli, check to see how far away the player is.
//										//If player is close by less than 15m Michael should just wait for the player inside the heli. 
//										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]) < 15
//										
//											//Remove Michael from players group
//											IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//												REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											ENDIF										
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//											MichaelAITaskGiven = TRUE
//											
//										ELSE
//											
//											//If player is further than 15metres tell Michael to get out the heli and go to the player and help him out.
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//											IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//												OPEN_SEQUENCE_TASK(seq)
//													TASK_LEAVE_VEHICLE(NULL, HeliToSteal, ECF_DONT_CLOSE_DOOR)
//													TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID())
//												CLOSE_SEQUENCE_TASK(seq)
//												TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seq)
//												CLEAR_SEQUENCE_TASK(seq)
//											ENDIF
//											//Put Michael in players group
//											IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//												IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//													SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//												ENDIF
//											ENDIF
//											MichaelAITaskGiven = TRUE
//											
//										ENDIF
//									ENDIF									
//								ENDIF
//							
//							ELSE
//							
//								//If Michael is not in the heli give him the following tasks
//								//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI. ONLY NEED TO CHECK IF FRANKLIN EXISTS TO SEE IF HE IS FLYING HELI HERE.
//								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)																	
//									
//									SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//									CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)	
//									
//									//Michael is not in heli and Player is so tell Michael to get in the heli. If the heli is on the ground.
//									IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//										OPEN_SEQUENCE_TASK(seq)
//											TASK_GO_TO_ENTITY(NULL, HeliToSteal)
//											IF GET_PED_VEHICLE_SEAT(PLAYER_PED_ID(), HeliToSteal) = VS_DRIVER
//												TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_ANY_PASSENGER)
//											ELSE
//												//Check if Franklin exists and if he is in the heli.
//												IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//													IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])	
//														IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal)	
//															IF GET_PED_VEHICLE_SEAT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal) = VS_DRIVER
//																TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_ANY_PASSENGER)
//															ELSE
//																TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//															ENDIF
//														ELSE
//															TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//														ENDIF
//													ENDIF
//												ELSE
//													TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//												ENDIF
//											ENDIF
//										CLOSE_SEQUENCE_TASK(seq)
//										TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seq)
//										CLEAR_SEQUENCE_TASK(seq)
//										//Put Michael in players group
//										IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//											SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//										ENDIF
//									ELSE
//										//Remove Michael from players group
//										IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//											REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//										ENDIF
//									ENDIF
//									
//									MichaelAITaskGiven = TRUE
//												
//								ELSE
//									
//									//Michael and the Trevor are not in the heli
//									SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//									CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
//
//									//Put Michael in players group
//									IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//										SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//									ENDIF
//									MichaelAITaskGiven = TRUE
//								
//								ENDIF
//							ENDIF
//						ENDIF	
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		//Give Franklin's AI task
//		IF FranklinAITaskGiven = FALSE
//			IF DOES_ENTITY_EXIST(HeliToSteal)	
//				IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//					
//					//Check if Franklin is inside the heli
//					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])					
//							IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal)
//								
//								//Run a check to see what seat Franklin is in.
//								//If Franklin is flying give him flying tasks.
//								IF GET_PED_VEHICLE_SEAT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal) = VS_DRIVER
//											
//									//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)									
//											
//										//Check if Michael exists and if he is in the heli.
//										IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])	
//												IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], HeliToSteal)									
//										
//													//Franklin is flying the heli and everyone is in the heli, give him flying tasks.
//													vFranklinsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//													SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//													CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//													OPEN_SEQUENCE_TASK(seq)
//														IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vFranklinsCoords.x,vFranklinsCoords.y, (vFranklinsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//														ENDIF
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//													CLOSE_SEQUENCE_TASK(seq)
//													TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], seq)
//													CLEAR_SEQUENCE_TASK(seq)
//													//Put Michael in players group
//													IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//														SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//													ENDIF
//													FranklinAITaskGiven = TRUE
//													
//												ELSE
//													
//													//Franklin and Trevor are in the heli, but Michael is not. IF they are on the ground they need to wait for Michael to get in. Give Franklin waiting task. If in the air they can go with out Michael.
//													SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//													CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//													OPEN_SEQUENCE_TASK(seq)
//														IF IS_ENTITY_IN_AIR(HeliToSteal)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//														ENDIF
//													CLOSE_SEQUENCE_TASK(seq)
//													TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], seq)
//													CLEAR_SEQUENCE_TASK(seq)
//													//Put Franklin in players group
//													IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//														SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//													ENDIF
//													FranklinAITaskGiven = TRUE
//												
//												ENDIF
//											
//											ELSE
//												
//												//If Michael doesn't exist
//												vFranklinsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//												SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//												CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//												OPEN_SEQUENCE_TASK(seq)
//													IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vFranklinsCoords.x,vFranklinsCoords.y, (vFranklinsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//													ENDIF
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//												CLOSE_SEQUENCE_TASK(seq)
//												TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], seq)
//												CLEAR_SEQUENCE_TASK(seq)
//												//Put Franklin in players group
//												IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//													SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//												ENDIF
//												FranklinAITaskGiven = TRUE
//											ENDIF
//											
//										ELSE
//											
//											//If Michael doesn't exist
//											vFranklinsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//											OPEN_SEQUENCE_TASK(seq)
//												IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vFranklinsCoords.x,vFranklinsCoords.y, (vFranklinsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//												ENDIF
//												TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//												TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//											CLOSE_SEQUENCE_TASK(seq)
//											TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], seq)
//											CLEAR_SEQUENCE_TASK(seq)
//											//Put Franklin in players group
//											IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//												SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//											ENDIF
//											FranklinAITaskGiven = TRUE
//										ENDIF
//									
//									ELSE
//										
//										//If player is not in the heli, check to see how far away the player is.
//										//If player is close by less than 15m Franklin should just wait for the player inside the heli. 
//										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) < 15
//										
//											//Remove Franklin from players group
//											IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//												REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											ENDIF										
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//											//If heli is in the air tell Franklin to land it
//											IF IS_ENTITY_IN_AIR(HeliToSteal)
//												vFranklinsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//												GET_GROUND_Z_FOR_3D_COORD(vFranklinsCoords, fGroundzCoord)
//												TASK_HELI_MISSION(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal, NULL, NULL, <<vFranklinsCoords.x,vFranklinsCoords.y, fGroundzCoord>>, MISSION_GOTO, 10, 1, -1, 0, 0)
//											ENDIF
//											FranklinAITaskGiven = TRUE
//											
//										ELSE
//											
//											//If player is further than 15metres tell Franklin to get out the heli and go to the player and help him out.
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//											OPEN_SEQUENCE_TASK(seq)
//												IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//													vFranklinsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//													GET_GROUND_Z_FOR_3D_COORD(vFranklinsCoords, fGroundzCoord)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vFranklinsCoords.x,vFranklinsCoords.y, fGroundzCoord>>, MISSION_GOTO, 10, 1, -1, 0, 0)
//												ENDIF
//												TASK_LEAVE_VEHICLE(NULL, HeliToSteal, ECF_DONT_CLOSE_DOOR)
//												TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID())
//											CLOSE_SEQUENCE_TASK(seq)
//											TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], seq)
//											CLEAR_SEQUENCE_TASK(seq)	
//											//Put Franklin in players group
//											IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//												SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//											ENDIF
//											FranklinAITaskGiven = TRUE
//											
//										ENDIF
//									ENDIF
//								
//								ELSE
//									
//									//If Franklin is not the driver of the heli sort out his tasks. No need to check if Michael is alive or in the heli here.
//									//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)									
//																			
//										//Franklin is not flying the heli and everyone is in the heli, give him rocket tasks.
//										SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//										CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//										
//										//*********GIVE FRANKLIN ROCKET TASKS HERE*********
//										
//										//Put Franklin in players group
//										IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//											SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//										ENDIF
//										FranklinAITaskGiven = TRUE
//									
//									ELSE
//										
//										//If player is not in the heli, check to see how far away the player is.
//										//If player is close by less than 15m Franklin should just wait for the player inside the heli. 
//										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) < 15
//										
//											//Remove Franklin from players group
//											IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//												REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											ENDIF										
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//											FranklinAITaskGiven = TRUE
//											
//										ELSE
//											
//											//If player is further than 15metres tell Franklin to get out the heli and go to the player and help him out.
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//											IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//												OPEN_SEQUENCE_TASK(seq)
//													TASK_LEAVE_VEHICLE(NULL, HeliToSteal, ECF_DONT_CLOSE_DOOR)
//													TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID())
//												CLOSE_SEQUENCE_TASK(seq)
//												TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], seq)
//												CLEAR_SEQUENCE_TASK(seq)
//											ENDIF
//											//Put Franklin in players group
//											IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//												IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//													SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//												ENDIF
//											ENDIF
//											FranklinAITaskGiven = TRUE
//											
//										ENDIF
//									ENDIF									
//								ENDIF
//							
//							ELSE
//							
//								//If Franklin is not in the heli give him the following tasks
//								//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI. ONLY NEED TO CHECK IF MICHAEL EXISTS TO SEE IF HE IS FLYING HELI HERE.
//								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)																	
//									
//									SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//									CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)	
//									
//									//Franklin is not in heli and Player is so tell Franklin to get in the heli. If the heli is on the ground.
//									IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//										OPEN_SEQUENCE_TASK(seq)
//											TASK_GO_TO_ENTITY(NULL, HeliToSteal)
//											IF GET_PED_VEHICLE_SEAT(PLAYER_PED_ID(), HeliToSteal) = VS_DRIVER
//												TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_ANY_PASSENGER)
//											ELSE
//												//Check if Michael exists and if he is in the heli.
//												IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//													IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])	
//														IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], HeliToSteal)	
//															IF GET_PED_VEHICLE_SEAT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], HeliToSteal) = VS_DRIVER
//																TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_ANY_PASSENGER)
//															ELSE
//																TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//															ENDIF
//														ELSE
//															TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//														ENDIF
//													ENDIF
//												ELSE
//													TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//												ENDIF
//											ENDIF
//										CLOSE_SEQUENCE_TASK(seq)
//										TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], seq)
//										CLEAR_SEQUENCE_TASK(seq)
//										//Put Franklin in players group
//										IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//											SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//										ENDIF
//									ELSE
//										//Remove Franklin from players group
//										IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//											REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//										ENDIF
//									ENDIF
//									
//									FranklinAITaskGiven = TRUE
//												
//								ELSE
//									
//									//Franklin and the Trevor are not in the heli
//									SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//									CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
//
//									//Put Franklin in players group
//									IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//										SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//									ENDIF
//									FranklinAITaskGiven = TRUE
//								
//								ENDIF
//							ENDIF
//						ENDIF	
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		
//	ENDIF
//	
//	//Control Trevor and Franklins AI if playing as Michael
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//				
//		//Give Trevor AI task
//		IF TrevorAITaskGiven = FALSE
//			IF DOES_ENTITY_EXIST(HeliToSteal)	
//				IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//					
//					//Check if Trevor is inside the heli
//					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])					
//							IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], HeliToSteal)
//								
//								//Run a check to see what seat Trevor is in.
//								//If Trevor is flying give him flying tasks.
//								IF GET_PED_VEHICLE_SEAT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], HeliToSteal) = VS_DRIVER
//											
//									//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)									
//											
//										//Check if Franklin exists and if he is in the heli.
//										IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])	
//												IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal)									
//										
//													//Trevor is flying the heli and everyone is in the heli, give him flying tasks.
//													vTrevorsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//													SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//													CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//													OPEN_SEQUENCE_TASK(seq)
//														IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vTrevorsCoords.x,vTrevorsCoords.y, (vTrevorsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//														ENDIF
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//													CLOSE_SEQUENCE_TASK(seq)
//													TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
//													CLEAR_SEQUENCE_TASK(seq)
//													//Put Trevor in players group
//													IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//														SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//													ENDIF
//													TrevorAITaskGiven = TRUE
//													
//												ELSE
//													
//													//Michael and Trevor are in the heli, but Franklin is not. IF they are on the ground they need to wait for Franklin to get in. Give Trevor waiting task. If in the air they can go with out Franklin.
//													SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//													CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//													OPEN_SEQUENCE_TASK(seq)
//														IF IS_ENTITY_IN_AIR(HeliToSteal)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//														ENDIF
//													CLOSE_SEQUENCE_TASK(seq)
//													TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
//													CLEAR_SEQUENCE_TASK(seq)
//													//Put Trevor in players group
//													IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//														SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//													ENDIF
//													TrevorAITaskGiven = TRUE
//												
//												ENDIF
//											ELSE
//												//If Franklin doesn't exist
//												vTrevorsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//												SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//												CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//												OPEN_SEQUENCE_TASK(seq)
//													IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vTrevorsCoords.x,vTrevorsCoords.y, (vTrevorsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//													ENDIF
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//												CLOSE_SEQUENCE_TASK(seq)
//												TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
//												CLEAR_SEQUENCE_TASK(seq)
//												//Put Trevor in players group
//												IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//													SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//												ENDIF
//												TrevorAITaskGiven = TRUE
//											ENDIF
//											
//										ELSE
//											
//											//If Franklin doesn't exist
//											vTrevorsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//											OPEN_SEQUENCE_TASK(seq)
//												IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vTrevorsCoords.x,vTrevorsCoords.y, (vTrevorsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//												ENDIF
//												TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//												TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//											CLOSE_SEQUENCE_TASK(seq)
//											TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
//											CLEAR_SEQUENCE_TASK(seq)
//											//Put Trevor in players group
//											IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//												SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//											ENDIF
//											TrevorAITaskGiven = TRUE
//										ENDIF
//									
//									ELSE
//										
//										//If player is not in the heli, check to see how far away the player is.
//										//If player is close by less than 15m Trevor should just wait for the player inside the heli. 
//										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) < 15
//										
//											//Remove Trevor from players group
//											IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//												REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											ENDIF										
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//											//If heli is in the air tell Trevor to land it
//											IF IS_ENTITY_IN_AIR(HeliToSteal)
//												vTrevorsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//												GET_GROUND_Z_FOR_3D_COORD(vTrevorsCoords, fGroundzCoord)
//												TASK_HELI_MISSION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], HeliToSteal, NULL, NULL, <<vTrevorsCoords.x,vTrevorsCoords.y, fGroundzCoord>>, MISSION_GOTO, 10, 1, -1, 0, 0)
//											ENDIF
//											TrevorAITaskGiven = TRUE
//											
//										ELSE
//											
//											//If player is further than 15metres tell Trevor to get out the heli and go to the player and help him out.
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//											OPEN_SEQUENCE_TASK(seq)
//												IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//													vTrevorsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//													GET_GROUND_Z_FOR_3D_COORD(vTrevorsCoords, fGroundzCoord)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vTrevorsCoords.x,vTrevorsCoords.y, fGroundzCoord>>, MISSION_GOTO, 10, 1, -1, 0, 0)
//												ENDIF
//												TASK_LEAVE_VEHICLE(NULL, HeliToSteal, ECF_DONT_CLOSE_DOOR)
//												TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID())
//											CLOSE_SEQUENCE_TASK(seq)
//											TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
//											CLEAR_SEQUENCE_TASK(seq)	
//											//Put Trevor in players group
//											IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//												SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//											ENDIF
//											TrevorAITaskGiven = TRUE
//											
//										ENDIF
//									ENDIF
//								
//								ELSE
//									
//									//If Trevor is not the driver of the heli sort out his tasks. No need to check if Franklin is alive or in the heli here.
//									//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)									
//																			
//										//Trevor is not flying the heli and everyone is in the heli, give him rocket tasks.
//										SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//										CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//										
//										//*********GIVE Trevor ROCKET TASKS HERE*********
//										
//										//Put Trevor in players group
//										IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//											SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//										ENDIF
//										TrevorAITaskGiven = TRUE
//									
//									ELSE
//										
//										//If player is not in the heli, check to see how far away the player is.
//										//If player is close by less than 15m Trevor should just wait for the player inside the heli. 
//										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) < 15
//										
//											//Remove Trevor from players group
//											IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//												REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											ENDIF										
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//											TrevorAITaskGiven = TRUE
//											
//										ELSE
//											
//											//If player is further than 15metres tell Trevor to get out the heli and go to the player and help him out.
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//											IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//												OPEN_SEQUENCE_TASK(seq)
//													TASK_LEAVE_VEHICLE(NULL, HeliToSteal, ECF_DONT_CLOSE_DOOR)
//													TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID())
//												CLOSE_SEQUENCE_TASK(seq)
//												TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
//												CLEAR_SEQUENCE_TASK(seq)
//											ENDIF
//											//Put Trevor in players group
//											IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//												IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//													SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//												ENDIF
//											ENDIF
//											TrevorAITaskGiven = TRUE
//											
//										ENDIF
//									ENDIF									
//								ENDIF
//							
//							ELSE
//							
//								//If Trevor is not in the heli give him the following tasks
//								//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI. ONLY NEED TO CHECK IF FRANKLIN EXISTS TO SEE IF HE IS FLYING HELI HERE.
//								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)																	
//									
//									SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//									CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)	
//									
//									//Trevor is not in heli and Player is so tell Trevor to get in the heli. If the heli is on the ground.
//									IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//										OPEN_SEQUENCE_TASK(seq)
//											TASK_GO_TO_ENTITY(NULL, HeliToSteal)
//											IF GET_PED_VEHICLE_SEAT(PLAYER_PED_ID(), HeliToSteal) = VS_DRIVER
//												TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_ANY_PASSENGER)
//											ELSE
//												//Check if Franklin exists and if he is in the heli.
//												IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//													IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])	
//														IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal)	
//															IF GET_PED_VEHICLE_SEAT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal) = VS_DRIVER
//																TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_ANY_PASSENGER)
//															ELSE
//																TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//															ENDIF
//														ELSE
//															TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//														ENDIF
//													ENDIF
//												ELSE
//													TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//												ENDIF
//											ENDIF
//										CLOSE_SEQUENCE_TASK(seq)
//										TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
//										CLEAR_SEQUENCE_TASK(seq)
//										//Put Trevor in players group
//										IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//											SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//										ENDIF
//									ELSE
//										//Remove Trevor from players group
//										IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//											REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//										ENDIF
//									ENDIF
//									
//									TrevorAITaskGiven = TRUE
//												
//								ELSE
//									
//									//Michael and the Trevor are not in the heli
//									SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//									CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
//
//									//Put Trevor in players group
//									IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//										SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//									ENDIF
//									TrevorAITaskGiven = TRUE
//								
//								ENDIF
//							ENDIF
//						ENDIF	
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		//Give Franklin's AI task
//		IF FranklinAITaskGiven = FALSE
//			IF DOES_ENTITY_EXIST(HeliToSteal)	
//				IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//					
//					//Check if Franklin is inside the heli
//					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])					
//							IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal)
//								
//								//Run a check to see what seat Franklin is in.
//								//If Franklin is flying give him flying tasks.
//								IF GET_PED_VEHICLE_SEAT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal) = VS_DRIVER
//											
//									//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)									
//											
//										//Check if Trevor exists and if he is in the heli.
//										IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])	
//												IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], HeliToSteal)									
//										
//													//Franklin is flying the heli and everyone is in the heli, give him flying tasks.
//													vFranklinsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//													SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//													CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//													OPEN_SEQUENCE_TASK(seq)
//														IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vFranklinsCoords.x,vFranklinsCoords.y, (vFranklinsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//														ENDIF
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//													CLOSE_SEQUENCE_TASK(seq)
//													TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], seq)
//													CLEAR_SEQUENCE_TASK(seq)
//													//Put Franklin in players group
//													IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//														SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//													ENDIF
//													FranklinAITaskGiven = TRUE
//													
//												ELSE
//													
//													//Franklin and Michael are in the heli, but Trevor is not. IF they are on the ground they need to wait for Michael to get in. Give Franklin waiting task. If in the air they can go with out Trevor.
//													SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//													CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//													OPEN_SEQUENCE_TASK(seq)
//														IF IS_ENTITY_IN_AIR(HeliToSteal)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//														ENDIF
//													CLOSE_SEQUENCE_TASK(seq)
//													TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], seq)
//													CLEAR_SEQUENCE_TASK(seq)
//													//Put Franklin in players group
//													IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//														SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//													ENDIF
//													FranklinAITaskGiven = TRUE
//												
//												ENDIF
//										
//											ELSE
//												
//												//If Trevor doesn't exist
//												vFranklinsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//												SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//												CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//												OPEN_SEQUENCE_TASK(seq)
//													IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vFranklinsCoords.x,vFranklinsCoords.y, (vFranklinsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//													ENDIF
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//												CLOSE_SEQUENCE_TASK(seq)
//												TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], seq)
//												CLEAR_SEQUENCE_TASK(seq)
//												//Put Franklin in players group
//												IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//													SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//												ENDIF
//												FranklinAITaskGiven = TRUE
//											ENDIF
//											
//										ELSE
//											
//											//If Trevor doesn't exist
//											vFranklinsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//											OPEN_SEQUENCE_TASK(seq)
//												IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vFranklinsCoords.x,vFranklinsCoords.y, (vFranklinsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//												ENDIF
//												TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//												TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//											CLOSE_SEQUENCE_TASK(seq)
//											TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], seq)
//											CLEAR_SEQUENCE_TASK(seq)
//											//Put Franklin in players group
//											IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//												SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//											ENDIF
//											FranklinAITaskGiven = TRUE
//										ENDIF
//									
//									ELSE
//										
//										//If player is not in the heli, check to see how far away the player is.
//										//If player is close by less than 15m Franklin should just wait for the player inside the heli. 
//										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) < 15
//										
//											//Remove Franklin from players group
//											IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//												REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											ENDIF										
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//											//If heli is in the air tell Franklin to land it
//											IF IS_ENTITY_IN_AIR(HeliToSteal)
//												vFranklinsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//												GET_GROUND_Z_FOR_3D_COORD(vFranklinsCoords, fGroundzCoord)
//												TASK_HELI_MISSION(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal, NULL, NULL, <<vFranklinsCoords.x,vFranklinsCoords.y, fGroundzCoord>>, MISSION_GOTO, 10, 1, -1, 0, 0)
//											ENDIF
//											FranklinAITaskGiven = TRUE
//											
//										ELSE
//											
//											//If player is further than 15metres tell Franklin to get out the heli and go to the player and help him out.
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//											OPEN_SEQUENCE_TASK(seq)
//												IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//													vFranklinsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//													GET_GROUND_Z_FOR_3D_COORD(vFranklinsCoords, fGroundzCoord)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vFranklinsCoords.x,vFranklinsCoords.y, fGroundzCoord>>, MISSION_GOTO, 10, 1, -1, 0, 0)
//												ENDIF
//												TASK_LEAVE_VEHICLE(NULL, HeliToSteal, ECF_DONT_CLOSE_DOOR)
//												TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID())
//											CLOSE_SEQUENCE_TASK(seq)
//											TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], seq)
//											CLEAR_SEQUENCE_TASK(seq)	
//											//Put Franklin in players group
//											IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//												SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//											ENDIF
//											FranklinAITaskGiven = TRUE
//											
//										ENDIF
//									ENDIF
//								
//								ELSE
//									
//									//If Franklin is not the driver of the heli sort out his tasks. No need to check if Trevor is alive or in the heli here.
//									//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)									
//																			
//										//Franklin is not flying the heli and everyone is in the heli, give him rocket tasks.
//										SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//										CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//										
//										//*********GIVE FRANKLIN ROCKET TASKS HERE*********
//										
//										//Put Franklin in players group
//										IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//											SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//										ENDIF
//										FranklinAITaskGiven = TRUE
//									
//									ELSE
//										
//										//If player is not in the heli, check to see how far away the player is.
//										//If player is close by less than 15m Franklin should just wait for the player inside the heli. 
//										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) < 15
//										
//											//Remove Franklin from players group
//											IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//												REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											ENDIF										
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//											FranklinAITaskGiven = TRUE
//											
//										ELSE
//											
//											//If player is further than 15metres tell Franklin to get out the heli and go to the player and help him out.
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//											IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//												OPEN_SEQUENCE_TASK(seq)
//													TASK_LEAVE_VEHICLE(NULL, HeliToSteal, ECF_DONT_CLOSE_DOOR)
//													TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID())
//												CLOSE_SEQUENCE_TASK(seq)
//												TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], seq)
//												CLEAR_SEQUENCE_TASK(seq)
//											ENDIF
//											//Put Franklin in players group
//											IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//												IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//													SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//												ENDIF
//											ENDIF
//											FranklinAITaskGiven = TRUE
//											
//										ENDIF
//									ENDIF									
//								ENDIF
//							
//							ELSE
//							
//								//If Franklin is not in the heli give him the following tasks
//								//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI. ONLY NEED TO CHECK IF TREVOR EXISTS TO SEE IF HE IS FLYING HELI HERE.
//								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)																	
//									
//									SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//									CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)	
//									
//									//Franklin is not in heli and Player is so tell Franklin to get in the heli. If the heli is on the ground.
//									IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//										OPEN_SEQUENCE_TASK(seq)
//											TASK_GO_TO_ENTITY(NULL, HeliToSteal)
//											IF GET_PED_VEHICLE_SEAT(PLAYER_PED_ID(), HeliToSteal) = VS_DRIVER
//												TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_ANY_PASSENGER)
//											ELSE
//												//Check if Trevor exists and if he is in the heli.
//												IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//													IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])	
//														IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], HeliToSteal)	
//															IF GET_PED_VEHICLE_SEAT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], HeliToSteal) = VS_DRIVER
//																TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_ANY_PASSENGER)
//															ELSE
//																TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//															ENDIF
//														ELSE
//															TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//														ENDIF
//													ENDIF
//												ELSE
//													TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//												ENDIF
//											ENDIF
//										CLOSE_SEQUENCE_TASK(seq)
//										TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], seq)
//										CLEAR_SEQUENCE_TASK(seq)
//										//Put Franklin in players group
//										IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//											SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//										ENDIF
//									ELSE
//										//Remove Franklin from players group
//										IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//											REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//										ENDIF
//									ENDIF
//									
//									FranklinAITaskGiven = TRUE
//												
//								ELSE
//									
//									//Franklin and the Trevor are not in the heli
//									SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//									CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
//
//									//Put Franklin in players group
//									IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//										SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_GROUP_ID())
//									ENDIF
//									FranklinAITaskGiven = TRUE
//								
//								ENDIF
//							ENDIF
//						ENDIF	
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		
//	ENDIF	
//	
//	
//	//Control Trevor and Michael's AI if playing as Franklin
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//				
//		//Give Trevor AI task
//		IF TrevorAITaskGiven = FALSE
//			IF DOES_ENTITY_EXIST(HeliToSteal)	
//				IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//					
//					//Check if Trevor is inside the heli
//					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])					
//							IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], HeliToSteal)
//								
//								//Run a check to see what seat Trevor is in.
//								//If Trevor is flying give him flying tasks.
//								IF GET_PED_VEHICLE_SEAT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], HeliToSteal) = VS_DRIVER
//											
//									//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)									
//											
//										//Check if Franklin exists and if he is in the heli.
//										IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//											IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])	
//												IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal)									
//										
//													//Trevor is flying the heli and everyone is in the heli, give him flying tasks.
//													vTrevorsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//													SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//													CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//													OPEN_SEQUENCE_TASK(seq)
//														IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vTrevorsCoords.x,vTrevorsCoords.y, (vTrevorsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//														ENDIF
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//													CLOSE_SEQUENCE_TASK(seq)
//													TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
//													CLEAR_SEQUENCE_TASK(seq)
//													//Put Trevor in players group
//													IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//														SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//													ENDIF
//													TrevorAITaskGiven = TRUE
//													
//												ELSE
//													
//													//Franklin and Trevor are in the heli, but Michael is not. IF they are on the ground they need to wait for Franklin to get in. Give Trevor waiting task. If in the air they can go with out Franklin.
//													SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//													CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//													OPEN_SEQUENCE_TASK(seq)
//														IF IS_ENTITY_IN_AIR(HeliToSteal)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//														ENDIF
//													CLOSE_SEQUENCE_TASK(seq)
//													TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
//													CLEAR_SEQUENCE_TASK(seq)
//													//Put Trevor in players group
//													IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//														SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//													ENDIF
//													TrevorAITaskGiven = TRUE
//												
//												ENDIF
//											
//											ELSE
//												
//												//If Franklin doesn't exist
//												vTrevorsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//												SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//												CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//												OPEN_SEQUENCE_TASK(seq)
//													IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vTrevorsCoords.x,vTrevorsCoords.y, (vTrevorsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//													ENDIF
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//												CLOSE_SEQUENCE_TASK(seq)
//												TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
//												CLEAR_SEQUENCE_TASK(seq)
//												//Put Trevor in players group
//												IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//													SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//												ENDIF
//												TrevorAITaskGiven = TRUE
//											ENDIF
//											
//										ELSE
//											
//											//If Franklin doesn't exist
//											vTrevorsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//											OPEN_SEQUENCE_TASK(seq)
//												IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vTrevorsCoords.x,vTrevorsCoords.y, (vTrevorsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//												ENDIF
//												TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//												TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//											CLOSE_SEQUENCE_TASK(seq)
//											TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
//											CLEAR_SEQUENCE_TASK(seq)
//											//Put Trevor in players group
//											IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//												SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//											ENDIF
//											TrevorAITaskGiven = TRUE
//										ENDIF
//									
//									ELSE
//										
//										//If player is not in the heli, check to see how far away the player is.
//										//If player is close by less than 15m Trevor should just wait for the player inside the heli. 
//										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) < 15
//										
//											//Remove Trevor from players group
//											IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//												REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											ENDIF										
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//											//If heli is in the air tell Trevor to land it
//											IF IS_ENTITY_IN_AIR(HeliToSteal)
//												vTrevorsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//												GET_GROUND_Z_FOR_3D_COORD(vTrevorsCoords, fGroundzCoord)
//												TASK_HELI_MISSION(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], HeliToSteal, NULL, NULL, <<vTrevorsCoords.x,vTrevorsCoords.y, fGroundzCoord>>, MISSION_GOTO, 10, 1, -1, 0, 0)
//											ENDIF
//											TrevorAITaskGiven = TRUE
//											
//										ELSE
//											
//											//If player is further than 15metres tell Trevor to get out the heli and go to the player and help him out.
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//											OPEN_SEQUENCE_TASK(seq)
//												IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//													vTrevorsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//													GET_GROUND_Z_FOR_3D_COORD(vTrevorsCoords, fGroundzCoord)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vTrevorsCoords.x,vTrevorsCoords.y, fGroundzCoord>>, MISSION_GOTO, 10, 1, -1, 0, 0)
//												ENDIF
//												TASK_LEAVE_VEHICLE(NULL, HeliToSteal, ECF_DONT_CLOSE_DOOR)
//												TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID())
//											CLOSE_SEQUENCE_TASK(seq)
//											TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
//											CLEAR_SEQUENCE_TASK(seq)	
//											//Put Trevor in players group
//											IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//												SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//											ENDIF
//											TrevorAITaskGiven = TRUE
//											
//										ENDIF
//									ENDIF
//								
//								ELSE
//									
//									//If Trevor is not the driver of the heli sort out his tasks. No need to check if Franklin is alive or in the heli here.
//									//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)									
//																			
//										//Trevor is not flying the heli and everyone is in the heli, give him rocket tasks.
//										SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//										CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//										
//										//*********GIVE Trevor ROCKET TASKS HERE*********
//										
//										//Put Trevor in players group
//										IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//											SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//										ENDIF
//										TrevorAITaskGiven = TRUE
//									
//									ELSE
//										
//										//If player is not in the heli, check to see how far away the player is.
//										//If player is close by less than 15m Trevor should just wait for the player inside the heli. 
//										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) < 15
//										
//											//Remove Trevor from players group
//											IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//												REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											ENDIF										
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//											TrevorAITaskGiven = TRUE
//											
//										ELSE
//											
//											//If player is further than 15metres tell Trevor to get out the heli and go to the player and help him out.
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//											IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//												OPEN_SEQUENCE_TASK(seq)
//													TASK_LEAVE_VEHICLE(NULL, HeliToSteal, ECF_DONT_CLOSE_DOOR)
//													TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID())
//												CLOSE_SEQUENCE_TASK(seq)
//												TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
//												CLEAR_SEQUENCE_TASK(seq)
//											ENDIF
//											//Put Trevor in players group
//											IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//												IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//													SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//												ENDIF
//											ENDIF
//											TrevorAITaskGiven = TRUE
//											
//										ENDIF
//									ENDIF									
//								ENDIF
//							
//							ELSE
//							
//								//If Trevor is not in the heli give him the following tasks
//								//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI. ONLY NEED TO CHECK IF FRANKLIN EXISTS TO SEE IF HE IS FLYING HELI HERE.
//								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)																	
//									
//									SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//									CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)	
//									
//									//Trevor is not in heli and Player is so tell Trevor to get in the heli. If the heli is on the ground.
//									IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//										OPEN_SEQUENCE_TASK(seq)
//											TASK_GO_TO_ENTITY(NULL, HeliToSteal)
//											IF GET_PED_VEHICLE_SEAT(PLAYER_PED_ID(), HeliToSteal) = VS_DRIVER
//												TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_ANY_PASSENGER)
//											ELSE
//												//Check if Franklin exists and if he is in the heli.
//												IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//													IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])	
//														IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal)	
//															IF GET_PED_VEHICLE_SEAT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal) = VS_DRIVER
//																TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_ANY_PASSENGER)
//															ELSE
//																TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//															ENDIF
//														ELSE
//															TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//														ENDIF
//													ENDIF
//												ELSE
//													TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//												ENDIF
//											ENDIF
//										CLOSE_SEQUENCE_TASK(seq)
//										TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
//										CLEAR_SEQUENCE_TASK(seq)
//										//Put Trevor in players group
//										IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//											SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//										ENDIF
//									ELSE
//										//Remove Trevor from players group
//										IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//											REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//										ENDIF
//									ENDIF
//									
//									TrevorAITaskGiven = TRUE
//												
//								ELSE
//									
//									//Franklin and the Trevor are not in the heli
//									SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//									CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
//
//									//Put Trevor in players group
//									IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//										SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//									ENDIF
//									TrevorAITaskGiven = TRUE
//								
//								ENDIF
//							ENDIF
//						ENDIF	
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		//Give Michaels AI task
//		IF MichaelAITaskGiven = FALSE
//			IF DOES_ENTITY_EXIST(HeliToSteal)	
//				IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//					
//					//Check if Michael is inside the heli
//					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])					
//							IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], HeliToSteal)
//								
//								//Run a check to see what seat Michael is in.
//								//If Michael is flying give him flying tasks.
//								IF GET_PED_VEHICLE_SEAT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], HeliToSteal) = VS_DRIVER
//											
//									//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)									
//											
//										//Check if Trevor exists and if he is in the heli.
//										IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//											IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])	
//												IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], HeliToSteal)									
//										
//													//Michael is flying the heli and everyone is in the heli, give him flying tasks.
//													vMichaelsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//													SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//													CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//													OPEN_SEQUENCE_TASK(seq)
//														IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vMichaelsCoords.x,vMichaelsCoords.y, (vMichaelsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//														ENDIF
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//													CLOSE_SEQUENCE_TASK(seq)
//													TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seq)
//													CLEAR_SEQUENCE_TASK(seq)
//													//Put Michael in players group
//													IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//														SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//													ENDIF
//													MichaelAITaskGiven = TRUE
//													
//												ELSE
//													
//													//Michael and Franklin are in the heli, but Trevor is not. IF they are on the ground they need to wait for Trevor to get in. Give Michael waiting task. If in the air they can go with out Trevor.
//													SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//													CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//													SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//													OPEN_SEQUENCE_TASK(seq)
//														IF IS_ENTITY_IN_AIR(HeliToSteal)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//															TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//														ENDIF
//													CLOSE_SEQUENCE_TASK(seq)
//													TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seq)
//													CLEAR_SEQUENCE_TASK(seq)
//													//Put Michael in players group
//													IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//														SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//													ENDIF
//													MichaelAITaskGiven = TRUE
//												
//												ENDIF
//											
//											ELSE
//												
//												//If Trevor doesn't exist
//												vMichaelsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//												SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//												CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//												SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//												OPEN_SEQUENCE_TASK(seq)
//													IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//														TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vMichaelsCoords.x,vMichaelsCoords.y, (vMichaelsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//													ENDIF
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//												CLOSE_SEQUENCE_TASK(seq)
//												TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seq)
//												CLEAR_SEQUENCE_TASK(seq)
//												//Put Michael in players group
//												IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//													SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//												ENDIF
//												MichaelAITaskGiven = TRUE
//											ENDIF
//											
//										ELSE
//											
//											//If Trevor doesn't exist
//											vMichaelsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//											OPEN_SEQUENCE_TASK(seq)
//												IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vMichaelsCoords.x,vMichaelsCoords.y, (vMichaelsCoords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 240, 50)
//												ENDIF
//												TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<1057, 3084, 200>>, MISSION_GOTO, 30, 2, -1, 240, 50)
//												TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, vHanger, MISSION_GOTO, 30, 2, -1, 240, 50)
//											CLOSE_SEQUENCE_TASK(seq)
//											TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seq)
//											CLEAR_SEQUENCE_TASK(seq)
//											//Put Michael in players group
//											IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//												SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//											ENDIF
//											MichaelAITaskGiven = TRUE
//										ENDIF
//									
//									ELSE
//										
//										//If player is not in the heli, check to see how far away the player is.
//										//If player is close by less than 15m Michael should just wait for the player inside the heli. 
//										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]) < 15
//										
//											//Remove Michael from players group
//											IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//												REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											ENDIF										
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//											//If heli is in the air tell Michael to land it
//											IF IS_ENTITY_IN_AIR(HeliToSteal)
//												vMichaelsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//												GET_GROUND_Z_FOR_3D_COORD(vMichaelsCoords, fGroundzCoord)
//												TASK_HELI_MISSION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], HeliToSteal, NULL, NULL, <<vMichaelsCoords.x,vMichaelsCoords.y, fGroundzCoord>>, MISSION_GOTO, 10, 1, -1, 0, 0)
//											ENDIF
//											MichaelAITaskGiven = TRUE
//											
//										ELSE
//											
//											//If player is further than 15metres tell Michael to get out the heli and go to the player and help him out.
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//											OPEN_SEQUENCE_TASK(seq)
//												IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//													vMichaelsCoords = GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//													GET_GROUND_Z_FOR_3D_COORD(vMichaelsCoords, fGroundzCoord)
//													TASK_HELI_MISSION(NULL, HeliToSteal, NULL, NULL, <<vMichaelsCoords.x,vMichaelsCoords.y, fGroundzCoord>>, MISSION_GOTO, 10, 1, -1, 0, 0)
//												ENDIF
//												TASK_LEAVE_VEHICLE(NULL, HeliToSteal, ECF_DONT_CLOSE_DOOR)
//												TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID())
//											CLOSE_SEQUENCE_TASK(seq)
//											TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seq)
//											CLEAR_SEQUENCE_TASK(seq)	
//											//Put Michael in players group
//											IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//												SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//											ENDIF
//											MichaelAITaskGiven = TRUE
//											
//										ENDIF
//									ENDIF
//								
//								ELSE
//									
//									//If Michael is not the driver of the heli sort out his tasks. No need to check if Trevor is alive or in the heli here.
//									//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)									
//																			
//										//Michael is not flying the heli and everyone is in the heli, give him rocket tasks.
//										SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//										CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//										SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//										
//										//*********GIVE MICHAEL ROCKET TASKS HERE*********
//										
//										//Put Michael in players group
//										IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//											SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//										ENDIF
//										MichaelAITaskGiven = TRUE
//									
//									ELSE
//										
//										//If player is not in the heli, check to see how far away the player is.
//										//If player is close by less than 15m Michael should just wait for the player inside the heli. 
//										IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]) < 15
//										
//											//Remove Michael from players group
//											IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//												REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											ENDIF										
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//											MichaelAITaskGiven = TRUE
//											
//										ELSE
//											
//											//If player is further than 15metres tell Michael to get out the heli and go to the player and help him out.
//											SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//											CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//											IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//												OPEN_SEQUENCE_TASK(seq)
//													TASK_LEAVE_VEHICLE(NULL, HeliToSteal, ECF_DONT_CLOSE_DOOR)
//													TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID())
//												CLOSE_SEQUENCE_TASK(seq)
//												TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seq)
//												CLEAR_SEQUENCE_TASK(seq)
//											ENDIF
//											//Put Michael in players group
//											IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//												IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//													SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//												ENDIF
//											ENDIF
//											MichaelAITaskGiven = TRUE
//											
//										ENDIF
//									ENDIF									
//								ENDIF
//							
//							ELSE
//							
//								//If Michael is not in the heli give him the following tasks
//								//UPDATE TASKS FOR BUDDY'S IF PLAYER IS INSIDE THE HELI. ONLY NEED TO CHECK IF TREVOR EXISTS TO SEE IF HE IS FLYING HELI HERE.
//								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)																	
//									
//									SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//									CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)	
//									
//									//Michael is not in heli and Player is so tell Michael to get in the heli. If the heli is on the ground.
//									IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
//										OPEN_SEQUENCE_TASK(seq)
//											TASK_GO_TO_ENTITY(NULL, HeliToSteal)
//											IF GET_PED_VEHICLE_SEAT(PLAYER_PED_ID(), HeliToSteal) = VS_DRIVER
//												TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_ANY_PASSENGER)
//											ELSE
//												//Check if Trevor exists and if he is in the heli.
//												IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//													IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])	
//														IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], HeliToSteal)	
//															IF GET_PED_VEHICLE_SEAT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], HeliToSteal) = VS_DRIVER
//																TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_ANY_PASSENGER)
//															ELSE
//																TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//															ENDIF
//														ELSE
//															TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//														ENDIF
//													ENDIF
//												ELSE
//													TASK_ENTER_VEHICLE(NULL, HeliToSteal, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER)
//												ENDIF
//											ENDIF
//										CLOSE_SEQUENCE_TASK(seq)
//										TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], seq)
//										CLEAR_SEQUENCE_TASK(seq)
//										//Put Michael in players group
//										IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//											SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//										ENDIF
//									ELSE
//										//Remove Michael from players group
//										IF IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//											REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//										ENDIF
//									ENDIF
//									
//									MichaelAITaskGiven = TRUE
//												
//								ELSE
//									
//									//Michael and the Trevor are not in the heli
//									SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//									CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
//
//									//Put Michael in players group
//									IF NOT IS_PED_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//										SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//									ENDIF
//									MichaelAITaskGiven = TRUE
//								
//								ENDIF
//							ENDIF
//						ENDIF	
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		
//	ENDIF	
//
//ENDPROC

//PURPOSE:Deals with the custom camera's for switching from the player to the chosen character at close distance < 400m.
FUNC BOOL SETUP_HOTSWAP_CAM_FROM_PLAYER_CLOSE(SELECTOR_SLOTS_ENUM eSelectedPed)
//	PRINTSTRING("*********************** SETUP_HOTSWAP_CAM_FROM_TREVOR_TO_FRANKLIN_CLOSE IS BEING CALLED ***********************") PRINTNL()
	VECTOR vGamePlayCamRot = GET_GAMEPLAY_CAM_ROT()
	
	IF NOT sCamDetails.bSplineCreated
		
		//Create main spline camera
		IF NOT DOES_CAM_EXIST(sCamDetails.camID)
			sCamDetails.camID = CREATE_CAM("DEFAULT_SPLINE_CAMERA", FALSE) 
		ENDIF  
		SET_CAM_FOV(sCamDetails.camID, SELECTOR_CAM_DEFAULT_FOV)
		
		//Create Node Camera's
		IF NOT DOES_CAM_EXIST(NodeCam[0])
			NodeCam[0] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", true)
		ENDIF 
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			ATTACH_CAM_TO_ENTITY(NodeCam[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), << 0,0,20>>)
			SET_CAM_ROT(NodeCam[0], <<-88.5, 0, vGamePlayCamRot.z>>)
			SET_CAM_FOV(NodeCam[0], SELECTOR_CAM_DEFAULT_FOV)
		ELSE
			ATTACH_CAM_TO_ENTITY(NodeCam[0], PLAYER_PED_ID(), << 0,0,5>>)
			SET_CAM_ROT(NodeCam[0], <<-88.5, 0, vGamePlayCamRot.z>>)
			SET_CAM_FOV(NodeCam[0], SELECTOR_CAM_DEFAULT_FOV)			
		ENDIF
		
		//The high point above Trevor
		IF NOT DOES_CAM_EXIST(NodeCam[1])
			NodeCam[1] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", true)
		ENDIF 
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[eSelectedPed])
			IF IS_PED_IN_ANY_VEHICLE(sSelectorPeds.pedID[eSelectedPed])
				ATTACH_CAM_TO_ENTITY(NodeCam[1], GET_VEHICLE_PED_IS_IN(sSelectorPeds.pedID[eSelectedPed]), <<0.0, 0.0, 25>>, TRUE)
				SET_CAM_ROT(NodeCam[1], <<-88.5, 0, vGamePlayCamRot.z>>)
				SET_CAM_FOV(NodeCam[1], SELECTOR_CAM_DEFAULT_FOV)			
			ELSE
				ATTACH_CAM_TO_ENTITY(NodeCam[1], sSelectorPeds.pedID[eSelectedPed], <<0.0, 0.0, 5>>, TRUE)
				SET_CAM_ROT(NodeCam[1], <<-88.5, 0, vGamePlayCamRot.z>>)
				SET_CAM_FOV(NodeCam[1], SELECTOR_CAM_DEFAULT_FOV)
			ENDIF
		ENDIF
		
		
		//Add the nodes in order
		ADD_CAM_SPLINE_NODE(sCamDetails.camID, GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), 0, CAM_SPLINE_NODE_SMOOTH_ROT)//starting with gamecam coords and rotation
		ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, NodeCam[0], 500, CAM_SPLINE_NODE_SMOOTH_ROT)//5m's above player
		ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, NodeCam[1], 2000, CAM_SPLINE_NODE_SMOOTH_ROT)//high above player
		ADD_CAM_SPLINE_NODE_USING_GAMEPLAY_FRAME(sCamDetails.camID, 500)//Add the final camera node attached to new player model
				
		
		sCamDetails.bSplineCreated = TRUE
		sCamDetails.camType = SELECTOR_CAM_DEFAULT
		
		sCamDetails.pedTo = sSelectorPeds.pedID[eSelectedPed]
		
		RETURN TRUE
		
	ENDIF 
	
	RETURN FALSE 	

ENDFUNC


////PURPOSE: Handles all the switching between Trevor and Michael
//PROC HANDLE_SWITCHING()
//	
//	//When playing as Michael
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//				TrevorSwitchAvailable = TRUE
//			ELSE
//				TrevorSwitchAvailable = FALSE
//			ENDIF
//		ELSE
//			TrevorSwitchAvailable = FALSE
//		ENDIF
//		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//				FranklinSwitchAvailable = TRUE
//			ELSE
//				FranklinSwitchAvailable = FALSE
//			ENDIF
//		ELSE
//			FranklinSwitchAvailable = FALSE
//		ENDIF
//	ENDIF
//	
//	//When playing as Trevor
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//				MichaelSwitchAvailable = TRUE
//			ELSE
//				MichaelSwitchAvailable = FALSE
//			ENDIF
//		ELSE
//			MichaelSwitchAvailable = FALSE
//		ENDIF
//		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//				FranklinSwitchAvailable = TRUE
//			ELSE
//				FranklinSwitchAvailable = FALSE
//			ENDIF
//		ELSE
//			FranklinSwitchAvailable = FALSE
//		ENDIF
//	ENDIF
//	
//	//When playing as Franklin
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//				TrevorSwitchAvailable = TRUE
//			ELSE
//				TrevorSwitchAvailable = FALSE
//			ENDIF
//		ELSE
//			TrevorSwitchAvailable = FALSE
//		ENDIF
//		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//				MichaelSwitchAvailable = TRUE
//			ELSE
//				MichaelSwitchAvailable = FALSE
//			ENDIF
//		ELSE
//			MichaelSwitchAvailable = FALSE
//		ENDIF
//	ENDIF
//	
//	//Check for hotswapping between Michael to Trevor or franklin.
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//		IF TrevorSwitchAvailable = TRUE
//		OR FranklinSwitchAvailable = TRUE
//			IF NOT sCamDetails.bRun
//				IF UPDATE_SELECTOR_HUD(sSelectorPeds)
//					IF HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_TREVOR)	
//						IF GET_DISTANCE_BETWEEN_ENTITIES(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_PED_ID()) < 400
//							IF SETUP_HOTSWAP_CAM_FROM_PLAYER_CLOSE(SELECTOR_PED_TREVOR)
//								sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
//								sCamDetails.bRun  = TRUE
//								LongDistanceCamRequired = FALSE
//							ENDIF
//						ELSE
//							sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
//							sCamDetails.bRun  = TRUE	
//							LongDistanceCamRequired = TRUE
//						ENDIF
//					ENDIF
//					IF HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_FRANKLIN)
//						IF GET_DISTANCE_BETWEEN_ENTITIES(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_PED_ID()) < 400
//							IF SETUP_HOTSWAP_CAM_FROM_PLAYER_CLOSE(SELECTOR_PED_FRANKLIN)
//								sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
//								sCamDetails.bRun  = TRUE
//								LongDistanceCamRequired = FALSE
//							ENDIF
//						ELSE				
//							sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
//							sCamDetails.bRun  = TRUE
//							LongDistanceCamRequired = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ELSE
//				IF LongDistanceCamRequired = TRUE
//					IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_LONG_RANGE(sCamDetails)	// Returns FALSE when the camera spline is complete
//						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//						AND IS_MESSAGE_BEING_DISPLAYED()
//							CLEAR_PRINTS()
//						ENDIF
//						//Move Trev into safe position if he was selected at close range
//						IF sCamDetails.bOKToSwitchPed
//							IF NOT sCamDetails.bPedSwitched
//								IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
//									
//									//Reset flags for AI controller
//									MichaelAITaskGiven = FALSE
//									FranklinAITaskGiven = FALSE
//									TrevorAITaskGiven = FALSE
//									IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//										SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//										SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
//										SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], RELGROUPHASH_PLAYER)
//										SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE, FALSE, TRUE, FALSE, FALSE)
//										SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 100)
//									ENDIF
//									SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
//									
//									sCamDetails.bPedSwitched = TRUE
//									
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF		
//				ELSE
//					IF RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(sCamDetails, 370, 370, SELECTOR_CAM_DEFAULT, 0)	// Returns FALSE when the camera spline is complete					
//						
//						SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sCamDetails.pedTo)
//						IF sCamDetails.bOKToSwitchPed
//							IF NOT sCamDetails.bPedSwitched
//								IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)					
//									
//									//Reset flags for AI controller
//									MichaelAITaskGiven = FALSE
//									FranklinAITaskGiven = FALSE
//									TrevorAITaskGiven = FALSE
//									IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//										SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
//										SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
//										SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], RELGROUPHASH_PLAYER)
//										SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE, FALSE, TRUE, FALSE, FALSE)
//										SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 100)
//									ENDIF
//									SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
//									
//									sCamDetails.bPedSwitched = TRUE
//									
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF	
//	ENDIF	
//	
//	//Check for hotswapping between Trevor to Michael or franklin.
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//		IF MichaelSwitchAvailable = TRUE
//		OR FranklinSwitchAvailable = TRUE
//			IF NOT sCamDetails.bRun
//				IF UPDATE_SELECTOR_HUD(sSelectorPeds)
//					IF HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MICHAEL)	
//						IF GET_DISTANCE_BETWEEN_ENTITIES(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_PED_ID()) < 400
//							IF SETUP_HOTSWAP_CAM_FROM_PLAYER_CLOSE(SELECTOR_PED_MICHAEL)
//								sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
//								sCamDetails.bRun  = TRUE
//								LongDistanceCamRequired = FALSE
//							ENDIF
//						ELSE
//							sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
//							sCamDetails.bRun  = TRUE	
//							LongDistanceCamRequired = TRUE
//						ENDIF
//					ENDIF
//					IF HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_FRANKLIN)
//						IF GET_DISTANCE_BETWEEN_ENTITIES(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PLAYER_PED_ID()) < 400
//							IF SETUP_HOTSWAP_CAM_FROM_PLAYER_CLOSE(SELECTOR_PED_FRANKLIN)
//								sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
//								sCamDetails.bRun  = TRUE
//								LongDistanceCamRequired = FALSE
//							ENDIF
//						ELSE				
//							sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
//							sCamDetails.bRun  = TRUE
//							LongDistanceCamRequired = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ELSE
//				IF LongDistanceCamRequired = TRUE
//					IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_LONG_RANGE(sCamDetails)	// Returns FALSE when the camera spline is complete
//						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//						AND IS_MESSAGE_BEING_DISPLAYED()
//							CLEAR_PRINTS()
//						ENDIF
//						
//						IF sCamDetails.bOKToSwitchPed
//							IF NOT sCamDetails.bPedSwitched
//								IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
//									
//									//Reset flags for AI controller
//									MichaelAITaskGiven = FALSE
//									FranklinAITaskGiven = FALSE
//									TrevorAITaskGiven = FALSE
//									IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//										SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//										SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
//										SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], RELGROUPHASH_PLAYER)
//										SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE, FALSE, TRUE, FALSE, FALSE)
//										SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 100)
//									ENDIF
//									SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
//									
//									sCamDetails.bPedSwitched = TRUE
//									
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF		
//				ELSE
//					IF RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(sCamDetails, 370, 370, SELECTOR_CAM_DEFAULT, 0)	// Returns FALSE when the camera spline is complete					
//						
//						SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sCamDetails.pedTo)
//						IF sCamDetails.bOKToSwitchPed
//							IF NOT sCamDetails.bPedSwitched
//								IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)					
//									
//									//Reset flags for AI controller
//									MichaelAITaskGiven = FALSE
//									FranklinAITaskGiven = FALSE
//									TrevorAITaskGiven = FALSE
//									IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//										SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
//										SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
//										SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], RELGROUPHASH_PLAYER)
//										SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE, FALSE, TRUE, FALSE, FALSE)
//										SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 100)
//									ENDIF
//									SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
//									
//									sCamDetails.bPedSwitched = TRUE
//									
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF	
//	ENDIF	
//	
//	//Check for hotswapping between franklin to Trevor or Michael.
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//		IF TrevorSwitchAvailable = TRUE
//		OR FranklinSwitchAvailable = TRUE
//			IF NOT sCamDetails.bRun
//				IF UPDATE_SELECTOR_HUD(sSelectorPeds)
//					IF HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_TREVOR)	
//						IF GET_DISTANCE_BETWEEN_ENTITIES(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_PED_ID()) < 400
//							IF SETUP_HOTSWAP_CAM_FROM_PLAYER_CLOSE(SELECTOR_PED_TREVOR)
//								sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
//								sCamDetails.bRun  = TRUE
//								LongDistanceCamRequired = FALSE
//							ENDIF
//						ELSE
//							sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
//							sCamDetails.bRun  = TRUE	
//							LongDistanceCamRequired = TRUE
//						ENDIF
//					ENDIF
//					IF HAS_SELECTOR_PED_BEEN_SELECTED(sSelectorPeds, SELECTOR_PED_MICHAEL)
//						IF GET_DISTANCE_BETWEEN_ENTITIES(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_PED_ID()) < 400
//							IF SETUP_HOTSWAP_CAM_FROM_PLAYER_CLOSE(SELECTOR_PED_MICHAEL)
//								sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
//								sCamDetails.bRun  = TRUE
//								LongDistanceCamRequired = FALSE
//							ENDIF
//						ELSE				
//							sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
//							sCamDetails.bRun  = TRUE
//							LongDistanceCamRequired = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ELSE
//				IF LongDistanceCamRequired = TRUE
//					IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED_LONG_RANGE(sCamDetails)	// Returns FALSE when the camera spline is complete
//						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//						AND IS_MESSAGE_BEING_DISPLAYED()
//							CLEAR_PRINTS()
//						ENDIF
//						//Move Trev into safe position if he was selected at close range
//						IF sCamDetails.bOKToSwitchPed
//							IF NOT sCamDetails.bPedSwitched
//								IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
//									
//									//Reset flags for AI controller
//									MichaelAITaskGiven = FALSE
//									FranklinAITaskGiven = FALSE
//									TrevorAITaskGiven = FALSE
//										
//									IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//										SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//										SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
//										SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], RELGROUPHASH_PLAYER)
//										SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE, FALSE, TRUE, FALSE, FALSE)
//										SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], 100)
//									ENDIF
//									SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
//									
//									sCamDetails.bPedSwitched = TRUE
//									
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF		
//				ELSE
//					IF RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(sCamDetails, 370, 370, SELECTOR_CAM_DEFAULT, 0)	// Returns FALSE when the camera spline is complete					
//						
//						SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sCamDetails.pedTo)
//						IF sCamDetails.bOKToSwitchPed
//							IF NOT sCamDetails.bPedSwitched
//								IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)					
//									
//									//Reset flags for AI controller
//									MichaelAITaskGiven = FALSE
//									FranklinAITaskGiven = FALSE
//									TrevorAITaskGiven = FALSE
//									
//									IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//										SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
//										SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
//										SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], RELGROUPHASH_PLAYER)
//										SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE, FALSE, TRUE, FALSE, FALSE)
//										SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], 100)
//									ENDIF
//									SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
//									
//									sCamDetails.bPedSwitched = TRUE
//									
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF	
//	ENDIF	
//
//ENDPROC

////PURPOSE: Controls everything for player buddies. Calling / Switching / updating tasks etc
//PROC PLAYER_BUDDY_CONTRLER()
//	
//	HANDLE_SWITCHING()
//		
//	HANDLE_PLAYER_CHARACTERS()	
//	
//	//Check who the player is first
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//		//Check if he is calling Trevor
//		IF IS_CALLING_CONTACT(CHAR_TREVOR)
//			//Do dialogue here 
//			//If dialogue starts flag that player has called this buddy for help so they have to come.
//			TrevorIsNeeded = TRUE
//		ENDIF
//		
//		IF TrevorIsNeeded = TRUE
//			IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
//					//Create Trevor, check whether the player is inside a vehicle or not.
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_FRONT_RIGHT)
//							WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_FRONT_RIGHT)
//								PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//								WAIT(0)
//							ENDWHILE
//						ELSE
//							IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//								WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//									PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//									WAIT(0)
//								ENDWHILE 
//							ELSE
//								IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT)
//									WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT)
//										PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//										WAIT(0)
//									ENDWHILE 
//								ELSE
//									WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, <<vPlayerCoords.x, vPlayerCoords.y + 2, vPlayerCoords.z>>, GET_ENTITY_HEADING(PLAYER_PED_ID()))
//										PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//										WAIT(0)
//									ENDWHILE
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, <<vPlayerCoords.x, vPlayerCoords.y + 2, vPlayerCoords.z>>, GET_ENTITY_HEADING(PLAYER_PED_ID()))
//							PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//							WAIT(0)
//						ENDWHILE
//					ENDIF
//				ENDIF
//				
//				GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], WEAPONTYPE_SMG, 2000, TRUE)
//				TrevorAITaskGiven = FALSE
//			ENDIF
//		ENDIF
//		
//		//Check if he is calling Franklin
//		IF IS_CALLING_CONTACT(CHAR_FRANKLIN)
//			//Do dialogue here 
//			//If dialogue starts flag that player has called this buddy for help so they have to come.
//			FranklinIsNeeded = TRUE
//		ENDIF
//		
//		IF FranklinIsNeeded = TRUE
//			IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
//					//Create Trevor, check whether the player is inside a vehicle or not.
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_FRONT_RIGHT)
//							WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_FRONT_RIGHT)
//								PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//								WAIT(0)
//							ENDWHILE
//						ELSE
//							IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//								WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//									PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//									WAIT(0)
//								ENDWHILE 
//							ELSE
//								IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT)
//									WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT)
//										PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//										WAIT(0)
//									ENDWHILE 
//								ELSE
//									WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, <<vPlayerCoords.x, vPlayerCoords.y + 2, vPlayerCoords.z>>, GET_ENTITY_HEADING(PLAYER_PED_ID()))
//										PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//										WAIT(0)
//									ENDWHILE
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, <<vPlayerCoords.x, vPlayerCoords.y + 2, vPlayerCoords.z>>, GET_ENTITY_HEADING(PLAYER_PED_ID()))
//							PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//							WAIT(0)
//						ENDWHILE
//					ENDIF
//				ENDIF
//				GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], WEAPONTYPE_SMG, 2000, TRUE)
//				FranklinAITaskGiven = FALSE
//			ENDIF
//		ENDIF	
//	ENDIF
//	
//	
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//		//Check if he is calling Michael
//		IF IS_CALLING_CONTACT(CHAR_MICHAEL)
//			//Do dialogue here 
//			//If dialogue starts flag that player has called this buddy for help so they have to come.
//			MichaelIsNeeded = TRUE
//		ENDIF
//		
//		IF MichaelIsNeeded = TRUE
//			IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
//					//Create Trevor, check whether the player is inside a vehicle or not.
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_FRONT_RIGHT)
//							WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_FRONT_RIGHT)
//								PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//								WAIT(0)
//							ENDWHILE
//						ELSE
//							IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//								WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//									PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//									WAIT(0)
//								ENDWHILE 
//							ELSE
//								IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT)
//									WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT)
//										PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//										WAIT(0)
//									ENDWHILE 
//								ELSE
//									WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<vPlayerCoords.x, vPlayerCoords.y + 2, vPlayerCoords.z>>, GET_ENTITY_HEADING(PLAYER_PED_ID()))
//										PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//										WAIT(0)
//									ENDWHILE
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<vPlayerCoords.x, vPlayerCoords.y + 2, vPlayerCoords.z>>, GET_ENTITY_HEADING(PLAYER_PED_ID()))
//							PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//							WAIT(0)
//						ENDWHILE
//					ENDIF
//				ENDIF
//				GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_SMG, 2000, TRUE)
//				MichaelAITaskGiven = FALSE
//			ENDIF
//		ENDIF
//		
//		//Check if he is calling Franklin
//		IF IS_CALLING_CONTACT(CHAR_FRANKLIN)
//			//Do dialogue here 
//			//If dialogue starts flag that player has called this buddy for help so they have to come.
//			FranklinIsNeeded = TRUE
//		ENDIF
//		
//		IF FranklinIsNeeded = TRUE
//			IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
//					//Create Trevor, check whether the player is inside a vehicle or not.
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_FRONT_RIGHT)
//							WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_FRONT_RIGHT)
//								PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//								WAIT(0)
//							ENDWHILE
//						ELSE
//							IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//								WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//									PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//									WAIT(0)
//								ENDWHILE 
//							ELSE
//								IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT)
//									WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT)
//										PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//										WAIT(0)
//									ENDWHILE 
//								ELSE
//									WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, <<vPlayerCoords.x, vPlayerCoords.y + 2, vPlayerCoords.z>>, GET_ENTITY_HEADING(PLAYER_PED_ID()))
//										PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//										WAIT(0)
//									ENDWHILE
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, <<vPlayerCoords.x, vPlayerCoords.y + 2, vPlayerCoords.z>>, GET_ENTITY_HEADING(PLAYER_PED_ID()))
//							PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//							WAIT(0)
//						ENDWHILE
//					ENDIF
//				ENDIF
//				GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], WEAPONTYPE_SMG, 2000, TRUE)
//				FranklinAITaskGiven = FALSE
//			ENDIF
//		ENDIF	
//	ENDIF
//	
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//		//Check if he is calling Trevor
//		IF IS_CALLING_CONTACT(CHAR_TREVOR)
//			//Do dialogue here 
//			//If dialogue starts flag that player has called this buddy for help so they have to come.
//			TrevorIsNeeded = TRUE
//		ENDIF
//		
//		IF TrevorIsNeeded = TRUE
//			IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
//					//Create Trevor, check whether the player is inside a vehicle or not.
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_FRONT_RIGHT)
//							WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_FRONT_RIGHT)
//								PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//								WAIT(0)
//							ENDWHILE
//						ELSE
//							IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//								WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//									PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//									WAIT(0)
//								ENDWHILE 
//							ELSE
//								IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT)
//									WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT)
//										PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//										WAIT(0)
//									ENDWHILE 
//								ELSE
//									WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, <<vPlayerCoords.x, vPlayerCoords.y + 2, vPlayerCoords.z>>, GET_ENTITY_HEADING(PLAYER_PED_ID()))
//										PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//										WAIT(0)
//									ENDWHILE
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, <<vPlayerCoords.x, vPlayerCoords.y + 2, vPlayerCoords.z>>, GET_ENTITY_HEADING(PLAYER_PED_ID()))
//							PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//							WAIT(0)
//						ENDWHILE
//					ENDIF
//				ENDIF
//				GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], WEAPONTYPE_SMG, 2000, TRUE)
//				TrevorAITaskGiven = FALSE
//			ENDIF
//		ENDIF
//		
//		//Check if he is calling Michael
//		IF IS_CALLING_CONTACT(CHAR_MICHAEL)
//			//Do dialogue here 
//			//If dialogue starts flag that player has called this buddy for help so they have to come.
//			MichaelIsNeeded = TRUE
//		ENDIF
//		
//		IF MichaelIsNeeded = TRUE
//			IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
//					//Create Trevor, check whether the player is inside a vehicle or not.
//					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//						IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_FRONT_RIGHT)
//							WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_FRONT_RIGHT)
//								PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//								WAIT(0)
//							ENDWHILE
//						ELSE
//							IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//								WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//									PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//									WAIT(0)
//								ENDWHILE 
//							ELSE
//								IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT)
//									WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT)
//										PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//										WAIT(0)
//									ENDWHILE 
//								ELSE
//									WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<vPlayerCoords.x, vPlayerCoords.y + 2, vPlayerCoords.z>>, GET_ENTITY_HEADING(PLAYER_PED_ID()))
//										PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//										WAIT(0)
//									ENDWHILE
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<vPlayerCoords.x, vPlayerCoords.y + 2, vPlayerCoords.z>>, GET_ENTITY_HEADING(PLAYER_PED_ID()))
//							PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//							WAIT(0)
//						ENDWHILE
//					ENDIF
//				ENDIF
//				GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_SMG, 2000, TRUE)
//				MichaelAITaskGiven = FALSE
//			ENDIF
//		ENDIF
//	ENDIF	
//ENDPROC


// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

//Removes all mission blips
PROC REMOVE_BLIPS()

	IF DOES_BLIP_EXIST(HeliToStealBlip)
		REMOVE_BLIP(HeliToStealBlip)
	ENDIF
//	IF DOES_BLIP_EXIST(BlipEnemyHeliPilot[0]) 
//		REMOVE_BLIP(BlipEnemyHeliPilot[0])
//	ENDIF
//	IF DOES_BLIP_EXIST(BlipEnemyHeliPilot[1])
//		REMOVE_BLIP(BlipEnemyHeliPilot[1])
//	ENDIF
	IF DOES_BLIP_EXIST(HangerBlip)
		REMOVE_BLIP(HangerBlip)
	ENDIF
//	IF DOES_BLIP_EXIST(BuddyBlip)
//		REMOVE_BLIP(BuddyBlip)
//	ENDIF
	IF DOES_BLIP_EXIST(ArmyJet1Blip)
		REMOVE_BLIP(ArmyJet1Blip)
	ENDIF

	FOR icount = 0 TO 12
		CLEANUP_AI_PED_BLIP(MarineCopBlip[icount])
	ENDFOR	
	
	CLEAR_GPS_MULTI_ROUTE()
	
ENDPROC

PROC Mission_Cleanup()
	
	RESET_ALL_BATTLEBUDDY_BEHAVIOUR_REQUESTS()
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(CARGOBOB, FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BUZZARD, FALSE)
	SET_POLICE_RADAR_BLIPS(TRUE)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2195.3, 3231.1, 41.4>>, <<-2164.1, 3250.5, 29>>, TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2050.8, 3139.5, 41>>, <<-2018.5, 3160.0, 27>>, TRUE)
//	SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_BASE, FALSE)	
	
	//Set car generators back to normal.
	CLEAR_VEHICLE_GENERATOR_AREA_OF_INTEREST()
	
	REMOVE_SCENARIO_BLOCKING_AREA(RemoveHeliBlockingArea)	
	REMOVE_SCENARIO_BLOCKING_AREA(RemoveHeliBlockingArea2)	
	REMOVE_SCENARIO_BLOCKING_AREA(RemoveHeliBlockingArea3)
//	REMOVE_SCENARIO_BLOCKING_AREA(RemoveFrontGuardBlockingArea)
	
    ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE, TRUE)
    ALLOW_DISPATCH(DT_POLICE_HELICOPTER, TRUE)
    ALLOW_DISPATCH(DT_POLICE_VEHICLE_REQUEST, TRUE)
    ALLOW_DISPATCH(DT_POLICE_ROAD_BLOCK, TRUE)
    ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, TRUE)
    ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, TRUE)
    ALLOW_DISPATCH(DT_SWAT_AUTOMOBILE, TRUE)
    ALLOW_DISPATCH(DT_SWAT_HELICOPTER, TRUE)	
	
	REMOVE_BLIPS()
	
	//RELEASE_PATH_NODES()
	
	//Disable the scenario guards
	IF DOES_SCENARIO_GROUP_EXIST("ARMY_GUARD")
		IF NOT IS_SCENARIO_GROUP_ENABLED("ARMY_GUARD")
			SET_SCENARIO_GROUP_ENABLED("ARMY_GUARD", TRUE)
		ENDIF
	ENDIF	
	
	STOP_ALARM("PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS",TRUE)
	
	SET_AMBIENT_ZONE_STATE("AZ_AFB_ALARM_SPEECH", FALSE, FALSE)
	
	SET_AUDIO_FLAG("PoliceScannerDisabled", FALSE)
	SET_AUDIO_FLAG("WantedMusicOnMission", FALSE)
	
	FOR icount = 0 TO 12
		CLEANUP_AI_PED_BLIP(MarineCopBlip[icount])
	ENDFOR	
	
	//Re-enable the ARMY_HELI group
	IF DOES_SCENARIO_GROUP_EXIST("ARMY_HELI") 
		IF NOT IS_SCENARIO_GROUP_ENABLED("ARMY_HELI") 
			SET_SCENARIO_GROUP_ENABLED("ARMY_HELI",TRUE)
		ENDIF
	ENDIF	
	
	REMOVE_RELATIONSHIP_GROUP(MARINE_GROUP_HASH)
	
	TERMINATE_THIS_THREAD()

ENDPROC

//═════════════════════════════════╡ DEBUG ╞═══════════════════════════════════
//DEBUG STAGE SELECTOR NAMING
#IF IS_DEBUG_BUILD
PROC SET_DEBUG_STAGE_NAMES()

	SkipMenuStruct[0].sTxtLabel = "STAGE_STEAL_CARGOBOB"  							// Stage 1 Name missionStage = STAGE_STEAL_CARGOBOB
	SkipMenuStruct[1].sTxtLabel = "STAGE_ESCAPE_MILITARY"  							// Stage 2 Name missionStage = STAGE_ESCAPE_MILITARY
	SkipMenuStruct[2].sTxtLabel = "STAGE_FLY_CARGOBOB_TO_HANGER"  					// Stage 3 Name missionStage = STAGE_FLY_CARGOBOB_TO_HANGER
	SkipMenuStruct[3].sTxtLabel = "STAGE_END_CUTSCENE"  							// Stage 4 Name missionStage = STAGE_END_CUTSCENE
	
ENDPROC
#ENDIF

//PURPOSE:Everything that is needed before the mission starts.
PROC SETUP_MISSION() 	
	
	INIT_BATTLEBUDDY_BEHAVIOUR_FOR_MISSION(SP_HEIST_DOCKS_PREP_2B)
	
	//Request and load text for god text.
	REQUEST_ADDITIONAL_TEXT("DOCKP2B", MISSION_TEXT_SLOT)
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		PRINTSTRING("Waiting for additional text to load") PRINTNL()
		WAIT(0)
	ENDWHILE			
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
	ENDIF							
	
//	ADD_RELATIONSHIP_GROUP("invisiblePedGroup", invisibleGroup)
	
	//Stop ambulances and fire engines coming.
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
	
	//Switch off the vehicle generators for the 2 areas where the CARGOBOBs spawn so i can create my own ones. 
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2024.2, 2968, 30.2>>, <<-2004.0, 2921.5, 37.6>>, FALSE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2133.3, 2993.7, 41.1>>, <<-2152.7, 3039.4, 31.6>>, FALSE)	
	//The area where the cargobobs are spawned needs to be clear.
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1884.6, 2827.7, 41.8>>, <<-1843.1, 2772.1, 29.9>>, FALSE)	

	RemoveHeliBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<-1884.6, 2827.7, 41.8>>, <<-1843.1, 2772.1, 29.9>>)
//	RemoveHeliBlockingArea2 = ADD_SCENARIO_BLOCKING_AREA(<<-2505.0, 3703.2, 196.6>>, <<-1776.1, 2707.1, 53.5>>)
	RemoveHeliBlockingArea3 = ADD_SCENARIO_BLOCKING_AREA(<<-2168.3, 3223.6, 52.8>>, <<-2156.2, 3205.0, 29>>)
//	RemoveFrontGuardBlockingArea = ADD_SCENARIO_BLOCKING_AREA(<<-1594.7, 2788.8, 20.2>>, <<-1585.5, 2808.2, 13.3>>)
	CLEAR_AREA(<<-1876.8, 2806.1, 32.3>>, 8, TRUE)
	CLEAR_AREA(<<-1857.7, 2797.9, 32.4>>, 8, TRUE)	

	SET_AUDIO_FLAG("WantedMusicOnMission", TRUE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BUZZARD, TRUE)
	
	//To stop the buzzards from spawning in.
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2195.3, 3231.1, 41.4>>, <<-2164.1, 3250.5, 29>>, FALSE)
	REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<-2195.3, 3231.1, 41.4>>, <<-2164.1, 3250.5, 29>>)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-2050.8, 3139.5, 41>>, <<-2018.5, 3160.0, 27>>, FALSE)
	REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(<<-2050.8, 3139.5, 41>>, <<-2018.5, 3160.0, 27>>)
	
//	SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_BASE, TRUE)	
	
	//Add in the stage names for the z menu debug only
	#IF IS_DEBUG_BUILD
		SET_DEBUG_STAGE_NAMES()	
	#ENDIF	
	
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Passed()

	PRINTSTRING("...Placeholder Mission Passed")
	PRINTNL()
	
	Mission_Flow_Mission_Passed()
	TRIGGER_MISSION_STATS_UI(TRUE,TRUE)
	Mission_Cleanup()
	
ENDPROC


// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------

//PURPOSE: Sets up the scene for if the player shitskips past the last stage. The mission should end with a pass here.
PROC SET_UP_MISSION_PASSED()
	
	missionCanFail = FALSE
	
	IF NOT IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_OUT(0)
	ENDIF
	
	SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1747.0403, 3267.4558, 40.2460>>)
	SET_ENTITY_HEADING(PLAYER_PED_ID(), 229.0887)	
	
	IF DOES_ENTITY_EXIST(HeliToSteal)
		DELETE_VEHICLE(HeliToSteal)
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(HeliToSteal)
		REQUEST_MODEL(CARGOBOB)
		WHILE NOT HAS_MODEL_LOADED(CARGOBOB)
			WAIT(0)
		ENDWHILE
		HeliToSteal = CREATE_VEHICLE(CARGOBOB, <<1740.8517, 3263.1614, 40.2743>>, 280.3712)
		SET_VEHICLE_DOOR_SHUT(HeliToSteal, SC_DOOR_FRONT_LEFT)
		SET_VEHICLE_DOOR_SHUT(HeliToSteal, SC_DOOR_FRONT_RIGHT)
		SET_VEHICLE_DOORS_LOCKED(HeliToSteal, VEHICLELOCK_LOCKED)
		SET_VEHICLE_STRONG(HeliToSteal, TRUE)
		FREEZE_ENTITY_POSITION(HeliToSteal, TRUE)
		SET_VEHICLE_ENGINE_CAN_DEGRADE(HeliToSteal, FALSE)
		SET_VEHICLE_AS_RESTRICTED(HeliToSteal, 0)
	ENDIF	

	IF IS_SCREEN_FADED_OUT()
		LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
	ENDIF
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	
	IF NOT IS_SCREEN_FADED_IN()
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
	ENDIF
		
	WHILE NOT IS_SCREEN_FADED_IN()
		DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		PRINTSTRING("waiting on screen fading in") PRINTNL()
		WAIT(0)
	ENDWHILE
	
	Mission_Passed()

ENDPROC

//PURPOSE: Deletes all mission entities
PROC DELETE_ALL_ENTITIES()

//	//Move player out an vehicle before calling this.
	IF NOT IS_REPLAY_BEING_SET_UP()
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1738.1306, 3283.3796, 40.1261>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 25.3545)
		ENDIF
	ENDIF
	
	//Peds
	IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
		DELETE_PED(EnemyHeliPilot[0])
	ENDIF
//	IF DOES_ENTITY_EXIST(EnemyHeliPilot[1])
//		DELETE_PED(EnemyHeliPilot[1])
//	ENDIF
//	IF DOES_ENTITY_EXIST(InvisiblePed)
//		DELETE_PED(InvisiblePed)
//	ENDIF
	
	FOR icount = 0 TO 12
		IF DOES_ENTITY_EXIST(MarineCop[icount])
			DELETE_PED(MarineCop[icount])
		ENDIF
	ENDFOR
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		ENDIF	
	ENDIF
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		ENDIF	
	ENDIF

	//Audio
//	IF DOES_ENTITY_EXIST(HeliToSteal)
//		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(HeliToSteal)
//	ENDIF
	IF DOES_ENTITY_EXIST(EnemyHeli[0])
		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(EnemyHeli[0])
	ENDIF
//	IF DOES_ENTITY_EXIST(EnemyHeli[1])
//		REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(EnemyHeli[1])
//	ENDIF
	STOP_ALARM("PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS",TRUE)
	
	SET_AMBIENT_ZONE_STATE("AZ_AFB_ALARM_SPEECH", FALSE, FALSE)
	
	IF IS_AUDIO_SCENE_ACTIVE("DH_P_2B_ENTER_CHOPPER")
		STOP_AUDIO_SCENE("DH_P_2B_ENTER_CHOPPER")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("DH_P_2B_ENEMY_CHOPPERS_ARRIVE")
		STOP_AUDIO_SCENE("DH_P_2B_ENEMY_CHOPPERS_ARRIVE")
	ENDIF

	
	//Vehicles
	IF DOES_ENTITY_EXIST(buddyCar)
		DELETE_VEHICLE(buddyCar)
	ENDIF	
	IF DOES_ENTITY_EXIST(PlayerCar)
		DELETE_VEHICLE(PlayerCar)
	ENDIF	
	IF DOES_ENTITY_EXIST(HeliToSteal)
		DELETE_VEHICLE(HeliToSteal)
	ENDIF		
	IF DOES_ENTITY_EXIST(EnemyHeli[0])
		DELETE_VEHICLE(EnemyHeli[0])
	ENDIF	
//	IF DOES_ENTITY_EXIST(EnemyHeli[1])
//		DELETE_VEHICLE(EnemyHeli[1])
//	ENDIF
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
		IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_sTriggerSceneAssets.veh[0])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
		ENDIF
		DELETE_VEHICLE(g_sTriggerSceneAssets.veh[0])
	ENDIF	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
		IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_sTriggerSceneAssets.veh[1])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[1], TRUE, TRUE)
		ENDIF
		DELETE_VEHICLE(g_sTriggerSceneAssets.veh[1])
	ENDIF	
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[2])
		IF NOT DOES_ENTITY_BELONG_TO_THIS_SCRIPT(g_sTriggerSceneAssets.veh[2])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[2], TRUE, TRUE)
		ENDIF
		DELETE_VEHICLE(g_sTriggerSceneAssets.veh[2])
	ENDIF	
	IF DOES_ENTITY_EXIST(GetAwayCar[0])
		DELETE_VEHICLE(GetAwayCar[0])
	ENDIF		
	
	//Objects
	IF DOES_ENTITY_EXIST(obj_lesters_rpg)
		DELETE_OBJECT(obj_lesters_rpg)
	ENDIF
	IF DOES_ENTITY_EXIST(obj_single_rocket)
		DELETE_OBJECT(obj_single_rocket)
	ENDIF	
	
	
	//Camera's
	IF DOES_CAM_EXIST(RocketCam)
		IF IS_CAM_ACTIVE(RocketCam)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
		ENDIF
		DESTROY_CAM(RocketCam)
	ENDIF
	
ENDPROC

//PURPOSE: Adds the players to dialogue by checking who the player is in control of.
FUNC BOOL IS_PLAYER_AND_BUDDY_ADDED_FOR_DIALOGUE(PED_INDEX BuddyPed)

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
		IF DOES_ENTITY_EXIST(BuddyPed)
			IF NOT IS_PED_INJURED(BuddyPed)
				IF BuddyPed = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
					RETURN TRUE
				ENDIF
				IF BuddyPed = sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
					RETURN TRUE
				ENDIF			
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, PLAYER_PED_ID(), "FRANKLIN")
		IF DOES_ENTITY_EXIST(BuddyPed)
			IF NOT IS_PED_INJURED(BuddyPed)
				IF BuddyPed = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
					RETURN TRUE
				ENDIF
				IF BuddyPed = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
					RETURN TRUE
				ENDIF		
			ENDIF
		ENDIF
	ENDIF	
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, PLAYER_PED_ID(), "TREVOR")
		IF DOES_ENTITY_EXIST(BuddyPed)
			IF NOT IS_PED_INJURED(BuddyPed)
				IF BuddyPed = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
					RETURN TRUE
				ENDIF
				IF BuddyPed = sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
					RETURN TRUE
				ENDIF			
			ENDIF
		ENDIF
	ENDIF	
	
	RETURN FALSE
	
ENDFUNC


//PURPOSE: Handles the players positioning for each stage
PROC SETUP_MILITARY_AND_COPS()
	
	IF SetupMilitaryCops = FALSE
	
		IF HAS_MODEL_LOADED(S_M_Y_MARINE_01)
		AND HAS_MODEL_LOADED(S_M_M_PILOT_02)
//			ECF_BLOCK_SEAT_SHUFFLING
			//Create invisible ped for heli passenger seat to prevent anyone getting in the front seat
//			IF NOT DOES_ENTITY_EXIST(InvisiblePed)
//				IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
////					InvisiblePed = CREATE_PED( PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-2140.7512, 3021.2112, 31.8100>>, 4.8633)
//					InvisiblePed = CREATE_PED_INSIDE_VEHICLE(HeliToSteal, PEDTYPE_MISSION, S_M_Y_MARINE_01, VS_FRONT_RIGHT)
//					SET_PED_RELATIONSHIP_GROUP_HASH(InvisiblePed, invisibleGroup)
//					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, invisibleGroup)
//					SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_ARMY, invisibleGroup)
//					SET_ENTITY_VISIBLE(InvisiblePed, FALSE)
//					SET_PED_CAN_BE_DRAGGED_OUT(InvisiblePed, FALSE)
//					SET_ENTITY_INVINCIBLE(InvisiblePed, TRUE)
//					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(InvisiblePed, TRUE)
//					SET_PED_CONFIG_FLAG(InvisiblePed, PCF_PreventAutoShuffleToDriversSeat, TRUE)
//					SET_PED_CAN_BE_TARGETTED(InvisiblePed, FALSE)
////					SET_PED_COMBAT_ATTRIBUTES(InvisiblePed, CA_USE_VEHICLE, FALSE)
////					TASK_ENTER_VEHICLE(InvisiblePed, HeliToSteal, 5000, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_RUN, ECF_BLOCK_SEAT_SHUFFLING|ECF_RESUME_IF_INTERRUPTED)
//				ENDIF
//			ENDIF

			IF NOT DOES_ENTITY_EXIST(MarineCop[0])
				MarineCop[0] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-1590.5227, 2796.3022, 15.9184>>, 247.0896)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(MarineCop[1])
				MarineCop[1] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-2303.7996, 3387.9900, 30.0340>>, 58.5763)
			ENDIF			
			IF NOT DOES_ENTITY_EXIST(MarineCop[2])
				MarineCop[2] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-1697.3290, 2909.9414, 31.9506>>, 245.6560)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(MarineCop[3])
				MarineCop[3] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-1696.3693, 2905.7722, 31.9454>>, 6.3714)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(MarineCop[4])
				MarineCop[4] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-1843.7073, 2821.6106, 31.8528>>, 271.6265)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(MarineCop[5])
				MarineCop[5] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-2006.8772, 2955.1013, 31.8103>>, 332.0101)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(MarineCop[6])
				MarineCop[6] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-2010.0220, 2956.8052, 31.8103>>, 288.1283)
			ENDIF
			//Don't create these 2 guys if stage is escape military.
			IF missionStage <> STAGE_ESCAPE_MILITARY
				IF NOT DOES_ENTITY_EXIST(MarineCop[7])
					MarineCop[7] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-2140.2131, 3034.6091, 31.8100>>, 302.4493)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(MarineCop[8])
					MarineCop[8] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-2136.0530, 3031.4944, 31.8100>>, 38.6856)
				ENDIF
			ENDIF
			IF NOT DOES_ENTITY_EXIST(MarineCop[9])
				MarineCop[9] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-2035.4293, 3058.8765, 31.8103>>, 169.8222)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(MarineCop[10])
				MarineCop[10] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-2146.4666, 3160.7490, 31.8106>>, 356.4766)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(MarineCop[11])
				MarineCop[11] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-2280.0869, 3172.4233, 31.8103>>, 353.663)
			ENDIF
			IF NOT DOES_ENTITY_EXIST(MarineCop[12])
				MarineCop[12] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_MARINE_01, <<-2277.3762, 3177.8000, 31.8103>>, 175.0685)
			ENDIF
	
			//Create 2 pilots
			IF NOT DOES_ENTITY_EXIST(EnemyHeliPilot[0])
				EnemyHeliPilot[0] = CREATE_PED(PEDTYPE_MISSION, S_M_M_PILOT_02, <<-1859.8307, 3071.7576, 31.8104>>, 139.3607)
				SET_PED_PROP_INDEX(EnemyHeliPilot[0], ANCHOR_HEAD, 2, 0)
				SET_PED_COMPONENT_VARIATION(EnemyHeliPilot[0], PED_COMP_SPECIAL, 0, 0)
				SET_ENTITY_LOAD_COLLISION_FLAG(EnemyHeliPilot[0], TRUE)
				GIVE_WEAPON_TO_PED(EnemyHeliPilot[0], WEAPONTYPE_ASSAULTRIFLE, 2000, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(EnemyHeliPilot[0], MARINE_GROUP_HASH)
				SET_PED_ACCURACY(EnemyHeliPilot[0], 0)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyHeliPilot[0], TRUE)
				ADD_DEADPOOL_TRIGGER(EnemyHeliPilot[0], DH2BP_KILLS)
				INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(EnemyHeliPilot[0])
			ENDIF
//			IF NOT DOES_ENTITY_EXIST(EnemyHeliPilot[1])
//				EnemyHeliPilot[1] = CREATE_PED(PEDTYPE_MISSION, S_M_M_PILOT_02, <<-2222.4194, 3251.4404, 31.8103>>, 221.3115)
//				SET_PED_PROP_INDEX(EnemyHeliPilot[1], ANCHOR_HEAD, 2, 0)
//				SET_PED_COMPONENT_VARIATION(EnemyHeliPilot[1], PED_COMP_SPECIAL, 0, 0)				
//				SET_ENTITY_LOAD_COLLISION_FLAG(EnemyHeliPilot[1], TRUE)
//				GIVE_WEAPON_TO_PED(EnemyHeliPilot[1], WEAPONTYPE_ASSAULTRIFLE, 2000, TRUE)
//				SET_PED_RELATIONSHIP_GROUP_HASH(EnemyHeliPilot[1], RELGROUPHASH_AMBIENT_ARMY)
//				SET_PED_ACCURACY(EnemyHeliPilot[1], 0)
//				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyHeliPilot[1], TRUE)
//				ADD_DEADPOOL_TRIGGER(EnemyHeliPilot[1], DH2BP_KILLS)
//				INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(EnemyHeliPilot[1])
//			ENDIF
			
			FOR icount = 0 TO 12
				IF MarineCopSetup[icount] = FALSE
					IF NOT IS_PED_INJURED(MarineCop[icount])
						SET_ENTITY_LOAD_COLLISION_FLAG(MarineCop[icount], TRUE)
						GIVE_WEAPON_TO_PED(MarineCop[icount], WEAPONTYPE_ASSAULTRIFLE, 2000, TRUE)
						SET_PED_RELATIONSHIP_GROUP_HASH(MarineCop[icount], MARINE_GROUP_HASH)
						SET_PED_ACCURACY(MarineCop[icount], 10)
						ADD_DEADPOOL_TRIGGER(MarineCop[icount], DH2BP_KILLS)
						INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(MarineCop[icount])
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MarineCop[icount], TRUE)
						MarineCopSetup[icount] = TRUE
					ENDIF
				ENDIF
			ENDFOR
				
			#IF IS_DEBUG_BUILD
				IF DOES_ENTITY_EXIST(MarineCop[0])
					IF NOT IS_PED_INJURED(MarineCop[0])
						TEXT_LABEL tDebugName = "MarineCop 0"
						SET_PED_NAME_DEBUG(MarineCop[0], tDebugName)
					ENDIF
				ENDIF	
				IF DOES_ENTITY_EXIST(MarineCop[1])
					IF NOT IS_PED_INJURED(MarineCop[1])
						TEXT_LABEL tDebugName = "MarineCop 1"
						SET_PED_NAME_DEBUG(MarineCop[1], tDebugName)
					ENDIF
				ENDIF	
				IF DOES_ENTITY_EXIST(MarineCop[2])
					IF NOT IS_PED_INJURED(MarineCop[2])
						TEXT_LABEL tDebugName = "MarineCop 2"
						SET_PED_NAME_DEBUG(MarineCop[2], tDebugName)
					ENDIF
				ENDIF	
				IF DOES_ENTITY_EXIST(MarineCop[3])
					IF NOT IS_PED_INJURED(MarineCop[3])
						TEXT_LABEL tDebugName = "MarineCop 3"
						SET_PED_NAME_DEBUG(MarineCop[3], tDebugName)
					ENDIF
				ENDIF	
				IF DOES_ENTITY_EXIST(MarineCop[4])
					IF NOT IS_PED_INJURED(MarineCop[4])
						TEXT_LABEL tDebugName = "MarineCop 4"
						SET_PED_NAME_DEBUG(MarineCop[4], tDebugName)
					ENDIF
				ENDIF	
				IF DOES_ENTITY_EXIST(MarineCop[5])
					IF NOT IS_PED_INJURED(MarineCop[5])
						TEXT_LABEL tDebugName = "MarineCop 5"
						SET_PED_NAME_DEBUG(MarineCop[5], tDebugName)
					ENDIF
				ENDIF	
				IF DOES_ENTITY_EXIST(MarineCop[6])
					IF NOT IS_PED_INJURED(MarineCop[6])
						TEXT_LABEL tDebugName = "MarineCop 6"
						SET_PED_NAME_DEBUG(MarineCop[6], tDebugName)
					ENDIF
				ENDIF	
				IF DOES_ENTITY_EXIST(MarineCop[7])
					IF NOT IS_PED_INJURED(MarineCop[7])
						TEXT_LABEL tDebugName = "MarineCop 7"
						SET_PED_NAME_DEBUG(MarineCop[7], tDebugName)
					ENDIF
				ENDIF	
				IF DOES_ENTITY_EXIST(MarineCop[8])
					IF NOT IS_PED_INJURED(MarineCop[8])
						TEXT_LABEL tDebugName = "MarineCop 8"
						SET_PED_NAME_DEBUG(MarineCop[8], tDebugName)
					ENDIF
				ENDIF	
				IF DOES_ENTITY_EXIST(MarineCop[9])
					IF NOT IS_PED_INJURED(MarineCop[9])
						TEXT_LABEL tDebugName = "MarineCop 9"
						SET_PED_NAME_DEBUG(MarineCop[9], tDebugName)
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(MarineCop[10])
					IF NOT IS_PED_INJURED(MarineCop[10])
						TEXT_LABEL tDebugName = "MarineCop 10"
						SET_PED_NAME_DEBUG(MarineCop[10], tDebugName)
					ENDIF
				ENDIF	
				IF DOES_ENTITY_EXIST(MarineCop[11])
					IF NOT IS_PED_INJURED(MarineCop[11])
						TEXT_LABEL tDebugName = "MarineCop 11"
						SET_PED_NAME_DEBUG(MarineCop[11], tDebugName)
					ENDIF
				ENDIF	
				IF DOES_ENTITY_EXIST(MarineCop[12])
					IF NOT IS_PED_INJURED(MarineCop[12])
						TEXT_LABEL tDebugName = "MarineCop 12"
						SET_PED_NAME_DEBUG(MarineCop[12], tDebugName)
					ENDIF
				ENDIF	
			#ENDIF	
			PRINTSTRING("SetupMilitaryCops = TRUE") PRINTNL()
			
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_MARINE_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_02)
			SetupMilitaryCops = TRUE
		ELSE
			PRINTSTRING("Waitiong on model S_M_Y_MARINE_01 loading ... SetupMilitaryCops = FALSE") PRINTNL()
		ENDIF
	ENDIF

ENDPROC

PROC CREATE_ENTITIES_REQUIRED()

	SWITCH missionStage
		
		CASE STAGE_STEAL_CARGOBOB
			
			CLEAR_AREA(<<-1588.1, 2794.9, 17.2>>, 20, TRUE)
//			DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<-1588.1, 2794.9, 17.2>>, 100)
			RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1720.5, 2568.4, 67.1>>, <<-1523.2, 3002, -70>>, 100, <<-1155.0603, 2665.9795, 17.0939>>, 219.2079, TRUE, FALSE)
			CLEAR_AREA_OF_OBJECTS(<<-1588.1, 2794.9, 17.2>>, 20, CLEAROBJ_FLAG_INCLUDE_DOORS|CLEAROBJ_FLAG_FORCE) 
			
			REQUEST_MODEL(CARGOBOB)
			REQUEST_MODEL(S_M_Y_MARINE_01)
			REQUEST_MODEL(S_M_M_PILOT_02)
			REQUEST_MODEL(BUZZARD)
			
			WHILE NOT HAS_MODEL_LOADED(CARGOBOB)
			OR NOT HAS_MODEL_LOADED(S_M_Y_MARINE_01)
			OR NOT HAS_MODEL_LOADED(S_M_M_PILOT_02)
			OR NOT HAS_MODEL_LOADED(BUZZARD)
				PRINTSTRING("Waiting on models loading for STAGE_STEAL_CARGOBOB")
				WAIT(0)
			ENDWHILE
			
			//Request and load checkpoint vehicle if on a replay	
			IF IS_REPLAY_IN_PROGRESS()
				IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
					IF NOT IS_THIS_MODEL_A_HELI(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
					AND NOT IS_THIS_MODEL_A_PLANE(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
						REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
						WHILE NOT  HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
							PRINTSTRING("Waiting for replay checkpoint vehicle loading")
							WAIT(0)
						ENDWHILE
					ENDIF
				ENDIF
			ENDIF
			
			//Flags
			SetupMilitaryCops = FALSE
			
			FOR icount = 0 TO 12
				MarineCopSetup[icount] = FALSE
			ENDFOR
			
			//Create the heli to steal
			IF NOT DOES_ENTITY_EXIST(HeliToSteal)
				CLEAR_AREA(vHeliCoords, 10, TRUE)
				HeliToSteal = CREATE_VEHICLE(CARGOBOB, vHeliCoords, fHeliHeading)
				SET_VEHICLE_CAN_BE_TARGETTED(HeliToSteal, FALSE)
				FREEZE_ENTITY_POSITION(HeliToSteal, TRUE)
				SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(CARGOBOB)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(CARGOBOB, TRUE)
				SET_VEHICLE_STRONG(HeliToSteal, TRUE)
//				SET_ENTITY_INVINCIBLE(HeliToSteal, TRUE)
				SET_VEHICLE_ENGINE_CAN_DEGRADE(HeliToSteal, FALSE)
				SET_VEHICLE_AS_RESTRICTED(HeliToSteal, 0)
			ENDIF			
			
			SETUP_MILITARY_AND_COPS()
			
			//Create the 2 enemy heli's
			IF NOT DOES_ENTITY_EXIST(EnemyHeli[0])
				IF HAS_MODEL_LOADED(BUZZARD)
					CLEAR_AREA(<<-1866.4460, 3071.3950, 31.8104>>, 10, TRUE)
					EnemyHeli[0] = CREATE_VEHICLE(BUZZARD, <<-1866.4460, 3071.3950, 31.8104>>, 150.2863)
					SET_ENTITY_LOAD_COLLISION_FLAG(EnemyHeli[0], TRUE)
					SET_VEHICLE_CAN_BE_TARGETTED(EnemyHeli[0], TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(EnemyHeli[0])
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(EnemyHeli[0], "DH_P_2B_ENEMY_CHOPPERS")
					SET_MODEL_AS_NO_LONGER_NEEDED(BUZZARD)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(EnemyHeli[0], FALSE)
				ENDIF
			ENDIF		
			
			//Create players car and put him in it.
			IF IS_REPLAY_IN_PROGRESS()
				IF NOT DOES_ENTITY_EXIST(PlayerCar)
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
						IF NOT IS_THIS_MODEL_A_HELI(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
						AND NOT IS_THIS_MODEL_A_PLANE(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
							PlayerCar = CREATE_REPLAY_CHECKPOINT_VEHICLE(<<-1535.2094, 2744.5505, 16.6437>>, 47.9754)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
							SET_VEHICLE_HAS_STRONG_AXLES(PlayerCar, TRUE)
							SET_VEHICLE_STRONG(PlayerCar, TRUE)
						ENDIF							
					ENDIF	
				ENDIF
			ENDIF		
			
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			SET_POLICE_RADAR_BLIPS(TRUE)

		BREAK
	
		CASE STAGE_ESCAPE_MILITARY
		
			PREPARE_ALARM("PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS")
			
			WAIT(1500)
		
			REQUEST_MODEL(CARGOBOB)
			REQUEST_MODEL(BUZZARD)
			REQUEST_MODEL(S_M_Y_MARINE_01)
			REQUEST_MODEL(S_M_M_PILOT_02)
			
			WHILE NOT HAS_MODEL_LOADED(CARGOBOB)
			OR NOT HAS_MODEL_LOADED(BUZZARD)
			OR NOT HAS_MODEL_LOADED(S_M_Y_MARINE_01)
			OR NOT HAS_MODEL_LOADED(S_M_M_PILOT_02)
				PRINTSTRING("Waiting on models loading for STAGE_STEAL_CARGOBOB")
				WAIT(0)
			ENDWHILE
			
			//Flags
			SetupMilitaryCops = FALSE
			
			FOR icount = 0 TO 12
				MarineCopSetup[icount] = FALSE
			ENDFOR			
			
			//Create the heli to steal
			IF NOT DOES_ENTITY_EXIST(HeliToSteal)
				CLEAR_AREA(vHeliCoords, 10, TRUE)
				HeliToSteal = CREATE_VEHICLE(CARGOBOB, vHeliCoords, fHeliHeading)
				FREEZE_ENTITY_POSITION(HeliToSteal, TRUE)
				SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
				SET_HELI_BLADES_FULL_SPEED(HeliToSteal)
				SET_MODEL_AS_NO_LONGER_NEEDED(CARGOBOB)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(CARGOBOB, TRUE)
				SET_VEHICLE_STRONG(HeliToSteal, TRUE)
//				SET_ENTITY_INVINCIBLE(HeliToSteal, TRUE)
				SET_VEHICLE_ENGINE_CAN_DEGRADE(HeliToSteal, FALSE)
				SET_VEHICLE_AS_RESTRICTED(HeliToSteal, 0)
			ENDIF	
		
			SETUP_MILITARY_AND_COPS()
			
			//Create the 2 enemy heli's
			IF NOT DOES_ENTITY_EXIST(EnemyHeli[0])
				IF HAS_MODEL_LOADED(BUZZARD)
					CLEAR_AREA(<<-1866.4460, 3071.3950, 31.8104>>, 10, TRUE)
					EnemyHeli[0] = CREATE_VEHICLE(BUZZARD, <<-1866.4460, 3071.3950, 31.8104>>, 150.2863)
					SET_ENTITY_LOAD_COLLISION_FLAG(EnemyHeli[0], TRUE)
					SET_VEHICLE_CAN_BE_TARGETTED(EnemyHeli[0], TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(EnemyHeli[0])
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(EnemyHeli[0], "DH_P_2B_ENEMY_CHOPPERS")
					SET_MODEL_AS_NO_LONGER_NEEDED(BUZZARD)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(EnemyHeli[0], FALSE)
				ENDIF
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(EnemyHeliPilot[0])
				EnemyHeliPilot[0] = CREATE_PED_INSIDE_VEHICLE(EnemyHeli[0], PEDTYPE_MISSION, S_M_M_PILOT_02)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_02)
			ELSE
				IF NOT IS_PED_IN_VEHICLE(EnemyHeliPilot[0], EnemyHeli[0])
					SET_PED_INTO_VEHICLE(EnemyHeliPilot[0], EnemyHeli[0])
				ENDIF
			ENDIF
			
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, -1)
			
//			IF NOT DOES_ENTITY_EXIST(EnemyHeli[1])
//				IF HAS_MODEL_LOADED(BUZZARD)
//					CLEAR_AREA(<<-2221.2722, 3245.3533, 31.8103>>, 10, TRUE)
//					EnemyHeli[1] = CREATE_VEHICLE(BUZZARD, <<-2221.2722, 3245.3533, 31.8103>>, 240.2412)
//					SET_ENTITY_LOAD_COLLISION_FLAG(EnemyHeli[1], TRUE)
//					SET_VEHICLE_CAN_BE_TARGETTED(EnemyHeli[1], TRUE)
//					FREEZE_ENTITY_POSITION(EnemyHeli[1], FALSE)
//					ADD_ENTITY_TO_AUDIO_MIX_GROUP(EnemyHeli[1], "DH_P_2B_ENEMY_CHOPPERS")
//					SET_MODEL_AS_NO_LONGER_NEEDED(BUZZARD)
//				ENDIF
//			ENDIF		
		
			START_ALARM("PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS",TRUE)
			SET_AMBIENT_ZONE_STATE("AZ_AFB_ALARM_SPEECH", TRUE, TRUE)
		
//			//Create Michael if player started mission as Trevor
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<1724.6218, 3293.3455, 40.2262>>, 283.0429)
//					PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//					WAIT(0)
//				ENDWHILE
//				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//					SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], RELGROUPHASH_PLAYER)
//					GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_CARBINERIFLE, 2000, FALSE, FALSE)
//					GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_MICROSMG, 2000, FALSE, FALSE)
//					SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE, FALSE, TRUE, FALSE, FALSE)
//					SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 100)
//					SET_PED_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], ANCHOR_EARS, 0,0)
//					SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
//				ENDIF				
//				
//				//Buddys car
//				CLEAR_AREA(vMilitaryBaseEntrance, 5, TRUE)
//				WHILE NOT CREATE_PLAYER_VEHICLE(buddyCar, CHAR_MICHAEL, vMilitaryBaseEntrance, fMilitaryBaseEntranceHeading)
//					PRINTSTRING("Waiting for Michaels car being spawned") PRINTNL()
//					WAIT(0)
//				ENDWHILE
//				SET_VEHICLE_ON_GROUND_PROPERLY(buddyCar)
//				SET_VEHICLE_COLOURS(buddyCar, 0, 0)
//			ENDIF	
			
//			//Create Trevor if player started mission as Michael
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, <<1724.6218, 3293.3455, 40.2262>>, 283.0429)
//					PRINTSTRING("Waiting for Trevor being spawned") PRINTNL()
//					WAIT(0)
//				ENDWHILE
//				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//					SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], RELGROUPHASH_PLAYER)
//					GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], WEAPONTYPE_CARBINERIFLE, 2000, FALSE, FALSE)
//					GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], WEAPONTYPE_MICROSMG, 2000, FALSE, FALSE)
//					SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE, FALSE, TRUE, FALSE, FALSE)
//					SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 100)
//					SET_PED_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], ANCHOR_EARS, 0,0)
//					SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
//				ENDIF	
//				CLEAR_AREA(vMilitaryBaseEntrance, 5, TRUE)
//				WHILE NOT CREATE_PLAYER_VEHICLE(buddyCar, CHAR_TREVOR,  vMilitaryBaseEntrance, fMilitaryBaseEntranceHeading)
//					PRINTSTRING("Waiting for Trevors car being spawned") PRINTNL()
//					WAIT(0)
//				ENDWHILE
//				SET_VEHICLE_ON_GROUND_PROPERLY(buddyCar)
//			ENDIF		
		
		BREAK
		
		CASE STAGE_FLY_CARGOBOB_TO_HANGER
		
			REQUEST_MODEL(CARGOBOB)
			
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			
			WHILE NOT HAS_MODEL_LOADED(CARGOBOB)
				PRINTSTRING("Waiting on models loading for STAGE_STEAL_CARGOBOB")
				WAIT(0)
			ENDWHILE

			//Create the heli to steal
			IF NOT DOES_ENTITY_EXIST(HeliToSteal)
				CLEAR_AREA(vHeliCoords, 10, TRUE)
				HeliToSteal = CREATE_VEHICLE(CARGOBOB, <<-629.1677, 2871.3101, 385.0313>>, 283.1214)
				SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(CARGOBOB)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(CARGOBOB, TRUE)
//				SET_ENTITY_INVINCIBLE(HeliToSteal, TRUE)
				SET_VEHICLE_ENGINE_CAN_DEGRADE(HeliToSteal, FALSE)
				SET_VEHICLE_STRONG(HeliToSteal, TRUE)
				SET_VEHICLE_ENGINE_ON(HeliToSteal, TRUE, TRUE)
				SET_HELI_BLADES_FULL_SPEED(HeliToSteal)
				SET_VEHICLE_FORWARD_SPEED(HeliToSteal, 30)
				SET_VEHICLE_AS_RESTRICTED(HeliToSteal, 0)
			ENDIF
			
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, -1)
			
//			//Create Michael if player started mission as Trevor
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<1724.6218, 3293.3455, 40.2262>>, 283.0429)
//					PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//					WAIT(0)
//				ENDWHILE
//				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//					SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], RELGROUPHASH_PLAYER)
//					GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_CARBINERIFLE, 2000, FALSE, FALSE)
//					GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_MICROSMG, 2000, FALSE, FALSE)
//					SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE, FALSE, TRUE, FALSE, FALSE)
//					SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 100)
//					SET_PED_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], ANCHOR_EARS, 0,0)
//					SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
//				ENDIF	
//			ENDIF	
//			
//			//Create Trevor if player started mission as Michael
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, <<1724.6218, 3293.3455, 40.2262>>, 283.0429)
//					PRINTSTRING("Waiting for Trevor being spawned") PRINTNL()
//					WAIT(0)
//				ENDWHILE
//				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//					SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], RELGROUPHASH_PLAYER)
//					GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], WEAPONTYPE_CARBINERIFLE, 2000, FALSE, FALSE)
//					GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], WEAPONTYPE_MICROSMG, 2000, FALSE, FALSE)
//					SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE, FALSE, TRUE, FALSE, FALSE)
//					SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 100)
//					SET_PED_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], ANCHOR_EARS, 0,0)
//					SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
//				ENDIF					
//			ENDIF			
		
		BREAK
		
		CASE STAGE_END_CUTSCENE
			REQUEST_MODEL(CARGOBOB)
			REQUEST_MODEL(SANDKING)
			
			WHILE NOT HAS_MODEL_LOADED(CARGOBOB)
			OR NOT HAS_MODEL_LOADED(SANDKING)
				WAIT(0)
			ENDWHILE
			
			//Create the heli to steal
			IF NOT DOES_ENTITY_EXIST(HeliToSteal)
				CLEAR_AREA(<<1749.6102, 3272.9497, 40.1533>>, 5, TRUE)
				HeliToSteal = CREATE_VEHICLE(CARGOBOB, <<1749.6102, 3272.9497, 40.1533>>, 51.1280)
				SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(CARGOBOB)
				SET_VEHICLE_STRONG(HeliToSteal, TRUE)
//				SET_ENTITY_INVINCIBLE(HeliToSteal, TRUE)
				SET_VEHICLE_ENGINE_CAN_DEGRADE(HeliToSteal, FALSE)
				SET_VEHICLE_AS_RESTRICTED(HeliToSteal, 0)
			ENDIF	
			
			//Create the 2 getawaycars
			IF NOT DOES_ENTITY_EXIST(GetAwayCar[0])
				GetAwayCar[0] = CREATE_VEHICLE(SANDKING, <<1754.9508, 3290.9939, 40.1176>>, 182.3970)	
				FREEZE_ENTITY_POSITION(GetAwayCar[0], TRUE)
				SET_MODEL_AS_NO_LONGER_NEEDED(SANDKING)
			ENDIF	
			
			IF DOES_ENTITY_EXIST(GetAwayCar[0])
				EndCarsCreated = TRUE
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

//PURPOSE: Handles the players positioning for each stage
PROC SET_PLAYER_COORDS()
			
	SWITCH missionStage		
		
//		CASE STAGE_GET_TO_CARGOBOB
//			CLEAR_PED_TASKS(PLAYER_PED_ID())
//			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
//			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1738.1306, 3283.3796, 40.1261>>)
//			SET_ENTITY_HEADING(PLAYER_PED_ID(), 25.3545)
//		BREAK
		
		CASE STAGE_STEAL_CARGOBOB
			CLEAR_PED_TASKS(PLAYER_PED_ID())
					
			IF DOES_ENTITY_EXIST(PlayerCar)		
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), PlayerCar)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), PlayerCar)
				ENDIF
			ELSE
				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-1565.0836, 2780.8130, 16.4022>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 53.6086)		
			ENDIF
			
		BREAK
	
		CASE STAGE_ESCAPE_MILITARY
			
			CLEAR_PED_TASKS(PLAYER_PED_ID())			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				IF DOES_ENTITY_EXIST(HeliToSteal)
					IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
						ENDIF					
						FREEZE_ENTITY_POSITION(HeliToSteal, FALSE)
					ENDIF
				ENDIF
			ENDIF
			
		BREAK	
		
		CASE STAGE_FLY_CARGOBOB_TO_HANGER
		
			CLEAR_PED_TASKS(PLAYER_PED_ID())

			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				IF DOES_ENTITY_EXIST(HeliToSteal)
					IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
						ENDIF	
					ENDIF
				ENDIF
			ENDIF	
		
		BREAK
	
		CASE STAGE_END_CUTSCENE
			
			CLEAR_PED_TASKS(PLAYER_PED_ID())			
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				IF DOES_ENTITY_EXIST(HeliToSteal)
					IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
						ENDIF					
					ENDIF
				ENDIF
			ENDIF	
		
		BREAK
		
	ENDSWITCH
			
ENDPROC

////PURPOSE: Handles buddies for skipping and replays.
//PROC REPLAY_BATTLEBUDDY_SNAPSHOT()
//
//
//	// start battle buddies restoring
//	CPRINTLN(DEBUG_REPLAY, "Waiting for friend activity script to shut down.")
//	WHILE NOT CAN_RESTORE_BATTLEBUDDY_SNAPSHOT()
//	    WAIT(0)
//	ENDWHILE
//	CPRINTLN(DEBUG_REPLAY, "Friend activity script is shutdown. Request battle buddies to restore in background.")
//	RESTORE_BATTLEBUDDY_SNAPSHOT(g_stageSnapshot.mBattleBuddyStruct)
//
//	// wait for battle buddies restore to complete
//	CPRINTLN(DEBUG_REPLAY, "Waiting for battle buddies to restore.")
//	WHILE NOT IS_BATTLEBUDDY_SNAPSHOT_RESTORED(g_stageSnapshot.mBattleBuddyStruct)
//	    WAIT(0)
//	ENDWHILE
//	CPRINTLN(DEBUG_REPLAY, "Battle buddies are restored.")
//	
//	//Place buddy's into the players vehicle if he's in one.
//	IF IS_BATTLEBUDDY_AVAILABLE(GET_BATTLEBUDDY_PED(CHAR_TREVOR), FALSE)
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			IF NOT IS_PED_IN_ANY_VEHICLE(GET_BATTLEBUDDY_PED(CHAR_TREVOR))
//				SET_PED_INTO_VEHICLE(GET_BATTLEBUDDY_PED(CHAR_TREVOR), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//			ENDIF
//		ENDIF
//	ENDIF
//	IF IS_BATTLEBUDDY_AVAILABLE(GET_BATTLEBUDDY_PED(CHAR_FRANKLIN), FALSE)
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			IF NOT IS_PED_IN_ANY_VEHICLE(GET_BATTLEBUDDY_PED(CHAR_FRANKLIN))
//				IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//					SET_PED_INTO_VEHICLE(GET_BATTLEBUDDY_PED(CHAR_FRANKLIN), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//				ELSE
//					SET_PED_INTO_VEHICLE(GET_BATTLEBUDDY_PED(CHAR_FRANKLIN), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT)
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	IF IS_BATTLEBUDDY_AVAILABLE(GET_BATTLEBUDDY_PED(CHAR_MICHAEL), FALSE)
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			IF NOT IS_PED_IN_ANY_VEHICLE(GET_BATTLEBUDDY_PED(CHAR_MICHAEL))
//				IF IS_VEHICLE_SEAT_FREE(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//					SET_PED_INTO_VEHICLE(GET_BATTLEBUDDY_PED(CHAR_MICHAEL), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_LEFT)
//				ELSE
//					SET_PED_INTO_VEHICLE(GET_BATTLEBUDDY_PED(CHAR_MICHAEL), GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), VS_BACK_RIGHT)
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//				
//ENDPROC


//PURPOSE: Handles everything for replays, or skipping to different stages through out the mission.
PROC STAGE_SELECTOR_MISSION_SETUP()

	REMOVE_BLIPS()
	
	DELETE_ALL_ENTITIES()			
	
	CREATE_ENTITIES_REQUIRED()
	
	IF NOT IS_REPLAY_BEING_SET_UP()
	OR IS_REPEAT_PLAY_ACTIVE()
		SET_PLAYER_COORDS()		
	ENDIF
	
//	REPLAY_BATTLEBUDDY_SNAPSHOT()
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)

ENDPROC

/// PURPOSE:
///    Sets the fail reason, updates the mission stage and resets iControlFlag
///    (If we've already failed the mission, it only edits the fail reason
///    - so this can be used to update fail reason after mission has faded out)
/// PARAMS:
///    eFail - the reason the player has failed the mission
PROC SET_MISSION_FAILED(FAIL_REASON eFail)
	eFailReason = eFail
	IF missionStage <> STAGE_MISSION_FAILED
		missionStage = STAGE_MISSION_FAILED
		iControlFlag = 0
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC DEBUG_KEYS()
	
		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			Mission_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			SET_MISSION_FAILED(FAIL_DEFAULT)
			EXIT
		ENDIF
		
		//Check for J-Skip
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			IF NOT IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_OUT(0)
			ENDIF
		
			SWITCH missionStage 
		
				CASE STAGE_OPENING_CUTSCENE	
				
					iControlFlag = 0
					missionStage = STAGE_STEAL_CARGOBOB
					MissionStageBeingSkippedTo = TRUE					
				
				BREAK
				
				CASE STAGE_STEAL_CARGOBOB
					
					iControlFlag = 0
					missionStage = STAGE_ESCAPE_MILITARY
					MissionStageBeingSkippedTo = TRUE
				
				BREAK
				
				CASE STAGE_ESCAPE_MILITARY
					
					iControlFlag = 0
					missionStage = STAGE_FLY_CARGOBOB_TO_HANGER
					MissionStageBeingSkippedTo = TRUE
				
				BREAK
				
				CASE STAGE_FLY_CARGOBOB_TO_HANGER
					
					iControlFlag = 0
					SET_UP_MISSION_PASSED()
					
				BREAK
			
			ENDSWITCH
		
		ENDIF
		
		//Check for P-Skip
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
		
			SWITCH missionStage				
			
				CASE STAGE_STEAL_CARGOBOB
				
					iControlFlag = 0
					missionStage = STAGE_STEAL_CARGOBOB
					MissionStageBeingSkippedTo = TRUE	
					
				BREAK
				
				CASE STAGE_ESCAPE_MILITARY
					
					iControlFlag = 0
					missionStage = STAGE_STEAL_CARGOBOB
					MissionStageBeingSkippedTo = TRUE
					
				BREAK
				
				CASE STAGE_FLY_CARGOBOB_TO_HANGER
				
					iControlFlag = 0
					missionStage = STAGE_ESCAPE_MILITARY
					MissionStageBeingSkippedTo = TRUE
					
				BREAK
				
			ENDSWITCH
		
		ENDIF
		
ENDPROC
#ENDIF

//PURPOSE: Runs throughout mission checking for all fail instances.
PROC FAIL_CHECKS()
	
	//Fail mission if both CARGOBOBs are destroyed
	IF missionStage = STAGE_STEAL_CARGOBOB
	OR missionStage = STAGE_ESCAPE_MILITARY
	OR missionStage = STAGE_FLY_CARGOBOB_TO_HANGER
		IF HeliToStealDestroyed = TRUE
			SET_MISSION_FAILED(FAIL_HELI_DESTROYED)
			EXIT		
		ENDIF
	ENDIF
	
	IF missionStage = STAGE_STEAL_CARGOBOB
		IF DOES_ENTITY_EXIST(HeliToSteal)
			IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
				IF GET_DISTANCE_BETWEEN_ENTITIES(HeliToSteal, PLAYER_PED_ID()) > 850
					SET_MISSION_FAILED(FAIL_HELI_ABANDONED)
					EXIT
				ENDIF
			ENDIF
		ENDIF		
	ENDIF
	
	IF missionStage = STAGE_ESCAPE_MILITARY
	OR missionStage = STAGE_FLY_CARGOBOB_TO_HANGER
		IF DOES_ENTITY_EXIST(HeliToSteal)
			IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
				IF GET_DISTANCE_BETWEEN_ENTITIES(HeliToSteal, PLAYER_PED_ID()) > 100
					SET_MISSION_FAILED(FAIL_HELI_ABANDONED)
					EXIT
				ELSE
					IF IS_VEHICLE_STUCK_TIMER_UP(HeliToSteal, VEH_STUCK_JAMMED, JAMMED_TIME)
					OR IS_VEHICLE_STUCK_TIMER_UP(HeliToSteal, VEH_STUCK_ON_SIDE, SIDE_TIME)
					OR IS_VEHICLE_STUCK_TIMER_UP(HeliToSteal, VEH_STUCK_ON_ROOF, ROOF_TIME)
						SET_MISSION_FAILED(FAIL_HELI_STUCK)
						EXIT
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		

ENDPROC

//PURPOSE: Controls the military created by this script
PROC HANDLE_MILITARY()
	
	//Run a check to see if player is inside the military base or not
	IF playerIsInBase = FALSE
		IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_MILITARY_BASE)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1601.513794,2794.147705,14.842220>>, <<-1587.691650,2806.101318,20.295572>>, 16.000000)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1598.278198,2803.979736,17.017721>>, <<-1665.979614,2895.397705,34.589527>>, 14.500000)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1648.554199,2873.739990,24.850174>>, <<-1673.864258,2889.697754,38.188564>>, 14.500000)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1647.446045,2873.968750,24.823730>>, <<-1667.316284,2914.302002,39.394775>>, 14.500000)
			PRINTSTRING("player is inside base") PRINTNL()
			playerIsInBase = TRUE
		ENDIF
	ENDIF
	IF playerIsInBase = TRUE
		IF NOT IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_MILITARY_BASE)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1601.513794,2794.147705,14.842220>>, <<-1587.691650,2806.101318,20.295572>>, 16.000000)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1598.278198,2803.979736,17.017721>>, <<-1665.979614,2895.397705,34.589527>>, 14.500000)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1648.554199,2873.739990,24.850174>>, <<-1673.864258,2889.697754,38.188564>>, 14.500000)
		AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1647.446045,2873.968750,24.823730>>, <<-1667.316284,2914.302002,39.394775>>, 14.500000)	
			PRINTSTRING("player is not inside base") PRINTNL()
			playerIsInBase = FALSE
		ENDIF
	ENDIF
	
	IF SetupMilitaryCops = TRUE
		
//		//Run this every frame to display blips when the enemypeds shoot.
//		IF missionStage = STAGE_STEAL_CARGOBOB
//			FOR icount = 0 TO 12
//				PRINTSTRING("UPDATE_AI_PED_BLIP for Marine Cop ") PRINTINT(icount) PRINTNL()
//				UPDATE_AI_PED_BLIP(MarineCop[icount], MarineCopBlip[icount])
//			ENDFOR	
//		ELSE
//			FOR icount = 0 TO 12
//				CLEANUP_AI_PED_BLIP(MarineCopBlip[icount])
//			ENDFOR
//		ENDIF	

		//Update enemyPedState depending on what the player is doing
		IF playerIsInBase = TRUE
		OR playerSpottedFiringAiming = TRUE
		OR playerWaitedTooLongToLeaveGate = TRUE
		OR AlarmStarted = TRUE
			
//			PRINTSTRING("Either playerIsInBase = TRUE or playerSpottedFiringAiming = TRUE or playerWaitedTooLongToLeaveGate = TRUE") PRINTNL()
		
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <> 4
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 4)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			ENDIF
			
			FOR icount = 0 TO 12
				IF DOES_ENTITY_EXIST(MarineCop[icount])
					IF NOT IS_PED_INJURED(MarineCop[icount])
						IF DOES_ENTITY_EXIST(HeliToSteal)
							IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
								IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
								OR playerIsNearHeli = TRUE
									//Make peds goto the heli if player is near it
									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[icount]) < 39
										IF militaryPNHPNTasksGiven[icount] = FALSE
											CLEAR_PED_TASKS(MarineCop[icount])
											SET_PED_SPHERE_DEFENSIVE_AREA(MarineCop[icount], GET_ENTITY_COORDS(MarineCop[icount]), 50)
											TASK_COMBAT_PED(MarineCop[icount], PLAYER_PED_ID())
											SET_PED_COMBAT_ATTRIBUTES(MarineCop[icount], CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(MarineCop[icount], CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, TRUE)
											militaryPNHPFTasksGiven[icount] = FALSE
											militaryPCRTasksGiven[icount] = FALSE
											militaryPFRTasksGiven[icount] = FALSE
											militaryPNHPNTasksGiven[icount] = TRUE
//											PRINTSTRING("TASK_COMBAT_PED 1 for MarineCop ") PRINTINT(icount) PRINTSTRING(" given.") PRINTNL()
										ENDIF	
									ELSE
										IF militaryPNHPFTasksGiven[icount] = FALSE
											CLEAR_PED_TASKS(MarineCop[icount])
											OPEN_SEQUENCE_TASK(seq)
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, GET_ENTITY_COORDS(HeliToSteal), PEDMOVEBLENDRATIO_RUN, -1, 20)
												TASK_COMBAT_PED(NULL, PLAYER_PED_ID())
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(MarineCop[icount], seq)
											CLEAR_SEQUENCE_TASK(seq)
											militaryPCRTasksGiven[icount] = FALSE
											militaryPFRTasksGiven[icount] = FALSE
											militaryPNHPNTasksGiven[icount] = FALSE
											militaryPNHPFTasksGiven[icount] = TRUE
//											PRINTSTRING("TASK_PERFORM_SEQUENCE for MarineCop ") PRINTINT(icount) PRINTSTRING(" given.") PRINTNL()
										ENDIF
									ENDIF
								ELSE
									//Set peds tasks dependant on how close they are to the player
									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[icount]) < 39	
										PRINTSTRING("Distance between MarineCop ") PRINTINT(icount) PRINTSTRING(" and the player is less than 40m's.") PRINTNL()
										IF militaryPCRTasksGiven[icount] = FALSE
											CLEAR_PED_TASKS(MarineCop[icount])
											SET_PED_SPHERE_DEFENSIVE_AREA(MarineCop[icount], GET_ENTITY_COORDS(MarineCop[icount]), 50)
											TASK_COMBAT_PED(MarineCop[icount], PLAYER_PED_ID())
											SET_PED_COMBAT_ATTRIBUTES(MarineCop[icount], CA_REMOVE_AREA_SET_WILL_ADVANCE_WHEN_DEFENSIVE_AREA_REACHED, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(MarineCop[icount], CA_SWITCH_TO_ADVANCE_IF_CANT_FIND_COVER, TRUE)
											militaryPNHPFTasksGiven[icount] = FALSE
											militaryPFRTasksGiven[icount] = FALSE
											militaryPNHPNTasksGiven[icount] = FALSE
											militaryPCRTasksGiven[icount] = TRUE
//											PRINTSTRING("TASK_COMBAT_PED 2 for MarineCop ") PRINTINT(icount) PRINTSTRING(" given.") PRINTNL()
										ELSE
//											PRINTSTRING("militaryPCRTasksGiven[") PRINTINT(icount) PRINTSTRING("] = TRUE") PRINTNL()
										ENDIF
									ENDIF
									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[icount]) > 41
									AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[icount]) < 400	
										IF militaryPFRTasksGiven[icount] = FALSE
											iRandomInt = GET_RANDOM_INT_IN_RANGE(0,4)
											CLEAR_PED_TASKS(MarineCop[icount])
											SET_PED_SPHERE_DEFENSIVE_AREA(MarineCop[icount], GET_ENTITY_COORDS(MarineCop[icount]), 50)
											OPEN_SEQUENCE_TASK(seq)
												IF iRandomInt = 0
													TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), -1, 25)
												ELIF iRandomInt = 1
													TASK_STAND_STILL(NULL, 500)
													TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), -1, 25)
												ELIF iRandomInt = 2
													TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), -1, 25, PEDMOVEBLENDRATIO_SPRINT)
												ELIF iRandomInt = 3
													TASK_STAND_STILL(NULL, 500)
													TASK_GO_TO_ENTITY(NULL, PLAYER_PED_ID(), -1, 25, PEDMOVEBLENDRATIO_SPRINT)
												ENDIF
											CLOSE_SEQUENCE_TASK(seq)
											TASK_PERFORM_SEQUENCE(MarineCop[icount], seq)
											CLEAR_SEQUENCE_TASK(seq)
											militaryPNHPFTasksGiven[icount] = FALSE
											militaryPCRTasksGiven[icount] = FALSE
											militaryPNHPNTasksGiven[icount] = FALSE
											militaryPFRTasksGiven[icount] = TRUE
//											PRINTSTRING("TASK_GO_TO_ENTITY for MarineCop ") PRINTINT(icount) PRINTSTRING(" given.") PRINTNL()
										ENDIF
									ENDIF
								ENDIF
							ENDIF	
						ENDIF
					ENDIF
				ENDIF			
				
			ENDFOR		
		ENDIF
		
		//If player hasn't entered the base yet and is standing outside have the front guards deal with things
		IF playerIsInBase = FALSE
		AND playerSpottedFiringAiming = FALSE
		AND playerWaitedTooLongToLeaveGate = FALSE
		AND AlarmStarted = FALSE

			FOR iGuard2Count = 0 TO 1
				IF DOES_ENTITY_EXIST(MarineCop[iGuard2Count])
					IF NOT IS_PED_INJURED(MarineCop[iGuard2Count])
						IF GET_DISTANCE_BETWEEN_ENTITIES(MarineCop[iGuard2Count], PLAYER_PED_ID()) < 25
							IF AimTaskGiven[iGuard2Count] = FALSE
								IF NOT IS_ENTITY_ON_SCREEN(MarineCop[iGuard2Count])
									CLEAR_PED_TASKS_IMMEDIATELY(MarineCop[iGuard2Count])
								ELSE
									CLEAR_PED_TASKS(MarineCop[iGuard2Count])
								ENDIF
								TASK_AIM_GUN_AT_ENTITY(MarineCop[iGuard2Count], PLAYER_PED_ID(), -1)
								AimTaskGiven[iGuard2Count] = TRUE
							ENDIF
							//Set the guards to hostile and start the alarm etc if player takes too long to leave the area
							IF hostileTimerSet = TRUE
								IF GET_GAME_TIMER() > (iBecomeHostileTimer + 20000)
									playerWaitedTooLongToLeaveGate = TRUE
								ENDIF
							ENDIF
						ELSE
							IF GET_DISTANCE_BETWEEN_ENTITIES(MarineCop[iGuard2Count], PLAYER_PED_ID()) > 30
								IF AimTaskGiven[iGuard2Count] = TRUE
									IF NOT IS_ENTITY_ON_SCREEN(MarineCop[iGuard2Count])
										CLEAR_PED_TASKS_IMMEDIATELY(MarineCop[iGuard2Count])
									ELSE
										CLEAR_PED_TASKS(MarineCop[iGuard2Count])
									ENDIF
									TASK_AIM_GUN_AT_ENTITY(MarineCop[iGuard2Count], PLAYER_PED_ID(), 1)
									AimTaskGiven[iGuard2Count] = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
			
	ENDIF
	
ENDPROC


//PROC SETUP_ROCKET_CONTROLLER()
//
//	IF iRocketControlFlag = 0
//		REQUEST_MODEL(CARGOBOB)
//		
//		WHILE NOT HAS_MODEL_LOADED(CARGOBOB)
//			WAIT(0)
//		ENDWHILE
//		
//		testCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
//		
//		TestCargoBob = CREATE_VEHICLE(CARGOBOB, <<-1560.2627, 2759.3958, 16.6026>>, 0.2677)
//		iRocketControlFlag = 1
//	ENDIF
//	
//	IF iRocketControlFlag = 1
//		
//		IF IS_BUTTON_JUST_PRESSED(pad1, select)
//			PRINTSTRING("camera offset is ") PRINTVECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(TestCargoBob, GET_CAM_COORD(GET_DEBUG_CAM())))
//		ENDIF
//		
//		IF DOES_ENTITY_EXIST(TestCargoBob)
//			IF IS_VEHICLE_DRIVEABLE(TestCargoBob)
//				IF DOES_CAM_EXIST(testCam)
//					ATTACH_CAM_TO_ENTITY(testCam, TestCargoBob, << -1.40739, 4.40528, 0.236747 >>)
//					POINT_CAM_AT_COORD(testCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TestCargoBob, << -10, 2.25834, -0.21304 >>))
//					SET_CAM_ACTIVE(testCam, TRUE)
//					SET_CAM_FOV(testCam, 50)
//					DISPLAY_RADAR(FALSE)
//					RENDER_SCRIPT_CAMS(TRUE, FALSE)
//					fCamNewDirectionZCoord = -0.21304
//					fCamNewDirectionYCoord = 2.25834
//					RocketReadyToBeFired = TRUE
//					iRocketControlFlag = 2
//				ENDIF
//			ENDIF
//		ENDIF
//		
//	ENDIF
//
//	IF iRocketControlFlag = 2
//		
//		//MAX_LEFT_LOOK_OFFSET = << -6.18786, -9.44297, 0.50376 >>
//		//MAX_RIGHT_LOOK_OFFSET = << -4.30052, 16.678, 0.636055 >>
//		//MAX_DOWN_LOOK_OFFSET = << -3.00419, 4.14787, -4.62308 >>
//		//MAX_UP_LOOK_OFFSET = << -3.64329, 3.95352, 4.77156 >>
//		
//		//MIDDLE_OFFSET = << -6.17104, 4.20117, 0.285392 >>
//		
//		PRINTSTRING("SHOW_HUD_COMPONENT_THIS_FRAME is being called this frame")
//		SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
////		DISPLAY_SNIPER_SCOPE_THIS_FRAME()
//
//		// UP AND DOWN CONTROL CAMERA MOVEMENT
//		IF (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_UP_ONLY) AND NOT IS_LOOK_INVERTED())
//		OR (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_DOWN_ONLY) AND IS_LOOK_INVERTED())
//			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
//				fCamNewDirectionZCoord = (fCamNewDirectionZCoord + 0.25)
//			ELSE
//				fCamNewDirectionZCoord = (fCamNewDirectionZCoord + 0.5)
//			ENDIF
//		ENDIF
//		
//		IF (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_DOWN_ONLY) AND NOT IS_LOOK_INVERTED())
//		OR (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_UP_ONLY) AND IS_LOOK_INVERTED())
//			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
//				fCamNewDirectionZCoord = (fCamNewDirectionZCoord - 0.25)
//			ELSE
//				fCamNewDirectionZCoord = (fCamNewDirectionZCoord - 0.5)
//			ENDIF
//		ENDIF
//
//		IF fCamNewDirectionZCoord > 4.5
//			fCamNewDirectionZCoord = 4.5
//		ENDIF
//		IF fCamNewDirectionZCoord < -4.5
//			fCamNewDirectionZCoord = -4.5
//		ENDIF		
//
//
//		// LEFT AND RIGHT CONTROL CAMERA MOVEMENT		
//		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_LEFT_ONLY)
//			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
//				fCamNewDirectionYCoord = (fCamNewDirectionYCoord - 0.75)
//			ELSE
//				fCamNewDirectionYCoord = (fCamNewDirectionYCoord - 1.5)
//			ENDIF
//		ENDIF	
//		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_RIGHT_ONLY)
//			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
//				fCamNewDirectionYCoord = (fCamNewDirectionYCoord + 0.75)
//			ELSE
//				fCamNewDirectionYCoord = (fCamNewDirectionYCoord + 1.5)
//			ENDIF
//		ENDIF	
//	
//		IF fCamNewDirectionYCoord > 30
//			fCamNewDirectionYCoord = 30
//		ENDIF
//		IF fCamNewDirectionYCoord < -30
//			fCamNewDirectionYCoord = -30
//		ENDIF	
//		
//		POINT_CAM_AT_COORD(testCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TestCargoBob, << -10, fCamNewDirectionYCoord, fCamNewDirectionZCoord >>))
//	
//		//Zoom in if aim is pressed.
//		IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_AIM)
//			SET_CAM_FOV(testCam, 37)
//		ELSE
//			SET_CAM_FOV(testCam, 50)
//		ENDIF
//		
//		//Fire rocket if control is pressed/
//		IF RocketReadyToBeFired = TRUE
//			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)
//				SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_CAM_COORD(testCam), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(TestCargoBob, << -10, fCamNewDirectionYCoord, fCamNewDirectionZCoord >>), 50, FALSE, WEAPONTYPE_RPG)
//				iRocketFired = GET_GAME_TIMER()
//				RocketReadyToBeFired = FALSE
//			ENDIF
//		ENDIF
//		
//		IF RocketReadyToBeFired = FALSE
//			IF GET_GAME_TIMER() > iRocketFired + 5000
//				RocketReadyToBeFired = TRUE
//			ENDIF
//		ENDIF
//		
//		
////		SET_CAM_ROT(testCam, <<fCamNewDirectionXCoord,0,fCamNewDirectionZCoord>>)
//	
//	ENDIF
//ENDPROC


//PURPOSE: Checks to see if the CARGOBOBs are still flyable. If not mission should fail.
PROC HANDLE_BLIPS_AND_GOD_TEXT()

	//Handle everything regardless of who the player is.
//	IF DOES_BLIP_EXIST(BuddyBlip)
//		REMOVE_BLIP(BuddyBlip)
//	ENDIF
//	IF DOES_BLIP_EXIST(BlipEnemyHeliPilot[0])
//		REMOVE_BLIP(BlipEnemyHeliPilot[0])
//	ENDIF
//	IF DOES_BLIP_EXIST(BlipEnemyHeliPilot[1])
//		REMOVE_BLIP(BlipEnemyHeliPilot[1])
//	ENDIF
	
	IF missionStage = STAGE_END_CUTSCENE
		IF DOES_BLIP_EXIST(HeliToStealBlip)
			REMOVE_BLIP(HeliToStealBlip)
		ENDIF		
		IF DOES_BLIP_EXIST(HangerBlip)
			REMOVE_BLIP(HangerBlip)
		ENDIF	
	ENDIF
	
	IF DOES_ENTITY_EXIST(HeliToSteal)
		IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
				IF DOES_BLIP_EXIST(HeliToStealBlip)
					REMOVE_BLIP(HeliToStealBlip)
				ENDIF
				IF doneGetBackInText = TRUE
					IF GET_GAME_TIMER() < (iGetBackinTimer + DEFAULT_GOD_TEXT_TIME)
						CLEAR_PRINTS()
					ENDIF
				ENDIF
				IF missionStage = STAGE_FLY_CARGOBOB_TO_HANGER
					IF NOT DOES_BLIP_EXIST(HangerBlip)
						HangerBlip = CREATE_BLIP_FOR_COORD(vHanger)
					ENDIF
					IF DoneGetToHangerText = FALSE
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								PRINT_NOW("DP_GOD4", DEFAULT_GOD_TEXT_TIME, 1)//~s~Go to the ~y~hangar.
								iGetTohangerTextTimer = GET_GAME_TIMER()
								DoneGetToHangerText = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF missionStage = STAGE_ESCAPE_MILITARY
					IF NOT DOES_BLIP_EXIST(EnemyHeliPilotBlip[0])
						EnemyHeliPilotBlip[0] = CREATE_BLIP_FOR_ENTITY(EnemyHeliPilot[0], TRUE)
					ENDIF
				ENDIF
			ELSE
				IF missionStage <> STAGE_END_CUTSCENE
					IF NOT DOES_BLIP_EXIST(HeliToStealBlip)
						HeliToStealBlip = CREATE_BLIP_FOR_VEHICLE(HeliToSteal)
	//					SET_BLIP_SPRITE(HeliToStealBlip, RADAR_TRACE_HELICOPTER)
					ENDIF
					IF missionStage = STAGE_ESCAPE_MILITARY
					OR missionStage = STAGE_FLY_CARGOBOB_TO_HANGER
						IF doneGetBackInText = FALSE
							CLEAR_PRINTS()
							PRINT_NOW("DP_GOD6", DEFAULT_GOD_TEXT_TIME, 1)//~s~Get back in the ~b~Cargobob.
							iGetBackinTimer = GET_GAME_TIMER()
							doneGetBackInText = TRUE
						ENDIF
					ENDIF
					IF DOES_BLIP_EXIST(HangerBlip)
						REMOVE_BLIP(HangerBlip)
					ENDIF	
					IF missionStage = STAGE_ESCAPE_MILITARY
						IF DOES_BLIP_EXIST(EnemyHeliPilotBlip[0])
							REMOVE_BLIP(EnemyHeliPilotBlip[0])
						ENDIF
					ENDIF
					IF missionStage = STAGE_FLY_CARGOBOB_TO_HANGER
						IF DoneGetToHangerText = TRUE
							IF GET_GAME_TIMER() < (iGetTohangerTextTimer + DEFAULT_GOD_TEXT_TIME)
								CLEAR_PRINTS()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(HeliToStealBlip)
				REMOVE_BLIP(HeliToStealBlip)
			ENDIF	
			IF missionCanFail = TRUE
				HeliToStealDestroyed = TRUE
			ENDIF
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(HeliToStealBlip)
			REMOVE_BLIP(HeliToStealBlip)
		ENDIF
		IF missionCanFail = TRUE
			HeliToStealDestroyed = TRUE
		ENDIF
	ENDIF
	
	
ENDPROC

//PURPOSE:Deals with the custom camera's for SWITCHing from Franklin to Trevor at close distance < 400m.
FUNC BOOL SETUP_HOTSWAP_CAM_FROM_MICHAEL_TO_TREVOR_CLOSE()

//	PRINTSTRING("*********************** SETUP_HOTSWAP_CAM_FROM_FRANKLIN_TO_TREVOR_CLOSE IS BEING CALLED ***********************") PRINTNL()
	VECTOR vGamePlayCamRot = GET_GAMEPLAY_CAM_ROT()
	
	IF NOT sCamDetails.bSplineCreated	
		
		//Create main spline camera
		IF NOT DOES_CAM_EXIST(sCamDetails.camID)
			sCamDetails.camID = CREATE_CAM("DEFAULT_SPLINE_CAMERA", FALSE) 
		ENDIF  
		SET_CAM_FOV(sCamDetails.camID, SELECTOR_CAM_DEFAULT_FOV)
		
		//Create Node Camera's
		IF NOT DOES_CAM_EXIST(NodeCam[0])
			NodeCam[0] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", true)
		ENDIF 
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			ATTACH_CAM_TO_ENTITY(NodeCam[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), << 0,0,20>>)
			SET_CAM_ROT(NodeCam[0], <<-88.5, 0, vGamePlayCamRot.z>>)
			SET_CAM_FOV(NodeCam[0], SELECTOR_CAM_DEFAULT_FOV)
		ELSE
			ATTACH_CAM_TO_ENTITY(NodeCam[0], PLAYER_PED_ID(), << 0,0,5>>)
			SET_CAM_ROT(NodeCam[0], <<-88.5, 0, vGamePlayCamRot.z>>)
			SET_CAM_FOV(NodeCam[0], SELECTOR_CAM_DEFAULT_FOV)			
		ENDIF
		
		//The high point above Trevor
		IF NOT DOES_CAM_EXIST(NodeCam[1])
			NodeCam[1] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", true)
		ENDIF 
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			IF IS_PED_IN_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				ATTACH_CAM_TO_ENTITY(NodeCam[1], GET_VEHICLE_PED_IS_IN(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]), <<0.0, 0.0, 25>>, TRUE)
				SET_CAM_ROT(NodeCam[1], <<-88.5, 0, vGamePlayCamRot.z>>)
				SET_CAM_FOV(NodeCam[1], SELECTOR_CAM_DEFAULT_FOV)			
			ELSE
				ATTACH_CAM_TO_ENTITY(NodeCam[1], sSelectorPeds.pedID[SELECTOR_PED_TREVOR], <<0.0, 0.0, 5>>, TRUE)
				SET_CAM_ROT(NodeCam[1], <<-88.5, 0, vGamePlayCamRot.z>>)
				SET_CAM_FOV(NodeCam[1], SELECTOR_CAM_DEFAULT_FOV)
			ENDIF
		ENDIF

		
		//Add the nodes in order
		ADD_CAM_SPLINE_NODE(sCamDetails.camID, GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), 0, CAM_SPLINE_NODE_SMOOTH_ROT)//starting with gamecam coords and rotation
		ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, NodeCam[0], 500, CAM_SPLINE_NODE_SMOOTH_ROT)//5m's above player
		ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, NodeCam[1], 2000, CAM_SPLINE_NODE_SMOOTH_ROT)//high above player
		ADD_CAM_SPLINE_NODE_USING_GAMEPLAY_FRAME(sCamDetails.camID, 500)//Add the final camera node attached to new player model
		

		
		sCamDetails.bSplineCreated = TRUE
		sCamDetails.camType = SELECTOR_CAM_DEFAULT
		
		sCamDetails.pedTo = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
		
		RETURN TRUE
		
	ENDIF 
	
	RETURN FALSE 

ENDFUNC

//PURPOSE:Deals with the custom camera's for SWITCHing from Trevor to Franklin at close distance < 400m.
FUNC BOOL SETUP_HOTSWAP_CAM_FROM_TREVOR_TO_MICHAEL_CLOSE()

//	PRINTSTRING("*********************** SETUP_HOTSWAP_CAM_FROM_TREVOR_TO_FRANKLIN_CLOSE IS BEING CALLED ***********************") PRINTNL()

	VECTOR vGamePlayCamRot = GET_GAMEPLAY_CAM_ROT()
	
	IF NOT sCamDetails.bSplineCreated
		
		//Create main spline camera
		IF NOT DOES_CAM_EXIST(sCamDetails.camID)
			sCamDetails.camID = CREATE_CAM("DEFAULT_SPLINE_CAMERA", FALSE) 
		ENDIF  
		SET_CAM_FOV(sCamDetails.camID, SELECTOR_CAM_DEFAULT_FOV)
		
		//Create Node Camera's
		IF NOT DOES_CAM_EXIST(NodeCam[0])
			NodeCam[0] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", true)
		ENDIF 
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			ATTACH_CAM_TO_ENTITY(NodeCam[0], GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), << 0,0,20>>)
			SET_CAM_ROT(NodeCam[0], <<-88.5, 0, vGamePlayCamRot.z>>)
			SET_CAM_FOV(NodeCam[0], SELECTOR_CAM_DEFAULT_FOV)
		ELSE
			ATTACH_CAM_TO_ENTITY(NodeCam[0], PLAYER_PED_ID(), << 0,0,5>>)
			SET_CAM_ROT(NodeCam[0], <<-88.5, 0, vGamePlayCamRot.z>>)
			SET_CAM_FOV(NodeCam[0], SELECTOR_CAM_DEFAULT_FOV)			
		ENDIF
		
		//The high point above Trevor
		IF NOT DOES_CAM_EXIST(NodeCam[1])
			NodeCam[1] = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", true)
		ENDIF 
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			IF IS_PED_IN_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				ATTACH_CAM_TO_ENTITY(NodeCam[1], GET_VEHICLE_PED_IS_IN(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]), <<0.0, 0.0, 25>>, TRUE)
				SET_CAM_ROT(NodeCam[1], <<-88.5, 0, vGamePlayCamRot.z>>)
				SET_CAM_FOV(NodeCam[1], SELECTOR_CAM_DEFAULT_FOV)			
			ELSE
				ATTACH_CAM_TO_ENTITY(NodeCam[1], sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], <<0.0, 0.0, 5>>, TRUE)
				SET_CAM_ROT(NodeCam[1], <<-88.5, 0, vGamePlayCamRot.z>>)
				SET_CAM_FOV(NodeCam[1], SELECTOR_CAM_DEFAULT_FOV)
			ENDIF
		ENDIF
		
		
		//Add the nodes in order
		ADD_CAM_SPLINE_NODE(sCamDetails.camID, GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), 0, CAM_SPLINE_NODE_SMOOTH_ROT)//starting with gamecam coords and rotation
		ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, NodeCam[0], 500, CAM_SPLINE_NODE_SMOOTH_ROT)//5m's above player
		ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, NodeCam[1], 2000, CAM_SPLINE_NODE_SMOOTH_ROT)//high above player
		ADD_CAM_SPLINE_NODE_USING_GAMEPLAY_FRAME(sCamDetails.camID, 500)//Add the final camera node attached to new player model
				
		
		sCamDetails.bSplineCreated = TRUE
		sCamDetails.camType = SELECTOR_CAM_DEFAULT
		
		sCamDetails.pedTo = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
		
		RETURN TRUE
		
	ENDIF 
	
	RETURN FALSE 	

ENDFUNC



////PURPOSE: Handles all the help text for using the Hunter. Should only be called when the player is using the Hunter.
//PROC HANDLE_HUNTER_HELP_TEXT()
//
//	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//	  	IF DOES_ENTITY_EXIST(BuddyHeli)
//			IF IS_VEHICLE_DRIVEABLE(BuddyHeli)
//				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), BuddyHeli)
//					IF DoneHelp1 = FALSE
//						PRINT_HELP("DP_HELP2")
//						//Hold ~INPUT_VEH_ACCELERATE~ to fly up.
//						DoneHelp1 = TRUE
//					ELSE
//						IF DoneHelp2 = FALSE
//							IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//							AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
//								CLEAR_HELP()
//								PRINT_HELP("DP_HELP3")
//								//Hold ~INPUT_VEH_BRAKE~ to fly down.
//								DoneHelp2 = TRUE
//							ENDIF
//							IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//								PRINT_HELP("DP_HELP3")
//								//Hold ~INPUT_VEH_BRAKE~ to fly down.
//								DoneHelp2 = TRUE
//							ENDIF
//						ELSE	
//							IF DoneHelp3 = FALSE
//								IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//								AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
//									CLEAR_HELP()
//									PRINT_HELP("DP_HELP4")
//									//Hold ~INPUT_VEH_FLY_YAW_LEFT~ to yaw left.
//									DoneHelp3 = TRUE
//								ENDIF
//								IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//									PRINT_HELP("DP_HELP4")
//									//Hold ~INPUT_VEH_FLY_YAW_LEFT~ to yaw left.
//									DoneHelp3 = TRUE
//								ENDIF
//							ELSE
//								IF DoneHelp4 = FALSE
//									IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//									AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_LEFT)
//										CLEAR_HELP()
//										PRINT_HELP("DP_HELP5")
//										//Hold ~INPUT_VEH_FLY_YAW_RIGHT~ to yaw right.
//										DoneHelp4 = TRUE
//									ENDIF
//									IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//										PRINT_HELP("DP_HELP5")
//										//Hold ~INPUT_VEH_FLY_YAW_RIGHT~ to yaw right.
//										DoneHelp4 = TRUE
//									ENDIF
//								ELSE
//									IF DoneHelp5 = FALSE
//										IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//										AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_YAW_RIGHT)
//											CLEAR_HELP()
//											PRINT_HELP("DP_HELP7")
//											//Hold ~INPUT_VEH_FLY_PITCH_UD~ to move forward/backward.
//											DoneHelp5 = TRUE
//										ENDIF
//										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//											PRINT_HELP("DP_HELP7")
//											//Hold ~INPUT_VEH_FLY_PITCH_UD~ to move forward/backward.
//											DoneHelp5 = TRUE
//										ENDIF
//									ELSE
//										IF DoneHelp6 = FALSE
//											IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//											AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_PITCH_UD)
//												CLEAR_HELP()
//												PRINT_HELP("DP_HELP1")
//												//Press ~INPUT_JUMP~ to toggle weapon.
//												DoneHelp6 = TRUE
//											ENDIF
//											IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//												PRINT_HELP("DP_HELP1")
//												//Press ~INPUT_JUMP~ to toggle weapon.
//												DoneHelp6 = TRUE
//											ENDIF
//										ELSE
//											IF DoneHelp7 = FALSE
//												IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//												AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_JUMP)
//													CLEAR_HELP()
//													PRINT_HELP("DP_HELP6")
//													//Press ~INPUT_VEH_FLY_ATTACK~ to fire weapon.
//													DoneHelp7 = TRUE
//												ENDIF
//												IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//													PRINT_HELP("DP_HELP6")
//													//Press ~INPUT_VEH_FLY_ATTACK~ to fire weapon.
//													DoneHelp7 = TRUE
//												ENDIF
//											ELSE
//												IF DoneHelp8 = FALSE
//													GET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(), PlayerVehicleWeapon)
//													IF PlayerVehicleWeapon = WEAPONTYPE_VEHICLE_SPACE_ROCKET
//														IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//														AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
//															CLEAR_HELP()
//															PRINT_HELP("DP_HELP8")
//															//Press ~INPUT_VEH_FLY_SELECT_TARGET_LEFT~ or ~INPUT_VEH_FLY_SELECT_TARGET_RIGHT~ to switch targets when using rockets.
//															DoneHelp8 = TRUE
//														ENDIF
//														IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//															PRINT_HELP("DP_HELP8")
//															//Press ~INPUT_VEH_FLY_SELECT_TARGET_LEFT~ or ~INPUT_VEH_FLY_SELECT_TARGET_RIGHT~ to switch targets when using rockets.
//															DoneHelp8 = TRUE
//														ENDIF
//													ENDIF
//												ELSE
//													IF ClearedFinalHelp = FALSE
//														IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//														AND IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_LEFT)
//														OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_FLY_SELECT_TARGET_RIGHT)
//															CLEAR_HELP()
//															ClearedFinalHelp = TRUE
//														ENDIF	
//													ENDIF
//												ENDIF
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ELSE
//					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//						CLEAR_HELP()
//					ENDIF
//				ENDIF
//			ELSE
//				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//					CLEAR_HELP()
//				ENDIF
//			ENDIF
//		ELSE
//			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//				CLEAR_HELP()
//			ENDIF
//		ENDIF
//	ELSE
//		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//			CLEAR_HELP()
//		ENDIF
//	ENDIF
//		
//ENDPROC

PROC HANDLE_ARMY_JETS()
	
	ArmyJet1 = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 300, LAZER, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
	ArmyJet2 = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 300, LAZER, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES|VEHICLE_SEARCH_FLAG_RETURN_PLANES_ONLY)
	
	IF DOES_ENTITY_EXIST(ArmyJet1)
	AND IS_ENTITY_IN_AIR(ArmyJet1)
		IF NOT DOES_BLIP_EXIST(ArmyJet1Blip)
			ArmyJet1Blip = ADD_BLIP_FOR_ENTITY(ArmyJet1)
			SET_BLIP_AS_FRIENDLY(ArmyJet1Blip, FALSE)
			PRINTSTRING("plane 1 blipped")
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(ArmyJet2)
	AND IS_ENTITY_IN_AIR(ArmyJet2)
		IF NOT DOES_BLIP_EXIST(ArmyJet2Blip)
			ArmyJet2Blip = ADD_BLIP_FOR_ENTITY(ArmyJet2)
			SET_BLIP_AS_FRIENDLY(ArmyJet2Blip, FALSE)
			PRINTSTRING("plane 2 blipped")
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Handles spawning of enemy helicopters.
PROC HANDLE_ENEMY_CHOPPERS()
	
	//Update enemyhelistate depending on what the player is doing
	IF missionStage = STAGE_ESCAPE_MILITARY
		IF DOES_ENTITY_EXIST(HeliToSteal)
			IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
					vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
					IF DOES_ENTITY_EXIST(EnemyHeli[0])
						IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
							IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
								IF NOT IS_PED_INJURED(EnemyHeliPilot[0])	
									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyHeliPilot[0]) < 600
					
										IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, <<-75.8, -819.9, 326>>) > 1600
										AND GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, <<-2159, 3067, 33>>) < 1000
											//Check players height if in heli. 
											IF vPlayerCoords.z > 70
												IF EnemyHeliState <> ENEMY_HELI_ATTACK_PLAYER_VEHICLE_HIGH
													TaskNeedsUpdated[0] = TRUE
													PRINTSTRING("EnemyHeliState = ENEMY_HELI_ATTACK_PLAYER_VEHICLE_HIGH") PRINTNL()
													EnemyHeliState = ENEMY_HELI_ATTACK_PLAYER_VEHICLE_HIGH
												ENDIF
											ELSE
												IF EnemyHeliState <> ENEMY_HELI_ATTACK_PLAYER_VEHICLE_LOW
													TaskNeedsUpdated[0] = TRUE
													PRINTSTRING("EnemyHeliState = ENEMY_HELI_ATTACK_PLAYER_VEHICLE_LOW") PRINTNL()
													EnemyHeliState = ENEMY_HELI_ATTACK_PLAYER_VEHICLE_LOW
												ENDIF	
											ENDIF
											IF doneChat53 = TRUE
												IF doneChat54 = FALSE
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														IF NOT IS_MESSAGE_BEING_DISPLAYED()
														OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
															ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "PilotDispatch")
															ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, NULL, "PilotDispatch2")
															IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT54", CONV_PRIORITY_MEDIUM)
																//Zancudu One to base, enemy has strayed from the met area, requesting permission to engage. 
																//Base to Zancudu One, permission granted. Light up that son of a bitch.
																//Roger that. He's going down. 
																doneChat54 = TRUE
															ENDIF
														ENDIF
													ENDIF								
												ENDIF
											ENDIF
										ELSE
											IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, <<-75.8, -819.9, 326>>) < 1600
												IF EnemyHeliState <> ENEMY_HELI_BACK_OFF_PLAYER
													TaskNeedsUpdated[0] = TRUE
													PRINTSTRING("EnemyHeliState = ENEMY_HELI_BACK_OFF_PLAYER player has entered city air space") PRINTNL()
													EnemyHeliState = ENEMY_HELI_BACK_OFF_PLAYER
												ENDIF
												IF doneChat53 = FALSE
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														IF NOT IS_MESSAGE_BEING_DISPLAYED()
														OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
															ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "PilotDispatch")
															ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, NULL, "PilotDispatch2")
															IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT53", CONV_PRIORITY_MEDIUM)
																//Zancudu One to base, enemy's entered met area airspace. Awaiting instructions. 
																//Base to Zancudu One, do not engage in met area airspace. Hold for orders. Over. 
																//Roger that. 
																doneChat53 = TRUE
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
											//If player is further than 1000metres away from the base then slow the enemy heli down so player can get away from it.
											IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, <<-2159, 3067, 33>>) > 1000
												IF EnemyHeliState <> ENEMY_HELI_BACK_OFF_PLAYER
													TaskNeedsUpdated[0] = TRUE
													PRINTSTRING("EnemyHeliState = ENEMY_HELI_BACK_OFF_PLAYER player is further than 1000m from army base") PRINTNL()
													EnemyHeliState = ENEMY_HELI_BACK_OFF_PLAYER
												ENDIF	
											ENDIF
										ENDIF
									ELSE
										IF EnemyHeliState <> ENEMY_HELI_FLEE
											TaskNeedsUpdated[0] = TRUE
											PRINTSTRING("EnemyHeliState = ENEMY_HELI_FLEE") PRINTNL()
											EnemyHeliState = ENEMY_HELI_FLEE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, <<-75.8, -819.9, 326>>) > 1600
						//Set enemy heli task to attack player on ground if out vehicle
						IF EnemyHeliState <> ENEMY_HELI_ATTACK_PLAYER_PED_ON_FOOT
							TaskNeedsUpdated[0] = TRUE
							PRINTSTRING("EnemyHeliState = ENEMY_HELI_ATTACK_PLAYER_PED_ON_FOOT") PRINTNL()
							EnemyHeliState = ENEMY_HELI_ATTACK_PLAYER_PED_ON_FOOT
						ENDIF	
						IF doneChat53 = TRUE
							IF doneChat54 = FALSE
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_MESSAGE_BEING_DISPLAYED()
									OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
										ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "PilotDispatch")
										ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, NULL, "PilotDispatch2")
										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT54", CONV_PRIORITY_MEDIUM)
											//Zancudu One to base, enemy has strayed from the met area, requesting permission to engage. 
											//Base to Zancudu One, permission granted. Light up that son of a bitch.
											//Roger that. He's going down. 
											doneChat54 = TRUE
										ENDIF
									ENDIF
								ENDIF								
							ENDIF
						ENDIF
					ELSE
						IF EnemyHeliState <> ENEMY_HELI_BACK_OFF_PLAYER
							TaskNeedsUpdated[0] = TRUE
							PRINTSTRING("EnemyHeliState = ENEMY_HELI_BACK_OFF_PLAYER") PRINTNL()	
							EnemyHeliState = ENEMY_HELI_BACK_OFF_PLAYER
						ENDIF
						IF doneChat53 = FALSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, NULL, "PilotDispatch")
									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 5, NULL, "PilotDispatch2")
									IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT53", CONV_PRIORITY_MEDIUM)
										//Zancudu One to base, enemy's entered met area airspace. Awaiting instructions. 
										//Base to Zancudu One, do not engage in met area airspace. Hold for orders. Over. 
										//Roger that. 
										doneChat53 = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
//	IF NOT DOES_ENTITY_EXIST(EnemyHeli[1])
//		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[2])
//			IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[2])
//				SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[2], TRUE, TRUE)
//				EnemyHeli[1] = g_sTriggerSceneAssets.veh[2]
//				SET_VEHICLE_CAN_BE_TARGETTED(EnemyHeli[1], TRUE)
//				SET_ENTITY_LOAD_COLLISION_FLAG(EnemyHeli[1], TRUE)	
//				FREEZE_ENTITY_POSITION(EnemyHeli[1], FALSE)
//				ADD_ENTITY_TO_AUDIO_MIX_GROUP(EnemyHeli[1], "DH_P_2B_ENEMY_CHOPPERS")
//			ENDIF
//		ENDIF
//	ENDIF
	
	//Give the pilots their relevant tasks dependant on which stage of the mission player is in.
	IF EnemyHeliPilotTaskGiven[0] = FALSE
		//Tell him to attack any enemies around his area and set up his defensive zone around his heli.
		IF missionStage = STAGE_STEAL_CARGOBOB
			IF DOES_ENTITY_EXIST(EnemyHeli[0])
				IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
					IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
						IF NOT IS_PED_INJURED(EnemyHeliPilot[0])
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyHeliPilot[0]) < 200
								vEnemyHeli0Coords = GET_ENTITY_COORDS(EnemyHeli[0])
								CLEAR_PED_TASKS(EnemyHeliPilot[0])
								SET_PED_SPHERE_DEFENSIVE_AREA(EnemyHeliPilot[0], vEnemyHeli0Coords, 10)
								TASK_COMBAT_HATED_TARGETS_AROUND_PED(EnemyHeliPilot[0], 250)
								EnemyHeliPilotTaskGiven[0] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		//Tell him to get in heli and attack now
		IF missionStage = STAGE_ESCAPE_MILITARY
			IF DOES_ENTITY_EXIST(EnemyHeli[0])
				IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
					IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
						IF NOT IS_PED_INJURED(EnemyHeliPilot[0])
							IF NOT DOES_BLIP_EXIST(EnemyHeliPilotBlip[0])
								EnemyHeliPilotBlip[0] = CREATE_BLIP_FOR_ENTITY(EnemyHeliPilot[0], TRUE)
							ENDIF
							vEnemyHeli0Coords = GET_ENTITY_COORDS(EnemyHeli[0])
							CLEAR_PED_TASKS(EnemyHeliPilot[0])
//							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyHeliPilot[0], TRUE)
							OPEN_SEQUENCE_TASK(seq)
								IF NOT IS_PED_IN_VEHICLE(EnemyHeliPilot[0],EnemyHeli[0])
									TASK_ENTER_VEHICLE(NULL, EnemyHeli[0])
								ENDIF
								TASK_HELI_MISSION(NULL, EnemyHeli[0], NULL, NULL, <<vEnemyHeli0Coords.x,vEnemyHeli0Coords.y, (vEnemyHeli0Coords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 170, 40)
								TASK_HELI_MISSION(NULL, EnemyHeli[0], NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_ATTACK, 40, 20, -1, 170, 40)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(EnemyHeliPilot[0], seq)
							CLEAR_SEQUENCE_TASK(seq)
							EnemyHeliPilotTaskGiven[0] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		//Set the heli's weapon to rockets once pilot is inside.
		IF missionStage = STAGE_ESCAPE_MILITARY
		
			//Control the enemy pilots accuracy and shooting pattern
			IF DOES_ENTITY_EXIST(EnemyHeli[0])
				IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])	
					IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
						IF NOT IS_PED_INJURED(EnemyHeliPilot[0])
							IF IS_PED_IN_VEHICLE(EnemyHeliPilot[0],EnemyHeli[0])
								//Allow for 90 seconds before upping the accuracy
								IF GET_GAME_TIMER() > (iEnemyPilotAccuracyTimer + 90000)
									IF Accuracy100 = FALSE
										SET_PED_ACCURACY(EnemyHeliPilot[0], 100)
										Accuracy0 = FALSE
										Accuracy100 = TRUE
									ENDIF
								ELSE
									//Set the enemy heli accuracy to 100 if player gets out the cargobob/
									IF GET_GAME_TIMER() > (iEnemyPilotAccuracyTimer + 10000)
									AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
										IF Accuracy100 = FALSE
											SET_PED_ACCURACY(EnemyHeliPilot[0], 100)
											Accuracy0 = FALSE
											Accuracy100 = TRUE
										ENDIF
									ELSE
										IF Accuracy0 = FALSE
											SET_PED_ACCURACY(EnemyHeliPilot[0], 0)
											Accuracy100 = FALSE
											Accuracy0 = TRUE
										ENDIF
									ENDIF
								ENDIF
								//Only allow pilot to fire if less than 300metres away
								IF GET_DISTANCE_BETWEEN_ENTITIES(EnemyHeli[0], PLAYER_PED_ID()) < 300
									IF FiringTypeRocket = FALSE
										SET_CURRENT_PED_VEHICLE_WEAPON(EnemyHeliPilot[0], WEAPONTYPE_VEHICLE_SPACE_ROCKET)
										SET_PED_FIRING_PATTERN(EnemyHeliPilot[0], FIRING_PATTERN_BURST_FIRE_HELI)
										FiringTypeBullet = FALSE
										FiringTypeRocket = TRUE
									ENDIF
								ELSE
									IF FiringTypeBullet = FALSE
										SET_CURRENT_PED_VEHICLE_WEAPON(EnemyHeliPilot[0], WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
										SET_PED_FIRING_PATTERN(EnemyHeliPilot[0], FIRING_PATTERN_SINGLE_SHOT)
										FiringTypeRocket = FALSE
										FiringTypeBullet = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
					
			IF Heli0RocketsEquipped = FALSE
				IF DOES_ENTITY_EXIST(EnemyHeli[0])
					IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])	
						IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
							IF NOT IS_PED_INJURED(EnemyHeliPilot[0])
								IF IS_PED_IN_VEHICLE(EnemyHeliPilot[0],EnemyHeli[0])
									SET_PED_ACCURACY(EnemyHeliPilot[0], 0)
									SET_CURRENT_PED_VEHICLE_WEAPON(EnemyHeliPilot[0], WEAPONTYPE_VEHICLE_SPACE_ROCKET)
									Heli0RocketsEquipped = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			SWITCH EnemyHeliState
				CASE ENEMY_HELI_ATTACK_PLAYER_PED_ON_FOOT
					IF TaskNeedsUpdated[0] = TRUE
						IF DOES_ENTITY_EXIST(EnemyHeli[0])
							IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
								IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
									IF NOT IS_PED_INJURED(EnemyHeliPilot[0])	
										TASK_HELI_MISSION(EnemyHeliPilot[0], EnemyHeli[0], NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_ATTACK, 20, 20, -1, 15, 10)
										TaskNeedsUpdated[0] = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE ENEMY_HELI_ATTACK_PLAYER_VEHICLE_LOW
					IF TaskNeedsUpdated[0] = TRUE
						IF DOES_ENTITY_EXIST(EnemyHeli[0])
							IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
								IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
									IF NOT IS_PED_INJURED(EnemyHeliPilot[0])	
										TASK_HELI_MISSION(EnemyHeliPilot[0], EnemyHeli[0], NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_ATTACK, 40, 20, -1, 50, 20)
										TaskNeedsUpdated[0] = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF				
				BREAK
				CASE ENEMY_HELI_ATTACK_PLAYER_VEHICLE_HIGH
					IF TaskNeedsUpdated[0] = TRUE
						IF DOES_ENTITY_EXIST(EnemyHeli[0])
							IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
								IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
									IF NOT IS_PED_INJURED(EnemyHeliPilot[0])	
										TASK_HELI_MISSION(EnemyHeliPilot[0], EnemyHeli[0], NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_ATTACK, 40, 20, -1, 170, 70)
										TaskNeedsUpdated[0] = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF				
				BREAK
				CASE ENEMY_HELI_BACK_OFF_PLAYER
					IF TaskNeedsUpdated[0] = TRUE
						IF DOES_ENTITY_EXIST(EnemyHeli[0])
							IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
								IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
									IF NOT IS_PED_INJURED(EnemyHeliPilot[0])	
										TASK_HELI_MISSION(EnemyHeliPilot[0], EnemyHeli[0], NULL, NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(EnemyHeli[0], <<20,0,0>>), MISSION_GOTO, 10, 10, GET_ENTITY_HEADING(EnemyHeli[0]), ROUND(GET_ENTITY_HEIGHT(EnemyHeli[0], GET_ENTITY_COORDS(EnemyHeli[0]), TRUE, TRUE)) , 100)
										TaskNeedsUpdated[0] = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF	
					//Slow down the heli if player is further than 1000m from the army base
					IF TaskToSlowDownGiven = FALSE
						IF GET_DISTANCE_BETWEEN_COORDS(vPlayerCoords, <<-2159, 3067, 33>>) > 1000
							IF DOES_ENTITY_EXIST(EnemyHeli[0])
								IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
									IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
										IF NOT IS_PED_INJURED(EnemyHeliPilot[0])	
											TASK_HELI_MISSION(EnemyHeliPilot[0], EnemyHeli[0], NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_ATTACK, 20, 20, -1, 170, 70)
											TaskToSlowDownGiven = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE ENEMY_HELI_FLEE
					IF TaskNeedsUpdated[0] = TRUE
						IF DOES_ENTITY_EXIST(EnemyHeli[0])
							IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
								IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
									IF NOT IS_PED_INJURED(EnemyHeliPilot[0])	
										TASK_HELI_MISSION(EnemyHeliPilot[0], EnemyHeli[0], NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_FLEE, 40, 50, -1, 200, 170)
										TaskNeedsUpdated[0] = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
						
		ENDIF
	ENDIF
	
//	IF EnemyHeliPilotTaskGiven[1] = FALSE
//		//Tell him to attack any enemies around his area and set up his defensive zone around his heli.
//		IF missionStage = STAGE_STEAL_CARGOBOB
//			IF DOES_ENTITY_EXIST(EnemyHeli[1])
//				IF IS_VEHICLE_DRIVEABLE(EnemyHeli[1])
//					IF DOES_ENTITY_EXIST(EnemyHeliPilot[1])
//						IF NOT IS_PED_INJURED(EnemyHeliPilot[1])
//							vEnemyHeli1Coords = GET_ENTITY_COORDS(EnemyHeli[1])
//							CLEAR_PED_TASKS(EnemyHeliPilot[1])
//							SET_PED_SPHERE_DEFENSIVE_AREA(EnemyHeliPilot[1], vEnemyHeli1Coords, 10)
//							TASK_COMBAT_HATED_TARGETS_AROUND_PED(EnemyHeliPilot[1], 1000)
//							EnemyHeliPilotTaskGiven[1] = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF	
//		
//		//Tell him to get in heli and attack now
//		IF missionStage = STAGE_ESCAPE_MILITARY
//			IF DOES_ENTITY_EXIST(EnemyHeli[1])
//				IF IS_VEHICLE_DRIVEABLE(EnemyHeli[1])
//					IF DOES_ENTITY_EXIST(EnemyHeliPilot[1])
//						IF NOT IS_PED_INJURED(EnemyHeliPilot[1])
//							IF NOT DOES_BLIP_EXIST(EnemyHeliPilotBlip[1])
//								EnemyHeliPilotBlip[1] = CREATE_BLIP_FOR_ENTITY(EnemyHeliPilot[1], TRUE)
//							ENDIF
//							vEnemyHeli1Coords = GET_ENTITY_COORDS(EnemyHeli[1])
//							CLEAR_PED_TASKS(EnemyHeliPilot[1])
//							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(EnemyHeliPilot[1], TRUE)
//							OPEN_SEQUENCE_TASK(seq)
//								IF NOT IS_PED_IN_VEHICLE(EnemyHeliPilot[1],EnemyHeli[1])
//									TASK_ENTER_VEHICLE(NULL, EnemyHeli[1])
//								ENDIF
//								TASK_HELI_MISSION(NULL, EnemyHeli[1], NULL, NULL, <<vEnemyHeli1Coords.x,vEnemyHeli1Coords.y, (vEnemyHeli1Coords.z + 20)>>, MISSION_GOTO, 30, 10, -1, 170, 40)
//								TASK_HELI_MISSION(NULL, EnemyHeli[1], NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_ATTACK, 40, 45, -1, 190, 60)
//							CLOSE_SEQUENCE_TASK(seq)
//							TASK_PERFORM_SEQUENCE(EnemyHeliPilot[1], seq)
//							CLEAR_SEQUENCE_TASK(seq)
//							EnemyHeliPilotTaskGiven[1] = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	ELSE
//		//Set the heli's weapon to rockets once pilot is inside.
//		IF missionStage = STAGE_ESCAPE_MILITARY
//			
//			IF Heli1RocketsEquipped = FALSE
//				IF DOES_ENTITY_EXIST(EnemyHeli[1])
//					IF IS_VEHICLE_DRIVEABLE(EnemyHeli[1])	
//						IF DOES_ENTITY_EXIST(EnemyHeliPilot[1])
//							IF NOT IS_PED_INJURED(EnemyHeliPilot[1])
//								IF IS_PED_IN_VEHICLE(EnemyHeliPilot[1],EnemyHeli[1])
//									SET_PED_ACCURACY(EnemyHeliPilot[1], 0)
//									SET_CURRENT_PED_VEHICLE_WEAPON(EnemyHeliPilot[1], WEAPONTYPE_VEHICLE_SPACE_ROCKET)
//									Heli1RocketsEquipped = TRUE
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			SWITCH EnemyHeliState
//				CASE ENEMY_HELI_ATTACK_PLAYER_PED_ON_FOOT
//					IF TaskNeedsUpdated[1] = TRUE
//						IF DOES_ENTITY_EXIST(EnemyHeli[1])
//							IF IS_VEHICLE_DRIVEABLE(EnemyHeli[1])
//								IF DOES_ENTITY_EXIST(EnemyHeliPilot[1])
//									IF NOT IS_PED_INJURED(EnemyHeliPilot[1])	
//										TASK_HELI_MISSION(EnemyHeliPilot[1], EnemyHeli[1], NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_ATTACK, 20, 30, -1, 15, 10)
//										TaskNeedsUpdated[1] = FALSE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				BREAK
//				CASE ENEMY_HELI_ATTACK_PLAYER_VEHICLE_LOW
//					IF TaskNeedsUpdated[1] = TRUE
//						IF DOES_ENTITY_EXIST(EnemyHeli[1])
//							IF IS_VEHICLE_DRIVEABLE(EnemyHeli[1])
//								IF DOES_ENTITY_EXIST(EnemyHeliPilot[1])
//									IF NOT IS_PED_INJURED(EnemyHeliPilot[1])	
//										TASK_HELI_MISSION(EnemyHeliPilot[1], EnemyHeli[1], NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_ATTACK, 40, 30, -1, 50, 20)
//										TaskNeedsUpdated[1] = FALSE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF				
//				BREAK
//				CASE ENEMY_HELI_ATTACK_PLAYER_VEHICLE_HIGH
//					IF TaskNeedsUpdated[1] = TRUE
//						IF DOES_ENTITY_EXIST(EnemyHeli[1])
//							IF IS_VEHICLE_DRIVEABLE(EnemyHeli[1])
//								IF DOES_ENTITY_EXIST(EnemyHeliPilot[1])
//									IF NOT IS_PED_INJURED(EnemyHeliPilot[1])	
//										TASK_HELI_MISSION(EnemyHeliPilot[1], EnemyHeli[1], NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_ATTACK, 40, 30, -1, 170, 70)
//										TaskNeedsUpdated[1] = FALSE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF				
//				BREAK
//				CASE ENEMY_HELI_BACK_OFF_PLAYER
//					IF TaskNeedsUpdated[1] = TRUE
//						IF DOES_ENTITY_EXIST(EnemyHeli[1])
//							IF IS_VEHICLE_DRIVEABLE(EnemyHeli[1])
//								IF DOES_ENTITY_EXIST(EnemyHeliPilot[1])
//									IF NOT IS_PED_INJURED(EnemyHeliPilot[1])	
//										TASK_HELI_MISSION(EnemyHeliPilot[1], EnemyHeli[0], NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_FOLLOW, 40, 70, -1, 220, 200)
//										TaskNeedsUpdated[1] = FALSE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF				
//				BREAK
//			ENDSWITCH		
//		
//		ENDIF
//	ENDIF	
	
	//Run checks to see if the pilots or heli's have been destroyed, to be used for moving stage on to final stage.
	// If the pilot is dead
	IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
		IF IS_PED_INJURED(EnemyHeliPilot[0])
			IF DOES_BLIP_EXIST(EnemyHeliPilotBlip[0])
				REMOVE_BLIP(EnemyHeliPilotBlip[0])
			ENDIF
			EnemyHeliDown[0] = TRUE
		ENDIF
	ELSE			
		IF DOES_BLIP_EXIST(EnemyHeliPilotBlip[0])
			REMOVE_BLIP(EnemyHeliPilotBlip[0])
		ENDIF
		EnemyHeliDown[0] = TRUE
	ENDIF
	//If the heli is destroyed
	IF DOES_ENTITY_EXIST(EnemyHeli[0])
		IF NOT IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
			IF DOES_BLIP_EXIST(EnemyHeliPilotBlip[0])
				REMOVE_BLIP(EnemyHeliPilotBlip[0])
			ENDIF
			EnemyHeliDown[0] = TRUE
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(EnemyHeliPilotBlip[0])
			REMOVE_BLIP(EnemyHeliPilotBlip[0])
		ENDIF
		EnemyHeliDown[0] = TRUE
	ENDIF	
	//Or if the player is more than 500ms from the heli
	IF DOES_ENTITY_EXIST(EnemyHeli[0])
		IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
			IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
				IF NOT IS_PED_INJURED(EnemyHeliPilot[0])
					IF IS_PED_IN_VEHICLE(EnemyHeliPilot[0], EnemyHeli[0])
						IF DOES_BLIP_EXIST(EnemyHeliPilotBlip[0])
							SET_BLIP_SCALE(EnemyHeliPilotBlip[0], 1)
						ENDIF					
					ENDIF	
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), EnemyHeliPilot[0]) > 600
						IF DOES_BLIP_EXIST(EnemyHeliPilotBlip[0])
							REMOVE_BLIP(EnemyHeliPilotBlip[0])
						ENDIF
						EnemyHeliDown[0] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
		IF NOT IS_PED_INJURED(EnemyHeliPilot[0])
			IF IS_ENTITY_IN_WATER(EnemyHeliPilot[0])
				CLEAR_PED_TASKS(EnemyHeliPilot[0])
				SET_ENTITY_HEALTH(EnemyHeliPilot[0], 0)
				IF DOES_BLIP_EXIST(EnemyHeliPilotBlip[0])
					REMOVE_BLIP(EnemyHeliPilotBlip[0])
				ENDIF
				EnemyHeliDown[0] = TRUE
			ENDIF
		ENDIF
	ENDIF
	
//	IF DOES_ENTITY_EXIST(EnemyHeliPilot[1])
//		IF IS_PED_INJURED(EnemyHeliPilot[1])
//			IF DOES_BLIP_EXIST(EnemyHeliPilotBlip[1])
//				REMOVE_BLIP(EnemyHeliPilotBlip[1])
//			ENDIF
//			EnemyHeliDown[1] = TRUE
//		ENDIF
//	ELSE
//		IF DOES_BLIP_EXIST(EnemyHeliPilotBlip[1])
//			REMOVE_BLIP(EnemyHeliPilotBlip[1])
//		ENDIF
//		EnemyHeliDown[1] = TRUE
//	ENDIF
//	IF DOES_ENTITY_EXIST(EnemyHeli[1])
//		IF NOT IS_VEHICLE_DRIVEABLE(EnemyHeli[1])
//			IF DOES_BLIP_EXIST(EnemyHeliPilotBlip[1])
//				REMOVE_BLIP(EnemyHeliPilotBlip[1])
//			ENDIF
//			EnemyHeliDown[1] = TRUE
//		ENDIF
//	ELSE
//		IF DOES_BLIP_EXIST(EnemyHeliPilotBlip[1])
//			REMOVE_BLIP(EnemyHeliPilotBlip[1])
//		ENDIF
//		EnemyHeliDown[1] = TRUE
//	ENDIF	
	
	
	
//	//Do not create any enemy choppers until Michael has taken off in the Cargobob.
//	IF AllowEnemyHelisToSpawn = FALSE
//		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//			IF DOES_ENTITY_EXIST(HeliToSteal)
//				IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//						IF IS_ENTITY_IN_AIR(HeliToSteal)
//							AllowEnemyHelisToSpawn = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//			IF DOES_ENTITY_EXIST(HeliToSteal)
//				IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//						IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], HeliToSteal)
//							IF IS_ENTITY_IN_AIR(HeliToSteal)
//								AllowEnemyHelisToSpawn = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF					
//	ENDIF
//	
//	//Control for spawning in enemies. Stop spawning them once the Cargobob has got close enough to the hanger.
//	IF DOES_ENTITY_EXIST(HeliToSteal)
//		IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(HeliToSteal), vHanger) < 1500
//				HeliToCloseToHome = TRUE
//			ELSE
//				HeliToCloseToHome = FALSE
//			ENDIF
//			//Control police wanted level
//			IF WantedLevel4Set = FALSE
//				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(HeliToSteal), vHanger) > 2200
//					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 4)
//					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//					WantedLevel2Set = FALSE
//					WantedLevel3Set = FALSE
//					WantedLevel4Set = TRUE
//				ENDIF
//			ENDIF
//			IF WantedLevel3Set = FALSE
//				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(HeliToSteal), vHanger) < 2200
//				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(HeliToSteal), vHanger) > 1800
//					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 3)
//					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//					WantedLevel2Set = FALSE
//					WantedLevel4Set = FALSE
//					WantedLevel3Set = TRUE
//				ENDIF
//			ENDIF
//			IF WantedLevel2Set = FALSE
//				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(HeliToSteal), vHanger) < 1800
//					SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 2)
//					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//					WantedLevel3Set = FALSE
//					WantedLevel4Set = FALSE
//					WantedLevel2Set = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF
//	
//	vplayersCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
//	
//	//Create an enemy heli if there is less than 2 alive
//	IF AllowEnemyHelisToSpawn = TRUE
//		PRINTSTRING("AllowEnemyHelisToSpawn = TRUE") PRINTNL()
//		IF HeliToCloseToHome = FALSE
//			//Create 2 BUZZARD helis with pilots and give them their tasks.
//			//Heli 1
//			IF EnemyHeliCreated[0] = FALSE
//	//			PRINTSTRING("EnemyHeliCreated[0] = FALSE") PRINTNL()
//				REQUEST_MODEL(BUZZARD)
//				IF NOT DOES_ENTITY_EXIST(EnemyHeli[0])
//					IF HAS_MODEL_LOADED(BUZZARD)
//						EnemyHeli[0] = CREATE_VEHICLE(BUZZARD, << (vplayersCoords.x + 800), 2685, 200 >>, 91.9264)
//						SET_ENTITY_LOAD_COLLISION_FLAG(EnemyHeli[0], TRUE)
//						SET_HELI_BLADES_FULL_SPEED(EnemyHeli[0])
//						FREEZE_ENTITY_POSITION(EnemyHeli[0], TRUE)
//						SET_MODEL_AS_NO_LONGER_NEEDED(BUZZARD)
//						EnemyHeliCreated[0] = TRUE
//					ENDIF
//				ENDIF
//			ELSE
//	//			PRINTSTRING("EnemyHeliCreated[0] = TRUE") PRINTNL()
//				REQUEST_MODEL(S_M_M_PILOT_01)
//				IF EnemyHeliPilotCreated[0] = FALSE
//					PRINTSTRING("EnemyHeliPilotCreated[0] = FALSE") PRINTNL()
//					IF NOT DOES_ENTITY_EXIST(EnemyHeliPilot[0])
//						IF HAS_MODEL_LOADED(S_M_M_PILOT_01)
//							IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
//								EnemyHeliPilot[0] = CREATE_PED_INSIDE_VEHICLE(EnemyHeli[0], PEDTYPE_MISSION, S_M_M_PILOT_01)
//								SET_PED_ACCURACY(EnemyHeliPilot[0], 5)
//								FREEZE_ENTITY_POSITION(EnemyHeli[0], FALSE)
//								SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_01)
//								EnemyHeliPilotCreated[0] = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ELSE
//	//				PRINTSTRING("EnemyHeliPilotCreated[0] = TRUE") PRINTNL()
//					IF EnemyHeliTaskGiven[0] = FALSE
//						PRINTSTRING("EnemyHeliTaskGiven[0] = FALSE") PRINTNL()
//						IF IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
//							IF NOT IS_PED_INJURED(EnemyHeliPilot[0])
//								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//									SET_CURRENT_PED_VEHICLE_WEAPON(EnemyHeliPilot[0], WEAPONTYPE_VEHICLE_SPACE_ROCKET)
//									TASK_HELI_MISSION(EnemyHeliPilot[0], EnemyHeli[0], NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_ATTACK, 30, 20, -1, 170, 40)
//									EnemyHeliTaskGiven[0] = TRUE
//								ENDIF
//								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//									IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//										SET_CURRENT_PED_VEHICLE_WEAPON(EnemyHeliPilot[0], WEAPONTYPE_VEHICLE_SPACE_ROCKET)
//										TASK_HELI_MISSION(EnemyHeliPilot[0], EnemyHeli[0], NULL, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], <<0,0,0>>, MISSION_ATTACK, 30, 20, -1, 170, 40)
//										EnemyHeliTaskGiven[0] = TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//					IF NOT DOES_ENTITY_EXIST(EnemyHeliPilot[0])
//						EnemyHeliPilotKilled[0] = TRUE
//					ENDIF
//					IF IS_PED_INJURED(EnemyHeliPilot[0])
//						EnemyHeliPilotKilled[0] = TRUE
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			//Heli 2
//			IF EnemyHeliCreated[1] = FALSE
//				REQUEST_MODEL(BUZZARD)
//				IF NOT DOES_ENTITY_EXIST(EnemyHeli[1])
//					IF HAS_MODEL_LOADED(BUZZARD)
//						EnemyHeli[1] = CREATE_VEHICLE(BUZZARD, << (vplayersCoords.x + 900), 2685, 200 >>, 91.9264)
//						SET_ENTITY_LOAD_COLLISION_FLAG(EnemyHeli[1], TRUE)
//						SET_HELI_BLADES_FULL_SPEED(EnemyHeli[1])
//						FREEZE_ENTITY_POSITION(EnemyHeli[1], TRUE)
//						SET_MODEL_AS_NO_LONGER_NEEDED(BUZZARD)
//						EnemyHeliCreated[1] = TRUE
//					ENDIF
//				ENDIF
//			ELSE
//				IF EnemyHeliPilotCreated[1] = FALSE
//					REQUEST_MODEL(S_M_M_PILOT_01)
//					IF NOT DOES_ENTITY_EXIST(EnemyHeliPilot[1])
//						IF HAS_MODEL_LOADED(S_M_M_PILOT_01)
//							IF IS_VEHICLE_DRIVEABLE(EnemyHeli[1])
//								EnemyHeliPilot[1] = CREATE_PED_INSIDE_VEHICLE(EnemyHeli[1], PEDTYPE_MISSION, S_M_M_PILOT_01)
//								SET_PED_ACCURACY(EnemyHeliPilot[1], 5)
//								FREEZE_ENTITY_POSITION(EnemyHeli[1], FALSE)
//								SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_01)
//								EnemyHeliPilotCreated[1] = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ELSE
//					IF EnemyHeliTaskGiven[1] = FALSE
//						IF IS_VEHICLE_DRIVEABLE(EnemyHeli[1])
//							IF NOT IS_PED_INJURED(EnemyHeliPilot[1])
//								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//									SET_CURRENT_PED_VEHICLE_WEAPON(EnemyHeliPilot[1], WEAPONTYPE_VEHICLE_SPACE_ROCKET)
//									TASK_HELI_MISSION(EnemyHeliPilot[1], EnemyHeli[1], NULL, PLAYER_PED_ID(), <<0,0,0>>, MISSION_ATTACK, 30, 35, -1, 180, 60)
//									EnemyHeliTaskGiven[1] = TRUE
//								ENDIF
//								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//									IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//										SET_CURRENT_PED_VEHICLE_WEAPON(EnemyHeliPilot[1], WEAPONTYPE_VEHICLE_SPACE_ROCKET)
//										TASK_HELI_MISSION(EnemyHeliPilot[1], EnemyHeli[1], NULL, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], <<0,0,0>>, MISSION_ATTACK, 30, 35, -1, 180, 60)
//										EnemyHeliTaskGiven[1] = TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF		
//			
//			//Control for creating new heli's. if one is destroyed another should be created in it's place.		
//			IF NOT DOES_ENTITY_EXIST(EnemyHeli[0])
//				EnemyHeliCreated[0] = FALSE
//				IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
//					IF NOT IS_PED_INJURED(EnemyHeliPilot[0])
//						SET_ENTITY_HEALTH(EnemyHeliPilot[0], 0)
//					ELSE
//						SET_PED_AS_NO_LONGER_NEEDED(EnemyHeliPilot[0])
//						EnemyHeliPilotCreated[0] = FALSE
//						EnemyHeliTaskGiven[0] = FALSE
//					ENDIF
//				ELSE
//					EnemyHeliPilotCreated[0] = FALSE
//					EnemyHeliTaskGiven[0] = FALSE
//				ENDIF
//			ELSE
//				IF NOT IS_VEHICLE_DRIVEABLE(EnemyHeli[0])
//					IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
//						IF NOT IS_PED_INJURED(EnemyHeliPilot[0])
//							SET_ENTITY_HEALTH(EnemyHeliPilot[0], 0)
//						ELSE
//							SET_PED_AS_NO_LONGER_NEEDED(EnemyHeliPilot[0])
//							EnemyHeliPilotCreated[0] = FALSE
//							EnemyHeliTaskGiven[0] = FALSE
//						ENDIF
//					ELSE
//						EnemyHeliPilotCreated[0] = FALSE
//						EnemyHeliTaskGiven[0] = FALSE
//					ENDIF
//					SET_VEHICLE_AS_NO_LONGER_NEEDED(EnemyHeli[0])
//					EnemyHeliCreated[0] = FALSE
//				ELSE
//					IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
//						IF NOT IS_PED_INJURED(EnemyHeliPilot[0])	
//							IF NOT IS_PED_IN_VEHICLE(EnemyHeliPilot[0], EnemyHeli[0])
//								SET_PED_AS_NO_LONGER_NEEDED(EnemyHeliPilot[0])
//								EnemyHeliPilotCreated[0] = FALSE
//								EnemyHeliTaskGiven[0] = FALSE	
//							ENDIF
//						ELSE
//							SET_PED_AS_NO_LONGER_NEEDED(EnemyHeliPilot[0])
//							EnemyHeliPilotCreated[0] = FALSE
//							EnemyHeliTaskGiven[0] = FALSE
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			IF NOT DOES_ENTITY_EXIST(EnemyHeli[1])
//				EnemyHeliCreated[1] = FALSE
//				IF DOES_ENTITY_EXIST(EnemyHeliPilot[1])
//					IF NOT IS_PED_INJURED(EnemyHeliPilot[1])
//						SET_ENTITY_HEALTH(EnemyHeliPilot[1], 0)
//					ELSE
//						SET_PED_AS_NO_LONGER_NEEDED(EnemyHeliPilot[1])
//						EnemyHeliPilotCreated[1] = FALSE
//						EnemyHeliTaskGiven[1] = FALSE
//					ENDIF
//				ELSE
//					EnemyHeliPilotCreated[1] = FALSE
//					EnemyHeliTaskGiven[1] = FALSE
//				ENDIF
//			ELSE
//				IF NOT IS_VEHICLE_DRIVEABLE(EnemyHeli[1])
//					EnemyHeliCreated[1] = FALSE
//					IF DOES_ENTITY_EXIST(EnemyHeliPilot[1])
//						IF NOT IS_PED_INJURED(EnemyHeliPilot[1])
//							SET_ENTITY_HEALTH(EnemyHeliPilot[1], 0)
//						ELSE
//							SET_PED_AS_NO_LONGER_NEEDED(EnemyHeliPilot[1])
//							EnemyHeliPilotCreated[1] = FALSE
//							EnemyHeliTaskGiven[1] = FALSE
//						ENDIF
//					ELSE
//						EnemyHeliPilotCreated[1] = FALSE
//						EnemyHeliTaskGiven[1] = FALSE
//					ENDIF
//					SET_VEHICLE_AS_NO_LONGER_NEEDED(EnemyHeli[1])
//					EnemyHeliCreated[1] = FALSE
//				ELSE
//					IF DOES_ENTITY_EXIST(EnemyHeliPilot[1])
//						IF NOT IS_PED_INJURED(EnemyHeliPilot[1])	
//							IF NOT IS_PED_IN_VEHICLE(EnemyHeliPilot[1], EnemyHeli[1])
//								SET_PED_AS_NO_LONGER_NEEDED(EnemyHeliPilot[1])
//								EnemyHeliPilotCreated[1] = FALSE
//								EnemyHeliTaskGiven[1] = FALSE
//							ENDIF
//						ELSE
//							SET_PED_AS_NO_LONGER_NEEDED(EnemyHeliPilot[1])
//							EnemyHeliPilotCreated[1] = FALSE
//							EnemyHeliTaskGiven[1] = FALSE
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ELSE
////			PRINTSTRING("HeliToCloseToHome = TRUE") PRINTNL()
//			//Check for both pilots being killed for moving stage on
//			IF NOT DOES_ENTITY_EXIST(EnemyHeliPilot[0])
//				EnemyHeliPilotKilled[0] = TRUE
//			ELSE
//				IF IS_PED_INJURED(EnemyHeliPilot[0])
//					EnemyHeliPilotKilled[0] = TRUE
//				ENDIF
//			ENDIF		
//			
//			IF NOT DOES_ENTITY_EXIST(EnemyHeliPilot[1])
//				EnemyHeliPilotKilled[1] = TRUE
//			ELSE
//				IF IS_PED_INJURED(EnemyHeliPilot[1])
//					EnemyHeliPilotKilled[1] = TRUE
//				ENDIF
//			ENDIF
//			
//		ENDIF
//	ELSE
//		PRINTSTRING("AllowEnemyHelisToSpawn = FALSE") PRINTNL()
//	ENDIF
	
ENDPROC

//PURPOSE: Handles anything else that needs to be called every frame through out the mission
PROC MISC_CHECK()

		//Disable the hook on the cargobob
		IF DOES_ENTITY_EXIST(HeliToSteal)
			IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
						IF IS_PED_SITTING_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
							IF GET_PED_VEHICLE_SEAT(PLAYER_PED_ID(), HeliToSteal) = VS_DRIVER
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

ENDPROC 

//PURPOSE: Runs a check if the player is in the Military base then keeps the wanted level on all the time
PROC POLICE_CHECK()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		//Don't allow the player to lose their wanted level until last stage.
		IF missionStage = STAGE_STEAL_CARGOBOB
		OR missionStage = STAGE_ESCAPE_MILITARY
			IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_MILITARY_BASE, 200)
			OR AlarmStarted = TRUE
				REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
				SUPPRESS_LOSING_WANTED_LEVEL_IF_HIDDEN_THIS_FRAME(PLAYER_ID())
			ENDIF
		ENDIF
		IF missionStage = STAGE_FLY_CARGOBOB_TO_HANGER
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <> 0
			AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vHeliCoords) > 1000
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


// ===========================================================================================================
//		MAIN MISSION STAGES
// ===========================================================================================================

PROC DO_STAGE_OPENING_CUTSCENE()
	
	//No cutscene at the moment so skip to next stage
	IF NOT IS_REPLAY_IN_PROGRESS()
		//DO OPENING CUTSCENE
		IF iControlFlag = 0 
//		
//			//Create Michael if player started mission as Trevor
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<1723.2087, 3296.8179, 40.2235>>, 247.3402)
//					PRINTSTRING("Waiting for Michael being spawned") PRINTNL()
//					WAIT(0)
//				ENDWHILE
//				WHILE NOT CREATE_PLAYER_VEHICLE(buddyCar, CHAR_MICHAEL, vBuddyCarCoords, fBuddyCarHeading)
//					PRINTSTRING("Waiting for Michaels car being spawned") PRINTNL()
//					WAIT(0)
//				ENDWHILE
//				SET_VEHICLE_ON_GROUND_PROPERLY(buddyCar)
//				SET_VEHICLE_COLOURS(buddyCar, 0, 0)
//				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//					SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], RELGROUPHASH_PLAYER)
//					SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE, FALSE, TRUE, FALSE, FALSE)
//					GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_CARBINERIFLE, 2000, FALSE, FALSE)
//					GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_MICROSMG, 2000, FALSE, FALSE)
//					TASK_GO_TO_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],PLAYER_PED_ID(), -1, DEFAULT_SEEK_RADIUS, PEDMOVEBLENDRATIO_WALK)
//					SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 100)
//					SET_PED_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], ANCHOR_EARS, 0,0)
//					SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
//				ENDIF
//				
//				IF NOT DOES_BLIP_EXIST(BuddyBlip)
//					BuddyBlip = CREATE_BLIP_FOR_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//				ENDIF
//				
//				PRINT_NOW("DP_GOD5", DEFAULT_GOD_TEXT_TIME, 1)//Speak to ~b~Michael.
//				
//				iControlFlag = 1
//			ENDIF
//			
//			//Create Trevor if player started mission as Michael
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], CHAR_TREVOR, <<1723.2087, 3296.8179, 40.2235>>, 247.3402)
//					PRINTSTRING("Waiting for Trevor being spawned") PRINTNL()
//					WAIT(0)
//				ENDWHILE
//				WHILE NOT CREATE_PLAYER_VEHICLE(buddyCar, CHAR_TREVOR, vBuddyCarCoords, fBuddyCarHeading)
//					PRINTSTRING("Waiting for Trevors car being spawned") PRINTNL()
//					WAIT(0)
//				ENDWHILE				
//				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//					SET_PED_PROP_INDEX(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], ANCHOR_EARS, 0,0)
//					SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], RELGROUPHASH_PLAYER)
//					SET_ENTITY_PROOFS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE, FALSE, TRUE, FALSE, FALSE)
//					GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], WEAPONTYPE_MICROSMG, 2000, FALSE, FALSE)
//					GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], WEAPONTYPE_CARBINERIFLE, 2000, FALSE, FALSE)
//					TASK_GO_TO_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],PLAYER_PED_ID(), -1, DEFAULT_SEEK_RADIUS, PEDMOVEBLENDRATIO_WALK)
//					SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 100)
//					SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE)
//				ENDIF
//				IF NOT DOES_BLIP_EXIST(BuddyBlip)
//					BuddyBlip = CREATE_BLIP_FOR_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//				ENDIF
//				
//				PRINT_NOW("DP_GOD6", DEFAULT_GOD_TEXT_TIME, 1)//Speak to ~b~Trevor.
//				
//				iControlFlag = 1
//			ENDIF	
//
//			
//		ENDIF
//		
//		IF iControlFlag = 1
//			
//			IF NOT IS_SCREEN_FADED_IN()
//				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//			ENDIF
//			
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//				IF GET_DISTANCE_BETWEEN_ENTITIES(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_PED_ID()) < 5
//					CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//					SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], PLAYER_GROUP_ID())
//					IF DOES_BLIP_EXIST(BuddyBlip)
//						REMOVE_BLIP(BuddyBlip)
//					ENDIF
//					iControlFlag = 2
//				ENDIF
//			ENDIF
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//				IF GET_DISTANCE_BETWEEN_ENTITIES(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_PED_ID()) < 5
//					CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//					SET_PED_AS_GROUP_MEMBER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PLAYER_GROUP_ID())
//					IF DOES_BLIP_EXIST(BuddyBlip)
//						REMOVE_BLIP(BuddyBlip)
//					ENDIF					
//					iControlFlag = 2
//				ENDIF
//			ENDIF			
//			
//		ENDIF

			missionStage = STAGE_INIT_MISSION
		ENDIF
	ELSE
		iControlFlag = 0
		missionStage = STAGE_INIT_MISSION
	ENDIF
	
ENDPROC

PROC DO_STAGE_INIT_MISSION()

	IF iControlFlag = 0		
		
        ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE, FALSE)
        ALLOW_DISPATCH(DT_POLICE_HELICOPTER, FALSE)
        ALLOW_DISPATCH(DT_POLICE_VEHICLE_REQUEST, FALSE)
        ALLOW_DISPATCH(DT_POLICE_ROAD_BLOCK, FALSE)
        ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, FALSE)
        ALLOW_DISPATCH(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, FALSE)
        ALLOW_DISPATCH(DT_SWAT_AUTOMOBILE, FALSE)
        ALLOW_DISPATCH(DT_SWAT_HELICOPTER, FALSE)	
		
		ADD_RELATIONSHIP_GROUP("MarineGroupHash", MARINE_GROUP_HASH)
		
		//Have the marine group and army group both hate the player group
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, MARINE_GROUP_HASH, RELGROUPHASH_PLAYER)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, RELGROUPHASH_AMBIENT_ARMY, RELGROUPHASH_PLAYER)
		
		//Have the marine, army and cop groups all like each other 
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, MARINE_GROUP_HASH, RELGROUPHASH_AMBIENT_ARMY)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_ARMY, MARINE_GROUP_HASH)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, MARINE_GROUP_HASH)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, MARINE_GROUP_HASH, RELGROUPHASH_COP)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, RELGROUPHASH_AMBIENT_ARMY)
		SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_AMBIENT_ARMY, RELGROUPHASH_COP)
		
		//Check if the mission is using a replay, or a shitskip.
	    IF Is_Replay_In_Progress()
			//Check if player chose to shitskip
	        IF g_bShitskipAccepted = TRUE
				IF GET_REPLAY_MID_MISSION_STAGE() = 0
					START_REPLAY_SETUP(vHeliCoords, fHeliHeading)
					MissionStageBeingSkippedTo = TRUE
					iControlFlag = 0
					missionStage = STAGE_ESCAPE_MILITARY
				ENDIF
				IF GET_REPLAY_MID_MISSION_STAGE() = 1
					START_REPLAY_SETUP(<<-629.1677, 2871.3101, 385.0313>>, 283.1214)
					MissionStageBeingSkippedTo = TRUE
					iControlFlag = 0
					missionStage = STAGE_FLY_CARGOBOB_TO_HANGER
				ENDIF
				IF GET_REPLAY_MID_MISSION_STAGE() = 2
					SET_UP_MISSION_PASSED()
				ENDIF
	        ELSE
				IF GET_REPLAY_MID_MISSION_STAGE() = 0
					START_REPLAY_SETUP(<<-1565.0836, 2780.8130, 16.4022>>, 53.6086)
					MissionStageBeingSkippedTo = TRUE
					iControlFlag = 0
					missionStage = STAGE_STEAL_CARGOBOB
				ENDIF
				IF GET_REPLAY_MID_MISSION_STAGE() = 1
					START_REPLAY_SETUP(vHeliCoords, fHeliHeading)
					MissionStageBeingSkippedTo = TRUE
					iControlFlag = 0
					missionStage = STAGE_ESCAPE_MILITARY
				ENDIF
				IF GET_REPLAY_MID_MISSION_STAGE() = 2
					START_REPLAY_SETUP(<<-629.1677, 2871.3101, 385.0313>>, 283.1214)
					MissionStageBeingSkippedTo = TRUE
					iControlFlag = 0
					missionStage = STAGE_FLY_CARGOBOB_TO_HANGER
				ENDIF			
			ENDIF

	    ELSE	
				
			IF IS_REPEAT_PLAY_ACTIVE()	
				START_REPLAY_SETUP(<<-1565.0836, 2780.8130, 16.4022>>, 53.6086)
				MissionStageBeingSkippedTo = TRUE
				iControlFlag = 0
				missionStage = STAGE_STEAL_CARGOBOB
			ELSE
				iControlFlag = 0
				missionStage = STAGE_STEAL_CARGOBOB
			ENDIF
		
		ENDIF

	ENDIF

ENDPROC

//PROC DO_STAGE_GET_TO_CARGOBOB()
//
//	IF iControlFlag > 0
//		HANDLE_DIALOGUE_AND_GOD_TEXT()
//		
//		HANDLE_SWITCHING()
//		
//		HANDLE_MILITARY()
//		
//		HANDLE_PLAYER_CHARACTERS()
//	ENDIF
//	
//	IF iControlFlag = 0
//	
//		//For Stage Selector
//		IF MissionStageBeingSkippedTo = TRUE
//			missionCanFail 	= FALSE
//			STAGE_SELECTOR_MISSION_SETUP()
//			MissionStageBeingSkippedTo = FALSE
//		ENDIF	
//		
//		//Request models required for this stage
//		REQUEST_MODEL(CARGOBOB)
//		REQUEST_MODEL(HUNTER)
//		REQUEST_MODEL(S_M_Y_MARINE_01)
//		
//		//Reset any Flags for this stage here.
//		SetupMilitaryCops 		= FALSE
//		doneChat1				= FALSE
//		doneGodText1 			= FALSE
//		GpsRouteTurnedOn 		= FALSE
//				
//		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
//		IF NOT IS_SCREEN_FADED_IN()
//			LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
//			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//		ENDIF		
//		
//		missionCanFail 	= TRUE
//		iControlFlag = 1
//		
//	ENDIF
//	
//	IF iControlFlag = 1
//		
//		SETUP_MILITARY_AND_COPS()
//		
//		//Add blip and print god text for 1st objective
//		IF NOT DOES_BLIP_EXIST(AirFieldBlip)
//			AirFieldBlip = CREATE_BLIP_FOR_COORD(vMiddleOfAirField)
//		ENDIF
//		
//		//Handle GPS multi route.
//		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
//			IF GpsRouteTurnedOn = FALSE
//				vplayersCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
//				START_GPS_MULTI_ROUTE(HUD_COLOUR_YELLOW)
//					IF vplayersCoords.x > -480
//						ADD_POINT_TO_GPS_MULTI_ROUTE(<<-480.6436, 2848.6216, 32.8302>>)
//					ENDIF
//					IF vplayersCoords.x > -1265
//						ADD_POINT_TO_GPS_MULTI_ROUTE(<<-1265.5708, 2534.6106, 17.6124>>)
//					ENDIF
//					ADD_POINT_TO_GPS_MULTI_ROUTE(<<-1568.1309, 2776.1069, 16.1998>>)
//				SET_GPS_MULTI_ROUTE_RENDER(TRUE)
//				GpsRouteTurnedOn = TRUE
//			ENDIF
//		ELSE
//			IF GpsRouteTurnedOn = TRUE
//				SET_GPS_MULTI_ROUTE_RENDER(FALSE)
//				GpsRouteTurnedOn = FALSE
//			ENDIF
//		ENDIF
//		
//		//Create the Cargobob
//		IF NOT DOES_ENTITY_EXIST(HeliToSteal)
//			IF HAS_MODEL_LOADED(CARGOBOB)
//				CLEAR_AREA(vHeliCoords, 10, TRUE)
//				HeliToSteal = CREATE_VEHICLE(CARGOBOB, vHeliCoords, fHeliHeading)
//				SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
//				FREEZE_ENTITY_POSITION(HeliToSteal, TRUE)
//				SET_MODEL_AS_NO_LONGER_NEEDED(CARGOBOB)
//				SET_PED_MODEL_IS_SUPPRESSED(CARGOBOB, TRUE)
//				HeliToStealDestroyed = FALSE
//			ENDIF
//		ENDIF
//		
//		//Create your buddy heli to steal
//		IF NOT DOES_ENTITY_EXIST(BuddyHeli)
//			IF HAS_MODEL_LOADED(HUNTER)
//				CLEAR_AREA(vBuddyHelicoords, 10, TRUE)
//				BuddyHeli = CREATE_VEHICLE(HUNTER, vBuddyHelicoords, fBuddyHeliHeading)
//				SET_ENTITY_LOAD_COLLISION_FLAG(BuddyHeli, TRUE)
//				FREEZE_ENTITY_POSITION(BuddyHeli, TRUE)
//				SET_MODEL_AS_NO_LONGER_NEEDED(HUNTER)
//				SET_PED_MODEL_IS_SUPPRESSED(HUNTER, TRUE)
//			ENDIF
//		ENDIF			
//		
//		//Move stage on if player gets in these areas
////		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-2374.519287,3155.480957,27.827360>>, <<-1900.770264,2876.654785,106.556519>>, 188.250000)
//		IF IS_COORD_IN_SPECIFIED_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), AC_MILITARY_BASE)
//			iControlFlag = 0
//			missionStage = STAGE_STEAL_CARGOBOB
//		ENDIF
//		
//		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1596.788086,2789.726807,15.350899>>, <<-1583.259521,2801.135010,20.291382>>, 3.750000)
//			iControlFlag = 0
//			missionStage = STAGE_STEAL_CARGOBOB
//		ENDIF
////		ENDIF
//		
//	ENDIF
//	
//ENDPROC

PROC DO_STAGE_STEAL_CARGOBOB()
	
	//Anything that needs to run every frame call here.
	IF iControlFlag > 0
		
//		SETUP_ROCKET_CONTROLLER()
	
		HANDLE_BLIPS_AND_GOD_TEXT()
		
		HANDLE_MILITARY()
		
		HANDLE_ENEMY_CHOPPERS()
		
//		IF DOES_SCENARIO_GROUP_EXIST("WORLD_VEHICLE_DRIVE_SOLO")
//			PRINTSTRING("WORLD_VEHICLE_DRIVE_SOLO group exists") PRINTNL()
//		ELSE
//			PRINTSTRING("WORLD_VEHICLE_DRIVE_SOLO group Does NOT exist") PRINTNL()
//		ENDIF
	ENDIF

	IF iControlFlag = 0
	
		//For Stage Selector
		IF MissionStageBeingSkippedTo = TRUE
			missionCanFail 	= FALSE
			STAGE_SELECTOR_MISSION_SETUP()
			MissionStageBeingSkippedTo = FALSE
		ENDIF	
		
		//Reset any Flags for this stage here.
		DoneStealCargobobText 			= FALSE
		SetupMilitaryCops 				= FALSE
		EnemyHeliPilotTaskGiven[0] 		= FALSE
		EnemyHeliPilotTaskGiven[1] 		= FALSE
//		militaryFriendlyTaskGiven 		= FALSE
		playerIsInBase	 				= FALSE
		AlarmStarted 					= FALSE
		playerIsNearHeli				= FALSE
		doneChat55						= FALSE
		doneChat56						= FALSE
		AimTaskGiven[0]					= FALSE
		AimTaskGiven[1]					= FALSE
		playerWaitedTooLongToLeaveGate 	= FALSE
		hostileTimerSet					= FALSE
		TimerResetOnce 					= FALSE
		weaponTimerStarted 				= FALSE
		doneArmyWrn1aChat 				= FALSE
//		buddyMichaelCombatFlagSet 		= FALSE
//		buddyTrevorCombatFlagSet 		= FALSE
//		buddyFranklinCombatFlagSet 		= FALSE
//		relGroupsSetup 					= FALSE

		iBecomeHostileTimer 			= 0
//		iAlarmChatTimer = GET_GAME_TIMER()
		
		FOR icount = 0 TO 12
			militaryPNHPNTasksGiven[icount]			= FALSE
			militaryPNHPFTasksGiven[icount]			= FALSE
			militaryPCRTasksGiven[icount]			= FALSE
			militaryPFRTasksGiven[icount]			= FALSE
		ENDFOR
		
		//Set up checkpoint here
		//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0, "STAGE_STEAL_CARGOBOB")
		
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		
		PREPARE_ALARM("PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS")
		iAlarmTimer = GET_GAME_TIMER()
		
		//SHOULD BE REMOVED ONCE BUG HAS BEEN FIXED FOR REMOVING LAZER VEHICLES FROM AIRSPACE
		SET_VEHICLE_MODEL_IS_SUPPRESSED(LAZER, TRUE)
		
		REQUEST_MODEL(S_M_Y_MARINE_01)
		REQUEST_MODEL(S_M_M_PILOT_02)
		
		//GRAB ALL ENTITIES FROM TRIGGER SCENE
		IF NOT DOES_ENTITY_EXIST(HeliToSteal)
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
				IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
					SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
					HeliToSteal = g_sTriggerSceneAssets.veh[0]
					SET_VEHICLE_CAN_BE_TARGETTED(HeliToSteal, FALSE)
					SET_ENTITY_LOAD_COLLISION_FLAG(HeliToSteal, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(CARGOBOB)
					SET_VEHICLE_MODEL_IS_SUPPRESSED(CARGOBOB, TRUE)
//					SET_ENTITY_INVINCIBLE(HeliToSteal, TRUE)
					SET_VEHICLE_ENGINE_CAN_DEGRADE(HeliToSteal, FALSE)
					SET_VEHICLE_STRONG(HeliToSteal, TRUE)
					SET_VEHICLE_AS_RESTRICTED(HeliToSteal, 0)
				ENDIF
			ENDIF
		ENDIF		
		
		//Grab the 2 heli's from the trigger scene
		IF NOT DOES_ENTITY_EXIST(EnemyHeli[0])
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
				IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[1])
					SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[1],TRUE, TRUE)
					EnemyHeli[0] = g_sTriggerSceneAssets.veh[1]
					SET_VEHICLE_CAN_BE_TARGETTED(EnemyHeli[0], TRUE)
					SET_ENTITY_LOAD_COLLISION_FLAG(EnemyHeli[0], TRUE)	
					FREEZE_ENTITY_POSITION(EnemyHeli[0], FALSE)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(EnemyHeli[0], "DH_P_2B_ENEMY_CHOPPERS")
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(EnemyHeli[0], FALSE)
				ENDIF
			ENDIF
		ENDIF		
		
		//Set the players start car as a car gen.
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[2])
			PRINTSTRING("g_sTriggerSceneAssets.veh[2] exists") PRINTNL()
			IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[2])
				IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(g_sTriggerSceneAssets.veh[2])) 
					SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[2], TRUE, TRUE)
					SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<1734.7990, 3234.8335, 40.7041>>, 321.0399)
					PRINTSTRING("SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN being called as start car is a heli") PRINTNL()
				ENDIF
				IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(g_sTriggerSceneAssets.veh[2]))
				OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(g_sTriggerSceneAssets.veh[2]))
				OR IS_THIS_MODEL_A_BICYCLE(GET_ENTITY_MODEL(g_sTriggerSceneAssets.veh[2]))
				OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(g_sTriggerSceneAssets.veh[2]))
					SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[2], TRUE, TRUE)
					SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<1748.0499, 3294.7000, 40.1060>>, 204.2419)
					PRINTSTRING("SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN being called for start car") PRINTNL()
				ENDIF
			ENDIF
		ELSE
			PRINTSTRING("g_sTriggerSceneAssets.veh[2] Doesn't exist") PRINTNL()
		ENDIF		
		
		//Grab 1st 2 peds from the trigger scene. The rest will need to be created below.
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
		    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
			MarineCop[0] = g_sTriggerSceneAssets.ped[0]
		ENDIF
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
		    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[1], TRUE, TRUE)
			MarineCop[1] = g_sTriggerSceneAssets.ped[1]
		ENDIF		
		
		IF DOES_ENTITY_EXIST(HeliToSteal)
			FREEZE_ENTITY_POSITION(HeliToSteal, FALSE)
		ENDIF	

		SET_AUDIO_FLAG("PoliceScannerDisabled", TRUE)
		
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			END_REPLAY_SETUP(PlayerCar)
		ENDIF
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID())
		
		MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_DOCKS_PREP_2B)
				
		//Flags
		missionCanFail 	= TRUE
		iControlFlag 	= 1
		
	ENDIF
	
	IF iControlFlag = 1
		
		//Disable the scenario guards
		IF DOES_SCENARIO_GROUP_EXIST("ARMY_GUARD")
			IF IS_SCENARIO_GROUP_ENABLED("ARMY_GUARD")
				SET_SCENARIO_GROUP_ENABLED("ARMY_GUARD", FALSE)
			ENDIF
		ENDIF		
		
		SETUP_MILITARY_AND_COPS()
		
		//Start alarm for any of these reasons
		IF AlarmStarted = FALSE
			//Allow it time to load 
			IF GET_GAME_TIMER() > (iAlarmTimer + 3000)
				
				//Run a check for any one spotting the player
				FOR iAlarmStartCount = 0 TO 12
				
					IF DOES_ENTITY_EXIST(MarineCop[iAlarmStartCount])
						IF NOT IS_PED_INJURED(MarineCop[iAlarmStartCount])		
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), MarineCop[iAlarmStartCount]) < 40
								IF CAN_PED_SEE_HATED_PED(MarineCop[iAlarmStartCount], PLAYER_PED_ID())
									
									//Start the alarm and wanted level if player is seen to be jacking any vehicle
									IF IS_PED_JACKING(PLAYER_PED_ID())
										START_ALARM("PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS",FALSE)
										SET_AMBIENT_ZONE_STATE("AZ_AFB_ALARM_SPEECH", TRUE, TRUE)
										AlarmStarted = TRUE
									ENDIF
									
									//If player is in base start the alarm
									IF playerIsInBase = TRUE
										START_ALARM("PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS",FALSE)
										SET_AMBIENT_ZONE_STATE("AZ_AFB_ALARM_SPEECH", TRUE, TRUE)
										AlarmStarted = TRUE
									ENDIF
					
									//If player approaches with a gun equipped start the alarm
									IF weaponTimerStarted = FALSE
										IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										AND NOT IS_PHONE_ONSCREEN()
											IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
												iWeaponStillEquipedTimer = GET_GAME_TIMER()
												weaponTimerStarted = TRUE
											ENDIF
										ENDIF
									ELSE
										IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
										AND NOT IS_PHONE_ONSCREEN()
											IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED										
												IF GET_GAME_TIMER() > (iWeaponStillEquipedTimer + 3000)
													START_ALARM("PORT_OF_LS_HEIST_FORT_ZANCUDO_ALARMS",FALSE)
													SET_AMBIENT_ZONE_STATE("AZ_AFB_ALARM_SPEECH", TRUE, TRUE)
													AlarmStarted = TRUE
												ENDIF
											ELSE
												//Reset timer once only
												IF TimerResetOnce = FALSE
													weaponTimerStarted = FALSE
													TimerResetOnce = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDFOR
				
			ENDIF
		ENDIF
		
		IF DoneStealCargobobText = FALSE
			CLEAR_PRINTS()
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
			PRINT_NOW("DP_GOD2", DEFAULT_GOD_TEXT_TIME, 1)//Steal the ~b~Cargobob.		
			DoneStealCargobobText = TRUE
		ENDIF		
		
		IF playerIsInBase = TRUE
			IF NOT IS_BATTLEBUDDY_BEHAVIOUR_REQUESTED(BBF_CombatGroupSpacing)
				SET_BATTLEBUDDY_BEHAVIOUR_REQUEST(BBF_CombatGroupSpacing)
			ENDIF
		ELSE
			IF IS_BATTLEBUDDY_BEHAVIOUR_REQUESTED(BBF_CombatGroupSpacing)
				CLEAR_BATTLEBUDDY_BEHAVIOUR_REQUEST(BBF_CombatGroupSpacing)
			ENDIF	
		ENDIF
		
		//Move stage on once the player is in the heli
		IF DOES_ENTITY_EXIST(HeliToSteal)
			IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), HeliToSteal) < 30
					playerIsNearHeli = TRUE
				ENDIF
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)		
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, -1)
					iControlFlag = 0
					REPLAY_RECORD_BACK_FOR_TIME(10.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					missionStage = STAGE_ESCAPE_MILITARY
				ENDIF
			ENDIF
		ENDIF
		
		
//		//Move stage on once all players are in the heli
//		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//			IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//					//Check if Trevor exists and is in heli
//					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//							IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], HeliToSteal)
//								//Check if Franklin exists and is in heli
//								IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//									IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//										IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal)
//											iControlFlag = 0
//											missionStage = STAGE_ESCAPE_MILITARY
//										ENDIF
//									ELSE
//										iControlFlag = 0
//										missionStage = STAGE_ESCAPE_MILITARY
//									ENDIF
//								ELSE
//									iControlFlag = 0
//									missionStage = STAGE_ESCAPE_MILITARY									
//								ENDIF
//							ENDIF
//						ELSE
//							//Check if Franklin exists and is in heli
//							IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//								IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//									IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal)
//										iControlFlag = 0
//										missionStage = STAGE_ESCAPE_MILITARY
//									ENDIF
//								ENDIF
//							ELSE
//								iControlFlag = 0
//								missionStage = STAGE_ESCAPE_MILITARY									
//							ENDIF
//						ENDIF
//					ELSE
//						//Check if Franklin exists and is in heli
//						IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//							IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//								IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal)
//									iControlFlag = 0
//									missionStage = STAGE_ESCAPE_MILITARY
//								ENDIF
//							ELSE
//								iControlFlag = 0
//								missionStage = STAGE_ESCAPE_MILITARY									
//							ENDIF
//						ELSE
//							iControlFlag = 0
//							missionStage = STAGE_ESCAPE_MILITARY									
//						ENDIF						
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//	
//		//Move stage on once all players are in the heli
//		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//			IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//					//Check if Michael exists and is in heli
//					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//							IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], HeliToSteal)
//								//Check if Franklin exists and is in heli
//								IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//									IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//										IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal)
//											iControlFlag = 0
//											missionStage = STAGE_ESCAPE_MILITARY
//										ENDIF
//									ENDIF
//								ELSE
//									iControlFlag = 0
//									missionStage = STAGE_ESCAPE_MILITARY									
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						//Check if Franklin exists and is in heli
//						IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//							IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//								IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], HeliToSteal)
//									iControlFlag = 0
//									missionStage = STAGE_ESCAPE_MILITARY
//								ENDIF
//							ENDIF
//						ELSE
//							iControlFlag = 0
//							missionStage = STAGE_ESCAPE_MILITARY									
//						ENDIF						
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		//Move stage on once all players are in the heli
//		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//			IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//					//Check if Michael exists and is in heli
//					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//							IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], HeliToSteal)
//								//Check if Trevor exists and is in heli
//								IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//									IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//										IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], HeliToSteal)
//											iControlFlag = 0
//											missionStage = STAGE_ESCAPE_MILITARY
//										ENDIF
//									ENDIF
//								ELSE
//									iControlFlag = 0
//									missionStage = STAGE_ESCAPE_MILITARY									
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						//Check if Trevor exists and is in heli
//						IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//							IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//								IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], HeliToSteal)
//									iControlFlag = 0
//									missionStage = STAGE_ESCAPE_MILITARY
//								ENDIF
//							ENDIF
//						ELSE
//							iControlFlag = 0
//							missionStage = STAGE_ESCAPE_MILITARY									
//						ENDIF						
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF		
	ENDIF

ENDPROC

//PURPOSE: Creates and deletes end get away vehicles when required
PROC SET_UP_END_VEHICLES()
	
	//Set up and create the end mission vehicles when player is getting close to the hanger
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vHanger) < 500
		//Set up and create the end mission vehicles now
		IF EndCarsCreated = FALSE
			
			IF NOT DOES_ENTITY_EXIST(GetAwayCar[0])
				REQUEST_MODEL(SANDKING)
				IF HAS_MODEL_LOADED(SANDKING)
						
					GetAwayCar[0] = CREATE_VEHICLE(SANDKING, <<1754.9508, 3290.9939, 40.1176>>, 182.3970)	
					FREEZE_ENTITY_POSITION(GetAwayCar[0], TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(SANDKING)
					
				ENDIF
			ENDIF	
			
			IF DOES_ENTITY_EXIST(GetAwayCar[0])
				EndCarsCreated = TRUE
			ENDIF
			
		ENDIF
	ELSE
		//Delete the vehicles for memory reasons if the player flies away from this area
		IF EndCarsCreated = TRUE
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vHanger) > 550
				IF DOES_ENTITY_EXIST(GetAwayCar[0])
					DELETE_VEHICLE(GetAwayCar[0])
				ENDIF
				EndCarsCreated = FALSE
			ENDIF
		ENDIF
	ENDIF
			
	
ENDPROC

PROC DO_STAGE_ESCAPE_MILITARY()

	//Anything that needs to run every frame call here.
	IF iControlFlag > 0
		
		HANDLE_BLIPS_AND_GOD_TEXT()
		
//		HANDLE_SWITCHING()
		
		HANDLE_MILITARY()
		
		//Set up and create the end mission vehicles when player is getting close to the hanger
		SET_UP_END_VEHICLES()
		
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) <> 4
			SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 4)
			SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
		ENDIF
		
//		HANDLE_PLAYER_CHARACTERS()
		
		HANDLE_ENEMY_CHOPPERS()
		
		HANDLE_ARMY_JETS()
		
		//Give AI blips if in the base at escape section
		FOR icount = 0 TO 12
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
				UPDATE_AI_PED_BLIP(MarineCop[icount], MarineCopBlip[icount])
			ELSE
				CLEANUP_AI_PED_BLIP(MarineCopBlip[icount])
			ENDIF
		ENDFOR	
	ENDIF
	
	IF iControlFlag = 0
	
		//For Stage Selector
		IF MissionStageBeingSkippedTo = TRUE
			missionCanFail 	= FALSE
			STAGE_SELECTOR_MISSION_SETUP()
			MissionStageBeingSkippedTo = FALSE
		ENDIF	
		
		//Flags
		DoneGetToHangerText 				= FALSE
//		LongDistanceCamRequired 			= FALSE
//		MichaelAITaskGiven 					= FALSE
//		TrevorAITaskGiven 					= FALSE
		EnemyHeliPilotTaskGiven[0] 			= FALSE
		EnemyHeliPilotTaskGiven[1] 			= FALSE
		Heli0RocketsEquipped 				= FALSE
//		Heli1RocketsEquipped 				= FALSE
		EnemyHeliDown[0]					= FALSE
		EnemyHeliDown[1]					= FALSE
		godTextUpdtaed 						= FALSE
		EndCarsCreated 						= FALSE
		TaskNeedsUpdated[0] 				= FALSE
//		TaskNeedsUpdated[1] 				= FALSE
		doneChat53 							= FALSE
		doneChat54 							= FALSE
		TaskToSlowDownGiven					= FALSE
		Accuracy0							= FALSE
		Accuracy100							= FALSE
		FiringTypeBullet					= FALSE
		FiringTypeRocket					= FALSE
		doneGetBackInText 					= FALSE
		
//		i_player_rocket_event 				= 0
//		i_rocket_timer 						= GET_GAME_TIMER()
		iGodTextTimer 						= GET_GAME_TIMER()
		iEnemyPilotAccuracyTimer 			= GET_GAME_TIMER()
	
		IF DOES_ENTITY_EXIST(HeliToSteal)
			FREEZE_ENTITY_POSITION(HeliToSteal, FALSE)
		ENDIF
	
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "STAGE_ESCAPE_MILITARY")
	
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			END_REPLAY_SETUP(HeliToSteal)
		ENDIF
		
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("DH_P_2B_ENTER_CHOPPER")
			START_AUDIO_SCENE("DH_P_2B_ENTER_CHOPPER")
		ENDIF
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("DH_P_2B_ENEMY_CHOPPERS_ARRIVE")
			START_AUDIO_SCENE("DH_P_2B_ENEMY_CHOPPERS_ARRIVE")
		ENDIF
		
		INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(DH2BP_HELI_TAKEDOWN_TIME)
		
		SET_POLICE_RADAR_BLIPS(FALSE)
		
		IF DOES_ENTITY_EXIST(HeliToSteal)
			IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(HeliToSteal)
//				ADD_ENTITY_TO_AUDIO_MIX_GROUP(HeliToSteal, "DH_P_2B_CARGOBOB")
			ENDIF
		ENDIF
		
		//Add in a video recording of the last 10 seconds of action involving the player getting into the heli.
		REPLAY_RECORD_BACK_FOR_TIME(10.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
		
		missionCanFail 	= TRUE
		iControlFlag = 1
		
	ENDIF
	
	IF iControlFlag = 1
		
//		//Reset Trev's AI task so he will now attack the other heli.
//		IF TrevorTaskReset = FALSE
//			IF EnemyHeliPilotKilled[0] = TRUE
//				TrevorAITaskGiven = FALSE
//				TrevorTaskReset = TRUE
//			ENDIF
//		ENDIF
		
		//Move stage on if both pilots have been killed
		IF EnemyHeliDown[0] = TRUE
			IF DOES_BLIP_EXIST(EnemyHeliPilotBlip[0])
				REMOVE_BLIP(EnemyHeliPilotBlip[0])
			ENDIF
			IF DOES_ENTITY_EXIST(EnemyHeli[0])
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(EnemyHeli[0])
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("DH_P_2B_ENEMY_CHOPPERS_ARRIVE")
				STOP_AUDIO_SCENE("DH_P_2B_ENEMY_CHOPPERS_ARRIVE")
			ENDIF
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE, DH2BP_HELI_TAKEDOWN_TIME)
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(DH2BP_HELI_TIME)
			IF DOES_ENTITY_EXIST(HeliToSteal)
				IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
					IF IS_ENTITY_IN_ANGLED_AREA(HeliToSteal, <<1721.001465,3264.248291,38.400642>>, <<1765.508911,3276.518066,46.053852>>, 48.250000)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),0)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
							iControlFlag = 0
							missionStage = STAGE_END_CUTSCENE
						ELSE
							SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),0)
							SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
//							REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(HeliToSteal)
							Mission_Passed()
						ENDIF
					ELSE
						iControlFlag = 0
						missionStage = STAGE_FLY_CARGOBOB_TO_HANGER
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF godTextUpdtaed = FALSE
				IF DOES_ENTITY_EXIST(HeliToSteal)
					IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
							IF GET_GAME_TIMER() > (iGodTextTimer + 10000)
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
									PRINT_NOW("DP_GOD5", DEFAULT_GOD_TEXT_TIME, 1) 
									//~s~Lose the ~r~military chopper.
									godTextUpdtaed = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	

ENDPROC

PROC DO_STAGE_FLY_CARGOBOB_TO_HANGER()

	//Anything that needs to run every frame call here.
	IF iControlFlag > 0
		
		HANDLE_BLIPS_AND_GOD_TEXT()

//		HANDLE_SWITCHING()
		
		//Set up and create the end mission vehicles when player is getting close to the hanger
		SET_UP_END_VEHICLES()

//		HANDLE_PLAYER_CHARACTERS()
	ENDIF
	
	IF iControlFlag = 0
	
		//For Stage Selector
		IF MissionStageBeingSkippedTo = TRUE
			//Reset Flags
			missionCanFail 			= FALSE
//			MichaelAITaskGiven 		= FALSE
			
			STAGE_SELECTOR_MISSION_SETUP()
			MissionStageBeingSkippedTo = FALSE
		ENDIF	
	
		//Flags
		doneChat59 			= FALSE
		doneHelp4 			= FALSE
		checkpointSet 		= FALSE
		donePhoneCall		= FALSE
		
		
		IF DOES_BLIP_EXIST(ArmyJet1Blip)
			REMOVE_BLIP(ArmyJet1Blip)
		ENDIF		
	
		//bug fix 1945573
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	
		IF DOES_ENTITY_EXIST(HeliToSteal)
			FREEZE_ENTITY_POSITION(HeliToSteal, FALSE)
		ENDIF	
	
		SET_POLICE_RADAR_BLIPS(FALSE)
		
		//Clean up previous sections
		FOR icount = 0 TO 12 
			IF DOES_ENTITY_EXIST(MarineCop[icount])
				IF NOT IS_PED_INJURED(MarineCop[icount])
					SET_PED_KEEP_TASK(MarineCop[icount], TRUE)
					SET_PED_AS_NO_LONGER_NEEDED(MarineCop[icount])
				ENDIF
			ENDIF
		ENDFOR
		
		IF DOES_ENTITY_EXIST(EnemyHeli[0])
			SET_VEHICLE_AS_NO_LONGER_NEEDED(EnemyHeli[0])
		ENDIF
		
		IF DOES_ENTITY_EXIST(EnemyHeliPilot[0])
			SET_PED_AS_NO_LONGER_NEEDED(EnemyHeliPilot[0])
		ENDIF
	
		IF NOT IS_AUDIO_SCENE_ACTIVE("DH_P_2B_ENTER_CHOPPER")
			START_AUDIO_SCENE("DH_P_2B_ENTER_CHOPPER")
		ENDIF	
	
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			END_REPLAY_SETUP(HeliToSteal)
		ENDIF
		
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		REPLAY_RECORD_BACK_FOR_TIME(10.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
		
		missionCanFail 	= TRUE
		iControlFlag = 1
		
	ENDIF
	
	IF iControlFlag = 1			
		
		//Set up checkpoint once player gets clear of the military base
		IF checkpointSet = FALSE
			IF DOES_ENTITY_EXIST(HeliToSteal)
				IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vHeliCoords) > 800
							SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "STAGE_FLY_CARGOBOB_TO_HANGER", TRUE)		
							checkpointSet = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF		
		
		//Have Trev mention about Michael needing to learn to fly
		IF DOES_ENTITY_EXIST(HeliToSteal)
			IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
					IF IS_ENTITY_IN_AIR(HeliToSteal)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vHeliCoords) > 800
							IF doneChat59 = FALSE
							AND DoneGetToHangerText = TRUE
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
										ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, PLAYER_PED_ID(), "TREVOR")
										IF CREATE_CONVERSATION(MyLocalPedStruct, "DP2BAUD", "DP2B_CHAT59", CONV_PRIORITY_MEDIUM)
											//Oh! She's an unwieldy thing. If Michael don't get those lessons...
											//This is a big bird - Michael really better get those lessons.
											doneChat59 = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							//Phone call to Wade. 
							IF doneChat59 = TRUE
							AND donePhoneCall = FALSE
								IF GET_MISSION_COMPLETE_STATE(SP_HEIST_DOCKS_PREP_1) = FALSE
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
											ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "Wade")
											IF PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_WADE, "DP2BAUD", "DP2B_WADEP3", CONV_PRIORITY_MEDIUM)
												//Oh, hi Trevor.
												//The Airforce are missing a chopper.
												//They lost one. That's a shame.
												//We took it, you dick. Add it to the list, and I'll tell you when we've got the rest of it. 
												donePhoneCall = TRUE
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
											ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, NULL, "Wade")
											IF PLAYER_CALL_CHAR_CELLPHONE(MyLocalPedStruct, CHAR_WADE, "DP2BAUD", "DP2B_WADEP4", CONV_PRIORITY_MEDIUM)
												//How you doing, Trev?
												//Fine... for a guy who just went into an airbase and stole a chopper. Are we ready to go on this thing?
												//If it's the heavy lift chopper, then yeah. I'll get everything ready and I'll call you. 
												//Alright.
												donePhoneCall = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							//Only display this help text if Michael hasn't been to the flying school yet.
							IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_DONE_FLYING_LESSONS) = FALSE
								IF doneHelp4 = FALSE
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
											PRINT_HELP("DP_HELP4")//Michael will be flying the Cargobob during the heist. Improve Michael's flying stat by going to the pilot school.
											doneHelp4 = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		
		IF DOES_ENTITY_EXIST(HeliToSteal)
			IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vHanger, <<2, 2, LOCATE_SIZE_HEIGHT>>, TRUE)
					ENDIF
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1721.001465,3264.248291,38.400642>>, <<1765.508911,3276.518066,46.053852>>, 48.250000)
						IF NOT IS_ENTITY_IN_AIR(HeliToSteal)
						AND IS_VEHICLE_STOPPED(HeliToSteal)
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE, DH2BP_HELI_TIME)

							REPLAY_RECORD_BACK_FOR_TIME(8.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
		
							iControlFlag = 0
							missionStage = STAGE_END_CUTSCENE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC

//PURPOSE: handles the player skipping the cutscene
PROC HANDLE_SKIP_CUTSCENE()
	
	IF iControlFlag > 1
	AND iControlFlag < 5
		IF TIMERA() > 1500
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
				cutsceneSkipped = TRUE
			ENDIF
		ENDIF
		WHILE cutsceneSkipped = TRUE
			SWITCH iCutsceneSkipStage
			
				CASE 0
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
					PRINTSTRING("iCutsceneSkipStage = 0") PRINTNL()
					iCutsceneSkipStage++
				BREAK
				CASE 1
					PRINTSTRING("iCutsceneSkipStage = 1") PRINTNL()
					IF IS_SCREEN_FADED_OUT()
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(HeliToSteal, <<-4.29, 12.168, -1.277>>))
						SET_ENTITY_HEADING(PLAYER_PED_ID(), GET_ENTITY_HEADING(PLAYER_PED_ID()))
						
						IF DOES_ENTITY_EXIST(HeliToSteal)
							IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
								SET_VEHICLE_DOOR_SHUT(HeliToSteal, SC_DOOR_FRONT_LEFT)
								SET_VEHICLE_DOOR_SHUT(HeliToSteal, SC_DOOR_FRONT_RIGHT)
								SET_VEHICLE_DOORS_LOCKED(HeliToSteal, VEHICLELOCK_LOCKED)
								FREEZE_ENTITY_POSITION(HeliToSteal, TRUE)
							ENDIF
						ENDIF
						
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
						SET_CAM_ACTIVE(EndSceneCam, FALSE)
						DESTROY_CAM(EndSceneCam)
						DISPLAY_RADAR(TRUE)
						DISPLAY_HUD(TRUE)			
						
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
						SETTIMERA(0)
						iCutsceneSkipStage++
					ENDIF
				BREAK
				CASE 2
					PRINTSTRING("iCutsceneSkipStage = 2") PRINTNL()
					IF TIMERA() > 2000
						IF NOT IS_SCREEN_FADED_IN()
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
							PRINTSTRING("DO_SCREEN_FADE_IN being called") PRINTNL()
						ENDIF
						iCutsceneSkipStage++
					ENDIF
				BREAK
				CASE 3
					PRINTSTRING("iCutsceneSkipStage = 3") PRINTNL()
					IF IS_SCREEN_FADED_IN()
						SETTIMERB(0)
						iControlFlag = 5
						cutsceneSkipped = FALSE
					ENDIF
				BREAK
			ENDSWITCH
			WAIT(0)
		ENDWHILE
	ENDIF

ENDPROC

// PURPOSE: End cutscene for the mission
PROC DO_STAGE_END_CUTSCENE()
	
//	PRINTSTRING("camera offset is ") PRINTVECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(HeliToSteal, GET_CAM_COORD(GET_DEBUG_CAM())))
	
	HANDLE_BLIPS_AND_GOD_TEXT()
	
//	HANDLE_SKIP_CUTSCENE()
	
	IF Icontrolflag = 0
		
		//For Stage Selector
		IF MissionStageBeingSkippedTo = TRUE
			//Reset Flags
			missionCanFail 			= FALSE
			
			STAGE_SELECTOR_MISSION_SETUP()
			MissionStageBeingSkippedTo = FALSE
		ENDIF
		
		//Flags
//		cutsceneSkipped 	= FALSE
//		iCutsceneSkipStage 	= 0
//		NoBuddysAvailable 	= FALSE
		
//		REQUEST_VEHICLE_RECORDING(1, "BB_DP2B")
//		REQUEST_VEHICLE_RECORDING(2, "BB_DP2B")
		
		IF DOES_ENTITY_EXIST(HeliToSteal)
			FREEZE_ENTITY_POSITION(HeliToSteal, FALSE)
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
		ENDIF			
		
		//Delete the invisibel ped now
//		IF DOES_ENTITY_EXIST(InvisiblePed)
//			DELETE_PED(InvisiblePed)
//		ENDIF
	
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
		ENDIF
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
		
//		IF DOES_CAM_EXIST(EndSceneCam)
//			DESTROY_CAM(EndSceneCam)
//		ENDIF
		
//		//Take control of the battle buddies 
//		IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
//			PRINTSTRING("hBuddyMichael is avialable") PRINTNL()
//			IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddyMichael)
//				PRINTSTRING("hBuddyMichael is not overriden") PRINTNL()
//				WHILE NOT OVERRIDE_BATTLEBUDDY(hBuddyMichael, FALSE)
//					PRINTSTRING("Waiting on overriding hBuddyMichael buddy") PRINTNL()
//					WAIT(0)
//				ENDWHILE
//			ENDIF
//		ENDIF
//		IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
//			PRINTSTRING("hBuddyTrevor is avialable") PRINTNL()
//			IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddyTrevor)
//				PRINTSTRING("hBuddyTrevor is not overriden") PRINTNL()
//				WHILE NOT OVERRIDE_BATTLEBUDDY(hBuddyTrevor, FALSE)
//					PRINTSTRING("Waiting on overriding hBuddyTrevor buddy") PRINTNL()
//					WAIT(0)
//				ENDWHILE
//			ENDIF
//		ENDIF
//		IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
//			PRINTSTRING("hBuddyFranklin is avialable") PRINTNL()
//			IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddyFranklin)
//				PRINTSTRING("hBuddyFranklin is not overriden") PRINTNL()
//				WHILE NOT OVERRIDE_BATTLEBUDDY(hBuddyFranklin, FALSE)
//					PRINTSTRING("Waiting on overriding hBuddyFranklin buddy") PRINTNL()
//					WAIT(0)
//				ENDWHILE
//			ENDIF
//		ENDIF			
		
//		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
	
		//Make player exit vehicle
		IF DOES_ENTITY_EXIST(HeliToSteal)
			IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
					TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), HeliToSteal)		
				ENDIF
			ENDIF
		ENDIF
		
		missionCanFail 	= TRUE
		iControlFlag = 1
		
	ENDIF
	
	IF iControlFlag = 1
	
		IF DOES_ENTITY_EXIST(HeliToSteal)
			IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal, TRUE)
					SET_VEHICLE_DOOR_SHUT(HeliToSteal, SC_DOOR_FRONT_LEFT, FALSE)
					SET_VEHICLE_DOOR_SHUT(HeliToSteal, SC_DOOR_FRONT_RIGHT, FALSE)
					SET_VEHICLE_DOORS_LOCKED(HeliToSteal, VEHICLELOCK_LOCKED)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(HeliToSteal, FALSE)
					FREEZE_ENTITY_POSITION(HeliToSteal, TRUE)
					Mission_Passed()
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF 		
	
//	IF Icontrolflag = 1
//		
////		IF IS_VEHICLE_DRIVEABLE(heliToSteal)
////			IF IS_BUTTON_JUST_PRESSED(pad1, select)
////				PRINTSTRING("camera offset is ") PRINTVECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(HeliToSteal, GET_CAM_COORD(GET_DEBUG_CAM())))
////			ENDIF
////		ENDIF
////		
////		PRINTSTRING("camera offset is ") PRINTVECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(HeliToSteal, GET_CAM_COORD(GET_DEBUG_CAM())))
//		
//		IF NOT DOES_CAM_EXIST(EndSceneCam)
//			EndSceneCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
//			SET_CAM_COORD(EndSceneCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(HeliToSteal, << -4.3327, 9.96572, -0.5 >>))
//			POINT_CAM_AT_ENTITY(EndSceneCam, HeliToSteal, << 0.290697, 3.39268, 0.988922 >>)
//		ENDIF
//		
//		DISPLAY_RADAR(FALSE)
//		DISPLAY_HUD(FALSE)
//		RENDER_SCRIPT_CAMS(TRUE, FALSE)
//		
//		SETTIMERA(0)
//		iControlFlag = 2
//		
//		
//	ENDIF
//	
//	IF icontrolFlag = 2
//				
//		//Give the peds in the heli tasks to leave the heli
//		IF DOES_ENTITY_EXIST(HeliToSteal)
//			IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
////				//Make buddies exit vehicle
////				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
////					IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
////						IF IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
////							CLEAR_PED_TASKS(hBuddyTrevor)
////							REMOVE_PED_FROM_GROUP(hBuddyTrevor)
////							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyTrevor, TRUE)
////							TASK_LEAVE_VEHICLE(hBuddyTrevor, HeliToSteal)
////							PRINTSTRING("TASK_LEAVE_VEHICLE(hBuddyTrevor, HeliToSteal)") PRINTNL()
////							NoBuddysAvailable = FALSE
////						ENDIF
////					ENDIF
////					IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
////						IF IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
////							CLEAR_PED_TASKS(hBuddyFranklin)
////							REMOVE_PED_FROM_GROUP(hBuddyFranklin)
////							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyFranklin, TRUE)
////							TASK_LEAVE_VEHICLE(hBuddyFranklin, HeliToSteal)
////							PRINTSTRING("TASK_LEAVE_VEHICLE(hBuddyFranklin, HeliToSteal)") PRINTNL()
////							NoBuddysAvailable = FALSE
////						ENDIF
////					ENDIF
////					IF NOT IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
////					AND NOT IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
////						NoBuddysAvailable = TRUE
////					ENDIF
////				ENDIF
////				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
////					IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
////						IF IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
////							CLEAR_PED_TASKS(hBuddyMichael)
////							REMOVE_PED_FROM_GROUP(hBuddyMichael)
////							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyMichael, TRUE)
////							TASK_LEAVE_VEHICLE(hBuddyMichael, HeliToSteal)
////							PRINTSTRING("TASK_LEAVE_VEHICLE(hBuddyMichael, HeliToSteal)") PRINTNL()
////							NoBuddysAvailable = FALSE
////						ENDIF
////					ENDIF
////					IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
////						IF IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
////							CLEAR_PED_TASKS(hBuddyFranklin)
////							REMOVE_PED_FROM_GROUP(hBuddyFranklin)
////							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyFranklin, TRUE)
////							TASK_LEAVE_VEHICLE(hBuddyFranklin, HeliToSteal)
////							PRINTSTRING("TASK_LEAVE_VEHICLE(hBuddyFranklin, HeliToSteal)") PRINTNL()
////							NoBuddysAvailable = FALSE
////						ENDIF
////					ENDIF
////					IF NOT IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
////					AND NOT IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
////						NoBuddysAvailable = TRUE
////					ENDIF
////				ENDIF	
////				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
////					IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
////						IF IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
////							CLEAR_PED_TASKS(hBuddyMichael)
////							REMOVE_PED_FROM_GROUP(hBuddyMichael)
////							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyMichael, TRUE)
////							TASK_LEAVE_VEHICLE(hBuddyMichael, HeliToSteal)
////							PRINTSTRING("TASK_LEAVE_VEHICLE(hBuddyMichael, HeliToSteal)") PRINTNL()
////							NoBuddysAvailable = FALSE
////						ENDIF
////					ENDIF
////					IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
////						IF IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
////							CLEAR_PED_TASKS(hBuddyTrevor)
////							REMOVE_PED_FROM_GROUP(hBuddyTrevor)
////							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddyTrevor, TRUE)
////							TASK_LEAVE_VEHICLE(hBuddyTrevor, HeliToSteal)
////							PRINTSTRING("TASK_LEAVE_VEHICLE(hBuddyTrevor, HeliToSteal)") PRINTNL()
////							NoBuddysAvailable = FALSE
////						ENDIF
////					ENDIF
////					IF NOT IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
////					AND NOT IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
////						NoBuddysAvailable = TRUE
////					ENDIF
////				ENDIF	
//				//Make player exit vehicle
//				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//					TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//					PRINTSTRING("TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), HeliToSteal)") PRINTNL()
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		SETTIMERB(0)
//		icontrolFlag = 3
//	
//	ENDIF
//	
//	IF icontrolFlag = 3
//	
////		PRINTSTRING("offset from vehicle is ") PRINTVECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(HeliToSteal, GET_CAM_COORD(GET_DEBUG_CAM()))) PRINTNL()
//	
//		//Let them say some shit to each other then have the buddies walk off screen and get into parked cars ready to drive away
//		IF TIMERB() > 3000
////		OR NoBuddysAvailable = TRUE
//			IF DOES_ENTITY_EXIST(HeliToSteal)
//				IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//					//Make player exit vehicle
//					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), HeliToSteal)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(HeliToSteal, <<-4.29, 12.168, -1.277>>), PEDMOVE_WALK)
//					ENDIF
////					//Make buddies exit vehicle
////					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
////						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
////							IF NOT IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
////								TASK_FOLLOW_NAV_MESH_TO_COORD(hBuddyTrevor, <<1744.4069, 3295.2913, 40.1062>>, PEDMOVEBLENDRATIO_WALK)
////							ENDIF
////						ENDIF
////						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
////							IF NOT IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
////								TASK_FOLLOW_NAV_MESH_TO_COORD(hBuddyFranklin, <<1744.4069, 3295.2913, 40.1062>>, PEDMOVEBLENDRATIO_WALK)
////							ENDIF
////						ENDIF
////					ENDIF
////					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
////						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
////							IF NOT IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
////								TASK_FOLLOW_NAV_MESH_TO_COORD(hBuddyMichael, <<1744.4069, 3295.2913, 40.1062>>, PEDMOVEBLENDRATIO_WALK)
////							ENDIF
////						ENDIF
////						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
////							IF NOT IS_PED_IN_VEHICLE(hBuddyFranklin, HeliToSteal)
////								TASK_FOLLOW_NAV_MESH_TO_COORD(hBuddyFranklin, <<1744.4069, 3295.2913, 40.1062>>, PEDMOVEBLENDRATIO_WALK)
////							ENDIF
////						ENDIF
////					ENDIF	
////					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
////						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
////							IF NOT IS_PED_IN_VEHICLE(hBuddyMichael, HeliToSteal)
////								TASK_FOLLOW_NAV_MESH_TO_COORD(hBuddyMichael, <<1744.4069, 3295.2913, 40.1062>>, PEDMOVEBLENDRATIO_WALK)
////							ENDIF
////						ENDIF
////						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
////							IF NOT IS_PED_IN_VEHICLE(hBuddyTrevor, HeliToSteal)
////								TASK_FOLLOW_NAV_MESH_TO_COORD(hBuddyTrevor, <<1744.4069, 3295.2913, 40.1062>>, PEDMOVEBLENDRATIO_WALK)
////							ENDIF
////						ENDIF
////					ENDIF	
//				ENDIF
//			ENDIF			
// 		
//			SETTIMERB(0)
//			icontrolFlag = 4
//		ENDIF
//		
//	ENDIF
//	
//	IF icontrolFlag = 4
//	
//		IF TIMERB() > 3500
//			
////			//put the peds into the vehicles and start their playbacks
////			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
////				IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
////					IF DOES_ENTITY_EXIST(GetAwayCar[0])
////						IF IS_VEHICLE_DRIVEABLE(GetAwayCar[0])
////							IF NOT IS_PED_IN_VEHICLE(hBuddyTrevor, GetAwayCar[0])
////								SET_PED_INTO_VEHICLE(hBuddyTrevor, GetAwayCar[0])
////								FREEZE_ENTITY_POSITION(GetAwayCar[0], FALSE)
////								START_PLAYBACK_RECORDED_VEHICLE(GetAwayCar[0], 1, "BB_DP2B")
////								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(GetAwayCar[0], 3000)
////							ENDIF
////						ENDIF
////					ENDIF
////				ENDIF
////				IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
////					IF DOES_ENTITY_EXIST(GetAwayCar[1])
////						IF IS_VEHICLE_DRIVEABLE(GetAwayCar[1])
////							IF NOT IS_PED_IN_VEHICLE(hBuddyFranklin, GetAwayCar[1])
////								SET_PED_INTO_VEHICLE(hBuddyFranklin, GetAwayCar[1])
////								FREEZE_ENTITY_POSITION(GetAwayCar[1], FALSE)
////								START_PLAYBACK_RECORDED_VEHICLE(GetAwayCar[1], 2, "BB_DP2B")
////							ENDIF
////						ENDIF
////					ENDIF
////				ENDIF
////			ENDIF
////			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
////				IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
////					IF DOES_ENTITY_EXIST(GetAwayCar[0])
////						IF IS_VEHICLE_DRIVEABLE(GetAwayCar[0])
////							IF NOT IS_PED_IN_VEHICLE(hBuddyMichael, GetAwayCar[0])
////								SET_PED_INTO_VEHICLE(hBuddyMichael, GetAwayCar[0])
////								FREEZE_ENTITY_POSITION(GetAwayCar[0], FALSE)
////								START_PLAYBACK_RECORDED_VEHICLE(GetAwayCar[0], 1, "BB_DP2B")
////								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(GetAwayCar[0], 3000)
////							ENDIF
////						ENDIF
////					ENDIF
////				ENDIF
////				IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
////					IF DOES_ENTITY_EXIST(GetAwayCar[1])
////						IF IS_VEHICLE_DRIVEABLE(GetAwayCar[1])
////							IF NOT IS_PED_IN_VEHICLE(hBuddyFranklin, GetAwayCar[1])
////								SET_PED_INTO_VEHICLE(hBuddyFranklin, GetAwayCar[1])
////								FREEZE_ENTITY_POSITION(GetAwayCar[1], FALSE)
////								START_PLAYBACK_RECORDED_VEHICLE(GetAwayCar[1], 2, "BB_DP2B")
////							ENDIF
////						ENDIF
////					ENDIF
////				ENDIF
////			ENDIF	
////			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
////				IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
////					IF DOES_ENTITY_EXIST(GetAwayCar[0])
////						IF IS_VEHICLE_DRIVEABLE(GetAwayCar[0])
////							IF NOT IS_PED_IN_VEHICLE(hBuddyMichael, GetAwayCar[0])
////								SET_PED_INTO_VEHICLE(hBuddyMichael, GetAwayCar[0])
////								FREEZE_ENTITY_POSITION(GetAwayCar[0], FALSE)
////								START_PLAYBACK_RECORDED_VEHICLE(GetAwayCar[0], 1, "BB_DP2B")
////								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(GetAwayCar[0], 3000)
////							ENDIF
////						ENDIF
////					ENDIF
////				ENDIF
////				IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
////					IF DOES_ENTITY_EXIST(GetAwayCar[1])
////						IF IS_VEHICLE_DRIVEABLE(GetAwayCar[1])
////							IF NOT IS_PED_IN_VEHICLE(hBuddyTrevor, GetAwayCar[1])
////								SET_PED_INTO_VEHICLE(hBuddyTrevor, GetAwayCar[1])
////								FREEZE_ENTITY_POSITION(GetAwayCar[1], FALSE)
////								START_PLAYBACK_RECORDED_VEHICLE(GetAwayCar[1], 2, "BB_DP2B")
////							ENDIF
////						ENDIF
////					ENDIF
////				ENDIF
////			ENDIF			
//			
//			IF DOES_ENTITY_EXIST(HeliToSteal)
//				IF IS_VEHICLE_DRIVEABLE(HeliToSteal)
//					SET_VEHICLE_DOOR_SHUT(HeliToSteal, SC_DOOR_FRONT_LEFT)
//					SET_VEHICLE_DOOR_SHUT(HeliToSteal, SC_DOOR_FRONT_RIGHT)
//					SET_VEHICLE_DOORS_LOCKED(HeliToSteal, VEHICLELOCK_LOCKED)
//					FREEZE_ENTITY_POSITION(HeliToSteal, TRUE)
//				ENDIF
//			ENDIF
//			
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			SET_CAM_ACTIVE(EndSceneCam, FALSE)
//			DESTROY_CAM(EndSceneCam)
//			DISPLAY_RADAR(TRUE)
//			DISPLAY_HUD(TRUE)			
//			
//			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//			SETTIMERB(0)
//			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//			icontrolFlag = 5
//		ENDIF
//		
//	ENDIF
//	
//	IF icontrolFlag = 5
//	
//		IF TIMERB() > 1000
////			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
////				IF DOES_ENTITY_EXIST(GetAwayCar[0])
////					IF IS_VEHICLE_DRIVEABLE(GetAwayCar[0])			
////						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(GetAwayCar[0])
////							STOP_PLAYBACK_RECORDED_VEHICLE(GetAwayCar[0])
////						ENDIF
////						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
////							CLEAR_PED_TASKS(hBuddyTrevor)
////							TASK_VEHICLE_MISSION(hBuddyTrevor, GetAwayCar[0], HeliToSteal, MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS, -1, -1)
////							SET_PED_KEEP_TASK(hBuddyTrevor, TRUE)
////							PRINTSTRING("TASK_VEHICLE_MISSION(hBuddyTrevor") PRINTNL()
////						ENDIF
////					ENDIF
////				ENDIF
////				IF DOES_ENTITY_EXIST(GetAwayCar[1])
////					IF IS_VEHICLE_DRIVEABLE(GetAwayCar[1])			
////						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(GetAwayCar[1])
////							STOP_PLAYBACK_RECORDED_VEHICLE(GetAwayCar[1])
////						ENDIF
////						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
////							CLEAR_PED_TASKS(hBuddyFranklin)
////							TASK_VEHICLE_MISSION(hBuddyFranklin, GetAwayCar[1], HeliToSteal, MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS, -1, -1)
////							SET_PED_KEEP_TASK(hBuddyFranklin, TRUE)
////							PRINTSTRING("TASK_VEHICLE_MISSION(hBuddyFranklin") PRINTNL()
////						ENDIF
////					ENDIF
////				ENDIF
////			ENDIF
////			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
////				IF DOES_ENTITY_EXIST(GetAwayCar[0])
////					IF IS_VEHICLE_DRIVEABLE(GetAwayCar[0])			
////						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(GetAwayCar[0])
////							STOP_PLAYBACK_RECORDED_VEHICLE(GetAwayCar[0])
////						ENDIF
////						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
////							CLEAR_PED_TASKS(hBuddyMichael)
////							TASK_VEHICLE_MISSION(hBuddyMichael, GetAwayCar[0], HeliToSteal, MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS, -1, -1)
////							SET_PED_KEEP_TASK(hBuddyMichael, TRUE)
////							PRINTSTRING("TASK_VEHICLE_MISSION(hBuddyMichael") PRINTNL()
////						ENDIF
////					ENDIF
////				ENDIF
////				IF DOES_ENTITY_EXIST(GetAwayCar[1])
////					IF IS_VEHICLE_DRIVEABLE(GetAwayCar[1])			
////						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(GetAwayCar[1])
////							STOP_PLAYBACK_RECORDED_VEHICLE(GetAwayCar[1])
////						ENDIF
////						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyFranklin, FALSE)
////							CLEAR_PED_TASKS(hBuddyFranklin)
////							TASK_VEHICLE_MISSION(hBuddyFranklin, GetAwayCar[1], HeliToSteal, MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS, -1, -1)
////							SET_PED_KEEP_TASK(hBuddyFranklin, TRUE)
////							PRINTSTRING("TASK_VEHICLE_MISSION(hBuddyFranklin") PRINTNL()
////						ENDIF
////					ENDIF
////				ENDIF
////			ENDIF
////			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
////				IF DOES_ENTITY_EXIST(GetAwayCar[0])
////					IF IS_VEHICLE_DRIVEABLE(GetAwayCar[0])			
////						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(GetAwayCar[0])
////							STOP_PLAYBACK_RECORDED_VEHICLE(GetAwayCar[0])
////						ENDIF
////						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyMichael, FALSE)
////							CLEAR_PED_TASKS(hBuddyMichael)
////							TASK_VEHICLE_MISSION(hBuddyMichael, GetAwayCar[0], HeliToSteal, MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS, -1, -1)
////							SET_PED_KEEP_TASK(hBuddyMichael, TRUE)
////							PRINTSTRING("TASK_VEHICLE_MISSION(hBuddyMichael") PRINTNL()
////						ENDIF
////					ENDIF
////				ENDIF
////				IF DOES_ENTITY_EXIST(GetAwayCar[1])
////					IF IS_VEHICLE_DRIVEABLE(GetAwayCar[1])			
////						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(GetAwayCar[1])
////							STOP_PLAYBACK_RECORDED_VEHICLE(GetAwayCar[1])
////						ENDIF
////						IF IS_BATTLEBUDDY_AVAILABLE(hBuddyTrevor, FALSE)
////							CLEAR_PED_TASKS(hBuddyTrevor)
////							TASK_VEHICLE_MISSION(hBuddyTrevor, GetAwayCar[1], HeliToSteal, MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS, -1, -1)
////							SET_PED_KEEP_TASK(hBuddyTrevor, TRUE)
////							PRINTSTRING("TASK_VEHICLE_MISSION(hBuddyTrevor") PRINTNL()
////						ENDIF
////					ENDIF
////				ENDIF
////			ENDIF
////			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(HeliToSteal)
//			Mission_Passed()			
//		
//		ENDIF
//	
//	ENDIF
ENDPROC


//// PURPOSE: Sets where the player will be warped to if they accept the replay
//PROC SET_REPLAY_ACCEPTED_WARP_LOCATION()
//	MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<1872.4281, 3322.7512, 42.8431>>, 100.8492)
//ENDPROC

PROC DO_STAGE_MISSION_FAILED()

	SWITCH iControlFlag 
	
		CASE 0 // init
			CPRINTLN(DEBUG_MISSION, "Init DO_STAGE_MISSION_FAILED")
			CLEAR_PRINTS()
			CLEAR_HELP()
			// remove all blips here

			// set fail reason
			SWITCH eFailReason
				CASE FAIL_DEFAULT
					// no fail reason
				BREAK
				CASE FAIL_HELI_DESTROYED
					sFailReason = "DP_FAIL1"//The Cargobob was destroyed.
				BREAK
				CASE FAIL_HELI_ABANDONED
					sFailReason = "DP_FAIL2"//Trevor abandoned the Cargobob.
				BREAK
				CASE FAIL_HELI_STUCK
					sFailReason = "DP_FAIL3"//The Cargobob got stuck
				BREAK
			ENDSWITCH
			
			// call appropriate mission failed function
			IF eFailReason = FAIL_DEFAULT
				MISSION_FLOW_MISSION_FAILED()
			ELSE
				MISSION_FLOW_MISSION_FAILED_WITH_REASON(sFailReason)
			ENDIF	

			iControlFlag = 1
		BREAK
		
		CASE 1 // wait for fail fade to finish
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				
				// check if we need to respawn the player in a different position, 
				// if replay accepted, warp to short distance away from the blip position
				// (far enough away so that the mission gets blipped)
				IF HAS_PLAYER_ACCEPTED_REPLAY()
//					SET_REPLAY_ACCEPTED_WARP_LOCATION()
				ELSE
					// if he rejects the replay only warp him to just outside military base if he failed inside it
					// (This is a restricted area so is already handled by the replay controller)

					MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-1139.1643, 2662.4648, 16.9873>>, 74.9925)
					
//					SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(vHeliCoords, fHeliHeading)
					SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<-1147.5040, 2663.1035, 17.0938>>, 40.2433)
				ENDIF
				
				MISSION_CLEANUP() // must only take 1 frame and terminate the thread
			ELSE
				// fade not finished, you may want to handle dialogue or other behaviour here
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC



// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	PRINTSTRING("DOCKS HEIST PREP 2B LAUNCHED")
	PRINTNL()

	SET_MISSION_FLAG(TRUE)
	
	IF (HAS_FORCE_CLEANUP_OCCURRED())
//		SET_REPLAY_ACCEPTED_WARP_LOCATION()
		Mission_Flow_Mission_Force_Cleanup()
		Mission_Cleanup()
	ENDIF
	
	SET_WEATHER_TYPE_OVERTIME_PERSIST("EXTRASUNNY", 30)
	
	SETUP_MISSION()	
	
	IF NOT IS_REPLAY_IN_PROGRESS()
		missionStage = STAGE_OPENING_CUTSCENE	
	ELSE
		missionStage = STAGE_INIT_MISSION
	ENDIF
	
	WHILE (TRUE)
	
		WAIT(0)
		
		//Run a controller for player characters every frame through out the mission.
//		PLAYER_BUDDY_CONTRLER()		
		
		//Controls the tasks for the battle buddies if they are driving the vehicles.
//		IF missionStage <> STAGE_END_CUTSCENE
//			MONITOR_BATTLE_BUDDIES()
//		ENDIF
		
		//For video recorder
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ThePortOfLSHeistPrep2B") 		
		
		SCENARIO_CONTROLLER()
		
		MISSION_DIALOGUE_CONTROL()
		
		STATS_CONTROL()
		
		POLICE_CHECK()
		
		MISC_CHECK()
		
//		PRINTSTRING("mission is running and missionstage = ") PRINTINT(ENUM_TO_INT(missionStage)) PRINTNL()
		
		SWITCH missionStage

			CASE STAGE_OPENING_CUTSCENE
				DO_STAGE_OPENING_CUTSCENE()
			BREAK

			CASE STAGE_INIT_MISSION
				DO_STAGE_INIT_MISSION()
			BREAK
			
			CASE STAGE_STEAL_CARGOBOB
				DO_STAGE_STEAL_CARGOBOB()
			BREAK	
			
			CASE STAGE_ESCAPE_MILITARY
				DO_STAGE_ESCAPE_MILITARY()
			BREAK
			
			CASE STAGE_FLY_CARGOBOB_TO_HANGER
				DO_STAGE_FLY_CARGOBOB_TO_HANGER()
			BREAK
			
			CASE STAGE_END_CUTSCENE
				DO_STAGE_END_CUTSCENE()
			BREAK		
			
			CASE STAGE_MISSION_FAILED
				DO_STAGE_MISSION_FAILED()
			BREAK
			
		ENDSWITCH

		
		// only do fail checks and debug keys if we haven't failed mission 
		IF missionStage <> STAGE_MISSION_FAILED
			
			//Run fail checks throughout mission only if the mission is allowed to fail.
			IF missionCanFail = TRUE
				FAIL_CHECKS()
			ENDIF
			
			#IF IS_DEBUG_BUILD
				DEBUG_KEYS()
//				DO_DEBUG_NAMES()
			#ENDIF		
		
			#IF IS_DEBUG_BUILD
				//Checks for debug keys being pressed. J-skip, S-Pass, F-Fail, P-Previous
				IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, 0) = TRUE
					IF NOT IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_OUT(0)
					ENDIF
					iControlFlag = 0
					IF iReturnStage = 0
						MissionStageBeingSkippedTo = TRUE
						missionStage = STAGE_STEAL_CARGOBOB
					ENDIF
					IF iReturnStage = 1
						MissionStageBeingSkippedTo = TRUE
						missionStage = STAGE_ESCAPE_MILITARY
					ENDIF
					IF iReturnStage = 2
						MissionStageBeingSkippedTo = TRUE
						missionStage = STAGE_FLY_CARGOBOB_TO_HANGER
					ENDIF
					IF iReturnStage = 3
						MissionStageBeingSkippedTo = TRUE
						missionStage = STAGE_END_CUTSCENE
					ENDIF
				ENDIF
			#ENDIF			
		ENDIF
		
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT

