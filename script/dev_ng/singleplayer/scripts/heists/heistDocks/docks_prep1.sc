// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	get mini sub
//		AUTHOR			:	Craig Vincent
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "commands_itemsets.sch"

USING "script_blips.sch"
Using "Locates_public.sch"
Using "select_mission_stage.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"
USING "chase_hint_cam.sch"
using "mission_stat_public.sch"
using "cutscene_public.sch"
USING "replay_public.sch"
USING "taxi_functions.sch"
USING "area_checks.sch"
using "script_ped.sch"
using "RC_helper_functions.sch" 
using "asset_management_public.sch"
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

//-----------------------------------------------------------------------------------------------------------
//		ENUMS
//----------------------------------------------------------------------------------------------------------
ENUM MPF_MISSION_PED_FLAGS	
	mpf_mike,
	mpf_trev,
	mpf_frank,
	mpf_floyd,
	mpf_dockphone,
	mpf_dock2,
	mpf_sec1,
	mpf_sec2,
	
	MPF_NUM_OF_PEDS
ENDENUM
ENUM MVF_MISSION_VEHICLE_FLAGS
	mvf_sub,
	mvf_trailer,
	mvf_truck,
	mvf_bison,
	
	MVF_NUM_OF_VEH
ENDENUM
ENUM MSF_MISSION_STAGE_FLAGS
	msf_0_reached_sub,
	msf_1_get_to_crane,
	MSF_2_SAFETY,
	msf_3_passed,
	
	MSF_NUM_OF_STAGES
ENDENUM
ENUM mof_mission_object_flag
	mof_dummy1,
	mof_dummy2,
	mof_button,
	mof_phone,
	mof_subCover,
	mof_subStraps,
	mof_craneLD,
	mof_cranecab,
	mof_cranehook,
	
	MOF_NUM_OF_OBJECTS
endenum
ENUM MFF_MISSION_FAIL_FLAGS
	mff_debug_fail,
	mff_destroyed_sub,
	mff_destroyed_truck,
	mff_stuck_truck,
	mff_stuck_sub,
	mff_lost_sub,
	mff_player_dead,
	mff_floyd_dead,
	mff_floyd_car_dead,
	mff_left_area,
	mff_left_sub,
	mff_left_floyd,
	mff_default,	
	MFF_NUM_OF_FAILS
ENDENUM
Enum STAGE_SWITCHSTATE
	STAGESWITCH_IDLE,
	STAGESWITCH_REQUESTED,
	STAGESWITCH_EXITING,
	STAGESWITCH_ENTERING	
ENDENUM
enum AI_ENUM
	AI_NORM_TASK,
	AI_STEALTH_STAGE,
	AI_CALM_ALERT,
	AI_FULL_ALERT
endenum
enum CALM_ENUMS
	CALM_WARN1,
	CALM_WARN2,
	CALM_CALL_POLICE
endenum
enum ALARM_ENUM
	ALARM_NONE,
	ALARM_gun,
	ALARM_explosion,
	ALARM_SUB_SEEN,
	ALARM_POLICE,
	ALARM_FLEE
endenum
enum eDIALOGUE_SUBSTAGE
	DIA_CONDITIONS,
	DIA_TRIGGER,
	DIA_RUNNING,
	DIA_EXIT
endenum
enum eDIALOGUE_STAGE
	DHP1_TSUB,
	DHP1_TSUB2,
	DHP1_APP,
	DHP1_WHERE,
	DHP1_TRCK,
	DHP1_WHSE,
	DHP1_PARK
endenum

//-----------------------------------------------------------------------------------------------------------
//		STRUCTS
//-----------------------------------------------------------------------------------------------------------
STRUCT VEHICLE_STRUCT
	VEHICLE_INDEX 		id
	BLIP_INDEX			blip
endstruct
Struct PEDS_STRUCT
	PED_INDEX			id	
	BLIP_INDEX			blip
	AI_BLIP_STRUCT		blipstruct
ENDSTRUCT
struct object_struct
	OBJECT_INDEX		id
	BLIP_INDEX			blip
ENDSTRUCT

STRUCT ROPE_DATA
	ROPE_INDEX 	rope
	FLOAT 		fLength
	INT 		iNumSegments
	FLOAT 		fLengthPerSegment
	BOOL		bSnapped				= FALSE
ENDSTRUCT
//-----------------------------------------------------------------------------------------------------------
//		CONSTANTS
//-----------------------------------------------------------------------------------------------------------
CONST_INT				STAGE_ENTRY		0
CONST_INT				STAGE_EXIT		-1 

string					AnimDictReaction = "REACTION@MALE_STAND@BIG_VARIATIONS@IDLE_B"
string					AnimDictPhone 	 = "cellphone@str"
string					AnimDictMission	 = "missheistdocksprep1ig_1"	

VECTOR					vWAREHOUSE		 = <<-343.37607, -2633.53491, 5.00103>>
//-----------------------------------------------------------------------------------------------------------
//		VARIABLES
//-----------------------------------------------------------------------------------------------------------

VEHICLE_STRUCT					Vehs[MVF_NUM_OF_VEH]	//holds all of the vehicles for tis level
PEDS_STRUCT						peds[MPF_NUM_OF_PEDS]	//holds all of the peds for this level
rope_data						ropes[4]
VECTOR							subCGOffset

object_struct					objects[MOF_NUM_OF_OBJECTS]
LOCATES_HEADER_DATA 			sLocatesData
structPedsForConversation 		convo_struct			//holds peds in the conversation

BLIP_INDEX						blip_objective			//blip for mission objective

SEQUENCE_INDEX					seq						//used to create AI sequence

PTFX_ID							ptfx_DrippingFromSub

int								syncbutton

INT 							i
bool							bRopesCreated
bool							bCraneRopes
bool 							bactivatePhysics = FALSE
bool							bPhoneAdded

bool							b_displayGodText
bool							bHelpDisplayed 	= false
bool							bDistWarn 		= false
bool							battachprint 	= false
bool							bBoardedShip	= false
bool							bFailForStuck	= false
BOOL							b_PlayedPoliceScannerLine = FALSE

rel_group_hash					rel_buddy
rel_group_hash					rel_dock

INT								iHelpTimer = -1
INT								iSubHelp

BOOL 							bWaterSlashedSound

//----------------------AI---------------------
AI_ENUM 			eAI_stage
CALM_ENUMS			eCalm_stage
ALARM_ENUM			eALARM

int 				AITimer
int 				iStartTasktimer
bool				bINIT_workerTasks 
bool				brun = false
weapon_type 		wcurrent
int 				iWaypointPause
int 				iambspeechTimer
bool 				bsetwanted = false

#if IS_DEBUG_BUILD
	bool bDisplayDebug
	int wAIstage 
	int wCalm_stage
	int wAlarm_stage
	bool debug_bCurrentlyOnShip
#endif

//------------------------------- CRANE ------------------------------------------
int 					icranestage = 0

vector					vsub 			= <<327.263, -2968.799, -3>>
float 					hsub 			= 358.5

MODEL_NAMES				mCraneLD		= PROP_DOCK_CRANE_02_LD
vector					vcraneLD 		= <<305.053, -2971.395, 4.990>>
float 					hcraneLD		= 90

MODEL_NAMES				mCraneCab		= PROP_DOCK_CRANE_02_CAB
vector					voffsetcab		= <<0,0,19.9070>>
float 					hcranecab		= 0

MODEL_NAMES				mCraneHook		= PROP_DOCK_CRANE_02_HOOK
vector					voffsethook		= <<-0.005,-22.139,-15>>
float 					hcranehook		= 0
vector 					hookheight	

vector					vTopRope		= <<-0.005, -22.139,32.825>>
vector					vBottomRope		= <<0,0,0.68>>

ROPE_INDEX				craneRope

//vector					vSubOffset		= <<-9.052903, 0.364908, -2.452997>> - <<-9.055566, 4.358799, -1.653515>>
vector					vTarpOffset		= <<-9.107174, 1.993344, -1.499111>> - <<-9.052903, 0.364908, -2.452997>>
//vector					vStrapOffset	= <<-9.049294, 2.221005, -1.697000>> - <<-9.052903, 0.364908, -2.452997>>


//------------------------------- CAMS ------------------------------------------
float 						fNearClip
Camera_index				cameraIndex				//in game cut scene camera
bool						bcueCam
//------------CS---------------
bool bcs_trev 		= false
bool bcs_floyd 		= false
bool bcs_truck 		= false
bool bcs_sub 		= false
bool bcs_cover 		= false
bool bcs_trailer	= false

//----- stage game variable's -----
int 	iStageDelay
bool	bLoadstage		= false
bool	bcreateScene 	= false
//---------audio----------------
bool	DI_RAND_CONV
int 	dialogueTimer		
bool 	bAUDIOStreamLoaded 	= false

eDIALOGUE_SUBSTAGE 	dialogue_substage
eDIALOGUE_STAGE		dialogue_stage

//============================== streaming ============================== 
ASSET_MANAGEMENT_DATA	sAssetData
CUTSCENE_PED_VARIATION 	sCutscenePedVariationRegister[MPF_NUM_OF_PEDS]

//Stage Management and skip stuff
STAGE_SWITCHSTATE		stageswitch				//current switching status
INT						mission_stage			//current stage
INT						mission_substage		//current substage
INT						requestedStage			//the mission stage requested by a mission_stage switch
INT						iStageTimer				//timer used for debug
Bool					bDoSkip					//trigger the skip
INT						iSkipToStage			//the stage to skip to
//------------------------------WIDGET STUFF--------------------------------------------------
#if IS_DEBUG_BUILD
	MissionStageMenuTextStruct zMenuNames[MSF_NUM_OF_STAGES]
	WIDGET_GROUP_ID 		widget_debug
#endif

float 	WfRopeLength 	= 	3
	
// ===========================================================================================================
//		Termination
// ===========================================================================================================
// -----------------------------------------------------------------------------------------------------------
//		Mission Helpers
// -----------------------------------------------------------------------------------------------------------
FUNC BOOL ISENTITYALIVE(ENTITY_INDEX ENTITY)
  	IF DOES_ENTITY_EXIST(ENTITY)
		IF IS_ENTITY_A_VEHICLE(ENTITY)
			IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(ENTITY))
				RETURN TRUE 
			ENDIF
		ELIF IS_ENTITY_A_PED(ENTITY)
			IF NOT IS_PED_INJURED(GET_PED_INDEX_FROM_ENTITY_INDEX(ENTITY))
	          	RETURN TRUE 
	    	ENDIF	
		ENDIF           
	ENDIF
      RETURN FALSE
ENDFUNC
PROC POINT_GAMEPLAY_CAM_AT_COORD(FLOAT TARGETHEADING)	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(TARGETHEADING - GET_ENTITY_HEADING(PLAYER_PED_ID()))
ENDPROC

FUNC PED_INDEX TREV()
	RETURN PEDS[MPF_TREV].ID
ENDFUNC

PROC DeleteRopes()
	IF bRopesCreated
		DELETE_ROPE(ropes[0].rope)
		DELETE_ROPE(ropes[1].rope)
		DELETE_ROPE(ropes[2].rope)
		DELETE_ROPE(ropes[3].rope)		
		bRopesCreated = FALSE
	ENDIF
ENDPROC


// purpose:off radar/ hud / player control
proc prep_start_cutscene(bool bplayercontrol,vector varea,bool brender = true,bool binterp = false,bool bclearprj = true,int interptime = default_interp_to_from_game,set_player_control_flags controlflag = 0,bool bphoneaway = true)
	display_radar(false)
	display_hud(false)	
	set_player_control(player_id(),bplayercontrol,controlflag)
	clear_prints()
	clear_help()
	render_script_cams(brender,binterp,interptime)	
	if bclearprj
		clear_area_of_projectiles(varea,500)
	endif
	STOP_FIRE_IN_RANGE(varea,300)
	if IS_PED_IN_ANY_VEHICLE(player_ped_id())
		SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(player_ped_id()),false)
	endif
	if bphoneaway
		hang_up_and_put_away_phone()
	endif
endproc
// purpose:on radar/ hud / player control
proc prep_stop_cutscene(bool bplayercontrol,bool brender = false,bool binterp = false,int iduration = default_interp_to_from_game, set_player_control_flags controlflag = 0)
	display_radar(true)
	display_hud(true)	
	set_player_control(player_id(),bplayercontrol,controlflag)	
	render_script_cams(brender,binterp,iduration)	
	if not brender	
		destroy_all_cams()
	endif
	if IS_PED_IN_ANY_VEHICLE(player_ped_id())
		SET_VEHICLE_RADIO_ENABLED(GET_VEHICLE_PED_IS_IN(player_ped_id()),true)
	endif
endproc
proc AUDIO_manage_dialogue()
	switch dialogue_stage 
	
		case DHP1_TSUB
			switch DIALOGUE_SUBSTAGE
				case DIA_CONDITIONS
					if ISENTITYALIVE(vehs[mvf_sub].id)
						if not IS_MESSAGE_BEING_DISPLAYED()
						and IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_sub].id)
						and GET_PLAYER_WANTED_LEVEL(player_id()) = 0
							KILL_FACE_TO_FACE_CONVERSATION()
							dialogueTimer = get_game_timer()
							DIALOGUE_SUBSTAGE = DIA_TRIGGER
						endif
					endif
				break
				case DIA_TRIGGER
					if get_game_timer()- dialogueTimer > 6000
					and IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_sub].id)
						ADD_PED_FOR_DIALOGUE(convo_struct,1,null,"FLOYD")
						if CREATE_CONVERSATION(convo_struct,"DHP1AUD","DHP1_TSUB",CONV_PRIORITY_MEDIUM)
							DIALOGUE_SUBSTAGE = DIA_RUNNING
						endif		
					endif
				break
				case DIA_RUNNING
					if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()					
						if IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_sub].id)	
							if IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								PAUSE_FACE_TO_FACE_CONVERSATION(false)	
							endif
						else
							if not IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								PAUSE_FACE_TO_FACE_CONVERSATION(true)
							endif
						endif
					else
						DIALOGUE_SUBSTAGE = DIA_EXIT
					endif
				break
				case DIA_EXIT	
					dialogue_stage = DHP1_APP
					DIALOGUE_SUBSTAGE = DIA_CONDITIONS
					dialoguetimer = get_game_timer()
				break
			endswitch
		break
		
//		case DHP1_TSUB2			
//			switch DIALOGUE_SUBSTAGE
//				case DIA_CONDITIONS
//					if not IS_MESSAGE_BEING_DISPLAYED()
//					and IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_sub].id)	
//						KILL_FACE_TO_FACE_CONVERSATION()
//						DIALOGUE_SUBSTAGE = DIA_TRIGGER
//					endif	
//				break
//				case DIA_TRIGGER
//					if IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_sub].id)
//						if CREATE_CONVERSATION(convo_struct,"DHP1AUD","DHP1_TSUB2",CONV_PRIORITY_MEDIUM)
//							DIALOGUE_SUBSTAGE = DIA_RUNNING
//						endif	
//					endif
//				break
//				case DIA_RUNNING
//					if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//						if IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_sub].id)	
//							if IS_FACE_TO_FACE_CONVERSATION_PAUSED()
//								PAUSE_FACE_TO_FACE_CONVERSATION(false)	
//							endif
//						else
//							if not IS_FACE_TO_FACE_CONVERSATION_PAUSED()
//								PAUSE_FACE_TO_FACE_CONVERSATION(true)
//							endif
//						endif
//					else
//						DIALOGUE_SUBSTAGE = DIA_EXIT
//					endif
//				break
//				case DIA_EXIT	
//					dialogue_stage = DHP1_APP
//					DIALOGUE_SUBSTAGE = DIA_CONDITIONS
//					dialoguetimer = get_game_timer()
//				break
//			endswitch			
//			//break out of convo conditions
//			if IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_sub].id)	
//			and GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id()),<<324.76532, -2974.49463,-1.0>>) < 110
//				KILL_ANY_CONVERSATION()
//				DIALOGUE_SUBSTAGE = DIA_EXIT
//			endif			
//		break
		
		case DHP1_APP
			switch DIALOGUE_SUBSTAGE
				case DIA_CONDITIONS
					if not IS_MESSAGE_BEING_DISPLAYED()
					and IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_sub].id)	
					and GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id()),<<324.76532, -2974.49463,-1.0>>) < 110
						KILL_FACE_TO_FACE_CONVERSATION()
						DIALOGUE_SUBSTAGE = DIA_TRIGGER
					endif	
				break
				case DIA_TRIGGER
					if CREATE_CONVERSATION(convo_struct,"DHP1AUD","DHP1_APP",CONV_PRIORITY_MEDIUM)
						DIALOGUE_SUBSTAGE = DIA_RUNNING
					endif		
				break
				case DIA_RUNNING
					if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						
					else
						DIALOGUE_SUBSTAGE = DIA_EXIT
					endif
				break
				case DIA_EXIT	
					dialogue_stage = DHP1_WHERE
					DIALOGUE_SUBSTAGE = DIA_CONDITIONS
					dialoguetimer = get_game_timer()
				break
			endswitch
		break
		
		case DHP1_WHERE
			if ISENTITYALIVE(vehs[mvf_truck].id)
			and ISENTITYALIVE(peds[mpf_floyd].id)
				switch DIALOGUE_SUBSTAGE
					case DIA_CONDITIONS				
						if IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_truck].id)
						and IS_PED_IN_VEHICLE(peds[mpf_floyd].id,vehs[mvf_truck].id)
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								KILL_FACE_TO_FACE_CONVERSATION()
							ELSE
								DIALOGUE_SUBSTAGE = DIA_TRIGGER
							ENDIF
						endif
					break
					case DIA_TRIGGER						
						play_ped_ambient_speech_with_voice(peds[mpf_floyd].id,"DHP1_AIAA","FLOYD")
						DIALOGUE_SUBSTAGE = DIA_RUNNING
					break
					case DIA_RUNNING
						if not IS_AMBIENT_SPEECH_PLAYING(peds[mpf_floyd].id)
							DIALOGUE_SUBSTAGE = DIA_EXIT
						endif
					break
					case DIA_EXIT
						dialogue_stage = DHP1_TRCK
						DIALOGUE_SUBSTAGE = DIA_CONDITIONS
						dialoguetimer = get_game_timer()
					break
				endswitch
			endif
		break
		
		case DHP1_TRCK		
			if ISENTITYALIVE(vehs[mvf_truck].id)
			and ISENTITYALIVE(peds[mpf_floyd].id)
				switch DIALOGUE_SUBSTAGE
					case DIA_CONDITIONS				
						if IS_PED_IN_ANY_VEHICLE(player_ped_id())
						and IS_PED_IN_VEHICLE(peds[mpf_floyd].id, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						and IS_ENTITY_ATTACHED_TO_ENTITY(vehs[mvf_trailer].id,GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						and not IS_MESSAGE_BEING_DISPLAYED()
							KILL_FACE_TO_FACE_CONVERSATION()
							dialoguetimer = get_game_timer()
							DIALOGUE_SUBSTAGE = DIA_TRIGGER
						endif
					break
					case DIA_TRIGGER
						if get_game_timer() - dialogueTimer > 2000
							ADD_PED_FOR_DIALOGUE(convo_struct,1,peds[mpf_floyd].id,"FLOYD")
							if CREATE_CONVERSATION(convo_struct,"DHP1AUD","DHP1_TRCK",CONV_PRIORITY_MEDIUM)
								DIALOGUE_SUBSTAGE = DIA_RUNNING
							endif		
						endif
					break					
					case DIA_RUNNING
						if IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							if IS_PED_IN_ANY_VEHICLE(player_ped_id())
							and IS_PED_IN_VEHICLE(peds[mpf_floyd].id,GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
							and IS_ENTITY_ATTACHED_TO_ENTITY(vehs[mvf_trailer].id,GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
								if IS_FACE_TO_FACE_CONVERSATION_PAUSED()
									PAUSE_FACE_TO_FACE_CONVERSATION(false)	
								endif
							else
								if not IS_FACE_TO_FACE_CONVERSATION_PAUSED()
									PAUSE_FACE_TO_FACE_CONVERSATION(true)
								endif
							endif
						else
							DIALOGUE_SUBSTAGE = DIA_EXIT
						endif
					break
					case DIA_EXIT
						dialogue_stage = DHP1_WHSE
						DIALOGUE_SUBSTAGE = DIA_CONDITIONS
						dialoguetimer = get_game_timer()
					break
				endswitch
				
				if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehs[mvf_sub].id),<<-323.73004, -2613.70166, 5>>) < 60
					KILL_ANY_CONVERSATION()
					DIALOGUE_SUBSTAGE = DIA_EXIT
				endif
			endif
		break
		
		case DHP1_WHSE
			switch DIALOGUE_SUBSTAGE
				case DIA_CONDITIONS
					if IS_PED_IN_ANY_VEHICLE(player_ped_id())
					and IS_PED_IN_VEHICLE(peds[mpf_floyd].id, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					and IS_ENTITY_ATTACHED_TO_ENTITY(vehs[mvf_trailer].id,GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					and not IS_MESSAGE_BEING_DISPLAYED()
					//and GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehs[mvf_sub].id),<<-343.37607, -2633.53491, 5.00103>>) < 100 // 70
					and IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-273.635010,-2558.817871,4.751376>>, <<-405.721771,-2695.898438,14.374950>>, 51.000000)
						KILL_FACE_TO_FACE_CONVERSATION()
						DIALOGUE_SUBSTAGE = DIA_TRIGGER
					endif
				break
				case DIA_TRIGGER
					DI_RAND_CONV = GET_RANDOM_BOOL()	
					ADD_PED_FOR_DIALOGUE(convo_struct,1,peds[mpf_floyd].id,"FLOYD")
					if DI_RAND_CONV					
						if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"DHP1AUD","DHP1_WHSE","DHP1_WHSE_1",CONV_PRIORITY_MEDIUM)
							DIALOGUE_SUBSTAGE = DIA_RUNNING
						endif	
					else
						if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"DHP1AUD","DHP1_WHSE","DHP1_WHSE_2",CONV_PRIORITY_MEDIUM)
							DIALOGUE_SUBSTAGE = DIA_RUNNING
						endif
					endif	
				break
				case DIA_RUNNING
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						DIALOGUE_SUBSTAGE = DIA_EXIT
					endif
				break
				case DIA_EXIT
					dialogue_stage = DHP1_PARK
					DIALOGUE_SUBSTAGE = DIA_CONDITIONS
					dialoguetimer = get_game_timer()
				break
			endswitch
		break
		
		case DHP1_PARK
			switch DIALOGUE_SUBSTAGE
				case DIA_CONDITIONS
					if IS_PED_IN_ANY_VEHICLE(player_ped_id())
					and IS_PED_IN_VEHICLE(peds[mpf_floyd].id, GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					and IS_ENTITY_ATTACHED_TO_ENTITY(vehs[mvf_trailer].id,GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					and not IS_MESSAGE_BEING_DISPLAYED()
					and GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehs[mvf_sub].id),<<-323.73004, -2613.70166, 5>>) < 40
						KILL_FACE_TO_FACE_CONVERSATION()
						DIALOGUE_SUBSTAGE = DIA_TRIGGER
					endif
				break
				case DIA_TRIGGER
					DI_RAND_CONV = GET_RANDOM_BOOL()	
					ADD_PED_FOR_DIALOGUE(convo_struct,1,peds[mpf_floyd].id,"FLOYD")
					if DI_RAND_CONV
						if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"DHP1AUD","DHP1_PARK","DHP1_PARK_1",CONV_PRIORITY_MEDIUM)
							DIALOGUE_SUBSTAGE = DIA_RUNNING
						endif	
					else
						if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"DHP1AUD","DHP1_PARK","DHP1_PARK_2",CONV_PRIORITY_MEDIUM)
							DIALOGUE_SUBSTAGE = DIA_RUNNING
						endif
					endif	
				break
				case DIA_RUNNING
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						DIALOGUE_SUBSTAGE = DIA_EXIT
					endif
				break
				case DIA_EXIT
					
				break
			endswitch
		break
	endswitch	
endproc
proc SET_ALL_EXIT_STATES(bool bstate = false)	
	bcs_trev		= bstate	
	bcs_floyd 		= bstate
	bcs_truck		= bstate
	bcs_cover 		= bstate	
	bcs_sub			= bstate
	bcs_trailer		= bstate
endproc
proc move_hook(bool bdown = true,FLOAT vel = 2.0)
//SPREADER
		
	if bdown
		voffsethook.z = voffsethook.z -@ vel
	else
		voffsethook.z = voffsethook.z +@ vel
	endif
	
	SET_ENTITY_COORDS_NO_OFFSET(objects[mof_craneHook].id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objects[mof_craneCab].id,voffsethook))
	
endproc
proc rotate_cab(bool banticw = true,float vel = 4.5)
	
	if banticw
		hcranecab = hcranecab +@vel
	else
		hcranecab = hcranecab -@vel
	endif
	
	DETACH_ENTITY(objects[mof_craneCab].id)
	ATTACH_ENTITY_TO_ENTITY(objects[mof_craneCab].id,objects[mof_craneLD].id, -1, voffsetcab, <<0.0, 0.0, hcranecab>>)
	SET_ENTITY_COORDS_NO_OFFSET(objects[mof_craneHook].id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objects[mof_craneCab].id,voffsethook))	
	
endproc
func bool init_crane
	//bottom of crane
		if not DOES_ENTITY_EXIST(objects[mof_craneLD].id)
			objects[mof_craneLD].id = CREATE_OBJECT(mCraneLD,vcraneLD)	
			SET_ENTITY_COORDS(objects[mof_craneLD].id,vcraneLD)
			SET_ENTITY_HEADING(objects[mof_craneLD].id,hcraneLD)
			FREEZE_ENTITY_POSITION(objects[mof_craneLD].id,true)
		endif

		//cabin
		if not DOES_ENTITY_EXIST(objects[mof_craneCab].id)
			objects[mof_craneCab].id = CREATE_OBJECT(mCraneCab, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objects[mof_craneLD].id, voffsetcab))
		endif
		if not IS_ENTITY_ATTACHED_TO_ENTITY(objects[mof_craneCab].id,objects[mof_craneLD].id)
			ATTACH_ENTITY_TO_ENTITY(objects[mof_craneCab].id,objects[mof_craneLD].id, -1, voffsetcab, <<0.0, 0.0, hcranecab>>)		
		endif		
		//hook
		if not DOES_ENTITY_EXIST(objects[mof_craneHook].id)
			objects[mof_craneHook].id = CREATE_OBJECT(mCraneHook, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objects[mof_craneCab].id, voffsethook))
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(objects[mof_craneHook].id, FALSE)
			ATTACH_ENTITY_TO_ENTITY(objects[mof_craneHook].id,objects[mof_craneCab].id, -1, voffsethook, <<0.0, 0.0, hcranehook>>)
			DETACH_ENTITY(objects[mof_craneHook].id,true,false)
			FREEZE_ENTITY_POSITION(objects[mof_craneHook].id,true)
		endif
		
		IF NOT bCraneRopes
			IF DOES_ENTITY_HAVE_PHYSICS(objects[mof_craneCab].id)
			AND DOES_ENTITY_HAVE_PHYSICS(objects[mof_craneHook].id)
				println("!!!!!!!!!!!!!ROPE MADE!!!!!!!")
				craneRope = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objects[mof_craneCab].id, vtopRope),
									<<0.0, 0.0, 0.0>>, 70, PHYSICS_ROPE_THIN, -1, 4, 1.4)

				ROPE_SET_SMOOTH_REELIN(craneRope, TRUE )					
				ROPE_SET_UPDATE_ORDER(craneRope, 2 )
				ROPE_DRAW_SHADOW_ENABLED(craneRope, FALSE)
																
				ATTACH_ENTITIES_TO_ROPE(craneRope, objects[mof_craneCab].id, objects[mof_craneHook].id,
							GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objects[mof_craneCab].id, vTopRope),
							GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objects[mof_craneHook].id, vBottomRope)
							, 70 , 0, 0)
				bCraneRopes = true	
				//FREEZE_ENTITY_POSITION(objects[mof_craneHook].id,false)
				return true
			endif
			if not DOES_ENTITY_HAVE_PHYSICS(objects[mof_craneCab].id)
				println("!!!!!!!!!!!!!cab no physics!!!!!!!")
			endif
			if not DOES_ENTITY_HAVE_PHYSICS(objects[mof_craneHook].id)
				println("!!!!!!!!!!!!!hook no physics!!!!!!!")
			endif
		endif
		
return false
endfunc

INT iGenericTimer

func bool crane_cs()
	switch icranestage
	case 0
		DeleteRopes()
		
		//sub
		SET_ENTITY_COORDS(vehs[mvf_sub].id,vsub)
		SET_ENTITY_HEADING(vehs[mvf_sub].id,hsub)
		FREEZE_ENTITY_POSITION(vehs[mvf_sub].id,true)
		ptfx_DrippingFromSub = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_pls_sub_water_drips",vehs[mvf_sub].id,<<0.0,0.0,0.0>>,<<0,0,0>>)
		SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_DrippingFromSub,"flow",1)
		//bug 1114387 EVO
		REQUEST_CUTSCENE("dhp1_mcs_1")	
		if bAUDIOStreamLoaded
			PLAY_STREAM_FRONTEND()
		endif
		
		// 1985471
		IF GET_CLOCK_HOURS() >= 20
		OR GET_CLOCK_HOURS() <= 7
			REQUEST_IPL( "PO1_08_sub_waterplane" )
		ENDIF
		
		Load_Asset_NewLoadScene_Frustum(sAssetData, <<298.9806, -2977.6431, 6.1010>>, <<298.5479, -2964.6653, 7.8860>> - <<298.9806, -2977.6431, 6.1010>>, 3000)
		
		icranestage++
	break
	case 1		
		hookheight = GET_ENTITY_COORDS(objects[mof_craneHook].id)
		if hookheight.z <= 1.0		
			FREEZE_ENTITY_POSITION(vehs[mvf_sub].id,false)
			SET_CAM_PARAMS(cameraIndex,<<310.0923, -2949.5293, 10.9528>>, <<-20.2654, 2.7992, -141.6393>>,25.6,0)//<<319.0,-2957.5,9.7>>, <<-29.7,-0.0,-148.2>>
			SET_CAM_PARAMS(cameraIndex,<<308.3542, -2950.9724, 10.8434>>, <<0.0544, 2.7992, -137.3849>>,25.6,12500)//<<318.9986, -2957.5005, 9.7000>>, <<3.3584, -0.0704, -148.2573>>		
			ATTACH_ENTITY_TO_ENTITY(Vehs[mvf_sub].id,objects[mof_craneHook].id,-1,<<2.47,0,-1.96>>,<<0,0,-90>>)			
			icranestage++
		else			
			move_hook(true,2)
		endif
	break
	case 2	
		hookheight = GET_ENTITY_COORDS(objects[mof_craneHook].id)
		if hookheight.z >= 12.5
			SET_CAM_PARAMS(cameraIndex, <<356.9992, -2984.9612, 117.2741>>, <<-68.2497, -0.1106, 66.9743>>,20,0)
			SET_CAM_PARAMS(cameraIndex,<<361.4758, -2966.8240, 117.2872>>, <<-66.6976, -0.1106, 84.3720>>,20,10000)	
			fNearClip = GET_CAM_NEAR_CLIP(cameraIndex)
			SET_CAM_NEAR_CLIP(cameraIndex,1.8)
			
			icranestage++
		else	
			move_hook(false,1)
		endif		
	break
	case 3
		if hcranecab >= 40		
			SET_CAM_PARAMS(cameraIndex,<<289.8129, -2944.0457, 5.5715>>, <<17.0750, 0.1698, -109.4507>> ,38.3,0)//<<290.7,-2943.7,6.7>>, <<5.3,0.1,-109.7>>
			SET_CAM_PARAMS(cameraIndex,<<290.8941, -2941.9873, 5.5781>>, <<22.5176, 0.1698, -128.6537>>,38.3,7000)// <<290.5289, -2942.3059, 6.7100>>, <<19.0786, 0.2589, -134.1299>>			
			SET_CAM_NEAR_CLIP(cameraIndex,fNearClip)
			hcranecab = 80
			DETACH_ENTITY(objects[mof_craneCab].id)
			ATTACH_ENTITY_TO_ENTITY(objects[mof_craneCab].id,objects[mof_craneLD].id, -1, voffsetcab, <<0.0, 0.0, hcranecab>>)
			SET_ENTITY_COORDS_NO_OFFSET(objects[mof_craneHook].id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objects[mof_craneCab].id,voffsethook))
			iGenericTimer = GET_GAME_TIMER()
			icranestage++
		else
			rotate_cab(true,5.5)
		endif
	break
	case 4
		if hcranecab >= 108	
			OVERRIDE_LODSCALE_THIS_FRAME(1.0)
			SET_CAM_PARAMS(cameraIndex,<<300.0253, -2973.2529, 5.7369>>, <<10.5662, 0.1117, 4.0913>>,22.7929,0) //<<296.9,-2961.8,7.0>>, <<7.9, 0.0000,-4.8>>
			SET_CAM_PARAMS(cameraIndex,<<300.0292, -2973.2939, 6.1746>>, <<5.2629, 0.1117, 4.0913>>,22.7929,8000)	//<<296.9,-2961.7,6.5>>, <<7.9, 0.0000,-4.8>>
			SET_CAM_SHAKE_AMPLITUDE(cameraIndex, 0.25)
			DETACH_ENTITY(objects[mof_craneCab].id)
			ATTACH_ENTITY_TO_ENTITY(objects[mof_craneCab].id,objects[mof_craneLD].id, -1, voffsetcab, <<0.0, 0.0, hcranecab>>)
			SET_ENTITY_COORDS_NO_OFFSET(objects[mof_craneHook].id, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(objects[mof_craneCab].id,voffsethook))
			if DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_DrippingFromSub)
				STOP_PARTICLE_FX_LOOPED(ptfx_DrippingFromSub)
			endif
			icranestage++
		else
			float percentage				
			percentage = 1 - CLAMP(TO_FLOAT(GET_GAME_TIMER() - (iGenericTimer + 1500))/3000.0, 0.0, 1.0) 			
			println("flow percentage: ",percentage)
			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfx_DrippingFromSub,"flow",percentage)		
		
			rotate_cab(true,5.5)
		endif
	break
	case 5
		OVERRIDE_LODSCALE_THIS_FRAME(1.0)
		hookheight = GET_ENTITY_COORDS(objects[mof_craneHook].id)
		println("hook height: ", hookheight.z)
		if hookheight.z <= 11		
			DETACH_ENTITY(vehs[mvf_sub].id)
			icranestage++
		else
			move_hook(true,1)
		endif
	break
	case 6
		if HAS_ENTITY_COLLIDED_WITH_ANYTHING(Vehs[mvf_sub].id)
//			FREEZE_ENTITY_POSITION(Vehs[mvf_sub].id,true)
			iGenericTimer = GET_GAME_TIMER() + 1000
			icranestage++
		endif
	break
	case 7
		//if hcranecab <= 90
		if GET_GAME_TIMER() > iGenericTimer
			FREEZE_ENTITY_POSITION(objects[mof_craneHook].id,true)
			icranestage++
		else			
			rotate_cab(false,4)
			move_hook(false,1)
		endif		
	break
	case 8
		Unload_Asset_NewLoadScene(sAssetData)
		return true
	break
	endswitch
	
	IF IS_SCREEN_FADED_IN()
		if icranestage > 0
		and icranestage < 5
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
			endif
		ENDIF
	ELIF IS_SCREEN_FADED_OUT()
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_DrippingFromSub)
			STOP_PARTICLE_FX_LOOPED(ptfx_DrippingFromSub)
		ENDIF
		REMOVE_PARTICLE_FX_IN_RANGE(GET_ENTITY_COORDS(Vehs[mvf_sub].id), 30.0)
		iSkipToStage = ENUM_TO_INT(MSF_2_SAFETY)
		bDoSkip = true
	ENDIF
	
return false
endfunc
proc Manage_AI()
//----------------------------------------------------------- NORMAL --------------------------------------------------------------------		
	switch eAI_stage
		case AI_NORM_TASK
			if ISENTITYALIVE(peds[mpf_dockphone].id)
				if bINIT_workerTasks	
					task_play_anim(peds[mpf_dockphone].id,animDictPhone,"cellphone_call_listen_c",normal_blend_in,normal_blend_out,-1,af_looping | af_secondary| af_upperbody)
					if GET_IS_WAYPOINT_RECORDING_LOADED("docksprep1")
						TASK_FOLLOW_WAYPOINT_RECORDING(peds[mpf_dockphone].id,"docksprep1",0,EWAYPOINT_START_FROM_CLOSEST_POINT)	
					else
						REQUEST_WAYPOINT_RECORDING("docksprep1")
					endif
				endif					
			endif
			brun = false
			eAI_stage = AI_STEALTH_STAGE
		break
//----------------------------------------------------------- CHECKING  --------------------------------------------------------------------				
		case AI_STEALTH_STAGE //check if need to enter alerted stages		
			if not bINIT_workerTasks
				if get_game_timer() - iStartTasktimer > 2000				
				or GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(peds[mpf_dockphone].id)) < 10			
					bINIT_workerTasks = true
					eAI_stage = AI_NORM_TASK
				endif
			endif
			
			//--------------------------------------------------------
			if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),<<1240.51318, -2880.35571, 2.10339>>,<<1240.55945, -3057.27075, 17.4>>,29.45)
				if ISENTITYALIVE(peds[mpf_dockphone].id)
					if CAN_PED_SEE_HATED_PED(peds[mpf_dockphone].id,player_ped_id())
						CLEAR_PED_TASKS(peds[mpf_dockphone].id)
						STOP_CURRENT_PLAYING_AMBIENT_SPEECH(peds[mpf_dockphone].id)
						ADD_PED_FOR_DIALOGUE(convo_struct,3,peds[mpf_dockphone].id,"CONSTRUCTION2")
						eAI_stage = AI_CALM_ALERT					
					ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(peds[mpf_dockphone].id)) < 12
					and GET_PLAYER_CURRENT_STEALTH_NOISE(player_id()) > 8.5					
						STOP_CURRENT_PLAYING_AMBIENT_SPEECH(peds[mpf_dockphone].id)
						if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_dockphone].id,SCRIPT_TASK_TURN_PED_TO_FACE_COORD)
							TASK_TURN_PED_TO_FACE_COORD(peds[mpf_dockphone].id,GET_ENTITY_COORDS(PLAYER_PED_ID()),4000)						
							task_play_anim(peds[mpf_dockphone].id,"MISSHEISTDOCKSPREP1HOLD_CELLPHONE","HOLD_CELLPHONE",SLOW_BLEND_IN,normal_blend_out,-1,af_looping | af_secondary| AF_UPPERBODY)
						endif
					elif GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(peds[mpf_dockphone].id)) < 12
					and IS_PED_IN_ANY_VEHICLE(player_ped_id())
						if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_dockphone].id,SCRIPT_TASK_TURN_PED_TO_FACE_COORD)
							TASK_TURN_PED_TO_FACE_COORD(peds[mpf_dockphone].id,GET_ENTITY_COORDS(PLAYER_PED_ID()),4000)
							task_play_anim(peds[mpf_dockphone].id,"MISSHEISTDOCKSPREP1HOLD_CELLPHONE","HOLD_CELLPHONE",SLOW_BLEND_IN,normal_blend_out,-1,af_looping | af_secondary| AF_UPPERBODY)
						endif
					elif IS_FLASH_LIGHT_ON(PLAYER_PED_ID())
					and( GET_CLOCK_HOURS() >= 19 or GET_CLOCK_HOURS() < 6)
					and GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),peds[mpf_dockphone].id) < 25
					and( 	IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(),peds[mpf_dockphone].id) 
						 or IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(),peds[mpf_dockphone].id))
						if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_dockphone].id,SCRIPT_TASK_TURN_PED_TO_FACE_COORD)
							TASK_TURN_PED_TO_FACE_COORD(peds[mpf_dockphone].id,GET_ENTITY_COORDS(PLAYER_PED_ID()),4000)
							task_play_anim(peds[mpf_dockphone].id,"MISSHEISTDOCKSPREP1HOLD_CELLPHONE","HOLD_CELLPHONE",SLOW_BLEND_IN,normal_blend_out,-1,af_looping | af_secondary| AF_UPPERBODY)
						endif	
					ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(peds[mpf_dockphone].id)) < 12
					and IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						if not IS_SCRIPT_TASK_RUNNING_OR_STARTING(peds[mpf_dockphone].id,SCRIPT_TASK_TURN_PED_TO_FACE_COORD)
							TASK_TURN_PED_TO_FACE_COORD(peds[mpf_dockphone].id,GET_ENTITY_COORDS(PLAYER_PED_ID()),4000)
							task_play_anim(peds[mpf_dockphone].id,"MISSHEISTDOCKSPREP1HOLD_CELLPHONE","HOLD_CELLPHONE",SLOW_BLEND_IN,normal_blend_out,-1,af_looping | af_secondary| AF_UPPERBODY)
						endif
					endif
					
				endif
				//see subs missing
				if mission_substage > 1			
					if ISENTITYALIVE(peds[mpf_dockphone].id)
						if HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(peds[mpf_dockphone].id,vehs[mvf_sub].id)	
							STOP_CURRENT_PLAYING_AMBIENT_SPEECH(peds[mpf_dockphone].id)
							ADD_PED_FOR_DIALOGUE(convo_struct,3,peds[mpf_dockphone].id,"CONSTRUCTION2")						
							eAI_stage = AI_FULL_ALERT
							eALARM = ALARM_SUB_SEEN
							CLEAR_PED_TASKS_IMMEDIATELY(peds[mpf_dockphone].id)
							TASK_FOLLOW_NAV_MESH_TO_COORD(peds[mpf_dockphone].id,<<1252.55811, -3004.84570, 8.31909>>,
															PEDMOVE_run,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,-122.93)
							iStartTasktimer = get_game_timer()
							bINIT_workerTasks = false
						ENDIF
					endif
				endif
			endif
								
			if ISENTITYALIVE(peds[mpf_dockphone].id)
			
				if not IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(peds[mpf_dockphone].id)
					if bINIT_workerTasks
						if 	GET_SCRIPT_TASK_STATUS(peds[mpf_dockphone].id,script_TASK_PERFORM_SEQUENCE) 		!= PERFORMING_TASK
						and GET_SCRIPT_TASK_STATUS(peds[mpf_dockphone].id,SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) != PERFORMING_TASK
						and GET_SCRIPT_TASK_STATUS(peds[mpf_dockphone].id,SCRIPT_TASK_TURN_PED_TO_FACE_COORD) 	!= PERFORMING_TASK
							if not IS_ENTITY_PLAYING_ANIM(peds[mpf_dockphone].id,animDictPhone,"cellphone_call_listen_c")
								task_play_anim(peds[mpf_dockphone].id,animDictPhone,"cellphone_call_listen_c",normal_blend_in,normal_blend_out,-1,af_looping | af_secondary| af_upperbody)
							endif
							if IS_ENTITY_AT_COORD(peds[mpf_dockphone].id,<<1228.05591, -3000.42749, 8.44240>>,<<2,2,2>>)
								if GET_IS_WAYPOINT_RECORDING_LOADED("docksprep1")
									TASK_FOLLOW_WAYPOINT_RECORDING(peds[mpf_dockphone].id,"docksprep1",0)	
								endif
							else
								if GET_IS_WAYPOINT_RECORDING_LOADED("docksprep1")
									TASK_FOLLOW_WAYPOINT_RECORDING(peds[mpf_dockphone].id,"docksprep1",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
								endif	
							endif
						endif
					endif
				else
					if get_game_timer() - iWaypointPause > 10000											
						if not WAYPOINT_PLAYBACK_GET_IS_PAUSED(peds[mpf_dockphone].id)
							if IS_ENTITY_AT_COORD(peds[mpf_dockphone].id,<<1251.62537, -3003.33838, 8.31909>>,<<2,2,2>>)
								WAYPOINT_PLAYBACK_PAUSE(peds[mpf_dockphone].id,false)
							elif IS_ENTITY_AT_COORD(peds[mpf_dockphone].id,<<1228.84851, -2986.28394, 8.31909>>,<<2,2,2>>)
								WAYPOINT_PLAYBACK_PAUSE(peds[mpf_dockphone].id,false)
							endif	
						else
							iWaypointPause = get_game_timer()
							WAYPOINT_PLAYBACK_RESUME(peds[mpf_dockphone].id,true,-1,3000)
						endif
					endif
					//------------------- ambient dialogue ----------------------
					if not is_ambient_speech_playing(peds[mpf_dockphone].id)				
					and get_Game_timer() - iambspeechTimer > 4000	
						//play_ped_ambient_speech_with_voice(peds[mpf_dockphone].id,"DHP1_ACAA","CONSTRUCTION2",speech_params_force)
						play_ped_ambient_speech_with_voice(peds[mpf_dockphone].id,"Sol1_EFAA","CONSTRUCTION2",speech_params_force)
						iambspeechTimer = get_Game_timer()					
					endif
				endif
			
				if HAS_PED_RECEIVED_EVENT(peds[mpf_dockphone].id, EVENT_SHOT_FIRED)
				or HAS_PED_RECEIVED_EVENT(peds[mpf_dockphone].id, EVENT_SHOT_FIRED_BULLET_IMPACT)
				or HAS_PED_RECEIVED_EVENT(peds[mpf_dockphone].id, EVENT_SHOT_FIRED_WHIZZED_BY)
				or HAS_PED_RECEIVED_EVENT(peds[mpf_dockphone].id, EVENT_SHOCKING_SEEN_WEAPON_THREAT)
					STOP_CURRENT_PLAYING_AMBIENT_SPEECH(peds[mpf_dockphone].id)
					ADD_PED_FOR_DIALOGUE(convo_struct,3,peds[mpf_dockphone].id,"CONSTRUCTION2")
					eAI_stage = AI_FULL_ALERT
					eALARM = ALARM_gun			
					task_play_anim(peds[mpf_dockphone].id,"MISSHEISTDOCKSPREP1HOLD_CELLPHONE","HOLD_CELLPHONE",INSTANT_BLEND_IN,normal_blend_out,-1,af_looping | af_secondary| AF_UPPERBODY)
					TASK_HANDS_UP(peds[mpf_dockphone].id,-1,player_ped_id(), -1, HANDS_UP_STRAIGHT_TO_LOOP)
				elif HAS_PED_RECEIVED_EVENT(peds[mpf_dockphone].id, EVENT_EXPLOSION_HEARD)
					STOP_CURRENT_PLAYING_AMBIENT_SPEECH(peds[mpf_dockphone].id)
					ADD_PED_FOR_DIALOGUE(convo_struct,3,peds[mpf_dockphone].id,"CONSTRUCTION2")
					eAI_stage = AI_FULL_ALERT
					eALARM = ALARM_explosion
					CLEAR_PED_TASKS(peds[mpf_dockphone].id)
					task_play_anim(peds[mpf_dockphone].id,"MISSHEISTDOCKSPREP1HOLD_CELLPHONE","HOLD_CELLPHONE",INSTANT_BLEND_IN,normal_blend_out,-1,af_looping | af_secondary| AF_UPPERBODY)
					TASK_COWER(peds[mpf_dockphone].id,-1)
				elif HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(peds[mpf_dockphone].id,PLAYER_PED_ID())
					STOP_CURRENT_PLAYING_AMBIENT_SPEECH(peds[mpf_dockphone].id)
					task_play_anim(peds[mpf_dockphone].id,"MISSHEISTDOCKSPREP1HOLD_CELLPHONE","HOLD_CELLPHONE",INSTANT_BLEND_IN,normal_blend_out,-1,af_looping | af_secondary| AF_UPPERBODY)
					ADD_PED_FOR_DIALOGUE(convo_struct,3,peds[mpf_dockphone].id,"CONSTRUCTION2")
					eAI_stage = AI_FULL_ALERT
					eALARM = ALARM_POLICE
				endif
			endif
			
		break
//----------------------------------------------------------- CALM  --------------------------------------------------------------------				
		case AI_CALM_ALERT
			if ISENTITYALIVE(peds[mpf_dockphone].id)
				if not IS_PED_HEADTRACKING_PED(peds[mpf_dockphone].id,player_ped_id())
					TASK_LOOK_AT_ENTITY(peds[mpf_dockphone].id,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_USE_TORSO)
				endif
			endif
		
			switch eCalm_stage
				case CALM_WARN1
					if ISENTITYALIVE(peds[mpf_dockphone].id)	
						//if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"DHP1AUD","DHP1_HEY","DHP1_HEY_1",CONV_PRIORITY_MEDIUM)
						if CREATE_CONVERSATION(convo_struct, "SOL1AUD", "SOL1_SEE3", CONV_PRIORITY_MEDIUM)
							println("OPEN_1")
							OPEN_SEQUENCE_TASK(seq)
								TASK_GO_TO_ENTITY(null,player_ped_id(),DEFAULT_TIME_NEVER_WARP,4)								
								TASK_TURN_PED_TO_FACE_ENTITY(null,player_ped_id(),-1)
							println("CLOSE_1")
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(peds[mpf_dockphone].id,seq)
							CLEAR_SEQUENCE_TASK(seq)
							task_play_anim(peds[mpf_dockphone].id,"MISSHEISTDOCKSPREP1HOLD_CELLPHONE","HOLD_CELLPHONE",INSTANT_BLEND_IN,normal_blend_out,-1,af_looping | af_secondary| AF_UPPERBODY)
							AITimer = get_game_timer()						
							eCalm_stage = CALM_WARN2
						endif
					endif
				break
				case CALM_WARN2
					if GET_GAME_TIMER() - AITimer > 4000//8000					
						if ISENTITYALIVE(peds[mpf_dockphone].id)
							//if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"DHP1AUD","DHP1_HEY","DHP1_HEY_2",CONV_PRIORITY_MEDIUM)
							if CREATE_CONVERSATION(convo_struct, "SOL1AUD", "SOL1_SEE4", CONV_PRIORITY_MEDIUM)
								println("OPEN_2")
								OPEN_SEQUENCE_TASK(seq)
									TASK_GO_TO_ENTITY(null,player_ped_id(),DEFAULT_TIME_NEVER_WARP,3,PEDMOVEBLENDRATIO_WALK)
									TASK_TURN_PED_TO_FACE_ENTITY(null,player_ped_id(),-1)
								println("CLOSE_2")
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(peds[mpf_dockphone].id,seq)
								CLEAR_SEQUENCE_TASK(seq)
								task_play_anim(peds[mpf_dockphone].id,"MISSHEISTDOCKSPREP1HOLD_CELLPHONE","HOLD_CELLPHONE",INSTANT_BLEND_IN,normal_blend_out,-1,af_looping | af_secondary| AF_UPPERBODY)
								AITimer = get_game_timer()
								//convo
								eCalm_stage = CALM_CALL_POLICE
							endif
						endif
					endif
				break
				case CALM_CALL_POLICE
					if GET_GAME_TIMER() - AITimer > 8000
						if ISENTITYALIVE(peds[mpf_dockphone].id)
							//if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"DHP1AUD","DHP1_HEY","DHP1_HEY_3",CONV_PRIORITY_MEDIUM)								
							if CREATE_CONVERSATION(convo_struct, "SOL1AUD", "SOL1_COPS2", CONV_PRIORITY_MEDIUM)
								AITimer = get_game_timer()
								ealarm = ALARM_POLICE
								eAI_stage = AI_FULL_ALERT							
							endif
						endif
					endif
				break
			endswitch
			//---------------------------------------- REVERT -------------------------------------------					
			if not IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),<<1240.51318, -2880.35571, 2.10339>>,<<1240.55945, -3057.27075, 17.4>>,29.45)				
			or not ISENTITYALIVE(peds[mpf_dockphone].id)			
				eAI_stage = AI_NORM_TASK				
			endif
			//---------------------------------------- ALERT -------------------------------------------	
								
			GET_CURRENT_PED_WEAPON(player_ped_id(),wcurrent)	
						
			if ISENTITYALIVE(peds[mpf_dockphone].id)
				if HAS_PED_RECEIVED_EVENT(peds[mpf_dockphone].id, EVENT_SHOT_FIRED)
				or HAS_PED_RECEIVED_EVENT(peds[mpf_dockphone].id, EVENT_SHOT_FIRED_BULLET_IMPACT)
				or HAS_PED_RECEIVED_EVENT(peds[mpf_dockphone].id, EVENT_SHOT_FIRED_WHIZZED_BY)
					
					ADD_PED_FOR_DIALOGUE(convo_struct,3,peds[mpf_dockphone].id,"CONSTRUCTION2")
					eAI_stage = AI_FULL_ALERT
					eALARM = ALARM_gun			
					TASK_HANDS_UP(peds[mpf_dockphone].id,-1,player_ped_id(), -1, HANDS_UP_STRAIGHT_TO_LOOP)
					task_play_anim(peds[mpf_dockphone].id,"MISSHEISTDOCKSPREP1HOLD_CELLPHONE","HOLD_CELLPHONE",INSTANT_BLEND_IN,normal_blend_out,-1,af_looping | af_secondary| AF_UPPERBODY)
				elif IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),peds[mpf_dockphone].id)
				or IS_PLAYER_TARGETTING_ENTITY(player_id(),peds[mpf_dockphone].id)
				 	if GET_WEAPONTYPE_GROUP(wcurrent) != WEAPONGROUP_MELEE
				 	and wcurrent != WEAPONTYPE_UNARMED
						
						ADD_PED_FOR_DIALOGUE(convo_struct,3,peds[mpf_dockphone].id,"CONSTRUCTION2")
						eAI_stage = AI_FULL_ALERT
						eALARM = ALARM_gun			
						CLEAR_PED_TASKS_IMMEDIATELY(peds[mpf_dockphone].id)
						TASK_HANDS_UP(peds[mpf_dockphone].id,-1,player_ped_id())
					endif
				elif HAS_PED_RECEIVED_EVENT(peds[mpf_dockphone].id, EVENT_EXPLOSION_HEARD)
					
					ADD_PED_FOR_DIALOGUE(convo_struct,3,peds[mpf_dockphone].id,"CONSTRUCTION2")
					eAI_stage = AI_FULL_ALERT
					eALARM = ALARM_explosion
					TASK_COWER(peds[mpf_dockphone].id,-1)
					task_play_anim(peds[mpf_dockphone].id,"MISSHEISTDOCKSPREP1HOLD_CELLPHONE","HOLD_CELLPHONE",INSTANT_BLEND_IN,normal_blend_out,-1,af_looping | af_secondary| AF_UPPERBODY)
				elif HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(peds[mpf_dockphone].id,PLAYER_PED_ID())
					
					ADD_PED_FOR_DIALOGUE(convo_struct,3,peds[mpf_dockphone].id,"CONSTRUCTION2")
					eAI_stage = AI_FULL_ALERT
					eALARM = ALARM_POLICE
				endif
			endif
		break
		
//----------------------------------------------------------- HOSTILE  --------------------------------------------------------------------		
		case AI_FULL_ALERT
			switch eALARM
				case ALARM_gun
					if ISENTITYALIVE(peds[mpf_dockphone].id)
						//if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"DHP1AUD","DHP1_SSUB","DHP1_SSUB_2",CONV_PRIORITY_MEDIUM)
						if CREATE_CONVERSATION(convo_struct, "SOL1AUD", "SOL1_ARM2", CONV_PRIORITY_MEDIUM)
							bRun = true
							ealarm = ALARM_POLICE
						endif
					else
						bINIT_workerTasks = true
						eAI_stage = AI_NORM_TASK
					endif
				break
				case ALARM_explosion
					if ISENTITYALIVE(peds[mpf_dockphone].id)
						//if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"DHP1AUD","DHP1_SSUB","DHP1_SSUB_1",CONV_PRIORITY_MEDIUM)
						if CREATE_CONVERSATION(convo_struct, "SOL1AUD", "SOL1_PAN2", CONV_PRIORITY_MEDIUM)
							bRun = true
							ealarm = ALARM_POLICE
						endif
					else
						bINIT_workerTasks = true
						eAI_stage = AI_NORM_TASK
					endif
				break
				case ALARM_SUB_SEEN
					if ISENTITYALIVE(peds[mpf_dockphone].id)
						if IS_ENTITY_AT_COORD(peds[mpf_dockphone].id,<<1252.55811, -3004.84570, 8.31909>>,<<2,2,2>>)
							//if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"DHP1AUD","DHP1_HEY","DHP1_HEY_4",CONV_PRIORITY_MEDIUM)
							if CREATE_CONVERSATION(convo_struct, "SOL1AUD", "SOL1_SUB1", CONV_PRIORITY_MEDIUM)
								bRun = true
								ealarm = ALARM_POLICE
							endif
						endif
						if not IS_PED_HEADTRACKING_ENTITY(peds[mpf_dockphone].id, vehs[mvf_sub].id)
							TASK_LOOK_AT_ENTITY(peds[mpf_dockphone].id,vehs[mvf_sub].id,-1,SLF_WHILE_NOT_IN_FOV|SLF_WIDEST_PITCH_LIMIT|SLF_WIDEST_YAW_LIMIT)
						endif
					else
						bINIT_workerTasks = true
						eAI_stage = AI_NORM_TASK
					endif
				break
				case ALARM_POLICE
					if ISENTITYALIVE(peds[mpf_dockphone].id)
						if not IS_PED_RAGDOLL(peds[mpf_dockphone].id)
						and not IS_PED_GETTING_UP(peds[mpf_dockphone].id)
							//if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"DHP1AUD","DHP1_SSUB","DHP1_SSUB_3",CONV_PRIORITY_MEDIUM)								
							if CREATE_CONVERSATION(convo_struct, "SOL1AUD", "SOL1_COPSIN2", CONV_PRIORITY_MEDIUM)
								CLEAR_PED_TASKS(peds[mpf_dockphone].id)
								println("OPEN_3")
								OPEN_SEQUENCE_TASK(seq)										
									TASK_PLAY_ANIM(null,animDictPhone,"cellphone_call_listen_c",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING | AF_SECONDARY | AF_UPPERBODY)
									if bRun
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<1228.77539, -2923.85156, 8.31909>>,PEDMOVE_RUN)
										TASK_SMART_FLEE_PED(null,player_ped_id(),200,-1)
									else	
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<1228.77539, -2923.85156, 8.31909>>,PEDMOVE_WALK)
										TASK_WANDER_IN_AREA(null,<<1183.2872, -2947.8699, 4.8986>>,100)
									endif
								println("CLOSE_3")
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(peds[mpf_dockphone].id,seq)
								CLEAR_SEQUENCE_TASK(seq)
								//convo
								AITimer = get_game_timer()		
								TRIGGER_MUSIC_EVENT("DHP1_ATTACKED")
								bsetwanted = false
								ealarm = alarm_flee								
							endif	
						endif
					else
						bINIT_workerTasks = true
						eAI_stage = AI_NORM_TASK
					endif
				break
				case ALARM_FLEE
					if get_game_timer() - aitimer > 8000
						if not bsetwanted
							if GET_PLAYER_WANTED_LEVEL(player_id()) < 1
								SET_PLAYER_WANTED_LEVEL_NO_DROP(player_id(),1)	
								SET_PLAYER_WANTED_LEVEL_NOW(player_id())
								bsetwanted = true
							endif
						endif
						if ISENTITYALIVE(peds[mpf_dockphone].id)
							if brun	
								if not IS_PED_FLEEING(peds[mpf_dockphone].id)
							 		TASK_SMART_FLEE_PED(peds[mpf_dockphone].id,player_ped_id(),200,-1)
							 	endif							
							else
								if GET_SCRIPT_TASK_STATUS(peds[mpf_dockphone].id,SCRIPT_TASK_WANDER_IN_AREA) != PERFORMING_TASK
									TASK_WANDER_IN_AREA(peds[mpf_dockphone].id,<<1183.2872, -2947.8699, 4.8986>>,100)
								endif
							endif	
						endif
					endif
					
					if not brun
						
						GET_CURRENT_PED_WEAPON(player_ped_id(),wcurrent)	
						
						if ISENTITYALIVE(peds[mpf_dockphone].id)
							if HAS_PED_RECEIVED_EVENT(peds[mpf_dockphone].id, EVENT_SHOT_FIRED)
							or HAS_PED_RECEIVED_EVENT(peds[mpf_dockphone].id, EVENT_SHOT_FIRED_BULLET_IMPACT)
							or HAS_PED_RECEIVED_EVENT(peds[mpf_dockphone].id, EVENT_SHOT_FIRED_WHIZZED_BY)								
								ADD_PED_FOR_DIALOGUE(convo_struct,3,peds[mpf_dockphone].id,"CONSTRUCTION2")	
								//if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"DHP1AUD","DHP1_SSUB","DHP1_SSUB_2",CONV_PRIORITY_MEDIUM)
								if CREATE_CONVERSATION(convo_struct, "SOL1AUD", "SOL1_ARM2", CONV_PRIORITY_MEDIUM)
									CLEAR_PED_TASKS(peds[mpf_dockphone].id)		
									println("OPEN_4")
									OPEN_SEQUENCE_TASK(seq)
										TASK_HANDS_UP(null,3000,player_ped_id())
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<1228.77539, -2923.85156, 8.31909>>,PEDMOVE_RUN)
										TASK_SMART_FLEE_PED(null,player_ped_id(),200,-1)
									println("CLOSE_4")
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(peds[mpf_dockphone].id,seq)
									CLEAR_SEQUENCE_TASK(seq)
									AITimer = get_game_timer()
									brun = true	
								endif
							elif (IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),peds[mpf_dockphone].id)
							or IS_PLAYER_TARGETTING_ENTITY(player_id(),peds[mpf_dockphone].id))
							and IS_PED_FACING_PED(peds[mpf_dockphone].id, player_ped_id(), 160)
							 	if GET_WEAPONTYPE_GROUP(wcurrent) != WEAPONGROUP_MELEE
							 	and wcurrent != WEAPONTYPE_UNARMED
									ADD_PED_FOR_DIALOGUE(convo_struct,4,peds[mpf_dockphone].id,"CONSTRUCTION2")	
									//if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"DHP1AUD","DHP1_SSUB","DHP1_SSUB_2",CONV_PRIORITY_MEDIUM)
									if CREATE_CONVERSATION(convo_struct, "SOL1AUD", "SOL1_SCAR2", CONV_PRIORITY_MEDIUM)
										CLEAR_PED_TASKS(peds[mpf_dockphone].id)		
										println("OPEN_5")
										OPEN_SEQUENCE_TASK(seq)
											TASK_HANDS_UP(null,3000,player_ped_id())
											TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<1228.77539, -2923.85156, 8.31909>>,PEDMOVE_RUN)
											TASK_SMART_FLEE_PED(null,player_ped_id(),200,-1)
										println("CLOSE_5")
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(peds[mpf_dockphone].id,seq)
										CLEAR_SEQUENCE_TASK(seq)
										brun = true	
										AITimer = get_game_timer()
									endif	
								endif
							elif HAS_PED_RECEIVED_EVENT(peds[mpf_dockphone].id, EVENT_EXPLOSION_HEARD)
								ADD_PED_FOR_DIALOGUE(convo_struct,3,peds[mpf_dockphone].id,"CONSTRUCTION2")	
								//if PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"DHP1AUD","DHP1_SSUB","DHP1_SSUB_1",CONV_PRIORITY_MEDIUM)
								if CREATE_CONVERSATION(convo_struct, "SOL1AUD", "SOL1_PAN2", CONV_PRIORITY_MEDIUM)
									CLEAR_PED_TASKS(peds[mpf_dockphone].id)
									println("OPEN_6")
									OPEN_SEQUENCE_TASK(seq)
										TASK_COWER(null,-1)
										TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<1228.77539, -2923.85156, 8.31909>>,PEDMOVE_RUN)
										TASK_SMART_FLEE_PED(null,player_ped_id(),200,-1)
									println("CLOSE_6")
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(peds[mpf_dockphone].id,seq)
									CLEAR_SEQUENCE_TASK(seq)
								brun = true	
								endif
							elif HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(peds[mpf_dockphone].id,PLAYER_PED_ID())
								CLEAR_PED_TASKS(peds[mpf_dockphone].id)		
								println("OPEN_7")
								OPEN_SEQUENCE_TASK(seq)
									TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<1228.77539, -2923.85156, 8.31909>>,PEDMOVE_RUN)
									TASK_SMART_FLEE_PED(null,player_ped_id(),200,-1)
								println("CLOSE_7")
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(peds[mpf_dockphone].id,seq)								
								CLEAR_SEQUENCE_TASK(seq)
								brun = true	
								AITimer = get_game_timer()
							endif
						endif	
					endif
					
				break							
			endswitch
		break
	endswitch
endproc
// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------
proc reset_everything()
// 	General settings
	IF IS_CUTSCENE_ACTIVE()			
		STOP_CUTSCENE_IMMEDIATELY()
		REMOVE_CUTSCENE()
		WHILE IS_CUTSCENE_ACTIVE()
			WAIT(0)
		ENDWHILE		
	ENDIF
//======================================= Destroy ======================================
	
	DeleteRopes()	
	if ISENTITYALIVE(vehs[mvf_sub].id)
		SET_CGOFFSET( vehs[mvf_sub].id, subCGOffset )
	endif
	g_sTriggerSceneAssets.flag = false		
	
	if bCraneRopes
		DELETE_ROPE(craneRope)
		bCraneRopes = FALSE
	endif
		
	//all peds are deleted
	for i = 0 TO Enum_to_int(MPF_NUM_OF_PEDS)-1
		CLEANUP_AI_PED_BLIP(peds[i].blipstruct)
		if DOES_ENTITY_EXIST(peds[i].id)
		AND (NOT IS_PED_INJURED(peds[i].id))
			if IS_PED_IN_ANY_VEHICLE(peds[i].id)				
				SET_PED_COORDS_NO_GANG(peds[i].id,(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(peds[i].id))+<<0,-2,0>>))				
			endif	
			SAFE_DELETE_PED(peds[i].id)
		endif		
	endfor	
	
	//all vehicles are deleted
	for i = 0 TO Enum_to_int(MVF_NUM_OF_VEH)-1
		if DOES_ENTITY_EXIST(vehs[i].id)	
			EMPTY_VEHICLE(vehs[i].id)
		 	SAFE_DELETE_VEHICLE(vehs[i].id)
		endif
	endfor		
	//all objects are deleted
	for i = 0 to enum_to_int(MOF_NUM_OF_OBJECTS) -1
		if DOES_ENTITY_EXIST(objects[i].id)
			SAFE_DELETE_OBJECT(objects[i].id)
		endif	
	endfor
	
	if DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_DrippingFromSub)
		STOP_PARTICLE_FX_LOOPED(ptfx_DrippingFromSub)
	endif
	
	KILL_ANY_CONVERSATION()
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
	MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_DOCKS_PREP_1)
//======================================= Reset ======================================	
	prep_stop_cutscene(true)
	//wanted level
	SET_WANTED_LEVEL_MULTIPLIER(1)	//Should this reset to 1 not 0? Ross W 1829889 //SET_WANTED_LEVEL_MULTIPLIER(0)	
	//reset player control
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	//clear AI tasks
	if not IS_PED_INJURED(player_ped_id())
		CLEAR_PLAYER_WANTED_LEVEL(player_id())
		CLEAR_PED_TASKS(player_ped_id())
		CLEAR_AREA(GET_ENTITY_COORDS(player_ped_id()),300,true)
	endif	
	CLEAR_AREA(<<1219.1857, -2977.8999, 4.8653>>,300,true)
	
	//bool		
	iStartTasktimer = get_game_timer()
	bPhoneAdded			= false
	bINIT_workerTasks 	= false
	eAI_stage 			= AI_NORM_TASK
	eCalm_stage 		= CALM_WARN1
	eALARM 				= ALARM_NONE
	DIALOGUE_SUBSTAGE 	= DIA_CONDITIONS
	b_displayGodText 	= false
	
	RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS)
	
	IF IS_IPL_ACTIVE( "PO1_08_sub_waterplane" )
		Unload_Asset_NewLoadScene(sAssetData)
		REMOVE_IPL( "PO1_08_sub_waterplane" )
	ENDIF
	
	STOP_STREAM()
	
ENDPROC
PROC Mission_Cleanup()
	PRINTSTRING("...Docks Prep 1 Mission Cleanup")
	PRINTNL()	
	
	IF IS_CUTSCENE_ACTIVE()			
		STOP_CUTSCENE_IMMEDIATELY()
		REMOVE_CUTSCENE()	
	ENDIF
	
	//======================================= Destroy ======================================		
	if IsEntityAlive(player_ped_id())
		if ISENTITYALIVE(vehs[mvf_sub].id)
			if IS_PED_IN_VEHICLE(player_ped_id(),vehs[mvf_sub].id)
				SET_PED_COORDS_NO_GANG(player_ped_id(),(GET_ENTITY_COORDS(vehs[mvf_sub].id)+ <<0,0,2>>))
			endif
		endif
	endif
	
	DeleteRopes()	
	if ISENTITYALIVE(vehs[mvf_sub].id)
		SET_CGOFFSET( vehs[mvf_sub].id, subCGOffset )
	endif
	g_sTriggerSceneAssets.flag = false	
	
	if bCraneRopes
		DELETE_ROPE(craneRope)
		bCraneRopes = FALSE
	endif
	
	if DOES_PARTICLE_FX_LOOPED_EXIST(ptfx_DrippingFromSub)
		STOP_PARTICLE_FX_LOOPED(ptfx_DrippingFromSub)
	endif
	
	for i = 0 TO Enum_to_int(MPF_NUM_OF_PEDS)-1
		if IsEntityAlive(peds[i].id)
			CLEANUP_AI_PED_BLIP(peds[i].blipstruct)
			if DOES_BLIP_EXIST(peds[i].blip)
				REMOVE_BLIP(peds[i].blip)
			endif
			if IS_PED_IN_GROUP(peds[i].id)
				REMOVE_PED_FROM_GROUP(peds[i].id)
			endif
			if peds[i].id != player_ped_id()
				SAFE_RELEASE_PED(peds[i].id)
			endif
		endif
	endfor		
//---------------------vehs-----------------------
	for i = 0 TO Enum_to_int(MVF_NUM_OF_VEH)-1		
		if DOES_BLIP_EXIST(vehs[i].blip)
			REMOVE_BLIP(vehs[i].blip)
		endif
		if IsEntityAlive(vehs[i].id)
			if i != ENUM_TO_INT(mvf_sub)			
				SAFE_RELEASE_VEHICLE(vehs[i].id)
			endif
		endif		
	endfor	
	DISABLE_TAXI_HAILING(FALSE) //PD requested by bug #312601
//---------------------objects----------------------		
	for i = 0 to enum_to_int(MOF_NUM_OF_OBJECTS) -1
		if DOES_ENTITY_EXIST(objects[i].id)
			if objects[i].id = objects[mof_subCover].id
			or objects[i].id = objects[mof_subStraps].id
				SAFE_RELEASE_OBJECT(objects[i].id,true)
			else
				SAFE_DELETE_OBJECT(objects[i].id)
			endif			
		endif	
	endfor
//====================================  Reset  ======================================
	
	//clear AI tasks
	if not IS_PED_INJURED(player_ped_id())	
			//reset player control
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	endif	
	SET_AUTO_GIVE_SCUBA_GEAR_WHEN_EXIT_VEHICLE(player_id(),true)
	CLEAR_WEATHER_TYPE_PERSIST()
	SET_WANTED_LEVEL_MULTIPLIER(1)	//wanted level for mission reset to normal
	SET_MAX_WANTED_LEVEL(5)
	DISPLAY_RADAR(true)
	DISPLAY_HUD(true)	
	
	ALLOW_DIALOGUE_IN_WATER(False)
	clear_mission_location_text_and_blips(slocatesdata)
	RESET_ALL_BATTLEBUDDY_BEHAVIOUR_REQUESTS()	
	
	IF IS_IPL_ACTIVE( "PO1_08_sub_waterplane" )
		REMOVE_IPL( "PO1_08_sub_waterplane" )
	ENDIF
		
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Passed()

	PRINTSTRING("...Docks Prep 1 Mission Passed")

	PRINTNL()
	Mission_Flow_Mission_Passed()
	Mission_Cleanup()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------

/// PURPOSE:
///    Sets where the player will be warped to if they accept the replay
PROC SET_REPLAY_ACCEPTED_WARP_LOCATION()
	MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<787.0854, -2973.8716, 5.0585>>, 256.9100 )
ENDPROC

PROC MISSION_FAILED(mff_mission_fail_flags fail_condition = mff_default)
	
	TRIGGER_MUSIC_EVENT("DHP1_FAIL")
	string strreason = ""
		
	//show fail message
	switch fail_condition
		case mff_debug_fail
			strreason = ""
		break
		case mff_destroyed_sub
			strreason = "dkp1_fdes1"
		break
		case mff_stuck_sub
			strreason = "dkp1_fstuck1"
		break
		case mff_lost_sub
			strreason = "DKP1_FLOST1"
		BREAK
		case mff_destroyed_truck
			strreason = "dkp1_fdes2"
		break
		case mff_stuck_truck
			strreason = "dkp1_fstuck2"
		break
		case mff_floyd_dead
			strreason = "dkp1_ffloyd"
		break
		case mff_floyd_car_dead
			strreason = "DKP1_FFCAR"
		break
		case mff_left_area
			strreason = "dkp1_FABD"
		break
		case mff_left_sub	
			strreason = "DKP1_FABS"		
		break
		case mff_left_floyd	
			strreason = "DKP1_FABF"		
		break
		case mff_player_dead
			strreason = "cmn_tdied"
		break
		default
			strreason = ""
		break
	endswitch
	
	mission_flow_mission_failed_with_reason(strreason)
	
	while not get_mission_flow_safe_to_cleanup()	
		wait(0)
	endwhile
	
	// check if we need to respawn the player in a different position, 
	// if replay accepted, warp to short distance away from the submarine area
	// (far enough away so that the mission gets blipped)
	IF HAS_PLAYER_ACCEPTED_REPLAY()
		SET_REPLAY_ACCEPTED_WARP_LOCATION()
	ELSE
		// if he rejects the replay only warp him to just outside sub area if he failed inside it
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1298.369141,-3062.099609,-71.226906>>, <<790.912537,-3068.998047,63.914192>>, 250.000000, FALSE, FALSE)
			// failed in submarine area, warp outside
			// (this is currently the same as the replay accepted warp pos- left it in in case you want to change it)
			MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<787.0854, -2973.8716, 5.0585>>, 256.9100 )
		ENDIF
	ENDIF
	
	mission_cleanup() // must only take 1 frame and terminate the thread
	terminate_this_thread()
endproc
// -----------------------------------------------------------------------------------------------------------
//		MISSION STAGE MANAGEMENT
// -----------------------------------------------------------------------------------------------------------

PROC MISSION_STAGE_MANAGEMENT()
	SWITCH STAGESWITCH
		CASE STAGESWITCH_REQUESTED
			PRINTLN("[STAGEMANAGEMENT] MISSION_STAGE SWITCH REQUESTED FROM MISSION_STAGE:", MISSION_STAGE, "TO MISSION_STAGE", REQUESTEDSTAGE)
			STAGESWITCH = STAGESWITCH_EXITING
			MISSION_SUBSTAGE = STAGE_EXIT
		BREAK
		CASE STAGESWITCH_EXITING
			PRINTLN("[STAGEMANAGEMENT] EXITING MISSION_STAGE: ", MISSION_STAGE)
			STAGESWITCH = STAGESWITCH_ENTERING
			MISSION_SUBSTAGE = STAGE_ENTRY
			MISSION_STAGE = REQUESTEDSTAGE
		BREAK
		CASE STAGESWITCH_ENTERING
			PRINTLN("[STAGEMANAGEMENT] ENTERED MISSION_STAGE: ", MISSION_STAGE)
			REQUESTEDSTAGE = -1
			STAGESWITCH = STAGESWITCH_IDLE
		BREAK
		CASE STAGESWITCH_IDLE
			IF (GET_GAME_TIMER() - ISTAGETIMER) > 2500
			PRINTLN("[STAGEMANAGEMENT] MISSION_STAGE: ", MISSION_STAGE, " MISSION_SUBSTAGE: ", MISSION_SUBSTAGE)
				ISTAGETIMER = GET_GAME_TIMER()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
FUNC BOOL MISSION_SET_STAGE(MSF_MISSION_STAGE_FLAGS NEWSTAGE)
	IF STAGESWITCH = STAGESWITCH_IDLE
		REQUESTEDSTAGE = ENUM_TO_INT(NEWSTAGE)
		STAGESWITCH = STAGESWITCH_REQUESTED
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC
PROC LOAD_ASSET_STAGE(MSF_MISSION_STAGE_FLAGS LOADSTAGE)	
	SWITCH LOADSTAGE			
		CASE MSF_0_REACHED_SUB
			LOAD_ASSET_MODEL(sAssetData,SUBMERSIBLE)			
			LOAD_ASSET_MODEL(sAssetData,PROP_LD_TEST_01)
			LOAD_ASSET_MODEL(sAssetData,prop_sub_release)
			LOAD_ASSET_MODEL(sAssetData,S_M_M_DOCKWORK_01)
			Load_Asset_AnimDict(sAssetData,AnimDictPhone)
			Load_Asset_AnimDict(sAssetData,AnimDictMission)
			Load_Asset_Waypoint(sAssetData,"docksprep1")
			LOAD_ASSET_MODEL(sAssetData,P_AMB_PHONE_01)
			Load_Asset_Ropes(sAssetData)
			Load_Asset_Audiobank(sAssetData, "PORT_OF_LS_PREP_1")
		BREAK		
		CASE MSF_1_GET_TO_CRANE
			LOAD_ASSET_MODEL(sAssetData,SUBMERSIBLE)
			LOAD_ASSET_MODEL(sAssetData,PACKER)
			LOAD_ASSET_MODEL(sAssetData,ARMYTRAILER)
			LOAD_ASSET_MODEL(sAssetData,mCraneLD)
			LOAD_ASSET_MODEL(sAssetData,mCraneCab)
			LOAD_ASSET_MODEL(sAssetData,mCraneHook)
			Load_Asset_PTFX(sAssetData)
			Load_Asset_Ropes(sAssetData)
		BREAK		
		CASE MSF_2_SAFETY
		CASE msf_3_passed
			LOAD_ASSET_MODEL(sAssetData,SUBMERSIBLE)
			LOAD_ASSET_MODEL(sAssetData,PACKER)
			LOAD_ASSET_MODEL(sAssetData,ARMYTRAILER)
			LOAD_ASSET_MODEL(sAssetData,PROP_SUB_COVER_01)
			LOAD_ASSET_MODEL(sAssetData,PROP_TARP_STRAP)
		BREAK				
	ENDSWITCH
ENDPROC
PROC GET_SKIP_STAGE_COORD_AND_HEADING(MSF_MISSION_STAGE_FLAGS eStage, VECTOR &vCoord, FLOAT &fHeading)

	SWITCH eStage
		CASE msf_0_reached_sub		vCoord = <<1219.1857, -2977.8999, 4.8653>> 		fHeading =178.1887 		BREAK
		CASE msf_1_get_to_crane		vCoord = <<1272.3978, -2982.0720, -3.2414>>		fHeading = 201.5542	 	BREAK
		CASE MSF_2_SAFETY			vCoord = <<297.5948, -2950.3545, 5.0008>> 		fHeading =359.1976	 	BREAK
		CASE msf_3_passed			vCoord = <<-317.3936, -2610.7983, 5.0003>>		fHeading =328.0017	 	BREAK	
	ENDSWITCH

ENDPROC
proc su_general()
if ISENTITYALIVE(player_ped_id())
	ADD_PED_FOR_DIALOGUE(convo_struct,0,player_ped_id(),"TREVOR")
endif
if ISENTITYALIVE(peds[mpf_floyd].id)
	ADD_PED_FOR_DIALOGUE(convo_struct,1,peds[mpf_floyd].id,"FLOYD")
endif
endproc
proc su_msf_0_reached_sub()
	if not does_entity_exist(vehs[mvf_sub].id)
		vehs[mvf_sub].id =	create_vehicle(submersible,<<1260.80225, -3006.41699, 18.3289>>, 178.9076)
		SET_VEHICLE_AS_RESTRICTED(VEHS[MVF_SUB].ID,0)
		freeze_entity_position(vehs[mvf_sub].id,true)		
		set_entity_load_collision_flag(vehs[mvf_sub].id,true)
		set_entity_should_freeze_waiting_on_collision(vehs[mvf_sub].id,false)			
		SET_VEHICLE_RADIO_ENABLED(vehs[mvf_sub].id, TRUE)
	endif	
	//peds
	if not does_entity_exist(peds[mpf_dockphone].id)
		peds[mpf_dockphone].id = create_ped(pedtype_mission,s_m_m_dockwork_01,<<1229.32166, -3002.87915, 8.31861>>,270)			
		task_play_anim(peds[mpf_dockphone].id,animDictPhone,"cellphone_call_listen_c",normal_blend_in,normal_blend_out,-1,af_looping | af_secondary| af_upperbody)
		set_ped_flee_attributes(peds[mpf_dockphone].id,fa_never_flee,false)
		set_ped_combat_attributes(peds[mpf_dockphone].id,ca_always_flee,true)
		set_ped_relationship_group_hash(peds[mpf_dockphone].id,rel_dock)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_dockphone].id,true)
		SET_PED_VISUAL_FIELD_CENTER_ANGLE(peds[mpf_dockphone].id,45)
		SET_PED_VISUAL_FIELD_MAX_ANGLE(peds[mpf_dockphone].id,50)
		SET_PED_VISUAL_FIELD_MIN_ANGLE(peds[mpf_dockphone].id,-50)
		STOP_PED_SPEAKING(peds[mpf_dockphone].id, TRUE)
	endif
	//objects
	if not does_entity_exist(objects[mof_dummy1].id)
		objects[mof_dummy1].id = create_object(prop_ld_test_01,<<1260.69153, -3008.28589, 23.73365>>)
		freeze_entity_position(objects[mof_dummy1].id,true)	
	endif
	if not does_entity_exist(objects[mof_dummy2].id)
		objects[mof_dummy2].id = create_object(prop_ld_test_01,<<1260.75208, -3004.99414, 22.68315>>)
		freeze_entity_position(objects[mof_dummy2].id,true)
	endif	
	if not does_entity_exist(objects[mof_button].id)
		objects[mof_button].id = create_object(prop_sub_release,<<1249.10547, -3007.96777, 9.68718>>)
		set_entity_rotation(objects[mof_button].id,<<0,0,90>>)	
		freeze_entity_position(objects[mof_button].id,true)
	endif
	if not does_entity_exist(objects[mof_phone].id)
		objects[mof_phone].id = create_object(p_amb_phone_01,<<1229.32166, -3002.87915, 8.31861>>)		
	endif
	bropescreated = false
	 
	REQUEST_WAYPOINT_RECORDING("docksprep1")
	while not GET_IS_WAYPOINT_RECORDING_LOADED("docksprep1")		
		wait(0)
	endwhile
	
	clear_Area(get_entity_coords(player_ped_id()),100,true)
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)				
	endif
	
	iStartTasktimer = get_game_timer()
	dialogue_stage = DHP1_TSUB
	DIALOGUE_SUBSTAGE = DIA_CONDITIONS
endproc
proc su_msf_1_get_to_crane()
	if not does_entity_exist(vehs[mvf_sub].id)
		vehs[mvf_sub].id =	create_vehicle(submersible,<<1272.3978, -2982.0720, -3.2414>>, 201.5542)
		SET_VEHICLE_RADIO_ENABLED(vehs[mvf_sub].id, TRUE)
	endif
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP(vehs[mvf_sub].id)
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)			
		set_ped_into_vehicle(player_ped_id(),vehs[mvf_sub].id)
	endif
	clear_Area(get_entity_coords(player_ped_id()),100,true)
	
	dialogue_stage = DHP1_TSUB
	DIALOGUE_SUBSTAGE = DIA_CONDITIONS
endproc
proc su_msf_2_safety()
	//vehs
	if not isentityalive(vehs[mvf_sub].id)
		vehs[mvf_sub].id =	create_vehicle(submersible,<<320.4211, -2975.9927, -11.1896>>, 2.5159)
		SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehs[mvf_sub].id, FALSE)
		set_vehicle_engine_on(vehs[mvf_sub].id,false,true)
		set_vehicle_lights(vehs[mvf_sub].id,force_vehicle_lights_off)
		SET_VEHICLE_RADIO_ENABLED(vehs[mvf_sub].id, TRUE)
		SET_CAN_CLIMB_ON_ENTITY(vehs[mvf_sub].id, FALSE)
	endif	
	if not isentityalive(vehs[mvf_truck].id) 
		vehs[mvf_truck].id = create_vehicle(packer,<<297.5948, -2950.3545, 5.0008>>, 359.1976)
		set_vehicle_colour_combination(vehs[mvf_truck].id,0)
		SET_VEHICLE_TYRES_CAN_BURST(vehs[mvf_truck].id,false)
		SET_VEHICLE_HAS_STRONG_AXLES(vehs[mvf_truck].id,true)
	endif
	if not isentityalive(vehs[mvf_trailer].id)
		vehs[mvf_trailer].id = create_vehicle(armytrailer,<<297.2917, -2970.2456, 4.8884>>, 359.0483)
		SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehs[mvf_trailer].id, FALSE)
		SET_VEHICLE_HAS_STRONG_AXLES(vehs[mvf_trailer].id,true)
	endif	
	//attaching	
	attach_vehicle_to_trailer(vehs[mvf_truck].id,vehs[mvf_trailer].id, 0.5)
	SET_TRAILER_LEGS_RAISED(vehs[mvf_trailer].id)
	attach_vehicle_on_to_trailer(vehs[mvf_sub].id,vehs[mvf_trailer].id,<<0,-4,-0.7>>,<<0,0,0>>,<<0,0,0>>,-1)
	
	//objects
	if not does_entity_exist(objects[mof_subcover].id)
		objects[mof_subcover].id = create_object(prop_sub_cover_01,<<297.5948, -2950.3545, 5.0008>>)
		attach_entity_to_entity(objects[mof_subcover].id,vehs[mvf_trailer].id,-1,vTarpOffset,<<0,0,0>>)
	endif
	if not does_entity_exist(objects[mof_subStraps].id)
		objects[mof_subStraps].id = create_object(PROP_TARP_STRAP, <<-332.8449,-2622.1572,8.7838>>)
		attach_entity_to_entity(objects[mof_subStraps].id,vehs[mvf_trailer].id,-1, vTarpOffset,<<0,0,0>>)
	endif
	
	//peds
	while not create_npc_ped_inside_vehicle(peds[mpf_floyd].id,char_floyd,vehs[mvf_truck].id,vs_front_right)
		wait(0)
	endwhile	
	CPRINTLN(debug_mission, "[DHP1] su_msf_2_safety(): Floyd created @ ", TIMERA())
	
	IF IS_REPLAY_BEING_SET_UP()
		CPRINTLN(debug_mission, "[DHP1] su_msf_2_safety(): Starting wait for replay setup @ ", TIMERA())
		END_REPLAY_SETUP(vehs[mvf_truck].id)
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)		
		set_ped_into_vehicle(player_ped_id(),vehs[mvf_truck].id)
	endif
	
	set_ped_relationship_group_hash(peds[mpf_floyd].id,rel_buddy)	
	clear_Area(get_entity_coords(player_ped_id()),100,true)
	dialogue_stage = DHP1_WHERE
	DIALOGUE_SUBSTAGE = DIA_CONDITIONS
	
endproc
proc SU_msf_3_passed()
	//vehs
	if not isentityalive(vehs[mvf_sub].id)
		vehs[mvf_sub].id =	create_vehicle(submersible,<<-337.7228, -2623.8347, 8.6595>>, 135.24)
		set_vehicle_is_considered_by_player(vehs[mvf_sub].id,false)
		set_vehicle_engine_on(vehs[mvf_sub].id,false,true)
		set_vehicle_lights(vehs[mvf_sub].id,force_vehicle_lights_off)
		SET_VEHICLE_RADIO_ENABLED(vehs[mvf_sub].id, TRUE)
		SET_CAN_CLIMB_ON_ENTITY(vehs[mvf_sub].id, FALSE)
	endif	
	if not isentityalive(vehs[mvf_truck].id) 
		vehs[mvf_truck].id = create_vehicle(packer,<<-337.7228, -2627.0376, 5.0010>>, 134.9894)
		set_vehicle_is_considered_by_player(vehs[mvf_truck].id,false)
		set_vehicle_colour_combination(vehs[mvf_truck].id,0)
		SET_VEHICLE_TYRES_CAN_BURST(vehs[mvf_truck].id,false)
		SET_VEHICLE_HAS_STRONG_AXLES(vehs[mvf_truck].id,true)
	endif
	if not isentityalive(vehs[mvf_trailer].id)
		vehs[mvf_trailer].id = create_vehicle(armytrailer,<<-331.7280,-2621.0059,7.8828>>, 135.24)
		
		SET_VEHICLE_HAS_STRONG_AXLES(vehs[mvf_trailer].id,true)
	endif	
	//attaching	
	attach_vehicle_to_trailer(vehs[mvf_truck].id,vehs[mvf_trailer].id, 0.5)
	SET_TRAILER_LEGS_RAISED(vehs[mvf_trailer].id)
	attach_vehicle_on_to_trailer(vehs[mvf_sub].id,vehs[mvf_trailer].id,<<0,-4,-0.7>>,<<0,0,0>>,<<0,0,0>>,-1)
	
	//objects
	if not does_entity_exist(objects[mof_subcover].id)
		objects[mof_subcover].id = create_object(prop_sub_cover_01,<<297.5948, -2950.3545, 5.0008>>)
		attach_entity_to_entity(objects[mof_subcover].id,vehs[mvf_trailer].id,-1,vTarpOffset,<<0,0,0>>)
	endif
	if not does_entity_exist(objects[mof_subStraps].id)
		objects[mof_subStraps].id = create_object(PROP_TARP_STRAP, <<-332.8449,-2622.1572,8.7838>>)
		attach_entity_to_entity(objects[mof_subStraps].id,vehs[mvf_trailer].id,-1, vTarpOffset,<<0,0,0>>)
	endif
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP()
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)				
	endif
endproc
PROC MISSION_STAGE_SKIP()
//A SKIP HAS BEEN MADE
	IF BDOSKIP = TRUE
		
		//BEGIN THE SKIP IF THE SWITCHING IS IDLE
		IF STAGESWITCH = STAGESWITCH_IDLE
			IF NOT IS_SCREEN_FADED_OUT()
				IF NOT IS_SCREEN_FADING_OUT()
					DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				ENDIF
			ELSE
				MISSION_SET_STAGE(INT_TO_ENUM(MSF_MISSION_STAGE_FLAGS, ISKIPTOSTAGE))
			ENDIF
		//NEEDS TO BE CARRIED OUT BEFORE STATES OWN ENTERING STAGE
		ELIF STAGESWITCH = STAGESWITCH_ENTERING
			
			RENDER_SCRIPT_CAMS(FALSE,FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			RESET_EVERYTHING()
			IF IS_CUTSCENE_ACTIVE()
				REMOVE_CUTSCENE()
			ENDIF
			
			Start_Skip_Streaming(sAssetData)
			
			// -------------------------- Not a Replay ----------------------------
			IF NOT IS_REPLAY_BEING_SET_UP()					
				VECTOR vWarpCoord
				FLOAT fWarpHeading
				GET_SKIP_STAGE_COORD_AND_HEADING(int_to_enum(msf_mission_stage_flags, iSkipToStage), vWarpCoord, fWarpHeading)				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpCoord)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fWarpHeading)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				Load_Asset_NewLoadScene_Sphere(sAssetData, vWarpCoord, 50.0)
			ENDIF
			// --------------------------------------------------------------------
			
			LOAD_ASSET_STAGE(INT_TO_ENUM(MSF_MISSION_STAGE_FLAGS, MISSION_STAGE))
			CPRINTLN(debug_mission, "[DHP1] MISSION_STAGE_SKIP(): Requested assets for skip @ ", TIMERA())
			
			WHILE NOT Update_Skip_Streaming(sAssetData)
				WAIT(0)
				CPRINTLN(debug_mission, "[DHP1] MISSION_STAGE_SKIP(): Waiting on Update_Skip_Streaming @ ", TIMERA())
			ENDWHILE	
			CPRINTLN(debug_mission, "[DHP1] MISSION_STAGE_SKIP(): Finished streaming assets @ ", TIMERA())
			
			SWITCH INT_TO_ENUM(MSF_MISSION_STAGE_FLAGS, MISSION_STAGE)			
				CASE MSF_0_REACHED_SUB	SU_MSF_0_REACHED_SUB()	BREAK				
				CASE MSF_1_GET_TO_CRANE	SU_MSF_1_GET_TO_CRANE()	BREAK
				CASE MSF_2_SAFETY		SU_MSF_2_SAFETY()		BREAK	
				CASE msf_3_passed		SU_msf_3_passed()		BREAK
			ENDSWITCH
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			CPRINTLN(debug_mission, "[DHP1] MISSION_STAGE_SKIP(): Skip finished @ ", TIMERA())
			
			TRIGGER_MUSIC_EVENT("DHP1_START")

			BDOSKIP = FALSE
			IF NOT IS_REPLAY_BEING_SET_UP()	
				Unload_Asset_NewLoadScene(sAssetData)
			endif
		ENDIF
#IF IS_DEBUG_BUILD
	//CHECK IS A SKIP BEING ASKED FOR, DONT ALLOW SKIP DURING SETUP STAGE
	ELIF LAUNCH_MISSION_STAGE_MENU(ZMENUNAMES, ISKIPTOSTAGE, MISSION_STAGE, TRUE)
		IF ISKIPTOSTAGE > ENUM_TO_INT(MSF_NUM_OF_STAGES)-1
			MISSION_PASSED()
		ELSE
			ISKIPTOSTAGE = CLAMP_INT(ISKIPTOSTAGE, 0, ENUM_TO_INT(MSF_NUM_OF_STAGES)-1)
			IF IS_SCREEN_FADED_IN()
				DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME)
				BDOSKIP = TRUE
			ENDIF
		ENDIF
#ENDIF
	ENDIF
ENDPROC

BOOL	bTrackingSubDamage
BOOL 	bTrackingTruckDamage

PROC MISSION_CHECKS()
//------------------DEATH CHECKS-------------------------------

	FOR I = 0 TO ENUM_TO_INT(MPF_NUM_OF_PEDS) -1
		IF DOES_ENTITY_EXIST(PEDS[I].ID)
			IF PEDS[I].ID != PEDS[MPF_FLOYD].ID
				if mission_stage = enum_to_int(msf_0_reached_sub)	
					UPDATE_AI_PED_BLIP(PEDS[I].ID,PEDS[I].BLIPSTRUCT)
				else
					if DOES_BLIP_EXIST(PEDS[I].BLIPSTRUCT.BlipID)
						REMOVE_BLIP(PEDS[I].BLIPSTRUCT.BlipID)
					endif
				endif
			ENDIF
			IF IS_PED_INJURED(PEDS[I].ID)
				IF PEDS[I].ID = PEDS[MPF_FLOYD].ID
					MISSION_FAILED(mff_floyd_dead)
				ENDIF
				SAFE_RELEASE_PED(PEDS[I].ID)
			ELSE
				IF MISSION_STAGE = ENUM_TO_INT(MSF_2_SAFETY)
				AND PEDS[I].ID = PEDS[MPF_FLOYD].ID
					IF GET_DISTANCE_BETWEEN_ENTITIES(peds[mpf_floyd].id, PLAYER_PED_ID()) > 300.0
						MISSION_FAILED(mff_left_floyd)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	FOR I = 0 TO ENUM_TO_INT(MVF_NUM_OF_VEH) -1
		IF DOES_ENTITY_EXIST(VEHS[I].ID)
			IF NOT IS_VEHICLE_DRIVEABLE(VEHS[I].ID)
				IF VEHS[I].ID = VEHS[MVF_SUB].ID
					MISSION_FAILED(MFF_DESTROYED_SUB)
				ELIF VEHS[I].ID = VEHS[MVF_TRUCK].ID
				or VEHS[I].ID = VEHS[mvf_trailer].ID
					MISSION_FAILED(MFF_DESTROYED_TRUCK)
				ELIF i = ENUM_TO_INT(mvf_bison)
					MISSION_FAILED(mff_floyd_car_dead)
				ENDIF 
				SAFE_RELEASE_VEHICLE(VEHS[I].ID)
			else
				if bFailForStuck
					// Sub/trailer in the water
					if VEHS[I].ID = VEHS[mvf_sub].ID
					or VEHS[I].ID = VEHS[mvf_trailer].ID
						if mission_stage = enum_to_int(MSF_2_SAFETY)
							if GET_ENTITY_SUBMERGED_LEVEL(vehs[i].id) > 0.5
								MISSION_FAILED(mff_lost_sub)
							endif
						endif
					endif
					
					if vehs[i].id = VEHS[MVF_TRUCK].ID
					or VEHS[I].ID = VEHS[mvf_trailer].ID
						if IS_VEHICLE_PERMANENTLY_STUCK(vehs[i].id)
							MISSION_FAILED(mff_stuck_truck)
							SAFE_RELEASE_VEHICLE(vehs[i].id)
						endif
					endif
					IF VEHS[I].ID = VEHS[MVF_SUB].ID
						if mission_stage = enum_to_int(msf_1_get_to_crane)
						and mission_substage < 3
							if IS_VEHICLE_PERMANENTLY_STUCK(vehs[i].id)
								MISSION_FAILED(mff_stuck_sub)
								SAFE_RELEASE_VEHICLE(vehs[i].id)
							endif
						endif
					endif
				endif
			ENDIF
		ENDIF
	ENDFOR
	
	
	if ISENTITYALIVE(vehs[mvf_sub].id)
	and ISENTITYALIVE(player_ped_id())
	//fail for leaving the sub 
		if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id()), get_entity_coords(vehs[mvf_sub].id)) > 750//1500
			MISSION_FAILED(mff_left_sub)
		elif GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id()), get_entity_coords(vehs[mvf_sub].id)) > 700//1400
			if not bDistWarn
			and IS_SAFE_TO_DISPLAY_GODTEXT()
				PRINT_NOW("DKP1_ABSUB",DEFAULT_GOD_TEXT_TIME,1)
				bDistWarn = true
			endif			
		else
			bDistWarn = false
		endif

	endif	
	
//DIALGOUE	
	if ISENTITYALIVE(player_ped_id())
		AUDIO_manage_dialogue()
	endif
	
	if mission_stage = enum_to_int(msf_0_reached_sub)
	and mission_substage > STAGE_ENTRY	
		Manage_AI()
	endif	
	
//STATS
	//DAMAGE TO SUB
	IF MISSION_STAGE = ENUM_TO_INT(MSF_0_REACHED_SUB)
	OR MISSION_STAGE = ENUM_TO_INT(msf_1_get_to_crane)
		IF ISENTITYALIVE(VEHS[MVF_SUB].ID)
			IF NOT bTrackingSubDamage
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(VEHS[MVF_SUB].ID, DH1P_SUB_DAMAGE)
				CPRINTLN(DEBUG_MISSION_STATS, "<DOCKS_PREP_1> Passed, boarded ship")
				bTrackingSubDamage = TRUE
			ENDIF
		ELSE
			IF bTrackingSubDamage
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(null, DH1P_SUB_DAMAGE)
				bTrackingSubDamage = FALSE
			ENDIF
		ENDIF
	//DAMAGE TO TRUCK
	ELIF MISSION_STAGE = ENUM_TO_INT(MSF_2_SAFETY)
		IF ISENTITYALIVE(VEHS[MVF_TRUCK].ID)
			IF NOT bTrackingTruckDamage
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(VEHS[MVF_TRUCK].ID, DH1P_TRUCK_DAMAGE) 
				bTrackingTruckDamage = TRUE
			ENDIF
		ELSE
			IF bTrackingTruckDamage
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(null, DH1P_TRUCK_DAMAGE)
				bTrackingTruckDamage = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1240.401855,-2880.396240,8.250030>>, <<1240.384399,-2924.512695,40.819088>>, 28.875000)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1240.221558,-3035.469482,16.026249>>, <<1240.409058,-2924.159180,8.069090>>, 28.875000)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1240.464355,-3035.127197,13.006976>>, <<1240.616821,-3057.122070,16.971334>>, 27.687500)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1240.537476,-2967.821533,32.945580>>, <<1240.792236,-2943.656982,10.006589>>, 12.187500)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1261.524780,-3006.275391,29.216410>>, <<1237.787598,-3006.826172,12.877231>>, 8.187500)
	
		// Draw the boat map
		set_radar_as_interior_this_frame(get_hash_key("V_FakeBoatPO1SH2A"),1240.0,-2970.0,0)			
		
		ENTITY_INDEX entityToCheck
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			entityToCheck = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
		ELSE	
			entityToCheck = PLAYER_PED_ID()
		ENDIF
			
		IF NOT IS_ENTITY_IN_AIR(entityToCheck)
		AND NOT IS_ENTITY_IN_WATER(entityToCheck)
		AND (NOT DOES_ENTITY_EXIST(vehs[mvf_sub].id) OR NOT IS_ENTITY_TOUCHING_ENTITY(entityToCheck, vehs[mvf_sub].id))
		
			IF NOT bBoardedShip
				INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(DHP1_NO_BOARDING)
				bBoardedShip = TRUE
			ENDIF
			
			#IF IS_DEBUG_BUILD
				debug_bCurrentlyOnShip 	= TRUE
			#ENDIF
			
		ENDIF
	ENDIF
	
	IF NOT b_PlayedPoliceScannerLine
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", TRUE) 
			PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_DH_PREP_1_01", 0.0)
			b_PlayedPoliceScannerLine = TRUE
		ENDIF
	ENDIF
	
	//Fix for 1784052
	IF(IS_AIM_CAM_ACTIVE())
  		SET_GAMEPLAY_CAM_ENTITY_TO_LIMIT_FOCUS_OVER_BOUNDING_SPHERE_THIS_UPDATE(vehs[mvf_sub].id)
	ENDIF
	
	
#if IS_DEBUG_BUILD
	wAIstage 	 = enum_to_int(eAI_stage)
	wCalm_stage  = enum_to_int(eCalm_stage)	
	wAlarm_stage = enum_to_int(eALARM)
#endif

ENDPROC
PROC MISSION_SETUP()

	IF IS_REPLAY_IN_PROGRESS()	
	or IS_REPEAT_PLAY_ACTIVE()
		if IS_REPLAY_IN_PROGRESS()
			ISKIPTOSTAGE = GET_REPLAY_MID_MISSION_STAGE()
			IF g_bShitskipAccepted 
				ISKIPTOSTAGE++			
			ENDIF
			if iSkipToStage >= 3
				iSkipToStage = 3
			endif
		elif IS_REPEAT_PLAY_ACTIVE()
			iSkipToStage = 0
		endif		
		// New replay streaming 
		IF IS_REPLAY_IN_PROGRESS()
			VECTOR vSkipToCoord
			FLOAT fSkipToHeading
			GET_SKIP_STAGE_COORD_AND_HEADING(int_to_enum(MSF_MISSION_STAGE_FLAGS,iSkipToStage), vSkipToCoord, fSkipToHeading)
			START_REPLAY_SETUP(vSkipToCoord, fSkipToHeading)
			CPRINTLN(debug_mission, "[DHP1] MISSION_SETUP(): Started replay warp")
		ENDIF
		SETTIMERA(0)
		
		BDOSKIP = TRUE
	ELSE
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0,"STAGE 0: REACH SUB")
		MISSION_STAGE = ENUM_TO_INT(MSF_0_REACHED_SUB)					
		LOAD_ASSET_STAGE(MSF_0_REACHED_SUB)
	ENDIF	
	MISSION_SUBSTAGE = STAGE_ENTRY

	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
	ENDIF	
	SET_WEATHER_TYPE_PERSIST("EXTRASUNNY")
	SET_AUTO_GIVE_SCUBA_GEAR_WHEN_EXIT_VEHICLE(player_id(),FALSE)
	
	REMOVE_RELATIONSHIP_GROUP(REL_BUDDY)
	REMOVE_RELATIONSHIP_GROUP(rel_dock)
	ADD_RELATIONSHIP_GROUP("REL_BUDDY",REL_BUDDY)
	ADD_RELATIONSHIP_GROUP("rel_dock",rel_dock)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,rel_dock,RELGROUPHASH_PLAYER)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_PLAYER,REL_BUDDY)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,REL_BUDDY,RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_dock,rel_dock)
	
// GRAB THE sub
	IF NOT DOES_ENTITY_EXIST(VEHS[MVF_SUB].ID)		
		IF DOES_ENTITY_EXIST(G_STRIGGERSCENEASSETS.VEH[0])
			SET_ENTITY_AS_MISSION_ENTITY(G_STRIGGERSCENEASSETS.VEH[0], TRUE, TRUE)
			VEHS[MVF_SUB].ID = G_STRIGGERSCENEASSETS.VEH[0]
			SET_VEHICLE_AS_RESTRICTED(VEHS[MVF_SUB].ID,0)
			SET_VEHICLE_RADIO_ENABLED(vehs[mvf_sub].id, TRUE)
		ENDIF
	ENDIF
//PEDS
	IF NOT DOES_ENTITY_EXIST(PEDS[MPF_DOCKPHONE].ID)
		IF DOES_ENTITY_EXIST(G_STRIGGERSCENEASSETS.PED[0])
			SET_ENTITY_AS_MISSION_ENTITY(G_STRIGGERSCENEASSETS.PED[0],TRUE,TRUE)
			PEDS[MPF_DOCKPHONE].ID = G_STRIGGERSCENEASSETS.PED[0]
			set_ped_relationship_group_hash(peds[mpf_dockphone].id,rel_dock)
			SET_PED_CONFIG_FLAG(peds[mpf_dockphone].id,PCF_DisableExplosionReactions,true)
			SET_PED_VISUAL_FIELD_CENTER_ANGLE(peds[mpf_dockphone].id,45)
			SET_PED_VISUAL_FIELD_MAX_ANGLE(peds[mpf_dockphone].id,50)
			SET_PED_VISUAL_FIELD_MIN_ANGLE(peds[mpf_dockphone].id,-50)			
		endif
	ENDIF
//OBJECTS
	IF NOT DOES_ENTITY_EXIST(OBJECTS[MOF_DUMMY1].ID)
		IF DOES_ENTITY_EXIST(G_STRIGGERSCENEASSETS.OBJECT[0])
			SET_ENTITY_AS_MISSION_ENTITY(G_STRIGGERSCENEASSETS.OBJECT[0],TRUE,TRUE)
			OBJECTS[MOF_DUMMY1].ID = G_STRIGGERSCENEASSETS.OBJECT[0]
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(OBJECTS[MOF_DUMMY2].ID)
		IF DOES_ENTITY_EXIST(G_STRIGGERSCENEASSETS.OBJECT[1])
			SET_ENTITY_AS_MISSION_ENTITY(G_STRIGGERSCENEASSETS.OBJECT[1],TRUE,TRUE)
			OBJECTS[MOF_DUMMY2].ID = G_STRIGGERSCENEASSETS.OBJECT[1]
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(OBJECTS[MOF_BUTTON].ID)
		IF DOES_ENTITY_EXIST(G_STRIGGERSCENEASSETS.OBJECT[2])
			SET_ENTITY_AS_MISSION_ENTITY(G_STRIGGERSCENEASSETS.OBJECT[2],TRUE,TRUE)
			OBJECTS[MOF_BUTTON].ID = G_STRIGGERSCENEASSETS.OBJECT[2]
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(OBJECTS[mof_phone].ID)
		IF DOES_ENTITY_EXIST(G_STRIGGERSCENEASSETS.OBJECT[3])
			SET_ENTITY_AS_MISSION_ENTITY(G_STRIGGERSCENEASSETS.OBJECT[3],TRUE,TRUE)
			OBJECTS[mof_phone].ID = G_STRIGGERSCENEASSETS.OBJECT[3]
		ENDIF
	ENDIF
	//	// GRAB ROPE STUFF
//	
if g_sTriggerSceneAssets.flag
	println("[ROPES] attached")
	if DOES_ROPE_EXIST(G_STRIGGERSCENEASSETS.ROPE[0])
		ROPE_CHANGE_SCRIPT_OWNER(G_STRIGGERSCENEASSETS.ROPE[0],true,true)
		ROPES[0].ROPE = G_STRIGGERSCENEASSETS.ROPE[0]
		G_STRIGGERSCENEASSETS.ROPE[0] = null
	endif
	if DOES_ROPE_EXIST(G_STRIGGERSCENEASSETS.ROPE[1])
		ROPE_CHANGE_SCRIPT_OWNER(G_STRIGGERSCENEASSETS.ROPE[1],true,true)		
		ROPES[1].ROPE = G_STRIGGERSCENEASSETS.ROPE[1]
		G_STRIGGERSCENEASSETS.ROPE[1] = null
	endif
	if DOES_ROPE_EXIST(G_STRIGGERSCENEASSETS.ROPE[2])
		ROPE_CHANGE_SCRIPT_OWNER(G_STRIGGERSCENEASSETS.ROPE[2],true,true)
		ROPES[2].ROPE = G_STRIGGERSCENEASSETS.ROPE[2]
		G_STRIGGERSCENEASSETS.ROPE[2] = null
	endif
	if DOES_ROPE_EXIST(G_STRIGGERSCENEASSETS.ROPE[3])
		ROPE_CHANGE_SCRIPT_OWNER(G_STRIGGERSCENEASSETS.ROPE[3],true,true)
		ROPES[3].ROPE = G_STRIGGERSCENEASSETS.ROPE[3]
		G_STRIGGERSCENEASSETS.ROPE[3] = null
	endif
	bRopesCreated = true 
else
	println("[ROPES] set as not attached from trigger")
	bRopesCreated = false
endif	

	REQUEST_ADDITIONAL_TEXT("DOCKP1", MISSION_TEXT_SLOT)
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		WAIT(0)
	ENDWHILE
	
	if ISENTITYALIVE(player_ped_id())
		ADD_PED_FOR_DIALOGUE(convo_struct,0,player_ped_id(),"TREVOR")
	endif
	ALLOW_DIALOGUE_IN_WATER(TRUE)
		
	SET_MAX_WANTED_LEVEL(2)
	SET_WANTED_LEVEL_MULTIPLIER(0.5)
	
	INIT_BATTLEBUDDY_BEHAVIOUR_FOR_MISSION(SP_HEIST_DOCKS_PREP_1)
			
ENDPROC

INT iSwingingSound[2]
INT iSwingingSoundPlayed

PROC MANAGE_ROPE_SFX()

	INT iRopesSnapped
	BOOL bARopeWasJustSnapped
	REPEAT COUNT_OF(ropes) i
		if not IS_ROPE_ATTACHED_AT_BOTH_ENDS(ropes[i].rope)	
			IF NOT ropes[i].bSnapped
				bARopeWasJustSnapped = TRUE
			ENDIF
			iRopesSnapped++
		endif
	ENDREPEAT
	
	IF bARopeWasJustSnapped
	
	// ROPE SWING SOUND
	//-----------------------------------
		
		// both ropes just snapped on 1 side of the sub
		
		IF ((not IS_ROPE_ATTACHED_AT_BOTH_ENDS(ropes[0].rope) 
		and not IS_ROPE_ATTACHED_AT_BOTH_ENDS(ropes[1].rope) 
		AND (NOT ropes[0].bSnapped OR NOT ropes[1].bSnapped))
		
		// both ropes just snapped on the other side of the sub
		or (not IS_ROPE_ATTACHED_AT_BOTH_ENDS(ropes[2].rope)
		and not IS_ROPE_ATTACHED_AT_BOTH_ENDS(ropes[3].rope) 
		AND (NOT ropes[2].bSnapped OR NOT ropes[3].bSnapped))
		
		
		// Just snapped the 3rd rope so it will need to swing again.
		OR iRopesSnapped = 3)
		AND iRopesSnapped < 4
		
			IF iSwingingSoundPlayed > 0
				IF iSwingingSound[iSwingingSoundPlayed-1] != -1
					STOP_SOUND(iSwingingSound[iSwingingSoundPlayed-1])
				ENDIF
			ENDIF	
			
			IF iSwingingSound[iSwingingSoundPlayed] != -1
				PLAY_SOUND_FROM_COORD(iSwingingSound[iSwingingSoundPlayed], "DOCKS_HEIST_PREP_1_SUB_SWING", <<1260.8959, -3006.5557, 23.4213>>)
				CPRINTLN(DEBUG_MIKE, "PLAYED SOUND: DOCKS_HEIST_PREP_1_SUB_SWING")
				iSwingingSoundPlayed++
			ENDIF

	// ROPE TILT SOUND
	//-----------------------------------
		ELIF iRopesSnapped <= 2
			
			// one rope just snapped OR 2 ropes snapped
			//PLAY_SOUND_FRONTEND(-1, "DOCKS_HEIST_PREP_1_SUB_TILT", "")
			PLAY_SOUND_FROM_COORD(-1, "DOCKS_HEIST_PREP_1_SUB_TILT", <<1260.8959, -3006.5557, 23.4213>>)
			CPRINTLN(DEBUG_MIKE, "PLAYED SOUND: DOCKS_HEIST_PREP_1_SUB_TILT")
		
		ENDIF
		
	// ROPE SNAP SOUND
	//-----------------------------------
		REPEAT COUNT_OF(ropes) i
			if not IS_ROPE_ATTACHED_AT_BOTH_ENDS(ropes[i].rope)
				IF NOT ropes[i].bSnapped
					//PLAY_SOUND_FRONTEND(-1, "DOCKS_HEIST_PREP_1_CABLE_SNAP", "")
					PLAY_SOUND_FROM_COORD(-1, "DOCKS_HEIST_PREP_1_CABLE_SNAP", <<1260.8959, -3006.5557, 23.4213>>)
					CPRINTLN(DEBUG_MIKE, "PLAYED SOUND: DOCKS_HEIST_PREP_1_CABLE_SNAP for rope[", i, "]")
				ENDIF
				ropes[i].bSnapped = TRUE
			endif
		ENDREPEAT
		
		// Final rope snapped, stop the swining sound
		IF iRopesSnapped >= 4
			IF iSwingingSound[0] != -1
				STOP_SOUND(iSwingingSound[0])
			ENDIF
			IF iSwingingSound[1] != -1
				STOP_SOUND(iSwingingSound[1])
			ENDIF
		ENDIF
	
	ENDIF
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		mission stages
// -----------------------------------------------------------------------------------------------------------
proc st_0_reached_sub()

	switch mission_substage
		case stage_entry
			IF IS_SCREEN_FADED_OUT()			
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			if isentityalive(vehs[mvf_sub].id)
				if not does_blip_exist(vehs[mvf_sub].blip)
					vehs[mvf_sub].blip = create_blip_for_vehicle(vehs[mvf_sub].id)
				endif			
				
				subCGOffset = GET_CGOFFSET(vehs[mvf_sub].id)				
			endif
			if does_entity_exist(objects[mof_button].id)
			and not does_blip_exist(blip_objective)
				blip_objective = create_blip_for_object(objects[mof_button].id)				
			endif
			SET_BLIP_NAME_FROM_TEXT_FILE(blip_objective,"SUBBTN_LABEL")
			
			if IS_PED_IN_ANY_VEHICLE(player_ped_id())
				TRIGGER_MUSIC_EVENT("DHP1_VEHICLE_ARRIVE")
			else
				TRIGGER_MUSIC_EVENT("DHP1_START")
			endif
			if isentityalive(peds[mpf_dockphone].id)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_dockphone].id,true)
				SET_PED_PATH_CAN_USE_CLIMBOVERS(peds[mpf_dockphone].id,false)
			endif	
			Load_Asset_AnimDict(sAssetData,AnimDictMission)
			Load_Asset_AnimDict(sAssetData,"MISSHEISTDOCKSPREP1HOLD_CELLPHONE")
			
			Load_Asset_Audiobank(sAssetData, "PORT_OF_LS_PREP_1")

			print_now("dkp1_sub",default_god_text_time,1)
			eAI_stage 			= AI_NORM_TASK
			eCalm_stage 		= CALM_WARN1
			eALARM		 		= ALARM_NONE
			iStartTasktimer 	= get_game_timer()
			bINIT_workerTasks 	= false
			bHelpDisplayed		= false
			bWaterSlashedSound	= false
			IF iSwingingSound[0] = -1		iSwingingSound[0] = GET_SOUND_ID()		ENDIF
			IF iSwingingSound[1] = -1		iSwingingSound[1] = GET_SOUND_ID()		ENDIF
			mission_substage++
		break
		case 1		
			if bropescreated
				
				if not IS_ROPE_ATTACHED_AT_BOTH_ENDS(ropes[0].rope)	
				and not IS_ROPE_ATTACHED_AT_BOTH_ENDS(ropes[1].rope)
				and not IS_ROPE_ATTACHED_AT_BOTH_ENDS(ropes[2].rope)
				and not IS_ROPE_ATTACHED_AT_BOTH_ENDS(ropes[3].rope)
								
					ROPE_CONVERT_TO_SIMPLE( objects[mof_dummy1].id )
					ROPE_CONVERT_TO_SIMPLE( objects[mof_dummy2].id )
		
					if ISENTITYALIVE(vehs[mvf_sub].id)
						SET_CGOFFSET( vehs[mvf_sub].id, subCGOffset )
					endif
					if does_blip_exist(blip_objective)
						remove_blip(blip_objective)
					endif
					load_asset_stage(msf_1_get_to_crane)
					LOAD_ASSET_MODEL(sAssetData,S_M_M_SECURITY_01)
					Load_Asset_AnimDict(sAssetData,AnimDictReaction)
					TRIGGER_MUSIC_EVENT("DHP1_RELEASED")	
					CLEAR_HELP()
					istagedelay = get_game_timer()
					mission_substage = 4
				endif
				
				MANAGE_ROPE_SFX()

				if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),<<1249.10571, -3007.90796, 10.84327>>,<<1249.96155, -3007.83740, 8.31909>>,1.68)	
				and not IS_PED_IN_ANY_VEHICLE(player_ped_id(),true)
				AND NOT IS_PHONE_ONSCREEN(FALSE)
					if IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_CONTEXT)	
					
						cameraindex = create_camera_with_params(camtype_scripted, <<1249.929443,-3010.267334,9.590105>>, <<5.351202,-1.573690,8.124990>>, 21.333569, TRUE)
						SET_CAM_PARAMS(cameraIndex, <<1249.929443,-3010.267334,9.590105>>,<<4.448765,-1.573700,12.680055>>,21.333569, 3500, GRAPH_TYPE_LINEAR)
						SHAKE_CAM(cameraIndex, "hand_shake", 0.3)
						prep_start_cutscene(false,<<1249.78662, -3008.01587, 8.52751>>)
						
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						
						syncbutton = CREATE_SYNCHRONIZED_SCENE(<<1250.01,-3007.9,9.32>>,<<0,0,90>>)
						if GET_PED_STEALTH_MOVEMENT(player_ped_id())
							TASK_SYNCHRONIZED_SCENE(player_ped_id(),syncbutton,AnimDictMission,"ig_1_stealth_button",INSTANT_BLEND_IN,walk_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_ARM_IK)
						else
							TASK_SYNCHRONIZED_SCENE(player_ped_id(),syncbutton,AnimDictMission,"ig_1_button",INSTANT_BLEND_IN,walk_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT, RBF_NONE, INSTANT_BLEND_IN, AIK_DISABLE_ARM_IK)
						endif
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						CLEAR_HELP()
						VEHICLE_INDEX vehblock
						vehblock = GET_CLOSEST_VEHICLE(<<1249.78662, -3008.01587, 8.52751>>,5,DUMMY_MODEL_FOR_SCRIPT,VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
						if ISENTITYALIVE(vehblock)
							SET_ENTITY_COORDS(vehblock,<<1250.3335, -3010.2190, 8.3191>>)
							SET_ENTITY_HEADING(vehblock,85.8086)
							SET_VEHICLE_ON_GROUND_PROPERLY(vehblock)
						endif
						
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						bcueCam = true
						mission_substage++
					ELSE
						IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DKP1_BTN")
							PRINT_HELP_FOREVER("DKP1_BTN")
						ENDIF
					endif
				else
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DKP1_BTN")
						CLEAR_HELP()
					ENDIF
				endif
			endif
		break
		case 2
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(syncbutton)
			OR GET_SYNCHRONIZED_SCENE_PHASE(syncbutton) = 1.0

				REPLAY_STOP_EVENT()
				
				PLAY_SOUND_FROM_COORD(-1, "SUB_RELEASE", <<1260.8959, -3006.5557, 23.4213>>, "DOCKS_HEIST_PREP_1_SOUNDSET")
				
				SET_CAM_PARAMS(cameraindex,<<1249,-3010.3,10.7>>, <<37.7,0,-61.5>>,43.4)
				SET_CAM_PARAMS(cameraIndex,<<1249.0,-3010.3,10.7>>,<<-20.1,0,-48.2>>,43.4,1700,GRAPH_TYPE_ACCEL,GRAPH_TYPE_ACCEL)
				SET_CAM_SHAKE_AMPLITUDE(cameraIndex, 0.1)
				
				DETACH_ROPE_FROM_ENTITY( ropes[0].rope, vehs[mvf_sub].id )
				DETACH_ROPE_FROM_ENTITY( ropes[1].rope, vehs[mvf_sub].id )
				DETACH_ROPE_FROM_ENTITY( ropes[2].rope, vehs[mvf_sub].id )
				DETACH_ROPE_FROM_ENTITY( ropes[3].rope, vehs[mvf_sub].id )
				
				ROPE_CONVERT_TO_SIMPLE( objects[mof_dummy1].id )
				ROPE_CONVERT_TO_SIMPLE( objects[mof_dummy2].id )
				
				if ISENTITYALIVE(vehs[mvf_sub].id)							
					SET_CGOFFSET( vehs[mvf_sub].id, subCGOffset )
					ACTIVATE_PHYSICS( vehs[mvf_sub].id )
				endif
				istagedelay = get_game_timer()
				TRIGGER_MUSIC_EVENT("DHP1_RELEASED")	
				CLEAR_HELP()
			
				bcueCam = true
				mission_substage++
			elif IS_SYNCHRONIZED_SCENE_RUNNING(syncbutton)
			and GET_SYNCHRONIZED_SCENE_PHASE(syncbutton) >= 0.7
				if GET_SCRIPT_TASK_STATUS(player_ped_id(),SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD) <> PERFORMING_TASK	
					TASK_FOLLOW_NAV_MESH_TO_COORD(player_peD_id(),<<1252.5935, -3008.8831, 8.3191>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
				endif
			ENDIF
		break
		case 3
			if get_game_timer() - istagedelay > 1500	
			
				if bcueCam
					SET_CAM_PARAMS(cameraIndex,<<1261.7,-3013.3,21.7>>,<<-62.7,0,29.5>>,40.4,0,GRAPH_TYPE_DECEL,GRAPH_TYPE_DECEL)
					SET_CAM_PARAMS(cameraIndex,<<1259.9,-3014.1,21.7>>,<<-67.7,0,16.5>>,40.4,4000,GRAPH_TYPE_DECEL,GRAPH_TYPE_DECEL)
					bcueCam = false
				endif				
				
				if get_game_timer() - istagedelay > 5000
				or IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
					if does_blip_exist(blip_objective)
						remove_blip(blip_objective)
					endif
					load_asset_stage(msf_1_get_to_crane)
					LOAD_ASSET_MODEL(sAssetData,S_M_M_SECURITY_01)
					Load_Asset_AnimDict(sAssetData,AnimDictReaction)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					CLEAR_PED_TASKS(player_ped_id())
					prep_stop_cutscene(true)
					istagedelay = get_game_timer()					
					mission_substage++					
				endif
			endif
		break
		case 4
			if not IS_PLAYER_PED_SWITCH_IN_PROGRESS()
				if GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
					if not IS_BATTLEBUDDY_AVAILABLE(GET_BATTLEBUDDY_PED(CHAR_MICHAEL),false)
					and not IS_BATTLEBUDDY_AVAILABLE(GET_BATTLEBUDDY_PED(CHAR_FRANKLIN),false)
						SET_BATTLEBUDDY_BEHAVIOUR_REQUEST(BBF_RejectAllBattleBuddies) 					
						mission_substage++					
					elif IS_BATTLEBUDDY_AVAILABLE(GET_BATTLEBUDDY_PED(CHAR_MICHAEL),false)
					and not IS_BATTLEBUDDY_AVAILABLE(GET_BATTLEBUDDY_PED(CHAR_FRANKLIN),false)
						ADD_PED_FOR_DIALOGUE(convo_struct,0,player_ped_id(),"TREVOR")
						ADD_PED_FOR_DIALOGUE(convo_struct,1,GET_BATTLEBUDDY_PED(CHAR_MICHAEL),"MICHAEL")
						if CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(convo_struct,"DHP1AUD","DHP1_BUD","DHP1_BUD_1","DHP1_BUD","DHP1_BUD_2",CONV_PRIORITY_MEDIUM)
							SET_BATTLEBUDDY_BEHAVIOUR_REQUEST(BBF_RejectAllBattleBuddies) 					
							mission_substage++
						endif					
					elif not IS_BATTLEBUDDY_AVAILABLE(GET_BATTLEBUDDY_PED(CHAR_MICHAEL),false)
					and  IS_BATTLEBUDDY_AVAILABLE(GET_BATTLEBUDDY_PED(CHAR_FRANKLIN),false)
						ADD_PED_FOR_DIALOGUE(convo_struct,0,player_ped_id(),"TREVOR")
						ADD_PED_FOR_DIALOGUE(convo_struct,2,GET_BATTLEBUDDY_PED(CHAR_FRANKLIN),"FRANKLIN")
						if CREATE_MULTIPART_CONVERSATION_WITH_2_LINES(convo_struct,"DHP1AUD","DHP1_BUD","DHP1_BUD_1","DHP1_BUD","DHP1_BUD_3",CONV_PRIORITY_MEDIUM)
							SET_BATTLEBUDDY_BEHAVIOUR_REQUEST(BBF_RejectAllBattleBuddies) 					
							mission_substage++
						endif					
					else // both there		
						ADD_PED_FOR_DIALOGUE(convo_struct,0,player_ped_id(),"TREVOR")
						ADD_PED_FOR_DIALOGUE(convo_struct,1,GET_BATTLEBUDDY_PED(CHAR_MICHAEL),"MICHAEL")
						ADD_PED_FOR_DIALOGUE(convo_struct,2,GET_BATTLEBUDDY_PED(CHAR_FRANKLIN),"FRANKLIN")
						if CREATE_CONVERSATION(convo_struct,"DHP1AUD","DHP1_BUD",CONV_PRIORITY_MEDIUM)
							SET_BATTLEBUDDY_BEHAVIOUR_REQUEST(BBF_RejectAllBattleBuddies) 					
							mission_substage++
						endif
					endif
				else
					if IS_SAFE_TO_DISPLAY_GODTEXT()
						Print_now("DKP1_SWTCH",DEFAULT_GOD_TEXT_TIME,1)
					endif
				endif
			endif
		break
		case 5
			VECTOR vSubCoord
			vSubCoord = GET_ENTITY_COORDS(vehs[mvf_sub].id)
			if GET_ENTITY_SUBMERGED_LEVEL(vehs[mvf_sub].id) > 0.0
			OR vSubCoord.z < -0.5
				DELETE_CHILD_ROPE(ropes[0].rope)
				DELETE_CHILD_ROPE(ropes[1].rope)
				DELETE_CHILD_ROPE(ropes[2].rope)
				DELETE_CHILD_ROPE(ropes[3].rope)
				IF iSwingingSound[0] != -1		STOP_SOUND(iSwingingSound[0])		ENDIF
				IF iSwingingSound[1] != -1		STOP_SOUND(iSwingingSound[1])		ENDIF
			endif
		
			if is_ped_in_vehicle(player_ped_id(),vehs[mvf_sub].id)
				//MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_DOCKS_PREP_1)
				SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<1218.2369, -3005.1282, 4.8658>>, 359.5065)	
				
				if GET_PLAYER_WANTED_LEVEL(player_id()) > 0					
					//security 1
					peds[mpf_sec1].id = create_ped(PEDTYPE_MISSION,S_M_M_SECURITY_01,<<1229.55750, -3002.88184, 8.31909>>,-30.33)
					GIVE_WEAPON_TO_PED(peds[mpf_sec1].id,WEAPONTYPE_PISTOL,INFINITE_AMMO)
					SET_PED_CONFIG_FLAG(peds[mpf_sec1].id, PCF_DontBlip, TRUE)
					SET_PED_AS_COP(peds[mpf_sec1].id)				
					CLEAR_SEQUENCE_TASK(seq)					
					OPEN_SEQUENCE_TASK(seq)
						TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<1252.87610, -3014.03540, 8.31909>>,PEDMOVE_RUN)
						TASK_LOOK_AT_ENTITY(null,player_ped_id(),-1,SLF_WHILE_NOT_IN_FOV)
						TASK_COMBAT_PED(null,PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(peds[mpf_sec1].id,seq)
					CLEAR_SEQUENCE_TASK(seq)
					//security 2
					peds[mpf_sec2].id = create_ped(PEDTYPE_MISSION,S_M_M_SECURITY_01,<<1229.55750, -3002.88184, 8.31909>>,-30.33)
					SET_PED_CONFIG_FLAG(peds[mpf_sec2].id, PCF_DontBlip, TRUE)
					GIVE_WEAPON_TO_PED(peds[mpf_sec2].id,WEAPONTYPE_PISTOL,INFINITE_AMMO)
					SET_PED_AS_COP(peds[mpf_sec2].id)				
					CLEAR_SEQUENCE_TASK(seq)					
					OPEN_SEQUENCE_TASK(seq)
						TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<1252.05591, -3006.94043, 8.31909>>,PEDMOVE_RUN)
						TASK_LOOK_AT_ENTITY(null,player_ped_id(),-1,SLF_WHILE_NOT_IN_FOV)
						TASK_COMBAT_PED(null,PLAYER_PED_ID())
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(peds[mpf_sec2].id,seq)
					CLEAR_SEQUENCE_TASK(seq)
					Unload_Asset_Model(sAssetData,S_M_M_SECURITY_01)
				else
					peds[mpf_dock2].id = create_ped(PEDTYPE_MISSION,S_M_M_DOCKWORK_01,<<1228.92932, -3015.00806, 8.31909>>)			
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_dock2].id,true)
					CLEAR_SEQUENCE_TASK(seq)					
					OPEN_SEQUENCE_TASK(seq)
						TASK_LOOK_AT_ENTITY(null,player_peD_id(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)
						TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<1252.87610, -3014.03540, 8.31909>>,PEDMOVE_RUN)
						TASK_PLAY_ANIM(null,AnimDictReaction,"react_big_variations_g",walk_BLEND_IN,WALK_BLEND_OUT)
						TASK_PLAY_ANIM(null,AnimDictReaction,"react_big_variations_i",walk_BLEND_IN,WALK_BLEND_OUT)
						TASK_SMART_FLEE_COORD(null,<<1252.87610, -3014.03540, 8.31909>>,300,-1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(peds[mpf_dock2].id,seq)
					CLEAR_SEQUENCE_TASK(seq)
					Unload_Asset_Model(sAssetData,S_M_M_SECURITY_01)
				endif
								
				//new dock worker
				if ISENTITYALIVE(peds[mpf_dockphone].id)
					CLEAR_PED_TASKS(peds[mpf_dockphone].id)
					set_entity_coords(peds[mpf_dockphone].id,<<1229.55750, -3002.88184, 8.31909>>)
				else				
					peds[mpf_dockphone].id = create_ped(PEDTYPE_MISSION,S_M_M_DOCKWORK_01,<<1229.55750, -3002.88184, 8.31909>>,-99)
				endif
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_dockphone].id,true)
				STOP_PED_SPEAKING(peds[mpf_dockphone].id, TRUE)
				
				CLEAR_SEQUENCE_TASK(seq)					
				OPEN_SEQUENCE_TASK(seq)
					TASK_LOOK_AT_ENTITY(null,player_peD_id(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT)
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<1252.994,-3004.134,9.319>>,PEDMOVE_RUN)
					TASK_PLAY_ANIM(null,AnimDictReaction,"react_big_variations_f",NORMAL_BLEND_IN,WALK_BLEND_OUT)
					TASK_PLAY_ANIM(null,AnimDictReaction,"react_big_variations_g",walk_BLEND_IN,WALK_BLEND_OUT)
					TASK_PLAY_ANIM(null,AnimDictReaction,"react_big_variations_h",walk_BLEND_IN,WALK_BLEND_OUT)
					TASK_PLAY_ANIM(null,AnimDictReaction,"react_big_variations_i",walk_BLEND_IN,WALK_BLEND_OUT)
					TASK_PLAY_ANIM(null,AnimDictReaction,"react_big_variations_f",walk_BLEND_IN,WALK_BLEND_OUT)
					TASK_SMART_FLEE_COORD(null,<<1252.994,-3004.134,9.319>>,300,-1)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(peds[mpf_dockphone].id,seq)
				CLEAR_SEQUENCE_TASK(seq)
				
				dialoguetimer = get_game_timer()
				dialogue_stage 		= DHP1_TSUB
				dialogue_substage 	= DIA_CONDITIONS
				CLEAR_HELP()
				TRIGGER_MUSIC_EVENT("DHP1_SUB")
				if does_blip_exist(vehs[mvf_sub].blip)
					remove_blip(vehs[mvf_sub].blip)
				endif
				SET_OBJECT_AS_NO_LONGER_NEEDED(objects[mof_phone].id)
				SET_OBJECT_AS_NO_LONGER_NEEDED(objects[mof_dummy1].id)
				SET_OBJECT_AS_NO_LONGER_NEEDED(objects[mof_dummy2].id)	
				unload_asset_model(sAssetData,P_AMB_PHONE_01)
				unload_asset_model(sAssetData,S_M_M_DOCKWORK_01)
				unload_asset_model(sAssetData,prop_ld_test_01)
				unload_asset_model(sAssetData,prop_sub_release)
				Unload_Asset_Anim_Dict(sAssetData,AnimDictMission)
//				Unload_Asset_Waypoint(sAssetData,"docksprep1")
				REMOVE_WAYPOINT_RECORDING("docksprep1")
				
				REPLAY_RECORD_BACK_FOR_TIME(15.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				
				mission_set_stage(msf_1_get_to_crane)				
				mission_substage = STAGE_ENTRY
			else
				if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id()),GET_ENTITY_COORDS(vehs[mvf_sub].id)) < 5
				and not bHelpDisplayed
					print_help("DKP1_CLIMB")
					bHelpDisplayed = true
				endif
			endif			
		break
	endswitch
	
	IF NOT bWaterSlashedSound
	AND ISENTITYALIVE(vehs[mvf_sub].id)
	AND IS_ENTITY_IN_WATER(vehs[mvf_sub].id)
		PLAY_SOUND_FROM_ENTITY(-1, "SUB_SPLASH", vehs[mvf_sub].id, "DOCKS_HEIST_PREP_1_SOUNDSET")
		bWaterSlashedSound = TRUE
	ENDIF
	
//--------------------------------------------------------------- attach ropes ---------------------------------------------------------------
	if not bropescreated
	and does_entity_exist(objects[mof_dummy1].id)
	and does_entity_exist(objects[mof_dummy2].id)
	and ISENTITYALIVE(vehs[mvf_sub].id)
		if 	does_entity_have_physics(objects[mof_dummy1].id)
		and does_entity_have_physics(objects[mof_dummy2].id)		
	
		SET_CG_AT_BOUNDCENTER( vehs[mvf_sub].id )		
		
		//attach rope 1
			ropes[0].rope = add_rope(<<1260.69153, -3008.28589, 23.73365>>,<<0,0,0>>,wfropelength,physics_rope_default,wfropelength,0.5,0.5
									,false,false,false,10,true)
			attach_entities_to_rope(ropes[0].rope,objects[mof_dummy1].id,vehs[mvf_sub].id,
									 get_offset_from_entity_in_world_coords(objects[mof_dummy1].id,<<0,0,0>>)
									,get_offset_from_entity_in_world_coords(vehs[mvf_sub].id,<<1.8,1.02,1.11>>),wfropelength,0,0)
		//attach rope 2
			ropes[1].rope = add_rope(<<1260.69153, -3008.28589, 23.73365>>,<<0,0,0>>,wfropelength,physics_rope_default,wfropelength,0.5,0.5
									,false,false,false,10,true)
			attach_entities_to_rope(ropes[1].rope,objects[mof_dummy1].id,vehs[mvf_sub].id,
									 get_offset_from_entity_in_world_coords(objects[mof_dummy1].id,<<0,0,0>>)
									,get_offset_from_entity_in_world_coords(vehs[mvf_sub].id,<<-1.8,1.02,1.11>>),wfropelength,0,0)			
			//attach rope 1
			ropes[2].rope = add_rope(<<1260.69153, -3008.28589, 23.73365>>,<<0,0,0>>,wfropelength,physics_rope_default,wfropelength,0.5,0.5
									,false,false,false,10,true)
			attach_entities_to_rope(ropes[2].rope,objects[mof_dummy2].id,vehs[mvf_sub].id,
									 get_offset_from_entity_in_world_coords(objects[mof_dummy2].id,<<0,0,0>>)
									,get_offset_from_entity_in_world_coords(vehs[mvf_sub].id,<<1.8,-3.07,1.11>>),wfropelength,0,0)
		//attach rope 2
			ropes[3].rope = add_rope(<<1260.75208, -3004.99414, 22.68315>>,<<0,0,0>>,wfropelength,physics_rope_default,wfropelength,0.5,0.5
									,false,false,false,10,true)
			attach_entities_to_rope(ropes[3].rope,objects[mof_dummy2].id,vehs[mvf_sub].id,
									 get_offset_from_entity_in_world_coords(objects[mof_dummy2].id,<<0,0,0>>)
									,get_offset_from_entity_in_world_coords(vehs[mvf_sub].id,<<-1.8,-3.07,1.11>>),wfropelength,0,0)				
			bropescreated = true
			bactivatePhysics = false
			ACTIVATE_PHYSICS(vehs[mvf_sub].id)
			APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(vehs[mvf_sub].id, APPLY_TYPE_IMPULSE, <<0.0, 0.0, 0.01>>, 0, FALSE, TRUE)

		endif
	else		
		if isentityalive(vehs[mvf_sub].id)
			if not bactivatePhysics
				ACTIVATE_PHYSICS(vehs[mvf_sub].id)
				bactivatePhysics = true
			endif
			freeze_entity_position(vehs[mvf_sub].id,false)
			APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(vehs[mvf_sub].id, APPLY_TYPE_IMPULSE, <<0.0, 0.0, 0.01>>, 0, FALSE, TRUE)
		endif
	endif
//--------------------------------------------------------------- phone ---------------------------------------------------------------	
	if not bPhoneAdded
		if DOES_ENTITY_EXIST(objects[mof_phone].id)
			IF DOES_ENTITY_HAVE_PHYSICS(objects[mof_phone].id)
				ATTACH_ENTITY_TO_ENTITY(objects[mof_phone].id,peds[mpf_dockphone].id
						,GET_PED_BONE_INDEX(peds[mpf_dockphone].id,BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>)
				bPhoneAdded = true	
			endif
		endif
	endif
	if does_entity_exist(objects[mof_phone].id)
	AND ((isentityalive(peds[mpf_dockphone].id) AND is_ped_fleeing(peds[mpf_dockphone].id))
	OR NOT isentityalive(peds[mpf_dockphone].id))
		if isentityalive(peds[mpf_dockphone].id)
		AND IS_ENTITY_PLAYING_ANIM(peds[mpf_dockphone].id,animDictPhone,"cellphone_call_listen_c")
			stop_anim_task(peds[mpf_dockphone].id,animDictPhone,"cellphone_call_listen_c")
		endif
		if is_entity_attached_to_any_ped(objects[mof_phone].id)
			detach_entity(objects[mof_phone].id)
		endif
	endif
	
//----------------only trev can get in sub-----------------------
	if GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
		if ISENTITYALIVE(vehs[mvf_sub].id)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_sub].id,true)
		endif
	else
		if ISENTITYALIVE(vehs[mvf_sub].id)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_sub].id,false)
		endif
	endif

endproc
proc st_1_get_to_crane()
	switch mission_substage
		case stage_entry
			
			Unload_Asset_Audio_Bank(sAssetData, "PORT_OF_LS_PREP_1")
		
			IF IS_SCREEN_FADED_OUT()			
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			is_player_at_location_in_vehicle(slocatesdata,<<324.76532, -2974.49463,-1.0>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,true,vehs[mvf_sub].id,"dkp1_tk1","","DKP1_SUBBK",true)
			
			if has_model_loaded(packer)
			and has_model_loaded(armytrailer)	
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"STAGE 1: Get to crane")
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(DH1P_SUB_TIME)
				ADD_PED_FOR_DIALOGUE(convo_struct,1,null,"FLOYD")								
				SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS)
				vehs[mvf_truck].id = create_vehicle(packer,<<298.2343, -2942.7698, 5.0003>>, 0)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_truck].id,false)
				SET_VEHICLE_HAS_STRONG_AXLES(vehs[mvf_truck].id,true)
				set_vehicle_colour_combination(vehs[mvf_truck].id,0)
				SET_VEHICLE_TYRES_CAN_BURST(vehs[mvf_truck].id,false)
				vehs[mvf_trailer].id = create_vehicle(armytrailer,<<298.27, -2951.27, 7.86>>, 0)
				attach_vehicle_to_trailer(vehs[mvf_truck].id,vehs[mvf_trailer].id,0.5)
				SET_TRAILER_LEGS_RAISED(vehs[mvf_trailer].id)
				SET_VEHICLE_TYRES_CAN_BURST(vehs[mvf_trailer].id,false)
				SET_VEHICLE_HAS_STRONG_AXLES(vehs[mvf_trailer].id,true)
				bRopesCreated = false
				ADD_SCENARIO_BLOCKING_AREA(<<289.08466, -2980.38159, 9.24156>>,<<320.42603, -2931.59985, 4.0>>, FALSE, TRUE, TRUE, TRUE)
				bFailForStuck = TRUE
				
				SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", FALSE) 
				
				mission_substage++
			endif
		break
		case 1
			if init_crane()
				Unload_Asset_Anim_Dict(sAssetData,AnimDictReaction)	
				Unload_Asset_Anim_Dict(sAssetData,AnimDictPhone)	
				iHelpTimer = -1
				iSubHelp = 0
			 	mission_substage++
			endif
		break		
		case 2
			
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehs[mvf_sub].id)
				SWITCH iSubHelp
					CASE 0
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							IF iHelpTimer = -1
								iHelpTimer = GET_GAME_TIMER() + 5000
							ENDIF
							IF GET_GAME_TIMER() > iHelpTimer
								PRINT_HELP("DKP1_SUBTURN")
								iSubHelp++
							ENDIF
						ENDIF
					BREAK
					CASE 1
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							PRINT_HELP("SUB_HELP")
							iSubHelp++
						ENDIF
					BREAK
				ENDSWITCH
			ELSE
				IF iSubHelp = 0
					iHelpTimer = -1
				ENDIF
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DKP1_SUBTURN")
					OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SUB_HELP")
						CLEAR_HELP()
					ENDIF
				ENDIF
			ENDIF
			
			if IS_VEHICLE_STUCK_TIMER_UP(vehs[mvf_sub].id,VEH_STUCK_JAMMED,BEACHED_TIME)
				MISSION_FAILED(mff_destroyed_sub)
			endif
						
			if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<324.76532, -2974.49463,-1.5>>) < 150
				if LOAD_STREAM("DOCKS_HEIST_PREP_1_LIFT_SUB")
					println("audio stream ready")
					bAUDIOStreamLoaded = true
				endif
				
				SAFE_RELEASE_OBJECT(objects[mof_button].id)
				Unload_Asset_Model(sAssetData,PROP_SUB_RELEASE)
			else
				STOP_STREAM()
				bAUDIOStreamLoaded = false 
			endif
			
			if ISENTITYALIVE(peds[mpf_sec1].id)
				if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),<<1240.51318, -2880.35571, 2.10339>>,<<1240.55945, -3057.27075, 17.4>>,29.45)
				or IS_PED_FLEEING(peds[mpf_sec1].id)
				or GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),peds[mpf_sec1].id) > 100
					CLEAR_PED_TASKS(peds[mpf_sec1].id)
					SAFE_RELEASE_PED(peds[mpf_sec1].id)
				endif
			endif
			if ISENTITYALIVE(peds[mpf_sec2].id)
				if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),<<1240.51318, -2880.35571, 2.10339>>,<<1240.55945, -3057.27075, 17.4>>,29.45)
				or IS_PED_FLEEING(peds[mpf_sec2].id)
				or GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),peds[mpf_sec2].id) > 100
					CLEAR_PED_TASKS(peds[mpf_sec2].id)
					SAFE_RELEASE_PED(peds[mpf_sec2].id)
				endif
			endif
			if ISENTITYALIVE(peds[mpf_dockphone].id)				
				if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),<<1240.51318, -2880.35571, 2.10339>>,<<1240.55945, -3057.27075, 17.4>>,29.45)
				or IS_PED_FLEEING(peds[mpf_dockphone].id)
				or GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),peds[mpf_dockphone].id) > 100
					CLEAR_PED_TASKS(peds[mpf_dockphone].id)
					SAFE_RELEASE_PED(peds[mpf_dockphone].id)
				endif
			endif
			if ISENTITYALIVE(peds[mpf_dock2].id)				
				if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),<<1240.51318, -2880.35571, 2.10339>>,<<1240.55945, -3057.27075, 17.4>>,29.45)
				or IS_PED_FLEEING(peds[mpf_dock2].id)
				or GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),peds[mpf_dock2].id) > 100
					CLEAR_PED_TASKS(peds[mpf_dock2].id)
					SAFE_RELEASE_PED(peds[mpf_dock2].id)
				endif
			endif
			
			if is_player_at_location_in_vehicle(slocatesdata,<<324.76532, -2974.49463,-1.5>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,false,vehs[mvf_sub].id,"dkp1_tk1","","DKP1_SUBBK",true)
			or (does_blip_exist(slocatesdata.locationblip)and is_entity_in_angled_area(vehs[mvf_sub].id,<<312.85571, -2974.00317, 1.19166>>,<<339.16544, -2974.34766, -6.70078>>,15)
			and IS_PED_SITTING_IN_THIS_VEHICLE(player_ped_id(),vehs[mvf_sub].id))				
				if isentityalive(vehs[mvf_truck].id)
				and isentityalive(vehs[mvf_trailer].id)
					clear_mission_location_text_and_blips(slocatesdata)
					RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS)
					
					TRIGGER_MUSIC_EVENT("DHP1_STOP")		
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()	
					
					SET_PLAYER_CONTROL(player_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON)
					istagedelay = get_game_timer()		
					
					mission_substage++
				endif
			else
				if GET_PLAYER_WANTED_LEVEL(player_id()) = 0
				and does_blip_exist(slocatesdata.locationblip)
					DRAW_MARKER(MARKER_RING,<<324.76532, -2974.49463,-3.5>>,<<0,0,0>>,<<0,0,0>>,<<5,5,5>>,255,255,0,65)
				endif
			endif
		break
		case 3
			if IS_VEHICLE_ALMOST_STOPPED(vehs[mvf_sub].id)
			or get_game_timer() - iStageDelay > 3500
				//cam
				cameraindex = create_camera_with_params(camtype_scripted,<<341.641937,-2965.524902,4.037704>>,<<17.066954,0.473599,112.444778>>,34.00000)
				set_cam_active(cameraindex,true)
				SHAKE_CAM(cameraIndex,"HAND_SHAKE",0.4)
				SET_CAM_PARAMS(cameraIndex,<<341.641937,-2965.524902,4.037704>>,<<-4.346406,0.473595,112.444778>>,34.000000, 6000)
				prep_start_cutscene(false,<<340.6675, -2964.0103, 1.4937>>)
				
				Load_Asset_Model(sAssetData, PROP_TARP_STRAP)
				//sub
				SET_ENTITY_COORDS(vehs[mvf_sub].id,vsub)
				SET_ENTITY_HEADING(vehs[mvf_sub].id,hsub)
				istagedelay = get_game_timer()		
				icranestage = 0
				
				REPLAY_RECORD_BACK_FOR_TIME(8.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
				
				mission_substage++
			else
				BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehs[mvf_sub].id,7)
			endif
		break
		case 4
			if crane_cs()
				mission_substage++
			endif
		break
		case 5
			if HAS_CUTSCENE_LOADED()
			AND HAS_MODEL_LOADED(PROP_TARP_STRAP)
				REGISTER_ENTITY_FOR_CUTSCENE(vehs[mvf_truck].id, "DockHeist_truck", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				REGISTER_ENTITY_FOR_CUTSCENE(vehs[mvf_sub].id, "submarine", CU_ANIMATE_EXISTING_SCRIPT_ENTITY,SUBMERSIBLE)
				REGISTER_ENTITY_FOR_CUTSCENE(vehs[mvf_trailer].id, "dockheist_trailer", CU_ANIMATE_EXISTING_SCRIPT_ENTITY,ARMYTRAILER)
				REGISTER_ENTITY_FOR_CUTSCENE(null, "sub_cover", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,PROP_SUB_COVER_01)
				REGISTER_ENTITY_FOR_CUTSCENE(null, "Floyd", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,IG_FLOYD)
				
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor",player_ped_id())
				
				SET_VEHICLE_LIGHTS(vehs[mvf_truck].id, FORCE_VEHICLE_LIGHTS_OFF)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_truck].id,true)
				SET_ALL_EXIT_STATES()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
				
				START_CUTSCENE()
				
				mission_substage++
			endif
		break
		case 6
			// 1985471
			IF IS_CUTSCENE_PLAYING()
			
				IF IS_IPL_ACTIVE( "PO1_08_sub_waterplane" )
					REMOVE_IPL( "PO1_08_sub_waterplane" )
				ENDIF
				
			ENDIF
		
			if CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				
				REPLAY_STOP_EVENT()
				
			endif
			
			if NOT DOES_ENTITY_EXIST(peds[mpf_floyd].id)
				if DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Floyd"))
					peds[mpf_floyd].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Floyd"))
					ADD_PED_FOR_DIALOGUE(convo_struct,1,peds[mpf_floyd].id,"FLOYD")
					set_ped_relationship_group_hash(peds[mpf_floyd].id,rel_buddy)
					println("Grabbed Floyd")
				endif
			endif
		
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				println("trev")				
				bcs_trev =	true				
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Floyd")
				println("Floyd")
				bcs_Floyd = true
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("DockHeist_truck")
				SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehs[mvf_truck].id, TRUE, FALSE, FALSE)
				SET_VEHICLE_DOORS_SHUT(vehs[mvf_truck].id,true) //[MF] Fix for B* 1781720
				println("truck")
				bcs_truck = true
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("DockHeist_trailer")
				SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehs[mvf_trailer].id, TRUE, FALSE, FALSE)
				println("trailer")
				bcs_trailer = true
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("submarine")
				println("sub")
				bcs_sub = true
			endif
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("sub_cover")
				println("cover")
				bcs_cover= true
			elif NOT DOES_ENTITY_EXIST(objects[mof_subCover].id)
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("sub_cover",PROP_SUB_COVER_01))
					objects[mof_subCover].id = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("sub_cover",PROP_SUB_COVER_01))
					println("grabbed cover")
				ENDIF
			endif
			
			INT iLightsOn, iFullBeam
			if GET_VEHICLE_LIGHTS_STATE(vehs[mvf_truck].id, iLightsOn, iFullBeam)
				if iLightsOn = 0
					if GET_CUTSCENE_TIME() >= 14200
						SET_VEHICLE_LIGHTS(vehs[mvf_truck].id, SET_VEHICLE_LIGHTS_ON)
					endif
				endif
			endif
			
			if bcs_trev 
			and bcs_floyd 
			and bcs_truck 
			and bcs_sub 
			and bcs_cover 
			and bcs_trailer
			
				IF IS_SCREEN_FADED_IN()
				OR HAVE_ALL_STREAMING_REQUESTS_COMPLETED(peds[mpf_floyd].id)
			
					SET_PED_INTO_VEHICLE(peds[mpf_floyd].id,vehs[mvf_truck].id,VS_FRONT_RIGHT)
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehs[mvf_truck].id)				
					attach_vehicle_on_to_trailer(vehs[mvf_sub].id,vehs[mvf_trailer].id,<<0,-4,-0.7>>,<<0,0,0>>,<<0,0,0>>,-1)		
					set_vehicle_is_considered_by_player(vehs[mvf_sub].id,false)
					SET_VEHICLE_DOORS_SHUT(vehs[mvf_sub].id,true)				
					SET_VEHICLE_DOORS_LOCKED(vehs[mvf_sub].id,VEHICLELOCK_CANNOT_ENTER)								
					set_vehicle_engine_on(vehs[mvf_sub].id,false,true)
					set_vehicle_lights(vehs[mvf_sub].id,force_vehicle_lights_off)
					SET_VEHICLE_RADIO_ENABLED(vehs[mvf_sub].id, FALSE)
					
					attach_entity_to_entity(objects[mof_subcover].id,vehs[mvf_trailer].id,-1,vTarpOffset,<<0,0,0>>)
		
	//				objects[mof_subStraps].id = create_object(PROP_FLATBED_STRAP_B, <<-332.8449,-2622.1572,8.7838>>)
	//				attach_entity_to_entity(objects[mof_subStraps].id,vehs[mvf_trailer].id,-1, vStrapOffset,<<0,0,0>>)
					objects[mof_subStraps].id = create_object(PROP_TARP_STRAP, <<-332.8449,-2622.1572,8.7838>>)
					attach_entity_to_entity(objects[mof_subStraps].id,vehs[mvf_trailer].id,-1, vTarpOffset,<<0,0,0>>)
									
					prep_stop_cutscene(true,false)
					
					Unload_Asset_PTFX(sAssetData)
					
					mission_substage++	
				
				ENDIF
				
			endif
		break
		case 7						
			mission_set_stage(MSF_2_SAFETY)			
			mission_substage = stage_entry
		break
	endswitch
	//blip route to crane
	if does_blip_exist(slocatesdata.locationblip)
		set_blip_route(slocatesdata.locationblip,false)
	endif
	
	if ISENTITYALIVE(player_ped_id())
	and ISENTITYALIVE(vehs[mvf_sub].id)
		if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id()),<<324.76532, -2974.49463,-1.0>>) > 2500
			MISSION_FAILED(mff_left_area)		
		endif
	endif
	
endproc
proc st_2_safety()
	switch mission_substage
		case stage_entry
			IF IS_SCREEN_FADED_OUT()			
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"STAGE 2: Drop off sub", TRUE)
			if ISENTITYALIVE(vehs[mvf_sub].id)
				SET_ENTITY_PROOFS(vehs[mvf_sub].id,false,false,false,true,false)
			endif
			SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehs[mvf_sub].id, FALSE)
			SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehs[mvf_trailer].id, FALSE)
			SET_IGNORE_NO_GPS_FLAG(TRUE)
			SET_GPS_DISABLED_ZONE(<<288.09674, -3133.24854, 0>>,<<301.06049, -2969.62231, 11.47017>>)
			SET_CAN_CLIMB_ON_ENTITY(vehs[mvf_sub].id, FALSE)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_sub].id, FALSE)
			
			REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
			
			bLoadstage		= false
			bcreateScene 	= false
			mission_substage++
		break
		case 1			
		
			if ISENTITYALIVE(player_ped_id())
			and ISENTITYALIVE(vehs[mvf_sub].id)
				if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id()), GET_ENTITY_COORDS(vehs[mvf_sub].id)) > 400
				AND (NOT IS_ENTITY_ON_SCREEN(vehs[mvf_sub].id))
					MISSION_FAILED(mff_left_area)		
				endif
			endif
		
			if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),<<324.76532, -2974.49463,-1.5>>) > 150
				SAFE_RELEASE_OBJECT(objects[mof_cranecab].id)
				SAFE_RELEASE_OBJECT(objects[mof_cranehook].id)
				SAFE_RELEASE_OBJECT(objects[mof_craneLD].id)
				Unload_Asset_Model(sAssetData,PROP_DOCK_CRANE_02_LD)
				Unload_Asset_Model(sAssetData,PROP_DOCK_CRANE_02_CAB)
				Unload_Asset_Model(sAssetData,PROP_DOCK_CRANE_02_HOOK)
			endif
			
			if not bloadstage	
			and GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vWAREHOUSE) < 200
				Load_Asset_Model(sAssetData,BISON)
				bcreateScene = true
				bLoadstage 	= true
			elif bLoadstage
				if GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vWAREHOUSE) > 220
					Unload_Asset_Model(sAssetData,bison)
					bcreateScene 	= false
					bLoadstage 		= false
					IF DOES_ENTITY_EXIST(vehs[mvf_bison].id)
						DELETE_VEHICLE(vehs[mvf_bison].id)
					ENDIF
				elif GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(player_ped_id(),vWAREHOUSE) < 200
					if bcreateScene 
					and HAS_MODEL_LOADED(bison)
						IF NOT DOES_ENTITY_EXIST(vehs[mvf_bison].id)
							vehs[mvf_bison].id = CREATE_VEHICLE(BISON,<<-317.0741, -2594.1404, 5.0004>>, 316.2352)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_bison].id,false)
						ENDIF
						bcreateScene 	= false
					endif
				endif
			endif
			
			
			if IS_ENTITY_ATTACHED_TO_ENTITY(vehs[mvf_trailer].id, GET_PLAYERS_LAST_VEHICLE())	
			or (is_entity_in_angled_area(vehs[mvf_truck].id,<<-333.60571, -2623.68091, 5.00103>>,<<-350.12717, -2640.33911, 12.65742>>,15)
			and is_entity_in_angled_area(peds[mpf_floyd].id,<<-333.60571, -2623.68091, 5.00103>>,<<-350.12717, -2640.33911, 12.65742>>,15)
			and is_entity_in_angled_area(vehs[mvf_sub].id,  <<-333.60571, -2623.68091, 5.00103>>,<<-350.12717, -2640.33911, 12.65742>>,15))

				if ISENTITYALIVE(vehs[mvf_trailer].id)
					SET_TRAILER_INVERSE_MASS_SCALE(vehs[mvf_trailer].id,0.5)
				endif
				if DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(sLocatesData.LocationBlip,true)
				endif
				if DOES_BLIP_EXIST(blip_objective)
					REMOVE_BLIP(blip_objective)
				endif
				if battachprint
					CLEAR_PRINTS()
					battachprint = false
				endif
				
				if 	is_player_at_location_with_buddies_in_vehicle(slocatesdata,vWAREHOUSE,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,true
					,peds[mpf_floyd].id,null,null,GET_PLAYERS_LAST_VEHICLE(),"dkp1_tk2","DKP1_ABFLD","","","","DKP1_TRUCK","DKP1_TRUCKBK",false,true)
				or (does_blip_exist(slocatesdata.locationblip)and is_entity_in_angled_area(GET_PLAYERS_LAST_VEHICLE(),<<-340.132446,-2630.242676,5.001461>>, <<-346.550751,-2636.807617,12.688959>>, 14.937500))
					REMOVE_SCENARIO_BLOCKING_AREAS()
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),7,-1)
					set_vehicle_is_considered_by_player(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),false)
					set_vehicle_is_considered_by_player(vehs[mvf_sub].id,false)
					SET_VEHICLE_DOORS_LOCKED(vehs[mvf_sub].id,VEHICLELOCK_CANNOT_ENTER)				
					clear_mission_locate_stuff(slocatesdata)
					SET_IGNORE_NO_GPS_FLAG(false)
					SET_GPS_DISABLED_ZONE(<<0,0,0>>,<<0,0,0>>)
					iStageDelay = get_Game_timer()
					bFailForStuck = FALSE
					mission_substage++
				endif
				
				
			elif NOT IS_ENTITY_ATTACHED_TO_ENTITY(vehs[mvf_trailer].id, GET_PLAYERS_LAST_VEHICLE()) //vehs[mvf_truck].id)
				CLEAR_MISSION_LOCATION_BLIP(sLocatesData)
				if not DOES_BLIP_EXIST(blip_objective)
					blip_objective = CREATE_BLIP_FOR_VEHICLE(vehs[mvf_sub].id)
				endif
				if not battachprint
				and IS_SAFE_TO_DISPLAY_GODTEXT()
					PRINT_NOW("DKP1_ATTACH",DEFAULT_GOD_TEXT_TIME,1)
					battachprint = true
				endif
			endif
		break
		case 2
			if not IS_PED_IN_ANY_VEHICLE(player_ped_id())
				if CREATE_CONVERSATION(convo_struct,"DHP1AUD","DHP1_END",CONV_PRIORITY_MEDIUM)
					SET_PED_KEEP_TASK(peds[mpf_floyd].id,true)
					TASK_LOOK_AT_ENTITY(peds[mpf_floyd].id,player_ped_id(),-1,SLF_WHILE_NOT_IN_FOV)
					CLEAR_SEQUENCE_TASK(seq)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LEAVE_ANY_VEHICLE(null)
						TASK_GO_TO_ENTITY(null,player_ped_id(),DEFAULT_TIME_NEVER_WARP,3, PEDMOVE_WALK)
						TASK_TURN_PED_TO_FACE_ENTITY(null,player_ped_id(),-1)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(peds[mpf_floyd].id,seq)
					CLEAR_SEQUENCE_TASK(seq)
					mission_substage++
				endif
			else
				if not b_displayGodText
				and IS_SAFE_TO_DISPLAY_GODTEXT()
				and get_game_timer() - iStageDelay > 3000
					PRINT_NOW("dkp1_EXIT",DEFAULT_GOD_TEXT_TIME,1)
					REPLAY_RECORD_BACK_FOR_TIME(10.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					b_displayGodText = true
				endif
			endif
		break		
		case 3 		
			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				if ISENTITYALIVE(vehs[mvf_bison].id)
					CLEAR_PED_TASKS(peds[mpf_floyd].id)
					CLEAR_SEQUENCE_TASK(seq)
					OPEN_SEQUENCE_TASK(seq)
						TASK_ENTER_VEHICLE(null,vehs[mvf_bison].id,default_time_never_warp,VS_DRIVER,PEDMOVEBLENDRATIO_RUN)
						//TASK_VEHICLE_DRIVE_WANDER(null,vehs[mvf_bison].id,30,DRIVINGMODE_AVOIDCARS)
						TASK_VEHICLE_MISSION_COORS_TARGET(null, vehs[mvf_bison].id, <<-189.8985, -2622.7070, 5.0001>>, MISSION_GOTO_RACING, 30.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 5.0, 1.0, TRUE)
						TASK_VEHICLE_MISSION_PED_TARGET(null, vehs[mvf_bison].id, PLAYER_PED_ID(), MISSION_FLEE, 30, DRIVINGMODE_AVOIDCARS_RECKLESS, 1000, 10, TRUE)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(peds[mpf_floyd].id,seq)
					CLEAR_SEQUENCE_TASK(seq)
				else	
					CLEAR_PED_TASKS(peds[mpf_floyd].id)
					TASK_SMART_FLEE_COORD(peds[mpf_floyd].id,vWAREHOUSE,300,-1)
				endif
				iStageDelay = get_game_timer()
				mission_substage++
			endif
		break
		case 4
			if GET_DISTANCE_BETWEEN_ENTITIES(peds[mpf_floyd].id,player_ped_id()) > 70
			or get_Game_timer() - iStageDelay > 20000
				SAFE_RELEASE_PED(peds[mpf_floyd].id)
				mission_passed()
			endif
		break
	endswitch
endproc
proc st_3_passed()
	switch mission_substage
		case stage_entry
			IF IS_SCREEN_FADED_OUT()			
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(180)
			mission_substage++
		break
		case 1
			if IS_SCREEN_FADED_IN()
				mission_passed()
			endif
		break
	endswitch
endproc
// -----------------------------------------------------------------------------------------------------------
//		MISSION FLOW
// -----------------------------------------------------------------------------------------------------------
PROC MISSION_FLOW()
	SWITCH	INT_TO_ENUM(MSF_MISSION_STAGE_FLAGS, MISSION_STAGE)
		CASE 	MSF_0_REACHED_SUB 		ST_0_REACHED_SUB() 		BREAK
		CASE 	MSF_1_GET_TO_CRANE 		ST_1_GET_TO_CRANE() 	BREAK
		CASE 	MSF_2_SAFETY			st_2_safety()			BREAK
		CASE 	msf_3_passed			st_3_passed()			BREAK
	ENDSWITCH
ENDPROC
#IF IS_DEBUG_BUILD			
	PROC DO_DEBUG()
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME)
			bDisplayDebug = !bDisplayDebug
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(bDisplayDebug)
		ENDIF
	
		IF bDisplayDebug
			TEXT_LABEL_63 strDebug
			strDebug = "Mission Stage: "
			strDebug += mission_stage
			DRAW_DEBUG_TEXT_2D(strDebug, <<0.1,0.5,0.0>>, 0,0,0, 255)
			
			strDebug = "Mission Substage: "
			strDebug += mission_substage
			DRAW_DEBUG_TEXT_2D(strDebug, <<0.1,0.525,0.0>>, 0,0,0, 255)
			
			strDebug = "Mission Stage: "
			strDebug += mission_stage
			DRAW_DEBUG_TEXT_2D(strDebug, <<0.1,0.5,0.0>>, 0,0,0, 255)
			
			strDebug = "AI Stage: "
			strDebug += enum_to_int(eAI_stage)
			DRAW_DEBUG_TEXT_2D(strDebug, <<0.1,0.55,0.0>>, 0,0,0, 255)
			
			strDebug = "AI Substage: "
			strDebug += enum_to_int(eCalm_stage)
			DRAW_DEBUG_TEXT_2D(strDebug, <<0.1,0.575,0.0>>, 0,0,0, 255)
			
			strDebug = "bBoardedShip: "
			IF bBoardedShip
				strDebug += "TRUE"
			ELSE
				strDebug += "FALSE"
			ENDIF
			
			DRAW_DEBUG_TEXT_2D(strDebug, <<0.1,0.6,0.0>>, 0,0,0, 255)
			
			strDebug = "debug_bCurrentlyOnShip: "
			IF debug_bCurrentlyOnShip
				strDebug += "TRUE"
			ELSE
				strDebug += "FALSE"
			ENDIF
			DRAW_DEBUG_TEXT_2D(strDebug, <<0.1,0.625,0.0>>, 0,0,0, 255)
		ENDIF
		
		debug_bCurrentlyOnShip = FALSE
	
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_PASSED()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F) 
			MISSION_FAILED(MFF_DEBUG_FAIL)		
		ENDIF
		
	ENDPROC
#ENDIF
// ===========================================================================================================
//		SCRIPT LOOP
// ===========================================================================================================

SCRIPT

	PRINTSTRING("...DOCKS PREP 1 MISSION LAUNCHED")
	PRINTNL()
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		TRIGGER_MUSIC_EVENT("DHP1_FAIL")
		PRINTSTRING("...DOCKS PREP 1 MISSION FORCE CLEANUP")
		PRINTNL()
		SET_REPLAY_ACCEPTED_WARP_LOCATION()				
		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		if IS_CUTSCENE_ACTIVE()
			SET_CUTSCENE_FADE_VALUES()	
		endif
		MISSION_CLEANUP()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)

#IF IS_DEBUG_BUILD
	//Z MENU FOR SKIPPING STAGES
	ZMENUNAMES[MSF_0_REACHED_SUB].STXTLABEL 	=	"STAGE 0: REACHED SUB"
	ZMENUNAMES[MSF_1_GET_TO_CRANE].STXTLABEL 	=	"STAGE 1: GET TO CRANE"
	ZMENUNAMES[MSF_2_SAFETY].STXTLABEL 			=	"STAGE 3: SAFETY"
	ZMENUNAMES[msf_3_passed].STXTLABEL 			=	"------- PASSED -------"
	
	WIDGET_DEBUG = START_WIDGET_GROUP("GET MINI-SUB MENU")
		ADD_WIDGET_FLOAT_SLIDER("ROPE LENGTH",WFROPELENGTH,1,25,0.5)			
		ADD_WIDGET_INT_READ_ONLY("AI 0=norm 1=stealth 2=warn 3=alarm):",wAIstage)
		ADD_WIDGET_INT_READ_ONLY("calm(0=w1, 1=w2, 2=police) :",wCalm_stage)
		ADD_WIDGET_INT_READ_ONLY("alarm(1=gun,2=exp,3=sub,4=cops,5=flee):",wAlarm_stage)
		
		START_WIDGET_GROUP("CRANE")
			ADD_WIDGET_VECTOR_SLIDER("LD V:",vcraneLD,-4000,2000,0.001)
			ADD_WIDGET_FLOAT_SLIDER("LD H:",hcraneLD,0,360,0.5)
			
			ADD_WIDGET_VECTOR_SLIDER("cab offset:",voffsetcab,-4000,2000,0.001)
			ADD_WIDGET_FLOAT_SLIDER("cab H:",hcranecab,0,360,0.5)	
			
			ADD_WIDGET_VECTOR_SLIDER("hook offset:",voffsethook,-4000,2000,0.001)
			ADD_WIDGET_FLOAT_SLIDER("hook H:",hcranehook,0,360,0.5)	
			
			ADD_WIDGET_VECTOR_SLIDER("sub pos:",vsub,-4000,2000,0.001)
			ADD_WIDGET_FLOAT_SLIDER("hook H:",hsub,0,360,0.5)	
			
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	SET_LOCATES_HEADER_WIDGET_GROUP(WIDGET_DEBUG)
#ENDIF

	//INITIALIZE MISSION
	MISSION_SETUP()
	
	WHILE (TRUE)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ThePortOfLSHeistPrep1")
		
		//prestreaming loop
		Update_Asset_Management_System(sAssetData)		// Deals with loading any assets and keeps track of what has been loaded
		Update_Cutscene_Prestreaming(sCutscenePedVariationRegister)	
		//MAIN MISSION LOOP
		MISSION_STAGE_MANAGEMENT()
		MISSION_STAGE_SKIP()
		
		if not bDoSkip
			MISSION_CHECKS()
			mission_flow()
		endif 	
				
#IF IS_DEBUG_BUILD
	DO_DEBUG()
#ENDIF	
	
		WAIT(0)
	
	ENDWHILE
ENDSCRIPT
