//+-----------------------------------------------------------------------------+
//¦																				¦
//¦				Author: Adam Westwood			Date: 09/11/2010		 	    ¦
//¦																				¦
//¦-----------------------------------------------------------------------------¦
//¦																				¦
//¦ 			            Docks Setup - Scout Docks                 	        ¦
//¦ 																			¦
//¦ 	                                             							¦
//¦																				¦
//+-----------------------------------------------------------------------------+

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//---------------------------------¦ HEADERS ¦-----------------------------------

//CL 3663473 submitted to create new AGGRESSIVE_INVESTIGATE group. Adds RELGROUPHASH_AGGRESSIVE_INVESTIGATE to the REL_GROUP_HASH enum in script.
//Adam, back to you. Unfortunately my CL is code with a script header change, so you may need a new build to be able to change the new AGGRESSIVE_INVESTIGATE's relationship.

//And some other new roots:
//DS1_noton
//DS1_whyme

//Floyd waving at Trevor
//- Dictionary: friends@frj@ig_1

///depot/gta5/art/anim/export_mb/FRIENDS@/FRJ@/IG_1/
//WAVE_A.anim
//WAVE_B.anim
//WAVE_C.anim
//WAVE_D.anim
//WAVE_E.anim

//SUPERVISOR A0


USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_misc.sch"
USING "commands_pad.sch" 
USING "commands_script.sch"
USING "commands_player.sch"
USING "commands_streaming.sch"
USING "commands_vehicle.sch" 
USING "commands_camera.sch"
USING "commands_path.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_object.sch"
USING "commands_task.sch"
USING "commands_misc.sch"

USING "streamed_scripts.sch"
USING "cellphone_public.sch"
USING "flow_public_core_override.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "selector_public.sch"
USING "player_ped_public.sch"
USING "chase_hint_cam.sch"
USING "commands_physics.sch"
USING "cutscene_public.sch"
USING "script_blips.sch"
USING "replay_public.sch"
USING "locates_public.sch"
USING "script_ped.sch"
USING "commands_debug.sch" 
USING "commands_physics.sch"
USING "automatic_door_public.sch"
USING "script_heist.sch" 
USING "taxi_functions.sch"
USING "CompletionPercentage_public.sch"
USING "Mission_stat_public.sch"
USING "area_checks.sch"
USING "vehicle_gen_public.sch"
USING "Timelapse.sch"
USING "clearmissionarea.sch"
USING "flow_mission_trigger_public.sch"
USING "tv_control_public.sch"
USING "cheat_controller_public.sch"
USING "commands_recording.sch"
USING "script_misc.sch"
//USING "asset_management_public.sch"

//For the Text 
CONST_INT MAX_QUEUED_TEXT 10
CONST_INT MAX_QUEUED_HELP_AL 3
CONST_INT MAX_TEXT_HASHES 50
//STRING sConversationBlock = "D1AUD"

structPedsForConversation sSpeech

CONST_INT MPF_NUM_OF_PEDS 5

//CUTSCENE_PED_VARIATION sCutscenePedVariationRegister[MPF_NUM_OF_PEDS]

USING "text_queue.sch"

#IF IS_DEBUG_BUILD
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

//---------------------------------:CUTSCENE VARS:------------------------------------------
BOOL b_skipped_mocap
INT i_current_event
BOOL b_is_jumping_directly_to_stage
VEHICLE_INDEX veh_pre_mission_car

ENUM SECTION_STAGE
	SECTION_STAGE_SETUP = 0,
	SECTION_STAGE_RUNNING,
	SECTION_STAGE_CLEANUP,
	SECTION_STAGE_SKIP,
	SECTION_STAGE_JUMPING_TO,
	SECTION_DEBUG
ENDENUM

SECTION_STAGE e_section_stage 		= SECTION_STAGE_SETUP

CONST_INT MAX_ANIMS         15
CONST_INT iTotalBomb 3
CONST_INT iTotalMerc 15
CONST_INT iTotalDockWorker 22
CONST_INT iTotalNumberOfPtX 25
CONST_INT iNumberOfPatrols 20
CONST_INT iTotalVehicles 6
CONST_INT iNumberOfDockSetPieces 12
CONST_INT iNumberOfWayPointTasks 10
CONST_INT iTotalNumberOfPhotos 3
CONST_INT iTotalBuddy 3
CONST_INT MAX_NUM_OF_CLIPBOARDS 10
CONST_INT CONST_IGNORE_THIS 0
INT iPhotosTaken
INT iHelp 
INT iCraneHelp
INT iCraneTimer
INT i_placeholder_dialogue_timer	= 0
INT i_current_dialogue_duration		= 0
INT iMissionDialogue
INT iDialogueTimer
INT iHandlerSection
INT iCraneFailTimer
INT iCraneDialogueTimer
INT iReplayStage
INT iNextFloydHurrySpeechTime
INT iStageStartTime
INT iGateSecurityGuy
INT iSecurityTimer
INT sceneBlackAbuse
INT sceneBlackAbuse2
INT soundBeating
INT iTimer
INT iReleaseTimer
INT ssCraneWorker
INT iCraneHelpTimer
INT iFailTimer
INT iBlockReplayCameraTimer
INT iMerryWeatherFail
INT iDamagedContainer
INT iFloydHauler
INT iAmbientDialogueTimer
INT iJumpTimer
INT iStaggerTimer
INT iHandlerFailTimer
INT iCraneSound = GET_SOUND_ID()
INT iHandlerDialogueSwitch
INT iCraneStrain = GET_SOUND_ID()
INT iManagePickerAudio

INT ssWadeOnCouch

INT iLineMatch = -1

//Time of Day
//CAMERA_INDEX camTOD
//INT iTimeOfDayTracker
//TIMEOFDAY todReference

CAMERA_INDEX cam_interp
CAM_VIEW_MODE cCam

TEXT_LABEL_23 tSavedBanterRoot
TEXT_LABEL_23 tResumeBanterLabel

//BOOL 
BOOL bRunFailChecks
BOOL bCleanup 
BOOL bMissionStageLoaded
BOOL bDisabledHotSwap
BOOL balarmtriggered
//BOOL bPrinted
BOOL bCleanupStage
BOOL bInitStage
BOOL bForceTasks
BOOL bDialogue
BOOL bGodText
BOOL bdistancewarning
BOOL bTooFar = FALSE
BOOL bClearText = FALSE
BOOL bDoneWithTruck = FALSE
BOOL bContainerSetUpForDrive = FALSE
BOOL bClearTasks = FALSE
BOOL bLockForklift
BOOl bToggleObjective = FALSE
BOOL bHelpText = FALSE
BOOL bFakeSound
BOOL bRestrictedSupressed = FALSE
BOOL bPhoneCallStarted = FALSE
BOOL bFloydWalkPaused
BOOL bKilledOnFootConversation
BOOL bDoneHurrySpeech
BOOL bIncreaseFloydSpeed
FLOAT fFloydWalkSpeed
BOOL bSavedConvo 
//BOOL bDoneBanter
BOOL bDoneConversationAnim[20]
BOOL bSetAltAnim = FALSE
BOOL bCarryingContainer = FALSE
BOOL bInitialGodText = FALSE
BOOL bTrailerAttachements = FALSE
BOOL bCreateFirstHauler = FALSE
BOOL bWaveIn = FALSE
BOOl bClearCutscenArea = FALSE
BOOl bMercPatrolCreated[iTotalMerc]
BOOl bPatrolCreated[iTotalDockWorker]
BOOL bIgnoreTheseFails = FALSE
BOOL bHitMarkToTriggerAudioSceneChange = FALSE
BOOL bBoardCutsceneRequested = FALSE
BOOL bPedDockCraneVariation = FALSE

BOOL  bDoPlayerAnims = FALSE

BOOL bHasChanged

//TRUE, TRUE, FALSE, FALSE
BOOL b1 = TRUE
BOOL b2 = FALSE
BOOL b3 = FALSE
BOOL b4 = FALSE

BOOL bAddedRopes
BOOL bSetYourTimer
BOOL bForceNoGrab
BOOL bTriggerSetPiece[iNumberOfDockSetPieces] 
BOOL bWaypointTask[iNumberOfWayPointTasks]
BOOL bDoCatchUp
BOOL bCraneSectionComplete
BOOL bCrate1Attached
BOOL bCrate0Attached
BOOL bIsJumpingDirectlyToStage
BOOL bObjectsCreated[4]
BOOL bSkipping	= FALSE	
BOOL bGoToSpeach = FALSE
BOOL bcutsceneplaying = TRUE

INT iNumberAnimsLoaded
STRING sAnimLoaded[MAX_ANIMS]

INT iWayTarget
INT iWayPlayer
FLOAT fSpeed
VECTOR vDistTarget
VECTOR vDistPlayer
FLOAT fPlayerDist
FLOAT fTargetDist		
TWEAK_FLOAT fIdealDistance  0.6
TWEAK_FLOAT fMinDistance  1.1
TWEAK_FLOAT fMaxDistance  2.0
TWEAK_FLOAT fTargerMaxCatchUp  2.0
FLOAT fThisSpeed = 1

FLOAT fXTolerance = 0.48
FLOAT fYTolerance = 0.42

FLOAT fThisValue = -5.700

FLOAT fFallOff = 9.8
FLOAT fIntensity  = 30
FLOAT fInnerAngle  = 7.0
FLOAT fOuterAngle  = 7.0
FLOAT fExposure  = 10.1

INT icutsceneprog

//Crane stuff
BOOL b_is_crane_cinematic_active

VECTOR v_helper_offset

VECTOR v_crane_pos = << -53.6982, -2415.7900, 5.0500 >> //<< -53.2665, -2415.8633, 5.066 >>
BOOL b_is_audio_scene_active
CONST_FLOAT AUDIO_TRIGGER_THRESHOLD		0.01
CONST_FLOAT SPREADER_START_OFFSET		-7.0 //-15.4 
CAMERA_INDEX cam_cutscene
VECTOR v_left_door_offset 				= <<-1.3, -6.08, 1.4>>
VECTOR v_right_door_offset 				= <<1.3, -6.08, 1.4>>
BLIP_INDEX blip_current_destination
BLIP_INDEX container0_blip
BLIP_INDEX container1_blip
BLIP_INDEX Blip_player

OBJECT_INDEX ObjdockProps[10]
OBJECT_INDEX objHandlerContainer[3]
INT iGandDTimer
INT iCameraTimer

VECTOR vAttach1 = <<-0.800, 0.0, 0.0>>
VECTOR vAttach2 = <<-2.0, 0.0, 1.3>>
VECTOR vAttachRot = <<0,0,0>>
VECTOR vInitialCabinPosition

INTERIOR_INSTANCE_INDEX interior_living_room

//VECTOR VECTOR_ZERO = <<0.0, 0.0, 0.0>>
							
//Ensure you reset bPrint if P-skipping is going on
CONST_INT iPrintTotal 25
//STRING sPrintList[iPrintTotal]
ENUM PrintList
	PH_HELP1,
	PH_HELP2,
	PH_HELP3,
	PH_HELP4,
	FL_HELP1,
	FL_HELP2,
	DCKH_SWP_TRVR,
	DCKH_COVER,
	DCKH_GEOTAG,
	DCKH_DETONATE,
	DCKH_DIA1,
	DCKH_DIA2,
	DCKH_DIA3,
	DCKH_DIA4,
	DCKH_DIA5,
	DCKH_DIA6,
	DCKH_HELP1,
	DCKH_HELP2,
	DCKH_PASS,
	DCKH_FAIL
ENDENUM

ENUM MISSION_REQUIREMENT
	REQ_CRANE,
	REQ_HAULER_WITH_TRAILER,
	REQ_CONTAINERS_FOR_HANDLER_SECTION_START,
	REQ_CONTAINERS_FOR_HANDLER_SECTION_FINAL,
	REQ_HAULER_WITH_TRAILER_FINAL_STAGE,
	REQ_TREVORS_TRUCK,
	REQ_FLOYD,
	REQ_WADE,
	REQ_SECURITY,
	REQ_HANDLER,
	REQ_CAR_TO_DRIVE_FINAL_STAGE,
	REQ_MERRYWEATHER_GUARDS_AT_OTHER_DOCKS,
	REQ_GUARDS_AND_FLOYD,
	REQ_CRANE_CONTAINERS
ENDENUM

//SynchScene Data
INT sceneArriveStartId
INT sceneArriveMainId
//INT sceneArriveEndId

INT sceneDockPipe1Id
INT sceneDockPipe2Id
INT sceneDockPipe3Id
INT sceneDockPipe4Id
INT sceneDockPipe5Id

INT sceneBlackWater1Id
INT sceneBlackWater2Id
INT sceneBlackWater3Id
INT sceneBlackWater4Id
INT sceneBlackWater5Id

INT sceneGantryCraneDynamic1
INT sceneGantryCraneDynamic2
INT sceneGantryCraneDynamic3
INT sceneGantryCraneDynamic4

INT sceneGuysAroundCar1
INT sceneGuysAroundCar2
INT sceneGuysAroundCar3

INT sceneDockWorkerTalking1A
INT sceneDockWorkerTalking2A

INT ssCraneAnims

INT sceneHandlerA1

INT sceneForkliftB1

INT sceneWelder

INT sceneCraneStatic

INT sceneDummy

INT sceneFloydPackage1init

INT iWalkAndTalk
INT iMerryWeatherResponse

MODEL_NAMES model_crane = Prop_Dock_RTG_LD 
//MODEL_NAMES model_spreader = P_Dock_RTG_LD_spdr
MODEL_NAMES model_spreader = P_DOCK_CRANE_SLD_S //P_DOCK_CRANE_SPRDRLD
MODEL_NAMES model_cabin = P_Dock_RTG_LD_Cab
MODEL_NAMES model_container =  Prop_Container_LD //Prop_Container_LD
MODEL_NAMES model_wheel_left = P_Dock_RTG_LD_wheel
MODEL_NAMES model_wheel_right = P_Dock_RTG_LD_wheel
MODEL_NAMES model_boom = P_Dock_RTG_LD_wheel //P_DOCK_CRANE_CABLES //?
MODEL_NAMES model_left_door = PROP_CNTRDOOR_LD_L
MODEL_NAMES model_right_door = PROP_CNTRDOOR_LD_R
MODEL_NAMES model_wheel = model_wheel_left

//VECTOR vWadeArriveStart = <<-61.084805,-2525.028320,5.950000>>
//FLOAT fWadeArriveStart =  -10.750012
//
//VECTOR vFloydArriveStart = <<-61.744289,-2524.412598,5.950000>>
//FLOAT fFloydArriveStart =  -20.750008

STRUCT SETPIECE_PED
	PED_INDEX ped
	BLIP_INDEX blip
	MODEL_NAMES model
	BOOL b_is_created
	INT i_event
	INT i_timer
	OBJECT_INDEX obj_prop
	//VECTOR v_dest
	VECTOR v_spawn_position
	FLOAT f_start_heading
	COVERPOINT_INDEX cover
	VECTOR v_ss_pos
	FLOAT f_ss_rot
ENDSTRUCT

STRUCT SETPIECE_VEHICLE
	VEHICLE_INDEX veh
	MODEL_NAMES model
	BOOL b_is_created
	INT i_event
	VECTOR v_spawn_position
	FLOAT f_start_heading
ENDSTRUCT

SETPIECE_VEHICLE s_sv_car_to_admire[1]

SETPIECE_PED s_sp_supervisor1[1]
SETPIECE_PED s_sp_blackwater_with_dockworker [3]
SETPIECE_PED s_sp_gantry_guys_static [2]
SETPIECE_PED s_sp_guys_around_car [5]
SETPIECE_PED s_sp_dock_workers_on_pipe [3]
SETPIECE_PED s_sp_welder[1]
SETPIECE_PED s_sp_dock_workers_talking [2]
SETPIECE_PED s_sp_forklift_supervisorA[1] 
SETPIECE_PED s_sp_forklift_supervisorB[1]
SETPIECE_PED s_sp_gantry_guys_dynamic [4]
SETPIECE_PED s_sp_floyd_grabs_package [3]

ENUM AMBIENT_SET_PIECE_SPEECH_ENUM
	AMBSP_BLACKWATER_WITH_DOCK_WORKER,
	AMBSP_DOCK_WORKERS_ON_PIPE,
	AMBSP_GUYS_WITH_THE_PACKAGE,
	AMBSP_GUYS_BY_THE_CAR,
	AMBSP_WELDER
ENDENUM


ENUM SynchList
	SS_EMPTY,
	SS_ARRIVE_AT_DOCKS,
	SS_FLOYD_GRABS_PACKAGE,
	SS_GANTRY_GUYS,
	SS_guys_around_car,
	SS_dock_workers_on_pipe,
	SS_WELDER,
	SS_dock_workers_talking,
	SS_FORKLIFT_SUPERVISOR_A,
	SS_FORKLIFT_SUPERVISOR_B,
	SS_WALK_AND_TALK_1,
	SS_blackwater_with_dockworker,
	SS_STATIC_GANTRY_GUYS,
	SS_NUMBER_OF_ITEMS_IN_THIS_ENUM
ENDENUM

STRUCT SCENE_DATA
	//STRING st_AnimDict
	//STRING st_AnimName
	VECTOR v_scene_pos
	VECTOR v_scene_rot
	BOOL b_data_generated
ENDSTRUCT

BOOL bSetPiece[SS_NUMBER_OF_ITEMS_IN_THIS_ENUM]

PED_INDEX ped_dummy[5]

VECTOR v_ss_floyd[SS_NUMBER_OF_ITEMS_IN_THIS_ENUM]
FLOAT f_ss_floyd[SS_NUMBER_OF_ITEMS_IN_THIS_ENUM]
VECTOR v_ss_wade[SS_NUMBER_OF_ITEMS_IN_THIS_ENUM]
FLOAT f_ss_wade[SS_NUMBER_OF_ITEMS_IN_THIS_ENUM]

BOOL bSSComplete[SS_NUMBER_OF_ITEMS_IN_THIS_ENUM]

SCENE_DATA s_sd_arrive_at_docks
SCENE_DATA s_sd_gantry_guys_dynamic
SCENE_DATA s_sd_gantry_guys_static
SCENE_DATA s_sd_guys_around_car
SCENE_DATA s_sd_dock_workers_on_pipe
SCENE_DATA s_sd_welder
SCENE_DATA s_sd_forklift_supervisor_a
SCENE_DATA s_sd_forklift_supervisor_b
SCENE_DATA S_sd_blackwater_with_dockworker
SCENE_DATA s_sd_floyd_grabs_package

//SynchList eSynch = SS_ARRIVE_AT_DOCKS

//INT
INT iSetupProgress
INT iProgress
INT iCount
//INT iLastGodTextID
INT iPreStreamStage
INT iAlarmID
INT iThisCounter
INT iCutsceneStage

FLOAT fMercStart[iTotalMerc]
FLOAT fDockWorkerStart [iTotalDockWorker]
FLOAT fPlayerStart = 133.1177
FLOAT fstartheading
FLOAT ffloydStart = 30.6234
FLOAT fWadeStart = 30.6234
FLOAT fSecurity = 233.1648
FLOAT fDockVeh[iTotalVehicles]

FLOAT fVal1 = 0.8
FLOAT fVal2 = 0.8
FLOAT fVal3 = 3.1
FLOAT fVal4 = 3.5

PED_INDEX pedMerc[iTotalMerc]
PED_INDEX pedDockWorker[iTotalDockWorker]
PED_INDEX pedFloyd
PED_INDEX pedWade
PED_INDEX pedSecurity

VEHICLE_INDEX vehMission
VEHICLE_INDEX vehHandler
VEHICLE_INDEX vehDocks[iTotalVehicles]
//Other Docks
VEHICLE_INDEX vehFloydTruck
VEHICLE_INDEX vehFinal
VEHICLE_INDEX vehBlocking
VEHICLE_INDEX vehFirstHauler
VEHICLE_INDEX vehTrailer1
VEHICLE_INDEX vehTrailer2
VEHICLE_INDEX vanByDocks

OBJECT_INDEX objClipboard[MAX_NUM_OF_CLIPBOARDS]
OBJECT_INDEX objPencil[MAX_NUM_OF_CLIPBOARDS]
//OBJECT_INDEX objBlocking

//ROPE_INDEX RopeSub

SEQUENCE_INDEX seqMain

//Camera
CAMERA_INDEX camMain

//VECTOR
VECTOR vStageStart = << -1154.9279, -1521.5001, 9.6346 >>  
VECTOR vPlayerStart = << -1154.9279, -1521.5001, 9.6346 >> 
VECTOR vfloydStart = << -1149.9487, -1528.3273, 3.2755 >> 
VECTOR vWadeStart = << -1149.9487, -1526.3273, 3.2755 >> 
VECTOR vMercStart[iTotalMerc]
VECTOR vMercPatrol[iTotalMerc][4] 
VECTOR vDockWorkerStart [iTotalDockWorker]
VECTOR vDockPatrol[iTotalDockWorker][4]
VECTOR vDocks = << 18.1416, -2532.4927, 5.0504 >>
VECTOR vForkLiftLoadingArea = << -95.8455, -2455.7607, 5.0191 >>
VECTOR vSecurity = << 12.2127, -2531.0344, 5.0509 >>
VECTOR vParkUpHere = <<-59.0169, -2531.6919, 5.0103>> //<< -61.0438, -2531.3821, 5.0103 >>
VECTOR vDockVeh[iTotalVehicles]

VECTOR vSpreaderPosition = <<-52.94, -2415.64, 14.76>>

BLIP_INDEX blipDockWorker[iTotalDockWorker]
BLIP_INDEX blipHandlerContainers[3]
BLIP_INDEX blipCrane
BLIP_INDEX blipHaulerLoadingArea

PTFX_ID PTX_SHIP_EXPLOSIONS[iTotalNumberOfPtX]
BOOL bPTX_Triggered[iTotalNumberOfPtX]

//CRANE VARIABLES

FLOAT MAX_CABIN_Y		=5.5
FLOAT MIN_CABIN_Y	    =-6.4
//	
FLOAT MAX_SPREADER_Z 		=-3 //-2
FLOAT MIN_SPREADER_Z 		=-14 //-14
//	
FLOAT MAX_CRANE_OFFSET	= 5.0
FLOAT MIN_CRANE_OFFSET	=-56.600
//	
FLOAT MAX_CABIN_VEL		=3.0 //5.0
FLOAT MAX_SPREADER_VEL	= 3.0//5.0
FLOAT MAX_CRANE_VEL		=3.0
//	
FLOAT CABIN_ACCEL			=5.0
FLOAT SPREADER_ACCEL		=3.0//5.0
FLOAT CRANE_ACCEL			=3.0
//BOOL b_added_ropes = FALSE
INT i_spreader_dampingframes			= 120
FLOAT f_ropes_len_change_rate			= 1.4//3.5
FLOAT f_ropes_min_len					= 4 //2
FLOAT f_spreader_damping				= 1.3

BOOL bInhibitCrane = FALSE

//CRANE CAM

//VECTOR v_cabin_attach_start = <<15.1064, -25.756, 9.692>>
//VECTOR v_cabin_point_start = <<13.610, -23.579, 11.002>>
//
//VECTOR v_cabin_attach_end = <<9.763, -23.549, 12.491>>
//VECTOR v_cabin_point_end = <<9.100, -23.647, 9.751>>
//
//VECTOR v_spreader_attach_start = <<15.1069, -25.8024, 11.8412>>
//VECTOR v_spreader_point_start = <<13.6548, -23.6745, 10.3039>> 
//
//VECTOR v_spreader_attach_end =  <<15.1069, -25.5991, -10.000>> 
//VECTOR v_spreader_point_end = <<13.6548, -23.1884, -10.5>>

//Original

//VECTOR v_cabin_attach_start = <<15.1064, -25.8022, 11.8410>>
//VECTOR v_cabin_point_start = <<13.6553, -23.6718, 10.3063>>
//
//VECTOR v_cabin_attach_end = <<7.4302, -25.8017, 11.7590>>
//VECTOR v_cabin_point_end = <<6.4453, -23.4199, 10.2238>>
//
//VECTOR v_spreader_attach_start = <<15.1069, -25.8024, 11.8412>>
//VECTOR v_spreader_point_start = <<13.6548, -23.6745, 10.3039>> 
//
//VECTOR v_spreader_attach_end =  <<15.1069, -25.5991, -10.000>> 
//VECTOR v_spreader_point_end = <<13.6548, -23.1884, -10.5>>

VECTOR v_cabin_attach_start = <<15.1064, -25.8022, 11.8410>>
VECTOR v_cabin_point_start = <<13.6553, -23.6718, 10.3063>>

VECTOR v_cabin_attach_end = <<20.0, -21.400, 14.200>>
VECTOR v_cabin_point_end = <<2.800, -2.800, 4.343>>

VECTOR v_spreader_attach_start = <<15.1069, -25.8024, 11.8412>>
VECTOR v_spreader_point_start = <<13.6548, -23.6745, 10.3039>> 

VECTOR v_spreader_attach_end =  <<15.1069, -25.5991, -10.000>> 
VECTOR v_spreader_point_end = <<13.6548, -23.1884, -10.5>>

//VECTOR v_attach_to_cam_value = <<-1.512, 0.0, -1.678>>//<<-2.5, 0.0, -3.5479>>
VECTOR v_attach_to_cam_value = <<-1.512, 0.0, -1.078>>
VECTOR v_crane_attach_cam_rot = <<-89.0, 0.0, -90.0>>
FLOAT f_crane_attach_cam_fov = 100
FLOAT f_crane_cam_fov = 45.0193

VECTOR vRopeTop1= <<-2.6, -1.0, 0.285>> //<< -0.7,-2.6, 0.3>> //0.3
VECTOR vRopeBottom1 = <<1.0, -2.55, 1.75>>//<<-2.65, -0.7, 0.2>>

VECTOR vRopeTop2= <<2.6, -1.0, 0.285>>//<< -0.7,2.6, 0.3>>//
VECTOR vRopeBottom2 = <<1.0,2.65,1.75>>//<<2.65, -0.7, 0.2>>

VECTOR vRopeTop3= <<-2.6, 1.0, 0.285>>//<<0.7,-2.6,0.3>>
VECTOR vRopeBottom3 = <<-1.0,-2.55, 1.75>>//<<-2.65, 0.7, 0.2>>

VECTOR vRopeTop4= <<2.6, 1.0, 0.285>>//<< 0.7,2.6, 0.3>>
VECTOR vRopeBottom4 = <<-1.0,2.65,1.75>>//<<2.65, 0.7, 0.2>>

//Inside ropes

VECTOR vRopeTop5=  <<2.6, 0.05, 0.285>>//<< -0.05,2.6, 0.3>>
VECTOR vRopeBottom5 = <<-0.05,2.65,1.75>>//<<2.65, -0.05, 0.2>>

VECTOR vRopeTop6= <<2.6, -0.05, 0.285>>//<<0.05,2.6, 0.3>>
VECTOR vRopeBottom6 = <<0.05, 2.65, 1.75>>//<<2.65, 0.05, 0.2>>

VECTOR vRopeTop7= <<-2.6, 0.05, 0.285>>//<<-0.05, -2.6,0.3>>
VECTOR vRopeBottom7 = <<-0.05, -2.55, 1.75>>//<<-2.65, -0.05, 0.2>>

VECTOR vRopeTop8= <<-2.6, -0.05, 0.285>>//<<0.05,-2.6, 0.3>>
VECTOR vRopeBottom8 = <<0.05, -2.55, 1.75>>//<<-2.65,0.05, 0.2>>

LOCATES_HEADER_DATA sLocatesData

WEAPON_TYPE wtAdRifle = WEAPONTYPE_ADVANCEDRIFLE


structPedsForConversation s_conversation_peds

SCRIPTTASKSTATUS TaskStatus
CONST_FLOAT CONVERSATION_PAUSE_DISTANCE 50.0

//INT
INT iHelpDuration

//VECTOR
VECTOR vHelpText
STRING sHelpText

ENTITY_INDEX entHelpText
//Arrow direction for help text
eARROW_DIRECTION eArrowDirection = HELP_TEXT_SOUTH

// ========================================= ENUMS =========================================
// MISSION_ENUMS stages that make up each section of the mission
ENUM MISSION_STAGE_ENUM
    MISSION_STAGE_SETUP,			//0
	MISSION_STAGE_OPENING_CUTSCENE,  
	MISSION_STAGE_1,				
	MISSION_STAGE_2,								
	MISSION_STAGE_4,				
	MISSION_STAGE_5,				
	MISSION_STAGE_6,				
	MISSION_STAGE_7,
	MISSION_STAGE_8,
	MISSION_STAGE_10,
	MISSION_STAGE_11,
	MISSION_STAGE_PASSED,			
	MISSION_STAGE_FAIL				
ENDENUM
MISSION_STAGE_ENUM eMissionStage = MISSION_STAGE_SETUP

MISSION_STAGE_ENUM eFailedStage = MISSION_STAGE_SETUP
//different mission fail conditions

STRUCT CRANE_DATA
	OBJECT_INDEX obj_main
	OBJECT_INDEX obj_cabin
	OBJECT_INDEX obj_spreader
	OBJECT_INDEX obj_lift
	OBJECT_INDEX obj_wheels[8]
	OBJECT_INDEX obj_helper
	OBJECT_INDEX obj_boom_cable
	ROPE_INDEX ropes[8]
	
	VECTOR v_cabin_attach_offset
	VECTOR v_spreader_attach_offset
	VECTOR v_lift_attach_offset
	VECTOR v_start_pos
	VECTOR v_wheel_offsets[8]
	VECTOR v_wheel_rotation
	VECTOR v_prev_spreader_pos
	VECTOR v_prev_spreader_rot
	
	
	FLOAT f_crane_offset
	FLOAT f_cabin_vel
	FLOAT f_spreader_vel
	FLOAT f_lift_vel
	FLOAT f_crane_vel
	FLOAT f_cam_z_progress
	FLOAT f_cam_x_progress
	
	BOOL b_container_attached
	BOOL b_lift_active
	BOOL b_spreader_stuck_on_ground
	
	INT i_cabin_sound_id
	INT i_spreader_fail_sound_id
	INT i_spreader_sound_id
	INT i_crane_sound_id
	INT i_lift_sound_id
	INT i_strain_sound_id
	INT i_dampingframes
	INT i_num_vertical_movement_frames
	INT i_num_cabin_movement_frames
	INT i_current_cam
	INT i_prev_cam
ENDSTRUCT

STRUCT CONTAINER_SPOT
	VECTOR v_offset
	BOOL b_is_taken
	BOOL b_is_active
ENDSTRUCT

STRUCT CARGO_CONTAINER
	OBJECT_INDEX obj_main
	OBJECT_INDEX obj_left_door
	OBJECT_INDEX obj_right_door
	BLIP_INDEX blip
	
	CONTAINER_SPOT s_spots[2]
	BOOL b_loaded_on_truck
	BOOL b_is_full
	
	FLOAT f_speed
	FLOAT f_prev_speed
	VECTOR vPrevVel
	
	INT iTimeSinceDropped
	INT iTimeSinceLastCollisionSound
	BOOL bCollisionSoundPrimed
	
ENDSTRUCT

CARGO_CONTAINER s_containers[2]
CRANE_DATA s_crane

ENUM MISSION_FAIL_ENUM
	#IF IS_DEBUG_BUILD
		FORCE_FAIL,
	#ENDIF
	GENERIC,
	VEHICLE_DEAD,
	VEHICLE_STUCK,
	FORKLIFT_DEAD,
	FLOYDS_FORKLIFT_DEAD,
	FLOYD_LEFT,
	FLOYD_DEAD,
	WADE_LEFT,
	COVER_BLOWN,
	COVER_BLOWN_RAN_OVER,
	TRUCK_DEAD,
	TRUCK_STUCK,
	HANDLER_DEAD,
	HANDLER_STUCK,
	DAMAGED_CARGO,
	DAMAGED_HANDLER_CARGO,
	DAMAGED_CRATES,
	ABADONED_DOCKS,
	WADE_AND_FLOYD_DEAD,
	DITCHED_THE_CARGO,
	WADE_DEAD
ENDENUM

MISSION_FAIL_ENUM reason_for_fail = GENERIC
//debug things

#IF IS_DEBUG_BUILD
	//BLIP_INDEX
	WIDGET_GROUP_ID wgDOCKSSETUP
	INT iMissionStageDB
	INT iProgressDB
	INT iPreLoadStageDB
	INT iSpeechTextProgressDB
	INT iCraneHelpDB
	INT iWidgetMissionSkip
	INT ieMissionStageTemp
	BOOl bJumpToMissionJump
	INT iSpeechProgress
	INT iMissionDialogueDB 
	
	CONST_INT MAX_SKIP_MENU_LENGTH 11	//number of stages in mission(length of menu)
	INT iReturnStage					//mission stage to jump to
	MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]//struct containing the debug menu 
#ENDIF	

// =============================== PROCEDURES AND FUNCTIONS ================================

BOOL bPCControlsSetup = FALSE

// Initialise PC controls for peircing and tatooing minigames
PROC SETUP_PC_CONTROLS()

	IF IS_PC_VERSION()
		IF NOT bPCControlsSetup
			INIT_PC_SCRIPTED_CONTROLS( "Port_LS_Heist_crane")
			bPCControlsSetup = TRUE
		ENDIF
	ENDIF

ENDPROC


PROC CLEANUP_PC_CONTROLS()
	IF IS_PC_VERSION()
		IF bPCControlsSetup
			SHUTDOWN_PC_SCRIPTED_CONTROLS()
			bPCControlsSetup = FALSE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL INIT_CUTSCENE_STAGE()
	IF bInitStage = FALSE
		SETTIMERA(0)
		bInitStage = TRUE
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC ADVANCE_CUTSCENE()
	SETTIMERA(0)
	iCutsceneStage++
ENDPROC

PROC ADVANCE_STAGE()
	bCleanupStage = TRUE
ENDPROC

FUNC BOOL CLEANUP_STAGE()
	IF bCleanupStage = TRUE
		SETTIMERA(0)
//		hotswapStage = MISSION_HOTSWAP_STAGE_SELECT_CHARACTER
		bInitStage = FALSE
		bCleanupStage = FALSE
		iCutsceneStage = 0
		//bPrinted = FALSE
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC


PROC SET_PLAYER_START_POSITION(MISSION_STAGE_ENUM eMissionStagePassed)
	//DRIVE WITH floyd TO THE DOCKS
	IF (eMissionStagePassed = MISSION_STAGE_SETUP
	OR eMissionStagePassed = MISSION_STAGE_1)
		vStageStart = vPlayerStart
		fstartheading = fPlayerStart
	//PLAYER PARKS UP AND GETS CHANGED
	ELIF eMissionStagePassed = MISSION_STAGE_2
		vStageStart = << 24.4835, -2536.4578, 5.0410 >>
		fstartheading = 55.2078
	ELIF eMissionStagePassed = MISSION_STAGE_4
		vStageStart = << -61.5066, -2525.5120, 5.0101 >>
		fstartheading = 56.9516
	//FORKLIFT AND TASKS
	ELIF eMissionStagePassed = MISSION_STAGE_5
		vStageStart = <<-125.5439, -2421.8813, 5.0005>> 
		fstartheading = 49.0800 
	//FOLLOW floyd TO THE CRANE
	ELIF eMissionStagePassed = MISSION_STAGE_6
		vStageStart = <<-88.8838, -2451.5679, 5.0175>> 
		fstartheading = 317.2729
	//CRANE SECTION
	ELIF eMissionStagePassed = MISSION_STAGE_7
		vStageStart = << -50.2985, -2403.9036, 5.0003 >> 
		fstartheading = 191.2160
	//USE CAMERA
	ELIF eMissionStagePassed = MISSION_STAGE_8
		vStageStart = << -114.5969, -2406.8113, 22.2282 >>
		fstartheading = 359.8527
	//DRIVE TO OTHER DOCKS
	ELIF eMissionStagePassed = MISSION_STAGE_10
		vStageStart = <<-120.9023, -2411.2688, 5.0007>>
		fstartheading = 195.5543
	//DRIVE TO OTHER DOCKS
	ELIF eMissionStagePassed = MISSION_STAGE_11
		vStageStart =  <<-120.9023, -2411.2688, 5.0007>>
		fstartheading = 195.5543
	ENDIF
	IF IS_SCREEN_FADED_OUT()
		//LOAD_SCENE(vStageStart)
		CLEAR_PED_TASKS(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			CLEAR_PED_WETNESS(PLAYER_PED_ID())
		ENDIF
		WARP_PLAYER(vStageStart, fstartheading)
	ENDIF
ENDPROC

// ======================================================================
//                        Init Arrays
// ======================================================================

//PURPOSE: defines up the individual ped details
PROC INITALISE_ARRAYS() // defines up the individual ped details
	
	s_crane.v_cabin_attach_offset = <<0.0, 0.0, 18.000>>
	
	//Guards
	//Patrols
	//Merc 0
	vMercStart[0] = <<-104.07, -2374.69, 8.3191>>
	fMercStart[0] = 93.83
	vMercPatrol[0][0] = <<-104.07, -2374.69, 8.3191>> 
	vMercPatrol[0][1] = <<-147.40, -2378.30, 8.3191>> 
	vMercPatrol[0][2] = <<-104.07, -2374.69, 8.3191>> 
	vMercPatrol[0][3] = <<-147.40, -2378.30, 8.3191>>
	//Merc 1
	vMercStart[1] = <<-212.2350, -2377.4456, 16.3326>>  
	fMercStart[1] = 94.3986
	vMercPatrol[1][0] = <<-212.2350, -2377.4456, 16.3326>>
	vMercPatrol[1][1] = <<-231.7264, -2377.5935, 16.3319>>
	vMercPatrol[1][2] = <<-232.1804, -2374.6602, 16.3308>> 
	vMercPatrol[1][3] = <<-231.7264, -2377.5935, 16.3319>>
	//Merc 2
	vMercStart[2] = <<-248.9245, -2376.2087, 8.3191>> 
	fMercStart[2] = 274.0258
	vMercPatrol[2][0] = <<-248.9245, -2376.2087, 8.3191>>  
	vMercPatrol[2][1] = <<-209.7266, -2378.2537, 8.3191>>
	vMercPatrol[2][2] = <<-186.3949, -2377.6677, 8.3191>> 
	vMercPatrol[2][3] = <<-163.5379, -2378.0491, 8.3191>>
	//Merc 3
	vMercStart[3] = << -123.4885, -2352.8262, 8.3191 >>
	fMercStart[3] = 3.7757
	vMercPatrol[3][0] = << -151.3500, -2408.0063, 5.0005 >> 
	vMercPatrol[3][1] = << -102.0827, -2408.0342, 5.0005 >> 
	vMercPatrol[3][2] = << -102.2400, -2423.9700, 5.0005 >> 
	vMercPatrol[3][3] = << -61.0789, -2423.8125, 5.0012 >>
	
	//at other docks
	
	vMercStart[4] = <<483.60, -3119.91, 5.0696>>
	fMercStart[4] = -11.93
	
	vMercStart[5] = <<484.6697, -3110.7073, 5.2948>>
	fMercStart[5] = 1.5744
	
	vMercStart[6] = <<482.53, -3119.30, 6.07>>
	fMercStart[6] = 44.12
	
	//On boat
	vMercStart[7] = <<-124.56, -2371.68, 9.32>>
	fMercStart[7] = -75.45
	
	vMercStart[8] = <<482.53, -3119.30, 6.07>>
	fMercStart[8] = 44.12
	
	//Dock Workers 
	
	//in office - 0
	vDockWorkerStart[0] = << -151.3500, -2408.0063, 5.0005 >> 
	fDockWorkerStart[0] = 268.7411
	vDockPatrol[0][0] = << -151.3500, -2408.0063, 5.0005 >> 
	vDockPatrol[0][1] = << -102.0827, -2408.0342, 5.0005 >> 
	vDockPatrol[0][2] = << -102.2400, -2423.9700, 5.0005 >> 
	vDockPatrol[0][3] = << -61.0789, -2423.8125, 5.0012 >>  

	//patrolling round
	vDockWorkerStart[1] = << -43.6959, -2520.3784, 6.3988 >> 
	fDockWorkerStart[1] = 321.2447
	
	//patrol length of dock
	vDockWorkerStart[2] = << -176.3674, -2408.1582, 5.0007 >>
	fDockWorkerStart[2] = 271.7324
	
	//Sweeping up crap by the tanker
	vDockWorkerStart[3] = << -62.19, -2539.51, 6.01 >> 
	fDockWorkerStart[3] = -157.31
	
	//Guys chatting by crates lean
	vDockWorkerStart[4] = << -56.7946, -2448.8533, 6.2398 >>
	fDockWorkerStart[4] = 66.4825
	
	//Guys chatting by crates stood talking
	vDockWorkerStart[5] = << 14.2126, -2507.8977, 5.0067 >>
	fDockWorkerStart[5] = 102.599983
	
	//Welding guy
	vDockWorkerStart[6] =  << -121.86, -2451.69, 5.02 >>
	fDockWorkerStart[6] = -5.80
	
	//Behind Truck - SUPERVISOR 2
	vDockWorkerStart[7] = << -133.5132, -2450.7058, 5.0167 >>
	fDockWorkerStart[7] =  321.2153
	
	//On phone by doorway
	vDockWorkerStart[8] = << -104.7430, -2496.9646, 5.0058 >>
	fDockWorkerStart[8] = 319.1118
	
	//Dealing with fuse box
	vDockWorkerStart[9] =  << -124.0598, -2479.0505, 5.0196 >>
	fDockWorkerStart[9] = 329.9548
	
	//Circle crates 2 - PATROL
	vDockWorkerStart[10] = << -72.4153, -2485.4336, 5.0334 >> 
	fDockWorkerStart[10] =  52.6711
	vDockPatrol[10][0] = << -72.4153, -2485.4336, 5.0334 >> 
	vDockPatrol[10][1] =  << -96.9927, -2468.2859, 5.0208 >> 
	vDockPatrol[10][2] =  << -88.6509, -2456.4009, 5.0196 >>
	vDockPatrol[10][3] = << -135.2554, -2449.3057, 5.0170 >>
	
	//To Drive Truck
	vDockWorkerStart[11] = << -115.3376, -2517.0283, 5.0005 >>
	fDockWorkerStart[11] = 245.6216
	
	//To Drive Forklift
	vDockWorkerStart[12] = << -106.1724, -2464.8291, 5.0208 >> 
	fDockWorkerStart[12] = 125.7997
	
	//By crates - angry
	vDockWorkerStart[13] = << -98.68, -2467.15, 6.02 >>
	fDockWorkerStart[13] = -7.74
	
	//By crates - starts by office
	vDockWorkerStart[14] = <<-67.21, -2437.73, 7.25 >> 
	fDockWorkerStart[14] = 81.13
	
	//At other docks
	vDockWorkerStart[15] = << 481.98, -3052.50, 6.23 >>
	fDockWorkerStart[15] = -0.95
	
	vDockWorkerStart[16] = << 489.35, -3050.57, 6.12 >> 
	fDockWorkerStart[16] = 346.3188
	
	//to drive biffs at start of docks
	vDockWorkerStart[17] = << 488.6217, -3050.3328, 5.1097 >> 
	fDockWorkerStart[17] = 2.1321
	
	vDockWorkerStart[18] = << 479.67, -3049.37, 5.1097 >> 
	fDockWorkerStart[18] = 2.1321
	
	//inside hut SUPERVISOR 1
	vDockWorkerStart[19] = << -227.7128, -2426.8201, 5.1497 >>
	fDockWorkerStart[19] = 327.6013 
	
	//MILITARY DOCKS - by final vehilce
	vDockWorkerStart[20] = << 463.81, -3040.37, 6.07 >>
	fDockWorkerStart[20] = 327.6013
	
	//MILITARY DOCKS - By Blocking vehicle
	vDockWorkerStart[21] = << 493.09, -3047.20, 6.11 >>
	fDockWorkerStart[21] = 135.22
	
	//BIFF to be driven
	vDockVeh[0]= << -104.8244, -2513.3738, 4.5183 >>
	fDockVeh[0]= 56.5885 
	
	//FORKLIFT to be driven
	vDockVeh[1]=  << -104.7854, -2465.9553, 5.0208 >>
	fDockVeh[1]= 235.9101
	
	//UPON ENTERING THE DOCKS AMBIENT STUFF - BIFF 1
	vDockVeh[2] = << -69.5243, -2505.9260, 5.0075 >>
	fDockVeh[2] = 324.8585
	
	//UPON ENTERING THE DOCKS AMBIENT STUFF - BIFF 2
	vDockVeh[3] = << -39.1900, -2449.1589, 5.0043 >>
	fDockVeh[3] = 144.4548
	/*
	 ____ ___  _ _ _  __ ___  _  _  _ _ __ ___ __    __  __ ___ _  _ ___   __   _ ___ _  
	/ _\ V / \| | U |/ _| o \/ \| \| | / _| __|  \  / _|/ _| __| \| | __| |  \ / \_ _/ \ 
	\_ \\ /| \\ |   ( (_|   ( o ) \\ | \_ \ _|| o ) \_ ( (_| _|| \\ | _|  | o ) o | | o |
	|__/|_||_|\_|_n_|\__|_|\\\_/|_|\_|_|__/___|__/  |__/\__|___|_|\_|___| |__/|_n_|_|_n_|*/
	
	//BLACKWATER GUYS WITH DOCKWORKER
	
    s_sp_blackwater_with_dockworker[0].model = S_M_M_DOCKWORK_01
	s_sp_blackwater_with_dockworker[1].model = S_M_Y_BlackOps_01
	s_sp_blackwater_with_dockworker[2].model = S_M_Y_BlackOps_01
	
	//Fill these
	s_sp_blackwater_with_dockworker[0].v_spawn_position =  << -69.5243, -2505.9260, 5.0075 >>
	s_sp_blackwater_with_dockworker[0].f_start_heading = 0.0
	s_sp_blackwater_with_dockworker[1].v_spawn_position =  << -69.5243, -2505.9260, 5.0075 >>
	s_sp_blackwater_with_dockworker[1].f_start_heading = 0.0
	s_sp_blackwater_with_dockworker[2].v_spawn_position =  << -69.5243, -2505.9260, 5.0075 >>
	s_sp_blackwater_with_dockworker[2].f_start_heading = 0.0
	
	//SUPERVISOR IN HUT
	s_sp_supervisor1[0].model = S_M_M_DOCKWORK_01
	s_sp_supervisor1[0].v_spawn_position = << -62.2290, -2518.5679, 6.4002 >>
	s_sp_supervisor1[0].f_start_heading = 143.9633
	
	//GUY WHO TELLS YOU TO USE FORKLIFT
	s_sp_forklift_supervisorA[0].model = S_M_M_DOCKWORK_01
	s_sp_forklift_supervisorA[0].v_spawn_position = <<-122.25, -2419.58, 6.00>>
	s_sp_forklift_supervisorA[0].f_start_heading =  179.2108
	
	//GUY WHO TELLS YOU TO PICK UP CRATE
	s_sp_forklift_supervisorB[0].model = S_M_M_DOCKWORK_01
	s_sp_forklift_supervisorB[0].v_spawn_position = << -96.7150, -2455.2817, 5.0189 >> 
	s_sp_forklift_supervisorB[0].f_start_heading = 225.5408
	
	//DOCK WORKERS TALKING	
	//IG 5 Dockworker 2
	s_sp_dock_workers_talking[0].model = S_M_M_DOCKWORK_01
	s_sp_dock_workers_talking[0].v_spawn_position = <<-110.41, -2481.24, 6.02>>
	s_sp_dock_workers_talking[0].f_start_heading= -109.52
	//IG 5 Dockworker 1
	s_sp_dock_workers_talking[1].model = S_M_M_DOCKWORK_01
	s_sp_dock_workers_talking[1].v_spawn_position = << -108.83, -2482.12, 6.02 >>
	s_sp_dock_workers_talking[1].f_start_heading= -88.92
	
	//GANTRY CRANE GUYS SETPIECE
	s_sp_gantry_guys_dynamic[0].model = S_M_M_DOCKWORK_01
	s_sp_gantry_guys_dynamic[1].model = S_M_M_DOCKWORK_01
	s_sp_gantry_guys_dynamic[2].model = S_M_M_DOCKWORK_01
	s_sp_gantry_guys_dynamic[3].model = S_M_M_DOCKWORK_01
	
	s_sp_gantry_guys_dynamic[0].v_spawn_position = << -69.5243, -2505.9260, 5.0075 >>
	s_sp_gantry_guys_dynamic[1].v_spawn_position = << -69.5243, -2505.9260, 5.0075 >>
	s_sp_gantry_guys_dynamic[2].v_spawn_position = << -69.5243, -2505.9260, 5.0075 >>
	s_sp_gantry_guys_dynamic[3].v_spawn_position = << -69.5243, -2505.9260, 5.0075 >>
	
	s_sp_gantry_guys_dynamic[0].f_start_heading = 143.9633
	s_sp_gantry_guys_dynamic[1].f_start_heading = 143.9633
	s_sp_gantry_guys_dynamic[2].f_start_heading = 143.9633
	s_sp_gantry_guys_dynamic[3].f_start_heading = 143.9633
	
	//GANTRY CRANE GUYS AT THE FRONT
	s_sp_gantry_guys_static[0].model = S_M_M_DOCKWORK_01
	s_sp_gantry_guys_static[1].model = S_M_M_DOCKWORK_01
	
	s_sp_gantry_guys_static[0].v_spawn_position = << -69.5243, -2505.9260, 5.0075 >>
	s_sp_gantry_guys_static[1].v_spawn_position = << -69.5243, -2505.9260, 5.0075 >>
	
	s_sp_gantry_guys_static[0].f_start_heading = 143.9633
	s_sp_gantry_guys_static[1].f_start_heading = 143.9633
	
	//WELDER
	s_sp_welder[0].model = S_M_M_DOCKWORK_01
	s_sp_welder[0].v_spawn_position =  << -107.9554, -2465.7800, 7.8185 >>
	s_sp_welder[0].f_start_heading = 331.6483
	
	//GUYS AROUND CAR
	s_sp_guys_around_car[0].model = S_M_M_DOCKWORK_01
	s_sp_guys_around_car[1].model = S_M_M_DOCKWORK_01
	s_sp_guys_around_car[2].model = S_M_M_DOCKWORK_01
	s_sp_guys_around_car[3].model = S_M_M_DOCKWORK_01
	s_sp_guys_around_car[4].model = S_M_M_DOCKWORK_01
	
	s_sv_car_to_admire[0].model = BUFFALO
	s_sv_car_to_admire[0].v_spawn_position = << -109.5377, -2519.4890, 5.0000 >>
	s_sv_car_to_admire[0].f_start_heading = 51.6828
	
	s_sp_guys_around_car[0].v_spawn_position =  << -105.6039, -2520.6841, 5.0000 >>
	s_sp_guys_around_car[0].f_start_heading =10
	s_sp_guys_around_car[1].v_spawn_position =  << -107.4489, -2523.3547, 5.0000 >>
	s_sp_guys_around_car[1].f_start_heading = 22
	s_sp_guys_around_car[2].v_spawn_position =  << -111.8440, -2520.2720, 5.0000 >>
	s_sp_guys_around_car[2].f_start_heading = 55
	s_sp_guys_around_car[3].v_spawn_position =  << -110.1655, -2516.7170, 5.0000 >>
	s_sp_guys_around_car[3].f_start_heading = 43
	s_sp_guys_around_car[4].v_spawn_position =  << -114.7683, -2516.2803, 5.0999 >>
	s_sp_guys_around_car[4].f_start_heading =84
	
	//DOCKWORKERS WORKING ON PIPE
	
	s_sp_dock_workers_on_pipe[0].model = S_M_M_DOCKWORK_01
	s_sp_dock_workers_on_pipe[1].model = S_M_M_DOCKWORK_01
	s_sp_dock_workers_on_pipe[2].model = S_M_M_DOCKWORK_01
	
	s_sp_dock_workers_on_pipe[0].v_spawn_position = << -77.0331, -2527.0872, 5.0101 >>
	s_sp_dock_workers_on_pipe[1].v_spawn_position = << -75.1044, -2529.4731, 5.0101 >>
	s_sp_dock_workers_on_pipe[2].v_spawn_position = << -73.6988, -2529.5952, 5.0101 >>
	
	s_sp_dock_workers_on_pipe[0].f_start_heading = 20
	s_sp_dock_workers_on_pipe[1].f_start_heading = 60
	s_sp_dock_workers_on_pipe[2].f_start_heading = 266
	
	//FLOYD GRABS PACKAGE
	s_sp_floyd_grabs_package[0].model = S_M_M_DOCKWORK_01
	s_sp_floyd_grabs_package[1].model = S_M_M_DOCKWORK_01
	s_sp_floyd_grabs_package[2].model = S_M_M_DOCKWORK_01
	
	//PIPE GUYS
	s_sp_dock_workers_on_pipe[0].v_ss_pos = << -68.320, -2531.518, 5.060 >> //<<-62.118767,-2519.220947,7.414408>>
	s_sp_dock_workers_on_pipe[0].f_ss_rot = 3.640 //-170.750000
	s_sp_dock_workers_on_pipe[1].v_ss_pos = << -68.320, -2531.518, 5.060 >>
	s_sp_dock_workers_on_pipe[1].f_ss_rot = 3.640
	s_sp_dock_workers_on_pipe[2].v_ss_pos = << -68.320, -2531.518, 5.060 >>
	s_sp_dock_workers_on_pipe[2].f_ss_rot = 3.640
	
	//GANTRY
	s_sp_gantry_guys_dynamic[0].v_ss_pos = <<-115.7235, -2477.5984, 22.2266>>
	s_sp_gantry_guys_dynamic[1].v_ss_pos = <<-117.2564, -2479.7366, 22.2266>>
	s_sp_gantry_guys_dynamic[2].v_ss_pos = <<-114.1917, -2475.4312, 22.2266>>
	s_sp_gantry_guys_dynamic[3].v_ss_pos = <<-117.7939, -2480.5886, 22.2266>>
	
	//SCENE DATA
	
	s_sd_arrive_at_docks.v_scene_pos = << -60.173, -2525.329, 4.925 >>
	s_sd_arrive_at_docks.v_scene_rot = << 0.000, 0.000, 59.250 >>
	
	s_sd_floyd_grabs_package.v_scene_pos = << -88.341, -2502.575, 5.011 >>
	s_sd_floyd_grabs_package.v_scene_rot = << 0.000, -0.000, -32.844 >>
	
	s_sd_dock_workers_on_pipe.v_scene_pos = << -68.929, -2530.365, 5.126 >>
	s_sd_dock_workers_on_pipe.v_scene_rot =  << 0.000, 0.000, 46.000 >>
	
	s_sd_blackwater_with_dockworker.v_scene_pos = << -110.715, -2429.089, 5.001 >>
	s_sd_blackwater_with_dockworker.v_scene_rot = << 0.000, 0.000, 91.000 >>
	
	s_sd_forklift_supervisor_a.v_scene_pos = << -131.496, -2423.041, 5.000 >>
	s_sd_floyd_grabs_package.v_scene_rot = << 0.000, 0.000, 63.720 >>
	
	s_sd_gantry_guys_dynamic.v_scene_pos = << -116.113, -2475.791, 22.206 >>
	s_sd_gantry_guys_dynamic.v_scene_rot = << 0.000, 0.000, 145.520 >>
	
	s_sd_gantry_guys_static.v_scene_pos = << -29.053, -2481.937, 22.205 >>
	s_sd_gantry_guys_static.v_scene_rot = << 0.000, 0.000, 147.280 >>
	
	s_sd_welder.v_scene_pos = << -108.066, -2464.675, 5.020 >>
	s_sd_welder.v_scene_rot = << 0.000, 0.000, 55.000 >>

	//ARRIVE SETUP
	s_sp_supervisor1[0].v_ss_pos = <<-62.118767,-2519.220947,7.414408>>
	s_sp_supervisor1[0].f_ss_rot =	-170.750000
	
	v_ss_floyd[SS_FLOYD_GRABS_PACKAGE] = <<-76.45, -2508.30, 6.01>>
	f_ss_floyd[SS_FLOYD_GRABS_PACKAGE] = -38.15
	
	//for the skip menu
	#IF IS_DEBUG_BUILD
		SkipMenuStruct[0].sTxtLabel		= "Initial Mission"
		SkipMenuStruct[0].bSelectable	= FALSE
		SkipMenuStruct[1].sTxtLabel		= "Opening Mocap"
		SkipMenuStruct[2].sTxtLabel		= "Drive to docks | LSDH_INT"
		SkipMenuStruct[3].sTxtLabel	 	= "Park up and check in"
		SkipMenuStruct[4].sTxtLabel	 	= "Follow Floyd around the docks"
		SkipMenuStruct[5].sTxtLabel	 	= "Handler Task"
		SkipMenuStruct[6].sTxtLabel		= "Follow Floyd to the crane"
		SkipMenuStruct[7].sTxtLabel	 	= "Use the crane | LSDHS_MCS_1"
		SkipMenuStruct[8].sTxtLabel	 	= "Take photos of the boat"
		SkipMenuStruct[9].sTxtLabel	    = "Drive to the Docks | LSDHS_MCS_2"
		SkipMenuStruct[10].sTxtLabel	= "Return to Floyd's apartment"
	#ENDIF	
ENDPROC

// ======================================================================
//                        Tech Procs
// ======================================================================


// ╚══════════════════════════╡	 	Misc Speech Functions   	╞════════════════════════════╝   
//PURPOSE:  Checks to see if text of speech has been triggered

PROC DISABLE_COMBAT_CONTROLS_THIS_FRAME()
	
	WEAPON_TYPE playerWeap
	IF NOT GET_CURRENT_PED_VEHICLE_WEAPON(PLAYER_PED_ID(),playerWeap)
		IF GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), playerWeap)
			IF eMissionStage != MISSION_STAGE_8
				IF playerWeap != WEAPONTYPE_UNARMED
				AND NOT IS_PHONE_ONSCREEN()
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PREV_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
	//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
	//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
	//DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
ENDPROC

//PURPOSE:  Displays the text at the correct location
PROC MANAGE_HELP_TEXT_UPDATE()
	//Help Text
	IF entHelpText != NULL
		IF DOES_ENTITY_EXIST(entHelpText)
			IF NOT IS_ENTITY_DEAD(entHelpText)
				IF IS_ENTITY_A_PED(entHelpText)
					vHelpText = GET_PED_BONE_COORDS(GET_PED_INDEX_FROM_ENTITY_INDEX(entHelpText), BONETAG_ROOT, <<0.0, 0.0, 0.0>>) + <<0.0, 0.0, 1.0>>
				ELIF IS_ENTITY_A_VEHICLE(entHelpText)
					vHelpText = GET_ENTITY_COORDS(entHelpText) + <<0.0, 0.0, 1.0>>
				ELSE
					vHelpText = GET_ENTITY_COORDS(entHelpText) + <<0.0, 0.0, 1.0>>
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_STRING_NULL(sHelpText)
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sHelpText)
			IF NOT ARE_VECTORS_EQUAL(vHelpText, <<0.0, 0.0, 0.0>>)
				IF vHelpText.Z != 0.0
            		SET_HELP_MESSAGE_WORLD_POSITION(vHelpText)
				ELSE
					SET_HELP_MESSAGE_SCREEN_POSITION(vHelpText.X, vHelpText.Y)
				ENDIF
				
				SET_HELP_MESSAGE_STYLE(HELP_MESSAGE_STYLE_TAGGABLE, HUD_COLOUR_NET_PLAYER15, 80, eArrowDirection)
			ENDIF
			
			IF GET_GAME_TIMER() > iHelpDuration
			AND iHelpDuration <> -1
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


/// PURPOSE:
///    Plays a single line of dialogue, this is used for placeholder dialogue only so that the timings can be tweaked in script prior to the real dialogue being ready.
/// PARAMS:
///    s_text_block - The label for the entire block of text
///    s_dialogue_block - The label for the block of dialogue that contains the particular line
///    s_dialogue_line - The label for the particular line
///    i_duration - The length of time that this line should play for.
/// RETURNS:
///    Bool indicating whether playback was successful.
FUNC BOOL PLAY_PLACEHOLDER_DIALOGUE_LINE(STRING s_text_block, STRING s_dialogue_block, STRING s_dialogue_line, INT i_duration, BOOL b_only_play_once = TRUE)
	IF PLAY_SINGLE_LINE_FROM_CONVERSATION(s_conversation_peds, s_text_block, s_dialogue_block, s_dialogue_line, CONV_PRIORITY_MEDIUM)
		SET_LABEL_AS_TRIGGERED(s_dialogue_line, b_only_play_once)
		i_placeholder_dialogue_timer = GET_GAME_TIMER()
		i_current_dialogue_duration = i_duration
		RETURN TRUE
	ELSE
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE() //In case this overlaps another line
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Kills any current placeholder dialogue playing when it's played for a long enough time
PROC MANAGE_PLACEHOLDER_DIALOGUE()
	IF i_placeholder_dialogue_timer != 0 
		IF GET_GAME_TIMER() - i_placeholder_dialogue_timer > i_current_dialogue_duration
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			i_placeholder_dialogue_timer = 0
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops any placeholder dialogue immediately
PROC FORCE_RESET_PLACEHOLDER_DIALOGUE()
	KILL_FACE_TO_FACE_CONVERSATION()
	i_placeholder_dialogue_timer = 0
	i_current_dialogue_duration = 0
ENDPROC


// ╚═════════════════════════════════════════════════════════════════════════════════╝
// ╚══════════════════════════╡	 	Controls  		╞════════════════════════════╝    
// ╚═════════════════════════════════════════════════════════════════════════════════╝


PROC CONVERGE_VALUE_WITH_DECEL(FLOAT &val, FLOAT desired_val, FLOAT max_val, FLOAT min_amount_to_converge, FLOAT max_amount_to_converge, FLOAT decel_range = 1.0)
	IF val != desired_val
		FLOAT val_diff = ABSF(val - desired_val) / max_val
		FLOAT new_converge_value = max_amount_to_converge
		
		IF val_diff < decel_range
			new_converge_value = (val_diff / decel_range) * max_amount_to_converge
		ENDIF

		IF new_converge_value < min_amount_to_converge
			new_converge_value = min_amount_to_converge
		ENDIF
		
		CONVERGE_VALUE(val, desired_val, new_converge_value)
	ENDIF
ENDPROC

PROC SET_PED_POSITION(PED_INDEX pedIndex, VECTOR vCoords, FLOAT fHeading, BOOL bKeepVehicle = TRUE)
	IF bKeepVehicle = TRUE
		SET_PED_COORDS_KEEP_VEHICLE(pedIndex, vCoords)
		SET_ENTITY_HEADING(pedIndex, fHeading)
	ELSE
		SET_ENTITY_COORDS(pedIndex,vCoords,TRUE)
	ENDIF
ENDPROC

FUNC BOOL SAFE_DEATH_CHECK_PED(PED_INDEX &pedIndex)
	IF DOES_ENTITY_EXIST(pedIndex)
		IF IS_ENTITY_DEAD(pedIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SAFE_DEATH_CHECK_VEHICLE(VEHICLE_INDEX &vehIndex)
	IF DOES_ENTITY_EXIST(vehIndex)
		IF IS_ENTITY_DEAD(vehIndex)
		OR NOT IS_VEHICLE_DRIVEABLE(vehIndex)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC SAFE_ADD_BLIP_LOCATION(BLIP_INDEX &blipIndex, VECTOR vCoords, BOOL bRoute = FALSE)
	IF NOT DOES_BLIP_EXIST(blipIndex)
		blipIndex = ADD_BLIP_FOR_COORD(vCoords)
		SET_BLIP_ROUTE(blipIndex, bRoute)
	ENDIF
ENDPROC

PROC SAFE_ADD_BLIP_PED(BLIP_INDEX &blipIndex, PED_INDEX &pedIndex, BOOL bFriendly = FALSE)
	IF NOT DOES_BLIP_EXIST(blipIndex)
		IF DOES_ENTITY_EXIST(pedIndex)
			IF NOT IS_ENTITY_DEAD(pedIndex)
				blipIndex = CREATE_BLIP_FOR_ENTITY(pedIndex, NOT bFriendly)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_ADD_BLIP_VEHICLE(BLIP_INDEX &blipIndex, VEHICLE_INDEX &vehIndex, BOOL bFriendly = FALSE)
	IF NOT DOES_BLIP_EXIST(blipIndex)
		IF DOES_ENTITY_EXIST(vehIndex)
			IF NOT IS_ENTITY_DEAD(vehIndex)
				blipIndex = ADD_BLIP_FOR_ENTITY(vehIndex)
				SET_BLIP_AS_FRIENDLY(blipIndex, bFriendly)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SAFE_REMOVE_BLIP(BLIP_INDEX &blipIndex)
	IF DOES_BLIP_EXIST(blipIndex)
		REMOVE_BLIP(blipIndex)
	ENDIF
ENDPROC

PROC SPAWN_PED(PED_INDEX &pedIndex, MODEL_NAMES eThisModel, VECTOR vStart, FLOAT fStart = 0.0, BOOL bDefault = TRUE)
	REQUEST_MODEL(eThisModel)
	WHILE NOT HAS_MODEL_LOADED(eThisModel)
		WAIT (0)
	ENDWHILE
	IF NOT DOES_ENTITY_EXIST(pedIndex)
		pedIndex = CREATE_PED(PEDTYPE_MISSION, eThisModel, vStart, fStart)
		IF bDefault = TRUE
			SET_PED_DEFAULT_COMPONENT_VARIATION(pedIndex)
		ENDIF
	ENDIF
ENDPROC

PROC SPAWN_BUDDY(PED_INDEX &pedIndex, MODEL_NAMES eThisModel, VECTOR vStart, FLOAT fStart = 0.0, BOOL bDefault = TRUE, WEAPON_TYPE WpnIndex = WEAPONTYPE_UNARMED, BOOL bPassenger = TRUE,STRING sDialogue = NULL,INT iSpeaker = 0)
	REQUEST_MODEL(eThisModel)
	IF HAS_MODEL_LOADED(eThisModel)
		IF NOT DOES_ENTITY_EXIST(pedIndex)
			pedIndex = CREATE_PED(PEDTYPE_MISSION, eThisModel, vStart, fStart)
		ENDIF
		IF NOT IS_PED_INJURED(pedIndex)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedIndex, TRUE)
			GIVE_WEAPON_TO_PED(pedIndex, WpnIndex, 800, TRUE)
			SET_PED_CAN_BE_TARGETTED(pedIndex, FALSE)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedIndex, FALSE)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedIndex ,rel_group_buddies)
			SET_ENTITY_PROOFS(pedIndex, FALSE, FALSE, FALSE, FALSE, FALSE)
			//SET_PED_SUFFERS_CRITICAL_HITS(pedIndex ,FALSE)
			SET_ENTITY_HEALTH(pedIndex,200)
			IF bPassenger = TRUE
				//SET_PED_GROUP_MEMBER_PASSENGER_INDEX(pedIndex, VS_FRONT_RIGHT)
			ELSE
				//SET_PED_AS_GROUP_MEMBER(pedIndex, PLAYER_GROUP_ID())
			ENDIF
			SET_PED_AS_ENEMY(pedIndex , FALSE)
			SET_ENTITY_AS_MISSION_ENTITY(pedIndex)
			SET_PED_DEFAULT_COMPONENT_VARIATION(pedIndex)
			IF bDefault = TRUE
				SET_PED_DEFAULT_COMPONENT_VARIATION(pedIndex)
			ENDIF
			
			IF NOT IS_STRING_NULL_OR_EMPTY(sDialogue)
				ADD_PED_FOR_DIALOGUE(sSpeech,iSpeaker,pedIndex,sDialogue)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SPAWN_VEHICLE(VEHICLE_INDEX &vehIndex, MODEL_NAMES eThisModel, VECTOR vStart, FLOAT fStart = 0.0, INT iColour = -1, FLOAT fDirt = 0.0,BOOL bConsidered = TRUE)
	REQUEST_MODEL(eThisModel)
	WHILE NOT HAS_MODEL_LOADED(eThisModel)
		WAIT (0)
	ENDWHILE
	IF NOT DOES_ENTITY_EXIST(vehIndex)
		vehIndex = CREATE_VEHICLE(eThisModel, vStart, fStart)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehIndex,TRUE)
	ELSE
		IF IS_VEHICLE_DRIVEABLE(vehIndex)
			IF iColour >= 0
				SET_VEHICLE_COLOURS(vehIndex, iColour, iColour)
			ENDIF
			IF fDirt >= 0
				SET_VEHICLE_DIRT_LEVEL(vehIndex, fDirt)
			ENDIF
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehIndex,bConsidered)
		ENDIF
	ENDIF
ENDPROC

PROC SPAWN_OBJECT(OBJECT_INDEX &objIndex, MODEL_NAMES eThisModel, VECTOR vStart, FLOAT fStart = 0.0,BOOL bNetworkObject = TRUE, BOOL bScriptHostObject = TRUE)
	IF NOT DOES_ENTITY_EXIST(objIndex)
		objIndex = CREATE_OBJECT(eThisModel, vStart, bNetworkObject, bScriptHostObject)
		SET_ENTITY_HEADING(objIndex,fStart)
	ENDIF
ENDPROC

FUNC PED_INDEX CREATE_SETPIECE_PED(MODEL_NAMES model, VECTOR v_pos, FLOAT f_heading, REL_GROUP_HASH group, 
INT i_health = 200, INT i_armour = 0, WEAPON_TYPE weapon = WEAPONTYPE_UNARMED, PED_TYPE e_ped_type = PEDTYPE_MISSION)
	IF IS_VECTOR_ZERO(v_pos) 
		PRINTSTRING("VECTOR IS ZERO")PRINTNL()
	ENDIF
	PED_INDEX ped = CREATE_PED(e_ped_type, model, v_pos, f_heading)
	GIVE_WEAPON_TO_PED(ped, weapon, INFINITE_AMMO, TRUE)
	SET_PED_INFINITE_AMMO(ped, TRUE, weapon)
	SET_PED_MAX_HEALTH(ped, i_health)
	SET_ENTITY_HEALTH(ped, i_health)
	ADD_ARMOUR_TO_PED(ped, i_armour)
	SET_PED_DIES_WHEN_INJURED(ped, TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(ped, group)
	SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(ped,TRUE)
	RETURN ped

ENDFUNC

FUNC VEHICLE_INDEX CREATE_SETPIECE_VEHICLE(MODEL_NAMES model, VECTOR v_pos, FLOAT f_heading)
	VEHICLE_INDEX veh = CREATE_VEHICLE(model,v_pos,f_heading)
	RETURN veh
ENDFUNC

PROC CLEAN_UP_SETPIECE_VEHILCE(SETPIECE_VEHICLE &s_setpiece, BOOL b_reset_variables = FALSE, BOOL bFullClean = FALSE)
	
	IF bFullClean
		DELETE_VEHICLE(s_setpiece.veh)
	ELSE
		SET_VEHICLE_AS_NO_LONGER_NEEDED(s_setpiece.veh)	
	ENDIF

	IF b_reset_variables
		s_setpiece.b_is_created = FALSE
		IF s_setpiece.i_event != 0
			s_setpiece.i_event = 0
		ENDIF
	ENDIF
ENDPROC

PROC INITIALISE_SETPIECE_GROUP(SETPIECE_PED &s_setpiece[], STRING name)
	INT i = 0

	REPEAT COUNT_OF(s_setpiece) i
		IF NOT IS_PED_INJURED(s_setpiece[i].ped)
			//TASK_STAND_STILL(s_setpiece[i].ped, -1)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_setpiece[i].ped, TRUE)
			IF NOT s_setpiece[i].b_is_created = TRUE
				s_setpiece[i].b_is_created = TRUE
			ENDIF
			s_setpiece[i].i_event = 0
			IF s_setpiece[i].i_timer !=0
				s_setpiece[i].i_timer = 0
			ENDIF
			
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_31 debug_name = name
				debug_name += i
				SET_PED_NAME_DEBUG(s_setpiece[i].ped, debug_name)
			#ENDIF
			
			#IF NOT IS_DEBUG_BUILD
				name = name	//	to prevent an Unreferenced variable error
			#ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

PROC CLEAN_UP_SETPIECE_PED(SETPIECE_PED &s_setpiece, BOOL b_reset_variables = FALSE, BOOL bFullClean = FALSE)
	IF DOES_BLIP_EXIST(s_setpiece.blip)
		REMOVE_BLIP(s_setpiece.blip)
	ENDIF
	
	IF DOES_ENTITY_EXIST(s_setpiece.ped)
		IF NOT IS_PED_INJURED(s_setpiece.ped)
			CLEAR_PED_TASKS(s_setpiece.ped)
			STOP_SYNCHRONIZED_ENTITY_ANIM(s_setpiece.ped,0,TRUE)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(s_setpiece.ped)
		IF DOES_ENTITY_EXIST(s_setpiece.obj_prop)
			DETACH_ENTITY(s_setpiece.obj_prop)
			IF bFullClean
				DELETE_OBJECT(s_setpiece.obj_prop)
			ELSE
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_setpiece.obj_prop)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(s_setpiece.ped)
		IF bFullClean
			DELETE_PED(s_setpiece.ped)
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(s_setpiece.ped)	
		ENDIF
		
		REMOVE_COVER_POINT(s_setpiece.cover)
	ENDIF
	
	IF b_reset_variables
		s_setpiece.b_is_created = FALSE
		s_setpiece.i_event = 0
		s_setpiece.i_timer = 0
	ENDIF
ENDPROC

PROC createMercenary(INT i, BOOL bPatrol)
	SPAWN_PED(pedMerc[i], S_M_Y_BlackOps_01, vMercStart[i], fMercStart[i], FALSE)
	
	IF NOT IS_PED_INJURED(pedMerc[i])
		SET_PED_COMBAT_RANGE(pedMerc[i], CR_FAR)
		SET_PED_COMBAT_MOVEMENT(pedMerc[i], CM_WILLADVANCE)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedMerc[i] ,rel_group_buddies)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedMerc[i] ,TRUE)
		GIVE_WEAPON_TO_PED(pedMerc[i], wtAdRifle, INFINITE_AMMO, TRUE)
		
		IF bPatrol = TRUE
			IF bMercPatrolCreated[i] = FALSE
				TEXT_LABEL sPatrol = "miss_merc"
				sPatrol +=i
				IF NOT IS_VECTOR_ZERO(vMercPatrol[i][0])
					DELETE_PATROL_ROUTE(sPatrol)
					OPEN_PATROL_ROUTE(sPatrol)
						IF NOT IS_VECTOR_ZERO(vMercPatrol[i][0])
							ADD_PATROL_ROUTE_NODE(1, "WORLD_HUMAN_GUARD_STAND",vMercPatrol[i][0] , vMercPatrol[i][0] , GET_RANDOM_INT_IN_RANGE(500,1500))				
						ENDIF
						IF NOT IS_VECTOR_ZERO(vMercPatrol[i][1])
						ADD_PATROL_ROUTE_NODE(2, "WORLD_HUMAN_GUARD_STAND",vMercPatrol[i][1] , vMercPatrol[i][1] ,  GET_RANDOM_INT_IN_RANGE(1000,6000))
						ENDIF
						IF NOT IS_VECTOR_ZERO(vMercPatrol[i][2])
							ADD_PATROL_ROUTE_NODE(3, "WORLD_HUMAN_GUARD_STAND",vMercPatrol[i][2] , vMercPatrol[i][2] , GET_RANDOM_INT_IN_RANGE(1000,6000))
						ENDIF
						IF NOT IS_VECTOR_ZERO(vMercPatrol[i][3])
							ADD_PATROL_ROUTE_NODE(4, "WORLD_HUMAN_GUARD_STAND",vMercPatrol[i][3] , vMercPatrol[i][3] ,  GET_RANDOM_INT_IN_RANGE(1000,6000))	
						ENDIF
						ADD_PATROL_ROUTE_LINK(1, 2)
						ADD_PATROL_ROUTE_LINK(2, 3)
						ADD_PATROL_ROUTE_LINK(3, 4)
						ADD_PATROL_ROUTE_LINK(4, 1)
					CLOSE_PATROL_ROUTE()
					CREATE_PATROL_ROUTE()
					IF NOT IS_PED_INJURED(pedMerc[i])
						TASK_PATROL(pedMerc[i], sPatrol, PAS_CASUAL, TRUE)
					ENDIF
				ENDIF
				bMercPatrolCreated[i] = TRUE
			ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
			TEXT_LABEL tDebugName = "Merc "
			tDebugName += i
			SET_PED_NAME_DEBUG(pedMerc[i], tDebugName)
		#ENDIF
	ENDIF
ENDPROC

PROC createDockWorker(INT i,BOOL bPatrol = TRUE)
	IF NOT DOES_ENTITY_EXIST(pedDockWorker[i])
		IF IS_VECTOR_ZERO(vDockWorkerStart[i]) 
			PRINTSTRING("vDockWorkerStart:")PRINTINT(i)PRINTNL()
		ENDIF
		SPAWN_PED(pedDockWorker[i], S_M_M_DOCKWORK_01, vDockWorkerStart[i], fDockWorkerStart[i], FALSE)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedDockWorker[i],TRUE)
		SET_PED_CONFIG_FLAG(pedDockWorker[i],PCF_UseKinematicModeWhenStationary,TRUE)
	ELSE
		IF NOT IS_PED_INJURED(pedDockWorker[i])
			SET_PED_COMBAT_RANGE(pedDockWorker[i], CR_FAR)
			SET_PED_COMBAT_MOVEMENT(pedDockWorker[i], CM_WILLRETREAT)
			SET_PED_CONFIG_FLAG(pedDockWorker[i],PCF_CanBeAgitated,TRUE)
			
			//Temp make these dudes friendly
			SET_PED_RELATIONSHIP_GROUP_HASH(pedDockWorker[i] ,rel_group_buddies)
			//ADD SIGNAL MAN
			IF i = 7
				IF NOT IS_PED_INJURED(pedDockWorker[14])
					IF sSpeech.PedInfo[8].Index = NULL
					   	ADD_PED_FOR_DIALOGUE(sSpeech, 8, pedDockWorker[14], "SIGNALMAN")
					ENDIF
			    ENDIF
			ENDIF
		
			IF bPatrol = TRUE
				IF bPatrolCreated[i] = FALSE
					TEXT_LABEL sPatrol = "miss_dock"
					sPatrol +=i
					IF NOT IS_VECTOR_ZERO(vDockPatrol[i][0])
						DELETE_PATROL_ROUTE(sPatrol)
						OPEN_PATROL_ROUTE(sPatrol)
							IF NOT IS_VECTOR_ZERO(vDockPatrol[i][0])
								ADD_PATROL_ROUTE_NODE(1, "StandGuard",vDockPatrol[i][0] , vDockPatrol[i][0] , 1000)				
							ENDIF
							IF NOT IS_VECTOR_ZERO(vDockPatrol[i][1])
								ADD_PATROL_ROUTE_NODE(2, "StandGuard",vDockPatrol[i][1] , vDockPatrol[i][1] , 1000)
							ENDIF
							IF NOT IS_VECTOR_ZERO(vDockPatrol[i][2])
								ADD_PATROL_ROUTE_NODE(3, "StandGuard",vDockPatrol[i][2] , vDockPatrol[i][2] , 1000)
							ENDIF
							IF NOT IS_VECTOR_ZERO(vDockPatrol[i][3])
								ADD_PATROL_ROUTE_NODE(4, "StandGuard",vDockPatrol[i][3] , vDockPatrol[i][3] , 1000)	
							ENDIF
							ADD_PATROL_ROUTE_LINK(1, 2)
							ADD_PATROL_ROUTE_LINK(2, 3)
							ADD_PATROL_ROUTE_LINK(3, 4)
							ADD_PATROL_ROUTE_LINK(4, 1)
						CLOSE_PATROL_ROUTE()
						CREATE_PATROL_ROUTE()
						IF NOT IS_PED_INJURED(pedDockWorker[i])
							TASK_PATROL(pedDockWorker[i], sPatrol, PAS_CASUAL, TRUE)
						ENDIF
					ENDIF
					bPatrolCreated[i] = TRUE
				ENDIF
			ENDIF

			#IF IS_DEBUG_BUILD
				TEXT_LABEL tDebugName = "Dock Worker"
				tDebugName += i
				SET_PED_NAME_DEBUG(pedDockWorker[i], tDebugName)
			#ENDIF
		
		ENDIF
	ENDIF
ENDPROC

PROC createDockWorkersNow()
	INT i
	
	REPEAT 3 i
		createDockWorker(i)
	ENDREPEAT

ENDPROC

PROC createMercenaries()
	INT i
	
	REPEAT 3 i
		IF NOT DOES_ENTITY_EXIST(pedMerc[i])
			createMercenary(i)
		ENDIF
	ENDREPEAT
	
	//Patrols
	IF NOT IS_PED_INJURED(pedMerc[0])
		TASK_PATROL(pedMerc[0], "miss_merc0", PAS_CASUAL, TRUE)
	ENDIF
	
	IF NOT IS_PED_INJURED(pedMerc[1])
		TASK_PATROL(pedMerc[1], "miss_merc1", PAS_CASUAL, TRUE)
	ENDIF
	
	IF NOT IS_PED_INJURED(pedMerc[2])
		TASK_PATROL(pedMerc[2], "miss_merc2", PAS_CASUAL, TRUE)
	ENDIF
ENDPROC

// PURPOSE: If player is in a bike/car, remove player control 'til the car is stopped 
PROC STOP_PLAYER_CAR(BOOL bAndLeave = FALSE,INT iDelay = 0)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
			FLOAT playerCarSpeed
			VEHICLE_INDEX playerCar
			playerCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF IS_VEHICLE_DRIVEABLE(playerCar)	
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				playerCarSpeed = GET_ENTITY_SPEED(playerCar)
				WHILE playerCarSpeed > 0.2
					WAIT(0)
					IF NOT IS_ENTITY_DEAD(playerCar)
						playerCarSpeed = GET_ENTITY_SPEED(playerCar)
					ENDIF
				ENDWHILE
				
				IF NOT bAndLeave
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				ENDIF
				
			ENDIF
			
		ENDIF
	ENDIF
	
	//FINISH
	IF bAndLeave
		IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_LEAVE_ANY_VEHICLE)<> PERFORMING_TASK
			TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID(),iDelay)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
	
ENDPROC

PROC SET_PED_FACING_PED(PED_INDEX pedLookAt, PED_INDEX pedToFace)
	VECTOR v1, v2, v1v2
	IF NOT IS_PED_INJURED(pedLookAt)
		IF NOT IS_PED_INJURED(pedToFace)
			v1 = GET_ENTITY_COORDS(pedLookAt)
			v2 = GET_ENTITY_COORDS(pedToFace)
			v1v2 = v2 - v1
			SET_ENTITY_HEADING(pedLookAt, GET_HEADING_FROM_VECTOR_2D(v1v2.x, v1v2.y))
		ENDIF
	ENDIF
ENDPROC

PROC SET_PED_FACING_COORD(PED_INDEX pedLookAt, VECTOR vCoord)
	VECTOR v1,  v1v2
	IF NOT IS_PED_INJURED(pedLookAt)

		v1 = GET_ENTITY_COORDS(pedLookAt)
		v1v2 = vCoord - v1

		SET_ENTITY_HEADING(pedLookAt, GET_HEADING_FROM_VECTOR_2D(v1v2.x, v1v2.y))
		
	ENDIF
ENDPROC

PROC REMOVE_OBJECT(OBJECT_INDEX &obj, BOOL b_force_delete = FALSE)
	IF DOES_ENTITY_EXIST(obj)
		IF IS_ENTITY_ATTACHED(obj)
			DETACH_ENTITY(obj)
		ENDIF

		IF b_force_delete
			DELETE_OBJECT(obj)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(obj)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_THIS_SYNCHRONISED_SCENE_COMPLETE(SynchList eSynchStage)

	IF bSSComplete[eSynchStage]
		RETURN TRUE
	ENDIF

	RETURN FALSE
	
ENDFUNC

FUNC PED_INDEX CREATE_DUMMY_PED(MODEL_NAMES model, VECTOR v_pos, FLOAT f_heading, REL_GROUP_HASH group, 
INT i_health = 2000, INT i_armour = 0,PED_TYPE e_ped_type = PEDTYPE_MISSION)
	PED_INDEX ped = CREATE_PED(e_ped_type, model, v_pos, f_heading)
	SET_PED_MAX_HEALTH(ped, i_health)
	SET_ENTITY_HEALTH(ped, i_health)
	ADD_ARMOUR_TO_PED(ped, i_armour)
	SET_ENTITY_VISIBLE(ped,TRUE)
	SET_ENTITY_INVINCIBLE(ped,TRUE)
	SET_PED_RELATIONSHIP_GROUP_HASH(ped, group)
	RETURN ped
ENDFUNC

FUNC BOOL IS_SCENE_DATA_READY(SCENE_DATA sThisScene)
	IF sThisScene.b_data_generated
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

PROC RESET_SCENE_DATA(SCENE_DATA sThisScene)
	IF sThisScene.b_data_generated
		sThisScene.b_data_generated = FALSE
	ENDIF
ENDPROC


PROC GET_SCENE_DATA(SynchList eSynchStage)
	
	////PRINTLN(s_sd_arrive_at_docks.b_data_generated)
	
	SWITCH  eSynchStage
		CASE SS_ARRIVE_AT_DOCKS
			IF NOT IS_SCENE_DATA_READY(s_sd_arrive_at_docks)
				REQUEST_ANIM_DICT("missheistdockssetup1ig_2_p1@new_structure")
				REQUEST_MODEL(S_M_M_DOCKWORK_01)
				IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_2_p1@new_structure")
				AND HAS_MODEL_LOADED(S_M_M_DOCKWORK_01)
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneArriveStartId)
						ped_dummy[0] = CREATE_DUMMY_PED(S_M_M_DOCKWORK_01,<< -81.7436, -2456.1165, 5.0189 >>, 87.3373, rel_group_buddies)
						ped_dummy[1] = CREATE_DUMMY_PED(S_M_M_DOCKWORK_01,<< -81.7436, -2456.1165, 5.0189 >>, 87.3373, rel_group_buddies)
						ped_dummy[2] = CREATE_DUMMY_PED(S_M_M_DOCKWORK_01,<< -81.7436, -2456.1165, 5.0189 >>, 87.3373, rel_group_buddies)
						
						sceneDummy = CREATE_SYNCHRONIZED_SCENE(s_sd_arrive_at_docks.v_scene_pos, s_sd_arrive_at_docks.v_scene_rot)
						TASK_SYNCHRONIZED_SCENE (ped_dummy[0],sceneDummy, "missheistdockssetup1ig_2_p1@new_structure", "supervisor_exitdoor_startidle_supervisor", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_NONE )
						TASK_SYNCHRONIZED_SCENE (ped_dummy[1],sceneDummy, "missheistdockssetup1ig_2_p1@new_structure", "supervisor_exitdoor_startidle_wade", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_NONE,RBF_PLAYER_IMPACT  )
						TASK_SYNCHRONIZED_SCENE (ped_dummy[2],sceneDummy, "missheistdockssetup1ig_2_p1@new_structure", "supervisor_exitdoor_startidle_floyd", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_NONE,RBF_PLAYER_IMPACT  )
					ELSE
						IF NOT IS_PED_INJURED(ped_dummy[0])
							s_sp_supervisor1[0].v_ss_pos = GET_ENTITY_COORDS(ped_dummy[0])
							s_sp_supervisor1[0].f_ss_rot = GET_ENTITY_HEADING(ped_dummy[0])
							DELETE_PED(ped_dummy[0])
						ENDIF
						
						IF NOT IS_PED_INJURED(ped_dummy[1])
							v_ss_wade[SS_ARRIVE_AT_DOCKS]= GET_ENTITY_COORDS(ped_dummy[1])
							f_ss_wade[SS_ARRIVE_AT_DOCKS]= GET_ENTITY_HEADING(ped_dummy[1])
							DELETE_PED(ped_dummy[1])
						ENDIF
						
						IF NOT IS_PED_INJURED(ped_dummy[2])
							v_ss_floyd[SS_ARRIVE_AT_DOCKS]= GET_ENTITY_COORDS(ped_dummy[2])
							f_ss_floyd[SS_ARRIVE_AT_DOCKS] = GET_ENTITY_HEADING(ped_dummy[2])
							DELETE_PED(ped_dummy[2])
						ENDIF
						
						s_sd_arrive_at_docks.b_data_generated = TRUE
						
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE Ss_dock_workers_on_pipe
			IF NOT IS_SCENE_DATA_READY(s_sd_arrive_at_docks)
				
				REQUEST_MODEL(S_M_M_DOCKWORK_01)
	
				IF HAS_MODEL_LOADED(S_M_M_DOCKWORK_01)
				    //SCRIPT_ASSERT("1")
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneDummy)
						ped_dummy[0] = CREATE_DUMMY_PED(S_M_M_DOCKWORK_01,<< -81.7436, -2456.1165, 5.0189 >>, 87.3373, rel_group_buddies)
						ped_dummy[1] = CREATE_DUMMY_PED(S_M_M_DOCKWORK_01,<< -81.7436, -2456.1165, 5.0189 >>, 87.3373, rel_group_buddies)
						ped_dummy[2] = CREATE_DUMMY_PED(S_M_M_DOCKWORK_01,<< -81.7436, -2456.1165, 5.0189 >>, 87.3373, rel_group_buddies)
						
						sceneDummy = CREATE_SYNCHRONIZED_SCENE(s_sd_dock_workers_on_pipe.v_scene_pos, s_sd_dock_workers_on_pipe.v_scene_rot)
						//SCRIPT_ASSERT("2")
					ELSE
						IF NOT IS_PED_INJURED(ped_dummy[0])
							s_sp_dock_workers_on_pipe[0].v_ss_pos = GET_ENTITY_COORDS(ped_dummy[0])
							s_sp_dock_workers_on_pipe[0].f_ss_rot = GET_ENTITY_HEADING(ped_dummy[0])
							DELETE_PED(ped_dummy[0])
						ENDIF
						
						IF NOT IS_PED_INJURED(ped_dummy[1])
							s_sp_dock_workers_on_pipe[1].v_ss_pos = GET_ENTITY_COORDS(ped_dummy[1])
							s_sp_dock_workers_on_pipe[1].f_ss_rot = GET_ENTITY_HEADING(ped_dummy[1])
							DELETE_PED(ped_dummy[1])
						ENDIF
						
						IF NOT IS_PED_INJURED(ped_dummy[2])
							s_sp_dock_workers_on_pipe[2].v_ss_pos = GET_ENTITY_COORDS(ped_dummy[2])
							s_sp_dock_workers_on_pipe[2].f_ss_rot = GET_ENTITY_HEADING(ped_dummy[2])
							DELETE_PED(ped_dummy[2])
						ENDIF
						// SCRIPT_ASSERT("3")
						s_sd_dock_workers_on_pipe.b_data_generated = TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE SS_blackwater_with_dockworker
			IF NOT IS_SCENE_DATA_READY(S_sd_blackwater_with_dockworker)
				REQUEST_MODEL(S_M_M_DOCKWORK_01)
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneDummy)
					ped_dummy[0] = CREATE_DUMMY_PED(S_M_M_DOCKWORK_01,<< -81.7436, -2456.1165, 5.0189 >>, 87.3373, rel_group_buddies)
					ped_dummy[1] = CREATE_DUMMY_PED(S_M_M_DOCKWORK_01,<< -81.7436, -2456.1165, 5.0189 >>, 87.3373, rel_group_buddies)
					ped_dummy[2] = CREATE_DUMMY_PED(S_M_M_DOCKWORK_01,<< -81.7436, -2456.1165, 5.0189 >>, 87.3373, rel_group_buddies)
					
					sceneDummy = CREATE_SYNCHRONIZED_SCENE(S_sd_blackwater_with_dockworker.v_scene_pos, S_sd_blackwater_with_dockworker.v_scene_rot)
					
				ELSE
					IF NOT IS_PED_INJURED(ped_dummy[0])
						s_sp_blackwater_with_dockworker[0].v_ss_pos = GET_ENTITY_COORDS(ped_dummy[0])
						s_sp_blackwater_with_dockworker[0].f_ss_rot = GET_ENTITY_HEADING(ped_dummy[0])
						DELETE_PED(ped_dummy[0])
					ENDIF
					
					IF NOT IS_PED_INJURED(ped_dummy[1])
						s_sp_blackwater_with_dockworker[1].v_ss_pos = GET_ENTITY_COORDS(ped_dummy[1])
						s_sp_blackwater_with_dockworker[1].f_ss_rot = GET_ENTITY_HEADING(ped_dummy[1])
						DELETE_PED(ped_dummy[1])
					ENDIF
					
					IF NOT IS_PED_INJURED(ped_dummy[2])
						s_sp_blackwater_with_dockworker[2].v_ss_pos = GET_ENTITY_COORDS(ped_dummy[2])
						s_sp_blackwater_with_dockworker[2].f_ss_rot = GET_ENTITY_HEADING(ped_dummy[2])
						DELETE_PED(ped_dummy[2])
					ENDIF
					
					S_sd_blackwater_with_dockworker.b_data_generated = TRUE
				ENDIF
			ENDIF
		BREAK

		
		CASE SS_FLOYD_GRABS_PACKAGE
			IF NOT IS_SCENE_DATA_READY(s_sd_floyd_grabs_package)
				REQUEST_ANIM_DICT("missheistdockssetup1ig_4@start_idle")
				REQUEST_MODEL(S_M_M_DOCKWORK_01)
				IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_4@start_idle")
				AND HAS_MODEL_LOADED(S_M_M_DOCKWORK_01)
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneDummy)
						ped_dummy[0] = CREATE_DUMMY_PED(S_M_M_DOCKWORK_01,<< -81.7436, -2456.1165, 5.0189 >>, 87.3373, rel_group_buddies)
						ped_dummy[1] = CREATE_DUMMY_PED(S_M_M_DOCKWORK_01,<< -81.7436, -2456.1165, 5.0189 >>, 87.3373, rel_group_buddies)
						ped_dummy[2] = CREATE_DUMMY_PED(S_M_M_DOCKWORK_01,<< -81.7436, -2456.1165, 5.0189 >>, 87.3373, rel_group_buddies)
						ped_dummy[3] = CREATE_DUMMY_PED(S_M_M_DOCKWORK_01,<< -81.7436, -2456.1165, 5.0189 >>, 87.3373, rel_group_buddies)
						
						sceneDummy = CREATE_SYNCHRONIZED_SCENE(s_sd_floyd_grabs_package.v_scene_pos, s_sd_floyd_grabs_package.v_scene_rot)
						TASK_SYNCHRONIZED_SCENE (ped_dummy[0],sceneDummy, "missheistdockssetup1ig_4@start_idle", "FLOYD_FellPackage_StartIdle_DockWorker1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_NONE )
						TASK_SYNCHRONIZED_SCENE (ped_dummy[1],sceneDummy, "missheistdockssetup1ig_4@start_idle", "FLOYD_FellPackage_StartIdle_DockWorker2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_NONE )
						TASK_SYNCHRONIZED_SCENE (ped_dummy[2],sceneDummy, "missheistdockssetup1ig_4@start_idle", "FLOYD_FellPackage_StartIdle_DockWorker3", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_NONE )
						TASK_SYNCHRONIZED_SCENE (ped_dummy[3],sceneDummy, "missheistdockssetup1ig_4@start_idle", "FLOYD_FellPackage_StartIdle_Floyd", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_NONE,RBF_PLAYER_IMPACT  )
					ELSE
						IF NOT IS_PED_INJURED(ped_dummy[0])
							s_sp_floyd_grabs_package[0].v_ss_pos = GET_ENTITY_COORDS(ped_dummy[0])
							s_sp_floyd_grabs_package[0].f_ss_rot = GET_ENTITY_HEADING(ped_dummy[0])
							DELETE_PED(ped_dummy[0])
						ENDIF
						
						IF NOT IS_PED_INJURED(ped_dummy[1])
							s_sp_floyd_grabs_package[1].v_ss_pos = GET_ENTITY_COORDS(ped_dummy[1])
							s_sp_floyd_grabs_package[1].f_ss_rot = GET_ENTITY_HEADING(ped_dummy[1])
							DELETE_PED(ped_dummy[1])
						ENDIF
						
						IF NOT IS_PED_INJURED(ped_dummy[2])
							s_sp_floyd_grabs_package[2].v_ss_pos = GET_ENTITY_COORDS(ped_dummy[2])
							s_sp_floyd_grabs_package[2].f_ss_rot = GET_ENTITY_HEADING(ped_dummy[2])
							DELETE_PED(ped_dummy[2])
						ENDIF
						
						IF NOT IS_PED_INJURED(ped_dummy[3])
							v_ss_floyd[SS_FLOYD_GRABS_PACKAGE] = GET_ENTITY_COORDS(ped_dummy[3])
							f_ss_floyd[SS_FLOYD_GRABS_PACKAGE] = GET_ENTITY_HEADING(ped_dummy[3])
							DELETE_PED(ped_dummy[3])
						ENDIF
						
						s_sd_floyd_grabs_package.b_data_generated = TRUE
					ENDIF
				ENDIF
			ENDIF

		BREAK
		
		CASE SS_WELDER
			IF NOT IS_SCENE_DATA_READY(s_sd_welder)
				REQUEST_ANIM_DICT("missheistdockssetup1ig_3")
				REQUEST_MODEL(S_M_M_DOCKWORK_01)
				IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_3")
				AND HAS_MODEL_LOADED(S_M_M_DOCKWORK_01)
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneDummy)
						ped_dummy[0] = CREATE_DUMMY_PED(S_M_M_DOCKWORK_01,<< -81.7436, -2456.1165, 5.0189 >>, 87.3373, rel_group_buddies)
						IF DOES_ENTITY_EXIST(ped_dummy[0])
							IF NOT IS_PED_INJURED(ped_dummy[0])
								sceneDummy = CREATE_SYNCHRONIZED_SCENE(s_sd_welder.v_scene_pos,s_sd_welder.v_scene_rot)
								TASK_SYNCHRONIZED_SCENE (ped_dummy[0], sceneDummy, "missheistdockssetup1ig_3", "welding_enter_dockworker", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_NONE )
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_PED_INJURED(ped_dummy[0])
							s_sp_welder[0].v_ss_pos = GET_ENTITY_COORDS(ped_dummy[0])
							s_sp_welder[0].f_ss_rot = GET_ENTITY_HEADING(ped_dummy[0])
							DELETE_PED(ped_dummy[0])
						ENDIF
						s_sd_welder.b_data_generated = TRUE
					ENDIF
				ENDIF
			ENDIF

		BREAK
		
		
	ENDSWITCH
	
	
ENDPROC

FUNC BOOL SETUP_PEDS_FOR_DIALOGUE()
	
	IF DOES_ENTITY_EXIST(pedSecurity)
	    IF NOT IS_PED_INJURED(pedSecurity)
			IF sSpeech.PedInfo[1].Index = NULL
				ADD_PED_FOR_DIALOGUE(sSpeech, 1, pedSecurity, "GATEGUARD")
			ENDIF
	    ENDIF
    ELSE
      	IF sSpeech.PedInfo[1].Index <> NULL
      		REMOVE_PED_FOR_DIALOGUE(sSpeech, 1)
     	ENDIF
    ENDIF

	ADD_PED_FOR_DIALOGUE(sSpeech, 2, PLAYER_PED_ID(), "TREVOR")
	
	IF DOES_ENTITY_EXIST(pedFloyd)
	    IF NOT IS_PED_INJURED(pedFloyd)
			//IF sSpeech.PedInfo[3].Index = NULL
			IF sSpeech.PedInfo[3].ActiveInConversation = FALSE
			    ADD_PED_FOR_DIALOGUE(sSpeech, 3, pedFloyd, "FLOYD")
			ENDIF
	    ENDIF
    ELSE
      	//IF sSpeech.PedInfo[3].Index <> NULL
		IF sSpeech.PedInfo[3].ActiveInConversation = TRUE
      		REMOVE_PED_FOR_DIALOGUE(sSpeech, 3)
     	ENDIF
    ENDIF
	
	IF DOES_ENTITY_EXIST(pedWade)
	    IF NOT IS_PED_INJURED(pedWade)
			IF sSpeech.PedInfo[4].Index = NULL
			   	ADD_PED_FOR_DIALOGUE(sSpeech, 4, pedWade, "WADE")
			ENDIF
	    ENDIF
    ELSE
      	IF sSpeech.PedInfo[4].Index <> NULL
      		REMOVE_PED_FOR_DIALOGUE(sSpeech, 4)
     	ENDIF
    ENDIF
	
	IF DOES_ENTITY_EXIST(s_sp_supervisor1[0].ped)
	    IF NOT IS_PED_INJURED(s_sp_supervisor1[0].ped)
			ADD_PED_FOR_DIALOGUE(sSpeech, 6, s_sp_supervisor1[0].ped, "SUPERVISOR1")
	    ENDIF
    ELSE
      	IF sSpeech.PedInfo[6].Index <> NULL
      		REMOVE_PED_FOR_DIALOGUE(sSpeech, 6)
     	ENDIF
    ENDIF
	
	IF eMissionStage = MISSION_STAGE_4
		IF DOES_ENTITY_EXIST(s_sp_dock_workers_talking[0].ped)
		    IF NOT IS_PED_INJURED(s_sp_dock_workers_talking[0].ped)
				ADD_PED_FOR_DIALOGUE(sSpeech, 5, s_sp_dock_workers_talking[0].ped, "LONGSHOREMAN")
		    ENDIF
	    ELSE
	      	IF sSpeech.PedInfo[5].Index <> NULL
	      		REMOVE_PED_FOR_DIALOGUE(sSpeech, 5)
	     	ENDIF
	    ENDIF
	ENDIF
	
	IF eMissionStage = MISSION_STAGE_4
		IF DOES_ENTITY_EXIST(s_sp_forklift_supervisorA[0].ped)
		    IF NOT IS_PED_INJURED(s_sp_forklift_supervisorA[0].ped)
				ADD_PED_FOR_DIALOGUE(sSpeech, 7, s_sp_forklift_supervisorA[0].ped, "SUPERVISOR2")
		    ENDIF
	    ELSE
	      	IF sSpeech.PedInfo[7].Index <> NULL
	      		REMOVE_PED_FOR_DIALOGUE(sSpeech, 7)
	     	ENDIF
	    ENDIF
	ENDIF
	
	IF eMissionStage > MISSION_STAGE_5
		IF DOES_ENTITY_EXIST(pedDockWorker[14])
		    IF NOT IS_PED_INJURED(pedDockWorker[14])
				ADD_PED_FOR_DIALOGUE(sSpeech, 8, pedDockWorker[14], "SIGNALMAN")
		    ENDIF
	    ELSE
	      	IF sSpeech.PedInfo[8].Index <> NULL
	      		REMOVE_PED_FOR_DIALOGUE(sSpeech, 8)
	     	ENDIF
	    ENDIF
	ENDIF
	
	RETURN TRUE
	
ENDFUNC

PROC GENERATE_ALL_SCENE_DATA()

	IF NOT IS_SCENE_DATA_READY(s_sd_arrive_at_docks)
		GET_SCENE_DATA(SS_ARRIVE_AT_DOCKS)
		//SCRIPT_ASSERT("SS_ARRIVE_AT_DOCKS")
	ENDIF
	
	IF IS_SCENE_DATA_READY(s_sd_arrive_at_docks)
		IF NOT IS_SCENE_DATA_READY(S_sd_dock_workers_on_pipe)
			GET_SCENE_DATA(SS_dock_workers_on_pipe)
			//SCRIPT_ASSERT("SS_dock_workers_on_pipe")
		ENDIF
	ENDIF
	
	IF IS_SCENE_DATA_READY(S_sd_dock_workers_on_pipe)
		IF NOT IS_SCENE_DATA_READY(S_sd_blackwater_with_dockworker)
			GET_SCENE_DATA(SS_blackwater_with_dockworker)
			//SCRIPT_ASSERT("SS_blackwater_with_dockworker")
		ENDIF
	ENDIF
	
	IF IS_SCENE_DATA_READY(S_sd_blackwater_with_dockworker)
		IF NOT IS_SCENE_DATA_READY(s_sd_gantry_guys_dynamic)
			GET_SCENE_DATA(SS_GANTRY_GUYS)
			//SCRIPT_ASSERT("SS_GANTRY_GUYS")
		ENDIF
	ENDIF
	
	IF IS_SCENE_DATA_READY(s_sd_gantry_guys_dynamic)
		IF NOT IS_SCENE_DATA_READY(s_sd_gantry_guys_static)
			GET_SCENE_DATA(SS_STATIC_GANTRY_GUYS)
			//SCRIPT_ASSERT("SS_STATIC_GANTRY_GUYS")
		ENDIF
	ENDIF
	
	IF IS_SCENE_DATA_READY(s_sd_gantry_guys_static)
		IF NOT IS_SCENE_DATA_READY(s_sd_floyd_grabs_package)
			GET_SCENE_DATA(SS_FLOYD_GRABS_PACKAGE)
			//SCRIPT_ASSERT("SS_FLOYD_GRABS_PACKAGE")
		ENDIF
	ENDIF
	
	IF IS_SCENE_DATA_READY(s_sd_floyd_grabs_package)
		IF NOT IS_SCENE_DATA_READY(s_sd_welder)
			GET_SCENE_DATA(SS_WELDER)
			//SCRIPT_ASSERT("SS_WELDER")
		ENDIF
	ENDIF

ENDPROC

PROC MANAGE_SPEECH_FOR_AMBIENT_SET_PIECE(AMBIENT_SET_PIECE_SPEECH_ENUM eAmbientSP)

	SWITCH eAmbientSP
		
		CASE AMBSP_BLACKWATER_WITH_DOCK_WORKER
			IF NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[0].ped)
			AND NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[1].ped)
			AND NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[2].ped)
				IF s_sp_blackwater_with_dockworker[0].i_event = 0
//					IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_DWAA")
//						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(enemy[2].ped,"TR2_AIAA","TREVOR2BIKER1","SPEECH_PARAMS_FORCE_FRONTEND")
//						SET_LABEL_AS_TRIGGERED("TR2_DWAA",TRUE)
//					ELSE
//						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_DWAB")
//							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(enemy[2].ped,"TR2_AIAB","TREVOR2BIKER1","SPEECH_PARAMS_FORCE_FRONTEND")
//							SET_LABEL_AS_TRIGGERED("TR2_DWAB",TRUE)
//						ELSE
//							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_DWAC")
//								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(enemy[2].ped,"TR2_AIAC","TREVOR2BIKER1","SPEECH_PARAMS_FORCE_FRONTEND")
//								SET_LABEL_AS_TRIGGERED("TR2_DWAC",TRUE)
//								bDialogue = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE AMBSP_DOCK_WORKERS_ON_PIPE
			IF NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[0].ped)
			AND NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[1].ped)
			AND NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[2].ped)
//				IF s_sp_dock_workers_on_pipe[0].i_event = 0
//					IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_DWAA")
//						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(enemy[2].ped,"TR2_AIAA","TREVOR2BIKER1","SPEECH_PARAMS_FORCE_FRONTEND")
//						SET_LABEL_AS_TRIGGERED("TR2_DWAA",TRUE)
//					ELSE
//						IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_DWAB")
//							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(enemy[2].ped,"TR2_AIAB","TREVOR2BIKER1","SPEECH_PARAMS_FORCE_FRONTEND")
//							SET_LABEL_AS_TRIGGERED("TR2_DWAB",TRUE)
//						ELSE
//							IF NOT HAS_LABEL_BEEN_TRIGGERED("TR2_DWAC")
//								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(enemy[2].ped,"TR2_AIAC","TREVOR2BIKER1","SPEECH_PARAMS_FORCE_FRONTEND")
//								SET_LABEL_AS_TRIGGERED("TR2_DWAC",TRUE)
//								bDialogue = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
			ENDIF
		BREAK
		
		
	ENDSWITCH
	
ENDPROC

PROC MANAGE_DOCK_SETPIECES(VECTOR v_player_pos)
	
	//SEQUENCE_INDEX seq
	
	//Keep these models in memory
	REQUEST_MODEL(S_M_M_DOCKWORK_01)
	REQUEST_MODEL(S_M_Y_BLACKOPS_01)
	/*
	  _  ___ ___ _ _ _ ___    _ ___   __   _   __ _  ___ 
	 / \| o \ o \ | | | __|  / \_ _| |  \ / \ / _| |// _|
	| o |   /   / | V | _|  | o | |  | o | o | (_|  (\_ \
	|_n_|_|\\_|\\_|\_/|___| |_n_|_|  |__/ \_/ \__|_|\\__|	
	
	*/
IF NOT IS_THIS_SYNCHRONISED_SCENE_COMPLETE(SS_ARRIVE_AT_DOCKS)	
	IF NOT DOES_ENTITY_EXIST(s_sp_supervisor1[0].ped)
		IF eMissionStage > MISSION_STAGE_1
		AND eMissionStage < MISSION_STAGE_4
			REQUEST_ANIM_DICT("missheistdockssetup1ig_2_p1@new_structure")
			REQUEST_MODEL(s_sp_supervisor1[0].model)
			IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_2_p1@new_structure")
			AND HAS_MODEL_LOADED(s_sp_supervisor1[0].model)
				s_sp_supervisor1[0].ped = CREATE_SETPIECE_PED(s_sp_supervisor1[0].model,s_sp_supervisor1[0].v_ss_pos, s_sp_supervisor1[0].f_ss_rot, rel_group_buddies, 200, 0)
				ADD_PED_FOR_DIALOGUE(sSpeech, 6, s_sp_supervisor1[0].ped, "SUPERVISOR1")
				INITIALISE_SETPIECE_GROUP(s_sp_supervisor1, "Arrive at docks")
				v_ss_floyd[SS_ARRIVE_AT_DOCKS] = <<-61.30, -2523.86, 6.16>>
				f_ss_floyd[SS_ARRIVE_AT_DOCKS] = -31.41
				
				v_ss_wade[SS_ARRIVE_AT_DOCKS] = <<-60.50, -2523.52, 6.16>>
				f_ss_wade[SS_ARRIVE_AT_DOCKS] = 59.25
			ENDIF
		ENDIF
	ELSE
		IF bSetPiece[SS_ARRIVE_AT_DOCKS]
			SWITCH s_sp_supervisor1[0].i_event
				//SUPERVISOR LEAVES THE BUILDING
				CASE 0
					
					IF DOES_ENTITY_EXIST(s_sp_supervisor1[0].ped)
					AND DOES_ENTITY_EXIST(pedWade)
					AND DOES_ENTITY_EXIST(pedFloyd)
						IF NOT IS_PED_INJURED(s_sp_supervisor1[0].ped)
						AND NOT IS_PED_INJURED(pedWade)
						AND NOT IS_PED_INJURED(pedFloyd)
							//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							IF SETUP_PEDS_FOR_DIALOGUE()	
								sceneArriveStartId = CREATE_SYNCHRONIZED_SCENE(s_sd_arrive_at_docks.v_scene_pos, s_sd_arrive_at_docks.v_scene_rot)
								TASK_SYNCHRONIZED_SCENE (s_sp_supervisor1[0].ped,sceneArriveStartId, "missheistdockssetup1ig_2_p1@new_structure", "walk_down_supervisor", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT)
								s_sp_supervisor1[0].i_event ++	
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				//SUPERVISOR WAIT LOOP
				CASE 1
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneArriveStartId) > 0.1
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_03b") 
							IF NOT IS_PED_INJURED(s_sp_supervisor1[0].ped)
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_supervisor1[0].ped,"DH1_BXAB","SUPERVISOR",SPEECH_PARAMS_FORCE_FRONTEND)
								PRINTSTRING("SHOUT")PRINTNL()
								SET_LABEL_AS_TRIGGERED("DS1_03b",TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(s_sp_supervisor1[0].ped)
					AND DOES_ENTITY_EXIST(pedWade)
					AND DOES_ENTITY_EXIST(pedFloyd)
						IF NOT IS_PED_INJURED(s_sp_supervisor1[0].ped)
						AND NOT IS_PED_INJURED(pedWade)
						AND NOT IS_PED_INJURED(pedFloyd)
							IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneArriveStartId)
								IF GET_SYNCHRONIZED_SCENE_PHASE(sceneArriveStartId) = 1.0
									sceneArriveStartId = CREATE_SYNCHRONIZED_SCENE(s_sd_arrive_at_docks.v_scene_pos, s_sd_arrive_at_docks.v_scene_rot)
									TASK_SYNCHRONIZED_SCENE (s_sp_supervisor1[0].ped,sceneArriveStartId, "missheistdockssetup1ig_2_p1@new_structure", "wait_loop_supervisor", SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT)
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneArriveStartId,TRUE)
									s_sp_supervisor1[0].i_event ++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				//START THE ACTION
				CASE 2	
					IF DOES_ENTITY_EXIST(s_sp_supervisor1[0].ped)
					AND DOES_ENTITY_EXIST(pedWade)
					AND DOES_ENTITY_EXIST(pedFloyd)
						IF NOT IS_PED_INJURED(s_sp_supervisor1[0].ped)
						AND NOT IS_PED_INJURED(pedWade)
						AND NOT IS_PED_INJURED(pedFloyd)
							IF IS_PED_READY_FOR_ANIM(pedWade, v_ss_wade[SS_ARRIVE_AT_DOCKS], f_ss_wade[SS_ARRIVE_AT_DOCKS],<<0.2,0.2,2>>,360,TRUE)
							AND IS_PED_READY_FOR_ANIM(pedFloyd,v_ss_floyd[SS_ARRIVE_AT_DOCKS], f_ss_floyd[SS_ARRIVE_AT_DOCKS],<<0.2,0.2,2>>,360,TRUE)
								IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_03c")
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech,"D1AUD", "DS1_03c", CONV_PRIORITY_HIGH)
												REPLAY_RECORD_BACK_FOR_TIME(2.0, 7.0, REPLAY_IMPORTANCE_HIGHEST)
												SET_LABEL_AS_TRIGGERED("DS1_03c",TRUE)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									sceneArriveMainId = CREATE_SYNCHRONIZED_SCENE(s_sd_arrive_at_docks.v_scene_pos, s_sd_arrive_at_docks.v_scene_rot)
									TASK_SYNCHRONIZED_SCENE (s_sp_supervisor1[0].ped,sceneArriveMainId, "missheistdockssetup1ig_2_p1@new_structure", "push_guy_back_supervisor", SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT)
									TASK_SYNCHRONIZED_SCENE (pedWade, sceneArriveMainId, "missheistdockssetup1ig_2_p1@new_structure", "push_guy_back_wade", SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT,4)
									TASK_SYNCHRONIZED_SCENE (pedFloyd, sceneArriveMainId, "missheistdockssetup1ig_2_p1@new_structure", "push_guy_back_floyd", SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT,4)
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneArriveMainId,FALSE)
									s_sp_supervisor1[0].i_event ++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				//GET INTO END IDLE
				CASE 3
					
					IF DOES_ENTITY_EXIST(s_sp_supervisor1[0].ped)
					AND DOES_ENTITY_EXIST(pedWade)
					AND DOES_ENTITY_EXIST(pedFloyd)
						IF NOT IS_PED_INJURED(s_sp_supervisor1[0].ped)
						AND NOT IS_PED_INJURED(pedWade)
						AND NOT IS_PED_INJURED(pedFloyd)
							IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneArriveMainId)
								IF GET_SYNCHRONIZED_SCENE_PHASE(sceneArriveMainId) > 0.7
									SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(P_CUT_DOOR_02,<<-63.19, -2519.27, 7.79>>,TRUE,0,0,0)
									REQUEST_ANIM_DICT("missheistdockssetup1ig_2_p1@end_idle")
									s_sp_supervisor1[0].i_event ++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				//FINISH UP - CLEAR TASKS ECT
				CASE 4
					IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_2_p1@end_idle")
						IF DOES_ENTITY_EXIST(pedFloyd)
							IF NOT IS_PED_INJURED(pedFloyd)
								IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneArriveMainId)
									IF GET_SYNCHRONIZED_SCENE_PHASE(sceneArriveMainId) = 1
										IF eMissionStage = MISSION_STAGE_2
											sceneArriveMainId = CREATE_SYNCHRONIZED_SCENE(s_sd_arrive_at_docks.v_scene_pos, s_sd_arrive_at_docks.v_scene_rot)
											TASK_SYNCHRONIZED_SCENE (pedFloyd,sceneArriveMainId, "missheistdockssetup1ig_2_p1@end_idle", "supervisor_exitdoor_endidle_floyd", NORMAL_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
											SET_SYNCHRONIZED_SCENE_LOOPED(sceneArriveMainId,TRUE)
											REMOVE_ANIM_DICT("missheistdockssetup1ig_2_p1@new_structure")	
											bSSComplete[SS_ARRIVE_AT_DOCKS] = TRUE
											s_sp_supervisor1[0].i_event ++
										ELSE
											IF eMissionStage = MISSION_STAGE_4
												IF iProgress < 3
													sceneArriveMainId = CREATE_SYNCHRONIZED_SCENE(s_sd_arrive_at_docks.v_scene_pos, s_sd_arrive_at_docks.v_scene_rot)
													TASK_SYNCHRONIZED_SCENE (pedFloyd,sceneArriveMainId, "missheistdockssetup1ig_2_p1@end_idle", "supervisor_exitdoor_endidle_floyd", NORMAL_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS )
													SET_SYNCHRONIZED_SCENE_LOOPED(sceneArriveMainId,TRUE)
													REMOVE_ANIM_DICT("missheistdockssetup1ig_2_p1@new_structure")	
													bSSComplete[SS_ARRIVE_AT_DOCKS] = TRUE
													s_sp_supervisor1[0].i_event ++
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 5
				
				BREAK
				
				
			ENDSWITCH

		ELSE

		ENDIF
	ENDIF
ENDIF
	
/*   ___ _    ___ ____     __  ___  _  ___ __   ___  _   __ _  _ _   __  ___ 
	| __| |  / \ V /  \   / _|| o \/ \| o ) _| | o \/ \ / _| |/// \ / _|| __|
	| _|| |_( o ) /| o ) ( |_n|   / o | o \_ \ |  _/ o ( (_|  (| o ( |_n| _| 
	|_| |___|\_/|_||__/   \__/|_|\\_n_|___/__/ |_| |_n_|\__|_|\\_n_|\__/|___|*/

	
IF NOT IS_THIS_SYNCHRONISED_SCENE_COMPLETE(SS_FLOYD_GRABS_PACKAGE)	
		IF NOT DOES_ENTITY_EXIST(s_sp_floyd_grabs_package[0].ped)
			IF eMissionStage > MISSION_STAGE_1
				REQUEST_ANIM_DICT("missheistdockssetup1ig_4@start_idle")
				REQUEST_MODEL(s_sp_floyd_grabs_package[0].model)
				REQUEST_MODEL(s_sp_floyd_grabs_package[1].model)
				REQUEST_MODEL(s_sp_floyd_grabs_package[2].model)
				//REQUEST_MODEL(PROP_CS_PACKAGE_01)
				IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_4@start_idle")
				AND HAS_MODEL_LOADED(s_sp_floyd_grabs_package[0].model)
				AND HAS_MODEL_LOADED(s_sp_floyd_grabs_package[1].model)
				AND HAS_MODEL_LOADED(s_sp_floyd_grabs_package[2].model)
				//AND HAS_MODEL_LOADED(PROP_CS_PACKAGE_01)
					s_sp_floyd_grabs_package[0].ped = CREATE_SETPIECE_PED(s_sp_floyd_grabs_package[0].model,s_sp_floyd_grabs_package[0].v_ss_pos, s_sp_floyd_grabs_package[0].f_ss_rot, rel_group_buddies, 200, 0)
					s_sp_floyd_grabs_package[1].ped = CREATE_SETPIECE_PED(s_sp_floyd_grabs_package[1].model,s_sp_floyd_grabs_package[1].v_ss_pos, s_sp_floyd_grabs_package[0].f_ss_rot, rel_group_buddies, 200, 0)
					s_sp_floyd_grabs_package[2].ped = CREATE_SETPIECE_PED(s_sp_floyd_grabs_package[2].model,s_sp_floyd_grabs_package[2].v_ss_pos, s_sp_floyd_grabs_package[0].f_ss_rot, rel_group_buddies, 200, 0)
					//s_sp_floyd_grabs_package[0].obj_prop = CREATE_OBJECT(PROP_CS_PACKAGE_01,<< -56.6750, -2434.4624, 5.0008 >>)
					INITIALISE_SETPIECE_GROUP(s_sp_floyd_grabs_package, "Floyd Grabs Package")
				ENDIF
			ENDIF
		ELSE
			//All the enemies are triggered off the same locate, so do outside the loop.
				SET_PED_CAPSULE(s_sp_floyd_grabs_package[0].ped, 0.4)
				SET_PED_CAPSULE(s_sp_floyd_grabs_package[1].ped, 0.7)
				SET_PED_CAPSULE(s_sp_floyd_grabs_package[2].ped, 0.4)
				
				SWITCH s_sp_floyd_grabs_package[0].i_event
					//GET PEDS INTO POSITION
					CASE 0
						IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_4@start_idle")
							IF DOES_ENTITY_EXIST(s_sp_floyd_grabs_package[0].ped)
							AND DOES_ENTITY_EXIST(s_sp_floyd_grabs_package[1].ped)
							AND DOES_ENTITY_EXIST(s_sp_floyd_grabs_package[2].ped)
							AND DOES_ENTITY_EXIST(pedFloyd)
								IF NOT IS_PED_INJURED(s_sp_floyd_grabs_package[0].ped)
								AND NOT IS_PED_INJURED(s_sp_floyd_grabs_package[1].ped)
								AND NOT IS_PED_INJURED(s_sp_floyd_grabs_package[2].ped)
								AND NOT IS_PED_INJURED(pedFloyd)
									sceneFloydPackage1init = CREATE_SYNCHRONIZED_SCENE(<< -88.341, -2502.575, 5.011 >>, << 0.000, -0.000, -32.844 >>)
									TASK_SYNCHRONIZED_SCENE (s_sp_floyd_grabs_package[0].ped,sceneFloydPackage1init, "missheistdockssetup1ig_4@start_idle", "FLOYD_FellPackage_StartIdle_DockWorker1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
									TASK_SYNCHRONIZED_SCENE (s_sp_floyd_grabs_package[1].ped,sceneFloydPackage1init, "missheistdockssetup1ig_4@start_idle", "FLOYD_FellPackage_StartIdle_DockWorker2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
									TASK_SYNCHRONIZED_SCENE (s_sp_floyd_grabs_package[2].ped,sceneFloydPackage1init, "missheistdockssetup1ig_4@start_idle", "FLOYD_FellPackage_StartIdle_DockWorker3", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneFloydPackage1init,TRUE)
									s_sp_floyd_grabs_package[0].i_event ++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					
					CASE 1
						IF eMissionStage = MISSION_STAGE_4
							IF DOES_ENTITY_EXIST(s_sp_floyd_grabs_package[0].ped)
							AND DOES_ENTITY_EXIST(s_sp_floyd_grabs_package[1].ped)
							AND  DOES_ENTITY_EXIST(s_sp_floyd_grabs_package[2].ped)
							AND DOES_ENTITY_EXIST(pedFloyd)
								IF HAS_LABEL_BEEN_TRIGGERED("FLOYD SEEN BOAT")
									IF NOT IS_PED_INJURED(s_sp_floyd_grabs_package[0].ped)
									AND NOT IS_PED_INJURED(s_sp_floyd_grabs_package[1].ped)
									AND NOT IS_PED_INJURED(s_sp_floyd_grabs_package[2].ped)
									AND NOT IS_PED_INJURED(pedFloyd)
										IF NOT IS_ENTITY_ON_SCREEN(s_sp_floyd_grabs_package[0].ped)
										AND NOT IS_ENTITY_ON_SCREEN(s_sp_floyd_grabs_package[1].ped)
										AND NOT IS_ENTITY_ON_SCREEN(s_sp_floyd_grabs_package[2].ped)
											//CLEAR_PED_TASKS(pedFloyd)
											STOP_SYNCHRONIZED_ENTITY_ANIM(s_sp_floyd_grabs_package[0].ped,SLOW_BLEND_OUT,TRUE)
											STOP_SYNCHRONIZED_ENTITY_ANIM(s_sp_floyd_grabs_package[1].ped,SLOW_BLEND_OUT,TRUE)
											STOP_SYNCHRONIZED_ENTITY_ANIM(s_sp_floyd_grabs_package[2].ped,SLOW_BLEND_OUT,TRUE)
											CLEAR_PED_TASKS(s_sp_floyd_grabs_package[0].ped)
											CLEAR_PED_TASKS(s_sp_floyd_grabs_package[1].ped)
											CLEAR_PED_TASKS(s_sp_floyd_grabs_package[2].ped)
											SET_PED_AS_NO_LONGER_NEEDED(s_sp_floyd_grabs_package[0].ped)
											SET_PED_AS_NO_LONGER_NEEDED(s_sp_floyd_grabs_package[1].ped)
											SET_PED_AS_NO_LONGER_NEEDED(s_sp_floyd_grabs_package[2].ped)
											REMOVE_ANIM_DICT("missheistdockssetup1ig_4@start_idle")
											bSSComplete[SS_FLOYD_GRABS_PACKAGE] = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
				ENDSWITCH
		ENDIF
	ENDIF
	
/*
  | | | |/ \| o \ |// | \| |/ _|  / \| \| | | o \ | o \ __|
  | V V ( o )   /  (| | \\ ( |_n ( o ) \\ | |  _/ |  _/ _| 
   \_n_/ \_/|_|\\_|\\_|_|\_|\__/  \_/|_|\_| |_| |_|_| |___|
   															*/
IF NOT IS_THIS_SYNCHRONISED_SCENE_COMPLETE(SS_dock_workers_on_pipe)																
	IF bSetPiece[SS_dock_workers_on_pipe]												
		IF NOT DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[0].ped)
			IF eMissionStage > MISSION_STAGE_1
				REQUEST_MODEL(s_sp_dock_workers_on_pipe[0].model)
				REQUEST_MODEL(s_sp_dock_workers_on_pipe[1].model)
				REQUEST_MODEL(s_sp_dock_workers_on_pipe[2].model)
				
				
				IF HAS_MODEL_LOADED(s_sp_dock_workers_on_pipe[0].model)
				AND HAS_MODEL_LOADED(s_sp_dock_workers_on_pipe[1].model)
				AND HAS_MODEL_LOADED(s_sp_dock_workers_on_pipe[2].model)
					//IF IS_SCENE_DATA_READY(s_sd_dock_workers_on_pipe)
						s_sp_dock_workers_on_pipe[0].ped = CREATE_SETPIECE_PED(s_sp_dock_workers_on_pipe[0].model,s_sp_dock_workers_on_pipe[0].v_ss_pos, s_sp_dock_workers_on_pipe[0].f_ss_rot, rel_group_buddies, 200, 0)
						s_sp_dock_workers_on_pipe[1].ped = CREATE_SETPIECE_PED(s_sp_dock_workers_on_pipe[1].model,s_sp_dock_workers_on_pipe[1].v_ss_pos, s_sp_dock_workers_on_pipe[1].f_ss_rot, rel_group_buddies, 200, 0)
						SET_PED_COMPONENT_VARIATION(s_sp_dock_workers_on_pipe[1].ped, PED_COMP_HEAD, 1, 0)
						SET_PED_COMPONENT_VARIATION(s_sp_dock_workers_on_pipe[1].ped, PED_COMP_BERD, 0, 0)
						SET_PED_COMPONENT_VARIATION(s_sp_dock_workers_on_pipe[1].ped, PED_COMP_HAIR, 0, 0)
						SET_PED_COMPONENT_VARIATION(s_sp_dock_workers_on_pipe[1].ped, PED_COMP_TORSO, 1, 0)
						SET_PED_COMPONENT_VARIATION(s_sp_dock_workers_on_pipe[1].ped, PED_COMP_LEG, 1, 0)
						SET_PED_COMPONENT_VARIATION(s_sp_dock_workers_on_pipe[1].ped, PED_COMP_HAND, 0, 0)
						SET_PED_COMPONENT_VARIATION(s_sp_dock_workers_on_pipe[1].ped, PED_COMP_FEET, 0, 0)
						SET_PED_COMPONENT_VARIATION(s_sp_dock_workers_on_pipe[1].ped, PED_COMP_TEETH, 0, 0)
						SET_PED_COMPONENT_VARIATION(s_sp_dock_workers_on_pipe[1].ped, PED_COMP_SPECIAL, 1, 0)
						SET_PED_COMPONENT_VARIATION(s_sp_dock_workers_on_pipe[1].ped, PED_COMP_SPECIAL2, 0, 0)
						SET_PED_COMPONENT_VARIATION(s_sp_dock_workers_on_pipe[1].ped, PED_COMP_DECL, 0, 0)
						SET_PED_COMPONENT_VARIATION(s_sp_dock_workers_on_pipe[1].ped, PED_COMP_JBIB, 0, 0)
						SET_PED_PROP_INDEX(s_sp_dock_workers_on_pipe[1].ped, ANCHOR_HEAD, 0)
						
						s_sp_dock_workers_on_pipe[2].ped = CREATE_SETPIECE_PED(s_sp_dock_workers_on_pipe[2].model,s_sp_dock_workers_on_pipe[2].v_ss_pos, s_sp_dock_workers_on_pipe[2].f_ss_rot, rel_group_buddies, 200, 0)
						INITIALISE_SETPIECE_GROUP(s_sp_dock_workers_on_pipe, "Working on pipe")
					//ELSE
						//GET_SCENE_DATA(SS_dock_workers_on_pipe)
					//ENDIF
				ENDIF
			ENDIF
		ELSE
			//All the enemies are triggered off the same locate, so do outside the loop.
			IF GET_DISTANCE_BETWEEN_COORDS(v_player_pos,s_sd_dock_workers_on_pipe.v_scene_pos) < 100
				SWITCH s_sp_dock_workers_on_pipe[0].i_event
					//GET PEDS INTO POSITION
					
					CASE 0
						REQUEST_ANIM_DICT("missheistdockssetup1ig_10@base")
						s_sp_dock_workers_on_pipe[0].i_event ++
					BREAK
					
					CASE 1
						IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_10@base")
							IF DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[0].ped)
							AND DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[1].ped)
							AND DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[2].ped)
								IF NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[0].ped)
								AND NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[1].ped)
								AND NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[2].ped)
									sceneDockPipe1Id = CREATE_SYNCHRONIZED_SCENE(<< -68.320, -2531.518, 5.060 >>, << 0.000, -0.000, 3.640 >>)
									TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_on_pipe[0].ped,sceneDockPipe1Id, "missheistdockssetup1ig_10@base", "talk_pipe_base_worker1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
									TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_on_pipe[1].ped, sceneDockPipe1Id, "missheistdockssetup1ig_10@base", "talk_pipe_base_worker2", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
									TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_on_pipe[2].ped, sceneDockPipe1Id, "missheistdockssetup1ig_10@base", "talk_pipe_base_worker3", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneDockPipe1Id,TRUE)
									s_sp_dock_workers_on_pipe[0].i_event ++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					//START THE ACTION
					CASE 2 
						IF GET_DISTANCE_BETWEEN_COORDS(v_player_pos,s_sd_dock_workers_on_pipe.v_scene_pos) < 20
							REQUEST_ANIM_DICT("missheistdockssetup1ig_10@laugh")
							IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_10@laugh")
								IF DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[0].ped)
								AND DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[1].ped)
								AND DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[2].ped)
									IF NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[0].ped)
									AND NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[1].ped)
									AND NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[2].ped)
										sceneDockPipe2Id = CREATE_SYNCHRONIZED_SCENE(<< -68.320, -2531.518, 5.060 >>, << 0.000, -0.000, 3.640 >>)
										TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_on_pipe[0].ped, sceneDockPipe2Id, "missheistdockssetup1ig_10@laugh", "laugh_pipe_worker1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
										TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_on_pipe[1].ped, sceneDockPipe2Id, "missheistdockssetup1ig_10@laugh", "laugh_pipe_worker2",  NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
										TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_on_pipe[2].ped, sceneDockPipe2Id, "missheistdockssetup1ig_10@laugh", "laugh_pipe_worker3",  NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
										REMOVE_ANIM_DICT("missheistdockssetup1ig_10@laugh")
										REQUEST_ANIM_DICT("missheistdockssetup1ig_10@idle_b")
										s_sp_dock_workers_on_pipe[0].i_event ++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					//LOOP AMBIENT AFTER TRIGGERING LAUGH
					CASE 3
						IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_10@idle_b")
							IF DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[0].ped)
							AND DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[1].ped)
							AND DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[2].ped)
								IF NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[0].ped)
								AND NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[1].ped)
								AND NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[2].ped)
									SET_PED_CAPSULE(s_sp_dock_workers_on_pipe[0].ped, 0.5)
									SET_PED_CAPSULE(s_sp_dock_workers_on_pipe[1].ped, 0.5)
									SET_PED_CAPSULE(s_sp_dock_workers_on_pipe[2].ped, 0.5)
									IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneDockPipe2Id)
										IF GET_SYNCHRONIZED_SCENE_PHASE(sceneDockPipe2Id) = 1
											sceneDockPipe3Id = CREATE_SYNCHRONIZED_SCENE(<< -68.320, -2531.518, 5.060 >>, << 0.000, -0.000, 3.640 >>)
											TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_on_pipe[0].ped,sceneDockPipe3Id, "missheistdockssetup1ig_10@idle_b", "talk_pipe_b_worker1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
											TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_on_pipe[1].ped, sceneDockPipe3Id, "missheistdockssetup1ig_10@idle_b", "talk_pipe_b_worker2", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
											TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_on_pipe[2].ped, sceneDockPipe3Id, "missheistdockssetup1ig_10@idle_b", "talk_pipe_b_worker3", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
											REMOVE_ANIM_DICT("missheistdockssetup1ig_10@idle_b")
											REQUEST_ANIM_DICT("missheistdockssetup1ig_10@idle_c")
											s_sp_dock_workers_on_pipe[0].i_event ++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					//LOOP AMBIENT AFTER TRIGGERING LAUGH
					CASE 4
						IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_10@idle_c")
							IF DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[0].ped)
							AND DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[1].ped)
							AND DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[2].ped)
								IF NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[0].ped)
								AND NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[1].ped)
								AND NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[2].ped)
									SET_PED_CAPSULE(s_sp_dock_workers_on_pipe[0].ped, 0.5)
									SET_PED_CAPSULE(s_sp_dock_workers_on_pipe[1].ped, 0.5)
									SET_PED_CAPSULE(s_sp_dock_workers_on_pipe[2].ped, 0.5)
									IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneDockPipe3Id)
										IF GET_SYNCHRONIZED_SCENE_PHASE(sceneDockPipe3Id) = 1
											sceneDockPipe4Id = CREATE_SYNCHRONIZED_SCENE(<< -68.320, -2531.518, 5.060 >>, << 0.000, -0.000, 3.640 >>)
											TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_on_pipe[0].ped,sceneDockPipe4Id, "missheistdockssetup1ig_10@idle_c", "talk_pipe_c_worker1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
											TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_on_pipe[1].ped, sceneDockPipe4Id, "missheistdockssetup1ig_10@idle_c", "talk_pipe_c_worker2", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
											TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_on_pipe[2].ped, sceneDockPipe4Id, "missheistdockssetup1ig_10@idle_c", "talk_pipe_c_worker3", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
											REMOVE_ANIM_DICT("missheistdockssetup1ig_10@idle_c")
											REQUEST_ANIM_DICT("missheistdockssetup1ig_10@idle_d")
											s_sp_dock_workers_on_pipe[0].i_event ++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					//LOOP AMBIENT AFTER TRIGGERING LAUGH
					CASE 5
						IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_10@idle_d")
							IF DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[0].ped)
							AND DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[1].ped)
							AND DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[2].ped)
								IF NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[0].ped)
								AND NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[1].ped)
								AND NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[2].ped)
									SET_PED_CAPSULE(s_sp_dock_workers_on_pipe[0].ped, 0.5)
									SET_PED_CAPSULE(s_sp_dock_workers_on_pipe[1].ped, 0.5)
									SET_PED_CAPSULE(s_sp_dock_workers_on_pipe[2].ped, 0.5)
									IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneDockPipe4Id)
										IF GET_SYNCHRONIZED_SCENE_PHASE(sceneDockPipe4Id) = 1
											sceneDockPipe5Id = CREATE_SYNCHRONIZED_SCENE(<< -68.320, -2531.518, 5.060 >>, << 0.000, -0.000, 3.640 >>)
											TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_on_pipe[0].ped,sceneDockPipe5Id, "missheistdockssetup1ig_10@idle_d", "talk_pipe_d_worker1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
											TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_on_pipe[1].ped, sceneDockPipe5Id, "missheistdockssetup1ig_10@idle_d", "talk_pipe_d_worker2", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
											TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_on_pipe[2].ped, sceneDockPipe5Id, "missheistdockssetup1ig_10@idle_d", "talk_pipe_d_worker3", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
											SET_SYNCHRONIZED_SCENE_LOOPED(sceneDockPipe5Id,TRUE)
											s_sp_dock_workers_on_pipe[0].i_event ++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					//FINISH UP - CLEAR TASKS ECT
					CASE 6
						IF NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[0].ped)
						AND NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[1].ped)
						AND NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[2].ped)
							IF NOT IS_ENTITY_ON_SCREEN(s_sp_dock_workers_on_pipe[0].ped)
							AND NOT IS_ENTITY_ON_SCREEN(s_sp_dock_workers_on_pipe[1].ped)
							AND NOT IS_ENTITY_ON_SCREEN(s_sp_dock_workers_on_pipe[2].ped)
							
								IF NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[0].ped)
									CLEAR_PED_TASKS(s_sp_dock_workers_on_pipe[0].ped)
									SET_PED_AS_NO_LONGER_NEEDED(s_sp_dock_workers_on_pipe[0].ped)
								ENDIF
								
								IF NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[1].ped)
									CLEAR_PED_TASKS(s_sp_dock_workers_on_pipe[1].ped)
									SET_PED_AS_NO_LONGER_NEEDED(s_sp_dock_workers_on_pipe[1].ped)
								ENDIF
								
								IF NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[2].ped)
									CLEAR_PED_TASKS(s_sp_dock_workers_on_pipe[2].ped)
									SET_PED_AS_NO_LONGER_NEEDED(s_sp_dock_workers_on_pipe[2].ped)
								ENDIF
								REMOVE_ANIM_DICT("missheistdockssetup1ig_10@idle_d")
								bSSComplete[SS_dock_workers_on_pipe] = TRUE
							ENDIF
						ENDIF
		
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
ENDIF

	/*
      __   _  _  _ ___ _____ __   __  _ ___ ____   ____ ___  _  _  _   _ _  __ 
	 / _| / \| \| |_ _| o \ V /  / _|| | \ V / _| |  \ V / \| |/ \| \_/ | |/ _|
	( |_n| o | \\ || ||   /\ /  ( |_n| U |\ /\_ \ | o ) /| \\ | o | \_/ | ( (_ 
	 \__/|_n_|_|\_||_||_|\\|_|   \__/|___||_||__/ |__/|_||_|\_|_n_|_| |_|_|\__| */
	 
IF bSetPiece[SS_GANTRY_GUYS]
	
	IF NOT DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[0].ped)
		REQUEST_MODEL(s_sp_gantry_guys_dynamic[0].model)
		REQUEST_MODEL(s_sp_gantry_guys_dynamic[1].model)
		REQUEST_MODEL(s_sp_gantry_guys_dynamic[2].model)
		REQUEST_MODEL(s_sp_gantry_guys_dynamic[3].model)

		IF HAS_MODEL_LOADED(s_sp_gantry_guys_dynamic[0].model)
		AND HAS_MODEL_LOADED(s_sp_gantry_guys_dynamic[1].model)
		AND HAS_MODEL_LOADED(s_sp_gantry_guys_dynamic[2].model)
		AND HAS_MODEL_LOADED(s_sp_gantry_guys_dynamic[3].model)
			//IF IS_SCENE_DATA_READY(s_sd_gantry_guys_dynamic)
				s_sp_gantry_guys_dynamic[0].ped = CREATE_SETPIECE_PED(s_sp_gantry_guys_dynamic[0].model,s_sp_gantry_guys_dynamic[0].v_ss_pos, s_sp_gantry_guys_dynamic[0].f_ss_rot, rel_group_buddies, 200, 0)
				s_sp_gantry_guys_dynamic[1].ped = CREATE_SETPIECE_PED(s_sp_gantry_guys_dynamic[1].model,s_sp_gantry_guys_dynamic[1].v_ss_pos, s_sp_gantry_guys_dynamic[1].f_ss_rot, rel_group_buddies, 200, 0)
				s_sp_gantry_guys_dynamic[2].ped = CREATE_SETPIECE_PED(s_sp_gantry_guys_dynamic[2].model,s_sp_gantry_guys_dynamic[2].v_ss_pos, s_sp_gantry_guys_dynamic[2].f_ss_rot, rel_group_buddies, 200, 0)
				s_sp_gantry_guys_dynamic[3].ped = CREATE_SETPIECE_PED(s_sp_gantry_guys_dynamic[3].model,s_sp_gantry_guys_dynamic[3].v_ss_pos, s_sp_gantry_guys_dynamic[3].f_ss_rot, rel_group_buddies, 200, 0)
				INITIALISE_SETPIECE_GROUP(s_sp_gantry_guys_dynamic, "Gantry guys dynamic")
			//ELSE
				//GET_SCENE_DATA(SS_GANTRY_GUYS)
			//ENDIF
		ENDIF
	ELSE
		//All the enemies are triggered off the same locate, so do outside the loop.
		IF GET_DISTANCE_BETWEEN_COORDS(v_player_pos,s_sd_gantry_guys_dynamic.v_scene_pos) < 100
			//REPEAT COUNT_OF(s_sp_gantry_guys_dynamic) i
				//IF DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[i].ped)
					//IF NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[i].ped)
						SWITCH s_sp_gantry_guys_dynamic[0].i_event
							//GET PEDS INTO POSITION
							
							CASE 0
								REQUEST_ANIM_DICT("missheistdockssetup1ig_12@base")
								s_sp_gantry_guys_dynamic[0].i_event ++
							BREAK
								
							CASE 1
								REQUEST_ANIM_DICT("missheistdockssetup1ig_12@base")
								IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_12@base")
									//LOOP OVER UNTIL PARK UP
									IF DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[0].ped)
									AND DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[1].ped)
									AND DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[2].ped)
									AND DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[3].ped)
										IF NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[0].ped)
										AND NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[1].ped)
										AND NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[2].ped)
										AND NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[3].ped)
											sceneGantryCraneDynamic1 = CREATE_SYNCHRONIZED_SCENE(s_sd_gantry_guys_dynamic.v_scene_pos,s_sd_gantry_guys_dynamic.v_scene_rot)
											TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_dynamic[0].ped,sceneGantryCraneDynamic1, "missheistdockssetup1ig_12@base", "TALK_Gantry_Idle_Base_Worker1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
											TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_dynamic[1].ped, sceneGantryCraneDynamic1,"missheistdockssetup1ig_12@base", "TALK_Gantry_Idle_Base_Worker2", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
											TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_dynamic[2].ped, sceneGantryCraneDynamic1,"missheistdockssetup1ig_12@base", "TALK_Gantry_Idle_Base_Worker3", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
											TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_dynamic[3].ped, sceneGantryCraneDynamic1,"missheistdockssetup1ig_12@base", "TALK_Gantry_Idle_Base_Worker4", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
											SET_SYNCHRONIZED_SCENE_LOOPED(sceneGantryCraneDynamic1,TRUE)
											s_sp_gantry_guys_dynamic[0].i_event ++
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							//START THE ACTION - WHEN THE PLAYER APPROACHES GET COORDS FOR CONDITION
							CASE 2 
								IF GET_DISTANCE_BETWEEN_COORDS(v_player_pos,s_sd_gantry_guys_dynamic.v_scene_pos) < 50
									REQUEST_ANIM_DICT("missheistdockssetup1ig_12@idle_a")
									IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_12@idle_a")
										IF DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[0].ped)
										AND DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[1].ped)
										AND DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[2].ped)
										AND DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[3].ped)
											IF NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[0].ped)
											AND NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[1].ped)
											AND NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[2].ped)
											AND NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[3].ped)
												sceneGantryCraneDynamic2 = CREATE_SYNCHRONIZED_SCENE(s_sd_gantry_guys_dynamic.v_scene_pos,s_sd_gantry_guys_dynamic.v_scene_rot)
												TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_dynamic[0].ped, sceneGantryCraneDynamic2, "missheistdockssetup1ig_12@idle_a", "TALK_Gantry_Idle_A_Worker1", SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
												TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_dynamic[1].ped, sceneGantryCraneDynamic2,"missheistdockssetup1ig_12@idle_a", "TALK_Gantry_Idle_A_Worker2", SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
												TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_dynamic[2].ped, sceneGantryCraneDynamic2,"missheistdockssetup1ig_12@idle_a", "TALK_Gantry_Idle_A_Worker3", SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
												TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_dynamic[3].ped, sceneGantryCraneDynamic2,"missheistdockssetup1ig_12@idle_a", "TALK_Gantry_Idle_A_Worker4", SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
												REQUEST_ANIM_DICT("missheistdockssetup1ig_12@idle_b")
												
												s_sp_gantry_guys_dynamic[0].i_event ++
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							//SECOND ANIM
							CASE 3
								REQUEST_ANIM_DICT("missheistdockssetup1ig_12@idle_b")
								IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_12@idle_b")
									IF DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[0].ped)
									AND DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[1].ped)
									AND DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[2].ped)
									AND DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[3].ped)
										IF NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[0].ped)
										AND NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[1].ped)
										AND NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[2].ped)
										AND NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[3].ped)
											IF GET_SYNCHRONIZED_SCENE_PHASE(sceneGantryCraneDynamic2) = 1
												sceneGantryCraneDynamic3 = CREATE_SYNCHRONIZED_SCENE(s_sd_gantry_guys_dynamic.v_scene_pos,s_sd_gantry_guys_dynamic.v_scene_rot)
												TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_dynamic[0].ped,sceneGantryCraneDynamic3, "missheistdockssetup1ig_12@idle_b", "TALK_Gantry_Idle_B_Worker1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
												TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_dynamic[1].ped, sceneGantryCraneDynamic3,"missheistdockssetup1ig_12@idle_b", "TALK_Gantry_Idle_B_Worker2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
												TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_dynamic[2].ped, sceneGantryCraneDynamic3,"missheistdockssetup1ig_12@idle_b", "TALK_Gantry_Idle_B_Worker3", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
												TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_dynamic[3].ped, sceneGantryCraneDynamic3,"missheistdockssetup1ig_12@idle_b", "TALK_Gantry_Idle_B_Worker4", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
												REQUEST_ANIM_DICT("missheistdockssetup1ig_12@idle_c")
												s_sp_gantry_guys_dynamic[0].i_event ++
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							//FINAL LOOP
							CASE 4
								REMOVE_ANIM_DICT("missheistdockssetup1ig_12@idle_b")
								IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_12@idle_c")
									IF DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[0].ped)
									AND DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[1].ped)
									AND DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[2].ped)
									AND DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[3].ped)
										IF NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[0].ped)
										AND NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[1].ped)
										AND NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[2].ped)
										AND NOT IS_PED_INJURED(s_sp_gantry_guys_dynamic[3].ped)
											IF GET_SYNCHRONIZED_SCENE_PHASE(sceneGantryCraneDynamic3) = 1
												sceneGantryCraneDynamic4= CREATE_SYNCHRONIZED_SCENE(s_sd_gantry_guys_dynamic.v_scene_pos,s_sd_gantry_guys_dynamic.v_scene_rot)
												TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_dynamic[0].ped, sceneGantryCraneDynamic4, "missheistdockssetup1ig_12@idle_c", "TALK_Gantry_Idle_C_Worker1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
												TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_dynamic[1].ped, sceneGantryCraneDynamic4,"missheistdockssetup1ig_12@idle_c", "TALK_Gantry_Idle_C_Worker2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
												TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_dynamic[2].ped, sceneGantryCraneDynamic4,"missheistdockssetup1ig_12@idle_c", "TALK_Gantry_Idle_C_Worker3", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
												TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_dynamic[3].ped, sceneGantryCraneDynamic4,"missheistdockssetup1ig_12@idle_c", "TALK_Gantry_Idle_C_Worker4", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
												SET_SYNCHRONIZED_SCENE_LOOPED(sceneGantryCraneDynamic4,TRUE)
												s_sp_gantry_guys_dynamic[0].i_event ++
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							BREAK
							
							//FINISH UP - CLEAR TASKS ECT
							CASE 5
								REMOVE_ANIM_DICT("missheistdockssetup1ig_12@idle_a")
								REMOVE_ANIM_DICT("missheistdockssetup1ig_12@idle_c")
							BREAK
							
						
						ENDSWITCH
					//ENDIF
				//ENDIF
			//ENDREPEAT
		ELSE
				
		ENDIF
	ENDIF
ENDIF
	
	
	/*
  __  _ ___ ____    _  ___  _  _ _ _  _ __     __  _  ___ 
 / _|| | \ V / _|  / \| o \/ \| | | \| |  \   / _|/ \| o \
( |_n| U |\ /\_ \ | o |   ( o ) U | \\ | o ) ( (_| o |   /
 \__/|___||_||__/ |_n_|_|\\\_/|___|_|\_|__/   \__|_n_|_|\\ */
 
IF bSetPiece[SS_guys_around_car]

	IF NOT DOES_ENTITY_EXIST(s_sp_guys_around_car[4].ped)
		REQUEST_MODEL(s_sp_guys_around_car[0].model)
		REQUEST_MODEL(s_sp_guys_around_car[1].model)
		REQUEST_MODEL(s_sp_guys_around_car[2].model)
		REQUEST_MODEL(s_sp_guys_around_car[3].model)
		REQUEST_MODEL(s_sp_guys_around_car[4].model)
		REQUEST_MODEL(BUFFALO)
		IF HAS_MODEL_LOADED(s_sp_guys_around_car[0].model)
		AND HAS_MODEL_LOADED(s_sp_guys_around_car[1].model)
		AND HAS_MODEL_LOADED(s_sp_guys_around_car[2].model)
		AND HAS_MODEL_LOADED(s_sp_guys_around_car[3].model)
		AND HAS_MODEL_LOADED(s_sp_guys_around_car[4].model)
		AND HAS_MODEL_LOADED(s_sv_car_to_admire[0].model)
			s_sv_car_to_admire[0].veh = CREATE_SETPIECE_VEHICLE(BUFFALO,s_sv_car_to_admire[0].v_spawn_position,s_sv_car_to_admire[0].f_start_heading)
			IF NOT s_sv_car_to_admire[0].b_is_created
				s_sv_car_to_admire[0].b_is_created = TRUE
			ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(s_sv_car_to_admire[0].model)
			s_sp_guys_around_car[0].ped = CREATE_SETPIECE_PED(s_sp_guys_around_car[0].model,s_sp_guys_around_car[0].v_spawn_position, s_sp_guys_around_car[0].f_ss_rot, rel_group_buddies, 200, 0)
			s_sp_guys_around_car[1].ped = CREATE_SETPIECE_PED(s_sp_guys_around_car[1].model,s_sp_guys_around_car[1].v_spawn_position, s_sp_guys_around_car[1].f_ss_rot, rel_group_buddies, 200, 0)
			s_sp_guys_around_car[2].ped = CREATE_SETPIECE_PED(s_sp_guys_around_car[2].model,s_sp_guys_around_car[2].v_spawn_position, s_sp_guys_around_car[2].f_ss_rot, rel_group_buddies, 200, 0)
			s_sp_guys_around_car[3].ped = CREATE_SETPIECE_PED(s_sp_guys_around_car[3].model,s_sp_guys_around_car[3].v_spawn_position, s_sp_guys_around_car[3].f_ss_rot, rel_group_buddies, 200, 0)
			s_sp_guys_around_car[4].ped = CREATE_SETPIECE_PED(s_sp_guys_around_car[4].model,s_sp_guys_around_car[4].v_spawn_position, s_sp_guys_around_car[4].f_ss_rot, rel_group_buddies, 200, 0)
			INITIALISE_SETPIECE_GROUP(s_sp_guys_around_car, "Show off car")
		ENDIF
	ELSE
		//All the enemies are triggered off the same locate, so do outside the loop.
		IF IS_VEHICLE_DRIVEABLE(s_sv_car_to_admire[0].veh)
			IF GET_DISTANCE_BETWEEN_COORDS(v_player_pos,GET_ENTITY_COORDS(s_sv_car_to_admire[0].veh)) < 100
				IF s_sp_guys_around_car[0].i_event > 1
					IF GET_DISTANCE_BETWEEN_COORDS(v_player_pos,GET_ENTITY_COORDS(s_sv_car_to_admire[0].veh)) > 20
						REMOVE_ANIM_DICT("missheistdockssetup1showcar@idle_a")
						REMOVE_ANIM_DICT("missheistdockssetup1showcar@idle_b")
						REQUEST_ANIM_DICT("missheistdockssetup1showcar@base")
						s_sp_guys_around_car[0].i_event = 1
					ENDIF
				ENDIF
				
				SWITCH s_sp_guys_around_car[0].i_event
					//GET PEDS INTO POSITION
					
					CASE 0
						REQUEST_ANIM_DICT("missheistdockssetup1showcar@base")
						IF IS_VEHICLE_DRIVEABLE(s_sv_car_to_admire[0].veh)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(s_sv_car_to_admire[0].veh,FALSE)
							FREEZE_ENTITY_POSITION(s_sv_car_to_admire[0].veh,TRUE)
						ENDIF
						s_sp_guys_around_car[0].i_event ++
					BREAK
						
					CASE 1
						IF HAS_ANIM_DICT_LOADED("missheistdockssetup1showcar@base")
							//LOOP OVER UNTIL PARK UP
							IF DOES_ENTITY_EXIST(s_sp_guys_around_car[0].ped)
							AND DOES_ENTITY_EXIST(s_sp_guys_around_car[1].ped)
							AND DOES_ENTITY_EXIST(s_sp_guys_around_car[2].ped)
							AND DOES_ENTITY_EXIST(s_sp_guys_around_car[3].ped)
							AND DOES_ENTITY_EXIST(s_sp_guys_around_car[4].ped)
								IF NOT IS_PED_INJURED(s_sp_guys_around_car[0].ped)
								AND NOT IS_PED_INJURED(s_sp_guys_around_car[1].ped)
								AND NOT IS_PED_INJURED(s_sp_guys_around_car[2].ped)
								AND NOT IS_PED_INJURED(s_sp_guys_around_car[3].ped)
								AND NOT IS_PED_INJURED(s_sp_guys_around_car[4].ped)
									sceneGuysAroundCar1 = CREATE_SYNCHRONIZED_SCENE(s_sd_guys_around_car.v_scene_pos,s_sd_guys_around_car.v_scene_rot)
									IF IS_VEHICLE_DRIVEABLE(s_sv_car_to_admire[0].veh)
										ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneGuysAroundCar1,s_sv_car_to_admire[0].veh,-1)
									ENDIF
									
									TASK_SYNCHRONIZED_SCENE (s_sp_guys_around_car[0].ped,sceneGuysAroundCar1, "missheistdockssetup1showcar@base", "base_1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
									TASK_SYNCHRONIZED_SCENE (s_sp_guys_around_car[1].ped, sceneGuysAroundCar1,"missheistdockssetup1showcar@base", "base_2", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
									TASK_SYNCHRONIZED_SCENE (s_sp_guys_around_car[2].ped, sceneGuysAroundCar1,"missheistdockssetup1showcar@base", "base_3", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
									TASK_SYNCHRONIZED_SCENE (s_sp_guys_around_car[3].ped, sceneGuysAroundCar1,"missheistdockssetup1showcar@base", "base_4", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
									TASK_SYNCHRONIZED_SCENE (s_sp_guys_around_car[4].ped, sceneGuysAroundCar1,"missheistdockssetup1showcar@base", "base_5", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneGuysAroundCar1,TRUE)
									
									SET_ENTITY_NO_COLLISION_ENTITY(s_sp_guys_around_car[0].ped,s_sv_car_to_admire[0].veh,FALSE)
									SET_ENTITY_NO_COLLISION_ENTITY(s_sp_guys_around_car[1].ped,s_sv_car_to_admire[0].veh,FALSE)
									SET_ENTITY_NO_COLLISION_ENTITY(s_sp_guys_around_car[2].ped,s_sv_car_to_admire[0].veh,FALSE)
									SET_ENTITY_NO_COLLISION_ENTITY(s_sp_guys_around_car[3].ped,s_sv_car_to_admire[0].veh,FALSE)
									SET_ENTITY_NO_COLLISION_ENTITY(s_sp_guys_around_car[4].ped,s_sv_car_to_admire[0].veh,FALSE)
									
									s_sp_guys_around_car[0].i_event ++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 2
						IF GET_DISTANCE_BETWEEN_COORDS(v_player_pos,GET_ENTITY_COORDS(s_sv_car_to_admire[0].veh)) < 15
							REQUEST_ANIM_DICT("missheistdockssetup1showcar@idle_a")
							s_sp_guys_around_car[0].i_event ++
						ENDIF
					BREAK
					
					//START THE ACTION - WHEN THE PLAYER APPROACHES GET COORDS FOR CONDITION
					CASE 3
						REQUEST_ANIM_DICT("missheistdockssetup1showcar@idle_a")
						IF HAS_ANIM_DICT_LOADED("missheistdockssetup1showcar@idle_a")
						AND DOES_ENTITY_EXIST(s_sp_guys_around_car[0].ped)
						AND DOES_ENTITY_EXIST(s_sp_guys_around_car[1].ped)
						AND DOES_ENTITY_EXIST(s_sp_guys_around_car[2].ped)
						AND DOES_ENTITY_EXIST(s_sp_guys_around_car[3].ped)
						AND DOES_ENTITY_EXIST(s_sp_guys_around_car[4].ped)
							IF NOT IS_PED_INJURED(s_sp_guys_around_car[0].ped)
							AND NOT IS_PED_INJURED(s_sp_guys_around_car[1].ped)
							AND NOT IS_PED_INJURED(s_sp_guys_around_car[2].ped)
							AND NOT IS_PED_INJURED(s_sp_guys_around_car[3].ped)
							AND NOT IS_PED_INJURED(s_sp_guys_around_car[4].ped)
								sceneGuysAroundCar2 = CREATE_SYNCHRONIZED_SCENE(s_sd_guys_around_car.v_scene_pos,s_sd_guys_around_car.v_scene_rot)
								IF IS_VEHICLE_DRIVEABLE(s_sv_car_to_admire[0].veh)
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneGuysAroundCar2,s_sv_car_to_admire[0].veh,-1)
								ENDIF
								TASK_SYNCHRONIZED_SCENE (s_sp_guys_around_car[0].ped,sceneGuysAroundCar2, "missheistdockssetup1showcar@idle_a", "idle_a_1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
								TASK_SYNCHRONIZED_SCENE (s_sp_guys_around_car[1].ped, sceneGuysAroundCar2,"missheistdockssetup1showcar@idle_a", "idle_a_2", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
								TASK_SYNCHRONIZED_SCENE (s_sp_guys_around_car[2].ped, sceneGuysAroundCar2,"missheistdockssetup1showcar@idle_a", "idle_a_3", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
								TASK_SYNCHRONIZED_SCENE (s_sp_guys_around_car[3].ped, sceneGuysAroundCar2,"missheistdockssetup1showcar@idle_a", "idle_a_4", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
								TASK_SYNCHRONIZED_SCENE (s_sp_guys_around_car[4].ped, sceneGuysAroundCar2,"missheistdockssetup1showcar@idle_a", "idle_a_5", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
								SET_SYNCHRONIZED_SCENE_LOOPED(sceneGuysAroundCar2,TRUE)
								REMOVE_ANIM_DICT("missheistdockssetup1showcar@idle_a")
								REQUEST_ANIM_DICT("missheistdockssetup1showcar@idle_b")
								s_sp_guys_around_car[0].i_event ++
							ENDIF
						ENDIF
					BREAK
					
					//SECOND ANIM
					CASE 4
						REQUEST_ANIM_DICT("missheistdockssetup1showcar@idle_b")
						IF HAS_ANIM_DICT_LOADED("missheistdockssetup1showcar@idle_b")
							IF DOES_ENTITY_EXIST(s_sp_guys_around_car[0].ped)
							AND DOES_ENTITY_EXIST(s_sp_guys_around_car[1].ped)
							AND DOES_ENTITY_EXIST(s_sp_guys_around_car[2].ped)
							AND DOES_ENTITY_EXIST(s_sp_guys_around_car[3].ped)
							AND DOES_ENTITY_EXIST(s_sp_guys_around_car[4].ped)
								IF NOT IS_PED_INJURED(s_sp_guys_around_car[0].ped)
								AND NOT IS_PED_INJURED(s_sp_guys_around_car[1].ped)
								AND NOT IS_PED_INJURED(s_sp_guys_around_car[2].ped)
								AND NOT IS_PED_INJURED(s_sp_guys_around_car[3].ped)
								AND NOT IS_PED_INJURED(s_sp_guys_around_car[4].ped)
									IF GET_SYNCHRONIZED_SCENE_PHASE(sceneGuysAroundCar2) = 1	
										sceneGuysAroundCar3 = CREATE_SYNCHRONIZED_SCENE(s_sd_guys_around_car.v_scene_pos,s_sd_guys_around_car.v_scene_rot)
										IF IS_VEHICLE_DRIVEABLE(s_sv_car_to_admire[0].veh)
											ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneGuysAroundCar3,s_sv_car_to_admire[0].veh,-1)
										ENDIF
										TASK_SYNCHRONIZED_SCENE (s_sp_guys_around_car[0].ped,sceneGuysAroundCar3, "missheistdockssetup1showcar@idle_b", "idle_b_1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
										TASK_SYNCHRONIZED_SCENE (s_sp_guys_around_car[1].ped, sceneGuysAroundCar3,"missheistdockssetup1showcar@idle_b", "idle_b_2", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
										TASK_SYNCHRONIZED_SCENE (s_sp_guys_around_car[2].ped, sceneGuysAroundCar3,"missheistdockssetup1showcar@idle_b", "idle_b_3", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
										TASK_SYNCHRONIZED_SCENE (s_sp_guys_around_car[3].ped, sceneGuysAroundCar3,"missheistdockssetup1showcar@idle_b", "idle_b_4", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
										TASK_SYNCHRONIZED_SCENE (s_sp_guys_around_car[4].ped, sceneGuysAroundCar3,"missheistdockssetup1showcar@idle_b", "idle_b_5", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
										SET_SYNCHRONIZED_SCENE_LOOPED(sceneGuysAroundCar3,TRUE)
										s_sp_guys_around_car[0].i_event ++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					BREAK
					
					//FINAL LOOP
					CASE 5
						REMOVE_ANIM_DICT("missheistdockssetup1showcar@idle_b")
						REMOVE_ANIM_DICT("missheistdockssetup1showcar@idle_a")
						s_sp_guys_around_car[0].i_event ++
					BREAK
					
				
				ENDSWITCH
			ELSE
				REMOVE_ANIM_DICT("missheistdockssetup1showcar@idle_b")
				REMOVE_ANIM_DICT("missheistdockssetup1showcar@idle_a")
			ENDIF
		ELSE
			//SCRIPT_ASSERT("Set Piece Car Not Drivable")
		ENDIF
	ENDIF
ENDIF

 
 /*
  _ _ _ ___ _   __  ___ ___ 
| | | | __| | |  \| __| o \
| V V | _|| |_| o ) _||   /
 \_n_/|___|___|__/|___|_|\\ */
 
 //ANIMS DONE
IF NOT DOES_ENTITY_EXIST(s_sp_welder[0].ped)
	REQUEST_MODEL(s_sp_welder[0].model)
	REQUEST_MODEL(PROP_WELD_TORCH)
	IF HAS_MODEL_LOADED(s_sp_welder[0].model)
	AND HAS_MODEL_LOADED(PROP_WELD_TORCH)
		IF s_sp_welder[0].i_event < 1
			s_sp_welder[0].ped = CREATE_SETPIECE_PED(s_sp_welder[0].model,s_sp_welder[0].v_spawn_position, s_sp_welder[0].f_start_heading, rel_group_buddies, 200, 0)
			s_sp_welder[0].obj_prop = CREATE_OBJECT(PROP_WELD_TORCH,<<-110.56, -2463.63, 5.04>>)
			SET_RAGDOLL_BLOCKING_FLAGS(s_sp_welder[0].ped, RBF_PLAYER_IMPACT)
			//SET_ENTITY_HEADING(s_sp_welder[0].obj_prop,s_sp_welder[0].f_start_heading)
			INITIALISE_SETPIECE_GROUP(s_sp_welder, "Welder")
		ENDIF
	ENDIF
ELSE
//All the enemies are triggered off the same locate, so do outside the loop.
	IF bSetPiece[SS_WELDER]
		
		SWITCH s_sp_welder[0].i_event
			
			//GET PEDS INTO POSITION
			
			CASE 0
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< -102.1558, -2475.9209, 5.0203 >>,<<10,10,10>>)
					REQUEST_ANIM_DICT("missheistdockssetup1ig_3@enter")
					REQUEST_ANIM_DICT("missheistdockssetup1ig_3@talk")
					s_sp_welder[0].i_event ++
				ENDIF
			BREAK
			
			CASE 1
				IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_3@enter")
					IF DOES_ENTITY_EXIST(s_sp_welder[0].ped)
						IF NOT IS_PED_INJURED(s_sp_welder[0].ped)
							IF NOT IS_ENTITY_ON_SCREEN(s_sp_welder[0].ped)
								sceneWelder = CREATE_SYNCHRONIZED_SCENE(s_sd_welder.v_scene_pos,s_sd_welder.v_scene_rot)
								TASK_SYNCHRONIZED_SCENE (s_sp_welder[0].ped, sceneWelder, "missheistdockssetup1ig_3@enter", "welding_enter_dockworker", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_DONT_INTERRUPT | SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
								ATTACH_ENTITY_TO_ENTITY(s_sp_welder[0].obj_prop, s_sp_welder[0].ped, GET_PED_BONE_INDEX(s_sp_welder[0].ped, BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>)
								REMOVE_ANIM_DICT("missheistdockssetup1ig_3@enter")
								s_sp_welder[0].i_event ++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_3@talk")
					IF DOES_ENTITY_EXIST(s_sp_welder[0].ped)
						IF NOT IS_PED_INJURED(s_sp_welder[0].ped)
							SET_PED_CAPSULE(s_sp_welder[0].ped, 0.4)
							IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneWelder)
//								IF NOT HAS_LABEL_BEEN_TRIGGERED("PICK UP WELDER")
//									IF GET_SYNCHRONIZED_SCENE_PHASE(sceneWelder) > 0.66
//										ATTACH_ENTITY_TO_ENTITY(s_sp_welder[0].obj_prop, s_sp_welder[0].ped, GET_PED_BONE_INDEX(s_sp_welder[0].ped, BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>)
//										SET_LABEL_AS_TRIGGERED("PICK UP WELDER",TRUE)
//									ENDIF
//								ENDIF
								IF GET_SYNCHRONIZED_SCENE_PHASE(sceneWelder) = 1
									sceneWelder = CREATE_SYNCHRONIZED_SCENE(s_sd_welder.v_scene_pos,s_sd_welder.v_scene_rot)
									TASK_SYNCHRONIZED_SCENE (s_sp_welder[0].ped, sceneWelder, "missheistdockssetup1ig_3@talk", "oh_hey_vin_dockworker", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneWelder,TRUE)
									REMOVE_ANIM_DICT("missheistdockssetup1ig_3@talk")
									s_sp_welder[0].i_event ++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF NOT IS_PED_INJURED(s_sp_welder[0].ped)
					SET_PED_CAPSULE(s_sp_welder[0].ped, 0.4)
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDIF
ENDIF
 
 /*

 Do anim dictionary stuff set up vectors etc
 __   _   __ _  _  _ _ _  _  ___ _  ____ ___ __   ___ _  _   _  __ _  _  __ 
|  \ / \ / _| |// | | | |/ \| o \ |// __| o Y _| |_ _/ \| | | |// | \| |/ _|
| o | o | (_|  (  | V V ( o )   /  (| _||   |_ \  | | o | |_|  (| | \\ ( |_n
|__/ \_/ \__|_|\\  \_n_/ \_/|_|\\_|\\___|_|\\__/  |_|_n_|___|_|\\_|_|\_|\__/ */

	IF NOT DOES_ENTITY_EXIST(s_sp_dock_workers_talking[0].ped)
		REQUEST_MODEL(s_sp_dock_workers_talking[0].model)
		REQUEST_MODEL(s_sp_dock_workers_talking[1].model)
		
		IF HAS_MODEL_LOADED(s_sp_dock_workers_talking[0].model)
		AND HAS_MODEL_LOADED(s_sp_dock_workers_talking[1].model)
			//<<-110.41, -2481.24, 6.02>>
			IF s_sp_dock_workers_talking[0].i_event < 0
				s_sp_dock_workers_talking[0].ped = CREATE_SETPIECE_PED(s_sp_dock_workers_talking[0].model,s_sp_dock_workers_talking[0].v_spawn_position, s_sp_dock_workers_talking[0].f_start_heading, rel_group_buddies, 200, 0)
				s_sp_dock_workers_talking[1].ped = CREATE_SETPIECE_PED(s_sp_dock_workers_talking[1].model,s_sp_dock_workers_talking[1].v_spawn_position, s_sp_dock_workers_talking[1].f_start_heading, rel_group_buddies, 200, 0)
				INITIALISE_SETPIECE_GROUP(s_sp_dock_workers_talking, "Dockworkers talking by crates")
			ENDIF
		ENDIF	
	ELSE
		//All the enemies are triggered off the same locate, so do outside the loop.
		IF bSetPiece[Ss_dock_workers_talking]
				
				IF s_sp_dock_workers_talking[0].i_event = 0
					IF GET_DISTANCE_BETWEEN_COORDS(v_player_pos,s_sp_dock_workers_talking[0].v_spawn_position) < 20
						s_sp_dock_workers_talking[0].i_event = 1
					ELSE
						REMOVE_ANIM_DICT("missheistdockssetup1ig_5@base")
					ENDIF
				ENDIF
				

				SWITCH s_sp_dock_workers_talking[0].i_event
					//GET PEDS INTO POSITION
					
					CASE 0
					
					BREAK
					
					CASE 1
						REQUEST_ANIM_DICT("missheistdockssetup1ig_5@base")
						s_sp_dock_workers_talking[0].i_event ++
					BREAK
					
					CASE 2
						REQUEST_ANIM_DICT("missheistdockssetup1ig_5@base")
						IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_5@base")
							IF DOES_ENTITY_EXIST(s_sp_dock_workers_talking[1].ped)
							AND DOES_ENTITY_EXIST(s_sp_dock_workers_talking[0].ped)
								IF NOT IS_PED_INJURED(s_sp_dock_workers_talking[1].ped)
								AND NOT IS_PED_INJURED(s_sp_dock_workers_talking[0].ped)
								
									sceneDockWorkerTalking1A = CREATE_SYNCHRONIZED_SCENE(<< -110.402, -2481.259, 6.018 >>,<< 0.000, 0.000, -122.000 >>)
									TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_talking[0].ped, sceneDockWorkerTalking1A, "missheistdockssetup1ig_5@base","workers_talking_base_dockworker1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneDockWorkerTalking1A,TRUE)
									
									sceneDockWorkerTalking2A = CREATE_SYNCHRONIZED_SCENE(<< -109.253, -2481.959, 6.018 >>,<< 0.000, 0.000, 26.640 >>)
									TASK_SYNCHRONIZED_SCENE (s_sp_dock_workers_talking[1].ped, sceneDockWorkerTalking2A, "missheistdockssetup1ig_5@base","workers_talking_base_dockworker2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneDockWorkerTalking2A,TRUE)
									
									s_sp_dock_workers_talking[0].i_event ++
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 3
					
					BREAK
					
					
				ENDSWITCH
			ENDIF
		ENDIF

/*
 ___ _  ___ _  __   _ ___ ___   __ _ _ ___ ___ ___ _ _ _ __  _  ___    _  
| __/ \| o \ |// | | | __|_ _| / _| | | o \ __| o \ | | / _|/ \| o \  / \ 
| _( o )   /  (| |_| | _| | |  \_ \ U |  _/ _||   / V | \_ ( o )   / | o |
|_| \_/|_|\\_|\\___|_|_|  |_|  |__/___|_| |___|_|\\\_/|_|__/\_/|_|\\ |_n_|*/

IF bSetPiece[SS_FORKLIFT_SUPERVISOR_A]
	IF NOT DOES_ENTITY_EXIST(s_sp_forklift_supervisorA[0].ped)
		IF eMissionStage > MISSION_STAGE_1
			REQUEST_MODEL(s_sp_forklift_supervisorA[0].model)
			
			IF HAS_MODEL_LOADED(s_sp_forklift_supervisorA[0].model)
				s_sp_forklift_supervisorA[0].ped = CREATE_SETPIECE_PED(s_sp_forklift_supervisorA[0].model,s_sp_forklift_supervisorA[0].v_spawn_position, s_sp_forklift_supervisorA[0].f_start_heading, rel_group_buddies, 200, 0)
				INITIALISE_SETPIECE_GROUP(s_sp_forklift_supervisorA, "Supervisor A")
				
				//Set supervisor to specific variation
				SET_PED_COMPONENT_VARIATION(s_sp_forklift_supervisorA[0].ped, INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(s_sp_forklift_supervisorA[0].ped, INT_TO_ENUM(PED_COMPONENT,3), 0, 2, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(s_sp_forklift_supervisorA[0].ped, INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(s_sp_forklift_supervisorA[0].ped, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
				
			ENDIF
		ENDIF
	ELSE
		//All the enemies are triggered off the same locate, so do outside the loop.
		IF GET_DISTANCE_BETWEEN_COORDS(v_player_pos,s_sd_forklift_supervisor_a.v_scene_pos) < 50
			SWITCH s_sp_forklift_supervisorA[0].i_event
				
				CASE 0
					REQUEST_ANIM_DICT("missheistdockssetup1ig_9@start_idle")
					s_sp_forklift_supervisorA[0].i_event ++
				BREAK
				
				CASE 1
					IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_9@start_idle")
						IF NOT DOES_ENTITY_EXIST(objClipboard[1])
							objClipboard[1] = CREATE_OBJECT(P_CS_CLIPBOARD,<<-131.08, -2423.25, 8.00>>)
						ELSE
							IF NOT DOES_ENTITY_EXIST(objPencil[1])
								objPencil[1] = CREATE_OBJECT(PROP_PENCIL_01,<<-131.08, -2423.25, 8.00>>)
							ELSE
								IF DOES_ENTITY_EXIST(s_sp_forklift_supervisorA[0].ped)
									IF NOT IS_PED_INJURED(s_sp_forklift_supervisorA[0].ped)
										ATTACH_ENTITY_TO_ENTITY(objPencil[1],s_sp_forklift_supervisorA[0].ped,GET_PED_BONE_INDEX(s_sp_forklift_supervisorA[0].ped, BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>,TRUE)
										ATTACH_ENTITY_TO_ENTITY(objClipboard[1],s_sp_forklift_supervisorA[0].ped,GET_PED_BONE_INDEX(s_sp_forklift_supervisorA[0].ped, BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>,TRUE)
										TASK_PLAY_ANIM(s_sp_forklift_supervisorA[0].ped,"missheistdockssetup1ig_9@start_idle", "forklift_supervise_idlebase_supervisor",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
										SET_PED_CAN_RAGDOLL(s_sp_forklift_supervisorA[0].ped,TRUE)
										s_sp_forklift_supervisorA[0].i_event ++
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				BREAK
				
				CASE 2
				
				BREAK
				
			ENDSWITCH
		ENDIF
	ENDIF
ENDIF

/*
 ___ _  ___ _  __   _ ___ ___   __ _ _ ___ ___ ___ _ _ _ __  _  ___   ___ 
| __/ \| o \ |// | | | __|_ _| / _| | | o \ __| o \ | | / _|/ \| o \ | o )
| _( o )   /  (| |_| | _| | |  \_ \ U |  _/ _||   / V | \_ ( o )   / | o \
|_| \_/|_|\\_|\\___|_|_|  |_|  |__/___|_| |___|_|\\\_/|_|__/\_/|_|\\ |___/*/

IF bSetPiece[SS_FORKLIFT_SUPERVISOR_B]
	IF NOT DOES_ENTITY_EXIST(s_sp_forklift_supervisorB[0].ped)
		IF eMissionStage > MISSION_STAGE_1 
		AND eMissionStage < MISSION_STAGE_5
			
			REQUEST_MODEL(s_sp_forklift_supervisorB[0].model)
			IF HAS_MODEL_LOADED(s_sp_forklift_supervisorB[0].model)
				IF IS_SCENE_DATA_READY(s_sd_forklift_supervisor_b)
					s_sp_forklift_supervisorB[0].ped = CREATE_SETPIECE_PED(s_sp_forklift_supervisorB[0].model,s_sp_forklift_supervisorB[0].v_spawn_position, s_sp_dock_workers_talking[0].f_start_heading, rel_group_buddies, 200, 0)
					INITIALISE_SETPIECE_GROUP(s_sp_forklift_supervisorA, "Supervisor B")
				ELSE
					GET_SCENE_DATA(SS_FORKLIFT_SUPERVISOR_B)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		//All the enemies are triggered off the same locate, so do outside the loop.
		IF GET_DISTANCE_BETWEEN_COORDS(v_player_pos,s_sd_forklift_supervisor_b.v_scene_pos) < 50
			//REPEAT COUNT_OF(s_sp_forklift_supervisorB) i
				//IF DOES_ENTITY_EXIST(s_sp_forklift_supervisorB[i].ped)
					//IF NOT IS_PED_INJURED(s_sp_forklift_supervisorB[i].ped)
						SWITCH s_sp_forklift_supervisorB[0].i_event
							//GET PEDS INTO POSITION
							
							CASE 0
								REQUEST_ANIM_DICT("missheistdockssetup1ig_2_p1@new_structure")
								s_sp_forklift_supervisorB[0].i_event ++
							BREAK
							
							CASE 1
								IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_2_p1@new_structure")
									IF DOES_ENTITY_EXIST(s_sp_forklift_supervisorB[0].ped)
										IF NOT IS_PED_INJURED(s_sp_forklift_supervisorB[0].ped)
											sceneForkliftB1= CREATE_SYNCHRONIZED_SCENE(s_sd_forklift_supervisor_b.v_scene_pos,s_sd_forklift_supervisor_b.v_scene_rot)
											//TASK_SYNCHRONIZED_SCENE (s_sp_forklift_supervisorB[0].ped, sceneForkliftB1, "missheistdockssetup1ig_12", "TALK_Gantry_Idle_C_Worker1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_NONE )
											SET_SYNCHRONIZED_SCENE_LOOPED(sceneForkliftB1,TRUE)
											s_sp_forklift_supervisorB[0].i_event ++
										ENDIF
									ENDIF
								ENDIF
				
							BREAK
							
							CASE 2
							
							BREAK
							
						ENDSWITCH
					//ENDIF
				//ENDIF
			//ENDREPEAT
		ELSE
	
		ENDIF
	ENDIF
ENDIF

/*
 __ ___ _ ___ _  __    __ ___  _  _  _ ___    __  _ ___ ____ 
/ _|_ _/ \_ _| |/ _|  / _| o \/ \| \| | __|  / _|| | \ V / _|
\_ \| | o | || ( (_  ( (_|   / o | \\ | _|  ( |_n| U |\ /\_ \
|__/|_|_n_|_||_|\__|  \__|_|\\_n_|_|\_|___|  \__/|___||_||__/ */
 
 //ANIMS DONE
 
IF bSetPiece[SS_STATIC_GANTRY_GUYS]
	IF NOT DOES_ENTITY_EXIST(s_sp_gantry_guys_static[0].ped)
		REQUEST_MODEL(s_sp_gantry_guys_static[0].model)
		REQUEST_MODEL(s_sp_gantry_guys_static[1].model)
		IF HAS_MODEL_LOADED(s_sp_gantry_guys_static[0].model)
		AND HAS_MODEL_LOADED(s_sp_gantry_guys_static[1].model)
			//IF IS_SCENE_DATA_READY(s_sd_gantry_guys_static)
				s_sp_gantry_guys_static[0].ped = CREATE_SETPIECE_PED(s_sp_gantry_guys_static[0].model,s_sp_gantry_guys_static[0].v_ss_pos, s_sp_gantry_guys_static[0].f_ss_rot, rel_group_buddies, 200, 0)
				s_sp_gantry_guys_static[1].ped = CREATE_SETPIECE_PED(s_sp_gantry_guys_static[1].model,s_sp_gantry_guys_static[1].v_ss_pos, s_sp_gantry_guys_static[1].f_ss_rot, rel_group_buddies, 200, 0)
				INITIALISE_SETPIECE_GROUP(s_sp_gantry_guys_static, "Static Crane guys")
			//ELSE
				//GET_SCENE_DATA(SS_STATIC_GANTRY_GUYS)
			//ENDIF
		ENDIF
	ELSE
		//All the enemies are triggered off the same locate, so do outside the loop.
		//REPEAT COUNT_OF(s_sp_gantry_guys_static) i
			IF GET_DISTANCE_BETWEEN_COORDS(v_player_pos,s_sd_gantry_guys_static.v_scene_pos) < 100
				//IF DOES_ENTITY_EXIST(s_sp_gantry_guys_static[i].ped)
					//IF NOT IS_PED_INJURED(s_sp_gantry_guys_static[i].ped)
						SWITCH s_sp_gantry_guys_static[0].i_event
							//GET PEDS INTO POSITION
							
							CASE 0
								REQUEST_ANIM_DICT("missheistdockssetup1ig_12@base")
								s_sp_gantry_guys_static[0].i_event ++
							BREAK
							
							CASE 1
								IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_12@base")
									IF DOES_ENTITY_EXIST(s_sp_gantry_guys_static[0].ped)
									AND DOES_ENTITY_EXIST(s_sp_gantry_guys_static[1].ped)
										IF NOT IS_PED_INJURED(s_sp_gantry_guys_static[0].ped)
										AND NOT IS_PED_INJURED(s_sp_gantry_guys_static[1].ped)
											sceneCraneStatic = CREATE_SYNCHRONIZED_SCENE(s_sd_gantry_guys_static.v_scene_pos,s_sd_gantry_guys_static.v_scene_rot)
											TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_static[0].ped, sceneCraneStatic,"missheistdockssetup1ig_12@base", "talk_gantry_idle_base_worker2", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
											TASK_SYNCHRONIZED_SCENE (s_sp_gantry_guys_static[1].ped, sceneCraneStatic,"missheistdockssetup1ig_12@base", "talk_gantry_idle_base_worker1", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
											SET_SYNCHRONIZED_SCENE_LOOPED(sceneCraneStatic,TRUE)
											s_sp_gantry_guys_static[0].i_event ++
										ENDIF
									ENDIF
								ENDIF
				
							BREAK
							
							CASE 2
			
							BREAK
							
						ENDSWITCH
					ENDIF
				//ENDIF
			//ENDIF
		//ENDREPEAT
	ENDIF
ENDIF
	 
	 
ENDPROC


// ======================================================================
//                        The Crane
// ======================================================================


FUNC BOOL CAN_CRANE_PICK_UP_CONTAINER(VECTOR v_container_pos, VECTOR v_spreader_pos)
	IF NOT bForceNoGrab
		IF ABSF(v_container_pos.x - v_spreader_pos.x) < fVal1
			IF ABSF(v_container_pos.y - v_spreader_pos.y) < fVal2
				IF (v_spreader_pos.z - v_container_pos.z) > fVal3
					IF (v_spreader_pos.z - v_container_pos.z) < fVal4
						RETURN TRUE
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


INT iSoundAttachementTime

PROC CHECK_CONTAINER_ATTACHING(OBJECT_INDEX &obj_container)

	//VECTOR vTempOffset
	IF NOT bForceNoGrab
		IF HAS_ANIM_DICT_LOADED("map_objects")
			IF IS_ENTITY_ATTACHED(obj_container)
				PLAY_ENTITY_ANIM(s_crane.obj_spreader, "Dock_crane_SLD_unload", "map_objects", NORMAL_BLEND_IN, FALSE, TRUE)
				DETACH_ENTITY(obj_container, TRUE, FALSE)
				SET_OBJECT_PHYSICS_PARAMS(obj_container, -1.0, -1.0, <<-1.0, -1.0, -1.0>>, <<-1.0, -1.0, -1.0>>)
				PLAY_SOUND_FROM_ENTITY(-1, "Detach_Container", obj_container, "CRANE_SOUNDS")
				iSoundAttachementTime = GET_GAME_TIMER()
				s_crane.b_container_attached = FALSE
			ELSE
				IF NOT s_crane.b_container_attached 
					IF CAN_CRANE_PICK_UP_CONTAINER(GET_ENTITY_COORDS(obj_container), GET_ENTITY_COORDS(s_crane.obj_spreader))
						PLAY_ENTITY_ANIM(s_crane.obj_spreader, "Dock_crane_SLD_load", "map_objects", NORMAL_BLEND_IN, FALSE, TRUE)
						FREEZE_ENTITY_POSITION(obj_container, FALSE)
		
						ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(obj_container, s_crane.obj_spreader, -1, -1, vAttach1, vAttach2, vAttachRot, -1.0, b1, b2, b3, b4)
						PLAY_SOUND_FROM_ENTITY(-1, "Attach_Container", obj_container, "CRANE_SOUNDS")
						iSoundAttachementTime = GET_GAME_TIMER()
						s_crane.b_container_attached = TRUE	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

ENUM CRANE_ACTION
	CRANE_ACTION_SPREADER,
	CRANE_ACTION_CABIN,
	CRANE_ACTION_GRABBER,
	CRANE_ACTION_CRANE
ENDENUM

PROC INHIBIT_CRANE(CRANE_ACTION eAction)

	IF eAction = CRANE_ACTION_SPREADER
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y)
	ENDIF
	
	IF eAction = CRANE_ACTION_GRABBER
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
	ENDIF

	IF eAction = CRANE_ACTION_CABIN
		IF b_is_crane_cinematic_active
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
		ELSE
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
		ENDIF
	ENDIF
	
	IF eAction = CRANE_ACTION_CRANE
		IF b_is_crane_cinematic_active
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y)
		ELSE
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X)
		ENDIF
	ENDIF

ENDPROC

INT iMapAngle = 270
INT iOverheadMapAngle = 225

PROC MANAGE_CRANE(BOOL b_force_cam_into_position = FALSE)
	
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FakePortContainersPO102"), -80.0,-2410)
	
	IF b_is_crane_cinematic_active
		LOCK_MINIMAP_ANGLE(iMapAngle)
		IF NOT HAS_LABEL_BEEN_TRIGGERED("CRANE TIMECYCLE")
			CASCADE_SHADOWS_INIT_SESSION()
			CLEAR_TIMECYCLE_MODIFIER()
			SET_TIMECYCLE_MODIFIER("crane_cam")
			SET_LABEL_AS_TRIGGERED("CRANE TIMECYCLE",TRUE)
			SET_LABEL_AS_TRIGGERED("CINEMATIC TIMECYCLE",FALSE)
		ENDIF
	ELSE
		LOCK_MINIMAP_ANGLE(iOverheadMapAngle)
		IF NOT HAS_LABEL_BEEN_TRIGGERED("CINEMATIC TIMECYCLE")
			CASCADE_SHADOWS_SET_SPLIT_Z_EXP_WEIGHT(0.5)
			CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.2)
			CLEAR_TIMECYCLE_MODIFIER()
			SET_TIMECYCLE_MODIFIER("crane_cam_cinematic")
			SET_LABEL_AS_TRIGGERED("CINEMATIC TIMECYCLE",TRUE)
			SET_LABEL_AS_TRIGGERED("CRANE TIMECYCLE",FALSE)
		ENDIF
	ENDIF

	INT i_left_x = 0
	INT i_left_y = 0
	INT i_right_y = 0
	//,i_right_x
	
	IF bInhibitCrane = FALSE
		//GET_POSITION_OF_ANALOGUE_STICKS(PAD1, i_left_x, i_left_y, i_right_x, i_right_y)
		i_left_x = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_X) - 128
		i_left_y = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_SCRIPT_RIGHT_AXIS_Y) - 128
		i_right_y = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) - 128
	ENDIF
	
	INT i = 0
	INT iCurrentlyAttachedContainer = -1
	REPEAT COUNT_OF(s_Containers) i 
		IF DOES_ENTITY_EXIST(s_Containers[i].obj_Main)
			IF IS_ENTITY_ATTACHED(s_Containers[i].obj_Main)
				iCurrentlyAttachedContainer = i
			ENDIF
		ENDIF
	ENDREPEAT
	
	//IMRAN assigned to d pad
//	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
//		i_left_x = 127
//		//i_left_y = -127
//	ENDIF
//	
//	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
//		i_left_x = -127
//		//i_left_y = 127
//	ENDIF
//	
//	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
//		i_left_y = -127
//		//i_left_x = -127
//	ENDIF
//	
//	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
//		i_left_y = 127
//		//i_left_x = 127
//	ENDIF
	
	//Cap really small values to prevent jittering
	IF ABSI(i_Left_X) < 15
		i_Left_X = 0
	ENDIF
	
	IF ABSI(i_Left_Y) < 15
		i_Left_Y = 0
	ENDIF
	
	IF ABSI(i_Right_Y) < 15
		i_Right_Y = 0
	ENDIF
	

	//Detect if the spreader has hit the ground. If this is the case don't unwind ropes and don't move the camera.
	
	VECTOR v_spreader_vel = GET_ENTITY_VELOCITY(s_crane.obj_spreader)
	VECTOR v_spreader_pos = GET_ENTITY_COORDS(s_crane.obj_spreader)
	VECTOR v_spreader_rot = GET_ENTITY_ROTATION(s_crane.obj_spreader)
	
	VECTOR vLeftOffset1, vLeftOffset2, vRightOffset1, vRightOffset2, vBottomOffset1, vBottomOffset2
	
	IF iCurrentlyAttachedContainer >= 0
		vBottomOffset1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, <<0.0, 5.0, -3.5>>)
		vBottomOffset2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, <<0.0, -5.0, -3.5>>)
	ELSE
		vBottomOffset1 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, <<0.0, 5.0, -1.0>>)
		vBottomOffset2 = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, <<0.0, -5.0, -1.0>>)
	ENDIF
	
	REQUEST_ANIM_DICT("map_objects")

	IF NOT s_crane.b_spreader_stuck_on_ground
		IF i_right_y >= 1
			s_crane.i_num_vertical_movement_frames++

			IF s_crane.i_num_vertical_movement_frames > 10
				IF v_spreader_vel.z > -0.1
				AND ABSF(v_spreader_rot.y) < 5.0
				AND ABSF(s_crane.v_prev_spreader_rot.y - v_spreader_rot.y) < 0.5
					s_crane.b_spreader_stuck_on_ground = TRUE
					s_crane.i_num_vertical_movement_frames = 0
					s_crane.i_num_cabin_movement_frames = 0
				ENDIF
			ENDIF
		ELSE
			s_crane.i_num_vertical_movement_frames = 0
			s_crane.i_num_cabin_movement_frames = 0
		ENDIF
	ELSE
		IF i_left_x != 0
			s_crane.i_num_cabin_movement_frames++
		ENDIF
			
		IF ABSF(s_crane.v_prev_spreader_pos.z - v_spreader_pos.z) > 0.01
		OR ABSF(s_crane.v_prev_spreader_rot.y - v_spreader_rot.y) > 0.5
		OR ABSF(v_spreader_rot.y) > 5.0
		OR ABSF(v_spreader_rot.x) > 5.0
			INHIBIT_CRANE(CRANE_ACTION_SPREADER)
			s_crane.b_spreader_stuck_on_ground = FALSE
			s_crane.i_num_vertical_movement_frames = 0
			s_crane.i_num_cabin_movement_frames = 0
		ENDIF
	ENDIF

	s_crane.v_prev_spreader_pos = v_spreader_pos
	s_crane.v_prev_spreader_rot = v_spreader_rot
	
	REPEAT 8 i
		STOP_ROPE_UNWINDING_FRONT(s_crane.ropes[i])
		STOP_ROPE_WINDING(s_crane.ropes[i])
		
		IF i_right_y < 0				
			START_ROPE_WINDING(s_crane.ropes[i])
		ENDIF

		IF i_right_y > 0
			START_ROPE_UNWINDING_FRONT(s_crane.ropes[i])	
		ENDIF
	ENDREPEAT
	
	//Detect if the spreader is being pushed downwards into map collision, if this is the case we need to disable rope unwinding.
	IF i_Right_Y > 0
		FLOAT fGroundZ1, fGroundZ2
		GET_GROUND_Z_FOR_3D_COORD(vBottomOffset1 + <<0.0, 0.0, 50.0>>, fGroundZ1)
		GET_GROUND_Z_FOR_3D_COORD(vBottomOffset2 + <<0.0, 0.0, 50.0>>, fGroundZ2)
		
		IF fGroundZ1 > vBottomOffset1.z OR fGroundZ2 > vBottomOffset2.z
			i_Right_Y = 0
		ENDIF
		
		//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		//DRAW_DEBUG_SPHERE(vBottomOffset1, 0.1)
		//DRAW_DEBUG_SPHERE(vBottomOffset2, 0.1)
	ENDIF
	
	//Detect if the spreader is moving sideways into map collision, if this is the case we need to disable movement in a direction based on which side is touching.
	IF i_Left_X != 0
		FLOAT fLeftGroundZ1, fLeftGroundZ2, fRightGroundZ1, fRightGroundZ2
		GET_GROUND_Z_FOR_3D_COORD(vLeftOffset1 + <<0.0, 0.0, 50.0>>, fLeftGroundZ1)
		GET_GROUND_Z_FOR_3D_COORD(vLeftOffset2 + <<0.0, 0.0, 50.0>>, fLeftGroundZ2)
		GET_GROUND_Z_FOR_3D_COORD(vRightOffset1 + <<0.0, 0.0, 50.0>>, fRightGroundZ1)
		GET_GROUND_Z_FOR_3D_COORD(vRightOffset2 + <<0.0, 0.0, 50.0>>, fRightGroundZ2)
		IF fLeftGroundZ1 > vLeftOffset1.z OR fLeftGroundZ2 > vLeftOffset2.z
			IF i_Left_X < 0
				PRINTSTRING("INHIBIT SPREADER SIDEWAYS")PRINTNL()
				//INHIBIT_CRANE(CRANE_ACTION_CABIN)
				i_Left_X = 0
			ENDIF
		ELIF fRightGroundZ1 > vRightOffset1.z OR fRightGroundZ2 > vRightOffset2.z
			IF i_Left_X > 0
				PRINTSTRING("INHIBIT SPREADER SIDEWAYS")PRINTNL()
				//INHIBIT_CRANE(CRANE_ACTION_CABIN)
				i_Left_X = 0
			ENDIF
		ENDIF
	ENDIF
	
	//Detech for moving fowards into collision
	IF i_Left_Y != 0
		FLOAT fLeftGroundZ1, fLeftGroundZ2, fRightGroundZ1, fRightGroundZ2
		GET_GROUND_Z_FOR_3D_COORD(vLeftOffset1 + <<0.0, 0.0, 50.0>>, fLeftGroundZ1)
		GET_GROUND_Z_FOR_3D_COORD(vLeftOffset2 + <<0.0, 0.0, 50.0>>, fLeftGroundZ2)
		GET_GROUND_Z_FOR_3D_COORD(vRightOffset1 + <<0.0, 0.0, 50.0>>, fRightGroundZ1)
		GET_GROUND_Z_FOR_3D_COORD(vRightOffset2 + <<0.0, 0.0, 50.0>>, fRightGroundZ2)
		IF fLeftGroundZ1 > vLeftOffset1.z OR fLeftGroundZ2 > vLeftOffset2.z
			IF i_Left_Y < 0
				PRINTSTRING("INHIBIT SPREADER FORWARD")PRINTNL()
			//	INHIBIT_CRANE(CRANE_ACTION_CRANE)
				i_Left_Y = 0
			ENDIF
		ELIF fRightGroundZ1 > vRightOffset1.z OR fRightGroundZ2 > vRightOffset2.z
			IF i_Left_Y > 0
				PRINTSTRING("INHIBIT SPREADER FORWARD")PRINTNL()
				//INHIBIT_CRANE(CRANE_ACTION_CRANE)
				i_Left_Y = 0
			ENDIF
		ENDIF
	ENDIF
	
	//Detect if the spreader is touching one of the other containers, disable movement in this case too.
	REPEAT COUNT_OF(s_Containers) i 
		IF DOES_ENTITY_EXIST(s_Containers[i].obj_main)
			IF i != iCurrentlyAttachedContainer
				IF i_Left_X != 0
					IF IS_POINT_IN_ANGLED_AREA(vLeftOffset1, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_Containers[i].obj_main, <<0.0, -6.0, 0.0>>), 
															 GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_Containers[i].obj_main, <<0.0, 6.0, 3.0>>), 3.0)
					OR IS_POINT_IN_ANGLED_AREA(vLeftOffset2, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_Containers[i].obj_main, <<0.0, -6.0, 0.0>>), 
															 GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_Containers[i].obj_main, <<0.0, 6.0, 3.0>>), 3.0)
						IF i_Left_X < 0
							PRINTSTRING("INHIBIT SPREADER DOWN")PRINTNL()
							//INHIBIT_CRANE(CRANE_ACTION_CABIN)
							i_Left_X = 0
						ENDIF									 
					ENDIF
					
					IF IS_POINT_IN_ANGLED_AREA(vRightOffset1, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_Containers[i].obj_main, <<0.0, -6.0, 0.0>>), 
															 GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_Containers[i].obj_main, <<0.0, 6.0, 3.0>>), 3.0)
					OR IS_POINT_IN_ANGLED_AREA(vRightOffset2, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_Containers[i].obj_main, <<0.0, -6.0, 0.0>>), 
															 GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_Containers[i].obj_main, <<0.0, 6.0, 3.0>>), 3.0)
						IF i_Left_X > 0
							PRINTSTRING("INHIBIT SPREADER DOWN")PRINTNL()
							//INHIBIT_CRANE(CRANE_ACTION_CABIN)
							i_Left_X = 0
						ENDIF									 
					ENDIF
				ENDIF
				
				IF i_Right_Y > 0
					IF IS_POINT_IN_ANGLED_AREA(vBottomOffset1, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_Containers[i].obj_main, <<0.0, -6.0, 0.0>>), 
															 GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_Containers[i].obj_main, <<0.0, 6.0, 2.7>>), 3.0)
					OR IS_POINT_IN_ANGLED_AREA(vBottomOffset2, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_Containers[i].obj_main, <<0.0, -6.0, 0.0>>), 
															 GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_Containers[i].obj_main, <<0.0, 6.0, 2.7>>), 3.0)
						i_Right_Y = 0
						PRINTSTRING("INHIBIT SPREADER DOWN")PRINTNL()
						//INHIBIT_CRANE(CRANE_ACTION_SPREADER)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF NOT b_is_audio_scene_active
		START_AUDIO_SCENE("CAR_THEFT_EXPORT_CARS_CRANE_SECTION_SCENE")
		b_is_audio_scene_active = TRUE
	ENDIF
	
	//TODO 100140: should not be able to move diagonally on the left stick, cancel one axis out if the other is greater.
	IF ABSI(i_left_x) > ABSI(i_left_y)
		i_left_y = 0
	ELSE
		i_left_x = 0
	ENDIF
	
	//CABIN
	FLOAT f_cabin_speed_modifier = 1.0
	IF ABSF(s_crane.v_cabin_attach_offset.y - MAX_CABIN_Y) < 4.0 AND s_crane.f_cabin_vel > 0.0
		f_cabin_speed_modifier = ABSF(s_crane.v_cabin_attach_offset.y - MAX_CABIN_y) / 4.0
	ELIF ABSF(s_crane.v_cabin_attach_offset.y - MIN_CABIN_Y) < 4.0 AND s_crane.f_cabin_vel < 0.0
		f_cabin_speed_modifier = ABSF(s_crane.v_cabin_attach_offset.y - MIN_CABIN_y) / 4.0
	ENDIF
	
	FLOAT f_desired_cabin_vel
	
	//CHANGED CONTROLS
	IF b_is_crane_cinematic_active 
		f_desired_cabin_vel = ((TO_FLOAT(i_left_x) / 128.0) * MAX_CABIN_VEL) * f_cabin_speed_modifier
		CONVERGE_VALUE(s_crane.f_cabin_vel, f_desired_cabin_vel, CABIN_ACCEL, TRUE)
		s_crane.v_cabin_attach_offset.y = s_crane.v_cabin_attach_offset.y +@ s_crane.f_cabin_vel
	ELSE
		f_desired_cabin_vel = ((TO_FLOAT(i_left_y) / -128.0) * MAX_CABIN_VEL) * f_cabin_speed_modifier
		CONVERGE_VALUE(s_crane.f_cabin_vel, f_desired_cabin_vel, CABIN_ACCEL, TRUE)
		s_crane.v_cabin_attach_offset.y = s_crane.v_cabin_attach_offset.y +@ s_crane.f_cabin_vel
	ENDIF
	
//	IF b_is_crane_cinematic_active 
//		f_desired_cabin_vel = ((TO_FLOAT(i_left_x) / 128.0) * MAX_CABIN_VEL) * f_cabin_speed_modifier
//		CONVERGE_VALUE(s_crane.f_cabin_vel, f_desired_cabin_vel, CABIN_ACCEL, TRUE)
//		s_crane.v_cabin_attach_offset.y = s_crane.v_cabin_attach_offset.y +@ s_crane.f_cabin_vel
//	ELSE
//		f_desired_cabin_vel = ((TO_FLOAT(i_left_x) / 128.0) * MAX_CABIN_VEL) * f_cabin_speed_modifier
//		CONVERGE_VALUE(s_crane.f_cabin_vel, f_desired_cabin_vel, CABIN_ACCEL, TRUE)
//		s_crane.v_cabin_attach_offset.y = s_crane.v_cabin_attach_offset.y +@ s_crane.f_cabin_vel
//	ENDIF
	
	IF s_crane.v_cabin_attach_offset.y < MIN_CABIN_Y
		s_crane.v_cabin_attach_offset.y = MIN_CABIN_Y
		s_crane.f_cabin_vel = 0.0
	ELIF s_crane.v_cabin_attach_offset.y > MAX_CABIN_Y
		s_crane.v_cabin_attach_offset.y = MAX_CABIN_Y
		s_crane.f_cabin_vel = 0.0
	ENDIF 
		
////	
	
	//CABIN AUDIO
	IF ABSF(s_crane.f_cabin_vel) > AUDIO_TRIGGER_THRESHOLD
		IF HAS_SOUND_FINISHED(s_crane.i_cabin_sound_id)
			PLAY_SOUND_FROM_ENTITY(s_crane.i_cabin_sound_id, "Move_L_R", s_crane.obj_spreader, "CRANE_SOUNDS")
			////PRINTLN("CABIN AUDIO PLAYING")//PRINTNL()
		ENDIF
		SET_VARIABLE_ON_SOUND(s_crane.i_cabin_sound_id, "Speed", ABSF(s_crane.f_cabin_vel / MAX_CABIN_VEL))
	ELSE
		IF NOT HAS_SOUND_FINISHED(s_crane.i_cabin_sound_id)
			STOP_SOUND(s_crane.i_cabin_sound_id)
		ENDIF
	ENDIF
	
	//SPREADER
	FLOAT f_spreader_speed_modifier = 1.0
	IF ABSF(s_crane.v_spreader_attach_offset.z - MAX_SPREADER_Z) < 4.0 AND s_crane.f_spreader_vel > 0.0
		f_spreader_speed_modifier = ABSF(s_crane.v_spreader_attach_offset.z - MAX_SPREADER_Z) / 4.0
	ELIF ABSF(s_crane.v_spreader_attach_offset.z - MIN_SPREADER_Z) < 4.0 AND s_crane.f_spreader_vel < 0.0
		f_spreader_speed_modifier = ABSF(s_crane.v_spreader_attach_offset.z - MIN_SPREADER_Z) / 4.0
	ENDIF
	
	FLOAT f_desired_spreader_vel = ((TO_FLOAT(i_right_y) / 128.0) * -MAX_SPREADER_VEL) * f_spreader_speed_modifier
	
	IF s_crane.b_spreader_stuck_on_ground
		IF f_desired_spreader_vel < 0.0
			f_desired_spreader_vel = 0.0
		ENDIF
	ENDIF
	
	IF b_is_crane_cinematic_active
		f_desired_spreader_vel = ((TO_FLOAT(i_right_y) / 128.0) * MAX_SPREADER_VEL) * f_spreader_speed_modifier
		CONVERGE_VALUE(s_crane.f_spreader_vel, f_desired_spreader_vel, SPREADER_ACCEL, TRUE)
		s_crane.v_spreader_attach_offset.z = s_crane.v_spreader_attach_offset.z +@ s_crane.f_spreader_vel
	ELSE
		f_desired_spreader_vel = ((TO_FLOAT(i_right_y) / 128.0) * -MAX_SPREADER_VEL) * f_spreader_speed_modifier
		CONVERGE_VALUE(s_crane.f_spreader_vel, f_desired_spreader_vel, SPREADER_ACCEL, TRUE)
		s_crane.v_spreader_attach_offset.z = s_crane.v_spreader_attach_offset.z +@ s_crane.f_spreader_vel
	ENDIF
	
	IF s_crane.v_spreader_attach_offset.z < MIN_SPREADER_Z
		s_crane.v_spreader_attach_offset.z = MIN_SPREADER_Z
		s_crane.f_spreader_vel = 0.0
	ELIF s_crane.v_spreader_attach_offset.z > MAX_SPREADER_Z
		s_crane.v_spreader_attach_offset.z = MAX_SPREADER_Z
		s_crane.f_spreader_vel = 0.0
	ENDIF
	
	//DETACH_ENTITY(s_crane.obj_spreader)
	//ATTACH_ENTITY_TO_ENTITY(s_crane.obj_spreader, s_crane.obj_cabin, 0, s_crane.v_spreader_attach_offset, <<0.0, 0.0, 0.0>>)
	v_helper_offset = <<s_crane.v_spreader_attach_offset.x, s_crane.v_spreader_attach_offset.y, s_crane.v_spreader_attach_offset.z - SPREADER_START_OFFSET>>
	IF DOES_ENTITY_EXIST(s_crane.obj_cabin)
		SET_ENTITY_COORDS_NO_OFFSET(s_crane.obj_helper, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, v_helper_offset))	
	ENDIF
	
	
	IF NOT MANAGE_MY_TIMER(iSoundAttachementTime,5000)
		IF NOT HAS_LABEL_BEEN_TRIGGERED("CLAMP SOUND")
			PRINTSTRING("PLAYINGSOUND")PRINTNL()
			PLAY_SOUND_FROM_ENTITY(iCraneSound, "Clamp", s_crane.obj_spreader, "CRANE_SOUNDS")
			SET_LABEL_AS_TRIGGERED("CLAMP SOUND",TRUE)
		ENDIF
	ELSE
		IF HAS_LABEL_BEEN_TRIGGERED("CLAMP SOUND")
			PRINTSTRING("STOPPEDSOUND")PRINTNL()
			STOP_SOUND(iCraneSound)
			SET_LABEL_AS_TRIGGERED("CLAMP SOUND",FALSE)
		ENDIF
	ENDIF
	
	VECTOR vSpreaderPos
	
	IF DOES_ENTITY_EXIST(s_Crane.obj_spreader)
		vSpreaderPos = GET_ENTITY_COORDS(s_Crane.obj_spreader)
	ENDIF
	//SPEADER AUDIO
	IF i_Right_Y != 0
	AND NOT s_crane.b_spreader_stuck_on_ground
		PRINTSTRING("vSpreaderPos")PRINTVECTOR(vSpreaderPos)PRINTNL()
		IF vSpreaderPos. z > 17.52
			IF HAS_SOUND_FINISHED(s_Crane.i_spreader_sound_id)
				PLAY_SOUND_FROM_ENTITY(s_Crane.i_spreader_sound_id, "Move_Fail", s_Crane.obj_spreader, "CRANE_SOUNDS")
				//PRINTLN("SPREADER AUDIO PLAYING")//PRINTNL()
			ENDIF
			SET_VARIABLE_ON_SOUND(s_Crane.i_spreader_sound_id, "Speed", MAX_SPREADER_VEL / 2.0)
		ELSE
			IF HAS_SOUND_FINISHED(s_Crane.i_spreader_sound_id)
				PLAY_SOUND_FROM_ENTITY(s_Crane.i_spreader_sound_id, "Move_U_D", s_Crane.obj_spreader, "CRANE_SOUNDS")
				//PRINTLN("SPREADER AUDIO PLAYING")//PRINTNL()
			ENDIF
			SET_VARIABLE_ON_SOUND(s_Crane.i_spreader_sound_id, "Speed", MAX_SPREADER_VEL / 2.0)
		ENDIF
	ELSE
		IF NOT bFakeSound
			IF NOT HAS_SOUND_FINISHED(s_Crane.i_spreader_sound_id)
				STOP_SOUND(s_Crane.i_spreader_sound_id)
			ENDIF
		ENDIF
	ENDIF
	
	//MAIN CRANE
	//CHANGE CRANE CONTROLS
	//FLOAT f_desired_crane_vel = (TO_FLOAT(i_left_y) / 128.0) * -MAX_CRANE_VEL
	IF b_is_crane_cinematic_active 
		FLOAT f_desired_crane_vel = (TO_FLOAT(i_left_y) / 128.0) * -MAX_CRANE_VEL
		CONVERGE_VALUE(s_crane.f_crane_vel, f_desired_crane_vel, CRANE_ACCEL, TRUE)
		s_crane.f_crane_offset = s_crane.f_crane_offset +@ s_crane.f_crane_vel 
	ELSE
		FLOAT f_desired_crane_vel = (TO_FLOAT(i_left_x) / 128.0) * -MAX_CRANE_VEL
		CONVERGE_VALUE(s_crane.f_crane_vel, f_desired_crane_vel, CRANE_ACCEL, TRUE)
		s_crane.f_crane_offset = s_crane.f_crane_offset +@ s_crane.f_crane_vel 
	ENDIF
	
	IF s_crane.f_crane_offset < MIN_CRANE_OFFSET
		s_crane.f_crane_offset = MIN_CRANE_OFFSET
		s_crane.f_crane_vel = 0.0
	ELIF s_crane.f_crane_offset > MAX_CRANE_OFFSET
		s_crane.f_crane_offset = MAX_CRANE_OFFSET
		s_crane.f_crane_vel = 0.0
	ENDIF
	
	
//	DETACH_ENTITY(s_crane.obj_cabin)
//	ATTACH_ENTITY_TO_ENTITY(s_crane.obj_cabin, s_crane.obj_main, -1, s_crane.v_cabin_attach_offset, <<0.0, 0.0, 0.0>>)	
	
//	SET_ENTITY_COORDS_NO_OFFSET(s_crane.obj_main, s_crane.v_start_pos + <<s_crane.f_crane_offset, 0.0, 0.0>>)	
	
	//PRINTSTRING("Crane Coords:")PRINTVECTOR(GET_ENTITY_COORDS(s_crane.obj_main))//PRINTNL()
	
	//Spreader speed correction and damping
	FLOAT spreader_damping = 0.0
	IF ABSI(i_left_y) > 0 OR ABSI(i_left_x) > 0
		s_crane.i_dampingframes = -1
		VECTOR spreaderVelocity = GET_ENTITY_VELOCITY(s_crane.obj_spreader)
		VECTOR desiredSpreaderVelocity = <<s_crane.f_crane_vel, -s_crane.f_cabin_vel, spreaderVelocity.z>>
		FLOAT correctionSpeed = 0.3
		APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(s_crane.obj_spreader, APPLY_TYPE_IMPULSE, (desiredSpreaderVelocity-spreaderVelocity)*correctionSpeed, 0, FALSE, TRUE)	
	ELSE
		IF s_crane.i_dampingframes = -1
			s_crane.i_dampingframes = i_spreader_dampingframes
		ENDIF
		IF s_crane.i_dampingframes > 0
			spreader_damping = f_spreader_damping
			s_crane.i_dampingframes = s_crane.i_dampingframes - 1
		ENDIF
	ENDIF
	SET_DAMPING( s_crane.obj_spreader, PHYSICS_DAMPING_LINEAR_V, spreader_damping)
	
	//BASE AUDIO
	
	//Audio
	IF ABSF(s_crane.f_crane_vel) > AUDIO_TRIGGER_THRESHOLD
		IF HAS_SOUND_FINISHED(s_crane.i_crane_sound_id)
			PLAY_SOUND_FROM_ENTITY(s_crane.i_crane_sound_id, "Move_Base", s_crane.obj_spreader, "CRANE_SOUNDS")
		ENDIF
		SET_VARIABLE_ON_SOUND(s_crane.i_crane_sound_id, "Speed", ABSF(s_crane.f_crane_vel / MAX_CRANE_VEL))
	ELSE
		IF NOT HAS_SOUND_FINISHED(s_crane.i_crane_sound_id)
			STOP_SOUND(s_crane.i_crane_sound_id)
		ENDIF
	ENDIF
	
	//Fudge for spreader getting stuck: try to knock it into activity
	VECTOR v_vel = GET_ENTITY_VELOCITY(s_crane.obj_spreader)
	IF v_vel.x = 0.0 AND v_vel.y = 0.0 AND v_vel.z  = 0.0
		IF i_Right_Y < -10
			SET_ENTITY_VELOCITY(s_crane.obj_spreader, <<0.0, 0.0, 4.0>>)
		ENDIF
	ENDIF
	
	//Manage containers
	REPEAT COUNT_OF(s_containers) i 
		IF DOES_ENTITY_EXIST(s_containers[i].obj_main)
			s_containers[i].f_prev_speed = s_containers[i].f_speed
			s_containers[i].f_speed = ABSF(GET_SCALAR_VELOCITY(GET_ENTITY_VELOCITY(s_containers[i].obj_main), FALSE))
			VECTOR v_current_pos = GET_ENTITY_COORDS(s_containers[i].obj_main)
			
			//Freeze if it's not moving (to stop it from being knocked too easily)
			IF NOT IS_ENTITY_ATTACHED(s_containers[i].obj_main)
				IF s_containers[i].f_speed < 0.02
					IF v_current_pos.z > 1.0
						FREEZE_ENTITY_POSITION(s_containers[i].obj_main, TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			//Attachments
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_PAD_RIGHT)
				IF DOES_ENTITY_EXIST(s_containers[0].obj_main)
					IF NOT CAN_CRANE_PICK_UP_CONTAINER(GET_ENTITY_COORDS(s_containers[0].obj_main), GET_ENTITY_COORDS(s_crane.obj_spreader))
						IF DOES_ENTITY_EXIST(s_containers[1].obj_main)	
							IF NOT CAN_CRANE_PICK_UP_CONTAINER(GET_ENTITY_COORDS(s_containers[1].obj_main), GET_ENTITY_COORDS(s_crane.obj_spreader))
								//Fail sound
								IF HAS_SOUND_FINISHED(s_crane.i_spreader_fail_sound_id)
									PLAY_SOUND_FROM_ENTITY(s_crane.i_spreader_fail_sound_id, "Attach_Container_Fail", s_crane.obj_spreader, "CRANE_SOUNDS")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
					CHECK_CONTAINER_ATTACHING(s_containers[i].obj_main)
			ENDIF
			
			//Container collision sounds - messy
			FLOAT f_speed_diff = ABSF(s_containers[i].f_speed - s_containers[i].f_prev_speed)
			
			IF f_speed_diff > 6.0
				INT i_impact_sound_id = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(i_impact_sound_id, "Container_Impact", s_containers[i].obj_main, "CRANE_SOUNDS")
				SET_VARIABLE_ON_SOUND(i_impact_sound_id, "Speed", f_speed_diff / 100.0)
				RELEASE_SOUND_ID(i_impact_sound_id)
			ENDIF
			
				//Specific collision sound after dropping it
			IF NOT IS_ENTITY_ATTACHED(s_containers[i].obj_main)
				IF NOT s_containers[i].bCollisionSoundPrimed
					IF GET_GAME_TIMER() - s_containers[i].iTimeSinceDropped < 3000
					AND GET_GAME_TIMER() - s_containers[i].iTimeSinceLastCollisionSound > 4000
						IF s_containers[i].f_speed > 5.0
							s_containers[i].bCollisionSoundPrimed = TRUE
						ENDIF
					ENDIF
				ELSE
					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(s_containers[i].obj_main)
						PLAY_SOUND_FROM_ENTITY(-1, "Container_Impact_Land", s_containers[i].obj_main, "CRANE_SOUNDS")
						s_containers[i].bCollisionSoundPrimed = FALSE
						s_containers[i].iTimeSinceLastCollisionSound = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ELSE
				s_containers[i].bCollisionSoundPrimed = FALSE
				s_containers[i].iTimeSinceLastCollisionSound = 0
			ENDIF
			
			
		ENDIF
	ENDREPEAT
	
	
	INT j = 0
	IF ABSF(s_crane.f_spreader_vel) > 0.01
		REPEAT 8 j
			ROPE_RESET_LENGTH(s_crane.ropes[j], FALSE )
		ENDREPEAT
	ELSE
		REPEAT 8 j
			ROPE_RESET_LENGTH(s_crane.ropes[j], TRUE )
		ENDREPEAT
		
		FLOAT currentMaxLength = 0.0
		FLOAT currentMinLength = 1000.0
		FLOAT currentLength

		REPEAT 8 j
			currentLength = ROPE_GET_DISTANCE_BETWEEN_ENDS(s_crane.ropes[j])		
			IF currentLength > currentMaxLength
				currentMaxLength = currentLength
			ENDIF
			
			IF currentLength < currentMinLength
				currentMinLength = currentLength
			ENDIF
		ENDREPEAT
		
		IF (currentMaxLength - currentMinLength) > 0.02
			REPEAT 8 j
				ROPE_FORCE_LENGTH(s_crane.ropes[j], currentMaxLength)
			ENDREPEAT
		ENDIF					
	ENDIF	
	
	
	//Camera: interp based on both the position of the cabin and the position of the spreader
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_SELECT)
		b_is_crane_cinematic_active = NOT b_is_crane_cinematic_active
	ENDIF
	
	IF NOT DOES_CAM_EXIST(cam_cutscene)
		cam_cutscene = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
		SHAKE_CAM(cam_cutscene, "HAND_SHAKE", 0.1)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
	ENDIF		
	

	SET_ENTITY_COORDS_NO_OFFSET(s_crane.obj_main, s_crane.v_start_pos + <<s_crane.f_crane_offset, 0.0, 0.0>>)	
	DETACH_ENTITY(s_crane.obj_cabin)
	ATTACH_ENTITY_TO_ENTITY(s_crane.obj_cabin, s_crane.obj_main, -1, s_crane.v_cabin_attach_offset, <<0.0, 0.0, 0.0>>)
	
	//Rotate wheels
	REPEAT COUNT_OF(s_crane.obj_wheels) i
		DETACH_ENTITY(s_crane.obj_wheels[i])
		ATTACH_ENTITY_TO_ENTITY(s_crane.obj_wheels[i], s_crane.obj_main, -1, s_crane.v_wheel_offsets[i], <<0.0, s_crane.f_crane_offset*70*-1, 0.0>>)
	ENDREPEAT
	
	IF NOT b_is_crane_cinematic_active
		VECTOR v_spreader_offset_from_cabin = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(s_crane.obj_cabin, v_spreader_pos)
		VECTOR v_spreader_offset_from_main = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(s_crane.obj_main, v_spreader_pos)
		IF v_spreader_offset_from_cabin.z < MIN_SPREADER_Z
			v_spreader_offset_from_cabin.z = MIN_SPREADER_Z
		ELIF v_spreader_offset_from_cabin.z > MAX_SPREADER_Z
			v_spreader_offset_from_cabin.z  = MAX_SPREADER_Z
		ENDIF
		
		IF v_spreader_offset_from_main.x < MIN_CABIN_Y
			v_spreader_offset_from_main.x = MIN_CABIN_Y
		ELIF v_spreader_offset_from_main.x > MAX_CABIN_Y
			v_spreader_offset_from_main.x  = MAX_CABIN_Y
		ENDIF
	
		FLOAT f_cabin_progress = (v_spreader_offset_from_main.x - MIN_CABIN_Y) / (MAX_CABIN_Y - MIN_CABIN_Y)
		FLOAT f_spreader_progress = 1.0 - ((v_spreader_offset_from_cabin.z - MIN_SPREADER_Z) / (MAX_SPREADER_Z - MIN_SPREADER_Z))
		
		IF b_force_cam_into_position
			s_crane.f_cam_z_progress = f_spreader_progress
			s_crane.f_cam_x_progress = f_cabin_progress
		ELSE
			s_crane.f_cam_z_progress = s_crane.f_cam_z_progress + ((f_spreader_progress - s_crane.f_cam_z_progress) * 0.2)
			s_crane.f_cam_x_progress = s_crane.f_cam_x_progress + ((f_cabin_progress - s_crane.f_cam_x_progress) * 0.1)
		ENDIF
		
		VECTOR v_current_attach = v_cabin_attach_start
		v_current_attach.x += (v_cabin_attach_end.x - v_cabin_attach_start.x) * s_crane.f_cam_x_progress
		v_current_attach.y += (v_cabin_attach_end.y - v_cabin_attach_start.y) * s_crane.f_cam_x_progress
		v_current_attach.z += (v_cabin_attach_end.z - v_cabin_attach_start.z) * s_crane.f_cam_x_progress 
		
		v_current_attach.x += (v_spreader_attach_end.x - v_spreader_attach_start.x) * s_crane.f_cam_z_progress
		v_current_attach.y += (v_spreader_attach_end.y - v_spreader_attach_start.y) * s_crane.f_cam_z_progress
		v_current_attach.z += (v_spreader_attach_end.z - v_spreader_attach_start.z) * s_crane.f_cam_z_progress 
		
		VECTOR v_current_point = v_cabin_point_start
		v_current_point.x += (v_cabin_point_end.x - v_cabin_point_start.x) * s_crane.f_cam_x_progress
		v_current_point.y += (v_cabin_point_end.y - v_cabin_point_start.y) * s_crane.f_cam_x_progress
		v_current_point.z += (v_cabin_point_end.z - v_cabin_point_start.z) * s_crane.f_cam_x_progress
		
		v_current_point.x += (v_spreader_point_end.x - v_spreader_point_start.x) * s_crane.f_cam_z_progress
		v_current_point.y += (v_spreader_point_end.y - v_spreader_point_start.y) * s_crane.f_cam_z_progress
		v_current_point.z += (v_spreader_point_end.z - v_spreader_point_start.z) * s_crane.f_cam_z_progress
		
		ATTACH_CAM_TO_ENTITY(cam_cutscene, s_crane.obj_cabin, v_current_attach)
		POINT_CAM_AT_ENTITY(cam_cutscene, s_crane.obj_cabin, v_current_point)
		SET_CAM_FOV(cam_cutscene, f_crane_cam_fov)

		SHAKE_CAM(cam_cutscene, "HAND_SHAKE", 0.1)
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(-70.0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(-70.0)
	ELSE

		VECTOR spreaderPos		
		spreaderPos = GET_ENTITY_COORDS( s_crane.obj_spreader )
		VECTOR cabinPos
		cabinPos =  GET_ENTITY_COORDS( s_crane.obj_cabin )
		VECTOR cameraPos
		
		cameraPos.x = (cabinPos.x + spreaderPos.x) * 0.5 - spreaderPos.x // - 1.512
		cameraPos.y = (cabinPos.y + spreaderPos.y) * 0.5 - spreaderPos.y
		cameraPos.z = v_attach_to_cam_value.z - 1.55
		
		ATTACH_CAM_TO_ENTITY(cam_cutscene, s_crane.obj_cabin, cameraPos)
//		ATTACH_CAM_TO_ENTITY(cam_cutscene, s_crane.obj_cabin, v_attach_to_cam_value)
		STOP_CAM_POINTING(cam_cutscene)
		SET_CAM_NEAR_CLIP(cam_cutscene,0.1)
		SET_CAM_ROT(cam_cutscene, v_crane_attach_cam_rot)
		SET_CAM_FOV(cam_cutscene, f_crane_attach_cam_fov)
	ENDIF
	
	//Light: indicates where on the ground the spreader will land
	//VECTOR v_spreader_pos = GET_ENTITY_COORDS(s_crane.obj_spreader)
	VECTOR v_ground_pos = v_spreader_pos
	GET_GROUND_Z_FOR_3D_COORD(v_spreader_pos, v_ground_pos.z)
	v_ground_pos.z += 0.5
	
	IF NOT s_crane.b_container_attached
		REPEAT COUNT_OF(s_containers) i 
			IF DOES_ENTITY_EXIST(s_containers[i].obj_main)
				VECTOR v_offset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(s_containers[i].obj_main, v_ground_pos)
				
				IF ABSF(v_offset.x) < 1.3
					IF ABSF(v_offset.y) < 6.0
						IF v_offset.z < 2.8
							VECTOR v_container_pos = GET_ENTITY_COORDS(s_containers[i].obj_main)
							v_ground_pos.z = v_container_pos.z + 3.0
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF

	IF NOT s_crane.b_container_attached
		IF DOES_ENTITY_EXIST(s_Crane.obj_Spreader)
	
			DRAW_SPOT_LIGHT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_Crane.obj_Spreader,<<1.2658, 6.0654, -0.6602>>),<<0,0,-1>>,0, 255, 0, fFallOff,fIntensity,fInnerAngle,fOuterAngle,fExposure)
			
			DRAW_SPOT_LIGHT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_Crane.obj_Spreader,<<-1.2819, 6.0349, -0.7461>>),<<0,0,-1>>,0, 255, 0, fFallOff,fIntensity,fInnerAngle,fOuterAngle,fExposure)
			
			DRAW_SPOT_LIGHT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_Crane.obj_Spreader,<<-1.2073, -5.9944, -0.6715>>),<<0,0,-1>>,0, 255, 0, fFallOff,fIntensity,fInnerAngle,fOuterAngle,fExposure)
			
			DRAW_SPOT_LIGHT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_Crane.obj_Spreader,<<1.1668, -5.9398, -0.5084>>),<<0,0,-1>>,0, 255, 0, fFallOff,fIntensity,fInnerAngle,fOuterAngle,fExposure)
			//DRAW_LIGHT_WITH_RANGE(v_ground_pos, 0, 255, 0, 4.0, 50.0)
		ENDIF
	ENDIF

	//Strain sound effect: plays on the currently attached container.
	IF s_Crane.b_container_attached
		IF HAS_SOUND_FINISHED(s_Crane.i_strain_sound_Id)
			REPEAT COUNT_OF(s_Containers) i 
				IF IS_ENTITY_ATTACHED_TO_ENTITY(s_Containers[i].obj_Main, s_Crane.obj_Spreader)
					PLAY_SOUND_FROM_ENTITY(s_Crane.i_Strain_Sound_Id, "Strain", s_Containers[i].obj_Main, "CRANE_SOUNDS")
				ENDIF
			ENDREPEAT
		ENDIF
	ELSE
		IF NOT HAS_SOUND_FINISHED(s_Crane.i_Strain_Sound_Id)
			STOP_SOUND(s_Crane.i_Strain_Sound_Id)
		ENDIF
	ENDIF
	
ENDPROC

PROC REMOVE_ALL_OBJECTS(BOOL b_force_delete = FALSE)
	REMOVE_OBJECT(s_crane.obj_spreader, TRUE)
	REMOVE_OBJECT(s_crane.obj_cabin, TRUE)
	REMOVE_OBJECT(s_crane.obj_lift, TRUE)
	REMOVE_OBJECT(s_crane.obj_main, TRUE)
	REMOVE_OBJECT(s_crane.obj_helper, TRUE)
	REMOVE_OBJECT(s_crane.obj_boom_cable, TRUE)
	
	INT i = 0
	REPEAT COUNT_OF(s_containers) i
		REMOVE_OBJECT(s_containers[i].obj_left_door, b_force_delete)
		REMOVE_OBJECT(s_containers[i].obj_right_door, b_force_delete)
		REMOVE_OBJECT(s_containers[i].obj_main, b_force_delete)
	ENDREPEAT
	
	REPEAT COUNT_OF(s_crane.obj_wheels) i
		REMOVE_OBJECT(s_crane.obj_wheels[i], TRUE)
	ENDREPEAT
	
ENDPROC

FUNC BOOL INITIALISE_CRANE(BOOL bPlacePlayerInside = TRUE)
	//CRANE
		
		IF DOES_ENTITY_EXIST(s_crane.obj_main)
			INT i = 0	
			
			WAIT(0)
					
			IF bPlacePlayerInside
				REQUEST_ANIM_DICT("missheistdockssetup1trevor_crane")
				IF HAS_ANIM_DICT_LOADED("missheistdockssetup1trevor_crane")
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(ssCraneAnims)
						ssCraneAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.100, -0.150, -0.350 >>,<< 0.000, 0.000, 0.000 >>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssCraneAnims,s_crane.obj_cabin,-1)
						TASK_SYNCHRONIZED_SCENE (PLAYER_PED_ID(), ssCraneAnims, "missheistdockssetup1trevor_crane", "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ssCraneAnims,FALSE)
						SET_SYNCHRONIZED_SCENE_LOOPED(ssCraneAnims,TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			//Close container doors
			IF DOES_ENTITY_EXIST( s_containers[0].obj_main)
				IF DOES_ENTITY_EXIST(s_containers[0].obj_left_door)
					ATTACH_ENTITY_TO_ENTITY(s_containers[0].obj_left_door, s_containers[0].obj_main, -1, v_left_door_offset, <<0.0, 0.0, 0.0>>)
				ENDIF
				
				IF DOES_ENTITY_EXIST(s_containers[0].obj_right_door)
					ATTACH_ENTITY_TO_ENTITY(s_containers[0].obj_right_door, s_containers[0].obj_main, -1, v_right_door_offset, <<0.0, 0.0, 0.0>>)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST( s_containers[1].obj_main)
				IF DOES_ENTITY_EXIST(s_containers[1].obj_left_door)
					ATTACH_ENTITY_TO_ENTITY(s_containers[1].obj_left_door, s_containers[1].obj_main, -1, v_left_door_offset, <<0.0, 0.0, 0.0>>)
				ENDIF
				IF DOES_ENTITY_EXIST(s_containers[1].obj_right_door)
					ATTACH_ENTITY_TO_ENTITY(s_containers[1].obj_right_door, s_containers[1].obj_main, -1, v_right_door_offset, <<0.0, 0.0, 0.0>>)
				ENDIF
			ENDIF
		
			//Attach crane parts to the main crane (collision is no longer needed on them)
			//Added detach for freeze fix
			DETACH_ENTITY(s_crane.obj_cabin)
			FREEZE_ENTITY_POSITION(s_crane.obj_cabin, FALSE)
			ATTACH_ENTITY_TO_ENTITY(s_crane.obj_cabin, s_crane.obj_main, -1, s_crane.v_cabin_attach_offset, <<0.0, 0.0, 0.0>>)
			
			REPEAT COUNT_OF(s_crane.obj_wheels) i
				//FREEZE_ENTITY_POSITION(s_crane.obj_wheels[i], FALSE)
				ATTACH_ENTITY_TO_ENTITY(s_crane.obj_wheels[i], s_crane.obj_main, -1, s_crane.v_wheel_offsets[i], <<0.0, 0.0, 0.0>>)
			ENDREPEAT
			
			RETURN TRUE
		ELSE
			//SCRIPT_ASSERT("THE CRANE ISN'T LOADED IN...somehow.")
		ENDIF
	
	RETURN FALSE
ENDFUNC

// ======================================================================
//                        Waypoint Actions and Events
// ======================================================================

PROC MANAGE_PLAYER_OUTFIT()

	PRINTSTRING("SETTING COMPONENTS")PRINTNL()
	
	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_DOCK_WORKER, FALSE)
	SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR)
	
	IF DOES_ENTITY_EXIST(pedFloyd)
		IF NOT IS_PED_INJURED(pedFloyd)	
			
			SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_HEAD, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_BERD, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_HAIR, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_TORSO, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_LEG, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_HAND, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_FEET, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_TEETH, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_SPECIAL, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_DECL, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_JBIB, 0, 0)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedWade)
		IF NOT IS_PED_INJURED(pedWade)
			SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_HEAD, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_BERD, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_HAIR, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_TORSO, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_LEG, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_HAND, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_FEET, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_TEETH, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_SPECIAL, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_SPECIAL2, 0, 0)
			SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_DECL, 1, 0)
			SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_JBIB, 0, 0)
		ENDIF
	ENDIF
	
	
	
ENDPROC



//Triggers and sets up waypoint actions for following floyd
PROC MANAGE_WAYPOINT_ACTIONS()
	
	IF NOT IS_PED_INJURED(pedFloyd)
		
		//PAUSE WHEN HE SEES THE SHIP
		#IF CONST_IGNORE_THIS
			IF IS_ENTITY_AT_COORD(pedFloyd,<< -116.8039, -2454.5374, 5.0187 >> ,<<2.0,2.0,2.0>>) AND bWaypointTask[2] = FALSE
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedFloyd)
					WAYPOINT_PLAYBACK_PAUSE(pedFloyd,FALSE)
				ENDIF
				bWaypointTask[2] = TRUE
				bDoCatchUp = FALSE
			ENDIF
		
		
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),GET_ENTITY_COORDS(pedFloyd),<<2.0,2.0,2.0>>) AND bWaypointTask[2] = TRUE
				IF bNearShip = FALSE
					SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_GO_STRAIGHT_TO_COORD(NULL,<< -117.9427, -2455.8772, 5.0191 >>,PEDMOVE_WALK)
							TASK_ACHIEVE_HEADING(NULL,47.1444)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqMain)
				
					
						IF NOT DOES_CAM_EXIST(camMain)
							camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
						ENDIF
					SHAKE_CAM(camMain, "HAND_SHAKE", 0.01)
					SET_CAM_PARAMS(camMain,
									<< -116.3125, -2456.3374, 6.6209 >>, << -1.7519, -0.1310, 51.9340 >>,
									41.1853,
									0,
									GRAPH_TYPE_LINEAR,
									GRAPH_TYPE_LINEAR)
									
					SET_CAM_PARAMS(camMain,
									<< -116.3125, -2456.3374, 6.6209 >>, << 3.5014, -0.1310, 51.9340 >>,
									24.4644,
									6000,
									GRAPH_TYPE_LINEAR,
									GRAPH_TYPE_LINEAR)
					
					RENDER_SCRIPT_CAMS(TRUE, TRUE,3000)
					SETTIMERA(0)
					//bPrinted = FALSE
					bNearShip = TRUE
				ENDIF
			ENDIF
				
				IF bNearShip = TRUE AND bWaypointTask[3] = FALSE AND TIMERA() > 6000 
					CONTROL_COORD_CHASE_HINT_CAM_ON_FOOT(<< -217.4790, -2375.1294, 25.4562 >> )
					RENDER_SCRIPT_CAMS(FALSE, TRUE,3000)
					bDoCatchUp = TRUE
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedFloyd)
						WAYPOINT_PLAYBACK_RESUME(pedFloyd,FALSE)
						bWaypointTask[3] = TRUE
					ENDIF
				ENDIF
		#ENDIF
		
		//STOP FOR TRUCK
		IF IS_ENTITY_AT_COORD(pedFloyd,<< -73.1476, -2514.1521, 5.0094 >>,<<2,2,2>>) AND bWaypointTask[0] = FALSE
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedFloyd)
				//IF bTruckHasGone
				//WAYPOINT_PLAYBACK_PAUSE(pedFloyd,FALSE)
				bWaypointTask[0] = TRUE
				//bDoCatchUp = FALSE
				//ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehDocks[0])
			IF IS_ENTITY_AT_COORD(vehDocks[0],<< -72.5587, -2502.6030, 5.0074 >>,<<8,8,8>>)
				//bTruckHasGone = TRUE
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehDocks[0]) 
			IF DOES_ENTITY_EXIST(vehDocks[0])
				IF IS_ENTITY_AT_COORD(vehDocks[0],<< -72.5587, -2502.6030, 5.0074 >>,<<4,4,4>>) //AND bWaypointTask[1] = FALSE
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedFloyd)
						WAYPOINT_PLAYBACK_RESUME(pedFloyd,FALSE)
						bDoCatchUp = TRUE
						bWaypointTask[1] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//CRANE MOVING
		IF bWaypointTask[4] = FALSE
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< -72.5587, -2502.6030, 5.0074 >>,<<3,3,3>>)
				//bPrinted = FALSE
				//bPrinted = FALSE
				bWaypointTask[4] = TRUE
			ENDIF
		ENDIF
		
		//PRINTSTRING("bDOCatchUp:")PRINTBOOL(bDoCatchUp)//PRINTNL()
		//PRINTSTRING("GET_DISTANCE_BETWEEN_PEDS:")PRINTFLOAT(GET_DISTANCE_BETWEEN_PEDS(pedFloyd,PLAYER_PED_ID()))//PRINTNL()
		//PRINTSTRING("Doing Catch Up:")PRINTBOOL(bDoCatchUp)//PRINTNL()
		
		INT iWayPoint
		INT iRandBeckon
		
		REQUEST_ANIM_DICT("missheistdockssetup1ig_8")
		REQUEST_ANIM_DICT("MissHeistDocksSetup1")
		
		//CATCH UP
		IF bDoCatchUp = TRUE
			IF GET_DISTANCE_BETWEEN_PEDS(pedFloyd,PLAYER_PED_ID()) > 4
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedFloyd)
					//PRINTSTRING("WAYPOINT ACTION - DISTANCE LESS THAN 1")//PRINTNL()
					WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT("floyddocks1",GET_ENTITY_COORDS(pedFloyd),iWayPoint)
					IF bSetYourTimer = FALSE
						SETTIMERA(0)
						iRandBeckon = GET_RANDOM_INT_IN_RANGE(0,5)
						bSetYourTimer = TRUE
					ENDIF
					IF TIMERA() > 1000
						IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_8")
						AND  HAS_ANIM_DICT_LOADED("MissHeistDocksSetup1")
							IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedFloyd)
								CLEAR_PED_TASKS(pedFloyd)
								IF iRandBeckon = 0
									TASK_PLAY_ANIM(pedFloyd,"MissHeistDocksSetup1","Beckon")
								ELIF iRandBeckon = 1 
									TASK_PLAY_ANIM(pedFloyd,"missheistdockssetup1ig_8","are_you_coming")
								ELIF iRandBeckon = 2
									TASK_PLAY_ANIM(pedFloyd,"missheistdockssetup1ig_8","Are_You_Gonna_Hurt")
								ELIF iRandBeckon = 3
									TASK_PLAY_ANIM(pedFloyd,"missheistdockssetup1ig_8","its_over_here_if")
								ELIF iRandBeckon = 4
									TASK_PLAY_ANIM(pedFloyd,"missheistdockssetup1ig_8","this_dont_make_me")
								ENDIF
								bSetYourTimer = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF GET_DISTANCE_BETWEEN_PEDS(pedFloyd,PLAYER_PED_ID()) < 3
				//PRINTSTRING("WAYPOINT ACTION - DISTANCE GREATER THAN 1")//PRINTNL()
				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedFloyd)
					WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT("floyddocks1",GET_ENTITY_COORDS(pedFloyd),iWayPoint)
					TASK_FOLLOW_WAYPOINT_RECORDING(pedFloyd, "floyddocks1", iWayPoint)
				ENDIF
			ENDIF
		ENDIF
		
		
		
	ENDIF
ENDPROC

//Triggers and sets up ambient events around the docks
PROC MANAGE_DOCKS_AMBIENCE()
	
	VEHICLE_INDEX vehTemp
	
	IF eMissionStage < MISSION_STAGE_5
	AND eMissionStage > MISSION_STAGE_1
		IF DOES_ENTITY_EXIST(s_sp_forklift_supervisorA[0].ped)
			IF NOT IS_PED_INJURED(s_sp_forklift_supervisorA[0].ped)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
					IF IS_VEHICLE_DRIVEABLE(vehTemp)
						IF NOT IS_PED_INJURED(s_sp_forklift_supervisorA[0].ped)
							IF IS_ENTITY_TOUCHING_ENTITY(vehTemp,s_sp_forklift_supervisorA[0].ped)
								reason_for_fail = COVER_BLOWN
								eMissionStage = MISSION_STAGE_FAIL
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_PED_RAGDOLL(s_sp_forklift_supervisorA[0].ped)
					reason_for_fail = COVER_BLOWN
					eMissionStage = MISSION_STAGE_FAIL
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//CLEANING UP WASTE
	IF eMissionStage > MISSION_STAGE_1
	AND eMissionStage < MISSION_STAGE_5
		IF NOT HAS_LABEL_BEEN_TRIGGERED("WORLD_HUMAN_JANITOR")
			IF NOT IS_PED_INJURED(pedDockWorker[3])
				TASK_START_SCENARIO_AT_POSITION(pedDockWorker[3],"WORLD_HUMAN_JANITOR",<<-62.19, -2539.51, 6.01>>,-157.31)
				SET_LABEL_AS_TRIGGERED("WORLD_HUMAN_JANITOR",TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	//UPON ARRIVING TO THE DOCKS AMBIENCE
	IF eMissionStage = MISSION_STAGE_2
		IF bTriggerSetPiece[9] = FALSE
			IF DOES_ENTITY_EXIST(pedDockWorker[1])
				IF NOT IS_PED_INJURED(pedDockWorker[1])
					TASK_START_SCENARIO_IN_PLACE(pedDockWorker[1],"WORLD_HUMAN_STAND_MOBILE")
					bTriggerSetPiece[9] = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF bTriggerSetPiece[10] = FALSE
			IF IS_VEHICLE_DRIVEABLE(vehDocks[3])
				IF NOT IS_PED_INJURED(pedDockWorker[17]) 
					REQUEST_VEHICLE_RECORDING(1,"AWDocks5")
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDocks5")
						IF IS_PED_IN_VEHICLE(pedDockWorker[17],vehDocks[3])
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehDocks[3])
								START_PLAYBACK_RECORDED_VEHICLE(vehDocks[3],1,"AWDocks5")
								SET_PLAYBACK_SPEED(vehDocks[3],0.7)
								bTriggerSetPiece[10] = TRUE
							ENDIF
						ELSE
							IF IS_VEHICLE_SEAT_FREE(vehDocks[3],VS_DRIVER)
								SET_PED_INTO_VEHICLE(pedDockWorker[17],vehDocks[3])
							ENDIF
						ENDIF
					ELSE
						REQUEST_VEHICLE_RECORDING(1,"AWDocks5")
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(pedDockWorker[17]) 
					DELETE_PED(pedDockWorker[17]) 
				ENDIF
			ENDIF
		ENDIF
		
		IF TIMERA() > 4000
			IF bTriggerSetPiece[11] = FALSE
				IF NOT DOES_ENTITY_EXIST(vehDocks[2])
					IF IS_VEHICLE_DRIVEABLE(vehDocks[2])
						IF NOT IS_PED_INJURED(pedDockWorker[18]) 
							REQUEST_VEHICLE_RECORDING(1,"AWDocks4")
							IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDocks4")
								IF IS_PED_IN_VEHICLE(pedDockWorker[18],vehDocks[2])
									IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehDocks[2])
										START_PLAYBACK_RECORDED_VEHICLE(vehDocks[2],1,"AWDocks4")
										SET_PLAYBACK_SPEED(vehDocks[2],0.7)
										bTriggerSetPiece[11] = TRUE
									ENDIF
								ELSE
									IF IS_VEHICLE_SEAT_FREE(vehDocks[2],VS_DRIVER)
										SET_PED_INTO_VEHICLE(pedDockWorker[18],vehDocks[2])
									ENDIF
								ENDIF
							ELSE
								REQUEST_VEHICLE_RECORDING(1,"AWDocks4")
							ENDIF
						ENDIF
					ELSE
						IF DOES_ENTITY_EXIST(pedDockWorker[18])
							DELETE_PED(pedDockWorker[18])
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF eMissionStage >= MISSION_STAGE_4
			IF DOES_ENTITY_EXIST(vehDocks[2])
				IF IS_VEHICLE_DRIVEABLE(vehDocks[2])
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehDocks[2]) > 30
						IF NOT IS_ENTITY_ON_SCREEN(vehDocks[2])
							DELETE_VEHICLE(vehDocks[2])
							SET_MODEL_AS_NO_LONGER_NEEDED(BIFF)
							REMOVE_VEHICLE_RECORDING(1,"AWDocks4")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehDocks[3])
				IF IS_VEHICLE_DRIVEABLE(vehDocks[3])
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),vehDocks[3]) > 30
						IF NOT IS_ENTITY_ON_SCREEN(vehDocks[3])
							DELETE_VEHICLE(vehDocks[3])
							SET_MODEL_AS_NO_LONGER_NEEDED(BIFF)
							REMOVE_VEHICLE_RECORDING(1,"AWDocks5")
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF eMissionStage > MISSION_STAGE_2
	AND eMissionStage < MISSION_STAGE_10
			IF NOT IS_PED_INJURED(pedFloyd)
				IF bTriggerSetPiece[0] = FALSE
					IF NOT IS_PED_INJURED(pedDockWorker[11]) AND IS_VEHICLE_DRIVEABLE(vehDocks[0])
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_ENTER_VEHICLE(NULL,vehDocks[0],1)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(pedDockWorker[11], seqMain)
					ENDIF
					bTriggerSetPiece[0] = TRUE
				ENDIF
				
				IF bTriggerSetPiece[1] = FALSE
					IF NOT IS_PED_INJURED(pedDockWorker[11]) AND IS_VEHICLE_DRIVEABLE(vehDocks[0])
						IF IS_PED_IN_VEHICLE(pedDockWorker[11],vehDocks[0])
							//START_PLAYBACK_RECORDED_VEHICLE(vehDocks[0],1,"AWDock1")
							//SET_PLAYBACK_SPEED(vehDocks[0],0.7)
							bTriggerSetPiece[1] = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF bTriggerSetPiece[2] = FALSE
					IF NOT IS_PED_INJURED(pedDockWorker[4])
					AND NOT IS_PED_INJURED(pedDockWorker[5])
						//GUYS BY THE WALL
						//TASK_START_SCENARIO_AT_POSITION(pedDockWorker[4],"WORLD_HUMAN_LEANING",<<-109.06, -2481.83, 6.02>>,144.23)
						TASK_START_SCENARIO_AT_POSITION(pedDockWorker[4],"WORLD_HUMAN_SMOKING",<<-109.06, -2481.83, 6.02>>,144.23)
						TASK_START_SCENARIO_AT_POSITION(pedDockWorker[5],"WORLD_HUMAN_HANG_OUT_STREET",<<-110.18, -2482.25, 6.02>>,-80.21)
						bTriggerSetPiece[2] = TRUE
					ENDIF
				ENDIF
				
				IF eMissionStage = MISSION_STAGE_4
				OR eMissionStage = MISSION_STAGE_5
					//BEHIND LORRY
					IF bTriggerSetPiece[5] = FALSE
						IF NOT IS_PED_INJURED(pedDockWorker[14])
							TASK_START_SCENARIO_IN_PLACE(pedDockWorker[14],"WORLD_HUMAN_DRINKING")
							bTriggerSetPiece[5] = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				//FORKLIFT COMING PAST
				IF NOT IS_PED_INJURED(pedFloyd)
					IF bTriggerSetPiece[6] = FALSE
						IF IS_ENTITY_AT_COORD(pedFloyd,<<-106.5262, -2482.1042, 5.0181>>,<<5,5,5>>)
						OR eMissionStage > MISSION_STAGE_4 AND eMissionStage < MISSION_STAGE_10
							IF NOT IS_PED_INJURED(pedDockWorker[12]) AND IS_VEHICLE_DRIVEABLE(vehDocks[1])
								IF IS_VEHICLE_SEAT_FREE(vehDocks[1],VS_DRIVER)
									iBlockReplayCameraTimer = GET_GAME_TIMER() + 1000 // For B*2226188
									SET_VEHICLE_DOORS_LOCKED(vehDocks[1],VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
									SET_PED_INTO_VEHICLE(pedDockWorker[12],vehDocks[1])
									SET_FORKLIFT_FORK_HEIGHT(vehDocks[1],1.0)
								ELSE
									DELETE_PED(pedDockWorker[12])
								ENDIF
								bTriggerSetPiece[6] = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF bTriggerSetPiece[6] = TRUE
						IF bTriggerSetPiece[7] = FALSE
							REQUEST_VEHICLE_RECORDING(1,"AWDocks2")
							IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDocks2")
								IF NOT IS_PED_INJURED(pedDockWorker[12]) AND IS_VEHICLE_DRIVEABLE(vehDocks[1])
									IF NOT IS_PED_INJURED(pedDockWorker[7])
										TASK_START_SCENARIO_IN_PLACE(pedDockWorker[7],"WORLD_HUMAN_AA_SMOKE")
									ENDIF
									IF IS_PED_IN_VEHICLE(pedDockWorker[12],vehDocks[1])
										START_PLAYBACK_RECORDED_VEHICLE(vehDocks[1],1,"AWDocks2")
										SET_PLAYBACK_SPEED(vehDocks[1],1.1)
										bTriggerSetPiece[7] = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
ENDPROC

// ======================================================================
//                            Phone Stuff
// ======================================================================
PROC MANAGE_PHONE_HELP()
	//HELP STUFF

	SWITCH iHelp 
	
		CASE 0
			
			IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_PICTURE_TAKE_BUTTON_PRESSED)
			
			ENDIF
			
			IF NOT IS_CELLPHONE_ON_HOMESCREEN()
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PH_HELP2")
					CLEAR_HELP()
				ENDIF
			ENDIF
			
			IF NOT IS_PHONE_ONSCREEN()
				IF iPhotosTaken = 3
					IF NOT HAS_CONTACT_RECEIVED_PICTURE_MESSAGE(CHAR_RON)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("PH_HELP6")
							PRINT_HELP("PH_HELP6")
							SET_LABEL_AS_TRIGGERED("PH_HELP6",TRUE)
						ENDIF
					ENDIF
				ENDIF
				
//				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PH_HELP1")
//					CLEAR_HELP()
//					PRINT_HELP("PH_HELP1")
//				ENDIF
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PH_HELP2")
					CLEAR_HELP()
				ENDIF
			ELSE
				//SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PH_HELP1")
					CLEAR_HELP()
				ENDIF
				
				iHelp ++
			ENDIF
		BREAK
		
		CASE 1
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera")) = 0
				IF iPhotosTaken != 3
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PH_HELP2")
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP()
						ENDIF
						PRINT_HELP_FOREVER("PH_HELP2")
					ENDIF
				ENDIF
			ELSE
				IF iPhotosTaken = 3
					IF NOT HAS_CONTACT_RECEIVED_PICTURE_MESSAGE(CHAR_RON)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("PH_HELP6")
							PRINT_HELP("PH_HELP6")
							SET_LABEL_AS_TRIGGERED("PH_HELP6",TRUE)
						ENDIF
					ENDIF
				ENDIF
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
				ENDIF
				IF IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_CAMERA_READY_TO_TAKE_PHOTO)
					iHelp ++
				ENDIF
			ENDIF
			
			IF NOT IS_PHONE_ONSCREEN()
				iHelp = 0
			ENDIF
		BREAK
		
		CASE 2
			IF iPhotosTaken = 0 
				//REMOVED HELP TEXT
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PHOTO_H1A")
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
					SETTIMERA(0)
					IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_PICTURE_TAKE_BUTTON_PRESSED)
						PRINT_HELP("PHOTO_H1A",60000)
					ENDIF
				ENDIF
			ELSE
				IF iPhotosTaken = 1
					iHelp ++
				ENDIF
				
				IF iPhotosTaken = 2
					iHelp = 4
				ENDIF
				
				IF iPhotosTaken = 3
					iHelp = 5
				ENDIF
			ENDIF
			
			IF IS_PHONE_ONSCREEN()
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))= 0
					iHelp = 1
				ENDIF
			ELSE
				iHelp = 0
			ENDIF
		BREAK
		
		CASE 3
			IF iPhotosTaken = 1 
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PHOTO_H2A")
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
					SETTIMERA(0)
					IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_PICTURE_TAKE_BUTTON_PRESSED)
						PRINT_HELP("PHOTO_H2A",60000)
					ENDIF
				ENDIF
			ELSE
				IF iPhotosTaken = 2
					iHelp ++
				ENDIF
				
				IF iPhotosTaken = 3
					iHelp = 5
				ENDIF
			ENDIF
			
			IF IS_PHONE_ONSCREEN()
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))= 0
					iHelp = 1
				ENDIF
			ELSE
				iHelp = 0
			ENDIF
		BREAK
		
		CASE 4
			IF iPhotosTaken = 2 
				//REMOVED HELP TEXT
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PHOTO_H3A")
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
					SETTIMERA(0)
					IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_PICTURE_TAKE_BUTTON_PRESSED)
						PRINT_HELP("PHOTO_H3A",60000)
					ENDIF
				ENDIF
			ELSE
				IF iPhotosTaken = 3
					//ENABLE_AUTO_SCROLL_TO_CONTACT(CHAR_RON)
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
					SETTIMERA(0)
					IF NOT IS_BIT_SET (BitSet_CellphoneDisplay, g_BS_PICTURE_TAKE_BUTTON_PRESSED)
						PRINT_HELP_FOREVER("PH_HELP5")
						iHelp ++
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_PHONE_ONSCREEN()
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))= 0
					iHelp = 1
				ENDIF
			ELSE
				iHelp = 0
			ENDIF
		BREAK
		
		CASE 5
			IF IS_PHONE_ONSCREEN()
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))!= 0
					IF IS_CONTACTS_LIST_ON_SCREEN()
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PH_HELP5")
							CLEAR_HELP()
						ENDIF
					ENDIF
					
					IF NOT HAS_CONTACT_RECEIVED_PICTURE_MESSAGE(CHAR_RON)
					
					ELSE
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PH_HELP5")
							CLEAR_HELP()
						ENDIF
						iHelp ++
					ENDIF
				ELSE
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
					iHelp = 0
				ENDIF
			ELSE
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
				ENDIF
				iHelp = 0
			ENDIF
		BREAK
		
		CASE 6

			IF iPhotosTaken = 3 
				IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))= 0
					iHelp ++
					CLEAR_HELP()
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH
	
	
	
ENDPROC

// ======================================================================
//                                 Models 
// ======================================================================
//Cleans up the loaded models

//Cleans up the loaded car recs


//Clean up anim dictionary
PROC CLEANUP_LOADED_ANIM_ARRAY()
	INT i
	IF iNumberAnimsLoaded > 0
		FOR i = 0 TO (iNumberAnimsLoaded -1)
			IF HAS_ANIM_DICT_LOADED(sAnimLoaded[i])	
				REMOVE_ANIM_DICT(sAnimLoaded[i])
				////PRINTNL()PRINTSTRING("Anim cleaned up = ")PRINTINT(i)//PRINTNL()
			ENDIF
		ENDFOR
	ENDIF
	////PRINTNL()PRINTSTRING("CLEANUP_LOADED_CARREC_ARRAY done")//PRINTNL()
	iNumberAnimsLoaded = 0
ENDPROC

//PURPOSE: adds a single Anim Request to the Array
PROC ADD_ANIM_REQUEST_TO_ARRAY(STRING Anim)

	INT i
	BOOL bAnimRequested
	
	REQUEST_ANIM_DICT(Anim)
	
	IF iNumberAnimsLoaded > 0	
		FOR i = 0 TO (iNumberAnimsLoaded -1)
			IF IS_STRING_EQUAL_TO_STRING(Anim,sAnimLoaded[i])
				bAnimRequested = TRUE
				//PRINTSTRING("Anim already requested")//PRINTNL()
			ENDIF
		ENDFOR
		IF NOT bAnimRequested
			sAnimLoaded[iNumberAnimsLoaded] = Anim
			iNumberAnimsLoaded++
			//PRINTSTRING("number of anims requested : ")PRINTINT(iNumberAnimsLoaded)//PRINTNL()
		ENDIF
	ELIF iNumberAnimsLoaded = 0	
		sAnimLoaded[0] = Anim
		//PRINTSTRING("first anim requested")//PRINTNL()
		iNumberAnimsLoaded++
	ENDIF

ENDPROC

//PURPOSE: checks to see if all the requested models in the array are in memory
FUNC BOOL ARE_REQUESTED_ANIMS_LOADED() 

	INT i
	IF iNumberAnimsLoaded > 0
		FOR i = 0 TO (iNumberAnimsLoaded-1)
			IF NOT HAS_ANIM_DICT_LOADED(sAnimLoaded[i])
				//PRINTSTRING("anims loading")//PRINTNL()
				WAIT(0)
				RETURN FALSE
			ENDIF
		ENDFOR
	ENDIF	
	
	//PRINTSTRING("anims loaded")//PRINTNL()
	
	RETURN TRUE
	
ENDFUNC

//              			  Pass, Fail, Clean Up  
// ======================================================================

//PURPOSE: gets the player into position, fades in and turns on control.
PROC INIT_STAGE(BOOL bWithFadeIn = TRUE)
	//GIVE_PED_NECESSARY_WEAPONS()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
			ENDIF
		ENDIF
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	ENDIF
	bRunFailChecks = TRUE
	bDialogue = FALSE
	IF bWithFadeIn
		CONTROL_FADE_IN(500)
	ENDIF
ENDPROC

PROC CLEANUP_AUDIO_SCENES()
	IF IS_AUDIO_SCENE_ACTIVE("DH_1_DRIVE_START")
		STOP_AUDIO_SCENE("DH_1_DRIVE_START")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("DH_1_DRIVE_APPROACH_DOCKS")
		STOP_AUDIO_SCENE("DH_1_DRIVE_APPROACH_DOCKS")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("DH_1_FOLLOW_FLOYD_START")
		STOP_AUDIO_SCENE("DH_1_FOLLOW_FLOYD_START")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("DH_1_FOLLOW_FLOYD_SEE_FREIGHTER")
		STOP_AUDIO_SCENE("DH_1_FOLLOW_FLOYD_SEE_FREIGHTER")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("DH_1_HANDLER_START")
		STOP_AUDIO_SCENE("DH_1_HANDLER_START")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("DH_1_CLIMB_UP_CRANE")
		STOP_AUDIO_SCENE("DH_1_CLIMB_UP_CRANE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("DH_1_USE_CRANE")
		STOP_AUDIO_SCENE("DH_1_USE_CRANE")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("DH_1_TAKE_PHOTOS")
		STOP_AUDIO_SCENE("DH_1_TAKE_PHOTOS")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("DH_1_PHONE_RON")
		STOP_AUDIO_SCENE("DH_1_PHONE_RON")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("DH_1_GET_TO_TRUCK")
		STOP_AUDIO_SCENE("DH_1_GET_TO_TRUCK")
	ENDIF
	IF IS_AUDIO_SCENE_ACTIVE("DH_1_DRIVE_TO_DOCKS")
		STOP_AUDIO_SCENE("DH_1_DRIVE_TO_DOCKS")
	ENDIF
ENDPROC

PROC SET_UP_DOCKS_RADIO(BOOL bEnable = TRUE)
	SET_STATIC_EMITTER_ENABLED("SE_LS_DOCKS_RADIO_01",bEnable)
	SET_STATIC_EMITTER_ENABLED("SE_LS_DOCKS_RADIO_02",bEnable)
	SET_STATIC_EMITTER_ENABLED("SE_LS_DOCKS_RADIO_03",bEnable)
	SET_STATIC_EMITTER_ENABLED("SE_LS_DOCKS_RADIO_04",bEnable)
	SET_STATIC_EMITTER_ENABLED("SE_LS_DOCKS_RADIO_05",bEnable)
	SET_STATIC_EMITTER_ENABLED("SE_LS_DOCKS_RADIO_06",bEnable)
	SET_STATIC_EMITTER_ENABLED("SE_LS_DOCKS_RADIO_07",bEnable)
	SET_STATIC_EMITTER_ENABLED("SE_LS_DOCKS_RADIO_08",bEnable)
ENDPROC

//PURPOSE: cleans up all resources used in the mission
PROC MISSION_CLEANUP(BOOL bRestart = FALSE, BOOL bMissionPassed = FALSE)
	//IF IS_PLAYER_PLAYING(PLAYER_ID())
		#IF IS_DEBUG_BUILD 
//			IF DOES_WIDGET_GROUP_EXIST(wgDOCKSSETUP)
//				DELETE_WIDGET_GROUP(wgDOCKSSETUP)
//			ENDIF
		#ENDIF
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_PED_COMBAT_ATTRIBUTES(PLAYER_PED_ID(),CA_DO_DRIVEBYS,TRUE)
			SET_PED_PATH_CAN_USE_LADDERS(PLAYER_PED_ID(),TRUE)
		ENDIF
		
		REPOSITION_LANDSCAPE_PHONE_FOR_LONG_SUBTITLES (FALSE)
		SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(FALSE)
		CASCADE_SHADOWS_INIT_SESSION()
		SET_USER_RADIO_CONTROL_ENABLED(TRUE)
		SET_UP_DOCKS_RADIO(FALSE)
		CLEANUP_AUDIO_SCENES()
		CLEAR_TIMECYCLE_MODIFIER()
		REMOVE_SCENARIO_BLOCKING_AREAS()
		DISABLE_CHEAT(CHEAT_TYPE_ALL, FALSE) 
		REMOVE_FORCED_OBJECT(<<479.2571, -3115.5513, 5.0701>>,50,PROP_GATE_DOCKS_LD)
		
		ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_1")
		ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_2")
		ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_3")
		ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_4")
		ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_5")
		ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_6")
		ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_7")
		ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_8")

		IF IS_SCREEN_FADING_OUT()
		OR IS_SCREEN_FADED_OUT()
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				
				//RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID()) 
			ENDIF
		ENDIF
		
		IF GET_IS_WAYPOINT_RECORDING_LOADED("cranassist")
			REMOVE_WAYPOINT_RECORDING("cranassist")
		ENDIF
		
		IF b_is_audio_scene_active
			STOP_AUDIO_SCENE("CAR_THEFT_EXPORT_CARS_CRANE_SECTION_SCENE")
			b_is_audio_scene_active = FALSE
		ENDIF
		
		IF NOT HAS_SOUND_FINISHED(iCraneSound)
			STOP_SOUND(iCraneSound)
			RELEASE_SOUND_ID(iCraneSound)
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("DOCKS_HEIST_USING_CRANE")
			STOP_AUDIO_SCENE("DOCKS_HEIST_USING_CRANE")
		ENDIF
		
		IF IS_THIS_TV_AVAILABLE_FOR_USE(TV_LOC_TREVOR_VENICE)
			IF IS_THIS_TV_FORCED_ON(TV_LOC_TREVOR_VENICE)
				FORCE_STOP_TV(TV_LOC_TREVOR_VENICE)
			ENDIF
		ENDIF
		
		UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_DOCKS_GATE_IN,PLAYER_PED_ID())
		//DOOR_SYSTEM_SET_HOLD_OPEN(g_sAutoDoorData[AUTODOOR_MIL_DOCKS_GATE_IN].doorID, FALSE)
		DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_MIL_DOCKS_GATE_IN].doorID, 0.0)
		DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_MIL_DOCKS_GATE_IN].doorID,DOORSTATE_LOCKED)
		
		
		// Fix for PT - we need to stop locking the door open when we're done as the autodoor
		// system locks on to this door and forgets about the rest.
		LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_DOCKS_BACK_GATE_OUT, FALSE, FALSE)
		LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_DOCKS_FRONT_GATE_OUT, FALSE, FALSE)
		
		
		UNLOCK_MINIMAP_ANGLE()
		SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
		SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), FALSE)
		ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE) 
		BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE)	
		CLEAR_BIT(BitSet_CellphoneDisplay,g_BS_FORCE_CELLPHONE_CAM_AUTO_CONTINUE)
		DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
		HIDE_ACTIVE_PHONE(FALSE)
		bIgnoreTheseFails = FALSE
		
		ROPE_UNLOAD_TEXTURES()
		
		IF DOES_BLIP_EXIST(blipHaulerLoadingArea)
			REMOVE_BLIP(blipHaulerLoadingArea)
		ENDIF
		
		INT iThisCount
		REPEAT COUNT_OF(bDoneConversationAnim) iThisCount
			bDoneConversationAnim[iThisCount] = FALSE
		ENDREPEAT 
		
		CLEAR_TRIGGERED_LABELS()
		
		Blip_player = GET_MAIN_PLAYER_BLIP_ID()
		IF DOES_BLIP_EXIST(Blip_player)
			SET_BLIP_ALPHA(Blip_player,100)
		ENDIF
		
		SET_VEHICLE_POPULATION_BUDGET(3)
		SET_PED_POPULATION_BUDGET(3)
		
		IF IS_CUTSCENE_ACTIVE()
		AND IS_CUTSCENE_PLAYING()
			STOP_CUTSCENE()
			REMOVE_CUTSCENE()
			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_WETNESS(PLAYER_PED_ID())
			ENDIF
		ENDIF
		
		IF bRestart
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					DELETE_VEHICLE(vanByDocks)
				ELSE
					//TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
					ENDIF
					DELETE_VEHICLE(vanByDocks)
				ENDIF
			ELSE
				DELETE_VEHICLE(vanByDocks)
			ENDIF
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vanByDocks)
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedDockWorker[14])
			IF NOT IS_PED_INJURED(pedDockWorker[14])
				DISABLE_PED_PAIN_AUDIO(pedDockWorker[14],TRUE)
				STOP_PED_SPEAKING(pedDockWorker[14],TRUE)
				IF NOT IS_ENTITY_ON_SCREEN(pedDockWorker[14])
					DELETE_PED(pedDockWorker[14])
				ELSE
					SET_PED_AS_NO_LONGER_NEEDED(pedDockWorker[14])
				ENDIF
			ENDIF
		ENDIF

		v_crane_pos = << -53.6982, -2415.7900, 5.0500 >> //<< -53.2665, -2415.8633, 5.066 >>
		bContainerSetUpForDrive = FALSE
		g_bDocksBackGatesIgnoreTrevorsUniform = FALSE
		
		//RESET ALL SYNCH SCENE DATA
		
		INT iTemp
		REPEAT COUNT_OF(bSetPiece) iTemp
			bSetPiece[iTemp] = FALSE
		ENDREPEAT
		
		REPEAT COUNT_OF(bSSComplete) iTemp
			bSSComplete[iTemp] = FALSE
		ENDREPEAT
		
		REPEAT COUNT_OF(s_sv_car_to_admire) iCount
			CLEAN_UP_SETPIECE_VEHILCE(s_sv_car_to_admire[iCount],TRUE,bRestart)
		ENDREPEAT
		
		REPEAT COUNT_OF(s_sp_blackwater_with_dockworker) iCount
			CLEAN_UP_SETPIECE_PED(s_sp_blackwater_with_dockworker[iCount],TRUE,bRestart)
		ENDREPEAT
		
		REPEAT COUNT_OF(s_sp_dock_workers_on_pipe) iCount
			CLEAN_UP_SETPIECE_PED(s_sp_dock_workers_on_pipe[iCount],TRUE,bRestart)
		ENDREPEAT
		
		REPEAT COUNT_OF(s_sp_dock_workers_talking) iCount
			CLEAN_UP_SETPIECE_PED(s_sp_dock_workers_talking[iCount],TRUE,bRestart)
		ENDREPEAT
		
		REPEAT COUNT_OF(s_sp_forklift_supervisorA) iCount
			CLEAN_UP_SETPIECE_PED(s_sp_forklift_supervisorA[iCount],TRUE,bRestart)
		ENDREPEAT
		
		REPEAT COUNT_OF(s_sp_forklift_supervisorB) iCount
			CLEAN_UP_SETPIECE_PED(s_sp_forklift_supervisorB[iCount],TRUE,bRestart)
		ENDREPEAT
		
		REPEAT COUNT_OF(s_sp_blackwater_with_dockworker) iCount
			CLEAN_UP_SETPIECE_PED(s_sp_blackwater_with_dockworker[iCount],TRUE,bRestart)
		ENDREPEAT
		
		REPEAT COUNT_OF(s_sp_gantry_guys_static) iCount
			CLEAN_UP_SETPIECE_PED(s_sp_gantry_guys_static[iCount],TRUE,bRestart)
		ENDREPEAT
		
		REPEAT COUNT_OF(s_sp_guys_around_car) iCount
			CLEAN_UP_SETPIECE_PED(s_sp_guys_around_car[iCount],TRUE,bRestart)
		ENDREPEAT
		
		REPEAT COUNT_OF(s_sp_supervisor1) iCount
			CLEAN_UP_SETPIECE_PED(s_sp_supervisor1[iCount],TRUE,bRestart)
		ENDREPEAT
		
		REPEAT COUNT_OF(s_sp_welder) iCount
			CLEAN_UP_SETPIECE_PED(s_sp_welder[iCount],TRUE,bRestart)
		ENDREPEAT
		
		
		SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
		DISABLE_TAXI_HAILING(FALSE)
		CLEANUP_LOADED_MODEL_ARRAY()
		CLEANUP_LOADED_ANIM_ARRAY()
		CLEANUP_LOADED_CARREC_ARRAY("")
		
		REMOVE_PED_FOR_DIALOGUE(sSpeech, 0)
		REMOVE_PED_FOR_DIALOGUE(sSpeech, 1)
		REMOVE_PED_FOR_DIALOGUE(sSpeech, 2)
		REMOVE_PED_FOR_DIALOGUE(sSpeech, 3)
		REMOVE_PED_FOR_DIALOGUE(sSpeech, 4)
		REMOVE_PED_FOR_DIALOGUE(sSpeech, 5)
		REMOVE_PED_FOR_DIALOGUE(sSpeech, 6)
		REMOVE_PED_FOR_DIALOGUE(sSpeech, 7)
		REMOVE_PED_FOR_DIALOGUE(sSpeech, 8)
		REMOVE_PED_FOR_DIALOGUE(sSpeech, 9)
		
		KILL_ANY_CONVERSATION()
		
		IF DOES_BLIP_EXIST(blipHandlerContainers[0])
			REMOVE_BLIP(blipHandlerContainers[0])
		ENDIF
		
		IF DOES_BLIP_EXIST(blipHandlerContainers[1])
			REMOVE_BLIP(blipHandlerContainers[1])
		ENDIF
		
		IF DOES_BLIP_EXIST(blipHandlerContainers[2])
			REMOVE_BLIP(blipHandlerContainers[2])
		ENDIF
		
		
		bInhibitCrane = FALSE
		bForceNoGrab = FALSE
		bDoneWithTruck = FALSE

		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_VEHICLE_DRIVEABLE(vehMission)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehMission)
					IF IS_SCREEN_FADED_OUT() OR IS_SCREEN_FADING_OUT()
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						ENDIF
					ENDIF
				ELSE
					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehMission, FALSE)
						IF DOES_ENTITY_EXIST(vehMission)
							DELETE_VEHICLE(vehMission)
						ENDIF
					ENDIF
				ENDIF
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
			ENDIF
		ENDIF
		
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appCamera"))> 0
			HANG_UP_AND_PUT_AWAY_PHONE()
			SET_GAME_PAUSED (FALSE)
		ENDIF

		IF NOT bRestart 
			INIT_STAGE(FALSE)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(FORKLIFT,FALSE)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(BODHI2,FALSE)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(BIFF,FALSE)
		ENDIF
		
		//CLEAN UP OBJECTS
		FOR iCount = 0 TO MAX_NUM_OF_CLIPBOARDS  -1
			IF bRestart
				IF DOES_ENTITY_EXIST(objClipboard[iCount])
					DELETE_OBJECT(objClipboard[iCount])
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(objClipboard[iCount])
					SET_OBJECT_AS_NO_LONGER_NEEDED(objClipboard[iCount])
				ENDIF
			ENDIF
		ENDFOR
		
		FOR iCount = 0 TO MAX_NUM_OF_CLIPBOARDS  -1
			IF bRestart
				IF DOES_ENTITY_EXIST(objPencil[iCount])
					DELETE_OBJECT(objPencil[iCount])
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(objPencil[iCount])
					SET_OBJECT_AS_NO_LONGER_NEEDED(objPencil[iCount])
				ENDIF
			ENDIF
		ENDFOR
		
		iMissionDialogue = 0
		iManagePickerAudio = 0
		
		//Vehs
		FOR iCount = 0 To (MAX_MISSION_VEH -1)
			IF DOES_ENTITY_EXIST(mission_veh[iCount].veh)
				IF bRestart
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							DELETE_VEHICLE(mission_veh[iCount].veh)
						ELSE
							//TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
							ENDIF
							DELETE_VEHICLE(mission_veh[iCount].veh)
						ENDIF
					ELSE
						DELETE_VEHICLE(mission_veh[iCount].veh)
					ENDIF
				ELSE
					SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[iCount].veh)
				ENDIF
			ENDIF
			IF DOES_BLIP_EXIST(mission_veh[iCount].blip)
				REMOVE_BLIP(mission_veh[iCount].blip)
			ENDIF
		ENDFOR
		//Enemies
		FOR iCount = 0 To (MAX_ENEMY_PED -1)
			IF DOES_ENTITY_EXIST(enemy[iCount].ped)
				IF bRestart
					DELETE_PED(enemy[iCount].ped)
				ELSE
					SET_PED_AS_NO_LONGER_NEEDED(enemy[iCount].ped)
				ENDIF
			ENDIF
			IF DOES_BLIP_EXIST(enemy[iCount].blip)
				REMOVE_BLIP(enemy[iCount].blip)
			ENDIF
		ENDFOR
		//Buddies
		FOR iCount = 0 To (MAX_BUDDY_PED -1)
			IF DOES_ENTITY_EXIST(buddy[iCount].ped)
				IF bRestart
					DELETE_PED(buddy[iCount].ped)
				ELSE
					SET_PED_AS_NO_LONGER_NEEDED(buddy[iCount].ped)
				ENDIF
			ENDIF
			IF DOES_BLIP_EXIST(buddy[iCount].blip)
				REMOVE_BLIP(buddy[iCount].blip)
			ENDIF
		ENDFOR
		//reenable the hotswap Stuff
		IF bDisabledHotSwap = TRUE
			ENABLE_SELECTOR()
		ENDIF
			
		IF bPatrolCreated[0] = TRUE
			DELETE_PATROL_ROUTE("miss_dock")
			bPatrolCreated[0] = FALSE
		ENDIF
		
		IF bPatrolCreated[1] = TRUE
			DELETE_PATROL_ROUTE("miss_merc0")
			DELETE_PATROL_ROUTE("miss_merc1")
			DELETE_PATROL_ROUTE("miss_merc2")
			bPatrolCreated[1] = FALSE
		ENDIF
		
		IF bMercPatrolCreated[0] = TRUE
			DELETE_PATROL_ROUTE("miss_merc0")
			DELETE_PATROL_ROUTE("miss_merc1")
			DELETE_PATROL_ROUTE("miss_merc2")
			bPatrolCreated[0] = FALSE
			bPatrolCreated[1] = FALSE
			bPatrolCreated[2] = FALSE
		ENDIF

		FOR iCount = 0 TO (iTotalNumberOfPtX -1)
			IF bPTX_Triggered[iCount]
				REMOVE_PARTICLE_FX(PTX_SHIP_EXPLOSIONS[iCount])
				bPTX_Triggered[iCount] = FALSE
			ENDIF
		ENDFOR
		
		//Dock Workers
		FOR iCount = 0 To (iTotalDockWorker -1)
			IF DOES_ENTITY_EXIST(pedDockWorker[iCount])
				IF bRestart
					DELETE_PED(pedDockWorker[iCount])
				ELSE
					SET_PED_AS_NO_LONGER_NEEDED(pedDockWorker[iCount])
				ENDIF
			ENDIF
			IF DOES_BLIP_EXIST(blipDockWorker[iCount])
				REMOVE_BLIP(blipDockWorker[iCount])
			ENDIF
		ENDFOR
		//Mercs
		FOR iCount = 0 To (iTotalMerc -1)
			IF DOES_ENTITY_EXIST(pedMerc[iCount])
				IF bRestart
					DELETE_PED(pedMerc[iCount])
				ELSE
					SET_PED_AS_NO_LONGER_NEEDED(pedMerc[iCount])
				ENDIF
			ENDIF
		ENDFOR
		
		//Player entities
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			IF bRestart
				DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			ENDIF
		ENDIF

		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			IF bRestart
				DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			IF bRestart
				DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedFloyd)
			IF bRestart
				DELETE_PED(pedFloyd)
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(pedFloyd)
			ENDIF
		ENDIF
		
		IF bMissionPassed = FALSE
			IF DOES_ENTITY_EXIST(pedWade)
				IF bRestart
					DELETE_PED(pedWade)
				ELSE
					SET_PED_AS_NO_LONGER_NEEDED(pedWade)
				ENDIF
			ENDIF
		ENDIF
		
		//Docks Vehicles
		FOR iCount = 0 To (iTotalVehicles-1)
			IF DOES_ENTITY_EXIST(vehDocks[iCount])
				IF bRestart
					DELETE_VEHICLE(vehDocks[iCount])
				ELSE
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehDocks[iCount])
				ENDIF
			ENDIF
		ENDFOR
		
		IF DOES_ENTITY_EXIST(vehMission)
			IF bRestart
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						DELETE_VEHICLE(vehMission)
					ELSE
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						ENDIF
						DELETE_VEHICLE(vehMission)
					ENDIF
				ELSE
					DELETE_VEHICLE(vehMission)
				ENDIF
			ELSE
				IF NOT IS_ENTITY_DEAD(vehMission)
					IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehMission, FALSE)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehMission)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehFloydTruck)
			IF bRestart
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						DELETE_VEHICLE(vehFloydTruck)
					ELSE
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						ENDIF
						DELETE_VEHICLE(vehFloydTruck)
					ENDIF
				ELSE
					DELETE_VEHICLE(vehFloydTruck)
				ENDIF
			ELSE
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehFloydTruck, FALSE)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehFloydTruck)
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehTrailer2)
			IF bRestart
				DELETE_VEHICLE(vehTrailer2)
			ELSE
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTrailer2)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehFirstHauler)
			IF bRestart
				DELETE_VEHICLE(vehFirstHauler)
			ELSE
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehFirstHauler)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehTrailer1)
			IF bRestart
				DELETE_VEHICLE(vehTrailer1)
			ELSE
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTrailer1)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehBlocking)
			IF bRestart
				DELETE_VEHICLE(vehBlocking)
			ELSE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBlocking)
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehHandler)
			IF bRestart
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						DELETE_VEHICLE(vehHandler)
					ELSE
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						DELETE_VEHICLE(vehHandler)
					ENDIF
				ELSE
					DELETE_VEHICLE(vehHandler)
				ENDIF
			ELSE
				IF DOES_ENTITY_BELONG_TO_THIS_SCRIPT(vehHandler, FALSE)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehHandler)
				ENDIF
			ENDIF
		ENDIF
		
		//DELETE ROPES FIRST
		IF bAddedRopes
			INT i = 0
			REPEAT 8 i
				IF DOES_ROPE_EXIST(s_Crane.ropes[i])
					DELETE_ROPE(s_Crane.ropes[i])
				ENDIF
			ENDREPEAT
			bAddedRopes = FALSE
		ENDIF
		
		REMOVE_ANIM_DICT("map_objects")
	
		//DELETE THE CRANE AFTER THIS
		IF DOES_ENTITY_EXIST(s_crane.obj_main)
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						DETACH_ENTITY(PLAYER_PED_ID())
						SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-62.8851, -2406.8723, 5.0009>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(),338.2688)
					ENDIF
				ENDIF
				IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(s_crane.obj_spreader)
				FREEZE_ENTITY_POSITION(s_crane.obj_spreader,TRUE)
			ENDIF
			
			//PRINTSTRING("DELETING CRANE")//PRINTNL()
			IF IS_SCREEN_FADED_OUT()
				DELETE_OBJECT(s_crane.obj_main)
				DELETE_OBJECT(s_crane.obj_cabin)
				DELETE_OBJECT(s_crane.obj_spreader)
				DELETE_OBJECT(s_crane.obj_helper)
				DELETE_OBJECT(s_crane.obj_wheels[0])
				DELETE_OBJECT(s_crane.obj_wheels[1])
				DELETE_OBJECT(s_crane.obj_wheels[2])
				DELETE_OBJECT(s_crane.obj_wheels[3])
				DELETE_OBJECT(s_crane.obj_wheels[4])
				DELETE_OBJECT(s_crane.obj_wheels[5])
				DELETE_OBJECT(s_crane.obj_wheels[6])
				DELETE_OBJECT(s_crane.obj_wheels[7])
				DELETE_OBJECT(s_containers[0].obj_main)
				DELETE_OBJECT(s_containers[0].obj_left_door)
				DELETE_OBJECT(s_containers[0].obj_right_door)
				DELETE_OBJECT(s_containers[1].obj_main)
				DELETE_OBJECT(s_containers[1].obj_left_door)
				DELETE_OBJECT(s_containers[1].obj_right_door)
			ELSE
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_crane.obj_main)
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_crane.obj_cabin)
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_crane.obj_spreader)
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_crane.obj_helper)
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_crane.obj_wheels[0])
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_crane.obj_wheels[1])
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_crane.obj_wheels[2])
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_crane.obj_wheels[3])
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_crane.obj_wheels[4])
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_crane.obj_wheels[5])
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_crane.obj_wheels[6])
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_crane.obj_wheels[7])
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_containers[0].obj_main)
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_containers[0].obj_left_door)
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_containers[0].obj_right_door)
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_containers[1].obj_main)
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_containers[1].obj_left_door)
				SET_OBJECT_AS_NO_LONGER_NEEDED(s_containers[1].obj_right_door)
			ENDIF
		ENDIF
		
		IF IS_CELLPHONE_CAMERA_INHIBITED_FOR_DOCKS_SETUP()
			INHIBIT_CELLPHONE_CAMERA_FUNCTIONS_FOR_DOCKS_SETUP(FALSE)
		ENDIF

		//remove all blips
		CLEAN_OBJECTIVE_BLIP_DISPLAY()
		//remove all Models
		CLEANUP_LOADED_MODEL_ARRAY()
		CLEANUP_LOADED_CARREC_ARRAY("AWDocks1")
		CLEANUP_LOADED_ANIM_ARRAY()
		
		IF bRestart
			CLEAR_AREA(vPlayerStart,200,TRUE,TRUE)
			CLEAR_AREA_OF_OBJECTS(vPlayerStart,200)
			CLEAR_AREA(vDocks,500,TRUE,TRUE)
			CLEAR_AREA_OF_OBJECTS(vDocks,500)
			WASH_DECALS_IN_RANGE(vDocks,500,1)
			REMOVE_DECALS_IN_RANGE(vDocks,500) 
			REMOVE_ALL_PICKUPS_OF_TYPE(PICKUP_WEAPON_ASSAULTRIFLE)
			CLEAR_PRINTS()
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						DETACH_ENTITY(PLAYER_PED_ID())
					ENDIF
				ENDIF
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
			ENDIF
			
			//Allow particals to be displayed again
			FOR iCount = 0 TO (iTotalNumberOfPtX -1)
				bPTX_Triggered[iCount] = FALSE
			ENDFOR
		ENDIF
		
		//house keeping
		SET_WIDESCREEN_BORDERS(FALSE,0)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		DESTROY_ALL_CAMS()
		
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
		ENDIF
		
		SET_TIME_SCALE(1.0)
		CLEAR_TIMECYCLE_MODIFIER()
		
		CLEANUP_PC_CONTROLS()
		
		IF NOT bRestart
			SET_MAX_WANTED_LEVEL(6)
			SET_WANTED_LEVEL_MULTIPLIER(1.0)
			TERMINATE_THIS_THREAD()
		ENDIF
		
	//ENDIF
ENDPROC

PROC DISTANCE_FAIL_CHECKS(VECTOR vfailcheckcoords, FLOAT warningdist, FLOAT faildist,STRING sdistwarning,STRING scurrentobjective,MISSION_FAIL_ENUM efail = WADE_LEFT)
		
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),vfailcheckcoords) > faildist
		eFailedStage = eMissionStage
		reason_for_fail = efail
		eMissionStage = MISSION_STAGE_FAIL
	ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),vfailcheckcoords) > warningdist
		IF NOT bdistancewarning
			PRINTSTRING("sdistwarning")PRINTNL()
			PRINT_GOD_TEXT(sdistwarning)
			bdistancewarning = TRUE
		ENDIF
	ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),vfailcheckcoords) < (warningdist - 25)
		IF bdistancewarning
			PRINTSTRING("scurrentobjective")PRINTNL()
			PRINT_GOD_TEXT(scurrentobjective)
			bdistancewarning = FALSE
		ENDIF
	ENDIF

ENDPROC

INT iFlipTimer = -1
INT iTimeLastContact = 9999
BOOl bFlipTimerSetFork = FALSE
BOOL bContactTimerSet = FALSE

FUNC BOOL HAS_PLAYER_VEH_FLIPPED_VEH(VEHICLE_INDEX _player_veh, VEHICLE_INDEX _other_veh)
      BOOL bRet = FALSE
	  IF bFlipTimerSetFork = FALSE
	      IF IS_ENTITY_TOUCHING_ENTITY(_player_veh, _other_veh)
		  OR IS_ENTITY_AT_ENTITY(_player_veh, _other_veh,<<3,3,3>>)
            iTimeLastContact = GET_GAME_TIMER()
			bContactTimerSet = TRUE
	      ENDIF
	   ENDIF
      VECTOR vTestVec
      IF  bFlipTimerSet = FALSE
      AND bContactTimerSet
	  	IF NOT MANAGE_MY_TIMER(iTimeLastContact,4000)
	        vTestVec = NORMALISE_VECTOR(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(_other_veh, <<0,0,1>>) - GET_ENTITY_COORDS(_other_veh))
	        IF ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vTestVec.x, vTestVec.z, 0, 1)) > 80
	        OR ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vTestVec.y, vTestVec.z, 0, 1)) > 80
	              iFlipTimer = GET_GAME_TIMER()
				  bFlipTimerSetFork = TRUE
	        ENDIF
		ENDIF
      ELIF bFlipTimerSet = TRUE
        vTestVec = NORMALISE_VECTOR(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(_other_veh, <<0,0,1>>) - GET_ENTITY_COORDS(_other_veh))
        IF 	ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vTestVec.x, vTestVec.z, 0, 1)) < 45
        AND ABSF(GET_ANGLE_BETWEEN_2D_VECTORS(vTestVec.y, vTestVec.z, 0, 1)) < 45
			bFlipTimerSetFork = FALSE
            bContactTimerSet = FALSE
        ENDIF
      ENDIF
      IF MANAGE_MY_TIMER(iFlipTimer,4000)
      AND bFlipTimerSetFork = TRUE
            bRet = TRUE
      ENDIF
      RETURN bRet
ENDFUNC


BLIP_INDEX blipTrailer
INT iDialogueWarning
INT iConnectTrailerTimer

//PURPOSE: Tests for various fail conditions
PROC FAIL_CHECKS()

		
		IF IS_PED_INJURED(PLAYER_PED_ID())
			eFailedStage = eMissionStage
			eMissionStage = MISSION_STAGE_FAIL
		ENDIF
		
		IF eMissionStage <> MISSION_STAGE_PASSED
			IF DOES_ENTITY_EXIST(vehMission)
				//REMOVED THIS CHECK CAUSE OF PT
				IF NOT IS_VEHICLE_DRIVEABLE(vehMission)
					eFailedStage = eMissionStage
					reason_for_fail = VEHICLE_DEAD
					eMissionStage = MISSION_STAGE_FAIL
				ELSE
					IF IS_VEHICLE_PERMANENTLY_STUCK(vehMission)
						eFailedStage = eMissionStage
						reason_for_fail = VEHICLE_STUCK
						eMissionStage = MISSION_STAGE_FAIL
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedWade)
				IF DOES_ENTITY_EXIST(pedWade)
					IF eMissionStage <  MISSION_STAGE_4
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF NOT IS_PED_INJURED(pedWade)
								DISTANCE_FAIL_CHECKS(GET_ENTITY_COORDS(pedWade), 150, 200,"AW_DONT_LEAVE_W","AW_DISTWARNW",WADE_LEFT)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedWade) AND DOES_ENTITY_EXIST(pedFloyd)
				IF eMissionStage <  MISSION_STAGE_4
					IF IS_PED_INJURED(pedWade) AND IS_PED_INJURED(pedFloyd)
						eFailedStage = eMissionStage
						reason_for_fail = WADE_AND_FLOYD_DEAD
						eMissionStage = MISSION_STAGE_FAIL
					ELIF IS_PED_INJURED(pedWade)
						IF eMissionStage <  MISSION_STAGE_4
							eFailedStage = eMissionStage
							reason_for_fail = WADE_DEAD
							eMissionStage = MISSION_STAGE_FAIL
						ENDIF
					ELIF IS_PED_INJURED(pedFloyd)
						eFailedStage = eMissionStage
						reason_for_fail = FLOYD_DEAD
						eMissionStage = MISSION_STAGE_FAIL
					ENDIF
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(pedFloyd)
					IF IS_PED_INJURED(pedFloyd)
						eFailedStage = eMissionStage
						reason_for_fail = FLOYD_DEAD
						eMissionStage = MISSION_STAGE_FAIL
					ENDIF
				ENDIF
			ENDIF
		
			IF eMissionStage <  MISSION_STAGE_PASSED
				IF DOES_ENTITY_EXIST(pedFloyd)
					IF NOT IS_PED_INJURED(pedFloyd)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF eMissionStage =  MISSION_STAGE_1
								IF IS_VEHICLE_DRIVEABLE(vehMission)
									IF NOT IS_PED_IN_VEHICLE(pedFloyd,vehMission)
										DISTANCE_FAIL_CHECKS(GET_ENTITY_COORDS(pedFloyd), 190, 250,"AW_DONT_LEAVE_T","AW_DISTWARNF",FLOYD_LEFT)
									ENDIF
								ENDIF
							ELIF eMissionStage =  MISSION_STAGE_4
								DISTANCE_FAIL_CHECKS(GET_ENTITY_COORDS(pedFloyd), 190, 250,"AW_DONT_LEAVE_T","AW_DISTWARNF",FLOYD_LEFT)
							ELIF eMissionStage =  MISSION_STAGE_6
								DISTANCE_FAIL_CHECKS(<<-65.59, -2467.65, 5.96>>, 180, 250,"AW_DISTWARNL","AW_DISTWARND",ABADONED_DOCKS)
							ELIF eMissionStage =  MISSION_STAGE_7
								DISTANCE_FAIL_CHECKS(<<-65.59, -2467.65, 5.96>>, 180, 250,"AW_DISTWARNL","AW_DISTWARND",ABADONED_DOCKS)
							ELSE
								IF eMissionStage = MISSION_STAGE_2
									IF iProgress > 1
										DISTANCE_FAIL_CHECKS(<<-65.59, -2467.65, 5.96>>, 250, 270,"AW_DISTWARNL","AW_DISTWARND",ABADONED_DOCKS)
									ENDIF
								ENDIF
								IF eMissionStage >  MISSION_STAGE_2
								AND eMissionStage <  MISSION_STAGE_10
									IF eMissionStage !=  MISSION_STAGE_6
									OR eMissionStage !=  MISSION_STAGE_7
										DISTANCE_FAIL_CHECKS(<<-65.59, -2467.65, 5.96>>, 250, 270,"AW_DISTWARNL","AW_DISTWARND",ABADONED_DOCKS)
									ENDIF
								ENDIF
							ENDIF
							IF eMissionStage > MISSION_STAGE_2
							AND eMissionStage < MISSION_STAGE_4
								VEHICLE_INDEX vehPlayer
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
									IF IS_ENTITY_TOUCHING_ENTITY(vehPlayer,pedFloyd)
										IF IS_PED_RAGDOLL(pedFloyd)
											eFailedStage = eMissionStage
											reason_for_fail = COVER_BLOWN
											eMissionStage = MISSION_STAGE_FAIL
										ENDIF	
									ENDIF
									IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(pedFloyd)
										eFailedStage = eMissionStage
								   		reason_for_fail = COVER_BLOWN
										eMissionStage = MISSION_STAGE_FAIL
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF eMissionStage >  MISSION_STAGE_7 AND bDoneWithTruck = FALSE
				IF DOES_ENTITY_EXIST(vehFloydTruck)
					IF NOT IS_VEHICLE_DRIVEABLE(vehFloydTruck)
						eFailedStage = eMissionStage
						reason_for_fail = TRUCK_DEAD
						eMissionStage = MISSION_STAGE_FAIL
					ELSE
						IF IS_VEHICLE_PERMANENTLY_STUCK(vehFloydTruck)
							PRINTSTRING("WOULD HAVE FAILED")PRINTNL()
							eFailedStage = eMissionStage
							reason_for_fail = TRUCK_STUCK
							eMissionStage = MISSION_STAGE_FAIL
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehTrailer2)
					IF NOT IS_VEHICLE_DRIVEABLE(vehTrailer2)
						eFailedStage = eMissionStage
						reason_for_fail = DAMAGED_CARGO
						eMissionStage = MISSION_STAGE_FAIL
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
						IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(vehFloydTruck)
							IF IS_ENTITY_IN_WATER(vehTrailer2)
								eFailedStage = eMissionStage
								reason_for_fail = DAMAGED_CARGO
								eMissionStage = MISSION_STAGE_FAIL
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF eMissionStage =  MISSION_STAGE_10
				IF iProgress > 1
					IF DOES_ENTITY_EXIST(vehFloydTruck)
						IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
							
							IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(vehFloydTruck)
								IF NOT HAS_LABEL_BEEN_TRIGGERED("TRAILER DISCONNECTED")
									iConnectTrailerTimer = GET_GAME_TIMER()
									IF IS_VEHICLE_DRIVEABLE(vehTrailer2)
										IF NOT DOES_BLIP_EXIST(blipTrailer)
											blipTrailer = ADD_BLIP_FOR_ENTITY(vehTrailer2)
											SET_BLIP_COLOUR(blipTrailer,BLIP_COLOUR_BLUE)
										ENDIF
									ENDIF
									IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
											PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
										ENDIF
									ENDIF
									PRINT_GOD_TEXT("AW_RECONTRA")
									SET_LABEL_AS_TRIGGERED("TRAILER DISCONNECTED",TRUE)
								ELSE
									IF MANAGE_MY_TIMER(iConnectTrailerTimer,45000)
										PRINTSTRING("WOULD HAVE FAILED")PRINTNL()
										eFailedStage = eMissionStage
										reason_for_fail = DITCHED_THE_CARGO
										eMissionStage = MISSION_STAGE_FAIL
									ENDIF
								ENDIF
							ELSE	
								IF HAS_LABEL_BEEN_TRIGGERED("TRAILER DISCONNECTED")
									IF DOES_BLIP_EXIST(blipTrailer)
										REMOVE_BLIP(blipTrailer)
									ENDIF
									IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
										PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
									ENDIF
									IF IS_THIS_PRINT_BEING_DISPLAYED("AW_RECONTRA")
										CLEAR_PRINTS()
									ENDIF
									SET_LABEL_AS_TRIGGERED("TRAILER DISCONNECTED",FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF eMissionStage < MISSION_STAGE_6
				IF DOES_ENTITY_EXIST(vehHandler)
					IF NOT IS_VEHICLE_DRIVEABLE(vehHandler)
					OR IS_ENTITY_IN_WATER(vehHandler)
						eFailedStage = eMissionStage
						reason_for_fail = HANDLER_DEAD
						eMissionStage = MISSION_STAGE_FAIL
					ELSE
						IF IS_VEHICLE_PERMANENTLY_STUCK(vehHandler)
							eFailedStage = eMissionStage
							reason_for_fail = HANDLER_STUCK
							eMissionStage = MISSION_STAGE_FAIL
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Fail checks for containers: if placed on their side, dropped from a height, or slammed into another object, fail the mission.
			IF eMissionStage = MISSION_STAGE_7
				IF iCraneFailTimer = 0
					CONST_FLOAT SAFE_CONTAINER_ROT			32.0
					CONST_FLOAT SAFE_HELD_CONTAINER_ROT			45.0
					CONST_FLOAT SAFE_CONTAINER_ZROTDEV			15.0
					CONST_FLOAT SAFE_CONTAINER_VEL_CHANGE	5.0
					
					INT i = 0
					REPEAT COUNT_OF(s_Containers) i
						IF s_Containers[i].b_loaded_on_truck = FALSE
							IF DOES_ENTITY_EXIST(s_Containers[i].obj_main)
								VECTOR vContainerRot = GET_ENTITY_ROTATION(s_Containers[i].obj_main)
								VECTOR vContainerVel = GET_ENTITY_VELOCITY(s_Containers[i].obj_Main)
								
								PRINTSTRING("Container Number:")PRINTINT(i)PRINTSTRING("vContainerRot:")PRINTVECTOR(vContainerRot)PRINTNL()
								PRINTSTRING("Container Number:")PRINTINT(i)PRINTSTRING("vContainerVel:")PRINTVECTOR(vContainerVel)PRINTNL()
								
								IF NOT IS_ENTITY_ATTACHED(s_Containers[i].obj_main)
									IF vContainerRot.y > SAFE_HELD_CONTAINER_ROT OR vContainerRot.y < -SAFE_HELD_CONTAINER_ROT
										iCraneFailTimer = GET_GAME_TIMER()
									ENDIF
								ELSE
									IF vContainerRot.y > SAFE_CONTAINER_ROT OR vContainerRot.y < -SAFE_CONTAINER_ROT
										iCraneFailTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
								
								IF s_Containers[i].vPrevVel.z < -8.0
									IF vContainerVel.z - s_Containers[i].vPrevVel.z > SAFE_CONTAINER_VEL_CHANGE
										iCraneFailTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
								
								IF NOT IS_ENTITY_ATTACHED(s_Containers[i].obj_main)
									IF vContainerRot.z > 91.2705 + SAFE_CONTAINER_ZROTDEV OR vContainerRot.z < 91.2705 - SAFE_CONTAINER_ZROTDEV	
										iCraneFailTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
								
								IF NOT IS_ENTITY_ATTACHED(s_Containers[i].obj_main)
									IF NOT IS_ENTITY_IN_ANGLED_AREA(s_Containers[i].obj_main, <<-48.327007,-2415.719482,2.000961>>, <<-110.642082,-2416.208008,22.250463>>, 14.500000)
										iCraneFailTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
								
								//Supervisor shouts up if the player flings the containers around
								IF iCraneFailTimer = 0
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF ABSF(VMAG(vContainerVel) - VMAG(s_Containers[i].vPrevVel)) > 4.0
											KILL_FACE_TO_FACE_CONVERSATION()
											IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10h", CONV_PRIORITY_MEDIUM)
												iCraneDialogueTimer = GET_GAME_TIMER()
											ENDIF
										ENDIF
										
										IF (GET_GAME_TIMER() - iCraneDialogueTimer > 8000 AND ABSF(vContainerRot.y) > 45.0 AND IS_ENTITY_ATTACHED(s_Containers[i].obj_Main))
											KILL_FACE_TO_FACE_CONVERSATION()
											IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10h", CONV_PRIORITY_MEDIUM)
												iCraneDialogueTimer = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								s_Containers[i].vPrevVel = vContainerVel
							ENDIF
						ENDIF
					ENDREPEAT
				ELIF GET_GAME_TIMER() - iCraneFailTimer > 1000
					eFailedStage = eMissionStage
					reason_for_fail = DAMAGED_CARGO
					eMissionStage = MISSION_STAGE_FAIL
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(objHandlerContainer[0])
				IF IS_ENTITY_IN_WATER(objHandlerContainer[0])
					eFailedStage = eMissionStage
					reason_for_fail = DAMAGED_CRATES
					eMissionStage = MISSION_STAGE_FAIL
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(objHandlerContainer[1])
				IF IS_ENTITY_IN_WATER(objHandlerContainer[1])
					eFailedStage = eMissionStage
					reason_for_fail = DAMAGED_CRATES
					eMissionStage = MISSION_STAGE_FAIL
				ENDIF
			ENDIF
			

			IF (eMissionStage > MISSION_STAGE_1 OR (IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vDocks, << 20, 20, 20 >>) AND IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),0)))
			AND eMissionStage < MISSION_STAGE_11
				IF bIgnoreTheseFails = FALSE
					//player wanted level
					IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(),0)
						eFailedStage = eMissionStage
						reason_for_fail = COVER_BLOWN
						eMissionStage = MISSION_STAGE_FAIL
					ENDIF
					// start checking for attacking of people once player arrives at the docks
					//IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_PED(PLAYER_ID())
					IF HAS_PLAYER_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
						KILL_FACE_TO_FACE_CONVERSATION()
						IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
							//SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,rel_group_buddies)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,RELGROUPHASH_PLAYER)
							//SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_buddies,rel_group_enemies)
							eFailedStage = eMissionStage
							reason_for_fail = COVER_BLOWN
							eMissionStage = MISSION_STAGE_FAIL
						ENDIF
					ENDIF
					
					PED_INDEX pedArray[5]
					
					INT i = 0
        
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					    GET_PED_NEARBY_PEDS(PLAYER_PED_ID(), pedArray)
					    FOR i = 0 TO (COUNT_OF(pedArray) - 1)
							IF NOT IS_PED_INJURED(pedArray[i])
								IF NOT IS_PED_MODEL(pedArray[i],IG_WADE)
								AND NOT IS_PED_MODEL(pedArray[i],IG_FLOYD)
									IF IS_PED_MODEL(pedArray[i],S_M_Y_BLACKOPS_01)
										IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedArray[i],PLAYER_PED_ID())
										OR IS_PED_IN_COMBAT(pedArray[i],PLAYER_PED_ID())
										OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(),pedArray[i])
											//CHANGE RELATIONSHIP GROUPS
											SET_PED_RELATIONSHIP_GROUP_HASH(pedArray[i],rel_group_enemies)
											//SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,rel_group_enemies,rel_group_buddies)
											SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,rel_group_enemies,RELGROUPHASH_PLAYER)
											//SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,rel_group_buddies,rel_group_enemies)
											TASK_COMBAT_PED(pedArray[i],PLAYER_PED_ID())
											KILL_ANY_CONVERSATION()
											IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
												eFailedStage = eMissionStage
												reason_for_fail = COVER_BLOWN
												eMissionStage = MISSION_STAGE_FAIL
											ENDIF
										ENDIF
										IF HAS_PED_RECEIVED_EVENT(pedArray[i], EVENT_RAN_OVER_PED)
											KILL_ANY_CONVERSATION()
											IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
												eFailedStage = eMissionStage
												reason_for_fail = COVER_BLOWN_RAN_OVER
												eMissionStage = MISSION_STAGE_FAIL
											ENDIF
										ENDIF
									ELSE
										IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedArray[i],PLAYER_PED_ID())
										OR IS_PED_IN_COMBAT(pedArray[i],PLAYER_PED_ID())
											KILL_ANY_CONVERSATION()
											IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
												eFailedStage = eMissionStage
												reason_for_fail = COVER_BLOWN
												eMissionStage = MISSION_STAGE_FAIL
											ENDIF
										ENDIF
										IF HAS_PED_RECEIVED_EVENT(pedArray[i], EVENT_RAN_OVER_PED)
											KILL_ANY_CONVERSATION()
											IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
												eFailedStage = eMissionStage
												reason_for_fail = COVER_BLOWN_RAN_OVER
												eMissionStage = MISSION_STAGE_FAIL
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDFOR
					ENDIF
					
					FOR i = 0 TO (COUNT_OF(pedArray) - 1)
						IF DOES_ENTITY_EXIST(pedArray[i])
							IF NOT IS_PED_INJURED(pedArray[i])
								IF IS_PED_MODEL(pedArray[i],S_M_Y_BLACKOPS_01)
								OR IS_PED_MODEL(pedArray[i],S_M_Y_DOCKWORK_01)
								OR IS_PED_MODEL(pedArray[i],S_M_M_DOCKWORK_01)
									IF NOT IS_PED_MODEL(pedArray[i],IG_WADE)
									AND NOT IS_PED_MODEL(pedArray[i],IG_FLOYD)
										IF DOES_ENTITY_EXIST(objHandlerContainer[0])
										AND DOES_ENTITY_EXIST(objHandlerContainer[1])
											IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedArray[i],objHandlerContainer[0])
											OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedArray[i],objHandlerContainer[1])
												TASK_SMART_FLEE_PED(pedArray[i],PLAYER_PED_ID(),200,-1)
												eFailedStage = eMissionStage
												reason_for_fail = COVER_BLOWN
												eMissionStage = MISSION_STAGE_FAIL
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					
					// check for running over peds
					REPEAT COUNT_OF(pedDockWorker) i
						IF DOES_ENTITY_EXIST(pedDockWorker[i])
							IF NOT IS_PED_INJURED(pedDockWorker[i])
								IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
									IF HAS_PED_RECEIVED_EVENT(pedDockWorker[i], EVENT_POTENTIAL_GET_RUN_OVER) 
									OR HAS_PED_RECEIVED_EVENT(pedDockWorker[i], EVENT_RAN_OVER_PED) 
										IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_41")
											IF NOT IS_PED_INJURED(pedFloyd)
												KILL_ANY_CONVERSATION()
												IF DOES_ENTITY_EXIST(vehHandler)
													IF IS_VEHICLE_DRIVEABLE(vehHandler)
														IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
															PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFloyd,"DH1_EWAA","FLOYD",SPEECH_PARAMS_FORCE_FRONTEND)
														ELSE
															IF ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(),pedFloyd)
																PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFloyd,"DH1_EWAA","FLOYD",SPEECH_PARAMS_FORCE_FRONTEND)
															ELSE
																PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFloyd,"DH1_EWAA","FLOYD",SPEECH_PARAMS_FORCE_NORMAL)
															ENDIF
														ENDIF
													ELSE
														IF ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(),pedFloyd)
															PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFloyd,"DH1_EWAA","FLOYD",SPEECH_PARAMS_FORCE_FRONTEND)
														ELSE
															PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFloyd,"DH1_EWAA","FLOYD",SPEECH_PARAMS_FORCE_NORMAL)
														ENDIF
													ENDIF
												ELSE
													IF ARE_CHARS_SITTING_IN_SAME_VEHICLE(PLAYER_PED_ID(),pedFloyd)
														PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFloyd,"DH1_EWAA","FLOYD",SPEECH_PARAMS_FORCE_FRONTEND)
													ELSE
														PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFloyd,"DH1_EWAA","FLOYD",SPEECH_PARAMS_FORCE_NORMAL)
													ENDIF
												ENDIF
												SET_LABEL_AS_TRIGGERED("DS1_41",TRUE)
												iDialogueWarning = GET_GAME_TIMER()
											ENDIF
										ELSE
											IF MANAGE_MY_TIMER(iDialogueWarning,9000)
												SET_LABEL_AS_TRIGGERED("DS1_41",FALSE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF

								IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedDockWorker[i],PLAYER_PED_ID())
								OR (IS_PED_RAGDOLL(pedDockWorker[i]) AND IS_ENTITY_TOUCHING_ENTITY(pedDockWorker[i],PLAYER_PED_ID()))
									KILL_ANY_CONVERSATION()
									IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
										SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,rel_group_buddies)
										SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,RELGROUPHASH_PLAYER)
										SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_buddies,rel_group_enemies)
										eFailedStage = eMissionStage
										reason_for_fail = COVER_BLOWN
										eMissionStage = MISSION_STAGE_FAIL
									ENDIF
								ENDIF
							ELSE
								KILL_ANY_CONVERSATION()
								IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
									SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,rel_group_buddies)
									SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,RELGROUPHASH_PLAYER)
									SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_buddies,rel_group_enemies)
									eFailedStage = eMissionStage
									reason_for_fail = COVER_BLOWN
									eMissionStage = MISSION_STAGE_FAIL
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF NOT IS_PED_INJURED(pedSecurity)
						IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
							IF HAS_PED_RECEIVED_EVENT(pedSecurity, EVENT_POTENTIAL_GET_RUN_OVER) 
							OR HAS_PED_RECEIVED_EVENT(pedSecurity, EVENT_RAN_OVER_PED) 
								IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_41")
									IF NOT IS_PED_INJURED(pedFloyd)
										KILL_ANY_CONVERSATION()
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFloyd,"DH1_EWAA","FLOYD",SPEECH_PARAMS_FORCE_NORMAL)
										SET_LABEL_AS_TRIGGERED("DS1_41",TRUE)
										iDialogueWarning = GET_GAME_TIMER()
									ENDIF
								ELSE
									IF MANAGE_MY_TIMER(iDialogueWarning,9000)
										SET_LABEL_AS_TRIGGERED("DS1_41",FALSE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					
					IF IS_PED_SHOOTING(PLAYER_PED_ID())
						KILL_ANY_CONVERSATION()
						IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,rel_group_buddies)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,RELGROUPHASH_PLAYER)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_buddies,rel_group_enemies)
							eFailedStage = eMissionStage
							reason_for_fail = COVER_BLOWN
							eMissionStage = MISSION_STAGE_FAIL
						ENDIF
					ENDIF
					
					IF IS_PED_PERFORMING_STEALTH_KILL(PLAYER_PED_ID())
						KILL_ANY_CONVERSATION()
						IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,rel_group_buddies)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,RELGROUPHASH_PLAYER)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_buddies,rel_group_enemies)
							eFailedStage = eMissionStage
							reason_for_fail = COVER_BLOWN
							eMissionStage = MISSION_STAGE_FAIL
						ENDIF
					ENDIF
					
					IF IS_PED_IN_COMBAT(PLAYER_PED_ID())
						KILL_ANY_CONVERSATION()
						IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,rel_group_buddies)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,RELGROUPHASH_PLAYER)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_buddies,rel_group_enemies)
							eFailedStage = eMissionStage
							reason_for_fail = COVER_BLOWN
							eMissionStage = MISSION_STAGE_FAIL
						ENDIF
					ENDIF
					
					//Walked into office
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-63.843861,-2515.959229,6.150424>>, <<-51.076782,-2525.074463,9.171206>>, 4.250000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-63.257442,-2519.074707,8.900423>>, <<-62.217762,-2519.834961,6.150424>>, 0.500000)
						KILL_ANY_CONVERSATION()
						IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,rel_group_buddies)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,RELGROUPHASH_PLAYER)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_buddies,rel_group_enemies)
							eFailedStage = eMissionStage
							reason_for_fail = COVER_BLOWN
							eMissionStage = MISSION_STAGE_FAIL
						ENDIF
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-77.755180,-2364.332520,38.796139>>, <<-252.818283,-2363.691650,-2.910417>>, 35.750000)
						KILL_ANY_CONVERSATION()
						IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,rel_group_buddies)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,RELGROUPHASH_PLAYER)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_buddies,rel_group_enemies)
							eFailedStage = eMissionStage
							reason_for_fail = COVER_BLOWN
							eMissionStage = MISSION_STAGE_FAIL
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("MERRYWEATHER ALERTED")
						IF DOES_ENTITY_EXIST(pedMerc[4])
						AND DOES_ENTITY_EXIST(pedMerc[5])
						AND DOES_ENTITY_EXIST(pedMerc[5])
							IF NOT IS_PED_INJURED(pedMerc[4])
							AND NOT IS_PED_INJURED(pedMerc[5])
							AND NOT IS_PED_INJURED(pedMerc[6])
								IF IS_PED_IN_COMBAT(pedMerc[4])
								OR IS_PED_IN_COMBAT(pedMerc[5])
								OR IS_PED_IN_COMBAT(pedMerc[6])
									IF NOT HAS_SOUND_FINISHED(soundBeating)
										STOP_SOUND(soundBeating)
										RELEASE_SOUND_ID(soundBeating)
									ENDIF
								
									iMerryWeatherFail = GET_GAME_TIMER()
									SET_LABEL_AS_TRIGGERED("MERRYWEATHER ALERTED",TRUE)
								ENDIF
							ELSE
								IF eMissionStage = MISSION_STAGE_10
								OR (eMissionStage = MISSION_STAGE_11 AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<478.596, -3116.911, 5.069>>) < 40000.0)
									KILL_ANY_CONVERSATION()
									IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
										SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,rel_group_buddies)
										SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,RELGROUPHASH_PLAYER)
										SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_buddies,rel_group_enemies)
										eFailedStage = eMissionStage
										reason_for_fail = COVER_BLOWN
										eMissionStage = MISSION_STAGE_FAIL
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF MANAGE_MY_TIMER(iMerryWeatherFail,6000)
							KILL_ANY_CONVERSATION()
							IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,rel_group_buddies)
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,RELGROUPHASH_PLAYER)
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_buddies,rel_group_enemies)
								eFailedStage = eMissionStage
								reason_for_fail = COVER_BLOWN
								eMissionStage = MISSION_STAGE_FAIL
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF bIgnoreTheseFails = FALSE
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-63.600880,-2517.614014,6.400066>>, <<-61.070011,-2519.372559,8.900066>>, 1.750000)
					KILL_ANY_CONVERSATION()
					IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,rel_group_buddies)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,RELGROUPHASH_PLAYER)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_buddies,rel_group_enemies)
						eFailedStage = eMissionStage
						reason_for_fail = COVER_BLOWN
						eMissionStage = MISSION_STAGE_FAIL
					ENDIF
				ENDIF

				IF eMissionStage > MISSION_STAGE_1
					VEHICLE_INDEX vehTemp
					vehTemp = GET_RANDOM_VEHICLE_IN_SPHERE(<<-62.70, -2527.79, 5.01>>, 40, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES)
					IF vehTemp != vehMission
						IF DOES_ENTITY_EXIST(vehTemp)
							IF IS_ENTITY_IN_ANGLED_AREA(vehTemp, <<-52.457756,-2535.257813,4.010103>>, <<-73.385857,-2520.341309,8.510103>>, 14.250000)
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,rel_group_buddies)
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,RELGROUPHASH_PLAYER)
								SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_buddies,rel_group_enemies)
								eFailedStage = eMissionStage
								reason_for_fail = COVER_BLOWN
								eMissionStage = MISSION_STAGE_FAIL
							ENDIF
						ENDIF
					ENDIF
					
					INT iWorker
					FOR iWorker = 0 TO (iTotalDockWorker -1)
						IF DOES_ENTITY_EXIST(pedDockWorker[iWorker])
							IF NOT IS_PED_INJURED(pedDockWorker[iWorker])
								IF (IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedDockWorker[iWorker]) OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedDockWorker[iWorker]))
									KILL_ANY_CONVERSATION()
									IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
										TASK_SMART_FLEE_PED(pedDockWorker[iWorker],PLAYER_PED_ID(),200,-1)
										eFailedStage = eMissionStage
										reason_for_fail = COVER_BLOWN
										eMissionStage = MISSION_STAGE_FAIL
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					
					IF DOES_ENTITY_EXIST(pedSecurity)
						IF NOT IS_PED_INJURED(pedSecurity)
							IF (IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedSecurity) OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedSecurity))
								KILL_ANY_CONVERSATION()
								IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
									TASK_SMART_FLEE_PED(pedSecurity,PLAYER_PED_ID(),200,-1)
									eFailedStage = eMissionStage
									reason_for_fail = COVER_BLOWN
									eMissionStage = MISSION_STAGE_FAIL
								ENDIF
							ENDIF
							
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurity,PLAYER_PED_ID())
								IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
									SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,rel_group_buddies)
									SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,RELGROUPHASH_PLAYER)
									SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_buddies,rel_group_enemies)
									eFailedStage = eMissionStage
									reason_for_fail = COVER_BLOWN
									eMissionStage = MISSION_STAGE_FAIL
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					
					FOR iWorker = 0 TO (iTotalDockWorker -1)
						IF DOES_ENTITY_EXIST(pedDockWorker[iWorker])
							IF DOES_ENTITY_EXIST(objHandlerContainer[0])
							AND DOES_ENTITY_EXIST(objHandlerContainer[1])
								IF NOT IS_PED_INJURED(pedDockWorker[iWorker])
									IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedDockWorker[iWorker],objHandlerContainer[0])
									OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedDockWorker[iWorker],objHandlerContainer[1])
										TASK_SMART_FLEE_PED(pedDockWorker[iWorker],PLAYER_PED_ID(),200,-1)
										eFailedStage = eMissionStage
										reason_for_fail = COVER_BLOWN
										eMissionStage = MISSION_STAGE_FAIL
									ENDIF
								ELSE
									eFailedStage = eMissionStage
									reason_for_fail = COVER_BLOWN
									eMissionStage = MISSION_STAGE_FAIL
								ENDIF
							ENDIF
						ENDIF
					ENDFOR
					
				    
				ENDIF
				
			ENDIF
			
			IF eMissionStage = MISSION_STAGE_5
				IF iHandlerFailTimer = 0
					CONST_FLOAT SAFE_HANDLER_CONTAINER_ROT			75.0
					CONST_FLOAT SAFE_HANDLER_CONTAINER_ZROTDEV	    35.0
					VECTOR vHandlerContainerTemp
					
					INT i = 0
					REPEAT COUNT_OF(objHandlerContainer) i
						IF DOES_ENTITY_EXIST(objHandlerContainer[i])
							IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
								IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
									IF NOT IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[i], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.750000)
										IF DOES_ENTITY_EXIST(objHandlerContainer[i])
											VECTOR vHandlerContainerRot = GET_ENTITY_ROTATION(objHandlerContainer[i])
											
											IF vHandlerContainerRot.y > SAFE_HANDLER_CONTAINER_ROT OR vHandlerContainerRot.y < -SAFE_HANDLER_CONTAINER_ROT
												iHandlerFailTimer = GET_GAME_TIMER()
												PRINTSTRING("@@@@@@@@@ Fail for Y: @@@@@@@@@")PRINTNL()
											ENDIF
											
											IF NOT IS_ENTITY_ATTACHED(objHandlerContainer[i])
												// 91.2705 + | 91.2705 -
												IF vHandlerContainerRot.z > 55.003 + SAFE_HANDLER_CONTAINER_ZROTDEV OR vHandlerContainerRot.z < 55.003 - SAFE_HANDLER_CONTAINER_ZROTDEV	
													iHandlerFailTimer = GET_GAME_TIMER()
													PRINTSTRING("@@@@@@@@@ Fail for Z: @@@@@@@@@")PRINTNL()
												ENDIF
											ENDIF
											//PRINTSTRING("vHandlerContainerRot:")PRINTVECTOR(vHandlerContainerRot)PRINTNL()
										ENDIF
										
										IF DOES_ENTITY_EXIST(objHandlerContainer[i])
											vHandlerContainerTemp = GET_ENTITY_COORDS(objHandlerContainer[i])
											IF vHandlerContainerTemp. z > 10.6
												iHandlerFailTimer = GET_GAME_TIMER()
												PRINTSTRING("@@@@@@@@@ Fail for Z out of reach: @@@@@@@@@")PRINTNL()
											ENDIF
										ENDIF
										
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ELIF GET_GAME_TIMER() - iHandlerFailTimer > 2000
					
					eFailedStage = eMissionStage
					reason_for_fail = DAMAGED_HANDLER_CARGO
					eMissionStage = MISSION_STAGE_FAIL
				ENDIF
			ENDIF
			
		ENDIF
		
		
ENDPROC
//PURPOSE: controls the failure of the mission
PROC MISSION_FAILED()
	
	TRIGGER_MUSIC_EVENT("AHP1_FAIL") 

	
	STRING sFailString
	SWITCH reason_for_fail
		CASE GENERIC
			//FAIL_GENERIC
			CLEAR_PRINTS()
			sFailString = "AW_FAILED"
			//PRINT_GOD_TEXT("AW_FAILED")
		BREAK
		
		CASE FLOYD_LEFT
			//FAIL_GENERIC
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_FL"
			//PRINT_GOD_TEXT("AW_FAILED_FL")
		BREAK
		
		CASE FLOYD_DEAD
			//FAIL_GENERIC
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_FD"
			//PRINT_GOD_TEXT("AW_FAILED_FD")
		BREAK
		
		CASE WADE_LEFT
			//FAIL_GENERIC
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_WL"
			//PRINT_GOD_TEXT("AW_FAILED_WL")
		BREAK
		
		CASE WADE_DEAD
			//FAIL_GENERIC
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_WD"
			//PRINT_GOD_TEXT("AW_FAILED_WD")
		BREAK
		
		CASE FLOYDS_FORKLIFT_DEAD
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_FFLD"
			//PRINT_GOD_TEXT("AW_FAILED_FLD")
		BREAK
		
		CASE FORKLIFT_DEAD
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_FLD"
			//PRINT_GOD_TEXT("AW_FAILED_FLD")
		BREAK
		
		CASE VEHICLE_DEAD
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_TTD"
			//PRINT_GOD_TEXT("AW_FAILED_TTD")
		BREAK
		
		CASE VEHICLE_STUCK
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_TTS"
			//PRINT_GOD_TEXT("AW_FAILED_TTD")
		BREAK
		
		CASE TRUCK_DEAD
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_TRD"
			//PRINT_GOD_TEXT("AW_FAILED_TRD")
		BREAK
		
		CASE TRUCK_STUCK
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_TRS"
			//PRINT_GOD_TEXT("AW_FAILED_TRD")
		BREAK
		
		CASE COVER_BLOWN
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_CBL"
			//PRINT_GOD_TEXT("AW_FAILED_CBL")
		BREAK
		
		CASE COVER_BLOWN_RAN_OVER
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_CBL"
			//PRINT_GOD_TEXT("AW_FAILED_CBL")
		BREAK
		
		CASE DAMAGED_CARGO
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_DC"
			//PRINT_GOD_TEXT("AW_FAILED_CBL")
		BREAK
		
		CASE DAMAGED_CRATES
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_DC"
			//PRINT_GOD_TEXT("AW_FAILED_CBL")
		BREAK
		
		CASE HANDLER_DEAD
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_HD"
			//PRINT_GOD_TEXT("AW_FAILED_CBL")
		BREAK
		
		CASE HANDLER_STUCK
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_HS"
			//PRINT_GOD_TEXT("AW_FAILED_TRD")
		BREAK
		
		CASE DAMAGED_HANDLER_CARGO
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_HCD"
		BREAK
		
		CASE WADE_AND_FLOYD_DEAD
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_FWD"
			//PRINT_GOD_TEXT("AW_FAILED_CBL")
		BREAK
		
		CASE ABADONED_DOCKS
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_AD"
			//PRINT_GOD_TEXT("AW_FAILED_CBL")
		BREAK
		
		CASE DITCHED_THE_CARGO
			CLEAR_PRINTS()
			sFailString = "AW_FAILED_DTC"
			//PRINT_GOD_TEXT("AW_FAILED_CBL")
		BREAK
		
		#IF IS_DEBUG_BUILD
			CASE FORCE_FAIL
				//FAIL_GENERIC
				CLEAR_PRINTS()
				PRINT_GOD_TEXT("AW_FAILED")
			BREAK
		#ENDIF
	ENDSWITCH
	
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(sFailString)
	
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_PRIME_BOARD_TRANSITION, FALSE)
	IF GET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_PICTURE) != BUILDINGSTATE_NORMAL
		SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_PICTURE, BUILDINGSTATE_NORMAL)
	ENDIF
	IF GET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT) != BUILDINGSTATE_NORMAL
		SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT, BUILDINGSTATE_NORMAL)
	ENDIF
	IF GET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT_SOFA) != BUILDINGSTATE_NORMAL
		SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT_SOFA, BUILDINGSTATE_NORMAL)
	ENDIF
//	IF GET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM) != BUILDINGSTATE_NORMAL
//		SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM, BUILDINGSTATE_NORMAL)
//	ENDIF
//	IF GET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM_CUTSCENE) != BUILDINGSTATE_DESTROYED
//		SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM_CUTSCENE, BUILDINGSTATE_DESTROYED)
//	ENDIF
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		IF bAddedRopes
			INT i = 0
			REPEAT 8 i
				STOP_ROPE_UNWINDING_FRONT(s_Crane.ropes[i])
				STOP_ROPE_WINDING(s_Crane.ropes[i])
			ENDREPEAT
		ENDIF
		WAIT(0)
	ENDWHILE
	
	RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR, bHasChanged)
	RESTORE_MISSION_START_OUTFIT()
	
	// Call RemovePlayerFromRestrictedVehicle() here if you need to
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	
	IF eFailedStage > MISSION_STAGE_1
		VEHICLE_INDEX vehPlayer
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		ENDIF
		
		IF DOES_ENTITY_EXIST(vehPlayer)
			IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				IF IS_ENTITY_A_MISSION_ENTITY(vehPlayer)
				    IF NOT IS_ENTITY_DEAD(vehPlayer) 
						IF IS_VEHICLE_MODEL(vehPlayer, HANDLER)
						OR IS_VEHICLE_MODEL(vehPlayer, HAULER)
						OR IS_VEHICLE_MODEL(vehPlayer, FORKLIFT)
						OR IS_VEHICLE_MODEL(vehPlayer, BARRACKS)
						OR IS_VEHICLE_MODEL(vehPlayer, BARRACKS2)
							//RemovePlayerFromRestrictedVehicle(vehPlayer)	
						ENDIF
						IF GET_ENTITY_MODEL(vehPlayer) = GET_PLAYER_VEH_MODEL(CHAR_TREVOR)
							SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<27.8582, -2553.9465, 5.0001>>,263.0292)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<20.1336, -2536.1494, 5.0502>>,232.2569)
		
		IF DOES_ENTITY_EXIST(vehPlayer)
			IF IS_VEHICLE_DRIVEABLE(vehPlayer)
				IF IS_VEHICLE_MODEL(vehPlayer, GET_PLAYER_VEH_MODEL(CHAR_TREVOR))
					IF NOT IS_ENTITY_DEAD(vehPlayer) 
						IF IS_VEHICLE_SEAT_FREE(vehPlayer,VS_DRIVER)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehPlayer)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	

	MISSION_CLEANUP() // must only take 1 frame and terminate the thread
ENDPROC
// ======================================================================
//                                 Misc Procs 
// ======================================================================
//
//FUNC PED_INDEX PLAYER_PED(SELECTOR_SLOTS_ENUM sSelectorSlots)
//	PED_INDEX pedIndex
//	
//	IF sSelectorPeds.eCurrentSelectorPed = sSelectorSlots
//		pedIndex = PLAYER_PED_ID()
//	ELSE
//		pedIndex = sSelectorPeds.pedID[sSelectorSlots]
//	ENDIF
//	
//	RETURN pedIndex
//ENDFUNC
// all player peds block non-temporary events
PROC SET_PLAYER_PEDS_BLOCK_NON_TEMPORARY_EVENTS()
	INT i 
	REPEAT COUNT_OF(sSelectorPeds.pedID) i 
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[i])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[i], TRUE)
		ENDIF
	ENDREPEAT
ENDPROC

PROC INHIBIT_ALL_CONTROLS_EXCEPT_THIS(CONTROL_ACTION eAction)
	IF eAction = INPUT_FRONTEND_ACCEPT
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y)
	ENDIF
	IF eAction = INPUT_FRONTEND_RIGHT_AXIS_Y
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	ENDIF
	IF eAction = INPUT_FRONTEND_RIGHT_AXIS_X
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	ENDIF
	IF eAction = INPUT_FRONTEND_AXIS_X
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	ENDIF
	IF eAction = INPUT_FRONTEND_AXIS_Y
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	ENDIF
	//SPECIAL CASE
	IF eAction = INPUT_FRONTEND_X
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X)
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
	ENDIF
ENDPROC

PROC INHIBIT_LOWERING_SPREADER()

	IF NOT IS_ENTITY_DEAD(s_containers[0].obj_main)
	AND NOT IS_ENTITY_DEAD(vehTrailer1)
//		IF IS_ENTITY_TOUCHING_ENTITY(s_containers[0].obj_main,vehTrailer1)
//			PRINTLN("@@@@@@@@@@ IS_ENTITY_TOUCHING_ENTITY(s_containers[0].obj_main,vehTrailer1) @@@@@@@@@@@@")
			IF(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < 0)
				PRINTLN("@@@@@@@@@@ INHIBIT_CRANE @@@@@@@@@@@@")
				INHIBIT_CRANE(CRANE_ACTION_SPREADER)
			ENDIF
//		ENDIF
	ENDIF

	IF NOT IS_ENTITY_DEAD(s_containers[0].obj_main)
	AND NOT IS_ENTITY_DEAD(vehTrailer2)
//		IF IS_ENTITY_TOUCHING_ENTITY(s_containers[0].obj_main,vehTrailer2)
//			PRINTLN("@@@@@@@@@@ IS_ENTITY_TOUCHING_ENTITY(s_containers[0].obj_main,vehTrailer1) @@@@@@@@@@@@")
			IF(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < 0)
				PRINTLN("@@@@@@@@@@ INHIBIT_CRANE @@@@@@@@@@@@")
				INHIBIT_CRANE(CRANE_ACTION_SPREADER)
			ENDIF
//		ENDIF
	ENDIF

	IF NOT IS_ENTITY_DEAD(s_containers[1].obj_main)
	AND NOT IS_ENTITY_DEAD(vehTrailer1)
//		IF IS_ENTITY_TOUCHING_ENTITY(s_containers[1].obj_main,vehTrailer1)
//			PRINTLN("@@@@@@@@@@ IS_ENTITY_TOUCHING_ENTITY(s_containers[0].obj_main,vehTrailer1) @@@@@@@@@@@@")
			IF(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < 0)
				PRINTLN("@@@@@@@@@@ INHIBIT_CRANE @@@@@@@@@@@@")
				INHIBIT_CRANE(CRANE_ACTION_SPREADER)
			ENDIF
//		ENDIF
	ENDIF

	IF NOT IS_ENTITY_DEAD(s_containers[1].obj_main)
	AND NOT IS_ENTITY_DEAD(vehTrailer2)
//		IF IS_ENTITY_TOUCHING_ENTITY(s_containers[1].obj_main,vehTrailer2)
//			PRINTLN("@@@@@@@@@@ IS_ENTITY_TOUCHING_ENTITY(s_containers[0].obj_main,vehTrailer1) @@@@@@@@@@@@")
			IF(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < 0)
				PRINTLN("@@@@@@@@@@ INHIBIT_CRANE @@@@@@@@@@@@")
				INHIBIT_CRANE(CRANE_ACTION_SPREADER)
			ENDIF
//		ENDIF
	ENDIF
	
ENDPROC

PROC INHIBIT_LOWERING_SPREADER_IF_TOUCHING()

	IF NOT IS_ENTITY_DEAD(s_containers[0].obj_main)
	AND NOT IS_ENTITY_DEAD(vehTrailer1)
		IF IS_ENTITY_TOUCHING_ENTITY(s_containers[0].obj_main,vehTrailer1)
			PRINTLN("@@@@@@@@@@ IS_ENTITY_TOUCHING_ENTITY(s_containers[0].obj_main,vehTrailer1) @@@@@@@@@@@@")
			IF(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < 0)
				PRINTLN("@@@@@@@@@@ INHIBIT_CRANE @@@@@@@@@@@@")
				INHIBIT_CRANE(CRANE_ACTION_SPREADER)
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_ENTITY_DEAD(s_containers[0].obj_main)
	AND NOT IS_ENTITY_DEAD(vehTrailer2)
		IF IS_ENTITY_TOUCHING_ENTITY(s_containers[0].obj_main,vehTrailer2)
			PRINTLN("@@@@@@@@@@ IS_ENTITY_TOUCHING_ENTITY(s_containers[0].obj_main,vehTrailer1) @@@@@@@@@@@@")
			IF(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < 0)
				PRINTLN("@@@@@@@@@@ INHIBIT_CRANE @@@@@@@@@@@@")
				INHIBIT_CRANE(CRANE_ACTION_SPREADER)
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_ENTITY_DEAD(s_containers[1].obj_main)
	AND NOT IS_ENTITY_DEAD(vehTrailer1)
		IF IS_ENTITY_TOUCHING_ENTITY(s_containers[1].obj_main,vehTrailer1)
			PRINTLN("@@@@@@@@@@ IS_ENTITY_TOUCHING_ENTITY(s_containers[0].obj_main,vehTrailer1) @@@@@@@@@@@@")
			IF(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < 0)
				PRINTLN("@@@@@@@@@@ INHIBIT_CRANE @@@@@@@@@@@@")
				INHIBIT_CRANE(CRANE_ACTION_SPREADER)
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_ENTITY_DEAD(s_containers[1].obj_main)
	AND NOT IS_ENTITY_DEAD(vehTrailer2)
		IF IS_ENTITY_TOUCHING_ENTITY(s_containers[1].obj_main,vehTrailer2)
			PRINTLN("@@@@@@@@@@ IS_ENTITY_TOUCHING_ENTITY(s_containers[0].obj_main,vehTrailer1) @@@@@@@@@@@@")
			IF(GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_SCRIPT_LEFT_AXIS_Y) < 0)
				PRINTLN("@@@@@@@@@@ INHIBIT_CRANE @@@@@@@@@@@@")
				INHIBIT_CRANE(CRANE_ACTION_SPREADER)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC MANAGE_CRANE_HELP()

VECTOR v_cabin_pos
VECTOR v_container_pos_1
VECTOR v_container_pos_2
VECTOR v_spreader_pos
VECTOR vTemp
INT i
//FLOAT fTemp

SET_GAMEPLAY_CAM_RELATIVE_HEADING(fThisValue)
SET_GAMEPLAY_CAM_RELATIVE_HEADING(fThisValue)

INHIBIT_LOWERING_SPREADER_IF_TOUCHING()

//Full help text
IF iCraneHelp > 8
	IF DOES_ENTITY_EXIST(s_crane.obj_spreader)
		v_spreader_pos = GET_ENTITY_COORDS(s_crane.obj_spreader)
	ENDIF
	
	VECTOR vContainer1Pos
	VECTOR vContainer2Pos
	
	IF DOES_ENTITY_EXIST(s_containers[0].obj_main)
		vContainer1Pos = GET_ENTITY_COORDS(s_containers[0].obj_main,FALSE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(s_containers[1].obj_main)
		vContainer2Pos = GET_ENTITY_COORDS(s_containers[1].obj_main,FALSE)
	ENDIF

	IF CAN_CRANE_PICK_UP_CONTAINER(vContainer1Pos, v_spreader_pos)
	OR CAN_CRANE_PICK_UP_CONTAINER(vContainer2Pos, v_spreader_pos)
		IF s_crane.b_container_attached = FALSE
			IF iCraneHelp < 15
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_PICKUC")
					PRINT_HELP_FOREVER("CRH_PICKUC")
				ENDIF
			ENDIF
		ELSE
			IF b_is_crane_cinematic_active
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_H3A")
					PRINT_HELP_FOREVER("CRH_H3A")
				ENDIF
			ELSE
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_H3B")
					PRINT_HELP_FOREVER("CRH_H3B")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF s_crane.b_container_attached = TRUE
			IF DOES_ENTITY_EXIST(s_containers[1].obj_main)
				IF NOT IS_ENTITY_DEAD(s_containers[1].obj_main)
				AND NOT IS_ENTITY_DEAD(vehTrailer2)
					IF IS_ENTITY_AT_ENTITY(s_containers[1].obj_main,vehTrailer2,<<1.5,1,1>>)
						INHIBIT_LOWERING_SPREADER()
						IF iCraneHelp < 15
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_RELC")
								PRINT_HELP_FOREVER("CRH_RELC")
							ENDIF
						ENDIF
					ELSE
						IF b_is_crane_cinematic_active
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_H3A")
								PRINT_HELP_FOREVER("CRH_H3A")
							ENDIF
						ELSE
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_H3B")
								PRINT_HELP_FOREVER("CRH_H3B")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(s_containers[0].obj_main)
					IF NOT IS_ENTITY_DEAD(s_containers[0].obj_main)
					AND NOT IS_ENTITY_DEAD(vehTrailer2)
						IF IS_ENTITY_AT_ENTITY(s_containers[0].obj_main,vehTrailer2,<<1.5,1,1>>)
							INHIBIT_LOWERING_SPREADER()
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_RELC")
								PRINT_HELP_FOREVER("CRH_RELC")
							ENDIF
						ELSE
							IF b_is_crane_cinematic_active
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_H3A")
									PRINT_HELP_FOREVER("CRH_H3A")
								ENDIF
							ELSE
								IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_H3B")
									PRINT_HELP_FOREVER("CRH_H3B")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF b_is_crane_cinematic_active
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_H3A")
					PRINT_HELP_FOREVER("CRH_H3A")
				ENDIF
			ELSE
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_H3B")
					PRINT_HELP_FOREVER("CRH_H3B")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	

ENDIF

//Use ~INPUT_SCRIPT_RIGHT_AXIS_Y~ to move crane.~n~
//Use ~INPUT_SCRIPT_RIGHT_AXIS_X~ to move cabin.~n~
//Use ~INPUT_SCRIPT_LEFT_AXIS_Y~ to move spreader.~s~
IF iCraneHelp = 5
	IF b_is_crane_cinematic_active
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_HELP2A")
			PRINT_HELP_FOREVER("CRH_HELP2A")
		ENDIF
	ELSE
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_HELP2B")
			PRINT_HELP_FOREVER("CRH_HELP2B")
		ENDIF
	ENDIF
ENDIF

//~s~Use ~PAD_LSTICK_LEFTRIGHT~ to move the cabin.~n~
//~s~Use ~PAD_RSTICK_UPDOWN~ to move the spreader.~s~
IF iCraneHelp = 6
	IF ABSF(v_spreader_pos.x - vTemp.x) < 5.0 
	
	ENDIF
	IF b_is_crane_cinematic_active
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_HELP3A")
			PRINT_HELP_FOREVER("CRH_HELP3A")
		ENDIF
	ELSE
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_HELP3B")
			PRINT_HELP_FOREVER("CRH_HELP3B")
		ENDIF
	ENDIF
ENDIF

//~s~Use ~PAD_LSTICK_LEFTRIGHT~ to move the crane.~s~
IF iCraneHelp = 1
	IF b_is_crane_cinematic_active
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_H1A")
			PRINT_HELP_FOREVER("CRH_H1A")
		ENDIF
	ELSE
		IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_H1B")
			PRINT_HELP_FOREVER("CRH_H1B")
		ENDIF
	ENDIF
ENDIF

IF iCraneHelp > 8
	IF IS_VEHICLE_DRIVEABLE(vehFirstHauler)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehFirstHauler)
			//PRINTSTRING("Trailer Position:")PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(vehFirstHauler))PRINTNL() 
			IF GET_TIME_POSITION_IN_RECORDING(vehFirstHauler) > 9757.000000
				IF DOES_ENTITY_EXIST(vehTrailer1)
					DELETE_VEHICLE(vehTrailer1)
				ENDIF
				IF DOES_ENTITY_EXIST(vehFirstHauler)
					DELETE_VEHICLE(vehFirstHauler)
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
ENDIF

//BLIPS
IF iCraneHelp > 10 AND iCraneHelp < 15
	IF s_crane.b_container_attached
		
		IF IS_VEHICLE_DRIVEABLE(vehTrailer2)
			IF NOT DOES_BLIP_EXIST(dest_blip)
				dest_blip = CREATE_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehTrailer2))
				SET_ENTITY_PROOFS(vehTrailer2,TRUE,TRUE,TRUE,TRUE,TRUE)
				SET_BLIP_COLOUR(dest_blip,BLIP_COLOUR_BLUE)
			ENDIF
		ENDIF
		
		IF DOES_BLIP_EXIST(container0_blip)
			REMOVE_BLIP(container0_blip)
		ENDIF
		
		IF DOES_BLIP_EXIST(container1_blip)
			REMOVE_BLIP(container1_blip)
		ENDIF
	ELSE
		IF DOES_BLIP_EXIST(dest_blip)
			REMOVE_BLIP(dest_blip)
		ENDIF
				
		IF bCrate0Attached = FALSE
			IF DOES_ENTITY_EXIST(s_containers[0].obj_main)
				IF NOT DOES_BLIP_EXIST(container0_blip)
				AND NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(s_containers[0].obj_main))
					container0_blip = CREATE_BLIP_FOR_ENTITY(s_containers[0].obj_main)
					SET_BLIP_COLOUR(container0_blip,BLIP_COLOUR_GREEN)
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(container0_blip)
				REMOVE_BLIP(container0_blip)
			ENDIF
		ENDIF
		
		IF bCrate1Attached = FALSE
			IF DOES_ENTITY_EXIST(s_containers[1].obj_main)
				IF NOT DOES_BLIP_EXIST(container1_blip)
				AND NOT DOES_BLIP_EXIST(GET_BLIP_FROM_ENTITY(s_containers[1].obj_main))
					container1_blip = CREATE_BLIP_FOR_ENTITY(s_containers[1].obj_main)
					SET_BLIP_COLOUR(container1_blip,BLIP_COLOUR_GREEN)
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(container1_blip)
				REMOVE_BLIP(container1_blip)
			ENDIF
		ENDIF
	ENDIF
ELSE
	IF iCraneHelp > 0
		
		IF s_crane.b_container_attached
			
			IF DOES_BLIP_EXIST(container0_blip)
				REMOVE_BLIP(container0_blip)
			ENDIF
			
			IF DOES_BLIP_EXIST(container1_blip)
				REMOVE_BLIP(container1_blip)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTrailer1)
				IF NOT DOES_BLIP_EXIST(dest_blip)
					dest_blip = CREATE_BLIP_FOR_COORD(GET_ENTITY_COORDS(vehTrailer1))
					SET_BLIP_COLOUR(dest_blip,BLIP_COLOUR_BLUE)
				ENDIF
			ENDIF
		ELSE
			
			IF DOES_BLIP_EXIST(dest_blip)
				REMOVE_BLIP(dest_blip)
			ENDIF
			
			IF bCrate0Attached = FALSE
				IF NOT DOES_BLIP_EXIST(container0_blip)
					IF DOES_ENTITY_EXIST(s_containers[0].obj_main)
						container0_blip = CREATE_BLIP_FOR_ENTITY(s_containers[0].obj_main)
						SET_BLIP_COLOUR(container0_blip,BLIP_COLOUR_GREEN)
					ENDIF
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(container0_blip)
					REMOVE_BLIP(container0_blip)
				ENDIF
			ENDIF
			
			IF bCrate1Attached = FALSE
				IF NOT DOES_BLIP_EXIST(container1_blip)
					IF DOES_ENTITY_EXIST(s_containers[1].obj_main)
						container1_blip = CREATE_BLIP_FOR_ENTITY(s_containers[1].obj_main)
						SET_BLIP_COLOUR(container1_blip,BLIP_COLOUR_GREEN)
					ENDIF
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(container1_blip)
					REMOVE_BLIP(container1_blip)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDIF

//HIDE_HUD_AND_RADAR_THIS_FRAME()

#IF IS_DEBUG_BUILD // debug controls 
	//PRINTSTRING("iCraneHelp:")PRINTINT(iCraneHelp)PRINTNL()
	//PRINTSTRING("s_crane.b_container_attached:")PRINTBOOL(s_crane.b_container_attached)PRINTNL()
	//PRINTSTRING("Cabin Position:")PRINTVECTOR(GET_ENTITY_COORDS(s_crane.obj_cabin))PRINTNL()
#ENDIF

//LIGHTS
IF HAS_LABEL_BEEN_TRIGGERED("DS1_10a")
	IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10a2")
		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10a2", CONV_PRIORITY_MEDIUM)
					SET_LABEL_AS_TRIGGERED("DS1_10a2",TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDIF

SWITCH iCraneHelp

	CASE 0
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("DOCKS_HEIST_USING_CRANE")
			START_AUDIO_SCENE("DOCKS_HEIST_USING_CRANE")
		ENDIF
		
		REQUEST_ANIM_DICT("missheistdockssetup1trevor_crane")
		
		INHIBIT_CRANE(CRANE_ACTION_CABIN)
		INHIBIT_CRANE(CRANE_ACTION_SPREADER)
		INHIBIT_CRANE(CRANE_ACTION_CRANE)
		INHIBIT_CRANE(CRANE_ACTION_GRABBER)
		IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10")
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10", CONV_PRIORITY_MEDIUM)
					SET_LABEL_AS_TRIGGERED("DS1_10",TRUE)
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
				PRINT_GOD_TEXT("AW_GRAB_CR")
				container0_blip = CREATE_BLIP_FOR_ENTITY(s_containers[0].obj_main)
				container1_blip = CREATE_BLIP_FOR_ENTITY(s_containers[1].obj_main)
				SET_BLIP_COLOUR(container0_blip,BLIP_COLOUR_GREEN)
				SET_BLIP_COLOUR(container1_blip,BLIP_COLOUR_GREEN)
				
				//~s~Use ~PAD_LSTICK_UPDOWN~ to move the cabin.~n~
				//Press ~PAD_BACK~ to change view.~s~	
				IF b_is_crane_cinematic_active
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_H1A")
						PRINT_HELP_FOREVER("CRH_H1A")
					ENDIF
				ELSE
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_H1B")
						PRINT_HELP_FOREVER("CRH_H1B")
					ENDIF
				ENDIF
				SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
				HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
				SETTIMERA(0)
				bForceNoGrab = FALSE
				iCraneHelpTimer = GET_GAME_TIMER()
				vInitialCabinPosition = GET_ENTITY_COORDS(s_crane.obj_cabin, FALSE)
				iCraneHelp++
			ENDIF
		ENDIF
	BREAK
	
	CASE 1
		
		IF MANAGE_MY_TIMER(iCraneHelpTimer,200)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10a")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10a", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("DS1_10a",TRUE)
					ENDIF
				ENDIF
			ELSE
				
			ENDIF
			
			v_cabin_pos = GET_ENTITY_COORDS(s_crane.obj_cabin, FALSE)
			v_container_pos_1 = GET_ENTITY_COORDS(s_containers[0].obj_main, FALSE)
			v_container_pos_2 = GET_ENTITY_COORDS(s_containers[1].obj_main, FALSE)
			
			IF VDIST(vInitialCabinPosition,v_cabin_pos) > 1.0
			OR ABSF(v_cabin_pos.x - v_container_pos_1.x) < 1.0
			OR ABSF(v_cabin_pos.x - v_container_pos_2.x) < 1.0
			ENDIF
			
			IF DOES_ENTITY_EXIST(s_crane.obj_spreader)
				v_spreader_pos = GET_ENTITY_COORDS(s_crane.obj_spreader)
			ENDIF
			
			IF CAN_CRANE_PICK_UP_CONTAINER(GET_ENTITY_COORDS(s_containers[0].obj_main, FALSE), v_spreader_pos)
			OR CAN_CRANE_PICK_UP_CONTAINER(GET_ENTITY_COORDS(s_containers[1].obj_main, FALSE), v_spreader_pos)
				CLEAR_HELP()
				//~s~Use ~PAD_RSTICK_UPDOWN~ to move the spreader.~s~
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_SPR")
					PRINT_HELP_FOREVER("CRH_SPR")
				ENDIF
	
				iCraneHelpTimer = GET_GAME_TIMER()
				vInitialCabinPosition = GET_ENTITY_COORDS(s_crane.obj_cabin, FALSE)
				INHIBIT_CRANE(CRANE_ACTION_GRABBER)
				iCraneHelp++
			ENDIF
			
			//IF ABSF(v_cabin_pos.y - -2418.09) < 0.01
			IF v_cabin_pos.y < -2418.09
			OR v_cabin_pos.y > -2413.05
				CLEAR_HELP()
				//~s~Use ~PAD_RSTICK_UPDOWN~ to move the spreader.~s~
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_SPR")
					PRINT_HELP_FOREVER("CRH_SPR")
				ENDIF
	
				iCraneHelpTimer = GET_GAME_TIMER()
				vInitialCabinPosition = GET_ENTITY_COORDS(s_crane.obj_cabin, FALSE)
				INHIBIT_CRANE(CRANE_ACTION_GRABBER)
				iCraneHelp++
			ELSE
				INHIBIT_CRANE(CRANE_ACTION_GRABBER)
			ENDIF
		ELSE
			INHIBIT_CRANE(CRANE_ACTION_CABIN)
			INHIBIT_CRANE(CRANE_ACTION_SPREADER)
			INHIBIT_CRANE(CRANE_ACTION_CRANE)
			INHIBIT_CRANE(CRANE_ACTION_GRABBER)
		ENDIF
	BREAK
	
	CASE 2
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10b")
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10b", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("DS1_10b",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			iCraneHelp++
		ENDIF
	BREAK
	
	CASE 3
		IF MANAGE_MY_TIMER(iCraneHelpTimer,1000)
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10c")
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10c", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("DS1_10c",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(s_crane.obj_spreader)
				v_spreader_pos = GET_ENTITY_COORDS(s_crane.obj_spreader)
			ENDIF
			IF CAN_CRANE_PICK_UP_CONTAINER(GET_ENTITY_COORDS(s_containers[0].obj_main, FALSE), v_spreader_pos)
			OR CAN_CRANE_PICK_UP_CONTAINER(GET_ENTITY_COORDS(s_containers[1].obj_main, FALSE), v_spreader_pos)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_SPR")
					CLEAR_HELP()
				ENDIF
				IF IS_THIS_PRINT_BEING_DISPLAYED("AW_GRAB_CR")
					CLEAR_PRINTS()
				ENDIF
				//PRINT_GOD_TEXT("AW_CRA_DIA3")
				//PICK UP CONTAINER
				//~s~Press ~PAD_A~ to pick up a container.~s~
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_PICKUC")
					PRINT_HELP_FOREVER("CRH_PICKUC")
				ENDIF
				SETTIMERA(0)
				iCraneHelp++
			ELSE
				//INHIBIT_CRANE(CRANE_ACTION_CABIN)
				//INHIBIT_CRANE(CRANE_ACTION_CRANE)
				INHIBIT_CRANE(CRANE_ACTION_GRABBER)
			ENDIF
		ELSE
			INHIBIT_CRANE(CRANE_ACTION_CABIN)
			INHIBIT_CRANE(CRANE_ACTION_SPREADER)
			INHIBIT_CRANE(CRANE_ACTION_CRANE)
			INHIBIT_CRANE(CRANE_ACTION_GRABBER)
		ENDIF
	BREAK
	
	CASE 4
		IF s_crane.b_container_attached
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_PICKUC")
				CLEAR_HELP()
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTrailer1)
				SET_ENTITY_INVINCIBLE(vehTrailer1,TRUE)
				FREEZE_ENTITY_POSITION(vehTrailer1,TRUE)
				SET_ENTITY_INVINCIBLE(vehTrailer1,TRUE)
				SET_ENTITY_PROOFS(vehTrailer1,TRUE,TRUE,TRUE,TRUE,TRUE)
			ENDIF
			
			REMOVE_BLIP(container0_blip)
			REMOVE_BLIP(container1_blip)
			//PRINT_GOD_TEXT("AW_CRA_DIA4")
			//LSTICK MOVE CRANE UP DOWN
			IF b_is_crane_cinematic_active
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_HELP2A")
					PRINT_HELP_FOREVER("CRH_HELP2A")
				ENDIF
			ELSE
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_HELP2B")
					PRINT_HELP_FOREVER("CRH_HELP2B")
				ENDIF
			ENDIF
			iCraneHelp++
		ELSE
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10d")
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10d", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("DS1_10d",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			//INHIBIT_CRANE(CRANE_ACTION_CABIN)
			//INHIBIT_CRANE(CRANE_ACTION_SPREADER)
			//INHIBIT_CRANE(CRANE_ACTION_CRANE)
		ENDIF
	BREAK
	
	CASE 5
		IF s_crane.b_container_attached
			IF s_crane.f_crane_offset < -55
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_HELP2A")
					CLEAR_HELP()
				ENDIF
				IF DOES_ENTITY_EXIST(vehTrailer1)
					SET_ENTITY_INVINCIBLE(vehTrailer1,TRUE)
				ENDIF
				DELETE_VEHICLE(vehFloydTruck)
				DELETE_VEHICLE(vehTrailer2)
				//INHIBIT_CRANE(CRANE_ACTION_CABIN)
				//INHIBIT_CRANE(CRANE_ACTION_SPREADER)
				//INHIBIT_CRANE(CRANE_ACTION_CRANE)
				INHIBIT_CRANE(CRANE_ACTION_GRABBER)
				iCraneHelp++
			ELSE
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10e")
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10e", CONV_PRIORITY_MEDIUM)
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								SET_LABEL_AS_TRIGGERED("DS1_10e",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF HAS_LABEL_BEEN_TRIGGERED("DS1_10e")
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10e3")
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10e3", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("DS1_10e3",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10e5")
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10e5", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("DS1_10e5",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				INHIBIT_CRANE(CRANE_ACTION_GRABBER)
			ENDIF
		ENDIF
	BREAK
	
	//In position for dropping crate
	CASE 6
		IF NOT IS_ENTITY_DEAD(s_containers[1].obj_main)
		AND NOT IS_ENTITY_DEAD(s_containers[0].obj_main)
		AND NOT IS_ENTITY_DEAD(vehTrailer1)
			IF (IS_ENTITY_AT_ENTITY(s_containers[1].obj_main,vehTrailer1,<<1.5,1,1>>) AND GET_ENTITY_SPEED(s_containers[1].obj_main) < 0.3)
			OR (IS_ENTITY_AT_ENTITY(s_containers[0].obj_main,vehTrailer1,<<1.5,1,1>>) AND GET_ENTITY_SPEED(s_containers[0].obj_main) < 0.3)
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_RELC")
					PRINT_HELP_FOREVER("CRH_RELC")
				ENDIF
				INHIBIT_LOWERING_SPREADER()
				INHIBIT_CRANE(CRANE_ACTION_GRABBER)
				iCraneHelp++
			ELSE
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10f")
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10f", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("DS1_10f",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF HAS_LABEL_BEEN_TRIGGERED("DS1_10f")
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10e2")
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10e2", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("DS1_10e2",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				INHIBIT_CRANE(CRANE_ACTION_GRABBER)
			ENDIF
		ENDIF
	BREAK
	
	//Drop off crate
	CASE 7
		
		IF NOT IS_ENTITY_DEAD(s_containers[1].obj_main)
		AND NOT IS_ENTITY_DEAD(s_containers[0].obj_main)
		AND NOT IS_ENTITY_DEAD(vehTrailer1)
			IF (IS_ENTITY_AT_ENTITY(s_containers[1].obj_main,vehTrailer1,<<1.5,1,1>>) AND GET_ENTITY_SPEED(s_containers[1].obj_main) < 0.3)
			OR (IS_ENTITY_AT_ENTITY(s_containers[0].obj_main,vehTrailer1,<<1.5,1,1>>) AND GET_ENTITY_SPEED(s_containers[0].obj_main) < 0.3)
				INHIBIT_LOWERING_SPREADER()
				IF s_crane.b_container_attached = FALSE
					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_RELC")
						CLEAR_HELP()
					ENDIF
					INHIBIT_CRANE(CRANE_ACTION_GRABBER)
					INHIBIT_CRANE(CRANE_ACTION_CABIN)
					INHIBIT_CRANE(CRANE_ACTION_SPREADER)
					INHIBIT_CRANE(CRANE_ACTION_CRANE)
					iCraneHelp++
				ENDIF
			ELSE
				INHIBIT_CRANE(CRANE_ACTION_GRABBER)
			ENDIF
		ENDIF
		
		IF HAS_LABEL_BEEN_TRIGGERED("DS1_10f")
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10e2")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10e2", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("DS1_10e2",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
//		IF IS_VEHICLE_DRIVEABLE(vehTrailer1)
//			IF IS_VEHICLE_DRIVEABLE(vehFirstHauler)
//				IF ABSF(v_spreader_pos.x - vTemp.x ) < 0.8
//				AND ABSF(v_spreader_pos.y - vTemp.y ) < 0.4
//				AND ABSF(v_spreader_pos.z - vTemp.z ) < 4.2
//					IF s_crane.b_container_attached = FALSE
//						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CRH_RELC")
//							CLEAR_HELP()
//						ENDIF
//						INHIBIT_CRANE(CRANE_ACTION_GRABBER)
//						INHIBIT_CRANE(CRANE_ACTION_CABIN)
//						INHIBIT_CRANE(CRANE_ACTION_SPREADER)
//						INHIBIT_CRANE(CRANE_ACTION_CRANE)
//						iCraneHelp++
//
//					ENDIF
//				ELSE
//					INHIBIT_CRANE(CRANE_ACTION_GRABBER)
//				ENDIF
//			ENDIF
//		ENDIF
	BREAK
	
	//Crate dropped off
	CASE 8
		VECTOR vTempOffset1
		IF IS_VEHICLE_DRIVEABLE(vehTrailer1)
			IF s_crane.b_container_attached = FALSE
				IF DOES_BLIP_EXIST(dest_blip)
					REMOVE_BLIP(dest_blip)
				ENDIF
//				IF NOT DOES_BLIP_EXIST(dest_blip)
//					dest_blip = CREATE_BLIP_FOR_ENTITY(s_containers[1].obj_main)
//				ENDIF
				IF DOES_ENTITY_EXIST(vehTrailer1)
					IF NOT IS_ENTITY_DEAD(s_containers[1].obj_main)
						IF IS_ENTITY_AT_ENTITY(s_containers[1].obj_main,vehTrailer1,<<1.5,1,1>>)
							INHIBIT_LOWERING_SPREADER()
							IF GET_ENTITY_SPEED(s_containers[1].obj_main) < 0.3
								IF NOT DOES_BLIP_EXIST(container0_blip)
									container0_blip = CREATE_BLIP_FOR_ENTITY(s_containers[0].obj_main)
								ENDIF
								
								REMOVE_BLIP(dest_blip)
								
								IF DOES_BLIP_EXIST(container1_blip)
									REMOVE_BLIP(container1_blip)
								ENDIF

								vTempOffset1 = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehTrailer1,GET_ENTITY_COORDS(s_containers[1].obj_main))
								ATTACH_ENTITY_TO_ENTITY(s_containers[1].obj_main,vehTrailer1,0,vTempOffset1,<<0,0,0>>,TRUE,FALSE,FALSE)
								
								bCrate1Attached = TRUE
								bForceNoGrab = TRUE
								s_Containers[1].b_loaded_on_truck = TRUE
							ENDIF
						ELSE
							IF NOT IS_ENTITY_ATTACHED(s_containers[1].obj_main)
								IF GET_ENTITY_SPEED(s_containers[1].obj_main) < 0.2
									IF IS_ENTITY_AT_ENTITY(s_containers[1].obj_main,vehTrailer1,<<10.5,2,10>>)
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10l")
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10l", CONV_PRIORITY_MEDIUM)
														SET_LABEL_AS_TRIGGERED("DS1_10l",TRUE)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							IF NOT IS_ENTITY_DEAD(s_containers[0].obj_main)
								IF IS_ENTITY_AT_ENTITY(s_containers[0].obj_main,vehTrailer1,<<1.5,1,1>>)
									INHIBIT_LOWERING_SPREADER()
									IF GET_ENTITY_SPEED(s_containers[0].obj_main) < 0.3
										
										IF NOT DOES_BLIP_EXIST(container1_blip)
											container1_blip = CREATE_BLIP_FOR_ENTITY(s_containers[1].obj_main)
										ENDIF

										IF DOES_BLIP_EXIST(container0_blip)
											REMOVE_BLIP(container0_blip)
										ENDIF
										
										REMOVE_BLIP(dest_blip)
										vTempOffset1 = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehTrailer1,GET_ENTITY_COORDS(s_containers[0].obj_main))
										ATTACH_ENTITY_TO_ENTITY(s_containers[0].obj_main,vehTrailer1,0,vTempOffset1,<<0,0,0>>,TRUE,FALSE,FALSE)
										FREEZE_ENTITY_POSITION(vehFirstHauler,FALSE)
										FREEZE_ENTITY_POSITION(vehTrailer1,FALSE)
										bCrate0Attached = TRUE
										bForceNoGrab = TRUE
										s_Containers[0].b_loaded_on_truck = TRUE
										
									ENDIF
								ELSE
									IF NOT IS_ENTITY_ATTACHED(s_containers[0].obj_main)
										IF GET_ENTITY_SPEED(s_containers[0].obj_main) < 0.2
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
												IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10l")
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10l", CONV_PRIORITY_MEDIUM)
															SET_LABEL_AS_TRIGGERED("DS1_10l",TRUE)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF bCrate0Attached OR bCrate1Attached
					INHIBIT_CRANE(CRANE_ACTION_SPREADER)
					INHIBIT_CRANE(CRANE_ACTION_GRABBER)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10f3")
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10f3", CONV_PRIORITY_MEDIUM)
									REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
									SET_LABEL_AS_TRIGGERED("DS1_10f3",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_VEHICLE_DRIVEABLE(vehFirstHauler)
							//Start vehicle recording.
							IF IS_ENTITY_ATTACHED_TO_ENTITY(vehTrailer1,s_containers[0].obj_main) OR IS_ENTITY_ATTACHED_TO_ENTITY(vehTrailer1,s_containers[1].obj_main)
								IF HAS_VEHICLE_RECORDING_BEEN_LOADED(2,"AWDocks1")
									FREEZE_ENTITY_POSITION(vehFirstHauler,FALSE)
									FREEZE_ENTITY_POSITION(vehTrailer1,FALSE)
									START_PLAYBACK_RECORDED_VEHICLE(vehFirstHauler,2,"AWDocks1")
									SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
									bInhibitCrane = TRUE
									iCraneTimer = GET_GAME_TIMER()
									bFakeSound = TRUE
									SET_LABEL_AS_TRIGGERED("DS1_10l",FALSE)
									iCraneHelp++
								ELSE
									REQUEST_VEHICLE_RECORDING(2,"AWDocks1")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				INHIBIT_CRANE(CRANE_ACTION_CABIN)
				INHIBIT_CRANE(CRANE_ACTION_SPREADER)
				INHIBIT_CRANE(CRANE_ACTION_CRANE)
			ENDIF
		ENDIF
	BREAK
	
	//So you can watch the truck leaving
	CASE 9
		INHIBIT_CRANE(CRANE_ACTION_GRABBER)
		INHIBIT_CRANE(CRANE_ACTION_SPREADER)
		//1542589
		//inhibting spreader
		
		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10g")
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10g", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("DS1_10g",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NOT MANAGE_MY_TIMER(iCraneTimer, 1000)
			REPEAT 8 i
				STOP_ROPE_UNWINDING_FRONT(s_crane.ropes[i])		
				START_ROPE_WINDING(s_crane.ropes[i])
			ENDREPEAT
			
			IF HAS_SOUND_FINISHED(s_Crane.i_spreader_sound_id)
				PLAY_SOUND_FROM_ENTITY(s_Crane.i_spreader_sound_id, "CRANE_MOVE_U_D", s_Crane.obj_spreader)
				//PRINTLN("SPREADER AUDIO PLAYING")//PRINTNL()
			ENDIF
		ENDIF
		
		IF MANAGE_MY_TIMER(iCraneTimer,5000)
			bFakeSound = FALSE
			bInhibitCrane = FALSE
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			iCraneTimer = GET_GAME_TIMER()
			iCraneHelp++
		ENDIF
	BREAK
	
	//Picked up another container
	CASE 10
		//1542589
		//inhibting grabber until you are past
		//INHIBIT_CRANE(CRANE_ACTION_GRABBER)
		IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10g")
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10g", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("DS1_10g",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10g2")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10g2", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("DS1_10g2",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF MANAGE_MY_TIMER(iCraneTimer,3000)
			bForceNoGrab = FALSE
		ENDIF
		
		IF s_crane.b_container_attached
			INHIBIT_CRANE(CRANE_ACTION_GRABBER)
		ENDIF
		
		IF s_crane.b_container_attached OR s_crane.f_crane_offset > -22
			CLEAR_PRINTS()
			//PRINT_GOD_TEXT("AW_CRA_DIA7")
			bForceNoGrab = FALSE
			
			IF NOT DOES_ENTITY_EXIST(vehFloydTruck)
				SPAWN_VEHICLE(vehFloydTruck,HAULER,<< -117.9839, -2416.6272, 5.0003 >>,91.3366)
				
			ELSE
				IF NOT DOES_ENTITY_EXIST(vehTrailer2)
					IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
						SPAWN_VEHICLE(vehTrailer2,DOCKTRAILER,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehFloydTruck, <<-0.3, -9.5, -0.25>>), GET_ENTITY_HEADING(vehFloydTruck))
						SET_VEHICLE_COLOURS(vehFloydTruck,0,0)
						SET_VEHICLE_EXTRA(vehTrailer2,1,TRUE)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehTrailer2)
					IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
						//IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(vehFloydTruck)
							
							IF NOT IS_PED_INJURED(pedFloyd)
								TASK_ENTER_VEHICLE(pedFloyd,vehFloydTruck)
								IF IS_VEHICLE_DRIVEABLE(vehFloydTruck) AND DOES_ENTITY_EXIST(vehTrailer2)
									//IF IS_VEHICLE_ATTACHED_TO_TRAILER(vehFloydTruck)
										ATTACH_VEHICLE_TO_TRAILER(vehFloydTruck,vehTrailer2)
										//IF NOT IS_ENTITY_ATTACHED(vehFloydTruck)
											FREEZE_ENTITY_POSITION(vehFloydTruck,TRUE)
										//ENDIF
										//IF NOT IS_ENTITY_ATTACHED(vehTrailer2)
											FREEZE_ENTITY_POSITION(vehTrailer2,TRUE)
										//ENDIF
										SET_ENTITY_PROOFS(vehTrailer2,FALSE,FALSE,FALSE,TRUE,FALSE)
										iCraneHelp++
									//ELSE
										
									//ENDIF
								ENDIF
							ENDIF
						//ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
	BREAK
	
	CASE 11
		IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10g3")
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_10g3", CONV_PRIORITY_MEDIUM)
						SET_LABEL_AS_TRIGGERED("DS1_10g3",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF s_crane.b_container_attached = TRUE
			INHIBIT_CRANE(CRANE_ACTION_GRABBER)
		ENDIF
		IF s_crane.f_crane_offset < -55
			//PRINT_GOD_TEXT("AW_CRA_DIA10")
			iCraneHelp++
		ENDIF
	BREAK

//check if both crates are in the area	
//	IF IS_ENTITY_AT_COORD(s_containers[1].obj_main,<< -106.7826, -2414.5659, 5.0003 >>,<<10,10,2>>) 
//	AND IS_ENTITY_AT_COORD(s_containers[0].obj_main,<< -106.7826, -2414.5659, 5.0003 >>,<<10,10,2>>)
	CASE 12
	
		//INHIBIT_CRANE(CRANE_ACTION_GRABBER)
		
		VECTOR vTempOffset
		IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
			vTemp = GET_ENTITY_COORDS(vehFloydTruck)
			PRINTSTRING("float:")PRINTFLOAT(ABSF(v_spreader_pos.x - vTemp.x ))
		
			IF IS_VEHICLE_DRIVEABLE(vehTrailer2)
			//IF ABSF(v_spreader_pos.x - vTemp.x) < 5.0 
				IF s_crane.b_container_attached = FALSE
					IF DOES_ENTITY_EXIST(vehTrailer2)
						IF NOT IS_ENTITY_DEAD(s_containers[1].obj_main)
							IF IS_ENTITY_AT_ENTITY(s_containers[1].obj_main,vehTrailer2,<<1.5,1,1>>)
								INHIBIT_LOWERING_SPREADER()
								IF GET_ENTITY_SPEED(s_containers[1].obj_main) < 0.3
									IF bCrate1Attached = FALSE
										DETACH_ENTITY(s_containers[1].obj_main)
										s_Containers[1].b_loaded_on_truck = TRUE
										//ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(s_containers[1].obj_main,vehTrailer1, -1, -1, <<0,0,0>>,<<0,0,0>>,<<0,0,0>>, -1.0,TRUE,TRUE,TRUE,TRUE)
										vTempOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehTrailer2,GET_ENTITY_COORDS(s_containers[1].obj_main))
										ATTACH_ENTITY_TO_ENTITY(s_containers[1].obj_main,vehTrailer2,0,vTempOffset,<<0,0,0>>,TRUE,FALSE,FALSE)
										bCrate1Attached = TRUE
										bForceNoGrab = TRUE
										SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
										
										IF DOES_BLIP_EXIST(container0_blip)
											REMOVE_BLIP(container0_blip)
										ENDIF
										
										IF DOES_ENTITY_EXIST(vehFirstHauler)
											DELETE_VEHICLE(vehFirstHauler)
										ENDIF
										
										IF DOES_ENTITY_EXIST(vehTrailer1)
											DELETE_VEHICLE(vehTrailer1)
										ENDIF
										
										IF DOES_ENTITY_EXIST(s_containers[0].obj_main)
											DELETE_OBJECT(s_containers[0].obj_main)
										ENDIF
										
										IF DOES_ENTITY_EXIST(s_containers[0].obj_left_door)
											DELETE_OBJECT(s_containers[0].obj_left_door)
										ENDIF
										
										IF DOES_ENTITY_EXIST(s_containers[0].obj_right_door)
											DELETE_OBJECT(s_containers[0].obj_right_door)
										ENDIF

										CLEAR_PRINTS()
										//PRINT_GOD_TEXT("AW_CRA_DIA9")
										iCraneHelp++
									ENDIF
								ENDIF
							ELSE
								IF bCrate0Attached = FALSE
									IF NOT IS_ENTITY_DEAD(s_containers[0].obj_main)
										IF IS_ENTITY_AT_ENTITY(s_containers[0].obj_main,vehTrailer2,<<1.5,1,1>>)
											INHIBIT_LOWERING_SPREADER()
											IF GET_ENTITY_SPEED(s_containers[0].obj_main) < 0.3
												s_Containers[0].b_loaded_on_truck = TRUE
												DETACH_ENTITY(s_containers[0].obj_main)
												vTempOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehTrailer2,GET_ENTITY_COORDS(s_containers[0].obj_main))
												ATTACH_ENTITY_TO_ENTITY(s_containers[0].obj_main,vehTrailer2,0,vTempOffset,<<0,0,0>>,TRUE,FALSE,FALSE)
												//ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(s_containers[1].obj_main,vehTrailer1, -1, -1, <<0,0,0>>,<<0,0,0>>,<<0,0,0>>, -1.0,TRUE,TRUE,TRUE,TRUE)
												//ATTACH_ENTITY_TO_ENTITY(s_containers[0].obj_main,vehTrailer2,0,<<0,0,0>>,<<0,0,0>>,TRUE,FALSE,TRUE)
												bCrate0Attached = TRUE
												bForceNoGrab = TRUE
												SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
												
												IF DOES_BLIP_EXIST(container1_blip)
													REMOVE_BLIP(container1_blip)
												ENDIF
												
												IF DOES_ENTITY_EXIST(vehFirstHauler)
													DELETE_VEHICLE(vehFirstHauler)
												ENDIF
												
												IF DOES_ENTITY_EXIST(vehTrailer1)
													DELETE_VEHICLE(vehTrailer1)
												ENDIF
												
												IF DOES_ENTITY_EXIST(s_containers[1].obj_main)
													DELETE_OBJECT(s_containers[1].obj_main)
												ENDIF
												
												IF DOES_ENTITY_EXIST(s_containers[1].obj_left_door)
													DELETE_OBJECT(s_containers[1].obj_left_door)
												ENDIF
												
												IF DOES_ENTITY_EXIST(s_containers[1].obj_right_door)
													DELETE_OBJECT(s_containers[1].obj_right_door)
												ENDIF
												
												
												CLEAR_PRINTS()
												//PRINT_GOD_TEXT("AW_CRA_DIA9")
												iCraneHelp++
											ENDIF
										ELSE
											IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_noton")
												IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_noton", CONV_PRIORITY_MEDIUM)
															SET_LABEL_AS_TRIGGERED("DS1_noton",TRUE)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			//ENDIF
		ENDIF
	BREAK
	
	//bInhibitCrane = TRUE
	
	CASE 13
		INHIBIT_CRANE(CRANE_ACTION_GRABBER)
		IF s_crane.b_container_attached = FALSE
			IF s_crane.f_crane_offset < -55
				CLEAR_PRINTS()
				SETTIMERA(0)
				//PRINT_GOD_TEXT("AW_CRA_DIA8") 
				bInhibitCrane = TRUE
				bFakeSound = TRUE
				iCraneHelp++
			ELSE
				SETTIMERA(0)
				//PRINT_GOD_TEXT("AW_CRA_DIA9")
				//iCraneHelp++
			ENDIF
		ENDIF
	BREAK
	
	CASE 14
		
		IF s_crane.b_container_attached = FALSE
			//IF bCrate0Attached AND bCrate1Attached
			
				REPEAT 8 i
					STOP_ROPE_UNWINDING_FRONT(s_crane.ropes[i])		
					START_ROPE_WINDING(s_crane.ropes[i])
				ENDREPEAT
				
				
				IF TIMERA() > 3000
					IF s_crane.f_crane_offset < -55
						//PRINT_GOD_TEXT("AW_CRA_DIA8")
						IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
							//IF NOT IS_ENTITY_ATTACHED(vehFloydTruck)
								FREEZE_ENTITY_POSITION(vehFloydTruck,FALSE)
							//ENDIF
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(vehTrailer2)
							//IF NOT IS_ENTITY_ATTACHED(vehTrailer2)
								FREEZE_ENTITY_POSITION(vehTrailer2,FALSE)
							//ENDIF
						ENDIF
						IF DOES_BLIP_EXIST(dest_blip)
							REMOVE_BLIP(dest_blip)
						ENDIF
						IF DOES_BLIP_EXIST(container0_blip)
							REMOVE_BLIP(container0_blip)
						ENDIF
						IF DOES_BLIP_EXIST(container1_blip)
							REMOVE_BLIP(container1_blip)
						ENDIF
						IF NOT HAS_SOUND_FINISHED(s_Crane.i_spreader_sound_id)
							STOP_SOUND(s_Crane.i_spreader_sound_id)
						ENDIF
						UNLOCK_MINIMAP_ANGLE()
						
						INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(DH1_EMPLOYEE_OF_THE_MONTH)
						
						bCraneSectionComplete = TRUE
						IF IS_AUDIO_SCENE_ACTIVE("DOCKS_HEIST_USING_CRANE")
							STOP_AUDIO_SCENE("DOCKS_HEIST_USING_CRANE")
						ENDIF
						SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
						bFakeSound = FALSE
						iCraneHelp++
					ENDIF
				ELSE
					IF NOT s_crane.b_spreader_stuck_on_ground
						
						IF HAS_SOUND_FINISHED(s_Crane.i_spreader_sound_id)
							PLAY_SOUND_FROM_ENTITY(s_Crane.i_spreader_sound_id, "CRANE_MOVE_U_D", s_Crane.obj_spreader)
							//PRINTLN("SPREADER AUDIO PLAYING")//PRINTNL()
						ENDIF
						SET_VARIABLE_ON_SOUND(s_Crane.i_spreader_sound_id, "Speed", MAX_SPREADER_VEL / 2.0)
					ELSE
						IF NOT HAS_SOUND_FINISHED(s_Crane.i_spreader_sound_id)
							STOP_SOUND(s_Crane.i_spreader_sound_id)
						ENDIF
					ENDIF
				ENDIF
			//ENDIF
		ELSE
			INHIBIT_CRANE(CRANE_ACTION_GRABBER)
		ENDIF
	BREAK
	
	CASE 15
		UNLOCK_MINIMAP_ANGLE()
		DISPLAY_RADAR(TRUE)
		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			CLEAR_PRINTS()
		ENDIF
		IF IS_HELP_MESSAGE_ON_SCREEN()
			CLEAR_HELP()
		ENDIF
		
		IF DOES_BLIP_EXIST(dest_blip)
			REMOVE_BLIP(dest_blip)
		ENDIF
		IF DOES_BLIP_EXIST(container0_blip)
			REMOVE_BLIP(container0_blip)
		ENDIF
		IF DOES_BLIP_EXIST(container1_blip)
			REMOVE_BLIP(container1_blip)
		ENDIF
		
	BREAK
	
ENDSWITCH

ENDPROC

// ======================================================================
//                                Ropes
// ======================================================================

PROC CREATE_ROPES(ENTITY_INDEX &Entity, ROPE_INDEX &rope, BOOL &bRopesCreated)
	IF NOT bRopesCreated

//		INT i
		VECTOR vCreatePos 
		IF NOT IS_ENTITY_DEAD(Entity)
				vCreatePos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Entity, <<0,0,0.7>>)
				rope = ADD_ROPE(vCreatePos, <<0,0,0>>, 15, PHYSICS_ROPE_DEFAULT)
			bRopesCreated = TRUE
		ENDIF
	ENDIF
ENDPROC

// attach sub to container with rope
PROC ATTACH_ROPE_FROM_ENTITY_TO_OBJECT(ENTITY_INDEX &Vehicle, OBJECT_INDEX &containerObject, ROPE_INDEX &rope, BOOL &bRopesCreated, BOOL &bRopesAttached, BOOL bPin, BOOL bSetPlaybackMass = FALSE)
	IF NOT bRopesCreated
		CREATE_ROPES(Vehicle, rope, bRopesCreated)
	ENDIF
	
	IF bRopesCreated
		IF NOT bRopesAttached
//			INT i
			VECTOR vAttachPos
			VECTOR vContainerAttachPos
			IF NOT IS_ENTITY_DEAD(Vehicle)
			AND DOES_ENTITY_EXIST(containerObject)
				IF DOES_ENTITY_HAVE_PHYSICS(containerObject)
				OR bPin
					DETACH_ENTITY(containerObject)
						vAttachPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Vehicle, <<0,0,0.7>>)
						vContainerAttachPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(containerObject, <<0,0,0.7>>)
						IF bPin
							DETACH_ROPE_FROM_ENTITY(rope, containerObject)
							PIN_ROPE_VERTEX(rope, 0, vAttachPos)
							PIN_ROPE_VERTEX(rope, GET_ROPE_VERTEX_COUNT(rope)-1, vContainerAttachPos)
						ELSE
							UNPIN_ROPE_VERTEX(rope, 0)
							UNPIN_ROPE_VERTEX(rope, GET_ROPE_VERTEX_COUNT(rope)-1)
							ATTACH_ENTITIES_TO_ROPE(rope, containerObject, Vehicle, vContainerAttachPos, vAttachPos, 15, 0, 0)
						ENDIF
					
					IF NOT bPin
						FLOAT fMass
						FLOAT fGravity
						IF bSetPlaybackMass
							fMass = 0.00001
							fGravity = 1
						ELSE
							fMass = 0.00001
							fGravity = 1
						ENDIF
						SET_OBJECT_PHYSICS_PARAMS(containerObject, fMass, fGravity, <<-1,-1,-1>>, <<-1,-1,-1>>)
					ENDIF
					bRopesAttached = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_FORCE_MERC_TASKS()
	IF bForceTasks = TRUE
		FOR iThisCounter = 0 TO iTotalMerc - 1
			IF NOT IS_ENTITY_DEAD(pedMerc[iThisCounter])
				IF NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					IF GET_SCRIPT_TASK_STATUS(pedMerc[iThisCounter],SCRIPT_TASK_COMBAT)<> PERFORMING_TASK 
						TASK_COMBAT_PED(pedMerc[iThisCounter],sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	ENDIF
ENDPROC

PROC SET_MAP_CRANE_VISIBLE(BOOL b_is_visible)
	
	IF b_is_visible = FALSE
	
	ENDIF
		
	//Dockside Crane
	CREATE_MODEL_HIDE(<< -72.16, -2390.39, 24.11 >> , 200.0, Prop_Dock_Crane_02, b_is_visible)
	//SET_USES_COLLISION_OF_CLOSEST_OBJECT_OF_TYPE(<< -99.0574, -2389.7400, 5.0005 >> , 300.0, Prop_Dock_Crane_02, b_is_visible)

ENDPROC

//Some MISC debug procs.
#IF IS_DEBUG_BUILD
//PURPOSE:  does a script assert
PROC ScriptAssert(STRING errorPassed)
	PRINTSTRING("------- Docks Setup 1 Script Assert --------")//PRINTNL()	
	PRINTSTRING("MISSION_STAGE_ENUM = ")PRINTINT(ENUM_TO_INT(eMissionStage))//PRINTNL()
	PRINTSTRING("iprogress = ")PRINTINT(iprogress)//PRINTNL()
	PRINTSTRING("iPreLoadStage = ")PRINTINT(iPreStreamStage)//PRINTNL()
	PRINTSTRING("iSpeechProgress = ")PRINTINT(iSpeechProgress)//PRINTNL()
	//PRINTSTRING("iBuddyProgress = ")PRINTINT(iBuddyProgress)//PRINTNL()
	//PRINTSTRING("ienemyAIprog = ")PRINTINT(ienemyAIprog)//PRINTNL()
	//PRINTSTRING("icutsceneprog = ")PRINTINT(icutsceneprog)//PRINTNL()
	SCRIPT_ASSERT(errorPassed)
ENDPROC
//PURPOSE:  Prints a line so it's easy to find my db output in logs
PROC DBOutPut()
		PRINTSTRING("Docks Setup 1 output")//PRINTNL()
ENDPROC
//PURPOSE: Prints thigs when they change value
PROC Where_are_we()
	IF iMissionStageDB <> ENUM_TO_INT(eMissionStage)
		iMissionStageDB = ENUM_TO_INT(eMissionStage)
		DBOutPut()
		PRINTSTRING("MISSION_STAGE_ENUM = ")PRINTINT(iMissionStageDB)//PRINTNL()
	ENDIF	
	IF iProgressDB <> iProgress
		iProgressDB = iProgress
		DBOutPut()
		PRINTSTRING("iprogress = ")PRINTINT(iprogress)//PRINTNL()
	ENDIF
	IF iPreLoadStageDB <> iPreStreamStage
		iPreLoadStageDB = iPreStreamStage
		DBOutPut()
		PRINTSTRING("iPreLoadStage = ")PRINTINT(iPreStreamStage)//PRINTNL()
	ENDIF
	IF iSpeechTextProgressDB <> iSpeechProgress
		iSpeechTextProgressDB = iSpeechProgress
		DBOutPut()
		PRINTSTRING("iSpeechProgress = ")PRINTINT(iSpeechProgress)//PRINTNL()
	ENDIF
	
	IF iMissionDialogueDB <> iMissionDialogue
		iMissionDialogueDB = iMissionDialogue
		DBOutPut()
		PRINTSTRING("iMissionDialogue  = ")PRINTINT(iMissionDialogue)//PRINTNL()
	ENDIF
	
	IF iCraneHelpDB <> iCraneHelp
	   iCraneHelpDB = iCraneHelp
	   DBOutPut()
	   PRINTSTRING("iCraneHelp  = ")PRINTINT(iCraneHelp)//PRINTNL()
	ENDIF
	
ENDPROC
#ENDIF

//PURPOSE: controls the sucessfull completion of the mission
PROC MISSION_PASSED()
//	IF IS_SCREEN_FADED_OUT()
//		DO_SCREEN_FADE_IN(500)
//	ENDIF
	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(DH1_HONEST_DAYS_WORK)
	SET_HEIST_BOARD_DISPLAY_GROUP_VISIBLE(HEIST_DOCKS, PBDG_1, TRUE)
	//RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR, bHasChanged)  
	
	//Make sure the board intro cutscene is loading as we pass.
	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_PRIME_BOARD_TRANSITION, TRUE)
	IF GET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_PICTURE) != BUILDINGSTATE_DESTROYED
		SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_PICTURE, BUILDINGSTATE_DESTROYED)
	ENDIF
	IF GET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT) != BUILDINGSTATE_DESTROYED
		SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT, BUILDINGSTATE_DESTROYED)
	ENDIF
	IF GET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT_SOFA) != BUILDINGSTATE_DESTROYED
		SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT_SOFA, BUILDINGSTATE_DESTROYED)
	ENDIF
//	IF GET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM) != BUILDINGSTATE_DESTROYED
//		SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM, BUILDINGSTATE_DESTROYED)
//	ENDIF
//	IF GET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM_CUTSCENE) != BUILDINGSTATE_NORMAL
//		SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM_CUTSCENE, BUILDINGSTATE_NORMAL)
//	ENDIF
	
	Mission_Flow_Mission_Passed(TRUE)
	//RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID()) 
    MISSION_CLEANUP(FALSE,TRUE)	
ENDPROC


//PURPOSE: resets the mission stage ints
PROC RESET_MISSION_STAGE_VARIABLES()
	iProgress			= 0
	iPreStreamStage		= 0	
	//iSpeechProgress	= 0
	//iBuddyProgress		= 0
	//icutsceneprog 		= 0
	//ienemyAIprog 		= 0
	iSetupProgress      = 0
	bCraneSectionComplete = FALSE
	bForceNoGrab = FALSE
	bInhibitCrane = FALSE
	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
	bObjectsCreated[0] = FALSE 
	bObjectsCreated[1] = FALSE
	bObjectsCreated[2] = FALSE
	
	#IF IS_DEBUG_BUILD
		iProgressDB = 0
		iPreLoadStageDB = 0
		iSpeechTextProgressDB = 0
		//icutsceneprogDB = 0
		//ienemyAIprogDB = 0
		iMissionStageDB = 0
	#ENDIF	
ENDPROC
//PURPOSE: resets the mission stage ints if you skipp
PROC RESET_MISSION_STAGE_INTS_SKIP()
	RESET_MISSION_STAGE_VARIABLES()
	bmissionstageloaded = TRUE
	#IF IS_DEBUG_BUILD
		bskipping = FALSE
	#ENDIF
ENDPROC

PROC ADVANCE_MISSION_STAGE()
	//bPrinted = FALSE
	eMissionStage = INT_TO_ENUM(MISSION_STAGE_ENUM, (ENUM_TO_INT(eMissionStage) + 1))
	RESET_MISSION_STAGE_VARIABLES()

ENDPROC
// ======================================================================
//                        PED & Vehicle Creation 
// ======================================================================

//Create the docks stuff

FUNC BOOL CREATE_THE_DOCKS_STUFF()	
	IF eMissionStage = MISSION_STAGE_1
		IF bObjectsCreated[0] = FALSE
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),vDocks) < 100
				REQUEST_MODEL(S_M_Y_BlackOps_01)
				REQUEST_MODEL(S_M_M_DOCKWORK_01)
				REQUEST_MODEL(BIFF)
				REQUEST_MODEL(FORKLIFT)
				
				IF HAS_MODEL_LOADED(S_M_Y_BlackOps_01)
				AND HAS_MODEL_LOADED(S_M_M_DOCKWORK_01)
				AND HAS_MODEL_LOADED(BIFF)
				AND HAS_MODEL_LOADED(FORKLIFT)
					IF NOT DOES_ENTITY_EXIST(vehDocks[0])
						SPAWN_VEHICLE(vehDocks[0],BIFF,vDockVeh[0],fDockVeh[0],-1,1.0,FALSE)
					ELSE
						IF NOT DOES_ENTITY_EXIST(vehDocks[1])
							SPAWN_VEHICLE(vehDocks[1],FORKLIFT,vDockVeh[1],fDockVeh[1],-1,1.0,FALSE)
						ELSE
							createMercenary(0,TRUE)
							createMercenary(1,TRUE)
							createMercenary(2,TRUE)
							createMercenary(3,FALSE)
							createMercenary(7,FALSE)
							createMercenary(8,FALSE)
							//createDockWorker(3)
							createDockWorker(4)
							createDockWorker(5)
							createDockWorker(6)
							createDockWorker(7)
							createDockWorker(8)
							IF eMissionStage != MISSION_STAGE_6
								createDockWorker(12)
							ENDIF
							createDockWorker(13)
							createDockWorker(14)
							createDockWorker(17)
							createDockWorker(18)
							bObjectsCreated[0] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF eMissionStage > MISSION_STAGE_1 AND eMissionStage < MISSION_STAGE_5
		IF bObjectsCreated[0] = FALSE
			REQUEST_MODEL(S_M_Y_BlackOps_01)
			REQUEST_MODEL(S_M_M_DOCKWORK_01)
			REQUEST_MODEL(BIFF)
			REQUEST_MODEL(FORKLIFT)
			IF HAS_MODEL_LOADED(S_M_Y_BlackOps_01)
			AND HAS_MODEL_LOADED(S_M_M_DOCKWORK_01)
			AND HAS_MODEL_LOADED(BIFF)
			AND HAS_MODEL_LOADED(FORKLIFT)
				IF eMissionStage =  MISSION_STAGE_2
					IF NOT DOES_ENTITY_EXIST(vehDocks[2])
						SPAWN_VEHICLE(vehDocks[2],BIFF,vDockVeh[2],fDockVeh[2],-1,1.0,FALSE)
					ELSE
						IF NOT DOES_ENTITY_EXIST(vehDocks[3])
							SPAWN_VEHICLE(vehDocks[3],BIFF,vDockVeh[3],fDockVeh[3],-1,1.0,FALSE)
						ELSE
							IF NOT DOES_ENTITY_EXIST(vehDocks[0])
								SPAWN_VEHICLE(vehDocks[0],BIFF,vDockVeh[0],fDockVeh[0],-1,1.0,FALSE)
							ELSE
								IF NOT DOES_ENTITY_EXIST(vehDocks[1])
									SPAWN_VEHICLE(vehDocks[1],FORKLIFT,vDockVeh[1],fDockVeh[1],-1,1.0,FALSE)
								ELSE
									createMercenary(0,TRUE)
									createMercenary(1,TRUE)
									createMercenary(2,TRUE)
									createMercenary(3,FALSE)
									createMercenary(7,FALSE)
									createMercenary(8,FALSE)
									createDockWorker(4)
									createDockWorker(5)
									createDockWorker(6)
									createDockWorker(7)
									createDockWorker(8)
									IF eMissionStage != MISSION_STAGE_6
										createDockWorker(12)
									ENDIF
									createDockWorker(13)
									createDockWorker(14)
									createDockWorker(17)
									createDockWorker(18)
									bObjectsCreated[0] = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT DOES_ENTITY_EXIST(vehDocks[1])
						SPAWN_VEHICLE(vehDocks[1],FORKLIFT,vDockVeh[1],fDockVeh[1],-1,1.0,FALSE)
					ELSE
						createMercenary(0,TRUE)
						createMercenary(1,TRUE)
						createMercenary(2,TRUE)
						createMercenary(3,FALSE)
						createMercenary(7,FALSE)
						createMercenary(8,FALSE)
						createDockWorker(4)
						createDockWorker(5)
						createDockWorker(6)
						createDockWorker(7)
						createDockWorker(8)
						IF eMissionStage != MISSION_STAGE_6
							createDockWorker(12)
						ENDIF
						createDockWorker(13)
						createDockWorker(14)
						createDockWorker(17)
						createDockWorker(18)
						bObjectsCreated[0] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF eMissionStage > MISSION_STAGE_4
		IF bObjectsCreated[0] = FALSE
			REQUEST_MODEL(S_M_Y_BlackOps_01)
			REQUEST_MODEL(S_M_M_DOCKWORK_01)
			IF HAS_MODEL_LOADED(S_M_Y_BlackOps_01)
			AND HAS_MODEL_LOADED(S_M_M_DOCKWORK_01)
				createMercenary(0,TRUE)
				createMercenary(1,TRUE)
				createMercenary(2,TRUE)
				createMercenary(3,FALSE)
				createMercenary(7,FALSE)
				createMercenary(8,FALSE)
				createDockWorker(4)
				createDockWorker(5)
				createDockWorker(6)
				createDockWorker(7)
				createDockWorker(8)
				IF eMissionStage != MISSION_STAGE_6
					createDockWorker(12)
				ENDIF
				createDockWorker(13)
				createDockWorker(14)
				createDockWorker(17)
				createDockWorker(18)
				bObjectsCreated[0] = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF eMissionStage > MISSION_STAGE_2 AND eMissionStage < MISSION_STAGE_PASSED
		IF bObjectsCreated[1] = FALSE
			IF bObjectsCreated[0] = TRUE
				REQUEST_MODEL(HANDLER)
				REQUEST_MODEL(FORKLIFT)
				REQUEST_MODEL(PROP_CONTR_03B_LD)
				IF HAS_MODEL_LOADED(FORKLIFT)
				AND HAS_MODEL_LOADED(PROP_CONTR_03B_LD)
				AND HAS_MODEL_LOADED(HANDLER)
					IF eMissionStage < MISSION_STAGE_7
						IF NOT DOES_ENTITY_EXIST(vehHandler)
							IF eMissionStage = MISSION_STAGE_6
								SPAWN_VEHICLE(vehHandler,HANDLER,<<-92.7994, -2448.4775, 5.0163>>, 257.5271,-1,1.0,FALSE)
								SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehHandler,FALSE)
								SET_VEHICLE_COLOUR_COMBINATION(vehHandler,0)
								SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehHandler,FALSE)
								SET_VEHICLE_PROVIDES_COVER(vehHandler,FALSE)
							ELSE
								SPAWN_VEHICLE(vehHandler,HANDLER,<<-129.77, -2418.28, 6.24>>,183.0850,-1,1.0,FALSE)
								SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehHandler,FALSE)
								SET_VEHICLE_COLOUR_COMBINATION(vehHandler,0)
								SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehHandler,FALSE)
								FREEZE_ENTITY_POSITION(vehHandler,TRUE)
								SET_VEHICLE_PROVIDES_COVER(vehHandler,FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_PED_INJURED(pedMerc[3])
						TASK_START_SCENARIO_AT_POSITION(pedMerc[3],"WORLD_HUMAN_GUARD_STAND",<<-123.91, -2352.94, 9.32>>,-0.21)
					ENDIF
					
					IF NOT IS_PED_INJURED(pedMerc[7])
						TASK_START_SCENARIO_AT_POSITION(pedMerc[7],"WORLD_HUMAN_SMOKING",<<-133.26, -2379.25, 9.32>>,153.86)
					ENDIF
					
					IF NOT IS_PED_INJURED(pedMerc[8])
						TASK_START_SCENARIO_AT_POSITION(pedMerc[8],"WORLD_HUMAN_BINOCULARS",<<-216.83, -2376.18, 26.46>>,-160.35)
					ENDIF
					
					IF NOT IS_PED_INJURED(pedDockWorker[6])
						TASK_START_SCENARIO_IN_PLACE(pedDockWorker[6],"WORLD_HUMAN_WELDING")
						SET_PED_CAN_EVASIVE_DIVE(pedDockWorker[6],FALSE)
						SET_RAGDOLL_BLOCKING_FLAGS(pedDockWorker[6],RBF_PLAYER_IMPACT)
					ENDIF
					
					IF NOT IS_PED_INJURED(pedDockWorker[8])
						TASK_START_SCENARIO_IN_PLACE(pedDockWorker[8],"WORLD_HUMAN_STAND_MOBILE")
					ENDIF

					IF NOT IS_PED_INJURED(pedDockWorker[13])
						TASK_START_SCENARIO_IN_PLACE(pedDockWorker[13],"WORLD_HUMAN_CLIPBOARD")
						SET_RAGDOLL_BLOCKING_FLAGS(pedDockWorker[13],RBF_PLAYER_IMPACT)
					ENDIF
				
					bObjectsCreated[1] = TRUE
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(pedDockWorker[6])
				SET_PED_CAPSULE(pedDockWorker[6], 0.5)
			ENDIF
		ENDIF
	ENDIF
	
	IF eMissionStage = MISSION_STAGE_7
		IF bObjectsCreated[3] = FALSE
			REQUEST_MODEL(HAULER)
			REQUEST_MODEL(DOCKTRAILER)
			IF HAS_MODEL_LOADED(HAULER)
			AND HAS_MODEL_LOADED(DOCKTRAILER)
				IF NOT DOES_ENTITY_EXIST(vehFirstHauler)
					SPAWN_VEHICLE(vehFirstHauler,HAULER,<< -117.9839, -2416.6272, 5.0003 >>,91.3366)
				ELSE
					IF NOT DOES_ENTITY_EXIST(vehTrailer1)
						IF IS_VEHICLE_DRIVEABLE(vehFirstHauler)
							SPAWN_VEHICLE(vehTrailer1,DOCKTRAILER,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehFirstHauler, <<-0.3, -9.5, -0.25>>), GET_ENTITY_HEADING(vehFirstHauler))
							ACTIVATE_PHYSICS(vehFirstHauler)
							ACTIVATE_PHYSICS(vehTrailer1)
							SET_VEHICLE_EXTRA(vehTrailer1,1,TRUE)
						ENDIF
					ELSE
						bObjectsCreated[3] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF eMissionStage = MISSION_STAGE_8
		IF bObjectsCreated[3] = FALSE
			REQUEST_MODEL(HAULER)
			REQUEST_MODEL(DOCKTRAILER)
			IF HAS_MODEL_LOADED(HAULER)
			AND HAS_MODEL_LOADED(DOCKTRAILER)
				IF NOT DOES_ENTITY_EXIST(vehFloydTruck)
					SPAWN_VEHICLE(vehFloydTruck,HAULER,<< -117.9839, -2416.6272, 5.0003 >>,91.3366)
				ELSE
					IF NOT DOES_ENTITY_EXIST(vehTrailer2)
						IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
							SET_VEHICLE_COLOURS(vehFloydTruck,0,0)
							SPAWN_VEHICLE(vehTrailer2,DOCKTRAILER,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehFloydTruck, <<-0.3, -9.5, -0.25>>), GET_ENTITY_HEADING(vehFloydTruck))
							ACTIVATE_PHYSICS(vehFloydTruck)
							ACTIVATE_PHYSICS(vehTrailer2)
							SET_VEHICLE_EXTRA(vehTrailer2,1,TRUE)
						ENDIF
					ELSE
						bObjectsCreated[3] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF eMissionStage > MISSION_STAGE_8
		IF bObjectsCreated[2] = FALSE
			REQUEST_MODEL(S_M_M_DOCKWORK_01)
			REQUEST_MODEL(TIPTRUCK)
			IF HAS_MODEL_LOADED(S_M_M_DOCKWORK_01)
			AND HAS_MODEL_LOADED(TIPTRUCK)
				IF NOT DOES_ENTITY_EXIST(vehFinal)
					SPAWN_VEHICLE(vehFinal,TIPTRUCK, << 465.8751, -3037.0837, 5.0686 >>, 126.3433)
				ELSE
					IF NOT DOES_ENTITY_EXIST(pedDockWorker[15])
						createDockWorker(15)
					ELSE
						IF NOT DOES_ENTITY_EXIST(pedDockWorker[16])
							createDockWorker(16)
						ELSE
							bObjectsCreated[2] = TRUE
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
		IF eMissionStage > MISSION_STAGE_1
		
			IF eMissionStage = MISSION_STAGE_2
				IF bObjectsCreated[0] = TRUE
					CLEANUP_LOADED_MODEL_ARRAY()
					//PRINTSTRING("DOCKS STUFF CREATED")//PRINTNL()
					RETURN TRUE
				ELSE
					//PRINTSTRING("CREATING THE DOCKS STUFF")//PRINTNL()
				ENDIF
			ENDIF
		
			IF eMissionStage = MISSION_STAGE_2
				IF bObjectsCreated[0] = TRUE
					CLEANUP_LOADED_MODEL_ARRAY()
					//PRINTSTRING("DOCKS STUFF CREATED")//PRINTNL()
					
					RETURN TRUE
				ELSE
					//PRINTSTRING("CREATING THE DOCKS STUFF")//PRINTNL()
				ENDIF
			ENDIF
			
			IF eMissionStage = MISSION_STAGE_4
				IF bObjectsCreated[0] = TRUE AND bObjectsCreated[1] = TRUE
					CLEANUP_LOADED_MODEL_ARRAY()
					//PRINTSTRING("DOCKS STUFF CREATED")//PRINTNL()
					RETURN TRUE
				ELSE
					//PRINTSTRING("CREATING THE DOCKS STUFF")//PRINTNL()
				ENDIF
			ENDIF
			
			IF eMissionStage = MISSION_STAGE_5
				IF bObjectsCreated[0] = TRUE AND bObjectsCreated[1] = TRUE
					CLEANUP_LOADED_MODEL_ARRAY()
					//PRINTSTRING("DOCKS STUFF CREATED")//PRINTNL()
					RETURN TRUE
				ELSE
					//PRINTSTRING("CREATING THE DOCKS STUFF")//PRINTNL()
				ENDIF
			ENDIF
			
			IF eMissionStage = MISSION_STAGE_6
				IF bObjectsCreated[0] = TRUE AND bObjectsCreated[1] = TRUE
					CLEANUP_LOADED_MODEL_ARRAY()
					//PRINTSTRING("DOCKS STUFF CREATED")//PRINTNL()
					RETURN TRUE
				ELSE
					//PRINTSTRING("CREATING THE DOCKS STUFF")//PRINTNL()
				ENDIF
			ENDIF
			
			IF eMissionStage = MISSION_STAGE_7
				IF bObjectsCreated[0] = TRUE AND bObjectsCreated[1] = TRUE AND bObjectsCreated[3] = TRUE
					CLEANUP_LOADED_MODEL_ARRAY()
					//PRINTSTRING("DOCKS STUFF CREATED")//PRINTNL()
					RETURN TRUE
				ELSE
					//PRINTSTRING("CREATING THE DOCKS STUFF")//PRINTNL()
				ENDIF
			ENDIF
			
			IF eMissionStage = MISSION_STAGE_8
				IF bObjectsCreated[0] = TRUE AND bObjectsCreated[1] = TRUE AND bObjectsCreated[3] = TRUE
					CLEANUP_LOADED_MODEL_ARRAY()
					//PRINTSTRING("DOCKS STUFF CREATED")//PRINTNL()
					RETURN TRUE
				ELSE
					//PRINTSTRING("CREATING THE DOCKS STUFF")//PRINTNL()
				ENDIF
			ENDIF
			
			IF eMissionStage = MISSION_STAGE_10
				IF bObjectsCreated[0] = TRUE AND bObjectsCreated[1] = TRUE AND bObjectsCreated[2] = TRUE
					CLEANUP_LOADED_MODEL_ARRAY()
					//PRINTSTRING("DOCKS STUFF CREATED")//PRINTNL()
					RETURN TRUE
				ELSE
					//PRINTSTRING("CREATING THE DOCKS STUFF")//PRINTNL()
				ENDIF
			ENDIF

		ENDIF

	RETURN FALSE
ENDFUNC


//PURPOSE:  Makes a vehicle without a driver
FUNC BOOL CREATE_MISSION_VEHICLE(INT index,MODEL_NAMES vehmod,VECTOR vVehloc,FLOAT vehhead)
	//PRINTINT(index)
	////PRINTNL()
	IF NOT DOES_ENTITY_EXIST(mission_veh[index].veh)
		mission_veh[index].veh=CREATE_VEHICLE(vehmod,vVehloc,vehhead)
	ELSE
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[index].veh)
			#IF IS_DEBUG_BUILD
				TEXT_LABEL_23 nameLabel = "VEHICLE "
				nameLabel += index
				SET_VEHICLE_NAME_DEBUG(mission_veh[index].veh ,nameLabel)
				PRINTSTRING("vehicle made")//PRINTNL()
			#ENDIF
			RETURN TRUE
		ENDIF
	ENDIF
	//PRINTSTRING("CREATE_MISSION_VEHICLE - FALSE")//PRINTNL()	
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_MISSION_REQUIREMENT_WITH_LOCATION(MISSION_REQUIREMENT eRequirement, VECTOR vPos, FLOAT fHeading = 0.0)

	SWITCH eRequirement
		
		CASE REQ_HAULER_WITH_TRAILER
			
			REQUEST_MODEL(HAULER)
			REQUEST_MODEL(DOCKTRAILER)
			IF HAS_MODEL_LOADED(HAULER)
			AND HAS_MODEL_LOADED(DOCKTRAILER)
				IF NOT DOES_ENTITY_EXIST(vehFirstHauler)
					SPAWN_VEHICLE(vehFirstHauler,HAULER,<< -117.9839, -2416.6272, 5.0003 >>,91.3366)
				ELSE
					IF NOT DOES_ENTITY_EXIST(vehTrailer1)
						IF IS_VEHICLE_DRIVEABLE(vehFirstHauler)
							SPAWN_VEHICLE(vehTrailer1,DOCKTRAILER,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehFirstHauler, <<-0.3, -9.5, -0.25>>), GET_ENTITY_HEADING(vehFirstHauler))
							SET_VEHICLE_EXTRA(vehTrailer1,1,TRUE)
							SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehFirstHauler,SC_DOOR_FRONT_LEFT,FALSE)
							SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehFirstHauler,SC_DOOR_FRONT_RIGHT,FALSE)
							SET_VEHICLE_AS_RESTRICTED(vehTrailer1,0)
							SET_VEHICLE_AS_RESTRICTED(vehFirstHauler,1)
							//PRINTSTRING("REQ_HAULER_WITH_TRAILER")
							RETURN TRUE
						ELSE
							//SCRIPT_ASSERT("HAULER NOT DRIVABLE")
						ENDIF
					ELSE
						//PRINTSTRING("REQ_HAULER_WITH_TRAILER")
						IF IS_VEHICLE_DRIVEABLE(vehTrailer1)
							SET_VEHICLE_EXTRA(vehTrailer1,1,TRUE)
						ENDIF
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_CONTAINERS_FOR_HANDLER_SECTION_FINAL
			IF NOT DOES_ENTITY_EXIST(objHandlerContainer[0])
				REQUEST_MODEL(PROP_CONTR_03B_LD)
				IF HAS_MODEL_LOADED(PROP_CONTR_03B_LD)
					objHandlerContainer[0] = CREATE_OBJECT_NO_OFFSET(PROP_CONTR_03B_LD,<< -96.71, -2455.31, 5.02 >>)
					IF DOES_ENTITY_HAVE_PHYSICS(objHandlerContainer[0])
						SET_OBJECT_PHYSICS_PARAMS(objHandlerContainer[0],5000,-1,<<0,0,0>>,<<0,0,0>>)
					ENDIF
					
					SET_ENTITY_ROTATION (objHandlerContainer[0], <<0.000000,0.000000,55.00>>)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(objHandlerContainer[0],TRUE)
				ENDIF
			ELSE
				IF NOT DOES_ENTITY_EXIST(objHandlerContainer[1])
					objHandlerContainer[1] = CREATE_OBJECT_NO_OFFSET(PROP_CONTR_03B_LD,<<-99.99, -2461.02, 5.02 >>)
					IF DOES_ENTITY_HAVE_PHYSICS(objHandlerContainer[1])
						SET_OBJECT_PHYSICS_PARAMS(objHandlerContainer[1],5000,-1,<<0,0,0>>,<<0,0,0>>)
					ENDIF
					SET_ENTITY_ROTATION (objHandlerContainer[1],<<0.03, 0.02, 54.13>>)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(objHandlerContainer[1],TRUE)
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_CONTAINERS_FOR_HANDLER_SECTION_START
			IF NOT DOES_ENTITY_EXIST(objHandlerContainer[0])
				REQUEST_MODEL(PROP_CONTR_03B_LD)
				IF HAS_MODEL_LOADED(PROP_CONTR_03B_LD)
					objHandlerContainer[0] = CREATE_OBJECT_NO_OFFSET(PROP_CONTR_03B_LD,<<42.829399,-2476.966064,4.933219>>)
					IF DOES_ENTITY_HAVE_PHYSICS(objHandlerContainer[0])
						SET_OBJECT_PHYSICS_PARAMS(objHandlerContainer[0],5000,-1,<<0,0,0>>,<<0,0,0>>)
					ENDIF
					
					SET_ENTITY_ROTATION (objHandlerContainer[0], <<0.000000,0.000000,55.248119>>)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(objHandlerContainer[0],TRUE)
				ENDIF
			ELSE
				IF NOT DOES_ENTITY_EXIST(objHandlerContainer[1])
					objHandlerContainer[1] = CREATE_OBJECT_NO_OFFSET(PROP_CONTR_03B_LD,<<39.751469,-2481.112793,4.981983>>)
					IF DOES_ENTITY_HAVE_PHYSICS(objHandlerContainer[1])
						SET_OBJECT_PHYSICS_PARAMS(objHandlerContainer[1],5000,-1,<<0,0,0>>,<<0,0,0>>)
					ENDIF
					SET_ENTITY_ROTATION (objHandlerContainer[1],<<0.000000,0.000000,55.002975>>)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(objHandlerContainer[1],TRUE)
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_HAULER_WITH_TRAILER_FINAL_STAGE
			REQUEST_MODEL(HAULER)
			REQUEST_MODEL(DOCKTRAILER)
			IF HAS_MODEL_LOADED(HAULER)
			AND HAS_MODEL_LOADED(DOCKTRAILER)
				IF NOT DOES_ENTITY_EXIST(vehFloydTruck)
					SPAWN_VEHICLE(vehFloydTruck,HAULER,<<479.47, -3109.19, 6.28>>,180.19)
				ELSE
					IF NOT DOES_ENTITY_EXIST(vehTrailer2)
						IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
							SPAWN_VEHICLE(vehTrailer2,DOCKTRAILER,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehFloydTruck, <<-0.3, -9.5, -0.25>>), GET_ENTITY_HEADING(vehFloydTruck))
							SET_VEHICLE_EXTRA(vehTrailer2,1,FALSE)
							SET_VEHICLE_COLOURS(vehFloydTruck,0,0)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehFloydTruck,FALSE)
							ACTIVATE_PHYSICS(vehFloydTruck)
							ACTIVATE_PHYSICS(vehTrailer2)
							ATTACH_VEHICLE_TO_TRAILER(vehFloydTruck,vehTrailer2)
							SET_VEHICLE_AS_RESTRICTED(vehFloydTruck,2)
							SET_VEHICLE_AS_RESTRICTED(vehTrailer2,3)
							SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehFloydTruck,SC_DOOR_FRONT_LEFT,FALSE)
							SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehFloydTruck,SC_DOOR_FRONT_RIGHT,FALSE)
							//PRINTSTRING("REQ_HAULER_WITH_TRAILER")
							RETURN TRUE
						ELSE
							//SCRIPT_ASSERT("HAULER NOT DRIVABLE")
						ENDIF
					ELSE
						//PRINTSTRING("REQ_HAULER_WITH_TRAILER")
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_GUARDS_AND_FLOYD
			IF DOES_ENTITY_EXIST(pedFloyd)
				IF NOT IS_PED_INJURED(pedFloyd)
					REQUEST_MODEL(S_M_Y_BlackOps_01)
					REQUEST_ANIM_DICT("missheistdockssetup1ig_13@kick_idle")
					IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_13@kick_idle")
						IF DOES_ENTITY_EXIST(pedFloyd)
						AND DOES_ENTITY_EXIST(pedMerc[4])
						AND DOES_ENTITY_EXIST(pedMerc[5])
							IF NOT IS_PED_INJURED(pedFloyd)
							AND NOT IS_PED_INJURED(pedMerc[4])
							AND NOT IS_PED_INJURED(pedMerc[5])
								IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlackAbuse)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFloyd,TRUE)
									REMOVE_PED_FROM_GROUP(pedFloyd)
									sceneBlackAbuse = CREATE_SYNCHRONIZED_SCENE(<< 478.596, -3116.911, 5.069 >>,<< -0.000, -0.000, 53.446 >>)
									TASK_SYNCHRONIZED_SCENE (pedFloyd,sceneBlackAbuse, "missheistdockssetup1ig_13@kick_idle", "guard_beatup_kickidle_dockworker",  REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
									TASK_SYNCHRONIZED_SCENE (pedMerc[4], sceneBlackAbuse,"missheistdockssetup1ig_13@kick_idle", "guard_beatup_kickidle_guard1",  REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
									TASK_SYNCHRONIZED_SCENE (pedMerc[5], sceneBlackAbuse, "missheistdockssetup1ig_13@kick_idle", "guard_beatup_kickidle_guard2", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneBlackAbuse,TRUE)
									RETURN TRUE
								ENDIF
							ENDIF
						ELSE
							IF HAS_MODEL_LOADED(S_M_Y_BlackOps_01)
								IF NOT DOES_ENTITY_EXIST(pedMerc[4])
									createMercenary(4,FALSE)
								ENDIF
								IF NOT DOES_ENTITY_EXIST(pedMerc[5])
									createMercenary(5,FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF CREATE_NPC_PED_ON_FOOT(pedFloyd, CHAR_FLOYD, <<479.753, -3114.320, 5.070>>, 0.0)
					SET_PED_CAN_BE_TARGETTED(pedFloyd, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFloyd, TRUE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedFloyd, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedFloyd, RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedFloyd ,rel_group_buddies)
					SET_ENTITY_PROOFS(pedFloyd, FALSE, FALSE, FALSE, TRUE, FALSE)
					SET_PED_AS_ENEMY(pedFloyd , FALSE)
					SET_PED_CONFIG_FLAG(pedFloyd,PCF_CanActivateRagdollWhenVehicleUpsideDown,TRUE)
					SET_ENTITY_AS_MISSION_ENTITY(pedFloyd)
					ADD_PED_FOR_DIALOGUE(sSpeech, 3, pedFloyd, "FLOYD")
				ENDIF
			ENDIF
		BREAK
		

		CASE REQ_TREVORS_TRUCK
			IF NOT DOES_ENTITY_EXIST(vehMission)
				REQUEST_VEHICLE_ASSET(BODHI2)
				CREATE_PLAYER_VEHICLE(vehMission, CHAR_TREVOR ,vPos, fHeading, TRUE)
			ELSE
				REQUEST_VEHICLE_ASSET(BODHI2)
				IF HAS_VEHICLE_ASSET_LOADED(BODHI2)
					IF IS_VEHICLE_DRIVEABLE(vehMission)
						SET_VEHICLE_HAS_STRONG_AXLES(vehMission,TRUE)
						INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehMission)
						INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehMission)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehMission,TRUE)
						SET_VEHICLE_DISABLE_TOWING(vehMission,TRUE)
						SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehMission,FALSE)
						SET_VEHICLE_CAN_LEAK_PETROL(vehMission,FALSE)
						//PRINTLN("TREVOR TRUCK CREATED")//PRINTNL()
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_FLOYD
			IF NOT DOES_ENTITY_EXIST(pedFloyd)
				IF CREATE_NPC_PED_ON_FOOT(pedFloyd, CHAR_FLOYD, vPos, fHeading)
					SET_PED_CAN_BE_TARGETTED(pedFloyd, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFloyd, TRUE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedFloyd, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedFloyd, RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedFloyd ,rel_group_buddies)
					SET_ENTITY_PROOFS(pedFloyd, FALSE, FALSE, FALSE, TRUE, FALSE)
					SET_PED_AS_ENEMY(pedFloyd , FALSE)
					SET_PED_CONFIG_FLAG(pedFloyd,PCF_CanActivateRagdollWhenVehicleUpsideDown,TRUE)
					SET_ENTITY_AS_MISSION_ENTITY(pedFloyd)
					ADD_PED_FOR_DIALOGUE(sSpeech, 3, pedFloyd, "FLOYD")
					IF eMissionStage = MISSION_STAGE_OPENING_CUTSCENE 
						SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_HEAD, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_BERD, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_HAIR, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_TORSO, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_LEG, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_HAND, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_FEET, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_TEETH, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_SPECIAL, 1, 0)
						SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_SPECIAL2, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_DECL, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_JBIB, 0, 0)
						CLEAR_ALL_PED_PROPS(pedFloyd)
						PRINTSTRING("setting up Floyd's component variation for Opening cutscene for repeat play") PRINTNL()
						PRINTSTRING("CLEAR_ALL_PED_PROPS(pedFloyd)") PRINTNL()
					ENDIF
					//PRINTLN("CREATING FLOYD")//PRINTNL()
					RETURN TRUE
				ENDIF
			ELSE
				IF sSpeech.PedInfo[3].Index != pedFloyd
					ADD_PED_FOR_DIALOGUE(sSpeech, 3, pedFloyd, "FLOYD")
				ENDIF
				IF NOT IS_PED_INJURED(pedFloyd)
					SET_PED_CAN_BE_TARGETTED(pedFloyd, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFloyd, TRUE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedFloyd, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedFloyd, RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedFloyd ,rel_group_buddies)
					SET_ENTITY_PROOFS(pedFloyd, FALSE, FALSE, FALSE, TRUE, FALSE)
					SET_PED_AS_ENEMY(pedFloyd , FALSE)
					SET_PED_CONFIG_FLAG(pedFloyd,PCF_CanActivateRagdollWhenVehicleUpsideDown,TRUE)
					SET_ENTITY_AS_MISSION_ENTITY(pedFloyd)
				ENDIF
				//PRINTLN("CREATING FLOYD")//PRINTNL()
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_WADE
			IF NOT DOES_ENTITY_EXIST(pedWade)
				IF CREATE_NPC_PED_ON_FOOT(pedWade, CHAR_WADE, vPos, fHeading)
					SET_PED_CAN_BE_TARGETTED(pedWade, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWade, TRUE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedWade, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedWade, RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedWade ,rel_group_buddies)
					SET_ENTITY_PROOFS(pedWade, FALSE, FALSE, FALSE, TRUE, FALSE)
					SET_PED_CONFIG_FLAG(pedWade,PCF_CanActivateRagdollWhenVehicleUpsideDown,TRUE)
					SET_PED_AS_ENEMY(pedWade , FALSE)
					SET_ENTITY_AS_MISSION_ENTITY(pedWade)
					ADD_PED_FOR_DIALOGUE(sSpeech, 4, pedWade, "WADE")
					//PRINTLN("CREATED WADE")//PRINTNL()
					RETURN TRUE
				ENDIF
			ELSE
				IF sSpeech.PedInfo[4].Index != pedWade
					ADD_PED_FOR_DIALOGUE(sSpeech, 4, pedWade, "WADE")
				ENDIF
				IF NOT IS_PED_INJURED(pedWade)
					SET_PED_CAN_BE_TARGETTED(pedWade, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWade, TRUE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedWade, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedWade, RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedWade ,rel_group_buddies)
					SET_ENTITY_PROOFS(pedWade, FALSE, FALSE, FALSE, TRUE, FALSE)
					SET_PED_CONFIG_FLAG(pedWade,PCF_CanActivateRagdollWhenVehicleUpsideDown,TRUE)
					SET_PED_AS_ENEMY(pedWade , FALSE)
					SET_ENTITY_AS_MISSION_ENTITY(pedWade)
				ENDIF
				//PRINTLN("CREATED WADE")//PRINTNL()
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_SECURITY
			IF NOT DOES_ENTITY_EXIST(pedSecurity)
				REQUEST_MODEL(S_M_M_DOCKWORK_01)
				IF HAS_MODEL_LOADED(S_M_M_DOCKWORK_01)
					IF NOT DOES_ENTITY_EXIST(pedSecurity)
						SPAWN_PED(pedSecurity,S_M_M_DOCKWORK_01,vSecurity,fSecurity)
						SET_RAGDOLL_BLOCKING_FLAGS(pedSecurity,RBF_PLAYER_IMPACT)
						SET_PED_CAPSULE(pedSecurity,0.4)
						//PRINTLN("CREATED SECURITY")//PRINTNL()
						RETURN TRUE
					ENDIF
				ENDIF
			ELSE
				//PRINTLN("CREATED SECURITY")//PRINTNL()
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_HANDLER
			IF NOT DOES_ENTITY_EXIST(vehHandler)
				REQUEST_VEHICLE_ASSET(HANDLER)
				REQUEST_MODEL(HANDLER)
				IF HAS_MODEL_LOADED(HANDLER)
					SPAWN_VEHICLE(vehHandler,HANDLER,<<-129.77, -2418.28, 6.24>>,183.0850)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehHandler)
					SET_VEHICLE_PROVIDES_COVER(vehHandler,FALSE)
					SET_VEHICLE_AS_RESTRICTED(vehHandler,4)
					RETURN TRUE
				ENDIF
			ELSE
				REQUEST_VEHICLE_ASSET(HANDLER)
				IF HAS_VEHICLE_ASSET_LOADED(HANDLER)
					IF IS_VEHICLE_DRIVEABLE(vehHandler)	
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_CAR_TO_DRIVE_FINAL_STAGE
			IF NOT DOES_ENTITY_EXIST(vanByDocks)
				REQUEST_MODEL(TOWTRUCK)
				IF HAS_MODEL_LOADED(TOWTRUCK)
					SPAWN_VEHICLE(vanByDocks,TOWTRUCK,<<505.8820, -3053.2310, 5.0695>>, 180.3786)
					SET_VEHICLE_NUMBER_PLATE_TEXT(vanByDocks,"5T3ALM3")
					RETURN TRUE
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_MERRYWEATHER_GUARDS_AT_OTHER_DOCKS
			
			IF NOT DOES_ENTITY_EXIST(pedMerc[4])
			AND NOT DOES_ENTITY_EXIST(pedMerc[5])
			AND NOT DOES_ENTITY_EXIST(pedMerc[6])
				REQUEST_MODEL(S_M_Y_BlackOps_01)
				IF HAS_MODEL_LOADED(S_M_Y_BlackOps_01)
					createMercenary(4,FALSE)
					createMercenary(5,FALSE)
					createMercenary(6,FALSE)
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(pedMerc[4])
					TASK_START_SCENARIO_IN_PLACE(pedMerc[4],"WORLD_HUMAN_GUARD_STAND_ARMY")
				ENDIF
				IF NOT IS_PED_INJURED(pedMerc[5])
					TASK_START_SCENARIO_IN_PLACE(pedMerc[5],"WORLD_HUMAN_GUARD_STAND_ARMY")
				ENDIF
				IF NOT IS_PED_INJURED(pedMerc[6])
					TASK_START_SCENARIO_IN_PLACE(pedMerc[6],"WORLD_HUMAN_GUARD_STAND")
				ENDIF
				RETURN TRUE
			ENDIF

		BREAK
		

		CASE REQ_CRANE
		
			IF IS_VECTOR_ZERO(vPos)
			
			ENDIF
			
			IF fHeading > 0
			
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST(s_crane.obj_main)
				REQUEST_MODEL(model_crane)
				REQUEST_MODEL(model_spreader)
				REQUEST_MODEL(model_cabin)
				REQUEST_MODEL(model_wheel_left)
				REQUEST_MODEL(model_wheel_right)
				REQUEST_MODEL(model_boom)
				REQUEST_MODEL(model_container)
				REQUEST_MODEL(model_left_door)
				REQUEST_MODEL(model_right_door)
				REQUEST_COLLISION_FOR_MODEL(model_spreader)
				REQUEST_ANIM_DICT("missheistdockssetup1leadinoutlsdhs_mcs_1_p2")
				ROPE_LOAD_TEXTURES()
				
				
				IF HAS_MODEL_LOADED(model_crane)
				AND HAS_MODEL_LOADED(model_spreader)
				AND HAS_MODEL_LOADED(model_cabin)
				AND HAS_MODEL_LOADED(model_container)
				AND HAS_MODEL_LOADED(model_left_door)
				AND HAS_MODEL_LOADED(model_right_door)
				AND HAS_MODEL_LOADED(model_wheel_left)
				AND HAS_MODEL_LOADED(model_wheel_right)
				AND HAS_MODEL_LOADED(model_boom)
				AND REQUEST_AMBIENT_AUDIO_BANK("Crane")
				AND REQUEST_AMBIENT_AUDIO_BANK("Crane_Stress")
				AND REQUEST_AMBIENT_AUDIO_BANK("Crane_Impact_Sweeteners")
				AND HAS_ANIM_DICT_LOADED("missheistdockssetup1leadinoutlsdhs_mcs_1_p2")
				AND ROPE_ARE_TEXTURES_LOADED()
					
					REMOVE_IPL("pcranecont")
					//Main crane
					s_crane.v_start_pos = v_crane_pos
					s_crane.obj_main = CREATE_OBJECT(model_crane, s_crane.v_start_pos)
					SET_ENTITY_HEADING(s_crane.obj_main, 180)
					FREEZE_ENTITY_POSITION(s_crane.obj_main, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(model_crane)
					
					//Cabin
					//s_crane.v_cabin_attach_offset = <<9.028, 0.0, 33.65>>
					//P_Dock_RTG_LD_Cab
					s_crane.obj_cabin = CREATE_OBJECT(model_cabin, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_main, s_crane.v_cabin_attach_offset))
					SET_ENTITY_COORDS_NO_OFFSET(s_crane.obj_cabin, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_main, s_crane.v_cabin_attach_offset))
					SET_ENTITY_HEADING(s_crane.obj_cabin, GET_ENTITY_HEADING(s_crane.obj_main))
					FREEZE_ENTITY_POSITION(s_crane.obj_cabin, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(model_cabin)
					
					//Spreader and helper: the spreader is attached via ropes to an invisible helper object (currently no way to resize rope attachments, so instead
					//the invisible object is moved)
					s_crane.v_spreader_attach_offset = <<0.0, 0.0, SPREADER_START_OFFSET>>
					//s_crane.obj_spreader = CREATE_OBJECT(model_spreader, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, s_crane.v_spreader_attach_offset))
					s_crane.obj_spreader = CREATE_OBJECT_NO_OFFSET(model_spreader, vSpreaderPosition)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(s_crane.obj_spreader, FALSE)
					//DAMPENING
					SET_OBJECT_PHYSICS_PARAMS(s_crane.obj_spreader, -1, -1, <<-1,-1,-1>>, <<1,2.3,0.7>>)
					//NO SWING
					//FREEZE_ENTITY_POSITION(s_crane.obj_spreader, TRUE)
					ATTACH_ENTITY_TO_ENTITY(s_crane.obj_spreader, s_crane.obj_cabin, -1, s_crane.v_spreader_attach_offset, <<0.0, 0.0, -90.0>>)
					DETACH_ENTITY(s_Crane.obj_Spreader, TRUE, FALSE)	
					
					v_helper_offset = s_crane.v_spreader_attach_offset - <<0.0, 0.0, SPREADER_START_OFFSET>>
					//NO SWING
					//s_crane.obj_helper = CREATE_OBJECT(model_spreader, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, v_helper_offset))
					s_crane.obj_helper = CREATE_OBJECT(model_spreader, vSpreaderPosition)
					
					SET_ENTITY_HEADING(s_crane.obj_helper, GET_ENTITY_HEADING(s_crane.obj_cabin))
					FREEZE_ENTITY_POSITION(s_crane.obj_helper, TRUE)
					SET_ENTITY_VISIBLE(s_crane.obj_helper, FALSE)
					SET_ENTITY_COLLISION(s_crane.obj_helper,FALSE)
					//SET_ENTITY_VISIBLE(s_crane.obj_helper, TRUE)
					SET_MODEL_AS_NO_LONGER_NEEDED(model_spreader)
									
					s_crane.i_dampingframes = 0
					//15.26
					
					//New Wheel positions
	//							____________________
	//							|6                2|
	//							|7                3|
	//							|                  |
	//							|                  |
	//							|                  |
	//							|4                1|
	//                          _5________________0_ 

					//Set 1 0-1
//					s_crane.v_wheel_offsets[0] = <<4.800, 9.300, 0.680>>
//					s_crane.v_wheel_offsets[1] = <<3.400, 9.300, 0.680>>
//					//Set 2 2-3
//					s_crane.v_wheel_offsets[2] = <<-4.900, 9.300, 0.680>>
//					s_crane.v_wheel_offsets[3] = <<-3.500, 9.300, 0.680>>
//					//Set 3 4-5
//					s_crane.v_wheel_offsets[4] = <<4.800, -9.300, 0.680>>
//					s_crane.v_wheel_offsets[5] = <<3.400, -9.300, 0.680>>
//					//Set 4 6-7
//					s_crane.v_wheel_offsets[6] = <<-4.900, -9.300, 0.680>>
//					s_crane.v_wheel_offsets[7] = <<-3.500, -9.300, 0.680>>
					
					s_crane.v_wheel_offsets[0] = <<4.700, 9.450, 0.680>>
					s_crane.v_wheel_offsets[1] = <<3.300, 9.450, 0.680>>
					//Set 2 2-3
					s_crane.v_wheel_offsets[2] = <<-5.000, 9.450, 0.680>>
					s_crane.v_wheel_offsets[3] = <<-3.600, 9.450, 0.680>>
					//Set 3 4-5
					s_crane.v_wheel_offsets[4] = <<4.700, -9.025, 0.680>>
					s_crane.v_wheel_offsets[5] = <<3.300, -9.025, 0.680>>
					//Set 4 6-7
					s_crane.v_wheel_offsets[6] = <<-5.000, -9.025, 0.680>>
					s_crane.v_wheel_offsets[7] = <<-3.600, -9.025, 0.680>>
					
					INT i
					
					REPEAT COUNT_OF(s_crane.obj_wheels) i
						
						IF i > 3
							model_wheel = model_wheel_right
							s_crane.obj_wheels[i] = CREATE_OBJECT_NO_OFFSET(model_wheel, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_main, s_crane.v_wheel_offsets[i]))
							SET_ENTITY_HEADING(s_crane.obj_wheels[i], GET_ENTITY_HEADING(s_crane.obj_main)-180)
						ELSE
							s_crane.obj_wheels[i] = CREATE_OBJECT_NO_OFFSET(model_wheel, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_main, s_crane.v_wheel_offsets[i]))
							SET_ENTITY_HEADING(s_crane.obj_wheels[i], GET_ENTITY_HEADING(s_crane.obj_main))
						ENDIF
					ENDREPEAT
					
					REPEAT COUNT_OF(s_crane.obj_wheels) i
						//FREEZE_ENTITY_POSITION(s_crane.obj_wheels[i], FALSE)
						ATTACH_ENTITY_TO_ENTITY(s_crane.obj_wheels[i], s_crane.obj_main, -1, s_crane.v_wheel_offsets[i], <<0.0, 0.0, 0.0>>)
					ENDREPEAT
					
					SET_MODEL_AS_NO_LONGER_NEEDED(model_wheel_left)
					SET_MODEL_AS_NO_LONGER_NEEDED(model_wheel_right)

					//Setup crane sound ids
					s_crane.i_cabin_sound_id = GET_SOUND_ID()
					s_crane.i_crane_sound_id = GET_SOUND_ID()
					s_crane.i_spreader_sound_id = GET_SOUND_ID()
					s_crane.i_lift_sound_id = GET_SOUND_ID()	
					s_crane.i_strain_sound_id = GET_SOUND_ID()
					s_crane.i_spreader_fail_sound_id = GET_SOUND_ID()
					
					s_crane.b_spreader_stuck_on_ground = FALSE
					
				ENDIF
			ELIF NOT bAddedRopes
				IF DOES_ENTITY_HAVE_PHYSICS(s_Crane.obj_spreader)
				AND DOES_ENTITY_HAVE_PHYSICS(s_Crane.obj_cabin)
					//FLOAT fRopeStartLength
					//fRopeStartLength = -SPREADER_START_OFFSET - 4.0
					
					//Fudge for ropes not working on Z-skip: the code needs physics on the cabin, so load the scene and unfreeze the object, then freeze it again
					//after attaching the ropes.
					IF bIsJumpingDirectlyToStage
					AND eMissionStage > MISSION_STAGE_6
						FREEZE_ENTITY_POSITION(s_Crane.obj_Cabin, FALSE)
					
						SET_ENTITY_COORDS(PLAYER_PED_ID(), s_crane.v_start_pos)
						LOAD_SCENE(s_crane.v_start_pos)
						
						WAIT(500)
					ENDIF	
					
					s_crane.ropes[0] = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, <<-1.0, -2.7, -1.78>>), //-1.78 //-1.75
												<<0.0, 0.0, 0.0>>, 15.26, PHYSICS_ROPE_THIN, -SPREADER_START_OFFSET , f_ropes_min_len, f_ropes_len_change_rate ) //15.26
					s_crane.ropes[1] = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, <<-1.0, 2.7, -1.78>>),
												<<0.0, 0.0, 0.0>>, 15.26, PHYSICS_ROPE_THIN, -SPREADER_START_OFFSET , f_ropes_min_len, f_ropes_len_change_rate )
					s_crane.ropes[2] = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, <<1.0, 2.7, -1.78>>),
												<<0.0, 0.0, 0.0>>, 15.26, PHYSICS_ROPE_THIN, -SPREADER_START_OFFSET , f_ropes_min_len, f_ropes_len_change_rate )
					s_crane.ropes[3] = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin,<<1.0, 2.7, -1.78>>),
												<<0.0, 0.0, 0.0>>, 15.26, PHYSICS_ROPE_THIN, -SPREADER_START_OFFSET , f_ropes_min_len, f_ropes_len_change_rate )
												
					s_crane.ropes[4] = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, <<-0.05, -2.7, -1.78>>),
												<<0.0, 0.0, 0.0>>, 15.26, PHYSICS_ROPE_THIN, -SPREADER_START_OFFSET , f_ropes_min_len, f_ropes_len_change_rate )
					s_crane.ropes[5] = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, <<-0.05, 2.7, -1.78>>),
												<<0.0, 0.0, 0.0>>, 15.26, PHYSICS_ROPE_THIN, -SPREADER_START_OFFSET , f_ropes_min_len, f_ropes_len_change_rate )
					s_crane.ropes[6] = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, <<0.05, -2.7, -1.78>>),
												<<0.0, 0.0, 0.0>>, 15.26, PHYSICS_ROPE_THIN, -SPREADER_START_OFFSET , f_ropes_min_len, f_ropes_len_change_rate )
					s_crane.ropes[7] = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, <<0.05, 2.7, -1.78>>),
												<<0.0, 0.0, 0.0>>, 15.26, PHYSICS_ROPE_THIN, -SPREADER_START_OFFSET, f_ropes_min_len, f_ropes_len_change_rate )

					ROPE_SET_SMOOTH_REELIN( s_crane.ropes[0], TRUE )
					ROPE_SET_SMOOTH_REELIN( s_crane.ropes[1], TRUE )
					ROPE_SET_SMOOTH_REELIN( s_crane.ropes[2], TRUE )
					ROPE_SET_SMOOTH_REELIN( s_crane.ropes[3], TRUE )
					ROPE_SET_SMOOTH_REELIN( s_crane.ropes[4], TRUE )
					ROPE_SET_SMOOTH_REELIN( s_crane.ropes[5], TRUE )
					ROPE_SET_SMOOTH_REELIN( s_crane.ropes[6], TRUE )
					ROPE_SET_SMOOTH_REELIN( s_crane.ropes[7], TRUE )
					
					ROPE_SET_UPDATE_ORDER( s_crane.ropes[0], 2 )
					ROPE_SET_UPDATE_ORDER( s_crane.ropes[1], 2 )
					ROPE_SET_UPDATE_ORDER( s_crane.ropes[2], 2 )
					ROPE_SET_UPDATE_ORDER( s_crane.ropes[3], 2 )
					ROPE_SET_UPDATE_ORDER( s_crane.ropes[4], 2 )
					ROPE_SET_UPDATE_ORDER( s_crane.ropes[5], 2 )
					ROPE_SET_UPDATE_ORDER( s_crane.ropes[6], 2 )
					ROPE_SET_UPDATE_ORDER( s_crane.ropes[7], 2 )
					
																	
					ATTACH_ENTITIES_TO_ROPE(s_crane.ropes[0], s_crane.obj_cabin, s_crane.obj_spreader,
											GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, vRopeTop1),
											GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, vRopeBottom1), -SPREADER_START_OFFSET , 0, 0)
											
					ATTACH_ENTITIES_TO_ROPE(s_crane.ropes[1], s_crane.obj_cabin, s_crane.obj_spreader,
											GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, vRopeTop2),
											GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, vRopeBottom2), -SPREADER_START_OFFSET , 0, 0)
											
					ATTACH_ENTITIES_TO_ROPE(s_crane.ropes[2], s_crane.obj_cabin, s_crane.obj_spreader,
											GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, vRopeTop3),
											GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, vRopeBottom3), -SPREADER_START_OFFSET, 0, 0)
											
					ATTACH_ENTITIES_TO_ROPE(s_crane.ropes[3], s_crane.obj_cabin, s_crane.obj_spreader,
											GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, vRopeTop4),
											GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, vRopeBottom4), -SPREADER_START_OFFSET, 0, 0)
											
					ATTACH_ENTITIES_TO_ROPE(s_crane.ropes[4], s_crane.obj_cabin, s_crane.obj_spreader,
											GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, vRopeTop5),
											GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, vRopeBottom5), -SPREADER_START_OFFSET , 0, 0)
											
					ATTACH_ENTITIES_TO_ROPE(s_crane.ropes[5], s_crane.obj_cabin, s_crane.obj_spreader,
											GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, vRopeTop6),
											GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, vRopeBottom6), -SPREADER_START_OFFSET , 0, 0)
											
					ATTACH_ENTITIES_TO_ROPE(s_crane.ropes[6], s_crane.obj_cabin, s_crane.obj_spreader,
											GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, vRopeTop7),
											GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, vRopeBottom7), -SPREADER_START_OFFSET, 0, 0)
											
					ATTACH_ENTITIES_TO_ROPE(s_crane.ropes[7], s_crane.obj_cabin, s_crane.obj_spreader,
											GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, vRopeTop8),
											GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, vRopeBottom8), -SPREADER_START_OFFSET, 0, 0)
					
					//CRANE GUY STUFF
					IF eMissionStage != MISSION_STAGE_8
						IF DOES_ENTITY_EXIST(pedDockWorker[14])
							IF NOT IS_PED_INJURED(pedDockWorker[14])
								IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(ssCraneWorker)
									ssCraneWorker = CREATE_SYNCHRONIZED_SCENE(<< 3.050, -0.350, 0.350 >>, << 0.000, 0.000, 180.000 >>)
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssCraneWorker,s_crane.obj_cabin,-1)
									//ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssCraneWorker,s_crane.obj_helper,-1)
									TASK_SYNCHRONIZED_SCENE (pedDockWorker[14],ssCraneWorker, "missheistdockssetup1leadinoutlsdhs_mcs_1_p2", "leadin_idle_b", INSTANT_BLEND_IN, INSTANT_BLEND_OUT , SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT)
									SET_ENTITY_NO_COLLISION_ENTITY(pedDockWorker[14],s_crane.obj_cabin,FALSE)
									SET_SYNCHRONIZED_SCENE_LOOPED(ssCraneWorker,TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					bAddedRopes = TRUE
					
					//NO SWING
					FREEZE_ENTITY_POSITION(s_crane.obj_spreader, FALSE)
										
					APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(s_crane.obj_spreader, APPLY_TYPE_IMPULSE, <<0.0, 0.0, 0.01>>, 0, FALSE, TRUE)	
				
					
					IF bIsJumpingDirectlyToStage
					AND eMissionStage > MISSION_STAGE_4
						FREEZE_ENTITY_POSITION(s_crane.obj_cabin, TRUE)
					ENDIF
													
					RETURN TRUE
				ELSE
					PRINTSTRING("AWAITING PHYSICS ON CRANE PROPS")//PRINTNL()
					IF NOT DOES_ENTITY_HAVE_PHYSICS(s_Crane.obj_spreader)
						PRINTSTRING("NO PHYSICS ON SPREADER")//PRINTNL()
					ENDIF
					IF NOT DOES_ENTITY_HAVE_PHYSICS(s_Crane.obj_cabin)
						PRINTSTRING("NO PHYSICS ON CABIN")//PRINTNL()
					ENDIF
				ENDIF
			ELSE
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_CRANE_CONTAINERS
			REQUEST_MODEL(model_container)
			REQUEST_MODEL(model_left_door)
			REQUEST_MODEL(model_right_door)
			IF HAS_MODEL_LOADED(model_container)
			AND HAS_MODEL_LOADED(model_left_door)
			AND HAS_MODEL_LOADED(model_right_door)
			
				//Containers		
				IF NOT DOES_ENTITY_EXIST( s_containers[0].obj_main)
					s_containers[0].obj_main = CREATE_OBJECT_NO_OFFSET(model_container, << -53.2547, -2412.8684, 5.0007 >>)
					SET_ENTITY_HEADING(s_containers[0].obj_main, 91.2705)
					FREEZE_ENTITY_POSITION(s_containers[0].obj_main, TRUE)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST( s_containers[0].obj_left_door)
					s_containers[0].obj_left_door = CREATE_OBJECT(model_left_door, << -53.2547, -2411.8684, 5.0007 >>)
					SET_ENTITY_COORDS_NO_OFFSET(s_containers[0].obj_left_door, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_containers[0].obj_main, v_left_door_offset))
					SET_ENTITY_HEADING(s_containers[0].obj_left_door, GET_ENTITY_HEADING(s_containers[0].obj_main) - 150.0)
					FREEZE_ENTITY_POSITION(s_containers[0].obj_left_door, TRUE)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST( s_containers[0].obj_right_door)
					s_containers[0].obj_right_door = CREATE_OBJECT(model_right_door, << -53.2547, -2413.8684, 5.0007 >>)
					SET_ENTITY_COORDS_NO_OFFSET(s_containers[0].obj_right_door, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_containers[0].obj_main, v_right_door_offset))
					SET_ENTITY_HEADING(s_containers[0].obj_right_door, GET_ENTITY_HEADING(s_containers[0].obj_main) + 150.0)
					FREEZE_ENTITY_POSITION(s_containers[0].obj_right_door, TRUE)
				ENDIF
				
				//99.2664
				IF NOT DOES_ENTITY_EXIST( s_containers[1].obj_main)
					s_containers[1].obj_main = CREATE_OBJECT_NO_OFFSET(model_container, << -52.8189, -2418.3357, 5.0009 >>)
					SET_ENTITY_HEADING(s_containers[1].obj_main, 91.2705)
					FREEZE_ENTITY_POSITION(s_containers[1].obj_main, TRUE)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST( s_containers[1].obj_left_door)
					s_containers[1].obj_left_door = CREATE_OBJECT(model_left_door, << -52.8189, -2417.3357, 5.0009 >>)
					SET_ENTITY_COORDS_NO_OFFSET(s_containers[1].obj_left_door, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_containers[1].obj_main, v_left_door_offset))
					SET_ENTITY_HEADING(s_containers[1].obj_left_door, GET_ENTITY_HEADING(s_containers[0].obj_main) - 150.0)
					FREEZE_ENTITY_POSITION(s_containers[1].obj_left_door, TRUE)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST( s_containers[1].obj_right_door)
					s_containers[1].obj_right_door = CREATE_OBJECT(model_right_door,  << -52.8189, -2415.3357, 5.0009 >>)
					SET_ENTITY_COORDS_NO_OFFSET(s_containers[1].obj_right_door, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_containers[1].obj_main, v_right_door_offset))
					SET_ENTITY_HEADING(s_containers[1].obj_right_door, GET_ENTITY_HEADING(s_containers[0].obj_main) + 150.0)
					FREEZE_ENTITY_POSITION(s_containers[1].obj_right_door, TRUE)
				ENDIF
				
				//Close container doors
				IF DOES_ENTITY_EXIST( s_containers[0].obj_main)
					IF DOES_ENTITY_EXIST(s_containers[0].obj_left_door)
						ATTACH_ENTITY_TO_ENTITY(s_containers[0].obj_left_door, s_containers[0].obj_main, -1, v_left_door_offset, <<0.0, 0.0, 0.0>>,FALSE,FALSE,TRUE)
						SET_ENTITY_NO_COLLISION_ENTITY(s_containers[0].obj_left_door, s_containers[0].obj_main,TRUE)
					ENDIF
					
					IF DOES_ENTITY_EXIST(s_containers[0].obj_right_door)
						ATTACH_ENTITY_TO_ENTITY(s_containers[0].obj_right_door, s_containers[0].obj_main, -1, v_right_door_offset, <<0.0, 0.0, 0.0>>,FALSE,FALSE,TRUE)
						SET_ENTITY_NO_COLLISION_ENTITY(s_containers[0].obj_right_door, s_containers[0].obj_main,TRUE)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST( s_containers[1].obj_main)
				AND DOES_ENTITY_EXIST( s_containers[0].obj_main)
					IF DOES_ENTITY_EXIST(s_containers[1].obj_left_door)
					AND DOES_ENTITY_EXIST(s_containers[0].obj_left_door)
						ATTACH_ENTITY_TO_ENTITY(s_containers[1].obj_left_door, s_containers[1].obj_main, -1, v_left_door_offset, <<0.0, 0.0, 0.0>>,FALSE,FALSE,TRUE)
						SET_ENTITY_NO_COLLISION_ENTITY(s_containers[1].obj_left_door, s_containers[1].obj_main,TRUE)
					ENDIF
					IF DOES_ENTITY_EXIST(s_containers[1].obj_right_door)
					AND DOES_ENTITY_EXIST(s_containers[1].obj_right_door)
						ATTACH_ENTITY_TO_ENTITY(s_containers[1].obj_right_door, s_containers[1].obj_main, -1, v_right_door_offset, <<0.0, 0.0, 0.0>>,FALSE,FALSE,TRUE)
						SET_ENTITY_NO_COLLISION_ENTITY(s_containers[1].obj_right_door, s_containers[1].obj_main,TRUE)
					ENDIF
					RETURN TRUE
				ENDIF
				
				SET_MODEL_AS_NO_LONGER_NEEDED(model_container)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_left_door) 
				SET_MODEL_AS_NO_LONGER_NEEDED(model_right_door)
			ENDIF
		BREAK
		
	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SETUP_MISSION_REQUIREMENT(MISSION_REQUIREMENT eRequirement)
	RETURN SETUP_MISSION_REQUIREMENT_WITH_LOCATION(eRequirement, <<0.0, 0.0, 0.0>>)
ENDFUNC

PROC SWITCH_CRANE_VISIBILITY(BOOL bVisible)
	
	IF DOES_ENTITY_EXIST(s_crane.obj_spreader)
		SET_ENTITY_VISIBLE(s_crane.obj_spreader,bVisible)
	ENDIF
	IF DOES_ENTITY_EXIST(s_crane.obj_main)
		SET_ENTITY_VISIBLE(s_crane.obj_main,bVisible)
	ENDIF
	IF DOES_ENTITY_EXIST(s_crane.obj_cabin)
		SET_ENTITY_VISIBLE(s_crane.obj_cabin,bVisible)
	ENDIF
	IF DOES_ENTITY_EXIST(s_crane.obj_lift)
		SET_ENTITY_VISIBLE(s_crane.obj_lift,bVisible)
	ENDIF
	IF DOES_ENTITY_EXIST(s_crane.obj_boom_cable)
		SET_ENTITY_VISIBLE(s_crane.obj_boom_cable,bVisible)
	ENDIF
	
	IF DOES_ENTITY_EXIST(s_crane.obj_spreader)
		SET_ENTITY_COORDS(s_crane.obj_spreader,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, s_crane.v_spreader_attach_offset) - <<0.0, 0.0, 1.0>> )
	ENDIF
	
	IF DOES_ROPE_EXIST(s_crane.ropes[0])
		//SET_ENTITY_VISIBLE(s_crane.ropes[0],bVisible)
	ENDIF
	IF DOES_ROPE_EXIST(s_crane.ropes[1])
		//SET_ENTITY_VISIBLE(s_crane.ropes[1],bVisible)
	ENDIF
	IF DOES_ROPE_EXIST(s_crane.ropes[2])
		//SET_ENTITY_VISIBLE(s_crane.ropes[2],bVisible)
	ENDIF
	IF DOES_ROPE_EXIST(s_crane.ropes[3])
		//SET_ENTITY_VISIBLE(s_crane.ropes[3],bVisible)
	ENDIF
	IF DOES_ROPE_EXIST(s_crane.ropes[4])
		//SET_ENTITY_VISIBLE(s_crane.ropes[4],bVisible)
	ENDIF
	IF DOES_ROPE_EXIST(s_crane.ropes[5])
		//SET_ENTITY_VISIBLE(s_crane.ropes[5],bVisible)
	ENDIF
	IF DOES_ROPE_EXIST(s_crane.ropes[6])
		//SET_ENTITY_VISIBLE(s_crane.ropes[6],bVisible)
	ENDIF
	IF DOES_ROPE_EXIST(s_crane.ropes[7])
		//SET_ENTITY_VISIBLE(s_crane.ropes[7],bVisible)
	ENDIF
	
	IF DOES_ENTITY_EXIST(s_crane.obj_wheels[0])
		SET_ENTITY_VISIBLE(s_crane.obj_wheels[0],bVisible)
	ENDIF
	IF DOES_ENTITY_EXIST(s_crane.obj_wheels[1])
		SET_ENTITY_VISIBLE(s_crane.obj_wheels[1],bVisible)
	ENDIF
	IF DOES_ENTITY_EXIST(s_crane.obj_wheels[2])
		SET_ENTITY_VISIBLE(s_crane.obj_wheels[2],bVisible)
	ENDIF
	IF DOES_ENTITY_EXIST(s_crane.obj_wheels[3])
		SET_ENTITY_VISIBLE(s_crane.obj_wheels[3],bVisible)
	ENDIF
	IF DOES_ENTITY_EXIST(s_crane.obj_wheels[4])
		SET_ENTITY_VISIBLE(s_crane.obj_wheels[4],bVisible)
	ENDIF
	IF DOES_ENTITY_EXIST(s_crane.obj_wheels[5])
		SET_ENTITY_VISIBLE(s_crane.obj_wheels[5],bVisible)
	ENDIF
	IF DOES_ENTITY_EXIST(s_crane.obj_wheels[6])
		SET_ENTITY_VISIBLE(s_crane.obj_wheels[6],bVisible)
	ENDIF
	IF DOES_ENTITY_EXIST(s_crane.obj_wheels[7])
		SET_ENTITY_VISIBLE(s_crane.obj_wheels[7],bVisible)
	ENDIF
	
ENDPROC

// ======================================================================
//                                 Load Stages 
// ======================================================================

//PURPOSE:  PreLoads the next stage when the player get near to it.
FUNC BOOL PRE_STREAM_MISSION_STAGE()
	
	MISSION_STAGE_ENUM eStageToLoad = INT_TO_ENUM(MISSION_STAGE_ENUM, (ENUM_TO_INT(eMissionStage) + 1))
		
	SWITCH iPreStreamStage
	
		CASE 0
			IF eStageToLoad = MISSION_STAGE_SETUP
				iPreStreamStage++
			ELIF eStageToLoad = MISSION_STAGE_OPENING_CUTSCENE
				iPreStreamStage++
			ELIF eStageToLoad = MISSION_STAGE_1
				iPreStreamStage++
			//PLAYER PARKS UP AND GET'S CHANGED
			ELIF eStageToLoad = MISSION_STAGE_2
				//ADD_MODEL_REQUEST_TO_ARRAY(S_M_Y_BlackOps_01)
				//ADD_MODEL_REQUEST_TO_ARRAY(S_M_M_DOCKWORK_01)
				//ADD_MODEL_REQUEST_TO_ARRAY(BIFF)
				//ADD_MODEL_REQUEST_TO_ARRAY(FORKLIFT)
				REQUEST_VEHICLE_RECORDING(1,"AWDocks4")
				REQUEST_VEHICLE_RECORDING(1,"AWDocks5")
				iPreStreamStage++
			//CUTSCENE GET CHANGED
//			ELIF eStageToLoad = MISSION_STAGE_3
//				iPreStreamStage++
			//FOLLOW floyd AROUND DOCKS
			ELIF eStageToLoad = MISSION_STAGE_4
				CLEANUP_LOADED_MODEL_ARRAY()
				REQUEST_WAYPOINT_RECORDING("floyddocks1") 
				REQUEST_WAYPOINT_RECORDING("docksplayer1")
				REQUEST_VEHICLE_RECORDING(1,"AWDock1")
				REQUEST_VEHICLE_RECORDING(1,"AWDocks2")
				iPreStreamStage++
			//FORKLIFT AND TASKS
			ELIF eStageToLoad = MISSION_STAGE_5
				REQUEST_VEHICLE_RECORDING(1,"AWDocks3")
				iPreStreamStage++
			//FOLLOW floyd TO CRANE
			ELIF eStageToLoad = MISSION_STAGE_6
				CLEANUP_LOADED_MODEL_ARRAY()
				ADD_MODEL_REQUEST_TO_ARRAY(Prop_Dock_RTG_LD)
				ADD_MODEL_REQUEST_TO_ARRAY(P_Dock_RTG_LD_spdr)
				ADD_MODEL_REQUEST_TO_ARRAY(P_Dock_RTG_LD_Cab)
				ADD_MODEL_REQUEST_TO_ARRAY(PROP_CONTAINER_LD) //PROP_CONTAINER_LD_D
				ADD_MODEL_REQUEST_TO_ARRAY(P_Dock_RTG_LD_wheel)
				ADD_MODEL_REQUEST_TO_ARRAY(P_Dock_RTG_LD_wheel)
				//ADD_MODEL_REQUEST_TO_ARRAY(P_DOCK_CRANE_CABLES)
				ADD_MODEL_REQUEST_TO_ARRAY(PROP_CNTRDOOR_LD_L)
				ADD_MODEL_REQUEST_TO_ARRAY(PROP_CNTRDOOR_LD_R)
				iPreStreamStage++
		    //CRANE SECTION
			ELIF eStageToLoad = MISSION_STAGE_7
				CLEANUP_LOADED_MODEL_ARRAY()
				REQUEST_VEHICLE_RECORDING(2,"AWDocks1")
				ADD_MODEL_REQUEST_TO_ARRAY(BIFF)
				ADD_MODEL_REQUEST_TO_ARRAY(HAULER)
				ADD_MODEL_REQUEST_TO_ARRAY(DOCKTRAILER)
				iPreStreamStage++
			//USE CAMERA
			ELIF eStageToLoad = MISSION_STAGE_8
				CLEANUP_LOADED_MODEL_ARRAY()
				ADD_MODEL_REQUEST_TO_ARRAY(BIFF)
				ADD_MODEL_REQUEST_TO_ARRAY(HAULER)
				ADD_MODEL_REQUEST_TO_ARRAY(DOCKTRAILER)
				iPreStreamStage++
			//DRIVE TO OTHER DOCKS
			ELIF eStageToLoad = MISSION_STAGE_10
				CLEANUP_LOADED_MODEL_ARRAY()
				ADD_MODEL_REQUEST_TO_ARRAY(HAULER)
				ADD_MODEL_REQUEST_TO_ARRAY(DOCKTRAILER)
				ADD_MODEL_REQUEST_TO_ARRAY(TIPTRUCK)
				//ADD_MODEL_REQUEST_TO_ARRAY(SUBMARINE)
				ADD_MODEL_REQUEST_TO_ARRAY(S_M_Y_BlackOps_01)
				ADD_MODEL_REQUEST_TO_ARRAY(S_M_M_DOCKWORK_01)
				iPreStreamStage++

			ENDIF
		BREAK
		
		CASE 1
			IF ARE_REQUESTED_MODELS_LOADED()
			AND ARE_REQUESTED_CARRECS_LOADED("AWDocks1")
			//AND ARE_REQUESTED_ANIMS_LOADED()
				iPreStreamStage++
			ENDIF
		BREAK
		
		CASE 2
			IF eStageToLoad = MISSION_STAGE_SETUP
				iPreStreamStage++
			ELIF eStageToLoad = MISSION_STAGE_OPENING_CUTSCENE
				iPreStreamStage++
			ELIF eStageToLoad = MISSION_STAGE_1
				iPreStreamStage++
			//PLAYER PARKS UP AND GET'S CHANGED
			ELIF eStageToLoad = MISSION_STAGE_2
				iPreStreamStage++
			//CUTSCENE GET CHANGED
//			ELIF eStageToLoad = MISSION_STAGE_3
//				iPreStreamStage++
			//FOLLOW floyd AROUND DOCKS
			ELIF eStageToLoad = MISSION_STAGE_4
				IF  GET_IS_WAYPOINT_RECORDING_LOADED("floyddocks1")
				AND GET_IS_WAYPOINT_RECORDING_LOADED("docksplayer1")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDock1")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDocks2")
					iPreStreamStage++
				ENDIF
			//FORKLIFT AND TASKS
			ELIF eStageToLoad = MISSION_STAGE_5
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDocks3")
					iPreStreamStage++
				ENDIF
			//FOLLOW floyd TO CRANE
			ELIF eStageToLoad = MISSION_STAGE_6
				iPreStreamStage++
			 //CRANE SECTION
			ELIF eStageToLoad = MISSION_STAGE_7
				iPreStreamStage++
			//USE CAMERA
			ELIF eStageToLoad = MISSION_STAGE_8
				CLEANUP_LOADED_ANIM_ARRAY()
				CLEANUP_LOADED_MODEL_ARRAY()
				iPreStreamStage++
			//DRIVE TO DOCKS
			ELIF eStageToLoad = MISSION_STAGE_10
				//CLEANUP_LOADED_CARREC_ARRAY()
				iPreStreamStage++
			ENDIF

		BREAK
		
		CASE 3
			
			RETURN TRUE
			
		BREAK
		
	ENDSWITCH

	
	RETURN FALSE
	
ENDFUNC


// ======================================================================
//                                 Controls 
// ======================================================================

// PURPOSE: controls the display of objective blips (vehcile, ped or loc)
PROC CONTROL_OBJECTIVE_BLIP_DISPLAY(PED_INDEX target_ped, VEHICLE_INDEX target_veh, VECTOR target_coords)
	IF target_veh <> NULL // adds vehicle blip and removes others
		IF IS_VEHICLE_DRIVEABLE(target_veh)
			IF NOT DOES_BLIP_EXIST(veh_blip)
				veh_blip=CREATE_BLIP_FOR_VEHICLE(target_veh, FALSE) 
				TURN_ON_GPS_FOR_BLIP(veh_blip, sLocatesData)
				//PRINTSTRING("veh obj blip")
				////PRINTNL()
			ENDIF
		ENDIF
		IF DOES_BLIP_EXIST(dest_blip)
			REMOVE_BLIP(dest_blip)
		ENDIF
		IF DOES_BLIP_EXIST(ped_blip)
			REMOVE_BLIP(ped_blip)
		ENDIF			
	ELIF target_ped <> NULL //adds ped blip and removes others
		IF NOT IS_PED_INJURED(target_ped)
			IF NOT DOES_BLIP_EXIST(ped_blip)
				ped_blip = CREATE_BLIP_FOR_PED(target_ped, FALSE)
				TURN_ON_GPS_FOR_BLIP(ped_blip, sLocatesData)
				//PRINTSTRING("ped obj blip")
				////PRINTNL()
			ENDIF
		ENDIF
		IF DOES_BLIP_EXIST(veh_blip)
			REMOVE_BLIP(veh_blip)
		ENDIF
		IF DOES_BLIP_EXIST(dest_blip)
			REMOVE_BLIP(dest_blip)
		ENDIF
	ELSE //adds location blip and removes others
		IF NOT DOES_BLIP_EXIST(dest_blip)
			dest_blip = CREATE_BLIP_FOR_COORD(target_coords, FALSE)
			TURN_ON_GPS_FOR_BLIP(dest_blip, sLocatesData)
			//PRINTSTRING("dest obj blip")
			////PRINTNL()
		ENDIF
		IF DOES_BLIP_EXIST(veh_blip)
			REMOVE_BLIP(veh_blip)
		ENDIF
		IF DOES_BLIP_EXIST(ped_blip)
			REMOVE_BLIP(ped_blip)
		ENDIF
	ENDIF
ENDPROC

PROC RESET_CREATE_DOCKS_STUFF()
	bObjectsCreated[0] = FALSE
	bObjectsCreated[1] = FALSE
	bObjectsCreated[2] = FALSE
	//bCreateCrane = TRUE
ENDPROC

//PURPOSE: this is used to load a specific mission stage, for both restart and j/p skips.
//Load mission stage - lets go!
PROC LOAD_MISSION_STAGE(MISSION_STAGE_ENUM eStageToLoad) 
	IF NOT bcleanup //first cleans up all existing mission assets
		IF bSkipping = TRUE
			CONTROL_FADE_OUT(500)
		ENDIF
		
		IF IS_PHONE_ONSCREEN()
			HANG_UP_AND_PUT_AWAY_PHONE()
		ENDIF

		CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
		DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE) //disables all cheats.
		MISSION_CLEANUP(TRUE)
		//Assign model names
		//model_trevor_vehicle  = GET_PLAYER_VEH_MODEL(CHAR_TREVOR)

		PRINTSTRING("LOAD_MISSION_STAGE - MISSION_STAGE_ENUM = ")//PRINTNL()
		PRINTINT(ENUM_TO_INT(eStageToLoad))
		
		CLEAR_PRINTS()
		
		IF eStageToLoad != MISSION_STAGE_OPENING_CUTSCENE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
		ENDIF
		
		ADD_SCENARIO_BLOCKING_AREA(<<18.380921,-2524.460205,10.000374>>,<<10.180921,-2534.460205,4.800374>>)
		ADD_SCENARIO_BLOCKING_AREA(<<509.775757,-3161.031006,38.319389>> - <<87.250000,240.000000,43.000000>>, <<509.775757,-3161.031006,38.319389>> + <<87.250000,240.000000,43.000000>> )
		
		ADD_NAVMESH_BLOCKING_OBJECT(<<1712.584,3304.912,41.960>>,<<13.000,13.600,3.700>>,15.578)
		ADD_SCENARIO_BLOCKING_AREA(<<18.380921,-2524.460205,10.000374>>,<<10.180921,-2534.460205,4.800374>>)
		
		ADD_NAVMESH_BLOCKING_OBJECT(<<-107,-2415.304,5.381>>,<<16.4,15.3,1.6>>,-1.619)
		ADD_NAVMESH_BLOCKING_OBJECT(<<-53.081,-2415.703,5.119>>,<<16.4,15.3,1.6>>,1.584)
		ADD_SCENARIO_BLOCKING_AREA(<<-73.987930,-2415.800781,8.000994>> -<<43.250000,8.000000,3.250000>>,<<-73.987930,-2415.800781,8.000994>>+<<43.250000,8.000000,3.250000>>)
	
		CLEAR_AREA(vStageStart,1000,TRUE,TRUE)
		CLEAR_AREA(vDocks,1000,TRUE,TRUE)
		
		
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),0)
		PRINTSTRING("MISSION STAGE PRE-LOAD CLEANUP")//PRINTNL()
		
		bMercPatrolCreated[0] = FALSE
		bMercPatrolCreated[1] = FALSE
		bMercPatrolCreated[2] = FALSE
		iSetupProgress = 0
		i_current_event = 0
		iSecurityTimer = 0
		balarmtriggered = FALSE
		bLockForklift = FALSE
		bContainerSetUpForDrive = FALSE
		bIsJumpingDirectlyToStage = TRUE
		e_section_stage = SECTION_STAGE_SETUP
		
		INT iCounter
		
		FOR iCounter = 0 TO iNumberOfDockSetPieces -1
			bTriggerSetPiece[iCounter] = FALSE
		ENDFOR
		
		bRunFailChecks = FALSE
		bCrate1Attached = FALSE
		bCrate0Attached = FALSE
		bForceNoGrab = FALSE
		bIsJumpingDirectlyToStage = TRUE
		DISABLE_TAXI_HAILING(TRUE)
		SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
		//bCreateCrane = TRUE
		bcleanup = TRUE
	ENDIF
	
	SWITCH iSetupProgress
	
		CASE 0 //request shit and set the player to the correct model
			
			INITALISE_ARRAYS()
			SET_UP_DOCKS_RADIO()
			
			IF eStageToLoad = MISSION_STAGE_OPENING_CUTSCENE
				IF SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					iSetupProgress++
				ENDIF
			//DRIVE WITH FLOYD TO DOCKS
			ELIF eStageToLoad = MISSION_STAGE_1
				IF SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					WHILE NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_FLOYD, vfloydStart, ffloydStart )
					OR NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_WADE, vWadeStart, fWadeStart )
					OR NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_TREVORS_TRUCK, << -1155.02, -1522.81, 3.3397 >>  ,38.40)
					OR NOT SETUP_MISSION_REQUIREMENT(REQ_SECURITY)
						WAIT(0)
					ENDWHILE
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
						RESET_CREATE_DOCKS_STUFF()
						iSetupProgress++	
					ELSE
						SET_PLAYER_START_POSITION(eStageToLoad)
						//LOAD_SCENE(<< -1154.6117, -1520.0699, 3.3456 >>)
						RESET_CREATE_DOCKS_STUFF()
						iSetupProgress++					
					ENDIF
				ENDIF
			ELIF eStageToLoad = MISSION_STAGE_2
				IF SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_SECURITY)
					OR NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_TREVORS_TRUCK,<<21.3604, -2534.8997, 5.0505>>, 55.3357)
					OR NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_FLOYD, vfloydStart, ffloydStart)
					OR NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_WADE, vWadeStart, fWadeStart )
						WAIT(0)
					ENDWHILE
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_Y_BlackOps_01)
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_M_DOCKWORK_01)
					ADD_MODEL_REQUEST_TO_ARRAY(GET_NPC_PED_MODEL(CHAR_WADE))
					ADD_MODEL_REQUEST_TO_ARRAY(GET_NPC_PED_MODEL(CHAR_FLOYD))
					ADD_MODEL_REQUEST_TO_ARRAY(BIFF)
					ADD_MODEL_REQUEST_TO_ARRAY(FORKLIFT)
					REQUEST_ANIM_DICT("misslsdhs")
					RESET_CREATE_DOCKS_STUFF()
//					SET_VEHICLE_POPULATION_BUDGET(2)
//					SET_PED_POPULATION_BUDGET(2)
					
					IF NOT IS_PED_INJURED(pedFloyd)
						IF NOT IS_PED_INJURED(pedWade)
							IF IS_VEHICLE_DRIVEABLE(vehMission)
								IF IS_VEHICLE_SEAT_FREE(vehMission,VS_BACK_LEFT)
									SET_PED_INTO_VEHICLE(pedWade,vehMission,VS_BACK_LEFT)
								ENDIF
								IF IS_VEHICLE_SEAT_FREE(vehMission,VS_FRONT_RIGHT)
									SET_PED_INTO_VEHICLE(pedFloyd,vehMission,VS_FRONT_RIGHT)
								ENDIF
								SET_VEHICLE_ENGINE_ON(vehMission,TRUE,TRUE)
								CLEAR_AREA(<< 12.2127, -2531.0344, 5.0509 >>,100,TRUE,TRUE,TRUE)
								SET_PED_NON_CREATION_AREA(<< 12.2127, -2531.0344, 5.0509 >> - << 10, 10, 10 >>, << 12.2127, -2531.0344, 5.0509 >> + << 10, 10, 10 >>)
							ENDIF
						ENDIF
					ENDIF
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP(vehMission)
						RESET_CREATE_DOCKS_STUFF()
						iSetupProgress++	
					ELSE
						SET_PLAYER_START_POSITION(eStageToLoad)
						IF IS_VEHICLE_DRIVEABLE(vehMission)
							IF IS_VEHICLE_SEAT_FREE(vehMission,VS_DRIVER)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehMission,VS_DRIVER)
							ENDIF
						ENDIF
						LOAD_SCENE(<< 21.3604, -2534.8997, 5.0505 >>)
						RESET_CREATE_DOCKS_STUFF()
						iSetupProgress++					
					ENDIF
					
					
				ENDIF
			ELIF eStageToLoad = MISSION_STAGE_4
				IF SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					REQUEST_VEHICLE_RECORDING(1,"AWDocks1")
					REQUEST_WAYPOINT_RECORDING("floyddocks1") 
					REQUEST_WAYPOINT_RECORDING("docksplayer1")
					REQUEST_VEHICLE_RECORDING(1,"AWDock1")
					REQUEST_VEHICLE_RECORDING(1,"AWDocks2")
					
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_Y_BlackOps_01)
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_M_DOCKWORK_01)
					ADD_MODEL_REQUEST_TO_ARRAY(GET_NPC_PED_MODEL(CHAR_FLOYD))
					ADD_MODEL_REQUEST_TO_ARRAY(BIFF)
					ADD_MODEL_REQUEST_TO_ARRAY(FORKLIFT)
					RESET_CREATE_DOCKS_STUFF()
//					SET_VEHICLE_POPULATION_BUDGET(2)
//					SET_PED_POPULATION_BUDGET(2)
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
						iSetupProgress++
					ELSE
						SET_PLAYER_START_POSITION(eStageToLoad)
						iSetupProgress++				
					ENDIF
				ENDIF
			//FORKLIFT AND TASKS
			ELIF eStageToLoad = MISSION_STAGE_5
				IF SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					WHILE NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_CONTAINERS_FOR_HANDLER_SECTION_START,<<0,0,0>>)
						WAIT(0)
					ENDWHILE
					REQUEST_VEHICLE_RECORDING(1,"AWDock1")
					REQUEST_VEHICLE_RECORDING(1,"AWDocks2")
					REQUEST_VEHICLE_RECORDING(1,"AWDocks3")
					REQUEST_ANIM_DICT("missheistdockssetup1ig_13@start_idle")
					
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_Y_BlackOps_01)
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_M_DOCKWORK_01)
					ADD_MODEL_REQUEST_TO_ARRAY(GET_NPC_PED_MODEL(CHAR_FLOYD))
					ADD_MODEL_REQUEST_TO_ARRAY(BIFF)
					ADD_MODEL_REQUEST_TO_ARRAY(FORKLIFT)
					RESET_CREATE_DOCKS_STUFF()
//					SET_VEHICLE_POPULATION_BUDGET(2)
//					SET_PED_POPULATION_BUDGET(2)
					s_sp_blackwater_with_dockworker[0].i_event = 8
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
						iSetupProgress++
					ELSE
						SET_PLAYER_START_POSITION(eStageToLoad)
						iSetupProgress++				
					ENDIF
				ENDIF
			//FOLLOW floyd TO CRANE
			
			ELIF eStageToLoad = MISSION_STAGE_6
				IF SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					WHILE NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_CONTAINERS_FOR_HANDLER_SECTION_FINAL,<<0,0,0>>)
						WAIT(0)
					ENDWHILE
					REQUEST_VEHICLE_RECORDING(1,"AWDock1")
					REQUEST_VEHICLE_RECORDING(1,"AWDocks2")
					REQUEST_ANIM_DICT("missheistdockssetup1ig_13@start_idle")
					REQUEST_ANIM_DICT("missheistdockssetup1ig_14")
					START_AUDIO_SCENE("DH_1_HANDLER_START")
					
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_Y_BlackOps_01)
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_M_DOCKWORK_01)
					ADD_MODEL_REQUEST_TO_ARRAY(GET_NPC_PED_MODEL(CHAR_FLOYD))
					ADD_MODEL_REQUEST_TO_ARRAY(BIFF)
					ADD_MODEL_REQUEST_TO_ARRAY(FORKLIFT)
					ADD_MODEL_REQUEST_TO_ARRAY(PROP_CONTR_03B_LD)
					
					RESET_CREATE_DOCKS_STUFF()
//					SET_VEHICLE_POPULATION_BUDGET(2)
//					SET_PED_POPULATION_BUDGET(2)
					s_sp_blackwater_with_dockworker[0].i_event = 8
					iFloydHauler = 0
					vSpreaderPosition = <<-53.02, -2416.04, 14.75>>
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
						iSetupProgress++
					ELSE
						SET_PLAYER_START_POSITION(eStageToLoad)
						iSetupProgress++				
					ENDIF
				ENDIF
			//CRANE SECTION
			ELIF eStageToLoad = MISSION_STAGE_7
				IF SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					WHILE NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_CONTAINERS_FOR_HANDLER_SECTION_FINAL,<<0,0,0>>)
						WAIT(0)
					ENDWHILE
					REQUEST_VEHICLE_RECORDING(2,"AWDocks1")
					REQUEST_VEHICLE_RECORDING(1,"AWDock1")
					REQUEST_VEHICLE_RECORDING(1,"AWDocks2")
					REQUEST_ANIM_DICT("missheistdockssetup1trevor_crane")
					
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_Y_BlackOps_01)
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_M_DOCKWORK_01)
					ADD_MODEL_REQUEST_TO_ARRAY(GET_NPC_PED_MODEL(CHAR_FLOYD))
					ADD_MODEL_REQUEST_TO_ARRAY(BIFF)
					ADD_MODEL_REQUEST_TO_ARRAY(FORKLIFT)
					ADD_MODEL_REQUEST_TO_ARRAY(HAULER)
					ADD_MODEL_REQUEST_TO_ARRAY(DOCKTRAILER)
					RESET_CREATE_DOCKS_STUFF()
//					SET_VEHICLE_POPULATION_BUDGET(2)
//					SET_PED_POPULATION_BUDGET(2)
					s_sp_blackwater_with_dockworker[0].i_event = 8
					vSpreaderPosition = <<-53.02, -2416.04, 14.75>>
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
						iSetupProgress++
					ELSE
						SET_PLAYER_START_POSITION(eStageToLoad)
						iSetupProgress++				
					ENDIF
				ENDIF
			//USE CAMERA
			ELIF eStageToLoad = MISSION_STAGE_8
				IF SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),TRUE)
					WHILE NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_CONTAINERS_FOR_HANDLER_SECTION_FINAL,<<0,0,0>>)
						WAIT(0)
					ENDWHILE
					REQUEST_VEHICLE_RECORDING(1,"AWDock1")
					REQUEST_VEHICLE_RECORDING(1,"AWDocks2")
					
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_Y_BlackOps_01)
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_M_DOCKWORK_01)
					ADD_MODEL_REQUEST_TO_ARRAY(GET_NPC_PED_MODEL(CHAR_FLOYD))
					ADD_MODEL_REQUEST_TO_ARRAY(HAULER)
					ADD_MODEL_REQUEST_TO_ARRAY(DOCKTRAILER)
					RESET_CREATE_DOCKS_STUFF()
//					SET_VEHICLE_POPULATION_BUDGET(2)
//					SET_PED_POPULATION_BUDGET(2)
					s_sp_blackwater_with_dockworker[0].i_event = 8
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
						iSetupProgress++
					ELSE
						SET_PLAYER_START_POSITION(eStageToLoad)
						iSetupProgress++				
					ENDIF
				ENDIF
			//DRIVE TO OTHER DOCKS
			ELIF eStageToLoad = MISSION_STAGE_10
				IF SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					WHILE NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_CONTAINERS_FOR_HANDLER_SECTION_FINAL,<<0,0,0>>)
						WAIT(0)
					ENDWHILE
					REQUEST_VEHICLE_RECORDING(1,"AWDock1")
					REQUEST_VEHICLE_RECORDING(1,"AWDocks2")
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_Y_BlackOps_01)
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_M_DOCKWORK_01)
					ADD_MODEL_REQUEST_TO_ARRAY(GET_NPC_PED_MODEL(CHAR_FLOYD))
					ADD_MODEL_REQUEST_TO_ARRAY(BIFF)
					ADD_MODEL_REQUEST_TO_ARRAY(FORKLIFT)
					ADD_MODEL_REQUEST_TO_ARRAY(BIFF)
					ADD_MODEL_REQUEST_TO_ARRAY(HAULER)
					ADD_MODEL_REQUEST_TO_ARRAY(DOCKTRAILER)
					RESET_CREATE_DOCKS_STUFF()
					s_sp_blackwater_with_dockworker[0].i_event = 8
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
						iSetupProgress++
					ELSE
						SET_PLAYER_START_POSITION(eStageToLoad)
						LOAD_SCENE(<<-120.9023, -2411.2688, 5.0007>>)
						iSetupProgress++				
					ENDIF
				ENDIF
			//DRIVE TO FLOYDS
			ELIF eStageToLoad = MISSION_STAGE_11
				IF SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
					WHILE NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_CONTAINERS_FOR_HANDLER_SECTION_FINAL,<<0,0,0>>)
						WAIT(0)
					ENDWHILE
					WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_HAULER_WITH_TRAILER_FINAL_STAGE)
						WAIT(0)
					ENDWHILE
					
					WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_GUARDS_AND_FLOYD)
						WAIT(0)
					ENDWHILE
					PRINTSTRING("!!!JUMPING TO DRIVE TO FLOYDS 1")PRINTNL()
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_Y_BlackOps_01)
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_M_DOCKWORK_01)
					ADD_MODEL_REQUEST_TO_ARRAY(GET_NPC_PED_MODEL(CHAR_FLOYD))
					ADD_MODEL_REQUEST_TO_ARRAY(BIFF)
					ADD_MODEL_REQUEST_TO_ARRAY(HAULER)
					ADD_MODEL_REQUEST_TO_ARRAY(DOCKTRAILER)
					RESET_CREATE_DOCKS_STUFF()
					s_sp_blackwater_with_dockworker[0].i_event = 8
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
						iSetupProgress++
					ELSE
						SET_PLAYER_START_POSITION(eStageToLoad)
						iSetupProgress++				
					ENDIF
				ENDIF
			//PASSED MISSION
			ELIF eStageToLoad = MISSION_STAGE_PASSED
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP()
					iSetupProgress++
				ELSE
					LOAD_SCENE(<<-1153.4308, -1517.1697, 9.6327>>)
					SET_PLAYER_START_POSITION(eStageToLoad)
					iSetupProgress++				
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 1  // load shit that takes one frame and you only want to call once, create items
			//IF ARE_REQUESTED_ANIMS_LOADED()
				IF ARE_REQUESTED_MODELS_LOADED() 
					IF ARE_REQUESTED_CARRECS_LOADED("AWDocks1")
						IF eStageToLoad = MISSION_STAGE_OPENING_CUTSCENE
							iSetupProgress++
						//DRIVE WITH floyd TO DOCKS
						ELIF eStageToLoad = MISSION_STAGE_1
							IF SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
								MANAGE_PLAYER_OUTFIT()
								IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedFloyd)
								AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedWade)
								AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
									iSetupProgress++
								ENDIF
							ENDIF
						//PLAYER PARKS UP AND GETS CHANGED
						ELIF eStageToLoad = MISSION_STAGE_2
							IF NOT DOES_ENTITY_EXIST(pedFloyd)
								SPAWN_BUDDY(pedFloyd, GET_NPC_PED_MODEL(CHAR_FLOYD), vfloydStart, ffloydStart,TRUE,WEAPONTYPE_UNARMED,TRUE,"FLOYD",3)
							ELSE
								IF NOT DOES_ENTITY_EXIST(pedWade)
									SPAWN_BUDDY(pedWade, GET_NPC_PED_MODEL(CHAR_WADE), vWadeStart, fWadeStart,TRUE,WEAPONTYPE_UNARMED,TRUE,"WADE",4)
								ELSE
									IF NOT DOES_ENTITY_EXIST(vehMission)
										SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_TREVORS_TRUCK,vDocks ,55.2078)
									ELSE
										IF CREATE_THE_DOCKS_STUFF()
											IF NOT DOES_ENTITY_EXIST(objClipboard[0])
												objClipboard[0] = CREATE_OBJECT(P_CS_CLIPBOARD,vDocks)
											ELSE
												IF NOT DOES_ENTITY_EXIST(objPencil[0])
													objPencil[0] = CREATE_OBJECT(PROP_PENCIL_01,vDocks)
													MANAGE_PLAYER_OUTFIT()
												ELSE
													IF DOES_ENTITY_EXIST(pedSecurity)
														IF NOT IS_PED_INJURED(pedSecurity)
															REQUEST_ANIM_DICT("misslsdhsclipboard@base")
															IF HAS_ANIM_DICT_LOADED("misslsdhsclipboard@base")
															AND HAS_ANIM_DICT_LOADED("misslsdhs")
																IF NOT IS_PED_INJURED(pedFloyd)
																AND NOT IS_PED_INJURED(pedWade)
																	
																	IF HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedFloyd)
																	AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedWade)
																	AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
																		CLEAR_ALL_PED_PROPS(pedSecurity) 
																		ATTACH_ENTITY_TO_ENTITY(objPencil[0],pedSecurity,GET_PED_BONE_INDEX(pedSecurity, BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>,TRUE,TRUE)
																		ATTACH_ENTITY_TO_ENTITY(objClipboard[0],pedSecurity,GET_PED_BONE_INDEX(pedSecurity, BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>,TRUE,TRUE,TRUE)
																		SET_ENTITY_NO_COLLISION_ENTITY(objClipboard[0],pedSecurity,TRUE)
																		TASK_PLAY_ANIM(pedSecurity,"misslsdhsclipboard@base", "base",INSTANT_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
																		ADD_PED_FOR_DIALOGUE(sSpeech, 1, pedSecurity, "GATEGUARD")
																		SET_PED_CAN_EVASIVE_DIVE(pedSecurity,FALSE)
																		FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSecurity)
																		iSetupProgress++
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						//FOLLOW floyd AROUND THE DOCKS
						ELIF eStageToLoad = MISSION_STAGE_4
							IF  GET_IS_WAYPOINT_RECORDING_LOADED("floyddocks1")
							AND GET_IS_WAYPOINT_RECORDING_LOADED("docksplayer1")
							AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDocks2")
							AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDock1")
								IF NOT DOES_ENTITY_EXIST(pedFloyd)
									SPAWN_BUDDY(pedFloyd, GET_NPC_PED_MODEL(CHAR_FLOYD),  << -63.91, -2523.12, 5.0110 >>, 58.62,TRUE,WEAPONTYPE_UNARMED,TRUE,"FLOYD",3)
								ELSE
									IF NOT DOES_ENTITY_EXIST(vehMission)
										WHILE NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_TREVORS_TRUCK,vParkUpHere ,56)
											WAIT(0)
										ENDWHILE
									ELSE
										IF CREATE_THE_DOCKS_STUFF()
											IF IS_VEHICLE_DRIVEABLE(vehMission)
												SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehMission,FALSE)
											ENDIF
											iSetupProgress++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						//FORKLIFT AND TASKS
						ELIF eStageToLoad = MISSION_STAGE_5
							IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDock1")
							AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDocks2")
							AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDocks3")
								IF NOT DOES_ENTITY_EXIST(pedFloyd)
									SPAWN_BUDDY(pedFloyd, GET_NPC_PED_MODEL(CHAR_FLOYD),  << -58.7087, -2524.6753, 5.0110 >>, 191.0631,TRUE,WEAPONTYPE_UNARMED,TRUE,"FLOYD",3)
								ELSE
									IF NOT DOES_ENTITY_EXIST(vehMission)
										WHILE NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_TREVORS_TRUCK,vParkUpHere ,56)
											WAIT(0)
										ENDWHILE
									ELSE
										IF NOT DOES_ENTITY_EXIST(vehHandler)
											SPAWN_VEHICLE(vehHandler,HANDLER,<<-129.77, -2418.28, 6.24>>,183.0850,-1,1.0)
											SET_VEHICLE_COLOUR_COMBINATION(vehHandler,0)
											SET_VEHICLE_PROVIDES_COVER(vehHandler,FALSE)
										ELSE
											IF CREATE_THE_DOCKS_STUFF()
												
												IF NOT DOES_ENTITY_EXIST(s_sp_forklift_supervisorA[0].ped)
													REQUEST_MODEL(s_sp_forklift_supervisorA[0].model)
													IF HAS_MODEL_LOADED(s_sp_forklift_supervisorA[0].model)
														s_sp_forklift_supervisorA[0].ped = CREATE_SETPIECE_PED(s_sp_forklift_supervisorA[0].model,s_sp_forklift_supervisorA[0].v_spawn_position, s_sp_forklift_supervisorA[0].f_start_heading, rel_group_buddies, 200, 0)
													ENDIF
												ELSE
													REQUEST_ANIM_DICT("missheistdockssetup1ig_9@start_idle")
													IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_9@start_idle")
														REQUEST_MODEL(P_CS_CLIPBOARD)
														IF HAS_MODEL_LOADED(P_CS_CLIPBOARD)
															IF NOT DOES_ENTITY_EXIST(objClipboard[1])
																objClipboard[1] = CREATE_OBJECT(P_CS_CLIPBOARD,<<-131.08, -2423.25, 8.00>>)
															ELSE
																REQUEST_MODEL(PROP_PENCIL_01)
																IF HAS_MODEL_LOADED(PROP_PENCIL_01)
																	IF NOT DOES_ENTITY_EXIST(objPencil[1])
																		objPencil[1] = CREATE_OBJECT(PROP_PENCIL_01,<<-131.08, -2423.25, 8.00>>)
																	ELSE
																		IF DOES_ENTITY_EXIST(s_sp_forklift_supervisorA[0].ped)
																			IF NOT IS_ENTITY_ATTACHED(objPencil[1])
																				IF NOT IS_PED_INJURED(s_sp_forklift_supervisorA[0].ped)
																					ATTACH_ENTITY_TO_ENTITY(objPencil[1],s_sp_forklift_supervisorA[0].ped,GET_PED_BONE_INDEX(s_sp_forklift_supervisorA[0].ped, BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>,TRUE)
																				ENDIF
																			ELSE
																				IF NOT IS_ENTITY_ATTACHED(objClipboard[1])
																					IF NOT IS_PED_INJURED(s_sp_forklift_supervisorA[0].ped)
																						ATTACH_ENTITY_TO_ENTITY(objClipboard[1],s_sp_forklift_supervisorA[0].ped,GET_PED_BONE_INDEX(s_sp_forklift_supervisorA[0].ped, BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>,TRUE)
																					ENDIF
																				ELSE
																					IF IS_VEHICLE_DRIVEABLE(vehMission)
																						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehMission,FALSE)
																					ENDIF
																					iSetupProgress++
																				ENDIF
																			ENDIF
																		ENDIF
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						//FOLLOW floyd TO CRANE
						ELIF eStageToLoad = MISSION_STAGE_6
							IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDock1")
							AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDocks2")
							AND HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_14")
								IF NOT DOES_ENTITY_EXIST(pedFloyd)
									SPAWN_BUDDY(pedFloyd, GET_NPC_PED_MODEL(CHAR_FLOYD),  <<-129.77, -2418.28, 6.24>>, 191.0631,TRUE,WEAPONTYPE_UNARMED,TRUE,"FLOYD",3)
								ELSE
									IF NOT DOES_ENTITY_EXIST(vehMission)
										WHILE NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_TREVORS_TRUCK,vParkUpHere ,56)
											WAIT(0)
										ENDWHILE
									ELSE
										IF NOT DOES_ENTITY_EXIST(vehHandler)
											SPAWN_VEHICLE(vehHandler,HANDLER,<<-88.8838, -2451.5679, 5.0175>>, 317.2729,-1,1.0)
											SET_VEHICLE_COLOUR_COMBINATION(vehHandler,0)
											SET_VEHICLE_PROVIDES_COVER(vehHandler,FALSE)
										ELSE
											IF IS_VEHICLE_DRIVEABLE(vehHandler)
												IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
													IF IS_VEHICLE_SEAT_FREE(vehHandler)
														SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehHandler)
													ENDIF
												ELSE
													IF CREATE_THE_DOCKS_STUFF()
														iSetupProgress++
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						//CRANE SECTION
						ELIF eStageToLoad = MISSION_STAGE_7
							IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDock1")
							AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDocks2")
								IF HAS_ANIM_DICT_LOADED("missheistdockssetup1trevor_crane")
									IF NOT DOES_ENTITY_EXIST(pedFloyd)
										SPAWN_BUDDY(pedFloyd, GET_NPC_PED_MODEL(CHAR_FLOYD), <<-120.65, -2422.97, 5.00>>, -38.16,TRUE,WEAPONTYPE_UNARMED,TRUE,"FLOYD",3)
									ELSE
										IF NOT DOES_ENTITY_EXIST(vehMission)
											WHILE NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_TREVORS_TRUCK,vParkUpHere ,56)
												WAIT(0)
											ENDWHILE
										ELSE
											IF CREATE_THE_DOCKS_STUFF()
												iSetupProgress++
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						//USE CAMERA
						ELIF eStageToLoad = MISSION_STAGE_8
							IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDock1")
							AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDocks2")
								IF NOT DOES_ENTITY_EXIST(pedFloyd)
									SPAWN_BUDDY(pedFloyd, GET_NPC_PED_MODEL(CHAR_FLOYD),  << -145.1769, -2472.3149, 5.0310 >>, ffloydStart,TRUE,WEAPONTYPE_UNARMED,TRUE,"FLOYD",3)
								ELSE
									IF NOT DOES_ENTITY_EXIST(vehFloydTruck)
										SPAWN_VEHICLE(vehFloydTruck,HAULER,<< -117.9839, -2416.6272, 5.0003 >>,91.3366)
									ELSE
										IF NOT DOES_ENTITY_EXIST(vehTrailer2)
											IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
												SET_VEHICLE_COLOURS(vehFloydTruck,0,0)
												SPAWN_VEHICLE(vehTrailer2,DOCKTRAILER,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehFloydTruck, <<-0.3, -9.5, -0.25>>), GET_ENTITY_HEADING(vehFloydTruck))
												SET_VEHICLE_EXTRA(vehTrailer2,1,FALSE)
											ENDIF
										ELSE
											IF IS_VEHICLE_DRIVEABLE(vehFloydTruck) 
												IF IS_VEHICLE_DRIVEABLE(vehTrailer2)
													IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(vehFloydTruck)
														ATTACH_VEHICLE_TO_TRAILER(vehFloydTruck,vehTrailer2)
													ELSE
														IF NOT IS_PED_INJURED(pedFloyd)
															IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
																IF IS_VEHICLE_SEAT_FREE(vehFloydTruck,VS_FRONT_RIGHT)
																	SET_PED_INTO_VEHICLE(pedFloyd,vehFloydTruck,VS_FRONT_RIGHT)
																ENDIF
															ENDIF
														ENDIF
														//IF CREATE_CRANE()	
														IF CREATE_THE_DOCKS_STUFF()
															iSetupProgress++
														ENDIF
														FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
														//ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								//CREATE_THE_DOCKS_STUFF()
								
							ENDIF						
						//DRIVE TO OTHER DOCKS
						ELIF eStageToLoad = MISSION_STAGE_10
							IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDock1")
							AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDocks2")
								IF NOT DOES_ENTITY_EXIST(pedFloyd)
									SPAWN_BUDDY(pedFloyd, GET_NPC_PED_MODEL(CHAR_FLOYD),  << -145.1769, -2472.3149, 5.0310 >>, ffloydStart,TRUE,WEAPONTYPE_UNARMED,TRUE,"FLOYD",3)
								ELSE
									IF NOT DOES_ENTITY_EXIST(vehFloydTruck)
										SPAWN_VEHICLE(vehFloydTruck,HAULER,<<-116.3106, -2415.3901, 5.0005>>, 88.7066)
									ELSE
										IF NOT DOES_ENTITY_EXIST(vehTrailer2)
											IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
												SET_VEHICLE_COLOURS(vehFloydTruck,0,0)
												SPAWN_VEHICLE(vehTrailer2,DOCKTRAILER,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehFloydTruck, <<-0.3, -9.5, -0.25>>), GET_ENTITY_HEADING(vehFloydTruck))
												SET_VEHICLE_EXTRA(vehTrailer2,1,FALSE)
											ENDIF
										ELSE
											IF IS_VEHICLE_DRIVEABLE(vehFloydTruck) 
												IF IS_VEHICLE_DRIVEABLE(vehTrailer2)
													IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(vehFloydTruck)
														ATTACH_VEHICLE_TO_TRAILER(vehFloydTruck,vehTrailer2)
													ELSE
														IF NOT IS_PED_INJURED(pedFloyd)
															IF IS_VEHICLE_SEAT_FREE(vehFloydTruck,VS_FRONT_RIGHT)
																SET_PED_INTO_VEHICLE(pedFloyd,vehFloydTruck,VS_FRONT_RIGHT)
															ENDIF
//															IF IS_VEHICLE_SEAT_FREE(vehFloydTruck,VS_DRIVER)
//																SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehFloydTruck,VS_DRIVER)
//															ENDIF
															//IF CREATE_CRANE()	
															IF CREATE_THE_DOCKS_STUFF()
																REQUEST_IPL("pcranecont")
																iSetupProgress++
															ENDIF
															//ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						//DRIVE HOME
						ELIF eStageToLoad = MISSION_STAGE_11
							IF NOT DOES_ENTITY_EXIST(vehFinal)
								SPAWN_VEHICLE(vehFinal,TIPTRUCK, << 465.8751, -3037.0837, 5.0686 >>, 126.3433,-1,1)
							ELSE
								PRINTSTRING("!!!JUMPING TO DRIVE TO FLOYDS 2")PRINTNL()
								iSetupProgress++
							ENDIF
							//BLANK STAGE
						//PASSED MISSION
						ELIF eStageToLoad = MISSION_STAGE_PASSED
							IF IS_REPLAY_BEING_SET_UP()
								END_REPLAY_SETUP()
								iSetupProgress++
							ELSE
								SET_PLAYER_START_POSITION(eStageToLoad)
								iSetupProgress++				
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			//ENDIF
		BREAK
		
		CASE 2 //Things that might take more than one frame and you don't mind calling multiple times and set player starting position
			
			IF eStageToLoad > MISSION_STAGE_OPENING_CUTSCENE
				MANAGE_PLAYER_OUTFIT()
			ENDIF

			IF eStageToLoad > MISSION_STAGE_2
				REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_DOCKS_BACK_GATE_IN,PLAYER_PED_ID())
				REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_DOCKS_BACK_GATE_OUT,PLAYER_PED_ID())
				//LOCK GATE
				UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_DOCKS_FRONT_GATE_OUT,PLAYER_PED_ID())
			ENDIF
			
			IF eStageToLoad <> MISSION_STAGE_1 OR eStageToLoad <> MISSION_STAGE_OPENING_CUTSCENE
				iprogress = 0
				IF NOT Is_Replay_In_Progress()
					SET_PLAYER_START_POSITION(eStageToLoad)
				ENDIF
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)

			IF eStageToLoad = MISSION_STAGE_OPENING_CUTSCENE
				iprogress = 0
				eMissionStage = MISSION_STAGE_OPENING_CUTSCENE
				RESET_MISSION_STAGE_INTS_SKIP()
			ELIF eStageToLoad = MISSION_STAGE_1
				iprogress = 0
				eMissionStage = MISSION_STAGE_1
				IF bskipping = TRUE
					
				ENDIF	
				REQUEST_IPL("Talklaugh_Pipe")
				RESET_MISSION_STAGE_INTS_SKIP()
			ELIF eStageToLoad = MISSION_STAGE_2
				eMissionStage = MISSION_STAGE_2
				REQUEST_IPL("Talklaugh_Pipe")
				RESET_MISSION_STAGE_INTS_SKIP()			
			ELIF eStageToLoad = MISSION_STAGE_4	
				IF NOT DOES_ENTITY_EXIST(pedFloyd)
					FORCE_PED_MOTION_STATE(pedFloyd, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
				ENDIF
				
				eMissionStage = MISSION_STAGE_4
				RESET_MISSION_STAGE_INTS_SKIP()
			ELIF eStageToLoad = MISSION_STAGE_5
				CLEANUP_LOADED_MODEL_ARRAY()
				REQUEST_IPL("Talklaugh_Pipe")
				IF IS_VEHICLE_DRIVEABLE(vehHandler)
					IF IS_VEHICLE_SEAT_FREE(vehHandler)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehHandler)
					ENDIF
				ENDIF
				eMissionStage = MISSION_STAGE_5
				RESET_MISSION_STAGE_INTS_SKIP()			
			ELIF eStageToLoad = MISSION_STAGE_6
				REQUEST_IPL("Talklaugh_Pipe")
				eMissionStage = MISSION_STAGE_6
				RESET_MISSION_STAGE_INTS_SKIP()			
			ELIF eStageToLoad = MISSION_STAGE_7
				REQUEST_IPL("Talklaugh_Pipe")
				eMissionStage = MISSION_STAGE_7
				RESET_MISSION_STAGE_INTS_SKIP()
			ELIF eStageToLoad = MISSION_STAGE_8
				REQUEST_IPL("Talklaugh_Pipe")
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),TRUE)
				eMissionStage = MISSION_STAGE_8
				STOP_GAMEPLAY_HINT()
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
				SETTIMERA(0)
				RESET_MISSION_STAGE_INTS_SKIP()
			ELIF eStageToLoad = MISSION_STAGE_10
				REQUEST_IPL("Talklaugh_Pipe")
				//CLEANUP_LOADED_MODEL_ARRAY()
				eMissionStage = MISSION_STAGE_10
				RESET_MISSION_STAGE_INTS_SKIP()	
			ELIF eStageToLoad = MISSION_STAGE_11
				REQUEST_IPL("Talklaugh_Pipe")
				PRINTSTRING("!!!JUMPING TO DRIVE TO FLOYDS 3")PRINTNL()
				CLEANUP_LOADED_MODEL_ARRAY()
				CLEANUP_LOADED_ANIM_ARRAY()
				eMissionStage = MISSION_STAGE_11
				RESET_MISSION_STAGE_INTS_SKIP()	
			ELIF eStageToLoad = MISSION_STAGE_PASSED
				PRINTSTRING("!!!JUMPING TO ")PRINTNL()
				CLEANUP_LOADED_MODEL_ARRAY()
				CLEANUP_LOADED_ANIM_ARRAY()
				eMissionStage = MISSION_STAGE_PASSED
				RESET_MISSION_STAGE_INTS_SKIP()	
			ENDIF
		BREAK
		
	ENDSWITCH	
	
ENDPROC



// ======================================================================
//        	     MISSION STAGES AND CUTSCENES AS PROCEDURES
// ======================================================================
//PURPOSE: Requests and creates any assets etc that are initally required for the mission stage then holds in the load mission stage loop until complete
PROC MISSION_SETUP()
	
	ADD_SCENARIO_BLOCKING_AREA(<<509.775757,-3161.031006,38.319389>> - <<87.250000,240.000000,43.000000>>, <<509.775757,-3161.031006,38.319389>> + <<87.250000,240.000000,43.000000>> )
	
	ADD_NAVMESH_BLOCKING_OBJECT(<<-107,-2415.304,5.381>>,<<16.4,15.3,1.6>>,-1.619)
	ADD_NAVMESH_BLOCKING_OBJECT(<<-53.081,-2415.703,5.119>>,<<16.4,15.3,1.6>>,1.584)
	ADD_SCENARIO_BLOCKING_AREA(<<-73.987930,-2415.800781,8.000994>> -<<43.250000,8.000000,3.250000>>,<<-73.987930,-2415.800781,8.000994>>+<<43.250000,8.000000,3.250000>>)
	SET_ALL_SHOPS_TEMPORARILY_UNAVAILABLE(TRUE)
	
	PRINTSTRING("DOCKS HEIST - MISSION_SETUP")//PRINTNL()
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
	//INFORM_MISSION_STATS_OF_MISSION_START_DOCKS_HEIST_SETUP()
	// Loads the mission text
	REQUEST_ADDITIONAL_TEXT("DOCKH1", MISSION_TEXT_SLOT)
	//1045747
	//REQUEST_ADDITIONAL_TEXT("D1AUD", MISSION_DIALOGUE_TEXT_SLOT)
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT) 
	//OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
		WAIT(0)
	ENDWHILE
	
	DISABLE_CHEAT(CHEAT_TYPE_ALL, TRUE) //disables all cheats.

	SET_UP_DOCKS_RADIO()
	
	REGISTER_SCRIPT_WITH_AUDIO()

	//DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
	//DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<1187,-1398,4.3>>, 10)
	
	REMOVE_RELATIONSHIP_GROUP(rel_group_buddies)
	REMOVE_RELATIONSHIP_GROUP(rel_group_enemies)
	ADD_RELATIONSHIP_GROUP("BUDDIES", rel_group_buddies)
	ADD_RELATIONSHIP_GROUP("ENEMIES", rel_group_enemies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,rel_group_buddies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_buddies,rel_group_enemies)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_PLAYER,rel_group_buddies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,rel_group_enemies,rel_group_buddies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_enemies,rel_group_enemies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_enemies,rel_group_enemies)
	
	//SET_PED_RELATIONSHIP_GROUP_HASH(PLAYER_PED_ID() ,rel_group_buddies)
	SET_GROUP_SEPARATION_RANGE(PLAYER_GROUP_ID(), 150.0)
	
	//SET_MAX_WANTED_LEVEL(0)
	SET_WANTED_LEVEL_MULTIPLIER(0.4)
	DISABLE_TAXI_HAILING(TRUE)
	
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_WillFlyThroughWindscreen,FALSE)
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(FORKLIFT,TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BODHI2,TRUE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BIFF,TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(GET_NPC_PED_MODEL(CHAR_WADE),TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(GET_NPC_PED_MODEL(CHAR_FLOYD),TRUE)
	bHasChanged = GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR)
	
	SETUP_PEDS_FOR_DIALOGUE()
	
	IF Is_Replay_In_Progress()
		//Load appropriate mission stage dependent on checkpoint
		IF Get_Replay_Mid_Mission_Stage() > ENUM_TO_INT(MISSION_STAGE_SETUP) AND Get_Replay_Mid_Mission_Stage()  < ENUM_TO_INT(MISSION_STAGE_PASSED)
			iReplayStage = Get_Replay_Mid_Mission_Stage()	
			
			IF g_bShitskipAccepted = TRUE
				iReplayStage = iReplayStage + 1
				IF iReplayStage < ENUM_TO_INT(MISSION_STAGE_PASSED)
					PRINTSTRING("******* Actual value - iReplayStage= ")PRINTINT(iReplayStage)PRINTNL()
					IF iReplayStage = 1
						//DRIVE
						START_REPLAY_SETUP(<< 24.4835, -2536.4578, 5.0410 >>,55.2078)
						eMissionStage = MISSION_STAGE_2
						PRINTSTRING("******* iReplayStage = 2")//PRINTNL()
					ELIF iReplayStage = 2
						//PARK
						START_REPLAY_SETUP(<< 24.4835, -2536.4578, 5.0410 >>,55.2078)
						eMissionStage = MISSION_STAGE_2
						PRINTSTRING("******* iReplayStage = 3")//PRINTNL()
					ELIF iReplayStage = 3
						//FOLLOW FLOYD
						START_REPLAY_SETUP(<< -61.5066, -2525.5120, 5.0101 >>,56.9516)
						eMissionStage = MISSION_STAGE_4
						PRINTSTRING("******* iReplayStage = 4")//PRINTNL()
					ELIF iReplayStage = 4
						//FORKLIFTS
						START_REPLAY_SETUP(<<-125.5439, -2421.8813, 5.0005>> ,49.0800)
						eMissionStage = MISSION_STAGE_5
						PRINTSTRING("******* iReplayStage = 5")//PRINTNL()
					ELIF iReplayStage = 5
						//GO TO CRANE
						START_REPLAY_SETUP(<<-88.8838, -2451.5679, 5.0175>>  ,317.2729)
						eMissionStage = MISSION_STAGE_6
						PRINTSTRING("******* iReplayStage = 6")//PRINTNL()
					ELIF iReplayStage = 6
						//USE CRANE
						START_REPLAY_SETUP(<< -50.2985, -2403.9036, 5.0003 >>   ,191.2160)
						eMissionStage = MISSION_STAGE_7
						PRINTSTRING("******* iReplayStage = 7")//PRINTNL()
					ELIF iReplayStage = 7
						//PHOTOGRAPH BOAT
						START_REPLAY_SETUP(<< -114.5969, -2406.8113, 22.2282 >>   ,359.8527)
						eMissionStage = MISSION_STAGE_8
						PRINTSTRING("******* iReplayStage = 8")//PRINTNL()
					ELIF iReplayStage = 8
						START_REPLAY_SETUP(<<-120.9023, -2411.2688, 5.0007>>   ,195.5543)
						//DRIVE TO OTHER DOCKS
						eMissionStage = MISSION_STAGE_10
						PRINTSTRING("******* iReplayStage = 10")//PRINTNL()
					ELIF iReplayStage = 9
						//DRIVE HOME
						START_REPLAY_SETUP(<<479.0352, -3050.4910, 5.0937>>   , 0.7722 )
						eMissionStage = MISSION_STAGE_11
						PRINTSTRING("******* iReplayStage = 10")//PRINTNL()
					ELIF iReplayStage = 10
						//DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
						eMissionStage =  MISSION_STAGE_PASSED
						//PASSED
						START_REPLAY_SETUP(<<-1153.4308, -1517.1697, 9.6327>>, 126.3936 )
						eMissionStage = MISSION_STAGE_PASSED
						PRINTSTRING("******* iReplayStage = MISSION_STAGE_PASSED")//PRINTNL()
					ELIF iReplayStage = 11
						//DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
						eMissionStage =  MISSION_STAGE_PASSED
						//PASSED
						START_REPLAY_SETUP(<<-1153.4308, -1517.1697, 9.6327>>, 126.3936 )
						eMissionStage = MISSION_STAGE_PASSED
						PRINTSTRING("******* iReplayStage = MISSION_STAGE_PASSED")//PRINTNL()
					ELSE
						SCRIPT_ASSERT("Docks Setup: Trying to replay a mission stage that doesn't exist!")
					ENDIF
				ELSE
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
					eMissionStage =  MISSION_STAGE_PASSED
				ENDIF
			ELSE
				//If the player chose to start from a checkpoint, jump to the last checkpoint reached.
				//eMissionStage = INT_TO_ENUM(MISSION_STAGE_ENUM, (Get_Replay_Mid_Mission_Stage()+1))
				IF iReplayStage = 0
					//DRIVE
					START_REPLAY_SETUP(<<-1156.4092, -1523.9283, 3.2957>>,323.2638)
					eMissionStage = MISSION_STAGE_1
					PRINTSTRING("******* iReplayStage = 1")//PRINTNL()
				ELIF iReplayStage = 2
					//PARK
					START_REPLAY_SETUP(<< 24.4835, -2536.4578, 5.0410 >>,55.2078)
					eMissionStage = MISSION_STAGE_2
					PRINTSTRING("******* iReplayStage = 2")//PRINTNL()
				ELIF iReplayStage = 3
					//FOLLOW FLOYD
					START_REPLAY_SETUP(<< -61.5066, -2525.5120, 5.0101 >>,56.9516)
					eMissionStage = MISSION_STAGE_4
					PRINTSTRING("******* iReplayStage = 3")//PRINTNL()
				ELIF iReplayStage = 4
					//FORKLIFTS
					START_REPLAY_SETUP(<<-125.5439, -2421.8813, 5.0005>> ,49.0800)
					eMissionStage = MISSION_STAGE_5
					PRINTSTRING("******* iReplayStage = 4")//PRINTNL()
				ELIF iReplayStage = 5
					//GO TO CRANE
					START_REPLAY_SETUP(<<-88.8838, -2451.5679, 5.0175>>  ,317.2729)
					eMissionStage = MISSION_STAGE_6
					PRINTSTRING("******* iReplayStage = 5")//PRINTNL()
				ELIF iReplayStage = 6
					//USE CRANE
					START_REPLAY_SETUP(<< -50.2985, -2403.9036, 5.0003 >>   ,191.2160)
					eMissionStage = MISSION_STAGE_7
					PRINTSTRING("******* iReplayStage = 6")//PRINTNL()
				ELIF iReplayStage = 7
					//PHOTOGRAPH BOAT
					START_REPLAY_SETUP(<< -114.5969, -2406.8113, 22.2282 >>   ,359.8527)
					eMissionStage = MISSION_STAGE_8
					PRINTSTRING("******* iReplayStage = 7")//PRINTNL()
				ELIF iReplayStage = 8
					//DRIVE TO OTHER DOCKS
					START_REPLAY_SETUP(<<-120.9023, -2411.2688, 5.0007>>   ,195.5543)
					eMissionStage = MISSION_STAGE_10
					PRINTSTRING("******* iReplayStage = 8")//PRINTNL()
				ELIF iReplayStage = 9
					//DRIVE HOME
					START_REPLAY_SETUP(<<479.0352, -3050.4910, 5.0937>>   , 0.7722 )
					eMissionStage = MISSION_STAGE_11
					PRINTSTRING("******* iReplayStage =9")//PRINTNL()
				ELSE
					SCRIPT_ASSERT("Trevor2: Trying to replay a mission stage that doesn't exist!")
				ENDIF
			ENDIF
			
		ELSE
		
			IF iReplayStage = 0
				IF g_bShitskipAccepted = TRUE
					eMissionStage = MISSION_STAGE_2
					PRINTSTRING("******* jumping past the drive")PRINTNL()
				ELSE
					eMissionStage = MISSION_STAGE_1
					PRINTSTRING("******* iReplayStage = 0")PRINTNL()
				ENDIF
			ELSE
				eMissionStage = MISSION_STAGE_OPENING_CUTSCENE
			ENDIF

		ENDIF
		//SET_PLAYER_START_POSITION(eMissionStage)
	ELSE
		PRINTSTRING("******* Here - 1")PRINTNL()

        // Your mission is being played normally, not being replayed
		eMissionStage = MISSION_STAGE_OPENING_CUTSCENE
	ENDIF
	
	IF eMissionStage != MISSION_STAGE_OPENING_CUTSCENE
		PRINTSTRING("******* Here - 2")PRINTNL()
		WHILE NOT bmissionstageloaded // continues requesting asserts/setup for particular stage until all are loaded
			WAIT(0)
			LOAD_MISSION_STAGE(eMissionStage)	
		ENDWHILE
	ENDIF

	bmissionstageloaded = FALSE
	
ENDPROC

FUNC BOOL MANAGE_GOD_TEXT_AND_DIALOGUE(STRING sGodText, STRING sDialogueBlock, STRING sDialogueRoot)
	IF bDialogue = FALSE
		IF bGodText = FALSE
			IF NOT IS_STRING_NULL(sGodText)
				IF NOT IS_THIS_PRINT_BEING_DISPLAYED(sGodText)
					PRINT_GOD_TEXT(sGodText)
					iGandDTimer = GET_GAME_TIMER()
					bGodText = TRUE
				ENDIF
			ELSE
				bGodText = TRUE
			ENDIF
		ELSE
			//IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
				IF NOT IS_STRING_NULL(sGodText)
					IF MANAGE_MY_TIMER(iGandDTimer,4000)
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED(sGodText)
							IF CREATE_CONVERSATION(sSpeech,sDialogueBlock, sDialogueRoot, CONV_PRIORITY_HIGH )
								bDialogue = TRUE
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF CREATE_CONVERSATION(sSpeech,sDialogueBlock, sDialogueRoot, CONV_PRIORITY_HIGH)
						bDialogue = TRUE
						RETURN TRUE
					ENDIF
				ENDIF
			//ENDIF
		ENDIF
	ENDIF
	
RETURN FALSE
ENDFUNC

PROC MANAGE_DIALOGUE()

	//PRINTSTRING("iMissionDialogue: ")PRINTINT(iMissionDialogue)
		
	SWITCH iMissionDialogue
		
		CASE 0
			//PURPOSE:  Drive to the Docks with floyd, trigger dialogue about the bridge
			IF eMissionStage = MISSION_STAGE_1
				IF IS_VEHICLE_DRIVEABLE(vehMission)
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<352.884644,-2391.758301,8.183699>>, <<351.202393,-2238.528809,12.798820>>, 29.000000)
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<369.152985,-2225.729980,9.336354>>, <<342.831940,-2223.622803,27.368328>>, 86.500000)
							IF SETUP_PEDS_FOR_DIALOGUE()
								IF CREATE_CONVERSATION(sSpeech,"D1AUD", "DS1_01a", CONV_PRIORITY_HIGH)
									REPLAY_RECORD_BACK_FOR_TIME(2.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
									iMissionDialogue ++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			//PURPOSE:  Get to gate

			IF eMissionStage = MISSION_STAGE_1
				iMissionDialogue ++
			ENDIF
		BREAK
		
		CASE 2
			//PURPOSE: Wade gets called off
			IF eMissionStage = MISSION_STAGE_2
				IF iProgress >= 4
					iMissionDialogue++
				ENDIF
			ENDIF
		BREAK
			
		CASE 3

			iMissionDialogue ++

		BREAK
		
		CASE 4

			iMissionDialogue ++

		BREAK
		
		CASE 5
			iMissionDialogue ++
		BREAK
		
		CASE 6

			iMissionDialogue ++
		BREAK
		
		CASE 7
			//PURPOSE: Wander around the docks - That's the freighter I told you about.
			iMissionDialogue ++
		BREAK
		
		CASE 8
			//PURPOSE: Wander around the docks - You, you!  I need drivers on these forklifts. We got crates to clear ASAP.
			IF eMissionStage = MISSION_STAGE_4
				IF bcutsceneplaying
					IF iCutsceneStage > 0
						IF SETUP_PEDS_FOR_DIALOGUE()
							iMissionDialogue ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
		
		CASE 9
			//PURPOSE: Forklifts, player gets into forklift
			IF eMissionStage = MISSION_STAGE_5
				IF IS_VEHICLE_DRIVEABLE(vehHandler)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
						IF SETUP_PEDS_FOR_DIALOGUE()
							iMissionDialogue ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 10
			//PURPOSE: Ok now let's clear the area up and see what we can find.
			IF eMissionStage = MISSION_STAGE_5
				iMissionDialogue ++
			ENDIF
		BREAK
		
		CASE 11
			//PURPOSE: Drive to the crane
			IF eMissionStage = MISSION_STAGE_6
				//Add drive to crane god text.
				IF SETUP_PEDS_FOR_DIALOGUE()
					//IF MANAGE_GOD_TEXT_AND_DIALOGUE("","D1AUD","DS1_08a")
						bDialogue = FALSE
						bGodText = FALSE
						iMissionDialogue ++
					//ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 12
			//PURPOSE: In Crane
			IF eMissionStage = MISSION_STAGE_7
				//Add crane god text 
				IF iProgress > 1	
					IF SETUP_PEDS_FOR_DIALOGUE()
						IF DOES_ENTITY_EXIST(pedDockWorker[14])
						    IF NOT IS_PED_INJURED(pedDockWorker[14])
								ADD_PED_FOR_DIALOGUE(sSpeech, 8, pedDockWorker[14], "SIGNALMAN")
						    ENDIF
					    ELSE
					      	IF sSpeech.PedInfo[8].Index <> NULL
					      		REMOVE_PED_FOR_DIALOGUE(sSpeech, 8)
					     	ENDIF
					    ENDIF
						iMissionDialogue ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 13
			//PURPOSE: Take Photos
			IF eMissionStage = MISSION_STAGE_8
				IF iProgress = 2 
					IF SETUP_PEDS_FOR_DIALOGUE()
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED("AW_TAKE_PHOTO")
							IF SETUP_PEDS_FOR_DIALOGUE()
								//IF CREATE_CONVERSATION(sSpeech,"D1AUD", "DS1_11", CONV_PRIORITY_HIGH)
									iMissionDialogue ++
								//ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 14
			//PURPOSE: Take Photos - One taken
			IF eMissionStage = MISSION_STAGE_8	
				IF iProgress = 4
					IF SETUP_PEDS_FOR_DIALOGUE()
						//KILL_ANY_CONVERSATION()
					//	IF CREATE_CONVERSATION(sSpeech,"D1AUD", "DS1_1", CONV_PRIORITY_HIGH)
							iMissionDialogue ++
						//ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 15
			//PURPOSE: Take Photos - Two Taken
			IF eMissionStage = MISSION_STAGE_8	
				
				IF iProgress = 5
					IF SETUP_PEDS_FOR_DIALOGUE()
						//KILL_ANY_CONVERSATION()
						//CREATE_CONVERSATION(sSpeech,"D1AUD", "DS1_11c", CONV_PRIORITY_HIGH)
						iMissionDialogue ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 16
			//PURPOSE: Photos Done Cutscene
			iMissionDialogue ++
			//Now handled within the cutscene
		BREAK
		
		CASE 17
			//PURPOSE: Drive to other docks
			IF eMissionStage = MISSION_STAGE_10
				IF SETUP_PEDS_FOR_DIALOGUE()
					//IF MANAGE_GOD_TEXT_AND_DIALOGUE("AW_TREVTRU","D1AUD","DS1_15")
						//bDialogue = FALSE
						//bGodText = FALSE
						iMissionDialogue ++
					//ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 18
			
		BREAK
		
	ENDSWITCH


ENDPROC


FUNC BOOL DO_PHONECALL()
    IF NOT bPhoneCallStarted
        IF PLAYER_CALL_CHAR_CELLPHONE( sSpeech, CHAR_RON, "D1AUD", "DS1_12", CONV_PRIORITY_VERY_HIGH)
            DISABLE_HANGUP_FOR_THIS_CALL(TRUE)
            bPhoneCallStarted = TRUE
            SETTIMERA(0)
        ENDIF
    ELSE
        IF HAS_CELLPHONE_CALL_FINISHED()
        	RETURN TRUE
        ENDIF
    ENDIF
    RETURN FALSE
ENDFUNC

PROC CUTSCENE_SPY_ON_DOCKS()
	
	SETTIMERA(0)
	bcutsceneplaying = TRUE
	HIDE_HUD_AND_RADAR_THIS_FRAME()

	WHILE bcutsceneplaying  //holds the script inside here until cutscene is complete

		WAIT(0)
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		
		//CONTROL_SPEECH()
		
		#IF IS_DEBUG_BUILD // allows j/p skip to work even in cutscene
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				bcutsceneplaying = FALSE
			ENDIF
		#ENDIF
		
		IF icutsceneprog >1 
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY() // controls the cutscene skip
			OR HAS_CELLPHONE_CALL_FINISHED()
				SETTIMERB(6001)
				//CONTROL_FADE_OUT(500)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
					DETACH_ENTITY(PLAYER_PED_ID(),TRUE)
					HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
				ENDIF
				icutsceneprog = 6
			ENDIF
		ENDIF
		
		SWITCH icutsceneprog
			
			CASE 0
				bContainerSetUpForDrive = FALSE
				//CREATE_CRANE()
			
				//Player Control
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				CLEAR_PRINTS()
				DISABLE_CELLPHONE(FALSE)
				DESTROY_ALL_CAMS()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				IF NOT IS_PED_INJURED(pedFloyd) AND IS_VEHICLE_DRIVEABLE(vehFloydTruck)
					SET_PED_INTO_VEHICLE(pedFloyd,vehFloydTruck,VS_FRONT_RIGHT)
				ENDIF
			
				REQUEST_MODEL(BIFF)
		
				IF IS_PLAYER_PLAYING(PLAYER_ID())
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(PLAYER_PED_ID())
							DETACH_ENTITY(PLAYER_PED_ID())
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehFloydTruck) AND IS_VEHICLE_DRIVEABLE(vehTrailer2)
						IF IS_VEHICLE_ATTACHED_TO_TRAILER(vehFloydTruck)
						
						ELSE
							ATTACH_VEHICLE_TO_TRAILER(vehFloydTruck,vehTrailer2)
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(s_crane.obj_cabin)
						IF IS_ENTITY_ATTACHED_TO_ENTITY(PLAYER_PED_ID(), s_crane.obj_cabin)
							DETACH_ENTITY(PLAYER_PED_ID())
						ENDIF
					ENDIF
					
					IF NOT IS_PED_INJURED(pedFloyd)
						CLEAR_PED_TASKS_IMMEDIATELY(pedFloyd)
						SET_ENTITY_COORDS(pedFloyd,<<-52.97, -2412.93, 5.00>>)
						SET_ENTITY_HEADING(pedFloyd,91.63)
					ENDIF
					
				ENDIF
		
				s_crane.f_crane_offset = MIN_CRANE_OFFSET
				s_crane.f_crane_vel = 0.0
				
				IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED)
				AND NOT IS_PHONE_ONSCREEN()
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehDocks[0])
					DELETE_VEHICLE(vehDocks[0])
				ENDIF
				
				ADD_PED_FOR_DIALOGUE(sSpeech,0,NULL,"NERVOUSRON")
				bInitStage = TRUE
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				iMissionDialogue = 16	
				icutsceneprog ++
			BREAK
			
			CASE 1
				IF TIMERB() > 1000
					IF SETUP_PEDS_FOR_DIALOGUE()
						KILL_ANY_CONVERSATION()
						IF PLAYER_CALL_CHAR_CELLPHONE( sSpeech, CHAR_RON, "D1AUD", "DS1_12", CONV_PRIORITY_VERY_HIGH)
				            //DISABLE_HANGUP_FOR_THIS_CALL(TRUE)
				            bPhoneCallStarted = TRUE
							//Camera
							IF NOT DOES_CAM_EXIST(camMain)
								camMain = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
							ENDIF
							//BEHIND PLAYER AND DOWN
							SHAKE_CAM(camMain, "HAND_SHAKE", 0.01)
							SET_CAM_PARAMS(camMain,
											<< -112.6306, -2408.0786, 24.3142 >>, << -15.5866, -0.0846, 21.1613 >>,
											47.4496,
											0,
											GRAPH_TYPE_LINEAR,
											GRAPH_TYPE_LINEAR)
											
							SET_CAM_PARAMS(camMain,
											<< -112.6132, -2408.1243, 24.1435 >>, << -15.5866, -0.0846, 21.1613 >>,
											46.4496,
											8200,
											GRAPH_TYPE_LINEAR,
											GRAPH_TYPE_LINEAR)
							
							RENDER_SCRIPT_CAMS(TRUE, FALSE)
							
								
							SETTIMERB(0)
							icutsceneprog ++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				IF HAS_MODEL_LOADED(BIFF)
					
					IF TIMERB() > 7800
						//ZOOM IN ON GUARD
						SHAKE_CAM(camMain, "HAND_SHAKE", 0.01)
						SET_CAM_PARAMS(camMain,
										<< -103.4377, -2396.3796, 14.5640 >>, << -8.5924, -1.4284, 33.1103 >>,
										16.0059,
										0,
										GRAPH_TYPE_LINEAR,
										GRAPH_TYPE_LINEAR)
						
						SET_CAM_PARAMS(camMain,
										<< -109.5450, -2400.3953, 14.3838 >>, << -8.5924, -1.4284, 33.1103 >>,
										15.0059,
										9000,
										GRAPH_TYPE_LINEAR,
										GRAPH_TYPE_LINEAR)
						
						SETTIMERB(0)
						
						IF NOT DOES_ENTITY_EXIST(vehDocks[0])
							SPAWN_VEHICLE(vehDocks[0],BIFF,<< -42.4866, -2397.1814, 5.0003 >>, 91.1803)
						ENDIF
						
						
						REQUEST_VEHICLE_RECORDING(1,"AWDocks8")
						icutsceneprog ++
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF TIMERB() > 8200
					//REAR PAN
					SHAKE_CAM(camMain, "HAND_SHAKE", 0.05)
					SET_CAM_PARAMS(camMain,
									<< -165.5269, -2278.7788, 22.0802 >>, << -3.7631, 0.0397, -151.0943 >>,
									11.2438,
									0,
									GRAPH_TYPE_LINEAR,
									GRAPH_TYPE_LINEAR)
					
					SET_CAM_PARAMS(camMain,
									<< -180.5885, -2287.0940, 22.0683 >>, << -3.7631, 0.0397, -151.0943 >>,
									11.0438,
									15000,
									GRAPH_TYPE_LINEAR,
									GRAPH_TYPE_LINEAR)
					SETTIMERB(0)
					
					IF DOES_ENTITY_EXIST(vehDocks[0])
						IF IS_VEHICLE_DRIVEABLE(vehDocks[0])
							IF NOT IS_PED_INJURED(pedDockWorker[13])
								SET_PED_INTO_VEHICLE(pedDockWorker[13],vehDocks[0])
							ENDIF
						ENDIF
					ENDIF
					icutsceneprog ++
				ENDIF
			BREAK
			
			CASE 4
				IF TIMERB() > 10000
					//PAN BEHIND CRANE LOOKING AT SHIP
					SHAKE_CAM(camMain, "HAND_SHAKE", 0.01)
					SET_CAM_PARAMS(camMain,
									<< -74.0709, -2442.3132, 21.5670 >>, << -6.4020, -1.4284, 46.5438 >>,
									16.0059,
									0,
									GRAPH_TYPE_LINEAR,
									GRAPH_TYPE_LINEAR)
					
					SET_CAM_PARAMS(camMain,
									<< -65.6630, -2433.3894, 21.8708 >>, << -6.4020, -1.4284, 46.5438 >>,
									16.4059,
									24000,
									GRAPH_TYPE_LINEAR,
									GRAPH_TYPE_LINEAR)
					SETTIMERB(0)
					IF DOES_ENTITY_EXIST(vehDocks[0])
						IF IS_VEHICLE_DRIVEABLE(vehDocks[0])
							IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDocks8")
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehDocks[0])
									START_PLAYBACK_RECORDED_VEHICLE(vehDocks[0],1,"AWDocks8")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					icutsceneprog ++
				ENDIF
			BREAK
			
			CASE 5
				IF TIMERB() > 3000
					IF HAS_CELLPHONE_CALL_FINISHED()
						SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -105.3935, -2404.4043, 4.9999 >>  ,TRUE)
						SET_ENTITY_HEADING(PLAYER_PED_ID(),58.9704)
						//BEHIND PED LOOK AT FRONT OF SHIP
						SHAKE_CAM(camMain, "HAND_SHAKE", 0.01)
						SET_CAM_PARAMS(camMain,
										<< -131.2179, -2396.0811, 8.8231 >>, << -3.1815, -1.4284, -137.3740 >>,
									23.6303,
										0,
										GRAPH_TYPE_LINEAR,
										GRAPH_TYPE_LINEAR)
						
						SET_CAM_PARAMS(camMain,
										<< -126.9600, -2392.1514, 8.6789 >>, << -3.1815, -1.4284, -137.3740 >>,
										23.6303,
										3500,
										GRAPH_TYPE_LINEAR,
										GRAPH_TYPE_LINEAR)
						SETTIMERB(0)
						icutsceneprog ++
					ENDIF
				ENDIF
			BREAK

			CASE 6
		
				IF TIMERB() > 3500
				
					IF DOES_CAM_EXIST(camMain)
						SET_CAM_ACTIVE(camMain,FALSE)
						RENDER_SCRIPT_CAMS(FALSE,FALSE)
					ENDIF

					IF IS_VEHICLE_DRIVEABLE(vehMission)
						TASK_LEAVE_VEHICLE(PLAYER_PED_ID(),vehMission)
					ENDIF
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					SET_WIDESCREEN_BORDERS(FALSE,0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					CONTROL_FADE_IN(500)
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					CLEAR_PRINTS()
					PRINTSTRING("quad cut end")
					IF DOES_CAM_EXIST(camMain)
						DESTROY_CAM(camMain)
					ENDIF
					icutsceneprog = 0
					
					SET_WIDESCREEN_BORDERS(FALSE,0)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
					SET_TIME_SCALE(1.0)
					DISPLAY_RADAR(TRUE)

					IF IS_PLAYER_PLAYING(PLAYER_ID())
						SET_PLAYER_CONTROL (PLAYER_ID(), TRUE)
					ENDIF
			
					IF DOES_ENTITY_EXIST(vehDocks[0])
						DELETE_VEHICLE(vehDocks[0])
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehDocks[1])
						DELETE_VEHICLE(vehDocks[1])
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehDocks[2])
						DELETE_VEHICLE(vehDocks[2])
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehDocks[3])
						DELETE_VEHICLE(vehDocks[3])
					ENDIF
					
					IF DOES_ENTITY_EXIST(vehDocks[4])
						DELETE_VEHICLE(vehDocks[4])
					ENDIF
			
					SET_MODEL_AS_NO_LONGER_NEEDED(BIFF)
					SET_MODEL_AS_NO_LONGER_NEEDED(FORKLIFT)
					
					TASK_USE_MOBILE_PHONE(PLAYER_PED_ID(),FALSE)
									
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					DISPLAY_HUD(TRUE)
					DISABLE_CELLPHONE(FALSE)
					//SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_RON, "TXT_PHOTO3", TXTMSG_UNLOCKED)
						
					CLEAR_PRINTS()
					KILL_ANY_CONVERSATION()
					CLEAR_HELP()
					
					HIDE_ACTIVE_PHONE(FALSE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					KILL_ANY_CONVERSATION()
					brunfailchecks = FALSE
					bcutsceneplaying = FALSE
					
				ENDIF
			BREAK

		ENDSWITCH
		
	ENDWHILE

ENDPROC


PROC CLEAR_UP_EVENTS_AFTER_WALK_AND_TALK()
	IF NOT IS_PED_INJURED(pedFloyd)
		CLEAR_PED_TASKS(pedFloyd)
		CLEAR_PED_ALTERNATE_WALK_ANIM(pedFloyd)
		CLEAR_PED_ALTERNATE_WALK_ANIM(PLAYER_PED_ID())
	ENDIF
	
	IF bSetPiece[SS_STATIC_GANTRY_GUYS] = TRUE
		bSetPiece[SS_STATIC_GANTRY_GUYS] = FALSE
	ENDIF
	IF bSetPiece[SS_ARRIVE_AT_DOCKS] = TRUE
		bSetPiece[SS_ARRIVE_AT_DOCKS] = FALSE
	ENDIF
	IF bSetPiece[SS_FLOYD_GRABS_PACKAGE] = TRUE
		bSetPiece[SS_FLOYD_GRABS_PACKAGE] = FALSE
	ENDIF
	IF bSetPiece[SS_GANTRY_GUYS] = TRUE
		bSetPiece[SS_GANTRY_GUYS] = FALSE
	ENDIF
	IF bSetPiece[SS_dock_workers_on_pipe] = TRUE
		bSetPiece[SS_dock_workers_on_pipe] = FALSE
	ENDIF
	IF bSetPiece[Ss_dock_workers_talking] = TRUE
		bSetPiece[Ss_dock_workers_talking] = FALSE
	ENDIF
	IF bSetPiece[SS_guys_around_car] = TRUE
		bSetPiece[SS_guys_around_car] = FALSE
	ENDIF
	
	REPEAT COUNT_OF(s_sv_car_to_admire) iCount
		CLEAN_UP_SETPIECE_VEHILCE(s_sv_car_to_admire[iCount],TRUE,FALSE)
	ENDREPEAT
	
	REPEAT COUNT_OF(s_sp_dock_workers_on_pipe) iCount
		CLEAN_UP_SETPIECE_PED(s_sp_dock_workers_on_pipe[iCount],TRUE,FALSE)
	ENDREPEAT
	
	REPEAT COUNT_OF(s_sp_dock_workers_talking) iCount
		CLEAN_UP_SETPIECE_PED(s_sp_dock_workers_talking[iCount],TRUE,FALSE)
	ENDREPEAT
	
	REPEAT COUNT_OF(s_sp_floyd_grabs_package) iCount
		CLEAN_UP_SETPIECE_PED(s_sp_floyd_grabs_package[iCount],TRUE,FALSE)
	ENDREPEAT
	
	REPEAT COUNT_OF(s_sp_guys_around_car) iCount
		CLEAN_UP_SETPIECE_PED(s_sp_guys_around_car[iCount],TRUE,FALSE)
	ENDREPEAT
	
	REPEAT COUNT_OF(s_sp_gantry_guys_dynamic) iCount
		CLEAN_UP_SETPIECE_PED(s_sp_gantry_guys_dynamic[iCount],TRUE,FALSE)
	ENDREPEAT
	
	REPEAT COUNT_OF(s_sp_gantry_guys_static) iCount
		CLEAN_UP_SETPIECE_PED(s_sp_gantry_guys_static[iCount],TRUE,FALSE)
	ENDREPEAT
	
	REMOVE_ANIM_DICT("missheistdockssetup1ig_2_p1@new_structure")	
	REMOVE_ANIM_DICT("missdocksshowoffcar@idle_a")
	REMOVE_ANIM_DICT("missdocksshowoffcar@idle_b")
	REMOVE_ANIM_DICT("missdocksshowoffcar@base")
	
	REQUEST_ANIM_DICT("missheistdockssetup1ig_9@main_action")
	REQUEST_ANIM_DICT("missheistdockssetup1ig_14")
	
	IF IS_VEHICLE_DRIVEABLE(vehHandler)
		SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehHandler,FALSE)
	ENDIF
ENDPROC

INT ipedDockWorker13Timer
INT iAmbientDialogueTimerDock3
INT iAmbientDialogueTimerDock4

PROC MANAGE_DOCK_WORKER_AMBIENT_DIALOGUE()
	
	//WELDER {dock 1 sees Floyd} 
	IF DOES_ENTITY_EXIST(s_sp_welder[0].ped)
		IF NOT IS_PED_INJURED(s_sp_welder[0].ped)
			IF NOT IS_PED_INJURED(pedFloyd)
				IF MANAGE_MY_TIMER(iAmbientDialogueTimer, GET_RANDOM_INT_IN_RANGE(8000,10000))
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedFloyd),GET_ENTITY_COORDS(s_sp_welder[0].ped)) < 10
						IF NOT IS_PED_INJURED(s_sp_welder[0].ped)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_welder[0].ped,"DH1_CRAA","DOCKWORKER1",SPEECH_PARAMS_FORCE_NORMAL)
							iAmbientDialogueTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	//PED ON PHONE - {dock 3 is on the phone} - 1
	IF DOES_ENTITY_EXIST(pedDockWorker[5])
		IF NOT IS_PED_INJURED(pedDockWorker[5])
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(pedDockWorker[5])) < 10
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CUAA")
					IF NOT IS_AMBIENT_SPEECH_PLAYING(pedDockWorker[5])
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedDockWorker[5],"DH1_CUAA","DS1DOCKW3",SPEECH_PARAMS_FORCE)
						SET_LABEL_AS_TRIGGERED("DH1_CUAA",TRUE)
						PRINTSTRING("DH1_CUAA")PRINTNL()
					ENDIF
				ELSE
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CUAB")
						IF NOT IS_AMBIENT_SPEECH_PLAYING(pedDockWorker[5])
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedDockWorker[5],"DH1_CUAB","DS1DOCKW3",SPEECH_PARAMS_FORCE)
							SET_LABEL_AS_TRIGGERED("DH1_CUAB",TRUE)
							PRINTSTRING("DH1_CUAB")PRINTNL()
						ENDIF
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CUAC")
							IF NOT IS_AMBIENT_SPEECH_PLAYING(pedDockWorker[5])
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedDockWorker[5],"DH1_CUAC","DS1DOCKW3",SPEECH_PARAMS_FORCE)
								SET_LABEL_AS_TRIGGERED("DH1_CUAC",TRUE)
								PRINTSTRING("DH1_CUAC")PRINTNL()
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CUAD")
								IF NOT IS_AMBIENT_SPEECH_PLAYING(pedDockWorker[5])
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedDockWorker[5],"DH1_CUAD","DS1DOCKW3",SPEECH_PARAMS_FORCE)
									SET_LABEL_AS_TRIGGERED("DH1_CUAD",TRUE)
									PRINTSTRING("DH1_CUAD")PRINTNL()
									iAmbientDialogueTimerDock3 = GET_GAME_TIMER()
								ENDIF
							ELSE
								IF MANAGE_MY_TIMER(iAmbientDialogueTimerDock3, GET_RANDOM_INT_IN_RANGE(8000,15000))
									SET_LABEL_AS_TRIGGERED("DH1_CUAD",FALSE)
									SET_LABEL_AS_TRIGGERED("DH1_CUAC",FALSE)
									SET_LABEL_AS_TRIGGERED("DH1_CUAB",FALSE)
									SET_LABEL_AS_TRIGGERED("DH1_CUAA",FALSE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//WORKING ON PIPE
	IF DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[0].ped)
		IF NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[0].ped)
		AND NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[1].ped)
		AND NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[2].ped)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(s_sp_dock_workers_on_pipe[0].ped)) < 10
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CYAA")
					IF NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_dock_workers_on_pipe[0].ped)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_dock_workers_on_pipe[0].ped,"DH1_CYAA","DS1DOCKW4",SPEECH_PARAMS_FORCE)
						SET_LABEL_AS_TRIGGERED("DH1_CYAA",TRUE)
						PRINTSTRING("DH1_CYAA")PRINTNL()
					ENDIF
				ELSE
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CYAB")
						IF NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_dock_workers_on_pipe[0].ped)
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_dock_workers_on_pipe[1].ped,"DH1_CYAB","DS1DOCKW5",SPEECH_PARAMS_FORCE)
							SET_LABEL_AS_TRIGGERED("DH1_CYAB",TRUE)
							PRINTSTRING("DH1_CYAB")PRINTNL()
						ENDIF
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CYAC")
							IF NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_dock_workers_on_pipe[1].ped)
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_dock_workers_on_pipe[0].ped,"DH1_CYAC","DS1DOCKW4",SPEECH_PARAMS_FORCE)
								SET_LABEL_AS_TRIGGERED("DH1_CYAC",TRUE)
								PRINTSTRING("DH1_CYAC")PRINTNL()
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CYAD")
								IF NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_dock_workers_on_pipe[0].ped)
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_dock_workers_on_pipe[2].ped,"DH1_CYAD","DS1DOCKW6",SPEECH_PARAMS_FORCE)
									SET_LABEL_AS_TRIGGERED("DH1_CYAD",TRUE)
									PRINTSTRING("DH1_CYAD")PRINTNL()
								ENDIF
							ELSE
								IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CYAE")
									IF NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_dock_workers_on_pipe[2].ped)
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_dock_workers_on_pipe[0].ped,"DH1_CYAE","DS1DOCKW4",SPEECH_PARAMS_FORCE)
										SET_LABEL_AS_TRIGGERED("DH1_CYAE",TRUE)
										PRINTSTRING("DH1_CYAE")PRINTNL()
										
									ENDIF
								ELSE
									IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CYAF")
										IF NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_dock_workers_on_pipe[0].ped)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_dock_workers_on_pipe[2].ped,"DH1_CYAF","DS1DOCKW6",SPEECH_PARAMS_FORCE)
											SET_LABEL_AS_TRIGGERED("DH1_CYAF",TRUE)
											PRINTSTRING("DH1_CYAF")PRINTNL()
											
										ENDIF
									ELSE
										IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CYAG")
											IF NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_dock_workers_on_pipe[2].ped)
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_dock_workers_on_pipe[0].ped,"DH1_CYAG","DS1DOCKW4",SPEECH_PARAMS_FORCE)
												SET_LABEL_AS_TRIGGERED("DH1_CYAG",TRUE)
												PRINTSTRING("DH1_CYAG")PRINTNL()
												
											ENDIF
										ELSE
											IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CYAH")
												IF NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_dock_workers_on_pipe[0].ped)
													PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_dock_workers_on_pipe[2].ped,"DH1_CYAH","DS1DOCKW6",SPEECH_PARAMS_FORCE)
													SET_LABEL_AS_TRIGGERED("DH1_CYAH",TRUE)
													PRINTSTRING("DH1_CYAH")PRINTNL()
													
												ENDIF
											ELSE
												IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CYAI")
													IF NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_dock_workers_on_pipe[2].ped)
														PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_dock_workers_on_pipe[1].ped,"DH1_CYAI","DS1DOCKW5",SPEECH_PARAMS_FORCE)
														SET_LABEL_AS_TRIGGERED("DH1_CYAI",TRUE)
														PRINTSTRING("DH1_CYAI")PRINTNL()
														
													ENDIF
												ELSE
													IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CYAJ")
														IF NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_dock_workers_on_pipe[1].ped)
															PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_dock_workers_on_pipe[0].ped,"DH1_CYAJ","DS1DOCKW4",SPEECH_PARAMS_FORCE)
															SET_LABEL_AS_TRIGGERED("DH1_CYAJ",TRUE)
															PRINTSTRING("DH1_CYAJ")PRINTNL()
															iAmbientDialogueTimerDock4 = GET_GAME_TIMER()
														ENDIF
													ELSE
														IF MANAGE_MY_TIMER(iAmbientDialogueTimerDock4, GET_RANDOM_INT_IN_RANGE(8000,15000))
															SET_LABEL_AS_TRIGGERED("DH1_CYAJ",TRUE)
															SET_LABEL_AS_TRIGGERED("DH1_CYAI",TRUE)
															SET_LABEL_AS_TRIGGERED("DH1_CYAH",TRUE)
															SET_LABEL_AS_TRIGGERED("DH1_CYAF",TRUE)
															SET_LABEL_AS_TRIGGERED("DH1_CYAE",TRUE)
															SET_LABEL_AS_TRIGGERED("DH1_CYAD",TRUE)
															SET_LABEL_AS_TRIGGERED("DH1_CYAC",TRUE)
															SET_LABEL_AS_TRIGGERED("DH1_CYAB",TRUE)
															SET_LABEL_AS_TRIGGERED("DH1_CYAA",TRUE)
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//PED SMOKING - {dock4 sees Trevor}
	IF DOES_ENTITY_EXIST(pedDockWorker[7])
		IF NOT IS_PED_INJURED(pedDockWorker[7])
			IF MANAGE_MY_TIMER(iAmbientDialogueTimer, GET_RANDOM_INT_IN_RANGE(8000,10000))
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(pedDockWorker[7])) < 10
					//TASK_TURN_PED_TO_FACE_ENTITY(pedDockWorker[7],PLAYER_PED_ID())
					TASK_LOOK_AT_ENTITY(pedDockWorker[7],PLAYER_PED_ID(),5000)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedDockWorker[7],"DH1_CZAA","DS1DOCKW4",SPEECH_PARAMS_FORCE)
					iAmbientDialogueTimer = GET_GAME_TIMER()		
					PRINTSTRING("DH1_CZAA")PRINTNL()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(s_sp_floyd_grabs_package[0].ped)
	AND DOES_ENTITY_EXIST(s_sp_floyd_grabs_package[1].ped)
	AND DOES_ENTITY_EXIST(s_sp_floyd_grabs_package[2].ped)
		IF NOT IS_PED_INJURED(s_sp_floyd_grabs_package[0].ped)
		AND NOT IS_PED_INJURED(s_sp_floyd_grabs_package[1].ped)
		AND NOT IS_PED_INJURED(s_sp_floyd_grabs_package[2].ped)
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(s_sp_floyd_grabs_package[0].ped)) < 10
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_COAA")
					IF NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_floyd_grabs_package[0].ped)
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_floyd_grabs_package[0].ped,"DH1_COAA","DS1DOCKWORKER1",SPEECH_PARAMS_FORCE)
						SET_LABEL_AS_TRIGGERED("DH1_COAA",TRUE)
						PRINTSTRING("DH1_COAA")PRINTNL()
					ENDIF
				ELSE
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_COAB")
						IF NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_floyd_grabs_package[1].ped)
						AND NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_floyd_grabs_package[0].ped)
							IF NOT IS_PED_INJURED(s_sp_floyd_grabs_package[1].ped)
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_floyd_grabs_package[1].ped,"DH1_COAB","DS1DOCKW2",SPEECH_PARAMS_FORCE)
								SET_LABEL_AS_TRIGGERED("DH1_COAB",TRUE)
								PRINTSTRING("DH1_COAB")PRINTNL()
							ENDIF
						ENDIF
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_COAC")
							IF NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_floyd_grabs_package[0].ped)
							AND NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_floyd_grabs_package[1].ped)
								IF NOT IS_PED_INJURED(s_sp_floyd_grabs_package[0].ped)
									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_floyd_grabs_package[0].ped,"DH1_COAC","DS1DOCKWORKER1",SPEECH_PARAMS_FORCE)
									SET_LABEL_AS_TRIGGERED("DH1_COAC",TRUE)
									PRINTSTRING("DH1_COAC")PRINTNL()
								ENDIF
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_COAD")
								IF NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_floyd_grabs_package[1].ped)
								AND NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_floyd_grabs_package[0].ped)
									IF NOT IS_PED_INJURED(s_sp_floyd_grabs_package[1].ped)
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_floyd_grabs_package[1].ped,"DH1_COAD","DS1DOCKW2",SPEECH_PARAMS_FORCE)
										ipedDockWorker13Timer = GET_GAME_TIMER()
										SET_LABEL_AS_TRIGGERED("DH1_COAD",TRUE)
										PRINTSTRING("DH1_COAD")PRINTNL()
									ENDIF
								ENDIF
							ELSE
								IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_COAE")
									IF NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_floyd_grabs_package[0].ped)
									AND NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_floyd_grabs_package[1].ped)
										IF NOT IS_PED_INJURED(s_sp_floyd_grabs_package[0].ped)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_floyd_grabs_package[0].ped,"DH1_COAE","DS1DOCKWORKER1",SPEECH_PARAMS_FORCE)
											ipedDockWorker13Timer = GET_GAME_TIMER()
											SET_LABEL_AS_TRIGGERED("DH1_COAE",TRUE)
											PRINTSTRING("DH1_COAE")PRINTNL()
										ENDIF
									ENDIF
								ELSE
									IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_COAF")
										IF NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_floyd_grabs_package[1].ped)
										AND NOT IS_AMBIENT_SPEECH_PLAYING(s_sp_floyd_grabs_package[0].ped)
											IF NOT IS_PED_INJURED(s_sp_floyd_grabs_package[1].ped)
												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(s_sp_floyd_grabs_package[1].ped,"DH1_COAF","DS1DOCKW2",SPEECH_PARAMS_FORCE)
												ipedDockWorker13Timer = GET_GAME_TIMER()
												SET_LABEL_AS_TRIGGERED("DH1_COAF",TRUE)
												PRINTSTRING("DH1_COAF")PRINTNL()
											ENDIF
										ENDIF
									ELSE
										IF MANAGE_MY_TIMER(ipedDockWorker13Timer, 25000)
											SET_LABEL_AS_TRIGGERED("DH1_COAA",FALSE)
											SET_LABEL_AS_TRIGGERED("DH1_COAB",FALSE)
											SET_LABEL_AS_TRIGGERED("DH1_COAC",FALSE)
											SET_LABEL_AS_TRIGGERED("DH1_COAD",FALSE)
											SET_LABEL_AS_TRIGGERED("DH1_COAE",FALSE)
											SET_LABEL_AS_TRIGGERED("DH1_COAF",FALSE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF
	
	//PED IN LOADING AREA - {dock 3 sees T and F}
	IF DOES_ENTITY_EXIST(pedDockWorker[13])
		IF NOT IS_PED_INJURED(pedDockWorker[13])
			IF eMissionStage = MISSION_STAGE_5
				IF bCarryingContainer = TRUE
					IF IS_VEHICLE_DRIVEABLE(vehHandler)
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehHandler),GET_ENTITY_COORDS(pedDockWorker[13])) < 15
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(pedDockWorker[13])) < 8
								IF MANAGE_MY_TIMER(iAmbientDialogueTimer, GET_RANDOM_INT_IN_RANGE(8000,12000))
									IF NOT IS_PED_INJURED(pedDockWorker[13])
										TASK_TURN_PED_TO_FACE_ENTITY(pedDockWorker[13],PLAYER_PED_ID())
										TASK_LOOK_AT_ENTITY(pedDockWorker[13],PLAYER_PED_ID(),5000)
										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedDockWorker[13],"DH1_AYAA","SUPERVISOR2",SPEECH_PARAMS_FORCE)
										iAmbientDialogueTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(vehHandler)
						IF IS_VEHICLE_DRIVEABLE(vehHandler)
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehHandler),GET_ENTITY_COORDS(pedDockWorker[13])) < 15
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(pedDockWorker[13])) < 12
									IF MANAGE_MY_TIMER(iAmbientDialogueTimer, GET_RANDOM_INT_IN_RANGE(8000,12000))
										IF NOT IS_PED_INJURED(pedDockWorker[13])
											TASK_TURN_PED_TO_FACE_ENTITY(pedDockWorker[13],PLAYER_PED_ID())
											TASK_LOOK_AT_ENTITY(pedDockWorker[13],PLAYER_PED_ID(),5000)
											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedDockWorker[13],"DH1_AZAA","SUPERVISOR2",SPEECH_PARAMS_FORCE)
											iAmbientDialogueTimer = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				//NO LONGER ON PHONE
//				IF DOES_ENTITY_EXIST(pedDockWorker[13])
//					IF NOT IS_PED_INJURED(pedDockWorker[13])
//						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(pedDockWorker[13])) < 15
//							IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CVAA")
//								IF NOT IS_AMBIENT_SPEECH_PLAYING(pedDockWorker[13])
//									PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedDockWorker[13],"DH1_CVAA","DS1DOCKW3","SPEECH_PARAMS_FORCE")
//									SET_LABEL_AS_TRIGGERED("DH1_CVAA",TRUE)
//								ENDIF
//							ELSE
//								IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CVAB")
//									IF NOT IS_AMBIENT_SPEECH_PLAYING(pedDockWorker[13])
//										PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedDockWorker[13],"DH1_CVAB","DS1DOCKW3","SPEECH_PARAMS_FORCE")
//										SET_LABEL_AS_TRIGGERED("DH1_CVAB",TRUE)
//									ENDIF
//								ELSE
//									IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CVAC")
//										IF NOT IS_AMBIENT_SPEECH_PLAYING(pedDockWorker[13])
//											PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedDockWorker[13],"DH1_CVAC","DS1DOCKW3","SPEECH_PARAMS_FORCE")
//											SET_LABEL_AS_TRIGGERED("DH1_CVAC",TRUE)
//										ENDIF
//									ELSE
//										IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_CVAD")
//											IF NOT IS_AMBIENT_SPEECH_PLAYING(pedDockWorker[13])
//												PLAY_PED_AMBIENT_SPEECH_WITH_VOICE_NATIVE(pedDockWorker[13],"DH1_CVAD","DS1DOCKW3","SPEECH_PARAMS_FORCE")
//												ipedDockWorker13Timer = GET_GAME_TIMER()
//												SET_LABEL_AS_TRIGGERED("DH1_CVAD",TRUE)
//											ENDIF
//										ELSE
//											IF MANAGE_MY_TIMER(ipedDockWorker13Timer, 15000)
//												SET_LABEL_AS_TRIGGERED("DH1_CVAA",FALSE)
//												SET_LABEL_AS_TRIGGERED("DH1_CVAB",FALSE)
//												SET_LABEL_AS_TRIGGERED("DH1_CVAC",FALSE)
//												SET_LABEL_AS_TRIGGERED("DH1_CVAD",FALSE)
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
			ENDIF
		ENDIF
	ENDIF
	

ENDPROC

PROC IS_PED_IN_DOCKS_AREA()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<85.606041,-2512.530273,4.390195>>, <<-78.482826,-2401.053223,18.251068>>, 76.000000)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<15.037971,-2537.353271,4.401426>>, <<-230.935333,-2374.307129,21.569214>>, 76.000000)
	OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<65.696030,-2539.073730,4.405972>>, <<12.275644,-2502.675293,18.257154>>, 22.750000)
	
	ENDIF
ENDPROC

PROC SET_CUTSCENE_PED_COMPONENT_VARIATIONS(STRING strCutsceneName)
	INT iNameHash = GET_HASH_KEY(strCutsceneName)

	IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
		IF iNameHash = HASH("LSDH_INT")
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID())
			
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Floyd", g_sTriggerSceneAssets.ped[0])
				PRINTSTRING("SETTING g_sTriggerSceneAssets.ped[0] variations")PRINTNL()
			ENDIF
			
			IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Wade", g_sTriggerSceneAssets.ped[1])
				PRINTSTRING("SETTING g_sTriggerSceneAssets.ped[1] variations")PRINTNL()
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedFloyd)
				IF NOT IS_PED_INJURED(pedFloyd)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Floyd", pedFloyd)
					PRINTSTRING("SETTING pedFloyd variations")PRINTNL()
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedWade)
				IF NOT IS_PED_INJURED(pedWade)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Wade", pedWade)
					PRINTSTRING("SETTING pedWade variations")PRINTNL()
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
ENDPROC


INT iTrevorComponentSwitch
PED_INDEX pedTrevor
ENTITY_INDEX entity_trevor
INT iLegText
INT iLegDraw


PROC MANAGE_TREVOR_COMPONENT_SWITCH()
	
	PRINTSTRING("iTrevorComponentSwitch: ")PRINTINT(iTrevorComponentSwitch)PRINTNL()
	
	SWITCH iTrevorComponentSwitch
	
		CASE 0
			IF IS_CUTSCENE_PLAYING()
				IF NOT DOES_ENTITY_EXIST(pedTrevor)
					entity_trevor = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Trevor")
					IF DOES_ENTITY_EXIST(entity_trevor)
						pedTrevor = GET_PED_INDEX_FROM_ENTITY_INDEX(entity_trevor)
					ENDIF
				ELSE
					iTrevorComponentSwitch ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			iLegDraw = GET_PED_DRAWABLE_VARIATION(pedTrevor, PED_COMP_LEG) 
			iLegText = GET_PED_TEXTURE_VARIATION(pedTrevor, PED_COMP_LEG) 
			PRINTSTRING("Saving -iLegDraw:")PRINTINT(iLegDraw)PRINTNL()
			PRINTSTRING("Saving -iLegText:")PRINTINT(iLegText)PRINTNL()
			SET_PED_PRELOAD_VARIATION_DATA(pedTrevor,PED_COMP_LEG, 25, 0)
			//SET_PED_PRELOAD_VARIATION_DATA(pedTrevor,PED_COMP_LEG, iLegDraw, iLegText)
			iTrevorComponentSwitch ++
		BREAK
		
		CASE 2
			IF GET_CUTSCENE_TIME() > 54435
				IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(pedTrevor)
					SET_PED_COMPONENT_VARIATION(pedTrevor, PED_COMP_LEG, 25, 0)
					SET_PED_PRELOAD_VARIATION_DATA(pedTrevor,PED_COMP_LEG, iLegDraw, iLegText)
					iTrevorComponentSwitch ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF GET_CUTSCENE_TIME() > 76453
				IF HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(pedTrevor)
					PRINTSTRING("Restoring -iLegDraw:")PRINTINT(iLegDraw)PRINTNL()
					PRINTSTRING("Restoring -iLegText:")PRINTINT(iLegText)PRINTNL()
					SET_PED_COMPONENT_VARIATION(pedTrevor, PED_COMP_LEG, iLegDraw, iLegText)
					iTrevorComponentSwitch ++
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC


/// PURPOSE:
/// Play the into mocap
PROC OPENING_CUTSCENE()

	bcutsceneplaying = TRUE

		#IF IS_DEBUG_BUILD
			IF IS_CUTSCENE_ACTIVE()
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
					STOP_CUTSCENE()
					REMOVE_CUTSCENE()
					WHILE IS_CUTSCENE_ACTIVE()
						WAIT(0)
					ENDWHILE
				ENDIF
			ENDIF
		#ENDIF
		
		#IF NOT IS_JAPANESE_BUILD
			MANAGE_TREVOR_COMPONENT_SWITCH()
		#ENDIF

		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
			SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
		ENDIF

		IF e_section_stage = SECTION_STAGE_SETUP
			IF b_is_jumping_directly_to_stage
				CONTROL_FADE_IN(500)
				b_is_jumping_directly_to_stage = FALSE
			ELSE
				IF i_current_event = 0

					PRINTSTRING("PLAYING OPENING CUTSCENE - 1")PRINTNL()
					//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(0,"Opening Mocap") 
					bcutsceneplaying = TRUE
					
					IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[0])
						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0],TRUE,TRUE)
						pedFloyd = g_sTriggerSceneAssets.ped[0]
					ENDIF
					
					IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[1],TRUE,TRUE)
						pedWade = g_sTriggerSceneAssets.ped[1]
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(veh_pre_mission_car)
					
					ENDIF
					
					IF IS_REPEAT_PLAY_ACTIVE()
						WHILE NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_FLOYD, vfloydStart, ffloydStart )
						OR NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_WADE, vWadeStart, fWadeStart )
							WAIT(0)
						ENDWHILE
					ENDIF

					REQUEST_MODEL(GET_PLAYER_VEH_MODEL(CHAR_TREVOR, VEHICLE_TYPE_CAR))
					PRINTSTRING("REQUEST_CUTSCENE(LSDH_INT)")PRINTNL()
					//REMOVE_CUTSCENE()
					REQUEST_CUTSCENE("LSDH_INT")
					
					SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR)
					
					bClearCutscenArea = FALSE
					i_current_event++
				ELIF i_current_event = 1
					//Bug 1839805 Need to check for CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()returning true for repeat play.
					IF NOT IS_REPEAT_PLAY_ACTIVE()
					OR CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						PRINTSTRING("CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY() - TRUE")PRINTNL()
					
						//Components
						IF IS_REPEAT_PLAY_ACTIVE()
							SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
						ELSE
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Trevor", PLAYER_PED_ID())
						ENDIF
						IF IS_REPEAT_PLAY_ACTIVE()
							IF NOT IS_PED_INJURED(pedFloyd)
								SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Floyd", pedFloyd)
								PRINTSTRING("SETTING default Floyd variations")PRINTNL()
							ENDIF
						ELSE	
							IF NOT IS_PED_INJURED(pedFloyd)
								SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Floyd", pedFloyd)
								PRINTSTRING("SETTING g_sTriggerSceneAssets.ped[0] variations")PRINTNL()
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(pedWade)
							SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Wade", pedWade)
							PRINTSTRING("SETTING g_sTriggerSceneAssets.ped[1] variations")PRINTNL()
						ENDIF
						i_current_event++
					ELSE
						PRINTSTRING("CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY() - FALSE")PRINTNL()
					ENDIF
				ELIF i_current_event = 2
					IF HAS_CUTSCENE_LOADED()
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ELSE
							WAIT(0)
						ENDIF
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF NOT IS_PED_INJURED(pedWade)
							REGISTER_ENTITY_FOR_CUTSCENE(pedWade, "Wade", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF NOT IS_PED_INJURED(pedFloyd)
							REGISTER_ENTITY_FOR_CUTSCENE(pedFloyd, "Floyd", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
//						IF IS_VEHICLE_DRIVEABLE(vehMission)
//							REGISTER_ENTITY_FOR_CUTSCENE(vehMission, "Trevors_Lorry", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, DUMMY_MODEL_FOR_SCRIPT, CEO_IS_CASCADE_SHADOW_FOCUS_ENTITY_DURING_EXIT)
//						ENDIF

						//1987148 - Turn off TV subtitles during the cutscene. NOTE: it should be fine to not reset this, as by default subtitles should be off and 
						//the TV/cinema scripts turn them back on when needed. Resetting it to TRUE could potentially cause more issues.
						ENABLE_MOVIE_SUBTITLES(FALSE)

						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
						SET_CUTSCENE_FADE_VALUES(FALSE,FALSE,FALSE,FALSE)
						START_CUTSCENE()
						MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_DOCKS_1)
						PRINTSTRING("PLAYING OPENING CUTSCENE - 2")PRINTNL()
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						iTrevorComponentSwitch = 0
						b_skipped_mocap = FALSE
						e_section_stage = SECTION_STAGE_RUNNING
					ELSE
						PRINTSTRING("CUTSCENE LOADING")PRINTNL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_RUNNING
			IF NOT b_skipped_mocap
				IF IS_CUTSCENE_SKIP_BUTTON_PRESSED()
					//b_skipped_mocap = TRUE
				ENDIF
			ENDIF
			
			IF IS_CUTSCENE_PLAYING()
				IF bClearCutscenArea = FALSE
					IF IS_REPEAT_PLAY_ACTIVE()
						CONTROL_FADE_IN(500)
					ENDIF
					//Move the last vehicle somewhere if it's in the way.
					veh_pre_mission_car = GET_PLAYERS_LAST_VEHICLE()
					
					DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
					DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<1187,-1398,4.3>>, 10)
					DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_TREVOR_SAVEHOUSE_CITY)
					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1167.186768,-1506.043579,2.992951>>, <<-1146.530762,-1533.760376,6.597246>>, 9.000000,<<-1187.1825, -1491.1740, 3.3797>>, 304.3311,<<5,5,3>>)
					//RESOLVE_VEHICLES_AT_MISSION_TRIGGER(<<-1161.9291, -1525.1603, 3.2477>>, 35.1845,FALSE)
					//1396587
					//RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1167.186768,-1506.043579,2.992951>>, <<-1146.530762,-1533.760376,6.597246>>, 9.000000,<<-1187.1825, -1491.1740, 3.3797>>, 304.3311 )
					
					CLEAR_AREA(<< -1154.6117, -1520.0699, 3.3456 >>, 200.0, TRUE)
					DELETE_ALL_SCRIPT_CREATED_PLAYER_VEHICLES(CHAR_TREVOR)
					STOP_GAMEPLAY_HINT(TRUE)
					SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
					bClearCutscenArea = TRUE
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TOD SWITCH")
					IF GET_CUTSCENE_TIME() >= 111300
						SET_CLOCK_TIME(6,00,00)
						MANAGE_PLAYER_OUTFIT()
						SET_LABEL_AS_TRIGGERED("TOD SWITCH",TRUE)
					ENDIF
				ENDIF
			ENDIF
			
			IF bClearCutscenArea = TRUE
				PRINTSTRING("RUNNING OPENING CUTSCENE - 3")PRINTNL()
				SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_TREVORS_TRUCK, << -1155.02, -1522.81, 3.3397 >>  ,38.40 )
				
				IF NOT DOES_ENTITY_EXIST(pedWade)
					ENTITY_INDEX entity_wade = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Wade")
					IF DOES_ENTITY_EXIST(entity_wade)
						pedWade = GET_PED_INDEX_FROM_ENTITY_INDEX(entity_wade)
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(pedFloyd)
					ENTITY_INDEX entity_floyd = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Floyd_Wades_Cousin")
					IF DOES_ENTITY_EXIST(entity_floyd)
						pedFloyd = GET_PED_INDEX_FROM_ENTITY_INDEX(entity_floyd)
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("LSDH_INT CAMERA EXIT")
//					IF CAN_SET_EXIT_STATE_FOR_CAMERA()
//						PRINTSTRING("CAN_SET_EXIT_STATE_FOR_CAMERA")PRINTNL()
//						SET_LABEL_AS_TRIGGERED("LSDH_INT - CAMERA EXIT",TRUE) 
//					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("LSDH_INT TREVOR EXIT")
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
						IF IS_SCREEN_FADED_OUT()
							MANAGE_PLAYER_OUTFIT()
						ENDIF
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_ForceDirectEntry,TRUE)
						IF IS_VEHICLE_DRIVEABLE(vehMission)
							TASK_ENTER_VEHICLE(PLAYER_PED_ID(),vehMission,DEFAULT_TIME_NEVER_WARP,VS_DRIVER,PEDMOVEBLENDRATIO_WALK,ECF_BLOCK_SEAT_SHUFFLING)
						ENDIF
						//FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
						//SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1000, 115.3521, FALSE)
						PRINTSTRING("CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(Trevor)")PRINTNL()
						SET_LABEL_AS_TRIGGERED("LSDH_INT TREVOR EXIT",TRUE) 
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Floyd")
					IF DOES_ENTITY_EXIST(pedFloyd)
						IF NOT IS_PED_INJURED(pedFloyd)
							IF IS_SCREEN_FADED_OUT()
								SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_HEAD, 0, 0)
								SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_BERD, 0, 0)
								SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_HAIR, 1, 0)
								SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_TORSO, 1, 0)
								SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_LEG, 1, 0)
								SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_HAND, 1, 0)
								SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_FEET, 0, 0)
								SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_TEETH, 0, 0)
								SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_SPECIAL, 0, 0)
								SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_SPECIAL2, 0, 0)
								SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_DECL, 0, 0)
								SET_PED_COMPONENT_VARIATION(pedFloyd, PED_COMP_JBIB, 0, 0)
							ENDIF

							FORCE_PED_MOTION_STATE(pedFloyd, MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
							PRINTSTRING("CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY Floyd")PRINTNL()
						ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Wade")
					IF DOES_ENTITY_EXIST(pedWade)
						IF NOT IS_PED_INJURED(pedWade)
							IF IS_SCREEN_FADED_OUT()
								SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_HEAD, 0, 0)
								SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_BERD, 0, 0)
								SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_HAIR, 1, 0)
								SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_TORSO, 1, 0)
								SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_LEG, 1, 0)
								SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_HAND, 1, 0)
								SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_FEET, 0, 0)
								SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_TEETH, 0, 0)
								SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_SPECIAL, 1, 0)
								SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_SPECIAL2, 0, 0)
								SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_DECL, 1, 0)
								SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_JBIB, 0, 0)
							ENDIF
							FORCE_PED_MOTION_STATE(pedWade, MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
							PRINTSTRING("CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY Wade")PRINTNL()
						ENDIF
					ENDIF
				ENDIF

				IF HAS_CUTSCENE_FINISHED()
				AND bClearCutscenArea = TRUE
					e_section_stage = SECTION_STAGE_CLEANUP
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF IS_CUTSCENE_ACTIVE()
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
						STOP_CUTSCENE()
						REMOVE_CUTSCENE()
						WHILE IS_CUTSCENE_ACTIVE()
							WAIT(0)
						ENDWHILE
					ENDIF
				ENDIF
			#ENDIF
			
		ENDIF

		IF e_section_stage = SECTION_STAGE_CLEANUP
			
						
			IF b_skipped_mocap
				REPLAY_CANCEL_EVENT()
				//If the player skipped the mocap the peds need to be warped.
				STOP_CUTSCENE()
				//Make sure necessary assets get made.
				IF NOT IS_SCREEN_FADING_OUT()
				OR NOT IS_SCREEN_FADED_OUT()
					CONTROL_FADE_OUT(500)
				ENDIF
				
				IF IS_SCREEN_FADING_OUT()
				OR IS_SCREEN_FADED_OUT()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ENDIF
				
				WHILE IS_CUTSCENE_ACTIVE()
					WAIT(0)
				ENDWHILE
				
				//Make sure necessary assets get made.
				WHILE NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_FLOYD, vfloydStart, ffloydStart )
				OR NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_WADE, vWadeStart, fWadeStart )
				OR NOT SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_TREVORS_TRUCK, << -1155.02, -1522.81, 3.3397 >>  ,38.40 )
					WAIT(0)
				ENDWHILE
				
				IF IS_VEHICLE_DRIVEABLE(vehMission)
					IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
						IF IS_VEHICLE_SEAT_FREE(vehMission)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehMission)
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(pedFloyd)
						IF IS_VEHICLE_SEAT_FREE(vehMission,VS_FRONT_RIGHT)
							SET_ENTITY_COORDS(pedFloyd,vfloydStart)
							SET_ENTITY_HEADING(pedFloyd, ffloydStart)
							SET_PED_INTO_VEHICLE(pedFloyd,vehMission,VS_FRONT_RIGHT)
						ENDIF
					ENDIF

					IF NOT IS_ENTITY_DEAD(pedWade)
						IF IS_VEHICLE_SEAT_FREE(vehMission,VS_BACK_LEFT)
							SET_ENTITY_COORDS(pedWade, vfloydStart) 
							SET_PED_INTO_VEHICLE(pedWade,vehMission,VS_BACK_LEFT)
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehMission)
						//SET_VEHICLE_ON_GROUND_PROPERLY(vehMission)
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(vehMission)
					SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehMission,FALSE)
					SET_VEHICLE_DISABLE_TOWING(vehMission,TRUE)
				ENDIF

			ENDIF
			
			REPLAY_STOP_EVENT()
			
			//Setup buddy relationship groups etc.
			INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			
			i_current_event = 0
			PRINTSTRING("JUMPING TO STAGE 1")
			eMissionStage = MISSION_STAGE_1
			e_section_stage = SECTION_STAGE_SETUP
			SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
			SET_VEHICLE_POPULATION_BUDGET(3)
			SET_PED_POPULATION_BUDGET(3)
			bcutsceneplaying = FALSE
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_SKIP
			STOP_CUTSCENE()
			b_skipped_mocap = TRUE
			e_section_stage = SECTION_STAGE_RUNNING
		ENDIF

ENDPROC

PROC MAKE_THE_SECURITY_GUARD_WHEN_NEAR_TO_THE_DOCKS()
	IF NOT HAS_LABEL_BEEN_TRIGGERED("CREATE DOCKS SECURITY")
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),vDocks) < 100
			SETUP_MISSION_REQUIREMENT(REQ_SECURITY)
			REQUEST_ANIM_DICT("misslsdhs")
			REQUEST_ANIM_DICT("misslsdhsclipboard@idle_a")
			REQUEST_ANIM_DICT("misslsdhsclipboard@base")
			REQUEST_MODEL(P_CS_Clipboard)
			REQUEST_MODEL(PROP_PENCIL_01)
			IF HAS_ANIM_DICT_LOADED("misslsdhs")
			AND HAS_ANIM_DICT_LOADED("misslsdhsclipboard@idle_a")
			AND HAS_ANIM_DICT_LOADED("misslsdhsclipboard@base")
			AND HAS_MODEL_LOADED(P_CS_Clipboard)
			AND HAS_MODEL_LOADED(PROP_PENCIL_01)
				IF IS_VEHICLE_DRIVEABLE(vehMission) 
					IF NOT DOES_ENTITY_EXIST(objClipboard[0])
						objClipboard[0] = CREATE_OBJECT(P_CS_CLIPBOARD,vDocks)
					ELSE
						IF NOT DOES_ENTITY_EXIST(objPencil[0])
							objPencil[0] = CREATE_OBJECT(PROP_PENCIL_01,vDocks)
						ELSE
							IF DOES_ENTITY_EXIST(pedSecurity)
								IF NOT IS_PED_INJURED(pedSecurity)
									CLEAR_ALL_PED_PROPS(pedSecurity) 
									ATTACH_ENTITY_TO_ENTITY(objPencil[0],pedSecurity,GET_PED_BONE_INDEX(pedSecurity, BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>,TRUE,TRUE)
									ATTACH_ENTITY_TO_ENTITY(objClipboard[0],pedSecurity,GET_PED_BONE_INDEX(pedSecurity, BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>,TRUE,TRUE,TRUE)
									SET_ENTITY_NO_COLLISION_ENTITY(objClipboard[0],pedSecurity,TRUE)
									TASK_PLAY_ANIM(pedSecurity,"misslsdhsclipboard@base", "base",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,-1,AF_LOOPING |AF_USE_KINEMATIC_PHYSICS)
									ADD_PED_FOR_DIALOGUE(sSpeech, 1, pedSecurity, "GATEGUARD")
									SET_RAGDOLL_BLOCKING_FLAGS(pedSecurity,RBF_PLAYER_IMPACT)
									SET_PED_CAN_EVASIVE_DIVE(pedSecurity,FALSE)
									SET_LABEL_AS_TRIGGERED("CREATE DOCKS SECURITY",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:  Drive to the Docks with floyd
PROC MISSION_STAGE_1_PROC()

	CREATE_THE_DOCKS_STUFF()
	MAKE_THE_SECURITY_GUARD_WHEN_NEAR_TO_THE_DOCKS()
	
	SWITCH iProgress
		CASE 0//Create objective specific items- print objective, add blips etc
			IF SETUP_PEDS_FOR_DIALOGUE()
			AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_FLOYD, vfloydStart, ffloydStart )
			AND SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_WADE, vWadeStart, fWadeStart )
				DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
				bDialogue = FALSE
				iMissionDialogue = 0
				//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Drive to the docks")
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				bIsJumpingDirectlyToStage = FALSE
				INITALISE_ARRAYS()
				bWaveIn = FALSE
			//	SET_BIT (BitSet_CellphoneDisplay, g_BS_FORCE_CELLPHONE_CAM_AUTO_CONTINUE)
				bDialogue = FALSE
				bGodText = FALSE
				SETTIMERA(0)
				
				g_bDocksBackGatesIgnoreTrevorsUniform = TRUE
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(P_CUT_DOOR_02,<<-63.19, -2519.27, 7.79>>,TRUE,0,0,0)
				
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				
				IF IS_SCREEN_FADED_OUT()
					IF NOT HAS_LABEL_BEEN_TRIGGERED("TOD SWITCH")
						SET_CLOCK_TIME(6,00,00)
						SET_LABEL_AS_TRIGGERED("TOD SWITCH",TRUE)
					ENDIF
					
					//LOAD_SCENE(<<-1156.5227, -1523.5519, 3.3044>>)
					IF IS_VEHICLE_DRIVEABLE(vehMission)
//						SET_ENTITY_COORDS(vehMission,<<-1155.32, -1522.79, 4.27>>)
//						SET_ENTITY_HEADING(vehMission,38.40)
//						SET_VEHICLE_ON_GROUND_PROPERLY(vehMission)
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
								SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-1156.5227, -1523.5519, 3.3044>>)
								SET_ENTITY_HEADING(PLAYER_PED_ID(),306.1213)
							ELSE
								SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-1156.8860, -1524.1942, 3.2833>>)
								SET_ENTITY_HEADING(PLAYER_PED_ID(),313.4019)
								SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 100)
							ENDIF
//							IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(PLAYER_PED_ID(),vehMission,VS_DRIVER)
//								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),vehMission,VS_DRIVER)
//							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(pedFloyd)
							IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(pedFloyd,vehMission,VS_FRONT_RIGHT)
								SET_PED_INTO_VEHICLE(pedFloyd,vehMission,VS_FRONT_RIGHT)
							ENDIF
						ENDIF
						IF NOT IS_PED_INJURED(pedWade)
							IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(pedWade,vehMission,VS_BACK_LEFT)
								SET_PED_INTO_VEHICLE(pedWade,vehMission,VS_BACK_LEFT)
							ENDIF
						ENDIF
					ENDIF
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
					IF IS_THIS_TV_ON(TV_LOC_TREVOR_VENICE)
						FORCE_STOP_TV(TV_LOC_TREVOR_VENICE)
					ENDIF
					
				ENDIF
				iProgress = 99
			ENDIF
		BREAK
		
		//CLEAR PRINTS CHECK ANIM REQUESTS AND SET UP SECURITY GUY
		CASE 99

			IF IS_SCREEN_FADED_OUT()
				MANAGE_PLAYER_OUTFIT()
			ENDIF
			
			SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM, BUILDINGSTATE_DESTROYED)
			SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_RASPBERRY_JAM_CUTSCENE, BUILDINGSTATE_NORMAL)
			ADD_SCENARIO_BLOCKING_AREA(<<18.380921,-2524.460205,10.000374>>,<<10.180921,-2534.460205,4.800374>>)
			CLEAR_AREA(vDocks,40,TRUE)
			
			//LOCK GATE
			UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_DOCKS_FRONT_GATE_OUT,PLAYER_PED_ID())
			DOOR_SYSTEM_SET_HOLD_OPEN(g_sAutoDoorData[AUTODOOR_DOCKS_FRONT_GATE_OUT].doorID, FALSE)
			DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_DOCKS_FRONT_GATE_OUT].doorID, 0.0)
			DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_DOCKS_FRONT_GATE_OUT].doorID,DOORSTATE_LOCKED)
			
			INIT_STAGE()
			DISABLE_VEHICLE_GEN_ON_MISSION(FALSE)
			bGoToSpeach = FALSE

			iProgress = 1

		BREAK

		//HAS THE PLAYER ARRIVED AT THE DOCKS
		CASE 1
			IF IS_SCREEN_FADED_OUT()
			AND NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_IN(500)
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DH_1_DRIVE_START")
				IF IS_VEHICLE_DRIVEABLE(vehMission)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehMission)
						START_AUDIO_SCENE("DH_1_DRIVE_START")
						SET_LABEL_AS_TRIGGERED("DH_1_DRIVE_START",TRUE)
					ENDIF
				ENDIF
			ELSE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DH_1_DRIVE_APPROACH_DOCKS")
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<352.884644,-2391.758301,8.183699>>, <<351.202393,-2238.528809,12.798820>>, 29.000000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<369.152985,-2225.729980,9.336354>>, <<342.831940,-2223.622803,27.368328>>, 86.500000)
						IF IS_AUDIO_SCENE_ACTIVE("DH_1_DRIVE_START")
							STOP_AUDIO_SCENE("DH_1_DRIVE_START")
						ELSE
							START_AUDIO_SCENE("DH_1_DRIVE_APPROACH_DOCKS")
							SET_LABEL_AS_TRIGGERED("DH_1_DRIVE_APPROACH_DOCKS",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("TURN OFF RAGDOLL")
				IF NOT IS_PED_INJURED(pedWade)
					IF IS_PED_SITTING_IN_VEHICLE_SEAT(pedWade,vehMission,VS_BACK_LEFT)
						SET_RAGDOLL_BLOCKING_FLAGS(pedWade,RBF_NONE)
						SET_PED_CAN_RAGDOLL(pedWade,TRUE)
						SET_LABEL_AS_TRIGGERED("TURN OFF RAGDOLL",TRUE)
					ENDIF
				ENDIF
			ENDIF

			IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData,vDocks,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,TRUE,pedFloyd,pedWade,NULL,vehMission,"AW_GO_DOCKS","AW_DONT_LEAVE_T","AW_DONT_LEAVE_W","","AW_PICK_UP_B","AW_YTREVTRU","AW_GETBACK_IN",FALSE,TRUE,TRUE)
				
				REPLAY_RECORD_BACK_FOR_TIME(10.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				
				bGodText = FALSE
				bGoToSpeach = FALSE
				KILL_ANY_CONVERSATION()
				iProgress++
			ELSE
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					
					IF HAS_LABEL_BEEN_TRIGGERED("DS1_01b")
						SET_LABEL_AS_TRIGGERED("DS1_01b",FALSE)
					ENDIF
					
					IF NOT IS_PED_INJURED(pedFloyd)
						SET_PED_CAN_RAGDOLL(pedFloyd,TRUE)
					ENDIF
					
					IF NOT IS_PED_INJURED(pedWade)
						SET_PED_CAN_RAGDOLL(pedWade,TRUE)
					ENDIF
					
					IF IS_THIS_PRINT_BEING_DISPLAYED("AW_YTREVTRU")
						CLEAR_PRINTS()
					ENDIF
					
					//DS1_00b
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF bGoToSpeach = FALSE
							IF SETUP_PEDS_FOR_DIALOGUE()
								IF GET_FAILS_COUNT_TOTAL_FOR_THIS_MISSION_SCRIPT() = 0
									IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_01", CONV_PRIORITY_MEDIUM)
										bGoToSpeach = TRUE
									ENDIF
								ELSE
									IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_01", CONV_PRIORITY_MEDIUM)
										bGoToSpeach = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<11.943290,-2544.413086,4.799920>>, <<23.731033,-2528.028809,8.649944>>, 22.250000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<11.943290,-2544.413086,4.799920>>, <<23.731033,-2528.028809,8.649944>>, 22.250000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<10.628066,-2533.844971,7.540140>>, <<32.967224,-2550.335449,19.500061>>, 16.000000)
						
						IF bWaveIn = FALSE
							REQUEST_ANIM_DICT("misslsdhsclipboard@idle_a")
							IF HAS_ANIM_DICT_LOADED("misslsdhsclipboard@idle_a")
								IF NOT IS_PED_INJURED(pedSecurity)
									OPEN_SEQUENCE_TASK(seqMain)
										TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),-1)
										//TASK_TURN_PED_TO_FACE_ENTITY(NULL,PLAYER_PED_ID())
										TASK_PLAY_ANIM(NULL,"misslsdhs", "reaction_to_truck",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_DEFAULT | AF_USE_KINEMATIC_PHYSICS)
										TASK_PLAY_ANIM(NULL,"misslsdhs", "wave_truck",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_DEFAULT | AF_USE_KINEMATIC_PHYSICS)
										TASK_PLAY_ANIM(NULL,"misslsdhsclipboard@idle_a", "idle_a",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
									CLOSE_SEQUENCE_TASK(seqMain)
									TASK_PERFORM_SEQUENCE(pedSecurity, seqMain)
									bWaveIn = TRUE
								ENDIF
							ENDIF
						ENDIF
						
						BRING_VEHICLE_TO_HALT(vehMission,11.0,1)
						bGodText = FALSE
						REQUEST_IPL("Talklaugh_Pipe")
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						KILL_FACE_TO_FACE_CONVERSATION()
						iProgress++
					ENDIF
					
					
				ELSE

					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_01b")
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_01b", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("DS1_01b",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_00")
						IF DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_00", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("DS1_00",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//MANAGE PAUSE DIALOGUE
				IF bGoToSpeach = TRUE
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
							CLEAR_PRINTS()
						ENDIF
					//Play conversation
					ELSE
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
								CLEAR_PRINTS()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
		
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_02")
				IF SETUP_PEDS_FOR_DIALOGUE()
					KILL_FACE_TO_FACE_CONVERSATION()
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(sSpeech,"D1AUD", "DS1_02", CONV_PRIORITY_HIGH)
							REPLAY_RECORD_BACK_FOR_TIME(2.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
							SET_LABEL_AS_TRIGGERED("DS1_02",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(vehMission)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehMission)
						IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),"misslsdhs", "reaction_from_truck_fds")
							STOP_ANIM_TASK(PLAYER_PED_ID(),"misslsdhs", "reaction_from_truck_fds")
						ENDIF
					ENDIF
				ENDIF
				
	//			IF NOT IS_PED_INJURED(pedWade)
	//				OPEN_SEQUENCE_TASK(seqMain)
	//					TASK_PLAY_ANIM(NULL,"misslsdhs", "reaction_from_truck_rds",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_DEFAULT)
	//				CLOSE_SEQUENCE_TASK(seqMain)
	//				TASK_PERFORM_SEQUENCE(pedWade, seqMain)
	//			ENDIF
				
				IF NOT IS_PED_INJURED(pedFloyd)
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_PLAY_ANIM(NULL,"misslsdhs", "reaction_from_truck_fps",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_DEFAULT)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(pedFloyd, seqMain)
				ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_PLAY_ANIM(NULL,"misslsdhs", "reaction_from_truck_fds",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_DEFAULT|AF_SECONDARY|AF_UPPERBODY)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqMain)
				ENDIF
				
				bSetPiece[SS_dock_workers_on_pipe] = TRUE
				bSetPiece[SS_guys_around_car] = TRUE
				bSetPiece[SS_STATIC_GANTRY_GUYS] = TRUE
				REMOVE_ANIM_DICT("misslsdhs")
				REMOVE_ANIM_DICT("misslsdhsclipboard@idle_a")
				REMOVE_ANIM_DICT("misslsdhsclipboard@base")
				g_bDocksBackGatesIgnoreTrevorsUniform = FALSE
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				iProgress++
			ENDIF
		BREAK
		
		CASE 3
		
			IF IS_VEHICLE_DRIVEABLE(vehMission)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehMission)
					IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),"misslsdhs", "reaction_from_truck_fds")
						STOP_ANIM_TASK(PLAYER_PED_ID(),"misslsdhs", "reaction_from_truck_fds")
					ENDIF
				ENDIF
			ENDIF
			
			IF PRE_STREAM_MISSION_STAGE()
				IF NOT IS_PED_INJURED(pedFloyd)
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedFloyd)
				ENDIF
				IF NOT IS_PED_INJURED(pedWade)
					CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedWade)
				ENDIF
				RESET_MISSION_STAGE_VARIABLES()
				CLEAR_PLAYER_HAS_DAMAGED_AT_LEAST_ONE_NON_ANIMAL_PED(PLAYER_ID())
				ADVANCE_MISSION_STAGE()
			ENDIF
		
		BREAK

	ENDSWITCH		
	
ENDPROC


PROC MANAGE_GATE_SECURITY_GUY()
	
	SWITCH iGateSecurityGuy
	
		CASE 0
	
			IF NOT IS_PED_INJURED(pedSecurity)
				REQUEST_ANIM_DICT("misslsdhs")
				REQUEST_ANIM_DICT("misslsdhsclipboard@idle_a")
				iGateSecurityGuy ++
			ENDIF
			
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(pedSecurity)
				IF HAS_ANIM_DICT_LOADED("misslsdhsclipboard@idle_a")
				AND HAS_ANIM_DICT_LOADED("misslsdhs")
					IF GET_SCRIPT_TASK_STATUS(pedSecurity,SCRIPT_TASK_PERFORM_SEQUENCE) != PERFORMING_TASK 
						iSecurityTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_PED_INJURED(pedSecurity)
				IF MANAGE_MY_TIMER(iSecurityTimer,1000)
					IF IS_ENTITY_PLAYING_ANIM(pedSecurity,"misslsdhsclipboard@idle_a", "idle_a")
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<6.580805,-2543.854736,5.050115>>, <<19.957510,-2525.047852,6.009008>>, 20.000000)
							REQUEST_ANIM_DICT("misslsdhs")
							REQUEST_ANIM_DICT("misslsdhsclipboard@idle_a")
							IF HAS_ANIM_DICT_LOADED("misslsdhs")
							AND HAS_ANIM_DICT_LOADED("misslsdhsclipboard@idle_a")
								IF NOT IS_PED_INJURED(pedSecurity)
									OPEN_SEQUENCE_TASK(seqMain)
										TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),-1)
										TASK_PLAY_ANIM(NULL,"misslsdhs", "reaction_to_truck",NORMAL_BLEND_IN,SLOW_BLEND_OUT,-1,AF_DEFAULT | AF_USE_KINEMATIC_PHYSICS)
										TASK_PLAY_ANIM(NULL,"misslsdhs", "wave_truck",NORMAL_BLEND_IN,SLOW_BLEND_OUT,-1,AF_DEFAULT | AF_USE_KINEMATIC_PHYSICS)
										TASK_PLAY_ANIM(NULL,"misslsdhsclipboard@idle_a", "idle_a",NORMAL_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
									CLOSE_SEQUENCE_TASK(seqMain)
									TASK_PERFORM_SEQUENCE(pedSecurity, seqMain)
									iGateSecurityGuy ++
								ENDIF
							ENDIF
						ENDIF
					ELSE
						REQUEST_ANIM_DICT("misslsdhsclipboard@idle_a")
						IF HAS_ANIM_DICT_LOADED("misslsdhsclipboard@idle_a")
							IF NOT IS_PED_INJURED(pedSecurity)
								OPEN_SEQUENCE_TASK(seqMain)
									TASK_PLAY_ANIM(NULL,"misslsdhsclipboard@idle_a", "idle_a",NORMAL_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING | AF_USE_KINEMATIC_PHYSICS)
								CLOSE_SEQUENCE_TASK(seqMain)
								TASK_PERFORM_SEQUENCE(pedSecurity, seqMain)
								iGateSecurityGuy ++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
	
	ENDSWITCH
	
ENDPROC

INT iGetOutTimer
INT iWarningTimer

//PURPOSE: Player parks up and get's changed
PROC MISSION_STAGE_2_PROC() 
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_02b")
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-39.359322,-2382.808838,4.250790>>, <<-130.746674,-2514.060303,14.204694>>, 83.000000)
		OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-291.898834,-2400.596191,4.250148>>, <<-124.852646,-2449.263184,7.767045>>, 144.000000)
			IF GET_RANDOM_BOOL()
				IF CREATE_CONVERSATION(sSpeech,"D1AUD", "DS1_02b", CONV_PRIORITY_MEDIUM)
					iWarningTimer = GET_GAME_TIMER()
					SET_LABEL_AS_TRIGGERED("DS1_02b",TRUE)
				ENDIF
			ELSE
				IF CREATE_CONVERSATION(sSpeech,"D1AUD", "DS1_38", CONV_PRIORITY_MEDIUM)
					iWarningTimer = GET_GAME_TIMER()
					SET_LABEL_AS_TRIGGERED("DS1_38",TRUE)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF MANAGE_MY_TIMER(iWarningTimer,GET_RANDOM_INT_IN_RANGE(12000,22000))
			SET_LABEL_AS_TRIGGERED("DS1_02b",FALSE)
		ENDIF
	ENDIF

	IF iProgress > 2
		IF MANAGE_MY_TIMER(iGetOutTimer,4000)
			IF IS_VEHICLE_DRIVEABLE(vehMission)			
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehMission)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehMission, FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					TASK_LEAVE_VEHICLE(PLAYER_PED_ID(),vehMission)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	MANAGE_GATE_SECURITY_GUY()
	CREATE_THE_DOCKS_STUFF()
	
	IF iProgress > 4
		IF DOES_ENTITY_EXIST(pedWade)
			IF NOT IS_PED_INJURED(pedWade)
				IF WOULD_ENTITY_BE_OCCLUDED(IG_WADE,GET_ENTITY_COORDS(pedWade))
				OR NOT IS_ENTITY_ON_SCREEN(pedWade)
					DELETE_PED(pedWade)
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_WADE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehMission)
		IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehMission)
			IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(),"misslsdhs", "reaction_from_truck_fds")
				STOP_ANIM_TASK(PLAYER_PED_ID(),"misslsdhs", "reaction_from_truck_fds")
			ENDIF
		ENDIF
	ENDIF
	
	IF iProgress > 1
		DISABLE_CELLPHONE(TRUE)
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.5)
		IF NOT HAS_LABEL_BEEN_TRIGGERED("DH_1_FOLLOW_FLOYD_START")
			IF IS_AUDIO_SCENE_ACTIVE("DH_1_DRIVE_APPROACH_DOCKS")
				STOP_AUDIO_SCENE("DH_1_DRIVE_APPROACH_DOCKS")
			ELSE
				START_AUDIO_SCENE("DH_1_FOLLOW_FLOYD_START")
				SET_LABEL_AS_TRIGGERED("DH_1_FOLLOW_FLOYD_START",TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	//PRINTSTRING("s_sp_supervisor1[0].i_event")PRINTINT(s_sp_supervisor1[0].i_event)//PRINTNL()
	
	SWITCH iProgress
	
		CASE 0//Create objective specific items- print objective, add blips etc
			IF SETUP_PEDS_FOR_DIALOGUE()
				IF IS_SCREEN_FADED_OUT()
					MANAGE_PLAYER_OUTFIT()
					IF NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedFloyd)
					AND NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedWade)
						WAIT(0)
					ENDIF
				ENDIF
				IF IS_SCREEN_FADED_OUT()
					iGateSecurityGuy = 0
				ENDIF
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Park up")

				bSetPiece[SS_dock_workers_on_pipe] = TRUE
				bSetPiece[SS_STATIC_GANTRY_GUYS] = TRUE
				bSetPiece[SS_guys_around_car] = TRUE
				bSetPiece[SS_GANTRY_GUYS] = TRUE
				iMissionDialogue = 2
				REQUEST_ANIM_DICT("missheistdockssetup1ig_2_p1@new_structure")
				REQUEST_IPL("pcranecont")
				g_bDocksBackGatesIgnoreTrevorsUniform = FALSE
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(P_CUT_DOOR_02,<<-63.19, -2519.27, 7.79>>,TRUE,0,0,0)
				
				bIsJumpingDirectlyToStage = FALSE
				INIT_STAGE()
//				SET_VEHICLE_POPULATION_BUDGET(2)
//				SET_PED_POPULATION_BUDGET(2)
				SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
				IF bWaveIn = FALSE
					REQUEST_ANIM_DICT("misslsdhsclipboard@idle_a")
					REQUEST_ANIM_DICT("misslsdhs")
					IF HAS_ANIM_DICT_LOADED("misslsdhsclipboard@idle_a")
					AND HAS_ANIM_DICT_LOADED("misslsdhs")
						IF NOT IS_PED_INJURED(pedSecurity)
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_PLAY_ANIM(NULL,"misslsdhs", "wave_truck",REALLY_SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_DEFAULT| AF_USE_KINEMATIC_PHYSICS)
								TASK_PLAY_ANIM(NULL,"misslsdhs", "reaction_to_truck",NORMAL_BLEND_IN,SLOW_BLEND_OUT,-1,AF_DEFAULT| AF_USE_KINEMATIC_PHYSICS)
								TASK_PLAY_ANIM(NULL,"misslsdhsclipboard@idle_a", "idle_a",NORMAL_BLEND_IN,SLOW_BLEND_OUT,-1,AF_DEFAULT| AF_USE_KINEMATIC_PHYSICS)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(pedSecurity, seqMain)
							bWaveIn = TRUE
						ENDIF
					ENDIF
				ENDIF
				iProgress++
			ENDIF
		BREAK	

		CASE 1
						
			IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData,vParkUpHere,<<0.001,0.001,LOCATE_SIZE_HEIGHT>>,TRUE,pedFloyd,pedWade,NULL,vehMission,"AW_GO_PARK","AW_DONT_LEAVE_T","AW_DONT_LEAVE_W","","AW_PICK_UP_B","AW_DOCK1","AW_GETBACK_IN",FALSE,FALSE)
				bSetPiece[SS_ARRIVE_AT_DOCKS] = TRUE
				bGodText = FALSE
				bDialogue = FALSE
				KILL_ANY_CONVERSATION()
				REMOVE_BLIP(sLocatesData.LocationBlip)
				REQUEST_ANIM_DICT("missheistdockssetup1ig_2_p1@start_idle")
				iGetOutTimer = GET_GAME_TIMER()
				SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_LEAVE_CAMERA_CONTROL_ON)
				iProgress++
			ELSE
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF (IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-70.137924,-2523.794189,4.010104>>, <<-56.489647,-2533.662354,9.010103>>, 7.500000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-60.260574,-2528.604736,4.010104>>, <<-55.287464,-2531.858643,9.010103>>, 11.500000))
					AND CAN_PLAYER_START_CUTSCENE(TRUE,TRUE)
						IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehMission,5,-1)
							bSetPiece[SS_ARRIVE_AT_DOCKS] = TRUE
							bGodText = FALSE
							bDialogue = FALSE
							KILL_ANY_CONVERSATION()
							REMOVE_BLIP(sLocatesData.LocationBlip)
							REQUEST_ANIM_DICT("missheistdockssetup1ig_2_p1@start_idle")
							iGetOutTimer = GET_GAME_TIMER()
							SET_PLAYER_CONTROL(PLAYER_ID(),FALSE,SPC_LEAVE_CAMERA_CONTROL_ON)
							iProgress++
						ENDIF
					ENDIF
				ENDIF
				
				IF bClearTasks = FALSE
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1.572346,-2541.401611,4.841759>>, <<14.998902,-2521.360352,6.507655>>, 2.250000)
					OR NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						IF NOT IS_PED_INJURED(pedFloyd)
							CLEAR_PED_TASKS(pedFloyd)
						ENDIF
						IF NOT IS_PED_INJURED(pedWade)
							CLEAR_PED_TASKS(pedWade)
						ENDIF
						bClearTasks = TRUE
					ENDIF
				ENDIF
				
			ENDIF
		BREAK
		
		CASE 2
			IF IS_VEHICLE_DRIVEABLE(vehMission)
				REQUEST_ANIM_DICT("missheistdockssetup1ig_2_p1@start_idle")
				IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_2_p1@start_idle")
					IF SETUP_PEDS_FOR_DIALOGUE()
						SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(P_CUT_DOOR_02,<<-63.19, -2519.27, 7.79>>,FALSE,0,0,0)
						IF IS_THIS_PRINT_BEING_DISPLAYED("AW_GO_PARK")
							CLEAR_PRINTS()
						ENDIF
						KILL_FACE_TO_FACE_CONVERSATION()
						
						IF NOT IS_PED_INJURED(pedWade)
							SET_PED_CONFIG_FLAG(pedWade,PCF_DisablePedAvoidance,TRUE)
						ENDIF
					
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_AllowPlayerToInterruptVehicleEntryExit,TRUE)
						
						IF IS_VEHICLE_DRIVEABLE(vehMission)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehMission, FALSE)
						ENDIF
						
						IF NOT IS_PED_INJURED(pedWade)
							IF NOT IS_PED_INJURED(pedFloyd)
								REMOVE_PED_FROM_GROUP(pedFloyd)
								SET_PED_CAN_RAGDOLL(pedFloyd,FALSE)
								OPEN_SEQUENCE_TASK(seqMain)
									TASK_LEAVE_ANY_VEHICLE(NULL,1000)
									TASK_LOOK_AT_ENTITY(NULL,pedWade,3000)
									IF IS_VEHICLE_DRIVEABLE(vehMission)
										IF NOT IS_VEHICLE_ROUGHLY_FACING_THIS_DIRECTION(vehMission,238.3604,60)
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-62.05, -2524.82, 6.01>>,1.0,DEFAULT_TIME_BEFORE_WARP, 0.1, ENAV_DEFAULT | ENAV_NO_STOPPING)
										ENDIF
									ENDIF
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,v_ss_floyd[SS_ARRIVE_AT_DOCKS],1.0,DEFAULT_TIME_BEFORE_WARP, 0.1, ENAV_STOP_EXACTLY,f_ss_floyd[SS_ARRIVE_AT_DOCKS])
									TASK_PLAY_ANIM(NULL,"missheistdockssetup1ig_2_p1@start_idle", "supervisor_exitdoor_startidle_floyd",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_USE_KINEMATIC_PHYSICS| AF_LOOPING)
								CLOSE_SEQUENCE_TASK(seqMain)
								TASK_PERFORM_SEQUENCE(pedFloyd,seqMain)	
								CLEAR_SEQUENCE_TASK(seqMain)
							ENDIF
							
							IF NOT IS_PED_INJURED(pedWade)
								REMOVE_PED_FROM_GROUP(pedWade)
								SET_PED_CAN_RAGDOLL(pedWade,FALSE)
								OPEN_SEQUENCE_TASK(seqMain)
									TASK_LEAVE_ANY_VEHICLE(NULL)
									TASK_LOOK_AT_ENTITY(NULL,pedFloyd,3000)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-58.30, -2525.55, 6.16>>,1.0,DEFAULT_TIME_BEFORE_WARP, 0.1, ENAV_DEFAULT | ENAV_NO_STOPPING )
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,v_ss_wade[SS_ARRIVE_AT_DOCKS],1.0,DEFAULT_TIME_BEFORE_WARP, 0.1, ENAV_STOP_EXACTLY,f_ss_wade[SS_ARRIVE_AT_DOCKS])
									TASK_PLAY_ANIM(NULL,"missheistdockssetup1ig_2_p1@start_idle", "supervisor_exitdoor_startidle_wade",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_USE_KINEMATIC_PHYSICS| AF_LOOPING)
								CLOSE_SEQUENCE_TASK(seqMain)
								TASK_PERFORM_SEQUENCE(pedWade,seqMain)	
								CLEAR_SEQUENCE_TASK(seqMain)
							ENDIF
						ENDIF
						
						SAFE_ADD_BLIP_PED(dest_blip,pedFloyd,TRUE)
						
						IF NOT IS_PED_INJURED(pedFloyd)
							TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),pedFloyd,-1)
						ENDIF
						
						iTimer = GET_GAME_TIMER()
						iProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2222054
			
			IF IS_VEHICLE_DRIVEABLE(vehMission)			
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehMission)
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_02a")
				IF CREATE_CONVERSATION(sSpeech,"D1AUD", "DS1_02a", CONV_PRIORITY_MEDIUM)
					REPLAY_RECORD_BACK_FOR_TIME(2.0, 7.0, REPLAY_IMPORTANCE_HIGHEST)
					SET_LABEL_AS_TRIGGERED("DS1_02a",TRUE)
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehMission)
				IF NOT IS_PED_INJURED(pedFloyd)
				AND NOT IS_PED_INJURED(pedWade)
					IF NOT IS_PED_IN_VEHICLE(pedFloyd,vehMission)
					AND NOT IS_PED_IN_VEHICLE(pedWade,vehMission)
						IF NOT IS_PED_INJURED(pedWade)
							SET_RAGDOLL_BLOCKING_FLAGS(pedWade,RBF_PLAYER_IMPACT)
						ENDIF
						IF NOT IS_PED_INJURED(pedFloyd)
							SET_RAGDOLL_BLOCKING_FLAGS(pedFloyd,RBF_PLAYER_IMPACT)
						ENDIF
						bGodText = FALSE
						bDialogue = FALSE
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_AllowPlayerToInterruptVehicleEntryExit,FALSE)
						iProgress++
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 4
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2222054
			
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)

			IF MANAGE_MY_TIMER(iTimer,4000)
				PRINTSTRING("SLOWING PEDS DOWN")PRINTNL()
				IF NOT IS_PED_INJURED(pedWade)
					SET_PED_MAX_MOVE_BLEND_RATIO(pedWade,1.0)
				ENDIF
				IF NOT IS_PED_INJURED(pedFloyd)
					SET_PED_MAX_MOVE_BLEND_RATIO(pedFloyd,1.0)
				ENDIF
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			ENDIF
			
			IF s_sp_supervisor1[0].i_event > 3
				IF IS_VEHICLE_DRIVEABLE(vehMission)			
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehMission)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehMission, FALSE)
						SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
						TASK_LEAVE_VEHICLE(PLAYER_PED_ID(),vehMission)
					ENDIF
				ENDIF
				
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				
				IF NOT IS_PED_INJURED(pedFloyd)
					TASK_LOOK_AT_ENTITY(pedFloyd,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV,SLF_LOOKAT_MEDIUM)
				ENDIF
				
				IF NOT IS_PED_INJURED(pedWade)
					SET_PED_CONFIG_FLAG(pedWade,PCF_DisablePedAvoidance,FALSE)
				ENDIF
				
				REMOVE_ANIM_DICT("missheistdockssetup1ig_2_p1@start_idle")
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(P_CUT_DOOR_02,<<-63.19, -2519.27, 7.79>>,TRUE,0,0,0)
				bSetPiece[Ss_dock_workers_talking] = TRUE
				bSetPiece[SS_blackwater_with_dockworker] = TRUE
				bSetPiece[SS_WELDER] = TRUE
				bDialogue = FALSE
				bClearText = FALSE
				bGodText = FALSE
				iProgress++
			ENDIF
			
		BREAK
		
		CASE 5
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2222054
			
			IF IS_VEHICLE_DRIVEABLE(vehMission)			
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehMission)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehMission, FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					TASK_LEAVE_VEHICLE(PLAYER_PED_ID(),vehMission)
				ELSE
					IF PRE_STREAM_MISSION_STAGE()
						RESET_MISSION_STAGE_VARIABLES()
						eMissionStage = MISSION_STAGE_4
					ENDIF
				ENDIF
			ENDIF
				
		BREAK

	ENDSWITCH		
	
ENDPROC

PROC MANAGE_RUBBER_BANDING(PED_INDEX ped, FLOAT fIdealDist, FLOAT fMinSpeed, FLOAT FMaxSpeed, FLOAT fCatchUp, STRING sWaypoint)
	
	taskStatus = GET_SCRIPT_TASK_STATUS(ped, SCRIPT_TASK_FOLLOW_WAYPOINT_ROUTE)
	

	IF NOT IS_PED_INJURED(ped)
		IF taskStatus = PERFORMING_TASK
			WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(sWaypoint,GET_ENTITY_COORDS(ped),iWayTarget)
			WAYPOINT_RECORDING_GET_CLOSEST_WAYPOINT(sWaypoint,GET_ENTITY_COORDS(PLAYER_PED_ID()),iWayPlayer)
			
			WAYPOINT_RECORDING_GET_COORD(sWaypoint,iWayTarget,vDistTarget)
			WAYPOINT_RECORDING_GET_COORD(sWaypoint,iWayPlayer,vDistPlayer)
			
			fTargetDist = GET_DISTANCE_BETWEEN_COORDS(vDistTarget,GET_ENTITY_COORDS(ped))
			fPlayerDist = GET_DISTANCE_BETWEEN_COORDS(vDistTarget,GET_ENTITY_COORDS(PLAYER_PED_ID()))
			
			IF iWayPlayer > iWayTarget
			OR fPlayerDist < fTargetDist
				IF fThisSpeed < fCatchUp
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(ped)
						fThisSpeed = fThisSpeed + 0.1
						WAYPOINT_PLAYBACK_OVERRIDE_SPEED(ped,fThisSpeed)
					ENDIF
				ELSE
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(ped,fThisSpeed)
				ENDIF
			ELSE
				FLOAT fDist =  GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), ped)
				IF fDist < fIdealDist
		            fSpeed = fMaxSpeed
		      	ELSE
		            fSpeed = fMinSpeed
		      	ENDIF
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(ped)
					fThisSpeed = fSpeed
					WAYPOINT_PLAYBACK_OVERRIDE_SPEED(ped,fSpeed)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


// remove mission text
PROC REMOVE_MISSION_TEXT(BOOL bClearSpeech = TRUE, BOOL bClearGodText = TRUE, BOOL bClearHelpText = TRUE)
	IF bClearSpeech
		KILL_ANY_CONVERSATION()
	ENDIF
	
	IF bClearGodText
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		OR bClearSpeech
			CLEAR_PRINTS()
		ENDIF
		CLEAR_REMINDER_MESSAGE()
	ENDIF
	
	IF bClearHelpText
		IF IS_HELP_MESSAGE_BEING_DISPLAYED()
			CLEAR_HELP()
		ENDIF
	ENDIF
ENDPROC

// do mission speech
FUNC BOOL DO_MISSION_SPEECH(STRING sStringSpeech, BOOL bSupercedeText = FALSE, INT iLine = 0, BOOL bFromSpecifiedLine = FALSE)
	IF bSupercedeText OR NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			IF iLine = 0
				RETURN CREATE_CONVERSATION(sSpeech,"D1AUD", sStringSpeech, CONV_PRIORITY_HIGH)
			ELSE
				TEXT_LABEL tLine 
				tLine = sStringSpeech
				tLine+="_"
				tLine+=iLine
				IF NOT bFromSpecifiedLine
					RETURN PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech,"D1AUD", sStringSpeech, tLine, CONV_PRIORITY_HIGH)	
				ELSE
					RETURN CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech,"D1AUD", sStringSpeech, tLine, CONV_PRIORITY_HIGH)
				ENDIF
			ENDIF	
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

// restore banter
FUNC BOOL RESTORE_CONVO()
	IF bSavedConvo 	
		//IF DO_MISSION_SPEECH(tSavedBanterRoot, FALSE, 23, TRUE)
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			PRINTLN("@@@@@@@@@@@@@@@@@ RESTORE_CONVO() ROOT: ", tSavedBanterRoot, " LINE: ", tResumeBanterLabel, " @@@@@@@@@@@@@@@@")
			CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech,"D1AUD", tSavedBanterRoot, tResumeBanterLabel, CONV_PRIORITY_HIGH)
			RETURN TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF	
	
	RETURN FALSE
ENDFUNC

PROC STORE_CONVO()
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		TEXT_LABEL_23 tBanter = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
		bSavedConvo = TRUE
		tSavedBanterRoot = tBanter
		tResumeBanterLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
		KILL_FACE_TO_FACE_CONVERSATION()
		PRINTLN("@@@@@@@@@@@@@@@@@ STORE_CONVO() ROOT: ", tSavedBanterRoot, " LINE: ", tResumeBanterLabel, " @@@@@@@@@@@@@@@@")
	ENDIF
ENDPROC


// lamar follow pavement
PROC DO_FLOYD_WALK_AND_TALK()
	IF NOT IS_PED_INJURED(pedFloyd)
		TASK_FOLLOW_WAYPOINT_RECORDING(pedFloyd, "floyddocks1", 0, EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS | EWAYPOINT_START_FROM_CLOSEST_POINT)
	ENDIF
ENDPROC

PROC DO_WALK_AND_TALK_DIALOGUE()
	
	IF NOT IS_PED_INJURED(pedFloyd)
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("FLOYD SEEN BOAT")
			IF IS_ENTITY_IN_ANGLED_AREA(pedFloyd, <<-101.658623,-2472.183105,4.021717>>, <<-98.129723,-2474.698730,8.271718>>, 2.250000)
				SET_LABEL_AS_TRIGGERED("FLOYD SEEN BOAT",TRUE)
			ENDIF
		ENDIF
		
		SWITCH iWalkAndTalk
		
			CASE 0
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF SETUP_PEDS_FOR_DIALOGUE()
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sSpeech,"D1AUD","DS1_04", CONV_PRIORITY_HIGH)
								REPLAY_RECORD_BACK_FOR_TIME(2.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
								iWalkAndTalk ++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech,"D1AUD","DS1_05a", CONV_PRIORITY_HIGH)
						iWalkAndTalk ++
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				//PURPOSE: Wander around the docks - Land and air is fucked
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_05b")
						IF CREATE_CONVERSATION(sSpeech,"D1AUD","DS1_05b", CONV_PRIORITY_HIGH)
							SET_LABEL_AS_TRIGGERED("DS1_05b",TRUE)
						ENDIF
					ELSE
						iWalkAndTalk = 4
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				//SKIP FOR NOW
				//PURPOSE: Wander around the docks - Whoa. Gotta keep your head on the swivel round here
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech,"D1AUD","DS1_05c", CONV_PRIORITY_HIGH)
						iWalkAndTalk ++
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
				//PURPOSE: Wander around the docks - That's the freighter I told you about.
				IF HAS_LABEL_BEEN_TRIGGERED("FLOYD SEEN BOAT")
					IF REQUEST_AMBIENT_AUDIO_BANK("CREAK_V1")
						IF NOT IS_AUDIO_SCENE_ACTIVE("DH_1_FOLLOW_FLOYD_SEE_FREIGHTER")
							START_AUDIO_SCENE("DH_1_FOLLOW_FLOYD_SEE_FREIGHTER")
						ENDIF
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech,"D1AUD","DS1_06", CONV_PRIORITY_HIGH)
								PLAY_SOUND_FROM_COORD(-1,"CREAK_01",<<-212.5920, -2375.6167, 16.3326>>,"DOCKS_HEIST_SETUP_SOUNDS")
								iWalkAndTalk ++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			//Merryweather
			CASE 5
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlackWater1Id)
				OR IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlackWater2Id)
				OR IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlackWater3Id)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(sSpeech,"D1AUD","DS1_04b", CONV_PRIORITY_HIGH)
							IF NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[0].ped)
								TASK_LOOK_AT_ENTITY(pedFloyd, s_sp_blackwater_with_dockworker[0].ped,5000,SLF_WHILE_NOT_IN_FOV,SLF_LOOKAT_VERY_HIGH)
							ENDIF
							iWalkAndTalk ++
						ENDIF
					ENDIF
				ENDIF
			BREAK
		
		ENDSWITCH
	ENDIF
ENDPROC

INT iRandHurryUp
CONST_FLOAT FLOYD_PAUSE_DIST 7.0
CONST_FLOAT FLOYD_UNPAUSE_DIST 3.2

PROC MANAGE_WALK_AND_TALK()
	
	IF NOT IS_PED_INJURED(pedFloyd)
	
		SET_PED_CAN_PLAY_GESTURE_ANIMS(PLAYER_PED_ID(),false)
		SET_PED_CAN_PLAY_GESTURE_ANIMS(pedFloyd,false)
	
		IF GET_ENTITY_SPEED(PLAYER_PED_ID()) < 1.2
		OR IS_PED_RUNNING(PLAYER_PED_ID())
			CLEAR_PED_SECONDARY_TASK(PLAYER_PED_ID())
			PRINTLN("CLEAR SECONDARY PLAYER ANIMS")//PRINTNL()
		ENDIF
		
		IF GET_ENTITY_SPEED(pedFloyd) < 0.1
		//OR GET_SCRIPT_TASK_STATUS(pedFloyd, SCRIPT_TASK_FOLLOW_WAYPOINT_ROUTE) = FINISHED_TASK
			PRINTLN("CLEAR SECONDARY FLOYD ANIMS")//PRINTNL()
			CLEAR_PED_SECONDARY_TASK(pedFloyd)
		ENDIF
		
		// force walk
		IF GET_GAME_TIMER() <= iStageStartTime + 1500
			//SET_PED_MIN_MOVE_BLEND_RATIO(pedFloyd,PEDMOVE_WALK)
		ENDIF
		
		IF GET_GAME_TIMER() <= iStageStartTime + 1000
			//SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID() ,PEDMOVE_WALK)
		ENDIF
		
		IF bIncreaseFloydSpeed
			fFloydWalkSpeed = fFloydWalkSpeed +@ 0.014
			IF fFloydWalkSpeed > 1.08
				fFloydWalkSpeed = 1.08
			ENDIF
		ENDIF
		
		IF NOT bFloydWalkPaused
			IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedFloyd, <<FLOYD_PAUSE_DIST, FLOYD_PAUSE_DIST, 4>>)
				REQUEST_ANIM_DICT("missheistdockssetup1ig_8")
				REQUEST_ANIM_DICT("MissHeistDocksSetup1")
				IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_8")
				AND HAS_ANIM_DICT_LOADED("MissHeistDocksSetup1")
					CLEAR_SEQUENCE_TASK(seqMain)
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1,SLF_WHILE_NOT_IN_FOV)
						TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
						iRandHurryUp = GET_RANDOM_INT_IN_RANGE(0,5)
						IF iRandHurryUp = 0
							TASK_PLAY_ANIM(NULL,"MissHeistDocksSetup1","Beckon")
						ELIF iRandHurryUp = 1 
							TASK_PLAY_ANIM(NULL,"missheistdockssetup1ig_8","are_you_coming")
						ELIF iRandHurryUp = 2
							TASK_PLAY_ANIM(NULL,"missheistdockssetup1ig_8","Are_You_Gonna_Hurt")
						ELIF iRandHurryUp = 3
							TASK_PLAY_ANIM(NULL,"missheistdockssetup1ig_8","its_over_here_if")
						ELIF iRandHurryUp = 4
							TASK_PLAY_ANIM(NULL,"missheistdockssetup1ig_8","this_dont_make_me")
						ENDIF
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(pedFloyd, seqMain)
					
					CLEAR_PED_SECONDARY_TASK(pedFloyd)
					CLEAR_PED_SECONDARY_TASK(PLAYER_PED_ID())
					
					TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					bKilledOnFootConversation = FALSE
					bDoneHurrySpeech = FALSE
					bSetAltAnim = FALSE
					bFloydWalkPaused = TRUE
				ENDIF
			ELSE
				DO_WALK_AND_TALK_DIALOGUE()
				 
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedFloyd, <<FLOYD_PAUSE_DIST, FLOYD_UNPAUSE_DIST, 4>>)
					bIncreaseFloydSpeed = TRUE
				ENDIF

				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedFloyd) < 1
					PRINTLN("CONSTRAINING MOVESPEED")//PRINTNL()
					SET_PED_MOVE_RATE_OVERRIDE(PLAYER_PED_ID(), 0.91)
				ELSE
					SET_PED_MOVE_RATE_OVERRIDE(PLAYER_PED_ID(), 0.94)
				ENDIF
			
				IF bSavedConvo
					IF RESTORE_CONVO()
						bSavedConvo = FALSE
						bSetAltAnim = TRUE
					ENDIF
				ENDIF
				
				IF bSetAltAnim = TRUE
					// conversation anims
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						TEXT_LABEL_23 tCurrentLabel = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
						//INT iLineNumber = GET_CURRENT_SCRIPTED_CONVERSATION_LINE()
						
						PRINTSTRING("CURRENT LABEL: ")PRINTSTRING(tCurrentLabel)PRINTNL()
						
						IF ARE_STRINGS_EQUAL(tCurrentLabel,"DS1_05a_1")
							iLineMatch = 0
						ELIF ARE_STRINGS_EQUAL(tCurrentLabel, "DS1_05a_3")
							iLineMatch = 1
						ELIF ARE_STRINGS_EQUAL(tCurrentLabel, "DS1_05b_1")
							iLineMatch = 2
						ELIF ARE_STRINGS_EQUAL(tCurrentLabel, "DS1_05b_13")
							iLineMatch = 3
						ELIF ARE_STRINGS_EQUAL(tCurrentLabel, "DS1_05b_11")
							iLineMatch = 4
						ELIF ARE_STRINGS_EQUAL(tCurrentLabel, "DS1_06_1")
							iLineMatch = 5
						ELIF ARE_STRINGS_EQUAL(tCurrentLabel, "DS1_06_8")
							iLineMatch = 6
						ELIF ARE_STRINGS_EQUAL(tCurrentLabel, "DS1_06_12")
							iLineMatch = 7
						ELIF ARE_STRINGS_EQUAL(tCurrentLabel, "DS1_06_15")
							iLineMatch = 8
						ENDIF
						
						IF iLineMatch >= 0
							IF NOT bDoneConversationAnim[iLineMatch]
								STRING sTrevorLine
								STRING sFloydLine
								STRING sAnimDictionary
								SWITCH iLineMatch
									CASE 0
										sTrevorLine = "you_ever_get_helicopters_trevor"
										sFloydLine = "you_ever_get_helicopters_floyd"
										sAnimDictionary = "missheistdockssetup1ig_2_p2"
									BREAK
									CASE 1
										sTrevorLine = "now_this_is_restricted_trevor"
										sFloydLine = "now_this_is_restricted_floyd"
										sAnimDictionary = "missheistdockssetup1ig_2_p2"
									BREAK
									CASE 2
										sTrevorLine = "how_is_a_man_trevor"
										sFloydLine = "how_is_a_man_floyd"
										sAnimDictionary = "missheistdockssetup1ig_2_p3"
									BREAK												
									CASE 3
										sTrevorLine = "i_just_said_they_trevor"
										sFloydLine = "i_just_said_they_floyd"
										sAnimDictionary = "missheistdockssetup1ig_2_p3"
									BREAK														
									CASE 4
										sTrevorLine = "maybe_ill_have_to_trevor"
										sFloydLine = "maybe_ill_have_to_floyd"
										sAnimDictionary = "missheistdockssetup1ig_2_p3"
									BREAK	
									CASE 5
										sTrevorLine = "thats_the_freighter_i_trevor"
										sFloydLine = "thats_the_freighter_i_floyd"
										sAnimDictionary = "missheistdockssetup1ig_7"
									BREAK
									CASE 6
										sTrevorLine = "but_these_are_marked_trevor"
										sFloydLine = "but_these_are_marked_floyd"
										sAnimDictionary = "missheistdockssetup1ig_7"
									BREAK
									CASE 7
										sTrevorLine = "the_government_stuff_is_floyd"
										sFloydLine = "the_government_stuff_is_trevor"
										sAnimDictionary = "missheistdockssetup1ig_7"
									BREAK
									CASE 8
										sTrevorLine = "you_are_beginning_to_trevor"
										sFloydLine = "you_are_beginning_to_floyd"
										sAnimDictionary = "missheistdockssetup1ig_7"
									BREAK
								ENDSWITCH
								IF bDoPlayerAnims 
									TASK_PLAY_ANIM(PLAYER_PED_ID(), sAnimDictionary, sTrevorLine, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY|AF_SECONDARY|AF_TAG_SYNC_IN|AF_TAG_SYNC_OUT|AF_TAG_SYNC_CONTINUOUS)
									TASK_PLAY_ANIM(pedfloyd, sAnimDictionary, sFloydLine, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_UPPERBODY|AF_SECONDARY|AF_TAG_SYNC_IN|AF_TAG_SYNC_OUT|AF_TAG_SYNC_CONTINUOUS)
								ENDIF
								bDoneConversationAnim[iLineMatch] = TRUE
								REMOVE_ANIM_DICT(sAnimDictionary)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT bKilledOnFootConversation
				STORE_CONVO()
				bKilledOnFootConversation = TRUE		
			ENDIF
				
			IF NOT bDoneHurrySpeech
				IF iProgress > 5
					IF GET_GAME_TIMER() >= iNextFloydHurrySpeechTime
						IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						ELSE
							IF NOT IS_ANY_SPEECH_PLAYING(pedFloyd)
							OR NOT IS_ANY_SPEECH_PLAYING(PLAYER_PED_ID())
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedFloyd,"DH1_CAAA","FLOYD",SPEECH_PARAMS_FORCE_SHOUTED)
								PRINTSTRING("PLAY FLOYD HURRRY UP SPEECH")PRINTNL()
								bDoneHurrySpeech = TRUE
								iNextFloydHurrySpeechTime = GET_GAME_TIMER() + 10000
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF (IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedFloyd, <<FLOYD_UNPAUSE_DIST, FLOYD_UNPAUSE_DIST, 4>>))
				TASK_LOOK_AT_ENTITY(pedFloyd, PLAYER_PED_ID(), -1,SLF_WHILE_NOT_IN_FOV)
				TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(), pedFloyd, -1,SLF_DEFAULT)
				IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
					CLEAR_PRINTS()
				ELSE
					DO_FLOYD_WALK_AND_TALK()
					bKilledOnFootConversation = FALSE
					bSetAltAnim = TRUE
					bFloydWalkPaused = FALSE
				ENDIF
			ENDIF							
		ENDIF
	ENDIF
ENDPROC

VECTOR vHandlerRot

PROC MANAGE_FLOYD_ATTACHMENT()
	
	IF IS_VEHICLE_DRIVEABLE(vehHandler)
		IF NOT IS_VEHICLE_ON_ALL_WHEELS(vehHandler)
	        vHandlerRot = GET_ENTITY_ROTATION(vehHandler)
	        IF vHandlerRot.x > 80
			OR  vHandlerRot.x < -80
				IF IS_ENTITY_ATTACHED_TO_ENTITY(pedFloyd,vehHandler)
					DETACH_ENTITY(pedFloyd)
					SET_LABEL_AS_TRIGGERED("HANDLER NOT SAFE",TRUE)
				ENDIF
			ENDIF
		ELSE
			SET_LABEL_AS_TRIGGERED("HANDLER NOT SAFE",FALSE)
		ENDIF
	ENDIF

ENDPROC

INT iFloydTimer


//PURPOSE: Trevor follows floyd around the docks to the Forklift area
PROC MISSION_STAGE_4_PROC()

		IF NOT DOES_ENTITY_EXIST(vehHandler)
			SPAWN_VEHICLE(vehHandler,HANDLER,<<-129.77, -2418.28, 6.24>>,183.0850,-1,1.0,FALSE)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehHandler,FALSE)
			SET_VEHICLE_COLOUR_COMBINATION(vehHandler,0)
			SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehHandler,FALSE)
			FREEZE_ENTITY_POSITION(vehHandler,TRUE)
			SET_VEHICLE_PROVIDES_COVER(vehHandler,FALSE)
		ENDIF
	
		IF bHitMarkToTriggerAudioSceneChange = FALSE
			IF NOT IS_PED_INJURED(pedFloyd)
				IF IS_ENTITY_IN_ANGLED_AREA(pedFloyd, <<-89.282677,-2481.734375,1.273084>>, <<-91.874001,-2485.461670,10.770866>>, 2.250000)
					PRINTSTRING("DH_1_FOLLOW_FLOYD_SEE_FREIGHTER")PRINTNL()
					STOP_AUDIO_SCENE("DH_1_FOLLOW_FLOYD_START")
					START_AUDIO_SCENE("DH_1_FOLLOW_FLOYD_SEE_FREIGHTER")
					bHitMarkToTriggerAudioSceneChange = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF iProgress > 6
		AND iProgress < 12
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-127.401070,-2416.403564,4.500676>>, <<-127.351868,-2418.019287,7.250676>>, 1.750000)
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1.0)
			ELSE
				IF iProgress < 10
					SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.0)
				ENDIF
			ENDIF
		ENDIF
		
		IF iProgress > 0
		AND iProgress < 7
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		ENDIF
		
		//VECTOR vTemp
		CREATE_THE_DOCKS_STUFF()
		MANAGE_FLOYD_ATTACHMENT()
		
		IF DOES_ENTITY_EXIST(pedWade)
			IF NOT IS_PED_INJURED(pedWade)
				IF NOT IS_ENTITY_ON_SCREEN(pedWade)
					DELETE_PED(pedWade)
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_WADE)
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bLockForklift
			IF DOES_ENTITY_EXIST(vehHandler)
				IF IS_VEHICLE_DRIVEABLE(vehHandler)
				AND IS_VEHICLE_DRIVEABLE(vehDocks[1])
					SET_VEHICLE_DOORS_LOCKED(vehHandler,VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehHandler,FALSE)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehDocks[1],FALSE)
					bLockForklift = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(pedFloyd)
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedFloyd)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
				SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
			ENDIF
		ENDIF
		
		IF iProgress > 4
		AND iProgress < 7
			MANAGE_WALK_AND_TALK()
		ENDIF
		
		IF iProgress > 2
			IF bClearText = FALSE
				IF IS_THIS_PRINT_BEING_DISPLAYED("AW_FLOW_FLO")
					IF MANAGE_MY_TIMER(iDialogueTimer,5000)
						CLEAR_PRINTS()
						bClearText = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF iProgress < 7
			REQUEST_CLIP_SET("ANIM_GROUP_GESTURE_MISS_DocksSetup1")
			IF HAS_ANIM_SET_LOADED("ANIM_GROUP_GESTURE_MISS_DocksSetup1")
				SET_PED_GESTURE_GROUP(PLAYER_PED_ID(), "ANIM_GROUP_GESTURE_MISS_DocksSetup1")
				IF NOT IS_PED_INJURED(pedFloyd)
		 			SET_PED_GESTURE_GROUP(pedFloyd, "ANIM_GROUP_GESTURE_MISS_DocksSetup1")
				ENDIF
			ENDIF
		ENDIF

	
		SWITCH iProgress
		
		CASE 0//Create objective specific items- print objective, add blips etc
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2222054
			
			IF NOT IS_PED_INJURED(pedFloyd)
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),pedFloyd,<<15,15,15>>)
					SET_PED_MOVE_RATE_OVERRIDE(PLAYER_PED_ID(),0.8)
				ENDIF
			ENDIF
			IF SETUP_PEDS_FOR_DIALOGUE()
				
				IF IS_VEHICLE_DRIVEABLE(vehMission)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehMission, FALSE)
				ENDIF

				SAFE_ADD_BLIP_PED(dest_blip,pedFloyd,TRUE)

				REQUEST_WAYPOINT_RECORDING("floyddocks1")
				REQUEST_VEHICLE_RECORDING(1,"AWDocks3")
				REQUEST_VEHICLE_RECORDING(1,"AWDocks6")
				
				bSetPiece[Ss_dock_workers_talking] = TRUE
				bSetPiece[SS_blackwater_with_dockworker] = TRUE
				bSetPiece[SS_GANTRY_GUYS] = TRUE
				bSetPiece[SS_WELDER] = TRUE
				
				bDoCatchUp = FALSE
				bGodText = FALSE
				bDialogue = FALSE
				bLockForklift = FALSE
				REQUEST_ANIM_DICT("missheistdockssetup1ig_2_p1b")
				
				REQUEST_WAYPOINT_RECORDING("floyddocks1")
				REQUEST_IPL("pcranecont")
				iMissionDialogue = 3
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3,"Follow Floyd")
				iWalkAndTalk = 0
				DISABLE_CELLPHONE(TRUE)
				iStageStartTime = GET_GAME_TIMER()
				bTriggerSetPiece[6] = FALSE
				bTriggerSetPiece[5] = FALSE
				bIsJumpingDirectlyToStage = FALSE
				bHitMarkToTriggerAudioSceneChange = FALSE
				SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
				SET_PED_PATH_CAN_USE_LADDERS(PLAYER_PED_ID(),FALSE)

				IF IS_SCREEN_FADED_OUT()
					MANAGE_PLAYER_OUTFIT()
				ENDIF
				iHandlerDialogueSwitch = 0
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("DH_1_FOLLOW_FLOYD_START")
					START_AUDIO_SCENE("DH_1_FOLLOW_FLOYD_START")
				ENDIF
				
				iProgress++
			ENDIF
		
		BREAK
		
		CASE 1
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2222054
			
			IF NOT IS_PED_INJURED(pedFloyd)
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),pedFloyd,<<15,15,15>>)
					SET_PED_MOVE_RATE_OVERRIDE(PLAYER_PED_ID(),0.8)
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(pedFloyd)
				IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),pedFloyd,<<6,6,6>>)
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDocks6")
						IF bTooFar = FALSE
							IF NOT IS_THIS_PRINT_BEING_DISPLAYED("AW_FLOW_FLO")
								KILL_ANY_CONVERSATION()
								PRINT_NOW("AW_FLOW_FLO",DEFAULT_GOD_TEXT_TIME,1)
							ENDIF
							bTooFar = TRUE
						ENDIF
						REMOVE_PED_FROM_GROUP(pedFloyd)
						iDialogueTimer = GET_GAME_TIMER()
						REQUEST_ANIM_DICT("missheistdockssetup1ig_4@main_action")
						INIT_STAGE()
						iProgress++
					ENDIF
				ELSE
					IF bTooFar = FALSE
						iDialogueTimer = GET_GAME_TIMER()
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED("AW_FLOW_FLO")
							KILL_ANY_CONVERSATION()
							PRINT_NOW("AW_FLOW_FLO",DEFAULT_GOD_TEXT_TIME,1)
						ENDIF
						
						bTooFar = TRUE
					ENDIF
//					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_36")
//						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
//								IF CREATE_CONVERSATION(sSpeech,"D1AUD", "DS1_36", CONV_PRIORITY_MEDIUM) // {trevor wanders off}
//									iDialogueTimer = GET_GAME_TIMER()
//									SET_LABEL_AS_TRIGGERED("DS1_36", TRUE)
//								ENDIF
//							ENDIF
//						ENDIF
//					ELSE
//						IF MANAGE_MY_TIMER(iDialogueTimer,GET_RANDOM_INT_IN_RANGE(10000,15000))
//							SET_LABEL_AS_TRIGGERED("DS1_36", FALSE)
//						ENDIF
//					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2222054
			
			IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_4@main_action")
				IF GET_IS_WAYPOINT_RECORDING_LOADED("floyddocks1")
					CLEAR_PED_TASKS(pedFloyd)
					OPEN_SEQUENCE_TASK(seqMain)
						TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "floyddocks1", 0,EWAYPOINT_START_FROM_CLOSEST_POINT)//|EWAYPOINT_DO_NOT_RESPOND_TO_COLLISION_EVENTS)
					CLOSE_SEQUENCE_TASK(seqMain)
					TASK_PERFORM_SEQUENCE(pedFloyd,seqMain)	
					CLEAR_SEQUENCE_TASK(seqMain)
					
					bSetAltAnim = TRUE
					
					IF NOT IS_PED_INJURED(pedFloyd)
						TASK_CLEAR_LOOK_AT(PLAYER_PED_ID())
					ENDIF
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedFloyd,FALSE)
					iProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedFloyd)
				iProgress++
			ENDIF
		BREAK
		
		
		CASE 4
			IF SETUP_MISSION_REQUIREMENT(REQ_CRANE_CONTAINERS)
				iProgress++
			ENDIF
		BREAK
		
		CASE 5
			
			IF GET_IS_WAYPOINT_RECORDING_LOADED("floyddocks1") 
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"AWDocks3")
				IF NOT IS_PED_INJURED(pedFloyd)
					IF GET_IS_WAYPOINT_RECORDING_LOADED("floyddocks1")
						REMOVE_PED_FROM_GROUP(pedFloyd)
						SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedFloyd,FALSE)
						bSetPiece[SS_FORKLIFT_SUPERVISOR_A] = TRUE
						iProgress++
					ENDIF
				ENDIF
			ELSE
				REQUEST_WAYPOINT_RECORDING("floyddocks1")
				REQUEST_VEHICLE_RECORDING(1,"AWDocks3")
			ENDIF
			
		BREAK
	
		CASE 6
			IF bSetPiece[SS_blackwater_with_dockworker] = FALSE
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< -102.1558, -2475.9209, 5.0203 >>,<<3,3,3>>)
					REQUEST_ANIM_DICT("missheistdockssetup1ig_9@main_action")
					bSetPiece[SS_blackwater_with_dockworker] = TRUE
				ENDIF
			ENDIF

			IF NOT IS_PED_INJURED(pedFloyd)
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-121.87, -2425.34, 6.00>>,<<5,5,5>>)
				AND IS_ENTITY_AT_COORD(pedFloyd, <<-121.87, -2425.34, 6.00>>,<<5,5,5>>)
					bDoCatchUp = FALSE
					KILL_ANY_CONVERSATION()
					CLEAR_UP_EVENTS_AFTER_WALK_AND_TALK()
					IF NOT IS_PED_INJURED(s_sp_forklift_supervisorA[0].ped)
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-123.5851, -2422.3521, 5.0013>>,1.0,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_SUPPRESS_EXACT_STOP)
							TASK_LOOK_AT_ENTITY(NULL,s_sp_forklift_supervisorA[0].ped,6000,SLF_WHILE_NOT_IN_FOV)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(pedFloyd,seqMain)	
						CLEAR_SEQUENCE_TASK(seqMain)
					ENDIF
					
					CLEAR_PRINTS()
					iProgress ++
				ELSE
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-121.87, -2425.34, 6.00>>,<<10,10,10>>)
					AND IS_ENTITY_AT_COORD(pedFloyd, <<-121.87, -2425.34, 6.00>>,<<5,5,5>>)
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						KILL_ANY_CONVERSATION()
						CLEAR_UP_EVENTS_AFTER_WALK_AND_TALK()
						IF NOT IS_PED_INJURED(s_sp_forklift_supervisorA[0].ped)
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-123.5851, -2422.3521, 5.0013>>,1.0,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_SUPPRESS_EXACT_STOP)
								TASK_LOOK_AT_ENTITY(NULL,s_sp_forklift_supervisorA[0].ped,6000)
								CLOSE_SEQUENCE_TASK(seqMain)
								TASK_PERFORM_SEQUENCE(pedFloyd,seqMain)	
							CLEAR_SEQUENCE_TASK(seqMain)
						ENDIF
						CLEAR_PRINTS()
						
						iProgress ++
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("ANIMATEFLOYD")
							REQUEST_ANIM_DICT("MissHeistDocksSetup1")
							IF NOT HAS_ANIM_DICT_LOADED("MissHeistDocksSetup1")
								IF IS_ENTITY_AT_COORD(pedFloyd, <<-121.87, -2425.34, 6.00>>,<<5,5,5>>)
								AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<-121.87, -2425.34, 6.00>>,<<5,5,5>>)
									CLEAR_SEQUENCE_TASK(seqMain)
									OPEN_SEQUENCE_TASK(seqMain)
										TASK_LOOK_AT_ENTITY(NULL, PLAYER_PED_ID(), -1,SLF_WHILE_NOT_IN_FOV)
										TASK_PLAY_ANIM(NULL,"MissHeistDocksSetup1","Beckon")
										TASK_TURN_PED_TO_FACE_ENTITY(NULL, PLAYER_PED_ID(), -1)
										TASK_PLAY_ANIM(NULL,"amb@dockworker@stand@idle_a", "idle_a",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
									CLOSE_SEQUENCE_TASK(seqMain)
									TASK_PERFORM_SEQUENCE(pedFloyd, seqMain)
									SET_LABEL_AS_TRIGGERED("ANIMATEFLOYD",TRUE)
								ENDIF
							ENDIF
						ELSE
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-121.87, -2425.34, 6.00>>,<<5,5,5>>)
								CLEAR_PED_TASKS(pedFloyd)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		
		
		CASE 7
			REQUEST_ANIM_DICT("missheistdockssetup1ig_9@main_action")
			REQUEST_ANIM_DICT("missheistdockssetup1ig_9@start_idle")
			IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_9@main_action")
			AND HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_9@start_idle")
				IF NOT IS_PED_INJURED(pedFloyd)
					IF NOT IS_PED_INJURED(s_sp_forklift_supervisorA[0].ped)
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_LOOK_AT_ENTITY(NULL,pedFloyd,6000)
							TASK_PLAY_ANIM(NULL, "missheistdockssetup1ig_9@main_action", "forklift_supervise_mainaction_supervisor",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1)
							TASK_PLAY_ANIM(NULL, "missheistdockssetup1ig_9@start_idle", "forklift_supervise_idlea_supervisor",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING | AF_USE_KINEMATIC_PHYSICS,0,FALSE,AIK_NONE)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(s_sp_forklift_supervisorA[0].ped,seqMain)	
						CLEAR_SEQUENCE_TASK(seqMain)
						
						SET_RAGDOLL_BLOCKING_FLAGS(s_sp_forklift_supervisorA[0].ped,RBF_PLAYER_IMPACT)
						REMOVE_ANIM_DICT("missheistdockssetup1ig_9@main_action")
						REMOVE_ANIM_DICT("missheistdockssetup1ig_9@start_idle")
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehDocks[1])
					IF IS_VEHICLE_DRIVEABLE(vehDocks[1])
						IF NOT IS_ENTITY_ON_SCREEN(vehDocks[1])
							DELETE_VEHICLE(vehDocks[1])
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedDockWorker[12])
					IF NOT IS_PED_INJURED(pedDockWorker[12])
						IF NOT IS_ENTITY_ON_SCREEN(pedDockWorker[12])
							DELETE_PED(pedDockWorker[12])
						ENDIF
					ENDIF
				ENDIF

				IF IS_VEHICLE_DRIVEABLE(vehHandler)
					SET_VEHICLE_DOORS_LOCKED(vehHandler,VEHICLELOCK_UNLOCKED)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehHandler,FALSE)
				ENDIF
				
				IF DOES_BLIP_EXIST(dest_blip)
					REMOVE_BLIP(dest_blip)
				ENDIF
				
				REMOVE_ANIM_SET("ANIM_GROUP_GESTURE_MISS_DocksSetup1")
//				DISABLE_CELLPHONE(FALSE)
				SET_PED_PATH_CAN_USE_LADDERS(PLAYER_PED_ID(),TRUE)
				iProgress ++
			ENDIF
		BREAK
		
		CASE 8
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_05e")
				IF SETUP_PEDS_FOR_DIALOGUE()
					IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_05e", CONV_PRIORITY_MEDIUM)		
						REPLAY_RECORD_BACK_FOR_TIME(2.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
						SET_LABEL_AS_TRIGGERED("DS1_05e",TRUE)
					ENDIF
				ENDIF
			ELSE
				iFloydTimer = GET_GAME_TIMER()
				iProgress ++
			ENDIF
		BREAK
		
		CASE 9
			IF MANAGE_MY_TIMER(iFloydTimer,2000)
				IF NOT HAS_LABEL_BEEN_TRIGGERED("TASK FLOYD")
					SET_PED_STEERS_AROUND_VEHICLES(pedFloyd,FALSE)
					IF NOT IS_PED_INJURED(pedFloyd)
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_GO_STRAIGHT_TO_COORD(NULL, << -127.37, -2417.29, 5.54 >>,1.0,DEFAULT_TIME_BEFORE_WARP,88.64)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(pedFloyd,seqMain)	
						CLEAR_SEQUENCE_TASK(seqMain)
						REQUEST_ANIM_DICT("missheistdockssetup1ig_14")
						SET_LABEL_AS_TRIGGERED("TASK FLOYD",TRUE)
					ENDIF
				ELSE
					IF NOT IS_PED_INJURED(pedFloyd)
						IF IS_VEHICLE_DRIVEABLE(vehHandler)
							IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_14")
								IF IS_PED_READY_FOR_ANIM(pedFloyd, << -127.37, -2417.29, 5.94 >>, 97.5,<<0.2,0.2,2>>)
									sceneHandlerA1 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
									TASK_SYNCHRONIZED_SCENE (pedFloyd, sceneHandlerA1, "missheistdockssetup1ig_14", "floyd_climb_up", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT,NORMAL_BLEND_IN )
									SET_ENTITY_NO_COLLISION_ENTITY(pedFloyd,vehHandler,FALSE)
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneHandlerA1,vehHandler,-1)
									SET_PED_STEERS_AROUND_VEHICLES(pedFloyd,FALSE)
									iProgress ++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE 10
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneHandlerA1)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneHandlerA1) > 0.5
					IF IS_VEHICLE_DRIVEABLE(vehHandler)
						SET_VEHICLE_DOORS_LOCKED(vehHandler,VEHICLELOCK_UNLOCKED)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehHandler,TRUE)
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(vehHandler)
						SAFE_ADD_BLIP_VEHICLE(veh_blip,vehHandler,TRUE)
					ENDIF
					iProgress ++
				ENDIF
			ENDIF
		BREAK


		CASE 11
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneHandlerA1)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneHandlerA1) >= 1
					IF IS_VEHICLE_DRIVEABLE(vehHandler)
						SET_VEHICLE_DOORS_LOCKED(vehHandler,VEHICLELOCK_UNLOCKED)
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(vehHandler)
						SAFE_ADD_BLIP_VEHICLE(veh_blip,vehHandler,TRUE)
					ENDIF
					IF PRE_STREAM_MISSION_STAGE()
						SAFE_REMOVE_BLIP(dest_blip)
						RESET_MISSION_STAGE_VARIABLES()
						ADVANCE_MISSION_STAGE()
					ENDIF
				ENDIF
			ENDIF
		BREAK

	ENDSWITCH
ENDPROC

PROC MANAGE_FLOYD_ACTIVITY()
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("HANDLER NOT SAFE")

		SWITCH iFloydHauler
			
			//REQUESTS
			CASE 0
				REQUEST_ANIM_DICT("missheistdockssetup1ig_14")
				IF eMissionStage = MISSION_STAGE_6
					iFloydHauler = 6
				ELSE
					iFloydHauler ++
				ENDIF
			BREAK
			
			//LETS GET GOING
			CASE 1
				IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_14")
					IF NOT IS_PED_INJURED(pedFloyd)
						IF IS_VEHICLE_DRIVEABLE(vehHandler)
							sceneHandlerA1 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
							TASK_SYNCHRONIZED_SCENE (pedFloyd, sceneHandlerA1, "missheistdockssetup1ig_14", "floyd_lets_get_going", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,  SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneHandlerA1,vehHandler,-1)
							SET_ENTITY_NO_COLLISION_ENTITY(pedFloyd,vehHandler,TRUE)
							iFloydHauler ++
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			//GET TO THE CONTAINERS
			CASE 2
				IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_14")
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneHandlerA1)
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneHandlerA1) = 1
							IF NOT IS_PED_INJURED(pedFloyd)
								IF IS_VEHICLE_DRIVEABLE(vehHandler)
									sceneHandlerA1 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
									TASK_SYNCHRONIZED_SCENE (pedFloyd, sceneHandlerA1, "missheistdockssetup1ig_14", "floyd_get_to_the_containers", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneHandlerA1,vehHandler,-1)
									SET_ENTITY_NO_COLLISION_ENTITY(pedFloyd,vehHandler,TRUE)
									iFloydHauler ++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			//GENERAL IDLE
			CASE 3
				IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_14")
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneHandlerA1)
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneHandlerA1) = 1
							IF NOT IS_PED_INJURED(pedFloyd)
								IF IS_VEHICLE_DRIVEABLE(vehHandler)
									sceneHandlerA1 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
									TASK_SYNCHRONIZED_SCENE (pedFloyd, sceneHandlerA1, "missheistdockssetup1ig_14", "floyd_idle_base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneHandlerA1,vehHandler,-1)
									SET_ENTITY_NO_COLLISION_ENTITY(pedFloyd,vehHandler,TRUE)
									SET_SYNCHRONIZED_SCENE_LOOPED(sceneHandlerA1,TRUE)
									iFloydHauler ++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			//SPECIFIC EVENTS
			CASE 4
				IF NOT IS_PED_INJURED(pedFloyd)
					IF eMissionStage != MISSION_STAGE_6
						IF IS_VEHICLE_DRIVEABLE(vehHandler)
							IF DOES_ENTITY_EXIST(objHandlerContainer[0])
							AND DOES_ENTITY_EXIST(objHandlerContainer[1])
								IF GET_DISTANCE_BETWEEN_ENTITIES(vehHandler,objHandlerContainer[0]) < 8
								OR GET_DISTANCE_BETWEEN_ENTITIES(vehHandler,objHandlerContainer[1]) < 8
									IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
										sceneHandlerA1 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
										TASK_SYNCHRONIZED_SCENE (pedFloyd, sceneHandlerA1, "missheistdockssetup1ig_14", "floyd_ok_now_grab_the_container", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
										ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneHandlerA1,vehHandler,-1)
										SET_ENTITY_NO_COLLISION_ENTITY(pedFloyd,vehHandler,TRUE)
										iFloydHauler = 3
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehHandler)
							IF DOES_ENTITY_EXIST(objHandlerContainer[0])
							AND DOES_ENTITY_EXIST(objHandlerContainer[1])
								IF IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
									IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehHandler),<<-97.1716, -2459.1846, 5.0204>>) > 10
										sceneHandlerA1 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
										TASK_SYNCHRONIZED_SCENE (pedFloyd, sceneHandlerA1, "missheistdockssetup1ig_14", "floyd_take_this_to_the_loading_area", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
										ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneHandlerA1,vehHandler,-1)
										SET_ENTITY_NO_COLLISION_ENTITY(pedFloyd,vehHandler,TRUE)
										iFloydHauler = 3
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehHandler)
							IF DOES_ENTITY_EXIST(objHandlerContainer[0])
							AND DOES_ENTITY_EXIST(objHandlerContainer[1])
								IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
									IF IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[0], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
									OR IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[1], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
										IF NOT HAS_LABEL_BEEN_TRIGGERED("GOOD WORK")
											sceneHandlerA1 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
											TASK_SYNCHRONIZED_SCENE (pedFloyd, sceneHandlerA1, "missheistdockssetup1ig_14", "floyd_good_work", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
											ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneHandlerA1,vehHandler,-1)
											SET_ENTITY_NO_COLLISION_ENTITY(pedFloyd,vehHandler,TRUE)
											SET_LABEL_AS_TRIGGERED("GOOD WORK",TRUE)
											iFloydHauler = 3
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehHandler)
							IF DOES_ENTITY_EXIST(objHandlerContainer[0])
							AND DOES_ENTITY_EXIST(objHandlerContainer[1])
								IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
									IF IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[0], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
									AND IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[1], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
										IF HAS_LABEL_BEEN_TRIGGERED("GOOD WORK")
											IF NOT HAS_LABEL_BEEN_TRIGGERED("TASK COMPLETE")
												sceneHandlerA1 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
												TASK_SYNCHRONIZED_SCENE (pedFloyd, sceneHandlerA1, "missheistdockssetup1ig_14", "floyd_task_complete", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
												ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneHandlerA1,vehHandler,-1)
												SET_ENTITY_NO_COLLISION_ENTITY(pedFloyd,vehHandler,TRUE)
												SET_LABEL_AS_TRIGGERED("TASK COMPLETE",TRUE)
												iFloydHauler = 3
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE 
						IF iProgress > 2
							IF IS_VEHICLE_DRIVEABLE(vehHandler)
								sceneHandlerA1 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
								TASK_SYNCHRONIZED_SCENE (pedFloyd, sceneHandlerA1, "missheistdockssetup1ig_14", "floyd_climb_down", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
								ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneHandlerA1,vehHandler,-1)
								SET_ENTITY_NO_COLLISION_ENTITY(pedFloyd,vehHandler,TRUE)
								iFloydHauler = 5
							ENDIF
						ENDIF
					ENDIF
				ENDIF

			BREAK
			
			//ONCE FLOYD HAS LEFT THE HANDLER
			CASE 5
				IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_14")
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneHandlerA1)
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneHandlerA1) = 1
							IF NOT IS_PED_INJURED(pedFloyd)
								IF IS_VEHICLE_DRIVEABLE(vehHandler)
									IF NOT IS_PED_INJURED(pedFloyd)
										SET_RAGDOLL_BLOCKING_FLAGS(pedFloyd,RBF_PLAYER_IMPACT)
									ENDIF
									OPEN_SEQUENCE_TASK(seqMain)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-120.65, -2422.97, 6.00>>,1.0,DEFAULT_TIME_NEVER_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_DEFAULT,-38.16)
										TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),-1)
										TASK_START_SCENARIO_IN_PLACE(NULL,"WORLD_HUMAN_STAND_IMPATIENT",0,TRUE)
										SET_ENTITY_NO_COLLISION_ENTITY(pedFloyd,vehHandler,TRUE)
									CLOSE_SEQUENCE_TASK(seqMain)
									TASK_PERFORM_SEQUENCE(pedFloyd, seqMain)
									iFloydHauler = 7
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			//SPECIAL CASE IF THE PLAYER SKIPS
			CASE 6
				IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_14")
					IF NOT IS_PED_INJURED(pedFloyd)
						IF IS_VEHICLE_DRIVEABLE(vehHandler)
							sceneHandlerA1 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
							TASK_SYNCHRONIZED_SCENE (pedFloyd, sceneHandlerA1, "missheistdockssetup1ig_14", "floyd_idle_base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT )
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneHandlerA1,vehHandler,-1)
							SET_ENTITY_NO_COLLISION_ENTITY(pedFloyd,vehHandler,TRUE)
							SET_SYNCHRONIZED_SCENE_LOOPED(sceneHandlerA1,TRUE)
							SET_LABEL_AS_TRIGGERED("TASK COMPLETE",TRUE)
							SET_LABEL_AS_TRIGGERED("GOOD WORK",TRUE)
							iFloydHauler = 4
						ENDIF
					ENDIF
				ENDIF
			BREAK
		
		ENDSWITCH
	ENDIF

ENDPROC

INT iHandlerHelpTimer

PROC MANAGE_HANDLER_ATTACHMENTS()
//	
//	[HD_HELP1:DOCKH1]
//	Position the handler's frame over a container using ~INPUT_SCRIPT_LEFT_AXIS_Y~.
//
//	[HD_HELP1B:DOCKH1]
//	Press ~INPUT_CONTEXT~ to pick up the container.
//
//	[HD_HELP2:DOCKH1]
//	Press ~INPUT_CONTEXT~ to release the container.
//
//[HD_HELP3:DOCKH1]
//Use ~INPUT_SCRIPT_LEFT_AXIS_Y~ to move the handler frame.
	
	SET_VEHICLE_LIGHTS(vehHandler,FORCE_VEHICLE_LIGHTS_OFF)
	//PRINTSTRING("SCRIPTS COMPILED?")
	
	IF IS_VEHICLE_DRIVEABLE(vehHandler)
		IF DOES_ENTITY_EXIST(objHandlerContainer[0])
		AND DOES_ENTITY_EXIST(objHandlerContainer[1])
			IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
				IF IS_HANDLER_FRAME_LINED_UP_WITH_CONTAINER(vehHandler,objHandlerContainer[0])
				OR IS_HANDLER_FRAME_LINED_UP_WITH_CONTAINER(vehHandler,objHandlerContainer[1])
					IF NOT HAS_LABEL_BEEN_TRIGGERED("HD_HELP1B")
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HD_HELP1")
							CLEAR_HELP()
						ELSE
							iHandlerHelpTimer = GET_GAME_TIMER()
							PRINT_HELP_FOREVER("HD_HELP1B")
							SET_LABEL_AS_TRIGGERED("HD_HELP1B",TRUE)
						ENDIF
					ENDIF
				ELSE
					IF HAS_LABEL_BEEN_TRIGGERED("HD_HELP1B")
						IF MANAGE_MY_TIMER(iHandlerHelpTimer,3000)
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HD_HELP1B")
								CLEAR_HELP()
								SET_LABEL_AS_TRIGGERED("HD_HELP1B",FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//"Container_Release"
	IF HAS_LABEL_BEEN_TRIGGERED("PICKED UP A CONTAINER")
		IF IS_VEHICLE_DRIVEABLE(vehHandler)
			IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
				//PRINTSTRING("DROPPED SOMETHING")PRINTNL()
				IF NOT HAS_LABEL_BEEN_TRIGGERED("Container_Release_audio")
					PRINTSTRING("PLAYING RELEASE AUDIO")PRINTNL()
					PLAY_SOUND_FROM_ENTITY(-1,"Container_Release",vehHandler,"CONTAINER_LIFTER_SOUNDS")
					//DETACH_CONTAINER_FROM_HANDLER_FRAME(vehHandler)
					iReleaseTimer = GET_GAME_TIMER()
					SET_LABEL_AS_TRIGGERED("CONTAINER LAND",FALSE)
					SET_LABEL_AS_TRIGGERED("Container_Release_audio",TRUE)
				ENDIF
			ENDIF
		ENDIF
	ELSE	
		IF IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("PICKED UP A CONTAINER")
				SET_LABEL_AS_TRIGGERED("PICKED UP A CONTAINER",TRUE)
				PRINTSTRING("PICKED UP A CONTAINER")PRINTNL()
			ENDIF
		ENDIF
	ENDIF
	
	//"Container_Land"\
	IF HAS_LABEL_BEEN_TRIGGERED("PICKED UP A CONTAINER")
		IF MANAGE_MY_TIMER(iReleaseTimer,20)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("CONTAINER LAND")
				IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(objHandlerContainer[0])
					OR HAS_ENTITY_COLLIDED_WITH_ANYTHING(objHandlerContainer[1])
						IF REQUEST_SCRIPT_AUDIO_BANK("Container_Lifter")
							PRINTSTRING("PLAYING Container_Land SOUND")PRINTNL()
							PLAY_SOUND_FROM_ENTITY(-1,"Container_Land",vehHandler,"CONTAINER_LIFTER_SOUNDS")
						ENDIF
						SET_LABEL_AS_TRIGGERED("CONTAINER LAND",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
		IF MANAGE_MY_TIMER(iReleaseTimer,1000)
			IF IS_VEHICLE_DRIVEABLE(vehHandler)
				IF DOES_ENTITY_EXIST(objHandlerContainer[0])
				AND DOES_ENTITY_EXIST(objHandlerContainer[1])
					IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
						IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_CONTEXT)
							IF IS_HANDLER_FRAME_LINED_UP_WITH_CONTAINER(vehHandler,objHandlerContainer[0])
								IF REQUEST_SCRIPT_AUDIO_BANK("Container_Lifter")
									PRINTSTRING("PLAYING ATTACH SOUND")PRINTNL()
									PLAY_SOUND_FROM_ENTITY(-1,"Container_Attach",vehHandler,"CONTAINER_LIFTER_SOUNDS")
								ENDIF
								ATTACH_CONTAINER_TO_HANDLER_FRAME_WHEN_LINED_UP(vehHandler,objHandlerContainer[0])
								PRINTSTRING("ATTACHING")PRINTNL()
								IF NOT HAS_LABEL_BEEN_TRIGGERED("PICKED UP A CONTAINER")
									SET_LABEL_AS_TRIGGERED("PICKED UP A CONTAINER",TRUE)
								ENDIF
								SET_LABEL_AS_TRIGGERED("Container_Release_audio",FALSE)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
						IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_CONTEXT)
							IF IS_HANDLER_FRAME_LINED_UP_WITH_CONTAINER(vehHandler,objHandlerContainer[1])
								IF REQUEST_SCRIPT_AUDIO_BANK("Container_Lifter")
									PRINTSTRING("PLAYING ATTACH SOUND")PRINTNL()
									PLAY_SOUND_FROM_ENTITY(-1,"Container_Attach",vehHandler,"CONTAINER_LIFTER_SOUNDS")
								ENDIF
								ATTACH_CONTAINER_TO_HANDLER_FRAME_WHEN_LINED_UP(vehHandler,objHandlerContainer[1])
								PRINTSTRING("ATTACHING")PRINTNL()
								IF NOT HAS_LABEL_BEEN_TRIGGERED("PICKED UP A CONTAINER")
									SET_LABEL_AS_TRIGGERED("PICKED UP A CONTAINER",TRUE)
								ENDIF
								SET_LABEL_AS_TRIGGERED("Container_Release_audio",FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HD_HELP1B")
			CLEAR_HELP()
		ENDIF
		
	ENDIF
ENDPROC


FLOAT fHandlerHealth
INT iHandlerDamageTimer
BOOL bDropContainerObjective = FALSE

PROC MANAGE_HANDLER_DIALOGUE()
	//GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
	//GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_LABEL()
	
	SWITCH iHandlerDialogueSwitch
	
		CASE 0
			IF IS_VEHICLE_DRIVEABLE(vehHandler)
				
				//If the player damages the handler - wreckless
				IF NOT IS_PED_INJURED(pedFloyd)
					IF GET_ENTITY_SPEED(vehHandler) > 30.0 AND HAS_ENTITY_COLLIDED_WITH_ANYTHING(vehHandler)
					OR fHandlerHealth > GET_ENTITY_HEALTH(vehHandler)+GET_VEHICLE_ENGINE_HEALTH(vehHandler)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_07hb")
							
							INTERRUPT_CONVERSATION(pedFloyd,"DH1_DXAA","FLOYD") 
							iHandlerDamageTimer = GET_GAME_TIMER()
							SET_LABEL_AS_TRIGGERED("DS1_07hb",TRUE)
							//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	//						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
	//							IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_07hb", CONV_PRIORITY_MEDIUM)
	//								iHandlerDamageTimer = GET_GAME_TIMER()
	//								SET_LABEL_AS_TRIGGERED("DS1_07hb",TRUE)
	//							ENDIF
	//						ENDIF
						ELSE
							IF MANAGE_MY_TIMER(iHandlerDamageTimer,12000)
								SET_LABEL_AS_TRIGGERED("DS1_07hb",FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				fHandlerHealth = GET_ENTITY_HEALTH(vehHandler)+GET_VEHICLE_ENGINE_HEALTH(vehHandler)
				
				//Player damages a crate
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_41b")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF IS_VEHICLE_DRIVEABLE(vehHandler)
							IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
								IF GET_ENTITY_SPEED(objHandlerContainer[0]) > 4.0
								OR GET_ENTITY_SPEED(objHandlerContainer[1]) > 4.0
									IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_41b", CONV_PRIORITY_MEDIUM)
										iDamagedContainer = GET_GAME_TIMER()
										SET_LABEL_AS_TRIGGERED("DS1_41b",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF MANAGE_MY_TIMER(iDamagedContainer,7000)
						SET_LABEL_AS_TRIGGERED("DS1_41b",FALSE)
					ENDIF
				ENDIF
				
				
				//If the player is near to the containers - Floyd sees them
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_sees")
					IF DOES_ENTITY_EXIST(objHandlerContainer[0])
					AND DOES_ENTITY_EXIST(objHandlerContainer[1])
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),objHandlerContainer[0]) < 15
						OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),objHandlerContainer[1]) < 15
							KILL_FACE_TO_FACE_CONVERSATION()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_sees", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("DS1_sees",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//If the player is near the containers tells them to line up with them
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_line")
					IF DOES_ENTITY_EXIST(objHandlerContainer[0])
					AND DOES_ENTITY_EXIST(objHandlerContainer[1])
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),objHandlerContainer[0]) < 4
						OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),objHandlerContainer[1]) < 4
							KILL_FACE_TO_FACE_CONVERSATION()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_line", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("DS1_line",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//handler in place, Floyd says to attach the crate
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_attach")
					IF DOES_ENTITY_EXIST(objHandlerContainer[0])
					AND DOES_ENTITY_EXIST(objHandlerContainer[1])
						IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
							IF NOT IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[0], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
								IF IS_HANDLER_FRAME_LINED_UP_WITH_CONTAINER(vehHandler,objHandlerContainer[0])
									IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
										KILL_FACE_TO_FACE_CONVERSATION()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_attach", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("DS1_attach",TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
							IF NOT IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[1], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
								IF IS_HANDLER_FRAME_LINED_UP_WITH_CONTAINER(vehHandler,objHandlerContainer[1])
									IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
										KILL_FACE_TO_FACE_CONVERSATION()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_attach", CONV_PRIORITY_MEDIUM)
												SET_LABEL_AS_TRIGGERED("DS1_attach",TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF HAS_LABEL_BEEN_TRIGGERED("DS1_08")
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_attach2")
							IF DOES_ENTITY_EXIST(objHandlerContainer[0])
							AND DOES_ENTITY_EXIST(objHandlerContainer[1])
								IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
									IF NOT IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[0], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
										IF IS_HANDLER_FRAME_LINED_UP_WITH_CONTAINER(vehHandler,objHandlerContainer[0])
											IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
												KILL_FACE_TO_FACE_CONVERSATION()
												IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
													IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_attach", CONV_PRIORITY_MEDIUM)
														SET_LABEL_AS_TRIGGERED("DS1_attach2",TRUE)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
									IF NOT IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[1], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
										IF IS_HANDLER_FRAME_LINED_UP_WITH_CONTAINER(vehHandler,objHandlerContainer[1])
											IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
												KILL_FACE_TO_FACE_CONVERSATION()
												IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
													IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_attach", CONV_PRIORITY_MEDIUM)
														SET_LABEL_AS_TRIGGERED("DS1_attach2",TRUE)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//picked up the container
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_dockf")
					IF IS_VEHICLE_DRIVEABLE(vehHandler)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
							IF IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
								KILL_FACE_TO_FACE_CONVERSATION()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_dockf", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("DS1_dockf",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Random chat while going to container
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_13")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_13", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("DS1_13",TRUE)
						ENDIF
					ENDIF
				ELSE
					IF HAS_LABEL_BEEN_TRIGGERED("DS1_attach2")
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_13b")
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_13b", CONV_PRIORITY_MEDIUM)
									REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
									SET_LABEL_AS_TRIGGERED("DS1_13b",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Random chat after picking up the first container
				IF HAS_LABEL_BEEN_TRIGGERED("DS1_dockf")
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_13c")
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_13c", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("DS1_13c",TRUE)
							ENDIF
						ENDIF
					ELSE
						//First container in place
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_08")
							IF IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[0], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
							OR IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[1], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
								KILL_FACE_TO_FACE_CONVERSATION()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_Drop", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("DS1_08",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_13d")
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_13d", CONV_PRIORITY_MEDIUM)
										SET_LABEL_AS_TRIGGERED("DS1_13d",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF

			ENDIF
			
		
		BREAK
	
	
	ENDSWITCH

	
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_07hb")
		IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
			IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_07hb", CONV_PRIORITY_MEDIUM)
				SET_LABEL_AS_TRIGGERED("DS1_07hb",TRUE)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC



PROC MANAGE_HANDLER_SECTION()

	IF IS_VEHICLE_DRIVEABLE(vehHandler)
		IF IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
			bCarryingContainer = TRUE
		ELSE
			bCarryingContainer = FALSE
		ENDIF
	ENDIF

	MANAGE_HANDLER_ATTACHMENTS()
	
	IF iHandlerSection > 1
		MANAGE_HANDLER_DIALOGUE()
	ENDIF
	
	PRINTSTRING("iHandler:")PRINTINT(iHandlerSection)//PRINTNL()

	SWITCH iHandlerSection
	
		CASE 0
			REQUEST_SCRIPT_AUDIO_BANK("Container_Lifter")
			SET_LABEL_AS_TRIGGERED("HD_HELP1",FALSE)
			iHandlerSection ++
		BREAK
	
		CASE 1
			IF IS_VEHICLE_DRIVEABLE(vehHandler)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
				
					IF HAS_LABEL_BEEN_TRIGGERED("DS1_07hc")
						SET_LABEL_AS_TRIGGERED("DS1_07hc",FALSE)
					ENDIF

					IF IS_THIS_PRINT_BEING_DISPLAYED("AW_BACK_IN_HAND")
						CLEAR_PRINTS()
					ENDIF
					IF DOES_BLIP_EXIST(veh_blip)
						SAFE_REMOVE_BLIP(veh_blip)
					ENDIF
					IF NOT HAS_LABEL_BEEN_TRIGGERED("HD_HELP1")
						PRINT_HELP_FOREVER("HD_HELP1")
						SET_LABEL_AS_TRIGGERED("HD_HELP1",TRUE)
					ENDIF
					IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
						IF DOES_ENTITY_EXIST(objHandlerContainer[0])
							IF NOT IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[0], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
								SAFE_ADD_BLIP_LOCATION(blipHandlerContainers[0],GET_ENTITY_COORDS(objHandlerContainer[0]))
								SET_BLIP_COLOUR(blipHandlerContainers[0],BLIP_COLOUR_GREEN)
								SET_BLIP_NAME_FROM_TEXT_FILE(blipHandlerContainers[0],"CONT_LABEL")
								//SET_BLIP_SCALE(blipHandlerContainers[0],0.5)
							ELSE
								SAFE_REMOVE_BLIP(blipHandlerContainers[0])
							ENDIF
						ENDIF
						IF DOES_ENTITY_EXIST(objHandlerContainer[1])
							IF NOT IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[1], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
								SAFE_ADD_BLIP_LOCATION(blipHandlerContainers[1],GET_ENTITY_COORDS(objHandlerContainer[1]))
								SET_BLIP_COLOUR(blipHandlerContainers[1],BLIP_COLOUR_GREEN)
								SET_BLIP_NAME_FROM_TEXT_FILE(blipHandlerContainers[1],"CONT_LABEL")
								//SET_BLIP_SCALE(blipHandlerContainers[1],0.5)
							ELSE
//								IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_08")
//									KILL_FACE_TO_FACE_CONVERSATION()
//									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
//										IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_08", CONV_PRIORITY_MEDIUM)
//											SET_LABEL_AS_TRIGGERED("DS1_08",TRUE)
//										ENDIF
//									ENDIF
//								ENDIF
								SAFE_REMOVE_BLIP(blipHandlerContainers[1])
							ENDIF
						ENDIF
					ENDIF
					
					bToggleObjective = FALSE
					iHandlerSection ++
				ELSE
					IF bToggleObjective = FALSE
						IF NOT IS_THIS_PRINT_BEING_DISPLAYED("AW_BACK_IN_HAND")
							SAFE_REMOVE_BLIP(blipHandlerContainers[0])
							SAFE_REMOVE_BLIP(blipHandlerContainers[1])
							SAFE_REMOVE_BLIP(blipHaulerLoadingArea)
							SAFE_ADD_BLIP_VEHICLE(veh_blip,vehHandler,TRUE)
							KILL_FACE_TO_FACE_CONVERSATION()
							IF IS_HELP_MESSAGE_BEING_DISPLAYED()
								CLEAR_HELP()
							ENDIF
							CLEAR_PRINTS()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
								IF NOT HAS_LABEL_BEEN_TRIGGERED("AW_BACK_IN_HAND")
									PRINT("AW_BACK_IN_HAND",DEFAULT_GOD_TEXT_TIME,0)
									SET_LABEL_AS_TRIGGERED("AW_BACK_IN_HAND",TRUE)
								ENDIF
								bToggleObjective = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF HAS_LABEL_BEEN_TRIGGERED("AW_BACK_IN_HAND")
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_07hc")
							KILL_FACE_TO_FACE_CONVERSATION()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_07hc", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("DS1_07hc",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
		
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
				iHandlerSection = 1
			ENDIF
			
			
			IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HD_HELP2")
					CLEAR_HELP()
				ENDIF
			ENDIF
		
			
			IF IS_VEHICLE_DRIVEABLE(vehHandler)
				IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
					IF DOES_ENTITY_EXIST(objHandlerContainer[0])
						IF NOT IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[0], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
							IF NOT DOES_BLIP_EXIST(blipHandlerContainers[0])
								SAFE_ADD_BLIP_LOCATION(blipHandlerContainers[0],GET_ENTITY_COORDS(objHandlerContainer[0]))
								SET_BLIP_COLOUR(blipHandlerContainers[0],BLIP_COLOUR_GREEN)
								SET_BLIP_NAME_FROM_TEXT_FILE(blipHandlerContainers[0],"CONT_LABEL")
							ENDIF
						ELSE
							SET_LABEL_AS_TRIGGERED("1st CONTAINER IN",TRUE)
							SAFE_REMOVE_BLIP(blipHandlerContainers[0])
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(objHandlerContainer[1])
						IF NOT IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[1], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
							IF NOT DOES_BLIP_EXIST(blipHandlerContainers[1])
								SAFE_ADD_BLIP_LOCATION(blipHandlerContainers[1],GET_ENTITY_COORDS(objHandlerContainer[1]))
								SET_BLIP_COLOUR(blipHandlerContainers[1],BLIP_COLOUR_GREEN)
								SET_BLIP_NAME_FROM_TEXT_FILE(blipHandlerContainers[1],"CONT_LABEL")
							ENDIF
						ELSE
							SET_LABEL_AS_TRIGGERED("2nd CONTAINER IN",TRUE)
							SAFE_REMOVE_BLIP(blipHandlerContainers[1])
							//REPLAY_RECORD_BACK_FOR_TIME(1)
						ENDIF
					ENDIF
					
					IF DOES_BLIP_EXIST(blipHaulerLoadingArea)
						SAFE_REMOVE_BLIP(blipHaulerLoadingArea)
					ENDIF
					
					IF GET_DISTANCE_BETWEEN_ENTITIES(vehHandler,objHandlerContainer[0]) < 30
					OR GET_DISTANCE_BETWEEN_ENTITIES(vehHandler,objHandlerContainer[1]) < 30
						IF bHelpText = FALSE
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HD_HELP1")
								PRINT_HELP("HD_HELP1")
								SET_LABEL_AS_TRIGGERED("HD_HELP1",TRUE)
								bHelpText = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//CLEAR_PRINTS()
					//TAKE THE CRATE TO THE LOADING AREA
					//KILL_FACE_TO_FACE_CONVERSATION()
					SAFE_REMOVE_BLIP(blipHandlerContainers[0])
					SAFE_REMOVE_BLIP(blipHandlerContainers[1])
					SAFE_REMOVE_BLIP(blipHandlerContainers[2])
					SAFE_ADD_BLIP_LOCATION(blipHaulerLoadingArea,vForkLiftLoadingArea)
					bGodText = FALSE
					bDropContainerObjective = FALSE
					iHandlerSection ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
					IF bGodText = FALSE
						//Only do this god text once
						IF HAS_LABEL_BEEN_TRIGGERED("AW_HAND_GOTC")
							PRINT("AW_HAND_GOTC",DEFAULT_GOD_TEXT_TIME,0)
							bGodText = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
				iHandlerSection = 1
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehHandler)
				IF NOT IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
					IF NOT IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[0], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
					AND NOT IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[1], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
						CLEAR_PRINTS()
						//DRIVE TO THE CRATES
						SAFE_REMOVE_BLIP(blipHaulerLoadingArea)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("AW_HAND_TASK")
							PRINT("AW_HAND_TASK",DEFAULT_GOD_TEXT_TIME,0)
							SET_LABEL_AS_TRIGGERED("AW_HAND_TASK",TRUE)
						ENDIF
					ENDIF
					IF IS_THIS_PRINT_BEING_DISPLAYED("AW_HAND_GOTC")
						CLEAR_PRINTS()
					ENDIF
					IF IS_THIS_PRINT_BEING_DISPLAYED("AW_HAND_DROP")
						CLEAR_PRINTS()
					ENDIF
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF
					iHandlerSection = 2
				ELSE
					IF IS_ANY_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler)
						IF bDropContainerObjective = FALSE
							IF IS_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler,objHandlerContainer[0])
							AND IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[0], <<-93.626404,-2450.864014,28.266687>>, <<-102.872566,-2464.122803,3.520514>>, 16.250000)
								//SET THE CRATE DOWN IN THIS AREA
								IF NOT HAS_LABEL_BEEN_TRIGGERED("AW_HAND_DROP")
									CLEAR_PRINTS()
									PRINT("AW_HAND_DROP",DEFAULT_GOD_TEXT_TIME,0)
									SET_LABEL_AS_TRIGGERED("AW_HAND_DROP",TRUE)
								ENDIF
								PRINT_HELP("HD_HELP2")
								bDropContainerObjective = TRUE
							ENDIF
							IF IS_ENTITY_ATTACHED_TO_HANDLER_FRAME(vehHandler,objHandlerContainer[1])
							AND IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[1], <<-93.626404,-2450.864014,28.266687>>, <<-102.872566,-2464.122803,3.520514>>, 16.250000)
								//SET THE CRATE DOWN IN THIS AREA
								IF NOT HAS_LABEL_BEEN_TRIGGERED("AW_HAND_DROP")
									CLEAR_PRINTS()
									PRINT("AW_HAND_DROP",DEFAULT_GOD_TEXT_TIME,0)
									SET_LABEL_AS_TRIGGERED("AW_HAND_DROP",TRUE)
								ENDIF
								PRINT_HELP("HD_HELP2")
								bDropContainerObjective = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH
	
ENDPROC

INT iMWShoutTimer

PROC MERRYWEATHER_WITH_DOCK_WORKER()
/*
	| o ) |  / \ / _| |// | | |/ \_ _| __| o \ | | | | |_ _| U | |  \ / \ / _| |// | | |/ \| o \ |// __| o Y _|
	| o \ |_| o ( (_|  (| V V | o | || _||   / | V V | || ||   | | o | o | (_|  (| V V ( o )   /  (| _||   |_ \
	|___/___|_n_|\__|_|\\\_n_/|_n_|_||___|_|\\  \_n_/|_||_||_n_| |__/ \_/ \__|_|\\\_n_/ \_/|_|\\_|\\___|_|\\__/*/	
	
	IF s_sp_blackwater_with_dockworker[0].i_event > 0
	AND s_sp_blackwater_with_dockworker[0].i_event < 5
		IF NOT HAS_LABEL_BEEN_TRIGGERED("MERRYWEATHER SHOUT")
			IF GET_RANDOM_BOOL()
				IF NOT IS_PED_DEAD_OR_DYING(s_sp_blackwater_with_dockworker[1].ped)
					PLAY_PED_AMBIENT_SPEECH(s_sp_blackwater_with_dockworker[1].ped,"GENERIC_INSULT_HIGH")
					SET_LABEL_AS_TRIGGERED("MERRYWEATHER SHOUT",TRUE)
					iMWShoutTimer = GET_GAME_TIMER()
				ENDIF
			ELSE
				IF NOT IS_PED_DEAD_OR_DYING(s_sp_blackwater_with_dockworker[2].ped)
					PLAY_PED_AMBIENT_SPEECH(s_sp_blackwater_with_dockworker[2].ped,"GENERIC_INSULT_HIGH")
					SET_LABEL_AS_TRIGGERED("MERRYWEATHER SHOUT",TRUE)
					iMWShoutTimer = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ELSE
			IF MANAGE_MY_TIMER(iMWShoutTimer,GET_RANDOM_INT_IN_RANGE(7000,14000))
				SET_LABEL_AS_TRIGGERED("MERRYWEATHER SHOUT",FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	//BREAK OUT IF MESSED WITH
	IF s_sp_blackwater_with_dockworker[0].i_event < 7
		IF DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[0].ped)
		AND DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[1].ped)
		AND DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[2].ped)
			IF NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[0].ped)
			AND NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[1].ped)
			AND NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[2].ped)
				IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(),s_sp_blackwater_with_dockworker[0].ped)
				OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(),s_sp_blackwater_with_dockworker[0].ped)
				OR IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(),s_sp_blackwater_with_dockworker[0].ped)
					STOP_SYNCHRONIZED_ENTITY_ANIM(s_sp_blackwater_with_dockworker[0].ped,SLOW_BLEND_OUT,TRUE)
					TASK_SMART_FLEE_COORD(s_sp_blackwater_with_dockworker[0].ped, << -294.5143, -2497.9805, 5.0015 >>,100,-1)
					STOP_SYNCHRONIZED_ENTITY_ANIM(s_sp_blackwater_with_dockworker[1].ped,SLOW_BLEND_OUT,TRUE)
					TASK_COMBAT_PED(s_sp_blackwater_with_dockworker[1].ped,PLAYER_PED_ID())
					STOP_SYNCHRONIZED_ENTITY_ANIM(s_sp_blackwater_with_dockworker[2].ped,SLOW_BLEND_OUT,TRUE)
					TASK_COMBAT_PED(s_sp_blackwater_with_dockworker[2].ped,PLAYER_PED_ID())
					s_sp_blackwater_with_dockworker[0].i_event = 7
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF bSetPiece[SS_blackwater_with_dockworker]
	
		MANAGE_SPEECH_FOR_AMBIENT_SET_PIECE(AMBSP_BLACKWATER_WITH_DOCK_WORKER)
		
		IF NOT DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[0].ped)
			IF eMissionStage > MISSION_STAGE_1
			AND eMissionStage < MISSION_STAGE_5
				REQUEST_MODEL(s_sp_blackwater_with_dockworker[0].model)
				REQUEST_MODEL(s_sp_blackwater_with_dockworker[1].model)
				REQUEST_MODEL(s_sp_blackwater_with_dockworker[2].model)
				
				IF HAS_MODEL_LOADED(s_sp_blackwater_with_dockworker[0].model)
				AND HAS_MODEL_LOADED(s_sp_blackwater_with_dockworker[1].model)
				AND HAS_MODEL_LOADED(s_sp_blackwater_with_dockworker[2].model)
					s_sp_blackwater_with_dockworker[0].ped = CREATE_SETPIECE_PED(s_sp_blackwater_with_dockworker[0].model,<< -294.5143, -2497.9805, 5.0015 >>, 87.3385, rel_group_buddies, 200, 0)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(s_sp_blackwater_with_dockworker[0].ped,TRUE)
					SET_PED_CAPSULE(s_sp_blackwater_with_dockworker[0].ped,0.4)
					s_sp_blackwater_with_dockworker[1].ped = CREATE_SETPIECE_PED(s_sp_blackwater_with_dockworker[1].model,<< -295.4659, -2496.8794, 5.0015 >>, 349.2262, rel_group_buddies, 200, 0)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(s_sp_blackwater_with_dockworker[1].ped,TRUE)
					s_sp_blackwater_with_dockworker[2].ped = CREATE_SETPIECE_PED(s_sp_blackwater_with_dockworker[2].model,<< -294.3614, -2495.9116, 5.0015 >>, 181.5108, rel_group_buddies, 200, 0)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(s_sp_blackwater_with_dockworker[2].ped,TRUE)
					INITIALISE_SETPIECE_GROUP(s_sp_blackwater_with_dockworker, "Blackwater with dockworker")
				ENDIF
			ENDIF
		ELSE
			//All the enemies are triggered off the same locate, so do outside the loop.
			IF bSetPiece[SS_blackwater_with_dockworker]
				
				//PRINTSTRING("_sp_blackwater_with_dockworker[0].i_event: ")PRINTINT(s_sp_blackwater_with_dockworker[0].i_event)PRINTNL()
				
				IF s_sp_blackwater_with_dockworker[0].i_event > 3
					REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2221950
				ENDIF
				
				SWITCH s_sp_blackwater_with_dockworker[0].i_event
					//GET PEDS INTO POSITION
					
					CASE 0
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),S_sd_blackwater_with_dockworker.v_scene_pos) < 60
							REQUEST_ANIM_DICT("missheistdockssetup1ig_13@start_idle")
							INIT_SYNCH_SCENE_AUDIO_WITH_POSITION("LSDHS_IG_13", S_sd_blackwater_with_dockworker.v_scene_pos)
							s_sp_blackwater_with_dockworker[0].i_event ++
						ENDIF
					BREAK
						
					CASE 1
						REQUEST_ANIM_DICT("missheistdockssetup1ig_13@start_idle")
						IF PREPARE_SYNCHRONIZED_AUDIO_EVENT("LSDHS_IG_13", 0)
							IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_13@start_idle")
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),S_sd_blackwater_with_dockworker.v_scene_pos) < 60
									IF DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[0].ped)
									AND DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[1].ped)
									AND DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[2].ped)
										IF NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[0].ped)
										AND NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[1].ped)
										AND NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[2].ped)
											
											//CLEARING UP PREVIOUS SCENES ONCE WE GET HERE
											IF NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[0].ped)
												CLEAR_PED_TASKS(s_sp_dock_workers_on_pipe[0].ped)
												SET_PED_AS_NO_LONGER_NEEDED(s_sp_dock_workers_on_pipe[0].ped)
											ENDIF
											
											IF NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[1].ped)
												CLEAR_PED_TASKS(s_sp_dock_workers_on_pipe[1].ped)
												SET_PED_AS_NO_LONGER_NEEDED(s_sp_dock_workers_on_pipe[1].ped)
											ENDIF
											
											IF NOT IS_PED_INJURED(s_sp_dock_workers_on_pipe[2].ped)
												CLEAR_PED_TASKS(s_sp_dock_workers_on_pipe[2].ped)
												SET_PED_AS_NO_LONGER_NEEDED(s_sp_dock_workers_on_pipe[2].ped)
											ENDIF
											
											REMOVE_ANIM_DICT("missheistdockssetup1ig_10@idle_a")
											REMOVE_ANIM_DICT("missheistdockssetup1ig_10@idle_b")
											REMOVE_ANIM_DICT("missheistdockssetup1ig_10@idle_c")
											REMOVE_ANIM_DICT( "missheistdockssetup1ig_10@idle_d")
											
											IF NOT IS_PED_INJURED(s_sp_dock_workers_talking[0].ped)
												CLEAR_PED_TASKS(s_sp_dock_workers_talking[0].ped)
												SET_PED_AS_NO_LONGER_NEEDED(s_sp_dock_workers_talking[0].ped)
											ENDIF
											
											IF NOT IS_PED_INJURED(s_sp_dock_workers_talking[1].ped)
												CLEAR_PED_TASKS(s_sp_dock_workers_talking[1].ped)
												SET_PED_AS_NO_LONGER_NEEDED(s_sp_dock_workers_talking[1].ped)
											ENDIF
											
											REMOVE_ANIM_DICT("missheistdockssetup1ig_5@base")
											
											sceneBlackWater1Id = CREATE_SYNCHRONIZED_SCENE(S_sd_blackwater_with_dockworker.v_scene_pos,S_sd_blackwater_with_dockworker.v_scene_rot)
											TASK_SYNCHRONIZED_SCENE (s_sp_blackwater_with_dockworker[0].ped,sceneBlackWater1Id, "missheistdockssetup1ig_13@start_idle", "guard_beatup_startidle_dockworker", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
											TASK_SYNCHRONIZED_SCENE (s_sp_blackwater_with_dockworker[1].ped, sceneBlackWater1Id,"missheistdockssetup1ig_13@start_idle", "guard_beatup_startidle_guard1", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
											TASK_SYNCHRONIZED_SCENE (s_sp_blackwater_with_dockworker[2].ped, sceneBlackWater1Id,"missheistdockssetup1ig_13@start_idle", "guard_beatup_startidle_guard2", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
											SET_SYNCHRONIZED_SCENE_LOOPED(sceneBlackWater1Id,TRUE)
											REQUEST_ANIM_DICT("missheistdockssetup1ig_13@main_action")
											s_sp_blackwater_with_dockworker[0].i_event ++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					//START THE ACTION - WHEN THE PLAYER APPROACHES GET COORDS FOR CONDITION
					CASE 2
						REQUEST_ANIM_DICT("missheistdockssetup1ig_13@main_action")
						IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_13@main_action")
							
							IF VDIST(GET_ENTITY_COORDS(PLAYER_PED_ID()),S_sd_blackwater_with_dockworker.v_scene_pos) < 50
								IF DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[0].ped)
								AND DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[1].ped)
								AND DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[2].ped)
								
									IF NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[0].ped)
									AND NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[1].ped)
									AND NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[2].ped)
										IF IS_ENTITY_ON_SCREEN(s_sp_blackwater_with_dockworker[0].ped)
											IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlackWater1Id)
												
												IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBlackWater1Id) > 0.99
													
													sceneBlackWater2Id = CREATE_SYNCHRONIZED_SCENE(S_sd_blackwater_with_dockworker.v_scene_pos,S_sd_blackwater_with_dockworker.v_scene_rot)
													TASK_SYNCHRONIZED_SCENE (s_sp_blackwater_with_dockworker[0].ped,sceneBlackWater2Id, "missheistdockssetup1ig_13@main_action", "guard_beatup_mainaction_dockworker", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
													TASK_SYNCHRONIZED_SCENE (s_sp_blackwater_with_dockworker[1].ped, sceneBlackWater2Id, "missheistdockssetup1ig_13@main_action", "guard_beatup_mainaction_guard1",  REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
													TASK_SYNCHRONIZED_SCENE (s_sp_blackwater_with_dockworker[2].ped, sceneBlackWater2Id, "missheistdockssetup1ig_13@main_action", "guard_beatup_mainaction_guard2",  REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
													SET_SYNCHRONIZED_SCENE_LOOPED(sceneBlackWater2Id,FALSE)
													
													IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_13@start_idle")
														REMOVE_ANIM_DICT("missheistdockssetup1ig_13@start_idle")
													ENDIF
													
													PLAY_SYNCHRONIZED_AUDIO_EVENT(sceneBlackWater2Id)
													
													REQUEST_ANIM_DICT("missheistdockssetup1ig_13@kick_idle")
													
													//s_sp_blackwater_with_dockworker[0].i_event ++
													s_sp_blackwater_with_dockworker[0].i_event = 4
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					//BEGIN KICKING
					CASE 3

						REQUEST_ANIM_DICT("missheistdockssetup1ig_13@kick_idle")
						IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_13@kick_idle")
							IF DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[0].ped)
							AND DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[1].ped)
							AND DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[2].ped)
								IF NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[0].ped)
								AND NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[1].ped)
								AND NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[2].ped)
									IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlackWater2Id)
										IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBlackWater2Id) >= 1
											sceneBlackWater3Id = CREATE_SYNCHRONIZED_SCENE(S_sd_blackwater_with_dockworker.v_scene_pos,S_sd_blackwater_with_dockworker.v_scene_rot)
											TASK_SYNCHRONIZED_SCENE (s_sp_blackwater_with_dockworker[0].ped,sceneBlackWater3Id, "missheistdockssetup1ig_13@kick_idle", "guard_beatup_kickidle_dockworker", REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
											TASK_SYNCHRONIZED_SCENE (s_sp_blackwater_with_dockworker[1].ped, sceneBlackWater3Id,"missheistdockssetup1ig_13@kick_idle", "guard_beatup_kickidle_guard1",  REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
											TASK_SYNCHRONIZED_SCENE (s_sp_blackwater_with_dockworker[2].ped, sceneBlackWater3Id, "missheistdockssetup1ig_13@kick_idle", "guard_beatup_kickidle_guard2", REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT  )
											SET_SYNCHRONIZED_SCENE_LOOPED(sceneBlackWater3Id,FALSE)
											
											IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_13@main_action")
												REMOVE_ANIM_DICT( "missheistdockssetup1ig_13@main_action")
											ENDIF
											
											REQUEST_ANIM_DICT("missheistdockssetup1ig_13@exit")
											s_sp_blackwater_with_dockworker[0].i_event ++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					//EXIT BEATUP
					CASE 4
						
						
						REQUEST_ANIM_DICT("missheistdockssetup1ig_13@exit")
						IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_13@exit")
							IF DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[0].ped)
							AND DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[1].ped)
							AND DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[2].ped)
								IF NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[0].ped)
								AND NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[1].ped)
								AND NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[2].ped)
									IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlackWater2Id)
									//IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlackWater3Id)
										//IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBlackWater3Id) > 0.99
										IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBlackWater2Id) > 0.99
											IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_13@kick_idle")
												REMOVE_ANIM_DICT("missheistdockssetup1ig_13@kick_idle")
											ENDIF
											IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_13@main_action")
												REMOVE_ANIM_DICT( "missheistdockssetup1ig_13@main_action")
											ENDIF
											sceneBlackWater4Id = CREATE_SYNCHRONIZED_SCENE(S_sd_blackwater_with_dockworker.v_scene_pos,S_sd_blackwater_with_dockworker.v_scene_rot)
											TASK_SYNCHRONIZED_SCENE (s_sp_blackwater_with_dockworker[0].ped,sceneBlackWater4Id, "missheistdockssetup1ig_13@exit", "guard_beatup_exit_dockworker", REALLY_SLOW_BLEND_IN, INSTANT_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
											TASK_SYNCHRONIZED_SCENE (s_sp_blackwater_with_dockworker[1].ped, sceneBlackWater4Id,"missheistdockssetup1ig_13@exit", "guard_beatup_exit_guard1", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
											TASK_SYNCHRONIZED_SCENE (s_sp_blackwater_with_dockworker[2].ped, sceneBlackWater4Id, "missheistdockssetup1ig_13@exit", "guard_beatup_exit_guard2", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
											SET_SYNCHRONIZED_SCENE_LOOPED(sceneBlackWater4Id,FALSE)
											
											s_sp_blackwater_with_dockworker[0].i_event ++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					//FINISH UP - CLEAR TASKS ECT
					CASE 5
						REQUEST_ANIM_DICT("missheistdockssetup1ig_13@exit")
						IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_13@exit")
							IF DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[0].ped)
							AND DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[1].ped)
							AND DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[2].ped)
								IF NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[0].ped)
								AND NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[1].ped)
								AND NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[2].ped)
									IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlackWater4Id)
										IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBlackWater4Id) > 0.99
											
											sceneBlackWater5Id = CREATE_SYNCHRONIZED_SCENE(S_sd_blackwater_with_dockworker.v_scene_pos,S_sd_blackwater_with_dockworker.v_scene_rot)
											TASK_SYNCHRONIZED_SCENE (s_sp_blackwater_with_dockworker[0].ped,sceneBlackWater5Id, "missheistdockssetup1ig_13@exit", "GUARD_Beatup_RollIdle_DockWorker", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
											SET_SYNCHRONIZED_SCENE_LOOPED(sceneBlackWater5Id,TRUE)
											//CLEAR_PED_TASKS(s_sp_blackwater_with_dockworker[1].ped)
											STOP_SYNCHRONIZED_ENTITY_ANIM(s_sp_blackwater_with_dockworker[1].ped,SLOW_BLEND_OUT,TRUE)
											STOP_SYNCHRONIZED_ENTITY_ANIM(s_sp_blackwater_with_dockworker[2].ped,SLOW_BLEND_OUT,TRUE)
											
											OPEN_SEQUENCE_TASK(seqMain)
												TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),4000)
												TASK_PAUSE(NULL,1000)
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-116.1250, -2386.3333, 5.0000>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
												TASK_WANDER_STANDARD(NULL)
											CLOSE_SEQUENCE_TASK(seqMain)
											TASK_PERFORM_SEQUENCE(s_sp_blackwater_with_dockworker[1].ped,seqMain)	
											CLEAR_SEQUENCE_TASK(seqMain)
											IF NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[1].ped)
												SET_PED_CONFIG_FLAG(s_sp_blackwater_with_dockworker[1].ped,PCF_KeepRelationshipGroupAfterCleanUp,TRUE)	
											ENDIF
											SET_PED_KEEP_TASK(s_sp_blackwater_with_dockworker[1].ped,TRUE)
											SET_PED_AS_NO_LONGER_NEEDED(s_sp_blackwater_with_dockworker[1].ped)
											
											
											
											//CLEAR_PED_TASKS(s_sp_blackwater_with_dockworker[2].ped)
											OPEN_SEQUENCE_TASK(seqMain)
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-68.8121, -2431.8848, 5.0005>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
												TASK_WANDER_STANDARD(NULL)
											CLOSE_SEQUENCE_TASK(seqMain)
											TASK_PERFORM_SEQUENCE(s_sp_blackwater_with_dockworker[2].ped,seqMain)	
											CLEAR_SEQUENCE_TASK(seqMain)
											IF NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[2].ped)
												SET_PED_CONFIG_FLAG(s_sp_blackwater_with_dockworker[2].ped,PCF_KeepRelationshipGroupAfterCleanUp,TRUE)
											ENDIF
											SET_PED_KEEP_TASK(s_sp_blackwater_with_dockworker[2].ped,TRUE)
											SET_PED_AS_NO_LONGER_NEEDED(s_sp_blackwater_with_dockworker[2].ped)
											
											
											s_sp_blackwater_with_dockworker[0].i_event ++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					BREAK
					
					CASE 6
				
						REQUEST_ANIM_DICT("missheistdockssetup1ig_13@exit")
						IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_13@exit")
							IF DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[0].ped)
							AND DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[1].ped)
							AND DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[2].ped)
								IF NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[0].ped)
								AND NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[1].ped)
								AND NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[2].ped)
									IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlackWater5Id)
										IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBlackWater5Id) > 0.99
											STOP_SYNCHRONIZED_ENTITY_ANIM(s_sp_blackwater_with_dockworker[0].ped,SLOW_BLEND_OUT,TRUE)
											CLEAR_PED_TASKS(s_sp_blackwater_with_dockworker[0].ped)
											SET_PED_AS_NO_LONGER_NEEDED(s_sp_blackwater_with_dockworker[0].ped)
											s_sp_blackwater_with_dockworker[0].i_event ++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						//
					BREAK
					
					CASE 7
						REMOVE_ANIM_DICT("missheistdockssetup1ig_13@exit")
						s_sp_blackwater_with_dockworker[0].i_event ++
						bSetPiece[SS_blackwater_with_dockworker] = FALSE
						
						IF DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[0].ped)
							SET_PED_AS_NO_LONGER_NEEDED(s_sp_blackwater_with_dockworker[0].ped)
						ENDIF
						IF DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[1].ped)
							SET_PED_AS_NO_LONGER_NEEDED(s_sp_blackwater_with_dockworker[1].ped)
						ENDIF
						IF DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[2].ped)
							SET_PED_AS_NO_LONGER_NEEDED(s_sp_blackwater_with_dockworker[2].ped)
						ENDIF
					BREAK
				ENDSWITCH
			ELSE

			ENDIF
		ENDIF
	ENDIF

ENDPROC

INT iSafetyTimer
BOOL bGetSafeTimer

//IS_ANY_ENTITY_ATTACHED_TO_FORKLIFT_FORKS
//DETACH_PALLET_FROM_FORKLIFT_FORKS.
//IS_ENTITY_ATTACHED_TO_FORKLIFT_FORKS
//PURPOSE: Trevor gets in the handler and does the tasks
PROC MISSION_STAGE_5_PROC()
	
	IF NOT IS_ENTITY_DEAD(vehHandler)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
			SET_AMBIENT_VEHICLE_RANGE_MULTIPLIER_THIS_FRAME(2.0)
			SET_AMBIENT_PED_RANGE_MULTIPLIER_THIS_FRAME(2.0)
		ENDIF
	ENDIF

	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ATTACK2)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("DH_1_HANDLER_START")
		IF IS_VEHICLE_DRIVEABLE(vehHandler)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
				IF IS_AUDIO_SCENE_ACTIVE("DH_1_FOLLOW_FLOYD_SEE_FREIGHTER")
					STOP_AUDIO_SCENE("DH_1_FOLLOW_FLOYD_SEE_FREIGHTER")
				ELSE
					START_AUDIO_SCENE("DH_1_HANDLER_START")
					SET_LABEL_AS_TRIGGERED("DH_1_HANDLER_START",TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[0].ped)
		IF NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[0].ped)
			IF VDIST(GET_ENTITY_COORDS(s_sp_blackwater_with_dockworker[0].ped),GET_ENTITY_COORDS(PLAYER_PED_ID())) > 5
				IF NOT IS_ENTITY_ON_SCREEN(s_sp_blackwater_with_dockworker[0].ped)
					DELETE_PED(s_sp_blackwater_with_dockworker[0].ped)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF s_sp_blackwater_with_dockworker[0].i_event < 6
		IF DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[1].ped)
			IF NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[1].ped)
				IF VDIST(GET_ENTITY_COORDS(s_sp_blackwater_with_dockworker[1].ped),GET_ENTITY_COORDS(PLAYER_PED_ID())) > 5
					IF NOT IS_ENTITY_ON_SCREEN(s_sp_blackwater_with_dockworker[1].ped)
						DELETE_PED(s_sp_blackwater_with_dockworker[1].ped)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(s_sp_blackwater_with_dockworker[2].ped)
			IF NOT IS_PED_INJURED(s_sp_blackwater_with_dockworker[2].ped)
				IF VDIST(GET_ENTITY_COORDS(s_sp_blackwater_with_dockworker[2].ped),GET_ENTITY_COORDS(PLAYER_PED_ID())) > 5
					IF NOT IS_ENTITY_ON_SCREEN(s_sp_blackwater_with_dockworker[2].ped)
						DELETE_PED(s_sp_blackwater_with_dockworker[2].ped)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("SET UP PHYSICS FOR 0")
		IF DOES_ENTITY_EXIST(objHandlerContainer[0])
			IF DOES_ENTITY_HAVE_PHYSICS(objHandlerContainer[0])
				SET_OBJECT_PHYSICS_PARAMS(objHandlerContainer[0],15000,-1,<<0,0,0>>,<<0,0,0>>)
				SET_LABEL_AS_TRIGGERED("SET UP PHYSICS FOR 0",TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("SET UP PHYSICS FOR 1")
		IF DOES_ENTITY_EXIST(objHandlerContainer[1])
			IF DOES_ENTITY_HAVE_PHYSICS(objHandlerContainer[1])
				SET_OBJECT_PHYSICS_PARAMS(objHandlerContainer[1],15000,-1,<<0,0,0>>,<<0,0,0>>)
				SET_LABEL_AS_TRIGGERED("SET UP PHYSICS FOR 1",TRUE)
			ENDIF
		ENDIF
	ENDIF

	MANAGE_FLOYD_ATTACHMENT()	
	CREATE_THE_DOCKS_STUFF()
	SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_CONTAINERS_FOR_HANDLER_SECTION_START,<<0,0,0>>)
	
	IF DOES_ENTITY_EXIST(objHandlerContainer[0])
	AND DOES_ENTITY_EXIST(objHandlerContainer[1])
		MANAGE_FLOYD_ACTIVITY()
	ELSE
		CREATE_THE_DOCKS_STUFF()
	ENDIF
	
	IF iProgress > 0
		IF bIsJumpingDirectlyToStage = TRUE
			IF NOT IS_PED_INJURED(pedFloyd)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneHandlerA1)
					INIT_STAGE()
					bIsJumpingDirectlyToStage = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH iProgress

	CASE 0//Create objective specific items- print objective, add blips etc
		IF SETUP_PEDS_FOR_DIALOGUE()
		
			IF IS_SCREEN_FADED_OUT()
				MANAGE_PLAYER_OUTFIT()
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehHandler)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
					SAFE_ADD_BLIP_VEHICLE(veh_blip,vehHandler,TRUE)
				ENDIF
			ENDIF
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4,"Handler Work")
			bGodText = FALSE
			iFloydHauler = 0
			iMissionDialogue = 9
			bGetSafeTimer = FALSE

			
			IF DOES_ENTITY_EXIST(vehHandler)
				IF IS_VEHICLE_DRIVEABLE(vehHandler)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehHandler,TRUE)
					FREEZE_ENTITY_POSITION(vehHandler,FALSE)
				ENDIF
			ENDIF

			IF NOT IS_PED_INJURED(pedFloyd)
				REMOVE_PED_FROM_GROUP(pedFloyd)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFloyd, TRUE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehMission)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehMission, FALSE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedDockWorker[12])
				IF NOT IS_PED_INJURED(pedDockWorker[12])
					DELETE_PED(pedDockWorker[12])
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedDockWorker[7])
				IF NOT IS_PED_INJURED(pedDockWorker[7])
					TASK_START_SCENARIO_IN_PLACE(pedDockWorker[7],"WORLD_HUMAN_AA_SMOKE")
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedDockWorker[13])
				IF NOT IS_PED_INJURED(pedDockWorker[13])
					TASK_START_SCENARIO_IN_PLACE(pedDockWorker[13],"WORLD_HUMAN_CLIPBOARD")
				ENDIF
			ENDIF
			
			SET_PED_COMBAT_ATTRIBUTES(PLAYER_PED_ID(),CA_DO_DRIVEBYS,FALSE)
			
			REQUEST_IPL("pcranecont")
			SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
			
			IF NOT IS_ENTITY_DEAD(vehHandler)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
					IF bIsJumpingDirectlyToStage = FALSE
						IF bDialogue = FALSE
							IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_07h", CONV_PRIORITY_MEDIUM)
								iHandlerSection = 0
								bDialogue = FALSE
								iProgress++
							ENDIF
						ENDIF
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("AW_HAND_TASK")
							PRINT("AW_HAND_TASK",DEFAULT_GOD_TEXT_TIME,0)
							SET_LABEL_AS_TRIGGERED("AW_HAND_TASK",TRUE)
						ENDIF
						iHandlerSection = 0
						bDialogue = FALSE
						iProgress++
					ENDIF
				ELSE
					iHandlerSection = 0
					bDialogue = FALSE
					iProgress++
				ENDIF
			ENDIF
		ENDIF

	BREAK	

	CASE 1
		
		IF NOT IS_ENTITY_DEAD(vehHandler)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
				IF IS_THIS_PRINT_BEING_DISPLAYED("AW_GETIN_HAND")
					CLEAR_PRINTS()
				ENDIF
				IF DOES_BLIP_EXIST(veh_blip)
					REMOVE_BLIP(veh_blip)
				ENDIF
				IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_07ha", CONV_PRIORITY_MEDIUM)
					REPLAY_RECORD_BACK_FOR_TIME(2.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(DH1_FORKLIFT_TIME) 	
					bToggleObjective = FALSE
					bGodText = FALSE
					bHelpText = FALSE
					bInitialGodText = FALSE
					iProgress++
				ENDIF
			ELSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF bGodText = FALSE
						PRINT_GOD_TEXT("AW_GETIN_HAND")
						bGodText = TRUE
					ENDIF
				ENDIF
				
				IF bGodText = TRUE
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_Act")
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_Act", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("DS1_Act", TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
	BREAK
	
	CASE 2
		MANAGE_HANDLER_SECTION()
		
		IF bInitialGodText = FALSE
			IF IS_VEHICLE_DRIVEABLE(vehHandler)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						PRINT_GOD_TEXT("AW_HAND_TASK")
						bInitialGodText = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		IF IS_VEHICLE_DRIVEABLE(vehHandler)
			IF bCarryingContainer = FALSE
				IF IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[0], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000)
				AND IS_ENTITY_IN_ANGLED_AREA(objHandlerContainer[1], <<-92.523666,-2461.800049,8.267201>>, <<-103.638123,-2453.989258,4.520514>>, 14.7500000) 
					IF bGetSafeTimer = FALSE
						iSafetyTimer = GET_GAME_TIMER()
						bGetSafeTimer = TRUE
					ENDIF
					IF bGetSafeTimer = TRUE
						IF HAS_LABEL_BEEN_TRIGGERED("CONTAINER LAND")
						OR MANAGE_MY_TIMER(iSafetyTimer,1000)
							IF IS_HELP_MESSAGE_BEING_DISPLAYED()
								CLEAR_HELP()
							ENDIF
							CLEAN_OBJECTIVE_BLIP_DISPLAY()
							SAFE_REMOVE_BLIP(blipHaulerLoadingArea)
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
							iStaggerTimer = GET_GAME_TIMER() 
							KILL_FACE_TO_FACE_CONVERSATION()
							iProgress++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	BREAK
	
	CASE 3
		IF MANAGE_MY_TIMER(iStaggerTimer,2000)
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_08a", CONV_PRIORITY_MEDIUM)
					IF NOT IS_PED_INJURED(pedDockWorker[14])
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,	<< -57.3390, -2448.3562, 6.2399 >>,1.0)
							TASK_ACHIEVE_HEADING(NULL,144.9673)
							TASK_STAND_STILL(NULL,2000)
							TASK_TURN_PED_TO_FACE_ENTITY(NULL,PLAYER_PED_ID())
							TASK_START_SCENARIO_IN_PLACE(NULL,"WORLD_HUMAN_AA_COFFEE",0,TRUE)
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(pedDockWorker[14], seqMain)
					ENDIF
					
					IF NOT IS_PED_INJURED(pedSecurity)
						SET_PED_AS_NO_LONGER_NEEDED(pedSecurity)
					ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					
					iProgress++
				ENDIF
			ENDIF
		ENDIF
	BREAK
	
	CASE 4
		IF PRE_STREAM_MISSION_STAGE()
			bIsJumpingDirectlyToStage = FALSE
			REMOVE_ANIM_DICT("missheistdockssetup1ig_12@idle_a")
			SAFE_REMOVE_BLIP(blipHandlerContainers[0])
			SAFE_REMOVE_BLIP(blipHandlerContainers[1])
			SAFE_REMOVE_BLIP(blipHandlerContainers[2])
			
			RESET_MISSION_STAGE_VARIABLES()
			ADVANCE_MISSION_STAGE()
		ENDIF	
	BREAK

ENDSWITCH
ENDPROC

/// PURPOSE:
/// Play the crane mocap
FUNC BOOL RUN_CRANE_CUTSCENE()

	bcutsceneplaying = TRUE

		#IF IS_DEBUG_BUILD
			IF IS_CUTSCENE_ACTIVE()
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
					STOP_CUTSCENE()
					REMOVE_CUTSCENE()
					WHILE IS_CUTSCENE_ACTIVE()
						WAIT(0)
					ENDWHILE
				ENDIF
			ENDIF
		#ENDIF
	
		IF e_section_stage = SECTION_STAGE_SETUP
			IF b_is_jumping_directly_to_stage
				CONTROL_FADE_IN(500)
				b_is_jumping_directly_to_stage = FALSE
			ELSE
				IF i_current_event = 0
					PRINTSTRING("PLAYING CRANE CUTSCNE- 1")//PRINTNL()
					bcutsceneplaying = TRUE
					REQUEST_CUTSCENE("LSDHS_MCS_1")
					REQUEST_MODEL(PROP_PENCIL_01)
					REQUEST_MODEL(P_CS_CLIPBOARD)
					i_current_event++
				ELIF i_current_event = 1
					
					IF HAS_CUTSCENE_LOADED()
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ELSE
							WAIT(0)
						ENDIF
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						SET_CUTSCENE_FADE_VALUES(FALSE,FALSE,FALSE,FALSE)
						
						//START_CUTSCENE()
						
						//Wait a couple of frames before clearing/warping stuff, so the cutscene is active first.
						WAIT(0)
						
						PRINTSTRING("RUNNING CRANE CUTSCENE - 3")//PRINTNL()
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						b_skipped_mocap = FALSE
						i_current_event = 0
						e_section_stage = SECTION_STAGE_RUNNING
					ELSE
						PRINTSTRING("CUTSCENE LOADING")//PRINTNL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_RUNNING
			IF NOT b_skipped_mocap
				IF IS_CUTSCENE_SKIP_BUTTON_PRESSED()
					b_skipped_mocap = TRUE
				ENDIF
			ENDIF
			
			PRINTSTRING("RUNNING CRANE CUTSCENE - 3")//PRINTNL()
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
					
					IF NOT IS_PED_INJURED(pedFloyd)
						SET_ENTITY_COORDS(pedFloyd,<<-114.33, -2426.18, 6.00>>)
						SET_ENTITY_HEADING(pedFloyd,-4.84)
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedDockWorker[12])
						DELETE_PED(pedDockWorker[12])
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehHandler)
						SET_ENTITY_COORDS(vehHandler,<< -166.8165, -2460.6479, 5.2973 >>)
					ENDIF
					
					IF NOT DOES_CAM_EXIST(cam_interp)
						cam_interp = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
						SET_CAM_PARAMS(cam_interp,<< -63.5991, -2390.1675, 23.4720 >>, << -20.2428, 0.0000, -154.0613 >>,45.0193)
						RENDER_SCRIPT_CAMS(TRUE,FALSE)
						SET_CAM_ACTIVE(cam_interp,TRUE)
					ENDIF

					SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)
					REMOVE_IPL("pcranecont")
					SWITCH_CRANE_VISIBILITY(TRUE)
				ENDIF
			ENDIF

			IF HAS_CUTSCENE_FINISHED()
				e_section_stage = SECTION_STAGE_CLEANUP
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF IS_CUTSCENE_ACTIVE()
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
						STOP_CUTSCENE()
						REMOVE_CUTSCENE()
						WHILE IS_CUTSCENE_ACTIVE()
							WAIT(0)
						ENDWHILE
					ENDIF
				ENDIF
			#ENDIF
			
		ENDIF

		IF e_section_stage = SECTION_STAGE_CLEANUP
			
			IF b_skipped_mocap
				//If the player skipped the mocap the peds need to be warped.
				WHILE IS_CUTSCENE_ACTIVE()
					WAIT(0)
				ENDWHILE
			ENDIF
			
			//Setup buddy relationship groups etc.
			INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			i_current_event = 0
			RETURN TRUE
			bcutsceneplaying = FALSE
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_SKIP
			CONTROL_FADE_OUT(500)
			STOP_CUTSCENE()
			b_skipped_mocap = TRUE
			e_section_stage = SECTION_STAGE_RUNNING
		ENDIF
		
		RETURN FALSE

ENDFUNC

/*
As discussed we want to wait until the get off has finished playing.

If you can query this anim
"get_off_Bottom_Front_Stand"

The get in animation should be played around phase 0.6.
That phase is quite aggressive so you might need to push it a bit further back.
Give me a shout when you are doing this so I can take a look.
*/

//PURPOSE: Trevor follows floyd to the Crane
STRING sGodText

PROC MISSION_STAGE_6_PROC()

		MANAGE_floyd_ACTIVITY()
		CREATE_THE_DOCKS_STUFF()
		MANAGE_FLOYD_ATTACHMENT()

		IF iProgress > 0
		AND iProgress < 8
			REQUEST_WAYPOINT_RECORDING("cranassist")
			IF GET_IS_WAYPOINT_RECORDING_LOADED("cranassist")
				USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("cranassist",TRUE,1,0.5)
			ENDIF
		ENDIF
		
		//Set the ped on the crane variation
		IF DOES_ENTITY_EXIST(pedDockWorker[14])
		AND NOT bPedDockCraneVariation
			PRINTLN("PED DOCK VARIATION SET!")
			SET_PED_COMPONENT_VARIATION(pedDockWorker[14], INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
			SET_PED_COMPONENT_VARIATION(pedDockWorker[14], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
			SET_PED_COMPONENT_VARIATION(pedDockWorker[14], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
			SET_PED_COMPONENT_VARIATION(pedDockWorker[14], INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
			bPedDockCraneVariation = TRUE
		ENDIF
		
		
		IF iProgress < 5
			IF NOT HAS_LABEL_BEEN_TRIGGERED("GOT TO CABIN EARLY")
				IF NOT IS_PED_CLIMBING(PLAYER_PED_ID())
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-51.946228,-2414.439209,20.533628>>, <<-51.964249,-2413.213867,22.335625>>, 1.750000)
						iProgress = 5
						SET_LABEL_AS_TRIGGERED("GOT TO CABIN EARLY",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		SWITCH iProgress

		CASE 0//Create objective specific items- print objective, add blips etc
			
			IF SETUP_PEDS_FOR_DIALOGUE()
			
				s_crane.f_crane_offset = 0.297
				s_crane.f_crane_vel = 0.0
				vSpreaderPosition = <<-53.02, -2416.04, 14.75>>
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_CRANE)
					WAIT(0)
					REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ThePortOfLSHeistSetup")
				ENDWHILE
				
				IF bIsJumpingDirectlyToStage = TRUE
					IF NOT IS_PED_INJURED(pedFloyd)
						IF IS_VEHICLE_DRIVEABLE(vehHandler)
							sceneHandlerA1 = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
							TASK_SYNCHRONIZED_SCENE (pedFloyd, sceneHandlerA1, "missheistdockssetup1ig_14", "floyd_lets_get_going", NORMAL_BLEND_IN, NORMAL_BLEND_OUT,  SYNCED_SCENE_USE_PHYSICS,RBF_PLAYER_IMPACT )
							ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneHandlerA1,vehHandler,-1)
							SET_ENTITY_NO_COLLISION_ENTITY(pedFloyd,vehHandler,TRUE)
							iFloydHauler = 4
						ENDIF
					ENDIF
				ENDIF
				
				SWITCH_CRANE_VISIBILITY(TRUE)
				iMissionDialogue = 11
				
				IF NOT IS_PED_INJURED(pedFloyd)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFloyd, TRUE)
					//CLEAR_PED_TASKS(pedFloyd)
				ENDIF
				
				REMOVE_CUTSCENE()
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5,"Follow Floyd to crane")
				IF IS_SCREEN_FADED_OUT()
					WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_CRANE_CONTAINERS)
						WAIT(0)
						REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ThePortOfLSHeistSetup")
					ENDWHILE
					MANAGE_PLAYER_OUTFIT()
				ENDIF
				
				REMOVE_IPL("pcranecont")
				SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
				
				IF DOES_ENTITY_EXIST(pedDockWorker[12])
					DELETE_PED(pedDockWorker[12])
				ENDIF

				SET_PED_COMBAT_ATTRIBUTES(PLAYER_PED_ID(),CA_DO_DRIVEBYS,FALSE)
				
				sGodText = "AW_CLIMB_CRA"
				iProgress++

			ENDIF
		BREAK
		
		CASE 1
			INIT_STAGE()
			iProgress++
		BREAK

		CASE 2

			IF IS_PLAYER_AT_LOCATION_ANY_MEANS(sLocatesData,<<-49.8470, -2404.4475, 5.0006>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,TRUE,"AW_DRIVE_TO_CRA")
				
			ELSE
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					SET_BLIP_ROUTE(sLocatesData.LocationBlip,FALSE)
				ENDIF
				
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
					CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
					iProgress = 3
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-36.008926,-2398.947754,5.000000>>, <<-65.415092,-2398.950684,15.036324>>, 35.000000)
					IF IS_VEHICLE_DRIVEABLE(vehHandler)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
							SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
							REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
							iProgress++
						ELSE	
							REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
							CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
							iProgress = 3
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 3
			IF IS_VEHICLE_DRIVEABLE(vehHandler)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
					IF NOT IS_PED_INJURED(pedFloyd)
						IF iFloydHauler = 5
							IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneHandlerA1)
								IF GET_SYNCHRONIZED_SCENE_PHASE(sceneHandlerA1) > 0.6
									IF NOT IS_PED_INJURED(pedFloyd)
										IF IS_VEHICLE_DRIVEABLE(vehHandler)
											TASK_LEAVE_VEHICLE(PLAYER_PED_ID(),vehHandler)
											SET_ENTITY_NO_COLLISION_ENTITY(pedFloyd,vehHandler,TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehHandler,FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					iProgress++
				ENDIF
			ENDIF
		BREAK
	
		
		CASE 4
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DH_1_CLIMB_UP_CRANE")
				IF IS_VEHICLE_DRIVEABLE(vehHandler)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
						IF IS_AUDIO_SCENE_ACTIVE("DH_1_HANDLER_START")
							STOP_AUDIO_SCENE("DH_1_HANDLER_START")
						ELSE
							START_AUDIO_SCENE("DH_1_CLIMB_UP_CRANE")
							SET_LABEL_AS_TRIGGERED("DH_1_CLIMB_UP_CRANE",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehHandler)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
					sGodText = ""
					iProgress--
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(pedFloyd)
				IF iFloydHauler = 5
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneHandlerA1)
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneHandlerA1) > 0.6
							IF NOT IS_PED_INJURED(pedFloyd)
								IF IS_VEHICLE_DRIVEABLE(vehHandler)
									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
										TASK_LEAVE_VEHICLE(PLAYER_PED_ID(),vehHandler)
									ENDIF
									SET_ENTITY_NO_COLLISION_ENTITY(pedFloyd,vehHandler,TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
				IF IS_PLAYER_AT_LOCATION_ON_FOOT(sLocatesData,<<-49.8470, -2404.4475, 5.0006>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,TRUE,sGodText)
					iProgress++
				ELSE
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-48.895756,-2405.313965,4.250962>>, <<-50.765312,-2405.310059,7.750961>>, 2.500000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-48.721390,-2405.790039,7.760520>>, <<-50.121132,-2405.803711,9.510519>>, 1.000000)
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						iProgress++
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
	
		//I've been waiting on a crane driver for over an hour	
		CASE 5

			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-53.425056,-2407.837402,14.789022>>, <<-49.817219,-2407.882324,17.539022>>, 1.250000)
				IF NOT IS_PED_CLIMBING(PLAYER_PED_ID())
					SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1.0)
				ENDIF
			ELSE
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),2.0)
			ENDIF
						
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_MCS1_P2")
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-51.256855,-2406.551514,18.558769>>, <<-50.185169,-2406.576416,20.952829>>, 1.500000)
					IF CREATE_CONVERSATION(sSpeech,"D1AUD","DS1_MCS1_P2", CONV_PRIORITY_HIGH)
						TASK_LOOK_AT_ENTITY(pedDockWorker[14],PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV,SLF_LOOKAT_HIGH)
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
						SET_LABEL_AS_TRIGGERED("DS1_MCS1_P2",TRUE)
					ENDIF
				ENDIF
			ELSE	
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF NOT IS_PED_CLIMBING(PLAYER_PED_ID())
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_getin")
								IF CREATE_CONVERSATION(sSpeech,"D1AUD","DS1_getin", CONV_PRIORITY_HIGH)
									SET_LABEL_AS_TRIGGERED("DS1_getin",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				SET_GPS_ACTIVE(FALSE)
			ENDIF
			
			IF IS_PLAYER_AT_LOCATION_ON_FOOT(sLocatesData,<<-52.2530, -2413.9524, 20.5856>>,<<0.100000,0.100000,LOCATE_SIZE_HEIGHT>>,TRUE,"")
			
			ELSE
				IF NOT IS_PED_CLIMBING(PLAYER_PED_ID())
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-51.946228,-2414.439209,20.533628>>, <<-51.964249,-2413.213867,22.335625>>, 1.750000)
						IF IS_VEHICLE_DRIVEABLE(vehHandler)
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehHandler)
								SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
							ENDIF
						ENDIF
						IF DOES_BLIP_EXIST(dest_blip)
							REMOVE_BLIP(dest_blip)
						ENDIF
						REQUEST_ANIM_DICT("missheistdockssetup1trevor_crane")
						REMOVE_BLIP(sLocatesData.LocationBlip)
						iProgress++
					ENDIF
				ENDIF
			ENDIF

		BREAK
		
		//P_Dock_RTG_LD_Cab
		//s_crane.obj_cabin
		//missheistdockssetup1ig
		
		CASE 6
			REQUEST_ANIM_DICT("missheistdockssetup1trevor_crane")
			IF HAS_ANIM_DICT_LOADED("missheistdockssetup1trevor_crane")
				IF NOT DOES_CAM_EXIST(camMain)
					camMain = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
				ENDIF
				
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("Container_Lifter")
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("Creak_V1")
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				PLAY_CAM_ANIM(camMain,  "get_in_cam", "missheistdockssetup1trevor_crane",<< -53.139, -2415.768, 22.604 >>,<< 0.000, 0.000, -178.200 >>)
				SET_CAM_ACTIVE(camMain,TRUE)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				
				ssCraneAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.100, -0.100, -0.350 >>,<< 0.000, 0.000, 0.000 >>)
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssCraneAnims,s_crane.obj_cabin,-1)
				TASK_SYNCHRONIZED_SCENE (PLAYER_PED_ID(), ssCraneAnims, "missheistdockssetup1trevor_crane", "get_in", INSTANT_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_NONE,RBF_NONE )
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ssCraneAnims,TRUE)
				
				
				iProgress++
			ENDIF
		BREAK
		
		CASE 7
			DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
			
			SETUP_MISSION_REQUIREMENT(REQ_CRANE_CONTAINERS)
			REQUEST_ANIM_DICT("missheistdockssetup1trevor_crane")
			IF HAS_ANIM_DICT_LOADED("missheistdockssetup1trevor_crane")
				IF IS_SYNCHRONIZED_SCENE_RUNNING(ssCraneAnims)
					IF GET_SYNCHRONIZED_SCENE_PHASE(ssCraneAnims) = 1
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
						ssCraneAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.100, -0.100, -0.350 >>,<< 0.000, 0.000, 0.000 >>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssCraneAnims,s_crane.obj_cabin,-1)
						TASK_SYNCHRONIZED_SCENE (PLAYER_PED_ID(), ssCraneAnims, "missheistdockssetup1trevor_crane", "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT )
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ssCraneAnims,FALSE)
						SET_SYNCHRONIZED_SCENE_LOOPED(ssCraneAnims,TRUE)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF NOT IS_PED_CLIMBING(PLAYER_PED_ID())
									IF CREATE_CONVERSATION(sSpeech,"D1AUD","DS1_climbin", CONV_PRIORITY_HIGH)
										SET_LABEL_AS_TRIGGERED("DS1_climbin",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						iProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			
			IF IS_VEHICLE_DRIVEABLE(vehHandler)
				SET_ENTITY_COORDS(vehHandler,<< -166.8165, -2460.6479, 5.2973 >>)
				DELETE_VEHICLE(vehHandler)
				SET_MODEL_AS_NO_LONGER_NEEDED(HANDLER)
			ENDIF
			REMOVE_WAYPOINT_RECORDING("cranassist")
			
			iProgress++
		BREAK
		
		
		CASE 9
			
			IF PRE_STREAM_MISSION_STAGE()
				RESET_MISSION_STAGE_VARIABLES()
				ADVANCE_MISSION_STAGE()
			ENDIF
		BREAK

	ENDSWITCH
ENDPROC

PROC CUTSCENE_FINISHED_CRANE()
	
	SETTIMERA(0)
	bcutsceneplaying = TRUE
	HIDE_HUD_AND_RADAR_THIS_FRAME()

	WHILE bcutsceneplaying  //holds the script inside here until cutscene is complete

		WAIT(0)
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		
		//CONTROL_SPEECH()
		
//		#IF IS_DEBUG_BUILD // allows j/p skip to work even in cutscene
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
//				bcutsceneplaying = FALSE
//			ENDIF
//		#ENDIF
		
//		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY() AND icutsceneprog >1 // controls the cutscene skip
//			SETTIMERB(6001)
//			CONTROL_FADE_OUT(500)
//			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//				SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
//				IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
//					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
//				ENDIF
//			ENDIF
//			icutsceneprog = 3
//		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_10j")
			IF CREATE_CONVERSATION(sSpeech,"D1AUD","DS1_10j", CONV_PRIORITY_HIGH)
				SET_LABEL_AS_TRIGGERED("DS1_10j",TRUE)
			ENDIF
		ENDIF
		
		//PRINTSTRING("icutsceneprog:")PRINTINT(icutsceneprog)PRINTNL()
		
		SWITCH icutsceneprog
			
			CASE 0
				REQUEST_ANIM_DICT("missheistdockssetup1trevor_crane")
				IF HAS_ANIM_DICT_LOADED("missheistdockssetup1trevor_crane")
					CLEAR_TIMECYCLE_MODIFIER()
					bContainerSetUpForDrive = FALSE
					//CREATE_CRANE()
					IF NOT IS_PED_INJURED(pedDockWorker[13])
						CLEAR_ALL_PED_PROPS(pedDockWorker[13]) 
					ENDIF
					
					INT i
					
					REPEAT COUNT_OF(s_containers) i
						IF DOES_ENTITY_EXIST( s_containers[i].obj_main )
						AND NOT IS_ENTITY_DEAD( s_containers[i].obj_main )
							IF IS_ENTITY_ATTACHED( s_containers[i].obj_main )
								DETACH_ENTITY( s_containers[i].obj_main )
								SET_ENTITY_COORDS(s_containers[i].obj_main, <<-110.211067, -2416.435303, 6.472473>>)
								SET_ENTITY_ROTATION(s_containers[i].obj_main, <<-0.051485, 0.000001, 91.338051>>)
								IF DOES_ENTITY_EXIST(vehTrailer2)
								AND NOT IS_ENTITY_DEAD(vehTrailer2)
									ATTACH_ENTITY_TO_ENTITY(s_containers[i].obj_main,vehTrailer2,0,GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehTrailer2, <<-110.211067, -2416.435303, 6.472473>>),<<0,0,0>>,TRUE,FALSE,TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
					
					//Player Control
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					CLEAR_PRINTS()
					CLEAR_HELP(TRUE)
					DISABLE_CELLPHONE(FALSE)
					DESTROY_ALL_CAMS()
					
					RENDER_SCRIPT_CAMS(FALSE,FALSE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
					s_crane.f_crane_offset = 0.297
					s_crane.f_crane_vel = 0.0
					
					bInitStage = TRUE
					INIT_STAGE()
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					KILL_ANY_CONVERSATION()
					REMOVE_PED_FOR_DIALOGUE(sSpeech,8)
					ADD_PED_FOR_DIALOGUE(sSpeech,8,pedDockWorker[13],"SIGNALMAN")
					
					IF DOES_ENTITY_EXIST(pedDockWorker[14])
						DELETE_PED(pedDockWorker[14])
					ENDIF
					
					IF NOT DOES_CAM_EXIST(camMain)
						camMain = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
					ENDIF
					
					SET_ENTITY_COORDS_NO_OFFSET(s_crane.obj_main, <<-110.2902, -2415.7900, 5.0000>>)
					
					PLAY_CAM_ANIM(camMain,  "get_out_cam", "missheistdockssetup1trevor_crane",<< -108.078, -2413.115, 20.586 >>,<< 0.000, 0.000, 180.000 >>)
					SET_CAM_ACTIVE(camMain,TRUE)
					RENDER_SCRIPT_CAMS(TRUE,FALSE)
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)

					ssCraneAnims = CREATE_SYNCHRONIZED_SCENE(<< -0.100, -0.10, -0.350 >>,<< 0.000, 0.000, 0.000 >>)
					SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ssCraneAnims,TRUE)
					SET_SYNCHRONIZED_SCENE_LOOPED(ssCraneAnims,FALSE)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(ssCraneAnims,s_crane.obj_cabin,-1)
					TASK_SYNCHRONIZED_SCENE (PLAYER_PED_ID(), ssCraneAnims, "missheistdockssetup1trevor_crane", "get_out", NORMAL_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
					
					SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					icutsceneprog ++
				ENDIF
			BREAK
			
			CASE 1 
				IF HAS_ANIM_DICT_LOADED("misslsdhs")
					IF NOT IS_PED_INJURED(pedDockWorker[13])
						OPEN_SEQUENCE_TASK(seqMain)
							TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),-1)
							TASK_PLAY_ANIM(NULL,"misslsdhs", "reaction_to_truck",NORMAL_BLEND_IN,SLOW_BLEND_OUT,-1,AF_DEFAULT | AF_USE_KINEMATIC_PHYSICS)
							TASK_PLAY_ANIM(NULL,"misslsdhs", "wave_truck",NORMAL_BLEND_IN,SLOW_BLEND_OUT,-1,AF_DEFAULT | AF_USE_KINEMATIC_PHYSICS)
							TASK_START_SCENARIO_IN_PLACE(NULL,"WORLD_HUMAN_CLIPBOARD")
						CLOSE_SEQUENCE_TASK(seqMain)
						TASK_PERFORM_SEQUENCE(pedDockWorker[13], seqMain)
					ENDIF

					SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
					IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
					ENDIF
					
					SET_ENTITY_COLLISION(s_crane.obj_cabin,TRUE)
					SET_FOLLOW_PED_CAM_VIEW_MODE(cCam)
					icutsceneprog ++
				ENDIF
			BREAK
			
			CASE 2
				IF IS_SYNCHRONIZED_SCENE_RUNNING(ssCraneAnims)
					IF GET_SYNCHRONIZED_SCENE_PHASE(ssCraneAnims) > 0.95
						//STOP_SYNCHRONIZED_ENTITY_ANIM(PLAYER_PED_ID(),NORMAL_BLEND_OUT,TRUE)
						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
							TASK_TURN_PED_TO_FACE_COORD(PLAYER_PED_ID(), <<-105.3195, -2603.9893, 5.0007>>)
						ELSE
							CLEAR_PED_TASKS(PLAYER_PED_ID())
						ENDIF
						//TASK_ACHIEVE_HEADING(PLAYER_PED_ID(),179.9661)
						SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
						SET_WIDESCREEN_BORDERS(FALSE,0)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(-90.000)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH()
						CONTROL_FADE_IN(500)
						CLEAR_PRINTS()
						
						RENDER_SCRIPT_CAMS(FALSE, TRUE,1000)
//							IF DOES_CAM_EXIST(camMain)
//								DESTROY_CAM(camMain)
//							ENDIF

						SET_WIDESCREEN_BORDERS(FALSE,0)
						//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
						SET_TIME_SCALE(1.0)
						DISPLAY_RADAR(TRUE)

						REMOVE_ANIM_DICT("misslsdhs")
									
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
						DISPLAY_HUD(TRUE)
						DISABLE_CELLPHONE(FALSE)

						CLEAR_HELP()
						KILL_ANY_CONVERSATION()
						
						REPLAY_STOP_EVENT()
						
						HIDE_ACTIVE_PHONE(FALSE)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
						icutsceneprog = 0
						brunfailchecks = FALSE
						bcutsceneplaying = FALSE
						//ENDIF
					ENDIF
				ENDIF
			BREAK

		ENDSWITCH
		
	ENDWHILE

ENDPROC


//PURPOSE: Trevor uses the crane
PROC MISSION_STAGE_7_PROC()

	IF NOT HAS_LABEL_BEEN_TRIGGERED("DH_1_USE_CRANE")
		IF IS_AUDIO_SCENE_ACTIVE("DH_1_CLIMB_UP_CRANE")
			STOP_AUDIO_SCENE("DH_1_CLIMB_UP_CRANE")
		ELSE
			START_AUDIO_SCENE("DH_1_USE_CRANE")
			SET_LABEL_AS_TRIGGERED("DH_1_USE_CRANE",TRUE)
		ENDIF
	ENDIF
	
	SET_MAP_CRANE_VISIBLE(FALSE)
	CREATE_THE_DOCKS_STUFF()
	
	REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2170341
	
	SWITCH iProgress
		
		CASE 0
			
			SETUP_PC_CONTROLS()

			cCam = GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT)
			IF bIsJumpingDirectlyToStage
				s_crane.f_crane_offset = 0.297
				s_crane.f_crane_vel = 0.0
				v_crane_pos = << -53.6982, -2415.7900, 5.0500 >> //<< -53.267, -2415.863, 5.066 >>
				vSpreaderPosition = <<-53.02, -2416.04, 14.75>>
				SET_LABEL_AS_TRIGGERED("DS1_10",FALSE)
				
				IF IS_VEHICLE_DRIVEABLE(vehHandler)
					SET_ENTITY_COORDS(vehHandler,<< -166.8165, -2460.6479, 5.2973 >>)
					DELETE_VEHICLE(vehHandler)
					SET_MODEL_AS_NO_LONGER_NEEDED(HANDLER)
				ENDIF
				
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_CRANE)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_HAULER_WITH_TRAILER)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_CRANE_CONTAINERS)
					WAIT(0)
					REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ThePortOfLSHeistSetup")
				ENDWHILE
				
				IF IS_VEHICLE_DRIVEABLE(vehFirstHauler)
				AND IS_VEHICLE_DRIVEABLE(vehTrailer1)
					ATTACH_VEHICLE_TO_TRAILER(vehFirstHauler,vehTrailer1)
					FREEZE_ENTITY_POSITION(vehFirstHauler,TRUE)
					FREEZE_ENTITY_POSITION(vehTrailer1,TRUE)
					SET_VEHICLE_EXTRA(vehTrailer1,1,TRUE)					
					SET_VEHICLE_STRONG(vehTrailer1, TRUE)
					SET_VEHICLE_HAS_STRONG_AXLES(vehTrailer1, TRUE)					
				ENDIF

				IF IS_SCREEN_FADED_OUT()
					MANAGE_PLAYER_OUTFIT()
				ENDIF
				
				IF SETUP_PEDS_FOR_DIALOGUE()
					IF NOT IS_PED_INJURED(pedFloyd)
						SET_ENTITY_COORDS(pedFloyd,<<-114.33, -2426.18, 6.00>>)
						SET_ENTITY_HEADING(pedFloyd,-4.84)
					ENDIF
					
					Blip_player = GET_MAIN_PLAYER_BLIP_ID()
					IF DOES_BLIP_EXIST(Blip_player)
						SET_BLIP_ALPHA(Blip_player,0)
					ENDIF
					
					IF DOES_ENTITY_EXIST(pedDockWorker[12])
						DELETE_PED(pedDockWorker[12])
					ENDIF
					
					g_bDocksBackGatesIgnoreTrevorsUniform = FALSE
					iMissionDialogue = 12
					iCraneHelp = 0
					
					REMOVE_IPL("pcranecont")
					SWITCH_CRANE_VISIBILITY(TRUE)
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6,"Use Crane")
					SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
					SET_USER_RADIO_CONTROL_ENABLED(FALSE)
					CASCADE_SHADOWS_ENABLE_FREEZER( false)
					iProgress++
				ENDIF
			ELSE
				SET_LABEL_AS_TRIGGERED("DS1_10",TRUE)
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6,"Use Crane")
				SETUP_PEDS_FOR_DIALOGUE()
				
				RENDER_SCRIPT_CAMS(FALSE,FALSE)
				DESTROY_CAM(camMain)
				
				MANAGE_CRANE(TRUE)
				bCreateFirstHauler = FALSE
				bTrailerAttachements = FALSE
				iCraneHelp = 0
				SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
				SET_USER_RADIO_CONTROL_ENABLED(FALSE)
				CASCADE_SHADOWS_ENABLE_FREEZER( false)
				iProgress++
			ENDIF
		BREAK
	
		CASE 1//Create objective specific items- print objective, add blips etc
		
			IF NOT bIsJumpingDirectlyToStage
				MANAGE_CRANE(TRUE)
				IF INITIALISE_CRANE()
					//MANAGE_CRANE(TRUE)
					IF NOT IS_PED_INJURED(pedDockWorker[13])
						SET_ENTITY_COORDS(pedDockWorker[13],<<-116.91, -2401.23, 6.00>>)
						SET_ENTITY_HEADING(pedDockWorker[13],163.87)
					ENDIF
					
					IF NOT IS_PED_INJURED(s_sp_forklift_supervisorA[0].ped)
						IF DOES_ENTITY_EXIST(objPencil[1])
							DELETE_OBJECT(objPencil[1])
						ENDIF
						IF DOES_ENTITY_EXIST(objClipboard[1])
							DELETE_OBJECT(objClipboard[1])
						ENDIF
						TASK_WANDER_IN_AREA(s_sp_forklift_supervisorA[0].ped,<<-149.3773, -2482.7092, 5.0189>>,15)
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehHandler)
						SET_ENTITY_COORDS(vehHandler,<< -166.8165, -2460.6479, 5.2973 >>)
						DELETE_VEHICLE(vehHandler)
						SET_MODEL_AS_NO_LONGER_NEEDED(HANDLER)
					ELSE
						DELETE_VEHICLE(vehHandler)
						SET_MODEL_AS_NO_LONGER_NEEDED(HANDLER)
					ENDIF
					
					PRINTSTRING("PASSING THROUGH HERE - 2")//PRINTNL()
					iProgress++
				ENDIF
			ELSE
				IF INITIALISE_CRANE()
					IF IS_VEHICLE_DRIVEABLE(vehFirstHauler) AND IS_VEHICLE_DRIVEABLE(vehTrailer1)
						IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(vehFirstHauler)
							SET_VEHICLE_EXTRA(vehTrailer1,1,TRUE)
							ATTACH_VEHICLE_TO_TRAILER(vehFirstHauler,vehTrailer1)
							SET_ENTITY_PROOFS(vehTrailer1,FALSE,FALSE,FALSE,TRUE,FALSE)
						ENDIF

						IF NOT IS_PED_INJURED(pedDockWorker[11]) AND IS_VEHICLE_DRIVEABLE(vehDocks[0])
							IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehDocks[0])
								IF IS_PED_IN_VEHICLE(pedDockWorker[11],vehDocks[0])
									START_PLAYBACK_RECORDED_VEHICLE(vehDocks[0],1,"AWDock1")
									SET_PLAYBACK_SPEED(vehDocks[0],0.8)
								ENDIF
							ENDIF
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehFirstHauler) AND DOES_ENTITY_EXIST(vehTrailer1)
							IF NOT IS_PED_INJURED(pedDockWorker[13])
								SET_ENTITY_COORDS(pedDockWorker[13],<<-116.91, -2401.23, 6.00>>)
								SET_ENTITY_HEADING(pedDockWorker[13],163.87)
							ENDIF
							
							FREEZE_ENTITY_POSITION(vehFirstHauler,TRUE)
							FREEZE_ENTITY_POSITION(vehTrailer1,TRUE)
							iProgress++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
				
		BREAK

		CASE 2
			MANAGE_CRANE(TRUE)
			IF bIsJumpingDirectlyToStage = FALSE
				PRINTSTRING("PASSING THROUGH HERE - 2")//PRINTNL()
			ENDIF

			IF DOES_ENTITY_EXIST(s_crane.obj_main)
				bIsJumpingDirectlyToStage = FALSE
				REQUEST_ANIM_DICT("misslsdhs")
				s_crane.b_container_attached = FALSE
				INIT_STAGE()
				IF DOES_CAM_EXIST(cam_interp)
					PRINTSTRING("INTERPING")//PRINTNL()
					SET_CAM_ACTIVE_WITH_INTERP(cam_cutscene,cam_interp,2000)
				ENDIF
				SET_AUDIO_FLAG("MobileRadioInGame", TRUE)
				SET_RADIO_TO_STATION_NAME("RADIO_04_PUNK")
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(DH1_CRANE_TIME)
				HANG_UP_AND_PUT_AWAY_PHONE()
				CASCADE_SHADOWS_SET_CASCADE_BOUNDS_SCALE(0.2)
				iProgress++
			ENDIF
		BREAK

		CASE 3
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL) 
			IF NOT bIsJumpingDirectlyToStage
				IF NOT bCreateFirstHauler
					IF SETUP_MISSION_REQUIREMENT(REQ_HAULER_WITH_TRAILER)
						bCreateFirstHauler = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT bTrailerAttachements
				IF IS_VEHICLE_DRIVEABLE(vehFirstHauler)
					IF IS_VEHICLE_DRIVEABLE(vehTrailer1)
						IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(vehFirstHauler)
							ATTACH_VEHICLE_TO_TRAILER(vehFirstHauler,vehTrailer1)
							FREEZE_ENTITY_POSITION(vehFirstHauler,TRUE)
							FREEZE_ENTITY_POSITION(vehTrailer1,TRUE)
							SET_VEHICLE_EXTRA(vehTrailer1,1,TRUE)
							bTrailerAttachements = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
			ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)

			//SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
			MANAGE_CRANE() 
			MANAGE_CRANE_HELP()
			IF bCraneSectionComplete
				IF NOT IS_PED_INJURED(pedDockWorker[13])
					IF HAS_LABEL_BEEN_TRIGGERED("CLAMP SOUND")
						PRINTSTRING("STOPPEDSOUND")PRINTNL()
						STOP_SOUND(iCraneSound)
						SET_LABEL_AS_TRIGGERED("CLAMP SOUND",FALSE)
					ENDIF
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
					REMOVE_PED_FOR_DIALOGUE(sSpeech,8)
					ADD_PED_FOR_DIALOGUE(sSpeech,8,pedDockWorker[13],"SIGNALMAN")
					iProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			CUTSCENE_FINISHED_CRANE()
			IF b_is_audio_scene_active
				STOP_AUDIO_SCENE("CAR_THEFT_EXPORT_CARS_CRANE_SECTION_SCENE")
				b_is_audio_scene_active = FALSE
			ENDIF
			CASCADE_SHADOWS_INIT_SESSION()
			CASCADE_SHADOWS_ENABLE_FREEZER(TRUE)
			CASCADE_SHADOWS_INIT_SESSION()
			
			CLEANUP_PC_CONTROLS()
				
			iProgress++
		BREAK
		
		CASE 5
			Blip_player = GET_MAIN_PLAYER_BLIP_ID()
			IF DOES_BLIP_EXIST(Blip_player)
				SET_BLIP_ALPHA(Blip_player,100)
			ENDIF
			
			SAFE_REMOVE_BLIP(blipCrane)
			SAFE_REMOVE_BLIP(blip_current_destination)
			STOP_GAMEPLAY_HINT()
			
			IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
				FREEZE_ENTITY_POSITION(vehFloydTruck,TRUE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehFloydTruck,FALSE)
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehTrailer2)
				FREEZE_ENTITY_POSITION(vehTrailer2,TRUE)
			ENDIF

			IF DOES_BLIP_EXIST(dest_blip)
				REMOVE_BLIP(dest_blip)
			ENDIF
			IF DOES_BLIP_EXIST(container0_blip)
				REMOVE_BLIP(container0_blip)
			ENDIF
			IF DOES_BLIP_EXIST(container1_blip)
				REMOVE_BLIP(container1_blip)
			ENDIF
					
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			REMOVE_ANIM_DICT("missheistdockssetup1trevor_crane")
			SETTIMERA(0)
			SET_AUDIO_FLAG("MobileRadioInGame", FALSE)
			SET_USER_RADIO_CONTROL_ENABLED(TRUE)
			iProgress++
		BREAK
		
		CASE 6
			IF PRE_STREAM_MISSION_STAGE()
				REMOVE_ANIM_DICT("map_objects")
				CLEAN_OBJECTIVE_BLIP_DISPLAY()
				RESET_MISSION_STAGE_VARIABLES()
				ADVANCE_MISSION_STAGE()
			ENDIF	
		BREAK

	ENDSWITCH
ENDPROC

FUNC BOOL IS_ENTITY_IN_CAMERA_PHONE_FRAME(PED_INDEX PedIndex, FLOAT fThisXTolerance = 0.23, FLOAT fThisYTolerance = 0.47)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(PedIndex)
			IF NOT IS_ENTITY_DEAD(PedIndex)
				IF IS_ENTITY_ON_SCREEN(PedIndex)
					IF IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(PedIndex), 0.001)
						FLOAT fXPosition, fYPosition
					
						GET_SCREEN_COORD_FROM_WORLD_COORD(GET_ENTITY_COORDS(PedIndex), fXPosition, fYPosition)
															
						IF 	(fXPosition > 0.5 - fThisXTolerance) AND (fXPosition < 0.5 + fThisXTolerance)
						AND (fYPosition > 0.5 - fThisYTolerance) AND (fYPosition < 0.5 + fThisYTolerance)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

BOOL bTakePhotoStat
BOOL bPerfectSurveillance = TRUE
BOOL bAtNeedToFreeze = FALSE
INT iTextMessageDelay
//camera section
PROC MISSION_STAGE_8_PROC()
	
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-108.585716,-2429.462158,26.250675>>, <<-108.808174,-2400.434082,16.000675>>, 13.000000)
			SET_PED_CAPSULE(PLAYER_PED_ID(),0.25)
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("DH_1_TAKE_PHOTOS")
			IF IS_AUDIO_SCENE_ACTIVE("DH_1_USE_CRANE")
				STOP_AUDIO_SCENE("DH_1_USE_CRANE")
			ELSE
				START_AUDIO_SCENE("DH_1_TAKE_PHOTOS")
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				SET_LABEL_AS_TRIGGERED("DH_1_TAKE_PHOTOS",TRUE)
			ENDIF
		ENDIF
		
		IF iProgress > 0
			IF NOT bTakePhotoStat
				IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC() 
					INFORM_MISSION_STATS_OF_INCREMENT(DH1_PHOTOS_TAKEN)
					bTakePhotoStat = TRUE
				ENDIF
			ELSE
				IF NOT HAS_CELLPHONE_CAM_JUST_TAKEN_PIC() 
					bTakePhotoStat = FALSE
				ENDIF
			ENDIF
		ENDIF

		SET_MAP_CRANE_VISIBLE(FALSE)
		
		IF iProgress > 2 AND iProgress != 66
			MANAGE_PHONE_HELP()
			//MANAGE_HELP_TEXT_UPDATE()
		ENDIF
		
		IF bAtNeedToFreeze = TRUE
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0)
		ELSE
			IF NOT IS_ENTITY_IN_WATER(PLAYER_PED_ID())
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1)
			ENDIF
		ENDIF

		IF iProgress > 2
			IF NOT HAS_LABEL_BEEN_TRIGGERED("START SKIP FADE")
				IF bIsJumpingDirectlyToStage = TRUE
					IF NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())
						iJumpTimer = GET_GAME_TIMER()
						SET_LABEL_AS_TRIGGERED("START SKIP FADE",TRUE)
					ENDIF
				ENDIF
			ELSE
				IF IS_SCREEN_FADED_OUT()
				OR NOT IS_SCREEN_FADING_IN()
					IF MANAGE_MY_TIMER(iJumpTimer,800)
						INIT_STAGE()
						bIsJumpingDirectlyToStage = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		SWITCH iProgress
		
		CASE 0
			
			
			
			ASSISTED_MOVEMENT_REQUEST_ROUTE("pols_1")
			ASSISTED_MOVEMENT_REQUEST_ROUTE("pols_2")
			ASSISTED_MOVEMENT_REQUEST_ROUTE("pols_3")
			ASSISTED_MOVEMENT_REQUEST_ROUTE("pols_4")
			ASSISTED_MOVEMENT_REQUEST_ROUTE("pols_5")
			ASSISTED_MOVEMENT_REQUEST_ROUTE("pols_6")
			ASSISTED_MOVEMENT_REQUEST_ROUTE("pols_7")
			ASSISTED_MOVEMENT_REQUEST_ROUTE("pols_8")
			
			bAtNeedToFreeze = FALSE
			bPerfectSurveillance = TRUE
			
			REPOSITION_LANDSCAPE_PHONE_FOR_LONG_SUBTITLES (TRUE)
			bTakePhotoStat = FALSE
			v_crane_pos = <<-110.2902, -2415.7900, 5.0500>>
			vSpreaderPosition = <<-109.87, -2415.71, 14.67>>
			IF bIsJumpingDirectlyToStage = TRUE
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
				IF SETUP_MISSION_REQUIREMENT(REQ_CRANE)
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7,"Photograph Boat")
					SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
					iProgress++
				ENDIF
			ELSE
				g_bDocksBackGatesIgnoreTrevorsUniform = FALSE
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7,"Photograph Boat")
				SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
				iProgress++
			ENDIF
		BREAK
	
		CASE 1 //Create objective specific items- print objective, add blips etc
			v_crane_pos = <<-110.2902, -2415.7900, 5.0500>>
			
			IF bIsJumpingDirectlyToStage = TRUE
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
			ENDIF
			
			IF IS_RADAR_HIDDEN()
				DISPLAY_RADAR(TRUE)
			ENDIF
			
			IF IS_SCREEN_FADED_OUT()
				MANAGE_PLAYER_OUTFIT()
			ENDIF
		
			CLEAR_CONTACT_PICTURE_MESSAGE(CHAR_RON)

			IF INITIALISE_CRANE(FALSE)
				IF IS_VEHICLE_DRIVEABLE(vehFloydTruck) AND IS_VEHICLE_DRIVEABLE(vehTrailer2) 
					IF SETUP_PEDS_FOR_DIALOGUE()
						ATTACH_VEHICLE_TO_TRAILER(vehFloydTruck,vehTrailer2)
						IF IS_SCREEN_FADED_OUT()
							IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< -114.5969, -2406.8113, 22.2282 >>,<<2,2,2>>)
								SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -114.5969, -2406.8113, 22.2282 >>,TRUE)
								SET_ENTITY_HEADING(PLAYER_PED_ID(),359.8527)
								SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(),FALSE)
							ENDIF
						ENDIF
						IF DOES_ENTITY_EXIST(s_containers[1].obj_main)
							DELETE_OBJECT(s_containers[1].obj_main)
						ENDIF
						IF DOES_ENTITY_EXIST(s_containers[1].obj_left_door)
							DELETE_OBJECT(s_containers[1].obj_left_door)
						ENDIF
						IF DOES_ENTITY_EXIST(s_containers[1].obj_right_door)
							DELETE_OBJECT(s_containers[1].obj_right_door)
						ENDIF
						IF DOES_ENTITY_EXIST(s_containers[0].obj_main)
							DELETE_OBJECT(s_containers[0].obj_main)
						ENDIF
						IF DOES_ENTITY_EXIST(s_containers[0].obj_left_door)
							DELETE_OBJECT(s_containers[0].obj_left_door)
						ENDIF
						IF DOES_ENTITY_EXIST(s_containers[0].obj_right_door)
							DELETE_OBJECT(s_containers[0].obj_right_door)
						ENDIF
						SET_VEHICLE_EXTRA(vehTrailer2,1,FALSE)
//						SET_GAMEPLAY_CAM_RELATIVE_HEADING()
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(PLAYER_PED_ID(),TRUE)
						
						DETACH_ENTITY(PLAYER_PED_ID(),TRUE)
						SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
						
						IF NOT IS_PHONE_ONSCREEN()
							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
						ENDIF
						SETTIMERA(0)
						
						iMissionDialogue = 13
						iPhotosTaken = 0
						iHelp = 0
						IF DOES_ENTITY_EXIST(s_crane.obj_cabin)
							SET_ENTITY_COLLISION(s_crane.obj_cabin,TRUE)
						ENDIF
						
						IF DOES_BLIP_EXIST(dest_blip)
							REMOVE_BLIP(dest_blip)
						ENDIF
						
						IF DOES_BLIP_EXIST(container0_blip)
							REMOVE_BLIP(container0_blip)
						ENDIF
						
						IF DOES_BLIP_EXIST(container1_blip)
							REMOVE_BLIP(container1_blip)
						ENDIF
						
						iProgress++
					ENDIF
				ENDIF
			ENDIF
					
		BREAK
		
		CASE 2
			IF bIsJumpingDirectlyToStage = TRUE
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehFloydTruck)
				IF DOES_ENTITY_EXIST(vehTrailer2)
					IF IS_VEHICLE_DRIVEABLE(vehFloydTruck) AND IS_VEHICLE_DRIVEABLE(vehTrailer2)
						ATTACH_VEHICLE_TO_TRAILER(vehFloydTruck,vehTrailer2)
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehFloydTruck)
				IF IS_VEHICLE_DRIVEABLE(vehFloydTruck) 
					SET_VEHICLE_ON_GROUND_PROPERLY(vehFloydTruck)
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehTrailer2)
				IF IS_VEHICLE_DRIVEABLE(vehTrailer2)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehTrailer2)
				ENDIF
			ENDIF
			
			//SET_BIT (BitSet_CellphoneDisplay, g_BS_FORCE_CELLPHONE_CAM_AUTO_CONTINUE)
			//INHIBIT_CELLPHONE_CAMERA_FUNCTIONS_FOR_DOCKS_SETUP(TRUE)
			BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(TRUE)
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			DISABLE_CELLPHONE(FALSE)
			UNLOCK_MINIMAP_ANGLE()
			IF bIsJumpingDirectlyToStage = FALSE
				INIT_STAGE()
			ENDIF
			
			iProgress = 66
		BREAK
		
		CASE 66
			IF bIsJumpingDirectlyToStage = TRUE
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DH1_START")
				TRIGGER_MUSIC_EVENT("DH1_START")
				SET_LABEL_AS_TRIGGERED("DH1_START",TRUE)
			ENDIF
			
			IF IS_SCREEN_FADED_IN()
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_SNAP")
					IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_SNAP", CONV_PRIORITY_HIGH)
						SET_LABEL_AS_TRIGGERED("DS1_SNAP",TRUE)
					ENDIF
				ELSE
					IF bIsJumpingDirectlyToStage = FALSE
						IF DOES_ENTITY_EXIST(s_crane.obj_main)
							IF IS_PLAYER_AT_LOCATION_ON_FOOT(sLocatesData,<<-114.5757, -2407.0234, 22.4000>>,<<0.500000,0.500000,LOCATE_SIZE_HEIGHT>>,TRUE,"AW_VANTAGE")
								IF DOES_BLIP_EXIST(dest_blip)
									REMOVE_BLIP(dest_blip)
								ENDIF
								REMOVE_BLIP(sLocatesData.LocationBlip)
								IF IS_THIS_PRINT_BEING_DISPLAYED("AW_VANTAGE")
									CLEAR_PRINTS()
								ENDIF
								bAtNeedToFreeze = TRUE
								iProgress = 3
							ELSE
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-114.597664,-2406.989258,22.045231>>, <<-112.793289,-2407.079346,24.337223>>, 1.750000)
									IF DOES_BLIP_EXIST(dest_blip)
										REMOVE_BLIP(dest_blip)
									ENDIF
									REMOVE_BLIP(sLocatesData.LocationBlip)
									IF IS_THIS_PRINT_BEING_DISPLAYED("AW_VANTAGE")
										CLEAR_PRINTS()
									ENDIF
									bAtNeedToFreeze = TRUE
									iProgress = 3
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(dest_blip)
							REMOVE_BLIP(dest_blip)
						ENDIF
						REMOVE_BLIP(sLocatesData.LocationBlip)
						IF IS_THIS_PRINT_BEING_DISPLAYED("AW_VANTAGE")
							CLEAR_PRINTS()
						ENDIF
						iProgress = 3
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE 3 //photo one
			IF NOT HAS_LABEL_BEEN_TRIGGERED("AW_TAKE_PHOTO")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					PRINT_GOD_TEXT("AW_TAKE_PHOTO")
					SET_LABEL_AS_TRIGGERED("AW_TAKE_PHOTO",TRUE)
				ENDIF
			ENDIF
			
			IF IS_BIT_SET(BitSet_CellphoneDisplay, g_BS_PICTURE_TAKE_BUTTON_PRESSED)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PHOTO_H1A")
					CLEAR_HELP()
				ENDIF
			ENDIF
			
			IF NOT WOULD_ENTITY_BE_OCCLUDED(S_M_M_DOCKWORK_01,<< -84.0127, -2365.9124, 16.5003 >>)
			AND IS_POINT_VISIBLE(<< -84.0127, -2365.9124, 16.5003 >>,0.2,200)
				IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC() 
					IF NOT IS_CELLPHONE_CAMERA_IN_SELFIE_MODE()
						CLEAR_HELP()
						#IF IS_DEBUG_BUILD
							PRINTSTRING("Photo is taken - 1")PRINTNL()
						#ENDIF
						SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
						iCameraTimer = GET_GAME_TIMER()
						iPhotosTaken = 1
						iProgress = 99
					ENDIF
					IF iProgress != 99
						IF bPerfectSurveillance
							bPerfectSurveillance = FALSE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTSTRING("PHOTO NOT READY")PRINTNL()
				#ENDIF
			ENDIF
		BREAK
		
		CASE 99
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			IF NOT HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
				IF NOT IS_PED_INJURED(pedMerc[0])
					REQUEST_PED_VISIBILITY_TRACKING(pedMerc[0])
				ENDIF
				IF NOT IS_PED_INJURED(pedMerc[1])
					REQUEST_PED_VISIBILITY_TRACKING(pedMerc[1])
				ENDIF
				IF NOT IS_PED_INJURED(pedMerc[2])
					REQUEST_PED_VISIBILITY_TRACKING(pedMerc[2])
				ENDIF
				IF NOT IS_PED_INJURED(pedMerc[3])
					REQUEST_PED_VISIBILITY_TRACKING(pedMerc[3])
				ENDIF
				iProgress = 4
			ENDIF
		BREAK
		
		
		CASE 4 //photo two
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			ENDIF
			IF IS_BIT_SET(BitSet_CellphoneDisplay, g_BS_PICTURE_TAKE_BUTTON_PRESSED)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PHOTO_H2A")
					CLEAR_HELP()
				ENDIF
			ENDIF
			
			IF NOT IS_PED_INJURED(pedMerc[0])
			AND NOT IS_PED_INJURED(pedMerc[1])
			AND NOT IS_PED_INJURED(pedMerc[2])
			AND NOT IS_PED_INJURED(pedMerc[3])
				PRINTSTRING("HERE-2")PRINTNL()
				IF IS_TRACKED_PED_VISIBLE(pedMerc[0])
				OR IS_TRACKED_PED_VISIBLE(pedMerc[1])
				OR IS_TRACKED_PED_VISIBLE(pedMerc[2])
				OR IS_TRACKED_PED_VISIBLE(pedMerc[3])
					IF IS_ENTITY_IN_CAMERA_PHONE_FRAME(pedMerc[0],fXTolerance,fYTolerance)
					OR IS_ENTITY_IN_CAMERA_PHONE_FRAME(pedMerc[1],fXTolerance,fYTolerance)
					OR IS_ENTITY_IN_CAMERA_PHONE_FRAME(pedMerc[2],fXTolerance,fYTolerance)
					OR IS_ENTITY_IN_CAMERA_PHONE_FRAME(pedMerc[3],fXTolerance,fYTolerance)
						IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
							IF NOT IS_CELLPHONE_CAMERA_IN_SELFIE_MODE()
								CLEAR_HELP()
								#IF IS_DEBUG_BUILD
									PRINTSTRING("Photo is taken - 2")PRINTNL()
								#ENDIF
								SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
								iPhotosTaken = 2
								iCameraTimer = GET_GAME_TIMER()
								iProgress = 98
							ENDIF
							IF iProgress != 98
								IF bPerfectSurveillance
									bPerfectSurveillance = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 98
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			IF NOT HAS_CELLPHONE_CAM_JUST_TAKEN_PIC()
				iProgress = 5
			ENDIF
		BREAK
		
		CASE 5 //photo three
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			//REMOVE SPECIFIC OBJECTIVES
			IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			ENDIF
			IF IS_BIT_SET(BitSet_CellphoneDisplay, g_BS_PICTURE_TAKE_BUTTON_PRESSED)
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("PHOTO_H2A")
					CLEAR_HELP()
				ENDIF
			ENDIF
			
			IF NOT WOULD_ENTITY_BE_OCCLUDED(S_M_M_DOCKWORK_01,<< -220.3576, -2365.8496, 28.6856 >>)
				IF HAS_CELLPHONE_CAM_JUST_TAKEN_PIC() AND MANAGE_MY_TIMER(iCameraTimer,2200)
					IF NOT IS_CELLPHONE_CAMERA_IN_SELFIE_MODE()
						IF IS_CELLPHONE_CAMERA_INHIBITED_FOR_DOCKS_SETUP()
							INHIBIT_CELLPHONE_CAMERA_FUNCTIONS_FOR_DOCKS_SETUP(FALSE)
						ENDIF
						ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(TRUE)
						PRINT_GOD_TEXT("PHOTO_OB1")
						iPhotosTaken = 3
						#IF IS_DEBUG_BUILD
							PRINTSTRING("Photo is taken - 3")PRINTNL()
						#ENDIF
						iProgress++
						SETTIMERA(0)
					ENDIF
					IF iProgress != 6
						IF bPerfectSurveillance
							bPerfectSurveillance = FALSE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					PRINTSTRING("PHOTO NOT READY")PRINTNL()
				#ENDIF
			ENDIF
			
		BREAK
		

		CASE 6
			REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
			IF IS_CONTACTS_LIST_ON_SCREEN()
				IF IS_PHONE_ONSCREEN()
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(DH1_PERFECT_SURVEILLANCE) 
                    FORCE_SELECTION_OF_THIS_CONTACT_ONLY(CHAR_RON)
					iProgress++
     			ENDIF
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			ENDIF
		BREAK
		
		CASE 7 //send the photos
			IF HAS_CONTACT_RECEIVED_PICTURE_MESSAGE(CHAR_RON)
				
				REPLAY_RECORD_BACK_FOR_TIME(4.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				
				bAtNeedToFreeze = FALSE
				iProgress++
				ENABLE_PICTURE_MESSAGE_SENDING_AND_HELP(FALSE)
				SETTIMERA(0)
			ENDIF
		BREAK
		
		CASE 8 //delay to show sending
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DH_1_PHONE_RON")
				IF IS_AUDIO_SCENE_ACTIVE("DH_1_TAKE_PHOTOS")
					STOP_AUDIO_SCENE("DH_1_TAKE_PHOTOS")
				ELSE
					START_AUDIO_SCENE("DH_1_PHONE_RON")
					SET_LABEL_AS_TRIGGERED("DH_1_PHONE_RON",TRUE)
				ENDIF
			ENDIF
			IF TIMERA() > 2000
				bcutsceneplaying = FALSE
				BYPASS_CELLPHONE_CAMERA_DEFAULT_SAVE_ROUTINE(FALSE)	
				SETTIMERA(0)
				iTextMessageDelay = GET_GAME_TIMER()
				iProgress++
			ENDIF
		BREAK
		
		CASE 9
			IF TIMERA() > 2000
				//CUTSCENE_SPY_ON_DOCKS()
				iProgress++
			ENDIF
		BREAK
		
		CASE 10
			IF PRE_STREAM_MISSION_STAGE()
				IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehFloydTruck,TRUE)
				ENDIF
				TRIGGER_MUSIC_EVENT("DH1_STOP")
				REPOSITION_LANDSCAPE_PHONE_FOR_LONG_SUBTITLES (FALSE)
				RESET_MISSION_STAGE_VARIABLES()
				eMissionStage = MISSION_STAGE_10
			ENDIF	
		BREAK

	ENDSWITCH
ENDPROC

/// PURPOSE:
/// Run the Merryweather cutscene
PROC RUN_MERRY_WEATHER_CUTSCENE()

	bcutsceneplaying = TRUE
	
	


	WHILE bcutsceneplaying
	
		WAIT(0)
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ThePortOfLSHeistSetup")
		
		DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_MIL_DOCKS_GATE_IN].doorID, 1.0)
		DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_MIL_DOCKS_GATE_IN].doorID,DOORSTATE_FORCE_OPEN_THIS_FRAME)
		
		IF NOT DOES_ENTITY_EXIST(vanByDocks)
			SETUP_MISSION_REQUIREMENT(REQ_CAR_TO_DRIVE_FINAL_STAGE)
		ENDIF
		
		IF e_section_stage = SECTION_STAGE_SETUP
			IF b_is_jumping_directly_to_stage
				b_is_jumping_directly_to_stage = FALSE
			ELSE
				IF i_current_event = 0
					REQUEST_ANIM_DICT("missheistdockssetup1ig_13@kick_idle")
					REQUEST_ANIM_DICT("missheistdockssetup1ig_9@main_action")
					IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_9@main_action")
					AND HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_9@main_action")
						bcutsceneplaying = TRUE
						bClearCutscenArea = FALSE
						REQUEST_CUTSCENE("LSDHS_MCS_2")
						IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
							SET_VEH_RADIO_STATION(vehFloydTruck, "OFF") 
						ENDIF
						i_current_event++
					ENDIF
				ELIF i_current_event = 1
					IF HAS_CUTSCENE_LOADED()
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ELSE
							WAIT(0)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
							REGISTER_ENTITY_FOR_CUTSCENE(vehFloydTruck, "Trevors_Lorry", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
							SET_VEHICLE_DOOR_CONTROL(vehFloydTruck,SC_DOOR_FRONT_LEFT,DT_DOOR_INTACT,0)
							SET_VEHICLE_DOOR_CONTROL(vehFloydTruck,SC_DOOR_FRONT_RIGHT,DT_DOOR_INTACT,0)
						ENDIF 
						
						IF NOT IS_PED_INJURED(pedFloyd)
							REGISTER_ENTITY_FOR_CUTSCENE(pedFloyd, "Floyd", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF 
						
						IF NOT IS_PED_INJURED(pedMerc[4])
							REGISTER_ENTITY_FOR_CUTSCENE(pedMerc[4], "Guard_kick_Floyd_back", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF NOT IS_PED_INJURED(pedMerc[5])
							REGISTER_ENTITY_FOR_CUTSCENE(pedMerc[5], "Office_guard", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF 
						
						IF NOT IS_PED_INJURED(pedMerc[6])
							REGISTER_ENTITY_FOR_CUTSCENE(pedMerc[6], "Guard_with_gun", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF

						START_CUTSCENE()
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)

						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						b_skipped_mocap = FALSE
						i_current_event = 0
						e_section_stage = SECTION_STAGE_RUNNING
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_RUNNING
			IF bClearCutscenArea = TRUE
				IF NOT b_skipped_mocap
					IF IS_CUTSCENE_SKIP_BUTTON_PRESSED()
					    #IF IS_DEBUG_BUILD
							e_section_stage = SECTION_STAGE_SKIP
						#ENDIF
						PRINTSTRING("J SKIPPED")//PRINTNL()
						b_skipped_mocap = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_CUTSCENE_PLAYING()
				IF bClearCutscenArea = FALSE
					IF IS_VEHICLE_DRIVEABLE(vehTrailer2)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehTrailer2)
					ENDIF
					CLEAR_AREA(<< 476.4661, -3040.6094, 6.0984 >>,100,TRUE,TRUE)
					bClearCutscenArea = TRUE
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DAMAGE FLOYD")
				ENTITY_INDEX entityFloyd
				PED_INDEX cutsceneFloyd
				entityFloyd = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Floyd", CS_FLOYD)
				IF DOES_ENTITY_EXIST(entityFloyd)
					cutsceneFloyd = GET_PED_INDEX_FROM_ENTITY_INDEX(entityFloyd)
				ENDIF
				IF NOT IS_PED_INJURED(cutsceneFloyd)
					IF HAS_ANIM_EVENT_FIRED(entityFloyd,GET_HASH_KEY("Floyd_Beaten"))
						IF DOES_ENTITY_EXIST(entityFloyd)
						AND NOT IS_ENTITY_DEAD(entityFloyd)
							APPLY_PED_DAMAGE_DECAL(GET_PED_INDEX_FROM_ENTITY_INDEX(entityFloyd), PDZ_HEAD, 0.426, 0.755,  218.157, 1.0, 1.0, 3, TRUE, "bruise")
							PRINTSTRING("DAMAGE FLOYD")PRINTNL()
							SET_LABEL_AS_TRIGGERED("DAMAGE FLOYD", TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_BRIEFCASE_02,-1,TRUE)
				//SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_RUN, TRUE, FAUS_CUTSCENE_EXIT)
				IF GET_FOLLOW_PED_CAM_VIEW_MODE() != CAM_VIEW_MODE_FIRST_PERSON
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_RUN, 3000, 1.2757, FALSE)
				ELSE
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_SPRINT, 3000, 1.2757, FALSE)
				ENDIF
			ENDIF
			

			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Floyd")
				IF NOT IS_PED_INJURED(pedFloyd)
					IF DOES_ENTITY_EXIST(pedFloyd)
					AND DOES_ENTITY_EXIST(pedMerc[4])
					AND DOES_ENTITY_EXIST(pedMerc[5])
						IF NOT IS_PED_INJURED(pedFloyd)
						AND NOT IS_PED_INJURED(pedMerc[4])
						AND NOT IS_PED_INJURED(pedMerc[5])
							IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlackAbuse)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFloyd,TRUE)
								REMOVE_PED_FROM_GROUP(pedFloyd)
								sceneBlackAbuse = CREATE_SYNCHRONIZED_SCENE(<< 478.596, -3116.911, 5.069 >>,<< -0.000, -0.000, 53.446 >>)
								TASK_SYNCHRONIZED_SCENE (pedFloyd,sceneBlackAbuse, "missheistdockssetup1ig_13@kick_idle", "guard_beatup_kickidle_dockworker",  REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT,RBF_PLAYER_IMPACT )
								TASK_SYNCHRONIZED_SCENE (pedMerc[4], sceneBlackAbuse,"missheistdockssetup1ig_13@kick_idle", "guard_beatup_kickidle_guard1",  REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT,RBF_PLAYER_IMPACT  )
								TASK_SYNCHRONIZED_SCENE (pedMerc[5], sceneBlackAbuse, "missheistdockssetup1ig_13@kick_idle", "guard_beatup_kickidle_guard2", REALLY_SLOW_BLEND_IN, NORMAL_BLEND_OUT,SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT,RBF_PLAYER_IMPACT  )
								SET_SYNCHRONIZED_SCENE_LOOPED(sceneBlackAbuse,TRUE)
								SET_PED_KEEP_TASK(pedFloyd,TRUE)
								SET_PED_KEEP_TASK(pedMerc[4],TRUE)
								SET_PED_KEEP_TASK(pedMerc[5],TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF HAS_CUTSCENE_FINISHED()
			AND bClearCutscenArea = TRUE
				IF DOES_ENTITY_EXIST(pedMerc[6])
					IF NOT IS_PED_INJURED(pedMerc[6])
						SET_ENTITY_COORDS(pedMerc[6],<<481.60, -3119.79, 6.07>>)
						SET_ENTITY_HEADING(pedMerc[6],51.57)
						TASK_START_SCENARIO_IN_PLACE(pedMerc[6],"WORLD_HUMAN_GUARD_STAND")
					ENDIF
				ENDIF
				e_section_stage = SECTION_STAGE_CLEANUP
			ENDIF
		ENDIF

		IF e_section_stage = SECTION_STAGE_CLEANUP
			
			IF b_skipped_mocap
				REPLAY_CANCEL_EVENT()
				STOP_CUTSCENE()
				//If the player skipped the mocap the peds need to be warped.
				WHILE IS_CUTSCENE_ACTIVE()
					WAIT(0)
				ENDWHILE
			ENDIF
			
			REPLAY_STOP_EVENT()
			
			//Setup buddy relationship groups etc.
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			
			REMOVE_CUTSCENE()
			
			i_current_event = 0
			e_section_stage = SECTION_STAGE_SETUP
			bcutsceneplaying = FALSE
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_SKIP
			STOP_CUTSCENE()
			b_skipped_mocap = TRUE
			e_section_stage = SECTION_STAGE_RUNNING
		ENDIF
	ENDWHILE
ENDPROC

PROC CLEANUP_THE_CRANE()
	
	INT i
	
	REPEAT 8 i
		IF DOES_ROPE_EXIST(s_crane.ropes[i])
			DELETE_ROPE(s_crane.ropes[i])
		ENDIF
		bAddedRopes = FALSE
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(s_crane.obj_main)
		DELETE_OBJECT(s_crane.obj_main)
	ENDIF
	
	IF DOES_ENTITY_EXIST(s_crane.obj_cabin)
		DELETE_OBJECT(s_crane.obj_cabin)
	ENDIF
	
	IF DOES_ENTITY_EXIST(s_crane.obj_spreader)
		DELETE_OBJECT(s_crane.obj_spreader)
	ENDIF
	
	IF DOES_ENTITY_EXIST(s_crane.obj_lift)
		DELETE_OBJECT(s_crane.obj_lift)
	ENDIF
	
	REPEAT COUNT_OF(s_crane.obj_wheels) i
		REMOVE_OBJECT(s_crane.obj_wheels[i], TRUE)
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(s_crane.obj_helper)
		DELETE_OBJECT(s_crane.obj_helper)
	ENDIF
	
	IF DOES_ENTITY_EXIST(s_crane.obj_boom_cable)
		DELETE_OBJECT(s_crane.obj_boom_cable)
	ENDIF
	
	SET_MODEL_AS_NO_LONGER_NEEDED(model_spreader)
	SET_MODEL_AS_NO_LONGER_NEEDED(model_cabin)
	SET_MODEL_AS_NO_LONGER_NEEDED(model_container)
	SET_MODEL_AS_NO_LONGER_NEEDED(model_wheel_left)
	SET_MODEL_AS_NO_LONGER_NEEDED(model_wheel_right)
	SET_MODEL_AS_NO_LONGER_NEEDED(model_boom)
	SET_MODEL_AS_NO_LONGER_NEEDED(model_left_door)
	SET_MODEL_AS_NO_LONGER_NEEDED(model_right_door)
	
ENDPROC

PROC MANAGE_MERRYWEATHER_RESPONSE()
	IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehFloydTruck)
			IF bRestrictedSupressed = FALSE
				SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS,TRUE)
				FOR iCount = 0 To (iTotalMerc -1)
					IF NOT IS_PED_INJURED(pedMerc[iCount])
						SET_PED_RELATIONSHIP_GROUP_HASH(pedMerc[iCount] ,rel_group_buddies)
					ENDIF
				ENDFOR
				bRestrictedSupressed = TRUE
			ENDIF
		ELSE
			IF bRestrictedSupressed = TRUE
				SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS,FALSE)
				RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS,FALSE)
				FOR iCount = 0 To (iTotalMerc -1)
					IF NOT IS_PED_INJURED(pedMerc[iCount])
						SET_PED_RELATIONSHIP_GROUP_HASH(pedMerc[iCount] ,rel_group_enemies)
					ENDIF
				ENDFOR
				bRestrictedSupressed = FALSE
			ENDIF
		ENDIF
	ELSE
		IF bRestrictedSupressed = TRUE
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS,FALSE)
			RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS,FALSE)
			FOR iCount = 0 To (iTotalMerc -1)
				IF NOT IS_PED_INJURED(pedMerc[iCount])
					SET_PED_RELATIONSHIP_GROUP_HASH(pedMerc[iCount] ,rel_group_enemies)
				ENDIF
			ENDFOR
			bRestrictedSupressed = FALSE
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_MERRYWEATHER_RESPONSE_TO_PLAYER()
	
	SWITCH iMerryWeatherResponse
		CASE 0
			IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehFloydTruck)
				
				ELSE
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<485.275024,-3108.497559,5.069427>>, <<485.774292,-3285.950684,17.068666>>, 52.000000)
						iMerryWeatherResponse = 1
					ELSE
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<471.226837,-3083.325684,3.819052>>, <<497.850739,-3085.000488,16.605831>>, 63.750000)
							FOR iCount = 0 To (iTotalMerc -1)
								IF NOT IS_PED_INJURED(pedMerc[iCount])
									IF HAS_PED_RECEIVED_EVENT(pedMerc[iCount],EVENT_SHOT_FIRED)
									OR IS_PED_SHOOTING_IN_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedMerc[iCount],<<-15,-15,-15>>),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedMerc[iCount],<<15,15,15>>), FALSE)
									OR IS_SNIPER_BULLET_IN_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedMerc[iCount],<<-2,-2,-1>>),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedMerc[iCount],<<2,2,1>>))
									OR HAS_PED_RECEIVED_EVENT(pedMerc[iCount],EVENT_SHOT_FIRED_BULLET_IMPACT)
									OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedMerc[iCount],PLAYER_PED_ID())
									OR HAS_PED_RECEIVED_EVENT(pedMerc[iCount],EVENT_SHOCKING_EXPLOSION)
									OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<471.610931,-3121.303711,13.559713>>, <<484.699463,-3121.514160,4.570057>>, 12.750000,WEAPONTYPE_GRENADE)
									OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<471.610931,-3121.303711,13.559713>>, <<484.699463,-3121.514160,4.570057>>, 12.750000,WEAPONTYPE_SMOKEGRENADE)
									OR ((IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedMerc[iCount]) OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedMerc[iCount])) AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedMerc[iCount], << 10, 10, 10 >>))
										iMerryWeatherResponse = 1
									ENDIF
								ENDIF
							ENDFOR
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<471.226837,-3083.325684,3.819052>>, <<497.850739,-3085.000488,16.605831>>, 63.750000)
					FOR iCount = 0 To (iTotalMerc -1)
						IF NOT IS_PED_INJURED(pedMerc[iCount])
							IF HAS_PED_RECEIVED_EVENT(pedMerc[iCount],EVENT_SHOT_FIRED)
							OR IS_PED_SHOOTING_IN_AREA(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedMerc[iCount],<<-15,-15,-15>>),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedMerc[iCount],<<15,15,15>>), FALSE)
							OR IS_SNIPER_BULLET_IN_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedMerc[iCount],<<-2,-2,-1>>),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedMerc[iCount],<<2,2,1>>))
							OR HAS_PED_RECEIVED_EVENT(pedMerc[iCount],EVENT_SHOT_FIRED_BULLET_IMPACT)
							OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedMerc[iCount],PLAYER_PED_ID())
							OR HAS_PED_RECEIVED_EVENT(pedMerc[iCount],EVENT_SHOCKING_EXPLOSION)
							OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<471.610931,-3121.303711,13.559713>>, <<484.699463,-3121.514160,4.570057>>, 12.750000,WEAPONTYPE_GRENADE)
							OR IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<471.610931,-3121.303711,13.559713>>, <<484.699463,-3121.514160,4.570057>>, 12.750000,WEAPONTYPE_SMOKEGRENADE)
							OR ((IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedMerc[iCount]) OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedMerc[iCount])) AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), pedMerc[iCount], << 10, 10, 10 >>))
								iMerryWeatherResponse = 1
							ENDIF
						ENDIF
					ENDFOR
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			FOR iCount = 0 To (iTotalMerc -1)
				IF NOT IS_PED_INJURED(pedMerc[iCount])
					SET_PED_RELATIONSHIP_GROUP_HASH(pedMerc[iCount] ,rel_group_enemies)
					TASK_COMBAT_PED(pedMerc[iCount],PLAYER_PED_ID())
				ENDIF
				
				IF NOT HAS_SOUND_FINISHED(soundBeating)
					STOP_SOUND(soundBeating)
					RELEASE_SOUND_ID(soundBeating)
				ENDIF
				
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,rel_group_buddies)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_enemies,RELGROUPHASH_PLAYER)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE,rel_group_buddies,rel_group_enemies)
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(),3)
				iFailTimer = GET_GAME_TIMER()
				iMerryWeatherResponse = 2
			ENDFOR
		BREAK
		
		CASE 2
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlackAbuse2)
				IF NOT IS_PED_INJURED(pedFloyd)
					REQUEST_ANIM_DICT("missheistdockssetup1ig_13@exit")
					IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_13@exit")
						IF NOT IS_PED_INJURED(pedFloyd)
							IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneBlackAbuse)
								IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBlackAbuse) > 0.99
									IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_13@kick_idle")
										REMOVE_ANIM_DICT("missheistdockssetup1ig_13@kick_idle")
									ENDIF
									sceneBlackAbuse2 = CREATE_SYNCHRONIZED_SCENE(<< 478.596, -3116.911, 5.069 >>,<< -0.000, -0.000, 53.446 >>)
									TASK_SYNCHRONIZED_SCENE(pedFloyd, sceneBlackAbuse2, "missheistdockssetup1ig_13@exit", "guard_beatup_exit_dockworker", REALLY_SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT, RBF_PLAYER_IMPACT)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
				
			IF eMissionStage = MISSION_STAGE_10
			OR (eMissionStage = MISSION_STAGE_11 AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<478.596, -3116.911, 5.069>>) < 40000.0)
				IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_38", CONV_PRIORITY_HIGH)
					IF MANAGE_MY_TIMER(iFailTimer,7000)
						reason_for_fail = COVER_BLOWN
						eMissionStage = MISSION_STAGE_FAIL
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
	ENDSWITCH

ENDPROC

PROC MANAGE_CRANE_CLEANUP()
	IF GET_DISTANCE_BETWEEN_COORDS(<<-119.67, -2415.13, 6.00>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100
		IF DOES_ENTITY_EXIST(s_crane.obj_main)
			IF NOT IS_ENTITY_ON_SCREEN(s_crane.obj_main)
				DELETE_OBJECT(s_crane.obj_main)
				DELETE_OBJECT(s_crane.obj_cabin)
				DELETE_OBJECT(s_crane.obj_spreader)
				DELETE_OBJECT(s_crane.obj_helper)
				DELETE_OBJECT(s_crane.obj_wheels[0])
				DELETE_OBJECT(s_crane.obj_wheels[1])
				DELETE_OBJECT(s_crane.obj_wheels[2])
				DELETE_OBJECT(s_crane.obj_wheels[3])
				DELETE_OBJECT(s_crane.obj_wheels[4])
				DELETE_OBJECT(s_crane.obj_wheels[5])
				DELETE_OBJECT(s_crane.obj_wheels[6])
				DELETE_OBJECT(s_crane.obj_wheels[7])
				DELETE_OBJECT(s_containers[0].obj_main)
				DELETE_OBJECT(s_containers[0].obj_left_door)
				DELETE_OBJECT(s_containers[0].obj_right_door)
				DELETE_OBJECT(s_containers[1].obj_main)
				DELETE_OBJECT(s_containers[1].obj_left_door)
				DELETE_OBJECT(s_containers[1].obj_right_door)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_crane)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_spreader)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_cabin)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_container)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_wheel_left)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_wheel_right)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_boom)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_left_door)
				SET_MODEL_AS_NO_LONGER_NEEDED(model_right_door)
				REMOVE_ANIM_DICT("missheistdockssetup1ig_12@idle_a")
				REQUEST_IPL("pcranecont")
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_DOCKWORKER_PROPS()
	IF NOT IS_PED_INJURED(pedDockWorker[15])
		IF IS_PED_IN_COMBAT(pedDockWorker[15],PLAYER_PED_ID())
		OR IS_PED_RAGDOLL(pedDockWorker[15])
			IF IS_ENTITY_ATTACHED_TO_ANY_PED(objPencil[3])
				DETACH_ENTITY(objPencil[3])
			ENDIF
			IF IS_ENTITY_ATTACHED_TO_ANY_PED(objClipboard[3])
				DETACH_ENTITY(objClipboard[3])
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(pedSecurity)
		IF IS_PED_IN_COMBAT(pedSecurity,PLAYER_PED_ID())
		OR IS_PED_RAGDOLL(pedSecurity)
			IF IS_ENTITY_ATTACHED_TO_ANY_PED(objPencil[0])
				DETACH_ENTITY(objPencil[0])
			ENDIF
			IF IS_ENTITY_ATTACHED_TO_ANY_PED(objClipboard[0])
				DETACH_ENTITY(objClipboard[0])
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_PED_INJURED(pedDockWorker[16])
		IF IS_PED_IN_COMBAT(pedDockWorker[16],PLAYER_PED_ID())
		OR IS_PED_RAGDOLL(pedDockWorker[16])
			IF IS_ENTITY_ATTACHED_TO_ANY_PED(objPencil[2])
				DETACH_ENTITY(objPencil[2])
			ENDIF
			IF IS_ENTITY_ATTACHED_TO_ANY_PED(objClipboard[2])
				DETACH_ENTITY(objClipboard[2])
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

INT iFloydPain
INT iPainTimer

PROC MANAGE_FLOYD_PAIN()

	SWITCH iFloydPain

		CASE 0
			IF NOT IS_PED_INJURED(pedFloyd)
				iPainTimer = GET_GAME_TIMER()
				PLAY_PAIN(pedFloyd, AUD_DAMAGE_REASON_SCREAM_PANIC_SHORT)
				iFloydPain ++
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(pedMerc[4])
				IF MANAGE_MY_TIMER(iPainTimer,GET_RANDOM_INT_IN_RANGE(9000,24000))
					iPainTimer = GET_GAME_TIMER()
					IF GET_RANDOM_BOOL()
						PLAY_PED_AMBIENT_SPEECH(pedMerc[4],"GENERIC_INSULT_HIGH",SPEECH_PARAMS_SHOUTED_CLEAR)
					ELSE
						IF NOT IS_PED_INJURED(pedMerc[5])
							PLAY_PED_AMBIENT_SPEECH(pedMerc[5],"GENERIC_INSULT_HIGH",SPEECH_PARAMS_SHOUTED_CLEAR)
						ENDIF
					ENDIF
					iFloydPain ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF MANAGE_MY_TIMER(iPainTimer,GET_RANDOM_INT_IN_RANGE(2000,6000))
				iFloydPain = 0
			ENDIF
		BREAK

	ENDSWITCH

ENDPROC

INT iAllowResponseTimer
VECTOR vPlayerPosition
BOOL bLeftDocks
INT iSafeTimer

//PURPOSE: Manage michael driving to other docks
PROC MISSION_STAGE_10_PROC()
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-108.585716,-2429.462158,26.250675>>, <<-108.808174,-2400.434082,16.000675>>, 13.000000)
		SET_PED_CAPSULE(PLAYER_PED_ID(),0.25)
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TEXT FROM RON")
		IF MANAGE_MY_TIMER(iTextMessageDelay,5000)
			SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER (CHAR_RON, "TXT_PHOTO_R", TXTMSG_UNLOCKED)
			SET_LABEL_AS_TRIGGERED("TEXT FROM RON",TRUE)
		ENDIF
	ENDIF

	MANAGE_CRANE_CLEANUP()
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("DH_1_GET_TO_TRUCK")
			IF IS_AUDIO_SCENE_ACTIVE("DH_1_PHONE_RON")
				STOP_AUDIO_SCENE("DH_1_PHONE_RON")
			ELSE
				START_AUDIO_SCENE("DH_1_GET_TO_TRUCK")
				SET_LABEL_AS_TRIGGERED("DH_1_GET_TO_TRUCK",TRUE)
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehFloydTruck)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DH_1_DRIVE_TO_DOCKS")
						IF IS_AUDIO_SCENE_ACTIVE("DH_1_GET_TO_TRUCK")
							STOP_AUDIO_SCENE("DH_1_GET_TO_TRUCK")
						ELSE
							START_AUDIO_SCENE("DH_1_DRIVE_TO_DOCKS")
							SET_LABEL_AS_TRIGGERED("DH_1_DRIVE_TO_DOCKS",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-207.146667,-2515.569336,4.152911>>, <<-187.418167,-2515.733154,13.297941>>, 43.000000)
			LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_DOCKS_BACK_GATE_OUT,TRUE,FALSE)
			SET_SCRIPT_UPDATE_DOOR_AUDIO(g_sAutoDoorData[AUTODOOR_DOCKS_BACK_GATE_OUT].doorID,TRUE)
		ELSE
			FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_DOCKS_BACK_GATE_OUT,FALSE)
		ENDIF
		
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<20.904465,-2527.153320,4.268342>>, <<9.272564,-2544.315674,11.300495>>, 46.250000)
			LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_DOCKS_FRONT_GATE_OUT,TRUE,FALSE)
			SET_SCRIPT_UPDATE_DOOR_AUDIO(g_sAutoDoorData[AUTODOOR_DOCKS_FRONT_GATE_OUT].doorID,TRUE)
		ELSE
			FORCE_AUTOMATIC_DOOR_SLIDE_OPEN(AUTODOOR_DOCKS_FRONT_GATE_OUT,FALSE)
		ENDIF

		CREATE_THE_DOCKS_STUFF()

		IF NOT bLeftDocks
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-183.039520,-2523.408203,13.262724>>, <<-208.735977,-2522.902832,4.504209>>, 12.750000)
				bLeftDocks = TRUE
			ENDIF
		ENDIF
		
		IF bLeftDocks
			IF GET_DISTANCE_BETWEEN_COORDS(<<5.0966, -2528.8704, 5.0503>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) > 300
				SAFE_CLEANUP_PED(pedMerc[0],S_M_Y_BLACKOPS_01)
				SAFE_CLEANUP_PED(pedMerc[1],S_M_Y_BLACKOPS_01)
				SAFE_CLEANUP_PED(pedMerc[2],S_M_Y_BLACKOPS_01)
				SAFE_CLEANUP_PED(pedMerc[3],S_M_Y_BLACKOPS_01)
				SAFE_CLEANUP_PED(pedMerc[7],S_M_Y_BLACKOPS_01)
				SAFE_CLEANUP_PED(pedMerc[8],S_M_Y_BLACKOPS_01)
			ENDIF
		ENDIF
		
		IF GET_DISTANCE_BETWEEN_COORDS(<<485.0831, -3051.7222, 5.2262>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) < 100
			IF NOT HAS_LABEL_BEEN_TRIGGERED("REQ_MERRYWEATHER_GUARDS_AT_OTHER_DOCKS")
				IF SETUP_MISSION_REQUIREMENT(REQ_MERRYWEATHER_GUARDS_AT_OTHER_DOCKS)
					SET_LABEL_AS_TRIGGERED("REQ_MERRYWEATHER_GUARDS_AT_OTHER_DOCKS",TRUE)
				ENDIF	
			ENDIF
		ENDIF

		IF iProgress < 6
		OR iProgress = 98
		OR iProgress = 99
			DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_MIL_DOCKS_GATE_IN].doorID, 1.0)
			DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_MIL_DOCKS_GATE_IN].doorID,DOORSTATE_FORCE_OPEN_THIS_FRAME)
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<497.518677,-3065.054443,3.576474>>, <<471.410797,-3065.453857,35.069195>>, 100.000000)
				MANAGE_MERRYWEATHER_RESPONSE_TO_PLAYER()
				MANAGE_MERRYWEATHER_RESPONSE()
			ENDIF
		ENDIF
		
		SWITCH iProgress
	
		CASE 0//Create objective specific items- print objective, add blips etc
			
			IF IS_SCREEN_FADED_OUT()
				MANAGE_PLAYER_OUTFIT()
			ENDIF
			
			s_crane.f_crane_offset = MIN_CRANE_OFFSET
			s_crane.f_crane_vel = 0.0
			
			IF DOES_ENTITY_EXIST(vehHandler)
				DELETE_VEHICLE(vehHandler)
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedDockWorker[17])
				DELETE_PED(pedDockWorker[17])
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedDockWorker[18])
				DELETE_PED(pedDockWorker[18])
			ENDIF

			IF SETUP_PEDS_FOR_DIALOGUE()
				IF IS_VEHICLE_DRIVEABLE(vehFloydTruck) AND IS_VEHICLE_DRIVEABLE(vehTrailer2) 
					IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(vehFloydTruck)
						ATTACH_VEHICLE_TO_TRAILER(vehFloydTruck,vehTrailer2)
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehFloydTruck) AND IS_VEHICLE_DRIVEABLE(vehTrailer2)
					
						IF NOT IS_SCREEN_FADED_OUT()
							IF NOT IS_PED_INJURED(pedFloyd)
								CLEAR_PED_TASKS_IMMEDIATELY(pedFloyd)
//								SET_ENTITY_COORDS(pedFloyd, <<-118.3164, -2412.3242, 5.0000>>)
//								SET_ENTITY_HEADING(pedFloyd, 330.0682)
//								IF NOT IS_PED_INJURED(pedFloyd)
//									IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
//										IF IS_VEHICLE_SEAT_FREE(vehFloydTruck,VS_FRONT_RIGHT)
//											SET_PED_INTO_VEHICLE(pedFloyd,vehFloydTruck,VS_FRONT_RIGHT)
//										ENDIF
//									ENDIF
//								ENDIF
								SET_ENTITY_COORDS(pedFloyd, <<-120.1837, -2418.3833, 5.0014>>)
								SET_ENTITY_HEADING(pedFloyd, 2.0550)
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(pedFloyd,TRUE)
								OPEN_SEQUENCE_TASK(seqMain)
									TASK_LEAVE_ANY_VEHICLE(NULL)
									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-118.3164, -2412.3242, 5.0000>>,1)
									TASK_START_SCENARIO_AT_POSITION(NULL,"WORLD_HUMAN_STAND_IMPATIENT",<<-118.3164, -2412.3242, 5.0000>>,330.0682)
								CLOSE_SEQUENCE_TASK(seqMain)
								TASK_PERFORM_SEQUENCE(pedFloyd, seqMain)
								
							ENDIF
						ENDIF
						
						SET_VEHICLE_EXTRA(vehTrailer2,1,FALSE)
						
						IF NOT IS_PED_INJURED(pedDockWorker[13])
							TASK_WANDER_STANDARD(pedDockWorker[13])
							SET_PED_AS_NO_LONGER_NEEDED(pedDockWorker[13])
						ENDIF
						
						REMOVE_ANIM_DICT("missheistdockssetup1ig_12@idle_a")
						REMOVE_ANIM_DICT("oddjobs@towingpleadingidle_a")
						
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehFloydTruck,TRUE)
						REQUEST_MODEL(TIPTRUCK)
						iMissionDialogue = 17
						iFloydPain = 0
					
						bRestrictedSupressed = FALSE
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							CLEAR_HELP()
						ENDIF
						SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(8,"Drive to the other docks")
						SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
						CLEAR_AREA(<< 476.4661, -3040.6094, 6.0984 >>,100,FALSE)
						SET_PED_NON_CREATION_AREA(<<682.4604, -2936.8667, 2.9499>>,<<260.5602, -3529.1248, -17.7944>>)
						
						SET_PED_COMBAT_ATTRIBUTES(PLAYER_PED_ID(),CA_DO_DRIVEBYS,TRUE)
						CREATE_FORCED_OBJECT(<<479.2571, -3115.5513, 5.0701>>,50,PROP_GATE_DOCKS_LD,TRUE)
						
						REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_DOCKS_GATE_IN,PLAYER_PED_ID())
						//DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_MIL_DOCKS_GATE_IN].doorID, 1.0)
						//DOOR_SYSTEM_SET_HOLD_OPEN(g_sAutoDoorData[AUTODOOR_MIL_DOCKS_GATE_IN].doorID, TRUE)

						IF bIsJumpingDirectlyToStage = TRUE
							v_crane_pos = <<-110.2902, -2415.7900, 5.0500>>
							vSpreaderPosition = <<-109.87, -2415.71, 14.67>>
							IF SETUP_MISSION_REQUIREMENT(REQ_CRANE)
								bLeftDocks = FALSE
								iProgress++
							ENDIF
						ELSE
							bLeftDocks = FALSE
							iProgress++
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF bContainerSetUpForDrive = FALSE
				IF IS_VEHICLE_DRIVEABLE(vehTrailer2)
					//HAD TO TURN OFF COLLISION
					IF IS_SCREEN_FADED_OUT()
						IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
							SET_ENTITY_COORDS(vehFloydTruck,<< -117.1943, -2415.8176, 5.0001 >>)
							SET_ENTITY_HEADING(vehFloydTruck,89.5928)
						ENDIF
						
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
						
						IF DOES_ENTITY_EXIST(s_containers[1].obj_main)
							DELETE_OBJECT(s_containers[1].obj_main)
						ENDIF
						
						IF DOES_ENTITY_EXIST(s_containers[0].obj_main)
							DELETE_OBJECT(s_containers[0].obj_main)
						ENDIF
						
						SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-118.3399, -2410.1582, 5.0013>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(),145.9175)
						
						IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
							IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(vehFloydTruck)
								ATTACH_VEHICLE_TO_TRAILER(vehFloydTruck,vehTrailer2)
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(pedFloyd)
							IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
								IF IS_VEHICLE_SEAT_FREE(vehFloydTruck,VS_FRONT_RIGHT)
									SET_PED_INTO_VEHICLE(pedFloyd,vehFloydTruck,VS_FRONT_RIGHT)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF DOES_ENTITY_EXIST(s_containers[1].obj_main)
						DELETE_OBJECT(s_containers[1].obj_main)
					ENDIF
						
					IF DOES_ENTITY_EXIST(s_containers[0].obj_main)
						DELETE_OBJECT(s_containers[0].obj_main)
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehFloydTruck) AND IS_VEHICLE_DRIVEABLE(vehTrailer2)
						FREEZE_ENTITY_POSITION(vehFloydTruck,FALSE)
						FREEZE_ENTITY_POSITION(vehTrailer2,FALSE)
					ENDIF
			
					SET_VEHICLE_EXTRA(vehTrailer2,1,FALSE)
					
					REQUEST_ANIM_DICT("misslsdhsclipboard@idle_a")
					REQUEST_ANIM_DICT("misslsdhsclipboard@base")
					REQUEST_ANIM_DICT("amb@code_human_police_crowd_control@idle_a")
					REQUEST_ANIM_DICT("misslsdhs")
					REQUEST_MODEL(P_CS_Clipboard)
					REQUEST_MODEL(PROP_PENCIL_01)
					REQUEST_MODEL(TIPTRUCK)
					REQUEST_MODEL(prop_barrier_work06a)
					bContainerSetUpForDrive = TRUE
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(vehFloydTruck) AND IS_VEHICLE_DRIVEABLE(vehTrailer2)
					IF IS_VEHICLE_ATTACHED_TO_TRAILER(vehFloydTruck)
						IF IS_VEHICLE_DRIVEABLE(vehFloydTruck) AND IS_VEHICLE_DRIVEABLE(vehTrailer2)
							FREEZE_ENTITY_POSITION(vehFloydTruck,FALSE)
							FREEZE_ENTITY_POSITION(vehTrailer2,FALSE)
						ENDIF

						INIT_STAGE()
						
						createDockWorker(21)
						REQUEST_ANIM_DICT("misslsdhsclipboard@idle_a")
						REQUEST_ANIM_DICT("misslsdhsclipboard@base")
						REQUEST_ANIM_DICT("amb@code_human_police_crowd_control@idle_a")
						REQUEST_ANIM_DICT("misslsdhs")
						REQUEST_MODEL(P_CS_Clipboard)
						REQUEST_MODEL(PROP_PENCIL_01)
						REQUEST_MODEL(TIPTRUCK)
						REQUEST_MODEL(prop_barrier_work06a)
						
						iProgress = 99
					ELSE
						ATTACH_VEHICLE_TO_TRAILER(vehFloydTruck,vehTrailer2)
					ENDIF
				ENDIF
			ENDIF
		
		BREAK

		
		//CLEAR PRINTS CHECK ANIM REQUESTS AND SET UP SECURITY GUY
		
		CASE 99
			REQUEST_MODEL(TIPTRUCK)
			IF HAS_ANIM_DICT_LOADED("misslsdhs")
			AND HAS_ANIM_DICT_LOADED("misslsdhsclipboard@idle_a")
			AND HAS_ANIM_DICT_LOADED("misslsdhsclipboard@base")
			AND HAS_ANIM_DICT_LOADED("amb@code_human_police_crowd_control@idle_a")
			AND HAS_MODEL_LOADED(P_CS_Clipboard)
			AND HAS_MODEL_LOADED(PROP_PENCIL_01)
			AND HAS_MODEL_LOADED(TIPTRUCK)
			AND HAS_MODEL_LOADED(prop_barrier_work06a)
				IF NOT DOES_ENTITY_EXIST(objClipboard[2])
					objClipboard[2] = CREATE_OBJECT(P_CS_CLIPBOARD,<< -128.2126, -2568.8252, 5.0010 >>)
				ELSE
					IF NOT DOES_ENTITY_EXIST(objPencil[2])
						objPencil[2] = CREATE_OBJECT(PROP_PENCIL_01,<< -128.2126, -2568.8252, 5.0010 >>)
					ELSE
						IF NOT DOES_ENTITY_EXIST(objClipboard[3])
							objClipboard[3] = CREATE_OBJECT(P_CS_CLIPBOARD,vDocks)
						ELSE
							IF NOT DOES_ENTITY_EXIST(objPencil[3])
								objPencil[3] = CREATE_OBJECT(PROP_PENCIL_01,vDocks)
							ELSE
								IF DOES_ENTITY_EXIST(pedDockWorker[15])
									IF NOT IS_PED_INJURED(pedDockWorker[15])
										CLEAR_ALL_PED_PROPS(pedDockWorker[15]) 
										ATTACH_ENTITY_TO_ENTITY(objPencil[3],pedDockWorker[15],GET_PED_BONE_INDEX(pedDockWorker[15], BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>,TRUE)
										ATTACH_ENTITY_TO_ENTITY(objClipboard[3],pedDockWorker[15],GET_PED_BONE_INDEX(pedDockWorker[15], BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>,TRUE)
										TASK_PLAY_ANIM(pedDockWorker[15],"misslsdhsclipboard@base", "base",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,-1,AF_LOOPING)
										ADD_PED_FOR_DIALOGUE(sSpeech, 5, pedDockWorker[15], "GATEGUARD")
										SET_PED_CAN_EVASIVE_DIVE(pedDockWorker[15],FALSE)
										SET_PED_KEEP_TASK(pedDockWorker[15],TRUE)
									ENDIF
								ENDIF
								
								//BLOCKING
								IF HAS_MODEL_LOADED(TIPTRUCK)
									IF NOT DOES_ENTITY_EXIST(vehBlocking)
										SPAWN_VEHICLE(vehBlocking,TIPTRUCK, << 491.91, -3051.15, 5.77 >>, 6.20,-1,1,FALSE)
									ENDIF
								ENDIF
								
								IF HAS_MODEL_LOADED(prop_barrier_work06a)
									IF NOT DOES_ENTITY_EXIST(ObjdockProps[0])
										ObjdockProps[0] = CREATE_OBJECT_NO_OFFSET(prop_barrier_work06a,<<488.82, -3048.82, 5.11>>)
									ENDIF
								ENDIF
								
								IF HAS_MODEL_LOADED(prop_barrier_work06a)
									IF NOT DOES_ENTITY_EXIST(ObjdockProps[1])
										ObjdockProps[1] = CREATE_OBJECT_NO_OFFSET(prop_barrier_work06a,<<494.70, -3049.03, 5.11>>)
									ENDIF
								ENDIF
								
								IF DOES_ENTITY_EXIST(pedDockWorker[16])
									IF NOT IS_PED_INJURED(pedDockWorker[16])
										CLEAR_ALL_PED_PROPS(pedDockWorker[16]) 
										ATTACH_ENTITY_TO_ENTITY(objPencil[2],pedDockWorker[16],GET_PED_BONE_INDEX(pedDockWorker[16], BONETAG_PH_R_HAND),<<0,0,0>>,<<0,0,0>>,TRUE)
										ATTACH_ENTITY_TO_ENTITY(objClipboard[2],pedDockWorker[16],GET_PED_BONE_INDEX(pedDockWorker[16], BONETAG_PH_L_HAND),<<0,0,0>>,<<0,0,0>>,TRUE)
										TASK_PLAY_ANIM(pedDockWorker[16],"misslsdhsclipboard@base", "base",INSTANT_BLEND_IN,INSTANT_BLEND_OUT,-1,AF_LOOPING)
										ADD_PED_FOR_DIALOGUE(sSpeech, 6, pedDockWorker[16], "GATEGUARD")
										SET_PED_CAN_EVASIVE_DIVE(pedDockWorker[16],FALSE)
										SET_PED_KEEP_TASK(pedDockWorker[16],TRUE)
									ENDIF
								ENDIF
								
								IF DOES_ENTITY_EXIST(pedDockWorker[21])
									IF NOT IS_PED_INJURED(pedDockWorker[21])
										OPEN_SEQUENCE_TASK(seqMain)
											TASK_LOOK_AT_ENTITY(NULL,pedDockWorker[16],-1)
											TASK_PLAY_ANIM(NULL,"amb@code_human_police_crowd_control@idle_a", "idle_a",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_DEFAULT)
											TASK_PAUSE(NULL,GET_RANDOM_INT_IN_RANGE(1000,3000))
											TASK_PLAY_ANIM(NULL,"amb@code_human_police_crowd_control@idle_a", "idle_b",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_DEFAULT)
											TASK_PLAY_ANIM(NULL,"amb@code_human_police_crowd_control@idle_a", "idle_c",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_DEFAULT)
											TASK_PAUSE(NULL,GET_RANDOM_INT_IN_RANGE(500,1000))
											TASK_PLAY_ANIM(NULL,"amb@code_human_police_crowd_control@idle_a", "idle_a",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_DEFAULT)
											TASK_PLAY_ANIM(NULL,"amb@code_human_police_crowd_control@idle_a", "idle_c",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_DEFAULT)
											TASK_PLAY_ANIM(NULL,"amb@code_human_police_crowd_control@idle_a", "idle_b",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_DEFAULT)
										SET_SEQUENCE_TO_REPEAT(seqMain,REPEAT_FOREVER)
										CLOSE_SEQUENCE_TASK(seqMain)
										
										TASK_PERFORM_SEQUENCE(pedDockWorker[21], seqMain)
										SET_PED_KEEP_TASK(pedDockWorker[21],TRUE)
										SET_PED_CAN_EVASIVE_DIVE(pedDockWorker[21],FALSE)
									ENDIF
								ENDIF
								IF bIsJumpingDirectlyToStage
									iProgress = 3
								ELSE	
									iProgress = 2
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			
			vPlayerPosition = GET_ENTITY_COORDS(PLAYER_PED_ID())
			PRINTSTRING("Z - ")PRINTFLOAT(vPlayerPosition.z)
			IF vPlayerPosition.z > 6.1
				IF IS_PLAYER_AT_LOCATION_ANY_MEANS(sLocatesData,<<-106.4769, -2404.5315, 5.0013>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,TRUE,"DESCRA")
				
				ELSE
					IF vPlayerPosition.z < 6.1
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_37")
							IF NOT IS_SCREEN_FADED_OUT()
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech,"D1AUD", "DS1_37", CONV_PRIORITY_HIGH) // {floyd yells to trevor from truck}
											REPLAY_RECORD_BACK_FOR_TIME(4.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
											SET_LABEL_AS_TRIGGERED("DS1_37", TRUE)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								SET_LABEL_AS_TRIGGERED("DS1_37", TRUE)
							ENDIF
						ELSE
							ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_1")
							ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_2")
							ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_3")
							ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_4")
							ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_5")
							ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_6")
							ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_7")
							ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_8")
							CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
							iSafeTimer = GET_GAME_TIMER()
							iProgress = 98
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF vPlayerPosition.z < 6.1
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_37")
						IF NOT IS_SCREEN_FADED_OUT()
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech,"D1AUD", "DS1_37", CONV_PRIORITY_HIGH) // {floyd yells to trevor from truck}
										SET_LABEL_AS_TRIGGERED("DS1_37", TRUE)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							SET_LABEL_AS_TRIGGERED("DS1_37", TRUE)
						ENDIF
					ELSE
						ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_1")
						ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_2")
						ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_3")
						ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_4")
						ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_5")
						ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_6")
						ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_7")
						ASSISTED_MOVEMENT_REMOVE_ROUTE("pols_8")
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						iSafeTimer = GET_GAME_TIMER()
						iProgress = 98
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 98
			
			IF NOT IS_PED_INJURED(pedFloyd)
				IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
					IF NOT IS_PED_IN_VEHICLE(pedFloyd,vehFloydTruck)
						IF GET_SCRIPT_TASK_STATUS(pedFloyd,SCRIPT_TASK_ENTER_VEHICLE)<> PERFORMING_TASK
							TASK_ENTER_VEHICLE(pedFloyd,vehFloydTruck,DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT,PEDMOVEBLENDRATIO_WALK,ECF_BLOCK_SEAT_SHUFFLING)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF MANAGE_MY_TIMER(iSafeTimer,5000)
				IF NOT IS_PED_INJURED(pedFloyd)
					IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
						IF IS_PED_GETTING_INTO_A_VEHICLE(pedFloyd)
						OR IS_PED_IN_VEHICLE(pedFloyd,vehFloydTruck)
							CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
							bGoToSpeach = FALSE
							iStageStartTime = GET_GAME_TIMER()
							iProgress = 3
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_37b")
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sSpeech,"D1AUD", "DS1_37b", CONV_PRIORITY_HIGH) // {floyd yells to trevor from truck}
							SET_LABEL_AS_TRIGGERED("DS1_37b", TRUE)
							IF NOT IS_PED_INJURED(pedFloyd)
								IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
									CLEAR_PED_TASKS(pedFloyd)
									TASK_ENTER_VEHICLE(pedFloyd,vehFloydTruck,DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT,PEDMOVEBLENDRATIO_WALK,ECF_BLOCK_SEAT_SHUFFLING)
								ENDIF
							ENDIF
							iStageStartTime = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(pedFloyd)
					IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
						IF IS_PED_GETTING_INTO_A_VEHICLE(pedFloyd)
						OR IS_PED_IN_VEHICLE(pedFloyd,vehFloydTruck)
							CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
							bGoToSpeach = FALSE
							iStageStartTime = GET_GAME_TIMER()
							iProgress = 3
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			
			IF NOT IS_PED_INJURED(pedFloyd)
				IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
					IF NOT IS_PED_IN_VEHICLE(pedFloyd,vehFloydTruck)
						IF GET_SCRIPT_TASK_STATUS(pedFloyd,SCRIPT_TASK_ENTER_VEHICLE)<> PERFORMING_TASK
							TASK_ENTER_VEHICLE(pedFloyd,vehFloydTruck,DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT,PEDMOVEBLENDRATIO_WALK,ECF_BLOCK_SEAT_SHUFFLING)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("GET IN BIG RIG")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData,IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
					PRINT_GOD_TEXT("AW_TREVTRU")
					SET_LABEL_AS_TRIGGERED("GET IN BIG RIG", TRUE)
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_ATTACHED_TO_TRAILER(vehFloydTruck)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<471.729736,-3121.691895,4.570057>>, <<484.070709,-3121.885742,11.820057>>, 11.250000)
					eFailedStage = eMissionStage
					reason_for_fail = COVER_BLOWN
					eMissionStage = MISSION_STAGE_FAIL
				ENDIF
			ELSE
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<483.672943,-3107.970459,4.570056>>, <<474.004242,-3108.686523,12.070057>>, 12.750000)
					eFailedStage = eMissionStage
					reason_for_fail = COVER_BLOWN
					eMissionStage = MISSION_STAGE_FAIL
				ENDIF
			ENDIF
			
			FLOAT vDistA
			FLOAT vDistB
			//TEXT_LABEL Conversation
			PED_INDEX pedTemp
			IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
				IF IS_VEHICLE_ATTACHED_TO_TRAILER(vehFloydTruck)
					IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(sLocatesData,<< 476.4661, -3040.6094, 5.0913>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,FALSE,pedFloyd,vehFloydTruck,"AW_DRI_SUB","AW_DONT_LEAVE_T","","AW_GETBACK_INT",FALSE,FALSE)
						
					ELSE
						IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<< 476.4661, -3040.6094, 6.0984 >>,<<7,7,7>>)
								REQUEST_ANIM_DICT("misslsdhsclipboard@idle_a")
								IF HAS_ANIM_DICT_LOADED("misslsdhsclipboard@idle_a")
									KILL_FACE_TO_FACE_CONVERSATION()
									IF NOT IS_PED_INJURED(pedDockWorker[15])
										vDistA = GET_DISTANCE_BETWEEN_ENTITIES(pedDockWorker[15],vehFloydTruck)
									ENDIF
									
									IF NOT IS_PED_INJURED(pedDockWorker[16])
										vDistB = GET_DISTANCE_BETWEEN_ENTITIES(pedDockWorker[16],vehFloydTruck)
									ENDIF
								
									IF vDistA < vDistB
										pedTemp = pedDockWorker[15]
									ELSE
										pedTemp = pedDockWorker[16]
									ENDIF
									
									IF NOT IS_PED_INJURED(pedTemp)
										OPEN_SEQUENCE_TASK(seqMain)
											TASK_PLAY_ANIM(NULL,"misslsdhsclipboard@idle_a", "idle_a",NORMAL_BLEND_IN,NORMAL_BLEND_OUT,-1,AF_LOOPING)
										CLOSE_SEQUENCE_TASK(seqMain)
										TASK_PERFORM_SEQUENCE(pedTemp, seqMain)
									ENDIF
									
									REMOVE_CUTSCENE()
									REQUEST_CUTSCENE("LSDHS_MCS_2")
									REQUEST_ANIM_DICT("missheistdockssetup1ig_13@kick_idle")
									CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
									iProgress++
								ENDIF
							ENDIF
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_37c")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_37c", CONV_PRIORITY_MEDIUM)
											REPLAY_RECORD_BACK_FOR_TIME(8.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
											SET_LABEL_AS_TRIGGERED("DS1_37c",TRUE)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF bGoToSpeach = FALSE
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_16", CONV_PRIORITY_MEDIUM)
												bGoToSpeach = TRUE
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_39")
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
												IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_39", CONV_PRIORITY_MEDIUM)
													SET_LABEL_AS_TRIGGERED("DS1_39",TRUE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF

						ENDIF
						
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehFloydTruck)
							IF NOT DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
								sLocatesData.vehicleBlip = ADD_BLIP_FOR_ENTITY(vehFloydTruck)
								SET_BLIP_COLOUR(sLocatesData.vehicleBlip,BLIP_COLOUR_BLUE)
							ENDIF
						ENDIF
						
						IF bGoToSpeach = TRUE
							//MANAGE PAUSE DIALOGUE
							IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
									CLEAR_PRINTS()
									PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
								ENDIF
							//Play conversation
							ELSE
								IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
									IF IS_THIS_PRINT_BEING_DISPLAYED("AW_GETBACK_INT")
										CLEAR_PRINTS()
									ENDIF
							    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF MANAGE_MY_TIMER(iStageStartTime,3000)
						IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						OR DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
							CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			PRINTLN("@@@@@@@ CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY() @@@@@@@@@@@")
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
				PRINTLN("@@@@@@@ SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED @@@@@@@@@@@")
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Floyd", pedFloyd)
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_17")
				IF CREATE_CONVERSATION(sSpeech,"D1AUD","DS1_17", CONV_PRIORITY_HIGH)
					SET_LABEL_AS_TRIGGERED("DS1_17",TRUE)
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_ATTACHED_TO_TRAILER(vehFloydTruck)
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<471.729736,-3121.691895,4.570057>>, <<484.070709,-3121.885742,11.820057>>, 11.250000)
					eFailedStage = eMissionStage
					reason_for_fail = COVER_BLOWN
					eMissionStage = MISSION_STAGE_FAIL
				ENDIF
			ELSE
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<483.672943,-3107.970459,4.570056>>, <<474.004242,-3108.686523,12.070057>>, 12.750000)
					eFailedStage = eMissionStage
					reason_for_fail = COVER_BLOWN
					eMissionStage = MISSION_STAGE_FAIL
				ENDIF
			ENDIF
			
			
			IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
				IF IS_VEHICLE_ATTACHED_TO_TRAILER(vehFloydTruck)
					IF IS_VEHICLE_DRIVEABLE(vehTrailer2)
						IF NOT IS_ENTITY_IN_ANGLED_AREA(vehTrailer2, <<478.711945,-3105.557373,4.570057>>, <<479.165710,-3123.542236,10.820057>>, 6.500000)
							//IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(sLocatesData,<< 478.8654, -3107.2019, 6.0984 >>,<<5,5,5>>,FALSE,pedFloyd,vehFloydTruck,"AW_DRI_SUB2","AW_DONT_LEAVE_T","AW_DOCK1","AW_GETBACK_INT",FALSE,FALSE)
							IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(sLocatesData,<< 478.8654, -3107.2019, 5.0690 >>,<<5,5,LOCATE_SIZE_HEIGHT>>,TRUE,pedFloyd,vehFloydTruck,"","AW_DONT_LEAVE_T","AW_DOCK1","AW_GETBACK_INT",FALSE,FALSE)
								IF NOT HAS_LABEL_BEEN_TRIGGERED("HALT")
									IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
										PLAY_SOUND_FROM_ENTITY(-1, "Truck_Stop", vehFloydTruck, "DOCKS_HEIST_SETUP_SOUNDS")
										SET_LABEL_AS_TRIGGERED("HALT",TRUE)
									ENDIF
								ENDIF
								IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehFloydTruck,6.0,1)
									
									REPLAY_RECORD_BACK_FOR_TIME(6.0, 0.0)
									
									bDoneWithTruck = TRUE
									KILL_ANY_CONVERSATION()
									SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
									REQUEST_CUTSCENE("LSDHS_MCS_2")
									bIgnoreTheseFails = TRUE
									e_section_stage = SECTION_STAGE_SETUP
									i_current_event = 0
									iProgress++
								ENDIF
							ENDIF
						ELSE
							eFailedStage = eMissionStage
							reason_for_fail = COVER_BLOWN
							eMissionStage = MISSION_STAGE_FAIL
						ENDIF
					ENDIF
			
				ELSE
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					OR DOES_BLIP_EXIST(sLocatesData.vehicleBlip)
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 5
			IF NOT IS_SCRIPTED_CONVERSATION_ONGOING() 
				IF IS_AUDIO_SCENE_ACTIVE("DH_1_DRIVE_TO_DOCKS")
					STOP_AUDIO_SCENE("DH_1_DRIVE_TO_DOCKS")
				ENDIF
				RUN_MERRY_WEATHER_CUTSCENE()
				REQUEST_ANIM_DICT("missheistdockssetup1ig_9@main_action")
				REQUEST_ANIM_DICT("misslsdhsclipboard@idle_a")
				iAllowResponseTimer = GET_GAME_TIMER()
				iProgress++	
			ENDIF
		BREAK
		
		CASE 6
			IF NOT DOES_ENTITY_EXIST(vanByDocks)
				SETUP_MISSION_REQUIREMENT(REQ_CAR_TO_DRIVE_FINAL_STAGE)
			ELSE
				IF NOT IS_CUTSCENE_ACTIVE()
					IF HAS_ANIM_DICT_LOADED("missheistdockssetup1ig_9@main_action")
						
						//FIX FOR CLEANING UP UNUSED ASSETS
						IF DOES_ENTITY_EXIST(vehMission)
							DELETE_VEHICLE(vehMission)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_VEH_MODEL(CHAR_TREVOR) )
						ELSE
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_VEH_MODEL(CHAR_TREVOR) )
						ENDIF
						
						IF DOES_ENTITY_EXIST(pedWade)
							DELETE_PED(pedWade)
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_WADE))
						ELSE
							SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_WADE))
						ENDIF
						
						IF DOES_ENTITY_EXIST(s_sp_floyd_grabs_package[0].obj_prop)
							DELETE_OBJECT(s_sp_floyd_grabs_package[0].obj_prop)
							SET_MODEL_AS_NO_LONGER_NEEDED(prop_cs_package_01)
						ELSE
							SET_MODEL_AS_NO_LONGER_NEEDED(prop_cs_package_01)
						ENDIF

						REMOVE_ANIM_DICT("missheistdockssetup1ig_4@start_idle")
						REMOVE_ANIM_DICT("missheistdockssetup1ig_12@base")
						REMOVE_ANIM_DICT("missheistdockssetup1ig_4@main_action")
						REMOVE_ANIM_DICT("missheistdockssetup1ig_12@idle_a")
						REMOVE_ANIM_DICT("missheistdockssetup1ig_13@start_idle")
						REMOVE_ANIM_DICT("missheistdockssetup1ig_3@talk")
						REMOVE_ANIM_DICT("missheistdockssetup1ig_3@base")
						REMOVE_ANIM_DICT("missheistdockssetup1ig_13@main_action")
						REMOVE_ANIM_DICT("missheistdockssetup1ig_5@base")
						REMOVE_ANIM_DICT("missheistdockssetup1ig_7")
						REMOVE_ANIM_DICT("misslsdhsclipboard@idle_a")
						REMOVE_ANIM_DICT("missheistdockssetup1ig_12@idle_c")
						
						CLEANUP_THE_CRANE()
						
						IF IS_VEHICLE_DRIVEABLE(vehFloydTruck)
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehFloydTruck,FALSE)
						ENDIF
						
						UNREGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_MIL_DOCKS_GATE_IN,PLAYER_PED_ID())
						//DOOR_SYSTEM_SET_HOLD_OPEN(g_sAutoDoorData[AUTODOOR_MIL_DOCKS_GATE_IN].doorID, FALSE)
						DOOR_SYSTEM_SET_OPEN_RATIO(g_sAutoDoorData[AUTODOOR_MIL_DOCKS_GATE_IN].doorID, 0.0)
						DOOR_SYSTEM_SET_DOOR_STATE(g_sAutoDoorData[AUTODOOR_MIL_DOCKS_GATE_IN].doorID,DOORSTATE_LOCKED)
						
						iProgress++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			
			IF MANAGE_MY_TIMER(iAllowResponseTimer,3000)
				MANAGE_MERRYWEATHER_RESPONSE_TO_PLAYER()
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_SOF")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_SOF", CONV_PRIORITY_MEDIUM)
							SET_LABEL_AS_TRIGGERED("DS1_SOF",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_PLAYER_AT_ANGLED_AREA_ANY_MEANS(sLocatesData,<<476.6509, -3048.0332, 5.0939>>,<<522.403198,-3048.688477,4.068941>>, <<465.621979,-3048.312012,21.168930>>, 8.000000,FALSE,"LEAVE_RESTA")
				REPLAY_RECORD_BACK_FOR_TIME(10.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
				iProgress++
			ELSE
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					REMOVE_BLIP(sLocatesData.LocationBlip)
				ENDIF
				IF VDIST(<<476.6509, -3048.0332, 5.0939>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) > 100
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						REMOVE_BLIP(sLocatesData.LocationBlip)
					ENDIF
					CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
					iProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS,FALSE)
			RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS,FALSE)
			RESET_MISSION_STAGE_VARIABLES()
			ADVANCE_MISSION_STAGE()
		BREAK

	ENDSWITCH
ENDPROC

INT iTimeBeforePhoneCall
INT iTimeInVehicle
BOOL bGotInVehicle
BOOL bDoPhoneCall


//PURPOSE: Drive home
PROC MISSION_STAGE_11_PROC()

	CAM_VIEW_MODE_CONTEXT activeViewModeContext
	CAM_VIEW_MODE activeViewMode

	//Disable changing camera when near to drop off
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< -1147.4010, -1522.5767, 9.4130 >>) < 30
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
	ENDIF
	
	IF iProgress > 0
		IF bDoPhoneCall = FALSE
			IF IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF bGotInVehicle = FALSE
					bGotInVehicle = TRUE
					SET_WANTED_LEVEL_MULTIPLIER(0.2)
					iTimeInVehicle = GET_GAME_TIMER()
				ENDIF
			ELSE
				IF bGotInVehicle = FALSE
					bGotInVehicle = TRUE
				ENDIF
			ENDIF
			
			IF MANAGE_MY_TIMER(iTimeBeforePhoneCall,60000)
			OR MANAGE_MY_TIMER(iTimeInVehicle,7500) AND IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
					IF SETUP_PEDS_FOR_DIALOGUE()
						IF CHAR_CALL_PLAYER_CELLPHONE( sSpeech, CHAR_RON, "D1AUD", "DS1_12", CONV_PRIORITY_VERY_HIGH)
							bDoPhoneCall = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1150.458984,-1521.551270,9.632723>>, <<-1153.754761,-1516.601074,12.382723>>, 22.250000)
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),0.8)
	ENDIF
		
	MANAGE_MERRYWEATHER_RESPONSE_TO_PLAYER()

	SWITCH iProgress
	
		CASE 0//Create objective specific items- print objective, add blips etc
		
			IF IS_SCREEN_FADED_OUT()
				MANAGE_PLAYER_OUTFIT()
			ENDIF
			
			WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_CAR_TO_DRIVE_FINAL_STAGE)
				WAIT(0)
			ENDWHILE
			
			REQUEST_MODEL(TIPTRUCK)
			DISABLE_TAXI_HAILING(FALSE)
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(9,"Drive home",TRUE)
			
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS,FALSE)
			RELEASE_SUPPRESSED_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS,FALSE)
			
			SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT, BUILDINGSTATE_DESTROYED) // Trodden mess in the apartment.
			SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_SHIT_SOFA, BUILDINGSTATE_DESTROYED) // Mess on the sofa
			SET_BUILDING_STATE(BUILDINGNAME_ES_FLOYDS_APPARTMENT_PICTURE, BUILDINGSTATE_DESTROYED)
			
			bDialogue = FALSE
			soundBeating = GET_SOUND_ID()
			INIT_STAGE()
			SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
			RELEASE_AMBIENT_AUDIO_BANK()
			iTimeBeforePhoneCall = GET_GAME_TIMER()
			bDoPhoneCall = FALSE
			
			ADD_PED_FOR_DIALOGUE(sSpeech, 2, PLAYER_PED_ID(), "TREVOR")
			ADD_PED_FOR_DIALOGUE(sSpeech,0,NULL,"NERVOUSRON")
			
			iProgress++
		BREAK
		
		CASE 1
			
			iProgress++
		BREAK
		
		CASE 2
			IF NOT HAS_LABEL_BEEN_TRIGGERED("Floyd_Beating")
				IF REQUEST_AMBIENT_AUDIO_BANK("SCRIPT\\PORT_OF_LS_01_FLOYD_BEATING")
					PLAY_SOUND_FROM_COORD(soundBeating,"Floyd_Beating",<<479, -3106, 8>>,"DOCKS_HEIST_SETUP_SOUNDS")
					SET_LABEL_AS_TRIGGERED("Floyd_Beating",TRUE)
				ENDIF
			ENDIF
			
			IF HAS_MODEL_LOADED(TIPTRUCK)
				IF NOT DOES_ENTITY_EXIST(vehFinal)
					SPAWN_VEHICLE(vehFinal,TIPTRUCK, << 465.8751, -3037.0837, 5.0686 >>, 126.3433,-1,1)
				ENDIF
			ENDIF
			
			//Request the end cutscene and planning board section once the player gets close enough to Floyd's.
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF NOT bBoardCutsceneRequested
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -1147.4010, -1522.5767, 9.4130 >>) < 5625 //75^2
						// Pre-load the cutscene in the heist controller for our arrival.
						SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_PRIME_BOARD_TRANSITION, TRUE)
						bBoardCutsceneRequested = TRUE
					ENDIF
				ELSE
					IF VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), << -1147.4010, -1522.5767, 9.4130 >>) > 6400 //80^2
						// Remove cutscene if player then drives further away from Floyd's place
						SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, FALSE)
						bBoardCutsceneRequested = FALSE
					ENDIF		
				ENDIF
			ENDIF
			//B* 1694464: Make player walk up the stairs so the streaming has time to load the interior
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< -1147.4010, -1522.5767, 9.4130 >>) < 30
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
			ELSE
				SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_SPRINT)
			ENDIF

			IF NOT HAS_LABEL_BEEN_TRIGGERED("Pin interior")
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< -1147.4010, -1522.5767, 9.4130 >>) < 100
					interior_living_room = get_interior_at_coords(<< -1158.3411, -1520.8929, 9.6345 >>)
					REQUEST_ANIM_DICT("missheistdockssetup1leadinoutlsdhs_mcs_3")
					IF is_valid_interior(interior_living_room)
						pin_interior_in_memory(interior_living_room)
						SET_LABEL_AS_TRIGGERED("Pin interior",TRUE)
					ENDIF
				ENDIF
			ELSE
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< -1147.4010, -1522.5767, 9.4130 >>) < 100
					IF NOT DOES_ENTITY_EXIST(pedWade)
						SETUP_MISSION_REQUIREMENT_WITH_LOCATION(REQ_WADE, vWadeStart, fWadeStart)
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("wade_sit_couch")
							REQUEST_ANIM_DICT("missheistdockssetup1leadinoutlsdhs_mcs_3")
							REQUEST_WAYPOINT_RECORDING("d1leadin")
							IF NOT IS_PED_INJURED(pedWade)
								IF HAS_ANIM_DICT_LOADED("missheistdockssetup1leadinoutlsdhs_mcs_3")
								AND GET_IS_WAYPOINT_RECORDING_LOADED("d1leadin")
									
									SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_HEAD, 0, 1)
									SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_BERD, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_HAIR, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_TORSO, 1, 1)
									SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_LEG, 1, 1)
									SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_HAND, 1, 1)
									SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_FEET, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_TEETH, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_SPECIAL, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_SPECIAL2, 0, 0)
									SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_DECL, 1, 0)
									SET_PED_COMPONENT_VARIATION(pedWade, PED_COMP_JBIB, 0, 0)
									
									USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("d1leadin",TRUE)
									ssWadeOnCouch = CREATE_SYNCHRONIZED_SCENE(<< -1158.595, -1519.261, 9.608 >>,<< 0.000, 0.000, -51.000 >>)
									TASK_SYNCHRONIZED_SCENE(pedWade, ssWadeOnCouch, "missheistdockssetup1leadinoutlsdhs_mcs_3", "wade_sit_couch", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
									SET_SYNCHRONIZED_SCENE_LOOPED(ssWadeOnCouch,TRUE)
									SET_PED_KEEP_TASK(pedWade, TRUE)
									SET_LABEL_AS_TRIGGERED("wade_sit_couch",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<< -1147.4010, -1522.5767, 9.4130 >>) > 120
					IF DOES_ENTITY_EXIST(pedWade)
						DELETE_PED(pedWade)
						SET_LABEL_AS_TRIGGERED("wade_sit_couch",FALSE)
					ENDIF
					REMOVE_ANIM_DICT("missheistdockssetup1leadinoutlsdhs_mcs_3")
					IF interior_living_room != NULL
						UNPIN_INTERIOR(interior_living_room)
						SET_LABEL_AS_TRIGGERED("Pin interior",FALSE)
					ENDIF
				ENDIF
			ENDIF

			IF IS_PLAYER_AT_LOCATION_ANY_MEANS(sLocatesData,<<-1156.1887, -1519.6130, 9.6327>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,TRUE,"AW_RETURN_HOME",TRUE)
				IF NOT HAS_SOUND_FINISHED(soundBeating)
					STOP_SOUND(soundBeating)
					RELEASE_SOUND_ID(soundBeating)
				ENDIF
				REQUEST_ANIM_DICT("missheistdockssetup1leadinoutlsdhs_mcs_3")
				iProgress++
			ELSE
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-1153.995117,-1518.853638,9.632723>>, <<-1155.095581,-1517.376221,12.382723>>, 2.250000)
					IF NOT HAS_SOUND_FINISHED(soundBeating)
						STOP_SOUND(soundBeating)
						RELEASE_SOUND_ID(soundBeating)
					ENDIF
					REQUEST_ANIM_DICT("missheistdockssetup1leadinoutlsdhs_mcs_3")
					iProgress++
				ENDIF
				
				//Play dialogue on entering the door in first person
				activeViewModeContext = GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()
				
				IF activeViewModeContext = CAM_VIEW_MODE_CONTEXT_ON_FOOT
					activeViewMode = GET_CAM_VIEW_MODE_FOR_CONTEXT(activeViewModeContext)
					IF activeViewMode = CAM_VIEW_MODE_FIRST_PERSON
						//On player entering the door in first person play dialogue line
						IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-1149.637939,-1522.367432,9.382867>>, <<-1151.353516,-1519.901245,12.382729>>, 1.500000)
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_MCS3LI")
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF PLAY_SINGLE_LINE_FROM_CONVERSATION(sSpeech, "D1AUD", "DS1_MCS3LI", "DS1_MCS3LI_3", CONV_PRIORITY_MEDIUM)
											PRINTLN("FIRST PERSON CONVERSATION TRIGGERED!")
											SET_LABEL_AS_TRIGGERED("DS1_MCS3LI",TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3			
			activeViewModeContext = GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()
							
			IF activeViewModeContext = CAM_VIEW_MODE_CONTEXT_ON_FOOT
				activeViewMode = GET_CAM_VIEW_MODE_FOR_CONTEXT(activeViewModeContext)
				IF activeViewMode != CAM_VIEW_MODE_FIRST_PERSON
					//If third person progress as usual
					REQUEST_ANIM_DICT("missheistdockssetup1leadinoutlsdhs_mcs_3")
					IF HAS_ANIM_DICT_LOADED("missheistdockssetup1leadinoutlsdhs_mcs_3")
						IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
							SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
							OPEN_SEQUENCE_TASK(seqMain)
								TASK_SWAP_WEAPON(NULL, TRUE)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-1155.5874, -1518.9718, 9.6327>>,PEDMOVE_WALK,DEFAULT_TIME_BEFORE_WARP,DEFAULT_NAVMESH_RADIUS,ENAV_NO_STOPPING)
								TASK_PLAY_ANIM(NULL,"missheistdockssetup1leadinoutlsdhs_mcs_3", "trevor_react",SLOW_BLEND_IN,SLOW_BLEND_OUT)
							CLOSE_SEQUENCE_TASK(seqMain)
							TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqMain)
						ENDIF
						SETTIMERB(0)
						iProgress++
					ENDIF
				ELSE
					//Have Trevor stop after entering trigger
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					//When first person mode progress stage immediately
					SETTIMERB(1500)
					iProgress++
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF TIMERB() > 420
				IF NOT IS_PHONE_ONSCREEN()
					SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
				ENDIF
				IF TIMERB() > 1500
					activeViewModeContext = GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()
							
					IF activeViewModeContext = CAM_VIEW_MODE_CONTEXT_ON_FOOT
						activeViewMode = GET_CAM_VIEW_MODE_FOR_CONTEXT(activeViewModeContext)
						IF activeViewMode != CAM_VIEW_MODE_FIRST_PERSON
							//Standard progression when third person
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DS1_MCS3LI")
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF CREATE_CONVERSATION(sSpeech, "D1AUD", "DS1_MCS3LI", CONV_PRIORITY_MEDIUM)
											SET_LABEL_AS_TRIGGERED("DS1_MCS3LI",TRUE)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF DOES_ENTITY_EXIST(pedWade)
										IF NOT IS_PED_INJURED(pedWade)
											g_sTriggerSceneAssets.ped[0] = pedWade
										ENDIF
									ENDIF
									RESET_MISSION_STAGE_VARIABLES()
									ADVANCE_MISSION_STAGE()
								ENDIF
							ENDIF
						ELSE
							//When in first person trigger immediately on hitting cut-scene location
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF DOES_ENTITY_EXIST(pedWade)
									IF NOT IS_PED_INJURED(pedWade)
										g_sTriggerSceneAssets.ped[0] = pedWade
									ENDIF
								ENDIF
								RESET_MISSION_STAGE_VARIABLES()
								ADVANCE_MISSION_STAGE()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		ENDSWITCH
ENDPROC


PROC MANAGE_PED_BLIPS()
	//Enemy blips
	REPEAT iTotalMerc iThisCounter
		IF SAFE_DEATH_CHECK_PED(pedMerc[iThisCounter])
//			SAFE_REMOVE_BLIP(blipMerc[iThisCounter])
		ENDIF
	ENDREPEAT
	
	//Worker blips
	REPEAT iTotalDockWorker iThisCounter
		IF SAFE_DEATH_CHECK_PED(pedDockWorker[iThisCounter])
			SAFE_REMOVE_BLIP(blipDockWorker[iThisCounter])
		ENDIF
	ENDREPEAT
ENDPROC



PROC MANAGE_CRANE_SOUNDS()
	
	SWITCH iManagePickerAudio
	
		CASE 0
			IF REQUEST_AMBIENT_AUDIO_BANK("Crane")
			AND REQUEST_AMBIENT_AUDIO_BANK("Crane_Stress")
			AND REQUEST_AMBIENT_AUDIO_BANK("Crane_Impact_Sweeteners")
				//PRINTSTRING("strain audio prepped")PRINTNL()
				iManagePickerAudio ++
			ENDIF
		BREAK
		
		CASE 1
			IF DOES_ENTITY_EXIST(s_crane.obj_spreader)
				IF DOES_ENTITY_EXIST(s_Containers[0].obj_Main)
				AND DOES_ENTITY_EXIST(s_Containers[1].obj_Main)
					IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(s_Containers[0].obj_Main,s_crane.obj_spreader)
					AND NOT IS_ENTITY_ATTACHED_TO_ENTITY(s_Containers[1].obj_Main,s_crane.obj_spreader)
						PLAY_SOUND_FROM_ENTITY(iCraneStrain, "Strain_No_Container", s_crane.obj_spreader, "CRANE_SOUNDS")
						iManagePickerAudio ++
						//PRINTSTRING("strain audio")PRINTNL()
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 2
			IF DOES_ENTITY_EXIST(s_crane.obj_spreader)
				IF DOES_ENTITY_EXIST(s_Containers[0].obj_Main)
				AND DOES_ENTITY_EXIST(s_Containers[1].obj_Main)
					IF IS_ENTITY_ATTACHED_TO_ENTITY(s_Containers[0].obj_Main,s_crane.obj_spreader)
					OR IS_ENTITY_ATTACHED_TO_ENTITY(s_Containers[1].obj_Main,s_crane.obj_spreader)
						iManagePickerAudio ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF DOES_ENTITY_EXIST(s_crane.obj_spreader)
				IF DOES_ENTITY_EXIST(s_Containers[0].obj_Main)
				AND DOES_ENTITY_EXIST(s_Containers[1].obj_Main)
					IF NOT IS_ENTITY_ATTACHED_TO_ENTITY(s_Containers[0].obj_Main,s_crane.obj_spreader)
					AND NOT IS_ENTITY_ATTACHED_TO_ENTITY(s_Containers[1].obj_Main,s_crane.obj_spreader)
						PLAY_SOUND_FROM_ENTITY(iCraneStrain, "Strain_No_Container", s_crane.obj_spreader, "CRANE_SOUNDS")
						iManagePickerAudio = 1
						//PRINTSTRING("strain audio")PRINTNL()
					ENDIF
				ENDIF
			ENDIF
		BREAK
	
	ENDSWITCH

ENDPROC



// ================================== START OF MAIN SCRIPT =================================
SCRIPT
	//SET_PROFILING_OF_THIS_SCRIPT(TRUE)
    SET_MISSION_FLAG (TRUE)
    // death arrest check
	IF HAS_FORCE_CLEANUP_OCCURRED()
		Mission_Flow_Mission_Force_Cleanup()
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_DOCKS_PRIME_BOARD_TRANSITION, FALSE)
		b_is_jumping_directly_to_stage = FALSE //If the mission is in the middle of a debug skip cleanup (e.g. waiting for a cutscene to finish) Force cleanup could break.
		MISSION_CLEANUP()
		TERMINATE_THIS_THREAD() //Just in case.
	ENDIF
	
	//set up the Widgets. 
//	#IF IS_DEBUG_BUILD 
//		wgDOCKSSETUP = START_WIDGET_GROUP("DOCKS SETUP CRANE")
//			// Widgets for the Main Mission Controlers
//			START_WIDGET_GROUP(" Main Mission Controlers")
//				ADD_WIDGET_INT_SLIDER("eMissionStage", ieMissionStageTemp, 0, 99, 1)
//				ADD_WIDGET_INT_SLIDER("iSetupProgress", iSetupProgress, 0, 99, 1)
//				ADD_WIDGET_INT_SLIDER("iProgress", iProgress, 0, 99, 1)
//				//ADD_WIDGET_INT_SLIDER("icutsceneprog", icutsceneprog, 0, 99, 1)
//				ADD_WIDGET_INT_SLIDER("iSpeechTextProgress", iSpeechProgress, 0, 99, 1)
//				//ADD_WIDGET_INT_SLIDER("iBuddyProgress", iBuddyProgress, 0, 99, 1)				
//				//ADD_WIDGET_INT_SLIDER("ienemyAIprog", ienemyAIprog, 0, 99, 1)			
//			STOP_WIDGET_GROUP()			
//			START_NEW_WIDGET_COMBO()				
//				ADD_TO_WIDGET_COMBO("SETUP")
//				ADD_TO_WIDGET_COMBO("MISSION_STAGE_1_PROC")
//				ADD_TO_WIDGET_COMBO("MISSION_STAGE_2_PROC")
//				ADD_TO_WIDGET_COMBO("MISSION_STAGE_3_PROC")
//				ADD_TO_WIDGET_COMBO("MISSION_STAGE_4_PROC")
//				ADD_TO_WIDGET_COMBO("MISSION_STAGE_5_PROC")
//				ADD_TO_WIDGET_COMBO("MISSION_STAGE_6_PROC")
//				ADD_TO_WIDGET_COMBO("MISSION_STAGE_7_PROC")
//				ADD_TO_WIDGET_COMBO("MISSION_STAGE_8_PROC")
//				ADD_TO_WIDGET_COMBO("MISSION_STAGE_9_PROC")
//				ADD_TO_WIDGET_COMBO("MISSION_STAGE_10_PROC")
//				ADD_TO_WIDGET_COMBO("MISSION_STAGE_11_PROC")
//				ADD_TO_WIDGET_COMBO("MISSION_STAGE_12_PROC")
//				ADD_TO_WIDGET_COMBO("END_CUT")
//				ADD_TO_WIDGET_COMBO("PASSED")
//				ADD_TO_WIDGET_COMBO("FAIL")
//			STOP_WIDGET_COMBO("Mission Skip",iWidgetMissionSkip)	
//			ADD_WIDGET_BOOL("Jump to point in mission",bJumpToMissionJump)	
//			ADD_WIDGET_BOOL("bmissionstageloaded",bmissionstageloaded)
//			//ADD_WIDGET_BOOL("Override attachments", b_override_crane_attach)
//			//ADD_WIDGET_BOOL("View spreader attachments", b_debug_view_attachments)
//			
//			//ADD_WIDGET_VECTOR_SLIDER("vAttachOffset",vAttachOffset,-10.0,10.0,0.001)
//			//ADD_WIDGET_BOOL("bAttachPallet", bAttachPallet)
//			//ADD_WIDGET_BOOL("Create Crane",bCreateCrane)
//			ADD_WIDGET_VECTOR_SLIDER("Crane",v_crane_pos,-4000.0,4000.0,0.001)
//			ADD_WIDGET_VECTOR_SLIDER("Cabin", s_crane.v_cabin_attach_offset, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("Lift", s_crane.v_lift_attach_offset, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("Spreader", s_crane.v_spreader_attach_offset, -100.0, 100.0, 0.001)
////			ADD_WIDGET_VECTOR_SLIDER("Wheel -1 0", s_crane.v_wheel_offsets[0], -100.0, 100.0, 0.001)
////			ADD_WIDGET_VECTOR_SLIDER("Wheel -1 1", s_crane.v_wheel_offsets[1], -100.0, 100.0, 0.001)
////			ADD_WIDGET_VECTOR_SLIDER("Wheel -2 2", s_crane.v_wheel_offsets[2], -100.0, 100.0, 0.001)
////			ADD_WIDGET_VECTOR_SLIDER("Wheel -2 3", s_crane.v_wheel_offsets[3], -100.0, 100.0, 0.001)
////			ADD_WIDGET_VECTOR_SLIDER("Wheel -3 4", s_crane.v_wheel_offsets[0], -100.0, 100.0, 0.001)
////			ADD_WIDGET_VECTOR_SLIDER("Wheel -3 5", s_crane.v_wheel_offsets[1], -100.0, 100.0, 0.001)
////			ADD_WIDGET_VECTOR_SLIDER("Wheel -4 6", s_crane.v_wheel_offsets[2], -100.0, 100.0, 0.001)
////			ADD_WIDGET_VECTOR_SLIDER("Wheel -4 7", s_crane.v_wheel_offsets[3], -100.0, 100.0, 0.001)
//			//Attachment offsets
//			ADD_WIDGET_VECTOR_SLIDER("vRopeTop1", vRopeTop1, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("vRopeBottom1", vRopeBottom1, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("vRopeTop2", vRopeTop2, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("vRopeBottom2", vRopeBottom2, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("vRopeTop3", vRopeTop3, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("vRopeBottom3", vRopeBottom3, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("vRopeTop4", vRopeTop4, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("vRopeBottom4", vRopeBottom4, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("vRopeTop5", vRopeTop5, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("vRopeBottom5", vRopeBottom5, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("vRopeTop6", vRopeTop6, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("vRopeBottom6", vRopeBottom6, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("vRopeTop7", vRopeTop7, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("vRopeBottom7", vRopeBottom7, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("vRopeTop8", vRopeTop8, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER("vRopeBottom8", vRopeBottom8, -100.0, 100.0, 0.001)
//			// - Other offsets
//			//ADD_WIDGET_VECTOR_SLIDER("Wheel offset", v_debug_wheel_offset, -100.0, 100.0, 0.001)
//			//ADD_WIDGET_VECTOR_SLIDER("Wheel rotation", v_debug_wheel_rotation, -180.0, 180.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("MAX_CABIN_Y",MAX_CABIN_Y,-100,100,0.1)
//			ADD_WIDGET_FLOAT_SLIDER("MIN_CABIN_Y",MIN_CABIN_Y,-100,100,0.1)
//			ADD_WIDGET_FLOAT_SLIDER("MAX_SPREADER_Z",MAX_SPREADER_Z,-100,100,0.1)
//			ADD_WIDGET_FLOAT_SLIDER("MIN_SPREADER_Z",MIN_SPREADER_Z,-100,100,0.1)
//			ADD_WIDGET_FLOAT_SLIDER("MAX_CRANE_OFFSET",MAX_CRANE_OFFSET,-100,100,0.1)
//			ADD_WIDGET_FLOAT_SLIDER("MIN_CRANE_OFFSET",MIN_CRANE_OFFSET,-100,100,0.1)
//			
//			ADD_WIDGET_FLOAT_SLIDER("MAX_CABIN_VEL",MAX_CABIN_VEL,-100,100,0.1)
//			ADD_WIDGET_FLOAT_SLIDER("MAX SPREADER_VEL",MAX_SPREADER_VEL,-100,100,0.1)
//			ADD_WIDGET_FLOAT_SLIDER("MAX CRANE_VEL",MAX_CRANE_VEL,-100,100,0.1)
//			
//			ADD_WIDGET_FLOAT_SLIDER("CABIN_ACCEL",CABIN_ACCEL,-100,100,0.1)
//			ADD_WIDGET_FLOAT_SLIDER("SPREADER_ACCEL",SPREADER_ACCEL,-100,100,0.1)
//			ADD_WIDGET_FLOAT_SLIDER("CRANE_ACCEL",CRANE_ACCEL,-100,100,0.1)
//
//			ADD_WIDGET_VECTOR_SLIDER(" v_spreader_attach_start",  v_spreader_attach_start, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER(" v_spreader_attach_end",  v_spreader_attach_end, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER(" v_spreader_point_end", v_spreader_point_end, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER(" v_cabin_attach_start",  v_cabin_attach_start, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER(" v_cabin_point_start",  v_cabin_point_start, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER(" v_cabin_attach_end",  v_cabin_attach_end, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER(" v_cabin_point_end",  v_cabin_point_end, -100.0, 100.0, 0.001)
//					
//			
//			ADD_WIDGET_VECTOR_SLIDER(" v_attach_to_cam_value",  v_attach_to_cam_value, -100.0, 100.0, 0.001)
//			ADD_WIDGET_VECTOR_SLIDER(" v_crane_attach_cam_rot",  v_crane_attach_cam_rot, -100.0, 100.0, 0.001)
//			ADD_WIDGET_FLOAT_SLIDER("f_crane_cam_fov",f_crane_cam_fov,-100,100,0.1)
//			ADD_WIDGET_FLOAT_SLIDER("f_crane_attach_cam_fov",f_crane_attach_cam_fov,-100,100,0.1)
//		STOP_WIDGET_GROUP()
//		SET_LOCATES_HEADER_WIDGET_GROUP(wgDOCKSSETUP )
//	#ENDIF


	#IF IS_DEBUG_BUILD 
		//SET_CURRENT_WIDGET_GROUP(wgThisMissionsWidget)
		wgDOCKSSETUP = START_WIDGET_GROUP("DOCKS SETUP")
			// Widgets for the Main Mission Controlers
			START_WIDGET_GROUP(" Main Mission Controlers")
				ADD_WIDGET_INT_SLIDER("eMissionStage", ieMissionStageTemp, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iSetupProgress", iSetupProgress, 0, 99, 1)
				ADD_WIDGET_INT_SLIDER("iProgress", iProgress, 0, 99, 1)
				ADD_WIDGET_INT_READ_ONLY("iHandlerSection",iHandlerSection)
				ADD_WIDGET_INT_SLIDER("iMapAngle",iMapAngle,-360, 360, 1)
				ADD_WIDGET_INT_SLIDER("iOverheadMapAngle",iOverheadMapAngle,-360, 360, 1)
				ADD_WIDGET_FLOAT_SLIDER("fXTolerance",fXTolerance,-100,100,0.01)
				ADD_WIDGET_FLOAT_SLIDER("fYTolerance",fYTolerance,-100,100,0.01)
				ADD_WIDGET_VECTOR_SLIDER("vAttach1",vAttach1,-10,10,0.01)
				ADD_WIDGET_VECTOR_SLIDER("vAttach2",vAttach2,-10,10,0.01)
				ADD_WIDGET_VECTOR_SLIDER("vAttachRot",vAttachRot,-360,360,0.01)
				ADD_WIDGET_FLOAT_SLIDER("MAX_CABIN_Y",MAX_CABIN_Y,-100,100,0.1)
				ADD_WIDGET_FLOAT_SLIDER("MIN_CABIN_Y",MIN_CABIN_Y,-100,100,0.1)
				ADD_WIDGET_FLOAT_SLIDER("MAX_SPREADER_Z",MAX_SPREADER_Z,-100,100,0.1)
				ADD_WIDGET_FLOAT_SLIDER("MIN_SPREADER_Z",MIN_SPREADER_Z,-100,100,0.1)
				ADD_WIDGET_BOOL("b1",b1)
				ADD_WIDGET_BOOL("b2",b2)
				ADD_WIDGET_BOOL("b3",b3)
				ADD_WIDGET_BOOL("b4",b4)
				ADD_WIDGET_FLOAT_SLIDER("fVal1",fVal1,0,10,0.1)
				ADD_WIDGET_FLOAT_SLIDER("fVal2",fVal2,0,10,0.1)
				ADD_WIDGET_FLOAT_SLIDER("fVal3",fVal3,0,10,0.1)
				ADD_WIDGET_FLOAT_SLIDER("fVal4",fVal4,0,10,0.1)
				
				ADD_WIDGET_FLOAT_SLIDER("fFallOff",fFallOff,-1000,1000,0.1)
				ADD_WIDGET_FLOAT_SLIDER("fIntensity",fIntensity,-1000,1000,0.1)
				ADD_WIDGET_FLOAT_SLIDER("fInnerAngle",fInnerAngle,-1000,1000,0.1)
				ADD_WIDGET_FLOAT_SLIDER("fOuterAngle", fOuterAngle,-1000,1000,0.1)
				ADD_WIDGET_FLOAT_SLIDER("fExposure",fExposure,-1000,1000,0.1)

				ADD_WIDGET_FLOAT_SLIDER("fIdealDistance",fIdealDistance,-10,10,00.01)
				ADD_WIDGET_FLOAT_SLIDER("fMinDistance",fMinDistance,-10,10,00.01)
				ADD_WIDGET_FLOAT_SLIDER("fMaxDistance",fMaxDistance,-10,10,00.01)
				ADD_WIDGET_FLOAT_SLIDER("fTargerMaxCatchUp",fTargerMaxCatchUp,-10,10,00.01)
				ADD_WIDGET_FLOAT_SLIDER("fThisValue",fThisValue,-1000,1000,0.1)
				ADD_WIDGET_INT_SLIDER("iSpeechTextProgress", iSpeechProgress, 0, 99, 1)
				
				ADD_WIDGET_VECTOR_SLIDER("Crane - v_crane_pos",v_crane_pos,-4000.0,4000.0,0.001)
				ADD_WIDGET_FLOAT_SLIDER("Crane - s_crane.f_crane_offset",s_crane.f_crane_offset,-4000.0,4000.0,0.001)
				ADD_WIDGET_INT_SLIDER("iCraneHelp", iCraneHelp, 0, 99, 1)
			STOP_WIDGET_GROUP()			
			START_NEW_WIDGET_COMBO()				
				ADD_TO_WIDGET_COMBO("SETUP")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_1_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_2_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_3_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_4_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_5_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_6_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_7_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_8_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_9_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_10_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_11_PROC")
				ADD_TO_WIDGET_COMBO("MISSION_STAGE_12_PROC")
				ADD_TO_WIDGET_COMBO("END_CUT")
				ADD_TO_WIDGET_COMBO("PASSED")
				ADD_TO_WIDGET_COMBO("FAIL")
			STOP_WIDGET_COMBO("Mission Skip",iWidgetMissionSkip)	
			ADD_WIDGET_BOOL("Jump to point in mission",bJumpToMissionJump)	
			ADD_WIDGET_BOOL("bmissionstageloaded",bmissionstageloaded)		
		STOP_WIDGET_GROUP()
		SET_LOCATES_HEADER_WIDGET_GROUP(wgDOCKSSETUP)
	#ENDIF
	
    // Mission Loop: Loops forever until mission is passed or failed
    WHILE TRUE
	
        WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ThePortOfLSHeistSetup")
		
		// Blocking replay camera movement when iBlockReplayCameraTimer is set B*2226188
		IF iBlockReplayCameraTimer > GET_GAME_TIMER()
			REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()
		ENDIF
		
		IF brunfailchecks // controls fail checks
			FAIL_CHECKS()
		ENDIF
		
		IF balarmtriggered
			IF HAS_SOUND_FINISHED(iAlarmID)
           		PLAY_SOUND_FRONTEND(iAlarmID,"Generic_Alarm_Electronic_01")
			ENDIF
		ENDIF
		
		IF eMissionStage = MISSION_STAGE_10
			IF iprogress > 5
				IF iprogress != 99
				OR iprogress != 98
					MANAGE_FLOYD_PAIN()
				ENDIF
			ENDIF
		ENDIF
		
		IF eMissionStage = MISSION_STAGE_11
			MANAGE_FLOYD_PAIN()
		ENDIF
		
		IF eMissionStage > MISSION_STAGE_5
			SET_GLOBAL_MIN_BIRD_FLIGHT_HEIGHT(25.3383)
		ENDIF
		
		//MANAGE_PED_BLIPS()
		//Always run dock ambience after stage 
		IF eMissionStage < MISSION_STAGE_10
			MANAGE_DOCKS_AMBIENCE()
			MANAGE_DOCK_WORKER_AMBIENT_DIALOGUE()
		ENDIF
		
		IF eMissionStage > MISSION_STAGE_5
		AND eMissionStage < MISSION_STAGE_10
			MANAGE_CRANE_SOUNDS()
		ENDIF
		
		// controls the dialog
		MANAGE_DIALOGUE()

		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF eMissionStage > MISSION_STAGE_SETUP
			AND eMissionStage < MISSION_STAGE_5
				//SCRIPT_ASSERT("MANAGING SETPIECES")
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),vDocks) < 200
					MANAGE_DOCK_SETPIECES(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				ENDIF
			ELSE
				IF eMissionStage > MISSION_STAGE_SETUP
					IF eMissionStage > MISSION_STAGE_4
					AND s_sp_blackwater_with_dockworker[0].i_event > 7
						IF bSetPiece[SS_STATIC_GANTRY_GUYS] = TRUE
							bSetPiece[SS_STATIC_GANTRY_GUYS] = FALSE
						ENDIF
						
						IF bSetPiece[SS_ARRIVE_AT_DOCKS] = TRUE
							bSetPiece[SS_ARRIVE_AT_DOCKS] = FALSE
						ENDIF
						
						IF bSetPiece[SS_FLOYD_GRABS_PACKAGE] = TRUE
							bSetPiece[SS_FLOYD_GRABS_PACKAGE] = FALSE
						ENDIF
						
						IF bSetPiece[SS_GANTRY_GUYS] = TRUE
							bSetPiece[SS_GANTRY_GUYS] = FALSE
						ENDIF
						
						IF bSetPiece[SS_dock_workers_on_pipe] = TRUE
							bSetPiece[SS_dock_workers_on_pipe] = FALSE
						ENDIF
						
						IF bSetPiece[Ss_dock_workers_talking] = TRUE
							bSetPiece[Ss_dock_workers_talking] = FALSE
						ENDIF
						
						IF bSetPiece[SS_guys_around_car] = TRUE
							bSetPiece[SS_guys_around_car] = FALSE
						ENDIF
							
						REPEAT COUNT_OF(s_sv_car_to_admire) iCount
							IF DOES_ENTITY_EXIST(s_sv_car_to_admire[iCount].veh)
								CLEAN_UP_SETPIECE_VEHILCE(s_sv_car_to_admire[iCount],TRUE,FALSE)
							ENDIF
						ENDREPEAT
						
						REPEAT COUNT_OF(s_sp_dock_workers_on_pipe) iCount
							IF DOES_ENTITY_EXIST(s_sp_dock_workers_on_pipe[iCount].ped)
								CLEAN_UP_SETPIECE_PED(s_sp_dock_workers_on_pipe[iCount],TRUE,FALSE)
							ENDIF
						ENDREPEAT
						
						REPEAT COUNT_OF(s_sp_dock_workers_talking) iCount
							IF DOES_ENTITY_EXIST(s_sp_dock_workers_talking[iCount].ped)
								CLEAN_UP_SETPIECE_PED(s_sp_dock_workers_talking[iCount],TRUE,FALSE)
							ENDIF
						ENDREPEAT
						
						REPEAT COUNT_OF(s_sp_floyd_grabs_package) iCount
							IF DOES_ENTITY_EXIST(s_sp_floyd_grabs_package[iCount].ped)
								CLEAN_UP_SETPIECE_PED(s_sp_floyd_grabs_package[iCount],TRUE,FALSE)
							ENDIF
						ENDREPEAT
						
						REPEAT COUNT_OF(s_sp_guys_around_car) iCount
							IF DOES_ENTITY_EXIST(s_sp_guys_around_car[iCount].ped)
								CLEAN_UP_SETPIECE_PED(s_sp_guys_around_car[iCount],TRUE,FALSE)
							ENDIF
						ENDREPEAT
						
						REPEAT COUNT_OF(s_sp_gantry_guys_dynamic) iCount
							IF DOES_ENTITY_EXIST(s_sp_gantry_guys_dynamic[iCount].ped)
								CLEAN_UP_SETPIECE_PED(s_sp_gantry_guys_dynamic[iCount],TRUE,FALSE)
							ENDIF
						ENDREPEAT
						
						REPEAT COUNT_OF(s_sp_gantry_guys_static) iCount
							IF DOES_ENTITY_EXIST(s_sp_gantry_guys_static[iCount].ped)
								CLEAN_UP_SETPIECE_PED(s_sp_gantry_guys_static[iCount],TRUE,FALSE)
							ENDIF
						ENDREPEAT
					
						REMOVE_ANIM_DICT("missheistdockssetup1ig_2_p1@new_structure")	
						REMOVE_ANIM_DICT("missdocksshowoffcar@idle_a")
						REMOVE_ANIM_DICT("missdocksshowoffcar@idle_b")
						REMOVE_ANIM_DICT("missdocksshowoffcar@base")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		// preloads and then creates assets for the next stage.
		PRE_STREAM_MISSION_STAGE()
		
		MANAGE_DOCKWORKER_PROPS()
		
		//do't do the locates header J skips. 		
		#IF IS_DEBUG_BUILD 
			DONT_DO_J_SKIP(sLocatesData) 
		#ENDIF
		
		IF eMissionStage < MISSION_STAGE_4
			IF g_bDocksBackGatesIgnoreTrevorsUniform = FALSE
				g_bDocksBackGatesIgnoreTrevorsUniform = TRUE
			ENDIF
		ELSE
			IF g_bDocksBackGatesIgnoreTrevorsUniform = TRUE
				REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_DOCKS_BACK_GATE_IN,PLAYER_PED_ID())
				REGISTER_PED_TO_ACTIVATE_AUTOMATIC_DOOR(AUTODOOR_DOCKS_BACK_GATE_OUT,PLAYER_PED_ID())
				g_bDocksBackGatesIgnoreTrevorsUniform = FALSE
			ENDIF
		ENDIF
		
		IF eMissionStage > MISSION_STAGE_8
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<497.625580,-3113.236328,4.570056>>, <<471.186218,-3113.169922,15.352262>>, 13.250000)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_JUMP)
			ENDIF
		ENDIF
		
		IF eMissionStage > MISSION_STAGE_1
		AND eMissionStage < MISSION_STAGE_10
			DISABLE_COMBAT_CONTROLS_THIS_FRAME()
		ENDIF
		
		IF eMissionStage >= MISSION_STAGE_4
			MERRYWEATHER_WITH_DOCK_WORKER()
		ENDIF
		
		// main mission switch
		SWITCH eMissionStage
			CASE MISSION_STAGE_SETUP //controls the loading of assets needed for the appropriate stage
				MISSION_SETUP()
			BREAK
			CASE MISSION_STAGE_OPENING_CUTSCENE //plays the opening cutscene
				OPENING_CUTSCENE()
			BREAK
			CASE MISSION_STAGE_1 //DRIVE
				MISSION_STAGE_1_PROC()
			BREAK
			CASE MISSION_STAGE_2 //PARK
				MISSION_STAGE_2_PROC()
			BREAK
			CASE MISSION_STAGE_4 //FOLLOW FLOYD
				MISSION_STAGE_4_PROC()
			BREAK 
			CASE MISSION_STAGE_5 //HANDLER WORK
				MISSION_STAGE_5_PROC()
			BREAK
			CASE MISSION_STAGE_6 // FOLLOW TO CRANE
				MISSION_STAGE_6_PROC()
			BREAK
			CASE MISSION_STAGE_7 //CRANE
				MISSION_STAGE_7_PROC()
			BREAK
			CASE MISSION_STAGE_8 //CAMERA
				MISSION_STAGE_8_PROC()
			BREAK
			CASE MISSION_STAGE_10 //DRIVE TO OTHER DOCKS
				MISSION_STAGE_10_PROC()
			BREAK
			CASE MISSION_STAGE_11 //DRIVE BACK TO FLOYD'S APARTMENT
				MISSION_STAGE_11_PROC()
			BREAK
			CASE MISSION_STAGE_PASSED //mission completed
				MISSION_PASSED()
			BREAK
			CASE MISSION_STAGE_FAIL  //mission failed
				MISSION_FAILED()
			BREAK
		ENDSWITCH
	
		
 		// debug controls 
		#IF IS_DEBUG_BUILD
			//Tells us where the mission stage is at.
			Where_are_we()
			
            // Checks if the keyboard key S has been pressed and if sets the current mission state to complete
            IF IS_KEYBOARD_KEY_PRESSED (KEY_S)
				PRINT_GOD_TEXT("AW_PASSED")
				eMissionStage = MISSION_STAGE_PASSED
            ENDIF
			
            // Checks if the keyboard key F has been pressed and if so sets the current mission state to failed
            IF IS_KEYBOARD_KEY_PRESSED (KEY_F)
				//PRINT_GOD_TEXT("AW_FAILED")
				reason_for_fail = GENERIC
				eMissionStage = MISSION_STAGE_FAIL
            ENDIF

			//launch the mission skip menu			
			IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, ENUM_TO_INT(eMissionStage), FALSE, "",TRUE,TRUE)

				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-90)
				bcleanup = FALSE
				iprogress = 99
				bskipping = TRUE

				IF iReturnStage < 1
					iReturnStage = 1
				ENDIF
				
				IF iReturnStage > ENUM_TO_INT(MISSION_STAGE_PASSED)  OR iReturnStage > ENUM_TO_INT(MISSION_STAGE_10)
					iReturnStage = ENUM_TO_INT(MISSION_STAGE_PASSED)
				ENDIF
				
				IF iReturnStage = 1
					SET_PLAYER_START_POSITION(eMissionStage)
				ENDIF
				
				IF iReturnStage > 11
					MISSION_PASSED()
				ENDIF

				eMissionStage = INT_TO_ENUM(MISSION_STAGE_ENUM, iReturnStage)
			ENDIF
			
			WHILE bskipping = TRUE  //Load new stage if skipping
				IF iReturnStage > ENUM_TO_INT(MISSION_STAGE_11)
					CONTROL_FADE_IN(500)
					bskipping = FALSE
				ELSE
					WAIT(0)
					LOAD_MISSION_STAGE(eMissionStage)
				ENDIF
			ENDWHILE

			//For the widget thing
			ieMissionStageTemp = ENUM_TO_INT(eMissionStage)
		#ENDIF
    ENDWHILE
ENDSCRIPT
