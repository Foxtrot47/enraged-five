// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	docks_heist_2a.sc
//		AUTHOR			:	Craig Vincent / Adam Westwood / Ryan Pendant
//		DESCRIPTION		:	Trevor has decided the best way to steal goods is by blowing up 
//							the ship.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "commands_recording.sch"

USING "script_blips.sch"
Using "Locates_public.sch"
Using "select_mission_stage.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"
USING "chase_hint_cam.sch"
using "mission_stat_public.sch"
using "cutscene_public.sch"
USING "CompletionPercentage_public.sch"
USING "taxi_functions.sch"
USING "script_heist.sch"
USING "replay_public.sch"
using "building_control_public.sch"
USING "timeLapse.sch"
using "asset_management_public.sch"
USING "heist_end_screen.sch"
USING "achievement_public.sch"
USING "spline_cam_edit.sch"
USING "tv_control_public.sch"
USING "push_in_public.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

//-----------------------------------------------------------------------------------------------------------
//		ENUMS
//-----------------------------------------------------------------------------------------------------------
ENUM MMF_MISSION_MERC_FLAGS	
	//msf_1
	merc1_start,
	merc2_start,
	merc3_C4_2,
	
	//msf_3
	merc4_C4_2,
	merc5_C4_2,
	merc6_C4_2,
	merc7_C4_2,
	merc8_C4_2,
	
	//msf_4
	merc9_C4_3,
	merc10_C4_3,
	merc11_C4_3,
	merc12_C4_3,
	merc13_C4_3,
	merc14_C4_3,
	MERC15_C4_3,
	//msf_5
	MERC16_C4_4,
	merc17_c4_4,
	merc18_c4_4,
	
	//msf_6	
	merc19_car1,
	merc20_car1,
	merc21_car1,
	merc22_car1,	
		
	//msf_6	
	merc23_car2,
	merc24_car2,
	merc25_car2,
	merc26_car2,
	
	merc27_cars,	
	merc28_cars,
	
	merc29_heli,
			
	MMF_NUM_OF_MERCS
ENDENUM
ENUM MERC_SITUATION_ENUM
	MS_SIT_NONE,
	ms_sit_bullet_ping,
	MS_SIT_SHOT_HEARD,
	MS_sit_MIKE_SEEN,
	MS_SIT_DEAD_BODY,
	MS_SIT_ATTACKED
ENDENUM
ENUM MERC_ACTION_ENUM
	GA_FIND_COVER,
	ga_aim_at_sniper,
	GA_INVESTIGATE_FRANK,
	GA_INVESTIGATE_BODY
ENDENUM
ENUM MPF_MISSION_PED_FLAGS
	mpf_frank,
	mpf_mike,
	mpf_trev,	
	mpf_heli,
	
	mpf_dock1,
	mpf_dock2,
	
	mpf_floyd,
	
	MPF_NUM_OF_PEDS
ENDENUM
ENUM MVF_MISSION_VEHICLE_FLAGS
	mvf_mission_veh,
	mvf_merc1,
	mvf_merc2,
	mvf_sub,
	mvf_heli,
	mvf_getAwayVan,
	mvf_dinghy,
	mvf_parked1,
	mvf_parked2,
	mvf_policeHeli,
	
	MVF_NUM_OF_VEH
ENDENUM
ENUM MSF_MISSION_STAGE_FLAGS
	
	msf_0_drive_to_bridge,	
	msf_1_snipe_gaurds,
	msf_2_first_bomb,
	msf_3_second_bomb,
	msf_4_third_bomb,	
	msf_5_fourth_bomb,
	msf_6_way_out,
	msf_7_detonate,
	msf_8_explosion_ctscne,	
	msf_9_the_goods,	
	msf_10_end_cutscene,
//	msf_passed,
	
	CST_INIT,
	CST_MCS_1,
	CST_MCS_2,
	CST_MCS_3,
	CST_EXT,
	
	MSF_NUM_OF_STAGES
ENDENUM
Enum C4_FLAGS
	C4_0_front_of_ship,
	C4_1_first_mast,
	C4_2_second_mast,
	C4_3_inside,
	
	C4_NUM_OF_BOMBS
endenum
ENUM MFF_MISSION_FAIL_FLAGS
	mff_debug_fail,
	mff_default,
	mff_franklin_dead,
	mff_michael_dead,
	mff_trevor_dead,
	
	mff_left_trev,
	mff_left_Michael,
	mff_out_pos,
	mff_TREV_LEFT,
	
	mff_left_docks,
	mff_frank_car_des,
	mff_sub_des,
	mff_sub_stuck,
	mff_spotted,
	mff_area,
	mff_ammo,
	mff_ammob,
	mff_dinghy_des,
	mff_dinghy_stuck,
	mff_police,
	mff_bombs_exploded,
	
	MFF_NUM_OF_FAILS
ENDENUM
Enum STAGE_SWITCHSTATE
	STAGESWITCH_IDLE,
	STAGESWITCH_REQUESTED,
	STAGESWITCH_EXITING,
	STAGESWITCH_ENTERING	
ENDENUM
// Used with asset management system
ENUM ASSET_TYPE_FLAG
	atf_empty = 0,
	atf_model,
	atf_waypoint,
	atf_anim_dict,
	atf_scaleform_mov,
	atf_veh_rec
ENDENUM	
// Hot swap status
enum SWITCHSTATE
	SWITCH_NOSTATE,
	SWITCH_COMPLETE,
	SWITCH_IN_PROGRESS
ENDENUM
enum STEALTH_STAGE
	FIRST_BOMB,
	SECOND_BOMB,
	THIRD_BOMB,
	FORTH_BOMB
endenum
enum BOAT_SECTION
	BOAT_FRONT,
	BOAT_MID1,
	BOAT_MID2,
	BOAT_BACK
endenum

//-----------------------------------------------------------------------------------------------------------
//		STRUCTS
//-----------------------------------------------------------------------------------------------------------
STRUCT VEHICLE_STRUCT
	VEHICLE_INDEX 		id
	BLIP_INDEX			blip
endstruct
Struct PEDS_STRUCT
	PED_INDEX				id				//Ped Index
	BLIP_INDEX				blip			//Blip Index
	VECTOR					startloc		//Ped start location
	FLOAT					head			//Ped start heading	
	MODEL_NAMES				model			//Ped model	
	COVERPOINT_INDEX    	cov				//Ped coverpoint index
	VECTOR              	vcov			//Ped goto vector
	INT              		iEnemyProgress	//Ped progressswitching INT
	PED_COMP_NAME_ENUM		ePedCompPropHead,ePedCompPropEyes, ePedCompSpecial	//prop and special clothing, for storing Trevor's
	
ENDSTRUCT
struct MERC_STRUCT
	PED_INDEX				id				//Ped Index
	BLIP_INDEX				blip			//Blip Index
	COVERPOINT_INDEX    	cov				//Ped coverpoint index 
	VECTOR					startloc		//Ped start location
	VECTOR              	vdef1			//Ped goto vector bow of the ship
	VECTOR					vdef2			//ped goto vector container section
	VECTOR					vdef3			//ped goto vector second mast
	VECTOR					vdef4			//ped goto vector bridge area
	VECTOR					vdefFrank		//ped goto vector if combat mike
	FLOAT					head			//Ped start heading
	
	WEAPON_TYPE      	   	wpn				//Ped Weapon type
	MODEL_NAMES				model			//Ped model
	COMBAT_MOVEMENT			move			//Ped movement type
	COMBAT_RANGE			range			//Ped combat range
	COMBAT_ABILITY_LEVEL	ability			//Ped combat ability
	MERC_SITUATION_ENUM		currentsit		//ped sit
	MERC_ACTION_ENUM		currentaction	//action
	int 					iRevertActionTime
	
	int						iAdvanceTimer = 0	//@RJP used to time mercs from cars advancing
	int						iAdvanceState = 0	//@RJP state for tracking merc advancing
	int						iAdvanceTime1		//@RJP time for ped to wait before advancing
	int						iAdvanceTime2		//@RJP time for ped to wait before advancing the second time
	int						iDialogueTimer
	
	bool					bzombie
	bool					bstats 	= false
	bool					balive = true
	bool					bincombat = false
	bool					bbulletmiss = false
	
	bool					bStealthKill = false
	bool					bAlertSpeech = false
	
	//@RJP track if this ped has seen a dead body
	BOOL					bSpottedDead = FALSE
	//@RJP track if we should display a print help saying to kill this merc
	BOOL					bPrintHelp
endstruct
STRUCT BOMB_STRUCT // contains details for the bombs

   OBJECT_INDEX               obj    
   BLIP_INDEX                 blip
   VECTOR                     ExpPos
   BOOL                       bInArea
   BOOL                       bPlanted
       
ENDSTRUCT

// Used with the asset laoding system to describe and asset and mark whether it is loaded or not
STRUCT LOADING_ASSET
	asset_type_flag		assetType
	BOOL				bRequested
	BOOL				bLoaded
	BOOL				bCleanup
	MODEL_NAMES			modelName
	STRING				strAssetName
	INT					iRecNo
	SCALEFORM_INDEX		sfi_mov
ENDSTRUCT

STRUCT PED_HEATSCALE_STRUCT
	INT iHeatScale = 0
	INT	iHeatState = 0
ENDSTRUCT

//-----------------------------------------------------------------------------------------------------------
//		CONSTANTS
//-----------------------------------------------------------------------------------------------------------
CONST_INT				STAGE_ENTRY			0
CONST_INT				STAGE_EXIT			-1 

CONST_INT				CAR_MERC_ACCURACY	4

CONST_INT				WHICH_PLAYER		0
CONST_INT				PLAYER_IS_FRANK		0
CONST_INT				PLAYER_IS_MIKE		1

//scanning for crate
CONST_FLOAT FIND_RANGE		3.0 
CONST_FLOAT CLOSE_RANGE		6.5 


CONST_FLOAT 			SONAR_MIN_SIZE_SWIM			 10.0	// Minimum sonar blip size when swimming
CONST_FLOAT 			SONAR_UPDATE_SPEED			 0.025	// Increase this to make the sonar update faster

//-----------------------------------------------------------------------------------------------------------
//		VARIABLES
//-----------------------------------------------------------------------------------------------------------

VEHICLE_STRUCT				Vehs[MVF_NUM_OF_VEH]	//holds all of the vehicles for tis level
PEDS_STRUCT					peds[MPF_NUM_OF_PEDS]	//holds all of the peds for this level
MERC_STRUCT					mercs[MMF_NUM_OF_MERCS]	//holds all of the mercenarys for this level
BOMB_STRUCT					C4[C4_NUM_OF_BOMBS]		//stores the bombs
PED_HEATSCALE_STRUCT 		sPedHeatScale[7]		//used to fade in heatscale on peds during way out stage

structPedsForConversation 	convo_struct			//holds peds in the conversation
LOCATES_HEADER_DATA 		sLocatesData
PUSH_IN_DATA				sPushInData

BLIP_INDEX					blip_playerCar			//@RJP blip for Franklin's car at the start
BLIP_INDEX					blip_objective			//blip for mission objective
BLIP_INDEX					blip_checksOb			//blip for outside of flow
Camera_index				cameraIndex				//in game cut scene camera
//Camera_index				camSwitch

SEQUENCE_INDEX				seq						//used to create AI sequence

COVERPOINT_INDEX			customCover				//used for created cover points 
INTERIOR_INSTANCE_INDEX 	interior_living_room	// interiors
STREAMVOL_ID 				volShip

COVERPOINT_INDEX			scriptCover[3]			//scripted cover points

//@RJP health pickups
PICKUP_INDEX				puiHealthPack[1]		//health packs

blip_index					blipCrate
OBJECT_INDEX				objSofa					//shitty sofa to hide during cutscene
OBJECT_INDEX				objLoot					//loot / cov
object_index				objcov1					//crate pile
object_index				objcov2					//crate mil small 
object_index				objcov3					//crate mil large
object_index				objcov4					//@RJP adding cover box for ped next to trailer
object_index				objcov5					//@RJP adding cover box for ped next to trailer
OBJECT_INDEX				objDoor					//door into ship, P_PO1_01_DoorM_S
OBJECT_INDEX				objDoorHide				//door into ship in scene to hide, PORT_XR_DOOR_05
OBJECT_INDEX				objBomb[3]				//bombs placed when playing from a checkpoint
INT							iDoorSyncScene
//INT 						iDoorCamTime

ENTITY_INDEX				objSubHook

BOOL						bScubaOutfitSet

// New Sonar Related stuff
BLIP_INDEX					biSonarBlip				//Sonar blip
FLOAT						fCurrentRadius
FLOAT						fTargetRadius
FLOAT						fSonarIncrement
BOOL						bSonarUpdate = TRUE
BOOL						bStartSonar = FALSE

INT							iStealthKillSyncScene	//sync scene for stealth killing anims

//------------------------------ RELATIONSHIPS ------------------------------
REL_GROUP_HASH 			rel_group_buddy
REL_GROUP_HASH 			rel_group_enemies

//------------------------------ Mission variables ------------------------------
//INT						i
INT 					iCSDelay
INT						iCombatTimer
bool					bGotWeapons = false
INT 					players_scene_id = -1
camera_index camera_a
camera_index camera_b
OBJECT_INDEX objFlare
PTFX_ID flare_ptfx

//------------------------------- Music Scores ------------------------------
bool					bdouble_music_cue
bool					bCamPan_Cued
bool					bCamPan2_Cued
INT 					iFlyOverMusicTrigger
//------------------------------ stealth stuff ------------------------------
INT 					iMike_AI_stage
INT						istealthkill
INT						istealthkillTimer
INT						iStealthTimer
BOOL					bMikeStealthDiag = false
BOOL					bMikeHealthWarn = false
BOOL					bMikeCombatWarn = false

//float 					fAI_distance
vector 					vAI_coverpoint
float 					fAI_coverheading

bool					bAlertedMerc		= false
bool					bKilledByBuddy 		= false

//------------------------------ cutscene stuff ------------------------------
INT						icutstage
BOOL 					b_cutscene_loaded
bool					bskip_cut 	= false
bool					bcs_trev 	= false
bool					bcs_cam		= false
bool					bcs_sub		= false
bool					bcs_hook	= false
bool					bSubHandler	= false

bool					bSwitchOccluders	= false
bool					bSkipDetonation		= false
bool					bCanSkip			= false
bool					bSwappedToFranklin	= false
bool					bSwappedToTrevor	= false					

//------------------------------ print / dialogue stuff ------------------------------
bool					bDialoguePlayed		= false
bool					bDialoguePlayed2	= false
bool					bDialoguePlayed3	= false
bool					bDialoguePlayed4	= false
bool					bPrintDisplayed 	= false
bool					bPrintDisplayed2 	= false
bool					bHelpDisplayed		= false
bool					bPrintLft_Mike		= false
bool					bPrintLft_trev		= false
bool					bPrintMikeLeftArea 	= false	
bool					bprinttrevleftarea 	= false
bool					bpauseconv 			= false
bool					bSonarHelp			= false
bool					bPrintProtect		= false
bool					bFrankSaysMove		= false

bool					bMikeAsksForHelp	= false

bool					bVantageText		= false
bool					bSternObjDisplayed	= false

int						iCarMercDiag		= 0

int						iWayOutDiag			= 0

INT						iCarConv			= 0
INT						iImpatientTimer		= 0

INT						iPoliceHeliState	= 0

INT						iHeliConv			= 0
BOOL					bHeliConv			= FALSE
//------------------------------ alarm stuff ------------------------------
bool					bcsalarmtriggered 	= false
bool					balarmtriggered		= false
bool					bAlarmInitialised	= false
bool 					bInitialiseDelayALR = false
bool					bAlertedEarly		= false
INT						iAlertedEarlyTimer

//------------------------------ Alarm gone off variables ------------------------------
int 					isnipedelay 						//sniper support int
//INT 					iRespawntimer
//bool					bQuickHelp
int 					iescapeStage
INT 					iBomb2Spawned = 0
INT 					iBomb3Spawned = 0
INT 					iBomb4Spawned = 0
MMF_MISSION_MERC_FLAGS	mercsBombArea2[2]
MMF_MISSION_MERC_FLAGS	mercsBombArea3[3]
MMF_MISSION_MERC_FLAGS	mercsBombArea4[3]
//------------------------------ ties in with stealthy kill active ------------------------------
bool					bShotReady 			= false
bool					bSnipeStageCheck 	= false
bool 					bincombat			= false 
int						idialogueDelay
int 					idialoguekillswitch 

//------------------------------ mercs stuff ------------------------------
//bool					bAllMercsDead		= false			
string					sDuckAnim = "alert_gunshot"
int 					ireactionlength
//int 					randoffset 
//bool					bCarCombatStarted	= false
bool					bAimIntro			= false
bool					bAimAtFranklin		= false
PED_INDEX				piAimAtFranklin		= NULL
BOOL 					bMercSpotterTasked	= FALSE
VECTOR 					vDeadMerc
INT 					iMercVeh1State
INT 					iMercVeh2State
INT 					iMercHeliState
INT						iMercHeliTime
bool					bRaisedAlarm		= false
bool					bFrankKilled13		= false
bool					bFrankKilled14		= false

INT						i_sound_heli_attack = -1
BOOL					bHeliAttackSoundsOn = FALSE

//------------------------------ TODS ------------------------------
//time of day variables 
structTimelapse		sTimelapse


//------------------------------ first aim ------------------------------
BOOL					bfirstaim				= false
//------------------------------ Sniper stuff ------------------------------
bool					bAudio_SnipeLoaded		= false
bool					bFrankFired 			= false
int						iFrankFiredTimer
int 					iaudioswitch_snipeStage		
ped_index				franks_target_ped
vector					vfranks_aim_coord
bool					b_nightvision_active
bool					b_sniper_scope_active
bool					bFilledSpecialMeter		= false
//int						iFranklinCamSwapTimer	= -1
//------------------------------ Underwater sounds ------------------------------
INT 		sound_underwater_ambience		= GET_SOUND_ID()
int 		sound_underwater_rebreather		= GET_SOUND_ID()
INT 		i_sound_scuba_gear				= GET_SOUND_ID()
INT 		i_sound_swishing 				= GET_SOUND_ID()
//bool 		bUnderWaterStreamed				= false
BOOL		bStopRebreather					= FALSE

BOOL		bAttachSoundPlayed				= false
INT			i_sound_attach_cargo			= GET_SOUND_ID()

//------------------------------ detector stuff ------------------------------
INT 		ibeepID					= GET_SOUND_ID()

//-------------------------------corpse handler stuff ------------------------
INT iCorpseHandlerState = 0
INT iCorpseTimer = 0

//------------------------------ weapon type ------------------------------
WEAPON_TYPE 			wtEnemyWpn 		= WEAPONTYPE_ASSAULTRIFLE
WEAPON_TYPE 			wtSniper 		= WEAPONTYPE_SNIPERRIFLE 
WEAPON_TYPE				wtBomb			= WEAPONTYPE_STICKYBOMB
weapon_type				wtCombatPistol 	= WEAPONTYPE_COMBATPISTOL
//@RJP weapon type that Michael should have when sneaking during bomb planting stages
WEAPON_TYPE				wtSneakingWeapon = WEAPONTYPE_COMBATPISTOL
//Stage Management and skip stuff
STAGE_SWITCHSTATE		stageswitch				//current switching status
INT						mission_stage			//current stage
INT						mission_substage		//current substage
INT						requestedStage			//the mission stage requested by a mission_stage switch
INT						iStageTimer				//timer used for debug
Bool					bDoSkip					//trigger the skip
INT						iSkipToStage			//the stage to skip to
int 					iMidReplay
bool					bis_zSkip

INT						iStairNavMeshBlocker[2]

//hot swap stuff
SELECTOR_PED_STRUCT			sSelectorPeds						//struct to hold the peds for hotswap		
SELECTOR_CAM_STRUCT 		sCamDetails							//camera for hotswap
bool						bHotSwap	= false   				//Toggle HotSwap
bool						bIncutscene = false
bool						bforced		= false
int iInterpBackToGameOverRide = -1
int iInterpToSwitchCam 		  = 1000

FLOAT 	fMass 				= 500
FLOAT	fGravity 			= 0.2
FLOAT	fBuoyancy			= -1.0
VECTOR 	vSecondEntityOffset = << 0.0, -2.2, -2.9 >>
VECTOR	vFirstEntityOffset	= << 0.0, 0.00, 0.00 >>
VECTOR	vRotation			= << 0.0, 0.0, -90.0 >>

BOOL	bPushInAttached			= TRUE
BOOL	bPushInAttachStartCam	= FALSE
FLOAT	fPushInStartPhase 		= 0.787
FLOAT	fPushInStartDistance 	= 1.0
INT		iPushInInterpTime		= 1000
INT		iPushInCutTime			= 600
INT		iPushInFXTime			= 300
INT		iPushInSpeedUpTime		= 450
FLOAT	fPushInSpeedUpProportion= 0.250
BOOL	bPushInDoColourFlash	= TRUE

FLOAT	fFirstPersonFlashPhase1	= 0.650
BOOL	bFirstPersonFlashPlayed

//============================== streaming ============================== 
ASSET_MANAGEMENT_DATA	sAssetData
CUTSCENE_PED_VARIATION 	sCutscenePedVariationRegister[MPF_NUM_OF_PEDS]
//----------------

RAYFIRE_INDEX 			rayfire_ship
BOOL					bForcedRayfire

#if IS_DEBUG_BUILD
	MissionStageMenuTextStruct zMenuNames[MSF_NUM_OF_STAGES]	
	WIDGET_GROUP_ID widget_debug
#endif

// ===========================================================================================================
//		Trigger boxes
// ===========================================================================================================
// @RJP - This is a structure that's designed to give a name to angled areas. 
// I find it difficult to look at an angled-area check, and know where that is, and what its purpose is.
// With this, you can create a "Trigger Box" variable with a name, and you can easily tell by the name, the purpose of it.
STRUCT TRIGGER_BOX
      VECTOR            vMin
      VECTOR            vMax
      FLOAT       		flWidth
ENDSTRUCT

//Enum of trigger boxes
ENUM TRIGGER_BOXES
	TB_FRANKLIN_KILL_MERC4,
	TB_FRANKLIN_KILL_MERC12,
	TB_FRANKLIN_KILL_MERC13,
	
	TB_MIKE_WAIT_FOR_MERC3_TO_MOVE,
	TB_MIKE_WAIT_FOR_MERC5_TO_MOVE,
	TB_MIKE_WAIT_FOR_MERC9_TO_MOVE,
	TB_MIKE_WAIT_FOR_MERC10_TO_MOVE,
	TB_MIKE_WAIT_FOR_MERC14_TO_MOVE,
	
	TB_BOAT_BOW_UPPER_AREA,
	TB_BOAT_BOW_CONTAINERS_AREA,
	TB_BOAT_FIRST_MAST_AREA,
	TB_BOAT_MID_CONTAINERS_1_AREA,
	TB_BOAT_MID_CONTAINERS_2_AREA,
	TB_BOAT_MID_CONTAINERS_3_AREA,
	TB_BOAT_SECOND_MAST_AREA,
	TB_BOAT_STERN_CONTAINERS_AREA,
	TB_BOAT_STERN_AREA,
	
	TB_REMOVE_3_PERCEPTION,
	TB_REMOVE_9_11_PERCEPTION,
	TB_REMOVE_11_PERCEPTION,
	TB_REMOVE_14_PERCEPTION,
	
	TB_NUM_TRIGGER_BOXES
ENDENUM

TRIGGER_BOX sTriggerBox[TB_NUM_TRIGGER_BOXES]

FUNC TRIGGER_BOX CREATE_TRIGGER_BOX(VECTOR vMin, VECTOR vMax, FLOAT flWidth)
      TRIGGER_BOX tbRetVal
      
      tbRetVal.vMin = vMin
      tbRetVal.vMax = vMax
      tbRetVal.flWidth = flWidth

      RETURN tbRetVal
ENDFUNC

PROC CREATE_SCRIPTED_COVER()
	//scripted cover
	scriptCover[0] = ADD_COVER_POINT(<<-184.0900, -2374.0142, 8.3191>>, 90.0, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180 )
	scriptCover[1] = ADD_COVER_POINT(<<-127.6602, -2374.7002, 8.3191>>, 0.0, COVUSE_WALLTORIGHT, COVHEIGHT_TOOHIGH, COVARC_180 )
	scriptCover[2] = ADD_COVER_POINT(<<-120.2045, -2378.9592, 8.3191>>, 90.0, COVUSE_WALLTOLEFT, COVHEIGHT_TOOHIGH, COVARC_180 )
ENDPROC

PROC CREATE_TRIGGER_BOXES()
	sTriggerBox[TB_FRANKLIN_KILL_MERC4] = CREATE_TRIGGER_BOX(<<-106.694305,-2375.552246,7.319069>>, <<-132.539337,-2375.650635,11.371798>>, 15.000000)
	sTriggerBox[TB_FRANKLIN_KILL_MERC12] = CREATE_TRIGGER_BOX(<<-186.147293,-2379.649902,7.319110>>, <<-185.926453,-2356.103271,13.425454>>, 23.000000)
	sTriggerBox[TB_FRANKLIN_KILL_MERC13] = CREATE_TRIGGER_BOX(<<-209.596725,-2380.277100,7.483111>>, <<-209.650925,-2369.061279,13.319110>>, 30.000000)
	
	sTriggerBox[TB_MIKE_WAIT_FOR_MERC3_TO_MOVE] = CREATE_TRIGGER_BOX(<<-115.710098,-2366.509277,7.345467>>, <<-97.801826,-2366.509277,11.462720>>, 32.000000)
	sTriggerBox[TB_MIKE_WAIT_FOR_MERC5_TO_MOVE] = CREATE_TRIGGER_BOX(<<-130.259781,-2352.182373,7.319069>>, <<-130.259781,-2379.292236,11.319069>>, 6.000000)
	sTriggerBox[TB_MIKE_WAIT_FOR_MERC9_TO_MOVE] = CREATE_TRIGGER_BOX(<<-149.013351,-2354.990723,7.367878>>, <<-149.013351,-2379.868896,11.425476>>, 12.000000)
	sTriggerBox[TB_MIKE_WAIT_FOR_MERC10_TO_MOVE] = CREATE_TRIGGER_BOX(<<-165.328262,-2355.013428,7.355578>>, <<-165.328262,-2379.838379,11.425476>>, 12.000000)
	sTriggerBox[TB_MIKE_WAIT_FOR_MERC14_TO_MOVE] = CREATE_TRIGGER_BOX(<<-194.258270,-2354.992676,7.378938>>, <<-194.248489,-2379.864746,11.425454>>, 13.000000)
	
	sTriggerBox[TB_BOAT_BOW_UPPER_AREA] = CREATE_TRIGGER_BOX(<<-97.801826,-2366.023193,11.338053>>, <<-77.093521,-2366.023193,18.304646>>, 28.000000)
	sTriggerBox[TB_BOAT_BOW_CONTAINERS_AREA] = CREATE_TRIGGER_BOX(<<-123.056564,-2366.157471,4.319066>>, <<-97.838348,-2366.157471,18.320217>>, 34.000000)
	sTriggerBox[TB_BOAT_FIRST_MAST_AREA] = CREATE_TRIGGER_BOX(<<-132.020584,-2366.692383,4.319069>>, <<-120.831322,-2366.692383,22.319069>>, 34.000000)
	sTriggerBox[TB_BOAT_MID_CONTAINERS_1_AREA] = CREATE_TRIGGER_BOX(<<-148.610519,-2366.055664,4.319069>>, <<-129.090439,-2366.055664,24.319069>>, 34.000000)
	sTriggerBox[TB_BOAT_MID_CONTAINERS_2_AREA] = CREATE_TRIGGER_BOX(<<-164.543167,-2366.324561,4.319069>>, <<-145.383041,-2366.324561,24.319069>>, 34.000000)
	sTriggerBox[TB_BOAT_MID_CONTAINERS_3_AREA] = CREATE_TRIGGER_BOX(<<-182.173538,-2366.210449,4.319110>>, <<-161.555466,-2366.210449,24.319069>>, 34.000000)
	sTriggerBox[TB_BOAT_SECOND_MAST_AREA] = CREATE_TRIGGER_BOX(<<-192.630936,-2366.276611,4.319111>>, <<-179.957703,-2366.276611,24.319069>>, 34.000000)
	sTriggerBox[TB_BOAT_STERN_CONTAINERS_AREA] = CREATE_TRIGGER_BOX(<<-208.496033,-2366.257324,4.319110>>, <<-190.488571,-2366.257324,24.319111>>, 34.000000)
	sTriggerBox[TB_BOAT_STERN_AREA] = CREATE_TRIGGER_BOX(<<-252.817169,-2366.276855,4.458529>>, <<-207.978333,-2366.276855,31.319111>>, 34.000000)
	
	sTriggerBox[TB_REMOVE_3_PERCEPTION] = CREATE_TRIGGER_BOX(<<-100.555099,-2373.169678,7.361284>>, <<-100.727997,-2354.543945,12.599417>>, 7.000000)
	sTriggerBox[TB_REMOVE_9_11_PERCEPTION] = CREATE_TRIGGER_BOX(<<-163.841309,-2351.967041,7.340090>>, <<-163.703033,-2368.716553,10.319068>>, 5.000000)
	sTriggerBox[TB_REMOVE_11_PERCEPTION] = CREATE_TRIGGER_BOX(<<-178.010101,-2376.210449,7.319069>>, <<-181.661316,-2352.646729,10.319069>>, 6.000000)
	sTriggerBox[TB_REMOVE_14_PERCEPTION] = CREATE_TRIGGER_BOX(<<-209.044952,-2374.887939,7.319207>>, <<-209.295425,-2352.120117,10.355095>>, 6.000000)
ENDPROC

FUNC BOOL IS_ENTITY_IN_TRIGGER_BOX(ENTITY_INDEX ped, TRIGGER_BOX trigger)
      IF DOES_ENTITY_EXIST(ped) AND NOT IS_ENTITY_DEAD(ped)
            IF IS_ENTITY_IN_ANGLED_AREA(ped, trigger.vMin, trigger.vMax, trigger.flWidth)
                  RETURN TRUE
            ENDIF
      ENDIF
      RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_IN_TRIGGER_BOX(TRIGGER_BOX trigger)
      RETURN IS_ENTITY_IN_TRIGGER_BOX(PLAYER_PED_ID(), trigger)
ENDFUNC



// ===========================================================================================================
//		Termination
// ===========================================================================================================
proc LOAD_asset_stage(MSF_MISSION_STAGE_FLAGS loadStage)	
	SWITCH loadStage
		case msf_0_drive_to_bridge				
			
		break
		
		case msf_1_snipe_gaurds
//			load_asset_model(sAssetData,s_m_y_dockwork_01)	
			load_asset_model(sAssetData,landstalker)
			Load_Asset_AnimDict(sAssetData,"missheistdocks2aswitchig_7")
			load_asset_animdict(sAssetData,"missheistdocks2a")
			load_asset_animdict(sAssetData,"missheistdocks2aig_1")
			load_asset_animdict(sAssetData,"missheistdocks2a@alert")	
			load_asset_animdict(sAssetData,"missheistdocks2a@crouch")
			load_asset_model(sAssetData,s_m_y_blackops_01)
			load_asset_model(sAssetData,prop_mil_crate_02)
			load_asset_model(sAssetData,prop_cratepile_07a)	
			load_asset_model(sAssetData,dinghy)
			Load_Asset_Weapon_Asset(sAssetData, wtSniper, WRF_REQUEST_ALL_ANIMS, WEAPON_COMPONENT_SCOPE|WEAPON_COMPONENT_SCLIP2)
		break
		
		case msf_2_first_bomb				
			load_asset_animdict(sAssetData,"missheistdocks2a")
			load_asset_model(sAssetData,landstalker)
			load_asset_animdict(sAssetData,"missheistdocks2a@alert")
			load_asset_animdict(sAssetData,"missheistdocks2a@crouch")
			load_asset_model(sAssetData,s_m_y_blackops_01)
			load_asset_model(sAssetData,prop_mil_crate_02)
			load_asset_model(sAssetData,prop_cratepile_07a)
			load_asset_model(sAssetData,dinghy)
			Load_Asset_Weapon_Asset(sAssetData, wtSniper, WRF_REQUEST_ALL_ANIMS, WEAPON_COMPONENT_SCOPE|WEAPON_COMPONENT_SCLIP2)
		break
		
		case msf_3_second_bomb					
			load_asset_model(sAssetData,s_m_y_blackops_01)
			load_asset_model(sAssetData,landstalker)
			load_asset_animdict(sAssetData,"missheistdocks2a")
			load_asset_animdict(sAssetData,"missheistdocks2a@alert")
			load_asset_animdict(sAssetData,"missheistdocks2a@crouch")
			load_asset_model(sAssetData,prop_mil_crate_02)
			load_asset_model(sAssetData,prop_cratepile_07a)
			load_asset_model(sAssetData,dinghy)
			Load_Asset_Weapon_Asset(sAssetData, wtSniper, WRF_REQUEST_ALL_ANIMS, WEAPON_COMPONENT_SCOPE|WEAPON_COMPONENT_SCLIP2)
		break
		
		case msf_4_third_bomb
			load_asset_model(sAssetData,s_m_y_blackops_01)
			load_asset_model(sAssetData,landstalker)
			load_asset_animdict(sAssetData,"missheistdocks2a")
			load_asset_animdict(sAssetData,"missheistdocks2a@alert")
			load_asset_animdict(sAssetData,"missheistdocks2a@crouch")
			load_asset_model(sAssetData,prop_mil_crate_02)
			load_asset_model(sAssetData,prop_cratepile_07a)
			load_asset_model(sAssetData,dinghy)
			Load_Asset_Weapon_Asset(sAssetData, wtSniper, WRF_REQUEST_ALL_ANIMS, WEAPON_COMPONENT_SCOPE|WEAPON_COMPONENT_SCLIP2)
		break
		
		case msf_5_fourth_bomb		
			load_asset_model(sAssetData,s_m_y_blackops_01)		
			load_asset_model(sAssetData,landstalker)
			load_asset_animdict(sAssetData,"missheistdocks2a")
			load_asset_animdict(sAssetData,"missheistdocks2a@alert")
			load_asset_animdict(sAssetData,"missheistdocks2a@crouch")
			load_asset_model(sAssetData,prop_mil_crate_02)
			load_asset_model(sAssetData,prop_cratepile_07a)
			load_asset_model(sAssetData,dinghy)
			Load_Asset_Weapon_Asset(sAssetData, wtSniper, WRF_REQUEST_ALL_ANIMS, WEAPON_COMPONENT_SCOPE|WEAPON_COMPONENT_SCLIP2)
		break		
		
		case msf_6_way_out
			load_asset_model(sAssetData,s_m_y_blackops_01)					
			load_asset_model(sAssetData,landstalker)
			load_asset_model(sAssetData,BUZZARD)
			load_asset_animdict(sAssetData,"missheistdocks2a")
			load_asset_waypoint(sAssetData,"docksheist2a01")
			load_asset_waypoint(sAssetData,"docksheist2a02")
			load_asset_waypoint(sAssetData,"docksheist2a03")
			load_asset_animdict(sAssetData,"missheistdocks2a@alert")
			load_asset_recording(sAssetData,001,"dh2arec")
			load_asset_recording(sAssetData,002,"dh2arec")
			load_asset_recording(sAssetData,003,"dh2arec")
			load_asset_model(sAssetData,prop_mil_crate_02)
			load_asset_model(sAssetData,prop_cratepile_07a)
			load_asset_model(sAssetData,dinghy)
			Load_Asset_Weapon_Asset(sAssetData, wtSniper, WRF_REQUEST_ALL_ANIMS, WEAPON_COMPONENT_SCOPE|WEAPON_COMPONENT_SCLIP2)
		break
		
		case msf_7_detonate
			load_asset_model(sAssetData,s_m_y_blackops_01)
			load_asset_model(sAssetData,landstalker)
			load_asset_model(sAssetData,prop_mil_crate_02)
			load_asset_model(sAssetData,prop_cratepile_07a)
			load_asset_model(sAssetData,dinghy)
			Load_Asset_Weapon_Asset(sAssetData, wtSniper, WRF_REQUEST_ALL_ANIMS, WEAPON_COMPONENT_SCOPE|WEAPON_COMPONENT_SCLIP2)
		break
		
		case msf_8_explosion_ctscne							
			load_asset_model(sAssetData,s_m_y_blackops_01)
			load_asset_model(sAssetData,landstalker)
			load_asset_model(sAssetData,dinghy)
		break
		
		case msf_9_the_goods					
			load_asset_model(sAssetData,s_m_y_blackops_01)	
			load_asset_model(sAssetData,Prop_Military_Pickup_01)
			load_asset_model(sAssetData,submersible)	
			load_asset_model(sAssetData,dinghy)
			Load_Asset_Model(sAssetData,Prop_Flare_01)
			load_asset_animdict(sAssetData,"missheistdocks2aswitchig_8")
			load_asset_animdict(sAssetData,"swimming@scuba")
		break 
		case msf_10_end_cutscene
			load_asset_model(sAssetData,Prop_Military_Pickup_01)
			load_asset_model(sAssetData,submersible)
		break 
//		case msf_passed
//			load_asset_model(sAssetData,Prop_Military_Pickup_01)
//			load_asset_model(sAssetData,submersible)					
//		break	
	endswitch	
endproc
func ped_index frank_ped()
	return peds[mpf_frank].id
endfunc
func ped_index mike_ped()
	return peds[mpf_mike].id
endfunc
func ped_index trev_ped()
	return peds[mpf_trev].id
endfunc
proc point_gameplay_cam_at_coord(float targetheading)	
	set_gameplay_cam_relative_heading(targetheading - get_entity_heading(player_ped_id()))
endproc
func bool isentityalive(entity_index mentity)
  	if does_entity_exist(mentity)
		if is_entity_a_vehicle(mentity)
			if is_vehicle_driveable(get_vehicle_index_from_entity_index(mentity))
				return true 
			endif
		elif is_entity_a_ped(mentity)
			if not is_ped_injured(get_ped_index_from_entity_index(mentity))
	          	return true 
	    	endif	
		endif           
	endif
      return false
endfunc
proc safe_remove_blip(blip_index &blipindex)
	if does_blip_exist(blipindex)
		remove_blip(blipindex)
	endif
endproc
proc remove_object(object_index &obj, bool b_force_delete = false)
	if does_entity_exist(obj)
		if not is_entity_dead(obj)
			if is_entity_attached(obj)
				detach_entity(obj)
			endif
		endif

		if b_force_delete
			delete_object(obj)
		else
			set_object_as_no_longer_needed(obj)
		endif
	endif
endproc	
proc spawn_ped(ped_index &pedindex, model_names emodel, vector vstart, float fstart = 0.0, bool bdefault = true)	
		if not does_entity_exist(pedindex)
			pedindex = create_ped(pedtype_mission, emodel, vstart, fstart)
			if bdefault = true
				set_ped_default_component_variation(pedindex)
			endif
		else
			//@RJP if the ped already exists, reset it's coords and heading to be the proper start location/heading
			if isentityalive( pedindex )
				SET_ENTITY_COORDS( pedindex, vstart )
				SET_ENTITY_HEADING( pedindex, fstart )
			endif
		endif	
endproc

proc spawn_vehicle(vehicle_index &vehindex, model_names emodel, vector vstart, float fstart = 0.0, int icolour = -1, float fdirt = 0.0)
	if not does_entity_exist(vehindex)
		request_model(emodel)
		if has_model_loaded(emodel)
			vehindex = create_vehicle(emodel, vstart, fstart)
			if icolour >= 0
				set_vehicle_colours(vehindex, icolour, icolour)
			endif
			set_vehicle_dirt_level(vehindex, fdirt)
		endif
	endif
endproc

/// PURPOSE:
///    Ttrying to free up more space for the cutscene.  Set FALSE to reset to normal.
PROC REDUCE_AMBIENT_MODELS(BOOL bTurnOn)
//	SET_REDUCE_VEHICLE_MODEL_BUDGET(bTurnOn) 
//	SET_REDUCE_PED_MODEL_BUDGET(bTurnOn)
	IF bTurnOn
		CPRINTLN(DEBUG_MISSION, "REDUCE_AMBIENT_MODELS: Setting ON")
		SET_VEHICLE_POPULATION_BUDGET(0) 
		SET_PED_POPULATION_BUDGET(0)
	ELSE
		CPRINTLN(DEBUG_MISSION, "REDUCE_AMBIENT_MODELS: Setting OFF")
		SET_VEHICLE_POPULATION_BUDGET(3) 
		SET_PED_POPULATION_BUDGET(3)
	ENDIF

ENDPROC

proc clear_players_task_on_control_input(script_task_name task_name)

	if get_script_task_status(player_ped_id(), task_name) = performing_task
			
		int left_stick_x
		int left_stick_y
		int right_stick_x
		int right_stick_y
		int stick_dead_zone = 28
	
		get_control_value_of_analogue_sticks(left_stick_x, left_stick_y, right_stick_x, right_stick_y)

		if not is_look_inverted()
			right_stick_y *= -1
		endif
		
		// invert the vertical
		if (left_stick_y > stick_dead_zone)
		or (left_stick_y < (stick_dead_zone * -1))	
		or (left_stick_x > stick_dead_zone)
		or (left_stick_x < (stick_dead_zone * -1))
		//or is_control_pressed(player_control, input_sprint) 
		
			clear_ped_tasks(player_ped_id())
			
		endif 		
	endif 

endproc 

/// PURPOSE:
///    Removes the health pickups on the ship
PROC DELETE_HEALTH_PICKUPS()
	
	IF DOES_PICKUP_EXIST( puiHealthPack[0] )
		REMOVE_PICKUP( puiHealthPack[0] )
	ENDIF
	
//	IF DOES_PICKUP_EXIST( puiHealthPack[1] )
//		REMOVE_PICKUP( puiHealthPack[1] )
//	ENDIF
	
ENDPROC

PROC CREATE_HEALTH_PICKUPS()
	
	DELETE_HEALTH_PICKUPS()
	
	// set placement flags
	INT PlacementFlags
	PlacementFlags = 0
	SET_BIT(PlacementFlags, enum_to_int(PLACEMENT_FLAG_FIXED))
	SET_BIT(PlacementFlags, enum_to_int(PLACEMENT_FLAG_LOCAL_ONLY)) 
	
	puiHealthPack[0] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, (<<-125.235405,-2357.386230,9.676947>>), (<<0.000000,-0.000000,0.000000>>), PlacementFlags)
//	puiHealthPack[1] = CREATE_PICKUP_ROTATE(PICKUP_HEALTH_STANDARD, (<<-225.270004,-2376.050049,9.060000>>), (<<90.00000,-0.000000,-150.000>>), PlacementFlags)
	
ENDPROC

proc createdockworker(mpf_mission_ped_flags ped)
	
	peds[ped].id = create_ped(pedtype_mission,s_m_y_dockwork_01,peds[ped].startloc,peds[ped].head)
	
	set_ped_money(peds[ped].id,0)
	set_ped_flee_attributes(peds[ped].id,fa_return_to_orignal_position_after_flee,false)
	set_ped_flee_attributes(peds[ped].id,fa_never_flee,false)
	set_ped_flee_attributes(peds[ped].id,fa_wander_at_end,true)
	set_ped_flee_attributes(peds[ped].id,fa_prefer_pavements,true)
	set_ped_combat_attributes(peds[ped].id,ca_always_flee,true)
	set_ped_hearing_range(peds[ped].id,15)
	set_ped_seeing_range(peds[ped].id,10)
	set_ped_alertness(peds[ped].id,as_alert)
	
endproc

PROC TASK_MERC_TO_DEFAULT_PATROL_SEQUENCE( INT merc )
	
	IF NOT isentityalive( mercs[merc].id )
		EXIT
	ENDIF
	
	OPEN_SEQUENCE_TASK( seq )
	SWITCH INT_TO_ENUM( MMF_MISSION_MERC_FLAGS, merc )
		CASE MERC3_C4_2
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-110.7773, -2374.6187, 8.3191>> ,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-123.3457, -2366.2498, 8.3191>> ,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-113.7629, -2357.5508, 8.3191>> ,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-99.3139, -2365.4387, 8.3220>> ,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
			SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
		BREAK
		CASE MERC4_C4_2
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,MERCS[MERC4_C4_2].STARTLOC,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
			TASK_ACHIEVE_HEADING( NULL, 180.0 )
			TASK_PLAY_ANIM(NULL,"missheistdocks2a","IDLE_guard",2,2,-1,AF_LOOPING)
			SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
		BREAK
		CASE MERC5_C4_2
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-129.9954, -2365.6687, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-138.6747, -2377.9009, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-147.1642, -2365.7585, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-138.5969, -2354.1770, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
		BREAK
		CASE MERC6_C4_2
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-129.9954, -2365.6687, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-138.6747, -2377.9009, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-147.1642, -2365.7585, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-138.5969, -2354.1770, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
		BREAK
		CASE MERC7_C4_2
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-129.9954, -2365.6687, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-138.6747, -2377.9009, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-147.1642, -2365.7585, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-138.5969, -2354.1770, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
		BREAK
		CASE MERC8_C4_2
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,MERCS[MERC8_C4_2].STARTLOC,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
			TASK_PATROL(NULL,"MISS_PATROL_6",PAS_ALERT)
		BREAK
		CASE MERC9_C4_3
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-155.3885, -2353.8774, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-147.7102, -2364.9680, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-154.9743, -2377.9568, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-163.6116, -2367.8689, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
		BREAK
		CASE MERC10_C4_3
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-181.9512, -2367.6396, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-171.5216, -2353.9668, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-163.9121, -2364.6187, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-171.5004, -2378.1313, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
		BREAK
		CASE MERC11_C4_3
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-181.9512, -2367.6396, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-171.5216, -2353.9668, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-163.9121, -2364.6187, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-171.5004, -2378.1313, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
		BREAK
		CASE MERC12_C4_3
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-193.5307, -2378.2000, 10.9717>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
			TASK_PLAY_ANIM(NULL,"missheistdocks2a","IDLE_guard")
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-193.6108, -2375.8923, 10.9717>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
			TASK_PLAY_ANIM(NULL,"missheistdocks2a","IDLE_guard")
			SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
//			TASK_PATROL(NULL,"MISS_PATROL_7",PAS_ALERT)
		BREAK
		CASE MERC13_C4_3
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,MERCS[MERC13_C4_3].STARTLOC,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
			TASK_ACHIEVE_HEADING( NULL, 180.0 )
			TASK_PLAY_ANIM(NULL,"missheistdocks2a","IDLE_guard",2,2,-1,AF_LOOPING)
			SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
		BREAK
		CASE MERC14_C4_3
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-200.7796, -2377.9519, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-209.5825, -2368.0845, 8.3192>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-200.6789, -2353.7856, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-190.3428, -2365.7305, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
		BREAK
		CASE MERC15_C4_3
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-200.7796, -2377.9519, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-209.5825, -2368.0845, 8.3192>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-200.6789, -2353.7856, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-190.3428, -2365.7305, 8.3191>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
		BREAK
		CASE MERC16_C4_4
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-221.3304, -2377.0103, 12.3329>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_STAND_STILL(NULL, 3000)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-230.3291, -2377.7026, 12.3329>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-222.6653, -2375.9956, 16.3326>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-215.1669, -2377.6995, 16.3326>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_STAND_STILL(NULL, 3000)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-222.6653, -2375.9956, 16.3326>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-230.3291, -2377.7026, 12.3329>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
			SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
		BREAK
		CASE MERC17_C4_4
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,MERCS[MERC17_C4_4].STARTLOC,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
			TASK_PATROL(NULL,"MISS_PATROL_8",PAS_ALERT)
		BREAK
		CASE MERC18_C4_4
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-250.5147, -2375.5378, 8.3192>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
			TASK_PLAY_ANIM(NULL,"missheistdocks2a","IDLE_guard")
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<-252.1649, -2363.8347, 8.3192>>,PEDMOVE_WALK,DEFAULT_TIME_NEVER_WARP)
			TASK_PLAY_ANIM(NULL,"missheistdocks2a","IDLE_guard")
			SET_SEQUENCE_TO_REPEAT(seq,REPEAT_FOREVER)
//			TASK_PATROL(NULL,"MISS_PATROL_9",PAS_ALERT)
		BREAK
		DEFAULT
			TASK_STAND_STILL( NULL, -1 )
		BREAK
	ENDSWITCH
	CLOSE_SEQUENCE_TASK( seq )
	TASK_PERFORM_SEQUENCE( mercs[merc].id, seq )
	CLEAR_SEQUENCE_TASK( seq )
ENDPROC

proc createmercenary(mmf_mission_merc_flags merc,float fseeingrange = 8.0,bool btempblkevents = true,bool blos = false)
	load_asset_model(sAssetData,s_m_y_blackops_01)	
	
	spawn_ped(mercs[merc].id, s_m_y_blackops_01, mercs[merc].startloc, mercs[merc].head, false)	
	if isentityalive(mercs[merc].id)
		set_ped_relationship_group_hash(mercs[merc].id, rel_group_enemies)
		set_ped_as_enemy(mercs[merc].id, true)	
		give_weapon_to_ped(mercs[merc].id, wtenemywpn, infinite_ammo, true)
		set_ped_dies_instantly_in_water(mercs[merc].id, true)
		set_ped_money(mercs[merc].id,0)	
		set_ped_path_can_use_ladders(mercs[merc].id, true)	
			
		set_ped_alertness(mercs[merc].id,as_not_alert)
		set_ped_flee_attributes(mercs[merc].id,fa_never_flee,true)
		
		//combat attributes		
		set_ped_target_loss_response(mercs[merc].id,tlr_never_lose_target)
		set_ped_combat_attributes(mercs[merc].id,ca_can_shoot_without_los ,blos)
		set_ped_combat_attributes(mercs[merc].id, ca_use_cover,true)
		set_ped_combat_attributes(mercs[merc].id,ca_can_investigate,true)
		set_ped_combat_attributes(mercs[merc].id, ca_use_vehicle, false)
		set_ped_combat_movement(mercs[merc].id, cm_defensive)	
		
		stop_ped_speaking(mercs[merc].id,true)	
		set_entity_load_collision_flag(mercs[merc].id,true)	
		set_ped_hearing_range(mercs[merc].id,10)//temp
		set_ped_seeing_range(mercs[merc].id,fseeingrange)
		set_ped_config_flag(mercs[merc].id,pcf_runfromfiresandexplosions,false)
		set_ped_accuracy(mercs[merc].id,30)
		stop_ped_weapon_firing_when_dropped(mercs[merc].id)
		SET_PED_DROPS_WEAPONS_WHEN_DEAD(mercs[merc].id,true)
		
		if btempblkevents
			set_blocking_of_non_temporary_events(mercs[merc].id,true)
		endif
		
		//@RJP init these to prevent problems in replay such as the alarm souding early
		mercs[merc].balive = true
		mercs[merc].bincombat = false
		mercs[merc].bbulletmiss = false
		mercs[merc].bStealthKill = false
		mercs[merc].currentaction = GA_FIND_COVER
		mercs[merc].bSpottedDead = false
		mercs[merc].bPrintHelp = false
		mercs[merc].bAlertSpeech = false
		//@RJP init irevertactiontime so mercs don't reset their patrols when the sneaking section starts
		mercs[merc].irevertactiontime = -1
		
		//Mercs won't all say first line at same time
		mercs[merc].iDialogueTimer = get_game_Timer() + GET_RANDOM_INT_IN_RANGE( 0, 20000 )
		
		mercs[merc].bzombie = false
		
		if mission_stage < enum_to_int(msf_6_way_out)
		//@RJP mercs should be perfoming their patrol routes as they can be seen from the vantage point and from Michael on the dinghy
			TASK_MERC_TO_DEFAULT_PATROL_SEQUENCE( ENUM_TO_INT(merc) )
		else
			if not does_blip_exist(mercs[merc].blip)
				mercs[merc].blip = create_blip_for_ped(mercs[merc].id,true)
			endif
		endif
		
		#if is_debug_build
			text_label tdebugname = "merc "
			tdebugname += enum_to_int(merc) + 1
			set_ped_name_debug(mercs[merc].id, tdebugname)
		#endif
			
		INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(mercs[merc].id,true)
				
	endif
endproc
FUNC BOOL createzombiemercenary( mmf_mission_merc_flags merc, VECTOR vPos, FLOAT fHeading, VECTOR vDef )
	
	load_asset_model(sAssetData,s_m_y_blackops_01)
	
	IF WOULD_ENTITY_BE_OCCLUDED( s_m_y_blackops_01, vPos )
		spawn_ped(mercs[merc].id, s_m_y_blackops_01, vPos, fHeading, false)
	ELSE
		RETURN FALSE
	ENDIF

	if isentityalive(mercs[merc].id)
		mercs[merc].bzombie = true
		
		//attributes 
		set_ped_flee_attributes(mercs[merc].id,fa_never_flee,true)
		set_ped_combat_attributes(mercs[merc].id,ca_aggressive,FALSE)
		set_ped_combat_attributes(mercs[merc].id, ca_use_cover,TRUE)
		set_ped_combat_movement(mercs[merc].id, CM_DEFENSIVE)
		//important config
		give_weapon_to_ped(mercs[merc].id, wtenemywpn, infinite_ammo, true)
		set_ped_can_be_targeted_without_los(mercs[merc].id,false)
		set_blocking_of_non_temporary_events(mercs[merc].id,true)	
		//misc	
		set_ped_relationship_group_hash(mercs[merc].id, rel_group_enemies)
		set_ped_as_enemy(mercs[merc].id, true)	
		set_ped_dies_instantly_in_water(mercs[merc].id, true)
		set_ped_money(mercs[merc].id,0)	
		set_ped_path_can_use_ladders(mercs[merc].id, true)	
		
		//combat attributes		
		set_ped_target_loss_response(mercs[merc].id,tlr_never_lose_target)
		set_ped_combat_range(mercs[merc].id, cr_near)
		set_ped_combat_movement(mercs[merc].id, cm_defensive)		
		set_entity_load_collision_flag(mercs[merc].id,true)	
		set_ped_hearing_range(mercs[merc].id,200)
		set_ped_seeing_range(mercs[merc].id,300)
		SET_PED_DROPS_WEAPONS_WHEN_DEAD(mercs[merc].id,true)
	
		set_ped_config_flag(mercs[merc].id,pcf_runfromfiresandexplosions,false)
//		if not does_blip_exist(mercs[merc].blip)
//			mercs[merc].blip = create_blip_for_ped(mercs[merc].id,true)
//		endif
		
		SET_PED_SPHERE_DEFENSIVE_AREA( mercs[merc].id, vDef, 10, TRUE )
		
		#if is_debug_build
			text_label tdebugname = "zombie merc "
			tdebugname += enum_to_int(merc) + 1
			set_ped_name_debug(mercs[merc].id, tdebugname)
		#endif
		
		TASK_COMBAT_PED( mercs[merc].id, mike_ped() )
	endif
	
	RETURN TRUE
	
ENDFUNC
func float get_heading_from_coords(vector oldcoords,vector newcoords, bool invert=true) 
	float heading
	float dx = newcoords.x - oldcoords.x
	float dy = newcoords.y - oldcoords.y
	if dy != 0
		heading = atan2(dx,dy)
	else
		if dx < 0
			heading = -90
		else
			heading = 90
		endif
	endif
	
	//flip because for some odd reason the coders think west is a heading of 90 degrees, so this'll match the output of commands such as get_entity_heading()
	if invert = true 	
		heading *= -1.0
		//below not necessary but helps for debugging
		if heading < 0
			heading += 360.0
		endif
	endif
	
	return heading
endfunc

/// PURPOSE:
///   	Check if an entity exists and is alive
FUNC BOOL IS_ENTITY_OK(ENTITY_INDEX ent)
	IF NOT DOES_ENTITY_EXIST(ent)
		RETURN FALSE
	ENDIF
	
	RETURN NOT IS_ENTITY_DEAD(ent)
ENDFUNC

PROC ENABLE_ALL_DISPATCH_SERVICES(BOOL ok = TRUE)
    ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, ok)
    ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, ok)
    ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, ok)
    ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, ok)
    ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, ok)
    ENABLE_DISPATCH_SERVICE(DT_POLICE_VEHICLE_REQUEST, ok)
  	ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, ok)
    ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, ok)
    ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, ok)
    ENABLE_DISPATCH_SERVICE(DT_GANGS, ok)
    ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, ok)
ENDPROC

func float GET_HEADING_FROM_ENTITIES(entity_index oldEnd, entity_index newEnt, bool invert=true)
	vector v1,v2
	v1 = GET_ENTITY_COORDS(oldEnd,FALSE)
	v2 = GET_ENTITY_COORDS(newEnt,FALSE)
	RETURN GET_HEADING_FROM_COORDS(v1,v2, invert)
endfunc

func float get_sniper_rifle_aim_heading_for_coord(VECTOR vCoord) 	
    vector vec_ba
    float aim_heading
	
  	vec_ba = vCoord - get_entity_coords(player_ped_id())      
  	//x rotation
  	aim_heading = (get_heading_from_vector_2d(vec_ba.x, vec_ba.y) - get_entity_heading(player_ped_id()))      
  	if aim_heading > 180
        aim_heading -= 360
 	 endif
  	if aim_heading < -180
        aim_heading += 360
  	endif
	
    return aim_heading      
endfunc 

func float get_sniper_rifle_aim_pitch_for_coord(VECTOR vCoord)
	
    vector vec_ba
    vector direction_vec
	  
	IF isentityalive(PLAYER_PED_ID())
    	vec_ba = vCoord - get_entity_coords(player_ped_id())
    endif
	  
    direction_vec = normalise_vector(vec_ba)
	
    return (atan2(direction_vec.z, vmag(<<direction_vec.x, direction_vec.y, 0.0>>)))

endfunc

func float get_sniper_rifle_aim_heading_for_ped(ped_index miss_ped) 	
    vector vec_ba
    float aim_heading
	
	if isentityalive(miss_ped)
      	vec_ba = get_entity_coords(miss_ped) - get_entity_coords(player_ped_id())      
      	//x rotation
      	aim_heading = (get_heading_from_vector_2d(vec_ba.x, vec_ba.y) - get_entity_heading(player_ped_id()))      
      	if aim_heading > 180
            aim_heading -= 360
     	 endif
      	if aim_heading < -180
            aim_heading += 360
      	endif
	endif
	
    return aim_heading      
endfunc 

func float get_sniper_rifle_aim_pitch_for_ped(ped_index miss_ped)
	
      vector vec_ba
      vector direction_vec
	  
      if isentityalive(miss_ped)
	  and isentityalive(PLAYER_PED_ID())
     	 vec_ba = get_entity_coords(miss_ped) - get_entity_coords(player_ped_id())
      endif
	  
      direction_vec = normalise_vector(vec_ba)

      return (atan2(direction_vec.z, vmag(<<direction_vec.x, direction_vec.y, 0.0>>)))

endfunc
proc do_exciting_near_bullet_miss_on_ped(ped_index pedtomiss, ped_index sourceofbullets, int &icontroltimer, vector sourceoffset, 
                                           int timebetweenbullets = 60, float minxrange = -3.9, float maxxrange = -1.0, float minyrange = -2.9, float maxyrange = 3.9,
										   weapon_type weapon = weapontype_sniperrifle)
    //fire bullets at the player as from the bad  guy...    
    vector bullethit
    vector bulletorigin
    
    if ((get_game_timer() - icontroltimer) > timebetweenbullets)
        if not is_entity_dead(pedtomiss)
        and not is_entity_dead(sourceofbullets)            
            bullethit = get_offset_from_entity_in_world_coords(pedtomiss, <<get_random_float_in_range(minxrange, maxxrange), get_random_float_in_range(minyrange, maxyrange), 0.0>>)
            bulletorigin = get_offset_from_entity_in_world_coords(sourceofbullets, sourceoffset)
            get_ground_z_for_3d_coord(bullethit, bullethit.z)           
            shoot_single_bullet_between_coords(bulletorigin, bullethit, 1,false,weapon)
            icontroltimer = get_game_timer()
      	endif
     endif
endproc
// purpose:off radar/ hud / player control
proc prep_start_cutscene(bool bplayercontrol,vector varea,bool brender = true,bool binterp = false,bool bclearprj = true,int interptime = default_interp_to_from_game,set_player_control_flags controlflag = 0)
	display_radar(false)
	display_hud(false)	
	set_player_control(player_id(),bplayercontrol,controlflag)
	clear_prints()
	clear_help()
	render_script_cams(brender,binterp,interptime)	
	if bclearprj
		clear_area_of_projectiles(varea,500)
	endif
	hang_up_and_put_away_phone()
	bincutscene = brender
endproc
// purpose:on radar/ hud / player control
proc prep_stop_cutscene(bool bplayercontrol,bool brender = false,bool binterp = false,int iduration = default_interp_to_from_game, set_player_control_flags controlflag = 0)
	display_radar(true)
	display_hud(true)	
	set_player_control(player_id(),bplayercontrol,controlflag)	
	render_script_cams(brender,binterp,iduration)
	bincutscene = brender
	if not brender	
		destroy_all_cams()
	endif
endproc

proc manage_ipls(BUILDING_STATE_ENUM bstate)
	
	// BUILDINGSTATE_NORMAL = ship fine /  BUILDINGSTATE_DESTROYED = exploded
	set_building_state(BUILDINGNAME_IPL_CARGOSHIP,bstate)
	set_building_state(BUILDINGNAME_IPL_CARGOSHIP_OCCLUSION,bstate)
	
endproc
proc scuba_suite_up(ped_index scuba_man, bool binwater = false, bool bMask = TRUE)
	if isentityalive(scuba_man)		
		if binwater			
			SET_PED_COMP_ITEM_CURRENT_SP(scuba_man,comp_type_outfit,outfit_p0_scuba_water,false)
			set_enable_scuba(scuba_man,true)
			set_ped_dies_in_water(scuba_man,false)
		else			
			SET_PED_COMP_ITEM_CURRENT_SP(scuba_man,comp_type_outfit,outfit_p0_scuba_land,false)
		endif		
		if not bMask
			SET_PED_COMP_ITEM_CURRENT_SP(scuba_man, COMP_TYPE_PROPS, PROPS_EYES_NONE)
		endif
	endif
endproc

proc do_stream_vol(bool bOn = true)
	if bOn
		volShip =	STREAMVOL_CREATE_SPHERE(<<-222.7796, -2369.0286, 21.6855>>,15,FLAG_COLLISIONS_WEAPON)
	else
		STREAMVOL_DELETE(volShip)
	endif
endproc

// -----------------------------------------------------------------------------------------------------------
//		SWITCH CAM VARIABLES
// -----------------------------------------------------------------------------------------------------------

ENUM FIB5_SWITCH_CAM_STATE
	SWITCH_CAM_IDLE,
	SWITCH_CAM_REQUEST_ASSETS,
	SWITCH_CAM_SETUP_SPLINE,
	SWITCH_CAM_PLAYING_SPLINE,
	SWITCH_CAM_SHUTDOWN_SPLINE,
	SWITCH_CAM_RETURN_TO_GAMEPLAY,
	SWITCH_CAM_WAIT_FOR_INTERPOLATION
ENDENUM
FIB5_SWITCH_CAM_STATE eSwitchCamState = SWITCH_CAM_IDLE

SWITCH_CAM_STRUCT scsSwitchCam_MichaelToTrevor

#IF IS_DEBUG_BUILD
//BOOL bSwitchCamDebugScenarioEnabled = TRUE
//BOOL bResetDebugScenario = FALSE
#ENDIF

BOOL bPlayerControlGiven

BOOL bSwitchCam_CharIsSwapped
BOOL bSwitchCam_SubStartedMoving
BOOL bSwitchCam_SubPropellersStarted 
BOOL bSwitchCam_MikeSwitchFX
BOOL bSwitchCam_TrevSwitchFX
FLOAT fSwitchCam_MikeSwitchFXPhase = 0.00142
FLOAT fSwitchCam_TrevSwitchFXPhase = 0.125
FLOAT fSwitchCam_SubPropellersStarted = 0.5
FLOAT fSwitchCam_SubStartedMovingPhase = 0.195
INT iSwitchCam_ReturnToGameplayBlendTime = 1000
//FLOAT fSwitchCam_SubRecPlaybackSpeed = 1.0
//FLOAT fSwitchCam_SubMovementSpeed = 5.0
//VECTOR vSwitchCam_SubTargetDest = <<-403, -2371, -11.5>>

//FLOAT fSwitchCam_MikeLeavesDingy = 0.3
//FLOAT fFranklinStartAimPhase = 0.2

#IF IS_DEBUG_BUILD
BOOL bSwitchCamDebug_SkipToSwitch
#ENDIF

//PED_INDEX piFranklinSwitch


func float get_sniper_rifle_aim_heading_for_vector(VECTOR aim_target) 	
   // vector vec_ba
    float aim_heading
	
	//if isentityalive(miss_ped)
      //	vec_ba = get_entity_coords(miss_ped) - get_entity_coords(player_ped_id())      
      	//x rotation
      	aim_heading = (get_heading_from_vector_2d(aim_target.x, aim_target.y) - get_entity_heading(player_ped_id()))      
      	if aim_heading > 180
            aim_heading -= 360
     	 endif
      	if aim_heading < -180
            aim_heading += 360
      	endif
	//endif
	
    return aim_heading      
endfunc 

FUNC BOOL IS_PLAYER_PLAYING_IN_FIRST_PERSON_ON_FOOT()
	RETURN GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON	
ENDFUNC

PROC SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_TREVOR(SWITCH_CAM_STRUCT &thisSwitchCam, PED_INDEX piMichael, VEHICLE_INDEX viSub )
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_TREVOR")
	
	IF NOT thisSwitchCam.bInitialized


		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<2.1696, -2.5168, 0.0412>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[0].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[0].vNodeDir = <<0.5515, 0.2998, 0.2401>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].fNodeFOV = 35.0000
		thisSwitchCam.nodes[0].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].iNodeToClone = 0
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[0].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 1500
		thisSwitchCam.nodes[1].vNodePos = <<2.1018, -2.5559, 0.0412>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[1].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[1].vNodeDir = <<0.4967, 0.2380, 0.3844>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].fNodeFOV = 35.0000
		thisSwitchCam.nodes[1].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].iNodeToClone = 0
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[1].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[1].iCamEaseType = 2
		thisSwitchCam.nodes[1].fCamEaseScaler = 0.9000
		thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = TRUE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = FALSE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0
		thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 600
		thisSwitchCam.nodes[2].vNodePos = <<-6.3518, -7.3249, 0.0389>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[2].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[2].vNodeDir = <<-7.9352, -4.5686, 0.1894>>
		thisSwitchCam.nodes[2].bPointAtEntity = TRUE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].fNodeFOV = 35.0000
		thisSwitchCam.nodes[2].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].iNodeToClone = 0
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 1.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 0.3000
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[2].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[2].iCamEaseType = 0
		thisSwitchCam.nodes[2].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[2].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 1.0000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bFlashEnabled = TRUE
		thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_SwitchShortTrevorMid
		thisSwitchCam.nodes[2].fMinExposure = 200.0000
		thisSwitchCam.nodes[2].fMaxExposure = 200.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 50
		thisSwitchCam.nodes[2].iRampDownDuration = 50
		thisSwitchCam.nodes[2].iHoldDuration = 100
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.9000
		thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[3].bIsCamCutNode = TRUE

		thisSwitchCam.nodes[4].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[4].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[4].iNodeTime = 0
		thisSwitchCam.nodes[4].vNodePos = <<5.9623, 9.3726, 0.3272>>
		thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[4].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[4].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[4].vNodeDir = <<-7.3216, 0.0000, 85.1943>>
		thisSwitchCam.nodes[4].bPointAtEntity = FALSE
		thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].fNodeFOV = 30.000
		thisSwitchCam.nodes[4].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].iNodeToClone = 0
		thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[4].fNodeMotionBlur = 0.3000
		thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[4].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[4].iCamEaseType = 0
		thisSwitchCam.nodes[4].fCamEaseScaler = 1.0000
		thisSwitchCam.nodes[4].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[4].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[4].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[4].fTimeScale = 1.0000
		thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[4].bFlashEnabled = FALSE
		thisSwitchCam.nodes[4].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[4].fMinExposure = 0.0000
		thisSwitchCam.nodes[4].fMaxExposure = 0.0000
		thisSwitchCam.nodes[4].iRampUpDuration = 0
		thisSwitchCam.nodes[4].iRampDownDuration = 0
		thisSwitchCam.nodes[4].iHoldDuration = 0
		thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[4].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[5].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[5].iNodeTime = 500
		thisSwitchCam.nodes[5].vNodePos = <<6.9998, 3.8269, 0.5564>>
		thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[5].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[5].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[5].vNodeDir = <<-7.3216, 0.0000, 97.1138>>
		thisSwitchCam.nodes[5].bPointAtEntity = FALSE
		thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].fNodeFOV = 30.000
		thisSwitchCam.nodes[5].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].iNodeToClone = 0
		thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[5].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[5].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[5].iCamEaseType = 0
		thisSwitchCam.nodes[5].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[5].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[5].fTimeScale = 1.0000
		thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[5].bFlashEnabled = FALSE
		thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[5].fMinExposure = 0.0000
		thisSwitchCam.nodes[5].fMaxExposure = 0.0000
		thisSwitchCam.nodes[5].iRampUpDuration = 0
		thisSwitchCam.nodes[5].iRampDownDuration = 0
		thisSwitchCam.nodes[5].iHoldDuration = 0
		thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[5].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[6].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[6].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[6].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[6].iNodeTime = 500
		thisSwitchCam.nodes[6].vNodePos = <<3.0225, 6.4558, 0.5031>>
		thisSwitchCam.nodes[6].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[6].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[6].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[6].vNodeDir = <<0.1229, 0.0020, -0.4059>>
		thisSwitchCam.nodes[6].bPointAtEntity = TRUE
		thisSwitchCam.nodes[6].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[6].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[6].fNodeFOV = 30.000
		thisSwitchCam.nodes[6].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].iNodeToClone = 0
		thisSwitchCam.nodes[6].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[6].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[6].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[6].fNodeMotionBlur = 1.0000
		thisSwitchCam.nodes[6].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[6].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[6].iCamEaseType = 0
		thisSwitchCam.nodes[6].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[6].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[6].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[6].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[6].fTimeScale = 1.0000
		thisSwitchCam.nodes[6].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[6].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[6].bFlashEnabled = TRUE
		thisSwitchCam.nodes[6].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[6].fMinExposure = 200.0000
		thisSwitchCam.nodes[6].fMaxExposure = 200.0000
		thisSwitchCam.nodes[6].iRampUpDuration = 50
		thisSwitchCam.nodes[6].iRampDownDuration = 50
		thisSwitchCam.nodes[6].iHoldDuration = 100
		thisSwitchCam.nodes[6].fFlashNodePhaseOffset = 0.9500
		thisSwitchCam.nodes[6].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[6].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[7].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[7].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[7].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[7].iNodeTime = 3000
		thisSwitchCam.nodes[7].vNodePos = <<7.0921, -0.5832, 1.1157>>
		thisSwitchCam.nodes[7].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[7].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[7].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[7].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[7].vNodeDir = <<0.1416, -1.1054, -0.2735>>
		thisSwitchCam.nodes[7].bPointAtEntity = TRUE
		thisSwitchCam.nodes[7].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[7].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[7].fNodeFOV = 30.000
		thisSwitchCam.nodes[7].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[7].iNodeToClone = 0
		thisSwitchCam.nodes[7].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[7].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[7].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[7].fNodeMotionBlur = 1.0000
		thisSwitchCam.nodes[7].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[7].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[7].iCamEaseType = 0
		thisSwitchCam.nodes[7].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[7].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[7].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[7].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[7].fTimeScale = 1.0000
		thisSwitchCam.nodes[7].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[7].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[7].bFlashEnabled = FALSE
		thisSwitchCam.nodes[7].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[7].fMinExposure = 0.0000
		thisSwitchCam.nodes[7].fMaxExposure = 0.0000
		thisSwitchCam.nodes[7].iRampUpDuration = 0
		thisSwitchCam.nodes[7].iRampDownDuration = 0
		thisSwitchCam.nodes[7].iHoldDuration = 0
		thisSwitchCam.nodes[7].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[7].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[7].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[8].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[8].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[8].bIsGameplayCamCopy = TRUE
		thisSwitchCam.nodes[8].iNodeTime = 2000
		thisSwitchCam.nodes[8].vNodePos = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[8].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[8].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[8].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[8].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[8].vNodeDir = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[8].bPointAtEntity = TRUE
		thisSwitchCam.nodes[8].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[8].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[8].fNodeFOV = 52.5000
		thisSwitchCam.nodes[8].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[8].iNodeToClone = 0
		thisSwitchCam.nodes[8].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[8].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[8].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[8].fNodeMotionBlur = 0.3000
		thisSwitchCam.nodes[8].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[8].fNodeCamShake = 0.2000
		thisSwitchCam.nodes[8].iCamEaseType = 0
		thisSwitchCam.nodes[8].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[8].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[8].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[8].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[8].fTimeScale = 0.0000
		thisSwitchCam.nodes[8].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[8].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[8].bFlashEnabled = FALSE
		thisSwitchCam.nodes[8].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[8].fMinExposure = 0.0000
		thisSwitchCam.nodes[8].fMaxExposure = 0.0000
		thisSwitchCam.nodes[8].iRampUpDuration = 0
		thisSwitchCam.nodes[8].iRampDownDuration = 0
		thisSwitchCam.nodes[8].iHoldDuration = 0
		thisSwitchCam.nodes[8].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[8].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[8].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[8].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[8].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[8].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.camVelocityOverrides[0].fStartPoint = 0.0000
		thisSwitchCam.camVelocityOverrides[0].fSpeed = 0.0500
		thisSwitchCam.camVelocityOverrides[1].fStartPoint = 0.9000
		thisSwitchCam.camVelocityOverrides[1].fSpeed = 0.0500
		thisSwitchCam.camVelocityOverrides[2].fStartPoint = 1.0000
		thisSwitchCam.camVelocityOverrides[2].fSpeed = 19.0000
		thisSwitchCam.camVelocityOverrides[3].fStartPoint = 4.0000
		thisSwitchCam.camVelocityOverrides[3].fSpeed = 19.0000
		thisSwitchCam.camVelocityOverrides[4].fStartPoint = 4.3000
		thisSwitchCam.camVelocityOverrides[4].fSpeed = 19.0000
		thisSwitchCam.camVelocityOverrides[5].fStartPoint = 5.3500
		thisSwitchCam.camVelocityOverrides[5].fSpeed = 2.5000
		thisSwitchCam.camVelocityOverrides[6].fStartPoint = 5.6000
		thisSwitchCam.camVelocityOverrides[6].fSpeed = 2.5000
		thisSwitchCam.camVelocityOverrides[7].fStartPoint = 6.3000
		thisSwitchCam.camVelocityOverrides[7].fSpeed = 25.0000
		thisSwitchCam.camVelocityOverrides[8].fStartPoint = 7.8000
		thisSwitchCam.camVelocityOverrides[8].fSpeed = 25.0000
		thisSwitchCam.camVelocityOverrides[9].fStartPoint = 7.9000
		thisSwitchCam.camVelocityOverrides[9].fSpeed = 3.0000
		thisSwitchCam.camVelocityOverrides[10].fStartPoint = 8.0000
		thisSwitchCam.camVelocityOverrides[10].fSpeed = 0.7000

		thisSwitchCam.camBlurOverrides[0].fBlurOverrideStartPoint = 0.0000
		thisSwitchCam.camBlurOverrides[0].fBlurOverrideBlurLevel = 0.0000
		thisSwitchCam.camBlurOverrides[1].fBlurOverrideStartPoint = 2.8000
		thisSwitchCam.camBlurOverrides[1].fBlurOverrideBlurLevel = 1.0000
		thisSwitchCam.camBlurOverrides[2].fBlurOverrideStartPoint = 3.2000
		thisSwitchCam.camBlurOverrides[2].fBlurOverrideBlurLevel = 1.0000
		thisSwitchCam.camBlurOverrides[3].fBlurOverrideStartPoint = 4.0000
		thisSwitchCam.camBlurOverrides[3].fBlurOverrideBlurLevel = 1.0000
		thisSwitchCam.camBlurOverrides[4].fBlurOverrideStartPoint = 4.8000
		thisSwitchCam.camBlurOverrides[4].fBlurOverrideBlurLevel = 0.0000
		thisSwitchCam.camBlurOverrides[5].fBlurOverrideStartPoint = 5.8000
		thisSwitchCam.camBlurOverrides[5].fBlurOverrideBlurLevel = 1.0000
		thisSwitchCam.camBlurOverrides[6].fBlurOverrideStartPoint = 6.0000
		thisSwitchCam.camBlurOverrides[6].fBlurOverrideBlurLevel = 1.0000
		thisSwitchCam.camBlurOverrides[7].fBlurOverrideStartPoint = 7.0000
		thisSwitchCam.camBlurOverrides[7].fBlurOverrideBlurLevel = 1.0000
		thisSwitchCam.camBlurOverrides[8].fBlurOverrideStartPoint = 8.0000
		thisSwitchCam.camBlurOverrides[8].fBlurOverrideBlurLevel = 0.0000

		thisSwitchCam.iNumNodes = 9
		thisSwitchCam.iCamSwitchFocusNode = 4
		thisSwitchCam.fSwitchSoundAudioStartPhase = -1.0
		thisSwitchCam.fSwitchSoundAudioEndPhase = -1.0
		thisSwitchCam.bSplineNoSmoothing = TRUE
		thisSwitchCam.bAddGameplayCamAsLastNode = FALSE
		thisSwitchCam.iGameplayNodeBlendDuration = 800


		//--- End of Cam Data ---

		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_HEIST_DOCKS_MichaelToSubmarine.txt"	
		thisSwitchCam.strXMLFileName = "CameraInfo_HEIST_DOCKS_MichaelToSubmarine.xml"
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.piPeds[0] = piMichael
	thisSwitchCam.viVehicles[1] = viSub
	
		
ENDPROC

/// PURPOSE:
///    Handle the playback of the switch cam.  The will setup, play, shut down and cleanup the switch cam spline
/// PARAMS:
///    thisSwitchCam - Switch camera to playback
///    bReturnTrueOnce - If you only want this function to return TRUE once upon completion. Set to FALSE to have this function to return true every time this is called once the camera is finished playback.
/// RETURNS:
///    TRUE when camera is complete, FALSE if playback is still active, or if the camera has completed playback and bReturnTrueOnce is set to TRUE.
FUNC BOOL HANDLE_SWITCH_CAM_MICHAEL_TO_TREVOR(SWITCH_CAM_STRUCT &thisSwitchCam, BOOL bReturnTrueOnce = TRUE)
	
	INT iCurrentNode
	
	DISABLE_FIRST_PERSON_FLASH_EFFECT_THIS_UPDATE()
	
	SWITCH eSwitchCamState

		CASE SWITCH_CAM_IDLE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")
		BREAK
		
		CASE SWITCH_CAM_REQUEST_ASSETS
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_REQUEST_ASSETS")
			
			load_asset_recording(sAssetData, 004, "dh2arec")
			REQUEST_ANIM_DICT("missswitch")
		
			SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_TREVOR(thisSwitchCam, mike_ped(), vehs[mvf_sub].id)
			LOAD_CAM_SHAKE_LIBRARIES(thisSwitchCam)
		BREAK
		
		CASE SWITCH_CAM_SETUP_SPLINE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_SETUP_SPLINE")
			
			REQUEST_ANIM_DICT("missswitch")
			load_asset_recording(sAssetData, 004, "dh2arec")

			SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_TREVOR(thisSwitchCam, mike_ped(), vehs[mvf_sub].id)

			IF LOAD_CAM_SHAKE_LIBRARIES(thisSwitchCam)
			AND HAS_ANIM_DICT_LOADED("missswitch")

				DESTROY_ALL_CAMS()			
				
				CREATE_SPLINE_CAM(thisSwitchCam)
				
				SET_CAM_ACTIVE(thisSwitchCam.ciSpline, TRUE)
				SET_CAM_MOTION_BLUR_STRENGTH(thisSwitchCam.ciSpline, 0.1)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)	
				
				SETTIMERB(0)
				
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
				
				bPlayerControlGiven = FALSE
				
				SET_VEHICLE_LIGHTS(vehs[mvf_sub].id, SET_VEHICLE_LIGHTS_ON)
				
				bSwitchCam_SubStartedMoving = FALSE
				
				bSwitchCam_SubPropellersStarted = FALSE
				bSwitchCam_MikeSwitchFX = FALSE
				bSwitchCam_TrevSwitchFX = FALSE
				
				eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE
			ENDIF
		BREAK
		
		CASE SWITCH_CAM_PLAYING_SPLINE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE")
			CDEBUG3LN(DEBUG_MISSION, GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline))

			iCurrentNode = GET_CAM_SPLINE_NODE_INDEX(thisSwitchCam.ciSpline)
//			iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam)
			
			IF IS_CAM_ACTIVE(thisSwitchCam.ciSpline)
				
				IF iCurrentNode >= thisSwitchCam.iCamSwitchFocusNode
					IF NOT bSwitchCam_CharIsSwapped
						IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
							TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
							bSwitchCam_CharIsSwapped = TRUE
						ENDIF
					ENDIF

				ENDIF
				
				IF NOT bSwitchCam_MikeSwitchFX
					IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= fSwitchCam_MikeSwitchFXPhase
						//PLAY_SOUND_FRONTEND(-1, "Camera_Move_One_Shot", "PLAYER_SWITCH_PORT_OF_LS_HEIST_2A_SOUNDSET")
						//PLAY_SOUND_FRONTEND(-1, "Hit_In", "PLAYER_SWITCH_PORT_OF_LS_HEIST_2A_SOUNDSET")
						PLAY_SOUND_FRONTEND(-1, "Hit_Out", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						ANIMPOSTFX_PLAY( "SwitchShortMichaelIn", 0, FALSE )
						bSwitchCam_MikeSwitchFX = TRUE
					ENDIF
				ENDIF
				
				IF NOT bSwitchCam_TrevSwitchFX
					IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= fSwitchCam_TrevSwitchFXPhase
						//PLAY_SOUND_FRONTEND(-1, "Short_Transition_Out", "PLAYER_SWITCH_PORT_OF_LS_HEIST_2A_SOUNDSET")
						PLAY_SOUND_FRONTEND(-1, "Short_Transition_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						ANIMPOSTFX_PLAY( "SwitchShortTrevorMid", 0, FALSE )
						bSwitchCam_TrevSwitchFX = TRUE
					ENDIF
				ENDIF
				
				IF NOT bSwitchCam_SubStartedMoving
					IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= fSwitchCam_SubStartedMovingPhase
						FREEZE_ENTITY_POSITION(vehs[mvf_sub].id, FALSE)
						TASK_PLAY_ANIM(trev_ped(), "missswitch", "mid_mission_inside_helicopter_trevor", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
						
						START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_sub].id, 004, "dh2arec", FALSE)
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_sub].id, 3800)
						bSwitchCam_SubStartedMoving = TRUE
					ENDIF
				ENDIF
				
				IF NOT bSwitchCam_SubPropellersStarted
					IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= fSwitchCam_SubPropellersStarted
						FORCE_SUB_THROTTLE_FOR_TIME(vehs[mvf_sub].id, 1.0, 5.0)
						bSwitchCam_SubPropellersStarted = TRUE
					ENDIF
				ENDIF
				
				IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= 1.00
					CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")
					eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
			
		FALLTHRU

		CASE SWITCH_CAM_RETURN_TO_GAMEPLAY
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")

			SET_TIME_SCALE(1.0)

			RENDER_SCRIPT_CAMS(FALSE, TRUE, iSwitchCam_ReturnToGameplayBlendTime, FALSE)

			IF NOT bPlayerControlGiven
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				DISPLAY_RADAR(TRUE)
				DISPLAY_HUD(TRUE)
				bPlayerControlGiven = TRUE
			ENDIF

			SETTIMERA(0)
			
			NEW_LOAD_SCENE_STOP()
			CLEAR_PED_TASKS(trev_ped())
			STOP_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_sub].id)
			REMOVE_VEHICLE_RECORDING(004, "dh2arec")
					
			SET_PLAYER_CONTROL(PLAYER_ID(), true)
			
			FORCE_SUB_THROTTLE_FOR_TIME(vehs[mvf_sub].id, 1.0, 0.5)
			
			REMOVE_ANIM_DICT("missswitch")
			CDEBUG3LN(DEBUG_MISSION, "Ending Switch Cam")

			eSwitchCamState = SWITCH_CAM_WAIT_FOR_INTERPOLATION
			
			RETURN FALSE

		BREAK
		
		CASE SWITCH_CAM_WAIT_FOR_INTERPOLATION
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_WAIT_FOR_INTERPOLATION")
		
			IF NOT IS_INTERPOLATING_FROM_SCRIPT_CAMS()
				CDEBUG3LN(DEBUG_MISSION, "Returning To Gameplay Cam")
				IF DOES_CAM_EXIST(thisSwitchCam.ciSpline)
					SET_CAM_ACTIVE(thisSwitchCam.ciSpline, FALSE)
					DESTROY_CAM(thisSwitchCam.ciSpline)
				ENDIF
				DESTROY_ALL_CAMS()

				thisSwitchCam.bIsSplineCamFinishedPlaying = TRUE
				sCamDetails.brun = FALSE
				eSwitchCamState = SWITCH_CAM_IDLE	
				
				RETURN TRUE
			ELSE
				RETURN FALSE
			ENDIF
		
		BREAK
		
	ENDSWITCH
	
	IF bReturnTrueOnce
		RETURN FALSE
	ELSE
		RETURN thisSwitchCam.bIsSplineCamFinishedPlaying
	ENDIF
ENDFUNC


#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Create the debug widgets for working with the scripted character switch cam.
PROC CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
	CDEBUG3LN(DEBUG_MISSION, "CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS")

	SET_CURRENT_WIDGET_GROUP(scsSwitchCam_MichaelToTrevor.wgidParent)
	START_WIDGET_GROUP("Custom Switch Cameras - Extra Tunables")
		
		START_WIDGET_GROUP("Michael To Trevor")
			ADD_WIDGET_BOOL("Skip To Switch Scene", bSwitchCamDebug_SkipToSwitch)
			//ADD_WIDGET_VECTOR_SLIDER("Sub Go To Dest", vSwitchCam_SubTargetDest, -100000, 10000, 10.0)
			ADD_WIDGET_FLOAT_SLIDER("Sub Movement Start Phase", fSwitchCam_SubStartedMovingPhase, 0.0, 1.0, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("Sub propellers Start Phase", fSwitchCam_SubPropellersStarted, 0.0, 1.0, 0.1)
			ADD_WIDGET_INT_SLIDER("Return TO Gameplay Cam Duration", iSwitchCam_ReturnToGameplayBlendTime, 0, 5000, 100)
			//ADD_WIDGET_FLOAT_SLIDER("Sub Movement Rec Playback Speed", fSwitchCam_SubRecPlaybackSpeed, 0.0, 10.0, 1.0)
			//ADD_WIDGET_FLOAT_SLIDER("Sub Movement Speed Target", fSwitchCam_SubMovementSpeed, 0.0, 100.0, 1.0)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC HANDLE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
		
//	IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_PAD_LEFT)
//		bdoskip = true
//		iskiptostage = enum_to_int(msf_9_the_goods)
//	ENDIF	
	
	IF bSwitchCamDebug_SkipToSwitch
		//	
		IF int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage) = msf_9_the_goods
			IF NOT IS_SCREEN_FADED_OUT()
			OR NOT IS_SCREEN_FADING_IN()
				DO_SCREEN_FADE_OUT(100)
			ENDIF		
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(players_scene_id)
				SET_SYNCHRONIZED_SCENE_RATE(players_scene_id, 8.0)
			ENDIF
			
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(players_scene_id)
				IF mission_substage > 3
					SET_ENTITY_COORDS(mike_ped(), (GET_ENTITY_COORDS(objLoot) + <<0.0, 0.0, 2.0>>))
					DO_SCREEN_FADE_IN(100)
					bSwitchCamDebug_SkipToSwitch = FALSE
				ENDIF
			ENDIF
		ELSE
			bSwitchCamDebug_SkipToSwitch = FALSE
		ENDIF
	ENDIF

ENDPROC

#ENDIF

// -----------------------------------------------------------------------------------------------------------
//		Initialise
// -----------------------------------------------------------------------------------------------------------
PROC INITIALISE()
	//MERCS
//MSF_1_SNIPE
	MERCS[MERC1_START].STARTLOC 	= << -95.3159, -2370.9175, 13.2960 >>	
	MERCS[MERC1_START].HEAD 		= 166.6539
	MERCS[MERC2_START].STARTLOC 	= << -95.4717, -2371.6418, 13.3370 >>
	MERCS[MERC2_START].HEAD 		= 352.3306
	
	MERCS[MERC3_C4_2].STARTLOC 		= <<-99.3547, -2373.5896, 8.3631>>			//FIRST PATROL GUY BY LADDER
	MERCS[MERC3_C4_2].HEAD			= 94.6563
	MERCS[MERC3_C4_2].VDEFFRANK		= <<-121.5680, -2378.0593, 8.3191>>
	
	MERCS[MERC4_C4_2].STARTLOC		= <<-130.8882, -2381.1301, 8.4830>>			//STAIRS TO SHIP 
	MERCS[MERC4_C4_2].HEAD			= 111.7926
	MERCS[MERC3_C4_2].VDEFFRANK		= <<-131.2666, -2380.9895, 8.4830>>
	
	MERCS[MERC5_C4_2].STARTLOC		= <<-133.4564, -2353.7512, 8.3191>>			//BOMB AREA WALKING BACK AND FORTH
	MERCS[MERC5_C4_2].HEAD			= 269.1131
	MERCS[MERC5_C4_2].VDEFFRANK		= <<-134.2778, -2378.5774, 8.3191>>
	
//	MERCS[MERC6_C4_2].STARTLOC		= <<-100.3547, -2371.0896, 8.3631>>			//PATROL DARK SIDE LADDER WITH 3
//	MERCS[MERC6_C4_2].HEAD			= 264.039
	
	MERCS[MERC6_C4_2].STARTLOC		= <<-136.5821, -2353.4810, 8.3191>>			// PATROL AROUND THE SECOND BLOCK(BEFORE MAST)
	MERCS[MERC6_C4_2].HEAD			= 257.6619
	MERCS[MERC6_C4_2].VDEFFRANK		= <<-140.7227, -2378.9175, 8.3191>>
	
	MERCS[MERC8_C4_2].STARTLOC		= <<-145.6482, -2373.9126, 10.9717>>		//WALKWAY
	MERCS[MERC8_C4_2].HEAD			= 193.1906
	
	MERCS[MERC9_C4_3].STARTLOC		= <<-163.7027, -2358.7537, 8.3191>>			//STATIC DARK SIDE
	MERCS[MERC9_C4_3].HEAD			= 349.0318
	MERCS[MERC9_C4_3].VDEFFRANK		= <<-153.3035, -2378.9810, 8.3191>>
	
	MERCS[MERC10_C4_3].STARTLOC		= <<-175.2808, -2377.9624, 8.3191>>			// PATROL WITH 11
	MERCS[MERC10_C4_3].HEAD			= 105.1062
	MERCS[MERC10_C4_3].VDEFFRANK 	= <<-153.3035, -2378.9810, 8.3191>>
	
	MERCS[MERC11_C4_3].STARTLOC		= <<-175.6108, -2378.5624, 8.3191>>			// PATROL WITH 10
	MERCS[MERC11_C4_3].HEAD			= 83.7819
	MERCS[MERC11_C4_3].VDEFFRANK	= <<-170.2135, -2378.9146, 8.3191>>
	
	MERCS[MERC12_C4_3].STARTLOC		= <<-193.3811, -2358.0469, 10.9719>>		// PATROL THIRD BOMB BALCONY
	MERCS[MERC12_C4_3].HEAD			= 352.3621
	MERCS[MERC12_C4_3].VDEFFRANK	= <<-193.6092, -2378.0381, 10.9717>>
//INSIDE
	MERCS[MERC13_C4_3].STARTLOC		= <<-209.4923, -2381.1301, 8.4833>>			//STATIC STAIRS
	MERCS[MERC13_C4_3].HEAD			= 180.0
	MERCS[MERC13_C4_3].VDEFFRANK	= <<-209.8036, -2380.9172, 8.4833>>
	
	MERCS[MERC14_C4_3].STARTLOC		= <<-190.3405, -2376.1091, 8.3191>>			//PATROL WITH 15
	MERCS[MERC14_C4_3].HEAD			= 120.2464
	MERCS[MERC14_C4_3].VDEFFRANK	= <<-198.1502, -2378.9272, 8.3191>>
	
	MERCS[MERC15_C4_3].STARTLOC		= <<-190.3402, -2372.9521, 8.3191>>			//PATROL WITH 14
	MERCS[MERC15_C4_3].HEAD			= 170.6087
	MERCS[MERC15_C4_3].VDEFFRANK	= <<-204.5925, -2378.8252, 8.3191>>
	
	MERCS[MERC16_C4_4].STARTLOC		= << -221.0865, -2377.1345, 12.3325 >>		//MID STAIRS
	MERCS[MERC16_C4_4].HEAD			= 92.8979
	MERCS[MERC16_C4_4].VDEFFRANK	= << -230.0103, -2379.9412, 12.3327 >>
	
	MERCS[MERC17_C4_4].STARTLOC		= <<-212.1228, -2371.5813, 16.3318>>		//TOP PATROL BALCONY
	MERCS[MERC17_C4_4].HEAD			= 179.6727
	MERCS[MERC17_C4_4].VDEFFRANK	= <<-225.4011, -2377.6538, 16.3326>>
	
	MERCS[MERC18_C4_4].STARTLOC		= << -250.4333, -2355.8440, 8.5061 >>		//PATROL BACK
	MERCS[MERC18_C4_4].HEAD			= 265.6055
	MERCS[MERC18_C4_4].VDEFFRANK	= <<-245.1081, -2377.0864, 8.3192>>
	
	MERCS[merc19_car1].startloc		= <<-185.0, -2512.0, 5.1600>>
	MERCS[merc19_car1].HEAD			= 90
	
	MERCS[merc20_car1].startloc		= <<-184.0, -2512.0, 5.1600>>
	MERCS[merc20_car1].HEAD			= 90
	
	MERCS[merc21_car1].startloc		= <<-183.0, -2512.0, 5.1600>>
	MERCS[merc21_car1].HEAD			= 90
	
	MERCS[merc22_car1].startloc		= <<-182.0, -2512.0, 5.1600>>
	MERCS[merc22_car1].HEAD			= 90
	
	MERCS[merc23_car2].startloc		= <<-185.0, -2513.0, 5.1600>>
	MERCS[merc23_car2].HEAD			= 90
	
	MERCS[merc24_car2].startloc		= <<-184.0, -2513.0, 5.1600>>
	MERCS[merc24_car2].HEAD			= 90
	
	MERCS[merc25_car2].startloc		= <<-183.0, -2513.0, 5.1600>>
	MERCS[merc25_car2].HEAD			= 90
	
	MERCS[merc26_car2].startloc		= <<-182.0, -2513.0, 5.1600>>
	MERCS[merc26_car2].HEAD			= 90
	
	MERCS[merc27_cars].startloc		= <<-209.39200, -2367.15527, 8.31911>>
	MERCS[merc27_cars].HEAD			= 83.7819
	
	MERCS[merc28_cars].startloc		= <<-209.2853, -2375.9495, 8.3191>>
	MERCS[merc28_cars].HEAD			= 83.7819
	
//============================ DEF 1 ============================
	MERCS[MERC1_START].VDEF1		= << -96.8524, -2376.0610, 13.7374 >> //
	MERCS[MERC2_START].VDEF1		= << -95.3553, -2357.4910, 13.7049 >> // 
	MERCS[MERC3_C4_2].VDEF1			= << -100.0022, -2372.7339, 8.8395 >> //
	MERCS[MERC4_C4_2].VDEF1			= << -114.89989, -2377.92456, 8.65607 >>
	MERCS[MERC5_C4_2].VDEF1			= << -121.77245, -2378.60083, 8.31872 >>
	MERCS[MERC6_C4_2].VDEF1			= << -124.83544, -2374.71558, 8.31890 >>
	MERCS[MERC8_C4_2].VDEF1			= << -128.26039, -2378.96094, 8.31891 >>
	MERCS[MERC9_C4_3].VDEF1			= << -92.30511, -2364.47778, 13.29613 >>
	MERCS[MERC10_C4_3].VDEF1		= << -128.59235, -2374.08301, 8.31891 >>
	MERCS[MERC11_C4_3].VDEF1		= << -126.72150, -2367.66284, 8.31891 >>	
	MERCS[MERC12_C4_3].VDEF1		= << -121.77224, -2359.75244, 8.31905 >>	
	MERCS[MERC13_C4_3].VDEF1		= << -124.31957, -2357.06665, 8.31929 >>
	MERCS[MERC14_C4_3].VDEF1		= << -131.76596, -2354.90674, 8.31929 >>
	MERCS[MERC16_C4_4].VDEF1		= << -95.58605, -2374.12793, 13.29636 >>
	MERCS[MERC15_C4_3].VDEF1		= << -136.62875, -2377.87866, 8.31866 >>
	MERCS[MERC17_C4_4].VDEF1		= << -145.02155, -2376.83936, 8.35225 >>
	MERCS[MERC18_C4_4].VDEF1		= << -163.79602, -2376.07031, 8.31863 >>
	
//============================ DEF 2 ============================
	MERCS[MERC1_START].VDEF2		= << -138.2723, -2377.7776, 8.7653 >>
	MERCS[MERC2_START].VDEF2		= << -172.7036, -2377.9490, 8.8366 >>	
	MERCS[MERC3_C4_2].VDEF2			= << -120.05733, -2378.49194, 8.31872 >>
	MERCS[MERC4_C4_2].VDEF2			= << -131.74411, -2376.39160, 8.31891 >>
	MERCS[MERC5_C4_2].VDEF2			= << -127.97167, -2367.26147, 8.31891 >>
	MERCS[MERC6_C4_2].VDEF2			= << -155.54379, -2379.40674, 8.31870 >>
	MERCS[MERC8_C4_2].VDEF2			= << -188.35437, -2373.97168, 8.31869 >>
	MERCS[MERC9_C4_3].VDEF2			= << -178.20407, -2377.00854, 8.31867 >>
	MERCS[MERC10_C4_3].VDEF2		= << -172.43294, -2379.21973, 8.31863 >>
	MERCS[MERC11_C4_3].VDEF2		= << -161.74001, -2376.78613, 8.31863 >>	
	MERCS[MERC12_C4_3].VDEF2		= << -163.63983, -2366.29907, 8.31889 >>	
	MERCS[MERC13_C4_3].VDEF2		= << -147.28908, -2366.81079, 8.31867>>	
	MERCS[MERC14_C4_3].VDEF2		= << -145.35626, -2377.01953, 8.31867 >>	
	MERCS[MERC16_C4_4].VDEF2		= << -163.80838, -2355.11157, 8.31889 >>	
	MERCS[MERC15_C4_3].VDEF2		= << -184.80934, -2356.76099, 8.31944 >>
	MERCS[MERC17_C4_4].VDEF2		= << -187.86914, -2368.31104, 8.31891>>
	MERCS[MERC18_C4_4].VDEF2		= << -181.34264, -2353.20605, 8.31944>>	
	
//============================ DEF 3 ============================
	MERCS[MERC1_START].VDEF3		= << -190.0471, -2374.7861, 9.0480 >>
	MERCS[MERC2_START].VDEF3		= << -181.0299, -2373.9075, 9.0480 >>
	MERCS[MERC3_C4_2].VDEF3			= << -172.33658, -2378.55493, 8.31889 >>
	MERCS[MERC4_C4_2].VDEF3			= << -189.87204, -2378.41504, 8.31869 >>
	MERCS[MERC5_C4_2].VDEF3			= << -184.40628, -2370.45020, 8.31891 >>
	MERCS[MERC6_C4_2].VDEF3			= << -183.70442, -2374.45508, 8.31869 >>
	MERCS[MERC8_C4_2].VDEF3			= << -188.05186, -2356.99048, 8.31944 >>
	MERCS[MERC9_C4_3].VDEF3			= << -188.38849, -2366.64624, 8.31891 >>
	MERCS[MERC10_C4_3].VDEF3		= << -180.21683, -2368.61401, 8.31891 >>
	MERCS[MERC11_C4_3].VDEF3		= << -183.14825, -2363.66626, 8.31944 >>	
	MERCS[MERC12_C4_3].VDEF3		= << -194.14546, -2354.39600, 8.31857 >>	
	MERCS[MERC13_C4_3].VDEF3		= << -200.58470, -2378.65967, 8.31857 >>
	MERCS[MERC14_C4_3].VDEF3		= << -184.26440, -2356.81226, 8.31946 >>	
	MERCS[MERC16_C4_4].VDEF3		= << -213.72719, -2379.04614, 8.31910 >>
	MERCS[MERC15_C4_3].VDEF3		= << -208.06680, -2374.51123, 8.31857 >>
	MERCS[MERC17_C4_4].VDEF3		= << -222.39253, -2377.68506, 12.33246 >>
	MERCS[MERC18_C4_4].VDEF3		= << -241.69656, -2375.63159, 8.31935 >>
	
//============================ DEF 4 ============================
	MERCS[MERC1_START].VDEF4		= << -215.7887, -2377.3557, 8.9309 >>
	MERCS[MERC2_START].VDEF4		= << -236.1554, -2377.2727, 8.8063 >>
	MERCS[MERC3_C4_2].VDEF4			= << -187.78954, -2378.71484, 8.31869 >>
	MERCS[MERC4_C4_2].VDEF4			= << -192.89276, -2376.36646, 8.31857 >>
	MERCS[MERC5_C4_2].VDEF4			= << -205.20845, -2378.42139, 8.31857 >>
	MERCS[MERC6_C4_2].VDEF4			= << -213.65590, -2378.89600, 8.31910 >>
	MERCS[MERC8_C4_2].VDEF4			= << -216.02234, -2375.47559, 8.31910 >>
	MERCS[MERC9_C4_3].VDEF4			= << -230.85701, -2375.86450, 8.31935 >>
	MERCS[MERC10_C4_3].VDEF4		= << -221.32365, -2378.48877, 8.31910 >>
	MERCS[MERC11_C4_3].VDEF4		= << -233.44698, -2376.79468, 8.31911 >>
	MERCS[MERC12_C4_3].VDEF4		= << -210.47473, -2367.42480, 8.31889 >>
	MERCS[MERC13_C4_3].VDEF4		= << -207.19572, -2353.86768, 8.31878 >>
	MERCS[MERC14_C4_3].VDEF4		= << -211.17357, -2355.48755, 8.31854 >>	
	MERCS[MERC16_C4_4].VDEF4		= << -213.02431, -2388.07349, 5.00116 >>
	MERCS[MERC15_C4_3].VDEF4		= << -247.65251, -2374.14819, 8.31935 >>
	MERCS[MERC17_C4_4].VDEF4		= << -216.42989, -2393.81543, 5.02178 >>
	MERCS[MERC18_C4_4].VDEF4		= << -241.69656, -2375.63159, 8.31935 >>

	//CAR 
	MERCS[MERC23_CAR2].VDEF4 = <<-215.1942, -2393.8730, 5.0012>>
	MERCS[MERC24_CAR2].VDEF4 = <<-211.1478, -2399.8049, 5.0012>>
	MERCS[MERC25_CAR2].VDEF4 = <<-216.2771, -2393.3704, 5.0012>>
	MERCS[MERC26_CAR2].VDEF4 = <<-216.6585, -2402.0989, 5.0012>>
	
	MERCS[MERC23_CAR2].VDEF3 = <<-213.7170, -2397.4321, 5.0012>>
	MERCS[MERC24_CAR2].VDEF3 = <<-202.8748, -2411.4336, 5.0012>>
	MERCS[MERC25_CAR2].VDEF3 = <<-215.9651, -2400.3318, 5.0012>>
	MERCS[MERC26_CAR2].VDEF3 = <<-205.8522, -2412.7998, 5.0012>>
	
	MERCS[MERC23_CAR2].VDEF2 = <<-202.8748, -2411.4336, 5.0012>>
	MERCS[MERC24_CAR2].VDEF2 = <<-199.6583, -2426.8000, 5.0012>>
	MERCS[MERC25_CAR2].VDEF2 = <<-205.8522, -2412.7998, 5.0012>>
	MERCS[MERC26_CAR2].VDEF2 = <<-206.0718, -2429.2795, 5.0004>>
	
	MERCS[MERC23_CAR2].VDEF1 = <<-199.6583, -2426.8000, 5.0012>>
	MERCS[MERC24_CAR2].VDEF1 = <<-175.3211, -2452.2039, 5.0130>>
	MERCS[MERC25_CAR2].VDEF1 = <<-206.0718, -2429.2795, 5.0004>>
	MERCS[MERC26_CAR2].VDEF1 = <<-178.0965, -2454.5967, 5.0133>>
	
	MERCS[MERC23_CAR2].iAdvanceTime1 = GET_RANDOM_INT_IN_RANGE(15000, 16000)
	MERCS[MERC24_CAR2].iAdvanceTime1 = GET_RANDOM_INT_IN_RANGE(5000, 6000)
	MERCS[MERC25_CAR2].iAdvanceTime1 = GET_RANDOM_INT_IN_RANGE(15000, 16000)
	MERCS[MERC26_CAR2].iAdvanceTime1 = GET_RANDOM_INT_IN_RANGE(5000, 6000)
	
	MERCS[MERC23_CAR2].iAdvanceTime2 = GET_RANDOM_INT_IN_RANGE(20000, 21000)
	MERCS[MERC24_CAR2].iAdvanceTime2 = GET_RANDOM_INT_IN_RANGE(20000, 21000)
	MERCS[MERC25_CAR2].iAdvanceTime2 = GET_RANDOM_INT_IN_RANGE(20000, 21000)
	MERCS[MERC26_CAR2].iAdvanceTime2 = GET_RANDOM_INT_IN_RANGE(20000, 21000)
	
	MERCS[merc19_car1].VDEF4 = <<-201.4169, -2400.3948, 5.0012>>
	MERCS[merc20_car1].VDEF4 = <<-211.1253, -2389.9844, 5.0012>>
	MERCS[merc21_car1].VDEF4 = <<-197.0782, -2397.9023, 5.0013>>
	MERCS[merc22_car1].VDEF4 = <<-209.4439, -2388.5757, 5.0012>>
	
	MERCS[merc19_car1].VDEF3 = <<-193.7419, -2422.9417, 5.0007>>
	MERCS[merc20_car1].VDEF3 = <<-202.1372, -2396.9736, 5.0012>>
	MERCS[merc21_car1].VDEF3 = <<-184.7490, -2422.0982, 5.0013>>
	MERCS[merc22_car1].VDEF3 = <<-198.5648, -2396.6946, 5.0013>>
	
	MERCS[merc19_car1].VDEF2 = <<-182.8242, -2430.2419, 5.0013>>
	MERCS[merc20_car1].VDEF2 = <<-193.7419, -2422.9417, 5.0007>>
	MERCS[merc21_car1].VDEF2 = <<-178.9606, -2429.8459, 5.0013>>
	MERCS[merc22_car1].VDEF2 = <<-184.7490, -2422.0982, 5.0013>>
	
	MERCS[merc19_car1].VDEF1 = <<-169.6456, -2449.3494, 5.0139>>
	MERCS[merc20_car1].VDEF1 = <<-182.8242, -2430.2419, 5.0013>>
	MERCS[merc21_car1].VDEF1 = <<-172.6127, -2450.1057, 5.0129>>
	MERCS[merc22_car1].VDEF1 = <<-178.9606, -2429.8459, 5.0013>>
	
	MERCS[merc19_car1].iAdvanceTime1 = GET_RANDOM_INT_IN_RANGE(10000, 11000)
	MERCS[merc20_car1].iAdvanceTime1 = GET_RANDOM_INT_IN_RANGE(20000, 21000)
	MERCS[merc21_car1].iAdvanceTime1 = GET_RANDOM_INT_IN_RANGE(10000, 11000)
	MERCS[merc22_car1].iAdvanceTime1 = GET_RANDOM_INT_IN_RANGE(20000, 21000)
	
	MERCS[merc19_car1].iAdvanceTime2 = GET_RANDOM_INT_IN_RANGE(20000, 21000)
	MERCS[merc20_car1].iAdvanceTime2 = GET_RANDOM_INT_IN_RANGE(20000, 21000)
	MERCS[merc21_car1].iAdvanceTime2 = GET_RANDOM_INT_IN_RANGE(20000, 21000)
	MERCS[merc22_car1].iAdvanceTime2 = GET_RANDOM_INT_IN_RANGE(20000, 21000)
	
	MERCS[merc27_cars].vdef4 = << -212.01079, -2386.45850, 5.00033 >>
	MERCS[merc28_cars].vdef4 = << -217.41843, -2391.90576, 5.00033 >>
	
	MERCS[merc27_cars].vdef3 = << -212.01079, -2386.45850, 5.00033 >>
	MERCS[merc28_cars].vdef3 = << -217.41843, -2391.90576, 5.00033 >>
	
	MERCS[merc27_cars].vdef2 = <<-213.7170, -2397.4321, 5.0012>>
	MERCS[merc28_cars].vdef2 = <<-215.9651, -2400.3318, 5.0012>>
	
	MERCS[merc27_cars].vdef1 = <<-202.8748, -2411.4336, 5.0012>>
	MERCS[merc28_cars].vdef1 = <<-205.8522, -2412.7998, 5.0012>>
	
	MERCS[merc27_cars].iAdvanceTime1 = GET_RANDOM_INT_IN_RANGE(25000, 26000)
	MERCS[merc28_cars].iAdvanceTime1 = GET_RANDOM_INT_IN_RANGE(25000, 26000)
	
	MERCS[merc27_cars].iAdvanceTime2 = GET_RANDOM_INT_IN_RANGE(20000, 21000)
	MERCS[merc28_cars].iAdvanceTime2 = GET_RANDOM_INT_IN_RANGE(20000, 21000)
	
	MERCS[merc29_heli].startloc		= <<-76.5442, -2581.7407, 5.0052>>
	MERCS[merc29_heli].HEAD			= 267.4709
	
	//BOMBS
	C4[C4_0_FRONT_OF_SHIP].EXPPOS	= << -83.8000, -2365.8125, 14.5418 >> 
	C4[C4_1_FIRST_MAST].EXPPOS		= << -128.1432, -2366.0024, 8.3193 >>
	C4[C4_2_SECOND_MAST].EXPPOS		= << -187.9897, -2365.4224, 8.3193 >>
	C4[C4_3_INSIDE].EXPPOS			= << -221.3571, -2375.6523, 12.3325 >>
	
	//DOCK WORKERS 
	PEDS[MPF_DOCK1].STARTLOC	= <<-262.60956, -2401.81250, 5.00131>> 	PEDS[MPF_DOCK1].HEAD =  0
	PEDS[MPF_DOCK1].VCOV	= <<-178.86514, -2511.78882, 5.14006>>
	PEDS[MPF_DOCK2].STARTLOC	= << -60.73100, -2407.58716, 5.00087 >> PEDS[MPF_DOCK2].HEAD =  0
	PEDS[MPF_DOCK2].VCOV	= <<-156.21315, -2516.67041, 5.00866>>
	
//	bCarCombatStarted = FALSE
	
ENDPROC
PROC SETUPMERCPATROL()	
		//@RJP heavily changing patrol routes
		
		//CONTAINERS BALCONY
		OPEN_PATROL_ROUTE("MISS_PATROL_6")			
			ADD_PATROL_ROUTE_NODE(1, "WORLD_HUMAN_GUARD_STAND", <<-145.5457, -2378.8364, 10.9717>>, <<-145.5457, -2378.8364, 10.9717>>, 3000)
			ADD_PATROL_ROUTE_NODE(2, "WORLD_HUMAN_GUARD_STAND", <<-145.5312, -2353.4207, 10.9717>>, <<-145.5312, -2353.4207, 10.9717>>, 3000)
			ADD_PATROL_ROUTE_LINK(1, 2)
			ADD_PATROL_ROUTE_LINK(2, 1)
		CLOSE_PATROL_ROUTE()
		CREATE_PATROL_ROUTE()
		
		//THIRD BOMB BALCONY
		OPEN_PATROL_ROUTE("MISS_PATROL_7")
			ADD_PATROL_ROUTE_NODE(1, "WORLD_HUMAN_GUARD_STAND", <<-193.5774, -2353.0503, 10.9719>>, <<-193.5774, -2353.0503, 10.9719>>, 3000)
			ADD_PATROL_ROUTE_NODE(2, "WORLD_HUMAN_GUARD_STAND", <<-193.4915, -2378.8650, 10.9719>>, <<-193.4915, -2378.8650, 10.9719>>, 3000)
			ADD_PATROL_ROUTE_LINK(1, 2)
			ADD_PATROL_ROUTE_LINK(2, 1)
		CLOSE_PATROL_ROUTE()
		CREATE_PATROL_ROUTE()
		
		//PATROL TOP CABIN BALCONY
		OPEN_PATROL_ROUTE("MISS_PATROL_8")
			ADD_PATROL_ROUTE_NODE(1, "WORLD_HUMAN_GUARD_STAND", <<-212.1342, -2377.4421, 16.3326>>, <<-212.1342, -2377.4421, 16.3326>>, 3000)
			ADD_PATROL_ROUTE_NODE(2, "WORLD_HUMAN_GUARD_STAND", <<-211.9285, -2355.0430, 16.3321>>, <<-211.9285, -2355.0430, 16.3321>>, 3000)
			ADD_PATROL_ROUTE_LINK(1, 2)
			ADD_PATROL_ROUTE_LINK(2, 1)
		CLOSE_PATROL_ROUTE()
		CREATE_PATROL_ROUTE()
		
		//PATROL STERN OF SHIP
		OPEN_PATROL_ROUTE("MISS_PATROL_9")
			ADD_PATROL_ROUTE_NODE(1, "WORLD_HUMAN_GUARD_STAND",MERCS[MERC18_C4_4].STARTLOC,MERCS[MERC18_C4_4].STARTLOC,2000)
			ADD_PATROL_ROUTE_NODE(2, "WORLD_HUMAN_GUARD_STAND",<< -250.2186, -2377.0210, 8.5309 >>,<< -250.2186, -2377.0210, 8.5309 >> ,2000)
			ADD_PATROL_ROUTE_LINK(1, 2)
			ADD_PATROL_ROUTE_LINK(2, 1)
		CLOSE_PATROL_ROUTE()
		CREATE_PATROL_ROUTE()
		
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		MISSION STAGE MANAGEMENT
// -----------------------------------------------------------------------------------------------------------

proc mission_stage_management()
	switch stageswitch
		case stageswitch_requested
			println("[stagemanagement] mission_stage switch requested from mission_stage:", mission_stage, "to mission_stage", requestedstage)
			stageswitch = stageswitch_exiting
			mission_substage = stage_exit
		break
		case stageswitch_exiting
			println("[stagemanagement] exiting mission_stage: ", mission_stage)
			stageswitch = stageswitch_entering
			mission_substage = stage_entry
			mission_stage = requestedstage
		break
		case stageswitch_entering
			println("[stagemanagement] entered mission_stage: ", mission_stage)
			requestedstage = -1
			stageswitch = stageswitch_idle
		break
		case stageswitch_idle
			if (get_game_timer() - istagetimer) > 2500
			println("[stagemanagement] mission_stage: ", mission_stage, " mission_substage: ", mission_substage)
				istagetimer = get_game_timer()
			endif
		break
	endswitch
endproc
func bool mission_set_stage(msf_mission_stage_flags newstage)
	if stageswitch = stageswitch_idle
		requestedstage = enum_to_int(newstage)
		stageswitch = stageswitch_requested		
		return true
	else
		return false
	endif
endfunc
// -----------------------------------------------------------------------------------------------------------
//		HOT SWAP
// -----------------------------------------------------------------------------------------------------------
PROC HOTSWAP_MENU(BOOL BBLOCKMIKE = FALSE, BOOL BBLOCKFRANK = FALSE, BOOL BBLOCKTREV = FALSE,
					BOOL BHINTMIKE = FALSE, BOOL BHINTFRANK = FALSE, BOOL BHINTTREV = FALSE)
	//ENABLE OR DISABLE SELECABILTY IN THE HOT SWAP
	IF ISENTITYALIVE(PEDS[MPF_FRANK].ID)
		SSELECTORPEDS.PEDID[SELECTOR_PED_FRANKLIN] = PEDS[MPF_FRANK].ID
		SET_SELECTOR_PED_BLOCKED(SSELECTORPEDS,SELECTOR_PED_FRANKLIN,BBLOCKFRANK) 
		SET_SELECTOR_PED_HINT(SSELECTORPEDS,SELECTOR_PED_FRANKLIN,BHINTFRANK)
	ELSE
		SET_SELECTOR_PED_BLOCKED(SSELECTORPEDS,SELECTOR_PED_FRANKLIN,TRUE)
	ENDIF
	
	IF ISENTITYALIVE(PEDS[MPF_TREV].ID)
	
		SSELECTORPEDS.PEDID[SELECTOR_PED_TREVOR] = PEDS[MPF_TREV].ID 
		SET_SELECTOR_PED_BLOCKED(SSELECTORPEDS,SELECTOR_PED_TREVOR,BBLOCKTREV) 
		SET_SELECTOR_PED_HINT(SSELECTORPEDS,SELECTOR_PED_TREVOR,BHINTTREV)
	ELSE
		SET_SELECTOR_PED_BLOCKED(SSELECTORPEDS,SELECTOR_PED_TREVOR,TRUE)
	ENDIF
	
	IF ISENTITYALIVE(PEDS[MPF_MIKE].ID)	
		SSELECTORPEDS.PEDID[SELECTOR_PED_MICHAEL] = PEDS[MPF_MIKE].ID
		SET_SELECTOR_PED_BLOCKED(SSELECTORPEDS,SELECTOR_PED_MICHAEL,BBLOCKMIKE) 
		SET_SELECTOR_PED_HINT(SSELECTORPEDS,SELECTOR_PED_MICHAEL,BHINTMIKE)
	ELSE
		SET_SELECTOR_PED_BLOCKED(SSELECTORPEDS,SELECTOR_PED_MICHAEL,TRUE)
	ENDIF
ENDPROC
FUNC BOOL DISPLAY_SELECTOR_HUD()
	IF UPDATE_SELECTOR_HUD(SSELECTORPEDS, FALSE)     // RETURNS TRUE WHEN THE PLAYER HAS MADE A SELECTION
		IF NOT HAS_SELECTOR_PED_BEEN_SELECTED(SSELECTORPEDS, SELECTOR_PED_MULTIPLAYER)
			SCAMDETAILS.PEDTO = SSELECTORPEDS.PEDID[SSELECTORPEDS.ENEWSELECTORPED]
//			SCAMDETAILS.BRUN  = TRUE
			RETURN TRUE
		ENDIF
	ENDIF	
	RETURN FALSE
ENDFUNC
//PURPOSE: CARRIES OUT A SWAP USING THE SCAMDETAIL STRUCT FOR CAM INTERPOLATION
FUNC SWITCHSTATE DO_HOTSWAP(SELECTOR_CAM_INTERP_TYPE SINTERP = SELECTOR_CAM_DEFAULT)
	
	IF NOT SCAMDETAILS.BRUN
		RETURN SWITCH_NOSTATE
	ELIF mission_stage = ENUM_TO_INT( msf_5_fourth_bomb )
	AND mission_substage > 2
		IF RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(SCAMDETAILS,0,0,SINTERP,IINTERPBACKTOGAMEOVERRIDE,IINTERPTOSWITCHCAM)
			RETURN SWITCH_IN_PROGRESS
		ELSE
			SCAMDETAILS.BRUN  = FALSE
			RETURN SWITCH_COMPLETE
		ENDIF
	//@RJP short code switch if swapping between Michael and Franklin when Michael is on the ship
//	ELIF mission_stage > ENUM_TO_INT( msf_0_drive_to_bridge )
//	AND mission_stage < ENUM_TO_INT( msf_6_way_out )
		//@RJP if swapping from franklin, give a bit of time to get out of aiming before doing the short code switch
//		IF PLAYER_PED_ID() = frank_ped()
//			IF iFranklinCamSwapTimer > -1
//				IF GET_GAME_TIMER() > iFranklinCamSwapTimer
//					IF DOES_CAM_EXIST(sCamDetails.camTo)
//						if RUN_SWITCH_CAM_FROM_PLAYER_TO_CAM( sCamDetails, sCamDetails.camTo, SWITCH_TYPE_AUTO )
//							RETURN SWITCH_IN_PROGRESS
//						ELSE
//							SET_PLAYER_CONTROL( PLAYER_ID(), TRUE )
//							iFranklinCamSwapTimer = -1
//							SCAMDETAILS.BRUN  = FALSE
//							RETURN SWITCH_COMPLETE
//						ENDIF
//					ELSE
//						IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED( sCamDetails, SWITCH_TYPE_AUTO )
//							RETURN SWITCH_IN_PROGRESS
//						ELSE
//							SET_PLAYER_CONTROL( PLAYER_ID(), TRUE )
//							iFranklinCamSwapTimer = -1
//							SCAMDETAILS.BRUN  = FALSE
//							RETURN SWITCH_COMPLETE
//						ENDIF
//					ENDIF
//				ENDIF
//			ELSE
//				iFranklinCamSwapTimer = (GET_GAME_TIMER() + 350)
//				SET_PLAYER_CONTROL( PLAYER_ID(), FALSE )
////				SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), FALSE)
//				SET_PLAYER_FORCED_AIM(PLAYER_ID(), FALSE)
//			ENDIF
//		ELIF DOES_CAM_EXIST(sCamDetails.camTo)
//			if RUN_SWITCH_CAM_FROM_PLAYER_TO_CAM( sCamDetails, sCamDetails.camTo, SWITCH_TYPE_AUTO )
//				RETURN SWITCH_IN_PROGRESS
//			ELSE
//				SCAMDETAILS.BRUN  = FALSE
//				RETURN SWITCH_COMPLETE
//			ENDIF
//		ELSE
//			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED( sCamDetails, SWITCH_TYPE_AUTO )
//				RETURN SWITCH_IN_PROGRESS
//			ELSE
//				SCAMDETAILS.BRUN  = FALSE
//				RETURN SWITCH_COMPLETE
//			ENDIF
//		ENDIF
//	//@RJP otherwise auto code switch
	ELSE
		IF DOES_CAM_EXIST(sCamDetails.camTo)
			if RUN_SWITCH_CAM_FROM_PLAYER_TO_CAM( sCamDetails, sCamDetails.camTo, SWITCH_TYPE_AUTO )
				RETURN SWITCH_IN_PROGRESS
			ELSE
				SCAMDETAILS.BRUN  = FALSE
				RETURN SWITCH_COMPLETE
			ENDIF
		ELSE
			IF mission_stage = ENUM_TO_INT(msf_9_the_goods)
				IF DOES_CAM_EXIST(cameraIndex)
					RENDER_SCRIPT_CAMS(FALSE,FALSE)
					IF IS_CAM_RENDERING(cameraIndex)
						SET_CAM_ACTIVE(cameraIndex,false)
					ENDIF
					DESTROY_CAM(cameraIndex)
				ENDIF
			ENDIF
			IF MISSION_STAGE > ENUM_TO_INT(msf_1_snipe_gaurds)
			AND MISSION_STAGE < ENUM_TO_INT(msf_6_way_out)
				IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED( sCamDetails, SWITCH_TYPE_SHORT )
					RETURN SWITCH_IN_PROGRESS
				ELSE
					SCAMDETAILS.BRUN  = FALSE
					RETURN SWITCH_COMPLETE
				ENDIF
			ELSE
				IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED( sCamDetails, SWITCH_TYPE_AUTO )
					RETURN SWITCH_IN_PROGRESS
				ELSE
					SCAMDETAILS.BRUN  = FALSE
					RETURN SWITCH_COMPLETE
				ENDIF
			ENDIF
		ENDIF
//		IF MISSION_STAGE = ENUM_TO_INT(MSF_1_SNIPE_GAURDS)
//		or (MISSION_STAGE = ENUM_TO_INT(MSF_1_SNIPE_GAURDS)
//		and mission_substage >= 13)
//			IF RUN_CAM_SPLINE_FROM_PLAYER_TO_CAM(SCAMDETAILS,400,400) // RETURNS FALSE WHEN THE CAMERA SPLINE IS COMPLETE
////			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_CAM( SCAMDETAILS, sCamDetails.camTo, SWITCH_TYPE_AUTO )
//				RETURN SWITCH_IN_PROGRESS
//			ELSE				
//				SCAMDETAILS.BRUN  = FALSE
//				RETURN SWITCH_COMPLETE
//			ENDIF
//		ELSE
//			IF RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(SCAMDETAILS,0,0,SINTERP,IINTERPBACKTOGAMEOVERRIDE,IINTERPTOSWITCHCAM)	// RETURNS FALSE WHEN THE CAMERA SPLINE IS COMPLETE
////			IF RUN_SWITCH_CAM_FROM_PLAYER_TO_PED( SCAMDETAILS, SWITCH_TYPE_AUTO )
//				RETURN SWITCH_IN_PROGRESS
//			ELSE				
//				SCAMDETAILS.BRUN  = FALSE
//				RETURN SWITCH_COMPLETE
//			ENDIF
//		ENDIF
	ENDIF		
	RETURN SWITCH_NOSTATE	
ENDFUNC
FUNC BOOL HOTSWAP_CAM(SELECTOR_SLOTS_ENUM TARGETPED, BOOL BISCUTSCENE = FALSE,INT INTERP_TIME = 4000)
	FLOAT DISTANCE_BETWEEN_PEDS
	
	IF NOT SCAMDETAILS.BSPLINECREATED 
		IF ISENTITYALIVE(SSELECTORPEDS.PEDID[TARGETPED])
			DISTANCE_BETWEEN_PEDS = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(SSELECTORPEDS.PEDID[TARGETPED]))
						
			IF DISTANCE_BETWEEN_PEDS < 8.0
			and not IS_PED_IN_ANY_VEHICLE(SSELECTORPEDS.PEDID[TARGETPED])
				INTERP_TIME = 2000				
			ENDIF 
						
			IF NOT DOES_CAM_EXIST(SCAMDETAILS.CAMID)
				SCAMDETAILS.CAMID = CREATE_CAM("DEFAULT_SPLINE_CAMERA", FALSE) 
			ENDIF  
//-----------------------------------------START NODE---------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------
			IF BISCUTSCENE 
				IF DOES_CAM_EXIST(CAMERAINDEX)
					SET_CAM_FOV(SCAMDETAILS.CAMID,GET_CAM_FOV(CAMERAINDEX)) 
//					ADD_CAM_SPLINE_NODE(SCAMDETAILS.CAMID, GET_CAM_COORD(CAMERAINDEX), GET_CAM_ROT(CAMERAINDEX),0)   
					ADD_CAM_SPLINE_NODE_USING_CAMERA_FRAME(SCAMDETAILS.CAMID,CAMERAINDEX,0,CAM_SPLINE_NODE_SMOOTH_ROT)
				ENDIF
				IINTERPTOSWITCHCAM = 0
			ELSE
				SET_CAM_FOV(SCAMDETAILS.CAMID, 45)
				IF SSELECTORPEDS.PEDID[TARGETPED] = FRANK_PED()					
					POINT_CAM_AT_ENTITY(SCAMDETAILS.CAMID,MIKE_PED(),<<0,0,0>>)
					IINTERPTOSWITCHCAM = 3000
				ELSE
					POINT_CAM_AT_ENTITY(SCAMDETAILS.CAMID,SSELECTORPEDS.PEDID[TARGETPED],<<0,0,0>>)
					IINTERPTOSWITCHCAM = 2000
				ENDIF				
				
				ADD_CAM_SPLINE_NODE(SCAMDETAILS.CAMID, GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), 0)  				
			ENDIF
//------------------------------------------------------------------------------------------------------------------------
//------------------------------------------------------------------------------------------------------------------------			
			IF IS_PED_IN_ANY_VEHICLE(SSELECTORPEDS.PEDID[TARGETPED],TRUE)																				
				ADD_CAM_SPLINE_NODE(SCAMDETAILS.CAMID, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(GET_VEHICLE_PED_IS_IN(SCAMDETAILS.PEDTO), GET_OFFSET_FOR_DEST_CAM(SCAMDETAILS.PEDTO)),GET_ENTITY_ROTATION(SSELECTORPEDS.PEDID[TARGETPED]),5000)						
				ADD_CAM_SPLINE_NODE_USING_GAMEPLAY_FRAME(SCAMDETAILS.CAMID,1000,CAM_SPLINE_NODE_SMOOTH_ROT)
			ELSE			
				VECTOR ENTITYROT = GET_ENTITY_ROTATION(SSELECTORPEDS.PEDID[TARGETPED])
				VECTOR VOFFSETCAM
				
				IF SSELECTORPEDS.PEDID[TARGETPED] = FRANK_PED()
					VOFFSETCAM = <<0.2,-2,0.8>> //GET_OFFSET_FOR_DEST_CAM(SSELECTORPEDS.PEDID[TARGETPED]) + <<0.1,0,0.5>>
					ADD_CAM_SPLINE_NODE(SCAMDETAILS.CAMID, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(SSELECTORPEDS.PEDID[TARGETPED],<<0.5,-0.45,0.6>> ) ,ENTITYROT, 5000)						
					ADD_CAM_SPLINE_NODE(SCAMDETAILS.CAMID, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(SSELECTORPEDS.PEDID[TARGETPED],<<0.3,-0.2,0.6>> ) ,ENTITYROT, 5000)
				ELIF SSELECTORPEDS.PEDID[TARGETPED] = MIKE_PED()
					IF IS_PED_IN_COVER(SSELECTORPEDS.PEDID[TARGETPED])	
						VOFFSETCAM = <<0,2,1.5>>
					ELSE
						VOFFSETCAM = GET_OFFSET_FOR_DEST_CAM(SSELECTORPEDS.PEDID[TARGETPED]) + <<0,1,1.5>>
					ENDIF
					ADD_CAM_SPLINE_NODE(SCAMDETAILS.CAMID, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(SSELECTORPEDS.PEDID[TARGETPED],VOFFSETCAM) ,ENTITYROT, 5000)
					ADD_CAM_SPLINE_NODE_USING_GAMEPLAY_FRAME(SCAMDETAILS.CAMID,1000)
				ELIF SSELECTORPEDS.PEDID[TARGETPED] = TREV_PED()
					VOFFSETCAM = GET_OFFSET_FOR_DEST_CAM(SSELECTORPEDS.PEDID[TARGETPED])
					ADD_CAM_SPLINE_NODE(SCAMDETAILS.CAMID, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(SSELECTORPEDS.PEDID[TARGETPED],VOFFSETCAM) ,ENTITYROT, 5000)
					ADD_CAM_SPLINE_NODE_USING_GAMEPLAY_FRAME(SCAMDETAILS.CAMID,1000)
				ENDIF										
			ENDIF 
			
			SET_CAM_SPLINE_SMOOTHING_STYLE(SCAMDETAILS.CAMID,CAM_SPLINE_SLOW_OUT_SMOOTH)
			SET_CAM_SPLINE_DURATION(SCAMDETAILS.CAMID, INTERP_TIME)
			
			SCAMDETAILS.BSPLINECREATED = TRUE			
			SCAMDETAILS.PEDTO =  SSELECTORPEDS.PEDID[TARGETPED]
			
			RETURN TRUE
		ENDIF
		
	ENDIF 	
	RETURN FALSE
ENDFUNC
FUNC BOOL SET_CURRENT_PLAYER_PED(SELECTOR_SLOTS_ENUM PEDCHAR, BOOL BWAIT = FALSE, BOOL BCLEANUPMODEL = TRUE)

	IF BWAIT
		WHILE NOT SET_CURRENT_SELECTOR_PED(PEDCHAR, BCLEANUPMODEL)	
			WAIT(0)
		ENDWHILE
	ELSE
		IF NOT SET_CURRENT_SELECTOR_PED(PEDCHAR, BCLEANUPMODEL)
			RETURN FALSE
		ENDIF
	ENDIF
			
	// ADDITIONAL STUFF TO SET UP PED FOR USE IN MISSION
	SWITCH PEDCHAR
		CASE SELECTOR_PED_MICHAEL
			PEDS[MPF_MIKE].ID = PLAYER_PED_ID()
		BREAK
		CASE SELECTOR_PED_FRANKLIN
			PEDS[MPF_FRANK].ID = PLAYER_PED_ID()
		BREAK
		CASE SELECTOR_PED_TREVOR
			PEDS[MPF_TREV].ID = PLAYER_PED_ID()			
		BREAK
	ENDSWITCH
	
	RETURN TRUE
ENDFUNC
// PURPOSE: RUN EVERYFRAME FOR HOTSWAPPING OPTION. 
PROC MANAGE_HOTSWAP()
	//HOT SWAP CHECKS-------------------------------------------------
SWITCHSTATE HSSTATUS
	IF BHOTSWAP			
		IF PLAYER_PED_ID() = MIKE_PED()
//			SET_SELECTOR_PED_HINT(SSELECTORPEDS,SELECTOR_PED_FRANKLIN,TRUE)
			SET_SELECTOR_PED_PRIORITY(SSELECTORPEDS,SELECTOR_PED_FRANKLIN,SELECTOR_PED_MICHAEL,SELECTOR_PED_TREVOR)
		ELIF PLAYER_PED_ID() = FRANK_PED()
//			SET_SELECTOR_PED_HINT(SSELECTORPEDS,SELECTOR_PED_MICHAEL,TRUE)
			SET_SELECTOR_PED_PRIORITY(SSELECTORPEDS,SELECTOR_PED_MICHAEL,SELECTOR_PED_FRANKLIN,SELECTOR_PED_TREVOR)
		ENDIF
		IF BFORCED = FALSE
			IF NOT SCAMDETAILS.BRUN 			
				IF DISPLAY_SELECTOR_HUD()
					INFORM_MISSION_STATS_OF_INCREMENT(DH2A_SWITCHES)
//====================================================== UI NEW PED CHOSEN ============================================================					
					IF IS_MESSAGE_BEING_DISPLAYED()
						CLEAR_PRINTS()
					ENDIF
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
						CLEAR_HELP()
					ENDIF	
		//========================================= DINGHY SWITCH ==============================================		
					IF MISSION_STAGE > ENUM_TO_INT(msf_0_drive_to_bridge)
					AND MISSION_STAGE < ENUM_TO_INT(msf_6_way_out)
						IF SSELECTORPEDS.ENEWSELECTORPED = SELECTOR_PED_FRANKLIN
							IF isentityalive( frank_ped() )
								SET_FOCUS_ENTITY( frank_ped() )
								CLEAR_PED_TASKS( frank_ped() )
							ENDIF
						ENDIF
					ENDIF
		
		//========================================= BOMB STAGES ===============================================		
					//SWITCH AUDIO SCENE
					IF MISSION_STAGE >= ENUM_TO_INT(MSF_2_FIRST_BOMB)			
					AND MISSION_STAGE <= ENUM_TO_INT(MSF_4_THIRD_BOMB)
						IF SSELECTORPEDS.ENEWSELECTORPED = SELECTOR_PED_MICHAEL	
							int merc
							for merc = enum_to_int(merc3_C4_2) to enum_to_int(merc18_c4_4)
								if DOES_BLIP_EXIST(mercs[merc].blip)
									remove_blip(mercs[merc].blip)
								endif
							endfor
							IF NOT BALARMTRIGGERED
								IF IS_AUDIO_SCENE_ACTIVE("DH2A_SNIPING_SCENE")
									STOP_AUDIO_SCENE("DH2A_SNIPING_SCENE")
								ENDIF
								START_AUDIO_SCENE("DH2A_PLANT_BOMBS_SCENE")
							ELSE
								IF IS_AUDIO_SCENE_ACTIVE("DH2A_SHOOTOUT_SNIPING_SCENE")
									STOP_AUDIO_SCENE("DH2A_SHOOTOUT_SNIPING_SCENE")
								ENDIF
								START_AUDIO_SCENE("DH2A_SHOOTOUT_SCENE")
							ENDIF
						ELIF SSELECTORPEDS.ENEWSELECTORPED = SELECTOR_PED_FRANKLIN
							IF NOT BALARMTRIGGERED
								IF IS_AUDIO_SCENE_ACTIVE("DH2A_PLANT_BOMBS_SCENE")
									STOP_AUDIO_SCENE("DH2A_PLANT_BOMBS_SCENE")
								ENDIF
								START_AUDIO_SCENE("DH2A_SNIPING_SCENE")
							ELSE
								IF IS_AUDIO_SCENE_ACTIVE("DH2A_SHOOTOUT_SCENE")
									STOP_AUDIO_SCENE("DH2A_SHOOTOUT_SCENE")
								ENDIF
								START_AUDIO_SCENE("DH2A_SHOOTOUT_SNIPING_SCENE")
							ENDIF
						ENDIF
						
					ENDIF
					
		//================================================= READY TO RUN CAM =======================================================					
					SCAMDETAILS.BRUN  = TRUE
				ENDIF				
			ENDIF
		endif
//====================================================== START SWITCH ============================================================			
		HSSTATUS = DO_HOTSWAP(SELECTOR_CAM_SHORT_SPLINE)
		
//======================================================    RUNNING   ============================================================					
		IF HSSTATUS = SWITCH_IN_PROGRESS
			IF SCAMDETAILS.BOKTOSWITCHPED
				IF NOT SCAMDETAILS.BPEDSWITCHED				
					IF TAKE_CONTROL_OF_SELECTOR_PED(SSELECTORPEDS, TRUE, TRUE)		
//=======================================  SWITCHED  =============================================					
						SCAMDETAILS.BPEDSWITCHED = TRUE					
						IF ISENTITYALIVE(PEDS[MPF_FRANK].ID)		
							IF SSELECTORPEDS.PEDID[SELECTOR_PED_FRANKLIN] = NULL
								PEDS[MPF_FRANK].ID = PLAYER_PED_ID()
							ELSE
								PEDS[MPF_FRANK].ID = SSELECTORPEDS.PEDID[SELECTOR_PED_FRANKLIN] 
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PEDS[MPF_FRANK].ID, TRUE)
								SET_ENTITY_LOAD_COLLISION_FLAG(PEDS[MPF_FRANK].ID,TRUE)
								SET_PED_RELATIONSHIP_GROUP_HASH(frank_ped(),rel_group_buddy)
							ENDIF
						ENDIF					
						IF ISENTITYALIVE(PEDS[MPF_TREV].ID)		
							IF SSELECTORPEDS.PEDID[SELECTOR_PED_TREVOR] = NULL
								PEDS[MPF_TREV].ID = PLAYER_PED_ID()						
							ELSE
								PEDS[MPF_TREV].ID = SSELECTORPEDS.PEDID[SELECTOR_PED_TREVOR] 
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PEDS[MPF_TREV].ID, TRUE)
								SET_ENTITY_LOAD_COLLISION_FLAG(PEDS[MPF_TREV].ID,TRUE)
								SET_PED_RELATIONSHIP_GROUP_HASH(trev_ped(),rel_group_buddy)
							ENDIF
						ENDIF					
						IF ISENTITYALIVE(PEDS[MPF_MIKE].ID)		
							IF SSELECTORPEDS.PEDID[SELECTOR_PED_MICHAEL] = NULL
								PEDS[MPF_MIKE].ID = PLAYER_PED_ID()		
								IF MISSION_STAGE = ENUM_TO_INT(MSF_1_SNIPE_GAURDS)									
									TRIGGER_MUSIC_EVENT("DH2A_1ST_SWITCH")									
								ENDIF
								IF MISSION_STAGE = ENUM_TO_INT(msf_0_drive_to_bridge)
									TRIGGER_MUSIC_EVENT("DH2A_RIB")
								endif
							ELSE
								PEDS[MPF_MIKE].ID = SSELECTORPEDS.PEDID[SELECTOR_PED_MICHAEL]	
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(PEDS[MPF_MIKE].ID, TRUE)
								SET_ENTITY_LOAD_COLLISION_FLAG(PEDS[MPF_MIKE].ID,TRUE)
								SET_PED_RELATIONSHIP_GROUP_HASH(mike_ped(),rel_group_buddy)
							ENDIF	
						ENDIF					
					ENDIF
				ELSE
					IF SCAMDETAILS.PEDTO = FRANK_PED()	
					and MISSION_STAGE >= ENUM_TO_INT(MSF_1_SNIPE_GAURDS)
//						SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(),  TRUE)
//						SET_PLAYER_FORCED_AIM(PLAYER_ID(), TRUE)
//						SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_SNIPER_RIFLE_AIM_HEADING_FOR_PED(MIKE_PED()))
//						SET_GAMEPLAY_CAM_RELATIVE_PITCH(GET_SNIPER_RIFLE_AIM_PITCH_FOR_PED(MIKE_PED()))
//						SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(7)
						//@RJP removing giving Mike a ton of health
						IF GET_ENTITY_HEALTH(MIKE_PED()) < 200
							SET_ENTITY_HEALTH(MIKE_PED(),200)
						ENDIF					
					ELIF SCAMDETAILS.PEDTO = MIKE_PED()
						SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
//						SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), FALSE)
						SET_PLAYER_FORCED_AIM(PLAYER_ID(), FALSE)
						IF NOT bFilledSpecialMeter
							bFilledSpecialMeter = TRUE
							SPECIAL_ABILITY_FILL_METER(PLAYER_ID(),TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
//			SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(SCAMDETAILS.PEDTO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_LOOK_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_LOOK_LR)
//			SET_CAM_AFFECTS_AIMING(SCAMDETAILS.CAMID,FALSE)
			IF MISSION_STAGE = ENUM_TO_INT(MSF_1_SNIPE_GAURDS)
			AND SSELECTORPEDS.ENEWSELECTORPED = SELECTOR_PED_MICHAEL	
				POINT_GAMEPLAY_CAM_AT_COORD(90)
			ENDIF			
			IF ISENTITYALIVE(VEHS[MVF_SUB].ID)
				FREEZE_ENTITY_POSITION(VEHS[MVF_SUB].ID,FALSE)
			ENDIF	
			IF IS_MESSAGE_BEING_DISPLAYED()
				CLEAR_PRINTS()
			ENDIF
			IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
			ENDIF
			
			if mission_stage = enum_to_int(msf_0_drive_to_bridge)
				if mission_substage < 14
					IF GET_SCRIPT_TASK_STATUS(mike_ped(),SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD) != PERFORMING_TASK
						SET_VEHICLE_FORWARD_SPEED(vehs[mvf_sub].id,20)
						SET_VEHICLE_ENGINE_ON(vehs[mvf_sub].id,true,true)	
						TASK_VEHICLE_DRIVE_TO_COORD(mike_ped(),vehs[mvf_dinghy].id,<<-457.7198, -2333.4165, 0.70821>>,15,DRIVINGSTYLE_ACCURATE,dinghy,DRIVINGMODE_PLOUGHTHROUGH,5,-1)														
					ENDIF
					if get_game_timer() - iFlyOverMusicTrigger > 4000
					and not bCamPan_Cued
						TRIGGER_MUSIC_EVENT("DH2A_RIB")
						bCamPan_Cued = true
					endif
				endif
			//@RJP removing this task, if Franklin is aiming when switching then we would immediately go into the sniper cam during the switch
			else
				IF ISENTITYALIVE(FRANK_PED())
				and isentityalive(mike_ped())
					IF mission_stage = enum_to_int(msf_5_fourth_bomb)
					AND mission_substage > 2
						CLEAR_PED_TASKS( FRANK_PED() )
					ELSE
						IF GET_SCRIPT_TASK_STATUS(FRANK_PED(),SCRIPT_TASK_AIM_GUN_AT_ENTITY) != PERFORMING_TASK
							TASK_AIM_GUN_AT_ENTITY(FRANK_PED(),MIKE_PED(),-1,TRUE)															
						ENDIF
					ENDIF
				ENDIF
			endif
		ENDIF
//======================================================    DONE   ============================================================				
		IF HSSTATUS = SWITCH_COMPLETE
		AND NOT SCAMDETAILS.BRUN			
		//=================================   MIKE   ====================================				
			IF PLAYER_PED_ID() = MIKE_PED()				
				IF MISSION_STAGE < ENUM_TO_INT(MSF_7_DETONATE)
				and mission_stage > enum_to_int(msf_1_snipe_gaurds)
					WEAPON_TYPE currentWeapon
					SET_ENTITY_COORDS(FRANK_PED(),<< -143.8203, -2488.2612, 43.4412 >>)	
					IF NOT balarmtriggered
						IF has_ped_got_weapon( MIKE_PED(), wtcombatpistol )
							IF GET_CURRENT_PED_WEAPON( MIKE_PED(), currentWeapon )
								IF currentWeapon != wtcombatpistol
									set_current_ped_weapon( MIKE_PED(), wtcombatpistol, TRUE )
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				endif
			ELSE
		//============================   FRANK AND TREV  =================================	
				//============ FRANK =============
				IF PLAYER_PED_ID() = FRANK_PED()
					IF ISENTITYALIVE(MIKE_PED())	
					    if MISSION_STAGE >= ENUM_TO_INT(MSF_1_SNIPE_GAURDS)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_SNIPER_RIFLE_AIM_HEADING_FOR_PED(MIKE_PED()))
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(GET_SNIPER_RIFLE_AIM_PITCH_FOR_PED(MIKE_PED()))
						else
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_SNIPER_RIFLE_AIM_HEADING_FOR_PED(mercs[merc1_start].id))
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(GET_SNIPER_RIFLE_AIM_PITCH_FOR_PED(mercs[merc1_start].id))
						endif
						SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), TRUE)
						SET_PLAYER_FORCED_AIM(PLAYER_ID(), TRUE)
						SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(7)
						FORCE_PED_AI_AND_ANIMATION_UPDATE( PLAYER_PED_ID(), TRUE )
						SET_PED_NO_TIME_DELAY_BEFORE_SHOT( PLAYER_PED_ID() )
						DISPLAY_SNIPER_SCOPE_THIS_FRAME()
						BFIRSTAIM = TRUE
					ENDIF
				ENDIF	
				//============ TREVOR =============
				IF PLAYER_PED_ID() = TREV_PED()
					IF ISENTITYALIVE(VEHS[MVF_SUB].ID)
						FREEZE_ENTITY_POSITION(VEHS[MVF_SUB].ID,FALSE)
						SET_VEHICLE_FORWARD_SPEED(vehs[mvf_sub].id,20)
						SET_VEHICLE_ENGINE_ON(vehs[mvf_sub].id,true,true)
					ENDIF
				ENDIF
				//============ BOTH =============
				IMIKE_AI_STAGE = 0
				ISTEALTHKILL = 0
			ENDIF			
		//=================================   STAGE SPECIFC   ====================================	
		
		
		//=================================   ALWAYS   ====================================					
			IF BFORCED
				BFORCED = FALSE
			ENDIF
			IF NOT IS_PED_IN_COVER(PLAYER_PED_ID())
			AND NOT IS_PED_PLANTING_BOMB(PLAYER_PED_ID())
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			SET_TIME_SCALE(1)
			CLEAR_FOCUS()
			CLEANUP_SELECTOR_CAM(SCAMDETAILS)				
		ENDIF			
	ENDIF
	
//----------------------------------------------------------------	
ENDPROC
PROC DO_FORCED_HOTSWAP(SELECTOR_SLOTS_ENUM TARGETPED)
	BFORCED = TRUE	
	IF MAKE_SELECTOR_PED_SELECTION(SSELECTORPEDS,TARGETPED)
		SCAMDETAILS.PEDTO = SSELECTORPEDS.PEDID[TARGETPED]
		SCAMDETAILS.BRUN  = TRUE		
//		HOTSWAP_CAM(TARGETPED,BINCUTSCENE)
	ENDIF
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		Mission Helpers
// -----------------------------------------------------------------------------------------------------------
proc give_mission_weapons(BOOL bDoFrank = TRUE)
	if not bGotWeapons
		int targetammo
		int newammo
		//weapons
		if isentityalive(mike_ped())
			REMOVE_ALL_PED_WEAPONS(mike_ped())
			//bomb stuff
			give_weapon_to_ped(mike_ped(),wtbomb,0)
			CPRINTLN( DEBUG_MISSION, "GAVE BOMBS TO MICHAEL" )
			targetammo = 10
			newammo = targetammo - get_ammo_in_ped_weapon(mike_ped(),wtbomb)
			if newammo > 0
				add_ammo_to_ped(mike_ped(),wtbomb,newammo)
			endif		
			if not has_ped_got_weapon(mike_ped(),weapontype_knife)
				give_weapon_to_ped(mike_ped(),weapontype_knife,infinite_ammo,true,true)
			endif	
			//pistol stuff
			give_weapon_to_ped(mike_ped(),wtcombatpistol,0,true,true)
			give_weapon_component_to_ped(mike_ped(),wtcombatpistol, weaponcomponent_at_pi_supp)	
			set_current_ped_weapon(mike_ped(),wtcombatpistol,true)
			targetammo = 50
			newammo = targetammo - get_ammo_in_ped_weapon(mike_ped(),wtcombatpistol)
			if newammo > 0
				add_ammo_to_ped(mike_ped(),wtcombatpistol,newammo)
			endif			
			SET_PED_COMP_ITEM_CURRENT_SP(mike_ped(), comp_type_props, props_p0_headset)
			HUD_SET_WEAPON_WHEEL_TOP_SLOT(wtcombatpistol)
			HUD_SET_WEAPON_WHEEL_TOP_SLOT(wtbomb)
		endif
		if isentityalive(frank_ped())
			if not has_ped_got_weapon(frank_ped(),wtsniper)
				if bDoFrank
					give_weapon_to_ped(frank_ped(),wtsniper,250,true,true)
				else
					give_weapon_to_ped(frank_ped(),wtsniper,250,false,false)
				endif
			else
				if bDoFrank
					set_current_ped_weapon(frank_ped(),wtsniper,true)
				endif
				add_ammo_to_ped(frank_ped(),wtsniper,250)
			endif
			if not has_ped_got_weapon_component(frank_ped(),wtsniper,weaponcomponent_at_scope_max)
				give_weapon_component_to_ped(frank_ped(),wtsniper, weaponcomponent_at_scope_max)
			endif
			if not has_ped_got_weapon_component(frank_ped(),wtsniper, weaponcomponent_at_ar_supp_02)
				give_weapon_component_to_ped(frank_ped(),wtsniper, weaponcomponent_at_ar_supp_02)
			endif
		endif
		bGotWeapons = true
	endif
endproc

proc FRANK_sniper_support()		
	if isentityalive(frank_ped())
		
		if not bFrankFired
			if get_game_timer() > isnipedelay			
				get_closest_ped(get_entity_coords(mike_ped()),20,false,true,franks_target_ped,false,true)			
				if isentityalive(franks_target_ped)
					vfranks_aim_coord = get_offset_from_entity_in_world_coords(franks_target_ped,<<0,0,0.4>>)
					if get_distance_between_coords(get_entity_coords(mike_ped()),vfranks_aim_coord) < 15
						IF NOT IS_ENTITY_OCCLUDED( franks_target_ped )
							IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(frank_ped(), franks_target_ped)
								TASK_SHOOT_AT_COORD( frank_ped(), vfranks_aim_coord, 1000, FIRING_TYPE_DEFAULT )
								EXPLODE_PED_HEAD( franks_target_ped )
								CPRINTLN( DEBUG_MISSION, "FRANK_sniper_support: Franklin shooting now." )
								CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_4C", CONV_PRIORITY_MEDIUM )
								iFrankFiredTimer = get_game_timer()
								isnipedelay = get_game_timer() + 4500
								bFrankFired = true
							ENDIF
						ENDIF
					endif
				else
					if not bFrankSaysMove
						if IS_SAFE_TO_START_CONVERSATION()
							IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_1A", CONV_PRIORITY_MEDIUM )
								bFrankSaysMove = TRUE
							ENDIF
						endif
					endif
				endif
			endif
		endif
		
		if not has_ped_got_weapon(frank_ped(),wtsniper)
			give_weapon_to_ped(frank_ped(),wtsniper,200,true,true)
			give_weapon_component_to_ped(frank_ped(),wtsniper, weaponcomponent_at_scope_max)
		else
			if not get_current_ped_weapon(frank_ped(),wtsniper)
				set_current_ped_weapon(frank_ped(),wtsniper,true)
			endif
		endif
		
	endif
	
	if bFrankFired
	and get_game_timer() - iFrankFiredTimer > 1000
		bFrankFired = false
	endif
	
endproc

proc FRANK_manage_ai_stealth()
			
//	float 		bestmercdist = 2000
//	float 		currentmercdist				
//	mmf_mission_merc_flags	closestmerc
//	int ii
	
	//Franklin will snipe a ped close to Mike if Mike hasn't killed him after 4.5 seconds
	if not bFrankFired
		if isentityalive(franks_target_ped)
			if VDIST2(get_entity_coords(mike_ped()),GET_ENTITY_COORDS(franks_target_ped)) < 121
				if isnipedelay > 0
					if IS_PED_IN_COMBAT( franks_target_ped, mike_ped() )
						if get_game_timer() > (isnipedelay-2000)
							if not IS_ENTITY_OCCLUDED(franks_target_ped)
								IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(frank_ped(), franks_target_ped)
									vfranks_aim_coord = get_offset_from_entity_in_world_coords(franks_target_ped,<<0,0,0.4>>)
//									TASK_SHOOT_AT_ENTITY( frank_ped(), franks_target_ped, 1000, FIRING_TYPE_DEFAULT )
									TASK_SHOOT_AT_COORD( frank_ped(), vfranks_aim_coord, 1000, FIRING_TYPE_DEFAULT )
									EXPLODE_PED_HEAD( franks_target_ped )
									CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_3C", CONV_PRIORITY_MEDIUM )
									CPRINTLN( DEBUG_MISSION, "FRANK_manage_ai_stealth: Franklin shooting combat ped now." )
									iFrankFiredTimer = get_game_timer()
									bFrankFired = TRUE
									isnipedelay = -1
								ENDIF
							endif
						endif
					else
						if get_game_timer() > isnipedelay
							if not IS_ENTITY_OCCLUDED(franks_target_ped)
								IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY(frank_ped(), franks_target_ped)
									vfranks_aim_coord = get_offset_from_entity_in_world_coords(franks_target_ped,<<0,0,0.4>>)
//									TASK_SHOOT_AT_ENTITY( frank_ped(), franks_target_ped, 1000, FIRING_TYPE_DEFAULT )
									TASK_SHOOT_AT_COORD( frank_ped(), vfranks_aim_coord, 1000, FIRING_TYPE_DEFAULT )
									EXPLODE_PED_HEAD( franks_target_ped )
									CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_3C", CONV_PRIORITY_MEDIUM )
									CPRINTLN( DEBUG_MISSION, "FRANK_manage_ai_stealth: Franklin shooting now." )
									iFrankFiredTimer = get_game_timer()
									bFrankFired = TRUE
									isnipedelay = -1
								ENDIF
							endif
						endif
					endif
				else
					if not IS_ENTITY_OCCLUDED(franks_target_ped)
						if not is_synchronized_scene_running(iStealthKillSyncScene)
							if not is_entity_playing_anim(franks_target_ped, "missheistdocks2a","stabbing_guard_guard", anim_synced_scene)
								CPRINTLN( DEBUG_MISSION, "FRANK_manage_ai_stealth: reset iSnipeDelay" )
								CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_DECK2", CONV_PRIORITY_MEDIUM )
								isnipedelay = get_game_timer() + 4500
							endif
						endif
					endif
				endif
			else
				isnipedelay = -1
			endif
		else
			get_closest_ped(get_entity_coords(mike_ped()),50,false,true,franks_target_ped,false,true)
			isnipedelay = -1
		endif
	endif
	
	IF isentityalive( mercs[merc4_C4_2].id )
		IF IS_PLAYER_IN_TRIGGER_BOX( sTriggerBox[TB_FRANKLIN_KILL_MERC4] )
		AND isentityalive(mike_ped())
			IF NOT IS_ENTITY_OCCLUDED( mercs[merc4_C4_2].id )
			AND NOT bshotready
			AND IS_SAFE_TO_START_CONVERSATION()
				if 	not is_synchronized_scene_running(iStealthKillSyncScene)
				and not is_entity_playing_anim(mercs[merc4_C4_2].id, "missheistdocks2a","stabbing_guard_guard", anim_synced_scene)
					IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_STEPS1F", CONV_PRIORITY_MEDIUM )
						bshotready = true
					ENDIF
				endif
			ENDIF
			IF bshotready
			AND IS_SAFE_TO_START_CONVERSATION()
			AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY(frank_ped(), mercs[merc4_C4_2].id)
				franks_target_ped = mercs[merc4_C4_2].id
				vfranks_aim_coord = get_offset_from_entity_in_world_coords(franks_target_ped,<<0,0,0.4>>)
				bFrankFired = true
				iFrankFiredTimer = get_game_timer()
				APPLY_FORCE_TO_ENTITY( mercs[merc4_C4_2].id, APPLY_TYPE_IMPULSE, <<10.0, 0.0, 0.0>>, <<1,0,0>>, 0, FALSE, TRUE, TRUE )
				shoot_single_bullet_between_coords(<<-125.6115, -2380.9084, 12.0460>>,get_offset_from_entity_in_world_coords(mercs[merc4_C4_2].id,<<0,0,0.4>>),1000,true,wtsniper)
				CPRINTLN( DEBUG_MISSION, "FRANK_manage_ai_stealth: Apply force to merc4_C4_2.")
				EXPLODE_PED_HEAD( mercs[merc4_C4_2].id )
				CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_GUY1F", CONV_PRIORITY_MEDIUM )
				bkilledbybuddy = true
				bshotready = false
			ENDIF
		ENDIF
	ENDIF
	
	IF isentityalive( mercs[merc12_C4_3].id )
		IF IS_PLAYER_IN_TRIGGER_BOX( sTriggerBox[TB_FRANKLIN_KILL_MERC12] )
		AND isentityalive(mike_ped())
			IF NOT IS_ENTITY_OCCLUDED( mercs[merc12_C4_3].id )
			AND NOT bshotready	
			AND IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_BALC", CONV_PRIORITY_MEDIUM )
					bshotready = true
				ENDIF
			ENDIF
			IF bshotready
			AND IS_SAFE_TO_START_CONVERSATION()
			AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY(frank_ped(), mercs[merc12_C4_3].id)
				franks_target_ped = mercs[merc12_C4_3].id
				vfranks_aim_coord = get_offset_from_entity_in_world_coords(franks_target_ped,<<0,0,0.4>>)
				bFrankFired = true
				iFrankFiredTimer = get_game_timer()
				shoot_single_bullet_between_coords(<< -143.17641, -2487.5, 45.44402 >>,get_offset_from_entity_in_world_coords(mercs[merc12_C4_3].id,<<0,0,0.4>>),1000,true,wtsniper)
				EXPLODE_PED_HEAD( mercs[merc12_C4_3].id )
				CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_3D", CONV_PRIORITY_MEDIUM )
				bkilledbybuddy = true
				bshotready = false
			ENDIF
		ENDIF
	ENDIF
	
	IF isentityalive( mercs[merc13_C4_3].id )
		IF IS_PLAYER_IN_TRIGGER_BOX( sTriggerBox[TB_FRANKLIN_KILL_MERC13] )
		AND isentityalive(mike_ped())
//		and get_game_timer() - icsdelay > 2000
			IF NOT IS_ENTITY_OCCLUDED( mercs[merc13_C4_3].id )
			AND NOT bshotready	
			AND IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_STEPS2F", CONV_PRIORITY_MEDIUM )
					bshotready = true
				ENDIF
			ENDIF
			IF bshotready
			AND IS_SAFE_TO_START_CONVERSATION()
			AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY(frank_ped(), mercs[merc13_C4_3].id)
				franks_target_ped = mercs[merc13_C4_3].id
				vfranks_aim_coord = get_offset_from_entity_in_world_coords(franks_target_ped,<<0,0,0.4>>)
				bFrankFired = true
				iFrankFiredTimer = get_game_timer()
				APPLY_FORCE_TO_ENTITY( mercs[merc13_C4_3].id, APPLY_TYPE_IMPULSE, <<10.0, 0.0, 0.0>>, <<1,0,0>>, 0, FALSE, TRUE, TRUE )
				shoot_single_bullet_between_coords(<<-204.6591, -2381.0024, 11.9054>>,get_offset_from_entity_in_world_coords(mercs[merc13_C4_3].id,<<0,0,0.4>>),1000,true,wtsniper)
				EXPLODE_PED_HEAD( mercs[merc13_C4_3].id )
				CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_3A", CONV_PRIORITY_MEDIUM )
				CPRINTLN( DEBUG_MISSION, "FRANK_manage_ai_stealth: Apply force to merc13_C4_3.")
//				SET_ENTITY_HEALTH( mercs[merc13_C4_3].id, 0 )
				bkilledbybuddy = true
				bshotready = false
			ENDIF
		ENDIF
	ENDIF
	
	if bFrankFired
	and get_game_timer() - iFrankFiredTimer > 1000
		bFrankFired = false
	endif

endproc
func bool MIKE_stealth_killing( MERC_STRUCT &merc )	
	
	if isentityalive(merc.id)			
		switch istealthkill
			case 0			
				if is_ped_in_combat(merc.id)
				or merc.currentsit <> MS_SIT_NONE
//					if is_entity_at_entity(mike_ped(),merc.id,<<3,3,2>>)
						clear_ped_tasks_immediately(mike_ped())
						wtSneakingWeapon = wtcombatpistol
						task_combat_ped(mike_ped(),merc.id)
						set_ped_sphere_defensive_area(mike_ped(),get_entity_coords(merc.id),2.2)
						set_ped_combat_attributes(mike_ped(),ca_perfect_accuracy,true)
						set_ped_combat_attributes(mike_ped(),ca_requires_los_to_shoot,true)
						istealthkill++
//					endif
				else
					if is_entity_at_entity(mike_ped(),merc.id,<<1,1,1>>)
						wtSneakingWeapon = weapontype_knife
						merc.bStealthKill = TRUE
						iStealthKillSyncScene = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(merc.id),<<0,0,GET_ENTITY_HEADING(merc.id)>>)
						TASK_SYNCHRONIZED_SCENE(mike_ped(),iStealthKillSyncScene,"missheistdocks2a","stabbing_guard_michael",SLOW_BLEND_IN,NORMAL_BLEND_OUT,SYNCED_SCENE_NONE,RBF_NONE,SLOW_BLEND_IN)
						TASK_SYNCHRONIZED_SCENE(merc.id,iStealthKillSyncScene,"missheistdocks2a","stabbing_guard_guard",NORMAL_BLEND_IN,NORMAL_BLEND_OUT)
//						task_stealth_kill(mike_ped(),merc,get_hash_key("AR_stealth_kill_knife"))
						istealthkill++
					endif
				endif
			break
			case 1
				if is_ped_in_combat(merc.id)
				or merc.currentsit <> MS_SIT_NONE
					if has_entity_been_damaged_by_entity(merc.id,mike_ped())
						set_entity_health(merc.id,0)
						istealthkill = 0	
						bkilledbybuddy = true
						return true
					endif
				else
					if GET_SYNCHRONIZED_SCENE_PHASE( iStealthKillSyncScene ) > 0.99
						DETACH_SYNCHRONIZED_SCENE( iStealthKillSyncScene )
						set_entity_health(merc.id,0)
						bkilledbybuddy = true
						istealthkill = 0
						return true
					endif
//					if is_ped_performing_stealth_kill(mike_ped())
//						istealthkilltimer = get_game_timer()
//						bkilledbybuddy = true
//						istealthkill++
//					endif
				endif
			break
			case 2
				if get_game_timer() - istealthkilltimer > 1000					
					istealthkill = 0	
					return true
				endif
			break
		endswitch		
	else
		return true
		istealthkill = 0
		istealthkilltimer = get_game_timer()
	endif	
	
	return false
endfunc

proc MIKE_stealth_combat()
	if not isentityalive(mike_ped())
		EXIT
	endif
	
	WEAPON_TYPE wtCurrentWeapon
	IF GET_CURRENT_PED_WEAPON( mike_ped(), wtCurrentWeapon )
		IF wtCurrentWeapon != wtCombatPistol
			set_current_ped_weapon( mike_ped(),wtCombatPistol,true )
		ENDIF
	ENDIF
	
	IF NOT IS_PED_IN_COMBAT( mike_ped() )
		CPRINTLN( DEBUG_MISSION, "MIKE_stealth_combat: Mike has entered stealth combat." )
		CLEAR_PED_TASKS( mike_ped() )
		TASK_COMBAT_HATED_TARGETS_AROUND_PED( mike_ped(), 20 )
	ENDIF
endproc

proc MIKE_manage_ai_stealth()
if isentityalive(mike_ped())
	//@RJP check to see if mike_ped is carrying the desired weapon, if not, set the desired weapon
	WEAPON_TYPE wtCurrentWeapon
	IF GET_CURRENT_PED_WEAPON( mike_ped(), wtCurrentWeapon )
		IF wtCurrentWeapon != wtSneakingWeapon
			set_current_ped_weapon( mike_ped(),wtSneakingWeapon,true )
		ENDIF
	ENDIF
	SET_PED_ACCURACY(mike_ped(), 100)
	//if the player is sniping then move mike up as the enemy gets shot.
	switch INT_TO_ENUM( MSF_MISSION_STAGE_FLAGS, mission_stage)
//═══════════════════════════════════════════════════════════════════╡ bomb 1 ╞═════════════════════════════════════════════════════════════════════	
		case msf_2_first_bomb
			switch imike_ai_stage
				case 0
					if not c4[c4_0_front_of_ship].bplanted
						wtSneakingWeapon = wtbomb
						set_current_ped_weapon( mike_ped(),wtbomb,true )
						clear_sequence_task(seq)
						open_sequence_task(seq)	
							task_follow_nav_mesh_to_coord(null,<<-83.92709, -2366.11841, 13.29613>>,pedmove_run,default_time_never_warp)
							TASK_PLANT_BOMB(null,<<-83.92709, -2366.11841, 13.29613>>,90)
						close_sequence_task(seq)
						task_perform_sequence(mike_ped(),seq)
						clear_sequence_task(seq)
						imike_ai_stage++
					endif
				break
				case 1
//					if is_projectile_type_in_angled_area((c4[c4_0_front_of_ship].exppos + <<-0.75,3,2>>),(c4[c4_0_front_of_ship].exppos + <<-0.75,3,-2>>),10,wtbomb)
					if c4[c4_0_front_of_ship].bplanted = true 
						wtSneakingWeapon = wtCombatPistol
						safe_remove_blip(c4[c4_0_front_of_ship].blip)
						icsdelay = get_game_timer()
						imike_ai_stage++	
					endif
				break
				case 2
					if get_game_timer() - icsdelay > 1000
						c4[c4_0_front_of_ship].bplanted = true
						imike_ai_stage++
					endif
				break
				case 3
					//end state
				break
			endswitch
		break
//═══════════════════════════════════════════════════════════════════╡ bomb 2 ╞═════════════════════════════════════════════════════════════════════		
		case msf_3_second_bomb
			switch imike_ai_stage
				case 0
					wtSneakingWeapon = wtCombatPistol
					//@RJP changing Mike's first cover point so he will not be so easily killed by new patrols
					IF IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_BOW_UPPER_AREA] )
						vai_coverpoint = <<-95.5662, -2375.6272, 13.2963>>
						fai_coverheading = 87.6044
						CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Mike in TB_BOAT_BOW_UPPER_AREA, imike_ai_stage++" )
						imike_ai_stage++
					ELIF IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_BOW_CONTAINERS_AREA] )
					OR IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_FIRST_MAST_AREA] )
						vai_coverpoint = <<-124.3208, -2373.9026, 8.3191>>
						fai_coverheading = 87.6044
						CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Mike in TB_BOAT_BOW_UPPER_AREA, imike_ai_stage = 3" )
						imike_ai_stage = 3
					ELSE
						vai_coverpoint = <<-132.6543, -2377.2488, 8.3191>>
						fai_coverheading = 358.0533
						remove_cover_point(customcover)
						customcover = add_cover_point(vai_coverpoint,fai_coverheading ,COVUSE_WALLTOLEFT,COVHEIGHT_TOOHIGH,covarc_180)
						clear_sequence_task(seq)
						open_sequence_task(seq)
							task_follow_nav_mesh_to_coord(null,vai_coverpoint,pedmove_run,default_time_never_warp)
							task_put_ped_directly_into_cover(null,vai_coverpoint,infinite_task_time,false,0.25,true,false,customcover,true)
						close_sequence_task(seq)
						task_perform_sequence(mike_ped(),seq)
						CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Mike in TB_BOAT_BOW_UPPER_AREA, imike_ai_stage = 4" )
						imike_ai_stage = 4
					ENDIF
				break
				case 1
					remove_cover_point(customcover)
					customcover = add_cover_point(vai_coverpoint,fai_coverheading ,COVUSE_WALLTONEITHER,COVHEIGHT_LOW,covarc_180)
					clear_sequence_task(seq)
					open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null,vai_coverpoint,pedmove_run,default_time_never_warp)
						task_put_ped_directly_into_cover(null,vai_coverpoint,infinite_task_time,false,0.25,true,false,customcover,true)
					close_sequence_task(seq)
					task_perform_sequence(mike_ped(),seq)
					CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Mike tasked to cover, imike_ai_stage++" )
					imike_ai_stage++
				break
				case 2
					//@RJP Removing the stealth kill of merc6
					IF iStealthTimer > 0
						IF (GET_GAME_TIMER() - iStealthTimer) > 1000
							if not isentityalive(mercs[merc3_c4_2].id)
							or not IS_ENTITY_IN_TRIGGER_BOX( mercs[merc3_c4_2].id, sTriggerBox[TB_MIKE_WAIT_FOR_MERC3_TO_MOVE] )
							AND IS_SAFE_TO_START_CONVERSATION()
								task_follow_nav_mesh_to_coord(mike_ped(),<< -124.5024, -2374.0386, 8.3189 >>,pedmove_run,default_time_never_warp)
								CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_LADDER", CONV_PRIORITY_MEDIUM )
								CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Mike tasked to advance to first mast, imike_ai_stage++" )
								iStealthTimer = 0
								bMikeStealthDiag = FALSE
								imike_ai_stage++
							else
								if isentityalive(mercs[merc3_c4_2].id)
									if not DOES_BLIP_EXIST(mercs[merc3_c4_2].blip)
										mercs[merc3_c4_2].blip = CREATE_BLIP_FOR_PED(mercs[merc3_c4_2].id,true)
									endif
								endif
								if not bMikeStealthDiag
									if IS_SAFE_TO_START_CONVERSATION()
										if CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_DECK", CONV_PRIORITY_MEDIUM )
											bMikeStealthDiag = TRUE
										endif
									endif
								endif
							endif
						ENDIF
					ELSE
						IF IS_ENTITY_AT_COORD( mike_ped(), <<-95.5662, -2375.6272, 13.2963>>, <<2.0,2.0,2.0>> )
						AND IS_PED_IN_COVER( mike_ped() )
							iStealthTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				break
				case 3
					//@RJP putting Michael into cover near first mast until Franklin clears the way
					vai_coverpoint = << -124.5024, -2374.0386, 8.3189 >>
					fai_coverheading = 87.6044
					remove_cover_point(customcover)
					customcover = add_cover_point(vai_coverpoint,fai_coverheading ,COVUSE_WALLTORIGHT,COVHEIGHT_TOOHIGH,covarc_180)
					clear_sequence_task(seq)
					open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null,vai_coverpoint,pedmove_run,default_time_never_warp)
						task_put_ped_directly_into_cover(null,vai_coverpoint,infinite_task_time,false,0.25,true,true,customcover,true)
					close_sequence_task(seq)
					task_perform_sequence(mike_ped(),seq)
					CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Mike tasked to cover at first mast, imike_ai_stage++" )
					imike_ai_stage++
				break
				case 4
					IF iStealthTimer > 0
						IF (GET_GAME_TIMER() - iStealthTimer) > 1000
							//merc 5+7 now patrol together
							if not isentityalive(mercs[merc5_c4_2].id)
//							and not isentityalive(mercs[MERC6_C4_2].id)
							or not IS_ENTITY_IN_TRIGGER_BOX( mercs[merc5_c4_2].id, sTriggerBox[TB_MIKE_WAIT_FOR_MERC5_TO_MOVE] )
//							and not IS_ENTITY_IN_TRIGGER_BOX( mercs[MERC6_C4_2].id, sTriggerBox[TB_MIKE_WAIT_FOR_MERC5_TO_MOVE] ))
								if isentityalive(mercs[merc4_c4_2].id)
								AND IS_SAFE_TO_START_CONVERSATION()
									CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_STEPS1M", CONV_PRIORITY_MEDIUM )
									task_follow_to_offset_of_entity(mike_ped(),mercs[merc4_c4_2].id,<<0.0,-0.1,0>>,pedmove_walk,-1)
									CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Mike tasked to stealth kill merc4, imike_ai_stage++" )
									iStealthTimer = 0
									bMikeAsksForHelp = FALSE
									imike_ai_stage++	
								else
									CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". merc4 dead, imike_ai_stage=6" )
									iStealthTimer = 0
									imike_ai_stage = 6
								endif						
							else
								if isentityalive(mercs[merc5_c4_2].id)
									if not DOES_BLIP_EXIST(mercs[merc5_C4_2].blip)
										mercs[merc5_C4_2].blip = CREATE_BLIP_FOR_PED(mercs[merc5_C4_2].id,true)
									endif
								endif
							endif
						ENDIF
					ELSE
						IF IS_ENTITY_AT_COORD( mike_ped(), << -124.5024, -2374.0386, 8.3189 >>, <<2.0,2.0,2.0>> )
						AND IS_PED_IN_COVER( mike_ped() )
							iStealthTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				break
				case 5
					if MIKE_stealth_killing(mercs[merc4_c4_2])
						wtSneakingWeapon = wtcombatpistol
						CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_GUY1M", CONV_PRIORITY_MEDIUM )
						CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Mike stealth killed merc4, imike_ai_stage++" )
						imike_ai_stage++
					endif
				break
				case 6
					vai_coverpoint = <<-132.6543, -2377.2488, 8.3191>>
					fai_coverheading = 358.0533
					remove_cover_point(customcover)
					customcover = add_cover_point(vai_coverpoint,fai_coverheading ,COVUSE_WALLTOLEFT,COVHEIGHT_TOOHIGH,covarc_180)
					clear_sequence_task(seq)
					open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null,vai_coverpoint,pedmove_run,default_time_never_warp)
						task_put_ped_directly_into_cover(null,vai_coverpoint,infinite_task_time,false,0.25,true,false,customcover,true)
					close_sequence_task(seq)
					task_perform_sequence(mike_ped(),seq)
					CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Mike tasked to cover near bomb site, imike_ai_stage++" )
					imike_ai_stage++
				break
				case 7
					IF iStealthTimer > 0
						IF (GET_GAME_TIMER() - iStealthTimer) > 1000
							if not isentityalive(mercs[merc5_c4_2].id)
							or not IS_ENTITY_IN_TRIGGER_BOX( mercs[merc5_c4_2].id, sTriggerBox[TB_MIKE_WAIT_FOR_MERC5_TO_MOVE] )
								wtSneakingWeapon = wtbomb
								set_current_ped_weapon( mike_ped(),wtbomb,true )
								clear_sequence_task(seq)
								open_sequence_task(seq)
									task_follow_nav_mesh_to_coord(null,<< -128.70566, -2367.17944, 8.31891 >>,pedmove_run,default_time_never_warp)
									TASK_PLANT_BOMB(null,<<-128.69624, -2366.82788, 9.22130 >>,270)
								close_sequence_task(seq)
								task_perform_sequence(mike_ped(),seq)
								clear_sequence_task(seq)
								CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Mike tasked to plant bomb, imike_ai_stage++" )
								iStealthTimer = 0
								IF bMikeAsksForHelp
									IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY( mercs[merc5_c4_2].id, frank_ped() )
										CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_2A", CONV_PRIORITY_MEDIUM )
									ELSE
										CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_MOVE1", CONV_PRIORITY_MEDIUM )
									ENDIF
								ENDIF
								bMikeAsksForHelp = FALSE
								imike_ai_stage++
							ELSE
								if isentityalive(mercs[merc5_c4_2].id)
									if not bMikeAsksForHelp
										if IS_SAFE_TO_START_CONVERSATION()
											if IS_ENTITY_IN_TRIGGER_BOX( mercs[merc9_c4_3].id, sTriggerBox[TB_MIKE_WAIT_FOR_MERC9_TO_MOVE] )
												if CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_DECK2", CONV_PRIORITY_MEDIUM )
													bMikeAsksForHelp = TRUE
												endif
											endif
										endif
									endif
								endif
							ENDIF
						ENDIF
					ELSE
						IF IS_ENTITY_AT_COORD( mike_ped(), <<-132.6543, -2377.2488, 8.3191>>, <<2.0,2.0,2.0>> )
						AND IS_PED_IN_COVER( mike_ped() )
							iStealthTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				break
				case 8
//					if is_projectile_type_in_angled_area((c4[c4_1_first_mast].exppos + <<-0.75,3,2>>),(c4[c4_1_first_mast].exppos + <<-0.75,3,-2>>),10,wtbomb)
					if c4[c4_1_first_mast].bplanted = true 
						wtSneakingWeapon = wtCombatPistol
						safe_remove_blip(c4[c4_1_first_mast].blip)
						icsdelay = get_game_timer()							
						imike_ai_stage++
					endif
				break
				case 9
					if get_game_timer() - icsdelay > 1000
						c4[c4_1_first_mast].bplanted = true
						imike_ai_stage++
					endif
				break
				case 10
					//end 
				break
			endswitch
		break
//═══════════════════════════════════════════════════════════════════╡ bomb 3 ╞═════════════════════════════════════════════════════════════════════		
		case msf_4_third_bomb
			switch imike_ai_stage
				case 0
					wtSneakingWeapon = wtCombatPistol
					//					vai_coverpoint = << -148.31889, -2376.58008, 8.31907 >>
					//@RJP first cover point should give cover from mercs 8+9
					IF IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_BOW_UPPER_AREA] )
					OR IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_BOW_CONTAINERS_AREA] )
					OR IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_FIRST_MAST_AREA] )
						imike_ai_stage++
					ELIF IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_MID_CONTAINERS_1_AREA] )
						imike_ai_stage = 3
					ELIF IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_MID_CONTAINERS_2_AREA] )
						vai_coverpoint = <<-161.0396, -2377.5042, 8.3191>>
						fai_coverheading = 356.7672
						imike_ai_stage = 5
					ELIF IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_MID_CONTAINERS_3_AREA] )
						vai_coverpoint = <<-177.2718, -2377.5042, 8.3191>>
						imike_ai_stage = 7
					ELSE IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_SECOND_MAST_AREA] )
						imike_ai_stage = 9
					ENDIF
				break
				case 1
					vai_coverpoint = << -132.01889, -2376.58008, 8.31907 >>
					fai_coverheading = 89.7021
					remove_cover_point(customcover)
					customcover = add_cover_point(vai_coverpoint,fai_coverheading,covuse_walltoright,covheight_toohigh,covarc_180)
					clear_sequence_task(seq)
					open_sequence_task(seq)				
						task_follow_nav_mesh_to_coord(null,vai_coverpoint,pedmove_run,default_time_never_warp,0.7)
						task_put_ped_directly_into_cover(null,vai_coverpoint,infinite_task_time,false,0.25,true,true,customcover,true)
					close_sequence_task(seq)
					task_perform_sequence(mike_ped(),seq)
					CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Mike tasked to cover, imike_ai_stage++" )
					imike_ai_stage++
				break
				case 2
					//@RJP Michael should wait in 1st cover spot until path ahead is clear
					IF iStealthTimer > 0
						IF (GET_GAME_TIMER() - iStealthTimer) > 1000
							if not isentityalive(mercs[merc9_c4_3].id)
							OR NOT IS_ENTITY_IN_TRIGGER_BOX( mercs[merc9_c4_3].id, sTriggerBox[TB_MIKE_WAIT_FOR_MERC9_TO_MOVE] )
									set_current_ped_weapon(mike_ped(),wtCombatPistol,true)
									task_follow_nav_mesh_to_coord(mike_ped(),<<-144.5449, -2377.5042, 8.3191>>,pedmove_run,default_time_never_warp)
								CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Merc9 passed, Mike tasked to move to cover, imike_ai_stage++" )
								iStealthTimer = 0
								IF bMikeAsksForHelp
									IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY( mercs[merc9_c4_3].id, frank_ped() )
										CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_2A", CONV_PRIORITY_MEDIUM )
									ELSE
										CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_MOVE1", CONV_PRIORITY_MEDIUM )
									ENDIF
								ENDIF
								bMikeAsksForHelp = FALSE
								imike_ai_stage++
							else
								if isentityalive(mercs[merc9_c4_3].id)
									if not DOES_BLIP_EXIST(mercs[merc9_c4_3].blip)
										mercs[merc9_c4_3].blip = CREATE_BLIP_FOR_PED(mercs[merc9_c4_3].id,true)
									endif
									if not bMikeAsksForHelp
										if IS_SAFE_TO_START_CONVERSATION()
											if IS_ENTITY_IN_TRIGGER_BOX( mercs[merc9_c4_3].id, sTriggerBox[TB_MIKE_WAIT_FOR_MERC9_TO_MOVE] )
												if CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_DECK2", CONV_PRIORITY_MEDIUM )
													bMikeAsksForHelp = TRUE
												endif
											endif
										endif
									endif
								endif
							endif
						ENDIF
					ELSE
						IF IS_ENTITY_AT_COORD( mike_ped(), << -132.01889, -2376.58008, 8.31907 >>, <<2.0,2.0,2.0>> )
						AND IS_PED_IN_COVER( mike_ped() )
							iStealthTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				break
				case 3
					remove_cover_point(customcover)
					//@RJP changing this cover point to the next block of cargo containers
					customcover =add_cover_point(<<-144.5449, -2377.5042, 8.3191>>, 354.7062,covuse_walltoright,covheight_toohigh,covarc_180)
					clear_sequence_task(seq)
					open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null,<<-144.5449, -2377.5042, 8.3191>>,pedmove_run,default_time_never_warp)
						task_put_ped_directly_into_cover(null,<<-144.5449, -2377.5042, 8.3191>>,infinite_task_time,false,0.25,true,true,customcover,true)
					close_sequence_task(seq)
					task_perform_sequence(mike_ped(),seq)
					CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_MOVE1", CONV_PRIORITY_MEDIUM )
					CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Mike tasked into cover, imike_ai_stage++" )
					iStealthTimer = 0
					imike_ai_stage++
				break
				case 4
					IF iStealthTimer > 0
						IF (GET_GAME_TIMER() - iStealthTimer) > 1000
							if not isentityalive(mercs[merc9_c4_3].id)
							OR NOT IS_ENTITY_IN_TRIGGER_BOX( mercs[merc9_c4_3].id, sTriggerBox[TB_MIKE_WAIT_FOR_MERC9_TO_MOVE] )
								task_follow_nav_mesh_to_coord(mike_ped(),<<-161.0396, -2377.5042, 8.3191>>,pedmove_run,default_time_never_warp)
								CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Merc9 passed, Mike tasked to move to cover, imike_ai_stage++" )
								iStealthTimer = 0
								IF bMikeAsksForHelp
									IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY( mercs[merc9_c4_3].id, frank_ped() )
										CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_2A", CONV_PRIORITY_MEDIUM )
									ENDIF
								ENDIF
								bMikeAsksForHelp = FALSE
								imike_ai_stage++
							else
								if isentityalive(mercs[merc9_c4_3].id)
									if not DOES_BLIP_EXIST(mercs[merc9_c4_3].blip)
										mercs[merc9_c4_3].blip = CREATE_BLIP_FOR_PED(mercs[merc9_c4_3].id,true)
									endif
									if not bMikeAsksForHelp
										if IS_SAFE_TO_START_CONVERSATION()
											if IS_ENTITY_IN_TRIGGER_BOX( mercs[merc9_c4_3].id, sTriggerBox[TB_MIKE_WAIT_FOR_MERC9_TO_MOVE] )
												if CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_DECK2", CONV_PRIORITY_MEDIUM )
													bMikeAsksForHelp = TRUE
												endif
											endif
										endif
									endif
								endif
							endif
						ENDIF
					ELSE
						IF IS_ENTITY_AT_COORD( mike_ped(), <<-144.5449, -2377.5042, 8.3191>>, <<2.0,2.0,2.0>> )
						AND IS_PED_IN_COVER( mike_ped() )
							iStealthTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				break
				case 5
					remove_cover_point(customcover)
					//@RJP changing this cover point to the next block of cargo containers
					customcover =add_cover_point(<<-161.0396, -2377.5042, 8.3191>>, 354.7062,covuse_walltoright,covheight_toohigh,covarc_180)
					clear_sequence_task(seq)
					open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null,<<-161.0396, -2377.5042, 8.3191>>,pedmove_run,default_time_never_warp)
						task_put_ped_directly_into_cover(null,<<-161.0396, -2377.5042, 8.3191>>,infinite_task_time,false,0.25,true,true,customcover,true)
					close_sequence_task(seq)
					task_perform_sequence(mike_ped(),seq)
					CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_MOVE1", CONV_PRIORITY_MEDIUM )
					CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Mike tasked into cover, imike_ai_stage++" )
					iStealthTimer = 0
					bMikeAsksForHelp = FALSE
					imike_ai_stage++
				break
				case 6
					IF iStealthTimer > 0
						IF (GET_GAME_TIMER() - iStealthTimer) > 1000
							if not isentityalive(mercs[merc11_c4_3].id)
							OR NOT IS_ENTITY_IN_TRIGGER_BOX( mercs[merc11_c4_3].id, sTriggerBox[TB_MIKE_WAIT_FOR_MERC10_TO_MOVE] )
								if not bdouble_music_cue
									trigger_music_event("dh2a_double_guards")
									bdouble_music_cue = true
								endif
								set_current_ped_weapon(mike_ped(),wtCombatPistol,true)
								task_follow_nav_mesh_to_coord(mike_ped(),<<-177.2718, -2377.5042, 8.3191>>,pedmove_run,default_time_never_warp)
								CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Merc10 and 11 passed, Mike tasked to move to cover, imike_ai_stage++" )
								IF bMikeAsksForHelp
									IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY( mercs[merc11_c4_3].id, frank_ped() )
										CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_2A", CONV_PRIORITY_MEDIUM )
									ENDIF
								ENDIF
								bMikeAsksForHelp = FALSE
								iStealthTimer = 0
								imike_ai_stage++
							else
								if isentityalive(mercs[merc11_c4_3].id)
									if not DOES_BLIP_EXIST(mercs[merc11_c4_3].blip)
										mercs[merc11_c4_3].blip = CREATE_BLIP_FOR_PED(mercs[merc11_c4_3].id,true)
									endif
									if not bMikeAsksForHelp
										if IS_SAFE_TO_START_CONVERSATION()
											if IS_ENTITY_IN_TRIGGER_BOX( mercs[merc11_c4_3].id, sTriggerBox[TB_MIKE_WAIT_FOR_MERC10_TO_MOVE] )
												if CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_DECK2", CONV_PRIORITY_MEDIUM )
													bMikeAsksForHelp = TRUE
												endif
											endif
										endif
									endif
								endif
							endif
						ENDIF
					ELSE
						IF IS_ENTITY_AT_COORD( mike_ped(), <<-161.0396, -2377.5042, 8.3191>>, <<2.0,2.0,2.0>> )
						AND IS_PED_IN_COVER( mike_ped() )
							iStealthTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				break
				case 7
					remove_cover_point(customcover)
					//@RJP changing this cover point to the next block of cargo containers
					customcover =add_cover_point(<<-177.2718, -2377.5042, 8.3191>>, 354.7062,covuse_walltoright,covheight_toohigh,covarc_180)
					clear_sequence_task(seq)
					open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null,<<-177.2718, -2377.5042, 8.3191>>,pedmove_run,default_time_never_warp)
						task_put_ped_directly_into_cover(null,<<-177.2718, -2377.5042, 8.3191>>,infinite_task_time,false,0.25,true,true,customcover,true)
					close_sequence_task(seq)
					task_perform_sequence(mike_ped(),seq)
					CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_MOVE1", CONV_PRIORITY_MEDIUM )
					CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Mike tasked into cover, imike_ai_stage++" )
					iStealthTimer = 0
					imike_ai_stage++
				break
				case 8
					IF iStealthTimer > 0
						IF (GET_GAME_TIMER() - iStealthTimer) > 1000
							if not isentityalive(mercs[merc14_c4_3].id)
							OR NOT IS_ENTITY_IN_TRIGGER_BOX( mercs[merc14_c4_3].id, sTriggerBox[TB_MIKE_WAIT_FOR_MERC14_TO_MOVE] )
								task_follow_nav_mesh_to_coord(mike_ped(),<<-187.2977, -2374.7778, 8.3191>>,pedmove_run,default_time_never_warp)
								CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Merc14 and 16 passed, Mike tasked to move to cover, imike_ai_stage++" )
								IF bMikeAsksForHelp
									IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY( mercs[merc14_c4_3].id, frank_ped() )
										CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_2A", CONV_PRIORITY_MEDIUM )
									ENDIF
								ENDIF
								bMikeAsksForHelp = FALSE
								iStealthTimer = 0
								imike_ai_stage++
							else
								if isentityalive(mercs[merc12_c4_3].id)
									if not DOES_BLIP_EXIST(mercs[merc12_c4_3].blip)
										mercs[merc12_c4_3].blip = CREATE_BLIP_FOR_PED(mercs[merc12_c4_3].id,true)
									endif
								endif
								if isentityalive(mercs[merc14_c4_3].id)
									if not DOES_BLIP_EXIST(mercs[merc14_c4_3].blip)
										mercs[merc14_c4_3].blip = CREATE_BLIP_FOR_PED(mercs[merc14_c4_3].id,true)
									endif
									if not bMikeAsksForHelp
										if IS_SAFE_TO_START_CONVERSATION()
											if IS_ENTITY_IN_TRIGGER_BOX( mercs[merc14_c4_3].id, sTriggerBox[TB_MIKE_WAIT_FOR_MERC14_TO_MOVE] )
												if CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_DECK2", CONV_PRIORITY_MEDIUM )
													bMikeAsksForHelp = TRUE
												endif
											endif
										endif
									endif
								endif
							endif
						ENDIF
					ELSE
						IF IS_ENTITY_AT_COORD( mike_ped(), <<-177.2718, -2377.5042, 8.3191>>, <<2.0,2.0,2.0>> )
						AND IS_PED_IN_COVER( mike_ped() )
							iStealthTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				break
				case 9
					remove_cover_point(customcover)
					//@RJP changing this cover point to the next block of cargo containers
					customcover =add_cover_point(<<-187.2977, -2374.7778, 8.3191>>, 354.7062,covuse_walltoright,covheight_toohigh,covarc_180)
					clear_sequence_task(seq)
					open_sequence_task(seq)
						task_follow_nav_mesh_to_coord(null,<<-187.2977, -2374.7778, 8.3191>>,pedmove_run,default_time_never_warp)
						task_put_ped_directly_into_cover(null,<<-187.2977, -2374.7778, 8.3191>>,infinite_task_time,false,0.25,true,true,customcover,true)
					close_sequence_task(seq)
					task_perform_sequence(mike_ped(),seq)
					CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_MOVE1", CONV_PRIORITY_MEDIUM )
					CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Mike tasked into cover, imike_ai_stage++" )
					iStealthTimer = 0
					imike_ai_stage++
				break
				case 10
					IF iStealthTimer > 0
						IF (GET_GAME_TIMER() - iStealthTimer) > 1000
							if not isentityalive(mercs[merc14_c4_3].id)
							OR NOT IS_ENTITY_IN_TRIGGER_BOX( mercs[merc14_c4_3].id, sTriggerBox[TB_MIKE_WAIT_FOR_MERC14_TO_MOVE] )
								wtSneakingWeapon = wtBomb
								set_current_ped_weapon( mike_ped(),wtbomb,true )
								clear_sequence_task(seq)
								open_sequence_task(seq)					
									task_follow_nav_mesh_to_coord(null,<< -188.1089, -2365.5100, 8.3194 >>,pedmove_walk,default_time_never_warp)
									TASK_PLANT_BOMB(null,<< -188.1089, -2365.5100, 8.3194 >>,270)
								close_sequence_task(seq)
								task_perform_sequence(mike_ped(),seq)
								IF bMikeAsksForHelp
									IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY( mercs[merc14_c4_3].id, frank_ped() )
										CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_2A", CONV_PRIORITY_MEDIUM )
									ENDIF
								ENDIF
								CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Merc14 and 16 passed, Mike tasked to plant bomb, imike_ai_stage++" )
								iStealthTimer = 0
								bMikeAsksForHelp = FALSE
								imike_ai_stage++
							else
								if isentityalive(mercs[merc12_c4_3].id)
									if not DOES_BLIP_EXIST(mercs[merc12_c4_3].blip)
										mercs[merc12_c4_3].blip = CREATE_BLIP_FOR_PED(mercs[merc12_c4_3].id,true)
									endif
								endif
								if isentityalive(mercs[merc14_c4_3].id)
									if not DOES_BLIP_EXIST(mercs[merc14_c4_3].blip)
										mercs[merc14_c4_3].blip = CREATE_BLIP_FOR_PED(mercs[merc14_c4_3].id,true)
									endif
									if not bMikeAsksForHelp
										if IS_SAFE_TO_START_CONVERSATION()
											if IS_ENTITY_IN_TRIGGER_BOX( mercs[merc14_c4_3].id, sTriggerBox[TB_MIKE_WAIT_FOR_MERC14_TO_MOVE] )
												if CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_DECK2", CONV_PRIORITY_MEDIUM )
													bMikeAsksForHelp = TRUE
												endif
											endif
										endif
									endif
								endif
							endif
						ENDIF
					ELSE
						IF IS_ENTITY_AT_COORD( mike_ped(), <<-187.2977, -2374.7778, 8.3191>>, <<2.0,2.0,2.0>> )
						AND IS_PED_IN_COVER( mike_ped() )
							iStealthTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				break
				case 11
//					if is_projectile_type_in_angled_area((c4[c4_2_second_mast].exppos + <<-0.75,3,2>>),(c4[c4_2_second_mast].exppos + <<-0.75,3,-2>>),10,wtbomb)
					if c4[c4_2_second_mast].bplanted = true
						wtSneakingWeapon = wtCombatPistol
						safe_remove_blip(c4[c4_2_second_mast].blip)							
						icsdelay = get_game_timer()
						CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Bomb planted, imike_ai_stage++" )
						imike_ai_stage++							
					endif
				break
				case 12
					if get_game_timer() - icsdelay > 1000
						c4[c4_2_second_mast].bplanted = true
						CPRINTLN( DEBUG_MISSION, "MIKE_manage_ai_stealth: ai_stage: ", imike_ai_stage, ". Third bomb complete, imike_ai_stage++" )
						imike_ai_stage++
					endif
				break
				case 13
					//end state
				break
			endswitch
		break
//═══════════════════════════════════════════════════════════════════╡ bomb 4 ╞═════════════════════════════════════════════════════════════════════		
		case msf_5_fourth_bomb
			switch imike_ai_stage
				case 0
					wtSneakingWeapon = wtCombatPistol
					if mission_stage = enum_to_int(msf_5_fourth_bomb)
						remove_cover_point(customcover)
						customcover = add_cover_point(<< -193.7045, -2376.3953, 8.3186 >>, 88.3441,covuse_walltoright,covheight_toohigh,covarc_180)
						clear_sequence_task(seq)
						open_sequence_task(seq)					
							task_follow_nav_mesh_to_coord(null,<< -193.7045, -2376.3953, 8.3186 >>,pedmove_run,default_time_never_warp)
							task_put_ped_directly_into_cover(null,<< -193.7045, -2376.3953, 8.3186 >>,infinite_task_time,false,0.25,true,true,null,true)
						close_sequence_task(seq)
						task_perform_sequence(mike_ped(),seq)					
						imike_ai_stage++
					endif 
				break
				case 1
					if isentityalive(mercs[merc13_c4_3].id)
						if IS_SAFE_TO_START_CONVERSATION()
						AND NOT IS_MESSAGE_BEING_DISPLAYED()
							CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_STEPS2M", CONV_PRIORITY_MEDIUM )
							task_follow_to_offset_of_entity(mike_ped(),mercs[merc13_c4_3].id,<<0,0.25,0>>,pedmove_run,default_time_never_warp)
							istealthkill = 0
							imike_ai_stage++
						endif
					else
						imike_ai_stage = 3
					endif
				break
				case 2
					if MIKE_stealth_killing(mercs[merc13_c4_3])
					and get_game_timer() - istealthkilltimer > 2000
						wtSneakingWeapon = wtcombatpistol
						CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_GUY2M", CONV_PRIORITY_MEDIUM )
						imike_ai_stage++
					endif
				break
				case 3
					remove_cover_point(customcover)
					customcover = add_cover_point(<<-210.9928, -2375.6543, 8.3191>>, 88.3441,covuse_walltoright,covheight_toohigh,covarc_180)
					clear_sequence_task(seq)
					open_sequence_task(seq)					
						task_follow_nav_mesh_to_coord(null,<<-210.9928, -2375.6543, 8.3191>>,pedmove_run,default_time_never_warp)
						task_put_ped_directly_into_cover(null,<<-210.9928, -2375.6543, 8.3191>>,infinite_task_time,false,0.25,true,true,null,true)
					close_sequence_task(seq)
					task_perform_sequence(mike_ped(),seq)
					imike_ai_stage++
				break
				case 4
					if not isentityalive(mercs[MERC16_C4_4].id)			
						set_current_ped_weapon(mike_ped(),wtCombatPistol,true)
						clear_sequence_task(seq)
						open_sequence_task(seq)					
							task_follow_nav_mesh_to_coord(null,c4[c4_3_inside].exppos,pedmove_run,default_time_never_warp)					
						close_sequence_task(seq)
						task_perform_sequence(mike_ped(),seq)
						CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_MOVE1", CONV_PRIORITY_MEDIUM )
						imike_ai_stage++
					else
						if not DOES_BLIP_EXIST(mercs[MERC16_C4_4].blip)
							mercs[MERC16_C4_4].blip = CREATE_BLIP_FOR_PED(mercs[MERC16_C4_4].id,true)
						endif
					endif
				break
			endswitch
		break
	endswitch	
	
//-------------checks---------------
	set_ped_stealth_movement(mike_ped(),true)
endif
endproc

//@RJP returns TRUE if any merc from 3 to 18 is combatting Mike
FUNC BOOL IS_ANY_MERC_COMBATTING_MIKE()
	INT i
	
	FOR i = merc3_C4_2 TO merc18_c4_4
		IF isentityalive( mercs[i].id )
			IF IS_PED_IN_COMBAT( mercs[i].id, mike_ped() )
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

proc MIKE_manage_combat()
	if isentityalive(mike_ped())
		//@RJP michael should die easier
//		set_ped_suffers_critical_hits(mike_ped(),false)	
		set_ped_combat_attributes(mike_ped(), ca_use_cover,true)
		set_ped_combat_attributes(mike_ped(), ca_aggressive,true)		
		set_ped_combat_attributes(mike_ped(), ca_can_shoot_without_los ,false)
		set_ped_combat_ability(mike_ped(), cal_professional)
		set_ped_combat_movement(mike_ped(), CM_WILLADVANCE)
		set_ped_combat_range(mike_ped(),CR_MEDIUM)		
		SET_RAGDOLL_BLOCKING_FLAGS( mike_ped(), RBF_BULLET_IMPACT )
		
		IF NOT bMikeHealthWarn
			IF isentityalive(mike_ped())
				IF GET_ENTITY_HEALTH(mike_ped()) < 120
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_DIE_NS", CONV_PRIORITY_HIGH )
							CPRINTLN( DEBUG_MISSION, "Mike's health low, setting bMikeHealthWarn" )
							bMikeHealthWarn = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bMikeCombatWarn
			IF IS_ANY_MERC_COMBATTING_MIKE()
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_HLP_NS", CONV_PRIORITY_MEDIUM )
						bMikeCombatWarn = TRUE
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ANY_MERC_COMBATTING_MIKE()
				bMikeCombatWarn = FALSE
			ENDIF
		ENDIF
		
		//move towards first bomb area if not planted yet and close to it. 
		if mission_stage = enum_to_int(msf_2_first_bomb)
			if c4[c4_0_front_of_ship].bplanted = false
				set_ped_sphere_defensive_area(mike_ped(),<< -86.0638, -2366.2502, 13.8644 >>,10)
				IF IS_ANY_MERC_COMBATTING_MIKE()
					IF NOT IS_PED_IN_COMBAT( mike_ped() )
						set_current_ped_weapon(mike_ped(),GET_BEST_PED_WEAPON( mike_ped() ),true)
						CLEAR_PED_TASKS( mike_ped() )
						task_combat_hated_targets_around_ped(mike_ped(),40)
					ENDIF
				ELIF GET_SCRIPT_TASK_STATUS( mike_ped(), SCRIPT_TASK_PERFORM_SEQUENCE ) > PERFORMING_TASK
					CLEAR_PED_TASKS( mike_ped() )
					set_current_ped_weapon(mike_ped(),wtbomb,true)
					clear_sequence_task(seq)
					open_sequence_task(seq)	
						task_follow_nav_mesh_to_coord(null,<<-83.92709, -2366.11841, 13.29613>>,pedmove_run,default_time_never_warp)
						TASK_PLANT_BOMB(null,<<-83.92709, -2366.11841, 13.29613>>,90)
					close_sequence_task(seq)
					task_perform_sequence(mike_ped(),seq)
					clear_sequence_task(seq)
					CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_MVE_NS", CONV_PRIORITY_MEDIUM )
				ENDIF
			else
				set_ped_sphere_defensive_area(mike_ped(),<< -126.6504, -2375.7917, 9.6284 >>,10)
			endif
		//move towards the second bomb area unless its been planted if so move to the third
		elif mission_stage = enum_to_int(msf_3_second_bomb)
			if c4[c4_1_first_mast].bplanted = false
				set_ped_sphere_defensive_area(mike_ped(),<< -126.6504, -2375.7917, 9.6284 >>,10)
				IF IS_ANY_MERC_COMBATTING_MIKE()
					IF NOT IS_PED_IN_COMBAT( mike_ped() )
						set_current_ped_weapon(mike_ped(),GET_BEST_PED_WEAPON( mike_ped() ),true)
//						if has_ped_got_weapon(mike_ped(),weapontype_carbinerifle)
//							set_current_ped_weapon(mike_ped(),WEAPONTYPE_CARBINERIFLE,true)
//						endif
						CLEAR_PED_TASKS( mike_ped() )
						task_combat_hated_targets_around_ped(mike_ped(),40)
					ENDIF
				ELIF GET_SCRIPT_TASK_STATUS( mike_ped(), SCRIPT_TASK_PERFORM_SEQUENCE ) > PERFORMING_TASK
					CLEAR_PED_TASKS( mike_ped() )
					set_current_ped_weapon(mike_ped(),wtbomb,true)
					clear_sequence_task(seq)
					open_sequence_task(seq)	
						task_follow_nav_mesh_to_coord(null,<< -128.70566, -2367.17944, 8.31891 >>,pedmove_run,default_time_never_warp)
						TASK_PLANT_BOMB(null,<<-128.69624, -2366.82788, 9.22130 >>,270)
					close_sequence_task(seq)
					task_perform_sequence(mike_ped(),seq)
					clear_sequence_task(seq)
					CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_MVE_NS", CONV_PRIORITY_MEDIUM )
				ENDIF
			else
				set_ped_sphere_defensive_area(mike_ped(),<< -186.4871, -2375.8062, 9.7455 >>,10)
			endif
		//move towards the third bomb area unless its been planted other wise move further onwards.
		elif mission_stage = enum_to_int(msf_4_third_bomb)
			if c4[c4_2_second_mast].bplanted = false
				set_ped_sphere_defensive_area(mike_ped(),<< -186.4871, -2375.8062, 9.7455 >>,10)
				IF IS_ANY_MERC_COMBATTING_MIKE()
					IF NOT IS_PED_IN_COMBAT( mike_ped() )
						set_current_ped_weapon(mike_ped(),GET_BEST_PED_WEAPON( mike_ped() ),true)
						CLEAR_PED_TASKS( mike_ped() )
						task_combat_hated_targets_around_ped(mike_ped(),40)
					ENDIF
				ELIF GET_SCRIPT_TASK_STATUS( mike_ped(), SCRIPT_TASK_PERFORM_SEQUENCE ) > PERFORMING_TASK
					CLEAR_PED_TASKS( mike_ped() )
					set_current_ped_weapon(mike_ped(),wtbomb,true)
					clear_sequence_task(seq)
					open_sequence_task(seq)	
						task_follow_nav_mesh_to_coord(null,<< -188.1089, -2365.5100, 8.3194 >>,pedmove_run,default_time_never_warp)
						TASK_PLANT_BOMB(null,<< -188.1089, -2365.5100, 8.3194 >>,270)
					close_sequence_task(seq)
					task_perform_sequence(mike_ped(),seq)
					clear_sequence_task(seq)
					CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_MVE_NS", CONV_PRIORITY_MEDIUM )
				ENDIF
			else
				set_ped_sphere_defensive_area(mike_ped(),<< -211.9178, -2377.0576, 9.1952 >>,10)
			endif
		elif mission_stage = enum_to_int(msf_5_fourth_bomb)
			set_ped_sphere_defensive_area(mike_ped(),<< -216.4087, -2377.5801, 8.6987 >>,10)
			IF IS_ANY_MERC_COMBATTING_MIKE()
				IF NOT IS_PED_IN_COMBAT( mike_ped() )
					set_current_ped_weapon(mike_ped(),GET_BEST_PED_WEAPON( mike_ped() ),true)
					CLEAR_PED_TASKS( mike_ped() )
					task_combat_hated_targets_around_ped(mike_ped(),40)
				ENDIF
			ELIF GET_SCRIPT_TASK_STATUS( mike_ped(), SCRIPT_TASK_PERFORM_SEQUENCE ) > PERFORMING_TASK
				CLEAR_PED_TASKS( mike_ped() )
				clear_sequence_task(seq)
				open_sequence_task(seq)	
					task_follow_nav_mesh_to_coord(null,c4[c4_3_inside].exppos,pedmove_run,default_time_never_warp)
				close_sequence_task(seq)
				task_perform_sequence(mike_ped(),seq)
				clear_sequence_task(seq)
				CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_MVE_NS", CONV_PRIORITY_MEDIUM )
			ENDIF
		//once player cant see turn back to combat mode for franklin. defensive spehere is current location.
		elif mission_stage = enum_to_int(msf_6_way_out)
			ped_index clsped = null
			float peddist = 15
			int merc
			for merc = enum_to_int(merc1_start) to enum_to_int(merc26_car2)
				if isentityalive(mercs[merc].id)				
					if get_distance_between_coords(get_entity_coords(mike_ped()),get_entity_coords(mercs[merc].id)) < peddist
						clsped = mercs[merc].id
						peddist = get_distance_between_coords(get_entity_coords(mike_ped()),get_entity_coords(mercs[merc].id))
					endif
				endif
			endfor
			if peddist < 15				
				if isentityalive(clsped)
					set_ped_sphere_defensive_area(mike_ped(),get_entity_coords(mike_ped()),2)
					set_ped_combat_attributes(clsped,ca_use_cover,false)
				endif
			else
				set_ped_sphere_defensive_area(mike_ped(),get_entity_coords(mike_ped()),1)
			endif
		//move to final bomb spot if all else fails.
		else 
			set_ped_sphere_defensive_area(mike_ped(),<< -216.4087, -2377.5801, 8.6987 >>,2)
		endif			
	
//		if has_ped_got_weapon(mike_ped(),weapontype_carbinerifle)
//			set_current_ped_weapon(mike_ped(),weapontype_carbinerifle,true)
//		endif
		
//		if not is_ped_in_combat(mike_ped())
//			task_combat_hated_targets_around_ped(mike_ped(),40)			
//		endif
	endif
endproc
proc MERC_manage_force_tasks(mmf_mission_merc_flags merc)
		if isentityalive(mercs[merc].id)
			if isentityalive(mike_ped())
				if get_script_task_status(mercs[merc].id,script_task_combat)!= performing_task 
				and get_script_task_status(mercs[merc].id,script_task_follow_nav_mesh_to_coord) != performing_task
					task_combat_ped(mercs[merc].id,mike_ped())
				endif
			endif
		endif
endproc

PROC MIKE_WAY_OUT_CONV_MANAGER()
	SWITCH iWayOutDiag
		CASE 0
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_COVER", CONV_PRIORITY_MEDIUM )
					iWayOutDiag++
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_GOUP", CONV_PRIORITY_MEDIUM )
					iWayOutDiag++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_GUYON", CONV_PRIORITY_MEDIUM )
					iWayOutDiag++
				ENDIF
			ENDIF
		BREAK
		CASE 3
			//Do nothing here
		BREAK
		CASE 4
			IF  IS_SAFE_TO_START_CONVERSATION()
				IF isentityalive(mike_ped())
				AND isentityalive(mercs[merc13_C4_3].id)
					IF IS_ENTITY_AT_COORD( mike_ped(), <<-225.4856, -2377.7957, 16.3326>>, <<6,6,4>> )
						IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_GUYMORE", CONV_PRIORITY_MEDIUM )
							iWayOutDiag++
						ENDIF
					ENDIF
				ELSE
					iWayOutDiag++
				ENDIF
			ENDIF
		BREAK
		CASE 5
			//Do nothing here
		BREAK
		CASE 6
			IF IS_SAFE_TO_START_CONVERSATION()
				IF bFrankKilled13
				AND bFrankKilled14
					IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_TAKEOUT", CONV_PRIORITY_MEDIUM )
						iWayOutDiag++
					ENDIF
				ELSE
					IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_4H", CONV_PRIORITY_MEDIUM )
						iWayOutDiag++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 7
			IF IS_SAFE_TO_START_CONVERSATION()
				IF isentityalive(mike_ped())
					IF isentityalive(mercs[MERC15_C4_3].id)
					OR isentityalive(mercs[MERC16_C4_4].id)
						IF IS_ENTITY_AT_COORD( mike_ped(), <<-229.89893, -2379.28979, 12.33288>>, <<6,6,3>> )
							IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_MORE2", CONV_PRIORITY_MEDIUM )
								iWayOutDiag++
							ENDIF
						ENDIF
					ELSE
						iWayOutDiag++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 8
			//Do nothing here
		BREAK
		CASE 9
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_DEAD", CONV_PRIORITY_MEDIUM )
					iWayOutDiag++
				ENDIF
			ENDIF
		BREAK
		CASE 10
			IF IS_SAFE_TO_START_CONVERSATION()
				IF isentityalive(mike_ped())
					IF isentityalive(mercs[merc17_c4_4].id)
					OR isentityalive(mercs[merc18_c4_4].id)
						IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_MAIN", CONV_PRIORITY_MEDIUM )
							iWayOutDiag++
						ENDIF
					ELSE
						iWayOutDiag++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 11
			//Do nothing here
		BREAK
		CASE 12
			IF IS_SAFE_TO_START_CONVERSATION()
				IF NOT isentityalive(mercs[merc17_c4_4].id)
				AND NOT isentityalive(mercs[merc18_c4_4].id)
					IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_4H", CONV_PRIORITY_MEDIUM )
						iWayOutDiag++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 13
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_NORTH", CONV_PRIORITY_MEDIUM )
					iWayOutDiag++
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC HEATSCALE_MANAGER( PED_INDEX &ped, INT i )
	IF NOT isentityalive( ped )
		EXIT
	ENDIF
	
	SWITCH sPedHeatScale[i].iHeatState
		CASE 0
			IF sPedHeatScale[i].iHeatScale < 255
				sPedHeatScale[i].iHeatScale += 10
				IF sPedHeatScale[i].iHeatScale > 255
					sPedHeatScale[i].iHeatScale = 255
				ENDIF
				SET_ENTITY_VISIBLE( ped, TRUE )
				SET_PED_HEATSCALE_OVERRIDE( ped, sPedHeatScale[i].iHeatScale )
			ELSE
				sPedHeatScale[i].iHeatState ++
			ENDIF
		BREAK
		CASE 1
			DISABLE_PED_HEATSCALE_OVERRIDE( ped )
			sPedHeatScale[i].iHeatState ++
		BREAK
		CASE 2
			//End case
		BREAK
	ENDSWITCH
ENDPROC

func bool MIKE_way_out()	
	
	HEATSCALE_MANAGER( peds[mpf_mike].id, 0 )
	
	switch iescapeStage
	// right at top
		case 0
			//@RJP add navmesh blockers to prevent Michael going around the back side of the boat
			ADD_NAVMESH_BLOCKING_OBJECT( <<-227.320953,-2359.709961,25.235537>>,<<3.000000,2.000000,2.000000>>, 0 )
			ADD_NAVMESH_BLOCKING_OBJECT( <<-236.819199,-2359.233643,17.231823>>,<<6.000000,3.000000,2.000000>>, 0 )
			ADD_NAVMESH_BLOCKING_OBJECT( <<-213.448837,-2375.129150,10.319207>>,<<2.500000,1.000000,2.000000>>, 0 )
			ADD_NAVMESH_BLOCKING_OBJECT( <<-222.074005,-2374.517578,14.051224>>,<<2.000000,1.000000,2.000000>>, 0 )
			SET_ENTITY_VISIBLE( mike_ped(), TRUE )
			//mike
			SET_PED_SPHERE_DEFENSIVE_AREA(mike_ped(),<<-225.4856, -2377.7957, 16.3326>>,2,true)
			CLEAR_SEQUENCE_TASK(seq)
			OPEN_SEQUENCE_TASK(seq)
				TASK_FOLLOW_WAYPOINT_RECORDING( null, "docksheist2A02" )
				TASK_PUT_PED_DIRECTLY_INTO_COVER(null,<<-225.4856, -2377.7957, 16.3326>>,-1,true,0.25,false,false,null,true)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(null,50)
			CLOSE_SEQUENCE_TASK(seq)
			TASK_PERFORM_SEQUENCE(mike_ped(),seq)
			CLEAR_SEQUENCE_TASK(seq)
			iCSDelay = (GET_GAME_TIMER()+4000)
			iescapeStage++
		break
		
		case 1
			IF (iWayOutDiag = 3)
			AND GET_GAME_TIMER() > iCSDelay
				//merc from top right
				createmercenary(merc13_C4_3,50, TRUE)
				SET_RAGDOLL_BLOCKING_FLAGS(mercs[merc13_C4_3].id, RBF_BULLET_IMPACT)
				SET_ENTITY_COORDS(mercs[merc13_C4_3].id,<<-214.50433, -2355.67139, 16.33203>>)
				SET_ENTITY_HEADING( mercs[merc13_C4_3].id, 285.8376 )
				SET_PED_SPHERE_DEFENSIVE_AREA(mercs[merc13_C4_3].id,<<-213.24863, -2374.95581, 16.33176>>,2,true)
				SET_ENTITY_VISIBLE( mercs[merc13_C4_3].id, FALSE )
				SET_PED_HEATSCALE_OVERRIDE( mercs[merc13_C4_3].id, 0 )
				CLEAR_SEQUENCE_TASK(seq)
				OPEN_SEQUENCE_TASK(seq)
					TASK_FOLLOW_NAV_MESH_TO_COORD( null, <<-213.24863, -2374.95581, 16.33176>>, PEDMOVE_RUN )
					TASK_PUT_PED_DIRECTLY_INTO_COVER(null,<<-213.24863, -2374.95581, 16.33176>>,2000,true,0.25,false,false,null,true)
					TASK_COMBAT_PED(null,mike_ped())
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(mercs[merc13_C4_3].id,seq)
				CLEAR_SEQUENCE_TASK(seq)
				//merc from top left
				createmercenary(merc14_C4_3,50, TRUE)
				SET_RAGDOLL_BLOCKING_FLAGS(mercs[merc14_C4_3].id, RBF_BULLET_IMPACT)
				set_entity_coords(mercs[merc14_C4_3].id,<< -220.8900, -2369.9204, 24.3350 >> )
				SET_ENTITY_HEADING( mercs[merc14_C4_3].id, 111.7760 )
				SET_PED_SPHERE_DEFENSIVE_AREA(mercs[merc14_C4_3].id,<<-231.06622, -2374.59497, 16.33076>>,2,true)
				SET_ENTITY_VISIBLE( mercs[merc14_C4_3].id, FALSE )
				SET_PED_HEATSCALE_OVERRIDE( mercs[merc14_C4_3].id, 0 )
				CLEAR_SEQUENCE_TASK(seq)
				OPEN_SEQUENCE_TASK(seq)
					TASK_FOLLOW_WAYPOINT_RECORDING( null, "docksheist2A03" )
					TASK_PUT_PED_DIRECTLY_INTO_COVER(null,<<-231.06622, -2374.59497, 16.33076>>,2000,true,0.25,false,false,null,true)
					TASK_COMBAT_PED(null,mike_ped())
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(mercs[merc14_C4_3].id,seq)
				CLEAR_SEQUENCE_TASK(seq)
				iWayOutDiag = 4
				iescapeStage++
			ENDIF
		break
		
		// second level
		case 2
			if not isentityalive(mercs[merc13_C4_3].id)
			and not isentityalive(mercs[merc14_C4_3].id)
			and not is_entity_occluded(mike_ped())
				//merc 15 & 16
				createmercenary(MERC16_C4_4,50, TRUE)
				SET_RAGDOLL_BLOCKING_FLAGS(mercs[MERC16_C4_4].id, RBF_BULLET_IMPACT)
				set_entity_coords(mercs[MERC16_C4_4].id, <<-250.5471, -2363.4216, 8.3192>>)
				SET_ENTITY_HEADING( mercs[MERC16_C4_4].id, 186.5092 )
				SET_PED_SPHERE_DEFENSIVE_AREA(mercs[MERC16_C4_4].id,<<-248.8481, -2375.2900, 8.3192>>,2,true)
				SET_ENTITY_VISIBLE( mercs[MERC16_C4_4].id, FALSE )
				SET_PED_HEATSCALE_OVERRIDE( mercs[MERC16_C4_4].id, 0 )
				TASK_COMBAT_PED(mercs[MERC16_C4_4].id,mike_ped())
				
				createmercenary(MERC15_C4_3,50, TRUE)
				SET_RAGDOLL_BLOCKING_FLAGS(mercs[MERC15_C4_3].id, RBF_BULLET_IMPACT)
				set_entity_coords(mercs[MERC15_C4_3].id, <<-250.6802, -2364.6062, 8.3192>>)
				SET_ENTITY_HEADING( mercs[MERC15_C4_3].id, 197.7292 )
				SET_PED_SPHERE_DEFENSIVE_AREA(mercs[MERC15_C4_3].id,<<-245.86349, -2376.99048, 8.31911>>,2,true)
				SET_ENTITY_VISIBLE( mercs[MERC15_C4_3].id, FALSE )
				SET_PED_HEATSCALE_OVERRIDE( mercs[MERC15_C4_3].id, 0 )
				TASK_COMBAT_PED(mercs[MERC15_C4_3].id,mike_ped())
				//mike
				SET_PED_SPHERE_DEFENSIVE_AREA(mike_ped(),<<-229.89893, -2379.28979, 12.33288>>,5,true)
				CLEAR_SEQUENCE_TASK(seq)
				OPEN_SEQUENCE_TASK(seq)
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<-229.89893, -2379.28979, 12.33288>>,PEDMOVE_RUN,DEFAULT_TIME_NEVER_WARP) 
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(null,50)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(mike_ped(),seq)
				CLEAR_SEQUENCE_TASK(seq)
				iWayOutDiag = 6
				iescapeStage++
			else
				HEATSCALE_MANAGER( mercs[merc13_C4_3].id, 1 )
				HEATSCALE_MANAGER( mercs[merc14_C4_3].id, 2 )
			endif
		break
		//on deck
		case 3
			if not isentityalive(mercs[MERC16_C4_4].id)
			and not isentityalive(mercs[MERC15_C4_3].id)
			and not is_entity_occluded(mike_ped())
				//merc 17 & 18
				createmercenary(merc17_c4_4,50, TRUE)
				SET_RAGDOLL_BLOCKING_FLAGS(mercs[merc17_c4_4].id, RBF_BULLET_IMPACT)
				set_entity_coords(mercs[merc17_c4_4].id,<<-209.40309, -2362.73633, 8.31911>>)
				SET_PED_SPHERE_DEFENSIVE_AREA(mercs[merc17_c4_4].id,<<-210.73999, -2375.15332, 8.31911>>,2,true)
				SET_ENTITY_VISIBLE( mercs[merc17_c4_4].id, FALSE )
				SET_PED_HEATSCALE_OVERRIDE( mercs[merc17_c4_4].id, 0 )
				TASK_COMBAT_PED(mercs[merc17_c4_4].id,mike_ped())
				
				createmercenary(merc18_c4_4,50, TRUE)
				SET_RAGDOLL_BLOCKING_FLAGS(mercs[merc18_c4_4].id, RBF_BULLET_IMPACT)
				set_entity_coords(mercs[merc18_c4_4].id,<<-207.84383, -2363.50244, 8.31911>>)
				SET_PED_SPHERE_DEFENSIVE_AREA(mercs[merc18_c4_4].id,<<-211.05281, -2377.40332, 8.31911>>,2,true)
				SET_ENTITY_VISIBLE( mercs[merc18_c4_4].id, FALSE )
				SET_PED_HEATSCALE_OVERRIDE( mercs[merc18_c4_4].id, 0 )
				TASK_COMBAT_PED(mercs[merc18_c4_4].id,mike_ped())
				//mike
				SET_PED_SPHERE_DEFENSIVE_AREA(mike_ped(),<<-218.31186, -2377.13525, 8.31911>>,2,true)
				CLEAR_SEQUENCE_TASK(seq)
				OPEN_SEQUENCE_TASK(seq)
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<-222.1750, -2376.2395, 12.3329>>,PEDMOVE_RUN,DEFAULT_TIME_NEVER_WARP) 
					TASK_FOLLOW_NAV_MESH_TO_COORD(null,<<-218.31186, -2377.13525, 8.31911>>,PEDMOVE_RUN,DEFAULT_TIME_NEVER_WARP) 
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(null,30)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(mike_ped(),seq)
				CLEAR_SEQUENCE_TASK(seq)
				iWayOutDiag = 9
				iescapeStage++
			else
				HEATSCALE_MANAGER( mercs[MERC16_C4_4].id, 3 )
				HEATSCALE_MANAGER( mercs[MERC15_C4_3].id, 4 )
			ENDIF
		break
		case 4
			if not isentityalive(mercs[merc17_c4_4].id)
			and not isentityalive(mercs[merc18_c4_4].id)
			and not is_entity_occluded(mike_ped())
				//music cue
				trigger_music_event("dh2a_clear_path")
				CLEAR_SEQUENCE_TASK(seq)
				OPEN_SEQUENCE_TASK(seq)
					TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( null, TRUE )
					IF NOT IS_ENTITY_AT_COORD(  mike_ped(), <<-218.31186, -2377.13525, 8.31911>>, <<8,8,2>> )
						TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<-218.31186, -2377.13525, 8.31911>>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP )
					ENDIF
					task_follow_waypoint_recording(null,"docksheist2a01",0,ewaypoint_start_from_closest_point | ewaypoint_use_tighter_turn_settings)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(mike_ped(),seq)
				CLEAR_SEQUENCE_TASK(seq)
				iWayOutDiag = 12
				return true
			else
				HEATSCALE_MANAGER( mercs[merc17_c4_4].id, 5 )
				HEATSCALE_MANAGER( mercs[merc18_c4_4].id, 6 )
			endif
		break
	endswitch
	//@RJP removing MERC_manage_force_tasks as the tasking above handles combat fine and tasking them this way can break the AI
//	int merc
//	for merc = enum_to_int(merc11_C4_3) to enum_to_int(merc18_c4_4)
//		if isentityalive(mercs[merc].id)
//			MERC_manage_force_tasks(int_to_enum(MMF_MISSION_MERC_FLAGS,merc))
//		endif
//	endfor
	return false
endfunc 
proc MERC_manage_combat()	
	if isentityalive(mike_ped())
	
		float fdefdist = get_distance_between_coords(get_entity_coords(mike_ped()),<<-116, -2366, 12>>)
		boat_section section = boat_front
		if get_distance_between_coords(get_entity_coords(mike_ped()),<<-154, -2366, 12>>) < fdefdist
			fdefdist = get_distance_between_coords(get_entity_coords(mike_ped()),<<-154, -2366, 12>>) 
			section = boat_mid1
		endif
		if get_distance_between_coords(get_entity_coords(mike_ped()),<<-186, -2366, 12>>) < fdefdist
			fdefdist = get_distance_between_coords(get_entity_coords(mike_ped()),<<-186, -2366, 12>>)
			section = boat_mid2
		endif
		if get_distance_between_coords(get_entity_coords(mike_ped()),<<-223, -2365, 12>>) < fdefdist
			fdefdist =  get_distance_between_coords(get_entity_coords(mike_ped()),<<-223, -2365, 12>>)
			section = boat_back
		endif
		
		float fdist
		int merc
		for merc = enum_to_int(merc1_start) to enum_to_int(merc18_c4_4)
			if isentityalive(mercs[merc].id)
				if not mercs[merc].bzombie
					REMOVE_PED_DEFENSIVE_AREA( mercs[merc].id )
					IF GET_PED_COMBAT_MOVEMENT( mercs[merc].id ) = CM_DEFENSIVE
						SET_PED_COMBAT_ATTRIBUTES( mercs[merc].id, CA_CAN_CHARGE, TRUE )
						SET_PED_COMBAT_MOVEMENT( mercs[merc].id, CM_WILLADVANCE )
					ENDIF
					IF GET_PED_COMBAT_RANGE( mercs[merc].id ) != CR_NEAR
						SET_PED_COMBAT_RANGE( mercs[merc].id, CR_NEAR )
					ENDIF
					if not DOES_BLIP_EXIST( mercs[merc].blip )
						mercs[merc].blip = create_blip_for_ped( mercs[merc].id, TRUE )
					else
						SET_BLIP_SHOW_CONE( mercs[merc].blip, FALSE )
					endif
					//@RJP Make sure no peds are in combat with franklin if player is Michael
					if PLAYER_PED_ID() = mike_ped()
						if IS_PED_IN_COMBAT( mercs[merc].id, frank_ped() )
							CLEAR_PED_TASKS( mercs[merc].id )
						endif
					endif
					if not is_ped_in_combat(mercs[merc].id)
						//@RJP Simplifying merc combat AI to go to Mike and enter combat
						switch section
							case boat_front
		//						set_ped_sphere_defensive_area(mercs[merc].id,mercs[merc].vdef1,2)
								fdist = get_distance_between_coords(get_entity_coords(mercs[merc].id),mercs[merc].vdef1)
								if fdist > 35
								or get_distance_between_coords(get_entity_coords(mercs[merc].id), GET_ENTITY_COORDS( mike_ped())) > 35
									if get_script_task_status(mercs[merc].id,script_task_follow_nav_mesh_to_coord) != performing_task
									and get_script_task_status(mercs[merc].id,script_task_follow_nav_mesh_to_coord) != WAITING_TO_START_TASK
										REMOVE_PED_DEFENSIVE_AREA( mercs[merc].id )
										task_follow_nav_mesh_to_coord(mercs[merc].id,mercs[merc].vdef1,pedmove_run,default_time_never_warp)
									endif
									
									//special case fix for enemy number 18, bug B*1858582
									//if the ped reaches his destination but is still further away from michael than 35 go to combat
									IF ( merc = enum_to_int(merc18_c4_4) )
										if fdist < 2
										and get_distance_between_coords(get_entity_coords(mercs[merc].id), GET_ENTITY_COORDS( mike_ped())) > 35
											REMOVE_PED_DEFENSIVE_AREA( mercs[merc].id )
											SET_PED_COMBAT_ATTRIBUTES( mercs[merc].id, CA_CAN_CHARGE, TRUE )
											task_combat_ped(mercs[merc].id,mike_ped(), COMBAT_PED_DISABLE_AIM_INTRO)
										ENDIF
									ENDIF
									
									
								else 
									if not is_ped_in_combat(mercs[merc].id,mike_ped())
										//@RJP release ped to AI for combat
										REMOVE_PED_DEFENSIVE_AREA( mercs[merc].id )
										SET_PED_COMBAT_ATTRIBUTES( mercs[merc].id, CA_CAN_CHARGE, TRUE )
										task_combat_ped(mercs[merc].id,mike_ped(), COMBAT_PED_DISABLE_AIM_INTRO)
									endif
								endif
							break
							case boat_mid1
		//						set_ped_sphere_defensive_area(mercs[merc].id,mercs[merc].vdef2,2)
								fdist = get_distance_between_coords(get_entity_coords(mercs[merc].id),mercs[merc].vdef2)
								if fdist > 35
								or get_distance_between_coords(get_entity_coords(mercs[merc].id),GET_ENTITY_COORDS( mike_ped())) > 35
									if get_script_task_status(mercs[merc].id,script_task_follow_nav_mesh_to_coord)!= performing_task
									and get_script_task_status(mercs[merc].id,script_task_follow_nav_mesh_to_coord) != WAITING_TO_START_TASK
										REMOVE_PED_DEFENSIVE_AREA( mercs[merc].id )
										task_follow_nav_mesh_to_coord(mercs[merc].id,mercs[merc].vdef2,pedmove_run,default_time_never_warp)
									endif
								else
									if not is_ped_in_combat(mercs[merc].id,mike_ped())	
										//@RJP release ped to AI for combat
										REMOVE_PED_DEFENSIVE_AREA( mercs[merc].id )
										SET_PED_COMBAT_ATTRIBUTES( mercs[merc].id, CA_CAN_CHARGE, TRUE )
										task_combat_ped(mercs[merc].id,mike_ped(), COMBAT_PED_DISABLE_AIM_INTRO)
									endif
								endif
							break
							case boat_mid2
		//						set_ped_sphere_defensive_area(mercs[merc].id,mercs[merc].vdef3,2) // other wise back to decent location.
								fdist = get_distance_between_coords(get_entity_coords(mercs[merc].id),mercs[merc].vdef3)
								if fdist > 35
								or get_distance_between_coords(get_entity_coords(mercs[merc].id),GET_ENTITY_COORDS( mike_ped())) > 35
									if get_script_task_status(mercs[merc].id,script_task_follow_nav_mesh_to_coord)!= performing_task
									and get_script_task_status(mercs[merc].id,script_task_follow_nav_mesh_to_coord) != WAITING_TO_START_TASK
										REMOVE_PED_DEFENSIVE_AREA( mercs[merc].id )
										task_follow_nav_mesh_to_coord(mercs[merc].id,mercs[merc].vdef3,pedmove_run,default_time_never_warp)
									endif
								else
									if not is_ped_in_combat(mercs[merc].id,mike_ped())	
										//@RJP release ped to AI for combat
										REMOVE_PED_DEFENSIVE_AREA( mercs[merc].id )
										SET_PED_COMBAT_ATTRIBUTES( mercs[merc].id, CA_CAN_CHARGE, TRUE )
										task_combat_ped(mercs[merc].id,mike_ped(), COMBAT_PED_DISABLE_AIM_INTRO)
									endif
								endif
							break
							case boat_back
		//						set_ped_sphere_defensive_area(mercs[merc].id,mercs[merc].vdef4,2) // other wise back to decent location.	
								fdist = get_distance_between_coords(get_entity_coords(mercs[merc].id),mercs[merc].vdef4)
								if fdist > 35
								or get_distance_between_coords(get_entity_coords(mercs[merc].id),GET_ENTITY_COORDS( mike_ped())) > 35
									if get_script_task_status(mercs[merc].id,script_task_follow_nav_mesh_to_coord)!= performing_task
									and get_script_task_status(mercs[merc].id,script_task_follow_nav_mesh_to_coord) != WAITING_TO_START_TASK
										REMOVE_PED_DEFENSIVE_AREA( mercs[merc].id )
										task_follow_nav_mesh_to_coord(mercs[merc].id,mercs[merc].vdef4,pedmove_run,default_time_never_warp)
									endif
								else
									if not is_ped_in_combat(mercs[merc].id,mike_ped())	
										//@RJP release ped to AI for combat
										REMOVE_PED_DEFENSIVE_AREA( mercs[merc].id )
										SET_PED_COMBAT_ATTRIBUTES( mercs[merc].id, CA_CAN_CHARGE, TRUE )
										task_combat_ped(mercs[merc].id,mike_ped(), COMBAT_PED_DISABLE_AIM_INTRO)
									endif
								endif
							break
						endswitch
					endif
				endif
			endif 
		endfor
	endif
endproc

proc MERC_react_to_sniper_fire(INT i)
	SWITCH i%2
		CASE 0
			task_play_anim(null, "missheistdocks2a@crouch", "enter_crouch_a", normal_blend_in, normal_blend_out)
			task_play_anim(null, "missheistdocks2a@crouch", "crouching_idle_a", normal_blend_in, REALLY_SLOW_BLEND_OUT)
		BREAK
		CASE 1
			task_play_anim(null,"missheistdocks2a@alert",sduckanim,normal_blend_in,NORMAL_BLEND_OUT)
		BREAK
	ENDSWITCH
	TASK_COMBAT_PED(null, frank_ped())
endproc
proc MERC_do_reaction()
	merc_situation_enum current_merc_rs
	current_merc_rs = ms_sit_none
	
	IF NOT IS_GAMEPLAY_CAM_RENDERING()
		EXIT
	ENDIF
	
	int i
	int iDeadMerc
	WEAPON_TYPE eCurrentWeaponType
	for i = enum_to_int(merc3_C4_2) to enum_to_int(merc18_c4_4)
		if isentityalive(mercs[i].id)
		and isentityalive(mike_ped())
		and isentityalive(frank_ped())
			if mercs[i].bStealthKill
				EXIT
			endif
			if not is_ped_in_combat(mercs[i].id)
				if current_merc_rs = MS_SIT_ATTACKED
					current_merc_rs = ms_sit_none
				endif
				if has_entity_been_damaged_by_entity(mercs[i].id,mike_ped())
					current_merc_rs = MS_SIT_ATTACKED
				endif
//--------------------michael seen reaction--------------------
				if current_merc_rs = ms_sit_mike_seen
					current_merc_rs = ms_sit_none
				endif
				//@RJP can_ped_see_hated_ped breaks for peds with without blocking of non-temp events
				if can_ped_see_hated_ped(mercs[i].id,mike_ped())
				or CAN_PED_HEAR_PLAYER( PLAYER_ID(), mercs[i].id )
//				or has_entity_been_damaged_by_entity(mercs[i].id,mike_ped())
					CPRINTLN( DEBUG_MISSION,"ms_sit_mike_seen: merc ",(i+1) )
					current_merc_rs = ms_sit_mike_seen
				endif
//--------------------	shot heard ------------------------------
				if current_merc_rs = ms_sit_shot_heard
					current_merc_rs = ms_sit_none
				endif
				if mission_stage > ENUM_TO_INT(msf_1_snipe_gaurds)
					if current_merc_rs = ms_sit_none		
						if has_ped_received_event(mercs[i].id,event_shot_fired)
						or has_ped_received_event(mercs[i].id,EVENT_SHOCKING_GUNSHOT_FIRED)	
	//					or has_ped_received_event(mercs[i].id,event_shout_target_position)
							current_merc_rs = ms_sit_shot_heard
						endif
						IF NOT IS_PED_CURRENT_WEAPON_SILENCED(MIKE_PED())
							IF GET_CURRENT_PED_WEAPON(MIKE_PED(), eCurrentWeaponType)
								IF IS_PED_SHOOTING(MIKE_PED())
									IF (eCurrentWeaponType != WEAPONTYPE_STICKYBOMB)
										CPRINTLN( DEBUG_MISSION,"ms_sit_bullet_ping: MICHAEL IS SHOOTING A NON_SILENCED WEAPON" )
										IF VDIST2(GET_ENTITY_COORDS(mercs[i].id), GET_ENTITY_COORDS(MIKE_PED())) < 2025	//45 m, enough for ped on stairs to hear unsilenced weapon
											CPRINTLN( DEBUG_MISSION,"ms_sit_bullet_ping: hearing unsilenced weapon: merc ",(i+1) )
											current_merc_rs = ms_sit_shot_heard
										ENDIF
									ELSE
										CPRINTLN( DEBUG_MISSION,"ms_sit_bullet_ping: MICHAEL IS SHOOTING A WEAPONTYPE_STICKYBOMB" )
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					endif
				endif
//--------near bullet miss from micheal and franklin-------------
			
				if current_merc_rs = ms_sit_bullet_ping
					current_merc_rs = ms_sit_none
				endif
				if current_merc_rs = ms_sit_none
				or current_merc_rs = ms_sit_shot_heard
					if player_ped_id() = frank_ped()
						//@RJP tweaking these values so peds react to bullets greater than 2 meters away
//						if is_sniper_bullet_in_area(get_offset_from_entity_in_world_coords(mercs[i].id,<<-2,-2,-1>>),get_offset_from_entity_in_world_coords(mercs[i].id,<<2,2,1>>))
						if is_sniper_bullet_in_area(get_offset_from_entity_in_world_coords(mercs[i].id,<<-5,-5,-1>>),get_offset_from_entity_in_world_coords(mercs[i].id,<<5,5,1>>))
							CPRINTLN( DEBUG_MISSION,"ms_sit_bullet_ping: merc ",(i+1) )
							current_merc_rs = ms_sit_bullet_ping
							if bMikeAsksForHelp
								if IS_SAFE_TO_START_CONVERSATION()
									CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_MISS", CONV_PRIORITY_MEDIUM )
								endif
							endif
						endif
					elif player_ped_id() = mike_ped()
						if bFrankFired	
							current_merc_rs = ms_sit_none
						else
							if has_ped_received_event(mercs[i].id,event_shot_fired_bullet_impact)															
								CPRINTLN( DEBUG_MISSION,"ms_sit_bullet_ping: merc ",(i+1) )
								current_merc_rs = ms_sit_bullet_ping	
							endif
						endif
					endif
				endif
////--------------------	body found ------------------------------
				VECTOR vDeadBody
				if current_merc_rs = ms_sit_dead_body
					current_merc_rs = ms_sit_none
				endif
				if mercs[i].currentsit = ms_sit_none
					if current_merc_rs = ms_sit_none
						if has_ped_received_event(mercs[i].id,EVENT_ACQUAINTANCE_PED_DEAD)
							current_merc_rs = ms_sit_dead_body
						endif
						int j
						//@RJP iterate over dead mercs to see if merc can see a dead body
						MMF_MISSION_MERC_FLAGS startMerc
						IF mission_stage < ENUM_TO_INT(msf_2_first_bomb)
							startMerc = merc3_C4_2
						ELSE
							startMerc = merc1_start
						ENDIF
						for j = startMerc to merc18_c4_4
							if DOES_ENTITY_EXIST( mercs[j].id )
							AND NOT (j = ENUM_TO_INT(MERC16_C4_4))
								if IS_PED_INJURED( mercs[j].id )
									vDeadBody = GET_ENTITY_COORDS(mercs[j].id, FALSE)
									if vDeadBody.z < 10.5
									and vDeadBody.z > 8.3
									or IS_ENTITY_IN_TRIGGER_BOX(mercs[i].id, sTriggerBox[TB_BOAT_BOW_UPPER_AREA])
										if GET_DISTANCE_BETWEEN_COORDS( GET_ENTITY_COORDS(mercs[i].id), vDeadBody ) < 15
											if HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT( mercs[i].id, mercs[j].id )
												current_merc_rs = ms_sit_dead_body
												mercs[i].bSpottedDead = TRUE
												iDeadMerc = j
											endif
										endif
									endif
								elif IS_PED_SHOOTING( mercs[j].id )
									IF VDIST2( GET_ENTITY_COORDS(mercs[j].id), GET_ENTITY_COORDS(mercs[i].id) ) < 900	//30 m
										IF NOT IS_ENTITY_OCCLUDED( mercs[i].id )
											CPRINTLN( DEBUG_MISSION,"ms_sit_shot_heard: hearing another guard's weapon: merc ",(i+1) )
											current_merc_rs = ms_sit_shot_heard
										ENDIF
									ENDIF
								endif
							endif
						endfor
					endif
				endif
//---------------------------------------------------------------	
				if current_merc_rs <> ms_sit_none
					if mercs[i].currentsit <> current_merc_rs
										
						ireactionlength = -1
						clear_sequence_task(seq)
						open_sequence_task(seq)
						switch current_merc_rs
							case MS_SIT_ATTACKED
								set_ped_seeing_range(mercs[i].id,16)
								SET_PED_HEARING_RANGE(mercs[i].id,16)
								task_combat_ped(null,mike_ped())
							break
							case ms_sit_mike_seen		
							//add timer in here for how long they have seen mike 
								ireactionlength = get_random_int_in_range(10000, 15000)
								set_ped_seeing_range(mercs[i].id,16)
								SET_PED_HEARING_RANGE(mercs[i].id,16)
								TASK_AIM_GUN_AT_ENTITY(NULL, MIKE_PED(), 1500)
								task_combat_ped(null,mike_ped())
								CPRINTLN( DEBUG_MISSION, "ms_sit_mike_seen: merc ",(i+1))
								//@RJP we should print a line making players aware that something happened and they should kill the mercs before alarm is raised
								if not mercs[i].bPrintHelp
									if not IS_THIS_HELP_MESSAGE_BEING_DISPLAYED( "DCKH_QUICK" )
										print_help("DCKH_QUICK")
									endif
									mercs[i].bPrintHelp = true
								endif
							break
							case ms_sit_shot_heard
								set_ped_seeing_range(mercs[i].id,16)
								SET_PED_HEARING_RANGE(mercs[i].id,16)							
								ireactionlength = get_random_int_in_range(30000, 60000)
								task_aim_gun_at_entity(null,mike_ped(),1000)
								task_go_to_coord_while_aiming_at_coord(null,get_entity_coords(mike_ped()),get_entity_coords(mike_ped()),PEDMOVE_RUN,false,1)
								CPRINTLN( DEBUG_MISSION,"ms_sit_shot_heard: merc ",(i+1) )
								//@RJP we should print a line making players aware that something happened and they should kill the mercs before alarm is raised
								if not mercs[i].bPrintHelp
									if not IS_THIS_HELP_MESSAGE_BEING_DISPLAYED( "DCKH_QUICK" )
										print_help("DCKH_QUICK")
									endif
									mercs[i].bPrintHelp = true
								endif
							break
							case MS_SIT_DEAD_BODY
								//@RJP task merc to go to dead body and look around
								CPRINTLN( DEBUG_MISSION, "MERC_do_reaction: merc, ", i, " has spotted a dead body." )
								set_ped_seeing_range(mercs[i].id,16)
								SET_PED_HEARING_RANGE(mercs[i].id,16)
								SET_PED_ALERTNESS( mercs[i].id, AS_VERY_ALERT )
								TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD( null, GET_ENTITY_COORDS( mercs[iDeadMerc].id, FALSE ), GET_ENTITY_COORDS( mercs[iDeadMerc].id, FALSE ), PEDMOVE_RUN, FALSE, 3.0 )
//								TASK_FOLLOW_NAV_MESH_TO_COORD( null, GET_ENTITY_COORDS( mercs[iDeadMerc].id, FALSE ), PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP, 3.0 )
								TASK_AIM_GUN_AT_COORD( null, (GET_ENTITY_COORDS(mercs[i].id)+<<GET_RANDOM_INT_IN_RANGE(0,10),GET_RANDOM_INT_IN_RANGE(0,10),0>>), GET_RANDOM_INT_IN_RANGE(2000,2500) )
								TASK_AIM_GUN_AT_COORD( null, (GET_ENTITY_COORDS(mercs[i].id)-<<GET_RANDOM_INT_IN_RANGE(0,10),GET_RANDOM_INT_IN_RANGE(0,10),0>>), GET_RANDOM_INT_IN_RANGE(2000,2500) )
								TASK_AIM_GUN_AT_COORD( null, (GET_ENTITY_COORDS(mercs[i].id)+<<GET_RANDOM_INT_IN_RANGE(0,10),GET_RANDOM_INT_IN_RANGE(0,10),0>>), GET_RANDOM_INT_IN_RANGE(2000,2500) )
								TASK_AIM_GUN_AT_COORD( null, (GET_ENTITY_COORDS(mercs[i].id)-<<GET_RANDOM_INT_IN_RANGE(0,10),GET_RANDOM_INT_IN_RANGE(0,10),0>>), GET_RANDOM_INT_IN_RANGE(2000,2500) )
								TASK_AIM_GUN_AT_COORD( null, (GET_ENTITY_COORDS(mercs[i].id)+<<GET_RANDOM_INT_IN_RANGE(0,10),GET_RANDOM_INT_IN_RANGE(0,10),0>>), GET_RANDOM_INT_IN_RANGE(2000,2500) )
								TASK_AIM_GUN_AT_COORD( null, (GET_ENTITY_COORDS(mercs[i].id)-<<GET_RANDOM_INT_IN_RANGE(0,10),GET_RANDOM_INT_IN_RANGE(0,10),0>>), GET_RANDOM_INT_IN_RANGE(2000,2500) )
								TASK_AIM_GUN_AT_COORD( null, (GET_ENTITY_COORDS(mercs[i].id)+<<GET_RANDOM_INT_IN_RANGE(0,10),GET_RANDOM_INT_IN_RANGE(0,10),0>>), GET_RANDOM_INT_IN_RANGE(2000,2500) )
								TASK_AIM_GUN_AT_COORD( null, (GET_ENTITY_COORDS(mercs[i].id)-<<GET_RANDOM_INT_IN_RANGE(0,10),GET_RANDOM_INT_IN_RANGE(0,10),0>>), GET_RANDOM_INT_IN_RANGE(2000,2500) )
								ireactionlength = get_random_int_in_range(10000,15000)
								//@RJP we should print a line making players aware that something happened and they should kill the mercs before alarm is raised
								if not mercs[i].bPrintHelp
									if not IS_THIS_HELP_MESSAGE_BEING_DISPLAYED( "DCKH_QUICK" )
										print_help("DCKH_QUICK")
									endif
									mercs[i].bPrintHelp = true
								endif
							break
							case ms_sit_bullet_ping		
								if player_ped_id() = mike_ped()
									//inital duck from shot
									//@RJP Removing initial animation if player is Michael, prevents peds from looking around all confused, and not going into combat with Michael even
									//	   if he is standing right there until the anim finishes
//									task_play_anim(null,"missheistdocks2a@alert",sduckanim,normal_blend_in,NORMAL_BLEND_OUT)
									set_ped_seeing_range(mercs[i].id,16)
									SET_PED_HEARING_RANGE(mercs[i].id,16)							
									ireactionlength = get_random_int_in_range(30000, 60000)
									task_aim_gun_at_entity(null,mike_ped(),1000)
									task_go_to_coord_while_aiming_at_coord(null,get_entity_coords(mike_ped()),get_entity_coords(mike_ped()),pedmove_walk,false,1)
									println("ms_sit_bullet_ping: merc ",(i+1))		
									CPRINTLN( DEBUG_MISSION,"ms_sit_bullet_ping: merc ",(i+1) )
									//@RJP we should print a line making players aware that something happened and they should kill the mercs before alarm is raised
									if not mercs[i].bPrintHelp
										if not IS_THIS_HELP_MESSAGE_BEING_DISPLAYED( "DCKH_QUICK" )
											print_help("DCKH_QUICK")
										endif
										mercs[i].bPrintHelp = true
									endif
								else									
									switch mercs[i].currentaction 	
										case ga_aim_at_sniper
											MERC_react_to_sniper_fire(i)
											//@RJP we should print a line making players aware that something happened and they should kill the mercs before alarm is raised
											if not mercs[i].bPrintHelp
												if not IS_THIS_HELP_MESSAGE_BEING_DISPLAYED( "DCKH_QUICK" )
													print_help("DCKH_QUICK")
												endif
												mercs[i].bPrintHelp = true
											endif
										break
										case ga_find_cover
											//inital duck from shot
											mercs[i].currentaction = ga_aim_at_sniper
											MERC_react_to_sniper_fire(i)
											ireactionlength = get_random_int_in_range(5000, 6000)
											if not DOES_BLIP_EXIST(mercs[i].blip)
												mercs[i].blip = CREATE_BLIP_FOR_PED(mercs[i].id,true) 
											endif
											//@RJP we should print a line making players aware that something happened and they should kill the mercs before alarm is raised
											if not mercs[i].bPrintHelp
												if not IS_THIS_HELP_MESSAGE_BEING_DISPLAYED( "DCKH_QUICK" )
													print_help("DCKH_QUICK")
												endif
												mercs[i].bPrintHelp = true
											endif
//											if not bQuickHelp
//												print_help("DCKH_QUICK")
//												bQuickHelp = true
//											endif
										break								
									endswitch
								endif
							break
						endswitch
						close_sequence_task(seq)
						if isentityalive(mercs[i].id)
							println("BEFORE CLEAR TASKS tasking merc ",i+1," reaction. ped health: ",GET_ENTITY_HEALTH(mercs[i].id))
//							CLEAR_PED_TASKS_IMMEDIATELY(mercs[i].id)
							CLEAR_PED_TASKS( mercs[i].id )
							println("AFTER CLEAR TASKS  tasking merc ",i+1," reaction. ped health: ",GET_ENTITY_HEALTH(mercs[i].id))
							task_perform_sequence(mercs[i].id,seq)
							println("AFTER TASK_PERFORM_SEQUENCE tasking merc ",i+1," reaction. ped health: ",GET_ENTITY_HEALTH(mercs[i].id))
						endif
												
						mercs[i].currentsit = current_merc_rs
						if ireactionlength >= 0
							mercs[i].irevertactiontime = get_game_timer() + ireactionlength
						else
							mercs[i].irevertactiontime = -1
						endif
					endif
				else
					if mercs[i].irevertactiontime >=0 
						if get_game_timer() >= mercs[i].irevertactiontime
							if mercs[i].currentaction != ga_aim_at_sniper	
								if not is_ped_injured(mercs[i].id)
									TASK_MERC_TO_DEFAULT_PATROL_SEQUENCE( i )
									println("revert: merc ",(i+1))
								endif
							endif
							mercs[i].currentsit = ms_sit_none
							mercs[i].irevertactiontime = -1	
						endif
					endif
				endif
			endif
		endif
	endfor
endproc

//@RJP handles mercs in cars advancing
PROC MANAGE_CAR_MERC_COMBAT( INT i )
	
	//Exit early if the merc is dead
	IF NOT isentityalive( mercs[i].id )
		EXIT
	ENDIF
	
	SWITCH iCarMercDiag
		CASE 0
			IF IS_SAFE_TO_START_CONVERSATION()
				ADD_PED_FOR_DIALOGUE(convo_struct,8,mercs[i].id,"MERRYWEATHER5",true)
				CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_MWPOS5", CONV_PRIORITY_MEDIUM )
				iCarMercDiag++
			ENDIF
		BREAK
		CASE 1
			IF IS_SAFE_TO_START_CONVERSATION()
				ADD_PED_FOR_DIALOGUE(convo_struct,8,mercs[i].id,"MERRYWEATHER5",true)
				CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_MWGO5", CONV_PRIORITY_MEDIUM )
				iCarMercDiag++
			ENDIF
		BREAK
		CASE 2
			IF IS_SAFE_TO_START_CONVERSATION()
				CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_SHOTAT", CONV_PRIORITY_MEDIUM )
				iCarMercDiag++
			ENDIF
		BREAK
	ENDSWITCH
	
	
	//Exit early if ped is in a vehicle
	IF IS_PED_IN_ANY_VEHICLE( mercs[i].id )
		EXIT
	ENDIF
	
	IF NOT IS_PED_IN_COMBAT( mercs[i].id )
		TASK_COMBAT_PED( mercs[i].id, frank_ped() )
	ENDIF
	
	SWITCH mercs[i].iAdvanceState
		CASE 0
			IF IS_ENTITY_AT_COORD( mercs[i].id, mercs[i].vdef3, <<4,4,4>> )
				IF mercs[i].iAdvanceTimer > 0
					IF ( GET_GAME_TIMER()-mercs[i].iAdvanceTimer ) >  mercs[i].iAdvanceTime1
						SET_PED_SPHERE_DEFENSIVE_AREA( mercs[i].id, mercs[i].vdef2, 2, TRUE )
						mercs[i].iAdvanceTimer = 0
						mercs[i].iAdvanceState++
					ENDIF
				ELSE
					mercs[i].iAdvanceTimer = GET_GAME_TIMER()
				ENDIF
			ELSE
				SET_PED_SPHERE_DEFENSIVE_AREA( mercs[i].id, mercs[i].vdef3, 2, TRUE )
			ENDIF
		BREAK
		CASE 1
			//end state
		BREAK
	ENDSWITCH

ENDPROC

proc manage_car_crash_stuff()
	
	if isentityalive(vehs[mvf_merc1].id)
		if is_playback_going_on_for_vehicle(vehs[mvf_merc1].id)
			if not isentityalive(mercs[merc19_car1].id)
				stop_playback_recorded_vehicle(vehs[mvf_merc1].id)
				if isentityalive(mercs[merc20_car1].id)
					set_ped_sphere_defensive_area(mercs[merc20_car1].id,<< -165.7794, -2421.9829, 5.6480 >>,2)
				endif
				if isentityalive(mercs[merc22_car1].id)
					set_ped_sphere_defensive_area(mercs[merc22_car1].id,<< -163.9438, -2416.1736, 5.4341 >>,2)
				endif
			else
				if is_vehicle_tyre_burst(vehs[mvf_merc1].id,sc_wheel_car_front_left)
				or is_vehicle_tyre_burst(vehs[mvf_merc1].id,sc_wheel_car_rear_left)
					stop_playback_recorded_vehicle(vehs[mvf_merc1].id)		
					open_sequence_task(seq)
						task_vehicle_temp_action(null,vehs[mvf_merc1].id,tempact_swerveleft,4000)
					close_sequence_task(seq)
					task_perform_sequence(mercs[merc19_car1].id,seq)
					clear_sequence_task(seq)
					if isentityalive(mercs[merc20_car1].id)
					set_ped_sphere_defensive_area(mercs[merc20_car1].id,<< -165.7794, -2421.9829, 5.6480 >>,2)
					endif
					if isentityalive(mercs[merc22_car1].id)
						set_ped_sphere_defensive_area(mercs[merc22_car1].id,<< -163.9438, -2416.1736, 5.4341 >>,2)
					endif
				elif is_vehicle_tyre_burst(vehs[mvf_merc1].id,sc_wheel_car_front_right)
				or is_vehicle_tyre_burst(vehs[mvf_merc1].id,sc_wheel_car_rear_right)
					stop_playback_recorded_vehicle(vehs[mvf_merc1].id)
					open_sequence_task(seq)
						task_vehicle_temp_action(null,vehs[mvf_merc1].id,tempact_swerveright,4000)
					close_sequence_task(seq)
					task_perform_sequence(mercs[merc19_car1].id,seq)
					clear_sequence_task(seq)
					if isentityalive(mercs[merc20_car1].id)
						set_ped_sphere_defensive_area(mercs[merc20_car1].id,<< -165.7794, -2421.9829, 5.6480 >>,2)
					endif
					if isentityalive(mercs[merc22_car1].id)
						set_ped_sphere_defensive_area(mercs[merc22_car1].id,<< -163.9438, -2416.1736, 5.4341 >>,2)
					endif
				endif
			endif						
//		else
//			if does_entity_exist(vehs[mvf_merc1].id)
//				if has_entity_collided_with_anything(vehs[mvf_merc1].id)
//				and not is_vehicle_stopped(vehs[mvf_merc1].id)			
//					speed = get_entity_speed(vehs[mvf_merc1].id)
//					if speed > 5
//						add_explosion(get_entity_coords(vehs[mvf_merc1].id),exp_tag_car)
//					endif
//				endif
//			endif
		endif
	endif	
	if isentityalive(vehs[mvf_merc2].id)
		if is_playback_going_on_for_vehicle(vehs[mvf_merc2].id)
			if not isentityalive(mercs[merc23_car2].id)
				stop_playback_recorded_vehicle(vehs[mvf_merc2].id)
				if isentityalive(mercs[merc25_car2].id)
					set_ped_sphere_defensive_area(mercs[merc25_car2].id,<< -112.3173, -2426.4836, 5.3646 >>,2)
				endif
			else
				if is_vehicle_tyre_burst(vehs[mvf_merc2].id,sc_wheel_car_front_left)
				or is_vehicle_tyre_burst(vehs[mvf_merc2].id,sc_wheel_car_rear_left)
					stop_playback_recorded_vehicle(vehs[mvf_merc2].id)		
					open_sequence_task(seq)
						task_vehicle_temp_action(null,vehs[mvf_merc2].id,tempact_swerveleft,4000)
					close_sequence_task(seq)
					task_perform_sequence(mercs[merc23_car2].id,seq)
					clear_sequence_task(seq)
					if isentityalive(mercs[merc23_car2].id)
						set_ped_sphere_defensive_area(mercs[merc23_car2].id,<< -105.7819, -2427.7837, 5.8586 >>,2)
					endif
					if isentityalive(mercs[merc25_car2].id)
						set_ped_sphere_defensive_area(mercs[merc25_car2].id,<< -112.3173, -2426.4836, 5.3646 >>,2)
					endif				
				elif is_vehicle_tyre_burst(vehs[mvf_merc2].id,sc_wheel_car_front_right)
				or is_vehicle_tyre_burst(vehs[mvf_merc2].id,sc_wheel_car_rear_right)
					stop_playback_recorded_vehicle(vehs[mvf_merc2].id)
					open_sequence_task(seq)
						task_vehicle_temp_action(null,vehs[mvf_merc2].id,tempact_swerveright,4000)
					close_sequence_task(seq)
					task_perform_sequence(mercs[merc23_car2].id,seq)
					clear_sequence_task(seq)					
					if isentityalive(mercs[merc23_car2].id)
						set_ped_sphere_defensive_area(mercs[merc23_car2].id,<< -105.7819, -2427.7837, 5.8586 >>,2)
					endif
					if isentityalive(mercs[merc25_car2].id)
						set_ped_sphere_defensive_area(mercs[merc25_car2].id,<< -112.3173, -2426.4836, 5.3646 >>,2)
					endif
				endif
			endif						
//		else
//			if does_entity_exist(vehs[mvf_merc2].id)
//				if has_entity_collided_with_anything(vehs[mvf_merc2].id)
//				and not is_vehicle_stopped(vehs[mvf_merc2].id)
//					speed = get_entity_speed(vehs[mvf_merc2].id)
//					if speed > 5 
//						add_explosion(get_entity_coords(vehs[mvf_merc2].id),exp_tag_car)
//					endif
//				endif
//			endif
		endif
	endif
//	//****temp******
//	if does_entity_exist(vehs[mvf_merc1].id)
//		if has_entity_been_damaged_by_entity(vehs[mvf_merc1].id,frank_ped())
//			set_vehicle_tyre_burst(vehs[mvf_merc1].id,sc_wheel_car_front_right)
//		endif
//	endif
//	if does_entity_exist(vehs[mvf_merc2].id)
//		if has_entity_been_damaged_by_entity(vehs[mvf_merc2].id,frank_ped())
//			set_vehicle_tyre_burst(vehs[mvf_merc2].id,sc_wheel_car_front_right)
//		endif
//	endif
	//*********
endproc

//@RJP blip enemies on the boat when sneaking
proc MICHAEL_SNEAKING_ENEMY_BLIP_MANAGER()
	INT i
	BOOL bBlipMerc[MMF_NUM_OF_MERCS]
	
	//@RJP blip enemies who have been alerted in some way
	FOR i = 2 TO 17
		IF mercs[i].bPrintHelp
			bBlipMerc[i] = TRUE
		ENDIF
	ENDFOR
	
	IF IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_BOW_UPPER_AREA] )
		FOR i = 2 TO 3
			bBlipMerc[i] = TRUE
		ENDFOR
	ENDIF
	
	IF IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_BOW_CONTAINERS_AREA] )
		FOR i = 2 TO 6
			bBlipMerc[i] = TRUE
		ENDFOR
	ENDIF
	
	IF IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_FIRST_MAST_AREA] )
		FOR i = 2 TO 6
			bBlipMerc[i] = TRUE
		ENDFOR
	ENDIF
	
	IF IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_MID_CONTAINERS_1_AREA] )
		FOR i = 3 TO 8
			bBlipMerc[i] = TRUE
		ENDFOR
	ENDIF
	
	IF IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_MID_CONTAINERS_2_AREA] )
		FOR i = 3 TO 10
			bBlipMerc[i] = TRUE
		ENDFOR
	ENDIF
	
	IF IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_MID_CONTAINERS_3_AREA] )
		FOR i = 8 TO 11
			bBlipMerc[i] = TRUE
		ENDFOR
		bBlipMerc[13] = TRUE
		bBlipMerc[14] = TRUE
		bBlipMerc[15] = TRUE
	ENDIF
	
	IF IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_SECOND_MAST_AREA] )
		FOR i = 9 TO 11
			bBlipMerc[i] = TRUE
		ENDFOR
		bBlipMerc[13] = TRUE
		bBlipMerc[14] = TRUE
		bBlipMerc[15] = TRUE
	ENDIF
	
	IF IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_STERN_CONTAINERS_AREA] )
		FOR i = 9 TO 16
			bBlipMerc[i] = TRUE
		ENDFOR
	ENDIF
	
	IF IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_STERN_AREA] )
		FOR i = 12 TO 17
			bBlipMerc[i] = TRUE
		ENDFOR
	ENDIF
	
	FOR i = merc3_C4_2 TO merc18_c4_4
		IF bBlipMerc[i]
			IF isentityalive(mercs[i].id)
				IF NOT DOES_BLIP_EXIST( mercs[i].blip )
					mercs[i].blip = CREATE_BLIP_FOR_PED( mercs[i].id, TRUE )
				ELSE
					IF PLAYER_PED_ID() = mike_ped()
						SET_BLIP_SHOW_CONE( mercs[i].blip, TRUE )
					ELSE
						SET_BLIP_SHOW_CONE( mercs[i].blip, FALSE )
					ENDIF
				ENDIF
			ELSE
				safe_remove_blip( mercs[i].blip )
			ENDIF
		ELSE
			safe_remove_blip( mercs[i].blip )
		ENDIF
	ENDFOR
	
endproc

PROC RELEASE_ZOMBIE_TO_COMBAT( MMF_MISSION_MERC_FLAGS merc )
	
	IF NOT isentityalive( mercs[merc].id )
		EXIT
	ENDIF
	
	IF (GET_PED_COMBAT_MOVEMENT( mercs[merc].id ) = CM_DEFENSIVE)
		set_ped_combat_movement( mercs[merc].id, CM_WILLADVANCE )
		SET_PED_COMBAT_ATTRIBUTES( mercs[merc].id, CA_CAN_CHARGE, TRUE )
		REMOVE_PED_DEFENSIVE_AREA( mercs[merc].id )
	ENDIF
	
	IF NOT IS_PED_IN_COMBAT( mercs[merc].id )
		IF isentityalive( mike_ped() )
			TASK_COMBAT_PED( mercs[merc].id, mike_ped() )
		ENDIF
	ENDIF
	
ENDPROC

PROC MANAGE_ALARM_EXTRA_GUARDS()
	
	INT i
	
	IF NOT c4[C4_1_first_mast].bPlanted
	AND IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_BOW_UPPER_AREA] )
		IF iBomb2Spawned < 2
			for i = enum_to_int(merc1_start) to enum_to_int(merc18_c4_4)
				if not DOES_ENTITY_EXIST(mercs[i].id)
					SWITCH iBomb2Spawned
						CASE 0
							IF createzombiemercenary(int_to_enum(mmf_mission_merc_flags,i), <<-129.0482, -2361.1956, 8.3191>>, 356.6929, <<-128.6367, -2357.4873, 8.3191>> )
								mercsBombArea2[iBomb2Spawned] = int_to_enum(mmf_mission_merc_flags,i)
								iBomb2Spawned ++ 
							ENDIF
						BREAK
						CASE 1
							IF createzombiemercenary(int_to_enum(mmf_mission_merc_flags,i), <<-128.9855, -2362.3123, 8.3191>>, 189.3570, <<-128.6614, -2373.9871, 8.3191>> )
								mercsBombArea2[iBomb2Spawned] = int_to_enum(mmf_mission_merc_flags,i)
								iBomb2Spawned ++ 
							ENDIF
						BREAK
					ENDSWITCH
				endif
			endfor
		ENDIF
	ELIF IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_FIRST_MAST_AREA] )
		REPEAT iBomb2Spawned i
			RELEASE_ZOMBIE_TO_COMBAT( mercsBombArea2[int_to_enum(mmf_mission_merc_flags,i)] )
		ENDREPEAT
	ENDIF
	
	IF NOT c4[c4_2_second_mast].bplanted
	AND NOT IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_SECOND_MAST_AREA] )
	AND NOT IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_STERN_CONTAINERS_AREA] )
	AND NOT IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_STERN_AREA] )
		IF iBomb3Spawned < 3
			for i = enum_to_int(merc1_start) to enum_to_int(merc18_c4_4)
				if not DOES_ENTITY_EXIST(mercs[i].id)
					SWITCH iBomb3Spawned
						CASE 0
							IF createzombiemercenary(int_to_enum(mmf_mission_merc_flags,i), <<-189.0334, -2360.9258, 8.3191>>, 7.7789, <<-188.0162, -2373.9033, 8.3191>> )
								mercsBombArea3[iBomb3Spawned] = int_to_enum(mmf_mission_merc_flags,i)
								iBomb3Spawned ++ 
							ENDIF
						BREAK
						CASE 1
							IF createzombiemercenary(int_to_enum(mmf_mission_merc_flags,i), <<-188.9252, -2362.5430, 8.3191>>, 166.6817, <<-188.0314, -2368.5366, 8.3191>> )
								mercsBombArea3[iBomb3Spawned] = int_to_enum(mmf_mission_merc_flags,i)
								iBomb3Spawned ++ 
							ENDIF
						BREAK
						CASE 2
							IF createzombiemercenary(int_to_enum(mmf_mission_merc_flags,i), <<-190.5101, -2362.8284, 8.3191>>, 150.3902, <<-178.0869, -2355.4004, 8.3191>> )
								mercsBombArea3[iBomb3Spawned] = int_to_enum(mmf_mission_merc_flags,i)
								iBomb3Spawned ++ 
							ENDIF
						BREAK
					ENDSWITCH
				endif
			endfor
		ENDIF
	ELSE
		REPEAT iBomb3Spawned i
			RELEASE_ZOMBIE_TO_COMBAT( mercsBombArea3[int_to_enum(mmf_mission_merc_flags,i)] )
		ENDREPEAT
	ENDIF
	
	IF NOT IS_ENTITY_IN_TRIGGER_BOX( mike_ped(), sTriggerBox[TB_BOAT_STERN_AREA] )
		IF iBomb4Spawned < 3
			for i = enum_to_int(merc1_start) to enum_to_int(merc18_c4_4)
				if not DOES_ENTITY_EXIST(mercs[i].id)
					SWITCH iBomb4Spawned
						CASE 0
							IF createzombiemercenary(int_to_enum(mmf_mission_merc_flags,i), <<-221.0331, -2376.2307, 8.3192>>, 236.8918, <<-209.0507, -2376.4771, 8.3192>> )
								mercsBombArea4[iBomb4Spawned] = int_to_enum(mmf_mission_merc_flags,i)
								iBomb4Spawned ++ 
							ENDIF
						BREAK
						CASE 1
							IF createzombiemercenary(int_to_enum(mmf_mission_merc_flags,i), <<-222.0928, -2375.8884, 12.3329>>, 188.8545, <<-221.2283, -2377.7432, 12.3329>> )
								mercsBombArea4[iBomb4Spawned] = int_to_enum(mmf_mission_merc_flags,i)
								iBomb4Spawned ++ 
							ENDIF
						BREAK
						CASE 2
							IF createzombiemercenary(int_to_enum(mmf_mission_merc_flags,i), <<-221.7503, -2355.7732, 8.3192>>, 356.4612, <<-207.1320, -2355.4556, 8.3191>> )
								mercsBombArea4[iBomb4Spawned] = int_to_enum(mmf_mission_merc_flags,i)
								iBomb4Spawned ++ 
							ENDIF
						BREAK
					ENDSWITCH
				endif
			endfor
		ENDIF
	ELSE
		REPEAT iBomb4Spawned i
			RELEASE_ZOMBIE_TO_COMBAT( mercsBombArea4[int_to_enum(mmf_mission_merc_flags,i)] )
		ENDREPEAT
	ENDIF
	
ENDPROC

PROC MANAGE_PLAYER_SWITCH_LOAD_SCENES()
	
	IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
		CPRINTLN( DEBUG_MISSION, "STARTING LOAD SCENE OF FRANKLIN ON VANTAGE POINT" )
		NEW_LOAD_SCENE_START_SPHERE(<< -143.8203, -2488.2612, 43.4412 >>, 18)
	ENDIF
	
ENDPROC

proc manage_stealth_killed_merc(merc_struct &merc)

	if does_entity_exist(merc.id) and not is_entity_dead(merc.id)
		if is_synchronized_scene_running(iStealthKillSyncScene)
			if is_entity_playing_anim(merc.id, "missheistdocks2a","stabbing_guard_guard", anim_synced_scene)
				if get_synchronized_scene_phase(iStealthKillSyncScene) > 0.99
					detach_synchronized_scene(iStealthKillSyncScene)
					set_entity_health(merc.id, 0)
					bkilledbybuddy = true
					istealthkill = 0
					println(get_this_script_name(), ": Killing off merc playing stealth kill sync scene anim.")
				endif
			endif
		endif
	endif

endproc

proc manage_bomb_stages()
//════════════════════════════════════════╡ stage flow stuff ╞══════════════════════════════════════════
	
	//--------------------------------------------blips-----------------------------------------------	
		if player_ped_id() = frank_ped()
			safe_remove_blip(c4[c4_0_front_of_ship].blip)
			safe_remove_blip(c4[c4_1_first_mast].blip)
			safe_remove_blip(c4[c4_2_second_mast].blip)
		else
			//@RJP only blip one bomb location at a time
			if mission_stage = ENUM_TO_INT(msf_2_first_bomb)
				if not c4[c4_0_front_of_ship].bplanted
					if not does_blip_exist(c4[c4_0_front_of_ship].blip)
						c4[c4_0_front_of_ship].blip = create_blip_for_coord(c4[c4_0_front_of_ship].exppos)				
					endif
				else
					safe_remove_blip(c4[c4_0_front_of_ship].blip)
				endif
			else
				safe_remove_blip(c4[c4_0_front_of_ship].blip)
			endif
			
			if mission_stage = ENUM_TO_INT(msf_3_second_bomb)
				if not c4[c4_1_first_mast].bplanted
					if not does_blip_exist(c4[c4_1_first_mast].blip)
						c4[c4_1_first_mast].blip = create_blip_for_coord(c4[c4_1_first_mast].exppos)				
					endif
				else
					safe_remove_blip(c4[c4_1_first_mast].blip)
				endif
			else
				safe_remove_blip(c4[c4_1_first_mast].blip)
			endif
			
			if mission_stage = ENUM_TO_INT(msf_4_third_bomb)
				if not c4[c4_2_second_mast].bplanted
					if not does_blip_exist(c4[c4_2_second_mast].blip)
						c4[c4_2_second_mast].blip = create_blip_for_coord(c4[c4_2_second_mast].exppos)				
					endif
				else
					safe_remove_blip(c4[c4_2_second_mast].blip)
				endif
			else
				safe_remove_blip(c4[c4_2_second_mast].blip)
			endif
		endif
		
//══════════════════════════════════════════════════════════════════════════════════════════════════════
	
	//check if alarm should be raised + delay on setting off alarm		
		int i
		int timer = 5000
		if not balarmtriggered
			for i = enum_to_int(merc3_C4_2) to enum_to_int(merc18_c4_4)
				if isentityalive(mercs[i].id)
					if is_ped_in_combat(mercs[i].id,mike_ped())		
						mercs[i].bincombat = true
						timer = 5000
					elif mercs[i].currentaction = ga_aim_at_sniper
						mercs[i].bincombat = true
						timer = 15000
					//@RJP check alarm if mercs have spotted a dead merc
					elif mercs[i].bSpottedDead
						mercs[i].bincombat = true
						timer = 15000
					else
						mercs[i].bincombat = false
					endif
				else
					mercs[i].bincombat = false
				endif
			endfor	
		
			bincombat = false
			for i = enum_to_int(merc3_C4_2) to enum_to_int(merc18_c4_4)
				if mercs[i].bincombat
				and isentityalive(mercs[i].id)
					bincombat = true
				endif
			endfor	
			
			if bincombat 
				if not binitialisedelayalr
					icombattimer = get_game_timer() 
					binitialisedelayalr = true
				else
					if get_game_timer() - icombattimer > timer
						IF PLAYER_PED_ID() = mike_ped()
							CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_ALRF_NS", CONV_PRIORITY_MEDIUM )
						ELSE
							CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_ALRM_NS", CONV_PRIORITY_MEDIUM )
						ENDIF
						INFORM_MISSION_STATS_OF_INCREMENT( DH2A_NO_ALARMS )
						CPRINTLN( DEBUG_MISSION, "DH2A_NO_ALARMS incremented to 1" )
						balarmtriggered = true
					endif
				endif
			else
				binitialisedelayalr = false
			endif
		
		endif
		
//		if not b_early_alarm_streamed
//			if prepare_music_event("dh2a_early_alarm")
//				b_early_alarm_streamed = true
//			endif
//		endif	
		
	//all combat michael if alarm set off.
		if balarmtriggered
		
			if balarminitialised = false				
				balarminitialised = true
				trigger_music_event("dh2a_early_alarm")
				if is_audio_scene_active("dh2a_plant_bombs_scene")
					stop_audio_scene("dh2a_plant_bombs_scene")
				endif
				if is_audio_scene_active("dh2a_sniping_scene")
					stop_audio_scene("dh2a_sniping_scene")
				endif
				if player_ped_id() = frank_ped()
					start_audio_scene("dh2a_shootout_sniping_scene")
				elif player_ped_id() = mike_ped()
					start_audio_scene("dh2a_shootout_scene")
				endif				
				
				create_conversation(convo_struct,"d2aaud","dh2a_alrm",conv_priority_medium)
	// spawn all other bomb areas that the ped hasn't reached yet and set all alarm settings on.
				bhotswap = true
				hotswap_menu(false,false,true)		
				
				if player_ped_id() = mike_ped()
					clear_ped_tasks(frank_ped())
				else
					clear_ped_tasks(mike_ped())
				endif
				
				prep_stop_cutscene(true)
				destroy_all_cams()				
			endif
			
			MERC_manage_combat()
							
	//buddy support
			if player_ped_id() = frank_ped()
				MIKE_manage_combat()
			else
				FRANK_sniper_support()
				set_ped_combat_attributes(mike_ped(), ca_requires_los_to_shoot, false)
				safe_remove_blip(peds[mpf_mike].blip)
			endif
			
			MANAGE_ALARM_EXTRA_GUARDS()

		else	//alarm not triggered
			
			//Disable threat engage cover bonus on Michael as per B*1417785
			IF PLAYER_PED_ID() = mike_ped()
				SET_PED_RESET_FLAG(mike_ped(), PRF_IgnoreThreatEngagePlayerCoverBonus, TRUE)
			ENDIF
			
			//@RJP Controls blipping of enemies while Michael is sneaking
			MICHAEL_SNEAKING_ENEMY_BLIP_MANAGER()
		
			//@RJP if player is Franklin, give mercs defensive areas that put them on the side of the ship, otherwise remove defensive areas
			if isentityalive(frank_ped())			
				for i = enum_to_int(merc3_C4_2) to enum_to_int(merc18_c4_4)
					if isentityalive(mercs[i].id)
						if PLAYER_PED_ID() = frank_ped()
							set_ped_sphere_defensive_area(mercs[i].id,mercs[i].vdeffrank,2)
						else
							REMOVE_PED_DEFENSIVE_AREA( mercs[i].id )
						endif
					endif
				endfor
			endif
			
			//loop patrol waypoint if not in combat or alarm triggered		
//			if isentityalive(mercs[merc7_c4_2].id)
//			and get_is_waypoint_recording_loaded("docksheist2a02")
//				if not is_waypoint_playback_going_on_for_ped(mercs[merc7_c4_2].id)
//					if not is_ped_in_combat(mercs[merc7_c4_2].id)	
//					and mercs[merc7_c4_2].iRevertActionTime = -1
//					and mercs[merc7_c4_2].currentsit = ms_sit_none	
//						if IS_ENTITY_AT_COORD(mercs[merc7_C4_2].id,<<-163.53667, -2375.51953, 8.31907>>,<<2,2,2>>)
//							task_follow_waypoint_recording(mercs[merc7_c4_2].id,"docksheist2a02",0)
//						else
//							task_follow_waypoint_recording(mercs[merc7_c4_2].id,"docksheist2a02",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
//						endif
//					endif
//				endif
//			endif
		
			// check if the player is looking to stealth kill someone whilst franklin or michael. 
			if not scamdetails.brun		
				if player_ped_id() = mike_ped()
					FRANK_manage_ai_stealth()
					manage_stealth_killed_merc(mercs[merc4_c4_2])
					manage_stealth_killed_merc(mercs[merc13_c4_3])
				else
					IF IS_ANY_MERC_COMBATTING_MIKE()
						MIKE_stealth_combat()
					ELSE
						MIKE_manage_ai_stealth()
					ENDIF
				endif
			endif
		endif // end of alarmed triggered check		
		
endproc

FUNC STRING GET_MERRYWEATHER_VOICE_NAME(INT merc)
	STRING sRet
	SWITCH merc%4
		CASE 0	sRet = "MERRYWEATHER1"	BREAK
		CASE 1	sRet = "MERRYWEATHER2"	BREAK
		CASE 2	sRet = "MERRYWEATHER3"	BREAK
		CASE 3	sRet = "MERRYWEATHER4"	BREAK
	ENDSWITCH
	
	RETURN sRet
ENDFUNC

FUNC STRING GET_PATROL_LINE(INT merc)
	STRING sRet
	SWITCH merc%4
		CASE 0	sRet = "DH2A_MERC3"		BREAK
		CASE 1	sRet = "DH2A_MERC2"		BREAK
		CASE 2	sRet = "DH2A_MWPAT3"	BREAK
		CASE 3	sRet = "DH2A_MERC4"		BREAK
	ENDSWITCH
	
	RETURN sRet
ENDFUNC

FUNC STRING GET_HEAR_MIKE_LINE(INT merc)
	STRING sRet
	SWITCH merc%4
		CASE 0	sRet = "DH2A_MWHEAR1"	BREAK
		CASE 1	sRet = "DH2A_MWHEAR2"	BREAK
		CASE 2	sRet = "DH2A_MWHEAR3"	BREAK
		CASE 3	sRet = "DH2A_MWHEAR4"	BREAK
	ENDSWITCH
	
	RETURN sRet
ENDFUNC

FUNC STRING GET_FIND_BODY_LINE(INT merc)
	STRING sRet
	SWITCH merc%4
		CASE 0	sRet = "DH2A_MWFIND1"	BREAK
		CASE 1	sRet = "DH2A_MWFIND2"	BREAK
		CASE 2	sRet = "DH2A_MWFIND3"	BREAK
		CASE 3	sRet = "DH2A_MWFIND4"	BREAK
	ENDSWITCH
	
	RETURN sRet
ENDFUNC

FUNC STRING GET_SEE_MIKE_LINE(INT merc)
	STRING sRet
	SWITCH merc%4
		CASE 0	sRet = "DH2A_MWSEE1"	BREAK
		CASE 1	sRet = "DH2A_MWSEE2"	BREAK
		CASE 2	sRet = "DH2A_MWSEE3"	BREAK
		CASE 3	sRet = "DH2A_MWSEE4"	BREAK
	ENDSWITCH
	
	RETURN sRet
ENDFUNC

FUNC STRING GET_RAISE_ALARM_LINE(INT merc)
	STRING sRet
	SWITCH merc%4
		CASE 0	sRet = "DH2A_MERC1"		BREAK
		CASE 1	sRet = "DH2A_MERC"		BREAK
		CASE 2	sRet = "DH2A_ARL3"		BREAK
		CASE 3	sRet = "DH2A_MWALR4"	BREAK
	ENDSWITCH
	
	RETURN sRet
ENDFUNC

//Line for merryweather guard raising alarm
proc AUDIO_dialogue_merc_combat()
	if bRaisedAlarm
		EXIT
	endif
	
	int merc
	for merc = enum_to_int(merc1_start) to enum_to_int(merc18_c4_4)
		if isEntityalive(mercs[merc].id)
			if IS_SAFE_TO_START_CONVERSATION()
				if mercs[merc].currentsit <> MS_SIT_NONE
					ADD_PED_FOR_DIALOGUE( convo_struct, (merc%4+4), mercs[merc].id, GET_MERRYWEATHER_VOICE_NAME(merc) )
					CREATE_CONVERSATION( convo_struct, "D2AAUD", GET_RAISE_ALARM_LINE(merc), CONV_PRIORITY_MEDIUM )
					bRaisedAlarm = TRUE
					EXIT
				endif
			endif
		endif
	endfor
endproc

//Ambient patrol lines and reactions to seeing or hearing player/spotting dead body
proc AUDIO_dialogue_merc_stealth()
	int merc
	bool bAMercCurrentlyAlerted = FALSE
	for merc = enum_to_int(merc1_start) to enum_to_int(merc18_c4_4)
		if isEntityalive(mercs[merc].id)
			if get_game_timer() > mercs[merc].iDialogueTimer
			and mercs[merc].currentsit = MS_SIT_NONE
				ADD_PED_FOR_DIALOGUE( convo_struct, (merc%4+4), mercs[merc].id, GET_MERRYWEATHER_VOICE_NAME(merc) )
				CREATE_CONVERSATION( convo_struct, "D2AAUD", GET_PATROL_LINE(merc), CONV_PRIORITY_LOW, DO_NOT_DISPLAY_SUBTITLES )
				mercs[merc].iDialogueTimer = get_game_Timer() + GET_RANDOM_INT_IN_RANGE( 15000, 20000 )
			elif mercs[merc].currentsit <> MS_SIT_NONE
				bAMercCurrentlyAlerted = TRUE
				if IS_SAFE_TO_START_CONVERSATION()
				and not bAlertedMerc
					if not mercs[merc].bAlertSpeech
						if not is_ambient_speech_playing(mercs[merc].id)
							ADD_PED_FOR_DIALOGUE( convo_struct, (merc%4+4), mercs[merc].id, GET_MERRYWEATHER_VOICE_NAME(merc) )
							mercs[merc].bAlertSpeech = TRUE
							SWITCH mercs[merc].currentsit
								CASE MS_sit_MIKE_SEEN
									CREATE_CONVERSATION( convo_struct, "D2AAUD", GET_SEE_MIKE_LINE(merc), CONV_PRIORITY_MEDIUM )
									bAlertedMerc = TRUE
//									play_ped_ambient_speech_with_voice(mercs[merc].id, GET_SEE_MIKE_LINE(merc), GET_MERRYWEATHER_VOICE_NAME(merc), SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
								BREAK
								CASE MS_SIT_SHOT_HEARD
								CASE ms_sit_bullet_ping
									CREATE_CONVERSATION( convo_struct, "D2AAUD", GET_HEAR_MIKE_LINE(merc), CONV_PRIORITY_MEDIUM )
									bAlertedMerc = TRUE
//									play_ped_ambient_speech_with_voice(mercs[merc].id, GET_HEAR_MIKE_LINE(merc), GET_MERRYWEATHER_VOICE_NAME(merc), SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
								BREAK
								CASE MS_SIT_DEAD_BODY
									CREATE_CONVERSATION( convo_struct, "D2AAUD", GET_FIND_BODY_LINE(merc), CONV_PRIORITY_MEDIUM )
									bAlertedMerc = TRUE
//									play_ped_ambient_speech_with_voice(mercs[merc].id, GET_FIND_BODY_LINE(merc), GET_MERRYWEATHER_VOICE_NAME(merc), SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
								BREAK
							ENDSWITCH
						endif
					endif
				endif
			endif
		endif
	endfor
	
	//Reset bAlertedMerc if there is no one alerted
	if not bAMercCurrentlyAlerted
		if bAlertedMerc
			bAlertedMerc = FALSE
		endif
	endif
endproc
proc AUDIO_dialogue_killed_merc()
//dialogue for killing mercs
	if not balarmtriggered
	and not bcsalarmtriggered
	and mission_stage > enum_to_int(msf_1_snipe_gaurds)	
	and mission_stage < enum_to_int(msf_6_way_out)
	and mission_substage > stage_entry
	
		//----------------second bomb merc-----------------
		if not isentityalive(mercs[merc3_c4_2].id)
		and mercs[merc3_c4_2].balive
			switch idialoguekillswitch 
				case 0
					idialoguedelay = get_game_timer()
					idialoguekillswitch++
				break
				case 1
					if get_game_timer() - idialoguedelay >= 500
						if IS_SAFE_TO_START_CONVERSATION()	
						and not is_message_being_displayed()
							if player_ped_id() = mike_ped()
//								create_conversation(convo_struct,"d2aaud","dh2a_1a",conv_priority_low)
							else
								create_conversation(convo_struct,"d2aaud","dh2a_2a",conv_priority_low)
							endif		
						endif
						idialoguekillswitch = 0
						mercs[merc3_c4_2].balive = false
					endif
				break
			endswitch							
		endif
			
		if not isentityalive(mercs[merc4_c4_2].id)			
		and mercs[merc4_c4_2].balive
			switch idialoguekillswitch
				case 0
					idialoguedelay = get_game_timer()
					idialoguekillswitch++
				break
				case 1	
					if get_game_timer() - idialoguedelay >= 500
						if IS_SAFE_TO_START_CONVERSATION()
						and not is_message_being_displayed()
						if player_ped_id() = mike_ped()
							if bKilledByBuddy
								create_conversation(convo_struct,"d2aaud","dh2a_1c",conv_priority_low)
								bKilledByBuddy = FALSE
							endif
						else
							create_conversation(convo_struct,"d2aaud","dh2a_2c",conv_priority_low)
						endif
						endif
						mercs[merc4_c4_2].balive = false
						idialoguekillswitch = 0
					endif
				break
			endswitch
		endif	
			
		//@RJP merc 7 patrols with merc 5, including both here
		if not isentityalive(mercs[merc5_c4_2].id)
//		and not isentityalive(mercs[MERC6_C4_2].id)
		and mercs[merc5_c4_2].balive
//		and mercs[MERC6_C4_2].balive
			switch idialoguekillswitch
			case 0
				idialoguedelay = get_game_timer()
				idialoguekillswitch++
			break
			case 1
				if get_game_timer() - idialoguedelay >= 500
					if IS_SAFE_TO_START_CONVERSATION()
						if player_ped_id() = mike_ped()
//							create_conversation(convo_struct,"d2aaud","dh2a_1b",conv_priority_low)
						else
							create_conversation(convo_struct,"d2aaud","dh2a_2b",conv_priority_low)
						endif
					endif
					mercs[merc5_c4_2].balive = false
//					mercs[MERC6_C4_2].balive = false
					idialoguekillswitch = 0
				endif
			break
			endswitch				
		endif
		
		//----------------------third bomb mercs----------------------------
		//@RJP merc 9 is now patrolling around here, no longer merc7
		if not isentityalive(mercs[merc9_c4_3].id)
		and mercs[merc9_c4_3].balive		
			switch idialoguekillswitch
				case 0
					idialoguedelay = get_game_timer()
					idialoguekillswitch++
				break
				case 1
					if get_game_timer() - idialoguedelay >= 500
						if IS_SAFE_TO_START_CONVERSATION()
						and not is_message_being_displayed()
							if player_ped_id() = frank_ped()
								create_conversation(convo_struct,"d2aaud","dh2a_3a",conv_priority_low)	
							endif
						endif
						mercs[merc9_c4_3].balive = false
						idialoguekillswitch = 0
					endif
				break	
			endswitch
		endif
			
//		if not isentityalive(mercs[merc10_c4_3].id)
//		and not isentityalive(mercs[merc11_c4_3].id)			
//		and mercs[merc10_c4_3].balive			
//		and mercs[merc11_c4_3].balive
//			switch idialoguekillswitch
//				case 0
//					idialoguedelay = get_game_timer()
//					idialoguekillswitch++
//				break
//				case 1
//					if get_game_timer() - idialoguedelay >= 500
//						if player_ped_id() = frank_ped()
//							if not is_any_conversation_ongoing_or_queued()
//							and not is_message_being_displayed()
//								create_conversation(convo_struct,"d2aaud","dh2a_3b",conv_priority_low)
//							endif
//						endif
//						mercs[merc10_c4_3].balive = false
//						mercs[merc11_c4_3].balive = false
//						idialoguekillswitch = 0
//					endif
//				break
//			endswitch	
//		endif
		if  not isentityalive(mercs[merc11_c4_3].id)			
		and mercs[merc11_c4_3].balive
			switch idialoguekillswitch
				case 0
					idialoguedelay = get_game_timer()
					idialoguekillswitch++
				break
				case 1
					if get_game_timer() - idialoguedelay >= 500
						if player_ped_id() = frank_ped()
							if IS_SAFE_TO_START_CONVERSATION()
							and not is_message_being_displayed()
								create_conversation(convo_struct,"d2aaud","dh2a_3b1",conv_priority_low)
							endif
						endif
						mercs[merc11_c4_3].balive = false
						idialoguekillswitch = 0
					endif
				break	
			endswitch	
		endif
//		if not isentityalive(mercs[merc10_c4_3].id)			
//		and mercs[merc10_c4_3].balive
//			switch idialoguekillswitch
//				case 0
//					idialoguedelay = get_game_timer()
//					idialoguekillswitch++
//				break
//				case 1
//					if get_game_timer() - idialoguedelay >= 500
//						if player_ped_id() = frank_ped()
//							if not is_any_conversation_ongoing_or_queued()	
//							and not is_message_being_displayed()
//								create_conversation(convo_struct,"d2aaud","dh2a_3b2",conv_priority_low)
//							endif	
//						endif
//						mercs[merc10_c4_3].balive = false
//						idialoguekillswitch = 0
//					endif
//				break	
//			endswitch	
//		endif
			
		//@RJP Franklin should not tell Michael to move until 14 and 15 are eliminated
		if not isentityalive(mercs[merc14_c4_3].id)
//		and not isentityalive(mercs[MERC15_C4_3].id)
		and mercs[merc14_c4_3].balive
//		and mercs[MERC15_C4_3].balive
			switch idialoguekillswitch
				case 0
					idialoguedelay = get_game_timer()
					idialoguekillswitch++
				break
				case 1
					if get_game_timer() - idialoguedelay >= 500
						if mission_stage > enum_to_int(msf_3_second_bomb)
							if player_ped_id() = frank_ped()
								if IS_SAFE_TO_START_CONVERSATION()
									create_conversation(convo_struct,"d2aaud","dh2a_3c",conv_priority_low)					
								endif
							endif
						endif
						mercs[merc14_c4_3].balive = false
//						mercs[MERC15_C4_3].balive = false
						idialoguekillswitch = 0
					endif
				break
			endswitch
		endif
			
		if not isentityalive(mercs[merc12_c4_3].id)			
		and mercs[merc12_c4_3].balive					
			switch idialoguekillswitch
				case 0
					idialoguedelay = get_game_timer()
					idialoguekillswitch++
				break
				case 1
					if get_game_timer() - idialoguedelay >= 500
						if mission_stage > enum_to_int(msf_3_second_bomb)
							if IS_SAFE_TO_START_CONVERSATION()
								if player_ped_id() = mike_ped()
									if bKilledByBuddy
										create_conversation(convo_struct,"d2aaud","dh2a_3d",conv_priority_low)		
										bKilledByBuddy = false
									endif
								else
									create_conversation(convo_struct,"d2aaud","dh2a_3d",conv_priority_low)
								endif
							endif
						endif
						mercs[merc12_c4_3].balive = false
						idialoguekillswitch = 0
					endif
				break	
			endswitch		
		endif
			
		if not isentityalive(mercs[merc13_c4_3].id)			
		and mercs[merc13_c4_3].balive	
			switch idialoguekillswitch
				case 0
					idialoguedelay = get_game_timer()
					idialoguekillswitch++
				break
				case 1
					if get_game_timer() - idialoguedelay >= 500
						if mission_stage > enum_to_int(msf_4_third_bomb)
							if IS_SAFE_TO_START_CONVERSATION()
								if player_ped_id() = mike_ped()							
									if bkilledbybuddy
										create_conversation(convo_struct,"d2aaud","dh2a_4c",conv_priority_low) // frank speaks
									else
//										create_conversation(convo_struct,"d2aaud","dh2a_4d",conv_priority_low) // mike speaks								
									endif
								else
									if bkilledbybuddy
//										create_conversation(convo_struct,"d2aaud","dh2a_4d",conv_priority_low) // mike speaks
									else
										create_conversation(convo_struct,"d2aaud","dh2a_4c",conv_priority_low) // frank speaks								
									endif
								endif					
							endif
						endif
						bkilledbybuddy = false
						mercs[merc13_c4_3].balive = false
						idialoguekillswitch = 0							
					endif
				break	
			endswitch	
		endif
			
		if not isentityalive(mercs[MERC16_C4_4].id)			
		and mercs[MERC16_C4_4].balive	
			switch idialoguekillswitch
				case 0
					idialoguedelay = get_game_timer()
					idialoguekillswitch++
				break
				case 1
					if get_game_timer() - idialoguedelay >= 500
						if mission_stage > enum_to_int(msf_4_third_bomb)
							if IS_SAFE_TO_START_CONVERSATION()
								if player_ped_id() = mike_ped()							
									if bkilledbybuddy
										create_conversation(convo_struct,"d2aaud","dh2a_4h",conv_priority_low) // frank speaks
									else
//										create_conversation(convo_struct,"d2aaud","dh2a_4h",conv_priority_low) // mike speaks								
									endif
								else
									create_conversation(convo_struct,"d2aaud","dh2a_4h",conv_priority_low) // frank speaks
								endif
							endif
						endif
						bkilledbybuddy = false
						mercs[MERC16_C4_4].balive = false	
						idialoguekillswitch = 0
					endif
				break	
			endswitch				
		endif			
		
	endif	
endproc


/// PURPOSE:
///    Helper function to remove sonar blip and outline
PROC REMOVE_CURRENT_SONAR_BLIP()
	// Main blip
	IF DOES_BLIP_EXIST(biSonarBlip)    
		REMOVE_BLIP(biSonarBlip)   
	ENDIF
ENDPROC

/// PURPOSE:
///    Update wreckage area blip when far away and sonar blips when nearby
PROC UPDATE_SONAR_BLIPS()

	// Get current player location
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	FLOAT  fDistance 
	
	IF DOES_ENTITY_EXIST(objLoot)
		fDistance = GET_DISTANCE_BETWEEN_COORDS(get_entity_coords(objLoot), vPlayerPos, FALSE)
	ENDIF
			
	// Trigger sonar update
	IF bSonarUpdate
			
		// Remove any previous existing blip
		REMOVE_CURRENT_SONAR_BLIP()
		
		// Initialise sonar blip
		fCurrentRadius  = 0
		fTargetRadius   = fDistance
		
		// Set minimum size based on whether player is in the dinghy or swimming
		fTargetRadius = CLAMP(fTargetRadius, SONAR_MIN_SIZE_SWIM, 200)
		
		// Set update rate
		fSonarIncrement = fTargetRadius*SONAR_UPDATE_SPEED
		bSonarUpdate    = FALSE
		
		PLAY_SOUND_FROM_COORD(ibeepid,"sonar_pulse",GET_ENTITY_COORDS(objLoot),"docks_heist_finale_2a_sounds")
	ELSE				
		// Remove blip ready for update
		REMOVE_CURRENT_SONAR_BLIP()
		
		// Update size
		fCurrentRadius = fCurrentRadius +@(fSonarIncrement * 30.0)
		fCurrentRadius = CLAMP(fCurrentRadius, 0, fTargetRadius)

		// Resize sonar blip based on distance
		biSonarBlip = ADD_BLIP_FOR_RADIUS(get_entity_coords(objLoot), fCurrentRadius)
		SET_BLIP_ALPHA(biSonarBlip, 255)
		SET_BLIP_COLOUR(biSonarBlip, BLIP_COLOUR_WHITE)
		SET_BLIP_AS_SHORT_RANGE(biSonarBlip, TRUE)
		SET_RADIUS_BLIP_EDGE(biSonarBlip, TRUE)
		SHOW_HEIGHT_ON_BLIP(biSonarBlip, FALSE)

		// We need to create a new blip
		IF fCurrentRadius >= fTargetRadius
			bSonarUpdate = TRUE
		ENDIF
	ENDIF
ENDPROC



//handles the audio scene and effects while the player is underwater
proc AUDIO_underwater()
	if isentityalive(player_ped_id())
		vector v_game_cam_pos = get_gameplay_cam_coord()
		vector v_player_pos = get_entity_coords(player_ped_id())
		
			//get distance from the ship
//		float fdistancetoship
//		fdistancetoship = get_distance_between_coords(get_entity_coords(player_ped_id()),<< -196.3878, -2328.8262, -14.7554 >>)
//		SET_VARIABLE_ON_UNDER_WATER_STREAM("DISTANCE_TO_SHIP",fdistancetoship)	
		
		if player_ped_id() = mike_ped()
			
			if does_entity_exist(objLoot)
				float distancefromcrate = get_distance_between_coords(get_entity_coords(objLoot),get_entity_coords(player_ped_id()))	
				set_variable_on_sound(ibeepid,"bombdistance",distancefromcrate)
				
				IF NOT bStopRebreather
					if  player_ped_id() = mike_ped()
					and not scamdetails.brun
						IF bStartSonar
							UPDATE_SONAR_BLIPS()
						endif
					ENDIF
				endif
				
			endif
			
			IF IS_PED_SWIMMING(mike_ped())
				FORCE_SONAR_BLIPS_THIS_FRAME()
			ENDIF
			
			if has_sound_finished(sound_underwater_rebreather)
				play_sound_from_entity(sound_underwater_rebreather, "rebreather",mike_ped(),"docks_heist_finale_2a_sounds")
			endif
			
		ENDIF
		
		if  v_player_pos.z < 0
		and v_game_cam_pos.z < 0
		
			if has_sound_finished(sound_underwater_ambience)
				play_sound_frontend(sound_underwater_ambience, "uw_ambience")		
			endif	
			
//			if not bUnderWaterStreamed
//				if load_stream("underwater_stream","docks_heist_finale_2a_sounds")
//					bUnderWaterStreamed = true
//				endif
//			else
//				play_stream_frontend()				
//				set_variable_on_stream("distance_to_ship",fdistancetoship)					
//				set_variable_on_stream("above_water",0)
//			endif
						
			if player_ped_id() = mike_ped()
				
				IF NOT bStopRebreather
					if has_sound_finished(sound_underwater_rebreather)
						play_sound_from_entity(sound_underwater_rebreather, "rebreather",mike_ped(),"docks_heist_finale_2a_sounds")
					endif
					
					float f_player_speed = get_entity_speed(mike_ped())
				
					if f_player_speed > 1.0
						if has_sound_finished(i_sound_swishing)
							play_sound_from_entity(i_sound_swishing, "foot_swish", mike_ped(),"docks_heist_finale_2a_sounds")
							set_variable_on_sound(i_sound_swishing, "swimspeed", (f_player_speed - 1.0) / 4.0)
						else
							set_variable_on_sound(i_sound_swishing, "swimspeed", (f_player_speed - 1.0) / 4.0)
						endif
					else 
						if not has_sound_finished(i_sound_swishing)
							stop_sound(i_sound_swishing)
						endif
					endif
				ENDIF
			elif player_ped_id() = trev_ped()	
			or scamdetails.brun
				if not has_sound_finished(ibeepid)
					stop_sound(ibeepid)
				endif
				
				if not has_sound_finished(sound_underwater_rebreather)
					stop_sound(sound_underwater_rebreather)
				endif

				if not has_sound_finished(i_sound_swishing)
					stop_sound(i_sound_swishing)
				endif
				
				if not has_sound_finished(i_sound_scuba_gear)
					stop_sound(i_sound_scuba_gear)
				endif
			endif
		else
			
			if not has_sound_finished(ibeepid)
				stop_sound(ibeepid)
			endif
			
//			if bUnderWaterStreamed
//				set_variable_on_stream("above_water",1)
//			endif
			
			if is_audio_scene_active("dock_heist_underwater_scene")
				stop_audio_scene("dock_heist_underwater_scene")
			endif
			
			if not has_sound_finished(sound_underwater_ambience)
				stop_sound(sound_underwater_ambience)
			endif
			
			if not has_sound_finished(sound_underwater_rebreather)
				stop_sound(sound_underwater_rebreather)
			endif

			if not has_sound_finished(i_sound_swishing)
				stop_sound(i_sound_swishing)
			endif
			
			if not has_sound_finished(i_sound_scuba_gear)
				stop_sound(i_sound_scuba_gear)
			endif
		endif
	endif
endproc
proc AUDIO_sniping()
	float snipeWidth = 0.1
	if not bAudio_SnipeLoaded
		if REQUEST_SCRIPT_AUDIO_BANK("PORT_OF_LS_SNIPING")
			bAudio_snipeLoaded = true
		endif
	endif
	
	switch iaudioswitch_snipeStage
	case 0
		if bFrankFired							
			CPRINTLN( DEBUG_MISSION, "AUDIO_sniping: Playing FRANKLIN FIRED SOUNDS" )
			PLAY_SOUND_FROM_COORD(-1, "SNIPER_SHOT_ZIP", GET_ENTITY_COORDS(frank_ped()) * snipeWidth 
				    	 + vfranks_aim_coord * (1-snipeWidth), "DOCKS_HEIST_FINALE_2A_SOUNDS")		
			if isentityalive(franks_target_ped)
				if HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(franks_target_ped,wtsniper,GENERALWEAPON_TYPE_INVALID)	
					PLAY_SOUND_FROM_COORD(-1, "SNIPER_SHOT_SPLAT", vfranks_aim_coord,"DOCKS_HEIST_FINALE_2A_SOUNDS")
					PLAY_SOUND_FROM_COORD(-1, "SNIPER_SHOT_SPRAY", GET_ENTITY_COORDS(frank_ped())  * (-1* snipeWidth) 
							 + vfranks_aim_coord * (1+snipeWidth), "DOCKS_HEIST_FINALE_2A_SOUNDS")					
				endif
			else
				PLAY_SOUND_FROM_COORD(-1, "SNIPER_SHOT_SPLAT", vfranks_aim_coord,"DOCKS_HEIST_FINALE_2A_SOUNDS")
				PLAY_SOUND_FROM_COORD(-1, "SNIPER_SHOT_SPRAY", GET_ENTITY_COORDS(frank_ped())  * (-1* snipeWidth) 
							 + vfranks_aim_coord * (1+snipeWidth), "DOCKS_HEIST_FINALE_2A_SOUNDS")	
			endif
			iaudioswitch_snipeStage++
		endif
	break
	case 1
		if not bFrankFired
			iaudioswitch_snipeStage = 0
		endif
	break
	endswitch
endproc
proc do_tods_dh2a()
	
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	
	while not do_timelapse(sp_heist_docks_2a,sTimelapse,FALSE,TRUE,TRUE, TRUE)
		IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_START(<<-79.16,-2355.64,1.87>>,NORMALISE_VECTOR(<<0.69,-0.72,0.11>>),300)
		ENDIF
		wait(0)
	endwhile
	
	FREEZE_ENTITY_POSITION(frank_ped(),FALSE)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	SET_TODS_CUTSCENE_RUNNING(sTimelapse, FALSE, FALSE, 2000, FALSE, FALSE)
	SET_PLAYER_CONTROL(player_id(),false)
	SET_FOCUS_ENTITY( mike_ped() )
	INT iCamSceneId
	players_scene_id = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
	IF IS_ENTITY_OK(vehs[mvf_dinghy].id)
		SET_BOAT_ANCHOR( vehs[mvf_dinghy].id, TRUE )
		SET_VEHICLE_ON_GROUND_PROPERLY( vehs[mvf_dinghy].id )
	ENDIF
	iCamSceneId = CREATE_SYNCHRONIZED_SCENE(GET_ENTITY_COORDS(vehs[mvf_dinghy].id) + << 0.0, 0.0, 0.30 >>, GET_ENTITY_ROTATION(vehs[mvf_dinghy].id))
	IF NOT IS_PED_INJURED(mike_ped())
		TASK_SYNCHRONIZED_SCENE(  mike_ped(), players_scene_id, "missheistdocks2aswitchig_7", "IG_7_m_dinghy_f_sniping_michael", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS|SYNCED_SCENE_DONT_INTERRUPT )
	ENDIF
	PLAY_SYNCHRONIZED_CAM_ANIM(camera_a, iCamSceneId, "IG_7_m_dinghy_f_sniping_cam", "missheistdocks2aswitchig_7")
	ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(players_scene_id, vehs[mvf_dinghy].id, 0)
	SET_CAM_ACTIVE(camera_a, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	bDialoguePlayed = FALSE
	bDialoguePlayed2 = FALSE
	KILL_ANY_CONVERSATION()
	CREATE_CONVERSATION(convo_struct,"D2AAUD","DS2A_GO",CONV_PRIORITY_MEDIUM)
endproc
func bool fly_over_cutscene()
		switch icutstage
			case 0				
				//make mercs
				int imerc
				for imerc = enum_to_int(merc3_c4_2) to enum_to_int(merc18_c4_4)
					if not isentityalive(mercs[imerc].id)
						createmercenary(int_to_enum(mmf_mission_merc_flags,imerc))
					endif
				endfor
				bcampan_cued = false	
				if isentityalive(mercs[merc3_c4_2].id)
					set_entity_coords(mercs[merc3_c4_2].id, << -131.54675, -2375.49292, 8.31907 >>)
				endif
				if isentityalive(mercs[merc7_c4_2].id)
					set_entity_coords(mercs[merc7_c4_2].id, << -163.77, -2359.94, 8.32 >>)
				endif
				if isentityalive(mercs[merc12_c4_3].id)
					set_entity_coords(mercs[merc12_c4_3].id,<<-181.17540, -2367.26172, 8.31907 >>)
				endif
				cameraindex = create_cam("default_spline_camera")
				add_cam_spline_node_using_camera(cameraindex, create_cam_with_params("default_scripted_camera", <<-83.6341, -2384.2241, 18.8433>>, <<-11.6585, 2.4080, -3.8427>>, 20.7496, true), 0, cam_spline_node_smooth_rot)
				add_cam_spline_node_using_camera(cameraindex, create_cam_with_params("default_scripted_camera", <<-105.5889, -2379.0615, 13.1824>>, <<-15.4827, -0.5527, 80.9819>>, 20.7496, true), 4800, cam_spline_node_smooth_rot)
				add_cam_spline_node_using_camera(cameraindex, create_cam_with_params("default_scripted_camera", <<-143.5431, -2377.3916, 8.8925>>, <<0.0128, 1.1140, 89.8615>>, 20.7496, true), 5800, cam_spline_node_smooth_rot)
				add_cam_spline_node_using_camera(cameraindex, create_cam_with_params("default_scripted_camera", <<-174.7800, -2380.8171, 10.4344>>, <<1.1428, 5.1966, 77.3842>>, 20.7496, true), 6500, cam_spline_node_smooth_rot)
				add_cam_spline_node_using_camera(cameraindex, create_cam_with_params("default_scripted_camera", <<-188.0171, -2392.9490, 10.7295>>, <<-1.5796, 2.6956, -0.0101>>, 20.7496, true), 6000, cam_spline_node_smooth_rot)
				add_cam_spline_node_using_camera(cameraindex, create_cam_with_params("default_scripted_camera", <<-214.2664, -2399.5540, 11.2041>>, <<-5.6128, 2.6956, -6.2567>>, 20.7496, true), 7400, cam_spline_node_smooth_rot)
				add_cam_spline_node_using_camera(cameraindex, create_cam_with_params("default_scripted_camera", <<-246.3513, -2386.9656, 14.2404>>, <<-2.4819, 2.6956, -65.2727>>, 20.7496, true), 6000, cam_spline_node_smooth_rot)
				add_cam_spline_node_using_camera(cameraindex, create_cam_with_params("default_scripted_camera", <<-248.4400, -2378.4102, 17.4281>>, <<-8.2277, 2.6956, -86.6964>>, 20.7496, true), 2000, cam_spline_node_smooth_rot)
				
				set_cam_active(cameraindex,true)
				shake_cam(cameraindex, "hand_shake", 0.5000)
				iflyovermusictrigger = 10000	
				prep_start_cutscene(false,<< -85.9317, -2364.7205, 13.2961 >>,true,false,false)	
				icsdelay = get_game_timer()	- 6000
				bhotswap = false
				icutstage++
			break
			case 1
				if get_game_timer() - icsdelay > 1500	
				and prepare_music_event("dh2a_camera_pan")
				create_conversation(convo_struct,"d2aaud","dh2a_tk1",conv_priority_medium)
					icutstage++
				endif
			break
			case 2
				if get_game_timer() - icsdelay > iflyovermusictrigger
				and	trigger_music_event("dh2a_camera_pan")
					bcampan_cued = true
					icutstage++
				endif
			break
			case 3
				if get_game_timer() - icsdelay > (iflyovermusictrigger + 14000)
					if prepare_music_event("dh2a_camera_pan_2nd")
						icutstage++
					endif
				endif
			break
			case 4				
				if get_game_timer() - icsdelay > 41000						
					if trigger_music_event("dh2a_camera_pan_2nd")
						bcampan2_cued = true
						icutstage++
					endif
				endif
			break
			case 5				
				if get_game_timer() - icsdelay > 46000						
					return true
				endif
			break
		endswitch
				
		if icutstage > 1
			if is_control_just_pressed(frontend_control,INPUT_SKIP_CUTSCENE)
				if not bcampan_cued
					cancel_music_event("dh2a_camera_pan")				
				elif bcampan_cued
				and not bcampan2_cued
					cancel_music_event("dh2a_camera_pan_2nd")
				endif
				return true
			endif
		endif
		
//		if isentityalive(mercs[merc7_c4_2].id)
//		and get_is_waypoint_recording_loaded("docksheist2a02")
//			if not is_waypoint_playback_going_on_for_ped(mercs[merc7_c4_2].id)						
//				if IS_ENTITY_AT_COORD(mercs[merc7_C4_2].id,<<-163.53667, -2375.51953, 8.31907>>,<<2,2,2>>)
//					task_follow_waypoint_recording(mercs[merc7_c4_2].id,"docksheist2a02",0)
//				else
//					task_follow_waypoint_recording(mercs[merc7_c4_2].id,"docksheist2a02",0,EWAYPOINT_START_FROM_CLOSEST_POINT)
//				endif				
//			endif
//		endif
		
	return false
endfunc
func bool alarm_cutscene()
	switch icutstage
		case 0
			if balarmtriggered
				bcsalarmtriggered = true
				balarmtriggered = false
			endif
			bDialoguePlayed = FALSE
			CLEAR_PED_TASKS(frank_ped())
			//@RJP delete peds during cutscene so they can't kill Michael
			int i
			for i = enum_to_int(merc1_start) to enum_to_int(merc18_c4_4)
				if DOES_ENTITY_EXIST(mercs[i].id)
					delete_ped(mercs[i].id)
				endif
			endfor
		//skip scene stuff
			settimera(0)
		//cam
			cameraIndex = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
			set_cam_active(cameraindex,true)
			prep_start_cutscene(false,<< -209.5674, -2386.8694, 15.0927 >>)	
		//door
			
			IF DOES_ENTITY_EXIST(objDoorHide)
				SET_ENTITY_VISIBLE(objDoorHide, FALSE)
				SET_ENTITY_DYNAMIC( objDoorHide, FALSE )
			ELSE
				objDoorHide = GET_CLOSEST_OBJECT_OF_TYPE( <<-222.0200, -2375.3555, 13.5443>>, 2, PORT_XR_DOOR_05 )
				FREEZE_ENTITY_POSITION( objDoorHide, TRUE )
				SET_ENTITY_VISIBLE(objDoorHide, FALSE)
				SET_ENTITY_DYNAMIC( objDoorHide, FALSE )
			ENDIF
			objDoor = CREATE_OBJECT( P_PO1_01_DoorM_S, <<-222.0200, -2375.3955, 12.327>> )
			iDoorSyncScene = CREATE_SYNCHRONIZED_SCENE ( <<-222.0200, -2375.3955, 13.2843>>, <<0,0,0>>)
		//mike
			SET_ENTITY_INVINCIBLE( mike_ped(), TRUE )
			SET_CURRENT_PED_WEAPON(mike_ped(), WEAPONTYPE_COMBATPISTOL, TRUE)
			IF bcsalarmtriggered
				TASK_SYNCHRONIZED_SCENE(  mike_ped(), iDoorSyncScene, "missheistdocks2aig_1", "IG_1_MichaelEnterBoat_Action", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
				PLAY_ENTITY_ANIM( objDoor, "IG_1_MichaelEnterBoat_Action_DOOR", "missheistdocks2aig_1", INSTANT_BLEND_IN, FALSE, FALSE, FALSE, 0.025 )
//				PLAY_SYNCHRONIZED_ENTITY_ANIM(objDoor, iDoorSyncScene, "IG_1_MichaelEnterBoat_Action_DOOR", "missheistdocks2aig_1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				PLAY_SYNCHRONIZED_CAM_ANIM(cameraIndex, iDoorSyncScene, "IG_1_MichaelEnterBoat_Action_cam", "missheistdocks2aig_1")
				SET_SYNCHRONIZED_SCENE_PHASE(iDoorSyncScene,0.025)
			ELSE
				TASK_SYNCHRONIZED_SCENE(  mike_ped(), iDoorSyncScene, "missheistdocks2aig_1", "IG_1_MichaelEnterBoat_Stealth", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
				PLAY_ENTITY_ANIM( objDoor, "IG_1_MichaelEnterBoat_Stealth_DOOR", "missheistdocks2aig_1", INSTANT_BLEND_IN, FALSE, FALSE, FALSE, 0.035 )
//				PLAY_SYNCHRONIZED_ENTITY_ANIM(objDoor, iDoorSyncScene, "IG_1_MichaelEnterBoat_Stealth_DOOR", "missheistdocks2aig_1", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
				PLAY_SYNCHRONIZED_CAM_ANIM(cameraIndex, iDoorSyncScene, "IG_1_MichaelEnterBoat_stealth_cam", "missheistdocks2aig_1")
				SET_SYNCHRONIZED_SCENE_PHASE(iDoorSyncScene,0.035)
				CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_GOIN", CONV_PRIORITY_MEDIUM )
			ENDIF
			SET_SEETHROUGH(FALSE)
			//b_nightvision_active = FALSE
			SET_CAM_ACTIVE(cameraIndex, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
		//frank
			set_entity_coords(frank_ped(),<< -143.8203, -2488.2612, 43.4412 >>)
//			task_aim_gun_at_coord(frank_ped(),c4[c4_3_inside].exppos,-1)
		//misc
		
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0)
		
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
			
			give_mission_weapons()
			icsdelay = get_game_timer()
			
			icutstage++
		break
		case 1
			IF bcsalarmtriggered
				IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_IN_NS", CONV_PRIORITY_MEDIUM )
					icutstage++
				ENDIF
			ELSE
				IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_GOIN", CONV_PRIORITY_MEDIUM )
					icutstage++
				ENDIF
			ENDIF
		break
		case 2
			IF NOT (PLAYER_PED_ID() = frank_ped())
				IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds,selector_ped_franklin)
					TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds,TRUE,TRUE)
					bSwappedToFranklin = TRUE
					SET_PLAYER_FORCED_AIM(PLAYER_ID(),TRUE)
					SET_PLAYER_FORCE_SKIP_AIM_INTRO( PLAYER_ID(), TRUE )
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					ENDIF
					NEW_LOAD_SCENE_START_SPHERE(<< -143.8203, -2488.2612, 43.4412 >>, 25)
//					NEW_LOAD_SCENE_START(<<-144.16,-2486.51,44.55>>,NORMALISE_VECTOR(<<0.15,-0.97,0.17>>),50)
				ENDIF
			ELSE
				if bcsalarmtriggered
					IF GET_SYNCHRONIZED_SCENE_PHASE(iDoorSyncScene) >= 0.7
						icsdelay = get_game_timer()
						icutstage++
					ENDIF
				else
					IF GET_SYNCHRONIZED_SCENE_PHASE(iDoorSyncScene) >= 0.72
						icsdelay = get_game_timer()
						icutstage++
					ENDIF
				endif
			ENDIF
			IF IS_SAFE_TO_START_CONVERSATION()
				IF NOT bDialoguePlayed
					ADD_PED_FOR_DIALOGUE(convo_struct,1,null,"MICHAEL")
					if bcsalarmtriggered
						if create_conversation(convo_struct,"d2aaud","DH2A_WTCH_NS",conv_priority_medium)
							bDialoguePlayed = TRUE
						endif
					else
						if create_conversation(convo_struct,"d2aaud","DH2A_FMB",conv_priority_medium)
							bDialoguePlayed = TRUE
						endif
					endif
				ENDIF
			ENDIF
		break
		case 3
			players_scene_id = CREATE_SYNCHRONIZED_SCENE( <<-143.8, -2487.555, 43.450>>, <<0,0,-34.5>> )
			TASK_SYNCHRONIZED_SCENE(  frank_ped(), players_scene_id, "missheistdocks2aswitchig_7", "IG_7_m_door_f_sniping_franklin", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_PROCESS_ATTACHMENTS_ON_START)
			PLAY_SYNCHRONIZED_CAM_ANIM( cameraIndex, players_scene_id, "IG_7_m_door_f_sniping_cam", "missheistdocks2aswitchig_7")
			CPRINTLN(DEBUG_MISSION, "RUNNING FRANKLIN SNIPING SYNC SCENE")
			
			IF bSwappedToFranklin
				ANIMPOSTFX_PLAY("SwitchSceneFranklin", 1000, FALSE)
				PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
			ENDIF
				
			bDialoguePlayed = FALSE
			icutstage++
		break
		case 4
			IF GET_SYNCHRONIZED_SCENE_PHASE(players_scene_id) > 0.999
				CPRINTLN(DEBUG_MISSION, "FRANKLIN SNIPING SYNC SCENE FINISHED")
				icsdelay = get_game_timer()
				if DOES_ENTITY_EXIST(MIKE_PED())
					SET_ENTITY_VISIBLE(mike_ped(),false)
				endif
				IF IS_AUDIO_SCENE_ACTIVE("DH2A_SHOOTOUT_SCENE")
					STOP_AUDIO_SCENE("DH2A_SHOOTOUT_SCENE")
				ENDIF
				IF IS_AUDIO_SCENE_ACTIVE("DH2A_PLANT_BOMBS_scene")
					STOP_AUDIO_SCENE("DH2A_PLANT_BOMBS_scene")
				ENDIF
				IF IS_AUDIO_SCENE_ACTIVE("DH2A_SNIPING_SCENE")
					STOP_AUDIO_SCENE("DH2A_SNIPING_SCENE")
				ENDIF
				CPRINTLN( DEBUG_MISSION, "STARTING: DH2A_SHOOTOUT_SNIPING_scene" )
				start_audio_scene("DH2A_SHOOTOUT_SNIPING_SCENE")
				trigger_music_event("dh2a_main_alarm")
				IF DOES_ENTITY_EXIST(objDoorHide)
					SET_ENTITY_VISIBLE(objDoorHide, TRUE)
					SET_OBJECT_AS_NO_LONGER_NEEDED(objDoorHide)
				ENDIF
				if does_entity_exist(objDoor)
					delete_object(objDoor)
				endif
				CLEAR_PED_TASKS( PLAYER_PED_ID() )
				SET_CURRENT_PED_WEAPON( PLAYER_PED_ID(), wtSniper, TRUE )
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(get_sniper_rifle_aim_heading_for_coord(<<-222.0200, -2375.3955, 14.2843>>))
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(get_sniper_rifle_aim_pitch_for_coord(<<-222.0200, -2375.3955, 14.2843>>))
				SET_PLAYER_FORCED_AIM( PLAYER_ID(), TRUE )
				SET_PLAYER_FORCE_SKIP_AIM_INTRO( PLAYER_ID(), TRUE )
				SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(7)
				DISPLAY_SNIPER_SCOPE_THIS_FRAME()
				FORCE_PED_MOTION_STATE( PLAYER_PED_ID(), MS_AIMING )
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(),TRUE)
				REPLAY_STOP_EVENT()
				prep_stop_cutscene(TRUE)
				balarmtriggered = TRUE
				bDialoguePlayed = FALSE
				icsdelay = get_game_timer()
				return true
			ELIF GET_SYNCHRONIZED_SCENE_PHASE(players_scene_id) > 0.3
				IF NOT bDialoguePlayed
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					ENDIF
					NEW_LOAD_SCENE_START(<<-143.73,-2488.60,45.02>>,NORMALISE_VECTOR(<<-0.56,0.80,-0.22>>),300)
					bDialoguePlayed = TRUE
				ENDIF
			ENDIF
		break
	endswitch
	
//======= cutscene checks =============
					
			if timera() > 1500
				if is_control_just_pressed(frontend_control,INPUT_SKIP_CUTSCENE)			
					REPLAY_CANCEL_EVENT()
					IF NOT (PLAYER_PED_ID() = frank_ped())
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds,selector_ped_franklin)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds,TRUE,TRUE)	
					ENDIF
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
						CPRINTLN( DEBUG_MISSION, "alarm_cutscene: Stopping new load scene." )
					ENDIF
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(get_sniper_rifle_aim_heading_for_coord(<<-222.0200, -2375.3955, 14.2843>>))
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(get_sniper_rifle_aim_pitch_for_coord(<<-222.0200, -2375.3955, 14.2843>>))
					SET_PLAYER_FORCED_AIM(PLAYER_ID(),TRUE)
					SET_PLAYER_FORCE_SKIP_AIM_INTRO( PLAYER_ID(), TRUE )
					SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(7)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(),TRUE)
					IF IS_AUDIO_SCENE_ACTIVE("DH2A_SHOOTOUT_SCENE")
						STOP_AUDIO_SCENE("DH2A_SHOOTOUT_SCENE")
					ENDIF
					IF IS_AUDIO_SCENE_ACTIVE("DH2A_PLANT_BOMBS_scene")
						STOP_AUDIO_SCENE("DH2A_PLANT_BOMBS_scene")
					ENDIF
					IF IS_AUDIO_SCENE_ACTIVE("DH2A_SNIPING_SCENE")
						STOP_AUDIO_SCENE("DH2A_SNIPING_SCENE")
					ENDIF
					CPRINTLN( DEBUG_MISSION, "STARTING: DH2A_SHOOTOUT_SNIPING_scene" )
					start_audio_scene("DH2A_SHOOTOUT_SNIPING_SCENE")
					bdoskip = true
					iskiptostage = enum_to_int(msf_6_way_out)				
				endif
			endif
			
		
//=====================================
	return false
endfunc
proc run_intro_cutscene()
	
	#if is_debug_build
		if is_cutscene_active()
			if is_keyboard_key_just_pressed(key_j)
				stop_cutscene()
				remove_cutscene()
				while is_cutscene_active()
					wait(0)
				endwhile
			endif
		endif
	#endif
	
	
	if not b_cutscene_loaded
		if not is_cutscene_active()
			SET_SRL_FORCE_PRESTREAM(SRL_PRESTREAM_FORCE_COMPLETELY_OFF)
			println("REQUESTING CS SECTIONS")
			if 	get_current_player_ped_enum()  = char_trevor
				request_cutscene_with_playback_list("lsdh_2a_int",cs_section_1 |cs_section_2 | cs_section_3 | cs_section_4 | cs_section_5 | cs_section_6)
			elif get_current_player_ped_enum()  = char_michael
				request_cutscene_with_playback_list("lsdh_2a_int",cs_section_3 | cs_section_4 | cs_section_5 | cs_section_6)
			elif get_current_player_ped_enum()  = char_franklin
				request_cutscene_with_playback_list("lsdh_2a_int",cs_section_5 | cs_section_6)
			endif
			wait(0)
		endif
		
		IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
//		if has_cutscene_loaded()
			
			//IF IS_GAMEPLAY_HINT_ACTIVE()
			//	STOP_GAMEPLAY_HINT()
			//ENDIF
			
			//Grab ownership of the entities created by the lead-in to this mission
			//and bind them into the intro cutscene.
			//See trigger_scene_docks_2A.sch for the lead-in scripts.
			// g_sTriggerSceneAssets.ped[0]	- Wade	
			// g_sTriggerSceneAssets.ped[1]	- Floyd	
			// g_sTriggerSceneAssets.ped[2]	- Trevor
			// g_sTriggerSceneAssets.ped[3]	- Michael
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
				SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], FALSE, TRUE)
				REGISTER_ENTITY_FOR_CUTSCENE(g_sTriggerSceneAssets.ped[0], "Wade", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, IG_WADE)
			ENDIF
			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
				SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[1], FALSE, TRUE)
				REGISTER_ENTITY_FOR_CUTSCENE(g_sTriggerSceneAssets.ped[1], "Floyd", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, IG_FLOYD)
			ENDIF
			
			//Franklin
			IF DOES_ENTITY_EXIST( g_sTriggerSceneAssets.ped[4] )
				sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = g_sTriggerSceneAssets.ped[4]
				SET_ENTITY_AS_MISSION_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE, TRUE)
				FREEZE_ENTITY_POSITION( sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE )
			ENDIF
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				CPRINTLN( DEBUG_MISSION, "PLAYER IS FRANKLIN" )
				sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = PLAYER_PED_ID()
			ENDIF
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					CPRINTLN( DEBUG_MISSION, "FRANKLIN EXISTS, REGISTER HIM" )
					REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ONE)
				ENDIF
			ELSE
				CPRINTLN( DEBUG_MISSION, "FRANKLIN DOES NOT EXIST, REGISTER NULL" )
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Franklin", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PLAYER_ONE)
			ENDIF
			
			//Michael
			IF DOES_ENTITY_EXIST( g_sTriggerSceneAssets.ped[3] )
				sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = g_sTriggerSceneAssets.ped[3]
				SET_ENTITY_AS_MISSION_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE, TRUE)
				FREEZE_ENTITY_POSITION( sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE )
			ENDIF
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				CPRINTLN( DEBUG_MISSION, "PLAYER IS MICHAEL" )
				sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = PLAYER_PED_ID()
			ENDIF
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					CPRINTLN( DEBUG_MISSION, "MICHAEL EXISTS, REGISTER HIM" )
					REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
				ENDIF
			ELSE
				CPRINTLN( DEBUG_MISSION, "MICHAEL DOES NOT EXIST, REGISTER NULL" )
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Michael", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PLAYER_ZERO)
			ENDIF
			
			//Trevor
			IF DOES_ENTITY_EXIST( g_sTriggerSceneAssets.ped[2] )
				sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = g_sTriggerSceneAssets.ped[2]
				SET_ENTITY_AS_MISSION_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE, TRUE)
			ENDIF
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				CPRINTLN( DEBUG_MISSION, "PLAYER IS TREVOR" )
				sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = PLAYER_PED_ID()
			ELSE
				IF NOT IS_REPLAY_IN_PROGRESS()
					IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
						bSwappedToTrevor = TRUE
						CPRINTLN( DEBUG_MISSION, "MAKE_SELECTOR_PED_SELECTION TREVOR TRUE" )
						SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE( PLAYER_PED_ID() )
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
					ENDIF
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					CPRINTLN( DEBUG_MISSION, "TREVOR EXISTS, REGISTER HIM" )
					REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
				ENDIF
			ELSE
				CPRINTLN( DEBUG_MISSION, "TREVOR DOES NOT EXIST, REGISTER NULL" )
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PLAYER_TWO)
			ENDIF
			
			//Ensure the trigger scene assets are cleaned up from memory.
			MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_DOCKS_2A)
			
			//replace the [MF] fix for B*1744611 from below with resolve vehicle command to re-position the vehicle blocking staircase instead of deleting it
			RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1150.051147,-1523.638184,3.329700>>, <<-1142.782593,-1518.456421,12.556736>>, 4.0,
												<< -1154.6117, -1520.0699, 3.3456 >> ,33.0431, TRUE, FALSE, TRUE, TRUE) //staircase
												
			RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1153.675415,-1516.567017,9.632723>>, <<-1149.717529,-1522.150024,12.633055>>, 4.0,
												<< -1154.6117, -1520.0699, 3.3456 >> ,33.0431, TRUE, FALSE, TRUE, TRUE) //house interior
			
			vehs[mvf_mission_veh].id = GET_MISSION_START_VEHICLE_INDEX()
			IF isentityalive( vehs[mvf_mission_veh].id )
				IF NOT IS_ENTITY_A_MISSION_ENTITY(vehs[mvf_mission_veh].id)
					SET_ENTITY_AS_MISSION_ENTITY( vehs[mvf_mission_veh].id, TRUE, TRUE )
				ENDIF
				SET_VEHICLE_DOORS_SHUT( vehs[mvf_mission_veh].id )
				SET_VEHICLE_ON_GROUND_PROPERLY( vehs[mvf_mission_veh].id )
				
				REQUEST_VEHICLE_ASSET(GET_ENTITY_MODEL(vehs[mvf_mission_veh].id))
			ENDIF
			
			//once the mission start vehicle has been made into mission entity, call the clear areas as orignally intented
			
			//[MF] Fix for B* 1744611 - Delete any vehicles that could be blocking the stairs to Trevor's apartment
			CLEAR_AREA_OF_VEHICLES((<<-1151.3, -1520.1, 11.1>>), 4.0)
			CLEAR_AREA_OF_VEHICLES((<<-1149.4, -1523.4, 10.9>>), 3.0)
			CLEAR_AREA_OF_VEHICLES((<<-1145.9, -1521.6, 9.3>>), 2.0)
			CLEAR_AREA_OF_VEHICLES((<<-1143.6, -1519.0, 7.5>>), 2.5)
			CLEAR_AREA_OF_VEHICLES((<<-1147.4, -1520.5, 5.9>>), 2.5)
			
			//interior
			interior_living_room = get_interior_at_coords(<< -1158.3411, -1520.8929, 9.6345 >>)
			if is_valid_interior(interior_living_room)
				pin_interior_in_memory(interior_living_room)
//				while not is_interior_ready(interior_living_room)
//					wait(0)
//				endwhile
			endif
//			clear_area(<< -1154.6117, -1520.0699, 3.3456 >>,100,true)			
			CLEAR_AREA_OF_PEDS( << -1154.6117, -1520.0699, 3.3456 >>, 100 )
			CLEAR_AREA_OF_VEHICLES( << -1154.6117, -1520.0699, 3.3456 >>, 100 )
			set_scripts_safe_for_cutscene(true)		
			set_cutscene_fade_values(false,false,false,false)
			SWITCH GET_PLAYER_CHARACTER_AT_MISSION_START()
				CASE CHAR_TREVOR
					start_cutscene()
				BREAK
				CASE CHAR_FRANKLIN
				CASE CHAR_MICHAEL
					start_cutscene(CUTSCENE_PLAYER_FP_FLASH_TREVOR)
				BREAK
			ENDSWITCH
			SET_PED_AS_NO_LONGER_NEEDED( g_sTriggerSceneAssets.ped[0] )
			SET_PED_AS_NO_LONGER_NEEDED( g_sTriggerSceneAssets.ped[1] )
			SET_MODEL_AS_NO_LONGER_NEEDED( IG_WADE )
			SET_MODEL_AS_NO_LONGER_NEEDED( IG_FLOYD )
			b_cutscene_loaded = true			
		endif	
	endif
endproc
//------------------------------------------------------------------------------------------------------------
//		Stage set-up
//------------------------------------------------------------------------------------------------------------
PROC check_time()
	IF NOT IS_TIME_BETWEEN_THESE_HOURS(g_sMissionStaticData[sp_heist_docks_2a].startHour, g_sMissionStaticData[sp_heist_docks_2a].endHour)
		CPRINTLN( DEBUG_MISSION, "check_time: time is not in window, setting to start time" )
		SET_CLOCK_TIME(g_sMissionStaticData[sp_heist_docks_2a].startHour, 00, 00)
	ENDIF
ENDPROC

PROC SU_GENERAL()
	
	//DIALOGUE	
	IF ISENTITYALIVE(FRANK_PED())
		ADD_PED_FOR_DIALOGUE(CONVO_STRUCT, 3, FRANK_PED(), "FRANKLIN")
		SET_ENTITY_LOAD_COLLISION_FLAG(FRANK_PED(),TRUE)
		//RESTORE_PLAYER_PED_VARIATIONS(FRANK_PED())
		SET_PED_COMP_ITEM_CURRENT_SP(FRANK_PED(), COMP_TYPE_PROPS, PROPS_P1_HEADSET)
		SET_PED_CONFIG_FLAG(FRANK_PED(),PCF_WILLFLYTHROUGHWINDSCREEN,FALSE)
		if player_ped_id() != frank_ped()
			SET_PED_RELATIONSHIP_GROUP_HASH(FRANK_PED(),rel_group_buddy)
		endif
	ENDIF
	IF ISENTITYALIVE(TREV_PED())
		ADD_PED_FOR_DIALOGUE(CONVO_STRUCT, 2, TREV_PED(), "TREVOR")
		SET_ENTITY_LOAD_COLLISION_FLAG(TREV_PED(),TRUE)
		//RESTORE_PLAYER_PED_VARIATIONS(TREV_PED())
		SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED(), COMP_TYPE_PROPS, PROPS_P2_HEADSET)
		SET_PED_CONFIG_FLAG(TREV_PED(),PCF_WILLFLYTHROUGHWINDSCREEN,FALSE)
		if player_ped_id() != trev_ped()
			SET_PED_RELATIONSHIP_GROUP_HASH(TREV_PED(),rel_group_buddy)
		endif
	ENDIF
	IF ISENTITYALIVE(MIKE_PED())
		SCUBA_SUITE_UP(MIKE_PED())		
		ADD_PED_FOR_DIALOGUE(CONVO_STRUCT, 1, MIKE_PED(), "MICHAEL")
		SET_ENTITY_LOAD_COLLISION_FLAG(MIKE_PED(),TRUE)
		SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED(), COMP_TYPE_PROPS, PROPS_P0_HEADSET)
		SET_PED_CONFIG_FLAG(MIKE_PED(),PCF_WILLFLYTHROUGHWINDSCREEN,FALSE)	
		if player_ped_id() != mike_ped()
			SET_PED_RELATIONSHIP_GROUP_HASH(MIKE_PED(),rel_group_buddy)
		endif
	ENDIF
ENDPROC

PROC CREATE_PROXY_BOMBS(INT iNumBombs)
	IF iNumBombs = 3
		objBomb[2] = CREATE_OBJECT( GET_WEAPONTYPE_MODEL(wtBomb), <<-187.77, -2365.49, 9.32>> )
		IF DOES_ENTITY_EXIST(objBomb[2])
			FREEZE_ENTITY_POSITION( objBomb[2], TRUE )
			SET_ENTITY_ROTATION( objBomb[2], <<22.668, -90.000, -10.000>> )
		ENDIF
	ENDIF
	IF iNumBombs >= 2
		objBomb[1] = CREATE_OBJECT( GET_WEAPONTYPE_MODEL(wtBomb), <<-127.93, -2365.46, 9.33>> )
		IF DOES_ENTITY_EXIST(objBomb[1]) 
			FREEZE_ENTITY_POSITION( objBomb[1], TRUE )
			SET_ENTITY_ROTATION( objBomb[1], <<22.668, -90.000, -10.000>> )
		ENDIF
	ENDIF
	IF iNumBombs >= 1
		objBomb[0] = CREATE_OBJECT( GET_WEAPONTYPE_MODEL(wtBomb), <<-84.715, -2365.90, 14.30>> )
		IF DOES_ENTITY_EXIST(objBomb[0])
			FREEZE_ENTITY_POSITION( objBomb[0], TRUE )
			SET_ENTITY_ROTATION( objBomb[0], <<22.668, 79.339, 4.143>> )
		ENDIF
	ENDIF
ENDPROC

PROC SU_0_DRIVE_TO_BRIDGE()
	//PLAYER
//	IF SET_CURRENT_PLAYER_PED(SELECTOR_PED_FRANKLIN,TRUE)
		
	IF NOT IS_REPLAY_BEING_SET_UP()
		SET_CURRENT_PLAYER_PED(SELECTOR_PED_TREVOR,TRUE)
		CPRINTLN(DEBUG_MISSION, "SU_0_DRIVE_TO_BRIDGE: REPLAY NOT BEING PROCESSED")
	//TREVOR
		SET_ENTITY_COORDS(trev_ped(),<<-1149.8112, -1522.1071, 9.6330>>)
		SET_ENTITY_HEADING(trev_ped(),228.2091)		
		while not NEW_LOAD_SCENE_START_SPHERE(<<-1149.8112, -1522.1071, 9.6330>>,10,NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
			wait(0)
		endwhile
		while not IS_NEW_LOAD_SCENE_LOADED()
			wait(0)
		endwhile
		NEW_LOAD_SCENE_STOP()
	ENDIF
	
	//FRANK			
	WHILE NOT CREATE_PLAYER_PED_ON_FOOT(PEDS[mpf_frank].ID,CHAR_FRANKLIN,<<-1154.6628, -1518.2433, 9.6327>>, 306.3135 )
		WAIT(0)
	ENDWHILE
	sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = PEDS[mpf_frank].ID
	
	END_REPLAY_SETUP()
	
	IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
		SEQUENCE_INDEX SequenceIndex
		CLEAR_SEQUENCE_TASK(SequenceIndex)
		OPEN_SEQUENCE_TASK(SequenceIndex)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1150.7572, -1520.9470, 9.6327>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1149.5098, -1522.4679, 9.6331>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
			TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1147.3914, -1522.7167, 9.4130>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, 312.1079)
		CLOSE_SEQUENCE_TASK(SequenceIndex)
		TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), SequenceIndex)
		CLEAR_SEQUENCE_TASK(SequenceIndex)
	ELSE
		SIMULATE_PLAYER_INPUT_GAIT( PLAYER_ID(), PEDMOVE_WALK, 3500 )
	ENDIF
	FORCE_PED_MOTION_STATE( PLAYER_PED_ID(), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT )
	
	BSKIP_CUT = TRUE
//	ELSE
//		WAIT(0)
//	ENDIF
ENDPROC
PROC SU_1_SNIPE_GAURDS()
	//PLAYER
	IF SET_CURRENT_PLAYER_PED(SELECTOR_PED_FRANKLIN,TRUE)	
		
		//IPL
		MANAGE_IPLS(BUILDINGSTATE_NORMAL)
		
		IF NOT IS_REPLAY_BEING_SET_UP()
			//FRANK	
			SET_ENTITY_COORDS(FRANK_PED(),<< -143.8203, -2488.2612, 43.4412 >>)
			SET_ENTITY_HEADING(FRANK_PED(), 337.5851)
			
			while not NEW_LOAD_SCENE_START_SPHERE(<< -143.8203, -2488.2612, 43.4412 >>,10,NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
				wait(0)
			endwhile
			while not IS_NEW_LOAD_SCENE_LOADED()
				wait(0)
			endwhile
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		//MIKE			
		WHILE NOT CREATE_PLAYER_PED_ON_FOOT(PEDS[MPF_MIKE].ID,CHAR_MICHAEL,<<-77.9618, -2359.0828, 0.5>>,90.7853)
			WAIT(0)
		ENDWHILE
		ADD_PED_FOR_DIALOGUE(CONVO_STRUCT,1,MIKE_PED(),"MICHAEL")		
		SCUBA_SUITE_UP(MIKE_PED())
		SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED(), COMP_TYPE_PROPS, PROPS_P0_HEADSET)					
		set_ped_dies_in_water(mike_ped(),false)
		
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_PED(),TRUE)
		//dinghy
		vehs[mvf_dinghy].id = CREATE_VEHICLE(dinghy,<<-86.7, -2353.2, 0>>, 270.3750)
		SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_dinghy].id,0)
		SET_VEHICLE_RADIO_ENABLED( vehs[mvf_dinghy].id, FALSE )	
		//MERCS
		CREATEMERCENARY(MERC1_START, 8, TRUE)
		CREATEMERCENARY(MERC2_START, 8, TRUE)
		GIVE_WEAPON_TO_PED(MERCS[MERC1_START].ID,WEAPONTYPE_HEAVYSNIPER,200,TRUE,TRUE)
		GIVE_WEAPON_TO_PED(MERCS[MERC2_START].ID,WEAPONTYPE_HEAVYSNIPER,200,TRUE,TRUE)	
		
		CREATEMERCENARY(MERC4_C4_2)
		//CREATE PATROLLING GUARDS
		CREATEMERCENARY(MERC3_C4_2)
		CREATEMERCENARY(MERC5_C4_2)
//		CREATEMERCENARY(MERC6_C4_2)		//@RJP B*1321797 - removing patrols of two guys
					
			//SECOND MAST STAGE
//		CREATEMERCENARY(MERC7_C4_2)		// WILL FOLLOW WAYPOINT	
//		CREATEMERCENARY(MERC8_C4_2)
		CREATEMERCENARY(MERC12_C4_3)		
		CREATEMERCENARY(MERC9_C4_3)
//		CREATEMERCENARY(MERC10_C4_3,1)	//@RJP B*1321797 - removing patrols of two guys
		CREATEMERCENARY(MERC11_C4_3,1)	
		
		//TRAPPED STAGE
		CREATEMERCENARY(MERC13_C4_3)
		CREATEMERCENARY(MERC14_C4_3)
		CREATEMERCENARY(MERC16_C4_4)
//		CREATEMERCENARY(MERC15_C4_3)	//@RJP B*1321797 - removing patrols of two guys
//		CREATEMERCENARY(MERC17_C4_4)
		CREATEMERCENARY(MERC18_C4_4)
		
		//VEHICLES
		VEHS[mvf_parked1].id = CREATE_VEHICLE(landstalker,<<-203.5000, -2413.4336, 5.0012>>, 293.3041)
		VEHS[mvf_parked2].id = CREATE_VEHICLE(landstalker,<<-180.8568, -2431.3264, 5.0013>>, 94.2693)
		SET_VEHICLE_COLOUR_COMBINATION(VEHS[mvf_parked1].ID,0)
		SET_VEHICLE_COLOUR_COMBINATION(VEHS[mvf_parked2].ID,0)
		SET_VEHICLE_CAN_LEAK_PETROL(VEHS[mvf_parked1].ID,TRUE)
		SET_VEHICLE_CAN_LEAK_PETROL(VEHS[mvf_parked2].ID,TRUE)
		
		//SET UP CRATES 
		OBJCOV1 =  CREATE_OBJECT (PROP_CRATEPILE_07A, <<-215.770004,-2392.330078,5>>)
		SET_ENTITY_ROTATION(OBJCOV1, <<-0.000000,0.000000,-29.999996>>)
		SET_ENTITY_LOD_DIST( OBJCOV1, 200 )
		OBJCOV2 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-211.029999,-2388.360107,5>>)
		SET_ENTITY_ROTATION (OBJCOV2, <<-0.000000,0.000000,-12.000000>>)
		SET_ENTITY_LOD_DIST( OBJCOV2, 200 )
		OBJCOV3 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-212.509995,-2387.169922,5>>)
		SET_ENTITY_ROTATION (OBJCOV3, <<-0.000000,0.000000,-39.999992>> )
		SET_ENTITY_LOD_DIST( OBJCOV3, 200 )
		OBJCOV4 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-184.7490, -2422.6982, 5.0013>>)
		SET_ENTITY_ROTATION (OBJCOV4, <<0.0,0.0,8.3525>> )
		SET_ENTITY_LOD_DIST( OBJCOV4, 200 )
		OBJCOV5 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-193.7419, -2422.9417, 5.0007>>)
		SET_ENTITY_ROTATION (OBJCOV5, <<0.0,0.0,220.8658>> )
		SET_ENTITY_LOD_DIST( OBJCOV5, 200 )
		
		WHILE NOT PREPARE_MUSIC_EVENT("DH2A_START")
			WAIT(0)
		ENDWHILE
		
//		Load_Asset_NewLoadScene_Sphere(sAssetData,<< -144.0797, -2489.8604, 43.4097 >>,10)
		
		CPRINTLN(DEBUG_MISSION, "ready to END_REPLAY_SETUP" )
		END_REPLAY_SETUP()
		
		SET_ENTITY_COORDS( PLAYER_PED_ID(), << -143.8203, -2488.2612, 43.4412 >> )
//		SET_ENTITY_HEADING( PLAYER_PED_ID(), 337.5851 )
		
//		NEW_LOAD_SCENE_START_SPHERE( << -93.3159, -2368.9175, 13.2960 >>, 50 )
		
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_SNIPER_RIFLE_AIM_HEADING_FOR_PED(MERCS[MERC1_START].ID))
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(GET_SNIPER_RIFLE_AIM_PITCH_FOR_PED(MERCS[MERC1_START].ID))
		SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), TRUE)
		SET_PLAYER_FORCED_AIM(PLAYER_ID(), TRUE)
		FORCE_PED_AI_AND_ANIMATION_UPDATE( PLAYER_PED_ID(), TRUE )
		SET_PED_NO_TIME_DELAY_BEFORE_SHOT( PLAYER_PED_ID() )
		DISPLAY_SNIPER_SCOPE_THIS_FRAME()
		
		TRIGGER_MUSIC_EVENT("DH2A_SNIPE_GUARDS_RT")
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Stage 1: Snipe Guards")
	ELSE 
		WAIT(0)
	ENDIF
ENDPROC
PROC SU_2_FIRST_BOMB()
		
	//IPL
	MANAGE_IPLS(BUILDINGSTATE_NORMAL)
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		IF SET_CURRENT_PLAYER_PED(SELECTOR_PED_MICHAEL,TRUE)
			SET_ENTITY_COORDS(MIKE_PED(), <<-83.5905, -2361.7979, 13.2963>>)
			set_entity_heading(mike_ped(),119.7045)	
			while not NEW_LOAD_SCENE_START_SPHERE(C4[C4_0_FRONT_OF_SHIP].EXPPOS,15,NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
				wait(0)
			endwhile
			while not IS_NEW_LOAD_SCENE_LOADED()
				wait(0)
			endwhile
			NEW_LOAD_SCENE_STOP()
			
			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(PEDS[MPF_FRANK].ID,CHAR_FRANKLIN,<< -143.8203, -2488.2612, 43.4412 >>, 10.7369)
				WAIT(0)
			ENDWHILE
			ADD_PED_FOR_DIALOGUE(CONVO_STRUCT,3,PEDS[MPF_FRANK].ID,"FRANKLIN")
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( PEDS[MPF_FRANK].ID, TRUE )
		ENDIF
	ELSE	
		WHILE NOT CREATE_PLAYER_PED_ON_FOOT(PEDS[MPF_FRANK].ID,CHAR_FRANKLIN,<< -143.8203, -2488.2612, 43.4412 >>, 10.7369)
			WAIT(0)
		ENDWHILE
		sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = PEDS[MPF_FRANK].ID
		ADD_PED_FOR_DIALOGUE(CONVO_STRUCT,3,PEDS[MPF_FRANK].ID,"FRANKLIN")	
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( PEDS[MPF_FRANK].ID, TRUE )
	ENDIF
	
	PRELOAD_OUTFIT( MIKE_PED(), OUTFIT_P0_SCUBA_LAND )
	WHILE NOT HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(MIKE_PED())
	AND NOT HAS_PED_PRELOAD_PROP_DATA_FINISHED(MIKE_PED())
		WAIT(0)
	ENDWHILE
	
	SCUBA_SUITE_UP(MIKE_PED(), FALSE, FALSE)
	
	WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(MIKE_PED())
		WAIT(0)
	ENDWHILE
	
	RELEASE_PED_PRELOAD_VARIATION_DATA(MIKE_PED())
	RELEASE_PED_PRELOAD_PROP_DATA(MIKE_PED())
	
	SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED(),COMP_TYPE_PROPS,PROPS_P0_HEADSET)
	SET_PED_STEALTH_MOVEMENT(mike_ped(),true)
	
	//dinghy
	vehs[mvf_dinghy].id = CREATE_VEHICLE(dinghy,<<-86.7, -2353.2, 0>>, 270.3750)
	SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_dinghy].id,0)
	SET_VEHICLE_RADIO_ENABLED( vehs[mvf_dinghy].id, FALSE )	
	
	SET_PICKUP_GENERATION_RANGE_MULTIPLIER(2.0)
	
	iStairNavMeshBlocker[0] = ADD_NAVMESH_BLOCKING_OBJECT(<<-135.905899,-2383.557861,5.000676>>,<<1.500000,1.500000,1.500000>>,0)
	iStairNavMeshBlocker[1] = ADD_NAVMESH_BLOCKING_OBJECT(<<-214.905899,-2383.557861,5.000676>>,<<1.500000,1.500000,1.500000>>,0)
	
	//MERCS
	CREATEMERCENARY(MERC4_C4_2)
	//CREATE PATROLLING GUARDS
	CREATEMERCENARY(MERC3_C4_2)
	CREATEMERCENARY(MERC5_C4_2)
//	CREATEMERCENARY(MERC6_C4_2)		//@RJP B*1321797 - removing patrols of two guys
				
	//SECOND MAST STAGE
//	CREATEMERCENARY(MERC7_C4_2)		// WILL FOLLOW WAYPOINT	
//	CREATEMERCENARY(MERC8_C4_2)
	CREATEMERCENARY(MERC12_C4_3)		
	CREATEMERCENARY(MERC9_C4_3)
//	CREATEMERCENARY(MERC10_C4_3,1)	//@RJP B*1321797 - removing patrols of two guys
	CREATEMERCENARY(MERC11_C4_3,1)	
	
	//TRAPPED STAGE
	CREATEMERCENARY(MERC13_C4_3)
	CREATEMERCENARY(MERC14_C4_3)
	CREATEMERCENARY(MERC16_C4_4)
//	CREATEMERCENARY(MERC15_C4_3)	//@RJP B*1321797 - removing patrols of two guys
//	CREATEMERCENARY(MERC17_C4_4)
	CREATEMERCENARY(MERC18_C4_4)	
	
	//Pickups
	CREATE_HEALTH_PICKUPS()
	
	//VEHICLES
	VEHS[mvf_parked1].id = CREATE_VEHICLE(landstalker,<<-203.5000, -2413.4336, 5.0012>>, 293.3041)
	VEHS[mvf_parked2].id = CREATE_VEHICLE(landstalker,<<-180.8568, -2431.3264, 5.0013>>, 94.2693)
	SET_VEHICLE_COLOUR_COMBINATION(VEHS[mvf_parked1].ID,0)
	SET_VEHICLE_COLOUR_COMBINATION(VEHS[mvf_parked2].ID,0)
	SET_VEHICLE_CAN_LEAK_PETROL(VEHS[mvf_parked1].ID,TRUE)
	SET_VEHICLE_CAN_LEAK_PETROL(VEHS[mvf_parked2].ID,TRUE)
	
	//MISC		
	HOTSWAP_MENU(FALSE,FALSE,TRUE)					
	BHOTSWAP = TRUE
	
	//COVER
	CREATE_SCRIPTED_COVER()
	
	//SET UP CRATES 
	OBJCOV1 =  CREATE_OBJECT (PROP_CRATEPILE_07A, <<-215.770004,-2392.330078,5>>)
	SET_ENTITY_ROTATION(OBJCOV1, <<-0.000000,0.000000,-29.999996>>)
	SET_ENTITY_LOD_DIST( OBJCOV1, 200 )
	OBJCOV2 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-211.029999,-2388.360107,5>>)
	SET_ENTITY_ROTATION (OBJCOV2, <<-0.000000,0.000000,-12.000000>>)
	SET_ENTITY_LOD_DIST( OBJCOV2, 200 )
	OBJCOV3 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-212.509995,-2387.169922,5>>)
	SET_ENTITY_ROTATION (OBJCOV3, <<-0.000000,0.000000,-39.999992>> )
	SET_ENTITY_LOD_DIST( OBJCOV3, 200 )
	OBJCOV4 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-184.7490, -2422.6982, 5.0013>>)
	SET_ENTITY_ROTATION (OBJCOV4, <<0.0,0.0,8.3525>> )
	SET_ENTITY_LOD_DIST( OBJCOV4, 200 )
	OBJCOV5 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-193.7419, -2422.9417, 5.0007>>)
	SET_ENTITY_ROTATION (OBJCOV5, <<0.0,0.0,220.8658>> )
	SET_ENTITY_LOD_DIST( OBJCOV5, 200 )
	
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"STAGE 2: FIRST BOMB")			
	TRIGGER_MUSIC_EVENT("DH2A_1ST_BOMB_RT")	
	GIVE_MISSION_WEAPONS()
//		do_stream_vol()
	
	CPRINTLN(DEBUG_MISSION, "ready to END_REPLAY_SETUP" )
	END_REPLAY_SETUP()
	
	IF g_replay.iReplayInt[WHICH_PLAYER] = PLAYER_IS_FRANK
		IF MAKE_SELECTOR_PED_SELECTION( sSelectorPeds, SELECTOR_PED_FRANKLIN )
			TAKE_CONTROL_OF_SELECTOR_PED( sSelectorPeds, TRUE, TRUE )
			IF isentityalive(mike_ped())
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mike_ped(), TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF PLAYER_PED_ID() = frank_ped()
		SET_PLAYER_FORCED_AIM( PLAYER_ID(), TRUE )
		SET_PED_NO_TIME_DELAY_BEFORE_SHOT( PLAYER_PED_ID() )
		bfirstaim = TRUE
	ELSE
		SPECIAL_ABILITY_FILL_METER(PLAYER_ID(),TRUE)
		bFilledSpecialMeter = TRUE
	ENDIF
	
	IF PLAYER_PED_ID() = mike_ped()
		START_AUDIO_SCENE("DH2A_PLANT_BOMBS_SCENE")
	ELSE
		START_AUDIO_SCENE("DH2A_SNIPE_GUARDS_scene")
	ENDIF	
	
ENDPROC
PROC SU_3_SECOND_BOMB()
	
	//IPL
	MANAGE_IPLS(BUILDINGSTATE_NORMAL)
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		IF SET_CURRENT_PLAYER_PED(SELECTOR_PED_MICHAEL,TRUE)
			//MIKE		
			SET_ENTITY_COORDS(MIKE_PED(), <<-94.9569, -2371.8770, 13.2969>>)
			SET_ENTITY_HEADING(MIKE_PED(),97.3018)
			while not NEW_LOAD_SCENE_START_SPHERE(C4[C4_0_FRONT_OF_SHIP].EXPPOS,15,NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
				wait(0)
			endwhile
			while not IS_NEW_LOAD_SCENE_LOADED()
				wait(0)
			endwhile
			NEW_LOAD_SCENE_STOP()
			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(PEDS[MPF_FRANK].ID,CHAR_FRANKLIN,<< -143.8203, -2488.2612, 43.4412 >>, 10.7369)
				WAIT(0)
			ENDWHILE
			ADD_PED_FOR_DIALOGUE(CONVO_STRUCT,3,PEDS[MPF_FRANK].ID,"FRANKLIN")	
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( PEDS[MPF_FRANK].ID, TRUE )
		ENDIF
	ELSE
		WHILE NOT CREATE_PLAYER_PED_ON_FOOT(PEDS[MPF_FRANK].ID,CHAR_FRANKLIN,<< -143.8203, -2488.2612, 43.4412 >>, 10.7369)
			WAIT(0)
		ENDWHILE
		sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = PEDS[MPF_FRANK].ID
		ADD_PED_FOR_DIALOGUE(CONVO_STRUCT,3,PEDS[MPF_FRANK].ID,"FRANKLIN")	
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( PEDS[MPF_FRANK].ID, TRUE )
	ENDIF
	
	PRELOAD_OUTFIT( MIKE_PED(), OUTFIT_P0_SCUBA_LAND )
	WHILE NOT HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(MIKE_PED())
	AND NOT HAS_PED_PRELOAD_PROP_DATA_FINISHED(MIKE_PED())
		WAIT(0)
	ENDWHILE
		
	SCUBA_SUITE_UP(MIKE_PED(), FALSE, FALSE)
	
	WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(MIKE_PED())
		WAIT(0)
	ENDWHILE
	
	RELEASE_PED_PRELOAD_VARIATION_DATA(MIKE_PED())
	RELEASE_PED_PRELOAD_PROP_DATA(MIKE_PED())
	
	SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED(), COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	SET_PED_STEALTH_MOVEMENT(mike_ped(),true)
	
	//dinghy
	vehs[mvf_dinghy].id = CREATE_VEHICLE(dinghy,<<-86.7, -2353.2, 0>>, 270.3750)
	SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_dinghy].id,0)
	SET_VEHICLE_RADIO_ENABLED( vehs[mvf_dinghy].id, FALSE )	
	//MISC
	HOTSWAP_MENU(FALSE,FALSE,TRUE)					
	BHOTSWAP = TRUE
	C4[C4_0_FRONT_OF_SHIP].BPLANTED = TRUE
	
	CREATEMERCENARY(MERC4_C4_2)
	//CREATE PATROLLING GUARDS
	CREATEMERCENARY(MERC3_C4_2)
	CREATEMERCENARY(MERC5_C4_2)
//	CREATEMERCENARY(MERC6_C4_2)		//@RJP B*1321797 - removing patrols of two guys
				
	//SECOND MAST STAGE
//	CREATEMERCENARY(MERC7_C4_2)		// WILL FOLLOW WAYPOINT	
//	CREATEMERCENARY(MERC8_C4_2)
	CREATEMERCENARY(MERC12_C4_3)		
	CREATEMERCENARY(MERC9_C4_3)
//	CREATEMERCENARY(MERC10_C4_3,1)	//@RJP B*1321797 - removing patrols of two guys
	CREATEMERCENARY(MERC11_C4_3,1)	
	
	//TRAPPED STAGE
	CREATEMERCENARY(MERC13_C4_3)
	CREATEMERCENARY(MERC14_C4_3)
	CREATEMERCENARY(MERC16_C4_4)
//	CREATEMERCENARY(MERC15_C4_3)	//@RJP B*1321797 - removing patrols of two guys
//	CREATEMERCENARY(MERC17_C4_4)
	CREATEMERCENARY(MERC18_C4_4)			
		
	SET_PICKUP_GENERATION_RANGE_MULTIPLIER(2.0)
	
	iStairNavMeshBlocker[0] = ADD_NAVMESH_BLOCKING_OBJECT(<<-135.905899,-2383.557861,5.000676>>,<<1.500000,1.500000,1.500000>>,0)
	iStairNavMeshBlocker[1] = ADD_NAVMESH_BLOCKING_OBJECT(<<-214.905899,-2383.557861,5.000676>>,<<1.500000,1.500000,1.500000>>,0)
	
	//Pickups
	CREATE_HEALTH_PICKUPS()
	
	//VEHICLES
	VEHS[mvf_parked1].id = CREATE_VEHICLE(landstalker,<<-203.5000, -2413.4336, 5.0012>>, 293.3041)
	VEHS[mvf_parked2].id = CREATE_VEHICLE(landstalker,<<-180.8568, -2431.3264, 5.0013>>, 94.2693)
	SET_VEHICLE_COLOUR_COMBINATION(VEHS[mvf_parked1].ID,0)
	SET_VEHICLE_COLOUR_COMBINATION(VEHS[mvf_parked2].ID,0)
	SET_VEHICLE_CAN_LEAK_PETROL(VEHS[mvf_parked1].ID,TRUE)
	SET_VEHICLE_CAN_LEAK_PETROL(VEHS[mvf_parked2].ID,TRUE)
	
	//SET UP CRATES 
	OBJCOV1 =  CREATE_OBJECT (PROP_CRATEPILE_07A, <<-215.770004,-2392.330078,5>>)
	SET_ENTITY_ROTATION(OBJCOV1, <<-0.000000,0.000000,-29.999996>>)
	SET_ENTITY_LOD_DIST( OBJCOV1, 200 )
	OBJCOV2 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-211.029999,-2388.360107,5>>)
	SET_ENTITY_ROTATION (OBJCOV2, <<-0.000000,0.000000,-12.000000>>)
	SET_ENTITY_LOD_DIST( OBJCOV2, 200 )
	OBJCOV3 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-212.509995,-2387.169922,5>>)
	SET_ENTITY_ROTATION (OBJCOV3, <<-0.000000,0.000000,-39.999992>> )
	SET_ENTITY_LOD_DIST( OBJCOV3, 200 )
	OBJCOV4 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-184.7490, -2422.6982, 5.0013>>)
	SET_ENTITY_ROTATION (OBJCOV4, <<0.0,0.0,8.3525>> )
	SET_ENTITY_LOD_DIST( OBJCOV4, 200 )
	
	//COVER
	CREATE_SCRIPTED_COVER()
	
	TRIGGER_MUSIC_EVENT("DH2A_2ND_BOMB_RT")

	GIVE_MISSION_WEAPONS()
	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3,"STAGE 3: SECOND BOMB")
//	do_stream_vol()
	
	CREATE_PROXY_BOMBS(1)
	
	CPRINTLN(DEBUG_MISSION, "ready to END_REPLAY_SETUP" )
	END_REPLAY_SETUP()
	
	IF g_replay.iReplayInt[WHICH_PLAYER] = PLAYER_IS_FRANK
		IF MAKE_SELECTOR_PED_SELECTION( sSelectorPeds, SELECTOR_PED_FRANKLIN )
			TAKE_CONTROL_OF_SELECTOR_PED( sSelectorPeds, TRUE, TRUE )
			IF isentityalive(mike_ped())
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mike_ped(), TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF PLAYER_PED_ID() = frank_ped()
		SET_PLAYER_FORCED_AIM( PLAYER_ID(), TRUE )
		SET_PED_NO_TIME_DELAY_BEFORE_SHOT( PLAYER_PED_ID() )
		bfirstaim = TRUE
	ELSE
		IF isentityalive(frank_ped())
			TASK_AIM_GUN_AT_ENTITY( frank_ped(), mike_ped(), -1 )
		ENDIF
		SPECIAL_ABILITY_FILL_METER(PLAYER_ID(),TRUE)
		bFilledSpecialMeter = TRUE
	ENDIF

	IF PLAYER_PED_ID() = mike_ped()
		START_AUDIO_SCENE("DH2A_PLANT_BOMBS_SCENE")
	ELSE
		START_AUDIO_SCENE("DH2A_SNIPING_scene")
	ENDIF

ENDPROC
PROC SU_4_THIRD_BOMB()

	//IPL
	MANAGE_IPLS(BUILDINGSTATE_NORMAL)
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		IF SET_CURRENT_PLAYER_PED(SELECTOR_PED_MICHAEL,TRUE)
			SET_ENTITY_COORDS(MIKE_PED(),<< -129.6871, -2366.8972, 8.3193 >>)
			SET_ENTITY_HEADING(MIKE_PED(), 183.9491)
			
			while not NEW_LOAD_SCENE_START_SPHERE(C4[C4_1_first_mast].EXPPOS,15,NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
				wait(0)
			endwhile
			while not IS_NEW_LOAD_SCENE_LOADED()
				wait(0)
			endwhile
			NEW_LOAD_SCENE_STOP()
			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(PEDS[MPF_FRANK].ID,CHAR_FRANKLIN,<< -143.8203, -2488.2612, 43.4412 >>, 10.7369)
				WAIT(0)
			ENDWHILE
			ADD_PED_FOR_DIALOGUE(CONVO_STRUCT,3,PEDS[MPF_FRANK].ID,"FRANKLIN")	
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( PEDS[MPF_FRANK].ID, TRUE )
		ENDIF
	ELSE
		WHILE NOT CREATE_PLAYER_PED_ON_FOOT(PEDS[MPF_FRANK].ID,CHAR_FRANKLIN,<< -143.8203, -2488.2612, 43.4412 >>, 10.7369)
			WAIT(0)
		ENDWHILE
		sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = PEDS[MPF_FRANK].ID
		ADD_PED_FOR_DIALOGUE(CONVO_STRUCT,3,PEDS[MPF_FRANK].ID,"FRANKLIN")	
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( PEDS[MPF_FRANK].ID, TRUE )
	ENDIF
	
	PRELOAD_OUTFIT( MIKE_PED(), OUTFIT_P0_SCUBA_LAND )
	WHILE NOT HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(MIKE_PED())
	AND NOT HAS_PED_PRELOAD_PROP_DATA_FINISHED(MIKE_PED())
		WAIT(0)
	ENDWHILE
	
	SCUBA_SUITE_UP(MIKE_PED(), FALSE, FALSE)
	
	WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(MIKE_PED())
		WAIT(0)
	ENDWHILE
	
	RELEASE_PED_PRELOAD_VARIATION_DATA(MIKE_PED())
	RELEASE_PED_PRELOAD_PROP_DATA(MIKE_PED())
	
	SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	SET_PED_STEALTH_MOVEMENT(mike_ped(),true)
	
	//dinghy
	vehs[mvf_dinghy].id = CREATE_VEHICLE(dinghy,<<-86.7, -2353.2, 0>>, 270.3750)
	SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_dinghy].id,0)
	SET_VEHICLE_RADIO_ENABLED( vehs[mvf_dinghy].id, FALSE )	
	// PREVIOUS MERCS DEAD ALREADY NO NEED FOR DIALOGUE
	MERCS[MERC3_C4_2].BALIVE = FALSE
	MERCS[MERC4_C4_2].BALIVE = FALSE
	MERCS[MERC5_C4_2].BALIVE = FALSE
	MERCS[MERC6_C4_2].BALIVE = FALSE
	
	MERCS[MERC7_C4_2].BALIVE = FALSE
	
	//SECOND MAST STAGE
	//@RJP merc7 is now right next to the bomb site, don't spawn him in this stage
//	CREATEMERCENARY(MERC7_C4_2)		// WILL FOLLOW WAYPOINT	
//	CREATEMERCENARY(MERC8_C4_2)
	CREATEMERCENARY(MERC12_C4_3)	
	CREATEMERCENARY(MERC9_C4_3)
//	CREATEMERCENARY(MERC10_C4_3,1)	//@RJP B*1321797 - removing patrols of two guys
	CREATEMERCENARY(MERC11_C4_3,1)
	
	//TRAPPED STAGE
	CREATEMERCENARY(MERC13_C4_3)
	CREATEMERCENARY(MERC14_C4_3)
	CREATEMERCENARY(MERC16_C4_4)
//	CREATEMERCENARY(MERC15_C4_3)	//@RJP B*1321797 - removing patrols of two guys
//	CREATEMERCENARY(MERC17_C4_4)
	CREATEMERCENARY(MERC18_C4_4)
	
	SET_PICKUP_GENERATION_RANGE_MULTIPLIER(2.0)
	
	iStairNavMeshBlocker[0] = ADD_NAVMESH_BLOCKING_OBJECT(<<-135.905899,-2383.557861,5.000676>>,<<1.500000,1.500000,1.500000>>,0)
	iStairNavMeshBlocker[1] = ADD_NAVMESH_BLOCKING_OBJECT(<<-214.905899,-2383.557861,5.000676>>,<<1.500000,1.500000,1.500000>>,0)
	
	//Pickups
	CREATE_HEALTH_PICKUPS()
	
	//VEHICLES
	VEHS[mvf_parked1].id = CREATE_VEHICLE(landstalker,<<-203.5000, -2413.4336, 5.0012>>, 293.3041)
	VEHS[mvf_parked2].id = CREATE_VEHICLE(landstalker,<<-180.8568, -2431.3264, 5.0013>>, 94.2693)
	SET_VEHICLE_COLOUR_COMBINATION(VEHS[mvf_parked1].ID,0)
	SET_VEHICLE_COLOUR_COMBINATION(VEHS[mvf_parked2].ID,0)
	SET_VEHICLE_CAN_LEAK_PETROL(VEHS[mvf_parked1].ID,TRUE)
	SET_VEHICLE_CAN_LEAK_PETROL(VEHS[mvf_parked2].ID,TRUE)
	
	//MISC
	HOTSWAP_MENU(FALSE,FALSE,TRUE)					
	BHOTSWAP = TRUE
	C4[C4_0_FRONT_OF_SHIP].BPLANTED = TRUE
	C4[C4_1_FIRST_MAST].BPLANTED = TRUE		
	
	//SET UP CRATES 
	OBJCOV1 =  CREATE_OBJECT (PROP_CRATEPILE_07A, <<-215.770004,-2392.330078,5>>)
	SET_ENTITY_ROTATION(OBJCOV1, <<-0.000000,0.000000,-29.999996>>)
	SET_ENTITY_LOD_DIST( OBJCOV1, 200 )
	OBJCOV2 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-211.029999,-2388.360107,5>>)
	SET_ENTITY_ROTATION (OBJCOV2, <<-0.000000,0.000000,-12.000000>>)
	SET_ENTITY_LOD_DIST( OBJCOV2, 200 )
	OBJCOV3 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-212.509995,-2387.169922,5>>)
	SET_ENTITY_ROTATION (OBJCOV3, <<-0.000000,0.000000,-39.999992>> )
	SET_ENTITY_LOD_DIST( OBJCOV3, 200 )
	OBJCOV4 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-184.7490, -2422.6982, 5.0013>>)
	SET_ENTITY_ROTATION (OBJCOV4, <<0.0,0.0,8.3525>> )
	SET_ENTITY_LOD_DIST( OBJCOV4, 200 )
	OBJCOV5 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-193.7419, -2422.9417, 5.0007>>)
	SET_ENTITY_ROTATION (OBJCOV5, <<0.0,0.0,220.8658>> )
	SET_ENTITY_LOD_DIST( OBJCOV5, 200 )
	
	//COVER
	CREATE_SCRIPTED_COVER()
	
	GIVE_MISSION_WEAPONS() 
	
	TRIGGER_MUSIC_EVENT("DH2A_3RD_BOMB_RT")

	SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4,"STAGE 3: THIRD BOMB")
	
	CREATE_PROXY_BOMBS(2)
	
	CPRINTLN(DEBUG_MISSION, "ready to END_REPLAY_SETUP" )
	END_REPLAY_SETUP()
	
	IF g_replay.iReplayInt[WHICH_PLAYER] = PLAYER_IS_FRANK
		IF MAKE_SELECTOR_PED_SELECTION( sSelectorPeds, SELECTOR_PED_FRANKLIN )
			TAKE_CONTROL_OF_SELECTOR_PED( sSelectorPeds, TRUE, TRUE )
			IF isentityalive(mike_ped())
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mike_ped(), TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF PLAYER_PED_ID() = frank_ped()
		SET_PLAYER_FORCED_AIM( PLAYER_ID(), TRUE )
		SET_PED_NO_TIME_DELAY_BEFORE_SHOT( PLAYER_PED_ID() )
		bfirstaim = TRUE
	ELSE
		IF isentityalive(frank_ped())
			TASK_AIM_GUN_AT_ENTITY( frank_ped(), mike_ped(), -1 )
		ENDIF
		SPECIAL_ABILITY_FILL_METER(PLAYER_ID(),TRUE)
		bFilledSpecialMeter = TRUE
	ENDIF
	
	IF PLAYER_PED_ID() = mike_ped()
		START_AUDIO_SCENE("DH2A_PLANT_BOMBS_SCENE")
	ELSE
		START_AUDIO_SCENE("DH2A_SNIPING_scene")
	ENDIF	
	
//	do_stream_vol()

ENDPROC
PROC SU_5_FOURTH_BOMB()
	
	//IPL
	MANAGE_IPLS(BUILDINGSTATE_NORMAL)
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		IF SET_CURRENT_PLAYER_PED(SELECTOR_PED_MICHAEL,TRUE)
			//MIKE
			SET_ENTITY_COORDS(MIKE_PED(),<< -191.4221, -2365.4763, 8.3193 >>)
			SET_ENTITY_HEADING(MIKE_PED(), 176.1654)
			
			while not NEW_LOAD_SCENE_START_SPHERE(C4[C4_2_second_mast].EXPPOS,15,NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
				wait(0)
			endwhile
			while not IS_NEW_LOAD_SCENE_LOADED()
				wait(0)
			endwhile
			NEW_LOAD_SCENE_STOP()
			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(PEDS[MPF_FRANK].ID,CHAR_FRANKLIN,<< -143.8203, -2488.2612, 43.4412 >>, 10.7369)
				WAIT(0)
			ENDWHILE
			ADD_PED_FOR_DIALOGUE(CONVO_STRUCT,3,PEDS[MPF_FRANK].ID,"FRANKLIN")
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( PEDS[MPF_FRANK].ID, TRUE )
		ENDIF
	ELSE
		WHILE NOT CREATE_PLAYER_PED_ON_FOOT(PEDS[MPF_FRANK].ID,CHAR_FRANKLIN,<< -143.8203, -2488.2612, 43.4412 >>, 10.7369)
			WAIT(0)
		ENDWHILE
		sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = PEDS[MPF_FRANK].ID
		ADD_PED_FOR_DIALOGUE(CONVO_STRUCT,3,PEDS[MPF_FRANK].ID,"FRANKLIN")	
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( PEDS[MPF_FRANK].ID, TRUE )
	ENDIF
	
	PRELOAD_OUTFIT( MIKE_PED(), OUTFIT_P0_SCUBA_LAND )
	WHILE NOT HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(MIKE_PED())
	AND NOT HAS_PED_PRELOAD_PROP_DATA_FINISHED(MIKE_PED())
		WAIT(0)
	ENDWHILE
	
	SCUBA_SUITE_UP(MIKE_PED(), FALSE, FALSE)
	
	WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(MIKE_PED())
		WAIT(0)
	ENDWHILE
	
	RELEASE_PED_PRELOAD_VARIATION_DATA(MIKE_PED())
	RELEASE_PED_PRELOAD_PROP_DATA(MIKE_PED())
	
	SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED(), COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	ADD_PED_FOR_DIALOGUE(CONVO_STRUCT,1,PEDS[MPF_MIKE].ID,"MICHAEL")
	SET_PED_STEALTH_MOVEMENT(mike_ped(),true)
	
	//dinghy
	vehs[mvf_dinghy].id = CREATE_VEHICLE(dinghy,<<-86.7, -2353.2, 0>>, 270.3750)
	SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_dinghy].id,0)
	SET_VEHICLE_RADIO_ENABLED( vehs[mvf_dinghy].id, FALSE )	
	// PREVIOUS MERCS DEAD ALREADY NO NEED FOR DIALOGUE
	MERCS[MERC3_C4_2].BALIVE  = FALSE
	MERCS[MERC4_C4_2].BALIVE  = FALSE
	MERCS[MERC5_C4_2].BALIVE  = FALSE
	MERCS[MERC6_C4_2].BALIVE  = FALSE
	MERCS[MERC7_C4_2].BALIVE  = FALSE
	MERCS[MERC8_C4_2].BALIVE  = FALSE
	MERCS[MERC10_C4_3].BALIVE = FALSE
	MERCS[MERC11_C4_3].BALIVE = FALSE
	MERCS[MERC12_C4_3].BALIVE = FALSE
	
	MERCS[MERC14_C4_3].BALIVE = FALSE
	MERCS[MERC15_C4_3].BALIVE = FALSE
	
	CREATEMERCENARY(MERC13_C4_3)
//	CREATEMERCENARY(MERC14_C4_3)
	CREATEMERCENARY(MERC16_C4_4)
//	CREATEMERCENARY(MERC15_C4_3)
//	CREATEMERCENARY(MERC17_C4_4)
	CREATEMERCENARY(MERC18_C4_4)
	
	SET_PICKUP_GENERATION_RANGE_MULTIPLIER(2.0)
	
	iStairNavMeshBlocker[0] = ADD_NAVMESH_BLOCKING_OBJECT(<<-135.905899,-2383.557861,5.000676>>,<<1.500000,1.500000,1.500000>>,0)
	iStairNavMeshBlocker[1] = ADD_NAVMESH_BLOCKING_OBJECT(<<-214.905899,-2383.557861,5.000676>>,<<1.500000,1.500000,1.500000>>,0)
	
	//Pickups
	CREATE_HEALTH_PICKUPS()
	
	//VEHICLES
	VEHS[mvf_parked1].id = CREATE_VEHICLE(landstalker,<<-203.5000, -2413.4336, 5.0012>>, 293.3041)
	VEHS[mvf_parked2].id = CREATE_VEHICLE(landstalker,<<-180.8568, -2431.3264, 5.0013>>, 94.2693)
	SET_VEHICLE_COLOUR_COMBINATION(VEHS[mvf_parked1].ID,0)
	SET_VEHICLE_COLOUR_COMBINATION(VEHS[mvf_parked2].ID,0)
	SET_VEHICLE_CAN_LEAK_PETROL(VEHS[mvf_parked1].ID,TRUE)
	SET_VEHICLE_CAN_LEAK_PETROL(VEHS[mvf_parked2].ID,TRUE)
	
	//MISC
	HOTSWAP_MENU(FALSE,FALSE,TRUE)	
	BHOTSWAP = TRUE
	
	C4[C4_0_FRONT_OF_SHIP].BPLANTED = TRUE
	C4[C4_1_FIRST_MAST].BPLANTED 	= TRUE
	C4[C4_2_SECOND_MAST].BPLANTED 	= TRUE
	
	IMIKE_AI_STAGE = 0
	
	//SET UP CRATES 
	OBJCOV1 =  CREATE_OBJECT (PROP_CRATEPILE_07A, <<-215.770004,-2392.330078,5>>)
	SET_ENTITY_ROTATION(OBJCOV1, <<-0.000000,0.000000,-29.999996>>)
	SET_ENTITY_LOD_DIST( OBJCOV1, 200 )
	OBJCOV2 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-211.029999,-2388.360107,5>>)
	SET_ENTITY_ROTATION (OBJCOV2, <<-0.000000,0.000000,-12.000000>>)
	SET_ENTITY_LOD_DIST( OBJCOV2, 200 )
	OBJCOV3 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-212.509995,-2387.169922,5>>)
	SET_ENTITY_ROTATION (OBJCOV3, <<-0.000000,0.000000,-39.999992>> )
	SET_ENTITY_LOD_DIST( OBJCOV3, 200 )
	OBJCOV4 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-184.7490, -2422.6982, 5.0013>>)
	SET_ENTITY_ROTATION (OBJCOV4, <<0.0,0.0,8.3525>> )
	SET_ENTITY_LOD_DIST( OBJCOV4, 200 )
	OBJCOV5 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-193.7419, -2422.9417, 5.0007>>)
	SET_ENTITY_ROTATION (OBJCOV5, <<0.0,0.0,220.8658>> )
	SET_ENTITY_LOD_DIST( OBJCOV5, 200 )
	
	//COVER
	CREATE_SCRIPTED_COVER()
	
	give_mission_weapons()
	
//	TRIGGER_MUSIC_EVENT("DH2A_3RD_BOMB_PLANTED")
	TRIGGER_MUSIC_EVENT("DH2A_4TH_BOMB_RT")
	
	CREATE_PROXY_BOMBS(3)
	
	CPRINTLN(DEBUG_MISSION, "ready to END_REPLAY_SETUP" )
	END_REPLAY_SETUP()
	
	IF g_replay.iReplayInt[WHICH_PLAYER] = PLAYER_IS_FRANK
		IF MAKE_SELECTOR_PED_SELECTION( sSelectorPeds, SELECTOR_PED_FRANKLIN )
			TAKE_CONTROL_OF_SELECTOR_PED( sSelectorPeds, TRUE, TRUE )
			IF isentityalive(mike_ped())
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mike_ped(), TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF PLAYER_PED_ID() = frank_ped()
		SET_PLAYER_FORCED_AIM( PLAYER_ID(), TRUE )
		SET_PED_NO_TIME_DELAY_BEFORE_SHOT( PLAYER_PED_ID() )
		bfirstaim = TRUE
	ELSE
		IF isentityalive(frank_ped())
			TASK_AIM_GUN_AT_ENTITY( frank_ped(), mike_ped(), -1 )
		ENDIF
		SPECIAL_ABILITY_FILL_METER(PLAYER_ID(),TRUE)
		bFilledSpecialMeter = TRUE
	ENDIF
	
	IF PLAYER_PED_ID() = mike_ped()
		START_AUDIO_SCENE("DH2A_PLANT_BOMBS_SCENE")
	ELSE
		START_AUDIO_SCENE("DH2A_SNIPING_scene")
	ENDIF	

ENDPROC
PROC SU_6_WAY_OUT()
	//PLAYER
	IF SET_CURRENT_PLAYER_PED(SELECTOR_PED_FRANKLIN,TRUE)
		//IPL
		MANAGE_IPLS(BUILDINGSTATE_NORMAL)
		
		IF NOT IS_REPLAY_BEING_SET_UP()
			//frank		
			SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -143.8203, -2488.2612, 43.4412 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 35.7369)
			
			while not NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(player_ped_id()),10,NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
				wait(0)
			endwhile
			while not IS_NEW_LOAD_SCENE_LOADED()
				wait(0)
			endwhile
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		//dinghy
		vehs[mvf_dinghy].id = CREATE_VEHICLE(dinghy,<<-86.7, -2353.2, 0>>, 270.3750)
		SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_dinghy].id,0)
		SET_VEHICLE_RADIO_ENABLED( vehs[mvf_dinghy].id, FALSE )	
		
		//MERCS
		MERCS[MERC3_C4_2].BALIVE  = FALSE
		MERCS[MERC4_C4_2].BALIVE  = FALSE
		MERCS[MERC5_C4_2].BALIVE  = FALSE		
		MERCS[MERC7_C4_2].BALIVE  = FALSE
		MERCS[MERC8_C4_2].BALIVE  = FALSE
		MERCS[MERC10_C4_3].BALIVE = FALSE
		MERCS[MERC11_C4_3].BALIVE = FALSE
		MERCS[MERC12_C4_3].BALIVE = FALSE
		
//		CREATEMERCENARY(merc27_cars,150,TRUE)
//		CREATEMERCENARY(merc28_cars,150,TRUE)	
		
		SET_PICKUP_GENERATION_RANGE_MULTIPLIER(2.0)
		
		//VEHICLES
		VEHS[mvf_parked1].id = CREATE_VEHICLE(landstalker,<<-203.5000, -2413.4336, 5.0012>>, 293.3041)
		VEHS[mvf_parked2].id = CREATE_VEHICLE(landstalker,<<-180.8568, -2431.3264, 5.0013>>, 94.2693)
		SET_VEHICLE_COLOUR_COMBINATION(VEHS[mvf_parked1].ID,0)
		SET_VEHICLE_COLOUR_COMBINATION(VEHS[mvf_parked2].ID,0)
		SET_VEHICLE_CAN_LEAK_PETROL(VEHS[mvf_parked1].ID,TRUE)
		SET_VEHICLE_CAN_LEAK_PETROL(VEHS[mvf_parked2].ID,TRUE)
		
		//SET UP CRATES 
		OBJCOV1 =  CREATE_OBJECT (PROP_CRATEPILE_07A, <<-215.770004,-2392.330078,5>>)
		SET_ENTITY_ROTATION(OBJCOV1, <<-0.000000,0.000000,-29.999996>>)
		SET_ENTITY_LOD_DIST( OBJCOV1, 200 )
		OBJCOV2 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-211.029999,-2388.360107,5>>)
		SET_ENTITY_ROTATION (OBJCOV2, <<-0.000000,0.000000,-12.000000>>)
		SET_ENTITY_LOD_DIST( OBJCOV2, 200 )
		OBJCOV3 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-212.509995,-2387.169922,5>>)
		SET_ENTITY_ROTATION (OBJCOV3, <<-0.000000,0.000000,-39.999992>> )
		SET_ENTITY_LOD_DIST( OBJCOV3, 200 )
		OBJCOV4 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-184.7490, -2422.6982, 5.0013>>)
		SET_ENTITY_ROTATION (OBJCOV4, <<0.0,0.0,8.3525>> )
		SET_ENTITY_LOD_DIST( OBJCOV4, 200 )
		OBJCOV5 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-193.7419, -2422.9417, 5.0007>>)
		SET_ENTITY_ROTATION (OBJCOV5, <<0.0,0.0,220.8658>> )
		SET_ENTITY_LOD_DIST( OBJCOV5, 200 )
		
		CREATE_PROXY_BOMBS(3)
		
		GIVE_MISSION_WEAPONS() 
		HOTSWAP_MENU(FALSE,FALSE,TRUE)					
		TRIGGER_MUSIC_EVENT("DH2A_WAY_OUT_RT")
		START_AUDIO_SCENE("DH2A_SHOOTOUT_SNIPING_SCENE")
		
		CPRINTLN(DEBUG_MISSION, "ready to END_REPLAY_SETUP" )
		END_REPLAY_SETUP()
		
		SET_ENTITY_COORDS( PLAYER_PED_ID(), << -143.8203, -2488.2612, 43.4412 >> )
//		SET_ENTITY_HEADING( PLAYER_PED_ID(), 35.7369 )
		
//		NEW_LOAD_SCENE_START_SPHERE( <<-221.8633, -2376.6187, 12.3329>>, 50 )
		
		SET_PLAYER_FORCED_AIM( PLAYER_ID(), TRUE )
		SET_PED_NO_TIME_DELAY_BEFORE_SHOT( PLAYER_PED_ID() )
		balarmtriggered = TRUE
		bAimIntro = TRUE
	ELSE
		WAIT(0)
	ENDIF
ENDPROC
PROC SU_7_DETONATE()
	//PLAYER
	IF SET_CURRENT_PLAYER_PED(SELECTOR_PED_FRANKLIN,TRUE)
		//IPL
		MANAGE_IPLS(BUILDINGSTATE_NORMAL)
		
		IF NOT IS_REPLAY_BEING_SET_UP()
			//FRANK
			SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -143.8203, -2488.2612, 43.4412 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 10.7369)
			
			while not NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(player_ped_id()),10,NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
				wait(0)
			endwhile
			while not IS_NEW_LOAD_SCENE_LOADED()
				wait(0)
			endwhile
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		//dinghy
		vehs[mvf_dinghy].id = CREATE_VEHICLE(dinghy,<<-80.2056, -2288.2832,0>>, 22)
		SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_dinghy].id,0)
		SET_VEHICLE_RADIO_ENABLED( vehs[mvf_dinghy].id, FALSE )	
		
		//FRANK
		WHILE NOT CREATE_PLAYER_PED_ON_FOOT(PEDS[MPF_MIKE].ID,CHAR_MICHAEL,<< -228.9973, -2283.4854, 3.3515 >>,10)
			WAIT(0)
		ENDWHILE
		
		ADD_PED_FOR_DIALOGUE(CONVO_STRUCT,1,PEDS[MPF_MIKE].ID,"MICHAEL")	
		SCUBA_SUITE_UP(MIKE_PED(),TRUE)
		SET_ENABLE_SCUBA(MIKE_PED(),TRUE)
		SET_PED_DIES_IN_WATER(MIKE_PED(),FALSE)
		//MERC
		MERCS[MERC3_C4_2].BALIVE  = FALSE
		MERCS[MERC4_C4_2].BALIVE  = FALSE
		MERCS[MERC5_C4_2].BALIVE  = FALSE		
		MERCS[MERC7_C4_2].BALIVE  = FALSE
		MERCS[MERC8_C4_2].BALIVE  = FALSE
		MERCS[MERC10_C4_3].BALIVE = FALSE
		MERCS[MERC11_C4_3].BALIVE = FALSE
		MERCS[MERC12_C4_3].BALIVE = FALSE
		
		//VEHICLES
		VEHS[mvf_parked1].id = CREATE_VEHICLE(landstalker,<<-203.5000, -2413.4336, 5.0012>>, 293.3041)
		VEHS[mvf_parked2].id = CREATE_VEHICLE(landstalker,<<-180.8568, -2431.3264, 5.0013>>, 94.2693)
		SET_VEHICLE_COLOUR_COMBINATION(VEHS[mvf_parked1].ID,0)
		SET_VEHICLE_COLOUR_COMBINATION(VEHS[mvf_parked2].ID,0)
		SET_VEHICLE_CAN_LEAK_PETROL(VEHS[mvf_parked1].ID,TRUE)
		SET_VEHICLE_CAN_LEAK_PETROL(VEHS[mvf_parked2].ID,TRUE)
		
		VEHS[MVF_MERC1].ID = CREATE_VEHICLE(landstalker,<<-198.2650, -2404.2424, 5.0013>>, 127.4752)
		VEHS[MVF_MERC2].ID = CREATE_VEHICLE(landstalker,<<-210.8442, -2406.9985, 5.0012>>, 297.9603)
		SET_VEHICLE_COLOUR_COMBINATION(VEHS[MVF_MERC1].ID,0)
		SET_VEHICLE_COLOUR_COMBINATION(VEHS[MVF_MERC2].ID,0)
		SET_VEHICLE_CAN_LEAK_PETROL(VEHS[MVF_MERC1].ID,TRUE)
		SET_VEHICLE_CAN_LEAK_PETROL(VEHS[MVF_MERC2].ID,TRUE)
		
		//SET UP CRATES 
		OBJCOV1 =  CREATE_OBJECT (PROP_CRATEPILE_07A, <<-215.770004,-2392.330078,5>>)
		SET_ENTITY_ROTATION(OBJCOV1, <<-0.000000,0.000000,-29.999996>>)
		SET_ENTITY_LOD_DIST( OBJCOV1, 200 )
		OBJCOV2 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-211.029999,-2388.360107,5>>)
		SET_ENTITY_ROTATION (OBJCOV2, <<-0.000000,0.000000,-12.000000>>)
		SET_ENTITY_LOD_DIST( OBJCOV2, 200 )
		OBJCOV3 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-212.509995,-2387.169922,5>>)
		SET_ENTITY_ROTATION (OBJCOV3, <<-0.000000,0.000000,-39.999992>> )
		SET_ENTITY_LOD_DIST( OBJCOV3, 200 )
		OBJCOV4 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-184.7490, -2422.6982, 5.0013>>)
		SET_ENTITY_ROTATION (OBJCOV4, <<0.0,0.0,8.3525>> )
		SET_ENTITY_LOD_DIST( OBJCOV4, 200 )
		OBJCOV5 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-193.7419, -2422.9417, 5.0007>>)
		SET_ENTITY_ROTATION (OBJCOV5, <<0.0,0.0,220.8658>> )
		SET_ENTITY_LOD_DIST( OBJCOV5, 200 )
		
		GIVE_MISSION_WEAPONS()
		HOTSWAP_MENU(FALSE,FALSE,TRUE)
		TRIGGER_MUSIC_EVENT("DH2A_DETONATE_RT")
		
		CREATE_PROXY_BOMBS(3)
		
		CPRINTLN(DEBUG_MISSION, "ready to END_REPLAY_SETUP" )
		END_REPLAY_SETUP()
		
		balarmtriggered = TRUE
		
	ELSE
		WAIT(0)		
	ENDIF
ENDPROC
PROC SU_8_EXPLOSION_CTSCNE()
	//PLAYER
	IF SET_CURRENT_PLAYER_PED(SELECTOR_PED_MICHAEL,TRUE)
		//IPL
		MANAGE_IPLS(BUILDINGSTATE_NORMAL)
		
		IF NOT IS_REPLAY_BEING_SET_UP()
			SET_ENTITY_COORDS(MIKE_PED(),<<-80.2056, -2288.2832,0>>)
			SET_ENTITY_HEADING(MIKE_PED(), 22)
			
			while not NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(player_ped_id()),10,NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
				wait(0)
			endwhile
			while not IS_NEW_LOAD_SCENE_LOADED()
				wait(0)
			endwhile
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		WHILE NOT CREATE_PLAYER_PED_ON_FOOT(PEDS[MPF_FRANK].ID,CHAR_FRANKLIN,<< -143.8203, -2488.2612, 43.4412 >>, 10.7369)
			WAIT(0)
		ENDWHILE
		ADD_PED_FOR_DIALOGUE(CONVO_STRUCT,3,PEDS[MPF_FRANK].ID,"FRANKLIN")
		
		//dinghy
		vehs[mvf_dinghy].id = CREATE_VEHICLE(dinghy,<<-80.2056, -2288.2832,0>>, 22)
		SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_dinghy].id,0)
		SET_VEHICLE_RADIO_ENABLED( vehs[mvf_dinghy].id, FALSE )	
		//VEHICLES
		VEHS[MVF_MERC1].ID = CREATE_VEHICLE(landstalker,<<-198.2650, -2404.2424, 5.0013>>, 127.4752)
		VEHS[MVF_MERC2].ID = CREATE_VEHICLE(landstalker,<<-210.8442, -2406.9985, 5.0012>>, 297.9603)
		SET_VEHICLE_COLOUR_COMBINATION(VEHS[MVF_MERC1].ID,0)
		SET_VEHICLE_COLOUR_COMBINATION(VEHS[MVF_MERC2].ID,0)
		SET_VEHICLE_CAN_LEAK_PETROL(VEHS[MVF_MERC1].ID,TRUE)
		SET_VEHICLE_CAN_LEAK_PETROL(VEHS[MVF_MERC2].ID,TRUE)

		//MISC
		HOTSWAP_MENU(FALSE,FALSE,TRUE)	
		REQUEST_CUTSCENE("LSDH_2A_RF_01")
		
		PRELOAD_OUTFIT( MIKE_PED(), OUTFIT_P0_SCUBA_WATER )
		WHILE NOT HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(MIKE_PED())
		AND NOT HAS_PED_PRELOAD_PROP_DATA_FINISHED(MIKE_PED())
			WAIT(0)
		ENDWHILE
		
		SCUBA_SUITE_UP(MIKE_PED(),TRUE)
		
		RELEASE_PED_PRELOAD_VARIATION_DATA(MIKE_PED())
		RELEASE_PED_PRELOAD_PROP_DATA(MIKE_PED())
		
		SET_ENABLE_SCUBA(MIKE_PED(),TRUE)
		SET_PED_DIES_IN_WATER(MIKE_PED(),FALSE)
		
		GIVE_MISSION_WEAPONS() 
		
		CPRINTLN(DEBUG_MISSION, "ready to END_REPLAY_SETUP" )
		END_REPLAY_SETUP()
		
		BOOL bFinishedLoad = FALSE
		INT iWaitTime = (GET_GAME_TIMER()+5000)
		
		//audio		
		while not bFinishedLoad
			IF LOAD_STREAM("PORT_OF_LS_SHIP_BLOW_UP_MASTER")
				CPRINTLN( DEBUG_MISSION, "FINISHED LOADING STREAM PORT_OF_LS_SHIP_BLOW_UP_MASTER" )
				bFinishedLoad = TRUE
			ELIF GET_GAME_TIMER() > iWaitTime
				CPRINTLN( DEBUG_MISSION, "TIME'S UP, FAILED TO LOAD" )
				bFinishedLoad = TRUE
			ENDIF
			CPRINTLN( DEBUG_MISSION, "LOADING STREAM PORT_OF_LS_SHIP_BLOW_UP_MASTER" )
			wait(0)
		endwhile
	ELSE
		WAIT(0)
	ENDIF
ENDPROC
PROC SU_9_THE_GOODS()
	//PLAYER
	IF SET_CURRENT_PLAYER_PED(SELECTOR_PED_MICHAEL,TRUE)		
		//IPL
		MANAGE_IPLS(BUILDINGSTATE_DESTROYED)
		
		IF NOT IS_REPLAY_BEING_SET_UP()
			SET_ENTITY_COORDS(MIKE_PED(),<<-90.4927, -2288.0029, -1.3081>>)
			SET_ENTITY_HEADING(MIKE_PED(), 130.4705)
			
			while not NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(player_ped_id()),10,NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
				wait(0)
			endwhile
			while not IS_NEW_LOAD_SCENE_LOADED()
				wait(0)
			endwhile
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		//dinghy
		IF NOT isentityalive( vehs[mvf_dinghy].id )
			vehs[mvf_dinghy].id = CREATE_VEHICLE(dinghy,<<-78.1857, -2287.6528, 0.0>>, 56.7927)
			SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_dinghy].id,0)
		ENDIF
		SET_VEHICLE_RADIO_ENABLED( vehs[mvf_dinghy].id, FALSE )	
		
		PRELOAD_OUTFIT( MIKE_PED(), OUTFIT_P0_SCUBA_WATER )
		WHILE NOT HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(MIKE_PED())
		AND NOT HAS_PED_PRELOAD_PROP_DATA_FINISHED(MIKE_PED())
			WAIT(0)
		ENDWHILE
		
		
		SCUBA_SUITE_UP(MIKE_PED(),TRUE)
		
		WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(MIKE_PED())
			WAIT(0)
		ENDWHILE
		
		RELEASE_PED_PRELOAD_VARIATION_DATA(MIKE_PED())
		RELEASE_PED_PRELOAD_PROP_DATA(MIKE_PED())
		
		request_ptfx_asset()
		IF NOT has_ptfx_asset_loaded()
			WAIT(0)
		ENDIF
		
		CPRINTLN(DEBUG_MISSION, "ready to END_REPLAY_SETUP" )
		END_REPLAY_SETUP(vehs[mvf_dinghy].id)
		
		IF NOT IS_PED_IN_ANY_VEHICLE( mike_ped() )
		AND NOT IS_ENTITY_DEAD(vehs[mvf_dinghy].id)
			SET_PED_INTO_VEHICLE(mike_ped(),vehs[mvf_dinghy].id)
			SET_VEHICLE_ON_GROUND_PROPERLY( vehs[mvf_dinghy].id )
			SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(vehs[mvf_dinghy].id,TRUE)
			SET_BOAT_ANCHOR(vehs[mvf_dinghy].id,TRUE)
		ENDIF
		
//		GIVE_MISSION_WEAPONS() 
		HOTSWAP_MENU(TRUE,FALSE,FALSE)
		POINT_GAMEPLAY_CAM_AT_COORD(254.4721)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(45)
		TRIGGER_MUSIC_EVENT("DH2A_GOODS_RT")
		
		REQUEST_IPL( "SUNK_SHIP_FIRE" )
		
	ELSE
		WAIT(0)
	ENDIF
ENDPROC

PROC SU_10_END_CUTSCENE()
	//PLAYER
	IF SET_CURRENT_PLAYER_PED(SELECTOR_PED_TREVOR,TRUE)
	
		//IPL
		MANAGE_IPLS(BUILDINGSTATE_DESTROYED)
		
		IF NOT IS_REPLAY_BEING_SET_UP()
			SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-332.2139, -2570.9912, 5.0010>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 97.2111)
			
			while not NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(player_ped_id()),10,NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
				wait(0)
			endwhile
			while not IS_NEW_LOAD_SCENE_LOADED()
				wait(0)
			endwhile
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		vehs[mvf_sub].id = CREATE_VEHICLE( SUBMERSIBLE, <<-466.7967, -2381.3794, -12.1213>> )
		objLoot = CREATE_OBJECT(Prop_Military_Pickup_01,<<-191.12270, -2331.33813, -19.40053>>)
		
		REQUEST_CUTSCENE("LSDH_2A_EXT")
		REQUEST_SCRIPT("docks2ASubHandler")
		
		END_REPLAY_SETUP()
		
		HOTSWAP_MENU(TRUE,TRUE,FALSE)	
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
	ELSE
		WAIT(0)
	ENDIF
ENDPROC

PROC SU_PASSED()
	//PLAYER
	IF SET_CURRENT_PLAYER_PED(SELECTOR_PED_TREVOR,TRUE)
	
		//IPL
		MANAGE_IPLS(BUILDINGSTATE_DESTROYED)
		
		IF NOT IS_REPLAY_BEING_SET_UP()
			SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-332.2139, -2570.9912, 5.0010>>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 97.2111)
			
			while not NEW_LOAD_SCENE_START_SPHERE(GET_ENTITY_COORDS(player_ped_id()),10,NEWLOADSCENE_FLAG_REQUIRE_COLLISION)
				wait(0)
			endwhile
			while not IS_NEW_LOAD_SCENE_LOADED()
				wait(0)
			endwhile
			NEW_LOAD_SCENE_STOP()
		ENDIF
		
		END_REPLAY_SETUP()
		
		HOTSWAP_MENU(TRUE,TRUE,FALSE)	
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
	ELSE
		WAIT(0)
	ENDIF
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

proc mission_cleanup(bool bmissionpassed = false)
	
	REPLAY_STOP_EVENT()
	SET_RADAR_ZOOM_PRECISE(0)
	RELEASE_SCRIPT_AUDIO_BANK()
	
	IF IS_ENTITY_OK(PLAYER_PED_ID())
		STOP_PED_SPEAKING( PLAYER_PED_ID(), FALSE )
	ENDIF
	
	IF IS_GAMEPLAY_HINT_ACTIVE()
		STOP_GAMEPLAY_HINT()
	ENDIF
	
	IF IS_IPL_ACTIVE("SUNK_SHIP_FIRE")
		REMOVE_IPL("SUNK_SHIP_FIRE")
	ENDIF
	
	//restore Michael's weapons from before the mission
	if isentityalive(mike_ped())
		RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT( mike_ped() )			//if michael ped exists and is the player
	else
		IF ( GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL )				//if michael ped is the player and is dead
			IF IS_PED_INJURED(PLAYER_PED_ID())
				g_RestoreSnapshotWeaponsOnDeath = TRUE
			ENDIF
		ENDIF
		RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT_FOR_CHAR(CHAR_MICHAEL)	//if michael ped does not exists or is not the player
	endif
	
	IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rayfire_ship)
		SET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfire_ship, RFMO_STATE_RESET)
	ENDIF
	
	//Set water back to normal
	WATER_OVERRIDE_SET_STRENGTH(0)
	
	CLEAR_TIMECYCLE_MODIFIER()
	
	//-------------------peds-----------------------
//all peds are no longer needed except the player ped
	int i
	for i = 0 to enum_to_int(mpf_num_of_peds)-1
		if isentityalive(peds[i].id)
			if peds[i].id != player_ped_id()
				if DOES_ENTITY_BELONG_TO_THIS_SCRIPT( peds[i].id )
					set_ped_as_no_longer_needed(peds[i].id)
				endif
			endif
			if does_blip_exist(peds[i].blip)
				remove_blip(peds[i].blip)
			endif
		endif
	endfor	
	
	if isentityalive(player_ped_id())
		clear_ped_tasks(player_ped_id())
	endif
//--------------------mercs-----------------------
	for i = 0 to enum_to_int(mmf_num_of_mercs) -1	
		if isentityalive(mercs[i].id)				
			if does_blip_exist(mercs[i].blip)	
				remove_blip(mercs[i].blip)
			endif
			SET_PED_KEEP_TASK(mercs[i].id,true)
			if DOES_ENTITY_BELONG_TO_THIS_SCRIPT( mercs[i].id )
				set_ped_as_no_longer_needed(mercs[i].id)
			endif
		endif
	endfor
//---------------------vehs-----------------------

	for i = 0 to enum_to_int(mvf_num_of_veh)-1
		if isentityalive(vehs[i].id)		 
			if DOES_ENTITY_BELONG_TO_THIS_SCRIPT( vehs[i].id )
				set_vehicle_as_no_longer_needed(vehs[i].id)
			endif
		endif
		if does_blip_exist(vehs[i].blip)
			remove_blip(vehs[i].blip)
		endif
	endfor
//-------------------bombs-------------------------
	for i = 0 to enum_to_int(c4_num_of_bombs) - 1
		if does_blip_exist(c4[i].blip)
			remove_blip(c4[i].blip)
		endif
	endfor
	
//-------------------alarm-------------------------
	IF IS_ALARM_PLAYING( "PORT_OF_LS_HEIST_SHIP_ALARMS" )
		STOP_ALARM( "PORT_OF_LS_HEIST_SHIP_ALARMS", TRUE )
	ENDIF
	
//-------------------destroy-----------------------
	clear_prints()
	clear_focus()
	clear_help()
	destroy_all_cams()
	
	if is_cutscene_active()			
		stop_cutscene()
		remove_cutscene()			
	endif
	
	IF  IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	
	SET_PICKUP_GENERATION_RANGE_MULTIPLIER(1.0)
	
	IF IS_VALID_INTERIOR( interior_living_room )
		unpin_interior(interior_living_room)
	ENDIF
	REMOVE_ALL_COVER_BLOCKING_AREAS()
	SET_ALL_VEHICLE_GENERATORS_ACTIVE()
	remove_scenario_blocking_areas()
	CLEAR_PED_NON_CREATION_AREA()
	kill_any_conversation()	
	
	if does_blip_exist(blip_objective)
		remove_blip(blip_objective)
	endif
	if does_blip_exist(blipcrate)
		remove_blip(blipcrate)
	endif
	
	IF DOES_BLIP_EXIST(biSonarBlip)
		REMOVE_BLIP(biSonarBlip)
	ENDIF
	
	if does_entity_exist(objDoor)
		delete_object(objDoor)
	endif
	
	IF DOES_ENTITY_EXIST(objDoorHide)
		SET_ENTITY_VISIBLE(objDoorHide, TRUE)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objDoorHide)
	ENDIF
	
	IF DOES_ENTITY_EXIST( objSofa )
		SET_ENTITY_VISIBLE( objSofa, TRUE )
		SET_OBJECT_AS_NO_LONGER_NEEDED( objSofa )
	ENDIF
	
	if does_entity_exist(objFlare)
		delete_object(objFlare)
	endif
	
	REPEAT COUNT_OF(objBomb) i
		if does_entity_exist(objBomb[i])
			delete_object(objBomb[i])
		endif
	ENDREPEAT
	
	if does_particle_fx_looped_exist(flare_ptfx)
		STOP_PARTICLE_FX_LOOPED(flare_ptfx)
	endif 
	
	IF DOES_CAM_EXIST( camera_a )
		DESTROY_CAM( camera_a )
	ENDIF
	IF DOES_CAM_EXIST( camera_b )
		DESTROY_CAM( camera_b )
	ENDIF
	
	//locates stuff
	clear_mission_locate_stuff(slocatesdata)
	clear_mission_location_text_and_blips(slocatesdata)
	//phone book	
	remove_contact_from_individual_phonebook(char_detonatebomb,michael_book)
	
	RESET_PUSH_IN(sPushInData)
	
//	do_stream_vol(false)
//-------------------resets------------------------
	
	set_wanted_level_multiplier(1)	//wanted level for mission reset to normal
	SET_MAX_WANTED_LEVEL(5)
	display_radar(true)
	display_hud(true)	
	
	disable_taxi_hailing(false)
	set_player_control(player_id(), true)
	if isentityalive(player_ped_id())
		set_player_force_skip_aim_intro(player_id(), false)
		set_player_forced_aim(player_id(), false)	
	endif
	
	set_time_scale(1.0)
	if is_screen_faded_out()
	and bmissionpassed
		do_screen_fade_in(2000)
	endif
	
	ENABLE_ALL_DISPATCH_SERVICES(TRUE)
	
	if not has_sound_finished(sound_underwater_ambience)
		stop_sound(sound_underwater_ambience)
	endif
	
	if not has_sound_finished(sound_underwater_rebreather)
		stop_sound(sound_underwater_rebreather)
	endif
	
	if not has_sound_finished(i_sound_scuba_gear)
		stop_sound(i_sound_scuba_gear)
	endif

	if not has_sound_finished(i_sound_swishing)
		stop_sound(i_sound_swishing)
	endif

	STOP_AUDIO_SCENES()
//	OVERRIDE_UNDERWATER_STREAM("DH_FINALE_2A_STREAM", FALSE)
//	if bUnderWaterStreamed
//		STOP_STREAM()
//	endif
	unregister_script_with_audio()
	set_vehicle_model_is_suppressed(tailgater,false)
	set_vehicle_model_is_suppressed(BUFFALO,false)
	set_vehicle_model_is_suppressed(FORKLIFT,false)
	
	disable_taxi_hailing(false)
	restore_player_ped_variations(mike_ped())
	restore_player_ped_variations(frank_ped())
	restore_player_ped_variations(trev_ped())
	allow_dialogue_in_water(false)
	//set_follow_ped_cam_view_mode(cam_view_mode_third_person_medium)
	
	if GET_USINGSEETHROUGH()
		SET_SEETHROUGH(false)
	endif
	SEETHROUGH_RESET()
	CLEANUP_SELECTOR_CAM(SCAMDETAILS)
//---------------------------------------------------
	println("...docks heist 2a mission cleanup")	
		
endproc

// -----------------------------------------------------------------------------------------------------------
//		mission pass
// -----------------------------------------------------------------------------------------------------------

proc mission_passed()
	trigger_music_event("dh2a_mission_complete")
	int icrewmem
	   	repeat get_heist_crew_size(heist_docks) icrewmem
	        set_heist_end_screen_crew_status(heist_docks, get_heist_crew_member_at_index(heist_docks, icrewmem), cmst_fine)
	  	endrepeat
	  
	  	//INFORM_MISSION_STATS_OF_INCREMENT(DH2A_ITEMS_STOLEN)
	  	INFORM_MISSION_STATS_OF_FINANCIAL_DAMAGE(0)
	  	set_heist_end_screen_potential_take(heist_docks, 10000000)
		set_heist_end_screen_actual_take(heist_docks, 10000000)
	  	//add_heist_end_screen_stolen_item(heist_docks, "weapons_lbl", 10000000)//item logging is now gone
	  	//set_heist_end_screen_max_items(heist_docks, 1 ) 
	  	//display_heist_end_screen(heist_docks) //this has been integrated with the mission stat system
	  
		println("...docks heist 2a mission passed")
		AWARD_ACHIEVEMENT_FOR_MISSION(ACH06) // Back On the Boat
		mission_flow_mission_passed()
		mission_cleanup()
		terminate_this_thread()
endproc

// -----------------------------------------------------------------------------------------------------------
//		mission fail
// -----------------------------------------------------------------------------------------------------------

proc mission_failed(mff_mission_fail_flags fail_condition = mff_default)
if not sCamDetails.bRun
	string strreason = ""
	
	//show fail message
	switch fail_condition
		case mff_debug_fail
			strreason = "dckh_fail"
		break		
		case mff_franklin_dead
			strreason = "cmn_fdied"			
		break
		case mff_michael_dead
			strreason = "cmn_mdied"
		break
		case mff_trevor_dead
			strreason = "cmn_tdied"
		break
		case mff_left_trev
			strreason  = "CMN_FLEFT"
		break
		case mff_left_michael
			strreason = "cmn_mleft"
		break
		case mff_left_docks
			strreason = "dckh_mfla" //mike abandoned the heist
		break
		case mff_out_pos
			strreason = "dckh_mfoutpos"
		break
		case mff_TREV_LEFT
			strreason = "DCKH_TFLA"
		break
		case mff_frank_car_des
			strreason = "cmn_fdest"
		break
		case mff_spotted
			strreason = "dckh_mfspo"
		break
		case mff_sub_des
			strreason = "dckh_mfds"
		break
		case mff_sub_stuck
			strreason = "DCKH_MFDS2"
		break
		case mff_area
			IF PLAYER_PED_ID() = mike_ped()
				strreason = "dckh_flemt"
			ELSE
				strreason = "DCKH_FLEFT"
			ENDIF
		break
		case mff_ammo
			strreason = "dckh_ammo"
		break
		case mff_ammob
			strreason = "dckh_ammob"
		break
		case mff_dinghy_des
			strreason = "dckh_Fdingy"
		break
		case mff_dinghy_stuck
			strreason = "dckh_Fdingy2"
		break
		case mff_police
			strreason = "dckh_polf"
		break
		case mff_bombs_exploded
			strreason = "DCKH_MFBOMB"	//bombs were exploded too early
		break
		default
			strreason = "dckh_fail"
		break
	endswitch
	trigger_music_event("dh2a_dead")
	println("...docks heist 2a mission failed")
	
	IF PLAYER_PED_ID() = FRANK_PED()
	AND mission_stage > ENUM_TO_INT(msf_0_drive_to_bridge)
		g_replay.iReplayInt[WHICH_PLAYER] = PLAYER_IS_FRANK
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-143.7584, -2488.7458, 43.4355>>, 5.5522)
		SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<-148.3160, -2498.0483, 47.2429>>, 54.9127)
	ELIF PLAYER_PED_ID() = MIKE_PED()
		g_replay.iReplayInt[WHICH_PLAYER] = PLAYER_IS_MIKE
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<14.6669, -2543.3850, 5.0503>>, 32.8470)
		SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<17.0702, -2542.6228, 5.0500>>, 235.9222)
	ENDIF
	
	mission_flow_mission_failed_with_reason(strreason)
	
	while not get_mission_flow_safe_to_cleanup()
		//maintain anything that could look weird during fade out (e.g. enemies walking off). 
		wait(0)
	endwhile
	
	// check if we need to respawn the player in a different position, 
	// if so call mission_flow_set_fail_warp_location() + set_replay_declined_vehicle_warp_location here

	mission_cleanup() // must only take 1 frame and terminate the thread

	terminate_this_thread()
endif
endproc
proc reset_everything()
//-----------------------destroys---------------------------
	
	if is_cutscene_active()			
		stop_cutscene()
		remove_cutscene()
		while is_cutscene_active()
			wait(0)
		endwhile		
	endif
	
	IF  IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	
	STOP_PED_SPEAKING( PLAYER_PED_ID(), TRUE )
	
	SET_RADAR_ZOOM_PRECISE(0)
	RELEASE_SCRIPT_AUDIO_BANK()
	
	//Set water back to normal
	WATER_OVERRIDE_SET_STRENGTH(0)
	
	CLEAR_TIMECYCLE_MODIFIER()
	
	//-------------------alarm-------------------------
	IF IS_ALARM_PLAYING( "PORT_OF_LS_HEIST_SHIP_ALARMS" )
		STOP_ALARM( "PORT_OF_LS_HEIST_SHIP_ALARMS", TRUE )
	ENDIF
	
	//all peds are deleted
	int i
	for i = 0 to enum_to_int(mpf_num_of_peds)-1
		if does_entity_exist(peds[i].id)
		and (not is_ped_injured(peds[i].id))
			IF NOT bSkipDetonation
				if is_ped_in_any_vehicle(peds[i].id)				
					set_ped_coords_no_gang(peds[i].id,(get_entity_coords(get_vehicle_ped_is_in(peds[i].id))+<<0,-2,0>>))				
				endif	
				if peds[i].id != player_ped_id()
					delete_ped(peds[i].id)	
				endif
			endif
		endif
	endfor	
	//all mercs are deleted
	for i = 0 to enum_to_int(mmf_num_of_mercs) -1	
		mercs[i].bstats = false
		mercs[i].iAdvanceTimer = 0
		mercs[i].iAdvanceState = 0
		mercs[i].currentsit = MS_SIT_NONE
		mercs[i].currentaction = GA_FIND_COVER
		mercs[i].bincombat = FALSE
		mercs[i].bbulletmiss = FALSE
		mercs[i].bStealthKill = false
		mercs[i].bSpottedDead = FALSE
		mercs[i].bPrintHelp = FALSE
		mercs[i].bAlertSpeech = FALSE
		mercs[i].bzombie = false
		mercs[i].irevertactiontime = -1
		if DOES_BLIP_EXIST( mercs[i].blip )
			REMOVE_BLIP( mercs[i].blip )
		endif
		REMOVE_COVER_POINT(mercs[i].cov)
		if does_entity_exist(mercs[i].id)
			if(not is_ped_injured(mercs[i].id))
				if is_ped_in_any_vehicle(mercs[i].id)				
					set_ped_coords_no_gang(mercs[i].id,(get_entity_coords(get_vehicle_ped_is_in(mercs[i].id))+<<0,-2,0>>))				
				endif	
				delete_ped(mercs[i].id)	
			else
				delete_ped(mercs[i].id)	
			endif
		endif
	endfor
	
	//all vehicles are deleted
	for i = 0 to enum_to_int(mvf_num_of_veh)-1
		if does_entity_exist(vehs[i].id)
			IF NOT bSkipDetonation
		 		delete_vehicle(vehs[i].id)
			ENDIF
		endif
	endfor	
	
	bSkipDetonation = FALSE
	
	for i = 0 to enum_to_int(c4_num_of_bombs) -1
		if does_blip_exist(c4[i].blip)
			remove_blip(c4[i].blip)
		endif
	endfor
	
	for i = 0 TO COUNT_OF(sPedHeatScale) - 1
		sPedHeatScale[i].iHeatScale = 0
		sPedHeatScale[i].iHeatState = 0
	endfor
	
	//Remove health pickups
	DELETE_HEALTH_PICKUPS()
	
	if does_entity_exist(objLoot)
		delete_object(objLoot)
	endif
	if does_blip_exist(blipcrate)
		remove_blip(blipcrate)
	endif
	if does_entity_exist(objcov1)
		delete_object(objcov1)
	endif
	if does_entity_exist(objcov2)
		delete_object(objcov2)
	endif
	if does_entity_exist(objcov3)
		delete_object(objcov3)
	endif
	if does_entity_exist(objcov4)
		delete_object(objcov4)
	endif
	if does_entity_exist(objcov5)
		delete_object(objcov5)
	endif
	if does_blip_exist(blip_objective)
		remove_blip(blip_objective)
	endif
	
	if does_entity_exist(objDoor)
		delete_object(objDoor)
	endif
	
	if does_particle_fx_looped_exist(flare_ptfx)
		STOP_PARTICLE_FX_LOOPED(flare_ptfx)
	endif 
	
	if does_entity_exist(objFlare)
		delete_object(objFlare)
	endif
	
	IF DOES_CAM_EXIST( camera_a )
		DESTROY_CAM( camera_a )
	ENDIF
	IF DOES_CAM_EXIST( camera_b )
		DESTROY_CAM( camera_b )
	ENDIF
	
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iStairNavMeshBlocker[0])
		REMOVE_NAVMESH_BLOCKING_OBJECT(iStairNavMeshBlocker[0])
	ENDIF
	IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iStairNavMeshBlocker[1])
		REMOVE_NAVMESH_BLOCKING_OBJECT(iStairNavMeshBlocker[1])
	ENDIF
	
	IF DOES_ENTITY_EXIST(objDoorHide)
		SET_ENTITY_VISIBLE(objDoorHide, TRUE)
		SET_OBJECT_AS_NO_LONGER_NEEDED(objDoorHide)
	ENDIF
	
	IF DOES_ENTITY_EXIST( objSubHook )
		CPRINTLN( DEBUG_MISSION, "SETTING SUBHHOOK AS NO LONGER NEEDED." )
		SET_ENTITY_AS_NO_LONGER_NEEDED( objSubHook )
	ENDIF
	
	REPEAT COUNT_OF(objBomb) i
		if does_entity_exist(objBomb[i])
			delete_object(objBomb[i])
		endif
	ENDREPEAT
	
	//locates stuff
	clear_mission_locate_stuff(slocatesdata)
	clear_mission_location_text_and_blips(slocatesdata)
	
	//clear ai tasks
	if isentityalive(player_ped_id())
		clear_ped_tasks(player_ped_id())
	endif
	
	//phone book	
	remove_contact_from_individual_phonebook(char_detonatebomb,michael_book)
	//sequence
	clear_sequence_task(seq)	
	
	clear_prints()
	kill_any_conversation()
	clear_focus()	
	
//sounds
	STOP_AUDIO_SCENES()
	STOP_STREAM()
	if not has_sound_finished(ibeepid)
		stop_sound(ibeepid)
	endif	
	
	if not has_sound_finished(sound_underwater_ambience)
		stop_sound(sound_underwater_ambience)
	endif
	
	if not has_sound_finished(sound_underwater_rebreather)
		stop_sound(sound_underwater_rebreather)
	endif

	if not has_sound_finished(i_sound_swishing)
		stop_sound(i_sound_swishing)
	endif
	
	if not has_sound_finished(i_sound_scuba_gear)
		stop_sound(i_sound_scuba_gear)
	endif

	if not has_sound_finished(i_sound_heli_attack)
		stop_sound(i_sound_heli_attack)
	endif
	
//	OVERRIDE_UNDERWATER_STREAM("DH_FINALE_2A_STREAM", FALSE)

	IF IS_VALID_INTERIOR( interior_living_room )
		unpin_interior(interior_living_room)
	ENDIF
	
//	do_stream_vol(false)
	
	SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_FRANKLIN, SAVEHOUSE_TREVOR_VB, false)
	
	REMOVE_COVER_POINT(CUSTOMCOVER)
	REMOVE_COVER_POINT( scriptCover[0] )
	REMOVE_COVER_POINT( scriptCover[1] )
	REMOVE_COVER_POINT( scriptCover[2] )
//------------resets-------------------------
	
	SET_PICKUP_GENERATION_RANGE_MULTIPLIER(2.0)
	
	//wanted level
	set_wanted_level_multiplier(0)
	clear_player_wanted_level(player_id())	
	//reset player control
	set_player_control(player_id(), true)
	set_player_force_skip_aim_intro(player_id(), false)
	set_player_forced_aim(player_id(), false)
	//bomb stage init
	
	set_time_scale(1)
	allow_dialogue_in_water(false)
	
	bFilledSpecialMeter = FALSE
	
	//bools
	bAimIntro	= false
	bhotswap 	= false
	bforced 	= false
	bGotWeapons = false
	bRaisedAlarm = false
	bFrankKilled13 = false
	bFrankKilled14 = false
	//------------ Sniper ------------
	b_sniper_scope_active = false
	b_nightvision_active =	false
	//-------------text------------
	bdialogueplayed		= false
	bdialogueplayed2	= false
	bDialoguePlayed3	= false
	bDialoguePlayed4	= false
	bprintdisplayed 	= false
	bprintdisplayed2 	= false
	bHelpDisplayed		= false
	bprintlft_mike		= false
	bprintlft_trev		= false
//	bQuickHelp 			= false
	bVantageText		= false
	bSonarHelp			= false
	bMikeStealthDiag	= false
	bMikeHealthWarn		= false
	bMikeCombatWarn		= FALSE
	bPrintProtect		= FALSE
	bFrankSaysMove		= FALSE
	//---------alarm stuff----------
	bcsalarmtriggered 		= false
	balarmtriggered			= false
	balarminitialised		= false
	binitialisedelayalr 	= false
	bAlertedEarly			= false
	
	bsnipestagecheck 	= false		
	bfirstaim			= false
	bkilledbybuddy			= false
	bFrankFired				= false
	bincombat 				= false
	//-----------Audio------------
	bAudio_snipeLoaded 	= false
	iaudioswitch_snipeStage = 0
	bAttachSoundPlayed	= false
	bStopRebreather = false
	//----------Cutscene---------
	b_cutscene_loaded 		= false
	bCanSkip				= false
	bSwappedToFranklin		= false
	bSwappedToTrevor		= false
	bSwitchOccluders		= false
	
	iPoliceHeliState = 0
	
	icutstage = 0
	
	iCarMercDiag = 0
	iCarConv = 0
	iWayOutDiag	= 0
	iHeliConv = 0
	bHeliConv = FALSE
	
	iCorpseHandlerState = 0
	
	iBomb2Spawned = 0
	iBomb3Spawned = 0
	iBomb4Spawned = 0
	
	players_scene_id = -1
	isnipedelay = -1
	
	iMercVeh1State = 0
	iMercVeh2State = 0
	iMercHeliState = 0
	
	//@RJP restting cam swap timer
//	iFranklinCamSwapTimer = -1
	
//cams
	if GET_USINGSEETHROUGH()
		SET_SEETHROUGH(false)
	endif
	
	cleanup_selector_cam(scamdetails)
	prep_stop_cutscene(true)
	destroy_all_cams()
	
	for i = 0 to enum_to_int(c4_num_of_bombs) -1
		c4[i].bplanted = false		
		clear_area(c4[i].exppos,300,true)
		wash_decals_in_range(c4[i].exppos,300,100)
	endfor
	
	//set_follow_ped_cam_view_mode(cam_view_mode_third_person_medium)
	SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(0)
endproc
proc mission_stage_skip()
//a skip has been made
	if bdoskip = true
		//begin the skip if the switching is idle
		if stageswitch = stageswitch_idle
			if not is_screen_faded_out()
				if not is_screen_fading_out()
					do_screen_fade_out(1000)
				endif
			else
				mission_set_stage(int_to_enum(msf_mission_stage_flags, iskiptostage))
			endif
			
			//@RJP only reset the rayfire if we are switching stages that change the state of the ship
			if (mission_stage < ENUM_TO_INT( msf_8_explosion_ctscne ) AND requestedstage > ENUM_TO_INT( msf_7_detonate ))
			or (mission_stage > ENUM_TO_INT( msf_7_detonate ) AND requestedstage < ENUM_TO_INT( msf_8_explosion_ctscne ))
				CPRINTLN( DEBUG_MISSION, "mission_stage_management: setting bResetRayfire to TRUE" )
			endif
		//needs to be carried out before states own entering stage
		elif stageswitch = stageswitch_entering
			
			render_script_cams(false,false)
			set_player_control(player_id(), true)
			clear_prints()
			clear_help()
			
			if is_cutscene_active()			
				stop_cutscene()
				remove_cutscene()
				while is_cutscene_active()
					wait(0)
				endwhile		
			endif
			
			reset_everything()
			
			// mark all assets for clean up
			Start_Skip_Streaming(sAssetData)
			
			Load_asset_Stage(int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage))
						
			PRINTLN("Start Skip Loading")
			WHILE NOT Update_Skip_Streaming(sAssetData)
				WAIT(0)				
			ENDWHILE						
			PRINTLN("End Skip Loading")	
			
			switch int_to_enum(msf_mission_stage_flags, mission_stage)
			
				case msf_0_drive_to_bridge
					su_0_drive_to_bridge()
				break
				
				case msf_1_snipe_gaurds
					su_1_snipe_gaurds()
				break
				
				case msf_2_first_bomb
					su_2_first_bomb()
				break
				
				case msf_3_second_bomb
					su_3_second_bomb()
				break
				
				case msf_4_third_bomb
					su_4_third_bomb()
				break
				
				case msf_5_fourth_bomb		
					SU_5_FOURTH_BOMB()
				break		
				
				case msf_6_way_out
					su_6_way_out()
				break
				
				case msf_7_detonate
					su_7_detonate()
				break
				
				case msf_8_explosion_ctscne
					su_8_explosion_ctscne()
				break
				
				case msf_9_the_goods
					su_9_the_goods()
				break 
				
				case msf_10_end_cutscene
					su_10_end_cutscene()
				break
				
//				case msf_passed
//					su_passed()
//				break
			endswitch
			
				su_general()
				
			set_gameplay_cam_relative_heading()
			set_gameplay_cam_relative_pitch()
			

			bdoskip = false
		endif
#if is_debug_build
	//check is a skip being asked for, dont allow skip during setup stage
	elif launch_mission_stage_menu(zmenunames, iskiptostage, mission_stage, true)
		if iskiptostage > enum_to_int(msf_10_end_cutscene)
			mission_passed()
		else
			iskiptostage = clamp_int(iskiptostage, 0, enum_to_int(msf_num_of_stages)-1)
			if is_screen_faded_in()
				do_screen_fade_out(1000)
				bdoskip = true
			endif
		endif
		RESET_PUSH_IN(sPushInData)
		check_time()
		bis_zskip = true
#endif
	endif
endproc

PROC update_merc_blip( MERC_STRUCT &merc )
	
	IF NOT DOES_ENTITY_EXIST( merc.id )
		safe_remove_blip(merc.blip)
		EXIT
	ENDIF
	
	IF IS_PED_INJURED( merc.id )
		safe_remove_blip(merc.blip)
		EXIT
	ENDIF
	
	IF NOT DOES_BLIP_EXIST( merc.blip )
		IF IS_PED_IN_COMBAT( merc.id )
			merc.blip = create_blip_for_ped( merc.id, TRUE )
		ENDIF
	ELSE
		IF merc.bPrintHelp
			SET_BLIP_FLASHES( merc.blip, TRUE )
		ELSE
			SET_BLIP_FLASHES( merc.blip, FALSE )
		ENDIF
	ENDIF
	
ENDPROC

proc mission_checks()
	IF mission_substage = STAGE_ENTRY
		EXIT
	ENDIF
	
	IF IS_REPLAY_BEING_SET_UP()
		EXIT
	ENDIF
	
	// handle wanted controls
	if mission_stage = enum_to_int(msf_0_drive_to_bridge)
	or mission_stage = enum_to_int(msf_9_the_goods)
		ENABLE_ALL_DISPATCH_SERVICES(TRUE)
		set_wanted_level_multiplier(1)
		SET_MAX_WANTED_LEVEL(5)	
	else
		ENABLE_ALL_DISPATCH_SERVICES(FALSE)
		set_wanted_level_multiplier(0)
		SET_MAX_WANTED_LEVEL(0)	
	endif
	
//--------------------------------------------------death/fail checks-----------------------------------------------
	int i
	for i = 0 to enum_to_int(mpf_num_of_peds) -1
		if does_entity_exist(peds[i].id)
			if not isentityalive(peds[i].id)			
				if peds[i].id = peds[mpf_frank].id
					mission_failed(mff_franklin_dead)
				endif
				if peds[i].id = peds[mpf_mike].id 
					mission_failed(mff_michael_dead)
				endif
				if peds[i].id = peds[mpf_trev].id
					mission_failed(mff_trevor_dead)
				endif
				set_ped_as_no_longer_needed(peds[i].id)
			endif
		endif
	endfor
	
	BOOL bAlertHelp = FALSE
	
	for i = 0 to enum_to_int(mmf_num_of_mercs) -1
		if mission_stage < enum_to_int(msf_6_way_out)			
			update_merc_blip( mercs[i] )
		endif
		if does_entity_exist(mercs[i].id)
			if is_ped_injured(mercs[i].id)	
				safe_remove_blip(mercs[i].blip)				
				if mercs[i].bzombie	
				or balarmtriggered
				or bcsalarmtriggered
					IF mission_stage = ENUM_TO_INT( msf_6_way_out )
						IF i = ENUM_TO_INT( merc13_C4_3 )
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY( mercs[i].id, PLAYER_PED_ID() )
								bFrankKilled13 = TRUE
							ENDIF
						ENDIF
						IF i = ENUM_TO_INT( merc14_C4_3 )
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY( mercs[i].id, PLAYER_PED_ID() )
								bFrankKilled14 = TRUE
							ENDIF
						ENDIF
					ENDIF
					set_ped_as_no_longer_needed(mercs[i].id)
				else
					if not mercs[i].bstats
						INFORM_MISSION_STATS_OF_INCREMENT(DH2A_STEALTH_KILLS)
						mercs[i].bstats = true
					endif
				endif
				if mercs[i].bPrintHelp
					mercs[i].bPrintHelp = FALSE
				endif
			endif
			if mercs[i].bPrintHelp
				bAlertHelp = TRUE
			endif
		else
			safe_remove_blip(mercs[i].blip)
		endif
	endfor
	
	IF NOT bAlertHelp
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DCKH_QUICK")
			CLEAR_HELP()
		ENDIF
	ENDIF
	
	for i = 0 to enum_to_int(mvf_num_of_veh) -1
		if does_entity_exist(vehs[i].id)
			if not is_vehicle_driveable(vehs[i].id)							
				if vehs[i].id = vehs[mvf_sub].id
				and mission_stage = enum_to_int(msf_9_the_goods)
					mission_failed(mff_sub_des)
				endif
				if vehs[i].id = vehs[mvf_dinghy].id
				and mission_stage = enum_to_int(msf_0_drive_to_bridge)
					mission_failed(mff_dinghy_des)
				endif
				set_vehicle_as_no_longer_needed(vehs[i].id)			
			endif
		endif
	endfor
	
	//@RJP end mission if players destroy a sticky bomb before the detonation cutscene (player shoots sticky bomb)
	if mission_stage > enum_to_int( msf_1_snipe_gaurds )
	and mission_stage < enum_to_int( msf_8_explosion_ctscne )
		IF IS_EXPLOSION_IN_ANGLED_AREA( EXP_TAG_STICKYBOMB, (C4[C4_0_front_of_ship].ExpPos + <<-0.75,3,2>>),(C4[C4_0_front_of_ship].ExpPos + <<-0.75,3,-2>>),10 )
		OR IS_EXPLOSION_IN_ANGLED_AREA( EXP_TAG_STICKYBOMB, (C4[C4_1_first_mast].ExpPos + <<-0.75,3,2>>),(C4[C4_1_first_mast].ExpPos + <<-0.75,3,-2>>),10 )
		OR IS_EXPLOSION_IN_ANGLED_AREA( EXP_TAG_STICKYBOMB, (C4[C4_2_second_mast].ExpPos + <<-0.75,3,2>>),(C4[C4_2_second_mast].ExpPos + <<-0.75,3,-2>>),10 )
			mission_failed(mff_bombs_exploded)
		ENDIF
	endif
	
	//franklin on the vantage point check
	if mission_stage > enum_to_int(msf_0_drive_to_bridge) 
	and mission_stage < enum_to_int(msf_8_explosion_ctscne)
		if isentityalive(frank_ped())
			if player_ped_id() = frank_ped()
				//stop the player changing from the sniper rifle 
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_PREV_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_NEXT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_SELECT_WEAPON)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL,INPUT_JUMP)
//				IF IS_SELECTOR_ONSCREEN()
//					SET_PLAYER_CONTROL( PLAYER_ID(), FALSE )
//				ELSE
//					SET_PLAYER_CONTROL( PLAYER_ID(), TRUE )
//				ENDIF
				// check that the player is still at the vantage point.
				if not is_entity_at_coord(frank_ped(),<< -142.1772, -2494.1208, 45.5379 >>,<<20,20,20>>)
					if not bprintlft_mike
					and not scamdetails.brun
						print_now("dckh_retvant",default_god_text_time,1)
						bprintlft_mike = true
					endif
					if not does_blip_exist(blip_checksob)
						blip_checksob =	create_blip_for_coord(<< -147.1491, -2495.4390, 48.9368 >>)
					endif
					if not is_entity_at_coord(frank_ped(),<< -142.1772, -2494.1208, 45.5379 >>,<<40,40,40>>)
						mission_failed(mff_left_michael)
					endif
				else
					bprintlft_mike = false					
					if is_entity_at_coord(frank_ped(),<< -147.1491, -2495.4390, 48.9368 >>,<<2,2,2>>)
						if does_blip_exist(blip_checksob)
							remove_blip(blip_checksob)
						endif
					endif					
				endif				
			else
				if does_blip_exist(blip_checksob)
					remove_blip(blip_checksob)
				endif
			endif
		endif
	else
		if does_blip_exist(blip_checksob)
			remove_blip(blip_checksob)
		endif
	endif
	//Franklin left Trev check
	if mission_stage = enum_to_int(msf_0_drive_to_bridge)
	and mission_substage = 7
	and isentityalive(trev_ped())
	and isentityalive(frank_ped())
		if GET_DISTANCE_BETWEEN_COORDS(get_entity_coords(frank_ped()), get_entity_coords(trev_ped())) > 150
			mission_failed(mff_left_trev)
		elif GET_DISTANCE_BETWEEN_COORDS(get_entity_coords(frank_ped()), get_entity_coords(trev_ped())) > 100
			if not bPrintLft_trev
				print_now("CMN_FLEAVE",default_god_text_time,1)
				bPrintLft_trev = true
			endif
		else
			bPrintLft_trev = false
		endif
	endif
	
	//mike in the right area still 
	if mission_stage < enum_to_int(msf_7_detonate)
	and mission_stage > enum_to_int(msf_0_drive_to_bridge)
		if isentityalive(mike_ped())
			if player_ped_id() = mike_ped()
				if not is_entity_at_coord(mike_ped(), << -171.1301, -2366.4204, 13.0810 >>,<<120,60,50>>)
					if not bprintmikeleftarea
							print_now("dckh_retship",default_god_text_time,1)
							bprintmikeleftarea = true
						endif
					if not is_entity_at_coord(mike_ped(), << -171.1301, -2366.4204, 13.0810 >>,<<130,70,50>>)
						mission_failed(mff_area) // mike left area fail.
					endif
				else
					bprintmikeleftarea = false
				endif
				//@RJP fail mission if Michael off boat
				if mission_stage < enum_to_int(msf_6_way_out)
					VECTOR vMichaelPos = GET_ENTITY_COORDS( mike_ped() )
					if vMichaelPos.z < 8.0
						mission_failed(mff_area) // mike left area fail.
					endif
				endif
			endif
		endif
	elif mission_stage = enum_to_int(msf_0_drive_to_bridge)
		if isentityalive(mike_ped())
			if player_ped_id() = mike_ped()
				if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mike_ped()),<<-163.55930, -2367.28882, 8.31906>>) > 1100	
					mission_failed(mff_area) // mike left area fail.
				elif GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mike_ped()),<<-163.55930, -2367.28882, 8.31906>>) > 1000
					if not bprintmikeleftarea
						print_now("DCKH_RET",default_god_text_time,1)
						bprintmikeleftarea = true
					endif
				else
					bprintmikeleftarea = false
				endif
			endif
		endif
	endif
	//trevor has left in the sub
	if mission_stage = enum_to_int(msf_9_the_goods)
		if isentityalive(trev_ped())
			if player_ped_id() = trev_ped()
				if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(trev_ped()),<<-163.55930, -2367.28882, 8.31906>>) > 1100	
					mission_failed(mff_TREV_LEFT) // mike left area fail.
				elif GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(trev_ped()),<<-163.55930, -2367.28882, 8.31906>>) > 1000
					if not bprinttrevleftarea
						print_now("DCKH_RET",default_god_text_time,1)
						bprinttrevleftarea = true
					endif
				else
					bprinttrevleftarea = false
				endif
			endif
		endif
	endif
	
	//ammo check
	if bGotWeapons
		if player_ped_id() = mike_ped()
			if mission_stage > enum_to_int(msf_1_snipe_gaurds)
			and mission_stage <= enum_to_int(msf_5_fourth_bomb)	
				if not C4[C4_0_front_of_ship].bPlanted
				or not C4[C4_1_first_mast].bPlanted
				or not C4[C4_2_second_mast].bPlanted
				or not C4[C4_3_inside].bPlanted
					bool bAmmo = true			
					if HAS_PED_GOT_WEAPON(MIKE_PED(),wtBomb)
						if GET_AMMO_IN_PED_WEAPON(MIKE_PED(),wtBomb) > 0
							bAmmo = false
						endif
					endif			
					if bAmmo
						CPRINTLN( DEBUG_MISSION, "MIKE OUT OF STICKY BOMB AMMO" )
						Mission_Failed(mff_ammob)//ran out of ammo
					endif
				endif
			endif
		endif
	endif
	
	//sniper ammo check
	if bGotWeapons
		if mission_stage < enum_to_int(msf_7_detonate)
			if PLAYER_PED_ID() = frank_ped()
				if not (mission_stage = enum_to_int(msf_0_drive_to_bridge) and mission_substage < 10)
				and not (mission_substage = STAGE_ENTRY)
					bool bAmmo = true			
					if HAS_PED_GOT_WEAPON(FRANK_PED(),WEAPONTYPE_SNIPERRIFLE)
						if GET_AMMO_IN_PED_WEAPON(FRANK_PED(),WEAPONTYPE_SNIPERRIFLE) > 0
							bAmmo = false
						endif
					endif
					if HAS_PED_GOT_WEAPON(FRANK_PED(),WEAPONTYPE_HEAVYSNIPER)
						if GET_AMMO_IN_PED_WEAPON(FRANK_PED(),WEAPONTYPE_HEAVYSNIPER) > 0
							bAmmo = false				
						endif
					endif 
					if bAmmo
						Mission_Failed(mff_ammo)//temp fail for test
					endif
				endif
			endif
		endif
	endif
	
//police check 
	if GET_PLAYER_WANTED_LEVEL(player_id()) > 0
		mission_failed(mff_police)
	endif
	
	//@RJP check to see that mercs 1 and 2 are not alerted during drive to bridge stage
	if mission_stage = enum_to_int(msf_0_drive_to_bridge)
		for i = enum_to_int(merc1_start) to enum_to_int(merc2_start)
			if DOES_ENTITY_EXIST(mercs[i].id)
				if isentityalive(frank_ped())
					if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mercs[i].id,frank_ped())
						mission_failed(mff_out_pos)
					endif
				endif
				if not IS_PED_INJURED(mercs[i].id)
					if IS_PED_IN_COMBAT( mercs[i].id )
						mission_failed(mff_out_pos)
					endif
				endif
			endif
		endfor
	endif
	
	//@RJP also check if Michael kills a ped, and during the first stage as well
	//shooting early
//	if mission_stage = enum_to_int(msf_0_drive_to_bridge)
//	or (mission_stage = enum_to_int(msf_1_snipe_gaurds) AND mission_substage = 1)
//		
//		for i = enum_to_int(merc3_C4_2) to enum_to_int(merc18_c4_4)
//			if DOES_ENTITY_EXIST(mercs[i].id)
//				if isentityalive(frank_ped())
//					if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mercs[i].id,frank_ped())
//						bAlertedEarly = TRUE
////						mission_failed(mff_out_pos)
//					endif
//				endif
//				if not IS_PED_INJURED(mercs[i].id)
//					if IS_PED_IN_COMBAT( mercs[i].id )
//						bAlertedEarly = TRUE
////						mission_failed(mff_out_pos)
//					endif
//				endif
//			endif
//		endfor		
//	endif
	
	if mission_stage = enum_to_int(msf_0_drive_to_bridge)
	or (mission_stage = enum_to_int(msf_1_snipe_gaurds) AND mission_substage = 1)
		IF balarmtriggered
			IF bAlertedEarly
				IF GET_GAME_TIMER() > iAlertedEarlyTimer
					mission_failed(mff_out_pos)
				ENDIF
			ELSE
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_NOSHT", CONV_PRIORITY_MEDIUM )
						bAlertedEarly = TRUE
						iAlertedEarlyTimer = (GET_GAME_TIMER()+5000)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		INT timer
		
		if not balarmtriggered
			for i = enum_to_int(merc3_C4_2) to enum_to_int(merc18_c4_4)
				if isentityalive(mercs[i].id)
					if is_ped_in_combat(mercs[i].id)		
						mercs[i].bincombat = true
						timer = 2500
					elif mercs[i].currentaction = ga_aim_at_sniper
						mercs[i].bincombat = true
						timer = 2500
					//@RJP check alarm if mercs have spotted a dead merc
					elif mercs[i].bSpottedDead
						mercs[i].bincombat = true
						timer = 2500
					elif mercs[i].currentsit <> MS_SIT_NONE
						mercs[i].bincombat = true
						timer = 2500
					else
						mercs[i].bincombat = false
					endif
				else
					mercs[i].bincombat = false
				endif
			endfor	
		
			bincombat = false
			for i = enum_to_int(merc3_C4_2) to enum_to_int(merc18_c4_4)
				if mercs[i].bincombat
				and isentityalive(mercs[i].id)
					bincombat = true
				endif
			endfor	
			
			if bincombat 
				if not binitialisedelayalr
					icombattimer = get_game_timer() 
					binitialisedelayalr = true
				else
					if get_game_timer() - icombattimer > timer
						INFORM_MISSION_STATS_OF_INCREMENT( DH2A_NO_ALARMS )
						CPRINTLN( DEBUG_MISSION, "DH2A_NO_ALARMS incremented to 1" )
						balarmtriggered = true
					endif
				endif
			else
				binitialisedelayalr = false
			endif
		
		endif
	ENDIF
//----------------------------------------------------------------------------------------------------------	
//--------------------------------player ped stuff--------------------------------
	if isentityalive(player_ped_id())
		IF player_ped_id() = MIKE_PED()
			IF mission_stage < ENUM_TO_INT(msf_6_way_out)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
			ENDIF
		ENDIF
		set_ped_combat_attributes(player_ped_id(),ca_requires_los_to_shoot,false)
	endif
//-------------------------------- Thermal --------------------------------	
	if mission_stage >= enum_to_int(msf_1_snipe_gaurds)
	and mission_stage <= enum_to_int(msf_7_detonate)
		if player_ped_id() = frank_ped()
		and IS_FIRST_PERSON_AIM_CAM_ACTIVE()
		and not sCamDetails.bRun
		and not bIncutscene
			b_sniper_scope_active = true
		else
			b_sniper_scope_active = false
		endif
	else 
		if GET_USINGSEETHROUGH()
			SET_SEETHROUGH(false)
		endif
		b_nightvision_active = FALSE
		b_sniper_scope_active = false
	endif
	
	if b_sniper_scope_active
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
			IF b_nightvision_active
				b_nightvision_active = FALSE
			ELSE
				b_nightvision_active = TRUE
			ENDIF
		ENDIF
	endif
	
	if b_nightvision_active
	and b_sniper_scope_active
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
		SET_SEETHROUGH(true)
	else
		SET_SEETHROUGH(false)
	endif
//------------------------------------------stage checks--------------------------------------

//------------------------------------ alarm ------------------------------------
	if balarmtriggered 
	or bcsalarmtriggered
		if PREPARE_ALARM( "PORT_OF_LS_HEIST_SHIP_ALARMS" )
			IF NOT IS_ALARM_PLAYING( "PORT_OF_LS_HEIST_SHIP_ALARMS" )
				START_ALARM( "PORT_OF_LS_HEIST_SHIP_ALARMS", FALSE )
			ENDIF
		endif
		if mission_stage > enum_to_int(msf_1_snipe_gaurds)
		and mission_stage < enum_to_int(msf_6_way_out)
			AUDIO_dialogue_merc_combat()
		endif
	else
		IF IS_ALARM_PLAYING( "PORT_OF_LS_HEIST_SHIP_ALARMS" )
			STOP_ALARM( "PORT_OF_LS_HEIST_SHIP_ALARMS", TRUE )
		ENDIF
		if mission_stage > enum_to_int(msf_1_snipe_gaurds)
		and mission_stage < enum_to_int(msf_6_way_out)
			AUDIO_dialogue_merc_stealth()
		endif
	endif
		
//------------mercs seeing distance------------
	if not balarmtriggered
	and not bcsalarmtriggered
		if mission_stage > enum_to_int(msf_1_snipe_gaurds)
		and mission_stage < enum_to_int(msf_6_way_out)
			if isentityalive(mike_ped())
				vector vmikecoord = get_entity_coords(mike_ped())
				//@RJP may want to play with seeing range, bit odd right now
				int imercs
				for imercs = enum_to_int(merc3_C4_2) to enum_to_int(merc18_c4_4)
					if isentityalive(mercs[imercs].id)
						IF mercs[imercs].currentsit = ms_sit_none
							//@RJP if the player is Franklin, give the ped enough seeing range to combat Franklin if he is in ga_aim_at_sniper
							if PLAYER_PED_ID() = frank_ped()
								if mercs[imercs].currentaction = ga_aim_at_sniper
								or IS_PED_IN_COMBAT( mercs[imercs].id, frank_ped() )
									set_ped_seeing_range(mercs[imercs].id,200)
								else
									if vmikecoord.z < 12
										set_ped_seeing_range(mercs[imercs].id,11)
										SET_PED_HEARING_RANGE(mercs[imercs].id,11)
										SET_PED_VISUAL_FIELD_PERIPHERAL_RANGE( mercs[imercs].id, 2 )
									else
										set_ped_seeing_range(mercs[imercs].id,8)
										SET_PED_HEARING_RANGE(mercs[imercs].id,8)
										SET_PED_VISUAL_FIELD_PERIPHERAL_RANGE( mercs[imercs].id, 2 )
									endif
									SET_PED_VISUAL_FIELD_MIN_ELEVATION_ANGLE( mercs[imercs].id, -75 )
									SET_PED_VISUAL_FIELD_MAX_ELEVATION_ANGLE( mercs[imercs].id, 60 )
								endif
							else
								if vmikecoord.z < 12
									set_ped_seeing_range(mercs[imercs].id,11)
									SET_PED_HEARING_RANGE(mercs[imercs].id,11)
								else
									set_ped_seeing_range(mercs[imercs].id,8)
									SET_PED_HEARING_RANGE(mercs[imercs].id,8)
								endif
								//@RJP set peripheral vision based on how much player moving
								if IS_PED_STILL( mike_ped() )
									SET_PED_VISUAL_FIELD_PERIPHERAL_RANGE( mercs[imercs].id, 2 )
								elif IS_PED_WALKING( mike_ped() )
									SET_PED_VISUAL_FIELD_PERIPHERAL_RANGE( mercs[imercs].id, 3 )
								elif IS_PED_RUNNING( mike_ped() )
									SET_PED_VISUAL_FIELD_PERIPHERAL_RANGE( mercs[imercs].id, 4 )
								elif IS_PED_SPRINTING( mike_ped() )
									SET_PED_VISUAL_FIELD_PERIPHERAL_RANGE( mercs[imercs].id, 5 )
								endif
								SET_PED_VISUAL_FIELD_MIN_ELEVATION_ANGLE( mercs[imercs].id, -30 )
								SET_PED_VISUAL_FIELD_MAX_ELEVATION_ANGLE( mercs[imercs].id, 30 )
								IF VDIST2( vmikecoord, GET_ENTITY_COORDS(mercs[imercs].id)) <= 225
									SET_PED_RESET_FLAG( mercs[imercs].id, PRF_ConsiderAsPlayerCoverThreatWithoutLOS, TRUE )
								ENDIF
							endif
						ENDIF
					endif
				endfor
			endif
		else
			int imercs
			for imercs = enum_to_int(merc1_start) to enum_to_int(merc26_car2)
				if isentityalive(mercs[imercs].id)
					set_ped_seeing_range(mercs[imercs].id,200)
					SET_PED_HEARING_RANGE( mercs[imercs].id,60 )
				endif
			endfor
		endif
	else //alarm sounded mercs can see ped to attack. 
		int imercs
		for imercs = enum_to_int(merc1_start) to enum_to_int(merc26_car2)
			if isentityalive(mercs[imercs].id)
				set_ped_seeing_range(mercs[imercs].id,200)
				SET_PED_HEARING_RANGE( mercs[imercs].id,60 )
			endif
		endfor
		
	endif
	
//------------action mode check------------
	if mission_stage < ENUM_TO_INT( msf_6_way_out )
		if isentityalive(mike_ped())
		and PLAYER_PED_ID() != mike_ped()
			if balarmtriggered
			or bcsalarmtriggered
				set_ped_using_action_mode(mike_ped(),true)
			else
				SET_PED_STEALTH_MOVEMENT(mike_ped(),true)
			endif
		endif
	endif
	
//------------mercs all dead check / none in combat check------------
		int imercs
//		bool bpeace = true
//		ballmercsdead = true
		for imercs = enum_to_int(merc1_start) to enum_to_int(merc26_car2)
			if isentityalive(mercs[imercs].id)
//				ballmercsdead = false					
				if is_player_dead(player_id())
					task_wander_standard(mercs[i].id)//clear task when frank dead
				endif								
				if is_ped_in_combat(mercs[imercs].id)
				and player_ped_id() = mike_ped()
//					bpeace = false
				endif
			endif			
		endfor
//		if bpeace
//			AUDIO_dialogue_killed_merc()
//		endif
//------------hot swap------------ 
	manage_hotswap()
	
//------------------------audio------------------------
	if mission_stage > enum_to_int(msf_8_explosion_ctscne)
		audio_underwater()
	endif
	if isentityalive(mike_ped())
		if is_ped_swimming_under_water(mike_ped())
			set_current_ped_weapon(mike_ped(),weapontype_unarmed,true)
		endif
	endif
	AUDIO_sniping()
	//dock workers	
//	AUDIO_manage_dock_workers()	
//------------------------first aim stuff------------------------
//lock aim to enemies
if mission_stage <= enum_to_int(msf_6_way_out)
	if player_ped_id() = frank_ped()
		if bfirstaim
			if is_player_free_aiming(player_id())
				if mission_stage <= enum_to_int(msf_1_snipe_gaurds)
					if isentityalive(mercs[merc1_start].id)
						set_gameplay_cam_relative_heading(get_sniper_rifle_aim_heading_for_ped(mercs[merc1_start].id))
						set_gameplay_cam_relative_pitch(get_sniper_rifle_aim_pitch_for_ped(mercs[merc1_start].id))
						set_first_person_aim_cam_zoom_factor(10)
						bfirstaim = false
					endif
				else
					if isentityalive(mike_ped())
						set_gameplay_cam_relative_heading(get_sniper_rifle_aim_heading_for_ped(mike_ped()))
						set_gameplay_cam_relative_pitch(get_sniper_rifle_aim_pitch_for_ped(mike_ped()))
						set_first_person_aim_cam_zoom_factor(10)
						bfirstaim = false
					endif
				endif
			endif
		endif 
	endif
endif

//------------------------critical hit by frank on all mercs------------------------
int merc
if isentityalive(frank_ped())
	for merc = enum_to_int(merc1_start) to enum_to_int(merc26_car2)
		if isentityalive(mercs[merc].id)
			if has_entity_been_damaged_by_entity(mercs[merc].id,frank_ped())
				set_entity_health(mercs[merc].id,50)
			endif
		endif
	endfor
endif
	
//------------------------event response for merc------------------------
//if mission_stage > enum_to_int(msf_1_snipe_gaurds)	
if mission_stage < enum_to_int(msf_6_way_out)
and not balarmtriggered
and not bcsalarmtriggered
	MERC_do_reaction()
endif

//----------------------------------blipping mike----------------------------------------
if mission_stage > enum_to_int(msf_1_snipe_gaurds)
and mission_stage < enum_to_int(msf_7_detonate)
and isentityalive(frank_ped())
and isentityalive(mike_ped())
	if player_ped_id() = frank_ped()
		if not DOES_BLIP_EXIST(peds[mpf_mike].blip)
			peds[mpf_mike].blip = CREATE_BLIP_FOR_PED(MIKE_PED())
			SET_BLIP_PRIORITY(peds[mpf_mike].blip, BLIPPRIORITY_HIGHEST)
		endif
	else
		if DOES_BLIP_EXIST(peds[mpf_mike].blip)
			REMOVE_BLIP(peds[mpf_mike].blip)
		endif
	endif
else
	if DOES_BLIP_EXIST(peds[mpf_mike].blip)
		REMOVE_BLIP(peds[mpf_mike].blip)
	endif
endif
	
	//@RJP set Franklin invincible if playing as Michael
	if mission_stage > enum_to_int(msf_1_snipe_gaurds)
	and mission_stage < enum_to_int(msf_6_way_out)
	and player_ped_id() = mike_ped()
		IF DOES_ENTITY_EXIST( frank_ped() )
			SET_ENTITY_INVINCIBLE(frank_ped(), TRUE)
		ENDIF
	else
		IF DOES_ENTITY_EXIST( frank_ped() )
			SET_ENTITY_INVINCIBLE(frank_ped(), FALSE)
		ENDIF
	endif

//----------------------------------------------MINI MAP--------------------------------------------

//		if mission_stage > enum_to_int(msf_0_drive_to_bridge)
//		and mission_stage < enum_to_int(msf_7_detonate)
//			IF PLAYER_PED_ID() = mike_ped()
//				//3432762226
//				set_radar_as_interior_this_frame(get_hash_key("v_fakeboatpo1sh1"),-165.34,-2350.03,0)
//			ENDIF
//		elif mission_stage > enum_to_int(msf_8_explosion_ctscne)
//		and mission_substage < 12
//			//2092539216
//			set_radar_as_interior_this_frame(get_hash_key("v_fakeboatpo1sh1sunk"),-165.0,-2350.0,0) 
//		endif


//----------------------------------------------STATS--------------------------------------------


	if isentityalive(player_ped_id())
//damage taken	
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(player_ped_id(), DH2A_DAMAGE)
//veh damage / speed
		if IS_PED_IN_ANY_VEHICLE(player_ped_id())
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(player_ped_id()), DH2A_CAR_DAMAGE)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(player_ped_id()))
		else
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(null, DH2A_CAR_DAMAGE)
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(null)			
		endif
	endif

endproc
proc mission_setup()

	initialise()
	setupmercpatrol()
	
	//no need to do this
	//IF NOT IS_REPLAY_IN_PROGRESS()
	//	RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT_FOR_CHAR(CHAR_MICHAEL)
	//ENDIF
	
	printstring("docks heist - mission_setup")printnl()

// loads the mission text
	request_additional_text("h5heist", mission_text_slot)
//	request_additional_text("cst7aud", mission_dialogue_text_slot)
	while not has_additional_text_loaded(mission_text_slot)
//	or not has_additional_text_loaded(mission_dialogue_text_slot)
		wait(0)
	endwhile

//sfx	
	register_script_with_audio()   	
	while not request_ambient_audio_bank("script\\underwater")
		wait(0)
	endwhile 			
	
//relationship	
	remove_relationship_group(rel_group_buddy)
	remove_relationship_group(rel_group_enemies)	
	add_relationship_group("buddy", rel_group_buddy)
	add_relationship_group("enemies", rel_group_enemies)	
//ignore
//	set_relationship_between_groups(acquaintance_type_ped_ignore,rel_group_enemies,rel_group_buddy)
//	set_relationship_between_groups(acquaintance_type_ped_ignore,rel_group_enemies,relgrouphash_player)
//haters		
	set_relationship_between_groups(acquaintance_type_ped_hate,rel_group_enemies,rel_group_buddy)	
	set_relationship_between_groups(acquaintance_type_ped_hate,rel_group_buddy,rel_group_enemies)
	set_relationship_between_groups(acquaintance_type_ped_hate,relgrouphash_player,rel_group_enemies)
	set_relationship_between_groups(acquaintance_type_ped_hate,rel_group_enemies,relgrouphash_player)
//friends		
	
	set_relationship_between_groups(acquaintance_type_ped_like,rel_group_enemies,rel_group_enemies)
	set_relationship_between_groups(acquaintance_type_ped_like,rel_group_buddy,relgrouphash_player)
	set_relationship_between_groups(acquaintance_type_ped_like,relgrouphash_player,rel_group_buddy)

//Cover blocking
	ADD_COVER_BLOCKING_AREA(<<-214.774490,-2376.247314,8.319207>>, <<-212.231201,-2374.965820,11.515604>>, TRUE, TRUE, TRUE)
	ADD_COVER_BLOCKING_AREA(<<-208.063126,-2382.714111,7.937626>>, <<-212.805313,-2378.890381,10.319207>>, TRUE, TRUE, TRUE)
	
//suppress vehciles	
	set_vehicle_model_is_suppressed(tailgater,true)
	set_vehicle_model_is_suppressed(buffalo,true)
	set_vehicle_model_is_suppressed(FORKLIFT,true)

//vehicle generators at the docks
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA( <<-146.52478, -2530.88916,38>>,<<34.11267, -2326.69922, 1.23919>>,FALSE )
	
//scenario block docks
	add_scenario_blocking_area(<<-146.52478, -2530.88916,38>>,<<34.11267, -2326.69922, 1.23919>>)
	set_ped_non_creation_area(<<-146.52478, -2530.88916,38>>,<<34.11267, -2326.69922, 1.23919>>)
//cutscene
	b_cutscene_loaded = false

	disable_taxi_hailing(true)

	IF GET_PLAYER_WANTED_LEVEL(player_id()) > 0
		SET_PLAYER_WANTED_LEVEL( player_id(), 0 )
	ENDIF
	
	println("start setup loading")
	WHILE NOT Update_Skip_Streaming(sAssetData)
		WAIT(0)				
	ENDWHILE	
	println("end setup loading")	

endproc

//PURPOSE: Creates the players vehicle using the stored data in the replay structs. If no data is found it will create the default vehicle for michael.
// Should be called until returns true. (May not create a vehicle if bIsVehicleNeeded = FALSE)
FUNC BOOL Create_Player_Mission_Vehicle(VEHICLE_INDEX &vehicle, VECTOR vCoord, FLOAT fHeading)
	
//	VEHICLE_SETUP_STRUCT s_vehicle_struct
	
	// Populate the struct
	IF IS_REPLAY_IN_PROGRESS() 
		IF IS_REPLAY_START_VEHICLE_AVAILABLE()
			IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(<<0,0,0>>,TRUE)
				CREATE_VEHICLE_FOR_REPLAY( vehicle, vCoord, fHeading, FALSE, FALSE, FALSE, FALSE, TRUE, BUFFALO2, 0, CHAR_FRANKLIN )
				RETURN TRUE
			ELSE
				CREATE_VEHICLE_FOR_REPLAY( vehicle, <<-1161.4044, -1504.9207, 3.3707>>, 318.8535, FALSE, FALSE, FALSE, FALSE, TRUE, BUFFALO2, 0, CHAR_FRANKLIN )
				RETURN TRUE
			ENDIF
		ELSE
			IF CREATE_PLAYER_VEHICLE(vehicle, CHAR_FRANKLIN, vCoord, fHeading, TRUE, VEHICLE_TYPE_CAR)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN), TRUE)
				RETURN TRUE	
			ENDIF
		ENDIF
	ELSE 
		IF CREATE_PLAYER_VEHICLE(vehicle, CHAR_FRANKLIN, vCoord, fHeading, TRUE, VEHICLE_TYPE_CAR)
			SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN), TRUE)
			RETURN TRUE	
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL Create_End_Cutscene_Vehicle(VEHICLE_INDEX &vehicle, VECTOR vCoord, FLOAT fHeading)
	
	IF IS_REPLAY_START_VEHICLE_AVAILABLE()
		IF IS_REPLAY_START_VEHICLE_UNDER_SIZE_LIMIT(<<0,0,0>>,TRUE)
		AND IS_THIS_MODEL_A_CAR( GET_REPLAY_START_VEHICLE_MODEL() )
			REQUEST_REPLAY_START_VEHICLE_MODEL()
			IF HAS_REPLAY_START_VEHICLE_LOADED()
				vehicle = CREATE_REPLAY_START_VEHICLE( vCoord, fHeading )
				RETURN TRUE	
			ENDIF
		ELSE
			IF CREATE_PLAYER_VEHICLE(vehicle, CHAR_FRANKLIN, vCoord, fHeading, TRUE, VEHICLE_TYPE_CAR)
				RETURN TRUE	
			ENDIF
		ENDIF
	ELSE
		IF CREATE_PLAYER_VEHICLE(vehicle, CHAR_FRANKLIN, vCoord, fHeading, TRUE, VEHICLE_TYPE_CAR)
			RETURN TRUE	
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DISABLE_PLAYER_MOVEMENT_FOR_FIRST_PERSON_SNIPING()

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
	ENDIF

ENDPROC

/// PURPOSE:
///    Controls conversation on trip to bridge between Franklin and Trevor
PROC CAR_CONVERSATION_MANAGER()
	
	SWITCH iCarConv
		CASE 0
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DS2A_COMON1", CONV_PRIORITY_MEDIUM )
					CPRINTLN( DEBUG_MISSION, "CAR_CONVERSATION_MANAGER: Trevor tells Franklin to hurry up" )
					iCarConv++
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DS2A_FRESP", CONV_PRIORITY_MEDIUM )
					CPRINTLN( DEBUG_MISSION, "CAR_CONVERSATION_MANAGER: Franklin says he's coming" )
					iImpatientTimer = GET_GAME_TIMER()+15000
					iCarConv++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				iCarConv++
			ELIF GET_GAME_TIMER() > iImpatientTimer
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DS2A_FRGO", CONV_PRIORITY_MEDIUM )
						CPRINTLN( DEBUG_MISSION, "CAR_CONVERSATION_MANAGER: Franklin impatient" )
						iCarConv++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF GET_ENTITY_MODEL( GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = GET_PLAYER_VEH_MODEL( CHAR_FRANKLIN, VEHICLE_TYPE_CAR )
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DS2A_FRCAR", CONV_PRIORITY_MEDIUM )
							CPRINTLN( DEBUG_MISSION, "CAR_CONVERSATION_MANAGER: Franklin says we taking my car?" )
							iCarConv++
						ENDIF
					ENDIF
				ELIF GET_ENTITY_MODEL( GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) = GET_PLAYER_VEH_MODEL( CHAR_FRANKLIN, VEHICLE_TYPE_BIKE )
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DS2A_FRBIKE", CONV_PRIORITY_MEDIUM )
							CPRINTLN( DEBUG_MISSION, "CAR_CONVERSATION_MANAGER: Franklin says that's my bike" )
							iCarConv++
						ENDIF
					ENDIF
				
				ELSE
					iCarConv++
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DS2A_WHERE", CONV_PRIORITY_MEDIUM )
						CPRINTLN( DEBUG_MISSION, "CAR_CONVERSATION_MANAGER: Trevor says where to go" )
						iCarConv++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF bDialoguePlayed2
				iCarConv++
			ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_PED_IN_VEHICLE(FRANK_PED(),GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF IS_SAFE_TO_START_CONVERSATION()
						IF GET_MISSION_COMPLETE_STATE( SP_MISSION_FRANKLIN_1 )
							IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DS2A_01", CONV_PRIORITY_MEDIUM )
								CPRINTLN( DEBUG_MISSION, "CAR_CONVERSATION_MANAGER: DS2A_01" )
								iCarConv++
							ENDIF
						ELSE
							IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DS2A_02", CONV_PRIORITY_MEDIUM )
								CPRINTLN( DEBUG_MISSION, "CAR_CONVERSATION_MANAGER: DS2A_02" )
								iCarConv++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 6
			IF bDialoguePlayed2
				iCarConv++
			ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF IS_PED_IN_VEHICLE(FRANK_PED(),GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DS2A_03", CONV_PRIORITY_MEDIUM )
							CPRINTLN( DEBUG_MISSION, "CAR_CONVERSATION_MANAGER: DS2A_03" )
							iCarConv++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 7
			IF bDialoguePlayed2
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION(convo_struct, "D2AAUD", "DS2A_PARK", CONV_PRIORITY_MEDIUM)
						CPRINTLN( DEBUG_MISSION, "CAR_CONVERSATION_MANAGER: Trevor says to park" )
						iCarConv++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 8
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(convo_struct, "D2AAUD", "DS2A_COOL", CONV_PRIORITY_MEDIUM)
					CPRINTLN( DEBUG_MISSION, "CAR_CONVERSATION_MANAGER: Franklin says cool" )
					iCarConv++
				ENDIF
			ENDIF
		BREAK
		CASE 9
			//End case, no more car conversation
		BREAK
	ENDSWITCH
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission stages
// -----------------------------------------------------------------------------------------------------------
	PROC ST_0_DRIVE_TO_BRIDGE()	
		ENTITY_INDEX entity_franklin
		ENTITY_INDEX entity_trevor
		ENTITY_INDEX entity_michael
		ENTITY_INDEX tempEntity
		
		TEXT_LABEL_23 tlConv
		STRING sConv
		
		SWITCH mission_substage
			case Stage_entry
				if IS_REPLAY_IN_PROGRESS()
				or bskip_cut
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
					mission_substage = 2
				else
					mission_substage++
				endif
			break
			case 1				
				DISPLAY_RADAR(false)
				DISPLAY_HUD(false)
				SET_PLAYER_CONTROL(PLAYER_ID(),false)
				REDUCE_AMBIENT_MODELS(TRUE)
				RUN_INTRO_CUTSCENE()	
				
				if b_cutscene_loaded									
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					mission_substage++
				endif				
			break		
			case 2
			
				IF IS_GAMEPLAY_HINT_ACTIVE()
					STOP_GAMEPLAY_HINT()
				ENDIF
			
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
					IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						entity_franklin = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklin")
						IF DOES_ENTITY_EXIST(entity_franklin)
							sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = GET_PED_INDEX_FROM_ENTITY_INDEX(entity_franklin)
						ENDIF
					ELSE
						SET_ENTITY_VISIBLE( sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE )
					ENDIF
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
					IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						entity_trevor = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor")
						IF DOES_ENTITY_EXIST(entity_trevor)
							sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = GET_PED_INDEX_FROM_ENTITY_INDEX(entity_trevor)
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
						entity_michael = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael")
						IF DOES_ENTITY_EXIST(entity_michael)
							sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = GET_PED_INDEX_FROM_ENTITY_INDEX(entity_michael)
						ENDIF
					ELSE
						SET_ENTITY_VISIBLE( sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE )
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST( objSofa )
					objSofa = GET_CLOSEST_OBJECT_OF_TYPE( <<-1158.5811, -1519.2471, 9.6308>>, 2.0, V_RES_TRE_SOFA_MESS_C )
				ELSE
					SET_ENTITY_VISIBLE( objSofa, FALSE )
				ENDIF
				
				if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
					CPRINTLN( DEBUG_MISSION, "EXIT STATE FOR FRANKLIN: TRUE" )
					IF IS_ENTITY_OK(SSELECTORPEDS.PEDID[SELECTOR_PED_FRANKLIN])
						FORCE_PED_MOTION_STATE( SSELECTORPEDS.PEDID[SELECTOR_PED_FRANKLIN], MS_ON_FOOT_WALK, true, FAUS_CUTSCENE_EXIT )
						SET_ENTITY_COORDS( sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], <<-1154.6628, -1518.2433, 9.6327>> )
						SET_ENTITY_HEADING( sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], 306.3135 )
					ENDIF
				ENDIF
				
				if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					CPRINTLN( DEBUG_MISSION, "EXIT STATE FOR MICHAEL: TRUE" )
					
					IF DOES_ENTITY_EXIST(SSELECTORPEDS.PEDID[SELECTOR_PED_MICHAEL])
						IF NOT IS_ENTITY_DEAD(SSELECTORPEDS.PEDID[SELECTOR_PED_MICHAEL])
							STORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(SSELECTORPEDS.PEDID[SELECTOR_PED_MICHAEL])	//store Michael's weapons from before the mission
						ENDIF
					ENDIF
					
				ENDIF
				
				if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
					IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						entity_trevor = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor")
						IF DOES_ENTITY_EXIST(entity_trevor)
							sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = GET_PED_INDEX_FROM_ENTITY_INDEX(entity_trevor)
						ENDIF
					ENDIF
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
						CPRINTLN( DEBUG_MISSION, "GET_CURRENT_PLAYER_PED_ENUM: PLAYER NOT TREVOR" )
						IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
							CPRINTLN( DEBUG_MISSION, "MAKE_SELECTOR_PED_SELECTION TREVOR TRUE" )
							TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
						ENDIF
					ENDIF
					IF IS_ENTITY_OK(PLAYER_PED_ID())
						SET_ENTITY_COORDS( PLAYER_PED_ID(), <<-1151.6340, -1519.4980, 9.6327>> )
						SET_ENTITY_HEADING( PLAYER_PED_ID(), 215.0 )
						SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
						SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
							SEQUENCE_INDEX SequenceIndex
							CLEAR_SEQUENCE_TASK(SequenceIndex)
							OPEN_SEQUENCE_TASK(SequenceIndex)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1150.7572, -1520.9470, 9.6327>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1149.5098, -1522.4679, 9.6331>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_NO_STOPPING)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-1147.3914, -1522.7167, 9.4130>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_STOP_EXACTLY, 312.1079)
							CLOSE_SEQUENCE_TASK(SequenceIndex)
							TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), SequenceIndex)
							CLEAR_SEQUENCE_TASK(SequenceIndex)
						ELSE
							SIMULATE_PLAYER_INPUT_GAIT( PLAYER_ID(), PEDMOVE_WALK, 3500 )
						ENDIF
						FORCE_PED_MOTION_STATE( PLAYER_PED_ID(), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT )
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					IF bSwappedToTrevor
						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) != CAM_VIEW_MODE_FIRST_PERSON
							ANIMPOSTFX_PLAY("SwitchSceneTrevor", 1000, FALSE)
							PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
						ENDIF
					ENDIF
					REPLAY_STOP_EVENT()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				ENDIF
				
				if not IS_CUTSCENE_PLAYING()
				or IS_REPLAY_IN_PROGRESS()
				or bskip_cut
					if GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
						IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
							TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
							SET_ENTITY_LOAD_COLLISION_FLAG(PLAYER_PED_ID(), TRUE)
							SET_RADAR_ZOOM_PRECISE(1)
						ENDIF
					ELSE
						IF isentityalive( SSELECTORPEDS.PEDID[SELECTOR_PED_MICHAEL] )
							PEDS[MPF_MIKE].ID = SSELECTORPEDS.PEDID[SELECTOR_PED_MICHAEL]
							DELETE_PED( PEDS[MPF_MIKE].ID )
						endif
						if isentityalive( SSELECTORPEDS.PEDID[SELECTOR_PED_FRANKLIN] )
							PEDS[mpf_frank].ID = SSELECTORPEDS.PEDID[SELECTOR_PED_FRANKLIN]
						endif
						PEDS[mpf_trev].ID = PLAYER_PED_ID()
						SET_RADAR_ZOOM_PRECISE(1)
						IF DOES_ENTITY_EXIST( objSofa )
							SET_ENTITY_VISIBLE( objSofa, TRUE )
							SET_OBJECT_AS_NO_LONGER_NEEDED( objSofa )
						ENDIF
						
						IF IS_THIS_TV_AVAILABLE_FOR_USE(TV_LOC_TREVOR_VENICE)
							STOP_TV_PLAYBACK(TRUE, TV_LOC_TREVOR_VENICE)
						ENDIF
					
						mission_substage++
						
						
					ENDIF
				endif
				
				
//				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE( V_ILEV_TREV_DOORFRONT, <<-1149.7089, -1521.0879, 10.7827>>, TRUE, 0, 0, 1 )
			break			
			case 3
				REDUCE_AMBIENT_MODELS(FALSE)
				//trevor						
				IF isentityalive( TREV_PED() )
					ADD_PED_FOR_DIALOGUE(convo_struct, 2, TREV_PED(), "TREVOR")
				ENDIF
				IF isentityalive( frank_ped() )
					ADD_PED_FOR_DIALOGUE(convo_struct, 3, FRANK_PED(), "FRANKLIN")
					SET_PED_CONFIG_FLAG( FRANK_PED(), PCF_ForcedToUseSpecificGroupSeatIndex, true )
					TASK_FOLLOW_NAV_MESH_TO_COORD( FRANK_PED(), <<-1151.3347, -1520.3198, 3.3653>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP )
				ENDIF
//				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE( V_ILEV_TREV_DOORFRONT, <<-1149.7089, -1521.0879, 10.7827>>, FALSE, 0, 0, 0 )
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				
				REMOVE_CUTSCENE()
				
				SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_FRANKLIN, SAVEHOUSE_TREVOR_VB, true)
//				SET_DOOR_STATE(DOORNAME_T_APARTMENT_VB,DOORSTATE_UNLOCKED)
				
//				Point_Gameplay_cam_at_coord(GET_ENTITY_HEADING(player_ped_id()))
				SU_General()
				
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				
				mission_substage++
			break
			case 4
				if not isentityalive( vehs[mvf_mission_veh].id )
					Create_Player_Mission_Vehicle( vehs[mvf_mission_veh].id, << -1154.6117, -1520.0699, 3.3456 >> ,33.0431)
				else
					mission_substage++
				endif
			break
			case 5	
				if HAVE_ALL_STREAMING_REQUESTS_COMPLETED(TREV_PED())
				and HAVE_ALL_STREAMING_REQUESTS_COMPLETED(FRANK_PED())
				
					if PREPARE_MUSIC_EVENT("DH2A_START")
						if IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)							
						endif		
						DISPLAY_RADAR(true)
						DISPLAY_HUD(true)
						//trev
						SET_PED_RELATIONSHIP_GROUP_HASH(frank_ped(),rel_group_buddy)
						
						//@RJP make sure the boat is in the right state at the start of the mission
						MANAGE_IPLS(BUILDINGSTATE_NORMAL)
						
						//scene
						bpauseconv = false
						bDialoguePlayed = false
						bDialoguePlayed2 = false
//						bPrintDisplayed = false
						SET_PLAYER_CONTROL(PLAYER_ID(),true)
						SET_RADAR_ZOOM_PRECISE(0)
						mission_substage++	
					endif
				endif
			break
			case 6
				CAR_CONVERSATION_MANAGER()
				VECTOR vPlayerPos 
				vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
				IF vPlayerPos.z < 5.0
				AND NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-1142.679688,-1518.525635,3.279745>>, <<-1148.989014,-1522.986450,9.341030>>, 6.000000)
					mission_substage++
				ENDIF
			break
			case 7
				CAR_CONVERSATION_MANAGER()
				if IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					if IS_PED_IN_VEHICLE(frank_ped(),GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
						//@RJP remove blip for Franklin's car when he is in it
						if DOES_BLIP_EXIST( blip_playerCar )
							REMOVE_BLIP( blip_playerCar )
						endif
						
						if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(trev_ped()),<<-160.51218, -2502.88281, 47.93985>>) < 125
						and not bDialoguePlayed2			
							KILL_FACE_TO_FACE_CONVERSATION()
							TASK_LOOK_AT_ENTITY( FRANK_PED(), trev_ped(), -1 )
							manage_ipls( BUILDINGSTATE_NORMAL )
							Load_Asset_AnimDict(sAssetData,"missheistdocks2a")
							Load_Asset_AnimDict(sAssetData,"missheistdocks2aig_1")
							load_asset_animdict(sAssetData,"missheistdocks2a@crouch")
							Load_Asset_AnimDict(sAssetData,"missheistdocks2a@alert")	
							Load_Asset_AnimDict(sAssetData,"missheistdocks2aswitchig_6")
							Load_Asset_AnimDict(sAssetData,"missheistdocks2aswitchig_7")
//								load_asset_model(sAssetData,s_m_y_dockwork_01)
							Load_Asset_Model(sAssetData,S_M_Y_BlackOps_01)
							Load_Asset_Model(sAssetData,Prop_Military_Pickup_01)
							Load_Asset_Model(sAssetData,landstalker)
							Load_Asset_Model(sAssetData,PROP_MIL_CRATE_02)
							Load_Asset_Model(sAssetData,PROP_CRATEPILE_07A)
							Load_Asset_Weapon_Asset(sAssetData, wtSniper, WRF_REQUEST_ALL_ANIMS, WEAPON_COMPONENT_SCOPE|WEAPON_COMPONENT_SCLIP2)
							//@RJP load dingy at start to prevent problem with jumping out of car and dingy not being loaded in time
							Load_Asset_Model(sAssetData,DINGHY)
							bDialoguePlayed2 = true						
						endif
						if GET_GAME_TIMER() - iCSDelay > 10000
						And not bDialoguePlayed					
							IF IS_VALID_INTERIOR( interior_living_room )
								unpin_interior(interior_living_room)
							ENDIF	
							SET_DOOR_STATE(DOORNAME_T_APARTMENT_VB,DOORSTATE_LOCKED)	
							SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_FRANKLIN, SAVEHOUSE_TREVOR_VB, false)	
							bDialoguePlayed = true
						endif
						if not bpauseconv
							PAUSE_FACE_TO_FACE_CONVERSATION(false)	
							bpauseconv = true
						endif
					else
						if bPauseconv
							tlConv  = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
							sConv = CONVERT_TEXT_LABEL_TO_STRING(tlConv)
							IF ARE_STRINGS_EQUAL(sConv, "DS2A_01")
							OR ARE_STRINGS_EQUAL(sConv, "DS2A_02")
							OR ARE_STRINGS_EQUAL(sConv, "DS2A_03")
								PAUSE_FACE_TO_FACE_CONVERSATION(true)
								bpauseconv = false
							ENDIF
						endif
					endif	
					IF NOT (GET_VEHICLE_PED_IS_IN(player_ped_id()) = vehs[mvf_mission_veh].id)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_mission_veh].id)
						vehs[mvf_mission_veh].id  = GET_VEHICLE_PED_IS_IN(player_ped_id())
						SET_ENTITY_AS_MISSION_ENTITY(vehs[mvf_mission_veh].id, TRUE, TRUE)
					ENDIF
				else
					if bPauseconv
						tlConv  = GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
						sConv = CONVERT_TEXT_LABEL_TO_STRING(tlConv)
						IF ARE_STRINGS_EQUAL(sConv, "DS2A_01")
						OR ARE_STRINGS_EQUAL(sConv, "DS2A_02")
						OR ARE_STRINGS_EQUAL(sConv, "DS2A_03")
							PAUSE_FACE_TO_FACE_CONVERSATION(true)
							bpauseconv = false
						ENDIF
					endif
				endif
				IF IS_PED_IN_ANY_HELI(trev_ped())
					if IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_ANY_VEHICLE(sLocatesData,<<-160.51218, -2502.88281, 47.93985>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,true,frank_ped(),NULL,NULL,"DCKH_DRIVE","DCKH_DONT_L_T","","","DCKH_P_UP_B","DCKH_CAR","DCKH_CARBK",FALSE,FALSE)
					or ((DOES_BLIP_EXIST(sLocatesData.LocationBlip)And IS_ENTITY_IN_ANGLED_AREA(FRANK_PED(),<<-168.01913, -2497.72852,  44.1933 >>,<< -146.66716, -2512.51978, 54.0683 >>,10))
					AND IS_PED_IN_VEHICLE( frank_ped(), vehs[mvf_mission_veh].id ))
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_mission_veh].id)
						vehs[mvf_mission_veh].id  = GET_VEHICLE_PED_IS_IN(player_ped_id())
						SET_ENTITY_AS_MISSION_ENTITY(vehs[mvf_mission_veh].id)
						bDialoguePlayed =false	
						bDialoguePlayed2 =false
						HANG_UP_AND_PUT_AWAY_PHONE()
						SET_PLAYER_CONTROL(player_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON)
//						BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehs[mvf_mission_veh].id,10)
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData,true)	
						TRIGGER_MUSIC_EVENT("DH2A_ON_BRIDGE")
						Load_Asset_Model(sAssetData,DINGHY)
						mission_substage++
					ENDIF
				ELSE
					if IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_ANY_VEHICLE(sLocatesData,<<-160.51218, -2502.88281, 47.93985>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,true,frank_ped(),NULL,NULL,"DCKH_DRIVE","DCKH_DONT_L_T","","","DCKH_P_UP_B","DCKH_CAR","DCKH_CARBK",FALSE,FALSE)
					or ((DOES_BLIP_EXIST(sLocatesData.LocationBlip)And IS_ENTITY_IN_ANGLED_AREA(FRANK_PED(),<<-168.01913, -2497.72852,  44.1933 >>,<< -146.66716, -2512.51978, 52.0683 >>,4))
					AND IS_PED_IN_VEHICLE( frank_ped(), vehs[mvf_mission_veh].id ))
						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_mission_veh].id)
						vehs[mvf_mission_veh].id  = GET_VEHICLE_PED_IS_IN(player_ped_id())
						SET_ENTITY_AS_MISSION_ENTITY(vehs[mvf_mission_veh].id)
						bDialoguePlayed =false	
						bDialoguePlayed2 =false
						HANG_UP_AND_PUT_AWAY_PHONE()
						BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehs[mvf_mission_veh].id,10)
						//@RJP disable player control when vehicle is coming to a stop to prevent player from exiting and breaking script
	//					SET_PLAYER_CONTROL( player_id(), false, SPC_LEAVE_CAMERA_CONTROL_ON )
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData,true)	
						TRIGGER_MUSIC_EVENT("DH2A_ON_BRIDGE")
						Load_Asset_Model(sAssetData,DINGHY)
						mission_substage++				
					endif
				ENDIF
			break
			case 8
				CAR_CONVERSATION_MANAGER()
				if IS_VEHICLE_ALMOST_STOPPED(vehs[mvf_mission_veh].id)	
					blip_objective = CREATE_BLIP_FOR_COORD(<< -143.8537, -2488.4031, 43.4098 >>)
					IF MAKE_SELECTOR_PED_SELECTION( sSelectorPeds, SELECTOR_PED_FRANKLIN )
						TAKE_CONTROL_OF_SELECTOR_PED( sSelectorPeds )
						ANIMPOSTFX_PLAY("SwitchSceneFranklin", 1000, FALSE)
						PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
					ENDIF
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_mission_veh].id,FALSE)	
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
					//dinghy and mike
					vehs[mvf_dinghy].id	= CREATE_VEHICLE(dinghy,<<-77.9618, -2359.0828, 0>>, 232.3750)
					SET_BOAT_ANCHOR(vehs[mvf_dinghy].id, true)
					SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_dinghy].id,0)
					SET_VEHICLE_RADIO_ENABLED( vehs[mvf_dinghy].id, FALSE )
					NEW_LOAD_SCENE_START(<<-142.35, -2487.92, 44.80>>, NORMALISE_VECTOR(<<-100, -0.03, 0.06>>), 400 )
//					NEW_LOAD_SCENE_START(<<-143.55, -2489.59, 45.58>>, NORMALISE_VECTOR(<<0.02, 0.97, -0.23>>), 1000 )
					
					REPLAY_RECORD_BACK_FOR_TIME(10.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					
					mission_substage++
				elif IS_PED_IN_ANY_HELI(trev_ped())
					IF GET_SCRIPT_TASK_STATUS( trev_ped(), SCRIPT_TASK_VEHICLE_MISSION ) > PERFORMING_TASK
						TASK_HELI_MISSION(trev_ped(), GET_VEHICLE_PED_IS_IN(trev_ped()), null, null,<<-160.51218, -2502.88281, 47.93985>>,MISSION_LAND,10,1,-1,48,0 )
					ENDIF
				else
					BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehs[mvf_mission_veh].id,10)
				endif
			break	
			case 9
				CAR_CONVERSATION_MANAGER()
				if not IS_PED_IN_VEHICLE(FRANK_PED(),vehs[mvf_mission_veh].id)
					if CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_mike].id,CHAR_MICHAEL,vehs[mvf_dinghy].id)
						//mike
						ADD_PED_FOR_DIALOGUE(convo_struct,1,peds[mpf_mike].id,"MICHAEL")
						Scuba_suite_up(MIKE_PED(),FALSE,FALSE)
						SET_PED_COMP_ITEM_CURRENT_SP(MIKE_PED(), COMP_TYPE_PROPS, PROPS_P0_HEADSET)		
						SET_PED_DIES_IN_WATER( MIKE_PED(), FALSE )
						SET_PED_RELATIONSHIP_GROUP_HASH(MIKE_PED(),rel_group_buddy)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_PED(),true)
						
						//frank
						give_mission_weapons(FALSE)
						if HAS_PED_GOT_WEAPON(FRANK_PED(),wtSniper)
//							SET_CURRENT_PED_WEAPON(FRANK_PED(),wtSniper,true)
						else
							GIVE_WEAPON_TO_PED(FRANK_PED(),wtSniper,200,FALSE,FALSE)
						endif
						if not IS_AUDIO_SCENE_ACTIVE("DH2A_GET_TO_VANTAGE_POINT")
							START_AUDIO_SCENE("DH2A_GET_TO_VANTAGE_POINT")
						endif
						
						SET_PLAYER_CONTROL(player_id(),true)
						
						//create mercs for snipe
						int merc
						for merc = enum_to_int(merc1_start) to enum_to_int(merc18_c4_4)
							//@RJP there is no longer a merc8 or merc6
							if merc != enum_to_int( merc8_C4_2 )
							and merc != enum_to_int( merc7_C4_2 )
							and merc != enum_to_int( merc6_C4_2 )
							and merc != enum_to_int( merc10_C4_3 )
							and merc != enum_to_int( MERC15_C4_3 )
							and merc != enum_to_int( merc17_c4_4 )
								//@RJP mercs should start blocking non-temporary events until we get to the bomb stages
								createmercenary(int_to_enum(MMF_MISSION_MERC_FLAGS,merc), 8, TRUE)
							endif
						endfor
						//@RJP adding parked vehicle for cover in way out section
						//VEHICLES
						VEHS[mvf_parked1].id = CREATE_VEHICLE(landstalker,<<-203.5000, -2413.4336, 5.0012>>, 293.3041)
						VEHS[mvf_parked2].id = CREATE_VEHICLE(landstalker,<<-180.8568, -2431.3264, 5.0013>>, 94.2693)
						SET_VEHICLE_COLOUR_COMBINATION(VEHS[mvf_parked1].ID,0)
						SET_VEHICLE_COLOUR_COMBINATION(VEHS[mvf_parked2].ID,0)
						SET_VEHICLE_CAN_LEAK_PETROL(VEHS[mvf_parked1].ID,TRUE)
						SET_VEHICLE_CAN_LEAK_PETROL(VEHS[mvf_parked2].ID,TRUE)
//						createdockworker(mpf_dock1)
//						createdockworker(mpf_dock2)
//						Unload_Asset_Model(sAssetData,S_M_Y_DOCKWORK_01)
//					//set up docks 
						TASK_PLAY_ANIM(mercs[merc1_start].id,"missheistdocks2a","idle_guard",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING)
						TASK_PLAY_ANIM(mercs[merc2_start].id,"missheistdocks2a","idle_guard",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING)
						GIVE_WEAPON_TO_PED(mercs[merc1_start].id,WEAPONTYPE_HEAVYSNIPER,INFINITE_AMMO,true,true)
						GIVE_WEAPON_TO_PED(mercs[merc2_start].id,WEAPONTYPE_HEAVYSNIPER,INFINITE_AMMO,true,true)
						SET_PED_ALERTNESS(mercs[merc1_start].id,AS_alert)
						SET_PED_ALERTNESS(mercs[merc2_start].id,AS_alert)
						mission_substage++
					endif
				endif
			break
			case 10
				CAR_CONVERSATION_MANAGER()
				//Turn off players ability to jump when near the vantage point to prevent 1442328
				IF IS_ENTITY_AT_COORD(FRANK_PED(),<<-142.7872, -2489.0518, 43.4330>>,<<8,8,10>>,FALSE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
				ENDIF
				if IS_ENTITY_AT_COORD(FRANK_PED(),<<-142.7872, -2489.0518, 43.5330>>,<<1,1,2>>,TRUE)
				and not IS_PED_RAGDOLL(FRANK_PED())
					if DOES_BLIP_EXIST(blip_objective)
						remove_blip(blip_objective)
					endif
					CLEAR_AREA(<< -147.0720, -2495.3691, 47.6477 >>,50,true)
					//audio					
					if IS_AUDIO_SCENE_ACTIVE("DH2A_GET_TO_VANTAGE_POINT")
						STOP_AUDIO_SCENE("DH2A_GET_TO_VANTAGE_POINT")					
					endif
					if not IS_AUDIO_SCENE_ACTIVE("DH2A_SNIPE_GUARDS_scene")
						START_AUDIO_SCENE("DH2A_SNIPE_GUARDS_scene")
					endif
					HANG_UP_AND_PUT_AWAY_PHONE()
					//frank
					if HAS_PED_GOT_WEAPON(FRANK_PED(),wtSniper)
						SET_CURRENT_PED_WEAPON(FRANK_PED(),wtSniper,false)
					else
						GIVE_WEAPON_TO_PED(FRANK_PED(),wtSniper,200,false,true)
					endif
					SET_PLAYER_CONTROL(player_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON)
					//frank
					OPEN_SEQUENCE_TASK(seq)
						TASK_GO_STRAIGHT_TO_COORD(null,<<-143.8061, -2488.3064, 43.4408>>,PEDMOVE_WALK)
						TASK_ACHIEVE_HEADING(null,GET_HEADING_FROM_ENTITIES( frank_ped(),mercs[merc1_start].id)) 
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(FRANK_PED(),seq)
					CLEAR_SEQUENCE_TASK(seq)	
					if not bVantageText
						//sort out trev and mike car
						if IsEntityAlive(TREV_PED())
							SET_PED_AS_NO_LONGER_NEEDED(peds[mpf_trev].id)
						endif
						if IsEntityAlive(vehs[mvf_mission_veh].id)
							SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_mission_veh].id)
						endif
					endif
										
					REPLAY_RECORD_BACK_FOR_TIME(4.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
									
					mission_substage++
				ELSE
					IF NOT IS_ENTITY_AT_COORD(FRANK_PED(),<<-142.7872, -2489.0518, 43.4330>>,<<80,80,80>>)
						Print_now("DCKH_VANTAGE",DEFAULT_GOD_TEXT_TIME,1)
						IF NOT IS_ENTITY_AT_COORD(FRANK_PED(),<<-142.7872, -2489.0518, 43.4330>>,<<100,100,100>>)
							mission_failed(mff_area)
						ENDIF
					ENDIF
					if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						if not bVantageText
							Print_now("DCKH_VANTAGE",DEFAULT_GOD_TEXT_TIME,1)
							bVantageText = TRUE
							OPEN_SEQUENCE_TASK(seq)						
							TASK_CLEAR_LOOK_AT(null)
							TASK_VEHICLE_DRIVE_TO_COORD(null,vehs[mvf_mission_veh].id,<<188.8807, -2988.6001, 4.7541>>,25,DRIVINGSTYLE_ACCURATE,GET_ENTITY_MODEL(vehs[mvf_mission_veh].id),DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS,4,-1)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(TREV_PED(),seq)
							CLEAR_SEQUENCE_TASK(seq)
							
							//sort out trev and mike car
							if IsEntityAlive(TREV_PED())
								SET_PED_AS_NO_LONGER_NEEDED(peds[mpf_trev].id)
							endif
							if IsEntityAlive(vehs[mvf_mission_veh].id)
								SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_mission_veh].id)
							endif
						endif
					endif
				endif
			break
			case 11
				IF isentityalive( vehs[mvf_dinghy].id )
					CPRINTLN( DEBUG_MISSION, "SETTING DINGHY LOD MULTIPLIER" )
					SET_VEHICLE_LOD_MULTIPLIER( vehs[mvf_dinghy].id, 5 )
					SET_BOAT_ANCHOR( vehs[mvf_dinghy].id, FALSE )
					SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER( vehs[mvf_dinghy].id, FALSE )
				ENDIF
				//cam
				camera_a = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
				set_cam_active(camera_a,true)
				prep_start_cutscene(false,<< -209.5674, -2386.8694, 15.0927 >>)	
				SET_FORCE_FOOTSTEP_UPDATE(frank_ped(), TRUE)
				CLEAR_PED_TASKS_IMMEDIATELY(frank_ped())
				SET_CURRENT_PED_WEAPON(FRANK_PED(),wtSniper,false)
				players_scene_id = CREATE_SYNCHRONIZED_SCENE( << -143.8203, -2488.2612, 43.4412 >>, <<0,0,-34.5>>) //<<-143.8, -2487.555, 43.450>>, <<0,0,-34.5>> )
				TASK_SYNCHRONIZED_SCENE(  frank_ped(), players_scene_id, "missheistdocks2aswitchig_6", "IG_6_f_sniping_m_dingy_franklin", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_PROCESS_ATTACHMENTS_ON_START)
				PLAY_SYNCHRONIZED_CAM_ANIM(camera_a, players_scene_id, "IG_6_f_sniping_m_dingy_franklin_cam", "missheistdocks2aswitchig_6")
				SET_CAM_ACTIVE(camera_a, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(frank_ped(),TRUE)
				bDialoguePlayed2 = FALSE
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				mission_substage++
			break
			case 12
				
				IF IS_SYNCHRONIZED_SCENE_RUNNING(players_scene_id)
					SET_CURRENT_PED_WEAPON(FRANK_PED(),wtSniper,true)	
					tempEntity = GET_CURRENT_PED_WEAPON_ENTITY_INDEX(frank_ped())
					IF DOES_ENTITY_EXIST(tempEntity)
						REQUEST_WEAPON_HIGH_DETAIL_MODEL(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(tempEntity))
					ENDIF
					REQUEST_WEAPON_ASSET(wtSniper, DEFAULT, WEAPON_COMPONENT_SCOPE | WEAPON_COMPONENT_GRIP | WEAPON_COMPONENT_SCLIP2)
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Requesting weapon asset.")
				ENDIF
			
				IF isentityalive( vehs[mvf_dinghy].id )
					REQUEST_VEHICLE_HIGH_DETAIL_MODEL( vehs[mvf_dinghy].id )
				ENDIF
				IF GET_SYNCHRONIZED_SCENE_PHASE( players_scene_id ) > 0.95
					SET_FORCE_FOOTSTEP_UPDATE(frank_ped(), FALSE)
					do_tods_dh2a()
					mission_substage++
				ELIF GET_SYNCHRONIZED_SCENE_PHASE( players_scene_id ) > 0.5
					IF NOT bDialoguePlayed2
						if IS_NEW_LOAD_SCENE_ACTIVE()
							NEW_LOAD_SCENE_STOP()
						endif
						IF IS_TIME_BETWEEN_THESE_HOURS(g_sMissionStaticData[sp_heist_docks_2a].startHour, g_sMissionStaticData[sp_heist_docks_2a].endHour)
							NEW_LOAD_SCENE_START(<<-79.16,-2355.64,1.87>>,NORMALISE_VECTOR(<<0.69,-0.72,0.11>>),300)
						ELSE
							NEW_LOAD_SCENE_START(<<-24.19,-2417.54,7.80>>,NORMALISE_VECTOR(<<-0.97,0.16,0.18>>),500)
						ENDIF
						bDialoguePlayed2 = TRUE
					ENDIF
				ELIF GET_SYNCHRONIZED_SCENE_PHASE( players_scene_id ) > 0.2
					IF NOT bDialoguePlayed
						KILL_ANY_CONVERSATION()
						IF PLAY_SINGLE_LINE_FROM_CONVERSATION(convo_struct,"D2AAUD","DS2A_DING","DS2A_DING_1",CONV_PRIORITY_MEDIUM)
							IF isentityalive( vehs[mvf_dinghy].id )
								CPRINTLN( DEBUG_MISSION, "SETTING DINGHY ANCHOR FALSE" )
								SET_BOAT_ANCHOR( vehs[mvf_dinghy].id, FALSE )
							ENDIF
							IF IS_NEW_LOAD_SCENE_ACTIVE()
								NEW_LOAD_SCENE_STOP()
							ENDIF
							NEW_LOAD_SCENE_START(<<-143.55, -2489.59, 45.58>>, NORMALISE_VECTOR(<<0.02, 0.97, -0.23>>), 1000 )
							bDialoguePlayed = TRUE
							
						ENDIF
					ENDIF
				ENDIF
			break
			case 13
				IF isentityalive(frank_ped())
					tempEntity = GET_CURRENT_PED_WEAPON_ENTITY_INDEX(frank_ped())
					IF DOES_ENTITY_EXIST(tempEntity)
						REQUEST_WEAPON_HIGH_DETAIL_MODEL(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(tempEntity))
						REQUEST_WEAPON_ASSET(wtSniper, DEFAULT, WEAPON_COMPONENT_SCOPE | WEAPON_COMPONENT_GRIP | WEAPON_COMPONENT_SCLIP2)
					ENDIF
				ENDIF
				IF GET_SYNCHRONIZED_SCENE_PHASE( players_scene_id ) > 0.99
					CLEAR_FOCUS()
					players_scene_id = CREATE_SYNCHRONIZED_SCENE( << -143.8203, -2488.2612, 43.4412 >>, <<0,0,-34.5>>) //<<-143.8, -2487.555, 43.450>>, <<0,0,-34.5>> )
					TASK_SYNCHRONIZED_SCENE(  frank_ped(), players_scene_id, "missheistdocks2aswitchig_7", "IG_7_f_sniping_franklin", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_PROCESS_ATTACHMENTS_ON_START)
					PLAY_SYNCHRONIZED_CAM_ANIM(camera_a, players_scene_id, "IG_7_f_sniping_cam", "missheistdocks2aswitchig_7")
					bDialoguePlayed = FALSE
					mission_substage++
				ELIF GET_SYNCHRONIZED_SCENE_PHASE( players_scene_id ) > 0.5
					IF NOT bDialoguePlayed
						bDialoguePlayed = TRUE
						if IS_NEW_LOAD_SCENE_ACTIVE()
							NEW_LOAD_SCENE_STOP()
						endif
						NEW_LOAD_SCENE_START(<<-139.22,-2486.47,44.71>>,NORMALISE_VECTOR(<<-0.83,-0.56,0.06>>),300)
					ENDIF
				ENDIF
			break
			case 14
				IF isentityalive(frank_ped())
					tempEntity = GET_CURRENT_PED_WEAPON_ENTITY_INDEX(frank_ped())
					IF DOES_ENTITY_EXIST(tempEntity)
						REQUEST_WEAPON_HIGH_DETAIL_MODEL(GET_OBJECT_INDEX_FROM_ENTITY_INDEX(tempEntity))
						REQUEST_WEAPON_ASSET(wtSniper, DEFAULT, WEAPON_COMPONENT_SCOPE | WEAPON_COMPONENT_GRIP | WEAPON_COMPONENT_SCLIP2)
					ENDIF
				ENDIF
				if GET_SYNCHRONIZED_SCENE_PHASE( players_scene_id ) > 0.999
					IF IS_ENTITY_OK(PLAYER_PED_ID())
						CLEAR_PED_TASKS( PLAYER_PED_ID() )
						SET_CURRENT_PED_WEAPON( PLAYER_PED_ID(), wtSniper, TRUE )
						SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(GET_SNIPER_RIFLE_AIM_HEADING_FOR_PED(mercs[merc1_start].id))
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(GET_SNIPER_RIFLE_AIM_PITCH_FOR_PED(mercs[merc1_start].id))
						SET_PLAYER_FORCED_AIM( PLAYER_ID(), TRUE )
						SET_PLAYER_FORCE_SKIP_AIM_INTRO( PLAYER_ID(), TRUE )
						SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(7)
						FORCE_PED_MOTION_STATE( PLAYER_PED_ID(), MS_AIMING )
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(),TRUE)
						SET_PED_NO_TIME_DELAY_BEFORE_SHOT(PLAYER_PED_ID())
					ENDIF
					prep_stop_cutscene(TRUE)
					REPLAY_STOP_EVENT()
					//@RJP make sure Michael has the combat pistol suppressor at the start
					if not HAS_PED_GOT_WEAPON_COMPONENT( mike_ped(), wtcombatpistol, weaponcomponent_at_pi_supp )
						give_weapon_component_to_ped(mike_ped(),wtcombatpistol, weaponcomponent_at_pi_supp)
					endif
					if not IS_AUDIO_SCENE_ACTIVE("DH2A_SNIPE_GUARDS_scene")
						START_AUDIO_SCENE("DH2A_SNIPE_GUARDS_scene")
					endif
					if IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					endif
					IF isentityalive( vehs[mvf_dinghy].id )
						SET_VEHICLE_LOD_MULTIPLIER( vehs[mvf_dinghy].id, 1 )
					ENDIF
					bDialoguePlayed = FALSE
					mission_substage++
				else
					IF NOT bDialoguePlayed
						bDialoguePlayed = TRUE
						if IS_NEW_LOAD_SCENE_ACTIVE()
							NEW_LOAD_SCENE_STOP()
						endif
						NEW_LOAD_SCENE_START_SPHERE( << -95.3159, -2370.9175, 13.2960 >>, 25 )
//						NEW_LOAD_SCENE_START(<<-143.82,-2488.26,45.02>>,NORMALISE_VECTOR(<<0.37,0.90,-0.23>>),200)
					ENDIF
				endif
			break
			case 15
				CPRINTLN( DEBUG_MISSION, "PLAYER SHOULD BE AIMING NOW" )
				Unload_Asset_Anim_Dict(sAssetData,"missheistdocks2aswitchig_6")
				SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), FALSE)
				bDialoguePlayed = false
				if IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				endif
				mission_set_stage(msf_1_snipe_gaurds)
				mission_substage = STAGE_ENTRY
			break
		endswitch		
		
		if IsEntityAlive(FRANK_PED())
		and mission_substage > 6
			if IS_ENTITY_IN_ANGLED_AREA(FRANK_PED(),<< 29.3069, -2545.3843, 1.2836 >>,<< -266.0822, -2313.2078, 20 >>,170)
			and not IS_PED_RAGDOLL(frank_ped())
				Mission_Failed(mff_out_pos)//temp
			endif
		endif		
		
		if IsEntityAlive(FRANK_PED())			
			if IS_ENTITY_IN_ANGLED_AREA(FRANK_PED(),<< -874.0031, -1981.0035, 23.5805 >>,<< 10.8306, -2628.9680, 81.1363 >>,80)
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.01)
			else
				SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1)
			endif
		endif
		
//		clear_players_task_on_control_input(SCRIPT_TASK_GO_STRAIGHT_TO_COORD)
		
	ENDPROC
	PROC ST_1_SNIPE_GUARDS()
		
		set_radar_as_interior_this_frame(get_hash_key("v_fakeboatpo1sh1"),-165.34,-2350.03,0)
		IF PLAYER_PED_ID() = mike_ped()
			SET_RADAR_ZOOM_PRECISE(1)
		ELSE
			SET_RADAR_AS_EXTERIOR_THIS_FRAME()
			SET_RADAR_ZOOM_PRECISE(75)
			SET_PED_NO_TIME_DELAY_BEFORE_SHOT(PLAYER_PED_ID())
		ENDIF
		
		SWITCH mission_substage
			case STAGE_ENTRY
				SET_PICKUP_GENERATION_RANGE_MULTIPLIER(2.0)
				Give_mission_weapons()	
				
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"Stage 1: Snipe Guards")
				
				//mercs
				if not DOES_BLIP_EXIST(mercs[merc1_start].blip)
					mercs[merc1_start].blip = CREATE_BLIP_FOR_PED(mercs[merc1_start].id,true)
				endif
				if not DOES_BLIP_EXIST(mercs[merc2_start].blip)
					mercs[merc2_start].blip = CREATE_BLIP_FOR_PED(mercs[merc2_start].id,true)
				endif
				
				if isentityalive(mercs[merc1_start].id)
					TASK_PLAY_ANIM(mercs[merc1_start].id,"missheistdocks2a","idle_guard",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING)
					IF IS_ENTITY_OK(mercs[merc1_start].id)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mercs[merc1_start].id,true)
					ENDIF
				endif
				if isentityalive(mercs[merc2_start].id)
					TASK_PLAY_ANIM(mercs[merc2_start].id,"missheistdocks2a","idle_guard",SLOW_BLEND_IN,SLOW_BLEND_OUT,-1,AF_LOOPING)
					IF IS_ENTITY_OK(mercs[merc2_start].id)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(mercs[merc2_start].id,true)
					ENDIF
				endif
				
				//mike
				IF IS_ENTITY_OK(MIKE_PED())
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(MIKE_PED(),true)				
					CLEAR_PED_TASKS(mike_ped())
				ENDIF
				
				//set up crates 
				if not DOES_ENTITY_EXIST(objcov1)
					objcov1 =  CREATE_OBJECT (prop_cratepile_07a, <<-215.770004,-2392.330078,5.0>>)
					SET_ENTITY_ROTATION(objcov1, <<-0.000000,0.000000,-29.999996>>)
					SET_ENTITY_LOD_DIST( objcov1, 200 )
				endif
				if not DOES_ENTITY_EXIST(objcov2)
					objcov2 = CREATE_OBJECT (prop_mil_crate_02, <<-211.029999,-2388.360107,5.0>>)
					SET_ENTITY_ROTATION (Objcov2, <<-0.000000,0.000000,-12.000000>>)
					SET_ENTITY_LOD_DIST( objcov2, 200 )
				endif
				if not DOES_ENTITY_EXIST(objcov3)
					objcov3 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-212.509995,-2387.169922,5.0>>)
					set_entity_rotation (objcov3, <<-0.000000,0.000000,-39.999992>> )
					SET_ENTITY_LOD_DIST( objcov3, 200 )
				endif
				if not DOES_ENTITY_EXIST(objcov4)
					OBJCOV4 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-184.7490, -2422.6982, 5.0013>>)
					SET_ENTITY_ROTATION (OBJCOV4, <<0.0,0.0,8.3525>> )
					SET_ENTITY_LOD_DIST( OBJCOV4, 200 )
				endif
				if not DOES_ENTITY_EXIST(objcov5)
					OBJCOV5 = CREATE_OBJECT (PROP_MIL_CRATE_02, <<-193.7419, -2422.9417, 5.0007>>)
					SET_ENTITY_ROTATION (OBJCOV5, <<0.0,0.0,220.8658>> )
					SET_ENTITY_LOD_DIST( OBJCOV5, 200 )
				endif
				//target bool
				bfirstaim = true
				//get rid of trevor now that he is not needed and driven off
				if IsEntityAlive(peds[mpf_trev].id)
					SET_PED_AS_NO_LONGER_NEEDED(peds[mpf_trev].id)					
				endif
				if IsEntityAlive(vehs[mvf_mission_veh].id)
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[mvf_mission_veh].id)
				endif
				if not IS_AUDIO_SCENE_ACTIVE("DH2A_SNIPE_GUARDS_scene")
					START_AUDIO_SCENE("DH2A_SNIPE_GUARDS_scene")
				endif			
				
				bDialoguePlayed = FALSE
				bDialoguePlayed2 = FALSE
				bPrintDisplayed = FALSE
				
				iStairNavMeshBlocker[0] = ADD_NAVMESH_BLOCKING_OBJECT(<<-135.905899,-2383.557861,5.000676>>,<<1.500000,1.500000,1.500000>>,0)
				iStairNavMeshBlocker[1] = ADD_NAVMESH_BLOCKING_OBJECT(<<-214.905899,-2383.557861,5.000676>>,<<1.500000,1.500000,1.500000>>,0)
				mission_substage++
			break
			case 1
				
				//disable controls when sniping in first person
				//since the player can now move in first person while sniping
				DISABLE_PLAYER_MOVEMENT_FOR_FIRST_PERSON_SNIPING()
				
				//display sniping zoom help text, fix for B*2090105
				IF GET_ALLOW_MOVEMENT_WHILE_ZOOMED()
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SNIPE_HLP0")
						CLEAR_HELP(TRUE)
						PRINT_HELP_FOREVER("SNIPE_HLP0")
					ENDIF
				ELSE
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("SNIPE_HLP1")
						CLEAR_HELP(TRUE)
						PRINT_HELP_FOREVER("SNIPE_HLP1")
					ENDIF
				ENDIF
				
				
				if IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(800)
				endif
				IF IsEntityAlive(mercs[merc1_start].id)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mercs[merc1_start].id,FRANK_PED())								
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)					
						SET_ENTITY_HEALTH(mercs[merc1_start].id,0)
					ENDIF
				ENDIF
				IF IsEntityAlive(mercs[merc2_start].id)	
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mercs[merc2_start].id,FRANK_PED())						
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
						SET_ENTITY_HEALTH(mercs[merc2_start].id,0)
					ENDIF
				ENDIF				
				
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF
				
				IF NOT bDialoguePlayed
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DS2A_GO2", CONV_PRIORITY_MEDIUM )
							bDialoguePlayed = TRUE
						ENDIF
					ENDIF
				ELIF NOT bDialoguePlayed2
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DS2A_SHADY", CONV_PRIORITY_MEDIUM )
							bDialoguePlayed2 = TRUE
						ENDIF
					ENDIF
				ELIF NOT bPrintDisplayed
					IF IS_SAFE_TO_DISPLAY_GODTEXT()
						PRINT("DCKH_SNIPER",DEFAULT_GOD_TEXT_TIME,1)
						bPrintDisplayed = TRUE
					ENDIF
				ENDIF
				
				if not IsEntityAlive(mercs[merc1_start].id)
				and not IsEntityAlive(mercs[merc2_start].id)
					bDialoguePlayed = FALSE
					bDialoguePlayed2 = FALSE
					bPrintDisplayed = FALSE
					IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_START( <<-93.05,-2367.14,14.99>>, NORMALISE_VECTOR(<<0.91,0.42,0.03>>), 500 )
					ENDIF
					IF IS_THIS_PRINT_BEING_DISPLAYED( "DCKH_SNIPER" )
						CLEAR_THIS_PRINT( "DCKH_SNIPER" )
					ENDIF
					mission_substage++
				elif not IsEntityAlive(mercs[merc1_start].id)
				and IsEntityAlive(mercs[merc2_start].id)
				
					if bSnipeStageCheck = false
						CLEAR_SEQUENCE_TASK(seq)
						OPEN_SEQUENCE_TASK(seq)							
							task_play_anim(null,"missheistdocks2a@alert",sduckanim,normal_blend_in,NORMAL_BLEND_OUT)
							TASK_AIM_GUN_AT_ENTITY(null,frank_ped(),1000)
							TASK_SHOOT_AT_ENTITY(null,FRANK_PED(),-1,FIRING_TYPE_CONTINUOUS)			
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(mercs[merc2_start].id,seq)
						
						bSnipeStageCheck = true
					endif
					
				elif IsEntityAlive(mercs[merc1_start].id)
				and not IsEntityAlive(mercs[merc2_start].id)
				
					if bSnipeStageCheck = false
						CLEAR_SEQUENCE_TASK(seq)
						OPEN_SEQUENCE_TASK(seq)							
							task_play_anim(null, "missheistdocks2a@crouch", "enter_crouch_a", normal_blend_in, normal_blend_out)
							task_play_anim(null, "missheistdocks2a@crouch", "crouching_idle_a", normal_blend_in, REALLY_SLOW_BLEND_OUT)
							TASK_AIM_GUN_AT_ENTITY(null,frank_ped(),1000)
							TASK_SHOOT_AT_ENTITY(null,FRANK_PED(),-1,FIRING_TYPE_CONTINUOUS)			
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(mercs[merc1_start].id,seq)
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
						bSnipeStageCheck = true 
					endif
				elif IS_SNIPER_BULLET_IN_AREA( GET_ENTITY_COORDS( mercs[merc1_start].id ), <<5,5,5>> )
				OR HAS_PED_RECEIVED_EVENT(mercs[merc1_start].id,EVENT_SHOT_FIRED_BULLET_IMPACT)
				or HAS_PED_RECEIVED_EVENT(mercs[merc2_start].id,EVENT_SHOT_FIRED_BULLET_IMPACT)
					if bSnipeStageCheck = false
						CLEAR_SEQUENCE_TASK(seq)
						OPEN_SEQUENCE_TASK(seq)							
							task_play_anim(null, "missheistdocks2a@crouch", "enter_crouch_a", normal_blend_in, normal_blend_out)
							task_play_anim(null, "missheistdocks2a@crouch", "crouching_idle_a", normal_blend_in, REALLY_SLOW_BLEND_OUT)
							TASK_AIM_GUN_AT_ENTITY(null,frank_ped(),1000)
							TASK_SHOOT_AT_ENTITY(null,FRANK_PED(),-1,FIRING_TYPE_CONTINUOUS)			
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(mercs[merc1_start].id,seq)
						
						CLEAR_SEQUENCE_TASK(seq)
						OPEN_SEQUENCE_TASK(seq)							
							task_play_anim(null,"missheistdocks2a@alert",sduckanim,normal_blend_in,NORMAL_BLEND_OUT)
							TASK_AIM_GUN_AT_ENTITY(null,frank_ped(),1000)
							TASK_SHOOT_AT_ENTITY(null,FRANK_PED(),-1,FIRING_TYPE_CONTINUOUS)			
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(mercs[merc2_start].id,seq)
						REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
						bSnipeStageCheck = true 
					endif
				endif
			break
			case 2
				iCSDelay = GET_GAME_TIMER()
				TRIGGER_MUSIC_EVENT("DH2A_START")
				Give_mission_weapons()					
				if has_ped_got_weapon(mike_ped(),wtCombatPistol)
					SET_CURRENT_PED_WEAPON(mike_ped(),wtCombatPistol,true)
				endif
				//if not IS_ENTITY_AT_COORD(mike_ped(),<<-83.5905, -2361.7979, 14.2963>>,<<5,5,3>>)
				//	SET_ENTITY_COORDS(mike_ped(),<<-83.5905, -2361.7979, 14.2963>>)
				//	set_entity_heading(mike_ped(),155.7045)
				//	CLEAR_PED_TASKS(mike_ped())
				//else
					icutstage = 0
					SCUBA_SUITE_UP(MIKE_PED(), FALSE, FALSE)
				//	SET_PED_STEALTH_MOVEMENT(mike_ped(),true)
				//	CLEAR_PED_TASKS(mike_ped())
				//	SET_CURRENT_PED_WEAPON( mike_ped(), WEAPONTYPE_UNARMED, TRUE )
					mission_substage++
				//endif
			break
			case 3
				if GET_GAME_TIMER()- iCSDelay > 2000
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_SUPP",CONV_PRIORITY_MEDIUM)
							CPRINTLN( DEBUG_MISSION, "CREATED CONVERSATION DH2A_SUPP" )
							REPLAY_RECORD_BACK_FOR_TIME(4.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
							mission_substage++
						ENDIF
					ENDIF
				ENDIF
			break
			case 4
				CLEAR_HELP(TRUE)
				SET_SEETHROUGH(FALSE)
				//b_nightvision_active = FALSE
				players_scene_id = CREATE_SYNCHRONIZED_SCENE( << -143.8203, -2488.2612, 43.4412 >>, <<0,0,-34.5>>) //<<-143.8, -2487.555, 43.450>>, <<0,0,-34.5>> )
				cameraIndex = CREATE_CAMERA( CAMTYPE_ANIMATED, TRUE )
				render_script_cams(true, false)
				TASK_SYNCHRONIZED_SCENE(  frank_ped(), players_scene_id, "missheistdocks2aswitchig_7", "IG_7_F_SNIPING_M_CLIMBING_FRANKLIN", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_PROCESS_ATTACHMENTS_ON_START)
				PLAY_SYNCHRONIZED_CAM_ANIM(cameraIndex, players_scene_id, "IG_7_F_SNIPING_M_CLIMBING_CAM", "missheistdocks2aswitchig_7")
				prep_start_cutscene( false, <<-143.3857, -2488.5540, 43.4383>> )
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				mission_substage++
			break
			case 5
				IF GET_SYNCHRONIZED_SCENE_PHASE( players_scene_id ) > 0.99
					SET_TIME_SCALE(1)					
					iCSDelay = GET_GAME_TIMER()
					players_scene_id = CREATE_SYNCHRONIZED_SCENE(<<-79.7377, -2359.4518, 14.2669>>, <<0,0,155.7045>>)
					CLEAR_PED_TASKS_IMMEDIATELY( mike_ped() )
					SCUBA_SUITE_UP(MIKE_PED(), FALSE, FALSE)
					SET_PED_STEALTH_MOVEMENT(mike_ped(),true)
					SET_CURRENT_PED_WEAPON( mike_ped(), WEAPONTYPE_UNARMED, TRUE )
					task_synchronized_scene( mike_ped(), players_scene_id, "missheistdocks2aig_1", "ig_1_michaelclimboverboat", instant_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT )
					FORCE_PED_AI_AND_ANIMATION_UPDATE( mike_ped() )
					PLAY_SYNCHRONIZED_CAM_ANIM( cameraIndex, players_scene_id, "ig_1_michaelclimboverboat_cam", "missheistdocks2aig_1" )
					SET_SYNCHRONIZED_SCENE_PHASE( players_scene_id, 0.2 )
					if IS_AUDIO_SCENE_ACTIVE("DH2A_SNIPE_GUARDS_scene")
						STOP_AUDIO_SCENE("DH2A_SNIPE_GUARDS_scene")					
					endif
					CPRINTLN( DEBUG_MISSION, "STARTING DH2A_PLANT_BOMBS_SCENE" )
					START_AUDIO_SCENE("DH2A_PLANT_BOMBS_SCENE")
					
//					prep_start_cutscene( false, <<-81.7093, -2358.2287, 14.2969>> )
					sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = peds[mpf_mike].id
					bDialoguePlayed2 = FALSE
					mission_substage++
				ENDIF
			break
			case 6
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_NEXT_CAMERA)
				IF GET_SYNCHRONIZED_SCENE_PHASE( players_scene_id ) > PICK_FLOAT(IS_PLAYER_PLAYING_IN_FIRST_PERSON_ON_FOOT(), fPushInStartPhase, 0.8)  
					IF MAKE_SELECTOR_PED_SELECTION( sSelectorPeds, SELECTOR_PED_MICHAEL )
						IF TAKE_CONTROL_OF_SELECTOR_PED( sSelectorPeds, TRUE, TRUE )

							//handle common stuff when ending synched scene
							IF isentityalive(frank_ped())
								IF GET_SCRIPT_TASK_STATUS( frank_ped(), SCRIPT_TASK_AIM_GUN_AT_ENTITY ) > PERFORMING_TASK
									TASK_AIM_GUN_AT_ENTITY( frank_ped(), mike_ped(), -1 )
								ENDIF
							ENDIF
							
							IF IS_NEW_LOAD_SCENE_ACTIVE()
								NEW_LOAD_SCENE_STOP()
							ENDIF
							
							REPLAY_STOP_EVENT()
							
							bDialoguePlayed  = FALSE
							bDialoguePlayed2 = FALSE

				
							//handle specific stuff depending on camera view mode
							IF IS_PLAYER_PLAYING_IN_FIRST_PERSON_ON_FOOT()
							
								FILL_PUSH_IN_DATA(sPushInData, mike_ped(), CHAR_MICHAEL, fPushInStartDistance, iPushInInterpTime, iPushInCutTime, iPushInFXTime, iPushInSpeedUpTime, fPushInSpeedUpProportion)
								
								mission_substage = 70	//go to special case to handle the push in
							ELSE
							
								SET_GAMEPLAY_CAM_RELATIVE_PITCH()
								SET_GAMEPLAY_CAM_RELATIVE_HEADING()
								
								CLEAR_PED_TASKS( mike_ped() )
								SET_PED_STEALTH_MOVEMENT(mike_ped(), TRUE)
								FORCE_PED_MOTION_STATE( mike_ped(), MS_STEALTHMODE_IDLE, FALSE, FAUS_DEFAULT)
								
								ANIMPOSTFX_PLAY("SwitchSceneMichael", 1000, FALSE)
								PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
								prep_stop_cutscene( true, false, true )
								mission_substage++		//increment the stage as normal
							ENDIF
						ENDIF
					ENDIF
				ELIF GET_SYNCHRONIZED_SCENE_PHASE( players_scene_id ) > 0.5
					IF NOT bDialoguePlayed
						IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_TK1", CONV_PRIORITY_MEDIUM )
							bDialoguePlayed = TRUE
						ENDIF
					ENDIF
				ELIF GET_SYNCHRONIZED_SCENE_PHASE( players_scene_id ) > 0.1
					IF NOT bDialoguePlayed2
						IF IS_NEW_LOAD_SCENE_ACTIVE()
							NEW_LOAD_SCENE_STOP()
						ENDIF
						NEW_LOAD_SCENE_START( <<-79.51,-2362.08,14.38>>, NORMALISE_VECTOR(<<-1.00,0.07,0.00>>), 500 )
						bDialoguePlayed2 = TRUE
					ENDIF
				ENDIF
			break
			case 70	//special case for handling push in when playing in first person
				DISABLE_CONTROL_ACTION(CAMERA_CONTROL, INPUT_NEXT_CAMERA)
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					IF HANDLE_PUSH_IN(sPushInData, bPushInAttached, DEFAULT, DEFAULT, bPushInAttachStartCam, bPushInDoColourFlash)
					
						CLEAR_PED_TASKS( mike_ped() )
						SET_PED_STEALTH_MOVEMENT(mike_ped(), TRUE)
						FORCE_PED_MOTION_STATE( mike_ped(), MS_STEALTHMODE_IDLE, FALSE, FAUS_DEFAULT)

						prep_stop_cutscene( true, false, false )
						mission_substage = 7
					ENDIF
				ENDIF
			break
			case 7
				if TRIGGER_MUSIC_EVENT("DH2A_READY_FOR_2ND")
//					SET_CURRENT_PED_WEAPON( mike_ped(), WEAPONTYPE_STICKYBOMB)
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(mike_ped())
					SPECIAL_ABILITY_FILL_METER(PLAYER_ID(),TRUE)
					bFilledSpecialMeter = TRUE
					CREATE_SCRIPTED_COVER()
					SET_PLAYER_CONTROL(player_id(),true)	
					prep_stop_cutscene(true,false,true)
					KILL_ANY_CONVERSATION()
					CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_02",CONV_PRIORITY_MEDIUM)
					Hotswap_Menu(false,false,true)					
					bHotSwap = true
//					do_stream_vol()

					REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)

					mission_substage++
				endif
			break
			case 8
				//@RJP getting rid of delay before things are blipped and we move to next stage
//				if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				or GET_PROFILE_SETTING(PROFILE_DISPLAY_SUBTITLES)= 0
//					if C4[C4_0_front_of_ship].bPlanted = FALSE
//						print_now("DCKH_PLB1",7500,1)	
//					endif
					
					//Pickups
					CREATE_HEALTH_PICKUPS()
					
					//@RJP unload animdict containing Mike climbing onto boat
					Unload_Asset_Anim_Dict(sAssetData,"missheistdocks2aig_1")
					Unload_Asset_Anim_Dict(sAssetData,"missheistdocks2aswitchig_7")
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"Stage 2: first bomb")
					PRINT_HELP("DCKH_SWHELP")
					mission_set_stage(msf_2_first_bomb)
					mission_substage = STAGE_ENTRY	
//				endif
			break
		endswitch
		
	ENDPROC		
	PROC ST_2_FIRST_BOMB()
	
	set_radar_as_interior_this_frame(get_hash_key("v_fakeboatpo1sh1"),-165.34,-2350.03,0)
	IF PLAYER_PED_ID() = mike_ped()
		SET_RADAR_ZOOM_PRECISE(1)
	ELSE
		SET_RADAR_AS_EXTERIOR_THIS_FRAME()
		SET_RADAR_ZOOM_PRECISE(75)
		SET_PED_NO_TIME_DELAY_BEFORE_SHOT(PLAYER_PED_ID())
	ENDIF
	
	MANAGE_PLAYER_SWITCH_LOAD_SCENES()
	
	Manage_bomb_stages()
	
	//disable controls when sniping in first person
	//since the player can now move in first person while sniping
	DISABLE_PLAYER_MOVEMENT_FOR_FIRST_PERSON_SNIPING()
	
		SWITCH mission_substage			
			case STAGE_ENTRY
				IF IS_ENTITY_OK(vehs[mvf_dinghy].id)
					SET_BOAT_ANCHOR( vehs[mvf_dinghy].id, TRUE )
					SET_VEHICLE_ON_GROUND_PROPERLY( vehs[mvf_dinghy].id )
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling SET_BOAT_ANCHOR() for DINGHY.")
					#ENDIF
				ENDIF
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				imike_ai_stage = 0
				IF isentityalive(mike_ped())
					SET_PED_COMP_ITEM_CURRENT_SP(mike_ped(), COMP_TYPE_PROPS, PROPS_EYES_NONE)
				ENDIF
				IF PLAYER_PED_ID() = mike_ped()
					IF isentityalive(frank_ped())
						TASK_AIM_GUN_AT_ENTITY( frank_ped(), mike_ped(), -1 )
					ENDIF
				ENDIF
				bDialoguePlayed = FALSE
				bPrintDisplayed = FALSE
				mission_substage++
			break	
			case 1		//@RJP new stage to make sure the objective prints
				if IS_SAFE_TO_DISPLAY_GODTEXT()
					if C4[C4_0_front_of_ship].bPlanted = FALSE
						print_now("DCKH_PLB1",DEFAULT_GOD_TEXT_TIME,1)	
					endif
					SET_CURRENT_PED_WEAPON( mike_ped(), WEAPONTYPE_STICKYBOMB)
					mission_substage++
				endif
			break
			case 2
			  	IF IS_THIS_PRINT_BEING_DISPLAYED("DCKH_BOMB1")
				AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), C4[C4_0_front_of_ship].ExpPos, <<3, 1.5, 2>>)
					CLEAR_THIS_PRINT("DCKH_BOMB1")
				ENDIF

				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), C4[C4_0_front_of_ship].ExpPos, <<3,1.5, 2>>)
				AND C4[C4_0_front_of_ship].bPlanted = FALSE
					IF not IS_THIS_PRINT_BEING_DISPLAYED("DCKH_PLB1")					
						IF IS_SAFE_TO_START_CONVERSATION()
							IF NOT bPrintDisplayed
								if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_06",CONV_PRIORITY_MEDIUM)
									bPrintDisplayed = TRUE
								endif
							ENDIF
						ENDIF
						if IS_SAFE_TO_DISPLAY_GODTEXT()											
							Print_now("DCKH_BOMB1",DEFAULT_GOD_TEXT_TIME,1)
						endif
					ENDIF
				ENDIF
									
				IF c4[C4_0_front_of_ship].bPlanted = false
					IF IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-87.442337,-2365.910889,12.796924>>, <<-82.767159,-2365.917480,15.296924>>, 2.5,wtBomb)
					OR C4[C4_0_front_of_ship].bPlanted = TRUE
						if balarmtriggered
							if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_BMB1_NS",CONV_PRIORITY_HIGH)
								SAFE_REMOVE_BLIP(C4[C4_0_front_of_ship].blip)
								c4[C4_0_front_of_ship].bPlanted = true
								bDialoguePlayed = FALSE
								bPrintDisplayed = FALSE
								mission_set_stage(msf_3_second_bomb)
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								mission_substage = STAGE_ENTRY
							endif
						else
							if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_03",CONV_PRIORITY_HIGH)
								SAFE_REMOVE_BLIP(C4[C4_0_front_of_ship].blip)
								c4[C4_0_front_of_ship].bPlanted = true
								bDialoguePlayed = FALSE
								bPrintDisplayed = FALSE
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								mission_set_stage(msf_3_second_bomb)
								mission_substage = STAGE_ENTRY
								REPLAY_RECORD_BACK_FOR_TIME(3, 0.0, REPLAY_IMPORTANCE_HIGH)
							endif
						endif
					ENDIF
				ENDIF
				
			break
		endswitch
		
		IF PLAYER_PED_ID() = frank_ped()
			IF NOT bPrintProtect
			AND NOT sCamDetails.bRun
				if IS_SAFE_TO_DISPLAY_GODTEXT()
					CPRINTLN(DEBUG_MISSION, "GIVE FRANKLIN PROTECT OBJECTIVE")
					PRINT_NOW("DCKH_PROTECT",DEFAULT_GOD_TEXT_TIME,1)
					bPrintProtect = TRUE
				endif
			ENDIF
		ENDIF
	ENDPROC
	PROC ST_3_SECOND_BOMB()
	
	set_radar_as_interior_this_frame(get_hash_key("v_fakeboatpo1sh1"),-165.34,-2350.03,0)
	IF PLAYER_PED_ID() = mike_ped()
		SET_RADAR_ZOOM_PRECISE(1)
	ELSE
		SET_RADAR_AS_EXTERIOR_THIS_FRAME()
		SET_RADAR_ZOOM_PRECISE(75)
		SET_PED_NO_TIME_DELAY_BEFORE_SHOT(PLAYER_PED_ID())
	ENDIF
	
	MANAGE_PLAYER_SWITCH_LOAD_SCENES()
	
	Manage_bomb_stages()
	
	//disable controls when sniping in first person
	//since the player can now move in first person while sniping
	DISABLE_PLAYER_MOVEMENT_FOR_FIRST_PERSON_SNIPING()
	
		SWITCH mission_substage			
			case STAGE_ENTRY
				IF IS_ENTITY_OK(vehs[mvf_dinghy].id)
					SET_BOAT_ANCHOR( vehs[mvf_dinghy].id, TRUE )
					SET_VEHICLE_ON_GROUND_PROPERLY( vehs[mvf_dinghy].id )
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling SET_BOAT_ANCHOR() for DINGHY.")
					#ENDIF
				ENDIF
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(800)
				ENDIF
				IF isentityalive(mike_ped())
					SET_PED_COMP_ITEM_CURRENT_SP(mike_ped(), COMP_TYPE_PROPS, PROPS_EYES_NONE)
				ENDIF
				imike_ai_stage = 0
				bDialoguePlayed = false
				bDialoguePlayed2 = false
				bDialoguePlayed3 = false
				bPrintDisplayed = FALSE
				set_replay_mid_mission_stage_with_name(3,"stage 3: second bomb")
				mission_substage++
			break
			case 1
				IF IS_THIS_PRINT_BEING_DISPLAYED("DCKH_BOMB2")
				AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), C4[C4_1_first_mast].ExpPos, <<1.5, 1.5, 2>>)		
					CLEAR_THIS_PRINT("DCKH_BOMB2")
				ENDIF
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), C4[C4_1_first_mast].ExpPos, <<1.5, 1.5, 2>>)
				AND C4[C4_1_first_mast].bPlanted = FALSE
				and	not sCamDetails.bRun
					IF IS_SAFE_TO_START_CONVERSATION()
						IF NOT bDialoguePlayed2
							if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_06",CONV_PRIORITY_MEDIUM)
								bDialoguePlayed2 = TRUE
							endif
						ENDIF
					ENDIF
					IF IS_SAFE_TO_DISPLAY_GODTEXT()
						//Tell them to place the C4
						Print_now("DCKH_BOMB2",DEFAULT_GOD_TEXT_TIME,1)
					ENDIF
				endif
				
				IF c4[C4_1_first_mast].bPlanted = false				
					IF IS_PROJECTILE_TYPE_IN_ANGLED_AREA((C4[C4_1_first_mast].ExpPos + <<-0.75,3,2>>),(C4[C4_1_first_mast].ExpPos + <<-0.75,-3,-2>>),10,wtBomb) 
					OR C4[C4_1_first_mast].bPlanted = TRUE
						IF balarmtriggered
							if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_BMB2_NS",CONV_PRIORITY_HIGH)
								SAFE_REMOVE_BLIP(C4[C4_1_first_mast].blip)
								c4[C4_1_first_mast].bPlanted = true
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								if not balarmtriggered
									trigger_music_event("dh2a_2nd_bomb_planted")
								endif
								bDialoguePlayed = FALSE
								bDialoguePlayed2 = FALSE
								bDialoguePlayed3 = FALSE
								bPrintDisplayed = FALSE
								mission_set_stage(msf_4_third_bomb)
								mission_substage = STAGE_ENTRY
							endif
						ELSE
							if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_04",CONV_PRIORITY_HIGH)
								SAFE_REMOVE_BLIP(C4[C4_1_first_mast].blip)
								c4[C4_1_first_mast].bPlanted = true
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								if not balarmtriggered
									trigger_music_event("dh2a_2nd_bomb_planted")
								endif
								bDialoguePlayed = FALSE
								bDialoguePlayed2 = FALSE
								bPrintDisplayed = FALSE
								mission_set_stage(msf_4_third_bomb)
								mission_substage = STAGE_ENTRY
								REPLAY_RECORD_BACK_FOR_TIME(3, 0.0, REPLAY_IMPORTANCE_HIGH)
							endif
						ENDIF
					ENDIF
					IF NOT bPrintDisplayed
						IF IS_SAFE_TO_DISPLAY_GODTEXT()
							IF PLAYER_PED_ID() = MIKE_PED()
								Print_now("DCKH_PLB2",DEFAULT_GOD_TEXT_TIME,1)
								bPrintDisplayed = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT bDialoguePlayed3
					AND NOT balarmtriggered
						IF IS_SAFE_TO_START_CONVERSATION()
							IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_SCHAT1", CONV_PRIORITY_MEDIUM )
								bDialoguePlayed3 = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
				//all clear convo 
				if not IsEntityAlive(mercs[merc3_C4_2].id)
				and not IsEntityAlive(mercs[merc4_C4_2].id)
				and not IsEntityAlive(mercs[merc5_C4_2].id)
				and bDialoguePlayed = false
				and not balarmtriggered
					if IS_SAFE_TO_START_CONVERSATION()
						if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_09",CONV_PRIORITY_MEDIUM)
							bDialoguePlayed = true
						endif
					endif
				endif
				
			break
		endswitch
		
		IF PLAYER_PED_ID() = frank_ped()
			IF NOT bPrintProtect
			AND NOT sCamDetails.bRun
				if IS_SAFE_TO_DISPLAY_GODTEXT()
					CPRINTLN(DEBUG_MISSION, "GIVE FRANKLIN PROTECT OBJECTIVE")
					PRINT_NOW("DCKH_PROTECT",DEFAULT_GOD_TEXT_TIME,1)
					bPrintProtect = TRUE
				endif
			ENDIF
		ENDIF
	ENDPROC
	PROC ST_4_THIRD_BOMB()
	
	set_radar_as_interior_this_frame(get_hash_key("v_fakeboatpo1sh1"),-165.34,-2350.03,0)
	IF PLAYER_PED_ID() = mike_ped()
		SET_RADAR_ZOOM_PRECISE(1)
	ELSE
		SET_RADAR_AS_EXTERIOR_THIS_FRAME()
		SET_RADAR_ZOOM_PRECISE(75)
		SET_PED_NO_TIME_DELAY_BEFORE_SHOT(PLAYER_PED_ID())
	ENDIF
	
	MANAGE_PLAYER_SWITCH_LOAD_SCENES()
	
	Manage_bomb_stages()
	
	//disable controls when sniping in first person
	//since the player can now move in first person while sniping
	DISABLE_PLAYER_MOVEMENT_FOR_FIRST_PERSON_SNIPING()
	
		SWITCH mission_substage			
			case STAGE_ENTRY
				IF IS_ENTITY_OK(vehs[mvf_dinghy].id)
					SET_BOAT_ANCHOR( vehs[mvf_dinghy].id, TRUE )
					SET_VEHICLE_ON_GROUND_PROPERLY( vehs[mvf_dinghy].id )
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling SET_BOAT_ANCHOR() for DINGHY.")
					#ENDIF
				ENDIF
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(800)
				ENDIF		
				IF isentityalive(mike_ped())
					SET_PED_COMP_ITEM_CURRENT_SP(mike_ped(), COMP_TYPE_PROPS, PROPS_EYES_NONE)
				ENDIF
				imike_ai_stage = 0
				bDialoguePlayed = false	
				bDialoguePlayed2 = false
				bDialoguePlayed3 = false
				bPrintDisplayed = false
				set_replay_mid_mission_stage_with_name(4,"stage 4: third bomb")
				mission_substage++						
			break
			case 1
				IF IS_THIS_PRINT_BEING_DISPLAYED("DCKH_BOMB2")
				AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), C4[C4_2_second_mast].ExpPos, <<1.5, 1.5, 2>>)					
					CLEAR_THIS_PRINT("DCKH_BOMB2")
				ENDIF
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), C4[C4_2_second_mast].ExpPos, <<1.5, 1.5, 2>>)
				AND C4[C4_2_second_mast].bPlanted = FALSE
				and	not sCamDetails.bRun
					IF IS_SAFE_TO_START_CONVERSATION()
					AND NOT IS_MESSAGE_BEING_DISPLAYED()
						IF NOT bDialoguePlayed2
							if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_06",CONV_PRIORITY_MEDIUM)
								bDialoguePlayed2 = TRUE
							endif
						ENDIF
					ENDIF
					IF IS_SAFE_TO_DISPLAY_GODTEXT()
						Print_now("DCKH_BOMB2",DEFAULT_GOD_TEXT_TIME,1)
					ENDIF
				ENDIF
				
				IF c4[C4_2_second_mast].bPlanted = false
					IF IS_PROJECTILE_TYPE_IN_ANGLED_AREA((C4[C4_2_second_mast].ExpPos + <<-0.75,3,2>>),(C4[C4_2_second_mast].ExpPos + <<-0.75,-3,-2>>),10,wtBomb)
					OR C4[C4_2_second_mast].bPlanted = TRUE
						IF balarmtriggered
							if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_BMB3_NS",CONV_PRIORITY_HIGH)
								SAFE_REMOVE_BLIP(C4[C4_2_second_mast].blip)
								c4[C4_2_second_mast].bPlanted = true
								if not balarmtriggered
									trigger_music_event("dh2a_3rd_bomb_planted")
								endif
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								mission_set_stage(msf_5_fourth_bomb)
								mission_substage = STAGE_ENTRY
							endif
						ELSE
							if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_BMB3",CONV_PRIORITY_HIGH)
								SAFE_REMOVE_BLIP(C4[C4_2_second_mast].blip)
								c4[C4_2_second_mast].bPlanted = true
								if not balarmtriggered
									trigger_music_event("dh2a_3rd_bomb_planted")
								endif
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								bDialoguePlayed3 = FALSE
								mission_set_stage(msf_5_fourth_bomb)
								mission_substage = STAGE_ENTRY
								REPLAY_RECORD_BACK_FOR_TIME(3, 0.0, REPLAY_IMPORTANCE_HIGH)
							endif
						ENDIF
					ENDIF
					IF NOT bPrintDisplayed
					AND NOT balarmtriggered
						IF IS_SAFE_TO_DISPLAY_GODTEXT()
							IF PLAYER_PED_ID() = MIKE_PED()
								Print_now("DCKH_PLB4",DEFAULT_GOD_TEXT_TIME,1)
								bPrintDisplayed = TRUE
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT bDialoguePlayed3
					AND NOT balarmtriggered
						IF IS_SAFE_TO_START_CONVERSATION()
						AND NOT IS_MESSAGE_BEING_DISPLAYED()
							IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_SCHAT2", CONV_PRIORITY_MEDIUM )
								bDialoguePlayed3 = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//all clear convo 
				if not IsEntityAlive(mercs[merc12_C4_3].id)
				and not IsEntityAlive(mercs[merc14_C4_3].id)
				and bDialoguePlayed = false
				and not balarmtriggered
					if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_09",CONV_PRIORITY_MEDIUM)
						bDialoguePlayed = true
					endif
				endif
				
			break			
		endswitch	
		
		IF PLAYER_PED_ID() = frank_ped()
			IF NOT bPrintProtect
			AND NOT sCamDetails.bRun
				if IS_SAFE_TO_DISPLAY_GODTEXT()
					CPRINTLN(DEBUG_MISSION, "GIVE FRANKLIN PROTECT OBJECTIVE")
					PRINT_NOW("DCKH_PROTECT",DEFAULT_GOD_TEXT_TIME,1)
					bPrintProtect = TRUE
				endif
			ENDIF
		ENDIF
	ENDPROC
	PROC ST_5_FOURTH_BOMB()
		
		int i
		
		set_radar_as_interior_this_frame(get_hash_key("v_fakeboatpo1sh1"),-165.34,-2350.03,0)
		IF PLAYER_PED_ID() = mike_ped()
			SET_RADAR_ZOOM_PRECISE(1)
		ELSE
			SET_RADAR_AS_EXTERIOR_THIS_FRAME()
			SET_RADAR_ZOOM_PRECISE(75)
			SET_PED_NO_TIME_DELAY_BEFORE_SHOT(PLAYER_PED_ID())
		ENDIF
		
		IF mission_substage < 3
			MANAGE_PLAYER_SWITCH_LOAD_SCENES()
		ENDIF
		
		//disable controls when sniping in first person
		//since the player can now move in first person while sniping
		DISABLE_PLAYER_MOVEMENT_FOR_FIRST_PERSON_SNIPING()
		
		SWITCH mission_substage
			case STAGE_ENTRY
				IF IS_ENTITY_OK(vehs[mvf_dinghy].id)
					SET_BOAT_ANCHOR( vehs[mvf_dinghy].id, TRUE )
					SET_VEHICLE_ON_GROUND_PROPERLY( vehs[mvf_dinghy].id )
					#IF IS_DEBUG_BUILD
						PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling SET_BOAT_ANCHOR() for DINGHY.")
					#ENDIF
				ENDIF
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(800)
				ENDIF
				imike_ai_stage = 0
				Load_Asset_AnimDict(sAssetData,"missheistdocks2aswitchig_7")
				IF isentityalive(mike_ped())
					SET_PED_COMP_ITEM_CURRENT_SP(mike_ped(), COMP_TYPE_PROPS, PROPS_EYES_NONE)
				ENDIF
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5,"Stage 5: fourth bomb")
				bDialoguePlayed = false
				bDialoguePlayed2 = false
				iCSDelay = GET_GAME_TIMER()
				mission_substage++
			break
			case 1	
				if not balarmtriggered
				and IS_SAFE_TO_START_CONVERSATION()
				and not IS_MESSAGE_BEING_DISPLAYED()
					if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_TK3",CONV_PRIORITY_MEDIUM)
						if not DOES_BLIP_EXIST(C4[C4_3_inside].blip)
							C4[C4_3_inside].blip = CREATE_BLIP_FOR_COORD(c4[C4_3_inside].ExpPos)						
						endif
						Load_Asset_Waypoint(sAssetData,"docksheist2A01")				
						Load_Asset_Model(sAssetData,landstalker)
						Load_Asset_Model(sAssetData,P_PO1_01_DOORM_S)
						Load_Asset_AnimDict(sAssetData, "missheistdocks2aig_1")
						iCSDelay = GET_GAME_TIMER()
//						IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
//							NEW_LOAD_SCENE_START(<<-219.36,-2381.92,14.26>>,NORMALISE_VECTOR(<<-0.39,0.92,-0.08>>),50)
//						ENDIF
						mission_substage++
					endif
				else					
					if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_ENT_NS",CONV_PRIORITY_MEDIUM)
						if not DOES_BLIP_EXIST(C4[C4_3_inside].blip)
							C4[C4_3_inside].blip = CREATE_BLIP_FOR_COORD(c4[C4_3_inside].ExpPos)						
						endif
						Load_Asset_Model(sAssetData,P_PO1_01_DOORM_S)
						Load_Asset_AnimDict(sAssetData, "missheistdocks2aig_1")
						Load_Asset_Waypoint(sAssetData,"docksheist2A01")				
						Load_Asset_Model(sAssetData,landstalker)
						iCSDelay = GET_GAME_TIMER()
						IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
							NEW_LOAD_SCENE_START(<<-219.36,-2381.92,14.26>>,NORMALISE_VECTOR(<<-0.39,0.92,-0.08>>),50)
						ENDIF
						bDialoguePlayed2 = FALSE
						mission_substage++
					endif
				endif
			break	
			case 2
				if IS_ENTITY_AT_COORD(MIKE_PED(),C4[C4_3_inside].ExpPos,<<2,2,2>>)		
				And not sCamDetails.bRun
				AND REQUEST_SCRIPT_AUDIO_BANK("PORT_OF_LS_SHIP_DOOR")
					IF isentityalive( mercs[MERC16_C4_4].id )
						IF CAN_PED_SEE_HATED_PED( mercs[MERC16_C4_4].id, mike_ped() )
						OR IS_PED_IN_COMBAT( mercs[MERC16_C4_4].id, mike_ped() )
							IF NOT bDialoguePlayed2
								IF IS_SAFE_TO_START_CONVERSATION()
									IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_HLP_NS", CONV_PRIORITY_MEDIUM )
										bDialoguePlayed2 = TRUE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							c4[C4_3_inside].bPlanted = true					
							if not sCamDetails.bRun
								bhotswap = false		
							endif			
							Load_Asset_Recording(sAssetData,001,"DH2AREC")
							Load_Asset_Recording(sAssetData,002,"DH2AREC")
							Load_Asset_Recording(sAssetData,003,"DH2AREC")
							load_asset_waypoint(sAssetData,"docksheist2a02")
							load_asset_waypoint(sAssetData,"docksheist2a03")
							Load_Asset_Model(sAssetData,BUZZARD)
							icutstage = 0
							PREPARE_ALARM( "PORT_OF_LS_HEIST_SHIP_ALARMS" )
							mission_substage++
						ENDIF
					ELSE
						c4[C4_3_inside].bPlanted = true					
						if not sCamDetails.bRun
							bhotswap = false		
						endif			
						Load_Asset_Recording(sAssetData,001,"DH2AREC")
						Load_Asset_Recording(sAssetData,002,"DH2AREC")
						Load_Asset_Recording(sAssetData,003,"DH2AREC")
						load_asset_waypoint(sAssetData,"docksheist2a02")
						load_asset_waypoint(sAssetData,"docksheist2a03")
						Load_Asset_Model(sAssetData,BUZZARD)
						icutstage = 0
						PREPARE_ALARM( "PORT_OF_LS_HEIST_SHIP_ALARMS" )
						mission_substage++
					ENDIF
				else
					if not bSternObjDisplayed
						//@RJP only display objective text to go to stern if current player is Michael
						if player_ped_id() = MIKE_PED()
							if IS_SAFE_TO_DISPLAY_GODTEXT()
								CLEAR_PRINTS()
								PRINT_NOW("DCKH_PLB3",DEFAULT_GOD_TEXT_TIME,1)
								bSternObjDisplayed = TRUE
							endif
						endif
					endif
				endif
			break
			case 3
				if ALARM_CUTSCENE()
					icsdelay = GET_GAME_TIMER()
					IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iStairNavMeshBlocker[0])
						REMOVE_NAVMESH_BLOCKING_OBJECT(iStairNavMeshBlocker[0])
					ENDIF
					IF DOES_NAVMESH_BLOCKING_OBJECT_EXIST(iStairNavMeshBlocker[1])
						REMOVE_NAVMESH_BLOCKING_OBJECT(iStairNavMeshBlocker[1])
					ENDIF
					bHotSwap = false
					mission_substage = STAGE_EXIT
				endif
			break
			case STAGE_EXIT		
				for i = enum_to_int(merc1_start) to enum_to_int(merc18_c4_4)
					if IsEntityAlive(mercs[i].id)
						delete_ped(mercs[i].id)
					endif
				endfor
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
					CPRINTLN( DEBUG_MISSION, "ST_5_FOURTH_BOMB: Stopping new load scene." )
				ENDIF
				DISPLAY_SNIPER_SCOPE_THIS_FRAME()
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("PORT_OF_LS_SHIP_DOOR")
//				SET_PLAYER_FORCE_SKIP_AIM_INTRO( PLAYER_ID(), FALSE )
				if IsEntityAlive(mike_ped())
					delete_ped(peds[mpf_mike].id)
				endif
				Unload_Asset_Model(sAssetData,P_PO1_01_DOORM_S)
				Unload_Asset_Anim_Dict(sAssetData, "missheistdocks2aig_1")
				Unload_Asset_Anim_Dict(sAssetData,"missheistdocks2aswitchig_7")
				mission_set_stage(msf_6_way_out)
			break
		endswitch
//================ Mission stage checks	========================
		//carry on with bomb stage untill frank reaches the door.
		if mission_substage < 3
			Manage_bomb_stages()
			if 	not IsEntityAlive(mercs[merc14_C4_3].id)
			and not IsEntityAlive(mercs[merc13_C4_3].id)
//			and not IsEntityAlive(mercs[MERC15_C4_3].id)
			and not IsEntityAlive(mercs[MERC16_C4_4].id)
			and IS_SAFE_TO_START_CONVERSATION()
			and not bDialoguePlayed
				IF PLAYER_PED_ID() = MIKE_PED()
					IF balarmtriggered
						IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_UP_NS", CONV_PRIORITY_MEDIUM )
							bDialoguePlayed = true
						ENDIF
					ELSE
						IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_INVIEW", CONV_PRIORITY_MEDIUM )
							trigger_music_event("DH2A_ALL_CLEAR")
							bDialoguePlayed = true
						ENDIF
					ENDIF
				ELSE
					IF balarmtriggered
						IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_UPM_NS", CONV_PRIORITY_MEDIUM )
							bDialoguePlayed = true
						ENDIF
					ELSE
						IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_UP", CONV_PRIORITY_MEDIUM )
							trigger_music_event("DH2A_ALL_CLEAR")
							bDialoguePlayed = true
						ENDIF
					ENDIF
				ENDIF
			endif
			if player_ped_id() = FRANK_PED()
				SAFE_REMOVE_BLIP(C4[C4_3_inside].blip)
				IF NOT bPrintProtect
				AND NOT sCamDetails.bRun
					if IS_SAFE_TO_DISPLAY_GODTEXT()
						CPRINTLN(DEBUG_MISSION, "GIVE FRANKLIN PROTECT OBJECTIVE")
						PRINT_NOW("DCKH_PROTECT",DEFAULT_GOD_TEXT_TIME,1)
						bPrintProtect = TRUE
					endif
				ENDIF
			else
				if not c4[C4_3_inside].bPlanted
					if not DOES_BLIP_EXIST(c4[C4_3_inside].blip)
						c4[C4_3_inside].blip = CREATE_BLIP_FOR_COORD(c4[C4_3_inside].ExpPos)				
					endif					
				endif
			endif
		endif			
		
	ENDPROC

FUNC BOOL UPDATE_MERC_VEH1( BOOL bCombat = FALSE )
	
	INT i
	
	SWITCH iMercVeh1State
		CASE 0
			//-------merc car 2--------			
			vehs[mvf_merc1].id = CREATE_VEHICLE(landstalker,<< 14.9690, -2524.4229, 5.5353 >>)
			SET_ENTITY_LOD_DIST(vehs[mvf_merc1].id,300)
			SET_ENTITY_LOAD_COLLISION_FLAG(vehs[mvf_merc1].id,true)
			SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_merc1].id,0)
			SET_VEHICLE_CAN_LEAK_PETROL(vehs[mvf_merc1].id,true)
			createMercenary(merc19_car1,150,true,true)
			SET_PED_DIES_WHEN_INJURED( mercs[merc19_car1].id, TRUE )
			SET_COMBAT_FLOAT(mercs[merc19_car1].id, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 0.5)
			SET_BLIP_SCALE(mercs[merc19_car1].blip, BLIP_SIZE_VEHICLE)
			SET_ENTITY_LOD_DIST(mercs[merc19_car1].id,300)
			SET_PED_INTO_VEHICLE(mercs[merc19_car1].id,vehs[mvf_merc1].id,VS_DRIVER)
			createMercenary(merc20_car1,150,true,true)
			SET_PED_DIES_WHEN_INJURED( mercs[merc20_car1].id, TRUE )
			SET_COMBAT_FLOAT(mercs[merc20_car1].id, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 0.5)
			SET_BLIP_SCALE(mercs[merc20_car1].blip, BLIP_SIZE_VEHICLE)
			SET_ENTITY_LOD_DIST(mercs[merc20_car1].id,300)
			SET_PED_INTO_VEHICLE(mercs[merc20_car1].id,vehs[mvf_merc1].id,VS_FRONT_RIGHT)
			createMercenary(merc21_car1,150,true,true)
			SET_PED_DIES_WHEN_INJURED( mercs[merc21_car1].id, TRUE )
			SET_COMBAT_FLOAT(mercs[merc21_car1].id, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 0.5)
			SET_BLIP_SCALE(mercs[merc21_car1].blip, BLIP_SIZE_VEHICLE)
			SET_ENTITY_LOD_DIST(mercs[merc21_car1].id,300)
			SET_PED_INTO_VEHICLE(mercs[merc21_car1].id,vehs[mvf_merc1].id,VS_BACK_LEFT)					
			createMercenary(merc22_car1,150,true,true)
			SET_PED_DIES_WHEN_INJURED( mercs[merc22_car1].id, TRUE )
			SET_COMBAT_FLOAT(mercs[merc22_car1].id, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 0.5)
			SET_BLIP_SCALE(mercs[merc22_car1].blip, BLIP_SIZE_VEHICLE)
			SET_ENTITY_LOD_DIST(mercs[merc22_car1].id,300)
			SET_PED_INTO_VEHICLE(mercs[merc22_car1].id,vehs[mvf_merc1].id,VS_BACK_RIGHT)				
			START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_merc1].id,002,"DH2AREC")			
			SET_PLAYBACK_SPEED(vehs[mvf_merc1].id,0.80)	
			ADD_ENTITY_TO_AUDIO_MIX_GROUP( vehs[mvf_merc1].id, "DH2A_ENEMY_VEHICLES" )
			iMercVeh1State ++
		BREAK
		CASE 1
			if IsEntityAlive(vehs[mvf_merc1].id)
				if not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_merc1].id)
				and IS_VEHICLE_ALMOST_STOPPED(vehs[mvf_merc1].id)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP( vehs[mvf_merc1].id )
					for i = enum_to_int(merc19_car1) to ENUM_TO_INT(merc22_car1)
						int delayleave
						if i = enum_to_int(merc20_car1)
							delayleave = 0
						elif i = enum_to_int(merc21_car1)
							delayleave = 200
						elif i = enum_to_int(merc22_car1)
							delayleave = 800
						else
							delayleave = 1000
						endif
						if IsEntityAlive(mercs[i].id)
							IF DOES_BLIP_EXIST(mercs[i].blip)
								SET_BLIP_SCALE(mercs[i].blip, BLIP_SIZE_PED)
							ENDIF
							SET_PED_SPHERE_DEFENSIVE_AREA(mercs[i].id,mercs[i].vdef4,10,true)
							SET_PED_COMBAT_ATTRIBUTES(MERCS[i].ID,CA_FLEE_WHILST_IN_VEHICLE,FALSE)
							SET_PED_COMBAT_ATTRIBUTES(MERCS[i].ID,CA_USE_VEHICLE,FALSE)
							SET_PED_ACCURACY(MERCS[i].ID,CAR_MERC_ACCURACY)
							OPEN_SEQUENCE_TASK(seq)
								TASK_LEAVE_ANY_VEHICLE( null, delayleave )
								TASK_SWAP_WEAPON( null, TRUE )
								IF bCombat
									SET_PED_SPHERE_DEFENSIVE_AREA(mercs[i].id,mercs[i].vdef3,2,true)
									TASK_COMBAT_PED( null, frank_ped() )
								ELSE
									TASK_FOLLOW_NAV_MESH_TO_COORD( null, mercs[i].vdef4, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP )
									TASK_AIM_GUN_AT_COORD( null, <<-222.0173, -2376.4438, 12.3329>>, -1 )
								ENDIF
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(mercs[i].id,seq)
							CLEAR_SEQUENCE_TASK(seq)				
						endif
					endfor
					iMercVeh1State++
				endif
			else
				iMercVeh1State++
			endif
		BREAK
		CASE 2
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL UPDATE_MERC_VEH2( BOOL bCombat = FALSE )
	
	INT i
	
	SWITCH iMercVeh2State
		CASE 0
			//-------merc car 2--------			
			vehs[mvf_merc2].id = CREATE_VEHICLE(landstalker,<< 14.9690, -2524.4229, 5.5353 >>)
			SET_ENTITY_LOD_DIST(vehs[mvf_merc2].id,300)
			SET_ENTITY_LOAD_COLLISION_FLAG(vehs[mvf_merc2].id,true)
			SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_merc2].id,0)
			SET_VEHICLE_CAN_LEAK_PETROL(vehs[mvf_merc2].id,true)
			createMercenary(merc23_car2,150,true,true)
			SET_PED_DIES_WHEN_INJURED( mercs[merc23_car2].id, TRUE )
			SET_COMBAT_FLOAT(mercs[merc23_car2].id, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 0.5)
			SET_BLIP_SCALE(mercs[merc23_car2].blip, BLIP_SIZE_VEHICLE)
			SET_ENTITY_LOD_DIST(mercs[merc23_car2].id,300)
			SET_PED_INTO_VEHICLE(mercs[merc23_car2].id,vehs[mvf_merc2].id,VS_DRIVER)
			createMercenary(merc24_car2,150,true,true)
			SET_PED_DIES_WHEN_INJURED( mercs[merc24_car2].id, TRUE )
			SET_COMBAT_FLOAT(mercs[merc24_car2].id, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 0.5)
			SET_BLIP_SCALE(mercs[merc24_car2].blip, BLIP_SIZE_VEHICLE)
			SET_ENTITY_LOD_DIST(mercs[merc24_car2].id,300)
			SET_PED_INTO_VEHICLE(mercs[merc24_car2].id,vehs[mvf_merc2].id,VS_FRONT_RIGHT)
			createMercenary(merc25_car2,150,true,true)
			SET_PED_DIES_WHEN_INJURED( mercs[merc25_car2].id, TRUE )
			SET_COMBAT_FLOAT(mercs[merc25_car2].id, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 0.5)
			SET_BLIP_SCALE(mercs[merc25_car2].blip, BLIP_SIZE_VEHICLE)
			SET_ENTITY_LOD_DIST(mercs[merc25_car2].id,300)
			SET_PED_INTO_VEHICLE(mercs[merc25_car2].id,vehs[mvf_merc2].id,VS_BACK_LEFT)					
			createMercenary(merc26_car2,150,true,true)
			SET_PED_DIES_WHEN_INJURED( mercs[merc26_car2].id, TRUE )
			SET_COMBAT_FLOAT(mercs[merc26_car2].id, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 0.5)
			SET_BLIP_SCALE(mercs[merc26_car2].blip, BLIP_SIZE_VEHICLE)
			SET_ENTITY_LOD_DIST(mercs[merc26_car2].id,300)
			SET_PED_INTO_VEHICLE(mercs[merc26_car2].id,vehs[mvf_merc2].id,VS_BACK_RIGHT)				
			START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_merc2].id,001,"DH2AREC")			
			SET_PLAYBACK_SPEED(vehs[mvf_merc2].id,1.0)	
			ADD_ENTITY_TO_AUDIO_MIX_GROUP( vehs[mvf_merc2].id, "DH2A_ENEMY_VEHICLES" )
			iMercVeh2State ++
		BREAK
		CASE 1
			if IsEntityAlive(vehs[mvf_merc2].id)
				if not IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_merc2].id)
				and IS_VEHICLE_ALMOST_STOPPED(vehs[mvf_merc2].id)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP( vehs[mvf_merc2].id )
					for i = enum_to_int(merc23_car2) to ENUM_TO_INT(merc26_car2)
						CPRINTLN(DEBUG_MISSION, "BSW tasking ped #", i, " out")
						int delayleave
						if i = enum_to_int(merc24_car2)
							delayleave = 0
						elif i = enum_to_int(merc25_car2)
							delayleave = 200
						elif i = enum_to_int(merc26_car2)
							delayleave = 400
						else
							delayleave = 600
						endif
						if IsEntityAlive(mercs[i].id)
							IF DOES_BLIP_EXIST(mercs[i].blip)
								SET_BLIP_SCALE(mercs[i].blip, BLIP_SIZE_PED)
							ENDIF
							SET_PED_SPHERE_DEFENSIVE_AREA(mercs[i].id,mercs[i].vdef4,10,true)
							SET_PED_COMBAT_ATTRIBUTES(MERCS[i].ID,CA_FLEE_WHILST_IN_VEHICLE,FALSE)
							SET_PED_COMBAT_ATTRIBUTES(MERCS[i].ID,CA_USE_VEHICLE,FALSE)
							SET_PED_ACCURACY(MERCS[i].ID,CAR_MERC_ACCURACY)
							OPEN_SEQUENCE_TASK(seq)
								TASK_LEAVE_ANY_VEHICLE( null, delayleave )
								TASK_SWAP_WEAPON( null, TRUE )
								IF bCombat
									SET_PED_SPHERE_DEFENSIVE_AREA(mercs[i].id,mercs[i].vdef3,2,true)
									TASK_COMBAT_PED( null, frank_ped() )
								ELSE
									TASK_FOLLOW_NAV_MESH_TO_COORD( null, mercs[i].vdef4, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP )
									TASK_AIM_GUN_AT_COORD( null, <<-222.0173, -2376.4438, 12.3329>>, -1 )
								ENDIF
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(mercs[i].id,seq)
							CLEAR_SEQUENCE_TASK(seq)				
						endif
					endfor
					iMercVeh2State++
				endif
			else
				iMercVeh2State++
			endif
		BREAK
		CASE 2
			RETURN TRUE
		BREAK
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

FUNC BOOL HELI_CONV_MANAGER()
	
	SWITCH iHeliConv
		CASE 0
			//Do nothing in this state
		BREAK
		CASE 1
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_H2",CONV_PRIORITY_MEDIUM)
					CPRINTLN( DEBUG_MISSION, "HELI_CONV_MANAGER: playing DH2A_H2" )
					iHeliConv++
				ENDIF
			ENDIF
		BREAK
		CASE 2
			//Do nothing in this state
		BREAK
		CASE 3
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_H3",CONV_PRIORITY_MEDIUM)	
					CPRINTLN( DEBUG_MISSION, "HELI_CONV_MANAGER: playing DH2A_H3" )
					iHeliConv++
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_DEAL",CONV_PRIORITY_MEDIUM)	
					iHeliConv++
				ENDIF
			ENDIF
		BREAK
		CASE 5
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_MOAN",CONV_PRIORITY_MEDIUM)	
					iHeliConv++
				ENDIF
			ENDIF
		BREAK
		CASE 6
			//Do nothing in this state
		BREAK
		CASE 7
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_H4",CONV_PRIORITY_MEDIUM)	
					iHeliConv++
				ENDIF
			ENDIF
		BREAK
		CASE 8
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_4E",CONV_PRIORITY_MEDIUM)	
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

FUNC BOOL UPDATE_HELICOPTER()
	
	SWITCH iMercHeliState
		CASE 0
			//-------heli--------			
			vehs[mvf_heli].id = CREATE_VEHICLE(BUZZARD,<<-51.4938, -2587.0032, 5.0052>>)
			SET_ENTITY_LOD_DIST(vehs[mvf_heli].id,300)
			SET_ENTITY_LOAD_COLLISION_FLAG(vehs[mvf_heli].id,true)
			SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_heli].id,0)
			createMercenary(merc29_heli,150,true,true)
			SET_BLIP_SCALE( mercs[merc29_heli].blip, BLIP_SIZE_VEHICLE )
			SET_PED_PROP_INDEX( mercs[merc29_heli].id, ANCHOR_HEAD, 2 )
			SET_ENTITY_LOD_DIST(mercs[merc29_heli].id,300)
			SET_PED_INTO_VEHICLE(mercs[merc29_heli].id,vehs[mvf_heli].id,VS_DRIVER)		
			START_PLAYBACK_RECORDED_VEHICLE(vehs[mvf_heli].id,003,"DH2AREC")			
			SET_PLAYBACK_SPEED(vehs[mvf_heli].id,1.0)	
			SET_PED_SHOOT_RATE( mercs[merc29_heli].id, 100 )
			SET_PED_FIRING_PATTERN( mercs[merc29_heli].id, FIRING_PATTERN_BURST_FIRE_HELI )
			SET_CURRENT_PED_VEHICLE_WEAPON(mercs[merc29_heli].id, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
			SET_PED_ACCURACY( mercs[merc29_heli].id, 4 )
			iMercHeliState ++
		BREAK
		CASE 1
			IF isentityalive( vehs[mvf_heli].id )
			AND isentityalive( mercs[merc29_heli].id )
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE( vehs[mvf_heli].id )
					IF GET_TIME_POSITION_IN_RECORDING( vehs[mvf_heli].id ) > 12000
						IF NOT IS_AUDIO_SCENE_ACTIVE("DH2A_CHOPPER_ARRIVES")
							START_AUDIO_SCENE("DH2A_CHOPPER_ARRIVES")
						ENDIF
						ADD_ENTITY_TO_AUDIO_MIX_GROUP( vehs[mvf_heli].id, "DH2A_CHOPPER_group" )
						CPRINTLN( DEBUG_MISSION, "UPDATE_HELICOPTER: Helicopter at bridge, starting audio scene")
						iHeliConv = 1
						iMercHeliState ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 2
			IF isentityalive( vehs[mvf_heli].id )
			AND isentityalive( mercs[merc29_heli].id )
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE( vehs[mvf_heli].id )
					IF GET_TIME_POSITION_IN_RECORDING( vehs[mvf_heli].id ) > 15000
						stop_playback_recorded_vehicle( vehs[mvf_heli].id )
						CPRINTLN( DEBUG_MISSION, "UPDATE_HELICOPTER: Helicopter stopping playback, going to heli mission")
						iHeliConv = 3
						iMercHeliState ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 3
			IF isentityalive( vehs[mvf_heli].id )
			AND isentityalive( mercs[merc29_heli].id )
				IF GET_SCRIPT_TASK_STATUS( mercs[merc29_heli].id, SCRIPT_TASK_VEHICLE_MISSION) > PERFORMING_TASK
				OR GET_GAME_TIMER() - iMercHeliTime > 1000
					TASK_HELI_MISSION( mercs[merc29_heli].id, vehs[mvf_heli].id, null, null, <<-168.6174, -2454.9380, 34.2428>>, MISSION_GOTO,
					40.0, 5, GET_HEADING_FROM_ENTITIES( vehs[mvf_heli].id, frank_ped()), 34, 10, -1, HF_AttainRequestedOrientation )
					CPRINTLN( DEBUG_MISSION, "UPDATE_HELICOPTER: Helicopter tasked to combat spot")
					iMercHeliState ++
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF isentityalive( vehs[mvf_heli].id )
			AND isentityalive( mercs[merc29_heli].id )
				IF GET_SCRIPT_TASK_STATUS( mercs[merc29_heli].id, SCRIPT_TASK_VEHICLE_MISSION) = PERFORMING_TASK
				AND IS_ENTITY_AT_COORD( vehs[mvf_heli].id, <<-168.6174, -2454.9380, 34.2428>>, <<10,10,10>> )
					CLEAR_PED_TASKS( mercs[merc29_heli].id )
					SET_CURRENT_PED_VEHICLE_WEAPON(mercs[merc29_heli].id, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
					TASK_HELI_MISSION( mercs[merc29_heli].id, vehs[mvf_heli].id, null, frank_ped(), GET_ENTITY_COORDS(frank_ped()), MISSION_ATTACK,
					40.0, 40.0, -1, 36, 10 )
					iMercHeliTime = GET_GAME_TIMER()
					i_sound_heli_attack = GET_SOUND_ID()
					bHeliAttackSoundsOn = FALSE
					CPRINTLN( DEBUG_MISSION, "UPDATE_HELICOPTER: Helicopter tasked to attack!")
					iMercHeliState ++
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	IF NOT isentityalive( vehs[mvf_heli].id )
	OR NOT isentityalive( mercs[merc29_heli].id )
		// stop recording only if heli exists
		IF DOES_ENTITY_EXIST(vehs[mvf_heli].id)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE( vehs[mvf_heli].id )
				stop_playback_recorded_vehicle( vehs[mvf_heli].id )
			ENDIF
		ENDIF
		IF iHeliConv < 7
			iHeliConv = 7
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC MANAGE_HELI_ATTACK_SOUNDS()

	IF iMercHeliState <= 4
		EXIT
	ENDIF
	
	IF isentityalive( vehs[mvf_heli].id )
	AND isentityalive( mercs[merc29_heli].id )
		IF NOT bHeliAttackSoundsOn
			IF IS_PED_SHOOTING(mercs[merc29_heli].id)
				IF NOT IS_AUDIO_SCENE_ACTIVE("DH2A_CHOPPER_SHOOTING")
					START_AUDIO_SCENE("DH2A_CHOPPER_SHOOTING")
					CPRINTLN( DEBUG_MISSION, "DH2A_CHOPPER_SHOOTING turning ON")
				ENDIF
				IF HAS_SOUND_FINISHED(i_sound_heli_attack)
					PLAY_SOUND_FROM_ENTITY(i_sound_heli_attack, "CONDUCTORS_PORT_OF_LS_2A_LOOP", vehs[mvf_heli].id)
					CPRINTLN( DEBUG_MISSION, "CONDUCTORS_PORT_OF_LS_2A_LOOP turning ON")
				ENDIF
				bHeliAttackSoundsOn = TRUE
			ENDIF
		ELSE
			IF NOT IS_PED_SHOOTING(mercs[merc29_heli].id)
				IF IS_AUDIO_SCENE_ACTIVE("DH2A_CHOPPER_SHOOTING")
					STOP_AUDIO_SCENE("DH2A_CHOPPER_SHOOTING")
					CPRINTLN( DEBUG_MISSION, "DH2A_CHOPPER_SHOOTING turning OFF")
				ENDIF
				IF NOT HAS_SOUND_FINISHED(i_sound_heli_attack)
					STOP_SOUND(i_sound_heli_attack)
					CPRINTLN( DEBUG_MISSION, "CONDUCTORS_PORT_OF_LS_2A_LOOP turning OFF")
				ENDIF
				bHeliAttackSoundsOn = FALSE
			ENDIF
		ENDIF
	ELSE
		IF IS_AUDIO_SCENE_ACTIVE("DH2A_CHOPPER_SHOOTING")
			STOP_AUDIO_SCENE("DH2A_CHOPPER_SHOOTING")
			CPRINTLN( DEBUG_MISSION, "Heli gone: DH2A_CHOPPER_SHOOTING turning OFF")
		ENDIF
		IF NOT HAS_SOUND_FINISHED(i_sound_heli_attack)
			STOP_SOUND(i_sound_heli_attack)
			CPRINTLN( DEBUG_MISSION, "Heli gone: CONDUCTORS_PORT_OF_LS_2A_LOOP turning OFF")
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_PLAYER_FIRED_ON_CAR_MERCS( BOOL bCarArrived = FALSE )
	
	INT i
	FLOAT fClosestMerc = 1000
	MMF_MISSION_MERC_FLAGS startingMerc
	
	IF bCarArrived
		startingMerc = merc23_car2
	ELSE
		startingMerc = merc27_cars
	ENDIF
	
	//@RJP check if mercs have been fried upon, and get the merc who should react to the sniper fire
	if not bAimAtFranklin
		if piAimAtFranklin = NULL
			for i = startingMerc TO merc28_cars
				if DOES_ENTITY_EXIST(mercs[i].id)
					if is_sniper_bullet_in_area(get_offset_from_entity_in_world_coords(mercs[i].id,<<-5,-5,-1>>),get_offset_from_entity_in_world_coords(mercs[i].id,<<5,5,1>>))
					or HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY( mercs[i].id, PLAYER_PED_ID() )
						if isentityalive( mercs[i].id )
							if not IS_PED_IN_ANY_VEHICLE( mercs[i].id )
								//@RJP merc close to sniper fire will react
								CPRINTLN( DEBUG_MISSION, "Merc ", i," should now react to sniper fire." )
								piAimAtFranklin = mercs[i].id
							endif
						else
							//@RJP get dead peds position for closest merc to react to
							bAimAtFranklin = TRUE
							CPRINTLN( DEBUG_MISSION, "Merc ", i," is dead, closest ped should react to sniper fire." )
							vDeadMerc = mercs[i].vdef4
						endif
					endif
				else
					///@RJP get dead peds position for closest merc to react to, this in case merc has been set as no longer needed
					bAimAtFranklin = TRUE
					CPRINTLN( DEBUG_MISSION, "Merc ", i," is dead, closest ped should react to sniper fire." )
					vDeadMerc = mercs[i].vdef4
				endif
			endfor
		endif
	else
		//@RJP determine which merc is closest to the dead merc
		for i = merc19_car1 TO merc28_cars
			if isentityalive( mercs[i].id )
				if not IS_PED_IN_ANY_VEHICLE( mercs[i].id )
					if GET_DISTANCE_BETWEEN_COORDS( GET_ENTITY_COORDS( mercs[i].id ), vDeadMerc ) < fClosestMerc
						fClosestMerc = GET_DISTANCE_BETWEEN_COORDS( GET_ENTITY_COORDS( mercs[i].id ), vDeadMerc )
						piAimAtFranklin = mercs[i].id
					endif
				endif
			endif
		endfor
	endif
	
	if (piAimAtFranklin != NULL)
		for i = merc19_car1 TO merc28_cars
			IF isentityalive( mercs[i].id )
				IF NOT IS_PED_IN_ANY_VEHICLE(mercs[i].id, TRUE)
					IF NOT bMercSpotterTasked
						CLEAR_PED_TASKS( mercs[i].id )
					ELIF NOT (i = ENUM_TO_INT(merc24_car2))
						CLEAR_PED_TASKS( mercs[i].id )
					ENDIF
					CPRINTLN(DEBUG_MISSION, "BSW clearing task for ped #", i)
				ENDIF
			ENDIF
		endfor
		ADD_PED_FOR_DIALOGUE(convo_struct,8,piAimAtFranklin,"MERRYWEATHER5",true)
		iCSDelay = GET_GAME_TIMER()
		RETURN TRUE
	endif
	
	RETURN FALSE

ENDFUNC

PROC ST_6_WAY_OUT()
//--------------------------------- stage checks ---------------------------------------
	Manage_car_crash_stuff()	
//----------------------------------  substages  ---------------------------------------	
	
	//disable controls when sniping in first person
	//since the player can now move in first person while sniping
	DISABLE_PLAYER_MOVEMENT_FOR_FIRST_PERSON_SNIPING()
	
	set_radar_as_interior_this_frame(get_hash_key("v_fakeboatpo1sh1"),-165.34,-2350.03,0)
	IF PLAYER_PED_ID() = mike_ped()
		SET_RADAR_ZOOM_PRECISE(1)
	ELSE
		SET_RADAR_AS_EXTERIOR_THIS_FRAME()
		SET_RADAR_ZOOM_PRECISE(75)
		SET_PED_NO_TIME_DELAY_BEFORE_SHOT(PLAYER_PED_ID())
	ENDIF
	
	bool bMercsInCarAlive = false // for dialouge of peds shouting up at mike. If all dead dont play. alive ped will set to true
	int i
	
	IF NOT isentityalive( vehs[mvf_heli].id )
		IF IS_AUDIO_SCENE_ACTIVE("DH2A_CHOPPER_ARRIVES")
			CPRINTLN( DEBUG_MISSION, "HELICOPTER DEAD, STOPPING DH2A_CHOPPER_ARRIVES AUDIO SCENE" )
			STOP_AUDIO_SCENE("DH2A_CHOPPER_ARRIVES")
		ENDIF
	ENDIF
	
	SWITCH mission_substage		
		case STAGE_ENTRY
			IF IS_ENTITY_OK(vehs[mvf_dinghy].id)
				SET_BOAT_ANCHOR( vehs[mvf_dinghy].id, TRUE )
				SET_VEHICLE_ON_GROUND_PROPERLY( vehs[mvf_dinghy].id )
				#IF IS_DEBUG_BUILD
					PRINTLN(GET_THIS_SCRIPT_NAME(), ": Calling SET_BOAT_ANCHOR() for DINGHY.")
				#ENDIF
			ENDIF
//			bCarCombatStarted = FALSE
			bAimAtFranklin = FALSE
			bMercSpotterTasked = FALSE
			bDialoguePlayed = FALSE
			bDialoguePlayed2 = FALSE
			piAimAtFranklin = NULL
			ADD_PED_FOR_DIALOGUE(convo_struct,3,FRANK_PED(),"FRANKLIN")	
			ADD_PED_FOR_DIALOGUE(convo_struct,1,null,"MICHAEL")
			vDeadMerc = <<0,0,0>>
			if not IS_AUDIO_SCENE_ACTIVE("DH2A_SHOOTOUT_SNIPING_SCENE")
				START_AUDIO_SCENE("DH2A_SHOOTOUT_SNIPING_SCENE")
			endif
			IF bAimIntro
				SET_PLAYER_FORCED_AIM( PLAYER_ID(), TRUE )
				SET_PLAYER_FORCE_SKIP_AIM_INTRO( PLAYER_ID(), TRUE )
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(get_sniper_rifle_aim_heading_for_coord(<<-222.0200, -2375.3955, 14.2843>>))
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(get_sniper_rifle_aim_pitch_for_coord(<<-222.0200, -2375.3955, 14.2843>>))
				TASK_AIM_GUN_AT_COORD(PLAYER_PED_ID(), <<-222.0200, -2375.3955, 14.2843>>, 10)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID(),TRUE)
				SET_FIRST_PERSON_AIM_CAM_ZOOM_FACTOR(7)
				DISPLAY_SNIPER_SCOPE_THIS_FRAME()
			ENDIF
			if IS_REPLAY_IN_PROGRESS()
			and bis_zSkip = false
				if iMidReplay = 7
					SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7,"Stage 6: way out- B escape")
//					vehs[mvf_merc2].id = CREATE_VEHICLE(landstalker,<<-200.1866, -2403.9558, 5.0004>>, 111.0261)
					vehs[mvf_merc2].id = CREATE_VEHICLE(landstalker,<<-200.3281, -2398.2253, 5.0012>>, 98.1005)
					SET_ENTITY_LOD_DIST(vehs[mvf_merc2].id,300)
					SET_ENTITY_LOAD_COLLISION_FLAG(vehs[mvf_merc2].id,true)
					SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_merc2].id,0)
//					vehs[mvf_merc1].id = CREATE_VEHICLE(landstalker,<<-211.9978, -2407.3733, 5.0004>>, 281.4008)
					vehs[mvf_merc1].id = CREATE_VEHICLE(landstalker,<<-213.6754, -2399.7925, 5.0012>>, 317.5730)
					SET_ENTITY_LOD_DIST(vehs[mvf_merc1].id,300)
					SET_ENTITY_LOAD_COLLISION_FLAG(vehs[mvf_merc1].id,true)
					SET_VEHICLE_COLOUR_COMBINATION(vehs[mvf_merc1].id,0)
					
					bDialoguePlayed = true
					mission_substage = 10
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(800)
					ENDIF
				else
					mission_substage++
				endif
				
			elif not IS_REPLAY_IN_PROGRESS()
			or bis_zSkip
				mission_substage++
			endif				
		break
		
		case 1
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6,"Stage 6: way out- A trucks")
			
			createMercenary(merc27_cars,150,TRUE)
			if isentityalive(mercs[merc27_cars].id)
				SET_PED_DIES_WHEN_INJURED( mercs[merc27_cars].id, TRUE )
				SET_ENTITY_COORDS(mercs[merc27_cars].id,<<-209.39200, -2367.15527, 8.31911>>)
				SET_PED_SPHERE_DEFENSIVE_AREA(mercs[merc27_cars].id,<< -212.01079, -2386.45850, 5.00033 >>,1)
				SET_COMBAT_FLOAT(mercs[merc27_cars].id, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 0.5)
				OPEN_SEQUENCE_TASK(seq)
					TASK_FOLLOW_NAV_MESH_TO_COORD( null, <<-210.4364, -2378.1077, 8.3191>>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP )
					TASK_AIM_GUN_AT_COORD( null, <<-222.0173, -2376.4438, 12.3329>>, -1 )
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(mercs[merc27_cars].id,seq)
				CLEAR_SEQUENCE_TASK(seq)
			endif
			createMercenary(merc28_cars,150,TRUE)	
			if isentityalive(mercs[merc28_cars].id)
				SET_PED_DIES_WHEN_INJURED( mercs[merc28_cars].id, TRUE )
				SET_ENTITY_COORDS(mercs[merc28_cars].id,<<-209.2853, -2375.9495, 8.3191>>)
				SET_PED_SPHERE_DEFENSIVE_AREA(mercs[merc28_cars].id,<< -217.41843, -2391.90576, 5.00033 >>,1)
				SET_COMBAT_FLOAT(mercs[merc28_cars].id, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 0.5)
				OPEN_SEQUENCE_TASK(seq)
					TASK_FOLLOW_NAV_MESH_TO_COORD( null, <<-230.6206, -2376.1785, 8.3191>>, PEDMOVE_RUN, DEFAULT_TIME_NEVER_WARP )
					TASK_AIM_GUN_AT_COORD( null, <<-222.0173, -2376.4438, 12.3329>>, -1 )
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(mercs[merc28_cars].id,seq)
				CLEAR_SEQUENCE_TASK(seq)
			endif								
			iCSDelay = get_game_timer()
			
			mission_substage++
		break
		
		case 2
			IF IS_SAFE_TO_START_CONVERSATION()
				IF bcsalarmtriggered
					IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_OUT_NS", CONV_PRIORITY_MEDIUM )
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						mission_substage++
					ENDIF
				ELSE
					IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_ALRM", CONV_PRIORITY_MEDIUM )
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						mission_substage++
					ENDIF
				ENDIF
			ENDIF
		break
		
		case 3
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(800)
			ENDIF
			
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				NEW_LOAD_SCENE_STOP()
			ENDIF
			
			IF GET_GAME_TIMER() - iCSDelay > 8000
				IF UPDATE_MERC_VEH2()
					IF NOT bDialoguePlayed2
						//convo mike comments on cars
						IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_CLEAR",CONV_PRIORITY_MEDIUM)	
							bDialoguePlayed2 = TRUE
						ENDIF
					ENDIF
					CPRINTLN( DEBUG_MISSION, "UPDATE_MERC_VEH1: vehicle arrived, setting mission_substage++" )
					bDialoguePlayed = FALSE
					iCSDelay = get_game_timer()
					mission_substage++
				ENDIF
			ENDIF
			
			IF HAS_PLAYER_FIRED_ON_CAR_MERCS()
				CPRINTLN( DEBUG_MISSION, "HAS_PLAYER_FIRED_ON_CAR_MERCS: car not arrived yet, setting mission_substage to 4" )
				iCSDelay = GET_GAME_TIMER()
				mission_substage = 4
			ENDIF
			
		break
		
		case 4
			IF HAS_PLAYER_FIRED_ON_CAR_MERCS(TRUE)
				CPRINTLN( DEBUG_MISSION, "HAS_PLAYER_FIRED_ON_CAR_MERCS: setting mission_substage++" )
				iCSDelay = GET_GAME_TIMER()
				mission_substage++
			ELIF NOT bMercSpotterTasked
				IF GET_GAME_TIMER() - iCSDelay > 10000
					if IsEntityAlive(mercs[merc24_car2].id)
						OPEN_SEQUENCE_TASK( seq )
							TASK_FOLLOW_NAV_MESH_TO_COORD( NULL, <<-208.0867, -2403.1211, 5.0012>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP )
							TASK_LOOK_AT_ENTITY( NULL, FRANK_PED(), 100 )
							TASK_PAUSE( NULL, 100 )
							TASK_AIM_GUN_AT_ENTITY( null, frank_ped(), 2000)
							task_play_anim(null,"missheistdocks2a@alert","spot_high",normal_blend_in,NORMAL_BLEND_OUT)
							TASK_COMBAT_PED(NULL, FRANK_PED())
						CLOSE_SEQUENCE_TASK( seq )
						TASK_PERFORM_SEQUENCE( mercs[merc24_car2].id, seq )
						CLEAR_SEQUENCE_TASK( seq )
						bMercSpotterTasked = TRUE
					endif
				ENDIF
			ELSE
				if IsEntityAlive(mercs[merc24_car2].id)
					if IS_ENTITY_PLAYING_ANIM( mercs[merc24_car2].id, "missheistdocks2a@alert", "spot_high" )
						ADD_PED_FOR_DIALOGUE(convo_struct,8,mercs[merc24_car2].id,"MERRYWEATHER5",true)
						CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_CARS",CONV_PRIORITY_MEDIUM)
						CPRINTLN( DEBUG_MISSION, "Merc24 animating spotting player: setting mission_substage++" )
						piAimAtFranklin = mercs[merc24_car2].id
					endif
				endif
			ENDIF
		break
		
		case 5
			IF UPDATE_MERC_VEH2(TRUE)
				IF NOT bDialoguePlayed2
					//convo mike comments on cars
					IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_CLEAR",CONV_PRIORITY_MEDIUM)	
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						bDialoguePlayed2 = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			//@RJP task peds to play anim reacting to sniper fire
			for i = merc19_car1 TO merc28_cars
				if mercs[i].id = piAimAtFranklin
					if not bMercSpotterTasked
						if isentityalive( mercs[i].id )
							if GET_SCRIPT_TASK_STATUS( mercs[i].id, SCRIPT_TASK_PERFORM_SEQUENCE ) > PERFORMING_TASK
								CPRINTLN( DEBUG_MISSION, "Tasking merc to respond to sniper fire." )
								SET_PED_SPHERE_DEFENSIVE_AREA(mercs[i].id,mercs[i].vdef3,2,true)
								CLEAR_PED_TASKS( mercs[i].id )
								OPEN_SEQUENCE_TASK(seq)
									TASK_PAUSE(null, 50)
									TASK_AIM_GUN_AT_ENTITY( null, frank_ped(), 2000)
									task_play_anim(null,"missheistdocks2a@alert","spot_high",normal_blend_in,NORMAL_BLEND_OUT)
									TASK_COMBAT_PED( null, frank_ped())
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(mercs[i].id,seq)
								CLEAR_SEQUENCE_TASK(seq)
							endif
						endif
					elif not (i = ENUM_TO_INT(merc24_car2))
						if isentityalive( mercs[i].id )
							if GET_SCRIPT_TASK_STATUS( mercs[i].id, SCRIPT_TASK_PERFORM_SEQUENCE ) > PERFORMING_TASK
								CPRINTLN( DEBUG_MISSION, "Tasking merc to respond to sniper fire." )
								SET_PED_SPHERE_DEFENSIVE_AREA(mercs[i].id,mercs[i].vdef3,2,true)
								CLEAR_PED_TASKS( mercs[i].id )
								OPEN_SEQUENCE_TASK(seq)
									TASK_PAUSE(null, 50)
									TASK_AIM_GUN_AT_ENTITY( null, frank_ped(), 2000)
									task_play_anim(null,"missheistdocks2a@alert","spot_high",normal_blend_in,NORMAL_BLEND_OUT)
									TASK_COMBAT_PED( null, frank_ped())
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(mercs[i].id,seq)
								CLEAR_SEQUENCE_TASK(seq)
							endif
						endif
					endif
				else
					if isentityalive( mercs[i].id )
						if not IS_PED_IN_ANY_VEHICLE( mercs[i].id )
							if GET_SCRIPT_TASK_STATUS( mercs[i].id, SCRIPT_TASK_PERFORM_SEQUENCE ) > PERFORMING_TASK
								SET_PED_SPHERE_DEFENSIVE_AREA(mercs[i].id,mercs[i].vdef3,2,true)
								CLEAR_PED_TASKS( mercs[i].id )
								OPEN_SEQUENCE_TASK(seq)
									TASK_PAUSE(null, GET_RANDOM_INT_IN_RANGE(100,1000))
									TASK_AIM_GUN_AT_COORD( null, GET_ENTITY_COORDS(frank_ped())+<<GET_RANDOM_INT_IN_RANGE(-100,100),0.0,0.0>>, GET_RANDOM_INT_IN_RANGE(500,1000) )
									TASK_AIM_GUN_AT_COORD( null, GET_ENTITY_COORDS(frank_ped())+<<GET_RANDOM_INT_IN_RANGE(-100,100),0.0,0.0>>, GET_RANDOM_INT_IN_RANGE(500,1000) )
									TASK_AIM_GUN_AT_COORD( null, GET_ENTITY_COORDS(frank_ped())+<<GET_RANDOM_INT_IN_RANGE(-100,100),0.0,0.0>>, GET_RANDOM_INT_IN_RANGE(500,1000) )
									TASK_AIM_GUN_AT_COORD( null, GET_ENTITY_COORDS(frank_ped())+<<GET_RANDOM_INT_IN_RANGE(-100,100),0.0,0.0>>, GET_RANDOM_INT_IN_RANGE(500,1000) )
									TASK_AIM_GUN_AT_ENTITY( null, frank_ped(), GET_RANDOM_INT_IN_RANGE(500,1000) )
									TASK_COMBAT_PED( null, frank_ped())
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(mercs[i].id,seq)
								CLEAR_SEQUENCE_TASK(seq)
							endif
						endif
					endif
				endif
			endfor
			if GET_GAME_TIMER() - iCSDelay > 3500
				CPRINTLN( DEBUG_MISSION, "Merc playing anim, advancing mission_substage to 5." )
				mission_substage++
			endif
		break
		
		case 6
			IF UPDATE_MERC_VEH2(TRUE)
				IF NOT bDialoguePlayed2
					//convo mike comments on cars
					IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_CLEAR",CONV_PRIORITY_MEDIUM)	
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						bDialoguePlayed2 = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT ((piAimAtFranklin = mercs[merc24_car2].id) AND bMercSpotterTasked)
				if IS_SAFE_TO_START_CONVERSATION()
					//god text
					bMercsInCarAlive = false
					for i = enum_to_int(merc19_car1) to ENUM_TO_INT(merc26_car2)
						if IsEntityAlive(mercs[i].id)
							bMercsInCarAlive = true
						endif
					endfor
					if bMercsInCarAlive
						CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_CARS",CONV_PRIORITY_MEDIUM)
					endif
					bDialoguePlayed = false
					mission_substage++
				endif
			ELSE
				bDialoguePlayed = false
				mission_substage++
			ENDIF
		break
		
		case 7
			IF UPDATE_MERC_VEH2(TRUE)
				IF NOT bDialoguePlayed2
					//convo mike comments on cars
					IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_CLEAR",CONV_PRIORITY_MEDIUM)	
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						bDialoguePlayed2 = TRUE
					ENDIF
				ENDIF
				//@RJP handle mercs advancing in cover
				for i = enum_to_int(merc19_car1) to ENUM_TO_INT(merc28_cars)
					MANAGE_CAR_MERC_COMBAT( i )
				endfor
			ENDIF
			
			IF GET_GAME_TIMER() - iCSDelay > 10000
				IF UPDATE_MERC_VEH1(TRUE)
					iCSDelay = GET_GAME_TIMER()
					mission_substage++
				ENDIF
			ENDIF
		break
		
		case 8
			if not bDialoguePlayed	
			and IS_SAFE_TO_START_CONVERSATION()
				ADD_PED_FOR_DIALOGUE(convo_struct,1,null,"Michael")
				if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_PLANT",CONV_PRIORITY_MEDIUM)
					bDialoguePlayed = true
				endif
			endif
			
			IF UPDATE_MERC_VEH2(TRUE)
				IF NOT bDialoguePlayed2
					//convo mike comments on cars
					CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_CLEAR",CONV_PRIORITY_MEDIUM)	
					bDialoguePlayed2 = TRUE
				ENDIF
				//@RJP handle mercs advancing in cover
				for i = enum_to_int(merc19_car1) to ENUM_TO_INT(merc28_cars)
					MANAGE_CAR_MERC_COMBAT( i )
				endfor
			ENDIF
			
			IF HELI_CONV_MANAGER()
				bHeliConv = TRUE
			ENDIF
			
			MANAGE_HELI_ATTACK_SOUNDS()
			
			IF GET_GAME_TIMER() - iCSDelay > 10000
				IF UPDATE_HELICOPTER()
				AND bHeliConv
					bDialoguePlayed = FALSE
					bDialoguePlayed2 = FALSE
					mission_substage++
				ENDIF
			ENDIF
		BREAK
		
		case 9
			bool bCarMercsAlive
			bCarMercsAlive = FALSE
			
			MANAGE_HELI_ATTACK_SOUNDS()
			
			for i = enum_to_int(merc19_car1) to ENUM_TO_INT(merc28_cars)
				if IsEntityAlive(mercs[i].id)
					bCarMercsAlive = TRUE
					//@RJP handle mercs advancing in cover
					MANAGE_CAR_MERC_COMBAT( i )
				endif
			endfor
			if not bCarMercsAlive
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_4GO", CONV_PRIORITY_MEDIUM )
						SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7,"Stage 6: way out- B escape")				
						mission_substage++
					ENDIF
				ENDIF
			else
				IF NOT bDialoguePlayed
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_CLEAR2", CONV_PRIORITY_MEDIUM )
							bDialoguePlayed = TRUE
						ENDIF
					ENDIF
				ENDIF
			endif
		break
		
		case 10
			//mike
			if CREATE_PLAYER_PED_ON_FOOT(peds[mpf_mike].id,CHAR_MICHAEL,<< -220.8900, -2369.9204, 24.3350 >>, 111.7760)	
				SET_PED_HEATSCALE_OVERRIDE( mike_ped(), 0 )
				SET_RAGDOLL_BLOCKING_FLAGS(mike_ped(),RBF_BULLET_IMPACT)
				SET_ENTITY_VISIBLE( mike_ped(), FALSE )
				set_ped_suffers_critical_hits(mike_ped(),false)	
				set_ped_combat_attributes(mike_ped(), ca_use_cover,true)		
				set_ped_combat_attributes(mike_ped(), ca_can_shoot_without_los ,true)
//				set_ped_combat_ability(mike_ped(), cal_professional)
				SET_PED_ACCURACY( mike_ped(), 15 )
				set_ped_combat_movement(mike_ped(), CM_DEFENSIVE)
				set_ped_combat_range(mike_ped(),CR_far)	
				SET_ENTITY_HEALTH(MIKE_PED(),1500)					
				GIVE_WEAPON_TO_PED(MIKE_PED(),wtEnemyWpn,INFINITE_AMMO,true,true)
				SET_PED_RELATIONSHIP_GROUP_HASH(MIKE_PED(),rel_group_buddy)
				SET_ENTITY_LOAD_COLLISION_FLAG(PEDS[MPF_MIKE].ID,TRUE)
				SET_PED_DIES_IN_WATER( PEDS[MPF_MIKE].ID,FALSE )
				IF IS_ENTITY_OK(PEDS[MPF_MIKE].ID)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( PEDS[MPF_MIKE].ID, TRUE )
				ENDIF
				SCUBA_SUITE_UP(MIKE_PED(), FALSE, FALSE)
				ADD_PED_FOR_DIALOGUE(convo_struct,1,peds[mpf_mike].id,"Michael")
				iCSDelay = get_game_timer()
				bfirstaim = false
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF
				mission_substage++
			endif
		break
		
		case 11
			IF IS_SAFE_TO_START_CONVERSATION()
				IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_H1",CONV_PRIORITY_MEDIUM)
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
					mission_substage++
				ENDIF
			ENDIF
		break
		
		case 12
			if get_game_timer() - iCSDelay > 5000
				trigger_music_event("DH2A_CHOPPER_DEAD")
				bDialoguePlayed = false
				iescapeStage = 0
				mission_substage++
			endif	
		break
		
		case 13
			MIKE_WAY_OUT_CONV_MANAGER()
			if MIKE_way_out()
				mission_substage++
			endif
		break
		
		case 14
			MIKE_WAY_OUT_CONV_MANAGER()
			if IS_ENTITY_AT_COORD(MIKE_PED(),<< -210.2508, -2357.4490, 8.5070 >>,<<3,3,2>>)
			or IS_PED_SWIMMING(MIKE_PED())
				SET_SEETHROUGH(FALSE)
				b_nightvision_active = FALSE
				for i = enum_to_int(merc1_start) to enum_to_int(merc26_car2)
					if DOES_ENTITY_EXIST(mercs[i].id)
						DELETE_ped(mercs[i].id)
					endif
				endfor
				//cam
				set_current_ped_weapon(MIKE_PED(),WEAPONTYPE_UNARMED,true)
				cameraIndex = CREATE_CAMERA( CAMTYPE_ANIMATED, TRUE )
				SET_CAM_ACTIVE(cameraIndex, true)
				PLAY_CAM_ANIM( cameraIndex, "michael_jumpsoff_boat_cam", "missheistdocks2a", <<-206.703, -2354.962, 8.354>>, <<0,0,0>> )
				START_AUDIO_SCENE( "DH2A_MICHAEL_DASH_AND_DIVE" )
				Prep_start_Cutscene(false,<<-206.703, -2354.962, 8.354>>)
				iCSDelay = get_game_timer()
//				SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), false)
				SET_PLAYER_FORCED_AIM(PLAYER_ID(), false)
				IF isentityalive(frank_ped())
					TASK_AIM_GUN_AT_COORD( frank_ped(), <<-209.3837, -2352.4211, 8.3192>>, -1 )
				ENDIF
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				bDialoguePlayed = FALSE
				mission_substage++
			endif
		break
		
		case 15
			IF GET_CAM_ANIM_CURRENT_PHASE( cameraIndex ) > 0.99
			OR is_control_just_pressed(frontend_control,INPUT_SKIP_CUTSCENE)		
				IF isentityalive(frank_ped())
					CLEAR_PED_TASKS(frank_ped())
					SET_PLAYER_FORCED_AIM(PLAYER_ID(), false)
				ENDIF
				TRIGGER_MUSIC_EVENT("DH2A_FRANK_JUMPS")
				if is_audio_scene_active("dh2a_shootout_sniping_scene")
					stop_audio_scene("dh2a_shootout_sniping_scene")
				endif
				if is_audio_scene_active("DH2A_MICHAEL_DASH_AND_DIVE")
					stop_audio_scene("DH2A_MICHAEL_DASH_AND_DIVE")
				endif
				REPLAY_STOP_EVENT()
				Prep_stop_Cutscene(true)
				Scuba_suite_up(MIKE_PED(),true)
				bDialoguePlayed = FALSE
				mission_substage++
			ELIF GET_CAM_ANIM_CURRENT_PHASE( cameraIndex ) > 0.7
				IF NOT bDialoguePlayed
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_DIVE", CONV_PRIORITY_MEDIUM )
							bDialoguePlayed = TRUE
						ENDIF
					ENDIF
				ENDIF
			endif
		break
		
		case 16
//			do_stream_vol(false)
			Unload_Asset_Anim_Dict(sAssetData,"missheistdocks2a")
			Mission_Set_Stage(msf_7_detonate)
			Mission_substage = STAGE_ENTRY
		break
	endswitch
	
ENDPROC
PROC ST_7_DETONATE()
	
	set_radar_as_interior_this_frame(get_hash_key("v_fakeboatpo1sh1"),-165.34,-2350.03,0)
	IF PLAYER_PED_ID() = mike_ped()
		SET_RADAR_ZOOM_PRECISE(1)
	ELSE
		SET_RADAR_AS_EXTERIOR_THIS_FRAME()
		SET_RADAR_ZOOM_PRECISE(75)
	ENDIF
	
	if mission_substage < 2
		DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	endif
	
	SWITCH mission_substage
		case STAGE_ENTRY
			STOP_AUDIO_SCENES()
			if not IS_AUDIO_SCENE_ACTIVE("DH2A_PREPARE_DETONATION")
				START_AUDIO_SCENE("DH2A_PREPARE_DETONATION")
			endif
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(800)
			ENDIF
			if DOES_BLIP_EXIST(peds[mpf_mike].blip)
				REMOVE_BLIP(peds[mpf_mike].blip)
			endif
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(8,"Stage 7: detonate")		
			ADD_PED_FOR_DIALOGUE(convo_struct,3,FRANK_PED(),"FRANKLIN")	
			ADD_PED_FOR_DIALOGUE(convo_struct,1,null,"MICHAEL")
			// add detonate contact to phone
			ADD_CONTACT_TO_PHONEBOOK(CHAR_DETONATEBOMB, FRANKLIN_BOOK, FALSE) 
			MAKE_CONTACT_ENTRY_PRIORITY(CHAR_DETONATEBOMB)
			request_cutscene("lsdh_2a_rf_01")
			mission_substage++
		break
		case 1
			IF LOAD_STREAM("PORT_OF_LS_SHIP_BLOW_UP_MASTER")
				mission_substage++
			endif
		break
		case 2
			if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_07",CONV_PRIORITY_MEDIUM)						
				bPrintDisplayed = FALSE
				mission_substage++
			endif
		break
		case 3
			IF IS_CONTACTS_LIST_ON_SCREEN()
				FORCE_SELECTION_OF_THIS_CONTACT_ONLY(CHAR_DETONATEBOMB)
			ENDIF
			IF IS_CALLING_CONTACT(CHAR_DETONATEBOMB)
				IF isentityalive( vehs[mvf_dinghy].id )
					set_entity_coords(vehs[mvf_dinghy].id,<<-80.2056, -2288.2832,0>>)
					set_entity_heading(vehs[mvf_dinghy].id,21)
					SET_BOAT_ANCHOR( vehs[mvf_dinghy].id, TRUE )
				ENDIF
				IF isentityalive( mike_ped() )
					set_entity_coords(mike_ped(),<<-90.4927, -2288.0029, -1.3081>>)
					set_entity_heading(mike_ped(),130.4705)
				ENDIF
				TRIGGER_MUSIC_EVENT("DH2A_DETONATE")
				SET_PLAYER_CONTROL(PLAYER_id(),false,SPC_LEAVE_CAMERA_CONTROL_ON)
				
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
				
				iCSDelay = GET_GAME_TIMER()
				CLEAR_HELP()
				CLEAR_PRINTS()
				bDialoguePlayed = FALSE
				mission_Substage++
			endif
			
			IF NOT bPrintDisplayed
				if IS_SAFE_TO_DISPLAY_GODTEXT()				
					PRINT_NOW("DCKH_DETONATE",DEFAULT_GOD_TEXT_TIME,1)
					PRINT_HELP("DCKH_BOMBH")
					iCSDelay = GET_GAME_TIMER()+10000
					bDialoguePlayed = FALSE
					bPrintDisplayed = TRUE
				endif
			ELIF NOT bDialoguePlayed
				IF GET_GAME_TIMER() > iCSDelay
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION( convo_struct, "D2AAUD", "DH2A_NOBLO", CONV_PRIORITY_MEDIUM )
							bDialoguePlayed = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		break
		case 4
			if get_game_timer() - iCSDelay > 1000
				mission_substage = STAGE_EXIT
			endif
		break
		case STAGE_EXIT
			INT i
			
			balarmtriggered = false
			bcsalarmtriggered = false
			REMOVE_CONTACT_FROM_INDIVIDUAL_PHONEBOOK(CHAR_DETONATEBOMB,FRANKLIN_BOOK)
			unLoad_Asset_Waypoint(sAssetData,"docksheist2A01")
			unLoad_Asset_Waypoint(sAssetData,"docksheist2A02")
			unLoad_Asset_Waypoint(sAssetData,"docksheist2A03")
			unLoad_Asset_Recording(sAssetData,001,"DH2AREC")
			unLoad_Asset_Recording(sAssetData,002,"DH2AREC")
			unLoad_Asset_Recording(sAssetData,003,"DH2AREC")
			Unload_Asset_Anim_Dict(sAssetData,"missheistdocks2a@crouch")
			Unload_Asset_Anim_Dict(sAssetData,"missheistdocks2a@alert")
			Unload_Asset_Model(sAssetData,PROP_CRATEPILE_07A)
			Unload_Asset_Model(sAssetData,PROP_MIL_CRATE_02)
			Unload_Asset_Model(sAssetData,landstalker)
			Unload_Asset_Model(sAssetData,BUZZARD)
			
			REPEAT COUNT_OF(objBomb) i
				if does_entity_exist(objBomb[i])
					delete_object(objBomb[i])
				endif
			ENDREPEAT
			
			//Remove health pickups
			DELETE_HEALTH_PICKUPS()
			mission_Set_Stage(msf_8_explosion_ctscne)
		break
	endswitch
	//@RJP going to the pause menu while the contact list is up here would cause the script to break, this fixes that
	IF NOT IS_PAUSE_MENU_ACTIVE()
		if IS_CONTACTS_LIST_ON_SCREEN()
			if not IS_AUDIO_SCENE_ACTIVE("DH_2A_DETONATE_BOMBS")
				START_AUDIO_SCENE("DH_2A_DETONATE_BOMBS")
			endif
		else
			if IS_AUDIO_SCENE_ACTIVE("DH_2A_DETONATE_BOMBS")
				STOP_AUDIO_SCENE("DH_2A_DETONATE_BOMBS")
			endif
		endif
	ENDIF
	
ENDPROC
INT iWaterModState
PROC HANDLE_WATER_MOD()
	INT i
	SWITCH iWaterModState
		CASE 0
			if GET_CUTSCENE_TIME() > 13250
				CPRINTLN(DEBUG_MISSION, "ADVANCING iWaterModState")
				iWaterModState++
			elif GET_CUTSCENE_TIME() > 13000
				FOR i = -86 TO -246 STEP -10
					MODIFY_WATER( TO_FLOAT(i), -2347.0, 4.0, 0.5)
				ENDFOR
			endif
		BREAK
		CASE 1
			if GET_CUTSCENE_TIME() > 15250
				CPRINTLN(DEBUG_MISSION, "ADVANCING iWaterModState")
				iWaterModState++
			elif GET_CUTSCENE_TIME() > 15000
				FOR i = -81 TO -241 STEP -10
					MODIFY_WATER( TO_FLOAT(i), -2345.0, 4.0, 0.5)
				ENDFOR
			endif
		BREAK
		CASE 2
			if GET_CUTSCENE_TIME() > 19000
				CPRINTLN(DEBUG_MISSION, "ADVANCING iWaterModState")
				iWaterModState++
			elif GET_CUTSCENE_TIME() > 18700
				MODIFY_WATER( -132, -2338.0, 4.0, 0.5)
				MODIFY_WATER( -137, -2338.0, 4.0, 0.5)
				MODIFY_WATER( -142, -2338.0, 4.0, 0.5)
				MODIFY_WATER( -148, -2338.0, 4.0, 0.5)
				MODIFY_WATER( -160, -2338.0, 4.0, 0.5)
			endif
		BREAK
		CASE 3
			if GET_CUTSCENE_TIME() > 19400
				CPRINTLN(DEBUG_MISSION, "ADVANCING iWaterModState")
				iWaterModState++
			elif GET_CUTSCENE_TIME() > 19200
				MODIFY_WATER( -122, -2342.0, 5.0, 0.5)
				MODIFY_WATER( -134, -2344.0, 5.0, 0.5)
				MODIFY_WATER( -143, -2344.0, 5.0, 0.5)
				MODIFY_WATER( -148, -2338.0, 5.0, 0.5)
				MODIFY_WATER( -160, -2338.0, 5.0, 0.5)
			endif
		BREAK
		CASE 4
			if GET_CUTSCENE_TIME() > 20666
				CPRINTLN(DEBUG_MISSION, "going in to explosion timecycle")
				SET_TRANSITION_TIMECYCLE_MODIFIER("ship_explosion_underwater", 0.2)
				iWaterModState++
			endif
		BREAK
		CASE 5
			if GET_CUTSCENE_TIME() > 21666
			AND GET_TIMECYCLE_MODIFIER_INDEX() <> -1
				CPRINTLN(DEBUG_MISSION, "clearing explosion timecycle")
				SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(0.2)
				iWaterModState++
			endif
		BREAK
		CASE 6
			if GET_CUTSCENE_TIME() > 22166
			AND GET_TIMECYCLE_MODIFIER_INDEX() = -1
				CPRINTLN(DEBUG_MISSION, "setting explosion timecycle")
				SET_TRANSITION_TIMECYCLE_MODIFIER("ship_explosion_underwater", 0.2)
				iWaterModState++
			endif
		BREAK
		CASE 7
			if GET_CUTSCENE_TIME() > 22666
			AND GET_TIMECYCLE_MODIFIER_INDEX() <> -1
				CPRINTLN(DEBUG_MISSION, "clearing explosion timecycle")
				SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(0.2)
				iWaterModState++
			endif
		BREAK
		CASE 8
			if GET_CUTSCENE_TIME() > 23000
			AND GET_TIMECYCLE_MODIFIER_INDEX() = -1
				CPRINTLN(DEBUG_MISSION, "setting explosion timecycle")
				SET_TRANSITION_TIMECYCLE_MODIFIER("ship_explosion_underwater", 0.2)
				iWaterModState++
			endif
		BREAK
		CASE 9
			if GET_CUTSCENE_TIME() > 23500
			AND GET_TIMECYCLE_MODIFIER_INDEX() <> -1
				CPRINTLN(DEBUG_MISSION, "clearing explosion timecycle")
				SET_TRANSITION_OUT_OF_TIMECYCLE_MODIFIER(0.2)
				iWaterModState++
			endif
		BREAK
		CASE 10
			if GET_CUTSCENE_TIME() > 24000
				CPRINTLN(DEBUG_MISSION, "ADVANCING iWaterModState")
				iWaterModState++
			elif GET_CUTSCENE_TIME() > 23500
				MODIFY_WATER( -105, -2340.0, 5.0, 0.5)
				MODIFY_WATER( -109, -2341.0, 5.0, 0.5)
				MODIFY_WATER( -114, -2341.0, 5.0, 0.5)
				MODIFY_WATER( -119, -2341.0, 5.0, 0.5)
				MODIFY_WATER( -126, -2344.0, 5.0, 0.5)
				MODIFY_WATER( -127, -2345.0, 5.0, 0.5)
				MODIFY_WATER( -132, -2345.0, 5.0, 0.5)
				MODIFY_WATER( -132, -2340.0, 5.0, 0.5)
				MODIFY_WATER( -132, -2335.0, 5.0, 0.5)
				MODIFY_WATER( -137, -2335.0, 5.0, 0.5)
				MODIFY_WATER( -142, -2335.0, 5.0, 0.5)
				MODIFY_WATER( -148, -2346.0, 5.0, 0.5)
				MODIFY_WATER( -148, -2341.0, 5.0, 0.5)
				MODIFY_WATER( -148, -2336.0, 5.0, 0.5)
				MODIFY_WATER( -148, -2331.0, 5.0, 0.5)
				MODIFY_WATER( -151, -2329.5, 3.0, 0.5)
				MODIFY_WATER( -153, -2331.0, 5.0, 0.5)
				MODIFY_WATER( -157, -2329.5, 3.0, 0.5)
				MODIFY_WATER( -160, -2331.0, 5.0, 0.5)
				MODIFY_WATER( -168, -2339.0, 5.0, 0.5)
				MODIFY_WATER( -173, -2339.0, 5.0, 0.5)
				MODIFY_WATER( -178, -2339.0, 5.0, 0.5)
				MODIFY_WATER( -186, -2342.0, 5.0, 0.5)
				MODIFY_WATER( -194, -2339.0, 5.0, 0.5)
				MODIFY_WATER( -199, -2339.0, 5.0, 0.5)
				MODIFY_WATER( -204, -2339.0, 5.0, 0.5)
				MODIFY_WATER( -213, -2341.0, 5.0, 0.5)
				MODIFY_WATER( -214, -2335.0, 5.0, 0.5)
				MODIFY_WATER( -219, -2335.0, 5.0, 0.5)
				MODIFY_WATER( -224, -2335.0, 5.0, 0.5)
				MODIFY_WATER( -229, -2335.0, 5.0, 0.5)
				MODIFY_WATER( -172, -2339.0, 5.0, 0.5)
				MODIFY_WATER( -167, -2339.0, 5.0, 0.5)
				MODIFY_WATER( -162, -2339.0, 5.0, 0.5)
			endif
		BREAK
		CASE 11
			if GET_CUTSCENE_TIME() > 25500
				CPRINTLN(DEBUG_MISSION, "ADVANCING iWaterModState")
				iWaterModState++
			elif GET_CUTSCENE_TIME() > 24800
				MODIFY_WATER( -124, -2319.0, 5.0, 0.5)
				MODIFY_WATER( -126, -2343.0, 5.0, 0.5)
				MODIFY_WATER( -132, -2344.0, 5.0, 0.5)
				MODIFY_WATER( -132, -2339.0, 5.0, 0.5)
				MODIFY_WATER( -133, -2333.0, 5.0, 0.5)
				MODIFY_WATER( -137, -2333.0, 5.0, 0.5)
				MODIFY_WATER( -142, -2333.0, 5.0, 0.5)
				MODIFY_WATER( -145, -2343.0, 5.0, 0.5)
				MODIFY_WATER( -148, -2346.0, 5.0, 0.5)
				MODIFY_WATER( -148, -2342.0, 5.0, 0.5)
				MODIFY_WATER( -148, -2337.0, 5.0, 0.5)
				MODIFY_WATER( -148, -2332.0, 5.0, 0.5)
				MODIFY_WATER( -152, -2332.0, 5.0, 0.5)
				MODIFY_WATER( -156, -2332.0, 5.0, 0.5)
				MODIFY_WATER( -160, -2332.0, 5.0, 0.5)
				MODIFY_WATER( -162, -2343.0, 5.0, 0.5)
				MODIFY_WATER( -165, -2345.0, 5.0, 0.5)
				MODIFY_WATER( -168, -2338.0, 5.0, 0.5)
				MODIFY_WATER( -173, -2338.0, 5.0, 0.5)
				MODIFY_WATER( -178, -2338.0, 5.0, 0.5)
				MODIFY_WATER( -179, -2345.0, 5.0, 0.5)
				MODIFY_WATER( -187, -2344.0, 5.0, 0.5)
				MODIFY_WATER( -186, -2342.0, 5.0, 0.5)
				MODIFY_WATER( -180, -2346.0, 5.0, 0.5)
				MODIFY_WATER( -185, -2346.0, 5.0, 0.5)
				MODIFY_WATER( -194, -2345.0, 5.0, 0.5)
				MODIFY_WATER( -194, -2340.0, 5.0, 0.5)
				MODIFY_WATER( -199, -2340.0, 5.0, 0.5)
				MODIFY_WATER( -204, -2340.0, 5.0, 0.5)
				MODIFY_WATER( -213, -2341.0, 5.0, 0.5)
				MODIFY_WATER( -214, -2346.0, 5.0, 0.5)
				MODIFY_WATER( -214, -2333.0, 5.0, 0.5)
				MODIFY_WATER( -219, -2333.0, 5.0, 0.5)
				MODIFY_WATER( -224, -2333.0, 5.0, 0.5)
				MODIFY_WATER( -229, -2333.0, 5.0, 0.5)
			endif
		BREAK
	ENDSWITCH
ENDPROC

BOOL bCutsceneSkipped
CAMERA_INDEX ScriptedCamera

PROC ST_8_EXPLOSION_CUTSCENE()

	SWITCH mission_substage
		case STAGE_ENTRY
			DISPLAY_RADAR(FALSE)
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(9,"Stage 8: explosion cutscene")
			IF isentityalive(mike_ped())
				CPRINTLN( DEBUG_MISSION, "SETTING MIKE COORDS" )
				Scuba_suite_up(MIKE_PED(),true)
				set_entity_coords( mike_ped(), <<-180,-2320,0>> )
				IF isentityalive( vehs[mvf_dinghy].id )
					SET_BOAT_ANCHOR(vehs[mvf_dinghy].id,TRUE)
					SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(vehs[mvf_dinghy].id,TRUE)
//					SET_PED_INTO_VEHICLE(mike_ped(),vehs[mvf_dinghy].id)
					SET_ENTITY_VISIBLE( vehs[mvf_dinghy].id, FALSE )
				ENDIF
				SET_ENTITY_VISIBLE( mike_ped(), FALSE)
				SET_FOCUS_ENTITY( mike_ped() )
			ENDIF
			WATER_OVERRIDE_SET_SHOREWAVEAMPLITUDE(0) 
			WATER_OVERRIDE_SET_SHOREWAVEMINAMPLITUDE(0) 
			WATER_OVERRIDE_SET_SHOREWAVEMAXAMPLITUDE(0) 
			WATER_OVERRIDE_SET_OCEANNOISEMINAMPLITUDE(0) 
			WATER_OVERRIDE_SET_OCEANWAVEAMPLITUDE(0) 
			WATER_OVERRIDE_SET_OCEANWAVEMINAMPLITUDE(0) 
			WATER_OVERRIDE_SET_OCEANWAVEMAXAMPLITUDE(0) 
			WATER_OVERRIDE_SET_RIPPLEBUMPINESS(0) 
			WATER_OVERRIDE_SET_RIPPLEMINBUMPINESS(0) 
			WATER_OVERRIDE_SET_RIPPLEMAXBUMPINESS(0) 
			WATER_OVERRIDE_SET_RIPPLEDISTURB(0) 
			WATER_OVERRIDE_SET_STRENGTH(0.1)			
			iWaterModState = 0
			mission_substage++
		break
		case 1					
		//start rayfire
		//rayfire					
			
			if HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(800)
				ENDIF
				SETTIMERA(0)					
				HANG_UP_AND_PUT_AWAY_PHONE()
				IF isentityalive(mike_ped())
					sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = peds[mpf_mike].id
					SET_SELECTOR_PED_BLOCKED(SSELECTORPEDS,SELECTOR_PED_MICHAEL,FALSE) 
					IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds,SELECTOR_PED_MICHAEL)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds,TRUE,TRUE)
						SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(frank_ped())
					ENDIF
				ENDIF
				START_CUTSCENE()
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				SET_CUTSCENE_ORIGIN(<<8.885, -2526.579, 5.046>>,0,0)
				STOP_AUDIO_SCENES()
				if not IS_AUDIO_SCENE_ACTIVE("DH2A_RAYFIRE")
					START_AUDIO_SCENE("DH2A_RAYFIRE")
				endif
				PLAY_STREAM_FRONTEND()
				bHotSwap = false
				bCutsceneSkipped = FALSE
				mission_substage++	
			endif	
		break
		case 2
			if IS_CUTSCENE_ACTIVE()
				load_asset_model(sAssetData,SUBMERSIBLE)
				Load_Asset_Model(sAssetData,Prop_Military_Pickup_01)
				request_ptfx_asset()
				Load_Asset_Model(sAssetData,Prop_Flare_01)
				load_asset_animdict(sAssetData,"missheistdocks2aswitchig_8")
				load_asset_animdict(sAssetData,"swimming@scuba")
				//@RJP delete cover objects and vehicles at this point to free up memory
				IF DOES_ENTITY_EXIST(objcov1)
					DELETE_OBJECT(objcov1)
				ENDIF
				IF DOES_ENTITY_EXIST(objcov2)
					DELETE_OBJECT(objcov2)
				ENDIF
				IF DOES_ENTITY_EXIST(objcov3)
					DELETE_OBJECT(objcov3)
				ENDIF
				IF DOES_ENTITY_EXIST(objcov4)
					DELETE_OBJECT(objcov4)
				ENDIF
				IF DOES_ENTITY_EXIST(objcov5)
					DELETE_OBJECT(objcov5)
				ENDIF
				IF DOES_ENTITY_EXIST(VEHS[mvf_parked1].id)
					DELETE_VEHICLE(VEHS[mvf_parked1].id)
				ENDIF
				IF DOES_ENTITY_EXIST(VEHS[mvf_parked2].id)
					DELETE_VEHICLE(VEHS[mvf_parked2].id)
				ENDIF
				IF DOES_ENTITY_EXIST(VEHS[mvf_merc1].id)
					DELETE_VEHICLE(VEHS[mvf_merc1].id)
				ENDIF
				IF DOES_ENTITY_EXIST(VEHS[mvf_merc2].id)
					DELETE_VEHICLE(VEHS[mvf_merc2].id)
				ENDIF
				SET_SRL_POST_CUTSCENE_CAMERA(<<-75.1, -2286.4, 2.1>>, <<3.1, -0.0, 132.1>>)
				SET_ENTITY_VISIBLE( mike_ped(), FALSE )
				IF DOES_CAM_EXIST(camera_a)
					DESTROY_CAM(camera_a)
				ENDIF
				bDialoguePlayed = FALSE
				bDialoguePlayed2 = FALSE
				mission_substage++
			endif
		break
		case 3
			IF NOT bCanSkip
				IF HAS_MODEL_LOADED( SUBMERSIBLE )
				AND HAS_MODEL_LOADED( Prop_Military_Pickup_01 )
				AND HAS_MODEL_LOADED( Prop_Flare_01 )
				AND HAS_ANIM_DICT_LOADED( "missheistdocks2aswitchig_8" )
				AND HAS_ANIM_DICT_LOADED( "swimming@scuba" )
				AND HAS_PTFX_ASSET_LOADED()
					CPRINTLN( DEBUG_MISSION, "!!!PLAYER CAN SKIP DETONATION CUTSCENE NOW!!!" )
					bCanSkip = TRUE
					SET_CUTSCENE_CAN_BE_SKIPPED(TRUE)
				ELSE
					SET_CUTSCENE_CAN_BE_SKIPPED(FALSE)
				ENDIF
			ENDIF
			
			IF IS_CUTSCENE_PLAYING()
			
				if not bDialoguePlayed
					if GET_CUTSCENE_TIME() > 17500
						TRIGGER_MUSIC_EVENT("DH2A_FINAL_EXP")						
						bDialoguePlayed = TRUE
					endif
				endif
			
				IF GET_CUTSCENE_TIME() > 22300
					REQUEST_IPL( "SUNK_SHIP_FIRE" )
				ENDIF
			
				IF NOT bSwitchOccluders
					IF GET_CUTSCENE_TIME() > 21000
						set_building_state( BUILDINGNAME_IPL_CARGOSHIP_OCCLUSION, BUILDINGSTATE_DESTROYED )
						bSwitchOccluders = true
						CPRINTLN( DEBUG_MISSION, "DETONATE CUTSCENE: Swapping ship occluders to destroyed state" )
					ENDIF
				ENDIF
				
				//match last cutscene shot with scripted camera and render from it after cutscene ends
				IF NOT DOES_CAM_EXIST(ScriptedCamera)
					ScriptedCamera = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
					//SET_CAM_PARAMS(ScriptedCamera, <<-121.454781,-2298.167480,20.978750>>,<<-19.987669,0.997791,133.803146>>,40.000000)
					SET_CAM_PARAMS(ScriptedCamera,<<-122.356133,-2297.994629,21.029718>>,<<-19.659889,1.225318,134.042130>>,40.000000)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
				ENDIF
				
				HANDLE_WATER_MOD()
			
			ENDIF

			IF NOT DOES_RAYFIRE_MAP_OBJECT_EXIST(rayfire_ship)
				CPRINTLN(DEBUG_MISSION, "RJM - Getting rayfire object of the boat")
				rayfire_ship = GET_RAYFIRE_MAP_OBJECT(<<-159.7051, -2374.3184, 5.0459>>, 50.0, "des_shipsink")
			ENDIF
					
			IF HAS_CUTSCENE_FINISHED()
				
				IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(players_scene_id)
					
					//while rendering from scripted camera matching last cutscene shot, wait for end rayfire end state and start synched scene
					IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rayfire_ship) AND GET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfire_ship) = RFMO_STATE_END
					
						IF 	isentityalive( vehs[mvf_dinghy].id )
						AND isentityalive( mike_ped() )
							set_entity_coords(vehs[mvf_dinghy].id,<<-78.1857, -2287.6528, 0.0>>)
							set_entity_heading(vehs[mvf_dinghy].id, 56.7927)
							SET_ENTITY_VISIBLE( vehs[mvf_dinghy].id, TRUE )
							SET_ENTITY_VISIBLE( mike_ped(), TRUE )
							SET_PED_INTO_VEHICLE( mike_ped(), vehs[mvf_dinghy].id )
							//SET_VEHICLE_ON_GROUND_PROPERLY( vehs[mvf_dinghy].id )
							SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(vehs[mvf_dinghy].id,TRUE)
							SET_BOAT_ANCHOR(vehs[mvf_dinghy].id,TRUE)
						ENDIF
						
						IF ( bCutsceneSkipped = TRUE )
							CLEAR_TIMECYCLE_MODIFIER()
							STOP_FIRE_IN_RANGE(<<-78.1857, -2287.6528, 0.0>>, 1000.0)
							REMOVE_DECALS_IN_RANGE(<<-78.1857, -2287.6528, 0.0>>, 1000.0)
							REMOVE_PARTICLE_FX_IN_RANGE(<<-78.1857, -2287.6528, 0.0>>, 1000.0)
						ENDIF
						
						IF HAS_ANIM_DICT_LOADED( "missheistdocks2aswitchig_8" )
							IF NOT DOES_CAM_EXIST(camera_a)
								CPRINTLN( DEBUG_MISSION, "CAM A DOES NOT EXIT: STARTING SYNC SCENE 1" )
								camera_a = CREATE_CAM_WITH_PARAMS( "default_scripted_camera", <<-75.1, -2286.4, 1.5>>, <<3.1, -0.0, 132.1>>, 50 )
								set_cam_active(camera_a, true)
								render_script_cams(true, false)
								if isentityalive( vehs[mvf_dinghy].id )
									ATTACH_CAM_TO_ENTITY( camera_a, vehs[mvf_dinghy].id, << 3.6348, -1.745, 1.15 >> )
								endif
								players_scene_id = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
								task_synchronized_scene(player_ped_id(), players_scene_id, "missheistdocks2aswitchig_8", "ig_8_switch", instant_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)	
								ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(players_scene_id, vehs[mvf_dinghy].id, 0)
								SET_SYNCHRONIZED_SCENE_PHASE( players_scene_id, 0.35 )
								PLAY_SOUND_FROM_ENTITY(-1,"BODY_FALL_DIVE_WATER_MASTER",player_ped_id(),"docks_heist_finale_2a_sounds")
							ENDIF
						ENDIF

						PLAY_SOUND_FROM_ENTITY(-1,"BODY_FALL_DIVE_WATER_MASTER",player_ped_id(),"docks_heist_finale_2a_sounds")
						
						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) != CAM_VIEW_MODE_FIRST_PERSON
							PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
							ANIMPOSTFX_PLAY("SwitchSceneMichael", 1000, FALSE)
						ENDIF
						
					ENDIF

				ELSE
				
					CLEAR_FOCUS()
					STOP_STREAM()
					REQUEST_IPL( "SUNK_SHIP_FIRE" )
					
					if IS_AUDIO_SCENE_ACTIVE("DH2A_RAYFIRE")
						STOP_AUDIO_SCENE("DH2A_RAYFIRE")
					endif
					
					if not IS_AUDIO_SCENE_ACTIVE("DH2A_SWIM_TO_CONTAINER")
						STOP_AUDIO_SCENES()
						START_AUDIO_SCENE("DH2A_SWIM_TO_CONTAINER")
					endif
				
					int i
					for i = 0 to enum_to_int(MMF_NUM_OF_MERCS)-1
						if DOES_ENTITY_EXIST(mercs[i].id)
							DELETE_PED(mercs[i].id)
						endif
					endfor	
	
					bHotSwap = false
					
					DISPLAY_RADAR(TRUE)
					SET_PLAYER_CONTROL(PLAYER_id(),true)
					
					manage_ipls(BUILDINGSTATE_DESTROYED)
				
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
										
					REPLAY_STOP_EVENT()

					mission_substage++
				
				ENDIF
				
			ELSE
			
				IF ( bCutsceneSkipped = FALSE )
					IF WAS_CUTSCENE_SKIPPED()
						STOP_STREAM()
						SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)
						bCutsceneSkipped = TRUE
					ENDIF
				ENDIF
			
			ENDIF
			
		break	
		case 4
			REPLAY_STOP_EVENT()
			bCanSkip = FALSE
			mission_set_Stage(msf_9_the_goods)
			mission_substage = STAGE_ENTRY	
		break
	endswitch
ENDPROC

FUNC BOOL CREATE_CORPSE(MMF_MISSION_MERC_FLAGS merc)
	
	VECTOR vCorpseSpawnPos
	
	IF PLAYER_PED_ID() = mike_ped()
		vCorpseSpawnPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( PLAYER_PED_ID(), <<GET_RANDOM_FLOAT_IN_RANGE(-2,2),8,3>> )
	ELSE
		vCorpseSpawnPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS( PLAYER_PED_ID(), <<GET_RANDOM_FLOAT_IN_RANGE(-3,3),12,6>> )
	ENDIF
	IF NOT WOULD_ENTITY_BE_OCCLUDED(S_M_Y_BLACKOPS_01, vCorpseSpawnPos)
		RETURN FALSE
	ENDIF
	CPRINTLN( DEBUG_MISSION, "CORPSE_HANDLER: Creating corpse at", vCorpseSpawnPos )
	createmercenary(merc)
	SET_ENTITY_HEALTH(mercs[merc].id,0)
	SET_ENTITY_COORDS(mercs[merc].id,vCorpseSpawnPos)
	SET_ENTITY_HEADING(mercs[merc].id,GET_RANDOM_FLOAT_IN_RANGE(0,360))
	
	RETURN TRUE
	
ENDFUNC

PROC CORPSE_HANDLER()
	
	IF NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-231.434326,-2319.805664,-42.960861>>, <<-99.622238,-2323.623047,8.504173>>, 40.000000)
		EXIT
	ENDIF
	
	VECTOR vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	IF PLAYER_PED_ID() = mike_ped()
		IF vPlayerPos.z > -5
			EXIT
		ENDIF
	ELSE
		IF vPlayerPos.z > -8
			EXIT
		ENDIF
	ENDIF
	
	
	
	SWITCH iCorpseHandlerState
		CASE 0
			IF CREATE_CORPSE( merc13_C4_3 )
				iCorpseTimer = GET_GAME_TIMER()+GET_RANDOM_INT_IN_RANGE(20000,25000)
				iCorpseHandlerState ++
			ENDIF
		BREAK
		CASE 1
			IF GET_GAME_TIMER() > iCorpseTimer
				iCorpseHandlerState ++
			ENDIF
		BREAK
		CASE 2
			IF CREATE_CORPSE( merc14_C4_3 )
				iCorpseTimer = GET_GAME_TIMER()+GET_RANDOM_INT_IN_RANGE(20000,25000)
				iCorpseHandlerState ++
			ENDIF
		BREAK
		CASE 3
			IF GET_GAME_TIMER() > iCorpseTimer
				iCorpseHandlerState ++
			ENDIF
		BREAK
		CASE 4
			IF CREATE_CORPSE( MERC15_C4_3 )
				iCorpseTimer = GET_GAME_TIMER()+GET_RANDOM_INT_IN_RANGE(20000,25000)
				iCorpseHandlerState ++
			ENDIF
		BREAK
		CASE 5
			IF GET_GAME_TIMER() > iCorpseTimer
				iCorpseHandlerState ++
			ENDIF
		BREAK
		CASE 6
			IF CREATE_CORPSE( MERC16_C4_4 )
				iCorpseTimer = GET_GAME_TIMER()+GET_RANDOM_INT_IN_RANGE(20000,25000)
				iCorpseHandlerState ++
			ENDIF
		BREAK
		CASE 7
			IF GET_GAME_TIMER() > iCorpseTimer
				iCorpseHandlerState ++
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC POLICE_HELI_HANDLER()
	
	IF mission_substage = 10
		IF isentityalive( vehs[mvf_policeHeli].id )
		AND isentityalive( peds[mpf_heli].id )
			iPoliceHeliState = 100
		ENDIF
	ENDIF
	
	SWITCH iPoliceHeliState
		CASE 0
			Load_Asset_Model( sAssetData, POLMAV )
			Load_Asset_Model( sAssetData, S_M_Y_SWAT_01 )
			iPoliceHeliState++
		BREAK
		CASE 1
			IF HAS_MODEL_LOADED( POLMAV )
			AND HAS_MODEL_LOADED( S_M_Y_SWAT_01 )
				CPRINTLN( DEBUG_MISSION, "POLICE_HELI_HANDLER: heli and swat loaded" )
				iPoliceHeliState++
			ENDIF
		BREAK
		CASE 2
			vehs[mvf_policeHeli].id = CREATE_VEHICLE( POLMAV, <<-92.8689, -2015.0421, 17.0169>>, 214.9528 )
			spawn_ped( peds[mpf_heli].id, S_M_Y_SWAT_01, <<-89.4357, -2014.3889, 17.0169>>, 190.5788 )
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS( peds[mpf_heli].id, TRUE )
			SET_PED_INTO_VEHICLE( peds[mpf_heli].id, vehs[mvf_policeHeli].id )
			SET_VEHICLE_SEARCHLIGHT( vehs[mvf_policeHeli].id, TRUE, TRUE )
			SET_VEHICLE_LIVERY( vehs[mvf_policeHeli].id, 0 )
			TASK_HELI_MISSION( peds[mpf_heli].id, vehs[mvf_policeHeli].id, NULL, NULL, <<-85.5841, -2281.5090, 57.3485>>, MISSION_GOTO, 30, 10, -1, 55, 20, -1 )
			Unload_Asset_Model( sAssetData, POLMAV )
			Unload_Asset_Model( sAssetData, S_M_Y_SWAT_01 )
			CPRINTLN( DEBUG_MISSION, "POLICE_HELI_HANDLER: heli and swat created" )
			iPoliceHeliState++
		BREAK
		CASE 3
			IF isentityalive( vehs[mvf_policeHeli].id )
			AND isentityalive( peds[mpf_heli].id )
				IF IS_ENTITY_AT_COORD( peds[mpf_heli].id, <<-85.5841, -2281.5090, 57.3485>>, <<30,30,30>> )
					CPRINTLN( DEBUG_MISSION, "POLICE_HELI_HANDLER: heli has arrived" )
					iPoliceHeliState++
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF isentityalive( vehs[mvf_policeHeli].id )
			AND isentityalive( peds[mpf_heli].id )
				CLEAR_PED_TASKS( peds[mpf_heli].id )
				OPEN_SEQUENCE_TASK(seq)
					TASK_HELI_MISSION( NULL, vehs[mvf_policeHeli].id, NULL, NULL, <<-97.1803, -2340.7891, 26.4520>>, MISSION_GOTO, 20, 10, 96.0322, 40, 20, -1, HF_AttainRequestedOrientation )
					TASK_HELI_MISSION( NULL, vehs[mvf_policeHeli].id, NULL, NULL, <<-157.1283, -2400.2373, 38.6996>>, MISSION_GOTO, 20, 10, 6.1374, 40, 20, -1, HF_AttainRequestedOrientation )
					TASK_HELI_MISSION( NULL, vehs[mvf_policeHeli].id, NULL, NULL, <<-255.1758, -2371.6716, 30.6111>>, MISSION_GOTO, 20, 10, 303.2508, 40, 20, -1, HF_AttainRequestedOrientation )
					TASK_HELI_MISSION( NULL, vehs[mvf_policeHeli].id, NULL, NULL, <<-266.8602, -2297.8379, 26.4934>>, MISSION_GOTO, 20, 10, -132.2165, 40, 20, -1, HF_AttainRequestedOrientation )
					TASK_HELI_MISSION( NULL, vehs[mvf_policeHeli].id, NULL, NULL, <<-157.1116, -2301.1860, 19.5018>>, MISSION_GOTO, 20, 10, 174.5608, 40, 20, -1, HF_AttainRequestedOrientation )
					SET_SEQUENCE_TO_REPEAT( seq, REPEAT_FOREVER )
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE( peds[mpf_heli].id, seq )
				CLEAR_SEQUENCE_TASK(seq)
				CPRINTLN( DEBUG_MISSION, "POLICE_HELI_HANDLER: heli tasked to circle" )
				iPoliceHeliState++
			ENDIF
		BREAK
		CASE 5
			IF isentityalive( peds[mpf_heli].id )
				IF GET_SCRIPT_TASK_STATUS( peds[mpf_heli].id, SCRIPT_TASK_PERFORM_SEQUENCE ) > PERFORMING_TASK
					CPRINTLN( DEBUG_MISSION, "POLICE_HELI_HANDLER: heli got off task, retasking" )
					iPoliceHeliState = 4
				ENDIF
			ENDIF
		BREAK
		CASE 100
			IF isentityalive( vehs[mvf_policeHeli].id )
			AND isentityalive( peds[mpf_heli].id )
				CLEAR_PED_TASKS( peds[mpf_heli].id )
				TASK_HELI_MISSION( peds[mpf_heli].id, vehs[mvf_policeHeli].id, NULL, NULL, <<723.2252, -1478.7260, 119.2312>>, MISSION_GOTO, 30, 10, -1, 55, 20, -1 )
				SET_PED_KEEP_TASK( peds[mpf_heli].id, TRUE )
				SET_PED_AS_NO_LONGER_NEEDED( peds[mpf_heli].id )
				SET_VEHICLE_AS_NO_LONGER_NEEDED( vehs[mvf_policeHeli].id )
				CPRINTLN( DEBUG_MISSION, "POLICE_HELI_HANDLER: heli tasked to leave area and no longer needed" )
				iPoliceHeliState++
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC

PROC ST_9_THE_GOODS()
//	IF mission_substage < 10
//		CORPSE_HANDLER()
//	ENDIF
	
	POLICE_HELI_HANDLER()
	
	set_radar_as_interior_this_frame(get_hash_key("v_fakeboatpo1sh1sunk"),-165.34,-2350.03,0)
	SET_RADAR_AS_EXTERIOR_THIS_FRAME()
	IF PLAYER_PED_ID() = mike_ped()
		SET_RADAR_ZOOM_PRECISE(1)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
		SET_PED_RESET_FLAG( PLAYER_PED_ID(), PRF_DisableTakeOffScubaGear, TRUE )
	ELSE
		SET_RADAR_ZOOM_PRECISE(85)
	ENDIF
	
	IF mission_substage < 9
		IF isentityalive(mike_ped())
			IF IS_PED_SWIMMING(mike_ped())
				IF GET_PED_COMP_ITEM_CURRENT_SP( mike_ped(), COMP_TYPE_SPECIAL ) != SPECIAL_P0_SCUBA_ACCS_1
					SET_PED_COMP_ITEM_CURRENT_SP(mike_ped(),COMP_TYPE_SPECIAL,SPECIAL_P0_SCUBA_ACCS_1,FALSE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	VECTOR vMikeArrivedPos = <<-191.0846, -2330.0593, -19.2324>>
	VECTOR vMikeArrivedRot = <<0.0, 0.0, 180.0>>
	
	SWITCH mission_substage
		case STAGE_ENTRY	
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(800)
				ENDIF
				bcsalarmtriggered 	= false
				balarmtriggered 	= false
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(10,"Stage 9: the goods",true)
			//sub
				if IsEntityAlive(vehs[mvf_sub].id)
					SET_ENTITY_COORDS(vehs[mvf_sub].id,<<-466.7967, -2381.3794, -12.1213>>)
					SET_ENTITY_HEADING(vehs[mvf_sub].id, 279.5468  )
					FREEZE_ENTITY_POSITION(vehs[mvf_sub].id,true)
				else						
					vehs[mvf_sub].id = CREATE_VEHICLE(SUBMERSIBLE,<<-466.7967, -2381.3794, -12.1213>>, 279.5468  )
					SET_VEHICLE_AS_RESTRICTED(vehs[mvf_sub].id,0)
					SET_VEHICLE_IGNORED_BY_QUICK_SAVE(vehs[mvf_sub].id)
					FREEZE_ENTITY_POSITION(vehs[mvf_sub].id,true)
				endif
			//crate
				objLoot = CREATE_OBJECT(Prop_Military_Pickup_01,<<-191.12270, -2331.33813, -19.40053>>)
			//mike				
				Scuba_suite_up(MIKE_PED(),true)
				SET_PED_DIES_IN_WATER(MIKE_PED(),false)
				//RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT( mike_ped() )	//commented out as it was restoring weapons too early
			//misc
			Load_Asset_AnimDict(sAssetData,"missheistdocks2a@swimtocontainer")
			ADD_PED_FOR_DIALOGUE(convo_struct,1,MIKE_PED(),"MICHAEL")
			ALLOW_DIALOGUE_IN_WATER(TRUE)
			if not IS_AUDIO_SCENE_ACTIVE("DH2A_SWIM_TO_CONTAINER")
				START_AUDIO_SCENE("DH2A_SWIM_TO_CONTAINER")
			endif
//			OVERRIDE_UNDERWATER_STREAM("DH_FINALE_2A_STREAM", TRUE)
			
			WATER_OVERRIDE_SET_SHOREWAVEAMPLITUDE(0) 
			WATER_OVERRIDE_SET_SHOREWAVEMINAMPLITUDE(0) 
			WATER_OVERRIDE_SET_SHOREWAVEMAXAMPLITUDE(0) 
			WATER_OVERRIDE_SET_OCEANNOISEMINAMPLITUDE(0) 
			WATER_OVERRIDE_SET_OCEANWAVEAMPLITUDE(0) 
			WATER_OVERRIDE_SET_OCEANWAVEMINAMPLITUDE(0) 
			WATER_OVERRIDE_SET_OCEANWAVEMAXAMPLITUDE(0) 
			WATER_OVERRIDE_SET_RIPPLEBUMPINESS(0) 
			WATER_OVERRIDE_SET_RIPPLEMINBUMPINESS(0) 
			WATER_OVERRIDE_SET_RIPPLEMAXBUMPINESS(0) 
			WATER_OVERRIDE_SET_RIPPLEDISTURB(0) 
			WATER_OVERRIDE_SET_STRENGTH(0.1)
			
			IF DOES_ENTITY_EXIST(vehs[mvf_mission_veh].id)
				DELETE_VEHICLE(vehs[mvf_mission_veh].id)
			ENDIF
			
			IF IS_ENTITY_OK( vehs[mvf_dinghy].id )
				SET_VEHICLE_ON_GROUND_PROPERLY( vehs[mvf_dinghy].id )
			ENDIF
			
			IF NOT DOES_CAM_EXIST(camera_a)
				CPRINTLN( DEBUG_MISSION, "CAM A DOES NOT EXIT: STARTING SYNC SCENE 3" )
				camera_a = CREATE_CAM_WITH_PARAMS( "default_scripted_camera", <<-75.1, -2286.4, 1.5>>, <<3.1, -0.0, 132.1>>, 50 )
				set_cam_active(camera_a, true)
				render_script_cams(true, false)
				if isentityalive( vehs[mvf_dinghy].id )
					ATTACH_CAM_TO_ENTITY( camera_a, vehs[mvf_dinghy].id, << 3.6348, -1.745, 1.15 >> )
				endif
				players_scene_id = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
				task_synchronized_scene(player_ped_id(), players_scene_id, "missheistdocks2aswitchig_8", "ig_8_switch", instant_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)	
				ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(players_scene_id, vehs[mvf_dinghy].id, 0)
				SET_SYNCHRONIZED_SCENE_PHASE( players_scene_id, 0.35 )
				PLAY_SOUND_FROM_ENTITY(-1,"BODY_FALL_DIVE_WATER_MASTER",player_ped_id(),"docks_heist_finale_2a_sounds")
			ENDIF
			REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)	
			bPrintDisplayed = FALSE
			bPrintDisplayed2 = FALSE
			bDialoguePlayed = FALSE
			bDialoguePlayed2 = FALSE
			bDialoguePlayed3 = FALSE
			bDialoguePlayed4 = FALSE
			bSonarUpdate = TRUE
			bStartSonar = FALSE
			bForcedRayfire = FALSE
			bFirstPersonFlashPlayed = FALSE
			
			//stats
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(DH2A_TIME_TO_FIND_CONTAINER)
			CREATE_CONVERSATION(convo_struct, "D2AAUD", "DH2A_ROLL", CONV_PRIORITY_MEDIUM)
			mission_substage++
			
			#IF IS_DEBUG_BUILD
			//bSwitchCamDebug_SkipToSwitch = TRUE
			#ENDIF
		break
		case 1
			IF GET_SYNCHRONIZED_SCENE_PHASE( players_scene_id ) > 0.99
			OR NOT IS_SYNCHRONIZED_SCENE_RUNNING(players_scene_id)
				// DO THIS HERE AGAIN AS SAFETY
				if not does_particle_fx_looped_exist(flare_ptfx)
					objFlare = create_object(PROP_FLARE_01, get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 0.0, 1.0>>))
					attach_entity_to_entity(objFlare, player_ped_id(), get_ped_bone_index(player_ped_id(), BONETAG_PH_L_HAND), <<0.07, 0.0, 0.03>>, <<-90.0, 0.0, 0.0>>)
					flare_ptfx = START_PARTICLE_FX_LOOPED_ON_entity("scr_bio_flare", objFlare, <<0.0, 0.0, 0.11>>, <<0.0, 0.0, 0.0>>) 
					TRIGGER_MUSIC_EVENT("DH2A_DIVER")
				endif
				
				IF DOES_CAM_EXIST( camera_a )
					IF IS_CAM_RENDERING( camera_a )
						render_script_cams(FALSE, FALSE)
					ENDIF
					DESTROY_CAM( camera_a )
				ENDIF
				IF DOES_CAM_EXIST( camera_b )
					DESTROY_CAM( camera_b )
				ENDIF
				OPEN_SEQUENCE_TASK(seq)
					TASK_PLAY_ANIM(NULL, "SWIMMING@scuba", "dive_run", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, 1000, AF_LOOPING )
					TASK_FORCE_MOTION_STATE(null, enum_to_int(MS_diving_swim))
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
				CLEAR_SEQUENCE_TASK(seq)
				SET_BOAT_REMAINS_ANCHORED_WHILE_PLAYER_IS_DRIVER(vehs[mvf_dinghy].id,FALSE)
				//Set water back to normal
				WATER_OVERRIDE_SET_STRENGTH(0)
				SET_TRANSITION_TIMECYCLE_MODIFIER("PORT_heist_underwater", 0.5)

				bStartSonar = TRUE
				mission_substage++
			ELIF GET_SYNCHRONIZED_SCENE_PHASE( players_scene_id ) > 0.675
				IF IS_CAM_RENDERING( camera_a )
					render_script_cams(FALSE, FALSE)
					REPLAY_STOP_EVENT()
				ENDIF
			ELIF GET_SYNCHRONIZED_SCENE_PHASE( players_scene_id ) > 0.65
				if not does_particle_fx_looped_exist(flare_ptfx)
					objFlare = create_object(PROP_FLARE_01, get_offset_from_entity_in_world_coords(player_ped_id(), <<0.0, 0.0, 1.0>>))
					attach_entity_to_entity(objFlare, player_ped_id(), get_ped_bone_index(player_ped_id(), BONETAG_PH_L_HAND), <<0.07, 0.0, 0.03>>, <<-90.0, 0.0, 0.0>>)
					flare_ptfx = START_PARTICLE_FX_LOOPED_ON_entity("scr_bio_flare", objFlare, <<0.0, 0.0, 0.11>>, <<0.0, 0.0, 0.0>>) 
					TRIGGER_MUSIC_EVENT("DH2A_DIVER")
				endif
				//IF IS_CAM_RENDERING( camera_a )
				//	render_script_cams(FALSE, FALSE)
				//ENDIF
				
				IF NOT bForcedRayfire
					IF NOT DOES_RAYFIRE_MAP_OBJECT_EXIST(rayfire_ship)
						rayfire_ship = GET_RAYFIRE_MAP_OBJECT(<<-159.7051, -2374.3184, 5.0459>>, 50.0, "des_shipsink")
					ELSE
						CPRINTLN(DEBUG_MISSION, "RJM - Forcing the Rayfire to ENDING state")
						SET_STATE_OF_RAYFIRE_MAP_OBJECT(rayfire_ship, RFMO_STATE_ENDING)
						REQUEST_IPL( "sunkcargoship" )
						bForcedRayfire = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(players_scene_id)
				IF GET_SYNCHRONIZED_SCENE_PHASE( players_scene_id ) > fFirstPersonFlashPhase1
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
						IF ( bFirstPersonFlashPlayed = FALSE )
							ANIMPOSTFX_PLAY(PICK_STRING(IS_REPLAY_IN_PROGRESS(), "CamPushInNeutral", "CamPushInMichael"), 0, FALSE)
							PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
							bFirstPersonFlashPlayed = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF

		break
		case 2
			if CREATE_PLAYER_PED_INSIDE_VEHICLE(peds[mpf_trev].id,CHAR_TREVOR,vehs[mvf_sub].id)
				sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = peds[mpf_trev].id
				Unload_Asset_Anim_Dict( sAssetData, "missheistdocks2aswitchig_8" )
				Unload_Asset_Anim_Dict( sAssetData, "SWIMMING@scuba" )
				Unload_Asset_Model( sAssetData, PROP_FLARE_01 )
				ADD_PED_FOR_DIALOGUE(convo_struct,2,peds[mpf_trev].id,"TREVOR")
//				print_help("DCKH_SCUBH")
				mission_substage++
			endif
		break
		case 3
			#IF IS_DEBUG_BUILD
			IF bSwitchCamDebug_SkipToSwitch != TRUE
			#ENDIF
			
				if IS_SAFE_TO_START_CONVERSATION()
					if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_FRCRT",CONV_PRIORITY_MEDIUM)					
						mission_substage++
					endif
				endif
				
			#IF IS_DEBUG_BUILD
			ELSE
				mission_substage++
			ENDIF
			#ENDIF
		break
		case 4
			if not bDialoguePlayed2
				if not bPrintDisplayed
					if IS_SAFE_TO_DISPLAY_GODTEXT()
						Print_now("DCKH_LOCCARGO",DEFAULT_GOD_TEXT_TIME,1)
						bPrintDisplayed = TRUE
					endif
				elif not bDialoguePlayed
					if IS_SAFE_TO_START_CONVERSATION()
						if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_LOOK",CONV_PRIORITY_MEDIUM)					
							CPRINTLN( DEBUG_MISSION, "CONVERSATION: Michael says he's looking" )
							bDialoguePlayed = TRUE
						endif
					endif
				elif not bDialoguePlayed4
					if IS_SAFE_TO_START_CONVERSATION()
						if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_HOLDUP",CONV_PRIORITY_MEDIUM)					
							CPRINTLN( DEBUG_MISSION, "CONVERSATION: Trevor says what's the hold up" )
							bDialoguePlayed4 = TRUE
						endif
					endif
				endif
			endif
			if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id()),GET_ENTITY_COORDS(objLoot)) < 15
				if not does_blip_exist(blipcrate)
					blipcrate = CREATE_BLIP_FOR_OBJECT(objLoot)
				endif
				if not bDialoguePlayed2
					if IS_SAFE_TO_START_CONVERSATION()
						if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_DEVICE",CONV_PRIORITY_MEDIUM)					
							CPRINTLN( DEBUG_MISSION, "CONVERSATION: Michael sees the device" )
							bDialoguePlayed2 = TRUE
						endif
					endif
				elif not bDialoguePlayed3
					if IS_SAFE_TO_START_CONVERSATION()
						if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_GO",CONV_PRIORITY_MEDIUM)					
							CPRINTLN( DEBUG_MISSION, "CONVERSATION: Trevor tells Michael to get it" )
							bDialoguePlayed3 = TRUE
						endif
					endif
				endif
			endif
			if not IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DCKH_SCUBH")
				if not bSonarHelp
					print_help("DCKH_SONARH")
					bSonarHelp = TRUE
				endif
			endif
			if IS_ENTITY_AT_COORD(MIKE_PED(),GET_ENTITY_COORDS(objLoot),<<5,5,4>>)
				bStopRebreather = TRUE
				players_scene_id = CREATE_SYNCHRONIZED_SCENE( vMikeArrivedPos, vMikeArrivedRot )

				cameraIndex = CREATE_CAMERA(CAMTYPE_ANIMATED, TRUE)
				TASK_SYNCHRONIZED_SCENE(  mike_ped(), players_scene_id, "missheistdocks2a@swimtocontainer", "Michael_Swim_to_Container", INSTANT_BLEND_IN, NORMAL_BLEND_OUT )
				PLAY_SYNCHRONIZED_CAM_ANIM( cameraIndex, players_scene_id, "CAM_Swim_to_Container", "missheistdocks2a@swimtocontainer")
				set_cam_active(cameraindex,true)
				Prep_start_Cutscene(false,<< -195.9108, -2318.3926, -18.6068 >>,true,false)
				if DOES_BLIP_EXIST(blipCrate)
					REMOVE_BLIP(blipCrate)
				endif
				REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGH)
						
				//misc	
					iCSDelay = GET_GAME_TIMER()
				//stats
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
				IF isentityalive(vehs[mvf_sub].id)
					SET_VEHICLE_DOORS_LOCKED(vehs[mvf_sub].id,VEHICLELOCK_LOCKED_PLAYER_INSIDE)
					SET_VEHICLE_FULLBEAM(vehs[mvf_sub].id,true)
				ENDIF
				REMOVE_CURRENT_SONAR_BLIP()
				bPrintDisplayed = FALSE
				bPrintDisplayed2 = FALSE
				bDialoguePlayed = FALSE
				bDialoguePlayed2 = FALSE
				bDialoguePlayed3 = FALSE
				bDialoguePlayed4 = FALSE
				
				mission_substage++
				eSwitchCamState = SWITCH_CAM_REQUEST_ASSETS
				HANDLE_SWITCH_CAM_MICHAEL_TO_TREVOR(scsSwitchCam_MichaelToTrevor)
			endif
		break
		case 5
			IF NOT bDialoguePlayed
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_CONF",CONV_PRIORITY_MEDIUM)
						bDialoguePlayed = TRUE
					ENDIF
				ENDIF
			ENDIF
			IF GET_SYNCHRONIZED_SCENE_PHASE( players_scene_id ) > 0.43
				bDialoguePlayed = FALSE
				bDialoguePlayed2 = FALSE
				Hotswap_Menu(false,true,false,false,false,false)
				eSwitchCamState = SWITCH_CAM_SETUP_SPLINE				
				
				IF DOES_ENTITY_EXIST(objFlare)
					IF IS_ENTITY_ATTACHED(objFlare)
						detach_entity(objFlare)
					ENDIF
				ENDIF
				
				//help for trevor.
				IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
					IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
						mission_substage++
					ENDIF
				ENDIF
			ENDIF
		break
		case 6
			//if not sCamDetails.bRun
			IF HANDLE_SWITCH_CAM_MICHAEL_TO_TREVOR(scsSwitchCam_MichaelToTrevor)
				if player_ped_id() = TREV_PED()
					TRIGGER_MUSIC_EVENT("DH2A_MINISUB")
					FREEZE_ENTITY_POSITION(vehs[mvf_sub].id,false)
					STOP_PED_SPEAKING( PLAYER_PED_ID(), TRUE )
				//mike
					CLEAR_PED_TASKS(MIKE_PED())
					SET_ENTITY_COORDS(MIKE_PED(),<<-231.6731, -2309.1953, -24.9887>>)
					set_entity_heading(MIKE_PED(), 95.6420)
					FREEZE_ENTITY_POSITION(MIKE_PED(),true)
					SET_ENTITY_LOAD_COLLISION_FLAG(MIKE_PED(),true)
				//crate
					SET_ENTITY_COORDS(objLoot,<<-231.21761, -2301.24365, -26.59173>>)
					SET_OBJECT_PHYSICS_PARAMS(objLoot,40,1,<<-1,-1,-1>>, <<-1,-1,-1>>,-1,-1,0)
					FREEZE_ENTITY_POSITION(objLoot,TRUE)									
				//misc
					if not IS_AUDIO_SCENE_ACTIVE("DH2A_CONTROL_SUB")
						START_AUDIO_SCENE("DH2A_CONTROL_SUB")
					endif
//					print_help("DCKH_SUBINS")
					CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_INC",CONV_PRIORITY_MEDIUM)
					bHotSwap = false
					Prep_stop_Cutscene(true,false,true)
					iCSDelay = GET_GAME_TIMER() 
					Unload_Asset_Anim_Dict(sAssetData,"missheistdocks2a@swimtocontainer")
					bPrintDisplayed = FALSE
					mission_substage++
//				else
//					PRINT_NOW("DCKH_SWP_TRVR",DEFAULT_GOD_TEXT_TIME,1)
				endif
			else
				if IS_AUDIO_SCENE_ACTIVE("DH2A_SWIM_TO_CONTAINER")
					STOP_AUDIO_SCENE("DH2A_SWIM_TO_CONTAINER")
				endif
			endif
		break
		case 7
//			if IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,GET_ENTITY_COORDS(objLoot),<<0.1,0.1,0.1>>,false,vehs[mvf_sub].id,"DCKH_PICK","","DCKH_GETBK")
			IF NOT bPrintDisplayed
				IF IS_SAFE_TO_DISPLAY_GODTEXT()
					bPrintDisplayed = TRUE
					PRINT( "DCKH_PICK", DEFAULT_GOD_TEXT_TIME, 1 )
					PRINT_HELP("DCKH_SUB_HELP")
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
				ENDIF
			ENDIF
			IF NOT bDialoguePlayed
				IF IS_SAFE_TO_START_CONVERSATION()
					IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_DRIVE",CONV_PRIORITY_MEDIUM)
						CPRINTLN( DEBUG_MISSION, "CONVERSATION: Trevor and Michael talk as Trevor drives to weapon" )
						bDialoguePlayed = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT bHelpDisplayed
				IF VDIST2( GET_ENTITY_COORDS(trev_ped()), GET_ENTITY_COORDS(objLoot) ) < 400
					PRINT_HELP( "DCKH_PCKUPH" )
					bHelpDisplayed = TRUE
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(mike_ped())
			AND DOES_ENTITY_EXIST( objLoot )
				IF VDIST2( GET_ENTITY_COORDS(trev_ped()), GET_ENTITY_COORDS(objLoot) ) < 1600
					FREEZE_ENTITY_POSITION( MIKE_PED(), FALSE )
					IF GET_SCRIPT_TASK_STATUS( mike_ped(), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY ) > PERFORMING_TASK
						TASK_TURN_PED_TO_FACE_ENTITY( mike_ped(), trev_ped() )
					ENDIF
					
					IF NOT bDialoguePlayed2
						IF NOT IS_ENTITY_OCCLUDED( objLoot )
							IF IS_SAFE_TO_START_CONVERSATION()
								IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_SEES",CONV_PRIORITY_MEDIUM)
									CPRINTLN( DEBUG_MISSION, "CONVERSATION: Trevor says he sees it" )
									bDialoguePlayed2 = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehs[mvf_sub].id)
					IF IS_ENTITY_TOUCHING_ENTITY(vehs[mvf_sub].id,mike_ped())
						IF IS_SAFE_TO_START_CONVERSATION()
							CPRINTLN( DEBUG_MISSION, "CONVERSATION: Michael says ouch" )
							CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_SMACK",CONV_PRIORITY_MEDIUM)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			IF bDialoguePlayed2
				IF NOT bDialoguePlayed3
					IF IS_SAFE_TO_START_CONVERSATION()
						IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_EASY",CONV_PRIORITY_MEDIUM)
							CPRINTLN( DEBUG_MISSION, "CONVERSATION: Michael tells Trevor to go easy" )
							iCSDelay = GET_GAME_TIMER() + 15000
							bDialoguePlayed3 = TRUE
						ENDIF
					ENDIF
				ELIF NOT bDialoguePlayed4
					IF GET_GAME_TIMER() > iCSDelay
						IF CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_CRT",CONV_PRIORITY_MEDIUM)
							CPRINTLN( DEBUG_MISSION, "CONVERSATION: Michael tells Trevor to hurry up" )
							bDialoguePlayed4 = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			if not does_blip_exist(blipcrate)
				blipcrate = CREATE_BLIP_FOR_OBJECT(objLoot)
			endif
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehs[mvf_sub].id)
				//Save Trevor eyes / head components
				peds[mpf_trev].ePedCompPropHead = GET_PED_COMP_ITEM_CURRENT_SP(trev_ped(),COMP_TYPE_PROPS,ENUM_TO_INT(ANCHOR_HEAD))
				peds[mpf_trev].ePedCompPropEyes = GET_PED_COMP_ITEM_CURRENT_SP(trev_ped(),COMP_TYPE_PROPS,ENUM_TO_INT(ANCHOR_EYES))
				peds[mpf_trev].ePedCompSpecial= GET_PED_COMP_ITEM_CURRENT_SP(trev_ped(),COMP_TYPE_SPECIAL)
				
				IF IS_ENTITY_TOUCHING_ENTITY(vehs[mvf_sub].id,objLoot)
					SET_PLAYER_CONTROL( PLAYER_ID(), FALSE, SPC_LEAVE_CAMERA_CONTROL_ON )
					BRING_VEHICLE_TO_HALT( vehs[mvf_sub].id, 2.0, 2 )
					//objective
					if DOES_BLIP_EXIST(blip_objective)
						REMOVE_BLIP(blip_objective)
					endif
					//frank
					FREEZE_ENTITY_POSITION(MIKE_PED(),false)
					SET_ENTITY_INVINCIBLE(MIKE_PED(),TRUE)
					if does_particle_fx_looped_exist(flare_ptfx)
						STOP_PARTICLE_FX_LOOPED(flare_ptfx)
					endif 
					CLEAR_PED_TASKS(MIKE_PED())
					bPrintDisplayed = FALSE
					bPrintDisplayed2 = FALSE
					bDialoguePlayed = FALSE
					bDialoguePlayed2 = FALSE
					bDialoguePlayed3 = FALSE
					mission_substage++
				ELIF HAS_ENTITY_COLLIDED_WITH_ANYTHING( vehs[mvf_sub].id )
					IF GET_ENTITY_SPEED(vehs[mvf_sub].id) > 2.0
						IF NOT IS_ENTITY_TOUCHING_ENTITY(vehs[mvf_sub].id,mike_ped())
							IF IS_SAFE_TO_START_CONVERSATION()
								CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_BANG",CONV_PRIORITY_MEDIUM)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		break
		case 8//<<< being used for minimap (mission checks)
			if IS_ENTITY_ATTACHED( objLoot )
				SET_PLAYER_CONTROL( PLAYER_ID(), TRUE )
				REPLAY_RECORD_BACK_FOR_TIME(3, 0.0, REPLAY_IMPORTANCE_HIGH)
				if does_blip_exist(blipcrate)
					remove_blip(blipcrate)
				endif
				//misc
				bDialoguePlayed = false
				Load_Asset_Model( sAssetData, P_CS_SUB_HOOK_01_S )
				Load_Asset_Model( sAssetData, PROP_ROPE_HOOK_01 )
				mission_substage++
			else
				if not bAttachSoundPlayed
					IF REQUEST_SCRIPT_AUDIO_BANK( "PORT_OF_LS_ATTACH_CARGO" )
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
						CLEAR_PED_TASKS( trev_ped() )
						bAttachSoundPlayed = TRUE
						FREEZE_ENTITY_POSITION(objLoot,FALSE)
						SET_OBJECT_PHYSICS_PARAMS(objLoot, fMass, fGravity, << -1.0, -1.0, -1.0 >>, << -1.0, -1.0, -1.0 >>, DEFAULT, DEFAULT, fBuoyancy)
						//ATTACH_ENTITY_TO_ENTITY( objLoot, vehs[mvf_sub].id, 1, <<0,-2.0,-1.7>>, <<0,0,90>>, FALSE, FALSE, FALSE )
						//SET_ENTITY_NO_COLLISION_ENTITY( objLoot, vehs[mvf_sub].id, FALSE )
						//SET_ENTITY_NO_COLLISION_ENTITY( vehs[mvf_sub].id, objLoot, FALSE )
						ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(objLoot, vehs[mvf_sub].id, -1, 1, vSecondEntityOffset, vFirstEntityOffset, vRotation, -1, TRUE)
						play_sound_from_entity(i_sound_attach_cargo, "Attach_Cargo",vehs[mvf_sub].id,"docks_heist_finale_2a_sounds")
						OPEN_SEQUENCE_TASK(seq)
							TASK_GO_STRAIGHT_TO_COORD(NULL, <<-261.1416, -2328.5322, -17.0984>>, PEDMOVE_RUN, -1)
							TASK_GO_STRAIGHT_TO_COORD(NULL, <<-278.5093, -2378.8352, -2.3485>>, PEDMOVE_RUN, -1)
							TASK_WANDER_STANDARD(NULL)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(mike_ped(), seq)
						CLEAR_SEQUENCE_TASK(seq)
					ENDIF
				ENDIF
			endif
		break
		case 9			
			if not bDialoguePlayed3
				if IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-348.900330,-2573.658936,2.997342>>, <<-468.707886,-2471.864014,-23.989750>>, 100.000000)
					IF IS_SAFE_TO_START_CONVERSATION()
						if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_FLOYD",CONV_PRIORITY_MEDIUM)
							bDialoguePlayed3 = true
						endif
					endif
				endif
			endif
			
			IF isentityalive( vehs[mvf_sub].id )
				IF GET_ENTITY_SPEED(vehs[mvf_sub].id) > 2.0
					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING( vehs[mvf_sub].id )
						IF IS_SAFE_TO_START_CONVERSATION()
							CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_BANG2",CONV_PRIORITY_MEDIUM)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			if not bDialoguePlayed
				IF IS_SAFE_TO_START_CONVERSATION()
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED( sLocatesData )
					ADD_PED_FOR_DIALOGUE(convo_struct,4,null,"FLOYD")
					if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_DONE",CONV_PRIORITY_MEDIUM)
						bDialoguePlayed = true
					endif
				endif
			elif not bDialoguePlayed2
				IF IS_SAFE_TO_START_CONVERSATION()
					if CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_RADIO",CONV_PRIORITY_MEDIUM)
						bDialoguePlayed2 = true
					endif
				ENDIF
			endif
			
			if DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				SET_BLIP_ROUTE(sLocatesData.LocationBlip,false)
				IF GET_BLIP_COLOUR( sLocatesData.LocationBlip ) != BLIP_COLOUR_YELLOW
					SET_BLIP_COLOUR( sLocatesData.LocationBlip, BLIP_COLOUR_YELLOW )
				ENDIF
			endif
			
			if not b_cutscene_loaded
				if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id()),<<-358.59189, -2560.92188, -5>>) < DEFAULT_CUTSCENE_LOAD_DIST
					REQUEST_CUTSCENE("LSDH_2A_EXT")
					REQUEST_SCRIPT("docks2ASubHandler")
					//[MF] Removing for Fix of B* 1642786
//					IF isentityalive(peds[mpf_mike].id)
//						DELETE_PED(peds[mpf_mike].id)
//						CPRINTLN(DEBUG_MISSION, "DELETING MIKE PED")
//					ENDIF
					b_cutscene_loaded = true
				endif
			else
				if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(player_ped_id()),<<-358.59189, -2560.92188, -5>>) > DEFAULT_CUTSCENE_UNLOAD_DIST
					REMOVE_CUTSCENE()
					b_cutscene_loaded = false
				endif
			endif
			
			if can_request_assets_for_cutscene_entity()
				IF isentityalive(mike_ped())
					SET_PED_COMP_ITEM_CURRENT_SP(mike_ped(), COMP_TYPE_PROPS, PROPS_EYES_NONE)
				ENDIF
				//INT iComponent
				//REPEAT NUM_PED_COMPONENTS iComponent
				//	SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", INT_TO_ENUM(PED_COMPONENT, iComponent), g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iDrawableVariation[iComponent], g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iTextureVariation[iComponent], GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				//ENDREPEAT
				SET_CUTSCENE_PED_OUTFIT("MICHAEL", PLAYER_ZERO, OUTFIT_P0_SCUBA_LAND)
				PRINTLN("Setting Michael's cutscene outfit to OUTFIT_P0_SCUBA_LAND attempt 1.")
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED( "TREVOR", PLAYER_PED_ID() )
			endif
			
			if IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,<<-358.59189, -2560.92188, -5>>,<<0.1,0.1,0.1>>,false,vehs[mvf_sub].id,"DCKH_RETCARGO","","DCKH_GETBK")				
			or (DOES_BLIP_EXIST(sLocatesData.LocationBlip)And GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(objLoot),<<-358.59189, -2560.92188, -12.90297>>) < 20 )
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				CREATE_CONVERSATION(convo_struct,"D2AAUD","DH2A_THERE",CONV_PRIORITY_MEDIUM)
				iCSDelay = get_game_timer()
				RELEASE_NAMED_SCRIPT_AUDIO_BANK("PORT_OF_LS_ATTACH_CARGO")
				ALLOW_DIALOGUE_IN_WATER( FALSE )
				REPLAY_RECORD_BACK_FOR_TIME(8.0, 2.0, REPLAY_IMPORTANCE_HIGHEST)
				mission_substage++
			else
				DRAW_MARKER(MARKER_RING,<<-358.59189, -2560.92188, -6>>,<<0,0,0>>,<<0,0,0>>,<<5,5,5>>,255,255,0,65)
			endif
		break			
		case 10
			if can_request_assets_for_cutscene_entity()
				IF isentityalive(mike_ped())
					SET_PED_COMP_ITEM_CURRENT_SP(mike_ped(), COMP_TYPE_PROPS, PROPS_EYES_NONE)
				ENDIF
				//INT iComponent
				//REPEAT NUM_PED_COMPONENTS iComponent
				//	SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", INT_TO_ENUM(PED_COMPONENT, iComponent), g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iDrawableVariation[iComponent], g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iTextureVariation[iComponent], GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				//ENDREPEAT
				SET_CUTSCENE_PED_OUTFIT("MICHAEL", PLAYER_ZERO, OUTFIT_P0_SCUBA_LAND)
				PRINTLN("Setting Michael's cutscene outfit to OUTFIT_P0_SCUBA_LAND attempt 2.")
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED( "TREVOR", PLAYER_PED_ID() )
//				SET_STORED_PLAYER_PED_CUTSCENE_VARIATIONS( CHAR_MICHAEL, "MICHAEL" )
			endif		
			if IS_VEHICLE_ALMOST_STOPPED(vehs[mvf_sub].id)
//			or get_game_Timer() - iCSDelay > 2000
			and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				STOP_PED_SPEAKING( PLAYER_PED_ID(), FALSE )
				mission_set_Stage(msf_10_end_cutscene)
				mission_substage = STAGE_ENTRY
			else
				BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehs[mvf_sub].id, DEFAULT_VEH_STOPPING_DISTANCE, 10)
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					REMOVE_BLIP(sLocatesData.LocationBlip)
				ENDIF
			endif
		break
	endswitch
		
		if player_ped_id() = MIKE_PED()
			if get_distance_between_coords(GET_ENTITY_COORDS(objLoot),get_entity_coords(MIKE_PED())) > 240
				if bPrintDisplayed2 = false
					print_now("DCKH_LEAVEA",DEFAULT_GOD_TEXT_TIME,1)
					bPrintDisplayed2 =true
				endif
				if get_distance_between_coords(GET_ENTITY_COORDS(objLoot),get_entity_coords(MIKE_PED())) > 250
					MISSION_FAILED(mff_left_docks)
				endif
			else
				bPrintDisplayed2 = false
			endif			
		endif
		
		//freez sub so easier to get into.
		if IsEntityAlive(vehs[mvf_sub].id)
		and IsEntityAlive(TREV_PED())
		and mission_substage > 6
		and mission_substage < 11
			if player_ped_id() = TREV_PED()
				IF IS_VEHICLE_STUCK_TIMER_UP(vehs[mvf_sub].id, VEH_STUCK_ON_ROOF, ROOF_TIME)
				OR IS_VEHICLE_STUCK_TIMER_UP(vehs[mvf_sub].id, VEH_STUCK_JAMMED, 15000)
				OR IS_VEHICLE_STUCK_TIMER_UP(vehs[mvf_sub].id, VEH_STUCK_HUNG_UP, 15000)
				OR IS_VEHICLE_STUCK_TIMER_UP(vehs[mvf_sub].id, VEH_STUCK_ON_SIDE, 15000)
					Mission_Failed(mff_sub_stuck)
				endif
			endif
		endif
		
		
		
ENDPROC

PROC ST_10_END_CUTSCENE()
	
	SWITCH mission_substage
		case STAGE_ENTRY
			if can_request_assets_for_cutscene_entity()
				//INT iComponent
				//REPEAT NUM_PED_COMPONENTS iComponent
				//	SET_CUTSCENE_PED_COMPONENT_VARIATION("MICHAEL", INT_TO_ENUM(PED_COMPONENT, iComponent), g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iDrawableVariation[iComponent], g_savedGlobals.sPlayerData.sInfo.sVariations[CHAR_MICHAEL].iTextureVariation[iComponent], GET_PLAYER_PED_MODEL(CHAR_MICHAEL))
				//ENDREPEAT
				SET_CUTSCENE_PED_OUTFIT("MICHAEL", PLAYER_ZERO, OUTFIT_P0_SCUBA_LAND)
				PRINTLN("Setting Michael's cutscene outfit to OUTFIT_P0_SCUBA_LAND attempt 3.")
				//Store Trevor's ped comps
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED( "TREVOR", PLAYER_PED_ID() )
			endif
			if HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
			and HAS_SCRIPT_LOADED("docks2ASubHandler")
			and Create_End_Cutscene_Vehicle( vehs[mvf_mission_veh].id, <<-329.75, -2577.94, 5.64>>, 128.25 )
				REMOVE_PTFX_ASSET()
				IF DOES_ENTITY_EXIST( objLoot )
					DETACH_ENTITY( objLoot )
					REGISTER_ENTITY_FOR_CUTSCENE(objLoot,"Miltary_Pickup",CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				//sub
				IF IS_ENTITY_OK( vehs[mvf_sub].id ) 
					REGISTER_ENTITY_FOR_CUTSCENE(vehs[mvf_sub].id,"submersible",CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF
				
				REGISTER_ENTITY_FOR_CUTSCENE( NULL, "Sub_Rope_complete", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PROP_CS_SUB_ROPE_01 )
				REGISTER_ENTITY_FOR_CUTSCENE( NULL, "DOM_Flatbed_truck", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, FLATBED )
				
				IF IS_ENTITY_OK(peds[mpf_mike].id)
					RESTORE_PLAYER_PED_WEAPONS_IN_SNAPSHOT(peds[mpf_mike].id)	//restore Michael's weapons from before the mission
					REGISTER_ENTITY_FOR_CUTSCENE( peds[mpf_mike].id, "MICHAEL", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_MICHAEL) )
				ELSE
					REGISTER_ENTITY_FOR_CUTSCENE( peds[mpf_mike].id, "MICHAEL", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, GET_PLAYER_PED_MODEL(CHAR_MICHAEL) )
				ENDIF
				
				IF IS_ENTITY_OK(PLAYER_PED_ID())
					REGISTER_ENTITY_FOR_CUTSCENE( PLAYER_PED_ID(), "TREVOR", CU_ANIMATE_EXISTING_SCRIPT_ENTITY )
				ENDIF
				HANG_UP_AND_PUT_AWAY_PHONE()
				START_CUTSCENE()
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				mission_substage++
			endif
		BREAK
		case 1
			if IS_CUTSCENE_PLAYING()
				CLEAR_TIMECYCLE_MODIFIER()
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(800)
				ENDIF
				STOP_AUDIO_SCENES()
				TRIGGER_MUSIC_EVENT("DH2A_MISSION_COMPLETE")
				CLEAR_AREA(<<-332.24289, -2571.74365, 5.00101>>,50,true)
				bScubaOutfitSet = FALSE
				mission_substage++
			endif
		break
		case 2
			IF isentityalive( trev_ped() )
				IF (GET_PED_PROP_CURRENT_FROM_LOOKUP( trev_ped(), ANCHOR_EYES ) = PROPS_P2_SCUBA_MASK )
					CPRINTLN( DEBUG_MISSION, "SWAPPING MASK FOR PREVIOUS HEADGEAR FOR TREVOR" )
					SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED(), COMP_TYPE_PROPS, peds[mpf_trev].ePedCompPropHead,FALSE)
					SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED(), COMP_TYPE_PROPS, peds[mpf_trev].ePedCompPropEyes,FALSE)
					SET_PED_COMP_ITEM_CURRENT_SP(TREV_PED(), COMP_TYPE_SPECIAL, peds[mpf_trev].ePedCompSpecial)
				ENDIF
			ENDIF
			IF NOT DOES_ENTITY_EXIST(objSubHook)
				objSubHook = (GET_ENTITY_INDEX_OF_REGISTERED_ENTITY( "Sub_Rope_complete", PROP_CS_SUB_ROPE_01 ))
			ENDIF
			
			IF ( bScubaOutfitSet = FALSE )
				IF NOT DOES_ENTITY_EXIST(peds[mpf_mike].id)
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY( "MICHAEL" ))
						peds[mpf_mike].id = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY( "MICHAEL" ))
						
						SET_CUTSCENE_PED_OUTFIT("MICHAEL", PLAYER_ZERO, OUTFIT_P0_SCUBA_LAND)
						PRINTLN("Setting Michael's cutscene outfit to OUTFIT_P0_SCUBA_LAND attempt 4.")
					ENDIF
				ELSE
					IF isentityalive( peds[mpf_mike].id )

						IF NOT IS_PED_COMP_ITEM_CURRENT_SP(peds[mpf_mike].id, COMP_TYPE_OUTFIT, OUTFIT_P0_SCUBA_LAND)
							//PRINTLN("Setting Michael's outfit to scuba gear without flippers.")
							SET_PED_COMP_ITEM_CURRENT_SP(peds[mpf_mike].id, COMP_TYPE_OUTFIT, OUTFIT_P0_SCUBA_LAND, FALSE)
							PRINTLN("Setting Michael's cutscene outfit to OUTFIT_P0_SCUBA_LAND attempt 5.")
						ENDIF
					
						IF (GET_PED_PROP_INDEX( peds[mpf_mike].id, ANCHOR_EYES ) > -1)
							CPRINTLN( DEBUG_MISSION, "REMOVING MASK FROM MIKE" )
							SET_PED_COMP_ITEM_CURRENT_SP(peds[mpf_mike].id, COMP_TYPE_PROPS, PROPS_EYES_NONE)
						ENDIF
						
						bScubaOutfitSet = TRUE
						PRINTLN("Setting bScubaOutfitSet to TRUE.")
					ENDIF
				ENDIF
			ENDIF
			
			IF isentityalive( vehs[mvf_sub].id )
				SET_VEHICLE_FULLBEAM( vehs[mvf_sub].id, FALSE )
				SET_VEHICLE_LIGHTS( vehs[mvf_sub].id, FORCE_VEHICLE_LIGHTS_OFF )
			ENDIF
			
			IF NOT DOES_ENTITY_EXIST( vehs[mvf_getAwayVan].id )
				vehs[mvf_getAwayVan].id = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY( "DOM_Flatbed_truck", FLATBED ))
			ELSE
				IF GET_CUTSCENE_TIME() > 112000
					IF IS_VEHICLE_DRIVEABLE( vehs[mvf_getAwayVan].id )
						SET_VEHICLE_LIGHTS( vehs[mvf_getAwayVan].id, FORCE_VEHICLE_LIGHTS_ON )
					ENDIF
				ENDIF
			ENDIF
			
			if CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor",PLAYER_TWO)
				PRINTLN(GET_THIS_SCRIPT_NAME(), "CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned TRUE for Trevor.")
				bcs_trev = true
			endif
			if CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				PRINTLN(GET_THIS_SCRIPT_NAME(), "CAN_SET_EXIT_STATE_FOR_CAMERA() returned TRUE for camera.")
				bcs_cam = true
			endif
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY( "Sub_Rope_complete" )
				IF DOES_ENTITY_EXIST( objSubHook )
					bcs_hook = TRUE
					CPRINTLN( DEBUG_MISSION, "EXIT STATE for SubHook" )
					FREEZE_ENTITY_POSITION( objSubHook, TRUE )
				ENDIF
				PRINTLN(GET_THIS_SCRIPT_NAME(), "CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned TRUE for Sub_Rope_complete.")
			ENDIF
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY( "submersible" )
				IF DOES_ENTITY_EXIST( vehs[mvf_sub].id )
					bcs_sub = TRUE
					CPRINTLN( DEBUG_MISSION, "EXIT STATE for submersible" )
					SET_ENTITY_AS_MISSION_ENTITY( vehs[mvf_sub].id )
					FREEZE_ENTITY_POSITION( vehs[mvf_sub].id, TRUE )
				ENDIF
				PRINTLN(GET_THIS_SCRIPT_NAME(), "CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY() returned TRUE for submersible.")
			ENDIF
			if bcs_sub and bcs_hook
			and not bSubHandler
				StructDocksSubHandover sSubHandover
				sSubHandover.subHandover = vehs[mvf_sub].id
				sSubHandover.hookHandover = objSubHook
				START_NEW_SCRIPT_WITH_ARGS("docks2ASubHandler", sSubHandover, SIZE_OF(sSubHandover), DEFAULT_STACK_SIZE)
				SET_SCRIPT_AS_NO_LONGER_NEEDED("docks2ASubHandler")
				CPRINTLN(DEBUG_MISSION, "Sub handed over to handler script.")
				bSubHandler = TRUE
			endif
			if bcs_trev and bcs_cam
			AND NOT IS_CUTSCENE_PLAYING()
				IF DOES_ENTITY_EXIST( vehs[mvf_getAwayVan].id )
					DELETE_VEHICLE( vehs[mvf_getAwayVan].id )
				ENDIF
				IF DOES_ENTITY_EXIST(peds[mpf_mike].id)
					DELETE_PED(peds[mpf_mike].id)
				ENDIF
				IF DOES_ENTITY_EXIST( vehs[mvf_mission_veh].id )
					DELETE_VEHICLE( vehs[mvf_mission_veh].id )
				ENDIF
				REPLAY_STOP_EVENT()
				Mission_Passed()
			endif

			PRINTLN(GET_THIS_SCRIPT_NAME(), "bcs_trev is ", GET_STRING_FROM_BOOL(bcs_trev))
			PRINTLN(GET_THIS_SCRIPT_NAME(), "bcs_cam is ", GET_STRING_FROM_BOOL(bcs_cam))
			PRINTLN(GET_THIS_SCRIPT_NAME(), "IS_CUTSCENE_PLAYING() is ", GET_STRING_FROM_BOOL(IS_CUTSCENE_PLAYING()))
			
		break
	ENDSWITCH
	
ENDPROC

Proc ST_PASSED()
	switch mission_substage
		case STAGE_ENTRY
			if HAS_CUTSCENE_LOADED()	
				START_CUTSCENE()
				mission_substage++
			endif
		break
		case 1
			
			mission_passed()
			
		break
	endswitch
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		MISSION FLOW
// -----------------------------------------------------------------------------------------------------------
PROC mission_flow()
	IF bDoSkip
		EXIT
	ENDIF
	
	Switch	int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage)
		
		case msf_0_drive_to_bridge 			ST_0_DRIVE_TO_BRIDGE() 			break
		case msf_1_snipe_gaurds				ST_1_SNIPE_GUARDS() 			break
		case msf_2_first_bomb				ST_2_FIRST_BOMB()				break
		case msf_3_second_bomb				ST_3_SECOND_BOMB()				break
		case msf_4_third_bomb				ST_4_THIRD_BOMB()				break
		case msf_5_fourth_bomb 				ST_5_FOURTH_BOMB() 				break
		case msf_6_way_out					ST_6_WAY_OUT()					break
		case msf_7_detonate					ST_7_DETONATE()					break
		case msf_8_explosion_ctscne			ST_8_EXPLOSION_CUTSCENE()		break
		case msf_9_the_goods				ST_9_THE_GOODS()				break
		case msf_10_end_cutscene			ST_10_END_CUTSCENE()			break
//		case msf_passed						ST_PASSED()						break
	endswitch
ENDPROC
#if is_debug_build

	proc do_debug()
		if is_keyboard_key_just_pressed(key_s)
			mission_passed()
		elif is_keyboard_key_just_pressed(key_f)
			mission_failed(mff_debug_fail)		
		endif
	endproc
#endif
// ===========================================================================================================
//		script loop
// ===========================================================================================================

script		
	printstring("...docks 2a mission launched")
	printnl()	

	if (has_force_cleanup_occurred())
		if not sCamDetails.bRun
			trigger_music_event("dh2a_dead")
			printstring("...docks 2a mission force cleanup")
			printnl()
			IF PLAYER_PED_ID() = mike_ped()
				g_replay.iReplayInt[WHICH_PLAYER] = PLAYER_IS_MIKE
			ELIF PLAYER_PED_ID() = frank_ped()
				g_replay.iReplayInt[WHICH_PLAYER] = PLAYER_IS_FRANK
			ENDIF
			mission_flow_mission_force_cleanup()
			mission_cleanup(true)
			terminate_this_thread()
		endif
	endif
	
	IF NOT GET_MISSION_FLAG()
		CPRINTLN(DEBUG_MISSION, "SETTING MISSION FLAG: TRUE")
		set_mission_flag(true)
	ENDIF

#if is_debug_build
	//z menu for skipping stages+
	zmenunames[msf_0_drive_to_bridge].stxtlabel 		=	"stage 0: drive to bridge"	
	zmenunames[msf_1_snipe_gaurds].stxtlabel 			=	"stage 1: snipe guards"
	zmenunames[msf_2_first_bomb].stxtlabel 				=	"stage 2: first bomb"
	zmenunames[msf_3_second_bomb].stxtlabel 			=	"stage 3: second bomb"
	zmenunames[msf_4_third_bomb].stxtlabel 				=	"stage 4: third bomb"
	zmenunames[msf_5_fourth_bomb].stxtlabel 			=	"stage 5: fourth bomb"	
	zmenunames[msf_6_way_out].stxtlabel 				=	"stage 6: way out"
	zmenunames[msf_7_detonate].stxtlabel 				=	"stage 7: detonate"
	zmenunames[msf_8_explosion_ctscne].stxtlabel 		=	"stage 8: explosion cutscene"
	zmenunames[msf_9_the_goods].stxtlabel 				=	"stage 9: the goods"	
	zmenunames[msf_10_end_cutscene].stxtlabel 			=	"stage 10: end cutscene"
	
	zmenunames[cst_init].stxtlabel 	 	=	"initial cutscene"
	zmenunames[cst_init].bselectable 	= false
	zmenunames[cst_mcs_1].stxtlabel    	= "stage 0: mcs_1"
	zmenunames[cst_mcs_1].bselectable  	= false
	zmenunames[cst_mcs_2].stxtlabel   	= "stage 5: mcs_2"
	zmenunames[cst_mcs_2].bselectable 	= false
	zmenunames[cst_mcs_3].stxtlabel   	= "stage 9: mcs_3"
	zmenunames[cst_mcs_3].bselectable 	= false
	zmenunames[cst_ext].stxtlabel   	= "exit cutscene"
	zmenunames[cst_ext].bselectable 	= false
		
	widget_debug = start_widget_group("docks heist 2a menu")
		start_widget_group("mission stage logic")
			add_widget_int_read_only("mission stage", mission_stage)
			add_widget_int_read_only("mission subtage", mission_substage)		
		stop_widget_group()
		start_widget_group("alarm logic - read only ")
			add_widget_bool("alarm init:", balarminitialised)
			add_widget_bool("alarm ", balarmtriggered)
			add_widget_bool("alarm cs", bcsalarmtriggered)
		stop_widget_group()
		start_widget_group("ai logic")
			add_widget_int_read_only("imike_ai_stage:", imike_ai_stage)
			ADD_WIDGET_INT_READ_ONLY("snipe sound stage:", iaudioswitch_snipeStage)
			ADD_WIDGET_BOOL("Frank Fired:",bFrankFired)
		stop_widget_group()	
		start_widget_group("loot attachment")
			add_widget_float_slider("fMass", fMass, 0.0, 3000.0, 1.0)
			add_widget_float_slider("fGravity", fGravity, -1.0, 3000.0, 1.0)
			add_widget_float_slider("fBuoyancy", fBuoyancy, -1.0, 3000.0, 1.0)
			add_widget_vector_slider("vSecondEntityOffset", vSecondEntityOffset, -10.0, 10.0, 1.0)
			add_widget_vector_slider("vFirstEntityOffset", vFirstEntityOffset, -10.0, 10.0, 1.0)
			add_widget_vector_slider("vRotation", vRotation, -360.0, 360.0, 1.0)
		stop_widget_group()
//		CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()	
		start_widget_group("push in")
			add_widget_bool("bPushInAttached", bPushInAttached)
			add_widget_bool("bPushInAttachStartCam", bPushInAttachStartCam)
			add_widget_float_slider("fPushInStartPhase", fPushInStartPhase, 0.0, 1.0, 0.1)
			add_widget_float_slider("fPushInDistance", fPushInStartDistance, 0.0, 10.0, 1.0)
			add_widget_int_slider("iPushInInterpTime", iPushInInterpTime, 0, 10000, 100)
			add_widget_int_slider("iPushInCutTime", iPushInCutTime, 0, 10000, 100)
			add_widget_int_slider("iPushInFXTime", iPushInFXTime, 0, 10000, 100)
			add_widget_int_slider("iPushInSpeedUpTime", iPushInSpeedUpTime, 0, 10000, 100)
			add_widget_float_slider("fPushInSpeedUpProportion", fPushInSpeedUpProportion, 0.0, 10.0, 1.0)
			add_widget_bool("bPushInDoColourFlash", bPushInDoColourFlash)
		stop_widget_group()
		add_widget_float_slider("fFirstPersonFlashPhase1", fFirstPersonFlashPhase1, 0.0, 1.0, 0.01)

	stop_widget_group()
	set_locates_header_widget_group(widget_debug)
	
	SETUP_SPLINE_CAM_NODE_ARRAY_MICHAEL_TO_TREVOR(scsSwitchCam_MichaelToTrevor, mike_ped(), vehs[mvf_sub].id)
	CREATE_SPLINE_CAM_WIDGETS(scsSwitchCam_MichaelToTrevor, "Michael", "Submarine", widget_debug)
#endif

//	if not IS_REPLAY_IN_PROGRESS()
//		do_tods_dh2a()	
//	endif
	//initialize mission
	mission_setup()
	CREATE_TRIGGER_BOXES() //initialize the trigger boxes
	
	if is_replay_in_progress()
		remove_cutscene()	
		//if heli stages send to start of stage inside stage will sort out parameters 
		imidreplay = get_replay_mid_mission_stage()
		
		if g_bshitskipaccepted 
			imidreplay++
		endif
		
		VECTOR vReplayPos
		FLOAT fReplayHeading
		BOOL bFranklin = FALSE
		BOOL bMichael = FALSE
		
		SWITCH imidreplay
			CASE 0
				vReplayPos = <<-1151.6340, -1519.4980, 9.6327>> //<<-1149.8112, -1522.1071, 9.6330>>
				fReplayHeading = 215.0
			BREAK
			CASE 1
				vReplayPos = << -93.3159, -2368.9175, 13.2960 >>
				fReplayHeading = 337.5851
				bFranklin = TRUE
			BREAK
			CASE 2
				vReplayPos = << -83.5905, -2361.7979, 13.2963 >>
				fReplayHeading = 119.7045
				bMichael = TRUE
			BREAK
			CASE 3
				vReplayPos = << -94.9569, -2371.8770, 13.2969 >>
				fReplayHeading = 97.3018
				bMichael = TRUE
			BREAK
			CASE 4
				vReplayPos = << -129.6871, -2366.8972, 8.3193 >>
				fReplayHeading = 183.9491
				bMichael = TRUE
			BREAK
			CASE 5
				vReplayPos = << -191.4221, -2365.4763, 8.3193 >>
				fReplayHeading = 176.1654
				bMichael = TRUE
			BREAK
			CASE 6
				vReplayPos = <<-221.8633, -2376.6187, 12.3329>>
				fReplayHeading = 35.7369
				bFranklin = TRUE
			BREAK
			CASE 7
				vReplayPos = << -143.8203, -2488.2612, 43.4412 >>
				fReplayHeading = 35.7369
				bFranklin = TRUE
			BREAK
			CASE 8
				vReplayPos = << -143.8203, -2488.2612, 43.4412 >>
				fReplayHeading = 10.7369
				bFranklin = TRUE
			BREAK
			CASE 9
			CASE 10
				vReplayPos = <<-90.4927, -2288.0029, -1.3081>>
				fReplayHeading = 130.4705
				bMichael = TRUE
			BREAK
			CASE 11
				vReplayPos = <<-332.2139, -2570.9912, 5.0010>>
				fReplayHeading = 97.2111
			BREAK
		ENDSWITCH
		
		IF bFranklin
			IF SET_CURRENT_PLAYER_PED(SELECTOR_PED_FRANKLIN,TRUE)
				CPRINTLN(DEBUG_MISSION, "START_REPLAY_SETUP as FRANKLIN at ", vReplayPos )
				START_REPLAY_SETUP( vReplayPos, fReplayHeading )
			ENDIF
		ELIF bMichael
			IF SET_CURRENT_PLAYER_PED(SELECTOR_PED_MICHAEL,TRUE)
				CPRINTLN(DEBUG_MISSION, "START_REPLAY_SETUP as MICHAEL at ", vReplayPos )
				START_REPLAY_SETUP( vReplayPos, fReplayHeading )
			ENDIF
		ELSE
			IF SET_CURRENT_PLAYER_PED(SELECTOR_PED_TREVOR,TRUE)
				CPRINTLN(DEBUG_MISSION, "START_REPLAY_SETUP as TREVOR at ", vReplayPos )
				START_REPLAY_SETUP( vReplayPos, fReplayHeading )
			ENDIF
		ENDIF
		
		if imidreplay = 7								// if replay comes back stage 7  tell it to go to stage 6 										
			iskiptostage = enum_to_int(msf_6_way_out)	// the later stage will need to be told to skip to the actual stages
		elif imidreplay = 8
			iskiptostage = enum_to_int(msf_7_detonate)	//st_7_detonate
		elif imidreplay = 9
			iskiptostage = enum_to_int(msf_8_explosion_ctscne) 	//st 8 explosion
		elif imidreplay = 10
			iskiptostage = enum_to_int(msf_9_the_goods)	//st 9_goods
		elif imidreplay = 11
			iskiptostage = enum_to_int(msf_10_end_cutscene)	// passed stage
		else
			iskiptostage = imidreplay// else set as normal
		endif
		
		bdoskip  = true		
		bis_zskip = false
		mission_set_stage(int_to_enum(msf_mission_stage_flags, iskiptostage))			
		
	else //start from begining
		set_replay_mid_mission_stage_with_name(0,"stage 0: drive to bridge")
		mission_stage = enum_to_int(msf_0_drive_to_bridge)
		mission_set_stage(int_to_enum(msf_mission_stage_flags, mission_stage))
		run_intro_cutscene()	
		bis_zskip = false
	endif		
	
	mission_substage = stage_entry	
	
	while (true)
	
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TheMerryweatherHeistFreight")
		
		//prestreaming loop
		Update_Asset_Management_System(sAssetData)		// Deals with loading any assets and keeps track of what has been loaded
		Update_Cutscene_Prestreaming(sCutscenePedVariationRegister)			
		
		mission_stage_management()
		mission_stage_skip()
		mission_checks()
		mission_flow()		
		
	#if is_debug_build
		do_debug()
		HANDLE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
		UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCam_MichaelToTrevor)
	#endif		
		wait(0)
	
	endwhile
endscript
