//+-----------------------------------------------------------------------------+
//¦																				¦
//¦				Author: Adam Westwood			Date: 09/11/2010		 	    ¦
//¦																				¦
//¦-----------------------------------------------------------------------------¦
//¦																				¦
//¦ 			            Docks Finale B - Steal Sub                      	¦
//¦ 																			¦
//¦ 	                                             							¦
//¦																				¦
//+-----------------------------------------------------------------------------+

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//---------------------------------¦ HEADERS ¦-----------------------------------

 
//SET_HELI_TURBULENCE_SCALAR

//TASK_BOAT_MISSION
//BCF_OPENOCEANSETTINGS

//DS2A_NOFLY
//DS2A_FEWFLY
//DS2A_FLYLOT
//
//DS2A_TRVRIDE
//DS2A_FKRRIDE
//DS2A_LETSGO
//DS2A_WANTED
//DS2A_LOSE
//DS2A_CHOP
//DS2A_LOWER
//DS2A_HIGHER
//DS2A_CRASHSB
//DS2A_FLYOFF
//DS2A_CRSHB4
//DS2A_LETGO
//DS2A_DROPST
//DS2A_FLYAWAY
//DS2A_PROMPT
//DS2A_GOGET
//DS2A_UPD1
//DS2A_UPD2
//DS2A_UPD3
//DS2A_NOWET
//DS2A_CONNECT
//DS2A_WHATUP
//DS2A_EXPL
//DS2A_PISSD
//DS2A_FPROMPT
//DS2A_FONIT
//DS2A_OPENUP
//DS2A_REARATT
//DS2A_BDOOR
//DS2A_SEEAIR
//DS2A_GENT

//MERRYWEATHER DIALOGUE

//And then could you pass to Adam to trigger DS2A_SHOW, DS2A_CHAT and DS2A_GEN. I've included speakers MERRYWEATHER1 for the random conversations, speaker ID 5 and MERRYWEATHER for for DS2A_GEN, speaker ID 4.

USING "rage_builtins.sch"
USING "globals.sch"
USING "script_blips.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_misc.sch"
USING "commands_player.sch"
USING "commands_camera.sch"
USING "commands_object.sch"
USING "commands_interiors.sch"
USING "commands_streaming.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_path.sch"
USING "commands_task.sch"
USING "commands_audio.sch"
USING "flow_public_core_override.sch"
USING "locates_private.sch"
USING "script_player.sch"
USING "script_MISC.sch"
USING "script_maths.sch"
USING "cellphone_public.sch"
USING "selector_public.sch"
USING "player_ped_public.sch"
USING "commands_entity.sch"
USING "commands_physics.sch"
USING "replay_public.sch"
USING "locates_public.sch"
USING "rappel_public.sch"
USING "area_checks.sch"
USING "building_control_public.sch"

USING "help_at_location.sch"
USING "script_heist.sch"
USING "CompletionPercentage_public.sch"
USING "taxi_functions.sch"
USING "shared_hud_displays.sch"
USING "chase_hint_cam.sch"                            CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct
USING "vehicle_gen_public.sch"
USING "clearmissionarea.sch"
USING "respawn_location_private.sch"
USING "heist_end_screen.sch"
USING "vehicle_gen_private.sch"
USING "achievement_public.sch"
USING "spline_cam_edit.sch"
USING "flow_public_core.sch"
USING "commands_recording.sch"

CONST_INT max_invPoints 60
CONST_INT max_sweep_peds 30

USING "AI_Sweep.sch"
USING "text_queue.sch"
//USING "hacking_public.sch"

/*
IF NOT sCamDetails.bPedSwitched
	SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(GET_SELECT_PED(SEL_MICHAEL))
ENDIF

*/

structPedsForConversation sSpeech

//CREW_OUTFIT_SCUBA_LAND

//HUD stuff
CONST_INT ENABLE_ENHANCED_HUD_FEATURES 1 //this is ‘0’ for multiplayer mode to cope with variable limitations
CONST_INT MAX_SCANNABLE_PEDS 10 //however many peds you’ll want to have as targets at any one time. (min 1)
CONST_INT MAX_HELIHUD_COORD_COUNT 1 //(however many location markers you want on screen at one time.
CONST_INT MAX_MESSAGES 1 //ignore for now. Meant to display ticker text on screen but currently bust.
//CONST_INT MAX_HELIHUD_COORD_COUNT 1
//CONST_INT MAX_HELIHUD_COORD_COUNT 5

//For the Text 
CONST_INT MAX_QUEUED_TEXT 10
CONST_INT MAX_QUEUED_HELP_AL 3
CONST_INT MAX_TEXT_HASHES 50
CONST_INT MAX_NUM_MISSILE 5
CONST_INT MAX_NUM_TARGETS 10
CONST_INT MAX_COVER_POINTS 10
//STRING sConversationBlock = "T1M3AUD"

//Debug things
#IF IS_DEBUG_BUILD
//Debug things
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
	INT ieMissionStageTemp
	BOOL bSkipping	
	CONST_INT MAX_SKIP_MENU_LENGTH 12	//number of stages in mission(length of menu)
	INT iReturnStage					//mission stage to jump to
	MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]//struct containing the debug menu
	BOOL b_debug_draw_debug
#ENDIF	

BOOL bHasChanged   //variable for storing the state of “player has changed outfit”

// ********************************* VARIABLES AND CONSTANTS *******************************

ENUM ROCKET_ANIM_STATE
	ROCKET_ANIM_USING_AI,
	ROCKET_ANIM_GET_LAUNCHER_OUT_THEN_AIM,
	ROCKET_ANIM_PUT_LAUNCHER_AWAY,
	ROCKET_ANIM_GET_CLOSER_THEN_AIM,
	ROCKET_ANIM_RELOAD_THEN_AIM,
	ROCKET_ANIM_AIMING
ENDENUM

ENUM ROCKET_LINE_UP_STATE
	LINE_UP_DONT_FIRE_TOO_FAR_AWAY,
	LINE_UP_DONT_FIRE_TOO_CLOSE,
	LINE_UP_DONT_FIRE_BAD_ANGLE,
	LINE_UP_FIRE_MISS_LEFT_SIDE,
	LINE_UP_FIRE_MISS_RIGHT_SIDE,
	LINE_UP_FIRE_LEFT_SIDE,
	LINE_UP_FIRE_RIGHT_SIDE
ENDENUM

STRING str_anim_rockets					= "missbigscore2big_5"
STRING str_anim_rocket_reload			= "missbigscore2big_6"
STRING str_anim_rocket_signal			= "missbigscore2big_7_p2"

OBJECT_INDEX obj_lesters_rpg
OBJECT_INDEX obj_single_rocket

OBJECT_INDEX obj_dummy_target

OBJECT_INDEX objSofa					//shitty sofa to hide during cutscene

ENTITY_INDEX vehGrabbedTemp

CONST_INT NUMBER_OF_PTFX 10

PTFX_ID merry_weather_ptfx[NUMBER_OF_PTFX]

PED_INDEX pedFloyd
PED_INDEX pedWade
PED_INDEX pedLester

INT iPauseBetweenDialogueTimer
BOOL bInRearShootingPosition = FALSE

INT i_lester_rocket_anim_event
INT i_lester_rocket_sync_scene
INT iFranklinShooting
INT i_rocket_line_up_dialogue_timer		= 0
INT i_player_rocket_event
INT i_rocket_timer

INT i_blast_timer_1
INT i_blast_timer_2
INT i_blast_timer_3

INT i_boat_timer
INT i_plane_timer

INT icolour1, icolour2

//INT iGodTextDelay

FLOAT IminRopeLength = 4.0
FLOAT imaxRopeLength = 6.0
FLOAT iStabliseLength = -6.0

TWEAK_FLOAT		f_cam_limits_aim_heading_min	-20.0
TWEAK_FLOAT 	f_cam_limits_aim_heading_max	50.0
TWEAK_FLOAT		f_cam_limits_aim_pitch_min		-60.0
TWEAK_FLOAT 	f_cam_limits_aim_pitch_max		0.0
TWEAK_FLOAT		f_cam_limits_aim_orbit_min		1.0
TWEAK_FLOAT 	f_cam_limits_aim_orbit_max		1.0

#IF IS_DEBUG_BUILD
FLOAT fGameplayCamHeading = 0.0
FLOAT fGameplayCamPitch = 0.0
#ENDIF

PTFX_ID ptfxDrips

INTERIOR_INSTANCE_INDEX 	interior_living_room	// interiors

ROCKET_ANIM_STATE e_rocket_anim_state 	= ROCKET_ANIM_USING_AI

ENUM SFX_FLAGS
	sfx_alarm,
	sfx_smoke_nade,
	sfx_num_sounds
ENDENUM

ENUM MISSION_VEHICLES
	MV_CHINOOK = 0,
	MV_SUBMERISIBLE,
	MV_MISSION_DINGHY,
	MV_TREVORS_TRUCK,
	MV_MICHAELS_CAR,
	MV_PACKER,
	MV_ARMY_TRAILER,
	MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1 = 10,
	MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2 = 11,
	MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1 = 12,
	MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2 = 13,
	MV_MERRY_WEATHER_CHASE_HELI_RHS_1 = 14,
	MV_MERRY_WEATHER_CHASE_HELI_REAR_1 = 15,
	MV_MERRY_WEATHER_CHASE_HELI_REAR_2 = 16,
	MV_MERRY_WEATHER_CHASE_HELI_REAR_3 = 17,
	MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1 = 18,
	MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2 = 19,
	MV_MERRY_WEATHER_DINGHY_1 = 20,
	MV_MERRY_WEATHER_DINGHY_2 = 21,
	MV_MERRY_WEATHER_DINGHY_3 = 22,
	MV_MERRY_WEATHER_HELI_1 = 23,
	MV_MERRY_WEATHER_HELI_2 = 24,
	MV_MERRY_WEATHER_HELI_3 = 25,
	MV_MERRY_WEATHER_CARGOPLANE =26,
	MV_MERRY_WEATHER_DINGHY_SETPIECE_1 =27,
	MV_MERRY_WEATHER_DINGHY_SETPIECE_2 =28,
	MV_MERRY_WEATHER_DINGHY_SETPIECE_3 =29,
	MV_MERRY_WEATHER_CARGOPLANE1 =30,
	MV_MERRY_WEATHER_CARGOPLANE2 =31,
	MV_FRANKLINS_CAR = 32
ENDENUM

ENUM MISSION_OBJECT
	MO_TARP,
	MO_RIG1,
	MO_RIG2,
	MO_CONTAINER
ENDENUM

ENUM MISSION_REQUIREMENT
	REQ_TREVORS_TRUCK,
	REQ_MICHAELS_CAR,
	REQ_FRANKLINS_CAR,
	REQ_FRANKLIN,
	REQ_TREVOR,
	REQ_MICHAEL,
	REQ_SUBMERSIBLE,
	REQ_SUB_TRUCK,
	REQ_SUB_ON_TRUCK,
	REQ_SUB_ON_TRUCK_ON_RUNWAY,
	REQ_SUB_TRUCK_ON_RUNWAY,
	REQ_SUB_ON_TRUCK_WITH_TARP,
	REQ_HELI_WITH_SUB,
	REQ_DINGHY,
	REQ_CONTAINER,
	REQ_SEA_PROPS,
	REQ_MISSION_HELI,
	REQ_MISSION_HELI_WITHOUT_ROPE,
	REQ_CARGO,
	REQ_SHARK,
	REQ_WADE,
	REQ_FLOYD,
	REQ_LESTER_FOR_CUTSCENE,
	REQ_LESTER_CAR_FOR_CUTSCENE,
	REQ_WADE_FOR_CUTSCENE,
	REQ_MERRYWEATHER_DINGHY_START,
	REQ_MERRYWEATHER_HELI_START,
	REQ_SHARK_BY_CRATE,
	REQ_MERRYWEATHER_DINGHY_SETPIECE,
	REQ_MERRYWEATHER_PLANE_SETPIECE,
	REQ_MERRYWEATHER_WORKING_ON_RIG,
	REQ_MERRYWEATHER_RIG,
	REQ_MERRYWEATHER_FX,
	REQ_WATERLIFE
ENDENUM


CONST_INT MAX_NUMBER_OF_DOCK_PROPS 25
CONST_INT MAX_NUMBER_OF_MISSION_PROPS 5
CONST_INT MAX_NUMBER_OF_SEABED_PROPS 25

CONST_FLOAT PENINSULA_Y 6280.0

//BLIP_INDEX
OBJECT_INDEX odoor[5]
OBJECT_INDEX obox
OBJECT_INDEX objcrate[MAX_NUMBER_OF_DOCK_PROPS]
OBJECT_INDEX objAmmo 
OBJECT_INDEX objMission[MAX_NUMBER_OF_MISSION_PROPS]

OBJECT_INDEX objSeabed[MAX_NUMBER_OF_SEABED_PROPS]

PED_INDEX pedUnderWater[MAX_NUMBER_OF_SEABED_PROPS]

//ENTITY_INDEX nullentity

//VECTOR vdockdoorcoords[5]
VECTOR vdockdoorrot[5]
VECTOR vFrankRappel = << 486.5248, -3201.5293, 39.7540 >>
VECTOR vDiveOut = << 491.3913, -3408.2791, 0.5384 >>
VECTOR vReturnFlyto

VECTOR vSubRaisedPosition = <<527.26, -3164.78, -0.60>>

//VECTOR vSubAttach = <<0,0,-5.0>>
//VECTOR vCrateAttach = <<0,1,-2.5>>


BOOL bDockProg[3]
BOOL bTriggerDialogue = FALSE
BOOL bForceRappel = FALSE
BOOL bMiniGameSuccess
BOOl bGodText
//BOOL b_cutscene_loaded
BOOL bDiveTask = FALSE
BOOL bVehicleHasBeenClose[COUNT_OF(MISSION_VEHICLES)]

//BOOL bTriggerHeli = FALSE
BOOL bReprintObjective = FALSE
BOOL bMichaelsCarColoursGrabbed
BOOL bMichaelsCarColoursSet
BOOL bFranklinInVeh = FALSE
BOOL bTrevorInVeh = FALSE

#IF IS_DEBUG_BUILD
	FLOAT f_fuel_mod_turbo
	FLOAT f_fuel_mod
#ENDIF

//PED_INDEX

//COVERPOINT_INDEX

CONST_INT MAX_ROPES 4

//VEHICLE_INDEX
VEHICLE_INDEX substolen

ROPE_INDEX SubRopes[MAX_ROPES]

OBJECT_INDEX objSwipe

//SEQUENCE_INDEX

//CAMERA_INDEX
CAMERA_INDEX cutscene_cam

BLIP_INDEX blip_this_location
BLIP_INDEX blip_pick_up_sub

SCALEFORM_INDEX SFDocksHUD

COVERPOINT_INDEX CovMissionCoverPoint[MAX_COVER_POINTS]

SCALEFORM_RETURN_INDEX scaleRet

ENUM SECTION_STAGE
	SECTION_STAGE_SETUP = 0,
	SECTION_STAGE_RUNNING,
	SECTION_STAGE_CLEANUP,
	SECTION_STAGE_SKIP,
	SECTION_STAGE_JUMPING_TO,
	SECTION_DEBUG
ENDENUM

SECTION_STAGE e_section_stage 		= SECTION_STAGE_SETUP


//---------------------------------:CUTSCENE VARS:------------------------------------------
BOOL b_skipped_mocap
INT i_current_event
BOOL b_is_jumping_directly_to_stage
BOOL bClearCutscenArea
VEHICLE_INDEX veh_pre_mission_car

BOOL bRunFailChecks
BOOL bCleanup 
BOOL bMissionStageLoaded
BOOL bcutsceneplaying
BOOL bdoorlocked[4]
BOOL bgotkey
BOOL bSwiped
BOOL bMissionPassed = FALSE
BOOl brappelled
BOOL bPausedSpeech
BOOL bDocksThingDone[3]
BOOL bHasPedGotAmmo
BOOL bdistancewarning
BOOL bComputerActive = FALSE
BOOL bTriggerWarning = FALSE
#IF IS_DEBUG_BUILD
BOOL bFailChecks = TRUE
#ENDIF
BOOL bTaskNonPlayer = FALSE

BOOL bGoToSpeach = FALSE
BOOL bWarpedDinghies

#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID objectwidget
#ENDIF			

INT iSetupProgress
INT iprogress
INT icutsceneprog
INT iCount
INT itimer
INT itimer3	
INT iRappelprog
INT iHitTime
//INT iMikeTimer
//INT iFrankTimer
INT iHeliAttack 
//INT iHeliAttackTimer
//INT iBulletTime

INT iMikeAI
INT iFrankAI
INT iGunmanAI
INT iPC
INT iLeadHeli = 3
INT iPtfx

INT iPadLeftX,iPadLeftY,iPadRightX,iPadRightY
INT iClick

INT iReplayStage

INT iSwitchStage 

INT iDialogueTimer

SELECTOR_CAM_STRUCT sSelectorCam

//Underwater sounds
INT sound_underwater_ambience		= GET_SOUND_ID()
int sound_underwater_rebreather		= GET_SOUND_ID()
INT i_sound_scuba_gear				= GET_SOUND_ID()
INT i_sound_swishing 				= GET_SOUND_ID()
BOOL b_allow_scuba_breath = TRUE

//FLOAT
FLOAT fstartheading

//VECTOR
VECTOR vStageStart  
VECTOR vsubcoords[2]

// *******************************************************************************************
//Crane stuff
// *******************************************************************************************
//BOOL b_swapped_crane
BOOL b_is_crane_cinematic_active

VECTOR v_crane_pos = << 488.4018, -3159.7063, 5.0000 >> 
BOOL b_is_audio_scene_active
CONST_FLOAT AUDIO_TRIGGER_THRESHOLD		0.01
CONST_FLOAT SPREADER_START_OFFSET		-4.0 

BOOL bCreateCrane

FLOAT MAX_CABIN_Y		=5.5
FLOAT MIN_CABIN_Y	    =-7.6
//	
FLOAT MAX_SPREADER_Z 		=-4.8
FLOAT MIN_SPREADER_Z 		=-14
//	
FLOAT MAX_CRANE_OFFSET	=15.0
FLOAT MIN_CRANE_OFFSET	=-76.600
//	
FLOAT MAX_CABIN_VEL		=5.0
FLOAT MAX_SPREADER_VEL	=5.0
FLOAT MAX_CRANE_VEL		=2.0
//	
FLOAT CABIN_ACCEL			=5.0
FLOAT SPREADER_ACCEL		=5.0
FLOAT CRANE_ACCEL			=2.0
//CRANE CAM

VECTOR v_spreader_attach_start = <<-15.1069, -25.8024, 8.8412>>

VECTOR v_cabin_attach_start = <<-15.1064, -25.8022, 8.8410>>
VECTOR v_cabin_point_start = <<-13.6553, -23.6718, 7.3063>>
VECTOR v_cabin_attach_end = <<-7.4302, -25.8017, 11.7590>>
VECTOR v_cabin_point_end = <<-6.4453, -23.4199, 10.2238>>
VECTOR v_spreader_point_start = <<-13.6548, -23.6745, 8.3039>> 
VECTOR v_spreader_attach_end =  <<-15.1069, -25.5991, -10.000>>
VECTOR v_spreader_point_end = <<-13.6548, -23.1884, -10.5>>
VECTOR v_attach_to_cam_value = <<-1.512, 3.0, -3.678>>
VECTOR v_crane_attach_cam_rot = <<-89.0, 0.0, -90.0>>
FLOAT f_crane_attach_cam_fov = 100

FLOAT f_crane_cam_fov = 45.0193


VECTOR vRopeTop1= <<-0.6, -0.7, -0.3>>
VECTOR vRopeBottom1 = <<-0.6, -0.7, 0.2>>

VECTOR vRopeTop2= <<0.6, -0.7, -0.3>>
VECTOR vRopeBottom2 = <<0.6, -0.7, 0.2>>

VECTOR vRopeTop3= <<-0.6, 0.7, -0.3>>
VECTOR vRopeBottom3 = <<-0.6, 0.7, 0.2>>

VECTOR vRopeTop4= <<0.6, 0.7, -0.3>>
VECTOR vRopeBottom4 = <<0.6, 0.7, 0.2>>

//Inside ropes

VECTOR vRopeTop5= <<2.6, 0.05, 0.3>>
VECTOR vRopeBottom5 = <<2.65, -0.05, 0.2>>

VECTOR vRopeTop6= <<2.6, 0.05, 0.3>>
VECTOR vRopeBottom6 = <<2.65, 0.05, 0.2>>

VECTOR vRopeTop7= <<-2.6, 0.05, 0.3>>
VECTOR vRopeBottom7 = <<-2.65, -0.05, 0.2>>

VECTOR vRopeTop8= <<-2.6, 0.05, 0.3>>
VECTOR vRopeBottom8 = <<-2.65,0.05, 0.2>>

BOOL b_added_ropes = FALSE
INT i_spreader_dampingframes			= 120
FLOAT f_ropes_len_change_rate			= 3.5
FLOAT f_ropes_min_len					= 2
FLOAT f_spreader_damping				= 1.3

SEQUENCE_INDEX seq


// *******************************************************************************************
//	STRUCTURES
// *******************************************************************************************
STRUCT CRANE_DATA
	OBJECT_INDEX obj_main
	OBJECT_INDEX obj_cabin
	OBJECT_INDEX obj_spreader
	OBJECT_INDEX obj_lift
	OBJECT_INDEX obj_wheels[8]
	OBJECT_INDEX obj_helper
	OBJECT_INDEX obj_boom_cable
	ROPE_INDEX ropes[8]
	
	VECTOR v_cabin_attach_offset
	VECTOR v_spreader_attach_offset
	VECTOR v_lift_attach_offset
	VECTOR v_start_pos
	VECTOR v_wheel_offsets[8]
	
	FLOAT f_crane_offset
	FLOAT f_cabin_vel
	FLOAT f_spreader_vel
	FLOAT f_lift_vel
	FLOAT f_crane_vel
	
	BOOL b_container_attached
	BOOL b_lift_active
	
	INT i_cabin_sound_id
	INT i_spreader_sound_id
	INT i_crane_sound_id
	INT i_lift_sound_id
	INT i_dampingframes
ENDSTRUCT
CRANE_DATA s_crane

RAPPEL_DATA rappelData
RAPPEL_DATA rappelDataFrank

STRUCT CONTAINER_SPOT
	VECTOR v_offset
	BOOL b_is_taken
	BOOL b_is_active
ENDSTRUCT


STRUCT CARGO_CONTAINER
	OBJECT_INDEX obj_main
	OBJECT_INDEX obj_left_door
	OBJECT_INDEX obj_right_door
	BLIP_INDEX blip
	
	CONTAINER_SPOT s_spots[2]
	BOOL b_is_on_boat
	BOOL b_is_full
	
	FLOAT f_speed
	FLOAT f_prev_speed
ENDSTRUCT

CARGO_CONTAINER s_containers[2]

LOCATES_HEADER_DATA sLocatesData

STRUCT ROPE_DATA
	ROPE_INDEX rope
	FLOAT f_length
	INT i_num_segments
	FLOAT f_length_per_segment
	BOOL bropecreated
ENDSTRUCT
ROPE_DATA s_rope

STRUCT ROCKET_DATA
	OBJECT_INDEX obj
	ENTITY_INDEX entity_owner
	VEHICLE_INDEX veh_target
	VECTOR v_dir
	VECTOR v_start
	VECTOR v_rot
	VECTOR v_offset
	VECTOR v_vel
	VECTOR v_prev_pos
	BOOL b_reached_target
	BOOL b_add_entity_speed
	BOOL b_rocket_belongs_to_player
	INT i_explode_timer
	FLOAT f_speed_multiplier
	PTFX_ID ptfx
ENDSTRUCT

ROCKET_DATA s_rockets[15]

// ***************************************** ENUMS *****************************************

// MISSION_ENUMS stages that make up each section of the mission
ENUM MISSION_STAGE_ENUM
    MISSION_STAGE_SETUP,
	MISSION_STAGE_OPENING_CUTSCENE,
	MISSION_STAGE_GET_TO_AIRSTRIP,
	MISSION_STAGE_PICK_UP_THE_SUB_FROM_AIRSTRIP,
	MISSION_STAGE_FLY_TO_SEA,
	//MISSION_STAGE_CLEAR_THE_AREA_OF_MERRY_WEATHER,
	MISSION_STAGE_DROP_THE_SUB,
	MISSION_STAGE_FIND_THE_CRATE,
	MISSION_STAGE_GET_TO_SURFACE,
	MISSION_STAGE_PICK_UP_SUB,
	MISSION_STAGE_DEFEND_AGAINST_MERRYWEATHER,
	MISSION_STAGE_FLY_AWAY,
	MISSION_STAGE_DROP_THE_SUB_AT_THE_AIRSTRIP,
	MISSION_STAGE_CLOSING_CUTSCENE,
	MISSION_STAGE_PASSED,
	MISSION_STAGE_FAIL
ENDENUM
MISSION_STAGE_ENUM eMissionStage = MISSION_STAGE_SETUP
MISSION_STAGE_ENUM eFailedStage = MISSION_STAGE_SETUP

ENUM CUSTOM_HOTSWAP
	CH_GENERAL,
	CH_FROM_OFFICE_TO_TREVOR,
	CH_FROM_TREVOR_IN_SUB_TO_FRANK,
	CH_FROM_TREVOR_IN_SUB_TO_MIKE,
	CH_SHOOTOUT_LONG_SWITCH,
	CH_BACK_TO_TREV_IN_SUBMARINE
ENDENUM

 //CUSTOM_HOTSWAP eCustomHotswap = CH_GENERAL

ENUM MISSION_FAIL_ENUM //different mission fail conditions
	GENERIC,
	TREVOR_DEAD,
	MICHAEL_DEAD,
	FRANKLIN_DEAD,
	WADE_DEAD,
	FLOYD_DEAD,
	TREVOR_LEFT,
	MICHAEL_LEFT,
	FRANKLIN_LEFT,
	CHINOOK_UNDRIVABLE,
	CHINOOK_STUCK,
	MICHAELS_CAR_UNDRIVABLE,
	SUBMERSIBLE_UNDRIVABLE,
	BUDDY_LEFT,
	CARGOBOB_ABADONED,
	DISCOVERED,
	DIDNT_LOSE_MERRYWEATHER
ENDENUM
MISSION_FAIL_ENUM reason_for_fail = GENERIC


enumCharacterList current_player_model

//Missile

STRUCT MISSILE_DATA
	OBJECT_INDEX obj
	FLOAT f_forward_vel
	FLOAT f_turn_vel
	FLOAT f_dist_from_target
	//FLOAT f_pitch_vel
	VECTOR v_pos
	VECTOR v_vel
	VECTOR v_rot
	FLOAT f_fuel
	BOOL b_destroyed
	// i_ptfx
ENDSTRUCT

VECTOR v_targets[MAX_NUM_TARGETS]
INT i_current_target
FLOAT f_uav_vel							= 25.0



ENUM DH2B_SWITCH_CAM_STATE
	SWITCH_CAM_IDLE,
	SWITCH_CAM_REQUEST_ASSETS,
	SWITCH_CAM_SETUP_SPLINE,
	SWITCH_CAM_PLAYING_SPLINE,
	SWITCH_CAM_SHUTDOWN_SPLINE,
	SWITCH_CAM_RETURN_TO_GAMEPLAY
ENDENUM
DH2B_SWITCH_CAM_STATE eSwitchCamState = SWITCH_CAM_IDLE

SWITCH_CAM_STRUCT scsSwitchCam_MichaelToTrevor
FLOAT fSwitchCamHeliToSub_HeliTeleportHeight = 15.0
FLOAT fSwitchCam_StartingVelocityScale = 0.2

FLOAT fSwitchCamHeliToSub_SubForwardMomentum = 10.0

FLOAT fSwitchCamHeliToSub_GameplayCamHeading = 0.0
FLOAT fSwitchCamHeliToSub_GameplayCamPitch = 0.0

BOOL bSwitchCamHeliToSub_PlayingTrevorAnims
FLOAT fSwitchCamheliToSub_StartTrevorAnimsPhase = 0.18

BOOL bDoneHitSound
BOOL bWooshStarted

#IF IS_DEBUG_BUILD
BOOL bSwitchCamDebugScenarioEnabled = FALSE
//BOOL bResetDebugScenario = FALSE
#ENDIF


PROC SETUP_SPLINE_CAM_NODE_ARRAY_SUBMARINE_SWITCH(SWITCH_CAM_STRUCT &thisSwitchCam, VEHICLE_INDEX viSub )
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY_SUBMARINE_SWITCH")
	
	IF NOT thisSwitchCam.bInitialized


		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[0].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<-6.3674, 8.7696, 13.0000>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[0].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[0].vNodeDir = <<2.6410, -1.9730, 5.2269>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].fNodeFOV = 20.0000
		thisSwitchCam.nodes[0].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].iNodeToClone = 0
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[0].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[0].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[0].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 0.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[1].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 1000
		thisSwitchCam.nodes[1].vNodePos = <<-6.3610, 9.2298, 13.5000>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[1].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[1].vNodeDir = <<2.5700, -1.3947, 3.8611>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].fNodeFOV = 20.0000
		thisSwitchCam.nodes[1].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].iNodeToClone = 0
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.9650
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[1].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[1].iCamEaseType = 0
		thisSwitchCam.nodes[1].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[1].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[1].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 0.05
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = TRUE
		thisSwitchCam.nodes[1].SCFE_FlashEffectUsed = SCFE_SwitchSceneMichael
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[2].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 1000
		thisSwitchCam.nodes[2].vNodePos = <<-6.3610, 9.2298, 13.5000>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[2].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[2].vNodeDir = <<2.5700, -1.3947, 3.8611>>
		thisSwitchCam.nodes[2].bPointAtEntity = TRUE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].fNodeFOV = 20.0000
		thisSwitchCam.nodes[2].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].iNodeToClone = 0
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.9650
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[2].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[2].iCamEaseType = 0
		thisSwitchCam.nodes[2].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[2].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[2].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 0.5000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bFlashEnabled = FALSE
		thisSwitchCam.nodes[2].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[2].fMinExposure = 0.0000
		thisSwitchCam.nodes[2].fMaxExposure = 0.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 0
		thisSwitchCam.nodes[2].iRampDownDuration = 0
		thisSwitchCam.nodes[2].iHoldDuration = 0
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[3].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[3].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[3].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[3].iNodeTime = 1000
		thisSwitchCam.nodes[3].vNodePos = <<-6.4675, 9.8042, 6.2917>>
		thisSwitchCam.nodes[3].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[3].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[3].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[3].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[3].vNodeDir = <<0.2768, -0.5341, 1.5624>>
		thisSwitchCam.nodes[3].bPointAtEntity = TRUE
		thisSwitchCam.nodes[3].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[3].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[3].fNodeFOV = 25.0000
		thisSwitchCam.nodes[3].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[3].iNodeToClone = 0
		thisSwitchCam.nodes[3].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[3].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[3].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[3].fNodeMotionBlur = 0.3000
		thisSwitchCam.nodes[3].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[3].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[3].iCamEaseType = 0
		thisSwitchCam.nodes[3].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[3].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[3].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[3].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[3].fTimeScale = 1.0000
		thisSwitchCam.nodes[3].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[3].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[3].bFlashEnabled = FALSE
		thisSwitchCam.nodes[3].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[3].fMinExposure = 0.0000
		thisSwitchCam.nodes[3].fMaxExposure = 0.0000
		thisSwitchCam.nodes[3].iRampUpDuration = 0
		thisSwitchCam.nodes[3].iRampDownDuration = 0
		thisSwitchCam.nodes[3].iHoldDuration = 0
		thisSwitchCam.nodes[3].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[3].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[3].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[4].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[4].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[4].iNodeTime = 1000
		thisSwitchCam.nodes[4].vNodePos = <<-4.4901, 10.2047, 3.2430>>
		thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[4].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[4].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[4].vNodeDir = <<0.5460, 0.4726, -0.4947>>
		thisSwitchCam.nodes[4].bPointAtEntity = TRUE
		thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[4].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[4].fNodeFOV = 30.0000
		thisSwitchCam.nodes[4].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].iNodeToClone = 0
		thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[4].fNodeMotionBlur = 0.3000
		thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[4].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[4].iCamEaseType = 0
		thisSwitchCam.nodes[4].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[4].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[4].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[4].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[4].fTimeScale = 1.0000
		thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[4].bFlashEnabled = FALSE
		thisSwitchCam.nodes[4].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[4].fMinExposure = 0.0000
		thisSwitchCam.nodes[4].fMaxExposure = 0.0000
		thisSwitchCam.nodes[4].iRampUpDuration = 0
		thisSwitchCam.nodes[4].iRampDownDuration = 0
		thisSwitchCam.nodes[4].iHoldDuration = 0
		thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.5000
		thisSwitchCam.nodes[4].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[5].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[5].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[5].iNodeTime = 1000
		thisSwitchCam.nodes[5].vNodePos = <<3.5055, 8.4825, 1.0037>>
		thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[5].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[5].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[5].vNodeDir = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].bPointAtEntity = TRUE
		thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[5].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[5].fNodeFOV = 35.0000
		thisSwitchCam.nodes[5].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].iNodeToClone = 0
		thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 2.0000
		thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[5].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[5].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[5].iCamEaseType = 0
		thisSwitchCam.nodes[5].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[5].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[5].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[5].fTimeScale = 1.0000
		thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[5].bFlashEnabled = FALSE
		thisSwitchCam.nodes[5].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[5].fMinExposure = 0.0000
		thisSwitchCam.nodes[5].fMaxExposure = 0.0000
		thisSwitchCam.nodes[5].iRampUpDuration = 0
		thisSwitchCam.nodes[5].iRampDownDuration = 0
		thisSwitchCam.nodes[5].iHoldDuration = 0
		thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[5].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[6].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[6].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[6].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[6].iNodeTime = 1000
		thisSwitchCam.nodes[6].vNodePos = <<10.1406, -1.3298, 2.2008>>
		thisSwitchCam.nodes[6].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[6].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[6].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[6].vNodeDir = <<-0.0640, -0.0139, 0.3411>>
		thisSwitchCam.nodes[6].bPointAtEntity = TRUE
		thisSwitchCam.nodes[6].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[6].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[6].fNodeFOV = 40.0000
		thisSwitchCam.nodes[6].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].iNodeToClone = 0
		thisSwitchCam.nodes[6].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[6].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[6].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[6].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[6].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[6].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[6].iCamEaseType = 0
		thisSwitchCam.nodes[6].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[6].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[6].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[6].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[6].fTimeScale = 0.0000
		thisSwitchCam.nodes[6].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[6].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[6].bFlashEnabled = FALSE
		thisSwitchCam.nodes[6].SCFE_FlashEffectUsed = SCFE_CODE_FLASH
		thisSwitchCam.nodes[6].fMinExposure = 0.0000
		thisSwitchCam.nodes[6].fMaxExposure = 0.0000
		thisSwitchCam.nodes[6].iRampUpDuration = 0
		thisSwitchCam.nodes[6].iRampDownDuration = 0
		thisSwitchCam.nodes[6].iHoldDuration = 0
		thisSwitchCam.nodes[6].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[6].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[6].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[7].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[7].iForceCamPointAtEntityIndex = -1
		thisSwitchCam.nodes[7].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[7].iNodeTime = 1000
		thisSwitchCam.nodes[7].vNodePos = <<1.0680, -14.4020, 2.1636>>
		thisSwitchCam.nodes[7].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[7].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[7].fNodeVerticleOffset = 0.0000
		thisSwitchCam.nodes[7].bAttachToOriginPed = FALSE
		thisSwitchCam.nodes[7].vNodeDir = <<0.3122, -0.0042, 2.1375>>
		thisSwitchCam.nodes[7].bPointAtEntity = TRUE
		thisSwitchCam.nodes[7].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[7].bAttachOffsetIsRelative = TRUE
		thisSwitchCam.nodes[7].fNodeFOV = 49.0000
		thisSwitchCam.nodes[7].vClonedNodeOffset = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[7].iNodeToClone = 0
		thisSwitchCam.nodes[7].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[7].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[7].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[7].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[7].NodeCamShakeType = CAM_SHAKE_DEFAULT
		thisSwitchCam.nodes[7].fNodeCamShake = 0.0000
		thisSwitchCam.nodes[7].iCamEaseType = 0
		thisSwitchCam.nodes[7].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[7].fCamNodeVelocityScale = 0.0000
		thisSwitchCam.nodes[7].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[7].bCamEaseForceLevel = FALSE
		thisSwitchCam.nodes[7].fTimeScale = 0.0000
		thisSwitchCam.nodes[7].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[7].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[7].bFlashEnabled = TRUE
		thisSwitchCam.nodes[7].SCFE_FlashEffectUsed = SCFE_SwitchShortTrevorMid
		thisSwitchCam.nodes[7].fMinExposure = 0.0000
		thisSwitchCam.nodes[7].fMaxExposure = 0.0000
		thisSwitchCam.nodes[7].iRampUpDuration = 0
		thisSwitchCam.nodes[7].iRampDownDuration = 0
		thisSwitchCam.nodes[7].iHoldDuration = 0
		thisSwitchCam.nodes[7].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[7].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[7].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.camVelocityOverrides[0].fStartPoint = 0.0000
		thisSwitchCam.camVelocityOverrides[0].fSpeed = 0.4000
		thisSwitchCam.camVelocityOverrides[1].fStartPoint = 1.0000
		thisSwitchCam.camVelocityOverrides[1].fSpeed = 0.4000
		thisSwitchCam.camVelocityOverrides[2].fStartPoint = 1.0000
		thisSwitchCam.camVelocityOverrides[2].fSpeed = 1.0000
		thisSwitchCam.camVelocityOverrides[3].fStartPoint = 2.0000
		thisSwitchCam.camVelocityOverrides[3].fSpeed = 10.0000
		thisSwitchCam.camVelocityOverrides[4].fStartPoint = 3.0000
		thisSwitchCam.camVelocityOverrides[4].fSpeed = 20.0000
		thisSwitchCam.camVelocityOverrides[5].fStartPoint = 3.6000
		thisSwitchCam.camVelocityOverrides[5].fSpeed = 20.0000
		thisSwitchCam.camVelocityOverrides[6].fStartPoint = 4.4000
		thisSwitchCam.camVelocityOverrides[6].fSpeed = 5.0000
		thisSwitchCam.camVelocityOverrides[7].fStartPoint = 5.8000
		thisSwitchCam.camVelocityOverrides[7].fSpeed = 20.0000
		thisSwitchCam.camVelocityOverrides[8].fStartPoint = 6.0000
		thisSwitchCam.camVelocityOverrides[8].fSpeed = 20.0000
		thisSwitchCam.camVelocityOverrides[9].fStartPoint = 7.0000
		thisSwitchCam.camVelocityOverrides[9].fSpeed = 40.0000

		thisSwitchCam.camBlurOverrides[0].fBlurOverrideStartPoint = 0.0000
		thisSwitchCam.camBlurOverrides[0].fBlurOverrideBlurLevel = 0.0000
		thisSwitchCam.camBlurOverrides[1].fBlurOverrideStartPoint = 0//1.0000
		thisSwitchCam.camBlurOverrides[1].fBlurOverrideBlurLevel = 0//1.0000
		thisSwitchCam.camBlurOverrides[2].fBlurOverrideStartPoint = 0//1.0000
		thisSwitchCam.camBlurOverrides[2].fBlurOverrideBlurLevel = 0//1.0000
		thisSwitchCam.camBlurOverrides[3].fBlurOverrideStartPoint = 0//5.0000
		thisSwitchCam.camBlurOverrides[3].fBlurOverrideBlurLevel = 0//1.0000

		thisSwitchCam.iNumNodes = 8
		thisSwitchCam.iCamSwitchFocusNode = 0
		thisSwitchCam.fSwitchSoundAudioStartPhase = 0.13
		thisSwitchCam.fSwitchSoundAudioEndPhase = 0.5
		thisSwitchCam.bSwitchSoundPlayOpeningPulse = TRUE
		thisSwitchCam.bSwitchSoundPlayMoveLoop = TRUE
		thisSwitchCam.bSwitchSoundPlayExitPulse = TRUE
		thisSwitchCam.bSplineNoSmoothing = TRUE
		thisSwitchCam.bAddGameplayCamAsLastNode = TRUE
		thisSwitchCam.iGameplayNodeBlendDuration = 100


		//--- End of Cam Data ---


		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_HEIST_DOCKS_MichaelToSubmarine_B.txt"	
		thisSwitchCam.strXMLFileName = "CameraInfo_HEIST_DOCKS_MichaelToSubmarine_B.xml"
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	//thisSwitchCam.piPeds[0] = piMichael
	thisSwitchCam.viVehicles[0] = viSub
	
		
ENDPROC

/// PURPOSE:
///    Handle the playback of the switch cam.  The will setup, play, shut down and cleanup the switch cam spline
/// PARAMS:
///    thisSwitchCam - Switch camera to playback
///    bReturnTrueOnce - If you only want this function to return TRUE once upon completion. Set to FALSE to have this function to return true every time this is called once the camera is finished playback.
/// RETURNS:
///    TRUE when camera is complete, FALSE if playback is still active, or if the camera has completed playback and bReturnTrueOnce is set to TRUE.
FUNC BOOL HANDLE_SWITCH_CAM_SUBMARINE(SWITCH_CAM_STRUCT &thisSwitchCam, BOOL bReturnTrueOnce = TRUE)
	
	INT iCurrentNode
	VECTOR vHeliTemp
	VECTOR vHeliTempWaterCheck
	
	SWITCH eSwitchCamState

		CASE SWITCH_CAM_IDLE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")
		BREAK
		
		CASE SWITCH_CAM_REQUEST_ASSETS
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_REQUEST_ASSETS")
			
			REQUEST_ANIM_DICT("missswitch")

		BREAK
		
		CASE SWITCH_CAM_SETUP_SPLINE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_SETUP_SPLINE")
			
			REQUEST_ANIM_DICT("missswitch")
			
			SETUP_SPLINE_CAM_NODE_ARRAY_SUBMARINE_SWITCH(thisSwitchCam, mission_veh[MV_SUBMERISIBLE].veh)
			IF LOAD_CAM_SHAKE_LIBRARIES(thisSwitchCam)
			AND HAS_ANIM_DICT_LOADED("missswitch")

				DESTROY_ALL_CAMS()			

				CREATE_SPLINE_CAM(thisSwitchCam)
				
				SET_CAM_ACTIVE(thisSwitchCam.ciSpline, TRUE)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				DISPLAY_RADAR(FALSE)
				DISPLAY_HUD(FALSE)	
				
				SETTIMERB(0)
				
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)

				SET_VEHICLE_LIGHTS(mission_veh[MV_SUBMERISIBLE].veh, SET_VEHICLE_LIGHTS_ON)
				
				vHeliTemp = GET_ENTITY_COORDS(mission_veh[0].veh)
				vHeliTempWaterCheck = GET_ENTITY_COORDS(mission_veh[0].veh)
				
				vHeliTemp.Z = fSwitchCamHeliToSub_HeliTeleportHeight
				SET_ENTITY_COORDS(mission_veh[0].veh, <<-1389.2123, 6163.0684, 16.4602>>/*vHeliTemp*/, TRUE, TRUE, TRUE)
				SET_ENTITY_ROTATION(mission_veh[0].veh, <<-1.2180, 0.0503, 39.5149>>)
				
				IF IS_ENTITY_IN_WATER(mission_veh[MV_SUBMERISIBLE].veh)
				AND vHeliTempWaterCheck.Z > 9.5
					//[MF] Sub is underwater, so just warping to the end of the spline.
					SET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline, 0.5)
				ELSE
					vHeliTemp.Z = vHeliTemp.Z - 6.7
					SET_ENTITY_COORDS(mission_veh[MV_SUBMERISIBLE].veh, <<-1389.2494, 6163.1162, 10.2905>>, TRUE, TRUE, TRUE)
					SET_ENTITY_ROTATION(mission_veh[MV_SUBMERISIBLE].veh, <<-27.9389, 1.3027, 39.7102>>)
				ENDIF 
				
				SET_ENTITY_VELOCITY(mission_veh[0].veh, GET_ENTITY_VELOCITY(mission_veh[0].veh) * fSwitchCam_StartingVelocityScale)
				SET_ENTITY_VELOCITY(mission_veh[1].veh, GET_ENTITY_VELOCITY(mission_veh[1].veh) * fSwitchCam_StartingVelocityScale)
				bSwitchCamHeliToSub_PlayingTrevorAnims = FALSE
				bDoneHitSound = FALSE
				bWooshStarted = FALSE

				eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE
			ENDIF
		BREAK
		
		CASE SWITCH_CAM_PLAYING_SPLINE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE")
			CDEBUG3LN(DEBUG_MISSION, GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline))

			iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(fSwitchCamHeliToSub_GameplayCamHeading)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(fSwitchCamHeliToSub_GameplayCamPitch)

			IF NOT bSwitchCamHeliToSub_PlayingTrevorAnims
				IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) > fSwitchCamheliToSub_StartTrevorAnimsPhase
					TASK_PLAY_ANIM(PLAYER_PED_ID(), "missswitch", "mid_mission_inside_helicopter_trevor", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
					bSwitchCamHeliToSub_PlayingTrevorAnims = TRUE
				ENDIF
			ENDIF
			
			IF IS_CAM_ACTIVE(thisSwitchCam.ciSpline)
				IF iCurrentNode >= thisSwitchCam.iCamSwitchFocusNode + 1
				AND NOT bDoneHitSound
					PRINTLN("@@@@@@@@@@ PLAY_SOUND_FRONTEND - Hit_Out @@@@@@@@@@")
					PLAY_SOUND_FRONTEND(-1, "Hit_Out", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
					iHitTime = GET_GAME_TIMER()
					bDoneHitSound = TRUE
				ENDIF
				IF iCurrentNode >= thisSwitchCam.iCamSwitchFocusNode + 2
				AND bDoneHitSound
				AND NOT bWooshStarted
				AND GET_GAME_TIMER() >= iHitTime + 300
					PRINTLN("@@@@@@@@@@ PLAY_SOUND_FRONTEND - Short_Transition_In @@@@@@@@@@")
					PLAY_SOUND_FRONTEND(-1, "Short_Transition_In", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
					bWooshStarted = TRUE
				ENDIF
				//[MF] Once cam is at the end, reset the gameplay cam positon and advance to next state.
				IF GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline) >= 1.00
					CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")
					eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
			
		FALLTHRU

		CASE SWITCH_CAM_RETURN_TO_GAMEPLAY

			SET_TIME_SCALE(1.0)

			RENDER_SCRIPT_CAMS(FALSE, FALSE)

			IF DOES_CAM_EXIST(thisSwitchCam.ciSpline)
				SET_CAM_ACTIVE(thisSwitchCam.ciSpline, FALSE)
				DESTROY_CAM(thisSwitchCam.ciSpline)
			ENDIF
			DESTROY_ALL_CAMS()

			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)

			CLEAR_PED_TASKS(PLAYER_PED_ID())

			SET_ENTITY_VELOCITY(mission_veh[MV_SUBMERISIBLE].veh, GET_ENTITY_FORWARD_VECTOR(mission_veh[MV_SUBMERISIBLE].veh) * fSwitchCamHeliToSub_SubForwardMomentum)
			SET_VEHICLE_ENGINE_ON(mission_veh[1].veh, TRUE, TRUE)
			
			SETTIMERA(0)
			
			eSwitchCamState = SWITCH_CAM_IDLE

			thisSwitchCam.bIsSplineCamFinishedPlaying = TRUE
			sCamDetails.brun = FALSE
			
			REMOVE_ANIM_DICT("missswitch")
			
			SET_PLAYER_CONTROL(PLAYER_ID(), true)

			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	IF bReturnTrueOnce
		RETURN FALSE
	ELSE
		RETURN thisSwitchCam.bIsSplineCamFinishedPlaying
	ENDIF
ENDFUNC

/// PURPOSE:
///    Check if the submarine is above the barge (used for custom switch logic)
/// RETURNS:
///    TRUE if the vehicle used for Trevor's submarine is above the barge.  FALSE otherwise.
FUNC BOOL IS_SUB_ABOVE_BARGE()

	IF IS_ENTITY_IN_ANGLED_AREA(mission_veh[1].veh, (<<-1331.4, 6129.2, 6.6>>), (<<-1284.1, 6136.8, 6.0>>), 60.0, TRUE, FALSE)
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC

#IF IS_DEBUG_BUILD

/// PURPOSE:
///    Create the debug widgets for working with the scripted character switch cam.
PROC CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
	CDEBUG3LN(DEBUG_MISSION, "CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS")

	START_WIDGET_GROUP("Custom Switch Cameras - Extra Tunables")
		
		START_WIDGET_GROUP("Michael To Trevor")
			ADD_WIDGET_BOOL("Skip To Switch Scene", bSwitchCamDebugScenarioEnabled)
			ADD_WIDGET_FLOAT_SLIDER("Heli Switch Teleport Height", fSwitchCamHeliToSub_HeliTeleportHeight, 0.0, 50.0, 0.5)
			ADD_WIDGET_FLOAT_SLIDER("Velocity Starting Scale", fSwitchCam_StartingVelocityScale, 0.0, 2.0, 0.1)
			
			ADD_WIDGET_FLOAT_SLIDER("Start Trev Anim Spline Phase", fSwitchCamheliToSub_StartTrevorAnimsPhase, 0.0, 1.0, 0.1)

			ADD_WIDGET_FLOAT_SLIDER("Sub Forward Momentum", fSwitchCamHeliToSub_SubForwardMomentum, 0.0, 50.0, 2.0)
			ADD_WIDGET_FLOAT_SLIDER("Gameplay Cam Heading", fSwitchCamHeliToSub_GameplayCamHeading, -360.0, 360.0, 10.0)
			ADD_WIDGET_FLOAT_SLIDER("Gameplay Cam Pitch", fSwitchCamHeliToSub_GameplayCamPitch, -360.0, 360.0, 10.0)
		STOP_WIDGET_GROUP()
		
	STOP_WIDGET_GROUP()
	
ENDPROC

#ENDIF


/////////////   DEBUG STUFF ***************************************************************************************************

#IF IS_DEBUG_BUILD	

	// PRINT_PROGRESS ************************************************************************************************************
	INT iMissionStageDB
	INT iSetupProgressDB
	INT iprogressDB
	INT icutsceneprogDB
	
	//Prints thigs when they change value
	PROC PRINT_PROGRESS()

		IF iMissionStageDB <> ENUM_TO_INT(eMissionStage)
			iMissionStageDB = ENUM_TO_INT(eMissionStage)
			PRINTSTRING("MISSION_STAGE_ENUM = ")PRINTINT(iMissionStageDB)PRINTNL()
		ENDIF  
		IF iprogressDB <> iprogress
			iprogressDB = iprogress
			PRINTSTRING("iprogress = ")PRINTINT(iprogressDB)PRINTNL()
		ENDIF

		IF iSetupProgressDB <> iSetupProgress
			iSetupProgressDB = iSetupProgress
			PRINTSTRING("iSetupProgress = ")PRINTINT(iSetupProgress)PRINTNL()
		ENDIF
		IF icutsceneprogDB <> icutsceneprog
			icutsceneprogDB = icutsceneprog
			PRINTSTRING("icutsceneprog = ")PRINTINT(icutsceneprog)PRINTNL()
		ENDIF
		
	ENDPROC

#ENDIF 

// ******************************* PROCEDURES AND FUNCTIONS ********************************

PROC CLEAN_UP_BARREL_GUYS(BOOL bFinal = FALSE)

	IF DOES_ENTITY_EXIST(enemy[44].obj)
		IF bFinal = FALSE
			DELETE_OBJECT(enemy[44].obj)
		ELSE
			IF NOT IS_ENTITY_DEAD(enemy[44].obj)
				IF IS_ENTITY_ATTACHED(enemy[44].obj)
					DETACH_ENTITY(enemy[44].obj)
					SET_OBJECT_AS_NO_LONGER_NEEDED(enemy[44].obj)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(enemy[44].ped)
		IF bFinal = FALSE
			DELETE_PED(enemy[44].ped)
		ELSE
			IF NOT IS_PED_INJURED(enemy[44].ped)
				SET_PED_AS_NO_LONGER_NEEDED(enemy[44].ped)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(enemy[45].obj)
		IF bFinal = FALSE
			DELETE_OBJECT(enemy[45].obj)
		ELSE
			IF NOT IS_ENTITY_DEAD(enemy[45].obj)
				IF IS_ENTITY_ATTACHED(enemy[45].obj)
					DETACH_ENTITY(enemy[45].obj)
					SET_OBJECT_AS_NO_LONGER_NEEDED(enemy[45].obj)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(enemy[45].ped)
		IF bFinal = FALSE
			DELETE_PED(enemy[45].ped)
		ELSE
			IF NOT IS_PED_INJURED(enemy[45].ped)
				SET_PED_AS_NO_LONGER_NEEDED(enemy[45].ped)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(enemy[44].ped)
	AND NOT DOES_ENTITY_EXIST(enemy[45].ped)
	AND NOT DOES_ENTITY_EXIST(enemy[44].obj)
	AND NOT DOES_ENTITY_EXIST(enemy[45].obj)
		SET_MODEL_AS_NO_LONGER_NEEDED(Prop_CS_Fertilizer)
		REMOVE_ANIM_DICT("misschinese2_barrelroll")
	ENDIF
	
ENDPROC

PROC REMOVE_OBJECT(OBJECT_INDEX &obj, BOOL b_force_delete = FALSE)
	IF DOES_ENTITY_EXIST(obj)
		IF NOT IS_ENTITY_DEAD(obj)
			IF IS_ENTITY_ATTACHED(obj)
				DETACH_ENTITY(obj)
			ENDIF
		ENDIF

		IF b_force_delete
			DELETE_OBJECT(obj)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(obj)
		ENDIF
	ENDIF
ENDPROC	

PROC REMOVE_ALL_OBJECTS(BOOL b_force_delete = FALSE)
	REMOVE_OBJECT(s_crane.obj_spreader, TRUE)
	REMOVE_OBJECT(s_crane.obj_cabin, TRUE)
	REMOVE_OBJECT(s_crane.obj_lift, TRUE)
	REMOVE_OBJECT(s_crane.obj_main, TRUE)
	REMOVE_OBJECT(s_crane.obj_helper, TRUE)
	REMOVE_OBJECT(s_crane.obj_boom_cable, TRUE)
	
	INT i = 0
	REPEAT COUNT_OF(s_containers) i
		REMOVE_OBJECT(s_containers[i].obj_left_door, b_force_delete)
		REMOVE_OBJECT(s_containers[i].obj_right_door, b_force_delete)
		REMOVE_OBJECT(s_containers[i].obj_main, b_force_delete)
	ENDREPEAT
	
	REPEAT COUNT_OF(s_crane.obj_wheels) i
		REMOVE_OBJECT(s_crane.obj_wheels[i], TRUE)
	ENDREPEAT
	
ENDPROC


PROC CLEAN_UP_AUDIO_SCENES()
	
	IF IS_AUDIO_SCENE_ACTIVE("DH_2B_DRIVE_TO_AIRSTRIP")
		STOP_AUDIO_SCENE("DH_2B_DRIVE_TO_AIRSTRIP")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DH_2B_ATTACH_SUB")
		STOP_AUDIO_SCENE("DH_2B_ATTACH_SUB")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DH_2B_GET_TO_DROP_ZONE")
		STOP_AUDIO_SCENE("DH_2B_GET_TO_DROP_ZONE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DH_2B_SEE_BOATS")
		STOP_AUDIO_SCENE("DH_2B_SEE_BOATS")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DH_2B_CHOPPERS_APPEAR")
		STOP_AUDIO_SCENE("DH_2B_CHOPPERS_APPEAR")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DH_2B_FIGHT_CHOPPERS")
		STOP_AUDIO_SCENE("DH_2B_FIGHT_CHOPPERS")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DH_2B_RELEASE_SUB")
		STOP_AUDIO_SCENE("DH_2B_RELEASE_SUB")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DH_2B_FIND_CONTAINER")
		STOP_AUDIO_SCENE("DH_2B_FIND_CONTAINER")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DH_2B_GET_TO_SURFACE")
		STOP_AUDIO_SCENE("DH_2B_GET_TO_SURFACE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DH_2B_PICK_UP_SUB_CARGO")
		STOP_AUDIO_SCENE("DH_2B_PICK_UP_SUB_CARGO")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DH_2B_ESCAPE_ENEMIES")
		STOP_AUDIO_SCENE("DH_2B_ESCAPE_ENEMIES")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DH_2B_RETURN_TO_AIRSTRIP")
		STOP_AUDIO_SCENE("DH_2B_RETURN_TO_AIRSTRIP")
	ENDIF
	

ENDPROC

//PURPOSE: cleans up all resources used in the mission
PROC MISSION_CLEANUP(BOOL bfinal)
	
	TRIGGER_MUSIC_EVENT("DH2B_FAIL")
	IF bfinal = FALSE
		RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR, bHasChanged)  //reset that the player has changed outfit on mission to how it was at the start of the mission
		RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL, bHasChanged)
		RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_FRANKLIN, bHasChanged)
	ENDIF
	
	INT i
	
	#IF IS_DEBUG_BUILD
		IF IS_CUTSCENE_ACTIVE()
		AND IS_CUTSCENE_PLAYING()
			STOP_CUTSCENE()
			WHILE IS_CUTSCENE_ACTIVE()
				WAIT(0)
			ENDWHILE
		ENDIF
	#ENDIF
	
	REPEAT 4 i
		IF DOES_ROPE_EXIST(subRopes[i])
			DELETE_ROPE(subRopes[i])
		ENDIF
	ENDREPEAT
	
	ROPE_UNLOAD_TEXTURES()
	
	IF DOES_ENTITY_EXIST(obj_dummy_target)
		DELETE_OBJECT(obj_dummy_target)
	ENDIF
	
	IF DOES_ENTITY_EXIST( objSofa )
		SET_ENTITY_VISIBLE( objSofa, TRUE )
		SET_OBJECT_AS_NO_LONGER_NEEDED( objSofa )
	ENDIF
	
	IF IS_SCREEN_FADED_OUT()
		CLEAR_PED_WETNESS(PLAYER_PED_ID())
	ENDIF
	
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
	
	RELEASE_AMBIENT_AUDIO_BANK()
	
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
		RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			RELEASE_PED_PRELOAD_PROP_DATA(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			RELEASE_PED_PRELOAD_VARIATION_DATA(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			RELEASE_PED_PRELOAD_PROP_DATA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			RELEASE_PED_PRELOAD_VARIATION_DATA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			RELEASE_PED_PRELOAD_PROP_DATA(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			RELEASE_PED_PRELOAD_VARIATION_DATA(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		ENDIF
	ENDIF
	
	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxDrips)
		STOP_PARTICLE_FX_LOOPED(ptfxDrips)
	ENDIF
	
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)

	CLEAN_UP_AUDIO_SCENES()
	REMOVE_SCENARIO_BLOCKING_AREAS()
	SET_BUILDING_STATE(BUILDINGNAME_IPL_AIRFIELD_PROPS, BUILDINGSTATE_DESTROYED)
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
	CLEAR_TRIGGERED_LABELS()
	//CLEAR_TIMECYCLE_MODIFIER()
	SET_VEHICLE_POPULATION_BUDGET(3)
	
	ENABLE_SECOND_SCREEN_TRACKIFY_APP(FALSE)
	
	SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_MICHAEL, SAVEHOUSE_TREVOR_VB, FALSE) 
	SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_FRANKLIN, SAVEHOUSE_TREVOR_VB, FALSE) 
	
	DISPLAY_HUD(TRUE)
	DISPLAY_RADAR(TRUE)
	
	IF DOES_CAM_EXIST(sCamDetails.camID)
		DESTROY_CAM(sCamDetails.camID)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedUnderWater[0])
		DELETE_PED(pedUnderWater[0])
	ENDIF
	
	FOR i = 0 TO MAX_DRIVER_PED - 1
		CLEANUP_AI_PED_BLIP(driver[i].EnemyBlipData)
	ENDFOR
	
	FOR i = 0 TO MAX_ENEMY_PED - 1
		CLEANUP_AI_PED_BLIP(enemy[i].EnemyBlipData)
	ENDFOR
	
	DESTROY_ALL_CAMS()
	
	IF IS_CUTSCENE_ACTIVE()
		STOP_CUTSCENE()
		REMOVE_CUTSCENE()
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, FALSE)
	ENDIF

	ALLOW_DIALOGUE_IN_WATER (FALSE)
	CLEAN_UP_BARREL_GUYS()
	
	bDockProg[0] = FALSE 
	bDockProg[1] = FALSE 
	bDockProg[2] = FALSE 
	bForceRappel = FALSE

	resetSweepData()
	
	IF DOES_BLIP_EXIST(blip_this_location)
		REMOVE_BLIP(blip_this_location)
	ENDIF
	
	REPEAT COUNT_OF(s_rockets) i
		REMOVE_OBJECT(s_rockets[i].obj, bfinal)
	ENDREPEAT
	
	IF DOES_ENTITY_EXIST(objSwipe)
		REMOVE_OBJECT(objSwipe, bfinal)
	ENDIF
	
	REPEAT COUNT_OF(objSeabed) i
		IF DOES_ENTITY_EXIST(objSeabed[i])
			DELETE_OBJECT(objSeabed[i])
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(objMission) i
		IF DOES_ENTITY_EXIST(objMission[i])
			DELETE_OBJECT(objMission[i])
		ENDIF
	ENDREPEAT

	REPEAT COUNT_OF(pedUnderWater) i
		IF DOES_ENTITY_EXIST(pedUnderWater[i])
			DELETE_PED(pedUnderWater[i])
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(merry_weather_ptfx) i
		IF DOES_PARTICLE_FX_LOOPED_EXIST(merry_weather_ptfx[i])
			REMOVE_PARTICLE_FX(merry_weather_ptfx[i])
		ENDIF
	ENDREPEAT
	
	DESTROY_ALL_CAMS()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			SET_ENTITY_COLLISION(PLAYER_PED_ID(),TRUE)
		ENDIF
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
	ENDIF
	
	CLEAR_WEATHER_TYPE_PERSIST()
//	CLEAR_TIMECYCLE_MODIFIER()
	
	IF DOES_CAM_EXIST(cutscene_cam)
		DESTROY_CAM(cutscene_cam)
	ENDIF

	FOR iCount = 0 TO MAX_COVER_POINTS - 1
		REMOVE_COVER_POINT(CovMissionCoverPoint[iCount])
	ENDFOR
	
	FOR iCount = 0 To (MAX_MISSION_VEH -1)
		mission_veh[iCount].iThisProgress = 0
		mission_veh[iCount].iDamage = 0
		mission_veh[iCount].iAttackPattern = 0
		IF DOES_BLIP_EXIST(mission_veh[iCount].blip)
			REMOVE_BLIP(mission_veh[iCount].blip)
		ENDIF
		IF DOES_ENTITY_EXIST(mission_veh[iCount].veh)
			IF bfinal
				SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[iCount].veh)
			ELSE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						IF IS_ENTITY_A_MISSION_ENTITY(mission_veh[iCount].veh)
							DELETE_VEHICLE(mission_veh[iCount].veh)
						ENDIF
					ELSE
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						IF IS_ENTITY_A_MISSION_ENTITY(mission_veh[iCount].veh)
							IF IS_ENTITY_A_MISSION_ENTITY(mission_veh[iCount].veh)
								DELETE_VEHICLE(mission_veh[iCount].veh)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR	
	
	FOR iCount = 0 To (MAX_ENEMY_PED -1)
		IF DOES_BLIP_EXIST(enemy[iCount].blip)
			REMOVE_BLIP(enemy[iCount].blip)
		ENDIF
		IF DOES_ENTITY_EXIST(enemy[iCount].ped)
			IF bfinal	
				IF NOT IS_PED_INJURED(enemy[iCount].ped)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(enemy[iCount].ped,40)
					SET_PED_KEEP_TASK(enemy[iCount].ped,TRUE)
				ENDIF
				enemy[iCount].actionFlag = 0
				REMOVE_COVER_POINT(enemy[iCount].Cov)
				IF IS_SCREEN_FADED_OUT()
					DELETE_PED(enemy[iCount].ped)
				ELSE
					SET_PED_AS_NO_LONGER_NEEDED(enemy[iCount].ped)
				ENDIF
			ELSE
				REMOVE_COVER_POINT(enemy[iCount].Cov)
				DELETE_PED(enemy[iCount].ped)
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iCount = 0 To (MAX_DRIVER_PED -1)
		IF DOES_BLIP_EXIST(driver[iCount].blip)
			REMOVE_BLIP(driver[iCount].blip)
		ENDIF
		driver[iCount].bTriggerTask = FALSE
		driver[iCount].actionFlag = 0
		IF DOES_ENTITY_EXIST(driver[iCount].ped)
			IF bfinal
				IF NOT IS_PED_INJURED(enemy[iCount].ped)
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(enemy[iCount].ped,40)
					SET_PED_KEEP_TASK(enemy[iCount].ped,TRUE)
				ENDIF
				REMOVE_COVER_POINT(driver[iCount].Cov)
				SET_PED_AS_NO_LONGER_NEEDED(driver[iCount].ped)
			ELSE
				REMOVE_COVER_POINT(driver[iCount].Cov)
				DELETE_PED(driver[iCount].ped)
			ENDIF
		ENDIF
	ENDFOR	
	
	FOR iCount = 0 To (MAX_BUDDY_PED -1)
		IF DOES_BLIP_EXIST(buddy[iCount].blip)
			REMOVE_BLIP(buddy[iCount].blip)
		ENDIF
		IF DOES_ENTITY_EXIST(buddy[iCount].ped)
			IF bfinal
				SET_PED_AS_NO_LONGER_NEEDED(buddy[iCount].ped)
			ELSE
				DELETE_PED(buddy[iCount].ped)
			ENDIF
		ENDIF
	ENDFOR
	
	
	FOR iCount = 0 TO MAX_NUMBER_OF_DOCK_PROPS -1
		IF DOES_ENTITY_EXIST(objcrate[iCount])
			IF bfinal
				SET_OBJECT_AS_NO_LONGER_NEEDED(objcrate[iCount])
			ELSE
				DELETE_OBJECT(objcrate[iCount])
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iCount = 0 TO 4
		IF DOES_ENTITY_EXIST(odoor[iCount])
			IF bfinal
				SET_OBJECT_AS_NO_LONGER_NEEDED(odoor[iCount])
			ELSE
				DELETE_OBJECT(odoor[iCount])
			ENDIF
		ENDIF
	ENDFOR
	
	IF DOES_ENTITY_EXIST(obox)
		IF bfinal
			SET_OBJECT_AS_NO_LONGER_NEEDED(obox)
		ELSE
			DELETE_OBJECT(obox)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		IF bfinal
			SET_PED_AS_NO_LONGER_NEEDED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		ELSE
			DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		IF bfinal
			SET_PED_AS_NO_LONGER_NEEDED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		ELSE
			DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		IF bfinal
			SET_PED_AS_NO_LONGER_NEEDED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		ELSE
			DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		ENDIF
	ENDIF
	
	//Clean up audio
	IF IS_AUDIO_SCENE_ACTIVE("BIOTECH_HEIST_UNDERWATER_SCENE")
		STOP_AUDIO_SCENE("BIOTECH_HEIST_UNDERWATER_SCENE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("BIOTECH_HEIST_GASMASK_SCENE")
		STOP_AUDIO_SCENE("BIOTECH_HEIST_GASMASK_SCENE")
	ENDIF

	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(SFDocksHUD)
	
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
	
	CLEANUP_LOADED_MODEL_ARRAY()
	
	CLEAN_OBJECTIVE_BLIP_DISPLAY()

	REMOVE_ALL_OBJECTS()
	
	IF s_rope.bropecreated
		DELETE_ROPE(s_rope.rope)
		s_rope.bropecreated = FALSE
	ENDIF
	
	CLEANUP_RAPPEL(rappelData)

	SET_WIDESCREEN_BORDERS(FALSE,0)
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	DISABLE_TAXI_HAILING(FALSE)
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_PED_POPULATION_BUDGET(3)
	
	IF bfinal
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, FALSE)
		ENDIF
		SET_ALL_VEHICLE_GENERATORS_ACTIVE()
		//enable_dispatch_services()
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		ENDIF
		IF bMissionPassed = FALSE
			KILL_ANY_CONVERSATION()
		ENDIF
		UNREGISTER_SCRIPT_WITH_AUDIO()
		TERMINATE_THIS_THREAD()
	ELSE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		ENDIF
		IF bMissionPassed = FALSE
			KILL_ANY_CONVERSATION()
		ENDIF
		iProgress = 0
		iSetupProgress = 0
	ENDIF
	

ENDPROC

//PURPOSE: controls the sucessfull completion of the mission
PROC MISSION_PASSED()

	//PRINT_GOD_TEXT("DS2_PASS")
	//RESET_HEIST_END_SCREEN(HEIST_DOCKS)
	//RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())

	  INT iCrewMem
	  REPEAT GET_HEIST_CREW_SIZE(HEIST_DOCKS) iCrewMem
	        SET_HEIST_END_SCREEN_CREW_STATUS(HEIST_DOCKS, GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_DOCKS, iCrewMem), CMST_FINE)
	  ENDREPEAT

	  SET_HEIST_END_SCREEN_POTENTIAL_TAKE(HEIST_DOCKS, 10000000)
	  SET_HEIST_END_SCREEN_ACTUAL_TAKE(HEIST_DOCKS, 10000000)
	  //ADD_HEIST_END_SCREEN_STOLEN_ITEM(HEIST_DOCKS, "WEAPONS_LBL", 10000000)
	  //SET_HEIST_END_SCREEN_MAX_ITEMS(HEIST_DOCKS, 1 ) 
	  //DISPLAY_HEIST_END_SCREEN(HEIST_DOCKS)
	  AWARD_ACHIEVEMENT_FOR_MISSION(ACH06) // Back On the Boat
	Mission_Flow_Mission_Passed()
    MISSION_CLEANUP(TRUE)
ENDPROC

PROC DISTANCE_FAIL_CHECKS(VECTOR vfailcheckcoords, FLOAT warningdist, FLOAT faildist,STRING sdistwarning,STRING scurrentobjective,MISSION_FAIL_ENUM efail =BUDDY_LEFT)
		
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),vfailcheckcoords) > faildist
		reason_for_fail = efail
		eMissionStage = MISSION_STAGE_FAIL
	ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),vfailcheckcoords) > warningdist
		IF NOT bdistancewarning
			PRINT_GOD_TEXT(sdistwarning)
			bdistancewarning = TRUE
		ENDIF
	ELIF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE),vfailcheckcoords) < (warningdist - 25)
		IF bdistancewarning
			PRINT_GOD_TEXT(scurrentobjective)
			bdistancewarning = FALSE
		ENDIF
	ENDIF

ENDPROC

INT iSubBeached
INT iSubOutOfWaterTimer

FUNC BOOL IS_VEHICLE_OUT_OF_WATER(VEHICLE_INDEX veh, INT &water_timer)

	IF DOES_ENTITY_EXIST(veh) 
		IF IS_VEHICLE_DRIVEABLE(veh)
			IF NOT IS_ENTITY_IN_WATER(veh) 
				IF MANAGE_MY_TIMER(water_timer, 10000)
					RETURN TRUE
				ENDIF
			ELSE
				water_timer = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

//PURPOSE: Tests for various fail conditions
PROC FAIL_CHECKS()
	IF eMissionStage > MISSION_STAGE_SETUP
		
		IF DOES_ENTITY_EXIST(pedWade)
			IF IS_PED_INJURED(pedWade)
				eFailedStage = eMissionStage
				reason_for_fail = WADE_DEAD
				eMissionStage = MISSION_STAGE_FAIL
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedFloyd)
			IF IS_PED_INJURED(pedFloyd)
				eFailedStage = eMissionStage
				reason_for_fail = FLOYD_DEAD
				eMissionStage = MISSION_STAGE_FAIL
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(mission_veh[MV_CHINOOK].veh)
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
				IF NOT IS_ENTITY_DEAD(mission_veh[MV_CHINOOK].veh)
					IF eMissionStage = MISSION_STAGE_DEFEND_AGAINST_MERRYWEATHER
						EXPLODE_VEHICLE(mission_veh[MV_CHINOOK].veh)
					ENDIF
				ENDIF
			
				eFailedStage = eMissionStage
				reason_for_fail = CHINOOK_UNDRIVABLE
				eMissionStage = MISSION_STAGE_FAIL
			ELSE
				IF IS_VEHICLE_PERMANENTLY_STUCK(mission_veh[MV_CHINOOK].veh)
					eFailedStage = eMissionStage
					reason_for_fail = CHINOOK_STUCK
					eMissionStage = MISSION_STAGE_FAIL
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(mission_veh[MV_SUBMERISIBLE].veh)
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				PRINTSTRING("THIS FAIL = SUB UNDRIVABLE")PRINTNL()
				eFailedStage = eMissionStage
				reason_for_fail = SUBMERSIBLE_UNDRIVABLE
				eMissionStage = MISSION_STAGE_FAIL
			ELSE
				IF eMissionStage = MISSION_STAGE_FIND_THE_CRATE
					IF NOT HAS_LABEL_BEEN_TRIGGERED("SUB OUT OF WATER")
						IF NOT IS_ENTITY_IN_WATER(mission_veh[MV_SUBMERISIBLE].veh)
							iSubOutOfWaterTimer = GET_GAME_TIMER()
							SET_LABEL_AS_TRIGGERED("SUB OUT OF WATER",TRUE)
						ENDIF
					ELSE
						IF IS_ENTITY_IN_WATER(mission_veh[MV_SUBMERISIBLE].veh)
							SET_LABEL_AS_TRIGGERED("SUB OUT OF WATER",FALSE)
						ENDIF
						IF IS_VEHICLE_OUT_OF_WATER(mission_veh[MV_SUBMERISIBLE].veh,iSubOutOfWaterTimer)
							PRINTSTRING("THIS FAIL = SUB OUT OF WATER")PRINTNL()
							eFailedStage = eMissionStage
							reason_for_fail = SUBMERSIBLE_UNDRIVABLE
							eMissionStage = MISSION_STAGE_FAIL
						ENDIF
					ENDIF
				ENDIF
				IF eMissionStage = MISSION_STAGE_DROP_THE_SUB
					//IF iprogress = 2
						//IF NOT HAS_LABEL_BEEN_TRIGGERED("SUB IN WATER")
					IF iprogress = 1	
						IF NOT IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
							IF MANAGE_MY_TIMER(iSubBeached,8000)
								IF DOES_ENTITY_EXIST(objMission[MO_RIG1])
								OR DOES_ENTITY_EXIST(objMission[MO_RIG2])
									IF IS_ENTITY_TOUCHING_ENTITY(mission_veh[MV_SUBMERISIBLE].veh,objMission[MO_RIG1])
									OR IS_ENTITY_TOUCHING_ENTITY(mission_veh[MV_SUBMERISIBLE].veh,objMission[MO_RIG2])
										IF NOT IS_ENTITY_IN_WATER(mission_veh[MV_SUBMERISIBLE].veh)
											eFailedStage = eMissionStage
											reason_for_fail = SUBMERSIBLE_UNDRIVABLE
											eMissionStage = MISSION_STAGE_FAIL
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF eMissionStage = MISSION_STAGE_FIND_THE_CRATE
				OR eMissionStage = MISSION_STAGE_PICK_UP_SUB
					IF IS_VEHICLE_PERMANENTLY_STUCK(mission_veh[MV_SUBMERISIBLE].veh)
						PRINTSTRING("THIS FAIL = SUB STUCK")PRINTNL()
						eFailedStage = eMissionStage
						reason_for_fail = SUBMERSIBLE_UNDRIVABLE
						eMissionStage = MISSION_STAGE_FAIL
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		
		//LEAVING BUDDIES BEHIND
		IF eMissionStage = MISSION_STAGE_GET_TO_AIRSTRIP
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]))> 1200
							eFailedStage = eMissionStage
							reason_for_fail = FRANKLIN_LEFT
							eMissionStage = MISSION_STAGE_FAIL
						ENDIF
					ELSE
						eFailedStage = eMissionStage
						reason_for_fail = FRANKLIN_DEAD
						eMissionStage = MISSION_STAGE_FAIL
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			IF IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				eFailedStage = eMissionStage
				reason_for_fail = FRANKLIN_DEAD
				eMissionStage = MISSION_STAGE_FAIL
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			IF IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				eFailedStage = eMissionStage
				reason_for_fail = TREVOR_DEAD
				eMissionStage = MISSION_STAGE_FAIL
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			IF IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				eFailedStage = eMissionStage
				reason_for_fail = TREVOR_DEAD
				eMissionStage = MISSION_STAGE_FAIL
			ENDIF
		ENDIF
		
		//LEAVING BUDDIES BEHIND
		IF eMissionStage = MISSION_STAGE_FIND_THE_CRATE
		OR eMissionStage = MISSION_STAGE_GET_TO_SURFACE
		OR eMissionStage = MISSION_STAGE_DROP_THE_SUB
		OR eMissionStage = MISSION_STAGE_PICK_UP_SUB
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]))> 1200
						eFailedStage = eMissionStage
						reason_for_fail = MICHAEL_LEFT
						eMissionStage = MISSION_STAGE_FAIL
					ENDIF
				ELSE	
					eFailedStage = eMissionStage
					reason_for_fail = MICHAEL_DEAD
					eMissionStage = MISSION_STAGE_FAIL
				ENDIF
			ENDIF
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]))> 1000
						eFailedStage = eMissionStage
						reason_for_fail = TREVOR_LEFT
						eMissionStage = MISSION_STAGE_FAIL
					ENDIF
				ELSE
					eFailedStage = eMissionStage
					reason_for_fail = TREVOR_DEAD
					eMissionStage = MISSION_STAGE_FAIL
				ENDIF
			ENDIF
		ENDIF
		
		//FAIL DISTANCES
		IF eMissionStage = MISSION_STAGE_FIND_THE_CRATE
		OR eMissionStage = MISSION_STAGE_GET_TO_SURFACE
		OR eMissionStage = MISSION_STAGE_DROP_THE_SUB
			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]))> 1200
						eFailedStage = eMissionStage
						reason_for_fail = FRANKLIN_LEFT
						eMissionStage = MISSION_STAGE_FAIL
					ENDIF
				ELSE
					eFailedStage = eMissionStage
					reason_for_fail = FRANKLIN_DEAD
					eMissionStage = MISSION_STAGE_FAIL
				ENDIF
			ENDIF
		ELSE
			IF eMissionStage = MISSION_STAGE_PICK_UP_SUB
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
					PRINTSTRING("GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR")PRINTNL()
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]))> 1200
								eFailedStage = eMissionStage
								reason_for_fail = FRANKLIN_LEFT
								eMissionStage = MISSION_STAGE_FAIL
							ENDIF
						ELSE
							eFailedStage = eMissionStage
							reason_for_fail = FRANKLIN_DEAD
							eMissionStage = MISSION_STAGE_FAIL
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					PRINTSTRING("GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL")PRINTNL()
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]))> 300
								eFailedStage = eMissionStage
								reason_for_fail = FRANKLIN_LEFT
								eMissionStage = MISSION_STAGE_FAIL
							ENDIF
						ELSE
							eFailedStage = eMissionStage
							reason_for_fail = FRANKLIN_DEAD
							eMissionStage = MISSION_STAGE_FAIL
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF eMissionStage = MISSION_STAGE_FLY_AWAY
		OR eMissionStage = MISSION_STAGE_FLY_TO_SEA
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh)
						IF IS_ENTITY_IN_AIR(mission_veh[MV_CHINOOK].veh)
							eFailedStage = eMissionStage
							reason_for_fail = CARGOBOB_ABADONED
							eMissionStage = MISSION_STAGE_FAIL
						ELSE
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(mission_veh[MV_CHINOOK].veh))> 25
								eFailedStage = eMissionStage
								reason_for_fail = CARGOBOB_ABADONED
								eMissionStage = MISSION_STAGE_FAIL
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		IF eMissionStage = MISSION_STAGE_DROP_THE_SUB
		OR eMissionStage = MISSION_STAGE_DROP_THE_SUB_AT_THE_AIRSTRIP
		OR eMissionStage = MISSION_STAGE_PICK_UP_SUB
		OR eMissionStage = MISSION_STAGE_GET_TO_SURFACE
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]))> 1000
							eFailedStage = eMissionStage
							reason_for_fail = TREVOR_LEFT
							eMissionStage = MISSION_STAGE_FAIL
						ENDIF
					ELSE
						eFailedStage = eMissionStage
						reason_for_fail = TREVOR_DEAD
						eMissionStage = MISSION_STAGE_FAIL
					ENDIF
				ENDIF
			ENDIF
		ELSE
			//TEMP REMOVED
			IF eMissionStage != MISSION_STAGE_FLY_AWAY
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]))> 250.0
								eFailedStage = eMissionStage
								reason_for_fail = TREVOR_LEFT
								eMissionStage = MISSION_STAGE_FAIL
							ENDIF
						ELSE
							eFailedStage = eMissionStage
							reason_for_fail = TREVOR_DEAD
							eMissionStage = MISSION_STAGE_FAIL
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		IF eMissionStage = MISSION_STAGE_DEFEND_AGAINST_MERRYWEATHER
			IF DOES_ENTITY_EXIST(mission_veh[MV_CHINOOK].veh)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),<<1754.4716, 3283.0715, 40.0925>>) < 200
						eFailedStage = eMissionStage
						reason_for_fail = DIDNT_LOSE_MERRYWEATHER
						eMissionStage = MISSION_STAGE_FAIL
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
			
ENDPROC
//PURPOSE: controls the failure of the mission
PROC MISSION_FAILED()

	STRING sFailString
	
	SWITCH reason_for_fail
	
		CASE GENERIC
			//FAIL_GENERIC
			CLEAR_PRINTS()
			sFailString = "DS2_FAILED"
			//PRINT_GOD_TEXT("DS2_FAIL")
		BREAK
		
		CASE TREVOR_DEAD
			//Killed buddy
			CLEAR_PRINTS()
			sFailString = "DS2_TREVDEAD"
			//PRINT_GOD_TEXT("DS2_FAIL1")
		BREAK	
		
		CASE TREVOR_LEFT
			//Killed buddy
			CLEAR_PRINTS()
			sFailString = "DS2_TREVLEFT"
			//PRINT_GOD_TEXT("DS2_FAIL1")
		BREAK	
		
		CASE MICHAEL_DEAD
			//Killed buddy
			CLEAR_PRINTS()
			sFailString = "DS2_MICHDEAD"
			//PRINT_GOD_TEXT("DS2_FAIL1")
		BREAK
		
		CASE MICHAEL_LEFT
			//Killed buddy
			CLEAR_PRINTS()
			sFailString = "DS2_MICHLEFT"
			//PRINT_GOD_TEXT("DS2_FAIL1")
		BREAK	
		
		CASE FRANKLIN_DEAD
			//Killed buddy
			CLEAR_PRINTS()
			sFailString = "DS2_FRANDEAD"
			//PRINT_GOD_TEXT("DS2_FAIL1")
		BREAK	
		
		CASE FRANKLIN_LEFT
			//Killed buddy
			CLEAR_PRINTS()
			sFailString = "DS2_FRANLEFT"
			//PRINT_GOD_TEXT("DS2_FAIL1")
		BREAK	
		
		CASE FLOYD_DEAD
			//Killed buddy
			CLEAR_PRINTS()
			sFailString = "DS2_FLOYDDEAD"
			//PRINT_GOD_TEXT("DS2_FAIL1")
		BREAK
		
		CASE WADE_DEAD
			//Killed buddy
			CLEAR_PRINTS()
			sFailString = "DS2_WADEDEAD"
			//PRINT_GOD_TEXT("DS2_FAIL1")
		BREAK	
		
		CASE BUDDY_LEFT
			//left buddy
			CLEAR_PRINTS()
			sFailString = "DS2_FABANCREW"
			//PRINT_GOD_TEXT("DS2_FAIL4")
		BREAK	
		
		CASE CHINOOK_UNDRIVABLE
			//vehicle undrivable
			CLEAR_PRINTS()
			sFailString = "DS2_HELIDEAD"
			//PRINT_GOD_TEXT("DS2_FAIL2")
		BREAK
		
		CASE CHINOOK_STUCK
			//vehicle undrivable
			CLEAR_PRINTS()
			sFailString = "DS2_HELISTUCK"
			//PRINT_GOD_TEXT("DS2_FAIL2")
		BREAK
		
		CASE SUBMERSIBLE_UNDRIVABLE
			//vehicle undrivable
			CLEAR_PRINTS()
			sFailString = "DS2_SUBDEAD"
			//PRINT_GOD_TEXT("DS2_FAIL2")
		BREAK
		
		CASE MICHAELS_CAR_UNDRIVABLE
			//vehicle undrivable
			CLEAR_PRINTS()
			sFailString = "DS2_MCARDEAD"
			//PRINT_GOD_TEXT("DS2_FAIL2")
		BREAK
		
		CASE CARGOBOB_ABADONED
			//vehicle undrivable
			CLEAR_PRINTS()
			sFailString = "DS2_CARGOAB"
		BREAK
		
		CASE DISCOVERED
			//vehicle undrivable
			CLEAR_PRINTS()
			sFailString = "DS2_BANKALERT"
			//PRINT_GOD_TEXT("DS2_FAIL3")
		BREAK
		
		CASE DIDNT_LOSE_MERRYWEATHER
			//failed to lose merry weather
			CLEAR_PRINTS()
			sFailString = "DS2_MERRYWF"
		BREAK

	ENDSWITCH
	
	TRIGGER_MUSIC_EVENT("DH2B_FAIL")
//	MISSION_STAGE_GET_TO_AIRSTRIP
	
	IF eFailedStage = MISSION_STAGE_FIND_THE_CRATE
	OR eFailedStage = MISSION_STAGE_GET_TO_SURFACE
	OR eFailedStage = MISSION_STAGE_DROP_THE_SUB
	OR eFailedStage = MISSION_STAGE_PICK_UP_SUB
	OR eFailedStage = MISSION_STAGE_FLY_TO_SEA
	OR eFailedStage = MISSION_STAGE_DEFEND_AGAINST_MERRYWEATHER
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-1577.8319, 5175.6240, 18.5209>>, 130.8145)
		SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<-1574.9478, 5169.6572, 18.5775>>, 163.3193)
	ENDIF
	
	IF eFailedStage = MISSION_STAGE_FLY_AWAY
	OR eFailedStage = MISSION_STAGE_DROP_THE_SUB_AT_THE_AIRSTRIP
	OR eFailedStage = MISSION_STAGE_PICK_UP_THE_SUB_FROM_AIRSTRIP
	OR eFailedStage = MISSION_STAGE_CLOSING_CUTSCENE
		MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<1745.9387, 3292.6257, 40.1041>>, 182.7365)
		SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<1756.6458, 3283.6611, 40.1061>>, 274.9329)
	ENDIF
	
	VEHICLE_INDEX vehPlayer
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(vehPlayer)
		IF GET_ENTITY_MODEL(vehPlayer) = ANNIHILATOR
		OR GET_ENTITY_MODEL(vehPlayer) = CARGOBOB3
		OR GET_ENTITY_MODEL(vehPlayer) = SUBMERSIBLE
			//RemovePlayerFromRestrictedVehicle(vehPlayer)	
		ENDIF
	ENDIF

	MISSION_FLOW_MISSION_FAILED_WITH_REASON(sFailString)
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_DOCKSP2B_CHINOOK, TRUE)
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		WAIT(0)
	ENDWHILE

	// Call RemovePlayerFromRestrictedVehicle() here if you need to
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	
	
	MISSION_CLEANUP(TRUE) // must only take 1 frame and terminate the thread
	
ENDPROC

//PURPOSE: returns true if the number of specified enemies drop below the threshold.
FUNC INT NUMBER_OF_ENEMIES_ALIVE(INT first = 0, INT last = MAX_DRIVER_PED)
	INT ienemycount = 0
	IF last >= MAX_DRIVER_PED
		last = (MAX_DRIVER_PED -1)
	ENDIF
	FOR iCount = first TO last
		IF DOES_ENTITY_EXIST(driver[iCount].ped)
			IF NOT IS_PED_INJURED(driver[iCount].ped)
				ienemycount++
			ENDIF
		ENDIF
	ENDFOR
	RETURN ienemycount
ENDFUNC

PROC SET_VEHICLE_NOISE_BASED_ON_FLIGHT_STAT(VEHICLE_INDEX veh, INT iFlyingStat)
	
	IF IS_VEHICLE_DRIVEABLE(veh)
		IF iFlyingStat >= 90
			SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(veh,0.75)
		ELIF iFlyingStat >= 80
			SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(veh,0.80)
		ELIF iFlyingStat >= 70
			SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(veh,0.85)
		ELIF iFlyingStat >= 60
			SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(veh,0.90)
		ELIF iFlyingStat >= 50
			SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(veh,0.95)
		ELSE
			SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(veh,1)
		ENDIF
	ENDIF

ENDPROC

PROC SET_PLAYER_START_POSITION(MISSION_STAGE_ENUM eMissionStagePassed)
	IF eMissionStagePassed = MISSION_STAGE_SETUP
		vStageStart = << 910.1729, 3335.7280, 270.3783 >>
		fstartheading = 39.5182
	ELIF eMissionStagePassed = MISSION_STAGE_FLY_TO_SEA
		vStageStart = <<2129.4431, 4799.7305, 40.1567>>
		fstartheading =  39.5182
//	ELIF eMissionStagePassed = MISSION_STAGE_CLEAR_THE_AREA_OF_MERRY_WEATHER
//		vStageStart = <<-2842.8845, 3567.4690, 2.0187>>
//		fstartheading = 39.5182
	ELIF eMissionStagePassed = MISSION_STAGE_DROP_THE_SUB
		vStageStart = <<-2842.8845, 3567.4690, 2.0187>>
		fstartheading = 39.5182
	ELIF eMissionStagePassed = MISSION_STAGE_FIND_THE_CRATE
		vStageStart = <<-853.2222, 6407.8569, -20.8874>>
		fstartheading = 108.3685
	ELIF eMissionStagePassed = MISSION_STAGE_DEFEND_AGAINST_MERRYWEATHER
		vStageStart = <<526.2101, -3160.0996,  -8.2578 >> 
		fstartheading = 108.3685
	ELIF eMissionStagePassed = MISSION_STAGE_FLY_AWAY
		vStageStart = <<526.2101, -3160.0996,  -8.2578 >> 
		fstartheading = 108.3685
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vStageStart)
		REQUEST_COLLISION_AT_COORD(vStageStart)
		WARP_PLAYER(vStageStart, fstartheading)
	ENDIF
ENDPROC


//Handles the audio scene and effects while the player is underwater
PROC MANAGE_UNDERWATER_AUDIO()
	
	VECTOR v_game_cam_pos = GET_GAMEPLAY_CAM_COORD()
	
	IF IS_PED_SWIMMING_UNDER_WATER(player_ped_id())//or v_game_cam_pos.z < -1.0 //0.0 -0.5
	OR (v_game_cam_pos.y < 3726.0 AND v_game_cam_pos.y > 3709.0 AND v_game_cam_pos.x > 3509.0 AND v_game_cam_pos.x < 3541.0 AND v_game_cam_pos.z < 17.5)
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("BIOTECH_HEIST_UNDERWATER_SCENE")
			START_AUDIO_SCENE("BIOTECH_HEIST_UNDERWATER_SCENE")
		ENDIF
		
		IF HAS_SOUND_FINISHED(sound_underwater_ambience)
			PLAY_SOUND_FRONTEND(sound_underwater_ambience, "UW_Ambience")
		ENDIF
		
		IF HAS_SOUND_FINISHED(sound_underwater_rebreather)
			PLAY_SOUND_FRONTEND(sound_underwater_rebreather, "UW_Rebreather")// "FBI_05_RAID_FAN")//"UW_Rebreather")
		ENDIF
		
		//These sounds shouldn't play once the player has taken off their scuba suit
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SCUBA_WATER)
				FLOAT f_player_speed = GET_ENTITY_SPEED(PLAYER_PED_ID())
			
				IF f_player_speed > 1.0
					IF HAS_SOUND_FINISHED(i_sound_swishing)
						PLAY_SOUND_FROM_ENTITY(i_sound_swishing, "FBI_05_RAID_FOOT_SWISH", PLAYER_PED_ID())
						SET_VARIABLE_ON_SOUND(i_sound_swishing, "SwimSpeed", (f_player_speed - 1.0) / 4.0)
					ELSE
						SET_VARIABLE_ON_SOUND(i_sound_swishing, "SwimSpeed", (f_player_speed - 1.0) / 4.0)
					ENDIF
				ELSE 
					IF NOT HAS_SOUND_FINISHED(i_sound_swishing)
						STOP_SOUND(i_sound_swishing)
					ENDIF
				ENDIF
			
				IF b_allow_scuba_breath //The are some special cases where scuba breathing shouldn't play
					IF HAS_SOUND_FINISHED(i_sound_scuba_gear)
						PLAY_SOUND_FRONTEND(i_sound_scuba_gear, "FBI_05_RAID_BREATH")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_SCUBA_WATER)
				FLOAT f_player_speed = GET_ENTITY_SPEED(PLAYER_PED_ID())
			
				IF f_player_speed > 1.0
					IF HAS_SOUND_FINISHED(i_sound_swishing)
						PLAY_SOUND_FROM_ENTITY(i_sound_swishing, "FBI_05_RAID_FOOT_SWISH", PLAYER_PED_ID())
						SET_VARIABLE_ON_SOUND(i_sound_swishing, "SwimSpeed", (f_player_speed - 1.0) / 4.0)
					ELSE
						SET_VARIABLE_ON_SOUND(i_sound_swishing, "SwimSpeed", (f_player_speed - 1.0) / 4.0)
					ENDIF
				ELSE 
					IF NOT HAS_SOUND_FINISHED(i_sound_swishing)
						STOP_SOUND(i_sound_swishing)
					ENDIF
				ENDIF
			
				IF b_allow_scuba_breath //The are some special cases where scuba breathing shouldn't play
					IF HAS_SOUND_FINISHED(i_sound_scuba_gear)
						PLAY_SOUND_FRONTEND(i_sound_scuba_gear, "FBI_05_RAID_BREATH")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_SCUBA_WATER)
				FLOAT f_player_speed = GET_ENTITY_SPEED(PLAYER_PED_ID())
			
				IF f_player_speed > 1.0
					IF HAS_SOUND_FINISHED(i_sound_swishing)
						PLAY_SOUND_FROM_ENTITY(i_sound_swishing, "FBI_05_RAID_FOOT_SWISH", PLAYER_PED_ID())
						SET_VARIABLE_ON_SOUND(i_sound_swishing, "SwimSpeed", (f_player_speed - 1.0) / 4.0)
					ELSE
						SET_VARIABLE_ON_SOUND(i_sound_swishing, "SwimSpeed", (f_player_speed - 1.0) / 4.0)
					ENDIF
				ELSE 
					IF NOT HAS_SOUND_FINISHED(i_sound_swishing)
						STOP_SOUND(i_sound_swishing)
					ENDIF
				ENDIF
			
				IF b_allow_scuba_breath //The are some special cases where scuba breathing shouldn't play
					IF HAS_SOUND_FINISHED(i_sound_scuba_gear)
						PLAY_SOUND_FRONTEND(i_sound_scuba_gear, "FBI_05_RAID_BREATH")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELSE	
		
		IF IS_AUDIO_SCENE_ACTIVE("BIOTECH_HEIST_UNDERWATER_SCENE")
			STOP_AUDIO_SCENE("BIOTECH_HEIST_UNDERWATER_SCENE")
		ENDIF
		
		IF sound_underwater_ambience != 0
			IF NOT HAS_SOUND_FINISHED(sound_underwater_ambience)
				STOP_SOUND(sound_underwater_ambience)
			ENDIF
		ENDIF
		
		IF sound_underwater_rebreather != 0
			IF NOT HAS_SOUND_FINISHED(sound_underwater_rebreather)
				STOP_SOUND(sound_underwater_rebreather)
			ENDIF
		ENDIF
		
		IF i_sound_swishing != 0
			IF NOT HAS_SOUND_FINISHED(i_sound_swishing)
				STOP_SOUND(i_sound_swishing)
			ENDIF
		ENDIF
		
		IF i_sound_scuba_gear != 0
			IF NOT HAS_SOUND_FINISHED(i_sound_scuba_gear)
				STOP_SOUND(i_sound_scuba_gear)
			ENDIF
		ENDIF
		
	ENDIF
ENDPROC

PROC CREATE_DOCK_DOORS()
	IF NOT DOES_ENTITY_EXIST(odoor[0])
		//odoor[0]=CREATE_OBJECT(Prop_SluiceGateL,vdockdoorcoords[0])
		SET_ENTITY_ROTATION(odoor[0],vdockdoorrot[0])
		FREEZE_ENTITY_POSITION(odoor[0],TRUE)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(odoor[1])
		//odoor[1]=CREATE_OBJECT(Prop_SluiceGateR,vdockdoorcoords[1])
		SET_ENTITY_ROTATION(odoor[1],vdockdoorrot[1])
		FREEZE_ENTITY_POSITION(odoor[1],TRUE)
	ENDIF
	IF NOT DOES_ENTITY_EXIST(odoor[3])
		//odoor[3]=CREATE_OBJECT(Prop_SluiceGate,vdockdoorcoords[3])
		SET_ENTITY_ROTATION(odoor[3],vdockdoorrot[3])
		FREEZE_ENTITY_POSITION(odoor[3],TRUE)
	ENDIF

ENDPROC

PROC OPEN_DOCK_DOORS()

	IF DOES_ENTITY_EXIST(odoor[0])
		IF vdockdoorrot[0].z < 90
			vdockdoorrot[0].z = vdockdoorrot[0].z + 0.5
		ELSE
			bDocksThingDone[2] = TRUE
		ENDIF
		SET_ENTITY_ROTATION(odoor[0],vdockdoorrot[0])
	ENDIF
	IF DOES_ENTITY_EXIST(odoor[1])
		IF vdockdoorrot[1].z > -90
			vdockdoorrot[1].z = vdockdoorrot[1].z - 0.5
		ENDIF
		SET_ENTITY_ROTATION(odoor[1],vdockdoorrot[1])
	ENDIF

ENDPROC
PROC HANDLE_DOOR_LOCKS()
	IF NOT bgotkey
		IF NOT bdoorlocked[0]
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< 586.1, -3117.3, 18.9>>,5,PROP_RON_DOOR_01)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_RON_DOOR_01,<< 586.1, -3117.3, 18.9>>,TRUE,0)
				bdoorlocked[0] = TRUE
			ELSE
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_RON_DOOR_01,<< 586.1, -3117.3, 18.9>>,TRUE,0)
				bdoorlocked[0] = TRUE
			ENDIF
		ENDIF
		IF NOT bdoorlocked[1]
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< 552.1, -3117.3, 18.9>>,5,PROP_RON_DOOR_01)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_RON_DOOR_01,<< 552.1, -3117.3, 18.9>>,TRUE,0)
				bdoorlocked[1] = TRUE
			ELSE
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_RON_DOOR_01,<< 552.1, -3117.3, 18.9>>,TRUE,0)
				bdoorlocked[1] = TRUE
			ENDIF
		ENDIF
	ELSE
		IF NOT bdoorlocked[2]		
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< 552.1, -3117.3, 18.9>>,5,PROP_RON_DOOR_01)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_RON_DOOR_01,<< 552.1, -3117.3, 18.9>>,FALSE,0)
				bdoorlocked[2] = TRUE
			ELSE
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_RON_DOOR_01,<< 552.1, -3117.3, 18.9>>,FALSE,0)
				bdoorlocked[2] = TRUE
			ENDIF
		ENDIF
		
		IF NOT bdoorlocked[3]
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(<< 586.1, -3117.3, 18.9>>,5,PROP_RON_DOOR_01)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(PROP_RON_DOOR_01,<< 586.1, -3117.3, 18.9>>,FALSE,0)
				bdoorlocked[3] = FALSE
			ELSE
				SET_LOCKED_UNSTREAMED_IN_DOOR_OF_TYPE(PROP_RON_DOOR_01,<< 586.1, -3117.3, 18.9>>,FALSE,0)
				bdoorlocked[2] = TRUE
			ENDIF
		ENDIF
		IF bSwiped = TRUE
		ENDIF
		
	ENDIF
ENDPROC

/// PURPOSE:
///    Generic function for handling a ped rappelling down a static rope.
/// PARAMS:
///    enemy - 
///    v_rope_pos - 
/// RETURNS:
///    
FUNC BOOL MANAGE_RAPPELLING()

	SWITCH iRappelprog

		CASE 0  //setup

			REQUEST_RAPPEL_ASSETS(TRUE)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			CONTROL_FADE_IN(500)
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			PRINT_HELP("DS2_RAPELH1")
			iRappelprog++
		
		BREAK

		CASE 1
		
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_JUMP) OR bForceRappel = TRUE
				SET_ENTITY_HEADING(PLAYER_PED_ID(),266.6435)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
				INIT_RAPPEL_WITH_ROPE(rappelDataFrank,NULL,vFrankRappel,<<0,0,0>>,-1.0,-35,35,TRUE,TRUE,TRUE,NULL,TRUE,TRUE,1.0)
				
				iRappelprog++
			ENDIF
			
		BREAK
		
		CASE 2
		
			IF GET_ENTITY_HEIGHT_ABOVE_GROUND(PLAYER_PED_ID()) < 3
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					DETACH_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					FREEZE_ENTITY_POSITION(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],FALSE)
				ENDIF
				DETACH_ENTITY(PLAYER_PED_ID())
				//IF NOT IS_ENTITY_ATTACHED(PLAYER_PED_ID())
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
				//ENDIF
				CANCEL_RAPPEL(rappelDataFrank)
				RETURN TRUE
			ELSE
				HANDLE_RAPPEL(rappelDataFrank)
			ENDIF
			
		BREAK

	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC


FUNC BOOL SETUP_CUSTOM_HOTSWAP_CAM(CUSTOM_HOTSWAP eCustom)

	PRINTSTRING("IN SETUP CAMS FUNC")PRINTNL()

	PED_INDEX targetPed
	
	IF eCustom = CH_GENERAL
		sCamDetails.bSplineCreated = FALSE
	ENDIF
	
	IF eCustom = CH_FROM_OFFICE_TO_TREVOR
		targetPed = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
	ENDIF
	
	IF eCustom = CH_FROM_TREVOR_IN_SUB_TO_FRANK
		targetPed = sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
	ENDIF
	
	IF eCustom = CH_FROM_TREVOR_IN_SUB_TO_MIKE
		targetPed = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
	ENDIF
	
	IF eCustom = CH_BACK_TO_TREV_IN_SUBMARINE
		targetPed = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
	ENDIF

	IF NOT DOES_CAM_EXIST(sCamDetails.camID)
		//sCamDetails.camID = CREATE_CAM("SMOOTHED_SPLINE_CAMERA", FALSE) 
		//CREATE_CAMERA(CAMTYPE_SPLINE_DEFAULT)
		sCamDetails.camID = CREATE_CAM("DEFAULT_SPLINE_CAMERA", FALSE) 
		//sCamDetails.camID = CREATE_CAM("ROUNDED_SPLINE_CAMERA", FALSE) 
	ENDIF
	
	IF DOES_CAM_EXIST(sCamDetails.camID)
		// Initial camera behind the player       
		//ADD_CAM_SPLINE_NODE(sCamDetails.camID, GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), 0)

		IF eCustom = CH_FROM_OFFICE_TO_TREVOR
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",GET_CAM_COORD(cutscene_cam), GET_CAM_ROT(cutscene_cam),  GET_CAM_FOV(cutscene_cam), FALSE), 0)
		ELSE
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), GET_GAMEPLAY_CAM_FOV(), FALSE), 0)
		ENDIF
		
		IF eCustom = CH_SHOOTOUT_LONG_SWITCH
			VECTOR vTemp, vCamRot
			IF NOT IS_PED_INJURED(targetPed)
				vTemp = NORMALISE_VECTOR(GET_ENTITY_COORDS(targetPed) - GET_GAMEPLAY_CAM_COORD())
			ENDIF
			vCamRot = GET_GAMEPLAY_CAM_ROT()
			FLOAT fTemp
			fTemp = vCamRot.z
			ADD_CAM_SPLINE_NODE(sCamDetails.camID, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(GET_GAMEPLAY_CAM_COORD(), fTemp, <<0,25,50>>), <<0,0,GET_ANGLE_BETWEEN_2D_VECTORS(0,1,vTemp.x,vTemp.y)>>, 3000)
		ENDIF
		
		IF eCustom = CH_FROM_OFFICE_TO_TREVOR
			//SET_CAM_SPLINE_DURATION(sCamDetails.camID,12000)
			//ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<568.4879, -3124.1775, 19.4457>>, <<-8.2791, -0.0000, -89.3576>>, 50.0000, FALSE), 2000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << 569.3671, -3124.1997, 19.2895 >>, << -11.4204, 0.0739, -92.0668 >>, 50.0000, FALSE), 1000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<568.8188, -3153.6736, 19.4457>>, <<-8.2791, -0.0000, -89.3576>>, 50.0000, FALSE), 2800, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<565.9450, -3177.5820, 28.1409>>, <<-8.2791, -0.0000, -35.4668>>, 50.0000, FALSE), 2000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<551.5543, -3178.2073, 36.0228>>, <<-3.5962, -0.0000, -4.8804>>, 50.0000, FALSE), 2000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << 550.2770, -3178.1160, 43.7703 >>, << -3.5962, 0.0000, -4.8804 >>, 50.0000, FALSE), 1800, CAM_SPLINE_NODE_SMOOTH_ROT)
			//ADD_CAM_SPLINE_NODE(sCamDetails.camID,  GET_OFFSET_FOR_DEST_CAM(targetPed), << -0.951428, 0.0, GET_ENTITY_HEADING(targetPed) >>, 3000)
		ENDIF
		
		IF eCustom = CH_FROM_TREVOR_IN_SUB_TO_FRANK
			//SET_CAM_SPLINE_DURATION(sCamDetails.camID,12000)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<526.7081, -3207.3159, 8.7663>>, <<-5.4989, -0.0000, 1.3897>>, 50.0000, TRUE), 2000,CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<507.6325, -3204.3640, 8.5357>>, <<-2.9543, -0.0000, 1.3897>>, 50.0000, TRUE), 2000,CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << 486.9892, -3207.8547, 8.3833 >>, << -8.8854, 0.0000, 2.0614 >>, 50.0000, TRUE), 2000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << 486.8662, -3204.4365, 41.3399 >>, << -8.8854, 0.0000, 2.0614 >>, 50.0000, TRUE), 2000,CAM_SPLINE_NODE_SMOOTH_ROT)
		ENDIF
		
		IF eCustom = CH_FROM_TREVOR_IN_SUB_TO_MIKE
			//SET_CAM_SPLINE_DURATION(sCamDetails.camID,12000)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA",GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), GET_GAMEPLAY_CAM_FOV(), FALSE), 0)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<527.5471, -3216.0691, 10.6868>>, <<-4.4891, -0.0000, -0.0216>>, 35.0000, TRUE), 1800, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<554.0043, -3216.2556, 8.4194>>, <<-4.4891, -0.0000, -0.0216>>, 35.0000, TRUE), 2000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<550.6530, -3192.7900, 7.5259>>, <<-0.3850, 0.0000, 19.5963>>, 35.0000, TRUE), 2000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<550.7260, -3149.2800, 7.6674>>, <<-2.1087, -0.0000, -4.5362>>, 35.0000, TRUE), 2000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<563.6189, -3134.0405, 7.5076>>, <<-0.4671, -0.0000, -2.4841>>, 35.0000, TRUE), 2000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << 561.4431, -3121.3843, 6.9109 >>, << -6.3582, -0.0000, 93.2605 >>, 45.0000, TRUE), 2000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_GAMEPLAY_FRAME(sCamDetails.camID,1000,CAM_SPLINE_NODE_SMOOTH_ROT)
			
			//ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<562.1273, -3122.8887, 7.1090>>, <<-5.3100, 0.0000, 90.9267>>, 35.0000, TRUE), 1000, CAM_SPLINE_NODE_SMOOTH_ROT)
		ENDIF
		
		IF eCustom = CH_BACK_TO_TREV_IN_SUBMARINE
			VECTOR vTemp = GET_GAMEPLAY_CAM_COORD()
			vTemp.Z = vTemp.Z + 5
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", vTemp,GET_GAMEPLAY_CAM_ROT(), 50, FALSE), 2000,CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << 525.9992, -3131.8538, 16.9511 >>, << -3.8621, 0.0000, -179.2871 >>, 50.0000, TRUE), 2000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", << 525.9899, -3131.1118, 5.9720 >>, << -3.8621, 0.0000, -179.2871 >>, 50.0000, TRUE), 2000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_GAMEPLAY_FRAME(sCamDetails.camID,1000,CAM_SPLINE_NODE_SMOOTH_ROT)
		ENDIF

		sCamDetails.bSplineCreated = TRUE
		RETURN TRUE
	
	ENDIF

	
	RETURN FALSE

ENDFUNC

PROC MANAGE_FRANKLIN_DRIVEBY()

	CLEAR_PED_TASKS(PLAYER_PED_ID())
    SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), TRUE)
    SET_PLAYER_FORCED_AIM(PLAYER_ID(), TRUE)
    
    FLOAT fSetCamHeading
    FLOAT fSetCamPitch
    
    IF IS_VEHICLE_DRIVEABLE(mission_veh[iLeadHeli].veh)
          VECTOR vTargetVector 
          FLOAT fTargetHeading
          FLOAT fTargetPitch
          vTargetVector = GET_ENTITY_COORDS(mission_veh[iLeadHeli].veh) - GET_ENTITY_COORDS(PLAYER_PED_ID())
          
          fTargetHeading = GET_HEADING_FROM_VECTOR_2D(vTargetVector.x, vTargetVector.y) - GET_ENTITY_HEADING(PLAYER_PED_ID())
          IF fTargetHeading > 360.0
                fTargetHeading -= 360.0
          ELIF fTargetHeading < 0.0
                fTargetHeading += 360.0
          ENDIF
          IF fTargetHeading > 180
                IF fTargetHeading > 270
                      fTargetHeading = 0
                ELSE
                      fTargetHeading = 180
                ENDIF
          ENDIF
          
          VECTOR vOffset 
          FLOAT fHorizontalDist 
          FLOAT fVerticalDist
          vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(mission_veh[iLeadHeli].veh))
          fHorizontalDist = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), mission_veh[iLeadHeli].veh, FALSE)
          fVerticalDist = vOffset.z
          
          fTargetPitch = ATAN(fVerticalDist/fHorizontalDist)
          
    
          fSetCamHeading = fTargetHeading
          fSetCamPitch = fTargetPitch
    ELSE
          fSetCamHeading = 90.0
          fSetCamPitch = 0.0
    ENDIF
    
    //printstring("final pitch = ") printfloat(fSetCamPitch) PRINTNL()
    SET_GAMEPLAY_CAM_RELATIVE_HEADING(fSetCamHeading)
    SET_GAMEPLAY_CAM_RELATIVE_PITCH(fSetCamPitch)

ENDPROC



PROC SET_CAM_POINT_AT_CLOSEST_VEHICLE(BOOL bLeftHandSide)
	FLOAT fSetCamHeading
	FLOAT fSetCamPitch
	
	BOOL bSetDefault = FALSE
	
	INT i
	INT iClosest = -1
	FLOAT fClosestDistance = 999999.0
	FOR i=ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1) TO ENUM_TO_INT(MV_MERRY_WEATHER_HELI_3)
		IF IS_VEHICLE_DRIVEABLE(mission_veh[i].veh)
			IF NOT IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(mission_veh[i].veh))
				FLOAT fThisDist = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(mission_veh[i].veh), FALSE)
				VECTOR vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, GET_ENTITY_COORDS(mission_veh[i].veh))
				
				IF (vOffset.x < 0 AND bLeftHandSide)
				OR (vOffset.x > 0 AND NOT bLeftHandSide)
					IF fThisDist < fClosestDistance
						fClosestDistance = fThisDist
						iClosest = i
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	printstring("iclosest = ") printint(iClosest) Printnl()
	
	IF iClosest >= 0
		IF IS_VEHICLE_DRIVEABLE(mission_veh[iClosest].veh)
			VECTOR vTargetVector 
			FLOAT fTargetHeading
			FLOAT fTargetPitch
			vTargetVector = GET_ENTITY_COORDS(mission_veh[iClosest].veh) - GET_ENTITY_COORDS(PLAYER_PED_ID())
			
			fTargetHeading = GET_HEADING_FROM_VECTOR_2D(vTargetVector.x, vTargetVector.y) - GET_ENTITY_HEADING(PLAYER_PED_ID())
		
			IF fTargetHeading > 360.0
				fTargetHeading -= 360.0
			ELIF fTargetHeading < 0.0
				fTargetHeading += 360.0
			ENDIF
			
			printstring("target heading 1 = ") printfloat(fTargetHeading) printnl()
			
			IF bLeftHandSide
				IF fTargetHeading > 180
					IF fTargetHeading > 270
						fTargetHeading = 0
					ELSE
						fTargetHeading = 180
					ENDIF
				ENDIF
			ELSE
				fTargetHeading -= 360
			
				IF fTargetHeading < -180
					IF fTargetHeading < -270
						fTargetHeading = 0
					ELSE
						fTargetHeading = 180
					ENDIF
				ENDIF		
			ENDIF
			
			//fTargetPitch = GET_HEADING_FROM_VECTOR_2D(vTargetVector.y, vTargetVector.z) - 90.0 - GET_ENTITY_ROLL(PLAYER_PED_ID())
			VECTOR vOffset 
			FLOAT fHorizontalDist 
			FLOAT fVerticalDist
			vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(mission_veh[iClosest].veh))
			fHorizontalDist = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), mission_veh[iClosest].veh, FALSE)
			fVerticalDist = vOffset.z
			
			fTargetPitch = ATAN(fVerticalDist/fHorizontalDist)
			
			PRINTSTRING("target heading = ") PRINTFLOAT(fTargetHeading) PRINTNL()
			PRINTSTRING("target pitch = ") PRINTFLOAT(fTargetPitch) PRINTNL()
		
			fSetCamHeading = fTargetHeading
			fSetCamPitch = fTargetPitch
		ELSE
			bSetDefault = TRUE
		ENDIF
	ELSE
		bSetDefault = TRUE
	ENDIF	
	
	IF bSetDefault
		IF bLeftHandSide
			fSetCamHeading = 90.0
		ELSE
			fSetCamHeading = -90.0
		ENDIF
		fSetCamPitch = 0.0
	ENDIF

	printstring("final pitch = ") printfloat(fSetCamPitch) PRINTNL()
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(fSetCamHeading)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(fSetCamPitch)
ENDPROC

PROC WARP_DINGHIES()
	// warp dinghies
	IF NOT bWarpedDinghies
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
				VECTOR vChopperPos = GET_ENTITY_COORDS(mission_veh[MV_CHINOOK].veh)
				FLOAT fWarpXLimit
				IF vChopperPos.y > PENINSULA_Y
					fWarpXLimit = -750
				ELSE
					fWarpXLimit = -920
				ENDIF

				IF vChopperPos.x < fWarpXLimit
					VECTOR vOffsetFromChopper
					VECTOR vIdealOffset
					FLOAT fGroundZ
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh)
						vOffsetFromChopper = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, GET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh))
						IF vOffsetFromChopper.y < -10
							vIdealOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, <<-70,-10,0>>)
							vIdealOffset.z = 0
							GET_GROUND_Z_FOR_3D_COORD(vIdealOffset,fGroundZ)
							IF fGroundZ <= 0 
							AND NOT IS_POINT_IN_ANGLED_AREA(vIdealOffset, <<-995.756104,6278.965820,-8.795349>>, <<-772.137329,5839.614258,27.650452>>, 90.000000)
							AND NOT IS_POINT_IN_ANGLED_AREA(vIdealOffset, <<-892.756104,6052.965820,-8.795349>>, <<-772.137329,5839.614258,27.650452>>, 150.000000)	
								SET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh, vIdealOffset)
								SET_ENTITY_HEADING(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh, GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh))
								SET_VEHICLE_FORWARD_SPEED(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh, 30)
							ENDIF
						ENDIF
					ENDIF

					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh)
						vOffsetFromChopper = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, GET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh))
						IF vOffsetFromChopper.y < -5
							vIdealOffset = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, <<-90,-5,0>>)
							vIdealOffset.z = 0
							GET_GROUND_Z_FOR_3D_COORD(vIdealOffset,fGroundZ)
							IF fGroundZ <= 0 
							AND NOT IS_POINT_IN_ANGLED_AREA(vIdealOffset, <<-995.756104,6278.965820,-8.795349>>, <<-772.137329,5839.614258,27.650452>>, 90.000000)
							AND NOT IS_POINT_IN_ANGLED_AREA(vIdealOffset, <<-892.756104,6052.965820,-8.795349>>, <<-772.137329,5839.614258,27.650452>>, 150.000000)	
								SET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh, vIdealOffset)
								SET_ENTITY_HEADING(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh, GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh))
								SET_VEHICLE_FORWARD_SPEED(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh, 30)
							ENDIF
						ENDIF
					ENDIF					
				ENDIF
			ENDIF
		ENDIF
		bWarpedDinghies = TRUE
	ENDIF
ENDPROC

	
INT iFranklinPosition	

PROC MANAGE_FREE_HOTSWAP(BOOL bStandardCam = TRUE, BOOL bShortCam = FALSE, BOOL bDestroyMerryweatherCam = FALSE, BOOL bForceShort = FALSE, BOOL bUseCustomSwitch = FALSE, BOOL bSetPointing = FALSE)
	
	SWITCH iSwitchStage 
	    CASE 0
			
			IF UPDATE_SELECTOR_HUD(sSelectorPeds,FALSE)
			    CLEAR_HELP()
				DESTROY_ALL_CAMS()

				sSelectorCam.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]

				IF eMissionStage = MISSION_STAGE_PICK_UP_SUB
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
							IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh)
								SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				iSwitchStage++
				
			ENDIF
			IF bUseCustomSwitch = TRUE
				sSelectorCam.pedTo = sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
				eSwitchCamState = SWITCH_CAM_SETUP_SPLINE
				iSwitchStage++
			ENDIF
			
	    BREAK 
	    
	    CASE 1	   
			FLOAT fSwitchHeading //fDestPedHeading
			IF NOT IS_PED_INJURED(sSelectorCam.pedTo)
				//fDestPedHeading = GET_ENTITY_HEADING(sSelectorCam.pedTo)
				
				IF IS_PED_IN_COVER(sSelectorCam.pedTo)
					IF IS_PED_IN_COVER_FACING_LEFT(sSelectorCam.pedTo)
						fSwitchHeading = -90.0
					ELSE
						fSwitchHeading = 90.0
					ENDIF
				ENDIF
			ENDIF
			
			IF bStandardCam
				IF NOT RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sSelectorCam)
					IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed])
					        SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], FALSE)     
					        SET_PED_SUFFERS_CRITICAL_HITS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], FALSE)
					        SET_PED_DIES_WHEN_INJURED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], TRUE)
							SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], rel_group_buddies)
							//SET_PED_COMBAT_PARAMS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 20, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, TLR_NEVER_LOSE_TARGET)
							SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_BLIND_FIRE_IN_COVER, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
							SET_PED_MAX_HEALTH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 1800)
							SET_ENTITY_HEALTH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 1800)
					    ENDIF
						INFORM_MISSION_STATS_OF_INCREMENT(DH2B_SWITCHES) 
						
						//REASSIGN_PLAYER_CHAR_DIALOGUE_SPEAKERS()
						iSwitchStage = 0
					ENDIF
				ELSE
					//SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorCam.pedTo)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(fSwitchHeading)
				ENDIF
			ENDIF
			
			IF bShortCam
				IF NOT RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sSelectorCam,SWITCH_TYPE_SHORT)
					IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed])
					        SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], FALSE)     
					        SET_PED_SUFFERS_CRITICAL_HITS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], FALSE)
					        SET_PED_DIES_WHEN_INJURED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], TRUE)
							SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], rel_group_buddies)
							//SET_PED_COMBAT_PARAMS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 20, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, TLR_NEVER_LOSE_TARGET)
							SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_BLIND_FIRE_IN_COVER, TRUE)
							SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
							SET_PED_MAX_HEALTH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 1800)
							SET_ENTITY_HEALTH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 1800)
					    ENDIF
						INFORM_MISSION_STATS_OF_INCREMENT(DH2B_SWITCHES) 
						
						//REASSIGN_PLAYER_CHAR_DIALOGUE_SPEAKERS()
						iSwitchStage = 0
					ENDIF
				ELSE
					//SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorCam.pedTo)
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(fSwitchHeading)
				ENDIF
			ENDIF
			
			IF bUseCustomSwitch
				
				IF NOT HANDLE_SWITCH_CAM_SUBMARINE(scsSwitchCam_MichaelToTrevor, FALSE)
					IF DOES_CAM_EXIST(scsSwitchCam_MichaelToTrevor.ciSpline)
						IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
							IF GET_CAM_SPLINE_NODE_INDEX(scsSwitchCam_MichaelToTrevor.ciSpline) >= scsSwitchCam_MichaelToTrevor.iCamSwitchFocusNode
								IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
									IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE, TCF_CLEAR_TASK_INTERRUPT_CHECKS)
										IF NOT IS_PED_INJURED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed])
									        SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], FALSE)     
									        SET_PED_SUFFERS_CRITICAL_HITS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], FALSE)
									        SET_PED_DIES_WHEN_INJURED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], TRUE)
											SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], rel_group_buddies)
											//SET_PED_COMBAT_PARAMS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 20, CM_DEFENSIVE, CAL_PROFESSIONAL, CR_MEDIUM, TLR_NEVER_LOSE_TARGET)
											SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_BLIND_FIRE_IN_COVER, TRUE)
											SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
											SET_PED_MAX_HEALTH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 1800)
											SET_ENTITY_HEALTH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 1800)
									    ENDIF
										INFORM_MISSION_STATS_OF_INCREMENT(DH2B_SWITCHES) 
									ENDIF
								ENDIF
								
		//						//REASSIGN_PLAYER_CHAR_DIALOGUE_SPEAKERS()
							
							ENDIF

						ENDIF
					ENDIF
				ELSE
					iSwitchStage = 0
				ENDIF
			ENDIF
			
			IF bDestroyMerryweatherCam
				SWITCH_TYPE eSwitchType
				IF bForceShort
					eSwitchType = SWITCH_TYPE_SHORT
				ELSE
					eSwitchType = SWITCH_TYPE_AUTO
				ENDIF
				eSwitchType = SWITCH_TYPE_AUTO
				
				IF NOT RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sSelectorCam, eSwitchType)
					INFORM_MISSION_STATS_OF_INCREMENT(DH2B_SWITCHES) 
					iSwitchStage = 0
				ELSE		
					IF sSelectorCam.bPedSwitched
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), TRUE)
							SET_PLAYER_FORCED_AIM(PLAYER_ID(), TRUE)	
						ENDIF
					ENDIF				
				
					IF sSelectorCam.bOKToSwitchPed
						IF NOT sSelectorCam.bPedSwitched
							IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
							
								IF NOT IS_PED_INJURED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed])
							        SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], FALSE)     
							        SET_PED_SUFFERS_CRITICAL_HITS(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], FALSE)
							        SET_PED_DIES_WHEN_INJURED(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], TRUE)
									SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], rel_group_buddies)
									SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_BLIND_FIRE_IN_COVER, TRUE)
									SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH, TRUE)
									SET_PED_MAX_HEALTH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 1800)
									SET_ENTITY_HEALTH(sSelectorPeds.pedID[sSelectorPeds.ePreviousSelectorPed], 1800)
							    ENDIF
								
								
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), TRUE)
									SET_PLAYER_FORCED_AIM(PLAYER_ID(), TRUE)									
									FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
									
									WARP_DINGHIES()
									
									IF bSetPointing
										IF iFranklinPosition <= 1
											SET_CAM_POINT_AT_CLOSEST_VEHICLE(iFranklinPosition = 0)
										ENDIF
									ENDIF
								ELSE
									//Fix for 1868356
									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), mission_veh[MV_SUBMERISIBLE].veh)
										SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
									ENDIF
								ENDIF
								
								sSelectorCam.bPedSwitched = TRUE				
							ENDIF
						ENDIF
					ENDIF	
				ENDIF			
			ENDIF
			
	    BREAK 
	ENDSWITCH 
ENDPROC
	
PROC CREATE_CRANE()
	//Load crane in advance
	IF bCreateCrane 
		IF NOT DOES_ENTITY_EXIST(s_crane.obj_main)
			PRINTSTRING("LOADING IN CRANE STUFF")PRINTNL()
			//IF DISTANCE_BETWEEN_COORDS(v_docks_pos, GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE, FALSE) < 40000.0
				
				//Prop_Dock_RTG_LD - the main frame
				//P_Dock_RTG_LD_spdr - the crate grabber.might need animated
				//P_Dock_RTG_LD_Cab - upper cab unit
				//P_Dock_RTG_LD_wheel - 1xwheel , needs duplicated 7 times.that ok?
				//possibly needs animated unless it can be scripted to turn.
				
				MODEL_NAMES model_crane = Prop_Dock_RTG_LD 
				MODEL_NAMES model_spreader = P_Dock_RTG_LD_spdr
				MODEL_NAMES model_cabin = P_Dock_RTG_LD_Cab
				MODEL_NAMES model_container = PROP_CONTAINER_LD
				MODEL_NAMES model_wheel_left = P_Dock_RTG_LD_wheel
				MODEL_NAMES model_wheel_right = P_Dock_RTG_LD_wheel
				//MODEL_NAMES model_boom = P_DOCK_CRANE_CABLES //?
//				MODEL_NAMES model_left_door = PROP_CNTRDOOR_LD_L
//				MODEL_NAMES model_right_door = PROP_CNTRDOOR_LD_R
			
				IF REQUEST_AMBIENT_AUDIO_BANK("SCRIPT\\CAR_THEFT_DOCK_CRANES")		
					REQUEST_MODEL(model_crane)
					//REQUEST_MODEL(model_lift)
					REQUEST_MODEL(model_spreader)
					REQUEST_MODEL(model_cabin)
					REQUEST_MODEL(model_container)
					REQUEST_MODEL(model_wheel_left)
					REQUEST_MODEL(model_wheel_right)
					//REQUEST_MODEL(model_boom)
					REQUEST_MODEL(PROP_BOX_WOOD03A)
					REQUEST_COLLISION_FOR_MODEL(model_spreader)
				
					IF HAS_MODEL_LOADED(model_crane)
					//AND HAS_MODEL_LOADED(model_lift)
					AND HAS_MODEL_LOADED(model_spreader)
					AND HAS_MODEL_LOADED(model_cabin)
					AND HAS_MODEL_LOADED(model_container)
					AND HAS_MODEL_LOADED(model_wheel_left)
					AND HAS_MODEL_LOADED(model_wheel_right)
					//AND HAS_MODEL_LOADED(model_boom)
					AND HAS_MODEL_LOADED(PROP_BOX_WOOD03A)
					AND HAS_COLLISION_FOR_MODEL_LOADED(model_spreader)						
						//Main crane
						s_crane.v_start_pos = v_crane_pos
						s_crane.obj_main = CREATE_OBJECT(model_crane, s_crane.v_start_pos)
						SET_ENTITY_HEADING(s_crane.obj_main, 90)
						FREEZE_ENTITY_POSITION(s_crane.obj_main, TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(model_crane)
						
						//Cabin
						s_crane.v_cabin_attach_offset = <<0.0, 0.0, 18.0>>
						s_crane.obj_cabin = CREATE_OBJECT(model_cabin, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_main, s_crane.v_cabin_attach_offset))
						SET_ENTITY_COORDS_NO_OFFSET(s_crane.obj_cabin, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_main, s_crane.v_cabin_attach_offset))
						SET_ENTITY_HEADING(s_crane.obj_cabin, GET_ENTITY_HEADING(s_crane.obj_main))
						FREEZE_ENTITY_POSITION(s_crane.obj_cabin, TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(model_cabin)
						
						//Spreader and helper: the spreader is attached to an invisible helper object
						//s_crane.v_spreader_attach_offset = <<-2.5, 0.0, SPREADER_START_OFFSET>>
						s_crane.v_spreader_attach_offset = <<0.0, 2.5, SPREADER_START_OFFSET>>
						s_crane.obj_spreader = CREATE_OBJECT(model_spreader, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, s_crane.v_spreader_attach_offset))
						SET_OBJECT_PHYSICS_PARAMS(s_crane.obj_spreader, 10000.0, -1.0, <<-1.0, -1.0, -1.0>>, <<-1.0, -1.0, -1.0>>)
						//SET_ENTITY_COLLISION(s_crane.obj_spreader,FALSE)
						ATTACH_ENTITY_TO_ENTITY(s_crane.obj_spreader, s_crane.obj_cabin, -1, s_crane.v_spreader_attach_offset, <<0.0, 0.0, 0.0>>)
						DETACH_ENTITY(s_crane.obj_spreader)
						//fudge
						//SET_ENTITY_VISIBLE(s_crane.obj_spreader, FALSE)
						
						VECTOR v_helper_offset = s_crane.v_spreader_attach_offset - <<0.0, 0.0, SPREADER_START_OFFSET>>
						s_crane.obj_helper = CREATE_OBJECT(model_spreader, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, v_helper_offset))
						SET_ENTITY_HEADING(s_crane.obj_helper, GET_ENTITY_HEADING(s_crane.obj_cabin))
						FREEZE_ENTITY_POSITION(s_crane.obj_helper, TRUE)
						SET_ENTITY_VISIBLE(s_crane.obj_helper, FALSE)
						SET_ENTITY_COLLISION(s_crane.obj_helper,FALSE)
						//SET_ENTITY_VISIBLE(s_crane.obj_helper, TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(model_spreader)

						s_crane.i_dampingframes = 0
					
						s_crane.ropes[0] = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, <<-1.0, -2.7, -1.75>>),
													<<0.0, 0.0, 0.0>>, 15.26, PHYSICS_ROPE_THIN, -SPREADER_START_OFFSET , f_ropes_min_len, f_ropes_len_change_rate )
						s_crane.ropes[1] = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, <<-1.0, 2.7, -1.75>>),
													<<0.0, 0.0, 0.0>>, 15.26, PHYSICS_ROPE_THIN, -SPREADER_START_OFFSET , f_ropes_min_len, f_ropes_len_change_rate )
						s_crane.ropes[2] = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, <<1.0, 2.7, -1.75>>),
													<<0.0, 0.0, 0.0>>, 15.26, PHYSICS_ROPE_THIN, -SPREADER_START_OFFSET , f_ropes_min_len, f_ropes_len_change_rate )
						s_crane.ropes[3] = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin,<<1.0, 2.7, -1.75>>),
													<<0.0, 0.0, 0.0>>, 15.26, PHYSICS_ROPE_THIN, -SPREADER_START_OFFSET , f_ropes_min_len, f_ropes_len_change_rate )
													
						s_crane.ropes[4] = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, <<-0.05, -2.7, -1.75>>),
													<<0.0, 0.0, 0.0>>, 15.26, PHYSICS_ROPE_THIN, -SPREADER_START_OFFSET , f_ropes_min_len, f_ropes_len_change_rate )
						s_crane.ropes[5] = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, <<-0.05, 2.7, -1.75>>),
													<<0.0, 0.0, 0.0>>, 15.26, PHYSICS_ROPE_THIN, -SPREADER_START_OFFSET , f_ropes_min_len, f_ropes_len_change_rate )
						s_crane.ropes[6] = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, <<0.05, -2.7, -1.75>>),
													<<0.0, 0.0, 0.0>>, 15.26, PHYSICS_ROPE_THIN, -SPREADER_START_OFFSET , f_ropes_min_len, f_ropes_len_change_rate )
						s_crane.ropes[7] = ADD_ROPE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, <<0.05, 2.7, -1.75>>),
													<<0.0, 0.0, 0.0>>, 15.26, PHYSICS_ROPE_THIN, -SPREADER_START_OFFSET, f_ropes_min_len, f_ropes_len_change_rate )
													
						b_added_ropes = TRUE

						ATTACH_ENTITIES_TO_ROPE(s_crane.ropes[0], s_crane.obj_cabin, s_crane.obj_spreader,
												GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, vRopeTop1),
												GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, vRopeBottom1), -SPREADER_START_OFFSET , 0, 0)
												
						ATTACH_ENTITIES_TO_ROPE(s_crane.ropes[1], s_crane.obj_cabin, s_crane.obj_spreader,
												GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, vRopeTop2),
												GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, vRopeBottom2), -SPREADER_START_OFFSET , 0, 0)
												
						ATTACH_ENTITIES_TO_ROPE(s_crane.ropes[2], s_crane.obj_cabin, s_crane.obj_spreader,
												GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, vRopeTop3),
												GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, vRopeBottom3), -SPREADER_START_OFFSET, 0, 0)
												
						ATTACH_ENTITIES_TO_ROPE(s_crane.ropes[3], s_crane.obj_cabin, s_crane.obj_spreader,
												GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, vRopeTop4),
												GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, vRopeBottom4), -SPREADER_START_OFFSET, 0, 0)
												
						ATTACH_ENTITIES_TO_ROPE(s_crane.ropes[4], s_crane.obj_cabin, s_crane.obj_spreader,
												GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, vRopeTop5),
												GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, vRopeBottom5), -SPREADER_START_OFFSET , 0, 0)
												
						ATTACH_ENTITIES_TO_ROPE(s_crane.ropes[5], s_crane.obj_cabin, s_crane.obj_spreader,
												GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, vRopeTop6),
												GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, vRopeBottom6), -SPREADER_START_OFFSET , 0, 0)
												
						ATTACH_ENTITIES_TO_ROPE(s_crane.ropes[6], s_crane.obj_cabin, s_crane.obj_spreader,
												GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, vRopeTop7),
												GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, vRopeBottom7), -SPREADER_START_OFFSET, 0, 0)
												
						ATTACH_ENTITIES_TO_ROPE(s_crane.ropes[7], s_crane.obj_cabin, s_crane.obj_spreader,
												GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, vRopeTop8),
												GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_spreader, vRopeBottom8), -SPREADER_START_OFFSET, 0, 0)

						// Wheel positions
//							____________________
//							|6                2|
//							|7                3|
//							|                  |
//							|                  |
//							|                  |
//							|4                0|
//                          _5________________1_ 

						//Set 1 0-1
						s_crane.v_wheel_offsets[0] = <<4.800, 9.300, 0.680>>
						s_crane.v_wheel_offsets[1] = <<3.400, 9.300, 0.680>>
						//Set 2 2-3
						s_crane.v_wheel_offsets[2] = <<-4.900, 9.300, 0.680>>
						s_crane.v_wheel_offsets[3] = <<-3.500, 9.300, 0.680>>
						//Set 3 4-5
						s_crane.v_wheel_offsets[4] = <<4.800, -9.300, 0.680>>
						s_crane.v_wheel_offsets[5] = <<3.400, -9.300, 0.680>>
						//Set 4 6-7
						s_crane.v_wheel_offsets[6] = <<-4.900, -9.300, 0.680>>
						s_crane.v_wheel_offsets[7] = <<-3.500, -9.300, 0.680>>
						INT i
						REPEAT COUNT_OF(s_crane.obj_wheels) i
							MODEL_NAMES model_wheel = model_wheel_left
							IF i > 3
								model_wheel = model_wheel_right
								s_crane.obj_wheels[i] = CREATE_OBJECT_NO_OFFSET(model_wheel, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_main, s_crane.v_wheel_offsets[i]))
								SET_ENTITY_HEADING(s_crane.obj_wheels[i], GET_ENTITY_HEADING(s_crane.obj_main)-180)
								ATTACH_ENTITY_TO_ENTITY(s_crane.obj_wheels[i], s_crane.obj_main, -1, s_crane.v_wheel_offsets[i], <<0.0, 0.0, 0.0>>)
							ELSE
								s_crane.obj_wheels[i] = CREATE_OBJECT_NO_OFFSET(model_wheel, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_main, s_crane.v_wheel_offsets[i]))
								SET_ENTITY_HEADING(s_crane.obj_wheels[i], GET_ENTITY_HEADING(s_crane.obj_main))
								ATTACH_ENTITY_TO_ENTITY(s_crane.obj_wheels[i], s_crane.obj_main, -1, s_crane.v_wheel_offsets[i], <<0.0, 0.0, 0.0>>)
							ENDIF
						ENDREPEAT
						
						SET_MODEL_AS_NO_LONGER_NEEDED(model_wheel_left)
						SET_MODEL_AS_NO_LONGER_NEEDED(model_wheel_right)

						//Containers				
						s_containers[0].obj_main = CREATE_OBJECT_NO_OFFSET(model_container, << 484.4861, -3161.6719,6.24 >>)
						SET_ENTITY_HEADING(s_containers[0].obj_main, 180.0)
						SET_OBJECT_PHYSICS_PARAMS(s_containers[0].obj_main, 1000.0, -1.0, <<-1.0, -1.0, -1.0>>, <<-1.0, -1.0, -1.0>>)
						ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(s_containers[0].obj_main, s_crane.obj_spreader, -1, -1, <<0.0, 0.0, -1.8>>, <<0.0, 0.0, 2.0>>, <<0.0, 0.0, 90.0>>, -1.0, TRUE, TRUE)
						s_crane.b_container_attached = TRUE	
						
						SET_MODEL_AS_NO_LONGER_NEEDED(model_container)
						
						//Ammo Dump
						objAmmo = CREATE_OBJECT_NO_OFFSET(PROP_BOX_WOOD03A, << 484.4861, -3161.6719,6.24 >>)
						SET_ENTITY_HEADING(objAmmo, 180.0)
						SET_OBJECT_PHYSICS_PARAMS(objAmmo, 1000.0, -1.0, <<-1.0, -1.0, -1.0>>, <<-1.0, -1.0, -1.0>>)
						ATTACH_ENTITY_TO_ENTITY(objAmmo, s_containers[0].obj_main, -1, <<0.0, 0.0, 0.2>>, <<0.0, 0.0, 0>>)

						SET_MODEL_AS_NO_LONGER_NEEDED(PROP_BOX_WOOD03A)

					
						//Setup crane sound ids
						s_crane.i_cabin_sound_id = GET_SOUND_ID()
						s_crane.i_crane_sound_id = GET_SOUND_ID()
						s_crane.i_spreader_sound_id = GET_SOUND_ID()
						s_crane.i_lift_sound_id = GET_SOUND_ID()
						
						//Attach crane parts to the main crane (collision is no longer needed on them)
						FREEZE_ENTITY_POSITION(s_crane.obj_cabin, FALSE)
						ATTACH_ENTITY_TO_ENTITY(s_crane.obj_cabin, s_crane.obj_main, -1, s_crane.v_cabin_attach_offset, <<0.0, 0.0, 0.0>>)
						
						REPEAT COUNT_OF(s_crane.obj_wheels) iCount
							FREEZE_ENTITY_POSITION(s_crane.obj_wheels[iCount], FALSE)
							ATTACH_ENTITY_TO_ENTITY(s_crane.obj_wheels[iCount], s_crane.obj_main, -1, s_crane.v_wheel_offsets[iCount], <<0.0, 0.0, 0.0>>)
						ENDREPEAT
						//Setup the container checking data
//							REPEAT COUNT_OF(s_containers) i
//								s_containers[i].b_is_on_boat = FALSE
//								s_containers[i].b_is_full = FALSE
//								
//								s_containers[i].s_spots[0].v_offset = <<0.0, 2.5, 0.5>>
//								s_containers[i].s_spots[0].b_is_active = TRUE
//								s_containers[i].s_spots[0].b_is_taken = FALSE
//								
//								s_containers[i].s_spots[1].v_offset = <<0.0, -3.0, 0.5>>
//								s_containers[i].s_spots[1].b_is_active = FALSE
//								s_containers[i].s_spots[1].b_is_taken = FALSE
//							ENDREPEAT
					
						//Stick all the cars except the player's car into the containers in advance
					//ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
	//Clean up Shiz
			//DELETE ROPES FIRST
		IF b_added_ropes
			INT i = 0
			REPEAT 8 i
				DELETE_ROPE(s_crane.ropes[i])
			ENDREPEAT
			
			b_added_ropes = FALSE
		ENDIF	
		DELETE_OBJECT(s_crane.obj_main)
		DELETE_OBJECT(s_crane.obj_cabin)
		DELETE_OBJECT(s_crane.obj_spreader)
		DELETE_OBJECT(s_crane.obj_helper)
		DELETE_OBJECT(s_crane.obj_wheels[0])
		DELETE_OBJECT(s_crane.obj_wheels[1])
		DELETE_OBJECT(s_crane.obj_wheels[2])
		DELETE_OBJECT(s_crane.obj_wheels[3])
		DELETE_OBJECT(s_crane.obj_wheels[4])
		DELETE_OBJECT(s_crane.obj_wheels[5])
		DELETE_OBJECT(s_crane.obj_wheels[6])
		DELETE_OBJECT(s_crane.obj_wheels[7])
	ENDIF
ENDPROC

FUNC BOOL CAN_CRANE_PICK_UP_CONTAINER(VECTOR v_container_pos, VECTOR v_spreader_pos)
	IF ABSF(v_container_pos.x - v_spreader_pos.x) < 5
		IF ABSF(v_container_pos.y - v_spreader_pos.y) < 5
			IF (v_spreader_pos.z - v_container_pos.z) > 2
				IF (v_spreader_pos.z - v_container_pos.z) < 6
					PRINTSTRING("can pickup")
					PRINTNL()
					RETURN TRUE
				ENDIF
			ENDIF	
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

PROC CHECK_CONTAINER_ATTACHING(OBJECT_INDEX &obj_container)
	IF IS_ENTITY_ATTACHED(obj_container)
		DETACH_ENTITY(obj_container)
		SET_OBJECT_PHYSICS_PARAMS(obj_container, -1.0, -1.0, <<-1.0, -1.0, -1.0>>, <<-1.0, -1.0, -1.0>>)
		PLAY_SOUND_FROM_ENTITY(-1, "CAR_THEFT_EXPORT_CARS_DETACH_CONTAINER", obj_container)
		s_crane.b_container_attached = FALSE
	ELSE
		IF NOT s_crane.b_container_attached
			PRINTSTRING("calling pickup")
			PRINTNL()
			IF CAN_CRANE_PICK_UP_CONTAINER(GET_ENTITY_COORDS(obj_container), GET_ENTITY_COORDS(s_crane.obj_spreader))
				FREEZE_ENTITY_POSITION(obj_container, FALSE)
				SET_OBJECT_PHYSICS_PARAMS(obj_container, 1000.0, -1.0, <<-1.0, -1.0, -1.0>>, <<-1.0, -1.0, -1.0>>)
				ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(obj_container, s_crane.obj_spreader, -1, -1, <<0.0, 0.0, -2.3>>, <<0.0, 0.0, 2.0>>, <<0.0, 0.0, 90.0>>, -1.0, TRUE, TRUE)
				PLAY_SOUND_FROM_ENTITY(-1, "CAR_THEFT_EXPORT_CARS_ATTACH_CONTAINER", obj_container)
				
				s_crane.b_container_attached = TRUE					
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_CRANE()
	
	INT i_left_x = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X) - 128
	INT i_left_y = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y) - 128
	INT i_right_y = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y) - 128
	
	INT i = 0
	
	REPEAT 8 i
		STOP_ROPE_UNWINDING_FRONT(s_crane.ropes[i])
		STOP_ROPE_WINDING(s_crane.ropes[i])
		
		IF i_right_y < 0				
			START_ROPE_WINDING(s_crane.ropes[i])
		ENDIF

		IF i_right_y > 0
			START_ROPE_UNWINDING_FRONT(s_crane.ropes[i])	
		ENDIF
	ENDREPEAT
	
	IF NOT b_is_audio_scene_active
		START_AUDIO_SCENE("CAR_THEFT_EXPORT_CARS_CRANE_SECTION_SCENE")
		b_is_audio_scene_active = TRUE
	ENDIF
	
	//TODO 100140: should not be able to move diagonally on the left stick, cancel one axis out if the other is greater.
	IF ABSI(i_left_x) > ABSI(i_left_y)
		i_left_y = 0
	ELSE
		i_left_x = 0
	ENDIF	
	
	//CABIN
	FLOAT f_cabin_speed_modifier = 1.0
	IF ABSF(s_crane.v_cabin_attach_offset.y - MAX_CABIN_Y) < 4.0 AND s_crane.f_cabin_vel > 0.0
		f_cabin_speed_modifier = ABSF(s_crane.v_cabin_attach_offset.y - MAX_CABIN_y) / 4.0
	ELIF ABSF(s_crane.v_cabin_attach_offset.y - MIN_CABIN_Y) < 4.0 AND s_crane.f_cabin_vel < 0.0
		f_cabin_speed_modifier = ABSF(s_crane.v_cabin_attach_offset.y - MIN_CABIN_y) / 4.0
	ENDIF
	FLOAT f_desired_cabin_vel
	IF b_is_crane_cinematic_active
		f_desired_cabin_vel = ((TO_FLOAT(i_left_y) / 128.0) * MAX_CABIN_VEL) * f_cabin_speed_modifier
		CONVERGE_VALUE(s_crane.f_cabin_vel, f_desired_cabin_vel, CABIN_ACCEL, TRUE)
		s_crane.v_cabin_attach_offset.y = s_crane.v_cabin_attach_offset.y +@ s_crane.f_cabin_vel
	ELSE
		f_desired_cabin_vel = ((TO_FLOAT(i_left_y) / 128.0) * -MAX_CABIN_VEL) * f_cabin_speed_modifier
		CONVERGE_VALUE(s_crane.f_cabin_vel, f_desired_cabin_vel, CABIN_ACCEL, TRUE)
		s_crane.v_cabin_attach_offset.y = s_crane.v_cabin_attach_offset.y +@ s_crane.f_cabin_vel
	ENDIF
	IF s_crane.v_cabin_attach_offset.y < MIN_CABIN_Y
		s_crane.v_cabin_attach_offset.y = MIN_CABIN_Y
		s_crane.f_cabin_vel = 0.0
	ELIF s_crane.v_cabin_attach_offset.y > MAX_CABIN_Y
		s_crane.v_cabin_attach_offset.y = MAX_CABIN_Y
		s_crane.f_cabin_vel = 0.0
	ENDIF 
	
	DETACH_ENTITY(s_crane.obj_cabin)
	ATTACH_ENTITY_TO_ENTITY(s_crane.obj_cabin, s_crane.obj_main, -1, s_crane.v_cabin_attach_offset, <<0.0, 0.0, 0.0>>)
	
	// damp spreader movement when stopping so it doesn't swing ... much	
		
	FLOAT spreader_damping = 0.0
	IF ABSI(i_left_y) > 0.0		
		s_crane.i_dampingframes = -1
	ELIF i_left_x = 0.0
		IF s_crane.i_dampingframes = -1
			s_crane.i_dampingframes = i_spreader_dampingframes
		ENDIF
		IF s_crane.i_dampingframes > 0
			spreader_damping = f_spreader_damping
			s_crane.i_dampingframes = s_crane.i_dampingframes - 1
		ENDIF
	ELIF ABSI(i_left_x) > 0.0
		s_crane.i_dampingframes = -1
	ENDIF
	ACTIVATE_PHYSICS( s_crane.obj_spreader )
	SET_DAMPING( s_crane.obj_spreader, PHYSICS_DAMPING_LINEAR_C, spreader_damping )
////		
	
	//Audio
	IF ABSF(s_crane.f_cabin_vel) > AUDIO_TRIGGER_THRESHOLD
		IF HAS_SOUND_FINISHED(s_crane.i_cabin_sound_id)
			PLAY_SOUND_FROM_ENTITY(s_crane.i_cabin_sound_id, "CAR_THEFT_EXPORT_CARS_CRANE_MOVE_L_R", s_crane.obj_spreader)
		ENDIF
		
		SET_VARIABLE_ON_SOUND(s_crane.i_cabin_sound_id, "Speed", ABSF(s_crane.f_cabin_vel / MAX_CABIN_VEL))
	ELSE
		IF NOT HAS_SOUND_FINISHED(s_crane.i_cabin_sound_id)
			STOP_SOUND(s_crane.i_cabin_sound_id)
		ENDIF
	ENDIF
	
	//SPREADER
	FLOAT f_spreader_speed_modifier = 1.0
	IF ABSF(s_crane.v_spreader_attach_offset.z - MAX_SPREADER_Z) < 4.0 AND s_crane.f_spreader_vel > 0.0
		f_spreader_speed_modifier = ABSF(s_crane.v_spreader_attach_offset.z - MAX_SPREADER_Z) / 4.0
	ELIF ABSF(s_crane.v_spreader_attach_offset.z - MIN_SPREADER_Z) < 4.0 AND s_crane.f_spreader_vel < 0.0
		f_spreader_speed_modifier = ABSF(s_crane.v_spreader_attach_offset.z - MIN_SPREADER_Z) / 4.0
	ENDIF
	
	FLOAT f_desired_spreader_vel = ((TO_FLOAT(i_right_y) / 128.0) * -MAX_SPREADER_VEL) * f_spreader_speed_modifier
	CONVERGE_VALUE(s_crane.f_spreader_vel, f_desired_spreader_vel, SPREADER_ACCEL, TRUE)
	s_crane.v_spreader_attach_offset.z = s_crane.v_spreader_attach_offset.z +@ s_crane.f_spreader_vel
	
	IF s_crane.v_spreader_attach_offset.z < MIN_SPREADER_Z
		s_crane.v_spreader_attach_offset.z = MIN_SPREADER_Z
		s_crane.f_spreader_vel = 0.0
	ELIF s_crane.v_spreader_attach_offset.z > MAX_SPREADER_Z
		s_crane.v_spreader_attach_offset.z = MAX_SPREADER_Z
		s_crane.f_spreader_vel = 0.0
	ENDIF
	
	//DETACH_ENTITY(s_crane.obj_spreader)
	//ATTACH_ENTITY_TO_ENTITY(s_crane.obj_spreader, s_crane.obj_cabin, 0, s_crane.v_spreader_attach_offset, <<0.0, 0.0, 0.0>>)
	VECTOR v_helper_offset = <<s_crane.v_spreader_attach_offset.x, s_crane.v_spreader_attach_offset.y, s_crane.v_spreader_attach_offset.z - SPREADER_START_OFFSET>>
	SET_ENTITY_COORDS_NO_OFFSET(s_crane.obj_helper, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_crane.obj_cabin, v_helper_offset))				  
	
	//Audio
	IF ABSF(s_crane.f_spreader_vel) > AUDIO_TRIGGER_THRESHOLD
		IF HAS_SOUND_FINISHED(s_crane.i_spreader_sound_id)
			PLAY_SOUND_FROM_ENTITY(s_crane.i_spreader_sound_id, "CAR_THEFT_EXPORT_CARS_CRANE_MOVE_U_D", s_crane.obj_spreader)
		ENDIF
		SET_VARIABLE_ON_SOUND(s_crane.i_spreader_sound_id, "Speed", ABSF(s_crane.f_spreader_vel / MAX_SPREADER_VEL))
	ELSE
		IF NOT HAS_SOUND_FINISHED(s_crane.i_spreader_sound_id)
			STOP_SOUND(s_crane.i_spreader_sound_id)
		ENDIF
	ENDIF
	
	//MAIN CRANE
	FLOAT f_desired_crane_vel
	IF b_is_crane_cinematic_active
		f_desired_crane_vel = (TO_FLOAT(i_left_x) / 128.0) * -MAX_CRANE_VEL
		CONVERGE_VALUE(s_crane.f_crane_vel, f_desired_crane_vel, CRANE_ACCEL, TRUE)
		s_crane.f_crane_offset = s_crane.f_crane_offset +@ s_crane.f_crane_vel 
	ELSE
		f_desired_crane_vel = (TO_FLOAT(i_left_x) / 128.0) * MAX_CRANE_VEL
		CONVERGE_VALUE(s_crane.f_crane_vel, f_desired_crane_vel, CRANE_ACCEL, TRUE)
		s_crane.f_crane_offset = s_crane.f_crane_offset +@ s_crane.f_crane_vel 
	ENDIF
	
	IF s_crane.f_crane_offset < MIN_CRANE_OFFSET
		s_crane.f_crane_offset = MIN_CRANE_OFFSET
		s_crane.f_crane_vel = 0.0
	ELIF s_crane.f_crane_offset > MAX_CRANE_OFFSET
		s_crane.f_crane_offset = MAX_CRANE_OFFSET
		s_crane.f_crane_vel = 0.0
	ENDIF
	
	SET_ENTITY_COORDS_NO_OFFSET(s_crane.obj_main, s_crane.v_start_pos + <<0.0, s_crane.f_crane_offset, 0.0>>)	
	
	//Audio
	IF ABSF(s_crane.f_crane_vel) > AUDIO_TRIGGER_THRESHOLD
		IF HAS_SOUND_FINISHED(s_crane.i_crane_sound_id)
			PLAY_SOUND_FROM_ENTITY(s_crane.i_crane_sound_id,"CAR_THEFT_EXPORT_CARS_CRANE_MOVE_BASE",s_crane.obj_spreader)
		ENDIF
		SET_VARIABLE_ON_SOUND(s_crane.i_crane_sound_id,"Speed",ABSF(s_crane.f_crane_vel / MAX_CRANE_VEL))
	ELSE
		IF NOT HAS_SOUND_FINISHED(s_crane.i_crane_sound_id)
			STOP_SOUND(s_crane.i_crane_sound_id)
		ENDIF
	ENDIF
	
//	//Fudge for spreader getting stuck: try to knock it into activity
//	VECTOR v_vel = GET_ENTITY_VELOCITY(s_crane.obj_spreader)
//	IF v_vel.x = 0.0 AND v_vel.y = 0.0 AND v_vel.z  = 0.0
//		IF f_desired_spreader_vel != 0.0 OR f_desired_cabin_vel != 0.0 OR f_desired_crane_vel != 0.0
//			SET_ENTITY_VELOCITY(s_crane.obj_spreader, <<0.0, 0.0, 4.0>>)
//		ENDIF
//	ENDIF
	
	//Manage containers
	REPEAT COUNT_OF(s_containers) i 
		IF DOES_ENTITY_EXIST(s_containers[i].obj_main)
			s_containers[i].f_prev_speed = s_containers[i].f_speed
			s_containers[i].f_speed = ABSF(GET_SCALAR_VELOCITY(GET_ENTITY_VELOCITY(s_containers[i].obj_main), FALSE))
			
			//Freeze if it's not moving (to stop it from being knocked too easily)
			IF NOT IS_ENTITY_ATTACHED(s_containers[i].obj_main)
				IF s_containers[i].f_speed < 0.02
					FREEZE_ENTITY_POSITION(s_containers[i].obj_main, TRUE)
				ENDIF
			ENDIF
			
			//Attachments
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				CHECK_CONTAINER_ATTACHING(s_containers[i].obj_main)
			ENDIF
			
			//Container collision sounds - messy
			FLOAT f_speed_diff = ABSF(s_containers[i].f_speed - s_containers[i].f_prev_speed)
			
			IF f_speed_diff > 6.0
				INT i_impact_sound_id = GET_SOUND_ID()
				PLAY_SOUND_FROM_ENTITY(i_impact_sound_id, "CAR_THEFT_EXPORT_CARS_CONTAINER_IMPACT", s_containers[i].obj_main)
				SET_VARIABLE_ON_SOUND(i_impact_sound_id, "Speed", f_speed_diff / 100.0)
				RELEASE_SOUND_ID(i_impact_sound_id)
			ENDIF
		ENDIF
	ENDREPEAT
	
	//Camera: interp based on both the position of the cabin and the position of the spreader
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_SELECT)
		b_is_crane_cinematic_active = NOT b_is_crane_cinematic_active
	ENDIF
	
	IF NOT DOES_CAM_EXIST(cutscene_cam)
		cutscene_cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
		SHAKE_CAM(cutscene_cam, "HAND_SHAKE", 0.1)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
	ENDIF	
	
	IF NOT b_is_crane_cinematic_active
		FLOAT f_cabin_progress = (s_crane.v_cabin_attach_offset.y - MIN_CABIN_Y) / (MAX_CABIN_Y - MIN_CABIN_Y)
		FLOAT f_spreader_progress = 1.0 - ((s_crane.v_spreader_attach_offset.z - MIN_SPREADER_Z) / (MAX_SPREADER_Z - MIN_SPREADER_Z))
		
//		VECTOR v_cabin_attach_start = <<15.1064, -25.8022, 11.8410>>
//		VECTOR v_cabin_point_start = <<13.6553, -23.6718, 10.3063>>
//		VECTOR v_cabin_attach_end = <<7.4302, -25.8017, 11.7590>>
//		VECTOR v_cabin_point_end = <<6.4453, -23.4199, 10.2238>>
//		
//		VECTOR v_spreader_attach_start = <<15.1069, -25.8024, 11.8412>>
//		VECTOR v_spreader_point_start = <<13.6548, -23.6745, 10.3039>>
//		VECTOR v_spreader_attach_end = <<15.1069, -25.5991, -24.6243>> //<<9.3152, -25.5991, -15.9827>>
//		VECTOR v_spreader_point_end = <<13.6548, -23.1884, -25.6489>> //<<8.2246, -23.1884, -17.3965>>
		
		VECTOR v_current_attach = v_cabin_attach_start
		v_current_attach.x += (v_cabin_attach_end.x - v_cabin_attach_start.x) * f_cabin_progress
		v_current_attach.y += (v_cabin_attach_end.y - v_cabin_attach_start.y) * f_cabin_progress
		v_current_attach.z += (v_cabin_attach_end.z - v_cabin_attach_start.z) * f_cabin_progress 
		
		v_current_attach.x += (v_spreader_attach_end.x - v_spreader_attach_start.x) * f_spreader_progress
		v_current_attach.y += (v_spreader_attach_end.y - v_spreader_attach_start.y) * f_spreader_progress
		v_current_attach.z += (v_spreader_attach_end.z - v_spreader_attach_start.z) * f_spreader_progress 
		
		VECTOR v_current_point = v_cabin_point_start
		v_current_point.x += (v_cabin_point_end.x - v_cabin_point_start.x) * f_cabin_progress
		v_current_point.y += (v_cabin_point_end.y - v_cabin_point_start.y) * f_cabin_progress
		v_current_point.z += (v_cabin_point_end.z - v_cabin_point_start.z) * f_cabin_progress
		
		v_current_point.x += (v_spreader_point_end.x - v_spreader_point_start.x) * f_spreader_progress
		v_current_point.y += (v_spreader_point_end.y - v_spreader_point_start.y) * f_spreader_progress
		v_current_point.z += (v_spreader_point_end.z - v_spreader_point_start.z) * f_spreader_progress
		
		ATTACH_CAM_TO_ENTITY(cutscene_cam, s_crane.obj_cabin, v_current_attach)
		POINT_CAM_AT_ENTITY(cutscene_cam, s_crane.obj_cabin, v_current_point)
		SET_CAM_FOV(cutscene_cam, f_crane_cam_fov)
	ELSE
		ATTACH_CAM_TO_ENTITY(cutscene_cam, s_crane.obj_cabin, v_attach_to_cam_value)
		STOP_CAM_POINTING(cutscene_cam)
		SET_CAM_ROT(cutscene_cam, v_crane_attach_cam_rot)
		SET_CAM_FOV(cutscene_cam, f_crane_attach_cam_fov)
	ENDIF
	
	//Light: indicates where on the ground the spreader will land
	VECTOR v_spreader_pos = GET_ENTITY_COORDS(s_crane.obj_spreader)
	VECTOR v_ground_pos = v_spreader_pos
	GET_GROUND_Z_FOR_3D_COORD(v_spreader_pos, v_ground_pos.z)
	v_ground_pos.z += 0.5
	
	IF NOT s_crane.b_container_attached
		DRAW_LIGHT_WITH_RANGE(v_ground_pos, 0, 255, 0, 2.0, 40.0)
	ENDIF
	
	
ENDPROC



//PURPOSE: defines up the individual ped details
PROC INITALISE_ARRAYS() // defines up the individual ped details

	//lester
	
	buddy[0].loc = << -332.2557, -2767.9988, 4.1403 >>
	buddy[0].head = 77.2407
	buddy[0].wpn = WEAPONTYPE_UNARMED
	buddy[0].model = GET_NPC_PED_MODEL(CHAR_RON)
	
	mission_veh[7].loc = << 591.86, -3126.65, 5.53 >>
	mission_veh[7].head = 80.21
	
	mission_veh[23].loc = << 488.73, -3120.27, 6.15 >>
	mission_veh[23].head = 182.92

	//enemies
	
	//Player Position = << 619.7509, -3167.1389, 5.0887 >>   heading: 116.6935 
	
	enemy[0].loc = << 616.25, -3197.86, 6.07 >> 
	enemy[0].head = 75.78
	enemy[0].wpn = WEAPONTYPE_PISTOL
	enemy[0].model =S_M_Y_BlackOps_01
	
	//NOW MARINE GUY
	enemy[1].loc = << 613.19, -3052.95, 5.11 >>
	enemy[1].head = -7.38
	enemy[1].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[1].model =S_M_Y_BlackOps_01
	
	enemy[2].loc =<< 609.54, -3201.64, 5.0690 >>
	enemy[2].head = 61.53
	enemy[2].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[2].model =S_M_Y_BlackOps_01

	enemy[3].loc = << 599.0598, -3140.7258, 5.0690 >>  
	enemy[3].head = 20.3269 
	enemy[3].wpn = WEAPONTYPE_UNARMED 
	enemy[3].model =S_M_Y_BlackOps_01
	enemy[3].vcov = << 581.1614, -3122.6624, 5.0693 >> 
	
	enemy[4].loc =<< 597.8522, -3137.7893, 5.0694 >> 
	enemy[4].head =  201.0221
	enemy[4].wpn = WEAPONTYPE_UNARMED
	enemy[4].model =S_M_Y_BlackOps_01
	enemy[4].vcov = << 581.4792, -3125.0190, 5.0693 >>
	
	enemy[5].loc =<< 546.5981, -3169.6133, 21.7621 >>
	enemy[5].head = 100.4340
	enemy[5].wpn = WEAPONTYPE_UNARMED
	enemy[5].model =S_M_Y_BlackOps_01
	
	enemy[6].loc =<< 541.4543, -3119.2209, 7.5634 >>
	enemy[6].head = 29.3428
	enemy[6].wpn = WEAPONTYPE_UNARMED
	enemy[6].model =S_M_Y_BlackOps_01

	enemy[7].loc =<< 587.0986, -3118.3572, 17.7141 >>
	enemy[7].head = 180.4285
	enemy[7].wpn = WEAPONTYPE_UNARMED
	enemy[7].model =S_M_Y_BlackOps_01
	
	enemy[8].loc = << 557.8976, -3130.9917, 16.3610 >>
	enemy[8].head = 91.5429
	enemy[8].wpn = WEAPONTYPE_UNARMED
	enemy[8].model =S_M_Y_BlackOps_01

	enemy[9].loc = << 549.73, -3175.39, 6.07 >>
	enemy[9].head = 3.39 
	enemy[9].wpn = WEAPONTYPE_PISTOL
	enemy[9].model =S_M_Y_BlackOps_01
	
	enemy[10].loc = << 576.40, -3111.26, 9.88 >>
	enemy[10].head = 13.53
	enemy[10].wpn = WEAPONTYPE_UNARMED
	enemy[10].model=S_M_Y_BlackOps_01
	
	//dude in the control room
	enemy[11].loc = << 567.7729, -3126.2927, 17.7686 >>
	enemy[11].head = 183.7132 
	enemy[11].wpn = WEAPONTYPE_UNARMED
	enemy[11].model =S_M_Y_BlackOps_01
	
	//dock worker who gets stunned
	enemy[12].loc = << 620.5584, -3171.3035, 5.0687 >>
	enemy[12].head = 286.0132 
	enemy[12].wpn = WEAPONTYPE_UNARMED
	enemy[12].model =S_M_Y_BlackOps_01
	
	//MERRYWEATHER GUYS
	
	//1 of 2 chatting by steps
	enemy[13].loc = << 591.06, -3288.66, 18.72 >>
	enemy[13].head = 0.00
	enemy[13].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[13].model =S_M_Y_BlackOps_01
	
	//2 of 2 chatting by steps
	enemy[14].loc = << 591.47, -3287.71, 18.72 >>
	enemy[14].head = -178.76
	enemy[14].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[14].model =S_M_Y_BlackOps_01
	
	//AT TOP OF STEPS
	enemy[15].loc = << 596.5318, -3288.6238, 17.7143 >>
	enemy[15].head = 220.6964
	enemy[15].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[15].model =S_M_Y_BlackOps_01
	
	//INTERIOR ON WALKWAY 
	enemy[16].loc = << 512.5623, -3128.1653, 5.0696 >>
	enemy[16].head = 271.0057
	enemy[16].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[16].model =S_M_Y_BlackOps_01
	
	//WALKS INTO BUILDING
	enemy[17].loc = << 495.7888, -3142.5996, 5.0419 >>
	enemy[17].head = 9.4754 
	enemy[17].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[17].model =S_M_Y_BlackOps_01
	
	//INTERIOR TO BE STUN GUNNED
	enemy[18].loc = << 545.9113, -3157.1587, 5.0699 >>
	enemy[18].head = -89.95 
	enemy[18].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[18].model =S_M_Y_BlackOps_01
	
	//ENEMY ON WALKWAY
	enemy[19].loc = << 510.9347, -3163.1167, 5.1579 >>
	enemy[19].head = 279.8616
	enemy[19].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[19].model =S_M_Y_BlackOps_01
	
	//WORKER IN COMPUTER ROOM2 - FEMALE
	enemy[20].loc = << 566.8541, -3123.1355, 17.7686 >>
	enemy[20].head = 118.0292
	enemy[20].wpn = WEAPONTYPE_UNARMED
	enemy[20].model = S_M_Y_BlackOps_01
	
	//SPAWN WHILE TREV RAPPELS
	enemy[21].loc = << 547.5675, -3123.0691, 5.0699 >>
	enemy[21].head = 183.9746
	enemy[21].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[21].model =S_M_Y_BlackOps_01
	
	enemy[22].loc = << 549.0079, -3127.4082, 5.0701 >>
	enemy[22].head = 175.3740 
	enemy[22].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[22].model =S_M_Y_BlackOps_01
	
	//LOOKING OUT ONTO THE SEA
	enemy[23].loc = << 509.5300, -3337.7878, 5.0699 >>
	enemy[23].head = 188.8407
	enemy[23].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[23].model =S_M_Y_BlackOps_01
	
	//ENEMIES TO COMBAT MIKE
	
	//BEHIND CONTAINER
	enemy[24].loc = << 503.9503, -3128.2463, 5.0699 >>
	enemy[24].head = 278.3160
	enemy[24].wpn = WEAPONTYPE_ASSAULTSHOTGUN
	enemy[24].model =S_M_Y_BlackOps_01
	
	//ALREADY IN COVER
	enemy[25].loc = << 500.2828, -3132.6196, 5.0699 >>
	enemy[25].head = 272.6464
	enemy[25].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[25].model =S_M_Y_BlackOps_01
	
	enemy[26].loc = << 496.1426, -3123.9343, 5.0699 >>
	enemy[26].head = 269.1508
	enemy[26].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[26].model =S_M_Y_BlackOps_01
	
	enemy[27].loc = << 507.8749, -3124.6238, 5.0698 >>
	enemy[27].head = 271.4555
	enemy[27].wpn = WEAPONTYPE_ASSAULTSHOTGUN
	enemy[27].model =S_M_Y_BlackOps_01
	
	enemy[28].loc = << 496.6405, -3124.6785, 5.0699 >>
	enemy[28].head = 267.4851 
	enemy[28].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[28].model =S_M_Y_BlackOps_01
	
	enemy[29].loc = << 496.7787, -3121.9277, 5.0695 >>
	enemy[29].head = 353.9953
	enemy[29].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[29].model =S_M_Y_BlackOps_01
	
	enemy[30].loc = << 495.8472, -3121.9832, 5.0699 >>
	enemy[30].head = 359.6471
	enemy[30].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[30].model =S_M_Y_BlackOps_01
	
	//GUYS AT THE OTHER SIDE
	
	enemy[31].loc =  << 533.1688, -3119.3777, 11.9102 >>
	enemy[31].head = 274.4106
	enemy[31].wpn = WEAPONTYPE_ASSAULTSHOTGUN
	enemy[31].model =S_M_Y_BlackOps_01
	
	enemy[32].loc = << 480.1254, -3138.5391, 5.0695 >>
	enemy[32].head = 13.7647 
	enemy[32].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[32].model =S_M_Y_BlackOps_01
	
	enemy[33].loc = << 471.6332, -3123.8315, 5.0695 >>
	enemy[33].head = 13.7647 
	enemy[33].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[33].model =S_M_Y_BlackOps_01
	
	enemy[34].loc = << 469.1807, -3134.7798, 5.0695 >>
	enemy[34].head = 287.3908
	enemy[34].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[34].model =S_M_Y_BlackOps_01
	
	//RUNNING ALONG WALKWAYS ON RAPPELING
	
	enemy[35].loc = << 587.5251, -3119.0803, 17.7151 >>
	enemy[35].head = 182.4486
	enemy[35].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[35].model =S_M_Y_BlackOps_01
	
	enemy[36].loc = << 591.2130, -3131.0691, 16.3609 >>
	enemy[36].head = 90.3731
	enemy[36].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[36].model =S_M_Y_BlackOps_01
	
	enemy[37].loc = << 547.3473, -3139.2258, 5.0699 >>
	enemy[37].head = 180.3411  
	enemy[37].wpn = WEAPONTYPE_ASSAULTSHOTGUN
	enemy[37].model =S_M_Y_BlackOps_01
	
	//ADDITIONAL PEDS FOR SHOOTOUT
	
	enemy[38].loc = << 501.7275, -3119.6602, 5.0694 >>
	enemy[38].head =  269.4093
	enemy[38].wpn = WEAPONTYPE_ASSAULTSHOTGUN
	enemy[38].model =S_M_Y_BlackOps_01
	
	enemy[39].loc = << 520.2127, -3120.0176, 17.9144 >>
	enemy[39].head = 132.5665
	enemy[39].wpn = WEAPONTYPE_SNIPERRIFLE
	enemy[39].model =S_M_Y_BlackOps_01
	
	//PEDS ALREADY IN COVER - MIKE
	enemy[40].loc = << 528.5178, -3121.2793, 5.0694 >>
	enemy[40].head = 176.8235
	enemy[40].wpn = WEAPONTYPE_ASSAULTSHOTGUN
	enemy[40].model =S_M_Y_BlackOps_01
	
	enemy[41].loc = << 520.8710, -3126.4878, 5.0699 >>
	enemy[41].head =  2.4706
	enemy[41].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[41].model =S_M_Y_BlackOps_01
	
	//PEDS ALREADY IN COVER - FRANKLIN
	enemy[42].loc = << 498.9655, -3134.0022, 5.0699 >>
	enemy[42].head = 288.6624
	enemy[42].wpn = WEAPONTYPE_PISTOL
	enemy[42].model =S_M_Y_BlackOps_01
	
	enemy[43].loc = << 500.4205, -3172.6599, 5.0693 >>
	enemy[43].head = 269.2941
	enemy[43].wpn = WEAPONTYPE_ASSAULTSHOTGUN
	enemy[43].model =S_M_Y_BlackOps_01
	
	//DOCK WORKERS BY TRUCK
	enemy[44].loc = << 612.23, -3147.81, 6.11 >>
	enemy[44].head = 359.9166
	enemy[44].wpn = WEAPONTYPE_UNARMED
	enemy[44].model =S_M_Y_BlackOps_01
	
	enemy[45].loc = << 614.52, -3152.42, 6.07 >>
	enemy[45].head = 2.1169 
	enemy[45].wpn = WEAPONTYPE_UNARMED
	enemy[45].model=S_M_M_DOCKWORK_01
	
	//ADDITIONAL ENEMIES INSIDE
	//- PATROLLING - BY CRATES
	enemy[46].loc = << 513.22, -3123.82, 6.07 >>
	enemy[46].head = -94.54 
	enemy[46].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[46].model=S_M_Y_BLACKOPS_01
	
	// BY ENTRANCE - ON MONITORS
	enemy[47].loc = << 484.83, -3110.55, 6.11 >>
	enemy[47].head = 0.0
	enemy[47].wpn = WEAPONTYPE_PISTOL
	enemy[47].model=S_M_Y_BLACKOPS_01
	
	//- BY NEW VEHICLE - CHATTING
	enemy[48].loc = << 500.26, -3154.82, 6.07 >>
	enemy[48].head = 120.89
	enemy[48].wpn = WEAPONTYPE_ADVANCEDRIFLE
	enemy[48].model=S_M_Y_BLACKOPS_01
	
	// VEHICLE - CHAT
	enemy[49].loc = << 500.23, -3153.88, 6.07 >>
	enemy[49].head = 0.0
	enemy[49].wpn = WEAPONTYPE_PISTOL
	enemy[49].model=S_M_Y_BLACKOPS_01
	
	//EXTRA DOCK WORKERS
	enemy[50].loc = << 592.09, -3156.96, 6.07 >>
	enemy[50].head = -98.12
	enemy[50].wpn = WEAPONTYPE_UNARMED
	enemy[50].model =S_M_Y_BlackOps_01
	
	//DRIVING TRUCK
	enemy[51].loc = << 500.74, -3060.28, 5.11 >>
	enemy[51].head = 0.0
	enemy[51].wpn = WEAPONTYPE_UNARMED
	enemy[51].model =S_M_Y_BlackOps_01


	#IF IS_DEBUG_BUILD

		SkipMenuStruct[0].sTxtLabel = "Setup"
		SkipMenuStruct[0].bSelectable	= FALSE
		SkipMenuStruct[1].sTxtLabel = "Opening Mocap"
		SkipMenuStruct[1].bSelectable	= FALSE
		SkipMenuStruct[2].sTxtLabel = "Get to the airstrip"
		SkipMenuStruct[3].sTxtLabel = "Pick up the sub from airstrip"
		SkipMenuStruct[4].sTxtLabel = "Fly to the sea"
//		SkipMenuStruct[5].sTxtLabel = "Clear Merryweather from area"
		SkipMenuStruct[5].sTxtLabel = "Drop off sub"
		SkipMenuStruct[6].sTxtLabel = "Find the container"
		SkipMenuStruct[7].sTxtLabel = "Get to surface"
		SkipMenuStruct[8].sTxtLabel = "Pick up Sub"
		SkipMenuStruct[9].sTxtLabel = "Defend Against Merryweather"
		SkipMenuStruct[10].sTxtLabel = "Fly away"
		SkipMenuStruct[11].sTxtLabel = "Drop off sub"
	#ENDIF 

	PRINTSTRING("Data array init")
	PRINTNL()	

	
ENDPROC

//PURPOSE: creates and sets up the specified enemies
PROC CREATE_STEALTH_ENEMIES(INT first, INT last)
	
	FOR iCount = first TO last
		enemy[iCount].ped = CREATE_PED(PEDTYPE_MISSION, enemy[iCount].model, enemy[iCount].loc,enemy[iCount].head,TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(enemy[iCount].ped ,rel_group_enemies)
		GIVE_WEAPON_TO_PED(enemy[iCount].ped , enemy[iCount].wpn , 999, TRUE)  
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(enemy[iCount].ped,TRUE)
		SET_ENTITY_LOAD_COLLISION_FLAG(enemy[iCount].ped,TRUE)
		SET_PED_AS_ENEMY(enemy[iCount].ped , TRUE)
		SET_ENTITY_AS_MISSION_ENTITY(enemy[iCount].ped)
		SET_PED_COMBAT_MOVEMENT(enemy[iCount].ped, CM_DEFENSIVE)
		SET_PED_COMBAT_RANGE(enemy[iCount].ped, CR_FAR)
		SET_PED_COMBAT_ABILITY(enemy[iCount].ped, CAL_POOR)
		SET_PED_SEEING_RANGE(enemy[iCount].ped,15)
		SET_PED_HEARING_RANGE(enemy[iCount].ped,5)
		SET_ENTITY_HEALTH(enemy[iCount].ped,150)
		SET_PED_ACCURACY(enemy[iCount].ped,15)
		TEXT_LABEL_23 nameLabel = "Enemy "
		nameLabel += iCount
		SET_PED_NAME_DEBUG(enemy[iCount].ped ,nameLabel)
		PRINTSTRING("enemy spawned : ")
		PRINTINT(iCount)
		PRINTNL()	
	ENDFOR

ENDPROC																			


//PURPOSE: Pauses the speech if it should be

FUNC BOOL SHOULD_SPEECH_BE_PAUSED()

	
//		IF 	 Main_stages = MISSION_STAGES_GOTO_CARVANPARK
//			IF NOT IS_PED_INJURED(psBuddy[0].PedIndex)
//			
//			//Distance Check
//				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(psBuddy[0].PedIndex)) > CONVERSATION_PAUSE_DISTANCE
//					RETURN TRUE
//				ENDIF
//				
//			//Not in Vehicle Check
//				IF IS_VEHICLE_DRIVEABLE(vsPlayerVehicle.VehicleIndex)
//					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vsPlayerVehicle.VehicleIndex)
//					OR NOT IS_PED_IN_VEHICLE(psBuddy[0].PedIndex, vsPlayerVehicle.VehicleIndex)
//						RETURN TRUE
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			//Wanted check
//			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
//				RETURN TRUE
//			ENDIF
//		ENDIF
	
	RETURN FALSE
	
ENDFUNC
PROC CONTROL_SPEECH()
		
	//if the speech should be paused then pause it.
	IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		IF SHOULD_SPEECH_BE_PAUSED()
			IF	bPausedSpeech = FALSE
				PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
				bPausedSpeech = TRUE
			ENDIF
		ELSE 
			IF bPausedSpeech = TRUE
				PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
				bPausedSpeech = FALSE
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC
PROC MANAGE_PLAYER_WEAPONS()
	
	//TREVOR
//	IF NOT IS_PED_THE_CURRENT_PLAYER_PED(CHAR_TREVOR)
//		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//			IF NOT HAS_PED_GOT_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],WEAPONTYPE_ADVANCEDRIFLE)
//				GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],WEAPONTYPE_ADVANCEDRIFLE,300,FALSE)
//			ENDIF
//		ENDIF
//	ELSE
//		IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ADVANCEDRIFLE)
//			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_ADVANCEDRIFLE,300,FALSE)
//		ENDIF
//	ENDIF
//	
//	//FRANKLIN
//	IF NOT IS_PED_THE_CURRENT_PLAYER_PED(CHAR_FRANKLIN)
//		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//			IF NOT HAS_PED_GOT_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_ASSAULTRIFLE)
//				GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_ASSAULTRIFLE,300,FALSE)
//			ENDIF
//			IF NOT HAS_PED_GOT_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_RPG)
//				GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_RPG,-1,FALSE,FALSE)
//			ENDIF
//		ENDIF
//	ELSE
//		IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE)
//			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_ASSAULTRIFLE,300,FALSE)
//		ENDIF
//		IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_RPG)
//			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_RPG,-1,FALSE,FALSE)
//		ENDIF
//	ENDIF
//	
//	//MICHAEL
//	IF NOT IS_PED_THE_CURRENT_PLAYER_PED(CHAR_MICHAEL)
//		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//			IF NOT HAS_PED_GOT_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_PISTOL)
//				GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],WEAPONTYPE_PISTOL,100,TRUE)
//				GIVE_WEAPON_COMPONENT_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_SUPP_02)
//			ENDIF
//			IF NOT HAS_PED_GOT_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],WEAPONTYPE_CARBINERIFLE)
//				GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],WEAPONTYPE_CARBINERIFLE,300,FALSE)
//			ENDIF
//		ENDIF
//	ELSE
//		IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_PISTOL)
//			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_PISTOL,100,TRUE)
//			GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PISTOL, WEAPONCOMPONENT_AT_PI_SUPP_02)
//		ENDIF
//		IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_CARBINERIFLE)
//			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_CARBINERIFLE,300,FALSE)
//		ENDIF
//	ENDIF

ENDPROC


PROC CREATE_DOCK_PROPS_NEW()

	INT i
	
	IF NOT DOES_ENTITY_EXIST(objcrate[23])
	AND NOT DOES_ENTITY_EXIST(objcrate[22])
	AND NOT DOES_ENTITY_EXIST(objcrate[21])
	AND NOT DOES_ENTITY_EXIST(objcrate[20])
	AND NOT DOES_ENTITY_EXIST(objcrate[19])
	AND NOT DOES_ENTITY_EXIST(objcrate[18])
	AND NOT DOES_ENTITY_EXIST(objcrate[17])
	AND NOT DOES_ENTITY_EXIST(objcrate[16])
	AND NOT DOES_ENTITY_EXIST(objcrate[15])
	AND NOT DOES_ENTITY_EXIST(objcrate[14])
	AND NOT DOES_ENTITY_EXIST(objcrate[13])
	AND NOT DOES_ENTITY_EXIST(objcrate[12])
	AND NOT DOES_ENTITY_EXIST(objcrate[11])
	AND NOT DOES_ENTITY_EXIST(objcrate[10])
	AND NOT DOES_ENTITY_EXIST(objcrate[9])
	AND NOT DOES_ENTITY_EXIST(objcrate[8])
	AND NOT DOES_ENTITY_EXIST(objcrate[7])
	AND NOT DOES_ENTITY_EXIST(objcrate[6])
	AND NOT DOES_ENTITY_EXIST(objcrate[5])
	AND NOT DOES_ENTITY_EXIST(objcrate[4])
	AND NOT DOES_ENTITY_EXIST(objcrate[3])
	AND NOT DOES_ENTITY_EXIST(objcrate[2])
	//AND NOT DOES_ENTITY_EXIST(objcrate[1])
	AND NOT DOES_ENTITY_EXIST(objcrate[0])
	AND NOT DOES_ENTITY_EXIST(mission_veh[14].veh)
	AND NOT DOES_ENTITY_EXIST(mission_veh[15].veh)
	AND NOT DOES_ENTITY_EXIST(mission_veh[19].veh)
	AND NOT DOES_ENTITY_EXIST(mission_veh[20].veh)
	AND NOT DOES_ENTITY_EXIST(mission_veh[21].veh)
	AND NOT DOES_ENTITY_EXIST(mission_veh[22].veh)
	AND NOT DOES_ENTITY_EXIST(mission_veh[23].veh)
		REQUEST_MODEL(Prop_BoxPile_09A)
		REQUEST_MODEL(Prop_Container_old1 )
		REQUEST_MODEL(Prop_Container_03b)
		REQUEST_MODEL(Prop_CS_Fertilizer)
		REQUEST_MODEL(Prop_LD_Crate_01)
		REQUEST_MODEL(PROP_BOXPILE_02B)
		REQUEST_MODEL(PROP_BOXPILE_02C)
		REQUEST_MODEL(Prop_BoxPile_04A)
		REQUEST_MODEL(BIFF)
		//REQUEST_MODEL(prop_ld_container)
		REQUEST_MODEL(PROP_BOXPILE_05A)
	ENDIF
	
	REQUEST_MODEL(PROP_BOX_WOOD01A)
	REQUEST_MODEL(Prop_LD_Crate_01)
	REQUEST_MODEL(PROP_BOXPILE_02C)
	REQUEST_MODEL(PROP_BOXPILE_02B)
	REQUEST_MODEL(Prop_CS_Fertilizer)
	REQUEST_MODEL(Prop_Container_old1)
	REQUEST_MODEL(BIFF)
	REQUEST_MODEL(FORKLIFT)
	REQUEST_MODEL(Prop_BoxPile_04A)
	REQUEST_MODEL(PROP_BOXPILE_05A)
	REQUEST_MODEL(Prop_Container_03b)

	IF HAS_MODEL_LOADED(Prop_LD_Crate_01) 
	AND HAS_MODEL_LOADED(PROP_BOXPILE_02C) 
	AND HAS_MODEL_LOADED(PROP_BOXPILE_02B)
	AND HAS_MODEL_LOADED(PROP_BOX_WOOD01A) 
	AND HAS_MODEL_LOADED(Prop_CS_Fertilizer) 
	AND HAS_MODEL_LOADED(Prop_Container_03b)
	AND HAS_MODEL_LOADED(Prop_BoxPile_04A)
	AND HAS_MODEL_LOADED(BIFF) 
	AND HAS_MODEL_LOADED(Prop_Container_old1)
	AND HAS_MODEL_LOADED(PROP_BOXPILE_05A)
	
		IF HAS_MODEL_LOADED(BIFF) 
			IF NOT DOES_ENTITY_EXIST(mission_veh[14].veh)
				mission_veh[14].veh = CREATE_VEHICLE(BIFF,<< 502.8150, -3180.4001, 5.0417 >>)
				SET_ENTITY_HEADING(mission_veh[14].veh,268.0708 )
				SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[14].veh)
				SET_VEHICLE_EXTRA(mission_veh[14].veh,1,TRUE)
				SET_VEHICLE_EXTRA(mission_veh[14].veh,2,TRUE)
				SET_VEHICLE_EXTRA(mission_veh[14].veh,3,TRUE)
				SET_VEHICLE_EXTRA(mission_veh[14].veh,4,TRUE)
				SET_VEHICLE_EXTRA(mission_veh[14].veh,5,TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[14].veh,TRUE)
				SET_VEHICLE_DOORS_LOCKED(mission_veh[14].veh,VEHICLELOCK_LOCKED)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(BIFF) 
			IF NOT DOES_ENTITY_EXIST(mission_veh[15].veh)
				mission_veh[15].veh = CREATE_VEHICLE(BIFF,<< 502.7802, -3174.0110, 5.0417 >>)
				SET_ENTITY_HEADING(mission_veh[15].veh,269.3149 )
				SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[15].veh)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[15].veh,TRUE)
				SET_VEHICLE_DOORS_LOCKED(mission_veh[15].veh,VEHICLELOCK_LOCKED)
			ENDIF
		ENDIF
		
		//CAR GENS
		IF HAS_MODEL_LOADED(BIFF) 
			IF NOT DOES_ENTITY_EXIST(mission_veh[20].veh)
				mission_veh[20].veh = CREATE_VEHICLE(BIFF,<<609.307678,-3117.964111,6.437520>>)
				SET_ENTITY_HEADING(mission_veh[20].veh,90.527306 )
				SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[20].veh)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[20].veh,TRUE)
				SET_VEHICLE_DOORS_LOCKED(mission_veh[20].veh,VEHICLELOCK_LOCKED)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(FORKLIFT) 
			IF NOT DOES_ENTITY_EXIST(mission_veh[21].veh)
				mission_veh[21].veh = CREATE_VEHICLE(FORKLIFT,<<608.018738,-3130.480469,5.582871>>)
				SET_ENTITY_HEADING(mission_veh[21].veh,105.997078 )
				SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[21].veh)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[21].veh,TRUE)
				SET_VEHICLE_DOORS_LOCKED(mission_veh[21].veh,VEHICLELOCK_LOCKED)
			ENDIF
		ENDIF
		
		//BIFF AT START
		IF HAS_MODEL_LOADED(BIFF) 
			IF NOT DOES_ENTITY_EXIST(mission_veh[19].veh)
				mission_veh[19].veh = CREATE_VEHICLE(BIFF,<<606.939453,-3150.961426,6.170882>>, 98.711838)
				SET_ENTITY_HEADING(mission_veh[19].veh,98.711838 )
				SET_VEHICLE_EXTRA(mission_veh[19].veh,1,TRUE)
				SET_VEHICLE_EXTRA(mission_veh[19].veh,2,TRUE)
				SET_VEHICLE_EXTRA(mission_veh[19].veh,3,TRUE)
				SET_VEHICLE_EXTRA(mission_veh[19].veh,4,TRUE)
				SET_VEHICLE_EXTRA(mission_veh[19].veh,5,TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[19].veh)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[19].veh,TRUE)
				SET_VEHICLE_DOORS_LOCKED(mission_veh[19].veh,VEHICLELOCK_LOCKED)
			ENDIF
		ENDIF
		
		//VEHICLE INSIDE WITH ENEMIES AROUND IT
		IF HAS_MODEL_LOADED(BIFF) 
			IF NOT DOES_ENTITY_EXIST(mission_veh[22].veh)
				mission_veh[22].veh = CREATE_VEHICLE(BIFF,<<502.86, -3155.49, 6.15>>, 272.34)
				SET_ENTITY_HEADING(mission_veh[22].veh,272.34 )
				SET_VEHICLE_EXTRA(mission_veh[22].veh,1,TRUE)
				SET_VEHICLE_EXTRA(mission_veh[22].veh,2,TRUE)
				SET_VEHICLE_EXTRA(mission_veh[22].veh,3,TRUE)
				SET_VEHICLE_EXTRA(mission_veh[22].veh,4,TRUE)
				SET_VEHICLE_EXTRA(mission_veh[22].veh,5,TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[22].veh)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[22].veh,TRUE)
				SET_VEHICLE_DOORS_LOCKED(mission_veh[22].veh,VEHICLELOCK_LOCKED)
			ENDIF
		ENDIF
		
		//CONTAINER WITH BARRELS ETC
		IF HAS_MODEL_LOADED(Prop_BoxPile_09A) 
			IF NOT DOES_ENTITY_EXIST(objcrate[0])
				objcrate[0] = CREATE_OBJECT(Prop_BoxPile_09A,<< 614.8198, -3198.2659, 5.079 >>)
				SET_ENTITY_HEADING(objcrate[0],93.2706)
				//FREEZE_ENTITY_POSITION(objcrate[0],TRUE)
			ENDIF
		ENDIF

		IF HAS_MODEL_LOADED(PROP_BOXPILE_02C) 
			IF NOT DOES_ENTITY_EXIST(objcrate[1])
				objcrate[1] =CREATE_OBJECT_NO_OFFSET(PROP_BOXPILE_02C,<<607.369812,-3123.757568,5.079>>)
				SET_ENTITY_HEADING(objcrate[1],105.997078)
				FREEZE_ENTITY_POSITION(objcrate[1],TRUE)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(Prop_Container_old1 ) 
			IF NOT DOES_ENTITY_EXIST(objcrate[2])
				objcrate[2] = CREATE_OBJECT(Prop_Container_old1, <<616.3440, -3176.0281, 5.1065>>)
				SET_ENTITY_HEADING(objcrate[2],-3.4000)
				FREEZE_ENTITY_POSITION(objcrate[2],TRUE)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(PROP_BOXPILE_02B) 
			IF NOT DOES_ENTITY_EXIST(objcrate[3])
				objcrate[3] = CREATE_OBJECT_NO_OFFSET(PROP_BOXPILE_02B,<< 611.3243, -3069.5320, 5.079 >>)
				SET_ENTITY_HEADING(objcrate[3],268.4050)
				FREEZE_ENTITY_POSITION(objcrate[3],TRUE)
			ENDIF
		ENDIF
		
		//BLUE SHIPPING CONTAINER - switch to once exported
		IF HAS_MODEL_LOADED(Prop_Container_03b ) 
			IF NOT DOES_ENTITY_EXIST(objcrate[4])
				objcrate[4] = CREATE_OBJECT_NO_OFFSET(Prop_Container_03b ,<<613.6138, -3203.8779, 5.0790>>)
				SET_ENTITY_HEADING(objcrate[4],93.2400)
				FREEZE_ENTITY_POSITION(objcrate[4],TRUE)
			ENDIF
		ENDIF
		
		//USED TO BE BY DOOR - NOW INSIDE
		IF HAS_MODEL_LOADED(PROP_BOXPILE_02B) 
			IF NOT DOES_ENTITY_EXIST(objcrate[5])
				objcrate[5] = CREATE_OBJECT_NO_OFFSET(PROP_BOXPILE_02B,<<577.1465, -3127.7300, 5.0790>>)
				SET_ENTITY_HEADING(objcrate[5],5.3000)
				FREEZE_ENTITY_POSITION(objcrate[5],TRUE)
			ENDIF
		ENDIF
		
		//INSIDE FOR FIRE FIGHT
		
		IF HAS_MODEL_LOADED(Prop_LD_Crate_01) 
			IF NOT DOES_ENTITY_EXIST(objcrate[6])
				objcrate[6] = CREATE_OBJECT(Prop_LD_Crate_01,<<521.700745,-3127.104004,5.1740>>)
				SET_ENTITY_HEADING(objcrate[6],92.699989)
				FREEZE_ENTITY_POSITION(objcrate[6],TRUE)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(PROP_BOXPILE_02C) 
			IF NOT DOES_ENTITY_EXIST(objcrate[7])
				objcrate[7] = CREATE_OBJECT_NO_OFFSET(PROP_BOXPILE_02C,<<515.0721, -3121.4580, 5.1740>>)
				SET_ENTITY_HEADING(objcrate[7],6.0000)
				FREEZE_ENTITY_POSITION(objcrate[7],TRUE)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(Prop_LD_Crate_01) 
			IF NOT DOES_ENTITY_EXIST(objcrate[8])
				objcrate[8] = CREATE_OBJECT(Prop_LD_Crate_01,<<478.8166, -3141.2300, 5.1740>>)
				SET_ENTITY_HEADING(objcrate[8],-6.1)
				FREEZE_ENTITY_POSITION(objcrate[8],TRUE)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(PROP_BOXPILE_02B) 
			IF NOT DOES_ENTITY_EXIST(objcrate[9])
				objcrate[9] = CREATE_OBJECT_NO_OFFSET(PROP_BOXPILE_02B,<<496.8888, -3141.9624, 5.1940>>)
				SET_ENTITY_HEADING(objcrate[9],9.9000)
				FREEZE_ENTITY_POSITION(objcrate[9],TRUE)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(Prop_LD_Crate_01) 
			IF NOT DOES_ENTITY_EXIST(objcrate[10])
				objcrate[10] = CREATE_OBJECT(Prop_LD_Crate_01,<<529.3556, -3120.6990, 5.1740>>)
				SET_ENTITY_HEADING(objcrate[10],87.3000)
				FREEZE_ENTITY_POSITION(objcrate[10],TRUE)
			ENDIF
		ENDIF
		
		//For Shootout - Trevor
		IF HAS_MODEL_LOADED(PROP_BOXPILE_02B) 
			IF NOT DOES_ENTITY_EXIST(objcrate[11])
				objcrate[11] = CREATE_OBJECT_NO_OFFSET(PROP_BOXPILE_02B,<<551.6595, -3153.6321, 5.1915>>)
				SET_ENTITY_HEADING(objcrate[11],0)
				FREEZE_ENTITY_POSITION(objcrate[11],TRUE)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(PROP_BOXPILE_02C) 
			IF NOT DOES_ENTITY_EXIST(objcrate[12])
				objcrate[12] = CREATE_OBJECT_NO_OFFSET(PROP_BOXPILE_02C,<<547.654785,-3159.545898,5.069335>>)
				SET_ENTITY_HEADING(objcrate[12],6)
				FREEZE_ENTITY_POSITION(objcrate[12],TRUE)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(PROP_BOXPILE_02B) 
			IF NOT DOES_ENTITY_EXIST(objcrate[13])
				objcrate[13] = CREATE_OBJECT_NO_OFFSET(PROP_BOXPILE_02B,<<550.4972, -3165.8770, 5.0721>>)
				SET_ENTITY_HEADING(objcrate[13],-93.6000)
				FREEZE_ENTITY_POSITION(objcrate[13],TRUE)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(Prop_LD_Crate_01) 
			IF NOT DOES_ENTITY_EXIST(objcrate[14])
				objcrate[14] = CREATE_OBJECT(Prop_LD_Crate_01,<<547.6868, -3131.8950, 5.0721>>)
				SET_ENTITY_HEADING(objcrate[14],0)
				FREEZE_ENTITY_POSITION(objcrate[14],TRUE)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(PROP_BOXPILE_02B) 
			IF NOT DOES_ENTITY_EXIST(objcrate[15])
				objcrate[15] = CREATE_OBJECT_NO_OFFSET(PROP_BOXPILE_02B,<<498.894287,-3163.023438,5.193835>>)
				SET_ENTITY_HEADING(objcrate[15],0)
				//FREEZE_ENTITY_POSITION(objcrate[15],TRUE)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(Prop_LD_Crate_01) 
			IF NOT DOES_ENTITY_EXIST(objcrate[16])
				objcrate[16] = CREATE_OBJECT(Prop_LD_Crate_01,<<536.7913, -3127.9526, 5.0721>>)
				SET_ENTITY_HEADING(objcrate[16],89.9000)
				FREEZE_ENTITY_POSITION(objcrate[16],TRUE)
			ENDIF
		ENDIF
		
		//BOXES ON BIFF
		IF HAS_MODEL_LOADED(Prop_CS_Fertilizer) 
			IF NOT DOES_ENTITY_EXIST(objcrate[17])
				objcrate[17] = CREATE_OBJECT_NO_OFFSET(Prop_CS_Fertilizer,<<610.3470, -3151.0942, 6.8859>>)
				SET_ENTITY_HEADING(objcrate[17],72.9000)
				FREEZE_ENTITY_POSITION(objcrate[17],TRUE)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(Prop_CS_Fertilizer) 
			IF NOT DOES_ENTITY_EXIST(objcrate[18])
				objcrate[18] = CREATE_OBJECT_NO_OFFSET(Prop_CS_Fertilizer,<<610.0722, -3149.8005, 6.9057>>)
				SET_ENTITY_HEADING(objcrate[18],120.9000)
				FREEZE_ENTITY_POSITION(objcrate[18],TRUE)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(Prop_CS_Fertilizer) 
			IF NOT DOES_ENTITY_EXIST(objcrate[19])
				objcrate[19] = CREATE_OBJECT_NO_OFFSET(Prop_CS_Fertilizer,<<609.7397, -3150.5525, 6.9000>>)
				SET_ENTITY_HEADING(objcrate[19],89.9000)
				FREEZE_ENTITY_POSITION(objcrate[19],TRUE)
			ENDIF
		ENDIF
		
		//BY CONTAINER
		IF HAS_MODEL_LOADED(Prop_CS_Fertilizer) 
			IF NOT DOES_ENTITY_EXIST(objcrate[20])
				objcrate[20] = CREATE_OBJECT_NO_OFFSET(Prop_CS_Fertilizer,<<614.7304, -3197.1174, 5.5501>>)
				SET_ENTITY_HEADING(objcrate[20],82.8000)
				//FREEZE_ENTITY_POSITION(objcrate[20],TRUE)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(Prop_CS_Fertilizer) 
			IF NOT DOES_ENTITY_EXIST(objcrate[21])
				objcrate[21] = CREATE_OBJECT_NO_OFFSET(Prop_CS_Fertilizer,<<614.1232, -3197.1411, 5.5501>>)
				SET_ENTITY_HEADING(objcrate[21],2.8000)
				//FREEZE_ENTITY_POSITION(objcrate[20],TRUE)
			ENDIF
		ENDIF
		
		//NEW STORAGE
		
		IF HAS_MODEL_LOADED(Prop_Container_old1) 
			IF NOT DOES_ENTITY_EXIST(objcrate[22])
				objcrate[22] = CREATE_OBJECT_NO_OFFSET(Prop_Container_old1,<<616.5203, -3167.3840, 6.4839>>)
				SET_ENTITY_HEADING(objcrate[22],2.8000)
				FREEZE_ENTITY_POSITION(objcrate[22],TRUE)
			ENDIF
		ENDIF
		
		IF HAS_MODEL_LOADED(Prop_BoxPile_09A) 
			IF NOT DOES_ENTITY_EXIST(objcrate[23])
				objcrate[23] = CREATE_OBJECT_NO_OFFSET(Prop_BoxPile_09A,<<611.8642, -3150.5791, 5.1005>>)
				SET_ENTITY_HEADING(objcrate[23],15.4000)
				FREEZE_ENTITY_POSITION(objcrate[23],TRUE)
			ENDIF
		ENDIF
		
	//<<610.17, -3151.11, 6.42>>
	//<<610.02, -3149.74, 6.44>>
	//<<608.65, -3150.09, 6.45>>
	
	//PROP_BOX_WOOD01A
		
		FOR i = 0 TO MAX_NUMBER_OF_DOCK_PROPS - 1
//			IF DOES_ENTITY_EXIST(objcrate[i])
//				SET_ACTIVATE_OBJECT_PHYSICS_AS_SOON_AS_IT_IS_UNFROZEN(objcrate[i] ,TRUE)
//			ENDIF
		ENDFOR
		
		
	ELSE
		REQUEST_MODEL(Prop_LD_Crate_01)
		REQUEST_MODEL(PROP_BOXPILE_02C)
		REQUEST_MODEL(PROP_BOXPILE_02B)
	ENDIF

//	FOR iCount = 0 TO MAX_NUMBER_OF_DOCK_PROPS - 1
//		IF DOES_ENTITY_EXIST(objcrate[iCount])
//			FREEZE_ENTITY_POSITION(objcrate[iCount],TRUE)
//		ENDIF
//	ENDFOR
																				
ENDPROC

PROC GIVE_ALL_PEDS_AN_EARPIECE(BOOL bremove = false)
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
	AND NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		IF NOT bremove
			SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
		ELSE
			REMOVE_PED_COMP_ITEM_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_PROPS, PROPS_P0_HEADSET)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
	AND NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		IF NOT bremove
			SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], COMP_TYPE_PROPS, PROPS_P1_HEADSET)
		ELSE
			REMOVE_PED_COMP_ITEM_SP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], COMP_TYPE_PROPS, PROPS_P1_HEADSET)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
	AND NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		IF NOT bremove
			SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_PROPS, PROPS_P2_HEADSET)
		ELSE
			REMOVE_PED_COMP_ITEM_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_PROPS, PROPS_P2_HEADSET)
		ENDIF
	ENDIF
	
ENDPROC

VECTOR vSubmarineCoor

FUNC BOOL SETUP_MISSION_REQUIREMENT(MISSION_REQUIREMENT eRequirement, VECTOR vPos, FLOAT fHeading = 0.0, BOOL bJustRequestAssets = FALSE, BOOL bWithOutfit = TRUE)

	IF NOT IS_VECTOR_ZERO(vPos)
	
	ENDIF
	
	IF fHeading !=0
	
	ENDIF

	SWITCH eRequirement
	
		CASE REQ_TREVORS_TRUCK
			IF NOT DOES_ENTITY_EXIST(mission_veh[ENUM_TO_INT(MV_TREVORS_TRUCK)].veh)
				REQUEST_VEHICLE_ASSET(BODHI2)
				CREATE_PLAYER_VEHICLE(mission_veh[ENUM_TO_INT(MV_TREVORS_TRUCK)].veh, CHAR_TREVOR ,vPos, fHeading, TRUE)
			ELSE
				REQUEST_VEHICLE_ASSET(BODHI2)
				IF HAS_VEHICLE_ASSET_LOADED(BODHI2)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_TREVORS_TRUCK)].veh)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[ENUM_TO_INT(MV_TREVORS_TRUCK)].veh,TRUE)
						SET_VEHICLE_DISABLE_TOWING(mission_veh[ENUM_TO_INT(MV_TREVORS_TRUCK)].veh,TRUE)
						SET_VEHICLE_AUTOMATICALLY_ATTACHES(mission_veh[ENUM_TO_INT(MV_TREVORS_TRUCK)].veh,FALSE)
						SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[ENUM_TO_INT(MV_TREVORS_TRUCK)].veh)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_MICHAELS_CAR
			IF bJustRequestAssets
				REQUEST_VEHICLE_ASSET(TAILGATER)
				REQUEST_MODEL(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
			ENDIF
			REQUEST_VEHICLE_ASSET(TAILGATER)
			REQUEST_MODEL(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
			IF HAS_MODEL_LOADED(TAILGATER)
			AND HAS_VEHICLE_ASSET_LOADED(TAILGATER)
				IF NOT DOES_ENTITY_EXIST(mission_veh[ENUM_TO_INT(MV_MICHAELS_CAR)].veh)
					REQUEST_VEHICLE_ASSET(TAILGATER)
					CREATE_PLAYER_VEHICLE(mission_veh[ENUM_TO_INT(MV_MICHAELS_CAR)].veh, CHAR_MICHAEL ,vPos, fHeading, TRUE)				
				ELSE
					REQUEST_VEHICLE_ASSET(TAILGATER)
					IF HAS_VEHICLE_ASSET_LOADED(TAILGATER)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_MICHAELS_CAR)].veh)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[ENUM_TO_INT(MV_MICHAELS_CAR)].veh,TRUE)
							SET_VEHICLE_DISABLE_TOWING(mission_veh[ENUM_TO_INT(MV_MICHAELS_CAR)].veh,TRUE)
							SET_VEHICLE_HAS_STRONG_AXLES(mission_veh[ENUM_TO_INT(MV_MICHAELS_CAR)].veh,TRUE)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_FRANKLINS_CAR
			IF bJustRequestAssets
				REQUEST_VEHICLE_ASSET(BUFFALO)
				REQUEST_MODEL(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN))
			ENDIF
			REQUEST_VEHICLE_ASSET(BUFFALO)
			REQUEST_MODEL(GET_PLAYER_VEH_MODEL(CHAR_FRANKLIN))
			IF HAS_MODEL_LOADED(BUFFALO)
			AND HAS_VEHICLE_ASSET_LOADED(BUFFALO)
				IF NOT DOES_ENTITY_EXIST(mission_veh[ENUM_TO_INT(MV_FRANKLINS_CAR)].veh)
					REQUEST_VEHICLE_ASSET(BUFFALO)
					CREATE_PLAYER_VEHICLE(mission_veh[ENUM_TO_INT(MV_FRANKLINS_CAR)].veh, CHAR_FRANKLIN ,vPos, fHeading, TRUE)				
				ELSE
					REQUEST_VEHICLE_ASSET(BUFFALO)
					IF HAS_VEHICLE_ASSET_LOADED(BUFFALO)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_FRANKLINS_CAR)].veh)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[ENUM_TO_INT(MV_FRANKLINS_CAR)].veh,TRUE)
							SET_VEHICLE_DISABLE_TOWING(mission_veh[ENUM_TO_INT(MV_FRANKLINS_CAR)].veh,TRUE)
							SET_VEHICLE_HAS_STRONG_AXLES(mission_veh[ENUM_TO_INT(MV_FRANKLINS_CAR)].veh,TRUE)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_SUB_TRUCK
			IF NOT DOES_ENTITY_EXIST(mission_veh[ENUM_TO_INT(MV_PACKER)].veh)
				REQUEST_MODEL(PACKER)
				IF HAS_MODEL_LOADED(PACKER)
					mission_veh[ENUM_TO_INT(MV_PACKER)].veh = CREATE_VEHICLE(packer,<<1731.78, 3443.08, 38.54>>, 205.99)
					SET_VEHICLE_COLOUR_COMBINATION(mission_veh[ENUM_TO_INT(MV_PACKER)].veh,0)
					REQUEST_COLLISION_FOR_MODEL(PACKER)
					REQUEST_COLLISION_AT_COORD(<<1731.78, 3443.08, 38.54>>)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[ENUM_TO_INT(MV_PACKER)].veh,FALSE)
					SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[ENUM_TO_INT(MV_PACKER)].veh)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[ENUM_TO_INT(MV_PACKER)].veh,TRUE)
					ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[ENUM_TO_INT(MV_PACKER)].veh,"DH_2B_TRUCK_GROUP")
					
					
				ENDIF
			ELSE
				IF NOT DOES_ENTITY_EXIST(mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh)
					REQUEST_MODEL(ARMYTRAILER)
					IF HAS_MODEL_LOADED(ARMYTRAILER)
						mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh = CREATE_VEHICLE(ARMYTRAILER,<<1728.12, 3450.66, 40.74>>, 205.34)
						REQUEST_COLLISION_FOR_MODEL(ARMYTRAILER)
						REQUEST_COLLISION_AT_COORD(<<1728.12, 3450.66, 40.74>>)
						SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh)
						SET_VEHICLE_COLOUR_COMBINATION(mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh,0)
					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_PACKER)].veh)
					AND IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh)
						IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(mission_veh[ENUM_TO_INT(MV_PACKER)].veh)
							ATTACH_VEHICLE_TO_TRAILER(mission_veh[ENUM_TO_INT(MV_PACKER)].veh,mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh)
						ELSE
							SET_VEHICLE_AUTOMATICALLY_ATTACHES(mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh,FALSE)
							SET_VEHICLE_AUTOMATICALLY_ATTACHES(mission_veh[ENUM_TO_INT(MV_PACKER)].veh,FALSE)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_SUB_ON_TRUCK
			IF SETUP_MISSION_REQUIREMENT(REQ_SUB_TRUCK,vPos, fHeading)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh)
					IF SETUP_MISSION_REQUIREMENT(REQ_SUBMERSIBLE,<<1729.84, 3447.04, 41.36>>,205.41)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_PACKER)].veh)
						AND IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh)
						AND IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh)
							IF NOT IS_ENTITY_ATTACHED(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh)
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh,FALSE) 
								ATTACH_VEHICLE_ON_TO_TRAILER(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh,mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh,<<0,-4,-0.7>>,<<0,0,0>>,<<0,0,0>>,-1)
							ELSE
								ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh,"DH_2B_SUBMARINE_GROUP")
								//SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[ENUM_TO_INT(MV_PACKER)].veh,FALSE)
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_SUB_TRUCK_ON_RUNWAY
			IF NOT DOES_ENTITY_EXIST(mission_veh[ENUM_TO_INT(MV_PACKER)].veh)
				REQUEST_MODEL(PACKER)
				IF HAS_MODEL_LOADED(PACKER)
					mission_veh[ENUM_TO_INT(MV_PACKER)].veh = CREATE_VEHICLE(packer,<<1728.96, 3264.86, 41.22>>, 116.40)
					SET_VEHICLE_COLOUR_COMBINATION(mission_veh[ENUM_TO_INT(MV_PACKER)].veh,0)
					REQUEST_COLLISION_FOR_MODEL(PACKER)
					REQUEST_COLLISION_AT_COORD(<<1728.96, 3264.86, 41.22>>)
					//SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[ENUM_TO_INT(MV_PACKER)].veh,FALSE)
					SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[ENUM_TO_INT(MV_PACKER)].veh)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[ENUM_TO_INT(MV_PACKER)].veh,TRUE)
				ENDIF
			ELSE
				IF NOT DOES_ENTITY_EXIST(mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh)
					REQUEST_MODEL(ARMYTRAILER)
					IF HAS_MODEL_LOADED(ARMYTRAILER)
						mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh = CREATE_VEHICLE(ARMYTRAILER,<<1736.60, 3268.56, 43.06>>, 115.39)
						REQUEST_COLLISION_FOR_MODEL(ARMYTRAILER)
						REQUEST_COLLISION_AT_COORD(<<1728.96, 3264.86, 41.22>>)
						SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh)
						SET_VEHICLE_COLOUR_COMBINATION(mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh,0)
					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_PACKER)].veh)
					AND IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh)
						IF NOT IS_VEHICLE_ATTACHED_TO_TRAILER(mission_veh[ENUM_TO_INT(MV_PACKER)].veh)
							ATTACH_VEHICLE_TO_TRAILER(mission_veh[ENUM_TO_INT(MV_PACKER)].veh,mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh)
						ELSE
							
							SET_VEHICLE_AUTOMATICALLY_ATTACHES(mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh,FALSE)
							SET_VEHICLE_AUTOMATICALLY_ATTACHES(mission_veh[ENUM_TO_INT(MV_PACKER)].veh,FALSE)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_SUB_ON_TRUCK_ON_RUNWAY
			IF SETUP_MISSION_REQUIREMENT(REQ_SUB_TRUCK_ON_RUNWAY,vPos, fHeading)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh)
					IF SETUP_MISSION_REQUIREMENT(REQ_SUBMERSIBLE,<<1733.00, 3266.85, 43.81>>,115.47)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_PACKER)].veh)
						AND IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh)
						AND IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh)
							IF NOT IS_ENTITY_ATTACHED(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh)
								ATTACH_VEHICLE_ON_TO_TRAILER(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh,mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh,<<0,-4,-0.7>>,<<0,0,0>>,<<0,0,0>>,-1)
							ELSE
								ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh,"DH_2B_SUBMARINE_GROUP")
								//SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[ENUM_TO_INT(MV_PACKER)].veh,FALSE)
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_SUB_ON_TRUCK_WITH_TARP
			IF SETUP_MISSION_REQUIREMENT(REQ_SUB_TRUCK,<<0,0,0>>)
				IF SETUP_MISSION_REQUIREMENT(REQ_SUBMERSIBLE,<<1732.5760, 3270.5779, 44.1069>>,139.4393)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_PACKER)].veh)
					AND IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh)
					AND IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh)
						IF NOT IS_ENTITY_ATTACHED(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh)
							
							ATTACH_VEHICLE_ON_TO_TRAILER(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh,mission_veh[ENUM_TO_INT(MV_ARMY_TRAILER)].veh,<<0,-4,-0.7>>,<<0,0,0>>,<<0,0,0>>,-1)
						ELSE
							IF NOT DOES_ENTITY_EXIST(objMission[MO_TARP])
								REQUEST_MODEL(prop_sub_cover_01)
								IF HAS_MODEL_LOADED(prop_sub_cover_01)
									objMission[MO_TARP] = create_object(prop_sub_cover_01,<<1732.5760, 3270.5779, 54.1069>>)
									ATTACH_ENTITY_TO_ENTITY(objMission[MO_TARP],mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh,0,<<-0.02,-2.38,0.17>>,<<0,0,0>>)
								ENDIF
							ELSE
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_MICHAEL
			IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],CHAR_MICHAEL,vPos,fHeading)
			ELSE
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] ,rel_group_buddies)
					//SET_ENTITY_INVINCIBLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],TRUE)
					//SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH, FALSE)
					//SET_PED_COMPONENT_VARIATION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],PED_COMP_SPECIAL,0,0,0)
					IF bWithOutfit
						PRELOAD_OUTFIT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], OUTFIT_P0_STEALTH_NO_MASK)
						SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH_NO_MASK, FALSE)
						CLEAR_PED_PROP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],ANCHOR_EYES)
					ENDIF
					
				ENDIF
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_FRANKLIN
			IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],CHAR_FRANKLIN,vPos,fHeading)
			ELSE
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] ,rel_group_buddies)
					//SET_ENTITY_INVINCIBLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
					IF bWithOutfit
						PRELOAD_OUTFIT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], OUTFIT_P1_STEALTH_NO_MASK)
						SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], COMP_TYPE_OUTFIT, OUTFIT_P1_STEALTH_NO_MASK, FALSE)
					ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_TREVOR
			IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],CHAR_TREVOR,vPos,fHeading)
			ELSE
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR] ,rel_group_buddies)
					//SET_ENTITY_INVINCIBLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRUE)
					SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],KNOCKOFFVEHICLE_NEVER)
					IF bWithOutfit
						PRELOAD_OUTFIT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], OUTFIT_P2_STEALTH_NO_MASK)
						SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_OUTFIT, OUTFIT_P2_STEALTH_NO_MASK, FALSE)
						CLEAR_PED_PROP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],ANCHOR_HEAD)
						CLEAR_PED_PROP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],ANCHOR_EYES)
					ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_LESTER_FOR_CUTSCENE
			IF NOT DOES_ENTITY_EXIST(pedLester)
				IF CREATE_NPC_PED_ON_FOOT(pedLester, CHAR_LESTER, <<1743.23, 3295.97, 41.11>>, -136.36)
					SET_PED_CAN_BE_TARGETTED(pedLester, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedLester, TRUE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedLester, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedLester, RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedLester ,rel_group_buddies)
					//SET_ENTITY_PROOFS(pedWade, FALSE, FALSE, FALSE, TRUE, FALSE)
					SET_PED_AS_ENEMY(pedLester , FALSE)
					SET_ENTITY_AS_MISSION_ENTITY(pedLester)
					//PRINTLN("CREATED WADE")//PRINTNL()
					RETURN TRUE
				ENDIF
			ELSE
				IF NOT IS_PED_INJURED(pedLester)
					SET_PED_CAN_BE_TARGETTED(pedLester, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedLester, TRUE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedLester, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedLester, RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedLester ,rel_group_buddies)
					SET_PED_AS_ENEMY(pedLester , FALSE)
					SET_ENTITY_AS_MISSION_ENTITY(pedLester)
				ENDIF
				//PRINTLN("CREATED WADE")//PRINTNL()
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_LESTER_CAR_FOR_CUTSCENE
			IF bJustRequestAssets
				REQUEST_VEHICLE_ASSET(TAILGATER)
				REQUEST_MODEL(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
			ENDIF
			REQUEST_VEHICLE_ASSET(TAILGATER)
			REQUEST_MODEL(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
			IF HAS_MODEL_LOADED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
			AND HAS_VEHICLE_ASSET_LOADED(TAILGATER)
				IF NOT DOES_ENTITY_EXIST(mission_veh[ENUM_TO_INT(MV_MICHAELS_CAR)].veh)
					REQUEST_VEHICLE_ASSET(TAILGATER)
					CREATE_PLAYER_VEHICLE(mission_veh[ENUM_TO_INT(MV_MICHAELS_CAR)].veh, CHAR_MICHAEL ,<<1740.76, 3294.06, 40.76>>, 247.69, TRUE)				
				ELSE
					REQUEST_VEHICLE_ASSET(TAILGATER)
					IF HAS_VEHICLE_ASSET_LOADED(TAILGATER)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_MICHAELS_CAR)].veh)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[ENUM_TO_INT(MV_MICHAELS_CAR)].veh,TRUE)
							SET_VEHICLE_DISABLE_TOWING(mission_veh[ENUM_TO_INT(MV_MICHAELS_CAR)].veh,TRUE)
							SET_VEHICLE_HAS_STRONG_AXLES(mission_veh[ENUM_TO_INT(MV_MICHAELS_CAR)].veh,TRUE)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_WADE_FOR_CUTSCENE
			IF NOT DOES_ENTITY_EXIST(pedWade)
				IF CREATE_NPC_PED_ON_FOOT(pedWade, CHAR_WADE, <<1743.55, 3297.09, 41.15>>, -133.50)
					SET_PED_CAN_BE_TARGETTED(pedWade, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWade, TRUE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedWade, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedWade, RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedWade ,rel_group_buddies)
					//SET_ENTITY_PROOFS(pedWade, FALSE, FALSE, FALSE, TRUE, FALSE)
					SET_PED_CONFIG_FLAG(pedWade,PCF_CanActivateRagdollWhenVehicleUpsideDown,TRUE)
					SET_PED_AS_ENEMY(pedWade , FALSE)
					SET_ENTITY_AS_MISSION_ENTITY(pedWade)
					ADD_PED_FOR_DIALOGUE(sSpeech, 4, pedWade, "WADE")
					//PRINTLN("CREATED WADE")//PRINTNL()
					RETURN TRUE
				ENDIF
			ELSE
				IF sSpeech.PedInfo[4].Index != pedWade
					ADD_PED_FOR_DIALOGUE(sSpeech, 4, pedWade, "WADE")
				ENDIF
				IF NOT IS_PED_INJURED(pedWade)
					SET_PED_CAN_BE_TARGETTED(pedWade, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWade, TRUE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedWade, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedWade, RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedWade ,rel_group_buddies)
					//SET_ENTITY_PROOFS(pedWade, FALSE, FALSE, FALSE, TRUE, FALSE)
					SET_PED_CONFIG_FLAG(pedWade,PCF_CanActivateRagdollWhenVehicleUpsideDown,TRUE)
					SET_PED_AS_ENEMY(pedWade , FALSE)
					SET_ENTITY_AS_MISSION_ENTITY(pedWade)
				ENDIF
				//PRINTLN("CREATED WADE")//PRINTNL()
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_WADE
			IF NOT DOES_ENTITY_EXIST(pedWade)
				IF CREATE_NPC_PED_ON_FOOT(pedWade, CHAR_WADE, vPos, fHeading)
					SET_PED_CAN_BE_TARGETTED(pedWade, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWade, TRUE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedWade, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedWade, RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedWade ,rel_group_buddies)
					//SET_ENTITY_PROOFS(pedWade, FALSE, FALSE, FALSE, TRUE, FALSE)
					SET_PED_CONFIG_FLAG(pedWade,PCF_CanActivateRagdollWhenVehicleUpsideDown,TRUE)
					SET_PED_AS_ENEMY(pedWade , FALSE)
					SET_ENTITY_AS_MISSION_ENTITY(pedWade)
					ADD_PED_FOR_DIALOGUE(sSpeech, 4, pedWade, "WADE")
					//PRINTLN("CREATED WADE")//PRINTNL()
					RETURN TRUE
				ENDIF
			ELSE
				IF sSpeech.PedInfo[4].Index != pedWade
					ADD_PED_FOR_DIALOGUE(sSpeech, 4, pedWade, "WADE")
				ENDIF
				IF NOT IS_PED_INJURED(pedWade)
					SET_PED_CAN_BE_TARGETTED(pedWade, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedWade, TRUE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedWade, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedWade, RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedWade ,rel_group_buddies)
					//SET_ENTITY_PROOFS(pedWade, FALSE, FALSE, FALSE, TRUE, FALSE)
					SET_PED_CONFIG_FLAG(pedWade,PCF_CanActivateRagdollWhenVehicleUpsideDown,TRUE)
					SET_PED_AS_ENEMY(pedWade , FALSE)
					SET_ENTITY_AS_MISSION_ENTITY(pedWade)
				ENDIF
				//PRINTLN("CREATED WADE")//PRINTNL()
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_FLOYD
			IF NOT DOES_ENTITY_EXIST(pedFloyd)
				IF CREATE_NPC_PED_ON_FOOT(pedFloyd, CHAR_FLOYD, vPos, fHeading)
					SET_PED_CAN_BE_TARGETTED(pedFloyd, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFloyd, TRUE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedFloyd, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedFloyd, RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedFloyd ,rel_group_buddies)
					//SET_ENTITY_PROOFS(pedFloyd, FALSE, FALSE, FALSE, TRUE, FALSE)
					SET_PED_AS_ENEMY(pedFloyd , FALSE)
					SET_PED_CONFIG_FLAG(pedFloyd,PCF_CanActivateRagdollWhenVehicleUpsideDown,TRUE)
					SET_ENTITY_AS_MISSION_ENTITY(pedFloyd)
					ADD_PED_FOR_DIALOGUE(sSpeech, 5, pedFloyd, "FLOYD")
					//PRINTLN("CREATING FLOYD")//PRINTNL()
					RETURN TRUE
				ENDIF
			ELSE
				IF sSpeech.PedInfo[3].Index != pedFloyd
					ADD_PED_FOR_DIALOGUE(sSpeech, 5, pedFloyd, "FLOYD")
				ENDIF
				IF NOT IS_PED_INJURED(pedFloyd)
					SET_PED_CAN_BE_TARGETTED(pedFloyd, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedFloyd, TRUE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedFloyd, FALSE)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedFloyd, RELGROUPHASH_PLAYER)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedFloyd ,rel_group_buddies)
					//SET_ENTITY_PROOFS(pedFloyd, FALSE, FALSE, FALSE, TRUE, FALSE)
					SET_PED_AS_ENEMY(pedFloyd , FALSE)
					SET_PED_CONFIG_FLAG(pedFloyd,PCF_CanActivateRagdollWhenVehicleUpsideDown,TRUE)
					SET_ENTITY_AS_MISSION_ENTITY(pedFloyd)
				ENDIF
				//PRINTLN("CREATING FLOYD")//PRINTNL()
				RETURN TRUE
			ENDIF
		BREAK
		
		CASE REQ_MISSION_HELI
			
			IF bJustRequestAssets
			 	REQUEST_MODEL(CARGOBOB3)
				REQUEST_VEHICLE_ASSET(CARGOBOB3)
			ENDIF
			ROPE_LOAD_TEXTURES()
			REQUEST_MODEL(CARGOBOB3)
			IF NOT DOES_ENTITY_EXIST(mission_veh[0].veh)
				IF HAS_MODEL_LOADED(CARGOBOB3)
				AND ROPE_ARE_TEXTURES_LOADED()
					mission_veh[0].veh = CREATE_VEHICLE(CARGOBOB3,vPos,fHeading)
					SET_VEHICLE_STRONG(mission_veh[0].veh,TRUE)
					SET_VEHICLE_AS_RESTRICTED(mission_veh[0].veh,0)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[0].veh,TRUE)
					SET_VEHICLE_ENGINE_CAN_DEGRADE(mission_veh[0].veh,FALSE) 
					SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(mission_veh[0].veh, TRUE)					
					SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(mission_veh[MV_CHINOOK].veh,imaxRopeLength,IminRopeLength)
					SET_VEHICLE_NOISE_BASED_ON_FLIGHT_STAT(mission_veh[MV_CHINOOK].veh,GET_SP_PLAYER_PED_STAT_VALUE(CHAR_MICHAEL, PS_FLYING_ABILITY))
				ENDIF
			ELSE
				REQUEST_VEHICLE_ASSET(CARGOBOB3)
				IF HAS_VEHICLE_ASSET_LOADED(CARGOBOB3)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						IF NOT DOES_CARGOBOB_HAVE_PICK_UP_ROPE(mission_veh[0].veh)
							CREATE_PICK_UP_ROPE_FOR_CARGOBOB(mission_veh[0].veh)
						ELSE
							FREEZE_ENTITY_POSITION(mission_veh[0].veh,TRUE)
							SET_VEHICLE_ENGINE_CAN_DEGRADE(mission_veh[0].veh,FALSE) 
							SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(mission_veh[0].veh, TRUE)					
							SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(mission_veh[MV_CHINOOK].veh,imaxRopeLength,IminRopeLength)
							SET_VEHICLE_NOISE_BASED_ON_FLIGHT_STAT(mission_veh[MV_CHINOOK].veh,GET_SP_PLAYER_PED_STAT_VALUE(CHAR_MICHAEL, PS_FLYING_ABILITY))
							SET_VEHICLE_AS_RESTRICTED(mission_veh[0].veh,0)
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_MISSION_HELI_WITHOUT_ROPE
			IF bJustRequestAssets
			 	REQUEST_MODEL(CARGOBOB3)
				REQUEST_VEHICLE_ASSET(CARGOBOB3)
			ENDIF
			ROPE_LOAD_TEXTURES()
			REQUEST_MODEL(CARGOBOB3)
			IF NOT DOES_ENTITY_EXIST(mission_veh[0].veh)
				IF HAS_MODEL_LOADED(CARGOBOB3)
				AND ROPE_ARE_TEXTURES_LOADED()
					mission_veh[0].veh = CREATE_VEHICLE(CARGOBOB3,vPos,fHeading)
					SET_VEHICLE_STRONG(mission_veh[0].veh,TRUE)
					SET_VEHICLE_AS_RESTRICTED(mission_veh[0].veh,0)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[0].veh,TRUE)
					SET_VEHICLE_ENGINE_CAN_DEGRADE(mission_veh[0].veh,FALSE) 
					SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(mission_veh[0].veh, TRUE)					
					//SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(mission_veh[MV_CHINOOK].veh,imaxRopeLength,IminRopeLength)
					SET_VEHICLE_NOISE_BASED_ON_FLIGHT_STAT(mission_veh[MV_CHINOOK].veh,GET_SP_PLAYER_PED_STAT_VALUE(CHAR_MICHAEL, PS_FLYING_ABILITY))
				ENDIF
			ELSE
				REQUEST_VEHICLE_ASSET(CARGOBOB3)
				IF HAS_VEHICLE_ASSET_LOADED(CARGOBOB3)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						FREEZE_ENTITY_POSITION(mission_veh[0].veh,TRUE)
						SET_VEHICLE_ENGINE_CAN_DEGRADE(mission_veh[0].veh,FALSE) 
						SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(mission_veh[0].veh, TRUE)					
						//SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(mission_veh[MV_CHINOOK].veh,imaxRopeLength,IminRopeLength)
						SET_VEHICLE_NOISE_BASED_ON_FLIGHT_STAT(mission_veh[MV_CHINOOK].veh,GET_SP_PLAYER_PED_STAT_VALUE(CHAR_MICHAEL, PS_FLYING_ABILITY))
						SET_VEHICLE_AS_RESTRICTED(mission_veh[0].veh,0)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_HELI_WITH_SUB
			
			IF bJustRequestAssets
				REQUEST_MODEL(CARGOBOB3)
				REQUEST_MODEL(SUBMERSIBLE)
				REQUEST_VEHICLE_ASSET(CARGOBOB3)
				REQUEST_VEHICLE_ASSET(SUBMERSIBLE)
			ENDIF
			REQUEST_MODEL(CARGOBOB3)
			REQUEST_MODEL(SUBMERSIBLE)
			IF NOT DOES_ENTITY_EXIST(mission_veh[0].veh)
				IF HAS_MODEL_LOADED(CARGOBOB3)
					REQUEST_VEHICLE_ASSET(CARGOBOB3)
					mission_veh[0].veh = CREATE_VEHICLE(CARGOBOB3,vPos,fHeading)
					SET_VEHICLE_ENGINE_CAN_DEGRADE(mission_veh[0].veh,FALSE) 
					SET_DISABLE_VEHICLE_PETROL_TANK_FIRES(mission_veh[0].veh, TRUE)		
					
					SET_VEHICLE_AS_RESTRICTED(mission_veh[0].veh,0)
					FREEZE_ENTITY_POSITION(mission_veh[0].veh,TRUE)
					SET_VEHICLE_STRONG(mission_veh[0].veh,TRUE)
					IF NOT DOES_CARGOBOB_HAVE_PICK_UP_ROPE(mission_veh[0].veh)
						CREATE_PICK_UP_ROPE_FOR_CARGOBOB(mission_veh[0].veh)
					ENDIF
					SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(mission_veh[MV_CHINOOK].veh,imaxRopeLength,IminRopeLength,TRUE)
					SET_VEHICLE_ENGINE_CAN_DEGRADE(mission_veh[0].veh,FALSE) 
				ENDIF
			ELSE
				IF NOT DOES_ENTITY_EXIST(mission_veh[1].veh)
					IF HAS_MODEL_LOADED(SUBMERSIBLE)
						REQUEST_VEHICLE_ASSET(SUBMERSIBLE)
						mission_veh[1].veh = CREATE_VEHICLE(SUBMERSIBLE,<<vPos.x,vPos.y,vPos.z -10>>,fHeading)
						SET_VEHICLE_ENGINE_CAN_DEGRADE(mission_veh[1].veh,FALSE) 
						SET_VEHICLE_ENGINE_ON(mission_veh[1].veh,FALSE,TRUE)
						FREEZE_ENTITY_POSITION(mission_veh[1].veh,TRUE)
						ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh,"DH_2B_SUBMARINE_GROUP")
						SET_VEHICLE_AS_RESTRICTED(mission_veh[1].veh,1)
					ENDIF
				ELSE
					REQUEST_VEHICLE_ASSET(SUBMERSIBLE)
					IF HAS_VEHICLE_ASSET_LOADED(SUBMERSIBLE)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						AND IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							vSubmarineCoor = GET_ENTITY_COORDS(mission_veh[0].veh)
							vSubmarineCoor += <<0,0,-10>>
							IF DOES_CARGOBOB_HAVE_PICK_UP_ROPE(mission_veh[0].veh)
								IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[0].veh,mission_veh[1].veh)
									STABILISE_ENTITY_ATTACHED_TO_HELI(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh,iStabliseLength)
									RETURN TRUE
								ELSE
									PRINTSTRING("SUB NOT ATTACHED TO CARGOBOB")PRINTNL()
									SET_ENTITY_COORDS(mission_veh[1].veh, vSubmarineCoor)
									ATTACH_VEHICLE_TO_CARGOBOB(mission_veh[0].veh,mission_veh[1].veh,-1,<<0,0,0.5>>)
								ENDIF
							ELSE
								PRINTSTRING("DOESN'T HAVE ROPE")PRINTNL()
								CREATE_PICK_UP_ROPE_FOR_CARGOBOB(mission_veh[0].veh)
								SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(mission_veh[MV_CHINOOK].veh,imaxRopeLength,IminRopeLength,TRUE)
								SET_ENTITY_COORDS(mission_veh[1].veh, vSubmarineCoor)
								ATTACH_VEHICLE_TO_CARGOBOB(mission_veh[0].veh,mission_veh[1].veh,-1,<<0,0,1>>)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_SUBMERSIBLE
			IF bJustRequestAssets
			 	REQUEST_MODEL(SUBMERSIBLE)
				REQUEST_VEHICLE_ASSET(SUBMERSIBLE)
			ENDIF
			REQUEST_MODEL(SUBMERSIBLE)
			IF NOT DOES_ENTITY_EXIST(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh)
				IF HAS_MODEL_LOADED(SUBMERSIBLE)
					mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh = CREATE_VEHICLE(SUBMERSIBLE,vPos,fHeading)
					SET_ENTITY_COORDS(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh,vPos)
					SET_VEHICLE_AS_RESTRICTED(mission_veh[MV_SUBMERISIBLE].veh,1)
					SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh,TRUE)
					REQUEST_COLLISION_FOR_MODEL(SUBMERSIBLE)
					SET_VEHICLE_ENGINE_ON(mission_veh[MV_SUBMERISIBLE].veh,FALSE,TRUE)
					//SET_VEHICLE_INFLUENCES_WANTED_LEVEL(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
					SET_SUBMARINE_CRUSH_DEPTHS(mission_veh[MV_SUBMERISIBLE].veh,FALSE,-190,-190,-190)
					//SET_VEHICLE_LIGHT_MULTIPLIER(mission_veh[1].veh,15)
					REQUEST_COLLISION_AT_COORD(vPos)	
				ENDIF
			ELSE
				REQUEST_VEHICLE_ASSET(SUBMERSIBLE)
				IF HAS_VEHICLE_ASSET_LOADED(SUBMERSIBLE)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh)
						SET_SUBMARINE_CRUSH_DEPTHS(mission_veh[MV_SUBMERISIBLE].veh,FALSE,-190,-190,-190)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		
		
		CASE REQ_DINGHY
			IF bJustRequestAssets
			 	REQUEST_MODEL(DINGHY)
				REQUEST_VEHICLE_ASSET(DINGHY)
			ENDIF
			REQUEST_MODEL(DINGHY)
			IF NOT DOES_ENTITY_EXIST(mission_veh[2].veh)
				IF HAS_MODEL_LOADED(DINGHY)
					mission_veh[2].veh = CREATE_VEHICLE(DINGHY,vPos,fHeading)
				ENDIF
			ELSE
				REQUEST_VEHICLE_ASSET(DINGHY)
				IF HAS_VEHICLE_ASSET_LOADED(DINGHY)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
						SET_VEHICLE_DISABLE_TOWING(mission_veh[2].veh,TRUE)
						SET_ENTITY_LOD_DIST(mission_veh[2].veh,1000)
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_CARGO
			IF bJustRequestAssets
				REQUEST_MODEL(prop_military_pickup_01)
			ENDIF
			REQUEST_MODEL(prop_military_pickup_01)
			IF HAS_MODEL_LOADED(prop_military_pickup_01)
				IF NOT DOES_ENTITY_EXIST(objSeabed[1])
					objSeabed[1] = CREATE_OBJECT(prop_military_pickup_01,vPos)
					SET_ENTITY_HEADING(objSeabed[1],fHeading)
					SET_ENTITY_ROTATION(objSeabed[1], <<0.0000, 0.0000, -89.8046>>)
					SET_ENTITY_QUATERNION(objSeabed[1], 0.0000, 0.0000, -0.7059, 0.7083)
					SET_ENTITY_COORDS_NO_OFFSET(objSeabed[1], <<-1255.3800, 6795.7202, -179.1137>>)
					FREEZE_ENTITY_POSITION(objSeabed[1],TRUE)
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_WATERLIFE
			IF bJustRequestAssets
				REQUEST_MODEL(A_C_FISH)
			ENDIF
			REQUEST_MODEL(A_C_FISH)
			IF HAS_MODEL_LOADED(A_C_FISH)
				IF NOT DOES_ENTITY_EXIST(pedUnderWater[1])
					pedUnderWater[1] = CREATE_PED(PEDTYPE_MISSION,A_C_FISH,<< -1260.04, 6802.21, -174.41 >>)
					SET_ENTITY_HEADING(pedUnderWater[1],-159.48)
					SET_ENTITY_LOD_DIST(pedUnderWater[1],100)
					TASK_START_SCENARIO_IN_PLACE(pedUnderWater[1],"WORLD_FISH_FLEE")
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_SHARK_BY_CRATE
			IF bJustRequestAssets
				REQUEST_MODEL(A_C_SHARKTIGER)
			ENDIF
			REQUEST_MODEL(A_C_SHARKTIGER)
			IF HAS_MODEL_LOADED(A_C_SHARKTIGER)
				IF NOT DOES_ENTITY_EXIST(pedUnderWater[0])
					pedUnderWater[0] = CREATE_PED(PEDTYPE_MISSION,A_C_SHARKTIGER, <<-1244.6625, 6783.5034, -177.7454>>)
					SET_ENTITY_HEADING(pedUnderWater[0], 49.7381)
					SET_ENTITY_LOD_DIST(pedUnderWater[0], 200)
				ELSE
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_SHARK
			
			VECTOR vTemp
			vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
			
			IF bJustRequestAssets
				REQUEST_MODEL(A_C_SHARKTIGER)
			ENDIF
			REQUEST_MODEL(A_C_SHARKTIGER)
			IF HAS_MODEL_LOADED(A_C_SHARKTIGER)
				IF NOT DOES_ENTITY_EXIST(pedUnderWater[2])
					pedUnderWater[2] = CREATE_PED(PEDTYPE_MISSION,A_C_SHARKTIGER, <<vTemp.x + 2, vTemp.y, vTemp.z>>)
					SET_ENTITY_LOD_DIST(pedUnderWater[2],100)
				ELSE
					IF NOT IS_PED_INJURED(pedUnderWater[2])
						SET_ENTITY_INVINCIBLE(pedUnderWater[2] ,TRUE)
					ENDIF
					RETURN TRUE
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_MERRYWEATHER_DINGHY_START
			IF bJustRequestAssets
				REQUEST_VEHICLE_ASSET(DINGHY)
			ENDIF
			REQUEST_MODEL(DINGHY)
			REQUEST_MODEL(S_M_Y_BLACKOPS_01)
			IF HAS_MODEL_LOADED(DINGHY)
			AND HAS_MODEL_LOADED(S_M_Y_BLACKOPS_01)
				IF NOT DOES_ENTITY_EXIST(mission_veh[20].veh)
					CREATE_ENEMY_VEHICLE(20,DINGHY,<<-1393.808716,6161.465332,0.505434>>, -128.466629, S_M_Y_BLACKOPS_01,TRUE, 0,1,FALSE,-1,WEAPONTYPE_MICROSMG)
					SET_ENTITY_LOD_DIST(mission_veh[20].veh,700)
				ELSE
					IF NOT DOES_ENTITY_EXIST(mission_veh[21].veh)
						CREATE_ENEMY_VEHICLE(21,DINGHY,<<-1387.404907,6134.791016,0.492382>>, -87.673141, S_M_Y_BLACKOPS_01,TRUE, 2,2,FALSE,-1,WEAPONTYPE_MICROSMG)
						SET_ENTITY_LOD_DIST(mission_veh[21].veh,700)
					ELSE
						IF NOT DOES_ENTITY_EXIST(mission_veh[22].veh)
							CREATE_ENEMY_VEHICLE(22,DINGHY,<<-1369.533081,6161.245117,0.307101>>, 112.634995, S_M_Y_BLACKOPS_01,TRUE, 5,1,FALSE,-1,WEAPONTYPE_MICROSMG)
							SET_ENTITY_LOD_DIST(mission_veh[22].veh,700)
						ELSE
							RETURN TRUE
						ENDIF
					ENDIF				
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_MERRYWEATHER_DINGHY_SETPIECE
			IF bJustRequestAssets
				REQUEST_VEHICLE_ASSET(DINGHY)
			ENDIF
			REQUEST_MODEL(DINGHY)
			REQUEST_MODEL(S_M_Y_BLACKOPS_01)
			REQUEST_VEHICLE_RECORDING(27,"DHF2")
			REQUEST_VEHICLE_RECORDING(28,"DHF2")
			REQUEST_VEHICLE_RECORDING(29,"DHF2")
			IF HAS_MODEL_LOADED(DINGHY)
			AND HAS_MODEL_LOADED(S_M_Y_BLACKOPS_01)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(27,"DHF2")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(28,"DHF2")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(29,"DHF2")
				IF NOT DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh)
					CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_DINGHY_SETPIECE_1),DINGHY,<<-406.5101, 4449.4565, 27.6139>>, 257.3971, S_M_Y_BLACKOPS_01,TRUE, 7,0,FALSE,-1,WEAPONTYPE_MICROSMG)
					SET_ENTITY_LOD_DIST(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh,1500)
					SET_VEHICLE_COLOURS(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh,0,0)
					SET_BOAT_SINKS_WHEN_WRECKED(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh,TRUE)
				ELSE
					IF NOT DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_2].veh)
						CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_DINGHY_SETPIECE_2),DINGHY,<<-441.2678, 4432.4106, 26.8193>>, 280.1574, S_M_Y_BLACKOPS_01,TRUE, 8,0,FALSE,-1,WEAPONTYPE_MICROSMG)
						SET_ENTITY_LOD_DIST(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_2].veh,1500)
						SET_VEHICLE_COLOURS(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_2].veh,0,0)
						SET_BOAT_SINKS_WHEN_WRECKED(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_2].veh,TRUE)
					ELSE
						IF NOT DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_3].veh)
							CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_DINGHY_SETPIECE_3),DINGHY,<<-376.0573, 4423.9873, 27.5698>>, 236.8367, S_M_Y_BLACKOPS_01,TRUE, 9,0,FALSE,-1,WEAPONTYPE_MICROSMG)
							SET_ENTITY_LOD_DIST(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_3].veh,1500)
							SET_VEHICLE_COLOURS(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_3].veh,0,0)
							SET_BOAT_SINKS_WHEN_WRECKED(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_3].veh,TRUE)
						ELSE
							IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh)
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh)
									START_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh,27,"DHF2")//,2,0,DRIVINGMODE_AVOIDCARS)
								ENDIF
							ENDIF
							IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_2].veh)
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_2].veh)
									START_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_2].veh,28,"DHF2")//,2,0,DRIVINGMODE_AVOIDCARS)
								ENDIF
							ENDIF
							IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_3].veh)
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_3].veh)
									START_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_3].veh,29,"DHF2")//,2,0,DRIVINGMODE_AVOIDCARS)
								ENDIF
							ENDIF
							RETURN TRUE
						ENDIF
					ENDIF				
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_MERRYWEATHER_PLANE_SETPIECE
			REQUEST_MODEL(LAZER)
			REQUEST_MODEL(S_M_Y_BLACKOPS_01)
			REQUEST_VEHICLE_RECORDING(97,"DHF2")
//			REQUEST_VEHICLE_RECORDING(97,"DHF2")
//			REQUEST_VEHICLE_RECORDING(97,"DHF2")
			IF HAS_MODEL_LOADED(LAZER)
			AND HAS_MODEL_LOADED(S_M_Y_BLACKOPS_01)
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(97,"DHF2") //97
//			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(ENUM_TO_INT(MV_MERRY_WEATHER_CARGOPLANE1),"DHF2") //98
//			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(ENUM_TO_INT(MV_MERRY_WEATHER_CARGOPLANE2),"DHF2") //99
				IF NOT DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh)
					CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CARGOPLANE),LAZER,<<1010.1705, 7373.8882, 433.2440>>, 100.0127, S_M_Y_BLACKOPS_01,TRUE, 10,0,FALSE,-1,WEAPONTYPE_MICROSMG)
					SET_ENTITY_LOD_DIST(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh,3000)
					SET_VEHICLE_AS_RESTRICTED(mission_veh[26].veh,2)
					SET_MODEL_AS_NO_LONGER_NEEDED(LAZER)
				ELSE
					IF NOT DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CARGOPLANE1].veh)
						CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CARGOPLANE1),LAZER,<<1010.1705, 7373.8882, 533.2440>>, -128.466629, S_M_Y_BLACKOPS_01,TRUE, 11,0,FALSE,-1,WEAPONTYPE_MICROSMG)
						SET_ENTITY_LOD_DIST(mission_veh[MV_MERRY_WEATHER_CARGOPLANE1].veh,3000)
						SET_VEHICLE_AS_RESTRICTED(mission_veh[MV_MERRY_WEATHER_CARGOPLANE1].veh,3)
						SET_MODEL_AS_NO_LONGER_NEEDED(LAZER)
					ELSE
						IF NOT DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CARGOPLANE2].veh)
							CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CARGOPLANE2),LAZER,<<-214.0745, 7287.8730, 256.5554>>, -128.466629, S_M_Y_BLACKOPS_01,TRUE, 12,0,FALSE,-1,WEAPONTYPE_MICROSMG)
							SET_ENTITY_LOD_DIST(mission_veh[MV_MERRY_WEATHER_CARGOPLANE2].veh,3000)
							SET_VEHICLE_AS_RESTRICTED(mission_veh[MV_MERRY_WEATHER_CARGOPLANE2].veh,4)
							SET_MODEL_AS_NO_LONGER_NEEDED(LAZER)
						ELSE
							IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh)
							AND IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE1].veh)
							AND IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE2].veh)
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh)
									START_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh,97,"DHF2")
									SET_ENTITY_INVINCIBLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh,TRUE)
									SET_PLAYBACK_SPEED(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh,1.6)
									
									START_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE1].veh,97,"DHF2")
									SET_GLOBAL_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(mission_veh[MV_MERRY_WEATHER_CARGOPLANE1].veh,<<-100,20,-10>>)
									SET_ENTITY_INVINCIBLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE1].veh,TRUE)
									SET_PLAYBACK_SPEED(mission_veh[MV_MERRY_WEATHER_CARGOPLANE1].veh,1.6)
									
									START_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE2].veh,97,"DHF2")
									SET_GLOBAL_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(mission_veh[MV_MERRY_WEATHER_CARGOPLANE2].veh,<<50,-20,-8>>)
									SET_ENTITY_INVINCIBLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE2].veh,TRUE)
									SET_PLAYBACK_SPEED(mission_veh[MV_MERRY_WEATHER_CARGOPLANE2].veh,1.6)
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_MERRYWEATHER_WORKING_ON_RIG
			REQUEST_MODEL(S_M_Y_BLACKOPS_01)
			IF HAS_MODEL_LOADED(S_M_Y_BLACKOPS_01)
				IF NOT DOES_ENTITY_EXIST(enemy[0].ped)
					CREATE_ENEMY_AT_COORD(0,<< -1309.16, 6145.46, 3.1 >>, -50.31,FALSE,FALSE)
					IF NOT IS_PED_INJURED(enemy[0].ped)
						REQUEST_COLLISION_FOR_MODEL(S_M_Y_BLACKOPS_01)
						REQUEST_COLLISION_AT_COORD(<< -1309.16, 6145.46, 2.36 >>)
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(enemy[0].ped,TRUE)
						TASK_START_SCENARIO_IN_PLACE(enemy[0].ped,"WORLD_HUMAN_CLIPBOARD")
					ENDIF
				ELSE
					IF NOT DOES_ENTITY_EXIST(enemy[1].ped)
						CREATE_ENEMY_AT_COORD(1,<< -1310.03, 6122.33, 3.1 >>, 156.17,FALSE,FALSE)
						IF NOT IS_PED_INJURED(enemy[1].ped)
							REQUEST_COLLISION_FOR_MODEL(S_M_Y_BLACKOPS_01)
							REQUEST_COLLISION_AT_COORD(<< -1309.16, 6145.46, 2.36 >>)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(enemy[1].ped,TRUE)
							TASK_START_SCENARIO_IN_PLACE(enemy[1].ped,"WORLD_HUMAN_CLIPBOARD")
						ENDIF	
					ELSE
						IF NOT DOES_ENTITY_EXIST(enemy[2].ped)
							CREATE_ENEMY_AT_COORD(2,<< -1308.89, 6145.38, 3.1 >>, -74.23,FALSE,FALSE)
							IF NOT IS_PED_INJURED(enemy[2].ped)
								REQUEST_COLLISION_FOR_MODEL(S_M_Y_BLACKOPS_01)
								REQUEST_COLLISION_AT_COORD(<< -1309.16, 6145.46, 2.36 >>)
								SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(enemy[2].ped,TRUE)
								TASK_START_SCENARIO_IN_PLACE(enemy[2].ped,"WORLD_HUMAN_STAND_IMPATIENT")
							ENDIF
						ELSE
							IF NOT DOES_ENTITY_EXIST(enemy[3].ped)
								CREATE_ENEMY_AT_COORD(3,<< -1311.39, 6123.26, 3.01 >>, -97.40,FALSE,FALSE)
								IF NOT IS_PED_INJURED(enemy[3].ped)
									REQUEST_COLLISION_FOR_MODEL(S_M_Y_BLACKOPS_01)
									REQUEST_COLLISION_AT_COORD(<< -1309.16, 6145.46, 2.36 >>)
									SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(enemy[3].ped,TRUE)
									TASK_START_SCENARIO_IN_PLACE(enemy[3].ped,"WORLD_HUMAN_STAND_IMPATIENT")
								ENDIF
							ELSE
								IF NOT DOES_ENTITY_EXIST(enemy[4].ped)
									CREATE_ENEMY_AT_COORD(4,<< -1293.21, 6134.04, 3.1 >>, -80.79,FALSE,FALSE)
									IF NOT IS_PED_INJURED(enemy[4].ped)
										REQUEST_COLLISION_FOR_MODEL(S_M_Y_BLACKOPS_01)
										REQUEST_COLLISION_AT_COORD(<< -1309.16, 6145.46, 2.36 >>)
										SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(enemy[4].ped,TRUE)
										TASK_START_SCENARIO_IN_PLACE(enemy[4].ped,"WORLD_HUMAN_BINOCULARS")
									ENDIF
								ELSE
									RETURN TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
		BREAK
		
		CASE REQ_MERRYWEATHER_RIG
			REQUEST_MODEL(Prop_LEV_DES_Barge_01)
			IF HAS_MODEL_LOADED(Prop_LEV_DES_Barge_01)
				IF NOT DOES_ENTITY_EXIST(objMission[MO_RIG1])
					objMission[MO_RIG1] = create_object(Prop_LEV_DES_Barge_01,<<-1301.52, 6128.91, -0.79>>)
					SET_ENTITY_ROTATION(objMission[MO_RIG1],<<-3.45, 2.11, -33.10>>)
					FREEZE_ENTITY_POSITION(objMission[MO_RIG1],TRUE)
					SET_ENTITY_LOD_DIST(objMission[MO_RIG1], 2500)
				ELSE
					IF NOT DOES_ENTITY_EXIST(objMission[MO_RIG2])
						objMission[MO_RIG2] = create_object(Prop_LEV_DES_Barge_01,<<-1316.14, 6137.97, -0.63>>)
						SET_ENTITY_ROTATION(objMission[MO_RIG2],<<0.00, 0.00, -33.55>>)
						FREEZE_ENTITY_POSITION(objMission[MO_RIG2],TRUE)
						SET_ENTITY_LOD_DIST(objMission[MO_RIG2], 2500)
					ELSE
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE REQ_MERRYWEATHER_FX
			REQUEST_PTFX_ASSET()
			
			IF HAS_PTFX_ASSET_LOADED()
				RETURN TRUE
			ENDIF
			
		BREAK
		
		CASE REQ_MERRYWEATHER_HELI_START
			IF bJustRequestAssets
				REQUEST_VEHICLE_ASSET(BUZZARD)
			ENDIF
			REQUEST_MODEL(BUZZARD)
			REQUEST_MODEL(S_M_Y_BLACKOPS_01)
			IF HAS_MODEL_LOADED(BUZZARD)
			AND HAS_MODEL_LOADED(S_M_Y_BLACKOPS_01)
				IF NOT DOES_ENTITY_EXIST(mission_veh[23].veh)
					CREATE_ENEMY_VEHICLE(23,BUZZARD,<<-1393.808716,6161.465332,24.505434>>, -128.466629, S_M_Y_BLACKOPS_01,TRUE, 7,1,FALSE,-1,WEAPONTYPE_MICROSMG)
					FREEZE_ENTITY_POSITION(mission_veh[23].veh,TRUE)
				ELSE
					IF NOT DOES_ENTITY_EXIST(mission_veh[24].veh)
						CREATE_ENEMY_VEHICLE(24,BUZZARD,<<-1387.404907,6134.791016,27.492382>>, -87.673141, S_M_Y_BLACKOPS_01,TRUE, 9,1,FALSE,-1,WEAPONTYPE_MICROSMG)
						FREEZE_ENTITY_POSITION(mission_veh[24].veh,TRUE)
					ELSE
//						IF NOT DOES_ENTITY_EXIST(mission_veh[25].veh)
//							CREATE_ENEMY_VEHICLE(25,BUZZARD,<<-1369.533081,6161.245117,33.307101>>, 112.634995, S_M_Y_BLACKOPS_01,TRUE, 11,1,FALSE,-1,WEAPONTYPE_MICROSMG)
//							FREEZE_ENTITY_POSITION(mission_veh[25].veh,TRUE)
//						ELSE
							RETURN TRUE
//						ENDIF
					ENDIF				
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC MANAGE_MERRYWEATHER_PTFX()

//scr_pls_exp_air_grenade --- 1 shot explosion
//scr_pls_exp_water ----------- 1 shot water explosion
//scr_pls_bubbles_lge----- looping surface bubbles
//"scr_trev2_flare_L"

SWITCH iPtfx

	CASE 0
		IF SETUP_MISSION_REQUIREMENT(REQ_MERRYWEATHER_FX,<<0,0,0>>)
			iPtfx ++
		ENDIF		
	BREAK
	
	CASE 1
		merry_weather_ptfx[0] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_pls_bubbles_lge", <<-1301.6078, 6151.1323, 0.7336>>, <<0.0, 0.0, 0.0>>)
		merry_weather_ptfx[1] = START_PARTICLE_FX_LOOPED_AT_COORD("scr_pls_bubbles_lge", <<-1292.5709, 6143.4194, 0.7336>>, <<0.0, 0.0, 0.0>>)
		iPtfx ++
	BREAK
	
	CASE 2
		IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER BLAST 1")
			START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_pls_exp_water",<<-1290.2866, 6162.8218, 0.8069>>,<<0.0, 0.0, 0.0>>)
			START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_pls_exp_air_grenade",<<-1290.2866, 6162.8218, 0.8069>>,<<0.0, 0.0, 0.0>>)
			i_blast_timer_1 = GET_GAME_TIMER()
			SET_LABEL_AS_TRIGGERED("TRIGGER BLAST 1",TRUE)
		ELSE
			IF MANAGE_MY_TIMER(i_blast_timer_1,GET_RANDOM_INT_IN_RANGE(10000,18000))
				SET_LABEL_AS_TRIGGERED("TRIGGER BLAST 1",FALSE)
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER BLAST 2")
			START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_pls_exp_water",<<-1285.7286, 6116.8320, 0.8069>>,<<0.0, 0.0, 0.0>>)
			START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_pls_exp_air_grenade",<<-1285.7286, 6116.8320, 0.8069>>,<<0.0, 0.0, 0.0>>)
			i_blast_timer_2 = GET_GAME_TIMER()
			SET_LABEL_AS_TRIGGERED("TRIGGER BLAST 2",TRUE)
		ELSE
			IF MANAGE_MY_TIMER(i_blast_timer_2,GET_RANDOM_INT_IN_RANGE(18000,23000))
				SET_LABEL_AS_TRIGGERED("TRIGGER BLAST 2",FALSE)
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER BLAST 3")
			START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_pls_exp_water",<<-1327.0239, 6146.7778, 0.8069>>,<<0.0, 0.0, 0.0>>)
			START_PARTICLE_FX_NON_LOOPED_AT_COORD("scr_pls_exp_air_grenade",<<-1327.0239, 6146.7778, 0.8069>>,<<0.0, 0.0, 0.0>>)
			i_blast_timer_3 = GET_GAME_TIMER()
			SET_LABEL_AS_TRIGGERED("TRIGGER BLAST 3",TRUE)
		ELSE
			IF MANAGE_MY_TIMER(i_blast_timer_3,GET_RANDOM_INT_IN_RANGE(16000,22000))
				SET_LABEL_AS_TRIGGERED("TRIGGER BLAST 3",FALSE)
			ENDIF
		ENDIF
	BREAK

ENDSWITCH

ENDPROC

PROC SETUP_PED_OUTFITS()
	
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			PRELOAD_OUTFIT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], OUTFIT_P0_STEALTH_NO_MASK)
			SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH_NO_MASK, FALSE)
			CLEAR_PED_PROP(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],ANCHOR_EYES)
		ENDIF
	ELSE
		PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P0_STEALTH_NO_MASK)
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH_NO_MASK, FALSE)
		CLEAR_PED_PROP(PLAYER_PED_ID(),ANCHOR_EYES)
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
		IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				PRELOAD_OUTFIT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], OUTFIT_P1_STEALTH_NO_MASK)
				SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], COMP_TYPE_OUTFIT, OUTFIT_P1_STEALTH_NO_MASK, FALSE)
			ENDIF
		ENDIF
	ELSE
		PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P1_STEALTH_NO_MASK)
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_STEALTH_NO_MASK, FALSE)
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
		IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				PRELOAD_OUTFIT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], OUTFIT_P2_STEALTH_NO_MASK)
				SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_OUTFIT, OUTFIT_P2_STEALTH_NO_MASK, FALSE)
				CLEAR_PED_PROP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],ANCHOR_HEAD)
				CLEAR_PED_PROP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],ANCHOR_EYES)
			ENDIF
		ENDIF
	ELSE
		PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P2_STEALTH_NO_MASK)
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_STEALTH_NO_MASK, FALSE)
		CLEAR_PED_PROP(PLAYER_PED_ID(),ANCHOR_HEAD)
		CLEAR_PED_PROP(PLAYER_PED_ID(),ANCHOR_EYES)
	ENDIF

ENDPROC

FUNC BOOL IS_ENTITY_ALIVE(ENTITY_INDEX mEntity)
	IF DOES_ENTITY_EXIST(mEntity)
		IF NOT IS_ENTITY_DEAD(mEntity)
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

#IF IS_DEBUG_BUILD

WIDGET_GROUP_ID widBomb
FLOAT 			fBombMass
FLOAT 			fBombGravity
VECTOR 			vBombTransDamping
VECTOR 			vBombRotatDamping
FLOAT 			fBombBuoyancy
BOOL 			bBombApply
BOOL			bWarpSubToBomb

PROC BOMB_WIDGET()
	IF( DOES_WIDGET_GROUP_EXIST( objectwidget ) )
		IF( NOT DOES_WIDGET_GROUP_EXIST( widBomb ) )
			SET_CURRENT_WIDGET_GROUP( objectwidget )
			widBomb = START_WIDGET_GROUP( "Bomb" )
				ADD_WIDGET_FLOAT_SLIDER( "fMass", fBombMass, -1.0, 50000.0, 0.5 )
				ADD_WIDGET_FLOAT_SLIDER( "fGravity", fBombGravity, -1.0, 50.0, 0.1 )
				ADD_WIDGET_VECTOR_SLIDER( "vTransationalDamping", vBombTransDamping,  -10.0, 20.0, 0.5 )
				ADD_WIDGET_VECTOR_SLIDER( "vRotationalDamping", vBombRotatDamping,  -10.0, 20.0, 0.5 )
				ADD_WIDGET_FLOAT_SLIDER( "fBuoyancy", fBombBuoyancy, -1.0, 20.0, 0.5 )
				ADD_WIDGET_BOOL( "bApply", bBombApply )
				ADD_WIDGET_BOOL( "Warp Sub To Bomb", bWarpSubToBomb )
			STOP_WIDGET_GROUP()
			CLEAR_CURRENT_WIDGET_GROUP( objectwidget )
		ELSE
			IF( bBombApply )
				OBJECT_INDEX bomb = objSeabed[1]
				VEHICLE_INDEX sub = mission_veh[1].veh
				VECTOR 	vSecondEntityOffset	= << 0.0, -2.2, -2.9 >>
				VECTOR 	vFirstEntityOffset  = << 0.0, 0.00, 0.00 >>
				VECTOR 	vRotation           = << 0.0, 0.0, -90.0 >>
				
				DETACH_ENTITY( bomb )
				SET_OBJECT_PHYSICS_PARAMS( bomb, fBombMass, fBombGravity, vBombTransDamping, vBombRotatDamping, 
					DEFAULT, DEFAULT, fBombBuoyancy )
				ATTACH_ENTITY_TO_ENTITY_PHYSICALLY( bomb, sub, -1, 1, vSecondEntityOffset, vFirstEntityOffset, vRotation, -1, TRUE )
			
				bBombApply = FALSE
			ENDIF
			IF( bWarpSubToBomb )
				IF( IS_ENTITY_ALIVE( mission_veh[1].veh ) )
					SET_ENTITY_COORDS( mission_veh[1].veh, <<-1263.3051, 6761.3687, -180.1062>> )
					SET_ENTITY_HEADING( mission_veh[1].veh, 357.0598 )
				ENDIF
				bWarpSubToBomb = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

#ENDIF

BOOL bAttachedPhysically

PROC ATTACH_BOMB_TO_SUB()
	OBJECT_INDEX bomb = objSeabed[ 1 ]
	VEHICLE_INDEX sub = mission_veh[ 1 ].veh
	
	IF( IS_ENTITY_ALIVE( bomb ) AND IS_ENTITY_ALIVE( sub ) )
		IF( NOT IS_ENTITY_ATTACHED_TO_ENTITY( bomb, sub ) )
			ATTACH_ENTITY_TO_ENTITY( bomb, sub, 1, <<0, -2.0, -1.7>>, <<0, 0, 90>>, TRUE, FALSE, TRUE )
			SET_ENTITY_NO_COLLISION_ENTITY( bomb, sub, FALSE )
			SET_ENTITY_NO_COLLISION_ENTITY( sub, bomb, FALSE )
			bAttachedPhysically = FALSE
		ENDIF
	ELSE
		ASSERTLN( "ATTACH_BOMB_TO_SUB either bomb or sub isn't alive" )
	ENDIF
ENDPROC

PROC ATTACH_BOMB_TO_SUB_PHYSICALLY()
	OBJECT_INDEX bomb = objSeabed[ 1 ]
	VEHICLE_INDEX sub = mission_veh[ 1 ].veh
	
	IF( IS_ENTITY_ALIVE( bomb ) AND IS_ENTITY_ALIVE( sub ) )
		IF( NOT IS_ENTITY_ATTACHED_TO_ENTITY( bomb, sub ) )
			ATTACH_BOMB_TO_SUB()
		ELSE
			IF( NOT bAttachedPhysically )
				IF( NOT IS_ENTITY_IN_ANGLED_AREA_COMPLEX( bomb, <<-1255.311401,6793.454590,-183.545486>>, 
					<<-1255.444092,6787.876953,-178.753220>>, 4.000000 )
				AND NOT IS_ENTITY_IN_ANGLED_AREA_COMPLEX( bomb, <<-1255.535522,6803.392578,-183.512192>>, 
					<<-1255.365967,6797.564941,-177.546799>>, 4.000000 )
				AND NOT IS_ENTITY_IN_ANGLED_AREA( sub, <<-1258.615479,6783.494141,-182.621857>>, 
					<<-1257.912964,6807.971680,-173.941727>>, 20.000000))
					FLOAT 	fBuoyancy          	= 1.4
					FLOAT 	fGravity            = 1.0
					FLOAT  	fMass               = 400.0
					VECTOR	vRotatDamping		= << -1.0, -1.0, -1.0 >>
					VECTOR 	vTransDamping		= << -1.0, -1.0, -0.99 >>
					VECTOR 	vSecondEntityOffset	= << 0.0, -2.2, -2.9 >>
					VECTOR 	vFirstEntityOffset  = << 0.0, 0.00, 0.00 >>
					VECTOR 	vRotation           = << 0.0, 0.0, -90.0 >>
					
					#IF IS_DEBUG_BUILD
					fBombMass			= fMass
					fBombGravity		= fGravity
					vBombTransDamping	= vTransDamping
					vBombRotatDamping 	= vRotatDamping
					fBombBuoyancy		= fBuoyancy
					#ENDIF
					
					IF( IS_ENTITY_ATTACHED( bomb ) )
						DETACH_ENTITY( bomb, FALSE )
					ENDIF
					
					FREEZE_ENTITY_POSITION( bomb, FALSE )
					FREEZE_ENTITY_POSITION( sub, FALSE )

					SET_OBJECT_PHYSICS_PARAMS( bomb, fMass, fGravity, vTransDamping, vRotatDamping, DEFAULT, DEFAULT, fBuoyancy )
					ATTACH_ENTITY_TO_ENTITY_PHYSICALLY( bomb, sub, -1, 1, vSecondEntityOffset, vFirstEntityOffset, vRotation, -1, TRUE )
					bAttachedPhysically = TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		ASSERTLN( "ATTACH_BOMB_TO_SUB either bomb or sub isn't alive" )
	ENDIF
ENDPROC

//PURPOSE: this is used to load a specific mission stage, for both restart and j/p skips.
PROC LOAD_MISSION_STAGE(MISSION_STAGE_ENUM eStageToLoad) 

	IF NOT bcleanup //first cleans up all existing mission assets
		MISSION_CLEANUP(FALSE)
		CLEAR_AREA(<< 1764.9365, 3270.3894, 40.3731 >>,1000,TRUE,TRUE)
		CLEAR_AREA(<< 1764.9365, 3270.3894, 40.3731 >>,1000,TRUE,TRUE)
		CLEAR_AREA(<<526.2101, -3160.0996,  -8.2578 >>,1000,TRUE,TRUE)
		
		bcleanup = TRUE
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_SUBMERISIBLE].veh)
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
			SET_SUBMARINE_CRUSH_DEPTHS(mission_veh[MV_SUBMERISIBLE].veh,FALSE,-190,-190,-190)
		ENDIF
	ENDIF

	SWITCH iSetupProgress

		CASE 0  // forces player to be franklin
			REMOVE_SCENARIO_BLOCKING_AREAS()
			ADD_SCENARIO_BLOCKING_AREA(<<549.343506,3923.977539,44.940300>> - <<250.000000,250.000000,29.250000>>, <<549.343506,3923.977539,44.940300>> + <<250.000000,250.000000,29.250000>> )
			ADD_SCENARIO_BLOCKING_AREA(<<-148.382263,4237.474609,44.618195>>-<<250.000000,250.000000,29.250000>>, <<-148.382263,4237.474609,44.618195>> +<<250.000000,250.000000,29.250000>>)
			
			ADD_SCENARIO_BLOCKING_AREA(<<1740.100586,3277.411865,56.610474>> - <<78.250000,78.250000,18.500000>>, <<1740.100586,3277.411865,56.610474>> + <<78.250000,78.250000,18.500000>> )
			
			ADD_SCENARIO_BLOCKING_AREA(<<-2174.845703,5189.527832,51.075100>> - <<250.000000,250.000000,59.000000>>, <<-2174.845703,5189.527832,51.075100>> + <<250.000000,250.000000,59.000000>> )
			ADD_SCENARIO_BLOCKING_AREA( <<-1002.454773,6266.978516,36.125729>> - <<250.000000,250.000000,59.000000>>,  <<-1002.454773,6266.978516,36.125729>> + <<250.000000,250.000000,59.000000>> )
			ADD_SCENARIO_BLOCKING_AREA( <<-1385.118164,5366.789551,37.042431>> - <<250.000000,250.000000,59.000000>>,  <<-1385.118164,5366.789551,37.042431>> + <<250.000000,250.000000,59.000000>> )
			
			SET_WEATHER_TYPE_NOW_PERSIST("extrasunny")		
			
			TRIGGER_MUSIC_EVENT("DH2B_FAIL")
			ROPE_LOAD_TEXTURES()
			DISABLE_TAXI_HAILING(TRUE)
			SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS,TRUE)
			INITALISE_ARRAYS()
			CONTROL_FADE_OUT(200)
			SET_VEHICLE_GEN_AVAILABLE(VEHGEN_DOCKSP2B_CHINOOK, FALSE)
			iprogress = 0
		
			IF estageToLoad = MISSION_STAGE_GET_TO_AIRSTRIP
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
				ELSE
					current_player_model = GET_CURRENT_PLAYER_PED_ENUM()
					ADD_MODEL_REQUEST_TO_ARRAY(CARGOBOB3)
					CLEAR_AREA(<<-1151.1503, -1521.5975, 3.3605>>,100.0,TRUE)
					PRINTSTRING("TAKE_OFF SETUP GOAL 1")
					PRINTNL()
					
					iSetupProgress++
				ENDIF
			ELIF estageToLoad = MISSION_STAGE_PICK_UP_THE_SUB_FROM_AIRSTRIP
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
				ELSE
					SET_BUILDING_STATE(BUILDINGNAME_IPL_AIRFIELD_PROPS, BUILDINGSTATE_DESTROYED)
					current_player_model = GET_CURRENT_PLAYER_PED_ENUM()
					ADD_MODEL_REQUEST_TO_ARRAY(CARGOBOB3)
					ADD_MODEL_REQUEST_TO_ARRAY(SUBMERSIBLE)

					PRINTSTRING("TAKE_OFF SETUP GOAL 1")
					PRINTNL()
					iSetupProgress++
				ENDIF
			ELIF estageToLoad = MISSION_STAGE_FLY_TO_SEA
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
				ELSE
					current_player_model = GET_CURRENT_PLAYER_PED_ENUM()
					ADD_MODEL_REQUEST_TO_ARRAY(DINGHY)
					ADD_MODEL_REQUEST_TO_ARRAY(CARGOBOB3)
					ADD_MODEL_REQUEST_TO_ARRAY(SUBMERSIBLE)

					PRINTSTRING("TAKE_OFF SETUP GOAL 1")
					PRINTNL()
					CLEAR_AREA(<<-833.0740, 6397.1450, -10.1586>>,500,TRUE)
					iSetupProgress++
				ENDIF
			ELIF eStageToLoad = MISSION_STAGE_DROP_THE_SUB
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
				ELSE
					current_player_model = GET_CURRENT_PLAYER_PED_ENUM()
					ADD_MODEL_REQUEST_TO_ARRAY(CARGOBOB3)
					ADD_MODEL_REQUEST_TO_ARRAY(SUBMERSIBLE)

					PRINTSTRING("TAKE_OFF SETUP GOAL 1")
					PRINTNL()
					CLEAR_AREA(<<-833.0740, 6397.1450, -10.1586>>,500,TRUE)
					iSetupProgress++
				ENDIF
			ELIF eStageToLoad = MISSION_STAGE_FIND_THE_CRATE 
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
					SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
				ELSE
					ADD_MODEL_REQUEST_TO_ARRAY(CARGOBOB3)
					ADD_MODEL_REQUEST_TO_ARRAY(SUBMERSIBLE)
					PRINTSTRING("TAKE_OFF SETUP GOAL 1")
					PRINTNL()
					CLEAR_AREA(<<-833.0740, 6397.1450, -10.1586>>,500,TRUE)
					iSetupProgress++
				ENDIF
			ELIF eStageToLoad = MISSION_STAGE_GET_TO_SURFACE
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
					SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
				ELSE
					ADD_MODEL_REQUEST_TO_ARRAY(CARGOBOB3)
					ADD_MODEL_REQUEST_TO_ARRAY(SUBMERSIBLE)
					ADD_MODEL_REQUEST_TO_ARRAY(Prop_Military_Pickup_01 )
					PRINTSTRING("TAKE_OFF SETUP GOAL 1")
					PRINTNL()
					CLEAR_AREA(<<-833.0740, 6397.1450, -10.1586>>,500,TRUE)
					iSetupProgress++
				ENDIF
			ELIF eStageToLoad = MISSION_STAGE_PICK_UP_SUB
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
				ELSE
					ADD_MODEL_REQUEST_TO_ARRAY(CARGOBOB3)
					ADD_MODEL_REQUEST_TO_ARRAY(SUBMERSIBLE)
					ADD_MODEL_REQUEST_TO_ARRAY(Prop_Military_Pickup_01)
					PRINTSTRING("TAKE_OFF SETUP GOAL 1")
					PRINTNL()
					CLEAR_AREA(<<-833.0740, 6397.1450, -10.1586>>,500,TRUE)
					iSetupProgress++
				ENDIF
			ELIF eStageToLoad = MISSION_STAGE_DEFEND_AGAINST_MERRYWEATHER
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
				ELSE
					ADD_MODEL_REQUEST_TO_ARRAY(CARGOBOB3)
					ADD_MODEL_REQUEST_TO_ARRAY(SUBMERSIBLE)
					ADD_MODEL_REQUEST_TO_ARRAY(Prop_Military_Pickup_01 )
					ADD_MODEL_REQUEST_TO_ARRAY(CARGOBOB3)
					PRINTSTRING("TAKE_OFF SETUP GOAL 1")
					PRINTNL()
					CLEAR_AREA(<<-833.0740, 6397.1450, -10.1586>>,500,TRUE)
					iSetupProgress++
				ENDIF
			ELIF eStageToLoad = MISSION_STAGE_FLY_AWAY
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
				ELSE
					ADD_MODEL_REQUEST_TO_ARRAY(CARGOBOB3)
					ADD_MODEL_REQUEST_TO_ARRAY(SUBMERSIBLE)
					ADD_MODEL_REQUEST_TO_ARRAY(Prop_Military_Pickup_01 )
					ADD_MODEL_REQUEST_TO_ARRAY(CARGOBOB3)
					PRINTSTRING("TAKE_OFF SETUP GOAL 1")
					PRINTNL()
					iSetupProgress++
				ENDIF
			ELIF eStageToLoad = MISSION_STAGE_DROP_THE_SUB_AT_THE_AIRSTRIP
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
				ELSE
					ADD_MODEL_REQUEST_TO_ARRAY(CARGOBOB3)
					ADD_MODEL_REQUEST_TO_ARRAY(SUBMERSIBLE)
					ADD_MODEL_REQUEST_TO_ARRAY(Prop_Military_Pickup_01 )
					ADD_MODEL_REQUEST_TO_ARRAY(CARGOBOB3)
					PRINTSTRING("TAKE_OFF SETUP GOAL 1")
					PRINTNL()
					iSetupProgress++
				ENDIF	
			ELIF eStageToLoad = MISSION_STAGE_CLOSING_CUTSCENE
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
				ELSE
					ADD_MODEL_REQUEST_TO_ARRAY(CARGOBOB3)
					ADD_MODEL_REQUEST_TO_ARRAY(SUBMERSIBLE)
					ADD_MODEL_REQUEST_TO_ARRAY(Prop_Military_Pickup_01 )
					ADD_MODEL_REQUEST_TO_ARRAY(CARGOBOB3)
					PRINTSTRING("CLOSING CUTSCENE")PRINTNL()
					PRINTNL()
					iSetupProgress++
				ENDIF	
			ELIF eStageToLoad > MISSION_STAGE_CLOSING_CUTSCENE
				PRINTSTRING("POTENTIALLY BAD LOAD STAGE")PRINTNL()
				iSetupProgress++
			ENDIF
			
		BREAK

		CASE 1  // 
		
		
		IF ARE_REQUESTED_MODELS_LOADED() 
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
				PRINTSTRING("MIKE OUTFIT SET")PRINTNL()
				//SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH, FALSE)
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				PRINTSTRING("FRANK OUTFIT SET")PRINTNL()
				//SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_STEALTH, FALSE)
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
				PRINTSTRING("TREVOR OUTFIT SET")PRINTNL()
				//SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P2_STEALTH, FALSE)
			ENDIF
			
			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)                //set that the player has changed clothes on mission
			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR)                //set that the player has changed clothes on mission
			SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_FRANKLIN)                //set that the player has changed clothes on mission
			
			IF eStageToLoad = MISSION_STAGE_GET_TO_AIRSTRIP
				//mission vehicle to fly to the sea
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_MISSION_HELI,<<1750.6334, 3285.0278, 40.0871>>,134.9916)	
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR,<<-1155.21, -1517.32, 3.36>>)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_FRANKLIN,<<-1157.92, -1519.33, 3.36>>)
					WAIT(0)
				ENDWHILE
				
				IF NOT IS_PLAYER_VEHICLE_IN_AREA(CHAR_TREVOR, VEHICLE_TYPE_CAR, <<-1154.7112, -1520.1177, 3.3521>>, 60)
					IF NOT DOES_ENTITY_EXIST(mission_veh[ENUM_TO_INT(MV_TREVORS_TRUCK)].veh)
						IF NOT IS_POSITION_OCCUPIED(<<-1155.5077, -1520.1217, 3.3467>>,5,FALSE,TRUE,FALSE,FALSE,FALSE)
							WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_TREVORS_TRUCK, <<-1155.5077, -1520.1217, 3.3467>>, 35.3350)
								WAIT(0)
							ENDWHILE
						ELSE
							IF NOT IS_POSITION_OCCUPIED(<<-1158.4879, -1529.3704, 3.2503>>,5,FALSE,TRUE,FALSE,FALSE,FALSE)
								WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_TREVORS_TRUCK, <<-1158.4879, -1529.3704, 3.2503>>, 35.6275)
									WAIT(0)
								ENDWHILE
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,RELGROUPHASH_PLAYER)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_PLAYER,rel_group_buddies)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-1149.6552, -1521.8536, 3.3298>>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(),76.5150)
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
					SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],<<-1148.9454, -1523.2805, 3.3434>>)
					SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],48.7325)
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
					SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],<<-1148.6027, -1525.5420, 3.3486>>)
					SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],79.3802)
				ENDIF
				
				INITALISE_ARRAYS()
				CLEANUP_LOADED_MODEL_ARRAY()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				
				SETUP_PED_OUTFITS()
				GIVE_ALL_PEDS_AN_EARPIECE()
				
				eMissionStage = MISSION_STAGE_GET_TO_AIRSTRIP
				bmissionstageloaded = TRUE
				iSetupProgress = 0
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP()
				ELSE
					LOAD_SCENE(<<-1149.6552, -1521.8536, 3.3298>>)				
				ENDIF
				
				#IF IS_DEBUG_BUILD
					bskipping = FALSE
				#ENDIF	
			ELIF eStageToLoad = MISSION_STAGE_PICK_UP_THE_SUB_FROM_AIRSTRIP
				
				//mission vehicle to fly to the sea
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_MISSION_HELI_WITHOUT_ROPE,<<1750.6334, 3285.0278, 40.0871>>,134.9916)	
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_SUB_ON_TRUCK_ON_RUNWAY,<<1728.97, 3264.86, 40.0871>>,116.37)	
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR,<< -1158.3065, -1518.4401, 9.6346 >>,237.8801)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_FRANKLIN,<< -1154.9404, -1521.8251, 9.6346 >>,113.9594)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_FLOYD,<<1728.44, 3294.27, 41.22>>,-112.87)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_WADE,<<1727.38, 3293.09, 41.20>>,-144.39)
					WAIT(0)
				ENDWHILE

				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
					ENDIF
				ENDIF

				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[MV_CHINOOK].veh,VS_FRONT_RIGHT)
					ENDIF
				ENDIF
	
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,RELGROUPHASH_PLAYER)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_PLAYER,rel_group_buddies)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					SET_VEHICLE_ENGINE_ON(mission_veh[MV_SUBMERISIBLE].veh,FALSE,TRUE)
				ENDIF
				
				CLEAR_AREA(<<1750.6334, 3285.0278, 40.0871>>,1000,TRUE,TRUE)
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP(mission_veh[MV_CHINOOK].veh)
				ELSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh,VS_DRIVER)
					ENDIF
					LOAD_SCENE(<<1750.6334, 3285.0278, 40.0871>>)			
				ENDIF
				
				INITALISE_ARRAYS()
				CLEANUP_LOADED_MODEL_ARRAY()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_LABEL_AS_TRIGGERED("ASSIGN TREVOR GET IN SUB",TRUE)
				TRIGGER_MUSIC_EVENT("DH2B_PICK_UP_RT")
				
				SETUP_PED_OUTFITS()
				GIVE_ALL_PEDS_AN_EARPIECE()
				
				eMissionStage = MISSION_STAGE_PICK_UP_THE_SUB_FROM_AIRSTRIP
				bmissionstageloaded = TRUE
				iSetupProgress = 0
				
				#IF IS_DEBUG_BUILD
					bskipping = FALSE
				#ENDIF	
			
			ELIF eStageToLoad = MISSION_STAGE_FLY_TO_SEA
				//mission vehicle to fly to the sea
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_HELI_WITH_SUB,<<910.1729, 3335.7280, 270.3783>>  ,39.5182)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR,<< -1158.3065, -1518.4401, 9.6346 >>,237.8801)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_FRANKLIN,<< -1154.9404, -1521.8251, 9.6346 >>,113.9594)
					WAIT(0)
				ENDWHILE
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[1].veh,VS_DRIVER)
					ENDIF
				ENDIF

				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[0].veh,VS_FRONT_RIGHT)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(objSeabed[0])
					FREEZE_ENTITY_POSITION(objSeabed[0],TRUE)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					SET_VEHICLE_ENGINE_ON(mission_veh[MV_SUBMERISIBLE].veh,FALSE,TRUE)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					SET_HELI_BLADES_FULL_SPEED(mission_veh[MV_CHINOOK].veh)
					SET_VEHICLE_ENGINE_ON(mission_veh[MV_CHINOOK].veh,TRUE,TRUE)
				ENDIF
			
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,RELGROUPHASH_PLAYER)
				SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_PLAYER,rel_group_buddies)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP(mission_veh[MV_CHINOOK].veh)
				ELSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh,VS_DRIVER)
					ENDIF
					LOAD_SCENE(<<910.1729, 3335.7280, 270.3783>>)			
				ENDIF
				
				INITALISE_ARRAYS()
				CLEANUP_LOADED_MODEL_ARRAY()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				TRIGGER_MUSIC_EVENT("DH2B_FLY_SEA_RT")
				
				SETUP_PED_OUTFITS()
				GIVE_ALL_PEDS_AN_EARPIECE()
				
				eMissionStage = MISSION_STAGE_FLY_TO_SEA
				bmissionstageloaded = TRUE
				iSetupProgress = 0
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
					FREEZE_ENTITY_POSITION(mission_veh[0].veh,FALSE)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					FREEZE_ENTITY_POSITION(mission_veh[1].veh,FALSE)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
					SET_VEHICLE_FORWARD_SPEED(mission_veh[0].veh,20)
				ENDIF
				
				#IF IS_DEBUG_BUILD
					bskipping = FALSE
				#ENDIF	
				
			ELIF eStageToLoad = MISSION_STAGE_DROP_THE_SUB
				
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_HELI_WITH_SUB,<<-1389.3110, 6163.1890,  40.3783>>  ,39.5182)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR,<< -1158.3065, -1518.4401, 9.6346 >>,237.8801)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_FRANKLIN,<< -1154.9404, -1521.8251, 9.6346 >>,113.9594)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_CARGO,<<-1255.3800, 6795.7202, -179.1137>>,0)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_MERRYWEATHER_RIG,<<0,0,0>>)
					WAIT(0)
				ENDWHILE
				
				HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[1].veh,VS_DRIVER)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
					SET_HELI_BLADES_FULL_SPEED(mission_veh[0].veh)
					FREEZE_ENTITY_POSITION(mission_veh[0].veh,TRUE)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					SET_HELI_BLADES_FULL_SPEED(mission_veh[MV_CHINOOK].veh)
					SET_VEHICLE_ENGINE_ON(mission_veh[MV_CHINOOK].veh,TRUE,TRUE)
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[0].veh,VS_FRONT_RIGHT)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(objSeabed[0])
					FREEZE_ENTITY_POSITION(objSeabed[0],TRUE)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					SET_VEHICLE_ENGINE_ON(mission_veh[MV_SUBMERISIBLE].veh,FALSE,TRUE)
				ENDIF
				
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
					FREEZE_ENTITY_POSITION(mission_veh[0].veh,FALSE)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					FREEZE_ENTITY_POSITION(mission_veh[1].veh,FALSE)
				ENDIF
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP(mission_veh[MV_CHINOOK].veh)
				ELSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh,VS_DRIVER)
					ENDIF
					LOAD_SCENE(<<-1389.3110, 6163.1890,  40.3783>>)			
				ENDIF
				
				INITALISE_ARRAYS()
				CLEANUP_LOADED_MODEL_ARRAY()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				TRIGGER_MUSIC_EVENT("DH2B_DROP_SUB_RT")
				
				SETUP_PED_OUTFITS()
				GIVE_ALL_PEDS_AN_EARPIECE()
				
				eMissionStage = MISSION_STAGE_DROP_THE_SUB
				bmissionstageloaded = TRUE
				iSetupProgress = 0
				
				#IF IS_DEBUG_BUILD
					bskipping = FALSE
				#ENDIF

			ELIF eStageToLoad = MISSION_STAGE_FIND_THE_CRATE
				
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_MISSION_HELI,<<-1388.26, 6160.88, 370.3783>>  ,39.5182)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_SUBMERSIBLE,<<-1402.3759, 6145.0410, -12.5895>>  ,39.5182)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_MICHAEL,<< -1158.3065, -1518.4401, 9.6346 >>,237.8801)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_FRANKLIN,<< -1154.9404, -1521.8251, 9.6346 >>,113.9594)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_CARGO,<<-1255.3800, 6795.7202, -179.1137>>,0)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_MERRYWEATHER_RIG,<<0,0,0>>)
					WAIT(0)
				ENDWHILE
				
				HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],mission_veh[0].veh,VS_DRIVER)
					ENDIF
				ENDIF

				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[0].veh,VS_BACK_LEFT)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(objSeabed[0])
					FREEZE_ENTITY_POSITION(objSeabed[0],TRUE)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					FREEZE_ENTITY_POSITION(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
					SET_ENTITY_INVINCIBLE(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
					APPLY_FORCE_TO_ENTITY(mission_veh[MV_SUBMERISIBLE].veh, APPLY_TYPE_IMPULSE, <<0,0,-1>>, <<0,0,0>>,0, TRUE, TRUE, FALSE)
					SET_VEHICLE_ENGINE_ON(mission_veh[MV_SUBMERISIBLE].veh,TRUE,TRUE)
					SET_VEHICLE_FORWARD_SPEED(mission_veh[MV_SUBMERISIBLE].veh,8)
				ENDIF
				
				IF DOES_ENTITY_EXIST(mission_veh[MV_SUBMERISIBLE].veh)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
				ENDIF
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP(mission_veh[MV_SUBMERISIBLE].veh)
				ELSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
					ENDIF
					LOAD_SCENE(<<-1402.3759, 6145.0410, -12.5895>>)			
				ENDIF

				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
				
				INITALISE_ARRAYS()
				CLEANUP_LOADED_MODEL_ARRAY()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				TRIGGER_MUSIC_EVENT("DH2B_FIND_CONT_RT")
				SETUP_PED_OUTFITS()
				GIVE_ALL_PEDS_AN_EARPIECE()
				eMissionStage = MISSION_STAGE_FIND_THE_CRATE
				bmissionstageloaded = TRUE
				iSetupProgress = 0
				
				#IF IS_DEBUG_BUILD
					bskipping = FALSE
				#ENDIF	
			
			ELIF eStageToLoad = MISSION_STAGE_GET_TO_SURFACE
				
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_MISSION_HELI,<<-1388.26, 6160.88, 370.3783>>  ,39.5182)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_MICHAEL,<< -1158.3065, -1518.4401, 9.6346 >>,237.8801)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_FRANKLIN,<< -1154.9404, -1521.8251, 9.6346 >>,113.9594)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_CARGO,<<-1255.3800, 6795.7202, -179.1137>>,0)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_MERRYWEATHER_RIG,<<0,0,0>>)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_SUBMERSIBLE,<< -1255.28, 6795.65, -135.63 >>  ,39.5182)
					WAIT(0)
				ENDWHILE

				HANG_UP_AND_PUT_AWAY_PHONE(TRUE)

				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],mission_veh[0].veh,VS_DRIVER)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					FREEZE_ENTITY_POSITION(mission_veh[1].veh,TRUE)
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[0].veh,VS_BACK_LEFT)
					ENDIF
				ENDIF
				
				ATTACH_BOMB_TO_SUB_PHYSICALLY()
				/*
				IF DOES_ENTITY_EXIST(mission_veh[MV_SUBMERISIBLE].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
						IF DOES_ENTITY_EXIST(objSeabed[1])
							IF NOT IS_ENTITY_ATTACHED(objSeabed[1])
								ATTACH_ENTITY_TO_ENTITY(objSeabed[1],mission_veh[1].veh,1,<<0,-2.0,-1.7>>,<<0,0,90>>,TRUE,FALSE,TRUE)
								SET_ENTITY_NO_COLLISION_ENTITY(objSeabed[1],mission_veh[MV_SUBMERISIBLE].veh,FALSE)
								SET_ENTITY_NO_COLLISION_ENTITY(mission_veh[MV_SUBMERISIBLE].veh,objSeabed[1],FALSE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				*/
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
					SET_VEHICLE_LIGHTS(mission_veh[MV_SUBMERISIBLE].veh, SET_VEHICLE_LIGHTS_ON)
				ENDIF
					
				IF DOES_ENTITY_EXIST(mission_veh[MV_SUBMERISIBLE].veh)
					SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
				ENDIF
				
				IF DOES_ENTITY_EXIST(objSeabed[0])
					FREEZE_ENTITY_POSITION(objSeabed[0],TRUE)
				ENDIF

				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP(mission_veh[MV_SUBMERISIBLE].veh)
				ELSE
					LOAD_SCENE(<< -1255.28, 6795.65, -135.63 >>)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					FREEZE_ENTITY_POSITION(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
					APPLY_FORCE_TO_ENTITY(mission_veh[MV_SUBMERISIBLE].veh,APPLY_TYPE_EXTERNAL_IMPULSE,<<0,0,0.1>>,<<0,0,0>>,0,TRUE,TRUE,TRUE)
				ENDIF
				
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
				
				INITALISE_ARRAYS()
				CLEANUP_LOADED_MODEL_ARRAY()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				TRIGGER_MUSIC_EVENT("DH2B_GET_SURFACE_RT")
				SETUP_PED_OUTFITS()
				GIVE_ALL_PEDS_AN_EARPIECE()
				eMissionStage = MISSION_STAGE_GET_TO_SURFACE
				bmissionstageloaded = TRUE
				iSetupProgress = 0
				
				#IF IS_DEBUG_BUILD
					bskipping = FALSE
				#ENDIF
			
			ELIF eStageToLoad = MISSION_STAGE_PICK_UP_SUB
				//mission vehicle to fly to the sea
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_MISSION_HELI,<<-1552.4729, 6488.5664, 118.6260>>  ,347.6931)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_SUBMERSIBLE,<< -1255.28, 6795.65,  0.34 >>  ,39.5182)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR,<< -1158.3065, -1518.4401, 9.6346 >>,237.8801)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_FRANKLIN,<< -1154.9404, -1521.8251, 9.6346 >>,113.9594)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_CARGO,<<-1255.3800, 6795.7202, -179.1137>>,0)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_MERRYWEATHER_RIG,<<0,0,0>>)
					WAIT(0)
				ENDWHILE
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID()) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						SET_VEHICLE_FORWARD_SPEED(mission_veh[0].veh,20)
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					SET_HELI_BLADES_FULL_SPEED(mission_veh[MV_CHINOOK].veh)
					SET_VEHICLE_ENGINE_ON(mission_veh[MV_CHINOOK].veh,TRUE,TRUE)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[1].veh,VS_DRIVER)
						FREEZE_ENTITY_POSITION(mission_veh[1].veh,TRUE)
					ENDIF
				ENDIF

				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					IF DOES_ENTITY_EXIST(objSeabed[1])
						IF NOT IS_ENTITY_ATTACHED(objSeabed[1])
							ATTACH_ENTITY_TO_ENTITY(objSeabed[1],mission_veh[1].veh,1,<<0,-2.0,-1.7>>,<<0,0,90>>,TRUE,FALSE,TRUE)
							SET_ENTITY_NO_COLLISION_ENTITY(objSeabed[1],mission_veh[MV_SUBMERISIBLE].veh,FALSE)
							SET_ENTITY_NO_COLLISION_ENTITY(mission_veh[MV_SUBMERISIBLE].veh,objSeabed[1],FALSE)
						ENDIF
					ENDIF
					
					FREEZE_ENTITY_POSITION(mission_veh[MV_SUBMERISIBLE].veh, FALSE)
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[0].veh,VS_BACK_LEFT)
					ENDIF
				ENDIF
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP(mission_veh[MV_CHINOOK].veh)
				ELSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh,VS_DRIVER)
					ENDIF
					LOAD_SCENE(<<-1552.4729, 6488.5664, 118.6260>>)			
				ENDIF
				
			
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
				
				INITALISE_ARRAYS()
				CLEANUP_LOADED_MODEL_ARRAY()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				TRIGGER_MUSIC_EVENT("DH2B_PICK_SUB_RT")
				SETUP_PED_OUTFITS()
				GIVE_ALL_PEDS_AN_EARPIECE()
				eMissionStage = MISSION_STAGE_PICK_UP_SUB
				bmissionstageloaded = TRUE
				iSetupProgress = 0
				
				#IF IS_DEBUG_BUILD
					bskipping = FALSE
				#ENDIF
			ELIF eStageToLoad = MISSION_STAGE_DEFEND_AGAINST_MERRYWEATHER
				
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_HELI_WITH_SUB,<<-1571.9094, 6204.8730, 40.3783>>  , -137.9830)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR,<< -1158.3065, -1518.4401, 9.6346 >>,237.8801)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_FRANKLIN,<< -1154.9404, -1521.8251, 9.6346 >>,113.9594)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_CARGO,<<-1255.3800, 6795.7202, -179.1137>>,0)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_MERRYWEATHER_RIG,<<0,0,0>>)
					WAIT(0)
				ENDWHILE

				IF NOT IS_PED_INJURED(PLAYER_PED_ID()) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						//SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh,VS_DRIVER)
						SET_VEHICLE_ENGINE_ON(mission_veh[MV_CHINOOK].veh,TRUE,TRUE)
						SET_HELI_BLADES_FULL_SPEED(mission_veh[MV_CHINOOK].veh)
						SET_VEHICLE_FORWARD_SPEED(mission_veh[MV_CHINOOK].veh,10)
					ENDIF
				ENDIF
				
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[0].veh,VS_BACK_LEFT)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
					ENDIF
				ENDIF

				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					IF DOES_ENTITY_EXIST(objSeabed[1])
						IF NOT IS_ENTITY_ATTACHED(objSeabed[1])
							ATTACH_ENTITY_TO_ENTITY(objSeabed[1],mission_veh[1].veh,1,<<0,-2.0,-1.7>>,<<0,0,90>>,TRUE,FALSE,TRUE)
							SET_ENTITY_NO_COLLISION_ENTITY(objSeabed[1],mission_veh[MV_SUBMERISIBLE].veh,FALSE)
							SET_ENTITY_NO_COLLISION_ENTITY(mission_veh[MV_SUBMERISIBLE].veh,objSeabed[1],FALSE)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
					FREEZE_ENTITY_POSITION(mission_veh[0].veh,FALSE)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					FREEZE_ENTITY_POSITION(mission_veh[1].veh,FALSE)
				ENDIF
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP(mission_veh[MV_CHINOOK].veh)
				ELSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh,VS_DRIVER)
					ENDIF
					LOAD_SCENE(<<-1571.9094, 6204.8730, 40.3783>>)			
				ENDIF

				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
				
				INITALISE_ARRAYS()
				CLEANUP_LOADED_MODEL_ARRAY()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				TRIGGER_MUSIC_EVENT("DH2B_FLY_AWAY_RT")
				SETUP_PED_OUTFITS()
				GIVE_ALL_PEDS_AN_EARPIECE()
				eMissionStage = MISSION_STAGE_DEFEND_AGAINST_MERRYWEATHER
				bmissionstageloaded = TRUE
				iSetupProgress = 0
				
				
				
				#IF IS_DEBUG_BUILD
					bskipping = FALSE
				#ENDIF	
				
			ELIF eStageToLoad = MISSION_STAGE_FLY_AWAY
				
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_HELI_WITH_SUB,<<-152.3409, 4654.3423, 261.3565>>, 219.9027)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR,<< -1158.3065, -1518.4401, 9.6346 >>,237.8801)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_FRANKLIN,<< -1154.9404, -1521.8251, 9.6346 >>,113.9594)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_CARGO,<<-1255.3800, 6795.7202, -179.1137>>,0)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_MERRYWEATHER_RIG,<<0,0,0>>)
					WAIT(0)
				ENDWHILE

				IF NOT IS_PED_INJURED(PLAYER_PED_ID()) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						//SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh,VS_DRIVER)
						SET_VEHICLE_ENGINE_ON(mission_veh[MV_CHINOOK].veh,TRUE,TRUE)
						SET_HELI_BLADES_FULL_SPEED(mission_veh[MV_CHINOOK].veh)
						SET_VEHICLE_FORWARD_SPEED(mission_veh[MV_CHINOOK].veh,10)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[0].veh,VS_BACK_LEFT)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
					ENDIF
				ENDIF

				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					IF DOES_ENTITY_EXIST(objSeabed[1])
						IF NOT IS_ENTITY_ATTACHED(objSeabed[1])
							ATTACH_ENTITY_TO_ENTITY(objSeabed[1],mission_veh[1].veh,1,<<0,-2.0,-1.7>>,<<0,0,90>>,TRUE,FALSE,TRUE)
							SET_ENTITY_NO_COLLISION_ENTITY(objSeabed[1],mission_veh[MV_SUBMERISIBLE].veh,FALSE)
							SET_ENTITY_NO_COLLISION_ENTITY(mission_veh[MV_SUBMERISIBLE].veh,objSeabed[1],FALSE)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
					FREEZE_ENTITY_POSITION(mission_veh[0].veh,FALSE)
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					FREEZE_ENTITY_POSITION(mission_veh[1].veh,FALSE)
				ENDIF
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP(mission_veh[MV_CHINOOK].veh)
				ELSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh,VS_DRIVER)
					ENDIF
					LOAD_SCENE(<<-152.3409, 4654.3423, 261.3565>>)			
				ENDIF

				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
				
				INITALISE_ARRAYS()
				CLEANUP_LOADED_MODEL_ARRAY()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				TRIGGER_MUSIC_EVENT("DH2B_FLY_AWAY_RT")
				SETUP_PED_OUTFITS()
				GIVE_ALL_PEDS_AN_EARPIECE()
				eMissionStage = MISSION_STAGE_FLY_AWAY
				bmissionstageloaded = TRUE
				iSetupProgress = 0
				
				
				#IF IS_DEBUG_BUILD
					bskipping = FALSE
				#ENDIF	
					
			ELIF eStageToLoad = MISSION_STAGE_DROP_THE_SUB_AT_THE_AIRSTRIP
				
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_HELI_WITH_SUB,<<1748.0168, 3283.6812, 95.0743>>  ,186.6673)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR,<< -1158.3065, -1518.4401, 9.6346 >>,237.8801)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_FRANKLIN,<< -1154.9404, -1521.8251, 9.6346 >>,113.9594)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_CARGO,<<-1255.3800, 6795.7202, -179.1137>>,0)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_MERRYWEATHER_RIG,<<0,0,0>>)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_LESTER_FOR_CUTSCENE,<<0,0,0>>,0)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_WADE_FOR_CUTSCENE,<<0,0,0>>,0)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_LESTER_CAR_FOR_CUTSCENE,<<0,0,0>>,0)
					WAIT(0)
				ENDWHILE

				IF NOT IS_PED_INJURED(PLAYER_PED_ID()) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						//SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh,VS_DRIVER)
						SET_HELI_BLADES_FULL_SPEED(mission_veh[MV_CHINOOK].veh)
						SET_VEHICLE_FORWARD_SPEED(mission_veh[MV_CHINOOK].veh,10)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[MV_CHINOOK].veh,VS_BACK_LEFT)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
					ENDIF
				ENDIF

				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					IF DOES_ENTITY_EXIST(objSeabed[1])
						IF NOT IS_ENTITY_ATTACHED(objSeabed[1])
							ATTACH_ENTITY_TO_ENTITY(objSeabed[1],mission_veh[1].veh,1,<<0,-2.0,-1.7>>,<<0,0,90>>,TRUE,FALSE,TRUE)
							SET_ENTITY_NO_COLLISION_ENTITY(objSeabed[1],mission_veh[MV_SUBMERISIBLE].veh,FALSE)
							SET_ENTITY_NO_COLLISION_ENTITY(mission_veh[MV_SUBMERISIBLE].veh,objSeabed[1],FALSE)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP(mission_veh[MV_CHINOOK].veh)
				ELSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh,VS_DRIVER)
					ENDIF
					LOAD_SCENE(<<1748.0168, 3283.6812, 95.0743>>)			
				ENDIF

				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
				
				INITALISE_ARRAYS()
				CLEANUP_LOADED_MODEL_ARRAY()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				TRIGGER_MUSIC_EVENT("DH2B_FLY_AWAY_RT")
				SETUP_PED_OUTFITS()
				GIVE_ALL_PEDS_AN_EARPIECE()
				eMissionStage = MISSION_STAGE_DROP_THE_SUB_AT_THE_AIRSTRIP
				bmissionstageloaded = TRUE
				iSetupProgress = 0
				
				#IF IS_DEBUG_BUILD
					bskipping = FALSE
				#ENDIF	
			ELIF eStageToLoad = MISSION_STAGE_CLOSING_CUTSCENE
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_HELI_WITH_SUB,<<1748.0168, 3283.6812, 95.0743>>  ,186.6673)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR,<< -1158.3065, -1518.4401, 9.6346 >>,237.8801)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_FRANKLIN,<< -1154.9404, -1521.8251, 9.6346 >>,113.9594)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_CARGO,<<-1255.3800, 6795.7202, -179.1137>>,0)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_LESTER_FOR_CUTSCENE,<<0,0,0>>,0)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_WADE_FOR_CUTSCENE,<<0,0,0>>,0)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_LESTER_CAR_FOR_CUTSCENE,<<0,0,0>>,0)
					WAIT(0)
				ENDWHILE

				IF NOT IS_PED_INJURED(PLAYER_PED_ID()) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						SET_HELI_BLADES_FULL_SPEED(mission_veh[MV_CHINOOK].veh)
						SET_VEHICLE_FORWARD_SPEED(mission_veh[MV_CHINOOK].veh,10)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[MV_CHINOOK].veh,VS_BACK_LEFT)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
					ENDIF
				ENDIF

				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					IF DOES_ENTITY_EXIST(objSeabed[1])
						IF NOT IS_ENTITY_ATTACHED(objSeabed[1])
							ATTACH_ENTITY_TO_ENTITY(objSeabed[1],mission_veh[1].veh,1,<<0,-2.0,-1.7>>,<<0,0,90>>,TRUE,FALSE,TRUE)
							SET_ENTITY_NO_COLLISION_ENTITY(objSeabed[1],mission_veh[MV_SUBMERISIBLE].veh,FALSE)
							SET_ENTITY_NO_COLLISION_ENTITY(mission_veh[MV_SUBMERISIBLE].veh,objSeabed[1],FALSE)
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					IF IS_ENTITY_ATTACHED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					AND NOT IS_PED_IN_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						DETACH_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					ENDIF
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					DETACH_ENTITY(mission_veh[MV_SUBMERISIBLE].veh)
					CLEAR_AREA(GET_ENTITY_COORDS(mission_veh[MV_SUBMERISIBLE].veh),100,TRUE)
					SET_ENTITY_COORDS_NO_OFFSET(mission_veh[MV_SUBMERISIBLE].veh, <<1763.7795, 3289.0815, 41.8658>>)
					SET_ENTITY_HEADING(mission_veh[MV_SUBMERISIBLE].veh, 143.22)
					SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[MV_SUBMERISIBLE].veh)
				ENDIF
				
				IF IS_REPLAY_BEING_SET_UP()
					END_REPLAY_SETUP(mission_veh[MV_CHINOOK].veh)
				ELSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh,VS_DRIVER)
					ENDIF
					LOAD_SCENE(<<1755.7994, 3275.8169, 40.1628>>)			
				ENDIF
				
				REQUEST_CUTSCENE("LSDH_2B_MCS_1")

				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
				
				INITALISE_ARRAYS()
				CLEANUP_LOADED_MODEL_ARRAY()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SETUP_PED_OUTFITS()
				GIVE_ALL_PEDS_AN_EARPIECE()
				eMissionStage = MISSION_STAGE_CLOSING_CUTSCENE
				bmissionstageloaded = TRUE
				iSetupProgress = 0
				
				#IF IS_DEBUG_BUILD
					bskipping = FALSE
				#ENDIF	
			ENDIF
		ENDIF
		
		BREAK
		
	ENDSWITCH

ENDPROC
//
//PROC RUN_INTRO_CUTSCENE()
//	
//	#IF IS_DEBUG_BUILD
//		IF IS_CUTSCENE_ACTIVE()
//			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
//				STOP_CUTSCENE()
//				REMOVE_CUTSCENE()
//				WHILE IS_CUTSCENE_ACTIVE()
//					WAIT(0)
//				ENDWHILE
//			ENDIF
//		ENDIF
//	#ENDIF
//	
//	//TREVOR: Play cutscene in full
//	//MICHAEL: Play cutscene starting in section #2
//	//FRANKLIN: Play Cutscene starting in section #3
//	
//	IF NOT b_cutscene_loaded
//		IF NOT IS_CUTSCENE_ACTIVE()
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//				REQUEST_CUTSCENE("LSDH_2B_INT")
//			ENDIF
//			
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("LSDH_2B_INT",CS_SECTION_2 | CS_SECTION_3)
//			ENDIF
//			
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("LSDH_2B_INT",CS_SECTION_3)
//			ENDIF
//
//			WAIT(0)
//		ENDIF
//		IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
//			CLEAR_AREA(<< -1154.6117, -1520.0699, 3.3456 >>,100,TRUE)
//			SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("LSDH_2B_INT",GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_DOCKS,1),CREW_OUTFIT_DEFAULT)
//			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//			
//			IF NOT IS_PED_INJURED(Buddy[1].ped)
//				REGISTER_ENTITY_FOR_CUTSCENE(Buddy[1].ped, "gunman_selection_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//			ENDIF
//			
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
//				
//			ELSE
//				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
//					REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//					PRINTSTRING("REGISTER MIKE")PRINTNL()
//				ENDIF
//			ENDIF
//			
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//				
//			ELSE
//				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
//					REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//					PRINTSTRING("REGISTER FRANKLIN")PRINTNL()
//				ENDIF
//			ENDIF
//			
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
//				
//			ELSE
//				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
//					REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//					PRINTSTRING("REGISTER TREVOR")PRINTNL()
//				ENDIF
//			ENDIF
//			
//			START_CUTSCENE()
//		
//			b_cutscene_loaded = TRUE
//		ENDIF
//	ELSE
//		IF NOT IS_CUTSCENE_ACTIVE()
//			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
//				SET_CURRENT_SELECTOR_PED(SELECTOR_PED_TREVOR)
//			ELSE
//				//cutscene finished, back to game
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//				//CONTROL_FADE_OUT(500)
//				b_cutscene_loaded = FALSE
//				REMOVE_CUTSCENE()
//			ENDIF
//		ENDIF
//	ENDIF
//ENDPROC

BOOL in_car_conv_started
BOOL in_car_conv_on_hold
TEXT_LABEL_31 sConvResumeLabel
STRING subGroupID = "D2BAUD"

PROC MANAGE_CONVERSATION(STRING label, BOOL bCanTalk)
    IF bCanTalk
        IF NOT in_car_conv_started
            IF CREATE_CONVERSATION(sSpeech, subGroupID, label, CONV_PRIORITY_HIGH)
	            in_car_conv_on_hold = FALSE
	            in_car_conv_started = TRUE
            ENDIF
        ENDIF
        IF in_car_conv_on_hold
            IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(sSpeech, subGroupID, label, sConvResumeLabel, CONV_PRIORITY_HIGH)
                in_car_conv_on_hold = FALSE
            ENDIF
        ENDIF
    ENDIF
    IF NOT bCanTalk
        IF NOT in_car_conv_on_hold
            sConvResumeLabel = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION() 
            KILL_FACE_TO_FACE_CONVERSATION()
            in_car_conv_on_hold = TRUE
        ENDIF
    ENDIF
ENDPROC


INT iBumpsDialogue
INT iBumpsTimer
INT iBumpsRand

PROC MANAGE_BUMPS_AND_BANG_DIALOGUE()

	SWITCH iBumpsDialogue
		
		CASE 0
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(mission_veh[MV_SUBMERISIBLE].veh)
					iBumpsRand = GET_RANDOM_INT_IN_RANGE(1,8)
					IF iBumpsRand = 1
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_BANG1",CONV_PRIORITY_HIGH)
								iBumpsTimer = GET_GAME_TIMER()
								iBumpsDialogue ++
							ENDIF
						ENDIF
					ELIF iBumpsRand = 2
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_BANG2",CONV_PRIORITY_HIGH)
								iBumpsTimer = GET_GAME_TIMER()
								iBumpsDialogue ++
							ENDIF
						ENDIF
					ELIF iBumpsRand = 3
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_BANG3",CONV_PRIORITY_HIGH)
								iBumpsTimer = GET_GAME_TIMER()
								iBumpsDialogue ++
							ENDIF
						ENDIF
					ELIF iBumpsRand = 4
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_BANG4",CONV_PRIORITY_HIGH)
								iBumpsTimer = GET_GAME_TIMER()
								iBumpsDialogue ++
							ENDIF
						ENDIF
					ELIF iBumpsRand = 5
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_BANG5",CONV_PRIORITY_HIGH)
								iBumpsTimer = GET_GAME_TIMER()
								iBumpsDialogue ++
							ENDIF
						ENDIF
					
					ELIF iBumpsRand = 6
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_BANG6",CONV_PRIORITY_HIGH)
								iBumpsTimer = GET_GAME_TIMER()
								iBumpsDialogue ++
							ENDIF
						ENDIF
					ELIF iBumpsRand = 7
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_GENBANG",CONV_PRIORITY_HIGH)
								iBumpsTimer = GET_GAME_TIMER()
								iBumpsDialogue ++
							ENDIF
						ENDIF
					ENDIF
					
					
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_COOLMC",CONV_PRIORITY_HIGH)
					iBumpsTimer = GET_GAME_TIMER()
					iBumpsDialogue ++
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 2
			
			IF MANAGE_MY_TIMER(iBumpsTimer,GET_RANDOM_INT_IN_RANGE(10000,16000))
				iBumpsDialogue = 0
			ENDIF
		
		BREAK
	
	ENDSWITCH
	
ENDPROC


// *********************** MISSION STAGES AND CUTSCENES AS PROCEDURES **********************

//PURPOSE: Requests and creates any assets etc that are initally required for the mission stage then holds in the load mission stage loop until complete
PROC MISSION_SETUP()
	SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1.0)
	REMOVE_SCENARIO_BLOCKING_AREAS()
	ADD_SCENARIO_BLOCKING_AREA(<<549.343506,3923.977539,44.940300>> - <<250.000000,250.000000,29.250000>>, <<549.343506,3923.977539,44.940300>> + <<250.000000,250.000000,29.250000>> )
	ADD_SCENARIO_BLOCKING_AREA(<<-148.382263,4237.474609,44.618195>>-<<250.000000,250.000000,29.250000>>, <<-148.382263,4237.474609,44.618195>> +<<250.000000,250.000000,29.250000>>)
	
	ADD_SCENARIO_BLOCKING_AREA(<<1740.100586,3277.411865,56.610474>> - <<78.250000,78.250000,18.500000>>, <<1740.100586,3277.411865,56.610474>> + <<78.250000,78.250000,18.500000>> )
	
	ADD_SCENARIO_BLOCKING_AREA(<<-2174.845703,5189.527832,51.075100>> - <<250.000000,250.000000,59.000000>>, <<-2174.845703,5189.527832,51.075100>> + <<250.000000,250.000000,59.000000>> )
	ADD_SCENARIO_BLOCKING_AREA( <<-1002.454773,6266.978516,36.125729>> - <<250.000000,250.000000,59.000000>>,  <<-1002.454773,6266.978516,36.125729>> + <<250.000000,250.000000,59.000000>> )
	ADD_SCENARIO_BLOCKING_AREA( <<-1385.118164,5366.789551,37.042431>> - <<250.000000,250.000000,59.000000>>,  <<-1385.118164,5366.789551,37.042431>> + <<250.000000,250.000000,59.000000>> )
	
	CLEAR_AREA(<<-833.0740, 6397.1450, -10.1586>>,500,TRUE)
	CLEAR_AREA(<<-1063.6212, 6311.7153, -6.9619>>,500,TRUE)
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		bHasChanged = GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		bHasChanged = GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR)
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		bHasChanged = GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_FRANKLIN)
	ENDIF

	//SET_CLOCK_TIME(2,30,00)
	DISABLE_TAXI_HAILING(TRUE)
    // Loads the mission text
	SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_MILITARY_DOCKS,TRUE)
	SET_VEHICLE_GEN_AVAILABLE(VEHGEN_DOCKSP2B_CHINOOK, FALSE)
	
	REQUEST_ADDITIONAL_TEXT("DOCKH2B", MISSION_TEXT_SLOT)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_AIRFIELD_PROPS, BUILDINGSTATE_DESTROYED)
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		WAIT(0)
	ENDWHILE
	
	REMOVE_RELATIONSHIP_GROUP(rel_group_buddies)
	REMOVE_RELATIONSHIP_GROUP(rel_group_enemies)
	ADD_RELATIONSHIP_GROUP("BUDDIES", rel_group_buddies)
	ADD_RELATIONSHIP_GROUP("ENEMIES", rel_group_enemies)
	
	SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_MICHAEL, SAVEHOUSE_TREVOR_VB, TRUE) 
	SET_PLAYER_PED_CAN_ENTER_SAVEHOUSE(CHAR_FRANKLIN, SAVEHOUSE_TREVOR_VB, TRUE) 

	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,rel_group_enemies,RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,rel_group_enemies,rel_group_buddies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE,rel_group_buddies,rel_group_enemies)
	
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,RELGROUPHASH_PLAYER,rel_group_buddies)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_enemies,rel_group_enemies)	

	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< 1735.5898, 3222.9558, 47.8356 >>,<< 1770.1437, 3264.6560, 28.1638 >>,FALSE)
	CLEAR_AREA(<< 1770.1437, 3264.6560, 28.1638 >>,1000,TRUE,TRUE)
	//disable_dispatch_services()
	
	REGISTER_SCRIPT_WITH_AUDIO()
	
	WHILE NOT REQUEST_AMBIENT_AUDIO_BANK("SCRIPT\\PORT_OF_LS_01_GENERAL")
		WAIT(0)
	ENDWHILE
	
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_WillFlyThroughWindscreen,FALSE)
	
	IF Is_Replay_In_Progress()
		//Load appropriate mission stage dependent on checkpoint
		IF Get_Replay_Mid_Mission_Stage() >=  ENUM_TO_INT(MISSION_STAGE_SETUP) AND Get_Replay_Mid_Mission_Stage()  < ENUM_TO_INT(MISSION_STAGE_PASSED)
			
			iReplayStage = Get_Replay_Mid_Mission_Stage()	
			
			IF g_bShitskipAccepted = TRUE
				//iReplayStage = iReplayStage + 1
				IF iReplayStage < ENUM_TO_INT(MISSION_STAGE_PASSED)
					IF iReplayStage = 0
						START_REPLAY_SETUP(<<1751.7072, 3286.3362, 40.1140>>, 103.6627)
						eMissionStage = MISSION_STAGE_PICK_UP_THE_SUB_FROM_AIRSTRIP
						PRINTSTRING("******* iReplayStage = 0")PRINTNL()
					ELIF iReplayStage = 1
						START_REPLAY_SETUP(<<910.1729, 3335.7280, 270.3783>>  ,39.5182)
						eMissionStage = MISSION_STAGE_FLY_TO_SEA
						PRINTSTRING("******* iReplayStage = 1")PRINTNL()
					ELIF iReplayStage = 2
						START_REPLAY_SETUP(<<-1389.3110, 6163.1890,  40.3783>>  ,39.5182)
						eMissionStage = MISSION_STAGE_DROP_THE_SUB
						PRINTSTRING("******* iReplayStage = 2")PRINTNL()
					ELIF iReplayStage = 3
						START_REPLAY_SETUP(<<-1402.3759, 6145.0410, -12.5895>>  ,39.5182)
						eMissionStage = MISSION_STAGE_FIND_THE_CRATE
						PRINTSTRING("******* iReplayStage = 3")PRINTNL()
					ELIF iReplayStage = 4
						START_REPLAY_SETUP(<< -1255.28, 6795.65, -135.63 >>,39.5182)
						eMissionStage = MISSION_STAGE_GET_TO_SURFACE
						PRINTSTRING("******* iReplayStage = 4")PRINTNL()
					ELIF iReplayStage = 5
						START_REPLAY_SETUP(<<-1552.4729, 6488.5664, 118.6260>>  ,347.6931)
						eMissionStage = MISSION_STAGE_PICK_UP_SUB
						PRINTSTRING("******* iReplayStage = 5")PRINTNL()
					ELIF iReplayStage = 6
						START_REPLAY_SETUP(<<-1411.9094, 5957.8730, 40.3783>>  ,221.9830)
						eMissionStage = MISSION_STAGE_DEFEND_AGAINST_MERRYWEATHER
						PRINTSTRING("******* iReplayStage = 6")PRINTNL()
					ELIF iReplayStage = 7
						START_REPLAY_SETUP(<<-152.3409, 4654.3423, 261.3565>>, 219.9027)
						eMissionStage = MISSION_STAGE_FLY_AWAY
						PRINTSTRING("******* iReplayStage = 7")PRINTNL()
					ELIF iReplayStage = 8
						START_REPLAY_SETUP(<<1748.0168, 3283.6812, 95.0743>>  ,186.6673)
						eMissionStage = MISSION_STAGE_DROP_THE_SUB_AT_THE_AIRSTRIP
						PRINTSTRING("******* iReplayStage = 8")PRINTNL()
					ELIF iReplayStage = 9
						START_REPLAY_SETUP(<<1755.7994, 3275.8169, 40.1628>>, 310.7707)
						eMissionStage = MISSION_STAGE_CLOSING_CUTSCENE
						PRINTSTRING("******* iReplayStage = 9")PRINTNL()
					ENDIF
				ELSE
					eMissionStage =  MISSION_STAGE_PASSED
				ENDIF
			ELSE
				IF iReplayStage < ENUM_TO_INT(MISSION_STAGE_PASSED)
					IF iReplayStage = 0
						START_REPLAY_SETUP(<<-1149.6552, -1521.8536, 3.3298>>, 76.5150)
						eMissionStage = MISSION_STAGE_GET_TO_AIRSTRIP
						PRINTSTRING("******* iReplayStage = 1")PRINTNL()
					ELIF iReplayStage = 1
						START_REPLAY_SETUP(<<1751.7072, 3286.3362,40.1140>>, 103.6627)
						eMissionStage = MISSION_STAGE_PICK_UP_THE_SUB_FROM_AIRSTRIP
						PRINTSTRING("******* iReplayStage = 2")PRINTNL()
					ELIF iReplayStage = 2
						START_REPLAY_SETUP(<<910.1729, 3335.7280,270.3783>> ,39.5182)
						eMissionStage = MISSION_STAGE_FLY_TO_SEA
						PRINTSTRING("******* iReplayStage = 2")PRINTNL()
					ELIF iReplayStage = 3
						START_REPLAY_SETUP(<<-1389.3110, 6163.1890,40.3783>> ,39.5182)
						eMissionStage = MISSION_STAGE_DROP_THE_SUB
						PRINTSTRING("******* iReplayStage = 3")PRINTNL()
					ELIF iReplayStage = 4
						START_REPLAY_SETUP(<<-1402.3759, 6145.0410, -12.5895>>,39.5182)
						eMissionStage = MISSION_STAGE_FIND_THE_CRATE
						PRINTSTRING("******* iReplayStage = 4")PRINTNL()
					ELIF iReplayStage = 5
						START_REPLAY_SETUP(<< -1255.28, 6795.65, -135.63 >>,39.5182)
						eMissionStage = MISSION_STAGE_GET_TO_SURFACE
						PRINTSTRING("******* iReplayStage = 6")PRINTNL()
					ELIF iReplayStage = 6
						START_REPLAY_SETUP(<<-1552.4729, 6488.5664, 118.6260>> ,347.6931)
						eMissionStage = MISSION_STAGE_PICK_UP_SUB
						PRINTSTRING("******* iReplayStage = 7")PRINTNL()
					ELIF iReplayStage = 7
						START_REPLAY_SETUP(<<-1411.9094, 5957.8730, 40.3783>>  ,221.9830)
						eMissionStage = MISSION_STAGE_DEFEND_AGAINST_MERRYWEATHER
						PRINTSTRING("******* iReplayStage = 6")PRINTNL()
					ELIF iReplayStage = 8
						START_REPLAY_SETUP(<<-152.3409, 4654.3423, 261.3565>>  ,221.9830)
						eMissionStage = MISSION_STAGE_FLY_AWAY
						PRINTSTRING("******* iReplayStage = 6")PRINTNL()
					ELIF iReplayStage = 9
						START_REPLAY_SETUP(<<1748.0168, 3283.6812, 95.0743>>  ,186.6673)
						eMissionStage = MISSION_STAGE_DROP_THE_SUB_AT_THE_AIRSTRIP
						PRINTSTRING("******* iReplayStage = 6")PRINTNL()
					ELIF iReplayStage = 10
						START_REPLAY_SETUP(<<1755.7994, 3275.8169, 40.1628>>, 310.7707)
						eMissionStage = MISSION_STAGE_CLOSING_CUTSCENE
						PRINTSTRING("******* iReplayStage = 7")PRINTNL()
					ELSE
						SCRIPT_ASSERT("Trevor2: Trying to replay a mission stage that doesn't exist!")
					ENDIF
				ELSE
					eMissionStage =  MISSION_STAGE_PASSED
				ENDIF
			ENDIF
			
		ELSE
			
			IF iReplayStage = 0
				IF g_bShitskipAccepted = TRUE
					eMissionStage = MISSION_STAGE_PICK_UP_THE_SUB_FROM_AIRSTRIP
					PRINTSTRING("******* jumping past the drive")PRINTNL()
				ELSE
					eMissionStage = MISSION_STAGE_GET_TO_AIRSTRIP
					PRINTSTRING("******* iReplayStage = 0")PRINTNL()
				ENDIF
			ELSE
				eMissionStage = MISSION_STAGE_OPENING_CUTSCENE
			ENDIF
		
		ENDIF
		SET_PLAYER_START_POSITION(eMissionStage)
	ELSE
        // Your mission is being played normally, not being replayed
		#IF IS_DEBUG_BUILD
			//DEBUG_DO_CREW_SELECTION_FOR_HEIST(HEIST_DOCKS)
		#ENDIF
		//RUN_INTRO_CUTSCENE()
		INITALISE_ARRAYS()
		eMissionStage = MISSION_STAGE_OPENING_CUTSCENE
	ENDIF
	#IF IS_DEBUG_BUILD
	bFailChecks = TRUE
	#ENDIF
	
	IF eMissionStage != MISSION_STAGE_OPENING_CUTSCENE
		WHILE NOT bmissionstageloaded // continues requesting asserts/setup for particular stage until all are loaded
			WAIT(0)
			LOAD_MISSION_STAGE(eMissionStage)	
		ENDWHILE
	ENDIF

ENDPROC


FUNC BOOL SETUP_PEDS_FOR_DIALOGUE()

	REMOVE_PED_FOR_DIALOGUE(sSpeech,1)
	REMOVE_PED_FOR_DIALOGUE(sSpeech,2)
	REMOVE_PED_FOR_DIALOGUE(sSpeech,3)
	REMOVE_PED_FOR_DIALOGUE(sSpeech,4)
	REMOVE_PED_FOR_DIALOGUE(sSpeech,5)
	REMOVE_PED_FOR_DIALOGUE(sSpeech,8)
	
	//MIKE
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				ADD_PED_FOR_DIALOGUE(sSpeech, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
			ENDIF
		ELSE
			ADD_PED_FOR_DIALOGUE(sSpeech, 1, NULL, "MICHAEL")
		ENDIF
	ELSE
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
		ENDIF
	ENDIF
	
	//TREVOR
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				ADD_PED_FOR_DIALOGUE(sSpeech, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
			ENDIF
		ELSE
			ADD_PED_FOR_DIALOGUE(sSpeech, 2, NULL, "TREVOR")
		ENDIF
	ELSE
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			ADD_PED_FOR_DIALOGUE(sSpeech, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF
	ENDIF
	
	//FRANKLIN
	IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				ADD_PED_FOR_DIALOGUE(sSpeech, 3, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
			ENDIF
		ELSE
			ADD_PED_FOR_DIALOGUE(sSpeech, 3, NULL, "FRANKLIN")
		ENDIF
	ELSE
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			ADD_PED_FOR_DIALOGUE(sSpeech, 3, PLAYER_PED_ID(), "FRANKLIN")
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(Buddy[1].ped)
		IF NOT IS_PED_INJURED(Buddy[1].ped)
			ADD_PED_FOR_DIALOGUE(sSpeech, 4, Buddy[1].ped, "HiredGun")
		ENDIF
	ELSE
		ADD_PED_FOR_DIALOGUE(sSpeech, 4, NULL, "HiredGun")
	ENDIF
	
	IF DOES_ENTITY_EXIST(enemy[11].ped)
		IF NOT IS_PED_INJURED(enemy[11].ped)
			ADD_PED_FOR_DIALOGUE(sSpeech, 8, enemy[11].ped, "DH2BWORKER1")
		ENDIF
	ENDIF
	
	ADD_PED_FOR_DIALOGUE(sSpeech, 5, NULL, "HACKER")
	
	RETURN TRUE
	
ENDFUNC


FUNC BOOL DIVE_OUT_CUTSCENE()

	VECTOR v_boat_pos = vDiveOut
	VECTOR v_scene_pos
	VECTOR v_scene_rot
	FLOAT f_boat_heading = 288.2763
	FLOAT f_time
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	IF icutsceneprog > 0 AND icutsceneprog < 2
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
			CONTROL_FADE_OUT(500)
			KILL_ANY_CONVERSATION()
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
				FREEZE_ENTITY_POSITION(mission_veh[2].veh, FALSE)
				
				IF DOES_CAM_EXIST(cutscene_cam)
					DESTROY_CAM(cutscene_cam)
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[24].veh)
					DELETE_VEHICLE(mission_veh[24].veh)
					SET_MODEL_AS_NO_LONGER_NEEDED(POLMAV)
				ENDIF
				
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
					TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE,TRUE) 
				ENDIF
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
				IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(PLAYER_PED_ID())
					DETACH_ENTITY(PLAYER_PED_ID())
				ENDIF
				SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), << 495.1615, -3405.0725,  -3.5885 >>)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 296.8342)
				
				//SET_PED_PITCH(PLAYER_PED_ID(),90)
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					SET_ENTITY_COORDS_NO_OFFSET(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],  << 499.50, -3400.01, -4.51 >>)
					SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], 296.8342)
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					SET_ENTITY_COORDS_NO_OFFSET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],  << 501.51, -3398.45, -5.7991 >>)
					SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 300.0199)
				ENDIF
				
				IF NOT IS_PED_INJURED(buddy[1].ped)
					SET_ENTITY_COORDS_NO_OFFSET(buddy[1].ped,  << 504.7156, -3402.1687, -4.0741 >>)
					SET_ENTITY_HEADING(buddy[1].ped, 300.0199)
				ENDIF
				
				SET_GAMEPLAY_CAM_WORLD_HEADING(296.4766)
				
				//SCRIPT_ASSERT("SKIP CALLED")

				//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
				//SET_GAMEPLAY_CAM_RELATIVE_PITCH(90)
				RENDER_SCRIPT_CAMS(FALSE, FALSE)
			ENDIF
			itimer= GET_GAME_TIMER()
			icutsceneprog = 2
		ENDIF
	ENDIF
	
	SWITCH icutsceneprog
			
		CASE 0
		
			IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
			
				REQUEST_ANIM_DICT("missheistchem2")
				REQUEST_ANIM_DICT("SWIMMING@swim")
				REQUEST_MODEL(FORKLIFT)
				REQUEST_MODEL(S_M_Y_BLACKOPS_01)
				REQUEST_MODEL(DINGHY)
				
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
					TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE) 
					current_player_model = GET_CURRENT_PLAYER_PED_ENUM()
				ELSE
					IF HAS_ANIM_DICT_LOADED("missheistchem2")
					AND HAS_ANIM_DICT_LOADED("SWIMMING@swim")
					AND HAS_MODEL_LOADED(FORKLIFT)
					AND HAS_MODEL_LOADED(DINGHY)
					AND HAS_MODEL_LOADED(S_M_Y_BLACKOPS_01)
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						IF NOT DOES_ENTITY_EXIST(mission_veh[6].veh)
							mission_veh[6].veh =CREATE_VEHICLE(FORKLIFT,<< 608.5880, -3133.4907, 5.0687 >> , 93.2513)
						ENDIF
						
						IF NOT DOES_ENTITY_EXIST(mission_veh[7].veh)
							mission_veh[7].veh =CREATE_VEHICLE(FORKLIFT,mission_veh[7].loc , mission_veh[7].head)
						ENDIF
						
						IF NOT DOES_ENTITY_EXIST(mission_veh[26].veh)
							CREATE_ENEMY_VEHICLE(26,DINGHY,<< 570.5148, -3222.1147, -0.2736 >>, -0.2736,S_M_Y_BlackOps_01,TRUE,38,0,TRUE)
							SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[26].veh,TRUE)
							REQUEST_COLLISION_AT_COORD(<< 570.5148, -3222.1147, -0.2736 >>)
							REQUEST_VEHICLE_RECORDING(26,"DHS2")
						ENDIF
						
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						
						SET_ENTITY_COORDS(mission_veh[2].veh, v_boat_pos)
						SET_ENTITY_HEADING(mission_veh[2].veh, f_boat_heading)
						IF NOT IS_ENTITY_ATTACHED(mission_veh[2].veh)
							FREEZE_ENTITY_POSITION(mission_veh[2].veh, TRUE)
						ENDIF
						
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), mission_veh[2].veh, -1, <<0.0, 0.0, 0.0>>,<<0,0,0>>)				
						TASK_PLAY_ANIM(PLAYER_PED_ID(), "missheistchem2", "boat_dive_one_alt", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, 
									AF_NOT_INTERRUPTABLE | AF_REPOSITION_WHEN_FINISHED | AF_REORIENT_WHEN_FINISHED | AF_FORCE_START)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())

						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							IF IS_PED_IN_GROUP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
								REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							ENDIF
							
							CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							ATTACH_ENTITY_TO_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], mission_veh[2].veh, -1, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
							TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missheistchem2", "boat_dive_player_alt", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
										AF_NOT_INTERRUPTABLE | AF_REPOSITION_WHEN_FINISHED | AF_REORIENT_WHEN_FINISHED | AF_FORCE_START)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
						ENDIF
						
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							IF IS_PED_IN_GROUP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
								REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							ENDIF
							
							CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							ATTACH_ENTITY_TO_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], mission_veh[2].veh, -1, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
							TASK_PLAY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "missheistchem2", "boat_dive_two", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,
										AF_NOT_INTERRUPTABLE | AF_REPOSITION_WHEN_FINISHED | AF_REORIENT_WHEN_FINISHED | AF_FORCE_START)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
						ENDIF
						
						IF NOT IS_PED_INJURED(buddy[1].ped)
							IF IS_PED_IN_GROUP(buddy[1].ped) 
								REMOVE_PED_FROM_GROUP(buddy[1].ped)
							ENDIF
							
							CLEAR_PED_TASKS_IMMEDIATELY(buddy[1].ped)
							ATTACH_ENTITY_TO_ENTITY(buddy[1].ped, mission_veh[2].veh, -1, <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
							
							
							CLEAR_PED_TASKS_IMMEDIATELY(buddy[1].ped)
							ATTACH_ENTITY_TO_ENTITY(buddy[1].ped, mission_veh[2].veh, -1, <<0.0, 0.0, 0.0>>,<<0,0,0>>)				
							TASK_PLAY_ANIM(buddy[1].ped, "missheistchem2", "boat_dive_three", INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, 
										AF_NOT_INTERRUPTABLE | AF_REPOSITION_WHEN_FINISHED | AF_REORIENT_WHEN_FINISHED | AF_FORCE_START)
							
							FORCE_PED_AI_AND_ANIMATION_UPDATE(buddy[1].ped)
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(buddy[1].ped, TRUE)
						ENDIF
						
						cutscene_cam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
						PLAY_CAM_ANIM(cutscene_cam, "boat_dive_cam_alt", "missheistchem2", GET_ENTITY_COORDS(mission_veh[2].veh), <<0.0, 0.0, f_boat_heading>>)
						
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						SET_WIDESCREEN_BORDERS(TRUE, 0)
						bTriggerDialogue = FALSE
						SETTIMERB(0)
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
						ENDIF
						
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							ADD_PED_FOR_DIALOGUE(sSpeech, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
						ENDIF
						
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							ADD_PED_FOR_DIALOGUE(sSpeech, 3, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
						ENDIF
						
						IF NOT IS_PED_INJURED(Buddy[1].ped)
							ADD_PED_FOR_DIALOGUE(sSpeech, 4, Buddy[1].ped, "HiredGun")
						ENDIF
						
						ADD_PED_FOR_DIALOGUE(sSpeech, 5, NULL, "HACKER")
						bDiveTask = FALSE
						icutsceneprog++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			
			//Manage the team: most of this is fudge to reduce animation popping when detaching them from the boat
			IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
			
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missheistchem2", "boat_dive_player_alt")
							f_time = GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missheistchem2", "boat_dive_player_alt")
							
							IF f_time > 0.55
								v_scene_pos = GET_ENTITY_COORDS(mission_veh[2].veh)
								v_scene_rot = <<0.0, 0.0, GET_ENTITY_HEADING(mission_veh[2].veh)>>
								
								DETACH_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								TASK_PLAY_ANIM_ADVANCED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missheistchem2", "boat_dive_player_alt", v_scene_pos, v_scene_rot, 
														INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET | AF_OVERRIDE_PHYSICS, f_time)
								SET_ENTITY_ANIM_SPEED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missheistchem2", "boat_dive_player_alt", 2.0)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])	
							ENDIF
						ENDIF
					ELSE
						IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missheistchem2", "boat_dive_player_alt")
							IF GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "missheistchem2", "boat_dive_player_alt") > 0.65
								CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								SET_ENTITY_COORDS_NO_OFFSET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) - <<0.0, 0.0, 5.0>>)
								TASK_GO_STRAIGHT_TO_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], << 516.49, -3393.53, -5.15 >>, PEDMOVE_RUN, -1)
								//FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "missheistchem2", "boat_dive_two")
							f_time = GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "missheistchem2", "boat_dive_two")
							
							IF f_time > 0.55
								v_scene_pos = GET_ENTITY_COORDS(mission_veh[2].veh)
								v_scene_rot = <<0.0, 0.0, GET_ENTITY_HEADING(mission_veh[2].veh)>>
								
								DETACH_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
								TASK_PLAY_ANIM_ADVANCED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "missheistchem2", "boat_dive_two", v_scene_pos, v_scene_rot, 
														INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET | AF_OVERRIDE_PHYSICS, f_time)
								SET_ENTITY_ANIM_SPEED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "missheistchem2", "boat_dive_two", 2.0)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])	
							ENDIF
						ENDIF
					ELSE
						IF IS_ENTITY_PLAYING_ANIM(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "missheistchem2", "boat_dive_two")
							IF GET_ENTITY_ANIM_CURRENT_TIME(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "missheistchem2", "boat_dive_two") > 0.65
								CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
								SET_ENTITY_COORDS_NO_OFFSET(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) - <<0.0, 0.0, 5.0>>)
								TASK_GO_STRAIGHT_TO_COORD(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], << 516.49, -3393.53, -4.15 >>, PEDMOVE_RUN, -1)
								//FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(Buddy[1].ped)
					IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(Buddy[1].ped)
						IF IS_ENTITY_PLAYING_ANIM(Buddy[1].ped, "missheistchem2", "boat_dive_three")
							f_time = GET_ENTITY_ANIM_CURRENT_TIME(Buddy[1].ped, "missheistchem2", "boat_dive_three")
							
							IF f_time > 0.55
								v_scene_pos = GET_ENTITY_COORDS(mission_veh[2].veh)
								v_scene_rot = <<0.0, 0.0, GET_ENTITY_HEADING(mission_veh[2].veh)>>
								
								DETACH_ENTITY(Buddy[1].ped)
								TASK_PLAY_ANIM_ADVANCED(Buddy[1].ped, "missheistchem2", "boat_dive_three", v_scene_pos, v_scene_rot, 
														INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET | AF_OVERRIDE_PHYSICS, f_time)
								SET_ENTITY_ANIM_SPEED(Buddy[1].ped, "missheistchem2", "boat_dive_three", 2.0)
								FORCE_PED_AI_AND_ANIMATION_UPDATE(Buddy[1].ped)	
							ENDIF
						ENDIF
					ELSE
						IF IS_ENTITY_PLAYING_ANIM(Buddy[1].ped, "missheistchem2", "boat_dive_three")
							IF GET_ENTITY_ANIM_CURRENT_TIME(Buddy[1].ped, "missheistchem2", "boat_dive_three") > 0.65
								CLEAR_PED_TASKS_IMMEDIATELY(Buddy[1].ped)
								SET_ENTITY_COORDS_NO_OFFSET(Buddy[1].ped, GET_ENTITY_COORDS(Buddy[1].ped) - <<0.0, 0.0, 5.0>>)
								TASK_GO_STRAIGHT_TO_COORD(Buddy[1].ped, << 516.82, -3391.19, -4.75 >>, PEDMOVE_RUN, -1)
								//FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_ENTITY_ATTACHED_TO_ANY_VEHICLE(PLAYER_PED_ID())
					IF IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missheistchem2", "boat_dive_one_alt")
						f_time = GET_ENTITY_ANIM_CURRENT_TIME(PLAYER_PED_ID(), "missheistchem2", "boat_dive_one_alt")
						
						IF f_time > 0.7
							v_scene_pos = GET_ENTITY_COORDS(mission_veh[2].veh)
							v_scene_rot = <<0.0, 0.0, GET_ENTITY_HEADING(mission_veh[2].veh)>>
							
							DETACH_ENTITY(PLAYER_PED_ID())
							
							OPEN_SEQUENCE_TASK(seq)
								TASK_PLAY_ANIM_ADVANCED(NULL, "missheistchem2", "boat_dive_one_alt", v_scene_pos, v_scene_rot, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, 
													    -1, AF_NOT_INTERRUPTABLE | AF_EXTRACT_INITIAL_OFFSET | AF_OVERRIDE_PHYSICS, f_time)
								//TASK_PLAY_ANIM(NULL, "swimming", "dive_run", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE | AF_FORCE_START)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seq)
							CLEAR_SEQUENCE_TASK(seq)

							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							
							IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
								FREEZE_ENTITY_POSITION(mission_veh[2].veh, FALSE)	
							ENDIF

						ENDIF
					ENDIF
				ELSE
					IF bDiveTask = FALSE
						IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missheistchem2", "boat_dive_one_alt")
							task_go_straight_to_coord(player_ped_id(), << 502.8210, -3403.0537, -4.9292 >>, pedmove_run, -1)
							fORCE_PED_MOTION_STATE(player_ped_id(), MS_diving_swim, TRUE)
							FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							//SCRIPT_ASSERT("DIVE TASK - NORMAL")
							bDiveTask = TRUE
						ENDIF
					ENDIF
					
					IF GET_CAM_ANIM_CURRENT_PHASE(cutscene_cam) = 1
						
						//DISPLAY_RADAR(TRUE)
						//DISPLAY_HUD(TRUE)
						//SET_WIDESCREEN_BORDERS(FALSE, 0)

						//SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
						//RENDER_SCRIPT_CAMS(FALSE,TRUE)

						itimer= GET_GAME_TIMER()
						
						//DIALOGUE
						
						REMOVE_PED_FOR_DIALOGUE(sSpeech,1)
						REMOVE_PED_FOR_DIALOGUE(sSpeech,2)
						REMOVE_PED_FOR_DIALOGUE(sSpeech,3)
						REMOVE_PED_FOR_DIALOGUE(sSpeech,4)
						REMOVE_PED_FOR_DIALOGUE(sSpeech,5)
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
						ENDIF
						
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							ADD_PED_FOR_DIALOGUE(sSpeech, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
						ENDIF
						
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							ADD_PED_FOR_DIALOGUE(sSpeech, 3, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
						ENDIF
						
						IF NOT IS_PED_INJURED(Buddy[1].ped)
							ADD_PED_FOR_DIALOGUE(sSpeech, 4, Buddy[1].ped, "HiredGun")
						ENDIF
						
						ADD_PED_FOR_DIALOGUE(sSpeech, 5, NULL, "HACKER")
						bTriggerDialogue = FALSE
						icutsceneprog++
					ENDIF

				ENDIF

			ENDIF
			
		BREAK
		
		CASE 2
		
			IF bDiveTask = FALSE
				IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "missheistchem2", "boat_dive_one_alt")
					task_go_straight_to_coord(player_ped_id(), << 502.8210, -3403.0537, -4.9292 >>, pedmove_run, -1)
					fORCE_PED_MOTION_STATE(player_ped_id(), MS_diving_swim, TRUE)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					bDiveTask = TRUE
					//SCRIPT_ASSERT("DIVE TASK - SKIP")
				ENDIF
			ENDIF
			
			IF bTriggerDialogue = FALSE
				IF SETUP_PEDS_FOR_DIALOGUE()
					IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_4", CONV_PRIORITY_MEDIUM )
						IF DOES_CAM_EXIST(cutscene_cam)
							//IF GET_CAM_ANIM_CURRENT_PHASE(cutscene_cam) = 1
								IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
									FREEZE_ENTITY_POSITION(mission_veh[2].veh, FALSE)
								ENDIF
								//CONTROL_FADE_IN(500) //In case of skip
								SETTIMERB(0)
								bTriggerDialogue = FALSE
								REMOVE_ANIM_DICT("missheistchem2")
								REMOVE_ANIM_DICT("SWIMMING@swim")
								SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
								SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
								DISPLAY_RADAR(TRUE)
								DISPLAY_HUD(TRUE)
								SET_WIDESCREEN_BORDERS(FALSE, 0)
								SET_GAMEPLAY_CAM_WORLD_HEADING(296.4766)
								RENDER_SCRIPT_CAMS(FALSE,TRUE)
								PRINTSTRING("ASSIGNING TASKS")
								
								IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
									//SET_ENTITY_COORDS_NO_OFFSET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],<< 579.7324, -3329.8237, -4.0098 >> )
									SET_ENABLE_SCUBA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRUE)
									CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
									FORCE_PED_MOTION_STATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], MS_diving_swim, TRUE)
									//FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
									OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
										//TASK_STAND_STILL(NULL,200)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 516.49, -3393.53, -5.15 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 608.39, -3303.12, -7.15 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 631.94, -3212.66,  -5.15 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 625.36, -3107.25, -1.93 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 622.89, -3107.12, -1.93 >>  , PEDMOVE_WALK, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 622.81, -3111.74, 0.48 >>  , PEDMOVE_WALK, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 622.84, -3113.61, 1.79 >>  , PEDMOVE_RUN, -1)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< 612.8863, -3099.8049, 5.0691 >>,PEDMOVE_WALK,-1 )
									CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
									TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TASK_SEQUENCE)
									CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
								ENDIF
								
								IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
									CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
									//SET_ENTITY_COORDS_NO_OFFSET(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], << 587.1451, -3327.4861, -3.8947 >> )
									SET_ENABLE_SCUBA(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
									FORCE_PED_MOTION_STATE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], MS_diving_swim, TRUE)
									//FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
									OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
										TASK_STAND_STILL(NULL,1500)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 516.49, -3393.53, -4.15 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 608.39, -3303.12, -6.15 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 631.94, -3212.66,  -5.55 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 625.36, -3107.25, -1.93 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 622.89, -3107.12, -1.93 >>  , PEDMOVE_WALK, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 622.81, -3111.74, 0.48 >>  , PEDMOVE_WALK, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 622.84, -3113.61, 1.79 >>  , PEDMOVE_RUN, -1)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< 612.8863, -3099.8049, 5.0691 >>,PEDMOVE_WALK,-1 )
									CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
									TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TASK_SEQUENCE)
									CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
								ENDIF
								
								IF NOT IS_PED_INJURED(buddy[1].ped)
									SET_ENABLE_SCUBA(buddy[1].ped,TRUE)
									CLEAR_PED_TASKS(buddy[1].ped)
									//SET_ENTITY_COORDS_NO_OFFSET(Buddy[1].ped, << 587.1451, -3327.4861, -4.8947 >> )
									FORCE_PED_MOTION_STATE(buddy[1].ped, MS_diving_swim, TRUE)
									//FORCE_PED_AI_AND_ANIMATION_UPDATE(buddy[1].ped)
									SET_PED_PATH_CAN_USE_LADDERS(buddy[1].ped,TRUE)
									OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 516.82, -3391.19, -4.75 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 606.00, -3303.25, -6.79 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 609.1154, -3249.3760, -0.1688>>  , PEDMOVE_SPRINT, -1)
										TASK_CLIMB_LADDER(NULL,TRUE)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 606.55, -3249.06, 5.07 >>  , PEDMOVE_SPRINT, -1)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 605.93, -3207.28, 5.07 >>,PEDMOVE_RUN)
									CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
									TASK_PERFORM_SEQUENCE(Buddy[1].ped,TASK_SEQUENCE)
									CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
								ENDIF
								
								icutsceneprog = 0
								RETURN TRUE
							//ENDIF
						ELSE
							IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
								FREEZE_ENTITY_POSITION(mission_veh[2].veh, FALSE)
							ENDIF
								PRINTSTRING("ASSIGNING TASKS")
								IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
									//SET_ENTITY_COORDS_NO_OFFSET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],<< 579.7324, -3329.8237, -4.0098 >> )
									SET_ENABLE_SCUBA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRUE)
									CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
									FORCE_PED_MOTION_STATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], MS_diving_swim, TRUE)
									//FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
									OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
										//TASK_STAND_STILL(NULL,500)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 516.49, -3393.53, -5.15 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 608.39, -3303.12, -7.15 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 631.94, -3212.66,  -5.15 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 625.36, -3107.25, -1.93 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 622.89, -3107.12, -1.93 >>  , PEDMOVE_WALK, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 622.81, -3111.74, 0.48 >>  , PEDMOVE_WALK, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 622.84, -3113.61, 1.79 >>  , PEDMOVE_RUN, -1)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< 612.8863, -3099.8049, 5.0691 >>,PEDMOVE_WALK,-1 )
									CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
									TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TASK_SEQUENCE)
									CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
								ENDIF
								
								IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
									CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
									//SET_ENTITY_COORDS_NO_OFFSET(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], << 587.1451, -3327.4861, -3.8947 >> )
									SET_ENABLE_SCUBA(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
									FORCE_PED_MOTION_STATE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], MS_diving_swim, TRUE)
									//FORCE_PED_AI_AND_ANIMATION_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
									OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
										//TASK_STAND_STILL(NULL,1500)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 516.49, -3393.53, -4.15 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 608.39, -3303.12, -6.15 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 631.94, -3212.66,  -5.55 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 625.36, -3107.25, -1.93 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 622.89, -3107.12, -1.93 >>  , PEDMOVE_WALK, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 622.81, -3111.74, 0.48 >>  , PEDMOVE_WALK, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 622.84, -3113.61, 1.79 >>  , PEDMOVE_RUN, -1)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< 612.8863, -3099.8049, 5.0691 >>,PEDMOVE_WALK,-1 )
									CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
									TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TASK_SEQUENCE)
									CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
								ENDIF
								
								IF NOT IS_PED_INJURED(buddy[1].ped)
									SET_ENABLE_SCUBA(buddy[1].ped,TRUE)
									CLEAR_PED_TASKS(buddy[1].ped)
									//SET_ENTITY_COORDS_NO_OFFSET(Buddy[1].ped, << 587.1451, -3327.4861, -4.8947 >> )
									SET_PED_PATH_CAN_USE_LADDERS(buddy[1].ped,TRUE)
									FORCE_PED_MOTION_STATE(buddy[1].ped, MS_diving_swim, TRUE)
									//FORCE_PED_AI_AND_ANIMATION_UPDATE(buddy[1].ped)
									OPEN_SEQUENCE_TASK(TASK_SEQUENCE)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 516.82, -3391.19, -4.75 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 606.00, -3303.25, -6.79 >>  , PEDMOVE_SPRINT, -1)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 609.1154, -3249.3760, -0.1688>>  , PEDMOVE_SPRINT, -1)
										TASK_CLIMB_LADDER(NULL,TRUE)
										TASK_GO_STRAIGHT_TO_COORD(NULL, << 606.65, -3249.94, 5.17 >>  , PEDMOVE_RUN, -1)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << 605.43, -3207.89, 5.07 >>,PEDMOVE_RUN)
										TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< 609.22, -3203.70, 6.07 >>,PEDMOVE_RUN,DEFAULT_TIME_BEFORE_WARP,0.2)
										TASK_PUT_BUDDY_INTO_CUSTOM_COVER(1,<< 609.9011, -3203.1477, 5.0696 >>,<<611.51, -3201.65, 5.07>>, 273.4789,-1,COVUSE_WALLTONEITHER,COVHEIGHT_HIGH,COVARC_180)
									CLOSE_SEQUENCE_TASK(TASK_SEQUENCE)
									TASK_PERFORM_SEQUENCE(Buddy[1].ped,TASK_SEQUENCE)
									CLEAR_SEQUENCE_TASK(TASK_SEQUENCE)
								ENDIF
							
							
								CONTROL_FADE_IN(500) //In case of skip
								bTriggerDialogue = FALSE
								SETTIMERB(0)
								REMOVE_ANIM_DICT("missheistchem2")
								REMOVE_ANIM_DICT("SWIMMING@swim")
								SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_UNARMED,TRUE)
								
								SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
								//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
								//RENDER_SCRIPT_CAMS(FALSE,TRUE)
								SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
								//
								icutsceneprog = 0
								RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF

		BREAK

		
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC MANAGE_DOCKS_COMPUTER_SF()

	IF bDockProg[0]
		OPEN_DOCK_DOORS()
	ENDIF
	
	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	IF iPC > 1
		IF bGodText = FALSE
			IF IS_CONVERSATION_STATUS_FREE()
				IF bDockProg[0] = FALSE
				AND bDockProg[2] = FALSE
					PRINT_GOD_TEXT("DS2_LOWSUB2")
					bGodText = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF bDockProg[2]
		IF IS_VEHICLE_DRIVEABLE(mission_veh[3].veh)
			IF vsubcoords[1].z < -5.2578
				vsubcoords[1].z = vsubcoords[1].z + 0.05
			ELIF vsubcoords[1].z < -4.2578
				vsubcoords[1].z = vsubcoords[1].z + (1+(-vsubcoords[1].z-5.2578))*0.05
				IF bDocksThingDone[1] = FALSE
					bDocksThingDone[1] = TRUE
				ENDIF
			ENDIF
			SET_ENTITY_COORDS(mission_veh[3].veh,vsubcoords[1])
		ENDIF
	ENDIF
	
	SWITCH iPC
		
		CASE 0
			//REQUESTS
			SFDocksHUD = REQUEST_SCALEFORM_MOVIE("sub_pc")
			bComputerActive = FALSE
			bDockProg[0] = FALSE
			bDockProg[2] = FALSE
			bGodText = FALSE
			vsubcoords[1] =  <<527.2601, -3164.7800, -8.9467>>
			iPC++
		BREAK
		
		CASE 1
			//REQUEST CHECKS AND SETUP
			IF HAS_SCALEFORM_MOVIE_LOADED(SFDocksHUD)
				KILL_ANY_CONVERSATION()
				IF SETUP_PEDS_FOR_DIALOGUE()
					IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "D2SA_8e", CONV_PRIORITY_MEDIUM )
						SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
						SETTIMERA(0)
						iPC++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			//CALL SCALEFORM REQUESTS SET ONCE, QUEUE UP THE HELP TEXT, SETUP CAMERAS
			IF TIMERA() > 5000
		
				bComputerActive = TRUE

				BEGIN_SCALEFORM_MOVIE_METHOD(SFDocksHUD, "RUN_PROGRAM")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(5)
				END_SCALEFORM_MOVIE_METHOD()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(SFDocksHUD, "SET_DATE")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(09)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(19)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(05)
				END_SCALEFORM_MOVIE_METHOD()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(SFDocksHUD, "SET_TIME")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(23)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(59)
				END_SCALEFORM_MOVIE_METHOD()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(SFDocksHUD, "SET_SECTION")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DS_SC_T8")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DS_SC_T7")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DS_SC_T5")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DS_SC_T6")
				END_SCALEFORM_MOVIE_METHOD()
				
				BEGIN_SCALEFORM_MOVIE_METHOD(SFDocksHUD, "SET_LABELS")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DS_SC_T1")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DS_SC_T2")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DS_SC_T3")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING("DS_SC_T4")
				END_SCALEFORM_MOVIE_METHOD()
				
				CALL_SCALEFORM_MOVIE_METHOD(SFDocksHUD,"TRIGGER_CAM_FLASH")

				CLEAR_PRINTS()
				
				IF NOT IS_MINIGAME_IN_PROGRESS()
					SET_MINIGAME_IN_PROGRESS(TRUE)
				ENDIF
				
				PRINT_HELP("DS2_COMPH1")
				IF NOT DOES_CAM_EXIST(cutscene_cam)
					cutscene_cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE)
				ENDIF
				//START ON MINI SUB
				SET_CAM_PARAMS(cutscene_cam,<< 578.6489, -3152.1707, 14.1949 >>, << 1.0420, -0.0042, 146.5565 >>,57.7048)
				SHAKE_CAM(cutscene_cam,"HAND_SHAKE",0.0)
				
				vsubcoords[0] =  << 567.2645, -3178.6802, 9.2318 >>
				vsubcoords[1] =  <<526.2101, -3160.0996,  -8.2578 >> 
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				SET_NOISINESSOVERIDE(0.08)
				SET_TIMECYCLE_MODIFIER("CAMERA_BW")
				SETTIMERA(0)
				//camRot = <<-5.274722,-0.004214,-172.938721>>
				
				iPC++
			ENDIF
		BREAK
		
		CASE 3
			
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_COMPH1")
				IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_COMPH2")
					//PRINT_HELP("DS2_COMPH2")
				ENDIF
			ENDIF
			
		// MANAGE MOUSE AND LISTEN FOR MOUSE CLICKS
		
			//GET_POSITION_OF_ANALOGUE_STICKS(PAD1,iPadLeftX,iPadLeftY,iPadRightX,iPadRightY)
			//iPadLeftX = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X) - 128
			//iPadLeftY = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y) - 128
			GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iPadLeftX,iPadLeftY,iPadRightX,iPadRightY)
			//iPadRightY = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y) - 128
			//iPadRightX = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X) - 128
			
			//Fix for unref
			IF iPadRightX < 0
			
			ENDIF
			
			IF iPadRightY < 0
			
			ENDIF
			
	    	BEGIN_SCALEFORM_MOVIE_METHOD(SFDocksHUD, "MOVE_CURSOR")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(iPadLeftX)*0.3)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(iPadLeftY)*0.3)
		    END_SCALEFORM_MOVIE_METHOD()
			
			IF NOT IS_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_READY(scaleRet)
				IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
					BEGIN_SCALEFORM_MOVIE_METHOD(SFDocksHUD,"SET_INPUT_EVENT")
						SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CROSS))
					scaleRet = END_SCALEFORM_MOVIE_METHOD_RETURN_VALUE()
					PRINTSTRING("iClick")PRINTINT(iClick)PRINTNL()
				ENDIF
			ELSE
				iClick = GET_SCALEFORM_MOVIE_METHOD_RETURN_VALUE_INT(scaleRet)
				PRINTSTRING("iReadyClick")PRINTINT(iClick)PRINTNL()
			ENDIF

			//Pressed open gate
			IF bDockProg[0] = FALSE
				IF iClick = 70
					IF SETUP_PEDS_FOR_DIALOGUE()
						IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "D2SA_8g", CONV_PRIORITY_MEDIUM )
							IF GET_CAM_FOV(cutscene_cam) != 57.7048
								BEGIN_SCALEFORM_MOVIE_METHOD(SFDocksHUD,"TRIGGER_CAM_FLASH")
								END_SCALEFORM_MOVIE_METHOD()
							ENDIF
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_COMPH1")
								CLEAR_HELP()
							ENDIF
							SET_CAM_PARAMS(cutscene_cam,<< 578.6489, -3152.1707, 14.1949 >>, << 1.0420, -0.0042, 146.5565 >>,57.7048)
							
							BEGIN_SCALEFORM_MOVIE_METHOD(SFDocksHUD,"SET_BUTTON_ACTIVE")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
							END_SCALEFORM_MOVIE_METHOD()
							
							CLEAR_PRINTS()
							bDockProg[0] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			
			IF bDockProg[2] = FALSE
				IF iClick = 71
					IF SETUP_PEDS_FOR_DIALOGUE()
						IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "D2SA_8f", CONV_PRIORITY_MEDIUM )
							IF GET_CAM_FOV(cutscene_cam) != 74.5080
								BEGIN_SCALEFORM_MOVIE_METHOD(SFDocksHUD,"TRIGGER_CAM_FLASH")
								END_SCALEFORM_MOVIE_METHOD()
							ENDIF
							
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_COMPH1")
								CLEAR_HELP()
							ENDIF
							
							IF IS_VEHICLE_DRIVEABLE(mission_veh[3].veh)
								SET_CAM_PARAMS(cutscene_cam,<< 534.4617, -3134.3838, 5.1698 >>, << 2.7429, -0.0042, 135.9985 >>,74.5080)
							ENDIF
							
							IF IS_VEHICLE_DRIVEABLE(mission_veh[3].veh)
								SET_ENTITY_COORDS(mission_veh[3].veh,vSubRaisedPosition)
							ENDIF
							
							BEGIN_SCALEFORM_MOVIE_METHOD(SFDocksHUD,"SET_BUTTON_ACTIVE")
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1)
								SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0)
							END_SCALEFORM_MOVIE_METHOD()
							
							bDockProg[2] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			IF bDockProg[0] = TRUE 
			AND bDockProg[2] = TRUE
			AND bDocksThingDone[1] = TRUE
				CLEAR_PRINTS()
				//REPLACE WITH DIALOGUE
				//PRINT_GOD_TEXT("DS2_PREPSUB")
				bGodText = FALSE
				bTriggerWarning = FALSE
				SETTIMERB(0)
				iPC++
			ENDIF
			
		BREAK
		
		CASE 4
			
			//GET_POSITION_OF_ANALOGUE_STICKS(PAD1,iPadLeftX,iPadLeftY,iPadRightX,iPadRightY)
			iPadLeftX = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X) - 128
			iPadLeftY = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y) - 128
			//iPadRightY = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y) - 128
			//iPadRightX = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y) - 128
			
	    	BEGIN_SCALEFORM_MOVIE_METHOD(SFDocksHUD, "MOVE_CURSOR")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(iPadLeftX)*0.3)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(TO_FLOAT(iPadLeftY)*0.3)
		    END_SCALEFORM_MOVIE_METHOD()
		
			IF TIMERB() > 3000
				IF bTriggerWarning = FALSE
					//CALL_SCALEFORM_MOVIE_METHOD_WITH_NUMBER(SFDocksHUD,"TRIGGER_WARNING",3)
					bTriggerWarning = TRUE
				ENDIF
			ENDIF

			IF TIMERB() > 5000
			//ONCE ALL DONE UNLOAD SHIT
		
				bComputerActive = FALSE
				
				IF NOT IS_MINIGAME_IN_PROGRESS()
					SET_MINIGAME_IN_PROGRESS(FALSE)
				ENDIF
				
				CLEAR_HELP(TRUE)
				CLEAR_PRINTS()
				
				SET_CAM_PARAMS(cutscene_cam,<< 571.2294, -3123.5569, 18.9524 >>, << -11.4722, 0.0000, -88.7923 >>,47.0667,0)
				SET_CAM_PARAMS(cutscene_cam, << 570.4655, -3124.7312, 19.8744 >>, << -32.6091, -0.0000, -46.6130 >>, 41.5556, 3000)
				SHAKE_CAM(cutscene_cam,"HAND_SHAKE",0.3)
				
				//DESTROY_ALL_CAMS()
				//CLEAR_PED_TASKS(PLAYER_PED_ID())
				SET_NOISEOVERIDE(FALSE)
				//RENDER_SCRIPT_CAMS(FALSE,FALSE)
				CLEAR_TIMECYCLE_MODIFIER()
				SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
				SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(SFDocksHUD)
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[3].veh)
					SET_ENTITY_COORDS(mission_veh[3].veh,vSubRaisedPosition)
				ENDIF
				
			ENDIF
		BREAK

	ENDSWITCH
	
	IF bComputerActive = TRUE
		//HIDE_HUD_AND_RADAR_THIS_FRAME()
		IF NOT IS_RADAR_HIDDEN()
			DISPLAY_RADAR(FALSE)
		ENDIF
		SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
		DRAW_SCALEFORM_MOVIE(SFDocksHUD,0.5,0.5,1,1,255,255,255,0)
		//SET_SCRIPT_GFX_DRAW_ORDER(GFX_ORDER_BEFORE_HUD)
	ENDIF
	

ENDPROC

//PURPOSE: runs the cutscene 
FUNC BOOL SUB_CUTSCENE() 

	SETTIMERA(0)
	bcutsceneplaying = TRUE

	WHILE bcutsceneplaying  //holds the script inside here until cutscene is complete

		WAIT(0)
		
		#IF IS_DEBUG_BUILD // allows j/p skip to work even in cutscene
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				bcutsceneplaying = FALSE
			ENDIF
		#ENDIF
		
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY() AND iprogress >0 // controls the cutscene skip
			CONTROL_FADE_OUT(500)
			icutsceneprog = 3
		ENDIF
		
		SWITCH icutsceneprog
		
			CASE 0  // creates inital cam and interp

				CLEAR_PRINTS()
				CLEAR_HELP()
				SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
				
				SET_WIDESCREEN_BORDERS(TRUE,0)			

				cutscene_cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE) 

				SET_CAM_PARAMS(cutscene_cam,<< 570.1249, -3130.3250, 19.5945 >>, << -4.2379, -0.0513, 146.7156 >>, 45.0000)
				SET_CAM_PARAMS(cutscene_cam,<< 573.1447, -3130.3250, 19.5945 >>, << -4.2379, -0.0513, 146.7156 >>,  45.0000,4100,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
				
				vsubcoords[0] =  << 567.2645, -3178.6802, 9.2318 >>
				vsubcoords[1] =  <<526.2101, -3160.0996,  -8.2578 >> 
				
				RENDER_SCRIPT_CAMS(TRUE,FALSE)

				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				SETTIMERB(0)
				PRINTSTRING("INTRO_CUTSCENE GOAL 3")
				PRINTNL()	
				icutsceneprog++

			BREAK
			
			CASE 1
			
				IF TIMERB() > 3000

					SET_CAM_PARAMS(cutscene_cam,<< 578.0532, -3166.3586, 8.6938 >>, << -1.9968, 0.0000, 144.9563 >>, 45.0000)
					SET_CAM_PARAMS(cutscene_cam,<< 575.0220, -3167.1697, 5.7290 >>, << -2.3949, 0.0000, 150.3366 >>,  45.0000,7100,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
					SETTIMERB(0)
					icutsceneprog++
				ENDIF
				
			BREAK

			
			CASE 2  
				OPEN_DOCK_DOORS()
				IF TIMERB() > 6000

					SET_CAM_PARAMS(cutscene_cam,<< 516.6874, -3194.9548, 2.5598 >>, << -2.6231, 0.2250, -36.9471 >>, 45.0000)
					SET_CAM_PARAMS(cutscene_cam,<< 514.7562, -3195.1958, 4.1191 >>, << -1.4213, 0.2250, -39.6472 >>, 45.0000,9000)

					PRINTSTRING("INTRO_CUTSCENE GOAL 6")
					PRINTNL()	
					SETTIMERB(0)
					icutsceneprog++
				ENDIF
			BREAK

			CASE 3
			
				OPEN_DOCK_DOORS()
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[3].veh)
					IF vsubcoords[1].z < -5.2578
						vsubcoords[1].z = vsubcoords[1].z + 0.05
					ELIF vsubcoords[1].z < -4.2578
						vsubcoords[1].z = vsubcoords[1].z + (1+(-vsubcoords[1].z-5.2578))*0.05
					ENDIF
					SET_ENTITY_COORDS(mission_veh[3].veh,vsubcoords[1])
				ENDIF
				
				IF TIMERB() > 9500

					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_WIDESCREEN_BORDERS(FALSE,0)

					IF IS_VEHICLE_DRIVEABLE(mission_veh[3].veh)
						SET_ENTITY_COORDS(mission_veh[3].veh,<<526.2101, -3163.0996,-5.2578>>)
					ENDIF
					IF DOES_CAM_EXIST(cutscene_cam)
						DESTROY_CAM(cutscene_cam)
					ENDIF
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					PRINTSTRING("SUB CUTSCENE COMPLETE")
					PRINTNL()	
					CONTROL_FADE_IN(500)
					icutsceneprog=0
					bcutsceneplaying = FALSE
					RETURN TRUE
				ENDIF
				
			BREAK

		ENDSWITCH	

	ENDWHILE
	
	RETURN FALSE

ENDFUNC

FUNC BOOL CUTSCENE_GET_INTO_BOAT()

	SETTIMERA(0)
	bcutsceneplaying = TRUE

	WHILE bcutsceneplaying  //holds the script inside here until cutscene is complete

		WAIT(0)
		
		#IF IS_DEBUG_BUILD // allows j/p skip to work even in cutscene
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				CONTROL_FADE_OUT(500)
				SETTIMERB(5000)
				icutsceneprog = 3
			ENDIF
		#ENDIF
		
		IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY() AND icutsceneprog  >0 // controls the cutscene skip
			CONTROL_FADE_OUT(500)
			icutsceneprog = 3
		ENDIF
		
		SWITCH icutsceneprog
		
			CASE 0  // creates inital cam and interp
				REQUEST_MODEL(S_M_Y_BLACKOPS_01)
				REQUEST_MODEL(S_M_M_DOCKWORK_01)
				REQUEST_MODEL(A_F_Y_BUSINESS_01)
//				REQUEST_MODEL(Prop_SluiceGate)
//				REQUEST_MODEL(Prop_SluiceGateL)
//				REQUEST_MODEL(Prop_SluiceGateR)
				
				IF HAS_MODEL_LOADED(S_M_Y_BLACKOPS_01)
				AND HAS_MODEL_LOADED(S_M_M_DOCKWORK_01)
//				AND HAS_MODEL_LOADED(Prop_SluiceGate)
//				AND HAS_MODEL_LOADED(Prop_SluiceGateL)
//				AND HAS_MODEL_LOADED(Prop_SluiceGateR)
				AND HAS_MODEL_LOADED(A_F_Y_BUSINESS_01)
				CLEAR_AREA(<< -337.3207, -2775.5488, 4.0001 >>,100,TRUE,FALSE)
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[10].veh)
						SET_ENTITY_COORDS(mission_veh[10].veh,<< -337.3207, -2775.5488, 4.0001 >>)
						SET_ENTITY_HEADING(mission_veh[10].veh,268.8268)
					ENDIF
						
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] ,rel_group_buddies)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
						OPEN_SEQUENCE_TASK(seq)
							TASK_LEAVE_ANY_VEHICLE(NULL,1000)
							TASK_STAND_STILL(NULL,-1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],seq)
						CLEAR_SEQUENCE_TASK(seq)
					ENDIF
					
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
						SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] ,rel_group_buddies)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],TRUE)
						OPEN_SEQUENCE_TASK(seq)
							TASK_LEAVE_ANY_VEHICLE(NULL,400)
							TASK_STAND_STILL(NULL,-1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],seq)
						CLEAR_SEQUENCE_TASK(seq)
					ENDIF
					
					IF NOT IS_PED_INJURED(Buddy[1].ped)
						SET_PED_RELATIONSHIP_GROUP_HASH(Buddy[1].ped ,rel_group_buddies)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(Buddy[1].ped,TRUE)
						OPEN_SEQUENCE_TASK(seq)
							TASK_LEAVE_ANY_VEHICLE(NULL,1200)
							TASK_STAND_STILL(NULL,-1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(Buddy[1].ped,seq)
						CLEAR_SEQUENCE_TASK(seq)
					ENDIF

					CLEAR_PRINTS()
					CLEAR_HELP()
					SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
					
					SET_WIDESCREEN_BORDERS(TRUE,0)			

					cutscene_cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE) 

					SET_CAM_PARAMS(cutscene_cam,<< -333.3030, -2780.6282, 6.7809 >>, << -7.0915, -0.0000, 29.9232 >>, 39.8038)
					SET_CAM_PARAMS(cutscene_cam,<< -333.2273, -2780.7600, 5.5619 >>, << -7.0915, -0.0000, 29.9232 >>, 38.7153,4000,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
					SHAKE_CAM(cutscene_cam,"HAND_SHAKE",0.3)
					RENDER_SCRIPT_CAMS(TRUE,FALSE)

					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ENDIF
					SETTIMERB(0)
					PRINTSTRING("GET IN TO BOAT CUTSCENE")
					PRINTNL()
					PRINT_HELP("DS2_MOCAP1")
					
					CREATE_ENEMIES(0,20,FALSE)
					CREATE_DOCK_DOORS()
					
					bTriggerDialogue = FALSE
					icutsceneprog++
				ENDIF

			BREAK
			
			CASE 1
				
				IF bTriggerDialogue = FALSE
					IF SETUP_PEDS_FOR_DIALOGUE()
						IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_2b", CONV_PRIORITY_MEDIUM )
							bTriggerDialogue = TRUE
						ENDIF
					ENDIF
				ENDIF
			
				IF TIMERB() > 3000
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							IF IS_VEHICLE_SEAT_FREE(mission_veh[2].veh,VS_BACK_LEFT)
								SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[2].veh,VS_BACK_LEFT)
							ENDIF
						ENDIF
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							IF IS_VEHICLE_SEAT_FREE(mission_veh[2].veh,VS_FRONT_RIGHT)
								SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],mission_veh[2].veh,VS_FRONT_RIGHT)
							ENDIF
						ENDIF
						IF NOT IS_PED_INJURED(Buddy[1].ped)
							IF IS_VEHICLE_SEAT_FREE(mission_veh[2].veh,VS_BACK_LEFT)
								SET_PED_INTO_VEHICLE(Buddy[1].ped,mission_veh[2].veh,VS_BACK_LEFT)
							ENDIF
						ENDIF
						IF IS_VEHICLE_SEAT_FREE(mission_veh[2].veh,VS_DRIVER)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh,VS_DRIVER)
						ENDIF
					ENDIF
					
					//FRANK VS_BACK_LEFT
					//MIKE VS_FRONT_RIGHT
					//GUNMAN VS_BACK_LEFT
					//mission_veh[2].veh
					SETTIMERB(0)
					icutsceneprog++
				ENDIF
				
			BREAK

			
			CASE 2  
				IF TIMERB() > 0
					
					SET_CAM_PARAMS(cutscene_cam,<< -285.8695, -2756.2280, 9.4254 >>, << -2.6397, -0.0000, 123.4216 >>,  38.7153)
					SET_CAM_PARAMS(cutscene_cam, << -285.6438, -2756.0784, 3.5622 >>, << -2.6397, -0.0000, 123.4216 >>, 38.7153,4200)
					SHAKE_CAM(cutscene_cam,"HAND_SHAKE",0.3)
					
					PRINTNL()	
					SETTIMERB(0)
					icutsceneprog++
				ENDIF
			BREAK

			CASE 3
				
				IF TIMERB() > 4000
				
					IF IS_VEHICLE_DRIVEABLE(mission_veh[2].veh)
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							IF IS_VEHICLE_SEAT_FREE(mission_veh[2].veh,VS_BACK_LEFT)
								SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[2].veh,VS_BACK_LEFT)
							ENDIF
						ENDIF
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							IF IS_VEHICLE_SEAT_FREE(mission_veh[2].veh,VS_FRONT_RIGHT)
								SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],mission_veh[2].veh,VS_FRONT_RIGHT)
							ENDIF
						ENDIF
						IF NOT IS_PED_INJURED(Buddy[1].ped)
							IF IS_VEHICLE_SEAT_FREE(mission_veh[2].veh,VS_BACK_LEFT)
								SET_PED_INTO_VEHICLE(Buddy[1].ped,mission_veh[2].veh,VS_BACK_LEFT)
							ENDIF
						ENDIF
						IF IS_VEHICLE_SEAT_FREE(mission_veh[2].veh,VS_DRIVER)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[2].veh,VS_DRIVER)
						ENDIF
					ENDIF

					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_WIDESCREEN_BORDERS(FALSE,0)

					IF DOES_CAM_EXIST(cutscene_cam)
						DESTROY_CAM(cutscene_cam)
					ENDIF
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					PRINTSTRING("GET IN BOAT CUTSCENE COMPLETE")
					PRINTNL()	
					CONTROL_FADE_IN(500)
					icutsceneprog=0
					bcutsceneplaying = FALSE
					KILL_ANY_CONVERSATION()
					bTriggerDialogue = FALSE
					RETURN TRUE
				ENDIF
				
			BREAK

		ENDSWITCH	

	ENDWHILE
	
	RETURN FALSE

ENDFUNC

//PURPOSE: runs the intro cutscene showing franklin exit the crate and the car be loaded onto the plane
FUNC BOOL COP_CUTSCENE() 

	SETTIMERA(0)
	bcutsceneplaying = TRUE

	WHILE bcutsceneplaying  //holds the script inside here until cutscene is complete

		WAIT(0)
		
		#IF IS_DEBUG_BUILD // allows j/p skip to work even in cutscene
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				bcutsceneplaying = FALSE
			ENDIF
		#ENDIF
		IF icutsceneprog > 2
			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()// controls the cutscene skip
				CONTROL_FADE_OUT(500)
				SETTIMERB(7000)
				icutsceneprog = 8
			ENDIF
		ENDIF

		SWITCH icutsceneprog
		
			CASE 0
			
				IF CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],CHAR_FRANKLIN,<< 485.7331, -3158.6196, 39.7286 >> ,356.6435)
					SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],<< 485.7331, -3158.6196, 39.7286 >>  )  
					SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], 356.6435)
					FREEZE_ENTITY_POSITION(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
					ADD_MODEL_REQUEST_TO_ARRAY(BARRACKS)
					ADD_MODEL_REQUEST_TO_ARRAY(S_M_Y_BlackOps_01)
					REQUEST_VEHICLE_RECORDING(1,"DHS2")
					SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
					icutsceneprog++
				ENDIF
				
			BREAK
		

			CASE 1  // creates inital cam and interp
			
				IF ARE_REQUESTED_MODELS_LOADED()
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1,"DHS2")
						IF NOT DOES_ENTITY_EXIST(mission_veh[0].veh)
							CREATE_ENEMY_VEHICLE(0,BARRACKS,<< 509.5533, -3017.2312, 5.0440 >> , 143.4425,S_M_Y_BlackOps_01,TRUE,0,1)
						ELSE
							IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
								START_PLAYBACK_RECORDED_VEHICLE(mission_veh[0].veh,1,"DHS2")
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(mission_veh[0].veh,3000)
								CLEANUP_LOADED_MODEL_ARRAY()
								REMOVE_VEHICLE_RECORDING(1,"DHS2")
								icutsceneprog++
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			BREAK
			
			CASE 2
			
				CLEAR_PRINTS()
				CLEAR_HELP()
				
				SET_WIDESCREEN_BORDERS(TRUE,0)			

				cutscene_cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE) 

				SET_CAM_PARAMS(cutscene_cam,<<485.396820,-3116.609131,6.009876>>, <<3.519925,0.000000,-10.670794>>, 38.971996)
				SET_CAM_PARAMS(cutscene_cam,<<486.052765,-3116.873535,6.009876>>, <<3.519925,-0.000000,-24.880651>>,  38.971996,2800,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
				
				RENDER_SCRIPT_CAMS(TRUE,FALSE)

				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(500)
				ENDIF
				SETTIMERB(0)
				icutsceneprog++

			BREAK
			
			CASE 3
			
				IF TIMERB() > 2700
				
					SET_CAM_PARAMS(cutscene_cam,<<495.927002,-3117.832764,6.065131>>, <<7.506814,-0.000000,150.035568>>, 43.478535)
					SET_CAM_PARAMS(cutscene_cam,<<496.552826,-3120.195557,6.065131>>, <<9.889005,0.000000,152.098465>>, 43.478535,2600,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)
					
					SETTIMERB(0)
					icutsceneprog++
				ENDIF

			BREAK

			CASE 4
			
			
				IF TIMERB() > 2500


					SET_CAM_PARAMS(cutscene_cam, << 492.1557, -3166.6506, 21.2532 >>, << -25.8159, 0.0000, 29.4920 >>, 45.0000)
					SET_CAM_PARAMS(cutscene_cam,<< 492.8423, -3165.2937, 20.3610 >>, << -30.7865, 0.0000, 21.7917 >>, 45.0000,4100,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)

					REQUEST_ANIM_DICT("MISSSAGRAB")
			
					SETTIMERB(0)
					icutsceneprog++
				ENDIF

			BREAK

			CASE 5
			
				IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
					IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[0].veh)
						FOR iCount = 0 TO 3
							IF NOT IS_PED_INJURED(driver[iCount].ped)
								TASK_LEAVE_VEHICLE(driver[iCount].ped,mission_veh[0].veh,ecf_dont_close_door)
								//TASK_LEAVE_ANY_VEHICLE(driver[iCount].ped)
							ENDIF
						ENDFOR
						SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
						icutsceneprog++
					ENDIF
				ENDIF
				
			BREAK 
			
			CASE 6
			
					
				SET_CAM_PARAMS(cutscene_cam, << 482.9392, -3162.8123, 44.0504 >>, << -47.0597, 0.0107, -32.7372 >>, 58.4886)
				SET_CAM_PARAMS(cutscene_cam, << 483.7547, -3162.8123, 44.0504 >>, << -47.1929, 0.0157, -22.2097 >>, 58.4886,4100,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)

				REQUEST_ANIM_DICT("MISSSAGRAB")
		
				SETTIMERB(0)
				icutsceneprog++

			
			BREAK
			
			
			CASE 7
			
				IF TIMERB() > 4000
				
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
						TASK_FOLLOW_NAV_MESH_TO_COORD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],<< 511.5626, -3125.4971, 5.0692 >>,PEDMOVE_RUN)
						//TASK_GO_STRAIGHT_TO_COORD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],<< 511.5626, -3125.4971, 5.0692 >>,PEDMOVE_RUN )
					ENDIF	
					
					SET_CAM_PARAMS(cutscene_cam, << 494.6721, -3147.9456, 14.3918 >>, << -8.1659, 0.0107, -52.1103 >>,12.5673)
					SET_CAM_PARAMS(cutscene_cam,<< 493.5122, -3145.7681, 12.8911 >>, << -9.0876, 0.0107, -50.8562 >>,12.5673,4100,GRAPH_TYPE_LINEAR,GRAPH_TYPE_LINEAR)

					REQUEST_ANIM_DICT("MISSSAGRAB")
			
					SETTIMERB(0)
					icutsceneprog++
					
				ENDIF

			BREAK
	
			CASE 8
				
				IF TIMERB() > 4000

					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					SET_WIDESCREEN_BORDERS(FALSE,0)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[0].veh)
							SKIP_TO_END_AND_STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[0].veh)
						ENDIF
					ENDIF
					IF DOES_CAM_EXIST(cutscene_cam)
						DESTROY_CAM(cutscene_cam) 
					ENDIF
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
						SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],<< 497.7497, -3126.0083, 5.0694 >> )  
						SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], 115.8071)
						//ADD_COVER_POINT(<< 497.7497, -3126.0083, 5.0694 >>,115.8071,COVUSE_WALLTONEITHER,COVHEIGHT_LOW,COVARC_180)
						TASK_PUT_PED_DIRECTLY_INTO_COVER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],<< 497.7497, -3126.0083, 5.0694 >>,-1)
					ENDIF
					PRINTSTRING("SUB CUTSCENE COMPLETE")
					PRINTNL()	
					CONTROL_FADE_IN(500)
					icutsceneprog=0
					bcutsceneplaying = FALSE
					RETURN TRUE
				ENDIF
				
			BREAK

		ENDSWITCH	

	ENDWHILE
	
	RETURN FALSE

ENDFUNC


FUNC BOOL DESTROY_VAN_CUTSCENE()

	SETTIMERA(0)
	bcutsceneplaying = TRUE

	WHILE bcutsceneplaying  //holds the script inside here until cutscene is complete

		WAIT(0)
		
		#IF IS_DEBUG_BUILD // allows j/p skip to work even in cutscene
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
				bcutsceneplaying = FALSE
			ENDIF
		#ENDIF

		SWITCH icutsceneprog
		
			CASE 0
			
				//bincrane = FALSE
				itimer =GET_GAME_TIMER()
				CLEAR_PRINTS()
				CLEAR_HELP()
				IF NOT DOES_CAM_EXIST(cutscene_cam)
					cutscene_cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",FALSE) 
				ENDIF
				DETACH_CAM(cutscene_cam)
				STOP_CAM_POINTING(cutscene_cam)
				SET_CAM_ACTIVE(cutscene_cam,TRUE)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				SET_CAM_PARAMS(cutscene_cam,<< 496.5680, -3150.8474, 5.5207 >>, << 12.7511, 0.0000, 53.5783 >>, 45.0000)
				SET_WIDESCREEN_BORDERS(TRUE,0)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
					APPLY_FORCE_TO_ENTITY(mission_veh[0].veh,APPLY_TYPE_EXTERNAL_IMPULSE,<<5,0,-3>>,<<0,0,0>>,0,TRUE,TRUE,TRUE)
				ENDIF
				icutsceneprog++
				
			BREAK
		
			CASE 1

				IF GET_GAME_TIMER() - itimer > 300
					SHAKE_CAM(cutscene_cam,"MEDIUM_EXPLOSION_SHAKE",1.0)
					SET_CONTROL_SHAKE(PLAYER_CONTROL,1000,200)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						//APPLY_FORCE_TO_ENTITY(mission_veh[0].veh,APPLY_TYPE_EXTERNAL_IMPULSE,<<5,0,-3>>,<<0,0,0>>,-1,TRUE,TRUE,TRUE)
						EXPLODE_VEHICLE(mission_veh[0].veh,TRUE)
					ENDIF
					itimer =GET_GAME_TIMER()
					icutsceneprog++
				ENDIF
				
			BREAK
		
			CASE 2
			
				IF GET_GAME_TIMER() - itimer > 2000
					IF DOES_CAM_EXIST(cutscene_cam)
						SET_CAM_ACTIVE(cutscene_cam,FALSE)
						DESTROY_CAM(cutscene_cam)
						RENDER_SCRIPT_CAMS(FALSE,FALSE)
					ENDIF
					SET_WIDESCREEN_BORDERS(FALSE,0)
					CONTROL_FADE_IN(500)
					//bincrane = TRUE
					icutsceneprog=0
					bcutsceneplaying = FALSE
					RETURN TRUE
				ENDIF
				
			BREAK

		ENDSWITCH	

	ENDWHILE
	
	RETURN FALSE

ENDFUNC

PROC MANAGE_BUDDIES()

	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		IF IS_ENTITY_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],<< 612.8863, -3099.8049, 5.0691 >>,<<3,3,5>>)
			DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		IF IS_ENTITY_AT_COORD(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],<< 612.8863, -3099.8049, 5.0691 >>,<<3,3,5>>)
			DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		ENDIF
	ENDIF

ENDPROC

PROC MANAGE_HIRED_TEAM_AI()
	SWITCH iGunmanAI
		CASE 0
		
		BREAK
	ENDSWITCH	 
ENDPROC



// PURPOSE: If player is in a bike/car, remove player control 'til the car is stopped 
PROC STOP_PLAYER_CAR(BOOL bAndLeave = FALSE,FLOAT carSpeed = 0.2)
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		
		FLOAT playerCarSpeed
		VEHICLE_INDEX playerCar
		playerCar = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		
		IF IS_VEHICLE_DRIVEABLE(playerCar)	
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			playerCarSpeed = GET_ENTITY_SPEED(playerCar)
			WHILE playerCarSpeed > carSpeed
				WAIT(0)
				IF NOT IS_ENTITY_DEAD(playerCar)
					playerCarSpeed = GET_ENTITY_SPEED(playerCar)
				ENDIF
			ENDWHILE

			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
		
	ENDIF
	
	//FINISH
	IF bAndLeave
	
	ENDIF
	
ENDPROC

// which ped is michael?
FUNC PED_INDEX GET_MICHAEL_PED()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		RETURN PLAYER_PED_ID()
	ELSE
		RETURN sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
	ENDIF
ENDFUNC

// which ped is trevor?
FUNC PED_INDEX GET_TREVOR_PED()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		RETURN PLAYER_PED_ID()
	ELSE
		RETURN sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
	ENDIF
ENDFUNC

// which ped is franklin?
FUNC PED_INDEX GET_FRANKLIN_PED()
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		RETURN PLAYER_PED_ID()
	ELSE
		RETURN sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
	ENDIF
ENDFUNC

PROC SET_FRANKLIN_CUSTOM_DRIVEBY_IN_CHOPPER()
	PED_INDEX FranklinPed = GET_FRANKLIN_PED()
	IF NOT IS_PED_INJURED(FranklinPed)
	AND IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
	    SET_PED_INTO_VEHICLE(FranklinPed, mission_veh[0].veh, VS_BACK_LEFT)
	    SET_PED_IN_VEHICLE_CONTEXT(FranklinPed, GET_HASH_KEY("MISSFBI2_MICHAEL_DRIVEBY"))
	    SET_PED_DRIVE_BY_CLIPSET_OVERRIDE(FranklinPed, "clipset@missfbi2_driveby")
		//SET_PED_DRIVE_BY_CLIPSET_OVERRIDE(FranklinPed,"veh@drivebyheli@rear_ds_2h")
	ENDIF
ENDPROC

PROC CLEANUP_FRANKLIN_SHOOTING_FROM_VEHICLE()
	SET_PED_INFINITE_AMMO_CLIP(PLAYER_PED_ID(), FALSE)
	RESET_PED_IN_VEHICLE_CONTEXT(PLAYER_PED_ID())
	CLEAR_PED_DRIVE_BY_CLIPSET_OVERRIDE(PLAYER_PED_ID())
	SET_PLAYER_FORCED_AIM(PLAYER_ID(), FALSE)
ENDPROC

PROC MANAGE_CURRENT_OBJECTIVE()

ENDPROC

//INT iTimeOfLastChaseTask

PROC DO_PERSISTENT_CHASE(PED_INDEX pedChaser,VEHICLE_INDEX vehChaser, BOOL bShoot, INT &iTimerForTask, INT maxSpeed = 25, INT minSpeed = 25, FLOAT vAttackOffsetX = 0.0,FLOAT vAttackOffsetY = 0.0,FLOAT vAttackOffsetZ = 0.0  )
	VECTOR vAttack
	IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
		IF IS_VECTOR_ZERO (<<vAttackOffsetX,vAttackOffsetY,vAttackOffsetZ>>)
			vAttack = GET_ENTITY_COORDS(mission_veh[0].veh)
			vAttack.z = 0.0
		ELSE
			vAttack = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, <<vAttackOffsetX,vAttackOffsetY,vAttackOffsetZ>>)
			vAttack.z = 0.0
		ENDIF
	ENDIF
	
	IF NOT IS_VECTOR_ZERO(vAttack)
	    IF NOT IS_PED_INJURED(pedChaser)
	    AND IS_VEHICLE_DRIVEABLE(vehChaser)
	        IF IS_PED_IN_VEHICLE(pedChaser, vehChaser)
	        AND IS_ENTITY_VISIBLE(vehChaser)
	          	IF (GET_GAME_TIMER() - iTimerForTask)>2000
	               	//TASK_VEHICLE_MISSION_COORS_TARGET(pedChaser, vehChaser, vAttack, MISSION_GOTO_RACING, 25, DRIVINGMODE_PLOUGHTHROUGH, 15, 15)
	                IF bShoot
	                  SET_PED_COMBAT_ATTRIBUTES(pedChaser, CA_DO_DRIVEBYS, TRUE)
					  IF NOT HAS_PED_GOT_WEAPON(pedChaser,WEAPONTYPE_MICROSMG)
					  	GIVE_WEAPON_TO_PED(pedChaser,WEAPONTYPE_MICROSMG,INFINITE_AMMO,TRUE)
					  ENDIF
					  SET_CURRENT_PED_WEAPON(pedChaser,WEAPONTYPE_MICROSMG,TRUE)
	                  TASK_DRIVE_BY(pedChaser, PLAYER_PED_ID(), NULL, <<0,0,0>>, -1, 50, TRUE)
					  
	                ENDIF
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedChaser,TRUE)
					SET_PED_COMBAT_ATTRIBUTES(pedChaser,CA_LEAVE_VEHICLES,FALSE)
					
					SET_ENTITY_LOAD_COLLISION_FLAG(vehChaser,TRUE)
					//NO LONGER USING MINI SPEED
					IF  minSpeed > 0
					
					ENDIF
					IF  maxSpeed > 0
					
					ENDIF
					//TASK_BOAT_MISSION(
					//BCF_OPENOCEANSETTINGS
						//IF IS_ENTITY_IN_ANGLED_AREA(pedChaser, <<-995.756104,6278.965820,-8.795349>>, <<-772.137329,5839.614258,27.650452>>, 110.000000)
						//OR IS_ENTITY_IN_ANGLED_AREA(pedChaser, <<-892.756104,6052.965820,-8.795349>>, <<-772.137329,5839.614258,27.650452>>, 160.000000)
						//OR IS_ENTITY_IN_ANGLED_AREA(pedChaser, <<-828.396667,5886.436523,-4.681030>>, <<-1314.487305,5324.369629,24.798889>>, 160.000000)
						//OR IS_ENTITY_IN_ANGLED_AREA(pedChaser, <<-1357.220947,5467.707031,-12.801041>>, <<-3248.458496,3311.530029,7.030090>>, 160.000000)
						//OR IS_ENTITY_IN_ANGLED_AREA(pedChaser, <<169.308746,7283.398926,-12.299652>>, <<-812.652039,6120.136719,12.434906>>, 160.000000)
						//	IF GET_SEAT_PED_IS_IN(pedChaser) = VS_DRIVER
						//		TASK_VEHICLE_TEMP_ACTION(pedChaser, GET_VEHICLE_PED_IS_USING(pedChaser), TEMPACT_HANDBRAKETURNRIGHT, 3000)
						//	ENDIF
						//ELSE
							TASK_BOAT_MISSION(pedChaser, vehChaser, NULL, NULL,vAttack,MISSION_GOTO,TO_FLOAT(maxSpeed),DRIVINGMODE_AVOIDCARS,-1,BCF_DEFAULTSETTINGS)
						//ENDIF
					//TASK_VEHICLE_DRIVE_TO_COORD(pedChaser, vehChaser, vAttack,TO_FLOAT(maxSpeed),DRIVINGSTYLE_ACCURATE,DINGHY,DRIVINGMODE_PLOUGHTHROUGH,1,1000)
					//TASK_GO_STRAIGHT_TO_COORD(pedChaser, vAttack,3.0,DEFAULT_TIME_NEVER_WARP)
					
					PRINTSTRING("TASKING BOATS")PRINTNL()
					PRINTSTRING("vChaseCoord:")PRINTVECTOR(vAttack)PRINTNL()
	                iTimerForTask = GET_GAME_TIMER()
	          	ENDIF
	        ENDIF
	    ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		DRAW_DEBUG_SPHERE(vAttack, 0.5, 255, 0, 0)
	#ENDIF
	
ENDPROC

FUNC BOOL INITIALISE_MERRY_WEATHER_ATTACK()
	VECTOR vAttack
	IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
		vAttack = GET_ENTITY_COORDS(mission_veh[0].veh)
		vAttack.z = 0.0
	ENDIF
	//DINGHY 1
	IF IS_VEHICLE_DRIVEABLE(mission_veh[20].veh)
		IF NOT IS_PED_INJURED(driver[0].ped)
		  TASK_VEHICLE_DRIVE_TO_COORD(driver[0].ped, mission_veh[20].veh, vAttack,20,DRIVINGSTYLE_ACCURATE,DINGHY,DRIVINGMODE_PLOUGHTHROUGH,-1,-1)
		  SET_PED_COMBAT_ATTRIBUTES(driver[0].ped, CA_DO_DRIVEBYS, TRUE)
          TASK_DRIVE_BY(driver[0].ped, PLAYER_PED_ID(), NULL, <<0,0,0>>, -1, 50, TRUE)
		  FREEZE_ENTITY_POSITION(mission_veh[20].veh,FALSE)
		  SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[20].veh,FALSE)
		  SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[20].veh,TRUE)
		ENDIF
		IF NOT IS_PED_INJURED(driver[1].ped)
			IF NOT HAS_PED_GOT_WEAPON(driver[1].ped,WEAPONTYPE_MICROSMG)
				GIVE_WEAPON_TO_PED(driver[1].ped,WEAPONTYPE_MICROSMG,INFINITE_AMMO,TRUE)
			ENDIF
			SET_CURRENT_PED_WEAPON(driver[1].ped,WEAPONTYPE_MICROSMG,TRUE)
			SET_PED_COMBAT_ATTRIBUTES(driver[1].ped, CA_DO_DRIVEBYS, TRUE)
          	TASK_DRIVE_BY(driver[1].ped, PLAYER_PED_ID(), NULL, <<0,0,0>>, -1, 50, TRUE)
		ENDIF
	ENDIF
	//DINGHY 2
	IF IS_VEHICLE_DRIVEABLE(mission_veh[21].veh)
		IF NOT IS_PED_INJURED(driver[2].ped)
		  TASK_VEHICLE_DRIVE_TO_COORD(driver[2].ped, mission_veh[21].veh, vAttack,20,DRIVINGSTYLE_ACCURATE,DINGHY,DRIVINGMODE_PLOUGHTHROUGH,-1,-1)
		  SET_PED_COMBAT_ATTRIBUTES(driver[2].ped, CA_DO_DRIVEBYS, TRUE)
          TASK_DRIVE_BY(driver[2].ped, PLAYER_PED_ID(), NULL, <<0,0,0>>, -1, 50, TRUE)
		  FREEZE_ENTITY_POSITION(mission_veh[21].veh,FALSE)
		  SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[21].veh,FALSE)
		  SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[21].veh,TRUE)
		ENDIF
		IF NOT IS_PED_INJURED(driver[3].ped)
			IF NOT HAS_PED_GOT_WEAPON(driver[3].ped,WEAPONTYPE_MICROSMG)
				GIVE_WEAPON_TO_PED(driver[3].ped,WEAPONTYPE_MICROSMG,-1,TRUE)
			ENDIF
			SET_CURRENT_PED_WEAPON(driver[3].ped,WEAPONTYPE_MICROSMG,TRUE)
			SET_PED_COMBAT_ATTRIBUTES(driver[3].ped, CA_DO_DRIVEBYS, TRUE)
          TASK_DRIVE_BY(driver[3].ped, PLAYER_PED_ID(), NULL, <<0,0,0>>, -1, 50, TRUE)
		ENDIF
	ENDIF
	//DINGHY 3
	IF IS_VEHICLE_DRIVEABLE(mission_veh[22].veh)
		IF NOT IS_PED_INJURED(driver[5].ped)
		  TASK_VEHICLE_DRIVE_TO_COORD(driver[5].ped, mission_veh[22].veh, vAttack,20,DRIVINGSTYLE_ACCURATE,DINGHY,DRIVINGMODE_PLOUGHTHROUGH,-1,-1)
		  SET_PED_COMBAT_ATTRIBUTES(driver[5].ped, CA_DO_DRIVEBYS, TRUE)
          TASK_DRIVE_BY(driver[5].ped, PLAYER_PED_ID(), NULL, <<0,0,0>>, 100, 50, TRUE)
		  FREEZE_ENTITY_POSITION(mission_veh[22].veh,FALSE)
		  SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[22].veh,FALSE)
		  SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[22].veh,TRUE)
		ENDIF
		
		IF NOT IS_PED_INJURED(driver[6].ped)
			IF NOT HAS_PED_GOT_WEAPON(driver[6].ped,WEAPONTYPE_MICROSMG)
				GIVE_WEAPON_TO_PED(driver[6].ped,WEAPONTYPE_MICROSMG,-1,TRUE)
			ENDIF
			SET_CURRENT_PED_WEAPON(driver[6].ped,WEAPONTYPE_MICROSMG,TRUE)
			SET_PED_COMBAT_ATTRIBUTES(driver[6].ped, CA_DO_DRIVEBYS, TRUE)
          TASK_DRIVE_BY(driver[6].ped, PLAYER_PED_ID(), NULL, <<0,0,0>>, -1, 50, TRUE)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
		//HELI 1
		IF IS_VEHICLE_DRIVEABLE(mission_veh[23].veh)
			IF NOT IS_PED_INJURED(driver[7].ped)
				FREEZE_ENTITY_POSITION(mission_veh[23].veh,FALSE)
				TASK_HELI_MISSION(driver[7].ped,mission_veh[23].veh,mission_veh[0].veh,NULL,<<0,0,0>>,MISSION_ATTACK, 50,50,-1,70,60)
				SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[23].veh,TRUE)
			ENDIF
		ENDIF
		//HELI 2
		IF IS_VEHICLE_DRIVEABLE(mission_veh[24].veh)
			IF NOT IS_PED_INJURED(driver[9].ped)
				FREEZE_ENTITY_POSITION(mission_veh[24].veh,FALSE)
				TASK_HELI_MISSION(driver[9].ped,mission_veh[24].veh,mission_veh[0].veh,NULL,<<0,0,0>>,MISSION_ATTACK, 40,30,-1,60,50)
				SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[24].veh,TRUE)
			ENDIF
		ENDIF
		//HELI 3
		IF IS_VEHICLE_DRIVEABLE(mission_veh[25].veh)
			IF NOT IS_PED_INJURED(driver[11].ped)
				FREEZE_ENTITY_POSITION(mission_veh[25].veh,FALSE)
				TASK_HELI_MISSION(driver[11].ped,mission_veh[25].veh,mission_veh[0].veh,NULL,<<0,0,0>>,MISSION_ATTACK,40,60,-1,50,40)
				SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[25].veh,TRUE)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[20].veh)
		IF NOT DOES_BLIP_EXIST(mission_veh[20].blip)
			mission_veh[20].blip = ADD_BLIP_FOR_ENTITY(mission_veh[20].veh)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[21].veh)
		IF NOT DOES_BLIP_EXIST(mission_veh[21].blip)
			mission_veh[21].blip = ADD_BLIP_FOR_ENTITY(mission_veh[21].veh)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[22].veh)
		IF NOT DOES_BLIP_EXIST(mission_veh[22].blip)
			mission_veh[22].blip = ADD_BLIP_FOR_ENTITY(mission_veh[22].veh)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[23].veh)
		IF NOT DOES_BLIP_EXIST(mission_veh[23].blip)
			mission_veh[23].blip = ADD_BLIP_FOR_ENTITY(mission_veh[23].veh)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[24].veh)
		IF NOT DOES_BLIP_EXIST(mission_veh[24].blip)
			mission_veh[24].blip = ADD_BLIP_FOR_ENTITY(mission_veh[24].veh)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[25].veh)
		IF NOT DOES_BLIP_EXIST(mission_veh[25].blip)
			mission_veh[25].blip = ADD_BLIP_FOR_ENTITY(mission_veh[25].veh)
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

INT iSkipCutsceneTimer 
BOOL bStartingCharMike

PROC OPENING_CUTSCENE()

	bcutsceneplaying = TRUE
	
	IF NOT IS_CUTSCENE_PLAYING()
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(),1.0)
	ENDIF

	SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
	SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0)
	SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0)
	IF DOES_ENTITY_EXIST(mission_veh[MV_MICHAELS_CAR].veh)
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MICHAELS_CAR].veh)
			REQUEST_VEHICLE_HIGH_DETAIL_MODEL(mission_veh[MV_MICHAELS_CAR].veh)
		ENDIF
	ENDIF

		#IF IS_DEBUG_BUILD
			IF IS_CUTSCENE_ACTIVE()
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
					STOP_CUTSCENE()
					REMOVE_CUTSCENE()
					WHILE IS_CUTSCENE_ACTIVE()
						WAIT(0)
					ENDWHILE
				ENDIF
			ENDIF
		#ENDIF

		IF e_section_stage = SECTION_STAGE_SETUP
			IF b_is_jumping_directly_to_stage
				CONTROL_FADE_IN(500)
				b_is_jumping_directly_to_stage = FALSE
			ELSE
				IF i_current_event = 0
					SET_WEATHER_TYPE_PERSIST("extrasunny")
				
					PRINTSTRING("PLAYING OPENING CUTSCENE - 1")PRINTNL()
					bcutsceneplaying = TRUE
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
						REQUEST_CUTSCENE("LSDH_2B_INT")
					ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("LSDH_2B_INT", CS_SECTION_3| CS_SECTION_4 |CS_SECTION_5| CS_SECTION_6| CS_SECTION_7)
					ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						REQUEST_CUTSCENE_WITH_PLAYBACK_LIST("LSDH_2B_INT",CS_SECTION_5| CS_SECTION_6| CS_SECTION_7)
					ENDIF
					
					IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[2])
						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[2],TRUE,TRUE)
						sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = g_sTriggerSceneAssets.ped[2]
					ENDIF
					
					IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[3])
						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[3],TRUE,TRUE)
						sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = g_sTriggerSceneAssets.ped[3]
					ENDIF
					
					IF NOT DOES_ENTITY_EXIST( objSofa )
						objSofa = GET_CLOSEST_OBJECT_OF_TYPE( <<-1158.5811, -1519.2471, 9.6308>>, 2.0, V_RES_TRE_SOFA_MESS_C )
					ELSE
						SET_ENTITY_VISIBLE( objSofa, FALSE )
					ENDIF
					
					SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1157.174316,-1523.046021,4.804760>>-<<65.000000,65.000000,1.750000>>,<<-1157.174316,-1523.046021,4.804760>>+<<65.000000,65.000000,1.750000>>,FALSE)
					
					SET_PED_POPULATION_BUDGET(0)		
					SET_VEHICLE_POPULATION_BUDGET(0)
					
					bStartingCharMike = FALSE
					bClearCutscenArea = FALSE
					i_current_event++
				ELIF i_current_event = 1
					IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
						IF IS_SCREEN_FADED_OUT()
							DO_SCREEN_FADE_IN(500)
						ELSE
							WAIT(0)
						ENDIF
						
//						IF IS_REPLAY_IN_PROGRESS()
							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
								IF NOT DOES_ENTITY_EXIST( sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
									WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_MICHAEL, <<-1155.21, -1517.32, 3.36>>,0,FALSE,TRUE)
										WAIT(0)
									ENDWHILE
									bStartingCharMike = TRUE
								ENDIF
							ENDIF
//						ENDIF
						
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
								MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
								TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
								
								SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], rel_group_buddies)
								SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							ENDIF
						ENDIF
						
						IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
								MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
								TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE,TCF_CLEAR_TASK_INTERRUPT_CHECKS)
						    	
								SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], rel_group_buddies)
								SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
							SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], FALSE, TRUE)
							REGISTER_ENTITY_FOR_CUTSCENE(g_sTriggerSceneAssets.ped[0], "Wade", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, IG_WADE)
						ENDIF
						
						IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
							SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[1], FALSE, TRUE)
							REGISTER_ENTITY_FOR_CUTSCENE(g_sTriggerSceneAssets.ped[1], "Floyd", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY, IG_FLOYD)
						ENDIF
						
						IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								SET_ENTITY_AS_MISSION_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], FALSE, TRUE)
								REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
							ENDIF
						ELSE
							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
								REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Trevor", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PLAYER_TWO)
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
								SET_ENTITY_AS_MISSION_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE, TRUE)
								REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ONE)
							ENDIF
						ELSE
							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
								REGISTER_ENTITY_FOR_CUTSCENE(NULL, "Franklin", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, PLAYER_ONE)
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
								SET_ENTITY_AS_MISSION_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE, TRUE)
								REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
							ENDIF
						ENDIF
						
						IF DOES_ENTITY_EXIST(veh_pre_mission_car)
						
						ENDIF
						
						SET_CUTSCENE_FADE_VALUES(FALSE,FALSE,FALSE,FALSE)
						
						IF 	GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_TREVOR
							START_CUTSCENE(CUTSCENE_PLAYER_FP_FLASH_MICHAEL)
						ELIF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_MICHAEL
							START_CUTSCENE()
						ELIF GET_CURRENT_PLAYER_PED_ENUM()  = CHAR_FRANKLIN
							START_CUTSCENE(CUTSCENE_PLAYER_FP_FLASH_MICHAEL)
						ENDIF

						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)

						PRINTSTRING("PLAYING OPENING CUTSCENE - 2")PRINTNL()

						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						b_skipped_mocap = FALSE
						e_section_stage = SECTION_STAGE_RUNNING
					ELSE
						PRINTSTRING("CUTSCENE LOADING")PRINTNL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_RUNNING
			IF NOT b_skipped_mocap
				IF bClearCutscenArea = TRUE
					IF MANAGE_MY_TIMER(iSkipCutsceneTimer,2000)
						IF IS_CUTSCENE_SKIP_BUTTON_PRESSED()
							b_skipped_mocap = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_CUTSCENE_PLAYING()
				IF bClearCutscenArea = FALSE
					
					VEHICLE_INDEX vehTemp
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		                vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
		                PRINTLN("PLAYER IS IN VEHICLE")
					ELSE
		                vehTemp = GET_LAST_DRIVEN_VEHICLE()
		                PRINTLN("PLAYER IS NOT IN VEHICLE")
					ENDIF

					IF DOES_ENTITY_EXIST(vehTemp)
					AND IS_VEHICLE_DRIVEABLE(vehTemp)
		                IF NOT IS_VEHICLE_MODEL(vehTemp, TAXI)
						AND IS_REPLAY_VEHICLE_MODEL_UNDER_SIZE_LIMIT(GET_ENTITY_MODEL(vehTemp), GET_DEFAULT_ALLOWABLE_VEHICLE_SIZE_VECTOR())
                            IF VDIST(GET_ENTITY_COORDS(vehTemp), GET_ENTITY_COORDS(PLAYER_PED_ID())) < 50
                                PRINTLN("PLAYER VEHICLE IS NEARBY")
                                SET_ENTITY_AS_MISSION_ENTITY(vehTemp, TRUE, TRUE)
                                #IF IS_DEBUG_BUILD
                                	PRINTLN("Vehicle belongs to: ", GET_PLAYER_PED_PERSONAL_VEHICLE_BELONGS_TO(vehTemp))
                                #ENDIF
								
								MODEL_NAMES model = GET_ENTITY_MODEL(vehTemp)
								VECTOR vMin
								VECTOR vMax
								GET_MODEL_DIMENSIONS(model, vMin, vMax)
								FLOAT fHeight = -vMin.z + vMax.z
								
                            	IF GET_PLAYER_PED_PERSONAL_VEHICLE_BELONGS_TO(vehTemp) = CHAR_TREVOR OR fHeight > 2.6
	                                DELETE_VEHICLE(vehTemp)
	                                PRINTLN("Deleting as this is the player's vehicle")
                                ELSE
									IF NOT IS_PLAYER_VEHICLE_IN_AREA(CHAR_TREVOR,VEHICLE_TYPE_CAR,<<-1150.4517, -1531.4274, 3.2486>>,5)
	                                    SET_MISSION_VEHICLE_GEN_VEHICLE(vehTemp, <<-1157.6465, -1522.1222, 3.3238>>, 35.2272)//<<-1150.4517, -1531.4274, 3.2486>>, 35.6290)
	                                    SET_ENTITY_COORDS(vehTemp, <<-1157.6465, -1522.1222, 3.3238>>)//<<-1150.4517, -1531.4274, 3.2486>>)
	                                    SET_ENTITY_HEADING(vehTemp, 35.2272)//35.6290)
	                                    SET_VEHICLE_DOORS_SHUT(vehTemp, TRUE)
	                                    PRINTLN("SETTING PLAYER VEH AS veh gen")
									ELSE
										IF NOT IS_POSITION_OCCUPIED( <<-1155.5046, -1520.1312, 3.3472>>,5,FALSE,TRUE,FALSE,FALSE,FALSE)
										OR IS_ENTITY_AT_COORD(vehTemp,<<-1155.5046, -1520.1312, 3.3472>>,<<5,5,5>>)
											SET_MISSION_VEHICLE_GEN_VEHICLE(vehTemp, <<-1155.5046, -1520.1312, 3.3472>>, 35.1124)
		                                    SET_ENTITY_COORDS(vehTemp, <<-1155.5046, -1520.1312, 3.3472>>)
		                                    SET_ENTITY_HEADING(vehTemp, 35.1124)
		                                    SET_VEHICLE_DOORS_SHUT(vehTemp, TRUE)
		                                    PRINTLN("SETTING PLAYER VEH AS veh gen")
										ELSE
											SET_MISSION_VEHICLE_GEN_VEHICLE(vehTemp, <<-1191.0647, -1504.3447, 3.3697>>, 303.3213)
		                                    SET_ENTITY_COORDS(vehTemp, <<-1191.0647, -1504.3447, 3.3697>>)
		                                    SET_ENTITY_HEADING(vehTemp, 303.3213)
		                                    SET_VEHICLE_DOORS_SHUT(vehTemp, TRUE)
		                                    PRINTLN("SETTING PLAYER VEH AS veh gen")
										ENDIF
									ENDIF
                                ENDIF
                            ENDIF
		                ENDIF
					ENDIF
					
					CLEAR_AREA(<< -1154.6117, -1520.0699, 3.3456 >>,20,TRUE)
					CLEAR_MUST_LEAVE_AREA_VEHICLE_GEN_FLAG(VEHGEN_MISSION_VEH)


//					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA(<<-1162.025269,-1511.654175,2.393471>>, <<-1151.060181,-1527.200562,6.268250>>, 10.000000, <<-1155.5077, -1520.1217, 3.3467>>, 35.3350)
//					SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<-1155.5077, -1520.1217, 3.3467>>, 35.3350)
//					
//					DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
//					DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<1187,-1398,4.3>>, 10)
//					DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_TREVOR_SAVEHOUSE_CITY)
//					RESOLVE_VEHICLES_INSIDE_ANGLED_AREA_WITH_SIZE_LIMIT(<<-1167.186768,-1506.043579,2.992951>>, <<-1146.530762,-1533.760376,6.597246>>, 9.000000,<<-1155.0127, -1522.8375, 3.3244>>, 38.2544,<<5,5,3>>)
					
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							IF IS_PED_WEARING_HELMET(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
								REMOVE_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],TRUE)
							ENDIF
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							IF IS_PED_WEARING_HELMET(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
								REMOVE_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
							ENDIF
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							IF IS_PED_WEARING_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								REMOVE_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRUE)
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF IS_PED_WEARING_HELMET(PLAYER_PED_ID())
							REMOVE_PED_HELMET(PLAYER_PED_ID(),TRUE)
						ENDIF
					ENDIF
					//Move the last vehicle somewhere if it's in the way.
					veh_pre_mission_car = GET_PLAYERS_LAST_VEHICLE()
//					DISABLE_VEHICLE_GEN_ON_MISSION(TRUE)
//					DELETE_VEHICLE_GEN_VEHICLES_IN_AREA(<<1187,-1398,4.3>>, 10)
//					DELETE_VEHICLE_GEN_VEHICLE(VEHGEN_TREVOR_SAVEHOUSE_CITY)
//					RESOLVE_VEHICLES_AT_MISSION_TRIGGER(<<-1161.9291, -1525.1603, 3.2477>>, 35.1845,FALSE)

					PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P0_STEALTH_NO_MASK)
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						PRELOAD_OUTFIT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], OUTFIT_P1_STEALTH_NO_MASK)
					ENDIF
					IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						PRELOAD_OUTFIT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], OUTFIT_P2_STEALTH_NO_MASK)
					ENDIF
					
					STOP_GAMEPLAY_HINT(TRUE)
					SET_GAMEPLAY_HINT_CAMERA_BLEND_TO_FOLLOW_PED_MEDIUM_VIEW_MODE(FALSE)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_FLOYD)
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_WADE)
					iSkipCutsceneTimer  = GET_GAME_TIMER()
					bClearCutscenArea = TRUE
				ENDIF
			ENDIF
			
			IF NOT IS_PLAYER_VEHICLE_IN_AREA(CHAR_TREVOR,VEHICLE_TYPE_CAR,<<-1154.6028, -1524.2793, 3.2583>>,40)
				IF bStartingCharMike = TRUE
					IF NOT DOES_ENTITY_EXIST(mission_veh[ENUM_TO_INT(MV_TREVORS_TRUCK)].veh)
						IF NOT IS_POSITION_OCCUPIED(<<-1155.5077, -1520.1217, 3.3467>>,5,FALSE,TRUE,FALSE,FALSE,FALSE)
							SETUP_MISSION_REQUIREMENT(REQ_TREVORS_TRUCK, <<-1155.5077, -1520.1217, 3.3467>>, 35.3350)
						ELSE
							IF NOT IS_POSITION_OCCUPIED(<<-1158.4879, -1529.3704, 3.2503>>,5,FALSE,TRUE,FALSE,FALSE,FALSE)
								SETUP_MISSION_REQUIREMENT(REQ_TREVORS_TRUCK, <<-1158.4879, -1529.3704, 3.2503>>, 35.6275)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(mission_veh[ENUM_TO_INT(MV_TREVORS_TRUCK)].veh)
					IF NOT IS_POSITION_OCCUPIED(<<-1155.5077, -1520.1217, 3.3467>>,5,FALSE,TRUE,FALSE,FALSE,FALSE)
						SETUP_MISSION_REQUIREMENT(REQ_TREVORS_TRUCK, <<-1155.5077, -1520.1217, 3.3467>>, 35.3350)
					ELSE
						IF NOT IS_POSITION_OCCUPIED(<<-1158.4879, -1529.3704, 3.2503>>,5,FALSE,TRUE,FALSE,FALSE,FALSE)
							SETUP_MISSION_REQUIREMENT(REQ_TREVORS_TRUCK, <<-1158.4879, -1529.3704, 3.2503>>, 35.6275)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF GET_CUTSCENE_TIME() > 134367
				IF NOT HAS_LABEL_BEEN_TRIGGERED("PRELOAD OUTFITS")
					
					PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P0_STEALTH_NO_MASK)
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						PRELOAD_OUTFIT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], OUTFIT_P1_STEALTH_NO_MASK)
					ENDIF
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						PRELOAD_OUTFIT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], OUTFIT_P2_STEALTH_NO_MASK)
					ENDIF
					SET_LABEL_AS_TRIGGERED("PRELOAD OUTFITS",TRUE)
				ENDIF
			ENDIF
			
			IF GET_CUTSCENE_TIME() > 144367
				IF NOT HAS_LABEL_BEEN_TRIGGERED("SWITCH OUTFIT")
					PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P0_STEALTH_NO_MASK)
					SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH_NO_MASK, FALSE)
					SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)                //set that the player has changed clothes on mission
					
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						PRELOAD_OUTFIT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], OUTFIT_P1_STEALTH_NO_MASK)
						SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], COMP_TYPE_OUTFIT, OUTFIT_P1_STEALTH_NO_MASK, FALSE)
						SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_FRANKLIN)                //set that the player has changed clothes on mission
					ENDIF
					
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						PRELOAD_OUTFIT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], OUTFIT_P2_STEALTH_NO_MASK)
						SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_OUTFIT, OUTFIT_P2_STEALTH_NO_MASK, FALSE)
						SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR)                //set that the player has changed clothes on mission
					ENDIF
					SET_LABEL_AS_TRIGGERED("SWITCH OUTFIT",TRUE)
				ENDIF
			ENDIF
			
			IF bClearCutscenArea = TRUE
				IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					ENTITY_INDEX entity_franklin = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklin")
					IF DOES_ENTITY_EXIST(entity_franklin)
						sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = GET_PED_INDEX_FROM_ENTITY_INDEX(entity_franklin)
					ENDIF
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					ENTITY_INDEX entity_trevor = GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Trevor")
					IF DOES_ENTITY_EXIST(entity_trevor)
						sSelectorPeds.pedID[SELECTOR_PED_TREVOR] = GET_PED_INDEX_FROM_ENTITY_INDEX(entity_trevor)
					ENDIF
				ENDIF
				
				//SETUP_MISSION_REQUIREMENT(REQ_MICHAELS_CAR, <<-1155.5077, -1520.1217, 3.3467>>, 35.3350)
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					PRINTSTRING("CAN_SET_EXIT_STATE_FOR_CAMERA")PRINTNL()
				ENDIF

				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
					IF NOT b_skipped_mocap
						//SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1700, 127.2389, FALSE)
						TASK_FOLLOW_NAV_MESH_TO_COORD(PLAYER_PED_ID(),<<-1153.01, -1524.73, 4.26>>,1)
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT, TRUE)
					ENDIF
					PRINTSTRING("CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(Michael)")PRINTNL()
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
						SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], TRUE)
						//FORCE_PED_MOTION_STATE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
					ENDIF
					PRINTSTRING("CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(Trevor)")PRINTNL()
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
						SET_ENTITY_LOAD_COLLISION_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], TRUE)
						FORCE_PED_MOTION_STATE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
					ENDIF
					PRINTSTRING("CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY(Franklin)")PRINTNL()
				ENDIF

				IF HAS_CUTSCENE_FINISHED()
				AND bClearCutscenArea = TRUE
					e_section_stage = SECTION_STAGE_CLEANUP
				ENDIF
			ENDIF
			
			#IF IS_DEBUG_BUILD
				IF IS_CUTSCENE_ACTIVE()
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
						STOP_CUTSCENE()
						REMOVE_CUTSCENE()
						WHILE IS_CUTSCENE_ACTIVE()
							WAIT(0)
						ENDWHILE
					ENDIF
				ENDIF
			#ENDIF
			
		ENDIF

		IF e_section_stage = SECTION_STAGE_CLEANUP
			
			//Make sure necessary assets get made.
			
			
			
			IF b_skipped_mocap
				REPLAY_CANCEL_EVENT()
				//If the player skipped the mocap the peds need to be warped.
				STOP_CUTSCENE()
				//Make sure necessary assets get made.
				IF NOT IS_SCREEN_FADING_OUT()
				OR NOT IS_SCREEN_FADED_OUT()
					CONTROL_FADE_OUT(500)
				ENDIF
				
				WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_TREVOR,<<-1155.21, -1517.32, 3.36>>)
				OR NOT SETUP_MISSION_REQUIREMENT(REQ_FRANKLIN,<<-1157.92, -1519.33, 3.36>>)
					WAIT(0)
				ENDWHILE
				
				PRELOAD_OUTFIT(PLAYER_PED_ID(), OUTFIT_P0_STEALTH_NO_MASK)
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
					PRELOAD_OUTFIT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], OUTFIT_P1_STEALTH_NO_MASK)
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
					PRELOAD_OUTFIT(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], OUTFIT_P2_STEALTH_NO_MASK)
				ENDIF
				
				WHILE NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
					WAIT(0)
				ENDWHILE
				
				WHILE NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				AND NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					WAIT(0)
				ENDWHILE
				
				WHILE NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				AND NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					WAIT(0)
				ENDWHILE
				
				SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_STEALTH_NO_MASK, FALSE)
				SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], COMP_TYPE_OUTFIT, OUTFIT_P1_STEALTH_NO_MASK, FALSE)
				SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], COMP_TYPE_OUTFIT, OUTFIT_P2_STEALTH_NO_MASK, FALSE)
				
				SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)                //set that the player has changed clothes on mission
				SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_TREVOR)                //set that the player has changed clothes on mission
				SET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_FRANKLIN)                //set that the player has changed clothes on mission

				GIVE_ALL_PEDS_AN_EARPIECE()
				
				WHILE IS_CUTSCENE_ACTIVE()
					WAIT(0)
				ENDWHILE
				
				//IF IS_SCREEN_FADING_OUT()
					SET_ENTITY_COORDS(PLAYER_PED_ID(),<<-1149.6552, -1521.8536, 3.3298>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(),76.5150)
					
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
						SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],<<-1148.6027, -1525.5420, 3.3486>>)
						SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],79.3802)
					ENDIF
					
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
						SET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],<<-1148.9454, -1523.2805, 3.3434>>)
						SET_ENTITY_HEADING(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],48.7325)
					ENDIF
				//ENDIF
				
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH()
			ENDIF
			
			IF DOES_ENTITY_EXIST( objSofa )
				SET_ENTITY_VISIBLE( objSofa, TRUE )
				SET_OBJECT_AS_NO_LONGER_NEEDED( objSofa )
			ENDIF

			REPLAY_STOP_EVENT()

			//Setup buddy relationship groups etc.
			INFORM_MISSION_STATS_SYSTEM_OF_INGAME_CUTSCENE_END()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			
			SET_PED_POPULATION_BUDGET(3)		
			SET_VEHICLE_POPULATION_BUDGET(3)
			
			i_current_event = 0
			PRINTSTRING("JUMPING TO STAGE 1")
			eMissionStage = MISSION_STAGE_GET_TO_AIRSTRIP
			e_section_stage = SECTION_STAGE_SETUP
			SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
			//Ensure the trigger scene assets are cleaned up from memory.
			MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_DOCKS_2B)
			SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-1157.174316,-1523.046021,4.804760>>-<<65.000000,65.000000,1.750000>>,<<-1157.174316,-1523.046021,4.804760>>+<<65.000000,65.000000,1.750000>>,TRUE)
			SET_VEHICLE_POPULATION_BUDGET(3)
			SET_PED_POPULATION_BUDGET(3)
			SET_WEATHER_TYPE_PERSIST("extrasunny")
			bcutsceneplaying = FALSE
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_SKIP
			STOP_CUTSCENE()
			b_skipped_mocap = TRUE
			e_section_stage = SECTION_STAGE_RUNNING
		ENDIF

ENDPROC

INT ihorn
INT ihornTime

PROC MANAGE_DRIVE_IN_HORN()
	SWITCH ihorn
		CASE 0
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_PACKER].veh)
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_PACKER].veh)
					IF GET_DISTANCE_BETWEEN_COORDS(<<1728.97, 3264.86, 40.0871>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) < 20
						SET_HORN_PERMANENTLY_ON_TIME(mission_veh[MV_PACKER].veh,500)
						ihornTime = GET_GAME_TIMER()
						ihorn ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 1
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_PACKER].veh)
				IF MANAGE_MY_TIMER(ihornTime,1000)
					SET_HORN_PERMANENTLY_ON_TIME(mission_veh[MV_PACKER].veh,600)
					ihorn ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_PACKER].veh)
				IF MANAGE_MY_TIMER(ihornTime,1000)
					SET_HORN_PERMANENTLY_ON_TIME(mission_veh[MV_PACKER].veh,800)
					ihorn ++
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

BOOL bCreateMissionAssets = FALSE
BOOL bFirstTime = FALSE
STRING sGoTo
INT iSafeTimer

PROC CHECK_PED_IS_BODHI2_PASSENGER(PED_INDEX ped, BOOL &bInVehicle)
	IF IS_PED_IN_ANY_VEHICLE(ped) // check to see if franklin is in a BODHI2
		IF NOT bInVehicle // just got in
			VEHICLE_INDEX veh = GET_VEHICLE_PED_IS_IN(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
			MODEL_NAMES model = GET_ENTITY_MODEL(veh)
			VEHICLE_SEAT seat = GET_SEAT_PED_IS_IN(ped)
			
			IF model = BODHI2 AND (seat <> VS_DRIVER AND seat <> VS_FRONT_RIGHT)
				SET_PED_CONFIG_FLAG(ped,PCF_DontActivateRagdollFromVehicleImpact,TRUE)
				SET_PED_CONFIG_FLAG(ped,PCF_DontActivateRagdollFromBulletImpact,TRUE)
				SET_PED_CONFIG_FLAG(ped,PCF_DontActivateRagdollFromExplosions,TRUE)
				SET_PED_CONFIG_FLAG(ped,PCF_DontActivateRagdollFromFire,TRUE)
				SET_PED_CONFIG_FLAG(ped,PCF_DontActivateRagdollFromElectrocution,TRUE)
			ENDIF
		ENDIF
		bInVehicle = TRUE
	ELSE
		IF bInVehicle // just got out
			SET_PED_CONFIG_FLAG(ped,PCF_DontActivateRagdollFromVehicleImpact,FALSE)
			SET_PED_CONFIG_FLAG(ped,PCF_DontActivateRagdollFromBulletImpact,FALSE)
			SET_PED_CONFIG_FLAG(ped,PCF_DontActivateRagdollFromExplosions,FALSE)
			SET_PED_CONFIG_FLAG(ped,PCF_DontActivateRagdollFromFire,FALSE)
			SET_PED_CONFIG_FLAG(ped,PCF_DontActivateRagdollFromElectrocution,FALSE)
		ENDIF
		bInVehicle = FALSE
	ENDIF
ENDPROC

PROC GET_TO_AIRSTRIP()
	
	#IF IS_DEBUG_BUILD 
		DONT_DO_J_SKIP(sLocatesData)
	#ENDIF
	
	//SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(mission_veh[MV_CHINOOK].veh,imaxRopeLength,IminRopeLength)
	
	MANAGE_DRIVE_IN_HORN()
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_TREVORS_TRUCK].veh)
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_TREVORS_TRUCK].veh)
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_TREVORS_TRUCK].veh)
				IF IS_ENTRY_POINT_FOR_SEAT_CLEAR(PLAYER_PED_ID(),mission_veh[MV_TREVORS_TRUCK].veh,VS_DRIVER)  
					SET_PED_RESET_FLAG(PLAYER_PED_ID(),PRF_ForcePlayerToEnterVehicleThroughDirectDoorOnly,TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT DOES_ENTITY_EXIST(mission_veh[MV_CHINOOK].veh)
		IF GET_DISTANCE_BETWEEN_COORDS(<<1757.8193, 3270.4543, 40.2458>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) < 300
			SETUP_MISSION_REQUIREMENT(REQ_MISSION_HELI_WITHOUT_ROPE,<<1750.6334, 3285.0278, 40.0871>>,134.9916)
		ENDIF
	ELSE
		IF GET_DISTANCE_BETWEEN_COORDS(<<1757.8193, 3270.4543, 40.2458>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) > 350
			IF DOES_ENTITY_EXIST(mission_veh[MV_CHINOOK].veh)
				DELETE_VEHICLE(mission_veh[MV_CHINOOK].veh)
			ENDIF
			SET_MODEL_AS_NO_LONGER_NEEDED(CARGOBOB3)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
			IF GET_VEHICLE_DOOR_LOCK_STATUS(mission_veh[MV_CHINOOK].veh) != VEHICLELOCK_LOCKED
				SET_VEHICLE_DOORS_LOCKED(mission_veh[MV_CHINOOK].veh,VEHICLELOCK_LOCKED)
			ENDIF
		ENDIF
	ENDIF
	
	IF bFirstTime = TRUE
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_PACKER].veh)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_PACKER].veh)
				SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
				PRINTSTRING("GET_TIME_POSITION_IN_RECORDING")PRINTFLOAT(GET_TIME_POSITION_IN_RECORDING(mission_veh[MV_PACKER].veh))PRINTNL()
				IF NOT HAS_LABEL_BEEN_TRIGGERED("GO TO AI RECORDING")	
					IF GET_DISTANCE_BETWEEN_COORDS(<<1728.97, 3264.86, 41.22>>,GET_ENTITY_COORDS(mission_veh[MV_PACKER].veh)) < 2
						//SET_PLAYBACK_TO_USE_AI(mission_veh[MV_PACKER].veh,DRIVINGMODE_AVOIDCARS)
						//SET_LABEL_AS_TRIGGERED("GO TO AI RECORDING",TRUE)
					ENDIF
				ENDIF
			ELSE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("GO TO AI RECORDING")	
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_ARMY_TRAILER].veh)
						FREEZE_ENTITY_POSITION(mission_veh[MV_ARMY_TRAILER].veh,TRUE)
						FREEZE_ENTITY_POSITION(mission_veh[MV_PACKER].veh,TRUE)
						SET_LABEL_AS_TRIGGERED("GO TO AI RECORDING",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_COORDS(<<1745.67480, 3418.85498, 36.94403>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) < 560
		IF NOT DOES_ENTITY_EXIST(pedFloyd)
			SETUP_MISSION_REQUIREMENT(REQ_FLOYD,<<1730.73, 3265.25, 41.16>>,-124.36)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(pedWade)
			SETUP_MISSION_REQUIREMENT(REQ_WADE,<<1729.2040, 3262.7756, 40.1735>>,274.1224)
		ENDIF
		
		IF bCreateMissionAssets = FALSE
			IF bFirstTime = FALSE
				IF SETUP_MISSION_REQUIREMENT(REQ_SUB_ON_TRUCK,<<1745.6003, 3418.2341, 36.9515>>, 209.7394)
					bCreateMissionAssets = TRUE
				ENDIF
			ENDIF
		ELSE
			IF bFirstTime = FALSE
				REQUEST_VEHICLE_RECORDING(34,"DHF2")
				REQUEST_VEHICLE_RECORDING(35,"DHF2")
				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(34,"DHF2")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(35,"DHF2")
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_PACKER].veh)
					AND IS_VEHICLE_DRIVEABLE(mission_veh[MV_ARMY_TRAILER].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_PACKER].veh)
						AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_ARMY_TRAILER].veh)
							START_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_PACKER].veh,34,"DHF2")
							START_PLAYBACK_RECORDED_VEHICLE(mission_veh[MV_ARMY_TRAILER].veh,35,"DHF2")
							SET_PLAYBACK_SPEED(mission_veh[MV_PACKER].veh,1.2)
							SET_PLAYBACK_SPEED(mission_veh[MV_ARMY_TRAILER].veh,1.2)
						ELSE
							IF DOES_ENTITY_EXIST(pedFloyd)
							AND DOES_ENTITY_EXIST(pedWade)
								IF NOT IS_PED_INJURED(pedFloyd)
								AND NOT IS_PED_INJURED(pedWade)
									IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(pedFloyd,mission_veh[MV_PACKER].veh,VS_DRIVER)
										SET_PED_INTO_VEHICLE(pedFloyd,mission_veh[MV_PACKER].veh,VS_DRIVER)
									ENDIF
									IF NOT IS_PED_SITTING_IN_VEHICLE_SEAT(pedWade,mission_veh[MV_PACKER].veh,VS_FRONT_RIGHT)
										SET_PED_INTO_VEHICLE(pedWade,mission_veh[MV_PACKER].veh,VS_FRONT_RIGHT)
									ENDIF
									PRINTSTRING("create recording mission assets")PRINTNL()
									bFirstTime = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF bFirstTime = TRUE
			IF bCreateMissionAssets = FALSE						
				IF SETUP_MISSION_REQUIREMENT(REQ_SUB_ON_TRUCK_ON_RUNWAY,<<1728.97, 3264.86, 40.0871>>,116.37)	
					PRINTSTRING("create mission assets")PRINTNL()
					bCreateMissionAssets = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	IF GET_DISTANCE_BETWEEN_COORDS(<<1750.6334, 3285.0278, 40.0871>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) > 650
		IF bFirstTime = TRUE
			IF DOES_ENTITY_EXIST(mission_veh[MV_PACKER].veh)
				DELETE_VEHICLE(mission_veh[MV_PACKER].veh)
				SET_MODEL_AS_NO_LONGER_NEEDED(PACKER)
			ENDIF
			
			IF DOES_ENTITY_EXIST(mission_veh[MV_SUBMERISIBLE].veh)
				DELETE_VEHICLE(mission_veh[MV_SUBMERISIBLE].veh)
				SET_MODEL_AS_NO_LONGER_NEEDED(SUBMERSIBLE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(mission_veh[MV_ARMY_TRAILER].veh)
				DELETE_VEHICLE(mission_veh[MV_ARMY_TRAILER].veh)
				SET_MODEL_AS_NO_LONGER_NEEDED(ARMYTRAILER)
			ENDIF
			
			IF bCreateMissionAssets = TRUE
				bCreateMissionAssets = FALSE
			ENDIF
			
			PRINTSTRING("delete mission assets")PRINTNL()

			IF DOES_ENTITY_EXIST(pedFloyd)
				DELETE_PED(pedFloyd)
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_FLOYD)
			ENDIF
			IF DOES_ENTITY_EXIST(pedWade)
				DELETE_PED(pedWade)
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_WADE)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_SUBMERISIBLE].veh)
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
			IF GET_VEHICLE_DOOR_LOCK_STATUS(mission_veh[MV_SUBMERISIBLE].veh) != VEHICLELOCK_LOCKED
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("DH_2B_DRIVE_TO_AIRSTRIP")
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_DRIVE_TO_AIRSTRIP")
				START_AUDIO_SCENE("DH_2B_DRIVE_TO_AIRSTRIP")
				SET_LABEL_AS_TRIGGERED("DH_2B_DRIVE_TO_AIRSTRIP",TRUE)
				REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
			ENDIF
		ENDIF
	ENDIF
	
	IF iprogress > 0
		MANAGE_CURRENT_OBJECTIVE()
	ENDIF
	
	VEHICLE_INDEX vehClosest[1]

	SWITCH iprogress

		CASE 0  //setup
			PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
			//
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
				SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
			ELSE
				IF IS_SCREEN_FADED_OUT()
					IF NOT IS_AREA_OCCUPIED( <<-1155.634277,-1520.603516,4.600849>> - <<3.500000,4.500000,1.500000>>, <<-1155.634277,-1520.603516,4.600849>> + <<3.500000,4.500000,1.500000>>,FALSE, TRUE, FALSE, FALSE, FALSE)
//						WHILE NOT SETUP_MISSION_REQUIREMENT(REQ_MICHAELS_CAR, <<-1155.5077, -1520.1217, 3.3467>>, 35.3350)
//							WAIT(0)
//						ENDWHILE
							
					ENDIF
				ENDIF
				
				//DIALOGUE
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],WEAPONTYPE_UNARMED)
					SET_ENTITY_LOD_DIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], 150)
					ADD_PED_FOR_DIALOGUE(sSpeech, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					ADD_PED_FOR_DIALOGUE(sSpeech, 3, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
				ENDIF
				
				SET_SELECTOR_PED_PRIORITY(sSelectorPeds, SELECTOR_PED_MICHAEL, SELECTOR_PED_TREVOR, SELECTOR_PED_FRANKLIN)
				
				GET_PED_NEARBY_VEHICLES(PLAYER_PED_ID(), vehClosest)
				
				IF DOES_ENTITY_EXIST(mission_veh[MV_MICHAELS_CAR].veh)
					vehClosest[0] = mission_veh[MV_MICHAELS_CAR].veh
				ENDIF
				
				IF IS_VEHICLE_DRIVEABLE(vehClosest[0])
				AND DOES_VEHICLE_HAVE_ENOUGH_SEATS(vehClosest[0], sLocatesData, 3)
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					AND IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], vehClosest[0], DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVE_WALK)
						TASK_ENTER_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], vehClosest[0], DEFAULT_TIME_BEFORE_WARP, VS_BACK_RIGHT, PEDMOVE_WALK)
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					SET_PED_MAX_MOVE_BLEND_RATIO(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], PEDMOVE_WALK)
				ENDIF
				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					SET_PED_MAX_MOVE_BLEND_RATIO(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], PEDMOVE_WALK)
				ENDIF
				SET_VEHICLE_GEN_AVAILABLE(VEHGEN_DOCKSP2B_CHINOOK, FALSE)	
				SET_BUILDING_STATE(BUILDINGNAME_IPL_AIRFIELD_PROPS, BUILDINGSTATE_DESTROYED)
				bCreateMissionAssets = FALSE
				bFirstTime = FALSE
				sGoTo = "DS2_GTAIR"
				RELEASE_SCRIPT_AUDIO_BANK()
				iprogress++
			ENDIF

		BREAK
		
		CASE 1
		
			CONTROL_FADE_IN(500)
			brunfailchecks = TRUE
			bGoToSpeach = FALSE
			IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_WHERET",CONV_PRIORITY_HIGH)
				iprogress++
			ENDIF
		BREAK
		
		CASE 2
			
			IF IS_PED_IN_ANY_HELI(PLAYER_PED_ID())
				sGoTo = "DS2_GTAIRFLY"
			ELSE
				sGoTo = "DS2_GTAIR"
			ENDIF
			
			IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_ANY_VEHICLE(sLocatesData, <<1757.8193, 3270.4543, 40.2458>>, <<0.001, 0.001, LOCATE_SIZE_HEIGHT>>, TRUE, sSelectorPeds.pedID[SELECTOR_PED_TREVOR],sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], NULL, sGoTo, "DS2_LEFTTREV","DS2_LEFTFRAN", "", "DS2_WAITCREW","DS2_FINDCAR","DS2_AIRGBV", FALSE,TRUE, 3)

			ELSE
				CHECK_PED_IS_BODHI2_PASSENGER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],bFranklinInVeh)
				CHECK_PED_IS_BODHI2_PASSENGER(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],bTrevorInVeh)
				
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF interior_living_room != NULL
						UNPIN_INTERIOR(interior_living_room)
					ENDIF
					
					IF GET_DISTANCE_BETWEEN_COORDS(<<1757.8193, 3270.4543, 40.2458>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) < 10
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1718.159424,3255.240967,39.383640>>, <<1760.721802,3296.342773,48.133595>>, 39.250000)
						IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()),8)
							IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
								TASK_PAUSE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],3000)
							ENDIF
							REPLAY_RECORD_BACK_FOR_TIME(10.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
							KILL_FACE_TO_FACE_CONVERSATION()
							iprogress++	
						ENDIF
					ENDIF
						IF bGoToSpeach = FALSE
							IF ARE_PEDS_IN_THE_SAME_VEHICLE(PLAYER_PED_ID(),sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							AND ARE_PEDS_IN_THE_SAME_VEHICLE(PLAYER_PED_ID(),sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
								IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_DRI")
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_DRI",CONV_PRIORITY_HIGH)
												SET_LABEL_AS_TRIGGERED("DS2A_DRI",TRUE)
												bGoToSpeach = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_DRIb")
								IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_DRIb",CONV_PRIORITY_HIGH)
									PRINTSTRING("MORE DRIVING")PRINTNL()
									SET_LABEL_AS_TRIGGERED("DS2A_DRIb",TRUE)
								ENDIF
							ELSE
								PRINTSTRING("WAITING TO EVALUATE FLIGHT SKILL")PRINTNL()
								IF NOT HAS_LABEL_BEEN_TRIGGERED("FLYING SKILL")
									IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_DONE_FLYING_LESSONS)
										IF (GET_MEDAL_FOR_PILOT_SCHOOL_LESSON(CHAR_MICHAEL,PSC_Takeoff) = PS_SILVER
										OR GET_MEDAL_FOR_PILOT_SCHOOL_LESSON(CHAR_MICHAEL,PSC_Takeoff) = PS_GOLD)
										AND (GET_MEDAL_FOR_PILOT_SCHOOL_LESSON(CHAR_MICHAEL,PSC_Landing) = PS_SILVER
										OR GET_MEDAL_FOR_PILOT_SCHOOL_LESSON(CHAR_MICHAEL,PSC_Landing) = PS_GOLD)
											PRINTSTRING("GOOD FLYING")PRINTNL()
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_FLYVG",CONV_PRIORITY_HIGH)
														SET_LABEL_AS_TRIGGERED("FLYING SKILL",TRUE)
													ENDIF
												ENDIF
											ENDIF
										ELSE
											PRINTSTRING("OK FLYING")PRINTNL()
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_FLYOK",CONV_PRIORITY_HIGH)
														SET_LABEL_AS_TRIGGERED("FLYING SKILL",TRUE)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										PRINTSTRING("BAD FLYING")PRINTNL()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_FLYSH",CONV_PRIORITY_HIGH)
													SET_LABEL_AS_TRIGGERED("FLYING SKILL",TRUE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
									PRINTSTRING("EVEN MORE DRIVING")PRINTNL()
									IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_1")
										IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_1",CONV_PRIORITY_HIGH)
											SET_LABEL_AS_TRIGGERED("DS2A_1",TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						IF IS_PED_ON_FOOT(PLAYER_PED_ID())
							IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(),TRUE)
								IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1713.684570,3246.190674,39.572960>>, <<1786.573364,3298.621826,44.284359>>, 53.500000)
									KILL_FACE_TO_FACE_CONVERSATION()
									CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
									iprogress++	
								ENDIF
							ENDIF
						ENDIF
				ENDIF

				IF GET_DISTANCE_BETWEEN_COORDS(<<1750.6334, 3285.0278, 40.0871>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) < 100
					IF NOT HAS_LABEL_BEEN_TRIGGERED("ASSIGN BUDDY TASKS")
						IF bFirstTime = TRUE
							IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_PACKER].veh)
								IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_PACKER].veh)
									IF NOT IS_PED_INJURED(pedFloyd)
										TASK_LOOK_AT_ENTITY(pedFloyd,PLAYER_PED_ID(),-1)
										OPEN_SEQUENCE_TASK(seq)
											IF IS_PED_IN_ANY_VEHICLE(pedFloyd)
												TASK_LEAVE_ANY_VEHICLE(NULL,500)
											ENDIF
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<1730.73, 3265.25, 41.16>>,1.0,45000,DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT,-124.36)
											TASK_START_SCENARIO_IN_PLACE(NULL,"WORLD_HUMAN_STAND_IMPATIENT",0,TRUE)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(pedFloyd, seq)
									ENDIF
									IF NOT IS_PED_INJURED(pedWade)
										TASK_LOOK_AT_ENTITY(pedWade,PLAYER_PED_ID(),-1)
										OPEN_SEQUENCE_TASK(seq)
											IF IS_PED_IN_ANY_VEHICLE(pedWade)
												TASK_LEAVE_ANY_VEHICLE(NULL)
											ENDIF
											TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<1729.2040, 3262.7756, 40.1735>>,1.0,45000,DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT,274.1224)
											TASK_START_SCENARIO_IN_PLACE(NULL,"WORLD_HUMAN_GUARD_STAND",0,TRUE)
										CLOSE_SEQUENCE_TASK(seq)
										TASK_PERFORM_SEQUENCE(pedWade, seq)
									ENDIF
									SET_LABEL_AS_TRIGGERED("ASSIGN BUDDY TASKS",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF iprogress = 2		
					IF bGoToSpeach = TRUE
						//MANAGE PAUSE DIALOGUE
						IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
							IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								CLEAR_PRINTS()
								PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
							ENDIF
						//Play conversation
						ELSE
							IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_AIRGB")
									CLEAR_PRINTS()
								ENDIF
						    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF

		BREAK
		
		
		CASE 3
			
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				REMOVE_BLIP(sLocatesData.LocationBlip)
			ENDIF
			KILL_FACE_TO_FACE_CONVERSATION()
			CLEAN_OBJECTIVE_BLIP_DISPLAY()
			iSafeTimer = GET_GAME_TIMER()
			iprogress++
		BREAK
		
		CASE 4
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_ARRIVE")
				IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_ARRIVE",CONV_PRIORITY_HIGH)
				OR MANAGE_MY_TIMER(iSafeTimer,2000)
					REPLAY_RECORD_BACK_FOR_TIME(7.0, 4.0)
					SET_LABEL_AS_TRIGGERED("DS2A_ARRIVE",TRUE)
				ENDIF
			ELSE
				bTriggerDialogue = FALSE
				iprogress = 0
				bReprintObjective = FALSE
				emissionstage = MISSION_STAGE_PICK_UP_THE_SUB_FROM_AIRSTRIP
			ENDIF
		BREAK
		
	ENDSWITCH
	
ENDPROC 

INT ssClimbInSub
INT iGetInTimer
INT iHeightTimer

PROC PICK_UP_THE_SUB_FROM_AIRSTRIP()

	#IF IS_DEBUG_BUILD 
		DONT_DO_J_SKIP(sLocatesData)
	#ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1723.261108,3262.075684,40.152885>>, <<1743.889404,3271.970215,45.189598>>, 7.437500)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_CHINOOK].veh)
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
			IF GET_VEHICLE_DOOR_LOCK_STATUS(mission_veh[MV_CHINOOK].veh) != VEHICLELOCK_UNLOCKED
				SET_VEHICLE_DOORS_LOCKED(mission_veh[MV_CHINOOK].veh,VEHICLELOCK_UNLOCKED)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_SUBMERISIBLE].veh)
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
			IF GET_VEHICLE_DOOR_LOCK_STATUS(mission_veh[MV_SUBMERISIBLE].veh) != VEHICLELOCK_UNLOCKED
				SET_VEHICLE_DOORS_LOCKED(mission_veh[MV_SUBMERISIBLE].veh,VEHICLELOCK_UNLOCKED)
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
		vehGrabbedTemp = GET_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh)
		IF NOT IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
			IF DOES_ENTITY_EXIST(vehGrabbedTemp)
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_REL_WRONG")
					PRINT_HELP("DS2_REL_WRONG")
					SET_LABEL_AS_TRIGGERED("DS2_REL_WRONG",TRUE)
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_REL_WRONG")
					CLEAR_HELP()
				ENDIF
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_REL_WRONG")
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
	

	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
		IF NOT IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh)
				IF DOES_CARGOBOB_HAVE_PICK_UP_ROPE(mission_veh[MV_CHINOOK].veh)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_GRAPPLEH")
						SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(mission_veh[MV_CHINOOK].veh,imaxRopeLength,IminRopeLength)
						CLEAR_HELP()
						PRINT_HELP("DS2_GRAPPLEH")
						SET_LABEL_AS_TRIGGERED("DS2_GRAPPLEH",TRUE)
					ENDIF
				ELSE
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_GRAP_HELP1")
						//SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(mission_veh[MV_CHINOOK].veh,imaxRopeLength,IminRopeLength)
						PRINT_HELP("DS2_GRAP_HELP1")
						SET_LABEL_AS_TRIGGERED("DS2_GRAP_HELP1",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
				IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK)
				ENDIF
			ENDIF
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_GRAPPLEH")
				CLEAR_HELP()
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("DH2B_START")
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh)
				IF TRIGGER_MUSIC_EVENT("DH2B_START")
					SET_LABEL_AS_TRIGGERED("DH2B_START",TRUE)
					REPLAY_RECORD_BACK_FOR_TIME(10.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	IF NOT HAS_LABEL_BEEN_TRIGGERED("BUDDIES LEAVE")
		IF GET_DISTANCE_BETWEEN_COORDS(<<1750.6334, 3285.0278, 40.0871>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) < 100
			IF NOT IS_PED_INJURED(pedFloyd)
				OPEN_SEQUENCE_TASK(seq)
					TASK_PAUSE(NULL,1000)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<1728.44, 3294.27, 41.22>>,1.0,45000,DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT,-112.87)
					TASK_START_SCENARIO_IN_PLACE(NULL,"WORLD_HUMAN_STAND_IMPATIENT",0,TRUE)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedFloyd, seq)
				TASK_LOOK_AT_ENTITY(pedFloyd,PLAYER_PED_ID(),-1)
			ENDIF
			IF NOT IS_PED_INJURED(pedWade)
				OPEN_SEQUENCE_TASK(seq)
					TASK_PAUSE(NULL,500)
					TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<1727.38, 3293.09, 41.20>>,1.0,45000,DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT,-144.39)
					TASK_LOOK_AT_ENTITY(NULL,PLAYER_PED_ID(),-1)
					TASK_START_SCENARIO_IN_PLACE(NULL,"WORLD_HUMAN_GUARD_STAND",0,TRUE)
				CLOSE_SEQUENCE_TASK(seq)
				TASK_PERFORM_SEQUENCE(pedWade, seq)
				TASK_LOOK_AT_ENTITY(pedWade,PLAYER_PED_ID(),-1)
			ENDIF
			SET_LABEL_AS_TRIGGERED("BUDDIES LEAVE",TRUE)
		ENDIF
	ENDIF

	IF NOT HAS_LABEL_BEEN_TRIGGERED("ASSIGN TREVOR GET IN SUB")
		VEHICLE_INDEX vehTemp
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			IF IS_PED_IN_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				vehTemp = GET_VEHICLE_PED_IS_IN(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				IF IS_PED_IN_ANY_HELI(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					IF GET_ENTITY_SPEED(vehTemp) < 0.1
					AND GET_ENTITY_HEIGHT_ABOVE_GROUND(vehTemp) < 0.2
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
							OPEN_SEQUENCE_TASK(seq)
								TASK_LEAVE_ANY_VEHICLE(NULL,500)
								TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL,TRUE)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<1738.64, 3267.09, 41.18>>,1.0,45000,DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT,25.00)
							CLOSE_SEQUENCE_TASK(seq)
							TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
							REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							SET_LABEL_AS_TRIGGERED("ASSIGN TREVOR GET IN SUB",TRUE)
							REQUEST_ANIM_DICT("missdocksheist2bfinale")
						ENDIF
					ENDIF
				ELSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
						OPEN_SEQUENCE_TASK(seq)
							TASK_LEAVE_ANY_VEHICLE(NULL,500)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL,TRUE)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<1738.64, 3267.09, 41.18>>,1.0,45000,DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT,25.00)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
						REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						SET_LABEL_AS_TRIGGERED("ASSIGN TREVOR GET IN SUB",TRUE)
						REQUEST_ANIM_DICT("missdocksheist2bfinale")
					ENDIF
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					OPEN_SEQUENCE_TASK(seq)
						TASK_LEAVE_ANY_VEHICLE(NULL,500)
						TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL,TRUE)
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<<1738.64, 3267.09, 41.18>>,1.0,45000,DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT,25.00)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], seq)
					REMOVE_PED_FROM_GROUP(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					SET_LABEL_AS_TRIGGERED("ASSIGN TREVOR GET IN SUB",TRUE)
					REQUEST_ANIM_DICT("missdocksheist2bfinale")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT HAS_LABEL_BEEN_TRIGGERED("PLAY GET IN ANIM")
			REQUEST_ANIM_DICT("missdocksheist2bfinale")
			IF HAS_ANIM_DICT_LOADED("missdocksheist2bfinale")
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					//IF IS_PED_READY_FOR_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],<<1738.15, 3271.29, 41.18>>,49.2341,<<0.3,0.3,0.3>>)
					IF IS_PED_READY_FOR_ANIM(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],<<1738.64, 3267.09, 41.18>>,25.00,<<0.3,0.3,0.3>>)
					OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<1738.64, 3267.09, 41.18>>, << 1, 1, 1 >>, FALSE, TRUE, TM_IN_VEHICLE)
						ssClimbInSub = CREATE_SYNCHRONIZED_SCENE(<< 1738.643, 3267.095, 41.179 >>, << 0.000, 0.000, 25.000 >>)
						TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], ssClimbInSub, "missdocksheist2bfinale", "get_in_sub", NORMAL_BLEND_IN, INSTANT_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_ABORT_ON_DEATH, RBF_PLAYER_IMPACT, SLOW_BLEND_IN)
						SET_LABEL_AS_TRIGGERED("PLAY GET IN ANIM",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(ssClimbInSub)
					IF GET_SYNCHRONIZED_SCENE_PHASE(ssClimbInSub) > 0.768
					AND GET_SYNCHRONIZED_SCENE_PHASE(ssClimbInSub) < 0.99
						SET_VEHICLE_DOOR_OPEN(mission_veh[MV_SUBMERISIBLE].veh,SC_DOOR_FRONT_LEFT)
					ENDIF
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(ssClimbInSub)
						IF GET_SYNCHRONIZED_SCENE_PHASE(ssClimbInSub) > 0.99
							IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh)
								CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
								SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh)
								SET_VEHICLE_DOOR_SHUT(mission_veh[MV_SUBMERISIBLE].veh,SC_DOOR_FRONT_LEFT)
								REMOVE_ANIM_DICT("missdocksheist2bfinale")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
SWITCH iprogress

	CASE 0  //setup
		
		bReprintObjective = FALSE
		bGoToSpeach = FALSE

		IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
			FREEZE_ENTITY_POSITION(mission_veh[0].veh,FALSE)
		ENDIF
		
		PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
			//SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(mission_veh[MV_CHINOOK].veh,imaxRopeLength,IminRopeLength)
			SET_HELI_TURBULENCE_SCALAR(mission_veh[MV_CHINOOK].veh,0.0)
			SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(mission_veh[MV_CHINOOK].veh,0.0)
		ENDIF

		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
			FORCE_SUBMARINE_SURFACE_MODE(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
			SET_ENTITY_PROOFS(mission_veh[MV_SUBMERISIBLE].veh,FALSE,FALSE,FALSE,TRUE,FALSE)
		ENDIF

		//DIALOGUE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			ADD_PED_FOR_DIALOGUE(sSpeech, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRUE)
			SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],PCF_GetOutUndriveableVehicle,TRUE)
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			ADD_PED_FOR_DIALOGUE(sSpeech, 3, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
		ENDIF
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1,"PICK_UP_THE_SUB_FROM_AIRSTRIP") 

		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,FALSE)
		
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData,FALSE)
		
		IF IS_AUDIO_SCENE_ACTIVE("DH_2B_DRIVE_TO_AIRSTRIP")
			STOP_AUDIO_SCENE("DH_2B_DRIVE_TO_AIRSTRIP")
		ENDIF
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_ATTACH_SUB")
			START_AUDIO_SCENE("DH_2B_ATTACH_SUB")
		ENDIF
		
		SET_VEHICLE_POPULATION_BUDGET(1)
		ALLOW_DIALOGUE_IN_WATER(TRUE)
		KILL_FACE_TO_FACE_CONVERSATION()
		
		CONTROL_FADE_IN(500)
		brunfailchecks = TRUE
		bGoToSpeach = FALSE
		iprogress++
	BREAK
	
	CASE 1
		IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_TGIS")
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_TGIS",CONV_PRIORITY_HIGH)
						SET_LABEL_AS_TRIGGERED("DS2A_TGIS",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_CHOP")
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_CHOP",CONV_PRIORITY_HIGH)
									SET_LABEL_AS_TRIGGERED("DS2A_CHOP",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ELSE	
						iGetInTimer = GET_GAME_TIMER()
						iprogress++
					ENDIF
				ELSE
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_CHOP")
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_CHOP",CONV_PRIORITY_HIGH)
									SET_LABEL_AS_TRIGGERED("DS2A_CHOP",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						iGetInTimer = GET_GAME_TIMER()
						iprogress++
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	BREAK
	
	CASE 2
		
		IF IS_SYNCHRONIZED_SCENE_RUNNING(ssClimbInSub)
			IF GET_SYNCHRONIZED_SCENE_PHASE(ssClimbInSub) < 0.1
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(ssClimbInSub)
		AND NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], mission_veh[MV_SUBMERISIBLE].veh)
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_THROTTLE_UP)
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh)
				IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_GET_TO_DROP_ZONE")
					START_AUDIO_SCENE("DH_2B_GET_TO_DROP_ZONE")
				ENDIF
			ELSE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_GETIN")
					IF MANAGE_MY_TIMER(iGetInTimer, GET_RANDOM_INT_IN_RANGE(20000,24000))
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_GETIN",CONV_PRIORITY_HIGH)
									SET_LABEL_AS_TRIGGERED("DS2A_GETIN",TRUE)
									iGetInTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF MANAGE_MY_TIMER(iGetInTimer, GET_RANDOM_INT_IN_RANGE(20000,24000))
						SET_LABEL_AS_TRIGGERED("DS2A_GETIN",FALSE)	
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
		AND NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			IF DOES_ENTITY_EXIST(objMission[0])
				DELETE_OBJECT(objMission[0])
				SET_MODEL_AS_NO_LONGER_NEEDED(prop_sub_cover_01)
			ENDIF
			
			IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(sLocatesData,GET_ENTITY_COORDS(mission_veh[MV_SUBMERISIBLE].veh),<<0.1,0.1,0.1>>,FALSE,sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[MV_CHINOOK].veh,"DS2_GIPOSFSUB","","DS2_FLYGI","DS2_FLYGB",TRUE)
				
			ELSE
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				AND NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh)
						IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
							SET_BLIP_ALPHA(sLocatesData.LocationBlip,0)
						ENDIF
					ELSE
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
							FREEZE_ENTITY_POSITION(mission_veh[MV_CHINOOK].veh,FALSE)
						ENDIF
						IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
							SET_BLIP_ALPHA(sLocatesData.LocationBlip,255)
						ENDIF
						IF IS_THIS_PRINT_BEING_DISPLAYED("DS2A_WAITTREV")
							CLEAR_PRINTS()
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_SUBBLIP")
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						SET_BLIP_NAME_FROM_TEXT_FILE(sLocatesData.LocationBlip,"DS2_SUBBLIP") 
						SET_LABEL_AS_TRIGGERED("DS2_SUBBLIP",TRUE)
					ENDIF
				ENDIF
				
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF NOT DOES_CARGOBOB_HAVE_PICK_UP_ROPE(mission_veh[MV_CHINOOK].veh)
						PRINTSTRING("DOES_CARGOBOB_HAVE_PICK_UP_ROPE- NO")PRINTNL()
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_GRAP_HELP1")
							PRINT_HELP("DS2_GRAP_HELP1")
							SET_LABEL_AS_TRIGGERED("DS2_GRAP_HELP1",TRUE)
						ENDIF
					ELSE
						PRINTSTRING("DOES_CARGOBOB_HAVE_PICK_UP_ROPE - YES")PRINTNL()
						IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_GRAP_HELP1")
							CLEAR_HELP()
						ENDIF
					ENDIF

					IF GET_BLIP_COLOUR(sLocatesData.LocationBlip) != BLIP_COLOUR_BLUE
						SET_BLIP_COLOUR(sLocatesData.LocationBlip,BLIP_COLOUR_BLUE)
					ENDIF
					
					IF IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh)
						IF DOES_CARGOBOB_HAVE_PICK_UP_ROPE(mission_veh[MV_CHINOOK].veh)
							IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
								CLEAR_PRINTS()
								CLEAR_HELP()
								IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
									FORCE_SUBMARINE_SURFACE_MODE(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
									SET_VEHICLE_ENGINE_ON(mission_veh[MV_SUBMERISIBLE].veh,FALSE,TRUE)
									SET_VEHICLE_LIGHTS(mission_veh[MV_SUBMERISIBLE].veh, SET_VEHICLE_LIGHTS_OFF)
								ENDIF
								STABILISE_ENTITY_ATTACHED_TO_HELI(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh,iStabliseLength)
								IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
									REMOVE_BLIP(sLocatesData.LocationBlip)
								ENDIF
								CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
								
								IF IS_AUDIO_SCENE_ACTIVE("DH_2B_ATTACH_SUB")
									STOP_AUDIO_SCENE("DH_2B_ATTACH_SUB")
								ENDIF
								
								START_AUDIO_SCENE("DH_2B_SUB_PICKED_UP_SCENE") 
								
								bGoToSpeach = FALSE
								iprogress++	
							ELSE
								IF HAS_LABEL_BEEN_TRIGGERED("DS2A_GSUB_DONE")
									IF IS_ENTITY_IN_ANGLED_AREA(mission_veh[MV_CHINOOK].veh, <<1723.022583,3291.410645,39.204441>>, <<1740.887817,3254.185791,69.646393>>, 36.000000)
										IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_LOWER")
											IF GET_ENTITY_HEIGHT_ABOVE_GROUND(mission_veh[MV_CHINOOK].veh) > 56.2049
												IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_LOWER",CONV_PRIORITY_HIGH)
															SET_LABEL_AS_TRIGGERED("DS2A_LOWER",TRUE)
															iHeightTimer = GET_GAME_TIMER()
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ELSE
											IF MANAGE_MY_TIMER(iHeightTimer,GET_RANDOM_INT_IN_RANGE(14000,16000))
												SET_LABEL_AS_TRIGGERED("DS2A_LOWER",FALSE)
											ENDIF
										ENDIF
									ELSE
										IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_FLYOFF")
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_FLYOFF",CONV_PRIORITY_HIGH)
														SET_LABEL_AS_TRIGGERED("DS2A_FLYOFF",TRUE)
														iHeightTimer = GET_GAME_TIMER()
													ENDIF
												ENDIF
											ENDIF
										ELSE
											IF MANAGE_MY_TIMER(iHeightTimer,GET_RANDOM_INT_IN_RANGE(14000,16000))
												SET_LABEL_AS_TRIGGERED("DS2A_FLYOFF",FALSE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								
								PRINTSTRING("WAITING TO EVALUATE FLIGHT SKILL")PRINTNL()
								IF NOT HAS_LABEL_BEEN_TRIGGERED("FLYING SKILL AIRSTRIP")
									IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_DONE_FLYING_LESSONS)
										IF (GET_MEDAL_FOR_PILOT_SCHOOL_LESSON(CHAR_MICHAEL,PSC_Takeoff) = PS_SILVER
										OR GET_MEDAL_FOR_PILOT_SCHOOL_LESSON(CHAR_MICHAEL,PSC_Takeoff) = PS_GOLD)
										AND (GET_MEDAL_FOR_PILOT_SCHOOL_LESSON(CHAR_MICHAEL,PSC_Landing) = PS_SILVER
										OR GET_MEDAL_FOR_PILOT_SCHOOL_LESSON(CHAR_MICHAEL,PSC_Landing) = PS_GOLD)
											PRINTSTRING("GOOD FLYING")PRINTNL()
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_FLYLOT",CONV_PRIORITY_HIGH)
														SET_LABEL_AS_TRIGGERED("FLYING SKILL AIRSTRIP",TRUE)
													ENDIF
												ENDIF
											ENDIF
										ELSE
											PRINTSTRING("OK FLYING")PRINTNL()
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_FEWFLY",CONV_PRIORITY_HIGH)
														SET_LABEL_AS_TRIGGERED("FLYING SKILL AIRSTRIP",TRUE)
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										PRINTSTRING("BAD FLYING")PRINTNL()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_NOFLY",CONV_PRIORITY_HIGH)
													SET_LABEL_AS_TRIGGERED("FLYING SKILL AIRSTRIP",TRUE)
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF IS_IT_SAFE_TO_PLAY_DIALOG()
							IF bGoToSpeach = FALSE
								IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_PSUB",CONV_PRIORITY_HIGH)
									bGoToSpeach = TRUE
									iPauseBetweenDialogueTimer = GET_GAME_TIMER()
								ENDIF
							ELSE
								IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_GSUB")
									IF MANAGE_MY_TIMER(iPauseBetweenDialogueTimer,7000)
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_GSUB",CONV_PRIORITY_HIGH)
												SET_LABEL_AS_TRIGGERED("DS2A_GSUB",TRUE)
												SET_LABEL_AS_TRIGGERED("DS2A_GSUB_DONE",TRUE)
												iTimer = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF MANAGE_MY_TIMER(itimer,16000)
										SET_LABEL_AS_TRIGGERED("DS2A_GSUB",FALSE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
						
				IF bGoToSpeach = TRUE
					//MANAGE PAUSE DIALOGUE
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							CLEAR_PRINTS()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						ENDIF
					//Play conversation
					ELSE
						IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_FLYGB")
								CLEAR_PRINTS()
							ENDIF
					    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF


	BREAK
	
	CASE 3
		IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
		AND IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
			
			iprogress++ 
		ENDIF
	BREAK
	
	CASE 4
		IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
			REMOVE_BLIP(sLocatesData.LocationBlip)
		ENDIF
		CLEAR_HELP()
		CLEAR_PRINTS()
		IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
		AND IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
			IF TRIGGER_MUSIC_EVENT("DH2B_GOT_SUB")
				PRINTSTRING("PICKING UP AND UNFREEZING VEHICLES")PRINTNL()
				iprogress++	
			ENDIF
		ENDIF
	BREAK
	
	CASE 5
		IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_GRABBED")
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_GRABBED",CONV_PRIORITY_HIGH)
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					SET_LABEL_AS_TRIGGERED("DS2A_GRABBED",TRUE)
				ENDIF
			ENDIF
		ELSE
			//IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
			MANAGE_FREE_HOTSWAP(FALSE, FALSE, TRUE, TRUE)
			//ENDIF
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			AND iSwitchStage = 0
				SET_VEHICLE_NOISE_BASED_ON_FLIGHT_STAT(mission_veh[MV_CHINOOK].veh,GET_SP_PLAYER_PED_STAT_VALUE(CHAR_MICHAEL, PS_FLYING_ABILITY))
				
				CLEAR_HELP()
				CLEAR_PRINTS()
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				bTriggerDialogue = FALSE
				iprogress = 0
				bReprintObjective = FALSE
				emissionstage = MISSION_STAGE_FLY_TO_SEA
			ENDIF
		ENDIF
	BREAK
	
ENDSWITCH
	

ENDPROC 

PROC FLY_TO_SEA()
	
	MANAGE_BUMPS_AND_BANG_DIALOGUE()
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
		IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK)
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
			IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh)
				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("TURN OFF PROOFS")
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_ARMY_TRAILER].veh)
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mission_veh[MV_ARMY_TRAILER].veh),GET_ENTITY_COORDS(mission_veh[MV_SUBMERISIBLE].veh)) > 30
					FORCE_SUBMARINE_SURFACE_MODE(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
					SET_ENTITY_PROOFS(mission_veh[MV_SUBMERISIBLE].veh,FALSE,FALSE,FALSE,FALSE,FALSE)
					SET_LABEL_AS_TRIGGERED("TURN OFF PROOFS",TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("DH_2B_SUB_PICKED_UP_SCENE")
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_ARMY_TRAILER].veh)
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mission_veh[MV_ARMY_TRAILER].veh),GET_ENTITY_COORDS(mission_veh[MV_SUBMERISIBLE].veh)) > 10
					STOP_AUDIO_SCENE("DH_2B_SUB_PICKED_UP_SCENE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh)
		//STREAMING NEW CONTENT
		IF GET_DISTANCE_BETWEEN_COORDS(<<-1388.26, 6160.88, 0.34>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) < 2500
			SETUP_MISSION_REQUIREMENT(REQ_MERRYWEATHER_RIG,<<0,0,0>>)
		ENDIF
		
		//STREAMING OUT OLD CONTENT
		IF GET_DISTANCE_BETWEEN_COORDS(<<1750.6334, 3285.0278, 40.0871>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) > 400
			
			IF DOES_ENTITY_EXIST(pedFloyd)
				IF NOT IS_ENTITY_ON_SCREEN(pedFloyd)
					DELETE_PED(pedFloyd)
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_FLOYD)
				ENDIF
			ELSE
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_FLOYD)
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedWade)
				IF NOT IS_ENTITY_ON_SCREEN(pedWade)
					DELETE_PED(pedWade)
					SET_MODEL_AS_NO_LONGER_NEEDED(IG_WADE)
				ENDIF
			ELSE
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_WADE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(mission_veh[MV_PACKER].veh)
				IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_PACKER].veh)
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(mission_veh[ENUM_TO_INT(MV_PACKER)].veh)
					DELETE_VEHICLE(mission_veh[MV_PACKER].veh)
					SET_MODEL_AS_NO_LONGER_NEEDED(PACKER)
				ENDIF
			ELSE
				SET_MODEL_AS_NO_LONGER_NEEDED(PACKER)
			ENDIF
			
			IF DOES_ENTITY_EXIST(mission_veh[MV_ARMY_TRAILER].veh)
				IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_ARMY_TRAILER].veh)
					DELETE_VEHICLE(mission_veh[MV_ARMY_TRAILER].veh)
					SET_MODEL_AS_NO_LONGER_NEEDED(ARMYTRAILER)
				ENDIF
			ELSE
				SET_MODEL_AS_NO_LONGER_NEEDED(ARMYTRAILER)
			ENDIF
			
			IF DOES_ENTITY_EXIST(mission_veh[MV_MICHAELS_CAR].veh)
				IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_MICHAELS_CAR].veh)
					DELETE_VEHICLE(mission_veh[MV_MICHAELS_CAR].veh)
					SET_MODEL_AS_NO_LONGER_NEEDED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL))
				ENDIF
			ENDIF
		ENDIF
	ENDIF	
	
	IF iprogress > 0
		//MANAGE_FREE_HOTSWAP()
		MANAGE_CURRENT_OBJECTIVE()
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("SEE THE SHIPS")
			IF GET_DISTANCE_BETWEEN_COORDS(<<-164.3709, 4176.9414, 30.3069>>,GET_ENTITY_COORDS(PLAYER_PED_ID()),FALSE) < 1000
				IF NOT WOULD_ENTITY_BE_OCCLUDED(SUBMERSIBLE,<<-164.3709, 4176.9414, 30.3069>>)
					IF SETUP_MISSION_REQUIREMENT(REQ_MERRYWEATHER_DINGHY_SETPIECE,<<0,0,0>>)
						SET_LABEL_AS_TRIGGERED("SEE THE SHIPS",TRUE)
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						i_boat_timer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_SBOATS")
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh),GET_ENTITY_COORDS(PLAYER_PED_ID()),FALSE) < 300
						IF IS_ENTITY_ON_SCREEN(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh)
							IF NOT WOULD_ENTITY_BE_OCCLUDED(SUBMERSIBLE,GET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh),FALSE)
								IF MANAGE_MY_TIMER(i_boat_timer,4000)
									IF TRIGGER_MUSIC_EVENT("DH2B_BOATS")
										IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_SEE_BOATS")
											START_AUDIO_SCENE("DH_2B_SEE_BOATS")
										ENDIF
										IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_SBOATS",CONV_PRIORITY_HIGH)
											SET_LABEL_AS_TRIGGERED("DS2A_SBOATS",TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh)
					IF GET_DISTANCE_BETWEEN_COORDS(<<-164.3709, 4176.9414, 30.3069>>,GET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh),FALSE) < 300
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh)
							SET_PLAYBACK_TO_USE_AI(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh,DRIVINGMODE_AVOIDCARS)
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_2].veh)
							SET_PLAYBACK_TO_USE_AI(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_2].veh,DRIVINGMODE_AVOIDCARS)
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_3].veh)
							SET_PLAYBACK_TO_USE_AI(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_3].veh,DRIVINGMODE_AVOIDCARS)
						ENDIF
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh),GET_ENTITY_COORDS(PLAYER_PED_ID()),FALSE) > 700
						IF IS_AUDIO_SCENE_ACTIVE("DH_2B_SEE_BOATS")
							STOP_AUDIO_SCENE("DH_2B_SEE_BOATS")
						ENDIF
					ENDIF
				ELSE
					IF IS_AUDIO_SCENE_ACTIVE("DH_2B_SEE_BOATS")
						STOP_AUDIO_SCENE("DH_2B_SEE_BOATS")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT HAS_LABEL_BEEN_TRIGGERED("SEE THE PLANE")
			IF GET_DISTANCE_BETWEEN_COORDS(<<-999.23163, 6248.24023, 1.13868>>,GET_ENTITY_COORDS(PLAYER_PED_ID()),FALSE) < 1000
				IF NOT WOULD_ENTITY_BE_OCCLUDED(SUBMERSIBLE,<<-1131.8038, 5913.6406, 25.2705>>)
					IF SETUP_MISSION_REQUIREMENT(REQ_MERRYWEATHER_PLANE_SETPIECE,<<0,0,0>>)
						SET_LABEL_AS_TRIGGERED("SEE THE PLANE",TRUE)
						i_plane_timer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_SPLANE")
				IF GET_DISTANCE_BETWEEN_COORDS(<<-999.23163, 6248.24023, 1.13868>>,GET_ENTITY_COORDS(PLAYER_PED_ID()),FALSE) < 1500
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh)
						IF NOT WOULD_ENTITY_BE_OCCLUDED(SUBMERSIBLE,GET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh))
							IF IS_ENTITY_ON_SCREEN(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh)
								IF MANAGE_MY_TIMER(i_plane_timer,12000)
									IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_SEE_PLANE")
										START_AUDIO_SCENE("DH_2B_SEE_PLANE")
									ENDIF
									IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_SPLANE",CONV_PRIORITY_HIGH)
										SET_LABEL_AS_TRIGGERED("DS2A_SPLANE",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

SWITCH iprogress

	CASE 0  //setup
		
		PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
		
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
			SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
		ELSE
			//DIALOGUE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRUE)
				SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],PCF_GetOutUndriveableVehicle,TRUE)
				ADD_PED_FOR_DIALOGUE(sSpeech, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				ADD_PED_FOR_DIALOGUE(sSpeech, 3, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				FORCE_SUBMARINE_SURFACE_MODE(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
			ENDIF
			
			SET_SELECTOR_PED_PRIORITY(sSelectorPeds, SELECTOR_PED_MICHAEL, SELECTOR_PED_TREVOR, SELECTOR_PED_FRANKLIN)
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2,"FLY_TO_SEA") 
			bGoToSpeach = FALSE
			REMOVE_VEHICLE_RECORDING(33,"DHF2")
			iprogress++
		ENDIF

	BREAK
	
	CASE 1

		CONTROL_FADE_IN(500)
		
		brunfailchecks = TRUE
		bGoToSpeach = FALSE
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
			FREEZE_ENTITY_POSITION(mission_veh[0].veh,FALSE)
		ENDIF
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_GET_TO_DROP_ZONE")
			START_AUDIO_SCENE("DH_2B_GET_TO_DROP_ZONE")
		ENDIF
		
		iprogress++
	BREAK
	
	CASE 2
		IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,<<-1388.26, 6160.88, 0.34>>,<<600,600,600>>,FALSE,mission_veh[0].veh,"DS2_FLY","DS2_FLYGI","DS2_FLYGB",TRUE)
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
			AND IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				IF DOES_CARGOBOB_HAVE_PICK_UP_ROPE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
						iprogress++
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				IF DOES_CARGOBOB_HAVE_PICK_UP_ROPE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
						IF HAS_LABEL_BEEN_TRIGGERED("DROPPED THE SUB")
							IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
									CLEAR_PRINTS()
									PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
								ENDIF
							ENDIF
							IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								SET_BLIP_ALPHA(sLocatesData.LocationBlip,255)
							ENDIF
							IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_GIPOSFSUB")
								CLEAR_PRINTS()
							ENDIF
							IF DOES_BLIP_EXIST(blip_pick_up_sub)
								REMOVE_BLIP(blip_pick_up_sub)
							ENDIF
							STABILISE_ENTITY_ATTACHED_TO_HELI(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh,iStabliseLength)
				
							SET_LABEL_AS_TRIGGERED("DROPPED THE SUB",FALSE)
						ENDIF
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DROPPED THE SUB")
							IF bGoToSpeach = TRUE
								//MANAGE PAUSE DIALOGUE
								IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
									IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
										CLEAR_PRINTS()
										PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
									ENDIF
								ENDIF
							ENDIF
							
							IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								SET_BLIP_ALPHA(sLocatesData.LocationBlip,0)
							ENDIF
							
							IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
								STABILISE_ENTITY_ATTACHED_TO_HELI(mission_veh[MV_CHINOOK].veh,NULL,iStabliseLength)
							ENDIF
							
							IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
								IF NOT DOES_BLIP_EXIST(blip_pick_up_sub)
									blip_pick_up_sub = CREATE_BLIP_FOR_VEHICLE(mission_veh[MV_SUBMERISIBLE].veh)
									PRINT_GOD_TEXT("DS2_GIPOSFSUB")
								ENDIF
							ENDIF
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							SET_LABEL_AS_TRIGGERED("DROPPED THE SUB",TRUE)
						ENDIF
					ENDIF
				ENDIF
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF IS_IT_SAFE_TO_PLAY_DIALOG()
						IF bGoToSpeach = FALSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P1")
								IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P1",CONV_PRIORITY_HIGH)
									SET_LABEL_AS_TRIGGERED("DS2A_P1",TRUE)
									bGoToSpeach = TRUE
								ENDIF
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P1a")
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<-1388.26, 6160.88, 0.34>>,<<850,850,850>>)
									IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
										IF NOT IS_ENTITY_OCCLUDED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
											IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P1a",CONV_PRIORITY_HIGH)
												SET_LABEL_AS_TRIGGERED("DS2A_P1a",TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
					
			IF bGoToSpeach = TRUE
				//MANAGE PAUSE DIALOGUE
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
					AND NOT HAS_LABEL_BEEN_TRIGGERED("DROPPED THE SUB")
						CLEAR_PRINTS()
						PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
					ENDIF
				//Play conversation
				ELSE
					IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_FLYGB")
							CLEAR_PRINTS()
						ENDIF
				    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	BREAK
	
	
	CASE 3
		IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
			REMOVE_BLIP(sLocatesData.LocationBlip)
		ENDIF
		CLEAN_OBJECTIVE_BLIP_DISPLAY()
		iprogress++
	BREAK
	
	CASE 4
		bTriggerDialogue = FALSE
		iprogress = 0
		bReprintObjective = FALSE
		emissionstage = MISSION_STAGE_DROP_THE_SUB
	BREAK
	
ENDSWITCH

ENDPROC

//PURPOSE: triggers multiple bullet hits around the player at given intervals.
//AUTHOR: Ross Wallace
PROC DO_EXCITING_NEAR_BULLET_MISS_ON_PED(PED_INDEX pedToMiss, PED_INDEX sourceOfBullets, INT &iControlTimer, VECTOR sourceOffset, 
                                                             INT timeBetweenBullets = 60, FLOAT minXrange = -3.9, FLOAT maxXrange = -1.0, FLOAT minYRange = -2.9, FLOAT maxYrange = 3.9)
    //Fire bullets at the player as from the bad  guy...
    INT currentBulletTime = GET_GAME_TIMER()
    VECTOR bulletHit
    VECTOR bulletOrigin
    
    IF ((currentBulletTime - iControlTimer) > timeBetweenBullets)
        IF NOT IS_ENTITY_DEAD(pedToMiss)
        AND NOT IS_ENTITY_DEAD(sourceOfBullets)            
            bulletHit = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedToMiss, <<GET_RANDOM_FLOAT_IN_RANGE(minXrange, maxXrange), GET_RANDOM_FLOAT_IN_RANGE(minYRange, maxYrange), 0.0>>)
            bulletOrigin = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sourceOfBullets, sourceOffset)
            GET_GROUND_Z_FOR_3D_COORD(bulletHit, bulletHit.z)           
            SHOOT_SINGLE_BULLET_BETWEEN_COORDS(bulletOrigin, bulletHit, 1)
            iControlTimer = currentBulletTime
      	ENDIF
     ENDIF
ENDPROC

FUNC BOOL FIRE_ROCKET(VECTOR v_start, VEHICLE_INDEX veh_target, ENTITY_INDEX entity_owner, VECTOR v_offset, BOOL b_enemy_rocket = FALSE, BOOL b_track_target_speed = FALSE, FLOAT f_rocket_speed_multiplier = 1.0)
	INT i = 0
	INT i_rocket_index = -1
	
	REPEAT COUNT_OF(s_rockets) i
		IF NOT DOES_ENTITY_EXIST(s_rockets[i].obj)
			i_rocket_index = i
		ENDIF
	ENDREPEAT
	
	REQUEST_MODEL(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG))
	REQUEST_PTFX_ASSET()
	
	IF HAS_MODEL_LOADED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG))
	AND HAS_PTFX_ASSET_LOADED()
	AND i_rocket_index > -1
		s_rockets[i_rocket_index].obj = CREATE_OBJECT_NO_OFFSET(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG), v_start)
		s_rockets[i_rocket_index].entity_owner = entity_owner
		s_rockets[i_rocket_index].v_start = v_start
		s_rockets[i_rocket_index].veh_target = veh_target
		s_rockets[i_rocket_index].v_offset = v_offset
		s_rockets[i_rocket_index].v_prev_pos = <<0.0, 0.0, 0.0>>
		s_rockets[i_rocket_index].b_reached_target = FALSE
		s_rockets[i_rocket_index].b_add_entity_speed = b_track_target_speed
		s_rockets[i_rocket_index].b_rocket_belongs_to_player = NOT b_enemy_rocket
		s_rockets[i_rocket_index].f_speed_multiplier = f_rocket_speed_multiplier
		s_rockets[i_rocket_index].ptfx = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_bigscore_rpg_trail", s_rockets[i_rocket_index].obj, <<0.0, 0.0, 0.0>>,  <<0.0, 0.0, 0.0>>)
		
		SET_ENTITY_LOD_DIST(s_rockets[i_rocket_index].obj, 500)
		SET_ENTITY_RECORDS_COLLISIONS(s_rockets[i_rocket_index].obj, TRUE)
		SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG))
		
		IF NOT b_enemy_rocket
			i_rocket_timer = 0 //When the timer = 0 no more rockets are allowed for the player.
		ENDIF
		
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC UPDATE_ROCKETS()
	CONST_FLOAT ROCKET_SPEED				60.0
	CONST_FLOAT MAX_ROCKET_SPEED			79.0

	INT i = 0
	REPEAT COUNT_OF(s_rockets) i
		IF DOES_ENTITY_EXIST(s_rockets[i].obj)
			BOOL b_force_blow_up = FALSE
			VECTOR v_current_pos = GET_ENTITY_COORDS(s_rockets[i].obj)
		
			//Update direction to home into offset, this lets us control how close the missile is to the target.
			IF NOT s_rockets[i].b_reached_target
			AND NOT IS_ENTITY_DEAD(s_rockets[i].veh_target)
				VECTOR v_dest = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_rockets[i].veh_target, s_rockets[i].v_offset)
				s_rockets[i].v_dir = (v_dest - v_current_pos) / VMAG(v_dest - v_current_pos)
				
				//Detect when rockets hit (or when they've missed and are flying away).
				IF s_rockets[i].v_prev_pos.x != 0.0
					FLOAT f_dist_from_target = VDIST2(v_current_pos, v_dest)
					//FLOAT f_prev_dist_from_target = VDIST2(s_rockets[i].v_prev_pos, v_dest)
					
					IF s_rockets[i].v_offset.x = 0.0 AND s_rockets[i].v_offset.y = 0.0 s_rockets[i].v_offset.z = 0.0
						//The rocket is meant to be on target, once it gets close blow it up
						IF f_dist_from_target < 25.0
							b_force_blow_up = TRUE
							s_rockets[i].b_reached_target = TRUE
						ENDIF
					ELSE
						IF f_dist_from_target < 100.0
							//IF f_prev_dist_from_target < f_dist_from_target //The rocket is moving away from the target.
								s_rockets[i].b_reached_target = TRUE
							
								//If the missed rocket was one the player fired then play some near-miss dialogue.
								IF s_rockets[i].b_rocket_belongs_to_player
									//i_time_since_last_rocket_missed = GET_GAME_TIMER()
								ENDIF
							//ENDIF
						ENDIF
					ENDIF
					
					IF s_rockets[i].b_reached_target
						s_rockets[i].i_explode_timer = GET_GAME_TIMER()
						
						//Start the timer for the player's next rocket.
						IF i_rocket_timer = 0
						AND s_rockets[i].b_rocket_belongs_to_player
							//b_played_line_up_dialogue = FALSE
							i_rocket_timer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
				
				s_rockets[i].v_vel = s_rockets[i].v_dir * ROCKET_SPEED * s_rockets[i].f_speed_multiplier
				
				IF s_rockets[i].b_add_entity_speed
					s_rockets[i].v_vel = s_rockets[i].v_vel + GET_ENTITY_VELOCITY(s_rockets[i].veh_target)
				ENDIF
				
				//Fix for B*550927: check the velocity to make sure it cannot go over the max amount.
				IF VMAG(s_rockets[i].v_vel) > MAX_ROCKET_SPEED
					s_rockets[i].v_vel = (s_rockets[i].v_vel / VMAG(s_rockets[i].v_vel)) * MAX_ROCKET_SPEED
				ENDIF
			ENDIF
		
			//Update rotation to match the direction of movement.
			s_rockets[i].v_rot  = <<0.0, 0.0, 0.0>>
			s_rockets[i].v_rot.x = ATAN2(s_rockets[i].v_dir.z, VMAG(<<s_rockets[i].v_dir.x, s_rockets[i].v_dir.y, 0.0>>))
			s_rockets[i].v_rot.z = ATAN2(s_rockets[i].v_dir.y, s_rockets[i].v_dir.x) - 90.0
		
			//SET_ENTITY_COORDS(s_rockets[i].obj, v_current_pos +@ (s_rockets[i].v_dir * ROCKET_SPEED))
			SET_ENTITY_VELOCITY(s_rockets[i].obj, s_rockets[i].v_vel)
			SET_ENTITY_ROTATION(s_rockets[i].obj, s_rockets[i].v_rot)
					
			#IF IS_DEBUG_BUILD
				IF b_debug_draw_debug
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
					DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(s_rockets[i].obj), 0.3)
				ENDIF
			#ENDIF
			
			//Explode on contact or if scripted to hit
			IF (s_rockets[i].b_reached_target AND GET_GAME_TIMER() - s_rockets[i].i_explode_timer > 3000)
			OR HAS_ENTITY_COLLIDED_WITH_ANYTHING(s_rockets[i].obj) AND NOT IS_ENTITY_TOUCHING_ENTITY(s_rockets[i].obj, s_rockets[i].entity_owner)
			OR b_force_blow_up
				IF NOT IS_ENTITY_DEAD(s_rockets[i].veh_target)
					IF IS_ENTITY_TOUCHING_ENTITY(s_rockets[i].obj, s_rockets[i].veh_target)
					OR b_force_blow_up
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(s_rockets[i].veh_target)
							STOP_PLAYBACK_RECORDED_VEHICLE(s_rockets[i].veh_target)
						ENDIF
						
						EXPLODE_VEHICLE(s_rockets[i].veh_target)
						SET_VEHICLE_PETROL_TANK_HEALTH(s_rockets[i].veh_target, -100.0)
						//i_time_since_last_chopper_exploded = GET_GAME_TIMER()
					ENDIF
				ENDIF
			
				ADD_EXPLOSION(v_current_pos, EXP_TAG_ROCKET, 1.0)

				IF s_rockets[i].ptfx != NULL
					STOP_PARTICLE_FX_LOOPED(s_rockets[i].ptfx)
					s_rockets[i].ptfx = NULL
				ENDIF

				REMOVE_OBJECT(s_rockets[i].obj, TRUE)
			ENDIF	
			
			s_rockets[i].v_prev_pos = v_current_pos
		ENDIF
	ENDREPEAT
ENDPROC

FUNC VEHICLE_INDEX GET_CLOSEST_ENEMY_VEHICLE_FROM_PED(PED_INDEX ped)
	VEHICLE_INDEX vehClosestCars[10]
	INT iClosest = 0
	INT i = 0
	FLOAT fClosestDist = 0.0
	
	IF NOT IS_PED_INJURED(ped)
		VECTOR vPedPos = GET_ENTITY_COORDS(ped)
	
		GET_PED_NEARBY_VEHICLES(ped, vehClosestCars)
		
		REPEAT COUNT_OF(vehClosestCars) i
			IF IS_VEHICLE_DRIVEABLE(vehClosestCars[i])
				IF GET_ENTITY_MODEL(vehClosestCars[i]) = BUZZARD
				OR GET_ENTITY_MODEL(vehClosestCars[i]) = DINGHY
					FLOAT fDist = VDIST2(GET_ENTITY_COORDS(vehClosestCars[i]), vPedPos)
					
					IF fDist < fClosestDist OR fClosestDist = 0.0
						fClosestDist = fDist
						iClosest = i
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF fClosestDist = 0.0
		vehClosestCars[iClosest] = NULL
	ENDIF
	
	RETURN vehClosestCars[iClosest]
ENDFUNC

FUNC ROCKET_LINE_UP_STATE GET_ROCKET_LINE_UP_STATE(VEHICLE_INDEX veh_target, VEHICLE_INDEX veh_player)
	VECTOR v_offset
	FLOAT f_vertical_angle, f_horizontal_angle, f_dist
	ROCKET_LINE_UP_STATE e_rocket_state = LINE_UP_DONT_FIRE_TOO_FAR_AWAY
	
	IF NOT IS_ENTITY_DEAD(veh_target)
	AND NOT IS_ENTITY_DEAD(veh_player)
		//Rules:
		// - If the player is too far away, don't fire at all.
		// - If the player is on the wrong side, don't fire at all.
		// - If the the player is on the right side but the angle is too tight, then fire but miss.
		f_dist = VDIST2(GET_ENTITY_COORDS(veh_target), GET_ENTITY_COORDS(veh_player))
		PRINTSTRING("distancefromtarget:")PRINTFLOAT(f_dist)PRINTNL()
		
		//IF f_dist < 10000.0
		IF f_dist < 18000.0 //17000.0
			IF f_dist > 400.0
				v_offset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(veh_player, GET_ENTITY_COORDS(veh_target))
				f_vertical_angle = ABSF(ATAN2(ABSF(v_offset.x), ABSF(v_offset.z)))
				f_horizontal_angle = ABSF(ATAN2(ABSF(v_offset.x), ABSF(v_offset.y)))
				
				IF f_vertical_angle > 20.0 AND f_horizontal_angle > 5.0
					IF v_offset.x < 0.0
						e_rocket_state = LINE_UP_FIRE_LEFT_SIDE
					ELSE
						e_rocket_state = LINE_UP_FIRE_RIGHT_SIDE
					ENDIF
				ELSE
					e_rocket_state = LINE_UP_DONT_FIRE_BAD_ANGLE
					/*IF v_offset.x < 0.0
						e_rocket_state = LINE_UP_FIRE_MISS_LEFT_SIDE
					ELSE
						e_rocket_state = LINE_UP_FIRE_MISS_RIGHT_SIDE
					ENDIF*/
				ENDIF
			ELSE
				e_rocket_state = LINE_UP_DONT_FIRE_TOO_CLOSE
			ENDIF
		ELSE
			e_rocket_state = LINE_UP_DONT_FIRE_TOO_FAR_AWAY
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
		//PRINTLN(f_dist, " ", v_offset, " ", f_vertical_angle, " ", f_horizontal_angle)
	#ENDIF
	
	RETURN e_rocket_state
ENDFUNC

//Updates Lester's rocket anims during the chase: Lester has a number of different states he can be in, these states link together different anims.
PROC UPDATE_FRANKLIN_ROCKET_ANIMS()
	REQUEST_ANIM_DICT(str_anim_rockets)
	REQUEST_ANIM_DICT(str_anim_rocket_signal)
	REQUEST_ANIM_DICT(str_anim_rocket_reload)

	IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
	AND NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
	AND HAS_ANIM_DICT_LOADED(str_anim_rockets)
	AND HAS_ANIM_DICT_LOADED(str_anim_rocket_signal)
	AND HAS_ANIM_DICT_LOADED(str_anim_rocket_reload)
		IF NOT DOES_ENTITY_EXIST(obj_lesters_rpg)
			REQUEST_MODEL(GET_WEAPONTYPE_MODEL(WEAPONTYPE_RPG))
			REQUEST_MODEL(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG))
			
			IF HAS_MODEL_LOADED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_RPG))
			AND HAS_MODEL_LOADED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG))
				obj_lesters_rpg = CREATE_OBJECT(GET_WEAPONTYPE_MODEL(WEAPONTYPE_RPG), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]))
				ATTACH_ENTITY_TO_ENTITY(obj_lesters_rpg, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], GET_PED_BONE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], BONETAG_PH_L_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				SET_ENTITY_VISIBLE(obj_lesters_rpg, FALSE)
				
				obj_single_rocket = CREATE_OBJECT(GET_WEAPONTYPE_MODEL(WEAPONTYPE_RPG), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]))
				ATTACH_ENTITY_TO_ENTITY(obj_single_rocket, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], GET_PED_BONE_INDEX(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				SET_ENTITY_VISIBLE(obj_single_rocket, FALSE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_RPG))
				SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPONTYPE_MODEL(WEAPONTYPE_AMMO_RPG))
			ENDIF
		ENDIF
		
		SWITCH e_rocket_anim_state
			CASE ROCKET_ANIM_GET_LAUNCHER_OUT_THEN_AIM
				IF DOES_ENTITY_EXIST(obj_lesters_rpg)
					IF NOT IS_ENTITY_VISIBLE(obj_lesters_rpg)
						SET_ENTITY_VISIBLE(obj_lesters_rpg, TRUE)
					ENDIF
				ENDIF
			
				IF i_lester_rocket_anim_event = 0
					IF IS_ENTITY_ATTACHED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						DETACH_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					ENDIF
					
					i_lester_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_lester_rocket_sync_scene, mission_veh[0].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[0].veh, "seat_dside_r"))
					TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], i_lester_rocket_sync_scene, str_anim_rockets, "idle_to_leansit", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, 
											SYNCED_SCENE_DONT_INTERRUPT, 
											RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
					SET_SYNCHRONIZED_SCENE_LOOPED(i_lester_rocket_sync_scene, FALSE)
					
					i_lester_rocket_anim_event++
				ELIF i_lester_rocket_anim_event = 1 
					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_lester_rocket_sync_scene)
					AND GET_SYNCHRONIZED_SCENE_PHASE(i_lester_rocket_sync_scene) > 0.99
						i_lester_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_lester_rocket_sync_scene, mission_veh[0].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[0].veh, "seat_dside_r"))
						TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], i_lester_rocket_sync_scene, str_anim_rockets, "intro_90r", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
												SYNCED_SCENE_DONT_INTERRUPT, 
												RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
						SET_SYNCHRONIZED_SCENE_LOOPED(i_lester_rocket_sync_scene, FALSE)
						
						i_lester_rocket_anim_event++
					ENDIF
				ELIF i_lester_rocket_anim_event = 2
					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_lester_rocket_sync_scene)
					AND GET_SYNCHRONIZED_SCENE_PHASE(i_lester_rocket_sync_scene) > 0.99
						i_lester_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_lester_rocket_sync_scene, mission_veh[0].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[0].veh, "seat_dside_r"))
						TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], i_lester_rocket_sync_scene, str_anim_rockets, "sweep_medium", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
												SYNCED_SCENE_DONT_INTERRUPT, 
												RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
						SET_SYNCHRONIZED_SCENE_LOOPED(i_lester_rocket_sync_scene, TRUE)
						SET_SYNCHRONIZED_SCENE_PHASE(i_lester_rocket_sync_scene, 0.98)
						SET_SYNCHRONIZED_SCENE_RATE(i_lester_rocket_sync_scene, 0.0)
						
						e_rocket_anim_state = ROCKET_ANIM_AIMING
					ENDIF
				ENDIF
			BREAK
			
			CASE ROCKET_ANIM_PUT_LAUNCHER_AWAY
				IF i_lester_rocket_anim_event = 0
					IF IS_ENTITY_ATTACHED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						DETACH_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					ENDIF
					
					i_lester_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_lester_rocket_sync_scene, mission_veh[0].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[0].veh, "seat_dside_r"))
					TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], i_lester_rocket_sync_scene, str_anim_rockets, "outro_90r", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
											SYNCED_SCENE_DONT_INTERRUPT, 
											RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
					SET_SYNCHRONIZED_SCENE_LOOPED(i_lester_rocket_sync_scene, FALSE)
					
					i_lester_rocket_anim_event++
				ELIF i_lester_rocket_anim_event = 1 
					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_lester_rocket_sync_scene)
					AND GET_SYNCHRONIZED_SCENE_PHASE(i_lester_rocket_sync_scene) > 0.99
						i_lester_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_lester_rocket_sync_scene, mission_veh[0].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[0].veh, "seat_dside_r"))
						TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], i_lester_rocket_sync_scene, str_anim_rockets, "leansit_to_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
												SYNCED_SCENE_DONT_INTERRUPT, 
												RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
						SET_SYNCHRONIZED_SCENE_LOOPED(i_lester_rocket_sync_scene, FALSE)
						
						i_lester_rocket_anim_event++
					ENDIF
				ELIF i_lester_rocket_anim_event = 2
					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_lester_rocket_sync_scene)
					AND GET_SYNCHRONIZED_SCENE_PHASE(i_lester_rocket_sync_scene) > 0.99
						IF DOES_ENTITY_EXIST(obj_lesters_rpg)
							SET_ENTITY_VISIBLE(obj_lesters_rpg, FALSE)
						ENDIF
					
						i_lester_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_lester_rocket_sync_scene, mission_veh[0].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[0].veh, "seat_dside_r"))
						TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], i_lester_rocket_sync_scene, str_anim_rockets, "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
												SYNCED_SCENE_DONT_INTERRUPT, 
												RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
						SET_SYNCHRONIZED_SCENE_LOOPED(i_lester_rocket_sync_scene, TRUE)
						
						i_lester_rocket_anim_event++
					ENDIF
				ELIF i_lester_rocket_anim_event = 3
					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_lester_rocket_sync_scene)
					AND GET_SYNCHRONIZED_SCENE_PHASE(i_lester_rocket_sync_scene) > 0.99
						STOP_SYNCHRONIZED_ENTITY_ANIM(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], NORMAL_BLEND_OUT, TRUE)
						CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						
						IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
							SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], mission_veh[0].veh, VS_BACK_LEFT)
						ENDIF
						
						i_lester_rocket_anim_event++
					ENDIF
				ENDIF
			BREAK
			
			CASE ROCKET_ANIM_GET_CLOSER_THEN_AIM
				IF i_lester_rocket_anim_event = 0
					IF IS_ENTITY_ATTACHED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						DETACH_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					ENDIF
					
					i_lester_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_lester_rocket_sync_scene, mission_veh[0].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[0].veh, "seat_dside_r"))
					TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], i_lester_rocket_sync_scene, str_anim_rockets, "outro_90r", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
											SYNCED_SCENE_DONT_INTERRUPT, 
											RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
					SET_SYNCHRONIZED_SCENE_LOOPED(i_lester_rocket_sync_scene, FALSE)
					
					i_lester_rocket_anim_event++
				ELIF i_lester_rocket_anim_event = 1 
					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_lester_rocket_sync_scene)
					AND GET_SYNCHRONIZED_SCENE_PHASE(i_lester_rocket_sync_scene) > 0.99
						i_lester_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_lester_rocket_sync_scene, mission_veh[0].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[0].veh, "seat_dside_r"))
						TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], i_lester_rocket_sync_scene, str_anim_rocket_signal, "signal_forward", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
												SYNCED_SCENE_DONT_INTERRUPT, 
												RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
						SET_SYNCHRONIZED_SCENE_LOOPED(i_lester_rocket_sync_scene, FALSE)
						
						i_lester_rocket_anim_event++
					ENDIF
				ELIF i_lester_rocket_anim_event = 2
					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_lester_rocket_sync_scene)
					AND GET_SYNCHRONIZED_SCENE_PHASE(i_lester_rocket_sync_scene) > 0.99
						i_lester_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_lester_rocket_sync_scene, mission_veh[0].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[0].veh, "seat_dside_r"))
						TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], i_lester_rocket_sync_scene, str_anim_rockets, "intro_90r", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
												SYNCED_SCENE_DONT_INTERRUPT, 
												RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
						SET_SYNCHRONIZED_SCENE_LOOPED(i_lester_rocket_sync_scene, FALSE)
						
						i_lester_rocket_anim_event++
					ENDIF
				ELIF i_lester_rocket_anim_event = 3
					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_lester_rocket_sync_scene)
					AND GET_SYNCHRONIZED_SCENE_PHASE(i_lester_rocket_sync_scene) > 0.99
						i_lester_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_lester_rocket_sync_scene, mission_veh[0].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[0].veh, "seat_dside_r"))
						TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], i_lester_rocket_sync_scene, str_anim_rockets, "sweep_medium", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
												SYNCED_SCENE_DONT_INTERRUPT, 
												RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
						SET_SYNCHRONIZED_SCENE_LOOPED(i_lester_rocket_sync_scene, TRUE)
						SET_SYNCHRONIZED_SCENE_PHASE(i_lester_rocket_sync_scene, 0.98)
						SET_SYNCHRONIZED_SCENE_RATE(i_lester_rocket_sync_scene, 0.0)
						
						e_rocket_anim_state = ROCKET_ANIM_AIMING
					ENDIF
				ENDIF
			BREAK
			
			CASE ROCKET_ANIM_RELOAD_THEN_AIM
				IF i_lester_rocket_anim_event = 0
					IF IS_ENTITY_ATTACHED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						DETACH_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					ENDIF
					
					IF DOES_ENTITY_EXIST(obj_single_rocket)
						SET_ENTITY_VISIBLE(obj_single_rocket, TRUE)
					ENDIF
					
					i_lester_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_lester_rocket_sync_scene, mission_veh[0].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[0].veh, "seat_dside_r"))
					TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], i_lester_rocket_sync_scene, str_anim_rockets, "outro_90r", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
											SYNCED_SCENE_DONT_INTERRUPT, 
											RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
					SET_SYNCHRONIZED_SCENE_LOOPED(i_lester_rocket_sync_scene, FALSE)
					
					i_lester_rocket_anim_event++
				ELIF i_lester_rocket_anim_event = 1 
					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_lester_rocket_sync_scene)
					AND GET_SYNCHRONIZED_SCENE_PHASE(i_lester_rocket_sync_scene) > 0.99
						i_lester_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_lester_rocket_sync_scene, mission_veh[0].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[0].veh, "seat_dside_r"))
						TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], i_lester_rocket_sync_scene, str_anim_rocket_reload, "reload_rocket_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
												SYNCED_SCENE_DONT_INTERRUPT, 
												RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
						SET_SYNCHRONIZED_SCENE_LOOPED(i_lester_rocket_sync_scene, FALSE)
						
						i_lester_rocket_anim_event++
					ENDIF
				ELIF i_lester_rocket_anim_event = 2
					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_lester_rocket_sync_scene)
					AND GET_SYNCHRONIZED_SCENE_PHASE(i_lester_rocket_sync_scene) > 0.99
						i_lester_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_lester_rocket_sync_scene, mission_veh[0].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[0].veh, "seat_dside_r"))
						TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], i_lester_rocket_sync_scene, str_anim_rockets, "intro_90r", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
												SYNCED_SCENE_DONT_INTERRUPT, 
												RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
						SET_SYNCHRONIZED_SCENE_LOOPED(i_lester_rocket_sync_scene, FALSE)
						
						i_lester_rocket_anim_event++
					ENDIF
				ELIF i_lester_rocket_anim_event = 3
					IF IS_SYNCHRONIZED_SCENE_RUNNING(i_lester_rocket_sync_scene)
					AND GET_SYNCHRONIZED_SCENE_PHASE(i_lester_rocket_sync_scene) > 0.99
						i_lester_rocket_sync_scene = CREATE_SYNCHRONIZED_SCENE(<<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(i_lester_rocket_sync_scene, mission_veh[0].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[0].veh, "seat_dside_r"))
						TASK_SYNCHRONIZED_SCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], i_lester_rocket_sync_scene, str_anim_rockets, "sweep_medium", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 
												SYNCED_SCENE_DONT_INTERRUPT, 
												RBF_BULLET_IMPACT | RBF_VEHICLE_IMPACT | RBF_PLAYER_IMPACT | RBF_EXPLOSION | RBF_WATER_JET)
						SET_SYNCHRONIZED_SCENE_LOOPED(i_lester_rocket_sync_scene, TRUE)
						SET_SYNCHRONIZED_SCENE_PHASE(i_lester_rocket_sync_scene, 0.98)
						SET_SYNCHRONIZED_SCENE_RATE(i_lester_rocket_sync_scene, 0.0)
						
						IF DOES_ENTITY_EXIST(obj_single_rocket)
							SET_ENTITY_VISIBLE(obj_single_rocket, FALSE)
						ENDIF
						
						e_rocket_anim_state = ROCKET_ANIM_AIMING
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC SET_FRANKLIN_ROCKET_ANIM_STATE(ROCKET_ANIM_STATE e_new_state)
	e_rocket_anim_state = e_new_state
	i_lester_rocket_anim_event = 0
ENDPROC


/// PURPOSE:
///    Updates the heli chase: the chase works using two main car recordings for cop helicopters, these recordings last the entire length of the chase. 
///    If there are no helicopters, then one is created behind the player, and told to catch up to the correct position in the chase. Once the chopper
///    dies a new one is created later.
/// PARAMS:
///    f_playback_time - 
///    f_playback_speed - 
PROC UPDATE_HELI_CHASE()
	//VECTOR v_player_pos = GET_ENTITY_COORDS(PLAYER_PED_ID())
	INT i 
	//Handle the spawning and rubber banding of enemy choppers.
	FOR i = 20 TO 25
		IF IS_VEHICLE_DRIVEABLE(mission_veh[i].veh)
			VECTOR v_chopper_pos = GET_ENTITY_COORDS(mission_veh[i].veh)
			FLOAT f_chopper_heading = GET_ENTITY_HEADING(mission_veh[i].veh) 
			VECTOR v_chopper_front_high = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v_chopper_pos, f_chopper_heading, <<0.0, 5.6773, 2.9826>>)
			VECTOR v_chopper_rear_low = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(v_chopper_pos, f_chopper_heading, <<0.0, -9.0260, -0.5653>>)
		
			IF IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, v_chopper_front_high, v_chopper_rear_low, 8.0)
				STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[i].veh)
				EXPLODE_VEHICLE(mission_veh[i].veh)
				SET_VEHICLE_PETROL_TANK_HEALTH(mission_veh[i].veh, -100.0)
			ENDIF	
		ENDIF
	ENDFOR
	
	//Lester rockets: don't do any of this stuff if we're in the military choppers.
	ROCKET_LINE_UP_STATE e_rocket_state
	VEHICLE_INDEX vehClosest
	
	IF i_player_rocket_event !=2
		vehClosest = GET_CLOSEST_ENEMY_VEHICLE_FROM_PED(PLAYER_PED_ID())
	ENDIF
	
	PRINTSTRING("i_player_rocket_event:")PRINTINT(i_player_rocket_event)PRINTNL()
	
	
	//Rockets: come from buddies, and later on enemies.
		//Sequence of events for Lester rockets:
		// - Waits for the player to get into position.
		// - Fires the rocket.
		// - Reloads.
		// - If the rocket missed then repeat, otherwise pick a new target.
		SWITCH i_player_rocket_event
			CASE 0 //Lester gets the rocket launcher out.
				SET_FRANKLIN_ROCKET_ANIM_STATE(ROCKET_ANIM_USING_AI)
				IF IS_VEHICLE_DRIVEABLE(vehClosest)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
						IF GET_DISTANCE_BETWEEN_ENTITIES(vehClosest,mission_veh[0].veh) < 850
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech, "D2BAUD", "DS2A_MER", CONV_PRIORITY_MEDIUM)
									SET_FRANKLIN_ROCKET_ANIM_STATE(ROCKET_ANIM_GET_LAUNCHER_OUT_THEN_AIM)
									i_player_rocket_event++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 1 //Lester waits for the player to get into a good position, then fires.
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_ROCHELP")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData)
						PRINT_HELP("DS2_ROCHELP")
						SET_LABEL_AS_TRIGGERED("DS2_ROCHELP", TRUE)
					ENDIF
				ENDIF
				
				//Play reloading dialogue.
				IF e_rocket_anim_state = ROCKET_ANIM_RELOAD_THEN_AIM
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_RELOAD")
							IF CREATE_CONVERSATION(sSpeech, "D2BAUD", "DS2A_RELOAD", CONV_PRIORITY_MEDIUM)
								SET_LABEL_AS_TRIGGERED("DS2A_RELOAD", TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			
				e_rocket_state = GET_ROCKET_LINE_UP_STATE(vehClosest, mission_veh[0].veh)
				
				IF GET_GAME_TIMER() - i_rocket_timer > 3000 //Guarantee some time between rockets.
					IF e_rocket_state = LINE_UP_DONT_FIRE_TOO_FAR_AWAY
						IF GET_GAME_TIMER() - i_rocket_line_up_dialogue_timer > 0
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech, "D2BAUD", "DS2A_TOOFAR", CONV_PRIORITY_MEDIUM)
									SET_FRANKLIN_ROCKET_ANIM_STATE(ROCKET_ANIM_GET_CLOSER_THEN_AIM)
									i_rocket_line_up_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 10000)
								ENDIF
							ENDIF
						ENDIF
					ELIF e_rocket_state = LINE_UP_DONT_FIRE_TOO_CLOSE
						IF GET_GAME_TIMER() - i_rocket_line_up_dialogue_timer > 0
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech, "D2BAUD", "DS2A_TOOCLOSE", CONV_PRIORITY_MEDIUM)
									SET_FRANKLIN_ROCKET_ANIM_STATE(ROCKET_ANIM_GET_CLOSER_THEN_AIM)
									i_rocket_line_up_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 10000)
								ENDIF
							ENDIF
						ENDIF
					ELIF e_rocket_state = LINE_UP_DONT_FIRE_BAD_ANGLE
						IF GET_GAME_TIMER() - i_rocket_line_up_dialogue_timer > 0
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech, "D2BAUD", "DS2A_LINEUP", CONV_PRIORITY_MEDIUM)
									SET_FRANKLIN_ROCKET_ANIM_STATE(ROCKET_ANIM_GET_CLOSER_THEN_AIM)
									i_rocket_line_up_dialogue_timer = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(8000, 10000)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_GOSHOT")
								IF CREATE_CONVERSATION(sSpeech, "D2BAUD", "DS2A_GOSHOT", CONV_PRIORITY_MEDIUM)
									SET_LABEL_AS_TRIGGERED("DS2A_GOSHOT", TRUE)
								ENDIF
							ELIF NOT IS_ENTITY_DEAD(vehClosest)
							AND e_rocket_anim_state = ROCKET_ANIM_AIMING
								VECTOR v_start_pos, v_end_offset
								
								IF e_rocket_state = LINE_UP_FIRE_LEFT_SIDE
									v_start_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[0].veh, <<-1.2, 2.0, 1.2>>)
									v_end_offset = <<0.0, 0.0, 0.0>>
								ELIF e_rocket_state = LINE_UP_FIRE_RIGHT_SIDE
									v_start_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[0].veh, <<1.2, 2.0, 1.2>>)
									v_end_offset = <<0.0, 0.0, 0.0>>
								ELIF e_rocket_state = LINE_UP_FIRE_MISS_LEFT_SIDE
									v_start_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[0].veh, <<-1.2, 2.0, 1.2>>)
									v_end_offset = <<5.0, -5.0, -5.0>>
								ELIF e_rocket_state = LINE_UP_FIRE_MISS_RIGHT_SIDE
									v_start_pos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[0].veh,<<1.2, 2.0, 1.2>>)
									v_end_offset = <<5.0, -5.0, -5.0>>
								ENDIF
								
								IF FIRE_ROCKET(v_start_pos, vehClosest, mission_veh[0].veh, v_end_offset, FALSE, FALSE)
									SET_LABEL_AS_TRIGGERED("DS2A_RELOAD", FALSE)
									SET_FRANKLIN_ROCKET_ANIM_STATE(ROCKET_ANIM_RELOAD_THEN_AIM)
									i_player_rocket_event++
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 2 //Reload and update target if they were hit.
				IF i_rocket_timer != 0
					
					IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[20].veh)
						IF DOES_BLIP_EXIST(mission_veh[20].blip)
							REMOVE_BLIP(mission_veh[20].blip)
						ENDIF
					ENDIF
					
					IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[21].veh)
						IF DOES_BLIP_EXIST(mission_veh[21].blip)
							REMOVE_BLIP(mission_veh[21].blip)
						ENDIF
					ENDIF
					
					IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[22].veh)
						IF DOES_BLIP_EXIST(mission_veh[22].blip)
							REMOVE_BLIP(mission_veh[22].blip)
						ENDIF
					ENDIF
					
					IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[23].veh)
						IF DOES_BLIP_EXIST(mission_veh[23].blip)
							REMOVE_BLIP(mission_veh[23].blip)
						ENDIF
					ENDIF
					
					IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[24].veh)
						IF DOES_BLIP_EXIST(mission_veh[24].blip)
							REMOVE_BLIP(mission_veh[24].blip)
						ENDIF
					ENDIF
					
					IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[25].veh)
						IF DOES_BLIP_EXIST(mission_veh[25].blip)
							REMOVE_BLIP(mission_veh[25].blip)
						ENDIF
					ENDIF
					
					BLIP_INDEX blipClosest
					IF DOES_ENTITY_EXIST(vehClosest)
						blipClosest = GET_BLIP_FROM_ENTITY(vehClosest)
					ENDIF
					IF NOT IS_VEHICLE_DRIVEABLE(vehClosest) //Chopper was blown up.
						IF DOES_BLIP_EXIST(blipClosest)
							REMOVE_BLIP(blipClosest)
						ENDIF
						IF CREATE_CONVERSATION(sSpeech, "D2BAUD", "DS2A_CHOHIT", CONV_PRIORITY_MEDIUM)
							i_rocket_timer = GET_GAME_TIMER()
							i_player_rocket_event--
						ENDIF
					ELSE
						IF CREATE_CONVERSATION(sSpeech, "D2BAUD", "DS2A_CHOMISS", CONV_PRIORITY_MEDIUM)
							i_rocket_timer = GET_GAME_TIMER()
							i_player_rocket_event--
						ENDIF
					ENDIF
				ENDIF
				
				IF i_player_rocket_event != 2
					SET_LABEL_AS_TRIGGERED("BS2B_GOSHOT", FALSE)
				ENDIF
			BREAK
		ENDSWITCH
		
		UPDATE_FRANKLIN_ROCKET_ANIMS()
	
		UPDATE_ROCKETS()

ENDPROC

INT iChaseTimer[3]

PROC MANAGE_FRANKLIN_SHOOTING()
	
	SWITCH iFranklinShooting
		
		CASE 0	
			IF NOT HAS_PED_GOT_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_RPG)
				GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_RPG,INFINITE_AMMO,TRUE)
			ENDIF
			REQUEST_ANIM_DICT("missbigscore2big_5")
			REQUEST_ANIM_DICT("missbigscore2big_6")
			REQUEST_ANIM_DICT("missbigscore2big_7_p2")
			i_player_rocket_event = 0
			iFranklinShooting ++
		BREAK
		
		CASE 1
			IF HAS_ANIM_DICT_LOADED("missbigscore2big_5")
			AND HAS_ANIM_DICT_LOADED("missbigscore2big_6")
			AND HAS_ANIM_DICT_LOADED("missbigscore2big_7_p2")
				iFranklinShooting ++
			ENDIF
		BREAK
		
		CASE 2
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[20].veh)
				IF DOES_BLIP_EXIST(mission_veh[20].blip)
					REMOVE_BLIP(mission_veh[20].blip)
				ENDIF
			ELSE
				PRINTSTRING("TASKING VEHICLE 20")PRINTNL()
				DO_PERSISTENT_CHASE(driver[0].ped,mission_veh[20].veh,TRUE, iChaseTimer[0],10,15)
			ENDIF
			
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[21].veh)
				IF DOES_BLIP_EXIST(mission_veh[21].blip)
					REMOVE_BLIP(mission_veh[21].blip)
				ENDIF
			ELSE
				PRINTSTRING("TASKING VEHICLE 21")PRINTNL()
				DO_PERSISTENT_CHASE(driver[2].ped,mission_veh[21].veh,TRUE, iChaseTimer[1],25,30)
			ENDIF
			
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[22].veh)
				IF DOES_BLIP_EXIST(mission_veh[22].blip)
					REMOVE_BLIP(mission_veh[22].blip)
				ENDIF
			ELSE
				PRINTSTRING("TASKING VEHICLE 22")PRINTNL()
				DO_PERSISTENT_CHASE(driver[5].ped,mission_veh[22].veh,TRUE, iChaseTimer[2],5,10)
			ENDIF
			
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[23].veh)
				IF DOES_BLIP_EXIST(mission_veh[23].blip)
					REMOVE_BLIP(mission_veh[23].blip)
				ENDIF
			ENDIF
			
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[24].veh)
				IF DOES_BLIP_EXIST(mission_veh[24].blip)
					REMOVE_BLIP(mission_veh[24].blip)
				ENDIF
			ENDIF
			
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[25].veh)
				IF DOES_BLIP_EXIST(mission_veh[25].blip)
					REMOVE_BLIP(mission_veh[25].blip)
				ENDIF
			ENDIF
			
			UPDATE_HELI_CHASE()
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC CLEAR_THE_AREA_OF_MERRY_WEATHER

	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
			IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh)
				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
			ENDIF
		ENDIF
	ENDIF
	
	SWITCH iprogress

		CASE 0  //setup
			CONTROL_FADE_IN(500)
			bReprintObjective = FALSE
			KILL_ANY_CONVERSATION()
			//
			PRINT_GOD_TEXT("DS2_KILL_M")
			
			//DIALOGUE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				ADD_PED_FOR_DIALOGUE(sSpeech, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				ADD_PED_FOR_DIALOGUE(sSpeech, 3, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
				FREEZE_ENTITY_POSITION(mission_veh[0].veh,FALSE)
			ENDIF
			
			bReprintObjective = FALSE
			iSwitchStage = 0
			iFranklinShooting = 0
			
			iprogress++
		BREAK
		
		CASE 1

			IF NOT HAS_LABEL_BEEN_TRIGGERED("TRIGGER MERRY WEATHER")
				IF INITIALISE_MERRY_WEATHER_ATTACK()
					SET_LABEL_AS_TRIGGERED("TRIGGER MERRY WEATHER",TRUE)
				ENDIF
			ELSE
				MANAGE_FRANKLIN_SHOOTING()
				IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[20].veh)
				AND NOT IS_VEHICLE_DRIVEABLE(mission_veh[21].veh)
				AND NOT IS_VEHICLE_DRIVEABLE(mission_veh[22].veh)
				AND NOT IS_VEHICLE_DRIVEABLE(mission_veh[23].veh)
				AND NOT IS_VEHICLE_DRIVEABLE(mission_veh[24].veh)
				AND NOT IS_VEHICLE_DRIVEABLE(mission_veh[25].veh)
					IF DOES_BLIP_EXIST(mission_veh[20].blip)
						REMOVE_BLIP(mission_veh[20].blip)
					ENDIF
					IF DOES_BLIP_EXIST(mission_veh[21].blip)
						REMOVE_BLIP(mission_veh[21].blip)
					ENDIF
					IF DOES_BLIP_EXIST(mission_veh[22].blip)
						REMOVE_BLIP(mission_veh[22].blip)
					ENDIF
					IF DOES_BLIP_EXIST(mission_veh[23].blip)
						REMOVE_BLIP(mission_veh[23].blip)
					ENDIF
					IF DOES_BLIP_EXIST(mission_veh[24].blip)
						REMOVE_BLIP(mission_veh[24].blip)
					ENDIF
					IF DOES_BLIP_EXIST(mission_veh[25].blip)
						REMOVE_BLIP(mission_veh[25].blip)
					ENDIF
					bGoToSpeach = FALSE
					iprogress++
				ENDIF
			ENDIF
		
		BREAK
		
		CASE 2
			
			VECTOR vTemp
			
			IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,<<-1388.26, 6160.88, 0.34>>,<<200,200,80>>,FALSE,mission_veh[0].veh,"DS2_FLYDOP","DS2_FLYGI","DS2_FLYGB",TRUE)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
					iprogress++	
				ENDIF
			ELSE
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF IS_IT_SAFE_TO_PLAY_DIALOG()
							IF bGoToSpeach = FALSE
								IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P1b")
									IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P1b",CONV_PRIORITY_HIGH)
										SET_LABEL_AS_TRIGGERED("DS2A_P1b",TRUE)
										bGoToSpeach = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
				
				IF bReprintObjective = FALSE
					IF vTemp.z > 80
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<-1388.26, 6160.88, 0.34>>,<<200,200,400>>)
							PRINT_NOW("DS2_FLYD",DEFAULT_GOD_TEXT_TIME,0)
							bReprintObjective = TRUE
						ENDIF
					ENDIF
				ENDIF
						
				IF bGoToSpeach = TRUE
					//MANAGE PAUSE DIALOGUE
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							CLEAR_PRINTS()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						ENDIF
					//Play conversation
					ELSE
						IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_FLYGB")
								CLEAR_PRINTS()
							ENDIF
					    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			bTriggerDialogue = FALSE
			iprogress = 0
			bReprintObjective = FALSE
			emissionstage = MISSION_STAGE_DROP_THE_SUB
		BREAK
		
	ENDSWITCH

ENDPROC


BOOL bBeenToDropOffPoint = FALSE

PROC DROP_THE_SUB()

VECTOR vTemp

	IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_SPLANE")
		IF GET_DISTANCE_BETWEEN_COORDS(<<-999.23163, 6248.24023, 1.13868>>,GET_ENTITY_COORDS(PLAYER_PED_ID()),FALSE) < 1500
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh)
				IF NOT WOULD_ENTITY_BE_OCCLUDED(SUBMERSIBLE,GET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh))
					IF IS_ENTITY_ON_SCREEN(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh)
						IF MANAGE_MY_TIMER(i_plane_timer,12000)
							IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_SEE_PLANE")
								START_AUDIO_SCENE("DH_2B_SEE_PLANE")
							ENDIF
							IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_SPLANE",CONV_PRIORITY_HIGH)
								SET_LABEL_AS_TRIGGERED("DS2A_SPLANE",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<-1388.26, 6160.88, 0.34>>,<<250,250,130>>)
			SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(mission_veh[MV_CHINOOK].veh,0.0)
		ELSE
			SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(mission_veh[MV_CHINOOK].veh,1.0)
		ENDIF
	ENDIF
	
	IF iprogress > 1
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
			IF IS_ENTITY_IN_WATER(mission_veh[MV_SUBMERISIBLE].veh)
				IF NOT HAS_LABEL_BEEN_TRIGGERED("VEHICLE_WATER_SPLASH_HEAVY_SCRIPT")
					PLAY_SOUND_FROM_ENTITY(-1,"VEHICLE_WATER_SPLASH_HEAVY_SCRIPT",mission_veh[MV_SUBMERISIBLE].veh)
					SET_LABEL_AS_TRIGGERED("VEHICLE_WATER_SPLASH_HEAVY_SCRIPT",TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
SWITCH iprogress
	
	CASE 0  //setup
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
			IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK)
			ENDIF
		ENDIF
		CONTROL_FADE_IN(500)
		bReprintObjective = FALSE
		KILL_ANY_CONVERSATION()
		PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
		//PRINT_NOW("DS2_RELSUB",DEFAULT_GOD_TEXT_TIME,0)
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_RELEASE_SUB")
			START_AUDIO_SCENE("DH_2B_RELEASE_SUB")
		ENDIF
		
		//DIALOGUE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRUE)
			ADD_PED_FOR_DIALOGUE(sSpeech, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
			SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],PCF_GetOutUndriveableVehicle,TRUE)
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			ADD_PED_FOR_DIALOGUE(sSpeech, 3, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
			FREEZE_ENTITY_POSITION(mission_veh[0].veh,FALSE)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
			FORCE_SUBMARINE_SURFACE_MODE(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
		ENDIF
		
		REPLAY_RECORD_BACK_FOR_TIME(0.0, 6.5, REPLAY_IMPORTANCE_HIGHEST)
		
		bReprintObjective = FALSE
		brunfailchecks = TRUE
		iSwitchStage = 0
		SET_LABEL_AS_TRIGGERED("DS2_GRAP_HELP2",FALSE)
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3,"Drop the sub") 
		iSubBeached = 0
		iprogress++
	BREAK
	
	CASE 1
		IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
			IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,<<-1388.26, 6160.88, 0.34>>,<<0.1,0.1,0.1>>,FALSE,mission_veh[0].veh,"DS2_RELSUB","DS2_FLYGI","DS2_FLYGB",TRUE)
				
			ELSE		
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<-1388.26, 6160.88, 0.34>>,<<250,250,130>>)
						
						IF iSubBeached = 0
							IF NOT IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
								iSubBeached = GET_GAME_TIMER()
							ENDIF
						ENDIF
						
						bBeenToDropOffPoint = TRUE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_GRAP_HELP2")
							PRINT_HELP("DS2_GRAP_HELP2")
							SET_LABEL_AS_TRIGGERED("DS2_GRAP_HELP2",TRUE)
						ENDIF
						IF bReprintObjective = TRUE
							IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_FLYRET")
								CLEAR_PRINTS()
							ENDIF
							IF DOES_BLIP_EXIST(dest_blip)
								REMOVE_BLIP(dest_blip)
							ENDIF
							PRINT_NOW("DS2_RELSUB",DEFAULT_GOD_TEXT_TIME,0)
							bReprintObjective = FALSE
						ENDIF
						IF NOT IS_SUB_ABOVE_BARGE()
						OR (IS_ENTITY_IN_WATER(mission_veh[1].veh) AND GET_ENTITY_HEIGHT(mission_veh[MV_SUBMERISIBLE].veh, GET_ENTITY_COORDS(mission_veh[MV_SUBMERISIBLE].veh), true, true) < 1.0)
							IF NOT IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
								IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
									IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
										REMOVE_BLIP(sLocatesData.LocationBlip)
									ENDIF
									
									IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
										STABILISE_ENTITY_ATTACHED_TO_HELI(mission_veh[MV_CHINOOK].veh,NULL,iStabliseLength)
									ENDIF
									
									CLEAR_PRINTS()
									CLEAR_HELP()
									SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,TRUE)
									SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,FALSE)
									sCamDetails.bSplineCreated = FALSE
									iSubBeached = GET_GAME_TIMER()
									iprogress++
								ENDIF
							ENDIF
						ELSE
							vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
							
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P1c")
								IF vTemp.z > 100
									IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P1c",CONV_PRIORITY_HIGH)
										SET_LABEL_AS_TRIGGERED("DS2A_P1c",TRUE)
										iDialogueTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ELSE
								IF MANAGE_MY_TIMER(iDialogueTimer,15000 + GET_RANDOM_INT_IN_RANGE(1000,5000))
									SET_LABEL_AS_TRIGGERED("DS2A_P1c",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
							IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK)
							ENDIF
						ENDIF
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<-1388.26, 6160.88, 0.34>>,<<260,260,130>>)
							IF bReprintObjective = FALSE
								IF bBeenToDropOffPoint = TRUE
									CLEAR_PRINTS()
									CLEAR_HELP()
									//dest_blip = ADD_BLIP_FOR_COORD(<<-1388.26, 6160.88, 0.34>>)
									PRINT_NOW("DS2_FLYRET",DEFAULT_GOD_TEXT_TIME,0)
									bReprintObjective = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	BREAK
	
	
	CASE 2
		IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
			REMOVE_BLIP(sLocatesData.LocationBlip)
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("DH_2B_RELEASE_SUB")
			STOP_AUDIO_SCENE("DH_2B_RELEASE_SUB")
		ENDIF
		
//		IF NOT HAS_LABEL_BEEN_TRIGGERED("SUB IN WATER")
//			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
//				//IF IS_ENTITY_IN_WATER(mission_veh[1].veh)
//					iGodTextDelay = GET_GAME_TIMER()
//					SET_LABEL_AS_TRIGGERED("SUB IN WATER",TRUE)
////					iprogress++
//				//ENDIF
//			ENDIF
//		ELSE
//			IF MANAGE_MY_TIMER(iGodTextDelay,1000)
//			IF MANAGE_MY_TIMER(iGodTextDelay, 1)
				//PRINT_NOW("DS2_S2TH",DEFAULT_GOD_TEXT_TIME,0)
//				SET_SELECTOR_PED_PRIORITY(sSelectorPeds, SELECTOR_PED_TREVOR, SELECTOR_PED_MICHAEL, SELECTOR_PED_FRANKLIN)
//				SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_TREVOR,TRUE)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
					IF IS_VEHICLE_SEAT_FREE(mission_veh[1].veh,VS_DRIVER)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[1].veh)
					ENDIF
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					SET_VEHICLE_NOISE_BASED_ON_FLIGHT_STAT(mission_veh[MV_CHINOOK].veh,GET_SP_PLAYER_PED_STAT_VALUE(CHAR_MICHAEL, PS_FLYING_ABILITY))
				ENDIF
				iprogress++
//			ENDIF
//		ENDIF
	BREAK
	
	CASE 3
		MANAGE_FREE_HOTSWAP(FALSE, FALSE, FALSE, FALSE, TRUE)
		IF scsSwitchCam_MichaelToTrevor.bIsSplineCamFinishedPlaying
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			AND iSwitchStage = 0
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh)
				SET_LABEL_AS_TRIGGERED("SUB IN WATER",TRUE)
				SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_TREVOR,FALSE)
				IF TRIGGER_MUSIC_EVENT("DH2B_TREV_SUB")
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
					ENDIF
					STOP_AUDIO_SCENE("DH_2B_GET_TO_DROP_ZONE")
					CLEAR_SELECTOR_PED_PRIORITY(sSelectorPeds)
					CLEAR_HELP()
					CLEAR_PRINTS()
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					
					scsSwitchCam_MichaelToTrevor.bIsSplineCamFinishedPlaying = FALSE
					
					iprogress++
				ENDIF
			ENDIF
		ENDIF
	BREAK

	CASE 4
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					TASK_HELI_MISSION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh,NULL,<<0,0,0>>,MISSION_ESCORT_REAR,20,20,-1,30,20)
				ENDIF
			ENDIF
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
			APPLY_FORCE_TO_ENTITY(mission_veh[MV_SUBMERISIBLE].veh, APPLY_TYPE_IMPULSE, <<0,0,-1>>, <<0,0,0>>,0, TRUE, TRUE, FALSE)
		ENDIF
		bTriggerDialogue = FALSE
		iprogress = 0
		emissionstage = MISSION_STAGE_FIND_THE_CRATE
	BREAK
	
ENDSWITCH

ENDPROC

PROC FIND_THE_CRATE()

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_SUBMERISIBLE].veh)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
			IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh)
				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
			ENDIF
		ENDIF
	ENDIF

	//SETUP_MISSION_REQUIREMENT(REQ_WATERLIFE,<<0,0,0 >>,0)
	
	IF GET_DISTANCE_BETWEEN_COORDS(<< -1260.04, 6802.21, -174.41 >>,GET_ENTITY_COORDS(PLAYER_PED_ID())) < 200
		SETUP_MISSION_REQUIREMENT(REQ_SHARK_BY_CRATE,<<0,0,0 >>,0)
	ENDIF
	
	IF NOT HAS_LABEL_BEEN_TRIGGERED("WORLD_SHARK_SWIM")
		IF NOT IS_PED_INJURED(pedUnderWater[0])
			IF DOES_ENTITY_EXIST(objSeabed[1])
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),GET_ENTITY_COORDS(objSeabed[1]),<<50,50,100>>)
					IF IS_ENTITY_ON_SCREEN(pedUnderWater[0])
						OPEN_SEQUENCE_TASK(seq)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL,FALSE)
							TASK_GO_STRAIGHT_TO_COORD(NULL,<<-1291.6602, 6841.8726, -176.0327>>, 1.0, DEFAULT_TIME_NEVER_WARP)
							TASK_GO_STRAIGHT_TO_COORD(NULL,<<-1310.5876, 6952.5659, -208.4632>>, 1.0, DEFAULT_TIME_NEVER_WARP)
							TASK_START_SCENARIO_IN_PLACE(NULL,"WORLD_SHARK_SWIM")
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL,150)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(pedUnderWater[0], seq)
						
						SET_LABEL_AS_TRIGGERED("WORLD_SHARK_SWIM",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

SWITCH iprogress

	CASE 0  //setup
		IF SETUP_MISSION_REQUIREMENT(REQ_CARGO,<<-1255.3800, 6795.7202, -179.1137>>,0)
			CONTROL_FADE_IN(500)
			bReprintObjective = FALSE
			bGoToSpeach = FALSE
			itimer = GET_GAME_TIMER()
			PRINT_NOW("DS2_USESON",DEFAULT_GOD_TEXT_TIME,0)
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_FIND_CONTAINER")
				START_AUDIO_SCENE("DH_2B_FIND_CONTAINER")
			ENDIF
			
			ENABLE_SECOND_SCREEN_TRACKIFY_APP(TRUE)
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				FREEZE_ENTITY_POSITION(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(mission_veh[MV_SUBMERISIBLE].veh)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
				FREEZE_ENTITY_POSITION(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
				SET_ENTITY_INVINCIBLE(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
				SET_VEHICLE_LIGHTS(mission_veh[MV_SUBMERISIBLE].veh, SET_VEHICLE_LIGHTS_ON)
			ENDIF
			
			IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh)
				DELETE_VEHICLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh)
				SET_MODEL_AS_NO_LONGER_NEEDED(LAZER)
			ENDIF
			
			IF DOES_ENTITY_EXIST(driver[10].ped)
				DELETE_PED(driver[10].ped)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BLACKOPS_01)
			ENDIF
			
			IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CARGOPLANE1].veh)
				DELETE_VEHICLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE1].veh)
				SET_MODEL_AS_NO_LONGER_NEEDED(LAZER)
			ENDIF
			
			IF DOES_ENTITY_EXIST(driver[11].ped)
				DELETE_PED(driver[11].ped)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BLACKOPS_01)
			ENDIF
			
			IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CARGOPLANE2].veh)
				DELETE_VEHICLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE2].veh)
				SET_MODEL_AS_NO_LONGER_NEEDED(LAZER)
			ENDIF
			
			IF DOES_ENTITY_EXIST(driver[12].ped)
				DELETE_PED(driver[12].ped)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BLACKOPS_01)
			ENDIF
			
			IF DOES_ENTITY_EXIST(enemy[10].ped)
				DELETE_PED(enemy[10].ped)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BLACKOPS_01)
			ENDIF
			
			IF DOES_ENTITY_EXIST(enemy[11].ped)
				DELETE_PED(enemy[11].ped)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BLACKOPS_01)
			ENDIF
			
			IF DOES_ENTITY_EXIST(enemy[12].ped)
				DELETE_PED(enemy[12].ped)
				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BLACKOPS_01)
			ENDIF
			
			REMOVE_VEHICLE_RECORDING(ENUM_TO_INT(MV_MERRY_WEATHER_CARGOPLANE),"DHF2")
			REMOVE_VEHICLE_RECORDING(ENUM_TO_INT(MV_MERRY_WEATHER_CARGOPLANE1),"DHF2")
			REMOVE_VEHICLE_RECORDING(ENUM_TO_INT(MV_MERRY_WEATHER_CARGOPLANE2),"DHF2")
			
			REMOVE_VEHICLE_RECORDING(34,"DHF2")
			REMOVE_VEHICLE_RECORDING(35,"DHF2")
			REMOVE_VEHICLE_RECORDING(97,"DHF2")
			
			//DIALOGUE
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				ADD_PED_FOR_DIALOGUE(sSpeech, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
			ENDIF
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				ADD_PED_FOR_DIALOGUE(sSpeech, 2, PLAYER_PED_ID(), "TREVOR")
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				ADD_PED_FOR_DIALOGUE(sSpeech, 3, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				FORCE_SUBMARINE_SURFACE_MODE(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
			ENDIF
			
			SET_ENTITY_PROOFS(mission_veh[MV_SUBMERISIBLE].veh,FALSE,FALSE,FALSE,TRUE,FALSE)
			
			ALLOW_DIALOGUE_IN_WATER(TRUE)
			itimer = GET_GAME_TIMER()
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4,"Find the crate")
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(DH2B_TIME_TO_FIND_CONTAINER)
			//SET_TIMECYCLE_MODIFIER("underwater_deep_clear")
			PRINTSTRING("SUBMERISBLE CRUSH DISABLED")
			SET_SUBMARINE_CRUSH_DEPTHS(mission_veh[1].veh,FALSE,-190,-190,-190)
								
			iprogress++
		ENDIF
	BREAK
	
	
	
	CASE 1
		IF MANAGE_MY_TIMER(itimer,2000)
			SET_TRACKIFY_TARGET_VECTOR(<< -1241.86, 6796.08, -180.88>>,TRUE)
			LAUNCH_TRACKIFY_IMMEDIATELY()
			bReprintObjective = FALSE
			itimer = GET_GAME_TIMER()
			PRINT_HELP("DS2_TRACK_HELP")
			iprogress++
		ELSE
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_NOAPP")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_NOAPP",CONV_PRIORITY_HIGH)
							SET_LABEL_AS_TRIGGERED("DS2A_NOAPP",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	BREAK
	
	CASE 2
		IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_SUB_CONT")
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP("DS2_SUB_CONT")
				SET_LABEL_AS_TRIGGERED("DS2_SUB_CONT",TRUE)
			ENDIF
		ENDIF
		
		IF IS_CELLPHONE_TRACKIFY_IN_USE()
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_LIVEAPP")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_LIVEAPP",CONV_PRIORITY_HIGH)
							SET_LABEL_AS_TRIGGERED("DS2A_LIVEAPP",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF bReprintObjective = FALSE
				IF MANAGE_MY_TIMER(itimer,5000)
					CLEAR_PRINTS()
					PRINT_NOW("DS2_LAPP",DEFAULT_GOD_TEXT_TIME,0)
					bReprintObjective = TRUE
				ENDIF
			ENDIF
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[1].veh)
				IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,GET_ENTITY_COORDS(objSeabed[1]),<<0.1,0.1,0.1>>,FALSE,mission_veh[1].veh,"DS2_USESON","DS2_CONG","DS2_CONGB",TRUE)
				
				ELSE
					IF bGoToSpeach = TRUE
						//MANAGE PAUSE DIALOGUE
						IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[1].veh)
								IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
									CLEAR_PRINTS()
									PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
								ENDIF
							//Play conversation
							ELSE
								IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
									IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_FLYGB")
										CLEAR_PRINTS()
									ENDIF
							    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						SET_BLIP_ALPHA(sLocatesData.LocationBlip,0)
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),GET_ENTITY_COORDS(objSeabed[1]),<<30,30,30>>)
							IF REQUEST_AMBIENT_AUDIO_BANK("PORT_OF_LS_ATTACH_CARGO")
								IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_SEEDEV")
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_SEEDEV",CONV_PRIORITY_HIGH)
												SET_LABEL_AS_TRIGGERED("DS2A_SEEDEV",TRUE)
											ENDIF
										ENDIF
									ENDIF
								ELSE
									HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
									CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
									iprogress++
								ENDIF
							ENDIF
						ELSE
							IF bGoToSpeach = FALSE
								IF HAS_LABEL_BEEN_TRIGGERED("DS2A_LIVEAPP")
									IF MANAGE_MY_TIMER(itimer,2000)
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF IS_IT_SAFE_TO_PLAY_DIALOG()
												IF bGoToSpeach = FALSE
													IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P2",CONV_PRIORITY_HIGH)
														itimer = GET_GAME_TIMER() 
														bGoToSpeach = TRUE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_UPD1")
									IF MANAGE_MY_TIMER(itimer,30000)
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
												IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_UPD1",CONV_PRIORITY_HIGH)
													SET_LABEL_AS_TRIGGERED("DS2A_UPD1",TRUE)
													itimer = GET_GAME_TIMER() 
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ELSE
									IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_UPD2")
										IF MANAGE_MY_TIMER(itimer,24000)
											IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
												IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
													IF CREATE_CONVERSATION(sSpeech,"D2BAUD","DS2A_UPD2",CONV_PRIORITY_HIGH)
														SET_LABEL_AS_TRIGGERED("DS2A_UPD2",TRUE)
														itimer = GET_GAME_TIMER() 
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ELSE
										IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_UPD3")
											IF MANAGE_MY_TIMER(itimer,24000)
												IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_UPD3",CONV_PRIORITY_HIGH)
															SET_LABEL_AS_TRIGGERED("DS2A_UPD3",TRUE)
															itimer = GET_GAME_TIMER() 
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	BREAK
	
	CASE 3
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
			IF DOES_ENTITY_EXIST(objSeabed[1])
				IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,GET_ENTITY_COORDS(objSeabed[1]),<<0.1,0.1,0.1>>,FALSE,mission_veh[1].veh,"DS2_GETCON","DS2_CONG","DS2_CONGB",TRUE)
					
				ELSE
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_CONTBLIP")
							IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								SET_BLIP_NAME_FROM_TEXT_FILE(sLocatesData.LocationBlip,"DS2_CONTBLIP") 
								SET_LABEL_AS_TRIGGERED("DS2_CONTBLIP",TRUE)
							ENDIF
						ENDIF
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_SUBMERISIBLE].veh)
							//IF IS_ENTITY_AT_ENTITY(mission_veh[MV_SUBMERISIBLE].veh,objSeabed[1],<<8,8,8>>)
							IF IS_ENTITY_TOUCHING_ENTITY(mission_veh[MV_SUBMERISIBLE].veh,objSeabed[1])
								CLEAR_PRINTS()
								CLEAR_HELP()
								IF DOES_BLIP_EXIST(dest_blip)
									REMOVE_BLIP(dest_blip)
								ENDIF

								ATTACH_BOMB_TO_SUB()
								/*
								IF DOES_ENTITY_EXIST(mission_veh[MV_SUBMERISIBLE].veh)
									IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
										IF DOES_ENTITY_EXIST(objSeabed[1])
											IF NOT IS_ENTITY_ATTACHED(objSeabed[1])
												ATTACH_ENTITY_TO_ENTITY(objSeabed[1],mission_veh[MV_SUBMERISIBLE].veh,1,<<0,-2.0,-1.7>>,<<0,0,90>>,FALSE,FALSE,TRUE)
												SET_ENTITY_NO_COLLISION_ENTITY(objSeabed[1],mission_veh[MV_SUBMERISIBLE].veh,FALSE)
												SET_ENTITY_NO_COLLISION_ENTITY(mission_veh[MV_SUBMERISIBLE].veh,objSeabed[1],FALSE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
								*/
								
								PLAY_SOUND_FROM_ENTITY(-1,"Attach_Cargo",mission_veh[MV_SUBMERISIBLE].veh,"DOCKS_HEIST_FINALE_2B_SOUNDS")
								CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
								INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE, DH2B_TIME_TO_FIND_CONTAINER)
								ENABLE_SECOND_SCREEN_TRACKIFY_APP(FALSE)
								HANG_UP_AND_PUT_AWAY_PHONE(TRUE)
								iSwitchStage = 0
								iprogress++
							ELSE
								IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_PICKUPCH")
									PRINT_HELP("DS2_PICKUPCH")
									SET_LABEL_AS_TRIGGERED("DS2_PICKUPCH",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_SUBMERISIBLE].veh)
							IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(),GET_ENTITY_COORDS(objSeabed[1]),<<40,40,40>>)
								IF bReprintObjective = FALSE
									CLEAR_PRINTS()
									CLEAR_HELP()
									IF NOT DOES_BLIP_EXIST(dest_blip)
										dest_blip = ADD_BLIP_FOR_COORD(GET_ENTITY_COORDS(objSeabed[1]))
									ENDIF
									PRINT_NOW("DS2_RETCON",DEFAULT_GOD_TEXT_TIME,0)
									bReprintObjective = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	BREAK
	
	CASE 4
		REPLAY_RECORD_BACK_FOR_TIME(2)

		ATTACH_BOMB_TO_SUB()
		/*
		IF DOES_ENTITY_EXIST(mission_veh[MV_SUBMERISIBLE].veh)
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				IF DOES_ENTITY_EXIST(objSeabed[1])
					IF NOT IS_ENTITY_ATTACHED(objSeabed[1])
						ATTACH_ENTITY_TO_ENTITY(objSeabed[1],mission_veh[MV_SUBMERISIBLE].veh,1,<<0,-2.0,-1.7>>,<<0,0,90>>,FALSE,FALSE,TRUE)
						SET_ENTITY_NO_COLLISION_ENTITY(objSeabed[1],mission_veh[MV_SUBMERISIBLE].veh,FALSE)
						SET_ENTITY_NO_COLLISION_ENTITY(mission_veh[MV_SUBMERISIBLE].veh,objSeabed[1],FALSE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		*/
		
		IF DOES_BLIP_EXIST(dest_blip)
			REMOVE_BLIP(dest_blip)
		ENDIF

		IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
			SET_ENTITY_COORDS(mission_veh[0].veh,<<-1411.9094, 5957.8730, 40.3783>>)
			SET_ENTITY_HEADING(mission_veh[0].veh,221)
		ENDIF	
		
		CLEAR_PRINTS()
		CLEAR_HELP()
		CLEAR_PRINTS()
		
		IF IS_AUDIO_SCENE_ACTIVE("DH_2B_FIND_CONTAINER")
			STOP_AUDIO_SCENE("DH_2B_FIND_CONTAINER")
		ENDIF
		
		iprogress++
	BREAK
	
	CASE 5
		IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_GETDEV")
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_GETDEV",CONV_PRIORITY_HIGH)
						REPLAY_RECORD_BACK_FOR_TIME(8.0, 10., REPLAY_IMPORTANCE_HIGHEST)
						SET_LABEL_AS_TRIGGERED("DS2A_GETDEV",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF TRIGGER_MUSIC_EVENT("DH2B_GOT_CONTAINER")
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				iprogress = 0
				iSwitchStage = 0
				
				emissionstage = MISSION_STAGE_GET_TO_SURFACE
			ENDIF
		ENDIF
	BREAK
	
ENDSWITCH

ENDPROC 

PROC SWIM_TO_CRATE()
SWITCH iprogress
	CASE 0
		emissionstage = MISSION_STAGE_GET_TO_SURFACE
	BREAK
ENDSWITCH
ENDPROC

BOOL bTaskMikeToFollowTrev
VECTOR vGoToPoint
INT iReTaskTimer

PROC GET_TO_SURFACE()

IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_SUBMERISIBLE].veh)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		ENDIF
	ENDIF
ENDIF

IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_SUBMERISIBLE].veh)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		ENDIF
	ENDIF
ENDIF	

IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
		IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh)
			SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
		ENDIF
	ENDIF
ENDIF

ATTACH_BOMB_TO_SUB_PHYSICALLY()
/*
IF DOES_ENTITY_EXIST(mission_veh[MV_SUBMERISIBLE].veh)
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
		IF NOT IS_ENTITY_ATTACHED(objSeabed[1])
			ATTACH_ENTITY_TO_ENTITY(objSeabed[1],mission_veh[MV_SUBMERISIBLE].veh,1,<<0,-2.0,-1.7>>,<<0,0,90>>,FALSE,FALSE,TRUE)
			SET_ENTITY_NO_COLLISION_ENTITY(objSeabed[1],mission_veh[MV_SUBMERISIBLE].veh,FALSE)
			SET_ENTITY_NO_COLLISION_ENTITY(mission_veh[MV_SUBMERISIBLE].veh,objSeabed[1],FALSE)
		ENDIF
	ENDIF
ENDIF
*/

IF DOES_ENTITY_EXIST(pedUnderWater[0])
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedUnderWater[0],FALSE),GET_ENTITY_COORDS(PLAYER_PED_ID())) > 75
		IF NOT IS_ENTITY_ON_SCREEN(pedUnderWater[0])
			DELETE_PED(pedUnderWater[0])
			SET_MODEL_AS_NO_LONGER_NEEDED(A_C_SHARKTIGER)
		ENDIF
	ENDIF
ENDIF

IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]) 
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
		IF bTaskMikeToFollowTrev = FALSE
			vGoToPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_SUBMERISIBLE].veh,<<100,100,0>>)
			vGoToPoint.z = 80
			TASK_HELI_MISSION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],mission_veh[MV_CHINOOK].veh,NULL,NULL,vGoToPoint,MISSION_GOTO,20,30,-1,80,70, -1.0, HF_HEIGHTMAPONLYAVOIDANCE)
			iReTaskTimer = GET_GAME_TIMER()
			bTaskMikeToFollowTrev = TRUE
		ELSE
			IF MANAGE_MY_TIMER(iReTaskTimer, 3000)
				bTaskMikeToFollowTrev = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDIF


SWITCH iprogress

	CASE 0  //setup

		//DIALOGUE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(sSpeech, 2, PLAYER_PED_ID(), "TREVOR")
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			ADD_PED_FOR_DIALOGUE(sSpeech, 1, sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "MICHAEL")
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			ADD_PED_FOR_DIALOGUE(sSpeech, 3, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "FRANKLIN")
		ENDIF
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_GET_TO_SURFACE")
			START_AUDIO_SCENE("DH_2B_GET_TO_SURFACE")
		ENDIF
		
		CLEAR_AREA(<<-833.0740, 6397.1450, -10.1586>>,500,TRUE)
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
			//GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_COMBATMG,INFINITE_AMMO,TRUE)
			//GIVE_WEAPON_COMPONENT_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_COMBATMG,WEAPONCOMPONENT_COMBATMG_CLIP_02)
			//SET_PED_INFINITE_AMMO_CLIP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
			//SET_AMMO_IN_CLIP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], WEAPONTYPE_COMBATMG, 100)
			
			SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],PCF_ForcedAim,TRUE)
			IF IS_VEHICLE_SEAT_FREE(mission_veh[MV_CHINOOK].veh,VS_BACK_LEFT)
				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[MV_CHINOOK].veh,VS_BACK_LEFT)
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh)
					IF IS_VEHICLE_SEAT_FREE(mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
						SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
						SET_ENTITY_VISIBLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
			FREEZE_ENTITY_POSITION(mission_veh[MV_CHINOOK].veh,FALSE)
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],mission_veh[MV_CHINOOK].veh)
					SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],mission_veh[MV_CHINOOK].veh)
				ENDIF
			ENDIF
		ENDIF
		
		PRINTSTRING("SUBMERISBLE CRUSH DISABLED")
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
			SET_SUBMARINE_CRUSH_DEPTHS(mission_veh[MV_SUBMERISIBLE].veh,FALSE,-190,-190,-190)
			SET_VEHICLE_LIGHTS(mission_veh[MV_SUBMERISIBLE].veh, SET_VEHICLE_LIGHTS_ON)
		ENDIF

		ALLOW_DIALOGUE_IN_WATER(TRUE)
		CONTROL_FADE_IN(500)
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		FREEZE_ENTITY_POSITION(mission_veh[1].veh,FALSE)
		itimer = GET_GAME_TIMER()
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5,"Get to the surface") 
		brunfailchecks = TRUE
		bTaskMikeToFollowTrev = FALSE
		//SET_TIMECYCLE_MODIFIER("underwater_deep_clear")
		iprogress++

	BREAK
	
	CASE 1
		IF MANAGE_MY_TIMER(itimer,1000)
			brunfailchecks = TRUE
			bGoToSpeach = FALSE
			PRINT_NOW("DS2_ASSSUR",DEFAULT_GOD_TEXT_TIME,0)
			itimer = GET_GAME_TIMER()
			
			KILL_ANY_CONVERSATION()
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh) 
				SET_ENTITY_PROOFS(mission_veh[MV_SUBMERISIBLE].veh,FALSE,FALSE,FALSE,FALSE,FALSE)
			ENDIF
			iprogress++
		ENDIF
	BREAK
	
	CASE 2
	
		VECTOR vTemp
		
		
		vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
		PRINTSTRING("Z HEIGHT:")PRINTFLOAT(vTemp.z)PRINTNL()
		
		IF MANAGE_MY_TIMER(itimer,60000)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P5c")
				IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P5c",CONV_PRIORITY_HIGH)
					SET_LABEL_AS_TRIGGERED("DS2A_P5c",TRUE)
					iDialogueTimer = GET_GAME_TIMER()
				ENDIF
			ENDIF
		ENDIF

		IF MANAGE_MY_TIMER(iDialogueTimer,10000)
			IF HAS_LABEL_BEEN_TRIGGERED("DS2A_P5c")
				SET_LABEL_AS_TRIGGERED("DS2A_P5c",FALSE)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[1].veh)
				IF vTemp.z > -2
					SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
					SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_MICHAEL,FALSE)
					SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,TRUE)
					SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,TRUE)
					CLEAR_PRINTS()
					CLEAR_HELP()
					PRINT_NOW("DS2_S2MH",DEFAULT_GOD_TEXT_TIME,0)
					RELEASE_AMBIENT_AUDIO_BANK()
					REPLAY_RECORD_BACK_FOR_TIME(8.0, 10., REPLAY_IMPORTANCE_HIGHEST)
					//FREEZE_ENTITY_POSITION(mission_veh[1].veh,TRUE)
					iprogress++	
				ENDIF
				
				IF MANAGE_MY_TIMER(itimer,5000)
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF IS_IT_SAFE_TO_PLAY_DIALOG()
							IF bGoToSpeach = FALSE
								IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P5",CONV_PRIORITY_HIGH)
									bGoToSpeach = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF

	BREAK
	
	CASE 3
		IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
			REMOVE_BLIP(sLocatesData.LocationBlip)
		ENDIF
		SET_SELECTOR_PED_PRIORITY(sSelectorPeds, SELECTOR_PED_MICHAEL, SELECTOR_PED_TREVOR, SELECTOR_PED_FRANKLIN)
		SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL,TRUE)
		CLEAN_OBJECTIVE_BLIP_DISPLAY()
		iprogress++
	BREAK
	
	CASE 4
		CLEAR_SELECTOR_PED_PRIORITY(sSelectorPeds)
		CLEAR_HELP()
		CLEAR_PRINTS()
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		bTriggerDialogue = FALSE
		iprogress = 0
		bReprintObjective = FALSE
		emissionstage = MISSION_STAGE_PICK_UP_SUB
	BREAK
	
ENDSWITCH

ENDPROC

BOOL bForceSubToSurface 
INT iDripsTimer 
INT iTimeTakenToPickUp


PROC PICK_UP_SUB()
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
		IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
		SET_VEHICLE_CEILING_HEIGHT(mission_veh[MV_CHINOOK].veh,100) 
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_SUBMERISIBLE].veh)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedUnderWater[0])
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedUnderWater[0],FALSE),GET_ENTITY_COORDS(PLAYER_PED_ID())) > 50
			IF NOT IS_ENTITY_ON_SCREEN(pedUnderWater[0])
				DELETE_PED(pedUnderWater[0])
				SET_MODEL_AS_NO_LONGER_NEEDED(A_C_SHARKTIGER)
			ENDIF
		ENDIF
	ENDIF
	
	VECTOR vFlyTo

	IF iprogress = 1

		MANAGE_FREE_HOTSWAP(FALSE,TRUE,FALSE,TRUE)
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		AND iSwitchStage = 0
			IF bForceSubToSurface = FALSE
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					FORCE_SUBMARINE_SURFACE_MODE(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
				ENDIF
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],KNOCKOFFVEHICLE_NEVER)
				ENDIF
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				bForceSubToSurface = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
		vehGrabbedTemp = GET_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh)
		IF NOT IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
			IF DOES_ENTITY_EXIST(vehGrabbedTemp)
				PRINT_HELP("DS2_REL_WRONG")
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_REL_WRONG")
					CLEAR_PRINTS()
				ENDIF
			ENDIF
		ELSE
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_REL_WRONG")
				CLEAR_PRINTS()
			ENDIF
		ENDIF
	ENDIF
	
	IF iprogress > 0
		IF MANAGE_MY_TIMER(iTimeTakenToPickUp,60000)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("FLYING SKILL PICK")
				IF GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_MICHAEL_DONE_FLYING_LESSONS)
					IF (GET_MEDAL_FOR_PILOT_SCHOOL_LESSON(CHAR_MICHAEL,PSC_Takeoff) = PS_SILVER
					AND GET_MEDAL_FOR_PILOT_SCHOOL_LESSON(CHAR_MICHAEL,PSC_Landing) = PS_SILVER)
					OR (GET_MEDAL_FOR_PILOT_SCHOOL_LESSON(CHAR_MICHAEL,PSC_Takeoff) = PS_GOLD
					AND GET_MEDAL_FOR_PILOT_SCHOOL_LESSON(CHAR_MICHAEL,PSC_Landing) = PS_GOLD)
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_FLYLOT",CONV_PRIORITY_HIGH)
									SET_LABEL_AS_TRIGGERED("FLYING SKILL PICK",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_FEWFLY",CONV_PRIORITY_HIGH)
									SET_LABEL_AS_TRIGGERED("FLYING SKILL PICK",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_NOFLY",CONV_PRIORITY_HIGH)
								SET_LABEL_AS_TRIGGERED("FLYING SKILL PICK",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	

SWITCH iprogress

	CASE 0  //setup
		
		bReprintObjective = FALSE
		bGoToSpeach = FALSE

		IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
			FREEZE_ENTITY_POSITION(mission_veh[0].veh,FALSE)
		ENDIF
		
		PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(PLAYER_PED_ID(),KNOCKOFFVEHICLE_NEVER)
		ELSE
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],KNOCKOFFVEHICLE_NEVER)
				SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],PCF_GetOutUndriveableVehicle,TRUE)
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]) 
			//GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_COMBATMG,INFINITE_AMMO,TRUE)
			//GIVE_WEAPON_COMPONENT_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_COMBATMG,WEAPONCOMPONENT_COMBATMG_CLIP_02)
			//SET_PED_INFINITE_AMMO_CLIP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
			//SET_AMMO_IN_CLIP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], WEAPONTYPE_COMBATMG, 100)
			
			SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],PCF_ForcedAim,TRUE)
			IF IS_VEHICLE_SEAT_FREE(mission_veh[MV_CHINOOK].veh,VS_BACK_LEFT)
				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[MV_CHINOOK].veh,VS_BACK_LEFT)
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(mission_veh[MV_SUBMERISIBLE].veh)
			FREEZE_ENTITY_POSITION(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
			SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
			SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
			SET_ENTITY_PROOFS(mission_veh[MV_SUBMERISIBLE].veh,FALSE,FALSE,FALSE,TRUE,FALSE)
			APPLY_FORCE_TO_ENTITY(mission_veh[MV_SUBMERISIBLE].veh, APPLY_TYPE_IMPULSE, <<0,0,-1>>, <<0,0,0>>,0, TRUE, TRUE, FALSE)
		ENDIF

		IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
			IF DOES_ENTITY_EXIST(objSeabed[1])				
				IF IS_ENTITY_ATTACHED(objSeabed[1])
					DETACH_ENTITY(objSeabed[1])
				ENDIF
				SET_OBJECT_PHYSICS_PARAMS(objSeabed[1], -1.0, -1.0, <<-1.0, -1.0, -1.0>>, <<-1.0, -1.0, -1.0>>)
				ATTACH_ENTITY_TO_ENTITY(objSeabed[1],mission_veh[1].veh,1,<<0,-2.0,-1.7>>,<<0,0,90>>,TRUE,FALSE,TRUE)
			ENDIF
		ENDIF

		IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
			FREEZE_ENTITY_POSITION(mission_veh[0].veh,FALSE)
			SET_VEHICLE_FORWARD_SPEED(mission_veh[0].veh,20)
			SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(mission_veh[MV_CHINOOK].veh,0.0)
		ENDIF
		
		//DIALOGUE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			ADD_PED_FOR_DIALOGUE(sSpeech, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],KNOCKOFFVEHICLE_NEVER)
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			ADD_PED_FOR_DIALOGUE(sSpeech, 3, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
		ENDIF

		SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,FALSE)
		
		ALLOW_DIALOGUE_IN_WATER(TRUE)
		CONTROL_FADE_IN(500)
		bGoToSpeach = FALSE
		
		REQUEST_AMBIENT_AUDIO_BANK("PORT_OF_LS_2B_SUB_LIFT")
		
		IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
			PRINT_NOW("DS2_S2MH",DEFAULT_GOD_TEXT_TIME,0)
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
			IF NOT DOES_CARGOBOB_HAVE_PICK_UP_ROPE(mission_veh[0].veh)
				CREATE_PICK_UP_ROPE_FOR_CARGOBOB(mission_veh[0].veh)
			ENDIF
			SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(mission_veh[MV_CHINOOK].veh,imaxRopeLength,IminRopeLength)
		ENDIF
		
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6,"Pick Up Sub") 
		itimer = GET_GAME_TIMER()
		bForceSubToSurface = FALSE
		SET_LABEL_AS_TRIGGERED("DROPPED THE SUB",FALSE)
		SET_LABEL_AS_TRIGGERED("DS2_SUBBLIP",FALSE)
		REQUEST_PTFX_ASSET()
		iTimeTakenToPickUp = GET_GAME_TIMER()
		SET_LABEL_AS_TRIGGERED("FLYING SKILL PICK",FALSE)
		iprogress++
	BREAK
	
	CASE 1
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
				IF NOT DOES_CARGOBOB_HAVE_PICK_UP_ROPE(mission_veh[MV_CHINOOK].veh)
					CREATE_PICK_UP_ROPE_FOR_CARGOBOB(mission_veh[MV_CHINOOK].veh)
					SET_PICKUP_ROPE_LENGTH_FOR_CARGOBOB(mission_veh[MV_CHINOOK].veh,imaxRopeLength,IminRopeLength)
				ENDIF		
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh)
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[ENUM_TO_INT(MV_SUBMERISIBLE)].veh,"DH_2B_SUBMARINE_GROUP")
			ENDIF
		
			SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL,FALSE)
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_MICHAEL,FALSE)
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,TRUE)
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,TRUE)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("CLEAR TC")
				//CLEAR_TIMECYCLE_MODIFIER()
				SET_LABEL_AS_TRIGGERED("CLEAR TC",TRUE)
			ENDIF
			
			IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_S2MH")
				CLEAR_PRINTS()
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("DH_2B_GET_TO_SURFACE")
				STOP_AUDIO_SCENE("DH_2B_GET_TO_SURFACE")
			ENDIF
			
//			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
//				FREEZE_ENTITY_POSITION(mission_veh[1].veh,TRUE)
//			ENDIF
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,GET_ENTITY_COORDS(mission_veh[1].veh),<<0.1,0.1,0.1>>,FALSE,mission_veh[0].veh,"DS2_FLYO","DS2_FLYGI","DS2_FLYGB",TRUE)
					
				ELSE
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_PICK_UP_SUB_CARGO")
							START_AUDIO_SCENE("DH_2B_PICK_UP_SUB_CARGO")
						ENDIF
						IF GET_BLIP_COLOUR(sLocatesData.LocationBlip) != BLIP_COLOUR_BLUE
							SET_BLIP_COLOUR(sLocatesData.LocationBlip,BLIP_COLOUR_BLUE)
						ENDIF
						
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_SUBBLIP")
							IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								SET_BLIP_NAME_FROM_TEXT_FILE(sLocatesData.LocationBlip,"DS2_SUBBLIP") 
								SET_LABEL_AS_TRIGGERED("DS2_SUBBLIP",TRUE)
							ENDIF
						ENDIF
						
						IF NOT DOES_CARGOBOB_HAVE_PICK_UP_ROPE(mission_veh[MV_CHINOOK].veh)
							PRINTSTRING("DOES_CARGOBOB_HAVE_PICK_UP_ROPE- NO")PRINTNL()
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_GRAP_HELP1")
								PRINT_HELP("DS2_GRAP_HELP1")
								SET_LABEL_AS_TRIGGERED("DS2_GRAP_HELP1",TRUE)
							ENDIF
						ELSE
							PRINTSTRING("DOES_CARGOBOB_HAVE_PICK_UP_ROPE - YES")PRINTNL()
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_GRAP_HELP1")
								CLEAR_HELP()
							ENDIF
						ENDIF
						
						IF DOES_CARGOBOB_HAVE_PICK_UP_ROPE(mission_veh[MV_CHINOOK].veh)
						AND REQUEST_AMBIENT_AUDIO_BANK("PORT_OF_LS_2B_SUB_LIFT")
							IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
								CLEAR_PRINTS()
								CLEAR_HELP()
								STABILISE_ENTITY_ATTACHED_TO_HELI(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh,iStabliseLength)
								IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
									IF HAS_PTFX_ASSET_LOADED()
										IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
											ptfxDrips = START_PARTICLE_FX_LOOPED_ON_ENTITY("scr_pls_sub_water_drips", mission_veh[MV_SUBMERISIBLE].veh,<<0,0,0>>,<<0,0,0>>)
											iDripsTimer = GET_GAME_TIMER()
										ENDIF
										
										REMOVE_BLIP(sLocatesData.LocationBlip)
										CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
										SET_VEHICLE_ENGINE_ON(mission_veh[1].veh,FALSE,TRUE)
										
										IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
											PLAY_SOUND_FROM_ENTITY(-1, "Sub_Lift", mission_veh[MV_SUBMERISIBLE].veh,"DOCKS_HEIST_FINALE_2B_SOUNDS")
										ENDIF

										IF IS_AUDIO_SCENE_ACTIVE("DH_2B_ATTACH_SUB")
											STOP_AUDIO_SCENE("DH_2B_ATTACH_SUB")
										ENDIF
										
										REPLAY_RECORD_BACK_FOR_TIME(8.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
										
										bGoToSpeach = FALSE
										IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
											IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
												IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh)
													SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
													SET_VEHICLE_ENGINE_ON(mission_veh[MV_SUBMERISIBLE].veh,FALSE,TRUE)
												ENDIF
											ENDIF
										ENDIF
										KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
										iprogress++	
									ENDIF
								ENDIF
							ELSE
								IF MANAGE_MY_TIMER(itimer,30000)
									IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P6")
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P6",CONV_PRIORITY_HIGH)
												SET_LABEL_AS_TRIGGERED("DS2A_P6",TRUE)
												iDialogueTimer = GET_GAME_TIMER()
											ENDIF
										ENDIF
									ELSE
										IF MANAGE_MY_TIMER(iDialogueTimer,15000 + GET_RANDOM_INT_IN_RANGE(1000,5000))
											SET_LABEL_AS_TRIGGERED("DS2A_P6",TRUE)
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF IS_IT_SAFE_TO_PLAY_DIALOG()
								IF bGoToSpeach = FALSE
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P6",CONV_PRIORITY_HIGH)
											bGoToSpeach = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
							
					IF bGoToSpeach = TRUE
						//MANAGE PAUSE DIALOGUE
						IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
							IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								CLEAR_PRINTS()
								PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
							ENDIF
						//Play conversation
						ELSE
							IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_FLYGB")
									CLEAR_PRINTS()
								ENDIF
						    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				FREEZE_ENTITY_POSITION(mission_veh[1].veh,FALSE)
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("ASSIGN MICHAEL TASK")
				vFlyTo = GET_ENTITY_COORDS(mission_veh[MV_SUBMERISIBLE].veh)
				vFlyTo.z = 60.8358
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]) 
						TASK_HELI_MISSION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],mission_veh[MV_CHINOOK].veh,NULL,NULL,vFlyTo,MISSION_GOTO,50,2.0,-1,100,60)
						SET_LABEL_AS_TRIGGERED("ASSIGN MICHAEL TASK",TRUE)
					ENDIF
				ENDIF
			ELSE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P5a")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P5a",CONV_PRIORITY_HIGH)
							SET_LABEL_AS_TRIGGERED("DS2A_P5a",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF

	BREAK
	
	CASE 2
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
		AND IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_CONNECT")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_CONNECT",CONV_PRIORITY_HIGH)
						SET_LABEL_AS_TRIGGERED("DS2A_CONNECT",TRUE)
					ENDIF
				ENDIF
			ELSE
				iprogress++ 
			ENDIF
		ENDIF
	BREAK
	
	CASE 3
		IF TRIGGER_MUSIC_EVENT("DH2B_GOT_SUB_2ND")
			IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				REMOVE_BLIP(sLocatesData.LocationBlip)
			ENDIF
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR]) 
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh)
						IF IS_VEHICLE_SEAT_FREE(mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
							SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh,VS_DRIVER)
							SET_ENTITY_VISIBLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_AUDIO_SCENE_ACTIVE("DH_2B_PICK_UP_SUB_CARGO")
				STOP_AUDIO_SCENE("DH_2B_PICK_UP_SUB_CARGO")
			ENDIF
			//MANAGE_FREE_HOTSWAP()
			//IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
			CLEAR_HELP()
			CLEAR_PRINTS()
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
				PRINT_NOW("DS2_S2MH",DEFAULT_GOD_TEXT_TIME,0)
			ENDIF
			SET_SELECTOR_PED_PRIORITY(sSelectorPeds, SELECTOR_PED_MICHAEL, SELECTOR_PED_TREVOR, SELECTOR_PED_FRANKLIN)
			IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
			AND IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				FREEZE_ENTITY_POSITION(mission_veh[1].veh,FALSE)
				FREEZE_ENTITY_POSITION(mission_veh[0].veh,FALSE)
				PRINTSTRING("PICKING UP AND UNFREEZING VEHICLES")PRINTNL()
				SET_VEHICLE_ENGINE_ON(mission_veh[MV_SUBMERISIBLE].veh,FALSE,TRUE)
				iprogress++	
			ENDIF
		ENDIF
		
		//ENDIF
	BREAK
	
	CASE 4
		MANAGE_FREE_HOTSWAP(FALSE, FALSE, TRUE, TRUE)
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		AND iSwitchStage = 0
			
				SET_VEHICLE_NOISE_BASED_ON_FLIGHT_STAT(mission_veh[MV_CHINOOK].veh,GET_SP_PLAYER_PED_STAT_VALUE(CHAR_MICHAEL, PS_FLYING_ABILITY))
				CLEAR_HELP()
				CLEAR_PRINTS()
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				bTriggerDialogue = FALSE
				iprogress = 0
				bReprintObjective = FALSE
				emissionstage = MISSION_STAGE_DEFEND_AGAINST_MERRYWEATHER
		ENDIF
	BREAK
	
ENDSWITCH

ENDPROC

PROC MANAGE_BLOW_UP_VEHICLE(INT iVehicleNum, BOOL bTough = FALSE)
	BOOL bHasBeenDamaged = FALSE
	IF DOES_ENTITY_EXIST(mission_veh[iVehicleNum].veh)
		IF NOT IS_ENTITY_DEAD(mission_veh[iVehicleNum].veh)	
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[iVehicleNum].veh,PLAYER_PED_ID())
				 bHasBeenDamaged = TRUE
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[iVehicleNum].veh,sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					 bHasBeenDamaged = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		

	
	SWITCH mission_veh[iVehicleNum].iDamage
		
		CASE 0
			mission_veh[iVehicleNum].iDamage ++
		BREAK

		CASE 1
			IF bHasBeenDamaged
				IF GET_ENTITY_HEALTH(mission_veh[iVehicleNum].veh) < 1000
					IF bTough
						SET_ENTITY_HEALTH(mission_veh[iVehicleNum].veh,800)
					ELSE
						SET_ENTITY_HEALTH(mission_veh[iVehicleNum].veh,500)
					ENDIF
					mission_veh[iVehicleNum].iDamage ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF bHasBeenDamaged
				IF GET_ENTITY_HEALTH(mission_veh[iVehicleNum].veh) < 500
					IF bTough
						SET_ENTITY_HEALTH(mission_veh[iVehicleNum].veh,450)
					ELSE
						SET_ENTITY_HEALTH(mission_veh[iVehicleNum].veh,400)
					ENDIF
					mission_veh[iVehicleNum].iDamage ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF bHasBeenDamaged
				IF GET_ENTITY_HEALTH(mission_veh[iVehicleNum].veh) < 400
					IF bTough
						SET_ENTITY_HEALTH(mission_veh[iVehicleNum].veh,350)
					ELSE
						SET_ENTITY_HEALTH(mission_veh[iVehicleNum].veh,300)
					ENDIF
					mission_veh[iVehicleNum].iDamage ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF bHasBeenDamaged
				IF GET_ENTITY_HEALTH(mission_veh[iVehicleNum].veh) < 300
					IF bTough
						SET_ENTITY_HEALTH(mission_veh[iVehicleNum].veh,250)
					ELSE
						SET_ENTITY_HEALTH(mission_veh[iVehicleNum].veh,200)
					ENDIF
					mission_veh[iVehicleNum].iDamage ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF NOT IS_ENTITY_DEAD(mission_veh[iVehicleNum].veh)
				IF GET_ENTITY_HEALTH(mission_veh[iVehicleNum].veh) < 200
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[iVehicleNum].veh)
						STOP_PLAYBACK_RECORDED_VEHICLE(mission_veh[iVehicleNum].veh)
					ENDIF
					SET_VEHICLE_PETROL_TANK_HEALTH(mission_veh[iVehicleNum].veh,-100)
					SET_VEHICLE_ENGINE_HEALTH(mission_veh[iVehicleNum].veh,-500)
					mission_veh[iVehicleNum].iDamageTimer = GET_GAME_TIMER()
					mission_veh[iVehicleNum].iDamage ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF NOT IS_ENTITY_DEAD(mission_veh[iVehicleNum].veh)
				IF NOT MANAGE_MY_TIMER(mission_veh[iVehicleNum].iDamageTimer,600)
					IF GET_RANDOM_BOOL()
						APPLY_FORCE_TO_ENTITY(mission_veh[iVehicleNum].veh, APPLY_TYPE_FORCE, <<GET_RANDOM_FLOAT_IN_RANGE(33.0,48.0), 0.0, 0.0>>, <<0.0, 1.8, 0.0>>, 0, TRUE, TRUE, TRUE)
						EXPLODE_VEHICLE(mission_veh[iVehicleNum].veh)
					ENDIF
					mission_veh[iVehicleNum].iDamageTimer = GET_GAME_TIMER()
					mission_veh[iVehicleNum].iDamage ++
				ENDIF
			ENDIF

		BREAK
		
		CASE 7
			IF NOT IS_ENTITY_DEAD(mission_veh[iVehicleNum].veh)
				IF NOT MANAGE_MY_TIMER(mission_veh[iVehicleNum].iDamageTimer,600)
					APPLY_FORCE_TO_ENTITY(mission_veh[iVehicleNum].veh, APPLY_TYPE_FORCE, <<GET_RANDOM_FLOAT_IN_RANGE(33.0,48.0), 0.0, 0.0>>, <<0.0, 1.8, 0.0>>, 0, TRUE, TRUE, TRUE)
					EXPLODE_VEHICLE(mission_veh[iVehicleNum].veh)
					mission_veh[iVehicleNum].iDamageTimer = GET_GAME_TIMER()
					mission_veh[iVehicleNum].iDamage ++
				ENDIF
			ELSE
				mission_veh[iVehicleNum].iDamageTimer = GET_GAME_TIMER()
				mission_veh[iVehicleNum].iDamage ++
			ENDIF

		BREAK
		
		CASE 8
			IF MANAGE_MY_TIMER(mission_veh[iVehicleNum].iDamageTimer,GET_RANDOM_INT_IN_RANGE(600,2000))
				IF NOT IS_ENTITY_DEAD(mission_veh[iVehicleNum].veh)
					IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(mission_veh[iVehicleNum].veh)
						EXPLODE_VEHICLE(mission_veh[iVehicleNum].veh)
						mission_veh[iVehicleNum].iDamage ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH
	
	MISSION_VEHICLES thisVehicle = INT_TO_ENUM(MISSION_VEHICLES,iVehicleNum)
	IF thisVehicle = MV_MERRY_WEATHER_CHASE_HELI_RHS_1
	OR thisVehicle = MV_MERRY_WEATHER_CHASE_HELI_REAR_1
	OR thisVehicle = MV_MERRY_WEATHER_CHASE_HELI_REAR_2
	OR thisVehicle = MV_MERRY_WEATHER_CHASE_HELI_REAR_3
	OR thisVehicle = MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1
	OR thisVehicle = MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2
	OR thisVehicle = MV_MERRY_WEATHER_HELI_1
	OR thisVehicle = MV_MERRY_WEATHER_HELI_2
	OR thisVehicle = MV_MERRY_WEATHER_HELI_3
		IF IS_VEHICLE_DRIVEABLE(mission_veh[iVehicleNum].veh)
			BOOL bExplode = FALSE
			FLOAT fDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(mission_veh[iVehicleNum].veh,FALSE))
			IF fDistance > 1000
				IF bVehicleHasBeenClose[iVehicleNum]
					bExplode = TRUE
				ENDIF
			ELIF fDistance > 350
				IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(mission_veh[iVehicleNum].veh)
					bExplode = TRUE
				ENDIF
			ELIF fDistance < 400
				bVehicleHasBeenClose[iVehicleNum] = TRUE
			ENDIF
			
			IF bExplode	
				EXPLODE_VEHICLE(mission_veh[iVehicleNum].veh)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

// do the vehicle headings match?
FUNC BOOL DO_VEHICLE_HEADINGS_MATCH(VEHICLE_INDEX vehicle1Veh, VEHICLE_INDEX vehicle2Veh, FLOAT allowAngleFrom0 = 30.0)
	IF IS_VEHICLE_DRIVEABLE(vehicle1Veh)
	AND IS_VEHICLE_DRIVEABLE(vehicle2Veh)	
		// must have a similar heading 
		FLOAT fVehicle1Heading = NORMALISE_HEADING(GET_ENTITY_HEADING(vehicle1Veh))
		FLOAT fVehicle2Heading = NORMALISE_HEADING(GET_ENTITY_HEADING(vehicle2Veh))
		FLOAT fHeadingDiff = fVehicle1Heading - fVehicle2Heading
		IF fHeadingDiff < 0 
			fHeadingDiff *= -1
		ENDIF
		IF fHeadingDiff <= allowAngleFrom0
		OR fHeadingDiff >= 360 - allowAngleFrom0
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC APPLY_CATCHUP_FORCE_TO_BOAT(VEHICLE_INDEX veh)
	IF IS_VEHICLE_DRIVEABLE(veh)
	AND IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
		IF DO_VEHICLE_HEADINGS_MATCH(veh, mission_veh[MV_CHINOOK].veh, 25)
			IF IS_ENTITY_IN_WATER(veh)
				VECTOR vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, GET_ENTITY_COORDS(veh))
				IF vOffset.y < -3
					APPLY_FORCE_TO_ENTITY(veh, APPLY_TYPE_EXTERNAL_FORCE, <<0,20,0>>, <<0,0,0>>, 0, TRUE, TRUE, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

VECTOR vChopperPos
VECTOR vHeliHeight

PROC MANAGE_MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1(INT &iThisProgress)
	
	VECTOR vTempSpawn
	
	
	
	SWITCH iThisProgress
		CASE 0
			REQUEST_MODEL(DINGHY)
			REQUEST_MODEL(S_M_Y_BlackOps_01)
			IF HAS_MODEL_LOADED(DINGHY)
			AND HAS_MODEL_LOADED(S_M_Y_BlackOps_01)
				vTempSpawn = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<-95, -50, 0>>)//<<-215,-50,0>>)
				vTempSpawn.z = 0.62
				CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1), DINGHY,vTempSpawn, mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].fThisHeading,S_M_Y_BlackOps_01,TRUE,0,3,TRUE)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh)
						
						SET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh,vTempSpawn)
						SET_ENTITY_HEADING(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh,GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh))
					ENDIF
				ENDIF
				SET_ENTITY_LOD_DIST(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh,1500)
				SET_VEHICLE_COLOURS(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh,0,0)
				SET_BOAT_SINKS_WHEN_WRECKED(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh,TRUE)
				SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh,TRUE)
				IF NOT IS_PED_INJURED(driver[0].ped)
					SET_PED_CONFIG_FLAG(driver[0].ped, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(driver[0].ped, PCF_RunFromFiresAndExplosions, FALSE)
					ADD_DEADPOOL_TRIGGER(driver[0].ped, DH2B_KILLS)
				ENDIF				
				IF NOT IS_PED_INJURED(driver[1].ped)
					SET_PED_CONFIG_FLAG(driver[1].ped, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(driver[1].ped, PCF_RunFromFiresAndExplosions, FALSE)
					GIVE_WEAPON_TO_PED(driver[1].ped,WEAPONTYPE_MICROSMG,INFINITE_AMMO,TRUE)
					ADD_DEADPOOL_TRIGGER(driver[1].ped, DH2B_KILLS)
				ENDIF
				IF NOT IS_PED_INJURED(driver[2].ped)
					SET_PED_CONFIG_FLAG(driver[2].ped, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(driver[2].ped, PCF_RunFromFiresAndExplosions, FALSE)
					GIVE_WEAPON_TO_PED(driver[2].ped,WEAPONTYPE_MICROSMG,INFINITE_AMMO,TRUE)
					ADD_DEADPOOL_TRIGGER(driver[2].ped, DH2B_KILLS)
				ENDIF
				IF NOT IS_PED_INJURED(driver[3].ped)
					SET_PED_CONFIG_FLAG(driver[3].ped, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(driver[3].ped, PCF_RunFromFiresAndExplosions, FALSE)
					GIVE_WEAPON_TO_PED(driver[3].ped,WEAPONTYPE_MICROSMG,INFINITE_AMMO,TRUE)
					ADD_DEADPOOL_TRIGGER(driver[3].ped, DH2B_KILLS)
				ENDIF
				bWarpedDinghies = FALSE
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh,"DH_2B_ENEMY_BOATS_GROUP")
				iThisProgress ++
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(driver[0].ped)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh)
						IF vHeliHeight.z < 50
							DO_PERSISTENT_CHASE(driver[0].ped,mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh,TRUE, mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].iThisTimer,1000,55,-70,30,0)
						ELSE
							DO_PERSISTENT_CHASE(driver[0].ped,mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh,TRUE, mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].iThisTimer,1000,55,-90,30,0)
						ENDIF
						APPLY_CATCHUP_FORCE_TO_BOAT(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh)
						
						IF NOT IS_PED_INJURED(driver[1].ped)
							IF IS_PED_IN_ANY_VEHICLE(driver[1].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[1].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[1].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(driver[1].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
									TASK_COMBAT_PED(driver[1].ped,PLAYER_PED_ID())
								ENDIF
							ENDIF
						ENDIF
						IF NOT IS_PED_INJURED(driver[2].ped)
							IF IS_PED_IN_ANY_VEHICLE(driver[2].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[2].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[2].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(driver[2].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
									TASK_COMBAT_PED(driver[2].ped,PLAYER_PED_ID())
								ENDIF
							ENDIF
						ENDIF
						IF NOT IS_PED_INJURED(driver[3].ped)
							IF IS_PED_IN_ANY_VEHICLE(driver[3].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[3].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[3].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(driver[3].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
									TASK_COMBAT_PED(driver[3].ped,PLAYER_PED_ID())
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC MANAGE_MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2(INT &iThisProgress)
	
	VECTOR vTempSpawn

	SWITCH iThisProgress
		CASE 0
			REQUEST_MODEL(DINGHY)
			REQUEST_MODEL(S_M_Y_BlackOps_01)
			IF HAS_MODEL_LOADED(DINGHY)
			AND HAS_MODEL_LOADED(S_M_Y_BlackOps_01)
				vTempSpawn = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<-100, -55, 0>>)//<<-235,-55,0>>)
				vTempSpawn.z = 0.62
				CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2), DINGHY,vTempSpawn, mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].fThisHeading,S_M_Y_BlackOps_01,TRUE,4,3,TRUE)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh)
						
						SET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh,vTempSpawn)
						SET_ENTITY_HEADING(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh,GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh))
					ENDIF
				ENDIF
				SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh,TRUE)
				SET_ENTITY_LOD_DIST(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh,1500)
				SET_VEHICLE_COLOURS(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh,0,0)
				SET_BOAT_SINKS_WHEN_WRECKED(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh,TRUE)
				IF NOT IS_PED_INJURED(driver[4].ped)
					SET_PED_CONFIG_FLAG(driver[4].ped, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(driver[4].ped, PCF_RunFromFiresAndExplosions, FALSE)
					ADD_DEADPOOL_TRIGGER(driver[4].ped, DH2B_KILLS)
				ENDIF				
				IF NOT IS_PED_INJURED(driver[5].ped)
					SET_PED_CONFIG_FLAG(driver[5].ped, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(driver[5].ped, PCF_RunFromFiresAndExplosions, FALSE)
					GIVE_WEAPON_TO_PED(driver[5].ped,WEAPONTYPE_MICROSMG,INFINITE_AMMO,TRUE)
					ADD_DEADPOOL_TRIGGER(driver[5].ped, DH2B_KILLS)
				ENDIF
				IF NOT IS_PED_INJURED(driver[6].ped)
					SET_PED_CONFIG_FLAG(driver[6].ped, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(driver[6].ped, PCF_RunFromFiresAndExplosions, FALSE)
					GIVE_WEAPON_TO_PED(driver[6].ped,WEAPONTYPE_MICROSMG,INFINITE_AMMO,TRUE)
					ADD_DEADPOOL_TRIGGER(driver[6].ped, DH2B_KILLS)
				ENDIF
				IF NOT IS_PED_INJURED(driver[7].ped)
					SET_PED_CONFIG_FLAG(driver[7].ped, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(driver[7].ped, PCF_RunFromFiresAndExplosions, FALSE)
					GIVE_WEAPON_TO_PED(driver[7].ped,WEAPONTYPE_MICROSMG,INFINITE_AMMO,TRUE)
					ADD_DEADPOOL_TRIGGER(driver[7].ped, DH2B_KILLS)
				ENDIF
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh,"DH_2B_ENEMY_BOATS_GROUP")
				iThisProgress ++
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(driver[4].ped)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh)
						IF vHeliHeight.z < 50
							DO_PERSISTENT_CHASE(driver[4].ped,mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh,TRUE, mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].iThisTimer,1000,55,-90,55,0)
						ELSE
							DO_PERSISTENT_CHASE(driver[4].ped,mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh,TRUE, mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].iThisTimer,1000,55,-95,55,0)
						ENDIF
						APPLY_CATCHUP_FORCE_TO_BOAT(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh)
						
						
						IF NOT IS_PED_INJURED(driver[5].ped)
							IF IS_PED_IN_ANY_VEHICLE(driver[5].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[5].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[5].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(driver[5].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
									TASK_COMBAT_PED(driver[5].ped,PLAYER_PED_ID())
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(driver[6].ped)
							IF IS_PED_IN_ANY_VEHICLE(driver[6].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[6].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[6].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(driver[6].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
									TASK_COMBAT_PED(driver[6].ped,PLAYER_PED_ID())
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(driver[7].ped)
							IF IS_PED_IN_ANY_VEHICLE(driver[7].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[7].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[7].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(driver[7].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
									TASK_COMBAT_PED(driver[7].ped,PLAYER_PED_ID())
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC MANAGE_MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1(INT &iThisProgress)
	
	VECTOR vTempSpawn
	
	SWITCH iThisProgress
		CASE 0
			REQUEST_MODEL(DINGHY)
			REQUEST_MODEL(S_M_Y_BlackOps_01)
			IF HAS_MODEL_LOADED(DINGHY)
			AND HAS_MODEL_LOADED(S_M_Y_BlackOps_01)
				CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1), DINGHY,mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].vThisLocation, mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].fThisHeading,S_M_Y_BlackOps_01,TRUE,8,3,TRUE)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh)
						vTempSpawn = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<110,-60,0>>)
						vTempSpawn.z = 0.62
						SET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh,vTempSpawn)
						SET_ENTITY_HEADING(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh,GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh))
					ENDIF
				ENDIF
				SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh,TRUE)
				SET_ENTITY_LOD_DIST(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh,1500)
				SET_VEHICLE_COLOURS(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh,0,0)
				SET_BOAT_SINKS_WHEN_WRECKED(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh,TRUE)
				IF NOT IS_PED_INJURED(driver[8].ped)
					SET_PED_CONFIG_FLAG(driver[8].ped, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(driver[8].ped, PCF_RunFromFiresAndExplosions, FALSE)
					ADD_DEADPOOL_TRIGGER(driver[8].ped, DH2B_KILLS)
				ENDIF
				IF NOT IS_PED_INJURED(driver[9].ped)
					SET_PED_CONFIG_FLAG(driver[9].ped, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(driver[9].ped, PCF_RunFromFiresAndExplosions, FALSE)				
					GIVE_WEAPON_TO_PED(driver[9].ped,WEAPONTYPE_MICROSMG,INFINITE_AMMO,TRUE)
					ADD_DEADPOOL_TRIGGER(driver[9].ped, DH2B_KILLS)
				ENDIF
				IF NOT IS_PED_INJURED(driver[10].ped)
					SET_PED_CONFIG_FLAG(driver[10].ped, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(driver[10].ped, PCF_RunFromFiresAndExplosions, FALSE)
					GIVE_WEAPON_TO_PED(driver[10].ped,WEAPONTYPE_MICROSMG,INFINITE_AMMO,TRUE)
					ADD_DEADPOOL_TRIGGER(driver[10].ped, DH2B_KILLS)
				ENDIF
				IF NOT IS_PED_INJURED(driver[11].ped)
					SET_PED_CONFIG_FLAG(driver[11].ped, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(driver[11].ped, PCF_RunFromFiresAndExplosions, FALSE)				
					GIVE_WEAPON_TO_PED(driver[11].ped,WEAPONTYPE_MICROSMG,INFINITE_AMMO,TRUE)
					ADD_DEADPOOL_TRIGGER(driver[11].ped, DH2B_KILLS)
				ENDIF
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh,"DH_2B_ENEMY_BOATS_GROUP")
				iThisProgress ++
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(driver[8].ped)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh)
						DO_PERSISTENT_CHASE(driver[8].ped,mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh,TRUE, mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].iThisTimer,90,55,80,50,0)
						APPLY_CATCHUP_FORCE_TO_BOAT(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh)

//						IF GET_SCRIPT_TASK_STATUS(driver[8].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
//							TASK_COMBAT_PED(driver[8].ped,PLAYER_PED_ID())
//						ENDIF
						IF NOT IS_PED_INJURED(driver[9].ped)
							IF IS_PED_IN_ANY_VEHICLE(driver[9].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[9].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[9].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(driver[9].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
									TASK_COMBAT_PED(driver[9].ped,PLAYER_PED_ID())
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(driver[10].ped)
							IF IS_PED_IN_ANY_VEHICLE(driver[10].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[10].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[10].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(driver[10].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
									TASK_COMBAT_PED(driver[10].ped,PLAYER_PED_ID())
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(driver[11].ped)
							IF IS_PED_IN_ANY_VEHICLE(driver[11].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[11].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[11].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(driver[11].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
									TASK_COMBAT_PED(driver[11].ped,PLAYER_PED_ID())
								ENDIF
							ENDIF
						ENDIF
						
						
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC MANAGE_MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2(INT &iThisProgress)
	
	VECTOR vTempSpawn
	
	SWITCH iThisProgress
		CASE 0
			REQUEST_MODEL(DINGHY)
			REQUEST_MODEL(S_M_Y_BlackOps_01)
			IF HAS_MODEL_LOADED(DINGHY)
			AND HAS_MODEL_LOADED(S_M_Y_BlackOps_01)
				CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2), DINGHY,mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].vThisLocation, mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].fThisHeading,S_M_Y_BlackOps_01,TRUE,12,3,TRUE)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh)
						vTempSpawn = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<140,-65,0>>)
						vTempSpawn.z = 0.62
						SET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh,vTempSpawn)
						SET_ENTITY_HEADING(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh,GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh))
					ENDIF
				ENDIF
				SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh,TRUE)
				SET_ENTITY_LOD_DIST(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh,1500)
				SET_VEHICLE_COLOURS(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh,0,0)
				SET_BOAT_SINKS_WHEN_WRECKED(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh,TRUE)
				IF NOT IS_PED_INJURED(driver[12].ped)
					SET_PED_CONFIG_FLAG(driver[12].ped, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(driver[12].ped, PCF_RunFromFiresAndExplosions, FALSE)	
					ADD_DEADPOOL_TRIGGER(driver[12].ped, DH2B_KILLS)
				ENDIF				
				IF NOT IS_PED_INJURED(driver[13].ped)
					SET_PED_CONFIG_FLAG(driver[13].ped, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(driver[13].ped, PCF_RunFromFiresAndExplosions, FALSE)	
					GIVE_WEAPON_TO_PED(driver[13].ped,WEAPONTYPE_MICROSMG,INFINITE_AMMO,TRUE)
					ADD_DEADPOOL_TRIGGER(driver[13].ped, DH2B_KILLS)
				ENDIF
				IF NOT IS_PED_INJURED(driver[14].ped)
					SET_PED_CONFIG_FLAG(driver[14].ped, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(driver[14].ped, PCF_RunFromFiresAndExplosions, FALSE)	
					GIVE_WEAPON_TO_PED(driver[14].ped,WEAPONTYPE_MICROSMG,INFINITE_AMMO,TRUE)
					ADD_DEADPOOL_TRIGGER(driver[14].ped, DH2B_KILLS)
				ENDIF
				IF NOT IS_PED_INJURED(driver[15].ped)
					SET_PED_CONFIG_FLAG(driver[15].ped, PCF_GetOutBurningVehicle, FALSE)
					SET_PED_CONFIG_FLAG(driver[15].ped, PCF_RunFromFiresAndExplosions, FALSE)	
					GIVE_WEAPON_TO_PED(driver[15].ped,WEAPONTYPE_MICROSMG,INFINITE_AMMO,TRUE)
					ADD_DEADPOOL_TRIGGER(driver[15].ped, DH2B_KILLS)
				ENDIF
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh,"DH_2B_ENEMY_BOATS_GROUP")
				iThisProgress ++
			ENDIF
		BREAK
		
		CASE 1
			IF NOT IS_PED_INJURED(driver[12].ped)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh)
						DO_PERSISTENT_CHASE(driver[12].ped,mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh,TRUE, mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].iThisTimer,90,55,100,30,0)
						APPLY_CATCHUP_FORCE_TO_BOAT(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh)
//						IF GET_SCRIPT_TASK_STATUS(driver[12].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
//							TASK_COMBAT_PED(driver[12].ped,PLAYER_PED_ID())
//						ENDIF
						IF NOT IS_PED_INJURED(driver[13].ped)
							IF IS_PED_IN_ANY_VEHICLE(driver[13].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[13].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[13].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(driver[13].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
									TASK_COMBAT_PED(driver[13].ped,PLAYER_PED_ID())
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(driver[14].ped)
							IF IS_PED_IN_ANY_VEHICLE(driver[14].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[14].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[14].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(driver[14].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
									TASK_COMBAT_PED(driver[14].ped,PLAYER_PED_ID())
								ENDIF
							ENDIF
						ENDIF
						
						IF NOT IS_PED_INJURED(driver[15].ped)
							IF IS_PED_IN_ANY_VEHICLE(driver[15].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[15].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[15].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ELSE
								IF GET_SCRIPT_TASK_STATUS(driver[15].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
									TASK_COMBAT_PED(driver[15].ped,PLAYER_PED_ID())
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
ENDPROC


PROC MANAGE_MV_MERRY_WEATHER_CHASE_HELI_RHS_1(INT &iThisProgress)

	SWITCH iThisProgress
		CASE 0
			REQUEST_MODEL(BUZZARD)
			REQUEST_MODEL(S_M_Y_BlackOps_01)
			IF HAS_MODEL_LOADED(BUZZARD)
			AND HAS_MODEL_LOADED(S_M_Y_BlackOps_01)
				CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_HELI_RHS_1),BUZZARD,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].vThisLocation, mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].fThisHeading,S_M_Y_BlackOps_01,TRUE,16,3,TRUE)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh)
					SET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<95,-95,65>>))
					SET_ENTITY_HEADING(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh,GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh))
				ENDIF
				////SET_PED_IS_AVOIDED_BY_OTHERS(driver[16].ped,FALSE)
				//SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh,TRUE)
				SET_VEHICLE_BOOST_ACTIVE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh, TRUE)
				DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_VEHICLE_ROCKET,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh,driver[16].ped)
				SET_CURRENT_PED_VEHICLE_WEAPON(driver[16].ped, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[16].ped,TRUE)
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh,"DH_2B_HELI_GROUP_01")
				mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].iGetToNewPositionTimer = GET_GAME_TIMER()
				bVehicleHasBeenClose[MV_MERRY_WEATHER_CHASE_HELI_RHS_1] = FALSE
				ADD_DEADPOOL_TRIGGER(driver[16].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[17].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[18].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[19].ped, DH2B_KILLS)
				iThisProgress ++
			ENDIF
		BREAK
		
		CASE 1
			SET_HELI_TURBULENCE_SCALAR(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh,0)
		
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].iThisTimer,50)
				IF NOT IS_PED_INJURED(driver[16].ped)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)					
							TASK_HELI_MISSION(driver[16].ped,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh, NULL, NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, <<30, 17, 5>>), MISSION_GOTO, 60.0, 0.1, -1, ROUND(vChopperPos.z)+ 10, 25, -1, HF_HEIGHTMAPONLYAVOIDANCE)
						ENDIF
						
//							IF GET_SCRIPT_TASK_STATUS(driver[16].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
//								TASK_COMBAT_PED(driver[16].ped,PLAYER_PED_ID())
//							ENDIF
						IF NOT IS_PED_INJURED(driver[17].ped)
							IF GET_SCRIPT_TASK_STATUS(driver[17].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									TASK_DRIVE_BY(driver[17].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ELSE
									TASK_DRIVE_BY(driver[17].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,100,TRUE)
								ENDIF
								////SET_PED_IS_AVOIDED_BY_OTHERS(driver[17].ped,FALSE)
							ENDIF
						ENDIF
						IF NOT IS_PED_INJURED(driver[18].ped)
							IF GET_SCRIPT_TASK_STATUS(driver[18].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									TASK_DRIVE_BY(driver[18].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ELSE
									TASK_DRIVE_BY(driver[18].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,100,TRUE)
								ENDIF
								////SET_PED_IS_AVOIDED_BY_OTHERS(driver[18].ped,FALSE)
							ENDIF
						ENDIF
						IF NOT IS_PED_INJURED(driver[19].ped)
							IF GET_SCRIPT_TASK_STATUS(driver[19].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
									TASK_DRIVE_BY(driver[19].ped,PLAYER_PED_ID(),NULL,<<0,0,0>>,-1,30,TRUE)
								ELSE
									TASK_DRIVE_BY(driver[19].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,100,TRUE)
								ENDIF
								////SET_PED_IS_AVOIDED_BY_OTHERS(driver[19].ped,FALSE)
							ENDIF
						ENDIF
						mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].iThisTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
			
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].iGetToNewPositionTimer,5000)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh,PLAYER_PED_ID())
						mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].iGetToNewPositionTimer = GET_GAME_TIMER()
						iThisProgress ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].iThisTimer,50)
				IF NOT IS_PED_INJURED(driver[16].ped)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
							TASK_HELI_MISSION(driver[16].ped,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh, NULL, NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, <<30, 35, 5>>), MISSION_GOTO, 60.0, 0.1, -1, ROUND(vChopperPos.z)+ 10, 25, -1, HF_HEIGHTMAPONLYAVOIDANCE)
//							IF GET_SCRIPT_TASK_STATUS(driver[16].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
//								TASK_COMBAT_PED(driver[16].ped,PLAYER_PED_ID())
//							ENDIF
							IF NOT IS_PED_INJURED(driver[17].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[17].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[17].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[18].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[18].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[18].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[19].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[19].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[19].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ENDIF
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].iThisTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			/*
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].iGetToNewPositionTimer,5000)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh,PLAYER_PED_ID())
						mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].iGetToNewPositionTimer = GET_GAME_TIMER()
						iThisProgress --
					ENDIF
				ENDIF
			ENDIF
			*/
		
		BREAK
	ENDSWITCH
ENDPROC

PROC MANAGE_MV_MERRY_WEATHER_CHASE_HELI_REAR_1(INT &iThisProgress)

	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
		SET_HELI_TURBULENCE_SCALAR(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh,0.0)
	ENDIF	

	SWITCH iThisProgress
		CASE 0
			REQUEST_MODEL(BUZZARD)
			REQUEST_MODEL(S_M_Y_BlackOps_01)
			IF HAS_MODEL_LOADED(BUZZARD)
			AND HAS_MODEL_LOADED(S_M_Y_BlackOps_01)
				CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_HELI_REAR_1),BUZZARD,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].vThisLocation, mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].fThisHeading,S_M_Y_BlackOps_01,TRUE,20,3,TRUE)
				SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh,TRUE)
				DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_VEHICLE_ROCKET,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh,driver[20].ped)
				SET_CURRENT_PED_VEHICLE_WEAPON(driver[20].ped, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[20].ped,TRUE)
				SET_VEHICLE_BOOST_ACTIVE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh, TRUE)
				//SET_PED_IS_AVOIDED_BY_OTHERS(driver[20].ped,FALSE)
				mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].iGetToNewPositionTimer = GET_GAME_TIMER()
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh,"DH_2B_HELI_GROUP_03")
				bVehicleHasBeenClose[MV_MERRY_WEATHER_CHASE_HELI_REAR_1] = FALSE
				ADD_DEADPOOL_TRIGGER(driver[20].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[21].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[22].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[23].ped, DH2B_KILLS)
				iThisProgress ++
			ENDIF
		BREAK
		
		CASE 1
	
		
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].iThisTimer,50)
				IF NOT IS_PED_INJURED(driver[20].ped)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
							
							IF NOT IS_PED_INJURED(driver[21].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[21].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									SET_PED_ACCURACY(driver[21].ped,50)
									TASK_DRIVE_BY(driver[21].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[21].ped,FALSE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[22].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[22].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									SET_PED_ACCURACY(driver[22].ped,50)
									TASK_DRIVE_BY(driver[22].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[22].ped,FALSE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[23].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[23].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									SET_PED_ACCURACY(driver[23].ped,5)
									TASK_DRIVE_BY(driver[23].ped,PLAYER_PED_ID(),NULL,<<0,0,0>>,-1,30,TRUE)
									//TASK_DRIVE_BY(driver[23].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[23].ped,FALSE)
								ENDIF
							ENDIF
				
							TASK_HELI_MISSION(driver[20].ped,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh, NULL, NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, <<-35, 55, 15>>), MISSION_GOTO, 60.0, 0.1, GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh), ROUND(vChopperPos.z) + 10, 20, -1, HF_HEIGHTMAPONLYAVOIDANCE)
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].iThisTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].iGetToNewPositionTimer,2000)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh,PLAYER_PED_ID())
						mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].iGetToNewPositionTimer = GET_GAME_TIMER()
						iThisProgress ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].iThisTimer,50)
				IF NOT IS_PED_INJURED(driver[20].ped)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
							TASK_HELI_MISSION(driver[20].ped,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh, NULL, NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, <<-35, 60, 15>>), MISSION_GOTO, 60.0, 0.1, GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh), ROUND(vChopperPos.z)+ 10, 20, -1, HF_HEIGHTMAPONLYAVOIDANCE)
//							IF GET_SCRIPT_TASK_STATUS(driver[20].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
//								TASK_DRIVE_BY(driver[20].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
//							ENDIF
							IF NOT IS_PED_INJURED(driver[21].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[21].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[21].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[22].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[22].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[22].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[23].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[23].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[23].ped,PLAYER_PED_ID(),NULL,<<0,0,0>>,-1,30,TRUE)
									//TASK_DRIVE_BY(driver[23].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ENDIF
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].iThisTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].iGetToNewPositionTimer,2000)
			//	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
			///		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh,PLAYER_PED_ID())
			//			mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].iGetToNewPositionTimer = GET_GAME_TIMER()
			//			iThisProgress --
			//		ENDIF
			//	ENDIF
			//ENDIF
		
		BREAK
		
	ENDSWITCH
ENDPROC




PROC MANAGE_MV_MERRY_WEATHER_CHASE_HELI_REAR_2(INT &iThisProgress)


	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
		SET_HELI_TURBULENCE_SCALAR(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh,0.0)
	ENDIF	

	SWITCH iThisProgress
		CASE 0
			REQUEST_MODEL(BUZZARD)
			REQUEST_MODEL(S_M_Y_BlackOps_01)
			IF HAS_MODEL_LOADED(BUZZARD)
			AND HAS_MODEL_LOADED(S_M_Y_BlackOps_01)
				CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_HELI_REAR_2),BUZZARD,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].vThisLocation, mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].fThisHeading,S_M_Y_BlackOps_01,TRUE,24,3,TRUE)
				SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh,TRUE)
				DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_VEHICLE_ROCKET,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh,driver[24].ped)
				SET_CURRENT_PED_VEHICLE_WEAPON(driver[24].ped, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[24].ped,TRUE)
				SET_VEHICLE_BOOST_ACTIVE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh, TRUE)
				//SET_PED_IS_AVOIDED_BY_OTHERS(driver[24].ped,FALSE)
				mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].iGetToNewPositionTimer = GET_GAME_TIMER()
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh,"DH_2B_HELI_GROUP_03")
				bVehicleHasBeenClose[MV_MERRY_WEATHER_CHASE_HELI_REAR_2] = FALSE
				ADD_DEADPOOL_TRIGGER(driver[24].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[25].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[26].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[27].ped, DH2B_KILLS)
				iThisProgress ++
			ENDIF
		BREAK
		
		CASE 1
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].iThisTimer,50)
				IF NOT IS_PED_INJURED(driver[24].ped)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
							TASK_HELI_MISSION(driver[24].ped,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh, NULL, NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, <<45, 20, 5>>), MISSION_GOTO, 70.0, 0.1, GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh), ROUND(vChopperPos.z), 10, -1, HF_HEIGHTMAPONLYAVOIDANCE)
							IF NOT IS_PED_INJURED(driver[25].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[25].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									SET_PED_ACCURACY(driver[25].ped,50)
									TASK_DRIVE_BY(driver[25].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[25].ped,FALSE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[26].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[26].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									SET_PED_ACCURACY(driver[26].ped,50)
									TASK_DRIVE_BY(driver[26].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[26].ped,FALSE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[27].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[27].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									SET_PED_ACCURACY(driver[27].ped,10)
									TASK_DRIVE_BY(driver[27].ped,PLAYER_PED_ID(),NULL,<<0,0,0>>,-1,30,TRUE)	
									//TASK_DRIVE_BY(driver[27].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[27].ped,FALSE)
								ENDIF
							ENDIF
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].iThisTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].iGetToNewPositionTimer,4000)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh,PLAYER_PED_ID())
						mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].iThisTimer = GET_GAME_TIMER()
						mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].iGetToNewPositionTimer = GET_GAME_TIMER()
						iThisProgress ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].iThisTimer,50)
				IF NOT IS_PED_INJURED(driver[24].ped)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
							TASK_HELI_MISSION(driver[24].ped,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh, NULL, NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, <<45, 42, 8>>), MISSION_GOTO, 70.0, 0.1, GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh), ROUND(vChopperPos.z), 10, -1, HF_HEIGHTMAPONLYAVOIDANCE)
							IF NOT IS_PED_INJURED(driver[25].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[25].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[25].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[26].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[26].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[26].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[27].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[27].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[27].ped,PLAYER_PED_ID(),NULL,<<0,0,0>>,-1,30,TRUE)	
									//TASK_DRIVE_BY(driver[27].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ENDIF
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].iThisTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].iGetToNewPositionTimer,4000)
			//	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
			//		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh,PLAYER_PED_ID())
			//			mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].iGetToNewPositionTimer = GET_GAME_TIMER()
			//			iThisProgress --
			//		ENDIF
			//	ENDIF
			//ENDIF
		BREAK
	ENDSWITCH
ENDPROC

INT iPotentialFailTimer

PROC MANAGE_ENEMY_HELI_ROCKETS(INT &iThisProgress)
	
	IF iThisProgress < 3
		IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
			IF MANAGE_MY_TIMER(iPotentialFailTimer,35000)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
					IF NOT HAS_ENTITY_BEEN_DAMAGED_BY_ANY_PED(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
						IF NOT IS_PED_INJURED(driver[28].ped)
							DISABLE_VEHICLE_WEAPON(FALSE,WEAPONTYPE_VEHICLE_SPACE_ROCKET,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,driver[28].ped)
							SET_CURRENT_PED_VEHICLE_WEAPON(driver[28].ped, WEAPONTYPE_VEHICLE_SPACE_ROCKET)
							//SET_PED_IS_AVOIDED_BY_OTHERS(driver[28].ped,FALSE)
							IF NOT IS_PED_INJURED(driver[28].ped)
								SET_PED_ACCURACY(driver[28].ped, 100)
							ENDIF
							iThisProgress = 3
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	SWITCH iThisProgress
		
		CASE 0
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
					REQUEST_MODEL(PROP_POOL_BALL_01)
					IF HAS_MODEL_LOADED(PROP_POOL_BALL_01)
						IF NOT DOES_ENTITY_EXIST(obj_dummy_target)
							obj_dummy_target = CREATE_OBJECT_NO_OFFSET(PROP_POOL_BALL_01,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<GET_RANDOM_INT_IN_RANGE(15,20),0,GET_RANDOM_INT_IN_RANGE(8,10)>>))
							SET_MODEL_AS_NO_LONGER_NEEDED(PROP_POOL_BALL_01)
							SET_ENTITY_INVINCIBLE(obj_dummy_target,TRUE)
							SET_ENTITY_VISIBLE(obj_dummy_target,FALSE)
							FREEZE_ENTITY_POSITION(obj_dummy_target,TRUE)
							IF NOT IS_PED_INJURED(driver[28].ped)
								SET_PED_ACCURACY(driver[28].ped, 100)
							ENDIF
							iThisProgress ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE 1
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
				IF bInRearShootingPosition = TRUE
					IF NOT IS_PED_INJURED(driver[28].ped)
						DISABLE_VEHICLE_WEAPON(FALSE,WEAPONTYPE_VEHICLE_SPACE_ROCKET,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,driver[28].ped)
						SET_CURRENT_PED_VEHICLE_WEAPON(driver[28].ped, WEAPONTYPE_VEHICLE_SPACE_ROCKET)
						//SET_CURRENT_PED_VEHICLE_WEAPON(driver[28].ped, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
						mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer = GET_GAME_TIMER()
						iThisProgress ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer,2000)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
						IF DOES_ENTITY_EXIST(obj_dummy_target)
							SET_ENTITY_COORDS(obj_dummy_target,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<15,0,10>>))
							FREEZE_ENTITY_POSITION(obj_dummy_target,TRUE)
						ENDIF
						IF NOT IS_PED_INJURED(driver[28].ped)
							SET_VEHICLE_SHOOT_AT_TARGET(driver[28].ped,obj_dummy_target,<<0,0,0>>)
							PLAY_SOUND_FROM_ENTITY(-1,"Missile_Incoming_Miss",mission_veh[MV_CHINOOK].veh,"DOCKS_HEIST_FINALE_2B_SOUNDS")
							//SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,<<-1.5662, 0.8777, -0.4377>>),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<GET_RANDOM_INT_IN_RANGE(10,15),0,GET_RANDOM_INT_IN_RANGE(5,10)>>),20,TRUE,WEAPONTYPE_RPG)
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer = GET_GAME_TIMER()
							iThisProgress ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer,GET_RANDOM_INT_IN_RANGE(6000,8000))
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
						IF DOES_ENTITY_EXIST(obj_dummy_target)
							SET_ENTITY_COORDS(obj_dummy_target,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<12,0,7>>))
							FREEZE_ENTITY_POSITION(obj_dummy_target,TRUE)
						ENDIF
						IF NOT IS_PED_INJURED(driver[28].ped)
							SET_VEHICLE_SHOOT_AT_TARGET(driver[28].ped, obj_dummy_target,<<0,0,0>>)
							PLAY_SOUND_FROM_ENTITY(-1,"Missile_Incoming_Miss",mission_veh[MV_CHINOOK].veh,"DOCKS_HEIST_FINALE_2B_SOUNDS")
							//SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,<<1.5662, 0.8777, -0.4377>>),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<GET_RANDOM_INT_IN_RANGE(-10,-15),0,GET_RANDOM_INT_IN_RANGE(5,10)>>),20,TRUE,WEAPONTYPE_RPG)
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer = GET_GAME_TIMER()
							iThisProgress ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer,GET_RANDOM_INT_IN_RANGE(6000,8000))
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
						IF DOES_ENTITY_EXIST(obj_dummy_target)
							SET_ENTITY_COORDS(obj_dummy_target,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<12,0,8>>))
							FREEZE_ENTITY_POSITION(obj_dummy_target,TRUE)
						ENDIF
						IF NOT IS_PED_INJURED(driver[28].ped)
							SET_VEHICLE_SHOOT_AT_TARGET(driver[28].ped, obj_dummy_target,<<0,0,0>>)
							PLAY_SOUND_FROM_ENTITY(-1,"Missile_Incoming_Miss",mission_veh[MV_CHINOOK].veh,"DOCKS_HEIST_FINALE_2B_SOUNDS")
							//SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,<<-1.5662, 0.8777, -0.4377>>),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<GET_RANDOM_INT_IN_RANGE(10,15),0,GET_RANDOM_INT_IN_RANGE(5,10)>>),20,TRUE,WEAPONTYPE_RPG)
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer = GET_GAME_TIMER()
							iThisProgress ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer,GET_RANDOM_INT_IN_RANGE(6000,8000))
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
						IF DOES_ENTITY_EXIST(obj_dummy_target)
							SET_ENTITY_COORDS(obj_dummy_target,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<-12,0,10>>))
							FREEZE_ENTITY_POSITION(obj_dummy_target,TRUE)
						ENDIF
						IF NOT IS_PED_INJURED(driver[28].ped)
							SET_VEHICLE_SHOOT_AT_TARGET(driver[28].ped, obj_dummy_target,<<0,0,0>>)
							PLAY_SOUND_FROM_ENTITY(-1,"Missile_Incoming_Miss",mission_veh[MV_CHINOOK].veh,"DOCKS_HEIST_FINALE_2B_SOUNDS")
							//SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,<<1.5662, 0.8777, -0.4377>>),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<GET_RANDOM_INT_IN_RANGE(-10,-15),0,GET_RANDOM_INT_IN_RANGE(5,10)>>),20,TRUE,WEAPONTYPE_RPG)
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer = GET_GAME_TIMER()
							iThisProgress ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 6
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer,GET_RANDOM_INT_IN_RANGE(6000,8000))
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
						IF DOES_ENTITY_EXIST(obj_dummy_target)
							SET_ENTITY_COORDS(obj_dummy_target,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<12,0,8>>))
							FREEZE_ENTITY_POSITION(obj_dummy_target,TRUE)
						ENDIF
						IF NOT IS_PED_INJURED(driver[28].ped)
							SET_VEHICLE_SHOOT_AT_TARGET(driver[28].ped, obj_dummy_target,<<0,0,0>>)
							PLAY_SOUND_FROM_ENTITY(-1,"Missile_Incoming_Miss",mission_veh[MV_CHINOOK].veh,"DOCKS_HEIST_FINALE_2B_SOUNDS")
							//SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,<<-1.5662, 0.8777, -0.4377>>),GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<GET_RANDOM_INT_IN_RANGE(10,15),0,GET_RANDOM_INT_IN_RANGE(5,10)>>),20,TRUE,WEAPONTYPE_RPG)
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer = GET_GAME_TIMER()
							iThisProgress ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 7
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer,GET_RANDOM_INT_IN_RANGE(6000,8000))
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
						IF DOES_ENTITY_EXIST(obj_dummy_target)
							SET_ENTITY_COORDS(obj_dummy_target,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<-12,0,10>>))
							FREEZE_ENTITY_POSITION(obj_dummy_target,TRUE)
						ENDIF
						IF NOT IS_PED_INJURED(driver[28].ped)
							SET_VEHICLE_SHOOT_AT_TARGET(driver[28].ped, obj_dummy_target,<<0,0,0>>)
							PLAY_SOUND_FROM_ENTITY(-1,"Missile_Incoming_Miss",mission_veh[MV_CHINOOK].veh,"DOCKS_HEIST_FINALE_2B_SOUNDS")
							//SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,<<1.5662, 0.8777, -0.4377>>),GET_ENTITY_COORDS(mission_veh[MV_CHINOOK].veh),400,TRUE,WEAPONTYPE_RPG)
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer = GET_GAME_TIMER()
							iThisProgress ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK

		CASE 8
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer,GET_RANDOM_INT_IN_RANGE(6000,8000))
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
						IF DOES_ENTITY_EXIST(obj_dummy_target)
							SET_ENTITY_COORDS(obj_dummy_target,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<15,0,8>>))
							FREEZE_ENTITY_POSITION(obj_dummy_target,TRUE)
						ENDIF
						IF NOT IS_PED_INJURED(driver[28].ped)
							SET_VEHICLE_SHOOT_AT_TARGET(driver[28].ped, obj_dummy_target,<<0,0,0>>)
							PLAY_SOUND_FROM_ENTITY(-1,"Missile_Incoming_Miss",mission_veh[MV_CHINOOK].veh,"DOCKS_HEIST_FINALE_2B_SOUNDS")
							//SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,<<-1.5662, 0.8777, -0.4377>>),GET_ENTITY_COORDS(mission_veh[MV_CHINOOK].veh),600,TRUE,WEAPONTYPE_RPG)
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer = GET_GAME_TIMER()
							iThisProgress ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 9
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer,GET_RANDOM_INT_IN_RANGE(6000,8000))
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
						IF NOT IS_PED_INJURED(driver[28].ped)
							SET_VEHICLE_SHOOT_AT_TARGET(driver[28].ped, mission_veh[MV_CHINOOK].veh,<<0,0,0>>)
							PLAY_SOUND_FROM_ENTITY(-1,"Missile_Incoming_Hit",mission_veh[MV_CHINOOK].veh,"DOCKS_HEIST_FINALE_2B_SOUNDS")
							//SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,<<1.5662, 0.8777, -0.4377>>),GET_ENTITY_COORDS(mission_veh[MV_CHINOOK].veh),400,TRUE,WEAPONTYPE_RPG)
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer = GET_GAME_TIMER()
							iThisProgress ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 10
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer,GET_RANDOM_INT_IN_RANGE(4000,8000))
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
						IF NOT IS_PED_INJURED(driver[28].ped)
							SET_VEHICLE_SHOOT_AT_TARGET(driver[28].ped, mission_veh[MV_CHINOOK].veh,<<0,0,0>>)
							PLAY_SOUND_FROM_ENTITY(-1,"Missile_Incoming_Hit",mission_veh[MV_CHINOOK].veh,"DOCKS_HEIST_FINALE_2B_SOUNDS")
							//SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,<<1.5662, 0.8777, -0.4377>>),GET_ENTITY_COORDS(mission_veh[MV_CHINOOK].veh),400,TRUE,WEAPONTYPE_RPG)
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer = GET_GAME_TIMER()
							iThisProgress ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 11
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer,GET_RANDOM_INT_IN_RANGE(4000,8000))
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
						IF NOT IS_PED_INJURED(driver[28].ped)
							SET_VEHICLE_SHOOT_AT_TARGET(driver[28].ped, mission_veh[MV_CHINOOK].veh,<<0,0,0>>)
							PLAY_SOUND_FROM_ENTITY(-1,"Missile_Incoming_Hit",mission_veh[MV_CHINOOK].veh,"DOCKS_HEIST_FINALE_2B_SOUNDS")
							//SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,<<1.5662, 0.8777, -0.4377>>),GET_ENTITY_COORDS(mission_veh[MV_CHINOOK].veh),400,TRUE,WEAPONTYPE_RPG)
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer = GET_GAME_TIMER()
							iThisProgress ++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 12
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer,GET_RANDOM_INT_IN_RANGE(4000,5000))
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
						IF NOT IS_PED_INJURED(driver[28].ped)
							SET_VEHICLE_SHOOT_AT_TARGET(driver[28].ped, mission_veh[MV_CHINOOK].veh,<<0,0,0>>)
							PLAY_SOUND_FROM_ENTITY(-1,"Missile_Incoming_Hit",mission_veh[MV_CHINOOK].veh,"DOCKS_HEIST_FINALE_2B_SOUNDS")
							//SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,<<1.5662, 0.8777, -0.4377>>),GET_ENTITY_COORDS(mission_veh[MV_CHINOOK].veh),400,TRUE,WEAPONTYPE_RPG)
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPatternTimer = GET_GAME_TIMER()
							iThisProgress --
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

PROC MANAGE_MV_MERRY_WEATHER_CHASE_HELI_REAR_3(INT &iThisProgress)
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
		MANAGE_ENEMY_HELI_ROCKETS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iAttackPattern)
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
		SET_HELI_TURBULENCE_SCALAR(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,0.0)
	ENDIF		

	SWITCH iThisProgress
		CASE 0
			REQUEST_MODEL(BUZZARD)
			REQUEST_MODEL(S_M_Y_BlackOps_01)
			IF HAS_MODEL_LOADED(BUZZARD)
			AND HAS_MODEL_LOADED(S_M_Y_BlackOps_01)
				CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_HELI_REAR_3),BUZZARD,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].vThisLocation, mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].fThisHeading,S_M_Y_BlackOps_01,TRUE,28,3,TRUE)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
					SET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<0,-65,35>>))
					SET_ENTITY_HEADING(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh))
				ENDIF
				SET_VEHICLE_BOOST_ACTIVE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh, TRUE)
				DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_VEHICLE_ROCKET,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,driver[28].ped)
				SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,TRUE)
				SET_CURRENT_PED_VEHICLE_WEAPON(driver[28].ped, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[28].ped,TRUE)
				//SET_PED_IS_AVOIDED_BY_OTHERS(driver[28].ped,FALSE)
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,"DH_2B_HELI_GROUP_03")
				bVehicleHasBeenClose[MV_MERRY_WEATHER_CHASE_HELI_REAR_3] = FALSE
				ADD_DEADPOOL_TRIGGER(driver[28].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[29].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[30].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[31].ped, DH2B_KILLS)
				iThisProgress ++
			ENDIF
		BREAK
		
		CASE 1
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iThisTimer,50)
				IF NOT IS_PED_INJURED(driver[28].ped)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)

							TASK_HELI_MISSION(driver[28].ped,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh, NULL, NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, <<3, -65, -7>>), MISSION_GOTO, 120.0, 0.1, GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh), ROUND(vChopperPos.z) -2, 8, -1, HF_HEIGHTMAPONLYAVOIDANCE)

//							IF GET_SCRIPT_TASK_STATUS(driver[28].ped,SCRIPT_TASK_COMBAT)<> PERFORMING_TASK
//								TASK_COMBAT_PED(driver[28].ped,PLAYER_PED_ID())
//							ENDIF
							IF NOT IS_PED_INJURED(driver[29].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[29].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[29].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[29].ped,FALSE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[30].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[30].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[30].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[30].ped,FALSE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[31].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[31].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[31].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[31].ped,FALSE)
								ENDIF
							ENDIF
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iThisTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

//TO FINISH

PROC MANAGE_MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1(INT &iThisProgress)

	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
		SET_HELI_TURBULENCE_SCALAR(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh,0.0)
	ENDIF	

	SWITCH iThisProgress
		CASE 0
			REQUEST_MODEL(BUZZARD)
			REQUEST_MODEL(S_M_Y_BlackOps_01)
			IF HAS_MODEL_LOADED(BUZZARD)
			AND HAS_MODEL_LOADED(S_M_Y_BlackOps_01)
				CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1),BUZZARD,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].vThisLocation, mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].fThisHeading,S_M_Y_BlackOps_01,TRUE,32,3,TRUE)
				SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh,TRUE)
				DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_VEHICLE_ROCKET,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh,driver[32].ped)
				SET_CURRENT_PED_VEHICLE_WEAPON(driver[32].ped, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[32].ped,TRUE)
				//SET_PED_IS_AVOIDED_BY_OTHERS(driver[32].ped,FALSE)
				mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].iGetToNewPositionTimer = GET_GAME_TIMER()
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh,"DH_2B_HELI_GROUP_02")
				SET_VEHICLE_BOOST_ACTIVE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh, TRUE)
				bVehicleHasBeenClose[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1] = FALSE
				ADD_DEADPOOL_TRIGGER(driver[32].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[33].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[34].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[35].ped, DH2B_KILLS)
				iThisProgress ++
			ENDIF
		BREAK
		
		CASE 1
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].iThisTimer,50)
				IF NOT IS_PED_INJURED(driver[32].ped)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
							TASK_HELI_MISSION(driver[32].ped,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh, NULL, NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, <<-30, 30, 5>>), MISSION_GOTO, 60.0, 0.1, -1, ROUND(vChopperPos.z)+ 10, 20, -1, HF_HEIGHTMAPONLYAVOIDANCE)
							//TASK_HELI_MISSION(driver[32].ped,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh, NULL, NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,  <<-65, 30, 15>>), MISSION_GOTO, 120.0, 0.1, GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh), ROUND(vChopperPos.z), 20, -1)
							IF NOT IS_PED_INJURED(driver[33].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[33].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[33].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,100,TRUE)
									SET_PED_ACCURACY(driver[33].ped,40)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[33].ped,FALSE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[34].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[34].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[34].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,100,TRUE)
									SET_PED_ACCURACY(driver[34].ped,40)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[34].ped,FALSE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[35].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[35].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[35].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,100,TRUE)
									SET_PED_ACCURACY(driver[35].ped,40)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[35].ped,FALSE)
								ENDIF
							ENDIF
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].iThisTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].iGetToNewPositionTimer,4000)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh,PLAYER_PED_ID())
						mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].iThisTimer = GET_GAME_TIMER()
						mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].iGetToNewPositionTimer = GET_GAME_TIMER()
						iThisProgress ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].iThisTimer,50)
				IF NOT IS_PED_INJURED(driver[32].ped)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
							TASK_HELI_MISSION(driver[32].ped,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh, NULL, NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, <<-50, 43, 12>>), MISSION_GOTO, 60.0, 0.1, GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh), ROUND(vChopperPos.z), 20, -1, HF_HEIGHTMAPONLYAVOIDANCE)
							IF NOT IS_PED_INJURED(driver[33].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[33].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[33].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[34].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[34].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[34].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[35].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[35].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[35].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,60,TRUE)
								ENDIF
							ENDIF
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].iThisTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].iGetToNewPositionTimer,4000)
			//	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
			//		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh,PLAYER_PED_ID())
			//			mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].iGetToNewPositionTimer = GET_GAME_TIMER()
			//			iThisProgress --
			//		ENDIF
			//	ENDIF
			//ENDIF
		BREAK
	ENDSWITCH
ENDPROC


PROC MANAGE_MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2(INT &iThisProgress)


	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
		SET_HELI_TURBULENCE_SCALAR(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh,0.0)
	ENDIF	

	SWITCH iThisProgress
		CASE 0
			REQUEST_MODEL(BUZZARD)
			REQUEST_MODEL(S_M_Y_BlackOps_01)
			IF HAS_MODEL_LOADED(BUZZARD)
			AND HAS_MODEL_LOADED(S_M_Y_BlackOps_01)
				CREATE_ENEMY_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2),BUZZARD,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].vThisLocation, mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].fThisHeading,S_M_Y_BlackOps_01,TRUE,36,3,TRUE)
				SET_ENTITY_LOAD_COLLISION_FLAG(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh,TRUE)
				DISABLE_VEHICLE_WEAPON(TRUE,WEAPONTYPE_VEHICLE_ROCKET,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh,driver[36].ped)
				SET_CURRENT_PED_VEHICLE_WEAPON(driver[36].ped, WEAPONTYPE_VEHICLE_PLAYER_BUZZARD)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(driver[36].ped,TRUE)
				//SET_PED_IS_AVOIDED_BY_OTHERS(driver[36].ped,FALSE)
				mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].iGetToNewPositionTimer = GET_GAME_TIMER()
				bVehicleHasBeenClose[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2] = FALSE
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh,"DH_2B_HELI_GROUP_02")
				SET_VEHICLE_BOOST_ACTIVE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh, TRUE)
				ADD_DEADPOOL_TRIGGER(driver[36].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[37].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[38].ped, DH2B_KILLS)
				ADD_DEADPOOL_TRIGGER(driver[39].ped, DH2B_KILLS)
				iThisProgress ++
			ENDIF
		BREAK
		
		CASE 1
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].iThisTimer,50)
				IF NOT IS_PED_INJURED(driver[36].ped)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
							TASK_HELI_MISSION(driver[36].ped,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh, NULL, NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, <<45, 40, 5>>), MISSION_GOTO, 60.0, 0.1, GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh), ROUND(vChopperPos.z), 20, -1, HF_HEIGHTMAPONLYAVOIDANCE)

							IF NOT IS_PED_INJURED(driver[37].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[37].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[37].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,100,TRUE)
									SET_PED_ACCURACY(driver[37].ped,40)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[37].ped,FALSE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[38].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[38].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[38].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,100,TRUE)
									SET_PED_ACCURACY(driver[38].ped,40)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[38].ped,FALSE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[39].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[39].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[39].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,100,TRUE)
									SET_PED_ACCURACY(driver[39].ped,40)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[39].ped,FALSE)
								ENDIF
							ENDIF
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].iThisTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].iGetToNewPositionTimer,2000)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh,PLAYER_PED_ID())
						mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].iThisTimer = GET_GAME_TIMER()
						mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].iGetToNewPositionTimer = GET_GAME_TIMER()
						iThisProgress ++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].iThisTimer,50)
				IF NOT IS_PED_INJURED(driver[36].ped)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)

							TASK_HELI_MISSION(driver[36].ped,mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh, NULL, NULL, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh, <<45, 30, 0>>), MISSION_GOTO, 60.0, 0.1,GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh), ROUND(vChopperPos.z), 20, -1, HF_HEIGHTMAPONLYAVOIDANCE)

							IF NOT IS_PED_INJURED(driver[37].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[37].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[37].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,100,TRUE)
									SET_PED_ACCURACY(driver[37].ped,60)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[37].ped,FALSE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[38].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[38].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[38].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,100,TRUE)
									SET_PED_ACCURACY(driver[38].ped,60)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[38].ped,FALSE)
								ENDIF
							ENDIF
							IF NOT IS_PED_INJURED(driver[39].ped)
								IF GET_SCRIPT_TASK_STATUS(driver[39].ped,SCRIPT_TASK_DRIVE_BY)<> PERFORMING_TASK
									TASK_DRIVE_BY(driver[39].ped,NULL,mission_veh[MV_CHINOOK].veh,<<0,0,0>>,-1,100,TRUE)
									SET_PED_ACCURACY(driver[39].ped,60)
									//SET_PED_IS_AVOIDED_BY_OTHERS(driver[39].ped,FALSE)
								ENDIF
							ENDIF
							mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].iThisTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			///IF MANAGE_MY_TIMER(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].iGetToNewPositionTimer,2000)
			//	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
			//		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh,PLAYER_PED_ID())
			//			mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].iGetToNewPositionTimer = GET_GAME_TIMER()
			//			iThisProgress --
			//		ENDIF
			//	ENDIF
			//ENDIF
			
		BREAK
	ENDSWITCH
ENDPROC


BOOL bRearOfPlayerHeliOpen = FALSE
BOOL bNextJumpIsToRear = FALSE

//FLOAT fGameplayCamHeading = 0.0
//FLOAT fGameplayCamPitch = 0.0

CAMERA_INDEX ingameCam

PROC COPY_INGAME_CAMERA()
	ingameCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
	SET_CAM_COORD(ingameCam, GET_GAMEPLAY_CAM_COORD() + (GET_ENTITY_VELOCITY(mission_veh[MV_CHINOOK].veh) * GET_FRAME_TIME()))
	SET_CAM_ROT(ingameCam, GET_GAMEPLAY_CAM_ROT())
	SET_CAM_FOV(ingameCam, GET_GAMEPLAY_CAM_FOV())
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
ENDPROC

BOOL bRunSetUpCamTransition

PROC SET_FRANKLIN_TO_RIGHT()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_SWITCHS")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_SWITCHS2")
		//CLEAR_HELP()
	ENDIF
	SET_LABEL_AS_TRIGGERED("DS2_SWITCHS",TRUE)

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			COPY_INGAME_CAMERA()

			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				IF IS_ENTITY_ATTACHED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					DETACH_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				ENDIF
			ENDIF
		
			//GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_COMBATMG,INFINITE_AMMO,TRUE)
			//GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_COMBATMG,WEAPONCOMPONENT_COMBATMG_CLIP_02)
			//SET_PED_INFINITE_AMMO_CLIP(PLAYER_PED_ID(),TRUE)
			//SET_AMMO_IN_CLIP(PLAYER_PED_ID(), WEAPONTYPE_COMBATMG, 100)
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_ForcedAim,TRUE)
			IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					DETACH_ENTITY(PLAYER_PED_ID())
				ENDIF
			ENDIF
			IF IS_VEHICLE_SEAT_FREE(mission_veh[MV_CHINOOK].veh,VS_BACK_RIGHT)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh,VS_BACK_RIGHT)
			ENDIF
			SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), mission_veh[MV_CHINOOK].veh,TRUE)
			SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), TRUE)
			
			SET_LABEL_AS_TRIGGERED("GO TO REAR",FALSE)
			bInRearShootingPosition = FALSE
			SET_LABEL_AS_TRIGGERED("FORCE CAM INTO POSITION",FALSE)
			PLAY_SOUND_FROM_ENTITY(-1, "SWAP_POSITION", PLAYER_PED_ID(),"DOCKS_HEIST_FINALE_2B_SOUNDS")
			IF bRunSetUpCamTransition = TRUE
			
			ENDIF
			bRunSetUpCamTransition = FALSE
			iFranklinPosition = 1
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			//GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_COMBATMG,INFINITE_AMMO,TRUE)
			//GIVE_WEAPON_COMPONENT_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_COMBATMG,WEAPONCOMPONENT_COMBATMG_CLIP_02)
			SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],PCF_ForcedAim,TRUE)
			IF IS_ENTITY_ATTACHED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				IF NOT IS_PED_IN_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					DETACH_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				ENDIF
			ENDIF
			IF IS_VEHICLE_SEAT_FREE( mission_veh[MV_CHINOOK].veh,VS_BACK_RIGHT)
				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[MV_CHINOOK].veh,VS_BACK_RIGHT)
			ENDIF
			SET_LABEL_AS_TRIGGERED("GO TO REAR",FALSE)
			bInRearShootingPosition = FALSE
			PLAY_SOUND_FROM_ENTITY(-1, "SWAP_POSITION", sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],"DOCKS_HEIST_FINALE_2B_SOUNDS")
			iFranklinPosition = 1
		ENDIF
	ENDIF
ENDPROC

PROC SET_FRANKLIN_TO_LEFT()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_SWITCHS")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_SWITCHS2")
		CLEAR_HELP()
	ENDIF
	SET_LABEL_AS_TRIGGERED("DS2_SWITCHS",TRUE)
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			COPY_INGAME_CAMERA()
		
			//CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			//GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_COMBATMG,INFINITE_AMMO,TRUE)
			//GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_COMBATMG,WEAPONCOMPONENT_COMBATMG_CLIP_02)

			IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				IF IS_ENTITY_ATTACHED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					DETACH_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				ENDIF
			ENDIF
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_ForcedAim,TRUE)
			IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					DETACH_ENTITY(PLAYER_PED_ID())
				ENDIF
			ENDIF
			IF IS_VEHICLE_SEAT_FREE(mission_veh[MV_CHINOOK].veh,VS_BACK_LEFT)
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(),mission_veh[MV_CHINOOK].veh,VS_BACK_LEFT)
			ENDIF
			SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), mission_veh[MV_CHINOOK].veh,TRUE)
			SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), TRUE)
			SET_LABEL_AS_TRIGGERED("GO TO REAR",FALSE)
			bInRearShootingPosition = FALSE
			PLAY_SOUND_FROM_ENTITY(-1, "SWAP_POSITION", PLAYER_PED_ID(),"DOCKS_HEIST_FINALE_2B_SOUNDS")
			bRunSetUpCamTransition = FALSE
			iFranklinPosition = 0
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			IF IS_ENTITY_ATTACHED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				IF NOT IS_PED_IN_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					DETACH_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				ENDIF
			ENDIF
			//CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			//GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_COMBATMG,INFINITE_AMMO,TRUE)
			//GIVE_WEAPON_COMPONENT_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_COMBATMG,WEAPONCOMPONENT_COMBATMG_CLIP_02)
			SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],PCF_ForcedAim,TRUE)
			IF IS_VEHICLE_SEAT_FREE( mission_veh[MV_CHINOOK].veh,VS_BACK_LEFT)
				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[MV_CHINOOK].veh,VS_BACK_LEFT)
			ENDIF
			SET_LABEL_AS_TRIGGERED("GO TO REAR",FALSE)
			bInRearShootingPosition = FALSE
			PLAY_SOUND_FROM_ENTITY(-1, "SWAP_POSITION", sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],"DOCKS_HEIST_FINALE_2B_SOUNDS")
			iFranklinPosition = 0
		ENDIF
	ENDIF
ENDPROC

PROC SET_FRANKLIN_TO_REAR()
	IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_SWITCHS")
	OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_SWITCHS2")
		CLEAR_HELP()
	ENDIF
	SET_LABEL_AS_TRIGGERED("DS2_SWITCHS",TRUE)

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
			IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					DETACH_ENTITY(PLAYER_PED_ID())
				ENDIF
			ENDIF
			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_COMBATMG,INFINITE_AMMO,TRUE)
			GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_COMBATMG,WEAPONCOMPONENT_COMBATMG_CLIP_02)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_ForcedAim,TRUE)
			IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
				DETACH_ENTITY(PLAYER_PED_ID())
			ENDIF
			ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), mission_veh[MV_CHINOOK].veh,-1, <<0.0477, -3.6, 0.2>>,<<0,0,180>>,TRUE,FALSE,TRUE,TRUE)
			SET_PLAYER_FORCED_ZOOM(PLAYER_ID(),TRUE)
			SET_ENTITY_NO_COLLISION_ENTITY(PLAYER_PED_ID(), mission_veh[MV_CHINOOK].veh,TRUE)
		ENDIF
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
		SET_GAMEPLAY_CAM_RELATIVE_PITCH()
		bInRearShootingPosition = TRUE
		bNextJumpIsToRear = FALSE
		PLAY_SOUND_FROM_ENTITY(-1, "SWAP_POSITION", PLAYER_PED_ID(),"DOCKS_HEIST_FINALE_2B_SOUNDS")
		bRunSetUpCamTransition = FALSE
		iFranklinPosition = 2
	ELSE
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			IF IS_ENTITY_ATTACHED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				IF NOT IS_PED_IN_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					DETACH_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				ENDIF
			ENDIF
			SET_ENTITY_VISIBLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
			CLEAR_PED_TASKS_IMMEDIATELY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			//GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_COMBATMG,INFINITE_AMMO,TRUE)
			//GIVE_WEAPON_COMPONENT_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_COMBATMG,WEAPONCOMPONENT_COMBATMG_CLIP_02)
			SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],PCF_ForcedAim,TRUE)
			ATTACH_ENTITY_TO_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], mission_veh[MV_CHINOOK].veh,-1, <<0.0477, -3.6, 0.2>>,<<0,0,180>>,TRUE,FALSE,TRUE,TRUE)
			SET_ENTITY_NO_COLLISION_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], mission_veh[MV_CHINOOK].veh,TRUE)
			bInRearShootingPosition = TRUE
			bNextJumpIsToRear = FALSE
			PLAY_SOUND_FROM_ENTITY(-1, "SWAP_POSITION", sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],"DOCKS_HEIST_FINALE_2B_SOUNDS")
			iFranklinPosition = 2
		ENDIF
	ENDIF
ENDPROC

//WITHOUT CAMERAS

BOOL bForcedToRightFromRearOnce
//0 - LEFT
//1 - RIGHT
//2 - REAR
INT iSide

PROC MANAGE_SHIFT_FRANKLINS_POSITION()
	IF DOES_CAM_EXIST(ingameCam)
		DESTROY_CAM(ingameCam)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_CAM_POINT_AT_CLOSEST_VEHICLE(iFranklinPosition = 0)
	ENDIF			
			
	IF bInRearShootingPosition = TRUE
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			FLOAT fNearClip = 0.4
			IF (IS_PC_VERSION() AND GET_ASPECT_RATIO(TRUE) > 1.8)
				// Reduce near clip for triple head as camera clips helicopter interior.
				fNearClip = 0.20
			ENDIF
			SET_PLAYER_FORCED_ZOOM(PLAYER_ID(), TRUE)
			SET_THIRD_PERSON_CAM_RELATIVE_HEADING_LIMITS_THIS_UPDATE(f_cam_limits_aim_heading_min,f_cam_limits_aim_heading_max)
			SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(f_cam_limits_aim_pitch_min,f_cam_limits_aim_pitch_max)
			SET_THIRD_PERSON_CAM_ORBIT_DISTANCE_LIMITS_THIS_UPDATE(f_cam_limits_aim_orbit_min, f_cam_limits_aim_orbit_max)
			SET_THIRD_PERSON_AIM_CAM_NEAR_CLIP_THIS_UPDATE(fNearClip)
		ENDIF
	ENDIF

	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
	
		// Ensure only fire button is used for shooting on PC, not aim like console driveby.
		IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_AIM)
		ENDIF
		
		SWITCH iFranklinPosition
			CASE 0
				IF NOT IS_SELECTOR_CAM_ACTIVE()
				AND NOT IS_SELECTOR_ONSCREEN()
				AND NOT IS_SELECTOR_UI_BUTTON_JUST_PRESSED()
				AND NOT IS_SELECTOR_UI_BUTTON_PRESSED()
					IF IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_CONTEXT)
						IF bNextJumpIsToRear
							iSide = 2
							SET_FRANKLIN_TO_REAR()
						ELSE
							iSide = 1
							//REMOVE
	//						IF bRunSetUpCamTransition
	//							IF NOT DOES_CAM_EXIST(cutscene_cam)			
	//								cutscene_cam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
	//								SET_CAM_COORD(cutscene_cam, <<1,1,1>>)
	//							ENDIF
	//							ATTACH_CAM_TO_ENTITY(cutscene_cam,mission_veh[MV_CHINOOK].veh)
	//							PLAY_CAM_ANIM(cutscene_cam,  "left-to-right_cam", "missheistdocks2bcarbob_cams",<< 0,0,0 >>,<< 0,0,0 >>)
	//							SET_CAM_ACTIVE(cutscene_cam,TRUE)
	//							RENDER_SCRIPT_CAMS(TRUE,FALSE)
	//						ENDIF
							SET_FRANKLIN_TO_RIGHT()
						ENDIF
					ENDIF
				ENDIF
			BREAK
			
			CASE 1
				IF NOT IS_SELECTOR_CAM_ACTIVE()
				AND NOT IS_SELECTOR_ONSCREEN()
				AND NOT IS_SELECTOR_UI_BUTTON_JUST_PRESSED()
				AND NOT IS_SELECTOR_UI_BUTTON_PRESSED()
					IF IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_CONTEXT)
						IF bNextJumpIsToRear
						OR bRearOfPlayerHeliOpen
							iSide = 2
							SET_FRANKLIN_TO_REAR()
						ELSE
							iSide = 0
							SET_FRANKLIN_TO_LEFT()
						ENDIF
					ENDIF		
				ENDIF
			BREAK
			
			CASE 2
				SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), TRUE)
				IF NOT IS_SELECTOR_CAM_ACTIVE()
				AND NOT IS_SELECTOR_ONSCREEN()
				AND NOT IS_SELECTOR_UI_BUTTON_JUST_PRESSED()
				AND NOT IS_SELECTOR_UI_BUTTON_PRESSED()
					IF IS_DISABLED_CONTROL_JUST_RELEASED(PLAYER_CONTROL, INPUT_CONTEXT)
						BOOL bForcedThisTime
						bForcedThisTime = FALSE
						IF NOT bForcedToRightFromRearOnce
							IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
								iSide = 1
								SET_FRANKLIN_TO_RIGHT()
								bForcedToRightFromRearOnce = TRUE
								bForcedThisTime = TRUE
							ENDIF
						ENDIF
						
						IF NOT bForcedThisTime
							iSide = 0
							SET_FRANKLIN_TO_LEFT()
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	

ENDPROC

//WITH ANIMATED CAMS
//
//BOOL bForcedToRightFromRearOnce
//INT sceneIDShiftCam
//
////0 - LEFT
////1 - RIGHT
////2 - REAR
//INT iSide
//
//PROC MANAGE_SHIFT_FRANKLINS_POSITION()
//
//
//	IF DOES_CAM_EXIST(ingameCam)
//		DESTROY_CAM(ingameCam)
//		RENDER_SCRIPT_CAMS(FALSE, FALSE)
//		SET_CAM_POINT_AT_CLOSEST_VEHICLE(iFranklinPosition = 0)
//	ENDIF			
//			
//	IF bInRearShootingPosition = TRUE
//		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//			SET_PLAYER_FORCED_ZOOM(PLAYER_ID(), TRUE)
//			SET_THIRD_PERSON_CAM_RELATIVE_HEADING_LIMITS_THIS_UPDATE(f_cam_limits_aim_heading_min,f_cam_limits_aim_heading_max)
//			SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(f_cam_limits_aim_pitch_min,f_cam_limits_aim_pitch_max)
//			SET_THIRD_PERSON_CAM_ORBIT_DISTANCE_LIMITS_THIS_UPDATE(f_cam_limits_aim_orbit_min, f_cam_limits_aim_orbit_max)
//			SET_THIRD_PERSON_AIM_CAM_NEAR_CLIP_THIS_UPDATE(0.4)
//		ENDIF
//	ENDIF
//
//	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
//		SWITCH iFranklinPosition
//			CASE 0
//				IF NOT bRunSetUpCamTransition
//					IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
//						IF bNextJumpIsToRear
//							iSide = 2
//							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN	
//								IF NOT bRunSetUpCamTransition
//									IF NOT DOES_CAM_EXIST(cutscene_cam)			
//										cutscene_cam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
//										SET_CAM_COORD(cutscene_cam, <<1,1,1>>)
//									ENDIF
//									
//									sceneIDShiftCam = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0.0, 0.0, 0.0>>)
//									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDShiftCam, mission_veh[MV_CHINOOK].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[1].veh, "chassis"))
//									PLAY_SYNCHRONIZED_CAM_ANIM(cutscene_cam,sceneIDShiftCam, "right-to-rear_cam","missheistdocks2bcarbob_cams")
//				
//									SET_CAM_ACTIVE(cutscene_cam,TRUE)
//									RENDER_SCRIPT_CAMS(TRUE,TRUE,1000)
//									bRunSetUpCamTransition = TRUE
//								ENDIF
//							ENDIF
//							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
//								SET_FRANKLIN_TO_REAR()
//							ENDIF
//						ELSE
//							iSide = 1
//							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN	
//								IF NOT bRunSetUpCamTransition
//									IF NOT DOES_CAM_EXIST(cutscene_cam)			
//										cutscene_cam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
//										SET_CAM_COORD(cutscene_cam, <<1,1,1>>)
//									ENDIF
//									
//									sceneIDShiftCam = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0.0, 0.0, 0.0>>)
//									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDShiftCam, mission_veh[MV_CHINOOK].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[1].veh, "chassis"))
//									PLAY_SYNCHRONIZED_CAM_ANIM(cutscene_cam,sceneIDShiftCam, "left-to-right_cam","missheistdocks2bcarbob_cams")
//				
//									SET_CAM_ACTIVE(cutscene_cam,TRUE)
//									RENDER_SCRIPT_CAMS(TRUE,TRUE,1000)
//									bRunSetUpCamTransition = TRUE
//								ENDIF
//							ELSE
//								SET_FRANKLIN_TO_RIGHT()
//							ENDIF
//						ENDIF
//					ENDIF
//				ELSE
//					IF bRunSetUpCamTransition = TRUE
//						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDShiftCam) = 1
//							IF bNextJumpIsToRear
//								SET_FRANKLIN_TO_REAR()
//							ELSE
//								SET_FRANKLIN_TO_RIGHT()
//							ENDIF
//							IF IS_CAM_ACTIVE(cutscene_cam)	
//								RENDER_SCRIPT_CAMS(FALSE,TRUE,1500)
//								SET_CAM_ACTIVE(cutscene_cam,FALSE)
//							ENDIF
//							
//						ENDIF
//					ENDIF
//				ENDIF
//			BREAK
//			
//			CASE 1
//				IF NOT bRunSetUpCamTransition
//					IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
//						IF bNextJumpIsToRear
//						OR bRearOfPlayerHeliOpen
//						iSide = 2
//							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN	
//								IF NOT bRunSetUpCamTransition
//									IF NOT DOES_CAM_EXIST(cutscene_cam)			
//										cutscene_cam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
//										SET_CAM_COORD(cutscene_cam, <<1,1,1>>)
//									ENDIF
//									
//									sceneIDShiftCam = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0.0, 0.0, 0.0>>)
//									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDShiftCam, mission_veh[MV_CHINOOK].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[1].veh, "chassis"))
//									PLAY_SYNCHRONIZED_CAM_ANIM(cutscene_cam,sceneIDShiftCam, "right-to-rear_cam","missheistdocks2bcarbob_cams")
//				
//									SET_CAM_ACTIVE(cutscene_cam,TRUE)
//									RENDER_SCRIPT_CAMS(TRUE,TRUE,1000)
//									bRunSetUpCamTransition = TRUE
//								ENDIF
//							ENDIF
//							IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
//								SET_FRANKLIN_TO_REAR()
//							ENDIF
//						ELSE
//							iSide = 0
//							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN	
//								IF NOT bRunSetUpCamTransition
//									IF NOT DOES_CAM_EXIST(cutscene_cam)			
//										cutscene_cam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
//										SET_CAM_COORD(cutscene_cam, <<1,1,1>>)
//									ENDIF
//									
//									sceneIDShiftCam = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0.0, 0.0, 0.0>>)
//									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDShiftCam, mission_veh[MV_CHINOOK].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[1].veh, "chassis"))
//									PLAY_SYNCHRONIZED_CAM_ANIM(cutscene_cam,sceneIDShiftCam, "right-to-left_cam","missheistdocks2bcarbob_cams")
//				
//									SET_CAM_ACTIVE(cutscene_cam,TRUE)
//									RENDER_SCRIPT_CAMS(TRUE,TRUE,1000)
//									bRunSetUpCamTransition = TRUE
//								ENDIF
//							ELSE
//								SET_FRANKLIN_TO_LEFT()
//							ENDIF
//						ENDIF
//					ENDIF
//				ELSE
//					IF bRunSetUpCamTransition = TRUE
//						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDShiftCam) = 1
//							IF IS_CAM_ACTIVE(cutscene_cam)	
//								RENDER_SCRIPT_CAMS(FALSE,TRUE,1500)
//								SET_CAM_ACTIVE(cutscene_cam,FALSE)
//							ENDIF
//							IF bNextJumpIsToRear
//								SET_FRANKLIN_TO_REAR()
//							ELSE
//								SET_FRANKLIN_TO_LEFT()
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//
//			BREAK
//			
//			CASE 2
//				SET_PLAYER_FORCE_SKIP_AIM_INTRO(PLAYER_ID(), TRUE)
//				
//				IF NOT bRunSetUpCamTransition
//					IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
//						BOOL bForcedThisTime
//						bForcedThisTime = FALSE
//						IF NOT bForcedToRightFromRearOnce
//							IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
//								IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN	
//									IF NOT bRunSetUpCamTransition
//										IF NOT DOES_CAM_EXIST(cutscene_cam)			
//											cutscene_cam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
//											SET_CAM_COORD(cutscene_cam, <<1,1,1>>)
//										ENDIF
//										
//										sceneIDShiftCam = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0.0, 0.0, 0.0>>)
//										ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDShiftCam, mission_veh[MV_CHINOOK].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[1].veh, "chassis"))
//										PLAY_SYNCHRONIZED_CAM_ANIM(cutscene_cam,sceneIDShiftCam, "rear-to-left_cam","missheistdocks2bcarbob_cams" )
//					
//										SET_CAM_ACTIVE(cutscene_cam,TRUE)
//										RENDER_SCRIPT_CAMS(TRUE,TRUE,1000)
//										bRunSetUpCamTransition = TRUE
//									ENDIF
//								ELSE
//									SET_FRANKLIN_TO_LEFT()
//								ENDIF
//							ENDIF
//						ENDIF
//						
//						IF NOT bForcedThisTime
//							IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN	
//								IF NOT bRunSetUpCamTransition
//									IF NOT DOES_CAM_EXIST(cutscene_cam)			
//										cutscene_cam = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
//										SET_CAM_COORD(cutscene_cam, <<1,1,1>>)
//									ENDIF
//									
//									sceneIDShiftCam = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0.0, 0.0, 0.0>>)
//									ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(sceneIDShiftCam, mission_veh[MV_CHINOOK].veh, GET_ENTITY_BONE_INDEX_BY_NAME(mission_veh[1].veh, "chassis"))
//									PLAY_SYNCHRONIZED_CAM_ANIM(cutscene_cam,sceneIDShiftCam, "rear-to-left_cam","missheistdocks2bcarbob_cams" )
//				
//									SET_CAM_ACTIVE(cutscene_cam,TRUE)
//									RENDER_SCRIPT_CAMS(TRUE,TRUE,1000)
//									bRunSetUpCamTransition = TRUE
//								ENDIF
//							ELSE
//								SET_FRANKLIN_TO_LEFT()
//							ENDIF
//						ENDIF
//					ENDIF
//				ELSE
//					IF bRunSetUpCamTransition = TRUE
//						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneIDShiftCam) = 1
//							IF IS_CAM_ACTIVE(cutscene_cam)	
//								RENDER_SCRIPT_CAMS(FALSE,TRUE,1500)
//								SET_CAM_ACTIVE(cutscene_cam,FALSE)
//							ENDIF
//							IF NOT bForcedToRightFromRearOnce
//								SET_FRANKLIN_TO_LEFT()
//							ELSE
//								SET_FRANKLIN_TO_LEFT()
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//				
//			BREAK
//		ENDSWITCH
//	ENDIF
//	
//
//ENDPROC

//INT iHeliTaskSpeed 
//INT iHeliHeight
BOOL bMikeHeliTask = FALSE

CONST_INT NUMBER_MICHAEL_FLYTO_NODES 5

FUNC VECTOR GET_MICHAEL_FLYTO_NODE_POS(INT i)
	SWITCH i
		CASE 0
			RETURN <<-963, 5561, 60>>
		BREAK	
		CASE 1
			RETURN <<-701, 5412, 136>>
		BREAK
		CASE 2
			RETURN <<-343, 4842, 266>>
		BREAK
		CASE 3
			RETURN <<468, 4066, 220>>
		BREAK
		CASE 4
			RETURN <<1754.4716, 3283.0715, 40.0925>> 
		BREAK
	ENDSWITCH
	
	RETURN <<1754.4716, 3283.0715, 40.0925>> 
ENDFUNC
	
PROC MANAGE_AI_TAKEOVER()
	VECTOR vPlayerChopperPos
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
		vPlayerChopperPos = GET_ENTITY_COORDS(mission_veh[MV_CHINOOK].veh)
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		// apply force to heli
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), mission_veh[MV_CHINOOK].veh)
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)
			ELSE
				SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
			ENDIF
			VECTOR vChopperRot = GET_ENTITY_ROTATION(mission_veh[MV_CHINOOK].veh)
			PRINTSTRING("rotation = ") PRINTVECTOR(vChopperRot) PRINTNL()
			IF vChopperRot.x > 15
				APPLY_FORCE_TO_ENTITY(mission_veh[MV_CHINOOK].veh, APPLY_TYPE_EXTERNAL_FORCE, <<0,0,1.0>>, <<0,-12,0>>, 0, TRUE, TRUE, TRUE)
				APPLY_FORCE_TO_ENTITY(mission_veh[MV_CHINOOK].veh, APPLY_TYPE_EXTERNAL_FORCE, <<0,0,-1.0>>, <<0,12,0>>, 0, TRUE, TRUE, TRUE)
			ELIF vChopperRot.x < -15
				APPLY_FORCE_TO_ENTITY(mission_veh[MV_CHINOOK].veh, APPLY_TYPE_EXTERNAL_FORCE, <<0,0,-1.0>>, <<0,-12,0>>, 0, TRUE, TRUE, TRUE)
				APPLY_FORCE_TO_ENTITY(mission_veh[MV_CHINOOK].veh, APPLY_TYPE_EXTERNAL_FORCE, <<0,0,1.0>>, <<0,12,0>>, 0, TRUE, TRUE, TRUE)
			ENDIF
		ENDIF	
	ENDIF
	
	IF bMikeHeliTask = FALSE
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN

			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],TRUE)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					SET_ENTITY_INVINCIBLE(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
				ENDIF
				
				//PRINTSTRING("iHeliHeight:")PRINTINT(iHeliHeight)PRINTNL()
				//TASK_HELI_MISSION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],mission_veh[MV_CHINOOK].veh,NULL,NULL,<<1754.4716, 3283.0715, 40.0925>>,MISSION_GOTO,TO_FLOAT(iHeliTaskSpeed),30,-1,iHeliHeight,iHeliHeight - 5)
				
				// get the nearest Michael flyto node
				INT i
				
				BOOL bFound = FALSE
				FLOAT fFlightSpeed = 30.0
				
				REPEAT NUMBER_MICHAEL_FLYTO_NODES i 
					IF NOT bFound 
						VECTOR vFlytoPos = GET_MICHAEL_FLYTO_NODE_POS(i)
						IF vPlayerChopperPos.x < vFlytoPos.x
						AND NOT IS_ENTITY_AT_COORD(mission_veh[MV_CHINOOK].veh, vFlytoPos, <<35,35,35>>)
							SWITCH i
								CASE 0
									fFlightSpeed = 13
								BREAK							
								CASE 1
									fFlightSpeed = 15
								BREAK
								CASE 2
									fFlightSpeed = 22
								BREAK
								CASE 3
									fFlightSpeed = 28
								BREAK
							ENDSWITCH
						
							vReturnFlyto = vFlyToPos
							bFound = TRUE
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF NOT bFound
					vReturnFlyto = <<1754.4716, 3283.0715, 40.0925>>
				ENDIF
				
				TASK_HELI_MISSION(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],mission_veh[MV_CHINOOK].veh,NULL,NULL,vReturnFlyto,MISSION_GOTO,fFlightSpeed,5,-1,FLOOR(vReturnFlyto.z), 15, -1.0, HF_HEIGHTMAPONLYAVOIDANCE)
				SET_HELI_TURBULENCE_SCALAR(mission_veh[MV_CHINOOK].veh,0.0)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					SET_ENTITY_COLLISION(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
				ENDIF
				
		
				START_AUDIO_SCENE("DH_2B_ESCAPE_AS_FRANKLIN")

				STOP_AUDIO_SCENE("DH_2B_ESCAPE_AS_MICHAEL")

				
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				ELSE
					REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID())
				ENDIF
				
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(PLAYER_PED_ID())

//				ADD_ENTITY_TO_HELI_IGNORE_AVOIDANCE_LIST(mission_veh[MV_CHINOOK].veh, mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh)
//				ADD_ENTITY_TO_HELI_IGNORE_AVOIDANCE_LIST(mission_veh[MV_CHINOOK].veh, mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
//				ADD_ENTITY_TO_HELI_IGNORE_AVOIDANCE_LIST(mission_veh[MV_CHINOOK].veh, mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
//				ADD_ENTITY_TO_HELI_IGNORE_AVOIDANCE_LIST(mission_veh[MV_CHINOOK].veh, mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
//				ADD_ENTITY_TO_HELI_IGNORE_AVOIDANCE_LIST(mission_veh[MV_CHINOOK].veh, mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
//				ADD_ENTITY_TO_HELI_IGNORE_AVOIDANCE_LIST(mission_veh[MV_CHINOOK].veh, mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
//				ADD_ENTITY_TO_HELI_IGNORE_AVOIDANCE_LIST(mission_veh[MV_CHINOOK].veh, mission_veh[MV_MERRY_WEATHER_HELI_1].veh)
//				ADD_ENTITY_TO_HELI_IGNORE_AVOIDANCE_LIST(mission_veh[MV_CHINOOK].veh, mission_veh[MV_MERRY_WEATHER_HELI_2].veh)
//				ADD_ENTITY_TO_HELI_IGNORE_AVOIDANCE_LIST(mission_veh[MV_CHINOOK].veh, mission_veh[MV_MERRY_WEATHER_HELI_3].veh)
				
				bMikeHeliTask = TRUE
				
			ENDIF
		ENDIF
	ELSE
		IF IS_ENTITY_AT_COORD(mission_veh[MV_CHINOOK].veh, vReturnFlyto, <<30,30,30>>)
		OR vPlayerChopperPos.x > vReturnFlyto.x
			bMikeHeliTask = FALSE
		ENDIF
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		IF bMikeHeliTask = TRUE
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					SET_ENTITY_INVINCIBLE(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
				ENDIF
				
				STOP_AUDIO_SCENE("DH_2B_ESCAPE_AS_FRANKLIN")
				START_AUDIO_SCENE("DH_2B_ESCAPE_AS_MICHAEL")

				
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],"DH_2B_FRANKLIN_GROUP")
				
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
				SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],CA_LEAVE_VEHICLES,FALSE)
				SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],100)
				SET_PED_FIRING_PATTERN(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FIRING_PATTERN_BURST_FIRE)
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],1000)
				SET_PLAYER_FORCED_ZOOM(PLAYER_ID(),FALSE)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					SET_ENTITY_COLLISION(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
				ENDIF
				bMikeHeliTask = FALSE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_SEQUENCE_GENERAL_FUNCTIONS()

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SPECIAL)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	ELIF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_NEXT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SELECT_PREV_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_UD)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MOVE_LR)
	ENDIF
ENDPROC

PROC UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(PED_STRUCT &array[])

	INT i
	
	FOR i = 0 TO ( COUNT_OF(array) - 1 )
		IF DOES_ENTITY_EXIST(array[i].ped)
			IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),array[i].ped) < 320
			OR array[i].bForceBlipOff = TRUE
				UPDATE_AI_PED_BLIP(array[i].ped, array[i].EnemyBlipData,-1,PLAYER_ID(),TRUE,FALSE,300)
			ELSE
				CLEANUP_AI_PED_BLIP(array[i].EnemyBlipData)
			ENDIF
		ENDIF
	ENDFOR

ENDPROC

INT iChaseShootOutStage 
INT iDoorOpenDelay
INT iMoveToNextStage
INT iNextRandomSpeechTime
BOOL bFranklinPromptedToSwitchPosition
BOOL bWarpFinalHelis1
BOOL bWarpFinalHelis2

//BOOL bNextJumpIsToRear

PROC DO_RANDOM_SPEECH_SHOOTOUT()
	IF GET_GAME_TIMER() >= iNextRandomSpeechTime
		INT i
		BOOL bFound = FALSE
		IF iChaseShootOutStage <> 7
			FOR i=ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1) TO ENUM_TO_INT(MV_MERRY_WEATHER_HELI_3)
				IF NOT bFound
					IF IS_VEHICLE_DRIVEABLE(mission_veh[i].veh)
						IF NOT IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(mission_veh[i].veh))
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(mission_veh[i].veh), FALSE) < 100
								bFound = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDFOR
		ENDIF
						
		IF bFound
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
			AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF DOES_ENTITY_EXIST(mission_veh[i].veh)
					IF GET_ENTITY_MODEL(mission_veh[i].veh) = BUZZARD 
						IF i = ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_HELI_REAR_3)
						AND bInRearShootingPosition
							IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P6j",CONV_PRIORITY_HIGH)
								iNextRandomSpeechTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(14000, 19000)
							ENDIF
						ELSE
							IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P6j",CONV_PRIORITY_HIGH)
								iNextRandomSpeechTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(14000, 19000)
							ENDIF
						ENDIF
					ELSE
						IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P6j",CONV_PRIORITY_HIGH)
							iNextRandomSpeechTime = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(14000, 19000)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			iNextRandomSpeechTime = GET_GAME_TIMER() + 5000
		ENDIF
	ENDIF
ENDPROC

INT iProgressStageTime = -1
BOOL bShownSwitchAIFranklinHelp

FUNC BOOL HAS_PLAYER_FINISHED_SHOOTOUT_SECTION()
	
	PRINTSTRING("iChaseShootOutStage")PRINTINT(iChaseShootOutStage)PRINTNL()
	
	MANAGE_BLOW_UP_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1))
	MANAGE_BLOW_UP_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2))
	MANAGE_BLOW_UP_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_HELI_RHS_1),TRUE)
	MANAGE_BLOW_UP_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_HELI_REAR_1),TRUE)
	MANAGE_BLOW_UP_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_HELI_REAR_2),TRUE)
	MANAGE_BLOW_UP_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_HELI_REAR_3),TRUE)
	MANAGE_BLOW_UP_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1),TRUE)
	MANAGE_BLOW_UP_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2),TRUE)
	MANAGE_BLOW_UP_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1))
	MANAGE_BLOW_UP_VEHICLE(ENUM_TO_INT(MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2))

	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
		SET_PED_INCREASED_AVOIDANCE_RADIUS(PLAYER_PED_ID())
	ENDIF

	//FIRST WAVE - HANDLED IN MAIN PROGRESS LOOP
	IF iChaseShootOutStage > 0
		//MANAGE_MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].iThisProgress)
		//MANAGE_MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].iThisProgress)
	ENDIF
	
	//SECOND WAVE
	IF iChaseShootOutStage > 2
		IF iSide = 0
			IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh)
			AND DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh)
			AND DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh)
				AND IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh)
				AND IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh)
					IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_RIGHTDR")
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_RIGHTDR",CONV_PRIORITY_HIGH)
								SET_LABEL_AS_TRIGGERED("DS2A_RIGHTDR",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		DO_RANDOM_SPEECH_SHOOTOUT()	
		MANAGE_MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].iThisProgress)
		MANAGE_MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].iThisProgress)
		MANAGE_MV_MERRY_WEATHER_CHASE_HELI_RHS_1(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].iThisProgress)
	ENDIF
	
	IF iChaseShootOutStage > 2
		IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh)
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh)
				IF IS_AUDIO_SCENE_ACTIVE("DH_2B_ENEMY_HELI_01_ARRIVES")
					STOP_AUDIO_SCENE("DH_2B_ENEMY_HELI_01_ARRIVES")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//THIRD WAVE
	IF iChaseShootOutStage > 4
		IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
		AND DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
			AND IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_MOREHEL")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_MOREHEL",CONV_PRIORITY_HIGH)
							SET_LABEL_AS_TRIGGERED("DS2A_MOREHEL",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		MANAGE_MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].iThisProgress)
		MANAGE_MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].iThisProgress)
	ENDIF
	
	IF iChaseShootOutStage > 4
	
		IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
		AND DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
			AND NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
				IF IS_AUDIO_SCENE_ACTIVE("DH_2B_ENEMY_HELI_02_ARRIVES")
					STOP_AUDIO_SCENE("DH_2B_ENEMY_HELI_02_ARRIVES")
				ENDIF
			ENDIF
			//gone
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P6i_1")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P6i",CONV_PRIORITY_HIGH)
						SET_LABEL_AS_TRIGGERED("DS2A_P6i_1",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//FIANL WAVE
	IF iChaseShootOutStage > 5
		IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
		AND DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
			AND IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_MHELI")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_MHELI",CONV_PRIORITY_HIGH)
							SET_LABEL_AS_TRIGGERED("DS2A_MHELI",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		//MANAGE_MV_MERRY_WEATHER_CHASE_HELI_REAR_1(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].iThisProgress)
		MANAGE_MV_MERRY_WEATHER_CHASE_HELI_REAR_2(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].iThisProgress)
		MANAGE_MV_MERRY_WEATHER_CHASE_HELI_REAR_3(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].iThisProgress)
	ENDIF
	
	IF iChaseShootOutStage > 5
		IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
		AND DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
			AND NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
				
				IF IS_AUDIO_SCENE_ACTIVE("DH_2B_ENEMY_HELI_03_ARRIVES")
					STOP_AUDIO_SCENE("DH_2B_ENEMY_HELI_03_ARRIVES")
				ENDIF
				//gone
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P6i_2")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P6i",CONV_PRIORITY_HIGH)
							SET_LABEL_AS_TRIGGERED("DS2A_P6i_2",TRUE)
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	ENDIF
	
	IF bFranklinPromptedToSwitchPosition
		MANAGE_SHIFT_FRANKLINS_POSITION()
		//CASCADE_SHADOWS_SET_AIRCRAFT_MODE(FALSE)
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
		vChopperPos = GET_ENTITY_COORDS(mission_veh[MV_CHINOOK].veh)
	ENDIF
	
	IF iChaseShootOutStage >= 4
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
			IF NOT bShownSwitchAIFranklinHelp
				PRINT_HELP_FOREVER("DS2_SWITCHS2")
				bShownSwitchAIFranklinHelp = TRUE
			ENDIF
		ENDIF
	ENDIF
	
	IF iChaseShootOutStage >= 3
	AND iChaseShootOutStage <= 6
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_SWITCHS")
				PRINT_HELP_FOREVER("DS2_SWITCHS")
				SET_LABEL_AS_TRIGGERED("DS2_SWITCHS",TRUE)
			ENDIF
		ENDIF	
	ENDIF

	SWITCH iChaseShootOutStage 
		CASE 0
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
				PRINT_GOD_TEXT("DS2_DEFTM")
				bInRearShootingPosition = FALSE
				bNextJumpIsToRear = FALSE
				bShownSwitchAIFranklinHelp = FALSE
				bForcedToRightFromRearOnce = FALSE
				iSide = 0
				iChaseShootOutStage ++
			ENDIF
		BREAK
		
		CASE 1
		
			IF vChopperPos.x < -1030
			AND (IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh) OR IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh))
			AND NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P6d")	
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P6d",CONV_PRIORITY_HIGH)
						SET_LABEL_AS_TRIGGERED("DS2A_P6d",TRUE)
					ENDIF
				ENDIF
			ELSE
				//iHeliTaskSpeed = 5
				//iHeliHeight = 10
				//bMikeHeliTask = FALSE
				iMoveToNextStage = GET_GAME_TIMER()
				iNextRandomSpeechTime = GET_GAME_TIMER() + 11000
				iChaseShootOutStage ++
			ENDIF
		BREAK
		
		CASE 2
			IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh)
			AND DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh)
				FLOAT fForceNewWaveX
				IF vChopperPos.y > PENINSULA_Y
					fForceNewWaveX = -730
				ELSE
					fForceNewWaveX = -970	
				ENDIF
			
				IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh)
				OR NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh)
				OR IS_PED_INJURED(driver[0].ped)
				OR IS_PED_INJURED(driver[4].ped)
				OR MANAGE_MY_TIMER(iMoveToNextStage,90000)
				OR vChopperPos.x > fForceNewWaveX
				OR (GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN AND MANAGE_MY_TIMER(iMoveToNextStage,30000))
					iMoveToNextStage = GET_GAME_TIMER()
					IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_ENEMY_HELI_01_ARRIVES")
						START_AUDIO_SCENE("DH_2B_ENEMY_HELI_01_ARRIVES")
					ENDIF
				//	iHeliTaskSpeed = 7
				//	iHeliHeight = 25
				//	bMikeHeliTask = FALSE
					bFranklinPromptedToSwitchPosition = TRUE
					iChaseShootOutStage ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P6e")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P6e",CONV_PRIORITY_HIGH)
						SET_LABEL_AS_TRIGGERED("DS2A_P6e",TRUE)
					ENDIF
				ENDIF
			ELSE
				bFranklinPromptedToSwitchPosition = TRUE
				iProgressStageTime = -1
				iChaseShootOutStage ++
			ENDIF
		BREAK
		
		CASE 4
			IF iSide = 1
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P6g")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P6g",CONV_PRIORITY_HIGH)
							SET_LABEL_AS_TRIGGERED("DS2A_P6g",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF iProgressStageTime >= 0
				IF GET_GAME_TIMER() >= iProgressStageTime + 2000
					IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_ENEMY_HELI_02_ARRIVES")
						START_AUDIO_SCENE("DH_2B_ENEMY_HELI_02_ARRIVES")
					ENDIF
					TRIGGER_MUSIC_EVENT("DH2B_HELIS_ARRIVE")
					bWarpFinalHelis1 = FALSE
					bWarpFinalHelis2 = FALSE
					iProgressStageTime = -1
					iChaseShootOutStage ++
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh)
				AND DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh)
				AND DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh)
					
					BOOL bAdvanceStage
					bAdvanceStage = FALSE
					IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh)
						bAdvanceStage = TRUE
					ELSE
						IF IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh))
							bAdvanceStage = TRUE
						ENDIF
					ENDIF
				
					IF bAdvanceStage
					OR MANAGE_MY_TIMER(iMoveToNextStage,120000)
					//	iHeliTaskSpeed = 10
					//	iHeliHeight = 40
					//	bMikeHeliTask = FALSE
						iMoveToNextStage = GET_GAME_TIMER()
						
						iProgressStageTime = GET_GAME_TIMER()
					ENDIF
				ENDIF			
			ENDIF
		BREAK
		
		CASE 5
			IF iProgressStageTime < 0
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P6mo")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P6mo",CONV_PRIORITY_HIGH)
							SET_LABEL_AS_TRIGGERED("DS2A_P6mo",TRUE)
						ENDIF
					ENDIF
				ENDIF
				
				
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("Speed up")
					IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh)
					OR NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh)
					OR NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
					OR NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
						SET_LABEL_AS_TRIGGERED("Speed up",TRUE)
					ENDIF
				ENDIF
				
				//WARP
				IF bWarpFinalHelis1 = FALSE
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
						IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh, <<300,300,300>>, FALSE, FALSE)
							IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
								SET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<195,-195,50>>))
								SET_ENTITY_HEADING(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh,GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh))
								SET_HELI_BLADES_FULL_SPEED(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
								bWarpFinalHelis1 = TRUE
							ENDIF
						ELSE
							bWarpFinalHelis1 = TRUE
						ENDIF
					ENDIF
					
				ENDIF
				
				
				IF bWarpFinalHelis2 = FALSE	
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
						IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh, <<300,300,300>>, FALSE, FALSE)
							IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
								SET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<-135,-285,60>>))
								SET_ENTITY_HEADING(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh,GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh))
								SET_HELI_BLADES_FULL_SPEED(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
								bWarpFinalHelis2 = TRUE
							ENDIF
						ELSE
							bWarpFinalHelis2 = TRUE
						ENDIF
					
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
				AND DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
					BOOL bHeli1OK
					BOOL bHeli2OK
					
					bHeli1OK = TRUE
					bHeli2OK = TRUE
					
					IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
						bHeli1OK = FALSE
					ELSE
						IF IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh))
							bHeli1OK = FALSE
						ENDIF
					ENDIF
					
					IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
						bHeli2OK = FALSE
					ELSE
						IF IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh))
							bHeli2OK = FALSE
						ENDIF
					ENDIF				
				
					IF NOT bHeli1OK
					AND NOT bHeli2OK
						iProgressStageTime = GET_GAME_TIMER()
						REQUEST_SCRIPT_AUDIO_BANK("PORT_OF_LS_2B_HELICOPTER_DOOR_SMASH")

					ENDIF
				ENDIF
			ELSE
				IF GET_GAME_TIMER() >= iProgressStageTime + 2500
					//iHeliTaskSpeed = 30
					//iHeliHeight = 60
					//bMikeHeliTask = FALSE
					iPotentialFailTimer = GET_GAME_TIMER()
					iProgressStageTime = -1
					
					iChaseShootOutStage ++				
				ENDIF
			ENDIF
		BREAK

		CASE 6

		
//			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
//				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_SWITCHS")
//					PRINT_HELP("DS2_SWITCHS")
//					SET_LABEL_AS_TRIGGERED("DS2_SWITCHS",TRUE)
//				ENDIF
//			ENDIF
			//IF vChopperPos.x > -685
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P6f")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P6f",CONV_PRIORITY_HIGH)
							REPLAY_RECORD_BACK_FOR_TIME(10.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
							SET_LABEL_AS_TRIGGERED("DS2A_P6f",TRUE)
						ENDIF
					ENDIF
				ELSE
					bNextJumpIsToRear = TRUE
					bRearOfPlayerHeliOpen = TRUE
					iChaseShootOutStage ++
				ENDIF
			//ENDIF
		BREAK
		
		CASE 7
			IF bInRearShootingPosition
				IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_GETREAR")
					CLEAR_PRINTS()
				ENDIF
				KILL_FACE_TO_FACE_CONVERSATION()
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P6g")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P6g",CONV_PRIORITY_HIGH)
							SET_LABEL_AS_TRIGGERED("DS2A_P6g",TRUE)
						ENDIF
					ENDIF
				ELSE
					STOP_AUDIO_SCENE("DH_2B_ENEMY_HELI_02_ARRIVES")
					START_AUDIO_SCENE("DH_2B_ENEMY_HELI_03_ARRIVES")
					PLAY_SOUND_FRONTEND(-1,"Door_Open","DOCKS_HEIST_FINALE_2B_SOUNDS")
					iDoorOpenDelay = GET_GAME_TIMER()
					bWarpFinalHelis1 = FALSE
					bWarpFinalHelis2 = FALSE
					iChaseShootOutStage ++
				ENDIF
			ELSE
				
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_GETREAR")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						PRINT_GOD_TEXT("DS2_GETREAR")
						SET_LABEL_AS_TRIGGERED("DS2_SWITCHS",FALSE)	
						SET_LABEL_AS_TRIGGERED("DS2_GETREAR",TRUE)	
					ENDIF
				ELSE
					IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_SWITCHS")
							PRINT_HELP_FOREVER("DS2_SWITCHS")
							SET_LABEL_AS_TRIGGERED("DS2_SWITCHS",TRUE)
						ENDIF	
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_SWITCHS")
							PRINT_HELP_FOREVER("DS2_SWITCHS2")
							SET_LABEL_AS_TRIGGERED("DS2_SWITCHS",TRUE)
						ENDIF							
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 8
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
				IF MANAGE_MY_TIMER(iDoorOpenDelay,800)
					IF bWarpFinalHelis1 = FALSE
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
							//IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
								SET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<-45,-95,6>>))
								SET_ENTITY_HEADING(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh,GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh))
								SET_HELI_BLADES_FULL_SPEED(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
							//ENDIF
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
							//IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
								SET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<45,-65,6>>))
								SET_ENTITY_HEADING(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh,GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh))
								SET_HELI_BLADES_FULL_SPEED(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
							//ENDIF
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
							//IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
								SET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(mission_veh[MV_CHINOOK].veh,<<0,-55,10>>))
								SET_ENTITY_HEADING(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh))
								SET_HELI_BLADES_FULL_SPEED(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
							//ENDIF
						ENDIF
						bWarpFinalHelis1 = TRUE
					ENDIF
				ENDIF
				IF MANAGE_MY_TIMER(iDoorOpenDelay,1000)
					SET_VEHICLE_DOOR_OPEN(mission_veh[MV_CHINOOK].veh,SC_DOOR_REAR_LEFT,FALSE)
					SET_VEHICLE_DOOR_BROKEN(mission_veh[MV_CHINOOK].veh,SC_DOOR_REAR_LEFT,FALSE)
					//SET_VEHICLE_DOOR_LATCHED(mission_veh[MV_CHINOOK].veh,SC_DOOR_REAR_LEFT,TRUE,TRUE)
					bWarpFinalHelis1 = FALSE
					iChaseShootOutStage ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 9
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P6f")
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
					IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P6f",CONV_PRIORITY_HIGH)
						SET_LABEL_AS_TRIGGERED("DS2A_P6f",TRUE)
					ENDIF
				ENDIF
			ELSE
				iMoveToNextStage = GET_GAME_TIMER()
				iChaseShootOutStage ++
			ENDIF
		BREAK
			
		CASE 10
			//IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
			IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
			AND DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
				//IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
				IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
				OR NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
					
					//iHeliTaskSpeed = 35
					//iHeliHeight = 60
					//bMikeHeliTask = FALSE
					iChaseShootOutStage ++
				ENDIF
			ENDIF
		BREAK
		
		CASE 11	
			IF iProgressStageTime < 0
			//	IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
				IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
				AND DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
					
					//BOOL bHeli1OK
					BOOL bHeli2OK
					BOOL bHeli3OK
					
					//bHeli1OK = TRUE
					bHeli2OK = TRUE
					bHeli3OK = TRUE
					
					//IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
					//	bHeli1OK = FALSE
					//ELSE
					//	IF IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh))
					//		bHeli1OK = FALSE
					//	ENDIF
					//ENDIF
					
					IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
						bHeli2OK = FALSE
					ELSE
						IF IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh))
							bHeli2OK = FALSE
						ENDIF
					ENDIF		
					
					IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
						bHeli3OK = FALSE
					ELSE
						IF IS_PED_INJURED(GET_PED_IN_VEHICLE_SEAT(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh))
							bHeli3OK = FALSE
						ENDIF
					ENDIF							
				
					//IF NOT bHeli1OK
					IF NOT bHeli2OK
					AND NOT bHeli3OK
						iProgressStageTime = GET_GAME_TIMER()
					ELSE
						IF MANAGE_MY_TIMER(iMoveToNextStage,60000)
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh,FALSE)) > 420
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh,FALSE)) > 420
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,FALSE)) > 420
								IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
									EXPLODE_VEHICLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
								ENDIF
								IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
									EXPLODE_VEHICLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
								ENDIF
								IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
									EXPLODE_VEHICLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					iProgressStageTime = GET_GAME_TIMER()
				ENDIF
			ELSE
				IF GET_GAME_TIMER() >= iProgressStageTime + 1500
					//	iHeliTaskSpeed = 35
					//	iHeliHeight = 80
					//	bMikeHeliTask = FALSE
					bWarpFinalHelis1 = FALSE
					iChaseShootOutStage = 99					
				ENDIF
			ENDIF
		BREAK
		
		CASE 99
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				SET_ENTITY_INVINCIBLE(mission_veh[MV_SUBMERISIBLE].veh,FALSE)
			ENDIF
			
			RETURN TRUE
		BREAK
	
	ENDSWITCH

	RETURN FALSE

ENDFUNC

PROC INITIAL_SHOOTOUT_DATA()

	//VECTOR				vThisLocation
	//FLOAT				    fThisHeading
	
	//FIRST WAVE
	mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].vThisLocation = <<-991.26, 6330.41, 0.62>>
	mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].fThisHeading = 113.26
	
	mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].vThisLocation = <<-1032.55, 6303.47, 0.24>>
	mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].fThisHeading = 131.26
	
	//SECOND WAVE
	mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].vThisLocation = <<-1527.6051, 5411.4316, 0>>
	mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].fThisHeading = 327.8288 
	
	mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].vThisLocation = <<-1646.5157, 5654.1089,0>>
	mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].fThisHeading = 326.2217
	
	mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].vThisLocation = <<-1147.5525, 4923.6914, 120.0419>>
	mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].fThisHeading = 1.1023
	
	//FINAL WAVE
	mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].vThisLocation = <<-1147.5525, 4923.6914, 320.0419>>
	mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].fThisHeading = 264.5329
	
	mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].vThisLocation = <<-769.4426, 5541.1660, 232.4886>>
	mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].fThisHeading = 196.6876
	
	mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].vThisLocation = <<-732.1653, 5812.6724, 216.4172>>
	mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].fThisHeading = 236.9917
	
	//EXTRA HELIS
	mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].vThisLocation = <<-1147.5525, 4923.6914, 150.0419>>
	mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].fThisHeading = 264.5329
	
	mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].vThisLocation = <<-769.4426, 5541.1660, 152.4886>>
	mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].fThisHeading = 196.6876


ENDPROC

PROC MANAGE_ENEMY_CLEANUP()
	
	IF iChaseShootOutStage > 2
		IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh)
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh)
				IF DOES_ENTITY_EXIST(driver[0].ped)
					SET_PED_AS_NO_LONGER_NEEDED(driver[0].ped)
				ENDIF
				IF DOES_ENTITY_EXIST(driver[1].ped)
					SET_PED_AS_NO_LONGER_NEEDED(driver[1].ped)
				ENDIF
				IF DOES_ENTITY_EXIST(driver[2].ped)
					SET_PED_AS_NO_LONGER_NEEDED(driver[2].ped)
				ENDIF
				IF DOES_ENTITY_EXIST(driver[3].ped)
					SET_PED_AS_NO_LONGER_NEEDED(driver[3].ped)
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh)
			IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh)
				IF DOES_ENTITY_EXIST(driver[4].ped)
					SET_PED_AS_NO_LONGER_NEEDED(driver[4].ped)
				ENDIF
				IF DOES_ENTITY_EXIST(driver[5].ped)
					SET_PED_AS_NO_LONGER_NEEDED(driver[5].ped)
				ENDIF
				IF DOES_ENTITY_EXIST(driver[6].ped)
					SET_PED_AS_NO_LONGER_NEEDED(driver[6].ped)
				ENDIF
				IF DOES_ENTITY_EXIST(driver[7].ped)
					SET_PED_AS_NO_LONGER_NEEDED(driver[7].ped)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	
	IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh)
		IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh)
			IF DOES_ENTITY_EXIST(driver[8].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[8].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[9].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[9].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[10].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[10].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[11].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[11].ped)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh)
		IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh)
			IF DOES_ENTITY_EXIST(driver[12].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[12].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[13].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[13].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[14].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[14].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[15].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[15].ped)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh)
		IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh)
			IF DOES_ENTITY_EXIST(driver[16].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[16].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[17].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[17].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[18].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[18].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[19].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[19].ped)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
		IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh)
			IF DOES_ENTITY_EXIST(driver[20].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[20].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[21].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[21].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[22].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[22].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[23].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[23].ped)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
		IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh)
			IF DOES_ENTITY_EXIST(driver[24].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[24].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[25].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[25].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[26].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[26].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[27].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[27].ped)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
		IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh)
			IF DOES_ENTITY_EXIST(driver[28].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[28].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[29].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[29].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[30].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[30].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[31].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[31].ped)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
		IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh)
			IF DOES_ENTITY_EXIST(driver[32].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[32].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[33].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[33].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[34].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[34].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[35].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[35].ped)
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
		IF NOT IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh)
			IF DOES_ENTITY_EXIST(driver[36].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[36].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[37].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[37].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[38].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[38].ped)
			ENDIF
			IF DOES_ENTITY_EXIST(driver[39].ped)
				SET_PED_AS_NO_LONGER_NEEDED(driver[39].ped)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

FUNC PED_INDEX GET_CLOSEST_PED_FROM_RELATIONSHIP_GROUP_FOR_PED(PED_INDEX PedIndex, REL_GROUP_HASH rgHash, INT iProximity = 0, BOOL bOnScreen = FALSE) 

  PED_INDEX pedArray[16]
  
  INT i = 0
        
  IF NOT IS_PED_INJURED(PedIndex)
  
    GET_PED_NEARBY_PEDS(PedIndex, pedArray)
    FOR i = 0 TO (COUNT_OF(pedArray) - 1)
    	IF DOES_ENTITY_EXIST(pedArray[i])
        	IF NOT IS_PED_INJURED(pedArray[i])
            	IF (GET_PED_RELATIONSHIP_GROUP_HASH(pedArray[i]) = rgHash)
                    IF (iProximity <= 0)
                        IF ( bOnScreen = TRUE )
                            IF IS_ENTITY_ON_SCREEN(pedArray[i])
                           		RETURN pedArray[i]
                            ENDIF
                        ELSE
                      		RETURN pedArray[i]
                        ENDIF
                    ELSE
                    	IF (i + iProximity <= COUNT_OF(pedArray) - 1 )
                 			IF DOES_ENTITY_EXIST(pedArray[i + iProximity])
                                IF NOT IS_PED_INJURED(pedArray[i + iProximity])
                                    IF (GET_PED_RELATIONSHIP_GROUP_HASH(pedArray[i + iProximity]) = rgHash )
                                        IF (bOnScreen = TRUE)
                                            IF IS_ENTITY_ON_SCREEN(pedArray[i + iProximity])
                                            	RETURN pedArray[i + iProximity]
                                            ENDIF
       										RETURN pedArray[i + iProximity]       
                                        ENDIF                                               
                                    ENDIF
                                ENDIF
							ENDIF
                        ENDIF
                    ENDIF                                    
              	ENDIF
        	ENDIF
  		ENDIF
	ENDFOR
    
  ENDIF
  
  RETURN NULL

ENDFUNC

INT iAmbientDialogueTimer

PROC MANAGE_MERRYWEATHER_BATTLE_DIALOGUE()
	
	PED_INDEX pedEnemySpeak = GET_CLOSEST_PED_FROM_RELATIONSHIP_GROUP_FOR_PED(PLAYER_PED_ID(),rel_group_enemies,GET_RANDOM_INT_IN_RANGE(0,3))
	
	INT iRand
	
	IF DOES_ENTITY_EXIST(pedEnemySpeak)
		IF NOT IS_PED_INJURED(pedEnemySpeak)
			IF IS_PED_MODEL(pedEnemySpeak,S_M_Y_BLACKOPS_01)
				IF MANAGE_MY_TIMER(iAmbientDialogueTimer, 9000)
					IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()),GET_ENTITY_COORDS(pedEnemySpeak)) < 120
						iRand = GET_RANDOM_INT_IN_RANGE(0,2)
						IF iRand = 0
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedEnemySpeak,"DH2B_HGAA","MERRYWEATHER1",SPEECH_PARAMS_FORCE_MEGAPHONE)
							iAmbientDialogueTimer = GET_GAME_TIMER()						
						ENDIF
						IF iRand = 1
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedEnemySpeak,"DH2B_HHAA","MERRYWEATHER1",SPEECH_PARAMS_FORCE_MEGAPHONE)
							iAmbientDialogueTimer = GET_GAME_TIMER()	
						ENDIF
						IF iRand = 2
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedEnemySpeak,"DH2B_HIAA","MERRYWEATHER1",SPEECH_PARAMS_FORCE_MEGAPHONE)
							iAmbientDialogueTimer = GET_GAME_TIMER()	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF


ENDPROC

PROC MANAGE_SHOW_NO_MERCENARY_ACHIEVEMENT()
	IF IS_PED_INJURED(driver[0].ped)
	AND IS_PED_INJURED(driver[1].ped)
	AND IS_PED_INJURED(driver[2].ped)
	AND IS_PED_INJURED(driver[3].ped)
	AND IS_PED_INJURED(driver[4].ped)
	AND IS_PED_INJURED(driver[5].ped)
	AND IS_PED_INJURED(driver[6].ped)
	AND IS_PED_INJURED(driver[7].ped)
	AND IS_PED_INJURED(driver[8].ped)
	AND IS_PED_INJURED(driver[9].ped)
	AND IS_PED_INJURED(driver[10].ped)
	AND IS_PED_INJURED(driver[11].ped)
	AND IS_PED_INJURED(driver[12].ped)
	AND IS_PED_INJURED(driver[13].ped)
	AND IS_PED_INJURED(driver[14].ped)
	AND IS_PED_INJURED(driver[15].ped)
	AND IS_PED_INJURED(driver[16].ped)
	AND IS_PED_INJURED(driver[17].ped)
	AND IS_PED_INJURED(driver[18].ped)
	AND IS_PED_INJURED(driver[19].ped)
	AND IS_PED_INJURED(driver[24].ped)
	AND IS_PED_INJURED(driver[25].ped)
	AND IS_PED_INJURED(driver[26].ped)
	AND IS_PED_INJURED(driver[27].ped)
	AND IS_PED_INJURED(driver[28].ped)
	AND IS_PED_INJURED(driver[29].ped)
	AND IS_PED_INJURED(driver[30].ped)
	AND IS_PED_INJURED(driver[31].ped)
	AND IS_PED_INJURED(driver[32].ped)
	AND IS_PED_INJURED(driver[33].ped)
	AND IS_PED_INJURED(driver[34].ped)
	AND IS_PED_INJURED(driver[35].ped)
	AND IS_PED_INJURED(driver[36].ped)
	AND IS_PED_INJURED(driver[37].ped)
	AND IS_PED_INJURED(driver[38].ped)
	AND IS_PED_INJURED(driver[39].ped)
		PRINTLN("No mercenary ahcievement unlocked")
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(DH2B_KILLED_ALL_MERRYWEATHER)
	ENDIF
ENDPROC

INT iTimeTillMerryWeatherShowUp
BOOL bUnblockedSelector
INT iTimeBeforeAutoSwitch
		
PROC DEFEND_AGAINST_MERRYWEATHER()
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
		DISABLE_REPLAY_RECORDING_UI_THIS_FRAME()	//Fix for bug 2228044
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
		SET_VEHICLE_CEILING_HEIGHT(mission_veh[MV_CHINOOK].veh,300) 
	ENDIF
	
	//MANAGE_ENEMY_CLEANUP()
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
		IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK)
		ENDIF
	ENDIF
	SET_ALLOW_CUSTOM_VEHICLE_DRIVE_BY_CAM_THIS_UPDATE(TRUE)
	
	IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		SET_PED_FIRING_PATTERN(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FIRING_PATTERN_BURST_FIRE_DRIVEBY)
		SET_PED_COMBAT_MOVEMENT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CM_STATIONARY)
	ENDIF
	
	PED_INDEX franklinPed
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		franklinPed = PLAYER_PED_ID()
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
			SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(mission_veh[MV_CHINOOK].veh, FALSE, rel_group_enemies)
		ENDIF
	ELSE
		franklinPed = sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
			SET_ENTITY_ONLY_DAMAGED_BY_RELATIONSHIP_GROUP(mission_veh[MV_CHINOOK].veh, TRUE, rel_group_enemies)
		ENDIF
	ENDIF
	
	IF NOT IS_ENTITY_DEAD(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh)
		VECTOR vBoatHeightAboveWater
		vBoatHeightAboveWater = GET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh)
		IF vBoatHeightAboveWater.z > 4.5
		OR vBoatHeightAboveWater.z < -4
			PRINTLN("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ HEIGHT BOAT RHS_1: ", vBoatHeightAboveWater.z, "... RELOCATING @@@@@@@@@@@@@@@@@@@")
			IF NOT IS_ENTITY_IN_WATER(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh)
			AND NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh)
			AND NOT IS_SPHERE_VISIBLE(<<-1049.7820, 5635.9116, 0.8559>>, 5)
				SET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh, <<-1049.7820, 5635.9116, 0.8559>>)
				SET_ENTITY_HEADING(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh,GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh))
			ENDIF
		ENDIF
	ENDIF
	IF NOT IS_ENTITY_DEAD(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh)
		VECTOR vBoatHeightAboveWater
		vBoatHeightAboveWater = GET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh)
		IF vBoatHeightAboveWater.z > 4.5
		OR vBoatHeightAboveWater.z < -4
			PRINTLN("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ HEIGHT BOAT RHS_2: ", vBoatHeightAboveWater.z, "... RELOCATING @@@@@@@@@@@@@@@@@@@")
			IF NOT IS_ENTITY_IN_WATER(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh)
			AND NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh)
			AND NOT IS_SPHERE_VISIBLE(<<-1039.4417, 5640.9355, 0.9954>>, 5)
				SET_ENTITY_COORDS(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh, <<-1039.4417, 5640.9355, 0.9954>>)
				SET_ENTITY_HEADING(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh,GET_ENTITY_HEADING(mission_veh[MV_CHINOOK].veh))
			ENDIF
		ENDIF
	ENDIF
	
	WEAPON_TYPE weapon
	IF NOT IS_PED_INJURED(franklinPed)
		SET_PED_RESET_FLAG(franklinPed, PRF_ExtraLongWeaponRange, TRUE)
	
		INT iAmmo
		GET_AMMO_IN_CLIP(franklinPed, WEAPONTYPE_COMBATMG, iAmmo)
		IF IS_PED_RELOADING(franklinPed)
		OR iAmmo <= 0
			IF iFranklinPosition <> 2
				PRINTSTRING("franklin is reloading!") PRINTNL()
				CLEAR_PED_TASKS(franklinPed)
			ENDIF
		ENDIF
	
		IF weapon <> WEAPONTYPE_COMBATMG
			GIVE_WEAPON_TO_PED(franklinPed,WEAPONTYPE_COMBATMG,INFINITE_AMMO,TRUE)
			GIVE_WEAPON_COMPONENT_TO_PED(franklinPed,WEAPONTYPE_COMBATMG,WEAPONCOMPONENT_COMBATMG_CLIP_02)
			SET_PED_INFINITE_AMMO_CLIP(franklinPed,TRUE)
			SET_AMMO_IN_CLIP(franklinPed, WEAPONTYPE_COMBATMG, 100)	
		ELSE
			SET_PED_INFINITE_AMMO_CLIP(franklinPed,TRUE)
			SET_AMMO_IN_CLIP(franklinPed, WEAPONTYPE_COMBATMG, 100)				
		ENDIF
	ENDIF
	
	MANAGE_SEQUENCE_GENERAL_FUNCTIONS()

	IF iprogress > 0
		IF iprogress < 4
			UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(enemy)
			UPDATE_AI_PED_BLIPS_FOR_ENEMY_GROUP(driver)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRUE)
			IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh)
				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh)
			ENDIF
		ENDIF
	ENDIF
	
	IF MANAGE_MY_TIMER(iDripsTimer,5000)
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxDrips)
			STOP_PARTICLE_FX_LOOPED(ptfxDrips)
		ENDIF
	ENDIF

	IF iprogress > 1

		MANAGE_FREE_HOTSWAP(FALSE, FALSE, TRUE, TRUE, FALSE, TRUE)
		
		MANAGE_AI_TAKEOVER()
		MANAGE_MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].iThisProgress)
		MANAGE_MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].iThisProgress)
	ENDIF
	
	SWITCH iprogress

		CASE 0  //setup
			
			bReprintObjective = FALSE
			bGoToSpeach = FALSE

			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				FREEZE_ENTITY_POSITION(mission_veh[1].veh,FALSE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
				FREEZE_ENTITY_POSITION(mission_veh[0].veh,FALSE)
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
				SET_VEHICLE_STRONG(mission_veh[MV_CHINOOK].veh,TRUE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				FORCE_SUBMARINE_SURFACE_MODE(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
				SET_ENTITY_PROOFS(mission_veh[MV_SUBMERISIBLE].veh,FALSE,FALSE,FALSE,FALSE,FALSE)
			ENDIF
			
			//DIALOGUE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRUE)
				ADD_PED_FOR_DIALOGUE(sSpeech, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
				SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],PCF_GetOutUndriveableVehicle,TRUE)
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				ADD_PED_FOR_DIALOGUE(sSpeech, 3, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
			ENDIF

			REQUEST_WEAPON_ASSET(WEAPONTYPE_CARBINERIFLE)
			REQUEST_WEAPON_ASSET(WEAPONTYPE_COMBATMG)
			
			PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
			
			IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_ESCAPE_ENEMIES")
				START_AUDIO_SCENE("DH_2B_ESCAPE_ENEMIES")
			ENDIF
			START_AUDIO_SCENE("DH_2B_ESCAPE_AS_MICHAEL")

			CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
			CONTROL_FADE_IN(500)
			iHeliAttack = 0
			DUMMY_REFERENCE_INT(iHeliAttack)	// Release fix
			iChaseShootOutStage = 0
			iFranklinPosition = 0
			bRearOfPlayerHeliOpen = FALSE
			SET_BUILDING_STATE(BUILDINGNAME_IPL_AIRFIELD_PROPS, BUILDINGSTATE_DESTROYED)
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7,"Defend Against Merryweather") 
			INITIAL_SHOOTOUT_DATA()
			
			itimer = GET_GAME_TIMER()
			
			SET_VEHICLE_POPULATION_BUDGET(1)
			SET_PED_POPULATION_BUDGET(1)
			
			bFranklinPromptedToSwitchPosition = FALSE
			bInRearShootingPosition = FALSE
			bRunSetUpCamTransition = FALSE
			
			REQUEST_ANIM_DICT("missheistdocks2bcarbob_cams")

			//iHeliTaskSpeed = 2
			//iHeliHeight = 20
			//bMikeHeliTask = FALSE
			
			SET_WANTED_LEVEL_MULTIPLIER(0.0)
			SET_MAX_WANTED_LEVEL(0)

			iTimeTillMerryWeatherShowUp = GET_GAME_TIMER()
			iprogress++
		BREAK
		
		CASE 1
			
			IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,<<1754.4716, 3283.0715, 40.0925>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,TRUE,mission_veh[0].veh,"DS2_FLYTD","DS2_FLYGI","DS2_FLYGB",TRUE)
		
			ELSE
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					VECTOR vPlayerPos
					vPlayerPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
					
					FLOAT fTriggerMerryWeatherX
					IF vPlayerPos.y >= PENINSULA_Y
						fTriggerMerryWeatherX = -1018
					ELSE
						fTriggerMerryWeatherX = -1180
					ENDIF
					
					IF vPlayerPos.x >= fTriggerMerryWeatherX
					AND MANAGE_MY_TIMER(iTimeTillMerryWeatherShowUp, 17000)
					//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<62.502800,7228.432617,-6.756985>>, <<-2186.874023,4586.129883,250.910217>>, 250.000000)
					//OR MANAGE_MY_TIMER(iTimeTillMerryWeatherShowUp,13000)
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						KILL_ANY_CONVERSATION()
						SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_MICHAEL,FALSE)
						SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,TRUE)
						SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,TRUE)
						iTimeTillMerryWeatherShowUp = GET_GAME_TIMER()
						
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							//GIVE_WEAPON_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_COMBATMG,INFINITE_AMMO,TRUE)
							//GIVE_WEAPON_COMPONENT_TO_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_COMBATMG,WEAPONCOMPONENT_COMBATMG_CLIP_02)
							//SET_CURRENT_PED_WEAPON(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],WEAPONTYPE_COMBATMG,TRUE)
							//SET_PED_INFINITE_AMMO_CLIP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
							//SET_AMMO_IN_CLIP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], WEAPONTYPE_COMBATMG, 100)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
							SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],CA_LEAVE_VEHICLES,FALSE)
							SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],CA_DO_DRIVEBYS,TRUE)
							SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],100)
							SET_PED_FIRING_PATTERN(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],FIRING_PATTERN_FULL_AUTO)
							SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],PCF_ForcedAim,TRUE)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],1000)
							IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
								SET_ENTITY_COLLISION(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
							ENDIF
						ENDIF
					
						bUnblockedSelector = FALSE
						iprogress++	
					ELSE
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P7")
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P7",CONV_PRIORITY_HIGH)
										SET_LABEL_AS_TRIGGERED("DS2A_P7",TRUE)
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_EXPL")
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
										IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_EXPL",CONV_PRIORITY_HIGH)
											SET_LABEL_AS_TRIGGERED("DS2A_EXPL",TRUE)
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_PISSD")
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
											IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_PISSD",CONV_PRIORITY_HIGH)
												SET_LABEL_AS_TRIGGERED("DS2A_PISSD",TRUE)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF DOES_CARGOBOB_HAVE_PICK_UP_ROPE(mission_veh[MV_CHINOOK].veh)
						IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
							IF HAS_LABEL_BEEN_TRIGGERED("DROPPED THE SUB")
								IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
									SET_BLIP_ALPHA(sLocatesData.LocationBlip,255)
								ENDIF
								IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
									STABILISE_ENTITY_ATTACHED_TO_HELI(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh,iStabliseLength)
								ENDIF
								IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_GIPOSFSUB")
									CLEAR_PRINTS()
								ENDIF
								IF DOES_BLIP_EXIST(blip_pick_up_sub)
									REMOVE_BLIP(blip_pick_up_sub)
								ENDIF
								SET_LABEL_AS_TRIGGERED("DROPPED THE SUB",FALSE)
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DROPPED THE SUB")
								IF bGoToSpeach = TRUE
									//MANAGE PAUSE DIALOGUE
									IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
										IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
											CLEAR_PRINTS()
											PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
										ENDIF
									ENDIF
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
									STABILISE_ENTITY_ATTACHED_TO_HELI(mission_veh[MV_CHINOOK].veh,NULL,iStabliseLength)
								ENDIF
								
								IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
									SET_BLIP_ALPHA(sLocatesData.LocationBlip,0)
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
									IF NOT DOES_BLIP_EXIST(blip_pick_up_sub)
										blip_pick_up_sub = CREATE_BLIP_FOR_VEHICLE(mission_veh[MV_SUBMERISIBLE].veh)
										PRINT_GOD_TEXT("DS2_GIPOSFSUB")
									ENDIF
								ENDIF
								SET_LABEL_AS_TRIGGERED("DROPPED THE SUB",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF

			ENDIF
			
		BREAK
		
		//
		CASE 2
		
			IF NOT bUnblockedSelector			
				IF GET_GAME_TIMER() >= iTimeTillMerryWeatherShowUp + 1000
					SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_MICHAEL,FALSE)
					SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,FALSE)
					SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,TRUE)	
					SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN,TRUE)
					bUnblockedSelector = TRUE
				ENDIF
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] ,SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) = FINISHED_TASK
						SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],100)
						SET_PED_COMBAT_ATTRIBUTES(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],CA_REQUIRES_LOS_TO_SHOOT,FALSE)
						SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],PCF_ForcedAim,TRUE)
						SET_PED_FIRING_PATTERN(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],FIRING_PATTERN_FULL_AUTO)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],1000)
					ENDIF
				ENDIF
			ENDIF
			
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_SBOATS")
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_SBOATS",CONV_PRIORITY_HIGH)
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
							SET_LABEL_AS_TRIGGERED("DS2A_SBOATS",TRUE)
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P6c")
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P6c",CONV_PRIORITY_HIGH)
							SET_LABEL_AS_TRIGGERED("DS2A_P6c",TRUE)
							PRINTSTRING("DS2A_P6c")PRINTNL()
						ENDIF
					ENDIF
				ELSE
					IF MANAGE_MY_TIMER(iTimeTillMerryWeatherShowUp,40000)
						IF HAS_ANIM_DICT_LOADED("missheistdocks2bcarbob_cams")
							IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_S2FH")
								CLEAR_PRINTS()
							ENDIF
							
							IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_ESCAPE_AS_MICHAEL")
								START_AUDIO_SCENE("DH_2B_ESCAPE_AS_MICHAEL")
							ENDIF
							
							iprogress++
						ENDIF
					ENDIF
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
							IF MANAGE_MY_TIMER(iTimeTillMerryWeatherShowUp,4000)
								IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_S2FH")
									PRINT_GOD_TEXT("DS2_S2FH")
									SET_LABEL_AS_TRIGGERED("DS2_S2FH",TRUE)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF HAS_ANIM_DICT_LOADED("missheistdocks2bcarbob_cams")
							IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_S2FH")
								CLEAR_PRINTS()
							ENDIF
							SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_ForcedAim,TRUE)
							SET_PLAYER_FORCED_ZOOM(PLAYER_ID(),TRUE)
							//GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_COMBATMG,INFINITE_AMMO,TRUE)
							//GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_COMBATMG,WEAPONCOMPONENT_COMBATMG_CLIP_02)
							//SET_PED_INFINITE_AMMO_CLIP(PLAYER_PED_ID(),TRUE)
							//SET_AMMO_IN_CLIP(PLAYER_PED_ID(), WEAPONTYPE_COMBATMG, 100)
							TRIGGER_MUSIC_EVENT("DH2B_CLEAR_MERRY")
							IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_ESCAPE_AS_MICHAEL")
								START_AUDIO_SCENE("DH_2B_ESCAPE_AS_MICHAEL")
							ENDIF
							iprogress++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF iprogress > 2
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(DH2B_ESCAPE_TIME)
			ENDIF
			
		BREAK
		
		CASE 3
			IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_FRANKLIN
				IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED)= FINISHED_TASK
						SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],100)	
						SET_PED_FIRING_PATTERN(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],FIRING_PATTERN_FULL_AUTO)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],1000)
					ENDIF
				ENDIF
			ELSE
				SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN,FALSE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
				vHeliHeight = GET_ENTITY_COORDS(mission_veh[MV_CHINOOK].veh)
			ENDIF
			
			SET_LIGHTS_CUTOFF_DISTANCE_TWEAK(200)
			
			MANAGE_MERRYWEATHER_BATTLE_DIALOGUE()
			
			IF HAS_PLAYER_FINISHED_SHOOTOUT_SECTION()
			
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE, DH2B_ESCAPE_TIME)
				
				KILL_FACE_TO_FACE_CONVERSATION()
				IF DOES_ENTITY_EXIST(obj_dummy_target)
					DELETE_OBJECT(obj_dummy_target)
				ENDIF
				
				INT i
				FOR i = 0 TO MAX_DRIVER_PED - 1
					CLEANUP_AI_PED_BLIP(driver[i].EnemyBlipData)
				ENDFOR	
				
				STOP_AUDIO_SCENE("DH_2B_ESCAPE_ENEMIES")
				TRIGGER_MUSIC_EVENT("DH2B_ALL_DEAD")
				iTimeBeforeAutoSwitch = GET_GAME_TIMER()
				sSelectorCam.pedTo = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					SET_ENTITY_VISIBLE(PLAYER_PED_ID(),TRUE)
				ELSE
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						SET_ENTITY_VISIBLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
					ENDIF
				ENDIF
				iprogress++
			ELSE
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
					//SET_ENTITY_VISIBLE(PLAYER_PED_ID(),FALSE)
				ELSE
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
						SET_ENTITY_VISIBLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
			
			MANAGE_SHIFT_FRANKLINS_POSITION()
			
		
			IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P6k")
				IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P6k",CONV_PRIORITY_HIGH)
					SET_LABEL_AS_TRIGGERED("DS2A_P6k",TRUE)
					bGoToSpeach = TRUE
				ENDIF
			ELSE
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2_S2MH")
							SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_MICHAEL,FALSE)
							SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,TRUE)
							SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,TRUE)
							PRINT_GOD_TEXT("DS2_S2MH")
							SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL,TRUE)
							CLEAR_HELP()
							SET_LABEL_AS_TRIGGERED("DS2_S2MH",TRUE)
						ENDIF
					ENDIF
					IF MANAGE_MY_TIMER(iTimeBeforeAutoSwitch,12000)
						IF NOT RUN_SWITCH_CAM_FROM_PLAYER_TO_PED(sSelectorCam,SWITCH_TYPE_SHORT)
						    
						ELSE
					      	IF sSelectorCam.bOKToSwitchPed
					            IF NOT sSelectorCam.bPedSwitched
					            	IF TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
					                	sSelectorCam.bPedSwitched = TRUE
					            	ENDIF
					            ENDIF
					     	ENDIF
						ENDIF
					ENDIF
				ELSE
					SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL,FALSE)
					IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_S2MH")
						CLEAR_PRINTS()
					ENDIF
					CLEAR_HELP()
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
						SET_ENTITY_COLLISION(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
					ENDIF
					INT i
					FOR i = 0 TO MAX_DRIVER_PED - 1
						CLEANUP_AI_PED_BLIP(driver[i].EnemyBlipData)
					ENDFOR
					SET_VEHICLE_DOOR_SHUT(mission_veh[MV_CHINOOK].veh,SC_DOOR_REAR_LEFT,FALSE)
					iprogress++
				ENDIF
			ENDIF
		BREAK

		CASE 5
			MANAGE_SHOW_NO_MERCENARY_ACHIEVEMENT()
			SET_PLAYER_FORCED_ZOOM(PLAYER_ID(),FALSE)
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_LONG)
			CLEAR_HELP()
			CLEAR_PRINTS()
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			bTriggerDialogue = FALSE
			iprogress = 0
			bReprintObjective = FALSE
			emissionstage = MISSION_STAGE_FLY_AWAY
		BREAK

		
	ENDSWITCH

ENDPROC

BOOL bStreamCutscene = FALSE

PROC FLY_AWAY()
	MANAGE_BUMPS_AND_BANG_DIALOGUE()
	
	IF GET_DISTANCE_BETWEEN_COORDS(<<1745.67480, 3418.85498, 36.94403>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) < 550
		IF NOT DOES_ENTITY_EXIST(pedLester)
			SETUP_MISSION_REQUIREMENT(REQ_LESTER_FOR_CUTSCENE,<<0,0,0>>,0)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(pedWade)
			SETUP_MISSION_REQUIREMENT(REQ_WADE_FOR_CUTSCENE,<<0,0,0>>,0)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(mission_veh[MV_MICHAELS_CAR].veh)
			SETUP_MISSION_REQUIREMENT(REQ_LESTER_CAR_FOR_CUTSCENE,<<0,0,0>>,0)
		ELSE
			//Grab Michaels cars colours for bug 1868378
			IF bMichaelsCarColoursGrabbed = FALSE
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MICHAELS_CAR].veh)
					GET_VEHICLE_COLOURS(mission_veh[MV_MICHAELS_CAR].veh, icolour1, icolour2)
					bMichaelsCarColoursGrabbed = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//TOGGLE CUTSCENE STREAMING
	IF GET_DISTANCE_BETWEEN_COORDS(<<1745.67480, 3418.85498, 36.94403>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) < DEFAULT_CUTSCENE_LOAD_DIST
		IF bStreamCutscene = FALSE
			bStreamCutscene = TRUE
			REQUEST_CUTSCENE("LSDH_2B_MCS_1")
		ENDIF
	ELSE
		IF bStreamCutscene = TRUE
			IF GET_DISTANCE_BETWEEN_COORDS(<<1745.67480, 3418.85498, 36.94403>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) > DEFAULT_CUTSCENE_UNLOAD_DIST
				REMOVE_CUTSCENE()
				bStreamCutscene = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	SET_ALLOW_CUSTOM_VEHICLE_DRIVE_BY_CAM_THIS_UPDATE(TRUE)
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
		IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK)
		ENDIF
	ENDIF
	
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRUE)
			IF NOT IS_PED_IN_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh)
				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],mission_veh[MV_SUBMERISIBLE].veh)
			ENDIF
		ENDIF
	ENDIF
	
	//MANAGE_SHIFT_FRANKLINS_POSITION()
	
	IF bInRearShootingPosition = TRUE
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
			SET_PLAYER_FORCED_ZOOM(PLAYER_ID(), TRUE)
			SET_THIRD_PERSON_CAM_RELATIVE_HEADING_LIMITS_THIS_UPDATE(f_cam_limits_aim_heading_min,f_cam_limits_aim_heading_max)
			SET_THIRD_PERSON_CAM_RELATIVE_PITCH_LIMITS_THIS_UPDATE(f_cam_limits_aim_pitch_min,f_cam_limits_aim_pitch_max)
			SET_THIRD_PERSON_CAM_ORBIT_DISTANCE_LIMITS_THIS_UPDATE(f_cam_limits_aim_orbit_min, f_cam_limits_aim_orbit_max)
		ENDIF
	ENDIF
	
	SAFE_CLEANUP_VEHICLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_1].veh,DINGHY,TRUE)
	SAFE_CLEANUP_VEHICLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_LHS_2].veh,DINGHY,TRUE)
	SAFE_CLEANUP_VEHICLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_1].veh,DINGHY,TRUE)
	SAFE_CLEANUP_VEHICLE(mission_veh[MV_MERRY_WEATHER_CHASE_DINGHY_RHS_2].veh,DINGHY,TRUE)
	SAFE_CLEANUP_VEHICLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_RHS_1].veh,BUZZARD,TRUE)
	SAFE_CLEANUP_VEHICLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_1].veh,BUZZARD,TRUE)
	SAFE_CLEANUP_VEHICLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_2].veh,BUZZARD,TRUE)
	SAFE_CLEANUP_VEHICLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_REAR_3].veh,BUZZARD,TRUE)
	SAFE_CLEANUP_VEHICLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_1].veh,BUZZARD,TRUE)
	SAFE_CLEANUP_VEHICLE(mission_veh[MV_MERRY_WEATHER_CHASE_HELI_ADDITIONAL_2].veh,BUZZARD,TRUE)
	
	IF DOES_ENTITY_EXIST(objMission[MO_RIG1])
		SET_OBJECT_AS_NO_LONGER_NEEDED(objMission[MO_RIG1])
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LEV_DES_BARGE_01)
	ENDIF
	
	IF DOES_ENTITY_EXIST(objMission[MO_RIG2])
		SET_OBJECT_AS_NO_LONGER_NEEDED(objMission[MO_RIG2])
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LEV_DES_BARGE_01)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedWade)
		DELETE_PED(pedWade)
		SET_MODEL_AS_NO_LONGER_NEEDED(IG_WADE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedFloyd)
		DELETE_PED(pedFloyd)
		SET_MODEL_AS_NO_LONGER_NEEDED(IG_FLOYD)
	ENDIF

	//
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_SPECIAL)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
	ENDIF

	SWITCH iprogress

		CASE 0  //setup

			bReprintObjective = FALSE
			bGoToSpeach = FALSE
			
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_MICHAEL,FALSE)
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,FALSE)
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,TRUE)

			IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
				FREEZE_ENTITY_POSITION(mission_veh[1].veh,FALSE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
				FREEZE_ENTITY_POSITION(mission_veh[0].veh,FALSE)
			ENDIF
			
			//DIALOGUE
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
				ADD_PED_FOR_DIALOGUE(sSpeech, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRUE)
				SET_PED_CONFIG_FLAG(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],PCF_GetOutUndriveableVehicle,TRUE)
			ENDIF
			
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				ADD_PED_FOR_DIALOGUE(sSpeech, 3, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
			ENDIF
			
			FOR iCount = 0 To (MAX_DRIVER_PED -1)
				IF DOES_BLIP_EXIST(driver[iCount].blip)
					REMOVE_BLIP(driver[iCount].blip)
				ENDIF
				IF DOES_ENTITY_EXIST(driver[iCount].ped)
					SAFE_CLEANUP_PED(driver[iCount].ped, S_M_Y_BLACKOPS_01,FALSE)
				ENDIF
			ENDFOR	
			
			REQUEST_WEAPON_ASSET(WEAPONTYPE_CARBINERIFLE)
			
		
			STOP_AUDIO_SCENE("DH_2B_ESCAPE_ENEMIES")

			IF NOT IS_AUDIO_SCENE_ACTIVE("DH_2B_RETURN_TO_AIRSTRIP")
				START_AUDIO_SCENE("DH_2B_RETURN_TO_AIRSTRIP")
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				FORCE_SUBMARINE_SURFACE_MODE(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
			ENDIF
			
			IF IS_VEHICLE_SEAT_FREE(mission_veh[MV_CHINOOK].veh,VS_BACK_LEFT)
				SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],mission_veh[MV_CHINOOK].veh,VS_BACK_LEFT)
			ENDIF
			
			CLEAR_HELP()
			
			REPLAY_RECORD_BACK_FOR_TIME(10.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
			
			SET_LABEL_AS_TRIGGERED("DROPPED THE SUB",FALSE)
			
			SET_VEHICLE_POPULATION_BUDGET(3)
			SET_PED_POPULATION_BUDGET(3)
			
			CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
			CONTROL_FADE_IN(500)
			iHeliAttack = 0
			SET_BUILDING_STATE(BUILDINGNAME_IPL_AIRFIELD_PROPS, BUILDINGSTATE_DESTROYED)
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(8,"Fly Away") 
			itimer = GET_GAME_TIMER()
			CLEAR_AREA(<<1754.4716, 3283.0715, 40.0925>>,200,TRUE)
			iprogress++
		BREAK
		
		CASE 1
			
			IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,<<1754.4716, 3283.0715, 40.0925>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,TRUE,mission_veh[0].veh,"DS2_FLYTD","DS2_FLYGI","DS2_FLYGB",TRUE)
		
			ELSE
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<1754.4716, 3283.0715, 40.0925>>,<<250,250,250>>)
						IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_SEEAIR")
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_SEEAIR",CONV_PRIORITY_HIGH)
									SET_LABEL_AS_TRIGGERED("DS2A_SEEAIR",TRUE)
								ENDIF
							ENDIF
						ELSE
							CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
							iprogress++	
						ENDIF
					ENDIF
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						IF IS_IT_SAFE_TO_PLAY_DIALOG()
							IF bGoToSpeach = FALSE
								IF NOT HAS_LABEL_BEEN_TRIGGERED("DS2A_P8")
									IF CREATE_CONVERSATION(sSpeech,"D2BAUD", "DS2A_P8",CONV_PRIORITY_HIGH)
										SET_LABEL_AS_TRIGGERED("DS2A_P8",TRUE)
										bGoToSpeach = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF DOES_CARGOBOB_HAVE_PICK_UP_ROPE(mission_veh[MV_CHINOOK].veh)
						IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
							IF HAS_LABEL_BEEN_TRIGGERED("DROPPED THE SUB")
								IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
									SET_BLIP_ALPHA(sLocatesData.LocationBlip,255)
								ENDIF
								IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
									STABILISE_ENTITY_ATTACHED_TO_HELI(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh,iStabliseLength)
								ENDIF
								IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_GIPOSFSUB")
									CLEAR_PRINTS()
								ENDIF
								IF DOES_BLIP_EXIST(blip_pick_up_sub)
									REMOVE_BLIP(blip_pick_up_sub)
								ENDIF
								SET_LABEL_AS_TRIGGERED("DROPPED THE SUB",FALSE)
							ENDIF
						ELSE
							IF NOT HAS_LABEL_BEEN_TRIGGERED("DROPPED THE SUB")
								IF bGoToSpeach = TRUE
									//MANAGE PAUSE DIALOGUE
									IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
										IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
											CLEAR_PRINTS()
											PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
										ENDIF
									ENDIF
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
									STABILISE_ENTITY_ATTACHED_TO_HELI(mission_veh[MV_CHINOOK].veh,NULL,iStabliseLength)
								ENDIF
								
								IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
									SET_BLIP_ALPHA(sLocatesData.LocationBlip,0)
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
									IF NOT DOES_BLIP_EXIST(blip_pick_up_sub)
										blip_pick_up_sub = CREATE_BLIP_FOR_VEHICLE(mission_veh[MV_SUBMERISIBLE].veh)
										PRINT_GOD_TEXT("DS2_GIPOSFSUB")
									ENDIF
								ENDIF
								REPLAY_RECORD_BACK_FOR_TIME(10.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
								SET_LABEL_AS_TRIGGERED("DROPPED THE SUB",TRUE)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
						
				IF bGoToSpeach = TRUE
					//MANAGE PAUSE DIALOGUE
					IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							CLEAR_PRINTS()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						ENDIF
					//Play conversation
					ELSE
						IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_FLYGB")
								CLEAR_PRINTS()
							ENDIF
					    	PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 2
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_LONG)
			CLEAR_HELP()
			REQUEST_CUTSCENE("LSDH_2B_MCS_1")
			//CLEAR_PRINTS()
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			bTriggerDialogue = FALSE
			iprogress = 0
			bReprintObjective = FALSE
			emissionstage = MISSION_STAGE_DROP_THE_SUB_AT_THE_AIRSTRIP
		BREAK

		
	ENDSWITCH

ENDPROC

BOOL bTurnEnginesOff
VEHICLE_INDEX vehTempCutsceneIndex 
ENTITY_INDEX entityTempCutsceneIndex

/// PURPOSE:
/// Play end cutscene
PROC RUN_END_CUTSCENE()
	
	e_section_stage = SECTION_STAGE_SETUP
	i_current_event = 0
	bcutsceneplaying = TRUE
	b_skipped_mocap = FALSE

	#IF IS_DEBUG_BUILD
		IF IS_CUTSCENE_ACTIVE()
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
				b_skipped_mocap = TRUE
				e_section_stage = SECTION_STAGE_SKIP
			ENDIF
		ENDIF
	#ENDIF

	WHILE bcutsceneplaying
	
		WAIT(0)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ThePortOfLSHeist2B")

		IF e_section_stage = SECTION_STAGE_SETUP
			IF b_is_jumping_directly_to_stage
				b_is_jumping_directly_to_stage = FALSE
			ELSE
				IF i_current_event = 0
					bcutsceneplaying = TRUE
					SET_BUILDING_STATE(BUILDINGNAME_IPL_AIRFIELD_PROPS, BUILDINGSTATE_DESTROYED)
					REQUEST_CUTSCENE("LSDH_2B_MCS_1")
					bTurnEnginesOff = FALSE
					bClearCutscenArea = FALSE
					
					i_current_event++
				ELIF i_current_event = 1
					IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()

						IF NOT IS_PED_INJURED(pedWade)
							REGISTER_ENTITY_FOR_CUTSCENE(pedWade, "Wade", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF NOT IS_PED_INJURED(pedLester)
							REGISTER_ENTITY_FOR_CUTSCENE(pedLester, "Lester", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF GET_PLAYER_VEH_MODEL(CHAR_MICHAEL) = TAILGATER
							IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MICHAELS_CAR].veh)
								REGISTER_ENTITY_FOR_CUTSCENE(mission_veh[MV_MICHAELS_CAR].veh, "michaels_car", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
								REGISTER_ENTITY_FOR_CUTSCENE(NULL, "showroom_car", CU_DONT_ANIMATE_ENTITY,PREMIER)
							ENDIF
						ELSE
							IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MICHAELS_CAR].veh)
								REGISTER_ENTITY_FOR_CUTSCENE(mission_veh[MV_MICHAELS_CAR].veh, "Showroom_Car", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
								REGISTER_ENTITY_FOR_CUTSCENE(NULL, "michaels_car", CU_DONT_ANIMATE_ENTITY,TAILGATER)
								PRINTSTRING("Michaels car is not a tailgater and is being registered with handle showroom_car")
							ENDIF
						ENDIF
						
						
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
							REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						ENDIF

						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							IF IS_ENTITY_ATTACHED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							AND NOT IS_PED_IN_ANY_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
								DETACH_ENTITY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							ENDIF
							REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "Franklin", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
							DETACH_ENTITY(mission_veh[MV_SUBMERISIBLE].veh)
							FREEZE_ENTITY_POSITION(mission_veh[MV_SUBMERISIBLE].veh, FALSE)
							REGISTER_ENTITY_FOR_CUTSCENE(mission_veh[MV_SUBMERISIBLE].veh, "Submarine", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
							REGISTER_ENTITY_FOR_CUTSCENE(mission_veh[MV_CHINOOK].veh, "FBI_5_Heli", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
							SET_VEHICLE_ENGINE_ON(mission_veh[MV_CHINOOK].veh,FALSE,FALSE)
						ENDIF
						
						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
							REMOVE_PED_HELMET(PLAYER_PED_ID(),TRUE)
							SET_ENABLE_SCUBA(PLAYER_PED_ID(),FALSE)
							//CLEAR_PED_SCUBA_GEAR_VARIATION(PLAYER_PED_ID())
						ENDIF
						
						IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
							REMOVE_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRUE)
							SET_ENABLE_SCUBA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],FALSE)
						ENDIF
		
						CLEAR_HELP()
						
						//SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, FALSE, FALSE)	
						START_CUTSCENE()
						
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
	
						//Wait a couple of frames before clearing/warping stuff, so the cutscene is active first.

						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						
						b_skipped_mocap = FALSE
						i_current_event = 0
						e_section_stage = SECTION_STAGE_RUNNING
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_RUNNING
			
			
			//CONTROL_FADE_IN(500)

			IF NOT bTurnEnginesOff
				entityTempCutsceneIndex = GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("FBI_5_Heli")
				IF DOES_ENTITY_EXIST(entityTempCutsceneIndex)
					vehTempCutsceneIndex =GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entityTempCutsceneIndex) 
					IF IS_VEHICLE_DRIVEABLE(vehTempCutsceneIndex)
						SET_VEHICLE_ENGINE_ON(vehTempCutsceneIndex,FALSE,FALSE)
						bTurnEnginesOff = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			IF bClearCutscenArea = FALSE
				IF IS_CUTSCENE_PLAYING()
					
					IF IS_SCREEN_FADED_OUT()
						DO_SCREEN_FADE_IN(500)
					ELSE
						WAIT(0)
					ENDIF
				
					IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_TREVOR
				      MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_TREVOR)
				      TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE,TCF_CLEAR_TASK_INTERRUPT_CHECKS )
				      
					  CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_EARS)
				      SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], rel_group_buddies)
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						SET_VEHICLE_ENGINE_ON(mission_veh[MV_CHINOOK].veh,FALSE,FALSE)
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						SET_VEHICLE_ENGINE_ON(mission_veh[MV_CHINOOK].veh,FALSE,FALSE)
					ENDIF
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						REMOVE_PED_HELMET(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRUE)
					ENDIF
					IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
						SET_ENABLE_SCUBA(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],FALSE)
					ENDIF
					
					IF DOES_ENTITY_EXIST(objSeabed[1])
						DELETE_OBJECT(objSeabed[1])
					ENDIF
					
					bClearCutscenArea = TRUE
				//ELSE
					//IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					//	SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					//ENDIF
				ENDIF
			ENDIF
			
			IF IS_CUTSCENE_PLAYING()
				IF GET_CUTSCENE_TIME() > 75000
					PRINTLN("DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_SKIP_CUTSCENE)")
					DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_SKIP_CUTSCENE)
				ENDIF
				IF NOT b_skipped_mocap
				AND bClearCutscenArea = TRUE
					IF WAS_CUTSCENE_SKIPPED()
						b_skipped_mocap = TRUE
					ENDIF
				ENDIF
				
				//Set Michaels cars colours for bug 1868378
				IF bMichaelsCarColoursGrabbed = TRUE
				AND bMichaelsCarColoursSet = FALSE
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Showroom_Car"))
						IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Showroom_Car")))
							PRINTSTRING("vehicle colours being set are ") PRINTINT(icolour1) PRINTINT(icolour2) PRINTNL()
							SET_VEHICLE_COLOURS(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("Showroom_Car")), icolour1, icolour2)
							bMichaelsCarColoursSet = TRUE
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Trevor")
				SET_PED_MOVEMENT_CLIPSET(PLAYER_PED_ID(), Get_Drunk_Level_Moveclip(DL_verydrunk), 0.0)
				MAKE_PED_DRUNK(PLAYER_PED_ID(), 30000)
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				ENDIF
			ENDIF	
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lester")
				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_car")
				
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("FBI_5_Heli")
				IF DOES_ENTITY_EXIST(mission_veh[MV_CHINOOK].veh)
					DELETE_VEHICLE(mission_veh[MV_CHINOOK].veh)
				ENDIF
			ENDIF
			
			IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Submarine")
				IF DOES_ENTITY_EXIST(mission_veh[MV_SUBMERISIBLE].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
						SET_ENTITY_COORDS_NO_OFFSET(mission_veh[MV_SUBMERISIBLE].veh, <<1763.7795, 3289.0815, 41.8658>>)
//						SET_VEHICLE_ON_GROUND_PROPERLY(mission_veh[MV_SUBMERISIBLE].veh)
					ENDIF
				ENDIF
			ENDIF
			

			IF HAS_CUTSCENE_FINISHED()
			AND bClearCutscenArea = TRUE
				WHILE IS_SCREEN_FADING_OUT()
					WAIT(0)
				ENDWHILE
				IF IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
				e_section_stage = SECTION_STAGE_CLEANUP
			ENDIF
		ENDIF

		IF e_section_stage = SECTION_STAGE_CLEANUP

			
			IF b_skipped_mocap
				REPLAY_CANCEL_EVENT()
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
				//If the player skipped the mocap the peds need to be warped.
				WHILE IS_CUTSCENE_ACTIVE()
					WAIT(0)
				ENDWHILE
			ENDIF
		
			REPLAY_STOP_EVENT()
			//Setup buddy relationship groups etc.
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WaitingForPlayerControlInterrupt, TRUE)
			REMOVE_CUTSCENE()
			i_current_event = 0
			bcutsceneplaying = FALSE
			e_section_stage = SECTION_STAGE_SETUP
		ENDIF
			
		IF e_section_stage = SECTION_STAGE_SKIP
			STOP_CUTSCENE()
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_WaitingForPlayerControlInterrupt, TRUE)
			b_skipped_mocap = TRUE
			e_section_stage = SECTION_STAGE_RUNNING
		ENDIF
	ENDWHILE
ENDPROC

INT iGrappleHelpTime
INT iDroppedTimer

PROC DROP_THE_SUB_AT_THE_AIRSTRIP()

SET_ALLOW_CUSTOM_VEHICLE_DRIVE_BY_CAM_THIS_UPDATE(TRUE)

IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<1750.6334, 3285.0278, 40.0871>>,<<250,250,130>>)
		SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(mission_veh[MV_CHINOOK].veh,0.0)
	ELSE
		SET_AIRCRAFT_PILOT_SKILL_NOISE_SCALAR(mission_veh[MV_CHINOOK].veh,1.0)
	ENDIF
ENDIF

//TOGGLE CUTSCENE STREAMING
IF GET_DISTANCE_BETWEEN_COORDS(<<1754.4716, 3283.0715, 40.0925>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) < 300
	IF bStreamCutscene = FALSE
		bStreamCutscene = TRUE
		REQUEST_CUTSCENE("LSDH_2B_MCS_1")
	ENDIF
ELSE
	IF bStreamCutscene = TRUE
		IF GET_DISTANCE_BETWEEN_COORDS(<<1754.4716, 3283.0715, 40.0925>>,GET_ENTITY_COORDS(PLAYER_PED_ID())) > 400
			REMOVE_CUTSCENE()
			bStreamCutscene = FALSE
		ENDIF
	ENDIF
ENDIF

//Grab Michaels cars colours for bug 1868378
IF bMichaelsCarColoursGrabbed = FALSE
	IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MICHAELS_CAR].veh)
		GET_VEHICLE_COLOURS(mission_veh[MV_MICHAELS_CAR].veh, icolour1, icolour2)
		bMichaelsCarColoursGrabbed = TRUE
	ENDIF
ENDIF

SWITCH iprogress

	CASE 0  //setup
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
			IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK)
			ENDIF
		ENDIF
		bReprintObjective = FALSE
		KILL_ANY_CONVERSATION()
		//PRINT_NOW("DS2_RELSUB",DEFAULT_GOD_TEXT_TIME,0)

		//DIALOGUE
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			ADD_PED_FOR_DIALOGUE(sSpeech, 1, PLAYER_PED_ID(), "MICHAEL")
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_TREVOR],TRUE)
			ADD_PED_FOR_DIALOGUE(sSpeech, 2, sSelectorPeds.pedID[SELECTOR_PED_TREVOR], "TREVOR")
		ENDIF
		
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			ADD_PED_FOR_DIALOGUE(sSpeech, 3, sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "FRANKLIN")
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
			FREEZE_ENTITY_POSITION(mission_veh[0].veh,FALSE)
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("DH_2B_SEE_PLANE")
			STOP_AUDIO_SCENE("DH_2B_SEE_PLANE")
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
			FORCE_SUBMARINE_SURFACE_MODE(mission_veh[MV_SUBMERISIBLE].veh,TRUE)
		ENDIF
	
		SET_BUILDING_STATE(BUILDINGNAME_IPL_AIRFIELD_PROPS, BUILDINGSTATE_DESTROYED)
		CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
		bReprintObjective = FALSE
		iSwitchStage = 0
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(9,"Drop the sub at airstrip") 
		CONTROL_FADE_IN(500)
		iDroppedTimer = -1
		
		iprogress++
	BREAK
	
	CASE 1
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
		
			IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,<<1754.4716, 3283.0715, 40.0925>>,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,TRUE,mission_veh[0].veh,"DS2_DROP_SUB","DS2_FLYGI","DS2_FLYGB",TRUE)		
			
			ELSE		
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),<<1750.6334, 3285.0278, 40.0871>>,<<250,250,130>>)
						IF bReprintObjective = TRUE
							IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_FLYTLOD")
								CLEAR_PRINTS()
							ENDIF
							IF DOES_BLIP_EXIST(dest_blip)
								REMOVE_BLIP(dest_blip)
							ENDIF
							PRINT_NOW("DS2_DROP_SUB",DEFAULT_GOD_TEXT_TIME,0)
							bReprintObjective = FALSE
						ENDIF
						
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<1683.87231, 3247.42944, 39.81261>>, <<1804.12073, 3280.57031, 63.77226>>, 65)
								
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_GRAP_HELP2")
								IF GET_GAME_TIMER() >= iGrappleHelpTime + 1000
									PRINT_HELP_FOREVER("DS2_GRAP_HELP2")
									iGrappleHelpTime = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ELSE
							IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
								IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
									DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK)
								ENDIF
							ENDIF
							
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("DS2_GRAP_HELP2")
								IF GET_GAME_TIMER() >= iGrappleHelpTime + 1000
									CLEAR_HELP()
									iGrappleHelpTime = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
							IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
								DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_GRAPPLING_HOOK)
							ENDIF
						ENDIF
						IF bReprintObjective = FALSE
							CLEAR_PRINTS()
							//dest_blip = ADD_BLIP_FOR_COORD(<<-1388.26, 6160.88, 0.34>>)
							PRINT_NOW("DS2_FLYTLOD",DEFAULT_GOD_TEXT_TIME,0)
							bReprintObjective = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
			AND IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				IF NOT IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
						STABILISE_ENTITY_ATTACHED_TO_HELI(mission_veh[MV_CHINOOK].veh,NULL,iStabliseLength)
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
						IF TRIGGER_MUSIC_EVENT("DH2B_SUB_RETURNED")
							DETACH_ENTITY(mission_veh[MV_SUBMERISIBLE].veh)
							IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
								REMOVE_BLIP(sLocatesData.LocationBlip)
							ENDIF
							IF IS_THIS_PRINT_BEING_DISPLAYED("DS2_DROP_SUB")
								CLEAR_PRINTS()
							ENDIF
							CLEAR_HELP()
							//SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
							SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_FRANKLIN,TRUE)
							SET_SELECTOR_PED_BLOCKED(sSelectorPeds,SELECTOR_PED_TREVOR,FALSE)
							sCamDetails.bSplineCreated = FALSE
							REQUEST_CUTSCENE("LSDH_2B_MCS_1")
							iprogress++
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
		ENDIF
	BREAK
	
	CASE 2
		IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
			REMOVE_BLIP(sLocatesData.LocationBlip)
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh)
			IF iDroppedTimer < 0
				IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(mission_veh[1].veh)
					PLAY_SOUND_FROM_ENTITY(-1, "DOCKS_HEIST_FINALE_2A_SUB_LAND",mission_veh[MV_SUBMERISIBLE].veh,"DOCKS_HEIST_FINALE_2A_SOUNDS")
					//INFORM_MISSION_STATS_OF_INCREMENT(DH2B_ITEMS_STOLEN) 
					iDroppedTimer = GET_GAME_TIMER()
				ENDIF
			ELSE
				IF GET_GAME_TIMER() >= iDroppedTimer + 3000
					iprogress++
				ENDIF
			ENDIF
		ENDIF
	BREAK
	
	CASE 3
		iprogress = 0
		emissionstage = MISSION_STAGE_CLOSING_CUTSCENE
	BREAK
	
ENDSWITCH

ENDPROC

FUNC BOOL REQUEST_DRUNK_ANIMS()
	STRING m_movementClip = Get_Drunk_Level_Moveclip(DL_verydrunk)
	REQUEST_CLIP_SET(m_movementClip)
	IF NOT HAS_CLIP_SET_LOADED(m_movementClip)
	      RETURN FALSE
	ELSE
		RETURN TRUE
	ENDIF
ENDFUNC

PROC CLOSING_CUTSCENE()

SWITCH iprogress

	CASE 0
		IF IS_AUDIO_SCENE_ACTIVE("DH_2B_RETURN_TO_AIRSTRIP")
			STOP_AUDIO_SCENE("DH_2B_RETURN_TO_AIRSTRIP")
		ENDIF
		SET_BUILDING_STATE(BUILDINGNAME_IPL_AIRFIELD_PROPS, BUILDINGSTATE_DESTROYED)
		bReprintObjective = FALSE
		KILL_ANY_CONVERSATION()
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
			FREEZE_ENTITY_POSITION(mission_veh[0].veh,FALSE)
		ENDIF
		REQUEST_DRUNK_ANIMS()
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(10,"Closing Cutscene") 
		bReprintObjective = FALSE
		iSwitchStage = 0
//		CONTROL_FADE_IN(500)
		
		REPLAY_RECORD_BACK_FOR_TIME(6.0, 0.0, REPLAY_IMPORTANCE_HIGHEST)
		
		iprogress++
	BREAK
	
	CASE 1
		IF REQUEST_DRUNK_ANIMS()
			RUN_END_CUTSCENE()
			iprogress++	
		ENDIF
	BREAK
	
	CASE 2
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		iprogress = 0
		emissionstage = MISSION_STAGE_PASSED
	BREAK

ENDSWITCH

ENDPROC

PROC MANAGE_AI_PLAYER_BEHAVIOR()

	//current_player_model = GET_CURRENT_PLAYER_PED_ENUM()

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
				IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) <> PERFORMING_TASK
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],150)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
				IF GET_SCRIPT_TASK_STATUS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) <> PERFORMING_TASK
					TASK_COMBAT_HATED_TARGETS_AROUND_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],150)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(buddy[1].ped)
		IF GET_SCRIPT_TASK_STATUS(buddy[1].ped,SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
			IF GET_SCRIPT_TASK_STATUS(buddy[1].ped,SCRIPT_TASK_COMBAT_HATED_TARGETS_AROUND_PED) <> PERFORMING_TASK
				TASK_COMBAT_HATED_TARGETS_AROUND_PED(buddy[1].ped,150)
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			SWITCH iMikeAI
				CASE 0
					IF bTaskNonPlayer = TRUE
						SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],1)
						SET_PED_COMBAT_MOVEMENT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],CM_DEFENSIVE)
						SET_PED_COMBAT_RANGE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],CR_FAR)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],FALSE)
						SET_PED_SPHERE_DEFENSIVE_AREA(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]),30)
						SET_PED_TO_LOAD_COVER(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],TRUE)
						SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],FALSE)
						SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] ,rel_group_buddies)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,rel_group_buddies)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,RELGROUPHASH_PLAYER)
						SET_PED_AS_ENEMY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], FALSE)
						CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL],150)
						bTaskNonPlayer = FALSE
					ENDIF
				BREAK
			
			ENDSWITCH
		ENDIF
	ENDIF
	
	//FINISH BUDDY AI
	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		IF brappelled = TRUE
			IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				SWITCH iFrankAI
					CASE 0
						IF bTaskNonPlayer = TRUE
							SET_PED_ACCURACY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],100)
							SET_PED_COMBAT_MOVEMENT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],CM_DEFENSIVE)
							SET_PED_COMBAT_RANGE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],CR_FAR)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],FALSE)
							SET_PED_SPHERE_DEFENSIVE_AREA(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]),30)
							SET_PED_TO_LOAD_COVER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],TRUE)
							SET_PED_CAN_BE_TARGETTED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],FALSE)
							SET_PED_RELATIONSHIP_GROUP_HASH(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] ,rel_group_buddies)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,rel_group_buddies)
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE,rel_group_buddies,RELGROUPHASH_PLAYER)
							SET_PED_AS_ENEMY(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], FALSE)
							CLEAR_PED_TASKS(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN],150)
							bTaskNonPlayer = FALSE
						ENDIF
					BREAK
				
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT IS_PED_INJURED(buddy[1].ped)
		SWITCH iGunmanAI
		
			CASE 0
				SET_PED_ACCURACY(buddy[1].ped,0)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(buddy[1].ped,FALSE)
				CLEAR_PED_TASKS(buddy[1].ped)
				GIVE_WEAPON_TO_PED(buddy[1].ped,WEAPONTYPE_ADVANCEDRIFLE,800,TRUE)
				
				iTimer3 = GET_GAME_TIMER()
				iGunmanAI++
			BREAK
			
			CASE 1
				IF NOT IS_PED_INJURED(buddy[1].ped)
					IF MANAGE_MY_TIMER(iTimer3,45000)
					OR (IS_PED_INJURED(enemy[40].ped) AND IS_PED_INJURED(enemy[41].ped))
						CLEAR_PED_TASKS(buddy[1].ped)
						SET_PED_SPHERE_DEFENSIVE_AREA(buddy[1].ped,GET_ENTITY_COORDS(PLAYER_PED_ID()),10)
						OPEN_SEQUENCE_TASK(seq)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL,TRUE)
							TASK_SET_SPHERE_DEFENSIVE_AREA(NULL,<< 530.0932, -3121.0486, 5.0693 >>,2)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< 530.0932, -3121.0486, 5.0693 >>,2.0)
							TASK_PUT_BUDDY_INTO_CUSTOM_COVER(1,<< 530.0932, -3121.0486, 5.0693 >>,<<521.23, -3123.77, 5.07>>,89.2291,-1,COVUSE_WALLTONEITHER,COVHEIGHT_LOW,COVARC_180,TRUE,2000)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL,FALSE)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL,150)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(buddy[1].ped, seq)
						iTimer3 = GET_GAME_TIMER()
						iGunmanAI++
					ENDIF
				ENDIF
			BREAK 
			
			CASE 2
				IF NOT IS_PED_INJURED(buddy[1].ped)
					IF MANAGE_MY_TIMER(iTimer3,30000)
						CLEAR_PED_TASKS(buddy[1].ped)
						SET_PED_SPHERE_DEFENSIVE_AREA(buddy[1].ped,GET_ENTITY_COORDS(PLAYER_PED_ID()),10)
						OPEN_SEQUENCE_TASK(seq)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL,TRUE)
							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL,<< 522.4401, -3126.6421, 5.0797 >>,2.0)
							TASK_SET_SPHERE_DEFENSIVE_AREA(NULL,<< 522.4401, -3126.6421, 5.0797 >>,2)
							TASK_PUT_BUDDY_INTO_CUSTOM_COVER(1,<< 522.4496, -3126.7852, 5.0797 >>,<<6.08, -3127.67, 5.0792>>,87.9567,-1,COVUSE_WALLTONEITHER,COVHEIGHT_LOW,COVARC_180,TRUE,2000)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL,50)
							TASK_SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(NULL,FALSE)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(buddy[1].ped, seq)
						iGunmanAI++
					ENDIF
				ENDIF
			BREAK
			
			CASE 3
				IF NOT IS_PED_INJURED(buddy[1].ped)
					IF MANAGE_MY_TIMER(iTimer3,25000)
						CLEAR_PED_TASKS(buddy[1].ped)
						SET_PED_SPHERE_DEFENSIVE_AREA(buddy[1].ped,GET_ENTITY_COORDS(PLAYER_PED_ID()),10)
						OPEN_SEQUENCE_TASK(seq)
							TASK_SET_SPHERE_DEFENSIVE_AREA(NULL,<< 497.6558, -3126.0911, 5.0699 >>,2)
							TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, << 497.6558, -3126.0911, 5.0699 >>, << 497.6558, -3126.0911, 5.0699 >>,1,FALSE)
							TASK_PUT_BUDDY_INTO_CUSTOM_COVER(1,<< 516.2747, -3121.4597, 5.0694 >>,<<507.83, -3125.23, 5.07>>,85.4177,-1,COVUSE_WALLTONEITHER,COVHEIGHT_LOW,COVARC_180,TRUE,2000)
							TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL,50)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(buddy[1].ped, seq)
						iGunmanAI++
					ENDIF
				ENDIF
				
			BREAK
		
		ENDSWITCH
	ENDIF
	

ENDPROC

PROC MANAGE_WEAPON_PICKUP()

	//objAmmo
	
	IF current_player_model = CHAR_MICHAEL
		IF DOES_ENTITY_EXIST(objAmmo)
			IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(),objAmmo,<<2,2,2>>)
				IF bHasPedGotAmmo = FALSE
					GIVE_WEAPON_TO_PED(PLAYER_PED_ID(),WEAPONTYPE_GRENADELAUNCHER,10,TRUE)
				   bHasPedGotAmmo = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


PROC ESCAPE()

	

	SWITCH iprogress
	
		
	ENDSWITCH
		
ENDPROC


FUNC BOOL SUBMARINE_CUTSCENCE()
		
	#IF IS_DEBUG_BUILD // allows j/p skip to work even in cutscene
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)OR IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
			bcutsceneplaying = FALSE
		ENDIF
	#ENDIF
	
	IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY(CUTSCENE_SKIP_DELAY) AND icutsceneprog >1 // controls the cutscene skip
		SETTIMERB(24000)
		CONTROL_FADE_OUT(500)
		icutsceneprog = 2
	ENDIF
	
	SWITCH icutsceneprog
		
		CASE 0
			//INFORM_MISSION_STATS_SYSTEM_OF_INENGINE_CUTSCENE_START()
			SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
			KILL_ANY_CONVERSATION()
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			DESTROY_ALL_CAMS()
			SETTIMERB(0)
			icutsceneprog++
		BREAK
		
		CASE 1
			cutscene_cam = CREATE_CAM("DEFAULT_SPLINE_CAMERA")
			ADD_CAM_SPLINE_NODE_USING_CAMERA(cutscene_cam, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<884.3281, -2759.3745, 3.5123>>, <<-8.0736, 0.0000, 28.4463>>, 50.0000, TRUE), 0, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(cutscene_cam, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<795.9953, -2613.6792, 6.0652>>, <<-8.4602, 0.0000, 84.4648>>, 50.0000, TRUE), 3000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(cutscene_cam, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<711.9581, -2602.5859, 4.9945>>, <<-3.7230, 0.0000, 71.7792>>, 50.0000, TRUE), 3000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(cutscene_cam, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<645.2623, -2560.9226, 5.1685>>, <<-11.9397, 0.0000, 45.8648>>, 50.0000, TRUE), 3000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(cutscene_cam, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<480.9850, -2415.6716, 2.2832>>, <<-5.7036, 0.0000, 46.8128>>, 50.0000, TRUE), 3000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(cutscene_cam, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<376.5441, -2317.0681, 2.3515>>, <<-7.2357, 0.0000, 87.1989>>, 50.0000, TRUE), 3000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(cutscene_cam, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-4.3359, -2319.6748, 2.8484>>, <<-6.3337, 0.0000, 88.7135>>, 50.0000, TRUE), 6000, CAM_SPLINE_NODE_SMOOTH_ROT)
			ADD_CAM_SPLINE_NODE_USING_CAMERA(cutscene_cam, CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-102.8173, -2334.7722, 13.5672>>, <<-6.1283, 0.0000, 135.9349>>, 50.0000, TRUE), 3000, CAM_SPLINE_NODE_SMOOTH_ROT)

			SET_CAM_ACTIVE(cutscene_cam, TRUE)
			SHAKE_CAM(cutscene_cam, "HAND_SHAKE", 0.3000)
			RENDER_SCRIPT_CAMS(TRUE, TRUE)
			icutsceneprog++
		BREAK
		
		CASE 2
		
			IF TIMERB() > 24000
				IF DOES_CAM_EXIST(cutscene_cam)
					SET_CAM_ACTIVE(cutscene_cam,FALSE)
					DESTROY_CAM(cutscene_cam)
					RENDER_SCRIPT_CAMS(FALSE,TRUE)
				ENDIF
				
				SET_WIDESCREEN_BORDERS(FALSE,0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING()
				CONTROL_FADE_IN(1000)
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				CLEAR_PRINTS()
				PRINTSTRING("quad cut end")
				PRINTNL()
				icutsceneprog = 0
				//INFORM_MISSION_STATS_SYSTEM_OF_INENGINE_CUTSCENE_END()
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
				KILL_ANY_CONVERSATION()
				bcutsceneplaying = FALSE
				RETURN TRUE
			ENDIF
		
		BREAK

	ENDSWITCH
	
	RETURN FALSE

ENDFUNC


FUNC FLOAT CAP_HEADING(FLOAT fHeading)

	IF fHeading > 360
		fHeading = fHeading - 360
	ENDIF
	
	IF fHeading < 0
		fHeading = fHeading * -1
		fHeading = 360 - fHeading
	ENDIF

	RETURN fHeading

ENDFUNC

//
//PROC EXPLODE_MISSILE(MISSILE_DATA &s_missile)
////	IF IS_VEHICLE_DRIVEABLE(veh_gps_helper)
////		DETACH_VEHICLE(veh_gps_helper)
////		FREEZE_VEHICLE_POSITION_AND_DONT_LOAD_COLLISION(veh_gps_helper, TRUE)
////	ENDIF
////	
//	//ADD_EXPLOSION(s_uav.v_pos, EXP_TAG_ROCKET, 1.0, TRUE, FALSE, 1.0)
//	s_missile.b_destroyed = TRUE
//	
////	IF s_missile.i_ptfx != -1
////		STOP_PARTICLE_FX_LOOPED(s_missile.i_ptfx)
////		s_missile.i_ptfx = -1
////	ENDIF
//	
//	//DETACH_PED(PLAYER_PED_ID())
//	//DELETE_OBJECT(s_uav.obj)
//ENDPROC

PROC MANAGE_MISSILE_MOVEMENT(MISSILE_DATA &s_this_missile)
	
	//CONST_FLOAT f_MAX_VEL				25.0
	CONST_FLOAT f_MAX_VERTICAL_VEL		15.0
	CONST_FLOAT f_ACCEL					0.5
	CONST_FLOAT f_VERTICAL_ACCEL		0.5
	CONST_FLOAT f_MAX_TURN_VEL			40.0
	CONST_FLOAT f_TURN_ACCEL			2.5
	CONST_FLOAT f_MAX_HEIGHT			8.0
	CONST_FLOAT f_FUEL_LOSS_RATE		0.01
	CONST_FLOAT f_FUTURE_CHECK_DIST		4.0
	CONST_FLOAT f_MAX_PITCH_VEL			1.5
	CONST_FLOAT f_PITCH_ACCEL			0.05
	FLOAT f_lower_min_height			= 1.5
	FLOAT f_upper_min_height			= 1.6
	
	IF NOT  s_this_missile.b_destroyed
		IF DOES_ENTITY_EXIST( s_this_missile.obj)
			//unref
			IF IS_VECTOR_ZERO(s_this_missile.v_pos)
			
			ENDIF
			IF IS_VECTOR_ZERO(s_this_missile.v_rot)
			
			ENDIF
			IF s_this_missile.f_dist_from_target < 0
			
			ENDIF
			IF s_this_missile.f_turn_vel < 0
			
			ENDIF
			IF s_this_missile.f_forward_vel < 0
			
			ENDIF
			IF IS_VECTOR_ZERO(s_this_missile.v_vel)
			
			ENDIF
			
			s_this_missile.v_pos = GET_ENTITY_COORDS(s_this_missile.obj)
			s_this_missile.f_dist_from_target = DISTANCE_BETWEEN_COORDS(s_this_missile.v_pos, v_targets[i_current_target])
		
			IF (NOT HAS_ENTITY_COLLIDED_WITH_ANYTHING(s_this_missile.obj) AND s_this_missile.v_pos.z > 4.0) //OR b_disable_uav_collisions OR b_photo_taken
			INT i_left_stick_x//, i_left_stick_y, i_right_stick_x, i_right_stick_y
			//GET_POSITION_OF_ANALOGUE_STICKS(PAD1, i_left_stick_x, i_left_stick_y, i_right_stick_x, i_right_stick_y)
			i_left_stick_x = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_X) - 128
//			i_left_stick_y = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_AXIS_Y) - 128
//			i_right_stick_x = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_X) - 128
//			i_right_stick_y = GET_CONTROL_VALUE(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT_AXIS_Y) - 128
			
			//Calculate left/right turn speed and new angle
			FLOAT f_turn_ratio = TO_FLOAT(i_left_stick_x) / 128.0
			CONVERGE_VALUE(s_this_missile.f_turn_vel, -f_turn_ratio * f_MAX_TURN_VEL, f_TURN_ACCEL)
			s_this_missile.v_rot.z = CAP_HEADING(s_this_missile.v_rot.z +@ s_this_missile.f_turn_vel)
			s_this_missile.v_rot.y = -(s_this_missile.f_turn_vel / f_MAX_TURN_VEL) * 8.0
			SET_ENTITY_ROTATION(s_this_missile.obj, s_this_missile.v_rot)
			
			//Calculate and apply new velocity
			IF s_this_missile.f_fuel > 0.0
				CONVERGE_VALUE(s_this_missile.f_forward_vel, f_uav_vel, f_ACCEL)
			ELSE
				CONVERGE_VALUE(s_this_missile.f_forward_vel, 0.0, f_ACCEL / 10.0)
			ENDIF
			s_this_missile.v_vel.x = s_this_missile.f_forward_vel * -SIN(s_this_missile.v_rot.z)
			s_this_missile.v_vel.y = s_this_missile.f_forward_vel * COS(s_this_missile.v_rot.z)
			
			IF s_this_missile.f_fuel > 0.0
				//Try to keep z position a fixed distance above ground: do this by increasing/decreasing vertical speed if the adjacent ground is too close/far away
				VECTOR v_unit = s_this_missile.v_vel / VMAG(<<s_this_missile.v_vel.x, s_this_missile.v_vel.y, 0.0>>)
				VECTOR v_future_pos = <<s_this_missile.v_pos.x + (v_unit.x * f_FUTURE_CHECK_DIST), s_this_missile.v_pos.y + (v_unit.y * f_FUTURE_CHECK_DIST), s_this_missile.v_pos.z>>
								
				FLOAT f_future_ground_z = 0.0
				GET_GROUND_Z_FOR_3D_COORD(v_future_pos + <<0.0, 0.0, 2.0>>, f_future_ground_z)
				IF f_future_ground_z < 4.0
					f_future_ground_z = 4.0
				ENDIF
				FLOAT f_future_height_diff = s_this_missile.v_pos.z - f_future_ground_z
				
				//Do an additional check in case the height change is very steep (doing this as a separate check helps with going under arches)
				IF f_future_height_diff < 0.0
					GET_GROUND_Z_FOR_3D_COORD(<<v_future_pos.x, v_future_pos.y, 1000.0>>, f_future_ground_z)
					IF f_future_ground_z < 4.0
						f_future_ground_z = 4.0
					ENDIF
					f_future_height_diff = s_this_missile.v_pos.z - f_future_ground_z
				ENDIF
			
				FLOAT f_desired_z_vel = 0.0	
				IF f_future_height_diff > f_upper_min_height
					//Move the UAV downwards if it's too high
					f_desired_z_vel = -(((f_future_height_diff - f_upper_min_height) / (f_MAX_HEIGHT - f_upper_min_height)) * (f_MAX_VERTICAL_VEL))
					IF f_desired_z_vel < -f_MAX_VERTICAL_VEL
						f_desired_z_vel = -f_MAX_VERTICAL_VEL
					ENDIF
					
					CONVERGE_VALUE(s_this_missile.v_vel.z, f_desired_z_vel, f_VERTICAL_ACCEL)
				ELIF f_future_height_diff < f_lower_min_height
					//Move the UAV upwards if it's too low
					f_desired_z_vel = -(((f_future_height_diff - f_lower_min_height) / (f_MAX_HEIGHT - f_lower_min_height)) * (f_MAX_VERTICAL_VEL * 2.0))
					IF f_desired_z_vel > f_MAX_VERTICAL_VEL
						f_desired_z_vel = f_MAX_VERTICAL_VEL
					ENDIF
					
					CONVERGE_VALUE(s_this_missile.v_vel.z, f_desired_z_vel, f_VERTICAL_ACCEL * 2.0)
				ENDIF
			ELSE
				CONVERGE_VALUE(s_this_missile.v_vel.z, -10.0, f_VERTICAL_ACCEL)
			ENDIF

			FREEZE_ENTITY_POSITION(s_this_missile.obj, TRUE)
			FREEZE_ENTITY_POSITION(s_this_missile.obj, FALSE)
			SET_ENTITY_INITIAL_VELOCITY(s_this_missile.obj, s_this_missile.v_vel)
		
			ELSE
				//EXPLODE_MISSILE(s_this_missile)
			ENDIF
			
			//PRINTNL()
		ENDIF
	ENDIF
	

ENDPROC

FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR v_rot)
      RETURN <<-SIN(v_rot.z) * COS(v_rot.x), COS(v_rot.z) * COS(v_rot.x), SIN(v_rot.x)>>
ENDFUNC

PROC BLOW_UP_SHIP()

	SWITCH iprogress
	
		CASE 0
			REMOVE_ALL_OBJECTS()
			SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(),FALSE)
			ENDIF
			CONTROL_FADE_IN(500) 
			substolen = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			
			IF IS_VEHICLE_DRIVEABLE(substolen)
				FREEZE_ENTITY_POSITION(substolen,true)
			ENDIF
			
			SET_BUILDING_STATE(BUILDINGNAME_IPL_CARGOSHIP, BUILDINGSTATE_NORMAL)
			//SET_BUILDING_STATE(BUILDINGNAME_IPL_CARGOSHIP_OCCLUSION, BUILDINGSTATE_NORMAL)
			REQUEST_MODEL(PROP_POOL_BALL_01)
			DESTROY_CAM(cutscene_cam)
			
			IF DOES_BLIP_EXIST(slocatesData.LocationBlip)
				SET_GPS_ACTIVE(FALSE)
			ELSE
				SET_GPS_ACTIVE(TRUE)
			ENDIF
			
			IF IS_PHONE_ONSCREEN()
				HANG_UP_AND_PUT_AWAY_PHONE()
			ENDIF
			//bcutsceneplaying = TRUE
			iprogress++
		BREAK
		
		CASE 1	
			iprogress++
		BREAK
		
		CASE 2
			SET_PLAYER_CONTROL(PLAYER_ID(),FALSE)
			DESTROY_ALL_CAMS()
			PRINTSTRING("destroying cam moving to mini game.")PRINTNL()
			WARP_PLAYER(<< 411.9302, -2371.4006, -14.2248 >>, 86.3681,FALSE)
			iprogress++
		BREAK
		
		CASE 3
			 
			 IF bMiniGameSuccess = TRUE
			 	DESTROY_CAM(cutscene_cam)
			 	iprogress++
			 ENDIF
		BREAK

		CASE 4
			IF NOT DOES_CAM_EXIST(cutscene_cam)
				cutscene_cam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA",TRUE) 	
				SET_CAM_PARAMS(cutscene_cam,<< -63.4984, -2298.7693, 12.5430 >>, << -11.4198, -0.0000, 118.8173 >>,50.0000)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				SETTIMERA(0)
				iprogress++
			ENDIF
		BREAK
		
		CASE 5
			IF TIMERA() > 6000
				RENDER_SCRIPT_CAMS(FALSE,TRUE,5000)
				SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				//RESET_MISSION_STAGE_VARIABLES()
				iprogress++
			ENDIF
		BREAK
		
		CASE 6
			MISSION_PASSED()
		BREAK 
		
	ENDSWITCH
		
ENDPROC

PROC MANAGE_FAKE_INTERIOR()
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<531.362305,-3016.687988,4.581850>>, <<523.931213,-3409.380127,48.706970>>, 189.500000)
		VECTOR vTemp = GET_ENTITY_COORDS(PLAYER_PED_ID())
		IF eMissionStage < MISSION_STAGE_PASSED
			IF vTemp.z < 10 //Ground
				SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FakeDockControl"),569.455,- 3122.849 ,0,0)
			ELIF vTemp.z > 10 AND vTemp.z < 21 // First
				SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_FakeDockControl"),569.455,- 3122.849,0,1)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC MANAGE_HINT_CAM()

	IF emissionstage = MISSION_STAGE_PICK_UP_SUB
		IF iprogress < 2
			IF IS_VEHICLE_DRIVEABLE(mission_veh[0].veh)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[1].veh) 
					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),mission_veh[0].veh)
						CONTROL_ENTITY_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, mission_veh[1].veh)
					ELSE
						KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
					ENDIF
				ELSE
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct) 
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

#IF IS_DEBUG_BUILD
	PROC NAME_THE_CRATES()
		
		INT iCrate
		VECTOR vTemp
		
		#IF IS_DEBUG_BUILD
			FOR iCrate = 0 TO MAX_NUMBER_OF_DOCK_PROPS - 1
				IF DOES_ENTITY_EXIST(objcrate[iCrate])
					#IF IS_DEBUG_BUILD
						TEXT_LABEL tDebugName = "CRATE"
						tDebugName += iCrate
						vTemp = GET_ENTITY_COORDS(objcrate[iCrate])
						DRAW_DEBUG_TEXT(tDebugName,<<vTemp.x,vTemp.y,vTemp.z + 1>>)
					#ENDIF
				ENDIF
			ENDFOR
		#ENDIF

	ENDPROC
	
PROC HANDLE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()


	IF eMissionStage > MISSION_STAGE_OPENING_CUTSCENE
		IF bSwitchCamDebugScenarioEnabled
		//OR IS_BUTTON_JUST_PRESSED(PAD1, DPADLEFT)
			bcleanup = FALSE
			bskipping = TRUE
			brunfailchecks = FALSE
			iSetupProgress = 0
			eMissionStage = MISSION_STAGE_DROP_THE_SUB
			SET_SELECTOR_PED_BLOCKED(sSelectorPeds, SELECTOR_PED_MICHAEL, FALSE)
			SET_TIME_SCALE(1.0)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			FREEZE_ENTITY_POSITION(mission_veh[0].veh, FALSE)			
			
			IF MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				WHILE NOT  TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
					PRINTLN("Waiting to control Michael...")
					WAIT(0)
				ENDWHILE
			ENDIF
			
			WHILE bskipping
				LOAD_MISSION_STAGE(MISSION_STAGE_DROP_THE_SUB)
				PRINTLN("bskipping = TRUE")
				WAIT(0)
			ENDWHILE
			bSwitchCamDebugScenarioEnabled = FALSE
		ENDIF
	ENDIF

ENDPROC
#ENDIF

// ********************************** START OF MAIN SCRIPT *********************************

SCRIPT

    SET_MISSION_FLAG (TRUE)
    
    IF HAS_FORCE_CLEANUP_OCCURRED() // death arrest check
		Mission_Flow_Mission_Force_Cleanup()
		TRIGGER_MUSIC_EVENT("DH2B_FAIL")
        MISSION_CLEANUP(TRUE)
    ENDIF
	
	#IF IS_DEBUG_BUILD // debug controls 
		objectwidget =START_WIDGET_GROUP(" dockheistsetup2.SC")
		//objectwidget =START_WIDGET_GROUP("MISSION DEBUG")
			ADD_WIDGET_INT_SLIDER("eMissionStage", ieMissionStageTemp, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("iSetupProgress", iSetupProgress, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("iProgress", iProgress, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("ihotswapProgress", iSwapProg, 0, 99, 1)
			ADD_WIDGET_INT_SLIDER("icutsceneprog", icutsceneprog, 0, 99, 1)
			ADD_WIDGET_INT_READ_ONLY("iHeliAttack",iHeliAttack)
			ADD_WIDGET_INT_READ_ONLY("iFrankAI",iFrankAI)
			ADD_WIDGET_INT_READ_ONLY("iMikeAI",iMikeAI)
			ADD_WIDGET_BOOL("Create Crane",bCreateCrane)
			ADD_WIDGET_FLOAT_SLIDER("f_fuel_mod_turbo",f_fuel_mod_turbo,0,1000,0.01)
			ADD_WIDGET_FLOAT_SLIDER("f_fuel_mod",f_fuel_mod,0,1000,0.01)
			ADD_WIDGET_FLOAT_SLIDER("f_cam_limits_aim_heading_min",f_cam_limits_aim_heading_min,-500,500,0.001)
			ADD_WIDGET_FLOAT_SLIDER("f_cam_limits_aim_heading_max",f_cam_limits_aim_heading_max,-500,500,0.001)
			ADD_WIDGET_FLOAT_SLIDER("f_cam_limits_aim_pitch_min",f_cam_limits_aim_pitch_min,-500,500,0.001)
			ADD_WIDGET_FLOAT_SLIDER("f_cam_limits_aim_pitch_max",f_cam_limits_aim_pitch_max,-500,500,0.001)
			ADD_WIDGET_FLOAT_SLIDER("f_cam_limits_aim_orbit_min",f_cam_limits_aim_orbit_min,-500,500,0.001)
			ADD_WIDGET_FLOAT_SLIDER("f_cam_limits_aim_orbit_max",f_cam_limits_aim_orbit_max,-500,500,0.001)
			ADD_WIDGET_FLOAT_SLIDER("fGameplayCamHeading",fGameplayCamHeading,-500,500,0.001)
			ADD_WIDGET_FLOAT_SLIDER("fGameplayCamPitch",fGameplayCamPitch,-500,500,0.001)
			ADD_WIDGET_VECTOR_SLIDER("vRopeTop1", vRopeTop1, -100.0, 100.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("vRopeBottom1", vRopeBottom1, -100.0, 100.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("vRopeTop2", vRopeTop2, -100.0, 100.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("vRopeBottom2", vRopeBottom2, -100.0, 100.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("vRopeTop3", vRopeTop3, -100.0, 100.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("vRopeBottom3", vRopeBottom3, -100.0, 100.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("vRopeTop4", vRopeTop4, -100.0, 100.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("vRopeBottom4", vRopeBottom4, -100.0, 100.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("vRopeTop5", vRopeTop5, -100.0, 100.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("vRopeBottom5", vRopeBottom5, -100.0, 100.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("vRopeTop6", vRopeTop6, -100.0, 100.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("vRopeBottom6", vRopeBottom6, -100.0, 100.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("vRopeTop7", vRopeTop7, -100.0, 100.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("vRopeBottom7", vRopeBottom7, -100.0, 100.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("vRopeTop8", vRopeTop8, -100.0, 100.0, 0.001)
			ADD_WIDGET_VECTOR_SLIDER("vRopeBottom8", vRopeBottom8, -100.0, 100.0, 0.001)
			
			CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
		STOP_WIDGET_GROUP()
		SET_LOCATES_HEADER_WIDGET_GROUP(objectwidget)
		
		SETUP_SPLINE_CAM_NODE_ARRAY_SUBMARINE_SWITCH(scsSwitchCam_MichaelToTrevor, mission_veh[MV_SUBMERISIBLE].veh)
		
		CREATE_SPLINE_CAM_WIDGETS(scsSwitchCam_MichaelToTrevor, "Sub", "", objectwidget)
	#ENDIF

    // Mission Loop: Loops forever until mission is passed or failed
    WHILE TRUE

        WAIT(0)
				
		IF brunfailchecks // controls fail checks
			
		ENDIF
		
		FAIL_CHECKS()
		
		MANAGE_HINT_CAM()
		
		MANAGE_UNDERWATER_AUDIO()
		MANAGE_PLAYER_WEAPONS()
		CONTROL_SPEECH()
		
		IF eMissionStage > MISSION_STAGE_PICK_UP_THE_SUB_FROM_AIRSTRIP
			//MANAGE_MERRYWEATHER_PTFX()
		ENDIF
		
		IF eMissionStage > MISSION_STAGE_DROP_THE_SUB
		OR eMissionStage < MISSION_STAGE_DEFEND_AGAINST_MERRYWEATHER
			IF DOES_ENTITY_EXIST(mission_veh[MV_SUBMERISIBLE].veh)
				IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
					SET_SUBMARINE_CRUSH_DEPTHS(mission_veh[MV_SUBMERISIBLE].veh,FALSE,-190,-190,-190)
				ENDIF
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(mission_veh[MV_SUBMERISIBLE].veh)
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				SET_SUBMARINE_CRUSH_DEPTHS(mission_veh[MV_SUBMERISIBLE].veh,FALSE,-190,-190,-190)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
			IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
				IF HAS_LABEL_BEEN_TRIGGERED("STABALISE")
					IF NOT IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
						STABILISE_ENTITY_ATTACHED_TO_HELI(mission_veh[MV_CHINOOK].veh,NULL,iStabliseLength)
						SET_LABEL_AS_TRIGGERED("STABALISE",FALSE)
					ENDIF
				ELSE
					IF IS_VEHICLE_ATTACHED_TO_CARGOBOB(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh)
						STABILISE_ENTITY_ATTACHED_TO_HELI(mission_veh[MV_CHINOOK].veh,mission_veh[MV_SUBMERISIBLE].veh,iStabliseLength)
						SET_LABEL_AS_TRIGGERED("STABALISE",TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF

		//CLEAN UP FOR UNUSED SETPIECES
		IF eMissionStage > MISSION_STAGE_PICK_UP_THE_SUB_FROM_AIRSTRIP
		AND eMissionStage < MISSION_STAGE_FIND_THE_CRATE
			IF HAS_LABEL_BEEN_TRIGGERED("SEE THE PLANE")
				IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh)
							IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh)
								DELETE_VEHICLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE].veh)
								IF DOES_ENTITY_EXIST(driver[10].ped)
									DELETE_PED(driver[10].ped)
								ENDIF
								SET_MODEL_AS_NO_LONGER_NEEDED(LAZER)
								SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BlackOps_01)
								SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_02)
								REMOVE_VEHICLE_RECORDING(26,"DHF2")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CARGOPLANE1].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE1].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE1].veh)
							IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_MERRY_WEATHER_CARGOPLANE1].veh)
								DELETE_VEHICLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE1].veh)
								IF DOES_ENTITY_EXIST(driver[11].ped)
									DELETE_PED(driver[11].ped)
								ENDIF
								SET_MODEL_AS_NO_LONGER_NEEDED(LAZER)
								SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BlackOps_01)
								SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_02)
								REMOVE_VEHICLE_RECORDING(26,"DHF2")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_CARGOPLANE2].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE2].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE2].veh)
							IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_MERRY_WEATHER_CARGOPLANE2].veh)
								DELETE_VEHICLE(mission_veh[MV_MERRY_WEATHER_CARGOPLANE2].veh)
								IF DOES_ENTITY_EXIST(driver[12].ped)
									DELETE_PED(driver[12].ped)
								ENDIF
								SET_MODEL_AS_NO_LONGER_NEEDED(LAZER)
								SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BlackOps_01)
								SET_MODEL_AS_NO_LONGER_NEEDED(S_M_M_PILOT_02)
								REMOVE_VEHICLE_RECORDING(26,"DHF2")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF HAS_LABEL_BEEN_TRIGGERED("SEE THE SHIPS")
				IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh)
						OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh) > 100
							IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh)
								IF DOES_ENTITY_EXIST(driver[7].ped)
									SET_PED_AS_NO_LONGER_NEEDED(driver[7].ped)
								ENDIF
								SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh)
								//DELETE_VEHICLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_1].veh)
								SET_MODEL_AS_NO_LONGER_NEEDED(DINGHY)
								SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BlackOps_01)
								REMOVE_VEHICLE_RECORDING(ENUM_TO_INT(MV_MERRY_WEATHER_DINGHY_SETPIECE_1),"DHF2")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_2].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_2].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_2].veh)
						OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_2].veh) > 100
							IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_2].veh)
								IF DOES_ENTITY_EXIST(driver[8].ped)
									SET_PED_AS_NO_LONGER_NEEDED(driver[8].ped)
								ENDIF
								SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_2].veh)
								//DELETE_VEHICLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_2].veh)
								SET_MODEL_AS_NO_LONGER_NEEDED(DINGHY)
								SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BlackOps_01)
								REMOVE_VEHICLE_RECORDING(ENUM_TO_INT(MV_MERRY_WEATHER_DINGHY_SETPIECE_2),"DHF2")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_3].veh)
					IF IS_VEHICLE_DRIVEABLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_3].veh)
						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_3].veh)
						OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(),mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_3].veh) > 100
							IF NOT IS_ENTITY_ON_SCREEN(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_3].veh)
								IF DOES_ENTITY_EXIST(driver[9].ped)
									SET_PED_AS_NO_LONGER_NEEDED(driver[9].ped)
								ENDIF
								SET_VEHICLE_AS_NO_LONGER_NEEDED(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_3].veh)
								//DELETE_VEHICLE(mission_veh[MV_MERRY_WEATHER_DINGHY_SETPIECE_3].veh)
								SET_MODEL_AS_NO_LONGER_NEEDED(DINGHY)
								SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_BlackOps_01)
								REMOVE_VEHICLE_RECORDING(ENUM_TO_INT(MV_MERRY_WEATHER_DINGHY_SETPIECE_3),"DHF2")
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		SWITCH eMissionStage // main mission switch

			CASE MISSION_STAGE_SETUP //controls the loading of assets needed for the appropriate stage
				MISSION_SETUP()
			BREAK
			
			CASE MISSION_STAGE_OPENING_CUTSCENE //plays the opening cutscene
				OPENING_CUTSCENE()
			BREAK
			
			CASE MISSION_STAGE_GET_TO_AIRSTRIP 
				GET_TO_AIRSTRIP()
			BREAK
			
			CASE MISSION_STAGE_PICK_UP_THE_SUB_FROM_AIRSTRIP
				PICK_UP_THE_SUB_FROM_AIRSTRIP()
			BREAK
			
			CASE MISSION_STAGE_FLY_TO_SEA
				FLY_TO_SEA()
			BREAK
			
//			CASE MISSION_STAGE_CLEAR_THE_AREA_OF_MERRY_WEATHER
//				CLEAR_THE_AREA_OF_MERRY_WEATHER()
//			BREAK
			
			CASE MISSION_STAGE_DROP_THE_SUB
				DROP_THE_SUB()
			BREAK
			
			CASE MISSION_STAGE_FIND_THE_CRATE
				FIND_THE_CRATE()
			BREAK
			
			CASE MISSION_STAGE_GET_TO_SURFACE
				GET_TO_SURFACE()
			BREAK
			
			CASE MISSION_STAGE_PICK_UP_SUB
				PICK_UP_SUB()
			BREAK
			
			CASE MISSION_STAGE_DEFEND_AGAINST_MERRYWEATHER
				DEFEND_AGAINST_MERRYWEATHER()
			BREAK
			
			CASE MISSION_STAGE_FLY_AWAY
				FLY_AWAY()
			BREAK
			
			CASE MISSION_STAGE_DROP_THE_SUB_AT_THE_AIRSTRIP
				DROP_THE_SUB_AT_THE_AIRSTRIP()
			BREAK
			
			CASE MISSION_STAGE_CLOSING_CUTSCENE
				CLOSING_CUTSCENE()
			BREAK

			CASE MISSION_STAGE_PASSED //mission completed
				MISSION_PASSED()
			BREAK
			
			CASE MISSION_STAGE_FAIL  //mission failed
				MISSION_FAILED()
			BREAK

		ENDSWITCH
		
		//PD Moved all damage and speed watches from elsewhere in the script to here
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(PLAYER_PED_ID(), DH2B_DAMAGE)
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), DH2B_MAX_SPEED)
			ELSE
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL, DH2B_MAX_SPEED)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(mission_veh[MV_SUBMERISIBLE].veh)
		AND IS_VEHICLE_DRIVEABLE(mission_veh[MV_SUBMERISIBLE].veh)
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(mission_veh[MV_SUBMERISIBLE].veh, DH2B_SUB_DAMAGE)
		ENDIF
		
		IF DOES_ENTITY_EXIST(mission_veh[MV_CHINOOK].veh)
		AND IS_VEHICLE_DRIVEABLE(mission_veh[MV_CHINOOK].veh)
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(mission_veh[MV_CHINOOK].veh, DH2B_HELI_DAMAGE)
		ENDIF
		
		#IF IS_DEBUG_BUILD 
			IF eMissionStage > MISSION_STAGE_SETUP
				DONT_DO_J_SKIP(sLocatesData)
			ENDIF
		#ENDIF
		
		#IF IS_DEBUG_BUILD // debug controls 
		
			#IF IS_DEBUG_BUILD
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_5)
					OUTPUT_DEBUG_CAM_RELATIVE_TO_VEHICLE(mission_veh[0].veh)
				ENDIF
					
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_8)
					OUTPUT_DEBUG_CAM_RELATIVE_TO_VEHICLE(mission_veh[1].veh)
				ENDIF
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
					IF bFailChecks = TRUE
						PRINTSTRING("FAIL CHECKS OFF")
						bFailChecks = FALSE
					ELSE
						PRINTSTRING("FAIL CHECKS ON")
						bFailChecks = TRUE
					ENDIF
				ENDIF
				
				IF IS_KEYBOARD_KEY_PRESSED(KEY_NUMPAD5)
					NAME_THE_CRATES()
				ENDIF
			#ENDIF

			REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_ThePortOfLSHeist2B")
        	
			PRINT_PROGRESS()
			
			//makes the mission debug menu
			IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, ENUM_TO_INT(eMissionStage), FALSE, "",TRUE,TRUE)
				SET_GAMEPLAY_CAM_RELATIVE_PITCH(-90)
				bcleanup = FALSE
				iprogress = 99
				bskipping = TRUE
				brunfailchecks = FALSE
				IF iReturnStage < 1
					iReturnStage = 1
				ENDIF
				
				IF iReturnStage > ENUM_TO_INT(MISSION_STAGE_DROP_THE_SUB_AT_THE_AIRSTRIP) OR iReturnStage > ENUM_TO_INT(MISSION_STAGE_PASSED)
					CONTROL_FADE_OUT(500)
					eMissionStage = MISSION_STAGE_PASSED
				ELSE
					eMissionStage = INT_TO_ENUM(MISSION_STAGE_ENUM, iReturnStage)
				ENDIF
			ENDIF
			
            WHILE bskipping = TRUE  //Load new stage if skipping
				IF iReturnStage > ENUM_TO_INT(MISSION_STAGE_DROP_THE_SUB_AT_THE_AIRSTRIP ) OR iReturnStage > ENUM_TO_INT(MISSION_STAGE_PASSED)
					CONTROL_FADE_IN(1000)
					bskipping = FALSE
				ELSE
					WAIT(0)
					LOAD_MISSION_STAGE(eMissionStage)
				ENDIF
			ENDWHILE
           
            IF IS_KEYBOARD_KEY_PRESSED (KEY_S) // Checks if the keyboard key S has been pressed and if sets the current mission state to complete
				PRINT_GOD_TEXT("DS2_PASSH")
				eMissionStage = MISSION_STAGE_PASSED
            ENDIF
        
            IF IS_KEYBOARD_KEY_PRESSED (KEY_F)// Checks if the keyboard key F has been pressed and if so sets the current mission state to failed
				eMissionStage = MISSION_STAGE_FAIL
            ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
				SET_CAM_POINT_AT_CLOSEST_VEHICLE(TRUE)
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
				SET_CAM_POINT_AT_CLOSEST_VEHICLE(FALSE)
			ENDIF

			UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCam_MichaelToTrevor)
			HANDLE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
			
		#ENDIF
		
		#IF IS_DEBUG_BUILD
		BOMB_WIDGET()
		#ENDIF

    ENDWHILE
    
ENDSCRIPT
