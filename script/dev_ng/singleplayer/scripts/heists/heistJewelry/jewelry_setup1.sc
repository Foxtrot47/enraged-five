//╔═════════════════════════════════════════════════════════════════════════════╗
//║																				║
//║ 				Author:  Ben Barclay				Date: 20/05/2010		║
//║					Prev:	 													║
//║																				║
//╠═════════════════════════════════════════════════════════════════════════════╣
//║ 																			║
//║ 				Jewellery Heist - Setup 1									║
//║																				║
//║ 		The crew check out the jewellery store								║
//║ 																			║
//║ 		*	Michael and the contact drive to the jewellery store			║
//║ 		*	They scope out the outside- noting the air vents on the roof	║
//║ 		*	The go inside and talk with an employee							║
//║ 		*	She reveals where the best jewellery and security are			║
//║ 		*	Possibly take pictures of inside the store						║
//║ 		*	Possibly cut back to Franklin collecting the pictures			║
//║ 																			║
//║ 		Gameplay elements introduced: Talking, taking subterfuge pictures	║
//║ 		Vehicle Introduced:													║
//║ 		Location: Jewellery store											║
//║ 		Notes:  															║
//║ 																			║
//╚═════════════════════════════════════════════════════════════════════════════╝

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//═════════════════════════════════╡ COMMAND HEADERS ╞═══════════════════════════════════

using "rage_builtins.sch"
using "globals.sch"


using "script_heist.sch"

using "commands_misc.sch"
using "commands_pad.sch" 
using "commands_script.sch"
using "commands_player.sch"
using "commands_streaming.sch"
using "commands_vehicle.sch" 
using "commands_camera.sch"
using "commands_path.sch"
using "commands_fire.sch"
using "commands_graphics.sch"
using "commands_object.sch"
using "script_player.sch"
using "script_debug.sch"
using "streamed_scripts.sch"
using "selector_public.sch"
using "model_enums.sch"
using "script_misc.sch"
using "dialogue_public.sch"
USING "flow_public_core_override.sch"
using "script_blips.sch"
using "commands_cutscene.sch"
using "script_buttons.sch"
using "replay_public.sch"
using "player_ped_public.sch"
using "script_ped.sch"
using "cutscene_public.sch"
using "seamless_cuts.sch"
USING "CompletionPercentage_public.sch"
USING "taxi_functions.sch"
USING "flow_public_GAME.sch"
USING "clearMissionArea.sch"
USING "mission_stat_public.sch"
//USING "timelapse.sch"
USING "locates_public.sch"
USING "overlay_effects.sch"
USING "commands_recording.sch"
USING "commands_audio.sch"

/////══════════════════════════════════════╡ DEBUG VARIABLES  ╞════════════════════════════════════════
#IF IS_DEBUG_BUILD

USING "select_mission_stage.sch"

CONST_INT MAX_SKIP_MENU_LENGTH 5                      // number of stages in mission + 2 (for menu )
INT iReturnStage                                       // mission stage to jump to

//FLOAT debugX = 0 
//FLOAT debugY = 0 
//FLOAT debugZ = 0

BOOL jSkipReady										= FALSE 
BOOL PSkipReady										= FALSE

MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]      // struct containing the debug menu 

//WIDGET_ID widgetDebug

#ENDIF
/////═════════════════════════════════════╡ END OF DEBUG   ╞═══════════════════════════════════════════




//═════════════════════════════════════════════╡ ENUMS ╞══════════════════════════════════════════════

//═════════╡ MISSION CONTROL ╞═════════

ENUM MISSION_STAGE
	STAGE_INIT_MISSION, 									
	STAGE_OPENING_CUTSCENE,
	STAGE_DRIVE_TO_STORE,
	STAGE_SCOUT_OUT_STORE,
	STAGE_FIND_ROOF_ENTRANCE,
	STAGE_CHECK_OUT_ROOF,
	STAGE_DRIVE_TO_WAREHOUSE,
	STAGE_GO_TO_OFFICE,
	STAGE_END_MISSION
ENDENUM

MISSION_STAGE missionStage


//Cam stuff
CONST_FLOAT MAX_ZOOM 			20.0
CONST_FLOAT MIN_ZOOM			55.0	//increased to match first person FOV settings

FLOAT f_current_cam_fov
FLOAT f_scale_rotate_speed_zoom
FLOAT f_rotate_speed
//═════════════════════════════════════════════╡ CONSTANTS ╞══════════════════════════════════════════════


//═════════╡ COORDINATES AND HEADINGS ╞═════════

//Players car
VECTOR vCarSpawnCoords								= <<714.9441, -981.2628, 23.1180>>// << 718.1437, -983.6423, 23.1401 >> //<< 718.1439, -985.8972, 23.1081 >>
CONST_FLOAT fCarSpawnHeading						268.4990

////Contact start position
//VECTOR vContactSpawnCoords							= << 707.8595, -964.1854, 29.4334 >>
//CONST_FLOAT fContactSpawnHeading					80.0883

//Store destination
VECTOR vParkingSpace								= <<-659.0, -270.9, 35>>
//VECTOR vOffice										= << 706.0189, -964.4463, 29.4335 >>
VECTOR vWareHouse									= <<718, -981.7, 23.5>>


//═════════════════════════════════════════════╡ VARIABLES ╞══════════════════════════════════════════════

//═════════╡ WEAPON TYPES ╞═════════
WEAPON_TYPE Current_Ped_weapon

//═════════╡ PLAYERS GLASSES ╞═════════
//PED_COMP_NAME_ENUM ePlayersGlasses

//═════════╡ INTEGERS ╞═════════
INT iCutsceneStage = 0
INT iControlFlag = 0								
//INT IcamStage = 0
//INT Cam1Scene, Cam2Scene, Cam3Scene
INT iGlassesCamTimer
INT iPlayerTakingPhotoTimer
INT iShotsBeforeStoreTimer
INT iSaveHouseCheck
INT icount
INT iSnapDialogueTimer = 0
INT StartPH8ChatTimer = 0
INT iPhotosTaken = 0
INT iTrackedAlarmPoint[2]
INT iTrackedCamPoint[4]
INT iTrackedVentPoint[4]

//INT SyncdScene1
INT iMoveStageOnTimer
INT iWarnMichaelChatTimer
INT iTimerForRoofEntranceBlip
INT iMeleeTimer
INT iHurryUpChatTimer
INT iGlassesActivatedTimer
INT iAroundChatTimer
INT iCamZoomInSoundID, iCamZoomOutSoundID, iCamShootSoundID, iBackGroundSoundID
INT ShopGuardSyncdScene, ShopGuardSyncdSceneHold, ShopGuardSyncdSceneOpen
INT iPicTakenTimer
INT iAllPicsTakenTimer
INT AllpicsDoneTimer
INT iPlayerCantFindVents
INT iPicSpeedTimer
INT NotHighEnoughTimer
INT iPlayerNotApproachingTimer
INT iVentGodTextTimer
INT iGetBackInTimer
INT iBackInCarTimer
INT icopsTextTimer
INT iGoToFactoryTimer
INT iChat46Timer
INT LesterGetInCarSyncdScene
INT iChat72Timer
INT iHurryUpTimer
INT iHurryUpRoofTimer
INT iKilledPedsCloseToStore
INT iKilledPedsAwayFromStore
INT iGodText3Timer
INT iVideoTimer[5]

//═════════╡ PEDS ╞═════════
PED_INDEX pedContact
PED_INDEX shopGuard
PED_INDEX shopAssistant
PED_INDEX ShopCustomer[1]
//PED_INDEX WorkshopPed[3]
PED_INDEX ClosestPed

OBJECT_INDEX walkingStick
//OBJECT_INDEX IronProp
OBJECT_INDEX ObjBallCam[4]
OBJECT_INDEX ObjBallVent[4]
OBJECT_INDEX ObjBallAlarm[2]
//OBJECT_INDEX ChairProp[2]
//OBJECT_INDEX SewingProp[2]
OBJECT_INDEX Vent1PropBall[8]
OBJECT_INDEX Vent2PropBall[8]
OBJECT_INDEX Vent3PropBall[8]
OBJECT_INDEX SouthWallPropBall[6]
OBJECT_INDEX MainVentPropBall[4]

//═════════╡ ENTITYS ╞═════════

//═════════╡ INTERIOR ╞═════════
INTERIOR_INSTANCE_INDEX InteriorJewelleryShop
INTERIOR_INSTANCE_INDEX WarehouseInterior

//═════════╡ VEHICLES ╞═════════
VEHICLE_INDEX vehCar
VEHICLE_INDEX playersStartCar

//═════════╡ SEQUENCES ╞═════════
SEQUENCE_INDEX seqSequencePlayer
SEQUENCE_INDEX seqSequenceAssistant
SEQUENCE_INDEX seqSequencePedContact
SEQUENCE_INDEX shopCustomerSeq

//═════════╡ BLIPS ╞═════════
BLIP_INDEX blipLocate
BLIP_INDEX blipContact
BLIP_INDEX BlipVeh
BLIP_INDEX shopBlip
BLIP_INDEX ScopeOutViewPointBlip
BLIP_INDEX shopAssistantBlip
BLIP_INDEX BlipWareHouse
BLIP_INDEX BlipOffice
BLIP_INDEX BlipRoofCheck
BLIP_INDEX ventsBlip
BLIP_INDEX MainVentBlip

//═════════╡ SCENARIO'S ╞═════════
SCENARIO_BLOCKING_INDEX ScenarioBlocking[5]

//═════════╡ CAMERAS ╞═════════
CAMERA_INDEX interpCamera
//CAMERA_INDEX initCam
//CAMERA_INDEX destCam
CAMERA_INDEX GlassesCamera
//═════════╡ STRINGS ╞═════════
STRING FailReason
//STRING playersRadioStation

//═════════╡ SCALEFORM ╞═════════
SCALEFORM_INDEX SF_Glasses_Shutter_Index

//═════════╡ FLOATS ╞═════════
FLOAT fCamNewDirectionZCoord, fCamDirectionZCoord, fPlayerHeading, fCamNewDirectionXCoord, fGlassesCamFOV
FLOAT fminFOV, fMaxFOV
FLOAT fOpenRatio

//═════════╡ VECTORS ╞═════════
VECTOR vplayerCoords
VECTOR vplayerStartRoofCoords

//═════════╡ STRUCTS ╞═════════
structPedsForConversation MyLocalPedStruct

//═════════╡ TEXT LABELS ╞═════════	
TEXT_LABEL_23 JHS1_OLDCREW1
TEXT_LABEL_23 STP12c_StartLine
TEXT_LABEL_23 STP13_StartLine

//══════════╡ FLAGS ╞══════════
BOOL doneGetInCarText								= FALSE
BOOL DoneCarChat									= FALSE
BOOL doneGlassChat 									= FALSE
BOOL GoToJewellersTextDisplayed						= FALSE
BOOL GetInCarText									= FALSE
BOOL GotoCarTextDone								= FALSE
BOOL CheckpointReplayStarting						= FALSE
BOOL CanMissionFail									= FALSE
BOOL playerhasbeenincar								= FALSE
BOOL doneGetInCarText2								= FALSE
BOOL MissionStageBeingSkippedTo						= FALSE
BOOL bTracker
BOOL clearedGodText									= FALSE
BOOL doneWarningText								= FALSE
BOOL cutsceneSkipped
BOOL doneGodText
BOOL playerCausedTroubleInsideStore					= FALSE
BOOL hasStartAreaBeenCleanedUp
BOOL PlayerEnteredShopTooSoon
BOOL updatedGodText
BOOL SecurityCamObjectiveDone
BOOL VentObjectiveDone
BOOL SpokeToStaffObjectiveDone
BOOL GlassesCamActive
BOOL glassesHaveBeenActivated
BOOL glassesActivatedTimerStarted
BOOL AlarmObjectiveDone
BOOL camSetup
BOOL playerTakingPhoto
BOOL biscutsceneActive
BOOL AllBallsCreated
BOOL updatedGodText2
BOOL doneHintCam
BOOL donePH1Chat
BOOL donePH8Chat
BOOL donePH9Chat
BOOL donePH10Chat
BOOL donePH11Chat 
BOOL donePH12Chat
BOOL doneTOCARChat 
BOOL donePH13Chat
BOOL donePH16Chat
BOOL donePH17Chat 
BOOL donePH18Chat
BOOL donePH19Chat
BOOL SnapDialogueNeeded
BOOL StoreStaffCreated
BOOL bMissionFailed
BOOL doneGodText2
BOOL doneGodText3
BOOL donePH23Chat
BOOL donePH28Chat
BOOL donePH29Chat
BOOL donePH20Chat 
BOOL donePH21Chat 
BOOL donePH22Chat
BOOL donePH24Chat
BOOL donePH25Chat
BOOL donePH26Chat
BOOL PlayerIsInsideJewellers
BOOL GunHolstered
BOOL PH8ChatTimerStarted
BOOL doneGetBackInCarText
//BOOL SweatshopPedsRequired
//BOOL SweatshopPedsCreated
BOOL player_coming_from_left
BOOL player_coming_from_right
BOOL donePH41Chat
BOOL donePH44Chat
BOOL donePH42Chat
BOOL doneDESCChat
BOOL playerOnRoof
BOOL moveStageOnTimerSet
BOOL donePH43Chat
BOOL donePH45Chat
BOOL donePH46Chat
BOOL DoneGlassesHelpText
BOOL doneGotoCar2Text
BOOL MusicStarted[3]
BOOL timerstartedForRooftopEntranceBlip
BOOL donePH47Chat
BOOL meleeNeedsDisabled
BOOL modelsRequested
BOOL LesterLookTaskGiven
BOOL shopAssistantLookTaskGiven
BOOL shopGuardLookTaskGiven
BOOL hasPhoneBeenDisabled
BOOL JewelStoreModelsRequested
BOOL ShopCustomerTaskGiven[6] 
BOOL customerReadyToMoveOn
BOOL ShopCustomerFrozen
BOOL ShopCustomerReadyToLeave
BOOL doneSTP14Chat
BOOL playerTookTooLong
BOOL PlayerInsideWarehouse
BOOL AlarmIsOnScreen[2]
BOOL VentIsOnScreen[4]
BOOL CamIsOnScreen[4]
BOOL AnAlarmIsOnScreen
BOOL ACamIsOnScreen
BOOL AVentIsOnScreen
BOOL endsceneRequested
BOOL interiorPinnedInMemory
BOOL isCamZoomInSoundPlaying
BOOL isCamZoomOutSoundPlaying
BOOL PlayerApproching
BOOL GuardOpeningDoor
BOOL GuardOpeningDoorSequenceStarted
BOOL doneGoToStoreText
BOOL donePH3Chat
BOOL hasCutsceneBeenRequested
BOOL CutSceneGoodToStart
BOOL doneOLDCREWChat
BOOL doneHelp1Text
//BOOL doneHelp2Text
BOOL allpicsTaken
BOOL PausedOldCrewChat
BOOL doneVentChat
BOOL donePH51Chat
BOOL playerHasBeenAtPerfectPlace
BOOL PlayerCantFindVentsTimerSet
BOOL doneVentsBlipText
BOOL doneVentsGodText
BOOL updatedGodText3
BOOL PlayerTakingPicsTooFast
BOOL PicSpeedTimerStarted
BOOL AlarmHasBeenSnapped
BOOL CamHasBeenSnapped
BOOL VentHasBeenSnapped
BOOL AlarmHasBeenMentioned
BOOL CamHasBeenMentioned
BOOL VentHasBeenMentioned
BOOL donePH6Chat
BOOL donePH5Chat
BOOL LoadedInteriorForCut
BOOL pinnedInterior
BOOL doneChatPH53
BOOL OfficeFail
BOOL CounterFail
BOOL doneChatPH32
BOOL convoskilled
BOOL BallsForVentsCreated
BOOL playerIsHighEnough
BOOL DoChatPH57
BOOL GoodShotDialogueRequired
BOOL GoodButNotRightChatRequired
BOOL DoChatPH58
BOOL DoChatPH59
BOOL DoChatPH60
BOOL GoodShotDone
BOOL doneChatPH57
BOOL doneChatPH60
BOOL ballsDeleted
BOOL DoChatPH61
BOOL doneChatPH61
BOOL PhotoJustTaken
BOOL DoChatPH62
BOOL DoChatPH63
BOOL PerfectShotDialogueRequired
BOOL DoChatPH64
BOOL HigherChatNeeded
BOOL doneChatPH64
BOOL playerInPerfectPlace
BOOL DoChatPH65
BOOL doneFirstHelp
BOOL OkShotDone
BOOL hintCamCancelled
BOOL doneChatPH68
BOOL doneChatPH69
BOOL doneGodTextMainVent
BOOL doneReturnText
BOOL clearedGetBackin	
BOOL clearedText
BOOL DoneChat46
BOOL bLockstate
BOOL DoorAnimStopped
BOOL syncSceneStarted
BOOL donePH71Chat
BOOL doneLoseCopsText
//BOOL gotPlayersRadioStation
BOOL donchat72
BOOL doneLoseWantedText
//BOOL michaelInRightClothes
BOOl openingDialogueRequired
BOOL doneWantedChat
BOOL playerInSafeHouse
BOOL statPicturePerfectSet
BOOL ScreenFadedIn
BOOL PausedSTP12cChat
BOOL PausedSTP13Chat
BOOL trackedPointsCreated
BOOL bVideoOfPlayerEnteringStore
BOOL bVideoOfPlayerTakingLastPic
BOOL bVideoOfPlayerEnteringRoof
BOOL bVideoOfPlayerTakingPicOfVent
BOOL bVideoTimerStarted[5]

//═════════════════════════════════╡ CLEANUP, MISSION PASSED/FAILED PROCEDURES ╞═══════════════════════════════════


//PURPOSE: Removes all blips ideal for stage selector
PROC REMOVE_BLIPS()

	IF DOES_BLIP_EXIST(blipLocate)
		REMOVE_BLIP(blipLocate)
	ENDIF
	IF DOES_BLIP_EXIST(blipContact)
		REMOVE_BLIP(blipContact)
	ENDIF
	IF DOES_BLIP_EXIST(BlipVeh)
		REMOVE_BLIP(BlipVeh)
	ENDIF
	IF DOES_BLIP_EXIST(shopBlip)
		REMOVE_BLIP(shopBlip)
	ENDIF
	IF DOES_BLIP_EXIST(ScopeOutViewPointBlip)
		REMOVE_BLIP(ScopeOutViewPointBlip)
	ENDIF
	IF DOES_BLIP_EXIST(shopAssistantBlip)
		REMOVE_BLIP(shopAssistantBlip)
	ENDIF
	IF DOES_BLIP_EXIST(BlipWareHouse)
		REMOVE_BLIP(BlipWareHouse)
	ENDIF
	IF DOES_BLIP_EXIST(BlipOffice)
		REMOVE_BLIP(BlipOffice)
	ENDIF
	IF DOES_BLIP_EXIST(BlipRoofCheck)
		REMOVE_BLIP(BlipRoofCheck)
	ENDIF
	IF DOES_BLIP_EXIST(ventsBlip)
		REMOVE_BLIP(ventsBlip)
	ENDIF
	IF DOES_BLIP_EXIST(MainVentBlip)
		REMOVE_BLIP(MainVentBlip)
	ENDIF
	
ENDPROC

//PURPOSE:		Deletes or removes all elements of the script
PROC MissionTidyUp()

//═════════╡ PLAYER ╞═════════
	//Activate player control
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	ENDIF
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
	
//═════════╡ ROAD NODES ╞═════════	
	SET_ROADS_IN_AREA(<<-611.9, -362.4, 30>>, <<-558.6, -264.5, 45>>, TRUE)
	SET_ROADS_IN_AREA(<<-538.9, -296.7, 43.3>>, <<-631.3, -185.2, 30>>, TRUE)
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-645.8, -301.2, 30>>, <<-674.2, -244.6, 46.1>>, TRUE, TRUE)

//═════════╡ TRACKED POINTS ╞═════════	
	//Destroy all tracked points
	IF trackedPointsCreated = TRUE
		DESTROY_TRACKED_POINT(iTrackedAlarmPoint[0])
		DESTROY_TRACKED_POINT(iTrackedAlarmPoint[1])
		DESTROY_TRACKED_POINT(iTrackedVentPoint[1])
		DESTROY_TRACKED_POINT(iTrackedVentPoint[2])
		DESTROY_TRACKED_POINT(iTrackedVentPoint[3])
		DESTROY_TRACKED_POINT(iTrackedCamPoint[0])
		DESTROY_TRACKED_POINT(iTrackedCamPoint[1])
		DESTROY_TRACKED_POINT(iTrackedCamPoint[2])
		DESTROY_TRACKED_POINT(iTrackedCamPoint[3])	
		trackedPointsCreated = FALSE
	ENDIF
	
//═════════╡ TEXT ╞═════════
	CLEAR_HELP()

//═════════╡ TIMECYCLE ╞═════════
	CLEAR_TIMECYCLE_MODIFIER()

//═════════╡ SCENARIO's ╞═════════
	REMOVE_SCENARIO_BLOCKING_AREA(ScenarioBlocking[0]) 	
	REMOVE_SCENARIO_BLOCKING_AREA(ScenarioBlocking[1]) 
	REMOVE_SCENARIO_BLOCKING_AREA(ScenarioBlocking[2]) 
	REMOVE_SCENARIO_BLOCKING_AREA(ScenarioBlocking[3]) 
	REMOVE_SCENARIO_BLOCKING_AREA(ScenarioBlocking[4]) 
	//Switch off the scenario 
	IF DOES_SCENARIO_GROUP_EXIST("SEW_MACHINE")
		IF IS_SCENARIO_GROUP_ENABLED("SEW_MACHINE")
			SET_SCENARIO_GROUP_ENABLED("SEW_MACHINE", FALSE)
		ENDIF
	ENDIF
	
//═════════╡ PEDS ╞═════════
	CLEAR_PED_NON_CREATION_AREA()
	
//	IF DOES_ENTITY_EXIST(pedContact)
//		SET_PED_AS_NO_LONGER_NEEDED(pedContact)
//	ENDIF
//═════════╡ SEQUENCES ╞═════════
	CLEAR_SEQUENCE_TASK(seqSequencePedContact)
	CLEAR_SEQUENCE_TASK(seqSequenceAssistant)
	CLEAR_SEQUENCE_TASK(seqSequencePlayer)
	
//═════════╡ CAMS ╞═════════
	DESTROY_ALL_CAMS()
	DISPLAY_RADAR(TRUE)
	DISPLAY_HUD(TRUE)
	STOP_GAMEPLAY_CAM_SHAKING(TRUE)
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	
//═════════╡ BLIPS ╞═════════		
	REMOVE_BLIPS()

//═════════╡ SPEECH ╞═════════
	KILL_ANY_CONVERSATION()

//═════════╡ TAXI'S ╞═════════
	DISABLE_TAXI_HAILING(FALSE)

//═════════╡ SOUNDS ╞═════════
	//Set the ambient sound back to normal
//	SET_PED_WALLA_DENSITY(0, 0)
	STOP_SOUND(iCamZoomInSoundID)
	STOP_SOUND(iCamZoomOutSoundID)
	STOP_SOUND(iCamShootSoundID)
	STOP_SOUND(iBackGroundSoundID)
	OVERRIDE_MICROPHONE_SETTINGS(0, FALSE)

//═════════╡ PHONE ╞═════════
	DISABLE_CELLPHONE(FALSE)

//═════════╡ ROADS ╞═════════	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-650.4, -286.5, 34.1>>, <<-670.4, -251.7, 37.5>>, TRUE)

//═════════╡ SYNC'D SCENES ╞═════════	
IF IS_SYNCHRONIZED_SCENE_RUNNING(ShopGuardSyncdScene)
	PRINTNL() PRINTSTRING("ShopGuardSyncdScene is running, door anim being stopped now") PRINTNL()
	STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-631.96, -236.33, 38.21>>, 2, P_JEWEL_DOOR_L, NORMAL_BLEND_OUT)
ENDIF

//═════════╡ INTERIORS ╞═════════	
// Steve R - Disable interiors of Jewel Store and Max Renda store
	SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_STORE, BUILDINGSTATE_NORMAL, FALSE)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_HEIST_MAX_RENDA_INTERIOR, BUILDINGSTATE_NORMAL, FALSE)
	
ENDPROC


//		/// PURPOSE:
/////    Checks if Michael is wearing any suit, or items bought from high end shop
/////    This function will need to be updated as clothes items get finalised in the high end shop
///// RETURNS:
/////    TRUE if outfit is suitable for triggering mission in. FALSE otherwise
//FUNC BOOL IS_MICHAEL_WEARING_SUITABLE_OUTFIT()
//	
//	IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_MOVIE_TUXEDO)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_BLACK_SUIT)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_DARK_GRAY_SUIT)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SHIRT_AND_PANTS)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_JEWEL_HEIST)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_NAVY_SUIT)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SUIT_5)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SUIT_6)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SUIT_7)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SUIT_8)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SUIT_9)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SUIT_10)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SUIT_11)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SUIT_12)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SUIT_13)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SUIT_14)
//	OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_SUIT_15)
//		// the player is wearing an outfit bought from the high end shop
//		RETURN TRUE
//	ENDIF
//	
//	IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_01)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_02)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_03)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_04)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_05)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_06)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_07)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_08)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_09)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_10)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_11)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_12)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_13)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_14)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_GREY_SUIT_15)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_SUIT_JACKET)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_SUIT_JACKET_1)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_SUIT_JACKET_2)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_SUIT_JACKET_3)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_SUIT_JACKET_4)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_SUIT_JACKET_5)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_SUIT_JACKET_6)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_SUIT_JACKET_7)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_VNECK)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_DRESS_SHIRT)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_DRESS_SHIRT_1)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_DRESS_SHIRT_2)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_DRESS_SHIRT_3)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_DRESS_SHIRT_4)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_DRESS_SHIRT_5)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_DRESS_SHIRT_6)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_DRESS_SHIRT_7)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_TORSO, TORSO_P0_MOVIE_TUXEDO)
//		// the player is wearing a torso that is not from the high end shop
//		RETURN FALSE
//	ENDIF
//	
//	IF NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_1)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_2)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_3)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_4)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_5)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_6)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_7)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_8)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_9)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_10)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_11)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_12)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_13)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_14)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_GREY_SUIT_15)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_SECURITY)
//	AND NOT IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_LEGS, LEGS_P0_BLACK_TROUSERS)
//		// the player is wearing a legs component that is not from the high end shop
//		RETURN FALSE
//	ENDIF
//	
//	// the player is wearing torso and legs from the high end shop
//	RETURN TRUE
//ENDFUNC

//PURPOSE: 		Cleans up the mission after passing or failing.
PROC MissionCleanup()
	MissionTidyUp()
	TERMINATE_THIS_THREAD()	
ENDPROC

//PURPOSE:		Performs necessary actions for player completing the mission, then calls cleanup
PROC MissionPassed()
	
	PRINTSTRING("MissionPassed") PRINTNL()
	
	//Set board intro cutscene to load and save reference to Lester and car 
	//so the heist controller can grab ownership of them and clean them up.
	IF NOT IS_REPEAT_PLAY_ACTIVE()
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, TRUE)
	ENDIF
	g_sTriggerSceneAssets.ped[0] = pedContact
	g_sTriggerSceneAssets.veh[0] = vehCar
	
	//Cutting directly into planning board view. 
	//Make sure radar is turned off.
	DISPLAY_RADAR(FALSE)

	//Increment global heist flow counter
	IF NOT IS_REPEAT_PLAY_ACTIVE()
		Mission_Flow_Mission_Passed(TRUE)
	ELSE
		IF NOT IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)
		ENDIF
		Mission_Flow_Mission_Passed()
	ENDIF
	MissionCleanup()
	
ENDPROC

//PURPOSE:		Performs necessary actions for player failing the mission, then calls cleanup
PROC MissionFailed()

	SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, FALSE)

	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here	
	
	KILL_ANY_CONVERSATION()
	
	MissionCleanup() // must only take 1 frame and terminate the thread
ENDPROC



//═════════════════════════════════╡ INITIALISING ╞═══════════════════════════════════

//═════════════════════════════════╡ DEBUG ╞═══════════════════════════════════
//DEBUG STAGE SELECTOR NAMING
#IF IS_DEBUG_BUILD
PROC SET_DEBUG_STAGE_NAMES()

	SkipMenuStruct[0].sTxtLabel = "STAGE_START_MISSION"  		// Stage 0 Name   
	SkipMenuStruct[1].sTxtLabel = "STAGE_SCOUT_OUT_STORE"  		// Stage 1 Name 
	SkipMenuStruct[2].sTxtLabel = "STAGE_FIND_ROOF_ENTRANCE"  	// Stage 2 Name
	SkipMenuStruct[3].sTxtLabel = "STAGE_CHECK_OUT_ROOF"  		// Stage 3 Name 
	SkipMenuStruct[4].sTxtLabel = "STAGE_DRIVE_TO_WAREHOUSE"  	// Stage 4 Name                
	
ENDPROC
#ENDIF

//PURPOSE: Deletes any peds if they were injured 
PROC DELETE_ALL_ENTITIES()
	
	//Destroy all tracked points
	IF trackedPointsCreated = TRUE
		DESTROY_TRACKED_POINT(iTrackedAlarmPoint[0])
		DESTROY_TRACKED_POINT(iTrackedAlarmPoint[1])
		DESTROY_TRACKED_POINT(iTrackedVentPoint[1])
		DESTROY_TRACKED_POINT(iTrackedVentPoint[2])
		DESTROY_TRACKED_POINT(iTrackedVentPoint[3])
		DESTROY_TRACKED_POINT(iTrackedCamPoint[0])
		DESTROY_TRACKED_POINT(iTrackedCamPoint[1])
		DESTROY_TRACKED_POINT(iTrackedCamPoint[2])
		DESTROY_TRACKED_POINT(iTrackedCamPoint[3])
		trackedPointsCreated = FALSE
	ENDIF
	
	//Delete Peds first
	IF DOES_ENTITY_EXIST(pedContact)
		DELETE_PED(pedContact)
	ENDIF
	IF DOES_ENTITY_EXIST(shopGuard)
		DELETE_PED(shopGuard)
	ENDIF
	IF DOES_ENTITY_EXIST(shopAssistant)
		DELETE_PED(shopAssistant)
	ENDIF
	IF DOES_ENTITY_EXIST(ShopCustomer[0])
		DELETE_PED(ShopCustomer[0])
	ENDIF
//	IF DOES_ENTITY_EXIST(WorkshopPed[0])
//		DELETE_PED(WorkshopPed[0])
//	ENDIF
//	IF DOES_ENTITY_EXIST(WorkshopPed[1])
//		DELETE_PED(WorkshopPed[1])
//	ENDIF
//	IF DOES_ENTITY_EXIST(WorkshopPed[2])
//		DELETE_PED(WorkshopPed[2])
//	ENDIF
//	//Delete Guadaloupe now for good.
//	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[4])
//		SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[4], TRUE, TRUE)
//		GuadaloupePed = g_sTriggerSceneAssets.ped[4]
//		CLEAR_PED_TASKS(GuadaloupePed)
//		DELETE_PED(GuadaloupePed)
//	ENDIF

	//Delete all vehicles
	IF DOES_ENTITY_EXIST(vehCar)
		DELETE_VEHICLE(vehCar)
	ENDIF

	//Delete all objects
	IF DOES_ENTITY_EXIST(walkingStick)
		DELETE_OBJECT(walkingStick)
	ENDIF
	FOR icount = 0 TO 3
		IF DOES_ENTITY_EXIST(ObjBallCam[icount])
			DELETE_OBJECT(ObjBallCam[icount])
		ENDIF
	ENDFOR
	FOR icount = 0 TO 3
		IF DOES_ENTITY_EXIST(ObjBallVent[icount])
			DELETE_OBJECT(ObjBallVent[icount])
		ENDIF
	ENDFOR
	IF DOES_ENTITY_EXIST(ObjBallAlarm[0])
		DELETE_OBJECT(ObjBallAlarm[0])
	ENDIF	
	IF DOES_ENTITY_EXIST(ObjBallAlarm[1])
		DELETE_OBJECT(ObjBallAlarm[1])
	ENDIF	
//	IF DOES_ENTITY_EXIST(IronProp)
//		DELETE_OBJECT(IronProp)
//	ENDIF			
//	IF DOES_ENTITY_EXIST(ChairProp[0])
//		DELETE_OBJECT(ChairProp[0])
//	ENDIF	
//	IF DOES_ENTITY_EXIST(ChairProp[1])
//		DELETE_OBJECT(ChairProp[1])
//	ENDIF	
//	IF DOES_ENTITY_EXIST(SewingProp[0])
//		DELETE_OBJECT(SewingProp[0])
//	ENDIF	
//	IF DOES_ENTITY_EXIST(SewingProp[1])
//		DELETE_OBJECT(SewingProp[1])
//	ENDIF
			
	//Anim dicts		
	REMOVE_ANIM_DICT("MISSHEIST_JEWEL_SETUP")
	REMOVE_ANIM_DICT("amb@world_human_window_shop@male@idle_a")
	
	//Remove any models that may have been requested
	SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_BEVHILLS_01)
					
	//Clean up the glasses camera scaleform stuff
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(SF_Glasses_Shutter_Index)
	SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("digitalOverlay")
	
	//Remove waypoint recordings		
	REMOVE_WAYPOINT_RECORDING("BB_JEW_6")			
	
	//Put rode nodes back on 
	SET_ROADS_IN_AREA(<<-611.9, -362.4, 30>>, <<-558.6, -264.5, 45>>, TRUE)
	SET_ROADS_IN_AREA(<<-538.9, -296.7, 43.3>>, <<-631.3, -185.2, 30>>, TRUE)
	
	//Audio
	CANCEL_MUSIC_EVENT("JH1_RESTART_1")
	CANCEL_MUSIC_EVENT("JH1_RESTART_2")
	CANCEL_MUSIC_EVENT("JH1_RESTART_3")
	CANCEL_MUSIC_EVENT("JH1_START")
	CANCEL_MUSIC_EVENT("JH1_STORE")
	CANCEL_MUSIC_EVENT("JH1_STOP_TRACK_ACTION")
//	CANCEL_MUSIC_EVENT("JH1_END")
	
ENDPROC

PROC CREATE_ENTITIES_REQUIRED()

//	//Creat the car the player started in if on replay
//	IF IS_REPLAY_IN_PROGRESS()
//		IF IS_REPLAY_START_VEHICLE_AVAILABLE()
//			IF NOT DOES_ENTITY_EXIST(playersStartCar)
//				IF GET_REPLAY_START_VEHICLE_MODEL() != TAILGATER
//					IF IS_THIS_MODEL_A_HELI(GET_REPLAY_START_VEHICLE_MODEL())
//						CREATE_REPLAY_START_VEHICLE(<<686.8995, -985.1403, 22.3440>>, 268.5802)	
//					ELSE
//						IF IS_THIS_MODEL_A_CAR(GET_REPLAY_START_VEHICLE_MODEL())
//						OR IS_THIS_MODEL_A_BIKE(GET_REPLAY_START_VEHICLE_MODEL())
//						OR IS_THIS_MODEL_A_QUADBIKE(GET_REPLAY_START_VEHICLE_MODEL())
//							CREATE_REPLAY_START_VEHICLE(<<706.0064, -979.6544, 23.1403>>, 227.0058)	
//						ELSE
//							DELETE_VEHICLE(playersStartCar)
//						ENDIF
//					ENDIF
//					SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<0.0,0.0,0.0>>, 0.0)
//				ELSE
//					DELETE_VEHICLE(playersStartCar)
//				ENDIF
//			ENDIF
//		ENDIF
//	ENDIF

	SWITCH missionStage
	
		CASE STAGE_DRIVE_TO_STORE
			
//			WHILE NOT NEW_LOAD_SCENE_START_SPHERE(<<718, -974, 24>>, 10)
//				PRINTSTRING("Waiting for new load scene to start") PRINTNL()
//				WAIT(0)
//			ENDWHILE
			
			REQUEST_PLAYER_VEH_MODEL(CHAR_MICHAEL)
			REQUEST_NPC_PED_MODEL(CHAR_LESTER)
			
			WHILE NOT HAS_PLAYER_VEH_MODEL_LOADED(CHAR_MICHAEL)
			OR NOT HAS_NPC_PED_MODEL_LOADED(CHAR_LESTER)
				PRINTSTRING("Waiting for models to load") PRINTNL()
				WAIT(0)
			ENDWHILE
			
			IF NOT DOES_ENTITY_EXIST(vehCar)
				CLEAR_AREA(vCarSpawnCoords, 15, TRUE)
				CREATE_PLAYER_VEHICLE(vehCar, CHAR_MICHAEL, vCarSpawnCoords, fCarSpawnHeading)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER, TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehCar)
				ADD_VEHICLE_UPSIDEDOWN_CHECK(vehCar)
				SET_VEHICLE_CAN_BREAK(vehCar, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(vehCar, TRUE)
				//Stat for how much damage the players car takes.
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehCar)
				//Stat for what speed the player gets their car up to.
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehCar)
			ENDIF
		
			IF NOT DOES_ENTITY_EXIST(pedContact)
				CREATE_NPC_PED_INSIDE_VEHICLE(pedContact, CHAR_LESTER, vehCar, VS_FRONT_RIGHT, TRUE)
				SET_PED_COMPONENT_VARIATION(pedContact, PED_COMP_TORSO, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedContact, PED_COMP_LEG, 1, 0)
				SET_PED_PROP_INDEX(pedContact, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
				SET_PED_CAN_BE_DRAGGED_OUT(pedContact, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedContact, TRUE)
				SET_PED_CAN_BE_TARGETTED(pedContact, FALSE)
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, pedContact, "LESTER")
				SET_PED_RELATIONSHIP_GROUP_HASH(pedContact, RELGROUPHASH_PLAYER)
				SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedContact, FALSE)
				SET_PED_CONFIG_FLAG(pedContact, PCF_LawWillOnlyAttackIfPlayerIsWanted, TRUE)
			ENDIF
			
			SET_PLAYER_VEH_MODEL_AS_NO_LONGER_NEEDED(CHAR_MICHAEL)
			
		BREAK
		
		CASE STAGE_SCOUT_OUT_STORE
			
			REQUEST_PLAYER_VEH_MODEL(CHAR_MICHAEL)
			REQUEST_NPC_PED_MODEL(CHAR_LESTER)
			
			WHILE NOT HAS_PLAYER_VEH_MODEL_LOADED(CHAR_MICHAEL)
			OR NOT HAS_NPC_PED_MODEL_LOADED(CHAR_LESTER)
				PRINTSTRING("Waiting for models to load") PRINTNL()
				WAIT(0)
			ENDWHILE
			
			IF NOT DOES_ENTITY_EXIST(vehCar)
				CLEAR_AREA(vCarSpawnCoords, 15, TRUE)
				CREATE_PLAYER_VEHICLE(vehCar, CHAR_MICHAEL, <<-659.1774, -270.6738, 34.8078>>, 30.7532)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER, TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehCar)
				ADD_VEHICLE_UPSIDEDOWN_CHECK(vehCar)
				SET_VEHICLE_CAN_BREAK(vehCar, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(vehCar, TRUE)
				SET_VEHICLE_ENGINE_ON(vehCar, TRUE, TRUE)
				//Stat for how much damage the players car takes.
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehCar)
				//Stat for what speed the player gets their car up to.
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehCar)
			ENDIF
		
			IF NOT DOES_ENTITY_EXIST(pedContact)
				CREATE_NPC_PED_INSIDE_VEHICLE(pedContact, CHAR_LESTER, vehCar, VS_FRONT_RIGHT, TRUE)
				SET_PED_COMPONENT_VARIATION(pedContact, PED_COMP_TORSO, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedContact, PED_COMP_LEG, 1, 0)
				SET_PED_PROP_INDEX(pedContact, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
				SET_PED_CAN_BE_DRAGGED_OUT(pedContact, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedContact, TRUE)
				SET_PED_CAN_BE_TARGETTED(pedContact, FALSE)
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, pedContact, "LESTER")
				SET_PED_RELATIONSHIP_GROUP_HASH(pedContact, RELGROUPHASH_PLAYER)
				SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedContact, FALSE)
				SET_PED_CONFIG_FLAG(pedContact, PCF_LawWillOnlyAttackIfPlayerIsWanted, TRUE)
			ENDIF
			
			SET_PLAYER_VEH_MODEL_AS_NO_LONGER_NEEDED(CHAR_MICHAEL)			
			
		BREAK
		
		CASE STAGE_FIND_ROOF_ENTRANCE
			
			REQUEST_PLAYER_VEH_MODEL(CHAR_MICHAEL)
			REQUEST_NPC_PED_MODEL(CHAR_LESTER)
			SF_Glasses_Shutter_Index = REQUEST_SCALEFORM_MOVIE ("camera_gallery")
			LOAD_OVERLAY_EFFECTS()			
			
			
			WHILE NOT HAS_PLAYER_VEH_MODEL_LOADED(CHAR_MICHAEL)
			OR NOT HAS_NPC_PED_MODEL_LOADED(CHAR_LESTER)
				PRINTSTRING("Waiting for models to load") PRINTNL()
				WAIT(0)
			ENDWHILE
			
			IF NOT DOES_ENTITY_EXIST(vehCar)
				CLEAR_AREA(vCarSpawnCoords, 15, TRUE)
				CREATE_PLAYER_VEHICLE(vehCar, CHAR_MICHAEL, <<-659.1774, -270.6738, 34.8078>>, 30.7532)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER, TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehCar)
				ADD_VEHICLE_UPSIDEDOWN_CHECK(vehCar)
				SET_VEHICLE_CAN_BREAK(vehCar, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(vehCar, TRUE)
				//Stat for how much damage the players car takes.
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehCar)
				//Stat for what speed the player gets their car up to.
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehCar)
			ENDIF
		
			IF NOT DOES_ENTITY_EXIST(pedContact)
				CREATE_NPC_PED_INSIDE_VEHICLE(pedContact, CHAR_LESTER, vehCar, VS_FRONT_RIGHT, TRUE)
				SET_PED_COMPONENT_VARIATION(pedContact, PED_COMP_TORSO, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedContact, PED_COMP_LEG, 1, 0)
				SET_PED_PROP_INDEX(pedContact, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
				SET_PED_CAN_BE_DRAGGED_OUT(pedContact, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedContact, TRUE)
				SET_PED_CAN_BE_TARGETTED(pedContact, FALSE)
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, pedContact, "LESTER")
				SET_PED_RELATIONSHIP_GROUP_HASH(pedContact, RELGROUPHASH_PLAYER)
				SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedContact, FALSE)
				SET_PED_CONFIG_FLAG(pedContact, PCF_LawWillOnlyAttackIfPlayerIsWanted, TRUE)
			ENDIF
			
			SET_PLAYER_VEH_MODEL_AS_NO_LONGER_NEEDED(CHAR_MICHAEL)			
			
		BREAK		
		
		CASE STAGE_CHECK_OUT_ROOF
		
			SF_Glasses_Shutter_Index = REQUEST_SCALEFORM_MOVIE ("camera_gallery")
			LOAD_OVERLAY_EFFECTS()		
			
			REQUEST_MISSION_AUDIO_BANK("JWL_HEIST_SETUP")
			REQUEST_PLAYER_VEH_MODEL(CHAR_MICHAEL)
			REQUEST_NPC_PED_MODEL(CHAR_LESTER)
			
			WHILE NOT HAS_PLAYER_VEH_MODEL_LOADED(CHAR_MICHAEL)
			OR NOT HAS_NPC_PED_MODEL_LOADED(CHAR_LESTER)
				PRINTSTRING("Waiting for models to load") PRINTNL()
				WAIT(0)
			ENDWHILE
			
			IF NOT DOES_ENTITY_EXIST(vehCar)
				CLEAR_AREA(vCarSpawnCoords, 15, TRUE)
				CREATE_PLAYER_VEHICLE(vehCar, CHAR_MICHAEL, <<-575.9287, -277.7308, 34.1598>>, 27.8937)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER, TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehCar)
				ADD_VEHICLE_UPSIDEDOWN_CHECK(vehCar)
				SET_VEHICLE_CAN_BREAK(vehCar, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(vehCar, TRUE)
				//Stat for how much damage the players car takes.
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehCar)
				//Stat for what speed the player gets their car up to.
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehCar)
			ENDIF
		
			IF NOT DOES_ENTITY_EXIST(pedContact)
				CREATE_NPC_PED_INSIDE_VEHICLE(pedContact, CHAR_LESTER, vehCar, VS_FRONT_RIGHT, TRUE)
				SET_PED_COMPONENT_VARIATION(pedContact, PED_COMP_TORSO, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedContact, PED_COMP_LEG, 1, 0)
				SET_PED_PROP_INDEX(pedContact, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
				SET_PED_CAN_BE_DRAGGED_OUT(pedContact, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedContact, TRUE)
				SET_PED_CAN_BE_TARGETTED(pedContact, FALSE)
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, pedContact, "LESTER")
				SET_PED_RELATIONSHIP_GROUP_HASH(pedContact, RELGROUPHASH_PLAYER)
				SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedContact, FALSE)
				SET_PED_CONFIG_FLAG(pedContact, PCF_LawWillOnlyAttackIfPlayerIsWanted, TRUE)
			ENDIF
			
			SET_PLAYER_VEH_MODEL_AS_NO_LONGER_NEEDED(CHAR_MICHAEL)		
		
		BREAK
		
		CASE STAGE_DRIVE_TO_WAREHOUSE
		
			REQUEST_PLAYER_VEH_MODEL(CHAR_MICHAEL)
			REQUEST_NPC_PED_MODEL(CHAR_LESTER)
//			PREPARE_MUSIC_EVENT("JH1_END")
			
			WHILE NOT HAS_PLAYER_VEH_MODEL_LOADED(CHAR_MICHAEL)
			OR NOT HAS_NPC_PED_MODEL_LOADED(CHAR_LESTER)
				PRINTSTRING("Waiting for models to load") PRINTNL()
				WAIT(0)
			ENDWHILE
			
			IF NOT DOES_ENTITY_EXIST(vehCar)
				CLEAR_AREA(vCarSpawnCoords, 15, TRUE)
				CREATE_PLAYER_VEHICLE(vehCar, CHAR_MICHAEL, <<-575.9287, -277.7308, 34.1598>>, 27.8937)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER, TRUE)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehCar)
				ADD_VEHICLE_UPSIDEDOWN_CHECK(vehCar)
				SET_VEHICLE_CAN_BREAK(vehCar, FALSE)
				SET_VEHICLE_HAS_STRONG_AXLES(vehCar, TRUE)
				//Stat for how much damage the players car takes.
				INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehCar)
				//Stat for what speed the player gets their car up to.
				INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehCar)
			ENDIF
		
			IF NOT DOES_ENTITY_EXIST(pedContact)
				CREATE_NPC_PED_INSIDE_VEHICLE(pedContact, CHAR_LESTER, vehCar, VS_FRONT_RIGHT, TRUE)
				SET_PED_COMPONENT_VARIATION(pedContact, PED_COMP_TORSO, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedContact, PED_COMP_LEG, 1, 0)
				SET_PED_PROP_INDEX(pedContact, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
				SET_PED_CAN_BE_DRAGGED_OUT(pedContact, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedContact, TRUE)
				SET_PED_CAN_BE_TARGETTED(pedContact, FALSE)
				ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, pedContact, "LESTER")
				SET_PED_RELATIONSHIP_GROUP_HASH(pedContact, RELGROUPHASH_PLAYER)
				SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedContact, FALSE)
				SET_PED_CONFIG_FLAG(pedContact, PCF_LawWillOnlyAttackIfPlayerIsWanted, TRUE)
			ENDIF
			
			SET_PLAYER_VEH_MODEL_AS_NO_LONGER_NEEDED(CHAR_MICHAEL)		
		
		BREAK		
		
	ENDSWITCH

ENDPROC

PROC SET_PLAYER_COORDS()

	SWITCH missionStage
	
		CASE STAGE_DRIVE_TO_STORE
			
//			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)	
			
			IF DOES_ENTITY_EXIST(vehCar)
				IF IS_VEHICLE_DRIVEABLE(vehCar)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					ENDIF
				ENDIF
			ENDIF	
		
		BREAK
		
		CASE STAGE_SCOUT_OUT_STORE
			
			IF DOES_ENTITY_EXIST(vehCar)
				IF IS_VEHICLE_DRIVEABLE(vehCar)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					ENDIF
				ENDIF
			ENDIF	
		
		BREAK
		
		CASE STAGE_FIND_ROOF_ENTRANCE
			
			IF DOES_ENTITY_EXIST(vehCar)
				IF IS_VEHICLE_DRIVEABLE(vehCar)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					ENDIF
				ENDIF
			ENDIF	
		
		BREAK		
		
		CASE STAGE_CHECK_OUT_ROOF
			
			IF DOES_ENTITY_EXIST(vehCar)
				IF IS_VEHICLE_DRIVEABLE(vehCar)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					ENDIF
				ENDIF
			ENDIF			
		
		BREAK			
		
		CASE STAGE_DRIVE_TO_WAREHOUSE
			
//			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_DEFAULT, FALSE)
		
			IF DOES_ENTITY_EXIST(vehCar)
				IF IS_VEHICLE_DRIVEABLE(vehCar)
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
					ENDIF
				ENDIF
			ENDIF			
		
		BREAK		
		
	ENDSWITCH

ENDPROC

PROC ANYTHING_STAGE_SPECIFIC_FOR_SKIPPING()

	SWITCH missionStage
	
		CASE STAGE_DRIVE_TO_STORE
			
			TRIGGER_MUSIC_EVENT("JH1_RESTART_1")
			StoreStaffCreated = FALSE
			modelsRequested = FALSE
//			SweatshopPedsCreated = FALSE
			PlayerInsideWarehouse = FALSE
		
		BREAK
		
		CASE STAGE_SCOUT_OUT_STORE
			
			TRIGGER_MUSIC_EVENT("JH1_RESTART_2")
			StoreStaffCreated = FALSE
			modelsRequested = FALSE
//			SweatshopPedsCreated = FALSE
			PlayerInsideWarehouse = FALSE
			interiorPinnedInMemory = FALSE
			
		BREAK
		
		CASE STAGE_FIND_ROOF_ENTRANCE
			
			TRIGGER_MUSIC_EVENT("JH1_RESTART_2")
			StoreStaffCreated = FALSE
			modelsRequested = FALSE
//			SweatshopPedsCreated = FALSE
			PlayerInsideWarehouse = FALSE
			interiorPinnedInMemory = FALSE
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P0_PROGRAMMER_GLASSES, FALSE)
			PRINTSTRING("JEWEL = Glasses being given to player now")
			
		BREAK		
		
		CASE STAGE_CHECK_OUT_ROOF
			
			TRIGGER_MUSIC_EVENT("JH1_RESTART_3")
			ScenarioBlocking[2] = ADD_SCENARIO_BLOCKING_AREA(<<-603.5, -271.1, 44.2>>, <<-566.5, -298.7, 31.4>>)
			StoreStaffCreated = FALSE
			modelsRequested = FALSE
//			SweatshopPedsCreated = FALSE
			PlayerInsideWarehouse = FALSE
			interiorPinnedInMemory = FALSE
			SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P0_PROGRAMMER_GLASSES, FALSE)
			
		BREAK		
		
		CASE STAGE_DRIVE_TO_WAREHOUSE
			
//			TRIGGER_MUSIC_EVENT("JH1_END")
			StoreStaffCreated = FALSE
			modelsRequested = FALSE
			PlayerInsideWarehouse = FALSE
			interiorPinnedInMemory = FALSE
		
		BREAK
		
	ENDSWITCH

ENDPROC

//PURPOSE: anything that doesn't need to be called when using new replay system
PROC ANYTHING_NOT_TO_BE_CALLED_WHEN_NEW_REPLAY_SYSTEM_IS_ACTIVE()

	IF IS_CUTSCENE_ACTIVE()
        STOP_CUTSCENE()
    ENDIF
	
	REMOVE_CUTSCENE()
	
	RELEASE_MISSION_AUDIO_BANK()

	DISABLE_CELLPHONE(FALSE)
	
	//Clear any conversation or text
	KILL_ANY_CONVERSATION()
	CLEAR_PRINTS()		
	
	//Move the player out of any vehicle before deleting the vehicle
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), GET_ENTITY_COORDS(PLAYER_PED_ID()))
	ENDIF	
	
ENDPROC

PROC STAGE_SELECTOR_MISSION_SETUP()
	
	//Stop and remove any cutscene if one is playing
	IF NOT IS_REPLAY_BEING_SET_UP()
		ANYTHING_NOT_TO_BE_CALLED_WHEN_NEW_REPLAY_SYSTEM_IS_ACTIVE()
	ENDIF
			
	REMOVE_BLIPS()
	
	DELETE_ALL_ENTITIES()			

	OVERRIDE_MICROPHONE_SETTINGS(0, FALSE)	
	
	CREATE_ENTITIES_REQUIRED()
	
	IF NOT IS_REPLAY_BEING_SET_UP()
		SET_PLAYER_COORDS()	
	ENDIF
	
	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
	
	ANYTHING_STAGE_SPECIFIC_FOR_SKIPPING()
	
	//Reset any flags global for mission
	LesterLookTaskGiven 			= FALSE
	shopAssistantLookTaskGiven 		= FALSE
	shopGuardLookTaskGiven 			= FALSE
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	
ENDPROC


//TIMEOFDAY currentTimeOfDay
//CAMERA_INDEX splineCamera
//INT iTimelapseCut
//
//PROC DO_STAGE_TIME_LAPSE()
//		
//	IF IS_TIME_BETWEEN_THESE_HOURS(TODS_SUNSET, TODS_SUNRISE)
//		TimelapseRequired = TRUE
//	ENDIF	
//		
//    IF DO_TIMELAPSE(SP_HEIST_JEWELRY_1, currentTimeOfDay, iTimelapseCut, splineCamera) 
//		missionstage = STAGE_INIT_MISSION        
//    ENDIF
//
//ENDPROC

//Sets up the end of the mission for skipping to end
PROC SET_UP_MISSION_PASSED()
	
	IF iControlFlag = 0
		
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, TRUE)
		
//		WHILE NOT HAS_CUTSCENE_LOADED()
//			PRINTSTRING("waiting for end cutscene to load") PRINTNL()
//			WAIT(0)
//		ENDWHILE
					
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			CLEAR_AREA(<< 718.0418, -984.2304, 23.1442 >>, 20, TRUE)
						
			IF NOT DOES_ENTITY_EXIST(vehCar)
				REQUEST_MODEL(TAILGATER)
				WHILE NOT HAS_MODEL_LOADED(TAILGATER)
					PRINTSTRING("waiting for model tailgater loading") PRINTNL()
					WAIT(0)
				ENDWHILE
				IF CREATE_PLAYER_VEHICLE(vehCar, CHAR_MICHAEL, << 718.0418, -984.2304, 23.1442 >>, 271.3340)
//					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
//						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
//					ENDIF
					PRINTSTRING("CREATE_PLAYER_VEHICLE has returned TRUE") PRINTNL()
					iControlFlag ++
				ENDIF
			ELSE
				IF IS_VEHICLE_DRIVEABLE(vehCar)
//					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
//						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
//					ENDIF
//					SET_ENTITY_COORDS(vehCar, << 718.0418, -984.2304, 23.1442 >>)
//					SET_ENTITY_HEADING(vehCar, 271.3340)
					iControlFlag ++
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
	
	IF iControlFlag = 1
		
		PRINTSTRING("iControlFlag = 1") PRINTNL()
		
//		IF NOT IS_SCREEN_FADED_IN()
			END_REPLAY_SETUP(vehCar)
//			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//		ENDIF
		
		MissionPassed()
	
	ENDIF
	
ENDPROC

//PURPOSE: Controls all the video recordings of important points in the mission
PROC VIDEO_RECORDER_MANAGER()

	IF missionStage = STAGE_SCOUT_OUT_STORE
		
		//Have a video of the player first entering the store
		IF bVideoOfPlayerEnteringStore = FALSE
			IF bVideoTimerStarted[0] = FALSE
				IF PlayerIsInsideJewellers = TRUE
					iVideoTimer[0] = GET_GAME_TIMER()
					bVideoTimerStarted[0] = TRUE
				ENDIF
			ELSE
				IF GET_GAME_TIMER() > (iVideoTimer[0] + 3000)
					REPLAY_RECORD_BACK_FOR_TIME(6)
					bVideoOfPlayerEnteringStore = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		//Have a video of last pic being taken
		IF bVideoOfPlayerTakingLastPic = FALSE
			IF bVideoTimerStarted[1] = FALSE
				IF VentObjectiveDone
				AND SecurityCamObjectiveDone
				AND AlarmObjectiveDone	
					iVideoTimer[1] = GET_GAME_TIMER()
					bVideoTimerStarted[1] = TRUE
				ENDIF
			ELSE
				IF GET_GAME_TIMER() > (iVideoTimer[1] + 2000)
					REPLAY_RECORD_BACK_FOR_TIME(6)
					bVideoOfPlayerTakingLastPic = TRUE
				ENDIF
			ENDIF
		ENDIF
			
	ENDIF
	
	IF missionStage = STAGE_CHECK_OUT_ROOF
	
		//Have a video of the player first entering the roof
		IF bVideoOfPlayerEnteringRoof = FALSE
			IF bVideoTimerStarted[2] = FALSE
				IF playerOnRoof = TRUE
					iVideoTimer[2] = GET_GAME_TIMER()
					bVideoTimerStarted[2] = TRUE
				ENDIF
			ELSE
				IF GET_GAME_TIMER() > (iVideoTimer[2] + 3000)
					REPLAY_RECORD_BACK_FOR_TIME(10)
					bVideoOfPlayerEnteringRoof = TRUE
				ENDIF
			ENDIF
		ENDIF		
	
		//Have a video of the player taking a snap of the vent
		IF bVideoOfPlayerTakingPicOfVent = FALSE
			IF bVideoTimerStarted[3] = FALSE
				IF GoodShotDone = TRUE
					iVideoTimer[3] = GET_GAME_TIMER()
					bVideoTimerStarted[3] = TRUE
				ENDIF
			ELSE
				IF GET_GAME_TIMER() > (iVideoTimer[3] + 2500)
					REPLAY_RECORD_BACK_FOR_TIME(8)
					bVideoOfPlayerTakingPicOfVent = TRUE
				ENDIF
			ENDIF
		ENDIF		
		
	ENDIF

ENDPROC

//═════════════════════════════════╡ START THE MISSION ╞═══════════════════════════════════
PROC DO_STAGE_INIT_MISSION()
	
	///DEBUG STUFF
	#IF IS_DEBUG_BUILD
		jSkipReady = FALSE	
		SET_DEBUG_STAGE_NAMES()	
	#ENDIF
	///END OF DEBUG STUFF
//═════════════════════════════════╡ SET FLAGS ╞═══════════════════════════════════
	
	doneGetInCarText								= FALSE
	DoneCarChat										= FALSE
	GoToJewellersTextDisplayed						= FALSE
	GetInCarText									= FALSE
//	shopAssistantMoved3							= FALSE
//	LookAtWatchesTextDone							= FALSE
	GotoCarTextDone									= FALSE
	CheckpointReplayStarting						= FALSE
	playerCausedTroubleInsideStore					= FALSE
	
	iControlFlag = 0
	
	IF NOT IS_REPLAY_IN_PROGRESS()
		//Request opening cutscene
		REQUEST_CUTSCENE("Jh_1_int")
	ENDIF
	
	//clear up Lester from previous mission attempts if replaying mission from start menu
	IF IS_REPEAT_PLAY_ACTIVE()
		IF DOES_SCENARIO_GROUP_EXIST("SEW_MACHINE")
			IF NOT IS_SCENARIO_GROUP_ENABLED("SEW_MACHINE")
				SET_SCENARIO_GROUP_ENABLED("SEW_MACHINE", TRUE)
				PRINTSTRING("Sceanrio peds should now be active") PRINTNL()
			ENDIF
		ENDIF
		CLEAR_AREA(<<717.7, -967.6, 27.8>>, 50, TRUE)
		
		IF NOT IS_MICHAEL_WEARING_SMART_OUTFIT()
			SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
		ENDIF
		
		//Start a new load scene now to help load in the interior
		NEW_LOAD_SCENE_START_SPHERE(<<713.1, -963.4, 29.9>>, 15, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
		PRINTSTRING("clear area being called for replaying mission") PRINTNL()
	ENDIF

	//Load heist mission text.
//	LOAD_ADDITIONAL_TEXT("H3SET1", MISSION_TEXT_SLOT)	
	
//	ScenarioBlocking[1] = ADD_SCENARIO_BLOCKING_AREA(<<710.2, -959.6, 31.5>>, <<721.8, -975.6, 26.3>>)
	//Remove the bin man scenario
	ScenarioBlocking[3] = ADD_SCENARIO_BLOCKING_AREA(<<713.7754, -996.4443, 22.3085>>, <<715.7624, -991.7067, 25.6214>>)
	
	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-645.8, -301.2, 30>>, <<-674.2, -244.6, 46.1>>, FALSE, FALSE)
	
	//Grab the players car they arrived in from the trigger scene.
	IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
		IF IS_VEHICLE_DRIVEABLE(g_sTriggerSceneAssets.veh[0])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
			playersStartCar = g_sTriggerSceneAssets.veh[0]
		ENDIF
	ENDIF
	
	// Steve R - Enable interiors for Jewel store and Max Renda store
	SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_STORE, BUILDINGSTATE_DESTROYED)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_HEIST_MAX_RENDA_INTERIOR, BUILDINGSTATE_DESTROYED)
	
	//Add the player for dialogue
	ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
	
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
	
	//Request and load models.
	REQUEST_MODEL(TAILGATER)
	
	TOGGLE_STEALTH_RADAR(FALSE)
	
	//Vehicles
//	CLEAR_AREA(vCarSpawnCoords, 300, TRUE)
	
	IF IS_SCREEN_FADED_OUT()
		LOAD_ALL_OBJECTS_NOW()
	ENDIF
	
	//Do replay stuff
	IF Is_Replay_In_Progress()	
		
//		//Kill the timelapse camera
//		SET_TODS_CUTSCENE_RUNNING(FALSE)
		
		SET_CLOCK_TIME(9,0,0)
		
		IF IS_CUTSCENE_ACTIVE()
			PRINTSTRING("Waiting for cutscene to stop") PRINTNL()
			STOP_CUTSCENE()
			REMOVE_CUTSCENE()
		ENDIF
		
		IF NOT IS_SCREEN_FADED_OUT()
			DO_SCREEN_FADE_OUT(0)
		ENDIF
		
		iControlFlag 					= 0
		iCutsceneStage 					= 0
		CanMissionFail 					= FALSE
		playerCausedTroubleInsideStore 	= FALSE
		MissionStageBeingSkippedTo 		= TRUE			
		
		//Check if player chose to shitskip
        IF g_bShitskipAccepted = TRUE
			IF GET_REPLAY_MID_MISSION_STAGE() = 0
				START_REPLAY_SETUP(<<-659.1774, -270.6738, 34.8078>>, 30.7532)
				missionStage = STAGE_SCOUT_OUT_STORE
			ENDIF
			IF GET_REPLAY_MID_MISSION_STAGE() = 1
				START_REPLAY_SETUP(<<-659.1774, -270.6738, 34.8078>>, 30.7532)
				missionStage = STAGE_FIND_ROOF_ENTRANCE
			ENDIF
			IF GET_REPLAY_MID_MISSION_STAGE() = 2
				START_REPLAY_SETUP(<<-575.9287, -277.7308, 34.1598>>, 27.8937)
				missionStage = STAGE_CHECK_OUT_ROOF
			ENDIF
			IF GET_REPLAY_MID_MISSION_STAGE() = 3
				START_REPLAY_SETUP(<<-575.9287, -277.7308, 34.1598>>, 27.8937)
				missionStage = STAGE_DRIVE_TO_WAREHOUSE
			ENDIF
			IF GET_REPLAY_MID_MISSION_STAGE() = 4
				START_REPLAY_SETUP(<< 718.0418, -984.2304, 23.1442 >>, 271.3340)
				SET_UP_MISSION_PASSED()
			ENDIF
		ELSE
			IF GET_REPLAY_MID_MISSION_STAGE() = 0
				START_REPLAY_SETUP(vCarSpawnCoords, fCarSpawnHeading)
				missionStage = STAGE_DRIVE_TO_STORE			
			ENDIF
			IF GET_REPLAY_MID_MISSION_STAGE() = 1
				START_REPLAY_SETUP(<<-659.1774, -270.6738, 34.8078>>, 30.7532)
				missionStage = STAGE_SCOUT_OUT_STORE
			ENDIF
			IF GET_REPLAY_MID_MISSION_STAGE() = 2
				START_REPLAY_SETUP(<<-659.1774, -270.6738, 34.8078>>, 30.7532)
				missionStage = STAGE_FIND_ROOF_ENTRANCE
			ENDIF
			IF GET_REPLAY_MID_MISSION_STAGE() = 3
				START_REPLAY_SETUP(<<-575.9287, -277.7308, 34.1598>>, 27.8937)
				missionStage = STAGE_CHECK_OUT_ROOF		
			ENDIF
			IF GET_REPLAY_MID_MISSION_STAGE() = 4
				START_REPLAY_SETUP(<<-575.9287, -277.7308, 34.1598>>, 27.8937)
				missionStage = STAGE_DRIVE_TO_WAREHOUSE
			ENDIF
		ENDIF
	ELSE
		PRINTSTRING("******************This is NOT a replay******************") PRINTNL()
		
		//Change flag so stage selector can be used again.
		#IF IS_DEBUG_BUILD
			MissionStageBeingSkippedTo = FALSE
		#ENDIF
		
		//Stop traffic being able to drive along the road at the jewellers.
		SET_ROADS_IN_AREA(<<-631.51, -332.75, 32>>, <<-692.36, -228.83, 38>>, FALSE)

		//Move the mission stage on
		iControlFlag 			= 0
		missionStage = STAGE_OPENING_CUTSCENE
	
	ENDIF

	REQUEST_ADDITIONAL_TEXT("H3SET1", MISSION_TEXT_SLOT)
	
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		PRINTSTRING("Waiting for additional text to load") PRINTNL()
		WAIT(0)
	ENDWHILE

ENDPROC 


//PURPOSE: Deletes the temp store staff for the mocap scenes.
PROC DELETE_TEMP_STORE_STAFF()
	
	IF DOES_ENTITY_EXIST(shopGuard)
		DELETE_PED(shopGuard)
	ENDIF
	
	IF DOES_ENTITY_EXIST(shopAssistant)
		DELETE_PED(shopAssistant)
	ENDIF
	
ENDPROC


/// PURPOSE:
///    Sets bMissionFailed to be True so that we start fading for screen to fade out
///  	Sets the fail reason string and calls the flow mission failed function.
///    This tells the replay controller to start displaying the fail reason + fading
///    (If bMissionFailed is already true, this function doesn't do anything- as we're already waiting for fail fade to finish)
/// PARAMS:
///    sFailReason - text label of the fail reason
PROC SetMissionFailed(String sFailReason)
	IF bMissionFailed = FALSE
		TRIGGER_MUSIC_EVENT("JH1_FAIL")
		PRINTSTRING("Triggering music event JH1_FAIL") PRINTNL()
		FailReason = sFailReason
		
		SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
		CLEAR_PRINTS()
		KILL_ANY_CONVERSATION()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		MISSION_FLOW_MISSION_FAILED_WITH_REASON(FailReason)
		bMissionFailed = TRUE
	ENDIF
ENDPROC



//═════════════════════════════════╡ DEBUG PROCEDURES ╞═══════════════════════════════════
#IF IS_DEBUG_BUILD
//PURPOSE:		Moniters for any keyboard inputs that would alter the mission(debug pass etc)
PROC Debug_Options()
	
	// Press S on keyboard to pass the mission
	IF NOT IS_CUTSCENE_PLAYING()
		IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
			IF IS_CUTSCENE_ACTIVE()
				PRINTSTRING("Waiting for cutscene to stop") PRINTNL()
				STOP_CUTSCENE()
				REMOVE_CUTSCENE()
			ENDIF
			SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, TRUE) //Add this in here for to guarantee it's going to get called incase something else breaks
			g_sTriggerSceneAssets.ped[0] = pedContact
			g_sTriggerSceneAssets.veh[0] = vehCar
			MissionPassed()
		ENDIF
	ENDIF
	
	// Press F on keyboard to fail the mission
	IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
		CanMissionFail = TRUE
		SetMissionFailed("")
	ENDIF
	
	// Press J to skip through stages of the mission
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		
		IF jSkipReady = TRUE
		
			IF IS_CUTSCENE_ACTIVE()
				PRINTSTRING("Waiting for cutscene to stop") PRINTNL()
				STOP_CUTSCENE()
				REMOVE_CUTSCENE()
			ENDIF
			
			IF NOT IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_OUT(0)
			ENDIF		
			
			SWITCH missionStage
				
				CASE STAGE_DRIVE_TO_STORE
					
					//SET_FLAGS
					GetInCarText 					= TRUE
					doneGetInCarText 				= TRUE
					DoneCarChat 					= TRUE
					doneGlassChat					= TRUE
					CanMissionFail 					= FALSE
					playerCausedTroubleInsideStore 	= FALSE
					MissionStageBeingSkippedTo 		= TRUE
					iControlFlag 					= 0
					iCutsceneStage 					= 0
					
					missionStage = STAGE_SCOUT_OUT_STORE
					
				BREAK
				
				CASE STAGE_SCOUT_OUT_STORE				
					
					//SET_FLAGS
					CanMissionFail 					= FALSE
					playerCausedTroubleInsideStore 	= FALSE
					MissionStageBeingSkippedTo 		= TRUE
					iControlFlag 					= 0
					iCutsceneStage 					= 0
					
					missionStage = STAGE_FIND_ROOF_ENTRANCE
					
				BREAK
				
				CASE STAGE_FIND_ROOF_ENTRANCE				
					
					//SET_FLAGS
					CanMissionFail 					= FALSE
					playerCausedTroubleInsideStore 	= FALSE
					MissionStageBeingSkippedTo 		= TRUE
					iControlFlag 					= 0
					iCutsceneStage 					= 0
					
					missionStage = STAGE_CHECK_OUT_ROOF
					
				BREAK
				
				CASE STAGE_CHECK_OUT_ROOF				
					
					//SET_FLAGS
					CanMissionFail 					= FALSE
					playerCausedTroubleInsideStore 	= FALSE
					MissionStageBeingSkippedTo 		= TRUE
					iControlFlag 					= 0
					
					missionStage = STAGE_DRIVE_TO_WAREHOUSE
					
				BREAK
				
				CASE STAGE_DRIVE_TO_WAREHOUSE
					
					IF DOES_ENTITY_EXIST(vehCar)
						IF IS_VEHICLE_DRIVEABLE(vehCar)
							IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
								SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
							ENDIF
							IF NOT IS_PED_INJURED(pedContact)
								IF NOT IS_PED_IN_VEHICLE(pedContact, vehCar)
									SET_PED_INTO_VEHICLE(pedContact, vehCar, VS_FRONT_RIGHT)
								ENDIF
							ENDIF
							SET_ENTITY_COORDS(vehCar, <<721.7731, -982.6254, 23.1553>>)
							SET_ENTITY_HEADING(vehCar, 89.6079)
						ENDIF
					ENDIF
					
					//Set board intro cutscene to load and save reference to Lester and car 
					//so the heist controller can grab ownership of them and clean them up.
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, TRUE)
					g_sTriggerSceneAssets.ped[0] = pedContact
					g_sTriggerSceneAssets.veh[0] = vehCar
					
					WHILE NOT HAS_CUTSCENE_LOADED()
						PRINTSTRING("Waiting for cutscene to load") PRINTNL()
						WAIT(0)
					ENDWHILE
					
					LOAD_SCENE(<<721.7731, -982.6254, 23.1553>>)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					
					IF NOT IS_SCREEN_FADED_IN()
						DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
					ENDIF
					
					iControlFlag = 2
					
				BREAK
						
			ENDSWITCH
			
		ENDIF
	
	ENDIF
	
	// Press P to skip back through stages of the mission
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		
		IF PSkipReady = TRUE
		
			IF IS_CUTSCENE_ACTIVE()
				PRINTSTRING("Waiting for cutscene to stop") PRINTNL()
				STOP_CUTSCENE()
				REMOVE_CUTSCENE()
			ENDIF
			
			IF NOT IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_OUT(0)
			ENDIF			
		
			SWITCH missionStage
				
				CASE STAGE_DRIVE_TO_STORE					
					
					CanMissionFail 					= FALSE
					playerCausedTroubleInsideStore 	= FALSE
					MissionStageBeingSkippedTo 		= TRUE
					iControlFlag 					= 0
					iCutsceneStage 					= 0
					
					missionStage = STAGE_DRIVE_TO_STORE
					
				BREAK
				
				CASE STAGE_SCOUT_OUT_STORE
				
					CanMissionFail 					= FALSE
					playerCausedTroubleInsideStore 	= FALSE
					MissionStageBeingSkippedTo 		= TRUE
					iControlFlag 					= 0
					iCutsceneStage 					= 0
					
					missionStage = STAGE_DRIVE_TO_STORE				
				
				BREAK
		
				CASE STAGE_FIND_ROOF_ENTRANCE
				
					CanMissionFail 					= FALSE
					playerCausedTroubleInsideStore 	= FALSE
					MissionStageBeingSkippedTo 		= TRUE
					iControlFlag 					= 0
					iCutsceneStage 					= 0
					
					missionStage = STAGE_SCOUT_OUT_STORE				
				
				BREAK	
		
				CASE STAGE_CHECK_OUT_ROOF
				
					CanMissionFail 					= FALSE
					playerCausedTroubleInsideStore 	= FALSE
					MissionStageBeingSkippedTo 		= TRUE
					iControlFlag 					= 0
					iCutsceneStage 					= 0
					
					missionStage = STAGE_FIND_ROOF_ENTRANCE				
				
				BREAK
				
				CASE STAGE_DRIVE_TO_WAREHOUSE
				
					CanMissionFail 					= FALSE
					playerCausedTroubleInsideStore 	= FALSE
					MissionStageBeingSkippedTo 		= TRUE
					iControlFlag 					= 0
					iCutsceneStage 					= 0
					
					missionStage = STAGE_CHECK_OUT_ROOF
					
				BREAK
				
			ENDSWITCH
			
		ENDIF
		
	ENDIF
	
ENDPROC

#ENDIF


//═════════════════════════════════╡ NON-Stage FUNCTIONS AND PROCEDURES ╞═══════════════════════════════════


//This will check for anything that should fail the mission.
PROC  FAIL_CHECKS()
	
	IF CanMissionFail = TRUE
		IF bMissionFailed = FALSE
		
			
			IF missionStage = STAGE_CHECK_OUT_ROOF
				//Fail if player gets a wanted level while on the roof top
				IF playerOnRoof = TRUE
				AND GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					PRINTSTRING("***********************~s~Michael failed to check out the roof.*************************") PRINTNL()
					SetMissionFailed("FAIL_ROOF2") //~s~Michael's cover was blown.	
				ENDIF
			
				//Fail if Michael drives off without checking out the roof
				IF iControlFlag = 1
					IF DOES_ENTITY_EXIST(vehCar)
						IF IS_VEHICLE_DRIVEABLE(vehCar)
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vplayerStartRoofCoords) > 80
									PRINTSTRING("***********************~s~Michael failed to check out the roof.*************************") PRINTNL()
									SetMissionFailed("FAIL_ROOF1") //~s~Michael failed to check out the roof.	
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Run checks for player having a wanted level
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) != 0
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-631.4, -237.6, 37.3>>) < 10
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-631.130676,-237.373398,36.556953>>, <<-617.120361,-227.004135,39.915310>>, 17.750000)
						PRINTSTRING("***********************Players entered the store with a wanted level*************************") PRINTNL()
						SetMissionFailed("FAIL_BBWAN") //Michael entered the store with a wanted level.	
					ENDIF
				ENDIF
			ENDIF
			
			//Check to see if player is trying to get to the store without hitting the trigger
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-631.4, -237.6, 37.3>>) < 80
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-649.311646,-218.046204,52.554173>>, <<-603.178650,-298.184631,34.903049>>, 32.500000)
						IF doneWarningText = FALSE
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH70", CONV_PRIORITY_MEDIUM)
								//Are you crazy? This is a pedestrian zone. Turn around!
								doneWarningText = TRUE
							ENDIF
						ELSE
							//Fail if player gets too close to store in car
							IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<-644.376892,-232.851837,35.540142>>, <<-629.327698,-251.028458,41.738682>>, 15.250000)
								PRINTSTRING("***********************Player has got too close to the store in his car*************************") PRINTNL()
								SetMissionFailed("FAIL_SHOP3") //Michael was spotted driving in a pedestrian zone by shop staff.
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF DOES_ENTITY_EXIST(vehCar)
					IF IS_VEHICLE_DRIVEABLE(vehCar)
						IF IS_ENTITY_IN_ANGLED_AREA(vehCar, <<-644.376892,-232.851837,35.540142>>, <<-629.327698,-251.028458,41.738682>>, 15.250000)
							PRINTSTRING("***********************Player's car has got too close to the store *************************") PRINTNL()
							SetMissionFailed("FAIL_SHOP3") //Michael was spotted driving in a pedestrian zone by shop staff.							
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			//Fail if player vehicle is destroyed
			IF NOT IS_VEHICLE_DRIVEABLE(vehCar, TRUE)
				PRINTSTRING("***********************Players car is not driveable*************************") PRINTNL()
				SetMissionFailed("CMN_GENDEST") //~r~You destroyed the car.
			ENDIF
			
			//Fail if the vehicle is stuck on its roof
			IF IS_VEHICLE_STUCK_ON_ROOF(vehCar)
				PRINTSTRING("***********************Players car is stuck*************************") PRINTNL()
				SetMissionFailed("FAIL_CAR") //The car is stuck.
			ENDIF
			
			IF IS_VEHICLE_FUCKED(vehCar)
				PRINTSTRING("***********************Players car is fucked*************************") PRINTNL()
				SetMissionFailed("CMN_GENDEST") //The car is stuck.
			ENDIF				
			
			//Fail if the vehicle is stuck
			IF DOES_ENTITY_EXIST(vehCar)
				IF IS_VEHICLE_PERMANENTLY_STUCK(vehCar)
					PRINTSTRING("***********************Players car is stuck*************************") PRINTNL()
					SetMissionFailed("FAIL_CAR") //The car is stuck.	
				ENDIF
			ENDIF
			
			//Fail if Lester is killed
			IF DOES_ENTITY_EXIST(pedContact)
				IF IS_PED_INJURED(pedContact)
					PRINTSTRING("***********************Lester has died*************************") PRINTNL()
					SetMissionFailed("FAIL_LEST") // ~r~Lester died.
				ENDIF
			ENDIF
			
			//Fail if shop assistant2 is killed
			IF DOES_ENTITY_EXIST(shopAssistant)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-631.3, -239, 37.4>>) < 100
					IF IS_PED_INJURED(shopAssistant)
						PRINTSTRING("***********************Shop assistant2 has died*************************") PRINTNL()
						SetMissionFailed("FAIL_ASS")  // ~r~The shop assistant was killed.
					ENDIF
				ENDIF
			ENDIF
				
			//Fail if shop Guard is killed
			IF DOES_ENTITY_EXIST(shopGuard)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-631.3, -239, 37.4>>) < 100
					IF IS_PED_INJURED(shopGuard)
						PRINTSTRING("***********************Shop guard has died*************************") PRINTNL()
						SetMissionFailed("FAIL_GUARD") // ~r~The shop guard was killed.
					ENDIF
				ENDIF
			ENDIF
			
			//Whilst inside the jewellers
			GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), Current_Ped_weapon)
			
			//Holster players weapon if they have one equipped.
			IF GunHolstered = FALSE
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-631.3, -239, 37.4>>) < 30	
					IF Current_Ped_weapon != WEAPONTYPE_UNARMED
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED)
						GunHolstered = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			//Fail if the player brings out a weapon
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-631.3, -239, 37.4>>) < 20	
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF Current_Ped_weapon != WEAPONTYPE_UNARMED
						IF NOT IS_PHONE_ONSCREEN()
							vplayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
							IF vplayerCoords.z < 46
								PRINTSTRING("***********************Player was spotted carrying a weapon*************************") PRINTNL()
								SetMissionFailed("FAIL_WEAP") // ~r~Michael were spotted carrying a weapon.
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Check if player is firing bullets at the store
			IF IS_BULLET_IN_AREA(<<-624.5, -232.5, 36.9>>, 9)
			OR IS_BULLET_IN_AREA(<<-632.1, -237.9, 37.1>>, 9)
				PRINTSTRING("***********************playerCausedTroubleInsideStore = TRUE firing bullets in the shop area*************************") PRINTNL()
				playerCausedTroubleInsideStore = TRUE
			ENDIF
			
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-622, -231, 37>>, <<20, 14, 5>>, FALSE, TRUE, TM_ON_FOOT)	
				GET_CLOSEST_PED(GET_ENTITY_COORDS(PLAYER_PED_ID()), 5, TRUE, TRUE, ClosestPed, FALSE, TRUE)
				//Fail if player tries to combat any peds inside the store
				IF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
				
					IF DOES_ENTITY_EXIST(ClosestPed)
	//					PRINTSTRING("closestPed Exists") PRINTNL()
						IF NOT IS_PED_INJURED(ClosestPed)
							IF IS_ENTITY_TOUCHING_ENTITY(PLAYER_PED_ID(), ClosestPed)
								PRINTSTRING("***********************Player is in Melee combat inside the store*************************") PRINTNL()
								playerCausedTroubleInsideStore = TRUE
							ENDIF
						ENDIF
					ELSE
	//					PRINTSTRING("closestPed doesn't Exist") PRINTNL()
					ENDIF
				ENDIF
			ENDIF
			
			//Check if the player has killed anyone near the store
			IF DOES_ENTITY_EXIST(shopGuard)
				IF NOT IS_PED_INJURED(shopGuard)
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), shopGuard) < 20
						STAT_GET_INT(SP0_KILLS_INNOCENTS, iKilledPedsCloseToStore, 0)
						IF iKilledPedsCloseToStore > iKilledPedsAwayFromStore
							PRINTSTRING("***********************Player has killed an innocent ped less than 20m from the shop guard*************************") PRINTNL()
							playerCausedTroubleInsideStore = TRUE
						ENDIF
					ELSE
						STAT_GET_INT(SP0_KILLS_INNOCENTS, iKilledPedsAwayFromStore, 0)
					ENDIF
				ENDIF
			ENDIF

			IF playerCausedTroubleInsideStore = TRUE
				PRINTSTRING("***********************MISSION FAIL FADE CALLED AS Player is in Melee combat inside the store*************************") PRINTNL()
				SetMissionFailed("FAIL_SHOP2") //Michael blew his cover.
			ENDIF
			
			//Fail if you get too far away from Lester
			IF DOES_ENTITY_EXIST(pedContact)
				IF NOT IS_PED_INJURED(pedContact)
					IF missionStage = STAGE_CHECK_OUT_ROOF
						IF doneReturnText = FALSE
							IF DOES_BLIP_EXIST(BlipVeh)
								IF Get_distance_between_peds(pedContact, PLAYER_PED_ID()) > 130
									CLEAR_PRINTS()
									PRINT_NOW("GOD8", DEFAULT_GOD_TEXT_TIME, 1)//~s~Return to ~b~Lester.
									doneReturnText = TRUE
								ENDIF
							ENDIF
						ENDIF
						IF Get_distance_between_peds(pedContact, PLAYER_PED_ID()) > 150
							PRINTSTRING("***********************player is too far away from Lester*************************") PRINTNL()
							SetMissionFailed("FAIL_DIST") //Lester was abandoned.
						ENDIF					
					ELSE
						IF doneReturnText = FALSE
							IF DOES_BLIP_EXIST(BlipVeh)
								IF Get_distance_between_peds(pedContact, PLAYER_PED_ID()) > 70
									CLEAR_PRINTS()
									PRINT_NOW("GOD8", DEFAULT_GOD_TEXT_TIME, 1)//~s~Return to ~b~Lester.
									doneReturnText = TRUE
								ENDIF
							ENDIF
						ENDIF
						IF Get_distance_between_peds(pedContact, PLAYER_PED_ID()) > 100
							PRINTSTRING("***********************player is too far away from Lester*************************") PRINTNL()
							SetMissionFailed("FAIL_DIST") //Lester was abandoned.
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Fail if player enters the office inside the store.
			IF doneChatPH53 = FALSE
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-625.9, -230.3, 37.2>>) < 15
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-628.614502,-227.853210,36.767178>>, <<-632.386353,-230.547195,39.406311>>, 2.750000)
						IF DOES_ENTITY_EXIST(shopAssistant)
							IF NOT IS_PED_INJURED(shopAssistant)
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, shopAssistant, "JewelSales")
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								TASK_TURN_PED_TO_FACE_ENTITY(shopAssistant, PLAYER_PED_ID(), 8000)
								SET_PED_KEEP_TASK(shopAssistant, TRUE)	
								//commenting out as conversation has been removed from dialoguestar
//								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH53", CONV_PRIORITY_MEDIUM)
//									//Hey! Only staff can go there.
//									OfficeFail = TRUE
//									doneChatPH53 = TRUE
//								ENDIF
								OfficeFail = TRUE
								doneChatPH53 = TRUE
							ENDIF
						ENDIF
					ENDIF
					//Fail if player goes behind the counter
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-620.852295,-232.565659,36.558861>>, <<-623.239685,-229.273026,38.558861>>, 2.250000)
						IF DOES_ENTITY_EXIST(shopAssistant)
							IF NOT IS_PED_INJURED(shopAssistant)
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, shopAssistant, "JewelSales")
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								TASK_TURN_PED_TO_FACE_ENTITY(shopAssistant, PLAYER_PED_ID(), 8000)
								SET_PED_KEEP_TASK(shopAssistant, TRUE)
								//commenting out as conversation has been removed from dialoguestar
//								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH53", CONV_PRIORITY_MEDIUM)
//									//Hey! Only staff can go there.
//									CounterFail = TRUE
//									doneChatPH53 = TRUE
//								ENDIF
								CounterFail = TRUE
								doneChatPH53 = TRUE								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF doneChatPH53 = TRUE
			AND OfficeFail = TRUE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRINTSTRING("***********************MISSION FAIL FADE CALLED AS player entered the office of the jewellers*************************") PRINTNL()
					SetMissionFailed("FAIL_SHOP5") //Michael was spotted entering the managers office.	
				ELSE
					PRINTSTRING("can't fail as conversation is on-going") PRINTNL()
				ENDIF
			ENDIF		
			
			IF doneChatPH53 = TRUE
			AND CounterFail = TRUE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					PRINTSTRING("***********************MISSION FAIL FADE CALLED AS player went behind the counter*************************") PRINTNL()
					SetMissionFailed("FAIL_SHOP6") //Michael was spotted going behind the pay desk.
				ENDIF
			ENDIF		
			
//			//Check for player wearing correct clothes before he's been in the store
//			IF NOT IS_MICHAEL_WEARING_SUITABLE_OUTFIT()
//				//Check for player wearing correct clothes before he's been in the store
//				IF missionStage = STAGE_DRIVE_TO_STORE
//				OR missionStage = STAGE_SCOUT_OUT_STORE			
//					michaelInRightClothes = FALSE
//					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WARN_SUIT")
//						PRINT_HELP_FOREVER("WARN_SUIT")
//						//Get changed into a smart outfit before entering the store.
//					ENDIF
//				ENDIF
//				//Warn player not to enter store without a suit if he gets too close to the store
//				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(shopGuard)) < 20
//					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WARN_SUIT")
//						PRINT_HELP_FOREVER("WARN_SUIT")
//						//Get changed into a smart outfit before entering the store.
//					ENDIF	
//				ENDIF
//				//Fail mission if player goes into the store not wearing a suit
//				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-631.130676,-237.373398,36.556953>>, <<-617.120361,-227.004135,39.915310>>, 17.750000)
//					PRINTSTRING("***********************Players entered the store with a wanted level*************************") PRINTNL()
//					SetMissionFailed("FAIL_SUIT") //Michael entered the store wearing the wrong clothes.
//				ENDIF	
//			ELSE
//				IF missionStage = STAGE_DRIVE_TO_STORE
//				OR missionStage = STAGE_SCOUT_OUT_STORE	
//					michaelInRightClothes = TRUE
//					IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WARN_SUIT")
//						CLEAR_HELP()
//					ENDIF
//				ENDIF
//				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("WARN_SUIT")
//					CLEAR_HELP()
//				ENDIF
//			ENDIF
		ENDIF
		
		
		// Wait for replay controller to finish fading
		IF bMissionFailed = TRUE
			IF convoskilled = FALSE
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				KILL_ANY_CONVERSATION()
				CLEAR_PRINTS()
				convoskilled = TRUE
			ENDIF
			IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
				IF DOES_ENTITY_EXIST(pedContact)
					DELETE_PED(pedContact)
				ENDIF
				IF PlayerIsInsideJewellers = TRUE
					MISSION_FLOW_SET_FAIL_WARP_LOCATION(<<-660.2747, -264.2336, 35.0936>>, 114.1384)
					SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION(<<-662.3032, -265.4387, 34.9448>>, 30.6827)
				ENDIF
//				REMOVE_PED_COMP_ITEM_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P0_PROGRAMMER_GLASSES)
				RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
				MissionFailed()
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Controls everything to do with the ambient scene inside the store
PROC JEWEL_STORE_AMBIENT_CONTROLLER()
	
	IF JewelStoreModelsRequested = FALSE
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-631.3, -237.4, 39>>) < 20
			REQUEST_MODEL(A_M_M_BEVHILLS_01)
			REQUEST_WAYPOINT_RECORDING("BB_JEW_6")
			REQUEST_ANIM_DICT("amb@world_human_window_shop@male@idle_a")
			JewelStoreModelsRequested = TRUE
		ENDIF
	ENDIF
	
	IF ShopCustomerFrozen = FALSE
		IF DOES_ENTITY_EXIST(ShopCustomer[0])
			IF NOT IS_PED_INJURED(ShopCustomer[0])	
				IF IS_ENTITY_PLAYING_ANIM(ShopCustomer[0], "amb@world_human_window_shop@male@idle_a", "browse_a")
					FREEZE_ENTITY_POSITION(ShopCustomer[0], TRUE)
					ShopCustomerFrozen = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	//Create the 1st customer and make them enter the store
	IF JewelStoreModelsRequested = TRUE
		IF PlayerIsInsideJewellers = TRUE
			IF ShopCustomerTaskGiven[0] = FALSE
				IF NOT DOES_ENTITY_EXIST(ShopCustomer[0])
					IF HAS_MODEL_LOADED(A_M_M_BEVHILLS_01)
					AND GET_IS_WAYPOINT_RECORDING_LOADED("BB_JEW_6")
					AND HAS_ANIM_DICT_LOADED("amb@world_human_window_shop@male@idle_a")
						ShopCustomer[0] = CREATE_PED(PEDTYPE_MISSION, A_M_M_BEVHILLS_01, <<-628.4003, -244.6562, 37.2985>>, 34.4691)
						SET_MODEL_AS_NO_LONGER_NEEDED(A_M_M_BEVHILLS_01)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ShopCustomer[0], TRUE)
						TASK_FOLLOW_WAYPOINT_RECORDING(ShopCustomer[0], "BB_JEW_6")
						ShopCustomerTaskGiven[0] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Stop the 1st customer at the 1st counter and make him play 3 anims in a sequence before moving onto next counter.
	IF ShopCustomerTaskGiven[1] = FALSE
		IF DOES_ENTITY_EXIST(ShopCustomer[0])
			IF NOT IS_PED_INJURED(ShopCustomer[0])
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(ShopCustomer[0])
					IF GET_PED_WAYPOINT_PROGRESS(ShopCustomer[0]) > 11
						WAYPOINT_PLAYBACK_PAUSE(ShopCustomer[0], FALSE)
						OPEN_SEQUENCE_TASK(shopCustomerSeq)
							TASK_ACHIEVE_HEADING(NULL, 214.3621)
							TASK_PLAY_ANIM(NULL, "amb@world_human_window_shop@male@idle_a", "browse_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							TASK_PLAY_ANIM(NULL, "amb@world_human_window_shop@male@idle_a", "browse_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							TASK_PLAY_ANIM(NULL, "amb@world_human_window_shop@male@idle_a", "browse_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
						CLOSE_SEQUENCE_TASK(shopCustomerSeq)
						TASK_PERFORM_SEQUENCE(ShopCustomer[0], shopCustomerSeq)
						CLEAR_SEQUENCE_TASK(shopCustomerSeq)
						TASK_LOOK_AT_COORD(ShopCustomer[0], <<-625.7, -238.8, 37.8>>, -1, SLF_USE_TORSO, SLF_LOOKAT_HIGH)
						ShopCustomerTaskGiven[1] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Resume the waypoint progress as soon as the customer has finished playing the last anim
	IF ShopCustomerTaskGiven[2] = FALSE
		IF DOES_ENTITY_EXIST(ShopCustomer[0])
			IF NOT IS_PED_INJURED(ShopCustomer[0])	
				IF IS_ENTITY_PLAYING_ANIM(ShopCustomer[0], "amb@world_human_window_shop@male@idle_a", "browse_c")
					customerReadyToMoveOn = TRUE
				ENDIF
				IF customerReadyToMoveOn = TRUE
					IF NOT IS_ENTITY_PLAYING_ANIM(ShopCustomer[0], "amb@world_human_window_shop@male@idle_a", "browse_c")
						IF ShopCustomerFrozen = TRUE
							FREEZE_ENTITY_POSITION(ShopCustomer[0], FALSE)
							ShopCustomerFrozen = FALSE
						ENDIF
						TASK_FOLLOW_WAYPOINT_RECORDING(ShopCustomer[0], "BB_JEW_6", 12)
						customerReadyToMoveOn = FALSE
						ShopCustomerTaskGiven[2] = TRUE
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
	//Stop the 1st customer at the 2nd counter and make him play 3 anims in a sequence before moving onto next counter.
	IF ShopCustomerTaskGiven[3] = FALSE
		IF DOES_ENTITY_EXIST(ShopCustomer[0])
			IF NOT IS_PED_INJURED(ShopCustomer[0])
				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(ShopCustomer[0])
					IF GET_PED_WAYPOINT_PROGRESS(ShopCustomer[0]) > 18
						WAYPOINT_PLAYBACK_PAUSE(ShopCustomer[0], FALSE)
						OPEN_SEQUENCE_TASK(shopCustomerSeq)
							TASK_ACHIEVE_HEADING(NULL, 302.2427)
							TASK_PLAY_ANIM(NULL, "amb@world_human_window_shop@male@idle_a", "browse_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							TASK_PLAY_ANIM(NULL, "amb@world_human_window_shop@male@idle_a", "browse_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							TASK_PLAY_ANIM(NULL, "amb@world_human_window_shop@male@idle_a", "browse_c", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
						CLOSE_SEQUENCE_TASK(shopCustomerSeq)
						TASK_PERFORM_SEQUENCE(ShopCustomer[0], shopCustomerSeq)
						CLEAR_SEQUENCE_TASK(shopCustomerSeq)
						TASK_LOOK_AT_COORD(ShopCustomer[0], <<-617.6, -229.3, 38>>, -1, SLF_USE_TORSO, SLF_LOOKAT_HIGH)
						ShopCustomerTaskGiven[3] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Resume the waypoint progress as soon as the customer has finished playing the last anim
	IF ShopCustomerTaskGiven[4] = FALSE
		IF DOES_ENTITY_EXIST(ShopCustomer[0])
			IF NOT IS_PED_INJURED(ShopCustomer[0])	
				IF IS_ENTITY_PLAYING_ANIM(ShopCustomer[0], "amb@world_human_window_shop@male@idle_a", "browse_c")
					customerReadyToMoveOn = TRUE
				ENDIF
				IF customerReadyToMoveOn = TRUE
					IF NOT IS_ENTITY_PLAYING_ANIM(ShopCustomer[0], "amb@world_human_window_shop@male@idle_a", "browse_c")
						IF ShopCustomerFrozen = TRUE
							FREEZE_ENTITY_POSITION(ShopCustomer[0], FALSE)
							ShopCustomerFrozen = FALSE
						ENDIF						
						TASK_FOLLOW_WAYPOINT_RECORDING(ShopCustomer[0], "BB_JEW_6", 19)
						ShopCustomerTaskGiven[4] = TRUE
					ENDIF
				ENDIF	
			ENDIF
		ENDIF
	ENDIF
	
	//Set ped as no longer needed once they've finished their cycle
	IF ShopCustomerTaskGiven[5] = FALSE	
		IF ShopCustomerTaskGiven[4] = TRUE
			IF DOES_ENTITY_EXIST(ShopCustomer[0])
				IF NOT IS_PED_INJURED(ShopCustomer[0])		
					IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(ShopCustomer[0])
						ShopCustomerReadyToLeave = TRUE
					ENDIF
					IF ShopCustomerReadyToLeave = TRUE
						IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(ShopCustomer[0])
							CLEAR_PED_TASKS(ShopCustomer[0])
							SET_PED_AS_NO_LONGER_NEEDED(ShopCustomer[0])
							REMOVE_ANIM_DICT("amb@world_human_window_shop@male@idle_a")
							REMOVE_WAYPOINT_RECORDING("BB_JEW_6")
							ShopCustomerTaskGiven[5] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Creates and deletes the staff inside the jewel store warehouse.
PROC JEWEL_STORE_STAFF_CONTROLLER()

	//Create the shop staff if player is less than 100m from the store
	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-631.3, -239, 37.4>>) < 100
		IF StoreStaffCreated = FALSE
			
			DoorAnimStopped = FALSE
			
			//Request the models and anim dictionaries required
			IF modelsRequested = FALSE
				REQUEST_MODEL(U_M_M_JEWELSEC_01)
				REQUEST_MODEL(IG_JEWELASS)
				REQUEST_ANIM_DICT("amb@world_human_stand_guard@male@idle_a")
				REQUEST_ANIM_DICT("MISSHEIST_JEWEL_SETUP")
				modelsRequested = TRUE
			ENDIF
			
			//Store the interior jewellery store in memory for loading reasons.
			IF interiorPinnedInMemory = FALSE
				InteriorJewelleryShop = GET_INTERIOR_AT_COORDS(<<-623, -229.7, 38.7>>)	
				PIN_INTERIOR_IN_MEMORY(InteriorJewelleryShop)
				PRINTSTRING("GET_INTERIOR_AT_COORDS BEING CALLED") PRINTNL()
				interiorPinnedInMemory = TRUE
			ENDIF
			
			//Create the staff and give them their tasks
			IF interiorPinnedInMemory = TRUE
				IF IS_INTERIOR_READY(InteriorJewelleryShop)
					IF modelsRequested = TRUE
						IF NOT DOES_ENTITY_EXIST(shopGuard)
							IF HAS_MODEL_LOADED(U_M_M_JEWELSEC_01)
								shopGuard = CREATE_PED(PEDTYPE_MISSION, U_M_M_JEWELSEC_01, <<-630.9388, -235.8340, 37.0570>>, 214.5493) //<<-630.8662, -235.6432, 37.0570>>, 214.5493)//<< -631.2426, -235.5237, 37.0570 >>, 295.9203)
								SET_ENTITY_LOAD_COLLISION_FLAG(shopGuard, TRUE)
								SET_PED_RESET_FLAG(shopGuard, PRF_ExpandPedCapsuleFromSkeleton, TRUE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(shopGuard, TRUE)
//								TASK_PLAY_ANIM(shopGuard, "amb@world_human_stand_guard@male@idle_a", "idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
								SET_PED_KEEP_TASK(shopGuard, TRUE)
								FREEZE_ENTITY_POSITION(shopGuard, TRUE)
							ENDIF
						ENDIF
						
						IF NOT DOES_ENTITY_EXIST(shopAssistant)
							REQUEST_ANIM_DICT("MISSHEIST_JEWEL_SETUP")
							IF HAS_MODEL_LOADED(IG_JEWELASS)
							AND HAS_ANIM_DICT_LOADED("MISSHEIST_JEWEL_SETUP")
								shopAssistant = CREATE_PED(PEDTYPE_MISSION, IG_JEWELASS, <<-623.0729, -230.0248, 37.0570>>, 130.6013)//<<-623.4161, -230.4396, 37.0570>>, 120.5971)
								SET_PED_PROP_INDEX(shopAssistant, ANCHOR_EYES, 0)
								SET_ENTITY_LOAD_COLLISION_FLAG(shopAssistant, TRUE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(shopAssistant, TRUE)
								TASK_PLAY_ANIM(shopAssistant, "MISSHEIST_JEWEL_SETUP", "idle_storeclerk", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
								SET_PED_KEEP_TASK(shopAssistant, TRUE)
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
		
			IF DOES_ENTITY_EXIST(shopGuard)
			AND DOES_ENTITY_EXIST(shopAssistant)
				SET_MODEL_AS_NO_LONGER_NEEDED(U_M_M_JEWELSEC_01)
				SET_MODEL_AS_NO_LONGER_NEEDED(IG_JEWELASS)
				GuardOpeningDoorSequenceStarted = FALSE
				StoreStaffCreated = TRUE
			ENDIF			
		ELSE
			//Handle the security guard opening and closing the door for the player.
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(shopGuard)
					IF NOT IS_PED_INJURED(shopGuard)
						
						//Run a control to check if player is entering/leaving or not near the guard to determine which anim to play.
//						IF SyncdSceneDisrupted = FALSE	
							IF PlayerIsInsideJewellers = TRUE
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), shopGuard) > 5
									PlayerApproching = FALSE
//									PRINTSTRING("PlayerApproching = FALSE 1") PRINTNL()
								ELSE
									IF GET_ENTITY_HEADING(PLAYER_PED_ID()) > 51.7575
									AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < 185.4324
										iPlayerNotApproachingTimer = GET_GAME_TIMER()
										PlayerApproching = TRUE
//										PRINTSTRING("PlayerApproching = TRUE 1") PRINTNL()
									ELSE
										//Have a timer before confirming that the player is not wanting the door open
										IF GET_GAME_TIMER() > (iPlayerNotApproachingTimer + 2000)
											PlayerApproching = FALSE
//											PRINTSTRING("PlayerApproching = FALSE 2") PRINTNL()
										ENDIF
									ENDIF						
								ENDIF
							ENDIF
							IF PlayerIsInsideJewellers = FALSE
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), shopGuard) > 7
									PlayerApproching = FALSE
//									PRINTSTRING("PlayerApproching = FALSE 3") PRINTNL()
								ELSE
									IF (GET_ENTITY_HEADING(PLAYER_PED_ID()) > 252 AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < 360)
									OR (GET_ENTITY_HEADING(PLAYER_PED_ID()) > 0 AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < 8)
										iPlayerNotApproachingTimer = GET_GAME_TIMER()
										PlayerApproching = TRUE
//										PRINTSTRING("PlayerApproching = TRUE 2") PRINTNL()
									ELSE
										//Have a timer before confirming that the player is not wanting the door open
										IF GET_GAME_TIMER() > (iPlayerNotApproachingTimer + 4000)
											PlayerApproching = FALSE
//											PRINTSTRING("PlayerApproching = FALSE 4") PRINTNL()
										ENDIF
									ENDIF
								ENDIF
							ENDIF									
							
							//Open the door if player is approaching guard. but is not standing between the door and the guard.
							IF GuardOpeningDoorSequenceStarted = FALSE
								IF PlayerApproching = TRUE
								AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-631.071350,-236.910248,36.568245>>, <<-631.664856,-236.094299,39.306984>>, 1.000000)
									IF NOT IS_ENTITY_PLAYING_ANIM(shopGuard, "MISSHEIST_JEWEL_SETUP", "enter_guard")
									AND NOT IS_ENTITY_PLAYING_ANIM(shopGuard, "MISSHEIST_JEWEL_SETUP", "exit_guard")
									AND NOT IS_ENTITY_PLAYING_ANIM(shopGuard, "MISSHEIST_JEWEL_SETUP", "base_guard")
										ShopGuardSyncdSceneOpen = CREATE_SYNCHRONIZED_SCENE(<<-631.96, -236.33, 38.21>>, << 0, 0.000, -54 >>)		
										TASK_SYNCHRONIZED_SCENE(shopGuard, ShopGuardSyncdSceneOpen, "MISSHEIST_JEWEL_SETUP", "enter_guard", SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)
										PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-631.96, -236.33, 38.21>>, 2, P_JEWEL_DOOR_L, ShopGuardSyncdSceneOpen, "enter_door", "MISSHEIST_JEWEL_SETUP", SLOW_BLEND_IN)
										SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ShopGuardSyncdSceneOpen, FALSE)
										GuardOpeningDoorSequenceStarted = TRUE
										DoorAnimStopped = FALSE
									ENDIF				
								ENDIF	
							ENDIF
							
							//Have the guard hold the door open until player is clear of doorway
							IF GuardOpeningDoorSequenceStarted = TRUE
								//Set flag to tell when the guard is opening the door
								IF IS_ENTITY_PLAYING_ANIM(shopGuard, "MISSHEIST_JEWEL_SETUP", "enter_guard")
									GuardOpeningDoor = TRUE
								ENDIF
								IF GuardOpeningDoor = TRUE
//									IF NOT IS_ENTITY_PLAYING_ANIM(shopGuard, "MISSHEIST_JEWEL_SETUP", "enter_guard")
									IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(ShopGuardSyncdSceneOpen)
									OR GET_SYNCHRONIZED_SCENE_PHASE(ShopGuardSyncdSceneOpen) >= 0.96
										ShopGuardSyncdSceneHold = CREATE_SYNCHRONIZED_SCENE(<<-631.96, -236.33, 38.21>>, << 0, 0.000, -54 >>)
										TASK_SYNCHRONIZED_SCENE(shopGuard, ShopGuardSyncdSceneHold, "MISSHEIST_JEWEL_SETUP", "base_guard", SLOW_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)
										PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-631.96, -236.33, 38.21>>, 2, P_JEWEL_DOOR_L, ShopGuardSyncdSceneHold, "base_door", "MISSHEIST_JEWEL_SETUP", INSTANT_BLEND_IN)
										SET_SYNCHRONIZED_SCENE_LOOPED(ShopGuardSyncdSceneHold, TRUE)
										SET_PED_KEEP_TASK(shopGuard, TRUE)
										GuardOpeningDoor = FALSE
										DoorAnimStopped = FALSE
									ENDIF	
								ENDIF					
							ENDIF
							
							//Close the door if the guard is holding it open.
							IF GuardOpeningDoorSequenceStarted = TRUE
								IF IS_ENTITY_PLAYING_ANIM(shopGuard, "MISSHEIST_JEWEL_SETUP", "base_guard")
									IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-630.477356,-238.321152,36.588997>>, <<-631.923706,-236.355164,38.814049>>, 3.500000)
									AND PlayerApproching = FALSE
										ShopGuardSyncdScene = CREATE_SYNCHRONIZED_SCENE(<<-631.96, -236.33, 38.21>>, << 0, 0.000, -54 >>)
										TASK_SYNCHRONIZED_SCENE(shopGuard, ShopGuardSyncdScene, "MISSHEIST_JEWEL_SETUP", "exit_guard", NORMAL_BLEND_IN, REALLY_SLOW_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT|SYNCED_SCENE_USE_PHYSICS)	
										PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-631.96, -236.33, 38.21>>, 2, P_JEWEL_DOOR_L, ShopGuardSyncdScene, "exit_door", "MISSHEIST_JEWEL_SETUP", NORMAL_BLEND_IN)
										SET_SYNCHRONIZED_SCENE_LOOPED(ShopGuardSyncdScene, FALSE)
										SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(ShopGuardSyncdScene, FALSE)
										SET_PED_KEEP_TASK(shopGuard, TRUE)
										DoorAnimStopped = FALSE
									ENDIF
								ENDIF	
								
								//Set flag to tell when the guard is closing the door to allow for the opening sequence to begin again if necessary.
								IF IS_ENTITY_PLAYING_ANIM(shopGuard, "MISSHEIST_JEWEL_SETUP", "exit_guard")
									GuardOpeningDoorSequenceStarted = FALSE
								ENDIF					
							ENDIF
							
							//Play a looped idle anim if door has been closed and player is not approaching it.
							IF GuardOpeningDoorSequenceStarted = FALSE
								IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-630.477356,-238.321152,36.588997>>, <<-631.923706,-236.355164,38.814049>>, 3.500000)
								AND PlayerApproching = FALSE
									IF NOT IS_ENTITY_PLAYING_ANIM(shopGuard, "amb@world_human_stand_guard@male@idle_a", "idle_a")
									AND NOT IS_ENTITY_PLAYING_ANIM(shopGuard, "MISSHEIST_JEWEL_SETUP", "enter_guard")
									AND NOT IS_ENTITY_PLAYING_ANIM(shopGuard, "MISSHEIST_JEWEL_SETUP", "exit_guard")
									AND NOT IS_ENTITY_PLAYING_ANIM(shopGuard, "MISSHEIST_JEWEL_SETUP", "base_guard")
									AND GuardOpeningDoor = FALSE
										//Create the sync'd scene for the sweatshop workers to play their anims.
										TASK_PLAY_ANIM(shopGuard, "amb@world_human_stand_guard@male@idle_a", "idle_a", REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
										SET_PED_KEEP_TASK(shopGuard, TRUE)
									ENDIF
									//Set the doors to closed if guard is playing idle anim
									IF IS_ENTITY_PLAYING_ANIM(shopGuard, "amb@world_human_stand_guard@male@idle_a", "idle_a")
										GET_STATE_OF_CLOSEST_DOOR_OF_TYPE(P_JEWEL_DOOR_L, <<-632, -236, 38>>, bLockstate, fOpenRatio)
										IF fOpenRatio <> 0
											IF DoorAnimStopped = FALSE
												fOpenRatio = 0
												bLockstate = TRUE
												IF IS_SYNCHRONIZED_SCENE_RUNNING(ShopGuardSyncdScene)
													PRINTSTRING("ShopGuardSyncdScene is running ") PRINTNL()
													STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-631.96, -236.33, 38.21>>, 2, P_JEWEL_DOOR_L, NORMAL_BLEND_OUT)
													SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(P_JEWEL_DOOR_L, <<-632, -236, 38>>, bLockstate, fOpenRatio)
													DoorAnimStopped = TRUE
												ELSE
													PRINTSTRING("ShopGuardSyncdScene is not running ") PRINTNL()
													SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(P_JEWEL_DOOR_L, <<-632, -236, 38>>, bLockstate, fOpenRatio)
													DoorAnimStopped = TRUE
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
//						ENDIF
						
//						//Run checks here that should cancel any anim and put the guard and door back to idle if the player is fucking around.
//						//If guard is starting the open door anim but player gets between him and the door, cancel his anim and set him back to idle 
//						IF SyncdSceneDisrupted = FALSE	
//							IF IS_ENTITY_PLAYING_ANIM(shopGuard, "MISSHEIST_JEWEL", "enter_guard")
//	//							PRINTSTRING("anim phase is ") PRINTFLOAT(GET_SYNCHRONIZED_SCENE_PHASE(ShopGuardSyncdScene)) PRINTNL()
//								IF GET_SYNCHRONIZED_SCENE_PHASE(ShopGuardSyncdScene) < 0.45
//									IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-631.621216,-236.748566,36.441540>>, <<-630.985107,-236.279022,39.307037>>, 1.000000)
//										CLEAR_PED_TASKS(shopGuard)
//										STOP_SYNCHRONIZED_ENTITY_ANIM(shopGuard, INSTANT_BLEND_OUT, TRUE)
//										STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(<<-631.96, -236.33, 38.21>>, 2, P_JEWEL_DOOR_L, INSTANT_BLEND_OUT)
//										SyncdSceneDisrupted = TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
						
//						IF SyncdSceneDisrupted = TRUE
//							IF NOT IS_ENTITY_PLAYING_ANIM(shopGuard, "amb@world_human_stand_guard@male@idle_a", "idle_a")
//							AND NOT IS_ENTITY_PLAYING_ANIM(shopGuard, "MISSHEIST_JEWEL", "enter_guard")
//							AND NOT IS_ENTITY_PLAYING_ANIM(shopGuard, "MISSHEIST_JEWEL", "exit_guard")
//							AND NOT IS_ENTITY_PLAYING_ANIM(shopGuard, "MISSHEIST_JEWEL", "base_guard")							
//								//Create the sync'd scene for the sweatshop workers to play their anims.
//								TASK_PLAY_ANIM(shopGuard, "amb@world_human_stand_guard@male@idle_a", "idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_NOT_INTERRUPTABLE)
//								SET_PED_KEEP_TASK(shopGuard, TRUE)
//							ENDIF
//							
//							IF IS_ENTITY_PLAYING_ANIM(shopGuard, "amb@world_human_stand_guard@male@idle_a", "idle_a")
//								SyncdSceneDisrupted = FALSE
//							ENDIF
//						ENDIF
						
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	
	ELSE
	
		//Reset flags
		IF StoreStaffCreated = TRUE
			StoreStaffCreated = FALSE
		ENDIF
		IF modelsRequested = TRUE
			REMOVE_ANIM_DICT("amb@world_human_stand_guard@male@idle_a")
			REMOVE_ANIM_DICT("MISSHEIST_JEWEL_SETUP")
			SET_MODEL_AS_NO_LONGER_NEEDED(U_M_M_JEWELSEC_01)
			SET_MODEL_AS_NO_LONGER_NEEDED(IG_JEWELASS)			
			modelsRequested = FALSE
		ENDIF
		//Delete peds
		IF DOES_ENTITY_EXIST(shopGuard)
			DELETE_PED(shopGuard)
		ENDIF
		IF DOES_ENTITY_EXIST(shopAssistant)
			DELETE_PED(shopAssistant)
		ENDIF
		//Remove interior from memory
		IF interiorPinnedInMemory = TRUE
			UNPIN_INTERIOR(InteriorJewelleryShop)
			interiorPinnedInMemory = FALSE
		ENDIF
	ENDIF

ENDPROC


//PURPOSE: Creates and deletes the workers inside the sweatshop warehouse.
PROC SWEATSHOP_AMBIENT_CONTROLLER()
//	
//	IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<718.1, -975.5, 23.6>>) < 35
//		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<717.4, -964.4, 30>>, <<20,20,20>>)
//			SweatshopPedsRequired = TRUE
//		ELSE
//			SweatshopPedsRequired = FALSE
//		ENDIF
//	ENDIF
	
	//Run a check to see if player is inside the warehouse or not.
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<718.1, -975.5, 23.6>>, <<5,5,5>>)
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<720.109497,-974.879761,23.414156>>, <<716.210571,-974.832031,25.664173>>, 1.000000)
			PlayerInsideWarehouse = TRUE
		ENDIF
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<714.390991,-976.440125,22.644867>>, <<721.805359,-976.489441,24.898579>>, 1.750000)
			PlayerInsideWarehouse = FALSE
		ENDIF	
	ENDIF
	
	IF PlayerInsideWarehouse = TRUE
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), Current_Ped_weapon)
		IF Current_Ped_weapon <> WEAPONTYPE_UNARMED
			SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
		ENDIF
  	ENDIF



//	IF SweatshopPedsRequired = TRUE
//		
//		IF SweatshopPedsCreated = FALSE
//			REQUEST_ANIM_DICT("MISSHEIST_JEWEL_SETUP")
//			REQUEST_MODEL(S_F_Y_SWEATSHOP_01)
//			REQUEST_MODEL(PROP_IRON_01)
//			REQUEST_MODEL(v_ind_ss_chair01)
//			REQUEST_MODEL(v_ind_ss_chair2)
//			REQUEST_MODEL(prop_sewing_machine)
//			
//			//Create the workers
//			IF HAS_MODEL_LOADED(S_F_Y_SWEATSHOP_01)
//				//Ironer
//				IF NOT DOES_ENTITY_EXIST(WorkshopPed[0])
//					WorkshopPed[0] = CREATE_PED(PEDTYPE_MISSION, S_F_Y_SWEATSHOP_01, <<711.6627, -968.0543, 29.3956>>, 260.2629)
//					SET_PED_COMPONENT_VARIATION(WorkshopPed[0], INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
//					SET_PED_COMPONENT_VARIATION(WorkshopPed[0], INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
//					SET_PED_COMPONENT_VARIATION(WorkshopPed[0], INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
//					SET_PED_COMPONENT_VARIATION(WorkshopPed[0], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
//				ENDIF
//				//Sewer 1
//				IF NOT DOES_ENTITY_EXIST(WorkshopPed[1])
//					WorkshopPed[1] = CREATE_PED(PEDTYPE_MISSION, S_F_Y_SWEATSHOP_01, <<714.9799, -967.7026, 29.3956>>, 83.3026)
//					SET_PED_COMPONENT_VARIATION(WorkshopPed[1], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
//					SET_PED_COMPONENT_VARIATION(WorkshopPed[1], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
//					SET_PED_COMPONENT_VARIATION(WorkshopPed[1], INT_TO_ENUM(PED_COMPONENT,3), 1, 3, 0) //(uppr)
//					SET_PED_COMPONENT_VARIATION(WorkshopPed[1], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
//				ENDIF
//				//Sewer 2
//				IF NOT DOES_ENTITY_EXIST(WorkshopPed[2])
//					WorkshopPed[2] = CREATE_PED(PEDTYPE_MISSION, S_F_Y_SWEATSHOP_01, <<716.0836, -962.3854, 29.3956>>, 187.4468)
//					SET_PED_COMPONENT_VARIATION(WorkshopPed[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
//					SET_PED_COMPONENT_VARIATION(WorkshopPed[2], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
//					SET_PED_COMPONENT_VARIATION(WorkshopPed[2], INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
//					SET_PED_COMPONENT_VARIATION(WorkshopPed[2], INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
//				ENDIF
//			ENDIF
//			
//			//Iron prop for Ironer
//			IF HAS_MODEL_LOADED(PROP_IRON_01)
//				IF NOT DOES_ENTITY_EXIST(IronProp)
//					IF DOES_ENTITY_EXIST(WorkshopPed[0])
//						IF NOT IS_PED_INJURED(WorkshopPed[0])
//							IronProp = CREATE_OBJECT(PROP_IRON_01, <<712.2, -967.8, 30.6>>)
////							ATTACH_ENTITY_TO_ENTITY(IronProp, WorkshopPed[0], 26, <<0.09, 0.07, 0.07>>, <<-103.996, 32.6619, 14.2599>>, TRUE, TRUE)
//							ATTACH_ENTITY_TO_ENTITY(IronProp, WorkshopPed[0], GET_PED_BONE_INDEX(WorkshopPed[0], BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>)
//							SET_MODEL_AS_NO_LONGER_NEEDED(PROP_IRON_01)
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF			
//			
//			//Seat 1 under sewer at top of stairs 
//			IF HAS_MODEL_LOADED(v_ind_ss_chair01)
//				IF NOT DOES_ENTITY_EXIST(ChairProp[0])
//					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[2])
//						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.object[2], TRUE, TRUE)
//						ChairProp[0] = g_sTriggerSceneAssets.object[2]
//						SET_MODEL_AS_NO_LONGER_NEEDED(v_ind_ss_chair01)
//					ELSE
//						CREATE_MODEL_HIDE(<<716.2011, -962.5433, 29.363>>, 1, v_ind_ss_chair01, TRUE)
//						ChairProp[0] = CREATE_OBJECT(v_ind_ss_chair01, <<716.2011, -962.5433, 29.363>>)
//						SET_ENTITY_ROTATION(ChairProp[0], <<0.0000, 0.0000, 4.6800>>)
//						SET_ENTITY_QUATERNION(ChairProp[0], 0.0000, 0.0000, 0.0408, 0.9992)	
//						FREEZE_ENTITY_POSITION(ChairProp[0], TRUE)
//						SET_MODEL_AS_NO_LONGER_NEEDED(v_ind_ss_chair01)
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			//Seat 2 under sewer opposite ironing lady
//			IF HAS_MODEL_LOADED(v_ind_ss_chair2)
//				IF NOT DOES_ENTITY_EXIST(ChairProp[1])
//					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[3])
//						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.object[3], TRUE, TRUE)
//						ChairProp[1] = g_sTriggerSceneAssets.object[3]
//						SET_MODEL_AS_NO_LONGER_NEEDED(v_ind_ss_chair2)
//					ELSE
//						CREATE_MODEL_HIDE(<<714.7697, -967.7910, 29.3672>>, 1, v_ind_ss_chair2, TRUE)
//						ChairProp[1] = CREATE_OBJECT(v_ind_ss_chair2, <<714.7697, -967.7910, 29.3672>>)
//						SET_ENTITY_ROTATION(ChairProp[1], <<0.0000, 0.0000, -92.5307>>)
//						SET_ENTITY_QUATERNION(ChairProp[1], -0.0000, 0.0000, 0.7225, -0.6913)
//						FREEZE_ENTITY_POSITION(ChairProp[1], TRUE)
//						SET_MODEL_AS_NO_LONGER_NEEDED(v_ind_ss_chair2)
//					ENDIF
//				ENDIF	
//			ENDIF
//			
//			//Sewing machine 1 under sewer at top of stairs 
//			IF HAS_MODEL_LOADED(prop_sewing_machine)
//				IF NOT DOES_ENTITY_EXIST(SewingProp[0])
//					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[4])
//						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.object[4], TRUE, TRUE)
//						SewingProp[0] = g_sTriggerSceneAssets.object[4]
//					ELSE
//						CREATE_MODEL_HIDE(<<716.0047, -963.1053, 30.5499>>, 1, prop_sewing_machine, TRUE)
//						SewingProp[0] = CREATE_OBJECT(prop_sewing_machine, <<716.0047, -963.1053, 30.3424>>)
//						SET_ENTITY_ROTATION(SewingProp[0], <<0.0000, 0.0000, 180.0000>>)
//						SET_ENTITY_QUATERNION(SewingProp[0], 0.0000, 0.0000, 1.0000, 0.0000)
//						FREEZE_ENTITY_POSITION(SewingProp[0], TRUE)
//					ENDIF
//				ENDIF
//			ENDIF
//			
//			//Sewing machine 2 under sewer opposite ironing lady
//			IF HAS_MODEL_LOADED(prop_sewing_machine)
//				IF NOT DOES_ENTITY_EXIST(SewingProp[1])
//					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[5])
//						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.object[5], TRUE, TRUE)
//						SewingProp[1] = g_sTriggerSceneAssets.object[5]
//					ELSE
//						CREATE_MODEL_HIDE(<<714.2352, -967.4576, 30.3425>>, 1, prop_sewing_machine, TRUE)
//						SewingProp[1] = CREATE_OBJECT(prop_sewing_machine, <<714.2352, -967.4576, 30.135>>)
//						SET_ENTITY_ROTATION(SewingProp[1], <<0.0000, 0.0000, 90.0000>>)
//						SET_ENTITY_QUATERNION(SewingProp[1], 0.0000, 0.0000, 0.7071, 0.7071)
//						FREEZE_ENTITY_POSITION(SewingProp[1], TRUE)
//					ENDIF
//				ENDIF
//			ENDIF			
//			
//			//Start sync'd scene once all entities have been created
//			IF DOES_ENTITY_EXIST(ChairProp[0])
//			AND DOES_ENTITY_EXIST(ChairProp[1])
//			AND DOES_ENTITY_EXIST(IronProp)
//			AND DOES_ENTITY_EXIST(WorkshopPed[0])
//			AND DOES_ENTITY_EXIST(WorkshopPed[1])
//			AND DOES_ENTITY_EXIST(WorkshopPed[2])
//			AND DOES_ENTITY_EXIST(SewingProp[0])
//			AND DOES_ENTITY_EXIST(SewingProp[1])
//			
//				//Create the sync'd scene for the sweatshop workers to play their anims.
//				SyncdScene1 = CREATE_SYNCHRONIZED_SCENE(<< 710.150, -964.900, 29.400 >>, << 0, 0.000, 0.000 >>)
//				
//				//Start anims for sync'd scene on the 3 sewers.
//				IF HAS_ANIM_DICT_LOADED("MISSHEIST_JEWEL_SETUP")
//					IF NOT IS_PED_INJURED(WorkshopPed[0])
//						CLEAR_PED_TASKS(WorkshopPed[0])
//						TASK_SYNCHRONIZED_SCENE(WorkshopPed[0], SyncdScene1, "MISSHEIST_JEWEL_SETUP", "Ironing_Idle_Character_A", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE|SYNCED_SCENE_USE_PHYSICS)
//						SET_PED_KEEP_TASK(WorkshopPed[0], TRUE)
//					ENDIF	
//					IF NOT IS_PED_INJURED(WorkshopPed[1])
//						CLEAR_PED_TASKS(WorkshopPed[1])
//						TASK_SYNCHRONIZED_SCENE(WorkshopPed[1], SyncdScene1, "MISSHEIST_JEWEL_SETUP", "Sewing_Idle_01_Character_B", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE|SYNCED_SCENE_USE_PHYSICS)
//						SET_PED_KEEP_TASK(WorkshopPed[1], TRUE)
//					ENDIF	
//					IF NOT IS_PED_INJURED(WorkshopPed[2])
//						CLEAR_PED_TASKS(WorkshopPed[2])
//						TASK_SYNCHRONIZED_SCENE(WorkshopPed[2], SyncdScene1, "MISSHEIST_JEWEL_SETUP", "Sewing_Idle_02_Character_C", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_LOOP_WITHIN_SCENE|SYNCED_SCENE_USE_PHYSICS)
//						SET_PED_KEEP_TASK(WorkshopPed[2], TRUE)
//					ENDIF	
//					
//					SET_SYNCHRONIZED_SCENE_LOOPED(SyncdScene1, TRUE)
//					
//					SET_MODEL_AS_NO_LONGER_NEEDED(S_F_Y_SWEATSHOP_01)
//					SET_MODEL_AS_NO_LONGER_NEEDED(v_ind_ss_chair2)
//					SET_MODEL_AS_NO_LONGER_NEEDED(v_ind_ss_chair01)
//					SET_MODEL_AS_NO_LONGER_NEEDED(PROP_IRON_01)
//					SET_MODEL_AS_NO_LONGER_NEEDED(prop_sewing_machine)
//					REMOVE_ANIM_DICT("MISSHEIST_JEWEL_SETUP")
//					
//					SweatshopPedsCreated = TRUE
//				ENDIF
//			ENDIF
//			
//		ENDIF
//	
//	ELSE
//		
//		//Delete everything to save memory when player leaves the area.
//		IF SweatshopPedsCreated = TRUE
//			
//			//Delete all peds and objects
//			IF DOES_ENTITY_EXIST(IronProp)
//				DELETE_OBJECT(IronProp)
//			ENDIF			
//			IF DOES_ENTITY_EXIST(ChairProp[0])
//				DELETE_OBJECT(ChairProp[0])
//			ENDIF	
//			IF DOES_ENTITY_EXIST(ChairProp[1])
//				DELETE_OBJECT(ChairProp[1])
//			ENDIF		
//			IF DOES_ENTITY_EXIST(SewingProp[0])
//				DELETE_OBJECT(SewingProp[0])
//			ENDIF	
//			IF DOES_ENTITY_EXIST(SewingProp[1])
//				DELETE_OBJECT(SewingProp[1])
//			ENDIF	
//			
//			IF DOES_ENTITY_EXIST(WorkshopPed[0])
//				DELETE_PED(WorkshopPed[0])
//			ENDIF
//			IF DOES_ENTITY_EXIST(WorkshopPed[1])
//				DELETE_PED(WorkshopPed[1])
//			ENDIF
//			IF DOES_ENTITY_EXIST(WorkshopPed[2])
//				DELETE_PED(WorkshopPed[2])
//			ENDIF
////			//Delete Guadaloupe now for good.
////			IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[4])
////				SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[4], TRUE, TRUE)
////				GuadaloupePed = g_sTriggerSceneAssets.ped[4]
////				CLEAR_PED_TASKS(GuadaloupePed)
////				DELETE_PED(GuadaloupePed)
////			ENDIF
//			
//			SET_MODEL_AS_NO_LONGER_NEEDED(S_F_Y_SWEATSHOP_01)
//			SET_MODEL_AS_NO_LONGER_NEEDED(v_ind_ss_chair2)
//			SET_MODEL_AS_NO_LONGER_NEEDED(v_ind_ss_chair01)
//			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_IRON_01)
//			SET_MODEL_AS_NO_LONGER_NEEDED(prop_sewing_machine)
//			REMOVE_ANIM_DICT("MISSHEIST_JEWEL_SETUP")
//					
//			SweatshopPedsCreated = FALSE
//			
//		ENDIF
//	
//	ENDIF
	
	
	
ENDPROC


//PURPOSE: Handles dialogue if player enter's mod shop
PROC CHECK_FOR_MOD_SHOP()

	//Pause the chat on route to the jewellers if player goes into mod shop
	IF missionStage = STAGE_DRIVE_TO_STORE
		IF DoneCarChat = TRUE
		AND doneGlassChat = FALSE
			IF PausedSTP12cChat = FALSE	
				IF IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						STP12c_StartLine = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						CLEAR_PRINTS()
						PausedSTP12cChat = TRUE
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct, "JHS1AUD", "JHS1_STP12c", STP12c_StartLine, CONV_PRIORITY_MEDIUM)
							PausedSTP12cChat = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
					
	//Pause the chat on way back to warehouse if player goes into mod shop
	IF missionStage = STAGE_DRIVE_TO_WAREHOUSE
		IF iControlFlag > 1
			IF PausedSTP13Chat = FALSE	
				IF IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						STP13_StartLine = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						CLEAR_PRINTS()
						PausedSTP13Chat = TRUE
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct, "JHS1AUD", "JHS1_STP13", STP13_StartLine, CONV_PRIORITY_MEDIUM)
							PausedSTP13Chat = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF					
ENDPROC

//PURPOSE:Handles Lester dialogue about the cops if player has a wanted level and is in the car with Lester
PROC LESTER_WANTED_CHAT()
	//Do some dialogue from Lester if player is in the car with Lester
	IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
		IF doneWantedChat = TRUE
			doneWantedChat = FALSE
		ENDIF
	ELSE
		IF doneWantedChat = FALSE
			IF DOES_ENTITY_EXIST(vehCar)
				IF IS_VEHICLE_DRIVEABLE(vehCar)
					IF DOES_ENTITY_EXIST(pedContact)
						IF NOT IS_PED_INJURED(pedContact)
							IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_MESSAGE_BEING_DISPLAYED()
									OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
										ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, pedContact, "LESTER")
										IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_WANT", CONV_PRIORITY_MEDIUM)
											//Maybe bringing the authorities along is not such a good idea.
											//I'm thinking we should ditch the police escort.
											//Cops while we case the joint? Not so smart.
											doneWantedChat = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
	ENDIF

ENDPROC
//═══════════════════════════════════════════╡ MISSION STAGES ╞═════════════════════════════════════════════

//Beginning of mission will start with a short cutscene of Player and Lester looking at the planning board.
PROC DO_STAGE_OPENING_CUTSCENE()
	
//	REQUEST_WAYPOINT_RECORDING("H3SET1_BUDDY6")
//	WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1_BUDDY6")
//		WAIT(0)
//	ENDWHILE
	
	IF iControlFlag = 0
		
		//Reset Flags
		cutsceneSkipped 			= FALSE
		hasStartAreaBeenCleanedUp 	= FALSE
		syncSceneStarted 			= FALSE
		ScreenFadedIn 				= FALSE
//		timeOfDayChanged 			= FALSE	
		
		REQUEST_ANIM_DICT("MISSHEIST_JEWEL_SETUP")
		
		//Set flags
		iCutsceneStage = 0
		iControlFlag = 1
		
	ENDIF
	
	IF iControlFlag = 1
	
		SWITCH iCutsceneStage
		
			CASE 0 	
				
				//Do a load scene now to help load in the interior. Mainly for if a repeat play is used.
//				IF IS_SCREEN_FADED_OUT()
//					LOAD_SCENE(<<713.1, -963.4, 29.9>>)
//				ENDIF
				
				IF HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
					//Create Michaels car
					IF NOT DOES_ENTITY_EXIST(vehCar)
						WHILE NOT CREATE_PLAYER_VEHICLE(vehCar, CHAR_MICHAEL, vCarSpawnCoords, fCarSpawnHeading)
							PRINTSTRING("Stuck waiting for players car to be created..") PRINTNL()
							WAIT(0)
						ENDWHILE
						SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER, TRUE)
						ADD_VEHICLE_UPSIDEDOWN_CHECK(vehCar)
						SET_VEHICLE_CAN_BREAK(vehCar, FALSE)
						SET_VEHICLE_HAS_STRONG_AXLES(vehCar, TRUE)
						//Stat for how much damage the players car takes.
						INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehCar)
						//Stat for what speed the player gets their car up to.
						INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehCar)
					ENDIF
					
//					WHILE NOT HAS_CUTSCENE_LOADED()
//						PRINTSTRING("Stuck waiting for cutscene to load..") PRINTNL()
//						WAIT(0)
//					ENDWHILE
					
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					//Register entities for cutscene
					//Lester				
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
					    SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
						pedContact = g_sTriggerSceneAssets.ped[0]
						SET_PED_CAN_BE_DRAGGED_OUT(pedContact, FALSE)
						SET_PED_CAN_BE_TARGETTED(pedContact, FALSE)
						SET_PED_RELATIONSHIP_GROUP_HASH(pedContact, RELGROUPHASH_PLAYER)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedContact, TRUE)
						SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedContact, FALSE)
						SET_PED_CONFIG_FLAG(pedContact, PCF_LawWillOnlyAttackIfPlayerIsWanted, TRUE)
					ENDIF
					//Register Lester
					IF DOES_ENTITY_EXIST(pedContact)
						IF NOT IS_PED_INJURED(pedContact)
							REGISTER_ENTITY_FOR_CUTSCENE(pedContact, "Lester", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
					ENDIF
					
					//Create Lester incase mission is being replayed after already been played as he wont exist as the trigger scene is skipped.
					IF NOT DOES_ENTITY_EXIST(pedContact)
						REQUEST_NPC_PED_MODEL(CHAR_LESTER)
						WHILE NOT HAS_NPC_PED_MODEL_LOADED(CHAR_LESTER)
							PRINTSTRING("Waiting on NPC Lester loading") PRINTNL()
							WAIT(0)
						ENDWHILE
						
						CREATE_NPC_PED_ON_FOOT(pedContact, CHAR_LESTER, <<718.6170, -964.8593, 29.3956>>, 120.4053)
						SET_MODEL_AS_NO_LONGER_NEEDED(GET_NPC_PED_MODEL(CHAR_LESTER))
						SET_PED_COMPONENT_VARIATION(pedContact, PED_COMP_TORSO, 0, 0)
						SET_PED_COMPONENT_VARIATION(pedContact, PED_COMP_LEG, 1, 0)
						SET_PED_PROP_INDEX(pedContact, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)	
						REGISTER_ENTITY_FOR_CUTSCENE(pedContact, "Lester", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedContact, TRUE)
						SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedContact, FALSE)
						SET_PED_CAN_BE_DRAGGED_OUT(pedContact, FALSE)
						SET_PED_CONFIG_FLAG(pedContact, PCF_LawWillOnlyAttackIfPlayerIsWanted, TRUE)
					ENDIF
					
					IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.object[0], TRUE, TRUE)
						walkingStick = g_sTriggerSceneAssets.object[0]
					ENDIF
					
	//				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
	//					IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[1])
	//						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[1], TRUE, TRUE)
	//						WorkshopPed[0] = g_sTriggerSceneAssets.ped[1]
	////						REGISTER_ENTITY_FOR_CUTSCENE(WorkshopPed[0], "worker_ironing", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)	
	//					ENDIF
	//				ENDIF
	//
	//				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])
	//					IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[2])
	//						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[2], TRUE, TRUE)
	//						WorkshopPed[1] = g_sTriggerSceneAssets.ped[2]
	////						REGISTER_ENTITY_FOR_CUTSCENE(WorkshopPed[1], "worker_sewing_nearstairs", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)	
	//					ENDIF
	//				ENDIF
					
	//				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])
	//					IF NOT IS_PED_INJURED(g_sTriggerSceneAssets.ped[3])
	//						SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[3], TRUE, TRUE)
	//						WorkshopPed[2] = g_sTriggerSceneAssets.ped[3]
	////						REGISTER_ENTITY_FOR_CUTSCENE(WorkshopPed[2], "worker_sewing_nearoffice", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)	
	//					ENDIF
	//				ENDIF					
					
					//Register players car
					IF DOES_ENTITY_EXIST(vehCar)
						IF IS_VEHICLE_DRIVEABLE(vehCar)
							REGISTER_ENTITY_FOR_CUTSCENE(vehCar, "Michaels_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
					ENDIF
					
	//				//Delete the iron from trigger scene
	//				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[1])
	//					SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.object[1], TRUE, TRUE)
	//					DELETE_OBJECT(g_sTriggerSceneAssets.object[1])
	//				ENDIF
					
	//				IF DOES_ENTITY_EXIST(walkingStick)
	//					REGISTER_ENTITY_FOR_CUTSCENE(walkingStick, "WalkingStick_Lester", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
	//				ENDIF
					
					MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_JEWELRY_1)				
					
					//Stop the new load scene now
					IF IS_REPEAT_PLAY_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					ENDIF
					
	//				//Kill the timelapse camera
	//				SET_TODS_CUTSCENE_RUNNING(FALSE)
					
			      	//Configure script systems into a safe mode.
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
					
					//Set peds up for dialogue
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, pedContact, "LESTER")				
					
					START_CUTSCENE()
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
						
	//				CREATE_MODEL_HIDE(<<709.98, -963.53, 30.55>>, 3, v_ilev_ss_door02, TRUE)
					
					SETTIMERA(0)
	//				SET_SEAMLESS_CUTS_ACTIVE(bTracker)				
									
					iCutsceneStage ++
				ENDIF
				
			BREAK
			
			CASE 1						
					
				#IF IS_DEBUG_BUILD
					IF IS_KEYBOARD_KEY_PRESSED(KEY_J)
						IF NOT HAS_CUTSCENE_FINISHED()
							STOP_CUTSCENE()
						ENDIF
					ENDIF
				#ENDIF		
				
				IF IS_CUTSCENE_PLAYING()
					//Make sure to fade in now that the cutscene has started
					IF ScreenFadedIn = FALSE
						IF NOT IS_SCREEN_FADED_IN()
							DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
							ScreenFadedIn = TRUE
						ENDIF
						IF IS_SCREEN_FADED_IN()
							ScreenFadedIn = TRUE
						ENDIF
					ENDIF
					IF GET_CUTSCENE_TIME() > 75000
						DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
					ELSE
						IF GET_CUTSCENE_TIME() > CUTSCENE_SKIP_DELAY
							IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
								cutsceneSkipped = TRUE
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(walkingStick)
					DELETE_OBJECT(walkingStick)
				ENDIF
				
				//clear start area for cutscene as soon as it starts.
				IF IS_CUTSCENE_ACTIVE()
					
					IF hasStartAreaBeenCleanedUp = FALSE
						CLEAR_AREA(<<717, -978, 24>>, 100, TRUE, TRUE)
						
						//Move vehicles to a safe area		
						IF DOES_ENTITY_EXIST(playersStartCar)
							IF IS_VEHICLE_DRIVEABLE(playersStartCar)
								IF GET_ENTITY_MODEL(playersStartCar) != TAILGATER
									IF IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(playersStartCar))
										SET_ENTITY_COORDS(playersStartCar, <<686.8995, -985.1403, 22.3440>>)
										SET_ENTITY_HEADING(playersStartCar, 268.5802)	
									ELSE
										IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(playersStartCar))
										OR IS_THIS_MODEL_A_BIKE(GET_ENTITY_MODEL(playersStartCar))
										OR IS_THIS_MODEL_A_QUADBIKE(GET_ENTITY_MODEL(playersStartCar))
											SET_ENTITY_COORDS(playersStartCar, <<706.0064, -979.6544, 23.1403>>)
											SET_ENTITY_HEADING(playersStartCar, 227.0058)
										ELSE
											DELETE_VEHICLE(playersStartCar)
										ENDIF
									ENDIF
									SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<0.0,0.0,0.0>>, 0.0)
								ELSE
									DELETE_VEHICLE(playersStartCar)
								ENDIF
							ENDIF
						ENDIF	
						hasStartAreaBeenCleanedUp = TRUE
					ENDIF
				ENDIF			
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lester")
				AND CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_car")
					IF IS_VEHICLE_DRIVEABLE(vehCar)
						IF NOT IS_PED_INJURED(pedContact)
							IF cutsceneSkipped = TRUE
							AND IS_SCREEN_FADED_OUT()
								IF NOT IS_PED_IN_VEHICLE(pedContact, vehCar)
									SET_PED_INTO_VEHICLE(pedContact, vehCar, VS_FRONT_RIGHT)
								ENDIF								
							ELSE
//								TASK_ENTER_VEHICLE(pedContact, vehCar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
								IF HAS_ANIM_DICT_LOADED("MISSHEIST_JEWEL_SETUP")
									IF syncSceneStarted = FALSE
//										LesterGetInCarSyncdScene = CREATE_SYNCHRONIZED_SCENE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehCar, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehcar, GET_WORLD_POSITION_OF_ENTITY_BONE(vehCar, GET_ENTITY_BONE_INDEX_BY_NAME(vehCar, "seat_pside_f")))), <<0,0,-90>>)
										LesterGetInCarSyncdScene = CREATE_SYNCHRONIZED_SCENE(<<0,0,0>>, <<0,0,0>>)
										ATTACH_SYNCHRONIZED_SCENE_TO_ENTITY(LesterGetInCarSyncdScene, vehCar, 0)	
										TASK_SYNCHRONIZED_SCENE(pedContact, LesterGetInCarSyncdScene, "MISSHEIST_JEWEL_SETUP", "LESTER_GET_IN_CAR", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_DONT_INTERRUPT)
//										TASK_VEHICLE_PLAY_ANIM(vehCar, "MISSHEIST_JEWEL_SETUP", "lester_get_in_car_cardoor")
//										PLAY_ENTITY_ANIM(vehCar, "lester_get_in_car_cardoor", "MISSHEIST_JEWEL_SETUP", NORMAL_BLEND_IN, FALSE, FALSE, FALSE, 0, ENUM_TO_INT(AF_USE_KINEMATIC_PHYSICS))
										PLAY_SYNCHRONIZED_ENTITY_ANIM(vehCar, LesterGetInCarSyncdScene, "lester_get_in_car_cardoor", "MISSHEIST_JEWEL_SETUP", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, ENUM_TO_INT(SYNCED_SCENE_USE_PHYSICS)| ENUM_TO_INT(SYNCED_SCENE_BLOCK_MOVER_UPDATE)| ENUM_TO_INT(SYNCED_SCENE_VEHICLE_ALLOW_PLAYER_ENTRY))
										syncSceneStarted = TRUE
									ENDIF
//									ATTACH_ENTITY_TO_ENTITY(pedContact, vehCar, GET_ENTITY_BONE_INDEX_BY_NAME(vehCar, "seat_pside_f"), <<0,0,0>>, <<0,0,0>>)
//									IF NOT IS_ENTITY_PLAYING_ANIM(pedContact, "MISSHEIST_JEWEL_SETUP", "LESTER_GET_IN_CAR")
//										TASK_PLAY_ANIM(pedContact, "MISSHEIST_JEWEL_SETUP", "LESTER_GET_IN_CAR")
//									ENDIF
//									IF NOT IS_ENTITY_PLAYING_ANIM(vehCar, "MISSHEIST_JEWEL_SETUP", "lester_get_in_car_cardoor")
//										TASK_VEHICLE_PLAY_ANIM(vehCar, "MISSHEIST_JEWEL_SETUP", "lester_get_in_car_cardoor")
//									ENDIF
								ENDIF
//								TASK_ENTER_VEHICLE(pedContact, vehCar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
							ENDIF
//							IF NOT IS_PED_IN_VEHICLE(pedContact, vehCar)
//								SET_PED_INTO_VEHICLE(pedContact, vehCar, VS_FRONT_RIGHT)
//							ENDIF
//							SET_ENTITY_COORDS(pedContact, <<716.0284, -983.0331, 23.1286>>)
//							SET_ENTITY_HEADING(pedContact, 75.7852)
//							TASK_ENTER_VEHICLE(pedContact, vehCar, DEFAULT_TIME_BEFORE_WARP, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK)
						ENDIF
					ENDIF
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael", PLAYER_ZERO)
//					IF IS_VEHICLE_DRIVEABLE(vehCar)
//						TASK_ENTER_VEHICLE(PLAYER_PED_ID(), vehCar, DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVEBLENDRATIO_WALK, ECF_USE_LEFT_ENTRY)
//					ENDIF
//					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2500) 
//                    FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					IF cutsceneSkipped = TRUE
					AND IS_SCREEN_FADED_OUT()
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<717.2747, -978.3644, 23.1189>>)
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 185.8557)	
					ENDIF
//					IF cutsceneSkipped = FALSE
//						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<717.3191, -978.5477, 23.1180>>)
//						SET_ENTITY_HEADING(PLAYER_PED_ID(), 188.7111)
//					ENDIF
				
					REPLAY_STOP_EVENT()
					
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 185.8557)
						
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-15.00)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						
						IF cutsceneSkipped = FALSE
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	//for some reason player control needs to be restored for the pitch command to work?
						ENDIF
						
					ELSE
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					ENDIF
				ENDIF
				
				IF NOT CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_car", PLAYER_ZERO)
					IF DOES_ENTITY_EXIST(vehCar)
						IF IS_VEHICLE_DRIVEABLE(vehCar)
							SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehCar, TRUE, TRUE, TRUE)
						ENDIF
					ENDIF
				ENDIF
				
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_car", PLAYER_ZERO)
//					IF DOES_ENTITY_EXIST(vehCar)
//						IF IS_VEHICLE_DRIVEABLE(vehCar)
//							SET_ENTITY_COORDS(vehCar, <<717.6990, -982.7634, 23.1318>>)
//							SET_ENTITY_HEADING(vehCar, 273.8153)
//						ENDIF
//					ENDIF
//				ENDIF
//				IF cutsceneSkipped = TRUE
//					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael", PLAYER_ZERO)
//						IF IS_VEHICLE_DRIVEABLE(vehCar)
//							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//								IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
//									SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
//								ENDIF
//							ENDIF
//						ENDIF
//						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//					ENDIF
//				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
					
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 185.8557)
					
						SET_GAMEPLAY_CAM_RELATIVE_PITCH(-15.00)
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
						
						//ANIMPOSTFX_PLAY("SwitchSceneMichael", 0, FALSE)
					ELSE
						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					ENDIF
				ENDIF
				
				IF HAS_CUTSCENE_FINISHED()
					//Return script systems to normal.
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)					
					
					//Stop the doors anim from before the lead in scene.
					IF NOT IS_REPEAT_PLAY_ACTIVE()
						STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(<<709.9813, -963.5311, 30.5453>>, 2, v_ilev_ss_door02, INSTANT_BLEND_OUT)
					ENDIF
					
					IF cutsceneSkipped = TRUE
					AND IS_SCREEN_FADED_OUT()

						IF NOT DOES_ENTITY_EXIST(vehCar)
							WHILE NOT HAS_MODEL_LOADED(TAILGATER)
								WAIT(0)
							ENDWHILE
							CREATE_PLAYER_VEHICLE(vehCar, CHAR_MICHAEL, vCarSpawnCoords, fCarSpawnHeading)
							SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER, TRUE)
							ADD_VEHICLE_UPSIDEDOWN_CHECK(vehCar)
							SET_VEHICLE_CAN_BREAK(vehCar, FALSE)
							SET_VEHICLE_HAS_STRONG_AXLES(vehCar, TRUE)
							//Stat for how much damage the players car takes.
							INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehCar)
							//Stat for what speed the player gets their car up to.
							INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehCar)
						ENDIF					
	
						IF IS_VEHICLE_DRIVEABLE(vehCar)
							IF NOT IS_PED_INJURED(pedContact)
								SET_PED_CAN_BE_DRAGGED_OUT(pedContact, FALSE)
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedContact, TRUE)
								SET_PED_CAN_BE_TARGETTED(pedContact, FALSE)
								ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, pedContact, "LESTER")
								IF NOT IS_PED_IN_VEHICLE(pedContact, vehCar)
									SET_PED_INTO_VEHICLE(pedContact, vehCar, VS_FRONT_RIGHT)
								ENDIF
							ENDIF
						ENDIF	
						
//						IF IS_VEHICLE_DRIVEABLE(vehCar)
//							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//								IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
//									SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
//								ENDIF
//							ENDIF
//						ENDIF
//						PRINTSTRING("cutscneneSkipped = TRUE") PRINTNL()
//						SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)

					ELSE
//						PRINTSTRING("cutscneneSkipped = FALSE") PRINTNL()
					ENDIF
					
					IF cutsceneSkipped = TRUE
						IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
						
							SET_ENTITY_COORDS(PLAYER_PED_ID(), <<717.2747, -978.3644, 23.1189>>)
							SET_ENTITY_HEADING(PLAYER_PED_ID(), 185.8557)	
							
							SET_GAMEPLAY_CAM_RELATIVE_PITCH(-15.00)
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)

						ENDIF
					ENDIF
					
					iCutsceneStage++
				ENDIF	
				
			BREAK
			
			CASE 2			
				
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				iCutsceneStage++
			
			BREAK
			
			CASE 3
				
//				REMOVE_MODEL_HIDE(<<709.98, -963.53, 30.55>>, 3, v_ilev_ss_door02)
				iControlFlag = 0
				missionStage = STAGE_DRIVE_TO_STORE
				iCutsceneStage ++
			
			BREAK
		
		ENDSWITCH
	
	ENDIF
				
ENDPROC


//Stage where the player has to drive Lester to the Jewellery Store.
PROC DO_STAGE_DRIVE_TO_STORE()
	
//	IF IS_SYNCHRONIZED_SCENE_RUNNING(LesterGetInCarSyncdScene)
//		IF GET_SYNCHRONIZED_SCENE_PHASE(LesterGetInCarSyncdScene) > 0.99
//			IF DOES_ENTITY_EXIST(vehCar)
//			AND DOES_ENTITY_EXIST(pedContact)
//				STOP_SYNCHRONIZED_ENTITY_ANIM(vehCar, INSTANT_BLEND_OUT, TRUE)
//				STOP_SYNCHRONIZED_ENTITY_ANIM(pedContact, INSTANT_BLEND_OUT, TRUE)
//				SET_VEHICLE_DOOR_SHUT(vehCar, SC_DOOR_FRONT_RIGHT)
//			ENDIF
//		ENDIF
//	ENDIF
	
	SWEATSHOP_AMBIENT_CONTROLLER()
	
	JEWEL_STORE_STAFF_CONTROLLER()
	
	IF iControlFlag = 0
		
		IF MissionStageBeingSkippedTo = TRUE
			STAGE_SELECTOR_MISSION_SETUP()
			MissionStageBeingSkippedTo = FALSE
		ENDIF			
		
		//═════════════════════════╡ SET FLAGS ╞═══════════════════════════	
		GetInCarText 				= FALSE
		doneGetInCarText 			= FALSE
		GoToJewellersTextDisplayed 	= FALSE
		DoneCarChat 				= FALSE
		doneGlassChat 				= FALSE
		GotoCarTextDone				= FALSE
		clearedGodText 				= FALSE
		doneWarningText 			= FALSE
//		CarStopped					= FALSE
		PlayerEnteredShopTooSoon	= FALSE
		CanMissionFail				= TRUE
		StoreStaffCreated 			= FALSE
//		SweatshopPedsCreated 		= FALSE
		musicStarted[0] 			= FALSE
		modelsRequested 			= FALSE
		PlayerInsideWarehouse 		= FALSE
		interiorPinnedInMemory 		= FALSE
		doneReturnText 				= FALSE
		PausedSTP12cChat	 		= FALSE	
		PausedSTP13Chat				= FALSE	
//		SyncdSceneDisrupted 		= FALSE	
		
		//═════════╡ SETUP ╞═════════	
		
		REMOVE_CUTSCENE()
		
		//DEBUG
		#IF IS_DEBUG_BUILD
			jSkipReady = TRUE
			PSkipReady = TRUE
		#ENDIF						
		
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-650.4, -286.5, 34.1>>, <<-670.4, -251.7, 37.5>>, FALSE)
		CLEAR_AREA(<<-662.3000, -265.4466, 34.9446>>, 15, TRUE)
			
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			END_REPLAY_SETUP(vehCar)
		ENDIF
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		
		//Blips
		IF DOES_ENTITY_EXIST(vehCar)
			IF IS_VEHICLE_DRIVEABLE(vehCar)
				IF NOT DOES_BLIP_EXIST(BlipVeh)
					BlipVeh = CREATE_BLIP_FOR_VEHICLE(vehCar)
				ENDIF
			ENDIF
		ENDIF				
			
		IF NOT IS_PED_INJURED(pedContact)
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, pedContact, "LESTER")
		ENDIF
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")					
		
		REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
		
		iControlFlag = 1
		
	ENDIF
	
	IF iControlFlag = 1			
		
		//Sort out car and Lester for once their sync'd scene is finished
		IF DOES_ENTITY_EXIST(vehCar)
			IF IS_VEHICLE_DRIVEABLE(vehCar)
				IF IS_ENTITY_PLAYING_ANIM(vehCar, "MISSHEIST_JEWEL_SETUP", "lester_get_in_car_cardoor")
					IF IS_SYNCHRONIZED_SCENE_RUNNING(LesterGetInCarSyncdScene)
						IF GET_SYNCHRONIZED_SCENE_PHASE(LesterGetInCarSyncdScene) = 1
							STOP_SYNCHRONIZED_ENTITY_ANIM(vehCar, NORMAL_BLEND_OUT, TRUE)
							SET_VEHICLE_DOOR_SHUT(vehCar, SC_DOOR_FRONT_RIGHT, TRUE)
							PRINTSTRING("stopping syncd scene anim on the car now.") PRINTNL()
						ENDIF
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(pedContact)
					IF NOT IS_PED_INJURED(pedContact)
						IF IS_ENTITY_PLAYING_ANIM(pedContact, "MISSHEIST_JEWEL_SETUP", "LESTER_GET_IN_CAR")
							IF IS_SYNCHRONIZED_SCENE_RUNNING(LesterGetInCarSyncdScene)
								IF GET_SYNCHRONIZED_SCENE_PHASE(LesterGetInCarSyncdScene) = 1
									STOP_SYNCHRONIZED_ENTITY_ANIM(pedContact, NORMAL_BLEND_OUT, TRUE)
									IF NOT IS_PED_IN_VEHICLE(pedContact, vehCar)
										SET_PED_INTO_VEHICLE(pedContact, vehCar, VS_FRONT_RIGHT)
									ENDIF
									PRINTSTRING("stopping syncd scene and putting Lester into car.") PRINTNL()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF PlayerEnteredShopTooSoon = TRUE
			SetMissionFailed("FAIL_SHOP4") //~r~You left Lester behind.
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehCar)		
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
				playerhasbeenincar = TRUE
				
				IF NOT IS_AUDIO_SCENE_ACTIVE("JSH_1_DRIVE_TO_STORE")
					START_AUDIO_SCENE("JSH_1_DRIVE_TO_STORE")
				ENDIF
				
				//Start the mission music
				IF musicStarted[0] = FALSE
					TRIGGER_MUSIC_EVENT("JH1_START")
					musicStarted[0] = TRUE
				ENDIF
				
				IF DOES_BLIP_EXIST(BlipVeh)
					REMOVE_BLIP(BlipVeh)
				ENDIF
				
				IF playerhasbeenincar = TRUE
					IF GotoCarTextDone = TRUE
						IF clearedGodText = FALSE
							CLEAR_PRINTS()	
							clearedGodText = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(pedContact)
					IF IS_PED_IN_VEHICLE(pedContact, vehCar)
						
						IF IS_PED_GROUP_MEMBER(pedContact, PLAYER_GROUP_ID())
							REMOVE_PED_FROM_GROUP(pedContact)
						ENDIF
						
						//BLIPS
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							IF NOT DOES_BLIP_EXIST(blipLocate)
								blipLocate = CREATE_BLIP_FOR_COORD(vParkingSpace, TRUE)
							ENDIF	
						ELSE
							IF DOES_BLIP_EXIST(blipLocate)
								CLEAR_PRINTS()	
								REMOVE_BLIP(blipLocate)
								PRINT_NOW("WANTED", DEFAULT_GOD_TEXT_TIME, 1)//~s~Lose your wanted level.
							ENDIF	
						ENDIF
						
						IF DOES_BLIP_EXIST(blipContact)
							REMOVE_BLIP(blipContact)
						ENDIF						
						
						//CHAT
						//opening line in dialogue
						IF doneGetInCarText = FALSE
							CLEAR_PRINTS()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_STP1", CONV_PRIORITY_MEDIUM)//Let's take a drive. Head to the jewellers in Rockford.
								WAIT(0)
								SETTIMERA(0)
								doneGetInCarText = TRUE		
							ENDIF
						ENDIF
						
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
							IF doneGetInCarText = TRUE	
								IF TIMERA() > 500
									IF NOT IS_MESSAGE_BEING_DISPLAYED()
										IF GoToJewellersTextDisplayed = FALSE	
											
											REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
											
											PRINT_NOW("ENTER_STORE", DEFAULT_GOD_TEXT_TIME, 1)//~s~Go to ~y~Rockford Hills.
											GoToJewellersTextDisplayed = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF GoToJewellersTextDisplayed = TRUE
							IF doneGetInCarText = TRUE
								IF DoneCarChat = FALSE
									IF TIMERA() > 7000
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
											CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_STP12c", CONV_PRIORITY_MEDIUM)//Your FIB buddies know you're back in business....
											WAIT(0)
											DoneCarChat = TRUE
										ENDIF
									ENDIF
								ELSE
									IF doneGlassChat = FALSE
									AND PausedSTP12cChat = FALSE
										IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
											IF NOT IS_MESSAGE_BEING_DISPLAYED()
											OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
												CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_GLASS", CONV_PRIORITY_MEDIUM)//Look, we can talk about this another time. Take these glasses....
												doneGlassChat = TRUE
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF DoneCarChat = TRUE
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
								IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
									PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
								ENDIF
							ENDIF
						ENDIF
						
						IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0	
							IF NOT IS_PED_JUMPING_OUT_OF_VEHICLE(PLAYER_PED_ID())
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<vParkingSpace.x, vParkingSpace.y, 34.97223>>, <<7, 7, 4>>)
									IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-661.279968,-267.138245,34.393761>>, <<-656.862854,-274.783630,37.700790>>, 5.750000)
										IF DOES_BLIP_EXIST(blipLocate)
											REMOVE_BLIP(blipLocate)
										ENDIF
										KILL_ANY_CONVERSATION()
										CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_THERE", CONV_PRIORITY_MEDIUM)
										//Here we are.
										//Break a leg. 
										
										REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
										
										iControlFlag = 2 
									ENDIF
								ENDIF
							ENDIF
							IF DOES_BLIP_EXIST(blipLocate)
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<vParkingSpace.x, vParkingSpace.y, 34.97223>>, <<2.5,2.5,LOCATE_SIZE_HEIGHT>>, TRUE)
								ENDIF
							ENDIF
						ENDIF
						
					ELSE
						
						IF GoToJewellersTextDisplayed = TRUE
							IF NOT IS_PED_GROUP_MEMBER(pedContact, PLAYER_GROUP_ID())
								SET_PED_AS_GROUP_MEMBER(pedContact, PLAYER_GROUP_ID())
							ENDIF
						
							IF NOT DOES_BLIP_EXIST(blipContact)
								blipContact = CREATE_BLIP_FOR_PED(pedContact)
								CLEAR_PRINTS()
								PRINT_NOW("WAIT_PED", DEFAULT_GOD_TEXT_TIME, 1)//~s~Wait for ~b~Lester.
							ENDIF
						
							IF DOES_BLIP_EXIST(blipLocate)
								REMOVE_BLIP(blipLocate)
							ENDIF
							
							IF DoneCarChat = TRUE
								IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
									PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				ENDIF
			ELSE
				//Blips
				IF DOES_BLIP_EXIST(blipLocate)
					REMOVE_BLIP(blipLocate)
				ENDIF
				
				IF DoneCarChat = TRUE
				AND OfficeFail = FALSE
				AND counterFail = FALSE
					IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					ENDIF
				ENDIF
				
				//God text
				IF IS_VEHICLE_DRIVEABLE(vehCar)
					IF NOT DOES_BLIP_EXIST(BlipVeh)
						BlipVeh = CREATE_BLIP_FOR_VEHICLE(vehCar)
						IF playerhasbeenincar = TRUE
							IF GotoCarTextDone = FALSE
								CLEAR_PRINTS()
								ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(FALSE)
								PRINT_NOW("GOTO_CAR2", DEFAULT_GOD_TEXT_TIME, 1)//~s~Get back in your ~b~car.
								GotoCarTextDone =  TRUE
							ENDIF
						ELSE
							IF GetInCarText = FALSE
								CLEAR_PRINTS()
								PRINT("GOTO_CAR", DEFAULT_GOD_TEXT_TIME, 1)//~s~Get in your ~b~car.
								GetInCarText = TRUE
							ENDIF	
						ENDIF
						SETTIMERB(0)
					ENDIF
				ENDIF
			ENDIF				
		ENDIF
		
	ENDIF

	IF iControlFlag = 2
		
		IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), DEFAULT_VEH_STOPPING_DISTANCE, 1, 0.2)
			TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), vehCar)
		
			REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
		
			iControlFlag = 0
			iCutsceneStage = 0
			missionStage = STAGE_SCOUT_OUT_STORE			
			
		ENDIF		

	ENDIF
	
//	IF iControlFlag = 3
//		
//		IF DOES_ENTITY_EXIST(vehCar)
//			IF IS_VEHICLE_DRIVEABLE(vehCar)
//				IF IS_ENTITY_IN_ANGLED_AREA(vehCar, <<-661.279968,-267.138245,34.393761>>, <<-656.862854,-274.783630,37.700790>>, 5.750000)
//					IF IS_VEHICLE_STOPPED(vehCar)				
//						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
//							TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), vehCar)
//						ENDIF
//						iControlFlag = 0
//						iCutsceneStage = 0
//						missionStage = STAGE_SCOUT_OUT_STORE
//					ENDIF
//				ELSE
//					//Go back to stage 1 if car goes passed the locate.
//					iControlFlag = 1
//				ENDIF
//			ENDIF
//		ENDIF
//				
//	ENDIF

ENDPROC

//PURPOSE:Controls the left and right movements for the glasses cam
PROC LEFT_AND_RIGHT_CONTROLLER()

	IF fCamNewDirectionZCoord < -179.9
		fCamNewDirectionZCoord = (fCamNewDirectionZCoord + 360)
	ENDIF
	IF fCamNewDirectionZCoord > 179.9
		fCamNewDirectionZCoord = (fCamNewDirectionZCoord - 360)
	ENDIF
	
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_LEFT_ONLY)
		fCamNewDirectionZCoord = (fCamNewDirectionZCoord + 2)
	ENDIF	
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_RIGHT_ONLY)
		fCamNewDirectionZCoord = (fCamNewDirectionZCoord - 2)
	ENDIF		

ENDPROC

//PURPOSE:Controls the up and down movements for the glasses cam
PROC UP_AND_DOWN_CONTROLLER()

	//General Rule 
	IF fCamNewDirectionXCoord > 80
		fCamNewDirectionXCoord = 80
	ENDIF
	IF fCamNewDirectionXCoord < -80
		fCamNewDirectionXCoord = -80
	ENDIF

	IF (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_UP_ONLY) AND NOT IS_LOOK_INVERTED())
	OR (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_DOWN_ONLY) AND IS_LOOK_INVERTED())
		IF fCamNewDirectionXCoord < 80
			fCamNewDirectionXCoord = (fCamNewDirectionXCoord + 2)
		ENDIF
	ENDIF
	
	IF (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_DOWN_ONLY) AND NOT IS_LOOK_INVERTED())
	OR (IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_LOOK_UP_ONLY) AND IS_LOOK_INVERTED())
		IF fCamNewDirectionXCoord > -80
			fCamNewDirectionXCoord = (fCamNewDirectionXCoord - 2)
		ENDIF
	ENDIF

ENDPROC

//PURPOSE:Controls the FOV movements for the glasses cam
PROC FOV_CONTROLLER()
	
	fminFOV = 27
	fMaxFOV = 46
	
	GET_SOUND_ID()
	
	//General rule.
	IF fGlassesCamFOV < fminFOV
		fGlassesCamFOV = fminFOV
	ENDIF
	IF fGlassesCamFOV > fMaxFOV
		fGlassesCamFOV = fMaxFOV
	ENDIF			
	
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_IN_ONLY)
		IF fGlassesCamFOV > fminFOV
			IF isCamZoomInSoundPlaying = FALSE
				PLAY_SOUND_FRONTEND (iCamZoomInSoundID, "Camera_Zoom", "Phone_SoundSet_Glasses_Cam")
				isCamZoomInSoundPlaying = TRUE
			ENDIF
			IF isCamZoomInSoundPlaying = TRUE
				IF GET_SOUND_ID() = iCamZoomInSoundID
					IF HAS_SOUND_FINISHED(iCamZoomInSoundID)
						PLAY_SOUND_FRONTEND (iCamZoomInSoundID, "Camera_Zoom", "Phone_SoundSet_Glasses_Cam")
					ENDIF
				ENDIF
			ENDIF
			fGlassesCamFOV = (fGlassesCamFOV - 1)
		ELSE
			IF isCamZoomInSoundPlaying = TRUE
				STOP_SOUND(iCamZoomInSoundID)
				isCamZoomInSoundPlaying = FALSE
			ENDIF	
		ENDIF
	ELSE
		IF isCamZoomInSoundPlaying = TRUE
			STOP_SOUND(iCamZoomInSoundID)
			isCamZoomInSoundPlaying = FALSE
		ENDIF		
	ENDIF
	
	IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SNIPER_ZOOM_OUT_ONLY)
		IF fGlassesCamFOV < fMaxFOV
			IF isCamZoomOutSoundPlaying = FALSE
				PLAY_SOUND_FRONTEND (iCamZoomOutSoundID, "Camera_Zoom", "Phone_SoundSet_Glasses_Cam")
				isCamZoomOutSoundPlaying = TRUE
			ENDIF
			IF isCamZoomOutSoundPlaying = TRUE
				IF GET_SOUND_ID() = iCamZoomOutSoundID
					IF HAS_SOUND_FINISHED(iCamZoomOutSoundID)
						PLAY_SOUND_FRONTEND (iCamZoomOutSoundID, "Camera_Zoom", "Phone_SoundSet_Glasses_Cam")
					ENDIF
				ENDIF
			ENDIF
		
			fGlassesCamFOV = (fGlassesCamFOV + 1)
		ELSE
			IF isCamZoomOutSoundPlaying = TRUE
				STOP_SOUND(iCamZoomOutSoundID)
				isCamZoomOutSoundPlaying = FALSE
			ENDIF
		ENDIF
	ELSE
		IF isCamZoomOutSoundPlaying = TRUE
			STOP_SOUND(iCamZoomOutSoundID)
			isCamZoomOutSoundPlaying = FALSE
		ENDIF	
	ENDIF
	
ENDPROC

PROC APPLY_TEMP_DEAD_ZONE(INT &iMod)
	IF iMod > 0
	AND iMod < 10
		iMod = 0
	ENDIF
	IF iMod < 0
	AND iMod > -10
		iMod = 0
	ENDIF
ENDPROC

//PURPOSE: Updates the glasses cam's position
PROC UPDATE_GLASSES_CAM()

	CONST_FLOAT ZOOM_SPEED		50.0
			
	//Convert input into rotation/zoom changes
	INT i_left_stick_x, i_left_stick_y, i_right_stick_x, i_right_stick_y
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(i_left_stick_x, i_left_stick_y, i_right_stick_x, i_right_stick_y)

	i_left_stick_y = FLOOR(GET_CONTROL_NORMAL(FRONTEND_CONTROL, INPUT_SNIPER_ZOOM) * 127)
	
	IF NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
	
		// temp deadzone
		APPLY_TEMP_DEAD_ZONE(i_left_stick_x)
		APPLY_TEMP_DEAD_ZONE(i_left_stick_y)
		APPLY_TEMP_DEAD_ZONE(i_right_stick_x)
		APPLY_TEMP_DEAD_ZONE(i_right_stick_y)
	
	ENDIF
	
	FLOAT f_zoom_modifier = (f_current_cam_fov - MIN_ZOOM) / (MAX_ZOOM - MIN_ZOOM) //Changes turn/zoom speed depending on how far the scope is zoomed in
			
	//Modify input based on controller settings: can swap zoom/aim to other sticks, and invert y-look
	INT i_rot_x_input = i_right_stick_y
	INT i_rot_z_input = i_right_stick_x
	INT i_zoom_input = i_left_stick_y
			
	IF IS_LOOK_INVERTED()
		i_rot_x_input = -i_rot_x_input
	ENDIF
			
	//Rotation
	FLOAT f_x_rot_vel = -(i_rot_x_input / 128.0) * f_rotate_speed //(ROTATE_SPEED / (1.0 + (f_zoom_modifier * 10.0)))
	FLOAT f_z_rot_vel = -(i_rot_z_input / 128.0) * f_rotate_speed //(ROTATE_SPEED / (1.0 + (f_zoom_modifier * 10.0)))
	
	
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		f_x_rot_vel *= 5.0
		f_z_rot_vel *= 5.0
	ENDIF

	//When the cam is at high zoom levels, reduce the turn speed for better accuracy.
	IF f_current_cam_fov <= f_scale_rotate_speed_zoom
		FLOAT f_turn_modifier = (f_current_cam_fov - MAX_ZOOM) / (f_scale_rotate_speed_zoom - MAX_ZOOM)
		IF f_turn_modifier < 0.05
			f_turn_modifier = 0.05
		ENDIF
		
		f_x_rot_vel *= f_turn_modifier
		f_z_rot_vel *= f_turn_modifier
	ENDIF

	VECTOR v_obj_rot = GET_CAM_ROT(GlassesCamera)
			
	// rotate Z
	IF ABSI(i_rot_z_input) > 0
		v_obj_rot =  v_obj_rot +@ <<0.0, 0.0, f_z_rot_vel>>
	ENDIF
	
	// rotate X
	IF ABSI(i_rot_x_input) > 0
		v_obj_rot =  v_obj_rot +@ <<f_x_rot_vel, 0.0, 0.0>>
	ENDIF
			
	//Zoom
	FLOAT f_zoom_vel = (i_zoom_input / 128.0) * (ZOOM_SPEED / (1.0 + (f_zoom_modifier * 2.0)))
	BOOL bZoomed = TRUE
	IF ABSI(i_zoom_input) > 0
		f_current_cam_fov = f_current_cam_fov +@ f_zoom_vel
		IF f_current_cam_fov <= MAX_ZOOM
			f_current_cam_fov = MAX_ZOOM
			bZoomed = FALSE
		ENDIF
		IF f_current_cam_fov >= MIN_ZOOM
			f_current_cam_fov = MIN_ZOOM
			bZoomed = FALSE
		ENDIF
	ELSE
		bZoomed = FALSE
	ENDIF

	//Lock the look up and down	so player can't look all the way down or all the way up.
	IF v_obj_rot.x < -50
		v_obj_rot.x = -50
	ENDIF
	IF v_obj_rot.x > 50
		v_obj_rot.x = 50
	ENDIF
	
	//Zoom audio
	IF bZoomed
		IF HAS_SOUND_FINISHED(iCamZoomInSoundID)
			PLAY_SOUND_FRONTEND (iCamZoomInSoundID, "Camera_Zoom", "Phone_SoundSet_Glasses_Cam")
		ENDIF		
	ELSE
		IF NOT HAS_SOUND_FINISHED(iCamZoomInSoundID)
			STOP_SOUND(iCamZoomInSoundID)
		ENDIF
	ENDIF
	
	//In first person mode always rotate the player to match scripted camera
	IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
		SET_ENTITY_ROTATION(PLAYER_PED_ID(), << 0.0, 0.0, v_obj_rot.z >>)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	ENDIF
			
	//Update the camera
	SET_CAM_ROT(GlassesCamera, v_obj_rot)
	SET_CAM_FOV(GlassesCamera, f_current_cam_fov)

ENDPROC


//PURPOSE:Controls everything required for taking a picture using the glasses cam
PROC TAKING_PIC_CONTROLLER()

//	PRINTSTRING("Game Timer = ") PRINTINT(GET_GAME_TIMER()) PRINTNL()
//	PRINTSTRING("SecCamIsOnScreenTimer = ") PRINTINT(SecCamIsOnScreenTimer) PRINTNL()
//	PRINTSTRING("VentIsOnScreenTimer = ") PRINTINT(VentIsOnScreenTimer) PRINTNL()
//	PRINTSTRING("AlarmIsOnScreenTimer = ") PRINTINT(AlarmIsOnScreenTimer) PRINTNL()
	
	
//	#IF IS_DEBUG_BUILD
//	
//	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_4) 
//		debugX = (debugX + 0.1)
//	ENDIF
//	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_6) 
//		debugX = (debugX - 0.1)
//	ENDIF
//	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_8) 
//		debugZ = (debugZ + 0.1)
//	ENDIF
//	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_2) 
//		debugZ = (debugZ - 0.1)
//	ENDIF
//	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_1) 
//		debugY = (debugY + 0.1)
//	ENDIF
//	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_3) 
//		debugY = (debugY - 0.1)
//	ENDIF
//	
//	PRINTSTRING("Offset Coords are = ") PRINTVECTOR(<<debugX, debugY, debugZ>>) PRINTNL()
//	
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[0], <<-0.2, 0, 0>>), 0.1)
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[0], <<0.2, 0, 0>>), 0.1)
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[0], <<0, -0.1, 0.3>>), 0.1)
//
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[1], <<-0.2, 0, 0>>), 0.1)
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[1], <<0.2, 0, 0>>), 0.1)
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[1], <<0, -0.1, 0.4>>), 0.1)
//	
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[2], <<-0.2, 0, 0>>), 0.1)
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[2], <<0.2, 0, 0>>), 0.1)
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[2], <<0, -0.1, 0.4>>), 0.1)
//	
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[3], <<-0.2, 0, 0>>), 0.1)
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[3], <<0.2, 0, 0>>), 0.1)
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[3], <<0, -0.1, 0.4>>), 0.1)
//
////	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[0], << 0.3, 0.2, -0.1 >>), 0.1)
////	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[0], << -0.1, 0.2, -0.3 >>), 0.1)
////	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[0], <<-0.2, -0.4, 0>>), 0.1)
//
// 	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[1], << 0.3, 0.2, -0.1 >>), 0.1)
// 	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[1], << 0.1, -0.2, -0.3 >>), 0.1)
// 	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[1], <<-0.4, 0.1, 0.1>>), 0.1)
//
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[2], <<-0.3, 0, 0.2>>), 0.1)
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[2], <<0, -0.3, -0.3>>), 0.1)
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[2], <<0, 0.3, -0.3>>), 0.1)
//	
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[3], <<-0.3, 0.1, -0.3>>), 0.1)
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[3], <<0.3, -0.1, -0.3>>), 0.1)
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[3], <<-0.1, -0.2, 0.3>>), 0.1)
//	
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallAlarm[0], <<0.1, 0.1, 0.1>>), 0.1)
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallAlarm[0], <<-0.1, -0.1, 0.1>>), 0.1)
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallAlarm[0], <<0, 0, -0.1>>), 0.1)
////	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallAlarm[0], <<debugX, debugY, debugZ>>), 0.1)
//	
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallAlarm[1], <<0.1, 0.1, 0.1>>), 0.1)
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallAlarm[1], <<-0.1, -0.1, 0.1>>), 0.1)
//	DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallAlarm[1], <<0, 0, -0.1>>), 0.1)
//
//	#ENDIF
	
	IF missionStage = STAGE_SCOUT_OUT_STORE
		//Check for the security cams being snapped	
		IF GlassesCamActive = TRUE
			IF playerTakingPhoto = TRUE
			AND PhotoJustTaken = TRUE
				IF DOES_ENTITY_EXIST(ObjBallCam[0])
				AND IS_TRACKED_POINT_VISIBLE(iTrackedCamPoint[0])
					IF IS_ENTITY_ON_SCREEN(ObjBallCam[0])
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[0], <<-0.2, 0, 0>>), 0.1)
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[0], <<0.2, 0, 0>>), 0.1)
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[0], <<0, -0.1, 0.3>>), 0.1)
						iSnapDialogueTimer = GET_GAME_TIMER()
	//					SecCamIsOnScreenTimer = GET_GAME_TIMER()
						CamIsOnScreen[0] = TRUE
						SecurityCamObjectiveDone = TRUE
//						PRINTSTRING("ObjBallCam[0] = TRUE") PRINTNL()
					ELSE
//						PRINTSTRING("ObjBallCam[0] = FALSE") PRINTNL()
						CamIsOnScreen[0] = FALSE
					ENDIF
				ELSE
					CamIsOnScreen[0] = FALSE
				ENDIF
				IF DOES_ENTITY_EXIST(ObjBallCam[1])
				AND IS_TRACKED_POINT_VISIBLE(iTrackedCamPoint[1])
					IF IS_ENTITY_ON_SCREEN(ObjBallCam[1])
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[1], <<-0.2, 0, 0>>), 0.1)
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[1], <<0.2, 0, 0>>), 0.1)
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[1], <<0, -0.1, 0.4>>), 0.1)
						iSnapDialogueTimer = GET_GAME_TIMER()
	//					SecCamIsOnScreenTimer = GET_GAME_TIMER()
						CamIsOnScreen[1] = TRUE
						SecurityCamObjectiveDone = TRUE
//						PRINTSTRING("ObjBallCam[1] = TRUE") PRINTNL()
					ELSE
						CamIsOnScreen[1] = FALSE
//						PRINTSTRING("ObjBallCam[1] = FALSE") PRINTNL()
					ENDIF
				ELSE
					CamIsOnScreen[1] = FALSE
				ENDIF
				IF DOES_ENTITY_EXIST(ObjBallCam[2])
				AND IS_TRACKED_POINT_VISIBLE(iTrackedCamPoint[2])
					IF IS_ENTITY_ON_SCREEN(ObjBallCam[2])
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[2], <<-0.2, 0, 0>>), 0.1)
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[2], <<0.2, 0, 0>>), 0.1)
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[2], <<0, -0.1, 0.4>>), 0.1)
						iSnapDialogueTimer = GET_GAME_TIMER()
	//					SecCamIsOnScreenTimer = GET_GAME_TIMER()
						CamIsOnScreen[2] = TRUE
						SecurityCamObjectiveDone = TRUE
//						PRINTSTRING("ObjBallCam[2] = TRUE") PRINTNL()
					ELSE
						CamIsOnScreen[2] = FALSE
//						PRINTSTRING("ObjBallCam[2] = FALSE") PRINTNL()
					ENDIF
				ELSE
					CamIsOnScreen[2] = FALSE
				ENDIF
				IF DOES_ENTITY_EXIST(ObjBallCam[3])
				AND IS_TRACKED_POINT_VISIBLE(iTrackedCamPoint[3])
					IF IS_ENTITY_ON_SCREEN(ObjBallCam[3])
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[3], <<-0.2, 0, 0>>), 0.1)
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[3], <<0.2, 0, 0>>), 0.1)
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallCam[3], <<0, -0.1, 0.4>>), 0.1)
						iSnapDialogueTimer = GET_GAME_TIMER()
	//					SecCamIsOnScreenTimer = GET_GAME_TIMER()
						CamIsOnScreen[3] = TRUE
						SecurityCamObjectiveDone = TRUE
//						PRINTSTRING("ObjBallCam[3] = TRUE") PRINTNL()
					ELSE
						CamIsOnScreen[3] = FALSE
//						PRINTSTRING("ObjBallCam[3] = FALSE") PRINTNL()
					ENDIF
				ELSE
					CamIsOnScreen[3] = FALSE
				ENDIF
				
				//Check for the vents being snapped			
				IF DOES_ENTITY_EXIST(ObjBallVent[1])
				AND IS_TRACKED_POINT_VISIBLE(iTrackedVentPoint[1])
					IF IS_ENTITY_ON_SCREEN(ObjBallVent[1])
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[1], << 0.3, 0.2, -0.1 >>), 0.1)
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[1], << 0.1, -0.2, -0.3 >>), 0.1)
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[1], <<-0.4, 0.1, 0.1>>), 0.1)
						iSnapDialogueTimer = GET_GAME_TIMER()
	//					VentIsOnScreenTimer = GET_GAME_TIMER()
						VentIsOnScreen[1] = TRUE
						VentObjectiveDone = TRUE
//						PRINTSTRING("ObjBallVent[1] = TRUE") PRINTNL()
					ELSE
						VentIsOnScreen[1] = FALSE
//						PRINTSTRING("ObjBallVent[1] = FALSE") PRINTNL()
					ENDIF
				ELSE
					VentIsOnScreen[1] = FALSE
				ENDIF
				IF DOES_ENTITY_EXIST(ObjBallVent[2])
				AND IS_TRACKED_POINT_VISIBLE(iTrackedVentPoint[2])
					IF IS_ENTITY_ON_SCREEN(ObjBallVent[2])
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[2], <<-0.3, 0, 0.2>>), 0.1)
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[2], <<0, -0.3, -0.3>>), 0.1)
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[2], <<0, 0.3, -0.3>>), 0.1)
						iSnapDialogueTimer = GET_GAME_TIMER()
	//					VentIsOnScreenTimer = GET_GAME_TIMER()
						VentIsOnScreen[2] = TRUE
						VentObjectiveDone = TRUE
//						PRINTSTRING("ObjBallVent[2] = TRUE") PRINTNL()
					ELSE
						VentIsOnScreen[2] = FALSE
//						PRINTSTRING("ObjBallVent[2] = FALSE") PRINTNL()
					ENDIF
				ELSE
					VentIsOnScreen[2] = FALSE
				ENDIF
				IF DOES_ENTITY_EXIST(ObjBallVent[3])
				AND IS_TRACKED_POINT_VISIBLE(iTrackedVentPoint[3])
					IF IS_ENTITY_ON_SCREEN(ObjBallVent[3])
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[3], <<-0.3, 0.1, -0.3>>), 0.1)
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[3], <<0.3, -0.1, -0.3>>), 0.1)
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallVent[3], <<-0.1, -0.2, 0.3>>), 0.1)
						iSnapDialogueTimer = GET_GAME_TIMER()
	//					VentIsOnScreenTimer = GET_GAME_TIMER()
						VentIsOnScreen[3] = TRUE
						VentObjectiveDone = TRUE
//						PRINTSTRING("ObjBallVent[3] = TRUE") PRINTNL()
					ELSE
						VentIsOnScreen[3] = FALSE
//						PRINTSTRING("ObjBallVent[3] = FALSE") PRINTNL()
					ENDIF
				ELSE
					VentIsOnScreen[3] = FALSE
				ENDIF
			
				//Check for the alarm being snapped.
				IF DOES_ENTITY_EXIST(ObjBallAlarm[0])
//				AND IS_OBJECT_VISIBLE(ObjBallAlarm[0])
				AND IS_TRACKED_POINT_VISIBLE(iTrackedAlarmPoint[0])
					IF IS_ENTITY_ON_SCREEN(ObjBallAlarm[0])
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallAlarm[0], <<0.1, 0.1, 0.1>>), 0.1)
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallAlarm[0], <<-0.1, -0.1, 0.1>>), 0.1)
					AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallAlarm[0], <<0, 0, -0.1>>), 0.1)
						iSnapDialogueTimer = GET_GAME_TIMER()
	//					AlarmIsOnScreenTimer = GET_GAME_TIMER()
						AlarmIsOnScreen[0] = TRUE
						AlarmObjectiveDone = TRUE
						PRINTSTRING("ObjBallAlarm[0] = TRUE") PRINTNL()
					ELSE
						AlarmIsOnScreen[0] = FALSE
						PRINTSTRING("ObjBallAlarm[0] = FALSE") PRINTNL()
					ENDIF
				ELSE
					AlarmIsOnScreen[0] = FALSE
				ENDIF
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-625.017029,-231.450577,36.307037>>, <<-616.516846,-236.969940,39.807037>>, 20.000000)
					IF DOES_ENTITY_EXIST(ObjBallAlarm[1])
					AND IS_TRACKED_POINT_VISIBLE(iTrackedAlarmPoint[1])
						IF IS_ENTITY_ON_SCREEN(ObjBallAlarm[1])
						AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallAlarm[1], <<0.1, 0.1, 0.1>>), 0.1)
						AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallAlarm[1], <<-0.1, -0.1, 0.1>>), 0.1)
						AND IS_SPHERE_VISIBLE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(ObjBallAlarm[1], <<0, 0, -0.1>>), 0.1)
							iSnapDialogueTimer = GET_GAME_TIMER()
	//						AlarmIsOnScreenTimer = GET_GAME_TIMER()
							AlarmIsOnScreen[1] = TRUE
							AlarmObjectiveDone = TRUE
//							PRINTSTRING("ObjBallAlarm[1] = TRUE") PRINTNL()
						ELSE
							AlarmIsOnScreen[1] = FALSE
//							PRINTSTRING("ObjBallAlarm[1] = FALSE") PRINTNL()
						ENDIF
					ELSE
						AlarmIsOnScreen[1] = FALSE
					ENDIF
				ENDIF
				PhotoJustTaken = FALSE
			ENDIF
			
			//Set flag for if one of the alarms is on screen or not
			IF AlarmIsOnScreen[0] = TRUE
			OR AlarmIsOnScreen[1] = TRUE
				AnAlarmIsOnScreen = TRUE
				PRINTSTRING("AnAlarmIsOnScreen = TRUE") PRINTNL()
			ELSE
				AnAlarmIsOnScreen = FALSE
//				PRINTSTRING("AnAlarmIsOnScreen = FALSE") PRINTNL()
			ENDIF
			
			//Set flag for if one of the cameras is on screen or not
			IF CamIsOnScreen[0] = TRUE
			OR CamIsOnScreen[1] = TRUE
			OR CamIsOnScreen[2] = TRUE
			OR CamIsOnScreen[3] = TRUE
				ACamIsOnScreen = TRUE
				PRINTSTRING("ACamIsOnScreen = TRUE") PRINTNL()
			ELSE
				ACamIsOnScreen = FALSE
//				PRINTSTRING("ACamIsOnScreen = FALSE") PRINTNL()
			ENDIF	
			
			//Set flag for if one of the vents is on screen or not
//			IF VentIsOnScreen[0] = TRUE
			IF VentIsOnScreen[1] = TRUE
			OR VentIsOnScreen[2] = TRUE
			OR VentIsOnScreen[3] = TRUE
				AVentIsOnScreen = TRUE
				PRINTSTRING("AVentIsOnScreen = TRUE") PRINTNL()
			ELSE
				AVentIsOnScreen = FALSE
//				PRINTSTRING("AVentIsOnScreen = FALSE") PRINTNL()
			ENDIF			
			
		ENDIF	
		//**************DIALOGUE********************
		
		//Only do dialogue when dialogue is needed and the timer is ready for a picture.
		IF SnapDialogueNeeded = TRUE
		AND GET_GAME_TIMER() > (iSnapDialogueTimer + 500)
//			PRINTSTRING("SnapDialogueNeeded = TRUE AND GET_GAME_TIMER() > (iSnapDialogueTimer + 500) ") PRINTNL()
				
			//Make sure everything gets mentioned if player is taking pics too quickly	
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				//Check for alarm being mentioned 
				IF AlarmHasBeenMentioned = FALSE
					//If all 3 have been snapped
					IF AlarmHasBeenSnapped = TRUE
					AND CamHasBeenSnapped = TRUE
					AND VentHasBeenSnapped = TRUE
						IF donePH16Chat = FALSE
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH16", CONV_PRIORITY_MEDIUM)
								//Shot's come through. Camera: check. Alarm: check. Vents: check. 
								AlarmHasBeenMentioned = TRUE
								CamHasBeenMentioned = TRUE
								VentHasBeenMentioned = TRUE
								donePH16Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF
					
					//If just the alarm and cam have been snapped
					IF AlarmHasBeenSnapped = TRUE
					AND CamHasBeenSnapped = TRUE
					AND VentHasBeenSnapped = FALSE
						IF donePH21Chat = FALSE
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH21", CONV_PRIORITY_MEDIUM)
								//Perfect, that's the security cam and alarm. Just need the vents now.
								AlarmHasBeenMentioned = TRUE
								CamHasBeenMentioned = TRUE
								donePH21Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF	
					ENDIF
					
					//If just the alarm and vent have been snapped
					IF AlarmHasBeenSnapped = TRUE
					AND CamHasBeenSnapped = FALSE
					AND VentHasBeenSnapped = TRUE
						IF donePH20Chat = FALSE
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH20", CONV_PRIORITY_MEDIUM)
								//Good, that's the alarm and vents. Just need the security cams now.
								AlarmHasBeenMentioned = TRUE
								VentHasBeenMentioned = TRUE
								donePH20Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF
					
					//If just the alarm has been snapped
					IF AlarmHasBeenSnapped = TRUE
					AND CamHasBeenSnapped = FALSE
					AND VentHasBeenSnapped = FALSE
						IF donePH26Chat = FALSE
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH26", CONV_PRIORITY_MEDIUM)
								//Great, that's the alarm. Now get the vents and security cams.
								AlarmHasBeenMentioned = TRUE
								SnapDialogueNeeded = FALSE
								donePH26Chat = TRUE
							ENDIF
						ENDIF
					ENDIF						
				ENDIF
				
				//Check for Cam being mentioned 
				IF CamHasBeenMentioned = FALSE
					//If all 3 have been snapped
					IF AlarmHasBeenSnapped = TRUE
					AND CamHasBeenSnapped = TRUE
					AND VentHasBeenSnapped = TRUE
						IF donePH16Chat = FALSE
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH16", CONV_PRIORITY_MEDIUM)
								//Shot's come through. Camera: check. Alarm: check. Vents: check. 
								AlarmHasBeenMentioned = TRUE
								CamHasBeenMentioned = TRUE
								VentHasBeenMentioned = TRUE
								donePH16Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF
					
					//If just the alarm and cam have been snapped
					IF AlarmHasBeenSnapped = TRUE
					AND CamHasBeenSnapped = TRUE
					AND VentHasBeenSnapped = FALSE
						IF donePH21Chat = FALSE
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH21", CONV_PRIORITY_MEDIUM)
								//Perfect, that's the security cam and alarm. Just need the vents now.
								AlarmHasBeenMentioned = TRUE
								CamHasBeenMentioned = TRUE
								donePH21Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF	
					ENDIF
					
					//If just the cam and vent have been snapped
					IF AlarmHasBeenSnapped = FALSE
					AND CamHasBeenSnapped = TRUE
					AND VentHasBeenSnapped = TRUE
						IF donePH22Chat = FALSE
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH22", CONV_PRIORITY_MEDIUM)
								//Great, that's the security cam and the vent. Just need the alarm now.
								VentHasBeenMentioned = TRUE
								CamHasBeenMentioned = TRUE
								donePH22Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF
					
					//If just the Cam has been snapped
					IF AlarmHasBeenSnapped = FALSE
					AND CamHasBeenSnapped = TRUE
					AND VentHasBeenSnapped = FALSE
						IF donePH25Chat = FALSE
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH25", CONV_PRIORITY_MEDIUM)
								//Ok got the security camera. Now get the alarm and vents.
								CamHasBeenMentioned = TRUE
								donePH25Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF						
				ENDIF					
				
				//Check for Vent being mentioned 
				IF VentHasBeenMentioned = FALSE
					//If all 3 have been snapped
					IF AlarmHasBeenSnapped = TRUE
					AND CamHasBeenSnapped = TRUE
					AND VentHasBeenSnapped = TRUE
						IF donePH16Chat = FALSE
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH16", CONV_PRIORITY_MEDIUM)
								//Shot's come through. Camera: check. Alarm: check. Vents: check. 
								AlarmHasBeenMentioned = TRUE
								CamHasBeenMentioned = TRUE
								VentHasBeenMentioned = TRUE
								donePH16Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF
					
					//If just the alarm and Vent have been snapped
					IF AlarmHasBeenSnapped = TRUE
					AND CamHasBeenSnapped = FALSE
					AND VentHasBeenSnapped = TRUE
						IF donePH20Chat = FALSE
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH20", CONV_PRIORITY_MEDIUM)
								//Good, that's the alarm and vents. Just need the security cams now.
								AlarmHasBeenMentioned = TRUE
								VentHasBeenMentioned = TRUE
								donePH20Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF	
					ENDIF
					
					//If just the cam and vent have been snapped
					IF AlarmHasBeenSnapped = FALSE
					AND CamHasBeenSnapped = TRUE
					AND VentHasBeenSnapped = TRUE
						IF donePH22Chat = FALSE
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH22", CONV_PRIORITY_MEDIUM)
								//Great, that's the security cam and the vent. Just need the alarm now.
								VentHasBeenMentioned = TRUE
								CamHasBeenMentioned = TRUE
								donePH22Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF
					
					//If just the Vent has been snapped
					IF AlarmHasBeenSnapped = FALSE
					AND CamHasBeenSnapped = FALSE
					AND VentHasBeenSnapped = TRUE
						IF donePH24Chat = FALSE
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH24", CONV_PRIORITY_MEDIUM)
								//Perfect. That's the vents. Now get the alarm and security cams.
								VentHasBeenMentioned = TRUE
								donePH24Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF						
				ENDIF					
			ELSE
//				PRINTSTRING("Conversation is ongoing or queued so can't play conversation") PRINTNL()
			ENDIF
				
			//IF ALARM ON SCREEN ONLY 
			IF AnAlarmIsOnScreen = TRUE
			AND AVentIsOnScreen = FALSE
			AND ACamIsOnScreen = FALSE
				
				AlarmHasBeenSnapped = TRUE
				
				//IF CAMS AND VENT STILL REQUIRED
				IF SecurityCamObjectiveDone = FALSE
				AND VentObjectiveDone = FALSE
					IF donePH26Chat = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH26", CONV_PRIORITY_MEDIUM)
								//Great, that's the alarm. Now get the vents and security cams.
								AlarmHasBeenMentioned = TRUE
								SnapDialogueNeeded = FALSE
								donePH26Chat = TRUE
							ENDIF
						ENDIF
					ELSE
						IF AlarmHasBeenMentioned = TRUE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH40", CONV_PRIORITY_MEDIUM)
									//Vents and cameras still required. 
									//Vents and cameras please. 
									//My checklist reads ventilation system and CCTV.
									SnapDialogueNeeded = FALSE
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//IF CAMS ONLY STILL REQUIRED
				IF SecurityCamObjectiveDone = FALSE
				AND VentObjectiveDone = TRUE		
					IF donePH20Chat = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH20", CONV_PRIORITY_MEDIUM)
								//Good, that's the alarm and vents. Just need the security cams now.
								AlarmHasBeenMentioned = TRUE
								VentHasBeenMentioned = TRUE
								donePH20Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ELSE
						IF AlarmHasBeenMentioned = TRUE
						AND CamHasBeenMentioned = TRUE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH35", CONV_PRIORITY_MEDIUM)
									//Just the cameras left to photograph.
									//All we needs a closed circuit tv camera.
									//Any of those CCTV cameras'll do.
									SnapDialogueNeeded = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//IF VENT ONLY STILL REQUIRED
				IF SecurityCamObjectiveDone = TRUE
				AND VentObjectiveDone = FALSE		
					IF donePH21Chat = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH21", CONV_PRIORITY_MEDIUM)
								//Perfect, that's the security cam and alarm. Just need the vents now.
								AlarmHasBeenMentioned = TRUE
								CamHasBeenMentioned = TRUE
								donePH21Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ELSE
						IF AlarmHasBeenMentioned = TRUE
						AND CamHasBeenMentioned = TRUE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH36", CONV_PRIORITY_MEDIUM)
									//If you can get me a shot of the vents, we're done.
									//All I needs a shot of the vents.
									//If you can just get me a shot of the ventilation...
									SnapDialogueNeeded = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//IF NOTHING ELSE REQUIRED
				IF SecurityCamObjectiveDone = TRUE
				AND VentObjectiveDone = TRUE	
					IF VentHasBeenMentioned = TRUE
					AND CamHasBeenMentioned = TRUE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH7", CONV_PRIORITY_MEDIUM)
								//Shot of the alarm's come through.
								AlarmHasBeenMentioned = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//IF VENTS ON SCREEN ONLY 
			IF AnAlarmIsOnScreen = FALSE
			AND AVentIsOnScreen = TRUE
			AND ACamIsOnScreen = FALSE
			
				VentHasBeenSnapped = TRUE
			
				//IF CAMS AND ALARM STILL REQUIRED
				IF SecurityCamObjectiveDone = FALSE
				AND AlarmObjectiveDone = FALSE
					IF donePH24Chat = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH24", CONV_PRIORITY_MEDIUM)
								//Perfect. That's the vents. Now get the alarm and security cams.
								VentHasBeenMentioned = TRUE
								donePH24Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ELSE
						IF VentHasBeenMentioned = TRUE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH39", CONV_PRIORITY_MEDIUM)
									//We've still got the alarm and the cameras on our checklist.
									//I'm waiting on a shot of the alarm and a shot of a camera.
									//How you getting on with the alarm and the security cameras?
									SnapDialogueNeeded = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//IF CAMS ONLY STILL REQUIRED
				IF SecurityCamObjectiveDone = FALSE
				AND AlarmObjectiveDone = TRUE
					IF donePH20Chat = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH20", CONV_PRIORITY_MEDIUM)
								//Good, that's the alarm and vents. Just need the security cams now.
								VentHasBeenMentioned = TRUE
								AlarmHasBeenMentioned = TRUE
								donePH20Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ELSE
						IF VentHasBeenMentioned = TRUE
						AND AlarmHasBeenMentioned = TRUE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH35", CONV_PRIORITY_MEDIUM)
									//Just the cameras left to photograph.
									//All we needs a closed circuit tv camera.
									//Any of those CCTV cameras'll do.
									SnapDialogueNeeded = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//IF ALARM ONLY STILL REQUIRED
				IF SecurityCamObjectiveDone = TRUE
				AND AlarmObjectiveDone = FALSE
					IF donePH22Chat = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH22", CONV_PRIORITY_MEDIUM)
								//Great, that's the security cam and the vent. Just need the alarm now.
								VentHasBeenMentioned = TRUE
								CamHasBeenMentioned = TRUE
								donePH22Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ELSE
						IF VentHasBeenMentioned = TRUE
						AND CamHasBeenMentioned = TRUE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH37", CONV_PRIORITY_MEDIUM)
									//Now, all I need is a shot of the alarm keypad.
									//The keypad, and we're done.
									//Come on, we need a shot of the alarm console. 
									SnapDialogueNeeded = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//IF NOTHING ELSE REQUIRED
				IF SecurityCamObjectiveDone = TRUE
				AND AlarmObjectiveDone = TRUE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF donePH5Chat = FALSE
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH5", CONV_PRIORITY_MEDIUM)
								//Alright, that's the ventilation.
								VentHasBeenMentioned = TRUE
								SnapDialogueNeeded = FALSE
								donePH5Chat = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//IF CAMS ON SCREEN ONLY 
			IF AnAlarmIsOnScreen = FALSE
			AND AVentIsOnScreen = FALSE
			AND ACamIsOnScreen = TRUE
				
				CamHasBeenSnapped = TRUE
			
				//IF VENTS AND ALARM STILL REQUIRED
				IF VentObjectiveDone = FALSE
				AND AlarmObjectiveDone = FALSE
					IF donePH25Chat = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH25", CONV_PRIORITY_MEDIUM)
								//Ok got the security camera. Now get the alarm and vents.
								CamHasBeenMentioned = TRUE
								donePH25Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ELSE
						//Do random dialogue here
						IF CamHasBeenMentioned = TRUE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH38", CONV_PRIORITY_MEDIUM)
									//We still need a shot of alarm and air vents.
									//Get a snap of the air vent's, Michael.
									//Ok we've got enough of the cams and alarm. Get a snap of the vents.
									SnapDialogueNeeded = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
				
				//IF ALARM ONLY STILL REQUIRED
				IF VentObjectiveDone = TRUE
				AND AlarmObjectiveDone = FALSE
					IF donePH22Chat = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH22", CONV_PRIORITY_MEDIUM)
								//Great, that's the security cam and the vent. Just need the alarm now.
								CamHasBeenMentioned = TRUE
								VentHasBeenMentioned = TRUE
								donePH22Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ELSE
						//Do random dialogue here
						IF VentHasBeenMentioned = TRUE
						AND CamHasBeenMentioned = TRUE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH37", CONV_PRIORITY_MEDIUM)
									//We still need a snap of the alarm, Michael.
									//Ok now get a snap of the alarm system.
									//We've got enough pics of the cams and vents. Get a snap of the alarm, Michael.
									SnapDialogueNeeded = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
				
				//IF VENTS ONLY STILL REQUIRED
				IF VentObjectiveDone = FALSE
				AND AlarmObjectiveDone = TRUE
					IF donePH21Chat = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH21", CONV_PRIORITY_MEDIUM)
								//Perfect, that's the security cam and alarm. Just need the vents now.
								CamHasBeenMentioned = TRUE
								AlarmHasBeenMentioned = TRUE
								donePH21Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ELSE
						//Do random dialogue here
						IF AlarmHasBeenMentioned = TRUE
						AND CamHasBeenMentioned = TRUE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH36", CONV_PRIORITY_MEDIUM)
									//We still need a shot of the air vents.
									//Get a snap of the air vent's, Michael.
									//Ok we've got enough of the cams and alarm. Get a snap of the vents.
									SnapDialogueNeeded = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
				
				//IF NOTHING ELSE REQUIRED
				IF VentObjectiveDone = TRUE
				AND AlarmObjectiveDone = TRUE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF donePH6Chat = FALSE
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH6", CONV_PRIORITY_MEDIUM)
								//Okay, that's a security camera.
								CamHasBeenMentioned = TRUE
								SnapDialogueNeeded = FALSE
								donePH6Chat = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//IF ALARM AND VENT ON SCREEN BUT NO CAM
			IF AnAlarmIsOnScreen = TRUE
			AND AVentIsOnScreen = TRUE
			AND ACamIsOnScreen = FALSE
			
				AlarmHasBeenSnapped = TRUE
				VentHasBeenSnapped = TRUE
				
				//IF ALL PICS HAVE BEEN TAKEN
				IF VentObjectiveDone = TRUE
				AND AlarmObjectiveDone = TRUE
				AND SecurityCamObjectiveDone = TRUE					
					IF donePH17Chat = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH17", CONV_PRIORITY_MEDIUM)
								//So, that's the ventilation and the alarm system.
								VentHasBeenMentioned = TRUE
								AlarmHasBeenMentioned = TRUE
								donePH17Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//IF CAM STILL REQUIRED
				IF VentObjectiveDone = TRUE
				AND AlarmObjectiveDone = TRUE
				AND SecurityCamObjectiveDone = FALSE
					IF donePH20Chat = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH20", CONV_PRIORITY_MEDIUM)
								//Good, that's the alarm and vents. Just need the security cams now.
								VentHasBeenMentioned = TRUE
								AlarmHasBeenMentioned = TRUE
								donePH20Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ELSE
						//Do random dialogue here
						IF AlarmHasBeenMentioned = TRUE
						AND VentHasBeenMentioned = TRUE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH35", CONV_PRIORITY_MEDIUM)
									//We still need a shot of the security camera's.
									//Get a snap of the security camera's, Michael.
									//Ok we've got the vents and alarm, get the security camera's.
									SnapDialogueNeeded = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//IF ALARM AND CAM BUT NO VENTS
			IF AnAlarmIsOnScreen = TRUE
			AND AVentIsOnScreen = FALSE
			AND ACamIsOnScreen = TRUE
				
				AlarmHasBeenSnapped = TRUE
				CamHasBeenSnapped = TRUE
				
				//IF ALL PICS HAVE BEEN TAKEN
				IF AlarmObjectiveDone = TRUE
				AND SecurityCamObjectiveDone = TRUE
				AND VentObjectiveDone = TRUE
					IF donePH18Chat = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH18", CONV_PRIORITY_MEDIUM)
								//What we got? A security camera and the alarm system. 
								CamHasBeenMentioned = TRUE
								AlarmHasBeenMentioned = TRUE
								donePH18Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//IF VENT STILL REQUIRED
				IF AlarmObjectiveDone = TRUE
				AND SecurityCamObjectiveDone = TRUE
				AND VentObjectiveDone = FALSE
					IF donePH21Chat = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH21", CONV_PRIORITY_MEDIUM)
								//Perfect, that's the security cam and alarm. Just need the vents now.
								CamHasBeenMentioned = TRUE
								AlarmHasBeenMentioned = TRUE
								donePH21Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ELSE
						//Do random dialogue here
						IF AlarmHasBeenMentioned = TRUE
						AND CamHasBeenMentioned = TRUE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH36", CONV_PRIORITY_MEDIUM)
									//We still need a shot of the air vents.
									//Get a snap of the air vent's, Michael.
									//Ok we've got enough of the cams and alarm. Get a snap of the vents.
									SnapDialogueNeeded = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//IF VENT AND CAM BUT NO ALARM
			IF AnAlarmIsOnScreen = FALSE
			AND AVentIsOnScreen = TRUE
			AND ACamIsOnScreen = TRUE
				
				CamHasBeenSnapped = TRUE
				VentHasBeenSnapped = TRUE
				
				//IF ALL PICS HAVE BEEN TAKEN
				IF VentObjectiveDone = TRUE
				AND SecurityCamObjectiveDone = TRUE
				AND AlarmObjectiveDone = TRUE
					IF donePH19Chat = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH19", CONV_PRIORITY_MEDIUM)
								//Great, you got a security camera and the ventilation in that shot.
								VentHasBeenMentioned = TRUE
								CamHasBeenMentioned = TRUE
								donePH19Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//IF ALARM STILL REQUIRED
				IF VentObjectiveDone = TRUE
				AND SecurityCamObjectiveDone = TRUE
				AND AlarmObjectiveDone = FALSE
					IF donePH22Chat = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH22", CONV_PRIORITY_MEDIUM)
								//Great, that's the security cam and the vent. Just need the alarm now.
								VentHasBeenMentioned = TRUE
								CamHasBeenMentioned = TRUE
								donePH22Chat = TRUE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ELSE
						//Do random dialogue here
						IF VentHasBeenMentioned = TRUE
						AND CamHasBeenMentioned = TRUE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH37", CONV_PRIORITY_MEDIUM)
									//We still need a snap of the alarm, Michael.
									//Ok now get a snap of the alarm system.
									//We've got enough pics of the cams and vents. Get a snap of the alarm, Michael.
									SnapDialogueNeeded = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
	
			//If all are on the screen 
			IF AnAlarmIsOnScreen = TRUE
			AND AVentIsOnScreen = TRUE
			AND ACamIsOnScreen = TRUE		
			
				CamHasBeenSnapped = TRUE
				VentHasBeenSnapped = TRUE
				AlarmHasBeenSnapped = TRUE
				
				//Set the stat for perfect shot to true
				IF statPicturePerfectSet = FALSE
					INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(JH1_PERFECT_PIC)	
					PRINTSTRING("PICTURE PERFECT STAT ACHIEVED") PRINTNL()
					statPicturePerfectSet = TRUE
				ENDIF
				
				IF donePH16Chat = FALSE
					IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH16", CONV_PRIORITY_MEDIUM)
						//Shot's come through. Camera: check. Alarm: check. Vents: check. 
						
						AlarmHasBeenMentioned = TRUE
						CamHasBeenMentioned = TRUE
						VentHasBeenMentioned = TRUE
						donePH16Chat = TRUE
						SnapDialogueNeeded = FALSE
					ENDIF
				ENDIF
			ENDIF
				
			//If none are on screen have random dialogue telling player that.
			IF AnAlarmIsOnScreen = FALSE
			AND AVentIsOnScreen = FALSE
			AND ACamIsOnScreen = FALSE

				//Do random dialogue here
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH49", CONV_PRIORITY_MEDIUM)
						//What's this of? Fuck all.  
						//There's nothing here. 
						//This shot is useless to us. 
						//This doesn't help us.
						//What am I looking at here?
						//I can't see anything helpful.
						SnapDialogueNeeded = FALSE
					ENDIF	
				ENDIF
				
			ENDIF
			
		ELSE
//			PRINTSTRING("SnapDialogueNeeded = FALSE OR GET_GAME_TIMER() < (iSnapDialogueTimer + 500) ") PRINTNL()
		ENDIF

	ENDIF
	
	
//
//	//Do random dialogue if the camera needs zoomed in more.
//	IF AlarmObjectiveDone = FALSE
//	OR VentObjectiveDone = FALSE
//	OR SecurityCamObjectiveDone = FALSE
//		IF playerTakingPhoto = TRUE
//			IF DOES_ENTITY_EXIST(ObjBallAlarm[0])
//				IF IS_ENTITY_ON_SCREEN(ObjBallAlarm[0])
//					IF fGlassesCamFOV > 30
//						KILL_ANY_CONVERSATION()
//						IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH4", CONV_PRIORITY_MEDIUM)
//							//Zoom in closer Michael that's no use.
//							//I can't make that one out Michael, zoom in a bit more.
//							//Use the zoom on the glasses Michael.
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//			FOR icount = 0 TO 3
//				IF DOES_ENTITY_EXIST(ObjBallVent[icount])
//					IF IS_ENTITY_ON_SCREEN(ObjBallVent[icount])
//						IF fGlassesCamFOV > 30
//							KILL_ANY_CONVERSATION()
//							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH4", CONV_PRIORITY_MEDIUM)
//								//Zoom in closer Michael that's no use.
//								//I can't make that one out Michael, zoom in a bit more.
//								//Use the zoom on the glasses Michael.
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDFOR
//			FOR icount = 0 TO 3
//				IF DOES_ENTITY_EXIST(ObjBallCam[icount])
//					IF IS_ENTITY_ON_SCREEN(ObjBallCam[icount])
//						IF fGlassesCamFOV > 30
//							KILL_ANY_CONVERSATION()
//							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH4", CONV_PRIORITY_MEDIUM)
//								//Zoom in closer Michael that's no use.
//								//I can't make that one out Michael, zoom in a bit more.
//								//Use the zoom on the glasses Michael.
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDFOR
//		ENDIF
//	ENDIF

ENDPROC

//PURPOSE: creates the invisible balls on the cameras, vents and alarm to be used as a check for if they've had a pic taken of them.
PROC CREATE_INVISIBLE_BALLS()

	IF HAS_MODEL_LOADED(PROP_BOWLING_BALL)
		//Create the 4 that will cover the security cams
		IF NOT DOES_ENTITY_EXIST(ObjBallCam[0])
			ObjBallCam[0] = CREATE_OBJECT(PROP_BOWLING_BALL, <<-627.5507, -229.4447, 40.1249>>)
			SET_ENTITY_COLLISION(ObjBallCam[0], FALSE)
			SET_ENTITY_VISIBLE(ObjBallCam[0], FALSE)
			FREEZE_ENTITY_POSITION(ObjBallCam[0], TRUE)
			iTrackedCamPoint[0] = CREATE_TRACKED_POINT()
			SET_TRACKED_POINT_INFO(iTrackedCamPoint[0], <<-627.6, -229.7, 40.3>>, 0.1)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(ObjBallCam[1])
			ObjBallCam[1] = CREATE_OBJECT(PROP_BOWLING_BALL, <<-627.4518, -240.0007, 40.1306>>)
			SET_ENTITY_COLLISION(ObjBallCam[1], FALSE)
			SET_ENTITY_VISIBLE(ObjBallCam[1], FALSE)
			FREEZE_ENTITY_POSITION(ObjBallCam[1], TRUE)
			iTrackedCamPoint[1] = CREATE_TRACKED_POINT()
			SET_TRACKED_POINT_INFO(iTrackedCamPoint[1], <<-627.4, -239.7, 40.3>>, 0.1)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(ObjBallCam[2])
			ObjBallCam[2] = CREATE_OBJECT(PROP_BOWLING_BALL, <<-622.4700, -236.3400, 40.1371>>)
			SET_ENTITY_COLLISION(ObjBallCam[2], FALSE)
			SET_ENTITY_VISIBLE(ObjBallCam[2], FALSE)
			FREEZE_ENTITY_POSITION(ObjBallCam[2], TRUE)
			iTrackedCamPoint[2] = CREATE_TRACKED_POINT()
			SET_TRACKED_POINT_INFO(iTrackedCamPoint[2], <<-622.4, -236.1, 40.3>>, 0.1)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(ObjBallCam[3])
			ObjBallCam[3] = CREATE_OBJECT(PROP_BOWLING_BALL, <<-620.2918, -224.1618, 40.1266>>)
			SET_ENTITY_COLLISION(ObjBallCam[3], FALSE)
			SET_ENTITY_VISIBLE(ObjBallCam[3], FALSE)
			FREEZE_ENTITY_POSITION(ObjBallCam[3], TRUE)
			iTrackedCamPoint[3] = CREATE_TRACKED_POINT()
			SET_TRACKED_POINT_INFO(iTrackedCamPoint[3], <<-620.3, -224.4, 40.3>>, 0.1)
		ENDIF
		//Create the 3 that will cover the air vents
//		IF NOT DOES_ENTITY_EXIST(ObjBallVent[0])
//			ObjBallVent[0] = CREATE_OBJECT(PROP_BOWLING_BALL, <<-629.7500, -231.3500, 40.8671>>)
//			SET_ENTITY_COLLISION(ObjBallVent[0], FALSE)
//			SET_ENTITY_VISIBLE(ObjBallVent[0], FALSE)
//			FREEZE_ENTITY_POSITION(ObjBallVent[0], TRUE)
//		ENDIF
		IF NOT DOES_ENTITY_EXIST(ObjBallVent[1])
			ObjBallVent[1] = CREATE_OBJECT(PROP_BOWLING_BALL, <<-625.0201, -237.8714, 40.8512>>)
			SET_ENTITY_COLLISION(ObjBallVent[1], FALSE)
			SET_ENTITY_VISIBLE(ObjBallVent[1], FALSE)
			FREEZE_ENTITY_POSITION(ObjBallVent[1], TRUE)
			iTrackedVentPoint[1] = CREATE_TRACKED_POINT()
			SET_TRACKED_POINT_INFO(iTrackedVentPoint[1], <<-625.1, -237.8, 40.8>>, 0.1)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(ObjBallVent[2])
			ObjBallVent[2] = CREATE_OBJECT(PROP_BOWLING_BALL, <<-616.7790, -233.1593, 40.9528>>)
			SET_ENTITY_COLLISION(ObjBallVent[2], FALSE)
			SET_ENTITY_VISIBLE(ObjBallVent[2], FALSE)
			FREEZE_ENTITY_POSITION(ObjBallVent[2], TRUE)
			iTrackedVentPoint[2] = CREATE_TRACKED_POINT()
			SET_TRACKED_POINT_INFO(iTrackedVentPoint[2], <<-616.9, -233.2, 41.0>>, 0.1)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(ObjBallVent[3])
			ObjBallVent[3] = CREATE_OBJECT(PROP_BOWLING_BALL, <<-622.7000, -224.9900, 40.9571>>)
			SET_ENTITY_COLLISION(ObjBallVent[3], FALSE)
			SET_ENTITY_VISIBLE(ObjBallVent[3], FALSE)
			FREEZE_ENTITY_POSITION(ObjBallVent[3], TRUE)
			iTrackedVentPoint[3] = CREATE_TRACKED_POINT()
			SET_TRACKED_POINT_INFO(iTrackedVentPoint[3], <<-622.8, -225.1, 41.1>>, 0.1)
		ENDIF
		//Create the 1 that will cover the alarm
		IF NOT DOES_ENTITY_EXIST(ObjBallAlarm[0])
			ObjBallAlarm[0] = CREATE_OBJECT(PROP_BOWLING_BALL, <<-629.4000, -230.4400, 38.4071>>)
			SET_ENTITY_COLLISION(ObjBallAlarm[0], FALSE)
			SET_ENTITY_VISIBLE(ObjBallAlarm[0], FALSE)
			FREEZE_ENTITY_POSITION(ObjBallAlarm[0], TRUE)
			iTrackedAlarmPoint[0] = CREATE_TRACKED_POINT()
			SET_TRACKED_POINT_INFO(iTrackedAlarmPoint[0], <<-629.4, -230.5, 38.6>>, 0.1)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(ObjBallAlarm[1])
			ObjBallAlarm[1] = CREATE_OBJECT(PROP_BOWLING_BALL, <<-620.1974, -223.7788, 38.4137>>)
			SET_ENTITY_COLLISION(ObjBallAlarm[1], FALSE)
			SET_ENTITY_VISIBLE(ObjBallAlarm[1], FALSE)
			FREEZE_ENTITY_POSITION(ObjBallAlarm[1], TRUE)
			iTrackedAlarmPoint[1] = CREATE_TRACKED_POINT()
			SET_TRACKED_POINT_INFO(iTrackedAlarmPoint[1], <<-620.2, -223.8, 38.6>>, 0.1)
		ENDIF
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_BOWLING_BALL)
		AllBallsCreated = TRUE
		trackedPointsCreated = TRUE
	ENDIF

ENDPROC

//PURPOSE:Shows a short cutscene of Michael speaking to the Shop assistant.
PROC DO_CUTSCENE_OF_MICHAEL_AND_SHOP_ASSISTANT()
	
	biscutsceneActive 	= TRUE
	iCutsceneStage 		= 0
	
		
	WHILE biscutsceneActive = TRUE	
	
		IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP2")
			CLEAR_HELP()
		ENDIF	
	
		SWITCH iCutsceneStage
			
			CASE 0 	
				
				WHILE NOT HAS_CUTSCENE_LOADED()
					PRINTSTRING("Waiting for cutscene to load") PRINTNL()
					WAIT(0)
				ENDWHILE	
					
//				REQUEST_ANIM_DICT("amb@world_human_stand_guard@male@idle_a")	
				REQUEST_ANIM_DICT("MISSHEIST_JEWEL_SETUP")
				
				//Register entities with cutscene
				//Shop assistant
				IF NOT IS_PED_INJURED(shopAssistant)
					REGISTER_ENTITY_FOR_CUTSCENE(shopAssistant, "Jewellery_Assitance", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF					

				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					REGISTER_ENTITY_FOR_CUTSCENE(PLAYER_PED_ID(), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				ENDIF					
				
		      	//Configure script systems into a safe mode.
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				START_CUTSCENE()			
								
				iCutsceneStage ++			
			BREAK
			
			CASE 1
				
				//Set up exit state for the player
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
//					TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), << -631.4476, -237.4267, 37.0589 >>, PEDMOVEBLENDRATIO_WALK, -1)
//					TASK_FORCE_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
//					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-625.6043, -231.3043, 37.0570>>)
//					SET_ENTITY_HEADING(PLAYER_PED_ID(), 126.2749)	
//					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 2000) 
//    				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					IF VentHasBeenMentioned
					AND CamHasBeenMentioned
					AND AlarmHasBeenMentioned
						SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1000) 
        				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK) 
					ENDIF
				ENDIF	
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
					REPLAY_STOP_EVENT()
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jewellery_Assitance")
					IF NOT IS_PED_INJURED(shopAssistant)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(shopAssistant, TRUE)
						SET_ENTITY_COORDS(shopAssistant, <<-623.0729, -230.0248, 37.0570>>)
						SET_ENTITY_HEADING(shopAssistant, 130.6013)					
						TASK_PLAY_ANIM(shopAssistant, "MISSHEIST_JEWEL_SETUP", "idle_storeclerk", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
//						TASK_PLAY_ANIM(shopAssistant, "MISSHEIST_JEWEL_SETUP", "attendant_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING|AF_SECONDARY)
						SET_PED_KEEP_TASK(shopAssistant, TRUE)
					ENDIF
				ENDIF					
							
				IF IS_CUTSCENE_PLAYING()
					IF GET_CUTSCENE_TIME() > 48000
						DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
					ENDIF
				ENDIF
							
				IF NOT IS_CUTSCENE_ACTIVE()
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)								
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
					
					//Reset this timer here so Lester doesn't mention about the glasses straight away.
					iGlassesActivatedTimer = GET_GAME_TIMER()
					
					//Set Flags
					SpokeToStaffObjectiveDone = TRUE
					biscutsceneActive = FALSE	
					
				ELSE
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
				ENDIF
			
			BREAK
			
		ENDSWITCH
		
		WAIT(0)
	
	ENDWHILE
	
	
//	IF NOT DOES_CAM_EXIST(StaticCam)
//		StaticCam = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
//	ENDIF
//	
//	WHILE biscutsceneActive
//		
//		SWITCH iCutsceneStage
//			CASE 0 
//				
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//				SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-624.7785, -231.1901, 37.0570>>)
//				SET_ENTITY_HEADING(PLAYER_PED_ID(), 307.5722)
//				SET_CAM_PARAMS(StaticCam, <<-624.919373,-234.357590,39.807911>>,<<-23.170853,-0.000000,-9.687524>>,50.000000)
//				SET_CAM_ACTIVE(StaticCam, TRUE)
//				RENDER_SCRIPT_CAMS(TRUE, FALSE)
//				iCutsceneStage ++
//			
//			BREAK
//			
//			CASE 1
//				
//				KILL_ANY_CONVERSATION()
//				CLEAR_PRINTS()
//				PRINT_NOW("CUT1", DEFAULT_GOD_TEXT_TIME, 1)//~s~PLACEHOLDER - SMALL CUTSCENE WILL GO HERE TO SHOW MICHAEL GETTING INFO OFF THE SHOP ASSISTANT.
//				SETTIMERB(0)
//				iCutsceneStage ++
//
//			BREAK
//			
//			CASE 2
//			
//				IF TIMERB() > 2000
//					IF NOT IS_MESSAGE_BEING_DISPLAYED()
//						biscutsceneActive = FALSE
//						iCutsceneStage ++
//					ENDIF
//				ENDIF
//			
//			BREAK
//		ENDSWITCH
//		
//		WAIT(0)
//	
//	ENDWHILE
//	
//	IF biscutsceneActive = FALSE
//		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//		RENDER_SCRIPT_CAMS(FALSE, FALSE)
//		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//		SET_CAM_ACTIVE(StaticCam, TRUE)
//		DESTROY_CAM(StaticCam)
//		SpokeToStaffObjectiveDone = TRUE
//		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//	ENDIF

ENDPROC

//PURPOSE:Handles the main objective dialogue between Lester and Michael over earpiece.
PROC DIALOGUE_CONTROLLER()
	
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			//Do chat over earpiece while Michael heads towards the store.
			IF doneGoToStoreText = TRUE
				IF donePH1Chat = FALSE
					IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					AND NOT DOES_BLIP_EXIST(blipLocate)
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-659.6, -269.1, 37>>) > 27
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH1", CONV_PRIORITY_MEDIUM)
									//You reading me?
									//Mmhmm. 
									//Okay. We need shots of the security features - the alarm system, ventilation, cameras. 
									//Okay.
									donePH1Chat = TRUE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						//Do chat from Lester if Michael doesn't get out the car or drives around
						IF NOT DOES_BLIP_EXIST(blipLocate)
							IF IS_VEHICLE_DRIVEABLE(vehCar)
								IF NOT IS_PED_INJURED(pedContact)
									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
									AND IS_PED_IN_VEHICLE(pedContact, vehCar)
										IF NOT IS_VEHICLE_STOPPED(vehCar)
											IF NOT IS_MESSAGE_BEING_DISPLAYED()
											OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
												IF GET_GAME_TIMER() > (iAroundChatTimer + 5000)
													IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_AROUND", CONV_PRIORITY_MEDIUM)
														//You got to do this on foot.
														//I said, leave me here.
														//We don't want to arouse suspicion.
														//You got to get out and walk, Michael.
														iAroundChatTimer = GET_GAME_TIMER()
													ENDIF
												ENDIF
											ENDIF	
										ELSE
											IF NOT IS_MESSAGE_BEING_DISPLAYED()
											OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
												IF GET_GAME_TIMER() > (iAroundChatTimer + 5000)
													IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_NOGO", CONV_PRIORITY_MEDIUM)
														//You're up. Vinqueef.
														//I thought you wanted a score.
														//Go on. Case the joint.
														//You're needed in the jewel store.
														iAroundChatTimer = GET_GAME_TIMER()
													ENDIF
												ENDIF
											ENDIF	
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Have some random dialogue to tell the player to go into the store to take pics.
			IF PlayerIsInsideJewellers = FALSE
				IF playerTakingPhoto = TRUE
					IF GET_GAME_TIMER() > (iShotsBeforeStoreTimer + 5000)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH67", CONV_PRIORITY_MEDIUM)
									//Go inside the store Michael.
									//These shots are lovely but can we focus on the job.
									//What are you doing? Get inside the jewellers.
									//Come on Michael this store isn't going to rob itself.
									iShotsBeforeStoreTimer = GET_GAME_TIMER()
									SnapDialogueNeeded = FALSE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		//*********All the below dialogue should only happen when player is inside the store.**********
		//Handle the taking a picture if any pics are still required
		IF allpicsTaken = FALSE
			IF PlayerIsInsideJewellers = TRUE
				TAKING_PIC_CONTROLLER()	
			ENDIF
			IF VentObjectiveDone = TRUE
			AND SecurityCamObjectiveDone = TRUE
			AND alarmObjectiveDone = TRUE
			AND CamHasBeenMentioned = TRUE
			AND AlarmHasBeenMentioned = TRUE
			AND VentHasBeenMentioned = TRUE
				PRINTSTRING("AllpicsTaken being set to TRUE") PRINTNL()
				AllpicsDoneTimer = GET_GAME_TIMER()
				allpicsTaken = TRUE
			ENDIF
		ELSE
			//Allow the pic controller to keep running for a few seconds to finish off all dialogue 
			IF GET_GAME_TIMER() < (AllpicsDoneTimer+3000)
				TAKING_PIC_CONTROLLER()	
			ENDIF
		ENDIF
		//Have Lester point out what to take pics of if Michael already hasn't done so. This shouldn't start until the shop assistant has said her bit.
		IF PlayerIsInsideJewellers = TRUE
			
			//Do dialogue if Michael takes too long to activate the glasses.
			IF glassesHaveBeenActivated = FALSE
				IF glassesActivatedTimerStarted = FALSE
					iGlassesActivatedTimer = GET_GAME_TIMER()
					glassesActivatedTimerStarted = TRUE
				ELSE
					IF GET_GAME_TIMER() > (iGlassesActivatedTimer + 15000)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()				
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_ACTIV", CONV_PRIORITY_AMBIENT_MEDIUM)
									//Okay, activate the camera.
									//Maybe I should have included an instruction manual. Boot up the camera.
									//Are you comfortable using this equipment?
									glassesActivatedTimerStarted = FALSE
								ENDIF						
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
				
			
			//Do GOD TEXT now to tell player to use glasses to take pics
			IF doneGodText2 = FALSE
				IF NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					PRINT_NOW("GOD2", DEFAULT_GOD_TEXT_TIME, 1)//~s~Use the glasses to take pictures.
					doneGodText2 = TRUE
				ENDIF
			ENDIF			
			
//			//Have the shop assistant greet Michael when he gets close enough to her. 
//			IF SpokeToStaffObjectiveDone = FALSE
//				//Only allow this chat if player hasn't taken all the pics yet
//				IF VentObjectiveDone = FALSE
//				OR SecurityCamObjectiveDone = FALSE
//				OR alarmObjectiveDone = FALSE
//					IF DOES_ENTITY_EXIST(shopAssistant)
//						IF NOT IS_PED_INJURED(shopAssistant)
//							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), shopAssistant) < 4
//							AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-623.723633,-234.112167,36.557037>>, <<-627.036621,-229.516586,38.557037>>, 3.500000)
//								IF donePH15Chat = FALSE
//									ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, shopAssistant, "JewelSales")
//									IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH15", CONV_PRIORITY_AMBIENT_MEDIUM)
//										REQUEST_ANIM_DICT("MISSHEIST_JEWEL_SETUP")
//										IF HAS_ANIM_DICT_LOADED("MISSHEIST_JEWEL_SETUP")
//											TASK_PLAY_ANIM(shopAssistant, "MISSHEIST_JEWEL_SETUP", "attendant_dialogue", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
//										ENDIF
//										//Hello, sir. Are you just looking today? Or...
//										donePH15Chat = TRUE
//									ENDIF
//								ENDIF 
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
			
//			IF donePH15Chat = TRUE
//			AND IS_ENTITY_PLAYING_ANIM(shopAssistant, "MISSHEIST_JEWEL_SETUP", "attendant_dialogue")
//				AttendantPlayingSpeachAnim = TRUE
//			ENDIF
			
//			//Get the shop assistant to play the idle anim after she's played the dialogue one.
//			IF AssistantBackToIdle = FALSE
//				IF AttendantPlayingSpeachAnim = TRUE
//					IF NOT IS_ENTITY_PLAYING_ANIM(shopAssistant, "MISSHEIST_JEWEL_SETUP", "attendant_dialogue")
//						REQUEST_ANIM_DICT("MISSHEIST_JEWEL_SETUP")
//						IF HAS_ANIM_DICT_LOADED("MISSHEIST_JEWEL_SETUP")
//							TASK_PLAY_ANIM(shopAssistant, "MISSHEIST_JEWEL_SETUP", "attendant_idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
//							AssistantBackToIdle = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
			
			//First chat with guard saying hi as Michael enters store.
			IF NOT IS_PED_INJURED(shopGuard)
				IF donePH47Chat = FALSE
					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, shopGuard, "JewelGuard")
					IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH47", CONV_PRIORITY_MEDIUM)
						//Welcome sir.
						//Thanks
						donePH47Chat = TRUE
					ENDIF
				ENDIF 
			ENDIF			
			
			//Have some discriptive dialogue of the alarm keypad
			IF donePH47Chat = TRUE
				IF AlarmHasBeenMentioned = FALSE
				AND AlarmObjectiveDone = FALSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND NOT IS_MESSAGE_BEING_DISPLAYED()
						IF donePH3Chat = FALSE
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH3", CONV_PRIORITY_MEDIUM)
								//The alarm keypad is on the left when you come in. On the wall by the side door.
								donePH3Chat = TRUE
							ENDIF						
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Have a timer here to allow for final pic dialogue to play before running the PH8 dialogue.
			IF PH8ChatTimerStarted = FALSE
				IF CamHasBeenMentioned = TRUE
				AND AlarmHasBeenMentioned = TRUE
				AND VentHasBeenMentioned = TRUE	
				AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					StartPH8ChatTimer = GET_GAME_TIMER()
					PH8ChatTimerStarted = TRUE
				ENDIF
			ENDIF		
			
			IF PH8ChatTimerStarted = TRUE
				IF donePH8Chat = FALSE
					IF SpokeToStaffObjectiveDone = FALSE
					AND VentObjectiveDone = TRUE
					AND SecurityCamObjectiveDone = TRUE
					AND alarmObjectiveDone = TRUE
					AND CamHasBeenMentioned = TRUE
					AND AlarmHasBeenMentioned = TRUE
					AND VentHasBeenMentioned = TRUE
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF GET_GAME_TIMER() > (StartPH8ChatTimer + 1500)
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH8", CONV_PRIORITY_MEDIUM)
										//Good work. Now, speak to the assistant, and see if there's anything else we need to know. 
										iAllPicsTakenTimer = GET_GAME_TIMER()
										donePH8Chat = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Do some random lines of chat if player is taking too long to speak to the assistant
			IF donePH8Chat = TRUE
				IF GET_GAME_TIMER() > (iAllPicsTakenTimer + 20000) 
					IF SpokeToStaffObjectiveDone = FALSE
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_TALK", CONV_PRIORITY_MEDIUM)
									//They remember the window shoppers. Talk to her. 
									//Charm the woman, and you can get out of there.  
									//She might tell us something.  
									iAllPicsTakenTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Do god text now to tell player to use glasses to take pics
			IF doneGodText3 = FALSE
				IF donePH8Chat = TRUE
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
					AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					AND SpokeToStaffObjectiveDone = FALSE
						PRINT_NOW("GOD3", DEFAULT_GOD_TEXT_TIME, 1)//~s~Speak to the ~b~shop assistant.
						iGodText3Timer = GET_GAME_TIMER()
						doneGodText3 = TRUE
					ENDIF
					IF NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
						PRINT_NOW("GOD3", DEFAULT_GOD_TEXT_TIME, 1)//~s~Speak to the ~b~shop assistant.
						iGodText3Timer = GET_GAME_TIMER()
						doneGodText3 = TRUE
					ENDIF
				ENDIF
			ENDIF
		
			//Do dialogue once all objectives have been met
			IF donePH9Chat = FALSE
				IF AlarmHasBeenMentioned = TRUE
				AND VentHasBeenMentioned = TRUE
				AND CamHasBeenMentioned = TRUE
					IF SpokeToStaffObjectiveDone = TRUE
					AND VentObjectiveDone = TRUE
					AND SecurityCamObjectiveDone = TRUE
					AND alarmObjectiveDone = TRUE
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH9", CONV_PRIORITY_MEDIUM)
									//Come back to me.
									donePH9Chat = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		
			IF donePH11Chat = FALSE
			AND donePH12Chat = FALSE
				//Do dialogue if Michael has spoken to shop assistant but hasn't taken any pics yet
				IF SpokeToStaffObjectiveDone = TRUE
				AND VentObjectiveDone = FALSE
				AND SecurityCamObjectiveDone = FALSE
				AND alarmObjectiveDone = FALSE
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
					OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JSH1_CHATUP", CONV_PRIORITY_MEDIUM)
								//Okay. Now you've got that out the way, can you take the photographs?
								donePH11Chat = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				//Do dialogue if Michael has spoken to shop assistant and has taken some pics but not all.
				IF SpokeToStaffObjectiveDone = TRUE
					IF VentObjectiveDone = FALSE
					OR SecurityCamObjectiveDone = FALSE
					OR alarmObjectiveDone = FALSE
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_CHATUP2", CONV_PRIORITY_MEDIUM)
									//Fine. That was what it was, but you're not done taking photographs. 
									donePH12Chat = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Do some random dialogue if the player is inside the store and hasn't taken any correct pics yet. 
			IF glassesHaveBeenActivated = TRUE
				IF VentObjectiveDone = FALSE
				AND SecurityCamObjectiveDone = FALSE
				AND alarmObjectiveDone = FALSE
					IF GET_GAME_TIMER() > (iPicTakenTimer + 30000)
						PRINTSTRING("GET_GAME_TIMER() > iPicTakenTimer + 30000") PRINTNL()
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_AVC", CONV_PRIORITY_MEDIUM)
									//The alarm, the vents, and the cameras. Come on.
									//Alarm, vents, and cameras. Get me some shots.
									//Alarm, vents, and cameras remember. 
									iPicTakenTimer = GET_GAME_TIMER()
								ENDIF	
							ENDIF
						ENDIF
					ELSE
						PRINTSTRING("GET_GAME_TIMER() < iPicTakenTimer + 30000") PRINTNL()
						PRINTSTRING("GET_GAME_TIMER() = ") PRINTINT(GET_GAME_TIMER()) PRINTNL()
						PRINTSTRING("iPicTakenTimer = ") PRINTINT(iPicTakenTimer) PRINTNL()
					ENDIF
				ENDIF
			
				//Do some random dialogue if the player is inside the store and is taking too long to get the vents and cam.
				IF GET_GAME_TIMER() > (iPicTakenTimer + 30000)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF VentObjectiveDone = FALSE
							AND SecurityCamObjectiveDone = FALSE
							AND alarmObjectiveDone = TRUE
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH40", CONV_PRIORITY_MEDIUM)
									//Vents and cameras still required. 
									//Vents and cameras please. 
									//My checklist reads ventilation system and CCTV.
									iPicTakenTimer = GET_GAME_TIMER()
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Do some random dialogue if the player is inside the store and is taking too long to get the vents and Alarm.
				IF GET_GAME_TIMER() > (iPicTakenTimer + 30000)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF VentObjectiveDone = FALSE
							AND SecurityCamObjectiveDone = TRUE
							AND alarmObjectiveDone = FALSE
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH38", CONV_PRIORITY_MEDIUM)
									//Still need the alarm and the ventilation.
									//Alarm and ventilation, Michael.
									//Get me a shot of the alarm console, and a vent please. 
									iPicTakenTimer = GET_GAME_TIMER()
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Do some random dialogue if the player is inside the store and is taking too long to get the alarm and cams.
				IF GET_GAME_TIMER() > (iPicTakenTimer + 30000)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF VentObjectiveDone = TRUE
							AND SecurityCamObjectiveDone = FALSE
							AND alarmObjectiveDone = FALSE
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH39", CONV_PRIORITY_MEDIUM)
									//We've still got the alarm and the cameras on our checklist.
									//I'm waiting on a shot of the alarm and a shot of a camera.
									//How you getting on with the alarm and the security cameras?
									iPicTakenTimer = GET_GAME_TIMER()
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Do some random dialogue if the player is inside the store and is taking too long to get the alarm.
				IF GET_GAME_TIMER() > (iPicTakenTimer + 30000)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF VentObjectiveDone = TRUE
							AND SecurityCamObjectiveDone = TRUE
							AND alarmObjectiveDone = FALSE
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH37", CONV_PRIORITY_MEDIUM)
									//Now, all I needs a shot of the alarm keypad.
									//The keypad, and we're done.
									//Come on, we need a shot of the alarm console. 
									iPicTakenTimer = GET_GAME_TIMER()
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
		
				//Do some random dialogue if the player is inside the store and is taking too long to get the vents.
				IF GET_GAME_TIMER() > (iPicTakenTimer + 30000)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF VentObjectiveDone = FALSE
							AND SecurityCamObjectiveDone = TRUE
							AND alarmObjectiveDone = TRUE
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH36", CONV_PRIORITY_MEDIUM)
									//If you can get me a shot of the vents, we're done.
									//All I needs a shot of the vents.
									//If you can just get me a shot of the ventilation...
									iPicTakenTimer = GET_GAME_TIMER()
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
					
				//Do some random dialogue if the player is inside the store and is taking too long to get the cams.
				IF GET_GAME_TIMER() > (iPicTakenTimer + 30000)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF VentObjectiveDone = TRUE
							AND SecurityCamObjectiveDone = FALSE
							AND alarmObjectiveDone = TRUE
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH35", CONV_PRIORITY_MEDIUM)
									//Just the cameras left to photograph.
									//All we needs a closed circuit tv camera.
									//Any of those CCTV cameras'll do.
									iPicTakenTimer = GET_GAME_TIMER()
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
//				PRINTSTRING("glassesHaveBeenActivated = FALSE") PRINTNL()
			ENDIF
			
		ENDIF
		
ENDPROC

PROC GLASSES_CAM_CONTROLLER()
	
//	PRINTSTRING("fCamNewDirectionZCoord =  ") PRINTFLOAT(fCamNewDirectionZCoord) PRINTNL()	
//	PRINTSTRING("fMaxLeftDir =  ") PRINTFLOAT(fMaxLeftDir) PRINTNL()
//	PRINTSTRING("fMaxRightDir =  ") PRINTFLOAT(fMaxRightDir) PRINTNL()
	
//	IF AlarmHasBeenMentioned = FALSE
//		PRINTSTRING("AlarmHasBeenMentioned = FALSE") PRINTNL()
//	ENDIF
//	IF CamHasBeenMentioned = FALSE
//		PRINTSTRING("CamHasBeenMentioned = FALSE") PRINTNL()
//	ENDIF
//	IF VentHasBeenMentioned = FALSE
//		PRINTSTRING("VentHasBeenMentioned = FALSE") PRINTNL()
//	ENDIF

	// PC controls
	CONTROL_ACTION caTakePhotoInput = INPUT_FRONTEND_ACCEPT
	CONTROL_ACTION caExitGlassesInput = INPUT_FRONTEND_CANCEL
	
	IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
		caTakePhotoInput = INPUT_ATTACK
		caExitGlassesInput = INPUT_AIM
	ENDIF
	
	//Run audio for when glasses are active
	IF GlassesCamActive = TRUE
	AND camSetup = TRUE
		IF HAS_SOUND_FINISHED(iBackGroundSoundID)
			PLAY_SOUND_FRONTEND (iBackGroundSoundID, "Background_Sound", "Phone_SoundSet_Glasses_Cam")
		ENDIF	
	ELSE
//		IF iBackGroundSoundID != 0
//			STOP_SOUND(iBackGroundSoundID)
//			RELEASE_SOUND_ID(iBackGroundSoundID)
//			iBackGroundSoundID = 0
//		ENDIF
//		IF iCamZoomInSoundID != 0
//			STOP_SOUND(iCamZoomInSoundID)
//			RELEASE_SOUND_ID(iCamZoomInSoundID)
//			iCamZoomInSoundID = 0
//		ENDIF		
//		IF iCamZoomOutSoundID != 0
//			STOP_SOUND(iCamZoomOutSoundID)
//			RELEASE_SOUND_ID(iCamZoomOutSoundID)
//			iCamZoomOutSoundID = 0
//		ENDIF
//		IF iCamShootSoundID != 0
//			STOP_SOUND(iCamShootSoundID)
//			RELEASE_SOUND_ID(iCamShootSoundID)
//			iCamShootSoundID = 0
//		ENDIF
		STOP_SOUND(iCamZoomInSoundID)
		STOP_SOUND(iCamZoomOutSoundID)
		STOP_SOUND(iCamShootSoundID)
		STOP_SOUND(iBackGroundSoundID)
		
	ENDIF
	
	//Run this every frame to toggle the camera off and on.
	IF GET_GAME_TIMER() > iGlassesCamTimer + 1000
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)	//check for disabled input as we now disable player control
			IF GlassesCamActive = FALSE
				IF glassesHaveBeenActivated = FALSE
					iPicTakenTimer = GET_GAME_TIMER()
					glassesHaveBeenActivated = TRUE
				ENDIF				
				
				iGlassesCamTimer = GET_GAME_TIMER()
				fPlayerHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
				GlassesCamActive = TRUE
			ELSE
				iMeleeTimer = GET_GAME_TIMER()
				meleeNeedsDisabled = TRUE
				iGlassesCamTimer = GET_GAME_TIMER()
				GlassesCamActive = FALSE
			ENDIF
		ENDIF
		IF GlassesCamActive = TRUE
			IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, caExitGlassesInput)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, caExitGlassesInput)
			OR bMissionFailed = TRUE
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
				iMeleeTimer = GET_GAME_TIMER()
				meleeNeedsDisabled = TRUE
				iGlassesCamTimer = GET_GAME_TIMER()
				GlassesCamActive = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	//Turn camera on and off when requested
	IF GlassesCamActive = TRUE
		
		IF camSetup = FALSE
			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)			//disable player control and freeze the player, needed both for first person mode
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			
			//Add multihead lateral fade
			SET_MULTIHEAD_SAFE(TRUE,TRUE)
			
 			WHILE NOT HAS_SCALEFORM_MOVIE_LOADED (SF_Glasses_Shutter_Index)
				PRINTSTRING("Waiting for scaleform loading ") PRINTNL()
				WAIT(0)
      		ENDWHILE			
			
			IF fPlayerHeading < 181
				fCamDirectionZCoord = fPlayerHeading
			ELSE
				fCamDirectionZCoord = (fPlayerHeading - 360)
			ENDIF
			
			fCamNewDirectionZCoord = fCamDirectionZCoord
			fCamNewDirectionXCoord = 0
			iPlayerTakingPhotoTimer = GET_GAME_TIMER()
			
			f_current_cam_fov 			= 40
			f_scale_rotate_speed_zoom 	= 10
			f_rotate_speed 				= 60			
			
			fGlassesCamFOV = 40
			
			//for first person mode use the current gameplay camera fov
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
				fGlassesCamFOV 		= GET_GAMEPLAY_CAM_FOV()
				f_current_cam_fov 	= GET_GAMEPLAY_CAM_FOV()
			ENDIF
			
			vplayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			
			IF NOT DOES_CAM_EXIST(GlassesCamera)
				GlassesCamera = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
			ENDIF

			SET_CAM_COORD(GlassesCamera, <<vplayerCoords.x, vplayerCoords.y, (vplayerCoords.z + 0.6)>>)
			
			//for first person mode use the current gameplay cam rotation as start rotation of scripted camera
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
				SET_CAM_ROT(GlassesCamera, GET_GAMEPLAY_CAM_ROT())
			ELSE
				SET_CAM_ROT(GlassesCamera, <<0,0,fCamDirectionZCoord>>)
			ENDIF
			
			SET_CAM_FOV(GlassesCamera, fGlassesCamFOV)
			SET_CAM_ACTIVE(GlassesCamera, TRUE)
			
//			OVERLAY_SET_SECURITY_CAM(TRUE, FALSE, 10, 2)
			SET_TIMECYCLE_MODIFIER("secret_camera")
			
			OVERRIDE_MICROPHONE_SETTINGS(HASH("CELL_PHONE_CAMERA_MIC"), TRUE)
			
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			iCamZoomInSoundID 	= GET_SOUND_ID()
			iCamZoomOutSoundID	= GET_SOUND_ID()
			iCamShootSoundID	= GET_SOUND_ID()
			iBackGroundSoundID	= GET_SOUND_ID()
			
			DISABLE_CELLPHONE(TRUE)
			
			CLEAR_HELP()
			IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
				PRINT_HELP_FOREVER("HELP1_KM")//~s~Take Photo ~INPUT_AIM~~n~Zoom ~INPUT_SNIPER_ZOOM~~n~Move ~INPUTGROUP_LOOK~~n~Exit Camera Mode ~INPUT_CONTEXT~
			ELSE
				PRINT_HELP_FOREVER("HELP1")//~s~Take Photo ~INPUT_FRONTEND_ACCEPT~~n~Zoom ~INPUT_SNIPER_ZOOM~~n~Move ~INPUTGROUP_LOOK~~n~Exit Camera Mode ~INPUT_CONTEXT~
			ENDIF
			camSetup = TRUE
		ELSE
		
			ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
			
			DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)	
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
			DISABLE_SELECTOR_THIS_FRAME()
			
			IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			ENDIF
			
			IF missionStage = STAGE_SCOUT_OUT_STORE
				IF donePH23Chat = FALSE
				AND PlayerIsInsideJewellers = TRUE
					IF CamHasBeenSnapped = FALSE
					AND AlarmHasBeenSnapped = FALSE
					AND VentHasBeenSnapped = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH23", CONV_PRIORITY_HIGH)
									//Glasses are live. Shoot away.
									donePH23Chat = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
			
			UPDATE_GLASSES_CAM()			
			
			//Call these every frame to update the camera
//			OVERLAY_SET_SECURITY_CAM(TRUE, FALSE, 10, 2)			
			
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP1_KM")
						PRINTSTRING("HELP1_KM is not being displayed") PRINTNL()
						PRINT_HELP_FOREVER("HELP1_KM")//~s~Take Photo ~INPUT_ATTACK~~n~Zoom ~INPUT_SNIPER_ZOOM~~n~Move ~INPUTGROUP_LOOK~~n~Exit Camera Mode ~INPUT_CONTEXT~
					ELSE
						PRINTSTRING("HELP1_KM is being displayed") PRINTNL()
					ENDIF
				ELSE	
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP1")
						PRINTSTRING("HELP1 is not being displayed") PRINTNL()
						PRINT_HELP_FOREVER("HELP1")//~s~Take Photo ~INPUT_FRONTEND_ACCEPT~~n~Zoom ~INPUT_SNIPER_ZOOM~~n~Move ~INPUTGROUP_LOOK~~n~Exit Camera Mode ~INPUT_CONTEXT~
					ELSE
						PRINTSTRING("HELP1 is being displayed") PRINTNL()
					ENDIF
				ENDIF
			
	
		ENDIF
	
	ELSE
		
		IF meleeNeedsDisabled = TRUE
			IF GET_GAME_TIMER() < iMeleeTimer + 2000
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
			ELSE
				meleeNeedsDisabled = FALSE
			ENDIF
		ENDIF
		
		IF camSetup = TRUE
			//Disable multihead fade
			SET_MULTIHEAD_SAFE(FALSE,TRUE)
		
			CLEAR_TIMECYCLE_MODIFIER()
		
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
			
			//when exiting scripted camera in first person mode, set the player to match scripted camera rotation
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON
			
				VECTOR vGlassesCamRot =	GET_CAM_ROT(GlassesCamera)
			
				SET_ENTITY_ROTATION(PLAYER_PED_ID(), << 0.0, 0.0, vGlassesCamRot.z >>)
				
				SET_FIRST_PERSON_SHOOTER_CAMERA_HEADING(0.0)
				SET_FIRST_PERSON_SHOOTER_CAMERA_PITCH(vGlassesCamRot.x)
			ELSE
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			ENDIF
			
			DISPLAY_RADAR(TRUE)
			DISPLAY_HUD(TRUE)
			CLEAR_HELP()
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			SET_CAM_ACTIVE(GlassesCamera, FALSE)
			
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)			//enable player control
			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
			
			OVERRIDE_MICROPHONE_SETTINGS(0, FALSE)
			
			IF PlayerIsInsideJewellers = FALSE
				DISABLE_CELLPHONE(FALSE)
			ENDIF
			
			updatedGodText = FALSE
			DoneGlassesHelpText = FALSE
			camSetup = FALSE
		ENDIF
		
		IF missionStage = STAGE_SCOUT_OUT_STORE
			//Keep help text on screen if not all pics have been taken
			IF PlayerIsInsideJewellers = TRUE
				IF VentObjectiveDone = FALSE
				OR SecurityCamObjectiveDone = FALSE
				OR AlarmObjectiveDone = FALSE
					IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP2")
						CLEAR_HELP()
						PRINT_HELP_FOREVER("HELP2")//~s~Press ~INPUT_CONTEXT~ to enter glasses hidden camera.
						updatedGodText = TRUE
					ENDIF
				ENDIF
			ELSE
				//Do help text for glasses after the god text has been displayed to go to the store.
				IF doneGoToStoreText = TRUE
					IF doneFirstHelp = FALSE
					AND NOT IS_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP("HELP2")//~s~Press ~INPUT_CONTEXT~ to enter glasses hidden camera.
						doneFirstHelp = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF missionStage = STAGE_CHECK_OUT_ROOF
			IF playerInPerfectPlace = TRUE
			AND GoodShotDone = FALSE
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				IF DoneGlassesHelpText = FALSE
					CLEAR_HELP()
					PRINT_HELP_FOREVER("HELP2")//~s~Press ~INPUT_CONTEXT~ to enter glasses hidden camera.
					DoneGlassesHelpText = TRUE
				ENDIF
			ELSE
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HELP2")
					CLEAR_HELP()
					DoneGlassesHelpText = FALSE
				ENDIF
			ENDIF
		ENDIF
	
	ENDIF
	
	IF PlayerTakingPicsTooFast = TRUE
//		PRINTSTRING("PlayerTakingPicsTooFast = TRUE") PRINTNL()
	ELSE
//		PRINTSTRING("PlayerTakingPicsTooFast = FALSE") PRINTNL()
	ENDIF 
	
	//Control the shutter and any timers required for dialogue if player is taking a pic.
	IF GlassesCamActive = TRUE
		IF playerTakingPhoto = FALSE
			IF GET_GAME_TIMER() > (iPlayerTakingPhotoTimer + 500)
//				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPRINT)
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, caTakePhotoInput)
				OR IS_DISABLED_CONTROL_PRESSED(FRONTEND_CONTROL, caTakePhotoInput)
					//If the next pic taken is less than 4 seconds after the last pic then the player is taking pics too quickly for the dialogue, 
					//so dialogue should pause and wait till player has stopped taking so many pics and then play one line to say what the player just got in last few pics
					IF PicSpeedTimerStarted = TRUE
						IF GET_GAME_TIMER() < (iPicSpeedTimer + 4000)
							iPicSpeedTimer = GET_GAME_TIMER()
							PlayerTakingPicsTooFast = TRUE
						ELSE
							//If it's been over 3 seconds since last pic then we can reset the timer and dialogue should be able to play as normal
							iPicSpeedTimer = GET_GAME_TIMER()
							PlayerTakingPicsTooFast = FALSE
						ENDIF
					ENDIF					
					
					//Start a timer to control how quickly player is taking photos.
					IF PicSpeedTimerStarted = FALSE
						iPicSpeedTimer = GET_GAME_TIMER()
						PicSpeedTimerStarted = TRUE
					ENDIF
					
					IF missionStage = STAGE_SCOUT_OUT_STORE
						IF PlayerIsInsideJewellers = TRUE
							KILL_ANY_CONVERSATION()
						ENDIF
					ELSE
						KILL_ANY_CONVERSATION()
					ENDIF
					
					SETTIMERB(0)
					LEGACY_SCALEFORM_MOVIE_METHOD_WITH_NUMBER (SF_Glasses_Shutter_Index, "DISPLAY_VIEW", TO_FLOAT(2)) //Shutter screen.
					LEGACY_SCALEFORM_MOVIE_METHOD(SF_Glasses_Shutter_Index, "CLOSE_THEN_OPEN_SHUTTER") 
//					PLAY_SOUND_FRONTEND (-1, "Camera_Shoot", "Phone_SoundSet_Default")
					PLAY_SOUND_FRONTEND (iCamShootSoundID, "Camera_Shoot", "Phone_SoundSet_Glasses_Cam")
					iPicTakenTimer = GET_GAME_TIMER()
					
					IF missionStage = STAGE_SCOUT_OUT_STORE
						IF PlayerIsInsideJewellers = TRUE
							SnapDialogueNeeded = TRUE
						ENDIF
					ELSE
						SnapDialogueNeeded = TRUE
					ENDIF
					
					//Add stat here for number of photo's taken
					iPhotosTaken++
					INFORM_MISSION_STATS_OF_INCREMENT(JH1_PHOTOS_TAKEN, iPhotosTaken, TRUE)					
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					
					playerTakingPhoto = TRUE
					
					PhotoJustTaken = TRUE
				ENDIF
			ENDIF
		ENDIF
	
		IF playerTakingPhoto = TRUE
			DRAW_SCALEFORM_MOVIE_FULLSCREEN(SF_Glasses_Shutter_Index,255,255,255,0)
			IF TIMERB() > 1500
				iPlayerTakingPhotoTimer = GET_GAME_TIMER()
				playerTakingPhoto = FALSE
			ENDIF
		ENDIF
	ELSE
		IF playerTakingPhoto = TRUE
			IF TIMERB() > 1500
				iPlayerTakingPhotoTimer = GET_GAME_TIMER()
				playerTakingPhoto = FALSE
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Handles the stage where Michael goes into scout out the store.
PROC DO_STAGE_SCOUT_OUT_STORE()

	//Anything that should be run every frame this stage
	//Handle ShopBlip
	//DEBUG ONLY TO BE REMOVED WHEN SCRIPT IS FINISHED
//	#IF IS_DEBUG_BUILD
//		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
//			donePH1Chat = TRUE
//			SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-632.8737, -238.7616, 37.0846>>)
//			SET_ENTITY_HEADING(PLAYER_PED_ID(), 298.5876)
//		ENDIF
//	#ENDIF
	
	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(player_id())
	
	SWEATSHOP_AMBIENT_CONTROLLER()
	
	JEWEL_STORE_AMBIENT_CONTROLLER()
	
	JEWEL_STORE_STAFF_CONTROLLER()
	
	IF PlayerIsInsideJewellers = FALSE
		IF GET_GAME_TIMER() > (iSaveHouseCheck + 5000)
			IF playerInSafeHouse = FALSE
				IF IS_PLAYER_IN_SAVEHOUSE()
					playerInSafeHouse = TRUE
				ENDIF
				iSaveHouseCheck = GET_GAME_TIMER()
			ENDIF
			IF playerInSafeHouse = TRUE
				IF NOT IS_PLAYER_IN_SAVEHOUSE()
					playerInSafeHouse = FALSE
				ENDIF
				iSaveHouseCheck = GET_GAME_TIMER()
			ENDIF
		ENDIF
	ENDIF
	
	IF iControlFlag > 0
		IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		AND NOT IS_PLAYER_IN_ANY_SHOP()
		AND NOT IS_PED_JUMPING(PLAYER_PED_ID())
		AND NOT IS_PED_CLIMBING(PLAYER_PED_ID())
		AND NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())
		AND NOT playerInSafeHouse
		AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
		AND bMissionFailed = FALSE
			GLASSES_CAM_CONTROLLER()
		ELSE
			IF GlassesCamActive = TRUE
				GLASSES_CAM_CONTROLLER()
			ENDIF
		ENDIF
	ENDIF
	
	IF PlayerIsInsideJewellers = TRUE
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)	
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
	  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
	    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
	ENDIF
	
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-631.130676,-237.373398,36.556953>>, <<-617.120361,-227.004135,39.915310>>, 17.750000)	
		PlayerIsInsideJewellers = TRUE
	ELSE
		PlayerIsInsideJewellers = FALSE
	ENDIF
	
	IF iControlFlag < 2		
		
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			//Draw locates for these 2 blips when active
//			IF DOES_BLIP_EXIST(shopBlip)
//				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -631.4476, -237.4267, 37.09479>>, <<2, 2, LOCATE_SIZE_HEIGHT>>, TRUE)
//				ENDIF
//			ENDIF
			IF DOES_BLIP_EXIST(blipLocate)
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<vParkingSpace.x, vParkingSpace.y, 34.97223>>, <<2.5,2.5,LOCATE_SIZE_HEIGHT>>, TRUE)
				ENDIF
			ENDIF
		
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-631.130676,-237.373398,36.556953>>, <<-617.120361,-227.004135,39.915310>>, 17.750000)	
			AND NOT DOES_BLIP_EXIST(BlipVeh)
			AND NOT DOES_BLIP_EXIST(blipLocate)
				PlayerIsInsideJewellers = TRUE
				donePH10Chat = FALSE
				
				//Handle audio inside the store and when using the glasses.
				IF IS_AUDIO_SCENE_ACTIVE("JSH_1_ENTER_STORE")
					STOP_AUDIO_SCENE("JSH_1_ENTER_STORE")
				ENDIF
				IF GlassesCamActive = FALSE
					IF IS_AUDIO_SCENE_ACTIVE("JSH_1_TAKE_PHOTOS")
						STOP_AUDIO_SCENE("JSH_1_TAKE_PHOTOS")
					ENDIF
					IF NOT IS_AUDIO_SCENE_ACTIVE("JSH_1_INSIDE_STORE")
						START_AUDIO_SCENE("JSH_1_INSIDE_STORE")
					ENDIF
				ELSE
					IF IS_AUDIO_SCENE_ACTIVE("JSH_1_INSIDE_STORE")
						STOP_AUDIO_SCENE("JSH_1_INSIDE_STORE")
					ENDIF
					//Audio to be switched on when using glasses
					IF NOT IS_AUDIO_SCENE_ACTIVE("JSH_1_TAKE_PHOTOS")
						START_AUDIO_SCENE("JSH_1_TAKE_PHOTOS")
					ENDIF
				ENDIF
				
				IF DOES_BLIP_EXIST(shopBlip)
					REMOVE_BLIP(shopBlip)
				ENDIF
				
				//Handle shop assistant blip once all the pics have been taken.
				IF VentObjectiveDone
				AND SecurityCamObjectiveDone
				AND AlarmObjectiveDone
					IF SpokeToStaffObjectiveDone = FALSE
						IF donePH8Chat = TRUE
							IF NOT IS_PED_INJURED(shopAssistant)
								IF NOT DOES_BLIP_EXIST(shopAssistantBlip)
									
									REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
									
									shopAssistantBlip = CREATE_BLIP_FOR_ENTITY(shopAssistant)
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(shopAssistantBlip)
							REMOVE_BLIP(shopAssistantBlip)
						ENDIF	
					ENDIF
				ENDIF			
			ELSE
				PlayerIsInsideJewellers = FALSE
				IF IS_AUDIO_SCENE_ACTIVE("JSH_1_INSIDE_STORE")
					STOP_AUDIO_SCENE("JSH_1_INSIDE_STORE")
				ENDIF
				IF DOES_ENTITY_EXIST(vehCar)
					IF IS_VEHICLE_DRIVEABLE(vehCar)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
//							IF IS_ENTITY_AT_COORD(vehCar, <<vParkingSpace.x, vParkingSpace.y, 34.97223>>, <<7, 7, 4>>)
							IF IS_ENTITY_IN_ANGLED_AREA(vehCar, <<-665.884460,-259.601532,34.244663>>, <<-652.584473,-283.042877,38.005383>>, 11.000000)
								IF DOES_BLIP_EXIST(shopAssistantBlip)
									IF doneGodText3 = TRUE
									AND GET_GAME_TIMER() < (iGodText3Timer + DEFAULT_GOD_TEXT_TIME)
										CLEAR_PRINTS()
									ENDIF
									REMOVE_BLIP(shopAssistantBlip)
								ENDIF
								IF DOES_BLIP_EXIST(blipLocate)
									REMOVE_BLIP(blipLocate)
								ENDIF
								IF DOES_BLIP_EXIST(BlipVeh)
									REMOVE_BLIP(BlipVeh)
								ENDIF
								IF NOT DOES_BLIP_EXIST(shopBlip)
									CLEAR_HELP()
									shopBlip = CREATE_BLIP_FOR_COORD(<< -631.4476, -237.4267, 37.0589 >>)
								ENDIF
							
								//Do dialogue of Lester telling Michael to get back into the store.
//								IF michaelInRightClothes = TRUE
									IF updatedGodText = TRUE
										IF donePH10Chat = FALSE
											KILL_ANY_CONVERSATION()
											IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH10", CONV_PRIORITY_MEDIUM)
												//Where you going? You're not done yet. 
												//Go back into the shop.
												//You've still got work to do in the jewel store.
												//We're not finished. Get back in there.
												SETTIMERB(0)
												donePH10Chat = TRUE
											ENDIF	
										ENDIF
									ENDIF
//								ENDIF
							
								//Check timer so dialogue is repeated if player doesnt go back inside shop
								IF TIMERB() > 15000
									donePH10Chat = FALSE
								ENDIF
							ELSE
								IF DOES_BLIP_EXIST(shopAssistantBlip)
									REMOVE_BLIP(shopAssistantBlip)
								ENDIF
								IF DOES_BLIP_EXIST(blipLocate)
									REMOVE_BLIP(blipLocate)
								ENDIF	
								IF DOES_BLIP_EXIST(shopBlip)
									REMOVE_BLIP(shopBlip)
								ENDIF
								IF NOT DOES_BLIP_EXIST(BlipVeh)
									BlipVeh = CREATE_BLIP_FOR_VEHICLE(vehCar)
									IF doneGetBackInCarText = FALSE
										PRINT_NOW("GOTO_CAR3", DEFAULT_GOD_TEXT_TIME, 1)//~s~Get back in your ~b~car.
										iBackInCarTimer = GET_GAME_TIMER()
										doneGetBackInCarText = TRUE
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF DOES_BLIP_EXIST(shopAssistantBlip)
								REMOVE_BLIP(shopAssistantBlip)
							ENDIF
							IF DOES_BLIP_EXIST(BlipVeh)
								REMOVE_BLIP(BlipVeh)
								IF doneGetBackInCarText = TRUE
									IF GET_GAME_TIMER() < (iBackInCarTimer + DEFAULT_GOD_TEXT_TIME)
										CLEAR_PRINTS()
									ENDIF
								ENDIF
							ENDIF
//							IF IS_ENTITY_AT_COORD(vehCar, <<vParkingSpace.x, vParkingSpace.y, 34.97223>>, <<7, 7, 4>>)
							IF IS_ENTITY_IN_ANGLED_AREA(vehCar, <<-665.884460,-259.601532,34.244663>>, <<-652.584473,-283.042877,38.005383>>, 11.000000)
								IF IS_ENTITY_IN_ANGLED_AREA(vehCar, <<-661.583679,-267.478821,34.398075>>, <<-657.259460,-274.933685,37.457153>>, 6.250000)
									IF DOES_BLIP_EXIST(blipLocate)
										REMOVE_BLIP(blipLocate)
									ENDIF 
									IF NOT DOES_BLIP_EXIST(shopBlip)
										shopBlip = CREATE_BLIP_FOR_COORD(<< -631.4476, -237.4267, 37.0589 >>)
									ENDIF
								ENDIF
							ELSE
								IF NOT DOES_BLIP_EXIST(blipLocate)
									blipLocate = CREATE_BLIP_FOR_COORD(vParkingSpace, TRUE)
								ENDIF	
								IF DOES_BLIP_EXIST(shopBlip)
									REMOVE_BLIP(shopBlip)
								ENDIF
								//If the parking blip exists then the player is fucking about and should have dialogue from Lester telling him to get back to the parking space
//								IF michaelInRightClothes = TRUE
									IF donchat72 = FALSE	
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
											IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH72", CONV_PRIORITY_MEDIUM)
												//Where are you going? We need to case out the store.
												//Park back up in that space and check out the jewellers Michael.
												//We don't have time for this shit.
												iChat72Timer = GET_GAME_TIMER()
												donchat72 = TRUE
											ENDIF
										ENDIF	
									ELSE
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
											IF GET_GAME_TIMER() > (iChat72Timer + 7000)
												IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH72", CONV_PRIORITY_MEDIUM)
													//Where are you going? We need to case out the store.
													//Park back up in that space and check out the jewellers Michael.
													//We don't have time for this shit.
													iChat72Timer = GET_GAME_TIMER()
												ENDIF
											ENDIF
										ENDIF	
									ENDIF
//								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-631.130676,-237.373398,36.556953>>, <<-617.120361,-227.004135,39.915310>>, 17.750000)	
				IF DOES_BLIP_EXIST(shopAssistantBlip)
					REMOVE_BLIP(shopAssistantBlip)
				ENDIF	
				IF DOES_BLIP_EXIST(shopBlip)
					REMOVE_BLIP(shopBlip)
				ENDIF
				IF DOES_BLIP_EXIST(blipLocate)
					REMOVE_BLIP(blipLocate)
				ENDIF 
				IF doneLoseWantedText = FALSE
					CLEAR_PRINTS()
					PRINT_NOW("WANTED", DEFAULT_GOD_TEXT_TIME, 1)//~s~Lose your wanted level.
					doneLoseWantedText = TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Set the player to only be able to walk while in the store.
	//Dont allow player to run if in this area.
	IF PlayerIsInsideJewellers = TRUE
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
		IF hasPhoneBeenDisabled = FALSE
			DISABLE_CELLPHONE(TRUE)
			hasPhoneBeenDisabled = TRUE
		ENDIF
	ELSE
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-641.029663,-233.201431,35.846058>>, <<-630.599304,-247.389801,39.151962>>, 12.000000)
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_UD)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_LR)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)	
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_AIM)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK2)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
		  	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
			IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
				SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
			ENDIF
			IF DOES_ENTITY_EXIST(shopGuard)
				IF NOT IS_PED_INJURED(shopGuard)
					IF GET_DISTANCE_BETWEEN_ENTITIES(shopGuard, PLAYER_PED_ID()) < 4
						SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
					ELSE
						IF GET_ENTITY_HEADING(PLAYER_PED_ID()) > 49.2682
						AND GET_ENTITY_HEADING(PLAYER_PED_ID()) < 191.6326
//							PRINTSTRING("Player should be ok to run now ") PRINTNL()
						ELSE
							SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF hasPhoneBeenDisabled = TRUE
			DISABLE_CELLPHONE(FALSE)
			hasPhoneBeenDisabled = FALSE
		ENDIF
	ENDIF
	
	IF iControlFlag = 0
	
				//**************SET MID MISSION CHECK POINT HERE*******************//
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "STAGE_SCOUT_OUT_STORE")
				PRINTSTRING("***********MID MISSION STAGE SET TO 1***********") PRINTNL()	

		IF MissionStageBeingSkippedTo = TRUE
			STAGE_SELECTOR_MISSION_SETUP()
			MissionStageBeingSkippedTo = FALSE
		ENDIF		
	
		//Flags
		updatedGodText 					= FALSE
		SecurityCamObjectiveDone 		= FALSE
		VentObjectiveDone 				= FALSE
		SpokeToStaffObjectiveDone 		= FALSE
		GlassesCamActive 				= FALSE
		AlarmObjectiveDone 				= FALSE
		camSetup	 					= FALSE
		playerTakingPhoto 				= FALSE
		AllBallsCreated 				= FALSE
		updatedGodText2 				= FALSE
		doneHintCam 					= FALSE
		donePH1Chat 					= FALSE
		donePH8Chat						= FALSE
		donePH9Chat						= FALSE
		donePH10Chat 					= FALSE
		donePH11Chat 					= FALSE
		donePH12Chat 					= FALSE
		doneTOCARChat 					= FALSE
//		donePH14Chat 					= FALSE
		donePH16Chat 					= FALSE
		donePH17Chat 					= FALSE
		donePH18Chat 					= FALSE
		donePH19Chat 					= FALSE
		donePH22Chat 					= FALSE
		donePH20Chat 					= FALSE
		donePH21Chat 					= FALSE
		donePH24Chat 					= FALSE
		donePH25Chat					= FALSE
		donePH26Chat 					= FALSE
		SnapDialogueNeeded				= FALSE
		doneGodText2 					= FALSE
		doneGodText3 					= FALSE

		donePH28Chat 					= FALSE
		donePH29Chat 					= FALSE
		CanMissionFail					= TRUE
		GunHolstered 					= FALSE
		PH8ChatTimerStarted 			= FALSE
		doneGetBackInCarText 			= FALSE
		player_coming_from_left			= FALSE
		player_coming_from_right		= FALSE
		donePH41Chat 					= FALSE
		MusicStarted[1] 				= FALSE
		timerstartedForRooftopEntranceBlip = FALSE
		donePH47Chat					= FALSE
		meleeNeedsDisabled 				= FALSE
		modelsRequested 				= FALSE
		hasPhoneBeenDisabled 			= FALSE
		JewelStoreModelsRequested 		= FALSE
		customerReadyToMoveOn			= FALSE
		ShopCustomerFrozen 				= FALSE
		ShopCustomerReadyToLeave		= FALSE
		playerTookTooLong				= FALSE
		donePH13Chat					= FALSE
//		DoneDriveChat					= FALSE
		glassesHaveBeenActivated		= FALSE
		glassesActivatedTimerStarted 	= FALSE
		isCamZoomInSoundPlaying 		= FALSE
		isCamZoomOutSoundPlaying 		= FALSE
		doneGoToStoreText 				= FALSE
		donePH23Chat 					= FALSE
		donePH3Chat						= FALSE
		hasCutsceneBeenRequested 		= FALSE
		CutSceneGoodToStart	 			= FALSE
		allpicsTaken 					= FALSE
		PlayerTakingPicsTooFast 		= FALSE
		PicSpeedTimerStarted 			= FALSE
		AlarmHasBeenSnapped				= FALSE
		CamHasBeenSnapped				= FALSE
		VentHasBeenSnapped				= FALSE
		AlarmHasBeenMentioned 			= FALSE
		CamHasBeenMentioned 			= FALSE
		VentHasBeenMentioned 			= FALSE
		donePH6Chat 					= FALSE
		donePH5Chat						= FALSE
		doneChatPH32 					= FALSE
		doneFirstHelp 					= FALSE
		doneReturnText 					= FALSE
		donchat72 						= FALSE
		doneLoseWantedText 				= FALSE
		playerInSafeHouse 				= FALSE
		statPicturePerfectSet 			= FALSE
		ACamIsOnScreen					= FALSE
		AVentIsOnScreen					= FALSE
		AnAlarmIsOnScreen				= FALSE
		AlarmIsOnScreen[0]				= FALSE
		AlarmIsOnScreen[1]				= FALSE
		CamIsOnScreen[0]				= FALSE
		CamIsOnScreen[1]				= FALSE
		CamIsOnScreen[2]				= FALSE
		CamIsOnScreen[3]				= FALSE
		VentIsOnScreen[1]				= FALSE
		VentIsOnScreen[2]				= FALSE
		VentIsOnScreen[3]				= FALSE
//		SyncdSceneDisrupted 			= FALSE	
		
		FOR icount = 0 TO 5
			ShopCustomerTaskGiven[icount] = FALSE	
		ENDFOR
		
		iAroundChatTimer 				= GET_GAME_TIMER()
		iGlassesCamTimer 				= GET_GAME_TIMER()
		iShotsBeforeStoreTimer 			= GET_GAME_TIMER()
		iSaveHouseCheck 				= GET_GAME_TIMER()
		iPlayerNotApproachingTimer 		= 0
		iChat72Timer					= 0
		iHurryUpTimer					= 0
		
		#IF IS_DEBUG_BUILD
			jSkipReady = TRUE
			pSkipReady = TRUE
		#ENDIF
		
		REQUEST_MODEL(PROP_BOWLING_BALL)
		REQUEST_WAYPOINT_RECORDING("BB_JEW_1")
		REQUEST_WAYPOINT_RECORDING("BB_JEW_2")
		REQUEST_MISSION_AUDIO_BANK("JWL_HEIST_SETUP")
        SF_Glasses_Shutter_Index = REQUEST_SCALEFORM_MOVIE ("camera_gallery")
		
		LOAD_OVERLAY_EFFECTS()  // security camera overlay scaleform
		
//		ePlayersGlasses = GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_HEAD)
		
		SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P0_PROGRAMMER_GLASSES, FALSE)		
		
		IF NOT IS_PED_INJURED(pedContact)
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 1, pedContact, "LESTER")
		ENDIF
		IF NOT IS_PED_INJURED(shopGuard)
			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, shopGuard, "JewelGuard")
		ENDIF
		ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 0, PLAYER_PED_ID(), "MICHAEL")
		
		CLEAR_AREA(<<-625.6, -233.1, 38>>, 20, TRUE)
		SET_PED_NON_CREATION_AREA(<<-627.5, -237.6, 35.9>>, <<-619.6, -223.3, 38.8>>)
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ScenarioBlocking[0] = ADD_SCENARIO_BLOCKING_AREA(<<-614.0, -215.6, 39.8>>, <<-636.7, -242.3, 35.8>>)
		
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			END_REPLAY_SETUP(vehCar)
		ENDIF
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF		
		
		IF IS_AUDIO_SCENE_ACTIVE("JSH_1_DRIVE_TO_STORE")
			STOP_AUDIO_SCENE("JSH_1_DRIVE_TO_STORE")
		ENDIF

		IF NOT IS_AUDIO_SCENE_ACTIVE("JSH_1_ENTER_STORE")
			START_AUDIO_SCENE("JSH_1_ENTER_STORE")
		ENDIF

		iControlFlag = 1
		
	ENDIF
	
	IF iControlFlag = 1
		
		//Request the mocap scene once the shop assistant has been created in script to pass on her components to the scene.
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-631.3, -239, 37.4>>) < DEFAULT_CUTSCENE_LOAD_DIST
			IF hasCutsceneBeenRequested = FALSE
				IF DOES_ENTITY_EXIST(shopAssistant)
					IF NOT IS_PED_INJURED(shopAssistant)
						REQUEST_CUTSCENE("JH_1_IG_3")
						hasCutsceneBeenRequested = TRUE
					ENDIF
				ENDIF
			ELSE
				IF NOT HAS_CUTSCENE_LOADED()
					IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Jewellery_Assitance", shopAssistant, CS_JEWELASS)
						PRINTSTRING("SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED being set") PRINTNL()
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-631.3, -239, 37.4>>) > DEFAULT_CUTSCENE_UNLOAD_DIST
				IF hasCutsceneBeenRequested = TRUE
					REMOVE_CUTSCENE()
					hasCutsceneBeenRequested = FALSE
				ENDIF
			ENDIF
		ENDIF
			
		IF doneGoToStoreText = FALSE
			IF IS_SCREEN_FADED_IN()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					PRINT_NOW("GOTO_STORE", 4000, 1) //~s~Go to the ~y~jewelers.
					doneGoToStoreText = TRUE
				ENDIF
			ENDIF
		ENDIF
		
//		PRINTSTRING("Offset from player is ") PRINTVECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PLAYER_PED_ID(), GET_CAM_COORD(GET_DEBUG_CAM()))) PRINTNL()	
		IF AllBallsCreated = FALSE
			CREATE_INVISIBLE_BALLS()	
		ENDIF
		
		//Only allow glasses to be used once player is inside the store and god text has instructed player to take photo's using the glasses.
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-631.130676,-237.373398,36.556953>>, <<-617.120361,-227.004135,39.915310>>, 17.750000)	
			IF MusicStarted[1] = FALSE
				TRIGGER_MUSIC_EVENT("JH1_STORE")
				MusicStarted[1] = TRUE
			ENDIF	
		ENDIF
		
		//Only allow this stage to work if these 2 blips are not active
		IF NOT DOES_BLIP_EXIST(BlipVeh)
		AND NOT DOES_BLIP_EXIST(blipLocate)	
//		AND michaelInRightClothes = TRUE
			DIALOGUE_CONTROLLER()
			
			IF SpokeToStaffObjectiveDone = FALSE
				IF GlassesCamActive = FALSE
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-625.783630,-230.236588,37.057037>>, <<-624.559448,-231.977615,39.057037>>, 2.750000)
					OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-624.248291,-231.488724,36.557037>>, <<-623.502380,-232.287186,38.807037>>, 1.000000)
						KILL_ANY_CONVERSATION()
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF DOES_BLIP_EXIST(shopAssistantBlip)
								REMOVE_BLIP(shopAssistantBlip)
							ENDIF				
							CutSceneGoodToStart = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF CutSceneGoodToStart = TRUE
			AND SpokeToStaffObjectiveDone = FALSE
			AND OfficeFail = FALSE
			AND CounterFail = FALSE
				//Do cutscene of Michael speaking to shop assistant.
				DO_CUTSCENE_OF_MICHAEL_AND_SHOP_ASSISTANT()	
			ENDIF
			
			//Move stage on once all 3 objectives are complete	
			IF GlassesCamActive = FALSE
				IF SecurityCamObjectiveDone 	= TRUE
				AND VentObjectiveDone 			= TRUE
				AND AlarmObjectiveDone 			= TRUE
				AND SpokeToStaffObjectiveDone 	= TRUE
				AND donePH9Chat 				= TRUE
					//Delete all the bowling balls now
					FOR icount = 0 TO 3
						IF DOES_ENTITY_EXIST(ObjBallCam[icount])
							DELETE_OBJECT(ObjBallCam[icount])
						ENDIF
					ENDFOR
					FOR icount = 0 TO 3
						IF DOES_ENTITY_EXIST(ObjBallVent[icount])
							DELETE_OBJECT(ObjBallVent[icount])
						ENDIF
					ENDFOR
					FOR icount = 0 TO 1
						IF DOES_ENTITY_EXIST(ObjBallAlarm[icount])
							DELETE_OBJECT(ObjBallAlarm[icount])
						ENDIF	
					ENDFOR
					//Destroy all tracked points
					IF trackedPointsCreated = TRUE
						DESTROY_TRACKED_POINT(iTrackedAlarmPoint[0])
						DESTROY_TRACKED_POINT(iTrackedAlarmPoint[1])
						DESTROY_TRACKED_POINT(iTrackedVentPoint[1])
						DESTROY_TRACKED_POINT(iTrackedVentPoint[2])
						DESTROY_TRACKED_POINT(iTrackedVentPoint[3])
						DESTROY_TRACKED_POINT(iTrackedCamPoint[0])
						DESTROY_TRACKED_POINT(iTrackedCamPoint[1])
						DESTROY_TRACKED_POINT(iTrackedCamPoint[2])
						DESTROY_TRACKED_POINT(iTrackedCamPoint[3])
						trackedPointsCreated = FALSE
					ENDIF
					iControlFlag = 2
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF
		
	IF iControlFlag = 2
//		PRINTSTRING("All objectives are comeplete, mission can move on now") PRINTNL()
		IF DOES_BLIP_EXIST(shopBlip)
			REMOVE_BLIP(shopBlip)
		ENDIF		
		
		//Do some dialogue if the player is leaving the store
		IF donePH29Chat = FALSE	
			IF NOT IS_PED_INJURED(shopGuard)
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-630.4255, -236.7872, 37.0570>>, <<2,2,2>>)
					IF GET_ENTITY_HEADING(PLAYER_PED_ID()) < 170
					AND GET_ENTITY_HEADING(PLAYER_PED_ID()) > 50
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, shopGuard, "JewelGuard")
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH29", CONV_PRIORITY_MEDIUM)
								//Thank you sir.
								donePH29Chat = TRUE
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
		
		IF donePH9Chat = TRUE
			IF IS_VEHICLE_DRIVEABLE(vehCar)
				IF NOT DOES_BLIP_EXIST(BlipVeh)
					BlipVeh = CREATE_BLIP_FOR_VEHICLE(vehCar)
				ENDIF
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
						PRINT_NOW("GOTO_CAR4", DEFAULT_GOD_TEXT_TIME, 1)//~s~Get back in your ~b~car.
						ScenarioBlocking[2] = ADD_SCENARIO_BLOCKING_AREA(<<-603.5, -271.1, 44.2>>, <<-566.5, -298.7, 31.4>>)
						
						SET_ROADS_IN_AREA(<<-611.9, -362.4, 30>>, <<-558.6, -264.5, 45>>, FALSE)
						SET_ROADS_IN_AREA(<<-538.9, -296.7, 43.3>>, <<-631.3, -185.2, 30>>, FALSE)
						iHurryUpTimer = GET_GAME_TIMER()
						iControlFlag = 3
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF 
	
	IF iControlFlag = 3
		
		//Do some dialogue if the player is leaving the store
		IF donePH29Chat = FALSE	
			IF NOT IS_PED_INJURED(shopGuard)
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-630.4255, -236.7872, 37.0570>>, <<2,2,2>>)
					IF GET_ENTITY_HEADING(PLAYER_PED_ID()) < 170
					AND GET_ENTITY_HEADING(PLAYER_PED_ID()) > 50
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 4, shopGuard, "JewelGuard")
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH29", CONV_PRIORITY_MEDIUM)
								REPLAY_RECORD_BACK_FOR_TIME(3.0, 7.0, REPLAY_IMPORTANCE_HIGH)
								//Thank you sir.
								donePH29Chat = TRUE
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF		
		
		IF DOES_ENTITY_EXIST(pedContact)
			IF NOT IS_PED_INJURED(pedContact)
				IF GET_GAME_TIMER() > (iHurryUpTimer + 20000)
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedContact) > 15
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()		
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH48", CONV_PRIORITY_MEDIUM)
								//Come on, Michael. Let's go.
								//Let's hurry this up, Michael.
								//If you take any longer, you're risking blowing this whole thing.
								//Let's speed this up, we don't want to draw attention.
								iHurryUpTimer = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF GET_GAME_TIMER() > (iHurryUpTimer + 30000)
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()		
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH48", CONV_PRIORITY_MEDIUM)
									//Come on, Michael. Let's go.
									//Let's hurry this up, Michael.
									//If you take any longer, you're risking blowing this whole thing.
									//Let's speed this up, we don't want to draw attention.
									iHurryUpTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-631.130676,-237.373398,36.556953>>, <<-617.120361,-227.004135,39.915310>>, 17.750000)
			IF IS_AUDIO_SCENE_ACTIVE("JSH_1_INSIDE_STORE")
				STOP_AUDIO_SCENE("JSH_1_INSIDE_STORE")
			ENDIF			
			//Do some more dialogue over earpiece as Michael is going back to the car.
			IF doneTOCARChat = FALSE
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_TOCAR", CONV_PRIORITY_MEDIUM)
						REPLAY_RECORD_BACK_FOR_TIME(2.0, 6.0, REPLAY_IMPORTANCE_HIGH)
						//So, we good?
						//Almost. I need to get eyes on the roof of the building. See where the ventilation comes out.
						doneTOCARChat = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehCar)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
				
				//Clean up previous stage
				IF DOES_ENTITY_EXIST(ShopCustomer[0])
					IF NOT IS_PED_INJURED(ShopCustomer[0])
						SET_PED_AS_NO_LONGER_NEEDED(ShopCustomer[0])
						REMOVE_ANIM_DICT("amb@world_human_window_shop@male@idle_a")
					ENDIF
				ENDIF
				
				//Update audio
				IF IS_AUDIO_SCENE_ACTIVE("JSH_1_LEAVE_STORE")
					STOP_AUDIO_SCENE("JSH_1_LEAVE_STORE")
				ENDIF
				
				//Move stage on
				missionStage = STAGE_FIND_ROOF_ENTRANCE
				iControlFlag = 0
				
			ENDIF
		ENDIF
		
	ENDIF
	
ENDPROC

//PURPOSE: Mission stage for player has to drive around the block to find the entrance to the roof
PROC DO_STAGE_FIND_ROOF_ENTRANCE()
	
	//Anything that needs called every fram throughout this stage
	
	SWEATSHOP_AMBIENT_CONTROLLER()
	
	JEWEL_STORE_STAFF_CONTROLLER()
	
	IF GET_GAME_TIMER() > (iSaveHouseCheck + 5000)
		IF playerInSafeHouse = FALSE
			IF IS_PLAYER_IN_SAVEHOUSE()
				playerInSafeHouse = TRUE
			ENDIF
			iSaveHouseCheck = GET_GAME_TIMER()
		ENDIF
		IF playerInSafeHouse = TRUE
			IF NOT IS_PLAYER_IN_SAVEHOUSE()
				playerInSafeHouse = FALSE
			ENDIF
			iSaveHouseCheck = GET_GAME_TIMER()
		ENDIF
	ENDIF	
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
	AND NOT IS_PLAYER_IN_ANY_SHOP()
	AND NOT IS_PED_JUMPING(PLAYER_PED_ID())
	AND NOT playerInSafeHouse
	AND NOT IS_PED_CLIMBING(PLAYER_PED_ID())
	AND NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())
	AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
	AND bMissionFailed = FALSE
		GLASSES_CAM_CONTROLLER()
	ELSE
		IF GlassesCamActive = TRUE
			GLASSES_CAM_CONTROLLER()
		ENDIF
	ENDIF
	
	IF iControlFlag = 0
		
		IF MissionStageBeingSkippedTo = TRUE
			STAGE_SELECTOR_MISSION_SETUP()
			MissionStageBeingSkippedTo = FALSE
		ENDIF			
		
		//**************SET MID MISSION CHECK POINT HERE*******************//
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "STAGE_FIND_ROOF_ENTRANCE")
		PRINTSTRING("***********MID MISSION STAGE SET TO 2***********") PRINTNL()	

		//FLAGS
		timerstartedForRooftopEntranceBlip 	= FALSE
		donePH13Chat 						= FALSE
		updatedGodText2 					= FALSE
		player_coming_from_left 			= FALSE
		playerTookTooLong 					= FALSE
		player_coming_from_right 			= FALSE
		donePH41Chat 						= FALSE	
		doneHintCam	 						= FALSE
		doneGetBackInCarText 				= FALSE
		doneChatPH32 						= FALSE
		clearedText 						= FALSE
		donePH71Chat 						= FALSE
		doneLoseCopsText 					= FALSE
		playerInSafeHouse	 				= FALSE
		CanMissionFail 						= TRUE
//		gotPlayersRadioStation 				= FALSE
		
		
		iTimerForRoofEntranceBlip 			= 0
		iBackInCarTimer						= 0
		icopsTextTimer						= 0
		iSaveHouseCheck 					= GET_GAME_TIMER()
	
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			END_REPLAY_SETUP(vehCar)
		ENDIF
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		iControlFlag = 1
		
	ENDIF
	
	IF iControlFlag = 1
			
		IF DOES_ENTITY_EXIST(vehCar)
			IF IS_VEHICLE_DRIVEABLE(vehCar)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
					IF clearedText = FALSE
						IF doneGetBackInCarText = TRUE
						AND GET_GAME_TIMER() < (iBackInCarTimer + DEFAULT_GOD_TEXT_TIME)
							CLEAR_PRINTS()
							clearedText = TRUE
						ENDIF
					ENDIF
					IF NOT IS_AUDIO_SCENE_ACTIVE("JSH_1_DRIVE_TO_BACK_ENTRANCE")
						START_AUDIO_SCENE("JSH_1_DRIVE_TO_BACK_ENTRANCE")
					ENDIF				
					
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						IF doneLoseCopsText = TRUE
							IF GET_GAME_TIMER() < (icopsTextTimer + DEFAULT_GOD_TEXT_TIME)
								CLEAR_PRINTS()
							ENDIF
							doneLoseCopsText = FALSE
						ENDIF
						IF DOES_BLIP_EXIST(BlipVeh)
							REMOVE_BLIP(BlipVeh)
						ENDIF
						IF timerstartedForRooftopEntranceBlip = FALSE
							iTimerForRoofEntranceBlip = GET_GAME_TIMER()
							timerstartedForRooftopEntranceBlip = TRUE
						ENDIF
		//				IF NOT DOES_BLIP_EXIST(ScopeOutViewPointBlip)
		//					ScopeOutViewPointBlip = CREATE_BLIP_FOR_COORD(<< -576.2567, -277.5580, 34.1593 >>, TRUE)
		//				ENDIF
						
						//1st line of dialogue when Michael gets into the car.
						IF donePH13Chat = FALSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()	
									IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH13", CONV_PRIORITY_MEDIUM)
										REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0)
										//Drive us around the block. We got to find a way up to the roof.  
										//Drive around the block, and look out for a way up to the roof. 
										donePH13Chat = TRUE
									ENDIF								
								ENDIF
							ENDIF
						ENDIF
						
						//Check if player is driving away from the block
						IF donePH71Chat = FALSE
							IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-623, -254, 38>>) > 145
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_MESSAGE_BEING_DISPLAYED()
									OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()							
										IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH71", CONV_PRIORITY_MEDIUM)
											//I said drive around the block, not the city. Turn around.
											donePH71Chat = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						//Do some dialogue and update god text
						IF updatedGodText2 = FALSE
							IF donePH13Chat = TRUE
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_MESSAGE_BEING_DISPLAYED()
									OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()		
										PRINT_NOW("GOD1", DEFAULT_GOD_TEXT_TIME, 1)//~s~Find an access point to the roof top.
										updatedGodText2 = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF					
						
						//Check for player coming in from the left of the scaffolding
						IF player_coming_from_left = FALSE
						AND player_coming_from_right = FALSE
							IF GET_GAME_TIMER() > (iTimerForRoofEntranceBlip + 30000)
								player_coming_from_left = TRUE
								playerTookTooLong = TRUE	
								KILL_ANY_CONVERSATION()
							ENDIF
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-578.006958,-264.253021,33.398685>>, <<-599.880920,-225.358902,39.783104>>, 34.500000)
								//Do dialogue from Lester pointing out the scaffolding here.
								player_coming_from_left = TRUE
								KILL_ANY_CONVERSATION()
							ENDIF
							//Check for player coming in from the right of the scaffolding
							IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-606.778870,-324.451355,32.634167>>, <<-549.892456,-292.703674,38.186039>>, 36.500000)
								//Do dialogue from Lester pointing out the scaffolding here.
								player_coming_from_right = TRUE
								KILL_ANY_CONVERSATION()
							ENDIF
						ENDIF
						IF NOT DOES_BLIP_EXIST(ScopeOutViewPointBlip)
							IF player_coming_from_left = TRUE
								ScopeOutViewPointBlip = CREATE_BLIP_FOR_COORD(<< -576.8, -275.6, 34.11925 >>, TRUE)
							ENDIF
							IF player_coming_from_right = TRUE
								ScopeOutViewPointBlip = CREATE_BLIP_FOR_COORD(<<-583.0, -300.1, 33.9756>>, TRUE)
							ENDIF
						ELSE
							IF donePH41Chat = FALSE						
								IF playerTookTooLong = TRUE	
									//Do some dialogue of them talking about the scaffolding.
									KILL_ANY_CONVERSATION()
									IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH50", CONV_PRIORITY_MEDIUM)
										//According to my phone there's a building site on the corner behind the store. Let's go check it out.
										donePH41Chat = TRUE
									ENDIF	
								ELSE
									KILL_ANY_CONVERSATION()
									IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH41", CONV_PRIORITY_MEDIUM)
										//Look. There. Building site.
										//Ah, if they're gutting the place, I might be able to get up on the roof.
										donePH41Chat = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF player_coming_from_left = TRUE
							IF doneHintCam = FALSE
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-579.7, -280.4, 35.3>>) < 40
									IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) != CAM_VIEW_MODE_FIRST_PERSON
										SET_GAMEPLAY_COORD_HINT(<<-580.5, -280.4, 36>>, DEFAULT_DWELL_TIME, 6000, 2000, HINTTYPE_DEFAULT)
									ENDIF
									doneHintCam = TRUE
								ENDIF
							ENDIF						
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -576.8, -275.6, 34.11925 >>, <<4,4,LOCATE_SIZE_HEIGHT>>, TRUE, TRUE, TM_IN_VEHICLE)
								IF DOES_BLIP_EXIST(ScopeOutViewPointBlip)
									REMOVE_BLIP(ScopeOutViewPointBlip)
								ENDIF	
								
//								IF gotPlayersRadioStation = FALSE
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
//										playersRadioStation = GET_PLAYER_RADIO_STATION_NAME()
//										gotPlayersRadioStation = TRUE
//									ENDIF
//								ENDIF
								
								iControlFlag = 2
							ENDIF
						ENDIF
						
						IF player_coming_from_right = TRUE
							IF doneHintCam = FALSE
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-583.2, -292.5, 35.1>>) < 30
									IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) != CAM_VIEW_MODE_FIRST_PERSON
										SET_GAMEPLAY_COORD_HINT(<<-583.6, -291.6, 36>>, DEFAULT_DWELL_TIME, 3000, 2000)
									ENDIF
									doneHintCam = TRUE
								ENDIF
							ENDIF
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-583.0, -300.1, 33.9756>>, <<3,3,LOCATE_SIZE_HEIGHT>>, TRUE, TRUE, TM_IN_VEHICLE)
								IF DOES_BLIP_EXIST(ScopeOutViewPointBlip)
									REMOVE_BLIP(ScopeOutViewPointBlip)
								ENDIF	

//								IF gotPlayersRadioStation = FALSE
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
//										playersRadioStation = GET_PLAYER_RADIO_STATION_NAME()
//										gotPlayersRadioStation = TRUE
//									ENDIF
//								ENDIF

								iControlFlag = 2
							ENDIF				
						ENDIF
					ELSE 
						IF DOES_BLIP_EXIST(BlipVeh)
							REMOVE_BLIP(BlipVeh)
						ENDIF	
						IF DOES_BLIP_EXIST(ScopeOutViewPointBlip)
							REMOVE_BLIP(ScopeOutViewPointBlip)
						ENDIF
						IF doneLoseCopsText = FALSE
							CLEAR_PRINTS()
							PRINT_NOW("WANTED", DEFAULT_GOD_TEXT_TIME, 1)//~s~Lose your wanted level.
							icopsTextTimer = GET_GAME_TIMER()
							doneLoseCopsText = TRUE
						ENDIF
					ENDIF
				ELSE
					//Update audio
					IF IS_AUDIO_SCENE_ACTIVE("JSH_1_INSIDE_STORE")
						STOP_AUDIO_SCENE("JSH_1_INSIDE_STORE")
					ENDIF
					IF NOT IS_AUDIO_SCENE_ACTIVE("JSH_1_LEAVE_STORE")
						START_AUDIO_SCENE("JSH_1_LEAVE_STORE")
					ENDIF
					IF DOES_BLIP_EXIST(ScopeOutViewPointBlip)
						REMOVE_BLIP(ScopeOutViewPointBlip)
					ENDIF	
					IF NOT DOES_BLIP_EXIST(BlipVeh)
						IF IS_VEHICLE_DRIVEABLE(vehCar)
							BlipVeh = CREATE_BLIP_FOR_VEHICLE(vehCar)
						ENDIF
					ENDIF
					IF doneGetBackInCarText = FALSE
						ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(FALSE)
						PRINT_NOW("GOTO_CAR2", DEFAULT_GOD_TEXT_TIME, 1)//~s~Get back in your ~b~car.
						iBackInCarTimer = GET_GAME_TIMER()
						doneGetBackInCarText = TRUE
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	IF iControlFlag = 2
		
		BRING_VEHICLE_TO_HALT(vehCar, 8, 2)

//		IF gotPlayersRadioStation = FALSE
//			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
//				playersRadioStation = GET_PLAYER_RADIO_STATION_NAME()
//				gotPlayersRadioStation = TRUE
//			ENDIF
//		ENDIF

		iControlFlag = 0
		missionStage = STAGE_CHECK_OUT_ROOF
	ENDIF	

ENDPROC

//PURPOSE: Controls everything for the roof top pics being taken.
PROC ROOF_PIC_CONTROLLER()
	
	IF GoodShotDone = FALSE
		IF BallsForVentsCreated = FALSE
			REQUEST_MODEL(PROP_POOL_BALL_01)
			IF HAS_MODEL_LOADED(PROP_POOL_BALL_01)
				//Create balls for all corners of vent 1 at coords <<-624.3, -242.4, 55.4>>
				//Top 4 corners are
				IF NOT DOES_ENTITY_EXIST(Vent1PropBall[0])
					Vent1PropBall[0] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-624.5390, -243.4689, 56.1382>>)
					FREEZE_ENTITY_POSITION(Vent1PropBall[0], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent1PropBall[1])
					Vent1PropBall[1] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-625.2047, -242.0767, 56.1382>>)
					FREEZE_ENTITY_POSITION(Vent1PropBall[1], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent1PropBall[2])
					Vent1PropBall[2] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-623.8929, -241.4008, 56.1382>>)
					FREEZE_ENTITY_POSITION(Vent1PropBall[2], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent1PropBall[3])
					Vent1PropBall[3] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-623.1953, -242.7602, 56.1417>>)
					FREEZE_ENTITY_POSITION(Vent1PropBall[3], TRUE)
				ENDIF
				//Bottom 4 corners are
				IF NOT DOES_ENTITY_EXIST(Vent1PropBall[4])
					Vent1PropBall[4] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-624.5664, -243.5409, 54.9196>>)
					FREEZE_ENTITY_POSITION(Vent1PropBall[4], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent1PropBall[5])
					Vent1PropBall[5] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-625.3116, -242.0517, 54.9196>>)
					FREEZE_ENTITY_POSITION(Vent1PropBall[5], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent1PropBall[6])
					Vent1PropBall[6] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-623.8905, -241.2989, 54.9196>>)
					FREEZE_ENTITY_POSITION(Vent1PropBall[6], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent1PropBall[7])
					Vent1PropBall[7] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-623.1135, -242.7757, 54.9196>>)
					FREEZE_ENTITY_POSITION(Vent1PropBall[7], TRUE)
				ENDIF
				
				//Create balls for all corners of vent 2 at coords <<-629.8, -227.4, 55.6>>
				//Top 4 corners are
				IF NOT DOES_ENTITY_EXIST(Vent2PropBall[0])
					Vent2PropBall[0] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-630.0252, -228.2989, 56.2472>>)
					FREEZE_ENTITY_POSITION(Vent2PropBall[0], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent2PropBall[1])
					Vent2PropBall[1] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-630.6877, -227.0615, 56.2472>>)
					FREEZE_ENTITY_POSITION(Vent2PropBall[1], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent2PropBall[2])
					Vent2PropBall[2] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-629.3130, -226.3524, 56.2472>>)
					FREEZE_ENTITY_POSITION(Vent2PropBall[2], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent2PropBall[3])
					Vent2PropBall[3] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-628.6467, -227.7106, 56.2472>>)
					FREEZE_ENTITY_POSITION(Vent2PropBall[3], TRUE)
				ENDIF
				//Bottom 4 corners are
				IF NOT DOES_ENTITY_EXIST(Vent2PropBall[4])
					Vent2PropBall[4] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-630.0801, -228.4265, 55.0286>>)
					FREEZE_ENTITY_POSITION(Vent2PropBall[4], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent2PropBall[5])
					Vent2PropBall[5] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-630.8126, -227.0237, 55.0286>>)
					FREEZE_ENTITY_POSITION(Vent2PropBall[5], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent2PropBall[6])
					Vent2PropBall[6] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-629.3090, -226.3575, 55.0286>>)
					FREEZE_ENTITY_POSITION(Vent2PropBall[6], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent2PropBall[7])
					Vent2PropBall[7] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-628.5722, -227.7299, 55.0286>>)
					FREEZE_ENTITY_POSITION(Vent2PropBall[7], TRUE)
				ENDIF			

				//Create balls for all corners of the main vent 3 at coords <<-622.3, -234.2, 56.9>>
				//Top 4 corners are
				IF NOT DOES_ENTITY_EXIST(Vent3PropBall[0])
					Vent3PropBall[0] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-621.6507, -239.7255, 58.2032>>)
					FREEZE_ENTITY_POSITION(Vent3PropBall[0], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent3PropBall[1])
					Vent3PropBall[1] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-628.7372, -229.9772, 58.1763>>)
					FREEZE_ENTITY_POSITION(Vent3PropBall[1], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent3PropBall[2])
					Vent3PropBall[2] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-621.4147, -225.7878, 58.1529>>)
					FREEZE_ENTITY_POSITION(Vent3PropBall[2], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent3PropBall[3])
					Vent3PropBall[3] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-615.3572, -236.0661, 58.1280>>)
					FREEZE_ENTITY_POSITION(Vent3PropBall[3], TRUE)
				ENDIF
				//Bottom 4 corners are
				IF NOT DOES_ENTITY_EXIST(Vent3PropBall[4])
					Vent3PropBall[4] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-621.6580, -239.7801, 55.1499>>)
					FREEZE_ENTITY_POSITION(Vent3PropBall[4], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent3PropBall[5])
					Vent3PropBall[5] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-628.7207, -229.9928, 55.0737>>)
					FREEZE_ENTITY_POSITION(Vent3PropBall[5], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent3PropBall[6])
					Vent3PropBall[6] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-621.3970, -225.7067, 55.0875>>)
					FREEZE_ENTITY_POSITION(Vent3PropBall[6], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(Vent3PropBall[7])
					Vent3PropBall[7] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-615.3497, -236.0789, 55.1182>>)
					FREEZE_ENTITY_POSITION(Vent3PropBall[7], TRUE)
				ENDIF

				//Create balls for all corners of the South wall at coords <<-619.3, -242.7, 53.6>>
				//Have the 4 corners of the wall and 2 in the middle of wall to make sure most of it is in shot for it to count
				//Top 2 corners are
				IF NOT DOES_ENTITY_EXIST(SouthWallPropBall[0])
					SouthWallPropBall[0] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-625.3745, -246.0848, 55.6554>>)
					FREEZE_ENTITY_POSITION(SouthWallPropBall[0], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(SouthWallPropBall[1])
					SouthWallPropBall[1] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-607.8821, -236.8790, 55.6684>>)
					FREEZE_ENTITY_POSITION(SouthWallPropBall[1], TRUE)
				ENDIF
				//Bottom 2 corners are
				IF NOT DOES_ENTITY_EXIST(SouthWallPropBall[2])
					SouthWallPropBall[2] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-625.2039, -245.7673, 51.3461>>)
					FREEZE_ENTITY_POSITION(SouthWallPropBall[2], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(SouthWallPropBall[3])
					SouthWallPropBall[3] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-608.2419, -236.9733, 50.3458>>)
					FREEZE_ENTITY_POSITION(SouthWallPropBall[3], TRUE)
				ENDIF
				//Middle of wall
				IF NOT DOES_ENTITY_EXIST(SouthWallPropBall[4])
					SouthWallPropBall[4] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-618.4209, -242.1374, 55.6521>>)
					FREEZE_ENTITY_POSITION(SouthWallPropBall[4], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(SouthWallPropBall[5])
					SouthWallPropBall[5] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-618.3972, -242.1671, 51.8769>>)
					FREEZE_ENTITY_POSITION(SouthWallPropBall[5], TRUE)
				ENDIF	
				
				//Create balls for all top corners of the actual vent at <<-622.4, -233.1, 58.1>>
				//This should only really be visible from point <<-625.9, -216.9, 59.4>>
				//Top 4 corners are
				IF NOT DOES_ENTITY_EXIST(MainVentPropBall[0])
					MainVentPropBall[0] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-622.7312, -230.0258, 58.1717>>)
					FREEZE_ENTITY_POSITION(MainVentPropBall[0], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(MainVentPropBall[1])
					MainVentPropBall[1] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-625.4466, -231.6030, 58.1723>>)
					FREEZE_ENTITY_POSITION(MainVentPropBall[1], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(MainVentPropBall[2])
					MainVentPropBall[2] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-622.1394, -237.3194, 58.1723>>)
					FREEZE_ENTITY_POSITION(MainVentPropBall[2], TRUE)
				ENDIF
				IF NOT DOES_ENTITY_EXIST(MainVentPropBall[3])
					MainVentPropBall[3] = CREATE_OBJECT(PROP_POOL_BALL_01, <<-619.3909, -235.7099, 58.1717>>)
					FREEZE_ENTITY_POSITION(MainVentPropBall[3], TRUE)
				ENDIF	
			
				FOR icount = 0 TO 7
					SET_ENTITY_VISIBLE(Vent1PropBall[icount], FALSE)
					SET_ENTITY_COLLISION(Vent1PropBall[icount], FALSE)
				ENDFOR
				FOR icount = 0 TO 7
					SET_ENTITY_VISIBLE(Vent2PropBall[icount], FALSE)
					SET_ENTITY_COLLISION(Vent2PropBall[icount], FALSE)
				ENDFOR	
				FOR icount = 0 TO 7
					SET_ENTITY_VISIBLE(Vent3PropBall[icount], FALSE)
					SET_ENTITY_COLLISION(Vent3PropBall[icount], FALSE)
				ENDFOR	
				FOR icount = 0 TO 5
					SET_ENTITY_VISIBLE(SouthWallPropBall[icount], FALSE)
					SET_ENTITY_COLLISION(SouthWallPropBall[icount], FALSE)
				ENDFOR			
				FOR icount = 0 TO 3
					SET_ENTITY_VISIBLE(MainVentPropBall[icount], FALSE)
					SET_ENTITY_COLLISION(MainVentPropBall[icount], FALSE)
				ENDFOR	
			
				SET_MODEL_AS_NO_LONGER_NEEDED(PROP_POOL_BALL_01)
				BallsForVentsCreated = TRUE
			
			ENDIF
		ENDIF
	ENDIF
	
	//Have a control here that player must be higher than 53m to be able to take a valid picture. And if he is on North side of roof he needs to be above 56m
	vplayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
	
	IF vplayerCoords.z > 54
	AND vplayerCoords.y < -225
		playerIsHighEnough = TRUE
	ELSE
		IF vplayerCoords.z > 56.5
			playerIsHighEnough = TRUE
		ELSE
			playerIsHighEnough = FALSE
		ENDIF
	ENDIF
	
	IF playerIsHighEnough = TRUE
	AND playerTakingPhoto = TRUE
	AND SnapDialogueNeeded = TRUE
	AND PhotoJustTaken = TRUE
		//Run a check for if player is on top of hut next to roof entrance at <<-593, -281.1, 54.5>>
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-593.1058, -281.6116, 53.4842>>, <<4,4,2>>)
			//Need to have the left corner of south wall in view and the top south corners of vent3 on screen otherwise no use.
			IF DOES_ENTITY_EXIST(SouthWallPropBall[0])
				IF IS_ENTITY_ON_SCREEN(SouthWallPropBall[0])
					IF DOES_ENTITY_EXIST(Vent3PropBall[0])
						IF IS_ENTITY_ON_SCREEN(Vent3PropBall[0])					
							IF DOES_ENTITY_EXIST(Vent3PropBall[3])
								IF IS_ENTITY_ON_SCREEN(Vent3PropBall[3])
									//Okay, if my calculations are right they must be the vents directly above the store.
									DoChatPH57 = TRUE
									GoodShotDialogueRequired = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Run a check for if player is on top of the vents at <<-600.1, -266.1, 54.9>>
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-600.1, -266.1, 54.9>>, <<5,5,5>>)
			//If any of the balls are on screen at these coords then he will be blocked off by the large vent in his face so Lester should comment on that
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-596.240173,-268.185150,53.699200>>, <<-597.876953,-265.852783,56.323952>>, 2.250000)
				//Run a check here for any balls being on screen before doing chat
				IF DOES_ENTITY_EXIST(SouthWallPropBall[4])
					IF IS_ENTITY_ON_SCREEN(SouthWallPropBall[4])
						//Move more to the left I can't get a clear view.
						//Shot's blocked off Michael, move over a bit.
						//This shot's no use Michael.
						DoChatPH58 = TRUE
						GoodButNotRightChatRequired = TRUE
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(Vent1PropBall[0])
					IF IS_ENTITY_ON_SCREEN(Vent1PropBall[0])
						//Move more to the left I can't get a clear view.
						//Shot's blocked off Michael, move over a bit.
						//This shot's no use Michael.
						DoChatPH58 = TRUE
						GoodButNotRightChatRequired = TRUE
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(Vent3PropBall[0])
					IF IS_ENTITY_ON_SCREEN(Vent3PropBall[0])
						//Move more to the left I can't get a clear view.
						//Shot's blocked off Michael, move over a bit.
						//This shot's no use Michael.
						DoChatPH58 = TRUE
						GoodButNotRightChatRequired = TRUE
					ENDIF
				ENDIF
			ELSE
				//Need to have the left corner of south wall in view and the top south corners of vent3 on screen otherwise no use.
				IF DOES_ENTITY_EXIST(SouthWallPropBall[0])
					IF IS_ENTITY_ON_SCREEN(SouthWallPropBall[0])
						IF DOES_ENTITY_EXIST(Vent3PropBall[0])
							IF IS_ENTITY_ON_SCREEN(Vent3PropBall[0])					
								IF DOES_ENTITY_EXIST(Vent3PropBall[3])
									IF IS_ENTITY_ON_SCREEN(Vent3PropBall[3])
										//Okay, if my calculations are right they must be the vents directly above the store.
										DoChatPH57 = TRUE
										GoodShotDialogueRequired = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF	
		
		//Run a check for if player is on top of either vents at <<-607.6, -272.1, 52.3>> or if he's on top of the glass dome at <<-610.9, -263.4, 54.4>>
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-607.6, -272.1, 53.1>>, <<4,4,2>>)	
		OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-610.9, -263.4, 54.4>>, <<4,4,2>>)	
			//Need to have the left corner of south wall and bottom middle of south wall in view and the top south corners of vent3 on screen otherwise no use.
			IF DOES_ENTITY_EXIST(SouthWallPropBall[0])
				IF IS_ENTITY_ON_SCREEN(SouthWallPropBall[0])
					IF DOES_ENTITY_EXIST(Vent3PropBall[0])
						IF IS_ENTITY_ON_SCREEN(Vent3PropBall[0])					
							IF DOES_ENTITY_EXIST(Vent3PropBall[3])
								IF IS_ENTITY_ON_SCREEN(Vent3PropBall[3])
									IF DOES_ENTITY_EXIST(SouthWallPropBall[5])
										IF IS_ENTITY_ON_SCREEN(SouthWallPropBall[5])
											//Okay, if my calculations are right they must be the vents directly above the store.
											DoChatPH57 = TRUE
											GoodShotDialogueRequired = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
		
			//Tell player to move slightly right if southwall left corner is on screen but not the right southside corner of vent3
			IF DOES_ENTITY_EXIST(SouthWallPropBall[0])
				IF IS_ENTITY_ON_SCREEN(SouthWallPropBall[0])			
					IF DOES_ENTITY_EXIST(Vent3PropBall[3])
						IF NOT IS_ENTITY_ON_SCREEN(Vent3PropBall[3])
							//Move more to the right, I can't get a clear view.
							//Shot's blocked off Michael, move over a bit.
							//This shot's no use Michael.
							DoChatPH59 = TRUE
							GoodButNotRightChatRequired = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF

			//Tell player to move slightly left if southwall right corner is on screen but not the left southside corner of vent3
			IF DOES_ENTITY_EXIST(SouthWallPropBall[1])
				IF IS_ENTITY_ON_SCREEN(SouthWallPropBall[1])			
					IF DOES_ENTITY_EXIST(Vent3PropBall[0])
						IF NOT IS_ENTITY_ON_SCREEN(Vent3PropBall[0])
							//Move more to the left I can't get a clear view.
							//Shot's blocked off Michael, move over a bit.
							//This shot's no use Michael.
							DoChatPH58 = TRUE
							GoodButNotRightChatRequired = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
		ENDIF
		
		//Run a check for if player is on top of the vent at <<-608.3, -251.4, 54.3>>
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-608.3, -251.4, 54.3>>, <<3,3,3>>)
			//If both middle southwall balls are on screen and the 2 southside corners of vent 3 that should do for here
			IF DOES_ENTITY_EXIST(SouthWallPropBall[4])
				IF IS_ENTITY_ON_SCREEN(SouthWallPropBall[4])			
					IF DOES_ENTITY_EXIST(SouthWallPropBall[5])
						IF IS_ENTITY_ON_SCREEN(SouthWallPropBall[5])	
							IF DOES_ENTITY_EXIST(Vent3PropBall[0])
								IF IS_ENTITY_ON_SCREEN(Vent3PropBall[0])			
									IF DOES_ENTITY_EXIST(Vent3PropBall[3])
										IF IS_ENTITY_ON_SCREEN(Vent3PropBall[3])
											//We'll be able to get up to them using those ladders. 
											DoChatPH61 = TRUE
											GoodShotDialogueRequired = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Run checks for if player is in the area directly above the store.
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-621.3, -232.8, 55.5>>, <<15, 15, 5>>)
			
			//Only allow vent 1 to be a good shot if player is not in these angled areas. Make sure all corners are on screen for a good shot.
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-625.657471,-231.565369,57.904049>>, <<-613.301208,-225.310837,54.217838>>, 17.000000)
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-627.125732,-226.431351,53.903927>>, <<-629.548157,-227.507263,58.712666>>, 6.500000)
				IF DOES_ENTITY_EXIST(Vent1PropBall[0])
					IF IS_ENTITY_ON_SCREEN(Vent1PropBall[0])			
						IF DOES_ENTITY_EXIST(Vent1PropBall[1])
							IF IS_ENTITY_ON_SCREEN(Vent1PropBall[1])
								IF DOES_ENTITY_EXIST(Vent1PropBall[2])
									IF IS_ENTITY_ON_SCREEN(Vent1PropBall[2])			
										IF DOES_ENTITY_EXIST(Vent1PropBall[3])
											IF IS_ENTITY_ON_SCREEN(Vent1PropBall[3])
												IF DOES_ENTITY_EXIST(Vent1PropBall[4])
													IF IS_ENTITY_ON_SCREEN(Vent1PropBall[4])			
														IF DOES_ENTITY_EXIST(Vent1PropBall[5])
															IF IS_ENTITY_ON_SCREEN(Vent1PropBall[5])
																IF DOES_ENTITY_EXIST(Vent1PropBall[6])
																	IF IS_ENTITY_ON_SCREEN(Vent1PropBall[6])			
																		IF DOES_ENTITY_EXIST(Vent1PropBall[7])
																			IF IS_ENTITY_ON_SCREEN(Vent1PropBall[7])
																				//Shot of the small vents has come through. 
																				DoChatPH64 = TRUE
																				GoodShotDialogueRequired = TRUE
																			ENDIF
																		ENDIF
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
			
			//Only allow vent 2 to be a good shot if player is not in these angled areas. Make sure all corners are on screen for a good shot.
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-627.874512,-229.483047,57.600204>>, <<-623.348755,-244.309509,54.267174>>, 2.250000)
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-624.594482,-237.525116,57.904713>>, <<-610.178711,-230.179276,54.083748>>, 17.000000)
				IF DOES_ENTITY_EXIST(Vent2PropBall[0])
					IF IS_ENTITY_ON_SCREEN(Vent2PropBall[0])			
						IF DOES_ENTITY_EXIST(Vent2PropBall[1])
							IF IS_ENTITY_ON_SCREEN(Vent2PropBall[1])
								IF DOES_ENTITY_EXIST(Vent2PropBall[2])
									IF IS_ENTITY_ON_SCREEN(Vent2PropBall[2])			
										IF DOES_ENTITY_EXIST(Vent2PropBall[3])
											IF IS_ENTITY_ON_SCREEN(Vent2PropBall[3])
												IF DOES_ENTITY_EXIST(Vent2PropBall[4])
													IF IS_ENTITY_ON_SCREEN(Vent2PropBall[4])			
														IF DOES_ENTITY_EXIST(Vent2PropBall[5])
															IF IS_ENTITY_ON_SCREEN(Vent2PropBall[5])
																IF DOES_ENTITY_EXIST(Vent2PropBall[6])
																	IF IS_ENTITY_ON_SCREEN(Vent2PropBall[6])			
																		IF DOES_ENTITY_EXIST(Vent2PropBall[7])
																			IF IS_ENTITY_ON_SCREEN(Vent2PropBall[7])
																				//Shot of the small vents has come through. 
																				DoChatPH64 = TRUE
																				GoodShotDialogueRequired = TRUE
																			ENDIF
																		ENDIF
																	ENDIF
																ENDIF
															ENDIF
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		//If player is standing on the back vents at coord <<-627, -217.2, 57>>
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-627, -217.2, 57>>, <<4,4,4>>)
			
			//Don't allow any good shots if player is in this zone as his veiw will be completely blocked so should be told to move
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-624.830994,-214.345215,55.705490>>, <<-628.013489,-215.890976,58.207249>>, 1.500000)
				IF DOES_ENTITY_EXIST(Vent3PropBall[1])
					IF IS_ENTITY_ON_SCREEN(Vent3PropBall[1])
						//Michael I can't see anything. Climb up onto that vent you should be able to get a better shot.
						//Can you climb up this to get a better view?
						//You need to get higher, climb on top of this.
						DoChatPH65 = TRUE
						GoodButNotRightChatRequired = TRUE
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(Vent2PropBall[1])
					IF IS_ENTITY_ON_SCREEN(Vent2PropBall[1])
						//Michael I can't see anything. Climb up onto that vent you should be able to get a better shot.
						//Can you climb up this to get a better view?
						//You need to get higher, climb on top of this.
						DoChatPH65 = TRUE
						GoodButNotRightChatRequired = TRUE
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(Vent3PropBall[2])
					IF IS_ENTITY_ON_SCREEN(Vent3PropBall[2])
						//Michael I can't see anything. Climb up onto that vent you should be able to get a better shot.
						//Can you climb up this to get a better view?
						//You need to get higher, climb on top of this.
						DoChatPH65 = TRUE
						GoodButNotRightChatRequired = TRUE
					ENDIF
				ENDIF
			ELSE	
				//If vent 2 is on screen with these corners then it's a good shot
				IF DOES_ENTITY_EXIST(Vent2PropBall[1])
					IF IS_ENTITY_ON_SCREEN(Vent2PropBall[1])
						IF DOES_ENTITY_EXIST(Vent2PropBall[2])
							IF IS_ENTITY_ON_SCREEN(Vent2PropBall[2])			
								IF DOES_ENTITY_EXIST(Vent2PropBall[3])
									IF IS_ENTITY_ON_SCREEN(Vent2PropBall[3])
										IF DOES_ENTITY_EXIST(Vent2PropBall[7])
											IF IS_ENTITY_ON_SCREEN(Vent2PropBall[7])
												IF DOES_ENTITY_EXIST(MainVentPropBall[1])
													IF NOT IS_ENTITY_ON_SCREEN(MainVentPropBall[1])
														//Shot of the small vents has come through. 
														DoChatPH64 = TRUE
														GoodShotDialogueRequired = TRUE
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF					
			ENDIF
		ENDIF
		
		//Run the main check here for moving the stage on
		IF playerInPerfectPlace = TRUE
			IF DOES_ENTITY_EXIST(MainVentPropBall[0])
				IF IS_ENTITY_ON_SCREEN(MainVentPropBall[0])
					IF DOES_ENTITY_EXIST(MainVentPropBall[1])
						IF IS_ENTITY_ON_SCREEN(MainVentPropBall[1])
							IF DOES_ENTITY_EXIST(MainVentPropBall[2])
								IF IS_ENTITY_ON_SCREEN(MainVentPropBall[2])
									IF DOES_ENTITY_EXIST(MainVentPropBall[3])
										IF IS_ENTITY_ON_SCREEN(MainVentPropBall[3])
											//Shot's come through of the ventalation system, perfect Michael.
											DoChatPH60 = TRUE
											PerfectShotDialogueRequired = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			//If only the left side is on screen tell player to move more to the right
			IF DOES_ENTITY_EXIST(MainVentPropBall[3])
				IF IS_ENTITY_ON_SCREEN(MainVentPropBall[3])		
					IF DOES_ENTITY_EXIST(MainVentPropBall[0])
						IF IS_ENTITY_ON_SCREEN(MainVentPropBall[0])		
							IF DOES_ENTITY_EXIST(MainVentPropBall[1])
								IF NOT IS_ENTITY_ON_SCREEN(MainVentPropBall[1])	
									IF DOES_ENTITY_EXIST(MainVentPropBall[2])
									 	IF NOT IS_ENTITY_ON_SCREEN(MainVentPropBall[2])	
											//Move slightly more to the right Michael.
											//Try and get the whole vent in the shot Michael.
											//Can you look more to the right please.
											DoChatPH62 = TRUE
											GoodButNotRightChatRequired = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			//If only the left side is on screen tell player to move more to the left
			IF DOES_ENTITY_EXIST(MainVentPropBall[1])
				IF IS_ENTITY_ON_SCREEN(MainVentPropBall[1])			
					IF DOES_ENTITY_EXIST(MainVentPropBall[3])
						IF NOT IS_ENTITY_ON_SCREEN(MainVentPropBall[3])	
							IF DOES_ENTITY_EXIST(MainVentPropBall[0])
								IF NOT IS_ENTITY_ON_SCREEN(MainVentPropBall[0])	
									//Can you move the camera left a bit.
									//Try and get the whole vent in the shot Michael.
									//Look more to the left Michael.
									DoChatPH63 = TRUE
									GoodButNotRightChatRequired = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
					
		PhotoJustTaken = FALSE
	ENDIF
				
		
	//DO THE DIALOGUE FOR THE SHOT
	IF SnapDialogueNeeded = TRUE
		IF GoodShotDone = FALSE
		
			IF PerfectShotDialogueRequired = TRUE

				IF DoChatPH60 = TRUE
					IF doneChatPH60 = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH60", CONV_PRIORITY_MEDIUM)
									//Shot's come through of the ventalation system, perfect Michael.
									doneChatPH60 = TRUE
									PerfectShotDialogueRequired = FALSE
									GoodShotDone = TRUE
									SnapDialogueNeeded = FALSE
								ENDIF
							ELSE
								//run a check here for the god text being on screen so you can clear it.
								IF doneGodTextMainVent = TRUE
									IF GET_GAME_TIMER() < (iVentGodTextTimer + DEFAULT_GOD_TEXT_TIME)
										CLEAR_PRINTS()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF				
			
			ENDIF
		
			//Do dialogue for a good shot getting taken
			IF GoodShotDialogueRequired = TRUE
			AND PerfectShotDialogueRequired = FALSE
				
				IF DoChatPH57 = TRUE
					IF doneChatPH57 = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH57", CONV_PRIORITY_MEDIUM)
									//Okay, if my calculations are right they must be the vents directly above the store.
									doneChatPH57 = TRUE
									GoodShotDialogueRequired = FALSE
									HigherChatNeeded = TRUE
									DoChatPH57 = FALSE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						GoodShotDialogueRequired = FALSE
						HigherChatNeeded = TRUE
						DoChatPH57 = FALSE
					ENDIF
				ENDIF
				
				IF DoChatPH64 = TRUE
					IF doneChatPH64 = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH64", CONV_PRIORITY_MEDIUM)
									//Shot of the small vents has come through. 
									doneChatPH64 = TRUE
									GoodShotDialogueRequired = FALSE
									HigherChatNeeded = TRUE
									DoChatPH64 = FALSE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						GoodShotDialogueRequired = FALSE
						HigherChatNeeded = TRUE
						DoChatPH64 = FALSE
					ENDIF
				ENDIF
			
				IF DoChatPH61 = TRUE
					IF doneChatPH61 = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH61", CONV_PRIORITY_MEDIUM)
									//We'll be able to get up to them using those ladders. 
									doneChatPH61 = TRUE
									GoodShotDialogueRequired = FALSE
									HigherChatNeeded = TRUE
									DoChatPH61 = FALSE
								ENDIF
							ENDIF
						ENDIF
					ELSE
						GoodShotDialogueRequired = FALSE
						HigherChatNeeded = TRUE
						DoChatPH61 = FALSE
					ENDIF
				ENDIF
			
			ENDIF
			
			IF HigherChatNeeded = TRUE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
					OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
						IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH66", CONV_PRIORITY_MEDIUM)
							//We could do with a shot of that main vent. Try and get higher up.
							//Can you get higher up and get a shot of that main vent that's surrounded with barb wire.
							//We need a shot of the main vent above the store. Get higher Michael.
							OkShotDone = TRUE
							SnapDialogueNeeded = FALSE
							HigherChatNeeded = FALSE
						ENDIF
					ENDIF
				ENDIF				
			ENDIF
			
			//Do dialogue for a shot being taken in the right direction but just not quite right
			IF GoodButNotRightChatRequired = TRUE
			AND GoodShotDialogueRequired = FALSE
			AND PerfectShotDialogueRequired = FALSE

				IF DoChatPH59 = TRUE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH59", CONV_PRIORITY_MEDIUM)
								//Move more to the right, I can't get a clear view.
								//Shot's blocked off Michael, move over a bit.
								//This shot's no use Michael.
								DoChatPH59 = FALSE
								GoodButNotRightChatRequired = FALSE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				IF DoChatPH58 = TRUE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH58", CONV_PRIORITY_MEDIUM)
								//Move more to the left I can't get a clear view.
								//Shot's blocked off Michael, move over a bit.
								//This shot's no use Michael.
								DoChatPH58 = FALSE
								GoodButNotRightChatRequired = FALSE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF		
	
				IF DoChatPH62 = TRUE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH62", CONV_PRIORITY_MEDIUM)
								//Move slightly more to the right Michael.
								//Try and get the whole vent in the shot Michael.
								//Can you look more to the right please.
								DoChatPH62 = FALSE
								GoodButNotRightChatRequired = FALSE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
				
				IF DoChatPH63 = TRUE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH63", CONV_PRIORITY_MEDIUM)
								//Can you move the camera left a bit.
								//Try and get the whole vent in the shot Michael.
								//Look more to the left Michael.
								DoChatPH63 = FALSE
								GoodButNotRightChatRequired = FALSE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
	
				IF DoChatPH65 = TRUE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH65", CONV_PRIORITY_MEDIUM)
								//Michael I can't see anything. Climb up onto that vent you should be able to get a better shot.
								//Can you climb up this to get a better view?
								//You need to get higher, climb on top of this.
								DoChatPH65 = FALSE
								GoodButNotRightChatRequired = FALSE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF				
			
			ENDIF
			
			//Do chat if the photo is useless
			IF GoodButNotRightChatRequired = FALSE
			AND GoodShotDialogueRequired = FALSE
			AND PerfectShotDialogueRequired = FALSE
			AND playerTakingPhoto = TRUE
				IF OkShotDone = FALSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_WRONG", CONV_PRIORITY_MEDIUM)
								//No, no. Their air conditioning roof unit. 
								//It's like a big box.. on the roof.
								//We need to know where those vents come out. 
								GoodButNotRightChatRequired = FALSE
								SnapDialogueNeeded = FALSE
							ENDIF
						ENDIF
					ENDIF	
				ELSE
					IF playerInPerfectPlace = FALSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH54", CONV_PRIORITY_MEDIUM)
									//You need to get higher up Michael.
									//This is no good try and get higher.
									//You are not high enough.
									NotHighEnoughTimer = GET_GAME_TIMER()
									GoodButNotRightChatRequired = FALSE
									SnapDialogueNeeded = FALSE
								ENDIF
							ENDIF
						ENDIF	
					ELSE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF NOT IS_MESSAGE_BEING_DISPLAYED()
							OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_WRONG", CONV_PRIORITY_MEDIUM)
									//No, no. Their air conditioning roof unit. 
									//It's like a big box.. on the roof.
									//We need to know where those vents come out. 
									GoodButNotRightChatRequired = FALSE
									SnapDialogueNeeded = FALSE
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ENDIF
	
	IF GoodShotDone = TRUE
		//Do move stage on chat
		IF donePH43Chat = FALSE
			IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH43", CONV_PRIORITY_MEDIUM)
						//Okay. That'll do. Come back to me before someone spots you up there.
						//Will do.
						iHurryUpChatTimer = GET_GAME_TIMER()
						donePH43Chat = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		//Do some random chat from Lester if Michael continues to take photo's after being told to get back to the car
		IF playerTakingPhoto = TRUE
			IF donePH43Chat = TRUE
				IF GET_GAME_TIMER() > (iHurryUpChatTimer + 7000)
					IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH48", CONV_PRIORITY_MEDIUM)
						//Come on, Michael. Let's go.
						//Let's hurry this up, Michael.
						//If you take any longer, you're risking blowing this whole thing.
						//Let's speed this up, we don't want to draw attention.
						iHurryUpChatTimer = GET_GAME_TIMER()
						donePH43Chat = TRUE
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Delete all the balls now to clean up memory
	IF ballsDeleted = FALSE	
		IF GoodShotDone = TRUE

			FOR icount = 0 TO 7
				IF DOES_ENTITY_EXIST(Vent1PropBall[icount])
					DELETE_OBJECT(Vent1PropBall[icount])
				ENDIF
			ENDFOR
			FOR icount = 0 TO 7
				IF DOES_ENTITY_EXIST(Vent2PropBall[icount])
					DELETE_OBJECT(Vent2PropBall[icount])
				ENDIF
			ENDFOR	
			FOR icount = 0 TO 7
				IF DOES_ENTITY_EXIST(Vent3PropBall[icount])
					DELETE_OBJECT(Vent3PropBall[icount])
				ENDIF
			ENDFOR	
			FOR icount = 0 TO 5
				IF DOES_ENTITY_EXIST(SouthWallPropBall[icount])
					DELETE_OBJECT(SouthWallPropBall[icount])
				ENDIF
			ENDFOR			
			FOR icount = 0 TO 3
				IF DOES_ENTITY_EXIST(MainVentPropBall[icount])
					DELETE_OBJECT(MainVentPropBall[icount])
				ENDIF
			ENDFOR
			
			ballsDeleted = TRUE
		
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Stage where Michael checks out the roof access for getting to the vents.
PROC DO_STAGE_CHECK_OUT_ROOF()

	//Anything that needs called every fram throughout this stage
	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(player_id())
	
	SWEATSHOP_AMBIENT_CONTROLLER()
	
	JEWEL_STORE_STAFF_CONTROLLER()
	
	IF GET_GAME_TIMER() > (iSaveHouseCheck + 5000)
		IF playerInSafeHouse = FALSE
			IF IS_PLAYER_IN_SAVEHOUSE()
				playerInSafeHouse = TRUE
			ENDIF
			iSaveHouseCheck = GET_GAME_TIMER()
		ENDIF
		IF playerInSafeHouse = TRUE
			IF NOT IS_PLAYER_IN_SAVEHOUSE()
				playerInSafeHouse = FALSE
			ENDIF
			iSaveHouseCheck = GET_GAME_TIMER()
		ENDIF
	ENDIF		
	
	IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
	AND NOT IS_PLAYER_IN_ANY_SHOP()
	AND NOT IS_PED_JUMPING(PLAYER_PED_ID())
	AND NOT playerInSafeHouse
	AND NOT IS_PED_CLIMBING(PLAYER_PED_ID())
	AND NOT IS_ENTITY_IN_AIR(PLAYER_PED_ID())
	AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
	AND bMissionFailed = FALSE
		GLASSES_CAM_CONTROLLER()
	ELSE
		IF GlassesCamActive = TRUE
			GLASSES_CAM_CONTROLLER()
		ENDIF
	ENDIF
	
	ROOF_PIC_CONTROLLER()

	IF iControlFlag = 0
		
		IF MissionStageBeingSkippedTo = TRUE
			STAGE_SELECTOR_MISSION_SETUP()
			MissionStageBeingSkippedTo = FALSE
		ENDIF			
		
		//**************SET MID MISSION CHECK POINT HERE*******************//
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "STAGE_CHECK_ROOF")
		PRINTSTRING("***********MID MISSION STAGE SET TO 3***********") PRINTNL()
		
		IF IS_VEHICLE_DRIVEABLE(vehCar)
			FREEZE_ENTITY_POSITION(vehCar, FALSE)
		ENDIF	
		
		//═════════════════════════╡ SET FLAGS ╞═══════════════════════════
		CanMissionFail 			= TRUE	
		donePH44Chat 			= FALSE
		donePH42Chat 			= FALSE
		doneDESCChat			= FALSE
		playerOnRoof 			= FALSE
		moveStageOnTimerSet 	= FALSE
		donePH43Chat 			= FALSE
		donePH45Chat 			= FALSE
		donePH46Chat 			= FALSE
		DoneGlassesHelpText 	= FALSE
		doneGotoCar2Text 		= FALSE
		modelsRequested 		= FALSE
		MusicStarted[2] 		= FALSE
		donePH23Chat 			= TRUE
		doneOLDCREWChat 		= FALSE
		doneHelp1Text 			= FALSE
//	 	doneHelp2Text 			= FALSE
		PausedOldCrewChat 		= FALSE
		doneVentChat 			= FALSE
		donePH51Chat 			= FALSE
		playerHasBeenAtPerfectPlace 	= FALSE
		doneVentsBlipText 		= FALSE
		doneVentsGodText 		= FALSE
		updatedGodText3 		= FALSE	
//		SyncdSceneDisrupted 	= FALSE	
		PlayerCantFindVentsTimerSet = FALSE
		DoChatPH57				= FALSE
		GoodShotDialogueRequired	= FALSE
		GoodButNotRightChatRequired	= FALSE
		DoChatPH58				= FALSE
		DoChatPH59				= FALSE
		DoChatPH60				= FALSE
		GoodShotDone			= FALSE
		doneChatPH57			= FALSE
		doneChatPH60 			= FALSE
		ballsDeleted 			= FALSE
		BallsForVentsCreated 	= FALSE
		DoChatPH61				= FALSE
		doneChatPH61 			= FALSE	
		playerInPerfectPlace 	= FALSE
		DoChatPH62				= FALSE
		DoChatPH63				= FALSE
		DoChatPH64				= FALSE
		HigherChatNeeded		= FALSE
		doneChatPH64			= FALSE
		PerfectShotDialogueRequired = FALSE
		DoChatPH65				= FALSE
		OkShotDone				= FALSE
		hintCamCancelled 		= FALSE
		doneChatPH68 			= FALSE
		doneChatPH69 			= FALSE
		doneGodTextMainVent		= FALSE
		doneReturnText 			= FALSE
		doneChat46				= FALSE
		openingDialogueRequired = FALSE
		doneChatPH32 			= FALSE
		playerInSafeHouse		= FALSE
		
		iVentGodTextTimer 		= 0
		iChat46Timer			= 0
		iSaveHouseCheck 		= GET_GAME_TIMER()
//		//Request the worker model here
//		REQUEST_MODEL(S_M_Y_CONSTRUCT_01)
//		REQUEST_WAYPOINT_RECORDING("BB_JEW_3")
//		REQUEST_WAYPOINT_RECORDING("BB_JEW_4")
		
		#IF IS_DEBUG_BUILD
			jSkipReady = TRUE
			pSkipReady = TRUE
		#ENDIF		
		
		SETTIMERA(0)
		
		IF NOT IS_PED_INJURED(pedContact)
			SET_PED_CAN_BE_DRAGGED_OUT(pedContact, FALSE)
			SET_PED_CAN_BE_TARGETTED(pedContact, FALSE)
			SET_PED_RELATIONSHIP_GROUP_HASH(pedContact, RELGROUPHASH_PLAYER)
		ENDIF
		
		//If screen is faded out do a load scene at players coordinates now and then fade in the screen
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_REPLAY_BEING_SET_UP()
				LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
			ENDIF
			END_REPLAY_SETUP(vehCar)
			openingDialogueRequired = TRUE
		ENDIF
		IF NOT IS_SCREEN_FADED_IN()
			DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
		ENDIF
		
		CLEAR_AREA(<<-587.7646, -287.2640, 49.3291>>, 5, TRUE)
		ScenarioBlocking[4] = ADD_SCENARIO_BLOCKING_AREA(<<-590.7, -292.8, 46>>, <<-575.7, -280.4, 54.6>>)
		
		iHurryUpRoofTimer 	= GET_GAME_TIMER()	
		NotHighEnoughTimer 	= GET_GAME_TIMER()
		
		SET_ROADS_IN_AREA(<<-611.9, -362.4, 30>>, <<-558.6, -264.5, 45>>, FALSE)
		SET_ROADS_IN_AREA(<<-538.9, -296.7, 43.3>>, <<-631.3, -185.2, 30>>, FALSE)		
		
		IF NOT DOES_BLIP_EXIST(BlipRoofCheck)
			BlipRoofCheck = CREATE_BLIP_FOR_COORD(<<-591.8343, -291.3900, 49.3270>>)//(<<-593.5949, -292.3533, 40.6863>>)
		ENDIF			
		
		vplayerStartRoofCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
		
		REPLAY_RECORD_BACK_FOR_TIME(2.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
		
		iControlFlag = 1
	ENDIF
	
	IF iControlFlag = 1
		
		//Allow player to cancel hintcam if he moves the right analogue stick
		INT iLeftx, iLefty, iRightX, iRightY
		IF hintCamCancelled = FALSE
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND(iLeftx, iLefty, iRightX, iRightY)
				IF iRightX > 10
				OR iRightX < -10
				OR iRightX > 10
				OR iRightX < -10
					STOP_GAMEPLAY_HINT()
					hintCamCancelled = TRUE
				ENDIF
			ELSE
				STOP_GAMEPLAY_HINT()
				hintCamCancelled = TRUE
			ENDIF
		ENDIF
		
		IF openingDialogueRequired = TRUE
			IF doneChatPH32 = FALSE
				IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH32", CONV_PRIORITY_MEDIUM)
					//See if you can get up to the roof. We need a shot of their airconditioning unit. 	
					doneChatPH32 = TRUE
				ENDIF
			ENDIF
		ENDIF		
		
		IF doneChatPH32 = TRUE
		OR openingDialogueRequired = FALSE
			IF updatedGodText3 = FALSE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF NOT IS_MESSAGE_BEING_DISPLAYED()
					OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						PRINT_NOW("GOD6", DEFAULT_GOD_TEXT_TIME, 1)//~s~Go to the ~y~roof top.
						updatedGodText3 = TRUE
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
		
		//Update audio
		IF updatedGodText3 = TRUE
			IF DOES_ENTITY_EXIST(vehCar)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
					IF IS_AUDIO_SCENE_ACTIVE("JSH_1_DRIVE_TO_BACK_ENTRANCE")
						STOP_AUDIO_SCENE("JSH_1_DRIVE_TO_BACK_ENTRANCE")
					ENDIF
					IF NOT IS_AUDIO_SCENE_ACTIVE("JSH_1_GET_TO_ROOF")
						START_AUDIO_SCENE("JSH_1_GET_TO_ROOF")
					ENDIF
					IF doneOLDCREWChat = FALSE
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehCar) > 8
						AND NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						AND NOT IS_MESSAGE_BEING_DISPLAYED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_OLDCREW", CONV_PRIORITY_MEDIUM)
								//So, you keep up with the old crew? ....
								doneOLDCREWChat = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//Pause this chat if it has started
					IF PausedOldCrewChat = FALSE
						IF doneOLDCREWChat = TRUE
							IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								JHS1_OLDCREW1 = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
								KILL_ANY_CONVERSATION()
								PausedOldCrewChat = TRUE
							ENDIF
						ENDIF
					ENDIF	
					//Have Lester tell Michael to get back and check out the roof if he drives off
					IF doneChat46 = FALSE
						IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vplayerStartRoofCoords) > 8
						OR GET_GAME_TIMER() > (iHurryUpRoofTimer + 8000)
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
									IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH46", CONV_PRIORITY_MEDIUM)
										//We need eyes on that roof, Michael. 
										//If you want me to help you with this, go back and find a way up to the roof.
										//You're here, and you're meant to be fifty feet above us.  
										//You know how I work, I need to see everything. Get up on the roof.  
										//This job is only half done. Get some images of the roof please. 
										iChat46Timer = GET_GAME_TIMER()
										doneChat46 = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ELSE
						IF GET_GAME_TIMER() > (iChat46Timer + 10000)
						AND bMissionFailed = FALSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
									IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH46", CONV_PRIORITY_MEDIUM)
										//We need eyes on that roof, Michael. 
										//If you want me to help you with this, go back and find a way up to the roof.
										//You're here, and you're meant to be fifty feet above us.  
										//You know how I work, I need to see everything. Get up on the roof.  
										//This job is only half done. Get some images of the roof please. 
										iChat46Timer = GET_GAME_TIMER()
										doneChat46 = TRUE
									ENDIF
								ENDIF	
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			//Pause the chat so Lester can mention the ladders
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-593.5949, -292.3533, 40.68631>>, <<4.5, 4.5, 4.5>>)
				IF PausedOldCrewChat = FALSE
					IF doneOLDCREWChat = TRUE
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							JHS1_OLDCREW1 = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
							KILL_ANY_CONVERSATION()
							PausedOldCrewChat = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					IF PausedOldCrewChat = TRUE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct, "JHS1AUD", "JHS1_OLDCREW", JHS1_OLDCREW1, CONV_PRIORITY_MEDIUM)
								PausedOldCrewChat = FALSE
							ENDIF
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
			
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-593.5949, -292.3533, 40.68631>>, <<2.5, 2.5, LOCATE_SIZE_HEIGHT>>)
//				IF DOES_BLIP_EXIST(BlipRoofCheck)
//					REMOVE_BLIP(BlipRoofCheck)
//				ENDIF	
				//Do some help text to tell player he can call his buddies in to help as long as FBI 2 hasn't been done.
				IF GET_MISSION_COMPLETE_STATE(SP_MISSION_LAMAR) = FALSE
					IF doneHelp1Text = FALSE
						IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
							
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							
							PRINT_HELP("JH_HELP1")//~s~Walk into ladders to use them.
							doneHelp1Text = TRUE
						ENDIF
					ENDIF
				ENDIF
		
//				BlipRoofCheck = CREATE_BLIP_FOR_COORD(<<-591.8343, -291.3900, 49.3270>>)
				iControlFlag = 2
			ELSE
				IF NOT DOES_BLIP_EXIST(BlipRoofCheck)
					BlipRoofCheck = CREATE_BLIP_FOR_COORD(<<-593.5949, -292.3533, 40.6863>>)
				ENDIF
			ENDIF
		ELSE
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-593.5949, -292.3533, 40.68631>>, <<2.5, 2.5, LOCATE_SIZE_HEIGHT>>)
				iControlFlag = 2
			ENDIF
			IF DOES_BLIP_EXIST(BlipRoofCheck)
				CLEAR_PRINTS()	
				REMOVE_BLIP(BlipRoofCheck)
				PRINT_NOW("WANTED", DEFAULT_GOD_TEXT_TIME, 1)//~s~Lose your wanted level.
			ENDIF				
		ENDIF
		
	ENDIF
	
	IF iControlFlag = 2
		vplayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
//		PRINTSTRING("vplayerCoords = ") PRINTVECTOR(vplayerCoords) PRINTNL()
		//Dialogue Lester tells Michael to go up the stairs
		IF donePH44Chat = FALSE
			IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-594.163025,-292.805176,40.685013>>, <<-595.490173,-293.655762,44.435013>>, 1.250000)
			AND vplayerCoords.z < 42
				IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH44", CONV_PRIORITY_MEDIUM)
					//I saw a ladder. Climb up it and see if you can get to the roof.
					donePH44Chat = TRUE
				ENDIF
			ELSE
				donePH44Chat = TRUE
			ENDIF
		ENDIF		
		
		IF donePH44Chat = TRUE
			IF PausedOldCrewChat = TRUE
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct, "JHS1AUD", "JHS1_OLDCREW", JHS1_OLDCREW1, CONV_PRIORITY_MEDIUM)
						PausedOldCrewChat = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-591.8343, -291.3900, 49.32705>>, <<2, 2, LOCATE_SIZE_HEIGHT>>, TRUE)
			ENDIF
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), <<-591.8343, -291.3900, 49.32705>>) < 15
			AND NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-594.091919,-298.258606,48.078365>>, <<-599.060669,-289.398438,53.927658>>, 8.750000)
			AND vplayerCoords.z > 49
				IF DOES_BLIP_EXIST(BlipRoofCheck)
					REMOVE_BLIP(BlipRoofCheck)
				ENDIF
				playerOnRoof = TRUE
				iControlFlag = 3
			ELSE
				IF NOT DOES_BLIP_EXIST(BlipRoofCheck)
					BlipRoofCheck = CREATE_BLIP_FOR_COORD(<<-591.8343, -291.3900, 49.3270>>)
				ENDIF
			ENDIF
		ELSE
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-590.501221,-293.780090,48.827049>>, <<-592.811951,-289.341644,51.577053>>, 3.250000)
				iControlFlag = 3
			ENDIF
			IF DOES_BLIP_EXIST(BlipRoofCheck)
				CLEAR_PRINTS()	
				REMOVE_BLIP(BlipRoofCheck)
				PRINT_NOW("WANTED", DEFAULT_GOD_TEXT_TIME, 1)//~s~Lose your wanted level.
			ENDIF				
		ENDIF
		
	ENDIF
	
	
//		IF NOT DOES_ENTITY_EXIST(RoofWorker)
//			IF HAS_MODEL_LOADED(S_M_Y_CONSTRUCT_01)
//			AND GET_IS_WAYPOINT_RECORDING_LOADED("BB_JEW_3")
//			AND GET_IS_WAYPOINT_RECORDING_LOADED("BB_JEW_4")
//				RoofWorker = CREATE_PED(PEDTYPE_MISSION, S_M_Y_CONSTRUCT_01, <<-586.7599, -285.6788, 34.4548>>, 41.9557)
//				
//				SET_MODEL_AS_NO_LONGER_NEEDED(S_M_Y_CONSTRUCT_01)
//				OPEN_SEQUENCE_TASK(seqWorker)
//					TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "BB_JEW_3", 10)
//					TASK_CLIMB_LADDER(NULL, FALSE)
//					TASK_CLIMB_LADDER(NULL, FALSE)
//					TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "BB_JEW_4", 10)
//				CLOSE_SEQUENCE_TASK(seqWorker)
//				TASK_PERFORM_SEQUENCE(RoofWorker, seqWorker)
//				CLEAR_SEQUENCE_TASK(seqWorker)
//				
//				//Do dialogue for Lester warning Michael someone is coming..
//				KILL_ANY_CONVERSATION()	
//				iControlFlag = 3
//			ENDIF
//		ENDIF
	
	IF iControlFlag = 3
		
		IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
			IF playerOnRoof = TRUE
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-625.9142, -216.1591, 58.4256>>, <<5,5,5>>)
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-624.356567,-215.621429,60.207253>>, <<-627.705872,-217.354919,58.207253>>, 2.750000)
						IF DOES_BLIP_EXIST(ventsBlip)
							REMOVE_BLIP(ventsBlip)
						ENDIF
						//have some dialogue for getting to the perfect spot
						IF doneChatPH68 = FALSE
							IF GoodShotDone = FALSE
							AND SnapDialogueNeeded = FALSE
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF NOT IS_MESSAGE_BEING_DISPLAYED()
									OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
										IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH68", CONV_PRIORITY_MEDIUM)
											//Right, you should be able to get a good shot of that main vent now.
											REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
											doneChatPH68 = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						//Create a blip and have some chat if the player looks at the vent but hasn't taken a pic of it yet.
						IF NOT DOES_BLIP_EXIST(MainVentBlip)
							IF GoodShotDone = FALSE
							AND SnapDialogueNeeded = FALSE
								IF DOES_ENTITY_EXIST(MainVentPropBall[0])
									IF DOES_ENTITY_EXIST(MainVentPropBall[1])
										IF IS_ENTITY_ON_SCREEN(MainVentPropBall[1])
											IF DOES_ENTITY_EXIST(MainVentPropBall[3])
												IF IS_ENTITY_ON_SCREEN(MainVentPropBall[3])
													IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
														IF NOT IS_MESSAGE_BEING_DISPLAYED()
														OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()	
															IF doneChatPH69 = FALSE
																IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH69", CONV_PRIORITY_MEDIUM)
																	//Ok Michael, get a pic of that vent and we're done.
																	REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
																	doneChatPH69 = TRUE
																ENDIF
															ENDIF
															MainVentBlip = CREATE_BLIP_FOR_OBJECT(MainVentPropBall[0])
														ENDIF
													ENDIF
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ELSE
							IF GoodShotDone = FALSE
							AND GlassesCamActive = FALSE
								IF doneGodTextMainVent = FALSE
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()				
											PRINT_NOW("GOD7", DEFAULT_GOD_TEXT_TIME, 1)
											//~s~Take a photo of the ~g~vent.
											iVentGodTextTimer = GET_GAME_TIMER()
											doneGodTextMainVent = TRUE
										ENDIF
									ENDIF
								ENDIF
							ELSE
								IF DOES_BLIP_EXIST(MainVentBlip)
									REMOVE_BLIP(MainVentBlip)
								ENDIF
							ENDIF
						ENDIF
						
						playerHasBeenAtPerfectPlace = TRUE
						playerInPerfectPlace = TRUE
					ELSE
						IF DOES_BLIP_EXIST(MainVentBlip)
							IF GET_GAME_TIMER() < (iVentGodTextTimer + DEFAULT_GOD_TEXT_TIME)
							AND IS_MESSAGE_BEING_DISPLAYED()
								CLEAR_PRINTS()
							ENDIF
							REMOVE_BLIP(MainVentBlip)
						ENDIF
						IF moveStageOnTimerSet = FALSE
							IF doneVentsBlipText = TRUE
								IF NOT DOES_BLIP_EXIST(ventsBlip)
									ventsBlip = CREATE_BLIP_FOR_COORD(<<-625.8755, -216.6342, 58.4256>>)
									PRINTSTRING("creating blip as player took too long") PRINTNL()
								ELSE
									//Draw the locate if the blip exists
									IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-625.8755, -216.6342, 58.4256>>,<<2, 2, LOCATE_SIZE_HEIGHT>>, TRUE, TRUE, TM_ANY)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						playerInPerfectPlace = FALSE
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(MainVentBlip)
						REMOVE_BLIP(MainVentBlip)
					ENDIF
					IF moveStageOnTimerSet = FALSE
						IF doneVentsBlipText = TRUE
							IF NOT DOES_BLIP_EXIST(ventsBlip)
								ventsBlip = CREATE_BLIP_FOR_COORD(<<-625.8755, -216.6342, 58.4256>>)
								PRINTSTRING("creating blip as player took too long") PRINTNL()
							ELSE
								//Draw the locate if the blip exists
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-625.8755, -216.6342, 58.4256>>,<<2, 2, LOCATE_SIZE_HEIGHT>>, TRUE, TRUE, TM_ANY)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					playerInPerfectPlace = FALSE
				ENDIF
			ENDIF
			
			vplayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			
			//checking if player is on the rooftop or not.
			IF vplayerCoords.z < 48
				playerOnRoof = FALSE
			ELSE
				playerOnRoof = TRUE
			ENDIF						
		
			
			//Have a timer to start the hurry up chat from Lester if Michael is taking too long
			IF PlayerCantFindVentsTimerSet = FALSE
				IF playerOnRoof = TRUE
					iPlayerCantFindVents = GET_GAME_TIMER()
					PlayerCantFindVentsTimerSet = TRUE
				ENDIF
			ENDIF			
			
			//Add a blip for the vents and some chat if player takes too long to find them
			IF GoodShotDone = FALSE
				IF playerInPerfectPlace = FALSE
					IF donePH51Chat = FALSE
						IF playerHasBeenAtPerfectPlace = FALSE
							IF PlayerCantFindVentsTimerSet = TRUE
								IF GET_GAME_TIMER() > (iPlayerCantFindVents + 40000)
								OR doneVentChat = TRUE
									IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
										IF NOT IS_MESSAGE_BEING_DISPLAYED()
										OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
											CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH51", CONV_PRIORITY_MEDIUM)
											//According to my smart phone, the highest point seems to be at the North West side.
											//You should be able to get a good shot from there. 
											donePH51Chat = TRUE
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//update God Text
			IF playerIsHighEnough = FALSE
				IF moveStageOnTimerSet = FALSE
					IF doneVentsBlipText = FALSE
						IF donePH51Chat = TRUE
						OR playerHasBeenAtPerfectPlace = TRUE
//							IF DOES_BLIP_EXIST(ventsBlip)
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									CLEAR_PRINTS()
									PRINT_NOW("GOD4", DEFAULT_GOD_TEXT_TIME, -1)//~s~Go to the ~y~vantage point
									doneVentsBlipText = TRUE
								ENDIF
//							ENDIF
						ENDIF
					ENDIF
				ENDIF
				IF doneVentsGodText = FALSE
					IF donePH42Chat = TRUE
					AND doneDESCChat = TRUE
						IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							CLEAR_PRINTS()
							PRINT_NOW("GOD5", DEFAULT_GOD_TEXT_TIME, -1)// ~s~Get to high ground and take some shots.
							doneVentsGodText = TRUE
						ENDIF
					ENDIF
				ENDIF
				IF doneVentsGodText = FALSE
					IF playerHasBeenAtPerfectPlace = TRUE
						IF moveStageOnTimerSet = FALSE
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								CLEAR_PRINTS()
								PRINT_NOW("GOD5", DEFAULT_GOD_TEXT_TIME, -1)// ~s~Get to high ground and take some pics.
								doneVentsGodText = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF	
			ENDIF
			
			//Dialogue Lester tells Michael to find the vents
			IF donePH42Chat = FALSE
				//Pause their chat so Lester can give instructions on what to do on the roof
				IF PausedOldCrewChat = FALSE
					IF doneOLDCREWChat = TRUE
						IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							JHS1_OLDCREW1 = GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
							KILL_ANY_CONVERSATION()
							PRINTSTRING("KILL_ANY_CONVO CALLED 1") PRINTNL()
							PausedOldCrewChat = TRUE
						ENDIF
					ENDIF
				ENDIF
				IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH42", CONV_PRIORITY_MEDIUM)
					//I'm on the roof.
					donePH42Chat = TRUE
				ENDIF
			ENDIF
			IF donePH42Chat = TRUE
				IF doneDESCChat = FALSE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_DESC", CONV_PRIORITY_MEDIUM)
								//Alright, use the glasses to get me a shot of Vangelico's roof unit. It'll be right above the store.
								//I need to know where the air coming out those vents in Vangelico originates. There should be a unit above the store.
								doneDESCChat = TRUE
							ENDIF
						ENDIF
					ENDIF
				ELSE
					//Resume the oldcrew chat now
					IF playerIsHighEnough = FALSE
					AND moveStageOnTimerSet = FALSE
						IF doneVentsGodText = TRUE
						AND NOT IS_MESSAGE_BEING_DISPLAYED()
							IF PausedOldCrewChat = TRUE
								IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
									IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct, "JHS1AUD", "JHS1_OLDCREW", JHS1_OLDCREW1, CONV_PRIORITY_MEDIUM)
										PausedOldCrewChat = FALSE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF		
			
			//Do some chat once player gets above the store at the vents
			IF doneVentChat = FALSE
				IF playerIsHighEnough = TRUE
				AND NOT IS_PED_CLIMBING(PLAYER_PED_ID())
//					PRINTSTRING("playerIsHighEnough = TRUE") PRINTNL()
					KILL_ANY_CONVERSATION()
					PRINTSTRING("KILL_ANY_CONVO CALLED 3") PRINTNL()
					IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH52", CONV_PRIORITY_MEDIUM)
						//I can see the vents from up here.
						//Yeah i'm seeing them. Can't you get any higher up Michael? 
						PRINTSTRING("Creating conversation PH52") PRINTNL()
						doneVentChat = TRUE
					ENDIF
				ELSE
//					PRINTSTRING("playerIsHighEnough = FALSE") PRINTNL()
				ENDIF
			ELSE
//				PRINTSTRING("doneVentChat = TRUE") PRINTNL()
			ENDIF
			
			//Start the move stage on timer once the player has taken a photo whilst at the vents location
			IF moveStageOnTimerSet = FALSE
				IF GoodShotDone = TRUE
					iMoveStageOnTimer = GET_GAME_TIMER()
					moveStageOnTimerSet = TRUE
				ENDIF
			ENDIF	
									
			//Do some dialogue if player is taking pics from a bad point.
			IF GoodShotDone = FALSE
			AND playerInPerfectPlace = FALSE
				IF playerIsHighEnough = FALSE
					IF playerTakingPhoto = TRUE
						IF GET_GAME_TIMER() > (NotHighEnoughTimer + 5000)
							PRINTSTRING("KILL_ANY_CONVO CALLED 4") PRINTNL()
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
									IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH54", CONV_PRIORITY_MEDIUM)
										//You need to get higher up Michael.
										//This is no good try and get higher.
										//You are not high enough.
										NotHighEnoughTimer = GET_GAME_TIMER()
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Update Audio when glasses are being used.
			IF GlassesCamActive = FALSE
				IF IS_AUDIO_SCENE_ACTIVE("JSH_1_TAKE_PHOTOS")
					STOP_AUDIO_SCENE("JSH_1_TAKE_PHOTOS")
				ENDIF
				IF NOT IS_AUDIO_SCENE_ACTIVE("JSH_1_GET_TO_ROOF")
					START_AUDIO_SCENE("JSH_1_GET_TO_ROOF")
				ENDIF		
			ELSE
				IF IS_AUDIO_SCENE_ACTIVE("JSH_1_GET_TO_ROOF")
					STOP_AUDIO_SCENE("JSH_1_GET_TO_ROOF")
				ENDIF
				IF NOT IS_AUDIO_SCENE_ACTIVE("JSH_1_TAKE_PHOTOS")
					START_AUDIO_SCENE("JSH_1_TAKE_PHOTOS")
				ENDIF					
			ENDIF
			
			//Lester should give Michael shit if he is off the roof at this stage
			IF moveStageOnTimerSet = FALSE
				IF playerOnRoof = FALSE
					IF NOT DOES_BLIP_EXIST(BlipRoofCheck)
						BlipRoofCheck = CREATE_BLIP_FOR_COORD(<<-591.8343, -291.3900, 49.3270>>)
					ENDIF
					IF DOES_BLIP_EXIST(ventsBlip)
						REMOVE_BLIP(ventsBlip)
					ENDIF
					//Dialogue Lester tells Michael to get back to the roof
					IF donePH45Chat = FALSE
						IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH45", CONV_PRIORITY_MEDIUM)
							//What are you doing? Get back to the roof Michael. Stop messing around.
							//Michael, hurry up. Get a snap of the roof and let's get out of here.
							iWarnMichaelChatTimer = GET_GAME_TIMER()
							donePH45Chat = TRUE
						ENDIF
					ENDIF
					
					IF IS_VEHICLE_DRIVEABLE(vehCar)
						IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
							IF donePH46Chat = FALSE
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH46", CONV_PRIORITY_MEDIUM)
									//What are you doing? We need to find a way up to the rooftop.
									//Get out the car and find us a way to the roof. Hurry up.
									iWarnMichaelChatTimer = GET_GAME_TIMER()
									donePH46Chat = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF GET_GAME_TIMER() > (iWarnMichaelChatTimer + 20000)
						donePH45Chat = FALSE
						donePH46Chat = FALSE
					ENDIF
				ELSE
					IF DOES_BLIP_EXIST(BlipRoofCheck)
						REMOVE_BLIP(BlipRoofCheck)	
					ENDIF
				ENDIF
			ENDIF		
			
			//Move the stage on once player has been to the vents and has taken a photo of the them
			IF moveStageOnTimerSet = TRUE	
				IF GET_GAME_TIMER() >  (iMoveStageOnTimer + 10000)
				OR donePH43Chat = TRUE
					IF DOES_BLIP_EXIST(ventsBlip)
						REMOVE_BLIP(ventsBlip)
					ENDIF					
				
					//Dialogue Lester tells Michael to take some pics
					IF donePH43Chat = FALSE
						IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH43", CONV_PRIORITY_MEDIUM)
							//Okay. That'll do. Come back to me before someone spots you up there.
							//Will do.
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							donePH43Chat = TRUE
						ENDIF
					ENDIF
					
					TRIGGER_MUSIC_EVENT("JH1_STOP_TRACK_ACTION")
					
					iHurryUpChatTimer = GET_GAME_TIMER()
					iControlFlag = 4
					PRINTSTRING("Moving stage on") PRINTNL()
				ENDIF
			ENDIF				
			
			//Have some dialogue if player is taking too long to exit the glasses
			IF donePH43Chat = TRUE
				IF GET_GAME_TIMER() > (iHurryUpChatTimer + 13000)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH48", CONV_PRIORITY_MEDIUM)
								//Come on, Michael. Let's go.
								//WLet's hurry this up, Michael.
								//If you take any longer, you're risking blowing this whole thing.
								//Let's speed this up, we don't want to draw attention.
								iHurryUpChatTimer = GET_GAME_TIMER()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			IF DOES_BLIP_EXIST(BlipRoofCheck)
				CLEAR_PRINTS()	
				REMOVE_BLIP(BlipRoofCheck)
				PRINT_NOW("WANTED", DEFAULT_GOD_TEXT_TIME, 1)//~s~Lose your wanted level.
			ENDIF
			IF DOES_BLIP_EXIST(ventsBlip)
				REMOVE_BLIP(ventsBlip)
				PRINT_NOW("WANTED", DEFAULT_GOD_TEXT_TIME, 1)//~s~Lose your wanted level.
			ENDIF
		ENDIF
	ENDIF	
//		//Dialogue Lester warns Michael that someone is coming.
//		IF donePH30Chat = FALSE
//			IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH30", CONV_PRIORITY_MEDIUM)
//				//Shit, someone's coming Michael, hide quick!
//				donePH30Chat = TRUE
//			ENDIF
//		ENDIF
//		
//		IF vplayerCoords.z < vworkerCoords.z
//		AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), RoofWorker) > 5
//			IF donePH31Chat = FALSE
//				IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH31", CONV_PRIORITY_MEDIUM)
//					//That was close, i'm coming back.
//					donePH31Chat = TRUE
//				ENDIF
//			ENDIF		
//			IF donePH31Chat = TRUE
//				IF IS_VEHICLE_DRIVEABLE(vehCar)
//					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//					OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//						IF NOT IS_MESSAGE_BEING_DISPLAYED()
//						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
//							IF NOT DOES_BLIP_EXIST(BlipVeh)
//								BlipVeh = CREATE_BLIP_FOR_VEHICLE(vehCar)
//								PRINT_NOW("GOTO_CAR2", DEFAULT_GOD_TEXT_TIME, 1)//~s~Get back in the ~b~car.
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF

	IF iControlFlag = 4	
		
		IF donePH43Chat = FALSE
			IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH43", CONV_PRIORITY_MEDIUM)
				//Okay. That'll do. Come back to me before someone spots you up there.
				//Will do.
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				donePH43Chat = TRUE
			ENDIF
		ENDIF		
		
		IF doneGotoCar2Text = FALSE
			IF donePH43Chat = TRUE
				IF IS_VEHICLE_DRIVEABLE(vehCar)
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
					OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							CLEAR_HELP()
							PRINT_NOW("GOTO_CAR5", DEFAULT_GOD_TEXT_TIME, 1)//~s~Get back in your ~b~car.
							doneGotoCar2Text = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
		//Resume the other chat if required now.
		IF doneGotoCar2Text = TRUE
		AND donePH43Chat = TRUE
			IF doneVentsGodText = TRUE
			AND NOT IS_MESSAGE_BEING_DISPLAYED()
				IF PausedOldCrewChat = TRUE
					IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(MyLocalPedStruct, "JHS1AUD", "JHS1_OLDCREW", JHS1_OLDCREW1, CONV_PRIORITY_MEDIUM)
							PausedOldCrewChat = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
		ENDIF
		
//		//Do some more help text for sliding down ladders here
//		IF GET_MISSION_COMPLETE_STATE(SP_MISSION_LAMAR) = FALSE
//			IF doneHelp2Text = FALSE
//				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-594.480164,-294.014130,47.435013>>, <<-595.368652,-292.485565,40.935013>>, 1.500000)
//					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
//						PRINT_HELP("JH_HELP2")//~s~Hold ~INPUT_SPRINT~ when going down a ladder to perform a slide.
//						doneHelp2Text = TRUE
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehCar)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
				
				RELEASE_MISSION_AUDIO_BANK()
				
				IF DOES_BLIP_EXIST(BlipVeh)
					REMOVE_BLIP(BlipVeh)
					IF NOT IS_PED_INJURED(pedContact)
						TASK_CLEAR_LOOK_AT(pedContact)
					ENDIF
					
					SET_ROADS_IN_AREA(<<-611.9, -362.4, 30>>, <<-558.6, -264.5, 45>>, TRUE)
					SET_ROADS_IN_AREA(<<-538.9, -296.7, 43.3>>, <<-631.3, -185.2, 30>>, TRUE)
					REPLAY_RECORD_BACK_FOR_TIME(8.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
					iControlFlag = 0
					missionStage = STAGE_DRIVE_TO_WAREHOUSE
				ENDIF

			ELSE
				IF NOT DOES_BLIP_EXIST(BlipVeh)
					BlipVeh = CREATE_BLIP_FOR_VEHICLE(vehCar)
				ENDIF
				
				//Update audio
				IF IS_AUDIO_SCENE_ACTIVE("JSH_1_GET_TO_ROOF")
					STOP_AUDIO_SCENE("JSH_1_GET_TO_ROOF")
				ENDIF
				IF IS_AUDIO_SCENE_ACTIVE("JSH_1_TAKE_PHOTOS")
					STOP_AUDIO_SCENE("JSH_1_TAKE_PHOTOS")
				ENDIF
				IF NOT IS_AUDIO_SCENE_ACTIVE("JSH1_RETURN_TO_CAR")
					START_AUDIO_SCENE("JSH1_RETURN_TO_CAR")
				ENDIF					
				
				//Have some random dialogue of Lester telling Michael to hurry up if he's taking too long to get back to the car.
				IF GET_GAME_TIMER() > (iHurryUpChatTimer + 40000)
					IF DOES_ENTITY_EXIST(pedContact)
						IF NOT IS_PED_INJURED(pedContact)
							IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedContact) > 8
								IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH48", CONV_PRIORITY_MEDIUM)
									//Hurry up Michael, before someone sees you.
									//What are you doing? Hurry up and get back to the car.
									//Michael are you trying to destroy this job before it's even started? Hurry up!
									iHurryUpChatTimer = GET_GAME_TIMER()
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		ENDIF
	
	ENDIF

ENDPROC

////PURPOSE: Stage where there is 1 long cutscene of player entering shop with Lester and taking a look around and then leaving
//PROC DO_STAGE_SHOP_CUTSCENE()
//
//	IF iControlFlag = 0
//		
//		SWITCH iCutsceneStage
//		
//			CASE 0
//				
//				//**************SET MID MISSION CHECK POINT HERE*******************//
//				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "STAGE_SHOP_CUTSCENE", TRUE)
//				PRINTSTRING("***********MID MISSION STAGE SET TO 1***********") PRINTNL()
//				
//				//DEBUG STAGE SELECTOR
//				IF MissionStageBeingSkippedTo = TRUE
//					STAGE_SELECTOR_MISSION_SETUP()
//					MissionStageBeingSkippedTo = FALSE
//				ENDIF
//				
//				CanMissionFail 			= FALSE
//				doneHelpText 			= FALSE
//				doneHelpText2			= FALSE
//				doneHelpText3			= FALSE
//				Cam1SceneSetup 			= FALSE	
//				Cam2SceneSetup			= FALSE	
//				Cam3SceneSetup			= FALSE	
////				buttonPressed 			= FALSE
//				IcamStage 				= 0
//				
//				DELETE_TEMP_STORE_STAFF()
//				
//				CLEAR_AREA(<<-628.45, -236.06, 38.01>>, 15, TRUE)
//				SET_PED_NON_CREATION_AREA(<<-636.08, -240.55, 36>>, <<-616.77, -226.67, 40>>)
//				
//				WHILE NOT HAS_CUTSCENE_LOADED()
//					WAIT(0)
//					PRINTSTRING("Stuck waiting for cutscene to load..") PRINTNL()
//				ENDWHILE
//				
//				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//				
//				//Register entities for cutscene
//				//Lester
//				IF NOT IS_PED_INJURED(pedContact)
//					REGISTER_ENTITY_FOR_CUTSCENE(pedContact, "Lester", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				//Players car
//				IF IS_VEHICLE_DRIVEABLE(vehCar)
//					REGISTER_ENTITY_FOR_CUTSCENE(vehCar, "Michaels_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				//Shop Guard
//				IF NOT IS_PED_INJURED(shopGuard)
//					REGISTER_ENTITY_FOR_CUTSCENE(shopGuard, "Jewellery_security", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				//Shop Assistant
//				IF NOT IS_PED_INJURED(shopAssistant)
//					REGISTER_ENTITY_FOR_CUTSCENE(shopAssistant, "Jewellery_Assitance", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				
//				CLEAR_AREA(<<-682, -236, 36>>, 20, TRUE)
//				
//				//Configure script systems into a safe mode.
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//							
//				
//				START_CUTSCENE()
//				
//				//For ambient sounds set to 0,0 after cutscene
//				SET_PED_WALLA_DENSITY(1, 0.8)
//				
////				SET_SEAMLESS_CUTS_ACTIVE(bTracker)
//
//				IF NOT IS_PED_INJURED(pedContact)
//					IF IS_PED_GROUP_MEMBER(pedContact, PLAYER_GROUP_ID())
//						REMOVE_PED_FROM_GROUP(pedContact)
//					ENDIF
//				ENDIF				
//				
//				IF IS_SCREEN_FADED_OUT()
//					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//				ENDIF
//				
//				iCutsceneStage ++
//				
//			BREAK
//			
//			CASE 1
//				
//				REQUEST_MODEL(U_M_M_JEWELSEC_01)//security guard
//				REQUEST_MODEL(A_F_Y_Business_01)//Shop assistant
//				REQUEST_MODEL(CS_JEWELASS)//Shop assistant 2
//				REQUEST_MODEL(TAILGATER)
//				REQUEST_NPC_PED_MODEL(CHAR_LESTER)
//				REQUEST_ANIM_DICT("MISSHEIST_JEWEL")
////				REQUEST_MODEL(A_F_Y_BevHills_03)//High End customer
//				
//				WHILE NOT HAS_ANIM_DICT_LOADED("MISSHEIST_JEWEL")
//					PRINTSTRING("Waiting for anim set to load") PRINTNL()
//					WAIT(0)
//				ENDWHILE				
//				
//				//Check for skipping cutscene
//				IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_SPRINT)
//					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//						CLEAR_HELP(TRUE)
//					ENDIF					
//					STOP_CUTSCENE()
//				ENDIF
//				
////				PRINTSTRING("Cutscene time is ") PRINTINT(GET_CUTSCENE_TIME()) PRINTNL()
//				
//				//Print help text to tell the player to press circle to view other camera angles.
//				IF doneHelpText = FALSE
//					IF GET_CUTSCENE_TIME() > 20000
//						PRINT_HELP("VIEW")//Hold ~INPUT_VEH_CIN_CAM~ to view Lester cam.
//						doneHelpText = TRUE
//					ENDIF
//				ENDIF
//				IF doneHelpText2 = FALSE
//					IF GET_CUTSCENE_TIME() > 56500
//						PRINT_HELP("VIEW")//Hold ~INPUT_VEH_CIN_CAM~ to view Lester cam.
//						doneHelpText2 = TRUE
//					ENDIF
//				ENDIF
//				IF doneHelpText3 = FALSE
//					IF GET_CUTSCENE_TIME() > 87000
//						PRINT_HELP("VIEW")//Hold ~INPUT_VEH_CIN_CAM~ to view Lester cam.
//						doneHelpText3 = TRUE
//					ENDIF
//				ENDIF
//				
//				//Set up Lester Cam
//				IF GET_CUTSCENE_TIME() > 20000
//				AND GET_CUTSCENE_TIME() < 121000
//				
//					SWITCH IcamStage
//					
//						CASE 0
//							
//							IF GET_CUTSCENE_TIME() < 56500
//								IF NOT DOES_CAM_EXIST(Cam1)
//									Cam1 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
//								ELSE
////									PLAY_CAM_ANIM(Cam1, "CAM_JH_1_MCS_1p2_lestercam", "MISSHEIST_JEWEL", <<-632.386, -238.258, 36.997>>, <<0,0,0>>)
////									RENDER_SCRIPT_CAMS(TRUE, FALSE)
//	       							
//									IF Cam1SceneSetup = FALSE
//										Cam1Scene = CREATE_SYNCHRONIZED_SCENE(<<-632.386, -238.258, 36.997>>, <<0,0,0>>)
//		                                PLAY_SYNCHRONIZED_CAM_ANIM(Cam1, Cam1Scene, "CAM_JH_1_MCS_1p2_lestercam", "MISSHEIST_JEWEL")
//		                                SET_SYNCHRONIZED_SCENE_LOOPED(Cam1Scene, FALSE)
//		                                SET_CAM_ACTIVE(Cam1, TRUE)
//		                                Cam1SceneSetup = TRUE
//									ENDIF
//									
//								ENDIF
//								
//							ELSE
//								IcamStage ++
//							ENDIF
//						
//						BREAK
//						
//						CASE 1
//							
//							IF GET_CUTSCENE_TIME() < 85000
//								IF NOT DOES_CAM_EXIST(Cam2)
//									Cam2 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
//								ELSE
////									PLAY_CAM_ANIM(Cam2, "CAM_JH_1_MCS_2_lestercam", "MISSHEIST_JEWEL", <<-632.386, -238.258, 36.997>>, <<0,0,0>>)
////									RENDER_SCRIPT_CAMS(TRUE, FALSE)
//									IF Cam2SceneSetup = FALSE
//										Cam2Scene = CREATE_SYNCHRONIZED_SCENE(<<-632.386, -238.258, 36.997>>, <<0,0,0>>)
//		                                PLAY_SYNCHRONIZED_CAM_ANIM(Cam2, Cam2Scene, "CAM_JH_1_MCS_2_lestercam", "MISSHEIST_JEWEL")
//		                                SET_SYNCHRONIZED_SCENE_LOOPED(Cam2Scene, FALSE)
//										SET_CAM_ACTIVE(Cam1, FALSE)
//		                                SET_CAM_ACTIVE(Cam2, TRUE)
//										Cam2SceneSetup = TRUE
//									ENDIF
//								ENDIF
//							ELSE
//								IcamStage ++
//							ENDIF
//						
//						BREAK
//						
//						CASE 2
//							
//							IF GET_CUTSCENE_TIME() > 87000
//								IF NOT DOES_CAM_EXIST(Cam3)
//									Cam3 = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
//								ELSE
////									PLAY_CAM_ANIM(Cam3, "CAM_JH_1_MCS_3p1_lestercam", "MISSHEIST_JEWEL", <<-632.386, -238.258, 36.997>>, <<0,0,0>>)
////									RENDER_SCRIPT_CAMS(TRUE, FALSE)
//									IF Cam3SceneSetup = FALSE
//										Cam3Scene = CREATE_SYNCHRONIZED_SCENE(<<-632.386, -238.258, 36.997>>, <<0,0,0>>)
//		                                PLAY_SYNCHRONIZED_CAM_ANIM(Cam3, Cam3Scene, "CAM_JH_1_MCS_3p1_lestercam", "MISSHEIST_JEWEL")
//		                                SET_SYNCHRONIZED_SCENE_LOOPED(Cam3Scene, FALSE)
//										SET_CAM_ACTIVE(Cam2, FALSE)
//		                                SET_CAM_ACTIVE(Cam3, TRUE)
//										Cam3SceneSetup = TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//							
//						BREAK
//					
//					ENDSWITCH				
//					
//					IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
//						
//						//set flags for help text 
//						doneHelpText = TRUE
//						doneHelpText2 = TRUE
//						doneHelpText3 = TRUE
//						
//						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//							CLEAR_HELP(TRUE)
//						ENDIF
//						
//						//INFORM_STAT_JEWELERY_HEIST_SETUP_LESTER_CAM_USED()						
//						
//						RENDER_SCRIPT_CAMS(TRUE, FALSE)
//						BYPASS_CUTSCENE_CAM_RENDERING_THIS_UPDATE()
//						
//					ELSE
//						RENDER_SCRIPT_CAMS(FALSE, FALSE)
//					ENDIF
//				ELSE
//					RENDER_SCRIPT_CAMS(FALSE, FALSE)
//				ENDIF
//				
//				#IF IS_DEBUG_BUILD
//					IF NOT HAS_CUTSCENE_FINISHED()
//						IF IS_KEYBOARD_KEY_PRESSED(KEY_J)
//							IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//								CLEAR_HELP(TRUE)
//							ENDIF
//							STOP_CUTSCENE()
//						ENDIF
//					ENDIF
//				#ENDIF
//					
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
//					IF IS_VEHICLE_DRIVEABLE(vehCar)
//						IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar, VS_DRIVER)
//							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//						ENDIF
//					ENDIF
//				ENDIF					
//				
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lester")
//					IF IS_VEHICLE_DRIVEABLE(vehCar)
//						IF NOT IS_PED_INJURED(pedContact)
//							SET_PED_INTO_VEHICLE(pedContact, vehCar, VS_FRONT_RIGHT)
//						ENDIF
//					ENDIF
//				ENDIF					
//					
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_car")
//					IF IS_VEHICLE_DRIVEABLE(vehCar)
//						CLEAR_AREA(<<-662.3000, -265.4466, 34.9446>>, 15, TRUE)
//						SET_ENTITY_COORDS(vehCar, <<-662.3000, -265.4466, 34.9446>>)
//						SET_ENTITY_HEADING(vehCar, 30.6670)						
//						SET_VEHICLE_ON_GROUND_PROPERLY(vehCar)
//					ENDIF
//				ENDIF				
//					
//				IF NOT IS_CUTSCENE_ACTIVE()
//					
//					//Set the ambient sound back to normal
//					SET_PED_WALLA_DENSITY(0, 0)
//					
//					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
//						CLEAR_HELP(TRUE)
//					ENDIF	
//					
//					REMOVE_ANIM_DICT("MISSHEIST_JEWEL")
//					iCutsceneStage++
//				ENDIF	
//				
//			BREAK	
//			
//			CASE 2
//				
////				WHILE NOT HAS_MODEL_LOADED(TAILGATER)
////				OR NOT HAS_NPC_PED_MODEL_LOADED(CHAR_LESTER)
////					WAIT(0)
////				ENDWHILE
//				
////				IF NOT DOES_ENTITY_EXIST(vehCar)
////					CREATE_PLAYER_VEHICLE(vehCar, CHAR_MICHAEL, << -682.4386, -236.8085, 35.7185 >>, 26.5773)
////					SET_VEHICLE_MODEL_IS_SUPPRESSED(TAILGATER, TRUE)
////					ADD_VEHICLE_UPSIDEDOWN_CHECK(vehCar)
////				ENDIF
////
////				IF NOT DOES_ENTITY_EXIST(pedContact)
////					CREATE_NPC_PED_INSIDE_VEHICLE(pedContact, CHAR_LESTER, vehCar, VS_FRONT_RIGHT, TRUE)	
////					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedContact, TRUE)
////					SET_PED_CAN_BE_DRAGGED_OUT(pedContact, FALSE)
////				ENDIF
//				
//				IF IS_VEHICLE_DRIVEABLE(vehCar)
//					IF NOT IS_PED_INJURED(pedContact)
//						IF NOT IS_PED_IN_VEHICLE(pedContact, vehCar)
//							SET_PED_INTO_VEHICLE(pedContact, vehCar, VS_FRONT_RIGHT)
//						ENDIF
//					ENDIF
//					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
//						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar, VS_DRIVER)
//					ENDIF	
////					SET_ENTITY_COORDS(vehCar, << -682.4386, -236.8085, 35.7185 >>, FALSE)
////					SET_ENTITY_HEADING(vehCar, 26.5773)
//					SET_VEHICLE_ON_GROUND_PROPERLY(vehCar)
//				ENDIF
//				
////				SET_MODEL_AS_NO_LONGER_NEEDED(TAILGATER)
////				SET_NPC_PED_MODEL_AS_NO_LONGER_NEEDED(CHAR_LESTER)
//				
//				
////				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//				
//				PRINTSTRING("CASE 2") PRINTNL()
//				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//				
//				//Return script systems to normal.
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//				iCutsceneStage++
//				
//			BREAK
//			
//			CASE 3
//				
//				CanMissionFail = TRUE
//				CheckpointReplayStarting = FALSE
//				iCutsceneStage = 0
//				iControlFlag = 0
//				missionStage = STAGE_DRIVE_TO_WAREHOUSE
//				
//			BREAK
//			
//		ENDSWITCH
//		
//	ENDIF
//	
//ENDPROC

////PURPOSE:		Cutscene showing Michael and Lester arriving at the jewellers and walking up to the door.
//PROC DO_STAGE_ARRIVAL_CUTSCENE()
//
//	IF iControlFlag = 0
//	
//		SWITCH iCutsceneStage
//		
//			CASE 0 	
//				
//				//DEBUG STAGE SELECTOR
//				#IF IS_DEBUG_BUILD
//					IF MissionStageBeingSkippedTo = TRUE
//						STAGE_SELECTOR_MISSION_SETUP()
//						MissionStageBeingSkippedTo = FALSE
//					ENDIF
//				#ENDIF					
//				
//				CanMissionFail = FALSE
//				
//				WHILE NOT HAS_CUTSCENE_LOADED()
//					WAIT(0)
//					PRINTSTRING("Stuck waiting for cutscene to load..") PRINTNL()
//				ENDWHILE
//				
//				//temp move car out of shot of mocap, this will be removed once mocap string names have been added
////				IF IS_VEHICLE_DRIVEABLE(vehCar)
////					SET_ENTITY_COORDS(vehCar, << -679.6872, -218.7484, 35.9554 >>)
////					SET_ENTITY_HEADING(vehCar, 303.0053)
////				ENDIF
//				
//				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//				
//				//Register entities for cutscene
//				//Lester
//				IF NOT IS_PED_INJURED(pedContact)
//					REGISTER_ENTITY_FOR_CUTSCENE(pedContact, "A_M_M_Salton_02", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				//Players car
//				IF IS_VEHICLE_DRIVEABLE(vehCar)
//					REGISTER_ENTITY_FOR_CUTSCENE(vehCar, "TAILGATER", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				//Shop Guard
//				IF NOT IS_PED_INJURED(shopGuard)
//					REGISTER_ENTITY_FOR_CUTSCENE(shopGuard, "U_M_M_JEWELSEC_01", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				//Shop Assistant
//				IF NOT IS_PED_INJURED(shopAssistant)
//					REGISTER_ENTITY_FOR_CUTSCENE(shopAssistant, "A_F_Y_Business_01", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				
//				CLEAR_AREA(<<-682, -236, 36>>, 20, TRUE)
//				
//				//Configure script systems into a safe mode.
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//				
//				START_CUTSCENE()
//				
//				SET_SEAMLESS_CUTS_ACTIVE(bTracker)
//
//				IF NOT IS_PED_INJURED(pedContact)
//					IF IS_PED_GROUP_MEMBER(pedContact, PLAYER_GROUP_ID())
//						REMOVE_PED_FROM_GROUP(pedContact)
//					ENDIF
//				ENDIF				
//				
//				iCutsceneStage ++
//				
//			BREAK
//			
//			CASE 1
//				
//				REQUEST_MODEL(U_M_M_JEWELSEC_01)//security guard
//				REQUEST_MODEL(A_F_Y_Business_01)//Shop assistant
//				REQUEST_MODEL(CS_JEWELASS)//Shop assistant 2
//				REQUEST_MODEL(A_F_Y_BevHills_03)//High End customer
//				
//				IF HAS_CUTSCENE_FINISHED()
//			      	//Return script systems to normal.
//					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//					
////					//temp move car out of shot of mocap, this will be removed once mocap string names have been added
////					IF IS_VEHICLE_DRIVEABLE(vehCar)
////						SET_ENTITY_COORDS(vehCar, << -679.5807, -241.5680, 35.6317 >>)
////						SET_ENTITY_HEADING(vehCar, 28.7210)
////					ENDIF					
//					
//					iCutsceneStage++
//				ENDIF	
//				
//			BREAK
//			
//			CASE 2
//				
//				//Make sure models have loaded for shop staff
//				WHILE NOT HAS_MODEL_LOADED(U_M_M_JEWELSEC_01)
//				OR NOT HAS_MODEL_LOADED(A_F_Y_Business_01)
//				OR NOT HAS_MODEL_LOADED(CS_JEWELASS)	
//				OR NOT HAS_MODEL_LOADED(A_F_Y_BevHills_03)
//					WAIT(0)
//				ENDWHILE
//				
//				//Set up shop staff and position them for end of cutscene
//				IF NOT DOES_ENTITY_EXIST(ShopGuard)
//					ShopGuard = CREATE_PED(PEDTYPE_MISSION, U_M_M_JEWELSEC_01, << -630.8696, -235.2035, 37.0570 >>, 248.2723)
//					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, shopGuard, "PED1")
//		//			RETAIN_ENTITY_IN_INTERIOR(shopGuard, InteriorJewelleryShop)
//				ENDIF 
//				SET_MODEL_AS_NO_LONGER_NEEDED(U_M_M_JEWELSEC_01)
//				
//				IF NOT DOES_ENTITY_EXIST(ShopAssistant)
//					ShopAssistant = CREATE_PED(PEDTYPE_MISSION, CS_JEWELASS, << -622.8374, -229.5900, 37.0570 >>, 314.7467)
//		//			RETAIN_ENTITY_IN_INTERIOR(ShopAssistant, InteriorJewelleryShop)
//				ENDIF
//				SET_MODEL_AS_NO_LONGER_NEEDED(CS_JEWELASS)
//				
//				IF NOT DOES_ENTITY_EXIST(shopAssistant)
//					shopAssistant = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Business_01, << -618.6191, -235.5955, 37.0570 >>, 36.0262)
//					SET_PED_COMPONENT_VARIATION(shopAssistant, PED_COMP_HEAD, 1, 0)
//					SET_PED_COMPONENT_VARIATION(shopAssistant, PED_COMP_BERD, 0, 0)
//					SET_PED_COMPONENT_VARIATION(shopAssistant, PED_COMP_HAIR, 0, 0)
//					SET_PED_COMPONENT_VARIATION(shopAssistant, PED_COMP_TORSO, 0, 0)
//					SET_PED_COMPONENT_VARIATION(shopAssistant, PED_COMP_LEG, 0, 0)
//					SET_PED_COMPONENT_VARIATION(shopAssistant, PED_COMP_HAND, 0, 0)
//					SET_PED_COMPONENT_VARIATION(shopAssistant, PED_COMP_FEET, 0, 0)
//					SET_PED_COMPONENT_VARIATION(shopAssistant, PED_COMP_TEETH, 0, 0)
//					SET_PED_COMPONENT_VARIATION(shopAssistant, PED_COMP_SPECIAL, 0, 0)
//					SET_PED_COMPONENT_VARIATION(shopAssistant, PED_COMP_SPECIAL2, 0, 0)
//					SET_PED_COMPONENT_VARIATION(shopAssistant, PED_COMP_DECL, 0, 0)
//					SET_PED_COMPONENT_VARIATION(shopAssistant, PED_COMP_JBIB, 0, 0)
//
//					CLEAR_PED_PROP(shopAssistant, ANCHOR_HEAD)
//					CLEAR_PED_PROP(shopAssistant, ANCHOR_EYES)
//					CLEAR_PED_PROP(shopAssistant, ANCHOR_EARS)
//					CLEAR_PED_PROP(shopAssistant, ANCHOR_MOUTH)
//					CLEAR_PED_PROP(shopAssistant, ANCHOR_LEFT_HAND)
//					CLEAR_PED_PROP(shopAssistant, ANCHOR_RIGHT_HAND)
//					CLEAR_PED_PROP(shopAssistant, ANCHOR_LEFT_WRIST)
//					CLEAR_PED_PROP(shopAssistant, ANCHOR_RIGHT_WRIST)
//					CLEAR_PED_PROP(shopAssistant, ANCHOR_HIP)
//					ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, shopAssistant, "PED1")
//		//			RETAIN_ENTITY_IN_INTERIOR(shopAssistant, InteriorJewelleryShop)
//				ELSE
//					IF NOT IS_PED_INJURED(shopAssistant)
//						SET_ENTITY_COORDS(shopAssistant, << -618.6191, -235.5955, 37.0570 >>)
//						SET_ENTITY_HEADING(shopAssistant, 36.0262)
//					ENDIF
//				ENDIF
//				SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_Business_01)
//				
//				//Set up high end shop customer
//				IF NOT DOES_ENTITY_EXIST(ShopCustomer)
//					ShopCustomer = CREATE_PED(PEDTYPE_MISSION, A_F_Y_BevHills_03, << -621.0125, -228.5511, 37.0570 >>, 121.4116)
//				ELSE
//					IF NOT IS_PED_INJURED(ShopCustomer)
//						CLEAR_PED_TASKS(ShopCustomer)
//						SET_ENTITY_COORDS(ShopCustomer, << -621.0125, -228.5511, 37.0570 >>)
//						SET_ENTITY_HEADING(ShopCustomer, 121.4116)
//					ENDIF					
//				ENDIF
//				SET_MODEL_AS_NO_LONGER_NEEDED(A_F_Y_BevHills_03)
//				
//				IF NOT IS_PED_INJURED(pedContact)
//					SET_ENTITY_COORDS(pedContact, << -626.9581, -238.2061, 37.0570 >>)
//					SET_ENTITY_HEADING(pedContact, 204.9566)
//				ENDIF
//	
//				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//				iCutsceneStage++
//			
//			BREAK
//			
//			CASE 3
//				
//				CanMissionFail = TRUE
//				iControlFlag = 0
//				missionStage = STAGE_LOOK_AT_JEWELLERY_2
//				iCutsceneStage ++
//			
//			BREAK
//		
//		ENDSWITCH
//	
//	ENDIF
//
//////═════════╡ SETUP ╞═════════
////	IF iControlFlag = 0 
////		
////		//DEBUG STAGE SELECTOR
////		#IF IS_DEBUG_BUILD
////		IF MissionStageBeingSkippedTo = TRUE
////			STAGE_SELECTOR_MISSION_SETUP()
////		ENDIF
////		#ENDIF
////			
////		//═════════════════════════╡ SET FLAGS ╞═══════════════════════════	
////		iCutsceneStage 				= 0
////		iskip_cutscene_flag 		= 0
////		b_isCutsceneRunning 		= TRUE
////		pedContactTask3Given 		= FALSE
////		playerTaskGiven 			= FALSE
////		pedsMoved 					= FALSE
////		pedrecordingsRequested 		= FALSE
////		PedslookingAtEachOther 		= FALSE
////		
////		IF DOES_BLIP_EXIST(blipLocate)	
////			REMOVE_BLIP(blipLocate)
////		ENDIF
////		
////		//Request and load shop assistant, guard and any peds to be inside the shop.
////		REQUEST_MODEL(U_M_M_JEWELSEC_01)//security guard
////		REQUEST_MODEL(A_F_Y_Business_01)//Shop assistant
////		REQUEST_MODEL(CS_JEWELASS)//Shop assistant 2
////		REQUEST_VEHICLE_RECORDING(002, "H3SET1")
////		REQUEST_WAYPOINT_RECORDING("H3SET1_PLAYER8")
////		REQUEST_WAYPOINT_RECORDING("H3SET1_BUDDY7")
////		
////		WHILE NOT HAS_MODEL_LOADED(U_M_M_JEWELSEC_01)
////		OR NOT HAS_MODEL_LOADED(A_F_Y_Business_01)
////		OR NOT HAS_MODEL_LOADED(CS_JEWELASS)
////		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(002, "H3SET1")
////		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1_PLAYER8")
////		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1_BUDDY7")
////			WAIT(0)
////		ENDWHILE
////			
////		SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
////		
////		IF HAS_MODEL_LOADED(U_M_M_JEWELSEC_01)
////		AND NOT DOES_ENTITY_EXIST(ShopGuard)
////			ShopGuard = CREATE_PED(PEDTYPE_MISSION, U_M_M_JEWELSEC_01, << -629.2624, -237.6700, 37.0570 >>, 36.7017)
////			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 2, shopGuard, "PED1")
//////			RETAIN_ENTITY_IN_INTERIOR(shopGuard, InteriorJewelleryShop)
////		ENDIF 
////		
////		IF HAS_MODEL_LOADED(CS_JEWELASS)
////		AND NOT DOES_ENTITY_EXIST(ShopAssistant)
////			ShopAssistant = CREATE_PED(PEDTYPE_MISSION, CS_JEWELASS, << -622.8374, -229.5900, 37.0570 >>, 314.7467)
//////			RETAIN_ENTITY_IN_INTERIOR(ShopAssistant, InteriorJewelleryShop)
////		ENDIF
////		
////		IF HAS_MODEL_LOADED(A_F_Y_Business_01)
////		AND NOT DOES_ENTITY_EXIST(shopAssistant)
////			shopAssistant = CREATE_PED(PEDTYPE_MISSION, A_F_Y_Business_01, << -624.6881, -232.5531, 37.0570 >>, 125.3657)
////			ADD_PED_FOR_DIALOGUE(MyLocalPedStruct, 3, shopAssistant, "PED1")
//////			RETAIN_ENTITY_IN_INTERIOR(shopAssistant, InteriorJewelleryShop)
////		ENDIF
////		
////		CLEAR_AREA_OF_VEHICLES(<<-677, -232, 37>>, 20)
////		
////		//Create camera's for cutscene
////		camInit = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-708.124878,-244.401886,47.005543>>,<<-14.045350,-0.000000,-112.931862>>,25.073933, TRUE)
////		camDest = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-696.199280,-216.212830,47.005543>>,<<-14.045350,-0.000000,-112.931862>>,25.073933, TRUE)
////		
////		CLEAR_PRINTS()
////		
////		//Reset debug flag for stage selector
////		#IF IS_DEBUG_BUILD
////			MissionStageBeingSkippedTo = FALSE
////		#ENDIF
////		iControlFlag = 1
////			
////	ENDIF
////	
////	IF iControlFlag = 1
////		
////		SETTIMERB(0)
////		
////		WHILE b_isCutsceneRunning = TRUE
////			
////			DISPLAY_RADAR(FALSE)
////			DISPLAY_HUD(FALSE)
////			
////			WAIT(0)
////			
////			//Check for skip cutscene
////			IF TIMERB() > 500
////				SkipCutscene()
////			ENDIF
////			
////			SWITCH iCutsceneStage
////			
////				CASE 0
////					
////					IF IS_VEHICLE_DRIVEABLE(vehCar)
////						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
////							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
////						ENDIF
////						IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCar)
////							START_PLAYBACK_RECORDED_VEHICLE(vehCar, 002, "H3SET1")
////							SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehCar, 5500)
////						ENDIF
////					ENDIF
////					
////					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
////					
////					SETTIMERA(0)
////					SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, 33000, GRAPH_TYPE_SIN_ACCEL_DECEL, GRAPH_TYPE_LINEAR)
////					RENDER_SCRIPT_CAMS(TRUE, FALSE, DEFAULT_INTERP_TO_FROM_GAME)
////
////					REQUEST_WAYPOINT_RECORDING("H3SET1_PLAYER")
////					REQUEST_WAYPOINT_RECORDING("H3SET1_BUDDY1")
////					iCutsceneStage++
////				BREAK
////				
////				CASE 1
////					
////					IF IS_VEHICLE_DRIVEABLE(vehCar)
////						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehCar)
////							IF pedContactTask3Given = FALSE
////								IF GET_TIME_POSITION_IN_RECORDING(vehCar) > 11300
////									IF NOT IS_PED_INJURED(pedContact)
////										OPEN_SEQUENCE_TASK(seqSequencePedContact)
////											TASK_LEAVE_VEHICLE(NULL, vehCar)
////											TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "H3SET1_BUDDY7", 1)
////										CLOSE_SEQUENCE_TASK(seqSequencePedContact)
////										TASK_PERFORM_SEQUENCE(pedContact, seqSequencePedContact)
////										CLEAR_SEQUENCE_TASK(seqSequencePedContact)
////										CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_STP2", CONV_PRIORITY_MEDIUM)//Come on. We need to get a feel for this place.
////										pedContactTask3Given = TRUE
////									ENDIF
////								ENDIF
////							ENDIF
////							IF playerTaskGiven = FALSE
////								IF GET_TIME_POSITION_IN_RECORDING(vehCar) > 9800
////									IF NOT IS_PED_INJURED(PLAYER_PED_ID())
////										OPEN_SEQUENCE_TASK(seqSequencePlayer)
////											TASK_LEAVE_VEHICLE(NULL, vehCar)
////											TASK_FOLLOW_WAYPOINT_RECORDING(NULL, "H3SET1_PLAYER8", 1)
////										CLOSE_SEQUENCE_TASK(seqSequencePlayer)
////										TASK_PERFORM_SEQUENCE(PLAYER_PED_ID(), seqSequencePlayer)
////										CLEAR_SEQUENCE_TASK(seqSequencePlayer)
////										playerTaskGiven = TRUE
////									ENDIF
////								ELSE
////									IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
////										SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
////									ENDIF	
////								ENDIF
////							ENDIF
////						ENDIF
////					ENDIF
////				
////					IF TIMERA() > 30000
////						REQUEST_INTERIOR_MODELS(V_JEWEL2, "GtaMloRoom01")
////						CLEAR_PRINTS()
////						SET_CAM_PARAMS(camInit, <<-630.003357,-261.884857,41.160511>>, <<-0.484803,-0.000003,13.241154>>, 34.140644)
////						SET_CAM_PARAMS(camDest, <<-629.999573,-261.900482,39.263489>>, <<-0.484803,-0.000003,13.241154>>, 34.140644)
////						SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, 11000)
////						
////						IF pedsMoved = FALSE
////							IF NOT IS_PED_INJURED(pedContact)
////								CLEAR_PED_TASKS(pedContact)
////								SET_ENTITY_COORDS(pedContact, << -642.2004, -239.6883, 36.8673 >>)
////								SET_ENTITY_HEADING(pedContact, 253.9904)
////								TASK_FOLLOW_WAYPOINT_RECORDING(pedContact, "H3SET1_BUDDY1", 7)
////							ENDIF
////							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
////							SET_ENTITY_COORDS(PLAYER_PED_ID(), << -642.4644, -237.2986, 36.8623 >>)
////							SET_ENTITY_HEADING(PLAYER_PED_ID(), 233.8376)
////							TASK_FOLLOW_WAYPOINT_RECORDING(PLAYER_PED_ID(), "H3SET1_PLAYER", 7)
////							pedsMoved = TRUE
////						ENDIF						
////						
////						SETTIMERA(0)
////						iCutsceneStage++
////					ENDIF
////				
////				BREAK
////							
////				CASE 2
////					
////					//Request ped recordings for inside the shop stages.
////					IF pedrecordingsRequested = FALSE
////						REQUEST_WAYPOINT_RECORDING("H3SET1_BUDDY2")
////						REQUEST_WAYPOINT_RECORDING("H3SET1_SHOPASS1")
////						REQUEST_WAYPOINT_RECORDING("H3SET1_PLAYER3")
////						REQUEST_WAYPOINT_RECORDING("H3SET1Shopass4")
////						REQUEST_WAYPOINT_RECORDING("H3SET1_PLAYER4")
////						pedrecordingsRequested = TRUE
////					ENDIF
////					
////					IF TIMERA() > 3500
////						IF PedslookingAtEachOther = FALSE
////							IF NOT IS_PED_INJURED(pedContact)
////							AND NOT IS_PED_INJURED(shopGuard)
////								TASK_LOOK_AT_ENTITY(pedContact, shopGuard, -1)
////								TASK_LOOK_AT_ENTITY(shopGuard, PLAYER_PED_ID(), -1)
////								PedslookingAtEachOther = TRUE
////							ENDIF
////						ENDIF
////					ENDIF		
////								
////					IF TIMERA() > 4000
////					AND pedrecordingsRequested = TRUE
////						iCutsceneStage++
////					 	b_isCutsceneRunning = FALSE
////					ENDIF
////				
////				BREAK
////				
////			ENDSWITCH
////			
////		ENDWHILE
////		
////		IF iskip_cutscene_flag = 1
////			
////			CLEAR_PRINTS()
////			REQUEST_WAYPOINT_RECORDING("H3Set1_BUDDY2")
////			REQUEST_WAYPOINT_RECORDING("H3Set1_SHOPASS1")
////			REQUEST_WAYPOINT_RECORDING("H3SET1_PLAYER3")
////			REQUEST_WAYPOINT_RECORDING("H3SET1Shopass4")
////			REQUEST_WAYPOINT_RECORDING("H3SET1_PLAYER4")
////			
////			WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED("H3Set1_BUDDY2")
////			OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("H3Set1_SHOPASS1")
////			OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1_PLAYER3")
////			OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1Shopass4")
////			OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1_PLAYER4")
////				WAIT(0)
////			ENDWHILE
////			
////			CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
////			SET_ENTITY_COORDS(PLAYER_PED_ID(), << -633.7156, -238.6721, 37.0797 >>, FALSE)
////			SET_ENTITY_HEADING(PLAYER_PED_ID(), 301.1499)
////			
////			IF NOT IS_PED_INJURED(pedContact)
////				CLEAR_PED_TASKS_IMMEDIATELY(pedContact)
////				SET_ENTITY_COORDS(pedContact, << -631.7568, -238.4949, 37.0217 >> )
////				SET_ENTITY_HEADING(pedContact, 340.0365)
////			ENDIF			
////			
////			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
////			
////			//Reset Flags
////			b_isCutsceneRunning = FALSE
////			
////		ENDIF
////		
////		IF b_isCutsceneRunning = FALSE
////			
////			IF iskip_cutscene_flag = 0
////				RENDER_SCRIPT_CAMS(FALSE, FALSE)
////			ELSE
////				RENDER_SCRIPT_CAMS(FALSE, FALSE)
////			ENDIF
////			
////			//Destroy cams
////			IF DOES_CAM_EXIST(camInit)
////				DESTROY_CAM(camInit)
////			ENDIF
////			IF DOES_CAM_EXIST(camDest)
////				DESTROY_CAM(camDest)
////			ENDIF
////			
////			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
////			DISPLAY_RADAR(TRUE)
////			DISPLAY_HUD(TRUE)
////			
////			iControlFlag = 0
////			iskip_cutscene_flag = 0
////			missionStage = STAGE_LOOK_AT_JEWELLERY_1
////			
////		ENDIF
////		
////	ENDIF
//	
//ENDPROC

//
////PURPOSE: Stage that checks to see if the player is at the coords for triggering mocap cut 1 inside store.
//PROC DO_STAGE_LOOK_AT_JEWELLERY_1()
//
//	IF iControlFlag = 0
//		
//		//**************SET MID MISSION CHECK POINT HERE*******************//
//		Set_Replay_Mid_Mission_Stage(1)
//		PRINTSTRING("***********MID MISSION STAGE SET TO 1***********") PRINTNL()
//		
//		
//		REQUEST_WAYPOINT_RECORDING("H3SET1_PLAYER3")
//		WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1_PLAYER3")
//			WAIT(0)
//		ENDWHILE
//		
//		IF MissionStageBeingSkippedTo = TRUE
//			STAGE_SELECTOR_MISSION_SETUP()
//			MissionStageBeingSkippedTo = FALSE
//		ENDIF	
//		
//		//═════════════════════════╡ SET FLAGS ╞═══════════════════════════	
//		Convo2Called		 				= FALSE
//		shopAssistantFollowingRecording 	= FALSE
//		pedContactTaskGiven 				= FALSE
//		pedContactwaypointPaused 			= FALSE
//		
//		iControlFlag = 1
//		
//	ENDIF
//	
//	IF iControlFlag = 1
//	
//		IF GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1_PLAYER3")
//			USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("H3SET1_PLAYER3", TRUE, 0.5, 1.0)
//		ENDIF
//		
//		//Do enter shop chat between guard and pedcontact 
//		IF Convo2Called = FALSE
//			KILL_ANY_CONVERSATION()
//			WAIT(0)
//			PLAY_SINGLE_LINE_FROM_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_STP3", "JHS1_STP3_1", CONV_PRIORITY_MEDIUM)//Afternoon.
//			WAIT(0)
//			SETTIMERB(0)
//			Convo2Called = TRUE
//		ENDIF	
//		
//		IF TIMERB() > 6000
//			IF Convo2Called = TRUE
//				IF NOT DOES_BLIP_EXIST(BlipViewingPoint1)
//					BlipViewingPoint1 = CREATE_BLIP_FOR_COORD(<<-626, -235, 37>>)
//					IF NOT IS_MESSAGE_BEING_DISPLAYED()
//						PRINT_NOW("VIEW_1", DEFAULT_GOD_TEXT_TIME, 1)//~s~Look at the ~y~rings.
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//			
//		//Sort out Lester 
//		IF pedContactTaskGiven = FALSE
//			IF NOT IS_PED_INJURED(shopAssistant)
//				IF NOT IS_PED_INJURED(pedContact)
//					TASK_LOOK_AT_ENTITY(pedContact, shopAssistant,-1)
//					TASK_FOLLOW_WAYPOINT_RECORDING(pedContact, "H3SET1_BUDDY2", 1)
//					pedContactTaskGiven = TRUE
//				ENDIF
//			ENDIF
//		ENDIF
//		IF pedContactwaypointPaused = FALSE
//			IF NOT IS_PED_INJURED(pedContact)
//				IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedContact)
//					IF GET_PED_WAYPOINT_PROGRESS(pedContact) = 3
//						CLEAR_PED_TASKS(pedContact)
//						OPEN_SEQUENCE_TASK(seqSequencePedContact)
//							TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -627.2033, -238.7574, 37.0570 >>, PEDMOVE_WALK, -1)
//							TASK_ACHIEVE_HEADING(NULL, 224.9482)
//							TASK_LOOK_AT_COORD(NULL, <<-626, -239, 38>>, -1)
//						CLOSE_SEQUENCE_TASK(seqSequencePedContact)
//						TASK_PERFORM_SEQUENCE(pedContact, seqSequencePedContact)
//						CLEAR_SEQUENCE_TASK(seqSequencePedContact)
//						pedContactwaypointPaused = TRUE
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-631, -237, 37>>, <<1,1,2>>)
//			//clean up waypoint recording not needed anymore.
//			REMOVE_WAYPOINT_RECORDING("H3SET1_PLAYER")
//			REMOVE_WAYPOINT_RECORDING("H3SET1_BUDDY1")
//			
//			IF DOES_BLIP_EXIST(BlipViewingPoint1)
//				REMOVE_BLIP(BlipViewingPoint1)
//			ENDIF
//			
//			IF shopAssistantFollowingRecording = FALSE
//				IF NOT IS_PED_INJURED(shopAssistant)
//					TASK_FOLLOW_WAYPOINT_RECORDING(shopAssistant, "H3SET1SHOPASS4", 1)
//					shopAssistantFollowingRecording = TRUE
//				ENDIF
//			ENDIF
//			
//			iControlFlag = 0
//			missionStage = STAGE_JS_SCOPESTORE_MOCAP_CUT
//			
//		ENDIF
//		
//	ENDIF
//		
//ENDPROC

//
////PURPOSE: 1st Mocap cutscene where the player meets the shop assistant as is shown in the 1st cabinet.
//PROC DO_STAGE_JS_SCOPESTORE_MOCAP_CUT()
//
//	IF iControlFlag = 0
//	
//		//═════════════════════════╡ SET FLAGS ╞═══════════════════════════	
//		iCutsceneStage = 0
//		pedContactwaypointPaused 	= FALSE
//		Convo3Called 				= FALSE
//		shopAssistantMoved 		= FALSE
//		shopGuardMoved 				= FALSE
//		Convo5Called 				= FALSE
//		
//		
//		//request 1st mocap cut
//		REQUEST_CUTSCENE("JH_ScopeStore")	
//		
//		iControlFlag = 1
//	
//	ENDIF
//	
//	IF iControlFlag = 1
//		
//		SWITCH iCutsceneStage
//		
//			CASE 0 
//				
//				PRINTSTRING("CASE 0") PRINTNL()
//				
//				IF pedContactwaypointPaused = FALSE
//					IF NOT IS_PED_INJURED(pedContact)
//						IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedContact)
//							IF GET_PED_WAYPOINT_PROGRESS(pedContact) = 3
//								CLEAR_PED_TASKS(pedContact)
//								OPEN_SEQUENCE_TASK(seqSequencePedContact)
//									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -627.2033, -238.7574, 37.0570 >>, PEDMOVE_WALK, -1)
//									TASK_ACHIEVE_HEADING(NULL, 224.9482)
//									TASK_LOOK_AT_COORD(NULL, <<-626, -239, 38>>, -1)
//								CLOSE_SEQUENCE_TASK(seqSequencePedContact)
//								TASK_PERFORM_SEQUENCE(pedContact, seqSequencePedContact)
//								CLEAR_SEQUENCE_TASK(seqSequencePedContact)
//								pedContactwaypointPaused = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF	
//				
//				WHILE NOT HAS_CUTSCENE_LOADED()
//					WAIT(0)
//					PRINTSTRING("Stuck waiting for cutscene to load..") PRINTNL()
//				ENDWHILE
//				
//				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//				
//				IF NOT IS_PED_INJURED(shopAssistant)
//					REGISTER_ENTITY_FOR_CUTSCENE(shopAssistant, "CS_JEWELASS", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				IF NOT IS_PED_INJURED(shopGuard)
//					REGISTER_ENTITY_FOR_CUTSCENE(shopGuard, "U_M_M_JEWELSEC_01", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				
//				//Configure script systems into a safe mode.
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)				
//				
//				START_CUTSCENE()
//				SETTIMERA(0)
//				iCutsceneStage++			
//				
//			BREAK
//			
//			CASE 1
//			
//				PRINTSTRING("CASE 1") PRINTNL()
//
//				IF pedContactwaypointPaused = FALSE
//					IF NOT IS_PED_INJURED(pedContact)
//						IF IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(pedContact)
//							IF GET_PED_WAYPOINT_PROGRESS(pedContact) = 3
//								CLEAR_PED_TASKS(pedContact)
//								OPEN_SEQUENCE_TASK(seqSequencePedContact)
//									TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, << -627.2033, -238.7574, 37.0570 >>, PEDMOVE_WALK, -1)
//									TASK_ACHIEVE_HEADING(NULL, 224.9482)
//									TASK_LOOK_AT_COORD(NULL, <<-626, -239, 38>>, -1)
//								CLOSE_SEQUENCE_TASK(seqSequencePedContact)
//								TASK_PERFORM_SEQUENCE(pedContact, seqSequencePedContact)
//								CLEAR_SEQUENCE_TASK(seqSequencePedContact)
//								pedContactwaypointPaused = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//
//				IF Convo3Called = FALSE
//					KILL_ANY_CONVERSATION()
//					WAIT(0)
//					CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_STP4", CONV_PRIORITY_MEDIUM)//Hi there. Is there anything we can help you with? ......
//					WAIT(0)
//					SETTIMERB(0)
//					Convo3Called = TRUE
//				ENDIF
//				
//				IF shopAssistantMoved = FALSE
//					IF TIMERB() > 4000
//						IF NOT IS_PED_INJURED(shopAssistant)
//							SET_ENTITY_COORDS(shopAssistant, << -622.3363, -234.8329, 37.0570 >>, FALSE)
//							SET_ENTITY_HEADING(shopAssistant, 257.1781)
//							shopAssistantMoved = TRUE
//						ENDIF
//					ENDIF
//				ENDIF
//				
//				IF shopGuardMoved = FALSE
//					IF NOT IS_PED_INJURED(shopGuard)
//						SET_ENTITY_COORDS(shopGuard, << -630.3765, -241.6091, 37.1754 >>, FALSE)
//						shopGuardMoved = TRUE
//					ENDIF
//				ENDIF			
//				
//				IF Convo5Called = FALSE
//					IF TIMERB() > 18000
//						//Some dialogue
//						KILL_ANY_CONVERSATION()
//						WAIT(0)			
//						CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_STP5", CONV_PRIORITY_MEDIUM)//Are those diamonds? ...
//						WAIT(0)
//						Convo5Called = TRUE
//					ENDIF
//				ENDIF
//				
//				IF HAS_CUTSCENE_FINISHED()
//			      	//Return script systems to normal.
//					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)				
//				
//					iCutsceneStage++
//				ENDIF	
//					
//			BREAK
//			
//			CASE 2
//				
//				//move shop guard back to position after mocap cut has finished
//				IF NOT IS_PED_INJURED(shopGuard)
//					SET_ENTITY_COORDS(shopGuard, << -629.2624, -237.6700, 37.0570 >>, FALSE)
//					SET_ENTITY_HEADING(shopGuard, 36.7017)
//					shopGuardMoved = TRUE
//				ENDIF			
//				
//				PRINTSTRING("CASE 2") PRINTNL()
//				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//				iCutsceneStage++
//				
//			BREAK
//			
//			CASE 3
//				
//				iControlFlag = 0
//				iCutsceneStage = 0
//				missionStage = STAGE_LOOK_AT_JEWELLERY_2
//				
//			BREAK
//			
//		ENDSWITCH
//	
//	ENDIF
//	
//ENDPROC


////PURPOSE checks to see if the player is at the right coords to start the 2nd mocap.
//PROC DO_STAGE_LOOK_AT_JEWELLERY_2()
//
//	IF iControlFlag = 0
//		
//		//This will be triggered if the mission checkpoint was reached and a replay text has been used.
//		IF MissionStageBeingSkippedTo = TRUE
//			STAGE_SELECTOR_MISSION_SETUP()
//		ENDIF
//		
//		//**************SET MID MISSION CHECK POINT HERE*******************//
//		Set_Replay_Mid_Mission_Stage(1)
//		PRINTSTRING("***********MID MISSION STAGE SET TO 1***********") PRINTNL()		
//			
//		//request 2nd mocap cut
//		REQUEST_CUTSCENE("jh_1_mcs_2")
//		//Request 1st waypoint for shop customer
//		REQUEST_WAYPOINT_RECORDING("H3SET1_CUST1")
//		WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1_CUST1")
//			WAIT(0)
//		ENDWHILE
//		
//		//═════════════════════════╡ SET FLAGS ╞═══════════════════════════
//		checkForLeavingShopToEarly 		= TRUE
//		customerTask1Given 				= FALSE
//		LookAtWatchesTextDone 			= FALSE
//		WaypointActive 					= FALSE
//		waypointSpeedSet 				= FALSE
//		
//		//Reset stage selector flag.
//		#IF IS_DEBUG_BUILD
//			MissionStageBeingSkippedTo = FALSE
//		#ENDIF
//		
//		IF NOT DOES_BLIP_EXIST(BlipViewingPoint2)
//			BlipViewingPoint2 = CREATE_BLIP_FOR_COORD(<< -620.5695, -234.1263, 37.0570 >>)
//		ENDIF		
//		
//		IF NOT IS_PED_INJURED(ShopCustomer)
//			TASK_FOLLOW_WAYPOINT_RECORDING(ShopCustomer, "H3SET1_CUST1")
//		ENDIF
//		
//		iControlFlag = 1
//		
//	ENDIF
//	
//	IF iControlFlag = 1
//		
//		IF LookAtWatchesTextDone = FALSE
//			IF NOT IS_MESSAGE_BEING_DISPLAYED()
//				PRINT("VIEW_2", 4000, 1)//~s~Look at the ~y~watches.
//				LookAtWatchesTextDone = TRUE
//			ENDIF
//		ENDIF
//		
//		IF customerTask1Given = FALSE
//			IF NOT IS_PED_INJURED(ShopCustomer)
//				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(ShopCustomer)
//					IF WaypointActive = TRUE
//						PRINTSTRING("Waypoint playback is not going on for shop customer")
//						TASK_LOOK_AT_COORD(ShopCustomer, <<-626.56, -233.62, 37>>, 10000, SLF_USE_TORSO)
//						customerTask1Given = TRUE
//						WaypointActive = FALSE
//					ENDIF
//				ELSE
//					PRINTSTRING("Waypoint playback is going on for shop customer")
//					WaypointActive = TRUE
//					IF waypointSpeedSet = FALSE
//						PRINTSTRING("Waypoint playback speed override being called")
//						WAYPOINT_PLAYBACK_OVERRIDE_SPEED(ShopCustomer, 0.5)
//						waypointSpeedSet = TRUE
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF
//		
//		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -620.5695, -234.1263, 37.0570 >>, <<1,1,2>>)	
//			
//			IF DOES_BLIP_EXIST(BlipViewingPoint2)
//				REMOVE_BLIP(BlipViewingPoint2)
//			ENDIF			
//			
//			iControlFlag = 0
//			missionStage = STAGE_JS_SCOPESTORE_01_MOCAP_CUT
//			
//		ENDIF
//	
//	ENDIF
//	
//ENDPROC


////PURPOSE: 2nd Mocap cutscene showing shop keeper showing Michael more jewellery.
//PROC DO_STAGE_JS_SCOPESTORE_01_MOCAP_CUT()
//
//	IF iControlFlag = 0
//	
//		//═════════════════════════╡ SET FLAGS ╞═══════════════════════════	
//		iCutsceneStage 					= 0
////		Convo6Called 					= FALSE
////		shopAssistantMoved2		 	= FALSE
////		shopAssistantRecordingStarted 	= FALSE
//		
//		
//		iControlFlag = 1
//	
//	ENDIF
//	
//	IF iControlFlag = 1
//
//		SWITCH iCutsceneStage
//			
//			CASE 0 
//				
//				PRINTSTRING("CASE 0") PRINTNL()
//				WHILE NOT HAS_CUTSCENE_LOADED()
//					PRINTSTRING("Stuck waiting for cutscene to load..") PRINTNL()
//					WAIT(0)
//				ENDWHILE
//				
//				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//				
//				//Register entities for cutscene
//				//Lester
//				IF NOT IS_PED_INJURED(pedContact)
//					REGISTER_ENTITY_FOR_CUTSCENE(pedContact, "A_M_M_Salton_02", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				//Shop Assistant
//				IF NOT IS_PED_INJURED(shopAssistant)
//					REGISTER_ENTITY_FOR_CUTSCENE(shopAssistant, "A_F_Y_Business_01", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF				
//
//				
//				//Configure script systems into a safe mode.
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)				
//				
//				START_CUTSCENE()
//				
//				SET_SEAMLESS_CUTS_ACTIVE(bTracker)
//				
//				SETTIMERA(0)
//				iCutsceneStage++			
//				
//				
//			BREAK
//			
//			CASE 1
//			
//				PRINTSTRING("CASE 1") PRINTNL()
//				
//				//Request waypoint to be used as assisted lines and 2nd and 3rd waypoint recordings for customer
//				REQUEST_WAYPOINT_RECORDING("H3SET1_PLAYER3")
//				REQUEST_WAYPOINT_RECORDING("H3SET1_CUST2")
//				REQUEST_WAYPOINT_RECORDING("H3SET1_CUST3")
//				
//				//Check for customer having moved.
//				IF customerTask1Given = FALSE
//					IF NOT IS_PED_INJURED(ShopCustomer)
//						IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(ShopCustomer)
//							IF WaypointActive = TRUE
//								PRINTSTRING("Waypoint playback is not going on for shop customer")
//								TASK_LOOK_AT_COORD(ShopCustomer, <<-626.56, -233.62, 37>>, 10000, SLF_USE_TORSO)
//								customerTask1Given = TRUE
//								WaypointActive = FALSE
//							ENDIF
//						ELSE
//							PRINTSTRING("Waypoint playback is going on for shop customer")
//							WaypointActive = TRUE
//							IF waypointSpeedSet = FALSE
//								PRINTSTRING("Waypoint playback speed override being called")
//								WAYPOINT_PLAYBACK_OVERRIDE_SPEED(ShopCustomer, 0.5)
//								waypointSpeedSet = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF				
//				
//				IF HAS_CUTSCENE_FINISHED()
//			      	//Return script systems to normal.
//					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)				
//				
//					iCutsceneStage++
//				ENDIF	
//					
//			BREAK
//			
//			CASE 2
//				
//				IF NOT IS_PED_INJURED(ShopCustomer)
//					IF GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1_CUST2")
//						CLEAR_PED_TASKS(ShopCustomer)
//						TASK_FOLLOW_WAYPOINT_RECORDING(ShopCustomer, "H3SET1_CUST2")
//					ENDIF
//				ENDIF
//				
//				//move shop guard back to position after mocap cut has finished
//				IF NOT IS_PED_INJURED(shopAssistant)
//					SET_ENTITY_COORDS(shopAssistant, << -623.4383, -230.3307, 37.0570 >>, FALSE)
//					SET_ENTITY_HEADING(shopAssistant, 120.3058)
//				ENDIF			
//				
//				WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1_PLAYER3")
//					WAIT(0)
//				ENDWHILE
//				
//				IF GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1_PLAYER3")
//					USE_WAYPOINT_RECORDING_AS_ASSISTED_MOVEMENT_ROUTE("H3SET1_PLAYER3", TRUE, 0.5, 1.0)
//				ENDIF
//				
////				IF shopAssistantRecordingStarted = FALSE
////					IF NOT IS_PED_INJURED(shopAssistant)
////						CLEAR_PED_TASKS(shopAssistant)
////						TASK_FOLLOW_WAYPOINT_RECORDING(shopAssistant, "H3SET1_SHOPASS1", 10)
////						CLEAR_PED_TASKS(PLAYER_PED_ID())
////						shopAssistantRecordingStarted = TRUE
////					ENDIF
////				ENDIF	
//				
//				PRINTSTRING("CASE 2") PRINTNL()
//				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//				iCutsceneStage++
//				
//			BREAK
//			
//			CASE 3
//				
//				iControlFlag = 0
//				iCutsceneStage = 0
//				missionStage = STAGE_LOOK_AT_JEWELLERY_3
//				
//			BREAK
//			
//		ENDSWITCH
//
//	ENDIF
//ENDPROC



////PURPOSE: Checks to see if the player is at the coords to start mocap cut 3.
//PROC DO_STAGE_LOOK_AT_JEWELLERY_3()
//
//	IF iControlFlag = 0
//
//		//═════════════════════════╡ SET FLAGS ╞═══════════════════════════	
////		shopAssistantAtCounter3 = FALSE
//		
//		//DEBUG STAGE SELECTOR
//		#IF IS_DEBUG_BUILD
//			IF MissionStageBeingSkippedTo = TRUE
//				STAGE_SELECTOR_MISSION_SETUP()
//				MissionStageBeingSkippedTo = FALSE
//			ENDIF
//		#ENDIF			
//		
//		
//		//FLAGS
//		customerTask2Given 		= FALSE
//		Waypoint2Active 		= FALSE
//		waypoint2SpeedSet 		= FALSE
//		
//		//request 3rd counter cutscene and exit store.
//		REQUEST_CUTSCENE("jh_1_mcs_3")	
//		
//		iControlFlag = 1
//		
//	ENDIF
//	
//	IF iControlFlag = 1
//		
////		IF shopAssistantAtCounter3 = TRUE
//		IF NOT IS_MESSAGE_BEING_DISPLAYED()
//			IF NOT DOES_BLIP_EXIST(BlipViewingPoint3)
//				BlipViewingPoint3 = CREATE_BLIP_FOR_COORD(<< -624.9990, -230.8543, 37.0570 >>)
//			ENDIF
//			CLEAR_PRINTS()
//			PRINT("VIEW_3", DEFAULT_GOD_TEXT_TIME,1)//~s~Look at the ~y~jewellery.
//			iControlFlag = 2
//		ENDIF
////		ENDIF
//		
//	ENDIF
//
//	IF iControlFlag = 2
//	
//		//Check for customer having moved.	
//		IF customerTask2Given = FALSE
//			IF NOT IS_PED_INJURED(ShopCustomer)
//				IF NOT IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_PED(ShopCustomer)
//					IF Waypoint2Active = TRUE
//						PRINTSTRING("Waypoint playback is not going on for shop customer")
//						TASK_LOOK_AT_COORD(ShopCustomer, <<-630.47, -231.74, 37.94>>, 8000, SLF_USE_TORSO)
//						customerTask2Given = TRUE
//						Waypoint2Active = FALSE
//					ENDIF
//				ELSE
//					PRINTSTRING("Waypoint playback is going on for shop customer")
//					Waypoint2Active = TRUE
//					IF waypoint2SpeedSet = FALSE
//						PRINTSTRING("Waypoint playback speed override being called")
//						WAYPOINT_PLAYBACK_OVERRIDE_SPEED(ShopCustomer, 0.5)
//						waypoint2SpeedSet = TRUE
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDIF	
//	
//		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -624.9990, -230.8543, 37.0570 >>, <<1,1,1.5>>)
//
//			IF DOES_BLIP_EXIST(BlipViewingPoint3)
//				REMOVE_BLIP(BlipViewingPoint3)
//			ENDIF
//			
//			checkForLeavingShopToEarly = FALSE
//			
//			iControlFlag = 0
//			missionStage = STAGE_JS_SCOPESTORE_02_MOCAP_CUT	
//		
//		ENDIF
//	
//	ENDIF
//	
//ENDPROC


////PURPOSE: 3rd Mocap cut showing the player flirting with the shop assistant and then heading off.
//PROC DO_STAGE_JS_SCOPESTORE_02_MOCAP_CUT()
//
//
//	IF iControlFlag = 0
//	
//		//═════════════════════════╡ SET FLAGS ╞═══════════════════════════	
////		Convo7Called 			= FALSE
//		shopAssistantMoved3 	= FALSE
//		iCutsceneStage 			= 0
//		
//		iControlFlag = 1
//		
//	ENDIF
//	
//	IF iControlFlag = 1
//	
//		SWITCH iCutsceneStage
//			
//			CASE 0 
//				
//				PRINTSTRING("CASE 0") PRINTNL()
//				WHILE NOT HAS_CUTSCENE_LOADED()
//					PRINTSTRING("Stuck waiting for cutscene to load..") PRINTNL()
//					WAIT(0)
//				ENDWHILE
//				
//				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//				
//				//Register entities for cutscene
//				//Lester
//				IF NOT IS_PED_INJURED(pedContact)
//					REGISTER_ENTITY_FOR_CUTSCENE(pedContact, "A_M_M_Salton_02", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				//Players car
//				IF IS_VEHICLE_DRIVEABLE(vehCar)
//					REGISTER_ENTITY_FOR_CUTSCENE(vehCar, "TAILGATER", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				//Shop Guard
//				IF NOT IS_PED_INJURED(shopGuard)
//					REGISTER_ENTITY_FOR_CUTSCENE(shopGuard, "U_M_M_JEWELSEC_01", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				//Shop Assistant
//				IF NOT IS_PED_INJURED(shopAssistant)
//					REGISTER_ENTITY_FOR_CUTSCENE(shopAssistant, "A_F_Y_Business_01", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				
//				//Configure script systems into a safe mode.
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)				
//				
//				IF NOT IS_PED_INJURED(ShopCustomer)
//					IF GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1_CUST3")
//						CLEAR_PED_TASKS(ShopCustomer)
//						TASK_FOLLOW_WAYPOINT_RECORDING(ShopCustomer, "H3SET1_CUST3")
//					ENDIF
//				ENDIF				
//				
//				START_CUTSCENE()
//				
//				SET_SEAMLESS_CUTS_ACTIVE(bTracker)
//				
//				SETTIMERA(0)
//				iCutsceneStage++			
//				
//			BREAK
//			
//			CASE 1
//			
//				PRINTSTRING("CASE 1") PRINTNL()
//				
////				//Dialogue for mocap
////				IF Convo7Called = FALSE
////					KILL_ANY_CONVERSATION()
////					CLEAR_PRINTS()
////					WAIT(0)
////					CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_STP7", CONV_PRIORITY_MEDIUM)//I think you're excited already, baby....
////					WAIT(0)
////					Convo7Called = TRUE
////				ENDIF			
//				
//				IF shopAssistantMoved3 = FALSE
//					IF NOT IS_PED_INJURED(shopAssistant)
//						SET_ENTITY_COORDS(shopAssistant, << -630.3765, -241.6091, 37.1754 >>, FALSE)
//						SET_ENTITY_HEADING(shopAssistant, 257.1781)
//						shopAssistantMoved3 = TRUE
//					ENDIF
//				ENDIF			
//				
//				IF HAS_CUTSCENE_FINISHED()
//			      	//Return script systems to normal.
//					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)				
//				
//					iCutsceneStage++
//				ENDIF	
//					
//			BREAK
//			
//			CASE 2
//				
//				//move shop assistant back to position after mocap cut has finished
//				IF NOT IS_PED_INJURED(shopAssistant)
//					SET_ENTITY_COORDS(shopAssistant, << -623.4316, -230.2350, 37.0570 >>, FALSE)
//					SET_ENTITY_HEADING(shopAssistant, 131.2077)
//				ENDIF	
//				
//				//Delete the shop customer
//				IF NOT IS_PED_INJURED(ShopCustomer)
//					CLEAR_PED_TASKS(ShopCustomer)
//					DELETE_PED(ShopCustomer)
//				ENDIF
//				
//				IF IS_VEHICLE_DRIVEABLE(vehCar)
//					IF NOT IS_PED_INJURED(pedContact)
//						IF NOT IS_PED_IN_VEHICLE(pedContact, vehCar)
//							SET_PED_INTO_VEHICLE(pedContact, vehCar, VS_FRONT_RIGHT)
//						ENDIF
//					ENDIF
//					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
//						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar, VS_DRIVER)
//					ENDIF	
////					SET_ENTITY_COORDS(vehCar, << -680.5485, -239.5513, 35.6722 >>, FALSE)
////					SET_ENTITY_HEADING(vehCar, 30.2407)
//				ENDIF
//				
//				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//				
//				PRINTSTRING("CASE 2") PRINTNL()
//				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//				iCutsceneStage++
//				
//			BREAK
//			
//			CASE 3
//				
//				iCutsceneStage = 0
//				iControlFlag = 0
//				missionStage = STAGE_DRIVE_TO_WAREHOUSE
//				
//			BREAK
//			
//		ENDSWITCH	
//	
//	ENDIF
//	
//ENDPROC

//
////PURPOSE: Checks to see if the player has left the store yet.
//PROC DO_STAGE_LEAVE_STORE()
//
//
////═════════╡ SETUP ╞═════════
//	IF iControlFlag = 0
//		
//		REQUEST_WAYPOINT_RECORDING("H3SET1_PLAYER2")
//		REQUEST_WAYPOINT_RECORDING("H3SET1_BUDDY3")
//		
//		WHILE NOT GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1_PLAYER2")
//		OR NOT GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1_BUDDY3")
//			WAIT(0)
//		ENDWHILE
//		
//		//═════════════════════════╡ SET FLAGS ╞═══════════════════════════	
//		doneLeaveShopText = FALSE
//		
//	
//		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//		
//		IF NOT IS_PED_INJURED(pedContact)
//			IF NOT IS_PED_GROUP_MEMBER(pedContact, PLAYER_GROUP_ID())
//				CLEAR_PED_TASKS(pedContact)
//				SET_PED_AS_GROUP_MEMBER(pedContact, PLAYER_GROUP_ID())
//			ENDIF
//		ENDIF
//		
//		IF NOT IS_PED_INJURED(shopGuard)
//			CLEAR_PED_TASKS(shopGuard)
//			TASK_LOOK_AT_ENTITY(shopGuard, PLAYER_PED_ID(), -1)
//		
//			SETTIMERA(0)
//		ENDIF
//		
//		iControlFlag = 1	
//		
//	ENDIF
//	
//	IF iControlFlag = 1
//				
//		IF doneLeaveShopText = FALSE
//			IF NOT IS_MESSAGE_BEING_DISPLAYED()
//				PRINT("EXIT_STORE", DEFAULT_GOD_TEXT_TIME, 1)//~s~Leave the store when you are ready.
//				doneLeaveShopText = TRUE
//			ENDIF
//		ENDIF
//		
//		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-631.93,-237.86, 37>>, <<1,1, 2>>)
//			iControlFlag = 0
//			missionStage = STAGE_LEAVE_STORE_CUTSCENE
//		ENDIF
//		
//	ENDIF
//
//ENDPROC
//
//
////PURPOSE: cut scene of player and lester leaving the jewellery store and getting back to the car.
//PROC DO_STAGE_LEAVE_STORE_CUTSCENE()
//	
//	
//	IF iControlFlag = 0
//	
//		//═════════════════════════╡ SET FLAGS ╞═══════════════════════════	
//		iskip_cutscene_flag 	= 0
//		iCutsceneStage 			= 0
//		camsActivated 			= FALSE
//		interiorpinned 			= FALSE
//		playerAndContactMoved 	= FALSE
//		
//		
//		iControlFlag = 1
//		
//	ENDIF
//	
//	IF iControlFlag = 1
//		
//		
//		SETTIMERB(0)
//		b_isCutsceneRunning = TRUE
//		
//		CLEAR_PRINTS()
//		KILL_ANY_CONVERSATION()
//		
//		DISPLAY_RADAR(FALSE)
//		DISPLAY_HUD(FALSE)
//		SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//		
//		//Camera's
//		IF NOT DOES_CAM_EXIST(camInit)
//			camInit = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-633.504150,-264.655304,68.779816>>, <<-51.618248,-0.000002,-2.820225>>, 26.301832)
//		ELSE
//			SET_CAM_PARAMS(camInit, <<-633.504150,-264.655304,68.779816>>, <<-51.618248,-0.000002,-2.820225>>, 39.998871 )
//		ENDIF
//		
//		IF NOT DOES_CAM_EXIST(camDest)
//			camDest = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", <<-633.504150,-264.655304,68.779816>>, <<-28.308561,-0.000003,-3.796131>>, 19.182617)
//		ELSE
//			SET_CAM_PARAMS(camDest, <<-633.504150,-264.655304,68.779816>>, <<-28.308561,-0.000003,-3.796131>>, 39.998871 )
//		ENDIF		
//		
//		WHILE b_isCutsceneRunning 
//		
//			WAIT(0)
//			
//			//Check for skip cutscene
//			IF TIMERB() > 500
//				SkipCutscene()
//			ENDIF		
//			
//			SWITCH iCutsceneStage 
//				
//				CASE 0
//					
//					IF camsActivated = FALSE
//						SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, 8000)
//						RENDER_SCRIPT_CAMS(TRUE, FALSE)
//						SETTIMERA(0)
//						camsActivated = TRUE
//					ENDIF
//					
//					IF GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1_BUDDY3")
//						IF NOT IS_PED_INJURED(pedContact)
//							SET_ENTITY_COORDS(pedContact, << -629.6565, -237.5475, 37.0570 >>)
//							SET_ENTITY_HEADING(pedContact, 93.0410)
//							CLEAR_PED_TASKS(pedContact)
//							TASK_FOLLOW_WAYPOINT_RECORDING(pedContact, "H3SET1_BUDDY3", 1)
//							pedContactWaypoint3Started = TRUE
//						ENDIF
//					ENDIF
//					
//					IF GET_IS_WAYPOINT_RECORDING_LOADED("H3SET1_PLAYER2")
//						CLEAR_PED_TASKS(PLAYER_PED_ID())
//						TASK_FOLLOW_WAYPOINT_RECORDING(PLAYER_PED_ID(), "H3SET1_PLAYER2", 2)
//						playerWaypoint2Started = TRUE
//					ENDIF
//					
//					IF camsActivated = TRUE
//					AND pedContactWaypoint3Started = TRUE
//					AND playerWaypoint2Started = TRUE
//						iCutsceneStage ++
//					ENDIF
//					
//				BREAK
//				
//				CASE 1
//					
//					IF interiorpinned = FALSE
//						IF TIMERA() > 2500 
//							WHILE NOT IS_INTERIOR_READY(InteriorJewelleryShop)
//								WAIT(0)
//							ENDWHILE
//							interiorpinned = TRUE
//						ENDIF
//					ENDIF
//					
//					IF TIMERA() > 7500
//						
//						REQUEST_INTERIOR_MODELS(V_JEWEL2, "GtaMloRoom01")
//						SET_CAM_PARAMS(camInit, <<-628.194885,-235.829895,39.224934>>, <<7.071847,-0.003521,-25.142834>>, 23.117485)
//						SET_CAM_PARAMS(camDest, <<-628.194885,-235.829895,39.224934>>, <<7.071814,-0.003521,-76.560287>>, 23.117485)
//						
//						SET_CAM_ACTIVE_WITH_INTERP(camDest, camInit, 5000)
//						SETTIMERA(0)
//						iCutsceneStage++
//						
//					ENDIF
//				
//				BREAK
//				
//				CASE 2
//					
//					IF playerAndContactMoved = FALSE
//						IF IS_VEHICLE_DRIVEABLE(vehCar)
//							IF NOT IS_PED_INJURED(pedContact)	
//								CLEAR_PED_TASKS_IMMEDIATELY(pedContact)
//								SET_PED_INTO_VEHICLE(pedContact, vehCar, VS_FRONT_RIGHT)
//							ENDIF
//							CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
//							playerAndContactMoved = TRUE
//						ENDIF
//					ENDIF
//					
//					IF TIMERA() > 5100
//									
//						b_isCutsceneRunning = FALSE
//						iCutsceneStage++
//						
//					ENDIF
//					
//				BREAK
//				
//			ENDSWITCH
//			
//		ENDWHILE
//		
//		IF iskip_cutscene_flag = 1
//			
//			CLEAR_PRINTS()
//			IF IS_VEHICLE_DRIVEABLE(vehCar)
//				IF NOT IS_PED_INJURED(pedContact)	
//					CLEAR_PED_TASKS_IMMEDIATELY(pedContact)
//					SET_PED_INTO_VEHICLE(pedContact, vehCar, VS_FRONT_RIGHT)
//					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedContact, TRUE)
//					IF IS_PED_IN_GROUP(pedContact)
//						REMOVE_PED_FROM_GROUP(pedContact)
//					ENDIF
//				ENDIF
//			
//				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
//				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehCar)
//			ENDIF
//			
//			//Reset Flags
//			b_isCutsceneRunning = FALSE
//			
//		ENDIF	
//		
//		IF b_isCutsceneRunning = FALSE
//		
//			RENDER_SCRIPT_CAMS(FALSE, FALSE)
//			SET_CAM_ACTIVE(camInit, FALSE)
//			SET_CAM_ACTIVE(camDest, FALSE)
//			STOP_CAM_POINTING(camDest)
//			
//			DISPLAY_HUD(TRUE)
//			DISPLAY_RADAR(TRUE)
//			
//			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
//			
//			//Set flags
//			iCutsceneStage = 0
//			iControlFlag = 0
//			missionStage = STAGE_DRIVE_TO_WAREHOUSE
//			
//		ENDIF
//		
//	ENDIF
//ENDPROC


//PURPOSE: Stage where player has to drive back to the office planning board.
PROC DO_STAGE_DRIVE_TO_WAREHOUSE()

		JEWEL_STORE_STAFF_CONTROLLER()

		IF iControlFlag = 0
			
			IF MissionStageBeingSkippedTo = TRUE
				STAGE_SELECTOR_MISSION_SETUP()
				MissionStageBeingSkippedTo = FALSE
			ENDIF			
			
			//**************SET MID MISSION CHECK POINT HERE*******************//
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "STAGE_DRIVE_TO_WAREHOUSE", TRUE)
			PRINTSTRING("***********MID MISSION STAGE SET TO 3***********") PRINTNL()
			
			//═════════════════════════╡ SET FLAGS ╞═══════════════════════════
			
			doneGetInCarText2 						= FALSE
			doneWarningText 						= FALSE
			CheckpointReplayStarting 				= FALSE
			CanMissionFail 							= TRUE
			doneGodText 							= FALSE
			modelsRequested 						= FALSE
			donePH28Chat 							= FALSE
			doneSTP14Chat							= FALSE
			endsceneRequested 						= FALSE
			LoadedInteriorForCut					= FALSE
			pinnedInterior 							= FALSE
			doneReturnText 							= FALSE
			clearedGetBackin 						= FALSE	
//			SyncdSceneDisrupted 					= FALSE	
			
			iGetBackInTimer							= 0
			iGoToFactoryTimer						= 0
			
			#IF IS_DEBUG_BUILD
				jSkipReady = TRUE
				pSkipReady = TRUE
			#ENDIF			
			
			SETTIMERA(0)
			
			IF NOT IS_PED_INJURED(pedContact)
				SET_PED_CAN_BE_DRAGGED_OUT(pedContact, FALSE)
				SET_PED_CAN_BE_TARGETTED(pedContact, FALSE)
				SET_PED_RELATIONSHIP_GROUP_HASH(pedContact, RELGROUPHASH_PLAYER)
			ENDIF
			
			SET_ROADS_BACK_TO_ORIGINAL(<<-631.51, -332.75, 32>>, <<-692.36, -228.83, 38>>)
			
			//If screen is faded out do a load scene at players coordinates now and then fade in the screen
			IF IS_SCREEN_FADED_OUT()
				IF NOT IS_REPLAY_BEING_SET_UP()
					LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
				ENDIF
				END_REPLAY_SETUP(vehCar)
			ENDIF
			IF NOT IS_SCREEN_FADED_IN()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF		
			
			IF IS_VEHICLE_DRIVEABLE(vehCar)
				FREEZE_ENTITY_POSITION(vehCar, FALSE)
//				SET_RADIO_RETUNE_UP()
			ENDIF			
			
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				IF donePH28Chat = FALSE
					KILL_ANY_CONVERSATION()
					IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH28", CONV_PRIORITY_MEDIUM)
						//Here you can have these glasses back.
						
						IF IS_REPEAT_PLAY_ACTIVE()
							REMOVE_PED_COMP_ITEM_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P0_PROGRAMMER_GLASSES)
						ELSE
							RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())			
						ENDIF
						donePH28Chat = TRUE
					ENDIF	
				ENDIF
				IF NOT DOES_BLIP_EXIST(BlipWareHouse)
					BlipWareHouse = CREATE_BLIP_FOR_COORD(vWareHouse)
//					SET_BLIP_SPRITE(BlipWareHouse, RADAR_TRACE_JEWELRY_HEIST)
					SET_BLIP_ROUTE(BlipWareHouse, TRUE)
				ENDIF	
//				IF doneHeistBlipHelp = FALSE
//					PRINT_HELP("HELP3")
//					//The ~BLIP_HEIST_PREP~ represents a Heist.
//					doneHeistBlipHelp = TRUE
//				ENDIF
			ELSE
				CLEAR_PRINTS()	
				REMOVE_BLIP(BlipWareHouse)
				PRINT_NOW("WANTED", DEFAULT_GOD_TEXT_TIME, 1)//~s~Lose your wanted level.
			ENDIF			
			
			DISABLE_CELLPHONE(FALSE)
			
			//Call this to put the radio back on
//			IF gotPlayersRadioStation = TRUE
//				SET_INITIAL_PLAYER_STATION(playersRadioStation)
//			ELSE
//				SET_INITIAL_PLAYER_STATION("RADIO_01_CLASS_ROCK")
//			ENDIF
			
			IF GET_PLAYER_RADIO_STATION_INDEX() = 255
				SET_RADIO_TO_STATION_NAME("RADIO_01_CLASS_ROCK")
			ENDIF
			 
			//Update audio
			IF IS_AUDIO_SCENE_ACTIVE("JSH1_RETURN_TO_CAR")
				STOP_AUDIO_SCENE("JSH1_RETURN_TO_CAR")
			ENDIF
			IF NOT IS_AUDIO_SCENE_ACTIVE("JSH_1_DRIVE_TO_FACTORY")
				START_AUDIO_SCENE("JSH_1_DRIVE_TO_FACTORY")
			ENDIF			
			
			REPLAY_RECORD_BACK_FOR_TIME(2.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
			
			iControlFlag = 1
		ENDIF
		
		IF iControlFlag = 1
		
//			//Update music
//			IF IS_VEHICLE_DRIVEABLE(vehCar)
//				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
//					IF GET_IS_VEHICLE_ENGINE_RUNNING(vehCar)
//						IF MusicEventPrepared = FALSE	
//							PREPARE_MUSIC_EVENT("JH1_END")
//							SETTIMERB(0)
//							MusicEventPrepared = TRUE
//						ELSE
//							IF MusicStarted[2] = FALSE	
//								IF TIMERB() > 1500
//									TRIGGER_MUSIC_EVENT("JH1_END")
//									SET_RADIO_RETUNE_DOWN()
//									MusicStarted[2] = TRUE
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
		
			IF donePH28Chat = FALSE
				KILL_ANY_CONVERSATION()
				IF NOT IS_MESSAGE_BEING_DISPLAYED()
				OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
					IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_PH28", CONV_PRIORITY_MEDIUM)
						//Here you can have these glasses back.
						IF IS_REPEAT_PLAY_ACTIVE()
							REMOVE_PED_COMP_ITEM_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_P0_PROGRAMMER_GLASSES)
						ELSE
							RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
						ENDIF
						//Clean up the glasses camera scaleform stuff now
						SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(SF_Glasses_Shutter_Index)
						SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("digitalOverlay")
						donePH28Chat = TRUE
					ENDIF	
				ENDIF
			ENDIF
		
			//Quick line of dialogue to tell the player to go back to the warehouse.
			IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
				IF doneSTP14Chat = FALSE
					IF donePH28Chat = TRUE
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
						OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
							IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_STP14", CONV_PRIORITY_MEDIUM)// Let's go back to the garment factory, I called ahead and told them to start setting up the information. 
								PRINTSTRING("Waiting for dialogue to catch up") PRINTNL()
								SETTIMERA(0)
								doneSTP14Chat = TRUE
							ENDIF	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			//Handle blips
			IF IS_VEHICLE_DRIVEABLE(vehCar)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
					IF DOES_BLIP_EXIST(BlipWareHouse)
						REMOVE_BLIP(BlipWareHouse)
					ENDIF
					IF NOT DOES_BLIP_EXIST(BlipVeh)
						BlipVeh = CREATE_BLIP_FOR_VEHICLE(vehCar)
					ENDIF
					IF doneGodText = TRUE
						IF GET_GAME_TIMER() < (iGoToFactoryTimer + DEFAULT_GOD_TEXT_TIME)
							CLEAR_PRINTS()
						ENDIF
					ENDIF
					IF doneGetInCarText2 = FALSE
						CLEAR_PRINTS()
						ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(FALSE)
						PRINT_NOW("GOTO_CAR2", DEFAULT_GOD_TEXT_TIME, 1)// ~s~Get back in your ~b~car.
						iGetBackInTimer = GET_GAME_TIMER()
						doneGetInCarText2 = TRUE
					ENDIF
				ELSE
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						IF clearedGetBackin = FALSE	
							IF doneGetInCarText2 = TRUE
							AND GET_GAME_TIMER() < (iGetBackInTimer + DEFAULT_GOD_TEXT_TIME)
								CLEAR_PRINTS()
								clearedGetBackin = TRUE
							ENDIF
						ENDIF
						IF TIMERA() > 3000
							IF doneSTP14Chat = TRUE
								IF doneGodText = FALSE
									IF NOT IS_MESSAGE_BEING_DISPLAYED()
										KILL_ANY_CONVERSATION()
										PRINT_NOW("GO_HOME", DEFAULT_GOD_TEXT_TIME, 1)//~s~Go to the ~y~garment factory.
										SETTIMERA(0)
										iGoToFactoryTimer = GET_GAME_TIMER()
										doneGodText = TRUE
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
						IF doneGodText = TRUE
						AND doneSTP14Chat = TRUE
							IF TIMERA() > 2000
								IF NOT IS_MESSAGE_BEING_DISPLAYED()
								OR NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()
									IF CREATE_CONVERSATION(MyLocalPedStruct, "JHS1AUD", "JHS1_STP13", CONV_PRIORITY_AMBIENT_HIGH)//So, what did you see?....
										iControlFlag = 2
									ENDIF	
								ENDIF
							ENDIF
						ENDIF
							
						IF DOES_BLIP_EXIST(BlipVeh)
							REMOVE_BLIP(BlipVeh)
						ENDIF
						IF NOT DOES_BLIP_EXIST(BlipWareHouse)
							BlipWareHouse = CREATE_BLIP_FOR_COORD(vWareHouse, TRUE)
							SET_BLIP_ROUTE(BlipWareHouse, TRUE)
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(BlipWareHouse)
							CLEAR_PRINTS()	
							REMOVE_BLIP(BlipWareHouse)
							PRINT_NOW("WANTED", DEFAULT_GOD_TEXT_TIME, 1)//~s~Lose your wanted level.
						ENDIF
						IF DOES_BLIP_EXIST(BlipVeh)
							REMOVE_BLIP(BlipVeh)
						ENDIF
					ENDIF
				ENDIF
			ENDIF			
		
		ENDIF
		
		IF iControlFlag = 2				
			
			//Request the end cutscene and planning board section once the player gets close enough to the warehouse.
			IF endsceneRequested = FALSE
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vWareHouse) < DEFAULT_CUTSCENE_LOAD_DIST
				AND GET_DISTANCE_BETWEEN_COORDS(<<723.2, -1089, 22.1>>, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 20
					//Set board intro cutscene to load and save reference to Lester and car 
					//so the heist controller can grab ownership of them and clean them up.
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, TRUE)
					PRINTSTRING("SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, TRUE)")
					g_sTriggerSceneAssets.ped[0] = pedContact
					g_sTriggerSceneAssets.veh[0] = vehCar
					endsceneRequested = TRUE
				ENDIF
			ELSE
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vWareHouse) > DEFAULT_CUTSCENE_UNLOAD_DIST
				OR GET_DISTANCE_BETWEEN_COORDS(<<723.2, -1089, 22.1>>, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 20
					//remove cutscene if player then drives further away from the warehouse
					SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, FALSE)
					PRINTSTRING("SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, FALSE)")
					endsceneRequested = FALSE
				ENDIF		
			ENDIF
			IF LoadedInteriorForCut = FALSE
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vWareHouse) < 200
				AND GET_DISTANCE_BETWEEN_COORDS(<<723.2, -1089, 22.1>>, GET_ENTITY_COORDS(PLAYER_PED_ID())) > 20
					IF pinnedInterior = FALSE
						WarehouseInterior = GET_INTERIOR_AT_COORDS(<<711.2, -960.2, 30.6>>)
						PIN_INTERIOR_IN_MEMORY(WarehouseInterior)
						NEW_LOAD_SCENE_START_SPHERE(<<713.1, -963.4, 29.9>>, 15, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE)
						pinnedInterior = TRUE
					ENDIF
				ENDIF
				IF pinnedInterior = TRUE
					IF IS_INTERIOR_READY(WarehouseInterior)
						PRINTSTRING("LoadedInteriorForCut = TRUE") PRINTNL()
						LoadedInteriorForCut = TRUE
					ENDIF
				ENDIF
			ELSE
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vWareHouse) > 200
				OR GET_DISTANCE_BETWEEN_COORDS(<<723.2, -1089, 22.1>>, GET_ENTITY_COORDS(PLAYER_PED_ID())) < 20
					IF pinnedInterior = TRUE
						UNPIN_INTERIOR(WarehouseInterior)
						NEW_LOAD_SCENE_STOP()
						pinnedInterior = FALSE
						LoadedInteriorForCut = FALSE
					ENDIF
				ENDIF			
			ENDIF
			
			//Handle blips
			IF IS_VEHICLE_DRIVEABLE(vehCar)
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
					IF DOES_BLIP_EXIST(BlipWareHouse)
						REMOVE_BLIP(BlipWareHouse)
					ENDIF
					IF NOT DOES_BLIP_EXIST(BlipVeh)
						BlipVeh = CREATE_BLIP_FOR_VEHICLE(vehCar)
						IF doneGetInCarText2 = FALSE
							CLEAR_PRINTS()
							ADD_NEXT_MESSAGE_TO_PREVIOUS_BRIEFS(FALSE)
							PRINT_NOW("GOTO_CAR2", DEFAULT_GOD_TEXT_TIME, 1)// ~s~Get back in your ~b~car.
							doneGetInCarText2 = TRUE
						ENDIF
					ENDIF
					IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
					ENDIF
				ELSE
					IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) = 0
						IF DOES_BLIP_EXIST(BlipVeh)
							REMOVE_BLIP(BlipVeh)
						ENDIF
						IF NOT DOES_BLIP_EXIST(BlipWareHouse)
							BlipWareHouse = CREATE_BLIP_FOR_COORD(vWareHouse, TRUE)
//							SET_BLIP_SPRITE(BlipWareHouse, RADAR_TRACE_JEWELRY_HEIST)
							SET_BLIP_ROUTE(BlipWareHouse, TRUE)
						ENDIF
						IF NOT IS_MESSAGE_BEING_DISPLAYED()
							IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
								PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
							ENDIF	
						ENDIF
						
						//Run end mocap scene once player gets here without a wanted level
						IF NOT IS_ENTITY_PLAYING_ANIM(PLAYER_PED_ID(), "veh@std@ds@enter_exit", "jump_out")
							IF DOES_BLIP_EXIST(BlipWareHouse)
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << vWareHouse.x, vWareHouse.y, 23.13782>>, <<2, 2, LOCATE_SIZE_HEIGHT>>, TRUE)
								ENDIF
							ENDIF
							IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << vWareHouse.x, vWareHouse.y, 23.13782>>, <<8.5, 11, LOCATE_SIZE_HEIGHT>>, FALSE, TRUE, TM_IN_VEHICLE)
				//				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
								IF DOES_BLIP_EXIST(BlipWareHouse)
									REMOVE_BLIP(BlipWareHouse)
								ENDIF
								IF DOES_BLIP_EXIST(BlipVeh)
									REMOVE_BLIP(BlipVeh)
								ENDIF	
								
								iControlFlag = 3
							ENDIF
						ENDIF
					ELSE
						IF DOES_BLIP_EXIST(BlipWareHouse)
							CLEAR_PRINTS()	
							REMOVE_BLIP(BlipWareHouse)
							PRINT_NOW("WANTED", DEFAULT_GOD_TEXT_TIME, 1)//~s~Lose your wanted level.
						ENDIF	
						IF DOES_BLIP_EXIST(BlipVeh)
							REMOVE_BLIP(BlipVeh)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
		
		IF iControlFlag = 3		
			
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), DEFAULT_VEH_STOPPING_DISTANCE, 1, 0.2)			
					IF DOES_ENTITY_EXIST(vehCar)
						IF IS_VEHICLE_DRIVEABLE(vehCar)
							SET_VEHICLE_ENGINE_HEALTH(vehCar, 1000)
						ENDIF
					ENDIF
					
					//Update audio
					IF IS_AUDIO_SCENE_ACTIVE("JSH_1_DRIVE_TO_FACTORY")
						STOP_AUDIO_SCENE("JSH_1_DRIVE_TO_FACTORY")
					ENDIF			
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					
					SETTIMERA(0)
					KILL_ANY_CONVERSATION()		
					CanMissionFail = FALSE
					iControlFlag = 0
					missionStage = STAGE_END_MISSION
				ENDIF
			ELSE
				//Update audio
				IF IS_AUDIO_SCENE_ACTIVE("JSH_1_DRIVE_TO_FACTORY")
					STOP_AUDIO_SCENE("JSH_1_DRIVE_TO_FACTORY")
				ENDIF		
				
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
				
				SETTIMERA(0)
				KILL_ANY_CONVERSATION()		
				CanMissionFail = FALSE
				iControlFlag = 0
				missionStage = STAGE_END_MISSION				
			ENDIF
			
		ENDIF
ENDPROC


//PURPOSE: Last stage where player and Lester have to go into the office to end the mission.
//PROC DO_STAGE_GO_TO_OFFICE()
//
//	IF iControlFlag = 0		
//		//DEBUG STAGE SELECTOR
//		#IF IS_DEBUG_BUILD
//			IF MissionStageBeingSkippedTo = TRUE
//				STAGE_SELECTOR_MISSION_SETUP()
//			ENDIF
//		#ENDIF
//		
//		DELETE_TEMP_STORE_STAFF()
//		KILL_ANY_CONVERSATION()							
//		iControlFlag = 0
//		missionStage = STAGE_END_MISSION
//	ENDIF
//			
//			REQUEST_CUTSCENE("JH_1_MCS_4_P1_CONCAT")			
//			
//			SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//			
//			IF IS_VEHICLE_DRIVEABLE(vehCar)
//				IF NOT IS_PED_INJURED(pedContact)
////					OPEN_SEQUENCE_TASK(seqSequencePedContact)
//					TASK_LEAVE_VEHICLE(pedContact, vehCar)
////					CLOSE_SEQUENCE_TASK(seqSequencePedContact)
////					TASK_PERFORM_SEQUENCE(pedContact, seqSequencePedContact)
////					CLEAR_SEQUENCE_TASK(seqSequencePedContact)
//				ENDIF
//			ENDIF				
//			
//			SETTIMERA(0)
//			
//			//reset stage selector flag
//			#IF IS_DEBUG_BUILD
//				MissionStageBeingSkippedTo = FALSE
//			#ENDIF			
//			
//			CanMissionFail = FALSE
//			
//			iControlFlag = 1
//		
//		ENDIF
//		
//		IF iControlFlag = 1
//			
//			IF TIMERA() > 175
//				IF IS_VEHICLE_DRIVEABLE(vehCar)
//					IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
//						TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), vehCar)
//						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						SETTIMERA(0)
//						iControlFlag = 2
//					ELSE
//						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						SETTIMERA(0)
//						iControlFlag = 2
//					ENDIF
//				ENDIF			
//			ENDIF
//		
//		ENDIF
//		
//		IF iControlFlag = 2
//			
//			IF TIMERA() > 1000
//				KILL_ANY_CONVERSATION()									
//			
//				iControlFlag = 0
//				missionStage = STAGE_JS_SCOPESTORE_03_MOCAP_CUT
//			
//			ENDIF
//		
//		ENDIF
//
//ENDPROC

//PROC DO_STAGE_JS_SCOPESTORE_03_MOCAP_CUT()
//
//	IF iControlFlag = 0
//		
//		iCutsceneStage = 0
//		
//		DELETE_TEMP_STORE_STAFF()		
//		
////		IF IS_SCREEN_FADED_OUT()
////			LOAD_SCENE(<<706.19, -964.47, 30.40>>)
////			iControlFlag = 1
////		ENDIF
//		
//		iControlFlag = 1
//		
//	ENDIF
//	
//	IF iControlFlag = 1
//	
//		SWITCH iCutsceneStage
//		
//			CASE 0 	
//				
//				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
//				
//				WHILE NOT HAS_CUTSCENE_LOADED()
//					WAIT(0)
//					PRINTSTRING("Stuck waiting for cutscene to load..") PRINTNL()
//				ENDWHILE
//				
//				//Register entities for cutscene
//				//Lester
//				IF NOT IS_PED_INJURED(pedContact)
//					REGISTER_ENTITY_FOR_CUTSCENE(pedContact, "Lester", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				
//				//Michaels car
//				IF IS_VEHICLE_DRIVEABLE(vehCar)
//					REGISTER_ENTITY_FOR_CUTSCENE(vehCar, "Michaels_car", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				ENDIF
//				
//				//Configure script systems into a safe mode.
//				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//				DISPLAY_RADAR(FALSE)
//				
//				CLEAR_PRINTS()
//				KILL_ANY_CONVERSATION()
//				
//				START_CUTSCENE()
//				
////				SET_SEAMLESS_CUTS_ACTIVE(bTracker)
//
////				IF NOT IS_PED_INJURED(pedContact)
////					IF IS_PED_GROUP_MEMBER(pedContact, PLAYER_GROUP_ID())
////						REMOVE_PED_FROM_GROUP(pedContact)
////					ENDIF
////					CLEAR_PED_TASKS(pedContact)
////					SET_ENTITY_COORDS(pedContact, << 716.3973, -979.2612, 23.1119 >>)
////				ENDIF				
//				
////				WHILE NOT  IS_CUTSCENE_ACTIVE()
////					WAIT(0)
////				ENDWHILE
////				
////				WAIT(1000)
//				
//				IF NOT IS_SCREEN_FADED_IN()
//					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
//				ENDIF
//				
//				iCutsceneStage ++
//				
//			BREAK
//			
//			CASE 1
//				
//				#IF IS_DEBUG_BUILD
//					IF NOT HAS_CUTSCENE_FINISHED()
//						IF IS_KEYBOARD_KEY_PRESSED(KEY_J)
//							STOP_CUTSCENE()
//						ENDIF
//					ENDIF
//				#ENDIF
//				
//				IF HAS_CUTSCENE_FINISHED()
//			      	//Return script systems to normal.
//					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
//
//					iCutsceneStage++
//				ENDIF	
//				
//			BREAK
//			
//			CASE 2
//				
//				IF DOES_ENTITY_EXIST(pedContact)
//					IF NOT IS_PED_INJURED(pedContact)
//						DELETE_PED(pedContact)
//					ENDIF
//				ENDIF
//	
//				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//				iCutsceneStage++
//			
//			BREAK
//			
//			CASE 3
//			
//				iControlFlag = 0
//				missionStage = STAGE_END_MISSION
//				iCutsceneStage ++
//			
//			BREAK
//		
//		ENDSWITCH
//	
//	ENDIF
//	
//ENDPROC

//PURPOSE: Ends mission
PROC DO_STAGE_END_MISSION()
	
//	IF iControlFlag = 0
//		
//		//Flags
//		bCutsceneRunning = FALSE
//		LesterLeftVehicle = FALSE
//		playerLeftVehicle = FALSE
//		
//		//destroy these cams as a safety check
//		IF DOES_CAM_EXIST(initCam)
//			DESTROY_CAM(initCam)
//		ENDIF
//		IF DOES_CAM_EXIST(destCam)
//			DESTROY_CAM(destCam)
//		ENDIF
//		
//		//Create camera's
//		IF NOT DOES_CAM_EXIST(initCam)
//			initCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<724.468201,-992.068420,24.251366>>,<<5.259225,0.000715,25.889177>>,44.131210)
//		ENDIF
//		IF NOT DOES_CAM_EXIST(destCam)
//			destCam = CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, <<724.372742,-991.320496,24.433039>>,<<31.499252,0.000715,-4.206256>>,44.131210)
//		ENDIF
//		
//		iCutsceneStage = 0
//		bCutsceneRunning = TRUE
//		iControlFlag = 1
//		
//	ENDIF
//	
//	IF iControlFlag = 1
//		
//		WHILE bCutsceneRunning = TRUE
//			
//			SWITCH iCutsceneStage 
//				
//				CASE 0
//				
//					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//					DISPLAY_RADAR(FALSE)
//					DISPLAY_HUD(FALSE)
//					
//					IF DOES_ENTITY_EXIST(vehCar)
//						IF IS_VEHICLE_DRIVEABLE(vehCar)
//							POINT_CAM_AT_ENTITY(initCam, vehCar, <<0,0,0>>)
//						ENDIF
//					ENDIF
//					
//					SET_CAM_ACTIVE_WITH_INTERP(destCam, initCam, 5000)
//					RENDER_SCRIPT_CAMS(TRUE, FALSE)
//					SETTIMERA(0)
//					iCutsceneStage ++
//					
//				BREAK
//				
//				CASE 1
//					
//					IF playerLeftVehicle = FALSE
//						IF TIMERA() > 1000
//							IF DOES_ENTITY_EXIST(vehCar)
//								IF IS_VEHICLE_DRIVEABLE(vehCar)		
//									IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
//										TASK_LEAVE_VEHICLE(PLAYER_PED_ID(), vehCar)
//										playerLeftVehicle = TRUE
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//					IF LesterLeftVehicle = FALSE
//						IF TIMERA() > 1800
//							IF DOES_ENTITY_EXIST(vehCar)
//								IF IS_VEHICLE_DRIVEABLE(vehCar)	
//									IF DOES_ENTITY_EXIST(pedContact)
//										IF NOT IS_PED_INJURED(pedContact)
//											IF IS_PED_IN_VEHICLE(pedContact, vehCar)
//												TASK_LEAVE_VEHICLE(pedContact, vehCar)
//												LesterLeftVehicle = TRUE
//											ENDIF
//										ENDIF
//									ENDIF
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				
//					IF TIMERA() > 5000
//							
//						iControlFlag = 2					
//						bCutsceneRunning = FALSE
//							
//					ENDIF
//				
//				BREAK
//				
//			ENDSWITCH
//			
//			WAIT(0)
//			
//		ENDWHILE
//	
//	ENDIF
	
	IF iControlFlag = 0

//		IF IS_NEW_LOAD_SCENE_ACTIVE()
//			IF IS_NEW_LOAD_SCENE_LOADED()				
//				NEW_LOAD_SCENE_STOP()	
//				SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, TRUE) //Add this in here for to guarantee it's going to get called incase something else breaks
//				g_sTriggerSceneAssets.ped[0] = pedContact
//				g_sTriggerSceneAssets.veh[0] = vehCar			
//				MissionPassed()
//			ELSE
//				PRINTSTRING("waiting on new load scene finishing") PRINTNL()
//			ENDIF
//		ENDIF
		
		//Have a safety net to make sure mission progresses if new load scene doesn't return true quick enough	
		NEW_LOAD_SCENE_STOP()
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, TRUE) //Add this in here for to guarantee it's going to get called incase something else breaks
		g_sTriggerSceneAssets.ped[0] = pedContact
		g_sTriggerSceneAssets.veh[0] = vehCar
		MissionPassed()	
	
	ENDIF
		
ENDPROC

//PURPOSE: Handles peds looking at the player
PROC PEDS_LOOK_AT_PLAYER()
		
	//Give Lester a task to look at the player
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(pedContact)
			IF NOT IS_PED_INJURED(pedContact)
				IF DOES_ENTITY_EXIST(vehCar)
					IF IS_VEHICLE_DRIVEABLE(vehCar)
						IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehCar)
							IF LesterLookTaskGiven = FALSE
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedContact) < 10
									TASK_LOOK_AT_ENTITY(pedContact, PLAYER_PED_ID(), -1, SLF_DEFAULT, SLF_LOOKAT_HIGH)
									LesterLookTaskGiven = TRUE
								ENDIF
							ENDIF
							IF LesterLookTaskGiven = TRUE
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedContact) > 10
									TASK_CLEAR_LOOK_AT(pedContact)
									LesterLookTaskGiven = FALSE
								ENDIF
							ENDIF
						ELSE
							IF LesterLookTaskGiven = TRUE
								TASK_CLEAR_LOOK_AT(pedContact)
								SET_PED_CAN_HEAD_IK(pedContact, TRUE)
								LesterLookTaskGiven = FALSE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Give shop assistant a task to look at the player
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(shopAssistant)
			IF NOT IS_PED_INJURED(shopAssistant)
				IF shopAssistantLookTaskGiven = FALSE
//					PRINTSTRING("shopAssistantLookTaskGiven = FALSE") PRINTNL()
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), shopAssistant) < 5
//						PRINTSTRING("distance is less than 5 between shopassistant and player") PRINTNL()
						TASK_LOOK_AT_ENTITY(shopAssistant, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
						shopAssistantLookTaskGiven = TRUE
					ELSE
//						PRINTSTRING("distance is more than 5 between shopassistant and player") PRINTNL()
					ENDIF
				ENDIF
				IF shopAssistantLookTaskGiven = TRUE
//					PRINTSTRING("shopAssistantLookTaskGiven = TRUE") PRINTNL()
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), shopAssistant) > 5
//						PRINTSTRING("distance is more than 5 between shopassistant and player") PRINTNL()
						TASK_CLEAR_LOOK_AT(shopAssistant)
						shopAssistantLookTaskGiven = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

	//Give Guard a task to look at the player
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF DOES_ENTITY_EXIST(shopGuard)
			IF NOT IS_PED_INJURED(shopGuard)
				IF shopGuardLookTaskGiven = FALSE
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), shopGuard) < 10
						TASK_LOOK_AT_ENTITY(shopGuard, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_HIGH)
						shopGuardLookTaskGiven = TRUE
					ENDIF
				ENDIF
				IF shopGuardLookTaskGiven = TRUE
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), shopGuard) > 10
						TASK_CLEAR_LOOK_AT(shopGuard)
						shopGuardLookTaskGiven = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	

ENDPROC


//═════════════════════════════════╡ MAIN SCRIPT ╞═══════════════════════════════════

SCRIPT
	SET_MISSION_FLAG(TRUE)
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		TRIGGER_MUSIC_EVENT("JH1_FAIL")
		PRINTSTRING("playing fail music for death arrest") PRINTNL()
		
		//Clear this to ensure the planning board sequence doesn't kick off on death/arrest.
		SET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_JEWEL_PRIME_BOARD_TRANSITION, FALSE)
		
		Mission_Flow_Mission_Force_Cleanup()
		MissionCleanup()
	ENDIF
	
	//INFORM_MISSION_STATS_OF_MISSION_START_JEWELERY_HEIST_SETUP()
	DISABLE_TAXI_HAILING(TRUE)
	
	missionStage = STAGE_INIT_MISSION
	
	WHILE TRUE
			
		//For video recorder
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TheJewelStoreJobSetup") 						
				
		PEDS_LOOK_AT_PLAYER()
		
		LESTER_WANTED_CHAT()
					
		RUN_SEAMLESS_CUTSCENE(interpCamera, bTracker)	
		
		CHECK_FOR_MOD_SHOP()
		
		VIDEO_RECORDER_MANAGER()
			
		SWITCH MissionStage
		
			CASE STAGE_INIT_MISSION
				DO_STAGE_INIT_MISSION()
			BREAK
			CASE STAGE_OPENING_CUTSCENE
				DO_STAGE_OPENING_CUTSCENE()
			BREAK
			CASE STAGE_DRIVE_TO_STORE
				DO_STAGE_DRIVE_TO_STORE()
			BREAK
			CASE STAGE_SCOUT_OUT_STORE
				DO_STAGE_SCOUT_OUT_STORE()
			BREAK
			CASE STAGE_FIND_ROOF_ENTRANCE
				DO_STAGE_FIND_ROOF_ENTRANCE()
			BREAK
			CASE STAGE_CHECK_OUT_ROOF
				DO_STAGE_CHECK_OUT_ROOF()
			BREAK
			CASE STAGE_DRIVE_TO_WAREHOUSE
				DO_STAGE_DRIVE_TO_WAREHOUSE()
			BREAK
			CASE STAGE_END_MISSION
				DO_STAGE_END_MISSION()
			BREAK
			
		ENDSWITCH
		
		IF CheckpointReplayStarting = FALSE
			FAIL_CHECKS()
		ENDIF
		
		//Checks for debug keys being pressed. J-skip, S-Pass, F-Fail, P-Previous
		#IF IS_DEBUG_BUILD
			Debug_Options()
			IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, 0) = TRUE
				
				IF IS_CUTSCENE_ACTIVE()
					PRINTSTRING("Waiting for cutscene to stop") PRINTNL()
					STOP_CUTSCENE()
					REMOVE_CUTSCENE()
				ENDIF
				
				IF NOT IS_SCREEN_FADED_OUT()
					DO_SCREEN_FADE_OUT(0)
				ENDIF
				
				iControlFlag 					= 0
				iCutsceneStage 					= 0
				CanMissionFail 					= FALSE
				playerCausedTroubleInsideStore 	= FALSE
				MissionStageBeingSkippedTo 		= TRUE
				
				IF iReturnStage = 0
					missionStage = STAGE_DRIVE_TO_STORE
				ENDIF
				IF iReturnStage = 1
					missionStage = STAGE_SCOUT_OUT_STORE
				ENDIF
				IF iReturnStage = 2
					missionStage = STAGE_FIND_ROOF_ENTRANCE
				ENDIF
				IF iReturnStage = 3
					missionStage = STAGE_CHECK_OUT_ROOF
				ENDIF
				IF iReturnStage = 4
					missionStage = STAGE_DRIVE_TO_WAREHOUSE
				ENDIF
			ENDIF
		#ENDIF
		
		WAIT(0)
	ENDWHILE
	
	MissionCleanup()
ENDSCRIPT
