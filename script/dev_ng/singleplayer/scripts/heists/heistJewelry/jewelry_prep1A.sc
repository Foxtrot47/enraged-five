
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "flow_public_core_override.sch"
USING "select_mission_stage.sch"
USING "replay_public.sch"
USING "CompletionPercentage_public.sch"
USING "locates_public.sch"
USING "comms_control_public.sch"

USING "script_ped.sch"

USING "asset_management_public.sch"
USING "mission_event_manager.sch"

USING "commands_physics.sch"
USING "commands_recording.sch"

// ENTITIES
//-----------------------------------------------------------

// Entity lists
ENUM MISSION_PED_FLAGS
	mpf_invalid = -1,
	mpf_bugstar_foreman_front,
	mpf_bugstar_foreman_rear,
	mpf_bugstar_front_1,
	mpf_bugstar_front_2,
	mpf_num_peds
ENDENUM

ENUM MISSION_VEHICLE_FLAGS
	mvf_invalid = -1,
	mvf_bugstar_van_1,
	mvf_bugstar_van_2,
	mvf_bugstar_van_3,
	mvf_security,
	mvf_replay,
	mvf_num_vehicles
ENDENUM

ENUM MISSION_OBJECT_FLAGS
	mof_blank,
	mof_num_objects
ENDENUM

ENUM PED_AI_STATES
	PEDAI_NONE = -1,
	PEDAI_WORKING,
	PEDAI_INVESTIGATE,
	PEDAI_CONFRONTING_PLAYER,
	PEDAI_COMBAT_PLAYER,
	PEDAI_HANDSUP,
	PEDAI_FLEEING,
	PEDAI_NUM_STATES
ENDENUM

ENUM PLAYER_STATUS
	PLAYERSTATUS_NONE = -1,
	PLAYERSTATUS_BLOCKING_ENTRANCE_ON_FOOT,
	PLAYERSTATUS_BLOCKING_ENTRANCE_WITH_VEHICLE,
	PLAYERSTATUS_WAREHOUSE_ON_FOOT,
	PLAYERSTATUS_WAREHOUSE_IN_VEHICLE,
	PLAYERSTATUS_ENTRANCE_DITCH_VEH,
	PLAYERSTATUS_WAREHOUSE_DITCH_VEH,
	PLAYERSTATUS_VAN_ENTERING,
	PLAYERSTATUS_VAN_EXITING,
	PLAYERSTATUS_VAN_DAMAGE,
	PLAYERSTATUS_VAN_STEALING,
	PLAYERSTATUS_VAN_STEALING_TOW,
	PLAYERSTATUS_SEC_CAR_STEALING
ENDENUM

STRUCT SCRIPT_AI_EVENT
	MISSION_PED_FLAGS	ePedTransmitter
	TEXT_LABEL_63		strEventName
	TEXT_LABEL_23		strConvo
	VECTOR				vEventCoord
	FLOAT				fEventRadius
	INT					iTimeOfEvent
	INT					iEventLife
	INT 				iEventDelay
	BOOL				bRaiseAlarm
	BOOL				bCalledSecurity
ENDSTRUCT

// Entity structs
STRUCT PED_STRUCT
	PED_INDEX			id
	TEXT_LABEL_15		strDebugName
	PED_AI_STATES		eAI											= PEDAI_WORKING
	PED_AI_STATES		eAIPrev										= PEDAI_NONE
	PED_AI_STATES		eAIPrevFrame								= PEDAI_NONE
	BOOL				bAIStateChangedLastFrame
	TEXT_LABEL_31		strAIEvent
	TEXT_LABEL_31		strAIEventPrev
	TEXT_LABEL_31		strAIEventPrevFrame
	BOOL				bAIEventChangedLastFrame
	INT					iReceivedEvent								= -1
	BOOL				bDirectEvent								= FALSE
	BOOL				bAlarmEvent									= FALSE
	
	INT					iTimeOfEnteringState						= -1
	INT					iTimeOfStateLastSet							= -1
	BOOL				bCanSeePlayer								= FALSE
	INT					iCanSeeTimer								= 0
	BOOL				bCanHearPlayer								= FALSE
	FLOAT				fEventNoticeRange							= 0.0
	VECTOR				vLastPosPlayerHeardAt
	INT					iFlagTask									= 0
	INT					iFlagState									= 0
	INT					iDiaFlag									= 0
	BOOL				bPlayedConvoStart							= FALSE
	SCRIPT_AI_EVENT		sQueuedBroadcastEvent
	BOOL				bAcquaintedPlayer
	BOOL				bThinksPlayerIsPolice
	BOOL				bHasFoughtWithPlayer
	BOOL				bObservedBeingKOd
	
	BOOL				bReactsToEntrance							= FALSE
	BOOL				bReactsToIntrusion							= FALSE
	VECTOR				vInvestigationCoord
	ENTITY_INDEX		entityInvestigating							= NULL
	INT					iInvestigateTimer							= -1
	BOOL				bDisplayAIBlip								= TRUE
	AI_BLIP_STRUCT		sAIBlip
	BOOL				b_BlipsEnemy								= FALSE
	INT					iTimeKilled									= -1
	INT					iWarningsToLeaveWarehouse					= 0
	
	// Prop spwaning, for scenario mimicing stuff
	OBJECT_INDEX		objProp										= NULL
	PED_BONETAG			pedPropHand									= BONETAG_NULL
	MODEL_NAMES			propName									= DUMMY_MODEL_FOR_SCRIPT
	BOOL				bDropWhenNotInWorkingState					= FALSE
	BOOL				bSafeToCreateProp							= FALSE
	
ENDSTRUCT

STRUCT VEHICLE_STRUCT
	VEHICLE_INDEX		id
	BLIP_INDEX			blip
	PED_INDEX			peds[4]
	BOOL				bAutoCleanup			= TRUE
ENDSTRUCT


// Entity Arrays
PED_STRUCT							peds[mpf_num_peds]
PED_STRUCT							pedSecurity
VEHICLE_STRUCT						vehs[mvf_num_vehicles]
OBJECT_INDEX						objs[mof_num_objects]

// MISSION STAGE ENUMS
//------------------------------------------------------------------

ENUM MISSION_FAIL_FLAGS
	mff_debug_forced,
	mff_van_dead,
	mff_vans_dead,
	mff_van_stuck,
	mff_van_abandoned,
	mff_mission_abandoned,
	mff_default
ENDENUM

ENUM MISSION_STAGE_FLAGS
	msf_bugstar_steal,
	msf_mission_passed,
	msf_num_mission_stages
ENDENUM

ENUM MISSION_EVENTS_LIST
	mef_dialogue,
	mef_bugstar_staff,
	mef_security,
	mef_alarms,
	mef_music_manager,
	mef_num_events
ENDENUM

ENUM PED_INJURED_TYPE
	INJUREDTYPE_UNARMED		= 1,		// fist fight
	INJUREDTYPE_MELEWEAPON	= 2,		// bats, golf clubs, etc
	INJUREDTYPE_GUN			= 4			// all other weapons
ENDENUM

// CONSTANTS
//---------------------------------------------------------------------

TEXT_LABEL_15						str_Dialogue 									= "JHP1ADS"
TEXT_LABEL_63 						str_Alarm 										= "JEWEL_STORE_HEIST_SETUP_BUGSTAR_ALARMS"

VECTOR 								v_WarehouseCoord 								= <<154.947784,-3092.523438,4.911984>>

CONST_FLOAT							ALERT_PED_NEAR_VAN_DIST							5.0

CONST_INT							WARNING_LIMIT_STAFF_LEAVE_AREA					5
CONST_INT							WARNING_LIMIT_STAFF_LEAVE_VAN					3
CONST_INT							WARNING_LIMIT_SEC_LEAVE_AREA					5
CONST_INT							WARNING_LIMIT_SEC_LEAVE_VAN						3

CONST_FLOAT							VAN_USABLE_RANGE_VAN_FROM_PLAYER				300.0
CONST_FLOAT							VAN_USABLE_RANGE_WAREHOUSE_FROM_PLAYER			500.0

CONST_FLOAT							PED_CLEANUP_RANGE_PED_FROM_PLAYER				300.0
CONST_FLOAT							PED_CLEANUP_RANGE_PED_FROM_WAREHOUSE			300.0

CONST_FLOAT						 	F_CONVO_PLAY_DISTANCE							25.0

TEXT_LABEL_23						str_wpSecurityArrive 							= "jhp1a_sec_arrive"

#IF IS_DEBUG_BUILD
CONST_INT 							I_SPACING 										10
#ENDIF

// VARIABLES
//----------------------------------------------------------------------

// Integral script structure variables
INT									i_missionStage								= enum_to_int(msf_bugstar_steal)
INT 								i_missionSubstage							= 0
BOOL 								b_StageSetup								= FALSE
BOOL								b_DoSkip									= FALSE		
INT									i_SkipToStage								= 0
BOOL 								b_MissionFailed								= FALSE
MISSION_FAIL_FLAGS					e_FailReason								= mff_default
ASSET_MANAGEMENT_DATA				s_AssetData
LOCATES_HEADER_DATA					s_LocatesData
MISSION_EVENT_LINK_STRUCT			s_Events[mef_num_events]
SCRIPT_AI_EVENT						s_BroadcastEvent[20]
TEXT_LABEL_31						str_AlarmAIEvent

structPedsForConversation			s_Convo
BOOL								b_ConvoWasPlayingLastFrame					= FALSE
INT									i_TimeOfLastConvo							= -1

REL_GROUP_HASH						rel_bugstar
SEQUENCE_INDEX						seq_main

INTERIOR_INSTANCE_INDEX 			interior_warehouse

// Conversation vars
MISSION_PED_FLAGS 					eQueuedConvoSpeaker				= mpf_invalid
PED_AI_STATES 						eQueuedConvoPriority 			= PEDAI_NONE
TEXT_LABEL_15 						strQueuedConvoRoot				= ""
TEXT_LABEL_15						strQueuedConvoLine				= ""
INT 								iQueuedConvoDelay				= 0
FLOAT 								fQueuedConvoDist				= 99999999
BOOL								bQueuedConvoInterrupt			= FALSE
BOOL								bQueuedConvoDoNotFinish			= FALSE
BOOL								bQueuedConvoCanBeInterrupted 	= TRUE
BOOL								bQueuedConvoKillAtDistance		= FALSE
BOOL								bQueuedConvoStoreForFutureResumption = FALSE

MISSION_PED_FLAGS 					eCurrentConvoSpeaker			= mpf_invalid


TEXT_LABEL_15 						strCurrentConvoRoot				= ""

PED_AI_STATES 						eCurrentConvoPriority 			= PEDAI_NONE
BOOL								bCurrentConvoCanBeInterruped	= TRUE
BOOL								bCurrentConvoStoreForFutureResumption = FALSE
BOOL								bCurrentConvoKillAtDistance		= FALSE
INT									iStockCheckListProg

MISSION_PED_FLAGS 					eInterruptedConvoSpeaker			= mpf_invalid
PED_AI_STATES 						eInterruptedConvoPriority 			= PEDAI_NONE
TEXT_LABEL_15						strInterruptedConvoRoot				= ""
TEXT_LABEL_15						strInterruptedConvoLine				= ""
BOOL								bInterruptedConvoCanBeInterrupted 	= TRUE
BOOL								bInterruptedConvoKillAtDistance		= FALSE

// Misison specific vars
BOOL								bPromptGetBackInVan							= TRUE
BOOL								b_HasPlayerDamagedAVan						= FALSE
BOOL								b_WantedLevelGiven							= FALSE
BOOL 								bIsPlayerStillStealth						= FALSE
INT									i_VanDamageTimer							= -1
BOOL								b_GotWantedAfterStealingVan					= FALSE

PLAYER_STATUS						e_PlayerStatus								= PLAYERSTATUS_NONE
MISSION_VEHICLE_FLAGS				e_PlayersLastVan							= mvf_invalid
VEHICLE_INDEX 						veh_TowTruckAttached
VEHICLE_INDEX						veh_DumpedAtEntrance
BOOL 								bUsingNightScenario

SCENARIO_BLOCKING_INDEX				sbi_SweatshopBin

// Debug
#IF IS_DEBUG_BUILD

BOOL								bDisplayDebug
INT									iDebugRenderedThisFrame

MissionStageMenuTextStruct			s_ZMenuStruct[msf_num_mission_stages]
WIDGET_GROUP_ID						debug_widget

#ENDIF

//PURPOSE: Performs a entity specific death check on an entity to determine if it is alive
FUNC BOOL IS_ENTITY_ALIVE(ENTITY_INDEX entity)
	IF entity != null
	AND DOES_ENTITY_EXIST(entity)
		SWITCH GET_ENTITY_TYPE(entity)
			CASE ET_PED
				IF NOT IS_PED_INJURED(GET_PED_INDEX_FROM_ENTITY_INDEX(entity))
					RETURN TRUE
				ENDIF
			BREAK
			CASE ET_VEHICLE
				IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entity))
					RETURN TRUE
				ENDIF
			BREAK
			DEFAULT
				IF NOT IS_ENTITY_DEAD(entity)
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PED_SPEAK(PED_INDEX ped)
	
	IF NOT IS_ENTITY_ALIVE(ped)
	OR IS_PED_BEING_STUNNED(ped)
	OR IS_PED_BEING_STEALTH_KILLED(ped)
	OR IS_PED_RUNNING_RAGDOLL_TASK(ped)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

//PURPOSE: Prints debug output at the specified level within the mision 
DEBUGONLY PROC PRINT_DEBUG(STRING strMsg, INT debugLevel = 0)
	IF NOT IS_STRING_NULL_OR_EMPTY(strMsg) AND debugLevel >= 0 AND debugLevel <= 3
		#IF IS_DEBUG_BUILD
		SWITCH debugLevel
			CASE 0	FALLTHRU
			DEFAULT CPRINTLN(DEBUG_MISSION, "[***", GET_THIS_SCRIPT_NAME(), "***] ", strMsg)	BREAK
			CASE 1 CDEBUG1LN(DEBUG_MISSION, "[***", GET_THIS_SCRIPT_NAME(), "***] ", strMsg)	BREAK
			CASE 2 CDEBUG2LN(DEBUG_MISSION, "[***", GET_THIS_SCRIPT_NAME(), "***] ", strMsg)	BREAK
			CASE 3 CDEBUG3LN(DEBUG_MISSION, "[***", GET_THIS_SCRIPT_NAME(), "***] ", strMsg)	BREAK
		ENDSWITCH
		#ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD

CONST_FLOAT	F_DEBUG_SPACING 0.025
PROC DRAW_DEBUG_TEXT_2D_WRAPPED(STRING strDebug, INT iR, INT iG, INT iB, INT iA)
	DRAW_DEBUG_TEXT_2D(strDebug, <<0.025,0.25+(F_DEBUG_SPACING*iDebugRenderedThisFrame),0>>, iR,iG,iB,iA)
	iDebugRenderedThisFrame++
ENDPROC

PROC RENDER_PED_DEBUG(PED_STRUCT sPed)

	TEXT_LABEL_63 strDebugText
	INT i, j // info counter

	IF DOES_ENTITY_EXIST(sPed.id)
	//AND NOT IS_PED_INJURED(peds[ePed].id)
	
		IF sPed.eAI = PEDAI_INVESTIGATE
			IF NOT IS_VECTOR_ZERO(sPed.vInvestigationCoord)
				DRAW_DEBUG_SPHERE(sPed.vInvestigationCoord, 0.2, 0, 255, 0, 255)
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(sPed.id, FALSE), sPed.vInvestigationCoord, 0, 0, 255, 255)
			ENDIF
			IF DOES_ENTITY_EXIST(sPed.entityInvestigating)
				DRAW_DEBUG_LINE(GET_ENTITY_COORDS(sPed.id, FALSE), GET_ENTITY_COORDS(sPed.entityInvestigating, FALSE), 0, 255, 0, 255)
			ENDIF
		ENDIF
	
		// Ped debug name
		strDebugText = "DebugName: "
		strDebugText += sPed.strDebugName
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*10, 0,0,0,255)
		j++
		
		// Render AI state info
		REPEAT 3 i
		
			PED_AI_STATES 	e_AI
			
			SWITCH i 
				CASE 0	
					strDebugText 	= "AI(Current): "
					e_AI 			= sPed.eAI				
				BREAK
				CASE 1	
					strDebugText 	= "AI(Previous): "
					e_AI 			= sPed.eAIPrev			
				BREAK
				CASE 2
					strDebugText 	= "AI(LastFrame): "
					e_AI 			= sPed.eAIPrevFrame		
				BREAK
			ENDSWITCH
			
			SWITCH e_AI
				CASE PEDAI_NONE										strDebugText += "NONE"				BREAK
				CASE PEDAI_WORKING									strDebugText += "WORKING"			BREAK
				CASE PEDAI_INVESTIGATE								strDebugText += "INVESTIGATE"		BREAK
				CASE PEDAI_CONFRONTING_PLAYER						strDebugText += "CONF_PLAYER"		BREAK
				CASE PEDAI_COMBAT_PLAYER							strDebugText += "COMBAT"			BREAK
				CASE PEDAI_FLEEING									strDebugText += "FLEEING"			BREAK
				CASE PEDAI_HANDSUP									strDebugText += "HANDS UP"			BREAK
			ENDSWITCH
			
			DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 255,0,0,255)
			j++
		
		ENDREPEAT
		
		// AIEvent
		strDebugText = "AIEvent(Current): "
		IF NOT IS_STRING_NULL_OR_EMPTY(sPed.strAIEvent)
			strDebugText += sPed.strAIEvent
		ENDIF
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 255,0,0,255)
		j++
		
		// AIEvent Prev
		strDebugText = "AIEvent(Previous): "
		IF NOT IS_STRING_NULL_OR_EMPTY(sPed.strAIEvent)
			strDebugText += sPed.strAIEventPrev
		ENDIF
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 255,0,0,255)
		j++
		
		// AIEvent last frame
		strDebugText = "AIEvent(LastFrame): "
		IF NOT IS_STRING_NULL_OR_EMPTY(sPed.strAIEvent)
			strDebugText += sPed.strAIEventPrevFrame
		ENDIF
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 255,0,0,255)
		j++
		
		// bDirectEvent
		strDebugText = "bDirectEvent: "
		IF sPed.bDirectEvent
			strDebugText += "TRUE"
		ELSE
			strDebugText += "FALSE"
		ENDIF
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 255,0,0,255)
		j++
		
		// bAlarmEvent
		strDebugText = "bAlarmEvent: "
		IF sPed.bAlarmEvent
			strDebugText += "TRUE"
		ELSE
			strDebugText += "FALSE"
		ENDIF
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 255,0,0,255)
		j++
		
		// Time in state
		strDebugText = "TimeInState: "
		strDebugText += GET_STRING_FROM_INT(GET_GAME_TIMER() - sPed.iTimeOfEnteringState)
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 0,255,255,255)
		j++
		
		// Time in state
		strDebugText = "TimeSinceStateLastSet: "
		strDebugText += GET_STRING_FROM_INT(GET_GAME_TIMER() - sPed.iTimeOfStateLastSet)
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 0,255,255,255)
		j++
		
		// Reacts to seeing the player hanging around the entrance
		strDebugText = "Reacts(entrance): "
		IF sPed.bReactsToEntrance
			strDebugText += "TRUE"
		ELSE
			strDebugText += "FALSE"
		ENDIF
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 0,0,255,255)
		j++
		
		// Reacts to seeing the player intrude the area
		strDebugText = "Reacts(intrusion): "
		IF sPed.bReactsToIntrusion
			strDebugText += "TRUE"
		ELSE
			strDebugText += "FALSE"
		ENDIF
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 0,0,255,255)
		j++
		
		FLOAT fDot, fAngle
		VECTOR v1, v2
		v1 = NORMALISE_VECTOR( GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,1,0>>) - GET_ENTITY_COORDS(PLAYER_PED_ID()))
		v2 = NORMALISE_VECTOR( GET_ENTITY_COORDS(sPed.id, FALSE) - GET_ENTITY_COORDS(PLAYER_PED_ID()))
		fDot = DOT_PRODUCT(	v1, v2)
		fAngle = ACOS(fDot)
		
		// Angle between ped and player
		strDebugText = "Angle to player: "
		strDebugText += GET_STRING_FROM_FLOAT(fAngle)
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 0,0,0,255)
		j++
		
		// Dot product between ped and player
		strDebugText = "Dot to player: "
		strDebugText += GET_STRING_FROM_FLOAT(fDot)
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 0,0,0,255)
		j++
		
		// Reacts to seeing the player intrude the area
		strDebugText = "bObservedBeingKOd: "
		IF sPed.bObservedBeingKOd
			strDebugText += "TRUE"
		ELSE
			strDebugText += "FALSE"
		ENDIF
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 0,0,0,255)
		j++
		
		// Peds's state flag
		strDebugText = "iFlagState: "
		strDebugText += sPed.iFlagState
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 0,0,0,255)
		j++
		
		// Peds's task flag
		strDebugText = "iFlagTask: "
		strDebugText += sPed.iFlagTask
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 0,0,0,255)
		j++
		
		// Peds's dialogue flag
		strDebugText = "iDiaFlag: "
		strDebugText += sPed.iDiaFlag
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 0,0,0,255)
		j++
		
		// Wanrings to leave warehouse
		strDebugText = "iWarningsToLeaveWarehouse: "
		strDebugText += sPed.iWarningsToLeaveWarehouse
		DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, GET_ENTITY_COORDS(sPed.id, FALSE)-<<0,0,1.0>>, 0, j*I_SPACING, 0,0,0,255)
		j++
		
	ENDIF

ENDPROC
#ENDIF

FUNC BOOL CAN_PED_SEE_THIS_PED_IS_DEAD(PED_STRUCT sObserver, PED_STRUCT sVictim, BOOL &bKilledInFrontOfObserver, BOOL &bSawPlayerDoIt, BOOL &bNonLethal)

	CONST_FLOAT	F_BODY_NOTICE_DIST 10.0

	IF DOES_ENTITY_EXIST(sVictim.id)
    AND IS_PED_INJURED(sVictim.id)
	
	// Can the ped see this dead body
		VECTOR vDeadBody = GET_ENTITY_COORDS(sVictim.id, FALSE)
		IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sObserver.id), vDeadBody) < F_BODY_NOTICE_DIST
			IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(sObserver.id, sVictim.id)
			
				// Look at how longer ago the ped was killed to decide if the observer saw the incident happen
				IF GET_GAME_TIMER() - sVictim.iTimeKilled < 3000
					// Saw it happen!
					bKilledInFrontOfObserver = TRUE
				ELSE
					// Came across body
					bKilledInFrontOfObserver = FALSE
				ENDIF
				
				// If the ped was killed infront of the observer check if it was the player who killed the ped
				// and that they can see the player
				IF bKilledInFrontOfObserver
					ENTITY_INDEX tempEntity = GET_PED_SOURCE_OF_DEATH(sVictim.id)
					IF IS_ENTITY_A_PED(tempEntity)					
						PED_INDEX pedAttacker = GET_PED_INDEX_FROM_ENTITY_INDEX(tempEntity)
						IF pedAttacker = PLAYER_PED_ID()
						AND NOT IS_PED_INJURED(pedAttacker)
						AND sObserver.bCanSeePlayer
							bSawPlayerDoIt = TRUE
						ELSE
							bSawPlayerDoIt = FALSE
						ENDIF
					ELSE
						bSawPlayerDoIt = FALSE
					ENDIF
				ELSE
					bSawPlayerDoIt = FALSE
				ENDIF
			
				// Check to see if the ped was taken down none lethally
				WEAPON_TYPE eWeaponType = GET_PED_CAUSE_OF_DEATH(sVictim.id)
				IF eWeaponType = WEAPONTYPE_UNARMED
				OR (eWeaponType != WEAPONTYPE_UNARMED AND GET_WEAPON_DAMAGE_TYPE(eWeaponType) = DAMAGE_TYPE_MELEE)
					bNonLethal = TRUE
				ENDIF
				
				RETURN TRUE
			ENDIF
		ENDIF
  	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PED_SEE_DEAD_BODY(PED_STRUCT observer, BOOL &bKilledInfrontOfObserver, BOOL &bSawPlayerDoIt, BOOL &bNonLethal)

	MISSION_PED_FLAGS ePed
	
	BOOL bHasSeenABody
	
	BOOL bKIFOO
	BOOL bSPDI
	BOOL bNL

	REPEAT COUNT_OF(peds) ePed
		IF CAN_PED_SEE_THIS_PED_IS_DEAD(observer, peds[ePed], bKIFOO, bSPDI, bNL)
		
			// knocked out infront of an observer, flag being seen KOd so not to generate "dead_body" event
			IF bKIFOO
			AND bNL
				peds[ePed].bObservedBeingKOd 	= TRUE
			ENDIF
			
			IF NOT bHasSeenABody
				bHasSeenABody 					= TRUE
				
				bKilledInfrontOfObserver		= bKIFOO
				bSawPlayerDoIt 					= bSPDI
				bNonLethal						= bNL
			ELSE
				IF bKIFOO
					bKilledInfrontOfObserver	= TRUE
				ENDIF
				IF bSPDI
					bSawPlayerDoIt				= TRUE
				ENDIF
				IF bNL
					bNonLethal					= TRUE
				ENDIF
			ENDIF
			
			// Not seen killed by this observer, but has been by another before, then force it to be non leathal
			// as not to generate "dead_body" found event
			IF peds[ePed].bObservedBeingKOd
				bSawPlayerDoIt					= TRUE
			ENDIF
			
		ENDIF
	ENDREPEAT
	
	IF bHasSeenABody
		RETURN TRUE
	ELSE
		bKilledInfrontOfObserver				= FALSE
		bSawPlayerDoIt							= FALSE
		bNonLethal								= FALSE
		RETURN FALSE
	ENDIF
ENDFUNC

FUNC BOOL CAN_PED_SEE_THIS_PED_BEING_ATTACKED(PED_STRUCT sObserver, PED_STRUCT sVictim, BOOL bIncludeVehicle)

	CONST_FLOAT F_ATTACK_NOTICE_DIST 15.0

	// url:bugstar:2109487
	IF DOES_ENTITY_EXIST(sVictim.id)
	AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sVictim.id, PLAYER_PED_ID(), bIncludeVehicle)
		IF GET_DISTANCE_BETWEEN_ENTITIES(sObserver.id, sVictim.id) < F_ATTACK_NOTICE_DIST
			IF HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(sObserver.id, sVictim.id)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL CAN_PED_SEE_A_PED_BEING_ATTACKED(PED_STRUCT sObserve, BOOL bIncludeVehicle)

	MISSION_PED_FLAGS ePed
	
	REPEAT COUNT_OF(peds) ePed
		IF CAN_PED_SEE_THIS_PED_BEING_ATTACKED(sObserve, peds[ePed], bIncludeVehicle)
			RETURN TRUE
		ENDIF
	ENDREPEAT

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_AIMING(INT iWeaponFlags)

	IF IS_PED_ARMED(PLAYER_PED_ID(), iWeaponFlags)
	AND (IS_PLAYER_FREE_AIMING(PLAYER_ID())
	OR IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID()))
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_AIMING_AT_PED(PED_STRUCT sPed, INT iWeaponFlags)

	IF sPed.id != null
	AND DOES_ENTITY_EXIST(sPed.id)
	AND NOT IS_PED_INJURED(sPed.id)
		IF IS_PED_ARMED(PLAYER_PED_ID(), iWeaponFlags)
		AND (IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), sPed.id)
		OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), sPed.id))
			RETURN TRUE	
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL PREPARE_INITIAL_SCENE(BOOL bGrabFromTriggerScene)

	BOOL bAssetMissingEntities 	= FALSE
	BOOL bReady 				= TRUE
	
	IF bGrabFromTriggerScene
			
	//>>>>>>>> VANS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	
		// Van inside left of the front entrance
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
			vehs[mvf_bugstar_van_1].id = g_sTriggerSceneAssets.veh[0]
			SET_ENTITY_AS_MISSION_ENTITY(vehs[mvf_bugstar_van_1].id, TRUE, TRUE)
		ELSE
			PRINT_DEBUG("[ERROR] g_sTriggerSceneAssets.veh[0] not found", 1)
			bAssetMissingEntities = TRUE
		ENDIF
		
		// Van inside right of the entrance
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[1])
			vehs[mvf_bugstar_van_2].id = g_sTriggerSceneAssets.veh[1]
			SET_ENTITY_AS_MISSION_ENTITY(vehs[mvf_bugstar_van_2].id, TRUE, TRUE)
		ELSE
			PRINT_DEBUG("[ERROR] g_sTriggerSceneAssets.veh[1] not found", 0)
			bAssetMissingEntities = TRUE
		ENDIF
		
		//Van inside middle with ped around the back
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[2])
			vehs[mvf_bugstar_van_3].id = g_sTriggerSceneAssets.veh[2]
			SET_ENTITY_AS_MISSION_ENTITY(vehs[mvf_bugstar_van_3].id, TRUE, TRUE)
		ELSE
			PRINT_DEBUG("[ERROR] g_sTriggerSceneAssets.veh[2] not found", 0)
			bAssetMissingEntities = TRUE
		ENDIF
		
	
	//>>>>> BUGSTAR STAFF <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
	
		// Ped standing the back of the van
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
			peds[mpf_bugstar_foreman_rear].id = g_sTriggerSceneAssets.ped[0]
			IF NOT IS_PED_INJURED( peds[mpf_bugstar_foreman_rear].id )
				SET_ENTITY_LOAD_COLLISION_FLAG(peds[mpf_bugstar_foreman_rear].id, FALSE)
			ENDIF
			SET_ENTITY_AS_MISSION_ENTITY(peds[mpf_bugstar_foreman_rear].id, TRUE, TRUE)
			
		ELSE
			PRINT_DEBUG("[ERROR] g_sTriggerSceneAssets.ped[0] not found", 0)
			bAssetMissingEntities = TRUE
		ENDIF
		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.object[0])
			peds[mpf_bugstar_foreman_rear].objProp = g_sTriggerSceneAssets.object[0]
			SET_ENTITY_AS_MISSION_ENTITY(peds[mpf_bugstar_foreman_rear].objProp, TRUE, TRUE)
		ELSE
			PRINT_DEBUG("[ERROR] g_sTriggerSceneAssets.object[0] not found", 0)
			bAssetMissingEntities = TRUE
		ENDIF

		// Ped standing at the front entrance acting as site manager
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])
			peds[mpf_bugstar_foreman_front].id = g_sTriggerSceneAssets.ped[2]
			IF NOT IS_PED_INJURED( peds[mpf_bugstar_foreman_front].id )
				SET_ENTITY_LOAD_COLLISION_FLAG(peds[mpf_bugstar_foreman_front].id, FALSE)
			ENDIF
			SET_ENTITY_AS_MISSION_ENTITY(peds[mpf_bugstar_foreman_front].id, TRUE, TRUE)
		ELSE
			PRINT_DEBUG("g_sTriggerSceneAssets.ped[2] not found", 0)
			bAssetMissingEntities = TRUE
		ENDIF
		
		// 1st ped at the front to the right of the entrance
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])
			peds[mpf_bugstar_front_1].id = g_sTriggerSceneAssets.ped[3]
			IF NOT IS_PED_INJURED( peds[mpf_bugstar_front_2].id )
				SET_ENTITY_LOAD_COLLISION_FLAG(peds[mpf_bugstar_front_1].id, FALSE)
			ENDIF
			SET_ENTITY_AS_MISSION_ENTITY(peds[mpf_bugstar_front_1].id, TRUE, TRUE)
		ELSE
			PRINT_DEBUG("g_sTriggerSceneAssets.ped[3] not found", 0)
			bAssetMissingEntities = TRUE
		ENDIF
	
		// 2nd ped at the front to the right of the entrance
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[4])
			peds[mpf_bugstar_front_2].id = g_sTriggerSceneAssets.ped[4]
			IF NOT IS_PED_INJURED( peds[mpf_bugstar_front_2].id )
				SET_ENTITY_LOAD_COLLISION_FLAG(peds[mpf_bugstar_front_2].id, FALSE)
			ENDIF
			SET_ENTITY_AS_MISSION_ENTITY(peds[mpf_bugstar_front_2].id, TRUE, TRUE)
		ELSE
			PRINT_DEBUG("g_sTriggerSceneAssets.ped[4] not found", 0)
			bAssetMissingEntities = TRUE
		ENDIF
		
	//>>>>> SECURITY GUARD & VEHICLE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[5])
			pedSecurity.id = g_sTriggerSceneAssets.ped[5]
			IF NOT IS_PED_INJURED( pedSecurity.id )
				SET_ENTITY_LOAD_COLLISION_FLAG(pedSecurity.id, FALSE)
			ENDIF
			SET_ENTITY_AS_MISSION_ENTITY(pedSecurity.id, TRUE, TRUE)
		ELSE
			PRINT_DEBUG("g_sTriggerSceneAssets.ped[5] not found", 0)
			bAssetMissingEntities = TRUE
		ENDIF
		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[3])
			vehs[mvf_security].id = g_sTriggerSceneAssets.veh[3]
			SET_ENTITY_AS_MISSION_ENTITY(vehs[mvf_security].id, TRUE, TRUE)
		ELSE
			PRINT_DEBUG("g_sTriggerSceneAssets.veh[3] not found", 0)
			bAssetMissingEntities = TRUE
		ENDIF
		
	ELSE
		
		// Manually create the entities 
		IF NOT DOES_ENTITY_EXIST(vehs[mvf_bugstar_van_1].id)
			vehs[mvf_bugstar_van_1].id = CREATE_VEHICLE(BURRITO2, <<148.7243, -3104.6189, 4.8962>>, 179.6158)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(vehs[mvf_bugstar_van_2].id)
			vehs[mvf_bugstar_van_2].id = CREATE_VEHICLE(BURRITO2, <<145.9856, -3080.0000, 4.8962>>, 269.0827)
		ENDIF
		IF NOT DOES_ENTITY_EXIST(vehs[mvf_bugstar_van_3].id)
			vehs[mvf_bugstar_van_3].id = CREATE_VEHICLE(BURRITO2, <<129.9696, -3089.3313, 4.8796>>, 269.9255)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(peds[mpf_bugstar_foreman_rear].id)
			peds[mpf_bugstar_foreman_rear].id 		= CREATE_PED(PEDTYPE_MISSION, S_M_Y_PESTCONT_01, <<126.2174, -3089.3833, 4.9199>>, 275.4068)
			SET_ENTITY_HEALTH(peds[mpf_bugstar_foreman_rear].id, 150)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_foreman_rear].id, PED_COMP_SPECIAL, 1, 0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_foreman_rear].id, PED_COMP_HEAD, 0, 2)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_foreman_rear].id, PED_COMP_TORSO, 0, 1)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_foreman_rear].id, PED_COMP_LEG, 0, 1)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_foreman_rear].id, PED_COMP_SPECIAL, 0, 0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_foreman_rear].id, PED_COMP_DECL, 1, 0)
			SET_PED_PROP_INDEX(peds[mpf_bugstar_foreman_rear].id, ANCHOR_HEAD, 1)
			SET_PED_PROP_INDEX(peds[mpf_bugstar_foreman_rear].id, ANCHOR_EYES, 0)
			SET_PED_LOD_MULTIPLIER(peds[mpf_bugstar_foreman_rear].id, 2.0)
		ENDIF

		IF NOT DOES_ENTITY_EXIST(peds[mpf_bugstar_foreman_rear].objProp)
			peds[mpf_bugstar_foreman_rear].objProp 	= CREATE_OBJECT(P_AMB_CLIPBOARD_01, GET_PED_BONE_COORDS(peds[mpf_bugstar_foreman_rear].id, BONETAG_PH_L_HAND, <<0,0,0>>))
			ATTACH_ENTITY_TO_ENTITY(peds[mpf_bugstar_foreman_rear].objProp, peds[mpf_bugstar_foreman_rear].id, GET_PED_BONE_INDEX(peds[mpf_bugstar_foreman_rear].id, BONETAG_PH_L_HAND), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE, TRUE)
		ENDIF
				
		IF NOT DOES_ENTITY_EXIST(peds[mpf_bugstar_foreman_front].id)
			peds[mpf_bugstar_foreman_front].id 			= CREATE_PED(PEDTYPE_MISSION, S_M_Y_PESTCONT_01, <<155.74, -3098.89, 5.93>>)		
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_foreman_front].id, PED_COMP_HEAD, 0, 0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_foreman_front].id, PED_COMP_TORSO, 0, 0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_foreman_front].id, PED_COMP_LEG, 0, 0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_foreman_front].id, PED_COMP_SPECIAL, 1, 0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_foreman_front].id, PED_COMP_DECL, 0, 0)
			SET_PED_PROP_INDEX(peds[mpf_bugstar_foreman_front].id, ANCHOR_HEAD, 0)
			SET_PED_LOD_MULTIPLIER(peds[mpf_bugstar_foreman_front].id, 2.0)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(peds[mpf_bugstar_front_1].id)
			peds[mpf_bugstar_front_1].id 			= CREATE_PED(PEDTYPE_MISSION, S_M_Y_PESTCONT_01, <<159.80, -3085.96, 6.00>>)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_front_1].id, PED_COMP_HEAD, 0, 1)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_front_1].id, PED_COMP_TORSO, 0, 2)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_front_1].id, PED_COMP_LEG, 0, 2)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_front_1].id, PED_COMP_SPECIAL, 1, 0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_front_1].id, PED_COMP_DECL, 1, 0)
			SET_PED_PROP_INDEX(peds[mpf_bugstar_front_1].id, ANCHOR_EYES, 0)
			SET_PED_LOD_MULTIPLIER(peds[mpf_bugstar_front_1].id, 2.0)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(peds[mpf_bugstar_front_2].id)
			peds[mpf_bugstar_front_2].id 			= CREATE_PED(PEDTYPE_MISSION, S_M_Y_PESTCONT_01, <<159.06, -3085.00, 6.01>>)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_front_2].id, PED_COMP_HEAD, 1, 1)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_front_2].id, PED_COMP_TORSO, 0, 3)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_front_2].id, PED_COMP_LEG, 0, 3)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_front_2].id, PED_COMP_SPECIAL, 1, 0)
			SET_PED_COMPONENT_VARIATION(peds[mpf_bugstar_front_2].id, PED_COMP_DECL, 0, 0)
			SET_PED_PROP_INDEX(peds[mpf_bugstar_front_2].id, ANCHOR_HEAD, 0)
			SET_PED_LOD_MULTIPLIER(peds[mpf_bugstar_front_2].id, 2.0)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(pedSecurity.id)
		AND NOT DOES_ENTITY_EXIST(vehs[mvf_security].id)
			IF GET_CLOCK_HOURS() >= 5 AND GET_CLOCK_HOURS() < 21	
				vehs[mvf_security].id = CREATE_VEHICLE(DILETTANTE2, <<144.84, -2982.75, 5.32>>, 266.5972)
				pedSecurity.id = CREATE_PED_INSIDE_VEHICLE(vehs[mvf_security].id, PEDTYPE_MISSION, S_M_M_SECURITY_01, VS_DRIVER)
			ELSE
				vehs[mvf_security].id = CREATE_VEHICLE(DILETTANTE2, <<169.3554, -3110.0254, 4.8228>>, 88.4411)
				pedSecurity.id = CREATE_PED(PEDTYPE_MISSION, S_M_M_SECURITY_01, <<161.7414, -3117.0742, 4.9643>>, 333.7041) //<<162.69, -3115.67, 4.95>>, 3.5948)
			ENDIF
		ENDIF

	ENDIF

	// Van inside left of the front entrance
	IF DOES_ENTITY_EXIST(vehs[mvf_bugstar_van_1].id)
		SET_VEHICLE_AS_RESTRICTED(vehs[mvf_bugstar_van_1].id, 0)
		SET_VEHICLE_DOORS_LOCKED(vehs[mvf_bugstar_van_1].id, VEHICLELOCK_LOCKED_BUT_CAN_BE_DAMAGED)
		SET_VEHICLE_ALARM(vehs[mvf_bugstar_van_1].id, TRUE)
		SET_VEHICLE_HAS_STRONG_AXLES(vehs[mvf_bugstar_van_1].id, TRUE)
		vehs[mvf_bugstar_van_1].bAutoCleanup = FALSE
//		SET_VEHICLE_DISABLE_TOWING(vehs[mvf_bugstar_van_1].id, TRUE)
	ENDIF

	// Van inside right of the front entrance
	IF DOES_ENTITY_EXIST(vehs[mvf_bugstar_van_2].id)
		SET_VEHICLE_AS_RESTRICTED(vehs[mvf_bugstar_van_2].id, 1)
		SET_VEHICLE_ENGINE_ON(vehs[mvf_bugstar_van_2].id, TRUE, TRUE)
		SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehs[mvf_bugstar_van_2].id, SC_DOOR_BONNET, FALSE)
		SET_VEHICLE_DOOR_OPEN(vehs[mvf_bugstar_van_2].id, SC_DOOR_FRONT_LEFT, TRUE, FALSE)
		SET_VEHICLE_DOOR_OPEN(vehs[mvf_bugstar_van_2].id, SC_DOOR_BONNET, FALSE, TRUE)
		SET_VEHICLE_HAS_STRONG_AXLES(vehs[mvf_bugstar_van_2].id, TRUE)
		vehs[mvf_bugstar_van_2].bAutoCleanup = FALSE
//		SET_VEHICLE_DISABLE_TOWING(vehs[mvf_bugstar_van_2].id, TRUE)
	ENDIF
	
	// Van in the middle with ped around the back
	IF DOES_ENTITY_EXIST(vehs[mvf_bugstar_van_3].id)
		SET_VEHICLE_AS_RESTRICTED(vehs[mvf_bugstar_van_3].id, 2)
		SET_VEHICLE_DOOR_OPEN(vehs[mvf_bugstar_van_3].id, SC_DOOR_REAR_LEFT, TRUE, FALSE)
		SET_VEHICLE_DOOR_OPEN(vehs[mvf_bugstar_van_3].id, SC_DOOR_REAR_RIGHT, TRUE, FALSE)
		SET_VEHICLE_HAS_STRONG_AXLES(vehs[mvf_bugstar_van_3].id, TRUE)
		vehs[mvf_bugstar_van_3].bAutoCleanup = FALSE
//		SET_VEHICLE_DISABLE_TOWING(vehs[mvf_bugstar_van_3].id, TRUE)
	ENDIF	
	
	// Ped at the back of the van
	IF DOES_ENTITY_EXIST(peds[mpf_bugstar_foreman_rear].id)
		
		SET_PED_NAME_DEBUG(peds[mpf_bugstar_foreman_rear].id, peds[mpf_bugstar_foreman_rear].strDebugName)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_bugstar_foreman_rear].id, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(peds[mpf_bugstar_foreman_rear].id, rel_bugstar)
		STOP_PED_SPEAKING(peds[mpf_bugstar_foreman_rear].id, TRUE)
		SET_PED_VISUAL_FIELD_PROPERTIES(peds[mpf_bugstar_foreman_rear].id, 20, 5, 90, -90, 90)
		SET_PED_HEARING_RANGE(peds[mpf_bugstar_foreman_rear].id, 50.0)
		
		peds[mpf_bugstar_foreman_rear].strDebugName 				= "foreman2(rear)"
		peds[mpf_bugstar_foreman_rear].fEventNoticeRange 				= 20.0
		peds[mpf_bugstar_foreman_rear].bReactsToEntrance			= TRUE
		peds[mpf_bugstar_foreman_rear].bReactsToIntrusion 			= TRUE
		peds[mpf_bugstar_foreman_rear].propName 					= P_AMB_CLIPBOARD_01
		peds[mpf_bugstar_foreman_rear].pedPropHand					= BONETAG_PH_L_HAND
		peds[mpf_bugstar_foreman_rear].bDropWhenNotInWorkingState 	= FALSE
	ENDIF
	
	// foreman
	IF DOES_ENTITY_EXIST(peds[mpf_bugstar_foreman_front].id)
		
		SET_PED_NAME_DEBUG(peds[mpf_bugstar_foreman_front].id, peds[mpf_bugstar_foreman_front].strDebugName)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_bugstar_foreman_front].id, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(peds[mpf_bugstar_foreman_front].id, rel_bugstar)
		STOP_PED_SPEAKING(peds[mpf_bugstar_foreman_front].id, TRUE)
		SET_PED_VISUAL_FIELD_PROPERTIES(peds[mpf_bugstar_foreman_front].id, 17.5, 10.0, 120, -90, 90)
		SET_PED_HEARING_RANGE(peds[mpf_bugstar_foreman_front].id, 50.0)
		
		peds[mpf_bugstar_foreman_front].strDebugName 				= "foreman1(front)" 
		peds[mpf_bugstar_foreman_front].bReactsToEntrance			= TRUE
		peds[mpf_bugstar_foreman_front].bReactsToIntrusion 			= TRUE
		peds[mpf_bugstar_foreman_front].fEventNoticeRange 				= 17.5
		
	ENDIF

	// 1st ped at the front to the right of the entrance
	IF DOES_ENTITY_EXIST(peds[mpf_bugstar_front_1].id)
		
		SET_PED_NAME_DEBUG(peds[mpf_bugstar_front_1].id, peds[mpf_bugstar_front_1].strDebugName)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_bugstar_front_1].id, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(peds[mpf_bugstar_front_1].id, rel_bugstar)
		SET_PED_VISUAL_FIELD_PROPERTIES(peds[mpf_bugstar_front_1].id, 20, 5, 90, -90, 90)
		STOP_PED_SPEAKING(peds[mpf_bugstar_front_1].id, TRUE)
		SET_PED_HEARING_RANGE(peds[mpf_bugstar_front_1].id, 50.0)
		
		peds[mpf_bugstar_front_1].strDebugName 					= "front 1"
		peds[mpf_bugstar_front_1].bReactsToEntrance				= FALSE
		peds[mpf_bugstar_front_1].bReactsToIntrusion 			= FALSE
		peds[mpf_bugstar_front_1].fEventNoticeRange 				= 17.5
	ENDIF
	
	// 2nd ped at the front to the right of the entrance
	IF DOES_ENTITY_EXIST(peds[mpf_bugstar_front_2].id)
		
		SET_PED_NAME_DEBUG(peds[mpf_bugstar_front_2].id, peds[mpf_bugstar_front_2].strDebugName)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(peds[mpf_bugstar_front_2].id, TRUE)
		SET_PED_RELATIONSHIP_GROUP_HASH(peds[mpf_bugstar_front_2].id, rel_bugstar)
		SET_PED_HEARING_RANGE(peds[mpf_bugstar_front_2].id, 50.0)
		SET_PED_VISUAL_FIELD_PROPERTIES(peds[mpf_bugstar_front_2].id, 20, 5, 90, -90, 90)
		STOP_PED_SPEAKING(peds[mpf_bugstar_front_2].id, TRUE)
		
		peds[mpf_bugstar_front_2].strDebugName 					= "front 2"
		peds[mpf_bugstar_front_2].bReactsToEntrance				= FALSE
		peds[mpf_bugstar_front_2].bReactsToIntrusion 			= FALSE
		peds[mpf_bugstar_front_2].fEventNoticeRange 			= 17.5
		
	ENDIF
	
	// Security ped and his vehicle
	IF DOES_ENTITY_EXIST(pedSecurity.id)
	AND DOES_ENTITY_EXIST(vehs[mvf_security].id)
		
		SET_PED_NAME_DEBUG(pedSecurity.id, pedSecurity.strDebugName)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSecurity.id, TRUE)
//		SET_PED_RELATIONSHIP_GROUP_HASH(pedSecurity.id, RELGROUPHASH_SECURITY_GUARD)
		SET_PED_RELATIONSHIP_GROUP_HASH(pedSecurity.id, rel_bugstar)
//		SET_PED_AS_COP(pedSecurity.id)
		SET_PED_TARGET_LOSS_RESPONSE(pedSecurity.id, TLR_SEARCH_FOR_TARGET)
		SET_PED_COMBAT_ATTRIBUTES(pedSecurity.id, CA_DO_DRIVEBYS, FALSE)
		GIVE_WEAPON_TO_PED(pedSecurity.id, WEAPONTYPE_NIGHTSTICK, INFINITE_AMMO, FALSE, TRUE)
		SET_PED_ACCURACY(pedSecurity.id, 5)
		SET_PED_VISUAL_FIELD_PROPERTIES(pedSecurity.id, 40, 5, 90, -90, 90)
//		STOP_PED_SPEAKING(pedSecurity.id, TRUE)
		
		pedSecurity.strDebugName = "sec"
		pedSecurity.bReactsToEntrance 				= TRUE
		pedSecurity.bReactsToIntrusion 				= TRUE
		pedSecurity.bDisplayAIBlip					= FALSE
		
		IF g_sTriggerSceneAssets.flag = TRUE
		
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedSecurity.id, SCRIPT_TASK_PERFORM_SEQUENCE)		
				IF DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<203.68, -3132.46, 4.79>>, "WORLD_HUMAN_SMOKING", 1.0, TRUE)

					bUsingNightScenario = FALSE

					SEQUENCE_INDEX seq
					
					OPEN_SEQUENCE_TASK(seq)
						TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(null, vehs[mvf_security].id, str_wpSecurityArrive, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS)
						TASK_LEAVE_VEHICLE(null, vehs[mvf_security].id)
						TASK_USE_NEAREST_SCENARIO_TO_COORD(null, <<203.68, -3132.46, 4.79>>, 1.0)
					CLOSE_SEQUENCE_TASK(seq)
					TASK_PERFORM_SEQUENCE(pedSecurity.id, seq)
					CLEAR_SEQUENCE_TASK(seq)
				ELSE
					CDEBUG1LN(DEBUG_MIKE, "SCENARIO: WORLD_HUMAN_SMOKING not ready")
					bReady = FALSE
				ENDIF
			ENDIF
		
		ELSE
			
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(pedSecurity.id, SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS)
				IF DOES_SCENARIO_OF_TYPE_EXIST_IN_AREA(<<162.69, -3115.67, 4.95>>, "WORLD_HUMAN_SECURITY_SHINE_TORCH", 1.0, TRUE)
					bUsingNightScenario = TRUE
					TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD_WARP(pedSecurity.id, <<162.69, -3115.67, 4.95>>, 1.0)

				ELSE
					CDEBUG1LN(DEBUG_MIKE, "SCENARIO: WORLD_HUMAN_SECURITY_SHINE_TORCH not ready")
					bReady = FALSE
				ENDIF
			ENDIF
		
		ENDIF
		
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehs[mvf_bugstar_van_1].id)
	AND DOES_ENTITY_EXIST(vehs[mvf_bugstar_van_2].id)
	AND DOES_ENTITY_EXIST(vehs[mvf_bugstar_van_3].id)
	AND DOES_ENTITY_EXIST(vehs[mvf_security].id)
	AND DOES_ENTITY_EXIST(peds[mpf_bugstar_foreman_rear].id)
	AND DOES_ENTITY_EXIST(peds[mpf_bugstar_foreman_front].id)
	AND DOES_ENTITY_EXIST(peds[mpf_bugstar_front_1].id)
	AND DOES_ENTITY_EXIST(peds[mpf_bugstar_front_2].id)
	AND DOES_ENTITY_EXIST(pedSecurity.id)
	AND bReady
	
		// Grabbed everything
		//MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_JEWELRY_PREP_1A)
		RETURN TRUE
	
	ELIF bAssetMissingEntities
		SCRIPT_ASSERT("Unable to grab all trigger scene entities")
		RETURN FALSE
	ENDIF

	RETURN FALSE
	
ENDFUNC

/// PURPOSE:
///    Checks if the player has gotten into one of the bugstar vans.
/// RETURNS:
///    Returns TRUE if the player has gotten into one of the bugstar vans
FUNC BOOL IS_PLAYER_IN_VAN()

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), FALSE)
		MISSION_VEHICLE_FLAGS eVan
		FOR eVan = mvf_bugstar_van_1 TO mvf_bugstar_van_3

			IF DOES_ENTITY_EXIST(vehs[eVan].id)
			AND IS_VEHICLE_DRIVEABLE(vehs[eVan].id)

				// otherwise check if they are in that van
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehs[eVan].id, FALSE)
					
					RETURN TRUE
					
				ENDIF
			ENDIF
			
		ENDFOR
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_ENTERING_VAN()

	IF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
		MISSION_VEHICLE_FLAGS eVan
		FOR eVan = mvf_bugstar_van_1 TO mvf_bugstar_van_3
			IF DOES_ENTITY_EXIST(vehs[eVan].id)
			AND IS_VEHICLE_DRIVEABLE(vehs[eVan].id)
			AND GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) = vehs[eVan].id
				RETURN TRUE
			ENDIF
		ENDFOR
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_PLAYER_EXITING_VAN()

	//IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE) 
	AND NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
	AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
	//AND GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_EXIT_VEHICLE)
	
		MISSION_VEHICLE_FLAGS eVan
		FOR eVan = mvf_bugstar_van_1 TO mvf_bugstar_van_3
			IF DOES_ENTITY_EXIST(vehs[eVan].id)
			AND IS_VEHICLE_DRIVEABLE(vehs[eVan].id)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehs[eVan].id, TRUE)
					RETURN TRUE
				ENDIF
			ENDIF
			
		ENDFOR
	ENDIF
	
	RETURN FALSE
ENDFUNC



/// PURPOSE:
///    Determines if the player is in a tow truck
/// RETURNS:
///   Returns TRUE if so
FUNC BOOL IS_PLAYER_IN_A_TOW_TRUCK()
	
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX vehTemp
		vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
	
		IF IS_VEHICLE_DRIVEABLE(vehTemp)
		AND (GET_ENTITY_MODEL(vehTemp) = TOWTRUCK OR GET_ENTITY_MODEL(vehTemp) = TOWTRUCK2)
		
			RETURN TRUE
		
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Determines if one of the vans is attached to a tow truck.
/// RETURNS:
///    Returns TRUE if so.
FUNC BOOL IS_A_VAN_ATTACHED_TO_A_TOW_TRUCK()

	IF DOES_ENTITY_EXIST(veh_TowTruckAttached)
	AND IS_VEHICLE_DRIVEABLE(veh_TowTruckAttached)
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Obtains the MISSION_VEHICLE_FLAG of the van being towed
/// RETURNS:
///    Returns the MISSION_VEHICLE_FLAG of the van being towed, mvf_invalid will return if no van is being towed.
FUNC MISSION_VEHICLE_FLAGS GET_VAN_PLAYER_IS_TOWING()

	IF IS_A_VAN_ATTACHED_TO_A_TOW_TRUCK()
	
		MISSION_VEHICLE_FLAGS eVeh
		FOR eVeh = mvf_bugstar_van_1 TO mvf_bugstar_van_3
			IF DOES_ENTITY_EXIST(vehs[eVeh].id)
			AND IS_VEHICLE_DRIVEABLE(vehs[eVeh].id)
				IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(veh_TowTruckAttached, vehs[eVeh].id)
					RETURN eVeh
				ENDIF
			ENDIF
		ENDFOR
	
	ENDIF
	
	RETURN mvf_invalid
ENDFUNC


/// PURPOSE:
///   Checks to see if the player has just damaged one of the vans
/// RETURNS:
///    Returns TRUE if the player has damaged a van
FUNC BOOL HAS_PLAYER_DAMAGED_A_VAN()
	MISSION_VEHICLE_FLAGS eVeh
	FOR eVeh = mvf_bugstar_van_1 TO mvf_bugstar_van_3
		
		IF DOES_ENTITY_EXIST(vehs[eVeh].id)
		AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehs[eVeh].id, PLAYER_PED_ID(), TRUE )
			RETURN TRUE
		ENDIF
		
	ENDFOR

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Resets the damage tracking on all the vans
PROC CLEAR_VAN_DAMAGE_DETECTION()
	MISSION_VEHICLE_FLAGS eVeh
	FOR eVeh = mvf_bugstar_van_1 TO mvf_bugstar_van_3
	
		IF IS_ENTITY_ALIVE(vehs[eVeh].id)
			CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehs[eVeh].id)
		ENDIF
	
	ENDFOR
	
ENDPROC

FUNC BOOL IS_VAN_WINDOW_SMASHED(MISSION_VEHICLE_FLAGS eVeh)

	IF eVeh != mvf_invalid
	AND DOES_ENTITY_EXIST(vehs[eVeh].id)
	AND IS_VEHICLE_DRIVEABLE(vehs[eVeh].id)
	
		SC_WINDOW_LIST eWindow
		REPEAT COUNT_OF(SC_WINDOW_LIST) eWindow
			
			IF NOT IS_VEHICLE_WINDOW_INTACT(vehs[eVeh].id, eWindow)
				RETURN TRUE
			ENDIF
		
		ENDREPEAT
	
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Finds which vehicle the player has stolen
/// RETURNS:
///    Returns the mission vehicle flag of the vehicle that has been stolen, returns mpf_invalid if none is stolen
FUNC MISSION_VEHICLE_FLAGS GET_VAN_PLAYER_IS_IN()

	IF IS_PLAYER_IN_VAN()
	
		MISSION_VEHICLE_FLAGS eVan
		FOR eVan = mvf_bugstar_van_1 TO mvf_bugstar_van_3
			IF DOES_ENTITY_EXIST(vehs[eVan].id)
			AND IS_VEHICLE_DRIVEABLE(vehs[eVan].id)
				IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehs[eVan].id, TRUE)
					RETURN eVan
				ENDIF
			ENDIF
		ENDFOR
		
	ENDIF
	
	RETURN mvf_invalid
ENDFUNC

FUNC BOOL IS_VEHICLE_STUCK(VEHICLE_INDEX veh)

	IF IS_VEHICLE_DRIVEABLE(veh)
		IF IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_ROOF, 7000)
		OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_ON_SIDE, 7000)
		OR IS_VEHICLE_STUCK_TIMER_UP(veh, VEH_STUCK_HUNG_UP, 7000)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_LAST_VEHICLE_A_VAN()

	MISSION_VEHICLE_FLAGS eVan
	FOR eVan = mvf_bugstar_van_1 TO mvf_bugstar_van_3
		IF DOES_ENTITY_EXIST(vehs[eVan].id)
		AND IS_VEHICLE_DRIVEABLE(vehs[eVan].id)
			IF GET_PLAYERS_LAST_VEHICLE() = vehs[eVan].id
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_VEHICLE_IN_USEABLE_RANGE(VEHICLE_INDEX veh)

	// If the van is in the warehouse give it a range boost
	IF GET_INTERIOR_FROM_ENTITY(veh) = interior_warehouse
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_WarehouseCoord) < VAN_USABLE_RANGE_WAREHOUSE_FROM_PLAYER
			RETURN TRUE
		ENDIF
	ELSE
		IF GET_DISTANCE_BETWEEN_ENTITIES(veh, PLAYER_PED_ID()) < VAN_USABLE_RANGE_VAN_FROM_PLAYER
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Gets the number of bugstar vans available.
/// RETURNS:
///    returns an integer count of the remaining number of vans.
FUNC INT GET_NUM_OF_VANS_LEFT(BOOL bWithinUsableRange)
	INT iResult

	MISSION_VEHICLE_FLAGS eVeh
	FOR eVeh = mvf_bugstar_van_1 TO mvf_bugstar_van_3
		IF DOES_ENTITY_EXIST(vehs[eVeh].id)
		AND IS_VEHICLE_DRIVEABLE(vehs[eVeh].id)
		AND NOT IS_VEHICLE_STUCK(vehs[eVeh].id)
		AND (NOT bWithinUsableRange OR IS_VEHICLE_IN_USEABLE_RANGE(vehs[eVeh].id))
			iResult++
		ENDIF
	ENDFOR
	
	RETURN iResult
ENDFUNC


/// PURPOSE:
///    Checks if one of the bugstars van's alarams are going off
/// RETURNS:
///    Returns TRUE if one of the alarms is going off
FUNC BOOL GET_IS_A_VAN_ALARM_GOING_OFF()
	MISSION_VEHICLE_FLAGS eVeh
	FOR eVeh = mvf_bugstar_van_1 TO mvf_bugstar_van_3
		IF DOES_ENTITY_EXIST(vehs[eVeh].id)
		AND IS_VEHICLE_DRIVEABLE(vehs[eVeh].id)
			IF IS_VEHICLE_ALARM_ACTIVATED(vehs[eVeh].id)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	
	RETURN FALSE
ENDFUNC

FUNC MISSION_VEHICLE_FLAGS GET_VAN_ALARM_IS_GOING_OFF_ON()

	MISSION_VEHICLE_FLAGS eVeh
	
	IF GET_IS_A_VAN_ALARM_GOING_OFF()
	
		FOR eVeh = mvf_bugstar_van_1 TO mvf_bugstar_van_3
			IF DOES_ENTITY_EXIST(vehs[eVeh].id)
			AND IS_VEHICLE_DRIVEABLE(vehs[eVeh].id)
				IF IS_VEHICLE_ALARM_ACTIVATED(vehs[eVeh].id)
					RETURN eVeh
				ENDIF
			ENDIF
		ENDFOR
	
	ENDIF

	RETURN mvf_invalid
ENDFUNC

FUNC BOOL IS_PED_A_FOREMAN(MISSION_PED_FLAGS ePed)
	IF ePed = mpf_bugstar_foreman_front
	OR ePed = mpf_bugstar_foreman_rear
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL PLAY_CONVERSATION(MISSION_PED_FLAGS ePed, STRING strRoot, PED_AI_STATES eConvoPriority, BOOL bInterrupt = FALSE, BOOL bDontFinish = FALSE, BOOL bCanThisBeInterrupted = TRUE, INT iDelay = 0, BOOL bStoreWhenInterruptedForFutureResumption = FALSE, STRING strLine = null, BOOL bConvoKillAtDistance = FALSE )

	FLOAT fTempDist = GET_DISTANCE_BETWEEN_ENTITIES(peds[ePed].id, PLAYER_PED_ID())

	IF eConvoPriority > eQueuedConvoPriority
	OR eConvoPriority = PEDAI_HANDSUP
	OR (eConvoPriority = eQueuedConvoPriority 
		AND (fTempDist < fQueuedConvoDist OR (IS_PED_A_FOREMAN(ePed) AND NOT IS_PED_A_FOREMAN(eQueuedConvoSpeaker)))
		AND eConvoPriority != PEDAI_FLEEING)
	
		BOOL bQueueConvo
	
		// Use priority to decide at what distance a conversation should be culled.
		SWITCH eConvoPriority
			CASE PEDAI_WORKING
				IF fTempDist < 20.0
					bQueueConvo = TRUE
				ENDIF
			BREAK
			
			DEFAULT		
				IF fTempDist < 40.0
					bQueueConvo = TRUE
				ENDIF		
			BREAK
		ENDSWITCH
		
		IF bQueueConvo
			eQueuedConvoSpeaker 					= ePed
			eQueuedConvoPriority					= eConvoPriority
			strQueuedConvoRoot						= strRoot
			strQueuedConvoLine						= strLine
			fQueuedConvoDist 						= fTempDist
			bQueuedConvoInterrupt					= bInterrupt
			bQueuedConvoDoNotFinish					= bDontFinish
			iQueuedConvoDelay						= iDelay
			bQueuedConvoCanBeInterrupted			= bCanThisBeInterrupted
			bQueuedConvoStoreForFutureResumption 	= bStoreWhenInterruptedForFutureResumption
			bQueuedConvoKillAtDistance				= bConvoKillAtDistance
			
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC TEXT_LABEL_23 MODIFY_CONVO_ROOT_FOR_FOREMAN(MISSION_PED_FLAGS ePed, STRING rootLabel)
	TEXT_LABEL_23 strResult = rootLabel
	IF ePed = mpf_bugstar_foreman_rear
		strResult += "b"
	ENDIF
	RETURN strResult
ENDFUNC

FUNC BOOL HAS_PED_AI_JUST_ENTERED_STATE(PED_STRUCT sPed)
	IF sPed.bAIStateChangedLastFrame
	OR sPed.bAIEventChangedLastFrame
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL HAS_PED_AI_JUST_CHANGED_STATE(PED_STRUCT sPed)
	IF sPed.eAI != sPed.eAIPrevFrame
	OR NOT ARE_STRINGS_EQUAL(sPed.strAIEvent, sPed.strAIEventPrevFrame)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

FUNC BOOL Private_SET_PED_AI_STATE(PED_STRUCT &sPed, PED_AI_STATES newState, STRING strEventName, VECTOR vCoords, ENTITY_INDEX entity = NULL, BOOL bCheckAgainstCurrentState = TRUE, BOOL bDirectEvent = TRUE, BOOL bAlarmEvent = FALSE)

	IF IS_ENTITY_ALIVE(sPed.id)

		IF (NOT bCheckAgainstCurrentState OR newState >= sPed.eAI)
		
			sPed.iTimeOfStateLastSet	= GET_GAME_TIMER()
			IF newState != sPed.eAI
			OR NOT ARE_STRINGS_EQUAL(sPed.strAIEvent,strEventName)
			
				sPed.eAIPrev 				= sPed.eAI
				sPed.eAI 					= newState
				
				sPed.strAIEventPrev			= sPed.strAIEvent
				sPed.strAIEvent 			= strEventName
				
				IF bAlarmEvent
					sPed.bAlarmEvent		= TRUE
					sPed.bDirectEvent 		= FALSE
				ELSE
					sPed.bAlarmEvent		= FALSE
					sPed.bDirectEvent 		= bDirectEvent
				ENDIF
			
				sPed.iFlagState				= 0
				sPed.iFlagTask				= 0
				sPed.iDiaFlag				= 0
				
				sPed.iTimeOfEnteringState	= GET_GAME_TIMER()
				
				// Extra data
				IF NOT IS_VECTOR_ZERO(vCoords)
					sPed.vInvestigationCoord = vCoords
				ENDIF
				IF entity != null
				AND DOES_ENTITY_EXIST(entity)
					sPed.entityInvestigating = entity
				ENDIF
				
				RETURN TRUE
			ENDIF
		
		ENDIF
	ENDIF

	RETURN FALSE

ENDFUNC

FUNC BOOL SET_PED_AI_STATE(PED_STRUCT &sPed, PED_AI_STATES newState, BOOL bCheckAgainstCurrentState, STRING strEventName, BOOL bDirectEvent = TRUE, BOOL bAlarmEvent = FALSE)
	RETURN Private_SET_PED_AI_STATE(sPed, newState, strEventName, <<0,0,0>>, NULL, bCheckAgainstCurrentState, bDirectEvent, bAlarmEvent)
ENDFUNC

FUNC BOOL SET_PED_AI_STATE_WITH_COORDS(PED_STRUCT &sPed, PED_AI_STATES newState, STRING strEventName, VECTOR vCoord, BOOL bCheckAgainstCurrentState, BOOL bDirectEvent = TRUE, BOOL bAlarmEvent = FALSE)
	RETURN Private_SET_PED_AI_STATE(sPed, newState, strEventName, vCoord, NULL, bCheckAgainstCurrentState, bDirectEvent, bAlarmEvent)
ENDFUNC

FUNC BOOL SET_PED_AI_STATE_WITH_ENTITY(PED_STRUCT &sPed, PED_AI_STATES newState, STRING strEventName, ENTITY_INDEX entity, BOOL bCheckAgainstCurrentState, BOOL bDirectEvent = TRUE, BOOL bAlarmEvent = FALSE)
	RETURN Private_SET_PED_AI_STATE(sPed, newState, strEventName, <<0,0,0>>, entity, bCheckAgainstCurrentState, bDirectEvent, bAlarmEvent)
ENDFUNC

FUNC BOOL BROADCAST_DISPATCH_SCRIPT_AI_EVENT(SCRIPT_AI_EVENT &sAIEvent, STRING strConv = NULL)

	IF IS_STRING_NULL_OR_EMPTY(sAIEvent.strEventName)
	AND sAIEvent.ePedTransmitter != mpf_invalid 
	AND (NOT DOES_ENTITY_EXIST(peds[sAIEvent.ePedTransmitter].id)
	OR IS_PED_INJURED(peds[sAIEvent.ePedTransmitter].id))
		RETURN FALSE
	ENDIF

	INT i
	WHILE i != -1
	AND NOT IS_STRING_NULL_OR_EMPTY(s_BroadcastEvent[i].strEventName)
		i++
		IF i > COUNT_OF(s_BroadcastEvent)-1
			i = -1
		ENDIF
	ENDWHILE
	
	// Gap in the queue found
	IF i != -1
		BOOL bWasDispatched
		IF sAIEvent.ePedTransmitter = mpf_invalid 
		OR (IS_STRING_NULL_OR_EMPTY(strConv) OR ARE_STRINGS_EQUAL(strConv, sAIEvent.strConvo))
		
			// Add to broadcast queue
			s_BroadcastEvent[i].ePedTransmitter				= sAIEvent.ePedTransmitter
			s_BroadcastEvent[i].strEventName 				= sAIEvent.strEventName
			IF sAIEvent.ePedTransmitter != mpf_invalid
				s_BroadcastEvent[i].vEventCoord				= GET_ENTITY_COORDS(peds[sAIEvent.ePedTransmitter].id)
			ELSE
				s_BroadcastEvent[i].vEventCoord				= sAIEvent.vEventCoord
			ENDIF
			s_BroadcastEvent[i].fEventRadius				= sAIEvent.fEventRadius
			s_BroadcastEvent[i].iEventLife					= sAIEvent.iEventLife
			s_BroadcastEvent[i].iEventDelay					= sAIEvent.iEventDelay
			s_BroadcastEvent[i].iTimeOfEvent				= GET_GAME_TIMER()
			s_BroadcastEvent[i].bRaiseAlarm					= sAIEvent.bRaiseAlarm
			s_BroadcastEvent[i].bCalledSecurity				= sAIEvent.bCalledSecurity
			
			bWasDispatched = TRUE
		ELSE
			bWasDispatched = FALSE
		ENDIF
		
		// Clear out ped's queued event
		sAIEvent.ePedTransmitter		= mpf_invalid
		sAIEvent.strEventName			= ""
		sAIEvent.vEventCoord			= <<0,0,0>>
		sAIEvent.fEventRadius			= 0.0
		sAIEvent.iEventLife				= 0
		sAIEvent.iEventDelay		 	= 0
		sAIEvent.iTimeOfEvent		 	= 0
		sAIEvent.bRaiseAlarm			= FALSE
		sAIEvent.bCalledSecurity		= FALSE
		
		RETURN bWasDispatched
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC BROADCAST_QUEUE_SCRIPT_AI_EVENT(STRING strEventName, VECTOR vCoord, FLOAT fEventRadius, INT iEventLife, INT iEventDelay = -1, BOOL bRaiseAlarm = FALSE, BOOL bCalledSecurity = FALSE)

	SCRIPT_AI_EVENT sAIEvent
		
	sAIEvent.ePedTransmitter	= mpf_invalid
	sAIEvent.strEventName		= strEventName
	sAIEvent.strConvo			= ""
	sAIEvent.iEventLife			= iEventLife
	sAIEvent.fEventRadius		= fEventRadius
	sAIEvent.iEventDelay		= iEventDelay
	sAIEvent.bRaiseAlarm		= bRaiseAlarm
	sAIEvent.bCalledSecurity	= bCalledSecurity
	sAIEvent.vEventCoord		= vCoord

	BROADCAST_DISPATCH_SCRIPT_AI_EVENT(sAIEvent)
ENDPROC

PROC BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(PED_STRUCT &sPed, STRING strEventName, STRING strConvoTiedTo, FLOAT fEventRadius, INT iEventLife, INT iEventDelay = -1, BOOL bInstantDispatch = FALSE, BOOL bRaiseAlarm = FALSE, BOOL bCalledSecurity = FALSE)

	MISSION_PED_FLAGS i, ePed
	REPEAT COUNT_OF(peds) i
		IF peds[i].id = sPed.id
			ePed = i
		ENDIF
	ENDREPEAT
	sPed.sQueuedBroadcastEvent.ePedTransmitter	= ePed
	sPed.sQueuedBroadcastEvent.strEventName		= strEventName
	sPed.sQueuedBroadcastEvent.strConvo			= strConvoTiedTo
	sPed.sQueuedBroadcastEvent.iEventLife		= iEventLife
	sPed.sQueuedBroadcastEvent.fEventRadius		= fEventRadius
	sPed.sQueuedBroadcastEvent.iEventDelay		= iEventDelay
	sPed.sQueuedBroadcastEvent.bRaiseAlarm		= bRaiseAlarm
	sPed.sQueuedBroadcastEvent.bCalledSecurity	= bCalledSecurity
	
	IF bInstantDispatch
		BROADCAST_DISPATCH_SCRIPT_AI_EVENT(sPed.sQueuedBroadcastEvent)
	ENDIF
ENDPROC

/// PURPOSE:
///    Keeps track of the players status
PROC UPDATE_PLAYER_STATUS()

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		// player is in the security guard's vehicle
		IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehs[mvf_security].id, TRUE)
		
			e_PlayerStatus				= PLAYERSTATUS_SEC_CAR_STEALING
			
		// in player is getting out of one of the vans
		ELIF IS_PLAYER_EXITING_VAN()
		
			e_PlayerStatus				= PLAYERSTATUS_VAN_EXITING
	
		// in player is getting into one of the vans
		ELIF IS_PLAYER_ENTERING_VAN()
		
			e_PlayerStatus				= PLAYERSTATUS_VAN_ENTERING
	
		// is driving or towing one of the vans
		ELIF IS_PLAYER_IN_VAN()
		
			e_PlayerStatus				= PLAYERSTATUS_VAN_STEALING	
			bPromptGetBackInVan 		= FALSE
		
		ELIF IS_A_VAN_ATTACHED_TO_A_TOW_TRUCK()

			e_PlayerStatus				= PLAYERSTATUS_VAN_STEALING_TOW	
		
		// player has just damaged one of the vans
		ELIF HAS_PLAYER_DAMAGED_A_VAN()
		
			e_PlayerStatus				= PLAYERSTATUS_VAN_DAMAGE
			
		// Player's vehicle is still ditched in warehouse
		ELIF DOES_ENTITY_EXIST(veh_DumpedAtEntrance) AND IS_VEHICLE_DRIVEABLE(veh_DumpedAtEntrance) 
		AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh_DumpedAtEntrance, TRUE)
		AND IS_ENTITY_IN_ANGLED_AREA(veh_DumpedAtEntrance, v_WarehouseCoord, <<119.722847,-3092.472412,13.461255>>, 44.000000)
		
			e_PlayerStatus			= PLAYERSTATUS_WAREHOUSE_DITCH_VEH
			
		// Player first ditches a vehicle in the warehouse
		ELIF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		AND DOES_ENTITY_EXIST( GET_PLAYERS_LAST_VEHICLE() )
		AND NOT IS_LAST_VEHICLE_A_VAN()
		AND IS_ENTITY_IN_ANGLED_AREA(GET_PLAYERS_LAST_VEHICLE(), v_WarehouseCoord, <<119.722847,-3092.472412,13.461255>>, 44.000000)
		
			veh_DumpedAtEntrance	= GET_PLAYERS_LAST_VEHICLE()
			e_PlayerStatus			= PLAYERSTATUS_WAREHOUSE_DITCH_VEH
			
		// In the warehouse on foot
		ELIF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), v_WarehouseCoord, <<119.722847,-3092.472412,13.461255>>, 44.000000)	
		
			e_PlayerStatus 				= PLAYERSTATUS_WAREHOUSE_ON_FOOT
		
		// Player's vehicle is still ditched
		ELIF DOES_ENTITY_EXIST(veh_DumpedAtEntrance) AND IS_VEHICLE_DRIVEABLE(veh_DumpedAtEntrance) 
		AND NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), veh_DumpedAtEntrance, TRUE)
		//AND (IS_ENTITY_AT_COORD(veh_DumpedAtEntrance, <<166.306366,-3091.712402,7.326923>>,<<11.062500,19.250000,2.687500>>)
		AND (IS_ENTITY_IN_ANGLED_AREA(veh_DumpedAtEntrance, <<170.823593,-3092.405762,4.849259>>, <<154.329956,-3092.450195,9.783653>>, 14.062500)
		OR IS_ENTITY_AT_COORD(veh_DumpedAtEntrance, <<115.755623,-3092.294434,7.576007>>,<<4.375000,10.187500,2.562500>>))
		
			e_PlayerStatus			= PLAYERSTATUS_ENTRANCE_DITCH_VEH
		
		// player first ditches a vehicle at the entrance
		ELIF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		AND DOES_ENTITY_EXIST( GET_PLAYERS_LAST_VEHICLE() )
		AND NOT IS_LAST_VEHICLE_A_VAN()
		//AND (IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), <<166.306366,-3091.712402,7.326923>>,<<11.062500,19.250000,2.687500>>)
		AND (IS_ENTITY_IN_ANGLED_AREA(GET_PLAYERS_LAST_VEHICLE(), <<170.823593,-3092.405762,4.849259>>, <<154.329956,-3092.450195,9.783653>>, 14.062500)
		OR IS_ENTITY_AT_COORD(GET_PLAYERS_LAST_VEHICLE(), <<115.755623,-3092.294434,7.576007>>,<<4.375000,10.187500,2.562500>>))
		
			veh_DumpedAtEntrance	= GET_PLAYERS_LAST_VEHICLE()
			e_PlayerStatus			= PLAYERSTATUS_ENTRANCE_DITCH_VEH
	
		// In the warehouse in a vehicle
		ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		AND NOT IS_LAST_VEHICLE_A_VAN()
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), v_WarehouseCoord, <<119.722847,-3092.472412,13.461255>>, 44.000000)
		
			veh_DumpedAtEntrance	= GET_PLAYERS_LAST_VEHICLE()
			e_PlayerStatus 			= PLAYERSTATUS_WAREHOUSE_IN_VEHICLE	
			
		// at the front entrance in a vehicle
		ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		AND NOT IS_LAST_VEHICLE_A_VAN()
		//AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<166.306366,-3091.712402,7.326923>>,<<11.062500,19.250000,2.687500>>)
		AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<170.823593,-3092.405762,4.849259>>, <<154.329956,-3092.450195,9.783653>>, 14.062500)
			
			veh_DumpedAtEntrance	= GET_PLAYERS_LAST_VEHICLE()
			e_PlayerStatus			= PLAYERSTATUS_BLOCKING_ENTRANCE_WITH_VEHICLE
			
		// at the rear entrance in a vehicle
		ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<115.755623,-3092.294434,7.576007>>,<<4.375000,10.187500,2.562500>>)
			e_PlayerStatus 			= PLAYERSTATUS_BLOCKING_ENTRANCE_WITH_VEHICLE
			
		ELSE
			
			veh_DumpedAtEntrance 	= NULL

			// at the front entrance on foot
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE) 
			AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<159.688004,-3092.446045,7.314032>>,<<4.937500,7.312500,2.312500>>)
				
				e_PlayerStatus 				= PLAYERSTATUS_BLOCKING_ENTRANCE_ON_FOOT
				
			// at rear entrance
			ELIF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<115.755623,-3092.294434,7.576007>>,<<4.375000,10.187500,2.562500>>)

				e_PlayerStatus 				= PLAYERSTATUS_BLOCKING_ENTRANCE_ON_FOOT				
			
			// not doing anything notible
			ELSE
			
				e_PlayerStatus 				= PLAYERSTATUS_NONE
				
			ENDIF
		ENDIF

	// Scale foremans perception when the player is in the locate at the front entrance
		IF e_PlayerStatus = PLAYERSTATUS_BLOCKING_ENTRANCE_WITH_VEHICLE
			
			IF IS_ENTITY_ALIVE(peds[mpf_bugstar_foreman_front].id)
				SET_PED_VISUAL_FIELD_PROPERTIES(peds[mpf_bugstar_foreman_front].id, 120, 20.0, 120, -90, 90)
			ENDIF
			
		ELSE
			
			IF IS_ENTITY_ALIVE(peds[mpf_bugstar_foreman_front].id)
				SET_PED_VISUAL_FIELD_PROPERTIES(peds[mpf_bugstar_foreman_front].id, 17.5, 10.0, 120, -90, 90)
			ENDIF
			
		ENDIF
		
	// Disable the players lethal stealth kills, use knock outs until all kicks off.
		IF bIsPlayerStillStealth
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_SuppressLethalMeleeActions, TRUE)
		ENDIF
		
		// Player arrives with a wanted level, set the staff to be alerted
		IF e_PlayerStatus != PLAYERSTATUS_NONE
		AND IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
		AND IS_COP_PED_IN_AREA_3D(<<182.953690,-3119.883789,101.922592>>-<<94.250000,239.750000,108.062500>>,
			<<182.953690,-3119.883789,101.922592>>+<<94.250000,239.750000,108.062500>>)
			
		ENDIF
		
		#IF IS_DEBUG_BUILD
		
			IF bDisplayDebug
				TEXT_LABEL_63 strDebugText
				strDebugText = "Player Status: "
				
				SWITCH e_PlayerStatus
					DEFAULT 
						strDebugText += "DEBUG_MISSING"
					BREAK
					CASE PLAYERSTATUS_NONE
						strDebugText += "NONE"
					BREAK
					CASE PLAYERSTATUS_BLOCKING_ENTRANCE_ON_FOOT
						strDebugText += "BLOCK_ENT_FOOT"
					BREAK
					CASE PLAYERSTATUS_BLOCKING_ENTRANCE_WITH_VEHICLE
						strDebugText += "BLOCK_ENT_VEH"
					BREAK
					CASE PLAYERSTATUS_WAREHOUSE_ON_FOOT
						strDebugText += "WH_FOOT"
					BREAK
					CASE PLAYERSTATUS_WAREHOUSE_IN_VEHICLE
						strDebugText += "WH_VEH"
					BREAK
					CASE PLAYERSTATUS_WAREHOUSE_DITCH_VEH
						strDebugText += "WH_DITCHED_VEH"
					BREAK
					CASE PLAYERSTATUS_ENTRANCE_DITCH_VEH
						strDebugText += "ENT_DITCHED_VEH"
					BREAK
					CASE PLAYERSTATUS_VAN_DAMAGE
						strDebugText += "VAN_DMG"
					BREAK
					CASE PLAYERSTATUS_VAN_ENTERING
						strDebugText += "VAN_ENTER"
					BREAK
					CASE PLAYERSTATUS_VAN_EXITING
						strDebugText += "VAN_EXIT"
					BREAK
					CASE PLAYERSTATUS_VAN_STEALING
						strDebugText += "VAN_STEALING"
					BREAK
					CASE PLAYERSTATUS_VAN_STEALING_TOW
						strDebugText += "VAN_STEALING_TOW"
					BREAK
					CASE PLAYERSTATUS_SEC_CAR_STEALING
						strDebugText += "SEC_CAR_STEALING"
					BREAK
					
				ENDSWITCH
				DRAW_DEBUG_TEXT_2D_WRAPPED(strDebugText, 0,0,0,255)
				
				strDebugText = "veh_DumpedAtEntrance: "
				IF DOES_ENTITY_EXIST(veh_DumpedAtEntrance)
					strDebugText += "STORED"
				ELSE
					strDebugText += "EMPTY"
				ENDIF
				DRAW_DEBUG_TEXT_2D_WRAPPED(strDebugText, 0,0,0,255)
			ENDIF
			
		#ENDIF
		
	ENDIF

ENDPROC

PROC BUGSTAR_STAFF_AI_STATE(PED_STRUCT &sPed)

	CONST_FLOAT			NOTICE_WEAPON_DIST				8.0

//-------------------------------------------------------------------------------------
// AI pre-update 
//-------------------------------------------------------------------------------------

	WEAPON_TYPE eWeapPlayer
	GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eWeapPlayer)
	
	FLOAT fDistPlayer = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sPed.id)
	
	BOOL bPunchedPed, bHitWithVehicle
	
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(sPed.id)
		bHitWithVehicle = TRUE
	ENDIF
	
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sPed.id, PLAYER_PED_ID(), TRUE)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sPed.id, WEAPONTYPE_UNARMED)
			bPunchedPed = TRUE
		ENDIF
	ENDIF

//-------------------------------------------------------------------------------------
// AI Event update
//-------------------------------------------------------------------------------------

	TEXT_LABEL_63 strEventName
	
	BOOL bCanSeeBody, bKilledInFrontOfObserver, bSawPlayerDoIt, bNonLethal
	bCanSeeBody = CAN_PED_SEE_DEAD_BODY(sPed, bKilledInFrontOfObserver, bSawPlayerDoIt, bNonLethal)
	
	// Saw a ped get killed infront of them
	IF sPed.eAI != PEDAI_FLEEING
	AND bCanSeeBody
	AND bKilledInFrontOfObserver
	AND NOT bNonLethal
	
		IF bSawPlayerDoIt
			SET_PED_AI_STATE(sPed, PEDAI_FLEEING, TRUE, "PLAYER_KILLED")
		ELSE
			SET_PED_AI_STATE(sPed, PEDAI_FLEEING, TRUE, "KILLED")
		ENDIF
	
	// Ped heard gun fire
	ELIF HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_SHOT_FIRED)
	//OR IS_BULLET_IN_AREA(GET_ENTITY_COORDS(sPed.id), 10.0, TRUE)
	OR HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_SHOT_FIRED_BULLET_IMPACT)
	OR HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_SHOT_FIRED_WHIZZED_BY)
	OR HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_EXPLOSION)
	OR HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_EXPLOSION_HEARD)
	OR HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_SHOCKING_EXPLOSION)
	
	
		SET_PED_AI_STATE(sPed, PEDAI_FLEEING, TRUE, "GUNSHOT")
	
	// Aiming gun around
	ELIF sPed.bCanSeePlayer 
	AND sPed.eAI != PEDAI_HANDSUP
	AND IS_PLAYER_AIMING(WF_INCLUDE_GUN)
	
		SET_PED_AI_STATE(sPed, PEDAI_HANDSUP, TRUE, "GUN_AIMED")
	
	// Aiming projectile at ped
	ELIF sPed.bCanSeePlayer AND IS_PLAYER_AIMING_AT_PED(sPed, WF_INCLUDE_PROJECTILE)
	
		SET_PED_AI_STATE(sPed, PEDAI_FLEEING, TRUE, "PROJ_AIMED")
		
	// Can see player with a gun
	ELIF sPed.bCanSeePlayer
	AND fDistPlayer < NOTICE_WEAPON_DIST
	AND sPed.eAI != PEDAI_HANDSUP
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
	AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_MELEE | WF_INCLUDE_GUN)
	
		SET_PED_AI_STATE(sPed, PEDAI_FLEEING, TRUE, "SEEN_WEAPON")
		
	// Player is seen getting out of a van
	ELIF (sPed.bCanSeePlayer OR sPed.bCanHearPlayer)
	AND e_PlayerStatus = PLAYERSTATUS_VAN_EXITING
	
		IF sPed.bCanSeePlayer
			SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "EXIT_VAN")
		ELIF sPed.bCanHearPlayer
			SET_PED_AI_STATE_WITH_COORDS(sPed, PEDAI_INVESTIGATE,  "HEARD_PLAYER", GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE)
		ENDIF

	// Player is seen getting into a van
	ELIF (sPed.bCanSeePlayer OR sPed.bCanHearPlayer)
	AND e_PlayerStatus = PLAYERSTATUS_VAN_ENTERING
	
		IF sPed.bCanSeePlayer
			SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "GET_IN_VAN")
		ELIF sPed.bCanHearPlayer
			SET_PED_AI_STATE_WITH_COORDS(sPed, PEDAI_INVESTIGATE,  "HEARD_PLAYER", GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE)
		ENDIF
		
	// player is seen towing a van away
	ELIF (sPed.bCanSeePlayer OR sPed.bCanHearPlayer)
	AND e_PlayerStatus = PLAYERSTATUS_VAN_STEALING_TOW
		
		IF sPed.bCanSeePlayer
			SET_PED_AI_STATE_WITH_ENTITY(sPed, PEDAI_COMBAT_PLAYER, "STEALING_VAN", vehs[GET_VAN_PLAYER_IS_TOWING()].id, TRUE)
		ELIF sPed.bCanHearPlayer
			SET_PED_AI_STATE_WITH_ENTITY(sPed, PEDAI_INVESTIGATE, "HEARD_VAN", vehs[GET_VAN_PLAYER_IS_TOWING()].id, TRUE)
		ENDIF

	// Starting a fight with a ped
	ELIF bPunchedPed
	OR HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_MELEE_ACTION)
	OR HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_SHOCKING_SEEN_MELEE_ACTION)
	OR CAN_PED_SEE_A_PED_BEING_ATTACKED(sPed, FALSE)

		SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "FIGHT")
		
	// Ran over
	ELIF bHitWithVehicle
	OR HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_SHOCKING_PED_RUN_OVER)
	OR CAN_PED_SEE_A_PED_BEING_ATTACKED(sPed, TRUE)
	
		SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "RAN_OVER")
		
	// After having a scrap with the player spots him again near the warehouse or near the ped
	ELIF sPed.eAI != PEDAI_COMBAT_PLAYER
	AND sPed.bHasFoughtWithPlayer
	AND (sPed.bCanSeePlayer OR sPed.bCanHearPlayer)
	AND (fDistPlayer < 8.0	OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_WarehouseCoord) < 40.0)
	
		IF sPed.bCanSeePlayer
			SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "FIGHT_RETURN")
		ELIF sPed.bCanHearPlayer
			SET_PED_AI_STATE_WITH_COORDS(sPed, PEDAI_INVESTIGATE,  "HEARD_PLAYER", GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE)
		ENDIF
		
	// Seed another ped knocked out
	ELIF sPed.eAI != PEDAI_FLEEING
	AND bCanSeeBody	

		// KO's infront of ped by player
		IF bKilledInFrontOfObserver
		AND bSawPlayerDoIt
		AND bNonLethal
			SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "PLAYER_KO")
			
		// otherwise count as found dead body (bSawPlayerDoIt will be forced to true if the player was observed doing it before)
		ELIF NOT bSawPlayerDoIt
			SET_PED_AI_STATE(sPed, PEDAI_FLEEING, TRUE, "DEAD_BODY")
		ENDIF
		
	// Noticed 1 of the vans leaving the warehouse
	ELIF (sPed.bCanSeePlayer OR sPed.bCanHearPlayer)
	AND (sPed.bReactsToEntrance OR sPed.bReactsToIntrusion)
	AND e_PlayerStatus = PLAYERSTATUS_VAN_STEALING
	AND NOT ARE_STRINGS_EQUAL(sPed.strAIEvent, "SEE_VAN_LEAVING")
	AND NOT ARE_STRINGS_EQUAL(sPed.strAIEvent, "SEE_VAN_LEAVING_1")
	AND NOT ARE_STRINGS_EQUAL(sPed.strAIEvent, "CHECK_DRIVER")
	AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_WarehouseCoord) < 15.0
	
		IF sPed.bCanSeePlayer
			SET_PED_AI_STATE_WITH_ENTITY(sPed, PEDAI_INVESTIGATE, "SEE_VAN_LEAVING", vehs[GET_VAN_PLAYER_IS_IN()].id, TRUE)
		ELIF sPed.bCanHearPlayer
			SET_PED_AI_STATE_WITH_ENTITY(sPed, PEDAI_INVESTIGATE, "HEARD_VAN", vehs[GET_VAN_PLAYER_IS_IN()].id, TRUE)
		ENDIF

	// Intrusion of private property
	ELIF e_PlayerStatus != PLAYERSTATUS_VAN_STEALING
		IF (sPed.bReactsToEntrance 	
			AND (e_PlayerStatus = PLAYERSTATUS_BLOCKING_ENTRANCE_ON_FOOT 	
			OR e_PlayerStatus = PLAYERSTATUS_BLOCKING_ENTRANCE_WITH_VEHICLE		
			OR e_PlayerStatus = PLAYERSTATUS_ENTRANCE_DITCH_VEH
			OR sPed.eAI = PEDAI_INVESTIGATE)
		OR (sPed.bReactsToInTrusion 	
			AND (e_PlayerStatus = PLAYERSTATUS_WAREHOUSE_IN_VEHICLE 		
			OR e_PlayerStatus = PLAYERSTATUS_WAREHOUSE_ON_FOOT					
			OR e_PlayerStatus = PLAYERSTATUS_WAREHOUSE_DITCH_VEH)))
		
			/* Don't need to see or hear the player to complain about the ditched vehicle, 
			 this is so if the player walks off the AI continue to complain about it */
			IF (e_PlayerStatus = PLAYERSTATUS_WAREHOUSE_DITCH_VEH
			OR e_PlayerStatus = PLAYERSTATUS_ENTRANCE_DITCH_VEH)
			//AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(sPed.id, veh_DumpedAtEntrance)
			AND HAS_ENTITY_CLEAR_LOS_TO_ENTITY(sPed.id, veh_DumpedAtEntrance,  SCRIPT_INCLUDE_MOVER | SCRIPT_INCLUDE_OBJECT | SCRIPT_INCLUDE_VEHICLE)
			
				SWITCH e_PlayerStatus
					CASE PLAYERSTATUS_WAREHOUSE_DITCH_VEH				strEventName = "WH_DITCH_VEH"		BREAK
					CASE PLAYERSTATUS_ENTRANCE_DITCH_VEH				strEventName = "ENT_DITCH_VEH"		BREAK
				ENDSWITCH
				SET_PED_AI_STATE(sPed, PEDAI_CONFRONTING_PLAYER, TRUE, strEventName)	
			
			ELIF sPed.bCanSeePlayer
				// Confront the player
				SWITCH e_PlayerStatus
					CASE PLAYERSTATUS_BLOCKING_ENTRANCE_ON_FOOT			strEventName = "ENT_FOOT"			BREAK
					CASE PLAYERSTATUS_BLOCKING_ENTRANCE_WITH_VEHICLE	strEventName = "ENT_VEH"			BREAK
					CASE PLAYERSTATUS_WAREHOUSE_ON_FOOT					strEventName = "WH_FOOT"			BREAK
					CASE PLAYERSTATUS_WAREHOUSE_IN_VEHICLE				strEventName = "WH_VEH"				BREAK
					CASE PLAYERSTATUS_WAREHOUSE_DITCH_VEH				strEventName = "WH_DITCH_VEH"		BREAK
					CASE PLAYERSTATUS_ENTRANCE_DITCH_VEH				strEventName = "ENT_DITCH_VEH"		BREAK
					CASE PLAYERSTATUS_NONE					
						IF sPed.eAI = PEDAI_INVESTIGATE
							strEventName = "ENT_FOOT"
						ENDIF
					BREAK
				ENDSWITCH
				
				SET_PED_AI_STATE(sPed, PEDAI_CONFRONTING_PLAYER, TRUE, strEventName)	
				
			ELIF sPed.bCanHearPlayer
				// Investigate sound 
				SET_PED_AI_STATE_WITH_COORDS(sPed, PEDAI_INVESTIGATE, "HEARD_PLAYER", GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE)
			ENDIF
			
		ENDIF
	ENDIF
	
//-------------------------------------------------------------------------------------
// CHECK FOR BROADCAST EVENT
//-------------------------------------------------------------------------------------

	IF sPed.iReceivedEvent > -1 
	AND sPed.iReceivedEvent < COUNT_OF(s_BroadcastEvent)-1
	
		BOOL bCameFromAlarm = s_BroadcastEvent[sPed.iReceivedEvent].bRaiseAlarm
	
		SWITCH GET_HASH_KEY(s_BroadcastEvent[sPed.iReceivedEvent].strEventName)
			CASE HASH("KILLED")
			CASE HASH("PLAYER_KILLED")
			CASE HASH("DEAD_BODY")
				IF sPed.eAI != PEDAI_HANDSUP
					SET_PED_AI_STATE(sPed, PEDAI_FLEEING, TRUE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, FALSE, bCameFromAlarm)
				ENDIF
			BREAK
			CASE HASH("PLAYER_KO")
				IF sPed.eAI != PEDAI_HANDSUP
					SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, FALSE, bCameFromAlarm)
				ENDIF
			BREAK
			CASE HASH("GUN_AIMED_AT")
				IF sPed.eAI != PEDAI_HANDSUP
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(sPed.id, s_BroadcastEvent[sPed.iReceivedEvent].vEventCoord) < 5.0
						SET_PED_AI_STATE(sPed, PEDAI_HANDSUP, TRUE, "GUN_AIMED_AT", FALSE, bCameFromAlarm)
					ELSE
						SET_PED_AI_STATE(sPed, PEDAI_FLEEING, TRUE, "SEEN_WEAPON", FALSE, bCameFromAlarm)
					ENDIF
				ENDIF
			BREAK
			CASE HASH("PROJ_AIMED")
				IF sPed.eAI != PEDAI_HANDSUP
					SET_PED_AI_STATE(sPed, PEDAI_FLEEING, TRUE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, FALSE, bCameFromAlarm)
				ENDIF
			BREAK
			CASE HASH("SEEN_WEAPON")
				IF sPed.eAI != PEDAI_HANDSUP
					SET_PED_AI_STATE(sPed, PEDAI_FLEEING, TRUE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, FALSE, bCameFromAlarm)
				ENDIF
			BREAK
			CASE HASH("FIGHT")
			CASE HASH("FIGHT_RETURN")
			CASE HASH("RAN_OVER")
			CASE HASH("VEH_FAILED_LEAVE")
				IF sPed.eAI != PEDAI_HANDSUP
					SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, FALSE, bCameFromAlarm)
				ENDIF
			BREAK
			CASE HASH("STEALING_VAN")
			CASE HASH("GET_IN_VAN")
			CASE HASH("EXIT_VAN")
				IF sPed.eAI != PEDAI_HANDSUP
				AND NOT ARE_STRINGS_EQUAL(sPed.strAIEvent, "STEALING_VAN")
				AND NOT ARE_STRINGS_EQUAL(sPed.strAIEvent, "GET_IN_VAN")
				AND NOT ARE_STRINGS_EQUAL(sPed.strAIEvent, "EXIT_VAN")
					SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, FALSE, bCameFromAlarm)
				ENDIF
			BREAK
			CASE HASH("GUNSHOT")
				IF sPed.eAI != PEDAI_HANDSUP
					SET_PED_AI_STATE(sPed, PEDAI_FLEEING, TRUE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, FALSE, bCameFromAlarm)
				ENDIF
			BREAK
			
			CASE HASH("ENT_FOOT")
				IF sPed.bReactsToEntrance
					SET_PED_AI_STATE(sPed, PEDAI_CONFRONTING_PLAYER, TRUE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, FALSE, bCameFromAlarm)
				ENDIF
			BREAK
			CASE HASH("ENT_VEH")
				IF sPed.bReactsToEntrance
					SET_PED_AI_STATE(sPed, PEDAI_CONFRONTING_PLAYER, TRUE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, FALSE, bCameFromAlarm)
				ENDIF
			BREAK
			CASE HASH("WH_FOOT")
				IF sPed.bReactsToInTrusion 
					SET_PED_AI_STATE(sPed, PEDAI_CONFRONTING_PLAYER, TRUE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, FALSE, bCameFromAlarm)
				ENDIF
			BREAK
			CASE HASH("WH_VEH")
				IF sPed.bReactsToInTrusion 
					SET_PED_AI_STATE(sPed, PEDAI_CONFRONTING_PLAYER, TRUE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, FALSE, bCameFromAlarm)
				ENDIF
			BREAK
			CASE HASH("WH_DITCHED_VEH")
				IF sPed.bReactsToInTrusion
					SET_PED_AI_STATE(sPed, PEDAI_CONFRONTING_PLAYER, TRUE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, FALSE, bCameFromAlarm)
				ENDIF
			BREAK
			CASE HASH("HEARD_PLAYER")
				IF sPed.bReactsToInTrusion
					SET_PED_AI_STATE_WITH_COORDS(sPed, PEDAI_INVESTIGATE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE, FALSE, bCameFromAlarm)
				ENDIF
			BREAK
			CASE HASH("HEARD_VAN")
				IF sPed.bReactsToInTrusion
					SET_PED_AI_STATE_WITH_ENTITY(sPed, PEDAI_INVESTIGATE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, vehs[GET_VAN_PLAYER_IS_IN()].id, TRUE, FALSE, bCameFromAlarm)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	sPed.iReceivedEvent = -1

//-------------------------------------------------------------------------------------
// 	 MANAGE STATES
//-------------------------------------------------------------------------------------

	// Check for state update again
	IF sPed.eAIPrevFrame != sPed.eAI
		sPed.bAIStateChangedLastFrame 			= TRUE
	ELSE
		sPed.bAIStateChangedLastFrame 			= FALSE
	ENDIF	
	sPed.eAIPrevFrame = sPed.eAI
	
	IF NOT ARE_STRINGS_EQUAL(sPed.strAIEventPrevFrame, sPed.strAIEvent)
		sPed.bAIEventChangedLastFrame			= TRUE
	ELSE
		sPed.bAIEventChangedLastFrame			= FALSE
	ENDIF
	sPed.strAIEventPrevFrame = sPed.strAIEvent

	MISSION_PED_FLAGS i, ePed
	REPEAT COUNT_OF(peds) i
		IF peds[i].id = sPed.id
			ePed = i
		ENDIF
	ENDREPEAT
	
	TEXT_LABEL_23 strRoot

	SWITCH sPed.eAI
		
		CASE PEDAI_FLEEING
		
			SWITCH GET_HASH_KEY(sPed.strAIEvent)
				CASE HASH("GUNSHOT")
				CASE HASH("SEEN_WEAPON")
				CASE HASH("PROJ_AIMED")
				CASE HASH("KILLED")
				CASE HASH("PLAYER_KILLED")
				CASE HASH("DEAD_BODY")
					
					// play panic responce
					IF HAS_PED_AI_JUST_ENTERED_STATE(sPed) 		// Either the AI has just jumped here
					OR sPed.iDiaFlag % 2 = 0					// every other convo should play this
					OR NOT IS_ENTITY_ALIVE(pedSecurity.id)		// or the security guard is dead
					
						IF GET_HASH_KEY(sPed.strAIEvent) = HASH("GUNSHOT")
						OR GET_HASH_KEY(sPed.strAIEvent) = HASH("SEEN_WEAPON")
						OR GET_HASH_KEY(sPed.strAIEvent) = HASH("PROJ_AIMED")
						
							IF IS_PED_A_FOREMAN(ePed)
								strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_GUN1")
								IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 1000)
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 15.0, 3000, 250, FALSE, TRUE)
								ELSE
									IF sPed.iDiaFlag = 0
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 15.0, 3000, 250, TRUE, TRUE)
									ENDIF
								ENDIF
							ELSE
							
								SWITCH ePed 
									CASE mpf_bugstar_front_1
										strRoot = "SOL1_ARM1"
									BREAK
									CASE mpf_bugstar_front_2
										strRoot = "SOL1_ARM2"
									BREAK
								ENDSWITCH
								IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 1000)
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 15.0, 3000, 250, FALSE, TRUE)
								ELSE
									IF sPed.iDiaFlag = 0
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 15.0, 3000, 250, TRUE, TRUE)
									ENDIF
								ENDIF
								
							ENDIF
						
						ELIF GET_HASH_KEY(sPed.strAIEvent) = HASH("KILLED")
						OR GET_HASH_KEY(sPed.strAIEvent) = HASH("PLAYER_KILLED")
						OR GET_HASH_KEY(sPed.strAIEvent) = HASH("DEAD_BODY")

							IF IS_PED_A_FOREMAN(ePed)
								strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_KILL")
								IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 2000)
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 15.0, 3000, 250, FALSE, TRUE)
								ELSE
									IF sPed.iDiaFlag = 0
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 15.0, 3000, 250, TRUE, TRUE)
									ENDIF
								ENDIF
							ELSE
								IF sPed.iDiaFlag = 0
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 15.0, 3000, 250, TRUE, TRUE)
								ENDIF
							ENDIF
						
						ENDIF
						
						sPed.iDiaFlag++
					
					// Otherwise call for security
					ELSE
						IF IS_PED_A_FOREMAN(ePed)
							strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_GETSEC")
							IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 2000)
								BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 15.0, 3000, 250, FALSE, TRUE, TRUE)
							ELSE
								IF sPed.iDiaFlag = 0
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 15.0, 3000, 250, TRUE, TRUE, TRUE)
								ENDIF
							ENDIF
						ELSE
						
							SWITCH ePed 
								CASE mpf_bugstar_front_1
									strRoot = "SOL1_SECIN1"
								BREAK
								CASE mpf_bugstar_front_2
									strRoot = "SOL1_SECIN2"
								BREAK
							ENDSWITCH
							
							IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 2000)
								BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 15.0, 3000, 250, FALSE, TRUE, TRUE)
							ELSE
								IF sPed.iDiaFlag = 0
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 15.0, 3000, 250, TRUE, TRUE, TRUE)
								ENDIF
							ENDIF
						ENDIF
						sPed.iDiaFlag++
							
					ENDIF
		
				BREAK
			ENDSWITCH
			
		BREAK
	
		CASE PEDAI_HANDSUP
		
			SWITCH GET_HASH_KEY(sPed.strAIEvent)
				CASE HASH("GUN_AIMED")
					IF IS_PLAYER_AIMING_AT_PED(sPed, WF_INCLUDE_GUN)
					
						SET_PED_AI_STATE(sPed, PEDAI_HANDSUP, FALSE, "GUN_AIMED_AT")
				
					ELIF GET_GAME_TIMER() - sPed.iTimeOfEnteringState > 1000
						
						SET_PED_AI_STATE(sPed, PEDAI_FLEEING, TRUE, "SEEN_WEAPON")
						
					ENDIF
				BREAK
				
				CASE HASH("GUN_AIMED_AT")
				
					// Keep setting the state as long as the player is aiming at the ped
					IF IS_PLAYER_AIMING_AT_PED(sPed, WF_INCLUDE_GUN)
						SET_PED_AI_STATE(sPed, PEDAI_HANDSUP, TRUE, "GUN_AIMED_AT")
					ENDIF
				
					IF (GET_GAME_TIMER() - sPed.iTimeOfEnteringState > 15000		// flee after 15 seconds of being aimed at
					OR GET_GAME_TIMER() - sPed.iTimeOfStateLastSet > 3000)			// flee after 3 seconds of NOT being aimed at
					AND NOT IS_PED_IN_CURRENT_CONVERSATION(sPed.id)
					
						SET_PED_AI_STATE(sPed, PEDAI_FLEEING, TRUE, "SEEN_WEAPON")
						
					ELSE
						// Play hands up pleading covnersations, first 1 interrupts and plays immediately
						IF HAS_PED_AI_JUST_ENTERED_STATE(sPed)
						OR sPed.iDiaFlag = 0
							IF IS_PED_A_FOREMAN(ePed)
								strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_HND1")
								IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 500)
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 5.0, 3000, 500, FALSE, TRUE)
								ELSE
									IF sPed.iDiaFlag = 0
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 5.0, 3000, 500, TRUE, TRUE)
									ENDIF
								ENDIF
							ELSE
								SWITCH ePed 
									CASE mpf_bugstar_front_1
										strRoot = "SOL1_SCAR1"
									BREAK
									CASE mpf_bugstar_front_2
										strRoot = "SOL1_SCAR2"
									BREAK
								ENDSWITCH
								IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 500)
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 5.0, 3000, 500, FALSE, TRUE)
								ELSE
									IF sPed.iDiaFlag = 0
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 5.0, 3000, 500, TRUE, TRUE)
									ENDIF
								ENDIF
							ENDIF
							sPed.iDiaFlag++
						
						// after that we don't interrupt and play every 2 seconds
						ELSE
							IF IS_PED_A_FOREMAN(ePed)
								strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_HND1")
								IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, FALSE, FALSE, TRUE, 2000)
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 5.0, 3000, 500, FALSE, TRUE)
								ELSE
									IF sPed.iDiaFlag = 0
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 5.0, 3000, 500, TRUE, TRUE)
									ENDIF
								ENDIF
							ELSE
								SWITCH ePed 
									CASE mpf_bugstar_front_1
										strRoot = "SOL1_SCAR1"
									BREAK
									CASE mpf_bugstar_front_2
										strRoot = "SOL1_SCAR2"
									BREAK
								ENDSWITCH
								IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, FALSE, FALSE, TRUE, 2000)
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 5.0, 3000, 500, FALSE, TRUE)
								ELSE
									IF sPed.iDiaFlag = 0
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 5.0, 3000, 500, TRUE, TRUE)
									ENDIF
								ENDIF
							ENDIF
							sPed.iDiaFlag++
						ENDIF
					
					ENDIF
				
				BREAK
			
			ENDSWITCH
		
			
		BREAK
	
		CASE PEDAI_WORKING

			SWITCH ePed 
				CASE mpf_bugstar_foreman_rear
					IF IS_SAFE_TO_START_CONVERSATION()
					AND GET_GAME_TIMER() - i_TimeOfLastConvo > 5000
						SWITCH iStockCheckListProg
							CASE 0			
								PLAY_CONVERSATION(ePed, "JS_STOCK", sPed.eAI)			
								iStockCheckListProg++
							BREAK
							CASE 1			
								PLAY_CONVERSATION(ePed, "JS_STOCKb", sPed.eAI)			
								iStockCheckListProg++
							BREAK
							CASE 2			
								PLAY_CONVERSATION(ePed, "JS_STOCKc", sPed.eAI)			
								iStockCheckListProg++
							BREAK
							CASE 3			
								PLAY_CONVERSATION(ePed, "JS_STOCKd", sPed.eAI)			
								iStockCheckListProg++
							BREAK
							CASE 4			
								PLAY_CONVERSATION(ePed, "JS_STOCKe", sPed.eAI)			
								iStockCheckListProg++
							BREAK
							CASE 5			
								PLAY_CONVERSATION(ePed, "JS_STOCKf", sPed.eAI)			
								iStockCheckListProg++
							BREAK
							CASE 6			
								PLAY_CONVERSATION(ePed, "JS_STOCKg", sPed.eAI)			
								iStockCheckListProg++
							BREAK
							CASE 7			
								PLAY_CONVERSATION(ePed, "JS_STOCKh", sPed.eAI)			
								iStockCheckListProg++
							BREAK
							CASE 8			
								PLAY_CONVERSATION(ePed, "JS_STOCKi", sPed.eAI)			
								iStockCheckListProg++
							BREAK
							CASE 9	
								PLAY_CONVERSATION(ePed, "JS_STOCKj", sPed.eAI)		
								iStockCheckListProg++
							BREAK
							CASE 10			
								PLAY_CONVERSATION(ePed, "JS_STOCKk", sPed.eAI)			
								iStockCheckListProg = 0
							BREAK
						ENDSWITCH
					ENDIF
				BREAK
				CASE mpf_bugstar_front_1
					IF NOT sPed.bPlayedConvoStart
						IF PLAY_CONVERSATION(ePed, "SOL1_AMB1", sPed.eAI, DEFAULT, DEFAULT, DEFAULT, DEFAULT, TRUE, DEFAULT, TRUE)
							ADD_PED_FOR_DIALOGUE(s_Convo, 4, peds[mpf_bugstar_front_1].id, "CONSTRUCTION1")
							ADD_PED_FOR_DIALOGUE(s_Convo, 3, peds[mpf_bugstar_front_2].id, "CONSTRUCTION2")
							sPed.bPlayedConvoStart = TRUE
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH

		BREAK
		
		CASE PEDAI_INVESTIGATE
			BOOL bCanSeeDriver
			IF ARE_STRINGS_EQUAL(sPed.strAIEvent, "SEE_VAN_LEAVING")
			OR ARE_STRINGS_EQUAL(sPed.strAIEvent, "CHECK_DRIVER")

				FLOAT fDot, fAngle
				VECTOR v1, v2
				v1 = NORMALISE_VECTOR( GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0,1,0>>) - GET_ENTITY_COORDS(PLAYER_PED_ID()))
				v2 = NORMALISE_VECTOR( GET_ENTITY_COORDS(peds[ePed].id) - GET_ENTITY_COORDS(PLAYER_PED_ID()))
				fDot = DOT_PRODUCT(	v1, v2)
				fAngle = ACOS(fDot)
				
				IF fAngle < 90.0
				AND fDistPlayer < 8.0
				AND GET_ENTITY_SPEED(PLAYER_PED_ID()) < 1.0
					bCanSeeDriver = TRUE
				ENDIF
				
			ENDIF
		
			SWITCH GET_HASH_KEY(sPed.strAIEvent)
				CASE HASH("SEE_VAN_LEAVING")
				CASE HASH("SEE_VAN_LEAVING_1")
				
					// Initial leaving convo
					IF HAS_PED_AI_JUST_ENTERED_STATE(sPed)
					AND ARE_STRINGS_EQUAL(sPed.strAIEvent, "SEE_VAN_LEAVING")
						
						IF IS_PED_A_FOREMAN(ePed)
							strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_VAN_LVE1")
							PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, FALSE, TRUE, 1000)
						ENDIF

					// Can see the driver, check his ID
					ELIF bCanSeeDriver
					
						SET_PED_AI_STATE_WITH_ENTITY(sPed, PEDAI_INVESTIGATE, "CHECK_DRIVER", vehs[GET_VAN_PLAYER_IS_IN()].id, FALSE)
						
					ELIF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_WarehouseCoord) >= 20.0
					
						// Van has left area, do not go over to investigate
						IF fDistPlayer > 5.0
							SET_PED_AI_STATE(sPed, PEDAI_INVESTIGATE, FALSE, "VAN_LOST")
						ENDIF
						
					ENDIF
				
				BREAK
				CASE HASH("VAN_LOST")
					
					IF HAS_PED_AI_JUST_ENTERED_STATE(sPed)
					
						strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_VAN_LVE2")
						PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, FALSE)
						
					ELSE
						// Go back to work after 3 seconds
						IF GET_GAME_TIMER() - sPed.iTimeOfEnteringState > 3000
							SET_PED_AI_STATE(sPed, PEDAI_WORKING, FALSE, "BACK_TO_WORK")
						ENDIF
					ENDIF
				
				BREAK
				CASE HASH("CHECK_DRIVER")
				
					IF bCanSeeDriver
						
						// Has looked at driver for a short while, go to stealing state
						IF GET_GAME_TIMER() - sPed.iTimeOfEnteringState > 2000
						AND NOT IS_PED_IN_CURRENT_CONVERSATION(sPed.id)

							SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "STEALING_VAN")
							
						ENDIF
						
					ELSE
						SET_PED_AI_STATE_WITH_ENTITY(sPed, PEDAI_INVESTIGATE, "SEE_VAN_LEAVING_1", vehs[GET_VAN_PLAYER_IS_IN()].id, FALSE)
					ENDIF
					
				BREAK
				CASE HASH("HEARD_PLAYER")
				
					// Has reached investigation coord
					IF GET_GAME_TIMER() - sPed.iTimeOfEnteringState > 4000	// give the ped chance to turn and look, safety for if the coord is very close to the ped
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(sPed.id, sPed.vInvestigationCoord) < 3.0
					AND IS_PED_STILL(sPed.id)

						SET_PED_AI_STATE(sPed, PEDAI_INVESTIGATE, FALSE, "SOUND_LOST")
											
					ELIF HAS_PED_AI_JUST_ENTERED_STATE(sPed)
						IF IS_PED_A_FOREMAN(ePed)
							strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_SND_INV")
							IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, TRUE, 0)
								BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 3.0, 3000, 500, FALSE)
							ENDIF
						ENDIF
					ENDIF
					
				BREAK
				CASE HASH("SOUND_LOST")
										
					IF GET_GAME_TIMER() - sPed.iTimeOfEnteringState > 4000
					AND NOT IS_PED_IN_CURRENT_CONVERSATION(sPed.id)
						
						IF IS_PED_A_FOREMAN(ePed)
							strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_SNDLOST2")
							PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, FALSE, FALSE, TRUE, 1000)
						ENDIF
						SET_PED_AI_STATE(sPed, PEDAI_WORKING, FALSE, "BACK_TO_WORK")
					
					ELIF HAS_PED_AI_JUST_ENTERED_STATE(sPed)
						IF IS_PED_A_FOREMAN(ePed)
							strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_SNDLOST1")
							PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, FALSE, FALSE, TRUE, 1000)
						ENDIF
					ENDIF
					
				BREAK
			ENDSWITCH
		
		BREAK
		
		CASE PEDAI_COMBAT_PLAYER
		
			SWITCH GET_HASH_KEY(sPed.strAIEvent)
				CASE HASH("FIGHT")
				CASE HASH("FIGHT_RETURN")
				CASE HASH("RAN_OVER")
				CASE HASH("VEH_FAILED_LEAVE")
				
					// Player has ran out and the delay has passed
					IF GET_GAME_TIMER() - sPed.iTimeOfStateLastSet > 8000
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_WarehouseCoord) > 65
					AND (GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sPed.id) > 10.0
						OR GET_GAME_TIMER() - sPed.iTimeOfStateLastSet > 16000)
					AND NOT IS_PED_IN_CURRENT_CONVERSATION(sPed.id)
					
						SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, FALSE, "FIGHT_ESC")
				
					ELIF HAS_PED_AI_JUST_ENTERED_STATE(sPed)
					AND sPed.bDirectEvent	// only do for the ped who got started on, the other's don't need to call for help
					
						// Starts fighting with the bugstar staff
						IF NOT sPed.bHasFoughtWithPlayer
						
							IF IS_PED_A_FOREMAN(ePed)
								strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_MELE_1")
								IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 500)
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000)
								ENDIF
							ELSE
								SWITCH ePed 
									CASE mpf_bugstar_front_1
										strRoot = "SOL1_FIGHT1"
									BREAK
									CASE mpf_bugstar_front_2
										strRoot = "SOL1_FIGHT2"
									BREAK
								ENDSWITCH
								IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 500)
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000)
								ENDIF
							ENDIF
							sPed.bHasFoughtWithPlayer = TRUE
						
						// Player returns after being chased off
						ELSE
							IF IS_PED_A_FOREMAN(ePed)
								strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_MELE_2")
								IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 500)
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000)
								ENDIF
							ELSE
								SWITCH ePed 
									CASE mpf_bugstar_front_1
										strRoot = "SOL1_FIGHT1"
									BREAK
									CASE mpf_bugstar_front_2
										strRoot = "SOL1_FIGHT2"
									BREAK
								ENDSWITCH
								IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 500)
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000)
								ENDIF
							ENDIF
						ENDIF
						
					ENDIF
				BREAK
				CASE HASH("FIGHT_ESC")
				
					// Jump back to working state
					IF GET_GAME_TIMER() - sPed.iTimeOfEnteringState > 4000
						SET_PED_AI_STATE(sPed, PEDAI_WORKING, FALSE, "BACK_TO_WORK")
					ENDIF
				
					IF HAS_PED_AI_JUST_ENTERED_STATE(sPed)
						IF IS_PED_A_FOREMAN(ePed)
							strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_MELE_ESC")
							PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 500)
						ENDIF
					ENDIF
					
				BREAK
				CASE HASH("STEALING_VAN")
				
					IF GET_GAME_TIMER() - sPed.iTimeOfEnteringState > 2000
					AND sPed.bCanSeePlayer
					AND NOT IS_PED_IN_CURRENT_CONVERSATION(sPed.id)
					AND e_PlayerStatus != PLAYERSTATUS_VAN_STEALING
					AND e_PlayerStatus != PLAYERSTATUS_VAN_STEALING_TOW
					
						SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, FALSE, "EXIT_VAN", TRUE, FALSE)
					
					ENDIF
					
					// Ped first realises the van is being stolen
					IF HAS_PED_AI_JUST_ENTERED_STATE(sPed)
						
						IF IS_PED_A_FOREMAN(ePed)
							strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_VAN_IN2")
							IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE)
								BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000, FALSE, TRUE)
							ELSE
								BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000, TRUE, TRUE)
							ENDIF
						ELSE
							SWITCH ePed 
								CASE mpf_bugstar_front_1
									strRoot = "SOL1_VAN1"
								BREAK
								CASE mpf_bugstar_front_2
									strRoot = "SOL1_VAN2"
								BREAK
							ENDSWITCH
							IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE)
								BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000, FALSE, TRUE)
							ELSE
								BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000, TRUE, TRUE)
							ENDIF
						ENDIF
						
					ELSE
						
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_TimeOfLastConvo > 2000
							IF IS_PED_A_FOREMAN(ePed)
								strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_VAN_TK")
								IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE)
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000, FALSE, TRUE)
								ENDIF
							ENDIF
						ENDIF
				
					ENDIF
				
				BREAK
				
				CASE HASH("GET_IN_VAN")
				
					IF HAS_PED_AI_JUST_ENTERED_STATE(sPed)
						
						IF IS_PED_A_FOREMAN(ePed)
							strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_VAN_IN1")
							IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE)
								BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000, FALSE, TRUE)
							ELSE
								BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000, TRUE, TRUE)
							ENDIF
						ELSE
							SWITCH ePed 
								CASE mpf_bugstar_front_1
									strRoot = "SOL1_VAN1"
								BREAK
								CASE mpf_bugstar_front_2
									strRoot = "SOL1_VAN2"
								BREAK
							ENDSWITCH
							IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE)
								BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000, FALSE, TRUE)
							ELSE
								BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000, TRUE, TRUE)
							ENDIF
						ENDIF
						
					ELSE
						
						IF IS_SAFE_TO_START_CONVERSATION()
						AND GET_GAME_TIMER() - i_TimeOfLastConvo > 2000
							IF IS_PED_A_FOREMAN(ePed)
								strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_VAN_TK")
								IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE)
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000, FALSE, TRUE)
								ELSE
									BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000, TRUE, TRUE)
								ENDIF
							ENDIF
						ENDIF
					
					ENDIF
				
				BREAK
				
				CASE HASH("EXIT_VAN")
				
					IF HAS_PED_AI_JUST_ENTERED_STATE(sPed)
						
						IF IS_PED_A_FOREMAN(ePed)
							strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_VAN_IN2")
							IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE)
								BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000, FALSE, TRUE)
							ELSE
								BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000, TRUE, TRUE)
							ENDIF
						ELSE
							SWITCH ePed 
								CASE mpf_bugstar_front_1
									strRoot = "SOL1_VAN1"
								BREAK
								CASE mpf_bugstar_front_2
									strRoot = "SOL1_VAN2"
								BREAK
							ENDSWITCH
							IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE)
								BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000, FALSE, TRUE)
							ELSE
								BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 25.0, 3000, 1000, TRUE, TRUE)
							ENDIF
						ENDIF
				
					ENDIF
				
				BREAK
				
			ENDSWITCH
		
		BREAK
		
		CASE PEDAI_CONFRONTING_PLAYER

			IF GET_GAME_TIMER() - sPed.iTimeOfStateLastSet > 5000
			AND NOT IS_PED_IN_CURRENT_CONVERSATION(sPed.id)
				// Go back to work
				IF ePed = mpf_bugstar_foreman_rear
					PLAY_CONVERSATION(ePed, "JS_STOCK2", PEDAI_WORKING, FALSE, FALSE, TRUE, 1000)
					iStockCheckListProg = 0
				ENDIF
				
				SET_PED_AI_STATE(sPed, PEDAI_WORKING, FALSE, "BACK_TO_WORK")
				
			ELSE
			
				IF sPed.bCanSeePlayer
					
					// Hasn't set this state for at least a second so player is assumed to have left, change AI event to leave version
					IF GET_GAME_TIMER() - sPed.iTimeOfStateLastSet > 1000
					AND NOT IS_PED_IN_CURRENT_CONVERSATION(sPed.id)
						IF ARE_STRINGS_EQUAL(sPed.strAIEvent, "ENT_FOOT")
						OR ARE_STRINGS_EQUAL(sPed.strAIEvent, "ENT_VEH")
						OR ARE_STRINGS_EQUAL(sPed.strAIEvent, "WH_FOOT")
						OR ARE_STRINGS_EQUAL(sPed.strAIEvent, "WH_VEH")
							TEXT_LABEL_31 strTemp 
							strTemp = sPed.strAIEvent
							strTemp += "_LEAVE"
							SET_PED_AI_STATE(sPed, PEDAI_CONFRONTING_PLAYER, FALSE, strTemp)
						ENDIF
					ENDIF
					
					// Player still hasn't left the warehouse after all the warnings
					IF ARE_STRINGS_EQUAL(sPed.strAIEvent, "WH_FOOT")
					OR ARE_STRINGS_EQUAL(sPed.strAIEvent, "WH_VEH")
					OR ARE_STRINGS_EQUAL(sPed.strAIEvent, "WH_DITCH_VEH")
					AND NOT IS_PED_IN_CURRENT_CONVERSATION(sPed.id)
						
						IF sPed.iWarningsToLeaveWarehouse >= 6
						AND i_TimeOfLastConvo != -1
						AND GET_GAME_TIMER() - i_TimeOfLastConvo > 7000 			// GIVE THE PLAYER 7 seconds
						AND GET_GAME_TIMER() - sPed.iTimeOfStateLastSet = 0
							SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, FALSE, "WH_FAILED_LEAVE")
						ENDIF
					ENDIF
					
				ENDIF
				
				SWITCH GET_HASH_KEY(sPed.strAIEvent)
					CASE HASH("ENT_FOOT")										
						// Has just jumpeped into the confrontation state
						IF HAS_PED_AI_JUST_ENTERED_STATE(sPed)
						
							// First geets player at the entrance
							IF NOT sPed.bAcquaintedPlayer

								IF IS_PED_A_FOREMAN(ePed)
									IF NOT sPed.bThinksPlayerIsPolice
										strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_F1")
										PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, FALSE, FALSE, 500)
									ELSE
										strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_F1_P")
										PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, FALSE, FALSE, 500)
									ENDIF
								ENDIF
								
							// Talk to the player upon returning to the site
							ELSE
							
								IF IS_PED_A_FOREMAN(ePed)
									IF NOT sPed.bThinksPlayerIsPolice
										strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_F2")
										PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 500)
									ELSE
										strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_F2_P")
										PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 500)
									ENDIF
								ENDIF
								
							ENDIF
						
							sPed.bAcquaintedPlayer = TRUE
						
						// Warns the player to leave the area
						ELSE
						
							IF IS_SAFE_TO_START_CONVERSATION()
							AND GET_GAME_TIMER() - i_TimeOfLastConvo > 5000
								IF IS_PED_A_FOREMAN(ePed)
									IF NOT sPed.bThinksPlayerIsPolice
										strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_F")
										PLAY_CONVERSATION(ePed, strRoot, sPed.eAI)
									ELSE
										strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_F_P")
										PLAY_CONVERSATION(ePed, strRoot, sPed.eAI)
									ENDIF
								ENDIF
							ENDIF
						
						ENDIF
					BREAK
					CASE HASH("ENT_VEH")
					
						// Has just jumped into the confrontation state
						IF HAS_PED_AI_JUST_ENTERED_STATE(sPed)
						OR sPed.iDiaFlag = 0
						
							// First geets player at the entrance
							IF NOT sPed.bAcquaintedPlayer

								IF IS_PED_A_FOREMAN(ePed)
									strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_V1")
									IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 500)
										sPed.iDiaFlag++
									ENDIF
								ENDIF
								
								sPed.bAcquaintedPlayer = TRUE
								
							ELSE
								IF ARE_STRINGS_EQUAL(sPed.strAIEventPrev, "WH_VEH")
									IF IS_PED_A_FOREMAN(ePed)
										strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_V")
										IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 1000)
											sPed.iDiaFlag++
										ENDIF
									ENDIF
								ELSE
									IF IS_PED_A_FOREMAN(ePed)
										strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_V3")
										IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE, FALSE, 1000)
											sPed.iDiaFlag++
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
						
						// Warns the player to leave the area
						ELSE
						
							IF IS_SAFE_TO_START_CONVERSATION()
							AND GET_GAME_TIMER() - i_TimeOfLastConvo > 5000
								IF IS_PED_A_FOREMAN(ePed)
									strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_V3")
									PLAY_CONVERSATION(ePed, strRoot, sPed.eAI)
								ENDIF
							ENDIF
						
						ENDIF
					
					BREAK
					CASE HASH("WH_FOOT")
					
						// Has just jumpeped into the confrontation state
						IF HAS_PED_AI_JUST_ENTERED_STATE(sPed)
						
							// First warns the player about being inside the warehouse
							IF sPed.iWarningsToLeaveWarehouse = 0

								IF IS_PED_A_FOREMAN(ePed)
									strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_WH1")
									IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE)
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 5.0, 3000, 500)
									ELSE
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 5.0, 3000, 500, TRUE)
									ENDIF
									sPed.iWarningsToLeaveWarehouse++
								ENDIF
								
							// player has already been warned before upon returning
							ELIF sPed.iWarningsToLeaveWarehouse > 0
							
								IF IS_PED_A_FOREMAN(ePed)
									strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_WH2")
									IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE)
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 5.0, 3000, 500)
									ELSE
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 5.0, 3000, 500, TRUE)
									ENDIF
									sPed.iWarningsToLeaveWarehouse++
								ENDIF
								
							ENDIF

							sPed.bAcquaintedPlayer = TRUE
						
						// Warns the player to leave the warehouse
						ELSE
						
							IF sPed.iWarningsToLeaveWarehouse < 6
						
								IF IS_SAFE_TO_START_CONVERSATION()
								AND GET_GAME_TIMER() - i_TimeOfLastConvo > 5000
									IF IS_PED_A_FOREMAN(ePed)
										// normal warning
										IF sPed.iWarningsToLeaveWarehouse < 5
											strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_WH")
										// issue final warning
										ELSE
											strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_WH_F")
										ENDIF
										IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE)
											BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 10.0, 3000, 500)
										ELSE
											BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 10.0, 3000, 500, TRUE)
										ENDIF
										sPed.iWarningsToLeaveWarehouse++
									ENDIF
								ENDIF

							ENDIF
						
						ENDIF
					
					BREAK

					CASE HASH("WH_DITCH_VEH")

						// Has just jumpeped into the confrontation state
						IF HAS_PED_AI_JUST_ENTERED_STATE(sPed)
						
							// First warns the player about being inside the warehouse
							IF sPed.iWarningsToLeaveWarehouse = 0

								IF IS_PED_A_FOREMAN(ePed)
									strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_WH1")
									IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE)
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 5.0, 3000, 500)
									ELSE
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 5.0, 3000, 500, TRUE)
									ENDIF
									sPed.iWarningsToLeaveWarehouse++
								ENDIF
								
							// player has already been warned before upon returning
							ELIF sPed.iWarningsToLeaveWarehouse > 0
							
								IF IS_PED_A_FOREMAN(ePed)
									strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_WH2")
									IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE)
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 5.0, 3000, 500)
									ELSE
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 5.0, 3000, 500, TRUE)
									ENDIF
									sPed.iWarningsToLeaveWarehouse++
								ENDIF
								
							ENDIF

							sPed.bAcquaintedPlayer = TRUE
						
						// Warns the player to leave the warehouse
						ELSE
						
							IF sPed.iWarningsToLeaveWarehouse < 6
						
								IF IS_SAFE_TO_START_CONVERSATION()
								AND GET_GAME_TIMER() - i_TimeOfLastConvo > 5000
									IF IS_PED_A_FOREMAN(ePed)
										// normal warning
										IF sPed.iWarningsToLeaveWarehouse < 5
											IF (sPed.iWarningsToLeaveWarehouse-1) % 2 = 0
												strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_V4")
											ELSE
												strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_V3")
											ENDIF
										// issue final warning
										ELSE
											strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_WH_F")
										ENDIF
										IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE)
											BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 10.0, 3000, 500)
										ELSE
											BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 10.0, 3000, 500, TRUE)
										ENDIF
										sPed.iWarningsToLeaveWarehouse++
									ENDIF
								ENDIF

							ENDIF
						
						ENDIF
		
					BREAK
					
					CASE HASH("WH_VEH")

						// Has just jumpeped into the confrontation state
						IF HAS_PED_AI_JUST_ENTERED_STATE(sPed)
						
							// First warns the player about being inside the warehouse
							IF sPed.iWarningsToLeaveWarehouse = 0

								IF IS_PED_A_FOREMAN(ePed)
									strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_WH1")
									IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE)
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 5.0, 3000, 500)
									ELSE
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 5.0, 3000, 500, TRUE)
									ENDIF
									sPed.iWarningsToLeaveWarehouse++
								ENDIF
								
							// player has already been warned before upon returning
							ELIF sPed.iWarningsToLeaveWarehouse > 0
							
								IF IS_PED_A_FOREMAN(ePed)
									strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_WH2")
									IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE)
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 5.0, 3000, 500)
									ELSE
										BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 5.0, 3000, 500, TRUE)
									ENDIF
									sPed.iWarningsToLeaveWarehouse++
								ENDIF
								
							ENDIF

							sPed.bAcquaintedPlayer = TRUE
						
						// Warns the player to leave the warehouse
						ELSE
						
							IF sPed.iWarningsToLeaveWarehouse < 6
						
								IF IS_SAFE_TO_START_CONVERSATION()
								AND GET_GAME_TIMER() - i_TimeOfLastConvo > 5000
									IF IS_PED_A_FOREMAN(ePed)
										// normal warning
										IF sPed.iWarningsToLeaveWarehouse < 5
											IF (sPed.iWarningsToLeaveWarehouse-1) % 2 = 0
												strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_V3")
											ELSE
												strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_WH")
											ENDIF
										// issue final warning
										ELSE
											strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_WH_F")
										ENDIF
										IF PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, TRUE, TRUE)
											BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, strRoot, 10.0, 3000, 500)
										ELSE
											BROADCAST_QUEUE_SCRIPT_AI_EVENT_PED(sPed, sPed.strAIEvent, "", 10.0, 3000, 500, TRUE)
										ENDIF
										sPed.iWarningsToLeaveWarehouse++
									ENDIF
								ENDIF

							ENDIF
						
						ENDIF
					
					BREAK
					CASE HASH("ENT_DITCH_VEH")
					// No broadcast events for ditching at the entrance
						IF sPed.iDiaFlag = 0
							IF IS_SAFE_TO_START_CONVERSATION()
							AND GET_GAME_TIMER() - i_TimeOfLastConvo > 1000
								IF IS_PED_A_FOREMAN(ePed)
									strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_V4")
									PLAY_CONVERSATION(ePed, strRoot, sPed.eAI)
								ENDIF
								sPed.iDiaFlag++
							ENDIF
						ELSE
							IF IS_SAFE_TO_START_CONVERSATION()
							AND GET_GAME_TIMER() - i_TimeOfLastConvo > 5000
								IF (sPed.iDiaFlag-1) % 3 = 0
									strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_V4")
								ELSE
									strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_V3")
								ENDIF
								PLAY_CONVERSATION(ePed, strRoot, sPed.eAI)
								sPed.iDiaFlag++
							ENDIF
						ENDIF
					
					BREAK
					CASE HASH("ENT_FOOT_LEAVE")

						IF sPed.iDiaFlag = 0
							// Player has left the area
							IF IS_SAFE_TO_START_CONVERSATION()
							AND GET_GAME_TIMER() - i_TimeOfLastConvo > 1000
								IF IS_PED_A_FOREMAN(ePed)
									IF NOT sPed.bThinksPlayerIsPolice
										strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_F3")
										PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, FALSE, FALSE)
									ELSE
										strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_F3_P")
										PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, FALSE, FALSE)
									ENDIF
								ENDIF
								sPed.iDiaFlag++
							ENDIF
						ENDIF
					
					BREAK
					CASE HASH("ENT_VEH_LEAVE")
					CASE HASH("WH_FOOT_LEAVE")
					CASE HASH("WH_VEH_LEAVE")
					
						IF sPed.iDiaFlag = 0
							// Player has left the area
							IF IS_SAFE_TO_START_CONVERSATION()
							AND GET_GAME_TIMER() - i_TimeOfLastConvo > 1000
								IF IS_PED_A_FOREMAN(ePed)
									strRoot = MODIFY_CONVO_ROOT_FOR_FOREMAN(ePed, "JS_ENT_V5")
									PLAY_CONVERSATION(ePed, strRoot, sPed.eAI, FALSE, FALSE)
								ENDIF
								sPed.iDiaFlag++
							ENDIF
						ENDIF
					
					BREAK
				ENDSWITCH
			
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

PROC BUGSTAR_STAFF_AI_TASKS(PED_STRUCT &sPed)
	
	SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(sPed.id)
	IF HAS_PED_AI_JUST_CHANGED_STATE(sPed)
		sPed.iFlagTask = 0
	ENDIF
		
	SWITCH sPed.eAI
	
		CASE PEDAI_WORKING
			
			MISSION_PED_FLAGS ePedFlag, ePed
			REPEAT COUNT_OF(peds) ePed
				IF peds[ePed].id = sPed.id
					ePedFlag = ePed
				ENDIF
			ENDREPEAT
			
			IF ePedFlag != mpf_invalid

				SWITCH ePedFlag
					CASE mpf_bugstar_foreman_front
					
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS)
							IF DOES_SCENARIO_EXIST_IN_AREA(<<155.74, -3098.89, 4.93>>, 0.5, TRUE)
								// go back to the mobile phone scenario
								TASK_USE_NEAREST_SCENARIO_TO_COORD(sPed.id, <<155.74, -3098.89, 4.93>>, 0.5, -1)
							ENDIF
						ENDIF
					
					BREAK
					CASE mpf_bugstar_foreman_rear
								
						IF (NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_PERFORM_SEQUENCE)
						OR HAS_PED_AI_JUST_CHANGED_STATE(sPed))
						AND HAS_ANIM_DICT_LOADED("misslsdhsclipboard@base")
						
							IF NOT IS_ENTITY_PLAYING_ANIM(sPed.id, "misslsdhsclipboard@base", "base")
							
								// ped goes to the back of the van
								OPEN_SEQUENCE_TASK(seq_main)
									IF NOT ARE_VECTORS_ALMOST_EQUAL(GET_ENTITY_COORDS(sPed.id), <<126.8496, -3089.2493, 4.9141>>, 0.75)
										TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<126.8496, -3089.2493, 4.9141>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 0.5, ENAV_DEFAULT, 264.2812)
									ENDIF
									TASK_ACHIEVE_HEADING(null, 264.2812)
									TASK_PLAY_ANIM(null, "misslsdhsclipboard@base", "base", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
								CLOSE_SEQUENCE_TASK(seq_main)
								TASK_PERFORM_SEQUENCE(sPed.id, seq_main)
								CLEAR_SEQUENCE_TASK(seq_main)
							
							ENDIF
							
							IF DOES_ENTITY_EXIST(vehs[mvf_bugstar_van_3].id)
								TASK_LOOK_AT_COORD(sPed.id, GET_WORLD_POSITION_OF_ENTITY_BONE(vehs[mvf_bugstar_van_3].id, GET_ENTITY_BONE_INDEX_BY_NAME(vehs[mvf_bugstar_van_3].id, "engine")), -1, SLF_DEFAULT, SLF_LOOKAT_MEDIUM)
							ENDIF
							
							sPed.iFlagTask = 0
						
						ELIF HAS_ANIM_DICT_LOADED("misslsdhsclipboard@idle_a")
						
							SWITCH sPed.iFlagTask 
								CASE 0 FALLTHRU
								CASE 2
								
									// Mark the clipboard prop ready for recreation
									IF sPed.iFlagTask = 0 
									AND GET_SEQUENCE_PROGRESS(sPed.id) = 2
										sPed.bSafeToCreateProp = TRUE
									ENDIF
								
									IF ((sPed.iFlagTask = 0 AND GET_SEQUENCE_PROGRESS(sPed.id) = 2)
									OR sPed.iFlagTask = 2)
									AND GET_GAME_TIMER() > sPed.iTimeOfEnteringState
										
										OPEN_SEQUENCE_TASK(seq_main)
											TEXT_LABEL_23 strAnim
											SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
												CASE 0		strAnim = "idle_a"		BREAK
												CASE 1		strAnim = "idle_b"		BREAK
												CASE 2		strAnim = "idle_c"		BREAK
											ENDSWITCH
											TASK_PLAY_ANIM(null, "misslsdhsclipboard@idle_a", strAnim, 	NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
											TASK_PLAY_ANIM(null, "misslsdhsclipboard@base", "base", 	NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
										CLOSE_SEQUENCE_TASK(seq_main)
										TASK_PERFORM_SEQUENCE(sPed.id, seq_main)
										CLEAR_SEQUENCE_TASK(seq_main)
										
										sPed.iFlagTask = 1
									ENDIF
								BREAK
								CASE 1
									IF GET_SEQUENCE_PROGRESS(sPed.id) = 1
										sPed.iFlagTask++
									ENDIF
								BREAK
							ENDSWITCH
							
						ENDIF
						
					BREAK
					CASE mpf_bugstar_front_1		FALLTHRU
					CASE mpf_bugstar_front_2
						
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS)
							IF DOES_SCENARIO_EXIST_IN_AREA(<<159.82, -3085.93, 5.00>>, 0.1, TRUE)
								TASK_USE_NEAREST_SCENARIO_TO_COORD(sPed.id, <<159.82, -3085.93, 5.00>>, 0.1, -1)

							ELIF DOES_SCENARIO_EXIST_IN_AREA(<<160.15, -3084.79, 4.99>>, 0.1, TRUE)
								TASK_USE_NEAREST_SCENARIO_TO_COORD(sPed.id,  <<160.15, -3084.79, 4.99>>, 0.1, -1)
								
							ELIF DOES_SCENARIO_EXIST_IN_AREA(<<159.09, -3085.02, 5.01>>, 0.1, TRUE)
								TASK_USE_NEAREST_SCENARIO_TO_COORD(sPed.id,  <<159.09, -3085.02, 5.01>>, 0.1, -1)
							
							ENDIF
						ENDIF
						
					BREAK
				ENDSWITCH
			
			ENDIF
		
		BREAK
		
		CASE PEDAI_INVESTIGATE
		
			SWITCH GET_HASH_KEY(sPed.strAIEvent)
				CASE HASH("HEARD_VAN")
				CASE HASH("CHECK_DRIVER")
				CASE HASH("SEE_VAN_LEAVING")
				CASE HASH("SEE_VAN_LEAVING_1")

					IF DOES_ENTITY_EXIST(sPed.entityInvestigating)
					
						IF NOT IS_PED_HEADING_TOWARDS_POSITION(sPed.id, GET_ENTITY_COORDS(sPed.entityInvestigating), 180)
						AND GET_GAME_TIMER() - sPed.iTimeOfEnteringState < 5000 // not faced it after 5 seconds just walk to target
						
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
							OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)

								TASK_TURN_PED_TO_FACE_ENTITY(sPed.id, sPed.entityInvestigating, -1)
								TASK_LOOK_AT_ENTITY(sPed.id, sPed.entityInvestigating, -1, SLF_WHILE_NOT_IN_FOV)

							ENDIF
						
						// begin walk over to the entity once facing it
						ELSE
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_GOTO_ENTITY_OFFSET)
							OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)
								TASK_GOTO_ENTITY_OFFSET_XY(sPed.id, sPed.entityInvestigating, DEFAULT_TIME_NEVER_WARP, 2.0, -1.0, 2.5, PEDMOVE_WALK, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
							ENDIF	
						ENDIF

					ENDIF
				
				BREAK
				CASE HASH("HEARD_PLAYER")
				
					BOOL bRetask
				
					// Update last heard at coord
					IF sPed.bCanHearPlayer
					AND NOT ARE_VECTORS_ALMOST_EQUAL(sPed.vInvestigationCoord, sPed.vLastPosPlayerHeardAt, 1.0)	// only update last heard coord every 1m
						sPed.vInvestigationCoord = sPed.vLastPosPlayerHeardAt
						bRetask = TRUE	// coord updated so retask the ped with the new coord
						
						CPRINTLN(DEBUG_MISSION, "INVESTIGATION COORD UPDATE vInvestigationCoord:", sPed.vInvestigationCoord, " vLastPosPlayerHeardAt:", sPed.vLastPosPlayerHeardAt)
					ENDIF
					
					IF NOT IS_VECTOR_ZERO(sPed.vInvestigationCoord)
					
						// turn the ped around
						IF NOT IS_PED_HEADING_TOWARDS_POSITION(sPed.id, sPed.vInvestigationCoord, 90)
						AND GET_GAME_TIMER() - sPed.iTimeOfEnteringState < 5000
						
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_TURN_PED_TO_FACE_COORD)
							OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)
							OR bRetask
						
								TASK_TURN_PED_TO_FACE_COORD(sPed.id, sPed.vInvestigationCoord)
								TASK_LOOK_AT_COORD(sPed.id, sPed.vInvestigationCoord, -1, SLF_WHILE_NOT_IN_FOV)

							ENDIF
						
						// begin walk over to the sound
						ELSE
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
							OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)
							OR bRetask
								TASK_FOLLOW_NAV_MESH_TO_COORD(sPed.id, sPed.vLastPosPlayerHeardAt, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 3.0)
							ENDIF	
						ENDIF

					ENDIF
				
				BREAK
				CASE HASH("HEARD_ALARM")
					
				
				BREAK
				
				CASE HASH("SOUND_LOST")
				
					// Stand and look at the van driving off
					IF NOT IS_VECTOR_ZERO(sPed.vInvestigationCoord)
				
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_TURN_PED_TO_FACE_COORD)
						OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)
							TASK_TURN_PED_TO_FACE_COORD(sPed.id, sPed.vInvestigationCoord, -1)
							TASK_LOOK_AT_COORD(sPed.id, sPed.vInvestigationCoord, -1, SLF_WHILE_NOT_IN_FOV)
						ENDIF
						
					// no van stored, just stand still
					ELSE
						TASK_STAND_STILL(sPed.id, -1)
					ENDIF
				
				
				BREAK
				CASE HASH("VAN_LOST")
				
					// Stand and look at the van driving off
					IF DOES_ENTITY_EXIST(sPed.entityInvestigating)
				
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
						OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)
							TASK_TURN_PED_TO_FACE_ENTITY(sPed.id, sPed.entityInvestigating, -1)
							TASK_LOOK_AT_ENTITY(sPed.id, sPed.entityInvestigating, -1, SLF_WHILE_NOT_IN_FOV)
						ENDIF
						
					// no van stored, just stand still
					ELSE
						TASK_STAND_STILL(sPed.id, -1)
					ENDIF
				
				BREAK
				
			ENDSWITCH
		
		BREAK
	
		CASE PEDAI_CONFRONTING_PLAYER	

			IF ARE_STRINGS_EQUAL(sPed.strAIEvent, "ENT_FOOT_LEAVE")
			OR ARE_STRINGS_EQUAL(sPed.strAIEvent, "ENT_VEH_LEAVE")
			OR ARE_STRINGS_EQUAL(sPed.strAIEvent, "WH_FOOT_LEAVE")
			OR ARE_STRINGS_EQUAL(sPed.strAIEvent, "WH_VEH_LEAVE")
						
				// Stop following and stand and watch player
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_PERFORM_SEQUENCE)
				OR NOT IS_PED_FACING_PED(sPed.id, PLAYER_PED_ID(), 90)
				OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)
				
					OPEN_SEQUENCE_TASK(seq_main)
						TASK_TURN_PED_TO_FACE_ENTITY(null, PLAYER_PED_ID())
						TASK_STAND_STILL(null, 2000)
						SET_SEQUENCE_TO_REPEAT(seq_main, REPEAT_FOREVER)
					CLOSE_SEQUENCE_TASK(seq_main)
					TASK_PERFORM_SEQUENCE(sPed.id, seq_main)
					CLEAR_SEQUENCE_TASK(seq_main)
					
					TASK_LOOK_AT_ENTITY(sPed.id, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
				
				ENDIF
			
			ELSE
			
				ENTITY_INDEX entityTarget
				FLOAT fDistFromTarget
				FLOAT fPlayerDistFromVehicle
				FLOAT fMoveBlendRatio
								
				IF DOES_ENTITY_EXIST(veh_DumpedAtEntrance)
				AND IS_VEHICLE_DRIVEABLE(veh_DumpedAtEntrance)
					fPlayerDistFromVehicle = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), veh_DumpedAtEntrance)
				ENDIF
				
				IF e_PlayerStatus = PLAYERSTATUS_WAREHOUSE_IN_VEHICLE
				OR e_PlayerStatus = PLAYERSTATUS_WAREHOUSE_ON_FOOT
				OR e_PlayerStatus = PLAYERSTATUS_VAN_DAMAGE

					entityTarget		= PLAYER_PED_ID()
					fMoveBlendRatio 	= PEDMOVE_RUN
					
				ELIF e_PlayerStatus = PLAYERSTATUS_BLOCKING_ENTRANCE_ON_FOOT
				OR e_PlayerStatus = PLAYERSTATUS_BLOCKING_ENTRANCE_WITH_VEHICLE
				
					entityTarget		= PLAYER_PED_ID()
					fMoveBlendRatio 	= PEDMOVE_WALK
				
				ELIF e_PlayerStatus = PLAYERSTATUS_WAREHOUSE_DITCH_VEH
				AND DOES_ENTITY_EXIST(veh_DumpedAtEntrance)
				
					IF fPlayerDistFromVehicle > 10.0
						entityTarget		= PLAYER_PED_ID()
						fMoveBlendRatio 	= PEDMOVE_RUN
					ELSE
						entityTarget		= veh_DumpedAtEntrance
						fMoveBlendRatio 	= PEDMOVE_RUN
					ENDIF
					
				ELIF e_PlayerStatus = PLAYERSTATUS_ENTRANCE_DITCH_VEH
				AND DOES_ENTITY_EXIST(veh_DumpedAtEntrance)

					entityTarget		= veh_DumpedAtEntrance
					fMoveBlendRatio 	= PEDMOVE_WALK
					
				ENDIF
				
				IF DOES_ENTITY_EXIST(entityTarget)
					fDistFromTarget = GET_DISTANCE_BETWEEN_ENTITIES(sPed.id, entityTarget)
				ENDIF
			
				// Track the player while close
				IF fDistFromTarget <= 7.5
								
					IF (NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_PERFORM_SEQUENCE)
					AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_GO_TO_ENTITY))
					OR NOT IS_PED_FACING_PED(sPed.id, PLAYER_PED_ID(), 90)
					OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)
					
						OPEN_SEQUENCE_TASK(seq_main)
							TASK_TURN_PED_TO_FACE_ENTITY(null, PLAYER_PED_ID())
							TASK_STAND_STILL(null, 2000)
							SET_SEQUENCE_TO_REPEAT(seq_main, REPEAT_FOREVER)
						CLOSE_SEQUENCE_TASK(seq_main)
						TASK_PERFORM_SEQUENCE(sPed.id, seq_main)
						CLEAR_SEQUENCE_TASK(seq_main)
						
						TASK_LOOK_AT_ENTITY(sPed.id, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
					
					ENDIF
				
				// Follow the player around
				ELSE
				
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_GO_TO_ENTITY)
					OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)

						FLOAT fRadius
						IF fMoveBlendRatio > PEDMOVE_WALK
							fRadius = 6.0
						ELSE
							fRadius = 3.0
						ENDIF
						TASK_GO_TO_ENTITY(sPed.id, entityTarget, DEFAULT_TIME_NEVER_WARP, fRadius, fMoveBlendRatio)					
						TASK_LOOK_AT_ENTITY(sPed.id, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
						
					ENDIF
					
				ENDIF
			
			ENDIF
		
		BREAK		
		
		CASE PEDAI_COMBAT_PLAYER
		
			SWITCH GET_HASH_KEY(sPed.strAIEvent)
				CASE HASH("FIGHT")
				CASE HASH("FIGHT_RETURN")
				CASE HASH("RAN_OVER")
				CASE HASH("WH_FAILED_LEAVE")
				CASE HASH("STEALING_VAN")
				CASE HASH("GET_IN_VAN")
				CASE HASH("EXIT_VAN")
				CASE HASH("PLAYER_KO")
				
					WEAPON_TYPE weapTemp
					IF GET_CURRENT_PED_WEAPON(sPed.id, weapTemp)
						IF weapTemp != WEAPONTYPE_UNARMED
							SET_CURRENT_PED_WEAPON(sPed.id, WEAPONTYPE_UNARMED)
						ENDIF
					ENDIF
					
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_COMBAT)

						TASK_COMBAT_PED(sPed.id, PLAYER_PED_ID())
						
					ENDIF
				
				BREAK
				CASE HASH("FIGHT_ESC")
				
					// Stop following and stand and watch player
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_PERFORM_SEQUENCE)
					OR NOT IS_PED_FACING_PED(sPed.id, PLAYER_PED_ID(), 90)
					OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)
					
						OPEN_SEQUENCE_TASK(seq_main)
							TASK_TURN_PED_TO_FACE_ENTITY(null, PLAYER_PED_ID())
							TASK_STAND_STILL(null, 2000)
							SET_SEQUENCE_TO_REPEAT(seq_main, REPEAT_FOREVER)
						CLOSE_SEQUENCE_TASK(seq_main)
						TASK_PERFORM_SEQUENCE(sPed.id, seq_main)
						CLEAR_SEQUENCE_TASK(seq_main)
						
						TASK_LOOK_AT_ENTITY(sPed.id, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
					
					ENDIF
				
				BREAK
			ENDSWITCH

		BREAK
		
		CASE PEDAI_FLEEING
		
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(sPed.id, <<177.7484, -3240.8967, 4.6079>>) < 15.0
			
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_SMART_FLEE_PED)
					TASK_SMART_FLEE_PED(sPed.id, PLAYER_PED_ID(), 1000.0, -1)
				ENDIF
			
			ELSE
		
				//IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_REACT_AND_FLEE_PED)
				IF (NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_SMART_FLEE_PED)
				AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD))
				OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)
					//TASK_SMART_FLEE_PED(sPed.id, PLAYER_PED_ID(), 1000.0, -1)
					//TASK_REACT_AND_FLEE_PED(sPed.id, PLAYER_PED_ID())
					TASK_FOLLOW_NAV_MESH_TO_COORD(sPEd.id, <<177.7484, -3240.8967, 4.6079>>, PEDMOVE_SPRINT, DEFAULT_TIME_NEVER_WARP, DEFAULT, ENAV_NO_STOPPING)
				ENDIF
			
			ENDIF
		
		BREAK
		
		CASE PEDAI_HANDSUP
		
			SWITCH GET_HASH_KEY(sPed.strAIEvent)
				CASE HASH("GUN_AIMED_AT")
			
					IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_HANDS_UP)
					OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)
						TASK_HANDS_UP(sPed.id, -1, PLAYER_PED_ID(), 0, HANDS_UP_STRAIGHT_TO_LOOP)
					ENDIF
			
				BREAK
			ENDSWITCH
		
		BREAK
		
	ENDSWITCH

ENDPROC

PROC SECURITY_AI_STATE(PED_STRUCT &sPed)

		CONST_FLOAT			NOTICE_WEAPON_DIST				8.0

//-------------------------------------------------------------------------------------
// AI pre-update 
//-------------------------------------------------------------------------------------

	WEAPON_TYPE eWeapPlayer
	GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eWeapPlayer)
	
	FLOAT fDistPlayer = GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sPed.id)
	
	BOOL bPunchedPed, bHitWithVehicle
	
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ANY_VEHICLE(sPed.id)
		bHitWithVehicle = TRUE
	ENDIF
	
	IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(sPed.id, PLAYER_PED_ID(), TRUE)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(sPed.id, WEAPONTYPE_UNARMED)
			bPunchedPed = TRUE
		ENDIF
	ENDIF

//-------------------------------------------------------------------------------------
// AI Event update
//-------------------------------------------------------------------------------------
	
	BOOL bCanSeeBody, bKilledInFrontOfObserver, bSawPlayerDoIt, bNonLethal
	bCanSeeBody = CAN_PED_SEE_DEAD_BODY(sPed, bKilledInFrontOfObserver, bSawPlayerDoIt, bNonLethal)
	
	// Saw a ped get killed infront of them
	IF bCanSeeBody
	AND bKilledInFrontOfObserver
	AND NOT bNonLethal
	
		IF bSawPlayerDoIt
			SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "PLAYER_KILLED")
		ELSE
			SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "KILLED")
		ENDIF
	
	// Ped heard gun fire
	ELIF HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_SHOT_FIRED)
	OR HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_SHOT_FIRED_BULLET_IMPACT)
	OR HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_SHOT_FIRED_WHIZZED_BY)
	OR HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_EXPLOSION)
	OR HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_EXPLOSION_HEARD)
	OR HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_SHOCKING_EXPLOSION)
	
		SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "GUNSHOT")
	
	// Aiming gun around
	ELIF sPed.bCanSeePlayer 
	AND IS_PLAYER_AIMING(WF_INCLUDE_GUN)
	
		SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "GUN_AIMED")
	
	// Aiming projectile at ped
	ELIF sPed.bCanSeePlayer AND IS_PLAYER_AIMING_AT_PED(sPed, WF_INCLUDE_PROJECTILE)
	
		SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "PROJ_AIMED")
		
	// Can see player with a gun
	ELIF sPed.bCanSeePlayer
	AND fDistPlayer < NOTICE_WEAPON_DIST
	AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
	AND IS_PED_ARMED(PLAYER_PED_ID(), WF_INCLUDE_MELEE | WF_INCLUDE_GUN)
	
		SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "SEEN_WEAPON")
		
	// Player is stealing the security guard's veh
	ELIF (sPed.bCanSeePlayer OR sPed.bCanHearPlayer)
	AND e_PlayerStatus = PLAYERSTATUS_SEC_CAR_STEALING
	
		IF sPed.bCanSeePlayer
			SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "STEALING_SEC_CAR")
		ELIF sPed.bCanHearPlayer
			SET_PED_AI_STATE_WITH_ENTITY(sPed, PEDAI_INVESTIGATE, "HEARD_SEC_CAR", vehs[mvf_security].id, TRUE)
		ENDIF

	// Player is seen getting into a van
	ELIF (sPed.bCanSeePlayer OR sPed.bCanHearPlayer)
	AND e_PlayerStatus = PLAYERSTATUS_VAN_ENTERING
	
		IF sPed.bCanSeePlayer
			SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "GET_IN_VAN")
		ELIF sPed.bCanHearPlayer
			SET_PED_AI_STATE_WITH_ENTITY(sPed, PEDAI_INVESTIGATE, "HEARD_VAN", vehs[GET_VAN_PLAYER_IS_IN()].id, TRUE)
		ENDIF
		
	// player is seen towing a van away
	ELIF (sPed.bCanSeePlayer OR sPed.bCanHearPlayer)
	AND e_PlayerStatus = PLAYERSTATUS_VAN_STEALING_TOW
		
		IF sPed.bCanSeePlayer
			SET_PED_AI_STATE_WITH_ENTITY(sPed, PEDAI_COMBAT_PLAYER, "STEALING_VAN", vehs[GET_VAN_PLAYER_IS_TOWING()].id, TRUE)
		ELIF sPed.bCanHearPlayer
			SET_PED_AI_STATE_WITH_ENTITY(sPed, PEDAI_INVESTIGATE, "HEARD_VAN", vehs[GET_VAN_PLAYER_IS_TOWING()].id, TRUE)
		ENDIF

	// Starting a fight with a ped
	ELIF bPunchedPed
	OR HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_MELEE_ACTION)
	OR HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_SHOCKING_SEEN_MELEE_ACTION)
	OR CAN_PED_SEE_A_PED_BEING_ATTACKED(sPed, FALSE)

		SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "FIGHT")
		
	// Ran over
	ELIF bHitWithVehicle
	OR HAS_PED_RECEIVED_EVENT(sPed.id, EVENT_SHOCKING_PED_RUN_OVER)
	OR CAN_PED_SEE_A_PED_BEING_ATTACKED(sPed, TRUE)
	
		SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "RAN_OVER")
		
	// After having a scrap with the player spots him again near the warehouse or near the ped
	ELIF sPed.eAI != PEDAI_COMBAT_PLAYER
	AND sPed.bHasFoughtWithPlayer
	AND (sPed.bCanSeePlayer OR sPed.bCanHearPlayer)
	AND (fDistPlayer < 8.0	OR GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_WarehouseCoord) < 40.0)
	
		IF sPed.bCanSeePlayer
			SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "FIGHT_RETURN")
		ELIF sPed.bCanHearPlayer
			SET_PED_AI_STATE_WITH_COORDS(sPed, PEDAI_INVESTIGATE,  "HEARD_PLAYER", GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE)
		ENDIF
		
	// Seed another ped knocked out
	ELIF sPed.eAI != PEDAI_FLEEING
	AND bCanSeeBody	

		// KO's infront of ped by player
		IF bKilledInFrontOfObserver
		AND bSawPlayerDoIt
		AND bNonLethal
			SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "PLAYER_KO")
			
		// otherwise count as found dead body (bSawPlayerDoIt will be forced to true if the player was observed doing it before)
		ELIF NOT bSawPlayerDoIt
			SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, "DEAD_BODY")
		ENDIF

	ENDIF
	
//-------------------------------------------------------------------------------------
// CHECK FOR BROADCAST EVENT
//-------------------------------------------------------------------------------------

	IF sPed.iReceivedEvent > -1 
	AND sPed.iReceivedEvent < COUNT_OF(s_BroadcastEvent)-1
	
		BOOL bCameFromAlarm = s_BroadcastEvent[sPed.iReceivedEvent].bRaiseAlarm
	
		SWITCH GET_HASH_KEY(s_BroadcastEvent[sPed.iReceivedEvent].strEventName)
			CASE HASH("KILLED")
			CASE HASH("PLAYER_KILLED")
			CASE HASH("DEAD_BODY")
			CASE HASH("PLAYER_KO")
			CASE HASH("GUN_AIMED_AT")
			CASE HASH("PROJ_AIMED")
			CASE HASH("SEEN_WEAPON")
			CASE HASH("FIGHT")
			CASE HASH("FIGHT_RETURN")
			CASE HASH("RAN_OVER")
			CASE HASH("STEALING_VAN")
			CASE HASH("GET_IN_VAN")
			CASE HASH("GUNSHOT")
			CASE HASH("STEALING_SEC_CAR")
				SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, TRUE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, FALSE, bCameFromAlarm)
			BREAK
			CASE HASH("HEARD_PLAYER")
				SET_PED_AI_STATE_WITH_COORDS(sPed, PEDAI_INVESTIGATE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, GET_ENTITY_COORDS(PLAYER_PED_ID()), TRUE, FALSE, bCameFromAlarm)
			BREAK
			CASE HASH("HEARD_VAN")
			CASE HASH("HEARD_SEC_CAR")
				SET_PED_AI_STATE_WITH_ENTITY(sPed, PEDAI_INVESTIGATE, s_BroadcastEvent[sPed.iReceivedEvent].strEventName, vehs[GET_VAN_PLAYER_IS_IN()].id, TRUE, FALSE, bCameFromAlarm)
			BREAK
		ENDSWITCH
	ENDIF
	
	sPed.iReceivedEvent = -1

//-------------------------------------------------------------------------------------
// 	 MANAGE STATES
//-------------------------------------------------------------------------------------

	// Check for state update again
	IF sPed.eAIPrevFrame != sPed.eAI
		sPed.bAIStateChangedLastFrame 			= TRUE
	ELSE
		sPed.bAIStateChangedLastFrame 			= FALSE
	ENDIF	
	sPed.eAIPrevFrame = sPed.eAI
	
	IF NOT ARE_STRINGS_EQUAL(sPed.strAIEventPrevFrame, sPed.strAIEvent)
		sPed.bAIEventChangedLastFrame			= TRUE
	ELSE
		sPed.bAIEventChangedLastFrame			= FALSE
	ENDIF
	sPed.strAIEventPrevFrame = sPed.strAIEvent

	SWITCH sPed.eAI
	
		CASE PEDAI_WORKING


		BREAK
		
		CASE PEDAI_INVESTIGATE
		
			SWITCH GET_HASH_KEY(sPed.strAIEvent)
				
				CASE HASH("VAN_LOST")
					
					// Go back to work after 3 seconds
					IF GET_GAME_TIMER() - sPed.iTimeOfEnteringState > 3000
						SET_PED_AI_STATE(sPed, PEDAI_WORKING, FALSE, "BACK_TO_WORK")
					ENDIF
				
				BREAK
				
				CASE HASH("HEARD_PLAYER")
				
					// Has reached investigation coord
					IF GET_GAME_TIMER() - sPed.iTimeOfEnteringState > 4000	// give the ped chance to turn and look, safety for if the coord is very close to the ped
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(sPed.id, sPed.vInvestigationCoord) < 3.0
					AND IS_PED_STILL(sPed.id)

						SET_PED_AI_STATE(sPed, PEDAI_INVESTIGATE, FALSE, "SOUND_LOST")
					
					ENDIF
					
				BREAK
				CASE HASH("SOUND_LOST")
										
					IF GET_GAME_TIMER() - sPed.iTimeOfEnteringState > 4000
					AND NOT IS_PED_IN_CURRENT_CONVERSATION(sPed.id)
						
						SET_PED_AI_STATE(sPed, PEDAI_WORKING, FALSE, "BACK_TO_WORK")
					
					ENDIF
					
				BREAK
			ENDSWITCH
		BREAK
		
		CASE PEDAI_COMBAT_PLAYER
		
			SWITCH GET_HASH_KEY(sPed.strAIEvent)
				CASE HASH("FIGHT")
				CASE HASH("FIGHT_RETURN")
				CASE HASH("RAN_OVER")
				
					// Player has ran out and the delay has passed
					IF GET_GAME_TIMER() - sPed.iTimeOfStateLastSet > 8000
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_WarehouseCoord) > 65
					AND (GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sPed.id) > 10.0
						OR GET_GAME_TIMER() - sPed.iTimeOfStateLastSet > 16000)
					AND NOT IS_PED_IN_CURRENT_CONVERSATION(sPed.id)
					
						SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, FALSE, "FIGHT_ESC")
						
					ENDIF
				BREAK
				
				CASE HASH("KILLED")
				CASE HASH("PLAYER_KILLED")
				CASE HASH("DEAD_BODY")
				CASE HASH("PLAYER_KO")
				CASE HASH("GUN_AIMED_AT")
				CASE HASH("PROJ_AIMED")
				CASE HASH("GUNSHOT")
				CASE HASH("SEEN_WEAPON")
				CASE HASH("STEALING_VAN")
				CASE HASH("GET_IN_VAN")
				CASE HASH("STEALING_SEC_CAR")
				
					IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_WarehouseCoord) > 300
					AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sPed.id) > 150.0
					
						SET_PED_AI_STATE(sPed, PEDAI_COMBAT_PLAYER, FALSE, "FIGHT_ESC")
						
					ENDIF
				
				BREAK
				
			ENDSWITCH
		BREAK
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Updates the security peds tasks based of their current state.
PROC SECURITY_AI_TASKS(PED_STRUCT &sPed)

	SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(sPed.id)
	IF HAS_PED_AI_JUST_CHANGED_STATE(sPed)
		pedSecurity.iFlagTask = 0
	ENDIF

	WEAPON_TYPE weapTemp
	
	SWITCH sPed.eAI
	
		CASE PEDAI_WORKING
		
			IF GET_CLOCK_HOURS() >= 5 AND GET_CLOCK_HOURS() < 21
			
				// Stand at security shack
				IF (NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_USE_NEAREST_SCENARIO_TO_POS)
				AND NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_PERFORM_SEQUENCE))
				OR bUsingNightScenario
					IF DOES_SCENARIO_EXIST_IN_AREA(<<203.68, -3132.46, 4.79>>, 1.0, TRUE)
						TASK_USE_NEAREST_SCENARIO_TO_COORD(sPed.id, <<203.68, -3132.46, 4.79>>, 1.0)
						bUsingNightScenario = FALSE
					ENDIF
				ENDIF
				
			ELSE
			
				// use torch
				IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_USE_NEAREST_SCENARIO_CHAIN_TO_POS)
				OR NOT bUsingNightScenario
					IF DOES_SCENARIO_EXIST_IN_AREA(<<162.69, -3115.67, 4.95>>, 1.0, TRUE)
						TASK_USE_NEAREST_SCENARIO_CHAIN_TO_COORD(sPed.id, <<162.69, -3115.67, 4.95>>, 1.0)
						bUsingNightScenario = TRUE
					ENDIF
				ENDIF
				
			ENDIF
		
		BREAK
		
		CASE PEDAI_COMBAT_PLAYER
		
			SET_PED_USING_ACTION_MODE(sPed.id, TRUE, -1, "DEFAULT_ACTION")
			
			SWITCH GET_HASH_KEY(sPed.strAIEvent)
				
				CASE HASH("FIGHT")
				CASE HASH("FIGHT_RETURN")
				CASE HASH("RAN_OVER")
				
					// attack the player with the night stick
					IF HAS_PED_GOT_WEAPON(sPed.id, WEAPONTYPE_PISTOL)
						REMOVE_WEAPON_FROM_PED(sPed.id, WEAPONTYPE_PISTOL)
					ENDIF
					IF NOT HAS_PED_GOT_WEAPON(sPed.id, WEAPONTYPE_NIGHTSTICK)
						GIVE_WEAPON_TO_PED(sPed.id, WEAPONTYPE_NIGHTSTICK, INFINITE_AMMO, TRUE, TRUE)
					ENDIF
					IF GET_CURRENT_PED_WEAPON(sPed.id, weapTemp)
						IF weapTemp != WEAPONTYPE_NIGHTSTICK
							SET_CURRENT_PED_WEAPON(sPed.id, WEAPONTYPE_NIGHTSTICK)
						ENDIF
					ENDIF
				
				BREAK
				
				CASE HASH("KILLED")
				CASE HASH("PLAYER_KILLED")
				CASE HASH("DEAD_BODY")
				CASE HASH("PLAYER_KO")
				CASE HASH("GUN_AIMED_AT")
				CASE HASH("PROJ_AIMED")
				CASE HASH("GUNSHOT")
				CASE HASH("SEEN_WEAPON")
				CASE HASH("STEALING_VAN")
				CASE HASH("GET_IN_VAN")
				CASE HASH("STEALING_SEC_CAR")
				
					// attack the player with the pistol
					IF HAS_PED_GOT_WEAPON(sPed.id, WEAPONTYPE_NIGHTSTICK)
						REMOVE_WEAPON_FROM_PED(sPed.id, WEAPONTYPE_NIGHTSTICK)
					ENDIF
					IF NOT HAS_PED_GOT_WEAPON(sPed.id, WEAPONTYPE_PISTOL)
						GIVE_WEAPON_TO_PED(sPed.id, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
					ENDIF
					IF GET_CURRENT_PED_WEAPON(sPed.id, weapTemp)
						IF weapTemp != WEAPONTYPE_PISTOL
							SET_CURRENT_PED_WEAPON(sPed.id, WEAPONTYPE_PISTOL)
						ENDIF
					ENDIF
				
				BREAK
				
			ENDSWITCH
		
			// Combat task
			IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_COMBAT)
			OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)

				TASK_COMBAT_PED(sPed.id, PLAYER_PED_ID())
				TASK_LOOK_AT_ENTITY(sPed.id, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV)
				
			ENDIF
		
		BREAK
		
		CASE PEDAI_INVESTIGATE
		
			SWITCH GET_HASH_KEY(sPed.strAIEvent)
				CASE HASH("HEARD_SEC_CAR")

					IF DOES_ENTITY_EXIST(sPed.entityInvestigating)
					
						IF NOT IS_PED_HEADING_TOWARDS_POSITION(sPed.id, GET_ENTITY_COORDS(sPed.entityInvestigating), 180)
						AND GET_GAME_TIMER() - sPed.iTimeOfEnteringState < 5000 // not faced it after 5 seconds just walk to target
						
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
							OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)

								TASK_TURN_PED_TO_FACE_ENTITY(sPed.id, sPed.entityInvestigating, -1)
								TASK_LOOK_AT_ENTITY(sPed.id, sPed.entityInvestigating, -1, SLF_WHILE_NOT_IN_FOV)

							ENDIF
						
						// begin walk over to the entity once facing it
						ELSE
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_GOTO_ENTITY_OFFSET)
							OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)
								TASK_GOTO_ENTITY_OFFSET_XY(sPed.id, sPed.entityInvestigating, DEFAULT_TIME_NEVER_WARP, 2.0, -1.0, 2.5, PEDMOVE_WALK, ESEEK_OFFSET_ORIENTATES_WITH_ENTITY)
							ENDIF	
						ENDIF

					ENDIF
				
				BREAK
				CASE HASH("HEARD_PLAYER")
				
					BOOL bRetask
				
					// Update last heard at coord
					IF sPed.bCanHearPlayer
					AND NOT ARE_VECTORS_ALMOST_EQUAL(sPed.vInvestigationCoord, sPed.vLastPosPlayerHeardAt, 1.0)	// only update last heard coord every 1m
						sPed.vInvestigationCoord = sPed.vLastPosPlayerHeardAt
						bRetask = TRUE	// coord updated so retask the ped with the new coord
						
						CPRINTLN(DEBUG_MISSION, "INVESTIGATION COORD UPDATE vInvestigationCoord:", sPed.vInvestigationCoord, " vLastPosPlayerHeardAt:", sPed.vLastPosPlayerHeardAt)
					ENDIF
					
					IF NOT IS_VECTOR_ZERO(sPed.vInvestigationCoord)
					
						// turn the ped around
						IF NOT IS_PED_HEADING_TOWARDS_POSITION(sPed.id, sPed.vInvestigationCoord, 90)
						AND GET_GAME_TIMER() - sPed.iTimeOfEnteringState < 5000
						
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_TURN_PED_TO_FACE_COORD)
							OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)
							OR bRetask
						
								TASK_TURN_PED_TO_FACE_COORD(sPed.id, sPed.vInvestigationCoord)
								TASK_LOOK_AT_COORD(sPed.id, sPed.vInvestigationCoord, -1, SLF_WHILE_NOT_IN_FOV)

							ENDIF
						
						// begin walk over to the sound
						ELSE
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
							OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)
							OR bRetask
								TASK_FOLLOW_NAV_MESH_TO_COORD(sPed.id, sPed.vLastPosPlayerHeardAt, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP, 3.0)
							ENDIF	
						ENDIF

					ENDIF
				
				BREAK
				CASE HASH("HEARD_ALARM")
					
				
				BREAK
				
				CASE HASH("SOUND_LOST")
				
					// Stand and look at the van driving off
					IF NOT IS_VECTOR_ZERO(sPed.vInvestigationCoord)
				
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_TURN_PED_TO_FACE_COORD)
						OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)
							TASK_TURN_PED_TO_FACE_COORD(sPed.id, sPed.vInvestigationCoord, -1)
							TASK_LOOK_AT_COORD(sPed.id, sPed.vInvestigationCoord, -1, SLF_WHILE_NOT_IN_FOV)
						ENDIF
						
					// no van stored, just stand still
					ELSE
						TASK_STAND_STILL(sPed.id, -1)
					ENDIF
				
				
				BREAK
				CASE HASH("VAN_LOST")
				
					// Stand and look at the van driving off
					IF DOES_ENTITY_EXIST(sPed.entityInvestigating)
				
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(sPed.id, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY)
						OR HAS_PED_AI_JUST_CHANGED_STATE(sPed)
							TASK_TURN_PED_TO_FACE_ENTITY(sPed.id, sPed.entityInvestigating, -1)
							TASK_LOOK_AT_ENTITY(sPed.id, sPed.entityInvestigating, -1, SLF_WHILE_NOT_IN_FOV)
						ENDIF
						
					// no van stored, just stand still
					ELSE
						TASK_STAND_STILL(sPed.id, -1)
					ENDIF
				
				BREAK
				
			ENDSWITCH
		
		BREAK
	
	ENDSWITCH
	
ENDPROC

PROC GET_SKIP_STAGE_COORD_AND_HEADING(INT iStage, VECTOR &vCoord, FLOAT &fHeading)

	SWITCH int_to_enum(MISSION_STAGE_FLAGS, iStage)
		CASE msf_bugstar_steal				vCoord = <<183.5330, -2946.1990, 5.5113>>		fHeading = 177.8164			BREAK
		CASE msf_mission_passed				vCoord = <<692.0670, -1004.8117, 21.9059>>		fHeading = 359.5735			BREAK
	ENDSWITCH

ENDPROC

PROC CLEANUP_PED(PED_STRUCT &sPed, BOOL bImmediately)

	IF DOES_ENTITY_EXIST(sPed.id)
		IF bImmediately
			DELETE_PED(sPed.id)
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(sPed.id)
		ENDIF
	ENDIF
	
// Reset ped structs
	sPed.eAI						= PEDAI_WORKING
	sPed.eAIPrev					= PEDAI_NONE
	sPed.eAIPrevFrame				= PEDAI_NONE
	
	sPed.bCanSeePlayer				= FALSE
	sPed.bCanHearPlayer				= FALSE
	sPed.bDisplayAIBlip				= TRUE
	sPed.b_BlipsEnemy				= FALSE
	
	sPed.iFlagState					= 0
	sPed.iFlagTask					= 0
	sPed.iDiaFlag					= 0
	
	sPed.iTimeOfEnteringState		= -1
	sPed.iTimeKilled				= -1
	sPed.iCanSeeTimer				= 0
	sPed.iInvestigateTimer			= -1
	
	sPed.vLastPosPlayerHeardAt 		= <<0,0,0>>
	sPed.vInvestigationCoord		= <<0,0,0>>
	sPed.entityInvestigating		= null
	
	IF DOES_ENTITY_EXIST(sPed.objProp)
		IF IS_ENTITY_ATTACHED(sPed.objProp)
			DETACH_ENTITY(sPed.objProp)
		ENDIF
		IF bImmediately
			DELETE_OBJECT(sPed.objProp)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(sPed.objProp)
		ENDIF
	ENDIF
	sPed.pedPropHand				= BONETAG_NULL
	sPed.propName					= DUMMY_MODEL_FOR_SCRIPT
	sPed.bSafeToCreateProp			= FALSE
	sPed.iWarningsToLeaveWarehouse	= 0

ENDPROC

//PURPOSE: Cleans up the mission
PROC Mission_Cleanup(BOOL bImmediately)
	INT i,j
	
	CLEAR_PRINTS()
	CLEAR_HELP()
	IF bImmediately
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		TRIGGER_MUSIC_EVENT("JHP1A_FAIL")
	ELSE
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		SET_PLAYER_NOISE_MULTIPLIER(PLAYER_ID(), 1.0)
	ENDIF

	SET_MAX_WANTED_LEVEL(5)
	SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
	SET_CREATE_RANDOM_COPS(TRUE)
	
	RESET_ALL_MISSION_EVENTS(s_Events)
	CLEAR_MISSION_LOCATE_STUFF(s_LocatesData)
	
	// Reset flags
	b_ConvoWasPlayingLastFrame					= FALSE
	i_TimeOfLastConvo							= -1
	b_HasPlayerDamagedAVan						= FALSE
	b_WantedLevelGiven							= FALSE
	bIsPlayerStillStealth						= FALSE
	i_VanDamageTimer							= -1
	e_PlayerStatus								= PLAYERSTATUS_NONE
	e_PlayersLastVan							= mvf_invalid
	veh_TowTruckAttached 						= NULL
	veh_DumpedAtEntrance 						= NULL
	b_GotWantedAfterStealingVan					= FALSE

	IF bImmediately
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
	
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		CLEAR_AREA_OF_PEDS(<<158.78, -3100.44, 6.0019>>, 100.0)
		CLEAR_AREA_OF_COPS(<<158.78, -3100.44, 6.0019>>, 100.0)
		CLEAR_AREA_OF_OBJECTS(<<158.78, -3100.44, 6.0019>>, 100.0)
		CLEAR_AREA_OF_VEHICLES(<<158.78, -3100.44, 6.0019>>, 100.0)
		CLEAR_AREA_OF_PROJECTILES(<<158.78, -3100.44, 6.0019>>, 100.0)
		REMOVE_DECALS_IN_RANGE(<<158.78, -3100.44, 6.0019>>, 100.0)
	ELSE
	ENDIF
	
	REPEAT COUNT_OF(objs) i
		IF DOES_ENTITY_EXIST(objs[i])
			IF bImmediately
				DELETE_OBJECT(objs[i])
			ELSE
				SET_OBJECT_AS_NO_LONGER_NEEDED(objs[i])
			ENDIF
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(peds) i
		CLEANUP_PED(peds[i], bImmediately)
	ENDREPEAT
	CLEANUP_PED(pedSecurity, bImmediately)
	
	REPEAT COUNT_OF(vehs) i
		REPEAT 4 j
			IF DOES_ENTITY_EXIST(vehs[i].peds[j])
				IF bImmediately
					DELETE_PED(vehs[i].peds[j])
				ELSE
					SET_PED_AS_NO_LONGER_NEEDED(vehs[i].peds[j])
				ENDIF
			ENDIF
		ENDREPEAT
	
		IF DOES_ENTITY_EXIST(vehs[i].id)
			IF bImmediately
				DELETE_VEHICLE(vehs[i].id)
			ELSE
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[i].id)
			ENDIF
		ENDIF
	ENDREPEAT
	
	IF IS_ALARM_PLAYING(str_Alarm)
		STOP_ALARM(str_Alarm, bImmediately)
	ENDIF
	
	IF sbi_SweatshopBin != NULL
		REMOVE_SCENARIO_BLOCKING_AREA(sbi_SweatshopBin)
	ENDIF
	
	RESET_ALL_BATTLEBUDDY_BEHAVIOUR_REQUESTS()
	
ENDPROC

//PURPOSE: Passed the mission
PROC Mission_Passed()

	INT iTempHealth

	IF e_PlayersLastVan != mvf_invalid
	AND DOES_ENTITY_EXIST(vehs[e_PlayersLastVan].id)
	AND IS_VEHICLE_DRIVEABLE(vehs[e_PlayersLastVan].id)
		iTempHealth = GET_ENTITY_HEALTH(vehs[e_PlayersLastVan].id)
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehs[e_PlayersLastVan].id) 
	ENDIF
	
	IF iTempHealth >= 750 OR iTempHealth <= 0
		g_iJewelPrep1AVanHealth = 1000
	ELSE
		g_iJewelPrep1AVanHealth = iTempHealth
	ENDIF
	
	IF NOT IS_MISSION_EVENT_TRIGGERED(s_Events[mef_alarms].sData)
		INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(JHP1A_SNEAKY_PEST)
	ENDIF
	
	Mission_Cleanup(FALSE)
	IF GET_MISSION_COMPLETE_STATE(SP_HEIST_JEWELRY_PREP_2A)
		MISSION_FLOW_MISSION_PASSED(FALSE, TRUE)
	ELSE
		MISSION_FLOW_MISSION_PASSED(FALSE, FALSE)
	ENDIF
	TERMINATE_THIS_THREAD()
	
ENDPROC

//PURPOSE: Starts a mission fail
PROC Mission_Failed(MISSION_FAIL_FLAGS fail_condition = mff_default)

	IF fail_condition = mff_default 
		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		Mission_Cleanup(FALSE)
		TRIGGER_MUSIC_EVENT("JHP1A_FAIL")
		TERMINATE_THIS_THREAD()

	ELIF NOT b_MissionFailed
	
		b_MissionFailed = TRUE
		e_FailReason = fail_condition
		
		TEXT_LABEL_23 strFailMsg, strSubstring
		SWITCH e_FailReason
			
			CASE mff_van_dead				strFailMsg = "JHP1A_VAN_DEAD"		BREAK
			CASE mff_van_stuck				strFailMsg = "JHP1A_VAN_STUCK"		BREAK
			CASE mff_vans_dead				strFailMsg = "JHP1A_VANS_DEAD"		BREAK
			CASE mff_van_abandoned			strFailMsg = "JHP1A_VAN_ABAN"		BREAK
			CASE mff_mission_abandoned		strFailMsg = "JHP1A_ABAN"			BREAK
			CASE mff_debug_forced												FALLTHRU
			DEFAULT							strFailMsg = "JHP1A_FF"				BREAK
		ENDSWITCH
			
		IF IS_STRING_NULL_OR_EMPTY(strSubstring)
			MISSION_FLOW_MISSION_FAILED_WITH_REASON(strFailMsg)
		ELSE
			MISSION_FLOW_MISSION_FAILED_WITH_REASON_CONTAINING_TEXT(strFailMsg, strSubstring)
		ENDIF
		
		TRIGGER_MUSIC_EVENT("JHP1A_FAIL")
	ENDIF
ENDPROC

//PURPOSE: Called when the mission has failed and for what reason
PROC Mission_Fail_Update()

	IF GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 

		// check if we need to respawn the player in a different position, 
		// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here

		Mission_Cleanup(TRUE) 
		TERMINATE_THIS_THREAD()
		
	ENDIF
	
ENDPROC

//PURPOSE: Checks key mission entities each frame to see if they are dead
PROC Mission_Entity_Death_Checks()
	INT i
	
	IF IS_PED_INJURED(PLAYER_PED_ID())
		Mission_Failed(mff_default)
	ENDIF
	
	
	REPEAT COUNT_OF(peds) i
		IF DOES_ENTITY_EXIST(peds[i].id)
		
			IF peds[i].bDisplayAIBlip
				
				IF peds[i].b_BlipsEnemy
					UPDATE_AI_PED_BLIP(peds[i].id, peds[i].sAIBlip, BLIP_GANG_ENEMY)
				ELSE
					UPDATE_AI_PED_BLIP(peds[i].id, peds[i].sAIBlip, BLIP_GANG_FRIENDLY)
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_ALIVE(peds[i].id)
				IF peds[i].iTimeKilled = -1
					peds[i].iTimeKilled = GET_GAME_TIMER()
				ELIF GET_GAME_TIMER() - peds[i].iTimeKilled >= 5000
					// do not clean up will need for peds finding dead bodies
				ENDIF
			ENDIF
			
		ENDIF
	ENDREPEAT
	
	REPEAT COUNT_OF(vehs) i
		IF DOES_ENTITY_EXIST(vehs[i].id)
		AND vehs[i].bAutoCleanup
			IF NOT IS_ENTITY_ALIVE(vehs[i].id)
				IF DOES_BLIP_EXIST(vehs[i].blip)
					REMOVE_BLIP(vehs[i].blip)
				ENDIF
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[i].id)
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

/// PURPOSE:
///    Performs checks every frames for fails and general stuff that needs to be looked after constantly throughout the mission
PROC Mission_Checks()

	MISSION_VEHICLE_FLAGS eVeh
	MISSION_PED_FLAGS ePed
	BOOL bStillAttached = FALSE
	
	IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_WarehouseCoord) < 70
		FORCE_SONAR_BLIPS_THIS_FRAME()
	ENDIF
	
	// Player has left the scene with a van, cleanup the warehouse
	IF e_PlayerStatus = PLAYERSTATUS_VAN_STEALING
	
		IF NOT b_GotWantedAfterStealingVan
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_JSH_PREP_1A_01", 0.0)
				b_GotWantedAfterStealingVan = TRUE
			ENDIF
		ENDIF
	
		IF DOES_ENTITY_EXIST(pedSecurity.id)
			IF GET_DISTANCE_BETWEEN_ENTITIES(pedSecurity.id, PLAYER_PED_ID()) > 300
				SET_PED_AS_NO_LONGER_NEEDED(pedSecurity.id)
			ENDIF
		ENDIF
	ENDIF
	
	// Check if stored tow truck is still attached to 1 of the vans
	IF DOES_ENTITY_EXIST(veh_TowTruckAttached)
	AND IS_VEHICLE_DRIVEABLE(veh_TowTruckAttached)

		FOR eVeh = mvf_bugstar_van_1 TO mvf_bugstar_van_3
		
			IF DOES_ENTITY_EXIST(vehs[eVeh].id)
			AND IS_VEHICLE_DRIVEABLE(vehs[eVeh].id)

				IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(veh_TowTruckAttached, vehs[eVeh].id)
					bStillAttached = TRUE
				ENDIF
			
			ENDIF
		
		ENDFOR
		
		// not still attached, get rid of the tow truck index stored
		IF NOT bStillAttached
			veh_TowTruckAttached = NULL
		ENDIF
		
	ENDIF
	
	// Overrite with truck player is currently in if any at all
	IF IS_PLAYER_IN_A_TOW_TRUCK()
	
		FOR eVeh = mvf_bugstar_van_1 TO mvf_bugstar_van_3
		
			IF DOES_ENTITY_EXIST(vehs[eVeh].id)
			AND IS_VEHICLE_DRIVEABLE(vehs[eVeh].id)

				IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), vehs[eVeh].id)
					veh_TowTruckAttached = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				ENDIF
			
			ENDIF
		
		ENDFOR
		
	ENDIF

	UPDATE_PLAYER_STATUS()

	// STEAL BUGSTAR VAN STAGE CHECKS
	//-----------------------------------------------------------------
	
	IF i_missionStage = enum_to_int(msf_bugstar_steal)
	
	
		BOOL bPlayerAwayFromWarehouse
		IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<182.953690,-3119.883789,101.922592>>,<<115.0,260.0,120.0>>)
			bPlayerAwayFromWarehouse = TRUE
		ENDIF
	
		// PROCESS PEDS
		REPEAT COUNT_OF(peds) ePed
			IF DOES_ENTITY_EXIST(peds[ePed].id)
				IF peds[ePed].eAI = PEDAI_FLEEING
				AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), peds[ePed].id) > PED_CLEANUP_RANGE_PED_FROM_PLAYER
				AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), v_WarehouseCoord) > PED_CLEANUP_RANGE_PED_FROM_WAREHOUSE
					DELETE_PED(peds[ePed].id)
				ENDIF
			ENDIF
		ENDREPEAT
		
		MISSION_FAIL_FLAGS 			eFailReason 	= mff_default
		
		// Clean up vans when the player is away from the warehouse 
		FOR eVeh = mvf_bugstar_van_1 TO mvf_bugstar_van_3
			IF DOES_ENTITY_EXIST(vehs[eVeh].id)
				
				// Van destroyed
				IF NOT IS_VEHICLE_DRIVEABLE(vehs[eVeh].id)
				
					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehs[eVeh].id)
					IF DOES_BLIP_EXIST(vehs[eVeh].blip)
						REMOVE_BLIP(vehs[eVeh].blip)
					ENDIF
					IF bPlayerAwayFromWarehouse
						eFailReason = mff_van_dead
					ELSE
						eFailReason = mff_vans_dead
					ENDIF
					
				// Stuck
				ELIF IS_VEHICLE_STUCK(vehs[eVeh].id)
				
					IF DOES_BLIP_EXIST(vehs[eVeh].blip)
						REMOVE_BLIP(vehs[eVeh].blip)
					ENDIF
					eFailReason = mff_van_stuck
				
				// Abandoned
				ELSE
					IF NOT IS_VEHICLE_IN_USEABLE_RANGE(vehs[eVeh].id)
						IF GET_INTERIOR_FROM_ENTITY(vehs[eVeh].id) = interior_warehouse
							IF eFailReason != mff_van_abandoned
								eFailReason = mff_mission_abandoned
							ENDIF
						ELSE
							eFailReason = mff_van_abandoned
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF
			
		// Sort the blips out
			// Player is already in a van and stealing it,
			// the van is drivable and not stuck
			// remove all van blips
			IF (e_PlayerStatus = PLAYERSTATUS_VAN_STEALING
				AND GET_VAN_PLAYER_IS_IN() != mvf_invalid
				AND DOES_ENTITY_EXIST(vehs[GET_VAN_PLAYER_IS_IN()].id)
				AND IS_VEHICLE_DRIVEABLE(vehs[GET_VAN_PLAYER_IS_IN()].id)
				AND NOT IS_VEHICLE_STUCK(vehs[GET_VAN_PLAYER_IS_IN()].id))
			OR (e_PlayerStatus = PLAYERSTATUS_VAN_STEALING_TOW
				AND GET_VAN_PLAYER_IS_TOWING() != mvf_invalid
				AND DOES_ENTITY_EXIST(vehs[GET_VAN_PLAYER_IS_TOWING()].id)
				AND IS_VEHICLE_DRIVEABLE(vehs[GET_VAN_PLAYER_IS_TOWING()].id))
		
				IF DOES_BLIP_EXIST(vehs[eVeh].blip)
					REMOVE_BLIP(vehs[eVeh].blip)
				ENDIF
				
			// player not in a van, or van is fucked blip other vans that are still useable
			// 
			ELIF DOES_ENTITY_EXIST(vehs[eVeh].id)
			AND IS_VEHICLE_DRIVEABLE(vehs[eVeh].id) 
			AND NOT IS_VEHICLE_STUCK(vehs[eVeh].id)
			AND IS_VEHICLE_IN_USEABLE_RANGE(vehs[eVeh].id)
			AND i_missionSubstage = 0
			
				IF NOT DOES_BLIP_EXIST(vehs[eVeh].blip)
					vehs[eVeh].blip = CREATE_BLIP_FOR_VEHICLE(vehs[eVeh].id)
					SET_BLIP_PRIORITY(vehs[eVeh].blip, BLIPPRIORITY_LOWEST)
				ENDIF
			
			ENDIF
			
		ENDFOR
		
		IF e_PlayerStatus = PLAYERSTATUS_VAN_STEALING
		OR e_PlayerStatus = PLAYERSTATUS_VAN_STEALING_TOW
			IF IS_THIS_PRINT_BEING_DISPLAYED("JHP1A_BKIN")
			OR IS_THIS_PRINT_BEING_DISPLAYED("JHP1A_BKINANY")
				CLEAR_PRINTS()
			ENDIF
		ELSE
			IF i_missionSubstage = 0
				IF NOT bPromptGetBackInVan
					IF GET_NUM_OF_VANS_LEFT(TRUE) = 1
						PRINT_NOW("JHP1A_BKIN", DEFAULT_GOD_TEXT_TIME, 1)	
					ELSE
						//PRINT_NOW("JHP1A_BKINANY", DEFAULT_GOD_TEXT_TIME, 1)
					ENDIF
					bPromptGetBackInVan = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		
		
	// Process fail
		
		// No vans left so either van was destroyed or cleaned up due to being abandoned
		IF GET_NUM_OF_VANS_LEFT(TRUE) = 0
			
			IF eFailReason != mff_default
				Mission_Failed(eFailReason)
			ENDIF
			
		ENDIF


	ENDIF
	
ENDPROC

//PURPOSE:
PROC Mission_Setup()

	ADD_RELATIONSHIP_GROUP("BUGSTAR", rel_bugstar)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_DISLIKE, RELGROUPHASH_PLAYER, rel_bugstar)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, rel_bugstar, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, rel_bugstar)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rel_bugstar, RELGROUPHASH_COP)

	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(
		<<139.579529,-3092.962402,8.646310>> - <<33.312500,24.000000,4.187500>>,
		<<139.579529,-3092.962402,8.646310>> + <<33.312500,24.000000,4.187500>>,
		FALSE)
	
	REMOVE_VEHICLES_FROM_GENERATORS_IN_AREA(
		<<138.426773,-3092.467041,7.271310>> - <<19.437500,22.750000,2.375000>>,
		<<138.426773,-3092.467041,7.271310>> + <<19.437500,22.750000,2.375000>>)
		
	CLEAR_AREA(<<138.43, -3092.47, 4.90>>, 28.375, TRUE)

	IF IS_REPLAY_IN_PROGRESS()		// RETRIES
	OR IS_REPEAT_PLAY_ACTIVE()		// REPLAY VIA START MENU
	
		i_SkipToStage = enum_to_int(msf_bugstar_steal)
		
		IF IS_REPLAY_IN_PROGRESS()
			IF g_bShitskipAccepted
				i_SkipToStage += 1
				CPRINTLN(DEBUG_MISSION, "JHP1A: g_bShitskipAccepted = TRUE")
			ENDIF
		ENDIF
		
		CPRINTLN(DEBUG_MISSION, "JHP1A: stage skipping to ", i_SkipToStage)
		
		// New replay streaming 
		IF IS_REPLAY_IN_PROGRESS()
			VECTOR vSkipToCoord
			FLOAT fSkipToHeading
			GET_SKIP_STAGE_COORD_AND_HEADING(i_SkipToStage, vSkipToCoord, fSkipToHeading)
			START_REPLAY_SETUP(vSkipToCoord, fSkipToHeading)
			CPRINTLN(DEBUG_MISSION, "JHP1A: Using replay warp system - Warp Coord ", vSkipToCoord, " Warp Heading ", fSkipToHeading)
		ENDIF
		
		b_DoSkip = TRUE
	ELSE
		WHILE NOT PREPARE_INITIAL_SCENE(TRUE)
			wait(0)
			Update_Asset_Management_System(s_AssetData)
		ENDWHILE
	ENDIF

	Load_Asset_Additional_Text(s_AssetData, "JHP1A", MISSION_TEXT_SLOT)
	Load_Asset_AnimDict(s_AssetData, "misslsdhsclipboard@base")
	Load_Asset_AnimDict(s_AssetData, "misslsdhsclipboard@idle_a")
	Load_Asset_AnimDict(s_AssetData, "misstrevor3")
	Load_Asset_Model(s_AssetData, P_AMB_CLIPBOARD_01)
	Load_Asset_Interior(s_AssetData, interior_warehouse)
	Load_Asset_Waypoint(s_AssetData, str_wpSecurityArrive)
	
	SET_PLAYER_NOISE_MULTIPLIER(PLAYER_ID(), 0.15)
	
	INIT_BATTLEBUDDY_BEHAVIOUR_FOR_MISSION(SP_HEIST_JEWELRY_PREP_1A)
	
	ADD_PED_FOR_DIALOGUE(s_Convo, 0, PLAYER_PED_ID(), "MICHAEL")
	ADD_PED_FOR_DIALOGUE(s_Convo, 3, null, "LESTER")

	// Temp compile fix
	DUMMY_REFERENCE_BOOL(b_ConvoWasPlayingLastFrame)

	#IF IS_DEBUG_BUILD
		debug_widget = START_WIDGET_GROUP("The Jewelry Store Job - Prep A")
			START_WIDGET_GROUP("Mission info")
				ADD_WIDGET_INT_READ_ONLY("Mission Stage", i_missionStage)
				ADD_WIDGET_INT_READ_ONLY("Mission Substage", i_missionSubstage)	
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		SET_LOCATES_HEADER_WIDGET_GROUP(debug_widget)
		
		s_ZMenuStruct[0].sTxtLabel 		= "STEAL BUGSTAR VAN"
		s_ZMenuStruct[1].sTxtLabel		= "MISSION PASSED"
	#ENDIF
	
	SET_START_CHECKPOINT_AS_FINAL() // tells replay controller to display "skip mission" for shitskips
	
	interior_warehouse = GET_INTERIOR_AT_COORDS_WITH_TYPE(v_WarehouseCoord, "po1_08_warehouseint1")
	
	PREPARE_ALARM(str_Alarm)
	
	sbi_SweatshopBin = ADD_SCENARIO_BLOCKING_AREA(<<713.7754, -996.4443, 22.3085>>, <<715.7624, -991.7067, 25.6214>>)
			
	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
	//OR NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_DIALOGUE_TEXT_SLOT)
		PRINT_DEBUG("WAITING ON MISSION TEXT")
		WAIT(0)
	ENDWHILE
ENDPROC

PROC AI_CONTROLLER_PRE_UPDATE(PED_STRUCT &sPed)
	
	IF CAN_PED_SEE_HATED_PED(sPed.id, PLAYER_PED_ID())
		sPed.iCanSeeTimer 	= GET_GAME_TIMER()
		sPed.bCanSeePlayer 	= TRUE					
	ELSE
		IF GET_GAME_TIMER() - sPed.iCanSeeTimer > 2000
		OR NOT IS_PED_FACING_PED(sPed.id, PLAYER_PED_ID(), 90)
			sPed.iCanSeeTimer 	= GET_GAME_TIMER()
			sPed.bCanSeePlayer 	= FALSE
		ENDIF
	ENDIF
	
	IF sPed.bCanSeePlayer
		bIsPlayerStillStealth 		= FALSE
	ENDIF
	
	IF bIsPlayerStillStealth
		SET_PED_RESET_FLAG(sPed.id, PRF_PreventFailedMeleeTakedowns, TRUE)
	ENDIF

	IF NOT GET_IS_A_VAN_ALARM_GOING_OFF()
		sPed.bCanHearPlayer		= CAN_PED_HEAR_PLAYER(PLAYER_ID(), sPed.id)
		IF sPed.bCanHearPlayer
			sPed.vLastPosPlayerHeardAt = GET_ENTITY_COORDS(PLAYER_PED_ID())
		ENDIF
	ELSE
		// Can't hear the player while the van alarm is going off.
		sPed.bCanHearPlayer = FALSE
	ENDIF
	
	SWITCH sPed.eAI
		CASE PEDAI_FLEEING								FALLTHRU
		CASE PEDAI_HANDSUP								FALLTHRU
		CASE PEDAI_COMBAT_PLAYER
			sPed.b_BlipsEnemy = TRUE
		BREAK
		DEFAULT 
			sPed.b_BlipsEnemy = FALSE
		BREAK
	ENDSWITCH

ENDPROC

PROC EVENT_AI_CONTROLLER(MISSION_EVENT_DATA &sData)
	IF NOT IS_MISSION_EVENT_ENDED(sData)
		MISSION_PED_FLAGS ePed
		
		bIsPlayerStillStealth = TRUE
		
		IF NOT b_HasPlayerDamagedAVan
			IF HAS_PLAYER_DAMAGED_A_VAN()
				b_HasPlayerDamagedAVan 	= TRUE
				i_VanDamageTimer		= GET_GAME_TIMER()
			ENDIF
			
		ELIF GET_GAME_TIMER() - i_VanDamageTimer > 3000
			b_HasPlayerDamagedAVan	= FALSE
			i_VanDamageTimer 		= -1
		
		ENDIF
	
		// >>>>>> PRE-STATE UPDATE PASS <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		
		REPEAT COUNT_OF(peds) ePed
			IF IS_ENTITY_ALIVE(peds[ePed].id)
				AI_CONTROLLER_PRE_UPDATE(peds[ePed])
			ENDIF
		ENDREPEAT
		IF IS_ENTITY_ALIVE(pedSecurity.id)
			AI_CONTROLLER_PRE_UPDATE(pedSecurity)
		ENDIF
		
		// >>>>>> BROADCAST EVENTS UPDATE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

		INT i
		REPEAT COUNT_OF(s_BroadcastEvent) i
		
			IF NOT IS_STRING_NULL_OR_EMPTY(s_BroadcastEvent[i].strEventName)
			AND s_BroadcastEvent[i].iTimeOfEvent != -1
				
				// Check that the event is still valid
				IF s_BroadcastEvent[i].iEventLife = -1
				OR GET_GAME_TIMER() - s_BroadcastEvent[i].iTimeOfEvent < s_BroadcastEvent[i].iEventLife	
				
					// and that enough time had passed for it to become active
					IF s_BroadcastEvent[i].iEventDelay <= 0
					OR GET_GAME_TIMER() - s_BroadcastEvent[i].iTimeOfEvent > s_BroadcastEvent[i].iEventDelay
					
						// Find the event location
						VECTOR vEventCoord
						IF s_BroadcastEvent[i].ePedTransmitter != mpf_invalid
						AND IS_ENTITY_ALIVE(peds[s_BroadcastEvent[i].ePedTransmitter].id)
							vEventCoord 						= GET_ENTITY_COORDS(peds[s_BroadcastEvent[i].ePedTransmitter].id)
							s_BroadcastEvent[i].vEventCoord 	= vEventCoord
						ELSE
							vEventCoord 						= s_BroadcastEvent[i].vEventCoord
						ENDIF
					
						// Raise alarm if the event wants it is within range of the warehouse
						IF NOT IS_MISSION_EVENT_TRIGGERED(s_Events[mef_alarms].sData)
							IF s_BroadcastEvent[i].bCalledSecurity
							AND GET_DISTANCE_BETWEEN_COORDS(vEventCoord, v_WarehouseCoord) < 35.0
								TRIGGER_MISSION_EVENT(s_Events[mef_alarms].sData)
							ENDIF
						ENDIF
					
						// Dispatch even to peds in range
						REPEAT COUNT_OF(peds) ePed
						
							// do not let receive it receive its own even or a none event
							IF s_BroadcastEvent[i].ePedTransmitter = mpf_invalid
							OR (ePed != s_BroadcastEvent[i].ePedTransmitter	
							AND peds[s_BroadcastEvent[i].ePedTransmitter].eAI >= peds[ePed].eAI)
							
								IF DOES_ENTITY_EXIST(peds[ePed].id)
								AND NOT IS_PED_INJURED(peds[ePed].id)

									// Check that its within hearing range of the event								
									// and dispatch event to the ped
									IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(peds[ePed].id, vEventCoord) < s_BroadcastEvent[i].fEventRadius + peds[ePed].fEventNoticeRange
										
										peds[ePed].iReceivedEvent = i
										
										// If the event causes the alarm to be raised trigger it
										IF s_BroadcastEvent[i].bRaiseAlarm
											IF NOT IS_MISSION_EVENT_TRIGGERED(s_Events[mef_alarms].sData)
												TRIGGER_MISSION_EVENT(s_Events[mef_alarms].sData)
												str_AlarmAIEvent 	= s_BroadcastEvent[i].strEventName
											ENDIF
										ENDIF
									ENDIF
								
								ENDIF
								
							ENDIF
						ENDREPEAT
						
					ENDIF
					
				// clean up dead event after 2 seconds
				ELIF GET_GAME_TIMER() - s_BroadcastEvent[i].iTimeOfEvent > s_BroadcastEvent[i].iEventLife + 2000
					
					s_BroadcastEvent[i].strEventName 			= ""
					s_BroadcastEvent[i].ePedTransmitter			= mpf_invalid
					s_BroadcastEvent[i].vEventCoord				= <<0,0,0>>
					s_BroadcastEvent[i].fEventRadius			= 0.0
					s_BroadcastEvent[i].iTimeOfEvent			= -1
					s_BroadcastEvent[i].iEventLife				= -1
					s_BroadcastEvent[i].iEventDelay				= -1
					
				ENDIF
				
			ENDIF
		ENDREPEAT
		
		// >>>>>> STATE UPDATE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		
		// Bugstar staff
		REPEAT COUNT_OF(peds) ePed
			IF IS_ENTITY_ALIVE(peds[ePed].id)
				BUGSTAR_STAFF_AI_STATE(peds[ePed])
			ENDIF
		ENDREPEAT
		
		// Security
		IF IS_ENTITY_ALIVE(pedSecurity.id)
			SECURITY_AI_STATE(pedSecurity)
		ENDIF

		// >>>>>> TASK UPDATE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<

		// Bugstar staff
		REPEAT COUNT_OF(peds) ePed
			IF IS_ENTITY_ALIVE(peds[ePed].id)
				BUGSTAR_STAFF_AI_TASKS(peds[ePed])
			ENDIF
		ENDREPEAT
		
		// Security
		IF IS_ENTITY_ALIVE(pedSecurity.id)
			SECURITY_AI_TASKS(pedSecurity)
		ENDIF
		
		// >>>>>> Prop spawning system <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		REPEAT COUNT_OF(peds) ePed
		
			IF DOES_ENTITY_EXIST(peds[ePed].objProp)
			
				BOOL bDetachObject, bDeleteObject
			
				IF NOT DOES_ENTITY_EXIST(peds[ePed].id)
				OR IS_PED_INJURED(peds[ePed].id)
				OR IS_PED_RUNNING_RAGDOLL_TASK(peds[ePed].id)
				OR (peds[ePed].bDropWhenNotInWorkingState 
				AND peds[ePed].eAI != PEDAI_WORKING)
					bDetachObject = TRUE
				ELIF peds[ePed].eAI != PEDAI_WORKING
					bDeleteObject = TRUE
				ENDIF

				IF bDetachObject OR bDeleteObject
				
					IF IS_ENTITY_ATTACHED_TO_ENTITY(peds[ePed].objProp, peds[ePed].id)
						DETACH_ENTITY(peds[ePed].objProp)
					ENDIF
				
					// Drop and clean up prop
					IF bDetachObject
						SET_OBJECT_AS_NO_LONGER_NEEDED(peds[ePed].objProp)
					// delete instantly
					ELIF bDeleteObject
						DELETE_OBJECT(peds[ePed].objProp)
					ENDIF
					
					peds[ePed].bSafeToCreateProp = FALSE
				ENDIF

			ELSE
				// recreate prop if there is one to recreate 
				IF peds[ePed].propName != DUMMY_MODEL_FOR_SCRIPT
				AND peds[ePed].pedPropHand != BONETAG_NULL
				
					IF peds[ePed].bSafeToCreateProp
					AND HAS_MODEL_LOADED(peds[ePed].propName)
						peds[ePed].objProp = CREATE_OBJECT(peds[ePed].propName, GET_PED_BONE_COORDS(peds[ePed].id, peds[ePed].pedPropHand, <<0,0,0>>))
						ATTACH_ENTITY_TO_ENTITY(peds[ePed].objProp, peds[ePed].id, GET_PED_BONE_INDEX(peds[ePed].id, peds[ePed].pedPropHand), <<0,0,0>>, <<0,0,0>>, TRUE, TRUE, TRUE)
						peds[ePed].bSafeToCreateProp = FALSE
					ENDIF	
				
				ENDIF
			ENDIF
			
			
		ENDREPEAT
		
		#IF IS_DEBUG_BUILD

			IF bDisplayDebug
						
				REPEAT COUNT_OF(peds) ePed
					RENDER_PED_DEBUG(peds[ePed])		
				ENDREPEAT
				RENDER_PED_DEBUG(pedSecurity)
				
				TEXT_LABEL_63 strDebugText
				INT j // info counter
				
				REPEAT COUNT_OF(s_BroadcastEvent) i
				
					strDebugText 	= ""
					j 				= 0
				
					IF NOT IS_STRING_NULL_OR_EMPTY(s_BroadcastEvent[i].strEventName)
					
						INT iR, iG, iB, iA
						// Waiting to start
						IF s_BroadcastEvent[i].iEventDelay > 0
						AND GET_GAME_TIMER() - s_BroadcastEvent[i].iTimeOfEvent < s_BroadcastEvent[i].iEventDelay
							iR = 0
							iG = 100
							iB = 255
							iA = 100
						
						// Expired
						ELIF s_BroadcastEvent[i].iEventLife != -1
						AND GET_GAME_TIMER() - s_BroadcastEvent[i].iTimeOfEvent > s_BroadcastEvent[i].iEventLife
							iR = 255
							iG = 0
							iB = 0
							iA = 100
						
						// Active
						ELSE
							iR = 0
							iG = 255
							iB = 0
							iA = 200						
						ENDIF
					
						DRAW_DEBUG_SPHERE(s_BroadcastEvent[i].vEventCoord, 0.05, iR, iG, iB, iA)
						DRAW_DEBUG_SPHERE(s_BroadcastEvent[i].vEventCoord, s_BroadcastEvent[i].fEventRadius, iR, iG, iB, 50)
						IF s_BroadcastEvent[i].ePedTransmitter != mpf_invalid
							DRAW_DEBUG_LINE(s_BroadcastEvent[i].vEventCoord, GET_ENTITY_COORDS(peds[s_BroadcastEvent[i].ePedTransmitter].id), iR, iG, iB, iA)
						ENDIF
						
						strDebugText = 	"EventName: "
						strDebugText +=	s_BroadcastEvent[i].strEventName
						DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, s_BroadcastEvent[i].vEventCoord, 0, j*I_SPACING, 0,0,0,iA)
						j++
						
						IF s_BroadcastEvent[i].ePedTransmitter != mpf_invalid
							strDebugText = 	"ePed: "
							strDebugText += peds[s_BroadcastEvent[i].ePedTransmitter].strDebugName
							DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, s_BroadcastEvent[i].vEventCoord, 0, j*I_SPACING, 0,0,0,iA)
							j++
						ENDIF
						
						strDebugText = 	"Coord: "
						strDebugText += GET_STRING_FROM_VECTOR(s_BroadcastEvent[i].vEventCoord)
						DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, s_BroadcastEvent[i].vEventCoord, 0, j*I_SPACING, 0,0,0,iA)
						j++
						
						strDebugText = 	"Radius: "
						strDebugText += GET_STRING_FROM_FLOAT(s_BroadcastEvent[i].fEventRadius)
						DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, s_BroadcastEvent[i].vEventCoord, 0, j*I_SPACING, 0,0,0,iA)
						j++
						
						strDebugText = 	"TimeActive: "
						INT iTime = GET_GAME_TIMER() - s_BroadcastEvent[i].iTimeOfEvent
						IF s_BroadcastEvent[i].iEventDelay > 0
							iTime -= s_BroadcastEvent[i].iEventDelay
						ENDIF
						strDebugText += GET_STRING_FROM_INT(iTime)
						DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, s_BroadcastEvent[i].vEventCoord, 0, j*I_SPACING, 0,0,0,iA)
						j++
						
						strDebugText = 	"Life: "
						strDebugText += s_BroadcastEvent[i].iEventLife
						DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, s_BroadcastEvent[i].vEventCoord, 0, j*I_SPACING, 0,0,0,iA)
						j++
						
						strDebugText = 	"RaiseAlarm: "
						IF s_BroadcastEvent[i].bRaiseAlarm
							strDebugText += "TRUE"
						ELSE
							strDebugText += "FALSE"
						ENDIF
						DRAW_DEBUG_TEXT_WITH_OFFSET(strDebugText, s_BroadcastEvent[i].vEventCoord, 0, j*I_SPACING, 0,0,0,iA)
						j++
						
					ENDIF
				
				ENDREPEAT
				
			ENDIF
		#ENDIF
		
		REPEAT COUNT_OF(peds) i
			IF DOES_ENTITY_EXIST(peds[i].id)
			AND NOT IS_PED_INJURED(peds[i].id)
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(peds[i].id)
				CLEAR_ENTITY_LAST_WEAPON_DAMAGE(peds[i].id)
			ENDIF
		ENDREPEAT
		
		CLEAR_VAN_DAMAGE_DETECTION()
		
		
	ENDIF
	
	IF IS_MISSION_EVENT_ENDED(sData)
		
	ENDIF
	
ENDPROC

PROC EVENT_ALARMS(MISSION_EVENT_DATA &sData)
	IF NOT IS_MISSION_EVENT_ENDED(sData)
		
		SWITCH sData.iStage
			CASE MISSION_EVENT_STAGE_START
				SET_MISSION_EVENT_STAGE(sData, 2, 2000)
			BREAK
			CASE 2
				IF PREPARE_ALARM(str_Alarm)
					START_ALARM(str_Alarm, FALSE)
					BROADCAST_QUEUE_SCRIPT_AI_EVENT(str_AlarmAIEvent, v_WarehouseCoord, 30.0, -1, 500, TRUE)
					str_AlarmAIEvent = ""
					SET_MISSION_EVENT_STAGE(sData, 3, 5000)
				ENDIF
			BREAK
			CASE 3
				SET_PED_AI_STATE(pedSecurity, PEDAI_COMBAT_PLAYER, TRUE, "ALARM EVENT SET IT")
				SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 2)
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				SET_MAX_WANTED_LEVEL(2)
				SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), FALSE)
				SET_CREATE_RANDOM_COPS(FALSE)
				UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
				REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
				sData.iGenericFlag = GET_GAME_TIMER() + 15000
				SET_MISSION_EVENT_STAGE(sData, 4)
			BREAK
			CASE 4
				IF GET_GAME_TIMER() > sData.iGenericFlag
					SET_DISPATCH_COPS_FOR_PLAYER(PLAYER_ID(), TRUE)
					SET_CREATE_RANDOM_COPS(TRUE)
					SET_MAX_WANTED_LEVEL(5)
					b_WantedLevelGiven = TRUE
					END_MISSION_EVENT(sData)
				ENDIF 
				
				UPDATE_WANTED_POSITION_THIS_FRAME(PLAYER_ID())
				REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
			BREAK
		ENDSWITCH
		
	ENDIF
ENDPROC

PROC EVENT_DIALOGUE(MISSION_EVENT_DATA &sData)
	IF NOT IS_MISSION_EVENT_ENDED(sData)
	
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			i_TimeOfLastConvo = -1
		ELSE
			IF i_TimeOfLastConvo = -1
				i_TimeOfLastConvo = GET_GAME_TIMER()
				
				// Reset current conversation vars
				#IF IS_DEBUG_BUILD
				eCurrentConvoSpeaker			= mpf_invalid
				strCurrentConvoRoot				= ""
				#ENDIF
				eCurrentConvoPriority			= PEDAI_NONE
				bCurrentConvoCanBeInterruped	= TRUE
			ENDIF
		ENDIF
		

		// >>>>>> PROCESS QUEUED DIALOGUE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		IF eQueuedConvoSpeaker != mpf_invalid
		AND NOT IS_STRING_NULL_OR_EMPTY(strQueuedConvoRoot)
		
			IF peds[eQueuedConvoSpeaker].id != null
			AND DOES_ENTITY_EXIST(peds[eQueuedConvoSpeaker].id)
			AND NOT IS_PED_INJURED(peds[eQueuedConvoSpeaker].id)
			AND NOT IS_CONVERSATION_PED_DEAD(peds[eQueuedConvoSpeaker].id)
			
				IF NOT IS_PED_RAGDOLL(peds[eQueuedConvoSpeaker].id)
			
					IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
						IF bQueuedConvoInterrupt 
						AND (bCurrentConvoCanBeInterruped OR eQueuedConvoPriority > eCurrentConvoPriority)

							IF bCurrentConvoStoreForFutureResumption
								eInterruptedConvoSpeaker			= eCurrentConvoSpeaker
								eInterruptedConvoPriority			= eCurrentConvoPriority
								strInterruptedConvoRoot				= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
								strInterruptedConvoLine 			= GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
								bInterruptedConvoCanBeInterrupted 	= bCurrentConvoCanBeInterruped
								bInterruptedConvoKillAtDistance		= bCurrentConvoKillAtDistance
							ENDIF
						
							IF bQueuedConvoDoNotFinish
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							ELSE
								KILL_FACE_TO_FACE_CONVERSATION()
							ENDIF
						ENDIF
					ENDIF
					
					IF IS_SAFE_TO_START_CONVERSATION()
						IF GET_GAME_TIMER() - i_TimeOfLastConvo > iQueuedConvoDelay
							TEXT_LABEL_23 strTextBlock
							IF IS_PED_A_FOREMAN(eQueuedConvoSpeaker)
								strTextBlock = str_Dialogue
							ELSE
								strTextBlock = "SOL1AUD"
							ENDIF
							
							SWITCH eQueuedConvoSpeaker
								CASE mpf_bugstar_foreman_front
									ADD_PED_FOR_DIALOGUE(s_Convo, 4, peds[mpf_bugstar_foreman_front].id, "JHP1A_FOREMAN")
								BREAK
								CASE mpf_bugstar_foreman_rear
									ADD_PED_FOR_DIALOGUE(s_Convo, 5, peds[mpf_bugstar_foreman_rear].id, "JHP1A_FOREMAN2")
								BREAK
								CASE mpf_bugstar_front_1
									ADD_PED_FOR_DIALOGUE(s_Convo, 4, peds[mpf_bugstar_front_1].id, "CONSTRUCTION1")
								BREAK
								CASE mpf_bugstar_front_2
									ADD_PED_FOR_DIALOGUE(s_Convo, 3, peds[mpf_bugstar_front_2].id, "CONSTRUCTION2")
								BREAK
							ENDSWITCH
							
							BOOL bConvoStarted
							IF IS_STRING_NULL_OR_EMPTY(strQueuedConvoLine)
								IF CREATE_CONVERSATION(s_Convo, strTextBlock, strQueuedConvoRoot, CONV_PRIORITY_HIGH)
									bConvoStarted = TRUE
								ENDIF
							ELSE
								IF CREATE_CONVERSATION_FROM_SPECIFIC_LINE(s_Convo, strTextBlock, strQueuedConvoRoot, strQueuedConvoLine, CONV_PRIORITY_HIGH)
									bConvoStarted = TRUE
								ENDIF
							ENDIF

							IF bConvoStarted
							
								BROADCAST_DISPATCH_SCRIPT_AI_EVENT(peds[eQueuedConvoSpeaker].sQueuedBroadcastEvent, strQueuedConvoRoot)
								
								// Store current convo information
								#IF IS_DEBUG_BUILD
								eCurrentConvoSpeaker					= eQueuedConvoSpeaker
								strCurrentConvoRoot						= strQueuedConvoRoot
								#ENDIF
								eCurrentConvoPriority					= eQueuedConvoPriority
								bCurrentConvoCanBeInterruped			= bQueuedConvoCanBeInterrupted
								bCurrentConvoStoreForFutureResumption 	= bQueuedConvoStoreForFutureResumption
								bCurrentConvoKillAtDistance				= bQueuedConvoKillAtDistance
								
								// Reset queued convo
								eQueuedConvoSpeaker						= mpf_invalid
								eQueuedConvoPriority					= PEDAI_NONE
								strQueuedConvoRoot 						= ""
								iQueuedConvoDelay 						= 0
								fQueuedConvoDist						= 9999999
								bQueuedConvoInterrupt					= FALSE
								bQueuedConvoDoNotFinish					= FALSE
								bQueuedConvoCanBeInterrupted			= TRUE
								bQueuedConvoStoreForFutureResumption	= FALSE
								bQueuedConvoKillAtDistance				= FALSE
							ENDIF
						ENDIF
					ENDIF
				
				ENDIF
			
			// Ped queued to speak is dead, remove from queue
			ELSE
				eQueuedConvoSpeaker				= mpf_invalid
				eQueuedConvoPriority			= PEDAI_NONE
				strQueuedConvoRoot 				= ""
				iQueuedConvoDelay 				= 0
				fQueuedConvoDist				= 9999999
				bQueuedConvoInterrupt			= FALSE
				bQueuedConvoDoNotFinish			= FALSE
				bQueuedConvoCanBeInterrupted	= TRUE
				bQueuedConvoKillAtDistance		= FALSE
			ENDIF
			
		ELSE

//			IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
//				IF peds[eCurrentConvoSpeaker].eAI > eCurrentConvoPriority
//					IF peds[eCurrentConvoSpeaker].eAI >= PEDAI_COMBAT_PLAYER
//						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()					
//					ELSE
//						KILL_FACE_TO_FACE_CONVERSATION()
//					ENDIF
//				ENDIF
//			ENDIF
			
		ENDIF
		
		// >>>>>> PROCESS INTERRUPTED DIALOGUE <<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
		
		// Stop playing conversation if the player is out of range
		IF IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
		AND eCurrentConvoSpeaker != mpf_invalid
		AND bCurrentConvoKillAtDistance
		AND NOT IS_STRING_NULL_OR_EMPTY(strCurrentConvoRoot)
		
			VECTOR vCoord = GET_ENTITY_COORDS( peds[eCurrentConvoSpeaker].id )
		
			IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD( PLAYER_PED_ID(), vCoord ) >= F_CONVO_PLAY_DISTANCE
			
				eInterruptedConvoSpeaker			= eCurrentConvoSpeaker
				eInterruptedConvoPriority			= eCurrentConvoPriority
				strInterruptedConvoRoot				= GET_CURRENTLY_PLAYING_STANDARD_CONVERSATION_ROOT()
				strInterruptedConvoLine 			= GET_STANDARD_CONVERSATION_LABEL_FOR_FUTURE_RESUMPTION()
				bInterruptedConvoCanBeInterrupted 	= bCurrentConvoCanBeInterruped
				bInterruptedConvoKillAtDistance		= bCurrentConvoKillAtDistance
				
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				
				eCurrentConvoSpeaker 				= mpf_invalid
				eCurrentConvoPriority				= PEDAI_NONE
				strCurrentConvoRoot					= ""
				bCurrentConvoCanBeInterruped		= FALSE
				bCurrentConvoKillAtDistance			= FALSE
			
			ENDIF
		
		ENDIF
		
		
		// !!! WIPE OUT STORED CONVO IF THE PEDS AI IS NOW IN A HIGHER STATE THAN THE STORED PRIORITY
		IF eInterruptedConvoSpeaker != mpf_invalid
		AND NOT IS_STRING_NULL_OR_EMPTY(strInterruptedConvoRoot)
			IF peds[eInterruptedConvoSpeaker].eAI > eInterruptedConvoPriority
				eInterruptedConvoSpeaker				= mpf_invalid
				eInterruptedConvoPriority				= PEDAI_NONE
				strInterruptedConvoRoot					= ""
				strInterruptedConvoLine					= ""
				bInterruptedConvoCanBeInterrupted		= TRUE
			ENDIF
		ENDIF
		
		IF eQueuedConvoSpeaker = mpf_invalid
		AND IS_STRING_NULL_OR_EMPTY(strQueuedConvoRoot)
		
			IF IS_SAFE_TO_START_CONVERSATION()
			AND GET_GAME_TIMER() - i_TimeOfLastConvo > 10000
				IF eInterruptedConvoSpeaker != mpf_invalid
				AND NOT IS_STRING_NULL_OR_EMPTY(strInterruptedConvoRoot)
				
					PLAY_CONVERSATION(eInterruptedConvoSpeaker, 
						strInterruptedConvoRoot, eInterruptedConvoPriority, 
						FALSE, FALSE, bInterruptedConvoCanBeInterrupted, 
						0, TRUE, strInterruptedConvoLine, bInterruptedConvoKillAtDistance)
						
					eInterruptedConvoSpeaker				= mpf_invalid
					eInterruptedConvoPriority				= PEDAI_NONE
					strInterruptedConvoRoot					= ""
					strInterruptedConvoLine					= ""
					bInterruptedConvoCanBeInterrupted		= TRUE
				ENDIF
			ENDIF
		
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF bDisplayDebug
		
			TEXT_LABEL_63 strDebug
			iDebugRenderedThisFrame++
			
			// QUEUED CONVERSATION DEBUG
			strDebug = "eQueuedConvoSpeaker: "
			IF eQueuedConvoSpeaker != mpf_invalid
				strDebug += peds[eQueuedConvoSpeaker].strDebugName
			ENDIF
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 0,0,0,255)
			
			strDebug = "eQueuedConvoPriority: "
			strDebug += enum_to_int(eQueuedConvoPriority)
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 0,0,0,255)
			
			strDebug = "strQueuedConvoRoot: "
			strDebug += strQueuedConvoRoot
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 0,0,0,255)
			
			strDebug = "iQueuedConvoDelay: "
			strDebug += iQueuedConvoDelay
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 0,0,0,255)
			
			strDebug = "fQueuedConvoDist: "
			strDebug += GET_STRING_FROM_FLOAT(fQueuedConvoDist)
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 0,0,0,255)
			
			strDebug = "bQueuedConvoInterrupt: "
			IF bQueuedConvoInterrupt
				strDebug += "TRUE"
			ELSE
				strDebug += "FALSE"
			ENDIF
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 0,0,0,255)
			
			strDebug = "bQueuedConvoDoNotFinish: "
			IF bQueuedConvoDoNotFinish
				strDebug += "TRUE"
			ELSE
				strDebug += "FALSE"
			ENDIF
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 0,0,0,255)
			
			strDebug = "bQueuedConvoCanBeInterrupted: "
			IF bQueuedConvoCanBeInterrupted
				strDebug += "TRUE"
			ELSE
				strDebug += "FALSE"
			ENDIF
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 0,0,0,255)

			iDebugRenderedThisFrame++
			
			// CURRENT CONVERSATION DEBUG
			strDebug = "eCurrentConvoSpeaker: "
			IF eCurrentConvoSpeaker != mpf_invalid
				strDebug += peds[eCurrentConvoSpeaker].strDebugName
			ENDIF
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 0,0,0,255)
			
			strDebug = "eCurrentConvoPriority: "
			strDebug += enum_to_int(eCurrentConvoPriority)
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 0,0,0,255)
			
			strDebug = "strCurrentConvoRoot: "
			strDebug += strCurrentConvoRoot
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 0,0,0,255)

			strDebug = "bCurrentConvoCanBeInterruped: "
			IF bCurrentConvoCanBeInterruped
				strDebug += "TRUE"
			ELSE
				strDebug += "FALSE"
			ENDIF
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 0,0,0,255)
			
			// INTERRUPTED CONVERSATION DEBUG
			strDebug = "eInterruptedConvoSpeaker: "
			IF eInterruptedConvoSpeaker != mpf_invalid
				strDebug += peds[eInterruptedConvoSpeaker].strDebugName
			ENDIF
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 255,255,255,255)
			
			strDebug = "eInterruptedConvoPriority: "
			strDebug += enum_to_int(eInterruptedConvoPriority)
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 255,255,255,255)
			
			strDebug = "strInterruptedConvoRoot: "
			strDebug += strInterruptedConvoRoot
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 255,255,255,255)
			
			strDebug = "strInterruptedConvoLine: "
			strDebug += strInterruptedConvoLine
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 255,255,255,255)

			strDebug = "bInterruptedConvoCanBeInterrupted: "
			IF bInterruptedConvoCanBeInterrupted
				strDebug += "TRUE"
			ELSE
				strDebug += "FALSE"
			ENDIF
			DRAW_DEBUG_TEXT_2D_WRAPPED(strDebug, 255,255,255,255)
		
		ENDIF
		#ENDIF
	
	ENDIF
	
	DUMMY_REFERENCE_BOOL( bInterruptedConvoKillAtDistance )
ENDPROC

PROC EVENT_MUSIC_MANAGER(MISSION_EVENT_DATA &sData)

	IF NOT IS_MISSION_EVENT_ENDED(sData)
	
		IF sData.iStage <= 4
			IF IS_ALARM_PLAYING(str_Alarm) 
			OR IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			OR IS_MISSION_EVENT_TRIGGERED(s_Events[mef_alarms].sData)
				TRIGGER_MUSIC_EVENT("JHP1A_ATTACK")
				SET_MISSION_EVENT_STAGE(sData, 5)
			ENDIF
		ENDIF

		SWITCH sData.iStage
			CASE MISSION_EVENT_STAGE_START
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<137.70, -3092.81, 4.90>>) < 60.0
					TRIGGER_MUSIC_EVENT("JHP1A_START")
					SET_MISSION_EVENT_STAGE(sData, 2)
				ENDIF
			BREAK
			CASE 2
				IF e_PlayerStatus = PLAYERSTATUS_WAREHOUSE_IN_VEHICLE
				OR e_PlayerStatus = PLAYERSTATUS_WAREHOUSE_ON_FOOT
					TRIGGER_MUSIC_EVENT("JHP1A_WAREHOUSE")
					SET_MISSION_EVENT_STAGE(sData, 3)
				ENDIF
			BREAK
			CASE 3
				IF e_PlayerStatus = PLAYERSTATUS_VAN_ENTERING
				OR e_PlayerStatus = PLAYERSTATUS_VAN_STEALING
					TRIGGER_MUSIC_EVENT("JHP1A_VAN")
					SET_MISSION_EVENT_STAGE(sData, 4)
				ENDIF
			BREAK
			CASE 4
				IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)

					IF e_PlayerStatus = PLAYERSTATUS_VAN_STEALING
					AND GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<137.70, -3092.81, 4.90>>) >= 500
						IF PREPARE_MUSIC_EVENT("JHP1A_RADIO_1")
							TRIGGER_MUSIC_EVENT("JHP1A_RADIO_1")
							END_MISSION_EVENT(sData)
						ENDIF
					ENDIF
				
				ENDIF
			BREAK
			CASE 5
				IF IS_ALARM_PLAYING(str_Alarm) 
				AND IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				AND b_WantedLevelGiven
					SET_MISSION_EVENT_STAGE(sData, 6)
				ENDIF
			BREAK
			CASE 6
				IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					IF PREPARE_MUSIC_EVENT("JHP1A_RADIO_2")
						TRIGGER_MUSIC_EVENT("JHP1A_RADIO_2")
						SET_MISSION_EVENT_STAGE(sData, 6)
						END_MISSION_EVENT(sData)
					ENDIF
					
				ENDIF
			BREAK
		ENDSWITCH
	
	ENDIF
	
	// Cleanup
	IF IS_MISSION_EVENT_ENDED(sData)
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
	ENDIF

ENDPROC

/// PURPOSE:
///    Adds all the events for this mission
PROC ADD_ALL_MISSION_EVENTS()
	ADD_NEW_MISSION_EVENT(	s_Events[mef_bugstar_staff], 		&EVENT_AI_CONTROLLER,		 		"AI Controller")
	ADD_NEW_MISSION_EVENT(	s_Events[mef_dialogue], 			&EVENT_DIALOGUE,		 			"Dialogue")
	ADD_NEW_MISSION_EVENT(	s_Events[mef_alarms], 				&EVENT_ALARMS,		 				"Alarms & Wanted Level")
	ADD_NEW_MISSION_EVENT(	s_Events[mef_music_manager],		&EVENT_MUSIC_MANAGER, 				"Music Manager")
ENDPROC

//PURPOSE: Progresses to the specified stage and carries out and resets
PROC Stage_Jump_To(MISSION_STAGE_FLAGS eNewStage)
	IF eNewStage = msf_num_mission_stages
		SCRIPT_ASSERT("MSF_NUM_MISSION_STAGES is NOT a stage, used only to monitor the number of stages")
	ELSE
		i_missionSubstage 	= 0
		b_StageSetup		= FALSE
		i_missionStage 		= enum_to_int(eNewStage)
	ENDIF
ENDPROC

//PURPOSE: Processes any change from one stage to another that is not part of the mission flow.
PROC Stage_Skip_Update()
	// a skip has been made
	IF b_DoSkip = TRUE
	
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(1000)
			ENDIF
		ELSE
			IF IS_CUTSCENE_ACTIVE()
				STOP_CUTSCENE()
				REMOVE_CUTSCENE()
			ENDIF
		
			PRINT_DEBUG("[STAGE SKIP]: Starting Skip", 1)
			Stage_Jump_To(int_to_enum(MISSION_STAGE_FLAGS, i_SkipToStage))
					
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			CLEAR_PRINTS()
			CLEAR_HELP()
			
			Mission_Cleanup(TRUE)
			
		// Warping
		// -----------------------------------------------
		
			IF NOT IS_REPLAY_BEING_SET_UP()
			
				CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_SKIP() Performing NON-REPLAY skip warping")
				
				VECTOR vWarpCoord
				FLOAT fWarpHeading
				GET_SKIP_STAGE_COORD_AND_HEADING(i_missionStage, vWarpCoord, fWarpHeading)
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpCoord)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fWarpHeading)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				Load_Asset_NewLoadScene_Sphere(s_AssetData, vWarpCoord, 50.0)
				
				CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_SKIP() NON-REPLAY skip warp coord ", vWarpCoord, " warp heading ", fWarpHeading)
				
			ENDIF
			
		// Asset loading
		// -----------------------------------------------
			CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_SKIP() Requesting assets")
			
			Start_Skip_Streaming(s_AssetData)
		
			// Asset loading
			SWITCH int_to_enum(MISSION_STAGE_FLAGS, i_missionStage)
				CASE msf_bugstar_steal
					Load_Asset_Model(s_AssetData, BURRITO2)
					Load_Asset_Model(s_AssetData, S_M_Y_PESTCONT_01)
					Load_Asset_Model(s_AssetData, S_M_M_SECURITY_01)
					Load_Asset_Model(s_AssetData, DILETTANTE2)
					Load_Asset_Model(s_AssetData, P_AMB_CLIPBOARD_01)
					Load_Asset_Waypoint(s_AssetData, str_wpSecurityArrive)
					Load_Asset_AnimDict(s_AssetData, "misslsdhsclipboard@base")
					Load_Asset_AnimDict(s_AssetData, "misslsdhsclipboard@idle_a")
					Load_Asset_AnimDict(s_AssetData, "misstrevor3")
					Load_Asset_Interior(s_AssetData, interior_warehouse)
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
					AND NOT IS_THIS_MODEL_A_BOAT( GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() )
					AND NOT IS_THIS_MODEL_A_PLANE( GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() )
					AND NOT IS_THIS_MODEL_A_HELI( GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() )
						Load_Asset_Model(s_AssetData, GET_REPLAY_CHECKPOINT_VEHICLE_MODEL())
					ENDIF
				BREAK
				CASE msf_mission_passed
					Load_Asset_Model(s_AssetData, BURRITO2)
				BREAK
			ENDSWITCH
			
			WHILE NOT Update_Skip_Streaming(s_AssetData)
				WAIT(0)
				Mission_Entity_Death_Checks()
			ENDWHILE
			
			// Set up the mission for that current stage
			SWITCH int_to_enum(MISSION_STAGE_FLAGS, i_missionStage)
				CASE msf_bugstar_steal
					
					IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
					AND NOT IS_THIS_MODEL_A_BOAT( GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() )
					AND NOT IS_THIS_MODEL_A_PLANE( GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() )
					AND NOT IS_THIS_MODEL_A_HELI( GET_REPLAY_CHECKPOINT_VEHICLE_MODEL() )
						WHILE NOT HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
							WAIT(0)
						ENDWHILE
						vehs[mvf_replay].id = CREATE_REPLAY_CHECKPOINT_VEHICLE(<<189.3979, -2932.7214, 5.6127>>, 180.5455)
					ENDIF
					
					IF IS_REPLAY_BEING_SET_UP()
						IF DOES_ENTITY_EXIST(vehs[mvf_replay].id)
							END_REPLAY_SETUP(vehs[mvf_replay].id, VS_DRIVER)
						ELSE
							END_REPLAY_SETUP()
						ENDIF
					ELSE
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						IF DOES_ENTITY_EXIST(vehs[mvf_replay].id)
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehs[mvf_replay].id, VS_DRIVER)
						ENDIF
					ENDIF
					
					WHILE NOT PREPARE_INITIAL_SCENE(FALSE)
						WAIT(0)
					ENDWHILE
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				
				BREAK
				CASE msf_mission_passed

					IF NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_JEWELRY_PREP_2A)
					
						vehs[mvf_bugstar_van_1].id = CREATE_VEHICLE(BURRITO2, <<693.7250, -1006.3015, 21.8355>>, 359.8840)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_bugstar_van_1].id, FALSE)
						SET_VEHICLE_ON_GROUND_PROPERLY(vehs[mvf_bugstar_van_1].id)

					ENDIF
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ELSE
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
					
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<692.0670, -1004.8117, 21.9059>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 359.5735)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
				BREAK
			ENDSWITCH
			
			IF IS_SCREEN_FADED_OUT()
			OR (NOT IS_SCREEN_FADING_IN())
				IF NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_JEWELRY_PREP_2A)
				OR i_missionStage != enum_to_int(msf_mission_passed)
					DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
				ENDIF
			ENDIF
			b_DoSkip = FALSE
			PRINT_DEBUG("[STAGE SKIP]: Skip complete", 1)
		ENDIF
#IF IS_DEBUG_BUILD
	// Check is a skip is being asked for, and dont allow skip during setup stage
	ELIF IS_SCREEN_FADED_IN()
		INT i_currently_selected
	
		DONT_DO_J_SKIP(s_LocatesData)
		IF LAUNCH_MISSION_STAGE_MENU(s_ZMenuStruct, i_SkipToStage, i_currently_selected, FALSE, "Jewelry Heist Prep 1A", TRUE, TRUE)
			b_DoSkip = TRUE
		ELSE
			IF IS_KEYBOARD_KEY_PRESSED(KEY_S)			
				Mission_Passed()
			ENDIF
			IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
				Mission_Failed(mff_debug_forced)
			ENDIF
		ENDIF		
#ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: STEAL A BUGSTAR VAN
FUNC BOOL STAGE_BUGSTAR_VAN_STEAL()

	IF NOT b_StageSetup
		Load_Asset_Audiobank(s_AssetData, "SCRIPT\\JWL_HA_RAID_STORE")
		Load_Asset_Model(s_AssetData, S_M_M_SECURITY_01)
		
		TRIGGER_MISSION_EVENT(s_Events[mef_bugstar_staff].sData)
		TRIGGER_MISSION_EVENT(s_Events[mef_dialogue].sData)
		TRIGGER_MISSION_EVENT(s_Events[mef_music_manager].sData)
		
		SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<0,0,0>>, 0, TRUE, GET_CURRENT_PLAYER_PED_ENUM())
		
		g_iJewelPrep1AVanHealth = 1000
		SET_WANTED_LEVEL_MULTIPLIER(0.1)
	
		PRINT_NOW("JHP1A_STEAL_BSV", DEFAULT_GOD_TEXT_TIME, 1)
		b_StageSetup = TRUE
	ENDIF

	IF b_StageSetup
		SWITCH i_missionSubstage
			CASE 0

				// No vans left, mission will fail
				IF GET_NUM_OF_VANS_LEFT(TRUE) = 0

					CLEAR_MISSION_LOCATE_STUFF(s_LocatesData)
					
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(null)
					INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(null)
				
				// Vans are available to steal
				ELSE
				
					// Stealing normally by being in the van
					IF IS_PLAYER_IN_VAN()
					AND DOES_ENTITY_EXIST(vehs[GET_VAN_PLAYER_IS_IN()].id)
					AND IS_VEHICLE_DRIVEABLE(vehs[GET_VAN_PLAYER_IS_IN()].id)
					//AND NOT IS_VEHICLE_STUCK(vehs[GET_VAN_PLAYER_IS_IN()].id)
					
						SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
					
						IF e_PlayersLastVan != GET_VAN_PLAYER_IS_IN()
							
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
							
							INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehs[GET_VAN_PLAYER_IS_IN()].id)
							INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehs[GET_VAN_PLAYER_IS_IN()].id)
							e_PlayersLastVan = GET_VAN_PLAYER_IS_IN()
						
						ENDIF
						
						IF IS_PLAYER_AT_ANGLED_AREA_IN_VEHICLE(s_LocatesData, <<692.8256, -1012.5445, 21.7220>>, <<692.914307,-1003.555786,21.508389>>, <<692.651123,-1021.604126,26.206753>>, 9.750000, 
								TRUE, vehs[GET_VAN_PLAYER_IS_IN()].id, "", "", "", TRUE)
								
							i_missionSubstage++
							
						ENDIF
					
					// Tow truck steal
					ELIF IS_A_VAN_ATTACHED_TO_A_TOW_TRUCK()
					
						SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.5)
						
						IS_PLAYER_AT_LOCATION_IN_VEHICLE(s_LocatesData, <<692.8256, -1012.5445, 21.7220>>, <<0.1, 0.1, 0.1>>,
							TRUE, veh_TowTruckAttached, "", "", "", TRUE)
							
						IF IS_ENTITY_IN_ANGLED_AREA(vehs[GET_VAN_PLAYER_IS_TOWING()].id, <<692.914307,-1003.555786,21.508389>>, <<692.651123,-1021.604126,26.206753>>, 9.750000)
							i_missionSubstage++
						ENDIF					
						
					// Is not stealing a van
					ELSE
						
						CLEAR_MISSION_LOCATION_BLIP(s_LocatesData)
						
						INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(null)
						INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(null)
					
					ENDIF

				ENDIF
			BREAK
			CASE 1
				BOOL bHalted
			
				IF IS_PLAYER_IN_VAN()
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()), 5.0, 1)
						bHalted = TRUE
					ENDIF
				ELIF IS_A_VAN_ATTACHED_TO_A_TOW_TRUCK()
					IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(veh_TowTruckAttached, 5.0, 1)
						bHalted = TRUE
					ENDIF
				ENDIF
				
				IF bHalted
					IF GET_MISSION_COMPLETE_STATE(SP_HEIST_JEWELRY_PREP_2A)
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
						SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
						BLOCK_PLAYER_FOR_LEAD_IN(TRUE)
						UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
						i_missionSubstage = 1000
					ELSE
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
						i_missionSubstage++
					ENDIF
					
					IF IS_PLAYER_IN_VAN()
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[e_PlayersLastVan].id, FALSE)
					ELIF IS_A_VAN_ATTACHED_TO_A_TOW_TRUCK()
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[GET_VAN_PLAYER_IS_TOWING()].id, FALSE)
						SET_VEHICLE_DISABLE_TOWING(vehs[GET_VAN_PLAYER_IS_TOWING()].id, TRUE)
						DETACH_VEHICLE_FROM_ANY_TOW_TRUCK(vehs[GET_VAN_PLAYER_IS_TOWING()].id)
					ENDIF
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				ENDIF
			BREAK
			CASE 2
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE 1000
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				IF NOT IS_SAFE_TO_START_CONVERSATION()
					KILL_FACE_TO_FACE_CONVERSATION()
				ELSE
					ADD_PED_FOR_DIALOGUE(s_Convo, 3, null, "LESTER")
					IF PLAYER_CALL_CHAR_CELLPHONE(s_Convo, CHAR_LESTER, "JHFAUD", "JHF_P10c", CONV_PRIORITY_HIGH) 
						i_missionSubstage++
					ENDIF
				ENDIF
			BREAK
			CASE 1001
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				IF IS_CALLING_ANY_CONTACT()	
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					i_missionSubstage++
				ENDIF
			BREAK
			CASE 1002
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				IF NOT IS_CALLING_ANY_CONTACT()	
					CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN()
					RETURN TRUE
				ENDIF
			BREAK
			
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Processes the mission flow
PROC Stage_Flow()
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			Mission_Passed()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			Mission_Failed(mff_debug_forced)
		ENDIF
	#ENDIF

	SWITCH int_to_enum(MISSION_STAGE_FLAGS, i_missionStage)
		 
		CASE msf_bugstar_steal
			IF STAGE_BUGSTAR_VAN_STEAL() 	Stage_Jump_To(msf_mission_passed)	ENDIF
		BREAK
		
		CASE msf_mission_passed			
			Mission_Passed()
		BREAK
		
	ENDSWITCH
	
ENDPROC

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED()	
		Mission_Failed(mff_default)
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	// Do everything to prepare the mission
	Mission_Setup()
	ADD_ALL_MISSION_EVENTS()
	
	WHILE (TRUE)
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_JewelStoreJobPrep1A")	
	
		#IF IS_DEBUG_BUILD
			iDebugRenderedThisFrame = 0
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME)
				bDisplayDebug = NOT bDisplayDebug
				IF bDisplayDebug
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				ELSE
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)
				ENDIF
			ENDIF
		#ENDIF
	
		IF b_MissionFailed
			Mission_Fail_Update()
		ENDIF
		
		Process_Streaming(s_AssetData)		// Deals with loading any assets and keeps track of what has been loaded
		Stage_Skip_Update()					// processes any stage skipping that is required
		IF NOT b_DoSkip
			Mission_Entity_Death_Checks()	// Check if any of the mission critical entities (peds, vehicles, objects) are dead and fail accordingly
			Mission_Checks()				// Mission checks
			UPDATE_MISSION_EVENTS(s_Events)	// Manages the events
		ENDIF
		Stage_Flow()						// process the mission flow
	
		WAIT(0)
		
	ENDWHILE

ENDSCRIPT
