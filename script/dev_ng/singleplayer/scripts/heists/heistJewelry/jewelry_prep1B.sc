// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	STEAL SWAT VAN
//		AUTHOR			:	Craig Vincent
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_audio.sch"
USING "commands_camera.sch"
USING "commands_clock.sch"
USING "commands_debug.sch"
USING "commands_fire.sch"
USING "commands_graphics.sch"
USING "commands_hud.sch"
USING "commands_object.sch"
USING "commands_pad.sch"
USING "commands_ped.sch"
USING "commands_player.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "commands_task.sch"
USING "commands_vehicle.sch"
USING "commands_interiors.sch"
USING "commands_itemsets.sch"

USING "script_blips.sch"
Using "Locates_public.sch"
Using "select_mission_stage.sch"
USING "model_enums.sch"
USING "script_player.sch"
USING "script_misc.sch"
USING "selector_public.sch"
USING "chase_hint_cam.sch"
using "mission_stat_public.sch"
using "cutscene_public.sch"
USING "replay_public.sch"
USING "taxi_functions.sch"
USING "flow_mission_trigger_public.sch" //trigger stuff
using "script_ped.sch"
USING "battlebuddy_public.sch"
using "asset_management_public.sch"
using "RC_helper_functions.sch" 
USING "commands_recording.sch"

#IF IS_DEBUG_BUILD
	USING "shared_debug.sch"
	USING "script_debug.sch"
#ENDIF

//-----------------------------------------------------------------------------------------------------------
//		ENUMS
//-----------------------------------------------------------------------------------------------------------
ENUM MPF_MISSION_PED_FLAGS	
	mpf_mike,
	mpf_frank,	
	
	mpf_villian,
	mpf_hostage,
	
	MPF_NUM_OF_PEDS
ENDENUM
ENUM SPF_SWAT_PED_FLAGS
	spf_swat_D,
	spf_swat_1,
	spf_swat_2,
	spf_swat_3,
	
	SPF_NUM_OF_SWAT
ENDENUM
ENUM MVF_MISSION_VEHICLE_FLAGS
	mvf_FBI2,
	mvf_Replay_car,
	
	MVF_NUM_OF_VEH
ENDENUM
ENUM MSF_MISSION_STAGE_FLAGS
	msf_0_Wander,
	msf_1_get_away,
	msf_2_passed,
	
	MSF_NUM_OF_STAGES
ENDENUM
ENUM MFF_MISSION_FAIL_FLAGS
	mff_debug_fail,
	mff_default,
	mff_goods_destroyed,
	mff_left_truck,
	mff_at_police_station,
	mff_player_dead,
	MFF_NUM_OF_FAILS
ENDENUM
Enum STAGE_SWITCHSTATE
	STAGESWITCH_IDLE,
	STAGESWITCH_REQUESTED,
	STAGESWITCH_EXITING,
	STAGESWITCH_ENTERING	
ENDENUM
ENUM BLOCK_STAGE
	DRIVING,
	BLOCKED,
	RETURNING
ENDENUM
ENUM ENUM_SWAT_STAGE
	SWATSTAGE_START_STAGE,
	SWATSTAGE_ONROUTE,
	SWATSTAGE_ARREST,
	SWATSTAGE_ALLDEAD
endenum
enum ENUM_HOST_STAGE
	HOST_CALM,
	HOST_REACT,
	HOST_EXIT
endenum
enum ENUM_REACT
	react_good,
	react_bad
endenum
ENUM ENUM_SWATSTAGE_ARREST_STAGE
	SWATSTAGE_ARREST_REACT,
	SWATSTAGE_ARREST_PLAYER,
	SWATSTAGE_ARREST_RELEASE	
ENDENUM
enum eSWAT_OPTION
	eNONE
endenum
enum SWATSTAGE_ARREST_TYPE
	eSWATSTAGE_ARREST_VAN2,
	eSWATSTAGE_ARREST_PART	
endenum

//-----------------------------------------------------------------------------------------------------------
//		STRUCTS
//-----------------------------------------------------------------------------------------------------------
STRUCT VEHICLE_STRUCT
	VEHICLE_INDEX 		id
	BLIP_INDEX			blip
endstruct
Struct PEDS_STRUCT
	PED_INDEX			id	
	BLIP_INDEX			blip
ENDSTRUCT
struct SWAT_STRUCT
	ped_index				id
	blip_index				blip
	AI_BLIP_STRUCT			blipstruct	
	int 					itargeted
	bool					bkilled		= false
	bool					bCharge		= false
endstruct
//-----------------------------------------------------------------------------------------------------------
//		CONSTANTS
//-----------------------------------------------------------------------------------------------------------
CONST_INT				STAGE_ENTRY				0
CONST_INT				STAGE_EXIT				-1 

VECTOR					vFACTORY	= <<692.8256, -1012.5445, 21.7220>>
//-----------------------------------------------------------------------------------------------------------
//		VARIABLES
//-----------------------------------------------------------------------------------------------------------

VEHICLE_STRUCT					Vehs[MVF_NUM_OF_VEH]	//holds all of the vehicles for tis level
PEDS_STRUCT						peds[MPF_NUM_OF_PEDS]	//holds all of the peds for this level

SWAT_STRUCT						swat[SPF_NUM_OF_SWAT]			
structPedsForConversation 		convo_struct			//holds peds in the conversation
LOCATES_HEADER_DATA 			sLocatesData

SEQUENCE_INDEX					seq						//used to create AI sequence

WEAPON_TYPE						wcurrent
object_index 					objCrate

//-----------audio-----------------
int 	DialogueTimer1
//bool	b_dialogue_played
//================= stage game variable's -----

bool				bleftWarn 					= false
TEXT_LABEL_63 		sWaypoint 					= ""
BOOL				bWantedDispatchBlocked		= TRUE
BOOL				b_PlayedPoliceScannerLine	= FALSE

//----------------------- AI -----
bool					bSWATSTAGE_ARREST
ENUM_SWAT_STAGE			swatstage
ENUM_SWATSTAGE_ARREST_STAGE		eSWATSTAGE_ARRESTstage
BLOCK_STAGE 			blockstage
INT						iblockTimer
INT						iblockstage

bool					bcuedstop 				= true
Int 					iNegAnims 				= 0
INT						iSpecAbilHlp		 	= 0

//============================== streaming ============================== 
ASSET_MANAGEMENT_DATA	sAssetData

//----------------

//Stage Management and skip stuff
STAGE_SWITCHSTATE		stageswitch				//current switching status
INT						mission_stage			//current stage
INT						mission_substage		//current substage
INT						requestedStage			//the mission stage requested by a mission_stage switch
INT						iStageTimer				//timer used for debug
Bool					bDoSkip					//trigger the skip
INT						iSkipToStage			//the stage to skip to
#if IS_DEBUG_BUILD
	MissionStageMenuTextStruct 	zMenuNames[MSF_NUM_OF_STAGES]
	WIDGET_GROUP_ID 			widget_debug
	BOOL						bDisplayDebug
#endif

// ===========================================================================================================
//		Termination
// ===========================================================================================================
// -----------------------------------------------------------------------------------------------------------
//		Mission Helpers
// -----------------------------------------------------------------------------------------------------------
FUNC BOOL IsEntityAlive(ENTITY_INDEX mEntity)
  	If DOES_ENTITY_EXIST(mEntity)
		if IS_ENTITY_A_VEHICLE(mEntity)
			if IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(mEntity))
				return TRUE 
			endif
		elif IS_ENTITY_A_PED(mEntity)
			if not IS_PED_INJURED(GET_PED_INDEX_FROM_ENTITY_INDEX(mEntity))
	          	return TRUE 
	    	ENDIF	
		endif           
	ENDIF
      return FALSE
ENDFUNC
func string	GET_NEGANIM()
	switch iNeganims
		case 0
			iNeganims++
			return "idle_a"
		break
		case 1
			iNeganims++
			return "idle_b"			
		break
		case 2
			iNeganims = 0
			return "idle_c"			
		break
	endswitch
return ""	
endfunc
PROC Point_Gameplay_cam_at_coord(float targetHeading)	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(targetHeading - GET_ENTITY_HEADING(Player_ped_id()))
endproc
FUNC ped_index MIKE()
	return peds[mpf_mike].id
ENDFUNC
func ped_index FRANK()
	return peds[mpf_frank].id
endfunc
func bool Check_Replay_Car(vector pos, float heading)
	//check 
	if IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
		REQUEST_REPLAY_CHECKPOINT_VEHICLE_MODEL()
		while not HAS_REPLAY_CHECKPOINT_VEHICLE_LOADED()
			wait(0)
			CPRINTLN(DEBUG_MIKE, "WAITING FOR REPLAY VEHICLE!")
		endwhile
		vehs[mvf_Replay_car].id = CREATE_REPLAY_CHECKPOINT_VEHICLE(pos,heading)
		SET_ENTITY_LOAD_COLLISION_FLAG(vehs[mvf_Replay_car].id,true)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehs[mvf_Replay_car].id,true)
		return true
	else
		return true
	endif
return false
endfunc

func bool Grab_mission_entites()
	
	g_replay.iReplayInt[0] = g_sTriggerSceneAssets.id
	sWaypoint = g_JP1b_string_WayPoint
	//g_JP1b_string_WayPoint = ""
	TS_SET_BLIP_COORD(STATIC_BLIP_MISSION_JEWELRY_P1B,<<0,0,2000>>)
	
	// Grab the target
	IF NOT DOES_ENTITY_EXIST(vehs[mvf_FBI2].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
			vehs[mvf_FBI2].id = g_sTriggerSceneAssets.veh[0]
			SET_VEHICLE_AS_RESTRICTED(vehs[mvf_FBI2].id,0)
			RESET_ENTITY_ALPHA(vehs[mvf_FBI2].id)
			ADD_VEHICLE_UPSIDEDOWN_CHECK(vehs[mvf_FBI2].id)
		ENDIF
	ENDIF	
	
// -----------Grab the swat----------
	IF NOT DOES_ENTITY_EXIST(swat[spf_swat_D].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
			swat[spf_swat_D].id = g_sTriggerSceneAssets.ped[0]
			SET_PED_ACCURACY(swat[spf_swat_D].id, 7)
			//INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(swat[spf_swat_D].id)
			SET_PED_RELATIONSHIP_GROUP_HASH(swat[spf_swat_D].id, RELGROUPHASH_SECURITY_GUARD)
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(swat[spf_swat_1].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[1])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[1], TRUE, TRUE)
			swat[spf_swat_1].id = g_sTriggerSceneAssets.ped[1]
			SET_PED_ACCURACY(swat[spf_swat_1].id, 7)
			//INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(swat[spf_swat_1].id)
			SET_PED_RELATIONSHIP_GROUP_HASH(swat[spf_swat_1].id, RELGROUPHASH_SECURITY_GUARD)
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(swat[spf_swat_2].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[2])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[2], TRUE, TRUE)
			swat[spf_swat_2].id = g_sTriggerSceneAssets.ped[2]
			SET_PED_ACCURACY(swat[spf_swat_2].id, 7)
			//INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(swat[spf_swat_2].id)
			SET_PED_RELATIONSHIP_GROUP_HASH(swat[spf_swat_2].id, RELGROUPHASH_SECURITY_GUARD)
		ENDIF
	ENDIF
	IF NOT DOES_ENTITY_EXIST(swat[spf_swat_3].id)		
		IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[3])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[3], TRUE, TRUE)
			swat[spf_swat_3].id = g_sTriggerSceneAssets.ped[3]
			SET_PED_ACCURACY(swat[spf_swat_3].id, 7)
			//INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(swat[spf_swat_3].id)
			SET_PED_RELATIONSHIP_GROUP_HASH(swat[spf_swat_3].id, RELGROUPHASH_SECURITY_GUARD)
		ENDIF
	ENDIF	
	
	IF NOT DOES_ENTITY_EXIST(objCrate)
		if does_entity_exist(g_sTriggerSceneAssets.object[0])
			SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.object[0],true,true)
			objCrate = g_sTriggerSceneAssets.object[0]			
		endif
	endif
			
	int SWATped
	for SWATped = 0 to enum_to_int(SPF_NUM_OF_SWAT) -1
		if IsEntityAlive(swat[SWATped].id)			
			RESET_ENTITY_ALPHA(swat[SWATped].id)
			//SET_ENTITY_CAN_ONLY_BE_DAMAGED_BY_SCRIPT_PARTICIPANTS(swat[SWATped].id,true)
		ENDIF
	ENDFOR
			
	TS_SET_BLIP_COORD(STATIC_BLIP_MISSION_JEWELRY_P1B,<<0,0,2000>>)
	
	if DOES_ENTITY_EXIST(vehs[mvf_FBI2].id)
	and DOES_ENTITY_EXIST(objCrate)
	and DOES_ENTITY_EXIST(swat[spf_swat_D].id)
	and DOES_ENTITY_EXIST(swat[spf_swat_1].id)
	and DOES_ENTITY_EXIST(swat[spf_swat_2].id)
	and DOES_ENTITY_EXIST(swat[spf_swat_3].id)
		RETURN TRUE
	endif
	
RETURN FALSE
endfunc
func enumSubtitlesState GetSubTitlesState(ped_index ped)
	float subDist = 50
	if IsEntityAlive(ped)
		if GET_DISTANCE_BETWEEN_ENTITIES(player_ped_id(),ped) < subDist
			return DISPLAY_SUBTITLES
		else
			return DO_NOT_DISPLAY_SUBTITLES
		endif
	endif
return DISPLAY_SUBTITLES	
endfunc
proc AUDIO_manage_dialouge()
	int SWATped
	for SWATped = 0 to enum_to_int(SPF_NUM_OF_SWAT) -1
		if isEntityalive(swat[SWATped].id)
		and get_game_timer() - dialogueTimer1 > 10000
			if not is_ambient_speech_playing(swat[SWATped].id)				
				play_ped_ambient_speech_with_voice(swat[SWATped].id,"","",speech_params_force)
				dialogueTimer1 = get_game_Timer()
			endif				
		endif
	endfor
endproc

proc create_swat_in_vehicle(SPF_SWAT_PED_FLAGS swatped,VEHICLE_INDEX veh,bool bPolice = false)
	
	VEHICLE_SEAT 	vsSeat
	MODEL_NAMES		model		
	WEAPON_TYPE		wtSWATpedWpn 	
	if bpolice
		model			= S_M_Y_COP_01
		wtSWATpedWpn 	= WEAPONTYPE_PISTOL
	else
		model			= S_M_Y_SWAT_01
		wtSWATpedWpn 	= WEAPONTYPE_SMG
	endif
	
	if 	swatped  	= spf_swat_D
		vsSeat 		= VS_DRIVER	
	elif swatped  	= spf_swat_1
		vsSeat 		= VS_FRONT_RIGHT
	elif swatped  	= spf_swat_2
		vsSeat 		= VS_BACK_LEFT				
	elif swatped  	= spf_swat_3
		vsSeat 		= VS_BACK_RIGHT	
	endif
	
	swat[swatped].id = CREATE_PED_INSIDE_VEHICLE(veh,PEDTYPE_COP,model,vsSeat)
		
	SET_PED_AS_ENEMY(swat[swatped].id, TRUE)	
	GIVE_WEAPON_TO_PED(swat[swatped].id, wtSWATpedWpn, INFINITE_AMMO, TRUE,true)
	SET_PED_COMBAT_ABILITY(swat[swatped].id,CAL_PROFESSIONAL)
	SET_PED_COMBAT_ATTRIBUTES(swat[swatped].id, CA_DISABLE_BLOCK_FROM_PURSUE_DURING_VEHICLE_CHASE, TRUE)
	SET_PED_COMBAT_ATTRIBUTES(swat[swatped].id, CA_DISABLE_CRUISE_IN_FRONT_DURING_BLOCK_DURING_VEHICLE_CHASE, TRUE)
	SET_PED_MONEY(swat[swatped].id,0)		
	SET_ENTITY_LOD_DIST(swat[swatped].id,250)
	//SET_PED_ALERTNESS(swat[swatped].id,AS_ALERT)
	SET_ENTITY_AS_MISSION_ENTITY(swat[swatped].id)
	//combat attributes		
	SET_PED_TO_LOAD_COVER(swat[swatped].id,true)
	SET_PED_CONFIG_FLAG(swat[swatped].id,PCF_PreventAutoShuffleToDriversSeat,true)
	STOP_PED_SPEAKING(swat[swatped].id,true)	
	SET_ENTITY_LOAD_COLLISION_FLAG(swat[swatped].id,TRUE)		
	SET_PED_ACCURACY(swat[swatped].id,7)
	SET_PED_AS_COP(swat[swatped].id)
	SET_PED_TARGET_LOSS_RESPONSE(swat[swatped].id,TLR_SEARCH_FOR_TARGET)
	SET_PED_CAN_PEEK_IN_COVER(swat[swatped].id, TRUE)
	SET_PED_ARMOUR(swat[swatped].id, 25)
	SET_PED_RELATIONSHIP_GROUP_HASH(swat[swatped].id, RELGROUPHASH_SECURITY_GUARD)
	if not bpolice
		SET_PED_COMPONENT_VARIATION(swat[swatped].id,PED_COMP_DECL,0,0)
	endif
	swat[swatped].bkilled = false
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL tDebugName = "SWAT: "
		tDebugName += enum_to_int(swatped) + 1
		SET_PED_NAME_DEBUG(swat[swatped].id, tDebugName)
	#ENDIF
	
endproc
proc SWAT_manage_Drive()
	vector vFrontMax
	vector vFrontMin
		
	if IsEntityAlive(vehs[mvf_FBI2].id)
	and IsEntityAlive(swat[spf_swat_D].id)	
	and IsEntityAlive(swat[spf_swat_3].id)
		vFrontMax 	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Vehs[mvf_FBI2].id,<<0,10,5.5>>)
		vFrontMin 	= GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(Vehs[mvf_FBI2].id,<<0,0,-6>>)
		IF NOT IS_VEHICLE_SIREN_ON(vehs[mvf_FBI2].id)
			SET_VEHICLE_ACT_AS_IF_HAS_SIREN_ON(vehs[mvf_FBI2].id,true)
			SET_VEHICLE_SIREN(vehs[mvf_FBI2].id,true)
		ENDIF
		switch blockstage			
			case DRIVING
				if IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),vFrontMax,vFrontMin,5)
					if GET_GAME_TIMER() - iblockTimer > 1000
						
						CLEAR_PED_TASKS(swat[spf_swat_D].id)
						BRING_VEHICLE_TO_HALT(Vehs[mvf_FBI2].id,4,1)
						
						OPEN_SEQUENCE_TASK(seq)
							if IS_PED_IN_ANY_VEHICLE(swat[spf_swat_3].id)
								TASK_LEAVE_ANY_VEHICLE(null, 0, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
							endif
							TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(null,player_ped_id(),player_ped_id(),PEDMOVE_WALK,false,3)
							TASK_AIM_GUN_AT_ENTITY(null,player_ped_id(),-1)
						CLOSE_SEQUENCE_TASK(seq)
						TASK_PERFORM_SEQUENCE(swat[spf_swat_3].id,seq)
						CLEAR_SEQUENCE_TASK(seq)
						
						blockstage = BLOCKED
						
					endif
				else
					if IS_PED_IN_ANY_VEHICLE(swat[spf_swat_3].id)	
					and IS_PED_IN_ANY_VEHICLE(swat[spf_swat_D].id)
						if not IS_WAYPOINT_PLAYBACK_GOING_ON_FOR_VEHICLE(vehs[mvf_FBI2].id)
							TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(swat[spf_swat_D].id,vehs[mvf_FBI2].id,sWaypoint,
							DRIVINGMODE_AVOIDCARS,0,EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN,-1,25)
						endif						
						SET_VEHICLE_USE_MORE_RESTRICTIVE_SPAWN_CHECKS(vehs[mvf_FBI2].id,true)
						SET_VEHICLE_ACT_AS_IF_HAS_SIREN_ON(vehs[mvf_FBI2].id,false)
						SET_VEHICLE_WILL_TELL_OTHERS_TO_HURRY(vehs[mvf_FBI2].id,true)

					endif
					iblockTimer = get_game_timer()
				endif
				
			break
			case BLOCKED
				switch iblockstage
					case 0	
						if not IS_PED_IN_ANY_VEHICLE(swat[spf_swat_3].id)						
							ADD_PED_FOR_DIALOGUE(convo_struct,1,swat[spf_swat_3].id,"SWAT1")								
							if create_CONVERSATION(convo_struct,"JP1bAUD","JP1b_WANDER",CONV_PRIORITY_MEDIUM)
								iblockTimer = get_game_timer()
								iblockstage++
							endif
						endif						
					break
					case 1
						if get_game_timer() - iblockTimer > 5000
						and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							if not IS_PED_IN_ANY_VEHICLE(swat[spf_swat_3].id)						
								ADD_PED_FOR_DIALOGUE(convo_struct,1,swat[spf_swat_3].id,"SWAT1")								
								if create_CONVERSATION(convo_struct,"JP1bAUD","JP1b_WANDER",CONV_PRIORITY_MEDIUM)
									iblockTimer = get_game_timer()
									iblockstage++
								endif							
							endif
						endif
					break
					case 2
						if get_game_timer() - iblockTimer > 5000
						and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							if not IS_PED_IN_ANY_VEHICLE(swat[spf_swat_3].id)						
								ADD_PED_FOR_DIALOGUE(convo_struct,1,swat[spf_swat_3].id,"SWAT1")								
								if create_CONVERSATION(convo_struct,"JP1bAUD","JP1b_WANDER",CONV_PRIORITY_MEDIUM)
									iblockTimer = get_game_timer()
									iblockstage++
								endif							
							endif
						endif
					break
					case 3
						if get_game_timer() - iblockTimer > 8000
						and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()						
							if not IS_PED_IN_ANY_VEHICLE(swat[spf_swat_3].id)						
								ADD_PED_FOR_DIALOGUE(convo_struct,1,swat[spf_swat_3].id,"SWAT1")								
								if create_CONVERSATION(convo_struct,"JP1bAUD","JP1b_WANDER",CONV_PRIORITY_MEDIUM)
									OPEN_SEQUENCE_TASK(seq)											
										TASK_GO_TO_ENTITY_WHILE_AIMING_AT_ENTITY(null,player_ped_id(),player_ped_id(),PEDMOVE_WALK,false,4)												
										TASK_AIM_GUN_AT_ENTITY(null,player_ped_id(),-1)
									CLOSE_SEQUENCE_TASK(seq)
									TASK_PERFORM_SEQUENCE(swat[spf_swat_3].id,seq)
									CLEAR_SEQUENCE_TASK(seq)
									iblockTimer = get_game_timer()
									iblockstage++
								endif
							endif
						endif
					break
					case 4						
					break								
				endswitch
				
				if not IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),vFrontMax,vFrontMin,5)	
					if GET_SCRIPT_TASK_STATUS(swat[spf_swat_3].id,script_task_aim_gun_at_entity) != PERFORMING_TASK
						TASK_AIM_GUN_AT_ENTITY(swat[spf_swat_3].id,PLAYER_PED_ID(),-1)
					endif
					if timera() > 3000
						if not IS_PED_IN_ANY_VEHICLE(swat[spf_swat_3].id)
							TASK_ENTER_VEHICLE(swat[spf_swat_3].id,Vehs[mvf_FBI2].id,DEFAULT_TIME_NEVER_WARP,VS_BACK_RIGHT,PEDMOVEBLENDRATIO_RUN)								
						endif
						blockstage = RETURNING
					endif
				else
					if iblockstage = 4
						if get_game_timer() - iblockTimer > 8000
						and not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()	
							TASK_ARREST_PED(swat[spf_swat_3].id,player_ped_id())
							bSWATSTAGE_ARREST = true							
						endif
					endif
					SETTIMERA(0)					
				endif
			break
			case RETURNING
				if  IS_ENTITY_IN_ANGLED_AREA(player_ped_id(),vFrontMax,vFrontMin,5)					
					blockstage = DRIVING		
				else				
					if (NOT IsEntityAlive(swat[spf_swat_D].id) OR IS_PED_IN_ANY_VEHICLE(swat[spf_swat_D].id))
					and (NOT IsEntityAlive(swat[spf_swat_1].id) OR IS_PED_IN_ANY_VEHICLE(swat[spf_swat_1].id))
					and (NOT IsEntityAlive(swat[spf_swat_2].id) OR IS_PED_IN_ANY_VEHICLE(swat[spf_swat_2].id))
					and (NOT IsEntityAlive(swat[spf_swat_3].id) OR IS_PED_IN_ANY_VEHICLE(swat[spf_swat_3].id))
						TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(swat[spf_swat_D].id,vehs[mvf_FBI2].id,sWaypoint,
								DRIVINGMODE_AVOIDCARS,0,EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN,-1,25)		
						blockstage = DRIVING			
					endif
				endif
			break
		endswitch
	endif
endproc
proc SWATSTAGE_ARREST_check()
// ------------------------------------------------------------- SWATSTAGE_ARREST CHECKS ----------------------------------------------------------------------------------		
				
	//---------------------------- peds attacked ----------------------------	
	GET_CURRENT_PED_WEAPON(player_ped_id(),wcurrent)
	int SWATped
	for SWATped = 0 to enum_to_int(SPF_NUM_OF_SWAT) - 1
		if IsEntityAlive(swat[SWATped].id)
			if IS_PED_IN_COMBAT(swat[SWATped].id,player_ped_id())	
			or IS_PED_IN_MELEE_COMBAT(swat[SWATped].id)
			or IS_PED_BEING_STUNNED(swat[SWATped].id)
			or IS_PED_BEING_STEALTH_KILLED(swat[SWATped].id)
			or IS_PED_BEING_JACKED(swat[SWATped].id)
			or HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(swat[SWATped].id,player_ped_id())	
			or HAS_PED_RECEIVED_EVENT(swat[SWATped].id,EVENT_SHOT_FIRED)
			or HAS_PED_RECEIVED_EVENT(swat[SWATped].id,EVENT_SHOT_FIRED_WHIZZED_BY)
			or HAS_PED_RECEIVED_EVENT(swat[SWATped].id,EVENT_SHOT_FIRED_BULLET_IMPACT) 
				bSWATSTAGE_ARREST = true
				println("eSWATSTAGE_ARREST_PART 1")
			
			elif HAS_ENTITY_CLEAR_LOS_TO_ENTITY_IN_FRONT(swat[SWATped].id,player_ped_id())
			and (IS_PLAYER_FREE_AIMING_AT_ENTITY(player_id(),swat[SWATped].id)or IS_PLAYER_TARGETTING_ENTITY(player_id(),swat[SWATped].id))						
			 	if GET_WEAPONTYPE_GROUP(wcurrent) != WEAPONGROUP_MELEE
			 	and wcurrent != WEAPONTYPE_UNARMED
					if get_game_timer() - swat[SWATped].itargeted > 800						
						println("eSWATSTAGE_ARREST_PART 2")
						bSWATSTAGE_ARREST = true
					endif
				endif
			else
				swat[SWATped].itargeted = get_game_Timer()
			endif
//		elif not IsEntityAlive(swat[SWATped].id)
//		and swat[SWATped].bkilled
//			println("eSWATSTAGE_ARREST_PART 3")
//			bSWATSTAGE_ARREST = FALSE
		endif
	endfor		
	//---------------------------- transport veh attacked ----------------------------	
	if IsEntityAlive(vehs[mvf_FBI2].id)
						
		if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehs[mvf_FBI2].id,player_ped_id(),false)	
		or HAS_VEHICLE_GOT_PROJECTILE_ATTACHED(player_ped_id(),vehs[mvf_FBI2].id)
		or IS_PED_ON_SPECIFIC_VEHICLE(player_ped_id(),vehs[mvf_FBI2].id)
			println("eSWATSTAGE_ARREST_VAN2 1")
			bSWATSTAGE_ARREST = true
		elif IS_PED_IN_ANY_VEHICLE(player_ped_id())
			if IS_PED_IN_MODEL(player_ped_id(),TOWTRUCK)
			or IS_PED_IN_MODEL(player_ped_id(),TOWTRUCK2)
				if IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(GET_VEHICLE_PED_IS_IN(player_ped_id()),vehs[mvf_FBI2].id)
					println("eSWATSTAGE_ARREST_VAN2 2")
					bSWATSTAGE_ARREST = true
				endif
			endif
		endif
		if IS_PED_IN_ANY_VEHICLE(player_ped_id())
			if GET_ENTITY_SPEED(GET_VEHICLE_PED_IS_IN(player_ped_id())) > 7.5
				if HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(vehs[mvf_FBI2].id,GET_VEHICLE_PED_IS_IN(player_ped_id()))
					println("eSWATSTAGE_ARREST_VAN2 3")
					bSWATSTAGE_ARREST = true
				endif							
			else
				CLEAR_ENTITY_LAST_DAMAGE_ENTITY(vehs[mvf_FBI2].id)
			endif
		endif
	else
		println("eSWATSTAGE_ARREST_VAN2 4")
		bSWATSTAGE_ARREST = true
	endif	
	
	
endproc
//proc Get_close_ped()
//	int SWATped
//	float closedist = 9999
//	for SWATped = 0 to enum_to_int(SPF_NUM_OF_swat)-1
//		if IsEntityAlive(swat[SWATped].id)
//			if GET_DISTANCE_BETWEEN_ENTITIES(swat[SWATped].id,player_peD_id()) < closedist
//				closedist = GET_DISTANCE_BETWEEN_ENTITIES(swat[SWATped].id,player_peD_id())
//				CloseCop = swat[SWATped].id
//			endif
//		endif
//	endfor
//endproc
func bool IS_PLAYER_CLOSE_TO_SWAT()
	int SWATped
	for SWATped = 0 to enum_to_int(SPF_NUM_OF_swat)-1
		if IsEntityAlive(swat[SWATped].id)
			if GET_DISTANCE_BETWEEN_ENTITIES(swat[SWATped].id,player_peD_id()) < 6.7
				return true
			endif
		endif
	endfor
return false	
endfunc
PROC SWAT_AI()
SPF_SWAT_PED_FLAGS SWATped
	switch SwatStage
		case SWATSTAGE_START_STAGE
			SwatStage =	SWATSTAGE_ONROUTE
		break				
		case SWATSTAGE_ONROUTE
			if not bSWATSTAGE_ARREST
				SWATSTAGE_ARREST_check()
				SWAT_manage_Drive()			
			else		
				swatstage = SWATSTAGE_ARREST	
				eSWATSTAGE_ARRESTstage = SWATSTAGE_ARREST_REACT
			endif
		break
//----------------------------------------------------------------- COMBAT -----------------------------------------------------------------		
		case SWATSTAGE_ARREST
			switch eSWATSTAGE_ARRESTstage	
				case SWATSTAGE_ARREST_REACT						
					SET_PLAYER_WANTED_LEVEL_NO_DROP(player_id(),1)
					SET_PLAYER_WANTED_LEVEL_NOW(player_id())
					for SWATped = 0 to enum_to_int(SPF_NUM_OF_swat)-1
						if IsEntityAlive(swat[SWATped].id)
						and IsEntityAlive(player_ped_id())							
							//set general combat stuff that wont change				
							SET_PED_TO_LOAD_COVER(swat[SWATped].id, FALSE)
							SET_PED_CONFIG_FLAG(swat[SWATped].id,PCF_PreventAutoShuffleToDriversSeat,false)
							//Combat task for non drivers
							if  SWATped != spf_swat_D
								int iPause	
								SWITCH SWATped
									CASE spf_swat_1				ipause = 300			BREAK
									CASE spf_swat_2				ipause = 3000			BREAK
									CASE spf_swat_3				ipause = 3300			BREAK
								ENDSWITCH
								if IS_PED_IN_ANY_VEHICLE(swat[SWATped].id)
									TASK_LEAVE_ANY_VEHICLE(swat[SWATped].id,ipause, ECF_DONT_DEFAULT_WARP_IF_DOOR_BLOCKED)
								endif									
							endif
							
						endif
					endfor	
					eSWATSTAGE_ARRESTstage = SWATSTAGE_ARREST_PLAYER
				break
				case SWATSTAGE_ARREST_PLAYER	
					
					IF NOT IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)

						BOOL bAllDead
						bAllDead = TRUE
				
						for SWATped = 0 to enum_to_int(SPF_NUM_OF_swat)-1

							if IsEntityAlive(swat[SWATped].id)	
							AND GET_DISTANCE_BETWEEN_ENTITIES(vehs[mvf_FBI2].id, swat[SWATped].id) < 100
									
								VEHICLE_SEAT eSeat
								SWITCH SWATped
									CASE spf_swat_D				eSeat = VS_DRIVER				BREAK
									CASE spf_swat_1				eSeat = VS_FRONT_RIGHT			BREAK
									CASE spf_swat_2				eSeat = VS_BACK_LEFT			BREAK
									CASE spf_swat_3				eSeat = VS_BACK_RIGHT			BREAK
								ENDSWITCH
							
								IF NOT IS_PED_IN_VEHICLE(swat[SWATped].id, vehs[mvf_FBI2].id, TRUE)
									IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(swat[SWATped].id, SCRIPT_TASK_ENTER_VEHICLE)
										TASK_ENTER_VEHICLE(swat[SWATped].id, vehs[mvf_FBI2].id, DEFAULT_TIME_NEVER_WARP, eSeat, PEDMOVE_RUN)
									ENDIF
								ELSE
									CLEAR_PED_TASKS(swat[SWATped].id)
								ENDIF
								
								bAllDead = FALSE
								
							ELSE
								SAFE_RELEASE_PED(swat[spf_swat_D].id)
							ENDIF
						endfor
						
					
						IF bAllDead
							SwatStage 			= SWATSTAGE_ALLDEAD
						ELSE
							bSWATSTAGE_ARREST 	= FALSE
							SwatStage 			= SWATSTAGE_ONROUTE
							blockstage 			= RETURNING
						ENDIF
					ENDIF
				break
			endswitch			
		break
	endswitch	
endproc
FUNC BOOL DOES_BUDDY_NEED_TO_DRIVE_TRUCK(PED_INDEX hPed)

    IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()  
        IF IsEntityAlive(vehs[mvf_FBI2].id)
        and not IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_FBI2].id,<<1364.24829, 3620.51123, 33.89069>>,<<1350.89954, 3616.22485, 37.12358>>,6.5)                              
	       
			IF IsEntityAlive(PLAYER_PED_ID())
	       	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehs[mvf_FBI2].id)
	            IF IS_PED_IN_VEHICLE(hPed, vehs[mvf_FBI2].id)
	            AND GET_PED_IN_VEHICLE_SEAT(vehs[mvf_FBI2].id, VS_DRIVER) = hPed
	                RETURN TRUE                        
	            ENDIF
	        ENDIF
			
        ENDIF
  	ENDIF
      
      RETURN FALSE
ENDFUNC
FUNC BOOL DOES_BUDDY_NEED_TO_FOLLOW_CONVOY(PED_INDEX hPed)

    IF NOT IS_PLAYER_PED_SWITCH_IN_PROGRESS()  
        IF IsEntityAlive(vehs[mvf_FBI2].id)
        and swatstage = SWATSTAGE_ONROUTE                              
	       
			IF IsEntityAlive(PLAYER_PED_ID())
	       	AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	            IF IS_PED_IN_VEHICLE(hPed, GET_VEHICLE_PED_IS_IN(player_ped_id()))
	            AND GET_PED_IN_VEHICLE_SEAT(GET_VEHICLE_PED_IS_IN(player_ped_id()), VS_DRIVER) = hPed
	                RETURN TRUE                        
	            ENDIF
	        ENDIF
			
        ENDIF
  	ENDIF
      
      RETURN FALSE
ENDFUNC
PROC MONITOR_BATTLE_BUDDIES()
    // For each playable character...
    enumCharacterList eChar
    REPEAT MAX_BATTLE_BUDDIES eChar
            
        PED_INDEX hBuddy = GET_BATTLEBUDDY_PED(eChar)
        
        IF NOT IS_PED_INJURED(hBuddy)
            IF NOT IS_BATTLEBUDDY_OVERRIDDEN(hBuddy)              
    // Does script need to take control of buddy and drive to dest?
                IF IS_BATTLEBUDDY_AVAILABLE(hBuddy, FALSE)
                    IF DOES_BUDDY_NEED_TO_DRIVE_TRUCK(hBuddy)
					or DOES_BUDDY_NEED_TO_FOLLOW_CONVOY(hBuddy)
                        IF OVERRIDE_BATTLEBUDDY(hBuddy, FALSE)
                            SET_ENTITY_AS_MISSION_ENTITY(hBuddy, TRUE, TRUE)
                            SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(hBuddy, TRUE)
                            CLEAR_PED_TASKS(hBuddy)
                        ENDIF
                    ENDIF
                ENDIF              
            ELSE
	// Does script still need to take control of buddy and drive to dest?
                IF DOES_BUDDY_NEED_TO_DRIVE_TRUCK(hBuddy)
                AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddy)                          
	                // Buddy drives truck to destination
	                IF  GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
	                AND GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
	                    TASK_VEHICLE_MISSION_COORS_TARGET(hBuddy, GET_VEHICLE_PED_IS_IN(hBuddy), vFACTORY,
														  MISSION_GOTO, 30.0, DRIVINGMODE_AVOIDCARS, 2.0, -1)
	                ENDIF   
				elif DOES_BUDDY_NEED_TO_FOLLOW_CONVOY(hBuddy)
				AND DOES_ENTITY_BELONG_TO_THIS_SCRIPT(hBuddy) 
					// Buddy drives truck behind convoy
					if IsEntityAlive(swat[spf_swat_D].id)
		                IF  GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> PERFORMING_TASK
		                AND GET_SCRIPT_TASK_STATUS(hBuddy, SCRIPT_TASK_VEHICLE_MISSION) <> WAITING_TO_START_TASK
		                    TASK_VEHICLE_MISSION_PED_TARGET(hBuddy, GET_VEHICLE_PED_IS_IN(hBuddy), swat[spf_swat_D].id,
															  MISSION_FOLLOW, 15.0, DRIVINGMODE_AVOIDCARS, 15.0, -1)
		                ENDIF 
					endif
                ELSE
                    RELEASE_BATTLEBUDDY(hBuddy)
                ENDIF              
            ENDIF
        ENDIF      
   ENDREPEAT
ENDPROC

PROC reset_everything()

	int i

//==================================== Destroy ====================================== 
//all peds are deleted
	for i = 0 TO Enum_to_int(MPF_NUM_OF_PEDS)-1
		if DOES_ENTITY_EXIST(peds[i].id)
		AND (NOT IS_PED_INJURED(peds[i].id))
			if IS_PED_IN_ANY_VEHICLE(peds[i].id)				
				SET_PED_COORDS_NO_GANG(peds[i].id,(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(peds[i].id))+<<0,-2,0>>))				
			endif		
			SAFE_DELETE_PED(peds[i].id)
		endif
	endfor	
	for i = 0 to enum_to_int(SPF_NUM_OF_SWAT) -1
		if DOES_ENTITY_EXIST(swat[i].id)
			CLEANUP_AI_PED_BLIP(swat[i].blipstruct)
			swat[i].bCharge	= false
			if not IS_PED_INJURED(swat[i].id)
				if IS_PED_IN_ANY_VEHICLE(swat[i].id)				
					SET_PED_COORDS_NO_GANG(swat[i].id,(GET_ENTITY_COORDS(GET_VEHICLE_PED_IS_IN(swat[i].id))+<<0,-2,0>>))				
				endif
				SAFE_DELETE_PED(swat[i].id)
			ENDIF
		ENDIF
	ENDFOR	
	//all vehicles are deleted
	for i = 0 TO Enum_to_int(MVF_NUM_OF_VEH)-1
		if DOES_ENTITY_EXIST(vehs[i].id)	
			EMPTY_VEHICLE(vehs[i].id)
			SAFE_DELETE_VEHICLE(vehs[i].id)
		endif
	endfor	
	
	safe_delete_object(objCrate)
	
	REMOVE_SCENARIO_BLOCKING_AREAS()
	CLEAR_PRINTS()
	CLEAR_HELP()
	DESTROY_ALL_CAMS()
	
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
	
//====================================  Reset  ======================================
	
	//reset player control
	if IsEntityAlive(player_ped_id())
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		CLEAR_PLAYER_WANTED_LEVEL(player_id())
	endif
	//clear AI tasks
	if not IS_PED_INJURED(player_ped_id())	
		CLEAR_PED_TASKS(player_ped_id())
	endif	
		
	DISPLAY_RADAR(true)
	DISPLAY_HUD(true)		
	
	swatstage 				= SWATSTAGE_START_STAGE	
	eSWATSTAGE_ARRESTstage			= SWATSTAGE_ARREST_REACT
	blockstage				= DRIVING
	iblockstage 			= 0
	bSWATSTAGE_ARREST 				= false
	bWantedDispatchBlocked 	= TRUE
		
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Cleanup()

	PRINTSTRING("...Rural Bank Prep1 - Mission Cleanup")
	PRINTNL()
	
//======================================= Destroy ======================================		
	
	CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
	int i
	for i = 0 TO Enum_to_int(MPF_NUM_OF_PEDS)-1
		if IsEntityAlive(peds[i].id)
			if peds[i].id != player_ped_id()
				SET_PED_KEEP_TASK(peds[i].id,true)
				SAFE_RELEASE_PED(peds[i].id)
			endif
			if DOES_BLIP_EXIST(peds[i].blip)
				REMOVE_BLIP(peds[i].blip)
			endif
		endif
	endfor		
//-------------------- swat-----------------------
	for i = 0 to ENUM_TO_INT(SPF_NUM_OF_SWAT) -1	
		if IsEntityAlive(swat[i].id)	
			SAFE_RELEASE_PED(swat[i].id)
			if DOES_BLIP_EXIST(swat[i].blip)	
				REMOVE_BLIP(swat[i].blip)
			endif
			CLEANUP_AI_PED_BLIP(swat[i].blipstruct)
		endif
	endfor
//---------------------vehs-----------------------
	for i = 0 TO Enum_to_int(MVF_NUM_OF_VEH)-1
		if IsEntityAlive(vehs[i].id)		 
			SAFE_RELEASE_VEHICLE(vehs[i].id)
		endif
		if DOES_BLIP_EXIST(vehs[i].blip)
			SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(TRUE)
				REMOVE_BLIP(vehs[i].blip)
			SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(FALSE)
		endif
	endfor	
	
	safe_release_object(objCrate)
	
	DISABLE_TAXI_HAILING(FALSE) //PD requested by bug #312601
	CLEAR_PRINTS()
	STOP_AUDIO_SCENES()
//====================================  Reset  ======================================
		//reset player control
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)	
	
//	RESET_ALL_BATTLEBUDDY_BEHAVIOUR_REQUESTS()

	SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<<-827.59247, -1101.78198, 14.48773>>, <<-812.16101, -1081.68372, 8>>, true)	
	SET_WANTED_LEVEL_MULTIPLIER(1)	//wanted level for mission reset to normal
	SET_MAX_WANTED_LEVEL(5)
	DISPLAY_RADAR(true)
	DISPLAY_HUD(true)	
	SET_PED_PATHS_BACK_TO_ORIGINAL(<<-788.13446, -1122.98328, 8>>,<<-867.92529, -1051.79126, 15.58141>>)
	REMOVE_SCENARIO_BLOCKING_AREAS()
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(FBI2,false)
	
	SET_SCENARIO_GROUP_ENABLED("MP_POLICE", TRUE)
	
	swatstage 	= SWATSTAGE_START_STAGE
	bSWATSTAGE_ARREST 	= false	
	TERMINATE_THIS_THREAD()
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Passed()

	PRINTSTRING("...Rural Prep 1 Mission Passed")
	PRINTNL()
	Mission_Flow_Mission_Passed(FALSE, TRUE)
	TRIGGER_MISSION_STATS_UI(TRUE,TRUE)
	Mission_Cleanup()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------


PROC Mission_Failed(MFF_MISSION_FAIL_FLAGS fail_condition = mff_default)
	TRIGGER_MUSIC_EVENT("JHP1B_FAIL")
	
	STRING strReason = ""
		
	//show fail message
	SWITCH fail_condition
		CASE mff_debug_fail
			strReason = ""
		BREAK	
		case mff_goods_destroyed
			strReason = "JHP1B_VAN_DEAD"
		break		
		case mff_left_truck
			strReason = "JHP1B_ABAN"
		break
		case mff_at_police_station
			strReason = "JHP1B_STAT"
		break
		DEFAULT
			strReason = ""
		BREAk
	ENDSWITCH
	
	MISSION_FLOW_MISSION_FAILED_WITH_REASON(strReason)
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
		//Maintain anything that could look weird during fade out (e.g. swat walking off). 
		WAIT(0)
		CPRINTLN(DEBUG_MIKE, "WAITING FOR FAIL!")
	ENDWHILE
		
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
		
	MISSION_CLEANUP()
	TERMINATE_THIS_THREAD()
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		MISSION STAGE MANAGEMENT
// -----------------------------------------------------------------------------------------------------------

PROC Mission_stage_management()
	SWITCH stageswitch
		CASE STAGESWITCH_REQUESTED
			PRINTLN("[stageManagement] mission_stage switch requested from mission_stage:", mission_stage, "to mission_stage", requestedStage)
			stageswitch = STAGESWITCH_EXITING
			mission_substage = STAGE_EXIT
		BREAK
		CASE STAGESWITCH_EXITING
			PRINTLN("[StageManagement] Exiting mission_stage: ", mission_stage)
			stageSwitch = STAGESWITCH_ENTERING
			mission_substage = STAGE_ENTRY
			mission_stage = requestedStage
		BREAK
		CASE STAGESWITCH_ENTERING
			PRINTLN("[StageManagement] Entered mission_stage: ", mission_stage)
			requestedStage = -1
			stageSwitch = STAGESWITCH_IDLE
		BREAK
		CASE STAGESWITCH_IDLE
			IF (GET_GAME_TIMER() - iStageTimer) > 2500
			PRINTLN("[StageManagement] mission_stage: ", mission_stage, " mission_substage: ", mission_substage)
				iStageTimer = GET_GAME_TIMER()
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC
FUNC BOOL Mission_Set_Stage(MSF_MISSION_STAGE_FLAGS newStage)
	If stageswitch = STAGESWITCH_IDLE
		requestedstage = ENUM_TO_INT(newStage)
		stageswitch = STAGESWITCH_REQUESTED
		return True
	else
		return false
	endif
ENDFUNC

proc load_asset_stage(MSF_MISSION_STAGE_FLAGS loadStage)	
	SWITCH loadStage	
		CASE msf_0_Wander
			load_asset_model(sAssetData,S_M_Y_SWAT_01)
			Load_Asset_Model(sAssetData,FBI2)			
			Load_Asset_Waypoint(sAssetData,sWaypoint)
			Load_Asset_Model(sAssetData,PROP_BOX_AMMO03A)	
		break	
		case msf_1_get_away
			Load_Asset_Model(sAssetData,FBI2)
			Load_Asset_Model(sAssetData,PROP_BOX_AMMO03A)
		break
		case msf_2_passed
			Load_Asset_Model(sAssetData,FBI2)
			Load_Asset_Model(sAssetData,PROP_BOX_AMMO03A)
		break
	ENDSWITCH
endproc

PROC GET_SKIP_STAGE_COORD_AND_HEADING(MSF_MISSION_STAGE_FLAGS eStage, VECTOR &vCoord, FLOAT &fHeading)
	
	SWITCH eStage
		CASE msf_0_Wander
			IF ARE_STRINGS_EQUAL(sWaypoint, "JHP1BRoute2")
				vCoord = <<-1517.2207, -654.4871, 28.1211>>	fHeading = 300.0097
			else
				vCoord = <<895.8368, -788.0649, 41.9022>>	fHeading = 	25.9344						
			endif			
		BREAK	
		CASE msf_2_passed 
			vCoord = <<693.7152, -1005.2172, 21.8993>>	
			fHeading = 180.4314		
		BREAK	
	ENDSWITCH

ENDPROC
PROC SU_GENERAL()
	if IsEntityAlive(player_ped_id())
		ADD_PED_FOR_DIALOGUE(convo_struct, 0, Player_ped_id(), "MICHAEL")
		SET_ENTITY_LOAD_COLLISION_FLAG(Player_ped_id(),true)
		RESTORE_PLAYER_PED_VARIATIONS(Player_ped_id())
		SET_PED_CONFIG_FLAG(Player_ped_id(),PCF_WillFlyThroughWindscreen,false)
	endif
	
	if IsEntityAlive(vehs[mvf_FBI2].id)
		objCrate = CREATE_OBJECT(PROP_BOX_AMMO03A,GET_ENTITY_COORDS(vehs[mvf_FBI2].id))
		ATTACH_ENTITY_TO_ENTITY(objCrate,vehs[mvf_FBI2].id,0,<<0,-1.77,0.0>>,<<0,0,90>>)
		ADD_VEHICLE_UPSIDEDOWN_CHECK(vehs[mvf_FBI2].id)
	endif
	
endproc
proc SU_msf_0_Wander()
	vector vVeh_current_node_pos
	vector vVeh_prev_node_pos
	float fSpawnHeading 
	
	// Create player vehicle
	WAYPOINT_RECORDING_GET_COORD(sWaypoint, 0, vVeh_current_node_pos)	
	WAYPOINT_RECORDING_GET_COORD(sWaypoint, 1, vVeh_prev_node_pos)	
	fSpawnHeading = GET_HEADING_BETWEEN_VECTORS_2D(vVeh_current_node_pos, vVeh_prev_node_pos)
	CLEAR_AREA_OF_VEHICLES(vVeh_current_node_pos, 20.0)
	WHILE NOT CREATE_PLAYER_VEHICLE(vehs[mvf_Replay_car].id, CHAR_MICHAEL, vVeh_current_node_pos, fSpawnHeading, TRUE)
		WAIT(0)
	ENDWHILE
	CLEAR_AREA_OF_VEHICLES(vVeh_current_node_pos, 20.0)

	SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL), TRUE)
		
	WAYPOINT_RECORDING_GET_COORD(sWaypoint, 10, vVeh_current_node_pos)	
	WAYPOINT_RECORDING_GET_COORD(sWaypoint, 11, vVeh_prev_node_pos)
	fSpawnHeading = GET_HEADING_BETWEEN_VECTORS_2D(vVeh_current_node_pos, vVeh_prev_node_pos)
	
	CLEAR_AREA_OF_VEHICLES(vVeh_current_node_pos, 20.0)
	vehs[mvf_FBI2].id = CREATE_VEHICLE(fbi2,vVeh_current_node_pos,fSpawnHeading )	
	SET_ENTITY_HEALTH(vehs[mvf_FBI2].id,(GET_ENTITY_HEALTH(vehs[mvf_FBI2].id)*2))	
	SET_VEHICLE_STRONG(vehs[mvf_FBI2].id,true)
	SET_VEHICLE_ENGINE_HEALTH(vehs[mvf_FBI2].id,(GET_VEHICLE_ENGINE_HEALTH(vehs[mvf_FBI2].id)*2))
	SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehs[mvf_FBI2].id,false)
	SET_VEHICLE_PETROL_TANK_HEALTH(vehs[mvf_FBI2].id,(GET_VEHICLE_PETROL_TANK_HEALTH(vehs[mvf_FBI2].id)*2))
	SET_VEHICLE_PROVIDES_COVER(vehs[mvf_FBI2].id,true)
	SET_VEHICLE_CAN_BREAK(vehs[mvf_FBI2].id, FALSE)
   	SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehs[mvf_FBI2].id, SC_DOOR_BOOT, FALSE)
	SET_VEHICLE_TYRES_CAN_BURST(vehs[mvf_FBI2].id, FALSE)
	SET_VEHICLE_AS_RESTRICTED(vehs[mvf_FBI2].id,0)
			
	int SWATped
	for SWATped = 0 to enum_to_int(spf_swat_3)
		if not IsEntityAlive(swat[SWATped].id)
			create_swat_in_vehicle(int_to_enum(SPF_SWAT_PED_FLAGS, SWATped),vehs[mvf_FBI2].id)
			//INFORM_MISSION_STATS_OF_HEADSHOT_WATCH_ENTITY(swat[SWATped].id)			
		endif
	endfor
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP(vehs[mvf_Replay_car].id, VS_DRIVER)
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehs[mvf_Replay_car].id)
	endif
	
	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
	if IsEntityAlive(swat[spf_swat_D].id)
	and IsEntityAlive(vehs[mvf_FBI2].id)
		TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(swat[spf_swat_D].id, vehs[mvf_FBI2].id,sWaypoint, DRIVINGMODE_AVOIDCARS,0,EWAYPOINT_START_FROM_CLOSEST_POINT| EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN,-1,25)	
		FORCE_PED_AI_AND_ANIMATION_UPDATE(swat[spf_swat_D].id)
	endif
	clear_Area(get_entity_coords(vehs[mvf_FBI2].id),100,true)	
endproc

PROC su_passed()

	vehs[mvf_FBI2].id = CREATE_VEHICLE(fbi2,<<693.7152, -1005.2172, 21.8993>>, 180.4314)
	SET_ENTITY_HEALTH(vehs[mvf_FBI2].id,(GET_ENTITY_HEALTH(vehs[mvf_FBI2].id)*2))	
	SET_VEHICLE_STRONG(vehs[mvf_FBI2].id,true)
	SET_VEHICLE_ENGINE_HEALTH(vehs[mvf_FBI2].id,(GET_VEHICLE_ENGINE_HEALTH(vehs[mvf_FBI2].id)*2))
	SET_VEHICLE_EXPLODES_ON_HIGH_EXPLOSION_DAMAGE(vehs[mvf_FBI2].id,false)
	SET_VEHICLE_PETROL_TANK_HEALTH(vehs[mvf_FBI2].id,(GET_VEHICLE_PETROL_TANK_HEALTH(vehs[mvf_FBI2].id)*2))
	SET_VEHICLE_PROVIDES_COVER(vehs[mvf_FBI2].id,true)
	SET_VEHICLE_CAN_BREAK(vehs[mvf_FBI2].id, FALSE)
   	SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehs[mvf_FBI2].id, SC_DOOR_BOOT, FALSE)
	SET_VEHICLE_TYRES_CAN_BURST(vehs[mvf_FBI2].id, FALSE)
	SET_VEHICLE_AS_RESTRICTED(vehs[mvf_FBI2].id,0)
	
	IF IS_REPLAY_BEING_SET_UP()
		END_REPLAY_SETUP(vehs[mvf_FBI2].id, VS_DRIVER)
	Else
		FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), False)	
		SET_PED_INTO_VEHICLE(player_ped_id(),vehs[mvf_FBI2].id)
	endif
	clear_Area(get_entity_coords(vehs[mvf_FBI2].id),100,true)
endproc
proc mission_stage_skip()
//a skip has been made
	if bdoskip = true
		
		//begin the skip if the switching is idle
		if stageswitch = stageswitch_idle
			if not is_screen_faded_out()
				if not is_screen_fading_out()
					do_screen_fade_out(DEFAULT_FADE_TIME)
				endif
			else
				mission_set_stage(int_to_enum(msf_mission_stage_flags, iskiptostage))
			endif
		//needs to be carried out before states own entering stage
		elif stageswitch = stageswitch_entering
			
			render_script_cams(false,false)
			set_player_control(player_id(), true)
			
			reset_everything()
						
			Start_Skip_Streaming(sAssetData)	
			// -------------------------- Not a Replay ----------------------------
			IF NOT IS_REPLAY_BEING_SET_UP()					
				VECTOR vWarpCoord
				FLOAT fWarpHeading
				GET_SKIP_STAGE_COORD_AND_HEADING(int_to_enum(msf_mission_stage_flags, iSkipToStage), vWarpCoord, fWarpHeading)				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpCoord)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fWarpHeading)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				Load_Asset_NewLoadScene_Sphere(sAssetData, vWarpCoord, 50.0)
			ENDIF
			// --------------------------------------------------------------------
				
			Load_asset_Stage(int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage))		
			WHILE NOT Update_Skip_Streaming(sAssetData)
				WAIT(0)
				CPRINTLN(DEBUG_MIKE, "WAITING FOR SKIP STREAMING!")
			ENDWHILE					
			
			switch int_to_enum(msf_mission_stage_flags, mission_stage)						
				case msf_0_Wander			SU_msf_0_Wander()		break	
				case msf_2_passed			su_passed() 			break
			endswitch
			
			SU_GENERAL()
			
			bdoskip = false
			IF NOT IS_REPLAY_BEING_SET_UP()	
				NEW_LOAD_SCENE_STOP()
			endif
		endif
#if is_debug_build
	//check is a skip being asked for, dont allow skip during setup stage
	elif launch_mission_stage_menu(zmenunames, iskiptostage, mission_stage, true)
		if iskiptostage > enum_to_int(msf_num_of_stages)-1
			mission_passed()
		else
			iskiptostage = clamp_int(iskiptostage, 0, enum_to_int(msf_num_of_stages)-1)
			if is_screen_faded_in()
				do_screen_fade_out(default_fade_time_long)
				bdoskip = true
			endif
		endif
#endif
	endif
endproc
PROC fail_checks()

	//At Police station
	if isentityalive(vehs[mvf_FBI2].id)
	and IsEntityAlive(player_ped_id())		
		if is_entity_in_angled_area(vehs[mvf_FBI2].id,<<457.57620, -1014.58716, 23>>,<<427.63681, -1014.97357, 33>>,27)
			if isentityalive(swat[spf_swat_D].id)
				if is_ped_in_vehicle(swat[spf_swat_D].id,vehs[mvf_FBI2].id)	
					mission_failed(mff_at_police_station)			
				endif
			endif
		endif
	endif	
	
	//distace check 
	if IsEntityAlive(vehs[mvf_FBI2].id)
	and IsEntityAlive(PLAYER_PED_ID())
		if GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehs[mvf_FBI2].id),GET_ENTITY_COORDS(player_ped_id())) > 700
			Mission_Failed(mff_left_truck)
		elif GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehs[mvf_FBI2].id),GET_ENTITY_COORDS(player_ped_id())) > 600
			if not bleftWarn
				IF SwatStage =	SWATSTAGE_ONROUTE
					PRINT_NOW("JHP1B_WARN_ESC",DEFAULT_GOD_TEXT_TIME,1)
				ELSE
					PRINT_NOW("JHP1B_WARN_LVE",DEFAULT_GOD_TEXT_TIME,1)
				ENDIF
				bleftWarn = true
			endif
		else
			bleftWarn = false
		endif
	endif
endproc
proc mission_checks()
//------------------death checks-------------------------------
	int i
	for i = 0 to enum_to_int(mpf_num_of_peds) -1
		if does_entity_exist(peds[i].id)
			if is_ped_injured(peds[i].id)
				SAFE_RELEASE_PED(peds[i].id)
			endif
		endif
	endfor
	for i = 0 to enum_to_int(mvf_num_of_veh) -1
		if does_entity_exist(vehs[i].id)
			if not is_vehicle_driveable(vehs[i].id)			
				if vehs[i].id = vehs[mvf_FBI2].id
					mission_failed(mff_goods_destroyed)
				endif
				SAFE_RELEASE_VEHICLE(vehs[i].id)
			else
				if vehs[i].id = vehs[mvf_FBI2].id
					if IS_VEHICLE_PERMANENTLY_STUCK(vehs[mvf_FBI2].id)
						mission_failed(mff_goods_destroyed)
						SAFE_RELEASE_VEHICLE(vehs[i].id)
					endif
				endif
			endif
		endif
	endfor
	for i = 0 to enum_to_int(SPF_NUM_OF_SWAT) -1
		if does_entity_exist(swat[i].id)					
			if is_ped_injured(swat[i].id)
				if does_blip_exist(swat[i].blip)
					remove_blip(swat[i].blip)
				endif
				if swat[i].bkilled = false
					//inform_mission_stats_of_increment(rh1p_kills)
					swat[i].bkilled = true
				endif
				SAFE_RELEASE_PED(swat[i].id)
			else
				if not IS_PED_IN_ANY_VEHICLE(swat[i].id)
					SET_PED_CONFIG_FLAG(swat[i].id, PCF_DontBlip, FALSE)
				else
					SET_PED_CONFIG_FLAG(swat[i].id, PCF_DontBlip, TRUE)
				endif
			endif
		endif
	endfor
//------------------------------------------------------------------		
							// random peds to flee
	SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(player_id())
	
							// head track truck
	if IsEntityAlive(vehs[mvf_FBI2].id)		
	
		IF NOT b_PlayedPoliceScannerLine
			IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehs[mvf_FBI2].id, TRUE)
					PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_JSH_PREP_1B_01", 0.0)
					b_PlayedPoliceScannerLine = TRUE
				ENDIF
			ENDIF
		ENDIF
	
		bool bAlldead
		bAlldead = true
		int iSWATped
		for iSWATped =  0 to enum_to_int(SPF_NUM_OF_SWAT) -1
			if IsEntityAlive(swat[iSWATped].id)			
				bAlldead = false
			endif
		endfor
		
		IF bWantedDispatchBlocked
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehs[mvf_FBI2].id)
			OR IS_PLAYER_TOWING_VEHICLE(vehs[mvf_FBI2].id)
			OR bAlldead
				ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, 		TRUE)
				ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, 		TRUE) 
				ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, 		TRUE)
				ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, 			TRUE)
				ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, 		TRUE)
				ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, 		TRUE)
				ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, 	TRUE)
				bWantedDispatchBlocked = FALSE
				
				IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
					SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 3)
					SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				ENDIF
			ENDIF
			
		ENDIF

		if bAlldead
			if not IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehs[mvf_FBI2].id)
				if not IS_PED_HEADTRACKING_ENTITY(player_ped_id(),vehs[mvf_FBI2].id)
					TASK_LOOK_AT_ENTITY(player_ped_id(),vehs[mvf_FBI2].id,-1,SLF_WHILE_NOT_IN_FOV,SLF_LOOKAT_LOW)
				endif
			endif
		endif
	endif
	
	//stats
	if does_entity_exist(player_ped_id())
	and not is_ped_injured(player_ped_id())
		if is_ped_in_any_vehicle(player_ped_id())
			inform_mission_stats_of_damage_watch_entity(get_vehicle_ped_is_in(player_ped_id()))
			inform_mission_stats_of_speed_watch_entity(get_vehicle_ped_is_in(player_ped_id()))
		else
			inform_mission_stats_of_damage_watch_entity(null) 
			inform_mission_stats_of_speed_watch_entity(null)
		endif
		inform_mission_stats_of_damage_watch_entity(player_ped_id())
	endif
	
	FAIL_CHECKS()		
	SWAT_AI()
endproc
proc mission_setup()	
	
	SET_START_CHECKPOINT_AS_FINAL() // tells replay controller to display "skip mission" for shitskips
		
	if IS_REPEAT_PLAY_ACTIVE()
	
		iSkipToStage = enum_to_int(msf_0_Wander)

		IF IS_REPLAY_IN_PROGRESS()
			IF g_bShitskipAccepted
				iSkipToStage = enum_to_int(msf_2_passed)
				CPRINTLN(DEBUG_MISSION, "JHP1B: g_bShitskipAccepted = TRUE")
			ENDIF
		ENDIF
		
		sWaypoint = "JHP1BRoute2" 
				
		// New replay streaming 
		IF IS_REPLAY_IN_PROGRESS()
			VECTOR vSkipToCoord
			FLOAT fSkipToHeading
			GET_SKIP_STAGE_COORD_AND_HEADING(int_to_enum(MSF_MISSION_STAGE_FLAGS,iSkipToStage), vSkipToCoord, fSkipToHeading)
			START_REPLAY_SETUP(vSkipToCoord, fSkipToHeading)
			CPRINTLN(DEBUG_MISSION, "JHP1B: Using replay warp system - Warp Coord ", vSkipToCoord, " Warp Heading ", fSkipToHeading)
		ENDIF
		bdoskip = true	
		
	endif		
	mission_substage = stage_entry			
	
	request_additional_text("JHP1B", mission_text_slot)
	
	if is_player_playing(player_id())
		set_player_control(player_id(), true)	
	endif			
		
	set_vehicle_model_is_suppressed(FBI2,true)
	
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_ROAD_BLOCK, FALSE) 
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_HELICOPTER, FALSE)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, FALSE)
	
	SET_SCENARIO_GROUP_ENABLED("MP_POLICE", FALSE)
	
	if NOT is_replay_in_progress()		
	AND NOT IS_REPEAT_PLAY_ACTIVE()
		while not grab_mission_entites()
			wait(0)
			CPRINTLN(DEBUG_MIKE, "grab_mission_entites() stuck trying to grab the entitys from the trigger scene")
		endwhile
		
		mission_stage = enum_to_int(msf_0_Wander)					
		load_asset_stage(msf_0_Wander)
	endif
	
	WHILE g_replay.replayStageID = RS_NOT_RUNNING
		WAIT(0)
		CPRINTLN(DEBUG_MIKE, "waiting for replay control to initialise")
	ENDWHILE
	
endproc
// -----------------------------------------------------------------------------------------------------------
//		Mission stages
// -----------------------------------------------------------------------------------------------------------
proc ST_0_WANDER()

	SWITCH iSpecAbilHlp
		CASE 0
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
			AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
		
				BOOL bShowHelpNow
				INT i
				REPEAT SPF_NUM_OF_SWAT i
					IF DOES_ENTITY_EXIST(swat[i].id)
					AND NOT IS_PED_INJURED(swat[i].id)
						IF IS_PED_IN_COMBAT(swat[i].id, PLAYER_PED_ID())
							bShowHelpNow = TRUE
						ENDIF
					ENDIF
				ENDREPEAT
				
				IF bShowHelpNow
					SPECIAL_ABILITY_CHARGE_NORMALIZED(PLAYER_ID(), 0.5, TRUE)
					FLASH_ABILITY_BAR(DEFAULT_HELP_TEXT_TIME*2)
					IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
						PRINT_HELP("JHP1B_HLP_SPCL1_KM", DEFAULT_HELP_TEXT_TIME)
					ELSE
						PRINT_HELP("JHP1B_HLP_SPCL1", DEFAULT_HELP_TEXT_TIME)
					ENDIF
					iSpecAbilHlp++
				ENDIF
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				PRINT_HELP("JHP1B_HLP_SPCL2", DEFAULT_HELP_TEXT_TIME)
				iSpecAbilHlp++
			ENDIF
		BREAK
	ENDSWITCH

	switch mission_substage
		case STAGE_ENTRY				
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			if IsEntityAlive(vehs[mvf_FBI2].id)
			and not DOES_BLIP_EXIST(vehs[mvf_FBI2].blip)
				IF GET_BLIP_FROM_ENTITY(vehs[mvf_FBI2].id) != null
					vehs[mvf_FBI2].blip = GET_BLIP_FROM_ENTITY(vehs[mvf_FBI2].id)
				ELSE
					vehs[mvf_FBI2].blip = CREATE_BLIP_FOR_VEHICLE(vehs[mvf_FBI2].id)  
				ENDIF
				
			endif			
			Print_now("JHP1B_STEAL_VAN",DEFAULT_GOD_TEXT_TIME,1)
			TRIGGER_MUSIC_EVENT("JHP1B_START")
			bcuedstop = true
			swatstage = SWATSTAGE_ONROUTE			
			mission_substage++
		break
		case 1			
			if IS_PED_IN_VEHICLE(PLAYER_PED_ID(),vehs[mvf_FBI2].id)
				if DOES_BLIP_EXIST(vehs[mvf_FBI2].blip)
					SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(TRUE)
						REMOVE_BLIP(vehs[mvf_FBI2].blip)
					SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(FALSE)
				endif	
				Mission_Set_Stage(msf_1_get_away)
				mission_substage = STAGE_ENTRY
			endif			
		break
	endswitch
	
endproc
proc ST_1_GET_AWAY()
	switch mission_substage
		case STAGE_ENTRY		
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			ENDIF
			TRIGGER_MUSIC_EVENT("JHP1B_VAN")
			SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<0,0,0>>,-1)
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(JHP1B_SWIFT_GETAWAY)
			bcuedstop = FALSE
			mission_substage++			
		break
		case 1				
			if not bcuedstop
				if GET_PLAYER_WANTED_LEVEL(player_id()) =0
					TRIGGER_MUSIC_EVENT("JHP1B_STOP")
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE, JHP1B_SWIFT_GETAWAY)
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					
					bcuedstop = true
				endif
			endif
			
			if IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData,vFACTORY,<<0.1,0.1,LOCATE_SIZE_HEIGHT>>,true,vehs[mvf_FBI2].id,"JHP1B_RTNVAN","","JHP1B_BACK_VAN",true)
			or (DOES_BLIP_EXIST(sLocatesData.LocationBlip)And IS_ENTITY_IN_ANGLED_AREA(vehs[mvf_FBI2].id,<<692.914307,-1003.555786,21.508389>>, <<692.651123,-1021.604126,26.206753>>, 9.750000))
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_FBI2].id,false)
				CLEAR_MISSION_LOCATE_STUFF(sLocatesData,true)
				INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED(TRUE, JHP1B_SWIFT_GETAWAY)
				
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				
				mission_substage++
			endif
		break		
		case 2
			if BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehs[mvf_FBI2].id,DEFAULT_VEH_STOPPING_DISTANCE,-1)

				if IS_VEHICLE_SIREN_ON(vehs[mvf_FBI2].id)
					SET_VEHICLE_SIREN(vehs[mvf_FBI2].id, FALSE)
				endif
				SET_VEHICLE_ENGINE_ON(vehs[mvf_FBI2].id, FALSE, FALSE)
				
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
				BLOCK_PLAYER_FOR_LEAD_IN(TRUE)
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				
				IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehs[mvf_FBI2].id)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehs[mvf_FBI2].id, FALSE)
				ENDIF
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				
				ADD_PED_FOR_DIALOGUE(convo_struct,0,player_ped_id(),"MICHAEL")
				ADD_PED_FOR_DIALOGUE(convo_struct,3,null,"LESTER")
				
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				
				mission_substage++
			endif
			
		break
		case 3
			if PLAYER_CALL_CHAR_CELLPHONE(convo_struct,CHAR_LESTER,"JHFAUD","JHF_CARB",CONV_PRIORITY_MEDIUM)
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				mission_substage++
			endif
		break
		case 4
			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				mission_passed()
			endif
		break
	endswitch
endproc
PROC ST_2_PASSED()
	switch mission_substage
		case STAGE_ENTRY
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME)
			endif
			SET_GAMEPLAY_CAM_RELATIVE_HEADING()
			mission_substage++
		break
		case 1
			SET_PLAYER_CONTROL(player_id(),false)
			SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
			ADD_PED_FOR_DIALOGUE(convo_struct,0,player_ped_id(),"MICHAEL")
			ADD_PED_FOR_DIALOGUE(convo_struct,3,null,"LESTER")
			if PLAYER_CALL_CHAR_CELLPHONE(convo_struct,CHAR_LESTER,"JHFAUD","JHF_CARB",CONV_PRIORITY_MEDIUM)
				mission_substage++
			endif
		break
		case 2
			if not IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				mission_passed()
			endif
		break
	endswitch
ENDPROC
// -----------------------------------------------------------------------------------------------------------
//		MISSION FLOW
// -----------------------------------------------------------------------------------------------------------
PROC mission_flow()
	Switch	int_to_enum(MSF_MISSION_STAGE_FLAGS, mission_stage)
		case 	msf_0_Wander				ST_0_WANDER() 		break
		case 	msf_1_get_away				ST_1_GET_AWAY()		break
		case 	msf_2_passed				ST_2_PASSED()		break
	endswitch
	
ENDPROC
#IF IS_DEBUG_BUILD			
	PROC DO_DEBUG()
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			MISSION_PASSED()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			MISSION_FAILED(mff_debug_fail)		
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME)
			bDisplayDebug = !bDisplayDebug
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(bDisplayDebug)
		ENDIF
		
		IF bDisplayDebug
			CONST_FLOAT f_Spacing			0.025
			FLOAT fX, fY
			TEXT_LABEL_63 strDebug
			strDebug = "Mission Stage: "
			strDebug += mission_stage
			DRAW_DEBUG_TEXT_2D(strDebug, <<fX, fY + (f_Spacing * 0), 0.0>>, 0,0,0,255)
			
			strDebug = "Mission Substage: "
			strDebug += mission_substage
			DRAW_DEBUG_TEXT_2D(strDebug, <<fX, fY + (f_Spacing * 1), 0.0>>, 0,0,0,255)
		
		ENDIF
	ENDPROC
#endif
// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	PRINTSTRING("...Jewel Prep 1B Mission Launched")
	PRINTNL()
	
	IF (HAS_FORCE_CLEANUP_OCCURRED())
		PRINTSTRING("...Jewel Prep 1B Mission Force Cleanup")
		PRINTNL()
		TRIGGER_MUSIC_EVENT("JHP1B_FAIL")
		Mission_Flow_Mission_Force_Cleanup()		
		Mission_Cleanup()
		TERMINATE_THIS_THREAD()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)

#if IS_DEBUG_BUILD
	//z menu for skipping stages
	zMenuNames[msf_0_Wander].sTxtLabel 			=	"Stage 0: ONROUTE"
	zMenuNames[msf_1_get_away].sTxtLabel 		=	"Stage 1: GET AWAY"
	zMenuNames[msf_1_get_away].bSelectable		= FALSE
	zMenuNames[msf_2_passed].sTxtLabel 			=	"---------- PASSED ---------"
	
	widget_debug = START_WIDGET_GROUP("Jewel Prep 1B Menu")
	ADD_WIDGET_BOOL("SWATSTAGE_ARREST",bSWATSTAGE_ARREST)
	STOP_WIDGET_GROUP()
	SET_LOCATES_HEADER_WIDGET_GROUP(widget_debug)
#endif

	//initialize mission
	MISSION_SETUP()
	
	WHILE (TRUE)

		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_JewelStoreJobPrep1B")

				//prestreaming loop
		Update_Asset_Management_System(sAssetData)		// Deals with loading any assets and keeps track of what has been loaded
		
		Mission_stage_management()
		MISSION_STAGE_SKIP()		
		if not bDoSkip
			MISSION_CHECKS()
			mission_flow()
		endif 		
#IF IS_DEBUG_BUILD
	DO_DEBUG()
#ENDIF	
	
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.8)
		WAIT(0)
		
	ENDWHILE
ENDSCRIPT
