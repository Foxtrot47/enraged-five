
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"


USING "script_ped.sch"

USING "flow_public_core_override.sch"
USING "select_mission_stage.sch"
USING "replay_public.sch"
USING "CompletionPercentage_public.sch"
USING "locates_public.sch"
USING "chase_hint_cam.sch"

USING "RC_threat_public.sch"

USING "asset_management_public.sch"
USING "mission_event_manager.sch"
USING "cheat_controller_public.sch"
USING "commands_recording.sch"
 
// ENUMS
//-----------------------------------------------------------
 
ENUM MISSION_FAIL_FLAGS
	mff_debug_forced,
	mff_van_lost,
	mff_van_left,
	mff_cargo_left,
	mff_van_in_water,
	mff_default
ENDENUM

ENUM MISSION_STAGE_FLAGS
	msf_gas_steal,
	msf_mission_passed,
	msf_num_mission_stages
ENDENUM

ENUM MISSION_EVENT_FLAGS
	mef_driver_manager,
	mef_cargo_manager,
	mef_num_events
ENDENUM

ENUM DRIVER_AI_ENUM
	DRIVERAI_INVALID = 0,
	DRIVERAI_ONROUTE,				// is on its normal route
	DRIVERAI_ONROUTE_AIRPORT,		// finished waypoint rec and is now driving into and around the airport
	DRIVERAI_STOPPED_AIRPORT,		// stopped inside the airport.
	DRIVERAI_FLEEING,				// is fleeing in the van
	DRIVERAI_COMBATING				// is attacking the player
ENDENUM

ENUM ENUM_VAN_STATE
	VANSTATE_DRIVABLE_WITH_CARGO,
	VANSTATE_NOT_DRIVABLE_WITH_CARGO,
	VANSTATE_CARGO_LOOSE_INSIDE,
	VANSTATE_CARGO_FALLEN_OUT
ENDENUM

// CONSTANTS
//---------------------------------------------------------------------

TEXT_LABEL_15						str_Dialogue								= "JHP2ADS"
		
// Damage indicators for the van
CONST_FLOAT		VAN_STOP_HEALTH_DAMAGE_LIMIT		0.0
CONST_FLOAT		VAN_STOP_ENGINE_DAMAGE_LIMIT		250.0		// (engine smokes @ 400.0, leaks oil @ 200.0)
CONST_FLOAT		VAN_STOP_PETROL_DAMAGE_LIMIT		400.0		// (petrol tank leaks @ 500.0)

//CONST_FLOAT		VAN_WEAK_PETROL						550.0		// just before leaks petrol

CONST_FLOAT 	MIN_CLOSING_SPEED					4.0			// min speed for a ram to register

MODEL_NAMES		mod_Driver							= S_M_M_Armoured_01
MODEL_NAMES		mod_Van								= BOXVILLE3
MODEL_NAMES		mod_Case							= PROP_IDOL_CASE_02
MODEL_NAMES		mod_DummyLock						= PROP_YELL_PLASTIC_TARGET

VECTOR 			v_VanCargo1 						= <<0,0.175,0.0>>
VECTOR 			v_VanCargo2 						= <<0,-3.6,0.0>>

// STRUCTS
//--------------------------------------------------------------------

STRUCT PED_STRUCT
	PED_INDEX 			id
	INT					iGeneric			// generic int flag that can be sued for whatever
	AI_BLIP_STRUCT		aiBlip
ENDSTRUCT

STRUCT VEHICLE_STRUCT
	VEHICLE_INDEX		id
	BLIP_INDEX			blip
ENDSTRUCT

STRUCT OBJECT_STRUCT
	OBJECT_INDEX		id
	BLIP_INDEX			blip
ENDSTRUCT

// VARIABLES
//----------------------------------------------------------------------

// Integral script structure variables
INT									i_missionStage								= enum_to_int(msf_gas_steal)
INT 								i_missionSubstage							= 0
BOOL 								b_StageSetup								= FALSE
BOOL								b_DoSkip									= FALSE		
INT									i_SkipToStage								= 0
BOOL 								b_MissionFailed								= FALSE
ASSET_MANAGEMENT_DATA				s_AssetData
MISSION_EVENT_LINK_STRUCT			s_Events[mef_num_events]
LOCATES_HEADER_DATA					s_LocatesData
structPedsForConversation			s_Convo
CHASE_HINT_CAM_STRUCT				localChaseHintCamStruct
ENUM_VAN_STATE 						eVanState
SEQUENCE_INDEX 						seq
REL_GROUP_HASH						rel_Security

// Entities
PED_STRUCT 							s_PedDriver
VEHICLE_STRUCT						s_VehPlayer
VEHICLE_STRUCT						s_VehVan
VEHICLE_STRUCT 						s_VehTowTruck
OBJECT_STRUCT						obj_Grenades[3]
OBJECT_INDEX						obj_Lock

SCENARIO_BLOCKING_INDEX				sbi_SweatshopBin

// Gas stealing vars
INT 								i_VanRammedCount							= 0
BOOL 								b_VanRammedLastFrame						= FALSE
FLOAT 								f_PlayerVsVanClosingSpeedLastFrame			= 0
INT 								i_RamTimer 									= 0			// used in order to do a safe delay before checking for another ram
INT 								i_NumBulletHitsOnDoorLock					= 0
INT 								i_BulletHitTimer							= 0
INT 								i_VanDriverDialogue
INT 								i_VanDriverDiaTimer
TEXT_LABEL_23 						str_VanDriverDiaRoot

BOOL 								bDisplayedStealBZCaseObjective
BOOL								b_VehVanNoLongerNeeded
BOOL								bDoorSwingingFreeL, bDoorSwingingFreeR
BOOL								b_PlayerPoliceScannerLine
INT									i_HornTimer									= -1

// Debug
#IF IS_DEBUG_BUILD

WIDGET_GROUP_ID						debug_widget
MissionStageMenuTextStruct			s_ZMenuStruct[msf_num_mission_stages]
FLOAT 								debug_fClosingSpeed
TEXT_WIDGET_ID						twi_vanState
BOOL								b_DisplayDebug

#ENDIF

//PURPOSE: Performs a entity specific death check on an entity to determine if it is alive
FUNC BOOL IS_THIS_ENTITY_ALIVE(ENTITY_INDEX entity)
	IF DOES_ENTITY_EXIST(entity)
		SWITCH GET_ENTITY_TYPE(entity)
			CASE ET_PED
				IF NOT IS_PED_INJURED(GET_PED_INDEX_FROM_ENTITY_INDEX(entity))
					RETURN TRUE
				ENDIF
			BREAK
			CASE ET_VEHICLE
				IF IS_VEHICLE_DRIVEABLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(entity))
					RETURN TRUE
				ENDIF
			BREAK
			DEFAULT
				IF NOT IS_ENTITY_DEAD(entity)
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Prints debug output at the specified level within the mision 
DEBUGONLY PROC PRINT_DEBUG(STRING strMsg, INT debugLevel = 0)
	IF NOT IS_STRING_NULL_OR_EMPTY(strMsg) AND debugLevel >= 0 AND debugLevel <= 3
		#IF IS_DEBUG_BUILD
		SWITCH debugLevel
			CASE 0	FALLTHRU
			DEFAULT CPRINTLN(DEBUG_MISSION, "[***", GET_THIS_SCRIPT_NAME(), "***] ", strMsg)	BREAK
			CASE 1 CDEBUG1LN(DEBUG_MISSION, "[***", GET_THIS_SCRIPT_NAME(), "***] ", strMsg)	BREAK
			CASE 2 CDEBUG2LN(DEBUG_MISSION, "[***", GET_THIS_SCRIPT_NAME(), "***] ", strMsg)	BREAK
			CASE 3 CDEBUG3LN(DEBUG_MISSION, "[***", GET_THIS_SCRIPT_NAME(), "***] ", strMsg)	BREAK
		ENDSWITCH
		#ENDIF
	ENDIF
ENDPROC

//PURPOSE: Creates the players vehicle using the stored data in the replay structs. If no data is found it will create the default vehicle for michael.
// Should be called until returns true. (May not create a vehicle if bIsVehicleNeeded = FALSE)
FUNC BOOL Create_Player_Mission_Vehicle(VEHICLE_INDEX &vehicle, VECTOR vCoord, FLOAT fHeading, BOOL bIsVehicleNeeded)
	
	VEHICLE_SETUP_STRUCT s_vehicle_struct
	
	// Populate the struct
	IF IS_REPLAY_IN_PROGRESS() 
		IF IS_REPLAY_CHECKPOINT_VEHICLE_AVAILABLE()
			CLONE_VEHICLE_SETUP_STRUCT(g_stageSnapshot.mVehicleStruct.mVehicle, s_vehicle_struct)
			CPRINTLN(DEBUG_MISSION, "USING REPLAY SNAPSHOT START")
		ENDIF
	ELIF IS_REPLAY_START_VEHICLE_AVAILABLE()
		CLONE_VEHICLE_SETUP_STRUCT(g_startSnapshot.mVehicleStruct.mVehicle, s_vehicle_struct)
		CPRINTLN(DEBUG_MISSION, "USING REPLAY SNAPSHOT END")
	ENDIF
	
	// Vehicle model found....
	IF s_vehicle_struct.eModel != DUMMY_MODEL_FOR_SCRIPT
		// ... and does not match the players standard car, so player was driving something else (that was prob jacked)
		IF NOT DOES_VEH_SETUP_STRUCT_MATCH_PLAYER_VEHICLE(CHAR_MICHAEL, s_vehicle_struct)
			IF IS_THIS_MODEL_A_CAR(s_vehicle_struct.eModel)
			OR IS_THIS_MODEL_A_BIKE(s_vehicle_struct.eModel)
				REQUEST_MODEL(s_vehicle_struct.eModel)
				IF HAS_MODEL_LOADED(s_vehicle_struct.eModel)
					vehicle = CREATE_VEHICLE(s_vehicle_struct.eModel, vCoord, fHeading)
					SET_VEHICLE_SETUP(vehicle, s_vehicle_struct)
					SET_MODEL_AS_NO_LONGER_NEEDED(s_vehicle_struct.eModel)
					SET_VEHICLE_ON_GROUND_PROPERLY(vehicle)
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
		ELSE
			bIsVehicleNeeded = TRUE
		ENDIF
	ENDIF
	
	// No data found, if a vehicle is needed then create the player standard
	IF bIsVehicleNeeded
		IF NOT DOES_ENTITY_EXIST(vehicle)	
			IF CREATE_PLAYER_VEHICLE(vehicle, CHAR_MICHAEL, vCoord, fHeading)
				SET_VEHICLE_MODEL_IS_SUPPRESSED(GET_PLAYER_VEH_MODEL(CHAR_MICHAEL), TRUE)
				RETURN TRUE	
			ENDIF
		ENDIF
		
		RETURN FALSE
	ELSE
		// RETURNS true to indicate that this procedure has finished, regardless of whether a vehicle was created
		RETURN TRUE
	ENDIF
ENDFUNC

// MISSION SPECIFIC PROCS/FUNCS
//-------------------------------------------------------------------------------------------------

PROC CREATE_LOCK_AND_CASES()

	CONST_FLOAT F_CASE_MASS 3.5

	IF DOES_ENTITY_EXIST(s_VehVan.id)

		//create shootable lock
		IF NOT IS_ENTITY_DEAD(s_VehVan.id)
		AND NOT DOES_ENTITY_EXIST(obj_Lock)
			obj_Lock = CREATE_OBJECT(mod_DummyLock, GET_ENTITY_COORDS(s_VehVan.id))
			SET_OBJECT_PHYSICS_PARAMS(obj_Lock, 0.1, -1, <<-1.0, -1.0, -1.0>>, <<-1.0, -1.0, -1.0>>, -1, -1, -1)
			ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(obj_Lock, s_VehVan.id, -1, -1, 
				GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(s_VehVan.id, GET_WORLD_POSITION_OF_ENTITY_BONE(s_VehVan.id, 5))+<<0.02,0,0>> ,<<0,0,0>>, <<0,0,0>>, -1, TRUE)
			SET_ENTITY_VISIBLE(obj_Lock, FALSE)
		ENDIF

		VECTOR vSpawnCaseCoord
		//create grenade cases 
		IF NOT DOES_ENTITY_EXIST(obj_Grenades[0].id)
			IF IS_ENTITY_DEAD(s_VehVan.id)
				vSpawnCaseCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_VehVan.id, <<GET_RANDOM_FLOAT_IN_RANGE(-2.5, 2.5),-5.0,0>>)
				GET_GROUND_Z_FOR_3D_COORD(vSpawnCaseCoord, vSpawnCaseCoord.z)
				obj_Grenades[0].id = CREATE_OBJECT(mod_Case, vSpawnCaseCoord)
			ELSE
				obj_Grenades[0].id = CREATE_OBJECT(mod_Case, GET_ENTITY_COORDS(s_VehVan.id))
				SET_OBJECT_PHYSICS_PARAMS(obj_Grenades[0].id, F_CASE_MASS, -1, <<-1.0, -1.0, -1.0>>, <<-1.0, -1.0, -1.0>>, -1, -1, -1)
				//ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(obj_Grenades[0].id, s_VehVan.id, -1, -1, <<0.35, -2.775, -0.14>>, <<0,0,0>>, <<-27.3,0,-90>>, -1, TRUE, TRUE, TRUE, FALSE)
				ATTACH_ENTITY_TO_ENTITY(obj_Grenades[0].id, s_VehVan.id, -1, <<-0.2,-2.675,0.05>>, <<-90.0,0,-90>>, FALSE, FALSE, FALSE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(obj_Grenades[0].id, TRUE)
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(obj_Grenades[1].id)
			IF IS_ENTITY_DEAD(s_VehVan.id)
				vSpawnCaseCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_VehVan.id, <<GET_RANDOM_FLOAT_IN_RANGE(-2.5, 2.5),-5.0,0>>)
				GET_GROUND_Z_FOR_3D_COORD(vSpawnCaseCoord, vSpawnCaseCoord.z)
				obj_Grenades[1].id = CREATE_OBJECT(mod_Case, vSpawnCaseCoord)
			ELSE
				obj_Grenades[1].id = CREATE_OBJECT(mod_Case, GET_ENTITY_COORDS(s_VehVan.id))
				SET_OBJECT_PHYSICS_PARAMS(obj_Grenades[1].id, F_CASE_MASS, -1, <<-1.0, -1.0, -1.0>>, <<-1.0, -1.0, -1.0>>, -1, -1, -1)
				//ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(obj_Grenades[1].id, s_VehVan.id, -1, -1, <<0.17, -2.675, -0.14>>, <<0,0,0>>, <<-32.5, 0.0, -90.0>>, -1, TRUE, TRUE, TRUE, FALSE)
				ATTACH_ENTITY_TO_ENTITY(obj_Grenades[1].id, s_VehVan.id, -1, <<0,-2.675,0.05>>, <<-90.0,0,-90>>, FALSE, FALSE, FALSE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(obj_Grenades[1].id, TRUE)
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(obj_Grenades[2].id)
			IF IS_ENTITY_DEAD(s_VehVan.id)
				vSpawnCaseCoord = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_VehVan.id, <<GET_RANDOM_FLOAT_IN_RANGE(-2.5, 2.5),-5.0,0>>)
				GET_GROUND_Z_FOR_3D_COORD(vSpawnCaseCoord, vSpawnCaseCoord.z)
				obj_Grenades[2].id = CREATE_OBJECT(mod_Case, vSpawnCaseCoord)
			ELSE
				obj_Grenades[2].id = CREATE_OBJECT(mod_Case, GET_ENTITY_COORDS(s_VehVan.id))
				SET_OBJECT_PHYSICS_PARAMS(obj_Grenades[2].id, F_CASE_MASS, -1, <<-1.0, -1.0, -1.0>>, <<-1.0, -1.0, -1.0>>, -1, -1, -1)
				//ATTACH_ENTITY_TO_ENTITY_PHYSICALLY(obj_Grenades[2].id, s_VehVan.id, -1, -1, <<-0.175, -3.075, -0.230>>, <<0,0,0>>, <<90.0, 22.5, 0.0>>, -1, TRUE, TRUE, TRUE, FALSE)
				ATTACH_ENTITY_TO_ENTITY(obj_Grenades[2].id, s_VehVan.id, -1, <<0.2,-2.675,0.05>>, <<-90.0,0,-90>>, FALSE, FALSE, FALSE)
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(obj_Grenades[2].id, TRUE)
			ENDIF
		ENDIF

	ENDIF
	
ENDPROC

//PURPOSE: Gets the entites from the trigger scene OR if they for some reason create them
FUNC BOOL PREPARE_INITIAL_SCENE(BOOL bGrabFromTriggerScene)

	BOOL bAssetMissingEntities = FALSE

	// this must be a retry warp
	Load_Asset_Waypoint(s_AssetData, g_JHP2A_string_WayPoint)
	Load_Asset_Model(s_AssetData, S_M_M_ARMOURED_01)
	Load_Asset_Model(s_AssetData, mod_Case)
	Load_Asset_Model(s_AssetData, mod_DummyLock)
	
	IF HAS_MODEL_LOADED(S_M_M_ARMOURED_01)
	AND HAS_MODEL_LOADED(mod_Case)
	AND HAS_MODEL_LOADED(mod_DummyLock)
	
		IF bGrabFromTriggerScene
		
			// Grab the van
			IF NOT DOES_ENTITY_EXIST(s_VehVan.id)
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.veh[0])
					SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.veh[0], TRUE, TRUE)
					s_VehVan.id = g_sTriggerSceneAssets.veh[0]
				ELSE
					PRINT_DEBUG("g_sTriggerSceneAssets.veh[0] not found", 0)
					bAssetMissingEntities = FALSE
				ENDIF
			ENDIF
			
			// Grab the peds
			IF NOT DOES_ENTITY_EXIST(s_PedDriver.id)
				IF DOES_ENTITY_EXIST(g_sTriggerSceneAssets.ped[0])
					SET_ENTITY_AS_MISSION_ENTITY(g_sTriggerSceneAssets.ped[0], TRUE, TRUE)
					s_PedDriver.id = g_sTriggerSceneAssets.ped[0]
				ELSE
					PRINT_DEBUG("g_sTriggerSceneAssets.ped[0] not found", 0)
					bAssetMissingEntities = FALSE
				ENDIF
			ENDIF

		ELSE
		
			// Create van
			VECTOR vVanSpawnCoord
			
			Load_Asset_Model(s_AssetData, mod_Van)
			IF HAS_MODEL_LOADED(mod_Van)
			AND GET_IS_WAYPOINT_RECORDING_LOADED(g_JHP2A_string_WayPoint)

				IF ARE_STRINGS_EQUAL(g_JHP2A_string_WayPoint, "jhp2a_main")
					WAYPOINT_RECORDING_GET_COORD(g_JHP2A_string_WayPoint, 23, vVanSpawnCoord)
					CLEAR_AREA_OF_VEHICLES(vVanSpawnCoord, 20.0)
					s_VehVan.id = CREATE_VEHICLE(mod_Van, vVanSpawnCoord, 119.4988)
					s_PedDriver.id = CREATE_PED_INSIDE_VEHICLE(s_VehVan.id, PEDTYPE_MISSION, mod_Driver, VS_DRIVER)
					SET_VEHICLE_ON_GROUND_PROPERLY(s_VehVan.id)
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(s_PedDriver.id,s_VehVan.id, g_JHP2A_string_WayPoint, DRIVINGMODE_STOPFORCARS_IGNORELIGHTS, 23, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, 12.0)
				ELSE
					WAYPOINT_RECORDING_GET_COORD(g_JHP2A_string_WayPoint, 29, vVanSpawnCoord)
					CLEAR_AREA_OF_VEHICLES(vVanSpawnCoord, 20.0)
					s_VehVan.id = CREATE_VEHICLE(mod_Van, vVanSpawnCoord, 134.0011)
					s_PedDriver.id = CREATE_PED_INSIDE_VEHICLE(s_VehVan.id, PEDTYPE_MISSION, mod_Driver, VS_DRIVER)
					SET_VEHICLE_ON_GROUND_PROPERLY(s_VehVan.id)
					TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(s_PedDriver.id,s_VehVan.id, g_JHP2A_string_WayPoint, DRIVINGMODE_STOPFORCARS_IGNORELIGHTS, 29, EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, 12.0)
				ENDIF
				
			ELSE
				PRINT_DEBUG("PREPARE_INITIAL_SCENE(): Van model not loaded yet.", 2)
			ENDIF
		
		ENDIF
		
// Common enetity set up
//-----------------------------------------------------
		
		IF DOES_ENTITY_EXIST(s_VehVan.id)
			SET_VEHICLE_AS_RESTRICTED(s_VehVan.id, 0)
			IF IS_VEHICLE_DRIVEABLE(s_VehVan.id)
				SET_VEHICLE_HAS_STRONG_AXLES(s_VehVan.id, TRUE)
				SET_ENTITY_LOAD_COLLISION_FLAG(s_VehVan.id, TRUE)
				SET_VEHICLE_CAN_LEAK_PETROL(s_VehVan.id, TRUE)
				//SET_VEHICLE_PETROL_TANK_HEALTH(s_VehVan.id, VAN_WEAK_PETROL)
				SET_VEHICLE_DISABLE_TOWING(s_VehVan.id, TRUE)
			ENDIF
		ENDIF

		IF DOES_ENTITY_EXIST(s_PedDriver.id)
			IF NOT IS_PED_INJURED(s_PedDriver.id)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_PedDriver.id, TRUE)
				//SET_PED_RELATIONSHIP_GROUP_HASH(s_PedDriver.id, RELGROUPHASH_SECURITY_GUARD)
				SET_PED_RELATIONSHIP_GROUP_HASH(s_PedDriver.id, rel_Security)
				SET_PED_ACCURACY(s_PedDriver.id, 5)
				SET_PED_CAN_BE_TARGETTED(s_PedDriver.id, FALSE)
				SET_PED_COMBAT_ATTRIBUTES(s_PedDriver.id, CA_USE_VEHICLE, FALSE)
				GIVE_WEAPON_TO_PED(s_PedDriver.id, WEAPONTYPE_PISTOL, 15)
				ADD_PED_FOR_DIALOGUE(s_Convo, 3, s_PedDriver.id, "JHP2A_Driver")
				TRIGGER_MISSION_EVENT(s_Events[mef_driver_manager].sData)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(s_VehVan.id)
		AND DOES_ENTITY_EXIST(s_PedDriver.id)
		
			CREATE_LOCK_AND_CASES()
			
			TRIGGER_MISSION_EVENT(s_Events[mef_cargo_manager].sData)
			
			//MISSION_FLOW_RELEASE_TRIGGER_SCENE_ASSETS(SP_HEIST_JEWELRY_PREP_2A) // NOT CALLING TO KEEP THE TRIGGERSCENE RUNNING OTHERWISE THE WAYPOINT RECORDING CLEANS UP
			Unload_Asset_Model(s_AssetData, BOXVILLE3)
			Unload_Asset_Model(s_AssetData, S_M_M_Armoured_01)
			Unload_Asset_Model(s_AssetData, mod_Case)
			Unload_Asset_Model(s_AssetData, mod_DummyLock)
			
			RETURN TRUE
		ELSE
			IF bAssetMissingEntities
				SCRIPT_ASSERT("Unable to grab all trigger scene entities")
			ENDIF
		ENDIF	
	ELSE
		PRINT_DEBUG("PREPARE_INITIAL_SCENE(): Assets not loaded yet.", 0)
	ENDIF
	
	RETURN FALSE
ENDFUNC

// Detaches the case from the van
PROC DETACH_CASE_FROM_VAN(OBJECT_INDEX object, BOOL bThrowFromVan)

	IF DOES_ENTITY_EXIST(object)

		IF IS_ENTITY_ATTACHED(object)
			DETACH_ENTITY(object, TRUE, bThrowFromVan)
			SET_ENTITY_COLLISION(object, TRUE)
			ACTIVATE_PHYSICS(object)
		ENDIF

		IF bThrowFromVan
			VECTOR vDir
			vDir = NORMALISE_VECTOR( GET_ENTITY_COORDS(object) - GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_VehVan.id, <<GET_RANDOM_FLOAT_IN_RANGE(-0.15, 0.15), -GET_RANDOM_INT_IN_RANGE(3, 6), GET_RANDOM_FLOAT_IN_RANGE(-0.1, 0.1)>>) )
			APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(object, APPLY_TYPE_IMPULSE, vDir * GET_RANDOM_FLOAT_IN_RANGE(0.25, 0.5), 0, TRUE, TRUE, FALSE)
			APPLY_FORCE_TO_ENTITY_CENTER_OF_MASS(object, APPLY_TYPE_ANGULAR_IMPULSE, <<GET_RANDOM_INT_IN_RANGE(0, 10), GET_RANDOM_INT_IN_RANGE(0, 10), GET_RANDOM_INT_IN_RANGE(0, 10)>>, 0, TRUE, TRUE, FALSE)
			SET_ENTITY_NO_COLLISION_ENTITY(object, s_VehVan.id, TRUE)
		ENDIF
	
	ENDIF
	
ENDPROC

FUNC BOOL IS_CASE_INSIDE_VAN(OBJECT_INDEX objCase)

	IF IS_ENTITY_DEAD(s_VehVan.id)
		RETURN FALSE
	ENDIF
	
	VECTOR vResult = GET_CLOSEST_POINT_ON_LINE(GET_ENTITY_COORDS(objCase), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_VehVan.id, v_VanCargo1), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_VehVan.id, v_VanCargo2), FALSE)
	
	#IF IS_DEBUG_BUILD
		IF g_bTriggerDebugOn
			DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_VehVan.id, v_VanCargo1), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_VehVan.id, v_VanCargo2), 255, 0,0, 255)
			DRAW_DEBUG_SPHERE(vResult, 0.025, 0, 255, 0, 100)
			DRAW_DEBUG_LINE(vResult, GET_ENTITY_COORDS(objCase), 0,0,255, 255)
		ENDIF
	#ENDIF
	
	IF VDIST2(vResult, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_VehVan.id, v_VanCargo1)) <= VDIST2(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_VehVan.id, v_VanCargo2), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_VehVan.id, v_VanCargo1))
		RETURN TRUE
	ELSE
		RETURN FALSE
	ENDIF
	
ENDFUNC

PROC UPDATE_VAN_STATE()

	IF eVanState = VANSTATE_DRIVABLE_WITH_CARGO
	OR eVanState = VANSTATE_NOT_DRIVABLE_WITH_CARGO
	
		IF IS_ENTITY_DEAD(s_VehVan.id)
		OR (DOES_ENTITY_EXIST(obj_Grenades[0].id) AND NOT IS_ENTITY_ATTACHED(obj_Grenades[0].id))
		OR (DOES_ENTITY_EXIST(obj_Grenades[1].id) AND NOT IS_ENTITY_ATTACHED(obj_Grenades[1].id))
		OR (DOES_ENTITY_EXIST(obj_Grenades[2].id) AND NOT IS_ENTITY_ATTACHED(obj_Grenades[2].id))
			eVanState = VANSTATE_CARGO_LOOSE_INSIDE
		ENDIF

	ENDIF
	
	IF eVanState = VANSTATE_CARGO_LOOSE_INSIDE
		IF (NOT DOES_ENTITY_EXIST(s_VehVan.id) OR NOT IS_VEHICLE_DRIVEABLE(s_VehVan.id))
		OR (DOES_ENTITY_EXIST(obj_Grenades[0].id) AND NOT IS_CASE_INSIDE_VAN(obj_Grenades[0].id))
		OR (DOES_ENTITY_EXIST(obj_Grenades[1].id) AND NOT IS_CASE_INSIDE_VAN(obj_Grenades[1].id))
		OR (DOES_ENTITY_EXIST(obj_Grenades[2].id) AND NOT IS_CASE_INSIDE_VAN(obj_Grenades[2].id))
			eVanState = VANSTATE_CARGO_FALLEN_OUT
		ENDIF
	ENDIF

	IF eVanState = VANSTATE_DRIVABLE_WITH_CARGO
	
		// check if the player is in a tow truck
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			VEHICLE_INDEX vehTemp
			vehTemp = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
			IF (GET_ENTITY_MODEL(vehTemp) = TOWTRUCK
			OR GET_ENTITY_MODEL(vehTemp) = TOWTRUCK2)
				IF IS_VEHICLE_DRIVEABLE(vehTemp)
					IF IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(vehTemp, s_VehVan.id)
						s_VehTowTruck.id = vehTemp
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ENDIF

	IF eVanState = VANSTATE_DRIVABLE_WITH_CARGO
	
		IF NOT IS_VEHICLE_DRIVEABLE(s_VehVan.id)
//		OR GET_VEHICLE_ENGINE_HEALTH(s_VehVan.id) <= 0.0
//		OR GET_VEHICLE_PETROL_TANK_HEALTH(s_VehVan.id) <= 0.0
			eVanState = VANSTATE_NOT_DRIVABLE_WITH_CARGO
		ENDIF
		
	ENDIF
	
	IF eVanState = VANSTATE_NOT_DRIVABLE_WITH_CARGO
	AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), s_VehVan.id, TRUE)
		DISABLE_CHEAT(CHEAT_TYPE_GIVE_HEALTH_ARMOR, TRUE)
	ELSE
		DISABLE_CHEAT(CHEAT_TYPE_GIVE_HEALTH_ARMOR, FALSE)
	ENDIF


	#IF IS_DEBUG_BUILD
	SWITCH eVanState
		CASE VANSTATE_DRIVABLE_WITH_CARGO			SET_CONTENTS_OF_TEXT_WIDGET(twi_vanState, "DRIVABLE WITH CARGO")		BREAK
		CASE VANSTATE_NOT_DRIVABLE_WITH_CARGO		SET_CONTENTS_OF_TEXT_WIDGET(twi_vanState, "NOT DRIVABLE WITH CARGO")	BREAK
		CASE VANSTATE_CARGO_LOOSE_INSIDE			SET_CONTENTS_OF_TEXT_WIDGET(twi_vanState, "CARGO LOOSE")				BREAK
		CASE VANSTATE_CARGO_FALLEN_OUT				SET_CONTENTS_OF_TEXT_WIDGET(twi_vanState, "CARGO FALLEN OUT")			BREAK
	ENDSWITCH
	#ENDIF

ENDPROC

// MISSION TEMPLATE STRUCTURE
//--------------------------------------------------------------------------------------------------

//PURPOSE: Cleans up the mission
PROC Mission_Cleanup(BOOL bImmediately)

	SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), FALSE)
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BOXVILLE3, FALSE)
	SET_PED_MODEL_IS_SUPPRESSED(S_M_M_Armoured_01, FALSE)
	
	IF IS_CELLPHONE_DISABLED()
		DISABLE_CELLPHONE(FALSE)
	ENDIF
	
	i_NumBulletHitsOnDoorLock 		= 0
	g_bLaptopMissionSuppressed 		= FALSE
	
	IF bImmediately
		SET_WANTED_LEVEL_MULTIPLIER(1.0)
		CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
		
		g_bLaptopMissionSuppressed = FALSE
	ENDIF
	
	IF DOES_ENTITY_EXIST(s_VehPlayer.id)
		IF bImmediately
			DELETE_VEHICLE(s_VehPlayer.id)
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(s_VehPlayer.id)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(s_PedDriver.id)
		IF bImmediately
			DELETE_PED(s_PedDriver.id)
		ELSE
			SET_PED_AS_NO_LONGER_NEEDED(s_PedDriver.id)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(s_VehVan.id)
		IF bImmediately
		AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			DELETE_VEHICLE(s_VehVan.id)
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(s_VehVan.id)
		ENDIF
	ENDIF
	
	INT i
	REPEAT COUNT_OF(obj_Grenades) i
		IF DOES_ENTITY_EXIST(obj_Grenades[i].id)
			IF bImmediately
				DELETE_OBJECT(obj_Grenades[i].id)
			ELSE
				IF IS_ENTITY_ATTACHED(obj_Grenades[i].id)
					DETACH_ENTITY(obj_Grenades[i].id)
				ENDIF
				SET_OBJECT_AS_NO_LONGER_NEEDED(obj_Grenades[i].id)
			ENDIF
		ENDIF
	ENDREPEAT
	if DOES_ENTITY_EXIST(obj_Lock)
		IF bImmediately
			DELETE_OBJECT(obj_Lock)
		ELSE
			IF IS_ENTITY_ATTACHED(obj_Lock)
				DETACH_ENTITY(obj_Lock)
			ENDIF
			SET_OBJECT_AS_NO_LONGER_NEEDED(obj_Lock)
		ENDIF
	endif
	
	IF sbi_SweatshopBin != NULL
		REMOVE_SCENARIO_BLOCKING_AREA(sbi_SweatshopBin)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_BRIEFCASE)
			REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), WEAPONTYPE_BRIEFCASE)
		ENDIF
	ENDIF
	
	KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
	
	RESET_ALL_BATTLEBUDDY_BEHAVIOUR_REQUESTS()
	
	IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_BRIEFCASE)
		REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), WEAPONTYPE_BRIEFCASE)
	ENDIF
	
	DISABLE_CHEAT(CHEAT_TYPE_GIVE_HEALTH_ARMOR, FALSE)
	
ENDPROC

//PURPOSE: Passed the mission
PROC Mission_Passed()
	TRIGGER_MUSIC_EVENT("JHP2A_STOP")
	CLEAR_PRINTS()
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	Mission_Cleanup(FALSE)
	IF GET_MISSION_COMPLETE_STATE(SP_HEIST_JEWELRY_PREP_1A)
		MISSION_FLOW_MISSION_PASSED(FALSE, TRUE)
	ELSE
		MISSION_FLOW_MISSION_PASSED(FALSE, FALSE)
	ENDIF
	TERMINATE_THIS_THREAD()
ENDPROC

//PURPOSE: Starts a mission fail
PROC Mission_Failed(MISSION_FAIL_FLAGS fail_condition = mff_debug_forced)

	TRIGGER_MUSIC_EVENT("JHP2A_FAIL")

	// Forced clean up fail!
	IF fail_condition = mff_default
		MISSION_FLOW_MISSION_FORCE_CLEANUP()
		Mission_Cleanup(FALSE)
		TERMINATE_THIS_THREAD()
		
	// all other normal fails
	ELIF NOT b_MissionFailed
		
		TEXT_LABEL_15 strFailMsg, strSubstring
		SWITCH fail_condition
			
			CASE mff_van_lost				strFailMsg = "JHP2A_FGOTAWAY"		BREAK
			CASE mff_van_left				strFailMsg = "JHP2A_FGOTAWAY2"		BREAK
			CASE mff_van_in_water			strFailMsg = "JHP2A_FCARGO"			BREAK
			CASE mff_cargo_left				strFailMsg = "JHP2A_FCARGO2"		BREAK
			CASE mff_debug_forced											
			FALLTHRU
			DEFAULT							strFailMsg = "JHP2A_FF"				BREAK
		ENDSWITCH

		IF IS_STRING_NULL_OR_EMPTY(strSubstring)
			MISSION_FLOW_MISSION_FAILED_WITH_REASON(strFailMsg)
		ELSE
			MISSION_FLOW_MISSION_FAILED_WITH_REASON_CONTAINING_TEXT(strFailMsg, strSubstring)
		ENDIF
		
		b_MissionFailed = TRUE
		
	ENDIF
	
ENDPROC

//PURPOSE: Called when the mission has failed and for what reason
PROC Mission_Fail_Update()

	IF b_MissionFailed
	AND (GET_MISSION_FLOW_SAFE_TO_CLEANUP() OR IS_SCREEN_FADED_OUT())

		Mission_Cleanup(TRUE)
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC

//PURPOSE: Checks key mission entities each frame to see if they are dead
PROC Mission_Entity_Death_Checks()

	IF DOES_ENTITY_EXIST(s_PedDriver.id)
		IF NOT IS_PED_IN_ANY_VEHICLE(s_PedDriver.id)
			UPDATE_AI_PED_BLIP(s_PedDriver.id, s_PedDriver.aiBlip)
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(s_VehVan.id)
		IF NOT IS_THIS_ENTITY_ALIVE(s_VehVan.id)
		AND IS_ENTITY_DEAD(s_VehVan.id)
		AND b_VehVanNoLongerNeeded
		AND GET_DISTANCE_BETWEEN_ENTITIES(s_VehVan.id, PLAYER_PED_ID()) > 200.0
			SET_VEHICLE_AS_NO_LONGER_NEEDED(s_VehVan.id)
		ENDIF
	ENDIF
	
ENDPROC

BOOL bGrabbedStatVehicle

//PURPOSE:
PROC Mission_Fail_Checks()

	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		IF NOT bGrabbedStatVehicle
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			bGrabbedStatVehicle = TRUE
		ENDIF
	ELSE
		IF bGrabbedStatVehicle
			INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(NULL)
			INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(NULL)
			bGrabbedStatVehicle = FALSE
		ENDIF
	ENDIF
	
	IF NOT b_PlayerPoliceScannerLine
		IF IS_PLAYER_WANTED_LEVEL_GREATER(PLAYER_ID(), 0)
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), s_VehVan.id, TRUE)
				PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_JSH_PREP_2A_01", 0.0)
				b_PlayerPoliceScannerLine = TRUE
			ELIF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_BRIEFCASE)
				PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_JSH_PREP_2A_02", 0.0)
				b_PlayerPoliceScannerLine = TRUE
			ENDIF
			
		ENDIF
	ENDIF

	// Do distance check on the van
	IF i_missionStage = enum_to_int(msf_gas_steal)
		
		SWITCH eVanState
			// Van is drivable and contains the cargo
			CASE VANSTATE_DRIVABLE_WITH_CARGO
			
				// Van driver is alive
				IF DOES_ENTITY_EXIST(s_PedDriver.id)
				AND NOT IS_PED_INJURED(s_PedDriver.id)
				AND IS_MISSION_EVENT_RUNNING(s_Events[mef_driver_manager].sData) 
					
					IF s_Events[mef_driver_manager].sData.iStage = enum_to_int(DRIVERAI_ONROUTE)
					
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), s_VehVan.id) > 500
							Mission_Failed(mff_van_lost)
						ENDIF
					
					ELIF s_Events[mef_driver_manager].sData.iStage = enum_to_int(DRIVERAI_FLEEING)
					
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), s_VehVan.id) > 250
							Mission_Failed(mff_van_lost)
						ENDIF
					
					ELSE
					
						IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), s_VehVan.id) > 250
							Mission_Failed(mff_van_left)
						ENDIF
					
					ENDIF
				
				// driver is dead
				ELSE
				
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), s_VehVan.id) > 250
						Mission_Failed(mff_van_left)
					ENDIF
				
				ENDIF
			BREAK
			// Van is not drivable and is at the side of the road with the cargo in the back
			CASE VANSTATE_NOT_DRIVABLE_WITH_CARGO
			
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), s_VehVan.id) > 250
					Mission_Failed(mff_van_left)
				ENDIF
			
			BREAK
			//Cargo has fallen out of the van the fail case should now be when a distance check on the cargo
			CASE VANSTATE_CARGO_FALLEN_OUT	FALLTHRU
			CASE VANSTATE_CARGO_LOOSE_INSIDE
			
				IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_BRIEFCASE)
					IF DOES_ENTITY_EXIST(obj_Grenades[0].id) AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), obj_Grenades[0].id) > 250
					AND DOES_ENTITY_EXIST(obj_Grenades[1].id) AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), obj_Grenades[1].id) > 250 
					AND DOES_ENTITY_EXIST(obj_Grenades[2].id) AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), obj_Grenades[2].id) > 250 
						Mission_Failed(mff_cargo_left)
					ENDIF
				ENDIF
			
			BREAK
		ENDSWITCH
			
		
	ENDIF
	
	IF DOES_ENTITY_EXIST(s_VehVan.id)
		IF IS_ENTITY_IN_WATER(s_VehVan.id)
		AND NOT IS_VEHICLE_DRIVEABLE(s_VehVan.id)
		AND NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_BRIEFCASE)
			Mission_Failed(mff_van_in_water)
		ENDIF
	ENDIF
ENDPROC


PROC EVENT_VAN_DRIVER_MANAGER(MISSION_EVENT_DATA &sData)
	
	IF NOT IS_MISSION_EVENT_ENDED(sData)
		// Driver is dead
		IF NOT DOES_ENTITY_EXIST(s_PedDriver.id)
		OR IS_PED_INJURED(s_PedDriver.id)
		OR (DOES_ENTITY_EXIST(s_VehVan.id) AND GET_DISTANCE_BETWEEN_ENTITIES(s_VehVan.id, s_PedDriver.id) > 200.0)
		OR (NOT IS_PED_IN_ANY_VEHICLE(s_PedDriver.id) AND GET_DISTANCE_BETWEEN_ENTITIES(s_VehVan.id, s_PedDriver.id) > 200.0)
			
			IF DOES_ENTITY_EXIST(s_VehVan.id)
			AND IS_VEHICLE_DRIVEABLE(s_VehVan.id)
				SET_VEHICLE_DISABLE_TOWING(s_VehVan.id, FALSE)
			ENDIF
			
			SET_PED_AS_NO_LONGER_NEEDED(s_PedDriver.id)
			END_MISSION_EVENT(sData)
		
		// Driver is alive
		ELSE
			IF sData.iStage != ENUM_TO_INT(DRIVERAI_COMBATING)
			AND sData.iStage != ENUM_TO_INT(DRIVERAI_STOPPED_AIRPORT)
			AND (NOT DOES_ENTITY_EXIST(s_VehVan.id) 
			OR NOT IS_VEHICLE_DRIVEABLE(s_VehVan.id)
			OR NOT IS_PED_IN_VEHICLE(s_PedDriver.id, s_VehVan.id))
			
				i_VanDriverDialogue = 0
				i_VanDriverDiaTimer	= 0
				str_VanDriverDiaRoot = ""
				SET_MISSION_EVENT_STAGE(sData, enum_to_int(DRIVERAI_COMBATING))
				CPRINTLN(DEBUG_MISSION, "GONE TO DRIVERAI_COMBATING: From GENERAL van fucked/driver not in van")
				
			ELSE
			
				// Update the number of times the van has been rammed
				IF sData.iStage = enum_to_int(DRIVERAI_ONROUTE) 
				OR sData.iStage = enum_to_int(DRIVERAI_ONROUTE_AIRPORT)
				OR sData.iStage = enum_to_int(DRIVERAI_FLEEING)
				
					IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
						// Count number of times the van has been rammed with enough force
						VEHICLE_INDEX vehPlayer = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
						IF IS_ENTITY_TOUCHING_ENTITY(vehPlayer, s_VehVan.id)
							IF NOT b_VanRammedLastFrame
							AND GET_GAME_TIMER() - i_RamTimer > 500
							AND f_PlayerVsVanClosingSpeedLastFrame >= MIN_CLOSING_SPEED	// check the closing speed was high enough the prev frame to register a collision (using the last frame due to the velocities already having been resolved by the physics engine)
								b_VanRammedLastFrame 	= TRUE
								i_VanRammedCount++	
							ENDIF
						ELSE
							IF b_VanRammedLastFrame
								i_RamTimer 				= GET_GAME_TIMER()
								b_VanRammedLastFrame 	= FALSE
							ENDIF
						ENDIF
						
						// Calculate closing velocity for this frame
						VECTOR vContactNormal = NORMALISE_VECTOR(GET_ENTITY_COORDS(vehPlayer) - GET_ENTITY_COORDS(s_VehVan.id))
						VECTOR vVelBMinusVelA = GET_ENTITY_VELOCITY(s_VehVan.id) - GET_ENTITY_VELOCITY(vehPlayer)
						f_PlayerVsVanClosingSpeedLastFrame = DOT_PRODUCT(vVelBMinusVelA, vContactNormal)
						#IF IS_DEBUG_BUILD
							debug_fClosingSpeed = f_PlayerVsVanClosingSpeedLastFrame
						#ENDIF
					ENDIF
				ENDIF
				
			ENDIF
		
		
			SWITCH int_to_enum(DRIVER_AI_ENUM, sData.iStage)
				CASE DRIVERAI_ONROUTE
					// Van has been rammed or damaged by the player 
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(s_VehVan.id, PLAYER_PED_ID())
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(s_PedDriver.id, PLAYER_PED_ID())
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOT_FIRED)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOT_FIRED_BULLET_IMPACT)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOT_FIRED_WHIZZED_BY)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_GUN_AIMED_AT)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOCKING_SEEN_WEAPON_THREAT)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOCKING_VISIBLE_WEAPON)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_EXPLOSION)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_FIRE_NEARBY)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_ON_FIRE)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_POTENTIAL_WALK_INTO_FIRE)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_POTENTIAL_WALK_INTO_FIRE)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_VEHICLE_ON_FIRE)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOCKING_EXPLOSION)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOCKING_FIRE)
					OR IS_PED_BEING_JACKED(s_PedDriver.id)
					OR IS_ENTITY_ON_FIRE(s_VehVan.id)
					OR i_VanRammedCount > 0
					
						i_VanDriverDialogue = 0
						i_VanDriverDiaTimer	= 0
						str_VanDriverDiaRoot = ""
						SET_MISSION_EVENT_STAGE(sData, enum_to_int(DRIVERAI_FLEEING))
						CPRINTLN(DEBUG_MISSION, "GONE TO DRIVERAI_FLEEING: From DRIVERAI_ONROUTE")

					ELSE
						
						// Manage horn
						IF DOES_ENTITY_EXIST(s_VehVan.id) AND IS_VEHICLE_DRIVEABLE(s_VehVan.id)
							IF (IS_ENTITY_STATIC(PLAYER_PED_ID()) OR f_PlayerVsVanClosingSpeedLastFrame > 0.5)
							AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),
								GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_VehVan.id, <<0,2.0,-0.5>>),
								GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_VehVan.id, <<0,10.0,2.0>>),
								4.5, FALSE)
								
								IF NOT IS_HORN_ACTIVE(s_VehVan.id)
									IF i_HornTimer = -1
										i_HornTimer = GET_GAME_TIMER()
									ELIF GET_GAME_TIMER() - i_HornTimer > 5000
										START_VEHICLE_HORN(s_VehVan.id, 2000, HASH("NORMAL"), FALSE)
									ENDIF
								ELSE
									i_HornTimer = -1
								ENDIF
							ENDIF
						ENDIF
					
						IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(s_VehVan.id, <<-993.7236, -2418.7566, 12.9447>>) < 20.0
							Load_Asset_Waypoint(s_AssetData, "jhp2a_airport")
						ENDIF
					
						// Make sure is driving on route
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(s_PedDriver.id, SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING)
							// Has reached the airport, change state
							IF IS_ENTITY_IN_ANGLED_AREA(s_VehVan.id, <<-990.631165,-2413.213867,12.694712>>, <<-996.019409,-2422.607666,16.058456>>, 8.750000)
								IF GET_IS_WAYPOINT_RECORDING_LOADED("jhp2a_airport")
									i_VanDriverDialogue = 0
									i_VanDriverDiaTimer	= 0
									str_VanDriverDiaRoot = ""
									SET_MISSION_EVENT_STAGE(sData, enum_to_int(DRIVERAI_ONROUTE_AIRPORT))
									CPRINTLN(DEBUG_MISSION, "GONE TO DRIVERAI_ONROUTE_AIRPORT: From DRIVERAI_ONROUTE")
								ENDIF
								
							// Has just lost the waypoint task, give it again
							ELSE
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(s_PedDriver.id, s_VehVan.id, g_JHP2A_string_WayPoint, DRIVINGMODE_STOPFORCARS_IGNORELIGHTS, -1, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, 12.0)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE DRIVERAI_ONROUTE_AIRPORT
					// Van has been rammed or damaged by the player 
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(s_VehVan.id, PLAYER_PED_ID())
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(s_PedDriver.id, PLAYER_PED_ID())
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOT_FIRED)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOT_FIRED_BULLET_IMPACT)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOT_FIRED_WHIZZED_BY)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_GUN_AIMED_AT)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOCKING_SEEN_WEAPON_THREAT)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOCKING_VISIBLE_WEAPON)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_EXPLOSION)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_FIRE_NEARBY)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_ON_FIRE)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_POTENTIAL_WALK_INTO_FIRE)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_POTENTIAL_WALK_INTO_FIRE)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_VEHICLE_ON_FIRE)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOCKING_EXPLOSION)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOCKING_FIRE)
					OR IS_PED_BEING_JACKED(s_PedDriver.id)
					OR IS_ENTITY_ON_FIRE(s_VehVan.id)
					OR i_VanRammedCount > 0
						
						// Straight to combatting now as the player is 
						i_VanDriverDialogue = 0
						i_VanDriverDiaTimer	= 0
						str_VanDriverDiaRoot = ""
						SET_MISSION_EVENT_STAGE(sData, enum_to_int(DRIVERAI_COMBATING))
						CPRINTLN(DEBUG_MISSION, "GONE TO DRIVERAI_COMBATING: From DRIVERAI_ONROUTE_AIRPORT")
					
					ELSE
					
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(s_PedDriver.id, SCRIPT_TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING)
							// Has parked up go
							IF IS_ENTITY_IN_ANGLED_AREA(s_VehVan.id, <<-1096.138672,-2467.116699,12.695615>>, <<-1101.641357,-2476.774658,15.630400>>, 7.812500)
								i_VanDriverDialogue = 0
								i_VanDriverDiaTimer	= 0
								str_VanDriverDiaRoot = ""
								SET_MISSION_EVENT_STAGE(sData, enum_to_int(DRIVERAI_STOPPED_AIRPORT))
								CPRINTLN(DEBUG_MISSION, "GONE TO DRIVERAI_STOPPED_AIRPORT: From DRIVERAI_ONROUTE_AIRPORT")
							
							// lost waypoint task
							ELSE
								TASK_VEHICLE_FOLLOW_WAYPOINT_RECORDING(s_PedDriver.id, s_VehVan.id, "jhp2a_airport", DRIVINGMODE_STOPFORCARS_IGNORELIGHTS, -1, EWAYPOINT_START_FROM_CLOSEST_POINT | EWAYPOINT_VEHICLES_USE_AI_SLOWDOWN, -1, 12.0)
							ENDIF
						ENDIF
					
					ENDIF
				BREAK
				CASE DRIVERAI_STOPPED_AIRPORT
				
					// Van has been rammed or damaged by the player 
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(s_VehVan.id, PLAYER_PED_ID())
					OR HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(s_PedDriver.id, PLAYER_PED_ID())
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOT_FIRED)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOT_FIRED_BULLET_IMPACT)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOT_FIRED_WHIZZED_BY)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_GUN_AIMED_AT)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOCKING_SEEN_WEAPON_THREAT)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOCKING_VISIBLE_WEAPON)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_EXPLOSION)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_FIRE_NEARBY)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_ON_FIRE)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_POTENTIAL_WALK_INTO_FIRE)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_POTENTIAL_WALK_INTO_FIRE)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_VEHICLE_ON_FIRE)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOCKING_EXPLOSION)
					OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOCKING_FIRE)
					OR IS_PED_BEING_JACKED(s_PedDriver.id)
					OR IS_ENTITY_ON_FIRE(s_VehVan.id)
					OR IS_PED_IN_VEHICLE(PLAYER_PED_ID(), s_VehVan.id, TRUE)
					OR i_VanRammedCount > 0
						
						// Straight to combatting now as the player is 
						i_VanDriverDialogue = 0
						i_VanDriverDiaTimer	= 0
						str_VanDriverDiaRoot = ""
						SET_MISSION_EVENT_STAGE(sData, enum_to_int(DRIVERAI_COMBATING))
						CPRINTLN(DEBUG_MISSION, "GONE TO DRIVERAI_COMBATING: From DRIVERAI_STOPPED_AIRPORT")
						
					ELSE

						IF IS_PED_IN_VEHICLE(s_PedDriver.id, s_VehVan.id, TRUE)
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(s_PedDriver.id, SCRIPT_TASK_LEAVE_VEHICLE)
								TASK_LEAVE_VEHICLE(s_PedDriver.id, s_VehVan.id)
							ENDIF
						ELSE
							SET_VEHICLE_DOORS_LOCKED(s_VehVan.id, VEHICLELOCK_LOCKED)
						
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(s_PedDriver.id, SCRIPT_TASK_PERFORM_SEQUENCE)
								OPEN_SEQUENCE_TASK(seq)
									TASK_FOLLOW_NAV_MESH_TO_COORD(null, <<-1093.4863, -2471.6694, 13.0716>>, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)						
									TASK_START_SCENARIO_IN_PLACE(null, "WORLD_HUMAN_SMOKING", -1, TRUE)
								CLOSE_SEQUENCE_TASK(seq)
								TASK_PERFORM_SEQUENCE(s_PedDriver.id, seq)
								CLEAR_SEQUENCE_TASK(seq)
							ENDIF
						ENDIF
					
					ENDIF
				
				BREAK
				CASE DRIVERAI_FLEEING
					// Check the number of tyres that have burst
					INT iNumTyresBurst
					IF IS_VEHICLE_TYRE_BURST(s_VehVan.id, SC_WHEEL_CAR_FRONT_LEFT, TRUE)	iNumTyresBurst++ ENDIF
					IF IS_VEHICLE_TYRE_BURST(s_VehVan.id, SC_WHEEL_CAR_FRONT_RIGHT, TRUE)	iNumTyresBurst++ ENDIF
					IF IS_VEHICLE_TYRE_BURST(s_VehVan.id, SC_WHEEL_CAR_REAR_LEFT, TRUE)		iNumTyresBurst++ ENDIF
					IF IS_VEHICLE_TYRE_BURST(s_VehVan.id, SC_WHEEL_CAR_REAR_RIGHT, TRUE)	iNumTyresBurst++ ENDIF
					
					// Check the following coniditions for stopping the van
					IF GET_ENTITY_HEALTH(s_VehVan.id) 					<= VAN_STOP_HEALTH_DAMAGE_LIMIT
					OR GET_VEHICLE_ENGINE_HEALTH(s_VehVan.id) 			<= VAN_STOP_ENGINE_DAMAGE_LIMIT
					OR GET_VEHICLE_PETROL_TANK_HEALTH(s_VehVan.id) 		<= VAN_STOP_PETROL_DAMAGE_LIMIT
					OR IS_PED_INJURED(s_PedDriver.id)
					OR iNumTyresBurst >= 2
					OR i_VanRammedCount > 6
						
						// Player can now steal the van by towing it, and the driver should just try to combat you.
						IF DOES_ENTITY_EXIST(s_VehVan.id)
						AND IS_VEHICLE_DRIVEABLE(s_VehVan.id)
							SET_VEHICLE_DISABLE_TOWING(s_VehVan.id, FALSE)
						ENDIF
						
						i_VanDriverDialogue = 0
						i_VanDriverDiaTimer	= 0
						str_VanDriverDiaRoot = ""
						SET_MISSION_EVENT_STAGE(sData, enum_to_int(DRIVERAI_COMBATING))
						CPRINTLN(DEBUG_MISSION, "GONE TO DRIVERAI_COMBATING: From DRIVERAI_FLEEING")
					
					ELSE
						IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(s_PedDriver.id, SCRIPT_TASK_VEHICLE_MISSION)
							TASK_VEHICLE_MISSION_PED_TARGET(s_PedDriver.id, s_VehVan.id, PLAYER_PED_ID(), MISSION_FLEE, 90.0, DRIVINGMODE_AVOIDCARS_RECKLESS, 400, 10, FALSE)
						ENDIF	
					ENDIF
				BREAK
				CASE DRIVERAI_COMBATING
				
					IF DOES_ENTITY_EXIST(s_VehVan.id)
					AND NOT IS_ENTITY_DEAD(s_VehVan.id)
						IF IS_PED_IN_VEHICLE(s_PedDriver.id, s_VehVan.id)
							IF NOT IS_VEHICLE_ALMOST_STOPPED(s_VehVan.id)
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(s_PedDriver.id, SCRIPT_TASK_VEHICLE_TEMP_ACTION)
									CLEAR_PED_TASKS(s_PedDriver.id)
									TASK_VEHICLE_TEMP_ACTION(s_PedDriver.id, s_VehVan.id, TEMPACT_HANDBRAKETURNRIGHT, -1)
								ENDIF		
							ELSE
								IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(s_PedDriver.id, SCRIPT_TASK_LEAVE_VEHICLE)
									TASK_LEAVE_VEHICLE(s_PedDriver.id, s_VehVan.id, ECF_DONT_CLOSE_DOOR)
								ENDIF
							ENDIF
						ELSE
							IF NOT IS_SCRIPT_TASK_RUNNING_OR_STARTING(s_PedDriver.id, SCRIPT_TASK_COMBAT)
								SET_PED_CAN_BE_TARGETTED(s_PedDriver.id, TRUE)
								TASK_COMBAT_PED(s_PedDriver.id, PLAYER_PED_ID())
							ENDIF
						ENDIF
					ENDIF
				
					// Apply damage to the van
					IF DOES_ENTITY_EXIST(s_VehVan.id)
					AND NOT IS_ENTITY_DEAD(s_VehVan.id)
					
						// Allow the doors to be broken
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(s_VehVan.id, SC_DOOR_REAR_LEFT, TRUE)
						SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(s_VehVan.id, SC_DOOR_REAR_RIGHT, TRUE)
						
					ENDIF
				BREAK
			ENDSWITCH
			
		ENDIF
		
		// Dialogue
		IF DOES_ENTITY_EXIST(s_PedDriver.id)
		AND NOT IS_PED_INJURED(s_PedDriver.id)
		AND NOT IS_CONVERSATION_PED_DEAD(s_PedDriver.id)
		
			SWITCH int_to_enum(DRIVER_AI_ENUM, sData.iStage)
				CASE DRIVERAI_FLEEING
				
					SWITCH i_VanDriverDialogue
						CASE 0
							IF IS_SAFE_TO_START_CONVERSATION()
							AND NOT IS_AMBIENT_SPEECH_PLAYING(s_PedDriver.id)
								IF IS_PED_BEING_JACKED(s_PedDriver.id)
									PLAY_PED_AMBIENT_SPEECH(s_PedDriver.id, "JACKED_GENERIC", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
								ELSE
									PLAY_PED_AMBIENT_SPEECH(s_PedDriver.id, "GENERIC_SHOCKED_HIGH", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
								ENDIF
								i_VanDriverDialogue++
							ENDIF
						BREAK
						CASE 1
							// cargo has fallen out or he is not in the vehicle skip the conversation
							IF NOT IS_PED_IN_VEHICLE(s_PedDriver.id, s_VehVan.id)
							OR eVanState = VANSTATE_CARGO_FALLEN_OUT
								IF NOT IS_PED_IN_VEHICLE(s_PedDriver.id, s_VehVan.id)
									i_VanDriverDialogue = 4	// stop playing dialogue as hes not in the vehicle 
								ELSE
									i_VanDriverDialogue++
								ENDIF
							ELIF IS_SAFE_TO_START_CONVERSATION()
							AND NOT IS_AMBIENT_SPEECH_PLAYING(s_PedDriver.id)
								IF CREATE_CONVERSATION(s_Convo, str_Dialogue, "JS_INIT_M", CONV_PRIORITY_HIGH)
									i_VanDriverDialogue++
								ENDIF
							ENDIF
						BREAK
						CASE 2
							IF IS_SAFE_TO_START_CONVERSATION()
							AND NOT IS_AMBIENT_SPEECH_PLAYING(s_PedDriver.id)
								
								IF IS_STRING_NULL_OR_EMPTY(str_VanDriverDiaRoot)
								
									FLOAT fDist
									fDist = VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(s_PedDriver.id))
								
									IF (b_VanRammedLastFrame
									OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOT_FIRED)
									OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOT_FIRED_BULLET_IMPACT)
									OR HAS_PED_RECEIVED_EVENT(s_PedDriver.id, EVENT_SHOT_FIRED_WHIZZED_BY)
									OR fDist < 49) // 7m^2
									
									AND GET_GAME_TIMER() - i_VanDriverDiaTimer > 5000
									AND fDist < 225 // 15m^2									
									
										IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
											str_VanDriverDiaRoot = "GENERIC_CURSE_HIGH"
										ELSE
											str_VanDriverDiaRoot = "JS_ATT_M"
										ENDIF
										
									ENDIF
									
								ENDIF
								
								IF NOT IS_STRING_NULL_OR_EMPTY(str_VanDriverDiaRoot)
									IF ARE_STRINGS_EQUAL(str_VanDriverDiaRoot, "GENERIC_CURSE_HIGH")
										PLAY_PED_AMBIENT_SPEECH(s_PedDriver.id, "GENERIC_CURSE_HIGH", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
										i_VanDriverDialogue++
									ELSE
										IF CREATE_CONVERSATION(s_Convo, str_Dialogue, str_VanDriverDiaRoot, CONV_PRIORITY_HIGH)
											i_VanDriverDialogue++
										ENDIF
										i_VanDriverDialogue++
									ENDIF
								ENDIF
								
							ENDIF
						BREAK
						CASE 3
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							AND NOT IS_AMBIENT_SPEECH_PLAYING(s_PedDriver.id)
								i_VanDriverDiaTimer 	= GET_GAME_TIMER()
								str_VanDriverDiaRoot	= ""
								i_VanDriverDialogue--
							ENDIF
						BREAK
					ENDSWITCH
				
				BREAK
				
				CASE DRIVERAI_COMBATING
					SWITCH i_VanDriverDialogue
						CASE 0
							IF IS_SAFE_TO_START_CONVERSATION()
							AND NOT IS_AMBIENT_SPEECH_PLAYING(s_PedDriver.id)
								
								IF IS_STRING_NULL_OR_EMPTY(str_VanDriverDiaRoot)
										
									IF GET_GAME_TIMER() - i_VanDriverDiaTimer > 5000
									AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(s_PedDriver.id)) < 400 // 20m^2
									
										IF GET_RANDOM_INT_IN_RANGE(0, 2) = 0
											str_VanDriverDiaRoot = "GENERIC_INSULT_HIGH"
										ELSE
											str_VanDriverDiaRoot = "JS_COMB_M"
										ENDIF
										
									ENDIF
									
								ENDIF
								
								IF NOT IS_STRING_NULL_OR_EMPTY(str_VanDriverDiaRoot)
									IF ARE_STRINGS_EQUAL(str_VanDriverDiaRoot, "GENERIC_INSULT_HIGH")
										PLAY_PED_AMBIENT_SPEECH(s_PedDriver.id, "GENERIC_INSULT_HIGH", SPEECH_PARAMS_FORCE_SHOUTED_CLEAR)
										i_VanDriverDialogue++
									ELSE
										IF CREATE_CONVERSATION(s_Convo, str_Dialogue, str_VanDriverDiaRoot, CONV_PRIORITY_HIGH)
											i_VanDriverDialogue++
										ENDIF
									ENDIF
								ENDIF
								
							ENDIF
						BREAK
						CASE 1
							IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
							AND NOT IS_AMBIENT_SPEECH_PLAYING(s_PedDriver.id)
								i_VanDriverDiaTimer 	= GET_GAME_TIMER()
								str_VanDriverDiaRoot	= ""
								i_VanDriverDialogue--
							ENDIF
						BREAK
					ENDSWITCH
				BREAK
			ENDSWITCH
		ENDIF
		
	ENDIF

	// Cleanup event
	IF IS_MISSION_EVENT_ENDED(sData)
		
	ENDIF
	
ENDPROC

PROC EVENT_CARGO_DROP_MANAGER(MISSION_EVENT_DATA &sData)

	IF NOT IS_MISSION_EVENT_ENDED(sData)
	
		IF IS_ENTITY_DEAD(s_VehVan.id)
		
			DETACH_CASE_FROM_VAN(obj_Grenades[0].id, TRUE)
			DETACH_CASE_FROM_VAN(obj_Grenades[1].id, TRUE)
			DETACH_CASE_FROM_VAN(obj_Grenades[2].id, TRUE)
			
			b_VehVanNoLongerNeeded = TRUE
			END_MISSION_EVENT(sData)
		
		ELSE
	
			SWITCH sData.iStage
				CASE MISSION_EVENT_STAGE_START
					Load_Asset_Audiobank(s_AssetData, "JWL_HEIST_PREP_2A_SHOOTING_LOCK")
					IF REQUEST_SCRIPT_AUDIO_BANK("JWL_HEIST_PREP_2A_SHOOTING_LOCK")
						IF GET_GAME_TIMER() - i_BulletHitTimer > 150
						
							IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(obj_Lock, player_ped_id(),false)
								
								IF HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(obj_Lock, WEAPONTYPE_MOLOTOV)
								OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(obj_Lock, WEAPONTYPE_BZGAS)
								OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(obj_Lock, WEAPONTYPE_KNIFE)
								OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(obj_Lock, WEAPONTYPE_UNARMED)
								OR HAS_ENTITY_BEEN_DAMAGED_BY_WEAPON(obj_Lock, WEAPONTYPE_WATER_CANNON)
									CLEAR_ENTITY_LAST_WEAPON_DAMAGE(obj_Lock)
								ELSE
									CLEAR_ENTITY_LAST_DAMAGE_ENTITY(obj_Lock)
									i_BulletHitTimer = GET_GAME_TIMER()
									i_NumBulletHitsOnDoorLock++
									
									IF i_NumBulletHitsOnDoorLock >= 3
										PLAY_SOUND_FROM_ENTITY(-1, "Lock_Destroyed", obj_Lock, "JWL_PREP_2A_SOUNDS")
									ELSE
										PLAY_SOUND_FROM_ENTITY(-1, "Lock_Damage", obj_Lock, "JWL_PREP_2A_SOUNDS")
									ENDIF
								ENDIF
								
							ENDIF
							
						ELSE
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(obj_Lock)
						ENDIF
					ENDIF
					
					IF i_NumBulletHitsOnDoorLock >= 3
					OR IS_ENTITY_DEAD(obj_Lock)
					OR IS_VEHICLE_DOOR_DAMAGED(s_VehVan.id, SC_DOOR_REAR_LEFT) 					OR IS_VEHICLE_DOOR_DAMAGED(s_VehVan.id, SC_DOOR_REAR_RIGHT)
					OR GET_VEHICLE_DOOR_ANGLE_RATIO(s_VehVan.id, SC_DOOR_REAR_LEFT) > 0.0 		OR GET_VEHICLE_DOOR_ANGLE_RATIO(s_VehVan.id, SC_DOOR_REAR_RIGHT) > 0.0

						IF DOES_ENTITY_EXIST(obj_Lock)
							DELETE_OBJECT(obj_Lock)
						ENDIF
						
						IF NOT IS_VEHICLE_DOOR_DAMAGED(s_VehVan.id, SC_DOOR_REAR_LEFT)
							SET_VEHICLE_DOOR_OPEN(s_VehVan.id,SC_DOOR_REAR_LEFT, FALSE)
						ENDIF
						IF NOT IS_VEHICLE_DOOR_DAMAGED(s_VehVan.id, SC_DOOR_REAR_RIGHT)
							SET_VEHICLE_DOOR_OPEN(s_VehVan.id,SC_DOOR_REAR_RIGHT, FALSE)
						ENDIF
						
						bDoorSwingingFreeL = FALSE
						bDoorSwingingFreeR = FALSE
						
						// detach the cargo so it falls out the back of the van.
						DETACH_CASE_FROM_VAN(obj_Grenades[0].id, FALSE)
						DETACH_CASE_FROM_VAN(obj_Grenades[1].id, FALSE)
						DETACH_CASE_FROM_VAN(obj_Grenades[2].id, FALSE)
						
						SET_MISSION_EVENT_STAGE(sData, 2)
					
					ENDIF
				BREAK
				CASE 2
					
					IF IS_ENTITY_ALIVE(s_VehVan.id)

						IF ABSF(GET_VEHICLE_DOOR_ANGLE_RATIO(s_VehVan.id, SC_DOOR_REAR_LEFT)) > 0.5
						AND ABSF(GET_VEHICLE_DOOR_ANGLE_RATIO(s_VehVan.id, SC_DOOR_REAR_RIGHT)) > 0.5
						
//							// detach the cargo so it falls out the back of the van.
//							DETACH_CASE_FROM_VAN(obj_Grenades[0].id, FALSE)
//							DETACH_CASE_FROM_VAN(obj_Grenades[1].id, FALSE)
//							DETACH_CASE_FROM_VAN(obj_Grenades[2].id, FALSE)
							
							SET_MISSION_EVENT_STAGE(sData, 3)
						
						ENDIF
					
					ENDIF
				
				BREAK
				CASE 3
				
					IF IS_ENTITY_ALIVE(s_VehVan.id)
				
						IF NOT IS_VEHICLE_DOOR_DAMAGED(s_VehVan.id, SC_DOOR_REAR_LEFT)
							IF IS_VEHICLE_DOOR_FULLY_OPEN(s_VehVan.id, SC_DOOR_REAR_LEFT)
								SET_VEHICLE_DOOR_OPEN(s_VehVan.id,SC_DOOR_REAR_LEFT, TRUE)
								SET_VEHICLE_DOOR_LATCHED(s_VehVan.id, SC_DOOR_REAR_LEFT, FALSE, FALSE)
								bDoorSwingingFreeL = TRUE
							ENDIF
						ELSE
							bDoorSwingingFreeL = TRUE
						ENDIF
						
						IF NOT IS_VEHICLE_DOOR_DAMAGED(s_VehVan.id, SC_DOOR_REAR_RIGHT)
							IF IS_VEHICLE_DOOR_FULLY_OPEN(s_VehVan.id, SC_DOOR_REAR_RIGHT)
								SET_VEHICLE_DOOR_OPEN(s_VehVan.id,SC_DOOR_REAR_RIGHT, TRUE)
								SET_VEHICLE_DOOR_LATCHED(s_VehVan.id, SC_DOOR_REAR_RIGHT, FALSE, FALSE)
								bDoorSwingingFreeR = TRUE
							ENDIF
						ELSE
							bDoorSwingingFreeR = TRUE
						ENDIF
						
						IF bDoorSwingingFreeL AND bDoorSwingingFreeR
							b_VehVanNoLongerNeeded = TRUE
							SET_MISSION_EVENT_STAGE(sData, 4)
						ENDIF
						
					ENDIF
				
				BREAK
			ENDSWITCH
		ENDIF
		
	ENDIF
	
	// Cleanup event
	IF IS_MISSION_EVENT_ENDED(sData)
		
	ENDIF

ENDPROC

PROC ADD_ALL_MISSION_EVENTS()
	ADD_NEW_MISSION_EVENT(s_Events[mef_driver_manager], 	&EVENT_VAN_DRIVER_MANAGER, 		"Van Driver Manager")
	ADD_NEW_MISSION_EVENT(s_Events[mef_cargo_manager],		&EVENT_CARGO_DROP_MANAGER,		"Cargo Manager")
ENDPROC

PROC GET_SKIP_STAGE_COORD_AND_HEADING(INT iStage, VECTOR &vCoord, FLOAT &fHeading)

	SWITCH int_to_enum(MISSION_STAGE_FLAGS, iStage)
		CASE msf_gas_steal					vCoord = <<1256.9364, 556.8262, 79.7001>>		fHeading = 134.0936			BREAK
		CASE msf_mission_passed				vCoord = <<692.2659, -1004.1848, 21.9451>>		fHeading = 3.5976 			BREAK
	ENDSWITCH

ENDPROC

PROC Mission_Setup()

	ADD_ALL_MISSION_EVENTS()
	
	ADD_RELATIONSHIP_GROUP("SECDRIVER", rel_Security)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rel_Security, RELGROUPHASH_COP)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, rel_Security, RELGROUPHASH_SECURITY_GUARD)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_COP, rel_Security)

	IF IS_REPEAT_PLAY_ACTIVE()		// REPLAY VIA START MENU
	OR IS_REPLAY_IN_PROGRESS()		// Do not do on retries as player is put off mision to start with so we will need to grab trigger scene stuff
	
		i_SkipToStage = enum_to_int(msf_gas_steal)
		
		IF IS_REPLAY_IN_PROGRESS()
			IF g_bShitskipAccepted
				i_SkipToStage += 1
				CPRINTLN(DEBUG_MISSION, "JHP1A: g_bShitskipAccepted = TRUE")
			ENDIF
		ENDIF
		
		CPRINTLN(DEBUG_MISSION, "JHP1A: stage skipping to ", i_SkipToStage)
		
		// always use this route when doing a repeat play
		g_JHP2A_string_WayPoint = "jhp2a_alt"
		
		// New replay streaming 
		IF IS_REPLAY_IN_PROGRESS()
			VECTOR vSkipToCoord
			FLOAT fSkipToHeading
			GET_SKIP_STAGE_COORD_AND_HEADING(i_SkipToStage, vSkipToCoord, fSkipToHeading)
			START_REPLAY_SETUP(vSkipToCoord, fSkipToHeading)
			CPRINTLN(DEBUG_MISSION, "JHP1A: Using replay warp system - Warp Coord ", vSkipToCoord, " Warp Heading ", fSkipToHeading)
		ENDIF
		
		b_DoSkip = TRUE
	ELSE
		WHILE NOT PREPARE_INITIAL_SCENE(TRUE)
			WAIT(0)
			Update_Asset_Management_System(s_AssetData)
		ENDWHILE
	ENDIF

	Load_Asset_Additional_Text(s_AssetData, "JHP2A", MISSION_TEXT_SLOT)

	#IF IS_DEBUG_BUILD
		debug_widget = START_WIDGET_GROUP("Jewelry Store Job - Prep2A")
			ADD_WIDGET_INT_READ_ONLY("Mission Stage", i_missionStage)
			ADD_WIDGET_INT_READ_ONLY("Mission Substage", i_missionSubstage)
			ADD_WIDGET_INT_READ_ONLY("RamCount", i_VanRammedCount)
			ADD_WIDGET_FLOAT_READ_ONLY("ClosingSpeed", debug_fClosingSpeed)
			START_WIDGET_GROUP("Cases")
				twi_vanState = ADD_TEXT_WIDGET("VanState")
				ADD_WIDGET_VECTOR_SLIDER("Van inside v1", v_VanCargo1, -20, 20, 0.1)
				ADD_WIDGET_VECTOR_SLIDER("Van inside v2", v_VanCargo2, -20, 20, 0.1)
			STOP_WIDGET_GROUP()
			
		STOP_WIDGET_GROUP()
		SET_LOCATES_HEADER_WIDGET_GROUP(debug_widget)
		
		s_ZMenuStruct[0].sTxtLabel 		= "STEAL THE VAN"
		s_ZMenuStruct[1].sTxtLabel 		= "MISSION PASSED"
	#ENDIF
	
	SET_WANTED_LEVEL_MULTIPLIER(0.1)
	SET_ALL_RANDOM_PEDS_FLEE(PLAYER_ID(), TRUE)
	
	INIT_BATTLEBUDDY_BEHAVIOUR_FOR_MISSION(SP_HEIST_JEWELRY_PREP_2A)
	
	ADD_PED_FOR_DIALOGUE(s_Convo, 0, PLAYER_PED_ID(), "MICHAEL")
	ADD_PED_FOR_DIALOGUE(s_Convo, 3, null, "Lester")
	
	SET_VEHICLE_MODEL_IS_SUPPRESSED(BOXVILLE3, TRUE)
	SET_PED_MODEL_IS_SUPPRESSED(S_M_M_Armoured_01, TRUE)
	
	IF HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_BRIEFCASE)
		REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), WEAPONTYPE_BRIEFCASE)
	ENDIF
	
	SET_START_CHECKPOINT_AS_FINAL() // tells replay controller to display "skip mission" for shitskips
	
	IF IS_STRING_NULL_OR_EMPTY(g_JHP2A_string_WayPoint)
		SCRIPT_ASSERT("Route global not set")
	ELSE
		CPRINTLN(debug_mission, "Route received from trigger scene global: ", g_JHP2A_string_WayPoint)
	ENDIF
	
	sbi_SweatshopBin = ADD_SCENARIO_BLOCKING_AREA(<<713.7754, -996.4443, 22.3085>>, <<715.7624, -991.7067, 25.6214>>)

	WHILE NOT HAS_ADDITIONAL_TEXT_LOADED(MISSION_TEXT_SLOT)
		WAIT(0)
	ENDWHILE
	
	g_bLaptopMissionSuppressed = TRUE
ENDPROC

//PURPOSE: Progresses to the specified stage and carries out and resets
PROC Stage_Jump_To(MISSION_STAGE_FLAGS eNewStage)
	IF eNewStage = msf_num_mission_stages
		SCRIPT_ASSERT("MSF_NUM_MISSION_STAGES is NOT a stage, used only to monitor the number of stages")
	ELSE
		i_missionSubstage 	= 0
		b_StageSetup		= FALSE
		i_missionStage 		= enum_to_int(eNewStage)
	ENDIF
ENDPROC

//PURPOSE: Processes any change from one stage to another that is not part of the mission flow.
PROC Stage_Skip_Update()
	// a skip has been made
	IF b_DoSkip = TRUE
	
		IF NOT IS_SCREEN_FADED_OUT()
			IF NOT IS_SCREEN_FADING_OUT()
				DO_SCREEN_FADE_OUT(1000)
			ENDIF
		ELSE
			IF IS_CUTSCENE_ACTIVE()
				STOP_CUTSCENE()
				REMOVE_CUTSCENE()
			ENDIF
		
			PRINT_DEBUG("[STAGE SKIP]: Starting Skip", 1)
			i_missionStage = i_SkipToStage
					
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			CLEAR_PRINTS()
			CLEAR_HELP()
			
			Mission_Cleanup(TRUE)
			IF IS_CUTSCENE_ACTIVE()
				REMOVE_CUTSCENE()
			ENDIF
			
		// Warping
		// -----------------------------------------------
		
			IF NOT IS_REPLAY_BEING_SET_UP()
			
				CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_SKIP() Performing NON-REPLAY skip warping")
				
				VECTOR vWarpCoord
				FLOAT fWarpHeading
				GET_SKIP_STAGE_COORD_AND_HEADING(i_missionStage, vWarpCoord, fWarpHeading)
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), vWarpCoord)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), fWarpHeading)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
				Load_Asset_NewLoadScene_Sphere(s_AssetData, vWarpCoord, 50.0)
				
				CPRINTLN(DEBUG_MISSION, "MISSION_STAGE_SKIP() NON-REPLAY skip warp coord ", vWarpCoord, " warp heading ", fWarpHeading)
				
			ENDIF
			
		// Asset loading
		// -----------------------------------------------	
			Start_Skip_Streaming(s_AssetData)
		
			// Asset loading
			SWITCH int_to_enum(MISSION_STAGE_FLAGS, i_missionStage)
				CASE msf_gas_steal
					Load_Asset_Model(s_AssetData, mod_Driver)
					Load_Asset_Model(s_AssetData, mod_Van)
					Load_Asset_Model(s_AssetData, mod_Case)
					Load_Asset_Waypoint(s_AssetData, g_JHP2A_string_WayPoint)
				BREAK
				CASE msf_mission_passed
					Load_Asset_Model(s_AssetData, BURRITO2)
				BREAK
			ENDSWITCH
			
			WHILE NOT Update_Skip_Streaming(s_AssetData)
				WAIT(0)
				Mission_Entity_Death_Checks()
			ENDWHILE
			
			// Set up the mission for that current stage
			SWITCH int_to_enum(MISSION_STAGE_FLAGS, i_missionStage)
				CASE msf_gas_steal
				
					WHILE NOT PREPARE_INITIAL_SCENE(FALSE)
						wait(0)
					ENDWHILE

					// Create player vehicle
					IF ARE_STRINGS_EQUAL(g_JHP2A_string_WayPoint, "jhp2a_main")
						WHILE NOT Create_Player_Mission_Vehicle(s_VehPlayer.id, <<1395.8510, -1069.3057, 52.4779>>, 118.1591, TRUE)
							WAIT(0)
							CLEAR_AREA_OF_VEHICLES(<<1395.8510, -1069.3057, 52.4779>>, 20.0)
						ENDWHILE
					ELSE
						WHILE NOT Create_Player_Mission_Vehicle(s_VehPlayer.id, <<1256.9131, 556.8416, 79.7013>>, 134.0793, TRUE)
							WAIT(0)
							CLEAR_AREA_OF_VEHICLES(<<1256.9131, 556.8416, 79.7013>>, 20.0)
						ENDWHILE
					ENDIF
					
					SET_VEHICLE_COLOUR_COMBINATION(s_VehPlayer.id, 0)
					SET_VEHICLE_EXTRA(s_VehPlayer.id, 1, FALSE)
					SET_VEHICLE_EXTRA(s_VehPlayer.id, 2, TRUE)
					SET_VEHICLE_ON_GROUND_PROPERLY(s_VehPlayer.id)
					SET_VEHICLE_ENGINE_ON(s_VehPlayer.id, TRUE, TRUE)
					//SET_ENTITY_HEALTH(s_VehPlayer.id, 3000)
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP(s_VehPlayer.id, VS_DRIVER)
					ELSE
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), s_VehPlayer.id, VS_DRIVER)
					ENDIF
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
				BREAK
				CASE msf_mission_passed

					IF NOT GET_MISSION_COMPLETE_STATE(SP_HEIST_JEWELRY_PREP_1A)
					
						s_VehVan.id = CREATE_VEHICLE(BOXVILLE3, <<693.7250, -1006.3015, 21.8355>>, 359.8840)
						SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(s_VehVan.id, FALSE)
						SET_VEHICLE_ON_GROUND_PROPERLY(s_VehVan.id)

					ENDIF
					
					IF IS_REPLAY_BEING_SET_UP()
						END_REPLAY_SETUP()
					ELSE
						FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
					ENDIF
					
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<692.0670, -1004.8117, 21.9059>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 359.5735)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING()
					SET_GAMEPLAY_CAM_RELATIVE_PITCH()
					
				BREAK
			ENDSWITCH
			
			IF IS_SCREEN_FADED_OUT()
			OR (NOT IS_SCREEN_FADING_IN())
				DO_SCREEN_FADE_IN(1000)
			ENDIF
			b_DoSkip = FALSE
			PRINT_DEBUG("[STAGE SKIP]: Skip complete", 1)
		ENDIF
#IF IS_DEBUG_BUILD
	// Check is a skip is being asked for, and dont allow skip during setup stage
	ELIF IS_SCREEN_FADED_IN()
		INT i_currently_selected
	
//		DONT_DO_J_SKIP(s_LocatesData)
		IF LAUNCH_MISSION_STAGE_MENU(s_ZMenuStruct, i_SkipToStage, i_currently_selected, FALSE, "Jewelry Heist Prep 2a", TRUE, TRUE)
			b_DoSkip = TRUE
		ELSE
			IF IS_KEYBOARD_KEY_PRESSED(KEY_S)			
				Mission_Passed()
			ENDIF
			IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
				Mission_Failed(mff_debug_forced)
			ENDIF
		ENDIF		
#ENDIF
	ENDIF
	
ENDPROC

BOOL bWasInVehiclePrevFrame

//PURPOSE: STEAL THE VAN OR THE CARGO
FUNC BOOL STAGE_STEAL_GRENADES()

	VECTOR 			vGarmentFactoryParkCoord 			= <<692.8256, -1012.5445, 21.7220>>
	VECTOR			vGarmentFactoryFootCoord			= <<707.15, -959.66, 29.40>>
	
	INT i

	IF NOT b_StageSetup
		
		IF IS_THIS_ENTITY_ALIVE(s_VehVan.id)
			IF GET_BLIP_FROM_ENTITY(s_VehVan.id) != NULL
				s_VehVan.blip = GET_BLIP_FROM_ENTITY(s_VehVan.id)
			ELSE
				s_VehVan.blip = CREATE_BLIP_FOR_VEHICLE(s_VehVan.id, FALSE)
			ENDIF
			PRINT_NOW("JHP2A_STEAL", DEFAULT_GOD_TEXT_TIME, 1)
			PRINT_HELP("JHP2A_HLP2", 15000)
		ELSE
			IF GET_BLIP_FROM_ENTITY(s_VehVan.id) != NULL
				SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(TRUE)
					BLIP_INDEX blip = GET_BLIP_FROM_ENTITY(s_VehVan.id)
					REMOVE_BLIP(blip)
				SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(FALSE)
			ENDIF
		ENDIF
		
		TRIGGER_MUSIC_EVENT("JHP2A_START")
		
		SET_MISSION_START_VEHICLE_AS_VEHICLE_GEN(<<0,0,0>>, 0, TRUE, GET_CURRENT_PLAYER_PED_ENUM())

		REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)

		bDisplayedStealBZCaseObjective	= FALSE
		b_StageSetup 					= TRUE
		bWasInVehiclePrevFrame			= TRUE
		i_missionSubstage 				= 1
	ENDIF
	
	IF b_StageSetup
	
		UPDATE_VAN_STATE()
	
		SWITCH i_missionSubstage
			CASE 1	
			
				// Manage hint cam
				IF eVanState = VANSTATE_DRIVABLE_WITH_CARGO
				AND DOES_ENTITY_EXIST(s_VehVan.id) AND IS_VEHICLE_DRIVEABLE(s_VehVan.id)
				AND DOES_ENTITY_EXIST(s_PedDriver.id) AND NOT IS_PED_INJURED(s_PedDriver.id)
				AND IS_PED_IN_VEHICLE(s_PedDriver.id, s_VehVan.id)
				AND IS_MISSION_EVENT_RUNNING(s_Events[mef_driver_manager].sData)
					CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, s_VehVan.id)		
				ELSE
					KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
				ENDIF
			
				// Cargo has fallen out the back of the van
				IF eVanState = VANSTATE_CARGO_FALLEN_OUT
				OR eVanState = VANSTATE_CARGO_LOOSE_INSIDE
				
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)

					CLEAR_MISSION_LOCATE_STUFF(s_LocatesData)
					IF IS_HELP_MESSAGE_BEING_DISPLAYED()
					AND IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("JHP2A_HLP2")
						CLEAR_HELP()	// clear help text instructing player to blow up or damage doors
					ENDIF
					IF NOT bDisplayedStealBZCaseObjective
						PRINT_NOW("JHP2A_TAKEBZ", DEFAULT_GOD_TEXT_TIME, 1)	
					ENDIF
					i_missionSubstage = 2
					
				ELSE

					// Tow truck steal
					IF DOES_ENTITY_EXIST(s_VehTowTruck.id)
					AND IS_VEHICLE_DRIVEABLE(s_VehTowTruck.id)
					AND IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(s_VehTowTruck.id, s_VehVan.id)
					
						IF DOES_BLIP_EXIST(s_VehVan.blip)
							
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							
							SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(TRUE)
								REMOVE_BLIP(s_VehVan.blip)
							SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(FALSE)
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(JH2AP_FACTORY_TIME)
						ENDIF
						
						IS_PLAYER_AT_LOCATION_IN_VEHICLE(s_LocatesData, vGarmentFactoryParkCoord, <<0.1, 0.1, 0.1>>,
							TRUE, s_VehTowTruck.id, "JHP2A_RTNVAN", "", "", TRUE)
							
						IF IS_ENTITY_IN_ANGLED_AREA(s_VehVan.id, <<692.914307,-1003.555786,21.508389>>, <<692.651123,-1021.604126,26.206753>>, 9.750000)
							BRING_VEHICLE_TO_HALT(s_VehTowTruck.id, 5, -1)
							i_missionSubstage = 101
						ENDIF
					
					
					// Van is not drivable OR van doors are open, tell player to go to the back and reteive the cargo
					ELIF eVanState = VANSTATE_NOT_DRIVABLE_WITH_CARGO
					
						CLEAR_MISSION_LOCATE_STUFF(s_LocatesData)
					
						IF NOT DOES_BLIP_EXIST(obj_Grenades[0].blip)
							
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							
							obj_Grenades[0].blip = CREATE_BLIP_FOR_OBJECT(obj_Grenades[0].id)
							
							PRINT_NOW("JHP2A_TAKEBZ", DEFAULT_GOD_TEXT_TIME, 1)
							bDisplayedStealBZCaseObjective = TRUE
							
							SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(s_VehVan.id, FALSE)	// player can't get back (shouldn't be able to any way as the van is fucked at this point, just a precaution)
							
						ENDIF
					
					// Van is drivable with the cargo in the back
					ELIF eVanState = VANSTATE_DRIVABLE_WITH_CARGO
											
						IF DOES_BLIP_EXIST(s_VehVan.blip)	
						
							REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						
							SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(TRUE)
								REMOVE_BLIP(s_VehVan.blip)
							SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(FALSE)
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(JH2AP_FACTORY_TIME)
						ENDIF
						
						IF IS_PLAYER_AT_ANGLED_AREA_IN_VEHICLE(s_LocatesData, vGarmentFactoryParkCoord, <<692.914307,-1003.555786,21.508389>>, <<692.651123,-1021.604126,26.206753>>, 9.750000, 
								TRUE, s_VehVan.id, "JHP2A_RTNVAN", "", "", IS_PED_IN_VEHICLE(PLAYER_PED_ID(), s_VehVan.id))
							i_missionSubstage = 101
						ENDIF
						
						IF DOES_BLIP_EXIST(s_LocatesData.vehicleBlip)
						AND DOES_BLIP_HAVE_GPS_ROUTE(s_LocatesData.vehicleBlip)
							SET_BLIP_ROUTE(s_LocatesData.vehicleBlip, FALSE)
						ENDIF
	
					ENDIF
					
				ENDIF
			BREAK
			CASE 101
			
				VEHICLE_INDEX vehTemp
				
				IF DOES_ENTITY_EXIST(s_VehVan.id) AND IS_PED_IN_VEHICLE(PLAYER_PED_ID(), s_VehVan.id)
					vehTemp = s_VehVan.id
				ELIF DOES_ENTITY_EXIST(s_VehTowTruck.id) AND IS_VEHICLE_ATTACHED_TO_TOW_TRUCK(s_VehTowTruck.id, s_VehVan.id)
					vehTemp = s_VehTowTruck.id
				ENDIF

				IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehTemp)
					IF GET_MISSION_COMPLETE_STATE(SP_HEIST_JEWELRY_PREP_1A)
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						i_missionSubstage = 1000
					ELSE
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
						i_missionSubstage++
					ENDIF
				ENDIF
			BREAK
			CASE 102
				IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
				
					CLEAR_PRINTS()
					
					// Player can no longer interact with the van.
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(s_VehVan.id, FALSE)
					SET_VEHICLE_DISABLE_TOWING(s_VehVan.id, TRUE)
					
					// Use this to reset the tow truck so it can move about
					IF DOES_ENTITY_EXIST(s_VehTowTruck.id)
						BRING_VEHICLE_TO_HALT(s_VehTowTruck.id, 5, 1)
					ENDIF
					
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
					
					IF DOES_ENTITY_EXIST(obj_Grenades[0].id)	DELETE_OBJECT(obj_Grenades[0].id)	 ENDIF
					IF DOES_ENTITY_EXIST(obj_Grenades[1].id)	DELETE_OBJECT(obj_Grenades[1].id)	 ENDIF
					IF DOES_ENTITY_EXIST(obj_Grenades[2].id)	DELETE_OBJECT(obj_Grenades[2].id)	 ENDIF					
					
					RETURN TRUE
					
				ENDIF
			BREAK
			CASE 2
			
				IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_BRIEFCASE)
				
					// Make sure the cases are blipped
					REPEAT COUNT_OF(obj_Grenades) i
						IF DOES_ENTITY_EXIST(obj_Grenades[i].id)
						AND NOT IS_ENTITY_ATTACHED(obj_Grenades[i].id)
						
							IF DOES_BLIP_EXIST(s_VehVan.blip)		
								SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(TRUE)
									REMOVE_BLIP(s_VehVan.blip)		
								SET_THIS_SCRIPT_CAN_REMOVE_BLIPS_CREATED_BY_ANY_SCRIPT(FALSE)
							ENDIF
						
							IF NOT DOES_BLIP_EXIST(obj_Grenades[i].blip)
								obj_Grenades[i].blip = CREATE_BLIP_FOR_OBJECT(obj_Grenades[i].id)
							ENDIF
				
						ENDIF
					ENDREPEAT
				
					REPEAT COUNT_OF(obj_Grenades) i
						IF DOES_ENTITY_EXIST(obj_Grenades[i].id)
						AND NOT IS_ENTITY_ATTACHED(obj_Grenades[i].id)
							IF NOT HAS_PED_GOT_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_BRIEFCASE)
								BOOL bIsInSideVan
								bIsInSideVan = IS_CASE_INSIDE_VAN(obj_Grenades[i].id)
								IF (bIsInSideVan AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),
															GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_VehVan.id, <<0,-3.0,-0.5>>),
															GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(s_VehVan.id, <<0,-4.5,1.0>>), 1.68, FALSE, TRUE, TM_ON_FOOT))
								OR (NOT bIsInSideVan AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(obj_Grenades[i].id), <<0.75,0.75,LOCATE_SIZE_ON_FOOT_ONLY>>, FALSE, FALSE, TM_ON_FOOT))
									//place briefcase into hand
									GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_BRIEFCASE, 1, FALSE, FALSE)
									DELETE_OBJECT(obj_Grenades[i].id)
									CLEAR_PRINTS()
									
									// play pickup sfx
									PLAY_SOUND_FRONTEND(-1, "PICKUP_WEAPON_SMOKEGRENADE","HUD_FRONTEND_WEAPONS_PICKUPS_SOUNDSET")
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT

				ELSE
				
					INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(JH2AP_FACTORY_TIME)
				
					// Remove any blips on the cases
					REPEAT COUNT_OF(obj_Grenades) i
						IF DOES_BLIP_EXIST(obj_Grenades[i].blip)
							REMOVE_BLIP(obj_Grenades[i].blip)
						ENDIF
					ENDREPEAT
					
					IS_PLAYER_AT_LOCATION_ON_FOOT(s_LocatesData, vGarmentFactoryFootCoord, <<0.1, 0.1, 0.1>>, FALSE, "JHP2A_RTNBZ", TRUE)
					
					WEAPON_TYPE weapPlayer
					BOOL bInDropOffRoom, bNearWarehouse
					
					GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), weapPlayer)
					
					// is near enough to the warehouse
					IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<722.984924,-965.747253,32.296906>>,<<50.000000,40.000000,11.437500>>)
						bNearWarehouse = TRUE
					ENDIF
				
					// Check to see if the player is at the end with the case
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<709.743225,-960.336670,29.395329>>, <<703.647766,-960.329285,33.651192>>, 4.125000)
						bInDropOffRoom = TRUE
					ENDIF
					
					IF bInDropOffRoom
					OR (bNearWarehouse AND NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE) AND bWasInVehiclePrevFrame)
					
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
						DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
						
						IF NOT IS_CELLPHONE_DISABLED()
							DISABLE_CELLPHONE(TRUE)
						ENDIF
						
						IF NOT IS_PED_RUNNING_RAGDOLL_TASK(PLAYER_PED_ID())
							
							IF NOT IS_PED_IN_COVER(PLAYER_PED_ID())
							AND NOT IS_PHONE_ONSCREEN()
								IF weapPlayer != WEAPONTYPE_BRIEFCASE
									SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(),WEAPONTYPE_BRIEFCASE, TRUE)
								ENDIF
							ENDIF
							
						ENDIF
					
					ENDIF
					
					IF bInDropOffRoom	
					
						IF weapPlayer = WEAPONTYPE_BRIEFCASE
							IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("JHP2A_HLP1")
								PRINT_HELP_FOREVER("JHP2A_HLP1")
							ENDIF
						ENDIF
					
						IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
						AND weapPlayer = WEAPONTYPE_BRIEFCASE
						
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("JHP2A_HLP1")
								CLEAR_HELP()
							ENDIF
							
							OBJECT_INDEX objCase 
							objCase = GET_WEAPON_OBJECT_FROM_PED(PLAYER_PED_ID())
							ACTIVATE_PHYSICS(objCase)
							
							REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							
							PLAY_SOUND_FROM_ENTITY(-1, "Drop_Case", objCase, "JWL_PREP_2A_SOUNDS")
							
							SET_OBJECT_AS_NO_LONGER_NEEDED(objCase)

							SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
							REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), WEAPONTYPE_BRIEFCASE)
						
							INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
							INFORM_STAT_SYSTEM_OF_BOOL_STAT_HAPPENED(JHP2A_LOOSE_CARGO)
							
							CLEAR_MISSION_LOCATE_STUFF(s_LocatesData)
							
							IF GET_MISSION_COMPLETE_STATE(SP_HEIST_JEWELRY_PREP_1A)
								IF IS_CELLPHONE_DISABLED()
									DISABLE_CELLPHONE(FALSE)
								ENDIF
								i_missionSubstage = 2000
							ELSE
								RETURN TRUE
							ENDIF
							
						ENDIF
					
					ELSE
						IF IS_CELLPHONE_DISABLED()
							DISABLE_CELLPHONE(FALSE)
						ENDIF
					
						IF IS_HELP_MESSAGE_BEING_DISPLAYED()
							IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("JHP2A_HLP1")
								CLEAR_HELP()
							ENDIF
						ENDIF
					ENDIF
					
					bWasInVehiclePrevFrame = IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
			
				ENDIF

			BREAK
			
			// Dropped off van and need to make the phone call
			CASE 1000
				TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(s_VehVan.id, FALSE)
				SET_VEHICLE_DISABLE_TOWING(s_VehVan.id, TRUE)
				
				BLOCK_PLAYER_FOR_LEAD_IN(TRUE)
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				
				IF IS_SAFE_TO_START_CONVERSATION()
					IF PLAYER_CALL_CHAR_CELLPHONE(s_Convo, CHAR_LESTER, "JHFAUD", "JHF_BZD2", CONV_PRIORITY_HIGH) 
						REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						i_missionSubstage++
					ENDIF
				ENDIF
			BREAK
			CASE 1001
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				IF IS_CALLING_ANY_CONTACT()	
					i_missionSubstage++
				ENDIF
			BREAk
			CASE 1002
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				IF NOT IS_CALLING_ANY_CONTACT()	
					CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN()
					RETURN TRUE
				ENDIF
			BREAk
			
			// Dropped off cases on foot and need to make the phone call
			CASE 2000
				REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
				SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), TRUE)
				g_bLaptopMissionSuppressed = TRUE
				BLOCK_PLAYER_FOR_LEAD_IN(TRUE)
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				i_missionSubstage++
			BREAK
			CASE 2001
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				IF IS_SAFE_TO_START_CONVERSATION()
					IF PLAYER_CALL_CHAR_CELLPHONE(s_Convo, CHAR_LESTER, "JHFAUD", "JHF_BZD2", CONV_PRIORITY_HIGH) 
						i_missionSubstage++
					ENDIF
				ENDIF
			BREAK
			CASE 2002
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				IF IS_CALLING_ANY_CONTACT()	
					i_missionSubstage++
				ENDIF
			BREAK
			CASE 2003
				UPDATE_BLOCKED_PLAYER_FOR_LEAD_IN(TRUE)
				IF NOT IS_CALLING_ANY_CONTACT()
					CLEAR_BLOCKED_PLAYER_FOR_LEAD_IN()
					RETURN TRUE
				ENDIF
			BREAK

		ENDSWITCH
	
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

//PURPOSE: Processes the mission flow
PROC Stage_Flow() 
	#IF IS_DEBUG_BUILD
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S)
			Mission_Passed()
		ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F)
			Mission_Failed(mff_debug_forced)
		ENDIF
		
		IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_HOME)
			b_DisplayDebug = !b_DisplayDebug
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(b_DisplayDebug)
		ENDIF
	#ENDIF
	
	SWITCH int_to_enum(MISSION_STAGE_FLAGS, i_missionStage)
		
		CASE msf_gas_steal
			IF STAGE_STEAL_GRENADES()		Stage_Jump_To(msf_mission_passed)	ENDIF
		BREAK
		
		CASE msf_mission_passed			
			Mission_Passed()					
		BREAK
		
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		IF b_DisplayDebug
			INT iDebugTextCounter
			TEXT_LABEL_63 strDebug
			strDebug = "i_NumBulletHitsOnDoorLock: "
			strDebug += i_NumBulletHitsOnDoorLock
			DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.3 + (iDebugTextCounter*0.02), 0.0>>, 0, 0, 0, 255)
			iDebugTextCounter++
			
			strDebug = "i_BulletHitTimer: "
			strDebug += i_BulletHitTimer
			DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.3 + (iDebugTextCounter*0.02), 0.0>>, 0, 0, 0, 255)
			iDebugTextCounter++
			
			IF DOES_ENTITY_EXIST(obj_Lock)
				strDebug = "Lock Object Health: "
				strDebug += GET_ENTITY_HEALTH(obj_Lock)
				DRAW_DEBUG_TEXT_2D(strDebug, <<0.05, 0.3 + (iDebugTextCounter*0.02), 0.0>>, 0, 0, 0, 255)
				iDebugTextCounter++
			ENDIF
			
		ENDIF
	#ENDIF
ENDPROC

SCRIPT

	IF HAS_FORCE_CLEANUP_OCCURRED()	
		Mission_Failed(mff_default)
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	// Do everything to prepare the mission
	Mission_Setup()
	
	WHILE (TRUE)
	
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_JewelStoreJobPrep2A")
	
		IF b_MissionFailed
			Mission_Fail_Update()
		ENDIF
		
		Process_Streaming(s_AssetData)		// Deals with loading any assets and keeps track of what has been loaded
		Stage_Skip_Update()					// processes any stage skipping that is required
		Mission_Entity_Death_Checks()		// Check if any of the mission critical entities (peds, vehicles, objects) are dead and fail accordingly
		IF NOT b_DoSkip
			Mission_Fail_Checks()			// Mission scenario checks; fails, disables running at certain points, etc
			UPDATE_MISSION_EVENTS(s_Events)
			Stage_Flow()						// process the mission flow
		ENDIF
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.8)
		
		WAIT(0)
		
	ENDWHILE

ENDSCRIPT
