//╔═════════════════════════════════════════════════════════════════════════════╗
//║																				║
//║ 				Author:  Ross Wallace				Date: 22/02/2010		║
//║					Prev:	David Gentles										║
//║																				║
//╠═════════════════════════════════════════════════════════════════════════════╣
//║ 																			║
//║ 				Jewelry Heist - Heist										║
//║																				║
//║ 		The crew perform the jewelry store heist							║
//║ 																			║
//║ 		*	Franklin drives to the jewelry store, and climbs onto the roof	║
//║ 		*	He will drop gas canisters into the air vents					║
//║ 		*	Michael and two goons will don gas masks and enter the store	║
//║ 		*	They will smash and grabs the loot, and run outside				║
//║ 		*	Everyone gets on bikes and drive to the service tunnel			║
//║ 		*	They drive through the service tunnel, storm drains and highway	║
//║ 		*	They drive the bikes into the back of a moving truck			║
//║ 																			║
//║ 		Gameplay elements introduced: Climbing, driving onto moving truck	║
//║ 		Vehicle Introduced: Benson, Bike			 						║
//║ 		Location: Jewelry store, service tunnel, storm drain				║
//║ 		Notes:  															║
//║ 																			║
//╚═════════════════════════════════════════════════════════════════════════════╝

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

//═════════════════════════════════╡ COMMAND HEADERS ╞═══════════════════════════════════

USING "rage_builtins.sch"
USING "script_heist.sch"

USING "selector_public.sch"
USING "dialogue_public.sch"
USING "select_mission_stage.sch"
USING "locates_public.sch"
USING "replay_public.sch"
USING "chase_hint_cam.sch"
USING "commands_cutscene.sch"
USING "cutscene_public.sch"
USING "script_ped.sch"
USING "shared_hud_displays.sch"
USING "flow_help_public.sch"
USING "CompletionPercentage_public.sch"
USING "taxi_functions.sch"
USING "timeLapse.sch"
USING "emergency_call.sch"
USING "heist_end_screen.sch"
USING "spline_cam_edit.sch"
USING "commands_event.sch"
USING "achievement_public.sch"
USING "replay_private.sch"
USING "player_ped_scenes.sch"
USING "cheat_controller_public.sch"
USING "commands_recording.sch"
USING "push_in_public.sch"

CONST_FLOAT FBIKE_BASE_SPEED  20.0
CONST_INT CURRENT_TAKE_REPLAY_VALUE 	0	//Array positino for replay globals
CONST_INT GOT_BAG_REPLAY_VALUE 			1	//Array positino for replay globals
CONST_INT CASE_COUNT_REPLAY_VALUE 		2	//Array positino for replay globals

CONST_INT BAG_NOT_COLLECTED 	0
CONST_INT BAG_COLLECTED 		1

CONST_INT TARGET_TAKE_VALUE 				3300000
CONST_INT MIN_TAKE_VALUE_GIVEN_BAG_LOSS 	2741082

BOOL bIsReplayGoingOn
BOOL bIsAnimFinished
BOOL bAimingLineShouldPlay

BOOL bManagerPopOut = FALSE

INT iManagerDialogue
INT iTimeOfManagerPoppingOut
INT iBalaclavaStage

OBJECT_INDEX oiBuddysBag
BOOL bInitialVelocityGiven
VECTOR vFallingBikeVelocity
VECTOR vBatiRecOffset

VECTOR vTargetVehicleOffsetChase
FLOAT fSpeedMultiplier

VECTOR vTarpPos = <<210.750, -2006.800, 20.20>>
VECTOR vTarpRot = <<0.0, 0.0, 50.0>>

INT iLastEngineHealth

INT iStageLestersIdle

BOOL bHasChangedClothesMichael
BOOL bHasChangedClothesFranklin

//USING "traffic_default_values.sch" // this must be included before traffic.sch 
// total should not exceed 225
CONST_INT TOTAL_NUMBER_OF_TRAFFIC_CARS                      36
CONST_INT TOTAL_NUMBER_OF_PARKED_CARS                       17   
CONST_INT TOTAL_NUMBER_OF_SET_PIECE_CARS                    35    

// total should not exceed 12
CONST_INT MAX_NUMBER_OF_TRAFFIC_CARS_PLAYING_BACK           28 
CONST_INT MAX_NUMBER_OF_SET_PIECE_CARS_PLAYING_BACK         14                             

// this should be fine
CONST_INT MAX_NUMBER_OF_PARKED_CARS_PLAYING_BACK            16

USING "traffic.sch"

FLOAT fDesiredPlaybackSpeed
FLOAT fPLaybackSpeedNumerator = 26.0
FLOAT fPLaybackSpeedCPS
FLOAT fminPlaybackSpeed = 0.5
FLOAT fMaxPlaybackSpeed = 1.75

WEAPON_TYPE mnMainWeaponType

BOOL SetPieceCarCopCreated[TOTAL_NUMBER_OF_SET_PIECE_CARS]

STRUCT SET_PIECE_COP
	
	VEHICLE_INDEX thisCar
	PED_INDEX thisDriver
	PED_INDEX thisPassenger
	BLIP_INDEX blipDriver
	BLIP_INDEX blipPassenger
	FLOAT fSpeed = 20.0
	 
ENDSTRUCT

BOOL bHasSmokeGrenades
INT iTimeOfThrowing
INT iGetLostStage
INT iTimeOfGettingLost
INT iRiverDialogue
INT iRiverDialogueSide
INT iRiverDialogueSideMoreCops
INT iCurrentBuddyCase
INT iBuddyLootStage

INT iBlipCount

//BOOL bBeenToldToCross = FALSE
VEHICLE_INDEX heliCrashTrain 			
INT iHeliCrashTrainStage

PED_INDEX pedTrafficWarden

BOOL bShowMichaelHelp
BOOL bEndOfLARiverReached

INTERIOR_INSTANCE_INDEX sweatShop

BLIP_INDEX blipAirVent

ENUM eManagerStage
	MANAGER_REQUEST_ASSETS,							//0
	MANAGER_WAIT_FOR_ASSETS,						//1
	MANAGER_WAIT_TO_APPEAR,							//2
	MANAGER_APPEARS,								//3
	MANAGER_WAIT_FOR_INTRO_TO_FINISH,				//4
	MANAGER_WAIT_FOR_INTRO_TO_FINISH_2,				//5
	MANAGER_WAIT_FOR_PLAYER_OR_GUNMAN_TO_SUBDUE,	//6
	MANAGER_WAIT_FOR_FAIL,							//7
	MANAGER_WAIT_FOR_FAIL_2,						//8
	MANAGER_LOOPS,									//9
	MANAGER_RUNS_BACK_INTO_ROOM,					//10
	MANAGER_KNEELS									//11
		
ENDENUM

eManagerStage eRandomManagerStage = MANAGER_REQUEST_ASSETS

//═════════════════════════════════╡ SOUND ╞═══════════════════════════════════
ENUM SOUND_LIST
	SND_ALARM_BELL									= 0,
	SND_GAS_GRENADE_PIN,
	SND_GRENADE_LAND_AND_EXPLODE,
	SND_POLICE_SIRENS,
	SND_RAYFIRE_CRASH,
	SND_ROOFTOP_AIRCON,
	SND_SMASH_CABINET,
	SND_SMASH_CABINET_NPC,
	SND_THROW_BAG,
	SND_CATCH_BAG,
	SND_GAS_ESCAPE,
	SND_GAS_MASK,
	
	SND_LIST_SIZE
ENDENUM

INT iTimeOfLastWhimper
INT iUpdateWhimpers
INT iRandomPedToWhimper


//═════════════════════════════════╡ ENUMS ╞═══════════════════════════════════

//═════════╡ MISSION CONTROL ╞═════════
ENUM MISSION_FAIL
	FAIL_GENERIC 									= 0,
	FAIL_SHOP_CLOSED,
	FAIL_ABANDON,
	FAIL_KILL_MICHAEL,
	FAIL_KILL_TREVOR,
	FAIL_KILL_CREW,
	FAIL_KILL_HACKER,
	FAIL_KILL_GUNMAN,
	FAIL_KILL_DRIVER,
	FAIL_DESTROY_BIKE,
	FAIL_DESTROY_VAN,
	FAIL_DESTROY_CAR,
	FAIL_DESTROY_TRUCK,
	FAIL_LOSE_CHASE,
	FAIL_LOSE_BIKE,
	FAIL_LOSE_MICHAEL,
	FAIL_LOSE_FRANKLIN,	
	FAIL_LOSE_CREW,
	FAIL_KILL_FRANKLIN,
	FAIL_CALL_POLICE,
	FAIL_SECURITY_ESCAPED,
	FAIL_DIDNT_STEAL_ENOUGH_DIAMONDS,
	FAIL_BLEW_HEIST,
	FAIL_BLEW_ID,
	FAIL_GAS,
	FAIL_NO_GAS,
	FAIL_ARRESTED
	
ENDENUM
MISSION_FAIL eMissionFail

ENUM MISSION_STAGE
	STAGE_INIT_MISSION 								= 0,
	STAGE_TIME_LAPSE,								//1
	STAGE_CUTSCENE_BEGIN,							//2
	STAGE_DRIVE_TO_STORE,							//3
	STAGE_ROOF,										//4
	STAGE_CUTSCENE_GAS_STORE,						//5
	STAGE_CUTSCENE_AGGRESSIVE_ARRIVE,				//6
	STAGE_GRAB_LOOT,								//7
	STAGE_CUTSCENE_LEAVE_STORE,						//8
	STAGE_BIKE_CHASE,								//9
	STAGE_BIKE_CHASE_START_OF_TUNNEL,				//10
	STAGE_BIKE_CHASE_END_OF_TUNNEL,					//11
	STAGE_TRUCK_FIGHT,								//12
	STAGE_CUTSCENE_RAMP_DOWN,						//13
	STAGE_RETURN_TO_BASE,							//14	
	PASSED_MISSION,									//15
	STAGE_FAIL_CUTSCENE,							//16
		
	UBER_RECORD,
	UBER_PLAYBACK,
	STAGE_DEBUG_STUFF
	
ENDENUM
CONST_INT MAX_SKIP_MENU_LENGTH 						17

TEXT_LABEL_15 strCrewMemberBest
TEXT_LABEL_15 strCrewMemberWorst

INT iControlBirds

STRUCT BIRD_DATA
	PED_INDEX ped 
	INT i_event
	INT i_timer
	VECTOR v_dir
ENDSTRUCT

BIRD_DATA s_birds[4]
BOOL bFranklinSeesSomebirds

//═════════════════════════════════╡ CONSTANTS ╞═══════════════════════════════════


//═════════╡ VALUES ╞═════════

CONST_INT NUMBER_OF_STORE_PEDS						5

CONST_INT iNoOfCops									6
CONST_INT iNoOfStormCops							4
//CONST_INT iTimeLimitC								60000

CONST_FLOAT fBagThrowSpeed							0.3
CONST_FLOAT fDoorAngleChangeSpeed					0.001
CONST_FLOAT fRFCheckRadius							1.0

CONST_FLOAT fPoliceWidth							0.95
CONST_FLOAT fPoliceFront							2.45
CONST_FLOAT fPoliceBack								2.70
CONST_FLOAT fPoliceHeight							0.80

CONST_FLOAT fTruckWidth								1.45
CONST_FLOAT fTruckFront 							4.00
CONST_FLOAT fTruckBack	 							5.30
CONST_FLOAT fTruckHeight							1.30

CONST_FLOAT fTrainInitialSpeed						5.00
CONST_FLOAT fTrainDeceleration						0.01
//FLOAT fTrainSpeed									= 0.0

//═════════╡ COORDINATES AND HEADINGS ╞═════════

VECTOR vMichaelSpawnCoords							= << 712.3005, -964.6440, 29.3953 >>//<< 716.9982, -965.8427, 29.4338 >>
CONST_FLOAT fMichaelSpawnHeading					253.4039  

VECTOR vMichaelWalkToCoords							= <<713.8555, -962.8375, 29.3956>>
CONST_FLOAT fMichaelWalkToHeading					198.7614

//VECTOR vMichaelShortWalkCoords						= << -630.0005, -245.9839, 37.2746 >>

VECTOR vFranklinSpawnCoords							= << 706.1772, -966.1973, 29.4598 >>
CONST_FLOAT fFranklinSpawnHeading					347.5504

VECTOR vLesterSpawnCoords							= <<706.4446, -963.6882, 29.4181>>
CONST_FLOAT fLesterSpawnHeading						213.4447 

VECTOR vBikeSpawnCoords[3]
FLOAT fBikeSpawnHeading[3]

VECTOR vBeginWalkToCoords[5]
FLOAT fBeginWalkToHeading[5]

VECTOR vCarSpawnCoords								= <<716.7158, -987.8787, 23.1366>>// <<715.9842, -986.3738, 23.1353>> //<< 717.0483, -986.2708, 23.1392 >>//<< 707.6824, -980.6934, 23.1063 >>
CONST_FLOAT fCarSpawnHeading						275.6314 //275.6308   //221.7486

VECTOR vTruckSpawnCoords							= << 726.6455, -980.4100, 23.1478 >>//= << 726.0810, -980.1198, 23.1392 >>
CONST_FLOAT fTruckSpawnHeading						269.3407

VECTOR vVanSpawnCoords								= << 726.4868, -987.6694, 23.1752 >>//, 275.6443//<< 726.4868, -987.6694, 23.1752 >>
CONST_FLOAT fVanSpawnHeading						275.8923

INT iArrivalAngle

VECTOR vBackOfStore									= << -579.8401, -279.8240, 34.1553 >> //<< -575.5158, -278.9760, 34.1733 >>
VECTOR vFrontOfStore								= << -668.3368, -228.1611, 36.1767 >>//<< -650.2766, -234.1784, 36.6031 >>
VECTOR vDriveAIPoint								= << -655.8611, -196.5738, 36.5397 >>
VECTOR vTruckDriveAIPoint							= <<-590.6932, -227.4862, 35.4301>>
VECTOR vVanDriveAIPoint								= <<-669.2445, -226.8173, 36.1665>>//<< -607.4092, -313.3467, 33.7383 >>

VECTOR vManageressPosition							= << -627.515, -231.408, 37.022 >>
VECTOR vManageressRotation							= << 0.000, 0.000, -21.000 >>

CONST_FLOAT fOutsideStore							128.6202

VECTOR vRoofRoute[6]

VECTOR vSmashParticleOffset							= <<0.0, 0.0, 0.0>>
VECTOR vSmashParticleRotation						= <<0.0, 0.0, 0.0>>

VECTOR vTopOfStairs									= << 717.1673, -965.0396, 29.4338 >>

VECTOR vJewelryExitCoords							= << -634.1773, -239.6257, 37.1375 >>
VECTOR vJewelWalkCoords								= <<-573.4960, -290.6799, 34.1626>> //<<-602.9581, -293.3708, 35.7793>>

VECTOR vJewelryWatchCoords							= << -633.7017, -237.1157, 36.9940 >>
CONST_FLOAT fJewelryWatchHeading					130.4809

VECTOR vLeaveScenePos								= << -631.645, -237.565, 37.107 >>

VECTOR vCopTrapTrigger								= << 542.7192, -890.5296, 11.8885 >>

CONST_FLOAT fBaseHeading							89.8822

VECTOR vMichaelTruckCoords							= <<1090.2834, -245.7385, 56.7453>>
CONST_FLOAT fMichaelTruckHeading					147.6818
VECTOR vCrewTruckCoords								= << 659.3379, -775.9125, 22.5320 >>
CONST_FLOAT fCrewTruckHeading						261.2985

VECTOR vSecurityRunCoords1							= <<-657.5221, -224.0321, 36.7346>>
VECTOR vSecurityRunCoords2							= << -657.3683, -211.9329, 36.2962 >>

VECTOR vVehicleFightTruckSpawn
FLOAT fVehicleFightTruckSpawn
VECTOR vVehicleFightBikeSpawn[3]
FLOAT fVehicleFightBikeSpawn[3]

VECTOR vJewelDoorCoords = <<-631.96, -236.34, 38.21>>
VECTOR vJewelDoorRot = <<-0.00, 0.00, 125.98>>

#IF IS_DEBUG_BUILD
	FLOAT fWaypointRubberBandSpeed2 = 15.0
	FLOAT fHeliProgressLaRiver
#ENDIF

VECTOR vFightEndTarget = << 636.5427, -1846.3568, 8.8126 >> //<< 633.1052, -1875.5404, 8.2200 >> //<<0,0,0>>
FLOAT fFightEndTarget =  175.6148 //173.4810 //0.0

VECTOR vEndOfStormDrain = << 648.2646, -1843.4121, 8.1401 >>

VECTOR vSecuritySpawnCoords
FLOAT fSecuritySpawnHeading
VECTOR vStoreSpawnCoords[NUMBER_OF_STORE_PEDS]
FLOAT fStoreSpawnHeading[NUMBER_OF_STORE_PEDS]

FLOAT fChaseSpeed									= 0
FLOAT fOffsetAmount
FLOAT fCatchUpSpeed
FLOAT fRecordingTime

MODEL_NAMES model_bird = A_C_SEAGULL
STRING str_bird_anims = "creatures@gull@move"

INT iUnloadBikes
SEQUENCE_INDEX seqGetOnBike

OBJECT_INDEX oiACTarget


//═════════════════════════════════╡ STRUCTS ╞═══════════════════════════════════
STRUCT sJewelCase
	VECTOR vCasePos, vPlayerPos, vPlayerRot
	VECTOR vCameraPos, vCameraRot, vCameraPos2, vCameraRot2
	FLOAT fCameraFov, fCameraFov2
	BLIP_INDEX blipCase
	BOOL bSmashed = FALSE
	STRING sLootName
	INT iWorth
	STRING sSmashAnim
	INT iSmashedByBulletStage
	RAYFIRE_INDEX rfCaseSmash
ENDSTRUCT
CONST_INT iNoOfCases								 20
sJewelCase jcStore[iNoOfCases]

STRUCT sLootControl
	INT iTargetCase
	OBJECT_INDEX objGrab
ENDSTRUCT
sLootControl lcCrew

STRUCT sCrowdControl
	VECTOR vLastTarget, vNextTarget
	INT iTimeLastFrame, iTimer, iTimerLimit
ENDSTRUCT
sCrowdControl ccCrew
sCrowdControl ccCrew2

//═════════════════════════════════╡ VARIABLES ╞═══════════════════════════════════

//═════════╡ ENUMS ╞═════════
MISSION_STAGE eMissionStage 						= STAGE_INIT_MISSION
LOCATES_HEADER_DATA sLocatesData

//═════════╡ FLAGS ╞═════════
BOOL bIsStealthPath									= TRUE

BOOL bStageSetup									= FALSE
BOOL bCameraShotSetup								= FALSE
BOOL bHackerWarnsGuys
BOOL bMissionFailed									= FALSE
#IF IS_DEBUG_BUILD
BOOL JSkip											= FALSE
BOOL PSkip											= FALSE
#ENDIF

BOOL bRemindAbout2Million							= FALSE
BOOL bIsPlayerLooting								= FALSE
BOOL bIsRandomGuardEventNow							= FALSE
BOOL bIsRandomManagerEventNow						= FALSE
BOOL bIsRandomGuardEventDone						= FALSE
BOOL bIsRandomManagerEventDone						= FALSE
BOOL bHasCopTrapTriggered							= FALSE
//BOOL bStartedKOGas									= FALSE
BOOL bIsDoorLocked									= FALSE
BOOL bIsHackTimeUp									= FALSE
BOOL bIsAlarmRinging 								= FALSE
BOOL bHasPrintedCaseHelp							= FALSE
//BOOL bHasClearedLateCops							= FALSE
BOOL bLateCopsShooting								= FALSE
BOOL bPreloadNextStage								= FALSE

BOOL bAttemptToResetCases							= FALSE
BOOL bDrawCashScaleform								= FALSE
BOOL bAimedAtGuard									= FALSE
BOOL bHasMichaelBeenDeletedForChase					= FALSE

BOOL bCopsAtExitCreated								= FALSE

BOOL bInSufficientTake								= FALSE
BOOL bWaitedTooLong 								= FALSE

BOOL bCaughtUp 										= FALSE

CONST_INT NUMBER_OF_EVENT_FLAGS 25
BOOL bEventFlag[NUMBER_OF_EVENT_FLAGS]

#IF IS_DEBUG_BUILD
BOOl bRecording										= FALSE
INT iDebugMissionStage
TEXT_LABEL_63 debugPedName
#ENDIF

//═════════╡ VECTORS ╞═════════
VECTOR vDriveToPoint

//═════════╡ INTS ╞═════════

INT iNearestCase
//INT iTotalSmashedCases								= 0
INT iFarthestCase										= 19 //Just make ti different from first case so player and buddy dont go for same case
INT iPreloadRecordingsTimer							= 0

//INT iPotentialTake									= 0
INT iCurrentTake									= 0
INT ilostBag										= 0
INT iLootHelpCounter								= 0

//═════════╡ FLOAT ╞═════════

//═════════╡ COUNTERS ╞═════════
INT iCounter										= 0

//═════════╡ Stages ╞═════════
INT iStageSection									= 0
INT iLootStage										= 0
INT iRandomGuardStage								= 0

//═════════╡ Selector ╞═════════
//SELECTOR_CAM_STRUCT sCamDetails

//═════════╡ Crew Choice Consequences ╞═════════
ENUM eCrewNames 
	CREW_DRIVER_NEW = 0,
	CREW_GUNMAN_NEW,
	CREW_HACKER
ENDENUM
eCrewNames cnSmashGunman							= CREW_DRIVER_NEW
eCrewNames cnAltGunman								= CREW_GUNMAN_NEW

INT iTimeLimit										= 0
CONST_INT iTimeLimitA								90000
CONST_INT iTimeLimitB								65000
FLOAT fCrewSmashMultiplier							= 1.0
BOOL bGuardRuns										= FALSE
BOOL bWorstGunDies									= FALSE
BOOL bSawBitchCrashOut 								= FALSE

BOOL bTellguysToputLightsOn
BOOL bMusicEventStart = FALSE
BOOL bMusicEvent1 = FALSE
BOOL bMusicEvent2 = FALSE
//BOOL bMusicEvent3 = FALSE
BOOL bMusicEvent4 = FALSE
BOOL bMusicEvent5 = FALSE
BOOL bMusicEvent6 = FALSE
BOOL bMusicEvent7 = FALSE
//BOOL bMusicEvent8 = FALSE

INT iSwitchSoundEffect

//═════════╡ Selector ╞═════════

//═════════╡ PEDS ╞═════════
PED_INDEX pedCrew[4]
PED_INDEX pedSecurity
PED_INDEX pedStore[NUMBER_OF_STORE_PEDS]
BOOL bLineAboutKillingThisGuyPlayed[NUMBER_OF_STORE_PEDS]
PED_INDEX pedBackroom
PED_INDEX pedPolice[iNoOfCops]
PED_INDEX pedPolicePassenger[iNoOfCops]
PED_INDEX pedLester
PED_INDEX copAtExit1, copAtExit2, copAtExit3

PED_INDEX copOnLeft1, copOnLeft2, copOnLeft3
PED_INDEX copOnRight1, copOnRight2, copOnRight3

VEHICLE_INDEX burnedWreck1, burnedWreck2

OBJECT_INDEX oiLestersStick

//═════════╡ VEHICLES ╞═════════
VEHICLE_INDEX vehBike[4]
VEHICLE_INDEX vehCar
VEHICLE_INDEX vehPolice[iNoOfCops]
FLOAT fPoliceSpeed[iNoOfCops]
VEHICLE_INDEX vehTruck
BLIP_INDEX vehPoliceBlips[TOTAL_NUMBER_OF_SET_PIECE_CARS]

VEHICLE_INDEX vehCollision
VEHICLE_INDEX vehVan
VEHICLE_INDEX vehTrainDodge
VEHICLE_INDEX vehControl

FLOAT mainPlaybackSpeed = 1.0

//═════════╡ OBJECTS ╞═════════
OBJECT_INDEX objGrenade
OBJECT_INDEX objBag
OBJECT_INDEX objPickup
OBJECT_INDEX objPhone
OBJECT_INDEX objWeapon
OBJECT_INDEX objWeapon1
OBJECT_INDEX objWeapon2

OBJECT_INDEX oiStoreDoorMap
OBJECT_INDEX oiStoreDoorTemp
INT thisSceneID

//═════════╡ INTERIORS ╞═════════
INTERIOR_INSTANCE_INDEX interiorJewelStore
INT keyJewelStore										= 877820577

//═════════╡ COVER POINT ╞═════════
COVERPOINT_INDEX covCrew[2]

//═════════╡ SEQUENCES ╞═════════
SEQUENCE_INDEX seqSequence
SEQUENCE_INDEX seqLeave

SEQUENCE_INDEX seqDealWithManager

//═════════╡ WEAPONS ╞═════════

//═════════╡ PARTICLE ID ╞═════════
#IF IS_NEXTGEN_BUILD
PTFX_ID ptfxGas
#ENDIF

//═════════╡ Scene ID ╞═════════
INT sceneLeaveStore
INT sceneManageress

//═════════╡ BLIPS ╞═════════
BLIP_INDEX blipCrew[3]
BLIP_INDEX blipLocate
BLIP_INDEX blipBike
BLIP_INDEX blipPolice[iNoOfCops]
BLIP_INDEX blipPassenger[4]
BLIP_INDEX blipGuard
BLIP_INDEX blipTruck
BLIP_INDEX blipFlee

//═════════╡ GROUPS ╞═════════
REL_GROUP_HASH grpShop
REL_GROUP_HASH grpCop

//═════════╡ STRINGS ╞═════════
STRING sAudioBlock = "JHAUD"
STRING sTextBlock = "H3HEIST"

//═════════╡ STRINGS ╞═════════
RAYFIRE_INDEX rfCaseSmash
RAYFIRE_INDEX rfCaseAISmash
RAYFIRE_INDEX rfReset

//═════════╡ CASE SMASHING ╞═════════
STRING strAnimName
STRING strCamAnimName
MODEL_NAMES mnPickupProp
FLOAT fSmash										= 0.0
FLOAT fGrabStart1									= 0.0
FLOAT fGrabEnd1										= 0.0
FLOAT fGrabStart2									= 0.0
FLOAT fGrabEnd2										= 0.0

STRING strBuddyAnimName
MODEL_NAMES mnBuddyPickupProp
FLOAT fBuddySmash										= 0.0
FLOAT fBuddyGrabStart									= 0.0
FLOAT fBuddyGrabEnd										= 0.0

//═════════╡ DEBUG ╞═════════
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID widgetDebug
	
	INT iDebugChaseBikeRec								= 998 //Asset.recNoGoodBikeChase
	
	INT iColour1 = 0
	INT iColour2 = 0
	INT iExtra = 0
	BOOL bExtraOnOff = FALSE
	INT iExtraColour1 = 0
	INT iExtraColour2 = 0
	INT iLivery = 0
	
	//INT recNoMain = 650
	FLOAT posx = 0.0
	FLOAT posy = 0.0
	FLOAT posz = 0.0
	BOOL bTrainDirection = FALSE
	
	BOOL bIsSuperDebugEnabled
	
	TEXT_LABEL_15 debugName
		
	INT iReturnStage                                      								 // mission stage to jump to
	MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]      				// struct containing the debug menu 
	STRING sMissionName									= "Jewellery Heist"
	
	INT iRandomManagerStage
	
	//CAM_RECORDING_DATA camRecData
	
#ENDIF

//  ___ ___  _ __ ___  _ __ __    ___  _ __		common functions for David (PRE FAILCHECK)
// / __/ _ \| '_ ` _ \| '_ ` _ \ / _ \| '_ \ 
//| (_| (_) | | | | | | | | | | | (_) | | | |
// \___\___/|_| |_| |_|_| |_| |_|\___/|_| |_|

VECTOR VECTOR_ZERO = <<0.0, 0.0, 0.0>>

//═════════════════════════════════╡ FAIL ╞═══════════════════════════════════

//═════════════════════════════════╡ PED CONTROL ╞═══════════════════════════════════
ENUM SELECT_PEDS
	SEL_MICHAEL									= 0,
	SEL_FRANKLIN,
	SEL_TREVOR	
ENDENUM

SELECTOR_PED_STRUCT sSelectorPeds
SELECTOR_CAM_STRUCT sCamDetails
structPedsForConversation myScriptedSpeech
CHASE_HINT_CAM_STRUCT		localChaseHintCamStruct

//PURPOSE:	Gets the current selector ped that the player is playing as
FUNC SELECT_PEDS GET_CURRENT_SELECT_PED()
	SWITCH GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM())
		CASE SELECTOR_PED_MICHAEL
			RETURN SEL_MICHAEL
		BREAK
		CASE SELECTOR_PED_FRANKLIN
			RETURN SEL_FRANKLIN
		BREAK
		CASE SELECTOR_PED_TREVOR
			RETURN SEL_TREVOR
		BREAK
	ENDSWITCH
	RETURN SEL_MICHAEL
ENDFUNC

//PURPOSE:		Gets whether or not a select ped exists
FUNC BOOL DOES_SELECT_PED_EXIST(SELECT_PEDS PedToGet)
	SWITCH PedToGet
		CASE SEL_MICHAEL
			IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL
				RETURN TRUE
			ELSE
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
		CASE SEL_FRANKLIN
			IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_FRANKLIN
				RETURN TRUE
			ELSE
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
		CASE SEL_TREVOR
			IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_TREVOR
				RETURN TRUE
			ELSE
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_TREVOR])
					RETURN TRUE
				ELSE
					RETURN FALSE
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	RETURN FALSE
ENDFUNC

//PURPOSE:		If the ped is controlled by the player, return player. Else, return the ped.
FUNC PED_INDEX GET_SELECT_PED(SELECT_PEDS PedToGet)
	SWITCH PedToGet
		CASE SEL_MICHAEL
			IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_MICHAEL
				RETURN PLAYER_PED_ID()
			ELSE
				RETURN sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]
			ENDIF
		BREAK
		CASE SEL_FRANKLIN
			IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_FRANKLIN
				RETURN PLAYER_PED_ID()
			ELSE
				RETURN sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN]
			ENDIF
		BREAK
		CASE SEL_TREVOR
			IF GET_SELECTOR_SLOT_FROM_PLAYER_PED_ENUM(GET_CURRENT_PLAYER_PED_ENUM()) = SELECTOR_PED_TREVOR
				RETURN PLAYER_PED_ID()
			ELSE
				RETURN sSelectorPeds.pedID[SELECTOR_PED_TREVOR]
			ENDIF
		BREAK
	ENDSWITCH
	RETURN PLAYER_PED_ID()
ENDFUNC


PROC DO_SWITCH_EFFECT(SELECT_PEDS PedToSwitchTo)

	SWITCH PedToSwitchTo
		CASE SEL_MICHAEL
			ANIMPOSTFX_PLAY("SwitchSceneMichael", 0, FALSE)	
		BREAK
		CASE SEL_FRANKLIN
			ANIMPOSTFX_PLAY("SwitchSceneFranklin", 0, FALSE)
		BREAK
		CASE SEL_TREVOR
			ANIMPOSTFX_PLAY("SwitchSceneTrevor", 0, FALSE)
		BREAK
	ENDSWITCH
	
	//Slot in sound effect
	PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")

ENDPROC

//PURPOSE:	Registers select peds for cutscenes if they exist
PROC REGISTER_SELECT_PEDS_FOR_CUTSCENE(BOOL bMichael=TRUE, BOOL bFranklin=TRUE, BOOL bTrevor=TRUE)
	IF bMichael
		IF GET_CURRENT_SELECT_PED() <> SEL_MICHAEL
			IF DOES_SELECT_PED_EXIST(SEL_MICHAEL)
				IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
					REGISTER_ENTITY_FOR_CUTSCENE(GET_SELECT_PED(SEL_MICHAEL), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF bFranklin
		IF GET_CURRENT_SELECT_PED() <> SEL_FRANKLIN
			IF DOES_SELECT_PED_EXIST(SEL_FRANKLIN)
				IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
					REGISTER_ENTITY_FOR_CUTSCENE(GET_SELECT_PED(SEL_FRANKLIN), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ONE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	IF bTrevor
		IF GET_CURRENT_SELECT_PED() <> SEL_TREVOR
			IF DOES_SELECT_PED_EXIST(SEL_TREVOR)
				IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
					REGISTER_ENTITY_FOR_CUTSCENE(GET_SELECT_PED(SEL_TREVOR), "Trevor", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_TWO)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//═════════════════════════════════╡ FAIL PROCEDURES ╞═══════════════════════════════════

//                _ 
//               | |
//  ___ _ __   __| |
// / _ \ '_ \ / _` |
//|  __/ | | | (_| |
// \___|_| |_|\__,_|	end common functions for David (PRE FAILCHECK)


FUNC BOOL IS_ENTITY_IN_STORE(ENTITY_INDEX thisEntity)

	//RETURN IS_ENTITY_IN_ANGLED_AREA(thisEntity, <<-631.284729,-237.207611,36.787712>>, <<-617.379028,-227.087051,40.557003>>, 17.750000)
	RETURN IS_ENTITY_IN_ANGLED_AREA(thisEntity, <<-617.046875,-227.000381,35.408142>>, <<-631.636230,-237.873123,42.078369>>, 17.250000)
ENDFUNC			

BOOL bWarnedNotToLose = FALSE
BOOL bToldPlayerToGoBackInside
INT iTimeOfLastInShop

//PURPOSE:		Checks if the user has failed a level stage
FUNC BOOL HAS_MISSION_FAILED()

	WEAPON_TYPE currentPlayerWeapon

	IF eMissionStage <> PASSED_MISSION
	AND eMissionStage <> STAGE_CUTSCENE_BEGIN
	#IF IS_DEBUG_BUILD
	AND eMissionStage <> STAGE_DEBUG_STUFF
	AND eMissionStage <> UBER_RECORD
	AND eMissionStage <> UBER_PLAYBACK
	#ENDIF
		
		IF eMissionStage <> STAGE_INIT_MISSION
		AND eMissionStage <> STAGE_TIME_LAPSE
			IF DOES_ENTITY_EXIST(GET_SELECT_PED(SEL_MICHAEL))
				IF IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
					
					IF eMissionStage = STAGE_BIKE_CHASE
					OR eMissionStage = STAGE_BIKE_CHASE_START_OF_TUNNEL
					OR eMissionStage = STAGE_BIKE_CHASE_END_OF_TUNNEL
						//IF IS_ENTITY_ON_SCREEN(GET_SELECT_PED(SEL_MICHAEL))
						IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(GET_SELECT_PED(SEL_MICHAEL), PLAYER_PED_ID())
							eMissionFail = FAIL_KILL_MICHAEL
							RETURN TRUE
						ENDIF
					ENDIF
					//IF eMissionStage <> STAGE_CUTSCENE_TRUCK_ARRIVE
					
					IF eMissionStage <> STAGE_BIKE_CHASE
					OR eMissionStage <> STAGE_BIKE_CHASE_START_OF_TUNNEL
					OR eMissionStage <> STAGE_BIKE_CHASE_END_OF_TUNNEL
						eMissionFail = FAIL_KILL_MICHAEL
						RETURN TRUE
					ENDIF
				ENDIF
			ENDIF
			IF eMissionStage <> STAGE_RETURN_TO_BASE
				IF IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
					eMissionFail = FAIL_KILL_FRANKLIN
					RETURN TRUE
				ENDIF	
			ENDIF	
//			IF eMissionStage <> STAGE_CUTSCENE_END_SCENE
//				IF IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
//					eMissionFail = FAIL_KILL_CREW
//					RETURN TRUE
//				ENDIF
//			ENDIF
		ENDIF

		SWITCH eMissionStage

			CASE STAGE_TIME_LAPSE
			BREAK
			

			CASE STAGE_CUTSCENE_BEGIN
				IF IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
					eMissionFail = FAIL_KILL_CREW
					RETURN TRUE
				ENDIF
				IF IS_PED_INJURED(pedCrew[CREW_HACKER])
					eMissionFail = FAIL_KILL_CREW
					RETURN TRUE
				ENDIF
				IF IS_PED_INJURED(pedCrew[CREW_HACKER])
					eMissionFail = FAIL_KILL_CREW
					RETURN TRUE
				ENDIF
				IF bIsStealthPath
					IF NOT IS_VEHICLE_DRIVEABLE(vehCar)
					ENDIF
					IF NOT IS_VEHICLE_DRIVEABLE(vehVan)
					ENDIF
					IF NOT IS_VEHICLE_DRIVEABLE(vehTruck)
					ENDIF
				ELSE
					IF NOT IS_VEHICLE_DRIVEABLE(vehVan)
					ENDIF
					IF NOT IS_VEHICLE_DRIVEABLE(vehTruck)
					ENDIF
				ENDIF
			BREAK
			CASE STAGE_DRIVE_TO_STORE
				IF DOES_ENTITY_EXIST(pedLester)
					IF IS_PED_INJURED(pedLester)
						eMissionFail = FAIL_KILL_CREW
						RETURN TRUE
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(pedCrew[CREW_GUNMAN_NEW])
					IF IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
						eMissionFail = FAIL_KILL_CREW
						RETURN TRUE
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedCrew[CREW_DRIVER_NEW])
					IF IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
						eMissionFail = FAIL_KILL_CREW
						RETURN TRUE
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedCrew[CREW_HACKER])
					IF IS_PED_INJURED(pedCrew[CREW_HACKER])
						eMissionFail = FAIL_KILL_CREW
						RETURN TRUE
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(pedCrew[CREW_HACKER])
					IF IS_PED_INJURED(pedCrew[CREW_HACKER])
						eMissionFail = FAIL_KILL_CREW
						RETURN TRUE
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(vehTruck)
					IF NOT IS_VEHICLE_DRIVEABLE(vehTruck)
						eMissionFail = FAIL_DESTROY_TRUCK
						RETURN TRUE
					ENDIF
				ENDIF
				IF bIsStealthPath
				
					IF IS_ENTITY_IN_ANGLED_AREA(GET_SELECT_PED(SEL_MICHAEL), <<-631.231689,-237.481918,37.057220>>, <<-617.073486,-227.068680,41.903957>>, 17.000000)
						eMissionFail = FAIL_BLEW_HEIST
						RETURN TRUE
					ENDIF
				
					IF NOT IS_VEHICLE_DRIVEABLE(vehCar)
						eMissionFail = FAIL_DESTROY_CAR
						RETURN TRUE
					ELSE
						IF IS_VEHICLE_PERMANENTLY_STUCK(vehCar)
							eMissionFail = FAIL_DESTROY_CAR
							RETURN TRUE
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(vehVan)
						IF NOT IS_VEHICLE_DRIVEABLE(vehVan)
							eMissionFail = FAIL_DESTROY_VAN
							RETURN TRUE
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_FRANKLIN))
					AND NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_MICHAEL))
						IF GET_DISTANCE_BETWEEN_COORDS(	GET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN)), 
														GET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL))) > 200
							
							PRINT_NOW("CMN_FLEAVE", DEFAULT_GOD_TEXT_TIME, 1)
							
							eMissionFail = FAIL_LOSE_FRANKLIN
							RETURN TRUE
						ENDIF
					ENDIF
				ELSE
				
					IF IS_ENTITY_IN_ANGLED_AREA(GET_SELECT_PED(SEL_FRANKLIN), <<-631.231689,-237.481918,37.057220>>, <<-617.073486,-227.068680,41.903957>>, 17.000000)
						eMissionFail = FAIL_BLEW_HEIST
						RETURN TRUE
					ENDIF
				
					IF DOES_ENTITY_EXIST(vehVan)
						IF NOT IS_VEHICLE_DRIVEABLE(vehVan)
							eMissionFail = FAIL_DESTROY_VAN
							RETURN TRUE
						ENDIF
						
						IF IS_VEHICLE_PERMANENTLY_STUCK(vehVan)
							eMissionFail = FAIL_DESTROY_VAN
							RETURN TRUE
						ENDIF
						
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_FRANKLIN))
						GET_CURRENT_PED_WEAPON(GET_SELECT_PED(SEL_FRANKLIN), currentPlayerWeapon)
						IF currentPlayerWeapon <> WEAPONTYPE_UNARMED
							IF IS_PED_ON_FOOT(GET_SELECT_PED(SEL_FRANKLIN))
								IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN)), vFrontOfStore) < 50.0
									eMissionFail = FAIL_BLEW_HEIST
									RETURN TRUE	
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_FRANKLIN))
					AND NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_MICHAEL))
						IF GET_DISTANCE_BETWEEN_COORDS(	GET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN)), GET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL))) > 200
							eMissionFail = FAIL_LOSE_MICHAEL
							RETURN TRUE
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_FRANKLIN))
					AND NOT IS_ENTITY_DEAD(pedCrew[CREW_DRIVER_NEW])
						IF GET_DISTANCE_BETWEEN_COORDS(	GET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN)), GET_ENTITY_COORDS(pedCrew[CREW_DRIVER_NEW])) > 200 
							eMissionFail = FAIL_LOSE_CREW
							RETURN TRUE
						ENDIF
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_FRANKLIN))
					AND NOT IS_ENTITY_DEAD(pedCrew[CREW_GUNMAN_NEW])
						IF GET_DISTANCE_BETWEEN_COORDS(	GET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN)), GET_ENTITY_COORDS(pedCrew[CREW_GUNMAN_NEW])) > 200
							eMissionFail = FAIL_LOSE_CREW
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF GET_CLOCK_HOURS() > 18
					eMissionFail = FAIL_SHOP_CLOSED
					RETURN TRUE
				ENDIF
				
			BREAK
			CASE STAGE_ROOF
			
				IF GET_CLOCK_HOURS() > 18
					eMissionFail = FAIL_SHOP_CLOSED
					RETURN TRUE
				ENDIF
			
				IF NOT IS_ENTITY_IN_ANGLED_AREA(GET_SELECT_PED(SEL_FRANKLIN), <<-580.509155,-296.544434,33.944912>>, <<-651.716370,-202.967545,90.151527>>, 40.750000)
				AND NOT IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), <<-578.8325, -280.2415, 34.3058>>, <<25.0, 25.0, 25.0>>)
				AND NOT IS_PED_RAGDOLL(GET_SELECT_PED(SEL_FRANKLIN))
				AND NOT IS_PED_JUMPING(GET_SELECT_PED(SEL_FRANKLIN))
					eMissionFail = FAIL_ABANDON
					RETURN TRUE
				ENDIF
				
				IF bHasSmokeGrenades = TRUE
					IF HAS_PED_GOT_WEAPON(GET_SELECT_PED(SEL_FRANKLIN), WEAPONTYPE_BZGAS)
						bHasSmokeGrenades = FALSE
						REQUEST_WEAPON_ASSET(WEAPONTYPE_BZGAS)
					ENDIF
				ENDIF
				
				IF bHasSmokeGrenades = FALSE
				AND bStageSetup
				AND iStageSection < 8
				AND (GET_GAME_TIMER() - iTimeOfThrowing) > 7500
					REQUEST_WEAPON_ASSET(WEAPONTYPE_BZGAS)
					IF HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_BZGAS)
						IF GET_AMMO_IN_PED_WEAPON(GET_SELECT_PED(SEL_FRANKLIN), WEAPONTYPE_BZGAS) = 0 // GET_PED_AMMO_BY_TYPE(GET_SELECT_PED(SEL_FRANKLIN), AMMOTYPE_SMOKE_GRENADE) = 0
							eMissionFail = FAIL_NO_GAS
							RETURN TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(GET_SELECT_PED(SEL_FRANKLIN), <<-631.231689,-237.481918,37.057220>>, <<-617.073486,-227.068680,41.903957>>, 17.000000)
				OR IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), <<-631.596985,-237.634689,38.877033>>,<<3.500000,3.500000,1.812500>>)
				
					eMissionFail = FAIL_BLEW_ID
					RETURN TRUE
				ENDIF
				
				IF IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_DONTCARE, <<-550.518066,-387.071686,30.874672>>, <<-727.126892,-77.976135,85.244141>>, 223.50000)
				AND NOT IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_BZGAS, <<-550.518066,-387.071686,30.874672>>, <<-727.126892,-77.976135,85.244141>>, 223.50000)
				
					//Horrible but ... 1489301
					WHILE NOT CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_BOOM", CONV_PRIORITY_VERY_HIGH)	
						wait(0)
					ENDWHILE
					WHILE IS_SCRIPTED_CONVERSATION_ONGOING()
						WAIT(0)
					ENDWHILE
					eMissionFail = FAIL_BLEW_HEIST
					RETURN TRUE
				ENDIF
				
				//If a grenade is in surrounding area but not in roof area...
				IF IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_BZGAS, <<-550.518066,-387.071686,30.874672>>, <<-727.126892,-77.976135,85.244141>>, 223.500000)
				AND NOT IS_EXPLOSION_IN_ANGLED_AREA(EXP_TAG_BZGAS, <<-587.745422,-295.559692,-34.072205>>, <<-638.567200,-203.272400,165.577709>>, 33.250000)
					eMissionFail = FAIL_GAS
					RETURN TRUE
				ENDIF
				
				IF (GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0	AND iStageSection < 6)
				OR IS_BULLET_IN_ANGLED_AREA(<<-550.518066,-387.071686,30.874672>>, <<-727.126892,-77.976135,85.244141>>, 223.50000, TRUE)
				
					eMissionFail = FAIL_BLEW_HEIST
					RETURN TRUE
				ENDIF
				
			BREAK
			CASE STAGE_CUTSCENE_GAS_STORE
				FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
					IF DOES_ENTITY_EXIST(pedStore[iCounter])
						IF IS_PED_INJURED(pedStore[iCounter])
						ENDIF
					ENDIF
				ENDFOR
				IF DOES_ENTITY_EXIST(pedSecurity)
					IF IS_PED_INJURED(pedSecurity)
					ENDIF
				ENDIF
			BREAK
			CASE STAGE_CUTSCENE_AGGRESSIVE_ARRIVE
				FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
					IF DOES_ENTITY_EXIST(pedStore[iCounter])
						IF IS_PED_INJURED(pedStore[iCounter])
						ENDIF
					ENDIF
				ENDFOR
				IF DOES_ENTITY_EXIST(pedSecurity)
					IF IS_PED_INJURED(pedSecurity)
					ENDIF
				ENDIF
				IF IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
					eMissionFail = FAIL_KILL_CREW
					RETURN TRUE
				ENDIF
				IF IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
					eMissionFail = FAIL_KILL_CREW
					RETURN TRUE
				ENDIF
				IF NOT IS_VEHICLE_DRIVEABLE(vehVan)
				ENDIF
			BREAK
			CASE STAGE_GRAB_LOOT
				IF IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
					eMissionFail = FAIL_KILL_CREW
					RETURN TRUE
				ENDIF
				
				IF IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
					eMissionFail = FAIL_KILL_CREW
					RETURN TRUE
				ENDIF
				
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					IF NOT IS_ENTITY_IN_ANGLED_AREA(GET_SELECT_PED(SEL_MICHAEL), <<-643.152100,-243.835480,50.909519>>, <<-609.548767,-224.078140,34.872833>>, 21.250000)

						eMissionFail = FAIL_ABANDON
						RETURN TRUE
					ENDIF
				ENDIF
				
				IF bGuardRuns
					IF DOES_ENTITY_EXIST(pedSecurity)
						IF NOT IS_PED_INJURED(pedSecurity)
							IF NOT IS_ENTITY_IN_ANGLED_AREA(pedSecurity, <<-643.152100,-243.835480,50.909519>>, <<-609.548767,-224.078140,34.872833>>, 21.250000)
							AND NOT IS_ENTITY_IN_ANGLED_AREA(pedSecurity, <<-673.401794,-222.553802,35.037868>>, <<-639.305359,-237.363617,39.207844>>, 18.500000)

								eMissionFail = FAIL_SECURITY_ESCAPED
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehTruck)
					IF IS_ENTITY_DEAD(vehTruck)
						eMissionFail = FAIL_DESTROY_TRUCK
						RETURN TRUE
					ENDIF					
				ENDIF
				
				FOR iCounter = 0 TO 2
					IF DOES_ENTITY_EXIST(vehBike[iCounter])
						IF NOT IS_VEHICLE_DRIVEABLE(vehBike[iCounter])
						OR (IS_VEHICLE_TYRE_BURST(vehBike[iCounter], SC_WHEEL_BIKE_FRONT)
						OR IS_VEHICLE_TYRE_BURST(vehBike[iCounter], SC_WHEEL_BIKE_REAR))
							eMissionFail = FAIL_DESTROY_BIKE
							RETURN TRUE
						ENDIF
					ENDIF
				ENDFOR
				
				//Fail if you go outside for too long
				IF NOT IS_ENTITY_IN_STORE(PLAYER_PED_ID())
					IF GET_GAME_TIMER() - iTimeOfLastInShop > 20000
					AND bToldPlayerToGoBackInside = TRUE
					OR bIsStealthPath
						eMissionFail = FAIL_ABANDON
						RETURN TRUE
					ENDIF
					
					IF bToldPlayerToGoBackInside = FALSE
					AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_DIALOGUE_IF_SUBTITLES_OFF)
						PRINT_NOW("JH_ABANDON", DEFAULT_GOD_TEXT_TIME, 1)
						bToldPlayerToGoBackInside = TRUE
					ENDIF		
					
				ELSE
					bToldPlayerToGoBackInside = FALSE
					iTimeOfLastInShop = GET_GAME_TIMER()
				ENDIF					
				
			BREAK
			CASE STAGE_CUTSCENE_LEAVE_STORE
			
				IF bInSufficientTake
					eMissionFail = FAIL_DIDNT_STEAL_ENOUGH_DIAMONDS
					RETURN TRUE
				ENDIF
			
				IF IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
					eMissionFail = FAIL_KILL_CREW
					RETURN TRUE
				ENDIF
			BREAK
			
			CASE STAGE_BIKE_CHASE
			CASE STAGE_BIKE_CHASE_START_OF_TUNNEL
			CASE STAGE_BIKE_CHASE_END_OF_TUNNEL
							
				IF NOT bWorstGunDies
					IF NOT IS_VEHICLE_DRIVEABLE(vehBike[1])
						eMissionFail = FAIL_DESTROY_BIKE
						RETURN TRUE
					ELSE
						IF IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
							PRINTLN("CREW_GUNMAN_NEW dided in bike chase")
							eMissionFail = FAIL_KILL_CREW
						ENDIF
						IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
							IF (NOT IS_PED_IN_ANY_VEHICLE(pedCrew[CREW_GUNMAN_NEW]) AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1]))
								PRINTLN("CREW_GUNMAN_NEW dided in bike chase 2")
								eMissionFail = FAIL_KILL_CREW
								RETURN TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF		
				
				IF NOT IS_VEHICLE_DRIVEABLE(vehBike[0])
					//IF NOT IS_ENTITY_IN_WATER(vehBike[0])
						eMissionFail = FAIL_DESTROY_BIKE
						RETURN TRUE
					//ENDIF
				ELSE
				
					IF GET_DISTANCE_BETWEEN_COORDS(	GET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN)), 
													GET_ENTITY_COORDS(vehBike[0])) > 150.00
						IF bWarnedNotToLose = FALSE
							PRINT_NOW("JH_LOSINGC", DEFAULT_GOD_TEXT_TIME, 1)
							bWarnedNotToLose = TRUE
						ENDIF
					ENDIF	
					IF GET_DISTANCE_BETWEEN_COORDS(	GET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN)), 
													GET_ENTITY_COORDS(vehBike[0])) > 200
						IF NOT IS_ENTITY_ON_SCREEN(vehBike[0])
							eMissionFail = FAIL_LOSE_CHASE
							RETURN TRUE
						ENDIF
					ENDIF
					
					//Fix for NG - 1765626
					IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), <<1003.3589, -206.0158, 69.3833+2.500000>>,<<100.000000,100.000000,2.000000>>)
						eMissionFail = FAIL_LOSE_CHASE
						RETURN TRUE
					ENDIF
					
					//Close to disabled interior car park
					IF IS_ENTITY_IN_ANGLED_AREA(GET_SELECT_PED(SEL_FRANKLIN), <<-213.071793,-615.926941,47.344517>>, <<-278.713379,-607.776978,30.757450>>, 25.000000)
						eMissionFail = FAIL_LOSE_CHASE
						RETURN TRUE
					ENDIF
					
					IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
						IF (NOT IS_PED_IN_ANY_VEHICLE(pedCrew[CREW_DRIVER_NEW]) AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0]))
							SCRIPT_ASSERT("CREW_DRIVER_NEW died in bike chase")
							eMissionFail = FAIL_KILL_CREW
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_VEHICLE_DRIVEABLE(vehBike[2])
					eMissionFail = FAIL_DESTROY_BIKE
					RETURN TRUE
				ELSE
					IF NOT IS_ENTITY_AT_ENTITY(vehBike[2], GET_SELECT_PED(SEL_FRANKLIN), <<150.0, 150.0, 150.0>>, FALSE, FALSE)
						IF NOT IS_ENTITY_ON_SCREEN(vehBike[2])
							eMissionFail = FAIL_LOSE_BIKE
							RETURN TRUE
						ENDIF
					ENDIF												
				ENDIF
				
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0])
					IF GET_TIME_POSITION_IN_RECORDING(vehBike[0]) > 152720.200
						IF GET_DISTANCE_BETWEEN_COORDS(	GET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN)), 
													GET_ENTITY_COORDS(vehBike[0])) > 125.0
							eMissionFail = FAIL_LOSE_CHASE
							RETURN TRUE	
						ENDIF
					ENDIF
				ENDIF
				
				
			BREAK

			CASE STAGE_TRUCK_FIGHT
				
					
				IF DOES_ENTITY_EXIST(pedCrew[CREW_DRIVER_NEW])
					IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
						IF NOT IS_PED_IN_ANY_VEHICLE(pedCrew[CREW_DRIVER_NEW])
							SCRIPT_ASSERT("CREW_DRIVER_NEW died out of car")
							//eMissionFail = FAIL_KILL_DRIVER
							//RETURN bStageSetup //TRUE will only fail when the setup flag is set to true
										   //Also... allows the death chceks to run to avoid asserts.
						ENDIF
					ELSE
						//SCRIPT_ASSERT("CREW_DRIVER_NEW died")
						eMissionFail = FAIL_KILL_DRIVER
						RETURN bStageSetup
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedCrew[CREW_HACKER])
					IF IS_PED_INJURED(pedCrew[CREW_HACKER])
						//SCRIPT_ASSERT("CREW_HACKER died")
						eMissionFail = FAIL_KILL_HACKER
						RETURN bStageSetup
					ENDIF
				ENDIF
				
				IF NOT bWorstGunDies
					IF DOES_ENTITY_EXIST(pedCrew[CREW_GUNMAN_NEW])
						IF IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
						OR NOT IS_PED_IN_ANY_VEHICLE(pedCrew[CREW_GUNMAN_NEW])
							//SCRIPT_ASSERT("CREW_GUNMAN_NEW died")
							eMissionFail = FAIL_KILL_GUNMAN
							RETURN bStageSetup
						ENDIF
					ENDIF
				ENDIF
				IF NOT IS_VEHICLE_DRIVEABLE(vehBike[0])
				OR NOT IS_VEHICLE_DRIVEABLE(vehBike[2])
					eMissionFail = FAIL_DESTROY_BIKE
					RETURN bStageSetup
				ENDIF
				
				IF NOT bWorstGunDies 
					IF NOT IS_VEHICLE_DRIVEABLE(vehBike[1])
						eMissionFail = FAIL_DESTROY_BIKE
						RETURN bStageSetup
					ENDIF
				ENDIF
				
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				//AND IS_VEHICLE_DRIVEABLE(vehTruck)
					//IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehTruck)
						IF NOT IS_VEHICLE_DRIVEABLE(vehTruck)
						OR IS_VEHICLE_PERMANENTLY_STUCK(vehTruck)
							eMissionFail = FAIL_DESTROY_TRUCK
							RETURN bStageSetup
						ENDIF
					//ENDIF
				ENDIF
												
				IF fRecordingTime > 222000.0
					IF iBlipCount > 0
						eMissionFail = FAIL_ARRESTED
						RETURN bStageSetup
					ENDIF
				ENDIF
				
				IF bEndOfLARiverReached = FALSE
								
					//Only  worry about distance from lead bike, no point checking all 3.
					IF DOES_ENTITY_EXIST(GET_SELECT_PED(SEL_MICHAEL))
						IF GET_DISTANCE_BETWEEN_ENTITIES(GET_SELECT_PED(SEL_MICHAEL), pedCrew[CREW_DRIVER_NEW], FALSE) > 350						
							PRINTLN("JH: lost best gunmen")
							eMissionFail = FAIL_LOSE_CREW
							RETURN bStageSetup
						ENDIF
					ENDIF
				ENDIF
				
				
				IF GET_CURRENT_SELECT_PED() = SEL_MICHAEL
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehTruck, FALSE) > 200
						PRINTLN("JH: lost Michael")
						eMissionFail = FAIL_ABANDON
						RETURN bStageSetup
					ENDIF
				ENDIF				
			BREAK
			
			CASE STAGE_CUTSCENE_RAMP_DOWN
				IF NOT bWorstGunDies
					IF IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
						//eMissionFail = FAIL_KILL_CREW
						//RETURN TRUE
					ENDIF
					IF NOT IS_VEHICLE_DRIVEABLE(vehBike[1])
						//eMissionFail = FAIL_DESTROY_BIKE
						//RETURN TRUE
					ENDIF
				ENDIF
				
				IF IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
					//eMissionFail = FAIL_KILL_CREW
					//RETURN TRUE
				ENDIF
				
				IF NOT IS_VEHICLE_DRIVEABLE(vehBike[0])
				OR NOT IS_VEHICLE_DRIVEABLE(vehBike[2])
					//eMissionFail = FAIL_DESTROY_BIKE
					//RETURN TRUE
				ENDIF
								
				IF NOT IS_VEHICLE_DRIVEABLE(vehTruck)
					//eMissionFail = FAIL_DESTROY_TRUCK
					//RETURN TRUE
				ENDIF
			BREAK
			
			CASE STAGE_RETURN_TO_BASE
				
				IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<206.7173, -2024.6572, 17.2513>>) > 1000.0
					eMissionFail = FAIL_ABANDON
					RETURN TRUE
				ENDIF
				
				
				IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<209.606293,-2005.642456,17.361458>>, <<212.183609,-2012.635254,21.525574>>, 11.000000)
						eMissionFail = FAIL_BLEW_HEIST
						RETURN TRUE	
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedLester)
					IF IS_PED_INJURED(pedLester)
						eMissionFail = FAIL_KILL_CREW
						RETURN TRUE
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(pedCrew[CREW_HACKER])
					IF IS_PED_INJURED(pedCrew[CREW_HACKER])
						eMissionFail = FAIL_KILL_HACKER
						RETURN TRUE
					ENDIF
				ENDIF
				
				IF NOT IS_VEHICLE_DRIVEABLE(vehTruck)
				OR IS_VEHICLE_PERMANENTLY_STUCK(vehTruck)
					eMissionFail = FAIL_DESTROY_TRUCK
					RETURN TRUE
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

INT iDisplayedTake
INT iMoneyCountEffect = -1

PROC DISPLAY_TAKE()
	IF bDrawCashScaleform
		
		SET_FAR_RIGHT_TITLE_POSITION_HUD_THIS_FRAME()
		
		IF iDisplayedTake < iCurrentTake
			
			IF iMoneyCountEffect = -1
				iMoneyCountEffect = GET_SOUND_ID()
				PLAY_SOUND_FRONTEND(iMoneyCountEffect, "ROBBERY_MONEY_TOTAL", "HUD_FRONTEND_CUSTOM_SOUNDSET")
			ENDIF
			iDisplayedTake = iDisplayedTake + ROUND(0 +@ 255000)
		ELSE
			//STOP_SOUND(iMoneyCountEffect)
			iMoneyCountEffect = -1
			iDisplayedTake = iCurrentTake
		ENDIF
		
		IF iDisplayedTake < TARGET_TAKE_VALUE
		AND eMissionStage = STAGE_GRAB_LOOT
			DRAW_GENERIC_SCORE(iDisplayedTake, "CMN_TAKE", 1000, HUD_COLOUR_RED, HUDORDER_DONTCARE, FALSE, "HUD_CASH")
		ELSE
			DRAW_GENERIC_SCORE(iDisplayedTake, "CMN_TAKE", 1000, HUD_COLOUR_GREEN, HUDORDER_DONTCARE, FALSE, "HUD_CASH")
		ENDIF
			
	ENDIF
ENDPROC


//  ___ ___  _ __ ___  _ __ __    ___  _ __		common functions for David (POST FAILCHECK)
// / __/ _ \| '_ ` _ \| '_ ` _ \ / _ \| '_ \ 
//| (_| (_) | | | | | | | | | | | (_) | | | |
// \___\___/|_| |_| |_|_| |_| |_|\___/|_| |_|


FUNC BOOL IS_GUNMAN_PACKIE()
	RETURN (GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)) = CM_GUNMAN_G_PACKIE_UNLOCK)
ENDFUNC

FUNC BOOL IS_GUNMAN_GUSTAVO()
	RETURN (GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)) = CM_GUNMAN_G_GUSTAV)
ENDFUNC


FUNC BOOL IS_GUNMAN_NORM()
	RETURN (GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)) = CM_GUNMAN_B_NORM)
ENDFUNC

FUNC BOOL IS_DRIVER_EDDIE()
	RETURN (GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW)) = CM_DRIVER_G_EDDIE)
ENDFUNC

FUNC BOOL IS_DRIVER_TALINA()
	RETURN (GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW)) = CM_DRIVER_G_TALINA_UNLOCK)
ENDFUNC

FUNC BOOL IS_DRIVER_KARIM()
	RETURN (GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW)) = CM_DRIVER_B_KARIM)
ENDFUNC


FUNC BOOL IS_HACKER_PAIGE()
	RETURN GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)) = CM_HACKER_G_PAIGE		
ENDFUNC

FUNC BOOL IS_HACKER_CHRISTIAN()
	RETURN GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)) = CM_HACKER_M_CHRIS	
ENDFUNC

FUNC BOOL IS_HACKER_RICKIE()
	RETURN GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)) = CM_HACKER_B_RICKIE_UNLOCK	
ENDFUNC


//═════════════════════════════════╡ SAFE WAIT ╞═══════════════════════════════════
//PURPOSE:	Performs a wait and a failcheck so that there are no deathcheck problems
PROC safeWait(INT iWaitTime)	

	DISPLAY_TAKE()
	WAIT(iWaitTime)
	HAS_MISSION_FAILED()
ENDPROC

//═════════════════════════════════╡ TEXT MANAGEMENT ╞═══════════════════════════════════
BOOL bConversationOngoing = FALSE
BOOL bGodTextOngoing = FALSE
BOOL bHelpTextOngoing = FALSE
//BOOL bWasConversationOngoingLastFrame = FALSE
//BOOL bWasGodTextOngoingLastFrame = FALSE
//BOOL bWasHelpTextOngoingLastFrame = FALSE
BOOL bHasConversationStoppedThisFrame = FALSE
//BOOL bHasGodTextStoppedThisFrame = FALSE
//BOOL bHasHelpTextStoppedThisFrame = FALSE
//STRING sLastPlayedConversation
INT iLastPlayedConversationHash
//STRING sLastPlayedGodText=""
INT iLastPlayedGodTextHash
//STRING sLastPlayedHelpText
INT iLastPlayedHelpTextHash
INT iTextCounter = 0
INT iTextTimer = 0
INT iTextTimeLastFrame = 0
BOOL bTextCountingDown = FALSE
BOOL bTextCountedDownThisFrame = FALSE
ENUM TEXT_TYPES
	TEXT_TYPE_CONVO,
	TEXT_TYPE_GOD,
	TEXT_TYPE_HELP
ENDENUM
CONST_INT TEXT_QUEUE_LENGTH 8
STRUCT sQueueEntry
	STRING sText
	INT TextHash
	BOOL bWaiting = FALSE
	INT textType
	BOOL bPlaysAlongsideOther
	INT iDelay
	BOOL bHasPlayed = FALSE
	
	#IF IS_DEBUG_BUILD
		TEXT_WIDGET_ID textQueueText
	#ENDIF
	
ENDSTRUCT
sQueueEntry textQueue[TEXT_QUEUE_LENGTH]

//Purpose: Stores what conversation was played last
PROC setLastPlayedConversation(STRING thisConversation)
	//sLastPlayedConversation = thisConversation
	iLastPlayedConversationHash = GET_HASH_KEY(thisConversation)
ENDPROC

//Purpose: Stores what god text was played last
PROC setLastPlayedGodText(STRING thisGodText)
	//sLastPlayedGodText = thisGodText
	iLastPlayedGodTextHash = GET_HASH_KEY(thisGodText)
ENDPROC

//Purpose: Stores what help text was played last
PROC setLastPlayedHelpText(STRING thisHelpText)
	//sLastPlayedHelpText = thisHelpText
	iLastPlayedHelpTextHash = GET_HASH_KEY(thisHelpText)
ENDPROC

PROC checkRunningText()
	bConversationOngoing = IS_SCRIPTED_CONVERSATION_ONGOING()
	//bGodTextOngoing = IS_THIS_PRINT_BEING_DISPLAYED(sLastPlayedGodText)
	bGodTextOngoing = IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF) //IS_MESSAGE_BEING_DISPLAYED()
	bHelpTextOngoing = IS_HELP_MESSAGE_BEING_DISPLAYED() //IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(sLastPlayedHelpText)
ENDPROC

//PURPOSE:	Sets all a text queue's entry to default and empty values
PROC clearTextQueueEntry(INT thisEntry)
	textQueue[thisEntry].sText = ""
	textQueue[thisEntry].TextHash = -1
	textQueue[thisEntry].iDelay = 0
	textQueue[thisEntry].bWaiting = FALSE
	textQueue[thisEntry].bHasPlayed = FALSE
ENDPROC

//PURPOSE:	Copies an entry from one to another
PROC copyTextQueueEntry(INT copyTo, INT copyFrom)
	textQueue[copyTo].sText = textQueue[copyFrom].sText
	textQueue[copyTo].TextHash = textQueue[copyFrom].TextHash
	textQueue[copyTo].bWaiting = textQueue[copyFrom].bWaiting
	textQueue[copyTo].bHasPlayed = textQueue[copyFrom].bHasPlayed
	textQueue[copyTo].textType = textQueue[copyFrom].textType
	textQueue[copyTo].bPlaysAlongsideOther = textQueue[copyFrom].bPlaysAlongsideOther
	textQueue[copyTo].iDelay = textQueue[copyFrom].iDelay
ENDPROC

//PURPOSE:	Clears all the text queue array entries
PROC emptyTextQueue()
	FOR iTextCounter = 0 TO TEXT_QUEUE_LENGTH-1
		clearTextQueueEntry(iTextCounter)
	ENDFOR
ENDPROC

//PURPOSE:	Empties the first slot, copies all the other slots into the neighbouring higher slot
PROC moveAlongTextQueue()
	clearTextQueueEntry(0)
	FOR iTextCounter = 0 TO TEXT_QUEUE_LENGTH-2
		copyTextQueueEntry(iTextCounter, iTextCounter+1)
	ENDFOR
	clearTextQueueEntry(TEXT_QUEUE_LENGTH-1)
ENDPROC

//PURPOSE:	Empties the last slot, copies all the other slots into the neighbouring lower slot
PROC moveBackTextQueue()
	clearTextQueueEntry(TEXT_QUEUE_LENGTH-1)
	FOR iTextCounter = 0 TO TEXT_QUEUE_LENGTH-2
		copyTextQueueEntry(TEXT_QUEUE_LENGTH-iTextCounter-1, TEXT_QUEUE_LENGTH-iTextCounter-2)
	ENDFOR
	clearTextQueueEntry(0)
ENDPROC

//PURPOSE:	Checks if the text string is waiting in the text queue
FUNC BOOL isTextWaitingInQueue(STRING thisText)
	BOOL bFound = FALSE
	iTextCounter = 0
	WHILE iTextCounter < TEXT_QUEUE_LENGTH AND bFound = FALSE
		IF textQueue[iTextCounter].bWaiting = TRUE
			IF textQueue[iTextCounter].TextHash = GET_HASH_KEY(thisText)
				bFound = TRUE
			ENDIF
		ENDIF
		iTextCounter++
	ENDWHILE
	IF bFound
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns true if supplied text is playing right now
FUNC BOOL isTextPlaying(STRING thisText)
	INT iThisText
	IF bConversationOngoing
		iThisText = GET_HASH_KEY(thisText)
		IF iThisText = iLastPlayedConversationHash 
			RETURN TRUE
		ENDIF
	ENDIF
	IF bGodTextOngoing
		iThisText = GET_HASH_KEY(thisText)
		IF iThisText = iLastPlayedGodTextHash 
			RETURN TRUE
		ENDIF
	ENDIF
	IF bHelpTextOngoing
		iThisText = GET_HASH_KEY(thisText)
		IF iThisText = iLastPlayedHelpTextHash 
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns true if a conversation is waiting in the queue
FUNC BOOL isAnyConversationWaitingInQueue()
	FOR iTextCounter = 0 TO TEXT_QUEUE_LENGTH-1
		IF textQueue[iTextCounter].bWaiting
			IF textQueue[iTextCounter].textType = ENUM_TO_INT(TEXT_TYPE_CONVO)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns true if a conversation is waiting in the queue
FUNC BOOL isAnyGodTextWaitingInQueue()
	FOR iTextCounter = 0 TO TEXT_QUEUE_LENGTH-1
		IF textQueue[iTextCounter].bWaiting
			IF textQueue[iTextCounter].textType = ENUM_TO_INT(TEXT_TYPE_GOD)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

//PURPOSE: Returns true if a conversation is waiting in the queue
FUNC BOOL isAnyHelpTextWaitingInQueue()
	FOR iTextCounter = 0 TO TEXT_QUEUE_LENGTH-1
		IF textQueue[iTextCounter].bWaiting
			IF textQueue[iTextCounter].textType = ENUM_TO_INT(TEXT_TYPE_HELP)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDFOR
	RETURN FALSE
ENDFUNC

//PURPOSE:	Clears all god text, help text and conversations
PROC clearText(BOOL bConversation=TRUE, BOOL bGodText=TRUE, BOOL bHelpText=TRUE)
	IF bConversation = TRUE
		KILL_ANY_CONVERSATION()
		//safeWait(0)
		//sLastPlayedConversation = ""
		iLastPlayedConversationHash = -1
	ENDIF
	IF bGodText
		CLEAR_PRINTS()
		//sLastPlayedGodText = ""
		iLastPlayedGodTextHash = -1
	ENDIF
	IF bHelpText
		CLEAR_HELP()
		//sLastPlayedHelpText = ""
		iLastPlayedHelpTextHash = -1
	ENDIF
ENDPROC

//Purpose: Starts a conversation running, will only play if previous conversation has stopped or bOverrideLast = TRUE
PROC pvtRunConversation(	STRING thisConversation, BOOL bClearAllText=FALSE, BOOL bOverrideLast=FALSE, BOOL bPlayWithHelp=TRUE,
							enumConversationPriority PassedConversationPriority=CONV_PRIORITY_MEDIUM,
							enumSubtitlesState ShouldDisplaySubtitles=DISPLAY_SUBTITLES,
							enumBriefScreenState ShouldAddToBriefScreen=DO_ADD_TO_BRIEF_SCREEN)
	BOOL bIsSameAsLastPlayed
	IF iLastPlayedConversationHash <> GET_HASH_KEY(thisConversation)
		bIsSameAsLastPlayed = FALSE
	ELSE
		bIsSameAsLastPlayed = TRUE
	ENDIF
	
	BOOL bShoudlClearOther = TRUE
	IF bPlayWithHelp
		bShoudlClearOther = FALSE
	ENDIF
	
	IF bClearAllText
		clearText()
		emptyTextQueue()
	ELSE
		IF bOverrideLast
			IF NOT bIsSameAsLastPlayed
				clearText(bOverrideLast, bOverrideLast, bShoudlClearOther)
			ENDIF
		ENDIF
	ENDIF
		
	IF (NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()) OR bClearAllText OR (bOverrideLast AND NOT bIsSameAsLastPlayed) OR NOT bConversationOngoing
		IF CREATE_CONVERSATION(myScriptedSpeech, sAudioBlock, thisConversation, PassedConversationPriority, ShouldDisplaySubtitles,
							ShouldAddToBriefScreen)
			setLastPlayedConversation(thisConversation)
			bConversationOngoing = TRUE
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:	Runs the conversation- can clear all text, override the last text of this time, and should play with compatible ther texts
PROC RunConversation(	STRING thisConversation, BOOL bClearAllText=FALSE, BOOL bOverrideLast=FALSE, BOOL bPlayWithHelp=TRUE, enumConversationPriority PassedConversationPriority=CONV_PRIORITY_MEDIUM, enumSubtitlesState ShouldDisplaySubtitles=DISPLAY_SUBTITLES, enumBriefScreenState ShouldAddToBriefScreen=DO_ADD_TO_BRIEF_SCREEN)
						
	IF bOverrideLast = FALSE
		bOverrideLast = (NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON())
	ENDIF
						
	pvtRunConversation(	thisConversation, bClearAllText, bOverrideLast, bPlayWithHelp, PassedConversationPriority, ShouldDisplaySubtitles, ShouldAddToBriefScreen)
	
	IF NOT (bConversationOngoing AND iLastPlayedConversationHash = GET_HASH_KEY(thisConversation))
	
		IF bClearAllText OR bOverrideLast OR (NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON())
			IF textQueue[0].TextHash <> -1
				moveBackTextQueue()
			ENDIF
			textQueue[0].sText = thisConversation
			textQueue[0].TextHash = GET_HASH_KEY(thisConversation)
			textQueue[0].bWaiting = TRUE
			textQueue[0].bHasPlayed = FALSE
			textQueue[0].textType = ENUM_TO_INT(TEXT_TYPE_CONVO)
			textQueue[0].bPlaysAlongsideOther = bPlayWithHelp
			textQueue[0].iDelay = 0
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:	Runs the god text- can clear all text, override the last text of this time, and should play with compatible ther texts
PROC runGodText(STRING thisGodText, BOOL bClearAllText=FALSE, BOOL bOverrideLast=FALSE, BOOL bPlayWithHelp=TRUE,
				INT Duration = DEFAULT_GOD_TEXT_TIME)
	BOOL bIsSameAsLastPlayed
	IF iLastPlayedGodTextHash <> GET_HASH_KEY(thisGodText)
		bIsSameAsLastPlayed = FALSE
	ELSE
		bIsSameAsLastPlayed = TRUE
	ENDIF
	
	BOOL bShoudlClearOther = TRUE
	IF bPlayWithHelp
		bShoudlClearOther = FALSE
	ENDIF
	
	IF bClearAllText
		clearText()
		emptyTextQueue()
	ELSE
		IF bOverrideLast
			IF NOT bIsSameAsLastPlayed
				clearText(bOverrideLast, bOverrideLast, bShoudlClearOther)
			ENDIF
		ENDIF
	ENDIF
		
	IF bClearAllText OR (bOverrideLast AND NOT bIsSameAsLastPlayed) OR NOT bGodTextOngoing OR (NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON())
		PRINT_NOW(thisGodText, Duration, 1)
		setLastPlayedGodText(thisGodText)
		bGodTextOngoing = TRUE
	ENDIF
ENDPROC

//PURPOSE:	Runs the help text- can clear all text, override the last text of this time, and should play with compatible ther texts
PROC runHelpText(	STRING thisHelpText, BOOL bClearAllText=FALSE, BOOL bOverrideLast=FALSE, BOOL bPlayWithOtherText=TRUE,
					BOOL bForever=FALSE)
	BOOL bIsSameAsLastPlayed
	IF iLastPlayedHelpTextHash <> GET_HASH_KEY(thisHelpText)
		bIsSameAsLastPlayed = FALSE
	ELSE
		bIsSameAsLastPlayed = TRUE
	ENDIF
	
	BOOL bShoudlClearOther = TRUE
	IF bPlayWithOtherText
		bShoudlClearOther = FALSE
	ENDIF
	
	IF bClearAllText
		clearText()
		emptyTextQueue()
	ELSE
		IF bOverrideLast
			IF NOT bIsSameAsLastPlayed
				clearText(bShoudlClearOther, bShoudlClearOther, bOverrideLast)
			ENDIF
		ENDIF
	ENDIF
		
	IF bClearAllText OR (bOverrideLast AND NOT bIsSameAsLastPlayed) OR NOT bHelpTextOngoing
		IF bForever
			PRINT_HELP_FOREVER(thisHelpText)
		ELSE
			PRINT_HELP(thisHelpText)
		ENDIF
		setLastPlayedHelpText(thisHelpText)
		bHelpTextOngoing = TRUE
	ENDIF
ENDPROC




//PURPOSE:	Adds the text to the queue, will play when free
PROC addTextToQueue(STRING thisText, TEXT_TYPES thisTextType, INT iDelay=0, BOOL bPlaysAlongsideOther=TRUE)
	INT thisEntry = -1
	iTextCounter = 0
	WHILE iTextCounter < TEXT_QUEUE_LENGTH AND thisEntry = -1
		IF textQueue[iTextCounter].bWaiting = FALSE
			thisEntry = iTextCounter
		ENDIF
		iTextCounter++
	ENDWHILE
	IF thisEntry <> -1
		textQueue[thisEntry].sText = thisText
		
		#IF IS_DEBUG_BUILD
			SET_CONTENTS_OF_TEXT_WIDGET(textQueue[thisEntry].textQueueText , textQueue[thisEntry].sText)
		#ENDIF
		
		textQueue[thisEntry].TextHash = GET_HASH_KEY(thisText)
		textQueue[thisEntry].bWaiting = TRUE
		textQueue[thisEntry].bHasPlayed = TRUE
		textQueue[thisEntry].textType = ENUM_TO_INT(thisTextType)
		textQueue[thisEntry].bPlaysAlongsideOther = bPlaysAlongsideOther
		textQueue[thisEntry].iDelay = iDelay
	ENDIF
ENDPROC

//Choose a specific line based on the driver
PROC ADD_DRIVER_TEXT_TO_QUEUE(STRING gdString, STRING mdString, STRING bdString)

	IF IS_DRIVER_EDDIE()
		addTextToQueue(gdString, TEXT_TYPE_CONVO)
	ELIF IS_DRIVER_TALINA()
		addTextToQueue(mdString, TEXT_TYPE_CONVO)
	ELIF IS_DRIVER_KARIM()		
		addTextToQueue(bdString, TEXT_TYPE_CONVO)		
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bIsSuperDebugEnabled
			TEXT_LABEL_63 errorMsg
			errorMsg += "ADD_DRIVER_TEXT_TO_QUEUE:"
			errorMsg +=gdString
			SCRIPT_ASSERT(errorMsg)
		ENDIF
	#ENDIF
	
ENDPROC

//Choose a specific line based on the driver
PROC PLAY_DRIVER_CONVO(STRING gdString, STRING mdString, STRING bdString)

	IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehBIke[0]) < 95.0
		IF IS_DRIVER_EDDIE()
			RunConversation(gdString, TRUE, TRUE)
		ELIF IS_DRIVER_TALINA()
			RunConversation(mdString, TRUE, TRUE)
		ELIF IS_DRIVER_KARIM()
			IF GET_LENGTH_OF_LITERAL_STRING(bdString) > 0
				RunConversation(bdString, TRUE, TRUE)
			ENDIF
		ENDIF
	ELSE
		PRINTLN("Didn't play directions as fell off bike:", gdString, ":",mdString,":", bdString )
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bIsSuperDebugEnabled
			TEXT_LABEL_63 errorMsg
			errorMsg += "PLAY_DRIVER_CONVO:"
			errorMsg +=gdString
			SCRIPT_ASSERT(errorMsg)
		ENDIF
	#ENDIF
	
ENDPROC

//PURPOSE:	Counts down wait time before playing text queue entry
FUNC BOOL isTextCountDownComplete(INT thisQueueEntry)
	bTextCountedDownThisFrame = TRUE
	IF NOT bTextCountingDown
		iTextTimeLastFrame = GET_GAME_TIMER()
		iTextTimer = 0
		bTextCountingDown = TRUE
		
		//Exit a frame earlier if there is no delay. Ross
		IF textQueue[thisQueueEntry].iDelay = 0
			RETURN TRUE
		ENDIF
		
	ELSE
		iTextTimer += GET_GAME_TIMER() - iTextTimeLastFrame
		iTextTimeLastFrame = GET_GAME_TIMER()
		IF iTextTimer > textQueue[thisQueueEntry].iDelay
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE:	Resets text countdown variables
PROC resetTextCountDown()
	iTextTimeLastFrame = 0
	iTextTimer = 0
	bTextCountingDown = FALSE
ENDPROC

BOOL bManagerGodTextDisplayed
BOOL bMichaelRespondsToGoodGunman
INT iTimeOfManagerDialogue

//PURPOSE:	Manages the text queue, plays text when ready
PROC updateTextManager()
	checkRunningText()
	
	bTextCountedDownThisFrame = FALSE
		
	#IF IS_DEBUG_BUILD
	
	IF bIsSuperDebugEnabled
		DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), "Q processing", 0.5)
	
		IF bConversationOngoing
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), "bConversationOngoing = TRUE", -0.5)
		ELSE
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), "bConversationOngoing = FALSE", -0.5)
		ENDIF
		
		IF bGodTextOngoing
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), "bGodTextOngoing = TRUE", -0.6)
		ELSE
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), "bGodTextOngoing = FALSE", -0.6)
		ENDIF
		
		IF bHelpTextOngoing
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), "bHelpTextOngoing = TRUE", -0.7)
		ELSE
			DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), "bHelpTextOngoing = FALSE", -0.7)
		ENDIF
	ENDIF
	#ENDIF
	
	IF textQueue[0].bWaiting
	AND (bIsRandomManagerEventNow = FALSE)		//Help the manager be able to speak by not playing Q
		#IF IS_DEBUG_BUILD
			IF bIsSuperDebugEnabled
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), "item waiting", 0.4)
			ENDIF
		#ENDIF
		SWITCH INT_TO_ENUM(TEXT_TYPES, textQueue[0].textType)
			CASE TEXT_TYPE_CONVO
				IF (NOT bConversationOngoing)
				AND ((NOT bGodTextOngoing) OR (NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()))					
					#IF IS_DEBUG_BUILD
						IF bIsSuperDebugEnabled
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), "CONVO: should play", 0.1)
						ENDIF
					#ENDIF
					IF NOT bHelpTextOngoing OR textQueue[0].bPlaysAlongsideOther
						#IF IS_DEBUG_BUILD
							IF bIsSuperDebugEnabled
								DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), "CONVO: Checking countdown", 0.0)
							ENDIF
						#ENDIF
						IF isTextCountDownComplete(0)
							pvtRunConversation(textQueue[0].sText, FALSE, FALSE, textQueue[0].bPlaysAlongsideOther)
							IF (bConversationOngoing AND iLastPlayedConversationHash = textQueue[0].TextHash)
								moveAlongTextQueue()
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE TEXT_TYPE_GOD
				IF (NOT bGodTextOngoing)
				AND ((NOT bConversationOngoing) OR (NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON()))					
					#IF IS_DEBUG_BUILD
						IF bIsSuperDebugEnabled
							DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), "GOD TEXT: should play", 0.1)
						ENDIF
					#ENDIF
					IF NOT bHelpTextOngoing OR textQueue[0].bPlaysAlongsideOther
						#IF IS_DEBUG_BUILD
							IF bIsSuperDebugEnabled
								DRAW_DEBUG_TEXT_ABOVE_ENTITY(PLAYER_PED_ID(), "GOD TEXT: Checking countdown", 0.0)
							ENDIF
						#ENDIF
						IF isTextCountDownComplete(0)
							runGodText(textQueue[0].sText, FALSE, FALSE, textQueue[0].bPlaysAlongsideOther)
							moveAlongTextQueue()
						ENDIF
					ENDIF
				ENDIF
			BREAK
			CASE TEXT_TYPE_HELP
				IF NOT bHelpTextOngoing	
					IF (NOT bConversationOngoing AND NOT bGodTextOngoing) OR textQueue[0].bPlaysAlongsideOther
						IF isTextCountDownComplete(0)
							runHelpText(textQueue[0].sText, FALSE, FALSE, textQueue[0].bPlaysAlongsideOther)
							moveAlongTextQueue()
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
		
	/*IF bWasConversationOngoingLastFrame AND NOT bConversationOngoing
		bHasConversationStoppedThisFrame = TRUE
	ELSE
		bHasConversationStoppedThisFrame = FALSE
	ENDIF
	bWasConversationOngoingLastFrame = bConversationOngoing*/
	
	/*IF bWasGodTextOngoingLastFrame AND NOT bGodTextOngoing
		bHasGodTextStoppedThisFrame = TRUE
	ELSE
		bHasGodTextStoppedThisFrame = FALSE
	ENDIF
	bWasGodTextOngoingLastFrame = bGodTextOngoing*/
	
	/*IF bWasHelpTextOngoingLastFrame AND NOT bHelpTextOngoing
		bHasHelpTextStoppedThisFrame = TRUE
	ELSE
		bHasHelpTextStoppedThisFrame = FALSE
	ENDIF
	bWasHelpTextOngoingLastFrame = bHelpTextOngoing*/
	
	IF NOT bTextCountedDownThisFrame
		resetTextCountDown()
	ENDIF
ENDPROC

//PURPOSE:	Clear all text manager variables
PROC cleanupTextManager()
	emptyTextQueue()
	bConversationOngoing = FALSE
	//bGodTextOngoing = FALSE
	bGodTextOngoing = FALSE
	bHelpTextOngoing = FALSE
	//sLastPlayedConversation = ""
	iLastPlayedConversationHash = -1
	//sLastPlayedGodText = ""
	iLastPlayedGodTextHash = -1
	//sLastPlayedHelpText = ""
	iLastPlayedHelpTextHash = -1
	iTextCounter = 0
	iTextTimer = 0
	iTextTimeLastFrame = 0
	bTextCountingDown = FALSE
	bTextCountedDownThisFrame = FALSE
ENDPROC

//═════════════════════════════════╡ SOUND ╞═══════════════════════════════════
STRUCT sSound
	INT SoundId
	BOOL bHasPlayed = FALSE
	BOOL bHasPlayedAtAll = FALSE
ENDSTRUCT
sSound sndList[SND_LIST_SIZE]

PROC PlaySound(SOUND_LIST ListName, STRING SoundName, BOOL PlayOnce = TRUE)
	IF NOT sndList[ListName].bHasPlayed
		IF NOT sndList[ListName].bHasPlayedAtAll
			sndList[ListName].SoundId = GET_SOUND_ID()
		ENDIF
		PLAY_SOUND(sndList[ListName].SoundId, SoundName)
		PRINTLN("**JEWEL HEIST** PLAYING SOUND: ", sndList[ListName].SoundId, " : ", SoundName, " **********")
		sndList[ListName].bHasPlayedAtAll = TRUE
		IF PlayOnce
			sndList[ListName].bHasPlayed = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC PlaySoundFromCoord(SOUND_LIST ListName, STRING SoundName, Vector Position, BOOL PlayOnce = TRUE)
	IF NOT sndList[ListName].bHasPlayed
		IF NOT sndList[ListName].bHasPlayedAtAll
			sndList[ListName].SoundId = GET_SOUND_ID()
		ENDIF
		PLAY_SOUND_FROM_COORD(sndList[ListName].SoundId, SoundName, Position)
		PRINTLN("**JEWEL HEIST** PLAYING SOUND FROM COORD: ", sndList[ListName].SoundId, " : ", SoundName, Position, " **********")
		sndList[ListName].bHasPlayedAtAll = TRUE
		IF PlayOnce
			sndList[ListName].bHasPlayed = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC PlaySoundFromEntity(SOUND_LIST ListName, STRING SoundName, ENTITY_INDEX EntityIndex, BOOL PlayOnce = TRUE)
	IF NOT sndList[ListName].bHasPlayed
		IF NOT sndList[ListName].bHasPlayedAtAll
			sndList[ListName].SoundId = GET_SOUND_ID()
		ENDIF
		PLAY_SOUND_FROM_ENTITY(sndList[ListName].SoundId, SoundName, EntityIndex)
		PRINTLN("**JEWEL HEIST** PLAYING SOUND FROM ENTITY: ", sndList[ListName].SoundId, " : ", SoundName, " **********")
		sndList[ListName].bHasPlayedAtAll = TRUE
		IF PlayOnce
			sndList[ListName].bHasPlayed = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC PlaySoundFrontend(SOUND_LIST ListName, STRING SoundName, BOOL PlayOnce = TRUE)
	IF NOT sndList[ListName].bHasPlayed
		IF NOT sndList[ListName].bHasPlayedAtAll
			sndList[ListName].SoundId = GET_SOUND_ID()
		ENDIF
		PLAY_SOUND_FRONTEND(sndList[ListName].SoundId, SoundName)
		PRINTLN("**JEWEL HEIST** PLAYING SOUND FRONTEND: ", sndList[ListName].SoundId, " : ", SoundName, " **********")
		sndList[ListName].bHasPlayedAtAll = TRUE
		IF PlayOnce
			sndList[ListName].bHasPlayed = TRUE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HasSoundPlayed(SOUND_LIST ListName)
	IF sndList[ListName].bHasPlayedAtAll
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC ReleaseSound(SOUND_LIST ListName)
	IF HasSoundPlayed(ListName)
		IF NOT HAS_SOUND_FINISHED(sndList[ListName].SoundId)
			STOP_SOUND(sndList[ListName].SoundId)
		ENDIF
		RELEASE_SOUND_ID(sndList[ListName].SoundId)
		sndList[ListName].bHasPlayed = FALSE
		sndList[ListName].bHasPlayedAtAll = FALSE
	ENDIF
ENDPROC

//═════════════════════════════════╡ NEED LIST ╞═══════════════════════════════════
//═════════════════════════════════╡ MODELS ╞═══════════════════════════════════
CONST_INT NUMBER_OF_MODELS							35
INT iNextEmptyModelEntry							= 0
STRUCT sModelInfo
	MODEL_NAMES modelname
	BOOL bNeeded
	BOOL bLoaded
ENDSTRUCT
sModelInfo ModelList[NUMBER_OF_MODELS]

//PURPOSE: Requests the model, then waits until it loads
PROC SAFE_REQUEST_MODEL(MODEL_NAMES thisModel, BOOL bWait, BOOL bSafely, BOOL bHideHudWhileWaiting = FALSE)
	//IF NOT HAS_MODEL_LOADED(thisModel)
		REQUEST_MODEL(thisModel)
				
		IF bWait
			WHILE NOT HAS_MODEL_LOADED(thisModel)
				IF bHideHudWhileWaiting
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
				ENDIF
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Assigns a model an entry in the need list
PROC ADD_MODEL_TO_NEED_LIST(MODEL_NAMES thisModelName)
	If iNextEmptyModelEntry < NUMBER_OF_MODELS
		ModelList[iNextEmptyModelEntry].modelname = thisModelName
		ModelList[iNextEmptyModelEntry].bNeeded = FALSE
		ModelList[iNextEmptyModelEntry].bLoaded = FALSE
		iNextEmptyModelEntry++
	ELSE
		SCRIPT_ASSERT("Too many models added to list")
	ENDIF
ENDPROC

//PURPOSE: Finds the model entry in the need list
FUNC INT FIND_NEED_LIST_MODEL_ENTRY(MODEL_NAMES thisModelName)
	INT iCorrectModelEntry = -1
	BOOL bSearchDone = FALSE
	
	INT iLoadCounter = 0
	WHILE NOT bSearchDone
		IF ModelList[iLoadCounter].modelname = thisModelName
			bSearchDone = TRUE
			iCorrectModelEntry = iLoadCounter
		ELSE
			iLoadCounter++
			IF iLoadCounter >= iNextEmptyModelEntry
				bSearchDone = TRUE
				SCRIPT_ASSERT("Model not found")
			ENDIF			
		ENDIF
	ENDWHILE
	
	RETURN iCorrectModelEntry
ENDFUNC

//PURPOSE: Sets the model's need list entry as needed; will be loaded with next call of "MANAGE_LOADING_NEEDED_LIST"
PROC SET_MODEL_AS_NEEDED(MODEL_NAMES thisModelName)
	INT iThisEntry = FIND_NEED_LIST_MODEL_ENTRY(thisModelName)
	IF iThisEntry <> -1
		ModelList[iThisEntry].bNeeded = TRUE
	ENDIF
ENDPROC

//PURPOSE: Immediately loads a model in the need list, setting it as loaded and needed
PROC IMMEDIATELY_LOAD_NEED_LIST_MODEL(MODEL_NAMES thisModelName, BOOL bHideHudWhileWaiting = FALSE)
	INT iThisEntry = FIND_NEED_LIST_MODEL_ENTRY(thisModelName)
	IF iThisEntry <> -1
		SAFE_REQUEST_MODEL(ModelList[iThisEntry].modelname, TRUE, TRUE, bHideHudWhileWaiting)
		ModelList[iThisEntry].bNeeded = TRUE
		ModelList[iThisEntry].bLoaded = TRUE
	ENDIF
ENDPROC

//PURPOSE: Immediately unloads a model in the need list, setting it as not loaded and not needed
PROC IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(MODEL_NAMES thisModelName)
	INT iThisEntry = FIND_NEED_LIST_MODEL_ENTRY(thisModelName)
	IF iThisEntry <> -1
		SET_MODEL_AS_NO_LONGER_NEEDED(ModelList[iThisEntry].modelname)
		ModelList[iThisEntry].bNeeded = FALSE
		ModelList[iThisEntry].bLoaded = FALSE
	ENDIF
ENDPROC

//PURPOSE: Requests a model in the need list, setting it as needed and starts non-blocking loading
PROC PRELOAD_NEED_LIST_MODEL(MODEL_NAMES thisModelName)
	INT iThisEntry = FIND_NEED_LIST_MODEL_ENTRY(thisModelName)
	IF iThisEntry <> -1
		IF NOT ModelList[iThisEntry].bLoaded
			SAFE_REQUEST_MODEL(ModelList[iThisEntry].modelname, FALSE, FALSE)
			ModelList[iThisEntry].bNeeded = TRUE
		ENDIF
	ENDIF
ENDPROC


//═════════════════════════════════╡ ANIM DICTS ╞═══════════════════════════════════
CONST_INT NUMBER_OF_ANIM_DICTS						50
INT iNextEmptyAnimDictEntry							= 0
STRUCT sAnimDictInfo
	STRING animdict
	BOOL bNeeded
	BOOL bLoaded
ENDSTRUCT
sAnimDictInfo AnimDictList[NUMBER_OF_ANIM_DICTS]

//PURPOSE: Requests the anim dict, then waits until it loads
PROC SAFE_REQUEST_ANIM_DICT(STRING thisDict, BOOL bWait, BOOL bSafely, BOOL bHideHudWhileWaiting = FALSE)
	//IF NOT HAS_ANIM_DICT_LOADED(thisDict)
		REQUEST_ANIM_DICT(thisDict)
		IF bWait
			WHILE NOT HAS_ANIM_DICT_LOADED(thisDict)
				IF bHideHudWhileWaiting
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
				ENDIF			
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Assigns an anim dict an entry in the need list
PROC ADD_ANIM_DICT_TO_NEED_LIST(STRING thisDict)
	If iNextEmptyAnimDictEntry < NUMBER_OF_ANIM_DICTS
		AnimDictList[iNextEmptyAnimDictEntry].animdict = thisDict
		AnimDictList[iNextEmptyAnimDictEntry].bNeeded = FALSE
		AnimDictList[iNextEmptyAnimDictEntry].bLoaded = FALSE
		iNextEmptyAnimDictEntry++
	ELSE
		SCRIPT_ASSERT("Too many anim dicts added to list")
	ENDIF
ENDPROC

//PURPOSE: Finds the anim dict entry in the need list
FUNC INT FIND_NEED_LIST_ANIM_DICT_ENTRY(STRING thisDict)
	INT iCorrectAnimDictEntry = -1
	BOOL bSearchDone = FALSE
	
	INT iLoadCounter = 0
	WHILE NOT bSearchDone
		IF ARE_STRINGS_EQUAL(AnimDictList[iLoadCounter].animdict, thisDict)
			bSearchDone = TRUE
			iCorrectAnimDictEntry = iLoadCounter
		ELSE
			iLoadCounter++
			IF iLoadCounter >= iNextEmptyAnimDictEntry
				bSearchDone = TRUE
				SCRIPT_ASSERT("Anim dictionary not found")
			ENDIF			
		ENDIF
	ENDWHILE
	
	RETURN iCorrectAnimDictEntry
ENDFUNC

//PURPOSE: Sets the anim dict's need list entry as needed; will be loaded with next call of "MANAGE_LOADING_NEEDED_LIST"
PROC SET_ANIM_DICT_AS_NEEDED(STRING thisDict)
	INT iThisEntry = FIND_NEED_LIST_ANIM_DICT_ENTRY(thisDict)
	IF iThisEntry <> -1
		AnimDictList[iThisEntry].bNeeded = TRUE
	ENDIF
ENDPROC

//PURPOSE: Immediately loads an anim dict in the need list, setting it as loaded and needed
PROC IMMEDIATELY_LOAD_NEED_LIST_ANIM_DICT(STRING thisDict)
	INT iThisEntry = FIND_NEED_LIST_ANIM_DICT_ENTRY(thisDict)
	IF iThisEntry <> -1
		SAFE_REQUEST_ANIM_DICT(AnimDictList[iThisEntry].animdict, TRUE, TRUE)
		AnimDictList[iThisEntry].bNeeded = TRUE
		AnimDictList[iThisEntry].bLoaded = TRUE
	ENDIF
ENDPROC

//PURPOSE: Immediately unloads an anim dict in the need list, setting it as not loaded and not needed
PROC IMMEDIATELY_UNLOAD_NEED_LIST_ANIM_DICT(STRING thisDict)
	INT iThisEntry = FIND_NEED_LIST_ANIM_DICT_ENTRY(thisDict)
	IF iThisEntry <> -1
		REMOVE_ANIM_DICT(AnimDictList[iThisEntry].animdict)
		AnimDictList[iThisEntry].bNeeded = FALSE
		AnimDictList[iThisEntry].bLoaded = FALSE
	ENDIF
ENDPROC

//PURPOSE: Requests an anim dictionary in the need list, setting it as needed and starts non-blocking loading
PROC PRELOAD_NEED_LIST_ANIM_DICT(STRING thisDict)
	INT iThisEntry = FIND_NEED_LIST_ANIM_DICT_ENTRY(thisDict)
	IF iThisEntry <> -1
		IF NOT AnimDictList[iThisEntry].bLoaded
			SAFE_REQUEST_ANIM_DICT(AnimDictList[iThisEntry].animdict, FALSE, FALSE)
			AnimDictList[iThisEntry].bNeeded = TRUE
		ENDIF
	ENDIF
ENDPROC


//═════════════════════════════════╡ VEHICLE RECORDINGS ╞═══════════════════════════════════
CONST_INT NUMBER_OF_VEHICLE_RECORDINGS				19
INT iNextEmptyVehRecEntry							= 0
STRUCT sVehRecInfo
	STRING vehrec
	INT vehrecno
	BOOL bNeeded
	BOOL bLoaded
ENDSTRUCT
sVehRecInfo VehRecList[NUMBER_OF_VEHICLE_RECORDINGS]

//PURPOSE: Requests the vehicle recording, then waits until it loads
PROC SAFE_REQUEST_VEHICLE_RECORDING(INT thisRecNumber, STRING thisRecName, BOOL bWait, BOOL bSafely, BOOL bHideHudWhileWaiting = FALSE)	
	//IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(thisRecNumber, thisRecName)
		PRINTNL() PRINTLN("\nRequesting vehicle rec: ", thisRecNumber, thisRecName)
		REQUEST_VEHICLE_RECORDING(thisRecNumber, thisRecName)
		IF bWait
			WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(thisRecNumber, thisRecName)
				IF bHideHudWhileWaiting
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
				ENDIF
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Assigns a vehicle recording an entry in the need list
PROC ADD_VEHICLE_RECORDING_TO_NEED_LIST(STRING thisRec, INT thisRecNo)
	If iNextEmptyVehRecEntry < NUMBER_OF_VEHICLE_RECORDINGS
		VehRecList[iNextEmptyVehRecEntry].vehrec = thisRec
		VehRecList[iNextEmptyVehRecEntry].vehrecno = thisRecNo
		VehRecList[iNextEmptyVehRecEntry].bNeeded = FALSE
		VehRecList[iNextEmptyVehRecEntry].bLoaded = FALSE
		iNextEmptyVehRecEntry++
	ELSE
		SCRIPT_ASSERT("Too many anim dicts added to list")
	ENDIF
ENDPROC

//PURPOSE: Finds the vehicle recording entry in the need list
FUNC INT FIND_NEED_LIST_VEHICLE_RECORDING_ENTRY(STRING thisRec, INT thisRecNo)
	INT iCorrectVehRecEntry = -1
	BOOL bSearchDone = FALSE
	
	INT iLoadCounter = 0
	WHILE NOT bSearchDone
		IF ARE_STRINGS_EQUAL(VehRecList[iLoadCounter].vehrec, thisRec)
		AND VehRecList[iLoadCounter].vehrecno = thisRecNo
			bSearchDone = TRUE
			iCorrectVehRecEntry = iLoadCounter
		ELSE
			iLoadCounter++
			IF iLoadCounter >= iNextEmptyVehRecEntry
				bSearchDone = TRUE
				PRINTLN("Vehicle Recording not found", thisRec, thisRecNo)
				SCRIPT_ASSERT("Vehicle Recording not found")
			ENDIF			
		ENDIF
	ENDWHILE
	
	RETURN iCorrectVehRecEntry
ENDFUNC

//PURPOSE: Sets the vehicle recording's need list entry as needed; will be loaded with next call of "MANAGE_LOADING_NEEDED_LIST"
PROC SET_VEHICLE_RECORDING_AS_NEEDED(STRING thisRec, INT thisRecNo)
	INT iThisEntry = FIND_NEED_LIST_VEHICLE_RECORDING_ENTRY(thisRec, thisRecNo)
	IF iThisEntry <> -1
		VehRecList[iThisEntry].bNeeded = TRUE
	ENDIF
ENDPROC

//PURPOSE: Immediately loads a vehicle recording in the need list, setting it as loaded and needed
PROC IMMEDIATELY_LOAD_NEED_LIST_VEHICLE_RECORDING(STRING thisRec, INT thisRecNo)
	INT iThisEntry = FIND_NEED_LIST_VEHICLE_RECORDING_ENTRY(thisRec, thisRecNo)
	IF iThisEntry <> -1
		SAFE_REQUEST_VEHICLE_RECORDING(VehRecList[iThisEntry].vehrecno, VehRecList[iThisEntry].vehrec, TRUE, TRUE)
		VehRecList[iThisEntry].bNeeded = TRUE
		VehRecList[iThisEntry].bLoaded = TRUE
	ENDIF
ENDPROC

//PURPOSE: Immediately unloads a vehicle recording in the need list, setting it as not loaded and not needed
PROC IMMEDIATELY_UNLOAD_NEED_LIST_VEHICLE_RECORDING(STRING thisRec, INT thisRecNo)
	INT iThisEntry = FIND_NEED_LIST_VEHICLE_RECORDING_ENTRY(thisRec, thisRecNo)
	IF iThisEntry <> -1
		REMOVE_VEHICLE_RECORDING(VehRecList[iThisEntry].vehrecno, VehRecList[iThisEntry].vehrec)
		VehRecList[iThisEntry].bNeeded = FALSE
		VehRecList[iThisEntry].bLoaded = FALSE
	ENDIF
ENDPROC

//PURPOSE: Requests a vehicle in the need list, setting it as needed and starts non-blocking loading
PROC PRELOAD_NEED_LIST_VEHICLE_RECORDING(STRING thisRec, INT thisRecNo)
	INT iThisEntry = FIND_NEED_LIST_VEHICLE_RECORDING_ENTRY(thisRec, thisRecNo)
	IF iThisEntry <> -1
		IF NOT VehRecList[iThisEntry].bLoaded
			SAFE_REQUEST_VEHICLE_RECORDING(VehRecList[iThisEntry].vehrecno, VehRecList[iThisEntry].vehrec, FALSE, FALSE)
			VehRecList[iThisEntry].bNeeded = TRUE
		ENDIF
	ENDIF
ENDPROC


//PURPOSE: Cleans up uber but doesn't leave references to the set piece bikes
PROC CLEANUP_UBER_PLAYBACK_AND_BIKES(BOOL bDelete = FALSE)

	SetPieceCarID[0]  = NULL
	//SetPieceCarID[1]  = NULL
	CLEANUP_UBER_PLAYBACK(bDelete)

	INT i
	FOR i = 0 to TOTAL_NUMBER_OF_SET_PIECE_CARS -1
		SetPieceCarCopCreated[i] = FALSE
	ENDFOR
	
//	STOP_CAM_PLAYBACK(cameraCar)
//	STOP_CAM_PLAYBACK(cameraCarCamera)
	
ENDPROC


//═════════════════════════════════╡ NEED LIST CONTROLS ╞═══════════════════════════════════
//PURPOSE: Resets/clears out the model list, replacing all models as DUMMY_MODEL_FOR_SCRIPT
PROC CLEAR_NEED_LIST()
	INT iLoadCounter
	FOR iLoadCounter = 0 to NUMBER_OF_MODELS-1
		ModelList[iLoadCounter].modelname = DUMMY_MODEL_FOR_SCRIPT
		ModelList[iLoadCounter].bNeeded = FALSE
		ModelList[iLoadCounter].bLoaded = FALSE
	ENDFOR
	iNextEmptyModelEntry = 0
	FOR iLoadCounter = 0 to NUMBER_OF_ANIM_DICTS-1
		AnimDictList[iLoadCounter].animdict = "missing"
		AnimDictList[iLoadCounter].bNeeded = FALSE
		AnimDictList[iLoadCounter].bLoaded = FALSE
	ENDFOR
	iNextEmptyAnimDictEntry = 0
	FOR iLoadCounter = 0 to NUMBER_OF_VEHICLE_RECORDINGS-1
		VehRecList[iLoadCounter].vehrec = "missing"
		VehRecList[iLoadCounter].vehrecno = -1
		VehRecList[iLoadCounter].bNeeded = FALSE
		VehRecList[iLoadCounter].bLoaded = FALSE
	ENDFOR
	iNextEmptyVehRecEntry = 0
ENDPROC

//PURPOSE: Resets all the entries in the model list to not needed
PROC RESET_NEED_LIST()
	INT iLoadCounter
	FOR iLoadCounter = 0 to iNextEmptyModelEntry-1
		ModelList[iLoadCounter].bNeeded = FALSE
	ENDFOR
	FOR iLoadCounter = 0 to iNextEmptyAnimDictEntry-1
		AnimDictList[iLoadCounter].bNeeded = FALSE
	ENDFOR
	FOR iLoadCounter = 0 to iNextEmptyVehRecEntry-1
		VehRecList[iLoadCounter].bNeeded = FALSE
	ENDFOR
ENDPROC

//PURPOSE: Checks needed of each entry- if needed but not loaded, loads. If loaded but but not needed, unloads.
PROC MANAGE_LOADING_NEED_LIST(BOOL bSafe = TRUE, BOOL bHideRadar = FALSE)
	INT iLoadCounter
	FOR iLoadCounter = 0 to iNextEmptyModelEntry-1
		IF ModelList[iLoadCounter].modelname <> DUMMY_MODEL_FOR_SCRIPT
			IF ModelList[iLoadCounter].bNeeded 
				IF NOT ModelList[iLoadCounter].bLoaded
					SAFE_REQUEST_MODEL(ModelList[iLoadCounter].modelname, TRUE, bSafe, bHideRadar)
					ModelList[iLoadCounter].bLoaded = TRUE
				ENDIF
			ELSE
				IF ModelList[iLoadCounter].bLoaded
					SET_MODEL_AS_NO_LONGER_NEEDED(ModelList[iLoadCounter].modelname)
					ModelList[iLoadCounter].bLoaded = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	FOR iLoadCounter = 0 to iNextEmptyAnimDictEntry-1
		IF NOT ARE_STRINGS_EQUAL(AnimDictList[iLoadCounter].animdict, "missing")
			IF AnimDictList[iLoadCounter].bNeeded 
				IF NOT AnimDictList[iLoadCounter].bLoaded
					SAFE_REQUEST_ANIM_DICT(AnimDictList[iLoadCounter].animdict, TRUE, bSafe, bHideRadar)
					AnimDictList[iLoadCounter].bLoaded = TRUE
				ENDIF
			ELSE
				IF AnimDictList[iLoadCounter].bLoaded
					REMOVE_ANIM_DICT(AnimDictList[iLoadCounter].animdict)
					AnimDictList[iLoadCounter].bLoaded = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	FOR iLoadCounter = 0 to iNextEmptyVehRecEntry-1
		IF NOT ARE_STRINGS_EQUAL(VehRecList[iLoadCounter].vehrec, "missing")
		AND VehRecList[iLoadCounter].vehrecno <> -1
			IF VehRecList[iLoadCounter].bNeeded 
				IF NOT VehRecList[iLoadCounter].bLoaded
					SAFE_REQUEST_VEHICLE_RECORDING(VehRecList[iLoadCounter].vehrecno, VehRecList[iLoadCounter].vehrec, TRUE, bSafe, bHideRadar)
					VehRecList[iLoadCounter].bLoaded = TRUE
				ENDIF
			ELSE
				IF VehRecList[iLoadCounter].bLoaded
					REMOVE_VEHICLE_RECORDING(VehRecList[iLoadCounter].vehrecno, VehRecList[iLoadCounter].vehrec)
					VehRecList[iLoadCounter].bLoaded = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC


//PURPOSE: Checks needed of each entry- if needed but not loaded, loads. If loaded but but not needed, unloads.
FUNC BOOL MANAGE_LOADING_NEED_LIST_SPEEDY()

	INT iLoadCounter
	
	FOR iLoadCounter = 0 to iNextEmptyModelEntry-1
		IF ModelList[iLoadCounter].modelname <> DUMMY_MODEL_FOR_SCRIPT
			IF ModelList[iLoadCounter].bNeeded 
				REQUEST_MODEL(ModelList[iLoadCounter].modelname)
				//PRINTLN("Request Model: ", ModelList[iLoadCounter].modelname)
				ModelList[iLoadCounter].bLoaded = TRUE
			ELSE
				IF ModelList[iLoadCounter].bLoaded
					SET_MODEL_AS_NO_LONGER_NEEDED(ModelList[iLoadCounter].modelname)
					ModelList[iLoadCounter].bLoaded = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iLoadCounter = 0 to iNextEmptyAnimDictEntry-1
		IF NOT ARE_STRINGS_EQUAL(AnimDictList[iLoadCounter].animdict, "missing")
			IF AnimDictList[iLoadCounter].bNeeded 
				REQUEST_ANIM_DICT(AnimDictList[iLoadCounter].animdict)
				//PRINTLN("Requesting Anims: ", AnimDictList[iLoadCounter].animdict)
				AnimDictList[iLoadCounter].bLoaded = TRUE
			ELSE
				IF AnimDictList[iLoadCounter].bLoaded
					REMOVE_ANIM_DICT(AnimDictList[iLoadCounter].animdict)
					AnimDictList[iLoadCounter].bLoaded = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iLoadCounter = 0 to iNextEmptyVehRecEntry-1
		IF NOT ARE_STRINGS_EQUAL(VehRecList[iLoadCounter].vehrec, "missing")
		AND VehRecList[iLoadCounter].vehrecno <> -1
			IF VehRecList[iLoadCounter].bNeeded 
				REQUEST_VEHICLE_RECORDING(VehRecList[iLoadCounter].vehrecno, VehRecList[iLoadCounter].vehrec)
				//PRINTLN("Request Recs: ", VehRecList[iLoadCounter].vehrecno, VehRecList[iLoadCounter].vehrec)
				VehRecList[iLoadCounter].bLoaded = TRUE
			ELSE
				IF VehRecList[iLoadCounter].bLoaded
					REMOVE_VEHICLE_RECORDING(VehRecList[iLoadCounter].vehrecno, VehRecList[iLoadCounter].vehrec)
					VehRecList[iLoadCounter].bLoaded = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	IF IS_SCREEN_FADED_OUT()
		LOAD_ALL_OBJECTS_NOW()
	ENDIF
	
	FOR iLoadCounter = 0 to iNextEmptyModelEntry-1
		IF ModelList[iLoadCounter].bLoaded = TRUE
			PRINTLN("Checking..Models: ", ModelList[iLoadCounter].modelname)
			IF NOT HAS_MODEL_LOADED(ModelList[iLoadCounter].modelname)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iLoadCounter = 0 to iNextEmptyAnimDictEntry-1
		IF AnimDictList[iLoadCounter].bLoaded = TRUE
			PRINTLN("Checking..Anims: ", AnimDictList[iLoadCounter].animdict)
			IF NOT HAS_ANIM_DICT_LOADED(AnimDictList[iLoadCounter].animdict)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iLoadCounter = 0 to iNextEmptyVehRecEntry-1
		IF VehRecList[iLoadCounter].bLoaded = TRUE
			PRINTLN("Checking..Recs: ", VehRecList[iLoadCounter].vehrecno, VehRecList[iLoadCounter].vehrec)
			IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(VehRecList[iLoadCounter].vehrecno, VehRecList[iLoadCounter].vehrec)
				RETURN FALSE
			ENDIF
		ENDIF
	ENDFOR
		
	//If we made it down to here all the models have loaded...
	RETURN TRUE
	
ENDFUNC


//═════════════════════════════════╡ SAFE REQUESTS ╞═══════════════════════════════════
//PURPOSE: Requests the ptfx asset, then waits until it loads
PROC SAFE_REQUEST_PTFX_ASSET(BOOL bWait, BOOL bSafely)
	//IF NOT HAS_PTFX_ASSET_LOADED(thisPTFXAsset)
		REQUEST_PTFX_ASSET()
		IF bWait
			WHILE NOT HAS_PTFX_ASSET_LOADED()
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF	
ENDPROC

//PURPOSE: Requests the heist crew models, then waits until it loads
PROC SAFE_REQUEST_MODELS_FOR_HEIST_CREW(INT thisHeistIndex, BOOL bWait, BOOL bSafely)
	//IF NOT HAVE_MODELS_LOADED_FOR_HEIST_CREW(thisHeistIndex)
		REQUEST_MODELS_FOR_HEIST_CREW(thisHeistIndex)
		IF bWait
			WHILE NOT HAVE_MODELS_LOADED_FOR_HEIST_CREW(thisHeistIndex)
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Requests the heist crew member models, then waits until it loads
PROC SAFE_REQUEST_MODEL_FOR_HEIST_CREW_MEMBER(INT thisHeistIndex, INT thisSlotIndex, BOOL bWait, BOOL bSafely)
	//IF NOT HAS_MODEL_LOADED_FOR_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(thisHeistIndex, thisSlotIndex))
		REQUEST_MODEL_FOR_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(thisHeistIndex, thisSlotIndex))
		IF bWait
			WHILE NOT HAS_MODEL_LOADED_FOR_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(thisHeistIndex, thisSlotIndex))
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC
		
//PURPOSE: Requests the additional text, then waits until it loads
PROC SAFE_REQUEST_ADDITIONAL_TEXT(STRING thisTextBlockName, TEXT_BLOCK_SLOTS thisSlotNumber, BOOL bWait, BOOL bSafely)
	//IF NOT HAS_THIS_ADDITIONAL_TEXT_LOADED(thisTextBlockName, thisSlotNumber)
		REQUEST_ADDITIONAL_TEXT(thisTextBlockName, thisSlotNumber)
		IF bWait
			WHILE NOT HAS_THIS_ADDITIONAL_TEXT_LOADED(thisTextBlockName, thisSlotNumber)
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Requests the interior at coords, then waits until it loads
PROC SAFE_REQUEST_INTERIOR_AT_COORDS(INTERIOR_INSTANCE_INDEX &storeInteriorIndex, VECTOR theseCoords, BOOL bWait, BOOL bSafely)
	storeInteriorIndex = GET_INTERIOR_AT_COORDS(theseCoords)
		
	//IF NOT IS_INTERIOR_READY(storeInteriorIndex)
		PIN_INTERIOR_IN_MEMORY(storeInteriorIndex)
		IF bWait
			WHILE NOT IS_INTERIOR_READY(storeInteriorIndex)
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Requests the interior at coords, then waits until it loads
PROC SAFE_REQUEST_INTERIOR_AT_COORDS_WITH_TYPE(	INTERIOR_INSTANCE_INDEX &storeInteriorIndex, VECTOR theseCoords, STRING thisName,
												BOOL bWait, BOOL bSafely)
	storeInteriorIndex = GET_INTERIOR_AT_COORDS_WITH_TYPE(theseCoords, thisName)
		
	//IF NOT IS_INTERIOR_READY(storeInteriorIndex)
		PIN_INTERIOR_IN_MEMORY(storeInteriorIndex)
		IF bWait
			WHILE NOT IS_INTERIOR_READY(storeInteriorIndex)
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Requests the ambient audio bank, then waits until it loads
PROC SAFE_REQUEST_AMBIENT_AUDIO_BANK(STRING thisBankName, BOOL bWait, BOOL bSafely)
	//IF NOT REQUEST_AMBIENT_AUDIO_BANK(thisBankName)
		REQUEST_AMBIENT_AUDIO_BANK(thisBankName) 
		IF bWait
			WHILE NOT REQUEST_AMBIENT_AUDIO_BANK(thisBankName) 
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Requests the mocap cutscene, then waits until it loads
PROC SAFE_REQUEST_MOCAP_CUTSCENE(STRING thisMocap, BOOL bWait, BOOL bSafely)
	//IF NOT HAS_CUTSCENE_LOADED()
		REQUEST_CUTSCENE(thisMocap)
		IF bWait
			WHILE NOT HAS_CUTSCENE_LOADED()
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Requests the mocap custscene with playback list, then waits until it loads
PROC SAFE_REQUEST_MOCAP_CUTSCENE_WITH_PLAYBACK_LIST(STRING thisMocap, CUTSCENE_SECTION theseSections, BOOL bWait, BOOL bSafely)
	//IF NOT HAS_CUTSCENE_LOADED()
		REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(thisMocap, theseSections)
		IF bWait
			WHILE NOT HAS_CUTSCENE_LOADED()
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	//ENDIF
ENDPROC

//PURPOSE: Requests the scaleform, then waits until it loads
PROC SAFE_REQUEST_SCALEFORM_MOVIE(SCALEFORM_INDEX &storeScaleform, STRING thisScaleform, BOOL bWait, BOOL bSafely)
	IF NOT HAS_SCALEFORM_MOVIE_LOADED(storeScaleform)
		storeScaleform = REQUEST_SCALEFORM_MOVIE(thisScaleform)
		IF bWait
			WHILE NOT HAS_SCALEFORM_MOVIE_LOADED(storeScaleform)
				IF bSafely
					safeWait(0)
				ELSE
					WAIT(0)
				ENDIF
			ENDWHILE
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Requests the weapon asset, then waits until it loads
PROC SAFE_REQUEST_WEAPON_ASSET(WEAPON_TYPE thisWeapon, BOOL bWait, BOOL bSafely, EXTRA_WEAPON_COMPONENT_RESOURCE_FLAGS weaponComponent = WEAPON_COMPONENT_NONE)
	REQUEST_WEAPON_ASSET(thisWeapon, ENUM_TO_INT(WRF_REQUEST_ALL_ANIMS), weaponComponent)
	IF bWait
		WHILE NOT HAS_WEAPON_ASSET_LOADED(thisWeapon)
			IF bSafely
				safeWait(0)
			ELSE
				WAIT(0)
			ENDIF
		ENDWHILE
	ENDIF
ENDPROC

//PURPOSE:	Safely adds the ped to dialogue, checking if that ped is dead. if so, adds the voice ID as NULL
PROC SAFE_ADD_PED_FOR_DIALOGUE(PED_INDEX thisPed, INT thisNumberID, STRING thisVoiceID)
	IF DOES_ENTITY_EXIST(thisPed)
	AND NOT IS_PED_INJURED(thisPed)
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, thisNumberID, thisPed, thisVoiceID)
	ELSE
		REMOVE_PED_FOR_DIALOGUE(myScriptedSpeech, thisNumberID)
		ADD_PED_FOR_DIALOGUE(myScriptedSpeech, thisNumberID, NULL, thisVoiceID)
	ENDIF
ENDPROC


//═════════════════════════════════╡ CUTSCENE ╞═══════════════════════════════════
//═════════╡ CAMERAS ╞═════════
CAMERA_INDEX initialCam, destinationCam, camAnim
BOOL bCutsceneStarted								= FALSE

PROC RESET_GAME_CAMERA()

	SET_GAMEPLAY_CAM_RELATIVE_HEADING()
	SET_GAMEPLAY_CAM_RELATIVE_PITCH()
	
ENDPROC


//═════════╡ FUNCTIONS ╞═════════
//PURPOSE:		Performs actions required to start all cutscenes
PROC DO_START_CUTSCENE(BOOL bClearPlayerTasks = TRUE, BOOL bAnimatedCam = FALSE, BOOL bHidePlayerWeapon = TRUE, BOOL bHideRadar = TRUE, BOOL bSetMultiHeadBlinders = TRUE)
	IF NOT bCutsceneStarted
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE,DEFAULT,bSetMultiHeadBlinders)
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			DESTROY_ALL_CAMS()
			IF NOT bAnimatedCam
				initialCam	= CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
				destinationCam	= CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
				SET_CAM_COORD(initialCam, <<1,1,1>>)
			ELSE
				initialCam	= CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
				destinationCam	= CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
				camAnim = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", FALSE)
				SET_CAM_COORD(camAnim, <<1,1,1>>)
			ENDIF
			IF bHideRadar
				DISPLAY_HUD(FALSE)
				DISPLAY_RADAR(FALSE)
				SET_WIDESCREEN_BORDERS(TRUE, 500)
			ENDIF
			
			IF bClearPlayerTasks
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_CLEAR_TASKS)
			ELSE 
				SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
			ENDIF
			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), bHidePlayerWeapon)
		ENDIF
		SETTIMERB(0)
		bCutsceneStarted = TRUE
	ENDIF
ENDPROC

//PURPOSE:		Performs actions required to end all cutscenes
PROC DO_END_CUTSCENE(BOOL bClearPlayerTasks = TRUE, BOOL bInterpBackToGame = FALSE, INT iCamBlend = DEFAULT_INTERP_TO_FROM_GAME, BOOL bUseCatchUpNotInterp = TRUE, BOOL bSetMultiHeadBlinders = TRUE)
	IF bCutsceneStarted
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE, DEFAULT, bSetMultiHeadBlinders)
		DISPLAY_HUD(TRUE)
		DISPLAY_RADAR(TRUE)
		SET_WIDESCREEN_BORDERS(FALSE, 500)
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			IF DOES_CAM_EXIST(initialCam)
				SET_CAM_ACTIVE(initialCam, FALSE)
				DESTROY_CAM	(initialCam)
			ENDIF
			IF DOES_CAM_EXIST(destinationCam)
				SET_CAM_ACTIVE(destinationCam, FALSE)
				DESTROY_CAM	(destinationCam)
			ENDIF
			IF DOES_CAM_EXIST(camAnim)
				SET_CAM_ACTIVE(camAnim, FALSE)
				DESTROY_CAM	(camAnim)
			ENDIF
			
			IF NOT bInterpBackToGame
				RENDER_SCRIPT_CAMS(FALSE, FALSE, iCamBlend)
			ELSE
				IF bUseCatchUpNotInterp
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
				ELSE
					RENDER_SCRIPT_CAMS(FALSE, TRUE, iCamBlend)
				ENDIF
			ENDIF
			
			STOP_GAMEPLAY_CAM_SHAKING(TRUE)
			IF bClearPlayerTasks
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF 
			HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(PLAYER_PED_ID(), FALSE)
			IF NOT IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				IF NOT IS_ENTITY_ATTACHED_TO_ANY_OBJECT(PLAYER_PED_ID())
					FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				ENDIF
			ENDIF
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			bCutsceneStarted = FALSE
		ENDIF 
	ENDIF
ENDPROC

//PURPOSE:		Shakes both cameras at the same time
PROC doCamShake(STRING ShakeName, FLOAT fAmplitudeScalar = 1.0)
	SHAKE_CAM(initialCam, ShakeName, fAmplitudeScalar)
	SHAKE_CAM(destinationCam, ShakeName, fAmplitudeScalar)
ENDPROC

//PURPOSE:		Changes shake amplitude
PROC setCamShakeAmp(FLOAT fAmplitudeScalar = 1.0)
	SET_CAM_SHAKE_AMPLITUDE(initialCam, fAmplitudeScalar)
	SET_CAM_SHAKE_AMPLITUDE(destinationCam, fAmplitudeScalar)
ENDPROC

//PURPOSE:		Stops both cams shaking
PROC stopCamShake(BOOL bStopImmediately = FALSE)
	IF DOES_CAM_EXIST(initialCam)
		STOP_CAM_SHAKING(initialCam, bStopImmediately)
	ENDIF
	IF DOES_CAM_EXIST(destinationCam)
		STOP_CAM_SHAKING(destinationCam, bStopImmediately)
	ENDIF
ENDPROC

//PURPOSE: Moves a camera between Init and Dest, for as long as duration
PROC doTwoPointCam(	VECTOR initialCamCoord, VECTOR initialCamRot, FLOAT initialCamFOV, 
					VECTOR destinationCamCoord, VECTOR destinationCamRot, FLOAT destinationCamFOV,
					INT duration, CAMERA_GRAPH_TYPE GraphType = GRAPH_TYPE_LINEAR)
					
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
	DETACH_CAM(initialCam)
	STOP_CAM_POINTING(initialCam)
	SET_CAM_COORD(initialCam, initialCamCoord)
	SET_CAM_ROT(initialCam, initialCamRot)
	SET_CAM_FOV(initialCam, initialCamFOV)
	SET_CAM_ACTIVE(destinationCam, FALSE)
	SET_CAM_ACTIVE(initialCam, TRUE)

	DETACH_CAM(destinationCam)
	STOP_CAM_POINTING(destinationCam)
	SET_CAM_COORD(destinationCam, destinationCamCoord)
	SET_CAM_ROT(destinationCam, destinationCamRot)
	SET_CAM_FOV(destinationCam, destinationCamFOV)
	SET_CAM_ACTIVE(destinationCam, TRUE)
	SET_CAM_ACTIVE(initialCam, TRUE)
	
	SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, duration, GraphType, GraphType)
ENDPROC

//═════════════════════════════════╡ Sync Scene ╞═══════════════════════════════════

FUNC BOOL HAS_SYNCHRONIZED_SCENE_FINISHED(INT sceneID)
	IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneID)
		IF GET_SYNCHRONIZED_SCENE_PHASE(sceneID) >= 1.0
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC DESTROY_SYNCHRONIZED_SCENE(INT &sceneID)
	sceneID = -1
ENDPROC

//═════════════════════════════════╡ SAFE FADE ╞═══════════════════════════════════

//PURPOSE:		Only fades out if the screen is faded in
PROC SAFE_FADE_OUT(INT iTime = 800, BOOL bWait = FALSE, BOOL bSafely = FALSE)
	IF IS_SCREEN_FADED_IN()
	OR IS_SCREEN_FADING_IN()
		DO_SCREEN_FADE_OUT(iTime)
	ENDIF
	IF bWait
		WHILE IS_SCREEN_FADING_OUT()
			IF bSafely
				safeWait(0)
			ELSE
				WAIT(0)
			ENDIF
		ENDWHILE
	ENDIF
ENDPROC

//PURPOSE:		Only fades in if the screen is faded out
PROC SAFE_FADE_IN(INT iTime = 800, BOOL bWait = FALSE, BOOL bSafely = FALSE)
	IF IS_SCREEN_FADED_OUT()
	OR IS_SCREEN_FADING_OUT()
		//CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100.0, TRUE)
		RESET_GAME_CAMERA()
		//LOAD_SCENE(GET_ENTITY_COORDS(PLAYER_PED_ID()))
		DO_SCREEN_FADE_IN(iTime)
	ENDIF
	IF bWait
		WHILE IS_SCREEN_FADING_IN()
			IF bSafely
				safeWait(0)
			ELSE
				WAIT(0)
			ENDIF
		ENDWHILE
	ENDIF
ENDPROC

//═════════════════════════════════╡ TIDY UP ╞═══════════════════════════════════
PROC CLEANUP_PED(PED_INDEX &pedTidy, BOOL bImmediately = FALSE)
	IF DOES_ENTITY_EXIST(pedTidy)
		//IF NOT IS_PED_A_PLAYER(pedTidy)
			IF bImmediately
				/*IF NOT IS_PED_INJURED(pedTidy)
					IF IS_ENTITY_ATTACHED(pedTidy)
						DETACH_ENTITY(pedTidy, FALSE)
					ENDIF
				ENDIF*/
				IF NOT IS_PED_INJURED(pedTidy)
					IF NOT IS_PED_INJURED(pedTidy)
						IF NOT IS_ENTITY_A_MISSION_ENTITY(pedTidy)
							SET_ENTITY_AS_MISSION_ENTITY(pedTidy)
						ENDIF
					ENDIF
				ENDIF
				DELETE_PED(pedTidy)
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(pedTidy)
			ENDIF
		//ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_VEHICLE(VEHICLE_INDEX &vehTidy, BOOL bImmediately = FALSE)
	IF DOES_ENTITY_EXIST(vehTidy)
		IF bImmediately
			EMPTY_VEHICLE(vehTidy)
			IF IS_VEHICLE_DRIVEABLE(vehTidy)
				IF IS_ENTITY_ATTACHED(vehTidy)
					DETACH_ENTITY(vehTidy, FALSE)
				ENDIF
			ENDIF
			IF NOT IS_ENTITY_A_MISSION_ENTITY(vehTidy)
				SET_ENTITY_AS_MISSION_ENTITY(vehTidy)
			ENDIF
			DELETE_VEHICLE(vehTidy)
		ELSE
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehTidy)
		ENDIF
	ENDIF
ENDPROC

PROC CLEANUP_OBJECT(OBJECT_INDEX &objTidy, BOOL bImmediately = FALSE)
	IF DOES_ENTITY_EXIST(objTidy)
		IF bImmediately
			IF IS_ENTITY_ATTACHED(objTidy)
				DETACH_ENTITY(objTidy, FALSE)
			ENDIF
			IF NOT IS_ENTITY_A_MISSION_ENTITY(objTidy)
				SET_ENTITY_AS_MISSION_ENTITY(objTidy)
			ENDIF
			DELETE_OBJECT(objTidy)
		ELSE
			SET_OBJECT_AS_NO_LONGER_NEEDED(objTidy)
		ENDIF
	ENDIF
ENDPROC


//PURPOSE: Unloads all the models in the model list
PROC TIDY_UP_NEED_LIST()
	INT iNeedCounter
	FOR iNeedCounter = 0 to iNextEmptyModelEntry-1
		#IF IS_DEBUG_BUILD
			PRINTLN("TIDY_UP_NEED_LIST -> Model: ", GET_MODEL_NAME_FOR_DEBUG(ModelList[iNeedCounter].modelname))
		#ENDIF
		SET_MODEL_AS_NO_LONGER_NEEDED(ModelList[iNeedCounter].modelname)
		ModelList[iNeedCounter].bNeeded = FALSE
		ModelList[iNeedCounter].bLoaded = FALSE
	ENDFOR
	FOR iNeedCounter = 0 to iNextEmptyAnimDictEntry-1
		REMOVE_ANIM_DICT(AnimDictList[iNeedCounter].animdict)
		AnimDictList[iNeedCounter].bNeeded = FALSE
		AnimDictList[iNeedCounter].bLoaded = FALSE
	ENDFOR
	FOR iNeedCounter = 0 to iNextEmptyVehRecEntry-1
		REMOVE_VEHICLE_RECORDING(VehRecList[iNeedCounter].vehrecno, VehRecList[iNeedCounter].vehrec)
		VehRecList[iNeedCounter].bNeeded = FALSE
		VehRecList[iNeedCounter].bLoaded = FALSE
	ENDFOR
ENDPROC


//═════════════════════════════════╡ GAMEPLAY ╞═══════════════════════════════════
//PURPOSE:	Queries if a ped is currently doing a task
FUNC BOOL IS_TASK_ONGOING(PED_INDEX thisPed, SCRIPT_TASK_NAME thisTask)
	IF NOT IS_ENTITY_DEAD(thisPed)
		IF GET_SCRIPT_TASK_STATUS(thisPed, thisTask) = PERFORMING_TASK
			RETURN TRUE
		ELSE
			RETURN FALSE
		ENDIF
	ELSE
		RETURN FALSE
	ENDIF
ENDFUNC

//PURPOSE:	Gets the point on a line nearest to another point, and finds the distance between the two
FUNC FLOAT GET_DISTANCE_BETWEEN_LINE_AND_POINT_2D(VECTOR vLineCoordA, VECTOR vLineCoordB, VECTOR vPoint)
	FLOAT fA = vLineCoordA.y - vLineCoordB.y
	FLOAT fB = vLineCoordB.x - vLineCoordA.x
	FLOAT fC = (vLineCoordA.x * vLineCoordB.y) - (vLineCoordB.x * vLineCoordA.y)

	FLOAT fAnswer = ((fA*vPoint.x) + (fB * vPoint.y) + fC) / SQRT((fA*fA)+(fB*fB))
	
	RETURN SQRT(fAnswer*fAnswer)
ENDFUNC

//PURPOSE:	Checks if the peds is at a coord and a head, if not tasks the ped to achieve the coord and head
FUNC BOOL HAS_PED_ACHIEVED_COORD_HEAD(PED_INDEX thisPed, VECTOR vCoords, FLOAT fHeading, FLOAT fMoveBlendRatio, FLOAT fTargetDistance = 0.75, //4.0,
										FLOAT fAngleRange = 20.0)
										
	NAVDATA ndStruct
	
	ndStruct.m_fSlideToCoordHeading = fHeading
											
	VECTOR vModPedPos = GET_ENTITY_COORDS(thisPed, FALSE) + <<0, 0, -1>>	//Modify ped height
	
	IF NOT IS_ENTITY_DEAD(thisPed)
		IF NOT IS_TASK_ONGOING(thisPed, SCRIPT_TASK_PERFORM_SEQUENCE)
			FREEZE_ENTITY_POSITION(thisPed, FALSE)
			
			OPEN_SEQUENCE_TASK(seqSequence)
			
				TASK_FOLLOW_NAV_MESH_TO_COORD_ADVANCED(NULL, vCoords, fMoveBlendRatio, DEFAULT_TIME_BEFORE_WARP * 2, fTargetDistance, ENAV_ADV_SLIDE_TO_COORD_AND_ACHIEVE_HEADING_AT_END, ndStruct) //, fHeading)
			CLOSE_SEQUENCE_TASK(seqSequence)
			TASK_PERFORM_SEQUENCE(thisPed, seqSequence)
			CLEAR_SEQUENCE_TASK(seqSequence)
		ELSE
			IF GET_DISTANCE_BETWEEN_COORDS(vModPedPos, vCoords) < fTargetDistance
			AND IS_PED_CLOSE_TO_IDEAL_HEADING(thisPed, fHeading, fAngleRange)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	RETURN FALSE
		
ENDFUNC


//PURPOSE:	Checks if the peds is at a coord and a head, if not tasks the ped to achieve the coord and head
FUNC BOOL HAS_PED_ACHIEVED_COORD_AND_GOT_INTO_VEHICLE(PED_INDEX thisPed, VECTOR vCoords, VEHICLE_INDEX viThisCar, FLOAT fMoveBlendRatio, FLOAT fTargetDistance = 4.0)
	VECTOR vModPedPos = GET_ENTITY_COORDS(thisPed) + <<0, 0, -1>>	//Modify ped height
	IF IS_PED_IN_VEHICLE(thisPed, viThisCar)
		RETURN TRUE
	ELSE
		IF IS_VEHICLE_SEAT_FREE(viThisCar, VS_DRIVER)
			IF NOT IS_TASK_ONGOING(thisPed, SCRIPT_TASK_PERFORM_SEQUENCE)
				IF NOT IS_ENTITY_ATTACHED(thisPed)
					FREEZE_ENTITY_POSITION(thisPed, FALSE)
				ENDIF
				OPEN_SEQUENCE_TASK(seqSequence)
					IF GET_DISTANCE_BETWEEN_COORDS(vModPedPos, vCoords) >= fTargetDistance
						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vCoords, fMoveBlendRatio)
					ENDIF
					TASK_ENTER_VEHICLE(NULL, viThisCar)
				CLOSE_SEQUENCE_TASK(seqSequence)
				TASK_PERFORM_SEQUENCE(thisPed, seqSequence)
				CLEAR_SEQUENCE_TASK(seqSequence)
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


//                _ 
//               | |
//  ___ _ __   __| |
// / _ \ '_ \ / _` |
//|  __/ | | | (_| |
// \___|_| |_|\__,_|	end common functions for David (POST FAILCHECK)



//═════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
//════════════════════════════════════════════╡ ASSET AND CONTROL ASSET LISTS ╞════════════════════════════════════════════════
//═════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════


STRUCT AssetList
	MODEL_NAMES mnBike
	MODEL_NAMES mnCar
	MODEL_NAMES mnVan
	MODEL_NAMES mnSecurity
	MODEL_NAMES mnShop[NUMBER_OF_STORE_PEDS]
	MODEL_NAMES mnBackroom
	MODEL_NAMES mnGrenade
	MODEL_NAMES mnTruck
	MODEL_NAMES mnTrain
	MODEL_NAMES mnPolice
	MODEL_NAMES mnPoliceman
	MODEL_NAMES mnPhone
	MODEL_NAMES mnBag
	MODEL_NAMES mnJewels1
	MODEL_NAMES mnJewels2
	MODEL_NAMES mnJewels4a
	MODEL_NAMES mnJewels4b
	MODEL_NAMES mnLester
	MODEL_NAMES mnLestersStick
	MODEL_NAMES mnGunman
	MODEL_NAMES mnDriver
	MODEL_NAMES mnHacker
			
	STRING adGestures
		STRING animDoIt
		STRING animTime
		STRING animFoldArms
		STRING animImTellingYou
		STRING animWhy
		STRING animDamn
		STRING animDespair
	STRING adJewelHeist
		STRING animCamSmashD
		STRING animSmashE
		STRING animCamSmashE
		STRING animSmashNecklace
		STRING animCamSmashNecklace
		STRING animSmashNecklaceSkull
		STRING animSmashTrayA
		STRING animCamSmashTrayA
		STRING animSmashTrayB
		STRING animCamSmashTrayB
		STRING animGasAssistant
		STRING animGasSecurity
		STRING animCowerOnFloor
		STRING animGas[4]
		STRING animManageressEnter
		STRING animManageressExit
		STRING animManageressKneelIntro
		STRING animManageressKneelLoop
		STRING animManageressPanicLoop
		STRING animCamManageressEnter
	STRING adJewelHeistSwitchCam
	STRING adWatch
		STRING animWatch
	
		STRING animTruckMic
		STRING animTruckBuddy
		
	STRING recBikeExit[2]
	STRING recBikeFlee[3]
	STRING recBikeMount[3]
	STRING recCopTrap[4]
	
	//STRING recTruck
	STRING recTruckExit
	STRING recTruckLead
	STRING recUber
		
	INT recNumber
	INT recNoGoodBikeChase
	
	INT recNoFinalTrain
	
	INT skipBikeChaseStart
	INT skipFirstCrossroadEnd
	INT skipBigTurnStart
	INT skipBigTurnMid
	INT skipBigTurnEnd
	INT skipSecondCrossroadStart
	INT skipSecondCrossroadEnd
	INT skipThirdCrossroadStart
	INT skipThirdCrossroadEnd
	INT skipSlowdownForJumpTurn
	INT skipJumpTurnStart
	INT skipJumpTurnEnd
	INT skipJump
	INT skipJumpCrash
	INT skipJumpLand
	INT skipTunnelEnter
	INT skipTunnelJump
	INT skipTunnelJumpLand
	INT skipTunnelCylinderStart
	INT skipTunnelCylinderEnd
	INT skipTunnelUnderRoadStart
	INT skipTunnelUnderRoadEnd
	INT skipTunnelRampStart
	INT skipTunnelRampEnd
	INT skipTunnelSewerStart
	INT skipTunnelSewerEnd
	INT skipTunnelHelterStart
	INT skipTunnelHelterEnd
	INT skipTrainTrackStart
	INT skipTrainTrackEnd
	INT skipStormDrain
	INT skipBikeChaseEnd
		
	STRING mocapIntroA
	STRING mocapIntroB
	STRING mocapRooftopGasKO
	STRING mocapFrontDoorSmash
	STRING mocapBagThrowA
	STRING mocapBagThrowB
		
	STRING convStealthPlan
	STRING convCarLeave
	STRING convMove
	STRING convTrap
	STRING convMichaelTruck
	STRING convMountTruck
	STRING convRoof
	STRING convHackerA10
	STRING convHackerA20
	STRING convHackerA30
	STRING convHackerA40
	STRING convHackerA50
	STRING convHackerA60
	STRING convHackerB10
	STRING convHackerB20
	STRING convHackerB30
	STRING convHackerB40
	STRING convHackerAStart
	STRING convHackerBStart
	STRING convHackerATimeUp
	STRING convHackerBTimeUp
	STRING convHackerPoliceOnWay
	STRING convMichaelEncouragement
	
	STRING convBikeCrash
	STRING convPolice2
	STRING convLostThem
	STRING convPolice3
	STRING convCollateral
	STRING convGetDown
	STRING convManagerDown
	STRING convFollowMichael
	STRING convFollowFranklin
	STRING convManager
	STRING convManagerFail
	STRING convRunAway
	STRING convChaseHardLeft
	STRING convChaseJump
	STRING convChaseTunnelEntry
	STRING convChaseTunnelRamp
	STRING convShoutAtCowerPed
	STRING convShoutAtKOPed
	STRING convYouDrive
	STRING convYoureDrivingRight
	STRING convComeOnBoys
	STRING convJesusFranklin
	STRING convSlowDownFranklin
	STRING convSpeedUpFranklin
	STRING convGetBehindMe
	STRING convSpeedUpMichael
	STRING convSlowDownMichael
	STRING convStealthPlan2
	STRING convRoofClimbStart
	STRING convAggressivePlan2
	STRING convStopGuard
	STRING convLeaveGoodBad
	STRING convLeaveGoodGood
	STRING convLeaveGoodMedium
	STRING convLeaveMediumBad
	STRING convLeaveBadBad
	STRING convCopsIncomingGood1
	STRING convCopsIncomingGood2
	STRING convCopsIncomingMedium
	STRING convCopsIncomingBad1
	STRING convCopsIncomingBad2
	STRING convRoofClear
	STRING convShootCops
	STRING convCrewResponds
	STRING convCrewCopReact1
	STRING convCrewCopReact2
	STRING convFranklinEncouragementOpen
	STRING convFranklinEncouragementTunnel
	STRING convCrewFranklinRespond1
	STRING convCrewFranklinRespond2
	STRING convShootHim
	STRING convGoAway
	
	STRING godGetInCar	
	STRING godFail
	STRING failAbandon
	STRING failMichaelDead
	STRING failTrevorDead
	STRING failCrewDead
	STRING failBikeDestroy
	STRING failChase
	STRING failBikeLost
	STRING failMichaelLost
	STRING failFranklinLost
	STRING failTrevorLost
	STRING failCrewLost
	STRING failFranklinDead
	STRING failPoliceCalled
	STRING failVanDestroyed
	STRING failCarDestroyed
	STRING failTruckDestroyed
	STRING godDriveToStore
	STRING godGetOntoRoof
	STRING godStealJewels
	STRING godLeaveStore
	STRING godGetOnBike
	STRING godFollowCrew
	STRING godTakeOutCops
	STRING godStopGuard
	STRING godGetBackInCar
	STRING godGetInTheCar
	STRING godFranklinLeft
	STRING godMichaelLeft
	
	STRING godPickUpCrew
	
	STRING godGetInVan
	STRING godGetBackInVan
	STRING godGetBackInTruck
	STRING godStormDrainSide
	STRING godSwitchToMichael
	STRING godSwitchToFranklin
		
	STRING hlpLadder
	STRING hlpSteal
	STRING hlpFocus
	STRING hlpViewMichael
	STRING hlpViewFranklin
	STRING hlpSwitchMichael
	STRING hlpSwitchFranklin
	
	STRING sbRaidStore
	STRING sbRaidGlassSmash
	STRING sbRaidRayfireCrash
	STRING sbUnderwater
	
	STRING soundRayfireCrash
	STRING soundSmashCabinet
	STRING soundSmashCabinetNPC
	STRING soundRoofAircon
	STRING soundAlarmBell
	STRING soundGasEscape
	STRING soundGasMask
	
	STRING sfCash
	
	STRING sJewelName[iNoOfCases]
	STRING sJewelNone
	
	STRING rfSmash
	STRING rfSmash2
	STRING rfSmash3
	STRING rfSmash4
ENDSTRUCT

AssetList Asset

//INT iBikePopHackRec
FLOAT fDefaultTopSpeed

PROC CreateAssetList()


	IF GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW))) = CMSK_GOOD
	OR GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW))) = CMSK_MEDIUM
		Asset.mnBike = SANCHEZ
		//iBikePopHackRec = 1
		vBatiRecOffset = <<0.0, 0.0, 0.0>>
		fDefaultTopSpeed = 5.0
	ELSE
		Asset.mnBike = BATI2
		//iBikePopHackRec = 2
		vBatiRecOffset = <<0.0, 0.0, -0.1227>>//-0.15>>
		fDefaultTopSpeed = 20.0
	ENDIF
	
	Asset.mnCar = PRIMO //TAILGATER
	IF bIsStealthPath
		Asset.mnVan = BURRITO2
	ELSE
		Asset.mnVan = BURRITO3
	ENDIF
	Asset.mnSecurity = U_M_M_JEWELSEC_01
	Asset.mnShop[0] = A_F_Y_Business_02
	Asset.mnShop[1] = IG_JEWELASS //A_M_Y_BevHills_01					//A_M_Y_BevHills_01		//PROBLEM!
	Asset.mnShop[2] = A_M_Y_BevHills_01 //A_F_Y_Business_02
	Asset.mnShop[3] = A_M_Y_BevHills_01					//A_M_Y_BevHills_01		//PROBLEM!
	IF bIsStealthPath
		Asset.mnShop[4] = A_M_Y_BevHills_01 //A_M_Y_BevHills_01					//A_M_Y_BevHills_01		//PROBLEM!
	ELSE
		Asset.mnShop[4] = A_F_Y_Business_02
	ENDIF
	Asset.mnBackroom = A_F_M_BEVHILLS_02
	Asset.mnGrenade = PROP_GAS_GRENADE
	Asset.mnTruck = BENSON
	Asset.mnPolice = POLICE3 //POLICE
	Asset.mnPoliceman = S_M_Y_COP_01
	Asset.mnPhone = PROP_PHONE_ING
	Asset.mnBag = Prop_CS_Heist_Bag_02
	Asset.mnJewels1 = Prop_J_Neck_disp_02//PROP_JEWEL_01A
	Asset.mnJewels2 = PROP_JEWEL_02A
	Asset.mnJewels4a = PROP_JEWEL_04A
	Asset.mnJewels4b = PROP_JEWEL_04B
	Asset.mnLester = IG_LESTERCREST
	Asset.mnLestersStick = PROP_CS_WALKING_STICK
	Asset.mnGunman = HC_GUNMAN
	Asset.mnDriver = HC_DRIVER
	Asset.mnHacker = HC_HACKER

	Asset.adJewelHeist = "missheist_jewel"
		Asset.animCamSmashD = "cam_smash_case_d"
		Asset.animSmashE = "smash_case_e"
		Asset.animCamSmashE = "cam_smash_case_e"
		Asset.animSmashNecklace = "smash_case_necklace"
		Asset.animCamSmashNecklace = "cam_smash_case_necklace"
		Asset.animSmashNecklaceSkull = "smash_case_necklace_skull"
		Asset.animSmashTrayA = "smash_case_tray_a"
		Asset.animCamSmashTrayA = "cam_smash_case_tray_a"
		Asset.animSmashTrayB = "smash_case_tray_b"
		Asset.animCamSmashTrayB = "cam_smash_case_tray_b"
		Asset.animGasAssistant = "gassed_npc_assistant"
		Asset.animGasSecurity = "gassed_npc_guard"
		Asset.animCowerOnFloor = "2b_guard_onfloor_loop"
		Asset.animGas[0] = "gassed_npc_customer1"
		Asset.animGas[1] = "gassed_npc_customer2"
		Asset.animGas[2] = "gassed_npc_customer3"
		Asset.animGas[3] = "gassed_npc_customer4"
		Asset.animManageressEnter = "manageress_enter"
		Asset.animManageressExit = "manageress_exit"
		Asset.animManageressKneelIntro = "manageress_kneel_intro"
		Asset.animManageressKneelLoop = "manageress_kneel_loop"
		Asset.animManageressPanicLoop = "manageress_loop"
		Asset.animCamManageressEnter = "manageress_enter_cam"
		//old gestures anims copied into dictionary.
		Asset.animDoIt = "do_it"
		Asset.animTime = "time"
		Asset.animFoldArms = "fold_arms_oh_yeah"
		Asset.animImTellingYou = "im_telling_you"
		Asset.animWhy = "but_why"
		Asset.animDamn = "damn"
		Asset.animDespair = "despair"

		//Asset.adWatch = "missbigscore2aig_1"
		Asset.adWatch = "missbigscore2big_2"
		//Asset.animWatch = "wait_gate_b"
		Asset.animWatch = "idle_e"

		Asset.adJewelHeistSwitchCam = "missHeist_Jewel@Switch"
		asset.animTruckBuddy = "switchcam_incar_buddy"
		asset.animTruckMic = "switchcam_incar_mic"
	
	Asset.recCopTrap[0] 									= "JHCopTrap1st"
	Asset.recCopTrap[1]										= "JHCopTrap2nd"
	Asset.recCopTrap[2] 									= "JHCopTrap3rd"
	Asset.recCopTrap[3] 									= "JHCopTrap4th"

	Asset.recTruckExit 										= "JHTruckExit"
	
	Asset.recTruckLead 										= "JHTruckLead"
	Asset.recUber											= "JHUber"
	
	Asset.recNumber											= 650 //999 //650
	Asset.recNoGoodBikeChase								= 998 //098
	//Asset.recNoBadBikeChase									= 099
	Asset.recNoFinalTrain									= 059
	
	Asset.skipBikeChaseStart 		= 0
	Asset.skipFirstCrossroadEnd 	= 6689
	Asset.skipBigTurnStart 			= 9792
	Asset.skipBigTurnMid 			= 11768
	Asset.skipBigTurnEnd 			= 13744
	Asset.skipSecondCrossroadStart	= 20344
	Asset.skipSecondCrossroadEnd	= 22413
	Asset.skipThirdCrossroadStart	= 25325
	Asset.skipThirdCrossroadEnd		= 26112
	Asset.skipSlowdownForJumpTurn	= 36000
	Asset.skipJumpTurnStart 		= 35737
	Asset.skipJumpTurnEnd			= 35737
	Asset.skipJump 					= 40891
	Asset.skipJumpCrash				= 41600 //40900 //42174.570
	Asset.skipJumpLand 				= 41377
	Asset.skipTunnelEnter 			= 45023
	Asset.skipTunnelJump 			= 47653
	Asset.skipTunnelJumpLand 		= 49412
	Asset.skipTunnelCylinderStart	= 54300
	Asset.skipTunnelCylinderEnd 	= 65621
	Asset.skipTunnelUnderRoadStart	= 68548
	Asset.skipTunnelUnderRoadEnd	= 78070
	Asset.skipTunnelRampStart 		= 79936
	Asset.skipTunnelRampEnd 		= 83350
	Asset.skipTunnelSewerStart 		= 93149
	Asset.skipTunnelSewerEnd		= 103173
	Asset.skipTunnelHelterStart 	= 105225
	Asset.skipTunnelHelterEnd 		= 113288
	Asset.skipTrainTrackStart 		= 123606
	Asset.skipTrainTrackEnd 		= 132227
	Asset.skipStormDrain 	 		= 134596
	Asset.skipBikeChaseEnd 			= 140440
		
	Asset.mocapIntroA = "JH_2A_INTp4"
	Asset.mocapIntroB = "jh_2b_int"
	Asset.mocapRooftopGasKo = "JH_2A_MCS_1"
	Asset.mocapFrontDoorSmash = "JH_2B_MCS_1"
	Asset.mocapBagThrowA = "Jh_2_fin_a_mcs4_a1" 
	Asset.mocapBagThrowB = "Jh2_fina_mcs4_a1a2" 
		
	Asset.convStealthPlan = "JH_PLAN"
	Asset.convCarLeave = "JH_CARLVE"
	Asset.convMove = "JH_MOVE"
	Asset.convTrap = "JH_TRAP"
	Asset.convMichaelTruck = "JH_TRUCK"
	Asset.convMountTruck = "JH_MOUNT"

	Asset.convRoof = "JH_ROOF"
	Asset.convHackerA10 = "JH_HCKA10"
	Asset.convHackerA20 = "JH_HCKA20"
	Asset.convHackerA30 = "JH_HCKA30"
	Asset.convHackerA40 = "JH_HCKA40"
	Asset.convHackerA50 = "JH_HCKA50"
	Asset.convHackerA60 = "JH_HCKA60"
	Asset.convHackerB10 = "JH_HCKB10"
	Asset.convHackerB20 = "JH_HCKB20"
	Asset.convHackerB30 = "JH_HCKB30"
	Asset.convHackerB40 = "JH_HCKB40"
	Asset.convHackerAStart = "JH_HCKAGO"
	Asset.convHackerBStart = "JH_HCKBGO"
	Asset.convHackerATimeUp = "JH_HCKA00"
	Asset.convHackerBTimeUp = "JH_HCKB00"
	Asset.convHackerPoliceOnWay = "JH_HCKLT"
	Asset.convMichaelEncouragement = "JH_MENC"

	Asset.convBikeCrash = "JH_BKCRSH"
	Asset.convPolice2 = "JH_PLCSC2"
	Asset.convLostThem = "JH_FRLST"
	Asset.convPolice3 = "JH_PLCSC3"
	Asset.convCollateral = "JH_MMISS"
	Asset.convGetDown = "JH_CTRL"
	SWITCH GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)))
		CASE CMSK_GOOD
			Asset.convFollowMichael = "JH_WALK1"
		BREAK
		CASE CMSK_MEDIUM
			Asset.convFollowMichael = "JH_WALK2"
		BREAK
		
		CASE CMSK_BAD
			Asset.convFollowMichael = "JH_WALK3"
		BREAK
	ENDSWITCH
	Asset.convFollowFranklin = "JH_VAN"
	Asset.convManager = "JH_MANAG"
	Asset.convManagerFail = "JH_FMANA"
	Asset.convRunAway = "JH_RUN"
	Asset.convChaseHardLeft = "JH_CLEFT"
	Asset.convChaseJump = "JH_CJUMP"
	Asset.convChaseTunnelEntry = "JH_CTUNN"
	Asset.convChaseTunnelRamp = "JH_CRAMP"
	Asset.convShoutAtCowerPed = "JH_AIM"
	Asset.convShoutAtKOPed = "JH_AIMKO"
	Asset.convYouDrive = "JH_BUMPM"
	Asset.convYoureDrivingRight = "JH_BUMPF"
	Asset.convComeOnBoys = "JH_BOYS"
	Asset.convJesusFranklin = "JH_VAN" //"JH_MFRNK"
	Asset.convSlowDownFranklin = "JH_SLOWF"
	Asset.convSpeedUpFranklin = "JH_FASTF"
	Asset.convGetBehindMe = "JH_BEHND"
	Asset.convSpeedUpMichael = "JH_FASTM"
	Asset.convSlowDownMichael = "JH_SLOWM"
	Asset.convStealthPlan2 = "JH_PLAN2"
	Asset.convRoofClimbStart = "JH_SROOF"
	Asset.convAggressivePlan2 = "JH_APLAN2"
	Asset.convStopGuard = "JH_GSTOP"
	Asset.convLeaveGoodBad = "JH_LVEGB"
	Asset.convLeaveGoodGood = "JH_LVEGG"
	Asset.convLeaveGoodMedium = "JH_LVEGM"
	Asset.convLeaveMediumBad = "JH_LVEMB"
	Asset.convLeaveBadBad = "JH_LVEBB"
	Asset.convCopsIncomingGood1 = "JH_COPG1"
	Asset.convCopsIncomingGood2 = "JH_COPG2"
	Asset.convCopsIncomingMedium = "JH_COPM"
	Asset.convCopsIncomingBad1 = "JH_COPB1"
	Asset.convCopsIncomingBad2 = "JH_COPB2"
	Asset.convRoofClear = "JH_ROOFC"
	Asset.convShootCops = "JH_SHTCOP"
	
	IF bIsStealthPath
		IF IS_GUNMAN_PACKIE()
			Asset.convCrewResponds = "JH_CRSPND"
		ELIF IS_GUNMAN_GUSTAVO()
			Asset.convCrewResponds = "JH_RESPG"
		ELIF IS_GUNMAN_NORM()
			Asset.convCrewResponds = "JH_RESPN"
		ENDIF
	ELSE
		IF IS_DRIVER_EDDIE()
			Asset.convCrewResponds = "JH_RESP_ET"
		ELIF IS_DRIVER_KARIM()
			Asset.convCrewResponds = "JH_RESP_KD"
		ENDIF	
	ENDIF
	
	Asset.convCrewCopReact1 = "JH_COPC1"
	Asset.convCrewCopReact2 = "JH_COPC2"
	Asset.convFranklinEncouragementOpen = "JH_LOSTF"
	Asset.convFranklinEncouragementTunnel = "JH_FRENCT"
	Asset.convCrewFranklinRespond1 = "JH_CRENC1"
	Asset.convCrewFranklinRespond2 = "JH_CRENC2"
	Asset.convShootHim = "JH_SHOOT"
	Asset.convGoAway = "JH_GOAWAY"
	
	Asset.failAbandon = "F_ABANDON"
	Asset.failMichaelDead = "F_KILLMICHAEL"
	Asset.failTrevorDead = "F_KILLTREVOR"
	Asset.failCrewDead = "F_KILLCREW"
	Asset.failBikeDestroy = "F_DESTROYBIKE"
	Asset.failCarDestroyed = "F_DESTROYCAR"
	Asset.failChase = "F_LOSECHASE"
	Asset.failBikeLost = "F_LOSEBIKE"
	Asset.failMichaelLost = "F_LOSEMICHAEL"
	Asset.failFranklinLost = "F_LOSEFRANKLIN"
	Asset.failTrevorLost = "F_LOSETREVOR"
	Asset.failCrewLost = "F_LOSECREW"
	Asset.failFranklinDead = "F_KILLFRANK"
	Asset.failPoliceCalled = "F_CALLPOLICE"
	Asset.failVanDestroyed = "F_DESTROYVAN"
	Asset.failTruckDestroyed = "F_DESTROYTRUCK"
	
	Asset.godFail = "M_FAIL"
	Asset.godFranklinLeft = "CMN_FLEAVE"
	Asset.godMichaelLeft = "CMN_MLEAVE"
	Asset.godGetInCar = "G_GETVEHICLE"
	Asset.godDriveToStore = "G_DRIVETOSTORE"
	Asset.godGetOntoRoof = "G_GETTOROOF"
	Asset.godStealJewels = "G_STEALJEWELRY"
	Asset.godLeaveStore = "G_LEAVE"
	Asset.godGetOnBike = "G_BIKE"
	Asset.godFollowCrew = "G_FOLLOWCREW"
	Asset.godTakeOutCops = "G_COPS"
	Asset.godStopGuard = "G_GUARD"
	Asset.godGetBackInCar = "G_GETBACKCAR"
	Asset.godGetInTheCar = "G_GETINCAR"
	Asset.godPickUpCrew = "G_PICKCREW"
	Asset.godGetInVan = "G_GETINVAN"
	Asset.godGetBackInVan = "G_GETBACKVAN"
	Asset.godGetBackInTruck = "G_GETBACKTRUCK"
	Asset.godStormDrainSide = "G_STRMSIDE"
	Asset.godSwitchToMichael = "G_DOSWCHM"
	Asset.godSwitchToFranklin = "G_DOSWCHF"	
	Asset.hlpLadder = "H_HELPCLIMB"
	Asset.hlpSteal = "H_HELPSTEAL"
	Asset.hlpFocus = "H_HELPFOCUS"
	Asset.hlpViewMichael = "H_VIEWMIKE"
	Asset.hlpViewFranklin = "H_VIEWFRNK"
	Asset.hlpSwitchMichael = "H_SWITCHM"
	Asset.hlpSwitchFranklin = "H_SWITCHF"
	
	Asset.sbRaidStore = "SCRIPT\\JWL_HA_RAID_STORE"
	Asset.sbRaidGlassSmash = "SCRIPT\\JWL_HEIST_RAID_GLASS_SMASH"
	Asset.sbRaidRayfireCrash = "SCRIPT\\JWL_HEIST_RAID_RAYFIRE_CRASH"
	Asset.sbUnderwater = "SCRIPT\\Underwater"
	
	Asset.soundRayfireCrash = "JWL_HA_RAYFIRE_CRASH"
	Asset.soundSmashCabinet = "JWL_HA_SMASH_CABINET"
	Asset.soundSmashCabinetNPC = "JWL_HA_SMASH_CABINET_NPC"
	Asset.soundRoofAircon = "JWL_HA_ROOFTOP_AIRCON"
	Asset.soundAlarmBell = "JWL_HA_ALARM_BELL"
	Asset.soundGasEscape = "JWL_HA_GAS_ESCAPE"
	Asset.soundGasMask = "JWL_HA_GAS_MASK"

	Asset.sfCash = "jewel_heist_counter"
	
	Asset.sJewelName[0] = "JWL_00"
	Asset.sJewelName[1] = "JWL_01"
	Asset.sJewelName[2] = "JWL_02"
	Asset.sJewelName[3] = "JWL_03"
	Asset.sJewelName[4] = "JWL_04"
	Asset.sJewelName[5] = "JWL_05"
	Asset.sJewelName[6] = "JWL_06"
	Asset.sJewelName[7] = "JWL_07"
	Asset.sJewelName[8] = "JWL_08"
	Asset.sJewelName[9] = "JWL_09"
	Asset.sJewelName[10] = "JWL_10"
	Asset.sJewelName[11] = "JWL_11"
	Asset.sJewelName[12] = "JWL_12"
	Asset.sJewelName[13] = "JWL_13"
	Asset.sJewelName[14] = "JWL_14"
	Asset.sJewelName[15] = "JWL_15"
	Asset.sJewelName[16] = "JWL_16"
	Asset.sJewelName[17] = "JWL_17"
	Asset.sJewelName[18] = "JWL_18"
	Asset.sJewelName[19] = "JWL_19"
//	Asset.sJewelName[20] = "JWL_20"
//	Asset.sJewelName[21] = "JWL_21"
	Asset.sJewelNone = "JWL_NONE"
	
	Asset.rfSmash = "DES_Jewel_Cab"
	Asset.rfSmash2 = "DES_Jewel_Cab2"
	Asset.rfSmash3 = "DES_Jewel_Cab3"
	Asset.rfSmash4 = "DES_Jewel_Cab4"
ENDPROC
//═════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════
//══════════════════════════════════════════════════════╡ END ASSET LIST ╞═════════════════════════════════════════════════════
//═════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════════



//═══════════════════════════╡ JEWEL HEIST SWITCH CAM ╞════════════════════════════

ENUM JEWEL_SWITCH_CAM_STATE
	SWITCH_CAM_IDLE,
	SWITCH_CAM_START_SPLINE1,
	SWITCH_CAM_PLAYING_SPLINE1,
	SWITCH_CAM_RETURN_TO_GAMEPLAY
ENDENUM
JEWEL_SWITCH_CAM_STATE eSwitchCamState = SWITCH_CAM_IDLE

SWITCH_CAM_STRUCT scsSwitchCam

#IF IS_DEBUG_BUILD
BOOL bSwitchCamDebugScenarioEnabled = FALSE //TRUE
BOOL bResetDebugScenario = FALSE
#ENDIF

OBJECT_INDEX oiTruckBuddyFakeWeapon //to do: move this

INT iPlayerControlDelay = 1000
FLOAT fGameplayCamHeading = 0.0
FLOAT fGameplayCamPitch = 0.0

STRING sSwitchCamAnimDict = "missHeist_Jewel@Switch"
STRING sAnimTruckBuddy = "switchcam_incar_buddy"
STRING sAnimTruckMichael = "switchcam_incar_mic"

FLOAT fMichaelTruckRecordingSkipTime = 3600.0

FLOAT fFranklinBikeRecordingSkipTime = 6000.0
FLOAT fFranklinBikeRecordingCruiseSpeed = 30.0
FLOAT fFranklinBikeRecordingSpeed = 0.9//1.0
INT iFranklinBikeRecordingRevertDelay = 3000

FLOAT fPostSwitchFranklinBikeRecordingSkipTime = 2000.0

BOOL bSwitchTruckConvoStarted = FALSE
BOOL bCustomTruckAnimsStarted = FALSE
BOOL bPlayerControlGiven = FALSE

BOOL bHasFranklinBikeRecordingStarted = FALSE

INT iWooshSoundID = -1

FLOAT fWooshStartPhase = 0.61
FLOAT fWooshStopPhase = 0.74
FLOAT fSwitchFX1StartPhase = 0.61
FLOAT fSwitchFX2StartPhase = 0.74

BOOL bWooshStarted = FALSE
BOOL bWooshStopped = FALSE
BOOL bSwitchFX1Started = FALSE
BOOL bSwitchFX2Started = FALSE
BOOL bSlomoAudioFXStarted = FALSE
BOOL bSlomoAudioFXStopped = FALSE

STRING strAnimDictCamShake = "shake_cam_all@"

PROC SETUP_SPLINE_CAM_NODE_ARRAY(SWITCH_CAM_STRUCT &thisSwitchCam, VEHICLE_INDEX vi1, VEHICLE_INDEX vi2)
	CDEBUG3LN(DEBUG_MISSION, "SETUP_SPLINE_CAM_NODE_ARRAY")
	
	IF NOT thisSwitchCam.bInitialized
		
		
		//--- Start of Cam Data ---


		thisSwitchCam.nodes[0].SwitchCamType = SWITCH_CAM_GAMEPLAY_CAM_COPY
		thisSwitchCam.nodes[0].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[0].iNodeTime = 0
		thisSwitchCam.nodes[0].vNodePos = <<1020.4197, -223.5525, 44.3141>>
		thisSwitchCam.nodes[0].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[0].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[0].vNodeDir = <<0.5452, 2.0735, 162.6459>>
		thisSwitchCam.nodes[0].bPointAtEntity = TRUE
		thisSwitchCam.nodes[0].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[0].bAttachOffsetIsRelative = FALSE
		
		IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) = CAM_VIEW_MODE_FIRST_PERSON)
			thisSwitchCam.nodes[0].fNodeFOV = 62.5
		ELSE		
			thisSwitchCam.nodes[0].fNodeFOV = 50.0
		ENDIF
		
		thisSwitchCam.nodes[0].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[0].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[0].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[0].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[0].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[0].fNodeCamShake = 0.2
		thisSwitchCam.nodes[0].iCamEaseType = 0
		thisSwitchCam.nodes[0].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[0].fTimeScale = 1.0000
		thisSwitchCam.nodes[0].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[0].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[0].bFlashEnabled = FALSE
		thisSwitchCam.nodes[0].fMinExposure = 0.0000
		thisSwitchCam.nodes[0].fMaxExposure = 0.0000
		thisSwitchCam.nodes[0].iRampUpDuration = 0
		thisSwitchCam.nodes[0].iRampDownDuration = 0
		thisSwitchCam.nodes[0].iHoldDuration = 0
		thisSwitchCam.nodes[0].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[0].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[0].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[0].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[1].SwitchCamType = SWITCH_CAM_GAMEPLAY_CAM_COPY
		thisSwitchCam.nodes[1].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[1].iNodeTime = 0
		thisSwitchCam.nodes[1].vNodePos = <<1020.4197, -223.5525, 44.3141>>
		thisSwitchCam.nodes[1].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[1].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[1].vNodeDir = <<0.5452, 2.0735, 162.6459>>
		thisSwitchCam.nodes[1].bPointAtEntity = TRUE
		thisSwitchCam.nodes[1].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[1].bAttachOffsetIsRelative = FALSE
		IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) = CAM_VIEW_MODE_FIRST_PERSON)
			thisSwitchCam.nodes[1].fNodeFOV = 62.5
		ELSE		
			thisSwitchCam.nodes[1].fNodeFOV = 50.0
		ENDIF
		thisSwitchCam.nodes[1].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[1].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[1].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[1].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[1].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[1].fNodeCamShake = 0.2
		thisSwitchCam.nodes[1].iCamEaseType = 0
		thisSwitchCam.nodes[1].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[1].fTimeScale = 1.0000
		thisSwitchCam.nodes[1].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[1].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[1].bFlashEnabled = FALSE
		thisSwitchCam.nodes[1].fMinExposure = 0.0000
		thisSwitchCam.nodes[1].fMaxExposure = 0.0000
		thisSwitchCam.nodes[1].iRampUpDuration = 0
		thisSwitchCam.nodes[1].iRampDownDuration = 0
		thisSwitchCam.nodes[1].iHoldDuration = 0
		thisSwitchCam.nodes[1].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[1].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[1].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[1].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[2].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[2].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[2].iNodeTime = 1000
		thisSwitchCam.nodes[2].vNodePos = <<-1.2686, 4.4238, 0.7634>>
		thisSwitchCam.nodes[2].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[2].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[2].vNodeDir = <<-0.1759, -0.0402, 0.7239>>
		thisSwitchCam.nodes[2].bPointAtEntity = TRUE
		thisSwitchCam.nodes[2].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[2].bAttachOffsetIsRelative = FALSE
		IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) = CAM_VIEW_MODE_FIRST_PERSON)
			thisSwitchCam.nodes[2].fNodeFOV = 65.5
		ELSE		
			thisSwitchCam.nodes[2].fNodeFOV = 50.0
		ENDIF
		thisSwitchCam.nodes[2].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[2].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[2].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[2].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[2].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[2].fNodeCamShake = 0.2
		thisSwitchCam.nodes[2].iCamEaseType = 0
		thisSwitchCam.nodes[2].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[2].fTimeScale = 1.0000
		thisSwitchCam.nodes[2].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[2].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[2].bFlashEnabled = FALSE
		thisSwitchCam.nodes[2].fMinExposure = 0.0000
		thisSwitchCam.nodes[2].fMaxExposure = 0.0000
		thisSwitchCam.nodes[2].iRampUpDuration = 0
		thisSwitchCam.nodes[2].iRampDownDuration = 0
		thisSwitchCam.nodes[2].iHoldDuration = 0
		thisSwitchCam.nodes[2].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[2].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[2].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[2].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[3].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[3].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[3].iNodeTime = 1000
		thisSwitchCam.nodes[3].vNodePos = <<-3.3723, 3.9445, -0.1419>>
		thisSwitchCam.nodes[3].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[3].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[3].vNodeDir = <<3.7017, 0.0000, -137.8480>>
		thisSwitchCam.nodes[3].bPointAtEntity = FALSE
		thisSwitchCam.nodes[3].bPointAtOffsetIsRelative = FALSE
		thisSwitchCam.nodes[3].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[3].fNodeFOV = 50.0000
		thisSwitchCam.nodes[3].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[3].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[3].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[3].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[3].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[3].fNodeCamShake = 0.2
		thisSwitchCam.nodes[3].iCamEaseType = 0
		thisSwitchCam.nodes[3].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[3].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[3].fTimeScale = 1.0000
		thisSwitchCam.nodes[3].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[3].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[3].bFlashEnabled = FALSE
		thisSwitchCam.nodes[3].fMinExposure = 0.0000
		thisSwitchCam.nodes[3].fMaxExposure = 0.0000
		thisSwitchCam.nodes[3].iRampUpDuration = 0
		thisSwitchCam.nodes[3].iRampDownDuration = 0
		thisSwitchCam.nodes[3].iHoldDuration = 0
		thisSwitchCam.nodes[3].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[3].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[3].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[3].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[4].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[4].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[4].iNodeTime = 500
		thisSwitchCam.nodes[4].vNodePos = <<-2.8073, 3.3803, 0.0177>>
		thisSwitchCam.nodes[4].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[4].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[4].vNodeDir = <<-0.5709, -0.2904, 0.0941>>
		thisSwitchCam.nodes[4].bPointAtEntity = TRUE
		thisSwitchCam.nodes[4].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[4].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[4].fNodeFOV = 50.0000
		thisSwitchCam.nodes[4].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[4].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[4].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[4].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[4].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[4].fNodeCamShake = 0.2
		thisSwitchCam.nodes[4].iCamEaseType = 0
		thisSwitchCam.nodes[4].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[4].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[4].fTimeScale = 1.0000
		thisSwitchCam.nodes[4].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[4].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[4].bFlashEnabled = FALSE
		thisSwitchCam.nodes[4].fMinExposure = 0.0000
		thisSwitchCam.nodes[4].fMaxExposure = 0.0000
		thisSwitchCam.nodes[4].iRampUpDuration = 0
		thisSwitchCam.nodes[4].iRampDownDuration = 0
		thisSwitchCam.nodes[4].iHoldDuration = 0
		thisSwitchCam.nodes[4].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[4].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[4].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[4].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[5].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[5].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[5].iNodeTime = 1000
		thisSwitchCam.nodes[5].vNodePos = <<-1.9069, 4.1475, 0.0177>>
		thisSwitchCam.nodes[5].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[5].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[5].vNodeDir = <<-0.5866, -0.2491, 0.0934>>
		thisSwitchCam.nodes[5].bPointAtEntity = TRUE
		thisSwitchCam.nodes[5].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[5].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[5].fNodeFOV = 50.0000
		thisSwitchCam.nodes[5].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[5].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[5].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[5].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[5].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[5].fNodeCamShake = 0.2
		thisSwitchCam.nodes[5].iCamEaseType = 0
		thisSwitchCam.nodes[5].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[5].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[5].fTimeScale = 0.5000
		thisSwitchCam.nodes[5].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[5].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[5].bFlashEnabled = FALSE
		thisSwitchCam.nodes[5].fMinExposure = 0.0000
		thisSwitchCam.nodes[5].fMaxExposure = 0.0000
		thisSwitchCam.nodes[5].iRampUpDuration = 0
		thisSwitchCam.nodes[5].iRampDownDuration = 0
		thisSwitchCam.nodes[5].iHoldDuration = 0
		thisSwitchCam.nodes[5].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[5].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[5].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[5].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[6].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[6].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[6].iNodeTime = 300
		thisSwitchCam.nodes[6].vNodePos = <<-0.2482, 10.6558, 0.9422>>
		thisSwitchCam.nodes[6].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[6].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[6].vNodeDir = <<2.3013, 0.9288, 1.0667>>
		thisSwitchCam.nodes[6].bPointAtEntity = TRUE
		thisSwitchCam.nodes[6].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[6].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[6].fNodeFOV = 50.0000
		thisSwitchCam.nodes[6].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[6].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[6].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[6].fNodeMotionBlur = 0.5000
		thisSwitchCam.nodes[6].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[6].fNodeCamShake = 0.2
		thisSwitchCam.nodes[6].iCamEaseType = 0
		thisSwitchCam.nodes[6].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[6].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[6].fTimeScale = 0.5000
		thisSwitchCam.nodes[6].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[6].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[6].bFlashEnabled = FALSE
		thisSwitchCam.nodes[6].fMinExposure = 0.0000
		thisSwitchCam.nodes[6].fMaxExposure = 0.0000
		thisSwitchCam.nodes[6].iRampUpDuration = 0
		thisSwitchCam.nodes[6].iRampDownDuration = 0
		thisSwitchCam.nodes[6].iHoldDuration = 0
		thisSwitchCam.nodes[6].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[6].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[6].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[6].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.nodes[7].SwitchCamType = SWITCH_CAM_OLD_SYSTEM
		thisSwitchCam.nodes[7].bIsGameplayCamCopy = FALSE
		thisSwitchCam.nodes[7].iNodeTime = 300
		thisSwitchCam.nodes[7].vNodePos = <<6.3795, 13.1729, 3.1116>>
		thisSwitchCam.nodes[7].vWorldPosLookAt = <<0.0000, 0.0000, 0.0000>>
		thisSwitchCam.nodes[7].fNodeOffsetDist = 0.0000
		thisSwitchCam.nodes[7].vNodeDir = <<-1.0851, -0.1238, 2.4081>>
		thisSwitchCam.nodes[7].bPointAtEntity = TRUE
		thisSwitchCam.nodes[7].bPointAtOffsetIsRelative = TRUE
		thisSwitchCam.nodes[7].bAttachOffsetIsRelative = FALSE
		thisSwitchCam.nodes[7].fNodeFOV = 50.0000
		thisSwitchCam.nodes[7].NodeTimePostFX_Type = NO_EFFECT
		thisSwitchCam.nodes[7].fNodeTimePostFXBlendTime = 0.0000
		thisSwitchCam.nodes[7].fNodeTimePostFXTimeOffset = 0.0000
		thisSwitchCam.nodes[7].fNodeMotionBlur = 0.0000
		thisSwitchCam.nodes[7].NodeCamShakeType = CAM_SHAKE_MEDIUM
		thisSwitchCam.nodes[7].fNodeCamShake = 0.2
		thisSwitchCam.nodes[7].iCamEaseType = 0
		thisSwitchCam.nodes[7].fCamEaseScaler = 0.0000
		thisSwitchCam.nodes[7].bCamEaseForceLinear = FALSE
		thisSwitchCam.nodes[7].fTimeScale = 1.0000
		thisSwitchCam.nodes[7].iTimeScaleEaseType = 0
		thisSwitchCam.nodes[7].fTimeScaleEaseScaler = 0.0000
		thisSwitchCam.nodes[7].bFlashEnabled = FALSE
		thisSwitchCam.nodes[7].fMinExposure = 0.0000
		thisSwitchCam.nodes[7].fMaxExposure = 0.0000
		thisSwitchCam.nodes[7].iRampUpDuration = 0
		thisSwitchCam.nodes[7].iRampDownDuration = 0
		thisSwitchCam.nodes[7].iHoldDuration = 0
		thisSwitchCam.nodes[7].fFlashNodePhaseOffset = 0.0000
		thisSwitchCam.nodes[7].bIsLowDetailNode = FALSE
		thisSwitchCam.nodes[7].bUseCustomDOF = FALSE
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_NearDOF = 0.0000
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_FarDOF = 0.0000
		thisSwitchCam.nodes[7].NodeDOF_Info.fDOF_EffectStrength = 0.0000

		thisSwitchCam.iNumNodes = 8
		thisSwitchCam.iCamSwitchFocusNode = 6
		thisSwitchCam.bSplineNoSmoothing = TRUE
		thisSwitchCam.bAddGameplayCamAsLastNode = TRUE
		thisSwitchCam.iGameplayNodeBlendDuration = 1000


		//--- End of Cam Data ---



		thisSwitchCam.strOutputStructName = "thisSwitchCam"
		thisSwitchCam.strOutputFileName = "CameraInfo_JewelHeist_SwitchCam.txt"		
		thisSwitchCam.strXMLFileName = "CameraInfo_JewelHeist_SwitchCam.xml"
		
		thisSwitchCam.bInitialized = TRUE
	ENDIF
	
	thisSwitchCam.viVehicles[0] = vi1
	thisSwitchCam.viVehicles[1] = vi2
	
		
ENDPROC

#IF IS_DEBUG_BUILD

PROC CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
	CDEBUG3LN(DEBUG_MISSION, "CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS")
	
	START_WIDGET_GROUP("Custom Switch Cameras - Extra Tunables -")
		ADD_WIDGET_INT_SLIDER("Player Control Delay", iPlayerControlDelay, 0, 999999, 100)
		ADD_WIDGET_FLOAT_SLIDER("Gameplay Cam Heading", fGameplayCamHeading, -180.0, 180.0, 0.25)
		ADD_WIDGET_FLOAT_SLIDER("Gameplay Cam Pitch", fGameplayCamPitch, -180.0, 180.0, 0.25)
		//ADD_WIDGET_FLOAT_SLIDER("Catch Up Distance to Blend", fCatchUpDistanceToBlend, 0.0, 1000.0, 0.25)
	STOP_WIDGET_GROUP()
	
ENDPROC

PROC UPDATE_SPLINE_CAM_HELPERS()
	//CDEBUG3LN(DEBUG_MISSION, "UPDATE_SPLINE_CAM_HELPERS")
	
	IF bResetDebugScenario

		bResetDebugScenario = FALSE
	ENDIF

ENDPROC

#ENDIF

FUNC BOOL IS_FRANKLIN_BIKE_RECORDING_TRIGGERED()
	//CDEBUG3LN(DEBUG_MISSION, "IS_FRANKLIN_BIKE_RECORDING_TRIGGERED")
	
	VECTOR vPos1, vPos2
	vPos1 = <<1007.6, -198.4, 35.0>>
	vPos2 = <<1033.6, -192.9, 50.0>>
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND DOES_ENTITY_EXIST(vehBike[2])
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(vehBike[2])
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBike[2])
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vPos1, vPos2, 5.0)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

BOOL bEntering3rdPersonPushin = FALSE

FUNC BOOL IS_SWITCH_CAM_TRIGGERED()
	//CDEBUG3LN(DEBUG_MISSION, "IS_SWITCH_CAM_TRIGGERED")

	VECTOR vPos1, vPos2
	vPos1 = <<1038.0, -226.0, 40.0>>
	vPos2 = <<1008.0, -231.5, 55.0>>
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
	AND DOES_ENTITY_EXIST(vehBike[2])
		IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		AND NOT IS_ENTITY_DEAD(vehBike[2])
			IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBike[2])
			
				IF bEntering3rdPersonPushin = FALSE
					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vPos1, vPos2, 12.5)
						IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) = CAM_VIEW_MODE_FIRST_PERSON)					
							ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
							PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
							SCRIPT_ASSERT("boom")
							bEntering3rdPersonPushin = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vPos1, vPos2, 5.0)
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC START_FRANKLIN_BIKE_RECORDING()
	CDEBUG3LN(DEBUG_MISSION, "START_FRANKLIN_BIKE_RECORDING")
	
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(722, "JHUBER")
		SCRIPT_ASSERT("START_FRANKLIN_BIKE_RECORDING - Vehicle recording not loaded!!!")
	ENDIF
	
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	
	IF DOES_ENTITY_EXIST(vehBike[2])
		IF NOT IS_ENTITY_DEAD(vehBike[2])
			//fFranklinsBikeInitialSpeed = GET_ENTITY_SPEED(vehBike[2])
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[2])
				STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[2])
			ENDIF
			START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehBike[2], 722, "JHUBER", fFranklinBikeRecordingCruiseSpeed, DRIVINGMODE_PLOUGHTHROUGH)
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[2], fFranklinBikeRecordingSkipTime)
			SET_PLAYBACK_SPEED(vehBike[2], fFranklinBikeRecordingSpeed)
			SET_PLAYBACK_TO_USE_AI_TRY_TO_REVERT_BACK_LATER(vehBike[2], iFranklinBikeRecordingRevertDelay, DRIVINGMODE_PLOUGHTHROUGH)
		ENDIF
	ENDIF
				
ENDPROC

PROC START_MICHAEL_TRUCK_RECORDING(FLOAT fMichaelTruckRecordingSpeed)
	CDEBUG3LN(DEBUG_MISSION, "START_MICHAEL_TRUCK_RECORDING")
	
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(717, "JHUBER")
		SCRIPT_ASSERT("START_MICHAEL_TRUCK_RECORDING - Vehicle recording not loaded!!!")
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehTruck)
		IF NOT IS_ENTITY_DEAD(vehTruck)
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehTruck)
			ENDIF
			START_PLAYBACK_RECORDED_VEHICLE(vehTruck, 717, "JHUBER")
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTruck, fMichaelTruckRecordingSkipTime)
			SET_PLAYBACK_SPEED(vehTruck, fMichaelTruckRecordingSpeed) // fMichaelTruckRecordingSpeed)
		ENDIF
	ENDIF
				
ENDPROC

PROC START_HACKER_SHOOTING()
	CDEBUG3LN(DEBUG_MISSION, "START_HACKER_SHOOTING")
	
	IF DOES_ENTITY_EXIST(pedCrew[CREW_HACKER])
	AND DOES_ENTITY_EXIST(SetPieceCarID[0])
		IF NOT IS_ENTITY_DEAD(pedCrew[CREW_HACKER])
		AND NOT IS_ENTITY_DEAD(SetPieceCarID[0])
			CLEAR_PED_TASKS(pedCrew[CREW_HACKER])
			TASK_DRIVE_BY(pedCrew[CREW_HACKER], NULL, SetPieceCarID[0], <<0.0, -5.0, 0.0>>, 200.0, 100, FALSE)
		ENDIF
	ENDIF
ENDPROC

PROC START_CUSTOM_TRUCK_ANIMS()
	CDEBUG3LN(DEBUG_MISSION, "START_CUSTOM_TRUCK_ANIMS")
	
	IF NOT HAS_ANIM_DICT_LOADED(sSwitchCamAnimDict)
		SCRIPT_ASSERT("START_CUSTOM_TRUCK_ANIMS - Animation Dictionary isn't loaded!!!")
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedCrew[CREW_HACKER])
		IF NOT IS_ENTITY_DEAD(pedCrew[CREW_HACKER])
			TASK_PLAY_ANIM(pedCrew[CREW_HACKER], sSwitchCamAnimDict, sAnimTruckBuddy, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(pedCrew[CREW_HACKER], TRUE)
		ENDIF
	ENDIF
	IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_MICHAEL))
		IF NOT IS_ENTITY_DEAD(pedCrew[CREW_HACKER])
			TASK_PLAY_ANIM(GET_SELECT_PED(SEL_MICHAEL), sSwitchCamAnimDict, sAnimTruckMichael, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_LOOPING)
			FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_SELECT_PED(SEL_MICHAEL), TRUE)
		ENDIF
	ENDIF
	
ENDPROC



PROC START_POST_SWITCH_FRANKLIN_ON_BIKE_RECORDING()
	CDEBUG3LN(DEBUG_MISSION, "START_POST_SWITCH_FRANKLIN_ON_BIKE_RECORDING")
	
	IF NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(700 ,"JHUBER")
		SCRIPT_ASSERT("START_POST_SWITCH_FRANKLIN_ON_BIKE_RECORDING - Vehicle recording 700 not loaded!!!")
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehBike[0])
		IF NOT IS_ENTITY_DEAD(vehBike[0])
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0])
				fPostSwitchFranklinBikeRecordingSkipTime = GET_TIME_POSITION_IN_RECORDING(vehBike[0]) - (SetPieceCarStartime[15])
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehBike[2])
		IF NOT IS_ENTITY_DEAD(vehBike[2])
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[2])
				STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[2])
			ENDIF
			START_PLAYBACK_RECORDED_VEHICLE(vehBike[2], 700 ,"JHUBER")
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[2], fPostSwitchFranklinBikeRecordingSkipTime)
		ENDIF
	ENDIF

ENDPROC

BOOL bFirstPersonEffectSwitchCam = FALSE

FUNC BOOL HANDLE_SWITCH_CAM(SWITCH_CAM_STRUCT &thisSwitchCam)
	
	INT iCurrentNode
	FLOAT fCamPhase
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
	
	SWITCH eSwitchCamState

		CASE SWITCH_CAM_IDLE
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_IDLE")
		BREAK
		
		CASE SWITCH_CAM_START_SPLINE1
						
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_START_SPLINE1")
	
			SET_GAME_PAUSES_FOR_STREAMING(FALSE)
			
			IF NOT IS_ENTITY_DEAD(vehBike[2])
				SET_ENTITY_INVINCIBLE(vehBike[2], TRUE)
			ENDIF
			
			REPLAY_START_EVENT()
			
			DESTROY_ALL_CAMS()
			
			SETUP_SPLINE_CAM_NODE_ARRAY(thisSwitchCam,  vehBike[2], vehTruck)
			CREATE_SPLINE_CAM(thisSwitchCam)

			SET_CAM_ACTIVE(thisSwitchCam.ciSpline, TRUE)
			RENDER_SCRIPT_CAMS(TRUE, FALSE)
			
			ANIMPOSTFX_PLAY("SwitchOpenFranklin", 0, FALSE)
			
			DISPLAY_RADAR(FALSE)
			DISPLAY_HUD(FALSE)
			
			//iSwitchSoundEffect = GET_SOUND_ID()
			//PLAY_SOUND_FRONTEND(iSwitchSoundEffect, "Camera_Move_Loop", "PLAYER_SWITCH_JEWEL_STORE_2B_SOUNDSET")
			//PLAY_SOUND_FRONTEND(-1, "Hit_In", "SHORT_PLAYER_SWITCH_SOUND_SET")
			//PLAY_SOUND_FRONTEND(-1, "Short_Transition_In", "SHORT_PLAYER_SWITCH_SOUND_SET")
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
			
			bSwitchTruckConvoStarted = FALSE
			bCustomTruckAnimsStarted = FALSE
			bPlayerControlGiven = FALSE
			bWooshStarted = FALSE
			bWooshStopped = FALSE
			bSwitchFX1Started = FALSE
			bSwitchFX2Started = FALSE
			bSlomoAudioFXStarted = FALSE
			bSlomoAudioFXStopped = FALSE
			
			IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) = CAM_VIEW_MODE_FIRST_PERSON)					
				PLAY_SOUND_FRONTEND(-1,"Hit_In","PLAYER_SWITCH_CUSTOM_SOUNDSET")
			ENDIF
			SETTIMERB(0)
			
			eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1
			CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1")
		FALLTHRU
		
		CASE SWITCH_CAM_PLAYING_SPLINE1
			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_PLAYING_SPLINE1")
			
			IF IS_CAM_ACTIVE(thisSwitchCam.ciSpline)
				
				iCurrentNode = UPDATE_SPLINE_CAM(thisSwitchCam)
				fCamPhase = GET_CAM_SPLINE_PHASE(thisSwitchCam.ciSpline)
				
				IF GET_CURRENT_PLAYER_PED_ENUM() != CHAR_MICHAEL
					IF iCurrentNode >= thisSwitchCam.iCamSwitchFocusNode
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
						sSelectorPeds.bInitialised = TRUE
						sSelectorPeds.eNewSelectorPed = SELECTOR_PED_MICHAEL
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
					ENDIF
				ENDIF
			
				IF bCustomTruckAnimsStarted = FALSE
					IF iCurrentNode >= thisSwitchCam.iCamSwitchFocusNode
						START_CUSTOM_TRUCK_ANIMS()
						bCustomTruckAnimsStarted = TRUE
					ENDIF
				ENDIF
				
				//woosh out audio
				IF NOT bSwitchTruckConvoStarted
					IF iCurrentNode >= 4
						//PLAY_SOUND_FRONTEND(-1, "Hit_1", "LONG_PLAYER_SWITCH_SOUNDS")
						emptyTextQueue()
						clearText()
						KILL_ANY_CONVERSATION()
						CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_TRUCK", CONV_PRIORITY_VERY_HIGH)	
						bSwitchTruckConvoStarted = TRUE	
					ENDIF
				ENDIF
				
				IF NOT bWooshStarted
					IF fCamPhase >= fWooshStartPhase
						iWooshSoundId = GET_SOUND_ID()
						
						IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) = CAM_VIEW_MODE_FIRST_PERSON)					
							PLAY_SOUND_FRONTEND(-1,"Hit_out","PLAYER_SWITCH_CUSTOM_SOUNDSET")
						ELSE						
							
						ENDIF
						PLAY_SOUND_FRONTEND(iWooshSoundId, "In", "SHORT_PLAYER_SWITCH_SOUND_SET")
						bWooshStarted = TRUE
					ENDIF
				ENDIF
				
				IF NOT bWooshStopped
					IF fCamPhase >= fWooshStopPhase
						STOP_SOUND(iWooshSoundId)
						bWooshStopped = TRUE
					ENDIF
				ENDIF

				IF NOT bSwitchFX1Started
					IF fCamPhase >= fSwitchFX1StartPhase
						ANIMPOSTFX_PLAY("SwitchShortFranklinIn", 0, FALSE)	
						bSwitchFX1Started = TRUE
					ENDIF
				ENDIF

				IF NOT bSwitchFX2Started
					IF fCamPhase >= fSwitchFX2StartPhase
						ANIMPOSTFX_PLAY("SwitchShortMichaelMid", 0, FALSE)	
						bSwitchFX2Started = TRUE
					ENDIF
				ENDIF
				
				IF NOT bSlomoAudioFXStarted
					IF iCurrentNode >= 4
						CDEBUG3LN(DEBUG_MISSION, "Activating audio slowmo mode...")
						ACTIVATE_AUDIO_SLOWMO_MODE("JSH_EXIT_TUNNEL_SLOWMO") 
						bSlomoAudioFXStarted = TRUE
					ENDIF
				ENDIF
				
				IF NOT bSlomoAudioFXStopped
					IF iCurrentNode >= 7
						CDEBUG3LN(DEBUG_MISSION, "Deactivating audio slowmo mode...")
						DEACTIVATE_AUDIO_SLOWMO_MODE("JSH_EXIT_TUNNEL_SLOWMO") 
						bSlomoAudioFXStopped = TRUE
					ENDIF
				ENDIF
			
				IF iCurrentNode = (thisSwitchCam.iNumNodes - 1)
					CDEBUG3LN(DEBUG_MISSION, "Setting gameplay cam heading/pitch")
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(fGameplayCamHeading)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(fGameplayCamPitch)
				ENDIF
				
				IF fCamPhase >=0.925
				AND bFirstPersonEffectSwitchCam = FALSE
					IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_IN_VEHICLE) = CAM_VIEW_MODE_FIRST_PERSON)					
						ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
						PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
					ENDIF
					bFirstPersonEffectSwitchCam = TRUE
				ENDIF
				
				IF fCamPhase >= 1.0
				
					SET_TIME_SCALE(1.0)
					IF DOES_CAM_EXIST(thisSwitchCam.ciSpline)
						DESTROY_CAM(thisSwitchCam.ciSpline)
						DESTROY_ALL_CAMS()
					ENDIF
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					
					//PLAY_SOUND_FRONTEND(-1, "Hit_Out", "PLAYER_SWITCH_JEWEL_STORE_2B_SOUNDSET")
					//STOP_SOUND(iSwitchSoundEffect)
									
					STOP_SOUND(iWooshSoundId)
					
					DISPLAY_RADAR(TRUE)
					DISPLAY_HUD(TRUE)
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					
					SETTIMERB(0)
					
						
						
					eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY
					CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")
				ENDIF
				
				
				
			ENDIF
			
		BREAK

		CASE SWITCH_CAM_RETURN_TO_GAMEPLAY
			//CDEBUG3LN(DEBUG_MISSION, "eSwitchCamState = SWITCH_CAM_RETURN_TO_GAMEPLAY")

				IF NOT bPlayerControlGiven
					INT iLeftX, iLeftY, iRightX, iRightY
					GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(iLeftX, iLeftY, iRightX, iRightY)
					IF iLeftX <> 0 OR iLeftY <> 0 OR iRightX <> 0 OR iRightY <> 0
					OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
					OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_BRAKE)
					OR TIMERB() > iPlayerControlDelay
					
						IF DOES_ENTITY_EXIST(vehTruck)
							IF NOT IS_ENTITY_DEAD(vehTruck)
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck)
									STOP_PLAYBACK_RECORDED_VEHICLE(vehTruck)
								ENDIF
							ENDIF
						ENDIF
						IF DOES_ENTITY_EXIST(GET_SELECT_PED(SEL_MICHAEL))
							IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_MICHAEL))
								CLEAR_PED_TASKS(GET_SELECT_PED(SEL_MICHAEL))
							ENDIF
						ENDIF
						
						REPLAY_STOP_EVENT()
						
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						
						SET_GAME_PAUSES_FOR_STREAMING(TRUE)
						
						SETTIMERA(0)
						
						eSwitchCamState = SWITCH_CAM_IDLE
						
						bPlayerControlGiven = TRUE
						
						RETURN TRUE
					ENDIF
				ENDIF
			
			
		BREAK
		
	ENDSWITCH


	RETURN FALSE
ENDFUNC


//═════════════════════════════════╡ LOADING ╞══════════════════════════════════

//PURPOSE: Sets up jewel case values
PROC INIT_JEWEL_CASE(	sJewelCase &jCase, VECTOR CasePos, VECTOR PlayerPos, VECTOR PlayerRot, STRING LootName, INT Worth, 
						STRING SmashAnim)
	jCase.vCasePos 			= CasePos
	jCase.vPlayerPos 		= PlayerPos
	jCase.vPlayerRot 		= PlayerRot
	jCase.bSmashed			= FALSE
	jCase.sLootName			= LootName
	jCase.iWorth			= Worth
	jCase.sSmashAnim		= SmashAnim
ENDPROC


//PURPOSE: Sets values for variables that cannot be set during creation
PROC SetupArrays()
		
	vBikeSpawnCoords[0]								= << -639.42, -237.89, 37.45 >> //<< -638.9341, -238.0901, 37.0373 >>
	fBikeSpawnHeading[0]							= 59.5395
	vBikeSpawnCoords[1]								= << -638.6117, -242.9966, 37.1304 >>
	fBikeSpawnHeading[1]							= 42.6795
	vBikeSpawnCoords[2]								= <<-634.4958, -241.2315, 37.1219>>//<< -634.733, -244.360, 37.390 >>//<< -634.733, -244.360, 37.790 >>
	fBikeSpawnHeading[2]							= 40.5422//49.740
			
		
	vBeginWalkToCoords[0]							= << 708.2659, -966.5298, 29.4334 >>
	fBeginWalkToHeading[0]							= 26.5931
	vBeginWalkToCoords[1]							= << 706.1772, -966.1973, 29.4598 >>
	fBeginWalkToHeading[1]							= 347.5504
	vBeginWalkToCoords[2]							= << 708.7750, -963.8549, 29.4334 >>
	fBeginWalkToHeading[2]							= 132.5186
	vBeginWalkToCoords[3]							= << 709.2004, -965.6567, 29.4334 >>
	fBeginWalkToHeading[3]							= 101.4240
	vBeginWalkToCoords[4]							= << 707.5271, -962.9240, 29.4334 >>
	fBeginWalkToHeading[4]							= 176.8285
	
	vRoofRoute[0]									= << -583.6948, -283.4582, 34.2523 >>
	vRoofRoute[1]									= << -596.4763, -284.1328, 40.6868 >>
	vRoofRoute[2]									= << -596.0881, -293.9532, 40.6868 >>
	vRoofRoute[3]									= << -596.8112, -294.6171, 47.0676 >>//<< -589.6608, -290.1633, 49.3286 >>
	vRoofRoute[4]									= << -616.3251, -239.7347, 54.6582 >>//<< -609.7062, -243.1836, 50.7084 >> //<< -615.6468, -241.5095, 50.3209 >>
	vRoofRoute[5]									= << -625.7886, -216.2038, 58.1056 >>//<< -607.9752, -251.8895, 53.3070 >> //<< -622.0686, -231.3150, 57.8029 >>
	
	
	vVehicleFightTruckSpawn							= << 793.2487, -462.3718, 28.3002 >>
	fVehicleFightTruckSpawn							= 75.2595
	
	vVehicleFightBikeSpawn[0]						= << 722.6227, -441.2054, 16.2914 >>
	fVehicleFightBikeSpawn[0]						= 126.6293
	vVehicleFightBikeSpawn[1]						= << 743.0473, -463.6686, 16.4246 >>
	fVehicleFightBikeSpawn[1]						= 122.2695
	vVehicleFightBikeSpawn[2]						= << 745.7171, -427.7663, 18.2352 >>
	fVehicleFightBikeSpawn[2]						= 116.5734
		
	//Security = "U_M_M_JEWELSEC_01"
	vSecuritySpawnCoords							= <<-629.037537,-232.825684,37.2080>>
	fSecuritySpawnHeading							= 103.248833
	//Shop[0] = A_F_Y_Business_01
	vStoreSpawnCoords[0]							= <<-622.008179,-230.516388,37.2080>>
	fStoreSpawnHeading[0]							= 57.439987
	//Shop[1] = "A_M_M_Business_01"
	vStoreSpawnCoords[1]							= <<-625.432312,-235.850708,37.2080>>
	fStoreSpawnHeading[1]							= 41.509983
	//Shop[2] = "A_F_Y_BevHills_03"
	vStoreSpawnCoords[2]							= <<-626.046387,-236.601852,37.2080>>
	fStoreSpawnHeading[2]							= 18.799986
	//Shop[3] = "A_M_Y_BevHills_02"
	vStoreSpawnCoords[3]							= <<-623.979370,-232.240952,37.2080>>
	fStoreSpawnHeading[3]							= 6.109993
	//Shop[4] = "A_M_Y_Business_02"
	vStoreSpawnCoords[4]							= <<-623.529846,-234.933823,37.2080>>
	fStoreSpawnHeading[4]							= -22.491545
		
	bIsPlayerLooting								= FALSE
		
//	bStartedKOGas									= FALSE
	
	bIsDoorLocked									= FALSE
	
	bIsRandomGuardEventNow							= FALSE
	bIsRandomManagerEventNow						= FALSE
	bIsRandomGuardEventDone							= FALSE
	bIsRandomManagerEventDone						= FALSE
	

//	INIT_JEWEL_CASE(jcStore[0],	<< -631.4504, -232.3133, 37.9016 >>, << -630.876, -233.075, 37.010 >>, << -0.000, -0.000, 36.097 >>,Asset.sJewelName[0], 29250, Asset.rfSmash3)
//	jcStore[0].vCameraPos = <<-626.389648,-230.750839,37.251659>>
//	jcStore[0].vCameraRot = <<11.065775,0.018565,103.489548>>
//	jcStore[0].fCameraFov = 24.680170
//
//	jcStore[0].vCameraPos2 = <<-626.389648,-231.444611,37.251659>>
//	jcStore[0].vCameraRot2 = <<11.065775,0.018565,99.268867>>
//	jcStore[0].fCameraFov2 = 24.680170
//
//	INIT_JEWEL_CASE(jcStore[1], << -630.613, -231.652, 37.8479 >>, << -629.63, -232.309, 37.0946 >>, << 0, 0, 36 >>,Asset.sJewelName[1], 63840, Asset.rfSmash4)
//	jcStore[1].vCameraPos = <<-631.403870,-234.622360,37.355644>>
//	jcStore[1].vCameraRot = <<11.869922,-6.134729,-29.403307>>
//	jcStore[1].fCameraFov = 24.685804
//
//	jcStore[1].vCameraPos2 = <<-630.412292,-234.623581,37.520927>>
//	jcStore[1].vCameraRot2 = <<10.396501,-6.134729,-11.109667>>
//	jcStore[1].fCameraFov2 = 24.685804
	
//	jcStore[0].bSmashed = TRUE
//	jcStore[1].bSmashed = TRUE
	
	INIT_JEWEL_CASE(jcStore[0], << -627.735, -234.439, 37.875 >>, << -628.187, -233.538, 37.0946 >>, << 0, 0, -144 >>, Asset.sJewelName[0], 99750, Asset.rfSmash)
	INIT_JEWEL_CASE(jcStore[1], << -626.716, -233.685, 37.8583 >>, << -627.136, -232.775, 37.0946 >>, << 0, 0, -144 >>,Asset.sJewelName[1], 159600, Asset.rfSmash)
	
	INIT_JEWEL_CASE(jcStore[2], << -627.35, -234.947, 37.8531 >>, << -626.62, -235.725, 37.0946 >>, << 0, 0, 36 >>,Asset.sJewelName[2], 104737, Asset.rfSmash3)
	INIT_JEWEL_CASE(jcStore[3], << -626.298, -234.193, 37.8492 >>, << -625.57, -234.962, 37.0946 >>, << 0, 0, 36 >>,Asset.sJewelName[3], 154613, Asset.rfSmash4)
	INIT_JEWEL_CASE(jcStore[4], << -626.399, -239.132, 37.8616 >>, << -626.894, -238.2, 37.0856 >>, << 0, 0, -144 >>,Asset.sJewelName[4], 39900, Asset.rfSmash2)

	INIT_JEWEL_CASE(jcStore[5], << -625.376, -238.358, 37.8687 >>, << -625.867, -237.458, 37.0946 >>, << 0, 0, -144 >>,Asset.sJewelName[5], 59850, Asset.rfSmash3)
//	jcStore[5].vCameraPos = <<-620.833069,-234.801987,39.115303>>
//	jcStore[5].vCameraRot = <<-5.517539,10.823854,126.548439>>
//	jcStore[5].fCameraFov =12.333325
//
//	jcStore[5].vCameraPos2 = <<-621.000916,-234.566605,39.060291>>
//	jcStore[5].vCameraRot2 = <<-5.517538,10.823854,130.684631>>
//	jcStore[5].fCameraFov2 = 12.333325

	
	INIT_JEWEL_CASE(jcStore[6], << -625.517, -227.421, 37.86 >>, << -624.738, -228.2, 37.0946 >>, << 0, 0, 36 >>,Asset.sJewelName[6], 219450, Asset.rfSmash3)
	INIT_JEWEL_CASE(jcStore[7], << -624.467, -226.653, 37.861 >>, << -623.688, -227.437, 37.0946 >>, << 0, 0, 36 >>,Asset.sJewelName[7], 299250, Asset.rfSmash4)
	INIT_JEWEL_CASE(jcStore[8],<< -623.8118, -228.6336, 37.8522 >>, << -624.293, -227.831, 37.0946 >>, << -0.000, -0.000, -143.511 >>,Asset.sJewelName[8], 305235, Asset.rfSmash2)
	INIT_JEWEL_CASE(jcStore[9],<< -624.1267, -230.7476, 37.8618 >>, << -624.939, -231.247, 37.0946 >>, << -0.000, 0.000, -54.130 >>,Asset.sJewelName[9], 448793, Asset.rfSmash4)
	INIT_JEWEL_CASE(jcStore[10],<< -621.7181, -228.9636, 37.8425 >>, << -620.864, -228.481, 37.0946 >>, << 0.000, -0.000, 126.925 >>,Asset.sJewelName[10], 546125, Asset.rfSmash3)
	jcStore[10].vCameraPos = <<-620.8779, -227.7553, 38.7692>>
	jcStore[10].vCameraRot = <<-15.0723, -0.3250, 147.1275>>
	jcStore[10].fCameraFov = 35.207096
	
	jcStore[10].vCameraPos2 = <<-620.4113, -227.7727, 38.6039>>
	jcStore[10].vCameraRot2 = <<-1.7684, 0.1702, 123.3868>>
	jcStore[10].fCameraFov2 = 35.207096
	
	
	INIT_JEWEL_CASE(jcStore[11],<< -622.7541, -232.6140, 37.8638 >>, <<-623.3596, -233.2296, 37.0946 >>, << -0.000, 0.000, -52.984 >>,Asset.sJewelName[11], 498750, Asset.rfSmash)
	jcStore[11].vCameraPos = <<-622.035950,-233.608185,38.449856>>
	jcStore[11].vCameraRot = <<-1.763292,-2.630027,46.963608>>
	jcStore[11].fCameraFov = 39.749928

	jcStore[11].vCameraPos2 = <<-621.489197,-232.971817,38.635601>>
	jcStore[11].vCameraRot2 = <<-8.361150,-2.628445,86.691910>>
	jcStore[11].fCameraFov2 = 37.115498
	
	INIT_JEWEL_CASE(jcStore[12],<< -620.3262, -230.8290, 37.8578 >>, <<-619.4080, -230.1969, 37.0946 >>, << 0.000, -0.000, 126.352 >>,Asset.sJewelName[12], 685775, Asset.rfSmash)
	INIT_JEWEL_CASE(jcStore[13],<< -620.6465, -232.9308, 37.8407 >>, << -620.184, -233.729, 37.0946 >>, << 0.000, 0.000, 36.398 >>,Asset.sJewelName[13], 118450, Asset.rfSmash4)
	jcStore[13].vCameraPos = <<-618.882996,-233.409760,38.153709>>
	jcStore[13].vCameraRot = <<-2.226855,-2.630027,64.844284>>
	jcStore[13].fCameraFov = 40.180321

	jcStore[13].vCameraPos2 = <<-618.9713, -233.3480, 38.6380>>
	jcStore[13].vCameraRot2 = <<-1.0873, -1.7225, 84.9176>>
	jcStore[13].fCameraFov2 = 40.180321
	
	INIT_JEWEL_CASE(jcStore[14], << -619.978, -234.93, 37.8537 >>, << -620.44, -234.084, 37.0946 >>, << 0, 0, -144 >>,Asset.sJewelName[14], 449375, Asset.rfSmash)
	INIT_JEWEL_CASE(jcStore[15], << -618.937, -234.16, 37.8425 >>, << -619.39, -233.32, 37.0946 >>, << 0, 0, -144 >>,Asset.sJewelName[15], 59250, Asset.rfSmash3)
	INIT_JEWEL_CASE(jcStore[16], << -620.163, -226.212,  37.8266 >>, << -620.797, -226.79, 37.0946 >>, << 0, 0, -54 >> ,Asset.sJewelName[16], 59350, Asset.rfSmash)
	INIT_JEWEL_CASE(jcStore[17], << -619.384, -227.259,  37.8342 >>, << -620.055, -227.817, 37.0856 >>, << 0, 0, -54 >>, Asset.sJewelName[17], 99250, Asset.rfSmash2)
	INIT_JEWEL_CASE(jcStore[18], << -618.019, -229.115, 37.8302 >>, << -618.679, -229.704, 37.0946 >>, << 0, 0, -54 >> ,Asset.sJewelName[18], 99250, Asset.rfSmash3)
	INIT_JEWEL_CASE(jcStore[19], << -617.249, -230.156, 37.8201 >>, << -617.937, -230.731, 37.0856 >>, << 0, 0, -54 >>,Asset.sJewelName[19], 439400, Asset.rfSmash2)
	
//	iPotentialTake = 0
//	FOR iCounter = 0 TO iNoOfCases-1
//		iPotentialTake += jcStore[iCounter].iWorth
//	ENDFOR

	#IF IS_DEBUG_BUILD
	SkipMenuStruct[ENUM_TO_INT(STAGE_INIT_MISSION)].sTxtLabel 				= "Initialise Mission"
	SkipMenuStruct[ENUM_TO_INT(STAGE_INIT_MISSION)].bSelectable = FALSE
	SkipMenuStruct[ENUM_TO_INT(STAGE_TIME_LAPSE)].bSelectable = FALSE
	
	SkipMenuStruct[ENUM_TO_INT(STAGE_CUTSCENE_BEGIN)].sTxtLabel				= "Cutscene: jh_2a_intp4/jh_2b_int"
	SkipMenuStruct[ENUM_TO_INT(STAGE_DRIVE_TO_STORE)].sTxtLabel 			= "Stage: Drive to Store"	
	SkipMenuStruct[ENUM_TO_INT(STAGE_ROOF)].sTxtLabel 						= "Stage: Roof Climb *STEALTH*"
	SkipMenuStruct[ENUM_TO_INT(STAGE_CUTSCENE_GAS_STORE)].sTxtLabel 		= "Cutscene: Gas Store JH_2A_MCS_1 *STEALTH*"
	IF NOT bIsStealthPath
		SkipMenuStruct[ENUM_TO_INT(STAGE_ROOF)].bSelectable = FALSE
		SkipMenuStruct[ENUM_TO_INT(STAGE_CUTSCENE_GAS_STORE)].bSelectable = FALSE
	ENDIF
	SkipMenuStruct[ENUM_TO_INT(STAGE_CUTSCENE_AGGRESSIVE_ARRIVE)].sTxtLabel = "Cutscene: Arrive at Store JH_2B_MCS_1 *AGGRESSIVE*"
	IF bIsStealthPath
		SkipMenuStruct[ENUM_TO_INT(STAGE_CUTSCENE_AGGRESSIVE_ARRIVE)].bSelectable = FALSE
	ENDIF
	SkipMenuStruct[ENUM_TO_INT(STAGE_GRAB_LOOT)].sTxtLabel 					= "Stage: Grab Loot"
	SkipMenuStruct[ENUM_TO_INT(STAGE_CUTSCENE_LEAVE_STORE)].sTxtLabel 		= "Cutscene: Leave Store Jh_2_fin_a_mcs4_a1 / Jh2_fina_mcs4_a1a2"
	
	SkipMenuStruct[ENUM_TO_INT(STAGE_BIKE_CHASE)].sTxtLabel 				= "Stage: Bike Chase"
	SkipMenuStruct[ENUM_TO_INT(STAGE_BIKE_CHASE_START_OF_TUNNEL)].sTxtLabel	= "Stage: Bike Chase - Tunnel Entrance"
	SkipMenuStruct[ENUM_TO_INT(STAGE_BIKE_CHASE_END_OF_TUNNEL)].sTxtLabel   = "Stage: Bike Chase - Tunnel Exit"
	//SkipMenuStruct[ENUM_TO_INT(STAGE_CUTSCENE_TRUCK_ARRIVE)].sTxtLabel 		= "Cutscene: Truck Arrive"
	SkipMenuStruct[ENUM_TO_INT(STAGE_TRUCK_FIGHT)].sTxtLabel				= "Stage: Truck Fight"
	SkipMenuStruct[ENUM_TO_INT(STAGE_TRUCK_FIGHT)].bSelectable = FALSE
	SkipMenuStruct[ENUM_TO_INT(STAGE_CUTSCENE_RAMP_DOWN)].sTxtLabel 		= "Cutscene: Ramp Down"	
	SkipMenuStruct[ENUM_TO_INT(STAGE_RETURN_TO_BASE)].sTxtLabel 		= "Take loot to lockup"

	SkipMenuStruct[ENUM_TO_INT(PASSED_MISSION)].sTxtLabel 					= "Passed Mission"
	
	SkipMenuStruct[ENUM_TO_INT(STAGE_FAIL_CUTSCENE)].sTxtLabel 				= "Fail Cutscene"

	
	#ENDIF	
	
ENDPROC


PROC LOAD_UBER_ARRAY()
		
	

// ****  UBER RECORDED TRAFFIC  **** 

//TrafficCarPos[0] = <<-708.6527, -201.9990, 36.9937>>
//TrafficCarQuatX[0] = 0.0045
//TrafficCarQuatY[0] = 0.0013
//TrafficCarQuatZ[0] = 0.2622
//TrafficCarQuatW[0] = 0.9650
//TrafficCarRecording[0] = 1
//TrafficCarStartime[0] = 0.0000
//TrafficCarModel[0] = intruder

//IF NOT g_bLateLeaveJewelStore
//
//	TrafficCarPos[1] = <<-676.7388, -250.0423, 36.0270>>
//	TrafficCarQuatX[1] = -0.0004
//	TrafficCarQuatY[1] = 0.0247
//	TrafficCarQuatZ[1] = 0.2695
//	TrafficCarQuatW[1] = 0.9627
//	TrafficCarRecording[1] = 26
//	TrafficCarStartime[1] = 700.0000
//	TrafficCarModel[1] = intruder
//
//ENDIF

//TrafficCarPos[2] = <<-681.8569, -249.8767, 36.1696>>
//TrafficCarQuatX[2] = 0.0057
//TrafficCarQuatY[2] = 0.0027
//TrafficCarQuatZ[2] = 0.2714
//TrafficCarQuatW[2] = 0.9624
//TrafficCarRecording[2] = 27
//TrafficCarStartime[2] = 0.0000
//TrafficCarModel[2] = intruder

//TrafficCarPos[3] = <<-745.0231, -158.7464, 36.5661>>
//TrafficCarQuatX[3] = 0.0027
//TrafficCarQuatY[3] = -0.0086
//TrafficCarQuatZ[3] = 0.9741
//TrafficCarQuatW[3] = -0.2258
//TrafficCarRecording[3] = 16
//TrafficCarStartime[3] = 0.0000
//TrafficCarModel[3] = futo

TrafficCarPos[4] = <<-33.8633, -499.4361, 32.4682>>
TrafficCarQuatX[4] = -0.0134
TrafficCarQuatY[4] = -0.0135
TrafficCarQuatZ[4] = 0.7107
TrafficCarQuatW[4] = 0.7033
TrafficCarRecording[4] = 25
TrafficCarStartime[4] = 0.0000
TrafficCarModel[4] = intruder

TrafficCarPos[5] = <<-782.2250, -280.0836, 36.5154>>
TrafficCarQuatX[5] = -0.0062
TrafficCarQuatY[5] = 0.0047
TrafficCarQuatZ[5] = 0.1901
TrafficCarQuatW[5] = 0.9817
TrafficCarRecording[5] = 28
TrafficCarStartime[5] = 5.0000
TrafficCarModel[5] = intruder

TrafficCarPos[6] = <<-780.7571, -281.1153, 36.4944>>
TrafficCarQuatX[6] = -0.0074
TrafficCarQuatY[6] = 0.0167
TrafficCarQuatZ[6] = 0.1845
TrafficCarQuatW[6] = 0.9827
TrafficCarRecording[6] = 29
TrafficCarStartime[6] = 7000.0000
TrafficCarModel[6] = intruder

TrafficCarPos[7] = <<-792.6519, -285.0189, 36.6133>>
TrafficCarQuatX[7] = 0.0236
TrafficCarQuatY[7] = 0.0063
TrafficCarQuatZ[7] = 0.9975
TrafficCarQuatW[7] = -0.0664
TrafficCarRecording[7] = 2
TrafficCarStartime[7] = 7458.0000
TrafficCarModel[7] = futo

TrafficCarPos[8] = <<-807.5777, -268.1831, 36.4000>>
TrafficCarQuatX[8] = 0.0231
TrafficCarQuatY[8] = -0.0174
TrafficCarQuatZ[8] = 0.9862
TrafficCarQuatW[8] = -0.1629
TrafficCarRecording[8] = 3
TrafficCarStartime[8] = 7722.0000
TrafficCarModel[8] = intruder

TrafficCarPos[9] = <<-804.7911, -324.1086, 36.6377>>
TrafficCarQuatX[9] = 0.0045
TrafficCarQuatY[9] = -0.0154
TrafficCarQuatZ[9] = 0.8405
TrafficCarQuatW[9] = -0.5416
TrafficCarRecording[9] = 8
TrafficCarStartime[9] = 9000.0000
TrafficCarModel[9] = intruder

IF NOT g_bLateLeaveJewelStore
	TrafficCarPos[10] = <<-761.9318, -336.7130, 35.7959>>
	TrafficCarQuatX[10] = 0.0030
	TrafficCarQuatY[10] = -0.0170
	TrafficCarQuatZ[10] = 0.8328
	TrafficCarQuatW[10] = -0.5533
	TrafficCarRecording[10] = 10
	TrafficCarStartime[10] = 9000.0000
	TrafficCarModel[10] = intruder
ENDIF

TrafficCarPos[11] = <<-743.8067, -349.9666, 35.1022>>
TrafficCarQuatX[11] = -0.0101
TrafficCarQuatY[11] = -0.0319
TrafficCarQuatZ[11] = 0.8296
TrafficCarQuatW[11] = -0.5573
TrafficCarRecording[11] = 12
TrafficCarStartime[11] = 9000.0000
TrafficCarModel[11] = intruder

TrafficCarPos[12] = <<-808.8842, -298.8781, 36.6708>>
TrafficCarQuatX[12] = 0.0049
TrafficCarQuatY[12] = 0.0272
TrafficCarQuatZ[12] = 0.5848
TrafficCarQuatW[12] = 0.8107
TrafficCarRecording[12] = 4
TrafficCarStartime[12] = 9240.0000
TrafficCarModel[12] = futo

TrafficCarPos[13] = <<-658.5707, -365.3690, 34.3948>>
TrafficCarQuatX[13] = -0.0044
TrafficCarQuatY[13] = -0.0145
TrafficCarQuatZ[13] = 0.5708
TrafficCarQuatW[13] = 0.8210
TrafficCarRecording[13] = 45
TrafficCarStartime[13] = 11000.0000
TrafficCarModel[13] = futo

TrafficCarPos[14] = <<-700.5820, -343.4963, 34.2747>>
TrafficCarQuatX[14] = -0.0070
TrafficCarQuatY[14] = 0.0146
TrafficCarQuatZ[14] = 0.5830
TrafficCarQuatW[14] = 0.8123
TrafficCarRecording[14] = 31
TrafficCarStartime[14] = 11000.0000
TrafficCarModel[14] = intruder

TrafficCarPos[15] = <<-742.2833, -333.4205, 35.5649>>
TrafficCarQuatX[15] = 0.0118
TrafficCarQuatY[15] = -0.0022
TrafficCarQuatZ[15] = 0.5698
TrafficCarQuatW[15] = 0.8217
TrafficCarRecording[15] = 30
TrafficCarStartime[15] = 13000.0000
TrafficCarModel[15] = intruder

IF g_bJewelStoreJobSeeCarCrashOnce= FALSE
	TrafficCarPos[16] = <<-652.4525, -309.8684, 34.8913>>
	TrafficCarQuatX[16] = 0.0048
	TrafficCarQuatY[16] = -0.0153
	TrafficCarQuatZ[16] = 0.9671
	TrafficCarQuatW[16] = -0.2538
	TrafficCarRecording[16] = 57
	TrafficCarStartime[16] = 14500.0000
	TrafficCarModel[16] = surfer
ENDIF

TrafficCarPos[17] = <<-629.4591, -419.8522, 34.3522>>
TrafficCarQuatX[17] = -0.0038
TrafficCarQuatY[17] = -0.0006
TrafficCarQuatZ[17] = 0.1536
TrafficCarQuatW[17] = 0.9881
TrafficCarRecording[17] = 56
TrafficCarStartime[17] = 17000.0000
TrafficCarModel[17] = washington

TrafficCarPos[18] = <<-646.1561, -330.1735, 34.4512>>
TrafficCarQuatX[18] = 0.0023
TrafficCarQuatY[18] = -0.0063
TrafficCarQuatZ[18] = 0.9720
TrafficCarQuatW[18] = -0.2347
TrafficCarRecording[18] = 6
TrafficCarStartime[18] = 17000.0000
TrafficCarModel[18] = buffalo

TrafficCarPos[19] = <<-645.9526, -365.6587, 34.2968>>
TrafficCarQuatX[19] = 0.0087
TrafficCarQuatY[19] = -0.0028
TrafficCarQuatZ[19] = 0.6804
TrafficCarQuatW[19] = 0.7328
TrafficCarRecording[19] = 11
TrafficCarStartime[19] = 17556.0000
TrafficCarModel[19] = intruder

TrafficCarPos[20] = <<-533.9698, -366.4778, 34.7457>>
TrafficCarQuatX[20] = 0.0024
TrafficCarQuatY[20] = -0.0100
TrafficCarQuatZ[20] = 0.6975
TrafficCarQuatW[20] = 0.7166
TrafficCarRecording[20] = 35
TrafficCarStartime[20] = 18000.0000
TrafficCarModel[20] = intruder

TrafficCarPos[21] = <<-537.3636, -339.1620, 34.6386>>
TrafficCarQuatX[21] = -0.0126
TrafficCarQuatY[21] = -0.0109
TrafficCarQuatZ[21] = 0.9881
TrafficCarQuatW[21] = -0.1527
TrafficCarRecording[21] = 38
TrafficCarStartime[21] = 18000.0000
TrafficCarModel[21] = futo

IF g_bJewelStoreJobSeeCarCrashOnce= FALSE
	TrafficCarPos[22] = <<-583.7382, -370.7444, 34.7169>>
	TrafficCarQuatX[22] = 0.0012
	TrafficCarQuatY[22] = -0.0106
	TrafficCarQuatZ[22] = 0.7034
	TrafficCarQuatW[22] = 0.7108
	TrafficCarRecording[22] = 5
	TrafficCarStartime[22] = 18000.0000
	TrafficCarModel[22] = bobcatxl
ENDIF

//TrafficCarPos[23] = <<-540.9004, -332.6434, 34.6619>>
//TrafficCarQuatX[23] = -0.0155
//TrafficCarQuatY[23] = -0.0036
//TrafficCarQuatZ[23] = 0.9736
//TrafficCarQuatW[23] = -0.2276
//TrafficCarRecording[23] = 33
//TrafficCarStartime[23] = 19500.0000
//TrafficCarModel[23] = manana

TrafficCarPos[24] = <<-519.8954, -365.4402, 34.6054>>
TrafficCarQuatX[24] = -0.0167
TrafficCarQuatY[24] = -0.0070
TrafficCarQuatZ[24] = 0.7061
TrafficCarQuatW[24] = 0.7079
TrafficCarRecording[24] = 14
TrafficCarStartime[24] = 22704.0000
TrafficCarModel[24] = intruder

TrafficCarPos[25] = <<-536.8012, -381.7743, 34.5752>>
TrafficCarQuatX[25] = 0.0049
TrafficCarQuatY[25] = 0.0087
TrafficCarQuatZ[25] = -0.7044
TrafficCarQuatW[25] = 0.7098
TrafficCarRecording[25] = 40
TrafficCarStartime[25] = 25000.0000
TrafficCarModel[25] = futo

TrafficCarPos[26] = <<-326.7438, -373.6238, 29.5950>>
TrafficCarQuatX[26] = -0.0009
TrafficCarQuatY[26] = -0.0062
TrafficCarQuatZ[26] = 0.9590
TrafficCarQuatW[26] = 0.2834
TrafficCarRecording[26] = 43
TrafficCarStartime[26] = 28000.0000
TrafficCarModel[26] = intruder

TrafficCarPos[27] = <<-292.2700, -394.0676, 29.6529>>
TrafficCarQuatX[27] = -0.0033
TrafficCarQuatY[27] = -0.0044
TrafficCarQuatZ[27] = 0.6476
TrafficCarQuatW[27] = 0.7620
TrafficCarRecording[27] = 44
TrafficCarStartime[27] = 28000.0000
TrafficCarModel[27] = futo

TrafficCarPos[28] = <<-430.5280, -391.5486, 32.3151>>
TrafficCarQuatX[28] = 0.0072
TrafficCarQuatY[28] = -0.0286
TrafficCarQuatZ[28] = 0.7321
TrafficCarQuatW[28] = -0.6806
TrafficCarRecording[28] = 47
TrafficCarStartime[28] = 29000.0000
TrafficCarModel[28] = intruder

TrafficCarPos[29] = <<-498.0784, -378.6452, 34.5305>>
TrafficCarQuatX[29] = 0.0118
TrafficCarQuatY[29] = -0.0114
TrafficCarQuatZ[29] = 0.7557
TrafficCarQuatW[29] = -0.6547
TrafficCarRecording[29] = 15
TrafficCarStartime[29] = 29000.0000
TrafficCarModel[29] = BjXl //futo

TrafficCarPos[30] = <<-408.6873, -378.5633, 31.8791>>
TrafficCarQuatX[30] = 0.0077
TrafficCarQuatY[30] = 0.0055
TrafficCarQuatZ[30] = 0.6494
TrafficCarQuatW[30] = 0.7604
TrafficCarRecording[30] = 42
TrafficCarStartime[30] = 30000.0000
TrafficCarModel[30] = intruder

TrafficCarPos[31] = <<-316.7394, -402.1969, 30.3198>>
TrafficCarQuatX[31] = 0.0029
TrafficCarQuatY[31] = -0.0031
TrafficCarQuatZ[31] = 0.7466
TrafficCarQuatW[31] = -0.6653
TrafficCarRecording[31] = 17
TrafficCarStartime[31] = 30228.0000
TrafficCarModel[31] = intruder

//TrafficCarPos[32] = <<-338.0081, -388.0249, 29.7062>>
//TrafficCarQuatX[32] = 0.0098
//TrafficCarQuatY[32] = 0.0078
//TrafficCarQuatZ[32] = 0.6553
//TrafficCarQuatW[32] = 0.7553
//TrafficCarRecording[32] = 49
//TrafficCarStartime[32] = 31000.0000
//TrafficCarModel[32] = intruder

TrafficCarPos[33] = <<-275.0721, -400.0645, 29.6053>>
TrafficCarQuatX[33] = 0.0063
TrafficCarQuatY[33] = 0.0002
TrafficCarQuatZ[33] = -0.2606
TrafficCarQuatW[33] = 0.9654
TrafficCarRecording[33] = 18
TrafficCarStartime[33] = 31746.0000
TrafficCarModel[33] = intruder

//IF g_bLateLeaveJewelStore = FALSE
//	TrafficCarPos[34] = <<-266.8417, -398.4064, 30.0646>>
//	TrafficCarQuatX[34] = -0.0062
//	TrafficCarQuatY[34] = 0.0106
//	TrafficCarQuatZ[34] = 0.8948
//	TrafficCarQuatW[34] = -0.4462
//	TrafficCarRecording[34] = 19
//	TrafficCarStartime[34] = 32076.0000
//	TrafficCarModel[34] = futo
//ENDIF

TrafficCarPos[35] = <<-223.3546, -530.3926, 26.3561>>
TrafficCarQuatX[35] = -0.0131
TrafficCarQuatY[35] = 0.0132
TrafficCarQuatZ[35] = 0.7157
TrafficCarQuatW[35] = -0.6982
TrafficCarRecording[35] = 718
TrafficCarStartime[35] = 34719.0000
TrafficCarModel[35] = jackal



// ****  UBER RECORDED PARKED CARS  **** 

ParkedCarPos[1] = <<-452.5270, -392.0529, 33.3284>>
ParkedCarQuatX[1] = -0.0042
ParkedCarQuatY[1] = -0.0277
ParkedCarQuatZ[1] = 0.7473
ParkedCarQuatW[1] = -0.6639
ParkedCarModel[1] = futo

ParkedCarPos[2] = <<-379.4753, -401.2219, 31.1100>>
ParkedCarQuatX[2] = -0.0141
ParkedCarQuatY[2] = -0.0365
ParkedCarQuatZ[2] = 0.7427
ParkedCarQuatW[2] = -0.6685
ParkedCarModel[2] = futo

ParkedCarPos[3] = <<-371.2489, -402.2385, 30.8473>>
ParkedCarQuatX[3] = -0.0040
ParkedCarQuatY[3] = -0.0306
ParkedCarQuatZ[3] = 0.7518
ParkedCarQuatW[3] = -0.6586
ParkedCarModel[3] = futo

ParkedCarPos[4] = <<-336.2818, -406.7365, 29.5646>>
ParkedCarQuatX[4] = -0.0099
ParkedCarQuatY[4] = -0.0202
ParkedCarQuatZ[4] = 0.7622
ParkedCarQuatW[4] = -0.6470
ParkedCarModel[4] = intruder

ParkedCarPos[5] = <<1041.3979, -276.8591, 50.2230>>
ParkedCarQuatX[5] = -0.0127
ParkedCarQuatY[5] = -0.0271
ParkedCarQuatZ[5] = 0.9913
ParkedCarQuatW[5] = 0.1283
ParkedCarModel[5] = POLICE3

ParkedCarPos[6] = <<1045.6538, -299.9178, 49.4434>>
ParkedCarQuatX[6] = 0.0082
ParkedCarQuatY[6] = -0.0401
ParkedCarQuatZ[6] = -0.4631
ParkedCarQuatW[6] = 0.8854
ParkedCarModel[6] = POLICE3

//ParkedCarPos[10] = <<559.8403, -836.3935, 9.9357>>
//ParkedCarQuatX[10] = -0.0010
//ParkedCarQuatY[10] = -0.0010
//ParkedCarQuatZ[10] = 0.7693
//ParkedCarQuatW[10] = -0.6389
//ParkedCarModel[10] = police

ParkedCarPos[11] = <<565.6103, -836.3173, 9.9359>>
ParkedCarQuatX[11] = 0.0012
ParkedCarQuatY[11] = 0.0008
ParkedCarQuatZ[11] = -0.6146
ParkedCarQuatW[11] = 0.7888
ParkedCarModel[11] = POLICE3

ParkedCarPos[12] = <<573.8864, -837.9350, 9.9314>>
ParkedCarQuatX[12] = 0.0012
ParkedCarQuatY[12] = -0.0009
ParkedCarQuatZ[12] = 0.7139
ParkedCarQuatW[12] = 0.7003
ParkedCarModel[12] = POLICE3

ParkedCarPos[13] = <<582.9113, -839.4673, 9.7816>>
ParkedCarQuatX[13] = 0.0501
ParkedCarQuatY[13] = 0.0509
ParkedCarQuatZ[13] = 0.6493
ParkedCarQuatW[13] = 0.7572
ParkedCarModel[13] = POLICE3

ParkedCarPos[14] = <<679.7463, -525.5728, 14.2560>>
ParkedCarQuatX[14] = -0.0102
ParkedCarQuatY[14] = -0.0023
ParkedCarQuatZ[14] = 0.9769
ParkedCarQuatW[14] = -0.2135
ParkedCarModel[14] = POLICE3

ParkedCarPos[15] = <<686.1791, -529.5911, 14.4451>>
ParkedCarQuatX[15] = -0.0120
ParkedCarQuatY[15] = 0.0100
ParkedCarQuatZ[15] = 0.7669
ParkedCarQuatW[15] = -0.6416
ParkedCarModel[15] = POLICE3

ParkedCarPos[16] = <<670.8864, -522.0038, 14.2409>>
ParkedCarQuatX[16] = 0.0035
ParkedCarQuatY[16] = -0.0055
ParkedCarQuatZ[16] = 0.5541
ParkedCarQuatW[16] = 0.8324
ParkedCarModel[16] = POLICE3



// ****  UBER RECORDED SET PIECE CARS  **** 

SetPieceCarPos[0] = <<-638.6117, -242.9966, 37.6235>>
SetPieceCarQuatX[0] = 0.0000
SetPieceCarQuatY[0] = 0.0000
SetPieceCarQuatZ[0] = 0.3646
SetPieceCarQuatW[0] = 0.9312
SetPieceCarRecording[0] = 998
SetPieceCarStartime[0] = 0.0000
SetPieceCarRecordingSpeed[0] = 1.0000
SetPieceCarModel[0] = Asset.mnBike

//		SetPieceCarPos[5] = <<-638.6117, -242.9966, 37.6235>>
//		SetPieceCarQuatX[5] = 0.0000
//		SetPieceCarQuatY[5] = 0.0000
//		SetPieceCarQuatZ[5] = 0.3646
//		SetPieceCarQuatW[5] = 0.9312
//		SetPieceCarRecording[5] = 425
//		SetPieceCarStartime[5] = 0.0000
//		SetPieceCarRecordingSpeed[5] = 1.0000
//		SetPieceCarModel[5] = Asset.mnBike

SetPieceCarPos[1] = <<-840.9790, -377.3532, 39.4003>>
SetPieceCarQuatX[1] = -0.0063
SetPieceCarQuatY[1] = 0.0194
SetPieceCarQuatZ[1] = -0.5120
SetPieceCarQuatW[1] = 0.8588
SetPieceCarRecording[1] = 53
SetPieceCarStartime[1] = 3500.0000
SetPieceCarRecordingSpeed[1] = 1.0000
SetPieceCarModel[1] = Biff

SetPieceCarPos[2] = <<-868.8333, -287.1481, 40.2375>>
SetPieceCarQuatX[2] = 0.0024
SetPieceCarQuatY[2] = -0.0158
SetPieceCarQuatZ[2] = 0.8924
SetPieceCarQuatW[2] = -0.4510
SetPieceCarRecording[2] = 54
SetPieceCarStartime[2] = 7250.0000
SetPieceCarRecordingSpeed[2] = 1.0000
SetPieceCarModel[2] = BjXl

SetPieceCarPos[3] = <<-875.2956, -282.3078, 39.9842>>
SetPieceCarQuatX[3] = -0.0007
SetPieceCarQuatY[3] = -0.0132
SetPieceCarQuatZ[3] = 0.8808
SetPieceCarQuatW[3] = -0.4733
SetPieceCarRecording[3] = 55
SetPieceCarStartime[3] = 7250.0000
SetPieceCarRecordingSpeed[3] = 1.0000
SetPieceCarModel[3] = washington

SetPieceCarPos[4] = <<-479.2266, -344.1364, 34.1400>>
SetPieceCarQuatX[4] = -0.0001
SetPieceCarQuatY[4] = -0.0003
SetPieceCarQuatZ[4] = 0.9967
SetPieceCarQuatW[4] = 0.0811
SetPieceCarRecording[4] = 7
SetPieceCarStartime[4] = 19500.0000
SetPieceCarRecordingSpeed[4] = 0.8000
SetPieceCarModel[4] = AMBULANCE


//Only see garbage truck on first try
//IF g_JH2AGarbageTruckSeen = FALSE
//	SetPieceCarPos[5] = <<-433.2168, -424.5156, 32.5667>>
//	SetPieceCarQuatX[5] = 0.0065
//	SetPieceCarQuatY[5] = 0.0070
//	SetPieceCarQuatZ[5] = -0.0536
//	SetPieceCarQuatW[5] = 0.9985
//	SetPieceCarRecording[5] = 34
//	SetPieceCarStartime[5] = 25500.0000
//	SetPieceCarRecordingSpeed[5] = 1.0000
//	SetPieceCarModel[5] = Trash
//ENDIF

SetPieceCarPos[6] = <<-160.6159, -349.8996, 34.0634>>
SetPieceCarQuatX[6] = -0.0608
SetPieceCarQuatY[6] = -0.0647
SetPieceCarQuatZ[6] = 0.9587
SetPieceCarQuatW[6] = 0.2702
SetPieceCarRecording[6] = 60
SetPieceCarStartime[6] = 30000.0000
SetPieceCarRecordingSpeed[6] = 1.0000
SetPieceCarModel[6] = police3


SetPieceCarPos[8] = <<-136.4866, -296.7225, 39.7642>>
SetPieceCarQuatX[8] = -0.0210
SetPieceCarQuatY[8] = -0.0531
SetPieceCarQuatZ[8] = 0.9846
SetPieceCarQuatW[8] = 0.1651
SetPieceCarRecording[8] = 50
SetPieceCarStartime[8] = 30000.0000
SetPieceCarRecordingSpeed[8] = 1.0000
SetPieceCarModel[8] = police3

SetPieceCarPos[9] = <<-233.3454, -619.1058, 33.8295>>
SetPieceCarQuatX[9] = 0.0149
SetPieceCarQuatY[9] = 0.0266
SetPieceCarQuatZ[9] = -0.1565
SetPieceCarQuatW[9] = 0.9872
SetPieceCarRecording[9] = 51
SetPieceCarStartime[9] = 31000.0000
SetPieceCarRecordingSpeed[9] = 1.0000
SetPieceCarModel[9] = bus

SetPieceCarPos[10] = <<-231.0461, -603.5657, 33.8332>>
SetPieceCarQuatX[10] = 0.0017
SetPieceCarQuatY[10] = 0.0013
SetPieceCarQuatZ[10] = -0.1655
SetPieceCarQuatW[10] = 0.9862
SetPieceCarRecording[10] = 52
SetPieceCarStartime[10] = 30000.0000
SetPieceCarRecordingSpeed[10] = 1.0000
SetPieceCarModel[10] = police3

//SetPieceCarPos[11] = <<1060.6450, -273.7614, 55.9396>>
//SetPieceCarQuatX[11] = 0.0321
//SetPieceCarQuatY[11] = -0.0367
//SetPieceCarQuatZ[11] = 0.7244
//SetPieceCarQuatW[11] = 0.6876
//SetPieceCarRecording[11] = 802
//SetPieceCarStartime[11] = 152000.0000
//SetPieceCarRecordingSpeed[11] = 1.0000
//SetPieceCarModel[11] = polmav

SetPieceCarPos[12] = <<1081.2266, -354.8069, 67.0946>>
SetPieceCarQuatX[12] = -0.0170
SetPieceCarQuatY[12] = -0.0003
SetPieceCarQuatZ[12] = 0.3461
SetPieceCarQuatW[12] = 0.9381
SetPieceCarRecording[12] = 803
SetPieceCarStartime[12] = 151328.0000
SetPieceCarRecordingSpeed[12] = 1.0000
SetPieceCarModel[12] = riot

SetPieceCarPos[13] = <<1021.5374, -310.6206, 66.7659>>
SetPieceCarQuatX[13] = -0.0100
SetPieceCarQuatY[13] = 0.0008
SetPieceCarQuatZ[13] = 0.8815
SetPieceCarQuatW[13] = -0.4722
SetPieceCarRecording[13] = 804
SetPieceCarStartime[13] = 159087.0000
SetPieceCarRecordingSpeed[13] = 1.0000
SetPieceCarModel[13] = POLICE3

SetPieceCarPos[14] = <<695.3250, -395.2741, 40.9185>>
SetPieceCarQuatX[14] = -0.0003
SetPieceCarQuatY[14] = -0.0020
SetPieceCarQuatZ[14] = 0.7092
SetPieceCarQuatW[14] = -0.7050
SetPieceCarRecording[14] = 805
SetPieceCarStartime[14] = 166840.0000
SetPieceCarRecordingSpeed[14] = 1.0000
SetPieceCarModel[14] = POLICE3

SetPieceCarPos[15] = <<1040.1321, -284.6946, 49.7886>>
SetPieceCarQuatX[15] = 0.0118
SetPieceCarQuatY[15] = -0.0124
SetPieceCarQuatZ[15] = 0.9569
SetPieceCarQuatW[15] = 0.2898
SetPieceCarRecording[15] = 700
SetPieceCarStartime[15] = 158000.0000
SetPieceCarRecordingSpeed[15] = 1.0000
SetPieceCarModel[15] = Asset.mnBike
SetPieceCarProgress[15] = 3 //Stop it creatign but stream in its recordinge tc.
	
////////SetPieceCarPos[16] = <<1097.3839, -234.6847, 57.2819>>
////////SetPieceCarQuatX[16] = 0.0040
////////SetPieceCarQuatY[16] = 0.0179
////////SetPieceCarQuatZ[16] = 0.9654
////////SetPieceCarQuatW[16] = 0.2599
////////SetPieceCarRecording[16] = 717
////////SetPieceCarStartime[16] = 151861.0000
////////SetPieceCarRecordingSpeed[16] = 0.8700
////////SetPieceCarModel[16] = Benson

SetPieceCarPos[17] = <<1084.0251, -254.2668, 57.7442>>
SetPieceCarQuatX[17] = 0.0041
SetPieceCarQuatY[17] = 0.0153
SetPieceCarQuatZ[17] = 0.9649
SetPieceCarQuatW[17] = 0.2623
SetPieceCarRecording[17] = 702
SetPieceCarStartime[17] =  152700.0000 //153200.0000//153600.0000
SetPieceCarRecordingSpeed[17] = 0.95 //0.9000
SetPieceCarModel[17] = POLICE3

SetPieceCarPos[18] = <<1085.7998, -257.0630, 57.7623>>
SetPieceCarQuatX[18] = 0.0000
SetPieceCarQuatY[18] = 0.0000
SetPieceCarQuatZ[18] = 0.9545
SetPieceCarQuatW[18] = 0.2982
SetPieceCarRecording[18] = 703
SetPieceCarStartime[18] = 153100.0000 //153600.0000
SetPieceCarRecordingSpeed[18] = 0.9200
SetPieceCarModel[18] = POLICE3

//SetPieceCarPos[19] = <<976.2125, -325.9881, 61.8851>>
//SetPieceCarQuatX[19] = 0.2809
//SetPieceCarQuatY[19] = -0.2158
//SetPieceCarQuatZ[19] = 0.9259
//SetPieceCarQuatW[19] = 0.1312
//SetPieceCarRecording[19] = 704
//SetPieceCarStartime[19] = 153200.0000
//SetPieceCarRecordingSpeed[19] = 0.9485
//SetPieceCarModel[19] = POLICE3

SetPieceCarPos[19] = <<976.2125, -325.9881, 61.8851>>
SetPieceCarQuatX[19] = 0.2821
SetPieceCarQuatY[19] = -0.2157
SetPieceCarQuatZ[19] = 0.9255
SetPieceCarQuatW[19] = 0.1313
SetPieceCarRecording[19] = 999
SetPieceCarStartime[19] = 153500.0 //153200.0000
SetPieceCarRecordingSpeed[19] = 1.0000
SetPieceCarModel[19] = POLICE3

SetPieceCarPos[20] = <<973.0397, -317.2896, 66.2431>>
SetPieceCarQuatX[20] = -0.0340
SetPieceCarQuatY[20] = -0.0516
SetPieceCarQuatZ[20] = 0.9158
SetPieceCarQuatW[20] = -0.3969
SetPieceCarRecording[20] = 705
SetPieceCarStartime[20] = 154400.0000
SetPieceCarRecordingSpeed[20] = 0.9500
SetPieceCarModel[20] = POLICE3

SetPieceCarPos[21] = <<623.5294, -468.4917, 24.5423>>
SetPieceCarQuatX[21] = 0.0006
SetPieceCarQuatY[21] = 0.0061
SetPieceCarQuatZ[21] = 0.9911
SetPieceCarQuatW[21] = 0.1329
SetPieceCarRecording[21] = 706
SetPieceCarStartime[21] = 172600.0000
SetPieceCarRecordingSpeed[21] = 0.9070
SetPieceCarModel[21] = police3

SetPieceCarPos[22] = <<659.6873, -854.3524, 14.0619>>
SetPieceCarQuatX[22] = -0.0338
SetPieceCarQuatY[22] = -0.0317
SetPieceCarQuatZ[22] = 0.7275
SetPieceCarQuatW[22] = 0.6846
SetPieceCarRecording[22] = 707
SetPieceCarStartime[22] = 187300.0000
SetPieceCarRecordingSpeed[22] = 0.99 // 1.0000
SetPieceCarModel[22] = POLICE3

//SetPieceCarPos[23] = <<649.6309, -1642.9402, 9.3658>>
//SetPieceCarQuatX[23] = -0.0001
//SetPieceCarQuatY[23] = 0.0000
//SetPieceCarQuatZ[23] = -0.0327
//SetPieceCarQuatW[23] = 0.9995
//SetPieceCarRecording[23] = 719
//SetPieceCarStartime[23] = 208000.0000
//SetPieceCarRecordingSpeed[23] = 1.0000
//SetPieceCarModel[23] = TipTruck

SetPieceCarPos[24] = <<658.1793, -1169.9729, 13.8373>>
SetPieceCarQuatX[24] = -0.0310
SetPieceCarQuatY[24] = -0.0276
SetPieceCarQuatZ[24] = 0.7661
SetPieceCarQuatW[24] = 0.6414
SetPieceCarRecording[24] = 708
SetPieceCarStartime[24] = 200000.0000
SetPieceCarRecordingSpeed[24] = 0.9205 //0.9500
SetPieceCarModel[24] = police3

SetPieceCarPos[25] = <<-582.8351, -304.9959, 34.7214>>
SetPieceCarQuatX[25] = -0.0057
SetPieceCarQuatY[25] = -0.0024
SetPieceCarQuatZ[25] = 0.8591
SetPieceCarQuatW[25] = 0.5118
SetPieceCarRecording[25] = 709
SetPieceCarStartime[25] = 15615.0000
SetPieceCarRecordingSpeed[25] = 1.0000
SetPieceCarModel[25] = POLICE3

SetPieceCarPos[26] = <<-571.1154, -309.1694, 34.7754>>
SetPieceCarQuatX[26] = -0.0020
SetPieceCarQuatY[26] = -0.0042
SetPieceCarQuatZ[26] = 0.8688
SetPieceCarQuatW[26] = 0.4952
SetPieceCarRecording[26] = 710
SetPieceCarStartime[26] = 11615.0000
SetPieceCarRecordingSpeed[26] = 1.0000
SetPieceCarModel[26] = POLICE3

SetPieceCarPos[27] = <<-623.1417, -446.3074, 34.3810>>
SetPieceCarQuatX[27] = -0.0018
SetPieceCarQuatY[27] = 0.0103
SetPieceCarQuatZ[27] = -0.0151
SetPieceCarQuatW[27] = 0.9998
SetPieceCarRecording[27] = 711
SetPieceCarStartime[27] = 16298.0000
SetPieceCarRecordingSpeed[27] = 1.0000
SetPieceCarModel[27] = POLICE3

SetPieceCarPos[28] = <<-522.9142, -446.6633, 33.8793>>
SetPieceCarQuatX[28] = -0.0099
SetPieceCarQuatY[28] = -0.0197
SetPieceCarQuatZ[28] = -0.0663
SetPieceCarQuatW[28] = 0.9976
SetPieceCarRecording[28] = 712
SetPieceCarStartime[28] = 21963.0000
SetPieceCarRecordingSpeed[28] = 1.0000
SetPieceCarModel[28] = POLICE3
//
//SetPieceCarPos[29] = <<-18.8808, -505.1215, 32.8986>>
//SetPieceCarQuatX[29] = 0.0021
//SetPieceCarQuatY[29] = -0.0217
//SetPieceCarQuatZ[29] = 0.7266
//SetPieceCarQuatW[29] = 0.6867
//SetPieceCarRecording[29] = 713
//SetPieceCarStartime[29] = 34719.0000
//SetPieceCarRecordingSpeed[29] = 1.0000
//SetPieceCarModel[29] = TAXI

SetPieceCarPos[30] = <<-37.9929, -499.3190, 32.5247>>
SetPieceCarQuatX[30] = -0.0082
SetPieceCarQuatY[30] = -0.0080
SetPieceCarQuatZ[30] = 0.7138
SetPieceCarQuatW[30] = 0.7002
SetPieceCarRecording[30] = 714
SetPieceCarStartime[30] = 34719.0000
SetPieceCarRecordingSpeed[30] = 1.0000
SetPieceCarModel[30] = TipTruck

SetPieceCarPos[31] = <<-22.2940, -493.0348, 33.4044>>
SetPieceCarQuatX[31] = -0.0053
SetPieceCarQuatY[31] = -0.0050
SetPieceCarQuatZ[31] = 0.7117
SetPieceCarQuatW[31] = 0.7024
SetPieceCarRecording[31] = 715
SetPieceCarStartime[31] = 34719.0000
SetPieceCarRecordingSpeed[31] = 1.0000
SetPieceCarModel[31] = flatbed

//SetPieceCarPos[32] = <<-216.6198, -535.8246, 26.8352>>
//SetPieceCarQuatX[32] = -0.0293
//SetPieceCarQuatY[32] = -0.0024
//SetPieceCarQuatZ[32] = 0.7216
//SetPieceCarQuatW[32] = -0.6917
//SetPieceCarRecording[32] = 716
//SetPieceCarStartime[32] = 33719.0000
//SetPieceCarRecordingSpeed[32] = 1.0000
//SetPieceCarModel[32] = flatbed

SetPieceCarPos[32] = <<-196.7399, -517.8179, 27.2766>>
SetPieceCarQuatX[32] = 0.0050
SetPieceCarQuatY[32] = -0.0180
SetPieceCarQuatZ[32] = -0.7058
SetPieceCarQuatW[32] = 0.7082
SetPieceCarRecording[32] = 696
SetPieceCarStartime[32] = 32841.0000
SetPieceCarRecordingSpeed[32] = 1.0000
SetPieceCarModel[32] = BUS


IF g_bLateLeaveJewelStore 
	SetPieceCarPos[33] = <<-651.2066, -193.8645, 37.2064>>
	SetPieceCarQuatX[33] = -0.0118
	SetPieceCarQuatY[33] = 0.0007
	SetPieceCarQuatZ[33] = 0.8824
	SetPieceCarQuatW[33] = 0.4703
	SetPieceCarRecording[33] = 721
	SetPieceCarStartime[33] = 1500.0000
	SetPieceCarRecordingSpeed[33] = 1.0
	SetPieceCarModel[33] = POLICE3 //POLICE
ELSE
	SetPieceCarPos[7] = <<-154.2711, -348.7168, 34.8215>>
	SetPieceCarQuatX[7] = -0.0296
	SetPieceCarQuatY[7] = -0.0560
	SetPieceCarQuatZ[7] = 0.9915
	SetPieceCarQuatW[7] = 0.1138
	SetPieceCarRecording[7] = 61
	SetPieceCarStartime[7] = 30000.0000
	SetPieceCarRecordingSpeed[7] = 1.0000
	SetPieceCarModel[7] = POLICE3
ENDIF
//SetPieceCarPos[33] = <<971.7173, -311.1626, 66.5521>>
//SetPieceCarQuatX[33] = -0.0199
//SetPieceCarQuatY[33] = 0.0098
//SetPieceCarQuatZ[33] = 0.9163
//SetPieceCarQuatW[33] = -0.3999
//SetPieceCarRecording[33] = 720
//SetPieceCarStartime[33] = 156000.0000
//SetPieceCarRecordingSpeed[33] = 1.1700
//SetPieceCarModel[33] = police

IF NOT g_bLateLeaveJewelStore

	SetPieceCarPos[34] = <<-676.7388, -250.0423, 36.0270>>
	SetPieceCarQuatX[34] = -0.0004
	SetPieceCarQuatY[34] = 0.0247
	SetPieceCarQuatZ[34] = 0.2695
	SetPieceCarQuatW[34] = 0.9627
	SetPieceCarRecording[34] = 26
	SetPieceCarStartime[34] = 700.0000
	SetPieceCarRecordingSpeed[34] = 1.0
	SetPieceCarModel[34] = intruder

ENDIF



// ****  POSSIBLE REDUNDANT CAR RECORDINGS  **** 
//
// The following car recordings MIGHT be redundant. 
// They were associated with traffic and set piece cars 
// which are now invalid. Please check carefully if they 
// are being used somewhere else. If not, remove them from 
// alienbrian and the car recording image. 
//
// 48
// 13
// 57
// 56
// 6
// 5
// 33
//

 // **** end of uber output **** 

ENDPROC

PROC LOAD_AND_MANAGE_UBER_ARRAY()
	
	//SET_UBER_PLAYBACK_DEFAULT_PED_MODEL(Asset.mnShop[1])
	SET_FORCE_UBER_PLAYBACK_TO_USE_DEFAULT_PED_MODEL(TRUE)
	
	LOAD_UBER_ARRAY()

ENDPROC

//═════════════════════════════════╡ PED CONTROL ╞═══════════════════════════════════

//PURPOSE:	Sets the peds attributes to that necessary for a crew member
PROC SET_PED_CREW(PED_INDEX thisPed)
	IF DOES_ENTITY_EXIST(thisPed)
		IF NOT IS_ENTITY_DEAD(thisPed)
			SET_PED_CAN_BE_DRAGGED_OUT(thisPed, FALSE)
			SET_PED_CAN_BE_TARGETTED(thisPed, FALSE)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(thisPed, FALSE)
			//SET_ENTITY_HEALTH(thisPed, 500)
			SET_PED_NEVER_LEAVES_GROUP(thisPed, TRUE)
			SET_PED_RELATIONSHIP_GROUP_HASH(thisPed, RELGROUPHASH_PLAYER)
			
			SET_PED_COMBAT_ATTRIBUTES(thisPed, 	CA_AGGRESSIVE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(thisPed, 	CA_CAN_INVESTIGATE, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(thisPed,  CA_PLAY_REACTION_ANIMS, FALSE)
			SET_PED_COMBAT_ATTRIBUTES(thisPed,  CA_WILL_SCAN_FOR_DEAD_PEDS, FALSE)
			
			SET_PED_ALERTNESS(thisPed, AS_NOT_ALERT)
			SET_PED_COMBAT_ABILITY(thisPed, CAL_POOR)
			SET_PED_COMBAT_RANGE(thisPed, CR_NEAR)
			SET_PED_GENERATES_DEAD_BODY_EVENTS(thisPed, FALSE)
			SET_PED_SEEING_RANGE(thisPed, 0)
			SET_PED_HEARING_RANGE(thisPed, 0)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thisPed, TRUE)
			SET_PED_CAN_BE_DRAGGED_OUT(thisPed, FALSE) 
			SET_PED_HELMET(thisPed, TRUE) //FALSE)
			//GIVE_PED_HELMET(thisPed)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:	Sets the ped's bag attributes- whether he has one, and whether it is open
PROC SET_PED_BAG(PED_INDEX thisPed, BOOL bWearing, BOOL bOpen = FALSE)
	IF NOT IS_PED_INJURED(thisPed)
		
		IF thisPed = GET_SELECT_PED(SEL_MICHAEL)
		OR thisPed = GET_SELECT_PED(SEL_FRANKLIN)
			IF bWearing
				IF thisPed = GET_SELECT_PED(SEL_MICHAEL)
					SET_PED_COMPONENT_VARIATION(thisPed, PED_COMP_SPECIAL2, 1, 0)
				ELSE
					SET_PED_COMPONENT_VARIATION(thisPed, INT_TO_ENUM(PED_COMPONENT,9), 6, 0, 0) //(task)
				ENDIF
			ELSE
				SET_PED_COMPONENT_VARIATION(thisPed, PED_COMP_SPECIAL2, 0, 0)
			ENDIF
		ELSE
		
			IF GET_ENTITY_MODEL(thisPed) = HC_GUNMAN
				IF bWearing
					IF bOpen
						SET_PED_COMPONENT_VARIATION(thisPed, PED_COMP_SPECIAL2, 4, 0) //2, 0)
					ELSE
						SET_PED_COMPONENT_VARIATION(thisPed, PED_COMP_SPECIAL2, 4, 0)
					ENDIF
				ELSE
					SET_PED_COMPONENT_VARIATION(thisPed, PED_COMP_SPECIAL2, 0, 0)
				ENDIF
			ELSE
				IF bWearing
					SET_PED_COMPONENT_VARIATION(thisPed, PED_COMP_SPECIAL2, 1, 0)
				ELSE
					SET_PED_COMPONENT_VARIATION(thisPed, PED_COMP_SPECIAL2, 0, 0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



//PURPOSE:	Sets the ped's hat based on which path the ped is using
PROC SET_PED_HEADGEAR(PED_INDEX thisPed, BOOL bWearing)
	IF NOT IS_PED_INJURED(thisPed)
		IF bIsStealthPath
			IF thisPed = GET_SELECT_PED(SEL_MICHAEL)
				IF bWearing
					SET_PED_COMPONENT_VARIATION(thisPed, INT_TO_ENUM(PED_COMPONENT,8), 9, 0, 0) //(accs)
				ELSE
					//SET_PED_COMPONENT_VARIATION(thisPed, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
					RESTORE_PLAYER_PED_HAIRDO(thisPed)
				ENDIF
			ELSE
			
				IF bWearing
					IF GET_ENTITY_MODEL(thisPed) = HC_GUNMAN
						SET_PED_COMPONENT_VARIATION(thisPed, INT_TO_ENUM(PED_COMPONENT,6), 3, 0, 0) //(feet)
						SET_PED_PROP_INDEX(thisPed, INT_TO_ENUM(PED_PROP_POSITION,2), 0, 0)
						SET_PED_PROP_INDEX(thisPed, INT_TO_ENUM(PED_PROP_POSITION,0), 2, 0)
					ELSE
						//Wait for artists to give us the variations
						SET_PED_COMPONENT_VARIATION(thisPed, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
						SET_PED_COMPONENT_VARIATION(thisPed, INT_TO_ENUM(PED_COMPONENT,6), 1, 0, 0) //(feet)
					ENDIF
				ELSE
					IF GET_ENTITY_MODEL(thisPed) = HC_GUNMAN
						//SET_PED_COMPONENT_VARIATION(thisPed, PED_COMP_SPECIAL, 1, 0)
						SET_PED_COMPONENT_VARIATION(thisPed, INT_TO_ENUM(PED_COMPONENT,6), 1, 0, 0) //(feet)
						SET_PED_COMPONENT_VARIATION(thisPed, PED_COMP_SPECIAL, 0, 0)
					ELSE
						//Wait for artists to give us the variations
						SET_PED_COMPONENT_VARIATION(thisPed, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
						SET_PED_COMPONENT_VARIATION(thisPed, INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF thisPed = GET_SELECT_PED(SEL_MICHAEL)
				IF bWearing
				
					SET_PED_PROP_INDEX(thisPed, INT_TO_ENUM(PED_PROP_POSITION,0), 25, 0)	//black helmet
				
					//SET_PED_COMPONENT_VARIATION(thisPed, PED_COMP_SPECIAL, 7, 0)	//Balaclava
					SET_PED_COMPONENT_VARIATION(thisPed, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair) short so it doesn't stick through.
				ELSE
					//SET_PED_COMPONENT_VARIATION(thisPed, PED_COMP_SPECIAL, 0, 0)
					RESTORE_PLAYER_PED_HAIRDO(thisPed)
				ENDIF
			ELSE
				IF bWearing
					IF GET_ENTITY_MODEL(thisPed) = HC_GUNMAN
						//Gunman
						SET_PED_PROP_INDEX(thisPed, INT_TO_ENUM(PED_PROP_POSITION,0), 4, 0)
						//SET_PED_COMPONENT_VARIATION(thisPed, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
					ELSE
						//Driver
						SET_PED_PROP_INDEX(thisPed, INT_TO_ENUM(PED_PROP_POSITION,0), 3, 0)
						//SET_PED_COMPONENT_VARIATION(thisPed, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
					ENDIF
				ELSE
					IF GET_ENTITY_MODEL(thisPed) = HC_GUNMAN
						//Gunman
						CLEAR_PED_PROP(thisPed, INT_TO_ENUM(PED_PROP_POSITION,0))
						SET_PED_COMPONENT_VARIATION(thisPed, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
					ELSE
						//Driver
						//Wait for artists to give us the variations
						CLEAR_PED_PROP(thisPed, INT_TO_ENUM(PED_PROP_POSITION,0))
						SET_PED_COMPONENT_VARIATION(thisPed, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
						SET_PED_COMPONENT_VARIATION(thisPed, INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)						
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: 
PROC SETUP_FRANKLINS_SUIT()

	IF bIsStealthPath
		IF DOES_ENTITY_EXIST(GET_SELECT_PED(SEL_FRANKLIN))
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
				SET_PED_COMP_ITEM_CURRENT_SP(GET_SELECT_PED(SEL_FRANKLIN), COMP_TYPE_OUTFIT, OUTFIT_P1_EXTERMINATOR , FALSE)
			ENDIF
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(GET_SELECT_PED(SEL_FRANKLIN))
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
				SET_PED_COMP_ITEM_CURRENT_SP(GET_SELECT_PED(SEL_FRANKLIN), COMP_TYPE_OUTFIT, OUTFIT_P1_SUIT_1 , FALSE)
			ENDIF
		ENDIF		
	ENDIF
	
	
ENDPROC

//PURPOSE:	Sets ped's clothes based on the path the player has chosen
PROC SETUP_PLAYER_PEDS_SUITS(BOOL bWithMask = FALSE, BOOL bDoMichael = TRUE, BOOL bDoFranklin = TRUE)
	IF bIsStealthPath
		IF DOES_ENTITY_EXIST(GET_SELECT_PED(SEL_MICHAEL))
		AND bDoMichael
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
								
				SET_PED_COMP_ITEM_CURRENT_SP(GET_SELECT_PED(SEL_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_EXTERMINATOR , FALSE)
				
				SET_PED_COMPONENT_VARIATION(GET_SELECT_PED(SEL_MICHAEL), PED_COMP_HAND, 6, 0)
				
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_HEAD)
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_EYES)
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_EARS)
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_MOUTH)
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_LEFT_HAND)
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_RIGHT_HAND)
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_LEFT_WRIST)
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_RIGHT_WRIST)
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_HIP)
							
			ENDIF
		ENDIF
		IF DOES_ENTITY_EXIST(GET_SELECT_PED(SEL_FRANKLIN))
		AND bDoFranklin
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
				SET_PED_COMPONENT_VARIATION(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				//SET_PED_COMPONENT_VARIATION(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_COMPONENT,3), 1, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_COMPONENT,5), 4, 0, 0) //(hand)
				SET_PED_COMPONENT_VARIATION(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_COMPONENT,6), 7, 0, 0) //(feet)
				SET_PED_COMPONENT_VARIATION(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
				SET_PED_COMPONENT_VARIATION(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_COMPONENT,8), 14, 0, 0) //(accs)
				SET_PED_COMPONENT_VARIATION(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_COMPONENT,9), 0, 0, 0) //(task)
				SET_PED_COMPONENT_VARIATION(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_COMPONENT,10), 2, 0, 0) //(decl)
								
				//SET_PED_COMP_ITEM_CURRENT_SP(GET_SELECT_PED(SEL_FRANKLIN), COMP_TYPE_OUTFIT, OUTFIT_P1_EXTERMINATOR , FALSE)
					
				SET_PED_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_PROP_POSITION,0), 15, 0)
								
			ENDIF
		ENDIF
	ELSE
		IF DOES_ENTITY_EXIST(GET_SELECT_PED(SEL_MICHAEL))
		AND bDoMichael
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))

				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_HEAD)
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_EYES)
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_EARS)
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_MOUTH)
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_LEFT_HAND)
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_RIGHT_HAND)
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_LEFT_WRIST)
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_RIGHT_WRIST)
				CLEAR_PED_PROP(GET_SELECT_PED(SEL_MICHAEL), ANCHOR_HIP)
				
				SET_PED_COMP_ITEM_CURRENT_SP(GET_SELECT_PED(SEL_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_JEWEL_HEIST, FALSE)
								
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(GET_SELECT_PED(SEL_FRANKLIN))
		AND bDoFranklin
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
				SET_PED_COMP_ITEM_CURRENT_SP(GET_SELECT_PED(SEL_FRANKLIN), COMP_TYPE_OUTFIT, OUTFIT_P1_SUIT_1, FALSE)
			ENDIF
		ENDIF		
	ENDIF
	SET_PED_HEADGEAR(GET_SELECT_PED(SEL_MICHAEL), bWithMask)
	
	SET_PED_COMP_ITEM_CURRENT_SP(GET_SELECT_PED(SEL_MICHAEL), COMP_TYPE_PROPS, PROPS_P0_HEADSET)
	
ENDPROC

//PURPOSE:	Sets ped's clothes based on the path the player has chosen
PROC SETUP_CREW_SUITS(BOOL bWithMask = FALSE)
	
	IF bIsStealthPath
		IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
			SET_HEIST_CREW_MEMBER_OUTFIT(pedCrew[CREW_DRIVER_NEW], GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_DRIVER_NEW)), CREW_OUTFIT_PEST_CONTROL)
		ENDIF
		IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
			SET_HEIST_CREW_MEMBER_OUTFIT(pedCrew[CREW_GUNMAN_NEW], GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_GUNMAN_NEW)), CREW_OUTFIT_PEST_CONTROL)
		ENDIF
		IF NOT IS_PED_INJURED(pedCrew[CREW_HACKER])
			SET_HEIST_CREW_MEMBER_OUTFIT(pedCrew[CREW_HACKER], GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_HACKER)), CREW_OUTFIT_DEFAULT)
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
			SET_HEIST_CREW_MEMBER_OUTFIT(pedCrew[CREW_DRIVER_NEW], GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_DRIVER_NEW)), CREW_OUTFIT_SUIT)
		ENDIF
		IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
			SET_HEIST_CREW_MEMBER_OUTFIT(pedCrew[CREW_GUNMAN_NEW], GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_GUNMAN_NEW)), CREW_OUTFIT_SUIT)
		ENDIF
		IF NOT IS_PED_INJURED(pedCrew[CREW_HACKER])
			SET_HEIST_CREW_MEMBER_OUTFIT(pedCrew[CREW_HACKER], GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_HACKER)), CREW_OUTFIT_DEFAULT)
		ENDIF
	ENDIF
	
	INT i
	FOR i = 0 TO 3
		SET_PED_CREW(pedCrew[i])
	ENDFOR
	
	SET_PED_HEADGEAR(pedCrew[CREW_DRIVER_NEW], bWithMask)
	SET_PED_HEADGEAR(pedCrew[CREW_GUNMAN_NEW], bWithMask)
	
ENDPROC

PROC SET_BIKES_MISSION_SAFE()

	INT i
	FOR i = 0 TO 2
		IF NOT IS_ENTITY_DEAD(vehBike[i])
			IF NOT IS_ENTITY_A_MISSION_ENTITY(vehBike[i])
				SET_ENTITY_AS_MISSION_ENTITY(vehBike[i])
			ENDIF
			SET_VEHICLE_STRONG(vehBike[i], TRUE)
			SET_VEHICLE_CAN_LEAK_OIL(vehBike[i], FALSE)
			SET_VEHICLE_CAN_LEAK_PETROL(vehBike[i], FALSE)
			
			SET_VEHICLE_ENGINE_ON(vehBike[i], TRUE, TRUE)
			
			IF i <> 2
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBike[i], FALSE)
			ENDIF
			SET_VEHICLE_CAN_BE_VISIBLY_DAMAGED(vehBike[i], FALSE)
			SET_VEHICLE_TYRE_FIXED(vehBike[i], SC_WHEEL_BIKE_FRONT)
			SET_VEHICLE_TYRE_FIXED(vehBike[i], SC_WHEEL_BIKE_REAR)
			
			SET_VEHICLE_TYRES_CAN_BURST(vehBike[i], FALSE)
			SET_VEHICLE_HAS_UNBREAKABLE_LIGHTS(vehBike[i], TRUE) 
			
			//SET_VEHICLE_ON_GROUND_PROPERLY(vehBike[i])				
			SET_VEHICLE_COLOUR_COMBINATION(vehBike[i], i)
			
		ENDIF
	ENDFOR

ENDPROC


FUNC VECTOR CONVERT_ROTATION_TO_DIRECTION_VECTOR(VECTOR v_rot)
	RETURN <<-SIN(v_rot.z) * COS(v_rot.x), COS(v_rot.z) * COS(v_rot.x), SIN(v_rot.x)>>
ENDFUNC

PROC UPDATE_BIRDS()

	//Birds: fly away as the cars drive past
	
	INT i
	
	SWITCH iControlBirds
	
		CASE 0
			IF g_bBirdsJHeist = TRUE
				REPEAT COUNT_OF(s_birds) i
					s_birds[i].i_event = 0
				ENDREPEAT
				bFranklinSeesSomebirds = FALSE
				REQUEST_MODEL(model_bird)
				REQUEST_ANIM_DICT(str_bird_anims)
				iControlBirds++
			ENDIF
		BREAK
		
		CASE 1
			IF HAS_MODEL_LOADED(model_bird)
			AND HAS_ANIM_DICT_LOADED(str_bird_anims)
				iControlBirds++
			ENDIF
		BREAK
		
		CASE 2
			
			REPEAT COUNT_OF(s_birds) i
				IF s_birds[i].i_event = 0
					IF i = 0
						s_birds[i].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -592.2896, -293.4879, 49.3284 >> + <<0.0, 0.0, -0.15>>, 298.4107)
					ELIF i = 1
						s_birds[i].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -589.9229, -279.8143, 49.3288 >> + <<0.0, 0.0, -0.15>>, 233.7321)
					ELIF i = 2
						s_birds[i].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -581.4795, -283.9244, 49.9288 >> + <<0.0, 0.0, -0.15>>,  358.2680)
					ELIF i = 3
						s_birds[i].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -580.4676, -285.6026, 49.9288 >> + <<0.0, 0.0, -0.15>>, 199.0853)
					ELIF i = 4
						s_birds[i].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -587.5387, -278.9510, 49.3287 >> + <<0.0, 0.0, -0.15>>, 45.4051)
					ELIF i = 5
						s_birds[i].ped = CREATE_PED(PEDTYPE_MISSION, model_bird, << -589.8799, -288.1526, 49.3285 >> + <<0.0, 0.0, -0.15>>,  319.8120)
						SET_MODEL_AS_NO_LONGER_NEEDED(model_bird)
					ENDIF
					
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(s_birds[i].ped, TRUE)
					SET_PED_CAN_RAGDOLL(s_birds[i].ped, TRUE)
					FREEZE_ENTITY_POSITION(s_birds[i].ped, TRUE)
					SET_ENTITY_COLLISION(s_birds[i].ped, TRUE)
					SET_PED_CAN_BE_TARGETTED(s_birds[i].ped, FALSE)
					
					#IF IS_DEBUG_BUILD
						DebugName = "bird:"
						DebugName += i
						SET_PED_NAME_DEBUG(s_birds[i].ped, DebugName)
					#ENDIF
					
					REQUEST_SCRIPT_AUDIO_BANK("Seagulls")
									
				ENDIF
			ENDREPEAT
			iControlBirds++
		BREAK
		
		CASE 3
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-584.779175,-291.471680,49.077847>>, <<-591.995972,-278.703003,55.472153>>, 15.750000)
			OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-592.7419, -291.9719, 49.3270>>, <<2.15, 2.15, 2.0>>)
			OR bFranklinSeesSomebirds = TRUE	
				IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_FRANSCARE",  CONV_PRIORITY_HIGH)
					//TASK_PLAY_ANIM(PLAYER_PED_ID(),  Asset.adJewelHeist, "JH_2_IG_PIGEONS_franklin", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, 1000, AF_UPPERBODY | AF_SECONDARY)
					bFranklinSeesSomebirds = TRUE
					iControlBirds++
				ENDIF
			ENDIF	
		BREAK

		CASE 4
			
			g_bBirdsJHeist = FALSE
			
			REPEAT COUNT_OF(s_birds) i
				IF s_birds[i].i_event = 0
					IF NOT IS_PED_INJURED(s_birds[i].ped)			
						TASK_PLAY_ANIM(s_birds[i].ped, str_bird_anims, "idle", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_LOOPING)
						
						#IF IS_DEBUG_BUILD
							DebugName = "bird:"
							DebugName += i
							SET_PED_NAME_DEBUG(s_birds[i].ped, DebugName)
						#ENDIF
						
						s_birds[i].i_event = 1
					ENDIF
							
				ELIF DOES_ENTITY_EXIST(s_birds[i].ped)
				
			
									
					IF NOT IS_PED_INJURED(s_birds[i].ped)
						SWITCH s_birds[i].i_event
							CASE 1
								IF REQUEST_SCRIPT_AUDIO_BANK("Seagulls")
								AND VDIST2(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_ENTITY_COORDS(s_birds[i].ped)) < 625.0
									IF i = 0
										//PLAY_SOUND_FROM_ENTITY(-1, "Roof_Birds", s_birds[i].ped, "JEWEL_HEIST_SOUNDS")
										PLAY_SOUND_FROM_ENTITY(-1, "Seagulls", s_birds[i].ped, "JEWEL_HEIST_SOUNDS")
										//"Seagulls" from "JEWEL_HEIST_SOUNDS"
									ENDIF
									
									s_birds[i].i_timer = GET_GAME_TIMER()
									s_birds[i].i_event++
								ENDIF
							BREAK
							
							CASE 2
								IF GET_GAME_TIMER() - s_birds[i].i_timer > 100//500
									FREEZE_ENTITY_POSITION(s_birds[i].ped, FALSE)
									SET_ENTITY_COLLISION(s_birds[i].ped, TRUE)
									s_birds[i].v_dir = CONVERT_ROTATION_TO_DIRECTION_VECTOR(GET_ENTITY_ROTATION(s_birds[i].ped) + <<45.0, 0.0, 0.0>>)
									SET_ENTITY_VELOCITY(s_birds[i].ped, s_birds[i].v_dir * 7.0)
									TASK_PLAY_ANIM(s_birds[i].ped, str_bird_anims, "flapping", SLOW_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)// | AF_IGNORE_GRAVITY)
									
									s_birds[i].i_event++
								ENDIF
							BREAK
							
							CASE 3
								
								IF NOT IS_PED_INJURED(s_birds[i].ped)
									SET_ENTITY_VELOCITY(s_birds[i].ped, s_birds[i].v_dir * 7.0)
									
									IF IS_ENTITY_PLAYING_ANIM(s_birds[i].ped, str_bird_anims, "flapping")
										SET_ENTITY_ANIM_SPEED(s_birds[i].ped, str_bird_anims, "flapping", 1.0)
										s_birds[i].i_event++
									ENDIF
								ENDIF
							BREAK
							
							CASE 4
								IF NOT IS_PED_INJURED(s_birds[i].ped)
									SET_ENTITY_VELOCITY(s_birds[i].ped, s_birds[i].v_dir * 7.0)
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
					
					IF (NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -591.4834, -291.3098, 49.3284 >>, <<7.5, 7.5, 7.5>>) OR NOT IS_PLAYER_CONTROL_ON(PLAYER_ID()))
						DELETE_PED(s_birds[i].ped)
					ENDIF
				ELSE
					IF i = 0
						REMOVE_ANIM_DICT(str_bird_anims)
						RELEASE_NAMED_SCRIPT_AUDIO_BANK("Seagulls")
					ENDIF
				ENDIF
				
			ENDREPEAT	
		
		BREAK
	
	ENDSWITCH

ENDPROC

//═════════════════════════════════╡ DOOR CONTROL ╞═══════════════════════════════════
//PURPOSE:	Sets the doors of the jewel heist
PROC controlJewelStoreDoors(BOOL bLock, FLOAT fOpenRatio)	
	SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(P_JEWEL_DOOR_R1, <<-630.39, -238.51, 38.96>>, bLock, fOpenRatio)
	SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(P_JEWEL_DOOR_L, <<-632.05, -236.19, 38.96>>, bLock, -fOpenRatio)
ENDPROC

//═════════════════════════════════╡ END SCREEN ╞═══════════════════════════════════
//PURPOSE:	Displays the end screen
PROC displayEndScreen()
			
	IF g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE] > iNoOfCases
		//iTotalSmashedCases = 22
		PRINTLN("cases limit breached")
	ENDIF
			
	INT iRepairCost = (1000 - g_iJewelPrep1AVanHealth) * 5
	
//	iPotentialTake = 0
//	FOR iCounter = 0 TO iNoOfCases-1
//		iPotentialTake += jcStore[iCounter].iWorth
//	ENDFOR
	
	IF iCurrentTake - iRepairCost <= 0
		iCurrentTake = 0
	ENDIF
	
	INFORM_MISSION_STATS_OF_INCREMENT(JH2A_CASES_SMASHED, g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE]) 
	INFORM_MISSION_STATS_OF_INCREMENT(JH2A_ACTUAL_TAKE, iCurrentTake) 
	
	ADD_PENALTY_TO_HEIST(HEIST_JEWEL,HEIST_PENALTY_MONEY_DROPPED, ilostBag)
	
	SET_HEIST_END_SCREEN_POTENTIAL_TAKE(HEIST_JEWEL, iCurrentTake + ilostBag)	
	SET_HEIST_END_SCREEN_ACTUAL_TAKE(HEIST_JEWEL, iCurrentTake + ilostBag)	
	
	//DISPLAY_HEIST_END_SCREEN(HEIST_JEWEL)
	CLEAR_HELP(TRUE)
	
ENDPROC

FUNC BOOL HAS_MISSION_FAILED_FOR_REASON(MISSION_FAIL thisFailReason)
	IF bMissionFailed
		IF eMissionFail = thisFailReason
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC ENABLE_ALL_DISPATCH_SERVICES(BOOL bEnable)
	ENABLE_DISPATCH_SERVICE(DT_AMBULANCE_DEPARTMENT, bEnable)
	ENABLE_DISPATCH_SERVICE(DT_FIRE_DEPARTMENT, bEnable)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_AUTOMOBILE, bEnable)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_HELICOPTER, bEnable)
	ENABLE_DISPATCH_SERVICE(DT_POLICE_RIDERS, bEnable)
	ENABLE_DISPATCH_SERVICE(DT_SWAT_AUTOMOBILE, bEnable)
ENDPROC

//═════════════════════════════════╡ CLEANUP PROCEDURES ╞═══════════════════════════════════

//PURPOSE:		Deletes or removes set piece.
PROC CLEANUP_SET_PIECE_COP_CAR(SET_PIECE_COP &thisSetpiece, BOOL bDelete = FALSE)

	IF NOT bDelete
		SET_VEHICLE_AS_NO_LONGER_NEEDED(thisSetpiece.thisCar)
		SET_PED_AS_NO_LONGER_NEEDED(thisSetpiece.thisDriver)
		SET_PED_AS_NO_LONGER_NEEDED(thisSetpiece.thisPassenger)
	ELSE
		DELETE_VEHICLE(thisSetpiece.thisCar)
		DELETE_PED(thisSetpiece.thisDriver)
		DELETE_PED(thisSetpiece.thisPassenger)
	ENDIF
	
	IF DOES_BLIP_EXIST(thisSetpiece.blipDriver)
		REMOVE_BLIP(thisSetpiece.blipDriver)
	ENDIF
	IF DOES_BLIP_EXIST(thisSetpiece.blipPassenger)
		REMOVE_BLIP(thisSetpiece.blipPassenger)
	ENDIF
	
ENDPROC

FUNC BOOL REMOVE_BLIP_AND_CHECK_IF_EXISTS(BLIP_INDEX &thisBlip)

	IF DOES_BLIP_EXIST(thisBlip)
		REMOVE_BLIP(thisBlip)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC DELETE_ARRAY_OF_BLIPS(BLIP_INDEX &theseBlips[])

	INT i
	
	FOR i = 0 TO COUNT_OF(theseBlips) -1
		REMOVE_BLIP_AND_CHECK_IF_EXISTS(theseBlips[i])
		PRINTLN("deleted Blip in array at: ", i)		
	ENDFOR
	
ENDPROC

PROC SET_TRAIN_MODELS_AS_NO_LONGER_NEEDED()

	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHT)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCONT1)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCONT2)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTGRAIN)
	SET_MODEL_AS_NO_LONGER_NEEDED(TANKERCAR)
	SET_MODEL_AS_NO_LONGER_NEEDED(FREIGHTCAR)
					
ENDPROC

PROC RELEASE_TRUCK_FIGHT_ASSETS()

	SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnTruck)
	SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnPolice)
	SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnPoliceman)
	REMOVE_VEHICLE_RECORDING(Asset.recNoGoodBikeChase, Asset.recUber)
	REMOVE_VEHICLE_RECORDING(Asset.recNumber,Asset.recUber)
		
	REMOVE_VEHICLE_RECORDING(717, "JHUBER")
		
	SET_MODEL_FOR_CREW_MEMBER_AS_NO_LONGER_NEEDED(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)))
	SET_MODEL_FOR_CREW_MEMBER_AS_NO_LONGER_NEEDED(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)))
	REMOVE_WEAPON_ASSET(WEAPONTYPE_PISTOL)
	
ENDPROC

//PURPOSE:		Deletes or removes all elements of the script
PROC DELETE_ALL_MISSION_ENTITIES(BOOL bImmediately = FALSE)

	#IF IS_DEBUG_BUILD
		
		STOP_CUTSCENE(TRUE)
		
		WHILE NOT HAS_CUTSCENE_FINISHED()
			WAIT(0)
		ENDWHILE
		
		//WAIT(1000)
				
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	#ENDIF
	
	IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
		STOP_PED_SPEAKING(GET_SELECT_PED(SEL_MICHAEL), FALSE)
	ENDIF
	
	IF bIsStealthPath
		TRIGGER_MUSIC_EVENT("JH2A_MISSION_FAIL")
	ELSE
		TRIGGER_MUSIC_EVENT("JH2B_MISSION_FAIL")
	ENDIF
	
	SET_TIME_SCALE(1.0)
		
	bWarnedNotToLose = FALSE
	
	REMOVE_CUTSCENE()
	
	RELEASE_TRUCK_FIGHT_ASSETS()
	
	STOP_AUDIO_SCENES()
	
	IF sweatShop <> NULL
		UNPIN_INTERIOR(sweatShop)
	ENDIF
	
	STOP_ALARM("JEWEL_STORE_HEIST_ALARMS", TRUE)
	
	//REPLAY! Remove for replay setup stuff
	
	//Activate player control
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
	ENDIF
	
	//CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	
	//XXRELEASE_PATH_NODES()

	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	// REPLAY!
	
	CLEAR_ALL_BROKEN_GLASS()
	
	STOP_GAMEPLAY_HINT()
	SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
	ENDIF
	
	SET_RANDOM_TRAINS(TRUE)
	ENABLE_ALL_DISPATCH_SERVICES(TRUE)
	
	//bUberAlreadyPlaying = FALSE
	bAttemptToResetCases = FALSE
	bDrawCashScaleform = FALSE	
	
	iStageLestersIdle = 0
	iBalaclavaStage = 0
	
	cleanupTextManager()
	
	CLEANUP_UBER_PLAYBACK_AND_BIKES(TRUE)
	
	
		/*FOR iCounter = 0 TO 3
			STOP_PARTICLE_FX_LOOPED(ptfxKOGasEffect[iCounter])
//		ENDFOR*/
//		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxKOGasHaze)
//			STOP_PARTICLE_FX_LOOPED(ptfxKOGasHaze)
//		ENDIF

		
	//SET_PED_DENSITY_MULTIPLIER(1)
	
	//SET_RADAR_ZOOM(1100)
	
	CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
	
	STOP_SOUND(sndList[SND_ALARM_BELL].SoundId)

//═════════╡ INTERIOR ╞═════════
	
	//IF IS_INTERIOR_READY(interiorJewelStore)
		//UNPIN_INTERIOR(interiorJewelStore)
	//ENDIF
	//IF IS_INTERIOR_READY(interiorSweatshop)
		//UNPIN_INTERIOR(interiorSweatshop)
	//ENDIF
	//IF IS_INTERIOR_READY(interiorTunnel1)
	//	UNPIN_INTERIOR(interiorTunnel1)
	//ENDIF
	//IF IS_INTERIOR_READY(interiorTunnel2)
		//UNPIN_INTERIOR(interiorTunnel2)
	//ENDIF
	
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_PED_POPULATION_BUDGET(3)
	
	IF bIsDoorLocked
		controlJewelStoreDoors(FALSE, 0.0)
		bIsDoorLocked = FALSE
	ENDIF
	
	

//═════════╡ BLIPS ╞═════════		
	
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipAirVent)
	
	DELETE_ARRAY_OF_BLIPS(blipCrew)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipLocate)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipBike)
	
	DELETE_ARRAY_OF_BLIPS(blipPolice)
	DELETE_ARRAY_OF_BLIPS(blipPassenger)
	
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipGuard)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipTruck)
	REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipFlee)
	
	FOR iCounter = 0 TO iNoOfCases-1
		REMOVE_BLIP_AND_CHECK_IF_EXISTS(jcStore[iCounter].blipCase)
	ENDFOR	
	
	
	DELETE_ARRAY_OF_BLIPS(vehPoliceBlips)
	

//═════════╡ OBJECTS ╞═════════
	CLEANUP_OBJECT(objGrenade, TRUE)
	CLEANUP_OBJECT(objBag, TRUE)
	CLEANUP_OBJECT(objPickup, TRUE)
	CLEANUP_OBJECT(objPhone, TRUE)
	
	CLEANUP_OBJECT(oiACTarget, TRUE)
	
	DELETE_OBJECT(objWeapon)
	DELETE_OBJECT(objWeapon1)
	DELETE_OBJECT(objWeapon2)
	
	DELETE_OBJECT(oiLestersStick)
				
//═════════╡ MODELS + ANIMS + RECORDINGS ╞═════════
	TIDY_UP_NEED_LIST()

	REMOVE_ANIM_DICT("missheist_jewellester_waitidles")
	REMOVE_ANIM_DICT("move_m@generic_variations@idle@b")

//═════════╡ VEHICLES ╞═════════
	IF HAS_MISSION_FAILED_FOR_REASON(FAIL_LOSE_CHASE)
		CLEANUP_VEHICLE(vehBike[0], TRUE)
		CLEANUP_VEHICLE(vehBike[1], TRUE)
		CLEANUP_VEHICLE(vehBike[2], bImmediately)
		CLEANUP_VEHICLE(vehBike[3], TRUE)
	ELSE
		FOR iCounter = 0 TO 3
			CLEANUP_VEHICLE(vehBike[iCounter], bImmediately)
		ENDFOR
	ENDIF
	CLEANUP_VEHICLE(vehCar, bImmediately)
	FOR iCounter = 0 TO iNoOfCops-1
		CLEANUP_VEHICLE(vehPolice[iCounter], bImmediately)
	ENDFOR
	CLEANUP_VEHICLE(vehTruck, bImmediately)
	CLEANUP_VEHICLE(vehCollision, bImmediately)
	CLEANUP_VEHICLE(vehVan, bImmediately)
	CLEANUP_VEHICLE(vehControl, TRUE)

	IF DOES_ENTITY_EXIST(vehTrainDodge)
		IF bImmediately
			DELETE_MISSION_TRAIN(vehTrainDodge)
		ELSE
			SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(vehTrainDodge)
		ENDIF
	ENDIF
	
	SET_TRAIN_MODELS_AS_NO_LONGER_NEEDED()
	
//═════════╡ SCALEFORM ╞═════════	
	
	REMOVE_VEHICLE_RECORDING(1, "JH2BArrive")
	REMOVE_VEHICLE_RECORDING(2, "JH2BArrive")
	REMOVE_VEHICLE_RECORDING(1, "JHBENSONEXIT")
	
//═════════╡ PEDS ╞═════════


	DELETE_PED(copOnLeft1)
	DELETE_PED(copOnLeft2)
	DELETE_PED(copOnLeft3)
	
	DELETE_PED(copOnRight1)
	DELETE_PED(copOnRight2)
	DELETE_PED(copOnRight3)
	
	DELETE_PED(pedTrafficWarden)

	IF HAS_MISSION_FAILED_FOR_REASON(FAIL_LOSE_CHASE)
		FOR iCounter = 0 TO 3
			CLEANUP_PED(pedCrew[iCounter], TRUE)
		ENDFOR
	ELSE
		FOR iCounter = 0 TO 3
			CLEANUP_PED(pedCrew[iCounter], bImmediately)
		ENDFOR
	ENDIF
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		OR IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			SET_PED_BAG(GET_SELECT_PED(SEL_FRANKLIN), FALSE)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		IF NOT IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
		OR IS_PED_INJURED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
			SET_PED_BAG(GET_SELECT_PED(SEL_MICHAEL), FALSE)
			SET_PED_HEADGEAR(GET_SELECT_PED(SEL_MICHAEL), FALSE)
		ENDIF
	ENDIF
	IF NOT bImmediately
		CLEANUP_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], bImmediately)
		CLEANUP_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], bImmediately)
	ENDIF
	FOR iCounter = 0 TO iNoOfCops-1
		CLEANUP_PED(pedPolice[iCounter], bImmediately)
	ENDFOR
	FOR iCounter = 0 TO 3
		CLEANUP_PED(pedPolicePassenger[iCounter], bImmediately)
	ENDFOR
	FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
		CLEANUP_PED(pedStore[iCounter], bImmediately)
	ENDFOR
	
	CLEANUP_PED(pedSecurity, bImmediately)
	CLEANUP_PED(pedBackroom, bImmediately)
	CLEANUP_PED(pedLester, bImmediately)
	
	DELETE_PED(copAtExit1)
	DELETE_PED(copAtExit2)
	DELETE_PED(copAtExit3)
	
//========= Set piecse ===========

	
//═════════╡ WEAPON ASSETS ╞═════════
	REMOVE_WEAPON_ASSET(WEAPONTYPE_PISTOL)
	REMOVE_WEAPON_ASSET(mnMainWeaponType)
	REMOVE_WEAPON_ASSET(WEAPONTYPE_PUMPSHOTGUN)
	REMOVE_WEAPON_ASSET(WEAPONTYPE_SMG)

//═════════╡ COVER POINT ╞═════════
	REMOVE_COVER_POINT(covCrew[0])
	REMOVE_COVER_POINT(covCrew[1])
	
//═════════╡ SEQUENCES ╞═════════
	CLEAR_SEQUENCE_TASK(seqSequence)
	CLEAR_SEQUENCE_TASK(seqLeave)
	
//═════════╡ CAMS ╞═════════
	IF DOES_CAM_EXIST(camAnim)
		DESTROY_CAM(camAnim)
	ENDIF
	IF DOES_CAM_EXIST(initialCam)
		DESTROY_CAM(initialCam)
	ENDIF
	IF DOES_CAM_EXIST(destinationCam)
		DESTROY_CAM(destinationCam)
	ENDIF
	IF DOES_CAM_EXIST(scsSwitchCam.ciSpline)
		SET_CAM_ACTIVE(scsSwitchCam.ciSpline, FALSE)
		DESTROY_CAM(scsSwitchCam.ciSpline)
	ENDIF
	DESTROY_ALL_CAMS()
	
	SET_TIME_SCALE(1.0)
	
	RENDER_SCRIPT_CAMS(FALSE, TRUE, 1000, TRUE)
	

	DESTROY_ALL_CAMS()

	DO_END_CUTSCENE()
			
	STOP_GAMEPLAY_CAM_SHAKING(TRUE)
	SET_WIDESCREEN_BORDERS(FALSE, 0)
	IF NOT IS_PED_INJURED(GET_PLAYER_PED(PLAYER_ID()))
		IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		ENDIF
	ENDIF
//═════════╡ GROUPS ╞═════════	
	REMOVE_RELATIONSHIP_GROUP(grpShop)
	REMOVE_RELATIONSHIP_GROUP(grpCop)
	
//	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
//		CLEAR_PED_TASKS(PLAYER_PED_ID())
//	ENDIF
		
//═════════╡ RECORDINGS ╞═════════	

	REMOVE_VEHICLE_RECORDING(1, "JHGetBike")
	REMOVE_VEHICLE_RECORDING(2, "JHGetBike")
	REMOVE_VEHICLE_RECORDING(3, "JHGetBike")
	REMOVE_VEHICLE_RECORDING(4, "JHGetBike")
	
//	#IF IS_DEBUG_BUILD
//		IF DOES_WIDGET_GROUP_EXIST(widgetDebug)
//			DELETE_WIDGET_GROUP(widgetDebug)
//		ENDIF
//	#ENDIF
	
	STOP_SCRIPTED_CONVERSATION(FALSE)
ENDPROC

PROC MissionCleanup()

	#IF IS_DEBUG_BUILD
				
		g_b_CellDialDebugTextToggle = TRUE
		
	#ENDIF
	
	STOP_CUTSCENE(TRUE)
	PRINTLN("JEWEL HEIST STOP_CUTSCENE CALLED!")
	
	DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, FALSE)
	DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, FALSE)
	
	LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_DRAINSERVICE_L, FALSE)
	LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_DRAINSERVICE_R, FALSE)
		
	#IF IS_NEXTGEN_BUILD
		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxGas)
			STOP_PARTICLE_FX_LOOPED(ptfxGas)
		ENDIF
	#ENDIF

	#IF NOT IS_NEXTGEN_BUILD
		IF bIsStealthPath
			REMOVE_TCMODIFIER_OVERRIDE("NEW_Jewel")
		ENDIF
		CLEAR_TIMECYCLE_MODIFIER()
	#ENDIF
	
	SET_FOLLOW_VEHICLE_CAM_HIGH_ANGLE_MODE_EVERY_UPDATE(FALSE, FALSE)
	IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
		REMOVE_SHOCKING_EVENT(ENUM_TO_INT(EVENT_SHOCKING_MAD_DRIVER))
		REMOVE_PED_HELMET(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
		SET_PED_HELMET_TEXTURE_INDEX(GET_SELECT_PED(SEL_FRANKLIN),-1) 
	ENDIF
	
	IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
		REMOVE_PED_HELMET(GET_SELECT_PED(SEL_MICHAEL), TRUE)
		SET_PED_HELMET_TEXTURE_INDEX(GET_SELECT_PED(SEL_MICHAEL),-1) 
	ENDIF
	
	FOR iCounter = 0 TO iNoOfCases-1
		rfReset = GET_RAYFIRE_MAP_OBJECT(	jcStore[iCounter].vCasePos, fRFCheckRadius, jcStore[iCounter].sSmashAnim)
		IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfReset)
			IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfReset) <> RFMO_STATE_START
				SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfReset, RFMO_STATE_RESET)
			ENDIF
		ENDIF
	ENDFOR
	
	DISABLE_INTERIOR( GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-164.3822, -619.0884, 33.3318>>, "dt1_02_carpark"), FALSE)
	DISABLE_INTERIOR( GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-179.3140, -180.2154, 42.6246>>, "bt1_04_carpark"), FALSE)
	DISABLE_INTERIOR( GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-16.2958, -684.0385, 33.5083>>, "dt1_03_carpark"), FALSE)
	DISABLE_INTERIOR( GET_INTERIOR_AT_COORDS_WITH_TYPE(<<141.2044, -717.2167, 32.1327>>, "dt1_05_carpark"), FALSE)
	
	SET_GAME_PAUSES_FOR_STREAMING(TRUE)
	
	IF IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_STOP()
	ENDIF
	
	CLEAR_WEATHER_TYPE_PERSIST()
	
	SET_PARTICLE_FX_BULLET_IMPACT_LODRANGE_SCALE(1.0)
	
	DISABLE_NAVMESH_IN_AREA(<<-623.1895, -231.5713, 36.8759>>, <<-620.6600, -229.6830, 38.0054>>, FALSE)
	
	SET_PLAYER_PED_DATA_IN_CUTSCENES(TRUE, TRUE)		
	
	SET_RADIO_POSITION_AUDIO_MUTE(FALSE)
	IF DOES_ENTITY_EXIST(oiStoreDoorMap)
		SET_ENTITY_VISIBLE(oiStoreDoorMap, TRUE)
		SET_ENTITY_COLLISION(oiStoreDoorMap, TRUE)
	ENDIF							
	DELETE_OBJECT(oiStoreDoorTemp)
	DISABLE_TAXI_HAILING(FALSE)
		
	DISTANT_COP_CAR_SIRENS(FALSE)
	SET_CINEMATIC_BUTTON_ACTIVE(TRUE)	
	DISABLE_TAXI_HAILING(FALSE)
	DELETE_ALL_MISSION_ENTITIES()
	DISABLE_CELLPHONE(FALSE)
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(TRUE)
	
	UNREGISTER_SCRIPT_WITH_AUDIO()
	
	IF bIsStealthPath
		TRIGGER_MUSIC_EVENT("JH2A_MISSION_FAIL")
	ELSE
		TRIGGER_MUSIC_EVENT("JH2B_MISSION_FAIL")
	ENDIF
	
	RELEASE_SUPPRESSED_EMERGENCY_CALLS()
	
	eSwitchCamState = SWITCH_CAM_IDLE

	IF DOES_CAM_EXIST(scsSwitchCam.ciSpline)
		SET_CAM_ACTIVE(scsSwitchCam.ciSpline, FALSE)
		DESTROY_CAM(scsSwitchCam.ciSpline)
	ENDIF
	DESTROY_ALL_CAMS()	
		
	sCamDetails.bPedSwitched = FALSE

//	IF CANCEL_MUSIC_EVENT("JH2A_MISSION_START_OS")
//		#IF IS_DEBUG_BUILD	PRINTLN("STOPPED AUDIO EVENT... [PROLOGUE_TEST_MISSION_START]")	#ENDIF
//	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bIsSuperDebugEnabled
			IF IS_SCREEN_FADED_OUT()
				DO_SCREEN_FADE_IN(500)
			ENDIF
		ENDIF
	#ENDIF
	
	
ENDPROC

INT m_iLeadoutScene
CAMERA_INDEX m_camLeadoutScene
CAMERA_INDEX camStripClubEstablisher
INT iFakeSwitchStage

OBJECT_INDEX oiPhoneProp

FUNC BOOL TRIGGER_HACKED_STRIP_CLUB_SWITCH()

    FLOAT fStartPhase
    FLOAT fTemp			    

	HIDE_HUD_AND_RADAR_THIS_FRAME()
	
	SWITCH iFakeSwitchStage
	
		CASE 0
			TRIGGER_MISSION_STATS_UI(TRUE, TRUE)
			IF IS_REPEAT_PLAY_ACTIVE()
				REQUEST_SCRIPT("controller_AmbientArea")		
				WHILE NOT HAS_SCRIPT_LOADED("controller_AmbientArea")
					WAIT(0)
				ENDWHILE
			
				START_NEW_SCRIPT("controller_AmbientArea", DEFAULT_STACK_SIZE)
			
			ENDIF
			
			DISABLE_NAVMESH_IN_AREA(<<(115.1569 - 5.0), (-1286.6840 - 5.0), (28.2613 - 5.0)>>, <<(115.1569 + 5.0), (-1286.6840 + 5.0), (28.2613 + 5.0)>>, TRUE)
			
			KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			//Wait a frame
			iFakeSwitchStage++
		BREAK
	
		CASE 1
			SETTIMERA(0)
			
			//[MF] If any cutscene is still loaded, make sure it's unloaded.  Fix for B* 1695524
			IF HAS_CUTSCENE_LOADED()
				STOP_CUTSCENE_IMMEDIATELY()
				REMOVE_CUTSCENE()
			ENDIF			
			
			iFakeSwitchStage++
		BREAK
		
		CASE 2
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("mission_stat_watcher")) < 1
			OR TIMERA() > 10000	//Give the stats screen 10 seconds to display otherwise move on.
				iFakeSwitchStage++
			ELSE
				IF g_bResultScreenDisplaying
					REQUEST_ANIM_DICT("SWITCH@FRANKLIN@STRIPCLUB")
					REQUEST_MODEL(Prop_phone_ING_03)
					SETTIMERA(0)
					iFakeSwitchStage++
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
		
			IF HAS_ANIM_DICT_LOADED("SWITCH@FRANKLIN@STRIPCLUB")
			AND HAS_MODEL_LOADED(Prop_phone_ING_03)			
			
				IF NOT g_bResultScreenDisplaying
				
					CLEAR_AREA_OF_VEHICLES(<<153.3041, -1308.0714, 31.1902>>, 50.0, FALSE)
				
					camStripClubEstablisher = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
					
					SET_CAM_COORD(camStripClubEstablisher ,<<153.3041, -1308.0714, 31.1902>>)
					SET_CAM_ROT(camStripClubEstablisher ,<<1.6837, 0.2426, 67.9347>>)
					SET_CAM_FOV(camStripClubEstablisher ,45.0)
					SET_CAM_ACTIVE(camStripClubEstablisher, TRUE)
					SHAKE_CAM(camStripClubEstablisher, "HAND_SHAKE", 1.0)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					
					SETTIMERA(0)
					SET_NO_LOADING_SCREEN(FALSE) 
					DO_SCREEN_FADE_IN(500)
					
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 1, PLAYER_PED_ID(), "FRANKLIN")
					ADD_PED_FOR_DIALOGUE(myScriptedSpeech, 4, NULL, "TANISHA")
					
					iFakeSwitchStage++
				ENDIF
			
			ENDIF
		BREAK
	
		CASE 4
			
			IF IS_CONVERSATION_STATUS_FREE()
				IF PRELOAD_CONVERSATION(myScriptedSpeech, "PRSAUD", "PSF_ARM3_D", CONV_PRIORITY_VERY_HIGH)
					iFakeSwitchStage++
				ENDIF
			ENDIF
		BREAK
			
		CASE 5
			IF TIMERA() > 4000
			AND (IS_BITMASK_AS_ENUM_SET(g_StripclubGlobals.iStripclubFlags, GSF_IN_CLUB)
					OR TIMERA() > 10000)
					
				m_iLeadoutScene = CREATE_SYNCHRONIZED_SCENE(<<115.1569, -1286.6840, 28.2613>>, << -0.000, 0.000, 111.0000>>)
				m_camLeadoutScene = CREATE_CAM("DEFAULT_ANIMATED_CAMERA", TRUE)
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(m_iLeadoutScene, FALSE)
				SET_SYNCHRONIZED_SCENE_LOOPED(m_iLeadoutScene, FALSE)
				SET_SYNCHRONIZED_SCENE_PHASE(m_iLeadoutScene, 0.0)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), m_iLeadoutScene, "SWITCH@FRANKLIN@STRIPCLUB", "002113_02_FRAS_15_STRIPCLUB_EXIT", INSTANT_BLEND_IN, WALK_BLEND_OUT)
				PLAY_SYNCHRONIZED_CAM_ANIM(m_camLeadoutScene, m_iLeadoutScene, "002113_02_FRAS_15_STRIPCLUB_EXIT_CAM", "SWITCH@FRANKLIN@STRIPCLUB")
				
				SET_CAM_ACTIVE(m_camLeadoutScene, TRUE)
				
				oiPhoneProp = CREATE_OBJECT(Prop_phone_ING_03, <<115.1569, -1286.6840, 28.2613>>)
				
				ATTACH_ENTITY_TO_ENTITY(oiPhoneProp, GET_SELECT_PED(SEL_FRANKLIN), GET_PED_BONE_INDEX(GET_SELECT_PED(SEL_FRANKLIN), BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				
		
				BEGIN_PRELOADED_CONVERSATION()
				iFakeSwitchStage++
			ENDIF
		BREAK
			
		CASE 6
		
			IF IS_SYNCHRONIZED_SCENE_RUNNING(m_iLeadoutScene)			
				DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CHARACTER_WHEEL)
			    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY)
			    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_SECONDARY)
			    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPECIAL_ABILITY_PC)
			    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DUCK)
			    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
			    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_COVER)
			    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
			    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DIVE)
			    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
			    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ENTER)
			    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_ATTACK)
			    HIDE_HUD_AND_RADAR_THIS_FRAME()
			    
				IF NOT FIND_ANIM_EVENT_PHASE("SWITCH@FRANKLIN@STRIPCLUB", "002113_02_FRAS_15_STRIPCLUB_EXIT", "WALKINTERRUPTABLE", fStartPhase, fTemp)
			        fStartPhase = 0.9
			    ENDIF
			    IF GET_SYNCHRONIZED_SCENE_PHASE(m_iLeadoutScene) >= fStartPhase
				
					IF DOES_ENTITY_EXIST(oiPhoneProp)
						DELETE_OBJECT(oiPhoneProp)
					ENDIF
					FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE, TRUE)
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					SET_ENTITY_INVINCIBLE(PLAYER_PED_ID(), FALSE)
					REMOVE_ANIM_DICT("SWITCH@FRANKLIN@STRIPCLUB")
					
					DISABLE_NAVMESH_IN_AREA(<<(115.1569 - 5.0), (-1286.6840 - 5.0), (28.2613 - 5.0)>>, <<(115.1569 + 5.0), (-1286.6840 + 5.0), (28.2613 + 5.0)>>, FALSE)
					
					RETURN TRUE
			    ENDIF
			ENDIF
			
			
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE

ENDFUNC

///Creates a new load scene sphere and waits until it has finished loading or it times out.
PROC NEW_LOAD_SCENE_SPHERE_WITH_WAIT(VECTOR vPos, FLOAT fRadius, NEWLOADSCENE_FLAGS controlFlags = 0, INT iMaxWaitTime = 10000)
      INT iTimeOut = GET_GAME_TIMER()

      WHILE GET_GAME_TIMER() - iTimeOut < iMaxWaitTime
            IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
                  NEW_LOAD_SCENE_START_SPHERE(vPos, fRadius, controlFlags)
            ELIF IS_NEW_LOAD_SCENE_LOADED()
                  iTimeOut = 0
            ENDIF
            
            WAIT(0)
      ENDWHILE
      
      NEW_LOAD_SCENE_STOP()   
ENDPROC


VEHICLE_INDEX vehFranklinsCar

//PURPOSE:		Performs necessary actions for player completing the mission, then calls cleanup
PROC MissionPassed()	
	
	SET_PED_BAG(GET_SELECT_PED(SEL_MICHAEL), FALSE)
	IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_FRANKLIN))
		REMOVE_PED_HELMET(GET_SELECT_PED(SEL_FRANKLIN), FALSE)		
	ENDIF
//	CLEAR_ALL_PED_PROPS(GET_SELECT_PED(SEL_FRANKLIN))
//	CLEAR_ALL_PED_PROPS(GET_SELECT_PED(SEL_MICHAEL))
		
	IF bIsStealthPath
		TRIGGER_MUSIC_EVENT("JH2A_MISSION_FAIL")
	ELSE
		TRIGGER_MUSIC_EVENT("JH2B_MISSION_FAIL")
	ENDIF

	SET_CLOCK_TIME(6,00,0)
	RESTORE_PLAYER_PED_VARIATIONS(GET_SELECT_PED(SEL_FRANKLIN))
	
	END_REPLAY_SETUP()
	
	g_sAutosaveData.bIgnoreScreenFade = TRUE
	displayEndScreen()
	
	CLEAR_PED_WETNESS(GET_SELECT_PED(SEL_FRANKLIN))
	CLEAR_PED_BLOOD_DAMAGE(GET_SELECT_PED(SEL_FRANKLIN))
	RESET_PED_VISIBLE_DAMAGE(GET_SELECT_PED(SEL_FRANKLIN))
			
	IF NOT DOES_ENTITY_EXIST(GET_SELECT_PED(SEL_FRANKLIN))
	OR IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
		DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
		WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN,<<117.2587, -1285.1025, 27.2705>>, 98.4916)
			WAIT(0)
		ENDWHILE
	ENDIF
	
	WHILE NOT CREATE_PLAYER_VEHICLE(vehFranklinsCar, CHAR_FRANKLIN, <<138.1478, -1301.5951, 28.1939>>, 120.2019, FALSE, VEHICLE_TYPE_CAR)
		WAIT(0)
	ENDWHILE
	
	IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
		MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
		TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
	ENDIF
	
//	CPRINTLN(DEBUG_ACHIEVEMENT, "ACH05 - AWARD ACHIEVEMENT")
//	AWARD_ACHIEVEMENT_FOR_MISSION(ACH05) // diamond hard
	
	
	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
	
	DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
	
	//DO_PLAYER_WARP_WITH_LOAD_AND_PAUSE(g_sPedSceneData[PR_SCENE_Fa_STRIPCLUB_ARM3].vCreateCoords)
	
	SET_ENTITY_COORDS(PLAYER_PED_ID(), g_sPedSceneData[PR_SCENE_Fa_STRIPCLUB_ARM3].vCreateCoords)
	//LOAD_SCENE(g_sPedSceneData[PR_SCENE_Fa_STRIPCLUB_ARM3].vCreateCoords)	
	
	NEW_LOAD_SCENE_SPHERE_WITH_WAIT(g_sPedSceneData[PR_SCENE_Fa_STRIPCLUB_ARM3].vCreateCoords, 40.0)
	
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(PLAYER_PED_ID(), FALSE)
		
	WHILE NOT TRIGGER_HACKED_STRIP_CLUB_SWITCH()
		//CPRINTLN(DEBUG_ACHIEVEMENT, "ACH05 - HACKED STRIP CLUB SWITCH")
		WAIT(0)
	ENDWHILE
			
	//Trigger_Specific_Switch_And_Wait(PR_SCENE_Fa_STRIPCLUB_ARM3, ENUM_TO_INT(SWITCH_FLAG_DESCENT_ONLY))
	
	// Steve R - Disable interiors of Jewel Store, setting to boarded up state and Max Renda store
	SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_STORE, BUILDINGSTATE_CLEANUP, TRUE)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_HEIST_MAX_RENDA_INTERIOR, BUILDINGSTATE_NORMAL, TRUE)
	
	//CPRINTLN(DEBUG_ACHIEVEMENT, "ACH05 - MISSION FLOW PASSED")
	AWARD_ACHIEVEMENT_FOR_MISSION(ACH05) // diamond hard
	Mission_Flow_Mission_Passed()
	MissionCleanup()
	
	TERMINATE_THIS_THREAD()	
	
ENDPROC

//PURPOSE:		Performs necessary actions for player failing the mission, then calls cleanup
PROC MissionFailed()	

	SWITCH eMissionFail
		CASE FAIL_GENERIC
			//Display generic fail text - "Mission Failed"
			MISSION_FLOW_SET_FAIL_REASON(Asset.godFail) //, TRUE)
			PRINTLN("Fail Generic")
		BREAK
		
		CASE FAIL_SHOP_CLOSED
			MISSION_FLOW_SET_FAIL_REASON("JH_FAILCLOSE") //, TRUE)
			PRINTLN("Fail FAIL_SHOP_CLOSED")
		BREAK
		
		CASE FAIL_ABANDON
			//Display generic fail text - "Mission Failed"
			MISSION_FLOW_SET_FAIL_REASON(Asset.failAbandon) //, TRUE)
			PRINTLN("Heist Abandoned")
		BREAK
		CASE FAIL_KILL_MICHAEL
			MISSION_FLOW_SET_FAIL_REASON(Asset.failMichaelDead) //, TRUE)
			PRINTLN("Fail Kill Michael")
		BREAK
		CASE FAIL_KILL_TREVOR
			MISSION_FLOW_SET_FAIL_REASON(Asset.failTrevorDead) //, TRUE)
			PRINTLN("Fail Kill Trevor")
		BREAK
		CASE FAIL_KILL_CREW
			MISSION_FLOW_SET_FAIL_REASON(Asset.failCrewDead) //, TRUE)
			PRINTLN("Fail Kill Crew")
		BREAK
		
		CASE FAIL_KILL_HACKER
			MISSION_FLOW_SET_FAIL_REASON("JH2_HCKDEAD") //, TRUE)
			PRINTLN("Fail Kill hacker")
		BREAK
		CASE FAIL_KILL_GUNMAN
			MISSION_FLOW_SET_FAIL_REASON("JH2_HCKGUN") //, TRUE)
			PRINTLN("Fail Kill Gunman")
		BREAK
		CASE FAIL_KILL_DRIVER
			MISSION_FLOW_SET_FAIL_REASON("JH2_HCKDRV") //, TRUE)
			PRINTLN("Fail Kill Driver")
		BREAK
		CASE FAIL_DESTROY_BIKE
			MISSION_FLOW_SET_FAIL_REASON(Asset.failBikeDestroy) //, TRUE)
			PRINTLN("Fail Destroy Bike")
		BREAK
		CASE FAIL_DESTROY_VAN
			MISSION_FLOW_SET_FAIL_REASON(Asset.failVanDestroyed) //, TRUE)
			PRINTLN("Fail Destroy Van")
		BREAK
		CASE FAIL_DESTROY_CAR
			MISSION_FLOW_SET_FAIL_REASON(Asset.failCarDestroyed) //, TRUE)
			PRINTLN("Fail Destroy Car")
		BREAK
		CASE FAIL_DESTROY_TRUCK
			MISSION_FLOW_SET_FAIL_REASON(Asset.failTruckDestroyed) //, TRUE)
			PRINTLN("Fail Destroy Truck")
		BREAK
		CASE FAIL_LOSE_CHASE
			MISSION_FLOW_SET_FAIL_REASON(Asset.failChase) //, TRUE)
			PRINTLN("Fail Lose Chase")
		BREAK
		CASE FAIL_LOSE_BIKE
			MISSION_FLOW_SET_FAIL_REASON(Asset.failBikeLost) //, TRUE)
			PRINTLN("Fail Lose Bike")
		BREAK
		CASE FAIL_LOSE_MICHAEL
			MISSION_FLOW_SET_FAIL_REASON(Asset.failMichaelLost) //, TRUE)
			PRINTLN("Fail Lose Michael")
		BREAK
		CASE FAIL_LOSE_FRANKLIN
			MISSION_FLOW_SET_FAIL_REASON(Asset.failFranklinLost) //, TRUE)
			PRINTLN("Fail Lose Franklin")
		BREAK
		CASE FAIL_LOSE_CREW
			MISSION_FLOW_SET_FAIL_REASON(Asset.failCrewLost) //, TRUE)
			PRINTLN("Fail Lose Crew")
		BREAK
		CASE FAIL_KILL_FRANKLIN
			MISSION_FLOW_SET_FAIL_REASON(Asset.failFranklinDead) //, TRUE)
			PRINTLN("Fail Kill Franklin")
		BREAK
		CASE FAIL_CALL_POLICE
			MISSION_FLOW_SET_FAIL_REASON(Asset.failPoliceCalled) //, TRUE)
			PRINTLN("Fail Call Police")
		BREAK
		CASE FAIL_SECURITY_ESCAPED
			MISSION_FLOW_SET_FAIL_REASON("F_SECESCAPE") //, TRUE)
			PRINTLN("Fail Security guard escaped")
		BREAK		
		CASE FAIL_DIDNT_STEAL_ENOUGH_DIAMONDS
			MISSION_FLOW_SET_FAIL_REASON("F_INSUFTAKE") //, TRUE)
			PRINTLN("Fail didn't steal enough diamonds")
		BREAK	
		CASE FAIL_ARRESTED
			MISSION_FLOW_SET_FAIL_REASON("F_ARRESTED") //, TRUE)
			PRINTLN("Fail got arrested for hanging around too long")
		BREAK	
		CASE FAIL_BLEW_HEIST
			MISSION_FLOW_SET_FAIL_REASON("F_BLEW") //, TRUE)
			PRINTLN("Fail got attracted the cops")
		BREAK
		CASE FAIL_BLEW_ID
			MISSION_FLOW_SET_FAIL_REASON("F_BLEWID") //, TRUE)
			PRINTLN("Fail blew your cover")
		BREAK
		
		
		CASE FAIL_GAS
			MISSION_FLOW_SET_FAIL_REASON("JWL_GASFAIL") //, TRUE)
			PRINTLN("Fail got gassed the peeps on the street the cops")
		BREAK
		CASE FAIL_NO_GAS
			MISSION_FLOW_SET_FAIL_REASON("F_GAS") //, TRUE)
			PRINTLN("Fail you ran out of smoke grenades.")
		BREAK
		
	ENDSWITCH

	Mission_Flow_Mission_Failed()
	
	IF bIsStealthPath
		TRIGGER_MUSIC_EVENT("JH2A_MISSION_FAIL")
	ELSE
		TRIGGER_MUSIC_EVENT("JH2B_MISSION_FAIL")
	ENDIF
	
	WHILE NOT GET_MISSION_FLOW_SAFE_TO_CLEANUP()
	//AND GET_CUTSCENE_TIME() < 18000
		//Maintain anything that could look weird during fade out (e.g. enemies walking off). 
		WAIT(0)
	ENDWHILE
	
	// Remove the bugstar outfit (behind faded screen)
	RESTORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
	
	// check if we need to respawn the player in a different position, 
	// if so call MISSION_FLOW_SET_FAIL_WARP_LOCATION() + SET_REPLAY_DECLINED_VEHICLE_WARP_LOCATION here
	
	CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	
	DELETE_VEHICLE(vehCar)
	IF DOES_ENTITY_EXIST(vehVan)
		IF IS_ENTITY_A_MISSION_ENTITY(vehVan)
			DELETE_VEHICLE(vehVan)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(pedCrew[CREW_DRIVER_NEW])
		IF IS_ENTITY_A_MISSION_ENTITY(pedCrew[CREW_DRIVER_NEW])
			DELETE_PED(pedCrew[CREW_DRIVER_NEW])
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(pedCrew[CREW_HACKER])
		IF IS_ENTITY_A_MISSION_ENTITY(pedCrew[CREW_HACKER])
			DELETE_PED(pedCrew[CREW_HACKER])
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(pedCrew[CREW_GUNMAN_NEW])
		IF IS_ENTITY_A_MISSION_ENTITY(pedCrew[CREW_GUNMAN_NEW])
			DELETE_PED(pedCrew[CREW_GUNMAN_NEW])
		ENDIF
	ENDIF
	
	RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL, bHasChangedClothesMichael)
	RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_FRANKLIN, bHasChangedClothesFranklin)
	
	// Steve R - Disable interiors of Jewel Store and Max Renda store
	SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_STORE, BUILDINGSTATE_NORMAL, TRUE)
	SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_HEIST_MAX_RENDA_INTERIOR, BUILDINGSTATE_NORMAL, TRUE)
	
	MissionCleanup()  // must only take 1 frame and terminate the thread
	TERMINATE_THIS_THREAD()	
ENDPROC


//═════════════════════════════════╡ INITIALISING ╞═══════════════════════════════════

//PURPOSE: Adds all the models in the asset list to the model list
PROC CREATE_NEED_LIST()
	CLEAR_NEED_LIST()
	ADD_MODEL_TO_NEED_LIST(PROP_LD_TEST_01)
	ADD_MODEL_TO_NEED_LIST(Asset.mnBike)
	ADD_MODEL_TO_NEED_LIST(Asset.mnCar)
	ADD_MODEL_TO_NEED_LIST(Asset.mnVan)
	ADD_MODEL_TO_NEED_LIST(Asset.mnSecurity)
	FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
		ADD_MODEL_TO_NEED_LIST(Asset.mnShop[iCounter])
	ENDFOR
	ADD_MODEL_TO_NEED_LIST(Asset.mnBackroom)
	ADD_MODEL_TO_NEED_LIST(Asset.mnGrenade)
	ADD_MODEL_TO_NEED_LIST(Asset.mnTruck)
	ADD_MODEL_TO_NEED_LIST(Asset.mnPolice)
	ADD_MODEL_TO_NEED_LIST(Asset.mnPoliceman)
	ADD_MODEL_TO_NEED_LIST(Asset.mnPhone)
	ADD_MODEL_TO_NEED_LIST(Asset.mnBag)
	ADD_MODEL_TO_NEED_LIST(Asset.mnJewels1)
	ADD_MODEL_TO_NEED_LIST(Asset.mnJewels2)
	ADD_MODEL_TO_NEED_LIST(Asset.mnJewels4a)
	ADD_MODEL_TO_NEED_LIST(Asset.mnJewels4b)
	ADD_MODEL_TO_NEED_LIST(Asset.mnGunman)
	ADD_MODEL_TO_NEED_LIST(Asset.mnDriver)
	
	ADD_MODEL_TO_NEED_LIST(IG_TRAFFICWARDEN)
	
	ADD_MODEL_TO_NEED_LIST(Asset.mnLester)
	ADD_MODEL_TO_NEED_LIST(Asset.mnLestersStick)
	
	ADD_ANIM_DICT_TO_NEED_LIST(Asset.adJewelHeist)
	ADD_ANIM_DICT_TO_NEED_LIST(Asset.adJewelHeist)
	ADD_ANIM_DICT_TO_NEED_LIST(Asset.adWatch)
	ADD_ANIM_DICT_TO_NEED_LIST(Asset.adJewelHeistSwitchCam)
	
	
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(Asset.recTruckLead, Asset.recNumber)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(Asset.recTruckExit, Asset.recNumber)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(Asset.recUber, Asset.recNumber)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(Asset.recUber, Asset.recNoGoodBikeChase)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(Asset.recUber, Asset.recNoFinalTrain)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST("JHLateCops", 001)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST("JHLateCops", 002)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST("JHLateCops", 003)
	
	ADD_VEHICLE_RECORDING_TO_NEED_LIST("JHUBER", 717)
	
	ADD_VEHICLE_RECORDING_TO_NEED_LIST(Asset.recUber, 998)
	
//	ADD_VEHICLE_RECORDING_TO_NEED_LIST("JHUber", 501)
//	ADD_VEHICLE_RECORDING_TO_NEED_LIST("JHUber", 504)
		
	ADD_VEHICLE_RECORDING_TO_NEED_LIST("JHArrive", 1)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST("JHArrive", 2)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST("JHArrive", 3)
	
	//ADD_VEHICLE_RECORDING_TO_NEED_LIST("BENSONEXIT", 1)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST("JHBENSONEXIT", 1)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST("JHBugStrExit", 1)
	
	ADD_VEHICLE_RECORDING_TO_NEED_LIST("JH2BArrive", 1)
	ADD_VEHICLE_RECORDING_TO_NEED_LIST("JH2BArrive", 2)
	
ENDPROC

//═════════╡ GENERAL ╞═════════



//PURPOSE:	Adds all the peds that speak in the mission to the dialogue struct
PROC ADD_ALL_PEDS_TO_DIALOGUE()

	SAFE_ADD_PED_FOR_DIALOGUE(GET_SELECT_PED(SEL_MICHAEL), ConvertSingleCharacter("0"), "MICHAEL")
	SAFE_ADD_PED_FOR_DIALOGUE(GET_SELECT_PED(SEL_FRANKLIN), ConvertSingleCharacter("1"), "FRANKLIN")

	IF IS_DRIVER_EDDIE()
		SAFE_ADD_PED_FOR_DIALOGUE(pedCrew[CREW_DRIVER_NEW], 3, "EDDIE")
		PRINTLN("IS_DRIVER_EDDIE TRUE!")
	ELIF IS_DRIVER_TALINA()
		SAFE_ADD_PED_FOR_DIALOGUE(pedCrew[CREW_DRIVER_NEW], 3, "TALINA")
		PRINTLN("IS_DRIVER_TALINA TRUE!")
	ELIF IS_DRIVER_KARIM()
		SAFE_ADD_PED_FOR_DIALOGUE(pedCrew[CREW_DRIVER_NEW], 3, "KARIM")
		PRINTLN("IS_DRIVER_KARIM TRUE!")
	ENDIF
	
	
	IF IS_GUNMAN_PACKIE()
		SAFE_ADD_PED_FOR_DIALOGUE(pedCrew[CREW_GUNMAN_NEW], 4, "PACKIE")
	ELIF IS_GUNMAN_GUSTAVO()
		SAFE_ADD_PED_FOR_DIALOGUE(pedCrew[CREW_GUNMAN_NEW], 4, "GUSTAVO")//"JewelGunGood")
	ELIF IS_GUNMAN_NORM()
		SAFE_ADD_PED_FOR_DIALOGUE(pedCrew[CREW_GUNMAN_NEW], 4, "NORM")//"JewelGunBad")
	ENDIF
	
	SAFE_ADD_PED_FOR_DIALOGUE(pedStore[0], ConvertSingleCharacter("5"), "JewelSales")
	SAFE_ADD_PED_FOR_DIALOGUE(pedSecurity, ConvertSingleCharacter("6"), "JewelGuard")
	
	IF (eMissionStage < STAGE_GRAB_LOOT)
	
		SWITCH GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)) 
			CASE CM_HACKER_G_PAIGE
				SAFE_ADD_PED_FOR_DIALOGUE(pedCrew[CREW_HACKER], 7, "PAIGE")
			BREAK
		
			CASE CM_HACKER_M_CHRIS
				SAFE_ADD_PED_FOR_DIALOGUE(pedCrew[CREW_HACKER], 7, "CHRISTIAN")
			BREAK
			
			CASE CM_HACKER_B_RICKIE_UNLOCK
				SAFE_ADD_PED_FOR_DIALOGUE(pedCrew[CREW_HACKER], 7, "LIEngineer")
			BREAK
		ENDSWITCH
	
	ELSE
		SWITCH GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)) 
			CASE CM_HACKER_G_PAIGE
				SAFE_ADD_PED_FOR_DIALOGUE(NULL, 7, "PAIGE")
			BREAK
		
			CASE CM_HACKER_M_CHRIS
				SAFE_ADD_PED_FOR_DIALOGUE(NULL, 7, "CHRISTIAN")
			BREAK
		
			CASE CM_HACKER_B_RICKIE_UNLOCK
				SAFE_ADD_PED_FOR_DIALOGUE(NULL, 7, "LIEngineer")
			BREAK
		ENDSWITCH	
	ENDIF
	SAFE_ADD_PED_FOR_DIALOGUE(pedBackroom, 8, "JewelManager")
	
	SAFE_ADD_PED_FOR_DIALOGUE(pedStore[1], ConvertSingleCharacter("B"), "JewelShop0")
	SAFE_ADD_PED_FOR_DIALOGUE(pedStore[2], ConvertSingleCharacter("C"), "JewelShop1")
	SAFE_ADD_PED_FOR_DIALOGUE(NULL, ConvertSingleCharacter("D"), "POLICE")
	SAFE_ADD_PED_FOR_DIALOGUE(pedLester, ConvertSingleCharacter("E"), "LESTER")
ENDPROC

//PURPOSE:	Sets the peds attributes to that necessary for a shop ped
PROC SET_PED_SHOP(PED_INDEX thisPed)
	SET_PED_RELATIONSHIP_GROUP_HASH(thisPed, grpShop)
	SET_PED_COMBAT_ATTRIBUTES(thisPed, 	CA_AGGRESSIVE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(thisPed, 	CA_CAN_INVESTIGATE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(thisPed,  CA_PLAY_REACTION_ANIMS, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(thisPed,  CA_WILL_SCAN_FOR_DEAD_PEDS, FALSE)
	SET_PED_ALERTNESS(thisPed, AS_NOT_ALERT)
	SET_PED_COMBAT_ABILITY(thisPed, CAL_POOR)
	SET_PED_COMBAT_RANGE(thisPed, CR_NEAR)
	SET_PED_GENERATES_DEAD_BODY_EVENTS(thisPed, FALSE)
	SET_PED_SEEING_RANGE(thisPed, 0)
	SET_PED_HEARING_RANGE(thisPed, 0)
	SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thisPed, TRUE)
ENDPROC

//PURPOSE:	Sets the peds attributes to that necessary for a cop
PROC SET_PED_COP(PED_INDEX thisPed, BOOL bAggressive = FALSE)
	SET_PED_RELATIONSHIP_GROUP_HASH(thisPed, grpCop)
	SET_PED_COMBAT_ATTRIBUTES(thisPed, 	CA_AGGRESSIVE, bAggressive)
	SET_PED_COMBAT_ATTRIBUTES(thisPed, 	CA_CAN_INVESTIGATE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(thisPed,  CA_PLAY_REACTION_ANIMS, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(thisPed,  CA_WILL_SCAN_FOR_DEAD_PEDS, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(thisPed,  CA_USE_VEHICLE, FALSE)
	SET_PED_COMBAT_ATTRIBUTES(thisPed,  CA_LEAVE_VEHICLES, TRUE)
	SET_PED_ALERTNESS(thisPed, AS_MUST_GO_TO_COMBAT)
	SET_PED_COMBAT_ABILITY(thisPed, CAL_POOR)
	IF bAggressive
		SET_PED_COMBAT_RANGE(thisPed, CR_NEAR)
	ELSE
		SET_PED_COMBAT_RANGE(thisPed, CR_FAR)
	ENDIF
	SET_PED_GENERATES_DEAD_BODY_EVENTS(thisPed, FALSE)
	SET_PED_SEEING_RANGE(thisPed, 60)
	SET_PED_HEARING_RANGE(thisPed, 60)
	SET_PED_ACCURACY(thisPed, 0)
	GIVE_WEAPON_TO_PED(thisPed, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
	
ENDPROC

//PURPOSE: Sets a ped to paly a single frame of an anim
PROC doPlayAnimFrameAndKill(PED_INDEX ped, STRING AnimDict, STRING AnimName, FLOAT Time)
	BOOL bAnim = FALSE
	IF NOT IS_PED_INJURED(ped)
		TASK_PLAY_ANIM(ped, AnimDict, AnimName)
		WHILE NOT bAnim
			IF NOT IS_PED_INJURED(ped)
				IF IS_ENTITY_PLAYING_ANIM(ped, AnimDict, AnimName)
					bAnim = TRUE
				ELSE
					safeWait(0)
				ENDIF
			ENDIF
		ENDWHILE
		SET_ENTITY_ANIM_CURRENT_TIME(ped, AnimDict, AnimName, TIME)
		SET_ENTITY_ANIM_SPEED(ped, AnimDict, AnimName, 0)
		SET_ENTITY_HEALTH(ped, 0)
	ENDIF
ENDPROC

//PURPOSE:	Instantly updates the animations of several peds
PROC IMMEDIATELY_UPDATE_ANIMS(PED_INDEX ped0, PED_INDEX ped1 = NULL, PED_INDEX ped2 = NULL, PED_INDEX ped3 = NULL, 
						PED_INDEX ped4 = NULL, PED_INDEX ped5 = NULL, PED_INDEX ped6 = NULL, PED_INDEX ped7 = NULL)
	
	printLn("************************************************")
	printLn("*****  IMMEDIATELY_UPDATE_ANIMS called *********")
	printLn("************************************************")
	
	FORCE_PED_AI_AND_ANIMATION_UPDATE(ped0)
	IF ped1 <> NULL
		FORCE_PED_AI_AND_ANIMATION_UPDATE(ped1)
		IF ped2 <> NULL
			FORCE_PED_AI_AND_ANIMATION_UPDATE(ped2)
			IF ped3 <> NULL
				FORCE_PED_AI_AND_ANIMATION_UPDATE(ped3)
				IF ped4 <> NULL
					FORCE_PED_AI_AND_ANIMATION_UPDATE(ped4)
					IF ped5 <> NULL
						FORCE_PED_AI_AND_ANIMATION_UPDATE(ped5)
						IF ped6 <> NULL
							FORCE_PED_AI_AND_ANIMATION_UPDATE(ped6)
							IF ped7 <> NULL
								FORCE_PED_AI_AND_ANIMATION_UPDATE(ped7)
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	//SafeWait(0)
ENDPROC

//PURPOSE:	Safely creates the store peds- checks if dead etc, acts accordingly. Can create them KO'd if they've been gassed
PROC SAFE_SETUP_STORE_PEDS(BOOL bKO, BOOL bCower, BOOL bImmediatelyUpdateAnims = TRUE, BOOL bSafeWait = TRUE, BOOL bLoadModelsMustWait = TRUE)
	
	//Alow this to be run with NO waits.
	IF bLoadModelsMustWait
		SET_MODEL_AS_NEEDED(Asset.mnSecurity)
		FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
			SET_MODEL_AS_NEEDED(Asset.mnShop[iCounter])
		ENDFOR
		MANAGE_LOADING_NEED_LIST(bSafeWait)
	ENDIF

	IF NOT DOES_ENTITY_EXIST(pedSecurity)
		pedSecurity = CREATE_PED(PEDTYPE_MISSION, Asset.mnSecurity, vSecuritySpawnCoords, fSecuritySpawnHeading)
		IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(Asset.mnSecurity)
		IF NOT IS_PED_INJURED(pedSecurity)
		ENDIF
	ELSE
		IF NOT IS_PED_INJURED(pedSecurity)
			SET_ENTITY_COORDS(pedSecurity, vSecuritySpawnCoords)
			SET_ENTITY_HEADING(pedSecurity, fSecuritySpawnHeading)
		ENDIF
	ENDIF
	IF NOT IS_PED_INJURED(pedSecurity)
		SET_PED_SHOP(pedSecurity)
	ENDIF
	
	FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
		IF NOT DOES_ENTITY_EXIST(pedStore[iCounter])
			pedStore[iCounter] = CREATE_PED(	PEDTYPE_MISSION, Asset.mnShop[iCounter], 
												vStoreSpawnCoords[iCounter], fStoreSpawnHeading[iCounter])
			#IF IS_DEBUG_BUILD
				debugPedName = "Store:"
				debugPedName += iCounter
				SET_PED_NAME_DEBUG(pedStore[iCounter], debugPedName)
			#ENDIF
			IF NOT IS_PED_INJURED(pedStore[iCounter])
			ENDIF
		ELSE
			IF NOT IS_PED_INJURED(pedStore[iCounter])
				SET_ENTITY_COORDS(pedStore[iCounter], vStoreSpawnCoords[iCounter])
				SET_ENTITY_HEADING(pedStore[iCounter], fStoreSpawnHeading[iCounter])
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(pedStore[iCounter])
			SET_PED_SHOP(pedStore[iCounter])
			SET_PED_IS_AVOIDED_BY_OTHERS(pedStore[iCounter], FALSE)
		ENDIF
	ENDFOR
	FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
		IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(Asset.mnShop[iCounter])
	ENDFOR
	
	IF NOT IS_PED_INJURED(pedStore[0])
		SET_PED_COMPONENT_VARIATION(pedStore[0], PED_COMP_HEAD, 1, 0)
		SET_PED_COMPONENT_VARIATION(pedStore[0], PED_COMP_BERD, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedStore[0], PED_COMP_HAIR, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedStore[0], PED_COMP_TORSO, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedStore[0], PED_COMP_LEG, 1, 0)
		SET_PED_COMPONENT_VARIATION(pedStore[0], PED_COMP_HAND, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedStore[0], PED_COMP_FEET, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedStore[0], PED_COMP_TEETH, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedStore[0], PED_COMP_SPECIAL, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedStore[0], PED_COMP_SPECIAL2, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedStore[0], PED_COMP_DECL, 0, 0)
		SET_PED_COMPONENT_VARIATION(pedStore[0], PED_COMP_JBIB, 0, 0)

		CLEAR_PED_PROP(pedStore[0], ANCHOR_HEAD)
		CLEAR_PED_PROP(pedStore[0], ANCHOR_EYES)
		CLEAR_PED_PROP(pedStore[0], ANCHOR_EARS)
		CLEAR_PED_PROP(pedStore[0], ANCHOR_MOUTH)
		CLEAR_PED_PROP(pedStore[0], ANCHOR_LEFT_HAND)
		CLEAR_PED_PROP(pedStore[0], ANCHOR_RIGHT_HAND)
		CLEAR_PED_PROP(pedStore[0], ANCHOR_LEFT_WRIST)
		CLEAR_PED_PROP(pedStore[0], ANCHOR_RIGHT_WRIST)
		CLEAR_PED_PROP(pedStore[0], ANCHOR_HIP)
	ENDIF
	
	IF NOT IS_PED_INJURED(pedStore[2])
		SET_PED_COMPONENT_VARIATION(pedStore[2], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedStore[2], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedStore[2], INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedStore[2], INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedStore[2], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
	ENDIF
	
	IF NOT IS_PED_INJURED(pedStore[3])
		SET_PED_COMPONENT_VARIATION(pedStore[3], INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedStore[3], INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedStore[3], INT_TO_ENUM(PED_COMPONENT,3), 0, 5, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedStore[3], INT_TO_ENUM(PED_COMPONENT,4), 1, 2, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedStore[3], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
	ENDIF
	
	IF NOT IS_PED_INJURED(pedStore[4])
	AND NOT bIsStealthPath
		SET_PED_COMPONENT_VARIATION(pedStore[4], INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
		SET_PED_COMPONENT_VARIATION(pedStore[4], INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
		SET_PED_COMPONENT_VARIATION(pedStore[4], INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
		SET_PED_COMPONENT_VARIATION(pedStore[4], INT_TO_ENUM(PED_COMPONENT,4), 1, 1, 0) //(lowr)
		SET_PED_COMPONENT_VARIATION(pedStore[4], INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
	ENDIF
	
	
//	VECTOR scenePosition = << -625.445, -234.010, 37.719 >>
//
//VECTOR sceneRotation = << -0.000, 0.000, 36.100 >>
	
	IF bKO
		TASK_PLAY_ANIM_ADVANCED(pedSecurity, Asset.adJewelHeist, Asset.animGasSecurity, vJewelDoorCoords, vJewelDoorRot,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_ENDS_IN_DEAD_POSE, 0.9)
		
		TASK_PLAY_ANIM_ADVANCED(pedStore[0], Asset.adJewelHeist, Asset.animGasAssistant, vJewelDoorCoords, vJewelDoorRot,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_ENDS_IN_DEAD_POSE, 0.9)
		
		FOR iCounter = 1 TO NUMBER_OF_STORE_PEDS-1
			TASK_PLAY_ANIM_ADVANCED(pedStore[iCounter], Asset.adJewelHeist, Asset.animGas[iCounter-1], vJewelDoorCoords, vJewelDoorRot,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,  AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_ENDS_IN_DEAD_POSE, 0.9)
		ENDFOR
		
		TASK_PLAY_ANIM_ADVANCED(pedStore[2], Asset.adJewelHeist, Asset.animGas[1], vJewelDoorCoords + <<0.0, 0.0, -0.85>>, vJewelDoorRot,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,  AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_ENDS_IN_DEAD_POSE, 0.9)
		
	ELIF bCower
	
		TASK_PLAY_ANIM_ADVANCED(pedStore[0], Asset.adJewelHeist, "JH_2B_EndLoop_Male2", << -625.444, -234.010, 37.720 >>, << 0.000, 0.000, 36.000 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION|AF_LOOPING | AF_NOT_INTERRUPTABLE)				
		TASK_PLAY_ANIM_ADVANCED(pedStore[1], Asset.adJewelHeist, "JH_2B_EndLoop_female1", << -625.444, -234.010, 37.720 >>, << 0.000, 0.000, 36.0 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION|AF_LOOPING | AF_NOT_INTERRUPTABLE)
		TASK_PLAY_ANIM_ADVANCED(pedStore[2], Asset.adJewelHeist, "JH_2B_EndLoop_female2", << -625.444, -234.010, 37.720 >>, << 0.000, 0.000, 36.00 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION|AF_LOOPING | AF_NOT_INTERRUPTABLE)
		TASK_PLAY_ANIM_ADVANCED(pedStore[3], Asset.adJewelHeist, "JH_2B_EndLoop_Male3", << -625.444, -234.010, 37.720 >>, << 0.000, 0.000, 36.000 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION|AF_LOOPING | AF_NOT_INTERRUPTABLE)
		TASK_PLAY_ANIM_ADVANCED(pedStore[4], Asset.adJewelHeist, "JH_2B_EndLoop_Male1",<< -625.444, -234.010, 37.720 >>, << 0.000, 0.000, 36.00 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION|AF_LOOPING | AF_NOT_INTERRUPTABLE)
		TASK_PLAY_ANIM_ADVANCED(pedSecurity, Asset.adJewelHeist, Asset.animCowerOnFloor, << -625.445, -234.010, 37.719 >>, << -0.000, 0.000, 36.100 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_LOOPING | AF_NOT_INTERRUPTABLE)
	
//		TASK_PLAY_ANIM_ADVANCED(pedStore[0], Asset.adJewelHeist, Asset.animCowerOnFloor, << -624.627, -235.592, 38.230 >>, << 0.000, 0.000, 146.000 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_LOOPING)
//		TASK_PLAY_ANIM_ADVANCED(pedStore[1], Asset.adJewelHeist, Asset.animCowerOnFloor, << -625.877, -231.004, 38.230 >>, << 0.000, 0.000, -13.750 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_LOOPING)
//		TASK_PLAY_ANIM_ADVANCED(pedStore[2], Asset.adJewelHeist, Asset.animCowerOnFloor, << -626.402, -230.417, 38.230 >>, << 0.000, 0.000, 3.500 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_LOOPING)
//		TASK_PLAY_ANIM_ADVANCED(pedStore[3], Asset.adJewelHeist, Asset.animCowerOnFloor, << -629.152, -235.004, 38.230 >>, << 0.000, 0.000, 125.000 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_LOOPING)
//		TASK_PLAY_ANIM_ADVANCED(pedStore[4], Asset.adJewelHeist, Asset.animCowerOnFloor, << -626.089, -231.892, 38.230 >>, << 0.000, 0.000, 47.250 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_LOOPING)
//		TASK_PLAY_ANIM_ADVANCED(pedSecurity, Asset.adJewelHeist, Asset.animCowerOnFloor, vJewelDoorCoords, vJewelDoorRot,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_LOOPING)
	ENDIF
	
	IF bImmediatelyUpdateAnims
		IMMEDIATELY_UPDATE_ANIMS(pedStore[0], pedStore[1], pedStore[2], pedStore[3], pedStore[4], pedSecurity)
	ENDIF
	
	IF bKO
		FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
			//FREEZE_ENTITY_POSITION(pedStore[iCounter], TRUE)
			//SET_ENTITY_COLLISION(pedStore[iCounter], FALSE)
			SET_ENTITY_HEALTH(pedStore[iCounter], 101)
			SET_PED_CAN_RAGDOLL(pedStore[iCounter], FALSE)
			//SET_PED_SUFFERS_CRITICAL_HITS(pedStore[iCounter], FALSE)
						
			SET_PED_KEEP_TASK(pedStore[iCounter], TRUE)
			
			SET_PED_CAN_BE_TARGETTED(pedStore[iCounter], FALSE)
			
		ENDFOR
		SET_ENTITY_HEALTH(pedSecurity, 10000)
		SET_PED_CAN_RAGDOLL(pedSecurity, FALSE)
		SET_PED_SUFFERS_CRITICAL_HITS(pedSecurity, FALSE)
		SET_PED_CAN_BE_TARGETTED(pedSecurity, FALSE)
	ELSE
		FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1						
			SET_PED_CAN_BE_TARGETTED(pedStore[iCounter], FALSE)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedStore[iCounter], FALSE)
		ENDFOR
		SET_PED_CAN_BE_TARGETTED(pedSecurity, FALSE)
		SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedSecurity, FALSE)
	ENDIF
	
	FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
		SET_ENTITY_HEALTH(pedStore[iCounter], 101)
	ENDFOR
	
	DELETE_PED(pedStore[3])
	
ENDPROC

PROC UPDATE_STORE_PEDS()
	IF bIsStealthPath
		IF NOT IS_PED_INJURED(pedStore[0])
			IF GET_ENTITY_HEALTH(pedStore[0]) < 10000
				SET_PED_CAN_RAGDOLL(pedStore[0], TRUE)
				//SET_ENTITY_HEALTH(pedStore[0], 50)
			ENDIF
			IF NOT IS_TASK_ONGOING(pedStore[0], SCRIPT_TASK_PLAY_ANIM)
				TASK_PLAY_ANIM_ADVANCED(pedStore[0], Asset.adJewelHeist, Asset.animGasAssistant, vJewelDoorCoords, vJewelDoorRot,
										INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_LOOPING)
			ENDIF
		ENDIF
		
		FOR iCounter = 1 TO NUMBER_OF_STORE_PEDS-1
			IF NOT IS_PED_INJURED(pedStore[iCounter])
				IF GET_ENTITY_HEALTH(pedStore[iCounter]) < 10000
					SET_PED_CAN_RAGDOLL(pedStore[iCounter], TRUE)
					//SET_ENTITY_HEALTH(pedStore[iCounter], 50)
				ENDIF
				IF NOT IS_TASK_ONGOING(pedStore[iCounter], SCRIPT_TASK_PLAY_ANIM)
					TASK_PLAY_ANIM_ADVANCED(pedStore[iCounter], Asset.adJewelHeist, Asset.animGas[iCounter-1], vJewelDoorCoords, vJewelDoorRot,
											INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_LOOPING)
				ENDIF
			ENDIF
		ENDFOR
		
		IF NOT IS_PED_INJURED(pedSecurity)
			IF GET_ENTITY_HEALTH(pedSecurity) < 10000
				SET_PED_CAN_RAGDOLL(pedSecurity, TRUE)
				SET_ENTITY_HEALTH(pedSecurity, 0)
			ENDIF
			IF NOT IS_TASK_ONGOING(pedSecurity, SCRIPT_TASK_PLAY_ANIM)
				TASK_PLAY_ANIM_ADVANCED(pedSecurity, Asset.adJewelHeist, Asset.animGasSecurity, vJewelDoorCoords, vJewelDoorRot,
										INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_LOOPING)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Sets up the player's bike to be more stable
PROC SET_BIKE_STABILISERS(PED_INDEX thisPed, BOOL bOn)
	IF bOn
		SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(thisPed, KNOCKOFFVEHICLE_HARD)
	ELSE
		SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(thisPed, KNOCKOFFVEHICLE_DEFAULT)
	ENDIF
ENDPROC


//PURPOSE: Clears out a peds drive to tasks by giving the ped one that it cannot fail to complete instantly
PROC CLEAR_PED_DRIVE_TASKS(PED_INDEX thisPed, VEHICLE_INDEX thisVehicle)
	IF NOT IS_ENTITY_DEAD(thisVehicle)
	AND NOT IS_PED_INJURED(thisPed)
		TASK_VEHICLE_DRIVE_TO_COORD(thisPed, thisVehicle, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisVehicle, <<0,10,0>>), GET_ENTITY_SPEED(thisVehicle),DRIVINGSTYLE_STRAIGHTLINE, GET_ENTITY_MODEL(thisVehicle), DRIVINGMODE_PLOUGHTHROUGH, 9999999.0, 9999999.0)
	ENDIF
ENDPROC

//PURPOSE: Clears out a peds drive to tasks by giving the ped one that it cannot fail to complete instantly
PROC DRIVE_STRAIGHT_FORWARDS(PED_INDEX thisPed, VEHICLE_INDEX thisVehicle)
	IF NOT IS_ENTITY_DEAD(thisVehicle)
	AND NOT IS_PED_INJURED(thisPed)
		TASK_VEHICLE_DRIVE_TO_COORD(thisPed, thisVehicle, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisVehicle, <<0,25,0>>), 30.0 ,DRIVINGSTYLE_STRAIGHTLINE, GET_ENTITY_MODEL(thisVehicle), DRIVINGMODE_PLOUGHTHROUGH, 1.0, 25.0)
	ENDIF
ENDPROC


//PURPOSE:	Takes a INT in the format of 123456789 and returns a Text label in the format of $123,456,789
FUNC TEXT_LABEL_23 MONETISE_INT(INT iThisValue)
	INT iFormat
	INT iNextApply
	TEXT_LABEL_23 moneyVal
	moneyVal = "$"
	iNextApply = iThisValue
	
	iFormat = iNextApply/1000000
	IF iFormat > 0
		moneyVal += iFormat
		moneyVal += ","
		iNextApply -= iFormat*1000000
	ENDIF
	iFormat = iNextApply/1000
	IF iFormat > 0
		moneyVal += iFormat
		moneyVal += ","
		iNextApply -= iFormat*1000
	ENDIF
	moneyVal += iNextApply
	
	RETURN moneyVal
ENDFUNC


//PURPOSE:	Sets the flags used to control the consequences of what the player has picked as their crew
PROC SETUP_CREW_CHOICE_CONSEQUENCES()
	//Hacker skill sets time limit
	SWITCH GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)))
		CASE CMSK_GOOD
			iTimeLimit = iTimeLimitA
		BREAK
		CASE CMSK_MEDIUM
			iTimeLimit = iTimeLimitB
		BREAK
		
		CASE CMSK_BAD
			iTimeLimit = 55000
		BREAK
		
	ENDSWITCH
	
	//Driver does nothing
	
	//First gunman smashes cases
	//Less money if crap
	
	IF bIsStealthPath
	
		SWITCH GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)))
			CASE CMSK_GOOD
				fCrewSmashMultiplier = 1.0
			BREAK
			CASE CMSK_MEDIUM
				fCrewSmashMultiplier = 0.9
			BREAK
			CASE CMSK_BAD
				fCrewSmashMultiplier = 0.8
			BREAK
		ENDSWITCH
	
	ELSE
		SWITCH GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW)))
			CASE CMSK_GOOD
				fCrewSmashMultiplier = 1.0
			BREAK
			CASE CMSK_MEDIUM
				fCrewSmashMultiplier = 0.9
			BREAK
			CASE CMSK_BAD
				fCrewSmashMultiplier = 0.8
			BREAK
		ENDSWITCH
	ENDIF
	
		
	//Best gunman takes first bike.
	//If second bike is crap, they die.
	
	cnSmashGunman = CREW_GUNMAN_NEW
	cnAltGunman = CREW_DRIVER_NEW
	
	IF GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW))) = CMSK_BAD
		bWorstGunDies = TRUE
	ELSE
		bWorstGunDies = FALSE
	ENDIF
		
	//Second gunman stands watch - 2A
	//No consequence
	//Second gunman crowd controls - 2B
	//If crap then security guard section
	IF NOT bIsStealthPath
		IF GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW))) = CMSK_BAD
		//OR GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW))) = CMSK_BAD
			bGuardRuns = TRUE
		ENDIF
	ELSE
		bGuardRuns = FALSE
	ENDIF
	
	SET_HEIST_END_SCREEN_CREW_STATUS(HEIST_JEWEL, GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW)), CMST_FINE)
	IF bWorstGunDies
		SET_HEIST_END_SCREEN_CREW_STATUS(HEIST_JEWEL, GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)), CMST_KILLED)
	ELSE
		SET_HEIST_END_SCREEN_CREW_STATUS(HEIST_JEWEL, GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)), CMST_FINE)
	ENDIF
	SET_HEIST_END_SCREEN_CREW_STATUS(HEIST_JEWEL, GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)), CMST_FINE)
	SET_HEIST_END_SCREEN_CREW_STATUS(HEIST_JEWEL, GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)), CMST_FINE)
	
	
ENDPROC


//═════════════════════════════════╡ DEBUG PROCEDURES ╞═══════════════════════════════════

#IF IS_DEBUG_BUILD

INT iTimerAValue

//PURPOSE:		Moniters for any keyboard inputs that would alter the mission(debug pass etc)
PROC Debug_Options()
	IF IS_KEYBOARD_KEY_PRESSED(KEY_S)
		//Give minimum take on S-pass, B* 1781272
		IF iCurrentTake<=0
			iCurrentTake = MIN_TAKE_VALUE_GIVEN_BAG_LOSS
		ENDIF
		MissionPassed()
//		//RESET_HEIST_END_SCREEN_STOLEN_ITEMS(HEIST_JEWEL)
//		g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE] = 0
//		//iTotalSmashedCases = 0
//		FOR iCounter = 0 TO (iNoOfCases - 1)
//			//ADD_HEIST_END_SCREEN_STOLEN_ITEM(HEIST_JEWEL, jcStore[iCounter].sLootName, jcStore[iCounter].iWorth)
//			//iTotalSmashedCases++
//			++g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE]
//		ENDFOR
//		eMissionStage = PASSED_MISSION
	ENDIF
	IF IS_KEYBOARD_KEY_PRESSED(KEY_F)
		//bIsSuperDebugEnabled = TRUE
		eMissionFail = FAIL_GENERIC
		MissionFailed()
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J)
		JSkip = TRUE
	ELSE
		JSkip = FALSE
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P)
		PSkip = TRUE
	ELSE
		PSkip = FALSE
	ENDIF
	
	iTimerAValue = TIMERA()
	
ENDPROC

//PURPOSE:		Creates debug widgets
PROC CREATE_WIDGETS() //Called on mission setup	
	
	widgetDebug = START_WIDGET_GROUP("Jewel Heist")
		
		ADD_WIDGET_BOOL("bIsSuperDebugEnabled", bIsSuperDebugEnabled)
		ADD_WIDGET_INT_READ_ONLY("Mission Stage", iDebugMissionStage)
		ADD_WIDGET_INT_READ_ONLY("iStageSection", iStageSection)
		ADD_WIDGET_BOOL("bGuardRuns", bGuardRuns)
		ADD_WIDGET_BOOL("bWorstGunDies", bWorstGunDies)
		
		ADD_WIDGET_INT_SLIDER("iArrivalAngle", iArrivalAngle, 0, 3, 1)
		
		
		ADD_WIDGET_BOOL("bWaitedTooLong", bWaitedTooLong)
		
		ADD_WIDGET_BOOL("bAimingLineShouldPlay", bAimingLineShouldPlay)
		//ADD_WIDGET_INT_SLIDER("g_iJewelHeistPlayedCounter", g_iJewelHeistPlayedCounter, 0, 50, 1)
	
		ADD_WIDGET_INT_READ_ONLY("iRandomGuardStage", iRandomGuardStage)
		ADD_WIDGET_BOOL("bIsRandomGuardEventDone", bIsRandomGuardEventDone)
		
		ADD_WIDGET_BOOL("bIsRandomGuardEventNow", bIsRandomGuardEventNow)
		
		
		ADD_WIDGET_INT_READ_ONLY("eRandomManagerStage", iRandomManagerStage)
		ADD_WIDGET_INT_READ_ONLY("iManagerDialogue", iManagerDialogue)
		
		
		ADD_WIDGET_BOOL("bIsRandomManagerEventDone", bIsRandomManagerEventDone)
		
		ADD_WIDGET_BOOL("bIsRandomManagerEventNow", bIsRandomManagerEventNow)
		
		ADD_WIDGET_INT_READ_ONLY("iTimeLimit", iTimeLimit)
		
		ADD_WIDGET_INT_READ_ONLY("TIMERA()", iTimerAValue)
		
		ADD_WIDGET_INT_READ_ONLY("iCurrentTake", iCurrentTake)
		ADD_WIDGET_INT_READ_ONLY("iDisplayedTake", iDisplayedTake)
		ADD_WIDGET_INT_READ_ONLY("ilostBag", ilostBag)
		
		
		ADD_WIDGET_INT_READ_ONLY("G//iTotalSmashedCases", g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE])
		ADD_WIDGET_INT_READ_ONLY("Gloabl cases", g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE])
		ADD_WIDGET_INT_SLIDER("iCurrentBuddyCase", iCurrentBuddyCase, 0, 100, 1)
		ADD_WIDGET_INT_SLIDER("iNearestCase", iNearestCase, 0, 100, 1)
		ADD_WIDGET_INT_SLIDER("iFarthestCase", iFarthestCase, 0, 100, 1)
		
		
		ADD_WIDGET_INT_SLIDER("iBuddyLootStage", iBuddyLootStage, 0, 100, 1)
		
		
//		ADD_WIDGET_INT_READ_ONLY("iCameraCuts", iCameraCuts)
//		ADD_WIDGET_INT_SLIDER("iCameraSelection", iCameraSelection, 0, 1, 1)
		
		ADD_WIDGET_FLOAT_READ_ONLY("fCrewSmashMultiplier", fCrewSmashMultiplier)
		
		ADD_WIDGET_INT_SLIDER("iUnloadBikes", iUnloadBikes, 0, 99, 1)
		
		ADD_WIDGET_INT_SLIDER("iGetLostStage", iGetLostStage, 0, 99, 1)
		//ADD_WIDGET_INT_SLIDER("iConvoBranch", iConvoBranch, 0, 99, 1)
		
		ADD_WIDGET_INT_SLIDER("iRiverDialogueSide", iRiverDialogueSide, 0, 99, 1)
		ADD_WIDGET_INT_SLIDER("iRiverDialogueSideMoreCops", iRiverDialogueSideMoreCops, 0, 99, 1)
		
		
		ADD_WIDGET_INT_SLIDER("iRiverDialogue", iRiverDialogue, 0, 99, 1)
		
		
		ADD_WIDGET_BOOL("bIsAnimFinished", bIsAnimFinished)
		ADD_WIDGET_INT_SLIDER("iStageLestersIdle", iStageLestersIdle, 0, 99, 1)	
					
		START_WIDGET_GROUP("New Rubber Banding")
//			
//			ADD_WIDGET_FLOAT_READ_ONLY("fDesiredActual", fDesiredActual)
//			
//			ADD_WIDGET_FLOAT_READ_ONLY("fMaxActual", fMaxActual)
			ADD_WIDGET_FLOAT_READ_ONLY("vTargetVehicleOffsetChase.y", vTargetVehicleOffsetChase.y)
			ADD_WIDGET_STRING("LA river stuff")	
			ADD_WIDGET_FLOAT_READ_ONLY("fOffsetAmount", fOffsetAmount)
			
			ADD_WIDGET_FLOAT_READ_ONLY("fDesiredPlaybackSpeed", fDesiredPlaybackSpeed)
			ADD_WIDGET_FLOAT_READ_ONLY("fPLaybackSpeedCPS", fPLaybackSpeedCPS)
			ADD_WIDGET_FLOAT_READ_ONLY("fSpeedMultiplier", fSpeedMultiplier)
			ADD_WIDGET_FLOAT_READ_ONLY("fminPlaybackSpeed", fminPlaybackSpeed)
			ADD_WIDGET_FLOAT_READ_ONLY("fMaxPlaybackSpeed", fMaxPlaybackSpeed)
							
//			ADD_WIDGET_FLOAT_READ_ONLY("fMaxPlaybackSpeedChase", fMaxPlaybackSpeedChase)
//			ADD_WIDGET_FLOAT_READ_ONLY("fDesiredPlaybackSpeedChase", fDesiredPlaybackSpeedChase)
//			
//			ADD_WIDGET_FLOAT_SLIDER("fPLaybackSpeedNumeratorChase", fPLaybackSpeedNumeratorChase, 0.1, 2000, 0.01)
//			
//			ADD_WIDGET_FLOAT_READ_ONLY("fPlaybackSpeedNew", fPlaybackSpeedNew)
//			
//			ADD_WIDGET_FLOAT_SLIDER("fSpeedMultiplier", fSpeedMultiplier, 0.0, 10.0, 0.01)
//			
//			
//			ADD_WIDGET_BOOL("bDEbugIsInFront", bDEbugIsInFront)
//						
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("LA RIVER")
		//	ADD_WIDGET_FLOAT_READ_ONLY("fWaypointRubberBandSpeed1", fWaypointRubberBandSpeed1)
						
			ADD_WIDGET_INT_READ_ONLY("iBlipCount", iBlipCount)
			
			ADD_WIDGET_FLOAT_READ_ONLY("fWaypointRubberBandSpeed2", fWaypointRubberBandSpeed2)
			ADD_WIDGET_FLOAT_READ_ONLY("fPoliceSpeed[0]", fPoliceSpeed[0])
			ADD_WIDGET_FLOAT_READ_ONLY("fPoliceSpeed[1]", fPoliceSpeed[1])
			ADD_WIDGET_FLOAT_READ_ONLY("fPoliceSpeed[2]", fPoliceSpeed[2])
			//ADD_WIDGET_FLOAT_READ_ONLY("fPoliceSpeed[3]", fPoliceSpeed[3])
			
			ADD_WIDGET_FLOAT_READ_ONLY("GET_ENTITY_SPEED(GET_LEADING_VEHICLE()", fWaypointRubberBandSpeed2)
			
			ADD_WIDGET_FLOAT_READ_ONLY("mainPlaybackSpeed", mainPlaybackSpeed)
			ADD_WIDGET_INT_READ_ONLY("iHeliCrashTrainStage", iHeliCrashTrainStage)
			ADD_WIDGET_FLOAT_READ_ONLY("fHeliProgressLaRiver", fHeliProgressLaRiver)
			
			ADD_WIDGET_INT_READ_ONLY("iLastEngineHealth", iLastEngineHealth)	
			
								
			ADD_WIDGET_VECTOR_SLIDER("vTarpPos", vTarpPos, -3000.0, 3000.0, 0.05)
			ADD_WIDGET_VECTOR_SLIDER("vTarpRot", vTarpRot, -360.0, 360.0, 0.1)
							
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Group1")
			ADD_WIDGET_FLOAT_SLIDER("X", posx, -2000, 2000, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Y", posy, -2000, 2000, 0.01)
			ADD_WIDGET_FLOAT_SLIDER("Z", posz, -2000, 2000, 0.01)
			ADD_WIDGET_BOOL("Direction", bTrainDirection)
			/*ADD_WIDGET_INT_SLIDER("red", colred, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("blue", colblue, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("green", colgreen, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("alpha", colalpha, 0, 255, 1)*/
		STOP_WIDGET_GROUP()

				
		TEXT_LABEL_23 nameLabel
		
		START_WIDGET_GROUP("Text Queue")
			FOR iTextCounter = 0 TO TEXT_QUEUE_LENGTH-1
				nameLabel = "Entry"
				nameLabel += " "
				nameLabel += iTextCounter
				START_WIDGET_GROUP(nameLabel)
				
					textQueue[iTextCounter].textQueueText = ADD_TEXT_WIDGET("sText")
					//ADD_WIDGET_STRING(textQueue[iTextCounter].sText)
					ADD_WIDGET_INT_READ_ONLY("Entry Hash", textQueue[iTextCounter].TextHash)
					ADD_WIDGET_BOOL("Waiting?", textQueue[iTextCounter].bWaiting)
					ADD_WIDGET_INT_READ_ONLY("Type ID", textQueue[iTextCounter].textType)
					ADD_WIDGET_BOOL("Plays alongside other", textQueue[iTextCounter].bPlaysAlongsideOther)
					ADD_WIDGET_INT_READ_ONLY("Delay", textQueue[iTextCounter].iDelay)
				STOP_WIDGET_GROUP()
			ENDFOR
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Glass Smash Effects")
			ADD_WIDGET_VECTOR_SLIDER("vSmashParticleOffset", vSmashParticleOffset, -100.0, 100.0, 0.05)
			ADD_WIDGET_VECTOR_SLIDER("VSmashParticleRotation", VSmashParticleRotation, -100.0, 100.0, 0.05)
			ADD_WIDGET_BOOL("bAttemptToResetCases", bAttemptToResetCases)
			
		STOP_WIDGET_GROUP()
		
		
		START_WIDGET_GROUP("Colours")
			ADD_WIDGET_INT_SLIDER("Colour1", iColour1, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Colour2", iColour2, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Extra", iExtra, 0, 255, 1)
			ADD_WIDGET_BOOL("Extra On/Off", bExtraOnOff)
			ADD_WIDGET_INT_SLIDER("ExtraColour1",iExtraColour1, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("ExtraColour2", iExtraColour2, 0, 255, 1)
			ADD_WIDGET_INT_SLIDER("Livery", iLivery, 0, 255, 1)
		STOP_WIDGET_GROUP()
				
		INT i
		
		START_WIDGET_GROUP("Event Flags")
			FOR i = 0 TO NUMBER_OF_EVENT_FLAGS - 1
				TEXT_LABEL_31 stEventFlag = "bEventFlag"
				stEventFlag += i
				ADD_WIDGET_BOOL(stEventFlag, bEventFlag[i])
			ENDFOR
		STOP_WIDGET_GROUP()
	STOP_WIDGET_GROUP()
	SET_LOCATES_HEADER_WIDGET_GROUP(widgetDebug)
ENDPROC

#ENDIF

//PURPOSE:	Manages skipping of player chars
PROC MANAGE_PLAYER_CHAR_SKIP(BOOL bReplaySetup = FALSE)
	BOOL bSkipDoAnything = FALSE
	BOOL bSkipPlayingAsFranklin = FALSE
	BOOL bSkipSpawnOtherPlayerPed = FALSE
	VECTOR vSkipFranklinSpawnCoords = VECTOR_ZERO
	FLOAT fSkipFranklinSpawnHeading = 0.0
	VECTOR vSkipMichaelSpawnCoords = VECTOR_ZERO
	FLOAT fSkipMichaelSpawnHeading = 0.0
	BOOL bSkipFranklinHasBag = FALSE
	BOOL bSkipMichaelHasBag = FALSE
	
	SWITCH eMissionStage
		CASE STAGE_INIT_MISSION
			
		BREAK
		CASE STAGE_CUTSCENE_BEGIN				
			/*bSkipDoAnything = TRUE
			bSkipPlayingAsFranklin = FALSE
			bSkipSpawnOtherPlayerPed = TRUE
			vSkipFranklinSpawnCoords = vFranklinSpawnCoords
			fSkipFranklinSpawnHeading = fFranklinSpawnHeading
			vSkipMichaelSpawnCoords = vMichaelSpawnCoords
			fSkipMichaelSpawnHeading = fMichaelSpawnHeading
			bSkipFranklinHasBag = FALSE
			bSkipMichaelHasBag = FALSE*/
			bSkipDoAnything = TRUE
			bSkipPlayingAsFranklin = FALSE
			bSkipSpawnOtherPlayerPed = FALSE
			vSkipMichaelSpawnCoords = vMichaelSpawnCoords
			fSkipMichaelSpawnHeading = fMichaelSpawnHeading
			bSkipMichaelHasBag = FALSE
		BREAK
		CASE STAGE_DRIVE_TO_STORE
			IF bIsStealthPath
				bSkipDoAnything = TRUE
				bSkipPlayingAsFranklin = FALSE
				bSkipSpawnOtherPlayerPed = TRUE
				vSkipFranklinSpawnCoords = vFranklinSpawnCoords
				fSkipFranklinSpawnHeading = fFranklinSpawnHeading
				vSkipMichaelSpawnCoords = vMichaelSpawnCoords
				fSkipMichaelSpawnHeading = fMichaelSpawnHeading
			ELSE
				bSkipDoAnything = TRUE
				bSkipPlayingAsFranklin = TRUE
				bSkipSpawnOtherPlayerPed = TRUE
				vSkipFranklinSpawnCoords = << 705.2056, -965.3800, 29.3953 >>//vFranklinSpawnCoords
				fSkipFranklinSpawnHeading = 278.2279 //fFranklinSpawnHeading
				vSkipMichaelSpawnCoords = vMichaelWalkToCoords
				fSkipMichaelSpawnHeading = fMichaelWalkToHeading
			ENDIF
			bSkipFranklinHasBag = FALSE
			bSkipMichaelHasBag = FALSE
		BREAK
		CASE STAGE_ROOF
			bSkipDoAnything = TRUE
			bSkipPlayingAsFranklin = TRUE
			bSkipSpawnOtherPlayerPed = TRUE
			vSkipFranklinSpawnCoords = <<-578.8325, -280.2415, 34.3058>>//vBackOfStore
			fSkipFranklinSpawnHeading = 109.3759//123.5087
			vSkipMichaelSpawnCoords = vDriveAIPoint+<<0,2,0>>
			fSkipMichaelSpawnHeading = fCarSpawnHeading
			bSkipFranklinHasBag = FALSE
			bSkipMichaelHasBag = FALSE
		BREAK
		CASE STAGE_CUTSCENE_GAS_STORE
			bSkipDoAnything = TRUE
			bSkipPlayingAsFranklin = TRUE
			bSkipSpawnOtherPlayerPed = TRUE
			vSkipFranklinSpawnCoords = <<-614.3, -224.1, 37.5>>//vDriveToPoint
			fSkipFranklinSpawnHeading = 0.0
			vSkipMichaelSpawnCoords = <<-634.8, -235.7, 38.0>>//vDriveAIPoint+<<0,2,0>>
			fSkipMichaelSpawnHeading = fCarSpawnHeading
			bSkipFranklinHasBag = FALSE
			bSkipMichaelHasBag = FALSE
		BREAK
		CASE STAGE_CUTSCENE_AGGRESSIVE_ARRIVE
			bSkipDoAnything = TRUE
			bSkipPlayingAsFranklin = TRUE
			bSkipSpawnOtherPlayerPed = TRUE
			vSkipFranklinSpawnCoords = << -668.6204, -227.7277, 36.1821 >>
			fSkipFranklinSpawnHeading = 0.0
			vSkipMichaelSpawnCoords = << -669.6204, -227.7277, 36.1821 >>
			fSkipMichaelSpawnHeading = 0.0
			bSkipFranklinHasBag = FALSE
			bSkipMichaelHasBag = TRUE
		BREAK
		CASE STAGE_GRAB_LOOT
			bSkipDoAnything = TRUE
			bSkipPlayingAsFranklin = FALSE
			bSkipSpawnOtherPlayerPed = TRUE
			vSkipFranklinSpawnCoords = << -620.1548, -232.3427, 56.1278 >>
			fSkipFranklinSpawnHeading = 31.6812
			IF bIsStealthPath
				vSkipMichaelSpawnCoords = << -630.3759, -235.1867, 37.0570 >> //<< -634.3728, -239.7245, 37.0449 >>
				fSkipMichaelSpawnHeading =  301.4192 //310.4579
			ELSE
				vSkipMichaelSpawnCoords = << -628.4352, -234.1821, 37.0570 >>//<< -629.7005, -234.0132, 37.0570 >>
				fSkipMichaelSpawnHeading =  292.9868  //276.6718
			ENDIF
			
			bSkipFranklinHasBag = FALSE
			bSkipMichaelHasBag = TRUE
		BREAK
		CASE STAGE_CUTSCENE_LEAVE_STORE
			bSkipDoAnything = TRUE
			bSkipPlayingAsFranklin = FALSE
			bSkipSpawnOtherPlayerPed = TRUE
			vSkipFranklinSpawnCoords = << -637.3582, -241.8902, 37.1180 >>
			fSkipFranklinSpawnHeading = 300.7801
			vSkipMichaelSpawnCoords = vLeaveScenePos
			fSkipMichaelSpawnHeading = 0.0
			bSkipFranklinHasBag = FALSE
			bSkipMichaelHasBag = FALSE
		BREAK
		CASE STAGE_BIKE_CHASE
			bSkipDoAnything = TRUE
			bSkipPlayingAsFranklin = TRUE
			bSkipSpawnOtherPlayerPed = TRUE
			vSkipFranklinSpawnCoords = << -637.3582, -241.8902, 37.1180 >>
			fSkipFranklinSpawnHeading = 300.7801
			//vSkipMichaelSpawnCoords = vJewelryExitCoords
			//fSkipMichaelSpawnHeading = 131.8341
			
			vSkipMichaelSpawnCoords = <<-631.91541, -244.42168,37.233>>
			fSkipFranklinSpawnHeading = 301.4192
			
			bSkipFranklinHasBag = TRUE
			bSkipMichaelHasBag = FALSE
		BREAK
		CASE STAGE_BIKE_CHASE_START_OF_TUNNEL
			bSkipDoAnything = TRUE
			bSkipPlayingAsFranklin = TRUE
			bSkipSpawnOtherPlayerPed = TRUE
			vSkipFranklinSpawnCoords = <<-63.0551, -541.7521, 30.8795>>//, 219.6241//<< -57.1670, -547.4032, 30.3138 >>
			fSkipFranklinSpawnHeading = 219.6241//183.8065
			vSkipMichaelSpawnCoords = vJewelryExitCoords
			fSkipMichaelSpawnHeading = 131.8341
			bSkipFranklinHasBag = TRUE
			bSkipMichaelHasBag = FALSE
		BREAK
		CASE STAGE_BIKE_CHASE_END_OF_TUNNEL
			bSkipDoAnything = TRUE
			bSkipPlayingAsFranklin = TRUE
			bSkipSpawnOtherPlayerPed = TRUE
			vSkipFranklinSpawnCoords = << 1021.4163, -201.1791, 41.7879 >>
			fSkipFranklinSpawnHeading = 183.8065
			vSkipMichaelSpawnCoords = vJewelryExitCoords
			fSkipMichaelSpawnHeading = 131.8341
			bSkipFranklinHasBag = TRUE
			bSkipMichaelHasBag = FALSE
		BREAK
		CASE STAGE_TRUCK_FIGHT
			bSkipDoAnything = TRUE
			bSkipPlayingAsFranklin = FALSE
			bSkipSpawnOtherPlayerPed = TRUE
			vSkipFranklinSpawnCoords = << 608.8254, -850.8471, 9.8777 >>
			fSkipFranklinSpawnHeading = fVehicleFightBikeSpawn[2]
			vSkipMichaelSpawnCoords = << 608.8254, -850.8471, 9.8777 >>//vVehicleFightBikeSpawn[2]+<<3,0,0>>
			fSkipMichaelSpawnHeading = fVehicleFightBikeSpawn[2]
			bSkipFranklinHasBag = TRUE
			bSkipMichaelHasBag = FALSE
		BREAK
		CASE STAGE_CUTSCENE_RAMP_DOWN
			bSkipDoAnything = TRUE
			bSkipPlayingAsFranklin = FALSE
			bSkipSpawnOtherPlayerPed = TRUE
			vSkipFranklinSpawnCoords = <<636.5349, -1846.3693, 8.2475>>
			fSkipFranklinSpawnHeading = 175.6113 
			vSkipMichaelSpawnCoords = <<636.5349, -1846.3693, 8.2475>>//vVehicleFightBikeSpawn[2]+<<3,0,0>>
			fSkipMichaelSpawnHeading =  175.6113 
			bSkipFranklinHasBag = TRUE
			bSkipMichaelHasBag = FALSE
		BREAK
		CASE STAGE_RETURN_TO_BASE
			bSkipDoAnything = TRUE
			bSkipPlayingAsFranklin = FALSE
			bSkipSpawnOtherPlayerPed = TRUE
			vSkipFranklinSpawnCoords = <<636.5349, -1846.3693, 8.2475>>
			fSkipFranklinSpawnHeading = 175.6113 
			vSkipMichaelSpawnCoords = <<636.5349, -1846.3693, 8.2475>>//vVehicleFightBikeSpawn[2]+<<3,0,0>>
			fSkipMichaelSpawnHeading =  175.6113 
			bSkipFranklinHasBag = FALSE
			bSkipMichaelHasBag = FALSE
		BREAK
		CASE PASSED_MISSION
			bSkipDoAnything = TRUE
			bSkipPlayingAsFranklin = TRUE
			bSkipSpawnOtherPlayerPed = FALSE
			vSkipFranklinSpawnCoords = <<-13.9543, -1451.4767, 29.5418>>
			fSkipFranklinSpawnHeading = 0.0
			vSkipMichaelSpawnCoords = <<-13.9543, -1455.4767, 29.5418>>
			fSkipMichaelSpawnHeading = 0.0
			bSkipFranklinHasBag = FALSE
			bSkipMichaelHasBag = FALSE	
		BREAK
	ENDSWITCH
	
	IF bSkipDoAnything
		IF bSkipPlayingAsFranklin
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
				IF NOT DOES_SELECT_PED_EXIST(SEL_FRANKLIN)
					WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, 
														vSkipFranklinSpawnCoords)
						WAIT(0)
					ENDWHILE
				ENDIF
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
			ENDIF
			
			
			IF NOT bReplaySetup
				SET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN), vSkipFranklinSpawnCoords)
				SET_ENTITY_HEADING(GET_SELECT_PED(SEL_FRANKLIN), fSkipFranklinSpawnHeading)
			ELSE
				START_REPLAY_SETUP(vSkipFranklinSpawnCoords, fSkipFranklinSpawnHeading)				
			ENDIF
			
			IF bSkipSpawnOtherPlayerPed
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, 
													vSkipMichaelSpawnCoords, fSkipMichaelSpawnHeading)
					WAIT(0)
				ENDWHILE
			ENDIF
		ELSE
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
				IF NOT DOES_SELECT_PED_EXIST(SEL_MICHAEL)
					WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, 
														vSkipMichaelSpawnCoords)
						WAIT(0)
					ENDWHILE
				ENDIF
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
			ENDIF
			IF NOT bReplaySetup
			// REPLAY! remove this for new replay setup stuff
				SET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL), vSkipMichaelSpawnCoords)
				SET_ENTITY_HEADING(GET_SELECT_PED(SEL_MICHAEL), fSkipMichaelSpawnHeading)
			// REPLAY!
			ELSE
				START_REPLAY_SETUP(vSkipMichaelSpawnCoords, fSkipMichaelSpawnHeading)
			ENDIF
			
			
			IF bSkipSpawnOtherPlayerPed
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, 
													vSkipFranklinSpawnCoords, fSkipFranklinSpawnHeading)
					WAIT(0)
				ENDWHILE
			ENDIF
		ENDIF
		IF bSkipPlayingAsFranklin
		OR bSkipSpawnOtherPlayerPed
			SET_PED_BAG(GET_SELECT_PED(SEL_FRANKLIN), bSkipFranklinHasBag)
			SET_PED_CREW(GET_SELECT_PED(SEL_FRANKLIN))
		ENDIF
		IF NOT bSkipPlayingAsFranklin
		OR bSkipSpawnOtherPlayerPed
			SET_PED_BAG(GET_SELECT_PED(SEL_MICHAEL), bSkipMichaelHasBag)
			SET_PED_CREW(GET_SELECT_PED(SEL_MICHAEL))
			SETUP_PLAYER_PEDS_SUITS()
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:	Manages skipping of crew
PROC MANAGE_CREW_SKIP()
	BOOL bSkipDoAnything = FALSE
	BOOL bSkipSpawnBestGun = FALSE
	VECTOR vSkipBestGunCoords = VECTOR_ZERO
	FLOAT fSkipBestGunHeading = 0.0
	BOOL bSkipBestGunHasBag = FALSE
	BOOL bSkipSpawnHackerNewNew = FALSE
	VECTOR vSkipDriverNewCoords = VECTOR_ZERO
	FLOAT fSkipDriverNewHeading = 0.0
	BOOL bSkipDriverNewHasBag = FALSE
	BOOL bSkipSpawnHackerNew = FALSE
	VECTOR vSkipHackerNewCoords = VECTOR_ZERO
	FLOAT fSkipHackerNewHeading = 0.0
//	BOOL bSkipSpawnHacker = FALSE
//	VECTOR vSkipHackerCoords = VECTOR_ZERO
//	FLOAT fSkipHackerHeading = 0.0

	RESET_NEED_LIST()
	IF bIsStealthPath
		SET_MODEL_AS_NEEDED(Asset.mnGunman)
	ELSE
		SET_MODEL_AS_NEEDED(Asset.mnGunman)
	ENDIF
	MANAGE_LOADING_NEED_LIST(FALSE)
	SAFE_REQUEST_MODELS_FOR_HEIST_CREW(HEIST_JEWEL, TRUE, FALSE)
	SAFE_REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL, TRUE, FALSE)

	SWITCH eMissionStage
		CASE STAGE_INIT_MISSION
			
		BREAK
		CASE STAGE_CUTSCENE_BEGIN
			/*bSkipDoAnything = TRUE
			bSkipSpawnBestGun = TRUE
			vSkipBestGunCoords = vBeginWalkToCoords[0]
			fSkipBestGunHeading = fBeginWalkToHeading[0]
			bSkipBestGunHasBag = FALSE
			bSkipSpawnHackerNewNew = TRUE
			vSkipDriverNewCoords = vBeginWalkToCoords[1]
			fSkipDriverNewHeading = fBeginWalkToHeading[1]
			bSkipDriverNewHasBag = FALSE
			bSkipSpawnHackerNew = TRUE
			vSkipHackerNewCoords = vBeginWalkToCoords[3]
			fSkipHackerNewHeading = fBeginWalkToHeading[3]
			bSkipSpawnHacker = TRUE
			vSkipHackerCoords = vBeginWalkToCoords[2]
			fSkipHackerHeading = fBeginWalkToHeading[2]*/
		BREAK
		CASE STAGE_DRIVE_TO_STORE
			bSkipDoAnything = TRUE
			bSkipSpawnBestGun = TRUE
			vSkipBestGunCoords = vBeginWalkToCoords[0]
			fSkipBestGunHeading = fBeginWalkToHeading[0]
			bSkipBestGunHasBag = FALSE
			bSkipSpawnHackerNewNew = TRUE
			vSkipDriverNewCoords = vBeginWalkToCoords[1]
			fSkipDriverNewHeading = fBeginWalkToHeading[1]
			bSkipDriverNewHasBag = FALSE
			bSkipSpawnHackerNew = TRUE
			vSkipHackerNewCoords = vBeginWalkToCoords[3]
			fSkipHackerNewHeading = fBeginWalkToHeading[3]
//			bSkipSpawnHacker = TRUE
//			vSkipHackerCoords = vBeginWalkToCoords[2]
//			fSkipHackerHeading = fBeginWalkToHeading[2]
		BREAK
		CASE STAGE_ROOF
			bSkipDoAnything = TRUE
			bSkipSpawnBestGun = TRUE
			vSkipBestGunCoords = vDriveAIPoint+<<2,0,0>>
			fSkipBestGunHeading = fBeginWalkToHeading[0]
			bSkipBestGunHasBag = FALSE
			bSkipSpawnHackerNewNew = TRUE
			vSkipDriverNewCoords = vDriveAIPoint+<<-2,0,0>>
			fSkipDriverNewHeading = fBeginWalkToHeading[1]
			bSkipDriverNewHasBag = FALSE
		BREAK
		CASE STAGE_CUTSCENE_GAS_STORE
			bSkipDoAnything = TRUE
			bSkipSpawnBestGun = TRUE
			vSkipBestGunCoords = vDriveAIPoint+<<2,0,0>>
			fSkipBestGunHeading = fBeginWalkToHeading[0]
			bSkipBestGunHasBag = FALSE
			bSkipSpawnHackerNewNew = TRUE
			vSkipDriverNewCoords = vDriveAIPoint+<<-2,0,0>>
			fSkipDriverNewHeading = fBeginWalkToHeading[1]
			bSkipDriverNewHasBag = FALSE
		BREAK
		CASE STAGE_CUTSCENE_AGGRESSIVE_ARRIVE
			bSkipDoAnything = TRUE
			bSkipSpawnBestGun = TRUE
			vSkipBestGunCoords = vDriveAIPoint+<<2,0,0>>
			fSkipBestGunHeading = fBeginWalkToHeading[0]
			bSkipBestGunHasBag = TRUE
			bSkipSpawnHackerNewNew = TRUE
			vSkipDriverNewCoords = vDriveAIPoint+<<-2,0,0>>
			fSkipDriverNewHeading = fBeginWalkToHeading[1]
			bSkipDriverNewHasBag = TRUE
		BREAK
		CASE STAGE_GRAB_LOOT
			bSkipDoAnything = TRUE
			bSkipSpawnBestGun = TRUE
			vSkipBestGunCoords = << -631.7494, -239.9052, 37.1067 >>
			fSkipBestGunHeading = 126.8600
			bSkipBestGunHasBag = TRUE
			bSkipSpawnHackerNewNew = TRUE
			vSkipDriverNewCoords = << -633.5875, -237.0871, 37.0487 >>
			fSkipDriverNewHeading = 125.3744
			bSkipDriverNewHasBag = TRUE
		BREAK
		CASE STAGE_CUTSCENE_LEAVE_STORE
			bSkipDoAnything = TRUE
			bSkipSpawnBestGun = TRUE
			vSkipBestGunCoords = << -633.5875, -237.0871, 37.0487 >>
			fSkipBestGunHeading = 125.3744
			bSkipBestGunHasBag = TRUE
			bSkipSpawnHackerNewNew = TRUE
			vSkipDriverNewCoords = << -631.7494, -239.9052, 37.1067 >>
			fSkipDriverNewHeading = 126.8600
			bSkipDriverNewHasBag = TRUE
		BREAK
		CASE STAGE_BIKE_CHASE
			bSkipDoAnything = TRUE
			bSkipSpawnBestGun = TRUE
			vSkipBestGunCoords = << -633.5875, -237.0871, 37.0487 >>
			fSkipBestGunHeading = 125.3744
			bSkipBestGunHasBag = TRUE
			bSkipSpawnHackerNewNew = TRUE
			vSkipDriverNewCoords = << -631.7494, -239.9052, 37.1067 >>
			fSkipDriverNewHeading = 126.8600
			bSkipDriverNewHasBag = TRUE
		BREAK
		CASE STAGE_BIKE_CHASE_START_OF_TUNNEL
			bSkipDoAnything = TRUE
			bSkipSpawnBestGun = TRUE
			vSkipBestGunCoords = << -633.5875, -237.0871, 37.0487 >>
			fSkipBestGunHeading = 125.3744
			bSkipBestGunHasBag = TRUE
			bSkipSpawnHackerNewNew = TRUE
			vSkipDriverNewCoords = << -631.7494, -239.9052, 37.1067 >>
			fSkipDriverNewHeading = 126.8600
			bSkipDriverNewHasBag = TRUE
		BREAK
		CASE STAGE_BIKE_CHASE_END_OF_TUNNEL
			bSkipDoAnything = TRUE
			bSkipSpawnBestGun = TRUE
			vSkipBestGunCoords = << 1025.4329, -211.5901, 42.0321 >>
			fSkipBestGunHeading = 182.3647
			bSkipBestGunHasBag = TRUE
			bSkipSpawnHackerNewNew = TRUE
			vSkipDriverNewCoords = << 1021.8690, -223.4366, 42.2820 >>
			fSkipDriverNewHeading = 187.8349
			bSkipDriverNewHasBag = TRUE
		BREAK
//		CASE STAGE_CUTSCENE_TRUCK_ARRIVE
//			bSkipDoAnything = TRUE
//			bSkipSpawnBestGun = TRUE
//			vSkipBestGunCoords = vVehicleFightBikeSpawn[0]+<<3,0,0>>
//			fSkipBestGunHeading = 125.3744
//			bSkipBestGunHasBag = TRUE
//			bSkipSpawnHackerNewNew = NOT bWorstGunDies
//			vSkipDriverNewCoords = vVehicleFightBikeSpawn[1]+<<3,0,0>>
//			fSkipDriverNewHeading = 126.8600
//			bSkipDriverNewHasBag = TRUE
//		BREAK
		CASE STAGE_TRUCK_FIGHT
			bSkipDoAnything = TRUE
			bSkipSpawnBestGun = TRUE
			vSkipBestGunCoords = vVehicleFightBikeSpawn[0]+<<3,0,0>>
			fSkipBestGunHeading = 125.3744
			bSkipBestGunHasBag = TRUE
			bSkipSpawnHackerNewNew = NOT bWorstGunDies
			vSkipDriverNewCoords = vVehicleFightBikeSpawn[1]+<<3,0,0>>
			fSkipDriverNewHeading = 126.8600
			bSkipDriverNewHasBag = TRUE
			bSkipSpawnHackerNew = TRUE
			vSkipHackerNewCoords = vCrewTruckCoords
			fSkipHackerNewHeading = fCrewTruckHeading
		BREAK
		CASE STAGE_CUTSCENE_RAMP_DOWN
			bSkipDoAnything = TRUE
			bSkipSpawnBestGun = TRUE
			vSkipBestGunCoords = vVehicleFightBikeSpawn[0]+<<3,0,0>>
			fSkipBestGunHeading = 125.3744
			bSkipBestGunHasBag = TRUE
			bSkipSpawnHackerNewNew = NOT bWorstGunDies
			vSkipDriverNewCoords = vVehicleFightBikeSpawn[1]+<<3,0,0>>
			fSkipDriverNewHeading = 126.8600
			bSkipDriverNewHasBag = TRUE
			bSkipSpawnHackerNew = TRUE
			vSkipHackerNewCoords = vCrewTruckCoords
			fSkipHackerNewHeading = fCrewTruckHeading
		BREAK
		CASE STAGE_RETURN_TO_BASE
			bSkipDoAnything = TRUE
			bSkipSpawnBestGun = TRUE
			vSkipBestGunCoords = << -633.5875, -237.0871, 37.0487 >>
			fSkipBestGunHeading = 125.3744
			bSkipBestGunHasBag = TRUE
			bSkipSpawnHackerNewNew = NOT bWorstGunDies
			vSkipDriverNewCoords = << -631.7494, -239.9052, 37.1067 >>
			fSkipDriverNewHeading = 126.8600
			bSkipDriverNewHasBag = TRUE
			bSkipSpawnHackerNew = TRUE
			vSkipHackerNewCoords = vCrewTruckCoords
			fSkipHackerNewHeading = fCrewTruckHeading
		BREAK
		CASE PASSED_MISSION
			
		BREAK
	ENDSWITCH
		
	IF bSkipDoAnything
		IF bSkipSpawnBestGun
			//pedCrew[CREW_DRIVER_NEW] = CREATE_PED(PEDTYPE_MISSION, Asset.mnGunman, vSkipBestGunCoords, fSkipBestGunHeading)						
			pedCrew[CREW_DRIVER_NEW] = CREATE_HEIST_CREW_MEMBER_AT_INDEX(	HEIST_JEWEL,  ENUM_TO_INT(CREW_DRIVER_NEW), 
																		vSkipBestGunCoords, fSkipBestGunHeading)
			SET_PED_BAG(pedCrew[CREW_DRIVER_NEW], bSkipBestGunHasBag)
			SET_PED_CREW(pedCrew[CREW_DRIVER_NEW])
		ENDIF
		IF bSkipSpawnHackerNewNew
			//pedCrew[CREW_GUNMAN_NEW] = CREATE_PED(PEDTYPE_MISSION, Asset.mnGunman, vSkipDriverNewCoords, fSkipDriverNewHeading)
			pedCrew[CREW_GUNMAN_NEW] = CREATE_HEIST_CREW_MEMBER_AT_INDEX(	HEIST_JEWEL,  ENUM_TO_INT(CREW_GUNMAN_NEW), 
																		vSkipDriverNewCoords, fSkipDriverNewHeading)
			
			SET_PED_BAG(pedCrew[CREW_GUNMAN_NEW], bSkipDriverNewHasBag)
			SET_PED_CREW(pedCrew[CREW_GUNMAN_NEW])
		ENDIF
		IF bSkipSpawnHackerNew
			pedCrew[CREW_HACKER] = CREATE_HEIST_CREW_MEMBER_AT_INDEX(	HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER), 
																		vSkipHackerNewCoords, fSkipHackerNewHeading)
			SET_PED_CREW(pedCrew[CREW_HACKER])
			GIVE_WEAPON_TO_PED(pedCrew[CREW_HACKER], WEAPONTYPE_PISTOL, 1000)
			SET_PED_ACCURACY(pedCrew[CREW_HACKER], 50)
		ENDIF
//		IF bSkipSpawnHacker
//			pedCrew[CREW_HACKER] = CREATE_HEIST_CREW_MEMBER_AT_INDEX(	HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER), 
//																		vSkipHackerCoords, fSkipHackerHeading)
//			SET_PED_CREW(pedCrew[CREW_HACKER])
//		ENDIF
	ENDIF
	SETUP_CREW_SUITS()
	SET_MODELS_FOR_HEIST_CREW_AS_NO_LONGER_NEEDED(HEIST_JEWEL)
	REMOVE_WEAPON_ASSET(WEAPONTYPE_PISTOL)
ENDPROC


PROC CREATE_BIKES_IF_THEY_DONT_EXIST(INT iCreatePosition = 0)

	SWITCH iCreatePosition
	
	CASE 0 //start of chase outside shop
		IF NOT DOES_ENTITY_EXIST(vehBike[0])
			vehBike[0] = CREATE_VEHICLE(	Asset.mnBike, vBikeSpawnCoords[0]+ vBatiRecOffset, fBikeSpawnHeading[0])
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehBike[1])
		AND NOT (bWorstGunDies AND eMissionStage >= STAGE_BIKE_CHASE_START_OF_TUNNEL )	//dont spawn it if playing from halfway
			vehBike[1] = CREATE_VEHICLE(	Asset.mnBike, vBikeSpawnCoords[1] + vBatiRecOffset, fBikeSpawnHeading[1])
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehBike[2])
			vehBike[2] = CREATE_VEHICLE(Asset.mnBike, vBikeSpawnCoords[2] + vBatiRecOffset, fBikeSpawnHeading[2])
		ENDIF
	BREAK
	
	CASE 1
		IF NOT DOES_ENTITY_EXIST(vehBike[0])
			vehBike[0] = CREATE_VEHICLE(	Asset.mnBike, <<-53.0656, -551.6052, 29.5633>>, 219.6935   )
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehBike[1])
		AND NOT (bWorstGunDies AND eMissionStage >= STAGE_BIKE_CHASE_START_OF_TUNNEL )	//dont spawn it if playing from halfway
			vehBike[1] = CREATE_VEHICLE(	Asset.mnBike, <<-53.0656, -551.6052, 29.5633>>, 219.6935   )
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehBike[2])
			vehBike[2] = CREATE_VEHICLE(Asset.mnBike, <<-63.0551, -541.7521, 30.8795>>, 219.6935   )
		ENDIF
	BREAK
	
	CASE 2
		IF NOT DOES_ENTITY_EXIST(vehBike[0])
			vehBike[0] = CREATE_VEHICLE(	Asset.mnBike, <<1033.8641, -284.8706, 49.2269>> + vBatiRecOffset, 208.3043)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehBike[1])
		AND NOT (bWorstGunDies AND eMissionStage >= STAGE_BIKE_CHASE_START_OF_TUNNEL )	//dont spawn it if playing from halfway
			vehBike[1] = CREATE_VEHICLE(	Asset.mnBike, <<1035.8641, -284.8706, 49.2269>> + vBatiRecOffset, 208.3043)
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(vehBike[2])
			vehBike[2] = CREATE_VEHICLE(Asset.mnBike, <<1037.8641, -284.8706, 49.2269>> + vBatiRecOffset, 208.3043)
		ENDIF
	BREAK
	
	ENDSWITCH
	
	IF NOT IS_ENTITY_DEAD(vehBike[0])
		SET_VEHICLE_LOD_MULTIPLIER(vehBike[0], 2.0)
		SET_VEHICLE_ACT_AS_IF_HIGH_SPEED_FOR_FRAG_SMASHING(vehBike[0], TRUE)
		SET_VEHICLE_ACTIVE_DURING_PLAYBACK(vehBike[0], TRUE)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehBike[0], FALSE)
		SET_ENTITY_PROOFS(vehBike[0], FALSE, FALSE, FALSE, TRUE, FALSE, TRUE)
	ENDIF
	IF NOT IS_ENTITY_DEAD(vehBike[1])
		SET_VEHICLE_LOD_MULTIPLIER(vehBike[1], 2.0)
		SET_VEHICLE_ACT_AS_IF_HIGH_SPEED_FOR_FRAG_SMASHING(vehBike[1], TRUE)
		SET_VEHICLE_ACTIVE_DURING_PLAYBACK(vehBike[1], TRUE)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehBike[1], FALSE)
		SET_ENTITY_PROOFS(vehBike[1], FALSE, FALSE, FALSE, TRUE, FALSE, TRUE)
	ENDIF
	IF NOT IS_ENTITY_DEAD(vehBike[2])
		SET_VEHICLE_LOD_MULTIPLIER(vehBike[2], 2.0)
		
		SET_ENTITY_PROOFS(vehBike[2], FALSE, FALSE, FALSE, TRUE, FALSE, TRUE)
		SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(vehBike[2], FALSE)
		
		IF asset.mnBike = BATI2
			FORCE_USE_AUDIO_GAME_OBJECT(vehBike[2], "CARBONRS")
		ENDIF
		INFORM_MISSION_STATS_OF_SPEED_WATCH_ENTITY(vehBIke[2])
		
	ENDIF
	
	SET_VEHICLE_NAME_DEBUG(vehBike[0], "Bike 0")
	IF NOT bWorstGunDies
		SET_VEHICLE_NAME_DEBUG(vehBike[1], "Bike 1")
	ENDIF
	SET_VEHICLE_NAME_DEBUG(vehBike[2], "Bike 2")
//	IF NOT IS_ENTITY_DEAD(vehBike[2])
//		SET_VEHICLE_RADIO_ENABLED(vehBike[2] , FALSE)
//	ENDIF	
	SET_BIKES_MISSION_SAFE()
	
	PRINTLN("Creating bikes")
		
ENDPROC

FUNC INT GET_REPLAY_STAGE_FROM_MISSION_STAGE(MISSION_STAGE thisStage)
	
	INT iReplayNumber
	
	iReplayNumber = ENUM_TO_INT(thisStage) - ENUM_TO_INT(STAGE_DRIVE_TO_STORE)
	
	IF iReplayNumber < 0
		#IF IS_DEBUG_BUILD
			SCRIPT_ASSERT("JH2A/B - trying to set replay to a negative value")
		#ENDIF
		iReplayNumber = 0
	ENDIF
	
	#IF IS_DEBUG_BUILD
		PRINTLN("****** --- GET_REPLAY_STAGE_FROM_MISSION_STAGE: ", iReplayNumber)
	#ENDIF
	
	RETURN iReplayNumber

ENDFUNC

//PURPOSE:		Gives the guys silencers if they are going in Stealth
PROC GIVE_WEAPON_COMPONENT_TO_PED_IF_2A(PED_INDEX thisPed, WEAPON_TYPE thisWeaponType, WEAPONCOMPONENT_TYPE thisWeaponComponent)

	IF NOT bIsStealthPath 
		GIVE_WEAPON_COMPONENT_TO_PED(thisPed, thisWeaponType, thisWeaponComponent)
	ELSE
		GIVE_WEAPON_COMPONENT_TO_PED(thisPed, thisWeaponType, WEAPONCOMPONENT_AT_AR_SUPP_02)
	ENDIF
	
ENDPROC

PROC REQUEST_TRUCK_FIGHT_ASSETS()

	REQUEST_MODEL(Asset.mnBike)
	REQUEST_MODEL(Asset.mnTruck)
	REQUEST_MODEL(Asset.mnPolice)
	REQUEST_MODEL(Asset.mnPoliceman)
	REQUEST_VEHICLE_RECORDING(Asset.recNoGoodBikeChase, Asset.recUber)
	REQUEST_VEHICLE_RECORDING(Asset.recNumber,Asset.recUber)
	REQUEST_VEHICLE_RECORDING(717, "JHUBER")
	REQUEST_VEHICLE_RECORDING(700, "JHUBER")
	REQUEST_VEHICLE_RECORDING(722, Asset.recUber) //Franklins bike jump

	REQUEST_ANIM_DICT(Asset.adJewelHeistSwitchCam)
	REQUEST_ANIM_DICT(strAnimDictCamShake)
	
	REQUEST_MODEL_FOR_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)))
	REQUEST_MODEL_FOR_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)))
	REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL)
	
	REQUEST_PLAYER_PED_MODEL(CHAR_MICHAEL)
		
ENDPROC

FUNC BOOL HAVE_TRUCK_FIGHT_ASSETS_LOADED()

	IF HAS_MODEL_LOADED(Asset.mnBike)
	AND HAS_MODEL_LOADED(Asset.mnTruck)
	AND HAS_MODEL_LOADED(Asset.mnPolice)
	AND HAS_MODEL_LOADED(Asset.mnPoliceman)
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(Asset.recNoGoodBikeChase, Asset.recUber)
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(Asset.recNumber, Asset.recUber)
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(717, "JHUBER")
	AND HAS_VEHICLE_RECORDING_BEEN_LOADED(700, "JHUBER")
	AND HAS_ANIM_DICT_LOADED(Asset.adJewelHeistSwitchCam)
	AND HAS_ANIM_DICT_LOADED(strAnimDictCamShake)
	AND HAS_MODEL_LOADED_FOR_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)))
	AND HAS_MODEL_LOADED_FOR_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)))
	AND HAS_WEAPON_ASSET_LOADED(WEAPONTYPE_PISTOL)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

PROC SETUP_VAN()

	IF NOT IS_ENTITY_DEAD(vehVan)
		IF bIsStealthPath
			SET_VEHICLE_NUMBER_PLATE_TEXT(vehVan, "TODO PT1")
		ELSE				
			SET_VEHICLE_NUMBER_PLATE_TEXT(vehVan, "RW1602B0")
			SET_VEHICLE_COLOURS(vehvan, 0, 0)		
		ENDIF
	ENDIF
	
ENDPROC

//Purpose: Checks a vehicle for any extras, disables all extras that are found.
PROC REMOVE_ALL_VEHICLE_EXTRAS(VEHICLE_INDEX viVehicle)

	INT i
	CONST_INT iNumPossibleVehicleExtras 10

	
	IF DOES_ENTITY_EXIST(viVehicle)
		IF NOT IS_ENTITY_DEAD(viVehicle)	

			REPEAT iNumPossibleVehicleExtras i
				IF i > 0 //[MF] extra indexes must be greater than 0
					IF DOES_EXTRA_EXIST(viVehicle, i)
						IF IS_VEHICLE_EXTRA_TURNED_ON(viVehicle, i)
							SET_VEHICLE_EXTRA(viVehicle, i, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
		ENDIF
	ENDIF

ENDPROC

PROC CREATE_TRUCK_FIGHT_TRUCK()
	CDEBUG3LN(DEBUG_MISSION, "CREATE_TRUCK_FIGHT_TRUCK")
	
	vehTruck = CREATE_VEHICLE(Asset.mnTruck, <<1104.0579, -222.2266, 55.7692>>, 147.2209)
	
	//[MF]Remove all randomly assigned extras from the vehicle (B* 1689820)
	REMOVE_ALL_VEHICLE_EXTRAS(vehTruck)
	
	IF DOES_ENTITY_EXIST(vehTruck)
		IF NOT IS_ENTITY_DEAD(vehTruck)
			SET_VEHICLE_COLOURS(vehTruck, 128, 3)
			SET_VEHICLE_EXTRA(vehTruck, 6, FALSE) //SET_VEHICLE_LIVERY(vehTruck, 1)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTruck, TRUE)
			SET_ENTITY_PROOFS(vehTruck, FALSE, TRUE, TRUE, FALSE, FALSE)
			
			SET_VEHICLE_CAN_BREAK(vehTruck, FALSE)
			SET_PED_INTO_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], vehTruck)
			
		ENDIF
	ENDIF
ENDPROC

//PURPOSE:	Manages stage skipping. Loads and creates assets etc.
PROC MANAGE_SKIP(BOOL bReplaySetup =FALSE)

	bIsReplayGoingOn = bReplaySetup 

	PRINTLN("Jewel Heist MANAGE_SKIP: ", ENUM_TO_INT(eMissionStage))

	SETUP_CREW_CHOICE_CONSEQUENCES()

	//ADD_RELATIONSHIP_GROUP("CREW", RELGROUPHASH_PLAYER)		
	// +++ Group Creation +++
	ADD_RELATIONSHIP_GROUP("SHOP", grpShop)
	ADD_RELATIONSHIP_GROUP("COPPERS", grpCop)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE , RELGROUPHASH_PLAYER, grpShop)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE , RELGROUPHASH_PLAYER, grpCop)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE , grpCop, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE , grpCop, grpShop)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_NONE , grpShop, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE , grpShop, grpCop)			// --- Group Creation ---
	
	MANAGE_PLAYER_CHAR_SKIP(bReplaySetup)
	
	MANAGE_CREW_SKIP()
	
	//MANAGE EVERYTHING ELSE
	//TOGGLE_STEALTH_RADAR(FALSE)
		
	REGISTER_SCRIPT_WITH_AUDIO()
	RELEASE_NAMED_SCRIPT_AUDIO_BANK(Asset.sbRaidStore)
	RELEASE_NAMED_SCRIPT_AUDIO_BANK(Asset.sbRaidGlassSmash)
	RELEASE_NAMED_SCRIPT_AUDIO_BANK(Asset.sbRaidRayfireCrash)
	RELEASE_NAMED_SCRIPT_AUDIO_BANK(Asset.sbUnderwater)
	RELEASE_NAMED_SCRIPT_AUDIO_BANK("JWL_HEIST_CAR_SMASHES_BIG")
	
	DISTANT_COP_CAR_SIRENS(FALSE)
	
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	
	IF interiorJewelStore <> NULL
		UNPIN_INTERIOR(interiorJewelStore)
	ENDIF
	
	SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")	
		
	bAttemptToResetCases = TRUE
				
	IF eMissionStage > STAGE_GRAB_LOOT			//Need to deal with this properly.
		IF iCurrentTake = 0
			iCurrentTake = 3160280	
			iDisplayedTake = iCurrentTake
		ENDIF
	ENDIF
	
	SWITCH eMissionStage
		CASE STAGE_INIT_MISSION
			SAFE_REQUEST_INTERIOR_AT_COORDS(sweatShop, << 716.5068, -966.4983, 29.1884 >>, TRUE, FALSE)
			
			//UNPIN_INTERIOR(sweatShop)
			END_REPLAY_SETUP()
		BREAK
		CASE STAGE_CUTSCENE_BEGIN				
			RESET_NEED_LIST()
			MANAGE_LOADING_NEED_LIST(FALSE)
			
			SAFE_REQUEST_INTERIOR_AT_COORDS(sweatShop, << 716.5068, -966.4983, 29.1884 >>, TRUE, FALSE)
			END_REPLAY_SETUP()
		BREAK
		CASE STAGE_DRIVE_TO_STORE
		
			RESET_NEED_LIST()
			SET_MODEL_AS_NEEDED(Asset.mnTruck)
			SET_MODEL_AS_NEEDED(Asset.mnVan)
			MANAGE_LOADING_NEED_LIST(FALSE)
						
			SAFE_REQUEST_INTERIOR_AT_COORDS_WITH_TYPE(sweatShop, << 716.5068, -966.4983, 29.1884 >>, "V_sweat", TRUE, FALSE)
			
			WHILE NOT CREATE_NPC_PED_ON_FOOT(pedLester, CHAR_LESTER, vLesterSpawnCoords, fLesterSpawnHeading)
				WAIT(0)
			ENDWHILE
			SET_PED_CREW(pedLester)
			
			IF NOT IS_ENTITY_DEAD(pedLester)
				SET_PED_COMPONENT_VARIATION(pedLester, INT_TO_ENUM(PED_COMPONENT,0), 0, 0)  //(head)
				SET_PED_COMPONENT_VARIATION(pedLester, INT_TO_ENUM(PED_COMPONENT,2), 0, 0)  //(hair)
				SET_PED_COMPONENT_VARIATION(pedLester, INT_TO_ENUM(PED_COMPONENT,3), 1, 0)  //(uppr)
				SET_PED_COMPONENT_VARIATION(pedLester, INT_TO_ENUM(PED_COMPONENT,4), 1, 0)  //(lowr)
				SET_PED_COMPONENT_VARIATION(pedLester, INT_TO_ENUM(PED_COMPONENT,5), 0, 0)  //(hand)
				SET_PED_COMPONENT_VARIATION(pedLester, INT_TO_ENUM(PED_COMPONENT,7), 0, 0)  //(teef)
				SET_PED_COMPONENT_VARIATION(pedLester, INT_TO_ENUM(PED_COMPONENT,8), 0, 0)  //(accs)
			
				SET_PED_PROP_INDEX(pedLester, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
				SET_PED_CONFIG_FLAG(pedLester, PCF_UseKinematicModeWhenStationary, TRUE)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedLester)
				oiLestersStick = CREATE_OBJECT(asset.mnLestersStick, GET_ENTITY_COORDS(pedLester, FALSE))
				ATTACH_ENTITY_TO_ENTITY(oiLestersStick, pedLester, GET_PED_BONE_INDEX(pedLester, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
			ENDIF
							
			IF bIsStealthPath																			// +++ Vehicle Creation +++
				
			REQUEST_MODEL(PRIMO)
			WHILE NOT HAS_MODEL_LOADED(PRIMO)  // CREATE_PLAYER_VEHICLE(vehCar, CHAR_MICHAEL, vCarSpawnCoords, fCarSpawnHeading)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
				safeWait(0)
			ENDWHILE
			vehCar = CREATE_VEHICLE(PRIMO, vCarSpawnCoords, fCarSpawnHeading)
			SET_VEHICLE_COLOURS(vehCar, 0, 0)
				vehVan = CREATE_VEHICLE(Asset.mnVan, vVanSpawnCoords, fVanSpawnHeading)
			ELSE
				vehVan = CREATE_VEHICLE(Asset.mnVan, vCarSpawnCoords, fCarSpawnHeading)
			ENDIF
			vehTruck = CREATE_VEHICLE(	Asset.mnTruck, vTruckSpawnCoords, fTruckSpawnHeading)		// --- Vehicle Creation ---
			
			//[MF]Remove all randomly assigned extras from the vehicle (B* 1689820)
			REMOVE_ALL_VEHICLE_EXTRAS(vehTruck)

			SET_VEHICLE_COLOURS(vehTruck, 128, 3) SET_VEHICLE_EXTRA(vehTruck, 6, FALSE) //SET_VEHICLE_LIVERY(vehTruck, 1)
						
			SET_CLOCK_TIME(9,00,0)
			

			END_REPLAY_SETUP()
		BREAK
		CASE STAGE_ROOF
			RESET_NEED_LIST()
			MANAGE_LOADING_NEED_LIST(FALSE)
			
			vDriveToPoint = vBackOfStore
			
//			WHILE NOT CREATE_PLAYER_VEHICLE(vehCar, CHAR_MICHAEL, vDriveAIPoint, fCarSpawnHeading)
//				WAIT(0)
//			ENDWHILE
			
			REQUEST_MODEL(PRIMO)
			WHILE NOT HAS_MODEL_LOADED(PRIMO)  // CREATE_PLAYER_VEHICLE(vehCar, CHAR_MICHAEL, vCarSpawnCoords, fCarSpawnHeading)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
				safeWait(0)
			ENDWHILE
			vehCar = CREATE_VEHICLE(PRIMO, vCarSpawnCoords, fCarSpawnHeading)
			SET_VEHICLE_COLOURS(vehCar, 0, 0)
			IF IS_VEHICLE_DRIVEABLE(vehCar)
				IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
					SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehCar)
				ENDIF
				IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
					SET_PED_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehCar, VS_BACK_LEFT)
				ENDIF
				IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
					SET_PED_INTO_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehCar, VS_BACK_RIGHT)
				ENDIF
			ENDIF
			TRIGGER_MUSIC_EVENT("JH2A_MISSION_START_ST")
			ENABLE_ALL_DISPATCH_SERVICES(FALSE)
			
			#IF IS_DEBUG_BUILD
				IF NOT IS_REPLAY_IN_PROGRESS()
					g_bBirdsJHeist = GET_RANDOM_BOOL()
				ENDIF
			#ENDIF
			CLEAR_AREA(vBackOfStore, 50.0, TRUE)
			
			
			END_REPLAY_SETUP()
		BREAK
		CASE STAGE_CUTSCENE_GAS_STORE
			SAFE_REQUEST_PTFX_ASSET(TRUE, FALSE)
			//NEW_LOAD_SCENE_START(<<-628.4290, -232.2283, 37.0570>>, <<3.4724, 0.0002, 121.8659>>, 25.0)
			//safeWait(1000)
			RESET_NEED_LIST()
			MANAGE_LOADING_NEED_LIST(FALSE)
			
			vDriveToPoint = vBackOfStore
			
			IF NOT bReplaySetup			
				NEW_LOAD_SCENE_START_SPHERE(<<-623.7916, -232.2508, 38.3262>>, 35.0)
				SETTIMERA(0)
				WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
				AND TIMERA() < 10000
					WAIT(0)
				ENDWHILE
				
				IF IS_NEW_LOAD_SCENE_ACTIVE()
					NEW_LOAD_SCENE_STOP()
				ENDIF
			ENDIF
			ENABLE_ALL_DISPATCH_SERVICES(FALSE)
			IF bIsStealthPath
				TRIGGER_MUSIC_EVENT("JH2A_GAS_SHOP_RESTART")
			ENDIF
			

			END_REPLAY_SETUP()

		BREAK
		CASE STAGE_CUTSCENE_AGGRESSIVE_ARRIVE
				
			RESET_NEED_LIST()
			SET_MODEL_AS_NEEDED(Asset.mnVan)
			SET_MODEL_AS_NEEDED(Asset.mnSecurity)
			FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
				SET_MODEL_AS_NEEDED(Asset.mnShop[iCounter])
			ENDFOR
			SET_ANIM_DICT_AS_NEEDED(Asset.adJewelHeist)
			MANAGE_LOADING_NEED_LIST(FALSE)	
			SAFE_REQUEST_WEAPON_ASSET(mnMainWeaponType, TRUE, FALSE)
		
			vDriveToPoint = vFrontOfStore
			vehVan = CREATE_VEHICLE(Asset.mnVan, vDriveAIPoint, fCarSpawnHeading)
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
				IF NOT bReplaySetup
					GIVE_WEAPON_TO_PED(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, 1000, TRUE)
					GIVE_WEAPON_COMPONENT_TO_PED_IF_2A(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, WEAPONCOMPONENT_AT_AR_SUPP)
				ELSE
					SET_CURRENT_PED_WEAPON(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, TRUE)
				ENDIF
			ENDIF
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
				//GIVE_WEAPON_TO_PED(GET_SELECT_PED(SEL_FRANKLIN), mnMainWeaponType, 1000, TRUE)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehVan)
				IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
					SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehVan)
				ENDIF
				IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
					SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehVan, VS_FRONT_RIGHT)
				ENDIF
				IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
					SET_PED_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehVan, VS_BACK_LEFT)
				ENDIF
				IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
					SET_PED_INTO_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehVan, VS_BACK_RIGHT)
				ENDIF
			ENDIF
			
			REMOVE_WEAPON_ASSET(mnMainWeaponType)
			
			ENABLE_ALL_DISPATCH_SERVICES(FALSE)
			
			IF iArrivalAngle = 0
				iArrivalAngle = GET_RANDOM_INT_IN_RANGE(1, 4)
			ENDIF
			
			iStageSection = 0
			END_REPLAY_SETUP(vehVan)
			
			//LOAD_SCENE(<<-692.619751,-220.052856,36.155941>>)
			
		BREAK
		CASE STAGE_GRAB_LOOT
		
			RESET_NEED_LIST()
			SET_MODEL_AS_NEEDED(Asset.mnSecurity)
			FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
				SET_MODEL_AS_NEEDED(Asset.mnShop[iCounter])
			ENDFOR
			If NOT bIsStealthPath
				SET_MODEL_AS_NEEDED(Asset.mnBackroom)
			ENDIF
			IF bIsStealthPath 
				TRIGGER_MUSIC_EVENT("JH2A_ENTER_SHOP_RESTART")
			ELSE
				TRIGGER_MUSIC_EVENT("JH2B_ENTER_SHOP_RESTART")				
			ENDIF				
			
			SET_MODEL_AS_NEEDED(Asset.mnJewels1)
			SET_MODEL_AS_NEEDED(Asset.mnJewels2)
			SET_MODEL_AS_NEEDED(Asset.mnJewels4a)
			SET_MODEL_AS_NEEDED(Asset.mnJewels4b)
			SET_MODEL_AS_NEEDED(Asset.mnBike)
			SET_ANIM_DICT_AS_NEEDED(Asset.adJewelHeist)			
			MANAGE_LOADING_NEED_LIST(FALSE)
			
			WHILE NOT MANAGE_LOADING_NEED_LIST_SPEEDY()
				WAIT(0)
			ENDWHILE
			
			SAFE_REQUEST_PTFX_ASSET(TRUE, FALSE)
				
			SAFE_REQUEST_INTERIOR_AT_COORDS_WITH_TYPE(interiorJewelStore, << -625.1706, -232.8408, 39.4937 >>, "V_JEWEL2", TRUE, TRUE)
			
			MANAGE_PLAYER_CHAR_SKIP()
			SAFE_REQUEST_WEAPON_ASSET(mnMainWeaponType, TRUE, FALSE)
			
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
			AND NOT bReplaySetup
				GIVE_WEAPON_TO_PED(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, 1000, TRUE)
				GIVE_WEAPON_COMPONENT_TO_PED_IF_2A(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, WEAPONCOMPONENT_AT_AR_SUPP)
			ELSE
				SET_CURRENT_PED_WEAPON(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, TRUE)
			ENDIF
			
			pedSecurity = CREATE_PED(PEDTYPE_MISSION, Asset.mnSecurity, vSecuritySpawnCoords, fSecuritySpawnHeading)
			
			IF bIsStealthPath
				SAFE_SETUP_STORE_PEDS(TRUE, FALSE, TRUE, FALSE)
			ELSE
				SAFE_SETUP_STORE_PEDS(FALSE, TRUE, TRUE, FALSE)
			ENDIF
			
			REMOVE_WEAPON_ASSET(mnMainWeaponType)
			
			ENABLE_ALL_DISPATCH_SERVICES(FALSE)
			
			SET_PED_BAG(GET_SELECT_PED(SEL_MICHAEL), TRUE)
			
			SET_PED_HEADGEAR(GET_SELECT_PED(SEL_MICHAEL), TRUE)
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(GET_SELECT_PED(SEL_MICHAEL))
				safeWait(0)
			ENDWHILE

			END_REPLAY_SETUP()
			
			CLEAR_AREA(vSecuritySpawnCoords, 300.0, TRUE)
			
			IF NOT IS_ENTITY_DEAD(pedCrew[CREW_DRIVER_NEW])
				WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedCrew[CREW_DRIVER_NEW])
					safeWait(0)
				ENDWHILE		
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedCrew[CREW_GUNMAN_NEW])
				WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedCrew[CREW_GUNMAN_NEW])
					safeWait(0)
				ENDWHILE
			ENDIF
			
		BREAK
		CASE STAGE_CUTSCENE_LEAVE_STORE
			
			RESET_NEED_LIST()
			SET_MODEL_AS_NEEDED(Asset.mnBike)
			MANAGE_LOADING_NEED_LIST(FALSE)
			SAFE_REQUEST_WEAPON_ASSET(mnMainWeaponType, TRUE, FALSE)
			SAFE_REQUEST_PTFX_ASSET(TRUE, FALSE)
			CREATE_BIKES_IF_THEY_DONT_EXIST()
			
			IF IS_VEHICLE_DRIVEABLE(vehBike[0])
			AND NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
				SET_PED_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehBike[1])
			AND NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
				SET_PED_INTO_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
			ENDIF
			
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
			AND NOT bReplaySetup
				GIVE_WEAPON_TO_PED(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, 1000, TRUE)
				GIVE_WEAPON_COMPONENT_TO_PED_IF_2A(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, WEAPONCOMPONENT_AT_AR_SUPP)
			ELSE
				SET_CURRENT_PED_WEAPON(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, TRUE)
			ENDIF
			
			
			SET_PED_HEADGEAR(GET_SELECT_PED(SEL_MICHAEL), TRUE)
			
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
				//GIVE_WEAPON_TO_PED(GET_SELECT_PED(SEL_FRANKLIN), mnMainWeaponType, 1000, FALSE)
			ENDIF
			
			//Make the cop cars show up randomly if debugging.
			#IF IS_DEBUG_BUILD
				IF NOT IS_REPLAY_IN_PROGRESS()
					g_bLateLeaveJewelStore = TRUE
				ENDIF
			#ENDIF
			
			REMOVE_WEAPON_ASSET(mnMainWeaponType)
			
			ENABLE_ALL_DISPATCH_SERVICES(FALSE)
			
			IF bIsStealthPath 
				TRIGGER_MUSIC_EVENT("JH2A_EXIT_SHOP_RESTART")
			ELSE
				TRIGGER_MUSIC_EVENT("JH2A_EXIT_SHOP_RESTART")				
			ENDIF	

			END_REPLAY_SETUP()
		BREAK

		CASE STAGE_BIKE_CHASE
			RESET_NEED_LIST()
			SAFE_REQUEST_PTFX_ASSET(TRUE, FALSE)
			SET_MODEL_AS_NEEDED(Asset.mnBike)
			MANAGE_LOADING_NEED_LIST(FALSE)
			
			CREATE_BIKES_IF_THEY_DONT_EXIST()
			
//			IF IS_VEHICLE_DRIVEABLE(vehBike[2])
//				SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
//			ENDIF
			
			
			//Make the cop cars show up randomly if debugging.
			#IF IS_DEBUG_BUILD
				IF NOT IS_REPLAY_IN_PROGRESS()
					g_bLateLeaveJewelStore = TRUE
				ENDIF
			#ENDIF
			
			ENABLE_ALL_DISPATCH_SERVICES(FALSE)
			SET_PED_BAG(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
			IF bIsStealthPath
				TRIGGER_MUSIC_EVENT("JH2A_ONTO_BIKE_RESTART")
			ELSE
				TRIGGER_MUSIC_EVENT("JH2B_ONTO_BIKE_RESTART")
			ENDIF

			END_REPLAY_SETUP(vehBike[2])
			
						
			IF g_bLateLeaveJewelStore
				REQUEST_MODEL(Asset.mnPolice)
				REQUEST_MODEL(Asset.mnPoliceman)				
				WHILE NOT HAS_MODEL_LOADED(Asset.mnPolice)
				OR NOT HAS_MODEL_LOADED(Asset.mnPoliceman)	
					WAIT(0)
				ENDWHILE
			ELSE
				REQUEST_MODEL(INTRUDER)
				WHILE NOT HAS_MODEL_LOADED(INTRUDER)
					WAIT(0)
				ENDWHILE
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehBike[0])
				IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
					SET_PED_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
				ENDIF
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehBike[1])
				IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
					SET_PED_INTO_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedCrew[CREW_DRIVER_NEW])
				SET_PED_HELMET_PROP_INDEX(pedCrew[CREW_DRIVER_NEW], 3)
				SET_PED_HELMET_TEXTURE_INDEX(pedCrew[CREW_DRIVER_NEW], 0)
				SET_PED_PROP_INDEX(pedCrew[CREW_DRIVER_NEW], INT_TO_ENUM(PED_PROP_POSITION,0), 3, 0)
				GIVE_PED_HELMET(pedCrew[CREW_DRIVER_NEW], TRUE, PV_FLAG_SCRIPT_HELMET)
			ENDIF
			IF NOT IS_ENTITY_DEAD(pedCrew[CREW_GUNMAN_NEW])
				SET_PED_HELMET_PROP_INDEX(pedCrew[CREW_GUNMAN_NEW], 4)
				SET_PED_HELMET_TEXTURE_INDEX(pedCrew[CREW_GUNMAN_NEW], 0)
				SET_PED_PROP_INDEX(pedCrew[CREW_GUNMAN_NEW], INT_TO_ENUM(PED_PROP_POSITION,0), 4, 0)	
				GIVE_PED_HELMET(pedCrew[CREW_GUNMAN_NEW], TRUE, PV_FLAG_SCRIPT_HELMET)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_FRANKLIN))
				SET_PED_HELMET_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), 6)
				SET_PED_HELMET_TEXTURE_INDEX(GET_SELECT_PED(SEL_FRANKLIN), 8)
				SET_PED_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_PROP_POSITION,0), 6, 8)
			ENDIF
			
			GIVE_PED_HELMET(GET_SELECT_PED(SEL_FRANKLIN), TRUE, PV_FLAG_SCRIPT_HELMET)
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedCrew[CREW_DRIVER_NEW])
				safeWait(0)
			ENDWHILE
			
			IF NOT bWorstGunDies
				WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedCrew[CREW_GUNMAN_NEW])
					safeWait(0)
				ENDWHILE
			ENDIF
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(GET_SELECT_PED(SEL_FRANKLIN))
				safeWait(0)
			ENDWHILE
			FOR iCounter = 0 TO iNoOfCases-1
				rfReset = GET_RAYFIRE_MAP_OBJECT(	jcStore[iCounter].vCasePos, fRFCheckRadius,
													jcStore[iCounter].sSmashAnim)
				IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfReset)					
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfReset, RFMO_STATE_ENDING)					
				ENDIF
			ENDFOR			
			
		BREAK
		
		CASE STAGE_BIKE_CHASE_START_OF_TUNNEL
			RESET_NEED_LIST()
			SET_MODEL_AS_NEEDED(Asset.mnBike)
			MANAGE_LOADING_NEED_LIST(FALSE)
			//MANAGE_LOADING_NEED_LIST_SPEEDY()
			SET_CLOCK_TIME(19,20,0)
			
			CREATE_BIKES_IF_THEY_DONT_EXIST(1)
			
//			IF IS_VEHICLE_DRIVEABLE(vehBike[2])
//				SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
//			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehBike[0])
				IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
					SET_PED_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
				ENDIF
			ENDIF
			
			IF NOT bWorstGunDies
			
				IF IS_VEHICLE_DRIVEABLE(vehBike[1])
					IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
						SET_PED_INTO_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
					ENDIF
				ENDIF
			ELSE
				DELETE_VEHICLE(vehBike[1])
				DELETE_PED(pedCrew[CREW_GUNMAN_NEW])
			ENDIF
		
			IF bIsStealthPath
				TRIGGER_MUSIC_EVENT("JH2A_EXIT_TUNNEL_RESTART")
			ELSE
				TRIGGER_MUSIC_EVENT("JH2B_EXIT_TUNNEL_RESTART")
			ENDIF
			//XXX : SET_POLICE_RADAR_BLIPS(FALSE)
			ENABLE_ALL_DISPATCH_SERVICES(FALSE)
			SET_PED_BAG(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(GET_REPLAY_STAGE_FROM_MISSION_STAGE(STAGE_BIKE_CHASE_START_OF_TUNNEL), "STAGE_BIKE_CHASE_START_OF_TUNNEL")
			//LOAD_SCENE(<<-63.1055, -541.1434, 30.8741>>)
			

			END_REPLAY_SETUP(vehBike[2])			
			
			IF NOT IS_ENTITY_DEAD(pedCrew[CREW_DRIVER_NEW])
				SET_PED_HELMET_PROP_INDEX(pedCrew[CREW_DRIVER_NEW], 3)
				SET_PED_HELMET_TEXTURE_INDEX(pedCrew[CREW_DRIVER_NEW], 0)
				SET_PED_PROP_INDEX(pedCrew[CREW_DRIVER_NEW], INT_TO_ENUM(PED_PROP_POSITION,0), 3, 0)
				GIVE_PED_HELMET(pedCrew[CREW_DRIVER_NEW], TRUE, PV_FLAG_SCRIPT_HELMET)
			ENDIF
			IF NOT IS_ENTITY_DEAD(pedCrew[CREW_GUNMAN_NEW])
				SET_PED_HELMET_PROP_INDEX(pedCrew[CREW_GUNMAN_NEW], 4)
				SET_PED_HELMET_TEXTURE_INDEX(pedCrew[CREW_GUNMAN_NEW], 0)
				SET_PED_PROP_INDEX(pedCrew[CREW_GUNMAN_NEW], INT_TO_ENUM(PED_PROP_POSITION,0), 4, 0)	
				GIVE_PED_HELMET(pedCrew[CREW_GUNMAN_NEW], TRUE, PV_FLAG_SCRIPT_HELMET)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_FRANKLIN))
				SET_PED_HELMET_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), 6)
				SET_PED_HELMET_TEXTURE_INDEX(GET_SELECT_PED(SEL_FRANKLIN), 8)
				SET_PED_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_PROP_POSITION,0), 6, 8)
			ENDIF
			
			GIVE_PED_HELMET(GET_SELECT_PED(SEL_FRANKLIN), TRUE, PV_FLAG_DEFAULT_HELMET, -1)
			
			INSTANTLY_FILL_PED_POPULATION()
			POPULATE_NOW()
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedCrew[CREW_DRIVER_NEW])
				safeWait(0)
			ENDWHILE
			
			IF NOT bWorstGunDies
				WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedCrew[CREW_GUNMAN_NEW])
					safeWait(0)
				ENDWHILE
			ENDIF
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(GET_SELECT_PED(SEL_FRANKLIN))
				safeWait(0)
			ENDWHILE
		BREAK
		
		CASE STAGE_BIKE_CHASE_END_OF_TUNNEL
		
			RESET_NEED_LIST()
		
			SET_MODEL_AS_NEEDED(Asset.mnBike)
			SET_MODEL_AS_NEEDED(Asset.mnTruck)
			SET_MODEL_AS_NEEDED(Asset.mnPolice)
			SET_MODEL_AS_NEEDED(Asset.mnPoliceman)
			SET_VEHICLE_RECORDING_AS_NEEDED(Asset.recUber, Asset.recNoGoodBikeChase)
			SET_VEHICLE_RECORDING_AS_NEEDED(Asset.recUber, Asset.recNumber)
		
			SET_VEHICLE_RECORDING_AS_NEEDED("JHUBER", 717)
					
			MANAGE_LOADING_NEED_LIST(FALSE)
			
			SAFE_REQUEST_MODEL_FOR_HEIST_CREW_MEMBER(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW), TRUE, TRUE)
			SAFE_REQUEST_MODEL_FOR_HEIST_CREW_MEMBER(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER), TRUE, TRUE)
			SAFE_REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL, TRUE, TRUE)
			
			
			//MANAGE_LOADING_NEED_LIST_SPEEDY()
			SET_CLOCK_TIME(19,20,0)
			
			vehBike[0] = CREATE_VEHICLE(Asset.mnBike, <<1035.8641, -284.8706, 49.2269>>,  208.3043)
			vehBike[1] = CREATE_VEHICLE(Asset.mnBike, <<1037.8641, -284.8706, 49.2269>>,  208.3043)
			vehBike[2] = CREATE_VEHICLE(Asset.mnBike, <<1033.8641, -284.8706, 49.2269>>,  208.3043)
			
			FOR iCounter = 0 TO 2
				IF NOT DOES_ENTITY_EXIST(vehBike[iCounter])
					SET_VEHICLE_ON_GROUND_PROPERLY(vehBike[iCounter])
					SET_VEHICLE_COLOUR_COMBINATION(vehBike[iCounter], iCounter)
				ENDIF
			ENDFOR
			
			
			//XXX : SET_POLICE_RADAR_BLIPS(FALSE)
						
			IF bIsStealthPath
				TRIGGER_MUSIC_EVENT("JH2A_EXIT_TUNNEL_RESTART")
			ELSE
				TRIGGER_MUSIC_EVENT("JH2B_EXIT_TUNNEL_RESTART")
			ENDIF
			ENABLE_ALL_DISPATCH_SERVICES(FALSE)
			SET_PED_BAG(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
			

			END_REPLAY_SETUP()
			
			IF IS_VEHICLE_DRIVEABLE(vehBike[2])
				SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehBike[0])
				IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
					SET_PED_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
				ENDIF
			ENDIF
			
			IF NOT bWorstGunDies
			
				IF IS_VEHICLE_DRIVEABLE(vehBike[1])
					IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
						SET_PED_INTO_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
					ENDIF
				ENDIF
			ELSE
				DELETE_VEHICLE(vehBike[1])
				DELETE_PED(pedCrew[CREW_GUNMAN_NEW])
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedCrew[CREW_DRIVER_NEW])
				SET_PED_HELMET_PROP_INDEX(pedCrew[CREW_DRIVER_NEW], 3)
				SET_PED_HELMET_TEXTURE_INDEX(pedCrew[CREW_DRIVER_NEW], 0)
				SET_PED_PROP_INDEX(pedCrew[CREW_DRIVER_NEW], INT_TO_ENUM(PED_PROP_POSITION,0), 3, 0)
				GIVE_PED_HELMET(pedCrew[CREW_DRIVER_NEW], TRUE, PV_FLAG_SCRIPT_HELMET)
			ENDIF
			IF NOT IS_ENTITY_DEAD(pedCrew[CREW_GUNMAN_NEW])
				SET_PED_HELMET_PROP_INDEX(pedCrew[CREW_GUNMAN_NEW], 4)
				SET_PED_HELMET_TEXTURE_INDEX(pedCrew[CREW_GUNMAN_NEW], 0)
				SET_PED_PROP_INDEX(pedCrew[CREW_GUNMAN_NEW], INT_TO_ENUM(PED_PROP_POSITION,0), 4, 0)	
				GIVE_PED_HELMET(pedCrew[CREW_GUNMAN_NEW], TRUE, PV_FLAG_SCRIPT_HELMET)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_FRANKLIN))
				SET_PED_HELMET_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), 6)
				SET_PED_HELMET_TEXTURE_INDEX(GET_SELECT_PED(SEL_FRANKLIN), 8)
				SET_PED_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_PROP_POSITION,0), 6, 8)
			ENDIF
			
			GIVE_PED_HELMET(GET_SELECT_PED(SEL_FRANKLIN), TRUE, PV_FLAG_DEFAULT_HELMET, -1)
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedCrew[CREW_DRIVER_NEW])
				safeWait(0)
			ENDWHILE
			
			IF NOT bWorstGunDies
				WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedCrew[CREW_GUNMAN_NEW])
					safeWait(0)
				ENDWHILE
			ENDIF
			
			WHILE NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(GET_SELECT_PED(SEL_FRANKLIN))
				safeWait(0)
			ENDWHILE
			
		BREAK
		
		CASE STAGE_TRUCK_FIGHT
			RESET_NEED_LIST()
			MANAGE_LOADING_NEED_LIST(FALSE)
			
			REQUEST_TRUCK_FIGHT_ASSETS()
			
			WHILE NOT HAVE_TRUCK_FIGHT_ASSETS_LOADED()
				WAIT(0)
			ENDWHILE
						
			SAFE_REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL, TRUE, FALSE)
			
			vehTruck = CREATE_VEHICLE(Asset.mnTruck, vVehicleFightTruckSpawn, fVehicleFightTruckSpawn)
			//[MF]Remove all randomly assigned extras from the vehicle (B* 1689820)
			REMOVE_ALL_VEHICLE_EXTRAS(vehTruck)			

			SET_VEHICLE_COLOURS(vehTruck, 128, 3) SET_VEHICLE_EXTRA(vehTruck, 6, FALSE) //SET_VEHICLE_LIVERY(vehTruck, 1)
			
			FOR iCounter = 0 TO 2
				IF NOT (bWorstGunDies AND iCounter = 1)
					vehBike[iCounter] = CREATE_VEHICLE(	Asset.mnBike, vVehicleFightBikeSpawn[iCounter], 
															fVehicleFightBikeSpawn[iCounter])
					SET_VEHICLE_ON_GROUND_PROPERLY(vehBike[iCounter])
					SET_VEHICLE_COLOUR_COMBINATION(vehBike[iCounter], iCounter)
				ENDIF
			ENDFOR
			
			IF IS_VEHICLE_DRIVEABLE(vehBike[2])
			AND NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
				IF NOT IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
					SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
				ENDIF
				SET_VEHICLE_ENGINE_ON(vehBike[2], TRUE, TRUE)
				SET_VEHICLE_FORWARD_SPEED(vehBike[2], 8.0)
				
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehTruck)
			AND NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
				IF NOT IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehTruck)
					SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehTruck)
				ENDIF
			ENDIF
			
			IF NOT bWorstGunDies
				IF IS_VEHICLE_DRIVEABLE(vehBike[1])
				AND NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
					IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
						SET_PED_INTO_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
					ENDIF
					SET_VEHICLE_ENGINE_ON(vehBike[1], TRUE, TRUE)	
					SET_VEHICLE_FORWARD_SPEED(vehBike[1], 8.0)
					
				ENDIF
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehBike[0])
			AND NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
				IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
					SET_PED_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
				ENDIF
				SET_VEHICLE_ENGINE_ON(vehBike[0], TRUE, TRUE)
				SET_VEHICLE_FORWARD_SPEED(vehBike[0], 8.0)
				
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehTruck)
			AND NOT IS_PED_INJURED(pedCrew[CREW_HACKER])
				IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_HACKER], vehTruck)
					SET_PED_INTO_VEHICLE(pedCrew[CREW_HACKER], vehTruck, VS_FRONT_RIGHT)
				ENDIF
			ENDIF
			
			SET_CLOCK_TIME(19,00,0)
			
			IF bIsStealthPath
				TRIGGER_MUSIC_EVENT("JH2A_EXIT_TUNNEL_RESTART")
			ELSE
				TRIGGER_MUSIC_EVENT("JH2B_EXIT_TUNNEL_RESTART")
			ENDIF
			
			REMOVE_WEAPON_ASSET(WEAPONTYPE_PISTOL)
			
			ENABLE_ALL_DISPATCH_SERVICES(FALSE)
			

			END_REPLAY_SETUP(vehTruck)
			
		BREAK
		CASE STAGE_CUTSCENE_RAMP_DOWN
			RESET_NEED_LIST()
			SET_MODEL_AS_NEEDED(Asset.mnTruck)
			SET_MODEL_AS_NEEDED(Asset.mnBike)
			MANAGE_LOADING_NEED_LIST(FALSE)
	
//			vFightEndTarget = vFightEndCoord[7]
//			fFightEndTarget = fFightEndHeading[7]
			
						
			DELETE_VEHICLE(vehTruck)
			
			vehTruck = CREATE_VEHICLE(Asset.mnTruck, vFightEndTarget, fFightEndTarget)
			//[MF]Remove all randomly assigned extras from the vehicle (B* 1689820)
			REMOVE_ALL_VEHICLE_EXTRAS(vehTruck)

			SET_VEHICLE_COLOURS(vehTruck, 128, 3) SET_VEHICLE_EXTRA(vehTruck, 6, FALSE) //SET_VEHICLE_LIVERY(vehTruck, 1)

			FOR iCounter = 0 TO 2
			
				DELETE_VEHICLE(vehBike[iCounter])
			
				IF NOT (bWorstGunDies AND iCounter = 1)
					vehBike[iCounter] = CREATE_VEHICLE(	Asset.mnBike, vVehicleFightBikeSpawn[iCounter], 
															fVehicleFightBikeSpawn[iCounter])
					SET_VEHICLE_ON_GROUND_PROPERLY(vehBike[iCounter])
					SET_VEHICLE_COLOUR_COMBINATION(vehBike[iCounter], iCounter)
				ENDIF
			ENDFOR
			
			IF IS_VEHICLE_DRIVEABLE(vehBike[2])
			AND NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
				SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehTruck)
			AND NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
				SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehTruck)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehBike[0])
			AND NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
				SET_PED_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
			ENDIF
			IF NOT bWorstGunDies
				IF IS_VEHICLE_DRIVEABLE(vehBike[1])
				AND NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
					SET_PED_INTO_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
				ENDIF
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehTruck)
			AND NOT IS_PED_INJURED(pedCrew[CREW_HACKER])
				SET_PED_INTO_VEHICLE(pedCrew[CREW_HACKER], vehTruck, VS_FRONT_RIGHT)
			ENDIF
			
			SET_CLOCK_TIME(22, 00, 00)
			
			ENABLE_ALL_DISPATCH_SERVICES(FALSE)

			END_REPLAY_SETUP(vehTruck)
			
		BREAK
		CASE STAGE_RETURN_TO_BASE
			RESET_NEED_LIST()
			SET_MODEL_AS_NEEDED(Asset.mnTruck)
			MANAGE_LOADING_NEED_LIST(FALSE)
		
			vehTruck = CREATE_VEHICLE(	Asset.mnTruck, vFightEndTarget, fFightEndTarget)
			//[MF]Remove all randomly assigned extras from the vehicle (B* 1689820)
			REMOVE_ALL_VEHICLE_EXTRAS(vehTruck)

			SET_VEHICLE_COLOURS(vehTruck, 128, 3) SET_VEHICLE_EXTRA(vehTruck, 6, FALSE) //SET_VEHICLE_LIVERY(vehTruck, 1)
		
			IF IS_VEHICLE_DRIVEABLE(vehTruck)
			AND NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
				SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehTruck)
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehTruck)
			AND NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_FRANKLIN))
				ATTACH_ENTITY_TO_ENTITY(GET_SELECT_PED(SEL_FRANKLIN), vehTruck, 0, <<-0.09, -1.63, 0.91>>, <<0,0,-165.24>>)
			ENDIF
			
			IF IS_VEHICLE_DRIVEABLE(vehTruck)
			AND NOT IS_ENTITY_DEAD(pedCrew[CREW_DRIVER_NEW])
				ATTACH_ENTITY_TO_ENTITY(pedCrew[CREW_DRIVER_NEW], vehTruck, 0, <<-0.67, -2.81, 0.91>>, <<0,0,-147.96>>)
			ENDIF
			IF NOT bWorstGunDies
				IF IS_VEHICLE_DRIVEABLE(vehTruck)
				AND NOT IS_ENTITY_DEAD(pedCrew[CREW_GUNMAN_NEW])
					ATTACH_ENTITY_TO_ENTITY(pedCrew[CREW_GUNMAN_NEW], vehTruck, 0, <<0.728, -2.928, 0.91>>, <<0,0,83.52>>)
				ENDIF
			ENDIF
			IF IS_VEHICLE_DRIVEABLE(vehTruck)
			AND NOT IS_PED_INJURED(pedCrew[CREW_HACKER])
				SET_PED_INTO_VEHICLE(pedCrew[CREW_HACKER], vehTruck, VS_FRONT_RIGHT)
			ENDIF
			
			SET_CLOCK_TIME(22, 00, 00)
			
			END_REPLAY_SETUP(vehTruck)
		BREAK
		
		CASE PASSED_MISSION
			MissionPassed()
		BREAK
	ENDSWITCH
	
//	#IF IS_DEBUG_BUILD
//		CREATE_WIDGETS()
//	#ENDIF
	
	SETUP_VAN()
	
	ADD_ALL_PEDS_TO_DIALOGUE()
	
	SETUP_CREW_SUITS()
		
	//SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	//SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
	
	PRINTLN("Jewel Heist MANAGE_SKIP SUCCESS!!!: ", ENUM_TO_INT(eMissionStage))
	
ENDPROC


//═════════════════════════════════╡ NON-Stage FUNCTIONS AND PROCEDURES ╞═══════════════════════════════════

FUNC BOOL IS_PLAYER_IN_FRONT_OF_VEHICLE(VEHICLE_INDEX thisVehicle)
	
	VECTOR a, b, c
	
	IF NOT IS_ENTITY_DEAD(thisVehicle)
		a = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisVehicle, <<-1.0, 6.0, 0.0>>)
		b = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(thisVehicle, <<1.0, 6.0, 0.0>>)
		c = GET_ENTITY_COORDS(PLAYER_PED_ID())
	ENDIF
	
	return ((b.x - a.x)*(c.y - a.y) - (b.y - a.y)*(c.x - a.x)) > 0
	
ENDFUNC

PROC CONTROL_PLAYBACK_SPEED_NEW(VEHICLE_INDEX viPlayersCar, VEHICLE_INDEX vehicleForPlaybackSpeed, FLOAT &ChaseSpeed)

//	VEHICLE_INDEX vehicleForPlaybackSpeed
//	vehicleForPlaybackSpeed = vehBike[0]
	
	vTargetVehicleOffsetChase = <<0.0, 0.0, 0.0>>

	//SWITCH eChaseStage 
			
//	fminPlaybackSpeedChase = 0.5
//	fMaxPlaybackSpeedChase = 1.75
	vTargetVehicleOffsetChase.y  = -20.0
	
	IF IS_VEHICLE_DRIVEABLE(vehBike[0])
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0])
			fRecordingTime = GET_TIME_POSITION_IN_RECORDING(vehBike[0])
		ENDIF
	ENDIF
	
	IF fRecordingTime > Asset.skipBikeChaseEnd					//Set custom distancing values for each section of the chase
		IF bWorstGunDies
			vTargetVehicleOffsetChase.y = 45.0
		ELSE
			vTargetVehicleOffsetChase.y = 35.0
		ENDIF
	ELIF fRecordingTime > Asset.skipTunnelEnter
		IF bWorstGunDies
			vTargetVehicleOffsetChase.y = 34.5
		ELSE
			vTargetVehicleOffsetChase.y = 24.5
		ENDIF
	ELIF fRecordingTime > Asset.skipJumpLand
		vTargetVehicleOffsetChase.y = 26.5
	ELIF fRecordingTime > Asset.skipJumpCrash
		vTargetVehicleOffsetChase.y = 22.0
	ELIF fRecordingTime > Asset.skipJump
		vTargetVehicleOffsetChase.y = 25.0
	ELIF fRecordingTime > Asset.skipJumpTurnEnd
		vTargetVehicleOffsetChase.y = 22.0
	ELIF fRecordingTime > Asset.skipJumpTurnStart
		vTargetVehicleOffsetChase.y = 22.5
	ELIF fRecordingTime > Asset.skipSlowdownForJumpTurn
		vTargetVehicleOffsetChase.y = 20.0
	ELIF fRecordingTime > Asset.skipThirdCrossroadEnd
		vTargetVehicleOffsetChase.y = 24.5
	ELIF fRecordingTime > Asset.skipThirdCrossroadStart
		vTargetVehicleOffsetChase.y = 20.0
	ELIF fRecordingTime > Asset.skipSecondCrossroadEnd
		vTargetVehicleOffsetChase.y = 21.0
	ELIF fRecordingTime > Asset.skipSecondCrossroadStart
		vTargetVehicleOffsetChase.y = 25.0
	ELIF fRecordingTime > Asset.skipBigTurnEnd
		vTargetVehicleOffsetChase.y = 25.0
	ELIF fRecordingTime > Asset.skipBigTurnMid
		vTargetVehicleOffsetChase.y = 25.0
	ELIF fRecordingTime > Asset.skipBigTurnStart
		vTargetVehicleOffsetChase.y = 30.0
	ELIF fRecordingTime > Asset.skipFirstCrossroadEnd
		vTargetVehicleOffsetChase.y = 30.0
	ELIF fRecordingTime > Asset.skipBikeChaseStart
		vTargetVehicleOffsetChase.y = 22.0
	ELSE
		vTargetVehicleOffsetChase.y = 20.0
	ENDIF
	
	IF DOES_ENTITY_EXIST(oiBuddysBag)
		CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(ChaseSpeed, viPlayersCar, vehicleForPlaybackSpeed, 5.0, 10.0, 50.0, 100.0, 30.0, 1.0, 0.4, 0.4, 2.0, TRUE, (fDefaultTopSpeed * 3), 30, TRUE)
	ELSE
		IF GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW))) = CMSK_BAD
		AND bWorstGunDies
		AND fRecordingTime > 55000.0
		AND fRecordingTime < 60000.0
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(ChaseSpeed, viPlayersCar, vehicleForPlaybackSpeed, 5.0, 10.0, 50.0, 100.0, 30.0, 1.0, 0.65, 0.5, 2.0, TRUE, (fDefaultTopSpeed * 3), 30, TRUE)
		ELSE
			CALCULATE_DESIRED_PLAYBACK_SPEED_FROM_TRIGGER_CAR(ChaseSpeed, viPlayersCar, vehicleForPlaybackSpeed, (vTargetVehicleOffsetChase.y * 0.85), vTargetVehicleOffsetChase.y, 50.0, 100.0, 30.0, 1.0, 0.75, 0.5, 2.0, TRUE, fDefaultTopSpeed, fDefaultTopSpeed * 2, TRUE)
		ENDIF	
	ENDIF
	
	IF bEventFlag[4]	//Only if the bike has reached the interior (shite flag - make better)
		MODIFY_VEHICLE_TOP_SPEED(vehBike[2], fDefaultTopSpeed * -1)
	ENDIF
	
ENDPROC

	
//PURPOSE:	Swagged from Matt- forces the chase recordings to be a loading priority
PROC PRELOAD_ALL_CHASE_RECORDINGS(FLOAT f_playback_time)
	//This proc is quite processor intensive, don't do every frame.
	IF GET_GAME_TIMER() - iPreloadRecordingsTimer > 200
		INT i = 0
		INT i_setpiece_size = COUNT_OF(SetPieceCarID)
		INT i_traffic_size = COUNT_OF(TrafficCarID)

		REPEAT i_setpiece_size i
			IF SetPieceCarRecording[i] > 0
				IF SetPieceCarStartime[i] > f_playback_time 
				    REQUEST_VEHICLE_RECORDING(SetPieceCarRecording[i], Asset.recUber)
				ENDIF
			ENDIF
		ENDREPEAT

		REPEAT i_traffic_size i 
			IF TrafficCarRecording[i] > 0
				IF TrafficCarStartime[i] > f_playback_time
					REQUEST_VEHICLE_RECORDING(TrafficCarRecording[i], Asset.recUber)
				ENDIF
			ENDIF
		ENDREPEAT

		iPreloadRecordingsTimer = GET_GAME_TIMER()
	ENDIF
ENDPROC


BLIP_INDEX	blipBuddysBag 

BOOL bSAHelpPrinted = FALSE

//PURPOSE:	Checks trigger areas to control dialogue
PROC DO_CHASE_DIALOGUE()

	IF NOT bHasMichaelBeenDeletedForChase
		IF NOT DOES_ENTITY_EXIST(GET_SELECT_PED(SEL_MICHAEL))
			ADD_ALL_PEDS_TO_DIALOGUE()
			bHasMichaelBeenDeletedForChase = TRUE
		ENDIF
	ENDIF

	//2nd left turn in chase
	IF NOT bEventFlag[0]
		IF IS_ENTITY_AT_COORD(vehBike[0], <<-777.482300,-259.395691,45.095806>>,<<11.062500,14.562500,9.000000>>)
			//runConversation(Asset.convChaseHardLeft, TRUE, TRUE)
			//addTextToQueue(Asset.convChaseHardLeft, TEXT_TYPE_CONVO)
			PLAY_DRIVER_CONVO("JH_SHARPLE", "JH_SHARPLT", "JH_LOSTF")//"JH_SHARPLK")
			
						
			IF NOT g_bLateLeaveJewelStore
				//addTextToQueue(Asset.convPolice1, TEXT_TYPE_CONVO)//Police:	Jewellery store robbery on Little Portola Drive. Suspects leaving on dirt bikes. All units.
				IF IS_DRIVER_KARIM()
					addTextToQueue(Asset.convFranklinEncouragementOpen, TEXT_TYPE_CONVO)
				ELSE
					addTextToQueue("JH_FRENCO", TEXT_TYPE_CONVO)					
				ENDIF
			//ELSE
				//PLAY_DRIVER_CONVO("JH_KEEPE", "JH_SHARPLT", "")//"JH_SHARPLK")
			ENDIF
			bEventFlag[0] = TRUE
		ENDIF
	ENDIF
	
	
	IF NOT bEventFlag[1]
		IF IS_ENTITY_IN_ANGLED_AREA(vehBike[0], <<-295.637177,-425.589539,28.487122>>, <<-291.265839,-364.647675,33.500710>>, 14.250000)
			//runConversation("JH_BEARRGHT", TRUE, TRUE)
		
			ADD_DRIVER_TEXT_TO_QUEUE("JH_BRIDE", "JH_BRIDT", "JH_LOSTF")//"JH_BRIDK")
									
			bEventFlag[1] = TRUE
						
			IF NOT IS_AUDIO_SCENE_ACTIVE("JSH_2B_CHASE_POLICE_AT_BRIDGE")
				START_AUDIO_SCENE("JSH_2B_CHASE_POLICE_AT_BRIDGE")
			ENDIF
		ENDIF
	ENDIF
	
	IF NOT bEventFlag[2]
		IF IS_ENTITY_AT_COORD(vehBike[0], <<-176.029526,-486.393555,42.424618>>,<<46.000000,40.000000,9.000000>>)
			//runConversation(Asset.convChaseJump, TRUE, TRUE)	
			//addTextToQueue("convChaseJump", TEXT_TYPE_CONVO)
					
			bEventFlag[2] = TRUE
		ENDIF
	ENDIF
	
	IF IS_ENTITY_AT_COORD(vehBike[0], <<-176.029526,-486.393555,42.424618>>,<<5.000000,5.000000,9.000000>>)
		IF NOT IS_AUDIO_SCENE_ACTIVE("JSH_2B_CHASE_JUMP_FREEWAY")
			START_AUDIO_SCENE("JSH_2B_CHASE_JUMP_FREEWAY")
		ENDIF
	ENDIF
	
	IF NOT bEventFlag[3]
		//IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-184.455109,-480.010376,38.679504>>, <<-164.158157,-497.522980,26.490414>>, 15.250000)
		IF bEventFlag[2] = TRUE
			IF NOT bWorstGunDies
				//addTextToQueue("JH_JUMP", TEXT_TYPE_CONVO)
				ADD_DRIVER_TEXT_TO_QUEUE("JH_OFFE", "JH_OFFT", "JH_OFFK")
			ENDIF
			
			
			IF IS_GUNMAN_NORM()
				addTextToQueue("JH_JUMP_NR", TEXT_TYPE_CONVO)
				addTextToQueue("JH_LSCN_NR", TEXT_TYPE_CONVO)
			ELIF IS_GUNMAN_GUSTAVO() 
				addTextToQueue("JH_JUMP_GM", TEXT_TYPE_CONVO)
			ELIF IS_GUNMAN_PACKIE()
				addTextToQueue("JH_JUMP_PM", TEXT_TYPE_CONVO)				
			ENDIF
						
			ADD_DRIVER_TEXT_TO_QUEUE("JH_RIGHTE", "JH_RIGHTT", "JH_LOSTF")//"JH_RIGHTK")
			
			
			bEventFlag[3] = TRUE
		ENDIF
	ENDIF
	
	//Start of the tunnel entrance
	IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-60.5417, -543.5551, 30.8804>>, <<8.0, 8.0, 8.0>>)
		
		IF bWorstGunDies
			SET_PED_AS_NO_LONGER_NEEDED(pedCrew[CREW_GUNMAN_NEW])
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBike[1])
		ENDIF
		
		IF IS_AUDIO_SCENE_ACTIVE("JSH_2B_CHASE_POLICE_AT_BRIDGE")
			STOP_AUDIO_SCENE("JSH_2B_CHASE_POLICE_AT_BRIDGE")
		ENDIF
		IF IS_AUDIO_SCENE_ACTIVE("JSH_2B_CHASE_JUMP_FREEWAY")
			STOP_AUDIO_SCENE("JSH_2B_CHASE_JUMP_FREEWAY")
		ENDIF
		
		IF NOT IS_AUDIO_SCENE_ACTIVE("JSH_2B_CHASE_ENTER_TUNNELS")
			PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_JSH_2A_04", 0.0)
			START_AUDIO_SCENE("JSH_2B_CHASE_ENTER_TUNNELS")
		ENDIF
		
	ENDIF
	
	IF NOT bEventFlag[4]
	
		
	
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -65.7330, -539.4092, 30.7915 >>, <<42.0, 42.0, 30.0>>)
			//runConversation(Asset.convChaseTunnelEntry, TRUE, TRUE)
					
			SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 6.5)
			
			SET_VEHICLE_LIGHTS(vehBike[0], FORCE_VEHICLE_LIGHTS_ON)
			IF NOT bWorstGunDies
				SET_VEHICLE_LIGHTS(vehBike[1], FORCE_VEHICLE_LIGHTS_ON)
			ENDIF
			SET_VEHICLE_LIGHTS(vehBike[2], SET_VEHICLE_LIGHTS_ON)
			
			SET_VEHICLE_FULLBEAM(vehBike[0], TRUE)
			IF NOT bWorstGunDies
				SET_VEHICLE_FULLBEAM(vehBike[1], TRUE)
			ENDIF
			SET_VEHICLE_FULLBEAM(vehBike[2], TRUE)
			
			//runConversation("JH_LIGHTSON", TRUE, TRUE)

			//Franklin says into the tunnel
			//addTextToQueue(Asset.convChaseTunnelEntry, TEXT_TYPE_CONVO)
			DISTANT_COP_CAR_SIRENS(FALSE)
			RELEASE_NAMED_SCRIPT_AUDIO_BANK(Asset.sbRaidStore)
			bEventFlag[4] = TRUE
			bEventFlag[1] = TRUE
		ENDIF
	ENDIF
	
	//They go off the jump
	IF NOT bEventFlag[5]
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-32.7017, -578.5150, 27.3165>>, <<5.0, 5.0, 5.0>>)
			//ADD_DRIVER_TEXT_TO_QUEUE("JH_JUMPE", "JH_JUMPT", "JH_JUMPK")
			bEventFlag[5] = TRUE
		ENDIF
	ENDIF
	
	//Get to the first tunnel section
	IF NOT bEventFlag[6]
		IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<20.5861, -647.6309, 15.0881>>, <<5.0, 5.0, 5.0>>)
			ADD_DRIVER_TEXT_TO_QUEUE("JH_TUNNELE", "JH_TUNNELT", "JH_TUNNELK")
			ADD_DRIVER_TEXT_TO_QUEUE("JH_SPLITE", "JH_SPLITT", "JH_SPLITK")
			bEventFlag[6] = TRUE
		ENDIF
	ENDIF
	
	IF NOT bEventFlag[7]
	
		IF IS_ENTITY_AT_COORD(vehBike[0], <<2.365517,-633.195984,18.065960>>,<<21.312500,20.000000,3.562500>>)
			//runConversation(Asset.convPolice2, TRUE, TRUE)
				//Police:	Suspects seen entering storm drain. Do not pursue. Hold for possible exit points.
			
				//Franklin:	It don't look like they following us.
			bEventFlag[7] = TRUE
		ENDIF
	ENDIF
	
	//the split in the tunnels
	IF NOT bEventFlag[8]
		IF IS_ENTITY_IN_ANGLED_AREA(vehBike[0], <<-14.763006,-805.227905,15.391008>>, <<-24.694069,-782.254944,22.467754>>, 21.750000)
//			ADD_DRIVER_TEXT_TO_QUEUE("JH_SPLITE", "JH_SPLITT", "JH_SPLITK")
			
			//addTextToQueue(Asset.convPolice2, TEXT_TYPE_CONVO)
			//addTextToQueue(Asset.convLostThem, TEXT_TYPE_CONVO)
			
			bEventFlag[8] = TRUE
		ENDIF					
	ENDIF					
	
	//It gets muddy
	IF NOT bEventFlag[9]
		IF IS_ENTITY_AT_COORD(vehBike[0], <<49.1018, -801.2584, 16.8009>>,<<15.0,15.000000,4.0>>)
			ADD_DRIVER_TEXT_TO_QUEUE("JH_MUDE", "JH_MUDT", "JH_MUDK")
						
			//Gunman comments on how shit bikes are
			IF Asset.mnBike = BATI2		
				addTextToQueue("JH_SHITBIKE", TEXT_TYPE_CONVO)
//				IF IS_GUNMAN_GUSTAVO()
//					addTextToQueue("JH_BIKEB_GM", TEXT_TYPE_CONVO)
//				ELIF IS_GUNMAN_PACKIE()
//					addTextToQueue("JH_BIKEB_PM", TEXT_TYPE_CONVO)
//				ENDIF
			ELSE
				IF IS_GUNMAN_GUSTAVO()
					addTextToQueue("JH_BIKEG_GM", TEXT_TYPE_CONVO)
				ELIF IS_GUNMAN_PACKIE()
					addTextToQueue("JH_BIKEG_PM", TEXT_TYPE_CONVO)
				ENDIF
			ENDIF
			
			ADD_DRIVER_TEXT_TO_QUEUE("JH_COMP_ET", "JH_COMP_ET", "JH_COMP_KD")	//Comments on best bike for the job etc..
			
			bEventFlag[9] = TRUE	
		ENDIF
	ENDIF
	
	//Charge SA meter while going off jump in tunnel
	IF IS_ENTITY_IN_AIR(vehBike[2])
		SPECIAL_ABILITY_CHARGE_CONTINUOUS(PLAYER_ID(), TRUE)
	ENDIF
		
	IF bSAHelpPrinted = FALSE
		IF IS_SPECIAL_ABILITY_ENABLED(PLAYER_ID())
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),  <<124.5188, -615.0482, 16.7761>>, <<5.0, 5.0, 5.0>>)
				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 10.5)
				IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
					PRINT_HELP("JWL_ABILITY_KM", 12000)
				ELSE
					PRINT_HELP("JWL_ABILITY", 12000)
				ENDIF
				bSAHelpPrinted = TRUE
			ENDIF
		ENDIF
	ENDIF	
	
	//
	IF NOT bEventFlag[10]
		IF IS_ENTITY_AT_COORD(vehBike[0], <<122.4857, -612.9412, 16.7999>>,<<15.0,15.000000,4.0>>)
			IF NOT IS_DRIVER_KARIM()
				//Only play "left up the scalators" in this instance as there no time otherwise.
				IF bWorstGunDies			
					ADD_DRIVER_TEXT_TO_QUEUE("JH_LEFTE2", "JH_LEFTT2", "JH_LEFTK2")
				ENDIF
			ELSE
				
			ENDIF
			bEventFlag[10] = TRUE	
		ENDIF
	ENDIF
	
	IF NOT bEventFlag[11]
		IF IS_ENTITY_AT_COORD(vehBike[0], <<230.4518, -504.3735, 23.5882>>,<<11.0,10.000000,4.0>>)
			SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 6.5)
			ADD_DRIVER_TEXT_TO_QUEUE("JH_CORRE", "JH_CORRT", "JH_CORRK")
			bEventFlag[11] = TRUE	
		ENDIF
	ENDIF
	
	IF NOT bEventFlag[12]
		IF IS_ENTITY_AT_COORD(vehBike[0], <<337.6855, -454.0294, 22.5050>>,<<11.0,10.000000,4.0>>)
			ADD_DRIVER_TEXT_TO_QUEUE("JH_LIPE", "JH_LIPT", "JH_LIPK")
			bEventFlag[12] = TRUE	
		ENDIF
	ENDIF
	
	IF NOT bEventFlag[13]
		IF IS_ENTITY_AT_COORD(vehBike[0], <<421.6507, -495.5566, 7.7602>>,<<11.0,10.000000,4.0>>)
			ADD_DRIVER_TEXT_TO_QUEUE("JH_LEFLEFE", "JH_LEFLEFT", "JH_LEFLEFK")
			bEventFlag[13] = TRUE	
		ENDIF
	ENDIF
	
	IF NOT bEventFlag[13]
		IF IS_ENTITY_AT_COORD(vehBike[0], <<551.4762, -520.8657, -4.4253>>,<<11.0,10.000000,4.0>>)
			ADD_DRIVER_TEXT_TO_QUEUE("JH_GATEE", "JH_GATET", "JH_GATEK")
			bEventFlag[13] = TRUE	
		ENDIF
	ENDIF
		
	IF NOT bEventFlag[14]
		IF IS_ENTITY_AT_COORD(vehBike[0], <<134.772995,-606.382690,19.629339>>,<<15.875000,15.000000,3.125000>>)
			
			//runConversation("JH_CLEFT", FALSE, TRUE)
//			addTextToQueue(Asset.convChaseTunnelRamp, TEXT_TYPE_CONVO)
			
			bEventFlag[14] = TRUE
		ENDIF
	ENDIF

	IF NOT bEventFlag[15]
		
		IF IS_ENTITY_AT_COORD(vehBike[0], << 1015.7123, -181.0643, 39.7388 >>,<<80.750000,70.562500,30.250000>>)
			
			//Hacker dialogue.
			
			ADD_DRIVER_TEXT_TO_QUEUE("JH_HILLE", "JH_HILLT", "JH_HILLK")
			
			SWITCH GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)))
				CASE CMSK_GOOD
					addTextToQueue("JH_HCKARIVER", TEXT_TYPE_CONVO)				
				BREAK
				CASE CMSK_MEDIUM
					addTextToQueue("JH_HCKBRIVER", TEXT_TYPE_CONVO)
				BREAK
				CASE CMSK_BAD
					addTextToQueue("JH_HCKCRIVER", TEXT_TYPE_CONVO)					
				BREAK
			ENDSWITCH				
						
			//addTextToQueue("JH_CLEAVE2", TEXT_TYPE_CONVO)						
						
			bEventFlag[15] = TRUE
		ENDIF
	ENDIF
	
	//Starting to ascend...
	IF NOT bEventFlag[16]
		IF IS_ENTITY_AT_COORD(vehBike[0], <<595.4016, -437.2173, -4.3002>>,<<10.750000,4.562500,4.250000>>)
			ADD_DRIVER_TEXT_TO_QUEUE("JH_CLIMBE", "JH_CLIMBT", "JH_CLIMBK")
			bEventFlag[16] = TRUE
		ENDIF
	ENDIF
	
	
	IF NOT bEventFlag[17]
		IF IS_ENTITY_AT_COORD(vehBike[0], << 1034.2031, -285.5826, 49.1856 >>, <<6,6,6>>)
			//runConversation(Asset.convPolice3, TRUE, TRUE)
			//addTextToQueue(Asset.convPolice3, TEXT_TYPE_CONVO)
				//Police:	Jewellery store suspects picked up in the LS River channel. All units respond.
			addTextToQueue(Asset.convTrap, TEXT_TYPE_CONVO)
				//Franklin:	Shit! They all over us.
//			addTextToQueue(Asset.convCrewCopReact1, TEXT_TYPE_CONVO)
//			
//			IF NOT bWorstGunDies
//				addTextToQueue(Asset.convCrewCopReact2, TEXT_TYPE_CONVO)
//			ENDIF
			
			bEventFlag[17] = TRUE
		ENDIF
	ENDIF
	
	IF NOT bEventFlag[18]
		IF IS_ENTITY_AT_COORD(vehBike[0], <<833.7619, -294.1365, 4.4777>>,<<11.0,10.000000,4.0>>)
			ADD_DRIVER_TEXT_TO_QUEUE("JH_TRUCKE", "JH_TRUCKT", "JH_TRUCKK")
			bEventFlag[18] = TRUE	
		ENDIF
	ENDIF
		
	IF NOT bGodTextOngoing AND NOT bConversationOngoing
		IF NOT isAnyConversationWaitingInQueue()
			IF NOT isAnyGodTextWaitingInQueue()
			AND (iGetLostStage = 0 OR iGetLostStage = 10)
			AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehBike[0]) < 80.0
			AND NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
				IF TIMERA() > 10000
				AND bSAHelpPrinted			//Only do during middle part of 
				AND bEventFlag[14] = FALSE 	// the tunnel.
					SETTIMERA(0)
					IF bEventFlag[6] //bEventFlag[1]//PLayer hit tunnel entrance
					AND NOT bEventFlag[15] //PLayer hit tunnel exit
						IF NOT bWorstGunDies
							RunConversation(Asset.convFranklinEncouragementTunnel, TRUE, TRUE, TRUE)
						ENDIF
						//addTextToQueue(Asset.convFranklinEncouragementTunnel,TEXT_TYPE_CONVO)
//						addTextToQueue(Asset.convCrewFranklinRespond1, TEXT_TYPE_CONVO)
//						IF NOT bWorstGunDies
//							addTextToQueue(Asset.convCrewFranklinRespond2, TEXT_TYPE_CONVO)
//						ENDIF
					ELSE			//IF PLAYER IS NOT IN TUNNEL
						IF NOT bEventFlag[15]						
							IF IS_DRIVER_KARIM()
								RunConversation(Asset.convFranklinEncouragementOpen, TRUE, TRUE, TRUE)
							ELSE
								RunConversation("JH_FRENCO", TRUE, TRUE, TRUE)							
							ENDIF
							//addTextToQueue(Asset.convFranklinEncouragementOpen,TEXT_TYPE_CONVO)
						ENDIF
						//addTextToQueue(Asset.convCrewFranklinRespond1, TEXT_TYPE_CONVO)
//						IF NOT bWorstGunDies
//							addTextToQueue(Asset.convCrewFranklinRespond2, TEXT_TYPE_CONVO)
//						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	
ENDPROC

//PURPOSE:	Manages requests for mid chase
PROC DO_MID_CHASE_REQUESTS(INT iGroup)
	SWITCH iGroup
		CASE 0
			SAFE_REQUEST_MODEL_FOR_HEIST_CREW_MEMBER(HEIST_JEWEL, 1, FALSE, FALSE)
		BREAK
		CASE 1
			//Do cop trap loading
			FOR iCounter = 0 TO iNoOfStormCops-1
				PRELOAD_NEED_LIST_VEHICLE_RECORDING(Asset.recCopTrap[iCounter], Asset.recNumber)
			ENDFOR
			PRELOAD_NEED_LIST_MODEL(Asset.mnTrain)
		BREAK
		CASE 2
			PRELOAD_NEED_LIST_MODEL(Asset.mnPolice)
			PRELOAD_NEED_LIST_MODEL(Asset.mnPoliceman)
			FOR iCounter = 0 TO iNoOfStormCops-1
				PRELOAD_NEED_LIST_VEHICLE_RECORDING(Asset.recCopTrap[iCounter], Asset.recNumber)
			ENDFOR
			bPreloadNextStage = TRUE
		BREAK
		CASE 3
			bPreloadNextStage = TRUE
		BREAK
	ENDSWITCH
ENDPROC


VEHICLE_INDEX thisHornCar 

//PURPOSE: Checks for event triggers during the chase
PROC DO_CHASE_TRIGGERS()

	
	IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0])
		fRecordingTime = GET_TIME_POSITION_IN_RECORDING(vehBike[0])
	ENDIF
	
	
	//Don't see crash on second try...
	IF g_bJewelStoreJobSeeCarCrashOnce= FALSE
		If fRecordingTime > 23480.400
			g_bJewelStoreJobSeeCarCrashOnce = TRUE
		ENDIF
	ENDIF

		
	IF DOES_ENTITY_EXIST(thisHornCar)
		IF NOT IS_ENTITY_DEAD(thisHornCar)
			IF IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(thisHornCar))
				SET_HORN_PERMANENTLY_ON(thisHornCar)	
			ENDIF
		ENDIF
	ELSE
		thisHornCar= GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 30.0, DUMMY_MODEL_FOR_SCRIPT, 0)
	ENDIF
	
//	IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxKOGasHaze)
//		IF IS_ENTITY_AT_COORD(vehBike[0], <<-777.482300,-259.395691,45.095806>>,<<11.062500,14.562500,9.000000>>, FALSE, FALSE)
//			IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxKOGasHaze)
//				STOP_PARTICLE_FX_LOOPED(ptfxKOGasHaze)
//			ENDIF
//		ENDIF
//	ENDIF

	IF NOT bHasCopTrapTriggered																// +++ Cop Ambush +++
		IF IS_ENTITY_AT_COORD(vehBike[2], vCopTrapTrigger, <<6,6,6>>, FALSE, FALSE)
//			IMMEDIATELY_LOAD_NEED_LIST_MODEL(Asset.mnPolice)
//			IMMEDIATELY_LOAD_NEED_LIST_MODEL(Asset.mnPoliceman)
//			FOR iCounter = 0 TO iNoOfStormCops-1
//				IMMEDIATELY_LOAD_NEED_LIST_VEHICLE_RECORDING(Asset.recCopTrap[iCounter], Asset.recNumber)
//			ENDFOR
			FOR iCounter = 0 TO iNoOfStormCops-1
				IF DOES_BLIP_EXIST(blipPolice[iCounter])
					REMOVE_BLIP(blipPolice[iCounter])
				ENDIF
				vehPolice[iCounter] = 	CREATE_VEHICLE(Asset.mnPolice, 
										GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(Asset.recNumber, 0, Asset.recCopTrap[iCounter]))
				pedPolice[iCounter] = CREATE_PED_INSIDE_VEHICLE(vehPolice[iCounter], PEDTYPE_COP, Asset.mnPoliceman)
				SET_PED_COP(pedPolice[iCounter])
				pedPolicePassenger[iCounter] = CREATE_PED_INSIDE_VEHICLE(	vehPolice[iCounter], 
																			PEDTYPE_COP, Asset.mnPoliceman, VS_FRONT_RIGHT)
				SET_PED_COP(pedPolicePassenger[iCounter])
				
				SET_PED_AS_NO_LONGER_NEEDED(pedPolice[iCounter])
				SET_PED_AS_NO_LONGER_NEEDED(pedPolicePassenger[iCounter])
				
				SET_VEHICLE_SIREN(vehPolice[iCounter], TRUE)
				//blipPolice[iCounter] = CREATE_BLIP_FOR_VEHICLE(vehPolice[iCounter], TRUE)
				
				START_PLAYBACK_RECORDED_VEHICLE(vehPolice[iCounter], Asset.recNumber, Asset.recCopTrap[iCounter])
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehPolice[iCounter], 0)//2000)
				fPoliceSpeed[iCounter] = 0.5
				SET_PLAYBACK_SPEED(vehPolice[iCounter], fPoliceSpeed[iCounter])
			ENDFOR

			bHasCopTrapTriggered = TRUE
		ENDIF
	ENDIF																					// --- Cop Ambush ---
	
		
	FOR iCounter = 0 TO TOTAL_NUMBER_OF_SET_PIECE_CARS-1									//Turns on police car sirens
		IF DOES_ENTITY_EXIST(SetPieceCarID[iCounter])
		//AND iCounter <> 33
			IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[iCounter])
			
				IF GET_ENTITY_MODEL(SetPieceCarID[iCounter]) = AMBULANCE
					IF NOT IS_VEHICLE_SIREN_ON(SetPieceCarID[iCounter])
						SET_VEHICLE_SIREN(SetPieceCarID[iCounter], TRUE)
						SET_SIREN_WITH_NO_DRIVER(SetPieceCarID[iCounter], TRUE)
					ENDIF
				ENDIF
				
				IF IS_ENTITY_UPSIDEDOWN(SetPieceCarID[iCounter])
					INT iRandomDoor = GET_RANDOM_INT_IN_RANGE(0, 6)
					INT iRandomDoorOpen = GET_RANDOM_INT_IN_RANGE(0, 6)
					IF IS_MODEL_POLICE_VEHICLE(GET_ENTITY_MODEL(SetPieceCarID[iCounter]))
						IF NOT IS_VEHICLE_DOOR_DAMAGED(SetPieceCarID[iCounter], INT_TO_ENUM(SC_DOOR_LIST, iRandomDoor))
							SET_VEHICLE_DOOR_BROKEN(SetPieceCarID[iCounter], INT_TO_ENUM(SC_DOOR_LIST, iRandomDoor), FALSE)
							SET_VEHICLE_DOOR_OPEN(SetPieceCarID[iCounter], INT_TO_ENUM(SC_DOOR_LIST, iRandomDoorOpen))
						ENDIF
					ENDIF
				ENDIF
			
				IF IS_MODEL_POLICE_VEHICLE(GET_ENTITY_MODEL(SetPieceCarID[iCounter]))// = POLICE
					
					IF NOT IS_VEHICLE_SIREN_ON(SetPieceCarID[iCounter])
						SET_VEHICLE_SIREN(SetPieceCarID[iCounter], TRUE)
						SET_SIREN_WITH_NO_DRIVER(SetPieceCarID[iCounter], TRUE)
					ENDIF
					IF  IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[iCounter])
					AND GET_TIME_POSITION_IN_RECORDING(SetPieceCarID[iCounter]) > 5000
					AND GET_ENTITY_SPEED(SetPieceCarID[iCounter]) < 1.0
					AND NOT IS_VEHICLE_MODEL(SetPieceCarID[iCounter], POLMAV)
					
					
						#IF IS_DEBUG_BUILD
							IF bIsSuperDebugEnabled
								DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(SetPieceCarID[iCounter]) + <<0.0, -1.0, 0.0>>, 2.2, 50, 255, 50)
							ENDIF
						#ENDIF
						
						IF NOT IS_VEHICLE_SEAT_FREE(SetPieceCarID[iCounter])
						
							PED_INDEX tempCop = GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[iCounter])
							IF DOES_ENTITY_EXIST(tempCop)
							AND NOT IS_PED_INJURED(tempCop)
								GIVE_WEAPON_TO_PED(tempCop, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
								SET_PED_COMBAT_ATTRIBUTES( tempCop, CA_USE_VEHICLE, false)
								
								//TASK_COMBAT_HATED_TARGETS_AROUND_PED(tempCop, 100.00)
								
								// Target Franklin directly, in case he fell off his bike.
								////TASK_DRIVE_BY(tempCop, GET_PED_IN_VEHICLE_SEAT(vehBike[2]), NULL,  <<0.0, 0.0, 0.0>>, 150.0, 100, TRUE, FIRING_PATTERN_FULL_AUTO)
								TASK_DRIVE_BY(tempCop, GET_SELECT_PED(SEL_FRANKLIN), NULL,  <<0.0, 0.0, 0.0>>, 150.0, 100, TRUE, FIRING_PATTERN_FULL_AUTO)
								SET_PED_SPHERE_DEFENSIVE_AREA(tempCop, GET_ENTITY_COORDS(tempCop), 5.5 ) // Around the entire car
								SET_PED_KEEP_TASK(tempCop, TRUE)
								SET_PED_AS_NO_LONGER_NEEDED(tempCop)
							ENDIF
						ENDIF
						
						
					ENDIF
						
						INT iRandomTarget
					
						IF IS_VEHICLE_SEAT_FREE(SetPieceCarID[iCounter], VS_FRONT_RIGHT)
						AND	SetPieceCarCopCreated[iCounter] = FALSE
							PED_INDEX tempPassCop = CREATE_PED_INSIDE_VEHICLE(SetPieceCarID[iCounter], PEDTYPE_COP, S_M_Y_COP_01, VS_FRONT_RIGHT)
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPassCop, TRUE)
							GIVE_WEAPON_TO_PED(tempPassCop, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
							SET_PED_COMBAT_ATTRIBUTES( tempPassCop, CA_USE_VEHICLE, false)
							
							iRandomTarget = GET_RANDOM_INT_IN_RANGE(0, 3)
							
							IF DOES_ENTITY_EXIST(vehBike[iRandomTarget])
								IF DOES_ENTITY_EXIST(GET_PED_IN_VEHICLE_SEAT(vehBike[iRandomTarget]))
									TASK_DRIVE_BY(tempPassCop, GET_PED_IN_VEHICLE_SEAT(vehBike[iRandomTarget]), NULL,<<0.0, 0.0, 0.0>>, 700.0, 100, TRUE, FIRING_PATTERN_FULL_AUTO)
								ENDIF
							ELSE
								// Target Franklin directly, in case he fell off his bike.
								////TASK_DRIVE_BY(tempPassCop, GET_PED_IN_VEHICLE_SEAT(vehBike[2]), NULL, <<0.0, 0.0, 0.0>>, 150.0, 100, TRUE, FIRING_PATTERN_FULL_AUTO)
								TASK_DRIVE_BY(tempPassCop, GET_SELECT_PED(SEL_FRANKLIN), NULL, <<0.0, 0.0, 0.0>>, 150.0, 100, TRUE, FIRING_PATTERN_FULL_AUTO)
							ENDIF
							
							SET_PED_KEEP_TASK(tempPassCop, TRUE)
							SET_PED_AS_NO_LONGER_NEEDED(tempPassCop)
							SetPieceCarCopCreated[iCounter] = TRUE
						ENDIF
					
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
	
	FOR iCounter = 0 TO TOTAL_NUMBER_OF_PARKED_CARS-1									//Turns on traffic jam engines
		IF DOES_ENTITY_EXIST(ParkedCarID[iCounter])	
			IF IS_VEHICLE_DRIVEABLE(ParkedCarID[iCounter])
								
				IF IS_ENTITY_AT_COORD(ParkedCarID[iCounter], <<-5.679473,-524.588013,34.294395>>,<<69.312500,16.250000,4.000000>>, FALSE, FALSE)
					IF IS_VEHICLE_SEAT_FREE(ParkedCarID[iCounter])
						IF CAN_CREATE_RANDOM_PED(RPM_DONT_CARE)
							CREATE_RANDOM_PED_AS_DRIVER(ParkedCarID[iCounter])
							SET_VEHICLE_ENGINE_ON(ParkedCarID[iCounter], TRUE, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR

	IF NOT bCopsAtExitCreated
		IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), << 1042.1293, -278.9554, 49.5190>>,<<28.0, 24.40, 13.75>>)
		
			copAtExit1 = CREATE_PED(PEDTYPE_COP, Asset.mnPoliceman, << 1042.1293, -278.9554, 49.5190 >>, 65.9411)
			copAtExit2 = CREATE_PED(PEDTYPE_COP, Asset.mnPoliceman, << 1044.8005, -301.9515, 48.7054 >>, 133.1042)
			copAtExit3 = CREATE_PED(PEDTYPE_COP, Asset.mnPoliceman, << 1047.8716, -299.8919, 49.1066 >>, 7.8463)
			SET_PED_SPHERE_DEFENSIVE_AREA(copAtExit1, << 1042.1293, -278.9554, 49.5190 >>, 2.0)
			SET_PED_SPHERE_DEFENSIVE_AREA(copAtExit2, << 1044.8005, -301.9515, 48.7054 >>, 2.0)
			SET_PED_SPHERE_DEFENSIVE_AREA(copAtExit3, << 1047.8716, -299.8919, 49.1066 >>, 2.0)
			SET_PED_COP(copAtExit1, FALSE)		
			SET_PED_COP(copAtExit2, FALSE)		
			SET_PED_COP(copAtExit3, FALSE)		
			
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(GET_REPLAY_STAGE_FROM_MISSION_STAGE(STAGE_BIKE_CHASE_END_OF_TUNNEL), "STAGE_BIKE_CHASE_END_OF_TUNNEL")
			
			bCopsAtExitCreated = TRUE
		ENDIF
	ENDIF
	
	IF bWorstGunDies		
		// +++ Incompetent Ped Death +++
		
		REQUEST_MODEL(Prop_CS_Heist_Bag_02)
		
		IF DOES_ENTITY_EXIST(vehBike[1])															
			IF IS_VEHICLE_DRIVEABLE(vehBike[1])
				IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1])
						
						IF fRecordingTime > 34174.0 //Asset.skipJumpCrash
							REQUEST_COLLISION_AT_COORD(<<-125.4693, -522.0388, 28.9007>>)
						ENDIF
						
						IF fRecordingTime > 45174.0 //Asset.skipJumpCrash
							
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedCrew[CREW_GUNMAN_NEW], KNOCKOFFVEHICLE_EASY)
							
							IF CAN_KNOCK_PED_OFF_VEHICLE(pedCrew[CREW_GUNMAN_NEW])
							
								PLAY_SOUND_FROM_ENTITY(-1, "Gunman_Bike_Crash", vehBike[1],"JEWEL_HEIST_SOUNDS")
								
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedCrew[CREW_GUNMAN_NEW], "JH_FPAA", "NORM", SPEECH_PARAMS_FORCE_FRONTEND)
														
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[0])
									STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[0])
								ENDIF
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1])
									STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[1])
								ENDIF
								
								SET_PED_BAG(pedCrew[CREW_GUNMAN_NEW], FALSE)
								
								SET_ENTITY_PROOFS(pedCrew[CREW_GUNMAN_NEW], FALSE, FALSE, FALSE, FALSE, FALSE)
								
								SET_PED_CAN_RAGDOLL(pedCrew[CREW_GUNMAN_NEW], TRUE)
								SET_PED_CAN_BE_DRAGGED_OUT(pedCrew[CREW_GUNMAN_NEW], TRUE)
								SET_ENTITY_PROOFS(vehBike[1], FALSE, FALSE, FALSE, FALSE, FALSE)
								KNOCK_PED_OFF_VEHICLE(pedCrew[CREW_GUNMAN_NEW])
								//CLEAR_PED_TASKS_IMMEDIATELY(pedCrew[CREW_GUNMAN_NEW])
								
								//SET_PED_TO_RAGDOLL(pedCrew[CREW_GUNMAN_NEW], 10000, 10000, TASK_NM_BALANCE, TRUE, TRUE)
								APPLY_FORCE_TO_ENTITY(	pedCrew[CREW_GUNMAN_NEW], APPLY_TYPE_IMPULSE, 
														<<0.0, 3.0, 1.0>>, <<0.0, 0.0, 0.3>>, 0, TRUE, TRUE, TRUE)
								APPLY_FORCE_TO_ENTITY(	vehBike[1], APPLY_TYPE_IMPULSE, 
														<<1.0, 0.5, 0.0>>, <<0.0, 0.3, 0.0>>, 0, TRUE, TRUE, TRUE)
								
								//IF HAS_MODEL_LOADED(Prop_CS_Heist_Bag_02)
									VECTOR vBagCoords = GET_ENTITY_COORDS((pedCrew[CREW_GUNMAN_NEW]))
									GET_GROUND_Z_FOR_3D_COORD(vBagCoords, vBagCoords.z)
	//								
	//								vBagCoords.z = vBagCoords.z 
									//SCRIPT_ASSERT("bag created")
				   					oiBuddysBag = CREATE_OBJECT(Prop_CS_Heist_Bag_02, vBagCoords)
									blipBuddysBag = CREATE_BLIP_FOR_OBJECT(oiBuddysBag)
									bInitialVelocityGiven = FALSE								
									
									vFallingBikeVelocity = GET_ENTITY_VELOCITY(vehBike[1])
									
									//FREEZE_ENTITY_POSITION(oiBuddysBag, TRUE)
									
									IF (iCurrentTake / 3) * 2 < MIN_TAKE_VALUE_GIVEN_BAG_LOSS //2410820
										ilostBag = iCurrentTake - MIN_TAKE_VALUE_GIVEN_BAG_LOSS//2410820
										iCurrentTake = MIN_TAKE_VALUE_GIVEN_BAG_LOSS//2410820
									ELSE
										ilostBag = iCurrentTake / 3
										iCurrentTake = (iCurrentTake / 3) * 2
									ENDIF
																
								//ENDIF
								
								safeWait(0)
								IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
									SET_ENTITY_HEALTH(pedCrew[CREW_GUNMAN_NEW], 99)
								ENDIF
								
								IF IS_VEHICLE_DRIVEABLE(vehBike[1])
									SET_ENTITY_HEALTH(vehBike[1], 0)
									SET_VEHICLE_ENGINE_HEALTH(vehBike[1], 0)
								ENDIF
								
								ADD_ALL_PEDS_TO_DIALOGUE()
								
								//								//g_replay.iReplayInt[CURRENT_TAKE_ XXX REPLAY_VALUE] = iCurrentTake
								//SCRIPT_ASSERT("bike dead")
							ENDIF
						ELSE
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedCrew[CREW_GUNMAN_NEW], TRUE)
							SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehBike[1], TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		
		ENDIF
		
		IF DOES_ENTITY_EXIST(oiBuddysBag)
			IF DOES_ENTITY_HAVE_PHYSICS(oiBuddysBag)
			AND bInitialVelocityGiven = FALSE
				
				PRINTLN("bag velotiy:", vFallingBikeVelocity)
					
				SET_ENTITY_VELOCITY(oiBuddysBag, vFallingBikeVelocity)
				bInitialVelocityGiven = TRUE
			ENDIF
		ENDIF
			
		
		IF bSawBitchCrashOut = FALSE
		AND fRecordingTime > 46500.0// 44500.0 //Asset.skipJumpCrash
			IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-171.154953,-488.452881,25.975220>>, <<-63.240566,-537.047485,40.247398>>, 64.750000)
				PRINTLN("in here dawg")
				RunConversation(Asset.convBikeCrash, TRUE, TRUE, TRUE)
				
				IF IS_DRIVER_EDDIE()
					addTextToQueue("JH_GDIE_ET", TEXT_TYPE_CONVO)
				ELSE
					addTextToQueue("JH_GDIE_KD", TEXT_TYPE_CONVO)
				ENDIF
				
				bSawBitchCrashOut = TRUE
			ENDIF
			
		ENDIF
	ELSE
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedCrew[CREW_GUNMAN_NEW], TRUE)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehBike[1], TRUE)
	ENDIF																						// --- Incompetent Ped Death ---
	
	IF bPreloadNextStage															//If preload flag is triggered, get that loaded
		SCRIPT_ASSERT("boom!")
		PRELOAD_NEED_LIST_MODEL(Asset.mnTruck)
		PRELOAD_NEED_LIST_MODEL(Asset.mnPolice)
		PRELOAD_NEED_LIST_MODEL(Asset.mnPoliceman)
		PRELOAD_NEED_LIST_VEHICLE_RECORDING(Asset.recTruckLead, Asset.recNumber)
		
		SAFE_REQUEST_MODEL_FOR_HEIST_CREW_MEMBER(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW), FALSE, FALSE)
		SAFE_REQUEST_MODEL_FOR_HEIST_CREW_MEMBER(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER), FALSE, FALSE)
		
		IF HAS_VEHICLE_RECORDING_BEEN_LOADED(Asset.recNumber, Asset.recTruckLead)
		AND HAS_MODEL_LOADED(Asset.mnTruck)
			IF NOT DOES_ENTITY_EXIST(vehTruck)
			OR NOT IS_VEHICLE_DRIVEABLE(vehTruck)
				vehTruck = CREATE_VEHICLE(Asset.mnTruck,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(	Asset.recNumber, 0, Asset.recTruckLead), 135)
				//[MF]Remove all randomly assigned extras from the vehicle (B* 1689820)
				REMOVE_ALL_VEHICLE_EXTRAS(vehTruck)	

				SET_VEHICLE_COLOURS(vehTruck, 128, 3) SET_VEHICLE_EXTRA(vehTruck, 6, FALSE) //SET_VEHICLE_LIVERY(vehTruck, 1)
			ELSE
				SET_ENTITY_COORDS(vehTruck, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(	Asset.recNumber, 0, Asset.recTruckLead))
				SET_ENTITY_HEADING(vehTruck, 135)
			ENDIF
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(GET_SELECT_PED(SEL_MICHAEL))
		OR IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
			CREATE_PLAYER_PED_ON_FOOT(	sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, 
										vMichaelTruckCoords, fMichaelTruckHeading)
		ELSE
			SET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL), vMichaelTruckCoords)
			SET_ENTITY_HEADING(GET_SELECT_PED(SEL_MICHAEL), fMichaelTruckHeading)
		ENDIF
		
		IF HAS_MODEL_LOADED_FOR_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)))
			IF NOT DOES_ENTITY_EXIST(pedCrew[CREW_HACKER])
			OR IS_PED_INJURED(pedCrew[CREW_HACKER])
				pedCrew[CREW_HACKER] = CREATE_HEIST_CREW_MEMBER_AT_INDEX(	HEIST_JEWEL,  ENUM_TO_INT(CREW_HACKER), 
																			vCrewTruckCoords, fCrewTruckHeading)
			ELSE
				SET_ENTITY_COORDS(pedCrew[CREW_HACKER], vCrewTruckCoords)
				SET_ENTITY_HEADING(pedCrew[CREW_HACKER], fCrewTruckHeading)
			ENDIF
			GIVE_WEAPON_TO_PED(pedCrew[CREW_HACKER], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
		ENDIF
	ENDIF
ENDPROC


//PURPOSE: Creates blip for cases
PROC updateStoreBlips()
	FOR iCounter = 0 TO iNoOfCases-1
		IF NOT DOES_BLIP_EXIST(jcStore[iCounter].blipCase)
			IF NOT jcStore[iCounter].bSmashed
				jcStore[iCounter].blipCase = CREATE_BLIP_FOR_COORD(jcStore[iCounter].vCasePos)
				SET_BLIP_SCALE(jcStore[iCounter].blipCase, 0.5)
				SET_BLIP_COLOUR(jcStore[iCounter].blipCase, BLIP_COLOUR_GREEN)
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

//PURPOSE: Clears blips for cases
PROC clearStoreBlips()
	FOR iCounter = 0 TO iNoOfCases-1
		IF DOES_BLIP_EXIST(jcStore[iCounter].blipCase)
			REMOVE_BLIP(jcStore[iCounter].blipCase)
		ENDIF
	ENDFOR
ENDPROC

//PURPOSE: Resets blips for cases
PROC resetStoreBlips()
	FOR iCounter = 0 TO iNoOfCases-1
		jcStore[iCounter].bSmashed = FALSE
	ENDFOR
	updateStoreBlips()
ENDPROC

//PURPOSE: Finds the nearest case to the player
PROC findNearestCase()
	
	VECTOR vMichaelPos = GET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL))

	FOR iCounter = 0 TO iNoOfCases-1
		IF NOT jcStore[iCounter].bSmashed
		//AND (jcStore[iCounter].iSmashedByBulletStage = 0)
			IF VDIST2(jcStore[iCounter].vCasePos, vMichaelPos) < VDIST2(jcStore[iNearestCase].vCasePos, vMichaelPos)
				IF iNearestCase <> iCounter
					PRINTLN("******************NEW NEAREST CASE**********************")
					iNearestCase = iCounter
				ENDIF
			ENDIF
		ENDIF
	ENDFOR
ENDPROC

//PURPOSE: 
FUNC BOOL ARE_ALL_CASES_SMASHED()

	FOR iCounter = 0 TO iNoOfCases-1
		IF NOT jcStore[iCounter].bSmashed
			RETURN FALSE	
		ENDIF
	ENDFOR
	
	PRINTLN("CASES ALL SMASHED ", iFarthestCase)
	
	RETURN TRUE
	
ENDFUNC

//PURPOSE: Finds the nearest case to the player
PROC findFurthestCase()

	FLOAT fDistanceToIndexCase = 0.0
	FLOAT fDistanceToCurrentFurthestCase = 0.0

	iFarthestCase = -1

	FOR iCounter = 0 TO iNoOfCases-1
	
		fDistanceToIndexCase = GET_DISTANCE_BETWEEN_COORDS(jcStore[iCounter].vCasePos, GET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL)))
	
		//PRINTLN("Is ", 	fDistanceToIndexCase, ">", fDistanceToCurrentFurthestCase)
	
		IF 	fDistanceToIndexCase > fDistanceToCurrentFurthestCase
		
			IF jcStore[iCounter].bSmashed = FALSE
				rfReset = GET_RAYFIRE_MAP_OBJECT(jcStore[iCounter].vCasePos, fRFCheckRadius,
												jcStore[iCounter].sSmashAnim)
				IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfReset)
					IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfReset) = RFMO_STATE_START//<> RFMO_STATE_PRIMED
						IF iFarthestCase =  iNearestCase
							PRINTLN("oh shit dawg we're trying to smash the same case yo")
						ELSE
							iFarthestCase = iCounter
							fDistanceToCurrentFurthestCase = fDistanceToIndexCase
							PRINTLN("New Furthest INTACT case: ", iFarthestCase)
						ENDIF
					ELSE
						PRINTLN("CAse intact but not PRIMED! ", iCounter)
					ENDIF
				ENDIF				
			ELSE
				//PRINTLN("Smashed Case: ", iCounter)
			ENDIF		
		ENDIF
	ENDFOR
	
	IF ARE_ALL_CASES_SMASHED()
		//SCRIPT_ASSERT("all smashed!")
	ENDIF
	
	PRINTLN("FAR CASE FOUND: ", iFarthestCase)
	
ENDPROC


//PURPOSE: Smashes any cases that have been shot
PROC smashShotCase()
	
	VECTOR vPosition, vRotation
	
	FOR iCounter = 0 TO iNoOfCases-1
	
		IF NOT jcStore[iCounter].bSmashed
			
			SWITCH jcStore[iCounter].iSmashedByBulletStage
			
				CASE 0
					
					#IF IS_DEBUG_BUILD
						IF bIsSuperDebugEnabled
							DRAW_DEBUG_SPHERE(	jcStore[iCounter].vCasePos, 0.8, 0, 255 ,0, 127)
						ENDIF
					#ENDIF
					
					IF IS_BULLET_IN_AREA(jcStore[iCounter].vCasePos, 0.8, TRUE)													
						jcStore[iCounter].rfCaseSmash = GET_RAYFIRE_MAP_OBJECT(	jcStore[iCounter].vCasePos, fRFCheckRadius, jcStore[iCounter].sSmashAnim)
						SET_STATE_OF_RAYFIRE_MAP_OBJECT(jcStore[iCounter].rfCaseSmash, RFMO_STATE_PRIMING)
						jcStore[iCounter].iSmashedByBulletStage++
					ENDIF
					
				BREAK
				
				CASE 1
				
					#IF IS_DEBUG_BUILD
						IF bIsSuperDebugEnabled
							DRAW_DEBUG_SPHERE(	jcStore[iCounter].vCasePos, 0.8, 0, 0 ,255, 127)
						ENDIF
					#ENDIF
					
					IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(jcStore[iCounter].rfCaseSmash) = RFMO_STATE_PRIMED
						SET_STATE_OF_RAYFIRE_MAP_OBJECT(jcStore[iCounter].rfCaseSmash, RFMO_STATE_START_ANIM)
						
						PlaySoundFromCoord(SND_SMASH_CABINET, Asset.soundSmashCabinet, jcStore[iCounter].vCasePos, FALSE)
						//START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_jewel_cab_smash", GET_CURRENT_PED_WEAPON_ENTITY_INDEX(PLAYER_PED_ID()), vSmashParticleOffset, VSmashParticleRotation) //, BONETAG_R_HAND)
						
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(jcStore[iCounter].vCasePos, 2, P_INT_JEWEL_MIRROR)
							GET_COORDS_AND_ROTATION_OF_CLOSEST_OBJECT_OF_TYPE(	jcStore[iCounter].vCasePos, 2, 
																					P_INT_JEWEL_MIRROR, vPosition, vRotation)
							IF NOT DOES_ENTITY_EXIST(objGrenade)
								objGrenade =  CREATE_OBJECT(Asset.mnGrenade, vPosition)
							ENDIF
							SET_ENTITY_VISIBLE(objGrenade, FALSE)
							APPLY_FORCE_TO_ENTITY(objGrenade, APPLY_TYPE_FORCE, <<0,0,0.1>>, VECTOR_ZERO, 0, TRUE, TRUE, FALSE)
						ENDIF
						
						jcStore[iCounter].iSmashedByBulletStage++
					ENDIF
				BREAK
				
				CASE 2
				
					#IF IS_DEBUG_BUILD
						IF bIsSuperDebugEnabled
							DRAW_DEBUG_SPHERE(	jcStore[iCounter].vCasePos, 1.0, 255, 0 ,0, 127)
						ENDIF
					#ENDIF
					
				BREAK
				
			ENDSWITCH
			
		ENDIF
	ENDFOR
ENDPROC



//PURPOSE: Plays dialogue based on how long the player has left
PROC UPDATE_COUNTDOWN_CONVERSATIONS(INT timer)
	//IF NOT bIsHackTimeUp
		//SWITCH GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)))
		SWITCH GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)) 
			CASE CM_HACKER_G_PAIGE   //CMSK_GOOD
				IF timer > (iTimeLimit)
					IF NOT bEventFlag[1]
						addTextToQueue("JH_HCKA00", TEXT_TYPE_CONVO)
							//Hacker:	Ten seconds.
						bEventFlag[1] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit)
					IF NOT bEventFlag[2]
						addTextToQueue(Asset.convHackerA10, TEXT_TYPE_CONVO)
							//Hacker:	Ten seconds.
						bEventFlag[2] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit-20000)
					IF NOT bEventFlag[3]
						IF g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE] < (iNoOfCases - 3)
							addTextToQueue("JH_NOTIME", TEXT_TYPE_CONVO)	
						ENDIF
						addTextToQueue(Asset.convHackerA20, TEXT_TYPE_CONVO)
							//Hacker:	Twenty seconds.
						
						IF iCurrentTake < TARGET_TAKE_VALUE
							addTextToQueue("JH_NO2MIL", TEXT_TYPE_CONVO)
						ENDIF
						
						bEventFlag[3] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit-30000)
					IF NOT bEventFlag[4]
						addTextToQueue(Asset.convHackerA30, TEXT_TYPE_CONVO)
							//Hacker:	Half a minute left.
						bEventFlag[4] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit-40000)
					IF NOT bEventFlag[5]
						addTextToQueue(Asset.convHackerA40, TEXT_TYPE_CONVO)
						
						IF iCurrentTake < TARGET_TAKE_VALUE
							addTextToQueue("JH_HALF", TEXT_TYPE_CONVO)
						ENDIF
							//Hacker:	Forty seconds.
						bEventFlag[5] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit-50000)
					IF NOT bEventFlag[6]
						addTextToQueue(Asset.convHackerA50, TEXT_TYPE_CONVO)
							//Hacker:	Fifty seconds.
						bEventFlag[6] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit-60000)
					IF NOT bEventFlag[7]
						addTextToQueue(Asset.convHackerA60, TEXT_TYPE_CONVO)
							//Hacker:	One minute.
						bEventFlag[7] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit-70000)
					IF NOT bEventFlag[8]
										
						//addTextToQueue("JH_HCKA70", TEXT_TYPE_CONVO)
							//Hacker:	One minute.
						bEventFlag[8] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit-80000)
					IF NOT bEventFlag[9]
						addTextToQueue("JH_LOTSTIME", TEXT_TYPE_CONVO)	
						addTextToQueue("JH_HCKA80", TEXT_TYPE_CONVO)
						
							//Hacker:	One minute.
						bEventFlag[9] = TRUE
					ENDIF
					
				ENDIF
			BREAK
			//CASE CMSK_MEDIUM
			CASE CM_HACKER_M_CHRIS
				IF timer > (iTimeLimit)
					IF NOT bEventFlag[1]
						addTextToQueue("JH_HCKB00", TEXT_TYPE_CONVO)
							//Hacker:	You better be outta there, boys.  
						bEventFlag[1] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit-10000)
					IF NOT bEventFlag[2]
						addTextToQueue(Asset.convHackerB10, TEXT_TYPE_CONVO)
							//Hacker:	Ten, nine, eight, seven... shit.
						bEventFlag[2] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit-20000)
					IF NOT bEventFlag[3]
						IF g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE] < (iNoOfCases - 3)
							addTextToQueue("JH_NOTIME", TEXT_TYPE_CONVO)	
						ENDIF
						addTextToQueue(Asset.convHackerB20, TEXT_TYPE_CONVO)
							//Hacker:	I'd only give it twenty more seconds.
						IF iCurrentTake < TARGET_TAKE_VALUE// 2000000
							addTextToQueue("JH_NO2MIL", TEXT_TYPE_CONVO)
						ENDIF
						bEventFlag[3] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit-30000)
					IF NOT bEventFlag[4]
						addTextToQueue(Asset.convHackerB30, TEXT_TYPE_CONVO)
						IF iCurrentTake < TARGET_TAKE_VALUE
							addTextToQueue("JH_HALF", TEXT_TYPE_CONVO)
						ENDIF
							//Hacker:	I think you got maybe thirty seconds left.
						bEventFlag[4] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit-40000)
					IF NOT bEventFlag[5]
						addTextToQueue(Asset.convHackerB40, TEXT_TYPE_CONVO)
							//Hacker:	Forty seconds.
						bEventFlag[5] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit-50000)
					IF NOT bEventFlag[6]
						
											
						addTextToQueue("JH_HCKB50", TEXT_TYPE_CONVO)
							//Hacker:	Forty seconds.
						bEventFlag[6] = TRUE
					ENDIF	
				ENDIF
			BREAK
			
			//CASE CMSK_BAD
			CASE CM_HACKER_B_RICKIE_UNLOCK
				IF timer > (iTimeLimit + 10000)
					IF NOT bEventFlag[0]
						addTextToQueue("JH_HCKCC5", TEXT_TYPE_CONVO)
							//Hacker:	10, 9 duh etc...
						bEventFlag[0] = TRUE
					ENDIF	
				ELIF timer > (iTimeLimit + 8000)
					IF NOT bEventFlag[1]
						addTextToQueue("JH_HCKCC4", TEXT_TYPE_CONVO)
							//Hacker:	10, 9 duh etc...
						bEventFlag[1] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit + 5000)
					IF NOT bEventFlag[2]
						//addTextToQueue("JH_HCKC10", TEXT_TYPE_CONVO)
							//Hacker:	10, 9 duh etc...
						bEventFlag[2] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit - 13000)
					IF NOT bEventFlag[3]
						addTextToQueue("JH_HCKCCDWN", TEXT_TYPE_CONVO)
						bEventFlag[3] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit - 19000)
					IF NOT bEventFlag[4]
						IF g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE] < (iNoOfCases - 3)
							addTextToQueue("JH_NOTIME", TEXT_TYPE_CONVO)	
							
						ENDIF
						//addTextToQueue("JH_HCKC20", TEXT_TYPE_CONVO)
							//Hacker:	I'd only give it twenty more seconds.
						bEventFlag[4] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit - 32000)
					IF NOT bEventFlag[4]
						addTextToQueue("JH_HCK30", TEXT_TYPE_CONVO)
							//Hacker:	I'd only give it twenty more seconds.
						bEventFlag[4] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit-36500)
					IF NOT bEventFlag[5]
						//addTextToQueue("JH_HCKC40", TEXT_TYPE_CONVO)
							//Hacker:	Shit, I dunno how long you got. Maybe forty seconds before the alarms goes?
						IF iCurrentTake < TARGET_TAKE_VALUE
							addTextToQueue("JH_NO2MIL", TEXT_TYPE_CONVO)
						ENDIF
						bEventFlag[5] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit-50000)
					IF NOT bEventFlag[6]						
						addTextToQueue("JH_HCKCC3", TEXT_TYPE_CONVO)
							//Hacker:	Shit, ummm, okay. The alarm hasn't gone off yet.
						bEventFlag[6] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit-50000)
					IF NOT bEventFlag[7]
						//addTextToQueue("JH_HCKCC2", TEXT_TYPE_CONVO)
							//Hacker:	Yeah, like, I'd start to think about getting out of there soon. 
						bEventFlag[7] = TRUE
					ENDIF
				ELIF timer > (iTimeLimit-60000)
					IF NOT bEventFlag[8]
						//addTextToQueue("JH_HCKCC1", TEXT_TYPE_CONVO)
							//Hacker:	Maybe you got a bit longer, but not much.		
						
						bEventFlag[8] = TRUE
					ENDIF
				ENDIF
			BREAK
			
		ENDSWITCH

ENDPROC

FLOAT fOpenMultiplier = 0.05
BOOL bToldToGetDown = FALSE
BOOL bManagerSaidHerLine
INT iGuardRunTimer
BOOL bSaidRunnerDialogue = FALSE

//PURPOSE: Spawns Guard run event and manager room enter event at correct time and if flags are set to do so
PROC UPDATE_GUARD_AND_MANAGER()
	VECTOR vHeading
	
	IF NOT IS_PED_INJURED(pedSecurity)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedSecurity, PLAYER_PED_ID())
			APPLY_DAMAGE_TO_PED(pedSecurity, 100, TRUE)
		ENDIF
	ENDIF
		
	IF NOT IS_PED_INJURED(pedBackroom)
		IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedBackroom, PLAYER_PED_ID())
			APPLY_DAMAGE_TO_PED(pedBackroom, 100, TRUE)
		ENDIF
	ENDIF
		
	IF bGuardRuns
	
		IF NOT bIsRandomGuardEventDone
			
			#IF IS_DEBUG_BUILD
				IF bIsSuperDebugEnabled
					PRINTLN("iRandomGuardStage:", iRandomGuardStage)
				ENDIF
			#ENDIF
			
			//CHECK FOR THE PLAYER STOPPING THE GUARD!
			IF NOT bAimedAtGuard					
			AND bIsRandomGuardEventNow = TRUE
			AND iRandomGuardStage <= 7
				IF NOT IS_PED_INJURED(pedSecurity)
					IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedSecurity)
					OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedSecurity)
						
						#IF IS_DEBUG_BUILD
							IF bIsSuperDebugEnabled
								DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(pedSecurity), 0.4, 255, 0, 0)
							ENDIF
						#ENDIF
						CLEAR_PRINTS()
						emptyTextQueue()
						KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
						//runConversation(Asset.convStopGuard, TRUE, TRUE, TRUE)
						runConversation("JH_DOWN", TRUE, TRUE, TRUE)
						addTextToQueue("JH_BERATE", TEXT_TYPE_CONVO)	
						
						REPLAY_RECORD_BACK_FOR_TIME(4.0)
						
						SET_ENTITY_IS_TARGET_PRIORITY(pedSecurity, FALSE)
						//addTextToQueue(Asset.godStopGuard, TEXT_TYPE_GOD)					
						
						IF IS_AUDIO_SCENE_ACTIVE("JSH_2B_SECURITY_ESCAPE")
							STOP_AUDIO_SCENE("JSH_2B_SECURITY_ESCAPE")
						ENDIF
						
						iRandomGuardStage = 97
						bAimedAtGuard = TRUE				
					ELSE
						#IF IS_DEBUG_BUILD
							IF bIsSuperDebugEnabled
								DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(pedSecurity), 0.4, 0, 255, 0)
							ENDIF
						#ENDIF
					ENDIF
				ELSE
					IF IS_AUDIO_SCENE_ACTIVE("JSH_2B_SECURITY_ESCAPE")
						STOP_AUDIO_SCENE("JSH_2B_SECURITY_ESCAPE")
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
					IF bIsSuperDebugEnabled
						DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(pedSecurity), 0.4, 0, 0, 255)
					ENDIF
				#ENDIF
			ENDIF
		
			SWITCH iRandomGuardStage
				CASE 0
					IF NOT IS_PED_INJURED(pedSecurity)
						SET_PED_SHOP(pedSecurity)
						SET_PED_CAN_BE_TARGETTED(pedSecurity, TRUE)
						CLEAR_PED_TASKS_IMMEDIATELY(pedSecurity)
						
						//Translation << -629.104, -231.389, 38.064 >>
						//Rotation = << 0.000, 0.000, 158.000 >>
						
						TASK_MOVE_NETWORK_ADVANCED_BY_NAME(	pedSecurity, "HeistJewel_SecurityGuard2B", << -1629.104, -231.389, 38.064 >>, << 0.000, 0.000, 158.000 >>, EULER_YXZ, 0.0, FALSE, "", MOVE_USE_KINEMATIC_PHYSICS)
						//FREEZE_ENTITY_POSITION(pedSecurity, TRUE)
						//SET_ENTITY_COLLISION(pedSecurity, FALSE)
					ENDIF
					bAimedAtGuard = FALSE
					iRandomGuardStage++
				BREAK
				
				CASE 1
					IF GET_GAME_TIMER() - iGuardRunTimer > 7000
					AND NOT bIsPlayerLooting
					AND NOT IS_MESSAGE_BEING_DISPLAYED()
					AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()	
					AND (eRandomManagerStage < MANAGER_APPEARS
						OR eRandomManagerStage = MANAGER_KNEELS
						OR (DOES_ENTITY_EXIST(pedBackRoom) AND IS_PED_INJURED(pedBackroom)))
					AND NOT IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedSecurity)
					AND NOT IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedSecurity)
					AND IS_PLAYER_CONTROL_ON(PLAYER_ID())	
					AND IS_GAMEPLAY_CAM_RENDERING()
						IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_RUN", CONV_PRIORITY_VERY_HIGH)
							START_AUDIO_SCENE("JSH_2B_SECURITY_ESCAPE")

							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							//RunConversation(Asset.convRunAway, TRUE, TRUE, TRUE)
							bIsRandomGuardEventNow = TRUE
								//Security : This ain't my time!
								//Michael : Where the fuck is he going?
							iRandomGuardStage++
						ENDIF
					ENDIF
				BREAK
				
				CASE 2
					iRandomGuardStage++
				BREAK
				
				CASE 3
					IF NOT IS_PED_INJURED(pedSecurity)
						IF IS_TASK_MOVE_NETWORK_ACTIVE(pedSecurity)
							
							SET_ENTITY_IS_TARGET_PRIORITY(pedSecurity, TRUE)
							FREEZE_ENTITY_POSITION(pedSecurity, FALSE)
							controlJewelStoreDoors(FALSE, 0.0)
							clearStoreBlips()
							blipFlee = CREATE_BLIP_FOR_PED(pedSecurity, TRUE)

							DO_START_CUTSCENE(TRUE, FALSE, FALSE)
							doTwoPointCam(  <<-628.310974,-230.896378,37.334579>>,<<3.294292,-0.000000,145.186615>>,45.000000,
											<<-628.530884,-231.212753,37.356766>>,<<3.294292,-0.000000,145.186615>>,45.000000,
											2500)
																																												
							SETTIMERB(0)
							
							IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(pedSecurity)
								REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(pedSecurity, "UP")
							ENDIF
														
							CREATE_MODEL_HIDE(<< -621.8983, -230.8202, 37.0570 >>, 10.0, P_Jewel_Door_L, FALSE)
									
							oiStoreDoorTemp = CREATE_OBJECT(P_Jewel_Door_L, <<-631.96, -236.33, 37.057>>)
							SET_ENTITY_HEADING(oiStoreDoorTemp, -54.430990525)
									
							thisSceneID = CREATE_SYNCHRONIZED_SCENE(<< -631.934, -236.331, 37.059 >>, << -0.000, 0.000, 36.000 >>)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(oiStoreDoorTemp, thisSceneID, "2B_door_shut", "missHeist_jewel", INSTANT_BLEND_IN)
							SET_SYNCHRONIZED_SCENE_LOOPED(thisSceneID, TRUE)
							
																
							//SET_ENTITY_COORDS(pedCrew[cnSmashGunman], << -621.8983, -230.8202, 37.0570 >>)
							//SET_ENTITY_HEADING(pedCrew[cnSmashGunman], 87.3800)
							//TASK_AIM_GUN_AT_ENTITY(pedCrew[cnSmashGunman], pedCrew[cnSmashGunman], 5000, TRUE )
							//FORCE_PED_AI_AND_ANIMATION_UPDATE(pedCrew[cnSmashGunman])
														
							bIsRandomGuardEventNow = TRUE
							iRandomGuardStage++
						ELSE
							iRandomGuardStage = 99
						ENDIF
					ELSE
						iRandomGuardStage = 99
					ENDIF
				BREAK
				CASE 4
					IF TIMERB() > 2500
						IF NOT IS_PED_INJURED(pedSecurity)
							vHeading = GET_ENTITY_COORDS(pedSecurity) - GET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL))
							SET_ENTITY_HEADING(GET_SELECT_PED(SEL_MICHAEL), GET_HEADING_FROM_VECTOR_2D(vHeading.x, vHeading.y))
							SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
							ADD_ALL_PEDS_TO_DIALOGUE()
							SETTIMERB(0)
							
							runGodText(Asset.godStopGuard, TRUE, TRUE, TRUE)
							//addTextToQueue(Asset.godStopGuard, TEXT_TYPE_GOD)
													
														
							IF NOT IS_PED_INJURED(pedSecurity)
								SET_ENTITY_IS_TARGET_PRIORITY(pedSecurity, TRUE)
							ENDIF
						ENDIF
						bSaidRunnerDialogue = FALSE
						DO_END_CUTSCENE()
						iRandomGuardStage++
					ENDIF
				BREAK
				CASE 5
				
					IF NOT IS_PED_INJURED(pedSecurity)	
						IF NOT IS_PED_INJURED(pedSecurity)
							IF GET_TASK_MOVE_NETWORK_SIGNAL_BOOL(pedSecurity, "Run")
								iRandomGuardStage++
							ENDIF
						ENDIF
					ELSE
						iRandomGuardStage = 99
					ENDIF
					
				BREAK
				CASE 6
				
					IF NOT IS_PED_INJURED(pedSecurity)
						IF GET_TASK_MOVE_NETWORK_SIGNAL_BOOL(pedSecurity, "Escape")
							//RunConversation(Asset.convShootHim, TRUE, TRUE, TRUE)
							//addTextToQueue(Asset.godStopGuard, TEXT_TYPE_GOD)
							
							thisSceneID = CREATE_SYNCHRONIZED_SCENE(<< -631.934, -236.331, 37.059 >>, << -0.000, 0.000, 36.000 >>)
							PLAY_SYNCHRONIZED_ENTITY_ANIM(oiStoreDoorTemp, thisSceneID, "2B_door_escape", "missHeist_jewel", INSTANT_BLEND_IN)
							SET_SYNCHRONIZED_SCENE_LOOPED(thisSceneID, FALSE)
							//PLAY_ENTITY_ANIM(oiStoreDoorTemp, "2B_door_escape", "missHeist_jewel", INSTANT_BLEND_IN, FALSE, FALSE)
					
							SETTIMERB(0)
							iRandomGuardStage++
						ENDIF	
					ELSE
						iRandomGuardStage = 99
					ENDIF
				BREAK
				CASE 7
					IF NOT IS_PED_INJURED(pedSecurity)
						IF TIMERB() > 2468
							SET_ENTITY_COLLISION(pedSecurity, TRUE)
							CLEAR_PED_TASKS_IMMEDIATELY(pedSecurity)
							OPEN_SEQUENCE_TASK(seqSequence)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vSecurityRunCoords1, PEDMOVEBLENDRATIO_SPRINT, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_SUPPRESS_EXACT_STOP)
								TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, vSecurityRunCoords2, PEDMOVEBLENDRATIO_SPRINT, DEFAULT_TIME_NEVER_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_SUPPRESS_EXACT_STOP)
							CLOSE_SEQUENCE_TASK(seqSequence)
							TASK_PERFORM_SEQUENCE(pedSecurity, seqSequence)
							CLEAR_SEQUENCE_TASK(seqSequence)
							FORCE_PED_MOTION_STATE(pedSecurity, MS_ON_FOOT_SPRINT, TRUE)
							IMMEDIATELY_UPDATE_ANIMS(pedSecurity)
							iRandomGuardStage++
						ENDIF
					ELSE
						iRandomGuardStage = 99
					ENDIF
				BREAK
				CASE 8
					
					IF NOT IS_PED_INJURED(pedSecurity)
						IF IS_ENTITY_AT_COORD(pedSecurity, <<-656.515869,-211.484665,38.809677>>,<<3.562500,3.750000,2.500000>>)
							eMissionFail = FAIL_SECURITY_ESCAPED
							MissionFailed()
						ENDIF
					ELSE
						iRandomGuardStage = 99
					ENDIF
				BREAK
				
				CASE 97
					
					//CLEAR_PED_TASKS(pedCrew[cnSmashGunman])
					
					//SCRIPT_ASSERT("bingo")
					//IF IS_TASK_MOVE_NETWORK_READY_FOR_TRANSITION(pedSecurity)
					//	IF REQUEST_TASK_MOVE_NETWORK_STATE_TRANSITION(pedSecurity, "DOWN")
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedSecurity, TRUE)
							TASK_PLAY_ANIM(pedSecurity, "missheist_jewel", "2b_guard_getdown", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE)
							
							REPLAY_RECORD_BACK_FOR_TIME(4.0)
							
							SETTIMERB(0)
							REMOVE_BLIP(blipFlee)
							iRandomGuardStage = 98
					//	ENDIF
					//ENDIF
					
				BREAK
				
				CASE 98				//Guard is getting on his knees, give him time then end
					IF TIMERB() > 1500
						iRandomGuardStage = 99
					ENDIF
				BREAK
				CASE 99
					DO_END_CUTSCENE()
					IF DOES_BLIP_EXIST(blipFlee)
						REMOVE_BLIP(blipFlee)
					ENDIF
										
					REMOVE_MODEL_HIDE(<< -621.8983, -230.8202, 37.0570 >>, 10.0, P_Jewel_Door_L)
					DELETE_OBJECT(oiStoreDoorTemp)		
					
					bIsRandomGuardEventNow = FALSE
					bIsRandomGuardEventDone = TRUE
					iRandomGuardStage = 100
				BREAK
						
			ENDSWITCH
			
			IF iRandomGuardStage >= 3
				IF bSaidRunnerDialogue = FALSE
					IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_RUNNER", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
						bSaidRunnerDialogue = TRUE
					ENDIF
				ENDIF
			ENDIF
			
		ELSE
			//SCRIPT_ASSERT("event done")
			IF NOT IS_PED_INJURED(pedSecurity)
				IF NOT IS_TASK_MOVE_NETWORK_ACTIVE(pedSecurity)
				AND IS_ENTITY_PLAYING_ANIM(pedSecurity, "missheist_jewel", "2b_guard_getdown")
				AND GET_ENTITY_ANIM_CURRENT_TIME(pedSecurity, "missheist_jewel", "2b_guard_getdown") >=0.99
				//AND NOT IS_TASK_ONGOING(pedSecurity, SCRIPT_TASK_PLAY_ANIM)
						IF NOT IS_PED_INJURED(pedSecurity)
							SET_ENTITY_IS_TARGET_PRIORITY(pedSecurity, FALSE)
						ENDIF	
						
						TASK_PLAY_ANIM(pedSecurity, "missheist_jewel", Asset.animCowerOnFloor, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_NOT_INTERRUPTABLE)
							
						
//						TASK_PLAY_ANIM_ADVANCED(pedSecurity, Asset.adJewelHeist, Asset.animCowerOnFloor, 
//									<< -631.955, -236.478, 38.044 >>, << 0.000, 0.000, 130.250 >>,
//									INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_LOOPING)
//				
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//Revert door back to normal
	IF iRandomGuardStage > 6
		//PRINTLN("synched: ", GET_SYNCHRONIZED_SCENE_PHASE(thisSceneID))
		IF IS_SYNCHRONIZED_SCENE_RUNNING(thisSceneID)
			IF GET_SYNCHRONIZED_SCENE_PHASE(thisSceneID) >= 1.0
				IF DOES_ENTITY_EXIST(oiStoreDoorTemp)
//					SET_ENTITY_VISIBLE(oiStoreDoorMap, TRUE)
//					SET_ENTITY_COLLISION(oiStoreDoorMap, TRUE)
					REMOVE_MODEL_HIDE(<< -621.8983, -230.8202, 37.0570 >>, 10.0, P_Jewel_Door_L)
					DELETE_OBJECT(oiStoreDoorTemp)						
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
		//Say her fucking line!!!
		IF bManagerSaidHerLine = FALSE
			IF eRandomManagerStage >= MANAGER_APPEARS	
				//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				//IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				//AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				IF NOT IS_PED_INJURED(pedBackroom)
					IF NOT IS_AMBIENT_SPEECH_PLAYING(pedBackroom)
					AND GET_GAME_TIMER() - iTimeOfManagerDialogue > 2000
						SWITCH iManagerDialogue
						
							CASE 0
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBackroom, "JH_SVAA", "JEWELMANAGER", SPEECH_PARAMS_FORCE_FRONTEND)
								bManagerGodTextDisplayed = FALSE
								iManagerDialogue++
							BREAK
						
							CASE 1
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBackroom, "JH_SWAA", "JEWELMANAGER", SPEECH_PARAMS_FORCE_FRONTEND)
								iManagerDialogue++
							BREAK
							
							CASE 2
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBackroom, "JH_SXAA", "JEWELMANAGER", SPEECH_PARAMS_FORCE_FRONTEND)
								iManagerDialogue++
							BREAK
							
							CASE 3
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedBackroom, "JH_SYAA", "JEWELMANAGER", SPEECH_PARAMS_FORCE_FRONTEND)
								iManagerDialogue++
							BREAK
							
						ENDSWITCH
						
						iTimeOfManagerDialogue = GET_GAME_TIMER() 
						
					ENDIF
				ENDIF
								
				iGuardRunTimer = GET_GAME_TIMER()	//Stops the guard running out straight after manager scene
				
			ENDIF
		ENDIF
	
		PRINTLN("GET_CURRENT_SCRIPTED_CONVERSATION_LINE()", GET_CURRENT_SCRIPTED_CONVERSATION_LINE())
	
		//Check this dialogue can play onces shes down on the ground.
		//IF bManagerSaidHerLine = TRUE
		IF iManagerDialogue >= 1
		AND bManagerGodTextDisplayed = FALSE
		//AND (GET_GAME_TIMER() - iTimeOfManagerDialogue) > 3000
		
			IF IS_GUNMAN_NORM()
				PRINT_NOW("G_BACK", DEFAULT_GOD_TEXT_TIME, 1)
				bMichaelRespondsToGoodGunman = TRUE	//Michael dont respond
				bManagerGodTextDisplayed = TRUE
			ELIF IS_GUNMAN_PACKIE()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_GUNMANDWN", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
					//SCRIPT_ASSERT("packie says get down")
					bManagerGodTextDisplayed = TRUE
				ENDIF
			ELIF IS_GUNMAN_GUSTAVO()
				KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
				IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_GUNMANDWB", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
					//SCRIPT_ASSERT("Gustavo says get down")
					bManagerGodTextDisplayed = TRUE
				ENDIF
			ENDIF
						
		ENDIF
		
		IF bManagerGodTextDisplayed = TRUE
		AND bMichaelRespondsToGoodGunman = FALSE
		AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
		
			//Michael responds
			IF IS_GUNMAN_PACKIE()
				IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_GUNDWNRES", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
					//SCRIPT_ASSERT("Michael responds")
					bMichaelRespondsToGoodGunman = TRUE
				ENDIF
			ELIF IS_GUNMAN_GUSTAVO()
				IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_GUNDWN2", CONV_PRIORITY_VERY_HIGH, DO_NOT_DISPLAY_SUBTITLES)
					//SCRIPT_ASSERT("Michael responds")
					bMichaelRespondsToGoodGunman = TRUE
				ENDIF
			ENDIF
		ENDIF
	
	
	IF NOT bIsRandomManagerEventDone
		
		IF eRandomManagerStage > MANAGER_WAIT_TO_APPEAR
		AND eRandomManagerStage < MANAGER_WAIT_FOR_INTRO_TO_FINISH_2
			IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS ( <<-629.1, -230.2, 38.2>>, 2.0, V_ILEV_J2_DOOR)
				fOpenMultiplier += 0.06
				IF fOpenMultiplier > 1.0
					fOpenMultiplier  = 1.0
				ENDIF
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_J2_DOOR, <<-629.1, -230.2, 38.2>>, TRUE, -1.0 * fOpenMultiplier)
			ENDIF
		ENDIF


		SWITCH eRandomManagerStage
			
			CASE MANAGER_REQUEST_ASSETS						
				REQUEST_MODEL(Asset.mnBackroom)
				REQUEST_MODEL(Asset.mnPhone)
				eRandomManagerStage	= MANAGER_WAIT_FOR_ASSETS
			BREAK
			
			CASE MANAGER_WAIT_FOR_ASSETS
				IF HAS_MODEL_LOADED(Asset.mnBackroom)
				AND HAS_MODEL_LOADED(Asset.mnPhone)
					eRandomManagerStage	= MANAGER_WAIT_TO_APPEAR
				ENDIF
			BREAK
			
			CASE MANAGER_WAIT_TO_APPEAR
				IF bManagerPopOut
				AND bIsRandomGuardEventNow = FALSE
				AND TIMERA() < (iTimeLimit - 10000)
				AND iCurrentTake < TARGET_TAKE_VALUE		 
				
				
					bManagerSaidHerLine = FALSE
					iManagerDialogue = 0
				//IF timer > (iTimeLimit/2) // + 5000
				//AND NOT bIsPlayerLooting
				//AND NOT IS_TASK_ONGOING(pedCrew[cnSmashGunman], SCRIPT_TASK_PLAY_ANIM)
				//AND bIsRandomGuardEventDone
				//AND IS_SPHERE_VISIBLE(<<-627.7369, -231.0153, 38.1904>>, 1.0)
				//AND NOT IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_MICHAEL), << -628.0393, -230.8265, 37.0570 >>, <<1.5, 1.5, 1.5>>)
				//AND NOT IS_ENTITY_AT_COORD(pedCrew[cnSmashGunman], << -628.0393, -230.8265, 37.0570 >>, <<1.5, 1.5, 1.5>>)
				//AND NOT isAnyConversationWaitingInQueue()
					bToldToGetDown = FALSE
					
					objPhone = CREATE_OBJECT(Asset.mnPhone, << -629.7300, -228.2782, 37.0570 >>)
					pedBackroom = CREATE_PED(PEDTYPE_MISSION, Asset.mnBackroom, << -629.7300, -228.2782, 37.1570 >>, 219.6529)
					ATTACH_ENTITY_TO_ENTITY(objPhone, pedBackroom, GET_PED_BONE_INDEX(pedBackroom, BONETAG_PH_L_HAND), VECTOR_ZERO, VECTOR_ZERO)
					
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedBackroom, FALSE)
					
					SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnBackroom)
					SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnPhone)
					
					ADD_ALL_PEDS_TO_DIALOGUE()
					SET_PED_SHOP(pedBackroom)

					controlJewelStoreDoors(FALSE, 0.0)
					clearStoreBlips()
					
					blipFlee = CREATE_BLIP_FOR_PED(pedBackroom, TRUE)
										
					sceneManageress = CREATE_SYNCHRONIZED_SCENE(vManageressPosition, vManageressRotation)
					//PLAY_SYNCHRONIZED_CAM_ANIM(camAnim, sceneManageress, Asset.animCamManageressEnter, Asset.adJewelHeist)
					TASK_SYNCHRONIZED_SCENE(pedBackroom, sceneManageress, Asset.adJewelHeist, Asset.animManageressEnter, 
											INSTANT_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
					SET_SYNCHRONIZED_SCENE_LOOPED(sceneManageress, FALSE)
					//SET_CAM_ACTIVE(camAnim, TRUE)
					//RENDER_SCRIPT_CAMS(TRUE, FALSE)
					//TASK_TURN_PED_TO_FACE_ENTITY(PLAYER_PED_ID(), pedBackroom)
					
					SETTIMERB(0)
					
					IF bGuardRuns
						IF NOT IS_PED_INJURED(pedSecurity)
							FREEZE_ENTITY_POSITION(pedSecurity, TRUE)
						ENDIF
					ENDIF
					
					SET_ENTITY_IS_TARGET_PRIORITY(pedBackroom, TRUE)
					
					START_AUDIO_SCENE("JSH_2B_MANAGER_ARRIVE")

					//runConversation(Asset.convManager, TRUE, TRUE, TRUEPa
						//Manager : Of course we can customize that for you.  For real class, you can never have enough stones on the bezel. 
						//Manager : Shit. Shit. Shit. Shit. Shit.
					
					TRIGGER_MUSIC_EVENT("JH2B_SECURITY_MA")
					//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					bIsRandomManagerEventNow = TRUE
					eRandomManagerStage = MANAGER_APPEARS
				ENDIF
			BREAK
			
			CASE MANAGER_APPEARS
				//IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					//IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", Asset.convManager, CONV_PRIORITY_VERY_HIGH)
						//SCRIPT_ASSERT("manager played dialogue")
						//WAIT(0)
						eRandomManagerStage = MANAGER_WAIT_FOR_INTRO_TO_FINISH
					//ENDIF
				//ENDIF
			BREAK
			
			CASE MANAGER_WAIT_FOR_INTRO_TO_FINISH
				
				IF NOT IS_PED_INJURED(pedBackroom)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneManageress)
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneManageress) >= 0.99//0.62//0.65
							//If bad gunman tell the player to deal with the manager
							IF IS_GUNMAN_NORM()
								//runGodText("G_BACK", TRUE, TRUE, FALSE)
							ELSE
								//Else the Gunman/ will run up and aggressively say a line of dialogue.
								OPEN_SEQUENCE_TASK(seqDealWithManager)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(NULL, <<-624.8931, -234.1630, 37.0570>>, pedBackroom, PEDMOVE_RUN, FALSE)
									TASK_AIM_GUN_AT_ENTITY(NULL, pedBackroom, 7000)
									TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(NULL, <<-623.2735, -236.2186, 37.0570>>, << -622.8721, -230.6936, 37.8568 >>, PEDMOVE_RUN, FALSE)
								CLOSE_SEQUENCE_TASK(seqDealWithManager)
								TASK_PERFORM_SEQUENCE(pedCrew[cnAltGunman], seqDealWithManager)
								CLEAR_SEQUENCE_TASK(seqDealWithManager)
//								IF IS_GUNMAN_PACKIE()
//									runConversation("JH_GUNMANDWN", TRUE, TRUE, TRUE)
//								ELIF IS_GUNMAN_GUSTAVO()
//									runConversation("JH_GUNMANDWB", TRUE, TRUE, TRUE)
//								ENDIF
//								addTextToQueue("JH_GUNDWNRES", TEXT_TYPE_CONVO)
							ENDIF
							
							IF NOT IS_PED_INJURED(pedBackroom)
								SET_ENTITY_IS_TARGET_PRIORITY(pedBackroom, TRUE)
							ENDIF
							eRandomManagerStage = MANAGER_WAIT_FOR_INTRO_TO_FINISH_2
						ENDIF
					ENDIF
				ELSE
					eRandomManagerStage = MANAGER_WAIT_FOR_PLAYER_OR_GUNMAN_TO_SUBDUE
				ENDIF
			BREAK
			
			CASE MANAGER_WAIT_FOR_INTRO_TO_FINISH_2
			 	IF NOT IS_PED_INJURED(pedBackroom)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneManageress)										
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneManageress) >= 1.0//0.65
						OR bToldToGetDown
							sceneManageress = CREATE_SYNCHRONIZED_SCENE(vManageressPosition, vManageressRotation)
							TASK_SYNCHRONIZED_SCENE(pedBackroom, sceneManageress, Asset.adJewelHeist, Asset.animManageressPanicLoop, 
													NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
							SET_SYNCHRONIZED_SCENE_LOOPED(sceneManageress, TRUE)
							
							SETTIMERB(0)
							iTimeOfManagerPoppingOut = GET_GAME_TIMER()				
							IF bGuardRuns
								IF NOT IS_PED_INJURED(pedSecurity)
									FREEZE_ENTITY_POSITION(pedSecurity, FALSE)
								ENDIF
							ENDIF
							
							//runGodText("JH_GETMANAGE", TEXT_TYPE_CONVO)
							//DO_END_CUTSCENE()
							eRandomManagerStage = MANAGER_WAIT_FOR_PLAYER_OR_GUNMAN_TO_SUBDUE
						ENDIF
					ENDIF
				ELSE
					//DO_END_CUTSCENE()
					eRandomManagerStage = MANAGER_WAIT_FOR_PLAYER_OR_GUNMAN_TO_SUBDUE
				ENDIF
			BREAK
			
			CASE MANAGER_WAIT_FOR_PLAYER_OR_GUNMAN_TO_SUBDUE
				IF NOT IS_PED_INJURED(pedBackroom)
					IF NOT IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedBackroom)
					AND NOT IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedBackroom)
					AND IS_GUNMAN_NORM()
						clearStoreBlips()
						
						IF GET_GAME_TIMER() - iTimeOfManagerPoppingOut  > DEFAULT_GOD_TEXT_TIME
							sceneManageress = CREATE_SYNCHRONIZED_SCENE(vManageressPosition, vManageressRotation)
							SET_SYNCHRONIZED_SCENE_LOOPED(sceneManageress, FALSE)
							TASK_SYNCHRONIZED_SCENE(pedBackroom, sceneManageress, Asset.adJewelHeist, Asset.animManageressExit, 
													NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
							eRandomManagerStage = MANAGER_WAIT_FOR_FAIL
							
						ENDIF
					ELSE
						IF NOT IS_PED_INJURED(pedBackroom)
							TASK_LOOK_AT_ENTITY(pedBackroom, GET_SELECT_PED(SEL_MICHAEL), 3000,  SLF_WHILE_NOT_IN_FOV)
						ENDIF
						sceneManageress = CREATE_SYNCHRONIZED_SCENE(vManageressPosition, vManageressRotation)
						SET_SYNCHRONIZED_SCENE_LOOPED(sceneManageress, FALSE)
						TASK_SYNCHRONIZED_SCENE(pedBackroom, sceneManageress, Asset.adJewelHeist, Asset.animManageressKneelIntro, 
												NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
						DETACH_ENTITY(objPhone, TRUE, TRUE)
						
						REMOVE_BLIP(blipFlee)
						
						PLAY_PAIN(pedBackroom, AUD_DAMAGE_REASON_SCREAM_TERROR)
						
						eRandomManagerStage = MANAGER_KNEELS
					ENDIF
				ELSE
					REMOVE_BLIP(blipFlee)
					bIsRandomManagerEventNow = FALSE
					bIsRandomManagerEventDone = TRUE
				ENDIF
			BREAK
			
			CASE MANAGER_KNEELS
					
				IF NOT IS_PED_INJURED(pedBackroom)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneManageress)
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneManageress) > 0.6
							sceneManageress = CREATE_SYNCHRONIZED_SCENE(vManageressPosition, vManageressRotation)
							TASK_SYNCHRONIZED_SCENE(pedBackroom, sceneManageress, Asset.adJewelHeist, Asset.animManageressKneelLoop, 
													SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
							SET_SYNCHRONIZED_SCENE_LOOPED(sceneManageress, TRUE)
							IF DOES_BLIP_EXIST(blipFlee)
								REMOVE_BLIP(blipFlee)
							ENDIF

							bIsRandomManagerEventNow = FALSE
							bIsRandomManagerEventDone = TRUE
						ELIF GET_SYNCHRONIZED_SCENE_PHASE(sceneManageress) > 0.1
							IF DOES_ENTITY_EXIST(objPhone)
								IF IS_ENTITY_ATTACHED(objPhone)
									DETACH_ENTITY(objPhone, FALSE, TRUE)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELSE
					REMOVE_BLIP(blipFlee)
					bIsRandomManagerEventNow = FALSE
					bIsRandomManagerEventDone = TRUE
				ENDIF
			BREAK
						
			CASE MANAGER_WAIT_FOR_FAIL
				IF NOT IS_PED_INJURED(pedBackroom)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneManageress)
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneManageress) > 0.025
							//runConversation(Asset.convManagerFail, TRUE, TRUE, TRUE)
							SETTIMERB(0)
							eRandomManagerStage= MANAGER_WAIT_FOR_FAIL_2
						ENDIF
					ENDIF
				ELSE
					DETACH_ENTITY(objPhone, TRUE, TRUE)
										
					REMOVE_BLIP(blipFlee)
					bIsRandomManagerEventNow = FALSE
					bIsRandomManagerEventDone = TRUE
				ENDIF
			BREAK
			
			CASE MANAGER_WAIT_FOR_FAIL_2
				//IF TIMERB() > 2000
				IF NOT IS_PED_INJURED(pedBackroom)
					IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneManageress)
					
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneManageress) > 0.61
							fOpenMultiplier -= 0.06
							IF fOpenMultiplier < 0.0
								fOpenMultiplier  = 0.0
							ENDIF
						ENDIF
												
						SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_J2_DOOR, <<-629.1, -230.2, 38.2>>, TRUE, -1.0 * fOpenMultiplier)
									
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneManageress) > 0.99
							//Manager : This is Vinqueef in Portola.  We’re being robbed! 
							
							IF TIMERA() < (iTimeLimit - 10000)
							AND iCurrentTake < TARGET_TAKE_VALUE		 
								eMissionFail = FAIL_CALL_POLICE
								MissionFailed()
							ENDIF
							
						ENDIF
					ENDIF
				ELSE
					DETACH_ENTITY(objPhone, TRUE, TRUE)
					clearText()
					emptyTextQueue()
					
					REMOVE_BLIP(blipFlee)
					bIsRandomManagerEventNow = FALSE
					bIsRandomManagerEventDone = TRUE
				ENDIF
			BREAK
			
		ENDSWITCH
		
		IF DOES_ENTITY_EXIST(pedBackroom)
			
			IF TIMERA() > (iTimeLimit - 10000)
			OR iCurrentTake > TARGET_TAKE_VALUE	
				IF DOES_BLIP_EXIST(blipFlee)
					REMOVE_BLIP(blipFlee)
				ENDIF
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedBackroom)
				IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedBackroom)
				OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedBackroom)
				OR (NOT IS_GUNMAN_NORM())
					IF bToldToGetDown = FALSE
						IF IS_AUDIO_SCENE_ACTIVE("JSH_2B_MANAGER_ARRIVE")
							STOP_AUDIO_SCENE("JSH_2B_MANAGER_ARRIVE")
							emptyTextQueue()
							CLEAR_PRINTS()
						ENDIF
						
						IF IS_GUNMAN_NORM()
							KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
							IF CREATE_CONVERSATION(	myScriptedSpeech, "JHAUD",  "JH_GETDOWN", CONV_PRIORITY_VERY_HIGH)
								bToldToGetDown = TRUE
							ENDIF
						ELSE
							bToldToGetDown = TRUE
						ENDIF
					ENDIF
				ELSE
					IF eRandomManagerStage >= MANAGER_WAIT_FOR_PLAYER_OR_GUNMAN_TO_SUBDUE
					AND bToldToGetDown = FALSE
						IF NOT IS_AMBIENT_SPEECH_PLAYING(pedCrew[CREW_GUNMAN_NEW])
							SET_AUDIO_FLAG("ScriptedConvListenerMaySpeak", TRUE) 
							//SCRIPT_ASSERT("sheeeeeeeeeet")
							PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedCrew[CREW_GUNMAN_NEW], "JH_IVAA", "JEWELGUNBAD", SPEECH_PARAMS_FORCE_FRONTEND)	
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF IS_AUDIO_SCENE_ACTIVE("JSH_2B_MANAGER_ARRIVE")
					STOP_AUDIO_SCENE("JSH_2B_MANAGER_ARRIVE")
				ENDIF
			ENDIF
		ENDIF
	ELSE
	
		IF NOT IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-632.527466,-230.295547,37.057034>>, <<-628.848938,-227.644104,40.557034>>, 3.600000)
		AND NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-629.1, -230.2, 38.2>>, <<1.5, 1.5, 1.5>>)
			fOpenMultiplier -= 0.06
			IF fOpenMultiplier < 0.0
				fOpenMultiplier  = 0.0
			ENDIF						
			SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_J2_DOOR, <<-629.1, -230.2, 38.2>>, TRUE, -1.0 * fOpenMultiplier)						
		ENDIF
	ENDIF
	
ENDPROC

//PURPOSE: Sets up the mini-cutscene showing the player smashing a jewel case
PROC SETUP_PLAYER_LOOT(sJewelCase &jewelcase)
	IF DOES_BLIP_EXIST(jewelcase.blipCase)
		REMOVE_BLIP(jewelcase.blipCase)
	ENDIF
	jewelcase.bSmashed = TRUE
	bIsPlayerLooting = TRUE
	iLootStage = 0
ENDPROC


FUNC TEXT_LABEL_63 MODIFY_ANIM_FOR_FIRST_PERSON(STRING stAnim)

	TEXT_LABEL_63 fpPrefix = "FP_"
	TEXT_LABEL_63 fpString = stAnim

	IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON)
		fpPrefix += fpString
		RETURN fpPrefix
	ELSE
		RETURN fpString
	ENDIF

ENDFUNC

INT nmBlockingObject

//PURPOSE: Does the logic for the player smash a jewel case
PROC UPDATE_PLAYER_LOOT()
	VECTOR vPosition, vRotation//, vResetPos
	FLOAT fPhaseStore = -1.0
	BOOL bPlayerLootDone = FALSE
	TEXT_LABEL_63 strFPAnimName
	FLOAT fStartTime = 0.0
	
	DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)

	SWITCH iLootStage
		CASE 0
					
			IF ARE_STRINGS_EQUAL(jcStore[iNearestCase].sSmashAnim, Asset.rfSmash)
				strAnimName = Asset.animSmashNecklace
				
				strFPAnimName  = MODIFY_ANIM_FOR_FIRST_PERSON(strAnimName)				
				strAnimName = TEXT_LABEL_TO_STRING(strFPAnimName)
				
				strCamAnimName = Asset.animCamSmashNecklace
				mnPickupProp = Asset.mnJewels4a
				fSmash = 0.2//0.136 //0.204
				fGrabStart1 = 0.400
				fGrabEnd1 = 0.710
				fGrabStart2 = -1.0
				fGrabEnd2 = -1.0
				
				IF jcStore[iNearestCase].iSmashedByBulletStage > 0
					//Skip into scene
					fStartTime = 0.490
				ELSE
					//Dont skip into scene
					fStartTime = 0.0
				ENDIF
				
				
				IF iNearestCase = 0
					strCamAnimName = Asset.animCamSmashD
				ENDIF
								
				//vCaseCoords = <<-0.009296, -1.019462, -0.550665>>
				
			ELIF ARE_STRINGS_EQUAL(jcStore[iNearestCase].sSmashAnim, Asset.rfSmash2)
				strAnimName = Asset.animSmashE
				strFPAnimName  = MODIFY_ANIM_FOR_FIRST_PERSON(strAnimName)				
				strAnimName = TEXT_LABEL_TO_STRING(strFPAnimName)
				
				strCamAnimName = Asset.animCamSmashE
				
				IF iNearestCase = 4
					strCamAnimName = Asset.animCamSmashTrayA
				ENDIF
								
				mnPickupProp = Asset.mnJewels4b
				fSmash = 0.143
				fGrabStart1 = 0.625
				fGrabEnd1 = 0.786
				fGrabStart2 = -1.0
				fGrabEnd2 = -1.0
				
				IF jcStore[iNearestCase].iSmashedByBulletStage > 0
					//Skip into scene
					fStartTime = 0.319
				ELSE
					//Dont skip into scene
					fStartTime = 0.0
				ENDIF
				
				//vCaseCoords = <<-0.040149, -1.022772, -0.559593>>
				
			ELIF ARE_STRINGS_EQUAL(jcStore[iNearestCase].sSmashAnim, Asset.rfSmash3)
				strAnimName = Asset.animSmashTrayA
				strFPAnimName  = MODIFY_ANIM_FOR_FIRST_PERSON(strAnimName)				
				strAnimName = TEXT_LABEL_TO_STRING(strFPAnimName)
				
				strCamAnimName = Asset.animCamSmashTrayA
				mnPickupProp = Asset.mnJewels2
				fSmash = 0.168
				fGrabStart1 = 0.483
				fGrabEnd1 = 0.753
				fGrabStart2 = -1.0
				fGrabEnd2 = -1.0
				
				IF jcStore[iNearestCase].iSmashedByBulletStage > 0
					//Skip into scene
					fStartTime = 0.269
				ELSE
					//Dont skip into scene
					fStartTime = 0.0
				ENDIF
				
				IF iNearestCase = 5
					strCamAnimName = Asset.animCamSmashTrayB
				ENDIF
				
				//vCaseCoords = <<-0.009296, -1.019462, -0.550669>>
				
			ELIF ARE_STRINGS_EQUAL(jcStore[iNearestCase].sSmashAnim, Asset.rfSmash4)
				strAnimName = Asset.animSmashTrayB
				strFPAnimName  = MODIFY_ANIM_FOR_FIRST_PERSON(strAnimName)				
				strAnimName = TEXT_LABEL_TO_STRING(strFPAnimName)
				
				strCamAnimName = Asset.animCamSmashTrayB
				mnPickupProp = Asset.mnJewels1
				fSmash = 0.041
				fGrabStart1 = 0.415
				fGrabEnd1 = 0.738
				fGrabStart2 = -1.0
				fGrabEnd2 = -1.0
				
				IF jcStore[iNearestCase].iSmashedByBulletStage > 0
					//Skip into scene
					fStartTime = 0.25
				ELSE
					//Dont skip into scene
					fStartTime = 0.0
				ENDIF
				
				//vCaseCoords = <<-0.009296, -1.019462,-0.55067>>
				
			ENDIF
			
			SET_CURRENT_PED_WEAPON(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, TRUE)
						
			START_AUDIO_SCENE("JSH_2B_CABINET_SMASH")
						
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) <> CAM_VIEW_MODE_FIRST_PERSON
				DO_START_CUTSCENE(FALSE, TRUE, FALSE, FALSE, FALSE)
			ENDIF
			CLEAR_HELP()
//			oiCaseObject = GET_CLOSEST_OBJECT_OF_TYPE(jcStore[iNearestCase].vPlayerPos ,1.0, p_new_j_counter_02)
//			jcStore[iNearestCase].vPlayerPos = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(oiCaseObject, vCaseCoords)
//			jcStore[iNearestCase].vPlayerRot =  GET_ENTITY_ROTATION(oiCaseObject)
			
			
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) <> CAM_VIEW_MODE_FIRST_PERSON			
				sceneLeaveStore = CREATE_SYNCHRONIZED_SCENE(jcStore[iNearestCase].vPlayerPos-<<0,0,0>>, jcStore[iNearestCase].vPlayerRot)
				TASK_SYNCHRONIZED_SCENE(GET_SELECT_PED(SEL_MICHAEL), sceneLeaveStore, Asset.adJewelHeist, strAnimName, 
										INSTANT_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			ELSE
				sceneLeaveStore = CREATE_SYNCHRONIZED_SCENE(jcStore[iNearestCase].vPlayerPos-<<0,0,0>>, jcStore[iNearestCase].vPlayerRot)
				TASK_SYNCHRONIZED_SCENE(GET_SELECT_PED(SEL_MICHAEL), sceneLeaveStore, Asset.adJewelHeist, strAnimName, 
										REALLY_SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS)
			ENDIF
			
			SET_SYNCHRONIZED_SCENE_PHASE(sceneLeaveStore, FStartTime)
			PRINTLN("INIT_JEWEL_CASE(jcStore[", iNearestCase, "], ", jcStore[iNearestCase].vCasePos, jcStore[iNearestCase].vPlayerPos, ", ", jcStore[iNearestCase].vPlayerRot)

			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) <> CAM_VIEW_MODE_FIRST_PERSON
				IF VMAG(jcStore[iNearestCase].vCameraPos) = 0.0
					PLAY_SYNCHRONIZED_CAM_ANIM(camAnim, sceneLeaveStore, strCamAnimName, Asset.adJewelHeist)
					SET_CAM_ACTIVE(camAnim, TRUE)
					SET_CAM_CONTROLS_MINI_MAP_HEADING(camAnim, TRUE)
				ELSE
					SET_CAM_COORD(initialCam, jcStore[iNearestCase].vCameraPos)
					SET_CAM_ROT(initialCam, jcStore[iNearestCase].vCameraRot)
					SET_CAM_FOV(initialCam, jcStore[iNearestCase].fCameraFov)
					
					SET_CAM_COORD(destinationCam, jcStore[iNearestCase].vCameraPos2)
					SET_CAM_ROT(destinationCam, jcStore[iNearestCase].vCameraRot2)
					SET_CAM_FOV(destinationCam, jcStore[iNearestCase].fCameraFov2)
					
					SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 2500, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL)
					SET_CAM_CONTROLS_MINI_MAP_HEADING(destinationCam, TRUE)
				ENDIF
				RENDER_SCRIPT_CAMS(TRUE, FALSE)		
			ENDIF
						
			nmBlockingObject = ADD_NAVMESH_BLOCKING_OBJECT(jcStore[iNearestCase].vCasePos, <<2.0, 2.0, 2.0>>, 0.0, FALSE)
			
			
			iLootStage++
		BREAK
		CASE 1
			
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() <> CAM_VIEW_MODE_FIRST_PERSON
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedCrew[cnSmashGunman]) < 1.0
					SET_ENTITY_VISIBLE(pedCrew[cnSmashGunman], FALSE)
				ENDIF
			ENDIF
		
			DISABLE_NEAR_CLIP_SCAN_THIS_UPDATE()
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneLeaveStore)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneLeaveStore) > fSmash
				
					IF jcStore[iNearestCase].iSmashedByBulletStage = 0
						//PlaySoundFromCoord(SND_SMASH_CABINET, Asset.soundSmashCabinet, jcStore[iNearestCase].vCasePos, FALSE)
						sndList[SND_SMASH_CABINET].SoundId = GET_SOUND_ID()
						PLAY_SOUND_FROM_COORD(sndList[SND_SMASH_CABINET].SoundId, "SMASH_CABINET_PLAYER", jcStore[iNearestCase].vCasePos, "JEWEL_HEIST_SOUNDS")
						
						IF iNearestCase % 2 = 0
							REPLAY_RECORD_BACK_FOR_TIME(2.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
						ENDIF
						
						START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_jewel_cab_smash", GET_CURRENT_PED_WEAPON_ENTITY_INDEX(PLAYER_PED_ID()), vSmashParticleOffset, VSmashParticleRotation) //, BONETAG_R_HAND)
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(jcStore[iNearestCase].vCasePos, 2, P_INT_JEWEL_MIRROR)
							GET_COORDS_AND_ROTATION_OF_CLOSEST_OBJECT_OF_TYPE(	jcStore[iNearestCase].vCasePos, 2, 
																					P_INT_JEWEL_MIRROR, vPosition, vRotation)
							IF NOT DOES_ENTITY_EXIST(objGrenade)
								objGrenade =  CREATE_OBJECT(Asset.mnGrenade, vPosition)
							ENDIF
							SET_ENTITY_VISIBLE(objGrenade, FALSE)
							APPLY_FORCE_TO_ENTITY(objGrenade, APPLY_TYPE_FORCE, <<0,0,0.1>>, VECTOR_ZERO, 0, TRUE, TRUE, FALSE)
						ENDIF
					ENDIF
					iLootStage++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() <> CAM_VIEW_MODE_FIRST_PERSON
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedCrew[cnSmashGunman]) < 1.0
					SET_ENTITY_VISIBLE(pedCrew[cnSmashGunman], FALSE)
				ENDIF
			ENDIF
		
			DISABLE_NEAR_CLIP_SCAN_THIS_UPDATE()
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneLeaveStore)
				IF jcStore[iNearestCase].iSmashedByBulletStage = 0
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneLeaveStore) > fSmash	
						IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfCaseSmash) = RFMO_STATE_PRIMED
							SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfCaseSmash, RFMO_STATE_START_ANIM)						
						ENDIF
					ENDIF
				ENDIF
				IF NOT DOES_ENTITY_EXIST(objPickup)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneLeaveStore) > fGrabStart1
						objPickup = CREATE_OBJECT(mnPickupProp, jcStore[iNearestCase].vPlayerPos)
						ATTACH_ENTITY_TO_ENTITY(objPickup, GET_SELECT_PED(SEL_MICHAEL), 
												GET_PED_BONE_INDEX(GET_SELECT_PED(SEL_MICHAEL), BONETAG_PH_L_HAND), 
												VECTOR_ZERO, VECTOR_ZERO)
						IF DOES_ENTITY_EXIST(objGrenade)
							DELETE_OBJECT(objGrenade)
						ENDIF
						iCurrentTake += jcStore[iNearestCase].iWorth
						//ADD_HEIST_END_SCREEN_STOLEN_ITEM(	HEIST_JEWEL, jcStore[iNearestCase].sLootName,
													//jcStore[iNearestCase].iWorth)
												
						IF iNearestCase = 10
						OR iNearestCase = 11
						OR iNearestCase = 13
							bManagerPopOut = TRUE
						ENDIF
													
						g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE]++
						iLootStage++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() <> CAM_VIEW_MODE_FIRST_PERSON
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedCrew[cnSmashGunman]) < 1.0
					SET_ENTITY_VISIBLE(pedCrew[cnSmashGunman], FALSE)
				ENDIF
			ENDIF
		
			DISABLE_NEAR_CLIP_SCAN_THIS_UPDATE()
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneLeaveStore)
				IF fGrabStart2 < 0
					iLootStage++
				ELSE
					IF DOES_ENTITY_EXIST(objPickup)
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneLeaveStore) > fGrabEnd1
							DELETE_OBJECT(objPickup)
						ENDIF
					ELSE
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneLeaveStore) > fGrabStart2
							objPickup = CREATE_OBJECT(mnPickupProp, jcStore[iNearestCase].vPlayerPos)
							ATTACH_ENTITY_TO_ENTITY(objPickup, GET_SELECT_PED(SEL_MICHAEL), 
													GET_PED_BONE_INDEX(GET_SELECT_PED(SEL_MICHAEL), BONETAG_PH_L_HAND), 
													VECTOR_ZERO, VECTOR_ZERO)
							iLootStage++
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE 4
			IF GET_FOLLOW_PED_CAM_VIEW_MODE() <> CAM_VIEW_MODE_FIRST_PERSON
				IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedCrew[cnSmashGunman]) < 1.0
					SET_ENTITY_VISIBLE(pedCrew[cnSmashGunman], FALSE)
				ENDIF
			ENDIF
		
			DISABLE_NEAR_CLIP_SCAN_THIS_UPDATE()
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneLeaveStore)
				IF DOES_ENTITY_EXIST(objPickup)
					IF fGrabStart2 < 0
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneLeaveStore) > fGrabEnd1
							DELETE_OBJECT(objPickup)
						ENDIF
					ELSE
						IF GET_SYNCHRONIZED_SCENE_PHASE(sceneLeaveStore) > fGrabEnd2
							DELETE_OBJECT(objPickup)
						ENDIF
					ENDIF
				ENDIF
				//IF HAS_ANIM_EVENT_FIRED(GET_SELECT_PED(SEL_MICHAEL), GET_HASH_KEY("activate_upperbody"))
					fPhaseStore = GET_SYNCHRONIZED_SCENE_PHASE(sceneLeaveStore)
				IF fPhaseStore > 0.92
					bPlayerLootDone = TRUE
				ENDIF
				
				//HACK FOR CASE ZERO
				IF iNearestCase = 0
					IF fPhaseStore > 0.8
						bPlayerLootDone = TRUE
					ENDIF	
				ENDIF
				//HACK FOR CASE ZERO^^^
				
				IF HAS_SYNCHRONIZED_SCENE_FINISHED(sceneLeaveStore)
				OR (HAS_ANIM_EVENT_FIRED(GET_SELECT_PED(SEL_MICHAEL), GET_HASH_KEY("early_out"))
					AND (IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_MOVE_LR)
						OR IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_MOVE_UD)))					
					bPlayerLootDone = TRUE
				ENDIF
			ELSE
				bPlayerLootDone = TRUE
			ENDIF
				
			IF bPlayerLootDone
				IF DOES_ENTITY_EXIST(objPickup)
					DELETE_OBJECT(objPickup)
				ENDIF
				
				//If she didn't pop out in time cancel her popping out.
				//bManagerPopOut = FALSE
				
				SET_CURRENT_PED_WEAPON(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, TRUE)
				HIDE_PED_WEAPON_FOR_SCRIPTED_CUTSCENE(GET_SELECT_PED(SEL_MICHAEL), FALSE)
								
				//bStolenSomething = TRUE
				
				REMOVE_NAVMESH_BLOCKING_OBJECT(nmBlockingObject)
				
				IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) <> CAM_VIEW_MODE_FIRST_PERSON
					DO_END_CUTSCENE(FALSE, TRUE, 1500,DEFAULT,FALSE)
				ENDIF
				
				
//				vResetPos = GET_ANIM_INITIAL_OFFSET_POSITION(Asset.adJewelHeist,strAnimName, jcStore[iNearestCase].vPlayerPos, jcStore[iNearestCase].vPlayerRot )
//				GET_GROUND_Z_FOR_3D_COORD(vResetPos, vResetPos.z)
//				SET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL), vResetPos)
//				
//				//CLEAR_PED_TASKS_IMMEDIATELY(GET_SELECT_PED(SEL_MICHAEL))
//				
				CLEAR_PED_TASKS(GET_SELECT_PED(SEL_MICHAEL))
			
				SET_ENTITY_VISIBLE(pedCrew[cnSmashGunman], TRUE)							
//				
//				
//				RECORD_BROKEN_GLASS(vResetPos, 2.0)
//				
				IF fPhaseStore > 0.0
					//TASK_PLAY_ANIM(	GET_SELECT_PED(SEL_MICHAEL), Asset.adJewelHeist, strAnimName, INSTANT_BLEND_IN, SLOW_BLEND_OUT,
					//				-1, AF_SECONDARY | AF_UPPERBODY | AF_NOT_INTERRUPTABLE, fPhaseStore)
				ENDIF
				IMMEDIATELY_UPDATE_ANIMS(GET_SELECT_PED(SEL_MICHAEL))
				
				STOP_AUDIO_SCENE("JSH_2B_CABINET_SMASH")
				
				//iTotalSmashedCases++
				bIsPlayerLooting = FALSE
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC


//PURPOSE: Using the phase as a percentage of the distance between the two points given, returns a point between the two. 0<fPhase<1
FUNC VECTOR GET_PHASE_POINT_BETWEEN_TWO_POINTS(VECTOR vPoint0, VECTOR vPoint1, FLOAT fPhase)
	IF fPhase > 1
		fPhase = 1
	ELIF fPhase < 0
		fPhase = 0
	ENDIF
	VECTOR vReturn
	vReturn.x = vPoint0.x + ((vPoint1.x-vPoint0.x)*fPhase)
	vReturn.y = vPoint0.y + ((vPoint1.y-vPoint0.y)*fPhase)
	vReturn.z = vPoint0.z + ((vPoint1.z-vPoint0.z)*fPhase)
	
	RETURN vReturn
ENDFUNC

//PURPOSE: Makes a peds point his gun at the store peds in sequence
PROC DO_PED_CROWD_CONTROL(PED_INDEX thisPed, sCrowdControl &CrowdControl)
	INT iRandomTarget = 0
	BOOL bFoundGoodTarget = FALSE
	CrowdControl.iTimer += GET_GAME_TIMER() - CrowdControl.iTimeLastFrame
	CrowdControl.iTimeLastFrame = GET_GAME_TIMER()
	
	IF CrowdControl.iTimer > CrowdControl.iTimerLimit
		CrowdControl.iTimer = 0
		CrowdControl.iTimerLimit = 1500 + GET_RANDOM_INT_IN_RANGE(0, 500)
		CrowdControl.vLastTarget = CrowdControl.vNextTarget
		
		IF IS_PED_INJURED(pedStore[0])
		AND IS_PED_INJURED(pedStore[1])
		AND IS_PED_INJURED(pedStore[2])
		AND IS_PED_INJURED(pedStore[3])
		AND IS_PED_INJURED(pedStore[4])
		AND IS_PED_INJURED(pedSecurity)
		AND IS_PED_INJURED(pedBackroom)
			CrowdControl.vNextTarget = GET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL))
			bFoundGoodTarget = TRUE
		ELSE
				
			IF NOT bFoundGoodTarget
				iRandomTarget = GET_RANDOM_INT_IN_RANGE(0, 100)
				
				IF bIsRandomManagerEventNow
					iRandomTarget = 1
				ENDIF
				IF iRandomTarget > 87
					IF DOES_ENTITY_EXIST(pedSecurity)
					AND NOT IS_PED_INJURED(pedSecurity)
						CrowdControl.vNextTarget = GET_ENTITY_COORDS(pedSecurity)
						bFoundGoodTarget = TRUE
					ENDIF
				ELIF iRandomTarget > 74
					IF DOES_ENTITY_EXIST(pedStore[0])
					AND NOT IS_PED_INJURED(pedStore[0])
						CrowdControl.vNextTarget = GET_ENTITY_COORDS(pedStore[0])
						bFoundGoodTarget = TRUE
					ENDIF
				ELIF iRandomTarget > 61
					IF DOES_ENTITY_EXIST(pedStore[1])
					AND NOT IS_PED_INJURED(pedStore[1])
						CrowdControl.vNextTarget = GET_ENTITY_COORDS(pedStore[1])
						bFoundGoodTarget = TRUE
					ENDIF
				ELIF iRandomTarget > 48
					IF DOES_ENTITY_EXIST(pedStore[2])
					AND NOT IS_PED_INJURED(pedStore[2])
						CrowdControl.vNextTarget = GET_ENTITY_COORDS(pedStore[2])
						bFoundGoodTarget = TRUE
					ENDIF
				ELIF iRandomTarget > 35
					IF DOES_ENTITY_EXIST(pedStore[3])
					AND NOT IS_PED_INJURED(pedStore[3])
						CrowdControl.vNextTarget = GET_ENTITY_COORDS(pedStore[3])
						bFoundGoodTarget = TRUE
					ENDIF
				ELIF iRandomTarget > 22
					IF DOES_ENTITY_EXIST(pedStore[4])
					AND NOT IS_PED_INJURED(pedStore[4])
						CrowdControl.vNextTarget = GET_ENTITY_COORDS(pedStore[4])
						bFoundGoodTarget = TRUE
					ENDIF
				ELIF iRandomTarget > 0
					IF DOES_ENTITY_EXIST(pedBackroom)
					AND NOT IS_PED_INJURED(pedBackroom)
						CrowdControl.vNextTarget = GET_ENTITY_COORDS(pedBackroom)
						bFoundGoodTarget = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT bIsRandomGuardEventNow AND NOT bIsRandomManagerEventNow
			
			IF IS_GUNMAN_PACKIE()
				IF NOT isTextWaitingInQueue("JH_STAYDWNGD")
					IF NOT bConversationOngoing AND NOT bGodTextOngoing
						RunConversation("JH_STAYDWNGD", TRUE, TRUE, TRUE)
									//Crew:	Down! Get fuckin' down.
					ENDIF
				ENDIF			
			ELIF IS_GUNMAN_GUSTAVO()
				IF NOT isTextWaitingInQueue("JH_CTRL")
					IF NOT bConversationOngoing AND NOT bGodTextOngoing
						RunConversation("JH_CTRL", TRUE, TRUE, TRUE)
									//Crew:	Down! Get fuckin' down.
					ENDIF
				ENDIF
			ENDIF
			
		ENDIF
	ELSE		
		IF NOT bIsHackTimeUp
		AND NOT (g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE] >= iNoOfCases-1)
			TASK_AIM_GUN_AT_COORD(thisPed, GET_PHASE_POINT_BETWEEN_TWO_POINTS(	CrowdControl.vLastTarget, CrowdControl.vNextTarget, 
																		TO_FLOAT(CrowdControl.iTimer)/CrowdControl.iTimerLimit), 1000)
		ENDIF
	ENDIF

	
ENDPROC

INT sceneBuddyLoot

BOOL bNotFirstCase = FALSE

//PURPOSE: Moves a ped to pick the farthest away jewel case from the player, walk to it and smash it
PROC UPDATE_BUDDY_LOOT_NEW(PED_INDEX Looter,  sLootControl &LootControl) //, VEHICLE_INDEX EscapeVehicle)	

	VECTOR vPosition, vRotation
	VECTOR vGlassPos
	FLOAT fStartTime = 0.0

	#IF IS_DEBUG_BUILD
		IF bIsSuperDebugEnabled
			FOR iCounter = 0 TO iNoOfCases-1
		
				TEXT_LABEL_63 stNumber = ""
				stNumber += iCounter
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
				DRAW_DEBUG_TEXT_ABOVE_COORDS(jcStore[iCounter].vCasePos, stNumber, 1.0)
			
			ENDFOR
		ENDIF
	#ENDIF
	SWITCH iBuddyLootStage
	
		CASE 0
			
			//If we dont have any cases left lets get the fuck out of here
			IF g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE] >= iNoOfCases-1
			AND NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneBuddyLoot)	//Dont stop half way through last case
				iBuddyLootStage = 100
			ELSE
			
				//Let's start to smash a new case but only if the security guard and the manager aren't in action.
				IF (bIsRandomManagerEventNow = TRUE
				OR bIsRandomGuardEventNow = TRUE)		
				AND bIsHackTimeUp  = FALSE
					//Do crowd control
					iBuddyLootStage = 99
				ELSE
					//Otherwise choose the case furthest from the player and lets go there...
					
					IF bNotFirstCase = FALSE
						//First case the guy will smash, avoids bumping into player.
												
						IF bIsStealthPath
							iFarthestCase = 8
						ELSE
							iFarthestCase = 6
						ENDIF
						
						bNotFirstCase = TRUE
					ELSE
						findFurthestCase()
					ENDIF
					
					IF iFarthestCase <> -1
						iCurrentBuddyCase = iFarthestCase	
						
						IF jcStore[iCurrentBuddyCase].iSmashedByBulletStage = 0
							rfCaseAISmash = GET_RAYFIRE_MAP_OBJECT(	jcStore[iCurrentBuddyCase].vCasePos, fRFCheckRadius, 
																	jcStore[iCurrentBuddyCase].sSmashAnim)
							SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfCaseAISmash, RFMO_STATE_PRIMING)
						ENDIF
						
						iBuddyLootStage = 1
					ENDIF
				ENDIF
			
			
			ENDIF
			
		BREAK
		
		CASE 1
			
			IF iCurrentBuddyCase = iNearestCase
			
				PRINTLN("reselecting case! Same one as player!")
				CLEAR_PED_TASKS(Looter)
				iBuddyLootStage = 0 //Reselect case.	
			ELSE
		
				IF HAS_PED_ACHIEVED_COORD_HEAD(Looter, jcStore[iCurrentBuddyCase].vPlayerPos, 
													GET_HEADING_BETWEEN_VECTORS_2D(jcStore[iCurrentBuddyCase].vPlayerPos, 
													jcStore[iCurrentBuddyCase].vCasePos),
													//GET_HEADING_FROM_VECTOR_2D(	jcStore[iCurrentBuddyCase].vPlayerRot.x,
													//							jcStore[iCurrentBuddyCase].vPlayerRot.y),
													PEDMOVE_RUN, 0.5, 10)
						
					IF jcStore[iCurrentBuddyCase].bSmashed = TRUE
						SCRIPT_ASSERT("whoa this is smashed already!")
					ENDIF
															
					IF ARE_STRINGS_EQUAL(jcStore[iCurrentBuddyCase].sSmashAnim, Asset.rfSmash)
						strBuddyAnimName = Asset.animSmashNecklaceSkull
						mnBuddyPickupProp = Asset.mnJewels2
						fBuddySmash = 0.204
						fBuddyGrabStart = 0.300
						fBuddyGrabEnd = 0.691
						
						IF jcStore[iCurrentBuddyCase].iSmashedByBulletStage > 0
							//Skip into scene
							fStartTime = 0.490
						ELSE
							//Dont skip into scene
							fStartTime = 0.0
						ENDIF
						
					ELIF ARE_STRINGS_EQUAL(jcStore[iCurrentBuddyCase].sSmashAnim, Asset.rfSmash2)
						strBuddyAnimName = Asset.animSmashE
						mnBuddyPickupProp = Asset.mnJewels4b
						fBuddySmash = 0.143
						fBuddyGrabStart = 0.625
						fBuddyGrabEnd = 0.786
						
						IF jcStore[iCurrentBuddyCase].iSmashedByBulletStage > 0
						//Skip into scene
						fStartTime = 0.319
					ELSE
						//Dont skip into scene
						fStartTime = 0.0
					ENDIF
						
					ELIF ARE_STRINGS_EQUAL(jcStore[iCurrentBuddyCase].sSmashAnim, Asset.rfSmash3)
						strBuddyAnimName = Asset.animSmashTrayA
						mnBuddyPickupProp = Asset.mnJewels2
						fBuddySmash = 0.168
						fBuddyGrabStart = 0.483
						fBuddyGrabEnd = 0.753
						
						IF jcStore[iCurrentBuddyCase].iSmashedByBulletStage > 0
							//Skip into scene
							fStartTime = 0.269
						ELSE
							//Dont skip into scene
							fStartTime = 0.0
						ENDIF
						
					ELIF ARE_STRINGS_EQUAL(jcStore[iCurrentBuddyCase].sSmashAnim, Asset.rfSmash4)
						strBuddyAnimName = Asset.animSmashTrayB
						mnBuddyPickupProp = Asset.mnJewels1
						fBuddySmash = 0.051
						fBuddyGrabStart = 0.415
						fBuddyGrabEnd = 0.738
						
						IF jcStore[iCurrentBuddyCase].iSmashedByBulletStage > 0
							//Skip into scene
							fStartTime = 0.25
						ELSE
							//Dont skip into scene
							fStartTime = 0.0
						ENDIF
						
					ENDIF
					
					SET_CURRENT_PED_WEAPON(Looter, mnMainWeaponType, TRUE)
					IF DOES_BLIP_EXIST(jcStore[iCurrentBuddyCase].blipCase)
						REMOVE_BLIP(jcStore[iCurrentBuddyCase].blipCase)
					ENDIF
					
					sceneBuddyLoot = CREATE_SYNCHRONIZED_SCENE(jcStore[iCurrentBuddyCase].vPlayerPos, jcStore[iCurrentBuddyCase].vPlayerRot)
					TASK_SYNCHRONIZED_SCENE(Looter, sceneBuddyLoot, Asset.adJewelHeist, strBuddyAnimName, 
										SLOW_BLEND_IN, SLOW_BLEND_OUT, SYNCED_SCENE_USE_PHYSICS, RBF_NONE, SLOW_BLEND_IN)
					SET_SYNCHRONIZED_SCENE_PHASE(sceneBuddyLoot, FStartTime)
			
					SET_SYNCHRONIZED_SCENE_LOOPED(sceneBuddyLoot, FALSE)
					
					
					jcStore[iCurrentBuddyCase].bSmashed = TRUE
					//iTotalSmashedCases++
					
					PRINTLN("BUddy smashing case: ", iCurrentBuddyCase)
										
					//ADD_HEIST_END_SCREEN_STOLEN_ITEM(HEIST_JEWEL, jcStore[iCurrentBuddyCase].sLootName,
													// ROUND(jcStore[iCurrentBuddyCase].iWorth * fCrewSmashMultiplier))
					g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE]++									 
					iBuddyLootStage++	
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneBuddyLoot)			
				IF NOT DOES_ENTITY_EXIST(LootControl.objGrab)
					IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBuddyLoot) > fBuddyGrabStart
					AND GET_SYNCHRONIZED_SCENE_PHASE(sceneBuddyLoot) < fBuddyGrabEnd
						LootControl.objGrab = CREATE_OBJECT(mnBuddyPickupProp, jcStore[iCurrentBuddyCase].vPlayerPos)
						ATTACH_ENTITY_TO_ENTITY(LootControl.objGrab, Looter, 
												GET_PED_BONE_INDEX(Looter, BONETAG_PH_L_HAND), 
												VECTOR_ZERO, VECTOR_ZERO)
						IF DOES_ENTITY_EXIST(objGrenade)
							DELETE_OBJECT(objGrenade)
						ENDIF
					ENDIF
				ELIF GET_SYNCHRONIZED_SCENE_PHASE(sceneBuddyLoot) > fBuddyGrabEnd
					DELETE_OBJECT(LootControl.objGrab)
				ENDIF
				
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBuddyLoot) > fBuddySmash
					
					IF jcStore[iCurrentBuddyCase].iSmashedByBulletStage = 0
					
						IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfCaseAISmash) = RFMO_STATE_PRIMED
							SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfCaseAISmash, RFMO_STATE_START_ANIM)
							//PlaySoundFromCoord(	SND_SMASH_CABINET_NPC, Asset.soundSmashCabinetNPC, 
							//					jcStore[iCurrentBuddyCase].vCasePos, FALSE)
							sndList[SND_SMASH_CABINET_NPC].SoundId = GET_SOUND_ID()
							PLAY_SOUND_FROM_COORD(sndList[SND_SMASH_CABINET_NPC].SoundId, "SMASH_CABINET_NPC", jcStore[iCurrentBuddyCase].vCasePos, "JEWEL_HEIST_SOUNDS")
							
							IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(jcStore[iCurrentBuddyCase].vCasePos, 2, P_INT_JEWEL_MIRROR)
								GET_COORDS_AND_ROTATION_OF_CLOSEST_OBJECT_OF_TYPE(	jcStore[iCurrentBuddyCase].vCasePos, 2, 
																					P_INT_JEWEL_MIRROR, vPosition, vRotation)
								IF NOT DOES_ENTITY_EXIST(objGrenade)
									objGrenade =  CREATE_OBJECT(Asset.mnGrenade, vPosition)
								ENDIF
								SET_ENTITY_VISIBLE(objGrenade, FALSE)
								APPLY_FORCE_TO_ENTITY(objGrenade, APPLY_TYPE_FORCE, <<0,0,0.1>>, VECTOR_ZERO, 0, TRUE, TRUE, FALSE)
							ENDIF
							
							vGlassPos = GET_ANIM_INITIAL_OFFSET_POSITION(Asset.adJewelHeist,strBuddyAnimName, jcStore[iCurrentBuddyCase].vPlayerPos, jcStore[iCurrentBuddyCase].vPlayerRot )
							GET_GROUND_Z_FOR_3D_COORD(vGlassPos, vGlassPos.z)
									
							RECORD_BROKEN_GLASS(vGlassPos, 2.0)
				
							
						ENDIF
						
					ENDIF
					iBuddyLootStage++	
				ENDIF	
			ENDIF
		BREAK
		
		CASE 3
			IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBuddyLoot) > 0.8
				//Increase the take
				iCurrentTake += ROUND(jcStore[iCurrentBuddyCase].iWorth * fCrewSmashMultiplier)
				iBuddyLootStage++	
			ENDIF
		BREAK
		
		CASE 4
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sceneBuddyLoot)
				IF GET_SYNCHRONIZED_SCENE_PHASE(sceneBuddyLoot) >= 1.0						
					//SCRIPT_ASSERT("clean up buddy smash")
					iBuddyLootStage = 0
					CLEAR_PED_TASKS(Looter)
				ENDIF
			ELSE
				//SCRIPT_ASSERT("buddy quit synced scene")
				PRINTLN("buddy quit synced scene")
				iBuddyLootStage = 0
			ENDIF
		BREAK
		
		CASE 99
			
			IF bIsRandomManagerEventNow = FALSE
			AND (iRandomGuardStage = 99  OR bIsRandomGuardEventNow = FALSE)
				iBuddyLootStage = 0	
			ENDIF
			
			DO_PED_CROWD_CONTROL(Looter, ccCrew2)
			
		BREAK
		
		CASE 100
		
			IF  NOT IS_SYNCHRONIZED_SCENE_RUNNING(sceneBuddyLoot) //NOT IS_TASK_ONGOING(	Looter, SCRIPT_TASK_PLAY_ANIM)
				IF DOES_ENTITY_EXIST(LootControl.objGrab)
					DELETE_OBJECT(LootControl.objGrab)
				ENDIF
				
				IF NOT IS_TASK_ONGOING(Looter, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
				AND NOT IS_TASK_ONGOING(Looter, SCRIPT_TASK_ENTER_VEHICLE)	
				AND NOT IS_PED_IN_ANY_VEHICLE(Looter)
					TASK_FOLLOW_NAV_MESH_TO_COORD(Looter, vJewelryExitCoords, PEDMOVE_RUN)
					
					iBuddyLootStage++
				ENDIF					
			ENDIF
		
		BREAK
	ENDSWITCH
	
	
ENDPROC


//PURPOSE:	Counts down wait time before playing text queue entry
PROC RESET_LOOT_CONTROL_STRUCT(sLootControl &LootControl)
	lcCrew.iTargetCase							= -1
	IF DOES_ENTITY_EXIST(LootControl.objGrab)
		DELETE_OBJECT(LootControl.objGrab)
	ENDIF
ENDPROC

//PURPOSE:	Counts down wait time before playing text queue entry
PROC RESET_CROWD_CONTROL_STRUCT(sCrowdControl &CrowdControl)
	CrowdControl.iTimer = 0
	CrowdControl.iTimeLastFrame = GET_GAME_TIMER()
	CrowdControl.iTimerLimit = 1000
	IF NOT IS_PED_INJURED(pedStore[0])
		CrowdControl.vLastTarget = GET_ENTITY_COORDS(pedStore[0])
	ELSE
		CrowdControl.vLastTarget = VECTOR_ZERO
	ENDIF
	IF NOT IS_PED_INJURED(pedStore[1])
		CrowdControl.vNextTarget = GET_ENTITY_COORDS(pedStore[1])
	ELSE
		CrowdControl.vNextTarget = VECTOR_ZERO
	ENDIF
ENDPROC


//PURPOSE: Once Driver has entered Vehicle, unfreezes and sets ped and vehicle properties
PROC doBikeEntryLogic(PED_INDEX Driver, VEHICLE_INDEX Vehicle)
	IF NOT IS_ENTITY_DEAD(Driver)
	AND NOT IS_ENTITY_DEAD(Vehicle)
		IF IS_PED_IN_VEHICLE(Driver, Vehicle)
			CLEAR_PED_TASKS(Driver)
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(Driver, KNOCKOFFVEHICLE_NEVER)
			//SET_PED_CAN_RAGDOLL(Driver, FALSE)
			//SET_ENTITY_PROOFS(Driver, TRUE, TRUE, TRUE, TRUE, TRUE)
			SET_PED_CAN_BE_DRAGGED_OUT(Driver, FALSE)
			SET_ENTITY_PROOFS(Vehicle, FALSE, TRUE, TRUE, TRUE, TRUE)
		FREEZE_ENTITY_POSITION(Vehicle, FALSE)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Clears store area of peds
PROC CLEAR_STORE_AREA_OF_PEDS()
	//SET_PED_DENSITY_MULTIPLIER(0)
	CLEAR_AREA_OF_PEDS(<< -625.1706, -232.8408, 39.4937 >>, 100)
		
ENDPROC

INT iCopSmashSound
INT iTimeOfLastSmash

//PURPOSE:	Makes vehicle A vulnerable to being flipped when in contact with vehicle B
FUNC BOOL HAS_VEHICLE_BEEN_FLIPPED_BY_VEHICLE(	VEHICLE_INDEX vehicleA, VEHICLE_INDEX vehicleB, FLOAT fVehicleAWidth, FLOAT fVehicleBWidth,
												BOOL bEasyFlip)
	
	IF REQUEST_SCRIPT_AUDIO_BANK("JWL_HEIST_CAR_SMASHES_BIG")
	AND IS_VEHICLE_DRIVEABLE(vehicleA)
		IF IS_VEHICLE_DRIVEABLE(vehicleB)
			IF IS_ENTITY_TOUCHING_ENTITY(vehicleB, vehicleA)
			AND NOT IS_ENTITY_IN_AIR(vehicleA)
			
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicleA)
					STOP_PLAYBACK_RECORDED_VEHICLE(vehicleA)
				ENDIF
				
				VECTOR vColLoc = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehicleA, GET_ENTITY_COORDS(vehicleB))
				VECTOR vForce = VECTOR_ZERO
				VECTOR vOffset = VECTOR_ZERO
				
				VECTOR vVehicleBVel = GET_ENTITY_SPEED_VECTOR(vehicleB)
				VECTOR vVehicleAVel = GET_ENTITY_SPEED_VECTOR(vehicleA)
				FLOAT fTruckVel = GET_ENTITY_SPEED(vehicleB)
				FLOAT fPoliceVel = GET_ENTITY_SPEED(vehicleA)
				
				BOOL bHasSuitableContactBeenMade = FALSE
				
				INT iColLoc = 0
				IF ABSF(vColLoc.x) > fVehicleAWidth + fVehicleBWidth
					iColLoc = 0 //(Side)
				ELIF vColLoc.y > 0
					iColLoc = 1 //(Front)
				ELSE
					iColLoc = 2 //(Back)
				ENDIf
				
				FLOAT fForceStrength
				FLOAT fTargetVel
				IF bEasyFlip
					fForceStrength = 0.6
					fTargetVel = 5.0
				ELSE
					fForceStrength = 1.4
					fTargetVel = 15.0
				ENDIF
				 

				SWITCH iColLoc
					CASE 0 //(Side)
						IF GET_ANGLE_BETWEEN_2D_VECTORS(vVehicleBVel.x, vVehicleBVel.y, vVehicleAVel.x, vVehicleAVel.y) > 2.5
						AND fTruckVel > fTargetVel
							vForce.x = -vColLoc.x
							vForce.y = 0
							vForce.z = 0
							vForce = NORMALISE_VECTOR(vForce)
							vForce.x *= fForceStrength
							vForce.z = fForceStrength
							
							vOffset.x = vColLoc.x
							vOffset.y = 0
							vOffset.z = 0
							
							IF vColLoc.x>0
								SMASH_VEHICLE_WINDOW(vehicleA, SC_WINDOW_FRONT_RIGHT)
								SMASH_VEHICLE_WINDOW(vehicleA, SC_WINDOW_REAR_RIGHT)
							ELSE
								SMASH_VEHICLE_WINDOW(vehicleA, SC_WINDOW_FRONT_LEFT)
								SMASH_VEHICLE_WINDOW(vehicleA, SC_WINDOW_REAR_LEFT)
							ENDIF
							PRINTLN("***************************SIDE HIT*****************************")
							bHasSuitableContactBeenMade = TRUE
						ENDIF
					BREAK
					CASE 1 //(Front)
						PRINTLN("***************************FRONT HIT*****************************")
						IF fPoliceVel > fTargetVel
							vForce.x = 0
							vForce.y = -vColLoc.y
							vForce.z = 0
							vForce = NORMALISE_VECTOR(vForce)
							vForce.y *= fForceStrength
							vForce.z = 0
							
							vOffset.x = vColLoc.x/2//0
							vOffset.y = vColLoc.y
							vOffset.z = 0
							PRINTLN("***************************BACK HIT*****************************")
							SMASH_VEHICLE_WINDOW(vehicleA, SC_WINDOW_FRONT_RIGHT)
							SMASH_VEHICLE_WINDOW(vehicleA, SC_WINDOW_FRONT_LEFT)
							bHasSuitableContactBeenMade = TRUE
						ENDIF
					BREAK
					CASE 2 //(Back)
						IF fTruckVel > fTargetVel
							vForce.x = -vColLoc.x
							vForce.y = 0
							vForce.z = 0
							vForce = NORMALISE_VECTOR(vForce)
							vForce.x *= fForceStrength
							vForce.z = 0
							
							vOffset.x = vColLoc.x//0
							vOffset.y = vColLoc.y/2
							vOffset.z = 0
							PRINTLN("***************************BACK HIT*****************************")
							SMASH_VEHICLE_WINDOW(vehicleA, SC_WINDOW_REAR_RIGHT)
							SMASH_VEHICLE_WINDOW(vehicleA, SC_WINDOW_REAR_LEFT)
							bHasSuitableContactBeenMade = TRUE
						ENDIF
					BREAK
				ENDSWITCH
				
				IF bHasSuitableContactBeenMade
					SET_VEHICLE_DAMAGE(vehicleA, vColLoc, 20.0, 100.0, TRUE)
					IF vOffset.x > 1.7
						vOffset.x = 1.7
					ENDIF
					IF vOffset.x < -1.7
						vOffset.x = -1.7
					ENDIF
										
					IF vOffset.y > 1.7
						vOffset.y = 1.7
					ENDIF
					IF vOffset.y < -1.7
						vOffset.y = -1.7
					ENDIF
					
					IF vOffset.z > 1.7
						vOffset.z = 1.7
					ENDIF
					IF vOffset.z < -1.7
						vOffset.z = -1.7
					ENDIF
					vForce = vForce * 0.72
					
					//APPLY_FORCE_TO_ENTITY(	vehicleA, APPLY_TYPE_EXTERNAL_IMPULSE, vForce, vOffset, 0, TRUE, TRUE, TRUE)
					
					IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						ADD_NON_CRITICAL_STANDARD_CONVERSATION_TO_BUFFER(myScriptedSpeech, "JHAUD", "JH_SMASHCOPS", CONV_PRIORITY_VERY_HIGH)
					ENDIF
										
//					INT iRandomDoor = GET_RANDOM_INT_IN_RANGE(0, 5)
//					IF NOT IS_VEHICLE_DOOR_DAMAGED(vehicleA, INT_TO_ENUM(SC_DOOR_LIST, iRandomDoor))
//						SET_VEHICLE_DOOR_BROKEN(vehicleA, INT_TO_ENUM(SC_DOOR_LIST, iRandomDoor), FALSE)
//					ENDIF
										
					IF NOT IS_ENTITY_DEAD(GET_PED_IN_VEHICLE_SEAT(vehicleA, VS_DRIVER))
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(GET_PED_IN_VEHICLE_SEAT(vehicleA, VS_DRIVER), FALSE)
						GIVE_WEAPON_TO_PED(GET_PED_IN_VEHICLE_SEAT(vehicleA, VS_DRIVER), WEAPONTYPE_PISTOL, INFINITE_AMMO)
						TASK_COMBAT_HATED_TARGETS_AROUND_PED(GET_PED_IN_VEHICLE_SEAT(vehicleA, VS_DRIVER), 200.0)
						SET_PED_COMBAT_ATTRIBUTES(GET_PED_IN_VEHICLE_SEAT(vehicleA, VS_DRIVER), CA_USE_VEHICLE, FALSE)
					ENDIF
										
					IF GET_GAME_TIMER() - iTimeOfLastSmash > 2500					
						
						REPLAY_RECORD_BACK_FOR_TIME(3.0)
						
						iCopSmashSound = GET_SOUND_ID()
					
						PLAY_SOUND_FROM_ENTITY(iCopSmashSound, "Destroy_Cop_Car", vehicleA, "JEWEL_HEIST_SOUNDS")
						iTimeOfLastSmash = GET_GAME_TIMER()							
					ENDIF
					
					IF GET_VEHICLE_ENGINE_HEALTH(vehicleA) > 100
						SET_VEHICLE_ENGINE_HEALTH(vehicleA, 20)
					ENDIF
					
					IF GET_VEHICLE_PETROL_TANK_HEALTH(vehicleA) > 100
						SET_VEHICLE_PETROL_TANK_HEALTH(vehicleA, 75)
					ENDIF
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE:	Has the vehicle entered the storage compartment of the truck
FUNC BOOL HAS_VEHICLE_MOUNTED_TRUCK(VEHICLE_INDEX thisVehicle)
	IF IS_POINT_IN_ANGLED_AREA(	GET_ENTITY_COORDS(thisVehicle),
								GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTruck, <<-0.035128,0.555196,0.113579>>),
								GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTruck, <<0.031168,-3.357668,2.159969>>), 2.375000)
			SET_VEHICLE_ENGINE_ON(thisVehicle, FALSE, TRUE)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE:	Has the Ped entered the storage compartment of the truck
FUNC BOOL HAS_PED_MOUNTED_TRUCK(PED_INDEX thisPed)
	IF IS_POINT_IN_ANGLED_AREA(	GET_ENTITY_COORDS(thisPed),
								GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTruck, <<-0.035128,0.555196,0.113579>>),
								GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTruck, <<0.031168,-3.357668,2.159969>>), 2.375000)
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE:	Sets the ped onto the vehicle while cancelling any drive to tasks
PROC SET_VEHICLE_FIGHT_COORD(VEHICLE_INDEX thisVehicle, PED_INDEX thisPed, VECTOR theseCoords, FLOAT thisHeading)
	IF IS_VEHICLE_DRIVEABLE(thisVehicle)
		IF NOT IS_PED_INJURED(thisPed)
			IF NOT IS_PED_SITTING_IN_VEHICLE(thisPed, thisVehicle)
				SET_PED_INTO_VEHICLE(thisPed, thisVehicle)
			ENDIF
			SET_ENTITY_COORDS(thisVehicle, theseCoords)
			SET_ENTITY_HEADING(thisVehicle, thisHeading)
			SET_VEHICLE_ON_GROUND_PROPERLY(thisVehicle)
			
			CLEAR_PED_DRIVE_TASKS(thisPed, thisVehicle)
		ENDIF
	ENDIF
ENDPROC

//PURPOSE: Sets whether the vehicle ramp is open or closed
PROC SET_RAMP_OPEN(VEHICLE_INDEX thisVehicle, BOOL bTrue, FLOAT fOpenRatio = 1.0)
	IF bTrue
		SET_VEHICLE_DOOR_LATCHED(thisVehicle, SC_DOOR_BOOT, FALSE, FALSE)
		//SET_VEHICLE_DOOR_OPEN(thisVehicle, SC_DOOR_BOOT)
		SET_VEHICLE_DOOR_CONTROL(thisVehicle, SC_DOOR_BOOT, DT_DOOR_SWINGING_FREE, fOpenRatio)
	ELSE
		SET_VEHICLE_DOOR_SHUT(thisVehicle, SC_DOOR_BOOT, FALSE)
		
		//SET_VEHICLE_DOOR_CONTROL(thisVehicle, SC_DOOR_BOOT, DT_DOOR_INTACT, 0.0)
		
	ENDIF
ENDPROC

//═════════════════════════════════╡ Stage FUNCTIONS AND PROCEDURES ╞═══════════════════════════════════

BOOL bPlayerStartedAsMichael

//PURPOSE:		Initialise Mission
PROC stageInitMission()
	
//	VECTOR vCarSizeMin, vCarSizeMax
//	VEHICLE_INDEX viPlayersLastCar
//	
//	IF DOES_ENTITY_EXIST(GET_PLAYERS_LAST_VEHICLE())
//		GET_MODEL_DIMENSIONS(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()), vCarSizeMin, vCarSizeMax)
//		
//		viPlayersLastCar = GET_PLAYERS_LAST_VEHICLE()
//	
//		IF (vCarSizeMax.x - vCarSizeMin.x) < 4.0
//		AND (vCarSizeMax.y - vCarSizeMin.y) < 8.0
//		AND NOT IS_THIS_MODEL_A_HELI(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
//		AND NOT IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
//		AND NOT IS_THIS_MODEL_A_PLANE(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
//		AND NOT IS_THIS_MODEL_A_TRAIN(GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()))
//		AND NOT (GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()) = TAXI)
//		AND (GET_VEHICLE_MAX_NUMBER_OF_PASSENGERS(GET_PLAYERS_LAST_VEHICLE()) >= 2)
//		AND NOT IS_VEHICLE_A_PLAYER_PERSONAL_VEHICLE(GET_PLAYERS_LAST_VEHICLE())
//		
//			PRINTLN("Dimensions suitable",(vCarSizeMax.x - vCarSizeMin.x), " : ",  (vCarSizeMax.y - vCarSizeMin.y))
//			vehCar = viPlayersLastCar
//			
//			Asset.mnCar = GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE())
//			
//			SET_ENTITY_AS_MISSION_ENTITY(vehCar)
//		ELSE
//			IF NOT IS_ENTITY_DEAD(viPlayersLastCar)
//				SET_ENTITY_COORDS(viPlayersLastCar,  <<711.2791, -979.8723, 23.1111>>)
//				SET_ENTITY_HEADING(viPlayersLastCar, 223.3972)
//			ENDIF
//			PRINTLN("Dimensions NOT suitable")
//			
//		ENDIF
//		
//	ENDIF

	//SHUTDOWN_LOADING_SCREEN()

	IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
		bPlayerStartedAsMichael = TRUE
	ELSE
		bPlayerStartedAsMichael = FALSE
	ENDIF
	
	IF bIsStealthPath
		mnMainWeaponType = WEAPONTYPE_ASSAULTRIFLE
	ELSE
		mnMainWeaponType = WEAPONTYPE_CARBINERIFLE
	ENDIF
	
	SET_AUDIO_FLAG("ScriptedConvListenerMaySpeak", TRUE) 
	
	REMOVE_SCENARIO_BLOCKING_AREAS()
	ADD_SCENARIO_BLOCKING_AREA(<<713.7754, -996.4443, 22.3085>>, <<715.7624, -991.7067, 25.6214>>)
	
	//ADD_SCENARIO_BLOCKING_AREA(<<-676.6546, -251.4005, 34.4454>>, <<-618.4500, -207.1253, 66.3651>>)
	ADD_SCENARIO_BLOCKING_AREA(<<-636.599548,-241.569046,42.108574>> - <<8.750000,9.500000,5.750000>>, <<-603.6248, -208.9597, 53.7091>>)

	//By back of shop
	ADD_SCENARIO_BLOCKING_AREA(<<-586.8279, -310.8990, 27.7631>>, <<-574.8138, -244.2133, 41.2733>>)
	
	ADD_SCENARIO_BLOCKING_AREA(<<-669.312866,-227.735291,37.381374>> - <<14.750000,2.250000,1.500000>>, <<-669.312866,-227.735291,37.381374>> + <<14.750000,2.250000,1.500000>>)
	SET_PED_NON_CREATION_AREA(<<-669.312866,-227.735291,37.381374>> - <<14.750000,2.250000,1.500000>>, <<-669.312866,-227.735291,37.381374>> + <<14.750000,2.250000,1.500000>>)
	//SET_ROADS_IN_AREA(<<730.1697, -1066.6226, 11.6503>>, <<820.9736, -939.9012, 51.0636>>, FALSE)
	
	
//	IF NOT IS_DOOR_REGISTERED_WITH_SYSTEM(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE))
//		ADD_DOOR_TO_SYSTEM(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE),V_)
//	ENDIF
	
	REGISTER_SCRIPT_WITH_AUDIO()
	CreateAssetList()
	CREATE_NEED_LIST()
	SetupArrays()
	SET_PLAYER_PED_DATA_IN_CUTSCENES(FALSE, FALSE)		
	SET_PLAYER_CAN_CHANGE_CLOTHES_ON_MISSION(FALSE)
	DELETE_ALL_TRAINS()
	DISABLE_TAXI_HAILING(TRUE)
//	#IF IS_DEBUG_BUILD
//		DEBUG_DO_CREW_SELECTION_FOR_HEIST(HEIST_JEWEL)
//	#ENDIF
//	
	//REQUEST_MODELS_FOR_HEIST_CREW(HEIST_JEWEL)   
			
	SAFE_REQUEST_ADDITIONAL_TEXT(sTextBlock, MISSION_TEXT_SLOT, TRUE, TRUE)
	
	SETUP_CREW_CHOICE_CONSEQUENCES()
	
	//ADD_RELATIONSHIP_GROUP("CREW", RELGROUPHASH_PLAYER)		
	// +++ Group Creation +++
	ADD_RELATIONSHIP_GROUP("SHOP", grpShop)
	ADD_RELATIONSHIP_GROUP("COPPERS", grpCop)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_IGNORE , RELGROUPHASH_PLAYER, grpShop)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE , RELGROUPHASH_PLAYER, grpCop)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE , grpCop, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE , grpCop, grpShop)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE , grpShop, RELGROUPHASH_PLAYER)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE , grpShop, grpCop)				// --- Group Creation ---
	
	IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))										// +++ Ped Creation +++
		IF IS_PED_DUCKING(GET_SELECT_PED(SEL_MICHAEL))
			SET_PED_DUCKING(GET_SELECT_PED(SEL_MICHAEL), FALSE)
		ENDIF
	ENDIF
			
	CLEAR_AREA_OF_PEDS(vMichaelWalkToCoords, 2.0)				//Clear any Lesters hanging around
	//CLEAR_AREA_OF_VEHICLES(vMichaelWalkToCoords, 50)
					
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
	
	CLEAR_PED_WETNESS(PLAYER_PED_ID())
	CLEAR_PED_BLOOD_DAMAGE(PLAYER_PED_ID())
	RESET_PED_VISIBLE_DAMAGE(PLAYER_PED_ID())
	
	SUPPRESS_EMERGENCY_CALLS()
	
	bHasChangedClothesMichael = GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL)  //ensure bChange is set to the state of "player has changed outfit” at the start of the mission
	bHasChangedClothesFranklin = GET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_FRANKLIN)  //ensure bChange is set to the state of "player has changed outfit” at the start of the mission
	
	IF bIsStealthPath
		ADD_TCMODIFIER_OVERRIDE ("NEW_Jewel","NEW_jewel_EXIT")
	ENDIF
		
ENDPROC

structTimelapse JH2Timelapse

//PURPOSE:		Performs the timelapse if you start the missino out of hours
FUNC BOOL stageTimeLapse()

	HIDE_HUD_AND_RADAR_THIS_FRAME()


	//Reduce the player to a walk so it flows better with the cut starting.
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<703.629761,-965.670105,29.407249>>, <<719.132935,-964.400696,31.645329>>, 12.250000)
			SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
		ENDIF
	ENDIF
		

	IF DO_TIMELAPSE(SP_HEIST_JEWELRY_2, JH2Timelapse, TRUE, FALSE)
		iStageSection = 0
		SETUP_PLAYER_PEDS_SUITS(FALSE)
		RETURN TRUE
		//emissionstage = STAGE_CUTSCENE_BEGIN
	ELSE
		IF iStageSection = 0 AND DOES_CAM_EXIST(JH2Timelapse.splineCamera) AND IS_CAM_ACTIVE(JH2Timelapse.splineCamera)
			IF IS_CAM_RENDERING(JH2Timelapse.splineCamera)
				IF JH2Timelapse.iTimeSkipStage >= 1 
					SHUTDOWN_LOADING_SCREEN()
					IF IS_SCREEN_FADED_OUT()
						SET_ENTITY_COORDS(PLAYER_PED_ID(), <<702.9835, -984.5344, 23.0969>>)
						LOAD_SCENE(<<718.7215, -977.4476, 36.5705>>)				
						DO_SCREEN_FADE_IN(500)
					ENDIF
					iStageSection++
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	
	RETURN FALSE
	
ENDFUNC


INT iWhichRandomIdle
INT iTimeOfLastIdle
INT iGuysIdles

STRING stAnim
STRING stAnimGuys

PROC UPDATE_LESTERS_IDLE_ANIMS()



	SWITCH iStageLestersIdle
			
		CASE 0
			REQUEST_ANIM_DICT("MISSHeist_JewelLester_waitidles")
			REQUEST_ANIM_DICT("move_characters@lester@STD_CaneUp")
			IF HAS_ANIM_DICT_LOADED("MISSHeist_JewelLester_waitidles")
			AND HAS_ANIM_DICT_LOADED("move_characters@lester@STD_CaneUp")
				IF NOT IS_PED_INJURED(pedLester)
					stAnim = "LESTER_WAITIDLE_1"
					TASK_PLAY_ANIM(pedLester, "move_characters@lester@STD_CaneUp", "idle_outro", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_HOLD_LAST_FRAME)
									
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedLester, TRUE)
					iStageLestersIdle++
				ENDIF
			ENDIF
		BREAK
		
		
		CASE 1
		
						
			IF DOES_ENTITY_EXIST(pedLester)
				IF NOT IS_PED_INJURED(pedLester)
			
					IF IS_ENTITY_PLAYING_ANIM(pedLester, "MISSHeist_JewelLester_waitidles", stAnim)
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedLester, "MISSHeist_JewelLester_waitidles", stAnim) >= 0.99
						
							bIsAnimFinished = TRUE
						ENDIF
					ENDIF
					IF IS_ENTITY_PLAYING_ANIM(pedLester,  "move_characters@lester@STD_CaneUp", "idle_outro")
						IF GET_ENTITY_ANIM_CURRENT_TIME(pedLester,  "move_characters@lester@STD_CaneUp", "idle_outro") >= 0.99
							REMOVE_ANIM_DICT("move_characters@lester@STD_CaneUp")
							bIsAnimFinished = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF	
			
			IF bIsAnimFinished = TRUE
			
				iWhichRandomIdle  = GET_RANDOM_INT_IN_RANGE(0, 5)
		
				SWITCH iWhichRandomIdle
				
					CASE 0
						stAnim = "LESTER_WAITIDLE_1"
					BREAK
					
					CASE 1
						stAnim = "LESTER_WAITIDLE_2"
					BREAK
					
					CASE 2
						stAnim = "LESTER_WAITIDLE_3"
					BREAK
				
					CASE 3
						stAnim = "LESTER_WAITIDLE_4"
					BREAK
					
					CASE 4
						stAnim = "LESTER_WAITIDLE_5"
					BREAK
				ENDSWITCH
				
				TASK_PLAY_ANIM(pedLester, "MISSHeist_JewelLester_waitidles", stAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
				bIsAnimFinished = FALSE
			ENDIF
				
				
			
		BREAK
	ENDSWITCH
	
	INT iRandomCrew
	
	SWITCH iGuysIdles
	
		CASE 0
			//Stream in dictionaries.
			//REQUEST_ANIM_DICT("MOVE_M@BRAVE")
			REQUEST_ANIM_DICT("move_m@generic_variations@idle@b")
			iGuysIdles++
		BREAK
	
		CASE 1
			//IF HAS_ANIM_DICT_LOADED("MOVE_M@BRAVE")
			IF HAS_ANIM_DICT_LOADED("move_m@generic_variations@idle@b")
				stAnimGuys = "idle_e"
				iGuysIdles++
			ENDIF
		BREAK
	
		CASE 2
		
			IF  GET_GAME_TIMER() - iTimeOfLastIdle > 2000
				iRandomCrew = GET_RANDOM_INT_IN_RANGE(0, 4)
			
				//Get random guy
				IF DOES_ENTITY_EXIST(pedCrew[iRandomCrew])
					IF NOT IS_PED_INJURED(pedCrew[iRandomCrew])
						IF NOT IS_ENTITY_PLAYING_ANIM(pedCrew[iRandomCrew], "move_m@generic_variations@idle@b", "idle_e")
						AND NOT IS_ENTITY_PLAYING_ANIM(pedCrew[iRandomCrew], "move_m@generic_variations@idle@b", "idle_f")
						AND NOT IS_ENTITY_PLAYING_ANIM(pedCrew[iRandomCrew], "move_m@generic_variations@idle@b", "idle_g")
						AND NOT IS_ENTITY_PLAYING_ANIM(pedCrew[iRandomCrew], "move_m@generic_variations@idle@b", "idle_h")
						AND NOT IS_ENTITY_PLAYING_ANIM(pedCrew[iRandomCrew], "move_m@generic_variations@idle@b", "idle_i")
							
							iWhichRandomIdle  = GET_RANDOM_INT_IN_RANGE(0, 3)
							SWITCH iWhichRandomIdle
							
								CASE 0
									stAnimGuys = "idle_e"
								BREAK
								
//								CASE 1
//									stAnimGuys = "idle_f"
//								BREAK
								
								CASE 1
									stAnimGuys = "idle_g"
								BREAK
								
//								CASE 2
//									stAnimGuys = "idle_h"
//								BREAK
								
								CASE 2
									stAnimGuys = "idle_i"
								BREAK
							ENDSWITCH
							TASK_CLEAR_LOOK_AT(pedCrew[iRandomCrew])
							TASK_PLAY_ANIM(pedCrew[iRandomCrew], "move_m@generic_variations@idle@b", stAnimGuys, SLOW_BLEND_IN, SLOW_BLEND_OUT, -1)
							iTimeOfLastIdle = GET_GAME_TIMER()
						ELSE
							TASK_LOOK_AT_ENTITY(pedCrew[iRandomCrew], PLAYER_PED_ID(), 4000)
						ENDIF
					ENDIF
				ENDIF
				
			ENDIF	
		BREAK
	
	ENDSWITCH

ENDPROC


//BOOL bHaveAllStreamingRequestsCompleted = FALSE

BOOL bIntroSwitchEffect = FALSE

//PURPOSE:		Performs the cutscene where Franklin drops a grenade into the store from the roof
FUNC BOOL stageCutsceneBegin()

	HIDE_HUD_AND_RADAR_THIS_FRAME()
	HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)

	//Reduce the player to a walk so it flows better with the cut starting.
	IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<703.629761,-965.670105,29.407249>>, <<719.132935,-964.400696,31.645329>>, 12.250000)
		SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
	ENDIF

//â•â•â•â•â•â•â•â•â•â•¡ SETUP â•žâ•â•â•â•â•â•â•â•â•
	IF NOT bStageSetup
		bIntroSwitchEffect = FALSE
		DISPLAY_RADAR(FALSE)
		
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), FALSE)
	
		PIN_HEIST_BOARD_IN_MEMORY(HEIST_JEWEL, TRUE)
			
		//SAFE_REQUEST_INTERIOR_AT_COORDS(sweatShop, << 716.5068, -966.4983, 29.1884 >>, TRUE, FALSE)
		//UNPIN_INTERIOR(sweatShop)
		
		RESET_NEED_LIST()
		REQUEST_ANIM_DICT("MISSHEIST_JEWEL")
		MANAGE_LOADING_NEED_LIST()
		
		
		IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			IF NOT IS_PED_A_PLAYER(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
				DELETE_PED(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
			ENDIF
		ENDIF
								
		SET_CURRENT_SELECTOR_PED(SELECTOR_PED_MICHAEL)
				
		UPDATE_SELECTOR_HUD(sSelectorPeds)
		
		IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN			
			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<0.0, 3.0, 0.0>>)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
				WAIT(0)
			ENDWHILE
		ELSE
			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, <<0.0, 3.0, 0.0>>)
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
				WAIT(0)
			ENDWHILE
		ENDIF
				
		//REQUEST_MODELS_FOR_HEIST_CREW(HEIST_JEWEL)
		
		IF bIsStealthPath	
			//SAFE_REQUEST_MOCAP_CUTSCENE(Asset.mocapIntroA, FALSE, TRUE)
			
			//SETUP_PLAYER_PEDS_SUITS(FALSE, TRUE, TRUE)
						
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(Asset.mocapIntroA, CS_SECTION_1 | CS_SECTION_2 | CS_SECTION_3 | CS_SECTION_7)
			ELSE		
				REQUEST_CUTSCENE_WITH_PLAYBACK_LIST(Asset.mocapIntroA, CS_SECTION_4 | CS_SECTION_5 | CS_SECTION_6 | CS_SECTION_7)
			ENDIF
				
			WHILE NOT (HAS_CUTSCENE_LOADED() AND HAS_HEIST_BOARD_LOADED(HEIST_JEWEL))
			
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
	
				safeWait(0)
				
				PRINTSTRING("ITS TRUE for F")
				
				SET_CUTSCENE_PED_OUTFIT("Michael", PLAYER_ZERO, OUTFIT_P0_EXTERMINATOR)
				
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,0), 0, 0)  //(head)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,2), 0, 0)  //(hair)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,3), 1, 0)  //(uppr)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,4), 1, 0)  //(lowr)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,5), 0, 0)  //(hand)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,7), 0, 0)  //(teef)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,8), 0, 0)  //(accs)
				SET_CUTSCENE_PED_PROP_VARIATION("Lester", INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
				
			
				SET_CUTSCENE_PED_OUTFIT("Franklin", PLAYER_ONE, OUTFIT_P1_EXTERMINATOR)
			
				SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("Driver_selection", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_DRIVER_NEW)), CREW_OUTFIT_PEST_CONTROL)
				SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("gunman_selection_1", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_GUNMAN_NEW)), CREW_OUTFIT_PEST_CONTROL)
				SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("hacker_selection", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_HACKER)), CREW_OUTFIT_DEFAULT)
				
				//SET_PED_COMP_ITEM_CURRENT_SP (PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P1_  , FALSE)
			
			ENDWHILE
			
		ELSE
		
			//SETUP_PLAYER_PEDS_SUITS(FALSE, TRUE, FALSE)
		
			//SAFE_REQUEST_MOCAP_CUTSCENE(Asset.mocapIntroB, FALSE, TRUE)
			IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
				REQUEST_CUTSCENE(Asset.mocapIntroB)
			ELSE		
				REQUEST_CUTSCENE(Asset.mocapIntroB)	
			ENDIF
						
			WHILE NOT (HAS_CUTSCENE_LOADED() AND HAS_HEIST_BOARD_LOADED(HEIST_JEWEL))
			
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
											
				safeWait(0)
				
				//SET_CUTSCENE_PED_OUTFIT("Michael", PLAYER_ZERO, OUTFIT_P0_BLACK_SUIT)
																
				PRINTSTRING("ITS TRUE for M")
					
					
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,0), 0, 0)  //(head)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,2), 0, 0)  //(hair)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,3), 1, 0)  //(uppr)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,4), 1, 0)  //(lowr)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,5), 0, 0)  //(hand)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,7), 0, 0)  //(teef)
				SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,8), 0, 0)  //(accs)
				SET_CUTSCENE_PED_PROP_VARIATION("Lester", INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
			
				
				SET_CUTSCENE_PED_OUTFIT("Michael", PLAYER_ZERO, OUTFIT_P0_JEWEL_HEIST)
				
				SET_CUTSCENE_PED_OUTFIT("Franklin", PLAYER_ONE, OUTFIT_P1_SUIT_1)
											
				SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("Driver_selection", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_DRIVER_NEW)), CREW_OUTFIT_SUIT)
				SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("gunman_selection_1", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_GUNMAN_NEW)), CREW_OUTFIT_SUIT)
				SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("hacker_selection", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_HACKER)), CREW_OUTFIT_DEFAULT)
			
			ENDWHILE
		ENDIF
		
		WHILE NOT stageTimeLapse()
			WAIT(0)
		ENDWHILE
		
		IMMEDIATELY_LOAD_NEED_LIST_MODEL(Asset.mnTruck, TRUE)
		IMMEDIATELY_LOAD_NEED_LIST_MODEL(Asset.mnVan, TRUE)
		CLEAR_AREA_OF_VEHICLES(vMichaelWalkToCoords, 50)
		vehTruck = CREATE_VEHICLE(	Asset.mnTruck, vTruckSpawnCoords, fTruckSpawnHeading)
		//[MF]Remove all randomly assigned extras from the vehicle (B* 1689820)
		REMOVE_ALL_VEHICLE_EXTRAS(vehTruck)

		SET_VEHICLE_COLOURS(vehTruck, 128, 3) SET_VEHICLE_EXTRA(vehTruck, 6, FALSE) //SET_VEHICLE_LIVERY(vehTruck, 1)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehTruck, TRUE)
		
		IF bIsStealthPath					
			IF NOT DOES_ENTITY_EXIST(vehCar)
				REQUEST_MODEL(PRIMO)
				WHILE NOT HAS_MODEL_LOADED(PRIMO)  // CREATE_PLAYER_VEHICLE(vehCar, CHAR_MICHAEL, vCarSpawnCoords, fCarSpawnHeading)
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
					safeWait(0)
					//Reduce the player to a walk so it flows better with the cut starting.
					IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<703.629761,-965.670105,29.407249>>, <<719.132935,-964.400696,31.645329>>, 12.250000)
						SET_PED_MAX_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
					ENDIF
				ENDWHILE
				vehCar = CREATE_VEHICLE(PRIMO, vCarSpawnCoords, fCarSpawnHeading)
				SET_VEHICLE_COLOURS(vehCar, 0, 0)
			ELSE
				IF NOT IS_ENTITY_DEAD(vehCar)
					SET_ENTITY_COORDS(vehCar, vCarSpawnCoords)
					SET_ENTITY_HEADING(vehCar, fCarSpawnHeading)
				ENDIF
			ENDIF
			vehVan = CREATE_VEHICLE(Asset.mnVan, vVanSpawnCoords, fVanSpawnHeading)
		ELSE	
			
			IF NOT IS_ENTITY_DEAD(vehCar)
				SET_ENTITY_COORDS(vehCar,  <<711.2791, -979.8723, 23.1111>>)
				SET_ENTITY_HEADING(vehCar, 223.3972)
			ENDIF
			vehVan = CREATE_VEHICLE(Asset.mnVan, vCarSpawnCoords, fCarSpawnHeading)		
		ENDIF
		
		SETUP_VAN()
		
		IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
			NEW_LOAD_SCENE_START_SPHERE(<< 716.5068, -966.4983, 29.1884 >>, 10.0)
			SETTIMERA(0)
			WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
			AND TIMERA() < 10000
				WAIT(0)
			ENDWHILE
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				NEW_LOAD_SCENE_STOP()
			ENDIF
		ENDIF
				
		DISABLE_TAXI_HAILING(TRUE)
		
		
		SET_DOOR_STATE(DOORNAME_SWEATSHOP_OFFICE, DOORSTATE_FORCE_CLOSED_THIS_FRAME)
		
		SET_DOOR_STATE(DOORNAME_SWEATSHOP_OFFICE, DOORSTATE_UNLOCKED)
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE), 0.8, TRUE, TRUE)
	
		DOOR_SYSTEM_SET_OPEN_RATIO(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE),-1.0, FALSE, FALSE)
		DOOR_SYSTEM_SET_DOOR_STATE(ENUM_TO_INT(DOORHASH_SWEATSHOP_OFFICE), DOORSTATE_FORCE_UNLOCKED_THIS_FRAME, FALSE, TRUE)
	
	
		CLEAR_AREA_OF_VEHICLES(vCarSpawnCoords, 120.0)	
		SET_ALL_VEHICLE_GENERATORS_ACTIVE_IN_AREA(<< 719.4675, -989.3281, 22.3075 >>, << 691.5195, -972.6179, 26.9428 >>, FALSE)
		
		SETTIMERA(0)
		
		
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
		iStageSection = 0
		bCameraShotSetup = FALSE
		bStageSetup = TRUE
		println("bStageSetup = TRUE")
//â•â•â•â•â•â•â•â•â•â•¡ UPDATE â•žâ•â•â•â•â•â•â•â•â•		
	ELSE	
		SWITCH iStageSection
			CASE 0

				
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_SS_DOOR8, <<716.8, -975.4, 25.0>>, FALSE, 0.0)
				SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_SS_DOOR7, <<719.4, -975.4, 25.0>>, FALSE, 0.0)
								
				REGISTER_SELECT_PEDS_FOR_CUTSCENE(TRUE, FALSE, FALSE)
				//REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO, CEO_REMOVE_BODY_BLOOD_DAMAGE)
				
				IF NOT IS_ENTITY_DEAD(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN])
					REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ONE, CEO_REMOVE_BODY_BLOOD_DAMAGE)
				ENDIF
				REGISTER_ENTITY_FOR_CUTSCENE(pedCrew[CREW_GUNMAN_NEW], "gunman_selection_1", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, Asset.mnGunman)
				REGISTER_ENTITY_FOR_CUTSCENE(pedCrew[CREW_DRIVER_NEW], "Driver_selection", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, Asset.mnDriver)

				REGISTER_ENTITY_FOR_CUTSCENE(pedCrew[CREW_HACKER] , "Hacker_selection", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, Asset.mnHacker)
				REGISTER_ENTITY_FOR_CUTSCENE(pedLester, "Lester", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, Asset.mnLester)
						
				IF bIsStealthPath
					REGISTER_ENTITY_FOR_CUTSCENE(NULL,"seamstress_near_michael", CU_DONT_ANIMATE_ENTITY, S_F_Y_SWEATSHOP_01)
					REGISTER_ENTITY_FOR_CUTSCENE(NULL,"seamstress_near_door", CU_DONT_ANIMATE_ENTITY, S_F_Y_SWEATSHOP_01)
				ENDIF
				
				REGISTER_ENTITY_FOR_CUTSCENE(oiLestersStick, "WalkingStick_lester", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, asset.mnLestersStick)
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				CREATE_PINNED_HEIST_BOARD(HEIST_JEWEL)
				
				iStageSection++				
			BREAK
			CASE 1
			CASE 2
				
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
				
				IF IS_CUTSCENE_PLAYING()
				AND iStageSection = 1
					
					SAFE_FADE_IN()
					SHUTDOWN_LOADING_SCREEN()
					
//					IF NOT bIsStealthPath
//						SETUP_PLAYER_PEDS_SUITS(FALSE, TRUE, TRUE)
//					ENDIF
					
					IF vehCar <> GET_PLAYERS_LAST_VEHICLE()
						IF NOT IS_ENTITY_DEAD(GET_PLAYERS_LAST_VEHICLE())
							IF GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()) <> BURRITO2
							AND GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()) <> BOXVILLE3
							AND GET_ENTITY_MODEL(GET_PLAYERS_LAST_VEHICLE()) <> FBI2
								REPOSITION_PLAYERS_VEHICLE(<< 706.8325, -980.0989, 23.1208 >>, 221.3399)
							ENDIF
						ENDIF
					ENDIF
					
					CLEAR_AREA(vCarSpawnCoords, 100.00, TRUE, FALSE)
					CLEAR_AREA(VECTOR_ZERO, 5000, TRUE, TRUE)
					SET_CLOCK_TIME(7, 00, 00)
					SET_TODS_CUTSCENE_RUNNING(JH2Timelapse, FALSE)
					
					
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					iStageSection = 2
				ENDIF
						
				
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester"))
				AND DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Hacker_selection"))
				AND DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Driver_selection"))
				AND DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("gunman_selection_1"))
				AND DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("WalkingStick_lester"))				
				
					pedLester = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Lester"))
					
					pedCrew[CREW_HACKER]  = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Hacker_selection"))
					pedCrew[CREW_DRIVER_NEW] = CREATE_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,  ENUM_TO_INT(CREW_DRIVER_NEW), <<100.0, 100.0, 100.0>>, 0.0)
					pedCrew[CREW_GUNMAN_NEW] = CREATE_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,  ENUM_TO_INT(CREW_GUNMAN_NEW), <<100.0, 100.0, 100.0>>, 0.0)
					DELETE_PED(pedCrew[CREW_DRIVER_NEW])
					DELETE_PED(pedCrew[CREW_GUNMAN_NEW])
					pedCrew[CREW_DRIVER_NEW] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Driver_selection"))
					pedCrew[CREW_GUNMAN_NEW] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("gunman_selection_1"))
					
					oiLestersStick = GET_OBJECT_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("WalkingStick_lester")) 
						
					//Franklin
					SET_PED_COMP_ITEM_CURRENT_SP(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], COMP_TYPE_PROPS, PROPS_P1_HEADSET)
					
					SET_PED_HEADGEAR(pedCrew[CREW_DRIVER_NEW], FALSE)
					SET_PED_HEADGEAR(pedCrew[CREW_GUNMAN_NEW], FALSE)
					//SETUP_PLAYER_PEDS_SUITS()
					//Michael
					
					IF NOT bIsStealthPath
												
						IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
														
							MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
							TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
						ENDIF		
					ENDIF
										
					iStageSection= 3 
				ENDIF
			BREAK
			
			
			CASE 3
			
							
									
				IF WAS_CUTSCENE_SKIPPED()
					RESET_GAME_CAMERA()
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				
					RESET_GAME_CAMERA()
					//iStageSection = 4
				ENDIF
								
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Lester")
					UPDATE_LESTERS_IDLE_ANIMS()
					IF NOT IS_ENTITY_DEAD(pedLester)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedLester)
						//oiLestersStick = CREATE_OBJECT(asset.mnLestersStick, GET_ENTITY_COORDS(pedLester, FALSE))
						ATTACH_ENTITY_TO_ENTITY(oiLestersStick, pedLester, GET_PED_BONE_INDEX(pedLester, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
					ENDIF
				ENDIF
				
				IF NOT bIsStealthPath
					
					IF bIntroSwitchEffect = FALSE
						IF GET_CUTSCENE_TIME() > 70432.32327
							IF bPlayerStartedAsMichael
								DO_SWITCH_EFFECT(SEL_FRANKLIN)
								bIntroSwitchEffect = TRUE
							ENDIF
						ENDIF
					ENDIF
					
//					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael"))
//						sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Michael"))
//					ENDIF
					
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
						IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_MICHAEL))
							TASK_FOLLOW_NAV_MESH_TO_COORD(GET_SELECT_PED(SEL_MICHAEL), << 713.2115, -962.9461, 29.4335 >>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP, DEFAULT_NAVMESH_RADIUS, ENAV_DEFAULT, 190.70)	
							FORCE_PED_MOTION_STATE(GET_SELECT_PED(SEL_MICHAEL), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
						ENDIF
					ENDIF
								
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")
					//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
											
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
//						HAS_PED_ACHIEVED_COORD_HEAD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], << 713.2115, -962.9461, 29.4335 >>, 190.7033,
//											PEDMOVE_WALK, 1.0, 15)
					
						iStageSection = 4
					ENDIF		
					
				ELSE
						
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Franklin")			
						IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
							SET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN), <<705.3368, -965.3012, 29.3953>>)
							SET_ENTITY_HEADING(GET_SELECT_PED(SEL_FRANKLIN), 275.2882)
						ENDIF
					ENDIF
								
								
					IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
						IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
							IF NOT bPlayerStartedAsMichael
								DO_SWITCH_EFFECT(SEL_MICHAEL)
							ENDIF
							MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
							TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
							SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
							PRINTLN("seeee thomas")
						ENDIF
						
						IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
							CLEAR_PED_TASKS(GET_SELECT_PED(SEL_FRANKLIN))
						ENDIF
						
						SET_ENTITY_HEADING(PLAYER_PED_ID(), 260.4394)
						
						IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
							SET_ENTITY_COORDS(pedCrew[CREW_DRIVER_NEW], <<706.6033, -966.9639, 29.4179>>)
							SET_ENTITY_HEADING(pedCrew[CREW_DRIVER_NEW], 7.1260)
						ENDIF
							
						IF bIsStealthPath
							SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
							SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 1500)
							FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
						ENDIF
						iStageSection = 4
					ENDIF
					
				ENDIF		
			
			BREAK
			
		ENDSWITCH
				
		//SET_PED_MIN_MOVE_BLEND_RATIO(PLAYER_PED_ID(), PEDMOVE_WALK)
		
				
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 4
		//AND NOT IS_CUTSCENE_PLAYING()
			IF bIsStealthPath
				IF NOT IS_PED_INJURED(pedCrew[CREW_HACKER])
					SET_ENTITY_COORDS(pedCrew[CREW_HACKER], <<705.5753, -966.6228, 29.3953>>)
					SET_ENTITY_HEADING(pedCrew[CREW_HACKER], 309.9281)
				ENDIF
			ENDIF
			
			REPLAY_STOP_EVENT()
			
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			PIN_HEIST_BOARD_IN_MEMORY(HEIST_JEWEL, FALSE)
			ADD_ALL_PEDS_TO_DIALOGUE()
							
			SET_PED_BAG(pedCrew[CREW_DRIVER_NEW], FALSE)
			SET_PED_BAG(pedCrew[CREW_GUNMAN_NEW], FALSE)
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
				SET_PED_BAG(GET_SELECT_PED(SEL_FRANKLIN), FALSE)
			ENDIF
			SET_PED_BAG(GET_SELECT_PED(SEL_MICHAEL), FALSE)
			SETUP_CREW_SUITS()
			SETUP_PLAYER_PEDS_SUITS()
			ADD_ALL_PEDS_TO_DIALOGUE()
						
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
				SET_PED_CREW(GET_SELECT_PED(SEL_MICHAEL))
			ENDIF
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
				SET_PED_CREW(GET_SELECT_PED(SEL_FRANKLIN))
				TASK_LOOK_AT_ENTITY(GET_SELECT_PED(SEL_FRANKLIN), GET_SELECT_PED(SEL_MICHAEL), INFINITE_TASK_TIME)
			ENDIF
			IF NOT IS_PED_INJURED(pedLester)
				SET_PED_CREW(pedLester)
				//TASK_LOOK_AT_ENTITY(pedLester, PLAYER_PED_ID(), INFINITE_TASK_TIME)
			ENDIF
			IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
				//SET_ENTITY_COORDS(pedCrew[CREW_DRIVER_NEW], <<705.9694, -966.9971, 29.4177>>)
				//SET_ENTITY_HEADING(pedCrew[CREW_DRIVER_NEW], 348.1387 )
				SET_PED_CREW(pedCrew[CREW_DRIVER_NEW])
				TASK_LOOK_AT_ENTITY(pedCrew[CREW_DRIVER_NEW], PLAYER_PED_ID(), INFINITE_TASK_TIME)
			ENDIF
			IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
				SET_PED_CREW(pedCrew[CREW_GUNMAN_NEW])
				TASK_LOOK_AT_ENTITY(pedCrew[CREW_GUNMAN_NEW], PLAYER_PED_ID(), INFINITE_TASK_TIME)
			ENDIF
			IF NOT IS_PED_INJURED(pedCrew[CREW_HACKER])
				//SET_ENTITY_COORDS(pedCrew[CREW_HACKER], << 705.4003, -966.2020, 29.3954 >>)
				//SET_ENTITY_HEADING(pedCrew[CREW_HACKER], 300.8608)
				SET_PED_CREW(pedCrew[CREW_HACKER])
				TASK_LOOK_AT_ENTITY(pedCrew[CREW_HACKER], PLAYER_PED_ID(), INFINITE_TASK_TIME)
			ENDIF
			IF NOT IS_PED_INJURED(pedCrew[CREW_HACKER])
				SET_PED_CREW(pedCrew[CREW_HACKER])
				SAFE_REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL, TRUE, TRUE)
				IF NOT IS_PED_INJURED(pedCrew[CREW_HACKER])
					GIVE_WEAPON_TO_PED(pedCrew[CREW_HACKER], WEAPONTYPE_PISTOL, 1000)
				ENDIF
				REMOVE_WEAPON_ASSET(WEAPONTYPE_PISTOL)
				SET_PED_ACCURACY(pedCrew[CREW_HACKER], 50)
				TASK_LOOK_AT_ENTITY(pedCrew[CREW_HACKER], PLAYER_PED_ID(), INFINITE_TASK_TIME)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(vehVan)
				SET_VEHICLE_HAS_STRONG_AXLES(vehVan, TRUE)
			ENDIF
			
			IF DOES_ENTITY_EXIST(vehCar)
				IF NOT IS_ENTITY_DEAD(vehCar)
					SET_VEHICLE_HAS_STRONG_AXLES(vehVan, TRUE)
				ENDIF
			ENDIF
		
			IF NOT IS_ENTITY_DEAD(pedLester)
				//FREEZE_ENTITY_POSITION(pedLester, TRUE)
				SET_PED_CONFIG_FLAG(pedLester, PCF_UseKinematicModeWhenStationary, TRUE)
			ENDIF
			
			g_bLaptopMissionSuppressed = FALSE
						
			DISPLAY_RADAR(TRUE)
												
			bStageSetup = FALSE
			bCameraShotSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//BOOL bGetCrewOutsideFlags[3][4]
FLOAT fMoveBlendRatio = 0.0

FUNC BOOL HAS_PED_GOTTEN_OUTSIDE_SWEATSHOP(	PED_INDEX thisPed, BOOL &thisFlag0, FLOAT thisMoveBlendRatio, INT thisTimeToWarp)
	IF NOT thisFlag0

		IF NOT IS_ENTITY_AT_COORD(thisPed, <<717.986267,-979.389282,25.111732>>,<<3.437500,1.562500,2.000000>>)
			IF NOT IS_TASK_ONGOING(thisPed, SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
				TASK_FOLLOW_NAV_MESH_TO_COORD(thisPed, <<717.986267,-979.389282,25.111732>>, thisMoveBlendRatio, thisTimeToWarp)
			ENDIF
		ELSE
			CLEAR_PED_TASKS(thisPed)
			//thisFlag2 = TRUE
			thisFlag0 = TRUE
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	RETURN FALSE
ENDFUNC

PROC GET_OUT_OF_SWEATSHOP_AND_INTO_VEHICLE(PED_INDEX thisPed, VEHICLE_INDEX thisVehicle, VEHICLE_SEAT thisSeat, FLOAT thisMoveBlendRatio, INT thisTimeToWarp, INT iPauseTime)
	
	SEQUENCE_INDEX seqGetOutOfSweatShop
	
	OPEN_SEQUENCE_TASK(seqGetOutOfSweatShop)
		TASK_PAUSE(NULL, iPauseTime)
		TASK_ENTER_VEHICLE(NULL, thisVehicle, thisTimeToWarp, thisSeat, thisMoveBlendRatio)
	CLOSE_SEQUENCE_TASK(seqGetOutOfSweatShop)
	
	IF NOT IS_PED_INJURED(thisPed)
		TASK_PERFORM_SEQUENCE(thisPed, seqGetOutOfSweatShop)
	ELSE
		SCRIPT_ASSERT("GET_OUT_OF_SWEATSHOP_AND_INTO_VEHICLE: ped injured!")
	ENDIF
	
	CLEAR_SEQUENCE_TASK(seqGetOutOfSweatShop)
	
ENDPROC

BOOL bMichaelsMaskON

PROC PROCESS_MICHAELS_HELMET()

	FLOAT fStartPhase, fTemp

	SWITCH iBalaclavaStage
	
		CASE 0
			REQUEST_ANIM_DICT("MISSCOMMON@VAN_PUT_ON_MASKS")
			iBalaclavaStage++
		BREAK

		CASE 1
			IF HAS_ANIM_DICT_LOADED("MISSCOMMON@VAN_PUT_ON_MASKS")
				TASK_PLAY_ANIM(GET_SELECT_PED(SEL_MICHAEL), "MISSCOMMON@VAN_PUT_ON_MASKS", "PUT_ON_MASK_PS", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,  AF_NOT_INTERRUPTABLE | AF_SECONDARY)
				iBalaclavaStage++
			ENDIF
		BREAK		

		CASE 2	
	
			IF IS_ENTITY_PLAYING_ANIM(GET_SELECT_PED(SEL_MICHAEL), "MISSCOMMON@VAN_PUT_ON_MASKS", "PUT_ON_MASK_PS")
			AND bMichaelsMaskON = FALSE
									
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
								
				FIND_ANIM_EVENT_PHASE("MISSCOMMON@VAN_PUT_ON_MASKS", "PUT_ON_MASK_PS", "MASK", fStartPhase, fTemp)
				
				IF GET_ENTITY_ANIM_CURRENT_TIME(GET_SELECT_PED(SEL_MICHAEL), "MISSCOMMON@VAN_PUT_ON_MASKS", "PUT_ON_MASK_PS") > fStartPhase
					SET_PED_HEADGEAR(GET_SELECT_PED(SEL_MICHAEL), TRUE)					
					REMOVE_ANIM_DICT("MISSCOMMON@VAN_PUT_ON_MASKS")
					bMichaelsMaskON = TRUE
				ENDIF
			ENDIF
			
		BREAK
		
	ENDSWITCH

ENDPROC

BOOL bHurryFranklinUp = FALSE

BOOL bHasPlayerBeenIncarYet
BOOL bBensonRecordingHelpStarted
BOOL bBugstarRecordingHelpStarted
BOOl bToldToHurry
BOOL bLesterSaysHurry = FALSE
BOOL bheadGearSet = FALSE

FLOAT fTruckPlayBackSpeedExiting = 1.0
FLOAT fvanPlayBackSpeedExiting = 1.0

BOOL bVanDamageCommented = FALSE

INT iPreStreamCutscene

BOOL bMentionedWantedLevel = FALSE


//PURPOSE:		Performs the section where Franklin drives to behind the store
FUNC BOOL stageDriveToStoreAggressive()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "STAGE_DRIVE_TO_STORE")
		g_replay.iReplayInt[CURRENT_TAKE_REPLAY_VALUE] = 0
		RESET_NEED_LIST()
		
//		SET_MODEL_AS_NEEDED(Asset.mnTruck)
//		SET_MODEL_AS_NEEDED(Asset.mnVan)
//				
//		//MANAGE_LOADING_NEED_LIST()
//		
//		WHILE NOT MANAGE_LOADING_NEED_LIST_SPEEDY()
//			WAIT(0)
//		ENDWHILE

		REQUEST_VEHICLE_RECORDING(1, "JHArrive")
		REQUEST_VEHICLE_RECORDING(2, "JHArrive")
		REQUEST_VEHICLE_RECORDING(3, "JHArrive")
		
		REQUEST_VEHICLE_RECORDING(1, "JHBENSONEXIT")

		SAFE_REQUEST_AMBIENT_AUDIO_BANK(Asset.sbRaidStore, FALSE, TRUE)
		SAFE_REQUEST_AMBIENT_AUDIO_BANK(Asset.sbRaidGlassSmash, FALSE, TRUE)
		//SAFE_REQUEST_AMBIENT_AUDIO_BANK(Asset.sbRaidRayfireCrash, TRUE, TRUE)
		//SAFE_REQUEST_AMBIENT_AUDIO_BANK(Asset.sbUnderwater, FALSE, FALSE)
			
		vDriveToPoint = vFrontOfStore
		
		ADD_ALL_PEDS_TO_DIALOGUE()
		
		FOR iCounter = 0 TO NUMBER_OF_EVENT_FLAGS-1
			bEventFlag[iCounter] = FALSE
		ENDFOR
		
		SET_VEHICLE_MODEL_IS_SUPPRESSED(Asset.mnVan, TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(Asset.mnTruck, TRUE)
		
		IF NOT IS_ENTITY_DEAD(vehTruck)
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTruck, FALSE)
		ENDIF

		sLocatesData.vehicleBlip = CREATE_BLIP_FOR_VEHICLE(vehVan)
		
		IF IS_VEHICLE_DRIVEABLE(vehTruck)
			SET_VEHICLE_DOORS_LOCKED(vehTruck, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
		ENDIF
		RunConversation(Asset.convFollowFranklin, TRUE, TRUE, TRUE)
		
		addTextToQueue(Asset.godGetInVan, TEXT_TYPE_GOD)
		
		IF IS_HACKER_PAIGE()
			addTextToQueue("JH_VAN_PH", TEXT_TYPE_CONVO)
		ELIF IS_HACKER_CHRISTIAN()
			addTextToQueue("JH_VAN_CF", TEXT_TYPE_CONVO)
		ELIF IS_HACKER_RICKIE()
			addTextToQueue("JH_VAN_RL", TEXT_TYPE_CONVO)
		ENDIF
							
		IF (IS_GUNMAN_GUSTAVO() AND IS_DRIVER_EDDIE())
			addTextToQueue("JH_VAN_GE", TEXT_TYPE_CONVO)
		ELIF (IS_GUNMAN_GUSTAVO() AND IS_DRIVER_KARIM())
			addTextToQueue("JH_VAN_GK", TEXT_TYPE_CONVO)
		ELIF (IS_GUNMAN_PACKIE() AND IS_DRIVER_EDDIE())
			addTextToQueue("JH_VAN_PE", TEXT_TYPE_CONVO)
		ELIF (IS_GUNMAN_PACKIE() AND IS_DRIVER_KARIM())
			addTextToQueue("JH_VAN_PK", TEXT_TYPE_CONVO)		
		ELIF (IS_GUNMAN_NORM() AND IS_DRIVER_EDDIE())
			addTextToQueue("JH_VAN_NE", TEXT_TYPE_CONVO)
		ELIF (IS_GUNMAN_NORM() AND IS_DRIVER_KARIM())
			addTextToQueue("JH_VAN_NK", TEXT_TYPE_CONVO)
		ENDIF
		
		//addTextToQueue("JH_AFTER_YOU", TEXT_TYPE_CONVO)
	
	
	
	
		SET_ROADS_IN_ANGLED_AREA(  <<756.440796,-1005.630188,21.892076>>, <<753.220398,-919.532532,30.691278>>, 55.000000, FALSE, FALSE)		//SET_VEHICLE_DENSITY_MULTIPLIER(0.2)
	
		SET_ROADS_IN_ANGLED_AREA( <<777.290100,-991.452209,23.630905>>, <<770.143494,-740.858765,31.306160>>, 13.500000, FALSE, FALSE)
		
		fMoveBlendRatio = PEDMOVEBLENDRATIO_WALK
	
		bHasPlayerBeenIncarYet = FALSE
	
		TRIGGER_MUSIC_EVENT("JH2B_VEHICLE")
	
		//SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE) 
		
		//XXLOAD_PATH_NODES_IN_AREA(-744.9758, -194.9404, 817.2436, -1034.6541)
		bheadGearSet = FALSE
		
		SETTIMERA(0)
		bBensonRecordingHelpStarted = FALSE
		SAFE_FADE_IN()
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
		bHurryFranklinUp = FALSE
//		UNPIN_INTERIOR(sweatShop)
		bToldToHurry = FALSE
		//SET_ROADS_IN_ANGLED_AREA( <<780.821716,-929.765686,22.374832>>, <<790.267761,-1079.209229,30.164776>>, 114.500000, FALSE, FALSE)
		
		iStageSection = 0
		iStageLestersIdle  = 0
		bStageSetup = TRUE
		
		REPLAY_RECORD_BACK_FOR_TIME(0.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
			
//═════════╡ UPDATE ╞═════════		
	ELSE		
		
		
		SWITCH iPreStreamCutscene
		
			CASE 0
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),vJewelDoorCoords, <<DEFAULT_CUTSCENE_LOAD_DIST, DEFAULT_CUTSCENE_LOAD_DIST, DEFAULT_CUTSCENE_LOAD_DIST>>)
					SET_PED_BAG(pedCrew[CREW_DRIVER_NEW], TRUE, TRUE)
					SET_PED_BAG(pedCrew[CREW_GUNMAN_NEW], TRUE, TRUE)
					REQUEST_CUTSCENE(Asset.mocapFrontDoorSmash)
					iPreStreamCutscene++
				ENDIF
			BREAK
			
			CASE 1
			
				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
					
					SET_CUTSCENE_PED_OUTFIT("Franklin", PLAYER_ONE, OUTFIT_P1_SUIT_1)
							
					
					SET_CUTSCENE_PED_OUTFIT("Michael", PLAYER_ZERO, OUTFIT_P0_JEWEL_HEIST)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Michael", INT_TO_ENUM(PED_COMPONENT,9), 1, 0) //(task)
					SET_CUTSCENE_PED_PROP_VARIATION("Michael", INT_TO_ENUM(PED_PROP_POSITION,2), 0, 0)
								
					//SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("Driver_selection", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_DRIVER_NEW)), CREW_OUTFIT_SUIT)
					//SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("gunman_selection_1", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_GUNMAN_NEW)), CREW_OUTFIT_SUIT)
					//SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("hacker_selection", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_HACKER)), CREW_OUTFIT_DEFAULT)
									
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("gunman_selection_1", pedCrew[CREW_GUNMAN_NEW])	
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("driver_selection", pedCrew[CREW_DRIVER_NEW])	
					
					SET_CUTSCENE_PED_COMPONENT_VARIATION("angry_customer", INT_TO_ENUM(PED_COMPONENT,0), 0, 0) //(head)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("angry_customer", INT_TO_ENUM(PED_COMPONENT,2), 1, 0) //(hair)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("angry_customer", INT_TO_ENUM(PED_COMPONENT,3), 1, 0) //(uppr)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("angry_customer", INT_TO_ENUM(PED_COMPONENT,4), 1, 0) //(lowr)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("angry_customer", INT_TO_ENUM(PED_COMPONENT,8), 0, 0) //(accs)
					
					IF DOES_ENTITY_EXIST(pedStore[1])
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Jewellery_Assitance", pedStore[1])
					ENDIF
					IF DOES_ENTITY_EXIST(pedStore[4])
						SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("shop_assistant", pedStore[4])
					ENDIF
		//			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("boyfriend", pedStore[3])
		//			IF DOES_ENTITY_EXIST(pedStore[2])
		//				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("customer", pedStore[2])				
		//			ENDIF
					
					SET_CUTSCENE_PED_COMPONENT_VARIATION("boyfriend", INT_TO_ENUM(PED_COMPONENT,0), 1, 0) //(head)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("boyfriend", INT_TO_ENUM(PED_COMPONENT,2), 1, 0) //(hair)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("boyfriend", INT_TO_ENUM(PED_COMPONENT,3), 0, 5) //(uppr)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("boyfriend", INT_TO_ENUM(PED_COMPONENT,4), 1, 2) //(lowr)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("boyfriend", INT_TO_ENUM(PED_COMPONENT,8), 0, 0) //(accs)
						
					
					SET_CUTSCENE_PED_COMPONENT_VARIATION("customer", INT_TO_ENUM(PED_COMPONENT,0), 0, 0) //(head)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("customer", INT_TO_ENUM(PED_COMPONENT,2), 0, 0) //(hair)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("customer", INT_TO_ENUM(PED_COMPONENT,3), 0, 1) //(uppr)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("customer", INT_TO_ENUM(PED_COMPONENT,4), 1, 1) //(lowr)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("customer", INT_TO_ENUM(PED_COMPONENT,8), 0, 0) //(accs)
					
							
					iPreStreamCutscene++
				ENDIF
			BREAK
			
			CASE 2
			
				IF HAS_CUTSCENE_LOADED()
					REQUEST_MODEL(Asset.mnSecurity)
					FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
						REQUEST_MODEL(Asset.mnShop[iCounter])
					ENDFOR
					REQUEST_ANIM_DICT(Asset.adJewelHeist)
											
					REQUEST_VEHICLE_RECORDING(1, "JHArrive")
					REQUEST_VEHICLE_RECORDING(2, "JHArrive")
					REQUEST_VEHICLE_RECORDING(3, "JHArrive")
							
					REQUEST_WEAPON_ASSET(mnMainWeaponType, DEFAULT, WEAPON_COMPONENT_GRIP)
					iPreStreamCutscene++
				ENDIF
			
			BREAK
			
		ENDSWITCH
		
		//AGGRESSIVE
		
//		IF IS_KEYBOARD_KEY_JUST_PRESSED(key_C)
//			PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "JH_AAAA", "JEWELMANAGER", SPEECH_PARAMS_FORCE_FRONTEND)
//		ENDIF
		
		UPDATE_LESTERS_IDLE_ANIMS()
	
		IF IS_PLAYER_PRESSING_HORN(PLAYER_ID())
		OR bHurryFranklinUp = TRUE
			SET_PED_DESIRED_MOVE_BLEND_RATIO(GET_SELECT_PED(SEL_MICHAEL), PEDMOVEBLENDRATIO_RUN)
			SET_PED_DESIRED_MOVE_BLEND_RATIO(pedCrew[CREW_DRIVER_NEW], PEDMOVEBLENDRATIO_RUN)
			SET_PED_DESIRED_MOVE_BLEND_RATIO(pedCrew[CREW_GUNMAN_NEW], PEDMOVEBLENDRATIO_RUN)
			
			bHurryFranklinUp = TRUE
		ELSE
			SET_PED_DESIRED_MOVE_BLEND_RATIO(GET_SELECT_PED(SEL_MICHAEL), PEDMOVEBLENDRATIO_WALK)
			SET_PED_DESIRED_MOVE_BLEND_RATIO(pedCrew[CREW_DRIVER_NEW], PEDMOVEBLENDRATIO_WALK)
			SET_PED_DESIRED_MOVE_BLEND_RATIO(pedCrew[CREW_GUNMAN_NEW], PEDMOVEBLENDRATIO_WALK)
		ENDIF
		SET_PED_DESIRED_MOVE_BLEND_RATIO(pedCrew[CREW_HACKER], PEDMOVEBLENDRATIO_WALK)
		
	
		SWITCH iStageSection
		
			CASE 0
				bLesterSaysHurry = FALSE
				TRIGGER_MUSIC_EVENT("JH2B_START")
			
				IF NOT IS_ENTITY_DEAD(vehTruck)
					SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehTruck, FALSE)
				ENDIF
				IF NOT IS_ENTITY_DEAD(vehVan)
					SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehVan, FALSE)
					SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehVan, SC_DOOR_FRONT_RIGHT, FALSE)
				ENDIF
				
				SETTIMERA(0)
				
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedCrew[CREW_DRIVER_NEW], TRUE)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedCrew[CREW_GUNMAN_NEW], TRUE)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedCrew[CREW_HACKER], TRUE)
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(GET_SELECT_PED(SEL_MICHAEL), TRUE)
				
				TASK_TURN_PED_TO_FACE_ENTITY(pedCrew[CREW_HACKER], PLAYER_PED_ID(), -1)
				TASK_LOOK_AT_ENTITY(pedCrew[CREW_HACKER], PLAYER_PED_ID(), -1)
				TASK_LOOK_AT_ENTITY(pedCrew[CREW_DRIVER_NEW], pedCrew[CREW_GUNMAN_NEW], -1)
				TASK_LOOK_AT_ENTITY(pedCrew[CREW_GUNMAN_NEW], pedCrew[CREW_DRIVER_NEW], -1)
				TASK_LOOK_AT_ENTITY(GET_SELECT_PED(SEL_MICHAEL), PLAYER_PED_ID(), -1)
				
				IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
				IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
				AND NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
					TASK_LOOK_AT_ENTITY(GET_SELECT_PED(SEL_MICHAEL), GET_SELECT_PED(SEL_FRANKLIN), 99999999,  SLF_WHILE_NOT_IN_FOV)
				ENDIF
				HAS_PED_ACHIEVED_COORD_HEAD(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], << 713.2115, -962.9461, 29.4335 >>, 190.7033,
											PEDMOVE_WALK, 1.0, 15)
				ENDIF
				iStageSection++
			BREAK
		
			CASE 1

			
			
				IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), vTopOfStairs, <<9.0,9.0,3.0>>)
				OR IS_ENTITY_IN_ANGLED_AREA(GET_SELECT_PED(SEL_FRANKLIN), <<718.095642,-975.372864,23.709906>>, <<717.560425,-965.223206,31.395351>>, 3.250000)
				OR IS_ENTITY_AT_ENTITY(GET_SELECT_PED(SEL_FRANKLIN), vehVan, <<5.0, 5.0, 1.5>>)
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_UD)
				OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_MOVE_LR)
					//IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_AFTER_YOU", CONV_PRIORITY_VERY_HIGH)
						//iStageSection++
					//ENDIF
					
					GET_OUT_OF_SWEATSHOP_AND_INTO_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehVan, VS_FRONT_RIGHT, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP * 3, 0)
					SET_PED_CONFIG_FLAG(GET_SELECT_PED(SEL_MICHAEL), PCF_OpenDoorArmIK, TRUE )
					SETTIMERA(0)
					PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(GET_SELECT_PED(SEL_MICHAEL), "WALK_ME_TO_THE_CAR", "MICHAEL", SPEECH_PARAMS_FORCE_FRONTEND)
					iStageSection++
				ELSE
					IF TIMERA() > 15000
					AND bLesterSaysHurry = FALSE
						addTextToQueue("JH_GOF", TEXT_TYPE_CONVO)
						bLesterSaysHurry = TRUE
					ENDIF
				ENDIF
			BREAK
			
			CASE 2
				
				IF IS_ENTITY_IN_ANGLED_AREA(GET_SELECT_PED(SEL_FRANKLIN), <<717.657166,-966.014893,31.645279>>, <<718.065430,-975.373047,23.730608>>, 3.250000)
				OR IS_ENTITY_AT_ENTITY(GET_SELECT_PED(SEL_FRANKLIN), vehVan, <<5.0, 5.0, 1.5>>)
					iGuysIdles = 99 	//Stop the idles!!!
					TASK_CLEAR_LOOK_AT(GET_SELECT_PED(SEL_FRANKLIN))
//					UNPIN_INTERIOR(sweatShop)
					GET_OUT_OF_SWEATSHOP_AND_INTO_VEHICLE(pedCrew[CREW_HACKER], vehTruck, VS_DRIVER, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP * 3, 0)
					GET_OUT_OF_SWEATSHOP_AND_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehVan, VS_BACK_RIGHT, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP * 3, 500)
					GET_OUT_OF_SWEATSHOP_AND_INTO_VEHICLE(pedCrew[CREW_GUNMAN_NEW],  vehVan, VS_BACK_LEFT, PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP * 3, 3000)
					
					
					SET_PED_CONFIG_FLAG(pedCrew[CREW_HACKER], PCF_OpenDoorArmIK, TRUE )
					SET_PED_CONFIG_FLAG(pedCrew[CREW_DRIVER_NEW], PCF_OpenDoorArmIK, TRUE )
					SET_PED_CONFIG_FLAG(pedCrew[CREW_GUNMAN_NEW], PCF_OpenDoorArmIK, TRUE )
					
					SET_PED_CONFIG_FLAG(GET_SELECT_PED(SEL_MICHAEL), PCF_OpenDoorArmIK, TRUE )
					
					SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedCrew[CREW_HACKER], FALSE)
					SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedCrew[CREW_DRIVER_NEW], FALSE)
					SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedCrew[CREW_GUNMAN_NEW], FALSE)
					
					
//					TASK_LOOK_AT_COORD(pedCrew[CREW_HACKER], <<707.2448, -967.4964, 30.5720>>, INFINITE_TASK_TIME)
//					OPEN_SEQUENCE_TASK(seqGoToLapTop)
//						TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<707.1595, -966.8809, 29.4181>>, PEDMOVE_WALK)
//						TASK_ACHIEVE_HEADING(NULL, 169.6505)
//					CLOSE_SEQUENCE_TASK(seqGoToLapTop)
//					TASK_PERFORM_SEQUENCE(pedCrew[CREW_HACKER],seqGoToLapTop)
//					CLEAR_SEQUENCE_TASK(seqGoToLapTop)
					
					//TASK_GO_TO_ENTITY(GET_SELECT_PED(SEL_MICHAEL), vehVan, DEFAULT_TIME_BEFORE_WARP, DEFAULT_SEEK_RADIUS, fMoveBlendRatio)
					iStageSection++
				ELSE
				
					IF TIMERA() > 15000
					AND bLesterSaysHurry = FALSE
						addTextToQueue("JH_GOF", TEXT_TYPE_CONVO)
						bLesterSaysHurry = TRUE
					ENDIF
				
				ENDIF
			BREAK
		
			CASE 3
										
				//IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), <<717.576355,-978.906006,25.302534>>,<<3.875000,3.125000,2.187500>>)
				IF IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehVan)
				//OR IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), GET_ENTITY_COORDS(vehVan), <<8.0,8.0,3.0>>)
					SETTIMERA(0)
					
					fMoveBlendRatio = PEDMOVEBLENDRATIO_RUN
					IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(ASset.mnLester)
					IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(ASset.mnLestersStick)
					
					SET_OBJECT_AS_NO_LONGER_NEEDED(oiLestersStick)
					CLEANUP_PED(pedLester, TRUE)
					REMOVE_ANIM_DICT("move_m@generic_variations@idle@b")
					
					START_AUDIO_SCENE("JSH_2B_GET_TO_STORE")
					
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
									
					iStageSection++
				ENDIF
				
			BREAK
			CASE 4
				//Aggressive Path
//				IF DOES_ENTITY_EXIST(vehTruck)
//				AND DOES_ENTITY_EXIST(pedCrew[CREW_HACKER])
//					IF IS_PED_IN_VEHICLE(pedCrew[CREW_HACKER], vehTruck)
//						IF NOT bBensonRecordingHelpStarted
//							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCrew[CREW_HACKER], TRUE)
//							START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehTruck, 1, "JHBENSONEXIT")
//							bBensonRecordingHelpStarted = TRUE
//						ENDIF
//						
//						IF bBensonRecordingHelpStarted
//						AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck)
//							IF NOT IS_ENTITY_AT_COORD(vehTruck, vTruckDriveAIPoint, <<5,5,5>>)
//								IF NOT IS_TASK_ONGOING(pedCrew[CREW_HACKER], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD)
//									//TASK_VEHICLE_DRIVE_TO_COORD(pedCrew[CREW_HACKER], vehTruck, vTruckDriveAIPoint, 18, DRIVINGSTYLE_NORMAL, Asset.mnTruck, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS, 3.0, 20.0)
//									TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(pedCrew[CREW_HACKER], vehTruck, vTruckDriveAIPoint, 18.0, DRIVINGMODE_STOPFORCARS, 5.0)
//											
//									//SET_VEHICLE_DENSITY_MULTIPLIER(1.0)
//								ENDIF
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
				
				IF DOES_ENTITY_EXIST(vehTruck)
					IF DOES_ENTITY_EXIST(pedCrew[CREW_HACKER])
						IF IS_PED_IN_VEHICLE(pedCrew[CREW_HACKER], vehTruck)
							IF NOT IS_ENTITY_AT_COORD(vehTruck, vTruckDriveAIPoint, <<5,5,5>>)
								IF NOT bBensonRecordingHelpStarted
								AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "JHBENSONEXIT")
									START_PLAYBACK_RECORDED_VEHICLE(vehTruck, 1, "JHBENSONEXIT")
									bBensonRecordingHelpStarted = TRUE
								ENDIF
								IF bBensonRecordingHelpStarted
									IF NOT IS_TASK_ONGOING(pedCrew[CREW_HACKER], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE)
										IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck)//IS_ENTITY_AT_COORD(vehTruck, <<758.5399, -1003.1363, 25.2859>>, <<5,5,6>>)												
											TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(pedCrew[CREW_HACKER], vehTruck, vTruckDriveAIPoint, 19.0, DRIVINGMODE_STOPFORCARS, 5.0)// DRIVINGMODE_STOPFORCARS, 5.0)
											SETTIMERA(0)
										ENDIF
									ENDIF
								ENDIF
								
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTruck, <<0.0, 3.0, 0.0>>), <<4.0, 4.0, 4.0>>)
									IF fTruckPlayBackSpeedExiting > 0.0
										fTruckPlayBackSpeedExiting = fTruckPlayBackSpeedExiting -@ 0.5
									ELSE
										fTruckPlayBackSpeedExiting = 0.0
									ENDIF
								ELSE
									IF fTruckPlayBackSpeedExiting < 1.0
										fTruckPlayBackSpeedExiting = fTruckPlayBackSpeedExiting  +@ 0.5
									ELSE
										fTruckPlayBackSpeedExiting = 1.0
									ENDIF
								ENDIF
								
								SET_PLAYBACK_SPEED(vehTruck, fTruckPlayBackSpeedExiting)
								
								
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
//				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck) 
//				OR IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<745.8588, -982.8824, 23.8156>>, <<90.0, 90.0, 90.0>>)
//					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)				
//				ELSE
//					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)					
//				ENDIF
				
				IF DOES_ENTITY_EXIST(GET_SELECT_PED(SEL_FRANKLIN))
					IF IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehVan)
						bHasPlayerBeenIncarYet = TRUE
					ENDIF
				ENDIF
							
				
				//"G_C1LEAVE", "G_C1LEAVE",
				IF IS_PLAYER_AT_LOCATION_WITH_CREW_MEMBERS_IN_VEHICLE(sLocatesData, vDriveToPoint, <<5,5,5>>, TRUE, 
																			GET_SELECT_PED(SEL_MICHAEL), pedCrew[CREW_GUNMAN_NEW], pedCrew[CREW_DRIVER_NEW], vehVan,
																			Asset.godDriveToStore, Asset.godMichaelLeft, "G_PICKCREW", "G_PICKCREW",
																			Asset.godPickUpCrew, Asset.godGetInVan, Asset.godGetBackInVan, "", strCrewMemberBest, strCrewMemberWorst, FALSE, bHasPlayerBeenIncarYet)
				OR (DOES_BLIP_EXIST(sLocatesData.LocationBlip) AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-665.706543,-246.371246,35.002647>>, <<-700.728882,-266.411346,46.027256>>, 38.750000))
				OR (DOES_BLIP_EXIST(sLocatesData.LocationBlip) AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-648.916443,-208.859085,35.889164>>, <<-659.242859,-187.904800,44.673836>>, 42.250000))
				OR (DOES_BLIP_EXIST(sLocatesData.LocationBlip) AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-701.604309,-207.684723,35.562073>>, <<-683.287537,-243.607956,43.694168>>, 42.250000))
				OR (DOES_BLIP_EXIST(sLocatesData.LocationBlip) AND IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-663.145874,-228.753036,35.363098>>, <<-646.998291,-233.374329,39.712559>>, 16.500000))
					IF CAN_PLAYER_START_CUTSCENE(TRUE, TRUE)
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-665.706543,-246.371246,35.002647>>, <<-700.728882,-266.411346,46.027256>>, 38.750000)
							iArrivalAngle = 1
						ENDIF
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-648.916443,-208.859085,35.889164>>, <<-659.242859,-187.904800,44.673836>>, 42.250000)
							iArrivalAngle = 2
						ENDIF
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-701.604309,-207.684723,35.562073>>, <<-683.287537,-243.607956,43.694168>>, 42.250000)
							iArrivalAngle = 3
						ENDIF
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-663.145874,-228.753036,35.363098>>, <<-646.998291,-233.374329,39.712559>>, 16.500000)
							iArrivalAngle = 4
							SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						ENDIF
						SETTIMERB(0)	
												
						CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						iStageSection++
					ENDIF
				ENDIF	
								
				IF IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehVan)
					
					IF IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehVan)
					AND IS_PED_IN_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehVan)
					AND IS_PED_IN_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehVan)
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData) 
						AND IS_BIT_SET(sLocatesData.iLocatesBitSet, BS_INITIAL_GOD_TEXT_PRINTED)
							
							IF NOT bEventFlag[1]
								clearText()
								
								
								
//								addTextToQueue(Asset.convAggressivePlan, TEXT_TYPE_CONVO, 2000)
//								//Michael:	You got it down?
//								addTextToQueue(Asset.convAggressivePlan2, TEXT_TYPE_CONVO)
								
								addTextToQueue("JH_PLAN3", TEXT_TYPE_CONVO, 2000)
								//addTextToQueue("JH_NEXT", TEXT_TYPE_CONVO)
								IF IS_GUNMAN_GUSTAVO()
									addTextToQueue("JH_GUS", TEXT_TYPE_CONVO)
								ELIF IS_GUNMAN_NORM()
									addTextToQueue("JH_NORM", TEXT_TYPE_CONVO)
								ELIF IS_GUNMAN_PACKIE()
									addTextToQueue("JH_PACKIE", TEXT_TYPE_CONVO)									
								ENDIF
								IF IS_DRIVER_EDDIE()
									addTextToQueue("JH_NOTCLOSE", TEXT_TYPE_CONVO)									
								ELIF IS_DRIVER_KARIM()
									addTextToQueue("JH_NOTCLOSEK", TEXT_TYPE_CONVO)									
								ENDIF
								
								addTextToQueue("JH_CREWMEM", TEXT_TYPE_HELP, 0, FALSE)
								
								TASK_CLEAR_LOOK_AT(GET_SELECT_PED(SEL_MICHAEL))
								bEventFlag[1] = TRUE
								bEventFlag[2] = TRUE
							ENDIF
						ENDIF
						
//					ELSE
//							IF NOT bEventFlag[2]
//								RunConversation(Asset.convComeOnBoys, TRUE, TRUE, TRUE)
//								//addTextToQueue(Asset.godPickUpCrew, TEXT_TYPE_GOD)
//								bEventFlag[2] = TRUE
//							ENDIF
						
					ENDIF
			
				ENDIF
				
				
				//MIchael says hurry up.
				IF bToldToHurry = FALSE
				AND IS_PED_IN_ANY_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN))
				AND IS_PED_IN_ANY_VEHICLE(GET_SELECT_PED(SEL_MICHAEL))
				AND GET_ENTITY_SPEED(vehVAn) < 0.5	
				AND bEventFlag[1] = TRUE
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				
					IF CREATE_CONVERSATION(myScriptedSpeech,"JHAUD", "JH_MICGO", CONV_PRIORITY_VERY_HIGH)
						bToldToHurry = TRUE
					ENDIF
				ENDIF				
				
				
				IF NOT IS_ENTITY_DEAD(vehVan)
					IF GET_ENTITY_HEALTH(vehVan) < 500
					AND bVanDamageCommented = FALSE
						addTextToQueue("JH_VANDAM", TEXT_TYPE_CONVO)	
						bVanDamageCommented = TRUE
					ENDIF
				ENDIF
				
				IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), vDriveToPoint, <<200,200,50>>)	//Preload Assets
					//SAFE_REQUEST_MOCAP_CUTSCENE(Asset.mocapFrontDoorSmash, FALSE, FALSE)
					SAFE_REQUEST_MODEL(Asset.mnSecurity, FALSE, FALSE)
					FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
						SAFE_REQUEST_MODEL(Asset.mnShop[iCounter], FALSE, FALSE)
					ENDFOR
					SAFE_REQUEST_ANIM_DICT(Asset.adJewelHeist, FALSE, FALSE)
					IF bheadGearSet = FALSE
						//SET_PED_HEADGEAR(pedCrew[CREW_DRIVER_NEW], TRUE)
						//SET_PED_HEADGEAR(pedCrew[CREW_GUNMAN_NEW], TRUE)
																	
						bheadGearSet = TRUE				
					ENDIF
				ENDIF
				
				IF bheadGearSet
					PROCESS_MICHAELS_HELMET()
				ENDIF
				
				//Pause dialogue if out of car.
				
				IF bEventFlag[1] = TRUE	//Wait until in car dialogue is triggered.
				AND bEventFlag[2] = TRUE
				
					IF NOT DOES_BLIP_EXIST(sLocatesData.LocationBlip)
					OR IS_ENTITY_UPSIDEDOWN(vehVan)
						IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
							bMentionedWantedLevel = FALSE
						ENDIF
						
						IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
						AND bMentionedWantedLevel = FALSE
							IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(GET_SELECT_PED(SEL_FRANKLIN), "POLICE_PURSUIT_DRIVEN", "FRANKLIN_NORMAL")
								bMentionedWantedLevel = TRUE
							ENDIF							
						ENDIF
					ELSE
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
						AND ARE_PEDS_IN_THE_SAME_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), GET_SELECT_PED(SEL_FRANKLIN))
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						ENDIF
					ENDIF
								
				ENDIF
				
			BREAK	
			
		ENDSWITCH
		
		
		SET_PED_RESET_FLAG(pedCrew[CREW_HACKER], PRF_SearchForClosestDoor, TRUE )
		SET_PED_RESET_FLAG(pedCrew[CREW_DRIVER_NEW], PRF_SearchForClosestDoor, TRUE )
		SET_PED_RESET_FLAG(pedCrew[CREW_GUNMAN_NEW], PRF_SearchForClosestDoor, TRUE )
		
		SET_PED_RESET_FLAG(GET_SELECT_PED(SEL_MICHAEL), PRF_SearchForClosestDoor, TRUE )
		
	
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 5
		
			//XXRELEASE_PATH_NODES()
		
			IF DOES_BLIP_EXIST(blipLocate)
				REMOVE_BLIP(blipLocate)
			ENDIF
			IF DOES_BLIP_EXIST(blipBike)
				REMOVE_BLIP(blipBike)
			ENDIF
			
			FOR iCounter = 0 TO 2
				IF DOES_BLIP_EXIST(blipCrew[iCounter])
					REMOVE_BLIP(blipCrew[iCounter])
				ENDIF
			ENDFOR
			
			SET_PED_CONFIG_FLAG(pedCrew[CREW_HACKER], PCF_OpenDoorArmIK, FALSE )
			SET_PED_CONFIG_FLAG(pedCrew[CREW_DRIVER_NEW], PCF_OpenDoorArmIK, FALSE )
			SET_PED_CONFIG_FLAG(pedCrew[CREW_GUNMAN_NEW], PCF_OpenDoorArmIK, FALSE )
			
			SET_PED_CONFIG_FLAG(GET_SELECT_PED(SEL_MICHAEL), PCF_OpenDoorArmIK, FALSE )
			
			IF bIsStealthPath
				CLEANUP_VEHICLE(vehVan, TRUE)
			ENDIF
			CLEANUP_VEHICLE(vehTruck, TRUE)
			SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCar)
			CLEANUP_PED(pedCrew[CREW_HACKER], TRUE)
			CLEANUP_PED(pedCrew[CREW_HACKER], TRUE)
			CLEANUP_PED(pedLester, TRUE)
			
			STOP_AUDIO_SCENE("JSH_2B_GET_TO_STORE")
			
			ADD_ALL_PEDS_TO_DIALOGUE()
	
			bStageSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//SEQUENCE_INDEX seqGetOutOfGarmentFactory
//INT iTimeOfVanRecordingStarted
//PURPOSE:		Performs the section where Franklin drives to behind the store
FUNC BOOL stageDriveToStoreStealth()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(1, "STAGE_DRIVE_TO_STORE")
		g_replay.iReplayInt[CURRENT_TAKE_REPLAY_VALUE] = 0

		RESET_NEED_LIST()
		IF bIsStealthPath
			REQUEST_VEHICLE_ASSET(Asset.mnCar)
		ENDIF

		REQUEST_VEHICLE_RECORDING(1, "JH2BArrive")
		REQUEST_VEHICLE_RECORDING(2, "JH2BArrive")
		REQUEST_VEHICLE_RECORDING(1, "JHBENSONEXIT")
		REQUEST_VEHICLE_RECORDING(1, "JHBugStrExit")
		
		
		SAFE_REQUEST_AMBIENT_AUDIO_BANK(Asset.sbRaidStore, FALSE, TRUE)
		SAFE_REQUEST_AMBIENT_AUDIO_BANK(Asset.sbRaidGlassSmash, FALSE, TRUE)
		//SAFE_REQUEST_AMBIENT_AUDIO_BANK(Asset.sbRaidRayfireCrash, TRUE, TRUE)
		//SAFE_REQUEST_AMBIENT_AUDIO_BANK(Asset.sbUnderwater, FALSE, FALSE)
				
		IF WAS_CUTSCENE_SKIPPED()
			//SCRIPT_ASSERT("get to fuck")
			RESET_GAME_CAMERA()
		ENDIF
	
		vDriveToPoint = vBackOfStore
				
		ADD_ALL_PEDS_TO_DIALOGUE()
			
		FOR iCounter = 0 TO NUMBER_OF_EVENT_FLAGS-1
			bEventFlag[iCounter] = FALSE
		ENDFOR

		SET_VEHICLE_MODEL_IS_SUPPRESSED(Asset.mnCar, TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(Asset.mnVan, TRUE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(Asset.mnTruck, TRUE)

		IF NOT IS_ENTITY_DEAD(vehTruck)
			SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehTruck, FALSE)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehVan, TRUE)
		ENDIF
		IF NOT IS_ENTITY_DEAD(vehCar)
			SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehCar, FALSE)
		ENDIF
		IF NOT IS_ENTITY_DEAD(vehVan)
			SET_VEHICLE_AUTOMATICALLY_ATTACHES(vehVan, FALSE)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(vehVan, TRUE)
		ENDIF
		
		sLocatesData.vehicleBlip = CREATE_BLIP_FOR_VEHICLE(vehCar)
		IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
		AND NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
			TASK_LOOK_AT_ENTITY(GET_SELECT_PED(SEL_FRANKLIN), GET_SELECT_PED(SEL_MICHAEL), 99999999,  SLF_WHILE_NOT_IN_FOV)
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(vehVan)
			SET_VEHICLE_DOORS_LOCKED(vehVan, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
		ENDIF
		IF IS_VEHICLE_DRIVEABLE(vehTruck)
			SET_VEHICLE_DOORS_LOCKED(vehTruck, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
		ENDIF
		//RunConversation(Asset.convFollowMichael, TRUE, TRUE, TRUE)
		
		RunConversation(Asset.convFollowMichael, TRUE, TRUE, TRUE)
		
		//Gus and Eddie
		IF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)) = CM_GUNMAN_G_GUSTAV
		AND GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW)) = CM_DRIVER_G_EDDIE
			addTextToQueue("JH_GETIN1", TEXT_TYPE_CONVO, 0)	
		ENDIF
		
		IF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)) = CM_GUNMAN_G_GUSTAV
		AND GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW)) = CM_DRIVER_G_TALINA_UNLOCK
			addTextToQueue("JH_GETIN2", TEXT_TYPE_CONVO, 0)	
		ENDIF
		
		IF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)) = CM_GUNMAN_G_GUSTAV
		AND GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW)) = CM_DRIVER_B_KARIM
			addTextToQueue("JH_GETIN3", TEXT_TYPE_CONVO, 0)	
		ENDIF
		
		IF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)) = CM_GUNMAN_G_PACKIE_UNLOCK
		AND GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW)) = CM_DRIVER_G_EDDIE
			addTextToQueue("JH_GETIN4", TEXT_TYPE_CONVO, 0)	
		ENDIF
		
		IF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)) = CM_GUNMAN_G_PACKIE_UNLOCK
		AND GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW)) = CM_DRIVER_G_TALINA_UNLOCK
			addTextToQueue("JH_GETIN5", TEXT_TYPE_CONVO, 0)	
		ENDIF
		
		IF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)) = CM_GUNMAN_G_PACKIE_UNLOCK
		AND GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW)) = CM_DRIVER_B_KARIM
			addTextToQueue("JH_GETIN6", TEXT_TYPE_CONVO, 0)	
		ENDIF
		
		IF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)) = CM_GUNMAN_B_NORM
		AND GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW)) = CM_DRIVER_G_EDDIE
			addTextToQueue("JH_GETIN7", TEXT_TYPE_CONVO, 0)	
		ENDIF
		
		IF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)) = CM_GUNMAN_B_NORM
		AND GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW)) = CM_DRIVER_G_TALINA_UNLOCK
			addTextToQueue("JH_GETIN8", TEXT_TYPE_CONVO, 0)	
		ENDIF
		
		IF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)) = CM_GUNMAN_B_NORM
		AND GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW)) = CM_DRIVER_B_KARIM
			addTextToQueue("JH_GETIN9", TEXT_TYPE_CONVO, 0)	
		ENDIF
				
		addTextToQueue(Asset.godGetInTheCar, TEXT_TYPE_GOD)
	
		addTextToQueue("JH_DIR", TEXT_TYPE_CONVO)
	
	
	
		bBensonRecordingHelpStarted = FALSE
		fMoveBlendRatio = PEDMOVEBLENDRATIO_WALK
		
		//Fix for 307560
		SET_BIT(sLocatesData.iLocatesBitSet, BS_HAS_BEEN_IN_VEHICLE)
		SET_BIT(sLocatesData.iLocatesBitSet, BS_PRINTED_GET_IN_VEHICLE)
		SET_BIT(sLocatesData.iLocatesBitSet, BS_PRINTED_GET_BACK_IN_VEHICLE)
		CLEAR_BIT(sLocatesData.iLocatesBitSet, BS_PRINTED_PICKUP_BUDDY_TEXT_0)
		SETTIMERA(0)
		
		SET_ROADS_IN_ANGLED_AREA(  <<756.440796,-1005.630188,21.892076>>, <<753.220398,-919.532532,30.691278>>, 55.000000, FALSE, FALSE)		//SET_VEHICLE_DENSITY_MULTIPLIER(0.2)
		
		SET_ROADS_IN_ANGLED_AREA( <<777.290100,-991.452209,23.630905>>, <<770.143494,-740.858765,31.306160>>, 13.500000, FALSE, FALSE)
		//SET_AUDIO_FLAG("AllowScoreAndRadio", TRUE) 
		
		CLEAR_AREA_OF_VEHICLES(vVanSpawnCoords, 500.0, TRUE)
		
		TRIGGER_MUSIC_EVENT("JH2A_VEHICLE")
		
		//XXLOAD_PATH_NODES_IN_AREA(-744.9758, -194.9404, 817.2436, -1034.6541)
		
		// REPLAY!
		// tell replay controller we've finished setting up for replay
		//END_REPLAY_SETUP()	
					
		SAFE_FADE_IN()
		
		bHurryFranklinUp = FALSE
		
		//UNPIN_INTERIOR(sweatShop)
		
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
		iStageSection = 0
		bStageSetup = TRUE
		
		bLesterSaysHurry = FALSE
		SETTIMERA(0)

		REPLAY_RECORD_BACK_FOR_TIME(0.0, 6.0, REPLAY_IMPORTANCE_HIGHEST)
		
		
//═════════╡ UPDATE ╞═════════		
	ELSE		
		//STEALTH
			
	
		//SET_ROADS_IN_ANGLED_AREA(  <<756.440796,-1005.630188,21.892076>>, <<753.220398,-919.532532,30.691278>>, 55.000000, FALSE, FALSE)		//SET_VEHICLE_DENSITY_MULTIPLIER(0.2)
			
		UPDATE_LESTERS_IDLE_ANIMS()
	
		IF IS_PLAYER_PRESSING_HORN(PLAYER_ID())
		OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE)
		OR bHurryFranklinUp = TRUE
			SET_PED_DESIRED_MOVE_BLEND_RATIO(GET_SELECT_PED(SEL_FRANKLIN), PEDMOVEBLENDRATIO_RUN)
			bHurryFranklinUp = TRUE
		ELSE
			SET_PED_DESIRED_MOVE_BLEND_RATIO(GET_SELECT_PED(SEL_FRANKLIN), PEDMOVEBLENDRATIO_WALK)
		ENDIF
		
		SET_PED_DESIRED_MOVE_BLEND_RATIO(pedCrew[CREW_HACKER], PEDMOVEBLENDRATIO_WALK)
		SET_PED_DESIRED_MOVE_BLEND_RATIO(pedCrew[CREW_DRIVER_NEW], PEDMOVEBLENDRATIO_WALK)
		SET_PED_DESIRED_MOVE_BLEND_RATIO(pedCrew[CREW_GUNMAN_NEW], PEDMOVEBLENDRATIO_WALK)
	
		SET_PED_RESET_FLAG(pedCrew[CREW_HACKER], PRF_SearchForClosestDoor, TRUE )
		SET_PED_RESET_FLAG(pedCrew[CREW_DRIVER_NEW], PRF_SearchForClosestDoor, TRUE )
		SET_PED_RESET_FLAG(pedCrew[CREW_GUNMAN_NEW], PRF_SearchForClosestDoor, TRUE )
		SET_PED_RESET_FLAG(GET_SELECT_PED(SEL_FRANKLIN), PRF_SearchForClosestDoor, TRUE )
		
//		IF iStageSection >= 1	//Fix for 1994344
//			IF GET_SCRIPT_TASK_STATUS(pedCrew[CREW_HACKER], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK
//				GET_OUT_OF_SWEATSHOP_AND_INTO_VEHICLE(pedCrew[CREW_HACKER], vehTruck, VS_DRIVER, fMoveBlendRatio, DEFAULT_TIME_BEFORE_WARP * 3, 0000)
//			ENDIF
//		ENDIF
		
		SWITCH iStageSection
		
			CASE 0
				IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_MICHAEL), vTopOfStairs, <<4.0,4.0,3.0>>)
					iGuysIdles = 99 	//Stop the idles!!!
					//UNPIN_INTERIOR(sweatShop)
					TASK_CLEAR_LOOK_AT(GET_SELECT_PED(SEL_FRANKLIN))
					GET_OUT_OF_SWEATSHOP_AND_INTO_VEHICLE(pedCrew[CREW_HACKER], vehTruck, VS_DRIVER, fMoveBlendRatio, DEFAULT_TIME_BEFORE_WARP * 3, 0000)
					GET_OUT_OF_SWEATSHOP_AND_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehVan, VS_DRIVER, fMoveBlendRatio, DEFAULT_TIME_BEFORE_WARP * 3, 3000)
					GET_OUT_OF_SWEATSHOP_AND_INTO_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehVan, VS_FRONT_RIGHT, fMoveBlendRatio, DEFAULT_TIME_BEFORE_WARP * 3, 4500)
					GET_OUT_OF_SWEATSHOP_AND_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehCar, VS_FRONT_RIGHT,  fMoveBlendRatio, DEFAULT_TIME_BEFORE_WARP * 3, 0)
				
					SET_PED_CONFIG_FLAG(pedCrew[CREW_HACKER], PCF_OpenDoorArmIK, TRUE )
					SET_PED_CONFIG_FLAG(pedCrew[CREW_DRIVER_NEW], PCF_OpenDoorArmIK, TRUE )
					SET_PED_CONFIG_FLAG(pedCrew[CREW_GUNMAN_NEW], PCF_OpenDoorArmIK, TRUE )
					SET_PED_CONFIG_FLAG(GET_SELECT_PED(SEL_FRANKLIN), PCF_OpenDoorArmIK, TRUE )
//				
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedCrew[CREW_DRIVER_NEW], TRUE)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedCrew[CREW_GUNMAN_NEW], TRUE)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedCrew[CREW_HACKER], TRUE)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(GET_SELECT_PED(SEL_FRANKLIN), TRUE)

					SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedCrew[CREW_HACKER], fALSE)
					SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedCrew[CREW_DRIVER_NEW], fALSE)
					SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedCrew[CREW_GUNMAN_NEW], fALSE)
										
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTruck, FALSE)
										
					iStageSection++
				ELSE
				
					IF TIMERA() > 25000
					AND bLesterSaysHurry = FALSE
						addTextToQueue("JH_LESGO", TEXT_TYPE_CONVO)
						bLesterSaysHurry = TRUE
					ENDIF
				
				ENDIF
				
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)	
				
				IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(sLocatesData, vDriveToPoint, <<5,5,5>>, TRUE,GET_SELECT_PED(SEL_FRANKLIN), vehCar, Asset.godDriveToStore,Asset.godFranklinLeft, Asset.godGetInTheCar, Asset.godGetBackInCar, FALSE, FALSE)
								
			BREAK
		
			CASE 1
		
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.2)
		
				IF IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehCar)	
					
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
					
					TASK_LOOK_AT_ENTITY(GET_SELECT_PED(SEL_FRANKLIN), GET_SELECT_PED(SEL_MICHAEL), 60000,  SLF_WHILE_NOT_IN_FOV)
										
					SETTIMERA(0)					
					fMoveBlendRatio = PEDMOVEBLENDRATIO_RUN
					
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehCar,JH2A_CAR_DAMAGE) 
										
					IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(ASset.mnLester)
					CLEANUP_PED(pedLester, TRUE)
					REMOVE_VEHICLE_ASSET(Asset.mnCar)
					REMOVE_ANIM_DICT("move_m@generic_variations@idle@b")
					SET_RADIO_TO_STATION_INDEX(4)
					
					START_AUDIO_SCENE("JSH_2A_GET_TO_STORE")
					
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedCrew[CREW_DRIVER_NEW]) < 15.0
						IF IS_DRIVER_EDDIE()
							addTextToQueue("JH_DRIVE_ET", TEXT_TYPE_CONVO, 0)	
						ELIF IS_DRIVER_KARIM()
							addTextToQueue("JH_DRIVE_KD", TEXT_TYPE_CONVO, 0)	
						ENDIF			
					ENDIF			
							
							
					iStageSection++
				ENDIF
				
				SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)	
				
				IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(sLocatesData, vDriveToPoint, <<5,5,5>>, TRUE,GET_SELECT_PED(SEL_FRANKLIN), vehCar, Asset.godDriveToStore,Asset.godFranklinLeft, Asset.godGetInTheCar, Asset.godGetBackInCar, FALSE, FALSE)
				
			BREAK
			CASE 2
				//Stealth path
				
				REQUEST_PATH_NODES_IN_AREA_THIS_FRAME(758.5399, -1003.1363, vTruckDriveAIPoint.x, vTruckDriveAIPoint.y)
			
				
				IF DOES_ENTITY_EXIST(vehTruck)
					IF DOES_ENTITY_EXIST(pedCrew[CREW_HACKER])
						IF IS_PED_IN_VEHICLE(pedCrew[CREW_HACKER], vehTruck)
							IF NOT IS_ENTITY_AT_COORD(vehTruck, vTruckDriveAIPoint, <<5,5,5>>)
							AND NOT IS_ENTITY_AT_COORD(vehVan, vVanSpawnCoords, <<5,5,3>>)
								IF NOT bBensonRecordingHelpStarted
								AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "JHBENSONEXIT")
									START_PLAYBACK_RECORDED_VEHICLE(vehTruck, 1, "JHBENSONEXIT", FALSE)
									SET_PLAYBACK_TO_USE_AI_TRY_TO_REVERT_BACK_LATER(vehTruck,2000, DRIVINGMODE_STOPFORCARS, TRUE)								
									SET_SHOULD_LERP_FROM_AI_TO_FULL_RECORDING(vehTruck, TRUE)
									//START_PLAYBACK_RECORDED_VEHICLE_USING_AI(vehTruck, 1, "JHBENSONEXIT",10.0, DRIVINGMODE_STOPFORCARS)
									bBensonRecordingHelpStarted = TRUE
								ENDIF
								
								IF bBensonRecordingHelpStarted
									IF NOT IS_TASK_ONGOING(pedCrew[CREW_HACKER], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE)
										IF IS_ENTITY_AT_COORD(vehTruck, <<758.5399, -1003.1363, 25.2859>>, <<5,5,6>>)												
											TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(pedCrew[CREW_HACKER], vehTruck, vTruckDriveAIPoint, 19.0, DRIVINGMODE_STOPFORCARS, 5.0)// DRIVINGMODE_STOPFORCARS, 5.0)
											SETTIMERA(0)
										ENDIF
									ENDIF
								ENDIF
								
								IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTruck, <<0.0, 4.0, 0.0>>), <<6.0, 6.0, 4.0>>)
								OR IS_ENTITY_AT_COORD(vehVan, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTruck, <<0.0, 5.0, 0.0>>), <<11.0, 11.0, 4.0>>)
									IF fTruckPlayBackSpeedExiting > 0.0
										fTruckPlayBackSpeedExiting = fTruckPlayBackSpeedExiting -@ 0.5
									ELSE
										fTruckPlayBackSpeedExiting = 0.0
									ENDIF
								ELSE
									IF fTruckPlayBackSpeedExiting < 1.0
										fTruckPlayBackSpeedExiting = fTruckPlayBackSpeedExiting  +@ 0.5
									ELSE
										fTruckPlayBackSpeedExiting = 1.0
									ENDIF
								ENDIF
								
								SET_PLAYBACK_SPEED(vehTruck, fTruckPlayBackSpeedExiting)
																
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(vehVan)
					IF DOES_ENTITY_EXIST(pedCrew[CREW_DRIVER_NEW])
						IF DOES_ENTITY_EXIST(pedCrew[CREW_GUNMAN_NEW])
							IF IS_PED_IN_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehVan)
								IF IS_PED_IN_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehVan)
									IF NOT IS_ENTITY_AT_COORD(vehVan, vVanDriveAIPoint, <<5,5,5>>)
									//AND TIMERA() > 2000
										IF NOT IS_TASK_ONGOING(pedCrew[CREW_DRIVER_NEW], SCRIPT_TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE)
											
											IF NOT bBugstarRecordingHelpStarted
											AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "JHBugStrExit")
												START_PLAYBACK_RECORDED_VEHICLE(vehVan, 1, "JHBugStrExit")
												SET_PLAYBACK_TO_USE_AI_TRY_TO_REVERT_BACK_LATER(vehVan,2000, DRIVINGMODE_STOPFORCARS, TRUE)								
												SET_SHOULD_LERP_FROM_AI_TO_FULL_RECORDING(vehVan, TRUE)
												bBugstarRecordingHelpStarted = TRUE
											ENDIF
							
											IF bBugstarRecordingHelpStarted
											AND IS_ENTITY_AT_COORD(vehVan, <<748.7, -1003.6,  26.7>>, <<9,9,6>>)
											
											//AND NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehVan)																				
//												TASK_VEHICLE_DRIVE_TO_COORD(pedCrew[CREW_DRIVER_NEW], vehVan, vVanDriveAIPoint, 21, DRIVINGSTYLE_NORMAL, Asset.mnVan, DRIVINGMODE_AVOIDCARS_STOPFORPEDS_OBEYLIGHTS,3.0, 5.0)
												TASK_VEHICLE_DRIVE_TO_COORD_LONGRANGE(pedCrew[CREW_DRIVER_NEW], vehVan, vVanDriveAIPoint, 19.0, DRIVINGMODE_STOPFORCARS, 5.0)// DRIVINGMODE_STOPFORCARS, 5.0)
											ENDIF
											
											IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehVan, <<0.0, 3.0, 0.0>>), <<4.0, 4.0, 4.0>>)
											OR IS_ENTITY_AT_COORD(vehTruck, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehVan, <<0.0, 3.0, 0.0>>), <<5.0, 5.0, 4.0>>)
												IF fvanPlayBackSpeedExiting > 0.0
													fvanPlayBackSpeedExiting = fvanPlayBackSpeedExiting -@ 0.5
												ELSE
													fvanPlayBackSpeedExiting = 0.0
												ENDIF
											ELSE
												IF fvanPlayBackSpeedExiting < 1.0
													fvanPlayBackSpeedExiting = fvanPlayBackSpeedExiting  +@ 0.5
												ELSE
													fvanPlayBackSpeedExiting = 1.0
												ENDIF
											ENDIF
											
											SET_PLAYBACK_SPEED(vehVan, fvanPlayBackSpeedExiting)
											
										
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
								
				IF IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehCar)
					IF IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehCar)
						IF NOT bEventFlag[0]
							//clearText()
							addTextToQueue(Asset.convStealthPlan, TEXT_TYPE_CONVO, 3000)
								//Michael:	You got it down?
								//Franklin:	I got it.
								//Michael:	You use too much gas on these guys, you'll kill 'em. etc
							addTextToQueue(Asset.convStealthPlan2, TEXT_TYPE_CONVO)
							
							
							
							IF GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW))) = CMSK_BAD
							AND GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW))) = CMSK_BAD
							AND (GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER))) = CMSK_BAD
									OR GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER))) = CMSK_MEDIUM)
								addTextToQueue("JH_TALK1", TEXT_TYPE_CONVO)
							ENDIF
							
							IF GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW))) = CMSK_GOOD
							AND GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW))) = CMSK_BAD
							AND (GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER))) = CMSK_BAD
									OR GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER))) = CMSK_MEDIUM)
								addTextToQueue("JH_TALK2", TEXT_TYPE_CONVO)
							ENDIF
							
							IF GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW))) = CMSK_GOOD
							AND GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW))) = CMSK_GOOD
							AND (GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER))) = CMSK_BAD
									OR GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER))) = CMSK_MEDIUM)
								addTextToQueue("JH_TALK3", TEXT_TYPE_CONVO)
							ENDIF
							
							IF GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW))) = CMSK_BAD
							AND GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW))) = CMSK_GOOD
							AND (GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER))) = CMSK_BAD
									OR GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER))) = CMSK_MEDIUM)
								addTextToQueue("JH_TALK4", TEXT_TYPE_CONVO)
							ENDIF
							
							IF GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW))) = CMSK_GOOD
							AND GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW))) = CMSK_GOOD
							AND GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER))) = CMSK_GOOD
								addTextToQueue("JH_TALK5", TEXT_TYPE_CONVO)
							ENDIF
							
							IF GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW))) = CMSK_BAD
							AND GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW))) = CMSK_GOOD
							AND GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER))) = CMSK_GOOD
								addTextToQueue("JH_TALK6", TEXT_TYPE_CONVO)
							ENDIF
							IF GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW))) = CMSK_BAD
							AND GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW))) = CMSK_BAD
							AND GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER))) = CMSK_GOOD
								addTextToQueue("JH_TALK7", TEXT_TYPE_CONVO)
							ENDIF
							IF GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW))) = CMSK_GOOD
							AND GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW))) = CMSK_BAD
							AND GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER))) = CMSK_GOOD
								addTextToQueue("JH_TALK8", TEXT_TYPE_CONVO)
							ENDIF
							
							addTextToQueue("JH_CREWMEM", TEXT_TYPE_HELP, 0, FALSE)
							
							TASK_CLEAR_LOOK_AT(GET_SELECT_PED(SEL_FRANKLIN))
								
							bEventFlag[0] = TRUE
							bEventFlag[2] = TRUE
						ENDIF
					ELSE
						IF NOT bEventFlag[2]
						AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
							//RunConversation(Asset.convJesusFranklin, TRUE, TRUE, TRUE)
							//addTextToQueue(Asset.convJesusFranklin, TEXT_TYPE_CONVO, 2000)
							//addTextToQueue(Asset.godPickUpFranklin, TEXT_TYPE_GOD)
							//SCRIPT_ASSERT("bingo")
							
							IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", Asset.convJesusFranklin, CONV_PRIORITY_VERY_HIGH)
								bEventFlag[2] = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
//				IF (IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck) 
//					OR IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehVan) 
//					OR NOT IS_PED_IN_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehVan)
//					OR IS_ENTITY_AT_COORD(vehVan, <<749.2994, -979.8915, 23.7985>>, <<37.0, 37.0, 40.0>> ))
//				AND (IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehTruck, <<50.0, 50.0, 50.0>>))				
//					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.1)				
//				ELSE
//					SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(1.0)					
//				ENDIF
				
				IF bEventFlag[0] = TRUE
					IF NOT DOES_BLIP_EXIST(sLocatesData.LocationBlip)
						IF NOT IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(TRUE)
							IF GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(GET_SELECT_PED(SEL_FRANKLIN), "POLICE_PURSUIT_DRIVEN", "FRANKLIN_NORMAL")
							ENDIF							
						ENDIF
					ELSE
						IF IS_FACE_TO_FACE_CONVERSATION_PAUSED()
							PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
						ENDIF
					ENDIF
				ENDIF
				
				IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_MICHAEL), vDriveToPoint, <<150.0, 150.0, 150.0>>)
				AND bEventFlag[3] =  FALSE
					addTextToQueue("JH_NEARLYTHE", TEXT_TYPE_CONVO)
					TRIGGER_MUSIC_EVENT("JH2A_RADIO_FADE")
					bEventFlag[3] =  TRUE
				ENDIF
				
				IF IS_PLAYER_AT_LOCATION_WITH_BUDDY_IN_VEHICLE(	sLocatesData, <<-576.1586, -277.9921, 33.8018>>, <<0.1,0.1,1.5>>, TRUE,GET_SELECT_PED(SEL_FRANKLIN), vehCar, Asset.godDriveToStore,Asset.godFranklinLeft, Asset.godGetInTheCar, Asset.godGetBackInCar, FALSE, TRUE)
				OR (DOES_BLIP_EXIST(sLocatesData.LocationBlip) AND IS_ENTITY_IN_ANGLED_AREA( GET_SELECT_PED(SEL_MICHAEL), <<-574.134888,-283.237915,33.778194>>, <<-579.450562,-274.081299,37.272499>>, 6.750000))
					IF CAN_PLAYER_START_CUTSCENE(TRUE, TRUE)		
						STOP_AUDIO_SCENE("JSH_2B_GET_TO_STORE")
						//SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						
						NEW_LOAD_SCENE_START(<<-583.3762, -282.5733, 35.2789>>, <<-583.3762, -282.5733, 35.2789>> - <<-583.9982, -282.8672, 35.2799>>, 500.00)

						REPLAY_RECORD_BACK_FOR_TIME(6.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
						
						STOP_FIRE_IN_RANGE(<<-576.1586, -277.9921, 33.8018>>, 20.0)
						
						WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehCar, 6.0, 5)
							REPLAY_RECORD_BACK_FOR_TIME(3.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
							WAIT(0)
						ENDWHILE
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						SETTIMERB(0)
						iStageSection++
					ENDIF
				ENDIF
				
			BREAK
			
			CASE 3
			
				IF bIsStealthPath
					IF PREPARE_MUSIC_EVENT("JH2A_MISSION_START_OS")
						TRIGGER_MUSIC_EVENT("JH2A_MISSION_START_OS")					
						iStageSection++
					ENDIF
				ENDIF
			BREAK
			
			CASE 4
			
				REQUEST_VEHICLE_HIGH_DETAIL_MODEL(vehCar)
			
				IF TIMERB() > DEFAULT_CAR_STOPPING_TO_CUTSCENE
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "JH2BArrive")
				AND HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "JH2BArrive")
					
					KILL_ANY_CONVERSATION()
					
					CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
					DO_START_CUTSCENE()
					
					CLEAR_AREA(vDriveToPoint, 100.0, TRUE, TRUE)
										
					IF IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(vehCar), 29.7, 90.0)
						SET_PED_COORDS_KEEP_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), << -575.5158, -278.9760, 34.1733 >>)
						SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(GET_SELECT_PED(SEL_FRANKLIN)), 29.7163)	
					ELSE
						SET_PED_COORDS_KEEP_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), <<-575.4160, -277.5119, 34.1830>>)
						SET_ENTITY_HEADING(GET_VEHICLE_PED_IS_IN(GET_SELECT_PED(SEL_FRANKLIN)), 208.8154)
					ENDIF
					
					SET_VEHICLE_ON_GROUND_PROPERLY(GET_VEHICLE_PED_IS_IN(GET_SELECT_PED(SEL_FRANKLIN)))
					
					
					SET_CAM_FAR_DOF(initialCam, 9.0)			
					SET_CAM_NEAR_DOF(initialCam, 3.5)	
					SET_CAM_FAR_DOF(destinationCam, 9.0)			
					SET_CAM_NEAR_DOF(destinationCam, 3.5)	
					doTwoPointCam(	<<-584.773071,-283.175964,35.333061>>, <<1.308213,1.022223,-63.658546>>, 25.952829,
									<<-584.768188,-283.165619,34.936966>>,<<1.308213,1.022223,-63.658546>>, 25.952829,
									5000)
					
					TASK_LOOK_AT_ENTITY(GET_SELECT_PED(SEL_MICHAEL), GET_SELECT_PED(SEL_FRANKLIN), 5000, SLF_WHILE_NOT_IN_FOV)
					TASK_LOOK_AT_ENTITY(GET_SELECT_PED(SEL_FRANKLIN), GET_SELECT_PED(SEL_MICHAEL), 5000, SLF_WHILE_NOT_IN_FOV)
					runConversation(Asset.convCarLeave, TRUE)
						//Michael:	Give us the heads up when it's sleep time and we'll hit it.
					addTextToQueue(Asset.convRoofClimbStart, TEXT_TYPE_CONVO)
						//Franklin:	I got it.
					SETTIMERB(0)
					
					IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, FALSE)
						SET_PED_CREW(GET_SELECT_PED(SEL_MICHAEL))
						SET_PED_CREW(GET_SELECT_PED(SEL_FRANKLIN))
					ENDIF
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehVan)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehVan)
					ENDIF
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehTruck)
					ENDIF
					START_PLAYBACK_RECORDED_VEHICLE(vehVan,   1, "JH2BArrive")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehVan, 2000)
					START_PLAYBACK_RECORDED_VEHICLE(vehTruck, 2, "JH2BArrive")
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTruck, 4000)
					
					iStageSection++									//STAGE SECTION MOVE ON
				
				ENDIF
			BREAK
			
			CASE 5
			
				SET_USE_HI_DOF()
				
				CLEAR_AREA_OF_VEHICLES(<<-584.773071,-283.175964,35.333061>>, 40.0, FALSE)
			
				REQUEST_VEHICLE_HIGH_DETAIL_MODEL(vehCar)
			
				IF IS_VEHICLE_DRIVEABLE(vehCar)
					IF IS_PED_SITTING_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehCar)
						IF IS_VEHICLE_STOPPED(vehCar)
						
							REPLAY_RECORD_BACK_FOR_TIME(3.0)
						
							CLEAR_PED_TASKS(GET_SELECT_PED(SEL_FRANKLIN))
							
							TASK_LOOK_AT_ENTITY(GET_SELECT_PED(SEL_FRANKLIN),GET_SELECT_PED(SEL_MICHAEL), 4000, SLF_WHILE_NOT_IN_FOV)
							
							//REMOVE_PED_FROM_GROUP(GET_SELECT_PED(SEL_FRANKLIN))
							OPEN_SEQUENCE_TASK(seqSequence)
								TASK_LEAVE_VEHICLE(NULL, vehCar)
								TASK_PAUSE(NULL, 0)
								TASK_ACHIEVE_HEADING(NULL, 108.7326)
							CLOSE_SEQUENCE_TASK(seqSequence)
							TASK_PERFORM_SEQUENCE(GET_SELECT_PED(SEL_FRANKLIN), seqSequence)
							CLEAR_SEQUENCE_TASK(seqSequence)
							iStageSection++		
						ENDIF
					ENDIF
				ENDIF
			BREAK

			CASE 6
				
				REQUEST_VEHICLE_HIGH_DETAIL_MODEL(vehCar)
				
				SET_USE_HI_DOF()
				IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
					IF IS_PED_ON_FOOT(GET_SELECT_PED(SEL_FRANKLIN))
						SETTIMERA(0)
						iStageSection=7
					ENDIF
				ENDIF
				
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED_WITH_DELAY()
					DO_SCREEN_FADE_OUT(500)
					WHILE IS_SCREEN_FADING_OUT()
						WAIT(0)
					ENDWHILE
					iStageSection = 7
				ENDIF
				
			BREAK
			
			CASE 7
			CASE 8
				
				REQUEST_VEHICLE_HIGH_DETAIL_MODEL(vehCar)
				
				SET_USE_HI_DOF()
				
				IF iStageSection=7
					IF TIMERA() > 500
					
						IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) = CAM_VIEW_MODE_FIRST_PERSON)					
							ANIMPOSTFX_PLAY("CamPushInNeutral", 0, FALSE)
							PLAY_SOUND_FRONTEND(-1, "1st_Person_Transition", "PLAYER_SWITCH_CUSTOM_SOUNDSET")
						ENDIF
					
						iStageSection=8
					ENDIF
				ENDIF					
				
				IF TIMERA() > 1000//750
					iStageSection=9
				ENDIF
				
				IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
					DO_SCREEN_FADE_OUT(500)
					WHILE IS_SCREEN_FADING_OUT()
						WAIT(0)
					ENDWHILE
					iStageSection = 9
				ENDIF
				
			BREAK
			
			CASE 9
			
				REQUEST_VEHICLE_HIGH_DETAIL_MODEL(vehCar)
			
				IF IS_VEHICLE_DRIVEABLE(vehCar)
					TASK_VEHICLE_DRIVE_TO_COORD(GET_SELECT_PED(SEL_MICHAEL), vehCar, vDriveAIPoint, 24, DRIVINGSTYLE_NORMAL, Asset.mnCar, DRIVINGMODE_AVOIDCARS, 5, 1)
				ENDIF
				IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
					IF IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(vehCar), 29.7, 90.0)
						SET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN), << -573.5828, -278.3935, 34.1906 >>)
						SET_ENTITY_HEADING(GET_SELECT_PED(SEL_FRANKLIN),105.0)
					ELSE
						SET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN), <<-576.8370, -279.6357, 34.2772>>)
						SET_ENTITY_HEADING(GET_SELECT_PED(SEL_FRANKLIN),108.7326)
					ENDIF
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				ENDIF
				FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_SELECT_PED(SEL_FRANKLIN))
				DO_END_CUTSCENE(FALSE)
				
				IF (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_FOOT) <> CAM_VIEW_MODE_FIRST_PERSON)					
					DO_SWITCH_EFFECT(SEL_FRANKLIN)				
				ENDIF
				
				IF IS_SCREEN_FADED_OUT()
				OR IS_SCREEN_FADING_OUT()
					WHILE IS_SCREEN_FADING_OUT()
						WAIT(0)
					ENDWHILE
					DO_SCREEN_FADE_IN(500)
				ENDIF
								
				iStageSection++
			BREAK
			
		ENDSWITCH
	
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 10
			IF DOES_BLIP_EXIST(blipLocate)
				REMOVE_BLIP(blipLocate)
			ENDIF
			IF DOES_BLIP_EXIST(blipBike)
				REMOVE_BLIP(blipBike)
			ENDIF
			
			IF IS_NEW_LOAD_SCENE_ACTIVE()
				NEW_LOAD_SCENE_STOP()
			ENDIF
			
			REMOVE_VEHICLE_RECORDING(1, "JH2BArrive")
			REMOVE_VEHICLE_RECORDING(2, "JH2BArrive")
			REMOVE_VEHICLE_RECORDING(1, "JHBENSONEXIT")
				
			FOR iCounter = 0 TO 2
				IF DOES_BLIP_EXIST(blipCrew[iCounter])
					REMOVE_BLIP(blipCrew[iCounter])
				ENDIF
			ENDFOR
			
//			IF NOT HAS_SOUND_FINISHED(iSwitchSoundEffect)
//				STOP_SOUND(iSwitchSoundEffect)
//			ENDIF
						
			ADD_ALL_PEDS_TO_DIALOGUE()
			
			CLEAR_MISSION_LOCATE_STUFF(sLocatesData)
						
			bStageSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//BOOL bClearedHelpText
INT iLastTimeToldToGoAWay
//INT iPreStreamCutscene
BOOL bToldHowToSelectFromSlot
BOOL bSaidMissed = TRUE
BOOL bSoundPlayedForMiss

WEAPON_TYPE wtCurrentWeapon

VECTOR vMissPosition
				
BOOL bToldHowToThrowBZGas = FALSE
BOOL bLoadJewelInt = FALSE
BOOL bSoundPLayedForHit = FALSE
INT iTimeRoofSectionStarted
BOOL bDialogueHit

//PURPOSE:		Performs the section where Franklin climbs onto the roof and puts canisters into the vents
FUNC BOOL stageRoof()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
			
		SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "STAGE_ROOF")
		g_replay.iReplayInt[CURRENT_TAKE_REPLAY_VALUE] = 0
		//CLEANUP_VEHICLE(vehVan, TRUE)
		CLEANUP_VEHICLE(vehTruck, TRUE)
		
		CLEANUP_PED(pedCrew[CREW_HACKER], TRUE)
		CLEANUP_PED(pedCrew[CREW_HACKER], TRUE)
		
		CLEANUP_PED(pedCrew[CREW_GUNMAN_NEW], TRUE)
		CLEANUP_PED(pedCrew[CREW_DRIVER_NEW], TRUE)
		
		CLEANUP_PED(pedLester, TRUE)
		
		RESET_NEED_LIST()
		SET_ANIM_DICT_AS_NEEDED(Asset.adJewelHeist)
		SET_MODEL_AS_NEEDED(PROP_LD_TEST_01)
		SET_MODEL_AS_NEEDED(Asset.mnVan)
		//MANAGE_LOADING_NEED_LIST()
		
		WHILE NOT MANAGE_LOADING_NEED_LIST_SPEEDY()
			WAIT(0)
		ENDWHILE
		
		//Put Van outside store ready to go.....
		IF NOT DOES_ENTITY_EXIST(vehVan)
			vehVan = CREATE_VEHICLE(Asset.mnVan, <<-651.1321, -234.4142, 36.5393>>, 75.5169)
			SETUP_VAN()
		ENDIF
		IF NOT IS_ENTITY_DEAD(vehVan)
			STOP_PLAYBACK_RECORDED_VEHICLE(vehvan)
			SET_ENTITY_COORDS(vehVan, <<-651.1321, -234.4142, 36.5393>>)
			SET_ENTITY_HEADING(vehVan, 75.5169)
		ENDIF
		
		//SAFE_SETUP_STORE_PEDS(FALSE, FALSE, FALSE)
		
		//ASSISTED_MOVEMENT_REQUEST_ROUTE(Asset.assistRooftop[0])
		//ASSISTED_MOVEMENT_REQUEST_ROUTE(Asset.assistRooftop[1])
		//ASSISTED_MOVEMENT_REQUEST_ROUTE(Asset.assistRooftop[2])
		//ASSISTED_MOVEMENT_REQUEST_ROUTE(Asset.assistRooftop[3])
		
		addTextToQueue(Asset.godGetOntoRoof, TEXT_TYPE_GOD)
		
		IF DOES_BLIP_EXIST(blipLocate)
			REMOVE_BLIP(blipLocate)
		ENDIF
		blipLocate = CREATE_BLIP_FOR_COORD(vRoofRoute[5])
				
		PlaySoundFromCoord(SND_ROOFTOP_AIRCON, Asset.soundRoofAircon, <<-622.4, -233.6, 57.7>>)
		
		SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
		
		IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
			SET_PED_CREW(GET_SELECT_PED(SEL_MICHAEL))
			SET_PED_CREW(GET_SELECT_PED(SEL_FRANKLIN))
			
			SETUP_PLAYER_PEDS_SUITS(TRUE)
			SET_PED_BAG(GET_SELECT_PED(SEL_MICHAEL), TRUE, TRUE)
			SET_PED_HEADGEAR(GET_SELECT_PED(SEL_MICHAEL), TRUE)
			
		ENDIF
		
//		IF GET_AMMO_IN_PED_WEAPON(GET_SELECT_PED(SEL_FRANKLIN), WEAPONTYPE_BZGAS) < 5//GET_PED_AMMO_BY_TYPE(GET_SELECT_PED(SEL_FRANKLIN), AMMOTYPE_SMOKE_GRENADE) < 4
//			SET_PED_AMMO(GET_SELECT_PED(SEL_FRANKLIN), WEAPONTYPE_BZGAS, 0)
//			REMOVE_WEAPON_FROM_PED(GET_SELECT_PED(SEL_FRANKLIN), WEAPONTYPE_BZGAS)	
//		ENDIF

		GIVE_WEAPON_TO_PED(GET_SELECT_PED(SEL_FRANKLIN), WEAPONTYPE_BZGAS, 5 - GET_AMMO_IN_PED_WEAPON(GET_SELECT_PED(SEL_FRANKLIN), WEAPONTYPE_BZGAS), FALSE, FALSE)
		
		bHasSmokeGrenades = TRUE

		TASK_CLEAR_LOOK_AT(GET_SELECT_PED(SEL_FRANKLIN))
		
		SET_VEHICLE_MODEL_IS_SUPPRESSED(Asset.mnCar, FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(Asset.mnVan, FALSE)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(Asset.mnTruck, FALSE)
		
		ENABLE_ALL_DISPATCH_SERVICES(FALSE)
				
		ADD_ALL_PEDS_TO_DIALOGUE()
				
		SAFE_FADE_IN()
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
		
		iTimeRoofSectionStarted = GET_GAME_TIMER()
		
		iStageSection = 0
		bStageSetup = TRUE
		//bClearedHelpText = FALSE
		bToldHowToThrowBZGas = FALSE
		//iPreStreamCutscene = 0
	
		SAFE_REQUEST_INTERIOR_AT_COORDS_WITH_TYPE(interiorJewelStore, << -625.1706, -232.8408, 39.4937 >>, "V_JEWEL2", FALSE, FALSE)
		bSoundPLayedForHit = FALSE
		
		REQUEST_AMBIENT_AUDIO_BANK("JWL_HEIST_GRENADES")
		
		bLoadJewelInt = FALSE
		//═════════╡ UPDATE ╞═════════		
	ELSE
	
//		IF IS_NEW_LOAD_SCENE_LOADED()
//			SCRIPT_ASSERT("pop")
//		ENDIF
		
		IF DOES_ENTITY_EXIST(vehCar)
			IF NOT IS_ENTITY_DEAD(vehCar)
				IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehCar), GET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN))) > 50.0
					IF NOT IS_ENTITY_ON_SCREEN(vehCar)
						DELETE_VEHICLE(vehCar)
						SET_MODEL_AS_NO_LONGER_NEEDED(PRIMO)
						SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnCar)
						
						SET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL), <<-661.0373, -222.0719, 36.7327>>)
						SET_ENTITY_HEADING(GET_SELECT_PED(SEL_MICHAEL), 80.2333)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		IF iStageSection <= 5
//			IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_ CIRCLE)
//				IF NOT IS_GAMEPLAY_HINT_ACTIVE()
//					SET_GAMEPLAY_COORD_HINT(vRoofRoute[iStageSection], 1000000)
//				ENDIF
//			ELSE
//				STOP_GAMEPLAY_HINT()
//			ENDIF
			IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), <<-631.596985,-237.634689,38.877033>>,<<3.00000,3.000000,1.812500>>)
			AND (GET_GAME_TIMER() - iLastTimeToldToGoAWay) > 10000
				controlJewelStoreDoors(TRUE, 0.0)	
				IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), <<-631.596985,-237.634689,38.877033>>,<<1.500000,1.500000,1.812500>>)
					IF NOT isTextPlaying(Asset.convGoAway) AND NOT isTextWaitingInQueue(Asset.convGoAway)
						runConversation(Asset.convGoAway, TRUE, TRUE, TRUE)
						iLastTimeToldToGoAWay = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		
		//Cycle through the locates to see if the player skips a section.
		FOR iCounter = 0 TO 5
			IF iStageSection < iCounter
				IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), vRoofRoute[iCounter], <<2,2,1.5>>, (iCounter = 5))		
					iStageSection = iCounter
				ENDIF
			ENDIF
		ENDFOR
		
//		SWITCH iPreStreamCutscene
//		
//			CASE 0
//				SET_PED_BAG(GET_SELECT_PED(SEL_MICHAEL), TRUE, TRUE)
//				SET_PED_HEADGEAR(GET_SELECT_PED(SEL_MICHAEL), TRUE)
//				REQUEST_CUTSCENE(Asset.mocapRooftopGasKo)
//				iPreStreamCutscene++
//			BREAK
//			
//			CASE 1
//				IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
//					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", GET_SELECT_PED(SEL_MICHAEL))	
//					iPreStreamCutscene++
//				ENDIF
//			BREAK
//			
//		ENDSWITCH
							
		SWITCH iStageSection
		
			CASE 0
				IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), vRoofRoute[0], <<2,2,1.5>>)		
				OR IS_ENTITY_IN_ANGLED_AREA(GET_SELECT_PED(SEL_FRANKLIN), <<-584.539978,-289.948761,34.454720>>, <<-591.398926,-279.327881,39.777760>>, 13.250000)
					//SET_PED_STEALTH_MOVEMENT(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
					//SET_PED_USING_ACTION_MODE(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
					//REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipLocate)			
					
					IF IS_AUDIO_SCENE_ACTIVE("JSH_2A_GET_TO_STORE")
						STOP_AUDIO_SCENE("JSH_2A_GET_TO_STORE")
					ENDIF
					START_AUDIO_SCENE("JSH_2A_GET_TO_ROOF")
					
					addTextToQueue("JH_SHOUT", TEXT_TYPE_CONVO)
					
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
					
					iStageSection++
					//blipLocate = CREATE_BLIP_FOR_COORD(vRoofRoute[iStageSection])
				ENDIF
			BREAK
		
			CASE 1
				IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), vRoofRoute[1], <<2,2,1.5>>)			
					//REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipLocate)			
					addTextToQueue(Asset.convRoofClear, TEXT_TYPE_CONVO)
															
					iStageSection++
					//blipLocate = CREATE_BLIP_FOR_COORD(vRoofRoute[iStageSection])
				ENDIF
			BREAK
		
			CASE 2
				IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), vRoofRoute[2], <<3,3,1.5>>)			
					//REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipLocate)			
					//runHelpText(Asset.hlpLadder)
					
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
					
					TASK_LOOK_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), <<-595.1260, -293.2816, 46.5502>>, 5000, SLF_WHILE_NOT_IN_FOV)
					iStageSection++
					//blipLocate = CREATE_BLIP_FOR_COORD(vRoofRoute[iStageSection])
				ENDIF
			BREAK
			
			CASE 3			
				IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), vRoofRoute[3], <<2,2,1.5>>)			
					//REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipLocate)			
					iStageSection++
					
					STOP_AUDIO_SCENE("JSH_2A_GET_TO_ROOF")
					
					START_AUDIO_SCENE("JSH_2A_GET_ACROSS_ROOF")

					REPLAY_RECORD_BACK_FOR_TIME(3.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
				
					//blipLocate = CREATE_BLIP_FOR_COORD(vRoofRoute[iStageSection])
				ENDIF
			BREAK
			
			CASE 4
				IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), vRoofRoute[4], <<4,4,1.5>>)		
			    AND CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", Asset.convRoof, CONV_PRIORITY_VERY_HIGH)
					
					//Franklin:	Nearly there. Get ready to roll.
					//Michael:	Make sure the package is delivered.
					//Franklin:	It gonna be delivered alright.
					//REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipLocate)		
					//If Franklin has grenades warn him to get them mixed up.
					IF HAS_PED_GOT_WEAPON(GET_SELECT_PED(SEL_FRANKLIN), WEAPONTYPE_GRENADE)
					AND GET_PED_AMMO_BY_TYPE(GET_SELECT_PED(SEL_FRANKLIN), GET_PED_AMMO_TYPE_FROM_WEAPON(GET_SELECT_PED(SEL_FRANKLIN), WEAPONTYPE_GRENADE)) > 0
					//AND GET_PED_AMMO_TYPE_FROM_WEAPON(
						addTextToQueue("JH_GRENADE", TEXT_TYPE_CONVO)
					ENDIF	
					
					CLEAR_AREA_OF_PEDS(GET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN)), 200.0)
					
					REPLAY_RECORD_BACK_FOR_TIME(3.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					
					iStageSection++
					//blipLocate = CREATE_BLIP_FOR_COORD(vRoofRoute[iStageSection])
				ENDIF
			BREAK
			
			CASE 5
				//SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
							
				//IF IS_ENTITY_IN_ANGLED_AREA(GET_SELECT_PED(SEL_FRANKLIN), <<-616.552368,-240.060303,51.884335>>, <<-623.860840,-227.209869,61.250866>>, 11.000000)
				//IF IS_ENTITY_IN_ANGLED_AREA(GET_SELECT_PED(SEL_FRANKLIN), <<-618.686646,-230.485168,54.487331>>, <<-626.151672,-234.674057,59.014927>>, 9.000000)
				//IF IS_ENTITY_IN_ANGLED_AREA(GET_SELECT_PED(SEL_FRANKLIN), <<-619.592712,-237.722092,55.313305>>, <<-626.422607,-226.049744,58.896301>>, 7.750000)
				
				//Draw locate in correct position
				IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), vRoofRoute[5], <<3.5, 3.5, 1.75>>, TRUE)
				//Check actual locate.
				IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), <<-625.9125, -216.1734, 60.4256>>, <<3.5, 3.5, 1.75>>, FALSE)
				AND NOT IS_PLAYER_CLIMBING(PLAYER_ID())
					REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipLocate)		
					
					STOP_AUDIO_SCENE("JSH_2A_GET_ACROSS_ROOF")
					
					START_AUDIO_SCENE("JSH_2A_THROW_GAS")			
					
					//SET_CURRENT_PED_WEAPON(GET_SELECT_PED(SEL_FRANKLIN), WEAPONTYPE_BZGAS, TRUE)
					//Tell player to equip BZ gas
					PRINT_HELP("JH_GRENHELP1")
											
					addTextToQueue("JH_ANGLE", TEXT_TYPE_CONVO)
					addTextToQueue("JWL_VENT", TEXT_TYPE_GOD)
						
					//PRINT_NOW("JWL_VENT", DEFAULT_GOD_TEXT_TIME, 1)
					
					oiACTarget = CREATE_OBJECT(PROP_LD_TEST_01,  <<-622.431091,-233.654831,58.412594>> )
					FREEZE_ENTITY_POSITION(oiACTarget, TRUE)
					SET_ENTITY_COLLISION(oiACTarget, FALSE)
					SET_OBJECT_TARGETTABLE(oiACTarget, TRUE)
					SET_ENTITY_IS_TARGET_PRIORITY(oiACTarget, TRUE)
					
					TASK_LOOK_AT_ENTITY(GET_SELECT_PED(SEL_FRANKLIN), oiACTarget, 60000, SLF_WHILE_NOT_IN_FOV)
					
					STOP_PED_SPEAKING(GET_SELECT_PED(SEL_MICHAEL), TRUE)
					
					blipAirVent = CREATE_BLIP_FOR_OBJECT(oiACTarget)
					SET_BLIP_COLOUR(blipAirVent, BLIP_COLOUR_GREEN)
										
					SET_MAX_WANTED_LEVEL(0)
					//bToldHowToSelectFromSlot = FALSE					
					iStageSection = 6
				ENDIF	
			BREAK
			
			CASE 6
				IF IS_HUD_COMPONENT_ACTIVE(NEW_HUD_WEAPON_WHEEL)
					IF HUD_GET_WEAPON_WHEEL_CURRENTLY_HIGHLIGHTED() = WEAPONTYPE_GRENADE
						IF bToldHowToSelectFromSlot = FALSE
							PRINT_HELP("JH_GRENHELPG")					
							bToldHowToSelectFromSlot = TRUE
						ENDIF
					ELIF HUD_GET_WEAPON_WHEEL_CURRENTLY_HIGHLIGHTED()	 = WEAPONTYPE_BZGAS
						iStageSection++
					ENDIF
				ENDIF
			BREAK
			
			CASE 7
				GET_CURRENT_PED_WEAPON(GET_SELECT_PED(SEL_FRANKLIN), wtCurrentWeapon)
				IF wtCurrentWeapon = WEAPONTYPE_BZGAS
				AND bToldHowToThrowBZGas = FALSE
					PRINT_HELP("JH_GRENHELP2")
					bToldHowToThrowBZGas = TRUE
				ENDIF				
			BREAK
			
			CASE 8
				IF TIMERA() > 500
					IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_CANIN", CONV_PRIORITY_VERY_HIGH )
						iStageSection++
					ENDIF
				ENDIF
			BREAK
			
			CASE 9
				IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					iStageSection++
				ENDIF
			BREAK
			
			CASE 10
				//Go to next section
			BREAK
			
		ENDSWITCH
		
		IF iStageSection >= 3
			
			IF bLoadJewelInt = FALSE
				IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_FRANKLIN), vRoofRoute[5], <<20.5, 20.5, 15.75>>, TRUE)
					NEW_LOAD_SCENE_START_SPHERE(<<-623.7916, -232.2508, 38.3262>>, 35.0)
					bLoadJewelInt = TRUE
				ENDIF
			ENDIF
			SUPPRESS_SHOCKING_EVENTS_NEXT_FRAME()
			SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_Refit"), -587.171, -286.01, 30, 2)
			SET_RADAR_ZOOM(0)
		ENDIF
		
		IF DOES_ENTITY_EXIST(oiACTarget)
		AND iStageSection < 8	
			IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<-622.4559, -233.6758, 56.7967>>, <<0.1, 0.1, 0.1>>, (NOT bSoundPlayedForHit))
		ENDIF
		
		//IF IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-624.487488,-229.292999,59.156681>>, <<-620.039307,-237.223618,29.659813>>, 4.000000, WEAPONTYPE_BZGAS, TRUE)
		IF iStageSection < 8	
			IF IS_PROJECTILE_TYPE_WITHIN_DISTANCE(<<-622.431091,-233.654831,58.412594>>, WEAPONTYPE_BZGAS, 1.0, TRUE)
				IF REQUEST_AMBIENT_AUDIO_BANK("JWL_HEIST_GRENADES")
					IF bSoundPlayedForHit = FALSE
						PLAY_SOUND_FROM_COORD(-1, "Grenade_Throw_Success", <<-622.431091,-233.654831,58.412594>>, "JEWEL_HEIST_SOUNDS")
						CLEAR_PRINTS()
						
						REPLAY_RECORD_BACK_FOR_TIME(8.0, 3.0, REPLAY_IMPORTANCE_HIGHEST)
						
						bDialogueHit = FALSE
						bSoundPlayedForHit = TRUE
					ENDIF					
				ENDIF
			ELSE
				IF NOT IS_PROJECTILE_TYPE_WITHIN_DISTANCE(<<-622.431091,-233.654831,58.412594>>, WEAPONTYPE_BZGAS, 1.4, TRUE)
					//Incase it bounces out
					bSoundPlayedForHit = FALSE
				ENDIF
			ENDIF
			
			IF IS_EXPLOSION_IN_SPHERE(EXP_TAG_BZGAS, <<-622.431091,-233.654831,58.412594>>, 1.500000)
				SETTIMERA(0)
				iStageSection = 8
			ENDIF
		ENDIF
		
		
		IF bSoundPlayedForHit = TRUE
		AND bDialogueHit = FALSE
			IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_LANDG", CONV_PRIORITY_VERY_HIGH )
				bDialogueHit = TRUE
			ENDIF
		ENDIF
		
		IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_ATTACK)
		AND NOT IS_PROJECTILE_TYPE_WITHIN_DISTANCE(GET_ENTITY_COORDS(PLAYER_PED_ID()), WEAPONTYPE_BZGAS, 2.0, TRUE)//Thrown 2m away
		AND iStageSection < 8	
			iTimeOfThrowing = GET_GAME_TIMER()
			//SCRIPT_ASSERT("yup")
			//CLEAR_AREA_OF_PROJECTILES(<<-622.4338, -233.6392, 56.5750>>, 50.0)
			bSoundPlayedForMiss = FALSE
			bSaidMissed = FALSE
		ENDIF
		
		IF bSaidMissed = FALSE
		AND iStageSection < 8	
			//SCRIPT_ASSERT("i see")
			IF GET_GAME_TIMER() - iTimeOfThrowing > 4200
				//SCRIPT_ASSERT("in time window")
				IF IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-587.745422,-295.559692,-34.072205>>, <<-638.567200,-203.272400,165.577709>>, 33.250000, WEAPONTYPE_BZGAS, TRUE)
				AND NOT IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-620.695801,-236.471100,58.137489>>, <<-624.116943,-230.833542,59.137512>>, 3.000000, WEAPONTYPE_BZGAS, TRUE)				
				AND NOT IS_EXPLOSION_IN_SPHERE(EXP_TAG_BZGAS, <<-622.431091,-233.654831,58.412594>>, 1.500000)
					//SCRIPT_ASSERT("in area")
					
					IF bSoundPlayedForMiss = FALSE
						GET_COORDS_OF_PROJECTILE_TYPE_WITHIN_DISTANCE(PLAYER_PED_ID(), WEAPONTYPE_BZGAS,  30.500000, vMissPosition)
						PLAY_SOUND_FROM_COORD(-1, "Grenade_Throw_Miss", vMissPosition, "JEWEL_HEIST_SOUNDS")
//						PLAY_SOUND_FROM_COORD(-1, "Grenade_Throw_Miss", <<-622.431091,-233.654831,58.412594>>, "JEWEL_HEIST_SOUNDS")
						bSoundPlayedForMiss = TRUE
					ENDIF
					
					//If too far
					IF IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-622.451050,-233.720657,59.387516>>, <<-609.813904,-256.923279,51.043133>>, 20.500000, WEAPONTYPE_BZGAS, TRUE)
						IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_FAR", CONV_PRIORITY_VERY_HIGH )
							bSaidMissed = TRUE
						ENDIF
					//Too shot
					ELIF IS_PROJECTILE_TYPE_IN_ANGLED_AREA(<<-622.451050,-233.720657,59.387516>>, <<-630.006714,-220.071609,52.294014>>, 20.500000, WEAPONTYPE_BZGAS, TRUE)
						IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_SHORT", CONV_PRIORITY_VERY_HIGH )
							bSaidMissed = TRUE
						ENDIF
					ELSE
						IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_MISS", CONV_PRIORITY_VERY_HIGH )
							bSaidMissed = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	
		IF (GET_GAME_TIMER() - iTimeRoofSectionStarted) > 70000
		AND bSoundPlayedForHit = FALSE
			addTextToQueue("JH_IMP", TEXT_TYPE_CONVO)
			addTextToQueue("JH_CHILL", TEXT_TYPE_CONVO)
			iTimeRoofSectionStarted = GET_GAME_TIMER()				
		ENDIF

				
		UPDATE_BIRDS()		
	
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 10
		
			CLEANUP_VEHICLE(vehVan, TRUE)
		
			SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
			REMOVE_BLIP(blipAirVent)
			controlJewelStoreDoors(FALSE, 0.0)	
			CLEANUP_OBJECT(oiACTarget)
			IF IS_VEHICLE_DRIVEABLE(vehCar)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vehCar)
			ENDIF

			IF DOES_BLIP_EXIST(blipLocate)
				REMOVE_BLIP(blipLocate)
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("JSH_2A_GET_TO_ROOF")
				STOP_AUDIO_SCENE("JSH_2A_GET_TO_ROOF")
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("JSH_2A_GET_TO_ROOF")
				STOP_AUDIO_SCENE("JSH_2A_GET_ACROSS_ROOF")
			ENDIF
			IF IS_AUDIO_SCENE_ACTIVE("JSH_2A_THROW_GAS")
				STOP_AUDIO_SCENE("JSH_2A_THROW_GAS")
			ENDIF
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
				STOP_PED_SPEAKING(GET_SELECT_PED(SEL_MICHAEL), FALSE)
			ENDIF
			bStageSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

BOOL bSetUpCrewSuits
BOOL bSetUpDriver = FALSE
BOOL bSetUpGunman = FALSE
BOOL bCutscenePlaying = FALSE

//PURPOSE:		Performs the cutscene where Franklin drops a grenade into the store from the roof
FUNC BOOL stageCutsceneGasStore()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
		
		CLEANUP_PED(pedCrew[CREW_GUNMAN_NEW], TRUE)
		CLEANUP_PED(pedCrew[CREW_DRIVER_NEW], TRUE)
		
		//Stop scenarios in jewel store.
		ADD_SCENARIO_BLOCKING_AREA(<< -655.1706, -272.8408, 9.4937 >>, << -575.1706, -152.8408, 69.4937 >>)
		SET_PED_NON_CREATION_AREA(<< -655.1706, -272.8408, 9.4937 >>, << -575.1706, -152.8408, 69.4937 >>)
		
		////SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(GET_REPLAY_STAGE_FROM_MISSION_STAGE(STAGE_ROOF))
		
		INT i 
		FOR i = 0 TO 3
			DELETE_PED(s_birds[i].ped)
		ENDFOR
		REMOVE_ANIM_DICT(str_bird_anims)
		SET_MODEL_AS_NO_LONGER_NEEDED(model_bird)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("SEAGULLS")
		
		RESET_NEED_LIST()
		SET_ANIM_DICT_AS_NEEDED(Asset.adJewelHeist)
		SET_MODEL_AS_NEEDED(ASset.mnGunman)
		SET_MODEL_AS_NEEDED(ASset.mnDriver)
		//MANAGE_LOADING_NEED_LIST()
		
		REQUEST_WEAPON_ASSET(mnMainWeaponType, ENUM_TO_INT(WRF_REQUEST_ALL_ANIMS), WEAPON_COMPONENT_SUPP)
		
		REQUEST_PTFX_ASSET()
		
		WHILE NOT MANAGE_LOADING_NEED_LIST_SPEEDY()
		OR NOT HAS_WEAPON_ASSET_LOADED(mnMainWeaponType)
			SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_Refit"), -587.171, -286.01, 30, 2)
			WAIT(0)
		ENDWHILE
		
		//SAFE_SETUP_STORE_PEDS(FALSE, TRUE, FALSE)
		//SAFE_REQUEST_WEAPON_ASSET(mnMainWeaponType, TRUE, TRUE, WEAPON_COMPONENT_SUPP)
		//SAFE_REQUEST_MOCAP_CUTSCENE(Asset.mocapRooftopGasKo, TRUE, TRUE)
		
		SETUP_CREW_SUITS(TRUE)
		
		SET_PED_BAG(pedCrew[CREW_DRIVER_NEW], TRUE)
		SET_PED_BAG(pedCrew[CREW_GUNMAN_NEW], TRUE)
		
		SETUP_PLAYER_PEDS_SUITS(TRUE)
		SET_PED_BAG(GET_SELECT_PED(SEL_MICHAEL), TRUE, TRUE)
		SET_PED_HEADGEAR(GET_SELECT_PED(SEL_MICHAEL), TRUE)
		
		
		REQUEST_CUTSCENE(Asset.mocapRooftopGasKo)
		REQUEST_ACTION_MODE_ASSET("MICHAEL_ACTION")
		
		WHILE NOT HAS_CUTSCENE_LOADED()		
		OR NOT HAS_ACTION_MODE_ASSET_LOADED("MICHAEL_ACTION")
			SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_Refit"), -587.171, -286.01, 30, 2)
			safeWait(0)
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
      			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", GET_SELECT_PED(SEL_MICHAEL))	
			ENDIF
		ENDWHILE
		
		SAFE_REQUEST_INTERIOR_AT_COORDS_WITH_TYPE(interiorJewelStore, << -625.1706, -232.8408, 39.4937 >>, "V_JEWEL2", TRUE, TRUE)
		
		SET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL), << -630.2189, -236.5104, 37.0570 >>)
		SET_ENTITY_HEADING(GET_SELECT_PED(SEL_MICHAEL), 300.8775 )
		GIVE_WEAPON_TO_PED(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, 1000, TRUE)
		GIVE_WEAPON_COMPONENT_TO_PED_IF_2A(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, WEAPONCOMPONENT_AT_AR_SUPP_02)
		GIVE_WEAPON_COMPONENT_TO_PED_IF_2A(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, WEAPONCOMPONENT_AT_AR_SUPP_02)
		
		//GIVE_WEAPON_TO_PED(GET_SELECT_PED(SEL_FRANKLIN), mnMainWeaponType, 1000, TRUE)
			
		IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
			SET_ENTITY_COORDS(pedCrew[CREW_DRIVER_NEW], << -634.4145, -243.1337, 37.2759 >>)
			SET_ENTITY_HEADING(pedCrew[CREW_DRIVER_NEW], 38.2811)
			GIVE_WEAPON_TO_PED(pedCrew[CREW_DRIVER_NEW], mnMainWeaponType, 1000, TRUE)
			//SET_PED_BAG(pedCrew[CREW_DRIVER_NEW], TRUE, TRUE)
			SET_PED_HEADGEAR(pedCrew[CREW_DRIVER_NEW], TRUE)
		ENDIF
		IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
			SET_ENTITY_COORDS(pedCrew[CREW_GUNMAN_NEW], << -637.5032, -238.9592, 37.0954 >>)
			SET_ENTITY_HEADING(pedCrew[CREW_GUNMAN_NEW], 292.9207)
			GIVE_WEAPON_TO_PED(pedCrew[CREW_GUNMAN_NEW], mnMainWeaponType, 1000, TRUE)
			GIVE_WEAPON_COMPONENT_TO_PED_IF_2A(pedCrew[CREW_DRIVER_NEW], mnMainWeaponType, WEAPONCOMPONENT_AT_AR_SUPP_02)
			//SET_PED_BAG(pedCrew[CREW_GUNMAN_NEW], TRUE, TRUE)
			SET_PED_HEADGEAR(pedCrew[CREW_GUNMAN_NEW], TRUE)
		ENDIF
		
		SET_CURRENT_PED_WEAPON(GET_SELECT_PED(SEL_MICHAEL), WEAPONTYPE_UNARMED, TRUE)
		
						
		CLEAR_STORE_AREA_OF_PEDS()
		
					
//		IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxKOGasHaze)
//			STOP_PARTICLE_FX_LOOPED(ptfxKOGasHaze)
//			SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxKOGasHaze, "smoke", 1.0)
//		ENDIF
						
		//ADD_ALL_PEDS_TO_DIALOGUE()
		
		ADD_ALL_PEDS_TO_DIALOGUE()
		
		bEventFlag[0] = FALSE
		
		SAFE_FADE_IN()
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
		iStageSection = 0
		bCameraShotSetup = FALSE
		bStageSetup = TRUE
		bSetUpCrewSuits = FALSE
		SET_RADIO_POSITION_AUDIO_MUTE(TRUE)
		
		IF bIsStealthPath
			TRIGGER_MUSIC_EVENT("JH2A_GAS_SHOP_MA")
			
		ENDIF

//═════════╡ UPDATE ╞═════════		
	ELSE
		SWITCH iStageSection
			CASE 0
				
				//REGISTER_SELECT_PEDS_FOR_CUTSCENE(FALSE, TRUE, FALSE)
				REGISTER_ENTITY_FOR_CUTSCENE(GET_SELECT_PED(SEL_MICHAEL), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				//REGISTER_ENTITY_FOR_CUTSCENE(GET_SELECT_PED(SEL_FRANKLIN), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
				//REGISTER_ENTITY_FOR_CUTSCENE(NULL, "jewel_assistant_1_2customers", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,  Asset.mnShop[0])
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "customer_1_withassist_1", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,  A_M_Y_BEVHILLS_01) //Asset.mnShop[1])
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "shop_assistant", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,  A_F_Y_BUSINESS_02) //Asset.mnShop[2])
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "customer_2_withassist_2", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,  A_M_Y_BEVHILLS_01) //Asset.mnShop[3])
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "jewellery_Assitance", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY,  IG_JEWELASS)
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "security", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, Asset.mnSecurity)

				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "gunman_selection_1", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, ASset.mnGunman)
				REGISTER_ENTITY_FOR_CUTSCENE(NULL, "driver_selection", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, ASset.mnDriver)

				SET_CURRENT_PED_WEAPON(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, TRUE)	
				GIVE_WEAPON_COMPONENT_TO_PED(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, WEAPONCOMPONENT_AT_AR_SUPP_02) GIVE_WEAPON_COMPONENT_TO_PED_IF_2A(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, WEAPONCOMPONENT_AT_AR_SUPP_02)
				objWeapon = CREATE_WEAPON_OBJECT_FROM_CURRENT_PED_WEAPON_WITH_COMPONENTS(GET_SELECT_PED(SEL_MICHAEL))   
				REGISTER_ENTITY_FOR_CUTSCENE(objWeapon, "Michaels_2_handedweapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				
//				SET_CURRENT_PED_WEAPON(pedCrew[CREW_DRIVER_NEW], mnMainWeaponType, TRUE)				
				objWeapon1 = CREATE_WEAPON_OBJECT(mnMainWeaponType, INFINITE_AMMO, <<0.1, 1.0, 1.0>>, TRUE)         
				GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(objWeapon1, WEAPONCOMPONENT_AT_AR_SUPP_02)
				REGISTER_ENTITY_FOR_CUTSCENE(objWeapon1, "HC_GUNMAN_2_HANDEDWEAPON", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//				
//				SET_CURRENT_PED_WEAPON(pedCrew[CREW_GUNMAN_NEW], mnMainWeaponType, TRUE)				
				objWeapon2 = CREATE_WEAPON_OBJECT(mnMainWeaponType, INFINITE_AMMO, <<0.1, 1.0, 1.0>>, TRUE)         
				GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(objWeapon2, WEAPONCOMPONENT_AT_AR_SUPP_02)
				REGISTER_ENTITY_FOR_CUTSCENE(objWeapon2, "HC_GUNMAN1_2_HANDEDWEAPON", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
//		
			
				bSetUpDriver = FALSE
				bSetUpGunman = FALSE
			
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
				
				bMusicEvent1 = FALSE
				bMusicEvent2 = FALSE
				START_CUTSCENE()
				
				REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
				
				REQUEST_MODEL(Asset.mnJewels1)
				REQUEST_MODEL(Asset.mnJewels2)
				REQUEST_MODEL(Asset.mnJewels4a)
				REQUEST_MODEL(Asset.mnJewels4b)
						
				REQUEST_ANIM_DICT(Asset.adJewelHeist)
				REQUEST_ANIM_DICT(Asset.adWatch)
				REQUEST_ANIM_DICT(Asset.adJewelHeist)
				bCutscenePlaying = FALSE
				iStageSection++
			BREAK
			
			CASE 1
			CASE 2
			
				IF GET_CUTSCENE_TIME() > 14100.0
				AND bMusicEvent1 = FALSE
					IF bIsStealthPath
						IF PREPARE_MUSIC_EVENT("JH2A_GAS_SHOP_OS")
							TRIGGER_MUSIC_EVENT("JH2A_GAS_SHOP_OS")
							bMusicEvent1 = TRUE
						ENDIF
					ENDIF
					
				ENDIF
			
//				IF GET_CUTSCENE_TIME() > 19000.0	
//					IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxKOGasHaze)
//					AND HAS_PTFX_ASSET_LOADED()
//						ptfxKOGasHaze = START_PARTICLE_FX_LOOPED_AT_COORD("scr_jewel_haze", <<-624.29,-232.23,37.31>>, VECTOR_ZERO, 1)
//						SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxKOGasHaze, "smoke", 1.0) //"increase", 1.00)
//					ENDIF										
//				ENDIF
			
				IF GET_CUTSCENE_TIME() > 20000.0
				AND bMusicEvent2 = FALSE
				AND bMusicEvent1 = TRUE 
					IF bIsStealthPath
						IF PREPARE_MUSIC_EVENT("JH2A_ENTER_SHOP_MA")
							TRIGGER_MUSIC_EVENT("JH2A_ENTER_SHOP_MA")
							bMusicEvent2 = TRUE
						ENDIF
					ENDIF
				
				ENDIF
				
			
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklin"))
					sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN] = GET_PED_INDEX_FROM_ENTITY_INDEX((GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklin")))
				ENDIF
				
//				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("jewel_assistant_1_2customers"))
//					pedStore[0] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("jewel_assistant_1_2customers"))
//				ENDIF
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("customer_1_withassist_1"))
					pedStore[1] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("customer_1_withassist_1"))
				ENDIF
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("shop_assistant"))
					pedStore[2] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("shop_assistant"))
				ENDIF
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("customer_2_withassist_2"))
					pedStore[3] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("customer_2_withassist_2"))
				ENDIF
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("jewellery_Assitance"))
					pedStore[4] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("jewellery_Assitance"))
				ENDIF
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("security"))
					pedSecurity = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("security"))
				ENDIF
														
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("driver_selection"))
				AND bSetUpDriver = FALSE	
					pedCrew[CREW_DRIVER_NEW] = CREATE_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,  ENUM_TO_INT(CREW_DRIVER_NEW), <<100.0, 100.0, 100.0>>, 0.0)
					DELETE_PED(pedCrew[CREW_DRIVER_NEW])
					pedCrew[CREW_DRIVER_NEW] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("driver_selection"))
					SET_PED_BAG(pedCrew[CREW_DRIVER_NEW], TRUE)
					bSetUpDriver = TRUE	
				ENDIF	
				IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("gunman_selection_1"))
				AND bSetUpGunman = FALSE
					pedCrew[CREW_GUNMAN_NEW] = CREATE_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,  ENUM_TO_INT(CREW_GUNMAN_NEW), <<100.0, 100.0, 100.0>>, 0.0)
					DELETE_PED(pedCrew[CREW_GUNMAN_NEW])				
					pedCrew[CREW_GUNMAN_NEW] = GET_PED_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("gunman_selection_1"))
					SET_PED_BAG(pedCrew[CREW_GUNMAN_NEW], TRUE)
					bSetUpGunman = TRUE
				ENDIF
				
				IF bSetUpCrewSuits = FALSE
					IF  DOES_ENTITY_EXIST(pedCrew[CREW_DRIVER_NEW])
					AND DOES_ENTITY_EXIST(pedCrew[CREW_GUNMAN_NEW])
						SETUP_CREW_SUITS(TRUE)
						SET_PED_BAG(pedCrew[CREW_GUNMAN_NEW], TRUE)
						SET_PED_BAG(pedCrew[CREW_DRIVER_NEW], TRUE)
						bSetUpCrewSuits = TRUE
					ENDIF
				ENDIF

				IF CAN_SET_EXIT_STATE_FOR_CAMERA(TRUE)
					//SCRIPT_ASSERT("we did the flag")
					//RESET_GAME_CAMERA()
				ENDIF

				IF iStageSection = 1
					IF GET_CUTSCENE_TIME() > 22655
						//ANIMPOSTFX_PLAY("SwitchSceneNeutral", 1000, FALSE)
						iStageSection = 2
					ENDIF
				ENDIF	
//
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("P_jewel_Door_L")
//				ENDIF

				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
			
			
					IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)//, FALSE)
						SET_PED_CREW(GET_SELECT_PED(SEL_MICHAEL))
						SET_PED_CREW(GET_SELECT_PED(SEL_FRANKLIN))
					ENDIF
					
					SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)
					FORCE_PED_MOTION_STATE(GET_SELECT_PED(SEL_MICHAEL), MS_ACTIONMODE_WALK, TRUE, FAUS_CUTSCENE_EXIT)
					
					SIMULATE_PLAYER_INPUT_GAIT(PLAYER_ID(), PEDMOVE_WALK, 250)
					
					//FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_SELECT_PED(SEL_MICHAEL))
					//RESET_GAME_CAMERA()
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michaels_2_handedweapon")
					GIVE_WEAPON_OBJECT_TO_PED(objWeapon, GET_SELECT_PED(SEL_MICHAEL))		
				ENDIF
				
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("HC_GUNMAN_2_HANDEDWEAPON")
					IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
						GIVE_WEAPON_OBJECT_TO_PED(objWeapon1, pedCrew[CREW_DRIVER_NEW])
					ENDIF
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("HC_GUNMAN1_2_HANDEDWEAPON")
					IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
						GIVE_WEAPON_OBJECT_TO_PED(objWeapon2, pedCrew[CREW_GUNMAN_NEW])
					ENDIF
				ENDIF
				
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("jewel_assistant_1_2customers")
//					TASK_PLAY_ANIM_ADVANCED(pedStore[0], Asset.adJewelHeist, Asset.animGasAssistant, vJewelDoorCoords, vJewelDoorRot,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_ENDS_IN_DEAD_POSE, 0.8)
//					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedStore[0])
//				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("customer_1_withassist_1")
					TASK_PLAY_ANIM_ADVANCED(pedStore[1], Asset.adJewelHeist, Asset.animGas[0], vJewelDoorCoords, vJewelDoorRot,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,  AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_USE_KINEMATIC_PHYSICS|AF_ENDS_IN_DEAD_POSE, 0.8)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedStore[1])
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("shop_assistant")
					TASK_PLAY_ANIM_ADVANCED(pedStore[2], Asset.adJewelHeist, Asset.animGas[1], vJewelDoorCoords + <<0.0, 0.0, -0.85>>, vJewelDoorRot,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,  AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_ENDS_IN_DEAD_POSE, 0.8)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedStore[2])
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("customer_2_withassist_2")
					TASK_PLAY_ANIM_ADVANCED(pedStore[3], Asset.adJewelHeist, Asset.animGas[2], vJewelDoorCoords, vJewelDoorRot,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,  AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_ENDS_IN_DEAD_POSE, 0.8)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedStore[3])
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("jewellery_Assitance")
					TASK_PLAY_ANIM_ADVANCED(pedStore[4], Asset.adJewelHeist, Asset.animGas[3], vJewelDoorCoords, vJewelDoorRot,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1,  AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_USE_KINEMATIC_PHYSICS|AF_ENDS_IN_DEAD_POSE, 0.8)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedStore[4])
				ENDIF
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("security")
					TASK_PLAY_ANIM_ADVANCED(pedSecurity, Asset.adJewelHeist, Asset.animGasSecurity, vJewelDoorCoords, vJewelDoorRot,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_ENDS_IN_DEAD_POSE, 0.8)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSecurity)
				ENDIF
				
//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("gunman_selection_1")				
//					IF NOT IS_ENTITY_DEAD(pedCrew[cnSmashGunman])
//						SET_ENTITY_COORDS(pedCrew[cnSmashGunman], <<-629.47, -232.86, 37.0570>>)
//						SET_ENTITY_HEADING(pedCrew[cnSmashGunman], -118.52)
//					ENDIF
//				ENDIF
//				
				//IF NOT IS_CUTSCENE_ACTIVE()		
				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("gunman_selection_1")	
					IF NOT IS_ENTITY_DEAD(pedCrew[cnSmashGunman])
						SET_ENTITY_COORDS(pedCrew[cnSmashGunman], <<-631.8019, -234.8645, 37.0570>>)
						SET_ENTITY_HEADING(pedCrew[cnSmashGunman], 320.9840)
						SET_PED_USING_ACTION_MODE(pedCrew[cnSmashGunman], TRUE)
					ENDIF
				ENDIF
				
				IF IS_CUTSCENE_PLAYING()
				AND bCutscenePlaying = FALSE
					CLEAR_AREA(VECTOR_ZERO, 5000, TRUE, TRUE)	
					SET_CURRENT_PED_WEAPON(GET_SELECT_PED(SEL_FRANKLIN), WEAPONTYPE_UNARMED, FALSE)
					bCutscenePlaying = TRUE
				ENDIF	
						
				IF IS_CUTSCENE_ACTIVE() // HAS_CUTSCENE_FINISHED() //IS_CUTSCENE_ACTIVE()
					IF NOT bEventFlag[0]
				
						PlaySoundFromCoord(SND_GAS_ESCAPE, Asset.soundGasEscape, <<-623.02, -224.89, 42.27>>)
						bEventFlag[0] = TRUE
					ENDIF
					
					IF bAttemptToResetCases					//Use bAttempToResetCases to flag whether or not the reset was a success
						bAttemptToResetCases = FALSE
						FOR iCounter = 0 TO iNoOfCases-1
							rfReset = GET_RAYFIRE_MAP_OBJECT(	jcStore[iCounter].vCasePos, fRFCheckRadius,
																jcStore[iCounter].sSmashAnim)
							IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfReset)
								IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfReset) <> RFMO_STATE_START
									SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfReset, RFMO_STATE_RESET)
								ENDIF
							ELSE
								bAttemptToResetCases = TRUE
							ENDIF
						ENDFOR
					ENDIF
				ELSE
					
					SAFE_SETUP_STORE_PEDS(TRUE, FALSE, FALSE, FALSE, TRUE)
					
//					SET_ENTITY_COORDS(pedCrew[CREW_DRIVER_NEW], << -629.3820, -236.8453, 37.0570 >>)
//					SET_ENTITY_HEADING(pedCrew[CREW_DRIVER_NEW],  277.3676 )
//					
//					SET_ENTITY_COORDS(pedCrew[CREW_GUNMAN_NEW], << -630.3480, -237.3595, 37.0570 >>)
//					SET_ENTITY_HEADING(pedCrew[CREW_GUNMAN_NEW], 277.8713 )
//
//					SET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL), << -630.2189, -236.5104, 37.0570 >>)
//					SET_ENTITY_HEADING(GET_SELECT_PED(SEL_MICHAEL), 300.8775 )
					//RESET_GAME_CAMERA()
					
					
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					ADD_ALL_PEDS_TO_DIALOGUE()
					iStageSection++
				ENDIF
			BREAK
		ENDSWITCH
				
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 3
		
			DO_SWITCH_EFFECT(SEL_MICHAEL)
		
			ReleaseSound(SND_ROOFTOP_AIRCON)
			//SET_ENTITY_COORDS(pedCrew[CREW_DRIVER_NEW], GET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL))+<<0,-1,-1>>)
		
			//SAFE_SETUP_STORE_PEDS(TRUE, FALSE, TRUE, FALSE, TRUE)
		
			ADD_ALL_PEDS_TO_DIALOGUE()
			
			REMOVE_WEAPON_ASSET(mnMainWeaponType)
			
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
				SET_PED_CREW(GET_SELECT_PED(SEL_MICHAEL))
				SET_PED_CREW(GET_SELECT_PED(SEL_FRANKLIN))
			ENDIF
			
			REPLAY_STOP_EVENT()
			
			NEW_LOAD_SCENE_STOP()
			bStageSetup = FALSE
			bCameraShotSetup = FALSE
			////ANIMPOSTFX_STOP("SwitchSceneNeutral")
			//ANIMPOSTFX_PLAY("SwitchSceneMichael", 500, FALSE)
			
			RETURN TRUE
		ENDIF
		//ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


BOOL BSkippedBeforeCutStarted = TRUE

//PURPOSE:		Performs the cutscene where the crew burst into the store
FUNC BOOL stageCutsceneAggressiveArrive()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
		
		//Stop scenarios in jewel store.
		ADD_SCENARIO_BLOCKING_AREA(<< -655.1706, -272.8408, 9.4937 >>, << -575.1706, -152.8408, 69.4937 >>)
		SET_PED_NON_CREATION_AREA(<< -655.1706, -272.8408, 9.4937 >>, << -575.1706, -152.8408, 69.4937 >>)
			
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
		
		CLEANUP_VEHICLE(vehTruck, TRUE)
		IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(Asset.mnTruck)
				
		IF iArrivalAngle = 0
			iArrivalAngle = GET_RANDOM_INT_IN_RANGE(1, 4)
		ENDIF		
		
		IF iArrivalAngle <> 4
			REQUEST_VEHICLE_RECORDING(iArrivalAngle, "JHArrive")
		ENDIF
		REQUEST_WEAPON_ASSET(mnMainWeaponType, ENUM_TO_INT(WRF_REQUEST_ALL_ANIMS), WEAPON_COMPONENT_SUPP |WEAPON_COMPONENT_GRIP )
		REQUEST_MODEL(GET_WEAPON_COMPONENT_TYPE_MODEL(WEAPONCOMPONENT_AT_AR_SUPP))
		
		emptyTextQueue()
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
			
		SET_RADIO_POSITION_AUDIO_MUTE(TRUE)
		TRIGGER_MUSIC_EVENT("JH2B_RADIO_FADE")
		START_AUDIO_SCENE("JSH_2B_REVERSE_TRUCK_CUTSCENE")
		
		iStageSection = 0
		bCameraShotSetup = FALSE
		bStageSetup = TRUE
		
//		SET_MODEL_AS_NEEDED(Asset.mnSecurity)
//		FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
//			SET_MODEL_AS_NEEDED(Asset.mnShop[iCounter])
//		ENDFOR
//		SET_ANIM_DICT_AS_NEEDED(Asset.adJewelHeist)
//								
//		SET_VEHICLE_RECORDING_AS_NEEDED("JHArrive", 1)
//		SET_VEHICLE_RECORDING_AS_NEEDED("JHArrive", 2)
//		SET_VEHICLE_RECORDING_AS_NEEDED("JHArrive", 3)
//				
//		MANAGE_LOADING_NEED_LIST()
//				//SCRIPT_ASSERT("stopped waiting")
//		SAFE_REQUEST_WEAPON_ASSET(mnMainWeaponType, TRUE, TRUE, WEAPON_COMPONENT_GRIP)
				

		REQUEST_MODEL(Asset.mnSecurity)
		FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
			REQUEST_MODEL(Asset.mnShop[iCounter])
		ENDFOR
		REQUEST_ANIM_DICT(Asset.adJewelHeist)
								
		REQUEST_VEHICLE_RECORDING(1, "JHArrive")
		REQUEST_VEHICLE_RECORDING(2, "JHArrive")
		REQUEST_VEHICLE_RECORDING(3, "JHArrive")
				
//		REQUEST_WEAPON_ASSET(mnMainWeaponType, DEFAULT, WEAPON_COMPONENT_GRIP)	
			
								
		//SAFE_REQUEST_MOCAP_CUTSCENE(Asset.mocapFrontDoorSmash, TRUE, TRUE)

		SETUP_CREW_SUITS()
	
		GIVE_WEAPON_TO_PED(pedCrew[CREW_DRIVER_NEW], mnMainWeaponType, 1000, TRUE)
		SET_PED_BAG(pedCrew[CREW_DRIVER_NEW], TRUE, TRUE)
		
		GIVE_WEAPON_TO_PED(pedCrew[CREW_GUNMAN_NEW], mnMainWeaponType, 1000, TRUE)
		SET_PED_BAG(pedCrew[CREW_GUNMAN_NEW], TRUE, TRUE)
		
		GIVE_WEAPON_TO_PED(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, 1000, TRUE, TRUE)
		//GIVE_WEAPON_TO_PED(GET_SELECT_PED(SEL_FRANKLIN), mnMainWeaponType, 1000, TRUE)
		SET_PED_BAG(GET_SELECT_PED(SEL_MICHAEL), TRUE, TRUE)
		SET_PED_HEADGEAR(GET_SELECT_PED(SEL_MICHAEL), TRUE)
					
		SET_PED_HEADGEAR(pedCrew[CREW_DRIVER_NEW], TRUE)
		SET_PED_HEADGEAR(pedCrew[CREW_GUNMAN_NEW], TRUE)
		BSkippedBeforeCutStarted = FALSE
		REQUEST_CUTSCENE(Asset.mocapFrontDoorSmash)
		REQUEST_ACTION_MODE_ASSET("MICHAEL_ACTION")
			
		iStageSection = 0
//═════════╡ UPDATE ╞═════════		
	ELSE
	
		//IF NOT HAS_CUTSCENE_LOADED()		
		IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", GET_SELECT_PED(SEL_MICHAEL))	
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("gunman_selection_1", pedCrew[CREW_GUNMAN_NEW])	
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("driver_selection", pedCrew[CREW_DRIVER_NEW])	
			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", GET_SELECT_PED(SEL_FRANKLIN))
			
			//SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("angry_customer", pedStore[0])
				
			SET_CUTSCENE_PED_COMPONENT_VARIATION("angry_customer", INT_TO_ENUM(PED_COMPONENT,0), 0, 0) //(head)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("angry_customer", INT_TO_ENUM(PED_COMPONENT,2), 1, 0) //(hair)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("angry_customer", INT_TO_ENUM(PED_COMPONENT,3), 1, 0) //(uppr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("angry_customer", INT_TO_ENUM(PED_COMPONENT,4), 1, 0) //(lowr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("angry_customer", INT_TO_ENUM(PED_COMPONENT,8), 0, 0) //(accs)
			
			IF DOES_ENTITY_EXIST(pedStore[1])
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Jewellery_Assitance", pedStore[1])
			ENDIF
			IF DOES_ENTITY_EXIST(pedStore[4])
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("shop_assistant", pedStore[4])
			ENDIF
//			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("boyfriend", pedStore[3])
//			IF DOES_ENTITY_EXIST(pedStore[2])
//				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("customer", pedStore[2])				
//			ENDIF
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION("boyfriend", INT_TO_ENUM(PED_COMPONENT,0), 1, 0) //(head)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("boyfriend", INT_TO_ENUM(PED_COMPONENT,2), 1, 0) //(hair)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("boyfriend", INT_TO_ENUM(PED_COMPONENT,3), 0, 5) //(uppr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("boyfriend", INT_TO_ENUM(PED_COMPONENT,4), 1, 2) //(lowr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("boyfriend", INT_TO_ENUM(PED_COMPONENT,8), 0, 0) //(accs)
				
			
			SET_CUTSCENE_PED_COMPONENT_VARIATION("customer", INT_TO_ENUM(PED_COMPONENT,0), 0, 0) //(head)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("customer", INT_TO_ENUM(PED_COMPONENT,2), 0, 0) //(hair)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("customer", INT_TO_ENUM(PED_COMPONENT,3), 0, 1) //(uppr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("customer", INT_TO_ENUM(PED_COMPONENT,4), 1, 1) //(lowr)
			SET_CUTSCENE_PED_COMPONENT_VARIATION("customer", INT_TO_ENUM(PED_COMPONENT,8), 0, 0) //(accs)
			
		
			
		ENDIF
	
	
		IF bAttemptToResetCases							//Use bAttempToResetCases to flag whether or not the reset was a success
			bAttemptToResetCases = FALSE
			FOR iCounter = 0 TO iNoOfCases-1
				rfReset = GET_RAYFIRE_MAP_OBJECT(	jcStore[iCounter].vCasePos, fRFCheckRadius,
													jcStore[iCounter].sSmashAnim)
				IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfReset)
					IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfReset) <> RFMO_STATE_START
						SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfReset, RFMO_STATE_RESET)
					ENDIF
				ELSE
					bAttemptToResetCases = TRUE
				ENDIF
			ENDFOR
		ENDIF
	
		IF NOT IS_CUTSCENE_ACTIVE()
		OR HAS_CUTSCENE_FINISHED()

			IF IS_CUTSCENE_SKIP_BUTTON_JUST_PRESSED()
			AND iStageSection > 1
				
				DO_SCREEN_FADE_OUT(500)
				WHILE  IS_SCREEN_FADING_OUT()
					WAIT(0)
				ENDWHILE
				
				REMOVE_CUTSCENE()				
				
				REQUEST_MODEL(Asset.mnSecurity)
				FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
					REQUEST_MODEL(Asset.mnShop[iCounter])
				ENDFOR
				REQUEST_ANIM_DICT(Asset.adJewelHeist)
				
				WHILE NOT HAS_MODEL_LOADED(Asset.mnSecurity)
				OR NOT HAS_MODEL_LOADED(Asset.mnShop[0])
				OR NOT HAS_MODEL_LOADED(Asset.mnShop[1])
				OR NOT HAS_MODEL_LOADED(Asset.mnShop[2])
				OR NOT HAS_MODEL_LOADED(Asset.mnShop[3])
				OR NOT HAS_MODEL_LOADED(Asset.mnShop[4])
				OR NOT HAS_ANIM_DICT_LOADED(Asset.adJewelHeist)
				OR NOT HAS_ACTION_MODE_ASSET_LOADED("MICHAEL_ACTION")
					WAIT(0)
				ENDWHILE
				
				SAFE_REQUEST_INTERIOR_AT_COORDS_WITH_TYPE(interiorJewelStore, << -625.1706, -232.8408, 39.4937 >>, "V_JEWEL2", TRUE, FALSE)
				
				
				IF NOT IS_ENTITY_DEAD(vehVan)
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehVan)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehVan)
					ENDIF
				ENDIF
				
				SAFE_SETUP_STORE_PEDS(FALSE, TRUE)
				
					IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL// CHAR_FRANKLIN
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
						SET_PED_CREW(GET_SELECT_PED(SEL_MICHAEL))
						SET_PED_CREW(GET_SELECT_PED(SEL_FRANKLIN))
					ENDIF
				
				BSkippedBeforeCutStarted = TRUE
								
				iStageSection = 5
			ENDIF
			
		ENDIF
				
		SWITCH iStageSection
		
			CASE 0
				IF (iArrivalAngle <> 4 AND HAS_VEHICLE_RECORDING_BEEN_LOADED(iArrivalAngle, "JHArrive"))
				OR iArrivalAngle = 4 
					//Check the two locates...
					//Arrive from south
					
					KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
					PAUSE_FACE_TO_FACE_CONVERSATION(FALSE)
					IF iArrivalAngle = 1 //IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-665.706543,-246.371246,35.002647>>, <<-700.728882,-266.411346,46.027256>>, 38.750000)
						DO_START_CUTSCENE(FALSE, TRUE, FALSE)			
						
						SET_CAM_COORD(initialCam,<<-681.977173,-222.692368,36.835697>>)
						SET_CAM_ROT(initialCam,<<9.685310,0.613758,-104.186478>>)
						SET_CAM_FOV(initialCam,43.554966)

						SET_CAM_COORD(destinationCam,<<-682.055847,-223.223938,36.830551>>)
						SET_CAM_ROT(destinationCam,<<1.751721,0.613758,-135.188568>>)
						SET_CAM_FOV(destinationCam,43.554966)
		
						SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 4250, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL) 
						iArrivalAngle = 1   
						START_PLAYBACK_RECORDED_VEHICLE(vehVan, iArrivalAngle, "JHArrive")
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehVan, 6000)
						SET_PLAYBACK_SPEED(vehVan, 0.9)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehVan)
						CLEAR_AREA(<< -669.3546, -224.7060, 36.2190 >>, 20.00, TRUE)
						CLEAR_STORE_AREA_OF_PEDS()
						RENDER_SCRIPT_CAMS(TRUE, FALSE)		
						SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
						iStageSection++
						SETTIMERA(0)
					//Arrive from west
					ELIF iArrivalAngle = 2 // IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-648.916443,-208.859085,35.889164>>, <<-659.242859,-187.904800,44.673836>>, 42.250000)
						DO_START_CUTSCENE(FALSE, TRUE, FALSE)			
						
						SET_CAM_COORD(initialCam,<<-678.811401,-227.016754,36.637974>>)
						SET_CAM_ROT(initialCam,<<14.191272,0.284852,-77.502815>>)
						SET_CAM_FOV(initialCam,34.806614)
						
						SET_CAM_COORD(destinationCam,<<-678.488403,-227.714111,36.713589>>)
						SET_CAM_ROT(destinationCam,<<7.486020,0.284850,-107.432816>>)
						SET_CAM_FOV(destinationCam,34.806614)
						
						SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 3250, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL) 
						iArrivalAngle = 2
						START_PLAYBACK_RECORDED_VEHICLE(vehVan, iArrivalAngle, "JHArrive")
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehVan, 3700)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehVan)
						CLEAR_AREA(<< -669.3546, -224.7060, 36.2190 >>, 20.00, TRUE)
						CLEAR_STORE_AREA_OF_PEDS()
						RENDER_SCRIPT_CAMS(TRUE, FALSE)		
						SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
						iStageSection++
						SETTIMERA(0)

					//You arrive head on.
					ELIF  iArrivalAngle = 3 //IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-701.604309,-207.684723,35.562073>>, <<-683.287537,-243.607956,43.694168>>, 42.250000)
						DO_START_CUTSCENE(FALSE, TRUE, FALSE)			
						
						//arriving from angle
						SET_CAM_COORD(initialCam,<<-687.870117,-219.993591,36.743427>>)
						SET_CAM_ROT(initialCam,<<9.234026,-2.307101,-110.812981>>)
						SET_CAM_FOV(initialCam,37.249252)

						SET_CAM_COORD(destinationCam,<<-686.596130,-219.915756,36.886288>>)
						SET_CAM_ROT(destinationCam,<<3.781409,-2.307101,-99.386169>>)
						SET_CAM_FOV(destinationCam,37.249252)

						
						SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 3750, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL) 
						
						iArrivalAngle = 3
						START_PLAYBACK_RECORDED_VEHICLE(vehVan, iArrivalAngle, "JHArrive")
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehVan, 4500)
						SET_PLAYBACK_SPEED(vehVan, 1.15)
						FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehVan)
						CLEAR_AREA(<< -669.3546, -224.7060, 36.2190 >>, 20.00, TRUE)
						CLEAR_STORE_AREA_OF_PEDS()
						RENDER_SCRIPT_CAMS(TRUE, FALSE)		
						SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
						SETTIMERA(0)
						iStageSection++
					ELIF iArrivalAngle = 4
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						iStageSection++
					ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL// CHAR_FRANKLIN
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
						SET_PED_CREW(GET_SELECT_PED(SEL_MICHAEL))
						SET_PED_CREW(GET_SELECT_PED(SEL_FRANKLIN))
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(vehVan)
						SET_ENTITY_HEALTH(vehVan, 1000)
					ENDIF
					
					//SET_VEHICLE_DOOR_CONTROL(vehVan, SC_DOOR_FRONT_RIGHT, DT_DOOR_INTACT, 0.0)
					
					IF NOT IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehVan)						
						SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehVan, VS_FRONT_RIGHT)
					ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(4.0, 5.0, REPLAY_IMPORTANCE_HIGH)
					
					SAFE_FADE_IN()
					
					
				ELSE
					PRINTLN("waiting on arrival cutscene vehicle recording")
				ENDIF
			BREAK
						
			CASE 1
				
				IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_ACRLVE", CONV_PRIORITY_VERY_HIGH)									
				
					#IF IS_DEBUG_BUILD
						IF JSkip = TRUE
							RESET_NEED_LIST()
						ENDIF
					#ENDIF
					
					TRIGGER_MUSIC_EVENT("JH2B_MISSION_START_ST")
					REQUEST_ACTION_MODE_ASSET("MICHAEL_ACTION")
										
					vDriveToPoint = vFrontOfStore
				
					ADD_ALL_PEDS_TO_DIALOGUE()
					
					SAFE_REQUEST_INTERIOR_AT_COORDS_WITH_TYPE(interiorJewelStore, << -625.1706, -232.8408, 39.4937 >>, "V_JEWEL2", FALSE, FALSE)
					
					ENABLE_ALL_DISPATCH_SERVICES(FALSE)
					
					IF iArrivalAngle = 4
						iStageSection = 5
					ELSE
						iStageSection = 3
					ENDIF
				ENDIF
			BREAK
		
			CASE 3
							
				PRINTLN("Arrival Angle:", iArrivalAngle, " cut time:", TIMERA())
								
				SWITCH iArrivalAngle
					
					CASE 1
						IF (TIMERA() > 4300)
							//2nd shot
							IF NOT IS_ENTITY_DEAD(vehVan)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehVan, 2000.0)
								FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehVan, FALSE)
							ENDIF
							SET_CAM_COORD(initialCam,<<-640.450867,-233.443878,37.806362>>)
							SET_CAM_ROT(initialCam,<<4.411565,0.527128,81.385551>>)
							SET_CAM_FOV(initialCam,33.844154)

							SET_CAM_COORD(destinationCam,<<-642.658508,-233.653000,37.528545>>)
							SET_CAM_ROT(destinationCam,<<3.940768,0.527128,96.260635>>)
							SET_CAM_FOV(destinationCam,33.844154)
							SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 2000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL) 
							iStageSection = 4
						ENDIF
					BREAK

					CASE 2
						IF (TIMERA() > 3000)
							//	DECEL
							IF NOT IS_ENTITY_DEAD(vehVan)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehVan, 1700.0)
								FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehVan, FALSE)
							ENDIF
//							SET_CAM_COORD(initialCam,<<-649.392578,-226.618286,37.260235>>)
//							SET_CAM_ROT(initialCam,<<5.597940,-2.307086,90.664223>>)
//							SET_CAM_FOV(initialCam,34.806614)
//
//							SET_CAM_COORD(destinationCam,<<-650.427246,-227.162109,37.340744>>)
//							SET_CAM_ROT(destinationCam,<<6.788949,-2.307086,94.805183>>)
//							SET_CAM_FOV(destinationCam,34.806614)		
						
							SET_CAM_COORD(destinationCam,<<-639.424500,-231.281174,37.316582>>)
							SET_CAM_ROT(destinationCam,<<4.645180,-2.296260,91.566910>>)
							SET_CAM_FOV(destinationCam,34.806614)


							SET_CAM_COORD(initialCam,<<-642.166687,-230.967010,37.464840>>)
							SET_CAM_ROT(initialCam,<<6.755442,-2.296260,90.176819>>)
							SET_CAM_FOV(initialCam,34.806614)

							
							SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 3250, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL) 
							iStageSection = 4
						ENDIF
					BREAK
									
					CASE 3
						IF (TIMERA() > 3000)
							IF NOT IS_ENTITY_DEAD(vehVan)
								SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehVan, 2000.0)
								FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehVan, FALSE)
							ENDIF
							
//							SET_CAM_COORD(initialCam,<<-654.984314,-228.646362,37.422787>>)
//							SET_CAM_ROT(initialCam,<<4.641698,-2.307102,73.456886>>)
//							SET_CAM_FOV(initialCam,37.249252)
//
////							SET_CAM_COORD(destinationCam,<<-656.330322,-232.231842,37.289951>>)
////							SET_CAM_ROT(destinationCam,<<4.641698,-2.307102,74.261292>>)
////							SET_CAM_FOV(destinationCam,37.249252)
//							
//							
//							SET_CAM_COORD(destinationCam,<<-651.110474,-232.004639,36.936970>>)
//							SET_CAM_ROT(destinationCam,<<7.144072,-2.307102,96.140305>>)
//							SET_CAM_FOV(destinationCam,37.249252)
							
							
							SET_CAM_COORD(initialCam,<<-633.478394,-235.197922,37.634644>>)
							SET_CAM_ROT(initialCam,<<4.998171,-2.242930,83.917809>>)
							SET_CAM_FOV(initialCam,29.616934)
							
							SET_CAM_COORD(destinationCam,<<-636.296814,-235.583054,37.673473>>)
							SET_CAM_ROT(destinationCam,<<4.210197,-2.242930,93.950935>>)
							SET_CAM_FOV(destinationCam,29.616934)

							
							
							SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 3000, GRAPH_TYPE_DECEL, GRAPH_TYPE_DECEL) 
						
							iStageSection = 4
						ENDIF
					BREAK
				
				ENDSWITCH				
							
			BREAK
		
			CASE 4
				//start  scripted cut
				
				SET_USE_HI_DOF()
				
				IF HAS_ACTION_MODE_ASSET_LOADED("MICHAEL_ACTION")	
				AND HAS_CUTSCENE_LOADED()
				AND HAS_MODEL_LOADED(Asset.mnSecurity)
				AND HAS_MODEL_LOADED(Asset.mnShop[0])
				AND HAS_MODEL_LOADED(Asset.mnShop[1])
				AND HAS_MODEL_LOADED(Asset.mnShop[2])
				AND HAS_MODEL_LOADED(Asset.mnShop[3])
				AND HAS_MODEL_LOADED(Asset.mnShop[4])
				AND HAS_ANIM_DICT_LOADED(Asset.adJewelHeist)
				AND HAS_WEAPON_ASSET_LOADED(mnMainWeaponType)							
					IF (TIMERA() > 7000 AND iArrivalAngle = 1)
					OR (TIMERA() > 7000 AND iArrivalAngle = 2)
					OR (TIMERA() > 6300 AND iArrivalAngle = 3)					
						SAFE_SETUP_STORE_PEDS(FALSE, TRUE)
						iStageSection++		
					ENDIF
				ENDIF
			BREAK
					
			CASE 5
				
				IF HAS_MODEL_LOADED(GET_WEAPON_COMPONENT_TYPE_MODEL(WEAPONCOMPONENT_AT_AR_SUPP))
				AND HAS_WEAPON_ASSET_LOADED(mnMainWeaponType)
				
					IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
						SET_PED_CREW(GET_SELECT_PED(SEL_MICHAEL))
						SET_PED_CREW(GET_SELECT_PED(SEL_FRANKLIN))
					ENDIF
					
					SET_CURRENT_PED_WEAPON(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, TRUE)	
					GIVE_WEAPON_COMPONENT_TO_PED(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, WEAPONCOMPONENT_AT_AR_SUPP) GIVE_WEAPON_COMPONENT_TO_PED_IF_2A(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, WEAPONCOMPONENT_CARBINERIFLE_CLIP_01)					
					objWeapon = CREATE_WEAPON_OBJECT_FROM_CURRENT_PED_WEAPON_WITH_COMPONENTS(GET_SELECT_PED(SEL_MICHAEL))   
			
					objWeapon1 = CREATE_WEAPON_OBJECT(mnMainWeaponType, INFINITE_AMMO, <<0.1, 1.0, 1.0>>, TRUE)         
					GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(objWeapon1, WEAPONCOMPONENT_CARBINERIFLE_CLIP_01)
					GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(objWeapon1, WEAPONCOMPONENT_AT_AR_SUPP)
			
					objWeapon2 = CREATE_WEAPON_OBJECT(mnMainWeaponType, INFINITE_AMMO, <<0.1, 1.0, 1.0>>, TRUE)         
					GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(objWeapon2, WEAPONCOMPONENT_CARBINERIFLE_CLIP_01)
					GIVE_WEAPON_COMPONENT_TO_WEAPON_OBJECT(objWeapon2, WEAPONCOMPONENT_AT_AR_SUPP)
		
					
					IF NOT BSkippedBeforeCutStarted
						REGISTER_SELECT_PEDS_FOR_CUTSCENE(TRUE, TRUE, FALSE)			
						REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ONE)
						//REGISTER_ENTITY_FOR_CUTSCENE(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, PLAYER_ZERO)
						
						REGISTER_ENTITY_FOR_CUTSCENE(pedStore[0], "angry_customer", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(pedStore[1], "Jewellery_Assitance", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(pedStore[4], "shop_assistant", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
		//				REGISTER_ENTITY_FOR_CUTSCENE(pedStore[3], "boyfriend", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(pedStore[2], "customer", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(pedSecurity, "security_guard", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(pedCrew[CREW_GUNMAN_NEW], "gunman_selection_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(pedCrew[CREW_DRIVER_NEW], "driver_selection", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						REGISTER_ENTITY_FOR_CUTSCENE(vehVan, "BURRITO3", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, BURRITO3)				
						
						REGISTER_ENTITY_FOR_CUTSCENE(objWeapon, "Michaels_weapon", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
		//				
		//				SET_CURRENT_PED_WEAPON(pedCrew[CREW_DRIVER_NEW], mnMainWeaponType, TRUE)				
						
						REGISTER_ENTITY_FOR_CUTSCENE(objWeapon1, "HC_GUNMAN_2_HANDEDWEAPON", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
		//				
		//				SET_CURRENT_PED_WEAPON(pedCrew[CREW_GUNMAN_NEW], mnMainWeaponType, TRUE)				
						
						REGISTER_ENTITY_FOR_CUTSCENE(objWeapon2, "HC_GUNMAN1_2_HANDEDWEAPON", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			
						FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
							IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(Asset.mnShop[iCounter])
						ENDFOR
						IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(Asset.mnSecurity)
			
						SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
						START_CUTSCENE()
												
						REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					ENDIF	
					
					STOP_AUDIO_SCENE("JSH_2B_REVERSE_TRUCK_CUTSCENE")
					TRIGGER_MUSIC_EVENT("JH2B_MISSION_START_ST")

					iStageSection++
				ENDIF
			BREAK
			CASE 6
			CASE 7
				//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")
				IF NOT IS_CUTSCENE_ACTIVE()	
					SET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL), <<-629.2506, -234.1249, 37.0570>>)
					SET_ENTITY_HEADING(GET_SELECT_PED(SEL_MICHAEL), 291.8499)
					
					IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
						SET_PED_CREW(GET_SELECT_PED(SEL_MICHAEL))
						SET_PED_CREW(GET_SELECT_PED(SEL_FRANKLIN))
					ENDIF
									
					IF DOES_ENTITY_EXIST(objWeapon)
						GIVE_WEAPON_OBJECT_TO_PED(objWeapon, PLAYER_PED_ID())
					ENDIF
					IF DOES_ENTITY_EXIST(objWeapon1)
						GIVE_WEAPON_OBJECT_TO_PED(objWeapon1, pedCrew[CREW_DRIVER_NEW])
					ENDIF
					IF DOES_ENTITY_EXIST(objWeapon2)
						GIVE_WEAPON_OBJECT_TO_PED(objWeapon2, pedCrew[CREW_GUNMAN_NEW])
					ENDIF
					SET_CURRENT_PED_WEAPON(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, TRUE)
					SET_PED_USING_ACTION_MODE(GET_SELECT_PED(SEL_MICHAEL), TRUE)
					FORCE_PED_MOTION_STATE(GET_SELECT_PED(SEL_MICHAEL), MS_ON_FOOT_IDLE, TRUE, FAUS_CUTSCENE_EXIT)
					
					IF NOT BSkippedBeforeCutStarted
					//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("angry_customer")
					//IF NOT IS_CUTSCENE_ACTIVE()	
						TASK_PLAY_ANIM_ADVANCED(pedStore[0], Asset.adJewelHeist, "JH_2B_EndLoop_Male2", << -625.444, -234.010, 37.720 >>, << 0.000, 0.000, 36.000 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_LOOPING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedStore[0])
					//ENDIF
					//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Jewellery_Assitance")
					//IF NOT IS_CUTSCENE_ACTIVE()	
						TASK_PLAY_ANIM_ADVANCED(pedStore[1], Asset.adJewelHeist, "JH_2B_EndLoop_female1", << -625.444, -234.010, 37.720 >>, << 0.000, 0.000, 36.0 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION|AF_LOOPING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedStore[1])
					//ENDIF
					//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("shop_assistant")
					//IF NOT IS_CUTSCENE_ACTIVE()	
						TASK_PLAY_ANIM_ADVANCED(pedStore[2], Asset.adJewelHeist, "JH_2B_EndLoop_female2", << -625.444, -234.010, 37.720 >>, << 0.000, 0.000, 36.00 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_LOOPING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedStore[2])
					//ENDIF
	//				IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("boyfriend")
	//					TASK_PLAY_ANIM_ADVANCED(pedStore[3], Asset.adJewelHeist, "JH_2B_EndLoop_Male3", << -625.444, -234.010, 37.720 >>, << 0.000, 0.000, 36.000 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_LOOPING)
	//					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedStore[3])
	//				ENDIF
					//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("customer")
					//IF NOT IS_CUTSCENE_ACTIVE()	
						TASK_PLAY_ANIM_ADVANCED(pedStore[4], Asset.adJewelHeist, "JH_2B_EndLoop_Male1",<< -625.444, -234.010, 37.720 >>, << 0.000, 0.000, 36.00 >>,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_EXTRACT_INITIAL_OFFSET | AF_USE_MOVER_EXTRACTION|AF_LOOPING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedStore[4])
					//ENDIF
					
					//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("security_guard")
					//IF NOT IS_CUTSCENE_ACTIVE()	
						TASK_PLAY_ANIM_ADVANCED(pedSecurity, Asset.adJewelHeist, Asset.animCowerOnFloor, vJewelDoorCoords, vJewelDoorRot,INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_EXTRACT_INITIAL_OFFSET|AF_OVERRIDE_PHYSICS|AF_LOOPING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedSecurity)
					ENDIF
//					IF bIsStealthPath
//						SET_ENTITY_COORDS(pedCrew[CREW_], <<-628.6705, -232.0854, 37.0570>>)
//						SET_ENTITY_HEADING(pedCrew[cnSmashGunman], 305.2619)		
//					ENDIF
					
					IF IS_SCREEN_FADED_OUT()
						NEW_LOAD_SCENE_START_SPHERE(<< -625.1706, -232.8408, 39.4937 >>, 10.0)
						WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
							WAIT(0)
						ENDWHILE
						NEW_LOAD_SCENE_STOP()
					ENDIF
					
					RESET_GAME_CAMERA()
					
				ENDIF
				
				//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("gunman_selection_1")				
				IF NOT IS_CUTSCENE_ACTIVE()	
					UPDATE_BUDDY_LOOT_NEW(pedCrew[CREW_DRIVER_NEW], lcCrew) //, vehBike[0])
				ENDIF
				
//				IF CAN_SET_EXIT_STATE_FOR_CAMERA()
//					RESET_GAME_CAMERA()
//				ENDIF
										
				IF GET_CUTSCENE_TIME() > 8890.0
				AND bMusicEvent1 = FALSE
					TRIGGER_MUSIC_EVENT("JH2B_ENTER_SHOP_MA")
					bMusicEvent1 = TRUE
				ENDIF
			
				IF iStageSection=6
					IF GET_CUTSCENE_TIME() > 18000
						//ANIMPOSTFX_PLAY("SwitchSceneNeutral", 500, FALSE)												
						iStageSection=7
					ENDIF
				ENDIF	
				
				IF IS_CUTSCENE_PLAYING()
				
					SET_PED_COMPONENT_VARIATION(GET_SELECT_PED(SEL_MICHAEL), PED_COMP_FEET, 0, 0)
				
					IF NOT IS_ENTITY_DEAD(vehVan)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehVan)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehVan)
						ENDIF
					ENDIF
				ENDIF
				
				RESET_GAME_CAMERA()
				
				//Avoid the frame at the end where you can see the scripted cam
				IF IS_CUTSCENE_ACTIVE()
					//RESET_GAME_CAMERA()
					DO_END_CUTSCENE(FALSE)
					DISPLAY_HUD(FALSE)
					DISPLAY_RADAR(FALSE)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)	
					
										
				ENDIF
				
				IF NOT IS_CUTSCENE_ACTIVE()
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					ADD_ALL_PEDS_TO_DIALOGUE()
					DO_END_CUTSCENE(FALSE)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)	
					
					iStageSection=8
				ENDIF
			BREAK
		ENDSWITCH
				
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 8
			stopCamShake()
			
			DO_SWITCH_EFFECT(SEL_MICHAEL)
			
			IF bMusicEvent1 = FALSE
				TRIGGER_MUSIC_EVENT("JH2B_MISSION_RESTART")	
			ENDIF
			
			IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
				MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
				TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
				SET_PED_CREW(GET_SELECT_PED(SEL_MICHAEL))
				SET_PED_CREW(GET_SELECT_PED(SEL_FRANKLIN))
			ENDIF
				
			IMMEDIATELY_UNLOAD_NEED_LIST_VEHICLE_RECORDING("JHArrive", 1)
			IMMEDIATELY_UNLOAD_NEED_LIST_VEHICLE_RECORDING("JHArrive", 2)
			IMMEDIATELY_UNLOAD_NEED_LIST_VEHICLE_RECORDING("JHArrive", 3)
												
			SAFE_SETUP_STORE_PEDS(FALSE, TRUE, FALSE)
			
			SET_MODEL_AS_NO_LONGER_NEEDED(GET_WEAPON_COMPONENT_TYPE_MODEL(WEAPONCOMPONENT_AT_AR_SUPP))
			
			DISPLAY_HUD(TRUE)
			DISPLAY_RADAR(TRUE)
			
			REPLAY_STOP_EVENT()
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			
			SET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN), << -617.9512, -240.0968, 54.6514 >>)
			
			REMOVE_WEAPON_ASSET(mnMainWeaponType)
			
			IF DOES_ENTITY_EXIST(vehVan)
				DELETE_VEHICLE(vehVan)
			ENDIF
			
			////ANIMPOSTFX_STOP("SwitchSceneNeutral")
			//ANIMPOSTFX_PLAY("SwitchSceneMichael", 1000, FALSE)
			
			bStageSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


//PURPOSE:		Checks if a peds being aimed at
FUNC  BOOL IS_PED_BEING_AIMED_AT(PED_INDEX thisPed)

	IF DOES_ENTITY_EXIST(thisPed)
		IF NOT IS_PED_INJURED(thisPed)
			IF IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), thisPed)
			OR IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), thisPed)
				//SCRIPT_ASSERT("aimed at!")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
		
	RETURN FALSE

ENDFUNC

INT iTimeOflAstShooting
INT iPedIterator
BOOL bHasPedbeenMurdered[NUMBER_OF_STORE_PEDS]
BOOL bHasCommentedOnMurder[NUMBER_OF_STORE_PEDS]
INT iTimeOflastScreaming

PROC UPDATE_WHIMPERS()

	IF NOT bIsStealthPath

		SWITCH iUpdateWhimpers
		
			CASE 0
				FOR iPedIterator = 0 To NUMBER_OF_STORE_PEDS - 1
					bHasPedbeenMurdered[iPedIterator] = FALSE
					bLineAboutKillingThisGuyPlayed[iPedIterator] = FALSE
				ENDFOR
				iUpdateWhimpers++
			BREAK
			
			CASE 1
			
				iRandomPedToWhimper = GET_RANDOM_INT_IN_RANGE(0, NUMBER_OF_STORE_PEDS)
			
				IF NOT IS_PED_INJURED(pedStore[iRandomPedToWhimper])
					//IF IS_PED_MALE(pedStore[iRandomPedToWhimper])
					
					IF (GET_GAME_TIMER() - iTimeOflAstShooting) > 4000
						IF (GET_GAME_TIMER() - iTimeOfLastWhimper) > 750
							IF IS_PED_MALE(pedStore[iRandomPedToWhimper])
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedStore[iRandomPedToWhimper], "WHIMPER", "WAVELOAD_PAIN_MALE") //, SPEECH_PARAMS_FORCE)	
							ELSE
								PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedStore[iRandomPedToWhimper], "WHIMPER", "WAVELOAD_PAIN_FEMALE") //, SPEECH_PARAMS_FORCE)	
							ENDIF
							iTimeOfLastWhimper = GET_GAME_TIMER()
						ENDIF
					ELSE
						IF (GET_GAME_TIMER() - iTimeOflastScreaming) > 50
							IF GET_RANDOM_BOOL()
								//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedStore[iRandomPedToWhimper], "SCREAM_PANIC", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE_FRONTEND)	
								 PLAY_PAIN(pedStore[iRandomPedToWhimper], AUD_DAMAGE_REASON_SCREAM_SHOCKED)
							ELSE
								//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedStore[iRandomPedToWhimper], "SCREAM_SHOCKED", "WAVELOAD_PAIN_FEMALE", SPEECH_PARAMS_FORCE_FRONTEND)	
								//PLAY_PAIN(pedStore[iRandomPedToWhimper], AUD_DAMAGE_REASON_SCREAM_SHOCKED)
								IF GET_RANDOM_BOOL()
									PLAY_PED_AMBIENT_SPEECH(pedStore[iRandomPedToWhimper], "GENERIC_FRIGHTENED_HIGH", SPEECH_PARAMS_FORCE_FRONTEND)
								ELSE
									PLAY_PAIN(pedStore[iRandomPedToWhimper], AUD_DAMAGE_REASON_SCREAM_PANIC)
								ENDIF
							ENDIF
							iTimeOflastScreaming = GET_GAME_TIMER()
						ENDIF
					ENDIF
					
					
					iUpdateWhimpers++
				ENDIF
				
			BREAK
			
			CASE 2
				IF (GET_GAME_TIMER() - iTimeOfLastWhimper) > 500
					iUpdateWhimpers = 1
				ENDIF
			BREAK
		
		ENDSWITCH
		
	ENDIF
	
	IF IS_PED_SHOOTING(PLAYER_PED_ID())
		iTimeOflAstShooting = GET_GAME_TIMER()	
	ENDIF

	//IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
		FOR iPedIterator = 0 TO NUMBER_OF_STORE_PEDS -1
			//IF iPedIterator <> 4	//Dont include the sec guard
			
			//IF IS_PED_SHOOTING(PLAYER_PED_ID())
				//IF (GET_GAME_TIMER() - iTimeOflAstShooting) > 2000
				IF DOES_ENTITY_EXIST(pedStore[iPedIterator])
				AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedStore[iPedIterator], PLAYER_PED_ID())
				AND bLineAboutKillingThisGuyPlayed[iPedIterator] = FALSE
					RunConversation("JH_SHOOTING", TRUE, TRUE, TRUE, CONV_PRIORITY_VERY_HIGH)
					bLineAboutKillingThisGuyPlayed[iPedIterator] = TRUE	
				ENDIF				
			//ENDIF
				
			IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)	
				IF NOT bHasPedbeenMurdered[iPedIterator]
					IF DOES_ENTITY_EXIST(pedStore[iPedIterator])
						IF IS_ENTITY_DEAD(pedStore[iPedIterator])
						AND HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedStore[iPedIterator], PLAYER_PED_ID())
							PRINTLN("pedStore: ", iPedIterator)
							
							IF IS_GUNMAN_PACKIE()
								RunConversation("JH_KOP", TRUE, TRUE, TRUE, CONV_PRIORITY_VERY_HIGH)
							ELIF IS_GUNMAN_GUSTAVO()
								RunConversation("JH_GUSSTOP", TRUE, TRUE, TRUE, CONV_PRIORITY_VERY_HIGH)
							ELIF IS_GUNMAN_NORM()
								RunConversation("JH_NORMSTOP", TRUE, TRUE, TRUE, CONV_PRIORITY_VERY_HIGH)
							ENDIF
							bHasPedbeenMurdered[iPedIterator] = TRUE
						ENDIF
					ENDIF
				ELSE
					IF NOT bHasCommentedOnMurder[iPedIterator]
					AND NOT IS_PED_INJURED(pedStore[iPedIterator])
						//PLAY_PAIN(pedStore[iPedIterator], AUD_DAMAGE_REASON_SCREAM_TERROR)
//						PLAY_PED_AMBIENT_SPEECH(pedStore[iPedIterator], "GENERIC_FRIGHTENED_HIGH", SPEECH_PARAMS_FORCE_FRONTEND)	
						bHasCommentedOnMurder[iPedIterator] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDFOR
	//ENDIF
	
ENDPROC

INT iRampTimer

PROC UNLOAD_BIKES_IN_BACKGROUND()

//	IF IS_ENTITY_DEAD(vehTruck)	
//		SCRIPT_ASSERT("boom")
//	ENDIF
	
	SWITCH iUnloadBikes
	
		CASE 0
			REQUEST_VEHICLE_RECORDING(1, "JHGetBike")
			REQUEST_VEHICLE_RECORDING(2, "JHGetBike")
			REQUEST_VEHICLE_RECORDING(3, "JHGetBike")
			REQUEST_VEHICLE_RECORDING(4, "JHGetBike")
			REQUEST_MODEL(Asset.mnTruck)
			REQUEST_MODEL(Asset.mnBike)
			
			REQUEST_MODEL_FOR_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)))
	
			IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_FRANKLIN))
				SETUP_FRANKLINS_SUIT()
				SET_PED_HELMET_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), 6)
				SET_PED_HELMET_TEXTURE_INDEX(GET_SELECT_PED(SEL_FRANKLIN), 8)
				SET_PED_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_PROP_POSITION,0), 6, 8)
				GIVE_PED_HELMET(GET_SELECT_PED(SEL_FRANKLIN), TRUE, PV_FLAG_DEFAULT_HELMET, -1)
			ENDIF
			
			iUnloadBikes++
		BREAK
		
		CASE 1
			IF HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "JHGetBike")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "JHGetBike")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(3, "JHGetBike")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(4, "JHGetBike")
			AND HAS_MODEL_LOADED(Asset.mnTruck)
			AND HAS_MODEL_LOADED(Asset.mnBike)
			AND HAS_MODEL_LOADED_FOR_CREW_MEMBER(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)))
				iUnloadBikes++
			ENDIF
		BREAK
		
		CASE 2
			IF NOT DOES_ENTITY_EXIST(vehTruck)
				vehTruck = CREATE_VEHICLE(Asset.mnTruck,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(1, 3000.0, "JHGetBike"), 135)
				//[MF]Remove all randomly assigned extras from the vehicle (B* 1689820)
				REMOVE_ALL_VEHICLE_EXTRAS(vehTruck)	

				SET_VEHICLE_COLOURS(vehTruck, 128, 3) SET_VEHICLE_EXTRA(vehTruck, 6, FALSE) //SET_VEHICLE_LIVERY(vehTruck, 1)				
			ENDIF
			
			pedCrew[CREW_HACKER] = CREATE_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,  ENUM_TO_INT(CREW_HACKER), vCrewTruckCoords, fCrewTruckHeading)
			SET_PED_INTO_VEHICLE(pedCrew[CREW_HACKER], vehTruck)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCrew[CREW_HACKER], TRUE)
			
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehTruck, FALSE)
			
			START_PLAYBACK_RECORDED_VEHICLE(vehTruck, 1, "JHGetBike")
			SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTruck, 3000.0)
			SET_ENTITY_PROOFS(vehTruck, FALSE, FALSE, FALSE, TRUE, FALSE)
			SET_PLAYBACK_SPEED(vehTruck, -1.1)
		//	SET_VEHICLE_DOORS_LOCKED(vehTruck, VEHICLELOCK_LOCKOUT_PLAYER_ONLY)
			iUnloadBikes++
		BREAK
		
		CASE 3
			IF NOT IS_ENTITY_DEAD(vehTruck)
				IF GET_TIME_POSITION_IN_RECORDING(vehTruck) < 100.00
					SET_RAMP_OPEN(vehTruck, TRUE, 0.92)
					iRampTimer = GET_GAME_TIMER()
					iUnloadBikes++
				ENDIF
			ENDIF
		BREAK
		
		CASE 4
		
			IF GET_GAME_TIMER() - iRampTimer > 500
		
				IF NOT DOES_ENTITY_EXIST(vehBike[2])
					vehBike[2] = CREATE_VEHICLE(Asset.mnBike,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(2, 0.0, "JHGetBike"), GET_ENTITY_HEADING(vehTruck))	
				ENDIF
				SET_VEHICLE_COLOUR_COMBINATION(vehBike[2], 0)
				SET_ENTITY_PROOFS(vehBike[2], FALSE, FALSE, FALSE, TRUE, FALSE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBike[2], FALSE)
				START_PLAYBACK_RECORDED_VEHICLE(vehBike[2], 2, "JHGetBike")
				SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehBike[2], vBatiRecOffset)
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike[2], FALSE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBike[2], FALSE)
				SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
				REMOVE_PED_HELMET(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
				SET_PED_HELMET_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), 6)
				SET_PED_HELMET_TEXTURE_INDEX(GET_SELECT_PED(SEL_FRANKLIN), 8)
				SET_PED_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_PROP_POSITION,0), 6, 8)
				SET_PED_HELMET(GET_SELECT_PED(SEL_FRANKLIN), TRUE)	
				
				SET_PED_STEERS_AROUND_OBJECTS(GET_SELECT_PED(SEL_FRANKLIN), FALSE)	
				//SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[0], 1000.0)
				SET_PLAYBACK_SPEED(vehBike[2], 1.15)
				
				
				SET_PED_CAN_RAGDOLL(GET_SELECT_PED(SEL_FRANKLIN), FALSE)
				
				IF NOT DOES_ENTITY_EXIST(vehBike[1])
					vehBike[1] = CREATE_VEHICLE(Asset.mnBike,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(3, 50.0, "JHGetBike"), GET_ENTITY_HEADING(vehTruck))	
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBike[1], FALSE)
					START_PLAYBACK_RECORDED_VEHICLE(vehBike[1], 3, "JHGetBike")
					SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehBike[1], vBatiRecOffset + <<0.3, -0.4, 0.0>>)
					PAUSE_PLAYBACK_RECORDED_VEHICLE(vehBike[1])
					SET_VEHICLE_COLOUR_COMBINATION(vehBike[1], 1)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike[1], FALSE)
					//SET_ENTITY_COLLISION(vehBike[1], FALSE)
				ENDIF
				
				IF NOT DOES_ENTITY_EXIST(vehBike[0])
					vehBike[0] = CREATE_VEHICLE(Asset.mnBike,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(4, 50.0, "JHGetBike"), GET_ENTITY_HEADING(vehTruck))	
					SET_ENTITY_PROOFS(vehBike[0], FALSE, FALSE, FALSE, TRUE, FALSE)
					SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBike[0], FALSE)
					START_PLAYBACK_RECORDED_VEHICLE(vehBike[0], 4, "JHGetBike")
					SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehBike[0], vBatiRecOffset)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike[0], FALSE)
					PAUSE_PLAYBACK_RECORDED_VEHICLE(vehBike[0])
					SET_VEHICLE_COLOUR_COMBINATION(vehBike[0], 2)
					SET_ENTITY_COLLISION(vehBike[0], FALSE)
				ENDIF			
				iUnloadBikes++
			ENDIF
		BREAK
		
		CASE 5
			IF NOT IS_ENTITY_DEAD(vehTruck)
				//SET_VEHICLE_DOOR_OPEN(vehTruck, SC_DOOR_BOOT)
				//SET_VEHICLE_DOOR_CONTROL(vehTruck, SC_DOOR_BOOT, DT_DOOR_INTACT, 0.92)
				SET_RAMP_OPEN(vehTruck, TRUE, 0.92)
			ENDIF
			IF NOT IS_ENTITY_DEAD(vehBike[2])
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[2])
					SET_VEHICLE_ON_GROUND_PROPERLY(vehBike[2])
					iUnloadBikes++
				ENDIF
			ENDIF
			
		BREAK
		
		CASE 6
			IF IS_VEHICLE_SEAT_FREE(vehBike[1])			
				IF DOES_ENTITY_EXIST(vehBike[1])
					SET_ENTITY_COLLISION(vehBike[1], TRUE)
				ENDIF
				SET_VEHICLE_INACTIVE_DURING_PLAYBACK(vehTruck, TRUE)
				SET_ENTITY_PROOFS(vehBike[1], FALSE, FALSE, FALSE, TRUE, FALSE)
				SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBike[1], FALSE)
				
				
				OPEN_SEQUENCE_TASK(seqGetOnBike)
					TASK_GO_STRAIGHT_TO_COORD(NULL, << -639.9664, -239.2199, 36.9977 >>, PEDMOVE_RUN)
					TASK_ENTER_VEHICLE(NULL, vehBike[1], 5000)
				CLOSE_SEQUENCE_TASK(seqGetOnBike)
				TASK_PERFORM_SEQUENCE(GET_SELECT_PED(SEL_FRANKLIN), seqGetOnBike)
				CLEAR_SEQUENCE_TASK(seqGetOnBike)
				iUnloadBikes++
			ENDIF
		BREAK
			
		CASE 7
					
			IF NOT IS_ENTITY_DEAD(vehTruck)
				//SET_VEHICLE_DOOR_OPEN(vehTruck, SC_DOOR_BOOT)
				SET_VEHICLE_DOOR_CONTROL(vehTruck, SC_DOOR_BOOT, DT_DOOR_INTACT, 1.0)
			ENDIF
		
			IF IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[1])
				UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehBike[1])
				SET_PLAYBACK_SPEED(vehBike[1], 1.15)
				iUnloadBikes++
			ENDIF
		BREAK
		
		CASE 8
			
			IF NOT IS_ENTITY_DEAD(vehTruck)
				//SET_VEHICLE_DOOR_OPEN(vehTruck, SC_DOOR_BOOT)
				SET_VEHICLE_DOOR_CONTROL(vehTruck, SC_DOOR_BOOT, DT_DOOR_INTACT, 1.0)
			ENDIF
		
			IF NOT IS_ENTITY_DEAD(vehBike[1])
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1])
					SET_VEHICLE_ON_GROUND_PROPERLY(vehBike[1])
					iUnloadBikes++
				ENDIF
			ENDIF
			
		BREAK
		
		CASE  9
			IF IS_VEHICLE_SEAT_FREE(vehBike[0])			
				IF DOES_ENTITY_EXIST(vehBike[0])
					SET_ENTITY_COLLISION(vehBike[0], TRUE)
				ENDIF
				
				OPEN_SEQUENCE_TASK(seqGetOnBike)
					TASK_GO_STRAIGHT_TO_COORD(NULL, << -638.5618, -240.0035, 37.0538 >>, PEDMOVE_RUN, 6000)
					TASK_ENTER_VEHICLE(NULL, vehBike[0], 5000)
				CLOSE_SEQUENCE_TASK(seqGetOnBike)
				TASK_PERFORM_SEQUENCE(GET_SELECT_PED(SEL_FRANKLIN), seqGetOnBike)
				CLEAR_SEQUENCE_TASK(seqGetOnBike)
				iUnloadBikes++
			ENDIF
		BREAK
		
		CASE 10
		
			IF NOT IS_ENTITY_DEAD(vehTruck)
				//SET_VEHICLE_DOOR_OPEN(vehTruck, SC_DOOR_BOOT)
				SET_VEHICLE_DOOR_CONTROL(vehTruck, SC_DOOR_BOOT, DT_DOOR_INTACT, 1.0)
			ENDIF
		
			IF IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[0])
				UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehBike[0])
				SET_PLAYBACK_SPEED(vehBike[0], 1.15)
				iUnloadBikes++
			ENDIF
		BREAK
		
		CASE 11
			
			IF NOT IS_ENTITY_DEAD(vehTruck)
				SET_VEHICLE_DOOR_CONTROL(vehTruck, SC_DOOR_BOOT, DT_DOOR_INTACT, 1.0)
			ENDIF
		
			IF NOT IS_ENTITY_DEAD(vehBike[0])
				IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0])
					SET_VEHICLE_ON_GROUND_PROPERLY(vehBike[0])
					SET_VEHICLE_INACTIVE_DURING_PLAYBACK(vehTruck, FALSE)
					TASK_LOOK_AT_ENTITY(GET_SELECT_PED(SEL_FRANKLIN), GET_SELECT_PED(SEL_MICHAEL), 60000, SLF_WHILE_NOT_IN_FOV)
					iUnloadBikes++
				ENDIF
			ENDIF
			
		BREAK
	
		CASE 12
			IF NOT HAS_PED_MOUNTED_TRUCK(GET_SELECT_PED(SEL_MICHAEL))
				SET_RAMP_OPEN(vehTruck, FALSE)
				UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTruck)
				
				
				SET_PLAYBACK_SPEED(vehTruck, 1.15)
				iUnloadBikes++
			ELSE
				SET_RAMP_OPEN(vehTruck, TRUE, 0.92)
			ENDIF
		BREAK
		
		CASE 13
			IF IS_VEHICLE_SEAT_FREE(vehBike[2])			
				OPEN_SEQUENCE_TASK(seqGetOnBike)
					TASK_LEAVE_ANY_VEHICLE(NULL)
					TASK_ENTER_VEHICLE(NULL, vehBike[2], 5000)
				CLOSE_SEQUENCE_TASK(seqGetOnBike)
				TASK_PERFORM_SEQUENCE(GET_SELECT_PED(SEL_FRANKLIN), seqGetOnBike)
				CLEAR_SEQUENCE_TASK(seqGetOnBike)
				iUnloadBikes++
			ENDIF
		BREAK
		
		CASE 14
		
			SET_RAMP_OPEN(vehTruck, FALSE)
		
			IF NOT IS_ENTITY_ON_SCREEN(vehTruck)	
				
				SET_MODEL_FOR_CREW_MEMBER_AS_NO_LONGER_NEEDED(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,  ENUM_TO_INT(CREW_HACKER)))
				DELETE_VEHICLE(vehTruck)
				DELETE_PED(pedCrew[CREW_HACKER])
				SET_MODEL_AS_NO_LONGER_NEEDED(Asset.mnTruck)
				
				REMOVE_VEHICLE_RECORDING(1, "JHGetBike")
				REMOVE_VEHICLE_RECORDING(2, "JHGetBike")
				REMOVE_VEHICLE_RECORDING(3, "JHGetBike")
				REMOVE_VEHICLE_RECORDING(4, "JHGetBike")
				
				SET_PED_CAN_RAGDOLL(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
				iUnloadBikes++
			ENDIF
		BREAK
		
	ENDSWITCH
	

ENDPROC

INT iDealWithMichaelPossiblyBeingOutside
BLIP_INDEX blipGetBuddy
//BOOL bPlayerIsOkToTriggerCut

FUNC BOOL DEAL_WITH_MICHAEL_POSSIBLY_BEING_OUTSIDE()
	
	SWITCH iDealWithMichaelPossiblyBeingOutside
	
		//Tell player to go back inside and fetch the crew.
		CASE 0
			PRINT_NOW("G_FETCHCREW", DEFAULT_GOD_TEXT_TIME, 1)
			blipGetBuddy = CREATE_BLIP_FOR_ENTITY(pedCrew[CREW_DRIVER_NEW], FALSE)
			iDealWithMichaelPossiblyBeingOutside++
		BREAK		
							
		//wait until he's at the crew
		CASE 1
			IF NOT IS_ENTITY_DEAD(pedCrew[CREW_DRIVER_NEW])
			AND NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_MICHAEL))
				IF IS_ENTITY_AT_ENTITY(GET_SELECT_PED(SEL_MICHAEL), pedCrew[CREW_DRIVER_NEW], <<6.0, 6.0, 2.0>>)
					REMOVE_BLIP(blipGetBuddy)
					iDealWithMichaelPossiblyBeingOutside++
				ENDIF
			ENDIF
		BREAK
				
		CASE 2
			IF (iCurrentTake > TARGET_TAKE_VALUE AND iCurrentTake < 3000000)	
					IF CREATE_CONVERSATION(myScriptedSpeech,  "JHAUD", "JH_SCORE_BAD",CONV_PRIORITY_VERY_HIGH)
						iDealWithMichaelPossiblyBeingOutside++
					ENDIF
				ELIF (iCurrentTake > TARGET_TAKE_VALUE AND iCurrentTake < 3000000)
					IF CREATE_CONVERSATION(myScriptedSpeech,  "JHAUD", "JH_SCORE_MED", CONV_PRIORITY_VERY_HIGH)
						iDealWithMichaelPossiblyBeingOutside++
					ENDIF
				ELIF (iCurrentTake > 3500000 AND iCurrentTake < 4000000)
					
				IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_SCORE_GID", CONV_PRIORITY_VERY_HIGH)
					iDealWithMichaelPossiblyBeingOutside++
				ENDIF
			ENDIF
		BREAK
								
		//Tell player to rendez vous with franklin.
		CASE 3
			iDealWithMichaelPossiblyBeingOutside = 0
			RETURN TRUE
		BREAK
		
	ENDSWITCH
	
	RETURN FALSE
	
ENDFUNC

INT iEncDialogue
VECTOR vTrafficWardenCoords
BOOL bWeGotItAllPrinted = FALSE
BOOL bPoliceScanner1 = FALSE
FLOAT fGasStrength = 0.0
BOOL bFlagEnterVehicleDriver = FALSE

//PURPOSE:		Performs the section where Michael smash and grabs the loot
FUNC BOOL stageGrabLoot()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
				
		g_replay.iReplayInt[CURRENT_TAKE_REPLAY_VALUE] = 0
				
		SET_PED_USING_ACTION_MODE(PLAYER_PED_ID(), TRUE)		
		
		SETUP_CREW_CHOICE_CONSEQUENCES()	//Resets time limits extra if you've skipped
		
		SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", TRUE)
		
		//Swap the gunman and drivers roles
		IF NOT bIsStealthPath
			cnSmashGunman = CREW_DRIVER_NEW
			cnAltGunman = CREW_GUNMAN_NEW
		ENDIF
		
		GIVE_WEAPON_TO_PED(pedCrew[CREW_DRIVER_NEW], mnMainWeaponType, 1000)
		GIVE_WEAPON_TO_PED(pedCrew[CREW_GUNMAN_NEW], mnMainWeaponType, 1000)
		//GIVE_WEAPON_TO_PED(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, 1000)
		SET_CURRENT_PED_WEAPON(pedCrew[CREW_DRIVER_NEW], mnMainWeaponType, TRUE)
		SET_CURRENT_PED_WEAPON(pedCrew[CREW_GUNMAN_NEW], mnMainWeaponType, TRUE)
		SET_CURRENT_PED_WEAPON(GET_SELECT_PED(SEL_MICHAEL), mnMainWeaponType, TRUE)
				
		PRINTLN("StartLoadTime:", GET_GAME_TIMER())
				
		DELETE_VEHICLE(vehVan)
		IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(Asset.mnVan)
				
		RESET_NEED_LIST()
		
		iCounter = 0
		
		FOR iCounter = 0 TO iNoOfCases-1
			rfReset = GET_RAYFIRE_MAP_OBJECT(	jcStore[iCounter].vCasePos, fRFCheckRadius,
												jcStore[iCounter].sSmashAnim)
			IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfReset)
				IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfReset) <> RFMO_STATE_START
					SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfReset, RFMO_STATE_RESET)//RFMO_STATE_RESET)
				ENDIF
			ENDIF
			jcStore[iCounter].iSmashedByBulletStage = 0
		ENDFOR

		g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE] = 0
				
		REQUEST_MODEL(Asset.mnJewels1)
		REQUEST_MODEL(Asset.mnJewels2)
		REQUEST_MODEL(Asset.mnJewels4a)
		REQUEST_MODEL(Asset.mnJewels4b)
				
		REQUEST_ANIM_DICT(Asset.adJewelHeist)
		REQUEST_ANIM_DICT(Asset.adWatch)
		REQUEST_ANIM_DICT(Asset.adJewelHeist)
				
		WHILE NOT HAS_MODEL_LOADED(Asset.mnJewels1)
		OR NOT HAS_MODEL_LOADED(Asset.mnJewels2)
		OR NOT HAS_MODEL_LOADED(Asset.mnJewels4a)
		OR NOT HAS_MODEL_LOADED(Asset.mnJewels4b)
				
		OR NOT HAS_ANIM_DICT_LOADED(Asset.adJewelHeist)
		OR NOT HAS_ANIM_DICT_LOADED(Asset.adWatch)
				
			WAIT(0)
		ENDWHILE
				
		PRINTLN("EndLoadTime:", GET_GAME_TIMER())
		
		REQUEST_AMBIENT_AUDIO_BANK(Asset.sbRaidGlassSmash)
		REQUEST_AMBIENT_AUDIO_BANK(Asset.sbRaidStore)
		
		REQUEST_PTFX_ASSET()
		
		IF IS_SCREEN_FADED_OUT()
			IF NOT IS_PED_INJURED(pedCrew[cnSmashGunman])
				IF bIsStealthPath
					SET_ENTITY_COORDS(pedCrew[cnSmashGunman], <<-629.47, -232.86, 37.0570>>)
					SET_ENTITY_HEADING(pedCrew[cnSmashGunman], -118.52)
				ELSE
					SET_ENTITY_COORDS(pedCrew[cnSmashGunman], <<-629.42, -236.67, 37.0570>>)
					SET_ENTITY_HEADING(pedCrew[cnSmashGunman], -121.52)
				ENDIF
				SET_PED_USING_ACTION_MODE(pedCrew[cnSmashGunman], TRUE)
				//TASK_FOLLOW_NAV_MESH_TO_COORD(pedCrew[cnSmashGunman], << -625.2348, -235.1159, 37.0570 >>, PEDMOVE_RUN)
			ENDIF
			IF NOT IS_PED_INJURED(pedCrew[cnAltGunman])
				
				IF bIsStealthPath
					SET_ENTITY_COORDS(pedCrew[cnAltGunman], <<-629.56, -236.72, 37.0570>>)
					SET_ENTITY_HEADING(pedCrew[cnAltGunman], -114.55)
				ELSE
					SET_ENTITY_COORDS(pedCrew[cnAltGunman], <<-628.6705, -232.0854, 37.0570>>)
					SET_ENTITY_HEADING(pedCrew[cnAltGunman], 305.2619)					
				ENDIF
				//TASK_FOLLOW_NAV_MESH_TO_COORD(pedCrew[cnAltGunman], << -623.5548, -236.8584, 37.0570 >>, PEDMOVE_RUN)
		
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(pedCrew[cnAltGunman])
			TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD(pedCrew[cnAltGunman], <<-623.2735, -236.2186, 37.0570>>, << -622.8721, -230.6936, 37.8568 >>, PEDMOVE_RUN, FALSE)
		ENDIF
		
		IF bIsStealthPath
			OPEN_SEQUENCE_TASK(seqSequence)
				TASK_GO_TO_COORD_ANY_MEANS(NULL, vJewelryWatchCoords, PEDMOVE_WALK, NULL)
				TASK_ACHIEVE_HEADING(NULL, fJewelryWatchHeading)
				TASK_PLAY_ANIM(NULL, Asset.adWatch, "idle_a", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				TASK_PLAY_ANIM(NULL, Asset.adWatch, "idle_b", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				TASK_PLAY_ANIM(NULL, Asset.adWatch, "idle_d", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
				TASK_PLAY_ANIM(NULL, Asset.adWatch, Asset.animWatch, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING)
			CLOSE_SEQUENCE_TASK(seqSequence)
			TASK_PERFORM_SEQUENCE(pedCrew[cnAltGunman], seqSequence)
			CLEAR_SEQUENCE_TASK(seqSequence)
			
//			IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxKOGasHaze)
//				ptfxKOGasHaze = START_PARTICLE_FX_LOOPED_AT_COORD("scr_jewel_haze", <<-624.29,-232.23,37.31>>, VECTOR_ZERO, 1)
//				
//				
//			ENDIF
		ENDIF
		
		CLEAR_STORE_AREA_OF_PEDS()
		
		IF bIsStealthPath
			SAFE_SETUP_STORE_PEDS(TRUE, FALSE, FALSE)
		ELSE
			SAFE_SETUP_STORE_PEDS(FALSE, FALSE, FALSE)
		ENDIF
		
		ADD_ALL_PEDS_TO_DIALOGUE()

		clearText()
		emptyTextQueue()	

		addTextToQueue("JH_GOGO", TEXT_TYPE_CONVO)
		
		IF bIsStealthPath
		
			IF IS_GUNMAN_GUSTAVO()
				addTextToQueue("JH_CLEARG", TEXT_TYPE_CONVO)
			ELIF IS_GUNMAN_PACKIE()
				addTextToQueue("JH_CLEARP", TEXT_TYPE_CONVO)
			ELIF IS_GUNMAN_NORM()
				addTextToQueue("JH_CLEARN", TEXT_TYPE_CONVO)
			ENDIF
		ELSE
			IF IS_DRIVER_EDDIE()
				addTextToQueue("JH_CLEAN_ET", TEXT_TYPE_CONVO)
			ELIF IS_DRIVER_KARIM()
				addTextToQueue("JH_CLEAN_KD", TEXT_TYPE_CONVO)
			ENDIF
		ENDIF

		addTextToQueue(Asset.godStealJewels, TEXT_TYPE_GOD)

		SWITCH GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)))
			CASE CMSK_GOOD
				addTextToQueue(Asset.convHackerAStart, TEXT_TYPE_CONVO)
				addTextToQueue("JH_HCKARESP", TEXT_TYPE_CONVO)
				//Hacker:	I've been able to get you around a minute and a half before the alarm resets itself.
				//Ninety seconds? Alright, that's why we pay you what we do.
			BREAK
			CASE CMSK_MEDIUM
				addTextToQueue(Asset.convHackerBStart, TEXT_TYPE_CONVO)
				addTextToQueue("JH_HCKBRESP", TEXT_TYPE_CONVO)
				//Hacker:	Shit, I dunno, you got maybe forty seconds before the alarm's gonna go off.
			BREAK
			CASE CMSK_BAD
				addTextToQueue("JH_HCKCGO", TEXT_TYPE_CONVO)
				addTextToQueue("JH_HCKCRESP", TEXT_TYPE_CONVO)				
				//Hacker:	Bros, I wouldn't stay in there too long, okay? This alarm is gonna go... at some point.  
			BREAK
		ENDSWITCH
		
		IF NOT bIsStealthPath
			eRandomManagerStage = MANAGER_REQUEST_ASSETS
			IF bGuardRuns
				iRandomGuardStage = 0
			ENDIF
		ENDIF
	
		//RESET_HEIST_END_SCREEN_STOLEN_ITEMS(HEIST_JEWEL)
		g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE] = 0
		//bStolenSomething = FALSE
				
		iGuardRunTimer = GET_GAME_TIMER()
				
		bIsHackTimeUp = FALSE
		
		bHasPrintedCaseHelp = FALSE
		
		bIsRandomGuardEventNow = FALSE
		bIsRandomManagerEventNow = FALSE
		bIsRandomGuardEventDone = FALSE
		bIsRandomManagerEventDone = FALSE
		
		bIsAlarmRinging = FALSE
		bInSufficientTake = FALSE
		bWeGotItAllPrinted = FALSE
		bNotFirstCase = FALSE
		
		resetStoreBlips()
		
		SET_CREATE_RANDOM_COPS(FALSE)
		
		PREPARE_ALARM("JEWEL_STORE_HEIST_ALARMS")
		SETTIMERA(0)
		SETTIMERB(0)
		
		FOR iCounter = 0 TO NUMBER_OF_EVENT_FLAGS-1
			bEventFlag[iCounter] = FALSE
		ENDFOR
		
		SET_PED_BAG(pedCrew[CREW_DRIVER_NEW], TRUE, TRUE)
		SET_PED_BAG(pedCrew[CREW_GUNMAN_NEW], TRUE, TRUE)
		
		//Put any SKIP related stuff in here.
		IF IS_SCREEN_FADED_OUT()
			SET_PED_BAG(GET_SELECT_PED(SEL_MICHAEL), TRUE, TRUE)
		ENDIF
		
		SET_PED_HEADGEAR(GET_SELECT_PED(SEL_MICHAEL), TRUE)
		SET_PED_HEADGEAR(pedCrew[CREW_DRIVER_NEW], TRUE)
		SET_PED_HEADGEAR(pedCrew[CREW_GUNMAN_NEW], TRUE)
		
		IF bIsStealthPath
			PlaySoundFrontend(SND_GAS_MASK, Asset.soundGasMask)
		ENDIF
		
		iCurrentTake = 0
		iLootHelpCounter = 0
		iNearestCase = 0
		
		//iTotalSmashedCases = 0
		iBuddyLootStage = 0
		iUnloadBikes = 0
		
		iEncDialogue = 0
		
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		SET_MAX_WANTED_LEVEL(0)
		SET_FAKE_WANTED_LEVEL(0)
		
		RESET_LOOT_CONTROL_STRUCT(lcCrew)

		RESET_CROWD_CONTROL_STRUCT(ccCrew)
		RESET_CROWD_CONTROL_STRUCT(ccCrew2)
		
		REMOVE_CUTSCENE()
		
		bDrawCashScaleform = TRUE
		bIsHackTimeUp = FALSE
		
		bManagerPopOut = FALSE
			
		SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(V_ILEV_J2_DOOR, <<-629.1, -230.2, 38.2>>, TRUE, 0.0)
		
//		FOR iCounter = 0 TO iNoOfCases-1
//			rfReset = GET_RAYFIRE_MAP_OBJECT(	jcStore[iCounter].vCasePos, fRFCheckRadius,
//												jcStore[iCounter].sSmashAnim)
//			IF DOES_RAYFIRE_MAP_OBJECT_EXIST(rfReset)
//				IF GET_STATE_OF_RAYFIRE_MAP_OBJECT(rfReset) <> RFMO_STATE_START
//					SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfReset, RFMO_STATE_RESET)
//				ENDIF
//			ENDIF
//			jcStore[iCounter].iSmashedByBulletStage = 0
//		ENDFOR
		
		//DISABLE_NAVMESH_IN_AREA(<<-623.1895, -231.5713, 36.8759>>, <<-620.6600, -229.6830, 38.0054>>, TRUE)
						
		//INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(JH2A_HEIST_TIME)
		SET_PLAYER_CLOTH_PIN_FRAMES(PLAYER_ID(), 1)
		
		INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_OPEN(JH2A_CASE_GRAB_TIME) 
		
		SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
		bRemindAbout2Million = FALSE
		//runGodText(Asset.godStealJewels, TRUE)
		
		START_AUDIO_SCENE("JSH_2B_GRAB_LOOT_MAIN")
		
		IF bIsStealthPath
		
			fGasStrength = 0.0
				

			#IF NOT IS_NEXTGEN_BUILD
				SET_TIMECYCLE_MODIFIER("jewel_gas")
				SET_TIMECYCLE_MODIFIER_STRENGTH(fGasStrength)
			#ENDIF	
			
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "STAGE_GRAB_LOOT")
		ELSE
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(2, "STAGE_GRAB_LOOT")	
		ENDIF
		
		SAFE_FADE_IN()
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
		iStageSection = 0
		bStageSetup = TRUE
		bPoliceScanner1 = FALSE
		
		IF Asset.mnBike = BATI2 //bIsStealthPath
			REQUEST_CUTSCENE(Asset.mocapBagThrowA)
		ELSE
			REQUEST_CUTSCENE(Asset.mocapBagThrowB)
		ENDIF
		REQUEST_VEHICLE_RECORDING(Asset.recNumber, Asset.recUber)
		REQUEST_VEHICLE_RECORDING(998,  Asset.recUber)
															//--- Preload next stage ---
		bFlagEnterVehicleDriver = FALSE
		
		REPLAY_RECORD_BACK_FOR_TIME(0.0, 8.0, REPLAY_IMPORTANCE_HIGHEST)
		
		//bPlayerIsOkToTriggerCut = FALSE
		
//═════════╡ UPDATE ╞═════════		
	ELSE
		
		//IF HAS_CUTSCENE_LOADED()	
		//	safeWait(0)
			//IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()
      			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", GET_SELECT_PED(SEL_FRANKLIN))	
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", GET_SELECT_PED(SEL_MICHAEL))	
			//ENDIF
		//ENDIF
				
		UPDATE_STORE_PEDS()
		
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		
		//Control gas effect strength
		IF bIsStealthPath
		
			#IF IS_NEXTGEN_BUILD
				IF NOT DOES_PARTICLE_FX_LOOPED_EXIST(ptfxGas)
					ptfxGas = START_PARTICLE_FX_LOOPED_AT_COORD("scr_jewel_fog_volume", <<-622.0, -231.0, 38>>, <<0.0, 0.0, 0.0>>)
					SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxGas, "scr_jewel_fog_volume", fGasStrength)
				ENDIF
			#ENDIF
		
			IF TIMERA() < (iTimeLimit - 7000)
			AND g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE] < (iNoOfCases - 4)
				fGasStrength = fGasStrength +@ 0.05
				IF fGasStrength >= 1.0
					fGasStrength = 1.0
				ENDIF
			ELSE
				fGasStrength = fGasStrength -@ 0.01
				IF fGasStrength <= 0.0
					fGasStrength = 0.1
				ENDIF
			ENDIF
			//PRINTLN("fGasStrength:", fGasStrength)
			
			#IF IS_NEXTGEN_BUILD
				SET_PARTICLE_FX_LOOPED_EVOLUTION(ptfxGas, "scr_jewel_fog_volume", fGasStrength)
			#ENDIF
			#IF NOT IS_NEXTGEN_BUILD
				SET_TIMECYCLE_MODIFIER_STRENGTH(fGasStrength)
			#ENDIF	
			
			
		ENDIF
		
		UPDATE_WHIMPERS()
		
		UNLOAD_BIKES_IN_BACKGROUND()
				
		IF (NOT bIsRandomGuardEventNow)
		AND (NOT bIsRandomManagerEventNow)
			//When aiming at a ped, chatter at appropriate time
			IF bAimingLineShouldPlay = FALSE									
				FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
					IF IS_PED_BEING_AIMED_AT(pedStore[iCounter])		
						bAimingLineShouldPlay = TRUE
					ENDIF
				ENDFOR
				
				IF IS_PED_BEING_AIMED_AT(pedBackroom)		
				OR IS_PED_BEING_AIMED_AT(pedSecurity)	
					bAimingLineShouldPlay = TRUE
				ENDIF
			ENDIF
			
			IF bAimingLineShouldPlay
			AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				//SCRIPT_ASSERT("boom")
				//IF NOT bConversationOngoing
				//AND NOT bGodTextOngoing
				
					//enumSubtitlesState displayTitles
//					IF (NOT IS_MESSAGE_BEING_DISPLAYED())
//						displayTitles = DISPLAY_SUBTITLES
//					ELSE
//						displayTitles = DO_NOT_DISPLAY_SUBTITLES
//					ENDIF
				
					IF bIsStealthPath
						//RunConversation(Asset.convShoutAtKOPed, TRUE, TRUE, TRUE)
						//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "JH_FIAA", "MICHAEL", SPEECH_PARAMS_FORCE_FRONTEND)
//						IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", Asset.convShoutAtKOPed, CONV_PRIORITY_VERY_HIGH, displayTitles)
//							bAimingLineShouldPlay = FALSE	
//						ENDIF
					ELSE
						//RunConversation(Asset.convShoutAtCowerPed, TRUE, TRUE, TRUE)
						//PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(PLAYER_PED_ID(), "JH_FHAA", "MICHAEL", SPEECH_PARAMS_FORCE_FRONTEND)
						//IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", Asset.convShoutAtCowerPed, CONV_PRIORITY_VERY_HIGH, displayTitles)
//						IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_GETDOWN", CONV_PRIORITY_VERY_HIGH, displayTitles)
//						
//							bAimingLineShouldPlay = FALSE	
//						ENDIF
					ENDIF
				//ELSE
				//	bEventFlag[9] = FALSE
				//ENDIF
			ENDIF
			
		ENDIF
		
		//Deal with crowd control....
		IF NOT (NOT IS_GUNMAN_NORM() AND (bIsRandomManagerEventNow = TRUE AND bIsRandomManagerEventDone = FALSE))	//Let  Packie deal with the manager
			IF NOT bIsStealthPath									// Jewel heist 2B
				IF NOT bIsHackTimeUp				
					IF (GET_SCRIPT_TASK_STATUS(pedCrew[cnAltGunman], SCRIPT_TASK_GO_TO_COORD_WHILE_AIMING_AT_COORD) <> PERFORMING_TASK)	//Getting into position
					AND (GET_SCRIPT_TASK_STATUS(pedCrew[cnAltGunman], SCRIPT_TASK_PERFORM_SEQUENCE) <> PERFORMING_TASK)	//taking care of manager
						DO_PED_CROWD_CONTROL(pedCrew[cnAltGunman], ccCrew)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		SET_INTERIOR_ACTIVE(interiorJewelStore, TRUE)
		SET_ROOM_FOR_GAME_VIEWPORT_BY_KEY(keyJewelStore)

		DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
		
		SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisablePlayerAutoVaulting, TRUE)
		
		SWITCH iStageSection
		
			CASE 0
			
				IF NOT bIsStealthPath
					UPDATE_GUARD_AND_MANAGER()
				ENDIF
			
				IF (NOT bIsRandomGuardEventNow)
				AND	(NOT bIsRandomManagerEventNow)		
					UPDATE_COUNTDOWN_CONVERSATIONS(TIMERA())
				ENDIF
												
				IF NOT bIsHackTimeUp
					IF (NOT bIsRandomGuardEventNow)
					AND (NOT bIsRandomManagerEventNow)				
						
						IF NOT bGodTextOngoing
						AND NOT bConversationOngoing
							IF NOT isAnyConversationWaitingInQueue()
							AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)	
							AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()	
								IF NOT isAnyGodTextWaitingInQueue()
									IF bHasConversationStoppedThisFrame
										SETTIMERB(0)
									ELSE
										IF TIMERB() > 3000
											IF iDisplayedTake > TARGET_TAKE_VALUE
												IF bRemindAbout2Million = FALSE
													addTextToQueue("JH_2MIL", TEXT_TYPE_CONVO)
													bRemindAbout2Million = TRUE
												ELSE
													addTextToQueue(Asset.convMichaelEncouragement, TEXT_TYPE_CONVO)
													addTextToQueue(Asset.convCrewResponds, TEXT_TYPE_CONVO)
												ENDIF
											ELSE
											
												SWITCH iEncDialogue
													
													CASE 0
														IF NOT bIsStealthPath
															//Good gun man reminds people to get down on the ground.
															IF IS_GUNMAN_PACKIE()
																addTextToQueue("JH_STAYDWNGD", TEXT_TYPE_CONVO)	
															ELIF IS_GUNMAN_GUSTAVO()
																addTextToQueue("JH_CTRL", TEXT_TYPE_CONVO)	
															ELIF IS_GUNMAN_NORM()
																PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedCrew[CREW_GUNMAN_NEW], "JH_CJAA", "NORM")
															ENDIF
															//Bad gun man doesn't
														ENDIF
														iEncDialogue++
													BREAK
												
													CASE 1
														//Michael shouts encouragement to the guy robbing
														addTextToQueue(Asset.convMichaelEncouragement, TEXT_TYPE_CONVO)
														iEncDialogue++
													BREAK
													CASE 2
														IF GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(cnSmashGunman))) = CMSK_GOOD
															addTextToQueue("JH_MENCGP", TEXT_TYPE_CONVO)
														ELSE
															addTextToQueue("JH_MENCN", TEXT_TYPE_CONVO)
														ENDIF
//
//														IF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)) = CM_GUNMAN_G_GUSTAV
//															addTextToQueue("JH_RESPG", TEXT_TYPE_CONVO)
//														ELIF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)) = CM_GUNMAN_G_PACKIE_UNLOCK
//															addTextToQueue("JH_CRSPND", TEXT_TYPE_CONVO)
//														ELIF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)) = CM_GUNMAN_B_NORM
//															addTextToQueue("JH_RESPN", TEXT_TYPE_CONVO)
															addTextToQueue(Asset.convCrewResponds, TEXT_TYPE_CONVO)
															PLAY_SOUND_FROM_ENTITY(-1, "DROP_ITEMS", pedCrew[cnSmashGunman], "JEWEL_HEIST_SOUNDS")
															
//														ENDIF
																												
														iEncDialogue++
													BREAK
													CASE 3
														//addTextToQueue(Asset.convMichaelEncouragement, TEXT_TYPE_CONVO)
														iEncDialogue = 0
													BREAK
												ENDSWITCH
																								
												addTextToQueue(Asset.convCrewResponds, TEXT_TYPE_CONVO)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF

				//IF (NOT bIsRandomGuardEventNow) AND (NOT bIsRandomManagerEventNow)
//					IF (NOT bIsHackTimeUp)	//Buddy loot in idle state
						UPDATE_BUDDY_LOOT_NEW(pedCrew[cnSmashGunman], lcCrew) //, vehBike[0])						
//					ENDIF
				
					smashShotCase()
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK1) 
			        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK2)
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_ALTERNATE)
					
					
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT) 
					DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY) 
					
					IF bIsPlayerLooting
						UPDATE_PLAYER_LOOT()
					ELSE
						updateStoreBlips()
						
						findNearestCase()
						
						IF ARE_STRINGS_EQUAL(jcStore[iNearestCase].sSmashAnim, Asset.rfSmash)
							strAnimName = Asset.animSmashNecklace
						ELIF ARE_STRINGS_EQUAL(jcStore[iNearestCase].sSmashAnim, Asset.rfSmash2)
							strAnimName = Asset.animSmashE
						ELIF ARE_STRINGS_EQUAL(jcStore[iNearestCase].sSmashAnim, Asset.rfSmash3)
							strAnimName = Asset.animSmashTrayA
						ELIF ARE_STRINGS_EQUAL(jcStore[iNearestCase].sSmashAnim, Asset.rfSmash4)
							strAnimName = Asset.animSmashTrayB	
						ENDIF
						
						IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_MICHAEL), GET_ANIM_INITIAL_OFFSET_POSITION(Asset.adJewelHeist,strAnimName, jcStore[iNearestCase].vPlayerPos, jcStore[iNearestCase].vPlayerRot ), <<0.75, 0.75, 1.0>>)
							DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)											
						ENDIF
											
						IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_MICHAEL), GET_ANIM_INITIAL_OFFSET_POSITION(Asset.adJewelHeist,strAnimName, jcStore[iNearestCase].vPlayerPos, jcStore[iNearestCase].vPlayerRot ), <<0.75, 0.75, 1.0>>)
						AND NOT IS_ENTITY_AT_COORD(pedCrew[cnSmashGunman], GET_ANIM_INITIAL_OFFSET_POSITION(Asset.adJewelHeist,strAnimName, jcStore[iNearestCase].vPlayerPos, jcStore[iNearestCase].vPlayerRot ), <<0.75, 0.75, 1.0>>)
						AND NOT jcStore[iNearestCase].bSmashed
						AND NOT IS_PED_IN_ANY_VEHICLE(GET_SELECT_PED(SEL_MICHAEL))			//Is player near enough to a case
						AND IS_PED_HEADING_TOWARDS_POSITION(GET_SELECT_PED(SEL_MICHAEL), jcStore[iNearestCase].vCasePos, 135/*90*/)
						AND NOT IS_PHONE_ONSCREEN()		
						AND IS_GAMEPLAY_CAM_RENDERING()
							IF NOT bHasPrintedCaseHelp
								IF iLootHelpCounter < 2
									
									IF IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
									
										IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("H_HELPSTEAL_KM")
											PRINTLN("*************PRINTING STEAL HELP PC *******************")
											//runHelpText(Asset.hlpSteal, FALSE, TRUE, TRUE, TRUE)
											PRINT_HELP_FOREVER("H_HELPSTEAL_KM")
											safeWait(0)
											bHasPrintedCaseHelp = TRUE
										ENDIF
									
									ELSE
									
										IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED(Asset.hlpSteal)
											PRINTLN("*************PRINTING STEAL HELP*******************")
											//runHelpText(Asset.hlpSteal, FALSE, TRUE, TRUE, TRUE)
											PRINT_HELP_FOREVER(Asset.hlpSteal)
											safeWait(0)
											bHasPrintedCaseHelp = TRUE
										ENDIF
									
									ENDIF
									
								ENDIF
							ELSE
								bHasPrintedCaseHelp = FALSE
							ENDIF
							
							IF (IS_DISABLED_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_SCRIPT_RRIGHT)
								AND NOT IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL))//INPUT_FRONTEND_LB)				//Are they pressing the smash button?
							OR (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_CONTEXT)
								AND NOT IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_AIM)
								AND IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL))
//								IF NOT bIsDoorLocked
//								AND NOT bIsHackTimeUp
//									controlJewelStoreDoors(TRUE, 0.0)									
//									bIsDoorLocked = TRUE
//								ENDIF
								rfCaseSmash = GET_RAYFIRE_MAP_OBJECT(	jcStore[iNearestCase].vCasePos, fRFCheckRadius,
															jcStore[iNearestCase].sSmashAnim)
								
								IF jcStore[iNearestCase].iSmashedByBulletStage = 0
									SET_STATE_OF_RAYFIRE_MAP_OBJECT(rfCaseSmash, RFMO_STATE_PRIMING)
								ENDIF
								iLootHelpCounter++
								SETUP_PLAYER_LOOT(jcStore[iNearestCase])			//Smash the case			
							ENDIF
						ELSE
							IF isTextPlaying(Asset.hlpSteal)
							OR (IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL) AND isTextPlaying("H_HELPSTEAL_KM"))
								clearText(FALSE, FALSE, TRUE)
							ENDIF
						ENDIF
						
						IF NOT bIsHackTimeUp
												
								IF NOT IS_TASK_ONGOING(pedCrew[cnSmashGunman], SCRIPT_TASK_PLAY_ANIM)
								AND NOT bIsPlayerLooting
								AND (iBuddyLootStage > 100 OR iBuddyLootStage = 0)	//Buddy not looting
								
									//Request the traffic warden
									IF TIMERA() > (iTimeLimit - 7000)	//5 seconds to go
									OR g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE] > (iNoOfCases - 4)	//3 cases to go
																				
										REQUEST_MODEL(IG_TRAFFICWARDEN)
									ENDIF
								
									IF (TIMERA() > iTimeLimit
									OR g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE] = iNoOfCases)
										IF HAS_MODEL_LOADED(IG_TRAFFICWARDEN)
										AND IS_ENTITY_IN_STORE(PLAYER_PED_ID())
											INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED() 
											IF NOT DOES_ENTITY_EXIST(pedTrafficWarden)
												pedTrafficWarden = CREATE_PED(PEDTYPE_MISSION, IG_TRAFFICWARDEN, <<-629.4808, -242.2689, 37.2096>>, 45.1069)
											ENDIF
											SET_PED_LOD_MULTIPLIER(pedTrafficWarden, 1.5)
											
											SET_PED_IS_AVOIDED_BY_OTHERS(pedTrafficWarden, FALSE)
											
											STOP_PED_SPEAKING(pedTrafficWarden, TRUE)
											
											NAVDATA ndStruct
		
											ndStruct.m_fSlideToCoordHeading = 170.4931 // 168.8842
										
											OPEN_SEQUENCE_TASK(seqSequence)
												//TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-632.8882, -239.3724, 37.0902>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 2, 0.5, ENAV_DEFAULT, ndStruct.m_fSlideToCoordHeading)
												TASK_FOLLOW_NAV_MESH_TO_COORD(NULL, <<-633.0743, -240.2480, 37.0325>>, PEDMOVE_WALK, DEFAULT_TIME_BEFORE_WARP * 2, 0.5, ENAV_DEFAULT, ndStruct.m_fSlideToCoordHeading)
												TASK_TURN_PED_TO_FACE_ENTITY(NULL, GET_SELECT_PED(SEL_FRANKLIN))
												TASK_PLAY_ANIM(NULL, Asset.adJewelHeist, Asset.animImTellingYou, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
												TASK_PLAY_ANIM(NULL, Asset.adJewelHeist, Asset.animWhy, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
												TASK_PLAY_ANIM(NULL, Asset.adJewelHeist, Asset.animDespair, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
												TASK_PLAY_ANIM(NULL, Asset.adJewelHeist, "warden", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
												TASK_PLAY_ANIM(NULL, Asset.adJewelHeist, Asset.animDamn, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
												TASK_PLAY_ANIM(NULL, Asset.adJewelHeist, Asset.animDespair, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
												TASK_PLAY_ANIM(NULL, Asset.adJewelHeist, "warden", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
												TASK_PLAY_ANIM(NULL, Asset.adJewelHeist, Asset.animWhy, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
												TASK_PLAY_ANIM(NULL, Asset.adJewelHeist, Asset.animDespair, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
												TASK_PLAY_ANIM(NULL, Asset.adJewelHeist, "warden", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
												TASK_PLAY_ANIM(NULL, Asset.adJewelHeist, Asset.animDamn, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1)
												TASK_PLAY_ANIM(NULL, Asset.adJewelHeist, Asset.animImTellingYou, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING | AF_UPPERBODY)
											CLOSE_SEQUENCE_TASK(seqSequence)
											TASK_PERFORM_SEQUENCE(pedTrafficWarden, seqSequence)
											CLEAR_SEQUENCE_TASK(seqSequence)
									
											
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTrafficWarden, TRUE)
											SET_PED_RELATIONSHIP_GROUP_HASH(pedTrafficWarden, RELGROUPHASH_PLAYER)
											SET_PED_COMPONENT_VARIATION(pedTrafficWarden, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
											SET_PED_COMPONENT_VARIATION(pedTrafficWarden, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
											SET_PED_COMPONENT_VARIATION(pedTrafficWarden, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
											SET_PED_COMPONENT_VARIATION(pedTrafficWarden, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
											SET_PED_PROP_INDEX(pedTrafficWarden, INT_TO_ENUM(PED_PROP_POSITION,0), 0, 0)

																			
											#IF IS_DEBUG_BUILD
												PRINTLN("iUnloadBikes:", iUnloadBikes)
												IF iUnloadBikes <= 13
													PRINTLN("Well done, you managed to grab all the loot before Franklin could unload the bikes. PLease add a bug for Ross W")
												ENDIF
											#ENDIF
											
											IF bIsDoorLocked
												controlJewelStoreDoors(FALSE, 0.0)
												bIsDoorLocked = FALSE
											ENDIF
																																									
											SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBike[0], FALSE)
											SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBike[1], FALSE)
											
											IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(Asset.mnBike)
											
											emptyTextQueue()
											
											//runConversation("JH_GOT_ALL", TRUE)
											//Franklin says coast is clear
											addTextToQueue("JH_CLEAR", TEXT_TYPE_CONVO)	
											
											
											IF (iCurrentTake > 0 AND iCurrentTake < TARGET_TAKE_VALUE)
												addTextToQueue("JH_SCORE_BAD", TEXT_TYPE_CONVO)
											ELIF (iCurrentTake > TARGET_TAKE_VALUE AND iCurrentTake < 5000000)
												addTextToQueue("JH_SCORE_MED", TEXT_TYPE_CONVO)
											ELIF (iCurrentTake > 5000000)
												addTextToQueue("JH_SCORE_GID", TEXT_TYPE_CONVO)
											ENDIF
											
											REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
																
											addTextToQueue(Asset.godLeaveStore, TEXT_TYPE_GOD)
											//Tell the guys if they leave now there should be no cops.
											//addTextToQueue("JH_LATECOPS1", TEXT_TYPE_CONVO)
											
											SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
											
											IF NOT IS_ENTITY_ATTACHED(GET_SELECT_PED(SEL_MICHAEL))
												FREEZE_ENTITY_POSITION(GET_SELECT_PED(SEL_MICHAEL), FALSE)
											ENDIF
											IF NOT IS_ENTITY_ATTACHED(pedCrew[CREW_DRIVER_NEW])
												FREEZE_ENTITY_POSITION(pedCrew[CREW_DRIVER_NEW], FALSE)
											ENDIF
											
											TASK_LOOK_AT_ENTITY(pedCrew[cnSmashGunman], GET_SELECT_PED(SEL_MICHAEL), 60000,  SLF_WHILE_NOT_IN_FOV)
											TASK_LOOK_AT_ENTITY(pedCrew[cnAltGunman], GET_SELECT_PED(SEL_MICHAEL), 60000,  SLF_WHILE_NOT_IN_FOV)
											
											TASK_LOOK_AT_ENTITY(GET_SELECT_PED(SEL_FRANKLIN), GET_SELECT_PED(SEL_MICHAEL), 60000,  SLF_WHILE_NOT_IN_FOV)
																																	
											PRELOAD_NEED_LIST_MODEL(Asset.mnBag)
											bIsHackTimeUp = TRUE
										ENDIF
									ENDIF						
								ENDIF
							
						ELSE
							
							IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0], TRUE)
							AND NOT IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[0], TRUE)
							//AND NOT IS_TASK_ONGOING(pedCrew[CREW_DRIVER_NEW], SCRIPT_TASK_ENTER_VEHICLE)								
							AND NOT (GET_SCRIPT_TASK_STATUS(pedCrew[CREW_DRIVER_NEW], SCRIPT_TASK_ENTER_VEHICLE) = PERFORMING_TASK)
//								#IF IS_DEBUG_BUILD
//								
//									MODEL_NAMES thisMOdel
//									GET_PED_MODEL_FROM_INDEX(NATIVE_TO_INT(GET_PED_IN_VEHICLE_SEAT(vehBike[0])), thisMOdel)
//									PRINTLN("Current VehBike[0] driver:" , GET_MODEL_NAME_FOR_DEBUG(thisMOdel))
//								#ENDIF
								
								IF bFlagEnterVehicleDriver = FALSE
									TASK_ENTER_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
									bFlagEnterVehicleDriver = TRUE
								ENDIF
							ENDIF
							
							IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
							AND NOT IS_TASK_ONGOING(pedCrew[CREW_GUNMAN_NEW], SCRIPT_TASK_ENTER_VEHICLE)
							
								IF NOT bIsStealthPath
									//Allow the smasher to get on the bike first to avoid issues.
									IF GET_DISTANCE_BETWEEN_ENTITIES(pedCrew[CREW_DRIVER_NEW], vehBike[0]) < 9.0
										TASK_ENTER_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_RUN, ECF_USE_LEFT_ENTRY)
									ENDIF
								ELSE
									TASK_ENTER_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1], DEFAULT_TIME_BEFORE_WARP, VS_DRIVER, PEDMOVE_RUN, ECF_USE_LEFT_ENTRY)
								ENDIF
							ENDIF
							
						ENDIF
						
						//Reached the doors...
						IF iCurrentTake >= TARGET_TAKE_VALUE
						OR bIsHackTimeUp
						
							IF NOT IS_ENTITY_DEAD(pedTrafficWarden)
								vTrafficWardenCoords = GET_ENTITY_COORDS(pedTrafficWarden)
							ENDIF
						
							IF IS_ENTITY_IN_ANGLED_AREA(GET_SELECT_PED(SEL_MICHAEL), <<-630.151917,-238.298950,37.067986>>, <<-631.641663,-236.044220,40.444160>>, 1.000000)
							OR (iCurrentTake >= TARGET_TAKE_VALUE AND IS_BULLET_IN_AREA(vTrafficWardenCoords, 3.0))
								iStageSection++
							ENDIF
						ENDIF
						
						IF g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE] = (iNoOfCases)	//3 cases to go
						AND bWeGotItAllPrinted = FALSE
							IF GET_RANDOM_BOOL()
								runConversation("JH_SMASHALL", TRUE)	
							ELSE
								runConversation("JH_SMASHALL", TRUE)								
							ENDIF
							bWeGotItAllPrinted = TRUE
						ENDIF
						
						//Lets go times up etc..
						IF bIsHackTimeUp
						AND bWeGotItAllPrinted = FALSE
							runConversation("JH_MBYE", TRUE)								
							bWeGotItAllPrinted = TRUE
						ENDIF
						
						
						//Timed stuff...
						
						IF bIsAlarmRinging = FALSE
						AND TIMERA() > (iTimeLimit + 3500)
							
							IF PREPARE_ALARM("JEWEL_STORE_HEIST_ALARMS")
								START_ALARM("JEWEL_STORE_HEIST_ALARMS", FALSE)
								
								//Coast is clear if we move now!
																								
								bIsAlarmRinging = TRUE
							ENDIF
						ENDIF
						
						IF bPoliceScanner1 = FALSE
						AND TIMERA() > (iTimeLimit + 5000)
							PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_JSH_2A_03", 0.0)
							bPoliceScanner1 = TRUE
						ENDIF
						
						
						IF NOT g_bLateLeaveJewelStore
							IF TIMERA() > (iTimeLimit + 15000)
								//Warn the guys the cops will be on their ass.
								
								SWITCH GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)))
									CASE CMSK_GOOD
										addTextToQueue("JH_HCKACOPS", TEXT_TYPE_CONVO)	
									BREAK
									CASE CMSK_MEDIUM
										addTextToQueue("JH_HCKBCOPS", TEXT_TYPE_CONVO)	
									BREAK
									CASE CMSK_BAD
										//addTextToQueue("JH_HCKACOPS", TEXT_TYPE_CONVO)	
										addTextToQueue("JH_LATECOPS2", TEXT_TYPE_CONVO)
									BREAK
								ENDSWITCH
								
								g_bLateLeaveJewelStore = TRUE
							ENDIF
						ENDIF
						
						IF TIMERA() > (iTimeLimit + 30000)
						OR bWaitedTooLong = TRUE	//Mostly for debug use...
							//SCRIPT_ASSERT("Failed")
							
							SWITCH GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)))
								CASE CMSK_GOOD
									RunConversation("JH_HCKAOUT", TRUE, TRUE, TRUE)
								BREAK
								CASE CMSK_MEDIUM
									RunConversation("JH_HCKBOUT", TRUE, TRUE, TRUE)
								BREAK
								CASE CMSK_BAD
									//addTextToQueue("JH_HCKCOUT", TEXT_TYPE_CONVO)JH_HCKCCOPS
									RunConversation("JH_HCKCCOPS", TRUE, TRUE, TRUE)
									addTextToQueue("JH_LATECOPS3", TEXT_TYPE_CONVO)
								BREAK
							ENDSWITCH
							
							IF NOT IS_PED_INJURED(pedTrafficWarden)
								CLEAR_PED_TASKS(pedTrafficWarden)
								TASK_FOLLOW_NAV_MESH_TO_COORD(pedTrafficWarden, <<-609.3799, -286.0605, 37.7909>>, PEDMOVE_WALK)
							ENDIF
							//RunConversation("JH_LATECOPS3", TRUE, TRUE, TRUE)
							bWaitedTooLong = TRUE
							iStageSection = 2
						ENDIF
						
						
					ENDIF
				//ENDIF
			BREAK
		ENDSWITCH		
	
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 1
						
			STOP_AUDIO_SCENE("JSH_2B_GRAB_LOOT_MAIN")
			
			INFORM_MISSION_STATS_SYSTEM_OF_TIME_WINDOW_CLOSED()
			
			ReleaseSound(SND_SMASH_CABINET)
			ReleaseSound(SND_SMASH_CABINET_NPC)
			IF bIsStealthPath
				ReleaseSound(SND_GAS_ESCAPE)
			ENDIF
			ReleaseSound(SND_GAS_MASK)
						
			DISABLE_NAVMESH_IN_AREA(<<-623.1895, -231.5713, 36.8759>>, <<-620.6600, -229.6830, 38.0054>>, FALSE)
			
			FOR iCounter = 0 TO iNoOfCases-1
				IF DOES_BLIP_EXIST(jcStore[iCounter].blipCase)
					REMOVE_BLIP(jcStore[iCounter].blipCase)
				ENDIF
			ENDFOR
						
			IF bWaitedTooLong = TRUE
				bStageSetup = FALSE
				iStageSection = 0
				clearText()
				emptyTextQueue()	
				eMissionStage = STAGE_FAIL_CUTSCENE
				RETURN FALSE
			ENDIF
						
			IF DOES_ENTITY_EXIST(lcCrew.objGrab)
				DELETE_OBJECT(lcCrew.objGrab)
			ENDIF
			IF DOES_ENTITY_EXIST(objGrenade)
				DELETE_OBJECT(objGrenade)
			ENDIF
			IF DOES_ENTITY_EXIST(objPhone)
				DELETE_OBJECT(objPhone)
			ENDIF
			IF DOES_BLIP_EXIST(blipLocate)
				REMOVE_BLIP(blipLocate)
			ENDIF
			FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
				IF DOES_ENTITY_EXIST(pedStore[iCounter])
					IF NOT IS_PED_INJURED(pedStore[iCounter])
						SET_PED_KEEP_TASK(pedStore[iCounter], TRUE)
					ENDIF
					SET_PED_AS_NO_LONGER_NEEDED(pedStore[iCounter])
				ENDIF
			ENDFOR
			
			IF DOES_ENTITY_EXIST(pedBackroom)
				IF NOT IS_PED_INJURED(pedBackroom)
					SET_PED_KEEP_TASK(pedBackroom, TRUE)
				ENDIF
				SET_PED_AS_NO_LONGER_NEEDED(pedBackroom)
			ENDIF
			
			IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(Asset.mnJewels1)	
			IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(Asset.mnJewels2)	
			IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(Asset.mnJewels4a)	
			IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(Asset.mnJewels4b)	
			
			//g_replay.iReplayInt[CURRENT_TAKE_ XXX REPLAY_VALUE] = iCurrentTake				
			
			IF IS_INTERIOR_READY(interiorJewelStore)
				UNPIN_INTERIOR(interiorJewelStore)
			ENDIF
			SET_INTERIOR_ACTIVE(interiorJewelStore, FALSE)
			
			CLEAR_ROOM_FOR_GAME_VIEWPORT()
						
			IF bIsDoorLocked
				controlJewelStoreDoors(FALSE, 0.0)
				bIsDoorLocked = FALSE
			ENDIF
			
			REMOVE_WEAPON_ASSET(mnMainWeaponType)
			
			clearText()
			emptyTextQueue()	
			
			bDrawCashScaleform = FALSE
	
			bStageSetup = FALSE
			
			iStageSection = 0
			
			IF iCurrentTake < TARGET_TAKE_VALUE
				bInSufficientTake = TRUE
			ENDIF
									
			RETURN TRUE
			
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

PROC HIDE_ENTITY(ENTITY_INDEX thisEntity, BOOL bHide)
	//SCRIPT_ASSERT("boom")
	
	IF DOES_ENTITY_EXIST(thisEntity)
	
		SET_ENTITY_VISIBLE(thisEntity, NOT bHide)
		
		IF NOT IS_PED_IN_ANY_VEHICLE(GET_PED_INDEX_FROM_ENTITY_INDEX(thisEntity))
			SET_ENTITY_COLLISION(thisEntity, NOT bHide)
		ENDIF
		IF NOT IS_ENTITY_ATTACHED_TO_ANY_OBJECT(thisEntity)
			IF NOT IS_PED_IN_ANY_VEHICLE(GET_PED_INDEX_FROM_ENTITY_INDEX(thisEntity))
				FREEZE_ENTITY_POSITION(thisEntity, bHide)
			ENDIF
		ENDIF

		IF bHide
			SET_ENTITY_COORDS(thisEntity, << -722.6866, -346.5041, 34.4500 >>)
		ENDIF
	ENDIF
	
ENDPROC

BOOL bCutsceneSetup

//PURPOSE:		Performs the cutscene where Franklin drops a grenade into the store from the roof
FUNC BOOL stageCutsceneLeaveStore()
	INT i_setpiece_size
	INT i_traffic_size
//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
	
		RELEASE_NAMED_SCRIPT_AUDIO_BANK(Asset.sbRaidGlassSmash)
		RELEASE_NAMED_SCRIPT_AUDIO_BANK(Asset.sbUnderwater)
		
		IF bIsStealthPath
			STOP_SOUND(sndList[SND_GAS_MASK].SoundId)
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(GET_REPLAY_STAGE_FROM_MISSION_STAGE(STAGE_CUTSCENE_LEAVE_STORE), "STAGE_CUTSCENE_LEAVE_STORE")
		ELSE
			//SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(GET_REPLAY_STAGE_FROM_MISSION_STAGE(STAGE_CUTSCENE_LEAVE_STORE), "STAGE_CUTSCENE_LEAVE_STORE")
		ENDIF
		
		//REQUEST_VEHICLE_RECORDING(iBikePopHackRec, "JHHACK")
		SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", TRUE)
		CLEAR_ALL_BROKEN_GLASS()
		
		DELETE_PED(pedStore[0])
		DELETE_PED(pedStore[1])
		DELETE_PED(pedStore[2])
		DELETE_PED(pedStore[3])
		DELETE_PED(pedStore[4])
		DELETE_PED(pedSecurity)
		
		IF interiorJewelStore <> NULL
			UNPIN_INTERIOR(interiorJewelStore)
		ENDIF
		
		
		IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_FRANKLIN))
		AND NOT IS_ENTITY_DEAD(vehBike[2])	
			IF NOT IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
				SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
				SET_PED_BAG(GET_SELECT_PED(SEL_FRANKLIN), FALSE)
				REMOVE_PED_HELMET(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
				
				SET_PED_HELMET_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), 6)
				SET_PED_HELMET_TEXTURE_INDEX(GET_SELECT_PED(SEL_FRANKLIN), 8)
				SET_PED_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_PROP_POSITION,0), 6, 8)
				
				SET_PED_HELMET(GET_SELECT_PED(SEL_FRANKLIN), TRUE)	
				TASK_LOOK_AT_ENTITY(GET_SELECT_PED(SEL_FRANKLIN), GET_SELECT_PED(SEL_MICHAEL), 5000,  SLF_WHILE_NOT_IN_FOV)
				
				//Preload bag
				SET_PED_PRELOAD_VARIATION_DATA(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_COMPONENT,9), 6, 0)
				//SET_PED_COMPONENT_VARIATION(thisPed, INT_TO_ENUM(PED_COMPONENT,9), 6, 0, 0) //(task)
			ENDIF
		ENDIF
		
		REMOVE_PTFX_ASSET()
		
		RESET_NEED_LIST()
		MANAGE_LOADING_NEED_LIST()
		
		REQUEST_VEHICLE_RECORDING(Asset.recNumber, Asset.recUber)
		REQUEST_VEHICLE_RECORDING(998,  Asset.recUber)
		REQUEST_VEHICLE_RECORDING(722, Asset.recUber) //Franklins bike jump
		REQUEST_ANIM_DICT("missheist_jewel")
		REQUEST_PTFX_ASSET()
		
		IF g_bLateLeaveJewelStore
			REQUEST_VEHICLE_RECORDING(001, "JHLateCops")
			REQUEST_VEHICLE_RECORDING(002, "JHLateCops")
			REQUEST_VEHICLE_RECORDING(003, "JHLateCops")	//Pre stream for next stage
			REQUEST_MODEL(Asset.mnPolice)
			REQUEST_MODEL(Asset.mnPoliceman)
		ENDIF
		
		IF Asset.mnBike = BATI2  //bIsStealthPath
			//SAFE_REQUEST_MOCAP_CUTSCENE(Asset.mocapBagThrowA, TRUE, TRUE)			
			REQUEST_CUTSCENE(Asset.mocapBagThrowA)			
		ELSE
			//SAFE_REQUEST_MOCAP_CUTSCENE(Asset.mocapBagThrowB, TRUE, TRUE)
			//SET_PED_HEADGEAR(GET_SELECT_PED(SEL_MICHAEL), FALSE)	
			REQUEST_CUTSCENE(Asset.mocapBagThrowB)					
		ENDIF
		
		REQUEST_VEHICLE_ASSET(ASset.mnBike)
				
		WHILE NOT HAS_CUTSCENE_LOADED()	
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(Asset.recNumber,  Asset.recUber)
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(998,  Asset.recUber)
		OR NOT HAS_ANIM_DICT_LOADED("missheist_jewel")
			safeWait(0)
			IF CAN_REQUEST_ASSETS_FOR_CUTSCENE_ENTITY()							
      			SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Franklin", GET_SELECT_PED(SEL_FRANKLIN))	
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", GET_SELECT_PED(SEL_MICHAEL))	
				IF DOES_ENTITY_EXIST(pedTrafficWarden)
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Traffic_Warden", pedTrafficWarden)	
				ENDIF
			ENDIF
						
		ENDWHILE
		
		SAFE_REQUEST_INTERIOR_AT_COORDS_WITH_TYPE(interiorJewelStore, << -625.1706, -232.8408, 39.4937 >>, "V_JEWEL2", TRUE, TRUE)
		
		ADD_ALL_PEDS_TO_DIALOGUE()
							
		SET_PED_BAG(pedCrew[CREW_GUNMAN_NEW], TRUE)
								
		SETTIMERA(0)
		
		//register vehbike[2] as Asset.mnBike
				
		SAFE_FADE_IN()
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
		iStageSection = 0
		bCameraShotSetup = FALSE
		bStageSetup = TRUE
		
		bCutsceneSetup = FALSE
		//If we are on the stealth route swtich to franklin for bike cahse
		//otherwise we remain as Michael for the shootout.
	
		
		IF NOT IS_ENTITY_DEAD(vehBike[2])
			STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[2])
			IF Asset.mnBike = BATI2
				REGISTER_ENTITY_FOR_CUTSCENE(vehBike[2], "Franklins_Heist_Bike", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, Asset.mnBike)
			ELSE
				REGISTER_ENTITY_FOR_CUTSCENE(vehBike[2], "Franklins_Heist_Bike", CU_ANIMATE_EXISTING_SCRIPT_ENTITY, Asset.mnBike)
			ENDIF
		ELSE
			SCRIPT_ASSERT("Bike died for before cut started")
		ENDIF
		
		IF DOES_ENTITY_EXIST(pedTrafficWarden)
			IF NOT IS_ENTITY_DEAD(pedTrafficWarden)
				REGISTER_ENTITY_FOR_CUTSCENE(pedTrafficWarden, "Traffic_Warden", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
			ENDIF
		ENDIF
		
		REGISTER_ENTITY_FOR_CUTSCENE(GET_SELECT_PED(SEL_MICHAEL), "Michael", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
		
		//REGISTER_ENTITY_FOR_CUTSCENE(pedPasserBy, "passer_by", CU_CREATE_AND_ANIMATE_NEW_SCRIPT_ENTITY, A_M_M_BEVHILLS_02)
		
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
		
		REGISTER_SELECT_PEDS_FOR_CUTSCENE(TRUE, TRUE, FALSE)
		
		KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
		
		START_CUTSCENE()
		
		REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
		
		PRELOAD_NEED_LIST_MODEL(Asset.mnBike)
							
		SAFE_REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL, FALSE, FALSE)
		
		LOAD_AND_MANAGE_UBER_ARRAY()
		i_setpiece_size = COUNT_OF(SetPieceCarID)
		i_traffic_size = COUNT_OF(TrafficCarID)
		
		REPEAT i_setpiece_size iCounter
			IF SetPieceCarRecording[iCounter] > 0
				REQUEST_VEHICLE_RECORDING(SetPieceCarRecording[iCounter], Asset.recUber)
			ENDIF
		ENDREPEAT

		REPEAT i_traffic_size iCounter
			IF TrafficCarRecording[iCounter] > 0
				REQUEST_VEHICLE_RECORDING(TrafficCarRecording[iCounter], Asset.recUber)
			ENDIF
		ENDREPEAT
		
		bMusicEvent7 = FALSE
		
		PREPARE_ALARM("JEWEL_STORE_HEIST_ALARMS")
		
		IF g_bLateLeaveJewelStore
			PRELOAD_NEED_LIST_MODEL(Asset.mnPolice)
			PRELOAD_NEED_LIST_MODEL(Asset.mnPoliceman)
			PRELOAD_NEED_LIST_VEHICLE_RECORDING("JHLateCops", 001)
			PRELOAD_NEED_LIST_VEHICLE_RECORDING("JHLateCops", 002)
			PRELOAD_NEED_LIST_VEHICLE_RECORDING("JHLateCops", 003)
		ENDIF
		
		HIDE_HUD_AND_RADAR_THIS_FRAME()
		
		iDisplayedTake = iCurrentTake 
		
//â•â•â•â•â•â•â•â•â•â•¡ UPDATE â•žâ•â•â•â•â•â•â•â•â•		
	ELSE
		
//		VECTOR vCoords
//		FLOAT fHeading
		
		SWITCH iStageSection
			CASE 0
			CASE 1
			CASE 2
			
				IF IS_CUTSCENE_PLAYING()
				AND bCutsceneSetup = FALSE
					SET_CLOCK_TIME(15,00,0)
				
					#IF IS_NEXTGEN_BUILD
						IF DOES_PARTICLE_FX_LOOPED_EXIST(ptfxGas)
							STOP_PARTICLE_FX_LOOPED(ptfxGas)
						ENDIF
					#ENDIF
				
					#IF NOT IS_NEXTGEN_BUILD
						CLEAR_TIMECYCLE_MODIFIER()
					#ENDIF
									
					//SET_PED_HEADGEAR(GET_SELECT_PED(SEL_MICHAEL), FALSE)
					SET_PED_BAG(GET_SELECT_PED(SEL_MICHAEL), FALSE)
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(Asset.recNumber, Asset.recUber)
					AND NOT IS_ENTITY_DEAD(vehBike[0])
						STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[0])
						SET_ENTITY_COORDS(vehBike[0], GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(Asset.recNumber, 1000.0, Asset.recUber) + vBatiRecOffset)
						SET_ENTITY_ROTATION(vehBike[0], GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(Asset.recNumber, 1000.0, Asset.recUber))
						FREEZE_ENTITY_POSITION(vehBike[0], TRUE)
						SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehBike[0],TRUE)
					ENDIF
					
					IF HAS_VEHICLE_RECORDING_BEEN_LOADED(SetPieceCarRecording[0], Asset.recUber)
					AND NOT IS_ENTITY_DEAD(vehBike[1])
						STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[1])
						SET_ENTITY_COORDS(vehBike[1], vBatiRecOffset + GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(SetPieceCarRecording[0], 1000.0, Asset.recUber))
						SET_ENTITY_ROTATION(vehBike[1], GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(SetPieceCarRecording[0], 1000.0, Asset.recUber))
						FREEZE_ENTITY_POSITION(vehBike[1], TRUE)
						SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehBike[1],TRUE)
					ENDIF
					
//					IF NOT IS_ENTITY_DEAD(vehBike[2])
//						SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehBike[2], TRUE)//, FALSE, FALSE)	
//					ENDIF
					
					CLEAR_AREA_OF_PEDS(<<-636.56, -241.84, 38.41>>, 200.00, TRUE)	
					CLEAR_AREA_OF_VEHICLES(<<-636.56, -241.84, 38.41>>, 200.00, TRUE)	
					CLEAR_AREA_OF_PROJECTILES(<<-636.56, -241.84, 38.41>>, 200.00, TRUE)	
									
					
					IF NOT IS_PED_INJURED(pedSecurity)
						SET_PED_KEEP_TASK(pedSecurity, TRUE)
					ENDIF
					SET_PED_AS_NO_LONGER_NEEDED(pedSecurity)
					
					DELETE_VEHICLE(vehTruck)
					DELETE_PED(pedCrew[CREW_HACKER])
					SET_MODEL_FOR_CREW_MEMBER_AS_NO_LONGER_NEEDED(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)))
					
					IF NOT IS_ENTITY_DEAD(vehBike[0])
					AND NOT IS_ENTITY_DEAD(pedCrew[CREW_DRIVER_NEW])
						IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
							SET_PED_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
						ENDIF
						SET_PED_HEADGEAR(pedCrew[CREW_DRIVER_NEW], FALSE)
						SET_PED_BAG(pedCrew[CREW_DRIVER_NEW], TRUE)
						
						//Driver
						SET_PED_PROP_INDEX(pedCrew[CREW_DRIVER_NEW], INT_TO_ENUM(PED_PROP_POSITION,0), 3, 0)
						doBikeEntryLogic(pedCrew[CREW_DRIVER_NEW], vehBike[0])
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(pedCrew[CREW_GUNMAN_NEW])
					AND NOT IS_ENTITY_DEAD(vehBike[1])
						IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
							SET_PED_INTO_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
						ENDIF	
						SET_PED_HEADGEAR(pedCrew[CREW_GUNMAN_NEW], FALSE)
						SET_PED_PROP_INDEX(pedCrew[CREW_GUNMAN_NEW], INT_TO_ENUM(PED_PROP_POSITION,0), 4, 0)
						doBikeEntryLogic(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
					ENDIF
					
					IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
						MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
						TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
					ENDIF
					
					bCutsceneSetup = TRUE
				ENDIF

				IF Asset.mnBike = BATI2
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklins_Heist_Bike"))
						vehBike[2] = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklins_Heist_Bike"))
						SET_VEHICLE_COLOUR_COMBINATION(vehBike[2], 2)
					ENDIF
				ELSE
					IF DOES_ENTITY_EXIST(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklins_getaway_Bike"))
						vehBike[2] = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_REGISTERED_ENTITY("Franklins_getaway_Bike"))
						SET_VEHICLE_COLOUR_COMBINATION(vehBike[2], 2)
					ENDIF
				ENDIF
				
					
				IF GET_CUTSCENE_TIME() > 1402				
				AND bMusicEvent7 = FALSE
					IF bIsStealthPath
						IF PREPARE_MUSIC_EVENT("JH2A_EXIT_SHOP_MA")
							TRIGGER_MUSIC_EVENT("JH2A_EXIT_SHOP_MA")
							bMusicEvent7 = TRUE
						ENDIF
					ELSE
						IF PREPARE_MUSIC_EVENT("JH2B_EXIT_SHOP_MA")
							TRIGGER_MUSIC_EVENT("JH2B_EXIT_SHOP_MA")
							bMusicEvent7 = TRUE
						ENDIF
					ENDIF
					
				ENDIF
				
				IF bIsAlarmRinging = FALSE
				AND GET_CUTSCENE_TIME() > 5402
					IF PREPARE_ALARM("JEWEL_STORE_HEIST_ALARMS")
						START_ALARM("JEWEL_STORE_HEIST_ALARMS", FALSE)
						bIsAlarmRinging = TRUE
					ENDIF
				ENDIF
				
				
				IF iStageSection=0
					IF GET_CUTSCENE_TIME() > 10866//15244
						//ANIMPOSTFX_PLAY("SwitchSceneNeutral", 1000, FALSE)
						iStageSection=1
					ENDIF
				ENDIF	
				
				IF iStageSection=1
					IF GET_CUTSCENE_TIME() > 12066//15244
						////ANIMPOSTFX_STOP("SwitchSceneNeutral")
						//ANIMPOSTFX_PLAY("SwitchSceneFranklin", 1000, FALSE)
						iSwitchSoundEffect = GET_SOUND_ID()
						PLAY_SOUND_FRONTEND(iSwitchSoundEffect, "all", "SHORT_PLAYER_SWITCH_SOUND_SET") 						iStageSection=2
					ENDIF
				ENDIF					
				
				IF GET_CUTSCENE_TIME() > 13000	
					SET_PED_BAG(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
				ENDIF
			
			
				IF NOT IS_ENTITY_DEAD(vehBike[2])
									
					IF Asset.mnBike = BATI2
									
						//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")//"Franklins_Heist_Bike")
						IF NOT IS_CUTSCENE_PLAYING()	
							//SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehBike[2], TRUE, FALSE, FALSE)
							
							SET_ENTITY_VELOCITY(vehBike[2], <<0.0, 0.0, 0.0>>)
							
							IF NOT IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
								SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
							ENDIF
							FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_SELECT_PED(SEL_FRANKLIN))
							
//							IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
//								MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
//								TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
//							ENDIF
				
							SET_PED_BAG(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
						
							
							IF NOT ( (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) = CAM_VIEW_MODE_FIRST_PERSON)
									OR ((GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON) AND IS_ALLOWED_INDEPENDENT_CAMERA_MODES()) )
								DO_START_CUTSCENE(FALSE)
								SET_CAM_COORD(destinationCam,<<-631.299072,-241.895905,38.921356>>)
								SET_CAM_ROT(destinationCam,<<-7.602906,0.000000,78.410202>>)
								SET_CAM_FOV(destinationCam,50.000000)
								SET_CAM_ACTIVE(destinationCam, TRUE)
								
								RENDER_SCRIPT_CAMS(TRUE, FALSE)		
							ENDIF
							
							//SET_BIKE_ON_STAND(vehBike[2], 0.0, -3.0)
							//SET_VEHICLE_HANDBRAKE(vehBike[2], TRUE)
							
							
							
							CREATE_BIKES_IF_THEY_DONT_EXIST()
						
							IF NOT IS_ENTITY_DEAD(vehBike[0])
							AND NOT IS_ENTITY_DEAD(pedCrew[CREW_DRIVER_NEW])
								IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
									SET_PED_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
								ENDIF
							ENDIF
							
							IF NOT IS_ENTITY_DEAD(vehBike[1])
							AND NOT IS_ENTITY_DEAD(pedCrew[CREW_GUNMAN_NEW])
								IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
									SET_PED_INTO_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
								ENDIF						
							ENDIF
						
							SET_ENTITY_HEADING(GET_SELECT_PED(SEL_MICHAEL), 205.4952)
							TASK_FOLLOW_NAV_MESH_TO_COORD(GET_SELECT_PED(SEL_MICHAEL), vJewelWalkCoords, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
							FORCE_PED_MOTION_STATE(GET_SELECT_PED(SEL_MICHAEL), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
						
						ENDIF
					ELSE
						//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")//("Franklins_Heist_Bike")
						IF NOT IS_CUTSCENE_PLAYING()
							IF NOT IS_ENTITY_DEAD(vehBike[2])
							AND NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_FRANKLIN))
							
								//SET_VEHICLE_USE_CUTSCENE_WHEEL_COMPRESSION(vehBike[2], TRUE, FALSE, FALSE)				
								
								IF NOT IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
									SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
								ENDIF
								FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_SELECT_PED(SEL_FRANKLIN))
								
//								IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
//									MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
//									TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds, TRUE, TRUE)
//								ENDIF
					
								SET_PED_BAG(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
								
								
								IF NOT ( (GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) = CAM_VIEW_MODE_FIRST_PERSON)
									OR ((GET_CAM_VIEW_MODE_FOR_CONTEXT(GET_CAM_ACTIVE_VIEW_MODE_CONTEXT()) = CAM_VIEW_MODE_FIRST_PERSON) AND IS_ALLOWED_INDEPENDENT_CAMERA_MODES()) )
									DO_START_CUTSCENE(FALSE)
									SET_CAM_COORD(destinationCam,<<-631.299072,-241.895905,38.921356>>)
									SET_CAM_ROT(destinationCam,<<-7.602906,0.000000,78.410202>>)
									SET_CAM_FOV(destinationCam,50.000000)
									SET_CAM_ACTIVE(destinationCam, TRUE)
									
									RENDER_SCRIPT_CAMS(TRUE, FALSE)
								ENDIF						
								
								//SET_BIKE_ON_STAND(vehBike[2], 0.0, -3.0)	
								
								//SET_VEHICLE_HANDBRAKE(vehBike[2], TRUE)
								
								CREATE_BIKES_IF_THEY_DONT_EXIST()
							
								IF NOT IS_ENTITY_DEAD(vehBike[0])
								AND NOT IS_ENTITY_DEAD(pedCrew[CREW_DRIVER_NEW])
									IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
										SET_PED_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
									ENDIF
								ENDIF
								
								IF NOT IS_ENTITY_DEAD(vehBike[1])
								AND NOT IS_ENTITY_DEAD(pedCrew[CREW_GUNMAN_NEW])
									IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
										SET_PED_INTO_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
									ENDIF						
								ENDIF
								
							ENDIF	
						
							SET_ENTITY_HEADING(GET_SELECT_PED(SEL_MICHAEL), 205.4952)
							TASK_FOLLOW_NAV_MESH_TO_COORD(GET_SELECT_PED(SEL_MICHAEL), vJewelWalkCoords, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
							FORCE_PED_MOTION_STATE(GET_SELECT_PED(SEL_MICHAEL), MS_ON_FOOT_WALK, TRUE, FAUS_CUTSCENE_EXIT)
							
						ENDIF
					ENDIF
				ENDIF
				
				//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Traffic_Warden")									
				IF NOT IS_CUTSCENE_PLAYING()
					IF NOT IS_PED_INJURED(pedTrafficWarden)
						//TASK_COWER(pedTrafficWarden, INFINITE_TASK_TIME)
						//FORCE_PED_AI_AND_ANIMATION_UPDATE(pedTrafficWarden)
						TASK_PLAY_ANIM(pedTrafficWarden, "missheist_jewel", "cop_on_floor", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_LOOPING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedTrafficWarden)
					ENDIF
				ENDIF
//				
				//IF CAN_SET_EXIT_STATE_FOR_REGISTERED_ENTITY("Michael")									
					
				//ENDIF
	
				//IF CAN_SET_EXIT_STATE_FOR_CAMERA()
				
					//RESET_GAME_CAMERA()
				//ENDIF
				
				IF NOT IS_CUTSCENE_PLAYING()
//					
//					IF NOT DOES_CAM_EXIST(destinationCam)
//						destinationCam	= CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
//					ENDIF
//					TASK_FOLLOW_NAV_MESH_TO_COORD(GET_SELECT_PED(SEL_MICHAEL), vMichaelShortWalkCoords, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
//					FORCE_PED_MOTION_STATE(GET_SELECT_PED(SEL_MICHAEL), MS_ON_FOOT_WALK, TRUE)
//							
						
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					ADD_ALL_PEDS_TO_DIALOGUE()
										
					//RESET_GAME_CAMERA()
					
					iStageSection = 3
				ENDIF
			BREAK
		ENDSWITCH
				
		IF IS_CUTSCENE_PLAYING()
			HIDE_HUD_AND_RADAR_THIS_FRAME()
		ENDIF
						
//â•â•â•â•â•â•â•â•â•â•¡ CLEANUP â•žâ•â•â•â•â•â•â•â•â•
		IF iStageSection >= 3
			
			DO_SWITCH_EFFECT(SEL_FRANKLIN)
			
			//ANIMPOSTFX_STOP("SwitchSceneNeutral")
			
			IF NOT bIsStealthPath
				SET_PED_CLOTH_PACKAGE_INDEX(PLAYER_PED_ID(),  1 )
			ENDIF
			
				doBikeEntryLogic(pedCrew[CREW_DRIVER_NEW], vehBike[0])
				doBikeEntryLogic(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
			//ENDIF
			
			UNPIN_INTERIOR(interiorJewelStore)
			
			SET_PED_BAG(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
						
//			SET_PED_AS_NO_LONGER_NEEDED(pedTrafficWarden)
			
//			IF NOT IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
//				SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
//			ENDIF
//			
//			SET_ENTITY_COORDS(vehBike[2], vBikeSpawnCoords[2])
//			SET_ENTITY_HEADING(vehBike[2], fBikeSpawnHeading[2])
		
			IF NOT HAS_SOUND_FINISHED(iSwitchSoundEffect)
				STOP_SOUND(iSwitchSoundEffect)
			ENDIF
								
			REPLAY_STOP_EVENT()
										
			bStageSetup = FALSE
			bCameraShotSetup = FALSE
			
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


PROC INIT_SET_PIECE_COP_CAR(SET_PIECE_COP &thisSetpiece, MODEL_NAMES carModel, INT iRec, STRING stRec, VEHICLE_SEAT passSeat = VS_FRONT_RIGHT, FLOAT fSkipTime = 3000.0, FLOAT fPlayBackSpeed = 1.0, BOOL bAddBlips = FALSE, BOOL BUseAI = FALSE, BOOL bBlockTempEvents = FALSE)

	MODEL_NAMES thisPedModel = S_M_Y_COP_01 //Set a default value
	INT iRandomBikeTarget

	IF carModel = POLICE
	OR carModel = POLMAV
		thisPedModel = S_M_Y_COP_01
	ENDIF
	
	
	thisSetpiece.thisCar = CREATE_VEHICLE(carModel, <<0.0, 0.0, 0.0>>)
	SET_VEHICLE_ENGINE_ON(thisSetpiece.thisCar, TRUE, TRUE)
	SET_VEHICLE_SIREN(thisSetpiece.thisCar, TRUE)
	SET_SIREN_WITH_NO_DRIVER(thisSetpiece.thisCar, TRUE)
	SET_ENTITY_HEALTH(thisSetpiece.thiscar, 1000)
		
	thisSetpiece.thisDriver = CREATE_PED_INSIDE_VEHICLE(thisSetpiece.thisCar, PEDTYPE_MISSION, thisPedModel )
	thisSetpiece.thisPassenger = CREATE_PED_INSIDE_VEHICLE(thisSetpiece.thisCar, PEDTYPE_MISSION, thisPedModel, passSeat )
	
	IF bAddBlips
		thisSetpiece.blipDriver    = CREATE_BLIP_FOR_PED(thisSetpiece.thisDriver, TRUE)
		thisSetpiece.blipPassenger = CREATE_BLIP_FOR_PED(thisSetpiece.thisPassenger, TRUE)
	ENDIF
	
	PRINTLN("Starting Set Piece: ", iRec, ":", stRec)
	IF NOT bUSeAi
		START_PLAYBACK_RECORDED_VEHICLE(thisSetpiece.thisCar, iRec, stRec)
	ELSE
		START_PLAYBACK_RECORDED_VEHICLE_WITH_FLAGS(thisSetpiece.thisCar, iRec, stRec, 2)
	ENDIF
	SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(thisSetpiece.thisCar, fSkipTime)
	SET_PLAYBACK_SPEED(thisSetpiece.thisCar, fPlayBackSpeed)
	SET_PED_RELATIONSHIP_GROUP_HASH(thisSetpiece.thisDriver, grpCop)
	SET_PED_RELATIONSHIP_GROUP_HASH(thisSetpiece.thisPassenger, grpCop)
	
	SET_ENTITY_PROOFS(thisSetpiece.thisCar, FALSE, FALSE, FALSE, TRUE, FALSE)
	
	IF carModel = POLMAV
		SET_HELI_BLADES_FULL_SPEED(thisSetpiece.thisCar)
	ENDIF
	
	IF carModel = SHERIFF
	OR carModel = PRANGER
		SET_VEHICLE_CAN_BE_TARGETTED(thisSetpiece.thisCar, TRUE)
		SET_ENTITY_IS_TARGET_PRIORITY(thisSetpiece.thisCar, TRUE)
	ENDIF
	
	IF NOT bBlockTempEvents
		
		
		GIVE_WEAPON_TO_PED(thisSetpiece.thisDriver, WEAPONTYPE_PUMPSHOTGUN, INFINITE_AMMO, TRUE)
		GIVE_WEAPON_TO_PED(thisSetpiece.thisPassenger, WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE)
			
		iRandomBikeTarget = GET_RANDOM_INT_IN_RANGE(0, 3)
		IF bWorstGunDies AND (iRandomBikeTarget = 1)
			iRandomBikeTarget = 0
		ENDIF
		IF NOT IS_ENTITY_DEAD(vehBike[iRandomBikeTarget])
			TASK_DRIVE_BY(thisSetpiece.thisDriver, NULL, vehBike[iRandomBikeTarget], <<0.0, 0.0, 0.0>>, 300.0, 100, TRUE)
		ENDIF
			
		iRandomBikeTarget = GET_RANDOM_INT_IN_RANGE(0, 3)
		IF bWorstGunDies AND (iRandomBikeTarget = 1)
			iRandomBikeTarget = 2
		ENDIF
		IF NOT IS_ENTITY_DEAD(vehBike[iRandomBikeTarget])
			TASK_DRIVE_BY(thisSetpiece.thisPassenger, NULL, vehBike[iRandomBikeTarget], <<0.0, 0.0, 0.0>>, 300.0, 100, TRUE)
		ENDIF
	ELSE
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thisSetpiece.thisDriver, TRUE)
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(thisSetpiece.thisPassenger, TRUE)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		TEXT_LABEL_63 thisDebugName = stRec
		thisDebugName += iRec
		SET_VEHICLE_NAME_DEBUG(thisSetpiece.thisCar, thisDebugName)
		
		thisDebugName += " Drv"
		SET_PED_NAME_DEBUG(thisSetpiece.thisDriver, thisDebugName)
		
		thisDebugName = stRec
		thisDebugName += iRec
		thisDebugName += " Pass"
		SET_PED_NAME_DEBUG(thisSetpiece.thisPassenger, thisDebugName)
	#ENDIF

ENDPROC

VEHICLE_INDEX tempCar1
VEHICLE_INDEX tempCar2
VEHICLE_INDEX tempCar3

BOOL bCar1SirenOn
BOOL bCar2SirenOn
BOOL bCar3SirenOn

//PURPOSE:		Performs the cutscene where the cops turn up and arrest everyone
FUNC BOOL stageFailCutscene()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
		////SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(GET_REPLAY_STAGE_FROM_MISSION_STAGE(STAGE_CUTSCENE_TRUCK_ARRIVE))
		RESET_NEED_LIST()		
		MANAGE_LOADING_NEED_LIST()
		
		SAFE_REQUEST_MODEL_FOR_HEIST_CREW_MEMBER(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW), TRUE, TRUE)
		SAFE_REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL, TRUE, TRUE)
		
		bDrawCashScaleform = FALSE
		
		REMOVE_CUTSCENE()	
		
		WAIT(0)
		
		FOR iCounter = 0 TO NUMBER_OF_EVENT_FLAGS-1
			bEventFlag[iCounter] = FALSE
		ENDFOR
		
		SET_RANDOM_TRAINS(FALSE)
		
		ADD_ALL_PEDS_TO_DIALOGUE()

		SAFE_FADE_IN()
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
		iStageSection = 0
		bCameraShotSetup = FALSE
		bStageSetup = TRUE
	
		

//═════════╡ UPDATE ╞═════════		
	ELSE
		
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)

		SWITCH iStageSection
						
			CASE 0
				SETTIMERA(0)
				REQUEST_CUTSCENE("JH_2_ARREST_FAIL_C")
				
				SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)		
				
				iStageSection++
			BREAK
			
			CASE 1
				
				IF bIsStealthPath
					TRIGGER_MUSIC_EVENT("JH2A_MISSION_FAIL")
				ELSE
					TRIGGER_MUSIC_EVENT("JH2B_MISSION_FAIL")
				ENDIF
			
				IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_MICHAEL))
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", GET_SELECT_PED(SEL_MICHAEL))
				ENDIF
			
				IF NOT IS_ENTITY_DEAD(pedCrew[CREW_DRIVER_NEW])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("driver_selection", pedCrew[CREW_DRIVER_NEW])
				ENDIF
				IF NOT IS_ENTITY_DEAD(pedCrew[CREW_GUNMAN_NEW])
					SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("gunman_selection_1", pedCrew[CREW_GUNMAN_NEW])
				ENDIF
				
				IF bIsStealthPath	
					SET_CUTSCENE_PED_OUTFIT("Franklin", PLAYER_ONE, OUTFIT_P1_EXTERMINATOR)
				ELSE
					SET_CUTSCENE_PED_OUTFIT("Franklin", PLAYER_ONE, OUTFIT_P1_SUIT_1)
				ENDIF
				
			
				IF HAS_CUTSCENE_LOADED()
				AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				
					IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_FRANKLIN))
						REGISTER_ENTITY_FOR_CUTSCENE(GET_SELECT_PED(SEL_FRANKLIN), "Franklin", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(pedCrew[CREW_DRIVER_NEW])
						REGISTER_ENTITY_FOR_CUTSCENE(pedCrew[CREW_DRIVER_NEW], "driver_selection", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					IF NOT IS_ENTITY_DEAD(pedCrew[CREW_GUNMAN_NEW])
						REGISTER_ENTITY_FOR_CUTSCENE(pedCrew[CREW_GUNMAN_NEW], "gunman_selection_1", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
					ENDIF
					
					IF Asset.mnBike = BATI2
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "drivers_bike", CU_DONT_ANIMATE_ENTITY,  Asset.mnBike)
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "gunmans_bike", CU_DONT_ANIMATE_ENTITY,  Asset.mnBike)
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "franklins_getaway_bike", CU_DONT_ANIMATE_ENTITY,  Asset.mnBike)
												
						IF NOT IS_ENTITY_DEAD(vehBike[0])
							REGISTER_ENTITY_FOR_CUTSCENE(vehBike[0], "drivers_bati", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						IF NOT IS_ENTITY_DEAD(vehBike[1])
							REGISTER_ENTITY_FOR_CUTSCENE(vehBike[1], "gunmans_bati", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						IF NOT IS_ENTITY_DEAD(vehBike[2])
							REGISTER_ENTITY_FOR_CUTSCENE(vehBike[2], "franklins_getaway_bati", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF	
					ELSE
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "drivers_bati", CU_DONT_ANIMATE_ENTITY,  Asset.mnBike)
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "gunmans_bati", CU_DONT_ANIMATE_ENTITY,  Asset.mnBike)
						REGISTER_ENTITY_FOR_CUTSCENE(NULL, "franklins_getaway_bati", CU_DONT_ANIMATE_ENTITY,  Asset.mnBike)
					
						IF NOT IS_ENTITY_DEAD(vehBike[0])
							REGISTER_ENTITY_FOR_CUTSCENE(vehBike[0], "drivers_bike", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						IF NOT IS_ENTITY_DEAD(vehBike[1])
							REGISTER_ENTITY_FOR_CUTSCENE(vehBike[1], "gunmans_bike", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF
						IF NOT IS_ENTITY_DEAD(vehBike[2])
							REGISTER_ENTITY_FOR_CUTSCENE(vehBike[2], "franklins_getaway_bike", CU_ANIMATE_EXISTING_SCRIPT_ENTITY)
						ENDIF		
					ENDIF
					
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
							
					START_CUTSCENE()
					iStageSection++	
				ENDIF
			BREAK
			
			CASE 2
				IF IS_CUTSCENE_PLAYING()
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
					SET_PED_BAG(GET_SELECT_PED(SEL_MICHAEL), FALSE)
					
					DELETE_OBJECT(oiStoreDoorTemp)	
					DELETE_PED(pedTrafficWarden)
					
					DELETE_VEHICLE(vehTruck)
					DELETE_PED(pedCrew[CREW_HACKER])
					iStageSection++	
				ENDIF
			BREAK
			
			CASE 3
						
				tempCar1 = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("police_left", POLICE))
				tempCar2 = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("police_right", POLICE))
				tempCar3 = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_ENTITY_INDEX_OF_CUTSCENE_ENTITY("rear_police", POLICE))
			
				IF NOT IS_ENTITY_DEAD(tempCar1)
				AND bCar1SirenOn = FALSE
					SET_SIREN_WITH_NO_DRIVER(tempCar1, TRUE)
					SET_VEHICLE_SIREN(tempCar1, TRUE)
					bCar1SirenOn = TRUE
				ENDIF
				IF NOT IS_ENTITY_DEAD(tempCar2)
				AND bCar2SirenOn = FALSE
					SET_SIREN_WITH_NO_DRIVER(tempCar2, TRUE)
					SET_VEHICLE_SIREN(tempCar2, TRUE)
					bCar2SirenOn = TRUE
				ENDIF
				IF NOT IS_ENTITY_DEAD(tempCar3)
				AND bCar3SirenOn = FALSE
					SET_SIREN_WITH_NO_DRIVER(tempCar3, TRUE)
					SET_VEHICLE_SIREN(tempCar3, TRUE)
					bCar3SirenOn = TRUE
				ENDIF
						
				IF GET_CUTSCENE_TIME() > 13000//10000//12000			
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
					eMissionFail = FAIL_ARRESTED
					MissionFailed()
				ENDIF
				
			BREAK

		ENDSWITCH
				
		//═════════╡ SKIP ╞═════════
				
		//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 4
			
			eMissionFail = FAIL_ARRESTED
			
			REMOVE_WEAPON_ASSET(WEAPONTYPE_PISTOL)
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		
			bStageSetup = FALSE
			bCameraShotSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
		
	
	RETURN FALSE
ENDFUNC



PROC DO_DYNAMIC_TYRE_POP(VEHICLE_INDEX vehTarget, INT &iCount, SC_WHEEL_LIST sWheel, FLOAT fTargetRadius)
	VECTOR vDamageArea
	VECTOR vDamageDirection
	SC_WINDOW_LIST windowMatch
	
	IF IS_VEHICLE_DRIVEABLE(vehTarget)
		IF iCount < 5
			SWITCH sWheel
				CASE SC_WHEEL_CAR_FRONT_LEFT
					vDamageArea = <<-1, 1, 0.5>>
					vDamageDirection = <<-0.5, 0.75, 0.05>>
					windowMatch = SC_WINDOW_FRONT_LEFT
				BREAK
				CASE SC_WHEEL_CAR_FRONT_RIGHT
					vDamageArea = <<1, 1, 0.5>>
					vDamageDirection = <<0.5, 0.75, 0.05>>
					windowMatch = SC_WINDOW_FRONT_RIGHT
				BREAK
				CASE SC_WHEEL_CAR_REAR_LEFT
					vDamageArea = <<-1, -1, 0.5>>
					vDamageDirection = <<-0.5, -0.75, 0.05>>
					windowMatch = SC_WINDOW_REAR_LEFT
				BREAK
				CASE SC_WHEEL_CAR_REAR_RIGHT
					vDamageArea = <<1, -1, 0.5>>
					vDamageDirection = <<0.5, -0.75, 0.05>>
					windowMatch = SC_WINDOW_REAR_RIGHT
				BREAK
			ENDSWITCH
			
			IF HAS_BULLET_IMPACTED_IN_AREA(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTarget, vDamageArea), fTargetRadius, FALSE)
				SWITCH iCount
					CASE 0
						SET_VEHICLE_TYRE_BURST(vehTarget, sWheel)
						iCount++
					BREAK
					CASE 1
						SMASH_VEHICLE_WINDOW(vehTarget, windowMatch)
						iCount++
					BREAK
					CASE 2
						iCount++
					BREAK
					CASE 3
						iCount++
					BREAK
					CASE 4
						SET_VEHICLE_DAMAGE(vehTarget, vDamageDirection, 150.0, 50.0, TRUE)
						iCount++
					BREAK
				ENDSWITCH
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC SETUP_CUSTOM_SWITCH_CAMERA_M_TO_F_SHOOTOUT()
			    
    sCamDetails.bRun = TRUE
       
    //Destroy the spline cam itself to reset
    IF DOES_CAM_EXIST(sCamDetails.camID)
        DESTROY_CAM(sCamDetails.camID)
    ENDIF
    
    //Recreate the spline cam
    sCamDetails.camID = CREATE_CAMERA(CAMTYPE_SPLINE_SMOOTHED, FALSE)
    
    //Add the starting node, I'm using the FINAL_RENDERED_CAM which is whatever is rendering on screen when this is called.
   // ADD_CAM_SPLINE_NODE_USING_CAMERA(sCamDetails.camID, CREATE_CAMERA_WITH_PARAMS(CAMTYPE_SCRIPTED, GET_FINAL_RENDERED_CAM_COORD(), GET_FINAL_RENDERED_CAM_ROT(), GET_FINAL_RENDERED_CAM_FOV(), TRUE), 0)
          
    //Add Spline Nodes to sCamDetails.camID from the closest node until the end of the array splineNodes
    ADD_CAM_SPLINE_NODE(sCamDetails.camID, GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), 500)
	ADD_CAM_SPLINE_NODE(sCamDetails.camID,<<-601.888184,-286.847534,43.794373>>,<<-30.073895,0.000000,161.941696>>, 500)
	ADD_CAM_SPLINE_NODE(sCamDetails.camID, <<-618.096558,-274.037323,42.422047>>,<<-2.788106,-0.000000,38.960205>>, 500)
	ADD_CAM_SPLINE_NODE(sCamDetails.camID, <<-640.644836,-238.961731,40.628960>>,<<-0.736021,-10.222223,53.160614>>, 500)
	ADD_CAM_SPLINE_NODE(sCamDetails.camID, <<-712.745850,-233.482346,39.936329>>,<<-0.407688,8.475986,113.179451>>, 500)
	ADD_CAM_SPLINE_NODE(sCamDetails.camID, <<-780.410950,-265.722717,39.397606>>,<<-0.407688,-4.560558,158.960602>>, 500)
	ADD_CAM_SPLINE_NODE(sCamDetails.camID, <<-767.824036,-322.895233,38.933601>>,<<-0.407688,-4.560558,-125.619209>>, 500)
//	ADD_CAM_SPLINE_NODE(sCamDetails.camID, <<-665.061707,-361.506104,38.127880>>,<<-0.653938,7.313464,-96.517548>>, 500)
	
    //Set the overall spline duration and smoothing style. 
    SET_CAM_SPLINE_DURATION(sCamDetails.camID, 7000)
    SET_CAM_SPLINE_SMOOTHING_STYLE(sCamDetails.camID, CAM_SPLINE_NO_SMOOTH)
    
    sCamDetails.bSplineCreated = TRUE
    
   // sCamDetails.pedTo = sSelectorPeds.pedID[sSelectorPeds.eNewSelectorPed]
    sCamDetails.bRun = TRUE
	
ENDPROC


INT iAmbientDialogueTimer

FUNC INT GET_TRAFFIC_VEHICLE_SEARCH_FLAGS()
	RETURN (VEHICLE_SEARCH_FLAG_ALLOW_VEHICLE_OCCUPANTS_TO_BE_PERFORMING_A_SCRIPTED_TASK |
			VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_RANDOM_VEHICLES |
			VEHICLE_SEARCH_FLAG_RETURN_VEHICLES_CONTAINING_A_DEAD_OR_DYING_PED)
ENDFUNC

PROC UPDATE_AMBIENT_SWEARING()

	VEHICLE_INDEX vehClosest
	PED_INDEX pedCurrentAngryDriver

	IF GET_GAME_TIMER() - iAmbientDialogueTimer > 3000
		IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
			vehClosest = GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 10.0, DUMMY_MODEL_FOR_SCRIPT, GET_TRAFFIC_VEHICLE_SEARCH_FLAGS())

			IF NOT IS_ENTITY_DEAD(vehClosest) //AND vehClosest != vehBike[0]
				BOOL bCrashed = FALSE
				BOOL bNearMiss = FALSE

				IF IS_ENTITY_TOUCHING_ENTITY(vehClosest, vehBike[2])
					bCrashed = TRUE
				ELIF ABSF(GET_ENTITY_SPEED(vehClosest)) > 2.0
					IF ABSF(GET_ENTITY_HEADING(vehClosest) - GET_ENTITY_HEADING(vehBike[2])) > 60.0 
						bNearMiss = TRUE
					ENDIF
				ENDIF

				IF bCrashed OR bNearMiss
					pedCurrentAngryDriver = GET_PED_IN_VEHICLE_SEAT(vehClosest)
					IF NOT IS_PED_INJURED(pedCurrentAngryDriver)                                                                
						IF bCrashed
						    PLAY_PED_AMBIENT_SPEECH(pedCurrentAngryDriver, "GENERIC_CURSE_HIGH", SPEECH_PARAMS_FORCE_NO_REPEAT_FRONTEND)
						ELSE
						    PLAY_PED_AMBIENT_SPEECH(pedCurrentAngryDriver, "GENERIC_INSULT_HIGH", SPEECH_PARAMS_FORCE_NO_REPEAT_FRONTEND)
						ENDIF
						iAmbientDialogueTimer = GET_GAME_TIMER()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC


//Gets the closest section of an array of vectors
FUNC INT GET_CLOSEST_PATH_SECTION(VECTOR point, VECTOR &path[])
      INT iTemp
      INT iClosest
      FLOAT closest_distance = 99999.9
      REPEAT COUNT_OF(path)-1 iTemp
            IF GET_DISTANCE_BETWEEN_COORDS(GET_CLOSEST_POINT_ON_LINE(point, path[iTemp], path[iTemp+1]), point) < closest_distance
                  iClosest = itemp
                  closest_distance = GET_DISTANCE_BETWEEN_COORDS(GET_CLOSEST_POINT_ON_LINE(point, path[iTemp], path[iTemp+1]), point)
            ENDIF
      ENDREPEAT
      RETURN iClosest
ENDFUNC

CONST_INT MAX_ROUTE_NODES 200
//VECTOR route_path[MAX_ROUTE_NODES]

#IF IS_DEBUG_BUILD
//draws a car recording (LOW RES)
PROC DRAW_UBER_ROUTE_DEBUG_PATH(VECTOR &path[], INT red = 0, INT green = 0, INT blue = 255)
      INT iTemp
     
	 SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	  
	  REPEAT COUNT_OF(path) iTemp
            DRAW_DEBUG_SPHERE(path[iTemp], 0.2, red, green, blue, 170)
		//	DRAW_*SPHERE(path[iTemp], 0.2)
            IF iTemp > 0
                  DRAW_DEBUG_LINE(path[iTemp], path[iTemp-1], red, green, blue, 170)
            ENDIF
      ENDREPEAT
ENDPROC

#ENDIF



PROC OVERRIDE_CINEMATIC_CAMERA()

	IF NOT IS_ENTITY_DEAD(vehBike[0])
		CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, vehBike[0], "H_HELPFOCUS", HINTTYPE_DEFAULT, TRUE, FALSE)
	ENDIF
			
ENDPROC


	
FUNC FLOAT CONTROL_PLAYBACK_SPEED(VEHICLE_INDEX vehicleForPlaybackSpeed, FLOAT fOffsetOverride = - 1.0)

	
	VECTOR vTargetVehicleOffset = <<0.0, 0.0, 0.0>>
	VECTOR vTruckFrontBumperOffset = <<0.0, 2.5, 0.0>>
	//Overides... lets see the mast break happen.
	
	FLOAT fRecTime
	
	VECTOR vPlayerPoint = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTruck, vTruckFrontBumperOffset)
	
	vTargetVehicleOffset.y  = -10.0
	
	IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0])
	
		fRecTime = GET_TIME_POSITION_IN_RECORDING(vehBike[0])
	
		IF fRecTime > 135000.0
			fminPlaybackSpeed = 0.675
			fMaxPlaybackSpeed = 1.95
			//vTargetVehicleOffset.y  = -35.0
		ENDIF
		IF fRecTime > 175000.0
			fminPlaybackSpeed = 0.8
			fMaxPlaybackSpeed = 1.75 //2.1
			//vTargetVehicleOffset.y  = -25.5
		ENDIF
		IF fRecTime > 190000.0
			fminPlaybackSpeed = 0.525
			fMaxPlaybackSpeed = 1.75 //2.1
			//vTargetVehicleOffset.y  = -10.0
		//ELIF fRecTime > 205000.0
		ENDIF
		
		//1st
		IF fRecTime > 184714.500
		AND fRecTime < 186961.000
			fminPlaybackSpeed = 0.525
			fMaxPlaybackSpeed = 1.3 //2.1
		ENDIF
		
		//2nd River crossing
		//IF fRecTime > 208559.500
		IF fRecTime > 207559.500
		AND fRecTime < 215057.10
			fminPlaybackSpeed = 0.525
			fMaxPlaybackSpeed = 1.15 //2.1
		ENDIF
		
		//Right at the end
		IF fRecTime > 231790.100
			fminPlaybackSpeed = 0.5
			fMaxPlaybackSpeed = 1.0
		ENDIF
		
	ENDIF	

	IF fOffsetOverride <> -1.0
		vTargetVehicleOffset.y = fOffsetOverride
	ENDIF
	
	SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)	
		
	IF NOT IS_ENTITY_DEAD(vehicleForPlaybackSpeed)
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehicleForPlaybackSpeed)
			
			//IF player is in front...
			//IF GET_DISTANCE_BETWEEN_COORDS(PLAYER_PED_ID(), vEndOfStormDrain) < GET_DISTANCE_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicleForPlaybackSpeed, vTargetVehicleOffset), vEndOfStormDrain) //10
			
			//ENDIF
			#IF IS_DEBUG_BUILD
				IF bIsSuperDebugEnabled
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
					DRAW_DEBUG_SPHERE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicleForPlaybackSpeed, vTargetVehicleOffset), 0.5, 0, 255, 0, 127)
				
					DRAW_DEBUG_SPHERE(vPlayerPoint, 1.0, 255, 0, 0)
				ENDIF
			#ENDIF
			
			
			
			//IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehicleForPlaybackSpeed) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicleForPlaybackSpeed, vTargetVehicleOffset))
			IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPoint, vEndOfStormDrain) < GET_DISTANCE_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicleForPlaybackSpeed, vTargetVehicleOffset), vEndOfStormDrain) //10
			AND fRecTime > 170211.400
			//OR GET_DISTANCE_BETWEEN_COORDS(vPlayerPoint, vEndOfStormDrain) < GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicleForPlaybackSpeed, <<0.0, 6.0, 0.0>>))
			
			//OR GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(vPlayerPoint, Asset.recNumber, Asset.recUber) > GET_CLOSEST_TIME_POSITION_IN_RECORDING_TO_POINT(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicleForPlaybackSpeed, <<0.0, 5.0, 0.0>>), Asset.recNumber, Asset.recUber)
				fDesiredPlaybackSpeed = fMaxPlaybackSpeed
				fSpeedMultiplier = 0.35 //1.09
				//SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 12.5)
				//SET_VEHICLE_CHEAT_POWER_INCREASE(vehTruck, 0.9)
				#IF IS_DEBUG_BUILD
					IF bIsSuperDebugEnabled
						DRAW_DEBUG_SPHERE(vPlayerPoint, 2.0, 255, 100, 255)
					ENDIF
				#ENDIF
			ELSE
				fDesiredPlaybackSpeed = ( fPLaybackSpeedNumerator / GET_DISTANCE_BETWEEN_COORDS(vPlayerPoint, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicleForPlaybackSpeed, vTargetVehicleOffset)))
				fSpeedMultiplier = 0.225 //1.007
				
				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
				SET_VEHICLE_CHEAT_POWER_INCREASE(vehTruck, 1.8)
				#IF IS_DEBUG_BUILD
					IF bIsSuperDebugEnabled
						DRAW_DEBUG_SPHERE(vPlayerPoint, 2.2, 0, 255, 255)
					ENDIF
				#ENDIF
			ENDIF
			
			IF fPLaybackSpeedCPS < fDesiredPlaybackSpeed
				fPLaybackSpeedCPS = fPLaybackSpeedCPS +@ fSpeedMultiplier //) //1.007)
			ELSE
				fPLaybackSpeedCPS = fPLaybackSpeedCPS -@ 0.75 // 0.995)
			ENDIF
			
			IF fPLaybackSpeedCPS < fminPlaybackSpeed 
				fPLaybackSpeedCPS = fminPlaybackSpeed
			ENDIF
			
//			IF fPLaybackSpeedCPS > fMaxPlaybackSpeed 
//				fPLaybackSpeedCPS = fMaxPlaybackSpeed 
//				SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 9.0)
//				SET_VEHICLE_CHEAT_POWER_INCREASE(vehTruck, 1.0)
//				#IF IS_DEBUG_BUILD
//					IF bIsSuperDebugEnabled
//						DRAW_DEBUG_SPHERE(vPlayerPoint, 2.0, 255, 255, 255)
//					ENDIF
//				#ENDIF
//			
//			ENDIF
		
			//if you've somehow managed to be a dick
			IF GET_ENTITY_MODEL(vehicleForPlaybackSpeed) = Asset.mnBike
				IF GET_DISTANCE_BETWEEN_COORDS(vPlayerPoint, vEndOfStormDrain) < GET_DISTANCE_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehicleForPlaybackSpeed, <<0.0, -1.75, 0.0>>), vEndOfStormDrain) //10//10
				AND NOT bEndOfLARiverReached
					fPLaybackSpeedCPS = fMaxPlaybackSpeed 
					SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 2.5)
					//SET_VEHICLE_CHEAT_POWER_INCREASE(vehTruck, 0.0)
				ENDIF
			ENDIF
		
		ENDIF	
	ENDIF
	
		
	//Slow down zones... Overrides
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<693.031311,-520.213928,20.344049>>, <<629.473816,-657.351013,11.148476>>, 46.000000)
		SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 2.55)
		//SET_VEHICLE_CHEAT_POWER_INCREASE(vehTruck, 1.0)
		//PRINTSTRING("Air drag!!")
	ENDIF

	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<560.620667,-1202.869507,41.158386>>, <<558.317505,-749.742798,8.305830>>, 46.750000)
		SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 2.55)		
		//SET_VEHICLE_CHEAT_POWER_INCREASE(vehTruck, 1.0)
		PRINTSTRING("Air drag!!")
	ENDIF
	
	//section on left under rail bridge
	IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<705.617493,-1545.082886,6.983787>>, <<632.251221,-1315.324585,18.492258>>, 46.000000)
		SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 2.55)		
		//SET_VEHICLE_CHEAT_POWER_INCREASE(vehTruck, 1.0)
		//PRINTSTRING("Air drag!!")
	ENDIF
	
	IF GET_DISTANCE_BETWEEN_ENTITIES(vehTruck, vehBike[0]) < 10.0
	OR GET_DISTANCE_BETWEEN_ENTITIES(vehTruck, vehBike[2]) < 10.0
		PRINTLN("too close")
		SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 14.5)		
		SET_VEHICLE_CHEAT_POWER_INCREASE(vehTruck, 0.7)
	ENDIF
	
	IF NOT bWorstGunDies
		IF GET_DISTANCE_BETWEEN_ENTITIES(vehTruck, vehBike[1]) < 10.0
			PRINTLN("too close")
			SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 14.5)		
			SET_VEHICLE_CHEAT_POWER_INCREASE(vehTruck, 0.7)
		ENDIF
	ENDIF
	
	//At the end dont speed cheat and slow the player down a touch.
	IF iBlipCount = 0
	AND bEndOfLARiverReached
		//SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 3.5)		
		SET_VEHICLE_CHEAT_POWER_INCREASE(vehTruck, 1.0)
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF bIsSuperDebugEnabled
			PRINTLN("Speed:", fPLaybackSpeedCPS, ",", fDesiredPlaybackSpeed, ",", fDesiredPlaybackSpeed, ",", fSpeedMultiplier, "::", fminPlaybackSpeed, "->", fmaxPlaybackSpeed)
		ENDIF
	#ENDIF
	
	//CALCULATE_NEW_PLAYBACK_SPEED_FROM_CHAR(vehicleForPlaybackSpeed, PLAYER_PED_ID(),mainPlaybackSpeed, fminPlaybackSpeed, ABSF(vTargetVehicleOffset.y), ABSF(2.0 * vTargetVehicleOffset.y))  
			

	RETURN fPLaybackSpeedCPS
	
ENDFUNC

PROC SETUP_WANTED_LEVEL()

	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_SWAT_HELICOPTER, TRUE)
    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_HELICOPTER, TRUE)
    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_SWAT_AUTOMOBILE, TRUE)
    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_VEHICLE_REQUEST, TRUE)
   	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_ROAD_BLOCK, TRUE)
    BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, TRUE)
   	BLOCK_DISPATCH_SERVICE_RESOURCE_CREATION(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, TRUE)
	
	enable_dispatch_service(dt_police_automobile, FALSE)
	enable_dispatch_service(DT_SWAT_HELICOPTER, FALSE)
	enable_dispatch_service(DT_POLICE_HELICOPTER, FALSE)
	enable_dispatch_service(DT_SWAT_AUTOMOBILE, FALSE)
	enable_dispatch_service(DT_POLICE_VEHICLE_REQUEST, FALSE)
	enable_dispatch_service(DT_POLICE_ROAD_BLOCK, FALSE)
	enable_dispatch_service(DT_POLICE_AUTOMOBILE_WAIT_PULLED_OVER, FALSE)
	enable_dispatch_service(DT_POLICE_AUTOMOBILE_WAIT_CRUISING, FALSE)

	SET_MAX_WANTED_LEVEL(5)
	SET_PLAYER_WANTED_LEVEL_NO_DROP(PLAYER_ID(), 4)	
	SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
	//SET_FAKE_WANTED_LEVEL(3)

ENDPROC

PROC MANAGE_BIKE_BLIPS()

	IF (NOT IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2]))			// +++ Blip management +++
		IF NOT DOES_BLIP_EXIST(blipBike)
			blipBike = CREATE_BLIP_FOR_VEHICLE(vehBike[2])
			//runGodText("G_GETBACKBIKE", FALSE, TRUE)
			runGodText("G_BIKE", FALSE, TRUE)
		ENDIF
		REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipCrew[0])
		REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipCrew[1])
	ELSE
		REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipBike)
		IF NOT DOES_BLIP_EXIST(blipCrew[0])
			IF DOES_ENTITY_EXIST(vehBike[0])
				blipCrew[0] = CREATE_BLIP_FOR_VEHICLE(vehBike[0])
			ENDIF
		ENDIF
		IF NOT DOES_BLIP_EXIST(blipCrew[1])
			IF DOES_ENTITY_EXIST(vehBike[1])
				blipCrew[1] = CREATE_BLIP_FOR_VEHICLE(vehBike[1])
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(vehBike[1])
		IF NOT IS_ENTITY_DEAD(vehBike[1])
			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1])
				REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipCrew[1])
			ENDIF
		ENDIF
	ELSE
		REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipCrew[1])
	ENDIF		
	
	#IF IS_DEBUG_BUILD
	IF IS_BUTTON_PRESSED(PAD1, DPADDOWN)											// +++ Debug vehicle reset +++
		IF DOES_ENTITY_EXIST(vehBike[1])
		AND IS_VEHICLE_DRIVEABLE(vehBike[1])
			IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBike[2])
				SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehBike[2])
				SET_ENTITY_COORDS(vehBike[2], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBike[1], <<0,-5,0>>))
				SET_ENTITY_HEADING(vehBike[2], GET_ENTITY_HEADING(vehBike[1]))
				SET_VEHICLE_FIXED(vehBike[2])
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(vehBike[0])
			AND IS_VEHICLE_DRIVEABLE(vehBike[0])
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBike[2])
					SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehBike[2])
					SET_ENTITY_COORDS(vehBike[2], GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBike[0], <<0,-5,0>>))
					SET_ENTITY_HEADING(vehBike[2], GET_ENTITY_HEADING(vehBike[0]))
					SET_VEHICLE_FIXED(vehBike[2])
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
				ENDIF
			ENDIF
		ENDIF
	ENDIF																				// --- Debug vehicle reset ---
	#ENDIF
				
ENDPROC

INT iTimeOfLastCopCarTakeOutComment
BOOL bInitialGetCopsLine = FALSE

PROC CONTROL_LATE_LEAVE_COPS()

	IF g_bLateLeaveJewelStore
//		IF TIMERB() > 10200
//			IF NOT bHasClearedLateCops
//				FOR iCounter = 0 TO 2
//					IF IS_VEHICLE_DRIVEABLE(vehPolice[iCounter])
//					AND NOT IS_PED_INJURED(pedPolice[iCounter])
////						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice[iCounter])
////							STOP_PLAYBACK_RECORDED_VEHICLE(vehPolice[iCounter])
////						ENDIF
//						SET_VEHICLE_AS_NO_LONGER_NEEDED(vehPolice[iCounter])
//						TASK_COMBAT_PED(pedPolice[iCounter], PLAYER_PED_ID())
//						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPolice[iCounter], FALSE)
//						SET_PED_AS_NO_LONGER_NEEDED(pedPolice[iCounter])
//					ENDIF
//				ENDFOR
//				bHasClearedLateCops = TRUE
//			ENDIF
//		ENDIF
	IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0])
		IF  GET_TIME_POSITION_IN_RECORDING(vehBike[0]) > 5753.612  //TIMERB() > 10200
			IF NOT bLateCopsShooting
				FOR iCounter = 0 TO 2
					IF IS_VEHICLE_DRIVEABLE(vehPolice[iCounter])
						IF NOT IS_PED_INJURED(pedPolice[iCounter])
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice[iCounter])
								STOP_PLAYBACK_RECORDED_VEHICLE(vehPolice[iCounter])
								BRING_VEHICLE_TO_HALT(vehPolice[iCounter], 3.0, -1)
							ELSE
								IF IS_VEHICLE_STOPPED(vehPolice[iCounter])
									SET_PED_ACCURACY(pedPolice[iCounter], 0)
									GIVE_WEAPON_TO_PED(pedPolice[iCounter], WEAPONTYPE_PISTOL, 1000, TRUE)
									SET_PED_RELATIONSHIP_GROUP_HASH(pedPolice[iCounter], grpCop)
									SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPolice[iCounter], FALSE)
									OPEN_SEQUENCE_TASK(seqSequence)
										TASK_PAUSE(NULL, GET_RANDOM_INT_IN_RANGE(0, 1000))
										TASK_LEAVE_ANY_VEHICLE(NULL)
										TASK_COMBAT_HATED_TARGETS_AROUND_PED(NULL, 200.0)
									CLOSE_SEQUENCE_TASK(seqSequence)
									TASK_PERFORM_SEQUENCE(pedPolice[iCounter], seqSequence)
									CLEAR_SEQUENCE_TASK(seqSequence)
									bLateCopsShooting = TRUE
								ELSE
									BRING_VEHICLE_TO_HALT(vehPolice[iCounter], 3.0, -1)
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDFOR
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(setpieceCarid[33])
			IF NOT IS_ENTITY_DEAD(setpieceCarid[33])
				
				SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(setpieceCarid[33], TRUE)
				
//						DRAW_DEBUG_TEXT_ABOVE_ENTITY(setpieceCarid[33], CONVERT_INT_TO_STRING(GET_ENTITY_HEALTH(setpieceCarid[33])), 1.0)
//						PRINTLN("what the fuck!: ",  GET_ENTITY_HEALTH(setpieceCarid[33]))
				IF GET_ENTITY_HEALTH(setpieceCarid[33]) < 850
					PED_INDEX driverToKill = GET_PED_IN_VEHICLE_SEAT(setpieceCarid[33])
					IF NOT IS_PED_INJURED(driverToKill)
					AND GET_ENTITY_HEALTH(driverToKill) > 0
						SET_ENTITY_HEALTH(driverToKill, 0)
						SET_PED_AS_NO_LONGER_NEEDED(driverToKill)
						
						IF IS_GUNMAN_GUSTAVO()
							RunConversation("JH_GET_COPRE", TRUE, TRUE, TRUE)
						ELIF IS_GUNMAN_PACKIE()
							RunConversation("JH_THNX_PM", TRUE, TRUE, TRUE)
						ELIF IS_GUNMAN_NORM()
							RunConversation("JH_THNX_NR", TRUE, TRUE, TRUE)						
						ENDIF
						
						IF IS_DRIVER_EDDIE()
							addTextToQueue("JH_GOOD_ET", TEXT_TYPE_CONVO)								
						ELIF IS_DRIVER_KARIM()
							addTextToQueue("JH_GOOD_KD", TEXT_TYPE_CONVO)
						ENDIF						
					ENDIF
					STOP_PLAYBACK_RECORDED_VEHICLE(setpieceCarid[33])
				ELSE
//					PED_INDEX driverToKill = GET_PED_IN_VEHICLE_SEAT(setpieceCarid[33])
//					IF NOT IS_PED_INJURED(driverToKill)
						IF GET_GAME_TIMER() - iTimeOfLastCopCarTakeOutComment > 4500
						AND GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), vehBike[0]) < 100.0
						
							IF bInitialGetCopsLine = FALSE
								IF IS_GUNMAN_PACKIE()								
									addTextToQueue("JH_COPOFF_PM", TEXT_TYPE_CONVO)
								ELIF IS_GUNMAN_GUSTAVO()
									addTextToQueue("JH_COPOFF_GM", TEXT_TYPE_CONVO)
								ELIF IS_GUNMAN_NORM()
									addTextToQueue("JH_COPOFF_NR", TEXT_TYPE_CONVO)								
								ENDIF
								bInitialGetCopsLine = TRUE
							ELSE
														
								IF IS_GUNMAN_PACKIE()
									addTextToQueue("JH_GET_COPCP", TEXT_TYPE_CONVO)
								ELIF IS_GUNMAN_GUSTAVO()
									addTextToQueue("JH_GET_COPCR", TEXT_TYPE_CONVO)
								ELIF IS_GUNMAN_NORM()
									addTextToQueue("JH_GET_COPCN", TEXT_TYPE_CONVO)
								ENDIF
							ENDIF					
							iTimeOfLastCopCarTakeOutComment = GET_GAME_TIMER()
						ENDIF
//					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		ENDIF
	ENDIF

ENDPROC

INTERIOR_INSTANCE_INDEX tunnelInterior

INT iHandleTunnelStage
VECTOR vInteriorCoords = <<-51.2869, -553.8421, 29.2955>>

PROC HANDLE_TUNNEL_INTERIOR()

	SWITCH iHandleTunnelStage
	
		CASE 0
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInteriorCoords, <<200.0, 200.0, 200.0>>)
				tunnelInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(vInteriorCoords, "V_31_TUN_01")
				iHandleTunnelStage++
			ENDIF
		BREAK
	
		CASE 1
		
			PIN_INTERIOR_IN_MEMORY(tunnelInterior)
			iHandleTunnelStage++
		BREAK
		
		CASE 2
			IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vInteriorCoords, <<9.0, 9.0, 9.0>>)
				UNPIN_INTERIOR(tunnelInterior)
				iHandleTunnelStage++
			ENDIF
		BREAK
		
	ENDSWITCH

ENDPROC

FLOAT fDeltaTime
FLOAT fMult
FLOAT fRecTime
BOOL bSwitchedRecsToAi
BOOL bInterioLoaded= FALSE

FLOAT fDesiredSpeed
FLOAT fAccel = 0.01

BOOL bBikeZeroStopped = FALSE
//BOOL bBikeOneStopped  = FALSE
//BOOL bBikeTwoStopped  = FALSE

FLOAT fRejoinSpeed = 0.0

FLOAT fTunnelEndSkiptime

INT iTimeOfLeaving
//INT iTunnelPinStage

BOOL bGuyLosesFranklin

//PURPOSE:		Performs the section where the crew escape the heist on bikes
FUNC BOOL stageBikeChase()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
		//SCRIPT_ASSERT("up here")		
		
		DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, TRUE)
		DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, TRUE)
		
		SET_FOLLOW_VEHICLE_CAM_HIGH_ANGLE_MODE_EVERY_UPDATE(TRUE, TRUE)
		
		DISABLE_INTERIOR( GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-164.3822, -619.0884, 33.3318>>, "dt1_02_carpark"), TRUE)
		DISABLE_INTERIOR( GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-179.3140, -180.2154, 42.6246>>, "bt1_04_carpark"), TRUE)
		DISABLE_INTERIOR( GET_INTERIOR_AT_COORDS_WITH_TYPE(<<-16.2958, -684.0385, 33.5083>>, "dt1_03_carpark"), TRUE)
		DISABLE_INTERIOR( GET_INTERIOR_AT_COORDS_WITH_TYPE(<<141.2044, -717.2167, 32.1327>>, "dt1_05_carpark"), TRUE)
		SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", TRUE)
		iHandleTunnelStage = -1
		bInitialGetCopsLine = FALSE
		
		IF eMissionStage = STAGE_BIKE_CHASE
			
			//iTunnelPinStage = 0
			
			RESET_GAME_CAMERA()
					
			IF GET_CAM_VIEW_MODE_FOR_CONTEXT(CAM_VIEW_MODE_CONTEXT_ON_BIKE) != CAM_VIEW_MODE_FIRST_PERSON
				DO_END_CUTSCENE(FALSE, TRUE, 1500, FALSE)
			ENDIF
			
			iHandleTunnelStage = 0
			
			IF bIsStealthPath
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "STAGE_BIKE_CHASE")
			ELSE
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(3, "STAGE_BIKE_CHASE")
			ENDIF		
			
			g_replay.iReplayInt[CURRENT_TAKE_REPLAY_VALUE] = iCurrentTake
			
			IF NOT bIsReplayGoingOn
			AND IS_SCREEN_FADED_OUT()
				LOAD_SCENE(<<-665.6722, -231.6896, 36.1003>>)
			ENDIF
			//SAFE_REQUEST_INTERIOR_AT_COORDS_WITH_TYPE(tunnelInterior, << -57.1670, -547.4032, 30.3138 >>, "V_31_TUN_01",FALSE, FALSE )
			
		ELIF eMissionStage = STAGE_BIKE_CHASE_START_OF_TUNNEL
			PRINTLN("Trying to load some interior shit")	
			//SAFE_REQUEST_INTERIOR_AT_COORDS_WITH_TYPE(tunnelInterior, << -57.1670, -547.4032, 30.3138 >>, "V_31_TUN_01", TRUE, TRUE )
						
			SET_ENTITY_COORDS(PLAYER_PED_ID(),  <<-63.0551, -541.7521, 30.8795>>)
			
			IF NOT bIsReplayGoingOn
				NEW_LOAD_SCENE_START_SPHERE(<<-63.0551, -541.7521, 30.8795>>, 15.0)
				WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
					WAIT(0)
				ENDWHILE
				NEW_LOAD_SCENE_STOP()
			ENDIF	
			
			IF bIsStealthPath
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "STAGE_BIKE_CHASE_START_OF_TUNNEL")
			ELSE
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "STAGE_BIKE_CHASE_START_OF_TUNNEL")
			ENDIF
					
			PRINTLN("Succeeded to load some interior shit")	
		ELIF eMissionStage = STAGE_BIKE_CHASE_END_OF_TUNNEL
			SAFE_REQUEST_INTERIOR_AT_COORDS_WITH_TYPE(tunnelInterior, << 1021.4163, -201.1791, 41.7879 >>, "V_31_NEWTUN4", TRUE, TRUE )
			
			IF bIsStealthPath
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6, "STAGE_BIKE_CHASE_END_OF_TUNNEL")
			ELSE
				SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "STAGE_BIKE_CHASE_END_OF_TUNNEL")
			ENDIF
			IF NOT bIsReplayGoingOn
				SET_ENTITY_COORDS(PLAYER_PED_ID(),  << 1021.4163, -201.1791, 41.7879 >>)
				NEW_LOAD_SCENE_START_SPHERE(<< 1021.4163, -201.1791, 41.7879 >>, 25.0)
				WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
					WAIT(0)
				ENDWHILE
				NEW_LOAD_SCENE_STOP()
			ENDIF
		ENDIF
	
		SET_OBJECT_AS_NO_LONGER_NEEDED(oiStoreDoorTemp)
	
		SET_VEHICLE_POPULATION_BUDGET(0)
		SET_PED_POPULATION_BUDGET(1)
		
		//SET_PED_DENSITY_MULTIPLIER(1)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(Asset.mnBike, TRUE)
		SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
		
		//Disabled peds around jump area
		//ADD_SCENARIO_BLOCKING_AREA(<< -186.7415, -506.2248, 30.7621 >>, << -166.2254, -476.2848, 39.7972 >>)
		SET_PED_NON_CREATION_AREA(<< -186.7415, -506.2248, 30.7621 >>, << -166.2254, -476.2848, 39.7972 >>)
		DISABLE_NAVMESH_IN_AREA(<< -186.7415, -506.2248, 30.7621 >>, << -166.2254, -476.2848, 39.7972 >>, TRUE)
				
		RESET_NEED_LIST()
		SET_MODEL_AS_NEEDED(Asset.mnBike)		
		SET_MODEL_AS_NEEDED(IG_TRAFFICWARDEN)
		SET_ANIM_DICT_AS_NEEDED(ASset.adJewelHeist)
		
		SET_VEHICLE_RECORDING_AS_NEEDED(Asset.recUber, Asset.recNumber)
		REQUEST_VEHICLE_RECORDING(722, Asset.recUber) //Franklins bike jump
		SET_VEHICLE_RECORDING_AS_NEEDED(Asset.recUber, Asset.recNoGoodBikeChase)
				
		IF g_bLateLeaveJewelStore
			SET_MODEL_AS_NEEDED(Asset.mnPolice)
			SET_MODEL_AS_NEEDED(Asset.mnPoliceman)
			SET_VEHICLE_RECORDING_AS_NEEDED("JHLateCops", 001)
			SET_VEHICLE_RECORDING_AS_NEEDED("JHLateCops", 002)
			SET_VEHICLE_RECORDING_AS_NEEDED("JHLateCops", 003)
		ENDIF
		//SCRIPT_ASSERT("about to load some shit")			
		
		REQUEST_VEHICLE_ASSET(ASset.mnBike)
		REQUEST_PTFX_ASSET()
		
		WHILE NOT MANAGE_LOADING_NEED_LIST_SPEEDY()
		OR NOT HAS_VEHICLE_ASSET_LOADED(ASset.mnBike)
		OR NOT HAS_PTFX_ASSET_LOADED()
			PRINTLN("Loading assets")
			safeWait(0)
		ENDWHILE
				
		//SCRIPT_ASSERT("middle here")
		
		IF eMissionStage = STAGE_BIKE_CHASE_START_OF_TUNNEL
			CREATE_BIKES_IF_THEY_DONT_EXIST(1)
		ELIF eMissionStage = STAGE_BIKE_CHASE_END_OF_TUNNEL
			CREATE_BIKES_IF_THEY_DONT_EXIST(2)
		ELSE
			CREATE_BIKES_IF_THEY_DONT_EXIST(0)
		ENDIF
		
		//safeWait(0)
		
		SET_PED_AMMO(GET_SELECT_PED(SEL_FRANKLIN), WEAPONTYPE_BZGAS, 0)
		REMOVE_WEAPON_FROM_PED(GET_SELECT_PED(SEL_FRANKLIN), WEAPONTYPE_BZGAS)
		
		IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
		AND NOT IS_ENTITY_DEAD(vehBike[0])
		
			IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
				SET_PED_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
			ENDIF
		
			SET_ENTITY_VISIBLE(pedCrew[CREW_DRIVER_NEW], TRUE)
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedCrew[CREW_DRIVER_NEW], TRUE)
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedCrew[CREW_DRIVER_NEW], KNOCKOFFVEHICLE_NEVER)
			IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
				SET_PED_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
			ENDIF
			SET_PED_PROP_INDEX(pedCrew[CREW_DRIVER_NEW], INT_TO_ENUM(PED_PROP_POSITION,0), 3, 0)
			
			SET_PED_LOD_MULTIPLIER(pedCrew[CREW_DRIVER_NEW], 2.0)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCrew[CREW_DRIVER_NEW], TRUE)
			
			ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_MAD_DRIVER, pedCrew[CREW_DRIVER_NEW])
			
		ENDIF
		IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
		AND NOT IS_ENTITY_DEAD(vehBike[1])
		
			IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
				SET_PED_INTO_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
			ENDIF
		
			SET_ENTITY_VISIBLE(pedCrew[CREW_GUNMAN_NEW], TRUE)
			IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
				SET_PED_INTO_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
			ENDIF
			SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedCrew[CREW_GUNMAN_NEW], TRUE)
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedCrew[CREW_GUNMAN_NEW], KNOCKOFFVEHICLE_NEVER)
			SET_PED_PROP_INDEX(pedCrew[CREW_GUNMAN_NEW], INT_TO_ENUM(PED_PROP_POSITION,0), 4, 0)
			SET_PED_BAG(pedCrew[CREW_GUNMAN_NEW], TRUE)
			//SET_VEHICLE_ON_GROUND_PROPERLY(vehBike[1])
			SET_PED_LOD_MULTIPLIER(pedCrew[CREW_GUNMAN_NEW], 2.0)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCrew[CREW_GUNMAN_NEW], TRUE)
			
			ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_MAD_DRIVER, pedCrew[CREW_GUNMAN_NEW])
			
		ENDIF
		IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
		AND NOT IS_ENTITY_DEAD(vehBike[2])
		
			SET_ENTITY_VISIBLE(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
			IF NOT IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
				SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
			ENDIF
			REMOVE_PED_HELMET(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
			SET_PED_HELMET_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), 6)
			SET_PED_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_PROP_POSITION,0), 6, 8)
			SET_PED_HELMET(GET_SELECT_PED(SEL_FRANKLIN), TRUE)	
			SET_PED_LOD_MULTIPLIER(GET_SELECT_PED(SEL_FRANKLIN), 2.0)
			
			ADD_SHOCKING_EVENT_FOR_ENTITY(EVENT_SHOCKING_MAD_DRIVER, GET_SELECT_PED(SEL_FRANKLIN))
		ENDIF
		
		SET_PED_CREW(GET_SELECT_PED(SEL_MICHAEL))
		SET_PED_HEADGEAR(GET_SELECT_PED(SEL_MICHAEL), TRUE)
		
		SET_PED_CREW(GET_SELECT_PED(SEL_FRANKLIN))
		
		HIDE_ENTITY(GET_SELECT_PED(SEL_FRANKLIN), FALSE)
		HIDE_ENTITY(pedCrew[CREW_DRIVER_NEW], FALSE)
		HIDE_ENTITY(pedCrew[CREW_GUNMAN_NEW], FALSE)
		
		REQUEST_WEAPON_ASSET(WEAPONTYPE_PISTOL)
		
		SET_ANIM_DICT_AS_NEEDED(Asset.adJewelHeistSwitchCam)		
		SAFE_REQUEST_ANIM_DICT(Asset.adJewelHeistSwitchCam, FALSE, FALSE)

		//IF bIsStealthPath // NOT bUberAlreadyPlaying
		IF NOT IS_ENTITY_DEAD(vehBike[0])
				
			LOAD_AND_MANAGE_UBER_ARRAY()
			
			CLEAR_AREA_OF_VEHICLES(GET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN)), 500)
						
			FREEZE_ENTITY_POSITION(vehBike[2], FALSE)
			
			#IF IS_DEBUG_BUILD
				set_uber_parent_widget_group(widgetDebug)
			#ENDIF
			
			INITIALISE_UBER_PLAYBACK(Asset.recUber, Asset.recNumber, TRUE)
			allow_veh_to_stop_on_PLAYER_impact = TRUE // url:bugstar:2096432
			iDontSwitchThisSetpieceRecordingToAI = 998	//	url:bugstar:2096432
			
			LOAD_AND_MANAGE_UBER_ARRAY()
			
			FREEZE_ENTITY_POSITION(vehBike[0], FALSE)
			START_PLAYBACK_RECORDED_VEHICLE(vehBike[0], Asset.recNumber, Asset.recUber)
		
					
			SetPieceCarID[0] = vehBike[1]
			SetPieceCarProgress[0] = 3

			IF eMissionStage = STAGE_BIKE_CHASE
				
				g_replay.iReplayInt[GOT_BAG_REPLAY_VALUE] = BAG_NOT_COLLECTED
				
				FREEZE_ENTITY_POSITION(vehBike[1], FALSE)
				START_PLAYBACK_RECORDED_VEHICLE(vehBike[1], SetPieceCarRecording[0], Asset.recUber)
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[1], 1000)
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[0], 1000)
				
				SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehBike[0], vBatiRecOffset)
				SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehBike[1], vBatiRecOffset)
								
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike[0])
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike[1])
			
				START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_jew_biKe_burnout", vehBike[0], <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>, 1.1)
				START_PARTICLE_FX_NON_LOOPED_ON_ENTITY("scr_jew_biKe_burnout", vehBike[1], <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
				
			ELIF eMissionStage = STAGE_BIKE_CHASE_START_OF_TUNNEL
				fChaseSpeed = 1.0
				IF (NOT bWorstGunDies)
					FREEZE_ENTITY_POSITION(vehBike[1], FALSE)
					START_PLAYBACK_RECORDED_VEHICLE(vehBike[1], SetPieceCarRecording[0], Asset.recUber)
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[1], 46810.320)
					SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehBike[1], vBatiRecOffset)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike[1])					
				ENDIF
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[0], 46810.320)
				SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehBike[0], vBatiRecOffset)
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike[0])
//				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(cameraCar, 46010.320)
//				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(cameraCarCamera, 46010.320)
				//CREATE_ALL_WAITING_UBER_CARS()					
			ELIF eMissionStage = STAGE_BIKE_CHASE_END_OF_TUNNEL
				
				fChaseSpeed = 1.0
				
				IF bWorstGunDies
					fTunnelEndSkiptime = 146000
				ELSE
					fTunnelEndSkiptime = 147000
				ENDIF
				
				IF (NOT bWorstGunDies)
					FREEZE_ENTITY_POSITION(vehBike[1], FALSE)
					START_PLAYBACK_RECORDED_VEHICLE(vehBike[1], SetPieceCarRecording[0], Asset.recUber)
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[1], fTunnelEndSkiptime)
					SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehBike[1], vBatiRecOffset)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike[1])
				ENDIF
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[0], fTunnelEndSkiptime)
				SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehBike[0], vBatiRecOffset)
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike[0])
//				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(cameraCar, 153000)
//				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(cameraCarCamera, 153000)
				CREATE_ALL_WAITING_UBER_CARS()					
			ENDIF
			
			
			IF eMissionStage = STAGE_BIKE_CHASE_START_OF_TUNNEL
				IF NOT IS_ENTITY_DEAD(vehBike[2])
					SET_ENTITY_COORDS(vehBike[2],<<-63.0551, -541.7521, 30.8795>>)
					
					SET_ENTITY_ROTATION(vehBike[2],<<0.0, 0.0, 0.0>>)	//make it up right
					SET_ENTITY_HEADING(vehBike[2], 219.6241)			//then set heading
				ENDIF	
				//LOAD_SCENE(<< -56.5724, -547.1007, 30.3222 >>)
				SET_VEHICLE_FORWARD_SPEED(vehBike[2], 16.0)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			ELIF eMissionStage = STAGE_BIKE_CHASE_END_OF_TUNNEL
				IF NOT IS_ENTITY_DEAD(vehBike[2])
					SET_ENTITY_COORDS(vehBike[2], <<979.6, -139.4, 34.2>>)
					SET_ENTITY_HEADING(vehBike[2], -130.0)
				ENDIF	
				//LOAD_SCENE(<< 1021.4163, -201.1791, 41.7879 >>)
				SET_VEHICLE_FORWARD_SPEED(vehBike[2], 20.0)
				SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			ENDIF
		
			IF eMissionStage = STAGE_BIKE_CHASE_END_OF_TUNNEL
				REQUEST_TRUCK_FIGHT_ASSETS()
			ENDIF
			
			//bTrafficOnlySwitchToAIOnCollision = TRUE
			
			IF g_bLateLeaveJewelStore
			AND eMissionStage = STAGE_BIKE_CHASE
				FOR iCounter = 0 TO 2
					IF iCounter <> 1
						vehPolice[iCounter] = CREATE_VEHICLE(Asset.mnPolice, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME((iCounter + 1), 0,	"JHLateCops"))
						pedPolice[iCounter] = CREATE_PED_INSIDE_VEHICLE(vehPolice[iCounter], PEDTYPE_COP, Asset.mnPoliceman)
						SET_PED_COP(pedPolice[iCounter])
						SET_PED_COMBAT_ATTRIBUTES(pedPolice[iCounter], CA_CAN_BUST, TRUE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedPolice[iCounter], TRUE)
						START_PLAYBACK_RECORDED_VEHICLE(vehPolice[iCounter], (iCounter + 1), "JHLateCops")
						SET_VEHICLE_SIREN(vehPolice[iCounter], TRUE) //SET_SIREN_WITH_NO_DRIVER(vehPolice[iCounter], TRUE)
						IMMEDIATELY_UNLOAD_NEED_LIST_VEHICLE_RECORDING("JHLateCops", (iCounter + 1))
						
						SET_VEHICLE_NAME_DEBUG(vehPolice[iCounter], CONVERT_INT_TO_STRING(iCounter))					
					ENDIF
				ENDFOR
				
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehPolice[2], 2000.0)
				SET_PLAYBACK_SPEED(vehPolice[2], 1.2)
				
				//bHasClearedLateCops = FALSE
			ENDIF
			
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(vehBike[2])
			SET_VEHICLE_IS_CONSIDERED_BY_PLAYER(vehBike[2], TRUE)
		ENDIF
		
		DISTANT_COP_CAR_SIRENS(TRUE)
		
		SET_MODELS_FOR_HEIST_CREW_AS_NO_LONGER_NEEDED(HEIST_JEWEL)
	
		SET_BIKE_STABILISERS(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
		
		fPlaybackCarStreamingDistance = 200.00
		fUberPlaybackParkedCarCleanupDistance = 50.00
				
		bLateCopsShooting 		= FALSE
		bPreloadNextStage 		= FALSE
		bCopsAtExitCreated 		= FALSE
		bSawBitchCrashOut		= FALSE
		bSAHelpPrinted = FALSE
		
		bHasFranklinBikeRecordingStarted = FALSE

		iPreloadRecordingsTimer = GET_GAME_TIMER()
		
		FOR iCounter = 0 TO NUMBER_OF_EVENT_FLAGS-1
			bEventFlag[iCounter] = FALSE
		ENDFOR
		
		bHasMichaelBeenDeletedForChase = FALSE
		
		bDrawCashScaleform = TRUE
		
		SET_RANDOM_TRAINS(FALSE)
		
		//SWITCH_ALL_RANDOM_CARS_ON()
		//Everything
		SET_ROADS_IN_AREA(<< -891.3078, -707.2606, -17.5355 >>, << 395.0734, -125.4589, 127.5168 >>, FALSE)
		//Left side of the road
		SET_ROADS_IN_AREA(<< -322.6849, -509.2203, 21.5231 >>, << 112.3121, -484.9729, 38.3947 >>, TRUE)
		
		ADD_ALL_PEDS_TO_DIALOGUE()
		
		IF eMissionStage = STAGE_BIKE_CHASE
		
			IF asset.mnBike = SANCHEZ
				PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_JSH_2A_01", 0.0)
			ELSE
				PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_JSH_2A_02", 0.0)
			ENDIF
		
			IF NOT DOES_ENTITY_EXIST(pedTrafficWarden)
				pedTrafficWarden = CREATE_PED(PEDTYPE_MISSION, IG_TRAFFICWARDEN, << -630.91, -240.94, 37.2314 >>,  132.867172)
			ENDIF
			
			IF DOES_ENTITY_EXIST(pedTrafficWarden)
				//SET_ENTITY_COORDS(pedTrafficWarden, << -630.91, -240.94, 37.2314 >>)
				//SET_ENTITY_HEADING(pedTrafficWarden, 132.867172)
				IF NOT IS_ENTITY_DEAD(pedTrafficWarden)
				
					STOP_PED_SPEAKING(pedTrafficWarden, TRUE)
				
					IF NOT IS_ENTITY_PLAYING_ANIM(pedTrafficWarden, "missheist_jewel", "cop_on_floor")				
						TASK_PLAY_ANIM(pedTrafficWarden, "missheist_jewel", "cop_on_floor", INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_USE_KINEMATIC_PHYSICS | AF_LOOPING)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedTrafficWarden)
					ENDIF
					
					SET_PED_PROP_INDEX(pedTrafficWarden, INT_TO_ENUM(PED_PROP_POSITION,0), 0, 0)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedTrafficWarden, TRUE)
					IMMEDIATELY_UNLOAD_NEED_LIST_MODEL(IG_TRAFFICWARDEN)
					REMOVE_ANIM_DICT(Asset.adJewelHeist)
					SET_PED_RELATIONSHIP_GROUP_HASH(pedTrafficWarden, RELGROUPHASH_PLAYER)
				ENDIF
			ENDIF
			
//			SET_VEHICLE_BURNOUT(vehBike[0], TRUE)
//			SET_VEHICLE_BURNOUT(vehBike[1], TRUE)
//			SET_VEHICLE_BURNOUT(vehBike[2], TRUE)
			
			runConversation("JH_BIKESGO", TRUE)
			
			
			IF IS_DRIVER_EDDIE()
				addTextToQueue("JH_LEFTE", TEXT_TYPE_CONVO)
			ELIF IS_DRIVER_TALINA()
				addTextToQueue("JH_LEFTT", TEXT_TYPE_CONVO)
			ELIF IS_DRIVER_KARIM()
				addTextToQueue("JH_LEFTK", TEXT_TYPE_CONVO)
			ENDIF
		
			//runConversation(Asset.convMove, TRUE)	//Franklin:	We gotta move.
//			IF g_bLateLeaveJewelStore
//				addTextToQueue("JH_TOOLONG", TEXT_TYPE_CONVO)//Fuck Michael took too long!
//				
//			ENDIF
			addTextToQueue(Asset.godFollowCrew, TEXT_TYPE_GOD)
			IF g_bLateLeaveJewelStore
				IF IS_GUNMAN_PACKIE()
					addTextToQueue("JH_GET_COPCP", TEXT_TYPE_CONVO)
				ELIF IS_GUNMAN_GUSTAVO()
					addTextToQueue("JH_GET_COPCR", TEXT_TYPE_CONVO)
				ELIF IS_GUNMAN_NORM()
					addTextToQueue("JH_GET_COPCN", TEXT_TYPE_CONVO)
				ENDIF
				
			ENDIF
			
			START_AUDIO_SCENE("JSH_2B_CHASE_START")
			
		ENDIF

		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehBike[2],JH2A_FRANKLIN_BIKE_DAMAGE) 

//		SET_ENTITY_INVINCIBLE(vehBike[0], TRUE)
		
		SAFE_FADE_IN()
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
			
		////SET_VEHICLE_DENSITY_MULTIPLIER(1.0)
		//iGenerateFreewayTraffic = 0
		iStageSection = 0
		bStageSetup = TRUE
		bCameraShotSetup = FALSE
		MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
			
		bHackerWarnsGuys = FALSE
				
		//bMusicEvent3 = FALSE
		bMusicEvent4 = FALSE
		bMusicEvent5 = FALSE
		bMusicEvent6 = FALSE
		bMusicEventStart = FALSE
		
		bTellguysToputLIghtsOn = FALSE
		bInterioLoaded= FALSE
		iGetLostStage = 0
		//iConvoBranch = 0
		
		
		IF eMissionStage = STAGE_BIKE_CHASE_START_OF_TUNNEL				
			SPECIAL_ABILITY_CHARGE_ABSOLUTE(PLAYER_ID(), 25, TRUE)
			iStageSection = 4
			bMusicEvent5 = TRUE
			bMusicEventStart = TRUE
		ENDIF
		
		SETUP_WANTED_LEVEL()
		
		SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING) 
		
		CREATE_FORCED_OBJECT(<<-63.0, -541.0, 32.0>>, 15.0, PROP_BARRIER_WORK06B, TRUE)
		CREATE_FORCED_OBJECT(<<-63.0, -541.0, 32.0>>, 15.0, PROP_BARRIER_WORK06A, TRUE)
		////		
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehBike[2], "JSH_2B_PLAYER_BIKE")
		
		ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehBike[0], "JSH_2B_BUDDY_BIKES")
		IF eMissionStage = STAGE_BIKE_CHASE
		OR bWorstGunDies = FALSE
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehBike[1], "JSH_2B_BUDDY_BIKES")
		ENDIF	
				
		SETTIMERA(0)
		SETTIMERB(0)
		iTimeOfLeaving = GET_GAME_TIMER()
		
		REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
		
//═════════╡ UPDATE ╞═════════		
	ELSE	
	
		//1449874
		IF NOT IS_ENTITY_DEAD(pedCrew[CREW_DRIVER_NEW])
			SET_PED_RESET_FLAG(pedCrew[CREW_DRIVER_NEW], PRF_PreventGoingIntoStillInVehicleState, TRUE)
		ENDIF
	
		IF NOT IS_ENTITY_DEAD(pedCrew[CREW_GUNMAN_NEW])
			SET_PED_RESET_FLAG(pedCrew[CREW_GUNMAN_NEW], PRF_PreventGoingIntoStillInVehicleState, TRUE)
		ENDIF
	
		IF DOES_ENTITY_EXIST(SetPieceCarID[3])
			IF NOT IS_ENTITY_DEAD(SetPieceCarID[3])
				//SET_ENTITY_COLLISION(SetPieceCarID[5], FALSE)
				SET_VEHICLE_COLOURS(SetPieceCarID[3], 14, 14)
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(TrafficCarID[5])
			IF NOT IS_ENTITY_DEAD(TrafficCarID[5])
				SET_VEHICLE_COLOURS(TrafficCarID[5], 28, 28)
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(pedTrafficWarden)
			IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedTrafficWarden, PLAYER_PED_ID())
			OR GET_DISTANCE_BETWEEN_ENTITIES(pedTrafficWarden, PLAYER_PED_ID()) > 60.0
				APPLY_DAMAGE_TO_PED(pedTrafficWarden, 100, TRUE)
				SET_PED_AS_NO_LONGER_NEEDED(pedTrafficWarden)
				SET_MODEL_AS_NEEDED(IG_TRAFFICWARDEN)
				
				SET_PED_AS_NO_LONGER_NEEDED(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				
			ENDIF
		ELSE
			IF DOES_ENTITY_EXIST(pedTrafficWarden)
				SET_PED_AS_NO_LONGER_NEEDED(pedTrafficWarden)
				SET_MODEL_AS_NEEDED(IG_TRAFFICWARDEN)
			ENDIF
		ENDIF
		
		UPDATE_CHASE_BLIP(blipCrew[0], vehBike[0], 195.0, 0.85) 
		IF DOES_ENTITY_EXIST(vehBike[1])
			UPDATE_CHASE_BLIP(blipCrew[1], vehBike[1], 195.0, 0.85) 
		ENDIF				
		
		//If the alarm hasn't started, start it up.
		IF bIsAlarmRinging = FALSE
			IF PREPARE_ALARM("JEWEL_STORE_HEIST_ALARMS")
				START_ALARM("JEWEL_STORE_HEIST_ALARMS", FALSE)
				bIsAlarmRinging = TRUE
			ENDIF
		ENDIF
		
		HANDLE_TUNNEL_INTERIOR()
		
		//Force interior to load 
		IF bInterioLoaded= FALSE
//			IF IS_INTERIOR_READY(tunnelInterior)
//				
//			ENDIF
		ENDIF
	
		//SET_FAKE_WANTED_LEVEL(3)
		
		IF bMusicEventStart = FALSE
			IF bIsStealthPath
				IF PREPARE_MUSIC_EVENT("JH2A_ONTO_BIKE_MA")
					TRIGGER_MUSIC_EVENT("JH2A_ONTO_BIKE_MA")
					bMusicEventStart = TRUE
				ENDIF
			ELSE
				IF PREPARE_MUSIC_EVENT("JH2B_ONTO_BIKE_MA")
					TRIGGER_MUSIC_EVENT("JH2B_ONTO_BIKE_MA")
					bMusicEventStart = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		IF iStageSection >= 5
			SET_RADAR_ZOOM(1100)
			SET_RADAR_AS_INTERIOR_THIS_FRAME(GET_HASH_KEY("V_fakeTunnels"), -150.0, -500.0) 
		ENDIF
		
		REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
		
		//Only see garbage truck on first try
		IF g_JH2AGarbageTruckSeen = FALSE
			IF DOES_ENTITY_EXIST(SetPieceCarID[5])
				g_JH2AGarbageTruckSeen = TRUE
			ENDIF
		ENDIF
		
		UPDATE_AMBIENT_SWEARING()
		
		IF eMissionStage = STAGE_BIKE_CHASE
			CONTROL_LATE_LEAVE_COPS()
		ENDIF
		
		SWITCH iStageSection
			
			CASE 0
		
				IF tunnelInterior <> NULL
					UNPIN_INTERIOR(tunnelInterior)
				ENDIF
				
				SETTIMERA(0)	
				IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
					MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
					TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
					SET_PED_CREW(GET_SELECT_PED(SEL_MICHAEL))
					SET_PED_CREW(GET_SELECT_PED(SEL_FRANKLIN))
				ENDIF
				GIVE_WEAPON_TO_PED(GET_SELECT_PED(SEL_FRANKLIN), WEAPONTYPE_MICROSMG, 200, TRUE, TRUE)
				//TASK_GO_STRAIGHT_TO_COORD(GET_SELECT_PED(SEL_MICHAEL), vJewelWalkCoords, PEDMOVE_WALK)
			
				IF NOT IS_TASK_ONGOING(GET_SELECT_PED(SEL_MICHAEL), SCRIPT_TASK_FOLLOW_NAV_MESH_TO_COORD)
					TASK_FOLLOW_NAV_MESH_TO_COORD(GET_SELECT_PED(SEL_MICHAEL), vJewelWalkCoords, PEDMOVE_WALK, DEFAULT_TIME_NEVER_WARP)
				ENDIF
				
				SET_ENTITY_INVINCIBLE(GET_SELECT_PED(SEL_MICHAEL), FALSE)
				SET_ENTITY_CAN_BE_DAMAGED(GET_SELECT_PED(SEL_MICHAEL), TRUE)
				
					
				IF DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					IF NOT IS_ENTITY_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL]), <<50.0, 50.0, 50.0>>)
						REMOVE_PED_ELEGANTLY(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
					ENDIF
				ENDIF
				
				fChaseSpeed = 1.0
				SET_PLAYBACK_SPEED(vehBike[0], fChaseSpeed)						// +++ Vehicle speed set +++
				UPDATE_UBER_PLAYBACK(vehBike[0], fChaseSpeed)
				IF IS_VEHICLE_DRIVEABLE(vehPolice[0])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice[0])
						SET_PLAYBACK_SPEED(vehPolice[0], fChaseSpeed)
					ENDIF
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehPolice[1])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice[1])
						SET_PLAYBACK_SPEED(vehPolice[1], fChaseSpeed)
					ENDIF
				ENDIF
				IF IS_VEHICLE_DRIVEABLE(vehPolice[2])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice[2])
						SET_PLAYBACK_SPEED(vehPolice[2], fChaseSpeed)
					ENDIF
				ENDIF															// --- Vehicle speed set ---
			
				SETTIMERA(0)
				DESTROY_CAM(camAnim)
				iStageSection = 3
				

				CREATE_ALL_WAITING_UBER_CARS()	
				UPDATE_UBER_PLAYBACK(vehBike[0], fChaseSpeed)

			BREAK
			
			CASE 3
			CASE 4
			CASE 5
			CASE 6
		
			
				// === Trigger chase events ===
				//CREATE_ALL_WAITING_UBER_CARS()	
				DO_CHASE_DIALOGUE()
				DO_CHASE_TRIGGERS()				
				IF iStageSection < 5	//Kill chase hint cam at tunnel.
					OVERRIDE_CINEMATIC_CAMERA()
				ENDIF
				MANAGE_BIKE_BLIPS()
				
				// --- Blip management ---

//				SWITCH iTunnelPinStage
//				
//					CASE 0
//
						IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-172.834839,-483.798981,33.229412>>, <<-176.148087,-494.020905,37.720833>>, 4.000000)

							
							bMusicEvent4 = TRUE
						ENDIF

				
				IF bMusicEvent4 = TRUE
				AND bMusicEventStart = TRUE
					IF iStageSection = 3
						IF bIsStealthPath
							IF PREPARE_MUSIC_EVENT("JH2A_JUMP_OS")
								TRIGGER_MUSIC_EVENT("JH2A_JUMP_OS")								
								iStageSection = 4
							ENDIF
						ELSE
							IF PREPARE_MUSIC_EVENT("JH2B_JUMP_OS")								
								TRIGGER_MUSIC_EVENT("JH2B_JUMP_OS")																
								iStageSection = 4
							ENDIF
						ENDIF
					ENDIF					
				ENDIF
				
				
				IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-58.079208,-540.804321,30.774742>>, <<-68.361420,-541.720276,36.437607>>, 8.000000)
				OR eMissionStage = STAGE_BIKE_CHASE_START_OF_TUNNEL
					//iStageSection = 4
					bMusicEvent5 = TRUE
				ENDIF
				
				IF bMusicEvent5 = TRUE
					IF iStageSection = 4
						IF bIsStealthPath
							IF PREPARE_MUSIC_EVENT("JH2A_ENTER_TUNNEL_MA")
								TRIGGER_MUSIC_EVENT("JH2A_ENTER_TUNNEL_MA")
								
								IF IS_AUDIO_SCENE_ACTIVE("JSH_2B_CHASE_START")
									STOP_AUDIO_SCENE("JSH_2B_CHASE_START")
								ENDIF
								
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
								
								//XXX : SET_POLICE_RADAR_BLIPS(FALSE)
																
								bInterioLoaded= TRUE
								IF tunnelInterior <> NULL
									UNPIN_INTERIOR(tunnelInterior)
								ENDIF
								
								IF bIsStealthPath
									SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "STAGE_BIKE_CHASE_START_OF_TUNNEL")
								ELSE
									SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "STAGE_BIKE_CHASE_START_OF_TUNNEL")
								ENDIF
								g_replay.iReplayInt[CURRENT_TAKE_REPLAY_VALUE] = iCurrentTake
								KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
								iStageSection = 5
							ENDIF
						ELSE
							IF PREPARE_MUSIC_EVENT("JH2B_ENTER_TUNNEL_MA")
								
								IF IS_AUDIO_SCENE_ACTIVE("JSH_2B_CHASE_START")
									STOP_AUDIO_SCENE("JSH_2B_CHASE_START")
								ENDIF
								
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 4.0, REPLAY_IMPORTANCE_HIGHEST)
								
								SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", TRUE)
								//XXX : SET_POLICE_RADAR_BLIPS(FALSE)
								IF tunnelInterior <> NULL
									UNPIN_INTERIOR(tunnelInterior)
								ENDIF
								bInterioLoaded= TRUE
								TRIGGER_MUSIC_EVENT("JH2B_ENTER_TUNNEL_MA")
								
								IF bIsStealthPath
									SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "STAGE_BIKE_CHASE_START_OF_TUNNEL")
								ELSE
									SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(4, "STAGE_BIKE_CHASE_START_OF_TUNNEL")
								ENDIF
								g_replay.iReplayInt[CURRENT_TAKE_REPLAY_VALUE] = iCurrentTake
								KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
								iStageSection = 5
							ENDIF
						ENDIF
					ENDIF					
				ENDIF
				
				IF iGetLostStage = 0	//not Started
				OR iGetLostStage = 10	//or finishsed
					IF NOT IS_ENTITY_DEAD(vehBike[1])					//If Bike[1] is not dead, use it to rubberband
						CONTROL_PLAYBACK_SPEED_NEW(vehBike[2], vehBike[1], fDesiredSpeed)	
						 
					ELSE
						CONTROL_PLAYBACK_SPEED_NEW(vehBike[2], vehBike[0], fDesiredSpeed)	
					ENDIF
					
					fAccel = 0.01
					
					//Suggested fix by Matt.
					IF fChaseSpeed < 1.0
					AND fDesiredPlaybackSpeed > fChaseSpeed
	      				fAccel *= 2.0
					ENDIF

					
					UPDATE_CURRENT_PLAYBACK_SPEED_WITH_SMOOTHING(fChaseSpeed, fDesiredSpeed, fAccel)
					
					SET_PLAYBACK_SPEED(vehBike[0], fChaseSpeed)						// +++ Vehicle speed set +++
					IF IS_VEHICLE_DRIVEABLE(vehPolice[1])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice[1])
							SET_PLAYBACK_SPEED(vehBike[1], fChaseSpeed)	
						ENDIF
					ENDIF
										
					IF GET_GAME_TIMER() - iTimeOfLeaving < 5000	//Force all cars for the first five seconds.
						CREATE_ALL_WAITING_UBER_CARS()		
					ENDIF
					
					UPDATE_UBER_PLAYBACK(vehBike[0], fChaseSpeed)
					
					IF IS_VEHICLE_DRIVEABLE(vehPolice[0])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice[0])
							SET_PLAYBACK_SPEED(vehPolice[0], fChaseSpeed)
						ENDIF
					ENDIF
					IF IS_VEHICLE_DRIVEABLE(vehPolice[1])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehPolice[1])
							SET_PLAYBACK_SPEED(vehPolice[1], fChaseSpeed)
						ENDIF
					ENDIF
				ENDIF				
			
				//If we have a shit driver he'll get lost etc... blah blah blah.
				IF GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW))) = CMSK_BAD
				AND bWorstGunDies
					//Get lost		
					SWITCH iGetLostStage
					
						CASE 0 //0 temp disable
							
							REQUEST_ANIM_DICT("missheist_jewel@lost")
							
							IF IS_ENTITY_IN_ANGLED_AREA(vehBike[0], <<-14.763006,-805.227905,15.391008>>, <<-24.694069,-782.254944,22.467754>>, 21.750000)
							AND HAS_ANIM_DICT_LOADED("missHeist_jewel@lost")	
								//Stop the recordings!
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0])
									STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[0])
								ENDIF
								bBikeZeroStopped = FALSE
								iGetLostStage++
							ENDIF
						BREAK
															
						CASE 1
						
							IF BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehBike[0], 15.0)
								TASK_VEHICLE_TEMP_ACTION( GET_PED_IN_VEHICLE_SEAT(vehBike[0], VS_DRIVER), vehBike[0], TEMPACT_HANDBRAKESTRAIGHT, INFINITE_TASK_TIME) 
								SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(GET_PED_IN_VEHICLE_SEAT(vehBike[0], VS_DRIVER), TRUE)
								bBikeZeroStopped = TRUE	
							ENDIF
																			
							IF bBikeZeroStopped
								
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
								
								emptyTextQueue()
								KILL_ANY_CONVERSATION()
								//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								iGetLostStage++
							ENDIF
							
						BREAK
						
						CASE 2
							
							IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_WHICH_KD", CONV_PRIORITY_VERY_HIGH)
								//IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_GOTLOST", CONV_PRIORITY_VERY_HIGH)
									bBikeZeroStopped= FALSE
									//bBikeOneStopped= FALSE
									fCatchUpSpeed = 0.1
									iTimeOfGettingLost = GET_GAME_TIMER()
									IF ASset.mnBike = BATI2
										TASK_PLAY_ANIM(pedCrew[CREW_DRIVER_NEW], "missheist_jewel@lost", "lost_idle_sports_b")
									ELSE
										TASK_PLAY_ANIM(pedCrew[CREW_DRIVER_NEW], "missheist_jewel@lost", "lost_idle_dirt_b")
									ENDIF
									TASK_LOOK_AT_ENTITY(pedCrew[CREW_DRIVER_NEW], PLAYER_PED_ID(), 15000, SLF_WHILE_NOT_IN_FOV)
									bGuyLosesFranklin = FALSE
									iGetLostStage++
								ENDIF
							ENDIF
							
						BREAK
						
						CASE 3
							
							//Where are you etc/?? 
							IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
								IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedCrew[CREW_DRIVER_NEW]) > 20.0
									IF GET_GAME_TIMER() - iTimeOfGettingLost > 6000
										IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_WHERE_KD", CONV_PRIORITY_VERY_HIGH)
											bGuyLosesFranklin = TRUE
											iTimeOfGettingLost = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ELSE
									IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), pedCrew[CREW_DRIVER_NEW]) < 16.0
										IF bGuyLosesFranklin
											IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_FRANK_KD", CONV_PRIORITY_VERY_HIGH)
												iGetLostStage++
											ENDIF
										ELSE
											iGetLostStage++
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						
						BREAK
						
						CASE 4
							IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_WHICH_KD", CONV_PRIORITY_VERY_HIGH)
									TASK_LOOK_AT_COORD(pedCrew[CREW_DRIVER_NEW], <<-17.8649, -809.1964, 18.7799>>, 4000, SLF_WHILE_NOT_IN_FOV)
									iGetLostStage = 5
								ENDIF
							ENDIF
						BREAK
						
						CASE 5						
							IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_LOST_KD", CONV_PRIORITY_VERY_HIGH)
									TASK_LOOK_AT_COORD(pedCrew[CREW_DRIVER_NEW], <<-17.8649, -809.1964, 18.7799>>, 4000, SLF_WHILE_NOT_IN_FOV)
									iGetLostStage++
								ENDIF
							ENDIF
						BREAK
						
						CASE 6
							IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_UMM_KD", CONV_PRIORITY_VERY_HIGH)
									TASK_LOOK_AT_COORD(pedCrew[CREW_DRIVER_NEW], <<-7.7608, -805.0465, 18.8407>>, 4000, SLF_WHILE_NOT_IN_FOV)
									iGetLostStage++
								ENDIF
							ENDIF
						BREAK
						
						CASE 7
							IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_THIS_KD", CONV_PRIORITY_VERY_HIGH)
									TASK_LOOK_AT_COORD(pedCrew[CREW_DRIVER_NEW], <<-17.8649, -809.1964, 18.7799>>, 4000, SLF_WHILE_NOT_IN_FOV)
									iGetLostStage++
								ENDIF
							ENDIF
						BREAK
						
						CASE 8				
														
							IF GET_GAME_TIMER() - iTimeOfGettingLost > 5000
							//AND IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), vehBike[0], <<7.0, 7.0, 7.0>>)
								IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
									IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_SETOFF_KD", CONV_PRIORITY_VERY_HIGH)
										
										REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
										
										//Restart recordings!
										START_PLAYBACK_RECORDED_VEHICLE(vehBike[0],  Asset.recNumber, Asset.recUber) //xyz
										SET_PLAYBACK_SPEED(vehBike[0], 0.1)
										SET_PLAYBACK_TO_USE_AI_TRY_TO_REVERT_BACK_LATER(vehBike[0],2000, DRIVINGMODE_AVOIDCARS, FALSE)
										fRejoinSpeed = 0.0
										iGetLostStage = 9
									ENDIF
								ENDIF
							ENDIF
						BREAK
								
						CASE 9
						
							IF fRejoinSpeed < fDesiredPlaybackSpeed 
								fRejoinSpeed = fRejoinSpeed +@ 0.2
							ELSE
								iGetLostStage = 10
							ENDIF
							
							SET_PLAYBACK_SPEED(vehBike[0], fRejoinSpeed)
														
						BREAK
																	
					ENDSWITCH
					
				ELSE
					//Do nothing - maybe comment on how well he knows the tunnels?
					//iGetLostStage = 
				ENDIF
				
				//After the tunnel splits turn off the new load scene
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),  <<77.9623, -758.2039, 16.7359>>, <<5.0, 5.0, 5.0>>)
					IF IS_NEW_LOAD_SCENE_ACTIVE()
						NEW_LOAD_SCENE_STOP()
					ENDIF
				ENDIF
				
				
				//At train track section.
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(),  <<124.5188, -615.0482, 16.7761>>, <<5.0, 5.0, 5.0>>)
				AND bMusicEvent6 = FALSE
					IF bIsStealthPath
						TRIGGER_MUSIC_EVENT("JH2A_TUNNEL_MID")
					ELSE
						TRIGGER_MUSIC_EVENT("JH2B_TUNNEL_MID")
					ENDIF
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
									
					REQUEST_TRUCK_FIGHT_ASSETS()
					bMusicEvent6 = TRUE
				ENDIF
				
						
								
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), << -65.7330, -539.4092, 30.7915 >>, <<25.0, 25.0, 30.0>>)
				AND bTellguysToputLightsOn = FALSE
										
					SET_VEHICLE_LIGHTS(vehBike[0], SET_VEHICLE_LIGHTS_ON)
					IF NOT bWorstGunDies
						SET_VEHICLE_LIGHTS(vehBike[1], FORCE_VEHICLE_LIGHTS_ON)
					ENDIF
					//SET_VEHICLE_LIGHTS(vehBike[2], FORCE_VEHICLE_LIGHTS_ON)
					IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_LIGHTSON", CONV_PRIORITY_VERY_HIGH)
						REPLAY_RECORD_BACK_FOR_TIME(10.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
						bTellguysToputLightsOn = TRUE
					ENDIF
				ENDIF
				
				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<1022.06, -207.22, 43.00>>, <<65.0, 65.0, 40.0>>)
				AND bHackerWarnsGuys = FALSE
					
					REQUEST_PLAYER_PED_MODEL(CHAR_MICHAEL)
					
					//SCRIPT_ASSERT("boom")
					
					//REQUEST_TRUCK_FIGHT_ASSETS() -now requested at midway point
					
					REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
					
					//addTextToQueue("JH_MIKECOPS", TEXT_TYPE_CONVO)
					DISTANT_COP_CAR_SIRENS(TRUE)
					SET_CLOCK_TIME(19, 20, 0)
					bHackerWarnsGuys = TRUE
				ENDIF

				
				//Prestream Michael - creat near truck Location
				IF NOT DOES_ENTITY_EXIST(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL])
				
					IF NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, <<1091.1180, -260.9309, 68.3108>>, 129.7877)
					ELSE
						IF bIsStealthPath
							SET_PED_COMP_ITEM_CURRENT_SP(GET_SELECT_PED(SEL_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_EXTERMINATOR , FALSE)
						ELSE
							SET_PED_COMP_ITEM_CURRENT_SP(GET_SELECT_PED(SEL_MICHAEL), COMP_TYPE_OUTFIT, OUTFIT_P0_JEWEL_HEIST, FALSE)
						ENDIF
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], TRUE)
					ENDIF
				ENDIF


				//At end of tunnel
				/*IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(),<<1037.7, -267.7, 54.4>>, <<1027.3, -272.3, 40.5>>, 20.000000)
					iStageSection = 6
				ENDIF*/			

				//Switch to Michael in drain...
				/*IF  bMusicEvent8 = TRUE
				AND iStageSection = 6
					STOP_AUDIO_SCENE("JSH_2B_CHASE_ENTER_TUNNELS")
					iStageSection = 7
				ENDIF */
				
				IF NOT bHasFranklinBikeRecordingStarted
					IF IS_FRANKLIN_BIKE_RECORDING_TRIGGERED()
						START_FRANKLIN_BIKE_RECORDING()
						
						bHasFranklinBikeRecordingStarted = TRUE
					ELSE
						CDEBUG3LN(DEBUG_MISSION, "Waiting for Franklin to trigger recording...")
					ENDIF
				ENDIF
				
		
				IF IS_SWITCH_CAM_TRIGGERED()
					
					IF bIsStealthPath
						TRIGGER_MUSIC_EVENT("JH2A_EXIT_TUNNEL_MA")
					ELSE
						TRIGGER_MUSIC_EVENT("JH2B_EXIT_TUNNEL_MA")
					ENDIF						
					
					IF bIsStealthPath
						SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6, "STAGE_BIKE_CHASE_END_OF_TUNNEL")
					ELSE
						SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(5, "STAGE_BIKE_CHASE_END_OF_TUNNEL")
					ENDIF
					
					g_replay.iReplayInt[CURRENT_TAKE_REPLAY_VALUE] = iCurrentTake
					
					CREATE_TRUCK_FIGHT_TRUCK()
										
					STOP_AUDIO_SCENE("JSH_2B_CHASE_ENTER_TUNNELS")
					
					SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", TRUE)
					
					//Do rubber band off the truck to stop it colliding with the cops
					//Update playback speeds...
					fChaseSpeed = CONTROL_PLAYBACK_SPEED(vehTruck, -7.0)
				
					IF fChaseSpeed <= 0.5
						fChaseSpeed = 0.5
					ENDIF
					
					SET_PLAYBACK_SPEED(vehBike[0], fChaseSpeed)						// +++ Vehicle speed set +++
					UPDATE_UBER_PLAYBACK(vehBike[0], fChaseSpeed)
					
					iStageSection = 7
				ENDIF
				
			BREAK
			
			
		ENDSWITCH
	
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 7
			FOR iCounter = 0 TO 2
				IF NOT (bWorstGunDies AND iCounter = 1)
					IF DOES_BLIP_EXIST(blipCrew[iCounter])
						REMOVE_BLIP(blipCrew[iCounter])
					ENDIF
				ENDIF
			ENDFOR
			IF DOES_BLIP_EXIST(blipBike)
				REMOVE_BLIP(blipBike)
			ENDIF
			
			SET_POLICE_RADAR_BLIPS(TRUE)
			
			SET_VEHICLE_POPULATION_BUDGET(3)
			SET_PED_POPULATION_BUDGET(3)
				
			ReleaseSound(SND_ALARM_BELL)
			ReleaseSound(SND_ROOFTOP_AIRCON)
			//ReleaseSound(SND_POLICE_SIRENS)
						
			SET_VEHICLE_MODEL_IS_SUPPRESSED(Asset.mnBike, FALSE)
			
			SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
			
			IF DOES_ENTITY_EXIST(vehTrainDodge)
				DELETE_MISSION_TRAIN(vehTrainDodge)
			ENDIF
						
			SET_CINEMATIC_BUTTON_ACTIVE(TRUE)
			
			SWITCH_ALL_RANDOM_CARS_ON()	
			STOP_GAMEPLAY_HINT()
			REMOVE_WEAPON_ASSET(WEAPONTYPE_PISTOL)
			
			bStageSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC



FUNC  VEHICLE_INDEX GET_LEADING_VEHICLE_FROM_INDEXES(VEHICLE_INDEX vehicle1, VEHICLE_INDEX vehicle2)

	FLOAT fDistVeh0ToEnd = 99999999.0
	FLOAT fDistVeh1ToEnd = 99999999.0

	IF DOES_ENTITY_EXIST(vehicle1)
		IF NOT IS_ENTITY_DEAD(vehicle1)  
			fDistVeh0ToEnd = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehicle1), vEndOfStormDrain)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehicle2)
		IF NOT IS_ENTITY_DEAD(vehicle2)
			fDistVeh1ToEnd = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehicle2), vEndOfStormDrain)
		ENDIF
	ENDIF
	
	IF fDistVeh0ToEnd < fDistVeh1ToEnd
		RETURN vehicle1
	ELSE
		RETURN vehicle2
	ENDIF
		
	SCRIPT_ASSERT("GET_LEADING_VEHICLE() about to return NULL!")
	
	RETURN NULL
		
ENDFUNC

FUNC  VEHICLE_INDEX GET_LEADING_VEHICLE()

	FLOAT fDistBike0ToEnd = 99999999.0
	FLOAT fDistBike1ToEnd = 99999999.0
	//FLOAT fDistBike2ToEnd = 99999999.0

	IF DOES_ENTITY_EXIST(vehBike[0])
		IF NOT IS_ENTITY_DEAD(vehBike[0])  
			fDistBike0ToEnd = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehBike[0]), vEndOfStormDrain)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehBike[1])
		IF NOT IS_ENTITY_DEAD(vehBike[1])
			fDistBike1ToEnd = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehBike[1]), vEndOfStormDrain)
		ENDIF
	ENDIF

//	//If its not the player on the bike (i.e. we switched to Michael)
//	IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBike[2])
//		IF DOES_ENTITY_EXIST(vehBike[2])
//			IF NOT IS_ENTITY_DEAD(vehBike[2])
//				fDistBike2ToEnd = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehBike[2]), vEndOfStormDrain)
//			ENDIF
//		ENDIF
//	ELSE
//		IF fDistBike0ToEnd < fDistBike1ToEnd
//			RETURN vehBike[0]
//		ELSE
//			RETURN vehBike[1]
//		ENDIF
//	ENDIF
	
	IF fDistBike0ToEnd < fDistBike1ToEnd
	//AND fDistBike0ToEnd < fDistBike2ToEnd
		RETURN vehBike[0]
	ENDIF
	
	IF fDistBike1ToEnd < fDistBike0ToEnd
//	AND fDistBike1ToEnd < fDistBike2ToEnd
		RETURN vehBike[1]
	ENDIF
	
//	IF fDistBike2ToEnd < fDistBike0ToEnd
//	AND fDistBike2ToEnd < fDistBike1ToEnd
//		RETURN vehBike[2]
//	ENDIF
	
	SCRIPT_ASSERT("GET_LEADING_VEHICLE() about to return NULL!")
	
	RETURN NULL
		
ENDFUNC

FUNC  VEHICLE_INDEX GET_LEADING_VEHICLE_INCLUDING_PLAYERS_BIKE()

	FLOAT fDistBike0ToEnd = 99999999.0
	FLOAT fDistBike1ToEnd = 99999999.0
	FLOAT fDistBike2ToEnd = 99999999.0

	IF DOES_ENTITY_EXIST(vehBike[0])
		IF NOT IS_ENTITY_DEAD(vehBike[0])  
			fDistBike0ToEnd = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehBike[0]), vEndOfStormDrain)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehBike[1])
		IF NOT IS_ENTITY_DEAD(vehBike[1])
			fDistBike1ToEnd = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehBike[1]), vEndOfStormDrain)
		ENDIF
	ENDIF

	//If its not the player on the bike (i.e. we switched to Michael)
	IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBike[2])
		IF DOES_ENTITY_EXIST(vehBike[2])
			IF NOT IS_ENTITY_DEAD(vehBike[2])
				fDistBike2ToEnd = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehBike[2]), vEndOfStormDrain)
			ENDIF
		ENDIF
	ELSE
		IF fDistBike0ToEnd < fDistBike1ToEnd
			RETURN vehBike[0]
		ELSE
			RETURN vehBike[1]
		ENDIF
	ENDIF
	
	IF fDistBike0ToEnd < fDistBike1ToEnd
	AND fDistBike0ToEnd < fDistBike2ToEnd
		RETURN vehBike[0]
	ENDIF
	
	IF fDistBike1ToEnd < fDistBike0ToEnd
	AND fDistBike1ToEnd < fDistBike2ToEnd
		RETURN vehBike[1]
	ENDIF
	
	IF fDistBike2ToEnd < fDistBike0ToEnd
	AND fDistBike2ToEnd < fDistBike1ToEnd
		RETURN vehBike[2]
	ENDIF
	
	//SCRIPT_ASSERT("GET_LEADING_VEHICLE() about to return NULL!")
	
	RETURN NULL
		
ENDFUNC


FUNC  VEHICLE_INDEX GET_CLOSEST_COP_SET_PIECE(VEHICLE_INDEX closeBike, ENTITY_INDEX closeToEntity)

	//RETURN GET_CLOSEST_VEHICLE(GET_ENTITY_COORDS(PLAYER_PED_ID()), 100.0, DUMMY_MODEL_FOR_SCRIPT, VEHICLE_SEARCH_FLAG_RETURN_MISSION_VEHICLES)

	INT i 
	
	FLOAT fClosestDistance = 999999.99
	INT iClosestIndex
	
	FOR i = 0 TO TOTAL_NUMBER_OF_SET_PIECE_CARS - 1
		IF i <> 14
		AND DOES_ENTITY_EXIST(SetPieceCarID[i])
			IF NOT IS_ENTITY_DEAD(SetPieceCarID[i])
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[i])
					IF GET_ENTITY_MODEL(SetPieceCarID[i]) = POLICE
					OR GET_ENTITY_MODEL(SetPieceCarID[i]) = POLICE2
					OR GET_ENTITY_MODEL(SetPieceCarID[i]) = POLICE3
						IF GET_DISTANCE_BETWEEN_ENTITIES(closeToEntity, SetPieceCarID[i]) < fClosestDistance
							fClosestDistance = GET_DISTANCE_BETWEEN_ENTITIES(closeToEntity, SetPieceCarID[i])
							iClosestIndex = i	
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDFOR


	IF  fClosestDistance > GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), closeBike)
		RETURN closeBike
	ELSE
		RETURN SetPieceCarID[iClosestIndex]	
	ENDIF

ENDFUNC


FUNC  VEHICLE_INDEX GET_CLOSEST_BIKE()

	FLOAT fDistBike0 = 99999999.0
	FLOAT fDistBike1 = 99999999.0
	FLOAT fDistBike2 = 99999999.0

	IF DOES_ENTITY_EXIST(vehBike[0])
		IF NOT IS_ENTITY_DEAD(vehBike[0])  
			fDistBike0 = GET_DISTANCE_BETWEEN_ENTITIES(vehBike[0], PLAYER_PED_ID())
		ENDIF
	ENDIF
	IF NOT bWorstGunDies
		IF DOES_ENTITY_EXIST(vehBike[1])
			IF NOT IS_ENTITY_DEAD(vehBike[1])
				fDistBike1 = GET_DISTANCE_BETWEEN_ENTITIES(vehBike[1], PLAYER_PED_ID())
			ENDIF
		ENDIF
	ENDIF

	//If its not the player on the bike (i.e. we switched to Michael)
	IF bCaughtUp //NOT bWorstGunDies
		IF DOES_ENTITY_EXIST(vehBike[2])
			IF NOT IS_ENTITY_DEAD(vehBike[2])
				fDistBike2 = GET_DISTANCE_BETWEEN_ENTITIES(vehBike[2], PLAYER_PED_ID())
			ENDIF
		ENDIF
	ENDIF
	
	IF fDistBike0 < fDistBike1
	AND fDistBike0 < fDistBike2
		RETURN vehBike[0]
	ENDIF
	
	IF NOT bWorstGunDies
		IF fDistBike1 < fDistBike0
		AND fDistBike1 < fDistBike2
			RETURN vehBike[1]
		ENDIF
	ENDIF
	
	IF fDistBike2 < fDistBike0
	AND fDistBike2 < fDistBike1
		RETURN vehBike[2]
	ENDIF
	
	RETURN NULL
		
ENDFUNC



FUNC  VEHICLE_INDEX GET_TRAILING_VEHICLE()

	FLOAT fDistBike0ToEnd = 0.0
	FLOAT fDistBike1ToEnd = 0.0
	//FLOAT fDistBike2ToEnd = 0.0

	IF DOES_ENTITY_EXIST(vehBike[0])
		IF NOT IS_ENTITY_DEAD(vehBike[0])  
			fDistBike0ToEnd = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehBike[0]), vEndOfStormDrain)
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(vehBike[1])
		IF NOT IS_ENTITY_DEAD(vehBike[1])
			fDistBike1ToEnd = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehBike[1]), vEndOfStormDrain)
		ENDIF
	ENDIF
//
//	//If its not the player on the bike (i.e. we switched to Michael)
//	IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vehBike[2])
//		IF DOES_ENTITY_EXIST(vehBike[2])
//			IF NOT IS_ENTITY_DEAD(vehBike[2])
//				fDistBike2ToEnd = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehBike[2]), vEndOfStormDrain)
//			ENDIF
//		ENDIF
//	ELSE
//		IF fDistBike0ToEnd > fDistBike1ToEnd
//			RETURN vehBike[0]
//		ELSE
//			RETURN vehBike[1]
//		ENDIF
//	ENDIF
	
	IF fDistBike0ToEnd > fDistBike1ToEnd
	//AND fDistBike0ToEnd > fDistBike2ToEnd
		RETURN vehBike[0]
	ENDIF
	
	IF fDistBike1ToEnd > fDistBike0ToEnd
	//AND fDistBike1ToEnd > fDistBike2ToEnd
		RETURN vehBike[1]
	ENDIF
	
//	IF fDistBike2ToEnd > fDistBike0ToEnd
//	AND fDistBike2ToEnd > fDistBike1ToEnd
//		RETURN vehBike[2]
//	ENDIF
	
	SCRIPT_ASSERT("GET_TRAILING_VEHICLE() about to return NULL!")
	
	RETURN NULL
		
ENDFUNC

FUNC  PED_INDEX GET_LEADING_VEHICLE_DRIVER()

	PED_INDEX thisPed

	IF NOT IS_ENTITY_DEAD(GET_LEADING_VEHICLE())
		thisPed = GET_PED_IN_VEHICLE_SEAT(GET_LEADING_VEHICLE(), VS_DRIVER)
	ENDIF

	RETURN thisPed

ENDFUNC

FUNC  PED_INDEX GET_TRAILING_VEHICLE_DRIVER()

	PED_INDEX thisPed

	IF NOT IS_ENTITY_DEAD(GET_TRAILING_VEHICLE())
		thisPed = GET_PED_IN_VEHICLE_SEAT(GET_TRAILING_VEHICLE(), VS_DRIVER)
	ENDIF

	RETURN thisPed

ENDFUNC


//PURPOSE: triggers multiple bullet hits around the player at given intervals.
//AUTHOR: Ross Wallace
PROC DO_EXCITING_NEAR_BULLET_MISS_ON_PED(PED_INDEX pedToMiss, PED_INDEX sourceOfBullets, INT &iControlTimer, VECTOR sourceOffset, 
                                                             INT timeBetweenBullets = 60, FLOAT minXrange = -3.9, FLOAT maxXrange = -1.0, FLOAT minYRange = -2.9, FLOAT maxYrange = 3.9)
    //Fire bullets at the player as from the bad  guy...
    INT currentBulletTime = GET_GAME_TIMER()
    VECTOR bulletHit
    VECTOR bulletOrigin
    
    IF ((currentBulletTime - iControlTimer) > timeBetweenBullets)
        IF NOT IS_ENTITY_DEAD(pedToMiss)
        AND NOT IS_ENTITY_DEAD(sourceOfBullets)            
            bulletHit = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedToMiss, <<GET_RANDOM_FLOAT_IN_RANGE(minXrange, maxXrange), GET_RANDOM_FLOAT_IN_RANGE(minYRange, maxYrange), 0.0>>)
            bulletOrigin = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(sourceOfBullets, sourceOffset)
            GET_GROUND_Z_FOR_3D_COORD(bulletHit, bulletHit.z)           
            SHOOT_SINGLE_BULLET_BETWEEN_COORDS(bulletOrigin, bulletHit, 1)
            iControlTimer = currentBulletTime
                  
            #IF IS_DEBUG_BUILD
			  	IF bIsSuperDebugEnabled
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
              		DRAW_DEBUG_SPHERE(bulletOrigin, 0.2)
               		DRAW_DEBUG_SPHERE(bulletHit, 0.2)
				ENDIF
			#ENDIF
        ELSE
                  //PRINTSTRING("SHOOTER DEAD")
                  //PRINTNL()
            ENDIF
    ELSE
            //PRINTSTRING("TIMER NOT READY")
            //PRINTNL()
      ENDIF
ENDPROC

PROC REQUEST_TRAIN_MODELS_1()

	REQUEST_MODEL(FREIGHTGRAIN)
	REQUEST_MODEL(TANKERCAR)
	REQUEST_MODEL(FREIGHTCAR)
					
ENDPROC

PROC REQUEST_TRAIN_MODELS_2()

	REQUEST_MODEL(FREIGHT)
	REQUEST_MODEL(FREIGHTCONT1)
	REQUEST_MODEL(FREIGHTCONT2)
					
ENDPROC


FUNC  BOOL HAVE_TRAIN_MODELS_LOADED()

	IF HAS_MODEL_LOADED(FREIGHT)
	AND HAS_MODEL_LOADED(FREIGHTCONT1)
	AND HAS_MODEL_LOADED(FREIGHTCONT2)
	//AND HAS_MODEL_LOADED(FREIGHTCRATE)
	AND HAS_MODEL_LOADED(FREIGHTGRAIN)
	AND HAS_MODEL_LOADED(TANKERCAR)
	AND HAS_MODEL_LOADED(FREIGHTCAR)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

INT iTimeToGetRidOfTrain

PROC MANAGE_TRAIN()

	SWITCH iHeliCrashTrainStage
		
		CASE 0
			IF NOT IS_ENTITY_DEAD(vehBike[0])
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0])
					IF GET_TIME_POSITION_IN_RECORDING(vehBike[0]) > 180763.700
						REQUEST_TRAIN_MODELS_1()
						iHeliCrashTrainStage++
					ENDIF
				ENDIF
			ENDIF
		BREAK
			
		CASE 1
			IF NOT IS_ENTITY_DEAD(vehBike[0])
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0])
					IF GET_TIME_POSITION_IN_RECORDING(vehBike[0]) > 190763.700
						REQUEST_TRAIN_MODELS_2()
						iHeliCrashTrainStage++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			//Wait for train to 
			IF NOT IS_ENTITY_DEAD(vehBike[0])
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0])
					IF GET_TIME_POSITION_IN_RECORDING(vehBike[0]) > 198763.700
						iHeliCrashTrainStage++
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		CASE 3
			IF HAVE_TRAIN_MODELS_LOADED()
				SET_RANDOM_TRAINS(FALSE) 
				heliCrashTrain = CREATE_MISSION_TRAIN(2,  << 542.7376, -1536.9722, 21.3978 >>, TRUE)
				SET_TRAIN_CRUISE_SPEED(heliCrashTrain, 18.0)
				SET_TRAIN_SPEED(heliCrashTrain, 18.0)
				iHeliCrashTrainStage++		
			ENDIF
		BREAK
		
		CASE 4
				
			mainPlaybackSpeed =	fChaseSpeed
		
			IF NOT IS_ENTITY_DEAD(heliCrashTrain)
				SET_TRAIN_SPEED(heliCrashTrain, 18.0 * mainPlaybackSpeed)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(vehBike[0])
				IF IS_ENTITY_IN_ANGLED_AREA(vehBike[0], <<647.327393,-1292.161865,19.982584>>, <<565.913818,-1380.371704,28.395435>>, 15.000000)
//					STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[0])
//					ADD_EXPLOSION(GET_ENTITY_COORDS(vehBike[0]), EXP_TAG_CAR, 1.0, TRUE, FALSE, 1.2)
//					EXPLODE_VEHICLE(vehBike[0])
//					EXPLODE_VEHICLE(vehBike[0])
//					
//					SET_VEHICLE_AS_NO_LONGER_NEEDED(vehBike[0])
//					SET_MODEL_AS_NO_LONGER_NEEDED(POLMAV)
					
					iTimeToGetRidOfTrain = GET_GAME_TIMER()
					iHeliCrashTrainStage++
				ENDIF
			ENDIF
		BREAK
		
		CASE 5
			IF (GET_GAME_TIMER() - iTimeToGetRidOfTrain) > 5000
				SET_MISSION_TRAIN_AS_NO_LONGER_NEEDED(heliCrashTrain)
				SET_TRAIN_MODELS_AS_NO_LONGER_NEEDED()
				iHeliCrashTrainStage++
			ENDIF
		BREAK
				
	ENDSWITCH


ENDPROC


//BOOL bHaveBeenRemindedAboutGodTextMichael
//BOOL bHaveBeenRemindedAboutGodTextFranklin
//BLIP_INDEX blipTruck
//BOOL bOnlyAddBlipOnce = FALSE

PROC LA_RIVER_MANAGE_MICHAEL_SECTION_BLIPS()

	IF IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehTruck)
		//REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipTruck)
		IF NOT DOES_BLIP_EXIST(blipCrew[0])
			blipCrew[0] = CREATE_BLIP_FOR_PED(pedCrew[0])
		ENDIF
		IF NOT bWorstGunDies
			IF NOT DOES_BLIP_EXIST(blipCrew[1])
				blipCrew[1] = CREATE_BLIP_FOR_PED(pedCrew[1])
			ENDIF
		ENDIF
		IF NOT DOES_BLIP_EXIST(blipCrew[2])
			blipCrew[2] = CREATE_BLIP_FOR_PED(GET_SELECT_PED(SEL_FRANKLIN))
		ENDIF
		//bOnlyAddBlipOnce = FALSE
	ELSE
	
//		IF NOT DOES_BLIP_EXIST(blipTruck)
//			IF bHaveBeenRemindedAboutGodTextMichael = FALSE
//				runGodText(Asset.godGetBackInTruck, TRUE, TRUE, TRUE)
//				bHaveBeenRemindedAboutGodTextMichael = TRUE
//			ENDIF
////			IF bOnlyAddBlipOnce = FALSE
////				blipTruck = CREATE_BLIP_FOR_VEHICLE(vehTruck)
////				bOnlyAddBlipOnce = TRUE
////			ENDIF
//		ENDIF
		
		REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipCrew[0])
		IF NOT bWorstGunDies
			REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipCrew[1])
		ENDIF
		REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipCrew[2])
	ENDIF
	
ENDPROC

PROC DO_COLLATERAL_DAMAGE_SPEECH()

	IF NOT IS_ENTITY_DEAD(vehTruck)
		IF NOT bWorstGunDies
			IF NOT IS_ENTITY_DEAD(vehBike[1])
				//Only run the check if close
				IF IS_ENTITY_AT_ENTITY(pedCrew[CREW_GUNMAN_NEW], GET_SELECT_PED(SEL_MICHAEL), <<20.0, 20.0, 20.0>>, FALSE, FALSE)
					IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCrew[CREW_GUNMAN_NEW], GET_SELECT_PED(SEL_MICHAEL))
					OR IS_ENTITY_TOUCHING_ENTITY(vehTruck, vehBike[1])
						IF NOT bEventFlag[0]
							RunConversation(Asset.convCollateral, TRUE)
								//Franklin:	Watch the collateral damage, boss.
							CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedCrew[CREW_GUNMAN_NEW])
							
							SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedCrew[CREW_GUNMAN_NEW], KNOCKOFFVEHICLE_EASY)
							KNOCK_PED_OFF_VEHICLE(pedCrew[CREW_GUNMAN_NEW])
							STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[1])
							SET_ENTITY_HEALTH(pedCrew[CREW_GUNMAN_NEW], 0)
							
							bEventFlag[0] = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NOT IS_ENTITY_DEAD(vehBike[0])
			IF IS_ENTITY_AT_ENTITY(pedCrew[CREW_DRIVER_NEW], GET_SELECT_PED(SEL_MICHAEL), <<20.0, 20.0, 20.0>>, FALSE, FALSE)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(pedCrew[CREW_DRIVER_NEW], GET_SELECT_PED(SEL_MICHAEL))
				OR IS_ENTITY_TOUCHING_ENTITY(vehTruck, vehBike[0])
					IF NOT bEventFlag[1]
						RunConversation(Asset.convCollateral, TRUE)
							//Franklin:	Watch the collateral damage, boss.
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(pedCrew[CREW_DRIVER_NEW])
						
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(pedCrew[CREW_DRIVER_NEW], KNOCKOFFVEHICLE_EASY)
						KNOCK_PED_OFF_VEHICLE(pedCrew[CREW_DRIVER_NEW])
						STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[0])
						SET_ENTITY_HEALTH(pedCrew[CREW_DRIVER_NEW], 0)
						
						bEventFlag[1] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		IF NOT IS_ENTITY_DEAD(vehBike[2])
			IF IS_ENTITY_AT_ENTITY(GET_SELECT_PED(SEL_FRANKLIN), GET_SELECT_PED(SEL_MICHAEL), <<20.0, 20.0, 20.0>>, FALSE, FALSE)
				IF HAS_ENTITY_BEEN_DAMAGED_BY_ENTITY(GET_SELECT_PED(SEL_FRANKLIN), GET_SELECT_PED(SEL_MICHAEL))
				OR IS_ENTITY_TOUCHING_ENTITY(vehTruck, vehBike[2])
					IF NOT bEventFlag[2]
						RunConversation(Asset.convCollateral, TRUE)
							//Franklin:	Watch the collateral damage, boss.
						CLEAR_ENTITY_LAST_DAMAGE_ENTITY(GET_SELECT_PED(SEL_FRANKLIN))
						
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), KNOCKOFFVEHICLE_EASY)
						KNOCK_PED_OFF_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN))
						STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[2])
						APPLY_DAMAGE_TO_PED(GET_SELECT_PED(SEL_FRANKLIN), 500, TRUE)
						
						bEventFlag[2] = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC


PROC DRIVEBY_RANDOM_BIKE(PED_INDEX thisPed)
	INT iRandomBikeTarget
	iRandomBikeTarget = GET_RANDOM_INT_IN_RANGE(0, 3)
	IF bWorstGunDies AND (iRandomBikeTarget = 1)
		iRandomBikeTarget = 2
	ENDIF
	IF NOT IS_ENTITY_DEAD(vehBike[iRandomBikeTarget])
	AND NOT IS_PED_INJURED(thisPed)
		TASK_DRIVE_BY(thisPed, NULL, vehBike[iRandomBikeTarget], <<0.0, 0.0, 0.0>>, 300.0, 100, TRUE)
	ENDIF
	
ENDPROC

FUNC BOOL IS_PLAYER_PUSHING_ANALOGUE_STICKS()

	INT padLX, padLy, padRX, padRy

	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS(padLX, padLy, padRX, padRy)
	
	IF ABSI(padLX) > 75
	OR ABSI(padLY) > 75
	OR IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL, INPUT_AIM)
	
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL IS_PLAYER_ON_RIGHT_OF_RIVER(ENTITY_INDEX thisEntity)

	//IF IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<570.978699,-625.323059,23.168709>>, <<563.492920,-1367.056274,8.639765>>, 40.250000)
	IF IS_ENTITY_IN_ANGLED_AREA(thisEntity, <<893.955505,-388.957184,33.313915>>, <<765.800964,-419.565338,19.336096>>, 25.750000)
	OR IS_ENTITY_IN_ANGLED_AREA(thisEntity, <<623.363953,-511.027740,13.724644>>, <<732.422424,-403.315308,35.580723>>, 46.250000)
	OR IS_ENTITY_IN_ANGLED_AREA(thisEntity, <<623.363953,-511.027740,11.974644>>, <<569.468201,-648.631348,27.654888>>, 46.250000)
	OR IS_ENTITY_IN_ANGLED_AREA(thisEntity, <<575.302917,-612.522217,7.281809>>, <<562.247437,-1278.585205,23.742533>>, 46.250000)
	OR IS_ENTITY_IN_ANGLED_AREA(thisEntity, <<653.665527,-1606.389038,2.708879>>, <<562.247437,-1278.585205,23.742533>>, 46.250000)
	OR IS_ENTITY_IN_ANGLED_AREA(thisEntity, <<653.665527,-1606.389038,2.708879>>, <<634.500793,-1797.157593,25.875072>>, 46.250000)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC

//Author: Ross Wallace
//PURPOSE: Returns the percentage progress of a car recording...
FUNC FLOAT GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(VEHICLE_INDEX thisVehicle) //, STRING RecName)
//	#IF NOT IS_DEBUG_BUILD	
//	RecName = RecName
//	#ENDIF
		
	IF NOT IS_ENTITY_DEAD(thisVehicle)
		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(thisVehicle)
			
			RECORDING_ID rID = GET_CURRENT_PLAYBACK_FOR_VEHICLE(thisVehicle)
									
			#IF IS_DEBUG_BUILD
				//INT recordingNumber
				
				//INT rePercentage = ROUND( 100.0 * ( GET_TIME_POSITION_IN_RECORDING(thisVehicle) / GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(rID) ))
//				TEXT_LABEL_63 debugName = RecName
//				TEXT_LABEL_3 tlColon = ":"
//				debugName += recordingNumber
//				debugName += tlColon
//				debugName += rePercentage
//				SET_VEHICLE_NAME_DEBUG(thisVehicle, debugName)					
			#ENDIF
			
			RETURN ( 100.0 * ( GET_TIME_POSITION_IN_RECORDING(thisVehicle) / GET_TOTAL_DURATION_OF_VEHICLE_RECORDING_ID(rID) ))// GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(recordingNumber, RecName) ) )

		ELSE
			PRINTSTRING("GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS: PLayback not going on!!!!!")
		ENDIF	
	ELSE
		PRINTSTRING("GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS: Vehicle dead!!!!!")
		RETURN -1.0
	ENDIF
	
	//SCRIPT_ASSERT("GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS: something went wrong!")
	//Fail safe as this indicates recording finished (though entity is likely dead)
	RETURN -1.0
	
ENDFUNC

FUNC BOOL SETUP_CUSTOM_SWITCH_SPLINE()

	IF NOT sCamDetails.bSplineCreated
		
		IF NOT DOES_CAM_EXIST(sCamDetails.camID)
			sCamDetails.camID = CREATE_CAM("DEFAULT_SPLINE_CAMERA", FALSE) 
		ENDIF                                                 
		
		// Initial camera behind the player       
		ADD_CAM_SPLINE_NODE(sCamDetails.camID, GET_GAMEPLAY_CAM_COORD(), GET_GAMEPLAY_CAM_ROT(), 0)    
		ADD_CAM_SPLINE_NODE(sCamDetails.camID, <<1043.030884,-290.804352,50.065514>>,<<7.864205,-5.595765,-90.506111>>, 800)
		ADD_CAM_SPLINE_NODE(sCamDetails.camID, <<1053.119141,-292.921692,51.005157>>,<<-0.327616,5.185540,-109.932098>>, 1100)
		
		//ADD_CAM_SPLINE_NODE(sCamDetails.camID, <<1055.728027,-291.181427,52.447994>>,<<-4.723970,-0.496480,169.219147>>, 600)
		ADD_CAM_SPLINE_NODE(sCamDetails.camID, <<1063.485474,-284.776703,55.845688>>,<<-9.688592,8.164109,151.438278>>, 1300)
		ADD_CAM_SPLINE_NODE_USING_GAMEPLAY_FRAME(sCamDetails.camID, 1000)
			
		set_cam_fov(sCamDetails.camID, get_final_rendered_cam_fov())

		sCamDetails.bSplineCreated = TRUE
		sCamDetails.camType = selector_cam_long_spline//SELECTOR_CAM_STRAIGHT_INTERP
		
		sCamDetails.pedTo = sSelectorPeds.pedID[SELECTOR_PED_MICHAEL] //ped that the spline camera is going to

		RETURN TRUE
		
	ENDIF 
	
	RETURN FALSE
	 
ENDFUNC

INT iTimeOfLastRiverDialogue
FLOAT fLastEngineHealth
CAM_VIEW_MODE playersPreviousCameraview
INT iRandomBike
INT iHackerDriveByTimer
VECTOR vBikeRotation


INT iCreateCopsInStormDrainStage

PROC CREATE_CARS_AND_COPS_IN_LS_RIVER()

	//Split up creation to avoid spikes in CPU usage.
	//If we've reach the first round of cops.... create some cop peds	
	SWITCH iCreateCopsInStormDrainStage
	
		CASE 0
			IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_MICHAEL), <<680.7623, -528.5031, 14.2773>>, <<105.0, 105.0, 100.0>>)	
				REQUEST_MODEL(PRIMO)
				iCreateCopsInStormDrainStage++
			ENDIF
		BREAK
		
		CASE 1
			copOnLeft1 = CREATE_PED(PEDTYPE_COP, Asset.mnPoliceman, <<680.7623, -528.5031, 14.2773>>, 112.5884)
			SET_PED_SPHERE_DEFENSIVE_AREA(copOnLeft1, <<680.7623, -528.5031, 14.2773>>, 2.0)
			SET_PED_COP(copOnLeft1, FALSE)		
			TASK_COMBAT_HATED_TARGETS_IN_AREA(copOnLeft1, <<680.7623, -528.5031, 14.2773>>, 200.0)
			iCreateCopsInStormDrainStage++
		BREAK
		
		CASE 2
			copOnLeft2 = CREATE_PED(PEDTYPE_COP, Asset.mnPoliceman, <<683.2385, -529.8103, 14.3534>>, 172.5926)
			SET_PED_SPHERE_DEFENSIVE_AREA(copOnLeft2, <<683.2385, -529.8103, 14.3534>>, 2.0)
			SET_PED_COP(copOnLeft2, FALSE)		
			TASK_COMBAT_HATED_TARGETS_IN_AREA(copOnLeft2, <<680.7623, -528.5031, 14.2773>>, 200.0)
			iCreateCopsInStormDrainStage++
		BREAK
	
		CASE 3
			copOnLeft3 = CREATE_PED(PEDTYPE_COP, Asset.mnPoliceman, <<668.7509, -522.4036, 14.2263>>, 66.0000)
			SET_PED_SPHERE_DEFENSIVE_AREA(copOnLeft3, <<668.7509, -522.4036, 14.2263>>, 2.0)	
			SET_PED_COP(copOnLeft3, FALSE)		
			TASK_COMBAT_HATED_TARGETS_IN_AREA(copOnLeft3, <<680.7623, -528.5031, 14.2773>>, 200.0)
			iCreateCopsInStormDrainStage++
		BREAK
		
		CASE 4
			IF HAS_MODEL_LOADED(PRIMO)
				burnedWreck1 = CREATE_VEHICLE(PRIMO, <<575.9414, -1103.2699, 9.1990>>, 227.2179)
				EXPLODE_VEHICLE(burnedWreck1)
				iCreateCopsInStormDrainStage++
			ENDIF
		BREAK
	
		CASE 5
			burnedWreck2 = CREATE_VEHICLE(PRIMO,  <<565.9907, -1116.2458, 9.1632>>, 303.9077 )
			EXPLODE_VEHICLE(burnedWreck2)							
			iCreateCopsInStormDrainStage++
		BREAK
	
		CASE 6
			//If we've reached the second round of cops ... create some peds.
			IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_MICHAEL), <<581.0883, -840.3623, 9.9252>>, <<100.0, 100.0, 100.0>>)
				SET_PED_AS_NO_LONGER_NEEDED(copOnLeft1)
				SET_PED_AS_NO_LONGER_NEEDED(copOnLeft2)
				SET_PED_AS_NO_LONGER_NEEDED(copOnLeft3)
		
				SET_MODEL_AS_NO_LONGER_NEEDED(PRIMO)
				
				copOnRight1 = CREATE_PED(PEDTYPE_COP, Asset.mnPoliceman, <<581.0883, -840.3623, 9.9252>>, 333.9399)
				SET_PED_SPHERE_DEFENSIVE_AREA(copOnRight1, <<581.0883, -840.3623, 9.9252>>, 2.0)
				SET_PED_COP(copOnRight1, FALSE)		
				TASK_COMBAT_HATED_TARGETS_IN_AREA(copOnRight1, <<581.0883, -840.3623, 9.9252>>, 200.0)
				iCreateCopsInStormDrainStage++
			ENDIF
		BREAK
		
		CASE 7			
			copOnRight2 = CREATE_PED(PEDTYPE_COP, Asset.mnPoliceman, <<575.5164, -839.3569, 9.9279>>, 348.5494)
			SET_PED_SPHERE_DEFENSIVE_AREA(copOnRight2, <<575.5164, -839.3569, 9.9279>>, 2.0)	
			SET_PED_COP(copOnRight2, FALSE)
			TASK_COMBAT_HATED_TARGETS_IN_AREA(copOnRight2, <<581.0883, -840.3623, 9.9252>>, 200.0)
			iCreateCopsInStormDrainStage++
		BREAK
		
		CASE 8
			copOnRight3 = CREATE_PED(PEDTYPE_COP, Asset.mnPoliceman, <<571.9435, -839.0884, 9.9286>>, 356.4424)
			SET_PED_SPHERE_DEFENSIVE_AREA(copOnRight3, <<571.9435, -839.0884, 9.9286>>, 2.0)	
			SET_PED_COP(copOnRight3, FALSE)	
			TASK_COMBAT_HATED_TARGETS_IN_AREA(copOnRight3, <<581.0883, -840.3623, 9.9252>>, 200.0)
			iCreateCopsInStormDrainStage++
		BREAK
	
	ENDSWITCH

ENDPROC

BOOL bAlternateRiverLines

//PURPOSE:		Performs the section where the crew drive bikes into a moving truck
FUNC BOOL stageTruckFightNEW()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
		
		DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, TRUE)
		DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, TRUE)
		
		SET_FOLLOW_VEHICLE_CAM_HIGH_ANGLE_MODE_EVERY_UPDATE(TRUE, TRUE)
		
		SET_PARTICLE_FX_BULLET_IMPACT_LODRANGE_SCALE(2.0)
		SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", TRUE)
		sCamDetails.bPedSwitched = FALSE
		RESET_NEED_LIST()
		
		//Request things without request system.
		REQUEST_TRUCK_FIGHT_ASSETS()
		
		WHILE NOT HAVE_TRUCK_FIGHT_ASSETS_LOADED()				//Possibly delay here causing 1816106
			safeWait(0)
		ENDWHILE
				
//		SetPieceCarID[0] = vehBike[1]
//		SetPieceCarProgress[0] = 3
		
		IF NOT DOES_ENTITY_EXIST(vehTruck)
			CREATE_TRUCK_FIGHT_TRUCK()
		ENDIF
		
		IF NOT DOES_ENTITY_EXIST(GET_SELECT_PED(SEL_MICHAEL))
		OR IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
			WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_MICHAEL], CHAR_MICHAEL, vMichaelTruckCoords, fMichaelTruckHeading)
				safeWait(0)	//Possibly delay here causing 1816106
			ENDWHILE
		ELSE
			SET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL), vMichaelTruckCoords)
			SET_ENTITY_HEADING(GET_SELECT_PED(SEL_MICHAEL), fMichaelTruckHeading)
			SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehTruck)
		ENDIF
		
		IF NOT IS_ENTITY_DEAD(vehBike[0])
			IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0])
				
				CREATE_BIKES_IF_THEY_DONT_EXIST()
				
				LOAD_AND_MANAGE_UBER_ARRAY()

				#IF IS_DEBUG_BUILD
					set_uber_parent_widget_group(widgetDebug)
				#ENDIF
							
				INITIALISE_UBER_PLAYBACK(Asset.recUber, Asset.recNumber, TRUE)
				
				LOAD_AND_MANAGE_UBER_ARRAY()
				
				SetPieceCarID[0] = vehBike[1]
				SetPieceCarProgress[0] = 3	
				
//				START_PLAYBACK_RECORDED_VEHICLE(vehBike[0], Asset.recNumber, Asset.recUber)
//				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[0], 155000)//168553)
//				SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehBike[0], vBatiRecOffset)
//				
//				IF (NOT bWorstGunDies)
//					FREEZE_ENTITY_POSITION(vehBike[1], FALSE)
//					START_PLAYBACK_RECORDED_VEHICLE(vehBike[1], SetPieceCarRecording[0], Asset.recUber)
//					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[1], 155000)
//					SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehBike[1], vBatiRecOffset)
//					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike[1])
//				ENDIF
				
												
				fChaseSpeed = 1.0
				START_PLAYBACK_RECORDED_VEHICLE(vehBike[0], Asset.recNumber, Asset.recUber)
		
				IF (NOT bWorstGunDies)
					FREEZE_ENTITY_POSITION(vehBike[1], FALSE)
					START_PLAYBACK_RECORDED_VEHICLE(vehBike[1], SetPieceCarRecording[0], Asset.recUber)
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[1], 157000)
					SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehBike[1], vBatiRecOffset)
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike[1])
				ENDIF
				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[0], 157000)
				SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehBike[0], vBatiRecOffset)
				FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehBike[0])
//				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(cameraCar, 153000)
//				SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(cameraCarCamera, 153000)
					
						
				SET_UBER_PLAYBACK_TO_TIME_NOW(vehBike[0], 157000) //168553) //163553
				CREATE_ALL_WAITING_UBER_CARS()		
				UPDATE_UBER_PLAYBACK(vehBike[0], 1.0)
				
//				SET_PLAYBACK_SPEED(vehBike[0], 0.0)
//				SET_PLAYBACK_SPEED(vehBike[1], 0.0)
				
				safeWait(0)
				CREATE_ALL_WAITING_UBER_CARS()	
			ENDIF
		ENDIF
				
		SETUP_PLAYER_PEDS_SUITS(FALSE, TRUE, FALSE)
		SET_PED_BAG(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
		
		IF NOT DOES_ENTITY_EXIST(pedCrew[CREW_HACKER])
		OR IS_PED_INJURED(pedCrew[CREW_HACKER])
			pedCrew[CREW_HACKER] = CREATE_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,  ENUM_TO_INT(CREW_HACKER), vCrewTruckCoords, fCrewTruckHeading)
			SET_MODEL_FOR_CREW_MEMBER_AS_NO_LONGER_NEEDED(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,  ENUM_TO_INT(CREW_HACKER)))
			GIVE_WEAPON_TO_PED(pedCrew[CREW_HACKER], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
		ELSE
			SET_ENTITY_COORDS(pedCrew[CREW_HACKER], vCrewTruckCoords)
			SET_ENTITY_HEADING(pedCrew[CREW_HACKER], fCrewTruckHeading)
			GIVE_WEAPON_TO_PED(pedCrew[CREW_HACKER], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
		ENDIF
		
		ADD_ALL_PEDS_TO_DIALOGUE()
		
		// url:bugstar:2078914
		SWITCH GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)) 
			CASE CM_HACKER_G_PAIGE
				SAFE_ADD_PED_FOR_DIALOGUE(pedCrew[CREW_HACKER], 7, "PAIGE")
			BREAK
		
			CASE CM_HACKER_M_CHRIS
				SAFE_ADD_PED_FOR_DIALOGUE(pedCrew[CREW_HACKER], 7, "CHRISTIAN")
			BREAK
			
			CASE CM_HACKER_B_RICKIE_UNLOCK
				SAFE_ADD_PED_FOR_DIALOGUE(pedCrew[CREW_HACKER], 7, "LIEngineer")
			BREAK
		ENDSWITCH
						
		SET_PED_GET_OUT_UPSIDE_DOWN_VEHICLE(pedCrew[CREW_HACKER], FALSE)
		CLEAR_PED_TASKS_IMMEDIATELY(pedCrew[CREW_HACKER])
		SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCrew[CREW_HACKER], TRUE)
		SET_PED_NAME_DEBUG(pedCrew[CREW_HACKER],"Driver")
		
		IF IS_VEHICLE_DRIVEABLE(vehTruck)				
			IF NOT IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehTruck)
				SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehTruck, VS_DRIVER)
			ENDIF
			IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_HACKER], vehTruck)
				SET_PED_INTO_VEHICLE(pedCrew[CREW_HACKER], vehTruck, VS_FRONT_RIGHT)
			ENDIF
		ENDIF
		
		GIVE_WEAPON_TO_PED(pedCrew[CREW_HACKER], WEAPONTYPE_PISTOL, INFINITE_AMMO, TRUE, TRUE)
				
		IF NOT IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
			SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2], VS_DRIVER)
		ENDIF
		
		SET_RANDOM_TRAINS(FALSE)

		bCameraShotSetup = FALSE
		bStageSetup = TRUE
		
		//bWarpedOntoRec = FALSE
		bCaughtUp	= FALSE
		SET_ENTITY_COLLISION(vehTruck, TRUE)
					
		REMOVE_WEAPON_ASSET(WEAPONTYPE_PISTOL)

		IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
			SET_PED_CREW(GET_SELECT_PED(SEL_MICHAEL))
			SET_PED_CREW(GET_SELECT_PED(SEL_FRANKLIN))
		ENDIF
				
		RESET_NEED_LIST()
		MANAGE_LOADING_NEED_LIST()
		
		GIVE_WEAPON_TO_PED(GET_SELECT_PED(SEL_MICHAEL), WEAPONTYPE_PISTOL, 200, TRUE)
				
		//SETUP_CREW_SUITS()
	
		FOR iCounter = 0 TO NUMBER_OF_EVENT_FLAGS-1
			bEventFlag[iCounter] = FALSE
		ENDFOR

		iHeliCrashTrainStage = 0
				
		SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
		
		SWITCH_ALL_RANDOM_CARS_OFF()
		
		SETTIMERA(0)
		
		bDrawCashScaleform = FALSE
		
//		SAFE_FADE_IN()
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
		iStageSection = 0
		bStageSetup = TRUE
		
		iRiverDialogue = 0
		iRiverDialogueSide = 0
		
		//Reposition Bikes
	
		FOR iCounter = 0 TO 2
			IF DOES_ENTITY_EXIST(vehBike[iCounter])
				IF NOT IS_ENTITY_DEAD(vehBike[iCounter])
					SET_VEHICLE_COLOUR_COMBINATION(vehBike[iCounter], iCounter)
					SET_VEHICLE_ENGINE_ON(vehBike[iCounter], TRUE, TRUE)					
				ENDIF
			ENDIF
		ENDFOR
					
		REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipBike)

		SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), KNOCKOFFVEHICLE_NEVER)
		SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
		
		fRecTime = 0.0
			
		//bHaveBeenRemindedAboutGodTextMichael = FALSE
		
		allow_veh_to_stop_on_PLAYER_impact = FALSE	//	url:bugstar:2096432
		allow_veh_to_stop_on_any_veh_impact = TRUE
		
		iStageSection = 0
		bShowMichaelHelp = FALSE
		bEndOfLARiverReached = FALSE
			
		eSwitchCamState = SWITCH_CAM_START_SPLINE1
		
		bSwitchedRecsToAi = FALSE
		iStageSection = 1
//═════════╡ UPDATE ╞═════════		
	ELSE	
			
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
			
		VEHICLE_INDEX rubberBandVehicle
		PED_INDEX tempPed, tempPed2
		MANAGE_TRAIN()
		
		INT iPCounter
				
//		#IF IS_DEBUG_BUILD
//			IF bIsSuperDebugEnabled
//				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
//      			DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(GET_TRAILING_VEHICLE()), 1.0)
//				DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(GET_LEADING_VEHICLE())+ <<0.0, 1.0, 0.0>>, 1.0, 255, 0, 0)
//			ENDIF
//		#ENDIF

		
		IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[2])
			IF NOT IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
				SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2], VS_DRIVER)
			ENDIF
		ENDIF
		
		IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
			SET_PED_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0], VS_DRIVER)
		ENDIF


		#IF IS_DEBUG_BUILD 
			IF bIsSuperDebugEnabled
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(vehBike[0], GET_STRING_FROM_FLOAT(GET_VEHICLE_ENGINE_HEALTH(vehBike[0])), -0.5, 128, 128, 0)
				IF DOES_ENTITY_EXIST(vehBike[1])
					DRAW_DEBUG_TEXT_ABOVE_ENTITY(vehBike[1], GET_STRING_FROM_FLOAT(GET_VEHICLE_ENGINE_HEALTH(vehBike[1])), -0.5, 128, 128, 0)
				ENDIF
				DRAW_DEBUG_TEXT_ABOVE_ENTITY(vehBike[2], GET_STRING_FROM_FLOAT(GET_VEHICLE_ENGINE_HEALTH(vehBike[2])), -0.5, 128, 128, 0)
			ENDIF
		#ENDIF
				
		CONTROL_VEHICLE_CHASE_HINT_CAM_IN_VEHICLE(localChaseHintCamStruct, vehBike[2])
				
		SWITCH iStageSection
				
			//being franklin case
			CASE 1	
				IF bShowMichaelHelp = FALSE
				
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
					emptyTextQueue()
					clearText()
							
					SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_MICHAEL, TRUE)
					SET_SELECTOR_PED_HINT(sSelectorPeds, SELECTOR_PED_FRANKLIN, FALSE)
					SETTIMERA(0)
					sCamDetails.pedTo = GET_SELECT_PED(SEL_MICHAEL)
				
					START_AUDIO_SCENE("JSH_2B_CHASE_EXIT_TUNNELS")
					
					//Possible fix for bug 1816106
//					IF fRecTime < 157632.300
//						fMichaelTruckRecordingSkipTime = 3250
//					ENDIF
					
					START_MICHAEL_TRUCK_RECORDING(0.975)
					START_HACKER_SHOOTING()
					
					//SETUP_CUSTOM_SWITCH_SPLINE()

					IF NOT IS_ENTITY_DEAD(pedCrew[CREW_HACKER])
					AND NOT IS_ENTITY_DEAD(SetPieceCarID[0])
						TASK_DRIVE_BY(pedCrew[CREW_HACKER], NULL, SetPieceCarID[0], <<0.0, -5.0, 0.0>>, 200.0, 100, FALSE)
					ENDIF
				
					bShowMichaelHelp = TRUE
				ENDIF
															
				//Do rubber band off the truck to stop it colliding with the cops
				//Update playback speeds...
				fChaseSpeed = 1.0//CONTROL_PLAYBACK_SPEED(vehTruck, -7.0)
				
				SET_PLAYBACK_SPEED(vehBike[0], fChaseSpeed)						// +++ Vehicle speed set +++
				UPDATE_UBER_PLAYBACK(vehBike[0], fChaseSpeed)
															
				DO_COLLATERAL_DAMAGE_SPEECH()
				
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_EXIT)
				
				//RUN_CAM_SPLINE_FROM_PLAYER_TO_PED(sCamDetails, 2000, 2000, SELECTOR_CAM_STRAIGHT_INTERP, 1000)
				IF HANDLE_SWITCH_CAM(scsSwitchCam) 
					sCamDetails.bPedSwitched = TRUE
				ENDIF
				
				IF sCamDetails.bPedSwitched
					
					REMOVE_ANIM_DICT(strAnimDictCamShake)
					
					DISPLAY_RADAR(TRUE)
					DISPLAY_HUD(TRUE)
					
					DESTROY_CAM(scsSwitchCam.ciSpline)
					
					playersPreviousCameraview = GET_FOLLOW_VEHICLE_CAM_VIEW_MODE()
					IF GET_FOLLOW_VEHICLE_CAM_ZOOM_LEVEL() <> VEHICLE_ZOOM_LEVEL_BONNET
						SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(CAM_VIEW_MODE_THIRD_PERSON_FAR)
					ENDIF
					
					STOP_AUDIO_SCENE("JSH_2B_CHASE_EXIT_TUNNELS")

					START_AUDIO_SCENE("JSH_2B_RAM_POLICE")

					bShowMichaelHelp = FALSE
			
					SETUP_WANTED_LEVEL()
					
					SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), KNOCKOFFVEHICLE_NEVER)
					IF NOT IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
						SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
					ENDIF
					
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[2])
						STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[2])
					ENDIF
					
					START_PLAYBACK_RECORDED_VEHICLE(vehBike[2], 700 ,"JHUBER") //xyz
					SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehBike[2], GET_TIME_POSITION_IN_RECORDING(vehBike[0]) - (SetPieceCarStartime[15]))
					
					IF NOT IS_ENTITY_DEAD(vehBike[2])
						SET_ENTITY_INVINCIBLE(vehBike[2], FALSE)
					ENDIF
					
					//SET_PLAYBACK_TO_USE_AI_TRY_TO_REVERT_BACK_LATER(vehBike[2],4000)
					IF DOES_ENTITY_EXIST(oiTruckBuddyFakeWeapon)
						DELETE_OBJECT(oiTruckBuddyFakeWeapon)
					ENDIF
					
					IF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)) <> CM_HACKER_B_RICKIE_UNLOCK
						IF NOT IS_ENTITY_DEAD(pedCrew[CREW_HACKER])
						AND NOT IS_ENTITY_DEAD(SetPieceCarID[17])
							TASK_DRIVE_BY(pedCrew[CREW_HACKER], null, SetPieceCarID[17], (<<0.0, 0.0, 0.0>>), 2000, 75, FALSE, FIRING_PATTERN_BURST_FIRE_DRIVEBY)
						ENDIF
					ENDIF
					
					//IF  GET_POSITION_IN_RECORDING(vehBike[2]) + 158000 <  GET_POSITION_IN_RECORDING(vehBike[0])
					SET_VEHICLE_ENGINE_HEALTH(vehBike[2], 100.00)
					
					SET_VEHICLE_STRONG(vehTruck, TRUE)
					SET_ENTITY_HEALTH(vehTruck, 1000)
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedCrew[CREW_HACKER], TRUE)
					
					SET_VEHICLE_TYRES_CAN_BURST(vehTruck, FALSE)
					
					IF DOES_ENTITY_EXIST(pedCrew[CREW_GUNMAN_NEW])
						SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedCrew[CREW_GUNMAN_NEW], TRUE)
					ENDIF
					SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(pedCrew[CREW_DRIVER_NEW], TRUE)
					
					SET_ENTITY_INVINCIBLE(vehBike[0], TRUE)
					IF NOT bWorstGunDies
						SET_ENTITY_INVINCIBLE(vehBike[1], TRUE)
					ENDIF
					
					SET_ENTITY_INVINCIBLE(vehBike[2], FALSE)
					
					SET_VEHICLE_FIXED(vehBike[2])
					INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehBike[2],JH2A_FRANKLIN_BIKE_DAMAGE) 
					
					PLAY_POLICE_REPORT("SCRIPTED_SCANNER_REPORT_JSH_2A_05", 0.0)
					
					fChaseSpeed = 0.7
							
					iTimeOfLastRiverDialogue = GET_GAME_TIMER()
					
					iStageSection = 2
				ENDIF
				
			BREAK
			
			//being Michael case
			CASE 2
							
				IF (IS_PLAYER_PUSHING_ANALOGUE_STICKS() OR IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_VEH_ACCELERATE))
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck)
						STOP_PLAYBACK_RECORDED_VEHICLE(vehTruck)
					ENDIF
				ENDIF
								
				iBlipCount = 0
								
				FOR iPCounter = 0 TO TOTAL_NUMBER_OF_PARKED_CARS - 1
					IF DOES_ENTITY_EXIST(ParkedCarID[iPCounter])
						HAS_VEHICLE_BEEN_FLIPPED_BY_VEHICLE(ParkedCarID[iPCounter], vehTruck, fPoliceWidth, fTruckWidth, TRUE)
					ENDIF
				ENDFOR
				
				//First cop cars are 17-18 (dont add extra driveby guy)
				FOR iCounter = 17 TO TOTAL_NUMBER_OF_SET_PIECE_CARS - 1
				
					IF DOES_ENTITY_EXIST(SetPieceCarID[iCounter])
					
						IF NOT IS_ENTITY_DEAD(SetPieceCarID[iCounter])
							IF GET_ENTITY_MODEL(SetPieceCarID[iCounter]) = POLICE3
								IF NOT DOES_BLIP_EXIST(vehPoliceBlips[iCounter])	
									IF IS_VEHICLE_DRIVEABLE(SetPieceCarID[iCounter])
									//AND GET_ENTITY_SPEED(SetPieceCarID[iCounter]) > 2.0 		
									AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[iCounter])
									AND GET_TIME_POSITION_IN_RECORDING(SetPieceCarID[iCounter]) > 3750
										
										SET_VEHICLE_ACTIVE_DURING_PLAYBACK(SetPieceCarID[iCounter], TRUE)
																				
										vehPoliceBlips[iCounter] = CREATE_BLIP_FOR_VEHICLE(SetPieceCarID[iCounter], TRUE)
										//XXX : SET_POLICE_RADAR_BLIPS(FALSE)
										tempPed = GET_PED_IN_VEHICLE_SEAT(SetPieceCarID[iCounter], VS_DRIVER)		
										IF NOT IS_PED_INJURED(tempPed)
											
											SET_PED_CONFIG_FLAG(tempPed, PCF_DontBlipCop, TRUE)
											
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed, TRUE)
											SET_PED_RELATIONSHIP_GROUP_HASH(tempPed, grpCop)
											GIVE_WEAPON_TO_PED(tempPed, WEAPONTYPE_PISTOL, INFINITE_AMMO)
											IF GET_RANDOM_BOOL()
												IF NOT IS_ENTITY_DEAD(vehBike[0])
													TASK_DRIVE_BY(tempPed, NULL, vehBike[0], <<0.0, 0.0, 0.0>>, 200.0, 75, TRUE)
												ENDIF
											ELSE
												IF NOT IS_ENTITY_DEAD(vehBike[2])
													TASK_DRIVE_BY(tempPed, NULL, vehBike[2], <<0.0, 0.0, 0.0>>, 200.0, 75, TRUE)
												ENDIF
											ENDIF
											
											SET_PED_ACCURACY(tempPed, 80)
											
											//TASK_COMBAT_HATED_TARGETS_AROUND_PED(tempPed, 200.0)
											//SET_PED_COMBAT_ATTRIBUTES(tempPed, CA_USE_VEHICLE, TRUE)
										ENDIF
										
										IF IS_VEHICLE_SEAT_FREE(SetPieceCarID[iCounter],VS_FRONT_RIGHT)
										AND iCounter <> 17
										AND iCounter <> 18	//Dont put peds in passenger seat of 17/18
										AND iCounter <> 20
											tempPed2 = CREATE_PED_INSIDE_VEHICLE(SetPieceCarID[iCounter], PEDTYPE_COP, S_M_Y_COP_01,VS_FRONT_RIGHT)
											SET_PED_CONFIG_FLAG(tempPed2, PCF_DontBlipCop, TRUE)
											SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(tempPed2, TRUE)
											GIVE_WEAPON_TO_PED(tempPed2, WEAPONTYPE_PISTOL, INFINITE_AMMO)
											SET_PED_RELATIONSHIP_GROUP_HASH(tempPed2, grpCop)
											IF GET_RANDOM_BOOL()
												IF NOT IS_ENTITY_DEAD(vehBike[0])
													TASK_DRIVE_BY(tempPed2, NULL, vehBike[0], <<0.0, 0.0, 0.0>>, 200.0, 90, FALSE)
												ENDIF
											ELSE
												IF NOT IS_ENTITY_DEAD(vehBike[2])
													TASK_DRIVE_BY(tempPed2, NULL, vehBike[2], <<0.0, 0.0, 0.0>>, 200.0, 90, FALSE)
												ENDIF
											ENDIF
											SET_PED_KEEP_TASK(tempPed2, TRUE)
											SET_PED_ACCURACY(tempPed2, 80)
											SET_PED_AS_NO_LONGER_NEEDED(tempPed2)											
										ENDIF
									ENDIF
								ELSE
									iBlipCount++
								ENDIF
						
								IF HAS_VEHICLE_BEEN_FLIPPED_BY_VEHICLE(SetPieceCarID[iCounter], vehTruck, fPoliceWidth, fTruckWidth, TRUE)
									REMOVE_BLIP_AND_CHECK_IF_EXISTS(vehPoliceBlips[iCounter])
								ENDIF
								
								//Dont kill a set piece before it has had 3 seconds to be alive
								IF fRecTime > (SetPieceCarStartime[iCounter] + 3000)
									IF NOT IS_PLAYBACK_GOING_ON_FOR_VEHICLE(SetPieceCarID[iCounter])
									//OR GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), SetPieceCarID[iCounter]) > 350
									OR (GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), vEndOfStormDrain) < GET_DISTANCE_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(SetPieceCarID[iCounter], <<0.0, 15.0, 0.0>>), vEndOfStormDrain) AND fRecTime > 214664.900)
										
										IF REMOVE_BLIP_AND_CHECK_IF_EXISTS(vehPoliceBlips[iCounter])
											IF NOT IS_ENTITY_DEAD(SetPieceCarID[iCounter])
												STOP_PLAYBACK_RECORDED_VEHICLE(SetPieceCarID[iCounter])
												SET_VEHICLE_ENGINE_HEALTH(SetPieceCarID[iCounter], 0)
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
								
				ENDFOR
				
				//Do rubber band off the leader...
				//Update playback speeds...
								
				DONT_PROCESS_UBER_PLAYBACK_HORNS_AND_LIGHTS()
								
				IF fRecTime < 187929.900
					rubberBandVehicle = GET_CLOSEST_COP_SET_PIECE(GET_CLOSEST_BIKE(), PLAYER_PED_ID())					
				ELSE
					IF NOT bWorstGunDies
						rubberBandVehicle = GET_CLOSEST_COP_SET_PIECE(GET_CLOSEST_BIKE(), vehBike[1])
					ELSE
						rubberBandVehicle = GET_CLOSEST_COP_SET_PIECE(GET_CLOSEST_BIKE(), vehBike[2])
					ENDIF
				ENDIF
				
				//task the hacker to shoot at the closest cop car every five seconds.
				IF (GET_GAME_TIMER() - iHackerDriveByTimer) > 5000
				AND iBlipCount > 0
					IF GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)) <> CM_HACKER_B_RICKIE_UNLOCK
						IF GET_ENTITY_MODEL(rubberBandVehicle) = POLICE3
							IF NOT IS_ENTITY_DEAD(pedCrew[CREW_HACKER])
							AND NOT IS_ENTITY_DEAD(rubberBandVehicle)
								TASK_DRIVE_BY(pedCrew[CREW_HACKER], NULL, rubberBandVehicle, <<0.0, -5.0, 0.0>>, 200.0, 100, FALSE)
							ENDIF
							iHackerDriveByTimer = GET_GAME_TIMER()
						ENDIF
					ENDIF
				ELSE
					CLEAR_PED_TASKS(pedCrew[CREW_HACKER])
				ENDIF	
				
				#IF IS_DEBUG_BUILD
					IF bIsSuperDebugEnabled
						SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
						DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(rubberBandVehicle), 1.5)
					ENDIF
				#ENDIF
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0])
					fRecTime = GET_TIME_POSITION_IN_RECORDING(vehBike[0])
				ENDIF
				
				IF (GET_ENTITY_MODEL(rubberBandVehicle) = Asset.mnBike)
					IF fRecTime > 0.0
						fOffsetAmount = -8.5
					ENDIF
					
					IF fRecTime > 180102.800
						fOffsetAmount = -12.75
					ENDIF
					
					IF fRecTime > 182929.900
						fOffsetAmount = -15.0
					ENDIF
					
					IF fRecTime > 188000.0
						fOffsetAmount = -16.0
					ENDIF							
					
					IF fRecTime > 201344.100
						fOffsetAmount = -18.0
					ENDIF
															
					IF iBlipCount = 0
					AND bEndOfLARiverReached
						fOffsetAmount = -26.0
					ENDIF
					
					fChaseSpeed = CONTROL_PLAYBACK_SPEED(rubberBandVehicle, fOffsetAmount)
				ELSE
				
					IF fRecTime > 0.0
						fOffsetAmount = 15.5
					ENDIF
					
					IF fRecTime > 180102.800
						fOffsetAmount = 9.0 //12.75
					ENDIF
					
					IF fRecTime > 185102.800
						fOffsetAmount = 13.0 //15.5
					ENDIF
					
					IF fRecTime > 200303.200
						fOffsetAmount = 15.5
					ENDIF
				
					fChaseSpeed = CONTROL_PLAYBACK_SPEED(rubberBandVehicle, fOffsetAmount)
				
				ENDIF
								
//				IF fRecTime > 170000.000
//				AND fRecTime < 185000.000
//					IF NOT bBeenToldToCross 
//						IF NOT IS_ENTITY_IN_ANGLED_AREA( PLAYER_PED_ID(), <<620.772705,-500.209930,13.839306>>, <<808.329773,-400.811890,31.744947>>, 31.500000)
//							//KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
//							IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_CROSS", CONV_PRIORITY_VERY_HIGH)	
//								bBeenToldToCross = TRUE
//							ENDIF
//						ENDIF
//					ENDIF
//				ENDIF
				
				
				SET_PLAYBACK_SPEED(vehBike[0], fChaseSpeed)						// +++ Vehicle speed set +++
				UPDATE_UBER_PLAYBACK(vehBike[0], fChaseSpeed)
				
				
				//PRINTLN("---->", GET_TIME_POSITION_IN_RECORDING(vehBike[2]), " + ", SetPieceCarStartime[26] , " < ", GET_TIME_POSITION_IN_RECORDING(vehBike[0]) )				
				
				
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[2])
				AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0])
				
					IF GET_TIME_POSITION_IN_RECORDING(vehBike[2]) + SetPieceCarStartime[15] <  GET_TIME_POSITION_IN_RECORDING(vehBike[0])
						PRINTLN("catch up!")			
						
						fDeltaTime = (GET_TIME_POSITION_IN_RECORDING(vehBike[0]) - (GET_TIME_POSITION_IN_RECORDING(vehBike[2]) + SetPieceCarStartime[15]))
						
						IF fDeltaTime > 1500
							fMult = 2.25
						ELSE
							fMult = (fDeltaTime/1500) * 2.25
						ENDIF
						
						fCatchUpSpeed = fChaseSpeed * fMult
						
						IF fCatchUpSpeed < 1.25
							fCatchUpSpeed = 1.25
						ENDIF
						
						IF NOT IS_SPHERE_VISIBLE(GET_ENTITY_COORDS(vehBike[2]), 1.0)
							fCatchUpSpeed = 6.0
						ENDIF
						
						SET_PLAYBACK_SPEED(vehBike[2], fCatchUpSpeed)
						
					ELSE
						bCaughtUp = TRUE
						PRINTLN("caught up!")
						//bWarpedOntoRec = TRUE
						SET_PLAYBACK_SPEED(vehBike[2], fChaseSpeed)
					ENDIF
				ENDIF				
				
				//1170892 - delete the inital cops waiting at the exit a little earlier.
				IF IS_ENTITY_AT_COORD(GET_SELECT_PED(SEL_MICHAEL), <<680.7623, -528.5031, 14.2773>>, <<165.0, 165.0, 100.0>>)
					IF DOES_ENTITY_EXIST(copAtExit1)
						DELETE_PED(copAtExit1)
					ENDIF
					IF DOES_ENTITY_EXIST(copAtExit2)
						DELETE_PED(copAtExit2)
					ENDIF
					IF DOES_ENTITY_EXIST(copAtExit3)
						DELETE_PED(copAtExit3)
					ENDIF
				ENDIF
				
				CREATE_CARS_AND_COPS_IN_LS_RIVER()
				
									
				DO_COLLATERAL_DAMAGE_SPEECH()
				
				LA_RIVER_MANAGE_MICHAEL_SECTION_BLIPS()
				//SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
				
				//If you've killed a sufficient number of cop cars...
				IF DOES_BLIP_EXIST(sLocatesData.GPSBlipID)
					SET_BLIP_ROUTE(sLocatesData.GPSBlipID, FALSE)
					SET_BLIP_EXTENDED_HEIGHT_THRESHOLD(sLocatesData.GPSBlipID, TRUE)
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(vehBike[0])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0])
						IF GET_TIME_POSITION_IN_RECORDING(vehBike[0]) > 213898.700
							IF bEndOfLARiverReached = FALSE
								IF bIsStealthPath
									TRIGGER_MUSIC_EVENT("JH2A_STORM_DRAIN_MA")
								ELSE
									TRIGGER_MUSIC_EVENT("JH2B_STORM_DRAIN_MA")
								ENDIF
								bEndOfLARiverReached = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				//Directions dialogue
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
					SWITCH iRiverDialogueSide
					//	addTextToQueue("JH_CROSS", TEXT_TYPE_CONVO)
					//addTextToQueue("JH_FRSAVE2", TEXT_TYPE_CONVO)
					
						CASE 0
							//First line from Michael
							IF fRecTime > 0.500 //0.0
								KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
								//IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_TRUCK", CONV_PRIORITY_VERY_HIGH)		
								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_TRAP", CONV_PRIORITY_VERY_HIGH)		
									
									iRiverDialogueSide++
									//iTimeOfLastRiverDialogue = GET_GAME_TIMER()
								ENDIF
							ENDIF
						BREAK
						
						CASE 1
							//First line from Michael
							IF fRecTime > 0.500 //0.0
															
								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_SWITCH", CONV_PRIORITY_VERY_HIGH)		
//								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_TRUCK", CONV_PRIORITY_VERY_HIGH)		
								//IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_FRSAVE2", CONV_PRIORITY_VERY_HIGH)		
									
									iRiverDialogueSide++
									//iTimeOfLastRiverDialogue = GET_GAME_TIMER()
								ENDIF
							ENDIF
						BREAK
						
						CASE 2
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								SET_BIT(sLocatesData.iLocatesBitSet, BS_INITIAL_GOD_TEXT_PRINTED)
								PRINT_NOW(Asset.godStormDrainSide, DEFAULT_GOD_TEXT_TIME, 1)
								iRiverDialogueSide++
							ENDIF
						BREAK
					
						CASE 3
							//Cross over if not on the right side of River
							IF fRecTime > 168995.500 //0.0
								IF NOT IS_PLAYER_ON_RIGHT_OF_RIVER(PLAYER_PED_ID())
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_CROSS", CONV_PRIORITY_VERY_HIGH)
										
										addTextToQueue("JH_FRSAVE2", TEXT_TYPE_CONVO)										
										IF IS_DRIVER_EDDIE()
											addTextToQueue("JH_OHSHIT_ET", TEXT_TYPE_CONVO)
										ELSE
											addTextToQueue("JH_OHSHIT_KD", TEXT_TYPE_CONVO)
										ENDIF										
										
										iTimeOfLastRiverDialogue = GET_GAME_TIMER()
										iRiverDialogueSide++
									ENDIF
								ELSE
									IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_FRSAVE2", CONV_PRIORITY_VERY_HIGH)
									
									//addTextToQueue("JH_FRSAVE2", TEXT_TYPE_CONVO)										
									
										iRiverDialogueSide++
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE 4
							//Get over to the left!
							IF (fRecTime > 173000.0	AND IS_SUBTITLE_PREFERENCE_SWITCHED_ON())//fix for 1966150 - different trigger times depending on if subtitels are on or not (fucks sake)
							OR (fRecTime > 177000.0 AND NOT IS_SUBTITLE_PREFERENCE_SWITCHED_ON())
								IF IS_PLAYER_ON_RIGHT_OF_RIVER(PLAYER_PED_ID())
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_RIVERLEFT", CONV_PRIORITY_VERY_HIGH)
										IF IS_DRIVER_EDDIE()
											addTextToQueue("JH_OHSHIT_ET", TEXT_TYPE_CONVO)
										ELSE
											addTextToQueue("JH_OHSHIT_KD", TEXT_TYPE_CONVO)
										ENDIF	
										iRiverDialogueSide++
									ENDIF
								ELSE
									//SCRIPT_ASSERT("not on right")
									IF IS_DRIVER_EDDIE()
										addTextToQueue("JH_OHSHIT_ET", TEXT_TYPE_CONVO)
									ELSE
										addTextToQueue("JH_OHSHIT_KD", TEXT_TYPE_CONVO)
									ENDIF	
									iRiverDialogueSide++
								ENDIF
							ENDIF
						BREAK
										
						CASE 5
							//Get over to the Right!
							IF fRecTime > 206199.100
								IF NOT IS_PLAYER_ON_RIGHT_OF_RIVER(PLAYER_PED_ID())
									KILL_FACE_TO_FACE_CONVERSATION_DO_NOT_FINISH_LAST_LINE()
									IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_RIVERRIGH", CONV_PRIORITY_VERY_HIGH)
										iRiverDialogueSide++
									ENDIF
								ENDIF
							ENDIF
						BREAK
																
					ENDSWITCH
				ENDIF
				
				IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				
					SWITCH iRiverDialogueSideMoreCops
					
						CASE 0
							IF iBlipCount > 0
								IF fRecTime > 175159.000						
									IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
										CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_RIVERHLPR", CONV_PRIORITY_VERY_HIGH)
										iRiverDialogueSideMoreCops++
									ELSE											
										CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_RIVERMORF", CONV_PRIORITY_VERY_HIGH)
										iRiverDialogueSideMoreCops++
									ENDIF
								ELSE
									iRiverDialogueSideMoreCops++
								ENDIF
							ENDIF
						BREAK
					
						CASE 1
							IF fRecTime > 190433.200	
								IF iBlipCount > 0
									IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
										CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_RIVERHLPR", CONV_PRIORITY_VERY_HIGH)
										iRiverDialogueSideMoreCops++
									ELSE											
										CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_RIVERMORF", CONV_PRIORITY_VERY_HIGH)
										iRiverDialogueSideMoreCops++
									ENDIF
								ELSE
									iRiverDialogueSideMoreCops++
								ENDIF
							ENDIF
						BREAK
						
						CASE 2
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF IS_HACKER_PAIGE()
									IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_HCKINT_PH", CONV_PRIORITY_VERY_HIGH)	
										iRiverDialogueSideMoreCops++
									ENDIF
								ELIF IS_HACKER_CHRISTIAN()
									IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_HCKINT_CF", CONV_PRIORITY_VERY_HIGH)	
										iRiverDialogueSideMoreCops++
									ENDIF
								ELIF IS_HACKER_RICKIE()
									IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_HCKINT_RL", CONV_PRIORITY_VERY_HIGH)	
										iRiverDialogueSideMoreCops++
									ENDIF
								ENDIF
							ENDIF
						BREAK
						
						CASE 3
						
							IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
								IF IS_GUNMAN_GUSTAVO()
									IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_COPS_GM", CONV_PRIORITY_VERY_HIGH)	
										iRiverDialogueSideMoreCops++
									ENDIF
								ELIF IS_GUNMAN_PACKIE()
									IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_COPS_PM", CONV_PRIORITY_VERY_HIGH)	
										iRiverDialogueSideMoreCops++
									ENDIF
								ENDIF
							ENDIF
						
						BREAK				
						
						
						CASE 4
							IF fRecTime > 202000.800					
								IF iBlipCount > 0
									IF GET_RANDOM_INT_IN_RANGE(0, 10) > 5
										CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_RIVERHLPR", CONV_PRIORITY_VERY_HIGH)
										iRiverDialogueSideMoreCops++
										ELSE											
										CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_RIVERMORF", CONV_PRIORITY_VERY_HIGH)
										iRiverDialogueSideMoreCops++
									ENDIF
								ELSE
									iRiverDialogueSideMoreCops++
								ENDIF
							ENDIF
						BREAK
					
						CASE 5
							IF fRecTime > 208199.100
							AND iBlipCount > 0
								//we're running out of river!
								IF NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
									IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_NORIVER", CONV_PRIORITY_VERY_HIGH)
										iRiverDialogueSideMoreCops++
									ENDIF
								ENDIF
							ENDIF
						BREAK
					
					ENDSWITCH
					
				ENDIF
				
				IF iBlipCount > 0
				AND NOT IS_ANY_TEXT_BEING_DISPLAYED(sLocatesData, IAT_IGNORE_GOD_TEXT_IF_SUBTITLES_OFF)
				AND NOT isAnyConversationWaitingInQueue()
				AND NOT isAnyGodTextWaitingInQueue()
				AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[2])	
					
					IF (GET_ENTITY_HEALTH(vehBike[2]) - iLastEngineHealth) > 0
					AND GET_GAME_TIMER() - iTimeOfLastRiverDialogue > 4000
						
						IF bAlternateRiverLines = FALSE
							IF IS_DRIVER_EDDIE()								
								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_COPOFF_ET", CONV_PRIORITY_VERY_HIGH)
									PRINTLN("Bike Damaged!!")
									bAlternateRiverLines = TRUE
									iTimeOfLastRiverDialogue = GET_GAME_TIMER()
									iLastEngineHealth = GET_ENTITY_HEALTH(vehBike[2])
								ENDIF	
							ELSE
								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_COPOFF_KD", CONV_PRIORITY_VERY_HIGH)
									PRINTLN("Bike Damaged!!")
									bAlternateRiverLines = TRUE
									iTimeOfLastRiverDialogue = GET_GAME_TIMER()
									iLastEngineHealth = GET_ENTITY_HEALTH(vehBike[2])
								ENDIF	
							ENDIF
						ELSE		
							IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_RIVERDAMA", CONV_PRIORITY_VERY_HIGH)
								//iTimeOfLastRiverDialogue = GET_GAME_TIMER()
								PRINTLN("Bike Damaged!!")
								bAlternateRiverLines = FALSE
								iTimeOfLastRiverDialogue = GET_GAME_TIMER()
								iLastEngineHealth = GET_ENTITY_HEALTH(vehBike[2])
							ENDIF
						ENDIF
					ENDIF
					
					
					SWITCH iRiverDialogue
					
						CASE 0
							IF GET_GAME_TIMER() - iTimeOfLastRiverDialogue > 6000
								IF GET_VEHICLE_ENGINE_HEALTH(vehBike[2]) < 500.0
									iRiverDialogue =3
								ELSE
									iRiverDialogue =1
								ENDIF
								iTimeOfLastRiverDialogue = GET_GAME_TIMER()
							ENDIF
						BREAK
						
						CASE 1
							//Franklin/Driver Telling Michael to get on the case
							
							IF bAlternateRiverLines = FALSE
								IF IS_DRIVER_EDDIE()								
									IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_COPOFF_ET", CONV_PRIORITY_VERY_HIGH)
										bAlternateRiverLines = TRUE
										iRiverDialogue =2
									ENDIF	
								ELSE
									IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_COPOFF_KD", CONV_PRIORITY_VERY_HIGH)
										bAlternateRiverLines = TRUE
										iRiverDialogue =2
									ENDIF	
								ENDIF
							ELSE							
							
								IF (IS_PLAYER_ON_RIGHT_OF_RIVER(GET_SELECT_PED(SEL_FRANKLIN))								
								AND NOT IS_PLAYER_ON_RIGHT_OF_RIVER(PLAYER_PED_ID()))
								OR (NOT IS_PLAYER_ON_RIGHT_OF_RIVER(GET_SELECT_PED(SEL_FRANKLIN))								
								AND IS_PLAYER_ON_RIGHT_OF_RIVER(PLAYER_PED_ID()))
									IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_RIVERSIDE", CONV_PRIORITY_VERY_HIGH)
										bAlternateRiverLines = FALSE
										iRiverDialogue =2
									ENDIF	
								ELSE								
									IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_RIVERHELP", CONV_PRIORITY_VERY_HIGH)
										bAlternateRiverLines = FALSE
										iRiverDialogue =2
									ENDIF	
								ENDIF	
							ENDIF
								
						BREAK
						
						CASE 2
							//Michael responds //addTextToQueue("JH_MIKECOPS", TEXT_TYPE_CONVO)
							IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_MIKECOPS", CONV_PRIORITY_VERY_HIGH)
								//iTimeOfLastRiverDialogue = GET_GAME_TIMER()
								iRiverDialogue=  0
							ENDIF	
						BREAK
										
					ENDSWITCH
					
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(setPieceCarid[24])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(setPieceCarid[24])
						IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setPieceCarid[24])  > 10.0
							SET_VEHICLE_ENGINE_HEALTH(vehBike[2], 20.0 + (83.0 - GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setPieceCarid[24])))
							PRINTLN("Engine Health due to 24:", 1.0 * (83.0 - GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setPieceCarid[24])) )
							//SET_VEHICLE_PETROL_TANK_HEALTH(vehBike[2], 2.0 * (70.0 - GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setPieceCarid[24])))
						ENDIF
					ENDIF
				ENDIF
				
				//10 * (100.0 - 0.0X)= 1000.0
				IF NOT IS_ENTITY_DEAD(setPieceCarid[22])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(setPieceCarid[22])	
						IF GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setPieceCarid[22])  > 10.0
							SET_VEHICLE_ENGINE_HEALTH(vehBike[2], 20.0 + (88.0 - GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setPieceCarid[22])))										
							PRINTLN("Engine Health due to 22:", 1.0 * (88.0 - GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setPieceCarid[24])) )
							//SET_VEHICLE_PETROL_TANK_HEALTH(vehBike[2], 2.0 * (70.0 - GET_VEHICLE_RECORDING_PLAYBACK_PERCENTAGE_PROGRESS(setPieceCarid[22])))
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_ENTITY_DEAD(vehBike[2])
								
					IF GET_VEHICLE_ENGINE_HEALTH(vehBike[2]) < 100.0
					AND GET_VEHICLE_ENGINE_HEALTH(vehBike[2]) > 40.0
					AND GET_VEHICLE_ENGINE_HEALTH(vehBike[2]) - fLastEngineHealth <> 0.0
					AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[2])
						iRiverDialogue = -1
					
						IF GET_GAME_TIMER() - iTimeOfLastRiverDialogue > 8000
							IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_MOREDAM", CONV_PRIORITY_VERY_HIGH)
								iTimeOfLastRiverDialogue = GET_GAME_TIMER()
							ENDIF
						ENDIF
						
						iRandomBike = GET_RANDOM_INT_IN_RANGE(0, 3)
						
						IF (GET_GAME_TIMER() - iTimeOfLastRiverDialogue) % 5 = 0
							IF DOES_ENTITY_EXIST(vehBike[iRandomBike])
								SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBike[iRandomBike], <<0.5, -2.0, 0.1>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBike[iRandomBike], <<0.0, 0.0, 0.2>>), 1, TRUE, WEAPONTYPE_ASSAULTRIFLE)
							ENDIF
						ENDIF	
						
					ENDIF					
				
					IF GET_VEHICLE_ENGINE_HEALTH(vehBike[2]) < 40.0
						
						SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), KNOCKOFFVEHICLE_EASY)
						
						IF GET_GAME_TIMER() - iTimeOfLastRiverDialogue > 5000
						AND GET_VEHICLE_ENGINE_HEALTH(vehBike[2]) - fLastEngineHealth <> 0.0
						AND IS_PED_IN_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
						AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[2])
							IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_RIVERDAM2", CONV_PRIORITY_VERY_HIGH)
														
								iTimeOfLastRiverDialogue = GET_GAME_TIMER()
							ENDIF
							
							iRandomBike = GET_RANDOM_INT_IN_RANGE(0, 3)
							
							IF (GET_GAME_TIMER() - iTimeOfLastRiverDialogue) % 2 = 0
								IF DOES_ENTITY_EXIST(vehBike[iRandomBike])
									SHOOT_SINGLE_BULLET_BETWEEN_COORDS(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBike[iRandomBike], <<0.5, -2.0, 0.1>>), GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBike[iRandomBike], <<0.0, 0.0, 0.2>>), 1, TRUE, WEAPONTYPE_ASSAULTRIFLE)
								ENDIF
							ENDIF	
							
							SET_VEHICLE_PETROL_TANK_HEALTH(vehBike[2], 5.0)
																						
						ENDIF
						
						IF GET_VEHICLE_ENGINE_HEALTH(vehBike[2]) < 30.0
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[2])
								STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[2])
								SET_VEHICLE_PETROL_TANK_HEALTH(vehBike[2], 30.0)
								SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), KNOCKOFFVEHICLE_EASY)
							ELSE	
								IF CAN_KNOCK_PED_OFF_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN))
									KNOCK_PED_OFF_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN))
									iTimeOfLastRiverDialogue = GET_GAME_TIMER()
								ENDIF
							ENDIF	
						ENDIF	
						
						vBikeRotation = GET_ENTITY_ROTATION(vehBike[2])
														
						IF (vBikeRotation.y > 70.0 AND vBikeRotation.y < 290.0)
						OR (vBikeRotation.x > 70.0 AND vBikeRotation.x < 290.0)
						OR GET_VEHICLE_PETROL_TANK_HEALTH(vehBike[2]) <  15.0
							IF NOT IS_ENTITY_DEAD(vehBike[2])
								WAIT(1000)
							ENDIF
							IF NOT IS_ENTITY_DEAD(vehBike[2])
								EXPLODE_VEHICLE(vehBike[2])
								SET_ENTITY_HEALTH(GET_SELECT_PED(SEL_FRANKLIN), 0)
							ENDIF
						ENDIF
					ENDIF
					
					fLastEngineHealth = GET_VEHICLE_ENGINE_HEALTH(vehBike[2])
					
				ENDIF											
				
				IF iBlipCount = 0
				AND bEndOfLARiverReached
				AND IS_VEHICLE_DRIVEABLE(vehBike[2])
				
					IF bSwitchedRecsToAi = FALSE
						
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						
						SET_PED_AS_NO_LONGER_NEEDED(copOnRight1)
						SET_PED_AS_NO_LONGER_NEEDED(copOnRight2)
						SET_PED_AS_NO_LONGER_NEEDED(copOnRight3)
						
						SET_VEHICLE_AS_NO_LONGER_NEEDED(burnedWreck1)
						SET_VEHICLE_AS_NO_LONGER_NEEDED(burnedWreck2)
						
						CLEAR_PED_TASKS(pedCrew[CREW_HACKER])
												
						//Branching conversation depending on what happened.
						IF GET_CREW_MEMBER_SKILL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW))) = CMSK_BAD
							addTextToQueue("JH_BRANCH2", TEXT_TYPE_CONVO)	//Two bikes. What the hell happened?
							addTextToQueue("JH_BRANCH3A", TEXT_TYPE_CONVO)	//Dude crashed before he got onto the Del Perro Freeway. 
							addTextToQueue("JH_BRANCH4A", TEXT_TYPE_CONVO)	//Shit, I shoulda paid for a better gunman. What happened to his part of the score?
							IF g_replay.iReplayInt[GOT_BAG_REPLAY_VALUE] = BAG_NOT_COLLECTED
								addTextToQueue("JH_BRANCH5A", TEXT_TYPE_CONVO)//Fuck I left it
							ELSE
								addTextToQueue("JH_BRANCH5B", TEXT_TYPE_CONVO)//Don't worry I picked it up
							ENDIF
							//addTextToQueue( "JH_BRANCH7", TEXT_TYPE_CONVO)	
						ELSE
							//runConversation("JH_RIVERFREE", TRUE, TRUE, TRUE, CONV_PRIORITY_VERY_HIGH)
							
							IF IS_HACKER_PAIGE()
								runConversation("JH_HCKNO_PH", TRUE, TRUE, TRUE, CONV_PRIORITY_VERY_HIGH)		
							ELIF IS_HACKER_CHRISTIAN()
								runConversation("JH_HCKNO_CF", TRUE, TRUE, TRUE, CONV_PRIORITY_VERY_HIGH)
							ELIF IS_HACKER_RICKIE()
								runConversation("JH_HCKNO_RL", TRUE, TRUE, TRUE, CONV_PRIORITY_VERY_HIGH)	
							ENDIF
							addTextToQueue("JH_LATECOPS1", TEXT_TYPE_CONVO)
						
							
						ENDIF
						
						bSwitchedRecsToAi = TRUE
					
					ENDIF
				
				ELSE
					REPORT_POLICE_SPOTTED_PLAYER(PLAYER_ID())
				ENDIF
				
				IF IS_PLAYER_AT_LOCATION_IN_VEHICLE(sLocatesData, vFightEndTarget, <<12.0, 8.0, 2.0>>, TRUE, vehTruck, Asset.godStormDrainSide, Asset.godGetBackInTruck, Asset.godGetBackInTruck, FALSE)			
					
					//NEW_LOAD_SCENE_START(<<636.5539, -1846.3218, 8.2481>>, NORMALISE_VECTOR(<<636.5444, -1846.3081, 8.2481>> - <<636.5539, -1846.3218, 8.2481>>), 600.00)
					
					WHILE NOT BRING_VEHICLE_TO_HALT_AND_DISABLE_VEH_CONTROLS(vehTruck, 6.0, 5)
						
						WAIT(0)
					ENDWHILE
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
					IF bIsStealthPath
						TRIGGER_MUSIC_EVENT("JH2A_ARRIVE_DRAIN_MA")
					ENDIF
					iStageSection = 6
				ENDIF
				
			BREAK
	
		ENDSWITCH
		
//		FOR iCounter = 0 TO TOTAL_NUMBER_OF_PARKED_CARS-1									//Turns on traffic jam engines
//			IF DOES_ENTITY_EXIST(ParkedCarID[iCounter])
//				IF NOT IS_ENTITY_DEAD(ParkedCarID[iCounter])
//					IF GET_ENTITY_MODEL(ParkedCarID[iCounter]) = POLICE3
//						//IF NOT IS_VEHICLE_SIREN_ON(ParkedCarID[iCounter])
//							SET_SIREN_WITH_NO_DRIVER(ParkedCarID[iCounter], TRUE)
//						//ENDIF
//					ENDIF
//				ENDIF
//			ENDIF
//		ENDFOR
		
		
		IF NOT IS_ENTITY_DEAD(vehBike[0])
			IF IS_ENTITY_AT_COORD(vehBike[0], vFightEndTarget, <<12.0, 8.0, 8.0>>)
				BRING_VEHICLE_TO_HALT(vehBike[0], 2.0, -1)
				IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCrew[CREW_DRIVER_NEW], TRUE)
				ENDIF
			ENDIF		
		ENDIF		
		
		IF NOT bWorstGunDies
			IF NOT IS_ENTITY_DEAD(vehBike[1])
				IF IS_ENTITY_AT_COORD(vehBike[1], vFightEndTarget, <<12.0, 8.0, 8.0>>)
					BRING_VEHICLE_TO_HALT(vehBike[1], 2.0, -1)
					IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCrew[CREW_GUNMAN_NEW], TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		IF NOT (GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN)
			IF NOT IS_ENTITY_DEAD(vehBike[2])
				IF IS_ENTITY_AT_COORD(vehBike[2], vFightEndTarget, <<12.0, 8.0, 8.0>>)
					BRING_VEHICLE_TO_HALT(vehBike[2], 2.0, -1)
					IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_FRANKLIN))
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
			
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 6
			
			STOP_AUDIO_SCENE("JSH_2B_RAM_POLICE")
			
			SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
			
			IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_FRANKLIN))
				SET_ENTITY_INVINCIBLE(GET_SELECT_PED(SEL_FRANKLIN), FALSE)
			ENDIF
			IF NOT IS_ENTITY_DEAD(GET_SELECT_PED(SEL_MICHAEL))
				SET_ENTITY_INVINCIBLE(GET_SELECT_PED(SEL_MICHAEL), FALSE)
			ENDIF
			
			SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
			
			FOR iCounter = 0 to iNoOfStormCops-1
				IF IS_VEHICLE_DRIVEABLE(vehTruck)
				AND (IS_VEHICLE_DRIVEABLE(vehPolice[iCounter]) OR NOT IS_VEHICLE_DRIVEABLE(vehPolice[iCounter]))
					//IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(vehTruck), GET_ENTITY_COORDS(vehPolice[iCounter])) < 20
						IF DOES_ENTITY_EXIST(pedPolice[iCounter])
							DELETE_PED(pedPolice[iCounter])
						ENDIF
						IF DOES_ENTITY_EXIST(pedPolicePassenger[iCounter])
							DELETE_PED(pedPolicePassenger[iCounter])
						ENDIF
						IF DOES_ENTITY_EXIST(vehPolice[iCounter])
							DELETE_VEHICLE(vehPolice[iCounter])
						ENDIF
					//ENDIF
				ENDIF
			ENDFOR
			
			REMOVE_WEAPON_ASSET(WEAPONTYPE_SMG)
			
			FOR iCounter = 0 TO 2
				IF DOES_BLIP_EXIST(blipCrew[iCounter])
					REMOVE_BLIP(blipCrew[iCounter])
				ENDIF
			ENDFOR
			SET_ENTITY_PROOFS(vehTruck, FALSE, FALSE, FALSE, FALSE, FALSE)
			
			bStageSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

BOOL bRampClanged = FALSE

//PURPOSE:		Performs the cutscene where the ramp is lowered
FUNC BOOL stageCutsceneRampDown()

	

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
	
		DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_DOWN, FALSE)
		DISABLE_CHEAT(CHEAT_TYPE_WANTED_LEVEL_UP, FALSE)
	
		IF NOT IS_ENTITY_DEAD(vehBike[2])
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehBike[2], "JSH_2B_PLAYER_BIKE")
			SET_AUDIO_VEHICLE_PRIORITY(vehBike[2], AUDIO_VEHICLE_PRIORITY_MAX)
		ENDIF
		IF NOT IS_ENTITY_DEAD(vehBike[0])
			ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehBike[0], "JSH_2B_BUDDY_BIKES")
			SET_AUDIO_VEHICLE_PRIORITY(vehBike[0], AUDIO_VEHICLE_PRIORITY_MAX)
		ENDIF
		IF NOT bWorstGunDies
			IF NOT IS_ENTITY_DEAD(vehBike[1])
				ADD_ENTITY_TO_AUDIO_MIX_GROUP(vehBike[1], "JSH_2B_BUDDY_BIKES")
				SET_AUDIO_VEHICLE_PRIORITY(vehBike[1], AUDIO_VEHICLE_PRIORITY_MAX)
			ENDIF
		ENDIF	
		SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", TRUE)
//		IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
//			NEW_LOAD_SCENE_START(<<636.5539, -1846.3218, 8.2481>>, NORMALISE_VECTOR(<<636.5444, -1846.3081, 8.2481>> - <<636.5539, -1846.3218, 8.2481>>), 600.00)
//		ENDIF
		
		IF bIsStealthPath
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7, "STAGE_CUTSCENE_RAMP_DOWN")
		ELSE
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(6, "STAGE_CUTSCENE_RAMP_DOWN")
		ENDIF
		
		g_replay.iReplayInt[CURRENT_TAKE_REPLAY_VALUE] = iCurrentTake
		
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("JWL_HEIST_CAR_SMASHES_BIG")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("ALARM_BELL_02")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("JWL_HEIST_RAID_GLASS_SMASH")
		RELEASE_NAMED_SCRIPT_AUDIO_BANK(Asset.sbUnderwater)
		
		CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
				
		SET_MODEL_AS_NO_LONGER_NEEDED(POLICE)
		
		REMOVE_VEHICLE_RECORDING(700, "JHUBER")
		REMOVE_VEHICLE_RECORDING(998, "JHUBER")
		
		RELEASE_TRUCK_FIGHT_ASSETS()
		
		DELETE_MISSION_TRAIN(heliCrashTrain)
		SET_TRAIN_MODELS_AS_NO_LONGER_NEEDED()
		
		RESET_NEED_LIST()
				
		REQUEST_VEHICLE_RECORDING(1, "JHLoadVan")
		REQUEST_VEHICLE_RECORDING(2, "JHLoadVan")
		REQUEST_VEHICLE_RECORDING(3, "JHLoadVan")
		
		REQUEST_AMBIENT_AUDIO_BANK("TRUCK_RAMP_DOOR")
		REQUEST_ANIM_DICT("MISSHeist_Jewel")
		
		PREFETCH_SRL("JHRAMPDOWNSCENE")
		
		WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "JHLoadVan")
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "JHLoadVan")
		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(3, "JHLoadVan")
		OR NOT REQUEST_AMBIENT_AUDIO_BANK("TRUCK_RAMP_DOOR")
		OR NOT HAS_ANIM_DICT_LOADED("MISSHeist_Jewel")
		OR NOT IS_SRL_LOADED()
			WAIT(0)
		ENDWHILE
		
		SET_PARTICLE_FX_BANG_SCRAPE_LODRANGE_SCALE(0.0)
		
		MANAGE_LOADING_NEED_LIST()
				
		DO_START_CUTSCENE(FALSE)
		BEGIN_SRL()
		KILL_CHASE_HINT_CAM(localChaseHintCamStruct)
		
		IF ENUM_TO_INT(playersPreviousCameraview) <> 0
			SET_FOLLOW_VEHICLE_CAM_VIEW_MODE(playersPreviousCameraview)
		ENDIF
		
		SET_CINEMATIC_BUTTON_ACTIVE(TRUE)	
		
		FOR iCounter = 0 TO 2
			IF DOES_ENTITY_EXIST(vehBike[iCounter])
				//IF IS_ENTITY_A_MISSION_ENTITY(vehBike[iCounter])
					SET_ENTITY_AS_MISSION_ENTITY(vehBike[iCounter], TRUE, TRUE)
				//ENDIF
			ENDIF
//			IF NOT (bWorstGunDies AND iCounter = 1)
//				vehBike[iCounter] = CREATE_VEHICLE(	Asset.mnBike, vVehicleFightBikeSpawn[iCounter], 
//														fVehicleFightBikeSpawn[iCounter])
//				SET_VEHICLE_ON_GROUND_PROPERLY(vehBike[iCounter])
//				SET_VEHICLE_COLOUR_COMBINATION(vehBike[iCounter], iCounter)
//			ENDIF
		ENDFOR
					
		SET_ENTITY_AS_MISSION_ENTITY(pedCrew[CREW_DRIVER_NEW], TRUE, TRUE)
					
		//DELETE_PED(pedCrew[CREW_DRIVER_NEW])
//		pedCrew[CREW_DRIVER_NEW] = CREATE_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,  ENUM_TO_INT(CREW_DRIVER_NEW), 
//																	vVehicleFightBikeSpawn[0], 0.0)
//		SET_PED_CREW(pedCrew[CREW_DRIVER_NEW])
		
		
		IF NOT bWorstGunDies
			//IF NOT IS_ENTITY_A_MISSION_ENTITY(pedCrew[CREW_GUNMAN_NEW])
				SET_ENTITY_AS_MISSION_ENTITY(pedCrew[CREW_GUNMAN_NEW], TRUE, TRUE)
			//ENDIF						
			
//			pedCrew[CREW_GUNMAN_NEW] = CREATE_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,  ENUM_TO_INT(CREW_GUNMAN_NEW), 
//																			vVehicleFightBikeSpawn[0], 0.0)			
//			SET_PED_CREW(pedCrew[CREW_GUNMAN_NEW])
		ENDIF
				
		CLEANUP_UBER_PLAYBACK_AND_BIKES(TRUE)
		
		IF IS_SCREEN_FADED_OUT()
			SETUP_CREW_SUITS(FALSE)
			SET_PED_BAG(pedCrew[CREW_GUNMAN_NEW], TRUE)
			
			//Driver
			IF NOT IS_ENTITY_DEAD(pedCrew[CREW_DRIVER_NEW])
				SET_PED_HELMET_PROP_INDEX(pedCrew[CREW_GUNMAN_NEW], 3)
				SET_PED_HELMET_TEXTURE_INDEX(pedCrew[CREW_GUNMAN_NEW], 0)
				SET_PED_PROP_INDEX(pedCrew[CREW_DRIVER_NEW], INT_TO_ENUM(PED_PROP_POSITION,0), 3, 0)
				GIVE_PED_HELMET(pedCrew[CREW_DRIVER_NEW], TRUE, PV_FLAG_SCRIPT_HELMET)
			ENDIF
			IF NOT IS_ENTITY_DEAD(pedCrew[CREW_GUNMAN_NEW])
				SET_PED_HELMET_PROP_INDEX(pedCrew[CREW_GUNMAN_NEW], 4)
				SET_PED_HELMET_TEXTURE_INDEX(pedCrew[CREW_GUNMAN_NEW], 0)
				SET_PED_PROP_INDEX(pedCrew[CREW_GUNMAN_NEW], INT_TO_ENUM(PED_PROP_POSITION,0), 4, 0)	
				GIVE_PED_HELMET(pedCrew[CREW_GUNMAN_NEW], TRUE, PV_FLAG_SCRIPT_HELMET)
			ENDIF
			
			
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
				SET_PED_HELMET_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), 6)
				SET_PED_HELMET_TEXTURE_INDEX(GET_SELECT_PED(SEL_FRANKLIN), 8)
				SET_PED_PROP_INDEX(GET_SELECT_PED(SEL_FRANKLIN), INT_TO_ENUM(PED_PROP_POSITION,0), 6, 8)
			ENDIF
		ENDIF
				
		IF IS_VEHICLE_DRIVEABLE(vehTruck)
			IF NOT IS_PED_INJURED(pedCrew[CREW_HACKER])
				IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_HACKER], vehTruck)
					CLEAR_PED_TASKS_IMMEDIATELY(pedCrew[CREW_HACKER])
					SET_PED_INTO_VEHICLE(pedCrew[CREW_HACKER], vehTruck, VS_FRONT_RIGHT)
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedCrew[CREW_HACKER])
				ENDIF
			ENDIF
		ENDIF

		IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
		AND NOT IS_ENTITY_DEAD(vehBike[0])
			SET_VEHICLE_FIXED(vehBike[0])
			STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[0])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCrew[CREW_DRIVER_NEW], FALSE)
			
			IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])
				CLEAR_PED_TASKS_IMMEDIATELY(pedCrew[CREW_DRIVER_NEW])
				SET_PED_INTO_VEHICLE(pedCrew[CREW_DRIVER_NEW], vehBike[0])		
			ENDIF
			
		ENDIF
		IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
		AND NOT IS_ENTITY_DEAD(vehBike[1])
			SET_VEHICLE_FIXED(vehBike[1])
			STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[1])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCrew[CREW_GUNMAN_NEW], FALSE)
			
			IF NOT IS_PED_IN_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
				CLEAR_PED_TASKS_IMMEDIATELY(pedCrew[CREW_GUNMAN_NEW])
				SET_PED_INTO_VEHICLE(pedCrew[CREW_GUNMAN_NEW], vehBike[1])								
			ENDIF
		ENDIF
		IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
		AND NOT IS_ENTITY_DEAD(vehBike[2])
			SET_VEHICLE_FIXED(vehBike[2])
			STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[2])
			CLEAR_PED_TASKS_IMMEDIATELY(GET_SELECT_PED(SEL_FRANKLIN))
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(GET_SELECT_PED(SEL_FRANKLIN), FALSE)
			SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
			SET_PED_CAN_BE_KNOCKED_OFF_VEHICLE(GET_SELECT_PED(SEL_FRANKLIN), KNOCKOFFVEHICLE_NEVER)
			
		ENDIF

		SET_VEHICLE_FIGHT_COORD(vehTruck, GET_SELECT_PED(SEL_MICHAEL), vFightEndTarget, fFightEndTarget)
		SET_VEHICLE_FIGHT_COORD(vehBike[0], pedCrew[CREW_DRIVER_NEW],  
								GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTruck, <<-0.25, -10, -1.0>>), fFightEndTarget)
		SET_VEHICLE_FIGHT_COORD(vehBike[1], pedCrew[CREW_GUNMAN_NEW],  
								GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTruck, <<0.8, -15, -1.0>>), fFightEndTarget)
		SET_VEHICLE_FIGHT_COORD(vehBike[2], GET_SELECT_PED(SEL_FRANKLIN),  
								GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehTruck, <<-0.1, -20, -1.0>>), fFightEndTarget)
		
		FOR iCounter = 0 TO	NUMBER_OF_EVENT_FLAGS-1
			bEventFlag[iCounter] = FALSE
		ENDFOR
		IF NOT IS_PED_INJURED(pedCrew[CREW_HACKER])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCrew[CREW_HACKER], TRUE)
		ENDIF

		FOR iCounter = 0 TO	2
			IF DOES_ENTITY_EXIST(vehBike[iCounter])
				IF NOT IS_ENTITY_DEAD(vehBike[iCounter])
					SET_VEHICLE_FIXED(vehBike[iCounter])
				ENDIF
			ENDIF
			IF DOES_BLIP_EXIST(blipCrew[iCounter])
				REMOVE_BLIP(blipCrew[iCounter])
			ENDIF
		ENDFOR
				
		SET_FAKE_WANTED_LEVEL(0)

		SWITCH_ALL_RANDOM_CARS_OFF()

		ADD_ALL_PEDS_TO_DIALOGUE()

		SAFE_FADE_IN()
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
		
		FOR iCounter = 0 TO NUMBER_OF_EVENT_FLAGS-1
			bEventFlag[iCounter] = FALSE
		ENDFOR

		iStageSection = 0
		bCameraShotSetup = FALSE
		bStageSetup = TRUE
		
		//Avoid one frame delay...
		SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
		SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
//		doTwoAttachLookAtEntityCam(vehTruck,
//									<< -3.5420, 4.5166, 0.9849 >>, << -0.0051, 2.0911, 0.7871 >>, 25, 
//									<< -3.5702, 4.4748, 0.9895 >>, << -0.0051, 2.0911, 0.7871 >>, 25, 1500)
//		

		doTwoPointCam(<<657.076233,-1879.361206,9.100791>>,<<8.990174,0.270771,33.970856>>,34.124870,
					<<656.565308,-1878.123169,9.054264>>,<<8.990174,0.270771,33.970856>>,34.124870,
					3000)
			
		doCamShake("ROAD_VIBRATION_SHAKE", 0.1)
		SETTIMERA(0)	
		bCameraShotSetup = TRUE
		
		CLEAR_AREA_OF_PEDS(<<657.076233,-1879.361206,9.100791>>, 200.0)
				
//		START_RECORDING_VEHICLE(vehBike[0], 1, "JHLoadVan")
//		START_RECORDING_VEHICLE(vehBike[1], 2, "JHLoadVan")
//		START_RECORDING_VEHICLE(vehBike[2], 3, "JHLoadVan")  
		
		
		TASK_LOOK_AT_COORD( pedCrew[CREW_HACKER], << 635.0606, -1848.3879, 10.1182 >>, 4000,  SLF_WHILE_NOT_IN_FOV)
		//TASK_LOOK_AT_COORD(GET_SELECT_PED(SEL_MICHAEL), << 637.7403, -1848.3641, 10.1465 >>, 4000,  SLF_WHILE_NOT_IN_FOV)
		
		TASK_PLAY_ANIM(GET_SELECT_PED(SEL_MICHAEL), "MISSHeist_Jewel", "Michael_InTruck_Shouting", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_NOT_INTERRUPTABLE | AF_SECONDARY)
				
		ROLL_DOWN_WINDOW(vehTruck, SC_WINDOW_FRONT_LEFT)
				
		IF bIsStealthPath
			TRIGGER_MUSIC_EVENT("JH2A_ENTER_TRUCK")
		ELSE
			TRIGGER_MUSIC_EVENT("JH2B_ENTER_TRUCK")
		ENDIF
		
		runConversation(Asset.convMountTruck, TRUE)
		
		START_AUDIO_SCENE("JSH_2B_LOAD_UP_TRUCK")
		
		REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)

//═════════╡ UPDATE ╞═════════		
	ELSE

		OVERRIDE_LODSCALE_THIS_FRAME(0.9)

		SET_SRL_TIME(TO_FLOAT(TIMERA()))
		
		IF NOT IS_ENTITY_DEAD(vehBike[0])
			SET_FORCE_HD_VEHICLE(vehBike[0], TRUE)
		ENDIF

		IF NOT IS_ENTITY_DEAD(vehBike[1])
			SET_FORCE_HD_VEHICLE(vehBike[1], TRUE)
		ENDIF
	
		IF NOT IS_ENTITY_DEAD(vehBike[2])
			SET_FORCE_HD_VEHICLE(vehBike[2], TRUE)
		ENDIF

		SWITCH iStageSection
			CASE 0
						
				//SETUP
				IF NOT bCameraShotSetup
					
					//Michael:	Ramps down etc..	
				//UPDATE
				ELSE				
				
					IF TIMERA() > 2000						
					AND NOT bEventFlag[5]	
						SET_RAMP_OPEN(vehTruck, TRUE, 0.95)
						bEventFlag[5] = TRUE
					ENDIF
				//CLEANUP
					IF TIMERA() > 3000						
						iStageSection++
											
						START_PLAYBACK_RECORDED_VEHICLE(vehBike[0], 1, "JHLoadVan")
						SET_ENTITY_INVINCIBLE(vehBike[0], TRUE)
						IF NOT bWorstGunDies
							START_PLAYBACK_RECORDED_VEHICLE(vehBike[1], 2, "JHLoadVan")
							SET_ENTITY_INVINCIBLE(vehBike[1], TRUE)
						ENDIF
						START_PLAYBACK_RECORDED_VEHICLE(vehBike[2], 3, "JHLoadVan")
						SET_ENTITY_INVINCIBLE(vehBike[2], TRUE)
								
						IF Asset.mnBike = SANCHEZ
							SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehBike[0], <<0.0, 0.0, 0.15>>)
							IF NOT bWorstGunDies
								SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehBike[1], <<0.0, 0.0, 0.15>>)
							ENDIF
							SET_POSITION_OFFSET_FOR_RECORDED_VEHICLE_PLAYBACK(vehBike[2], <<0.0, 0.0, 0.15>>)
						ENDIF
						
						bCameraShotSetup = FALSE
					ENDIF
				ENDIF
			BREAK
			CASE 1
			
				//SETUP
				IF NOT bCameraShotSetup
					
					doTwoPointCam(<<638.862610,-1824.619751,8.847853>>,<<4.575484,-1.939967,172.092651>>,34.247810,
					<<639.029968,-1824.619751,8.847853>>,<<4.575484,-1.939967,172.092651>>,34.247810,
					2500)
															
																			
					doCamShake("ROAD_VIBRATION_SHAKE", 0.1)
					//SETTIMERA(0)	
					bCameraShotSetup = TRUE
				//UPDATE
				ELSE
					
					SET_RAMP_OPEN(vehTruck, TRUE, 0.95)
					
				//CLEANUP
					IF TIMERA() > 0//1200
						iStageSection++
						bCameraShotSetup = FALSE
					ENDIF
				ENDIF
			BREAK
			CASE 2
				//SETUP
				IF NOT bCameraShotSetup	
						
					doCamShake("ROAD_VIBRATION_SHAKE", 0.1)
					
					//SETTIMERA(0)	
					bRampClanged = FALSE				
					bCameraShotSetup = TRUE
				//UPDATE
				ELSE
					
					IF bRampClanged = FALSE
					AND TIMERA() > 4700
						PLAY_SOUND_FRONTEND(-1, "RAMP_DOWN", "TRUCK_RAMP_DOWN")	
						bRampClanged = TRUE
					ENDIF
				
					IF NOT bEventFlag[0]
					AND TIMERA() > 3750
						IF HAS_VEHICLE_MOUNTED_TRUCK(vehBike[0])
							CLEAR_PED_DRIVE_TASKS(pedCrew[CREW_DRIVER_NEW], vehBike[0])
							bEventFlag[0] = TRUE
						ENDIF
					ENDIF
				
				//CLEANUP
					IF TIMERA() > 4800
						//SCRIPT_ASSERT("boom")
						
						//doCamShake("JOLT_SHAKE", 0.2)
						iStageSection++
						bCameraShotSetup = FALSE
					ENDIF
				ENDIF
			BREAK	
			
			CASE 3
				//SETUP
				IF NOT bCameraShotSetup	
					//SETTIMERA(0)	
					bCameraShotSetup = TRUE
				//UPDATE
				ELSE
				
					IF NOT bEventFlag[0]
					AND TIMERA() > 4800//500
						IF HAS_VEHICLE_MOUNTED_TRUCK(vehBike[0])
							CLEAR_PED_DRIVE_TASKS(pedCrew[CREW_DRIVER_NEW], vehBike[0])
							FREEZE_ENTITY_POSITION(vehTruck, TRUE)
							bEventFlag[0] = TRUE
						ENDIF
					ENDIF
					
					SET_RAMP_OPEN(vehTruck, TRUE, 0.95)
				
				//CLEANUP
					IF TIMERA() > 5050//1250
						iStageSection++
						bCameraShotSetup = FALSE
					ENDIF
				ENDIF
			BREAK	
						
			CASE 4
				//SETUP
				IF NOT bCameraShotSetup	
					doTwoPointCam(<<668.667847,-1764.783325,9.198649>>,<<2.612283,-0.362411,165.775452>>,17.235420,
								  <<664.334167,-1764.783325,9.198649>>,<<2.612283,-0.362411,165.775452>>,17.235420,
								  20000)	
						
					//SETTIMERA(0)	
					SETTIMERB(0)	
					
					SET_CAM_DOF_PLANES(destinationCam,5.0, 15.0, 200.0, 225.0)
										
					bCameraShotSetup = TRUE
				//UPDATE
				ELSE
				
					SET_USE_HI_DOF()
				
					SET_RAMP_OPEN(vehTruck, TRUE, 0.95)
				
					IF NOT bEventFlag[0]
					AND TIMERA() > 5050
						IF HAS_VEHICLE_MOUNTED_TRUCK(vehBike[0])
							CLEAR_PED_DRIVE_TASKS(pedCrew[CREW_DRIVER_NEW], vehBike[0])
							bEventFlag[0] = TRUE
						ENDIF
					ENDIF
					
					IF NOT bEventFlag[1]
					AND TIMERA() > 5050
						IF NOT bWorstGunDies
							IF HAS_VEHICLE_MOUNTED_TRUCK(vehBike[1])
								CLEAR_PED_DRIVE_TASKS(pedCrew[CREW_GUNMAN_NEW], vehBike[1])
								bEventFlag[1] = TRUE
							ENDIF
						ELSE
							bEventFlag[1] = TRUE
						ENDIF
					ENDIF
					IF NOT bEventFlag[2]
					AND TIMERA() > 5050 + 350
						IF HAS_VEHICLE_MOUNTED_TRUCK(vehBike[2])
							CLEAR_PED_DRIVE_TASKS(GET_SELECT_PED(SEL_FRANKLIN), vehBike[2])
							
							SET_RAMP_OPEN(vehTruck, FALSE)
							FREEZE_ENTITY_POSITION(vehTruck, FALSE)
							
							bEventFlag[2] = TRUE
						ENDIF						
					ENDIF
				
				//CLEANUP
					bEventFlag[3] = TRUE
					
					IF GET_CURRENT_SELECT_PED() = SEL_FRANKLIN
						IF NOT bWorstGunDies
							IF NOT HAS_VEHICLE_MOUNTED_TRUCK(vehBike[0])
								bEventFlag[3] = FALSE
							ENDIF
						ENDIF
					ELSE 
						IF NOT HAS_VEHICLE_MOUNTED_TRUCK(vehBike[0])
							bEventFlag[3] = FALSE
						ENDIF
						IF NOT bWorstGunDies
							IF NOT HAS_VEHICLE_MOUNTED_TRUCK(vehBike[1])
								bEventFlag[3] = FALSE
							ENDIF
						ENDIF
						IF NOT HAS_VEHICLE_MOUNTED_TRUCK(vehBike[2])
							bEventFlag[3] = FALSE
						ENDIF
					ENDIF
					
					IF (NOT bWorstGunDies AND TIMERB() > 5050 + 2500)
					OR (bWorstGunDies AND  TIMERB() > 5050 + 3300)
					//AND bEventFlag[2] = TRUE
					//AND NOT bEventFlag[4]
						SET_RAMP_OPEN(vehTruck, FALSE)
						FREEZE_ENTITY_POSITION(vehTruck, FALSE)						
					ENDIF
					
					IF TIMERB() > 5050 + 6500
					AND NOT bEventFlag[4]
						
						bEventFlag[4] = TRUE
					ENDIF
					
					IF (bEventFlag[3] AND TIMERB() > 5000)
					OR TIMERB() > 5050 + 7550//300
						iStageSection++
						
						doTwoPointCam(<<638.271179,-1836.951416,11.575692>>,<<-7.141413,-0.000000,169.558517>>,50.000000,
								  <<638.267822,-1836.969849,12.075953>>,<<-2.140849,0.000000,169.558517>>,50.000000,
								  2000, GRAPH_TYPE_DECEL)	
						SET_RAMP_OPEN(vehTruck, FALSE)
						
						SET_CAM_DOF_PLANES(destinationCam,0.0, 3.0, 175.0, 200.0)
						
						PLAY_SOUND_FRONTEND(-1, "RAMP_UP", "TRUCK_RAMP_DOWN")							
													
						SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
						SETTIMERB(0)
						//SETTIMERA(0)
						bCameraShotSetup = FALSE
					ENDIF
				ENDIF
			BREAK	
			
			CASE 5
				
				SET_USE_HI_DOF()
				
				IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_PARK", CONV_PRIORITY_VERY_HIGH)
					iStageSection++
				ENDIF
			BREAK
			
			CASE 6
			
				SET_USE_HI_DOF()
				
				IF TIMERB() > 2000				
					iStageSection++
				ENDIF
			BREAK
			
			/*CASE 4
				//SETUP
				IF NOT bCameraShotSetup	
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
					RENDER_SCRIPT_CAMS(FALSE, TRUE, 2000)
					SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
					
					SETTIMERA(0)	
					bCameraShotSetup = TRUE
				//UPDATE
				ELSE					
					//SET_RAMP_OPEN(vehTruck, TRUE, 0.95)
				//CLEANUP
					IF TIMERA() > 2000
						DO_END_CUTSCENE()

						iStageSection++
						bCameraShotSetup = FALSE
					ENDIF
				ENDIF
			BREAK*/
		ENDSWITCH
				
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 7
		
			REPLAY_STOP_EVENT()
		
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)

			SET_FOLLOW_VEHICLE_CAM_HIGH_ANGLE_MODE_EVERY_UPDATE(FALSE, FALSE)
		
			ROLL_UP_WINDOW(vehTruck, SC_WINDOW_FRONT_LEFT)
			END_SRL()
			
			DO_END_CUTSCENE(TRUE, TRUE)
			
			//NEW_LOAD_SCENE_STOP()
			
			SET_PARTICLE_FX_BANG_SCRAPE_LODRANGE_SCALE(1.0)
			
			FREEZE_ENTITY_POSITION(vehTruck, FALSE)
		
			STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[0])
			IF NOT bWorstGunDies
				STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[1])
			ENDIF
			STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[2])
			
			IF NOT IS_ENTITY_DEAD(vehTruck)
				IF NOT IS_ENTITY_DEAD(vehBike[0])
					//IF NOT IS_ENTITY_ATTACHED(vehBike[0])
						ATTACH_ENTITY_TO_ENTITY(vehBike[0], vehTruck, 0, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehTruck, GET_ENTITY_COORDS(vehBike[0])), GET_ENTITY_ROTATION(vehBike[0]) - GET_ENTITY_ROTATION(vehTruck))
						SET_ENTITY_COLLISION(vehBike[0], FALSE)
						SET_AUDIO_VEHICLE_PRIORITY(vehBike[0], AUDIO_VEHICLE_PRIORITY_NORMAL)
					//	SCRIPT_ASSERT("attaching 0")
					//ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(vehBike[1])
					//IF NOT IS_ENTITY_ATTACHED(vehBike[1])
						ATTACH_ENTITY_TO_ENTITY(vehBike[1], vehTruck, 0, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehTruck, GET_ENTITY_COORDS(vehBike[1])), GET_ENTITY_ROTATION(vehBike[1]) - GET_ENTITY_ROTATION(vehTruck))
						SET_ENTITY_COLLISION(vehBike[1], FALSE)
						SET_AUDIO_VEHICLE_PRIORITY(vehBike[1], AUDIO_VEHICLE_PRIORITY_NORMAL)
						//SCRIPT_ASSERT("attaching 1")
					//ENDIF
				ENDIF
				IF NOT IS_ENTITY_DEAD(vehBike[2])
					//IF NOT IS_ENTITY_ATTACHED(vehBike[2])
						ATTACH_ENTITY_TO_ENTITY(vehBike[2], vehTruck, 0, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehTruck, GET_ENTITY_COORDS(vehBike[2])), GET_ENTITY_ROTATION(vehBike[2]) - GET_ENTITY_ROTATION(vehTruck))
						SET_ENTITY_COLLISION(vehBike[2], FALSE)
						SET_AUDIO_VEHICLE_PRIORITY(vehBike[2], AUDIO_VEHICLE_PRIORITY_NORMAL)
					//	SCRIPT_ASSERT("attaching 2")
					//ENDIF
				ENDIF
			ENDIF
			
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehBike[0])
			IF DOES_ENTITY_EXIST(vehBike[1])
				REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehBike[1])
			ENDIF
			REMOVE_ENTITY_FROM_AUDIO_MIX_GROUP(vehBike[2])
			
			REMOVE_VEHICLE_RECORDING(1, "JHLoadVan")
			REMOVE_VEHICLE_RECORDING(2, "JHLoadVan")
			REMOVE_VEHICLE_RECORDING(3, "JHLoadVan")
			
			REMOVE_VEHICLE_RECORDING(700, "JHUber")
			
			SET_RAMP_OPEN(vehTruck, FALSE)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			bStageSetup = FALSE
			bCameraShotSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//BOOL bMocapRequested

OBJECT_INDEX oiTarp
BOOL bCheckForModels
BOOL bLesterCreated = FALSE
BOOL bVanForwards

INT iTimeOfSwearing

//PURPOSE:		Performs the section where Franklin drives to behind the store
FUNC BOOL stageReturnToBase()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup	
		
		IF bIsStealthPath
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(8, "STAGE_RETURN_TO_BASE")
		ELSE
			SET_REPLAY_MID_MISSION_STAGE_WITH_NAME(7, "STAGE_RETURN_TO_BASE")
		ENDIF
		SET_AUDIO_FLAG("OnlyAllowScriptTriggerPoliceScanner", TRUE)
		g_replay.iReplayInt[CURRENT_TAKE_REPLAY_VALUE] = iCurrentTake
		
		LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_DRAINSERVICE_L, TRUE)
		LOCK_AUTOMATIC_DOOR_OPEN(AUTODOOR_DRAINSERVICE_R, TRUE)
		
		RESET_NEED_LIST()
		MANAGE_LOADING_NEED_LIST()

		SET_CINEMATIC_BUTTON_ACTIVE(TRUE)	
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
		iStageSection = 0
		
		IF NOT IS_ENTITY_DEAD(vehTruck)
			SET_DOOR_ALLOWED_TO_BE_BROKEN_OFF(vehTruck, SC_DOOR_BOOT, FALSE)
			SET_ENTITY_PROOFS(vehTruck, FALSE, FALSE, FALSE, FALSE, FALSE)
		ENDIF
		
		IF NOT IS_PED_INJURED(pedCrew[CREW_HACKER])
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedCrew[CREW_HACKER], TRUE)
		ENDIF
		
		SWITCH_ALL_RANDOM_CARS_ON()
				
		DISTANT_COP_CAR_SIRENS(FALSE)
		
		SAFE_ADD_PED_FOR_DIALOGUE(GET_SELECT_PED(SEL_MICHAEL), ConvertSingleCharacter("0"), "MICHAEL")
		SAFE_ADD_PED_FOR_DIALOGUE(NULL, ConvertSingleCharacter("1"), "FRANKLIN")
	
		IF IS_DRIVER_EDDIE()
			SAFE_ADD_PED_FOR_DIALOGUE(NULL, 3, "EDDIE")
		ELIF IS_DRIVER_TALINA()
			SAFE_ADD_PED_FOR_DIALOGUE(NULL, 3, "TALINA")
		ELIF IS_DRIVER_KARIM()
			SAFE_ADD_PED_FOR_DIALOGUE(NULL, 3, "KARIM")
		ENDIF
				
		IF IS_GUNMAN_PACKIE()
			SAFE_ADD_PED_FOR_DIALOGUE(NULL, 4, "PACKIE")
		ELIF IS_GUNMAN_GUSTAVO()
			SAFE_ADD_PED_FOR_DIALOGUE(NULL, 4, "GUSTAVO")//"JewelGunGood")
		ELIF IS_GUNMAN_NORM()
			SAFE_ADD_PED_FOR_DIALOGUE(NULL, 4, "NORM")//"JewelGunBad")
		ENDIF
	
		SWITCH GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)) 
			CASE CM_HACKER_G_PAIGE
				SAFE_ADD_PED_FOR_DIALOGUE(pedCrew[CREW_HACKER], 7, "PAIGE")
			BREAK
		
			CASE CM_HACKER_M_CHRIS
				SAFE_ADD_PED_FOR_DIALOGUE(pedCrew[CREW_HACKER], 7, "CHRISTIAN")
			BREAK
			
			CASE CM_HACKER_B_RICKIE_UNLOCK
				SAFE_ADD_PED_FOR_DIALOGUE(pedCrew[CREW_HACKER], 7, "LIEngineer")
			BREAK
		ENDSWITCH
	
		SETTIMERA(0)
		
		SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), 0)
		
		SET_WANTED_LEVEL_MULTIPLIER(0.5)
		
		ENABLE_ALL_DISPATCH_SERVICES(TRUE)
		SET_CREATE_RANDOM_COPS(TRUE)
		
		bBensonRecordingHelpStarted = FALSE
		
		IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
			REMOVE_PED_HELMET(GET_SELECT_PED(SEL_FRANKLIN), TRUE)
		ENDIF
		
		RELEASE_NAMED_SCRIPT_AUDIO_BANK("JWL_HEIST_CAR_SMASHES_BIG")
		
		bCheckForModels = FALSE
		bLesterCreated = FALSE
		
		ADD_SCENARIO_BLOCKING_AREA(<<192.9551, -2060.9285, 10.8338>>, <<231.7729, -1969.8544, 34.4133>>)
	
		INFORM_MISSION_STATS_OF_DAMAGE_WATCH_ENTITY(vehTruck,JH2A_CAR_DAMAGE) 
		STOP_AUDIO_SCENE("JSH_2B_LOAD_UP_TRUCK")
		IF NOT IS_AUDIO_SCENE_ACTIVE("JSH_2B_GO_TO_LOCKUP")
			START_AUDIO_SCENE("JSH_2B_GO_TO_LOCKUP")
		ENDIF
		
		REPLAY_RECORD_BACK_FOR_TIME(0.0, 10.0, REPLAY_IMPORTANCE_HIGHEST)
		
//		IF NOT bWorstGunDies
//			INCREMENT_GUNMAN_STATS_DURING_HEIST(HEIST_JEWEL, GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_GUNMAN_NEW)))
//		ENDIF
//		
//		INCREMENT_DRIVER_STATS_DURING_HEIST(HEIST_JEWEL, GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_DRIVER_NEW)))
//		INCREMENT_HACKER_STATS_DURING_HEIST(HEIST_JEWEL, GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER)))
		
		INCREMENT_ALL_CREW_MEMBER_STATS_DURING_HEIST(HEIST_JEWEL, TRUE)
		
		SAFE_FADE_IN()

		//bMocapRequested = FALSE
		bStageSetup = TRUE
		iStageSection = 0
//═════════╡ UPDATE ╞═════════
	ELSE
	
//		IF TIMERA() > 10000
//		AND IS_CONVERSATION_STATUS_FREE()
//			SAFE_FADE_OUT(DEFAULT_FADE_TIME_LONG)
//			IF NOT IS_CELLPHONE_DISABLED()
//				DISABLE_CELLPHONE(TRUE)
//			ENDIF
//		ENDIF
//		IF IS_SCREEN_FADED_OUT()
//			iStageSection = 3 //99
//		ENDIF
	
//		NAVDATA ndStruct
//	
//		ndStruct.m_fSlideToCoordHeading = 67.6095
		
		SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
		
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(PLAYER_PED_ID(), <<207.8754, -2024.4990, 17.2804>>) < 200.0//DEFAULT_CUTSCENE_LOAD_DIST
		AND bCheckForModels = FALSE
			REQUEST_ANIM_DICT("missheist_jewel")
			REQUEST_MODEL(IG_LESTERCREST)
			
			REQUEST_MODEL(P_clothTarp_S)
			REQUEST_MODEL(asset.mnLestersStick)
			REQUEST_ANIM_DICT("p_clothtarp_s")
			REQUEST_ANIM_SET("move_lester_CaneUp")
			REQUEST_ANIM_DICT("missheist_jewel@tarp")
			REQUEST_AMBIENT_AUDIO_BANK("JWL_HEIST_FINALE_TARP")
			
			REQUEST_VEHICLE_RECORDING(1, "JHTarpScene")
			REQUEST_VEHICLE_RECORDING(2, "JHTarpScene")
									
			bCheckForModels = TRUE
	 	ENDIF
		
		
		
		IF bCheckForModels = TRUE
		AND bLesterCreated = FALSE
			
			IF  HAS_MODEL_LOADED(IG_LESTERCREST)
			AND HAS_MODEL_LOADED(P_clothTarp_S)
			AND HAS_MODEL_LOADED(asset.mnLestersStick)
			AND HAS_ANIM_DICT_LOADED("missheist_jewel")
			AND HAS_ANIM_DICT_LOADED("p_clothtarp_s")
			AND HAS_ANIM_DICT_LOADED("missheist_jewel@tarp")
			AND HAS_ANIM_SET_LOADED("move_lester_CaneUp")
			AND NOT DOES_ENTITY_EXIST(pedLester)
			AND REQUEST_AMBIENT_AUDIO_BANK("JWL_HEIST_FINALE_TARP")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "JHTarpScene")
			AND HAS_VEHICLE_RECORDING_BEEN_LOADED(2, "JHTarpScene")
		//	AND HAS_CUTSCENE_LOADED()
			
				pedLester = CREATE_PED(PEDTYPE_MISSION, IG_LESTERCREST, <<206.7173, -2024.6572, 17.2513>>, 227.0582)
				
				SET_PED_COMPONENT_VARIATION(pedLester, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
				SET_PED_COMPONENT_VARIATION(pedLester, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
				SET_PED_COMPONENT_VARIATION(pedLester, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(pedLester, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(pedLester, INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
				SET_PED_COMPONENT_VARIATION(pedLester, INT_TO_ENUM(PED_COMPONENT,7), 0, 0, 0) //(teef)
				SET_PED_COMPONENT_VARIATION(pedLester, INT_TO_ENUM(PED_COMPONENT,8), 1, 0, 0) //(accs)
				SET_PED_PROP_INDEX(pedLester, INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)

				SET_ENTITY_HEALTH(pedLester, 500)
				
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedLester, FALSE)
				
				//pedCrew[CREW_HACKER] = CREATE_HEIST_CREW_MEMBER_AT_INDEX(	HEIST_JEWEL, ENUM_TO_INT(CREW_HACKER), <<206.7735, -2023.0751, 17.2599>>, 336.9657)
				TASK_PLAY_ANIM(pedLester , "missheist_jewel", "hacker_waive_in_lester", NORMAL_BLEND_IN, SLOW_BLEND_OUT, -1, AF_LOOPING)
				TASK_LOOK_AT_ENTITY(pedLester, GET_SELECT_PED(SEL_MICHAEL), INFINITE_TASK_TIME, SLF_WHILE_NOT_IN_FOV)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedLester, TRUE)
				SET_PED_RELATIONSHIP_GROUP_HASH(pedLester, RELGROUPHASH_PLAYER)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedLester, TRUE)
				
				SET_PED_LOD_MULTIPLIER(pedLester, 2.0)
				
				oiLestersStick = CREATE_OBJECT(asset.mnLestersStick, GET_ENTITY_COORDS(pedLester, FALSE))
				ATTACH_ENTITY_TO_ENTITY(oiLestersStick, pedLester, GET_PED_BONE_INDEX(pedLester, BONETAG_PH_R_HAND), <<0.0, 0.0, 0.0>>, <<0.0, 0.0, 0.0>>)
						
						
				SET_PED_WEAPON_MOVEMENT_CLIPSET(pedLester, "move_lester_CaneUp")
				
				ADD_PED_FOR_DIALOGUE(myScriptedSpeech, ConvertSingleCharacter("E"), pedLester, "LESTER", FALSE)
						
				//SET_PED_MOVEMENT_CLIPSET(pedLester,"move_lester_CaneUp")
				//oiTarp = CREATE_OBJECT(P_clothTarp_up_S, vTarpPos)
				//SET_ENTITY_ROTATION(oiTarp, vTarpRot)
								
				oiTarp = CREATE_OBJECT(P_clothTarp_S, vTarpPos)
				SET_ENTITY_ROTATION(oiTarp, vTarpRot)
				
				bLesterCreated = TRUE
				
			ENDIF
		ENDIF
		
		IF DOES_ENTITY_EXIST(oiTarp)
			SET_ENTITY_COORDS(oiTarp, vTarpPos)
			SET_ENTITY_ROTATION(oiTarp, vTarpRot)
		ENDIF
		
		REPLAY_DISABLE_CAMERA_MOVEMENT_THIS_FRAME()	//Fix for bug 2045353
		
		IF DOES_ENTITY_EXIST(GET_SELECT_PED(SEL_FRANKLIN))
		AND DOES_ENTITY_EXIST(vehTruck)
			IF GET_VEHICLE_DOOR_ANGLE_RATIO(vehTruck, SC_DOOR_BOOT) < 0.05
				DELETE_VEHICLE(vehBike[0])
				DELETE_VEHICLE(vehBike[1])
				DELETE_VEHICLE(vehBike[2])
				DELETE_PED(pedCrew[CREW_DRIVER_NEW])
				DELETE_PED(pedCrew[CREW_GUNMAN_NEW])
				SET_ENTITY_VISIBLE(GET_SELECT_PED(SEL_FRANKLIN), FALSE)
				
			ELSE
				IF NOT IS_ENTITY_DEAD(vehTruck)
					IF NOT IS_ENTITY_DEAD(vehBike[0])
						IF NOT IS_ENTITY_ATTACHED(vehBike[0])
							ATTACH_ENTITY_TO_ENTITY(vehBike[0], vehTruck, 0, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehTruck, GET_ENTITY_COORDS(vehBike[0])), GET_ENTITY_ROTATION(vehBike[0]) - GET_ENTITY_ROTATION(vehTruck))
							SET_ENTITY_COLLISION(vehBike[0], FALSE)
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_DEAD(vehBike[1])
						IF NOT IS_ENTITY_ATTACHED(vehBike[1])
							ATTACH_ENTITY_TO_ENTITY(vehBike[1], vehTruck, 0, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehTruck, GET_ENTITY_COORDS(vehBike[1])), GET_ENTITY_ROTATION(vehBike[1]) - GET_ENTITY_ROTATION(vehTruck))
							SET_ENTITY_COLLISION(vehBike[1], FALSE)
						ENDIF
					ENDIF
					IF NOT IS_ENTITY_DEAD(vehBike[2])
						IF NOT IS_ENTITY_ATTACHED(vehBike[2])
							ATTACH_ENTITY_TO_ENTITY(vehBike[2], vehTruck, 0, GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(vehTruck, GET_ENTITY_COORDS(vehBike[2])), GET_ENTITY_ROTATION(vehBike[2]) - GET_ENTITY_ROTATION(vehTruck))
							SET_ENTITY_COLLISION(vehBike[2], FALSE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
		SWITCH iStageSection

			CASE 0
				IF bIsStealthPath
					IF PREPARE_MUSIC_EVENT("JH2A_ARRIVE_STOP_TRACK")
						TRIGGER_MUSIC_EVENT("JH2A_ARRIVE_STOP_TRACK")
						iStageSection = 2
					ENDIF
				ELSE
					IF PREPARE_MUSIC_EVENT("JH2B_ARRIVE_STOP_TRACK")
						TRIGGER_MUSIC_EVENT("JH2B_ARRIVE_STOP_TRACK")
						iStageSection = 2
					ENDIF
				ENDIF
				
				iStageSection = 2
				
			BREAK
			
			CASE 2
			CASE 3
			CASE 4
			CASE 5
			CASE 6
			CASE 7
			CASE 8
			CASE 9
			CASE 10
			CASE 11
			CASE 12
				
				IF DOES_BLIP_EXIST(sLocatesData.LocationBlip)
				
					IF iStageSection = 2
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
						AND IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<207.8754, -2024.4990, 17.2804>>, <<255.0, 255.0, 20.0>>)
							IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_END2", CONV_PRIORITY_VERY_HIGH)
								iStageSection = 3
							ENDIF
						ENDIF
					ENDIF
				
					IF iStageSection = 3
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<207.8754, -2024.4990, 17.2804>>, <<255.0, 255.0, 20.0>>)
							IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_CELEBRATE", CONV_PRIORITY_VERY_HIGH)
								iStageSection = 4
							ENDIF
						ENDIF
					ENDIF
					
					IF iStageSection = 4
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_CELEB", CONV_PRIORITY_VERY_HIGH)
								iStageSection = 5
							ENDIF
						ENDIF
					ENDIF
										
					//Gunman celebratates
					IF iStageSection = 5
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							IF IS_GUNMAN_GUSTAVO()
								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_CELEB_GM", CONV_PRIORITY_VERY_HIGH)
									iStageSection = 6
								ENDIF
							ELIF IS_GUNMAN_PACKIE()
								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_CELEB_PM", CONV_PRIORITY_VERY_HIGH)
									iStageSection = 6
								ENDIF
							ENDIF		
						ENDIF
						IF bWorstGunDies
							iStageSection = 6	
						ENDIF
					ENDIF
										
					//Driver celebratates
					IF iStageSection = 6
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							IF IS_DRIVER_EDDIE()
								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_CEL_ET", CONV_PRIORITY_VERY_HIGH)
									iStageSection = 7
								ENDIF
							ELSE
								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_CEL_KD", CONV_PRIORITY_VERY_HIGH)
									iStageSection = 7
								ENDIF
							ENDIF		
						ENDIF
					ENDIF
					
					//Hacker celebratates
					IF iStageSection = 7
						IF NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							IF IS_HACKER_PAIGE()
								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_HCKCEL_PH", CONV_PRIORITY_VERY_HIGH)
									iStageSection = 8
								ENDIF		
							ELIF IS_HACKER_CHRISTIAN()
								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_HCKCEL_CF", CONV_PRIORITY_VERY_HIGH)
									iStageSection = 8
								ENDIF
							ELIF IS_HACKER_RICKIE()
								IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_HCKCEL_RL", CONV_PRIORITY_VERY_HIGH)
									iStageSection = 8
								ENDIF	
							ENDIF		
						ENDIF
					ENDIF
													
				
					IF iStageSection = 8
						//IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<207.8754, -2024.4990, 17.2804>>, <<90.0, 90.0, 20.0>>)
						//AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							//IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_PARK", CONV_PRIORITY_VERY_HIGH)
								iStageSection = 9
							//ENDIF
						//ENDIF
					ENDIF
				
					IF iStageSection = 9
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<207.8754, -2024.4990, 17.2804>>, <<20.0, 20.0, 20.0>>)
							IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_WAVEIN", CONV_PRIORITY_VERY_HIGH)
							
								REPLAY_RECORD_BACK_FOR_TIME(5.0, 5.0, REPLAY_IMPORTANCE_HIGHEST)
							
								iStageSection = 10
							ENDIF
						ENDIF
					ENDIF
					
					IF iStageSection = 10
						IF CREATE_CONVERSATION(myScriptedSpeech, "JHAUD", "JH_GEMGUY", CONV_PRIORITY_VERY_HIGH)
						AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
							iStageSection = 11
						ENDIF
					ENDIF
				
					IF iStageSection = 11
						IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), <<204.2339, -2003.6603, 17.8616>>, <<20.5,20.5,4.0>>)				
							IF NOT IS_PED_INJURED(pedLester)
								CLEAR_PED_TASKS(pedLester)
								TASK_TURN_PED_TO_FACE_ENTITY(pedLester, vehTruck, 15000)
								
								//TASK_FOLLOW_TO_OFFSET_OF_ENTITY(pedLester, vehTruck,  <<1.0, -8.0, 0.0>>, PEDMOVE_RUN)
								
								iStageSection = 12
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
					
				IF NOT IS_ENTITY_DEAD(pedLester)
					IF IS_ENTITY_TOUCHING_ENTITY(vehTruck, pedLester)
					AND GET_GAME_TIMER() - iTimeOfSwearing > 6000
						PLAY_PED_AMBIENT_SPEECH_WITH_VOICE(pedLester, "GENERIC_CURSE_HIGH", "LESTER", SPEECH_PARAMS_FORCE)
						iTimeOfSwearing = GET_GAME_TIMER()
					ENDIF
				ENDIF
								
								
				//IF IS_PLAYER_AT_LOCATION_ANY_MEANS(sLocatesData, <<204.2339, -2003.6603, 17.8616>>, <<4.5,4.5,2.0>>, TRUE, "G_MIKEHOUSE")
				IF IS_PLAYER_AT_LOCATION_WITH_BUDDIES_IN_VEHICLE(sLocatesData, <<204.2339, -2003.6603, 17.8616>>, <<0.1,0.1,2.0>>, TRUE, pedCrew[CREW_HACKER], NULL, NULL, vehTruck, "G_MIKEHOUSE", "", "","", "", "G_JHTRUCK", "G_JHTRUCK", TRUE, TRUE)
				//OR (DOES_BLIP_EXIST(sLocatesData.LocationBlip) AND IS_ENTITY_IN_ANGLED_AREA(vehTruck, <<201.136108,-2005.871582,22.287184>>, <<207.223114,-1998.005005,16.698507>>, 3.85 ))//750000))
				OR (DOES_BLIP_EXIST(sLocatesData.LocationBlip) AND IS_ENTITY_IN_ANGLED_AREA(vehTruck, <<209.606293,-2005.642456,17.361458>>, <<212.183609,-2012.635254,21.525574>>, 11.000000))
	
					HIDE_HUD_AND_RADAR_THIS_FRAME()
	
					REQUEST_CUTSCENE("JH_2_CELEB")
	
					//SET_VEHICLE_LOD_CLAMP(vehTruck, 0)
					
					KILL_ANY_CONVERSATION()
					
					CREATE_MODEL_HIDE(<<207.3416, -2009.9231, 17.8473>>, 1.0, PROP_RUB_BINBAG_03,  TRUE)
							
					CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
					STOP_AUDIO_SCENE("JSH_2B_GO_TO_LOCKUP")
					
					IF NOT DOES_CAM_EXIST(initialCam)
						initialCam	= CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
					ENDIF
					IF NOT DOES_CAM_EXIST(destinationCam)
						destinationCam	= CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
					ENDIF
								
					SET_CAM_COORD(initialCam,<<210.349136,-2023.884766,19.040403>>)
					SET_CAM_ROT(initialCam,<<6.383267,0.000000,8.404858>>)
					SET_CAM_FOV(initialCam,39.818157)

					SET_CAM_COORD(destinationCam,<<210.975830,-2022.583740,23.660158>>)
					SET_CAM_ROT(destinationCam,<<5.365558,0.000000,13.652684>>)
					SET_CAM_FOV(destinationCam,39.818157)
								
					SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 14000)
					
					SHAKE_CAM(destinationCam, "HAND_SHAKE", 0.4)
					
					RENDER_SCRIPT_CAMS(TRUE, FALSE, 3500)
					SETTIMERA(0)
					CLEAR_AREA(<<204.7989, -2002.9592, 17.8615>>, 80.00, TRUE, TRUE, TRUE)
					REMOVE_DECALS_IN_RANGE(<<204.7989, -2002.9592, 17.8615>>, 80.00)
					SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
										
					IF IS_HEADING_ACCEPTABLE(GET_ENTITY_HEADING(vehTruck), 15.0468, 90.0)
						START_PLAYBACK_RECORDED_VEHICLE(vehTruck, 1, "JHTarpScene")
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTruck, 4600.0)
						bVanForwards = TRUE
					ELSE
						START_PLAYBACK_RECORDED_VEHICLE(vehTruck, 2, "JHTarpScene")
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehTruck, 4400.0)//3750.0)
						bVanForwards = FALSE
					ENDIF
					
					FORCE_PLAYBACK_RECORDED_VEHICLE_UPDATE(vehTruck)
									
					SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
										
					SETTIMERA(0)
					iStageSection = 13
				ENDIF
			BREAK
			
			CASE 13
								
				SET_CUTSCENE_PED_COMPONENT_VARIATION_FROM_PED("Michael", PLAYER_PED_ID())
			
				IF NOT IS_ENTITY_DEAD(vehTruck)
					SET_FORCE_HD_VEHICLE(vehTruck, TRUE)
				ENDIF
				
				IF bIsStealthPath
			
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,0), 0, 0)  //(head)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,2), 0, 0)  //(hair)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,3), 1, 0)  //(uppr)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,4), 1, 0)  //(lowr)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,5), 0, 0)  //(hand)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,7), 0, 0)  //(teef)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,8), 1, 0)  //(accs)
					SET_CUTSCENE_PED_PROP_VARIATION("Lester", INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
			
					
					SET_CUTSCENE_PED_OUTFIT("Franklin", PLAYER_ONE, OUTFIT_P1_EXTERMINATOR)
			
					
					SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("Driver_selection", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_DRIVER_NEW)), CREW_OUTFIT_PEST_CONTROL)
					IF NOT bWorstGunDies
						SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("gunman_selection_1", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_GUNMAN_NEW)), CREW_OUTFIT_PEST_CONTROL)
					ENDIF
					SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("hacker_selection", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_HACKER)), CREW_OUTFIT_DEFAULT)
							
				ELSE
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,0), 0, 0)  //(head)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,2), 0, 0)  //(hair)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,3), 1, 0)  //(uppr)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,4), 1, 0)  //(lowr)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,5), 0, 0)  //(hand)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,7), 0, 0)  //(teef)
					SET_CUTSCENE_PED_COMPONENT_VARIATION("Lester", INT_TO_ENUM(PED_COMPONENT,8), 1, 0)  //(accs)
					SET_CUTSCENE_PED_PROP_VARIATION("Lester", INT_TO_ENUM(PED_PROP_POSITION,1), 0, 0)
					
					SET_CUTSCENE_PED_OUTFIT("Franklin", PLAYER_ONE, OUTFIT_P1_SUIT_1)

					SET_CUTSCENE_PED_PROP_VARIATION("Michael", INT_TO_ENUM(PED_PROP_POSITION,2), 0, 0)
					
					SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("Driver_selection", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_DRIVER_NEW)), CREW_OUTFIT_SUIT)
					IF NOT bWorstGunDies
						SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("gunman_selection_1", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_GUNMAN_NEW)), CREW_OUTFIT_SUIT)
					ENDIF
					SET_HEIST_CREW_MEMBER_OUTFIT_FOR_CUTSCENE("hacker_selection", GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_HACKER)), CREW_OUTFIT_DEFAULT)
				
				ENDIF
			
				IF NOT IS_ENTITY_DEAD(vehTruck)
					SET_FORCE_HD_VEHICLE(vehTruck, TRUE)
				ENDIF
			
				HIDE_HUD_AND_RADAR_THIS_FRAME()
			
				IF TIMERA() > 3000
				AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
					TASK_LEAVE_ANY_VEHICLE(PLAYER_PED_ID())
				ENDIF
			
				IF TIMERA() > 5000
				AND NOT IS_SCRIPTED_CONVERSATION_ONGOING()
				AND HAS_CUTSCENE_LOADED_WITH_FAILSAFE()
									
					IF bWorstGunDies
						REGISTER_ENTITY_FOR_CUTSCENE(pedCrew[CREW_GUNMAN_NEW], "gunman_selection_1", CU_DONT_ANIMATE_ENTITY, HC_GUNMAN)
					ENDIF		
					
					SET_VEHICLE_ENGINE_ON(vehTruck, FALSE, FALSE)
					
					//REGISTER_ENTITY_FOR_CUTSCENE(vehTruck, "BENSON", CU_ANIMATE_AND_DELETE_EXISTING_SCRIPT_ENTITY)
										
					START_CUTSCENE()
					
					REPLAY_START_EVENT(REPLAY_IMPORTANCE_HIGHEST)
					
					//SET_ENTITY_ANIM_CURRENT_TIME(pedLester, "missheist_jewel@tarp", "les_releases_tarp", 0.0)
					SETTIMERA(0)
					iStageSection = 14
				ENDIF
			BREAK
			
			CASE 14
			
				IF NOT IS_ENTITY_DEAD(vehTruck)
					SET_FORCE_HD_VEHICLE(vehTruck, TRUE)
				ENDIF
			
				SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			
				IF IS_CUTSCENE_PLAYING()
				
					IF bVanForwards = TRUE
						SET_ENTITY_COORDS(vehTruck, <<204.7989, -2002.9592, 17.8615>>)
					ENDIF
					//SET_ENTITY_HEADING(vehTruck, 53.1227)
					CLEAR_AREA(<<204.7989, -2002.9592, 17.8615>>, 80.00, TRUE, TRUE, TRUE)
					REMOVE_DECALS_IN_RANGE(<<204.7989, -2002.9592, 17.8615>>, 80.00)
					SET_CUTSCENE_FADE_VALUES(FALSE, FALSE, TRUE, FALSE)
					DELETE_PED(pedLester)
					DELETE_PED(pedCrew[CREW_HACKER])
				ENDIF
			
				HIDE_HUD_AND_RADAR_THIS_FRAME()
			
				IF GET_CUTSCENE_TIME() > 2966.667
					PLAY_ENTITY_ANIM(oiTarp, "p_clothtarp_s_fall", "p_clothtarp_s", INSTANT_BLEND_IN, FALSE, TRUE)
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(oiTarp)
				
					PLAY_SOUND_FROM_ENTITY(-1, "TARP", oiTarp, "JEWEL_HEIST_SOUNDS")
				
					iStageSection =15				
				ENDIF
			BREAK
						
			CASE 15			
			
				IF NOT IS_ENTITY_DEAD(vehTruck)
					SET_FORCE_HD_VEHICLE(vehTruck, TRUE)
				ENDIF
			
				IF GET_CUTSCENE_TIME() > 34500.00
					IF NOT IS_SCREEN_FADING_OUT()
					AND NOT IS_SCREEN_FADED_OUT()
						//SET_NO_LOADING_SCREEN(TRUE) B* - 2021288
						DO_SCREEN_FADE_OUT(3000)
					ENDIF
				ENDIF
			
				IF NOT IS_CUTSCENE_PLAYING()
					iStageSection = 16
				ENDIF
			BREAK
			
		ENDSWITCH
			
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 16
			CLEAR_MISSION_LOCATION_TEXT_AND_BLIPS(sLocatesData)
			REMOVE_ANIM_DICT("missheist_jewel@hacking")
			SET_MODEL_AS_NO_LONGER_NEEDED(IG_LESTERCREST)
			//SET_MODEL_AS_NO_LONGER_NEEDED(P_clothTarp_down_S)
			//SET_MODEL_AS_NO_LONGER_NEEDED(P_clothTarp_up_S)
			SET_MODEL_AS_NO_LONGER_NEEDED(P_clothTarp_S)
			SET_MODEL_AS_NO_LONGER_NEEDED(asset.mnLestersStick)
			REMOVE_ANIM_DICT("p_clothtarp_s")
		
			IF DOES_BLIP_EXIST(blipLocate)
				REMOVE_BLIP(blipLocate)
			ENDIF
			IF DOES_BLIP_EXIST(blipTruck)
				REMOVE_BLIP(blipTruck)
			ENDIF
			
			REPLAY_STOP_EVENT()
			
			FOR iCounter = 0 TO 3
				CLEANUP_PED(pedCrew[iCounter], TRUE)
			ENDFOR
			FOR iCounter = 0 TO iNoOfCops-1
				CLEANUP_PED(pedPolice[iCounter], TRUE)
			ENDFOR
			FOR iCounter = 0 TO 3
				CLEANUP_PED(pedPolicePassenger[iCounter], TRUE)
			ENDFOR
			FOR iCounter = 0 TO NUMBER_OF_STORE_PEDS-1
				CLEANUP_PED(pedStore[iCounter], TRUE)
			ENDFOR
			CLEANUP_PED(pedSecurity, TRUE)
			CLEANUP_PED(pedBackroom, TRUE)
			CLEANUP_PED(pedLester, TRUE)
			
			CLEANUP_OBJECT(objGrenade, TRUE)
			FOR iCounter = 0 TO 3
				CLEANUP_VEHICLE(vehBike[iCounter], TRUE)
			ENDFOR
			CLEANUP_VEHICLE(vehCar, TRUE)
			FOR iCounter = 0 TO iNoOfCops-1
				CLEANUP_VEHICLE(vehPolice[iCounter], TRUE)
			ENDFOR
			CLEANUP_VEHICLE(vehTruck, TRUE)
			CLEANUP_VEHICLE(vehCollision, TRUE)
			CLEANUP_VEHICLE(vehVan, TRUE)
			CLEANUP_VEHICLE(vehControl, TRUE)
			SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
			
			RENDER_SCRIPT_CAMS(FALSE, FALSE, 3000)
			SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
			bStageSetup = FALSE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC


#IF IS_DEBUG_BUILD

VECTOR vJumpCoord = << -173.4798, -489.2333, 33.4486 >>
VECTOR vLandCoord = << -118.5983, -511.2776, 28.9481 >>
BLIP_INDEX blipJump
BLIP_INDEX blipLand

//PURPOSE:		Performs the section where the crew drive bikes into a moving truck
FUNC BOOL stageRecord()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
		DELETE_ALL_MISSION_ENTITIES(TRUE)
		
		RESET_NEED_LIST()
		SET_MODEL_AS_NEEDED(Asset.mnBike)
		SET_VEHICLE_RECORDING_AS_NEEDED(Asset.recUber, Asset.recNumber)
		SET_VEHICLE_RECORDING_AS_NEEDED(Asset.recUber, Asset.recNoGoodBikeChase)
		//SET_VEHICLE_RECORDING_AS_NEEDED(Asset.recUber, Asset.recNoBadBikeChase)
		MANAGE_LOADING_NEED_LIST()
		
		IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
		ENDIF

		blipJump = CREATE_BLIP_FOR_COORD(vJumpCoord)
		SET_BLIP_COLOUR(blipJump, BLIP_COLOUR_GREEN)
		blipLand = CREATE_BLIP_FOR_COORD(vLandCoord)
		SET_BLIP_COLOUR(blipLand, BLIP_COLOUR_RED)
		SET_VEHICLE_MODEL_IS_SUPPRESSED(Asset.mnBike, TRUE)

		vehBike[0] = CREATE_VEHICLE(Asset.mnBike, << 883.1086, -393.4241, 31.5234 >>, 95.0258)//vBikeSpawnCoords[0], fBikeSpawnHeading[0])
		SET_VEHICLE_COLOUR_COMBINATION(vehBike[0], 0)
		vehBike[1] = CREATE_VEHICLE(Asset.mnBike, vBikeSpawnCoords[1], fBikeSpawnHeading[1])
		SET_VEHICLE_COLOUR_COMBINATION(vehBike[1], 1)
		vehBike[2] = CREATE_VEHICLE(Asset.mnBike, vBikeSpawnCoords[2], fBikeSpawnHeading[2])
		SET_VEHICLE_COLOUR_COMBINATION(vehBike[2], 2)

		set_uber_parent_widget_group(widgetDebug)

		/*SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehBike[2])
		
		WHILE NOT CREATE_PLAYER_PED_INSIDE_VEHICLE(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, vehBike[1])
			safeWait(0)
		ENDWHILE
		
		WHILE NOT CREATE_NPC_PED_INSIDE_VEHICLE(pedLester, CHAR_LESTER, vehBike[0])
			safeWait(0)
		ENDWHILE

		INITIALISE_UBER_PLAYBACK(Asset.recUber)
		
		LOAD_AND_MANAGE_UBER_ARRAY()
				
		START_PLAYBACK_RECORDED_VEHICLE(vehBike[1], Asset.recNumber, Asset.recUber)
		
		SetPieceCarID[0] = vehBike[0]
		SetPieceCarProgress[0] = 3
		CREATE_ALL_WAITING_UBER_CARS()
		
		bShowTriggerCarPath = TRUE
		
		SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
		
		SET_PLAYBACK_SPEED(vehBike[1], 1.0)
		fMasterPlaybackSpeed = 1.0		*/	
		
		IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL))
			IF NOT IS_PED_INJURED(GET_SELECT_PED(SEL_FRANKLIN))
				SET_ENTITY_COORDS(GET_SELECT_PED(SEL_FRANKLIN), GET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL))+<<1,0,0>>)
			ENDIF
			IF NOT IS_PED_INJURED(pedCrew[CREW_DRIVER_NEW])
				SET_ENTITY_COORDS(pedCrew[CREW_DRIVER_NEW], GET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL))+<<-1,0,0>>)
			ENDIF
			IF NOT IS_PED_INJURED(pedCrew[CREW_GUNMAN_NEW])
				SET_ENTITY_COORDS(pedCrew[CREW_GUNMAN_NEW], GET_ENTITY_COORDS(GET_SELECT_PED(SEL_MICHAEL))+<<0,1,0>>)
			ENDIF
		ENDIF
		
		IF IS_VEHICLE_DRIVEABLE(vehBike[0])
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehBike[0])
		ENDIF
		IF DOES_ENTITY_EXIST(vehBike[1])
			DELETE_VEHICLE(vehBike[1])
		ENDIF
		IF DOES_ENTITY_EXIST(vehBike[2])
			DELETE_VEHICLE(vehBike[2])
		ENDIF
		
		INIT_UBER_RECORDING("UberImmyA")

		SAFE_FADE_IN()
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
		iStageSection = 0
		bStageSetup = TRUE
		
//═════════╡ UPDATE ╞═════════		
	ELSE	
		//PRINT_WITH_NUMBER_NOW("~1~", ROUND(GET_ENTITY_SPEED(vehBike[0])), 1000, 1)
		SWITCH iStageSection
			CASE 0	
//				IF IS_ENTITY_AT_COORD(PLAYER_PED_ID(), vJumpCoord, <<10, 10, 10>>)
//					IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), <<-172.249847,-486.534363,32.973804>>, 
//												<<-174.546890,-492.679413,37.430393>>, 4.375000)
//						SET_ENTITY_COLLISION(vehBike[0], FALSE)
//						SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
//					ELSE
//						SET_ENTITY_COLLISION(vehBike[0], TRUE)
//						SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
//					ENDIF
//				ELSE
//					SET_ENTITY_COLLISION(vehBike[0], TRUE)
//						SET_ENTITY_COLLISION(PLAYER_PED_ID(), TRUE)
//				ENDIF
			
				UPDATE_UBER_RECORDING()
				
				/*IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[1])
					IF IS_BUTTON_PRESSED(PAD1, CIRCLE)
						IF NOT DOES_CAM_EXIST(camAnim)
							IF IS_VEHICLE_DRIVEABLE(vehBike[0])
								camAnim = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
								ATTACH_CAM_TO_ENTITY(camAnim, vehBike[0], <<0,0,30>>)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								SET_CAM_ACTIVE(camAnim, TRUE)
							ENDIF
						ENDIF
						SET_CAM_ROT(camAnim, <<-90, 0, GET_ENTITY_HEADING(vehBike[0])>>)
					ELSE
						DESTROY_CAM(camAnim)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
					ENDIF
					//SET_PLAYBACK_SPEED(vehBike[1], 1.0)
					//UPDATE_UBER_PLAYBACK(vehBike[1], 1.0)
					//fMasterPlaybackSpeed = 1.0
					
					//SET_PLAYBACK_SPEED(vehBike[1], 0.95)
				ELSE
					CLEANUP_UBER_PLAYBACK_AND_BIKES()
				ENDIF*/
				//iStageSection++
			BREAK
		ENDSWITCH
	
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 2
		
			bStageSetup = FALSE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

//PURPOSE:		Performs the section where the crew drive bikes into a moving truck
FUNC BOOL stageRerecord()
INT iRec = 679
INT iReRecord = 669
STRING sRec = Asset.recTruckExit
MODEL_NAMES mnVehicle = Asset.mnTruck


//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
		DELETE_ALL_MISSION_ENTITIES(TRUE)
		
		SAFE_REQUEST_VEHICLE_RECORDING(iRec, sRec, TRUE, TRUE)
		SAFE_REQUEST_MODEL(mnVehicle, TRUE, TRUE)
		
		IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_MICHAEL
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_MICHAEL)
			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
		ENDIF

		vehTruck = CREATE_VEHICLE(mnVehicle, GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(iRec, 0, sRec))
		//[MF]Remove all randomly assigned extras from the vehicle (B* 1689820)
		REMOVE_ALL_VEHICLE_EXTRAS(vehTruck)		

		SET_VEHICLE_COLOURS(vehTruck, 128, 3) SET_VEHICLE_EXTRA(vehTruck, 6, FALSE) //SET_VEHICLE_LIVERY(vehTruck, 1)

		SET_PED_INTO_VEHICLE(GET_SELECT_PED(SEL_MICHAEL), vehTruck)
				
		START_PLAYBACK_RECORDED_VEHICLE(vehTruck, iRec, sRec)
		PAUSE_PLAYBACK_RECORDED_VEHICLE(vehTruck)

		bRecording = FALSE

		SAFE_FADE_IN()
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
		iStageSection = 0
		bStageSetup = TRUE
		
//═════════╡ UPDATE ╞═════════		
	ELSE		
		SWITCH iStageSection
			CASE 0
				IF (NOT IS_PED_INJURED(GET_SELECT_PED(SEL_MICHAEL)))
				AND (IS_VEHICLE_DRIVEABLE(vehTruck))
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1)
						UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehTruck)
					ENDIF
								
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD2)
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehTruck)
							STOP_PLAYBACK_RECORDED_VEHICLE(vehTruck)
						ENDIF
						START_PLAYBACK_RECORDED_VEHICLE(vehTruck, iRec, sRec)
						PAUSE_PLAYBACK_RECORDED_VEHICLE(vehTruck)
					ENDIF
								
					IF IS_BUTTON_JUST_PRESSED(PAD1, SQUARE)
						IF NOT bRecording       
							START_RECORDING_VEHICLE_TRANSITION_FROM_PLAYBACK(vehTruck, iReRecord, sRec, TRUE)
							bRecording = TRUE
						ELSE
							STOP_RECORDING_VEHICLE(vehTruck)
							bRecording = FALSE
						ENDIF
					ENDIF
				ENDIF

				//iStageSection++
			BREAK
		ENDSWITCH
	
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 2
		
			bStageSetup = FALSE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC



//PURPOSE:		Performs the section where the crew drive bikes into a moving truck
FUNC BOOL stagePlayback()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
		DELETE_ALL_MISSION_ENTITIES(TRUE)
		

		iDebugChaseBikeRec = Asset.recNoGoodBikeChase
		bWorstGunDies = FALSE
		
		RESET_NEED_LIST()
		SET_MODEL_AS_NEEDED(Asset.mnBike)
		SET_MODEL_AS_NEEDED(Asset.mnGunman)
		SET_VEHICLE_RECORDING_AS_NEEDED(Asset.recUber, Asset.recNumber)
		MANAGE_LOADING_NEED_LIST()
		
		LOAD_AND_MANAGE_UBER_ARRAY()
		SetPieceCarRecording[0] = Asset.recNoGoodBikeChase //iDebugChaseBikeRec
		
//		REQUEST_VEHICLE_RECORDING(501, "JHuber")
//		REQUEST_VEHICLE_RECORDING(504, "JHuber")
//		
//		WHILE NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(501, "JHuber")
//		OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(501, "JHuber")
//			WAIT(0)
//		ENDWHILE
//		
		g_bLateLeaveJewelStore = TRUE
		INT i_setpiece_size = COUNT_OF(SetPieceCarID)
		INT i_traffic_size = COUNT_OF(TrafficCarID)

		REPEAT i_setpiece_size iCounter
			IF SetPieceCarRecording[iCounter] > 0
				SAFE_REQUEST_VEHICLE_RECORDING(SetPieceCarRecording[iCounter], Asset.recUber, TRUE, TRUE)
			ENDIF
		ENDREPEAT

		REPEAT i_traffic_size iCounter
			IF TrafficCarRecording[iCounter] > 0
				SAFE_REQUEST_VEHICLE_RECORDING(TrafficCarRecording[iCounter], Asset.recUber, TRUE, TRUE)
			ENDIF
		ENDREPEAT
		
		IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
			IF NOT DOES_SELECT_PED_EXIST(SEL_FRANKLIN)
				WHILE NOT CREATE_PLAYER_PED_ON_FOOT(sSelectorPeds.pedID[SELECTOR_PED_FRANKLIN], CHAR_FRANKLIN, 
													vJewelryWatchCoords)
					WAIT(0)
				ENDWHILE
			ENDIF
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
		ENDIF
		
		/*IF GET_CURRENT_PLAYER_PED_ENUM() <> CHAR_FRANKLIN
			MAKE_SELECTOR_PED_SELECTION(sSelectorPeds, SELECTOR_PED_FRANKLIN)
			TAKE_CONTROL_OF_SELECTOR_PED(sSelectorPeds)
		ENDIF*/

		vehBike[0] = CREATE_VEHICLE(Asset.mnBike, vBikeSpawnCoords[0], fBikeSpawnHeading[0])
		SET_VEHICLE_COLOUR_COMBINATION(vehBike[0], 0)
//		vehBike[1] = CREATE_VEHICLE(Asset.mnBike, vBikeSpawnCoords[1], fBikeSpawnHeading[1])
//		SET_VEHICLE_COLOUR_COMBINATION(vehBike[1], 1)
		vehBike[2] = CREATE_VEHICLE(Asset.mnBike, vBikeSpawnCoords[2], fBikeSpawnHeading[2])
		SET_VEHICLE_COLOUR_COMBINATION(vehBike[2], 2)
		
		//pedCrew[CREW_DRIVER_NEW] = CREATE_PED_INSIDE_VEHICLE(vehBike[0], PEDTYPE_MISSION, Asset.mnGunman)
		//pedCrew[CREW_GUNMAN_NEW] = CREATE_PED_INSIDE_VEHICLE(vehBike[1], PEDTYPE_MISSION, Asset.mnGunman)
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehBike[2])
		
	//	FREEZE_ENTITY_POSITION(vehBike[2], TRUE)

		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		
		SWITCH_ALL_RANDOM_CARS_OFF()
				
		#IF IS_DEBUG_BUILD
		set_uber_parent_widget_group(widgetDebug)
		#ENDIF
		
		INITIALISE_UBER_PLAYBACK(Asset.recUber, Asset.recNumber, TRUE)
		
		LOAD_AND_MANAGE_UBER_ARRAY()
		
		
		//SetPieceCarRecording[0] = iDebugChaseBikeRec
		
		START_PLAYBACK_RECORDED_VEHICLE(vehBike[0], Asset.recNumber, Asset.recUber)
		//PAUSE_PLAYBACK_RECORDED_VEHICLE(vehBike[0])
		
		
//		SetPieceCarID[0] = vehBike[1]
//		SetPieceCarProgress[0] = 3
		
		UPDATE_UBER_PLAYBACK(vehBike[0], 0.0)
								
		SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
		
		SET_BIKE_STABILISERS(PLAYER_PED_ID(), TRUE)
		
		bTrafficOnlySwitchToAIOnCollision = TRUE
		
		fChaseSpeed = 1.0
		
		SETTIMERA(0)
		SETTIMERB(0)
		
		
		SAFE_FADE_IN()
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
		iStageSection = 3
		bStageSetup = TRUE
		
//═════════╡ UPDATE ╞═════════		
	ELSE	
		SWITCH iStageSection
		
//			CASE 0
//				REQUEST_VEHICLE_RECORDING(503, "JHUBer")
//				iStageSection++
//			BREAK
//			
//			CASE 1
//			
//				IF HAS_VEHICLE_RECORDING_BEEN_LOADED(503, "JHuber")
//					UNPAUSE_PLAYBACK_RECORDED_VEHICLE(vehBike[0])
//					iStageSection++
//				ENDIF
//			
//			BREAK
//		
//			CASE 2
//				cameraCar = CREATE_VEHICLE(Asset.mnBike, <<0.0, 0.0, 0.0>>)
//				cameraCarCamera = CREATE_VEHICLE(Asset.mnBike, <<0.0, 0.0, 0.0>>)
//				
//				START_PLAYBACK_RECORDED_VEHICLE(cameraCar, 503, "JHUber")
//				
//				SET_CAM_RECORDING_WIDGET_GROUP(camRecData, widgetDebug)
//				START_CAM_RECORDING_RELATIVE_TO_ENTITY(camRecData, cameraCar, cameraCarCamera, <<0.1, 1.75, 0.0>>, <<0.0, 0.0, 0.0>>, 25.5, "JHUber", 504, TRUE, TRUE)		
//				iStageSection++
//			BREAK


			
			CASE 3
	
				CREATE_ALL_WAITING_UBER_CARS()
	
				fChaseSpeed = 1.0
	
				IF NOT IS_ENTITY_DEAD(vehBike[0])
					//DO_CHASE_TRIGGERS()
				ENDIF
			
				//UPDATE_CHASE_SPEED(fChaseSpeed, TRUE)
				
				SET_PLAYBACK_SPEED(vehBike[0], fChaseSpeed)										// +++ Vehicle speed set +++
					
				UPDATE_UBER_PLAYBACK(vehBike[0], fChaseSpeed)
				
			
			//	UPDATE_CAM_PLAYBACK(cameraRecorded1, cameraCar, fChaseSpeed, "JHUber")
			//	UPDATE_CAM_PLAYBACK(cameraRecorded2, cameraCarCamera, fChaseSpeed, "JHUber")
	
//				IF IS_BUTTON_PRESSED(PAD1, DPADDOWN)
//					SET_CAM_ACTIVE(cameraRecorded1,TRUE)
//					SET_CAM_ACTIVE(cameraRecorded2,FALSE)
//				ELSE
//					
//					SET_CAM_ACTIVE(cameraRecorded1, FALSE)
//					SET_CAM_ACTIVE(cameraRecorded2,TRUE)
//				ENDIF
//				
//				RENDER_SCRIPT_CAMS(TRUE,FALSE)
				
				//CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(vehBike[0])
				//UPDATE_CAM_RECORDING(camRecData)

			BREAK
		ENDSWITCH
	
		
		IF DOES_ENTITY_EXIST(SetPieceCarID[5])
			IF NOT IS_ENTITY_DEAD(SetPieceCarID[5])
				SET_ENTITY_COLLISION(SetPieceCarID[5], FALSE)
			ENDIF
		ENDIF
	
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 411
			CLEANUP_UBER_PLAYBACK_AND_BIKES(TRUE)
			SWITCH_ALL_RANDOM_CARS_ON()
						
			bStageSetup = FALSE
			//RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

BOOL bChaseBikeRecording = FALSE

//PURPOSE:		Performs the section where the crew drive bikes into a moving truck
FUNC BOOL stageRecordSetpiece()

//═════════╡ SETUP ╞═════════
	IF NOT bStageSetup
		DELETE_ALL_MISSION_ENTITIES(TRUE)

		iDebugChaseBikeRec = Asset.recNoGoodBikeChase
		bWorstGunDies = FALSE
			
		RESET_NEED_LIST()
		SET_MODEL_AS_NEEDED(Asset.mnBike)
		SET_MODEL_AS_NEEDED(Asset.mnGunman)
		SET_VEHICLE_RECORDING_AS_NEEDED(Asset.recUber, Asset.recNumber)
		//SET_VEHICLE_RECORDING_AS_NEEDED(Asset.recUber, Asset.recNoGoodBikeChase)
		//SET_VEHICLE_RECORDING_AS_NEEDED(Asset.recUber, Asset.recNoBadBikeChase)
		MANAGE_LOADING_NEED_LIST()
		
		LOAD_AND_MANAGE_UBER_ARRAY()
		SetPieceCarRecording[0] = iDebugChaseBikeRec
		
		INT i_setpiece_size = COUNT_OF(SetPieceCarID)
		INT i_traffic_size = COUNT_OF(TrafficCarID)

		REPEAT i_setpiece_size iCounter
			IF SetPieceCarRecording[iCounter] > 0
				SAFE_REQUEST_VEHICLE_RECORDING(SetPieceCarRecording[iCounter], Asset.recUber, TRUE, TRUE)
			ENDIF
		ENDREPEAT

		REPEAT i_traffic_size iCounter
			IF TrafficCarRecording[iCounter] > 0
				SAFE_REQUEST_VEHICLE_RECORDING(TrafficCarRecording[iCounter], Asset.recUber, TRUE, TRUE)
			ENDIF
		ENDREPEAT

		vehBike[0] = CREATE_VEHICLE(Asset.mnBike, vBikeSpawnCoords[0], fBikeSpawnHeading[0])
		SET_VEHICLE_COLOUR_COMBINATION(vehBike[0], 0)
		vehBike[1] = CREATE_VEHICLE(Asset.mnBike, vBikeSpawnCoords[1], fBikeSpawnHeading[1])
		SET_VEHICLE_COLOUR_COMBINATION(vehBike[1], 1)
		vehBike[2] = CREATE_VEHICLE(Asset.mnBike, vBikeSpawnCoords[2], fBikeSpawnHeading[2])
		SET_VEHICLE_COLOUR_COMBINATION(vehBike[2], 2)
		
		pedCrew[CREW_DRIVER_NEW] = CREATE_PED_INSIDE_VEHICLE(vehBike[0], PEDTYPE_MISSION, Asset.mnGunman)
		pedCrew[CREW_GUNMAN_NEW] = CREATE_PED_INSIDE_VEHICLE(vehBike[1], PEDTYPE_MISSION, Asset.mnGunman)
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehBike[2])

		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		
		SWITCH_ALL_RANDOM_CARS_OFF()
				
		#IF IS_DEBUG_BUILD
		set_uber_parent_widget_group(widgetDebug)
		#ENDIF
		
		INITIALISE_UBER_PLAYBACK(Asset.recUber, TRUE)
		
		LOAD_AND_MANAGE_UBER_ARRAY()
		SetPieceCarRecording[0] = iDebugChaseBikeRec
				
		START_PLAYBACK_RECORDED_VEHICLE(vehBike[0], Asset.recNumber, Asset.recUber)
		
		SetPieceCarID[0] = vehBike[1]
		SetPieceCarProgress[0] = 3
		CREATE_ALL_WAITING_UBER_CARS()
		
		//START_RECORDING_VEHICLE(vehBike[1], 000, Asset.recUber, TRUE)
		bChaseBikeRecording = FALSE
					
		SET_VEHICLE_MODEL_IS_SUPPRESSED(Asset.mnBike, TRUE)
				
		SET_CINEMATIC_BUTTON_ACTIVE(FALSE)
		
		SET_BIKE_STABILISERS(PLAYER_PED_ID(), TRUE)
				
		SETTIMERA(0)
		SETTIMERB(0)
		
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		SAFE_FADE_IN()
		#IF IS_DEBUG_BUILD
			JSkip = FALSE
			PSkip = FALSE
		#ENDIF
		iStageSection = 0
		bStageSetup = TRUE
		
//═════════╡ UPDATE ╞═════════		
	ELSE	
		SWITCH iStageSection
			CASE 0	
				SET_PLAYBACK_SPEED(vehBike[0], 1.0)
				UPDATE_UBER_PLAYBACK(vehBike[0], 1.0)
			
				IF NOT bChaseBikeRecording
					IF IS_BUTTON_JUST_PRESSED(PAD1, RIGHTSHOULDER2)
						//STOP_PLAYBACK_RECORDED_VEHICLE(vehBike[1])
						bChaseBikeRecording = TRUE
					ENDIF
				ELSE
					IF IS_BUTTON_JUST_PRESSED(PAD1, SQUARE)
						//STOP_RECORDING_ALL_VEHICLES()
					ENDIF
				ENDIF
			
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBike[0])
					/*IF IS_BUTTON_PRESSED(PAD1, CIRCLE)
						IF NOT DOES_CAM_EXIST(camAnim)
							IF IS_VEHICLE_DRIVEABLE(vehBike[2])
								camAnim = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", FALSE)
								ATTACH_CAM_TO_ENTITY(camAnim, vehBike[2], <<0,0,30>>)
								RENDER_SCRIPT_CAMS(TRUE, FALSE)
								SET_CAM_ACTIVE(camAnim, TRUE)
							ENDIF
						ENDIF
						SET_CAM_ROT(camAnim, <<-90, 0, GET_ENTITY_HEADING(vehBike[2])>>)
					ELSE
						DESTROY_CAM(camAnim)
						RENDER_SCRIPT_CAMS(FALSE, FALSE)
					ENDIF*/
					
					SET_PLAYBACK_SPEED(vehBike[0], 1.0)
					UPDATE_UBER_PLAYBACK(vehBike[0], 1.0)
					
					CONTROL_VEHICLE_CHASE_HINT_CAM_ANY_MEANS(vehBike[0])
					
					DRAW_DEBUG_LINE(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBike[0], <<-10, 0, 0.1>>), 
									GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBike[0], <<10, 0, 0.1>>),
									0, 255, 0, 255)
					
					//fMasterPlaybackSpeed = 1.0
					
					//SET_PLAYBACK_SPEED(vehBike[1], 0.95)
					
					IF IS_BUTTON_PRESSED(PAD1, RIGHTSHOULDER1)
					AND IS_BUTTON_PRESSED(PAD1, LEFTSHOULDER1)
					AND IS_BUTTON_PRESSED(PAD1, TRIANGLE)
						STOP_RECORDING_ALL_VEHICLES()
						iStageSection = 411
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	
//═════════╡ CLEANUP ╞═════════
		IF iStageSection >= 411
			CLEANUP_UBER_PLAYBACK_AND_BIKES(TRUE)
			SWITCH_ALL_RANDOM_CARS_ON()
						
			bStageSetup = FALSE
			//RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

#ENDIF


#IF IS_DEBUG_BUILD

PROC debugProcedures()

	Debug_Options()
	
//	#IF IS_DEBUG_BUILD
//		g_b_CellDialDebugTextToggle = (bIsSuperDebugEnabled)
//	#ENDIF
	
	iRandomManagerStage = ENUM_TO_INT(eRandomManagerStage)
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
	
		FLOAT fX, fY, fZ, fW
		VECTOR vCoords
		
		ENTITY_INDEX thisCar = GET_FOCUS_ENTITY_INDEX()
		IF DOES_ENTITY_EXIST(thisCar)
			GET_ENTITY_QUATERNION(thisCar, fX, fY, fZ, fW)
			vCoords = GET_ENTITY_COORDS(thiscar)
		
			SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_QUATERNION(thisCar,") SAVE_FLOAT_TO_DEBUG_FILE(fX)
			SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fY)
			SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fZ)
			SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fW)
			SAVE_STRING_TO_DEBUG_FILE(")")
			
			
			SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_COORDS(thisCar,") SAVE_VECTOR_TO_DEBUG_FILE(vCoords) 
			SAVE_STRING_TO_DEBUG_FILE(")")
		ENDIF
		
	ENDIF
	
	IF bIsStealthPath
		sMissionName = "Jewellery Heist Stealth"
	ELSE
		sMissionName = "Jewellery Heist Aggressive"
	ENDIF
	
	iDebugMissionStage = ENUM_TO_INT(eMissionStage)
	
	DONT_DO_J_SKIP(sLocatesData)	//Stop the locates header J skipping...
	
	IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, ENUM_TO_INT(eMissionStage), FALSE, sMissionName)
	//(SkipMenuStruct, iReturnStage, ENUM_TO_INT(eMissionStage), FALSE, sMissionName)
		MISSION_STAGE selectedStage = INT_TO_ENUM(MISSION_STAGE, iReturnStage)
		
		IF selectedStage = STAGE_ROOF
		OR selectedStage = STAGE_CUTSCENE_GAS_STORE
			//Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_JEWEL, HEIST_CHOICE_JEWEL_STEALTH)
			bIsStealthPath = TRUE
		ELSE
			IF selectedStage = STAGE_CUTSCENE_AGGRESSIVE_ARRIVE
				//Set_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_JEWEL, HEIST_CHOICE_JEWEL_HIGH_IMPACT)
				bIsStealthPath = FALSE
			ENDIF
		ENDIF
		
		IF selectedStage > STAGE_GRAB_LOOT
			IF g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE] <= 0
				g_replay.iReplayInt[CASE_COUNT_REPLAY_VALUE] = 0
				FOR iCounter = 0 TO (iNoOfCases - 1)
					PRINTLN("Ross says:", iCounter)					
					//ADD_HEIST_END_SCREEN_STOLEN_ITEM(HEIST_JEWEL, jcStore[iCounter].sLootName, jcStore[iCounter].iWorth)
					//iTotalSmashedCases++
				ENDFOR
			ENDIF
		ENDIF
		SAFE_FADE_OUT(500, TRUE)
		DELETE_ALL_MISSION_ENTITIES(TRUE)
		eMissionStage = selectedStage
		bStageSetup = FALSE
		MANAGE_SKIP()
	ENDIF
	
	IF JSkip
		SWITCH eMissionStage
			CASE STAGE_CUTSCENE_BEGIN
				eMissionStage = STAGE_DRIVE_TO_STORE
			BREAK
			CASE STAGE_DRIVE_TO_STORE
				IF bIsStealthPath
					eMissionStage = STAGE_ROOF
				ELSE
					eMissionStage = STAGE_CUTSCENE_AGGRESSIVE_ARRIVE
				ENDIF
			BREAK
			CASE STAGE_ROOF
				eMissionStage = STAGE_CUTSCENE_GAS_STORE
			BREAK
			CASE STAGE_CUTSCENE_GAS_STORE
				eMissionStage = STAGE_GRAB_LOOT
			BREAK
			CASE STAGE_CUTSCENE_AGGRESSIVE_ARRIVE
				eMissionStage = STAGE_GRAB_LOOT
			BREAK
			CASE STAGE_GRAB_LOOT
				eMissionStage = STAGE_CUTSCENE_LEAVE_STORE
			BREAK
			CASE STAGE_CUTSCENE_LEAVE_STORE
				eMissionStage = STAGE_BIKE_CHASE
			BREAK
			CASE STAGE_BIKE_CHASE
				eMissionStage = STAGE_BIKE_CHASE_START_OF_TUNNEL
			BREAK
			CASE STAGE_BIKE_CHASE_START_OF_TUNNEL
				eMissionStage = STAGE_BIKE_CHASE_END_OF_TUNNEL
			BREAK
			CASE STAGE_BIKE_CHASE_END_OF_TUNNEL
				eMissionStage = STAGE_TRUCK_FIGHT 
			BREAK
			CASE STAGE_TRUCK_FIGHT
				eMissionStage = STAGE_CUTSCENE_RAMP_DOWN
			BREAK
			CASE STAGE_CUTSCENE_RAMP_DOWN
				eMissionStage = STAGE_RETURN_TO_BASE
			BREAK
			CASE STAGE_RETURN_TO_BASE
				eMissionStage = PASSED_MISSION
			BREAK
		ENDSWITCH
		SAFE_FADE_OUT(500, TRUE)
		DELETE_ALL_MISSION_ENTITIES(TRUE)
		bStageSetup = FALSE
		MANAGE_SKIP()
	ENDIF
	
	IF PSkip
		SWITCH eMissionStage
			CASE STAGE_CUTSCENE_BEGIN
				eMissionStage = STAGE_CUTSCENE_BEGIN
			BREAK
			CASE STAGE_DRIVE_TO_STORE
				eMissionStage = STAGE_CUTSCENE_BEGIN
			BREAK
			CASE STAGE_ROOF
				eMissionStage = STAGE_DRIVE_TO_STORE
			BREAK
			CASE STAGE_CUTSCENE_GAS_STORE
				eMissionStage = STAGE_ROOF
			BREAK
			CASE STAGE_CUTSCENE_AGGRESSIVE_ARRIVE
				eMissionStage = STAGE_DRIVE_TO_STORE
			BREAK
			CASE STAGE_GRAB_LOOT
				IF bIsStealthPath
					eMissionStage = STAGE_CUTSCENE_GAS_STORE
				ELSE
					eMissionStage = STAGE_CUTSCENE_AGGRESSIVE_ARRIVE
				ENDIF
			BREAK
			CASE STAGE_CUTSCENE_LEAVE_STORE
				eMissionStage = STAGE_GRAB_LOOT
			BREAK

			CASE STAGE_BIKE_CHASE
				//IF bIsStealthPath
					eMissionStage = STAGE_CUTSCENE_LEAVE_STORE
				//ELSE
				//	eMissionStage = STAGE_POLICE_SHOOTOUT
				//ENDIF
			BREAK
			CASE STAGE_BIKE_CHASE_START_OF_TUNNEL
				eMissionStage = STAGE_BIKE_CHASE
			BREAK
			CASE STAGE_BIKE_CHASE_END_OF_TUNNEL
				eMissionStage = STAGE_BIKE_CHASE_START_OF_TUNNEL
			BREAK
//			CASE STAGE_CUTSCENE_TRUCK_ARRIVE
//				eMissionStage = STAGE_BIKE_CHASE
//			BREAK
			CASE STAGE_TRUCK_FIGHT
				eMissionStage = STAGE_BIKE_CHASE_END_OF_TUNNEL //STAGE_CUTSCENE_TRUCK_ARRIVE
			BREAK
			CASE STAGE_CUTSCENE_RAMP_DOWN
				eMissionStage = STAGE_BIKE_CHASE_END_OF_TUNNEL //STAGE_CUTSCENE_TRUCK_ARRIVE //STAGE_TRUCK_FIGHT
			BREAK
			CASE STAGE_RETURN_TO_BASE
				eMissionStage = STAGE_CUTSCENE_RAMP_DOWN //STAGE_MOUNT_TRUCK
			BREAK
		ENDSWITCH
		SAFE_FADE_OUT(500, TRUE)
		DESTROY_ALL_CAMS()
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_TIME_SCALE(1.0)
		DELETE_ALL_MISSION_ENTITIES(TRUE)
		bStageSetup = FALSE
		MANAGE_SKIP()
	ENDIF
	
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
	
		FLOAT fX, fY, fZ, fW
		VECTOR vCoords
		
		ENTITY_INDEX thisCar = GET_FOCUS_ENTITY_INDEX()
		IF DOES_ENTITY_EXIST(thisCar)
			GET_ENTITY_QUATERNION(thisCar, fX, fY, fZ, fW)
			vCoords = GET_ENTITY_COORDS(thiscar)
		
			SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_QUATERNION(thisCar,") SAVE_FLOAT_TO_DEBUG_FILE(fX)
			SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fY)
			SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fZ)
			SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fW)
			SAVE_STRING_TO_DEBUG_FILE(")")
			
			
			SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_COORDS(thisCar,") SAVE_VECTOR_TO_DEBUG_FILE(vCoords) 
			SAVE_STRING_TO_DEBUG_FILE(")")
		ENDIF
		
	ENDIF
	
ENDPROC
	
#ENDIF

FUNC MISSION_STAGE DO_SHIT_SKIP(MISSION_STAGE &ePassedInStage)

	IF g_bShitskipAccepted = TRUE
		SWITCH ePassedInStage
			CASE STAGE_CUTSCENE_BEGIN
				RETURN STAGE_DRIVE_TO_STORE
			BREAK
			CASE STAGE_DRIVE_TO_STORE
				IF bIsStealthPath
					RETURN STAGE_ROOF
				ELSE
					RETURN STAGE_CUTSCENE_AGGRESSIVE_ARRIVE
				ENDIF
			BREAK
			CASE STAGE_ROOF
				RETURN STAGE_CUTSCENE_GAS_STORE
			BREAK
			CASE STAGE_CUTSCENE_GAS_STORE
				RETURN STAGE_GRAB_LOOT
			BREAK
			CASE STAGE_CUTSCENE_AGGRESSIVE_ARRIVE
				RETURN STAGE_GRAB_LOOT
			BREAK
			CASE STAGE_GRAB_LOOT
				RETURN STAGE_CUTSCENE_LEAVE_STORE
			BREAK
			CASE STAGE_CUTSCENE_LEAVE_STORE
				RETURN STAGE_BIKE_CHASE
			BREAK
			CASE STAGE_BIKE_CHASE
				RETURN STAGE_BIKE_CHASE_START_OF_TUNNEL
			BREAK			
			CASE STAGE_BIKE_CHASE_START_OF_TUNNEL
				RETURN STAGE_BIKE_CHASE_END_OF_TUNNEL
			BREAK			
			CASE STAGE_BIKE_CHASE_END_OF_TUNNEL
				RETURN STAGE_CUTSCENE_RAMP_DOWN
			BREAK
			CASE STAGE_TRUCK_FIGHT
				RETURN STAGE_CUTSCENE_RAMP_DOWN
			BREAK
			CASE STAGE_CUTSCENE_RAMP_DOWN
			CASE STAGE_RETURN_TO_BASE
			//	RETURN STAGE_RETURN_TO_BASE
			//BREAK
			
				RETURN PASSED_MISSION
			BREAK
		ENDSWITCH
	
	ENDIF
	
	RETURN ePassedInStage

ENDFUNC


PROC PICKUP_BUDDYS_BAG()

	//IF DOES_BLIP_EXIST(blipBuddysBag)
	IF DOES_ENTITY_EXIST(oiBuddysBag)
		//Collect the bag
		IF IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), oiBuddysBag, <<1.0, 1.0, 2.2>>)
			DELETE_OBJECT(oiBuddysBag)
			REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipBuddysBag)
			SET_CONTROL_SHAKE(PLAYER_CONTROL, 250, 255)
			iCurrentTake += ilostBag
			ilostBag = 0
			
			g_replay.iReplayInt[CURRENT_TAKE_REPLAY_VALUE] = iCurrentTake
						
			g_replay.iReplayInt[GOT_BAG_REPLAY_VALUE] = BAG_COLLECTED
			
		ELSE
			//Remove it if you go too far
			IF NOT IS_ENTITY_AT_ENTITY(PLAYER_PED_ID(), oiBuddysBag, <<65.0, 65.0, 120.0>>)
				DELETE_OBJECT(oiBuddysBag)
				REMOVE_BLIP_AND_CHECK_IF_EXISTS(blipBuddysBag)
			ENDIF
		ENDIF		
	ENDIF
		
ENDPROC


//═════════════════════════════════╡ MAIN SCRIPT ╞═══════════════════════════════════

SCRIPT


	//These entries are placeholder//////////////////////////////////////////////
	/*
	OPEN_HEIST_TIME_WINDOW(HEIST_JEWEL,HEIST_TIME_WINDOW_JC_DRIVE_TO_JOB_TIME)
	OPEN_HEIST_TIME_WINDOW(HEIST_JEWEL,HEIST_TIME_WINDOW_JC_FRANKLIN_TO_ROOF)
	OPEN_HEIST_TIME_WINDOW(HEIST_JEWEL,HEIST_TIME_WINDOW_JC_LOOTING_TIME)
	OPEN_HEIST_TIME_WINDOW(HEIST_JEWEL,HEIST_TIME_WINDOW_JC_MOTORBIKE_CHASE_TIME)
	CLOSE_HEIST_TIME_WINDOW(HEIST_JEWEL,HEIST_TIME_WINDOW_JC_DRIVE_TO_JOB_TIME)
	CLOSE_HEIST_TIME_WINDOW(HEIST_JEWEL,HEIST_TIME_WINDOW_JC_FRANKLIN_TO_ROOF)
	CLOSE_HEIST_TIME_WINDOW(HEIST_JEWEL,HEIST_TIME_WINDOW_JC_LOOTING_TIME)
	CLOSE_HEIST_TIME_WINDOW(HEIST_JEWEL,HEIST_TIME_WINDOW_JC_MOTORBIKE_CHASE_TIME)
	*/
	OPEN_HEIST_TIME_WINDOW(HEIST_JEWEL,HEIST_TIME_WINDOW_JC_MOTORBIKE_CHASE_TIME)
	CLOSE_HEIST_TIME_WINDOW(HEIST_JEWEL,HEIST_TIME_WINDOW_JC_MOTORBIKE_CHASE_TIME)
	
	ADD_USED_GEAR_TO_HEIST(HEIST_JEWEL,HEIST_GEAR_GAS_GRENADE)
	ADD_USED_GEAR_TO_HEIST(HEIST_JEWEL,HEIST_GEAR_REMOTE_RIFLE)
	
	ADD_PENALTY_TO_HEIST(HEIST_JEWEL,HEIST_PENALTY_JCVAN,(1000 - g_iJewelPrep1AVanHealth) * 5)
	////////////////////////////////////////////////////////////////////////////


	SET_MISSION_FLAG(TRUE)
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_MICHAEL, bHasChangedClothesMichael)
		RESET_PLAYER_HAS_CHANGE_CLOTHES_ON_MISSION(CHAR_FRANKLIN, bHasChangedClothesFranklin)
		Mission_Flow_Mission_Force_Cleanup()
		PRINTLN("JEWEL HEIST ABOUT TO CALL FORCE CLEANUP!")
		MissionCleanup()
		TERMINATE_THIS_THREAD()
	ENDIF
	
	// REPLAY!
	// handle pausing game, warping player etc for start of replay
	//START_REPLAY_SETUP(vMichaelSpawnCoords, fMichaelSpawnHeading)
	
	
	//Fix for 1870063 and 2144439. Check crew member selection is valid and if not repair it.
	VALIDATE_AND_REPAIR_CREW_MEMBERS_FOR_HEIST(HEIST_JEWEL)
	
	
	SWITCH(Get_Mission_Flow_Int_Value(FLOWINT_HEIST_CHOICE_JEWEL))
		CASE HEIST_CHOICE_JEWEL_STEALTH
			bIsStealthPath = TRUE
		BREAK
		CASE HEIST_CHOICE_JEWEL_HIGH_IMPACT
			bIsStealthPath = FALSE
		BREAK
	ENDSWITCH
	
				
	//BREAK_ON_NATIVE_COMMAND("REMOVE_CUTSCENE", FALSE)			
#IF IS_DEBUG_BUILD
	IF bSwitchCamDebugScenarioEnabled
		SETUP_SPLINE_CAM_NODE_ARRAY(scsSwitchCam, vehBike[2], vehTruck)
		CREATE_SPLINE_CAM_WIDGETS(scsSwitchCam, "Bike", "Truck", INT_TO_NATIVE(WIDGET_GROUP_ID, 0))
		CREATE_SWITCH_CAM_SCRIPT_SPECIFIC_WIDGETS()
	ENDIF
#ENDIF

INT myStage


// Steve R - Enable interiors for Jewel store and Max Renda store
SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_STORE, BUILDINGSTATE_DESTROYED)
SET_BUILDING_STATE(BUILDINGNAME_IPL_JEWEL_HEIST_MAX_RENDA_INTERIOR, BUILDINGSTATE_DESTROYED)

// ____________________________________ MISSION LOOP _______________________________________

#IF IS_DEBUG_BUILD
	//DELETE_WIDGET_GROUP(widgetDebug)
	CREATE_WIDGETS()
#ENDIF	
				
	WHILE TRUE

	#IF IS_DEBUG_BUILD
		IF bSwitchCamDebugScenarioEnabled
			UPDATE_SPLINE_CAM_HELPERS()
			UPDATE_SPLINE_CAM_WIDGETS(scsSwitchCam)
		ENDIF
	#ENDIF
	
		PICKUP_BUDDYS_BAG()
	
		SWITCH eMissionStage
		
			CASE STAGE_INIT_MISSION
				
				strCrewMemberBest = GET_CREW_MEMBER_FIRST_NAME_LABEL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_DRIVER_NEW)))
				strCrewMemberWorst = GET_CREW_MEMBER_FIRST_NAME_LABEL(GET_HEIST_CREW_MEMBER_AT_INDEX(HEIST_JEWEL,ENUM_TO_INT(CREW_GUNMAN_NEW)))
				
				stageInitMission()
				
				IF Is_Replay_In_Progress()
					eMissionStage = INT_TO_ENUM(MISSION_STAGE, Get_Replay_Mid_Mission_Stage())// + ENUM_TO_INT(STAGE_DRIVE_TO_STORE))					
					
					myStage = Get_Replay_Mid_Mission_Stage()
					
					IF bIsStealthPath									
						SWITCH myStage
							CASE 0
							CASE 1
								eMissionStage = STAGE_DRIVE_TO_STORE
							BREAK
							CASE 2
								eMissionStage = STAGE_ROOF
							BREAK
							CASE 3
								eMissionStage = STAGE_GRAB_LOOT
							BREAK
							CASE 4
								eMissionStage = STAGE_BIKE_CHASE
							BREAK
							CASE 5
								eMissionStage = STAGE_BIKE_CHASE_START_OF_TUNNEL
							BREAK
							CASE 6
								eMissionStage = STAGE_BIKE_CHASE_END_OF_TUNNEL
							BREAK
//							CASE 7
//								eMissionStage = STAGE_TRUCK_FIGHT
//							BREAK
							CASE 7
								eMissionStage = STAGE_CUTSCENE_RAMP_DOWN
							BREAK
							CASE 8
								eMissionStage = STAGE_RETURN_TO_BASE
							BREAK

						ENDSWITCH
					ELSE
						SWITCH myStage
							CASE 0
							CASE 1
								eMissionStage = STAGE_DRIVE_TO_STORE
							BREAK						
							CASE 2
								eMissionStage = STAGE_GRAB_LOOT
							BREAK
							CASE 3
								eMissionStage = STAGE_BIKE_CHASE
							BREAK
							CASE 4
								eMissionStage = STAGE_BIKE_CHASE_START_OF_TUNNEL
							BREAK
							CASE 5
								eMissionStage = STAGE_BIKE_CHASE_END_OF_TUNNEL
							BREAK
//							CASE 6
//								eMissionStage = STAGE_TRUCK_FIGHT
//							BREAK
							CASE 6
								eMissionStage = STAGE_CUTSCENE_RAMP_DOWN
							BREAK
							CASE 7
								eMissionStage = STAGE_RETURN_TO_BASE
							BREAK
							
						ENDSWITCH
					ENDIF
		
					
					eMissionStage = DO_SHIT_SKIP(eMissionStage)	//Will return itself if no shitskip...
					DELETE_ALL_MISSION_ENTITIES(TRUE)
					bStageSetup = FALSE
					
					iCurrentTake = g_replay.iReplayInt[CURRENT_TAKE_REPLAY_VALUE]				
					iDisplayedTake = iCurrentTake 
					CLEAR_AREA(VECTOR_ZERO, 5000, TRUE, TRUE)
					MANAGE_SKIP(TRUE)
				ELSE
					g_bBirdsJHeist = TRUE
					g_bLateLeaveJewelStore = FALSE
					eMissionStage = STAGE_CUTSCENE_BEGIN //STAGE_TIME_LAPSE //STAGE_CUTSCENE_BEGIN //UBER_PLAYBACK //0.8324
				ENDIF
				
			BREAK
			
		ENDSWITCH
		
		// REPLAY!
		//IF NOT IS_REPLAY_IN_PROGRESS()
		//OR g_eReplayWarpStage= RWS_DONE
			// either not on replay, or we've finished replay setup. check for fail
			
			IF HAS_MISSION_FAILED() //bMissionFailed
				IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					MissionFailed()
				ENDIF
			ENDIF
		// REPLAY!
		//ENDIF
			
		updateTextManager()
		
		REPLAY_CHECK_FOR_EVENT_THIS_FRAME("M_TheJewelStoreJob")
		
		//Main loop start
		SWITCH eMissionStage
		
//			CASE STAGE_TIME_LAPSE
//				stageTimeLapse()
//			BREAK
		
			CASE STAGE_CUTSCENE_BEGIN
				IF stageCutsceneBegin()
					eMissionStage = STAGE_DRIVE_TO_STORE
				ENDIF
			BREAK
			
			CASE STAGE_DRIVE_TO_STORE
				IF bIsStealthPath
					IF stageDriveToStoreStealth()
						eMissionStage = STAGE_ROOF
					ENDIF
				ELSE
					IF stageDriveToStoreAggressive()
						eMissionStage = STAGE_CUTSCENE_AGGRESSIVE_ARRIVE
					ENDIF
				ENDIF
			BREAK
			
			CASE STAGE_ROOF
				IF stageRoof()
					eMissionStage = STAGE_CUTSCENE_GAS_STORE
				ENDIF
			BREAK
			
			CASE STAGE_CUTSCENE_GAS_STORE
				IF stageCutsceneGasStore()
					eMissionStage = STAGE_GRAB_LOOT
				ENDIF
			BREAK
			
			CASE STAGE_CUTSCENE_AGGRESSIVE_ARRIVE
				IF stageCutsceneAggressiveArrive()
					eMissionStage = STAGE_GRAB_LOOT
				ENDIF
			BREAK
			
			CASE STAGE_GRAB_LOOT
				IF stageGrabLoot()
					eMissionStage = STAGE_CUTSCENE_LEAVE_STORE						
				ENDIF
			BREAK
			
			CASE STAGE_CUTSCENE_LEAVE_STORE
				IF stageCutsceneLeaveStore()
					eMissionStage = STAGE_BIKE_CHASE
				ENDIF
			BREAK
						
			CASE STAGE_BIKE_CHASE
			CASE STAGE_BIKE_CHASE_START_OF_TUNNEL
			CASE STAGE_BIKE_CHASE_END_OF_TUNNEL
				IF stageBikeChase()
					eMissionStage = STAGE_TRUCK_FIGHT
				ENDIF
			BREAK
			
			CASE STAGE_TRUCK_FIGHT
				IF stageTruckFightNEW()
					eMissionStage = STAGE_CUTSCENE_RAMP_DOWN
				ENDIF
			BREAK
			
			CASE STAGE_CUTSCENE_RAMP_DOWN
				IF stageCutsceneRampDown()
					eMissionStage = STAGE_RETURN_TO_BASE //STAGE_MOUNT_TRUCK //STAGE_MOUNT_TRUCK
				ENDIF
			BREAK
			
			CASE STAGE_RETURN_TO_BASE
				IF stageReturnToBase()
					eMissionStage = PASSED_MISSION //STAGE_CUTSCENE_END_SCENE
				ENDIF
			BREAK
			
			CASE PASSED_MISSION
				MissionPassed()
			BREAK
			
			CASE STAGE_FAIL_CUTSCENE
				IF StageFailCutscene()
					MissionFailed()
				ENDIF
			BREAK
			
			#IF IS_DEBUG_BUILD
						
			CASE UBER_RECORD
				//stageRecord()
				//stageRecordSetpiece()
				//stageMiniCut()
				//stageRerecord()
				//stageTrainPlacer()
			BREAK
			
			CASE UBER_PLAYBACK
				stagePlayback()
			BREAK
		
			#ENDIF
			
		ENDSWITCH
		
//		IF DOES_ENTITY_EXIST(vehBike[2])
//			IF NOT IS_ENTITY_DEAD(vehBike[2])
//				VECTOR tempCo = GET_ENTITY_COORDS(vehBike[2]) 
//				VECTOR tempRo = GET_ENTITY_ROTATION(vehBike[2]) 
//			
//				PRINTLN("Stage:", ENUM_TO_INT(eMissionStage) )
//				PRINTLN("COORDS:", tempCo)
//				PRINTLN("ROT:", tempRo )
//				PRINTNL()
//			ENDIF
//		ENDIF
		
		DISPLAY_TAKE()
		
		#IF IS_DEBUG_BUILD
			debugProcedures()
		#ENDIF
		
		WAIT(0)
	ENDWHILE

ENDSCRIPT

