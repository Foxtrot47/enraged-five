//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	clothes_shop_sp.sc											//
//		AUTHOR			:	Kenneth Ross / Andrew Minghella								//
//		DESCRIPTION		:	An example shop that should be used as a guide for setting	//
//							up a new shop.												//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "clothes_shop_core.sch"

CLOTHES_SHOP_STRUCT sData

SCRIPT(SHOP_LAUNCHER_STRUCT sShopLauncherData)

	CPRINTLN(DEBUG_SHOPS, GET_THIS_SCRIPT_NAME(), ": Shop script launched for ", GET_SHOP_NAME(sShopLauncherData.eShop), ", link=", sShopLauncherData.bLinkedShop)
	
	sData.sShopInfo.bLinkedShop = sShopLauncherData.bLinkedShop
	sData.sShopInfo.iContextID = NEW_CONTEXT_INTENTION
	sData.eInShop = sShopLauncherData.eShop
	
	IF NOT sData.sShopInfo.bLinkedShop
		SET_BIT(g_sShopSettings.iProperties[sShopLauncherData.eShop], SHOP_NS_FLAG_SHOP_SCRIPT_RUNNING)
	ENDIF
	 
	#IF IS_DEBUG_BUILD SETUP_CLOTHES_SHOP_DEBUG_WIDGETS(sData) #ENDIF
	
	// SP setup
	sData.fpIsPedCompItemCurrent = &IS_PED_COMP_ITEM_CURRENT_SP
	sData.fpGetPedCompItemCurrent = &GET_PED_COMP_ITEM_CURRENT_SP
	sData.fpSetPedCompItemCurrent = &SET_PED_COMP_ITEM_CURRENT_SP
	sData.fpIsPedCompItemAvailable = &IS_PED_COMP_ITEM_AVAILABLE_SP
	sData.fpIsPedCompItemAcquired = &IS_PED_COMP_ITEM_ACQUIRED_SP
	sData.fpSetPedCompItemAcquired = &SET_PED_COMP_ITEM_ACQUIRED_SP
	sData.fpGetPedCompDataForItem = &GET_PED_COMP_DATA_FOR_ITEM_SP
	sData.fpSetupClothingItemForShop = &SETUP_CLOTHING_ITEM_FOR_SHOP_SP
	sData.fpForceValidPedCompComboForItem = &FORCE_VALID_PED_COMPONENT_COMBO_FOR_ITEM_SP
	sData.fpDressFreemodePlayerAtStartTorso = &DUMMY_FUNCTION_POINTER_1
	sData.fpGetPedComponentItemRequisite = &GET_PED_COMPONENT_ITEM_REQUISITE_SP
	sData.fpIsAnyVariationOfItemAcquired = &IS_ANY_VARIATION_OF_ITEM_ACQUIRED_SP
	sData.fpCanPedComponentItemMixWithItem = &CAN_PED_COMPONENT_ITEM_MIX_WITH_ITEM_SP
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		CLEANUP_SHOP(sData)
	ENDIF
	
	INITIALISE_COMMERCE_STORE_TRIGGER_DATA(sStoreTriggerData, sData.eInShop)
	
	// Main loop
	WHILE TRUE
	
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		WAIT(0)
		#IF IS_DEBUG_BUILD MAINTAIN_CLOTHES_SHOP_DEBUG_WIDGETS(sData) #ENDIF		
		CLOTHES_SHOP_MAIN_UPDATE(sData)
		
	ENDWHILE
	
ENDSCRIPT

