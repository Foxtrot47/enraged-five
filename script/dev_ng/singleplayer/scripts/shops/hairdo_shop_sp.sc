//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	hairdo_shop_sp.sc											//
//		AUTHOR			:	Kenneth Ross / Andrew Minghella								//
//		DESCRIPTION		:	An example shop that should be used as a guide for setting	//
//							up a new shop.												//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "hairdo_shop_core.sch"
 
HAIRDO_SHOP_STRUCT sData

SCRIPT(SHOP_LAUNCHER_STRUCT sShopLauncherData)

	CPRINTLN(DEBUG_SHOPS, GET_THIS_SCRIPT_NAME(), ": Shop script launched for ", GET_SHOP_NAME(sShopLauncherData.eShop), ", link=", sShopLauncherData.bLinkedShop)
	
	sData.sShopInfo.bLinkedShop = sShopLauncherData.bLinkedShop
	sData.sShopInfo.iContextID = NEW_CONTEXT_INTENTION
	sData.eInShop = sShopLauncherData.eShop
	
	#IF IS_DEBUG_BUILD SETUP_HAIRDO_DEBUG_WIDGETS(sData) #ENDIF
	
	IF NOT sData.sShopInfo.bLinkedShop
		SET_BIT(g_sShopSettings.iProperties[sShopLauncherData.eShop], SHOP_NS_FLAG_SHOP_SCRIPT_RUNNING)
	ENDIF
	
	// SP setup
	sData.fpIsPedCompItemCurrent = &IS_PED_COMP_ITEM_CURRENT_SP
	sData.fpGetPedCompItemCurrent = &GET_PED_COMP_ITEM_CURRENT_SP
	sData.fpSetPedCompItemCurrent = &SET_PED_COMP_ITEM_CURRENT_SP
	sData.fpIsPedCompItemAvailable = &IS_PED_COMP_ITEM_AVAILABLE_SP
	sData.fpIsPedCompItemAcquired = &IS_PED_COMP_ITEM_ACQUIRED_SP
	sData.fpSetPedCompItemAcquired = &SET_PED_COMP_ITEM_ACQUIRED_SP
	sData.fpIsPedCompItemNew = &IS_PED_COMPONENT_ITEM_NEW_SP
	sData.fpGetPedCompDataForItem = &GET_PED_COMP_DATA_FOR_ITEM_SP
	sData.fpSetupClothingItemForShop = &SETUP_CLOTHING_ITEM_FOR_SHOP_SP
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
		CLEANUP_SHOP(sData)
	ENDIF
	
	// Fix for bug 2078449 - Add scenario blocks to barber shops
	IF sData.eInShop != HAIRDO_SHOP_01_BH
		VECTOR vAnimPos, vAnimRot
		IF GET_HAIRDO_SHOP_ANIM_POSITION(sData.eInShop, vAnimPos, vAnimRot)
			ADD_SCENARIO_BLOCKING_AREA_FROM_POSITION_AND_RADIUS(vAnimPos, 3.5)
		ENDIF
	ENDIF
	
	INITIALISE_COMMERCE_STORE_TRIGGER_DATA(sStoreTriggerData, sData.eInShop)
	
	// Main loop
	WHILE TRUE
	
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		WAIT(0)
		
		#IF IS_DEBUG_BUILD MAINTAIN_HAIRDO_DEBUG_WIDGETS(sData) #ENDIF
		
		HAIRDO_SHOP_MAIN_UPDATE(sData)
		
	ENDWHILE
ENDSCRIPT
