//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	tattoo_shop.sc												//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	A tattoo shop that gives the player the chance to add or  	//
//							remove tattoos from their body.								//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////
// Includes
USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "tattoo_shop_private.sch"
USING "menu_public.sch"
USING "savegame_public.sch"
USING "cheat_handler.sch"
USING "achievement_public.sch"
USING "clothes_shop_private.sch"
USING "store_trigger.sch"

// ****************
// BROADCAST DATA.
// ****************

// The server broadcast data.
// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData

	NETWORK_INDEX shopkeeperID
	
	BOOL bShopKeeperSetupDone
	
ENDSTRUCT
ServerBroadcastData serverBD


// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it. 
STRUCT PlayerBroadcastData
	INT iShopProperties
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]



// Structs
STRUCT TATTOO_CUTSCENE_ASSETS
	BOOL 				 bModelRequested
	MODEL_NAMES 		 eModel
	PED_INDEX 			 pedID
	PED_VARIATION_STRUCT sVariations
ENDSTRUCT

// Variables
SHOP_NAME_ENUM 		   eInShop
SHOP_NAME_ENUM		   eBaseShop

TATTOO_SHOP_STRUCT     sData
TATTOO_CUTSCENE_ASSETS sCutsceneData

REL_GROUP_HASH 		   tattooGroup
BOOL 				   bRelGroupSetup

BOOL           		   bInitialEntryComplete
BUDDY_HIDE_STRUCT 	   sBuddyHideData
BOOL 				   bUpdateHelpKeys
BOOL 				   bDoFinaliseHeadBlend

VECTOR 				   vStoredPlayerCoords
VECTOR 				   vStoredBrowseCamLookAt
VECTOR 				   vCurrentBrowseCamLookAt

FLOAT 					fCamZoomAlpha
FLOAT                   fCamRotAlpha
FLOAT                   fStoredFrontZ
FLOAT                   fStoredBackZ
FLOAT                   fInitFrontZ

BOOL					bBackTat   = FALSE

TATTOO_FACING			eBackTat = TATTOO_FRONT
//TATTOO_FACING			eStored_eBackTat = eBackTat
TATTOO_FACING			eStored_eFace = INT_TO_ENUM(TATTOO_FACING, -1)

BOOL					bResetCamRotAlpha = FALSE

STRING                 sAnimDictPlayer
STRING                 sAnimDictTattoo
STRING                 sAnimDictIdles
STRING                 sAnimDictArm = "MISSTATTOO_PARLOUR@SHOP_IG_4BPLAYER"
BOOL                   bIntroAnimDictSetup = FALSE

INT 				   iSyncSceneKeeper
INT					   iSyncScenePlayer

BOOL 				   bDlgChk_Greet_Triggered
INT 				   iDlgChk_Ask
TIME_DATATYPE 		   tdDlgChk_Ask
BOOL 			       bDlgChk_Ask_Triggered
BOOL                   bDlgChk_Ask_TimerSet

INT                    iDlgChk_Exit
TIME_DATATYPE          tdDlgChk_Exit
BOOL                   bDlgChk_Exit_TimerSet
BOOL                   bDlgChk_Exit_Trigger

INT                    iDlgChk_Reset
TIME_DATATYPE          tdDlgChk_Reset
BOOL                   bDlgChk_Reset_TimerSet

BOOL bRebuildMenuNextTime
BOOL bTransitionRestoreMenu

BOOL bRebuildMenuNextFrame
BOOL bUpdateTattoosThisFrame

VECTOR vChairRot
VECTOR vChairPos

INT iHeadBlendTimer

BOOL bDLCLabel

TEXT_LABEL_63 sInvalidCatalogueKey

BOOL bDidPlayerHaveCoupon = FALSE

//Players hair data
//INT iStoredHair
//INT iForcedHairItem
//INT iForcedHairType

CONTROL_ACTION	caZoomInput	= INPUT_FRONTEND_LT // Input used for zooming - different on PC.
BOOL			bZoomToggle	= FALSE


COMMERCE_STORE_TRIGGER_DATA sStoreTriggerData


#IF IS_DEBUG_BUILD

	BOOL bKillScript
	BOOL bOutputCamOffsets
	BOOL bResetTutorial
	BOOL bRemoveAllTattoos, bRebuildMenuForDebug, bMenuOnScreen
	BOOL bDrawDebugTattooText
	BOOL bOutputDLCDebug
	BOOL bOutputDLCTattooHash
	BOOL bMaleTattooHash
	BOOL bTestNewArmAnims, bTestTattooGroupEnum
	BOOL bOutputStatDetails
	TEXT_WIDGET_ID twTattooHash
	
	//BOOL bInteriorReady
	PROC SETUP_TATTOO_DEBUG_WIDGETS()
		START_WIDGET_GROUP("Shop Debug - Tattoo")
			
			ADD_WIDGET_BOOL("Output stat enum", bOutputStatDetails)
			ADD_WIDGET_BOOL("MENU ON SCREEN", bMenuOnScreen)
			ADD_WIDGET_BOOL("Kill script", bKillScript)
			ADD_WIDGET_BOOL("Remove all tattoos", bRemoveAllTattoos)
			ADD_WIDGET_BOOL("Output cam offsets", bOutputCamOffsets)
			ADD_WIDGET_BOOL("Reset tutorial", bResetTutorial)
			ADD_WIDGET_FLOAT_SLIDER("Tattoo Multiplier", g_sMPTunables.fTattooShopMultiplier, -1000.0, 1000.0, 0.1)
			ADD_WIDGET_BOOL("Draw Debug Tattoo Text", bDrawDebugTattooText)
			
			ADD_WIDGET_BOOL("Output DLC data", bOutputDLCDebug)
			ADD_WIDGET_BOOL("Output enum value for tattoo hash", bOutputDLCTattooHash)
			ADD_WIDGET_BOOL("Male tattoo hash", bMaleTattooHash)
			twTattooHash = ADD_TEXT_WIDGET("dlc tattoo preset")
			ADD_WIDGET_BOOL("Test New Arm Tattoos", bTestNewArmAnims)
			ADD_WIDGET_BOOL("Test Tattoo Group Enum", bTestTattooGroupEnum)
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC MAINTAIN_TATTOO_DEBUG_WIDGETS()
//		INT i
//		IF bOutputStatDetails
//			REPEAT MAX_MP_SAVED_VEHICLES i
//				SAVE_NEWLINE_TO_DEBUG_FILE()
//				SAVE_STRING_TO_DEBUG_FILE("DISPLAY_SLOT_STAT #") SAVE_INT_TO_DEBUG_FILE(i)  SAVE_STRING_TO_DEBUG_FILE(" : key = ") SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(GET_INT_PACKED_STAT_KEY(MPSV_GET_DISPLAY_SLOT_STAT_OFFSET(i)))) SAVE_STRING_TO_DEBUG_FILE(", bitshift = ") SAVE_INT_TO_DEBUG_FILE(GET_INT_PACKED_STAT_BITSHIFT(MPSV_GET_DISPLAY_SLOT_STAT_OFFSET(i)))
//			ENDREPEAT
//			bOutputStatDetails = FALSE
//		ENDIF
		
		IF bOutputStatDetails
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("PACKED_MP_FAKE_DIX_WHITE_TSHIRT : key = ") SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(GET_BOOL_PACKED_STAT_KEY(PACKED_MP_FAKE_DIX_WHITE_TSHIRT))) SAVE_STRING_TO_DEBUG_FILE(", bitshift = ") SAVE_INT_TO_DEBUG_FILE(GET_BOOL_PACKED_STAT_BITSHIFT(PACKED_MP_FAKE_DIX_WHITE_TSHIRT))
			bOutputStatDetails = FALSE
		ENDIF
		
		IF bOutputDLCTattooHash
			TATTOO_NAME_ENUM eRetTat
			IF bMaleTattooHash
				eRetTat = GET_TATTOO_ENUM_FROM_DLC_HASH(GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twTattooHash)), TATTOO_MP_FM)
				PRINTLN("Tattoo enum for hash ", GET_CONTENTS_OF_TEXT_WIDGET(twTattooHash), " (", GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twTattooHash)), ") = ", eRetTat)
			ELSE
				eRetTat = GET_TATTOO_ENUM_FROM_DLC_HASH(GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twTattooHash)), TATTOO_MP_FM_F)
				PRINTLN("Tattoo enum for hash ", GET_CONTENTS_OF_TEXT_WIDGET(twTattooHash), " (", GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(twTattooHash)), ") = ", eRetTat)
			ENDIF
			bOutputDLCTattooHash = FALSE
		ENDIF
		
		bMenuOnScreen =	IS_CUSTOM_MENU_ON_SCREEN()
	
		IF bKillScript
			FORCE_SHOP_CLEANUP(eInShop)
		ENDIF
		
		IF bRemoveAllTattoos
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				INT iBitsetIndex, iBitIndex
				TATTOO_DATA_STRUCT sTattooData
				TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
				
				REPEAT MAX_NUMBER_OF_TATTOO_BITSETS iBitsetIndex
					REPEAT 32 iBitIndex
						IF GET_TATTOO_DATA(sTattooData, GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex), eFaction, PLAYER_PED_ID())
							IF g_bInMultiplayer
								SET_MP_TATTOO_CURRENT(sTattooData.eEnum, FALSE)
							ELSE
								SET_PED_TATTOO_CURRENT(GET_CURRENT_PLAYER_PED_ENUM(), sTattooData.eEnum, FALSE)
							ENDIF
						ENDIF
					ENDREPEAT
				ENDREPEAT
				
				IF g_bInMultiplayer
					SET_MP_PLAYER_TATTOOS_AND_PATCHES()
				ELSE
					RESTORE_PLAYER_PED_TATTOOS(PLAYER_PED_ID())
				ENDIF
				IF sData.sBrowseInfo.bBrowsing
					bRebuildMenuForDebug = TRUE
				ENDIF
			ENDIF
			
			bRemoveAllTattoos = FALSE
		ENDIF
		
		IF bOutputCamOffsets
			IF IS_PLAYER_PLAYING(PLAYER_ID())
				PRINTLN("Cam coord offset: ", GET_STRING_FROM_VECTOR(GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(PLAYER_PED_ID(), GET_FINAL_RENDERED_CAM_COORD())))
				PRINTLN("Cam rot offset: ",   GET_STRING_FROM_VECTOR(GET_FINAL_RENDERED_CAM_ROT()-GET_ENTITY_ROTATION(PLAYER_PED_ID())))
			ENDIF
			bOutputCamOffsets = FALSE
		ENDIF
		
		IF bResetTutorial
			SET_SHOP_HAS_RUN_ENTRY_INTRO(sData.sShopInfo.eShop, FALSE)
			bResetTutorial = FALSE
		ENDIF
		
		IF bTestNewArmAnims
			
			//MISS/TATTOO_PARLOUR@/SHOP_IG_4B/PLAYER/LEFT_ARM_INTRO.anim
			//MISS/TATTOO_PARLOUR@/SHOP_IG_4B/PLAYER/LEFT_ARM_BASE.anim
			//MISS/TATTOO_PARLOUR@/SHOP_IG_4B/PLAYER/LEFT_ARM_IDLE_A.anim
			//MISS/TATTOO_PARLOUR@/SHOP_IG_4B/PLAYER/LEFT_ARM_IDLE_B.anim
			//MISS/TATTOO_PARLOUR@/SHOP_IG_4B/PLAYER/LEFT_ARM_IDLE_C.anim
			//MISS/TATTOO_PARLOUR@/SHOP_IG_4B/PLAYER/LEFT_ARM_OUTRO.anim
			
			//MISS/TATTOO_PARLOUR@/SHOP_IG_4B/PLAYER/RIGHT_ARM_INTRO.anim
			//MISS/TATTOO_PARLOUR@/SHOP_IG_4B/PLAYER/RIGHT_ARM_BASE.anim
			//MISS/TATTOO_PARLOUR@/SHOP_IG_4B/PLAYER/RIGHT_ARM_IDLE_A.anim
			//MISS/TATTOO_PARLOUR@/SHOP_IG_4B/PLAYER/RIGHT_ARM_IDLE_B.anim
			//MISS/TATTOO_PARLOUR@/SHOP_IG_4B/PLAYER/RIGHT_ARM_IDLE_C.anim
			//MISS/TATTOO_PARLOUR@/SHOP_IG_4B/PLAYER/RIGHT_ARM_OUTRO.anim
			
			
			
			REQUEST_ANIM_DICT(sAnimDictArm)
			
			BOOL bLEFT_ARM_INTRO = FALSE, bLEFT_ARM_BASE = FALSE, bLEFT_ARM_OUTRO = FALSE
			START_WIDGET_GROUP("Test New Arm Anims")
				ADD_WIDGET_BOOL("Test New Arm Tattoos", bTestNewArmAnims)
				ADD_WIDGET_BOOL("bLEFT_ARM_INTRO", bLEFT_ARM_INTRO)
				ADD_WIDGET_BOOL("bLEFT_ARM_BASE", bLEFT_ARM_BASE)
				ADD_WIDGET_BOOL("bLEFT_ARM_OUTRO", bLEFT_ARM_OUTRO)
			STOP_WIDGET_GROUP()
			
			WHILE NOT HAS_ANIM_DICT_LOADED(sAnimDictArm)
				WAIT(0)
			ENDWHILE
			
			PED_INDEX animPedIndex = PLAYER_PED_ID()
			IF NOT IS_PED_INJURED(g_pShopClonePed)
				animPedIndex = g_pShopClonePed
			ENDIF
			WHILE bTestNewArmAnims
			AND NOT IS_PED_INJURED(animPedIndex)
				IF bLEFT_ARM_INTRO
					TASK_PLAY_ANIM(animPedIndex, sAnimDictArm, "LEFT_ARM_INTRO")
					bLEFT_ARM_INTRO = FALSE
				ENDIF
				IF bLEFT_ARM_BASE
					TASK_PLAY_ANIM(animPedIndex, sAnimDictArm, "LEFT_ARM_BASE")
					bLEFT_ARM_BASE = FALSE
				ENDIF
				IF bLEFT_ARM_OUTRO
					TASK_PLAY_ANIM(animPedIndex, sAnimDictArm, "LEFT_ARM_OUTRO")
					bLEFT_ARM_OUTRO = FALSE
				ENDIF
				
				WAIT(0)
			ENDWHILE
			
			bTestNewArmAnims = FALSE
		ENDIF
		
		IF bTestTattooGroupEnum
			BOOL bFailedMatch = FALSE
			TATTOO_GROUP_ENUM groupA, groupB
			REPEAT MAX_TATTOO_GROUPS groupA
				REPEAT MAX_TATTOO_GROUPS groupB
					BOOL bGroupA_groupB = DO_TATTOO_GROUPS_OVERLAP(groupA, groupB)
					BOOL bGroupB_groupA = DO_TATTOO_GROUPS_OVERLAP(groupB, groupA)
					
					IF bGroupA_groupB = bGroupB_groupA
						CPRINTLN(DEBUG_SHOPS, "<match> groupA[", GET_STRING_FROM_TATTOO_GROUP(groupA), "] and groupB[", GET_STRING_FROM_TATTOO_GROUP(groupB), "]")
					ELSE
						CPRINTLN(DEBUG_SHOPS, "<match_FAIL> groupA[",
								GET_STRING_FROM_TATTOO_GROUP(groupA),
								"] not groupB[",
								GET_STRING_FROM_TATTOO_GROUP(groupB),
								"]   bGroupA_groupB:",
								GET_STRING_FROM_BOOL(bGroupA_groupB),
								" != bGroupB_groupA:",
								GET_STRING_FROM_BOOL(bGroupB_groupA))
						bFailedMatch = TRUE
					ENDIF
				ENDREPEAT
			ENDREPEAT
			
			IF bFailedMatch
				CASSERTLN(DEBUG_SHOPS, "<match_FAIL>")
			ENDIF
			
			bTestTattooGroupEnum = FALSE
		ENDIF
	ENDPROC
	
#ENDIF

PROC UPDATE_SHOP_KEEPER_PED_ID(SHOP_KEEPER_STRUCT &sShopKeeper)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.shopkeeperID)
			sShopKeeper.pedID = NET_TO_PED(serverBD.shopkeeperID)
			sShopKeeper.bSetup = TRUE
		ELSE
			sShopKeeper.pedID = NULL
		ENDIF
		sShopKeeper.bNetKeeperSetup = serverBD.bShopKeeperSetupDone
	ENDIF
ENDPROC

FUNC BOOL DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(BOOL bRequestUse)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.shopkeeperID)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.shopkeeperID)
				RETURN TRUE
			ELSE
				IF bRequestUse
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.shopkeeperID)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC


/// PURPOSE:
///    Requests all the animation dictionaries used to play intro animations
FUNC BOOL LOAD_TATTOO_SHOP_ANIMS()

	// Work out correct anim dictionaries to load...
	IF eBaseShop = TATTOO_PARLOUR_01_HW
		sAnimDictTattoo  = "misstattoo_parlour@shop_ig_4"		// Seperate dictionaries for shop type 1
		sAnimDictPlayer  = "misstattoo_parlour@shop_ig_4b"
	ELSE 
		sAnimDictTattoo  = "misstattoo_parlour@shop_ig_4"		// Shared dictionary for type 2
		sAnimDictPlayer  = "misstattoo_parlour@shop_ig_5_b"
	ENDIF

	IF sData.sShopInfo.bRunEntryIntro
		// Request dictionaries
		REQUEST_ANIM_DICT(sAnimDictPlayer)
		REQUEST_ANIM_DICT(sAnimDictTattoo)
		
		// Have they loaded?
		IF HAS_ANIM_DICT_LOADED(sAnimDictPlayer)
		AND HAS_ANIM_DICT_LOADED(sAnimDictTattoo)
			bIntroAnimDictSetup = TRUE
		ENDIF
	ENDIF
	
	IF (bIntroAnimDictSetup OR NOT sData.sShopInfo.bRunEntryIntro) 
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Requests idle anims
FUNC BOOL LOAD_TATTOO_SHOP_IDLE_ANIMS()

	sAnimDictIdles  = "random@shop_tattoo"
	
	// Request dictionaries
	REQUEST_ANIM_DICT(sAnimDictIdles)
	
	// Have they loaded?
	IF HAS_ANIM_DICT_LOADED(sAnimDictIdles)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Unloads all the loaded animation dictionaries.
PROC REMOVE_TATTOO_SHOP_INTRO_ANIMS()
	IF (bIntroAnimDictSetup)
		REMOVE_ANIM_DICT(sAnimDictPlayer)
		REMOVE_ANIM_DICT(sAnimDictTattoo)
	ENDIF
ENDPROC

/// PURPOSE:
///    Unloads all the loaded animation dicts.
PROC REMOVE_ALL_TATTOO_SHOP_ANIMS()
	REMOVE_TATTOO_SHOP_INTRO_ANIMS()
	IF NOT IS_STRING_NULL_OR_EMPTY(sAnimDictIdles)	
		REMOVE_ANIM_DICT(sAnimDictIdles)
	ENDIF
ENDPROC

/// PURPOSE: Cleanup model for cutscene entities
PROC CLEANUP_TATTOO_CUTSCENE_ASSETS()
	IF sCutsceneData.bModelRequested
		SET_MODEL_AS_NO_LONGER_NEEDED(sCutsceneData.eModel)
		sCutsceneData.bModelRequested = FALSE
	ENDIF
	
	IF DOES_ENTITY_EXIST(sCutsceneData.pedID)
		DELETE_PED(sCutsceneData.pedID)
	ENDIF
	IF DOES_ENTITY_EXIST(g_pShopClonePed)
		DELETE_PED(g_pShopClonePed)
	ENDIF
ENDPROC

/// PURPOSE: Makes a copy of all the current ped components
PROC STORE_CURRENT_PED_COMPONENTS()
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		GET_PED_VARIATIONS(PLAYER_PED_ID(), sData.sCurrentVariations)
		sData.bCurrentVariationsStored = TRUE
	ENDIF
ENDPROC

/// PURPOSE: Preloads stored player clothes
PROC PRELOAD_STORED_CURRENT_PED_COMPONENTS()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND sData.bCurrentVariationsStored
		INT i
		REPEAT NUM_PED_COMPONENTS i
			// Skip the head
			IF i != ENUM_TO_INT(COMP_TYPE_HEAD)
				SET_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, i), sData.sCurrentVariations.iDrawableVariation[i], sData.sCurrentVariations.iTextureVariation[i])
			ENDIF
		ENDREPEAT
		
		REPEAT NUM_PLAYER_PED_PROPS i
			IF sData.sCurrentVariations.iPropIndex[i] <> -1
				SET_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, i), sData.sCurrentVariations.iPropIndex[i], sData.sCurrentVariations.iPropTexture[i])
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE: Sets player clothes to the ones we previously stored
PROC RESTORE_CURRENT_PED_COMPONENTS()
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND sData.bCurrentVariationsStored
		INT i
		REPEAT NUM_PED_COMPONENTS i
			// Skip the head
			IF i != ENUM_TO_INT(COMP_TYPE_HEAD)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), INT_TO_ENUM(PED_COMPONENT, i), sData.sCurrentVariations.iDrawableVariation[i], sData.sCurrentVariations.iTextureVariation[i], sData.sCurrentVariations.iPaletteVariation[i])
			ENDIF
		ENDREPEAT
		
		REPEAT NUM_PLAYER_PED_PROPS i
			IF sData.sCurrentVariations.iPropIndex[i] <> -1
				SET_PED_PROP_INDEX(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, i), sData.sCurrentVariations.iPropIndex[i], sData.sCurrentVariations.iPropTexture[i], NETWORK_IS_GAME_IN_PROGRESS())
			ELSE
				CLEAR_PED_PROP(PLAYER_PED_ID(), INT_TO_ENUM(PED_PROP_POSITION, i))
			ENDIF
		ENDREPEAT
	ENDIF
ENDPROC

/// PURPOSE: Unhides the blood that was hidden by PREPARE_PLAYER_FOR_TATTOO
PROC UNHIDE_BLOOD()
	
	// If the cutscene ped exists, set the items on them, otherwise set it on the player
	PED_INDEX pedID_ShopPed = PLAYER_PED_ID()
	IF DOES_ENTITY_EXIST(sCutsceneData.pedID)
	AND NOT IS_PED_INJURED(sCutsceneData.pedID)
		pedID_ShopPed = sCutsceneData.pedID
	ENDIF
	
	IF DOES_ENTITY_EXIST(pedID_ShopPed)	
		HIDE_PED_BLOOD_DAMAGE_BY_ZONE(pedID_ShopPed, PDZ_HEAD, FALSE)
		HIDE_PED_BLOOD_DAMAGE_BY_ZONE(pedID_ShopPed, PDZ_TORSO, FALSE)
		HIDE_PED_BLOOD_DAMAGE_BY_ZONE(pedID_ShopPed, PDZ_LEFT_LEG, FALSE)
		HIDE_PED_BLOOD_DAMAGE_BY_ZONE(pedID_ShopPed, PDZ_RIGHT_LEG, FALSE)
		HIDE_PED_BLOOD_DAMAGE_BY_ZONE(pedID_ShopPed, PDZ_LEFT_ARM, FALSE)
		HIDE_PED_BLOOD_DAMAGE_BY_ZONE(pedID_ShopPed, PDZ_RIGHT_ARM, FALSE)		
	ENDIF

ENDPROC

/// PURPOSE: Cleans up any assets that were created
PROC CLEANUP_SHOP(BOOL bResetOnly = FALSE)

	PROCESS_END_SHOPPING_SAVE(TRUE)
	
	IF sData.sBrowseInfo.bBrowsing
		IF SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
			PRINTLN("KR STORE TEST = restore tattoo")
			SET_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
			g_sTransitionSessionData.sEndReserve.iShopMenuRestore[0] = ENUM_TO_INT(sData.eCurrentTattoo)
			g_sTransitionSessionData.sEndReserve.iShopMenuRestore[1] = sData.sBrowseInfo.iCurrentGroup
			g_sTransitionSessionData.sEndReserve.iShopLocateRestore = sData.sBrowseInfo.iCurrentLocate
			g_sTransitionSessionData.sEndReserve.iShopMenuRestore[2] = sData.sBrowseInfo.iCurrentItem
			g_sTransitionSessionData.sEndReserve.bRestoreSubMenu = TRUE
		ENDIF
	ENDIF
	
	IF sData.sShopInfo.iContextID != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(sData.sShopInfo.iContextID)
	ENDIF
	
	PERFORM_DEFAULT_SHOP_CLEANUP(sData.sShopInfo, eInShop)
	
	// Destroy any assets we may have loaded
	DESTROY_SHOP_CAMERAS(sData.sShopInfo.sCameras)
	
	// If we wre browing then make sure player has control etc
	IF sData.sBrowseInfo.bBrowsing
		CPRINTLN(DEBUG_SHOPS, "Shop cleanup: was browsing so release cut-scene assets.")
		UNLOCK_PLAYER_FOR_BROWSE(sData.sBrowseInfo)
		SET_WIDESCREEN_BORDERS(FALSE, 0)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
		
		//Set multihead blinders off
		SET_MULTIHEAD_SAFE(FALSE,TRUE)
		
		CLEANUP_PLAYER_FOR_MP_SHOP_CUTSCENE()
		END_SHOP_CUTSCENE()
		
		IF NETWORK_IS_GAME_IN_PROGRESS()	
			IF g_sMPTunables.b_onsale_tattoo_shop
				SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPShops")
			ENDIF
		ENDIF
		
		CLEANUP_MENU_ASSETS(TRUE, ENUM_TO_INT(sData.sShopInfo.eShop))
		UNLOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
		CLEANUP_TATTOO_CUTSCENE_ASSETS()
		
		g_sShopSettings.bPreviewTattooMode = FALSE
		
		IF NOT g_bInMultiplayer	
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())	
			RESTORE_CURRENT_PED_COMPONENTS()
			RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
			RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
			REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
		ENDIF
			
		SET_PLAYER_IS_CHANGING_CLOTHES(FALSE)
			
		UNHIDE_BLOOD()
		
		sBuddyHideData.buddyHide = FALSE
		UPDATE_BUDDIES_FOR_SHOP_CUTSCENE(sBuddyHideData)
	ELSE
		CPRINTLN(DEBUG_SHOPS, "Shop cleanup: was NOT browsing.")
	ENDIF
	
	/*
	IF interiorShop <> NULL
		IF IS_INTERIOR_READY(interiorShop)
			UNPIN_INTERIOR(interiorShop)
		ENDIF
	ENDIF
	*/
	
	// Cleanup shop keeper
	IF NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
		IF DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE)
			SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sData.sShopInfo.sShopKeeper.pedID,TRUE)
		ENDIF
		IF sData.sShopInfo.bForceCleanup
			IF SHOP_SCRIPT_WAS_LAUNCHED_IN_MP(eInShop)
			OR NETWORK_IS_GAME_IN_PROGRESS()
				// Do not cleanup in MP - other scripts may be using them.
			ELSE
				DELETE_PED(sData.sShopInfo.sShopKeeper.pedID)
			ENDIF
		ELIF bResetOnly
			// remove the ped incase we need to flush memory
			IF SHOP_SCRIPT_WAS_LAUNCHED_IN_MP(eInShop)
			OR NETWORK_IS_GAME_IN_PROGRESS()
				// Do not cleanup in MP - other scripts may be using them.
			ELSE
				DELETE_PED(sData.sShopInfo.sShopKeeper.pedID)
			ENDIF
		ELSE
			IF SHOP_SCRIPT_WAS_LAUNCHED_IN_MP(eInShop)
			OR NETWORK_IS_GAME_IN_PROGRESS()
				// Do not cleanup in MP - other scripts may be using them.
			ELSE
				SET_PED_KEEP_TASK(sData.sShopInfo.sShopKeeper.pedID, TRUE)
				SET_PED_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopKeeper.pedID)
			ENDIF
		ENDIF
	ELSE
		sData.sShopInfo.sShopKeeper.pedID = NULL
	ENDIF
	
	IF sData.sShopInfo.sShopKeeper.bModelRequested
		SET_MODEL_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopKeeper.model)
		sData.sShopInfo.sShopKeeper.bModelRequested = FALSE
	ENDIF
	
	// Cleanup shop customer
	IF NOT IS_PED_INJURED(sData.sShopInfo.sShopCustomer.pedID)
		IF sData.sShopInfo.bForceCleanup
			DELETE_PED(sData.sShopInfo.sShopCustomer.pedID)
		ELIF bResetOnly
			// remove the ped incase we need to flush memory
			DELETE_PED(sData.sShopInfo.sShopCustomer.pedID)
		ELSE
			FREEZE_ENTITY_POSITION(sData.sShopInfo.sShopCustomer.pedID,FALSE)
			SET_PED_KEEP_TASK(sData.sShopInfo.sShopCustomer.pedID, TRUE)
			SET_PED_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopCustomer.pedID)
		ENDIF
	ELSE
		sData.sShopInfo.sShopCustomer.pedID = NULL
	ENDIF
	
	IF sData.sShopInfo.sShopCustomer.bModelRequested
		SET_MODEL_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopCustomer.model)
		sData.sShopInfo.sShopCustomer.bModelRequested = FALSE
	ENDIF
	
	IF bRelGroupSetup
		REMOVE_RELATIONSHIP_GROUP(tattooGroup)
		bRelGroupSetup = FALSE
	ENDIF
	
	REMOVE_ALL_TATTOO_SHOP_ANIMS()
	
	CLEAR_BIT(g_iRepeatPlayBits, ENUM_TO_INT(RPB_PLAYER_BROWSING_TATTOO_SHOP))
	
	IF NOT g_bInMultiplayer
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
		RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
	ENDIF
	
	IF bResetOnly
		PRINT_SHOP_DEBUG_LINE("Resetting script")
		SET_BIT(g_sShopSettings.iProperties[eInShop], SHOP_NS_FLAG_FORCE_RESET_PROCESSING)
	ELSE
		#IF FEATURE_TUNER
		g_bTattooArtistPedCreated = FALSE
		IF eInShop = TATTOO_PARLOUR_07_CCT
			g_iTattooShopInstance = -1
			CDEBUG1LN(DEBUG_SHOPS, "Tattoo_Shop - CLEANUP_SHOP - Setting g_iTattooShopInstance: -1")
			STOP_AUDIO_SCENE("Ls_Car_Meet_Tattoo_Shop_Scene")
		ENDIF
		#ENDIF
		
		PRINT_SHOP_DEBUG_LINE("Terminating script")
		CLEAR_BIT(g_sShopSettings.iProperties[eInShop], SHOP_NS_FLAG_SHOP_SCRIPT_RUNNING)
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC

/// PURPOSE: Cleans up any assets that were created and resets script
PROC RESET_SHOP()
	CLEANUP_SHOP(TRUE)
	RESET_TATTOO_SHOP_DATA(sData)
ENDPROC

/// PURPOSE: Do necessary pre game start initialisation
PROC PROCESS_PRE_GAME(INT iNetInstanceID)

	IF NETWORK_IS_GAME_IN_PROGRESS()
	
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": PROCESS_PRE_GAME started in MP for ", GET_SHOP_NAME(eInShop))
		
		NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, iNetInstanceID)
		
		// This makes sure the net script is active, waits untull it is.
		IF NOT HANDLE_NET_SCRIPT_INITIALISATION(FALSE, -1, TRUE)
			CLEANUP_SHOP()
		ENDIF
		
		NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
		NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
		
		RESERVE_NETWORK_MISSION_PEDS(1)
		
		SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
		
		// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
		// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
		IF NOT Wait_For_First_Network_Broadcast()
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": Failed to receive initial network broadcast for ", GET_SHOP_NAME(eInShop), ". Cleaning up.")
			CLEANUP_SHOP()
		ENDIF
	ELSE
		PRINTLN(GET_THIS_SCRIPT_NAME(), ": PROCESS_PRE_GAME started in SP for ", GET_SHOP_NAME(eInShop))
		IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP | FORCE_CLEANUP_FLAG_REPEAT_PLAY)
			CLEANUP_SHOP()
		ENDIF
	ENDIF
	
	PRINTLN(GET_THIS_SCRIPT_NAME(), ": PROCESS_PRE_GAME complete for ", GET_SHOP_NAME(eInShop))
ENDPROC

/// PURPOSE: Make a call to finalise head blend when ready
PROC PROCESS_FINALISE_HEAD_BLEND()
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF bDoFinaliseHeadBlend
				IF sData.sShopInfo.eStage != SHOP_STAGE_BROWSE_ITEMS
				OR sData.sBrowseInfo.eStage > SHOP_BROWSE_STAGE_BROWSING
					PRINT_SHOP_DEBUG_LINE("We are here 1")
					IF HAS_PED_HEAD_BLEND_FINISHED(PLAYER_PED_ID())
					AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
						PRINTLN("HAS_PED_HEAD_BLEND_FINISHED = TRUE")
						FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
						bDoFinaliseHeadBlend = FALSE
					ELSE
						PRINT_SHOP_DEBUG_LINE("TK - HAS_PED_HEAD_BLEND_FINISHED = FALSE")
					ENDIF
				ENDIF
			ELSE
				IF sData.sShopInfo.eStage = SHOP_STAGE_BROWSE_ITEMS
				AND sData.sBrowseInfo.eStage <= SHOP_BROWSE_STAGE_BROWSING
					bDoFinaliseHeadBlend = TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		bDoFinaliseHeadBlend = FALSE
	ENDIF

ENDPROC

FUNC BOOL OK_TO_DO_NORMAL_DIALOGUE()

	IF IS_PED_INJURED(PLAYER_PED_ID())
		RETURN FALSE
	ELSE
		IF IS_PLAYER_UNDER_ATTACK()
		OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())	
		OR GET_PLAYER_WANTED_LEVEL(PLAYER_ID()) > 0
			RETURN FALSE
		ENDIF
		RETURN TRUE
	ENDIF
ENDFUNC

/// PURPOSE: Plays lines of dialogue when required
PROC PROCESS_TATTOO_SHOP_DIALOGUE()

	IF NOT sData.sShopInfo.bDataSet	
		SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, TATT_DLG_NONE, DLG_PRIORITY_MID)
		EXIT
	ENDIF
	
	IF IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
		EXIT
	ENDIF
	
	IF NOT DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE)
		EXIT
	ENDIF
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///     SETUP CONVERSATION
	// Player enters shop
		
	IF OK_TO_DO_NORMAL_DIALOGUE()	
		
		IF sData.sShopInfo.bPlayerInShop
			IF sData.sShopInfo.eStage = SHOP_STAGE_WAIT_FOR_TRIGGER
				IF NOT bDlgChk_Greet_Triggered
					IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), sData.sShopInfo.sShopKeeper.pedID) < 5.0
						IF IS_PED_FACING_PED(sData.sShopInfo.sShopKeeper.pedID, PLAYER_PED_ID(), 45.0)
							SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, TATT_DLG_GREET, DLG_PRIORITY_HIGH)
							TASK_LOOK_AT_ENTITY(sData.sShopInfo.sShopKeeper.pedID, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
							bDlgChk_Greet_Triggered = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			// Reset the greet dialogue when player leaves shop
			IF GET_DISTANCE_BETWEEN_PLAYER_AND_SHOP(sData.sShopInfo.eShop) > 30.0
				bDlgChk_Greet_Triggered = FALSE
			ENDIF
		ENDIF
		
		// Browsing dialogue
		IF sData.sBrowseInfo.bBrowsing
		AND sData.sBrowseInfo.iControl <> 0
			
			// No longer required to greet the player
			bDlgChk_Greet_Triggered = TRUE
			
			IF sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_BROWSING
				
				// Reset exit dialogue
				bDlgChk_Exit_Trigger = TRUE
				//bDlgChk_Exit_TimerSet = FALSE
				
				IF NOT bDlgChk_Ask_Triggered
					IF NOT bDlgChk_Ask_TimerSet
						IF NETWORK_IS_GAME_IN_PROGRESS()
							tdDlgChk_Ask = GET_TIME_OFFSET(GET_NETWORK_TIME(), GET_RANDOM_INT_IN_RANGE(300, 600))
						ELSE
							iDlgChk_Ask = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(300, 600)
						ENDIF	
						bDlgChk_Ask_TimerSet = TRUE
					ELSE
						IF NETWORK_IS_GAME_IN_PROGRESS()
							IF IS_TIME_MORE_THAN(GET_NETWORK_TIME(), tdDlgChk_Ask)
								SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, TATT_DLG_ASK, DLG_PRIORITY_HIGH)
								bDlgChk_Ask_Triggered = TRUE
							ENDIF
						ELSE
							IF (GET_GAME_TIMER() - iDlgChk_Ask) > 0
								SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, TATT_DLG_ASK, DLG_PRIORITY_HIGH)
								bDlgChk_Ask_Triggered = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			bDlgChk_Ask_Triggered = FALSE
			bDlgChk_Ask_TimerSet = FALSE
		ENDIF
			
		// Finished browsing
		IF NOT sData.sBrowseInfo.bBrowsing
			IF bDlgChk_Exit_Trigger
				IF NOT bDlgChk_Exit_TimerSet
					IF NETWORK_IS_GAME_IN_PROGRESS()
						tdDlgChk_Exit = GET_TIME_OFFSET(GET_NETWORK_TIME(), GET_RANDOM_INT_IN_RANGE(0, 100))
					ELSE
						iDlgChk_Exit = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(0, 100)
					ENDIF
					bDlgChk_Exit_TimerSet = TRUE
				ELSE
					IF NETWORK_IS_GAME_IN_PROGRESS()
						IF IS_TIME_MORE_THAN(GET_NETWORK_TIME(), tdDlgChk_Exit)
							SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, TATT_DLG_EXIT, DLG_PRIORITY_HIGH)
							tdDlgChk_Exit = GET_TIME_OFFSET(GET_NETWORK_TIME(), GET_RANDOM_INT_IN_RANGE(5000, 10000))
							bDlgChk_Exit_Trigger = FALSE
						ENDIF
					ELSE
						IF (GET_GAME_TIMER() - iDlgChk_Exit) > 0
							SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, TATT_DLG_EXIT, DLG_PRIORITY_HIGH)
							iDlgChk_Exit = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(5000, 10000)
							bDlgChk_Exit_Trigger = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	///     TRIGGER CONVERSATION
	// Safe to play?
	IF sData.sDialogueInfo.iDialogueToPlay != -1
		IF NOT IS_SHOP_DIALOGUE_SAFE_TO_PLAY(sData.sDialogueInfo, sData.sShopInfo.eShop)
			IF NETWORK_IS_GAME_IN_PROGRESS()
				// Allow a little time before we clear
				IF NOT bDlgChk_Reset_TimerSet
					tdDlgChk_Reset = GET_TIME_OFFSET(GET_NETWORK_TIME(), 3000)
					bDlgChk_Reset_TimerSet = TRUE
				ELIF IS_TIME_MORE_THAN(GET_NETWORK_TIME(), tdDlgChk_Reset)
					RESET_SHOP_DIALOGUE_STRUCT(sData.sDialogueInfo)
					bDlgChk_Reset_TimerSet = FALSE
				ENDIF
			ELSE
				// Allow a little time before we clear
				IF NOT bDlgChk_Reset_TimerSet
					iDlgChk_Reset = GET_GAME_TIMER()
					bDlgChk_Reset_TimerSet = TRUE
				ELIF (GET_GAME_TIMER() - iDlgChk_Reset) > 3000
					RESET_SHOP_DIALOGUE_STRUCT(sData.sDialogueInfo)
					bDlgChk_Reset_TimerSet = FALSE
				ENDIF
			ENDIF
		ELSE
			bDlgChk_Reset_TimerSet = FALSE
			
			SWITCH INT_TO_ENUM(TATTOO_DIALOGUE_ENUM, sData.sDialogueInfo.iDialogueToPlay)
				CASE TATT_DLG_GREET
					IF SHOULD_PLAYER_LOSE_COPS_FOR_SHOP()
						PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_NO_COPS", "SPEECH_PARAMS_FORCE")
					ELSE	
						IF Is_Ped_Drunk(PLAYER_PED_ID())
						OR (IS_PED_IN_UNDERWEAR_SP(PLAYER_PED_ID()) AND NOT NETWORK_IS_GAME_IN_PROGRESS())
							PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_GREET_UNUSUAL", "SPEECH_PARAMS_FORCE")
						ELSE	
							IF NOT NETWORK_IS_GAME_IN_PROGRESS()
							AND GET_ENTITY_MODEL(PLAYER_PED_ID()) = PLAYER_ZERO
							AND (IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON)
							OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON_WITH_MEDAL))
								PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "KIFFLOM_GREET", "SPEECH_PARAMS_FORCE")
//							ELIF IS_SPECIAL_EDITION_GAME() 
//							OR IS_COLLECTORS_EDITION_GAME()
//								PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_GREET_SPECIAL", "SPEECH_PARAMS_FORCE")
							ELSE
								PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_GREET", "SPEECH_PARAMS_FORCE")
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE TATT_DLG_ASK
					IF GET_RANDOM_INT_IN_RANGE(0,2) = 0		
						PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_BANTER", "SPEECH_PARAMS_FORCE")
					ELSE
						PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_BROWSE_TATTOO_MENU", "SPEECH_PARAMS_FORCE")
					ENDIF
				BREAK
				CASE TATT_DLG_WORK
					IF GET_RANDOM_INT_IN_RANGE(0,2) = 0		
						PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_SELL", "SPEECH_PARAMS_FORCE")
					ELSE	
						PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_TATTOO_APPLIED", "SPEECH_PARAMS_FORCE")
					ENDIF	
				BREAK
				CASE TATT_DLG_EXIT
					PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_GOODBYE", "SPEECH_PARAMS_FORCE")
				BREAK
				DEFAULT
					#IF IS_DEBUG_BUILD
						CASSERTLN(DEBUG_SHOPS, "PROCESS_TATTOO_SHOP_DIALOGUE() - missing dialogue case. Tell Kenneth R.")
						CPRINTLN(DEBUG_SHOPS, "PROCESS_TATTOO_SHOP_DIALOGUE() - missing dialogue case. Tell Kenneth R.")
					#ENDIF
				BREAK
			ENDSWITCH

			SET_SHOP_DIALOGUE_HAS_JUST_BEEN_TRIGGERED(sData.sDialogueInfo)
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_TATTOO_ARTIST()
	
	IF NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
	AND IS_SHOP_PED_OK()
	AND DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE)
		
		IF sData.sShopInfo.bPlayerInShop
			
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				IF NOT IS_PED_HEADTRACKING_PED(sData.sShopInfo.sShopKeeper.pedID, PLAYER_PED_ID())
					TASK_LOOK_AT_ENTITY(sData.sShopInfo.sShopKeeper.pedID, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
				ENDIF
				
				IF NOT IS_PED_RAGDOLL(sData.sShopInfo.sShopKeeper.pedID)
				AND NOT IS_PED_GETTING_UP(sData.sShopInfo.sShopKeeper.pedID)
					IF NOT IS_PED_FACING_PED(sData.sShopInfo.sShopKeeper.pedID, PLAYER_PED_ID(), 120)  //60
						IF GET_SCRIPT_TASK_STATUS(sData.sShopInfo.sShopKeeper.pedID, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
							TASK_TURN_PED_TO_FACE_ENTITY(sData.sShopInfo.sShopKeeper.pedID, PLAYER_PED_ID())
						ENDIF
					ENDIF	
				ENDIF
			ELSE
				PED_INDEX pedID_ShopPed = PLAYER_PED_ID()
				IF DOES_ENTITY_EXIST(sCutsceneData.pedID)
				AND NOT IS_PED_INJURED(sCutsceneData.pedID)
					pedID_ShopPed = sCutsceneData.pedID
				ENDIF
				
				// Headtrack nearest player infront of shop keeper
				IF IS_DLC_TATTOO_SHOP(sData.sShopInfo.eShop)
					
					PLAYER_INDEX nearestPlayerToPed = INT_TO_PLAYERINDEX(GET_NEAREST_PLAYER_TO_ENTITY(sData.sShopInfo.sShopKeeper.pedID))
					IF (nearestPlayerToPed = INVALID_PLAYER_INDEX())
						EXIT
					ENDIF
					
					BOOL bShouldHeadTrackEntity = FALSE
					pedID_ShopPed = GET_PLAYER_PED(nearestPlayerToPed)
					
					IF IS_ENTITY_ALIVE(pedID_ShopPed)
						IF IS_ENTITY_IN_RANGE_ENTITY(sData.sShopInfo.sShopKeeper.pedID, pedID_ShopPed, 5.0)
							VECTOR vPedDirection = GET_ENTITY_FORWARD_VECTOR(sData.sShopInfo.sShopKeeper.pedID)
							VECTOR vDirectionToEntity = NORMALISE_VECTOR(GET_ENTITY_COORDS(pedID_ShopPed) - GET_ENTITY_COORDS(sData.sShopInfo.sShopKeeper.pedID))
							
							IF DOT_PRODUCT(vPedDirection, vDirectionToEntity) > 0.1
								bShouldHeadTrackEntity = TRUE
							ENDIF
						ENDIF
						
						IF bShouldHeadTrackEntity
							IF NOT IS_PED_HEADTRACKING_ENTITY(sData.sShopInfo.sShopKeeper.pedID, pedID_ShopPed)
								TASK_LOOK_AT_ENTITY(sData.sShopInfo.sShopKeeper.pedID, pedID_ShopPed, -1, DEFAULT, SLF_LOOKAT_VERY_HIGH)
							ENDIF
						ELSE
							IF IS_PED_HEADTRACKING_ENTITY(sData.sShopInfo.sShopKeeper.pedID, pedID_ShopPed)
								TASK_CLEAR_LOOK_AT(sData.sShopInfo.sShopKeeper.pedID)
							ENDIF
						ENDIF
					ENDIF
				ELSE
					IF NOT IS_PED_HEADTRACKING_PED(sData.sShopInfo.sShopKeeper.pedID, pedID_ShopPed)
						TASK_LOOK_AT_ENTITY(sData.sShopInfo.sShopKeeper.pedID, pedID_ShopPed, -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
					ENDIF
				ENDIF
				
				IF NOT IS_DLC_TATTOO_SHOP(sData.sShopInfo.eShop)
				AND NOT IS_PED_RAGDOLL(sData.sShopInfo.sShopKeeper.pedID)
				AND NOT IS_PED_GETTING_UP(sData.sShopInfo.sShopKeeper.pedID)
					IF NOT IS_PED_FACING_PED(sData.sShopInfo.sShopKeeper.pedID, pedID_ShopPed, 130)  //120)  //60
						IF GET_SCRIPT_TASK_STATUS(sData.sShopInfo.sShopKeeper.pedID, SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) != PERFORMING_TASK
							TASK_TURN_PED_TO_FACE_ENTITY(sData.sShopInfo.sShopKeeper.pedID, pedID_ShopPed)
						ENDIF
					
					ELSE
						VECTOR V1 = GET_ENTITY_COORDS(sData.sShopInfo.sShopKeeper.pedID)
						VECTOR V2 = GET_ENTITY_COORDS(pedID_ShopPed)
						
						FLOAT fOffHead = GET_HEADING_FROM_VECTOR_2D(V2.x-V1.x, V2.y-V1.y)
						
						CPRINTLN(DEBUG_SHOPS, "UPDATE_TATTOO_ARTIST: fOffHead is ", fOffHead, " degrees...")
						
						IF NOT IS_PED_FACING_PED(sData.sShopInfo.sShopKeeper.pedID, pedID_ShopPed, 180)
							SET_ENTITY_HEADING(sData.sShopInfo.sShopKeeper.pedID, fOffHead)
							CWARNINGLN(DEBUG_SHOPS, "UPDATE_TATTOO_ARTIST: set ped heading to ", fOffHead, " degrees")
						ENDIF
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Preloads player's naked variation data
PROC PRELOAD_NAKED_VARIATION() 
	IF IS_SHOP_PED_OK()
	AND NOT NETWORK_IS_GAME_IN_PROGRESS()
	
		// If the cutscene ped exists, set the items on them, otherwise set it on the player
		PED_INDEX pedID_ShopPed = PLAYER_PED_ID()
		IF DOES_ENTITY_EXIST(sCutsceneData.pedID)
		AND NOT IS_PED_INJURED(sCutsceneData.pedID)
			pedID_ShopPed = sCutsceneData.pedID
		ENDIF

		SWITCH GET_ENTITY_MODEL(pedID_ShopPed)
		
			CASE PLAYER_ZERO //CHAR_MICHAEL
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_TORSO,TORSO_P0_BARE_CHEST)
				IF NOT IS_PED_IN_UNDERWEAR_SP(pedID_ShopPed)
					PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_LEGS,LEGS_P0_BED)
				ENDIF
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_FEET,FEET_P0_1)
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_HAND,HAND_P0_NONE)
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_SPECIAL,SPECIAL_P0_NONE)
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_SPECIAL2,SPECIAL2_P0_NONE)
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_DECL,DECL_P0_NONE)
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_JBIB,JBIB_P0_NONE)
			BREAK
			
			CASE PLAYER_ONE //CHAR_FRANKLIN
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_TORSO,TORSO_P1_BARE_CHEST)
				IF NOT IS_PED_IN_UNDERWEAR_SP(pedID_ShopPed)
					PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_LEGS,LEGS_P1_BOXERS)
				ENDIF
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_FEET,FEET_P1_DUMMY)
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_HAND,HAND_P1_NONE)
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_SPECIAL,SPECIAL_P1_DUMMY)
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_SPECIAL2,SPECIAL2_P1_NONE)
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_DECL,DECL_P1_NONE)
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_JBIB,JBIB_P1_NONE)
			BREAK
			
			CASE PLAYER_TWO //CHAR_TREVOR
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_TORSO,TORSO_P2_NONE)
				IF NOT IS_PED_IN_UNDERWEAR_SP(pedID_ShopPed)
					PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_LEGS,LEGS_P2_UNDERWEAR)
				ENDIF
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_FEET,FEET_P2_DUMMY)
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_HAND,HAND_P2_NONE)
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_SPECIAL,SPECIAL_P2_DUMMY)
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_SPECIAL2,SPECIAL2_P2_NONE)
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_DECL,DECL_P2_NONE)
				PRELOAD_PED_COMP(pedID_ShopPed,COMP_TYPE_JBIB,JBIB_P2_NONE)
			BREAK
		ENDSWITCH
	ENDIF

ENDPROC

/// PURPOSE: Sets the ped component variations so we can see skin for tattoo
PROC PREPARE_PLAYER_FOR_TATTOO(BOOL bGrabCrew)
	
	SetPedCompItemCurrent fpSetPedCompItemCurrent
	
	// choose set ped comp current command
	IF NETWORK_IS_GAME_IN_PROGRESS()
		fpSetPedCompItemCurrent = &SET_PED_COMP_ITEM_CURRENT_MP
	ELSE
		fpSetPedCompItemCurrent = &SET_PED_COMP_ITEM_CURRENT_SP
	ENDIF
	
	IF IS_SHOP_PED_OK()
	
		g_sShopSettings.bPreviewTattooMode = TRUE
	
		// If the cutscene ped exists, set the items on them, otherwise set it on the player
		PED_INDEX pedID_ShopPed = PLAYER_PED_ID()
		IF DOES_ENTITY_EXIST(sCutsceneData.pedID)
		AND NOT IS_PED_INJURED(sCutsceneData.pedID)
			pedID_ShopPed = sCutsceneData.pedID
		ENDIF
		
		SWITCH GET_ENTITY_MODEL(pedID_ShopPed)
		
			CASE PLAYER_ZERO //CHAR_MICHAEL
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_TORSO,TORSO_P0_BARE_CHEST)
				IF NOT IS_PED_IN_UNDERWEAR_SP(pedID_ShopPed)
					SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_LEGS,LEGS_P0_BED)
				ENDIF
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_FEET,FEET_P0_1)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_HAND,HAND_P0_NONE)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_SPECIAL,SPECIAL_P0_NONE)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_SPECIAL2,SPECIAL2_P0_NONE)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_DECL,DECL_P0_NONE)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_JBIB,JBIB_P0_NONE)
				REMOVE_ALL_MASKS(pedID_ShopPed,fpSetPedCompItemCurrent)
				REMOVE_PED_HELMET(pedID_ShopPed,TRUE)
			BREAK
			
			CASE PLAYER_ONE //CHAR_FRANKLIN
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_TORSO,TORSO_P1_BARE_CHEST)
				IF NOT IS_PED_IN_UNDERWEAR_SP(pedID_ShopPed)
					SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_LEGS,LEGS_P1_BOXERS)
				ENDIF
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_FEET,FEET_P1_DUMMY)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_HAND,HAND_P1_NONE)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_SPECIAL,SPECIAL_P1_DUMMY)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_SPECIAL2,SPECIAL2_P1_NONE)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_DECL,DECL_P1_NONE)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_JBIB,JBIB_P1_NONE)
				REMOVE_ALL_MASKS(pedID_ShopPed,fpSetPedCompItemCurrent)
				REMOVE_PED_HELMET(pedID_ShopPed,TRUE)
			BREAK
			
			CASE PLAYER_TWO //CHAR_TREVOR
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_TORSO,TORSO_P2_NONE)
				IF NOT IS_PED_IN_UNDERWEAR_SP(pedID_ShopPed)
					SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_LEGS,LEGS_P2_UNDERWEAR)
				ENDIF
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_FEET,FEET_P2_DUMMY)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_HAND,HAND_P2_NONE)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_SPECIAL,SPECIAL_P2_DUMMY)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_SPECIAL2,SPECIAL2_P2_NONE)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_DECL,DECL_P2_NONE)
				SET_PED_COMP_ITEM_CURRENT_SP(pedID_ShopPed,COMP_TYPE_JBIB,JBIB_P2_NONE)
				REMOVE_ALL_MASKS(pedID_ShopPed,fpSetPedCompItemCurrent)
				REMOVE_PED_HELMET(pedID_ShopPed,TRUE)
			BREAK

			CASE MP_M_FREEMODE_01
				//Save the players forced hair data
//				iStoredHair = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO, g_iPedComponentSlot)
//				iForcedHairItem = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_HAIR_ITEM, g_iPedComponentSlot)
//				iForcedHairType = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_HAIR_TYPE, g_iPedComponentSlot)
				
				IF bGrabCrew
					sData.eCrewLogoTattoo = GET_CURRENT_CREW_LOGO_TATTOO()
				ENDIF
				
				// Temp hack to get emblems showing
				IF sData.eCurrentTattoo = TATTOO_MP_FM_CREW_A
				OR sData.eCurrentTattoo = TATTOO_MP_FM_CREW_B
				OR sData.eCurrentTattoo = TATTOO_MP_FM_CREW_C
				OR sData.eCurrentTattoo = TATTOO_MP_FM_CREW_D
					SET_PED_VARIATIONS(pedID_ShopPed, sData.sCurrentVariations)
				ELSE
					SET_PED_COMPONENT_VARIATION(pedID_ShopPed, PED_COMP_JBIB, 15, 0)
					IF GET_PED_DRAWABLE_VARIATION(pedID_ShopPed, PED_COMP_TORSO) != 15
						SET_PED_COMPONENT_VARIATION(pedID_ShopPed, PED_COMP_TORSO, 15, 0)
					ENDIF
					SET_PED_COMPONENT_VARIATION(pedID_ShopPed, PED_COMP_SPECIAL, 15, 0)
					SET_PED_COMPONENT_VARIATION(pedID_ShopPed, PED_COMP_SPECIAL2, 0, 0)
				ENDIF
				SET_PED_COMPONENT_VARIATION(pedID_ShopPed,PED_COMP_DECL, 0, 0)
				SET_PED_COMPONENT_VARIATION(pedID_ShopPed, PED_COMP_HAND, 0, 0)
				IF GET_PED_DRAWABLE_VARIATION(pedID_ShopPed, PED_COMP_FEET) != 5
					SET_PED_COMPONENT_VARIATION(pedID_ShopPed, PED_COMP_FEET, 5, 0)
				ENDIF
				IF GET_PED_DRAWABLE_VARIATION(pedID_ShopPed, PED_COMP_LEG) != 14 //12	
					SET_PED_COMPONENT_VARIATION(pedID_ShopPed, PED_COMP_LEG, 14, 0)
				ENDIF
				IF GET_PED_DRAWABLE_VARIATION(pedID_ShopPed,  PED_COMP_TEETH) != 0	
					SET_PED_COMPONENT_VARIATION(pedID_ShopPed, PED_COMP_TEETH, 0, 0)
				ENDIF
				IF GET_PED_DRAWABLE_VARIATION(pedID_ShopPed,  PED_COMP_BERD) != 0	
					SET_PED_COMPONENT_VARIATION(pedID_ShopPed, PED_COMP_BERD, 0, 0)
				ENDIF
				
				CLEAR_ALL_PED_PROPS(pedID_ShopPed)
				REMOVE_ALL_MASKS(pedID_ShopPed,fpSetPedCompItemCurrent)
				
				IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_USING_HAIR_SA_STAT)
					CPRINTLN(DEBUG_SHOPS, "PREPARE_PLAYER_FOR_TATTOO set male hair to SA stat ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA))
					SET_PED_COMP_ITEM_CURRENT_MP(pedID_ShopPed, COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA)), FALSE)
				ELSE
					CPRINTLN(DEBUG_SHOPS, "PREPARE_PLAYER_FOR_TATTOO set male hair to stat ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA))
					SET_PED_COMP_ITEM_CURRENT_MP(pedID_ShopPed, COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO)), FALSE)
				ENDIF
			BREAK
			CASE MP_F_FREEMODE_01
				//Save the players forced hair data
//				iStoredHair = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO, g_iPedComponentSlot)
//				iForcedHairItem = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_HAIR_ITEM, g_iPedComponentSlot)
//				iForcedHairType = GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_HAIR_TYPE, g_iPedComponentSlot)
				
				IF bGrabCrew
					sData.eCrewLogoTattoo = GET_CURRENT_CREW_LOGO_TATTOO()
				ENDIF
				
				// Temp hack to get emblems showing
				IF sData.eCurrentTattoo = TATTOO_MP_FM_CREW_A
				OR sData.eCurrentTattoo = TATTOO_MP_FM_CREW_B
				OR sData.eCurrentTattoo = TATTOO_MP_FM_CREW_C
				OR sData.eCurrentTattoo = TATTOO_MP_FM_CREW_D
					SET_PED_VARIATIONS(pedID_ShopPed, sData.sCurrentVariations)
				ELSE
					SET_PED_COMP_ITEM_CURRENT_MP(pedID_ShopPed, COMP_TYPE_JBIB, JBIB_FMF_15_0, FALSE)
					SET_PED_COMP_ITEM_CURRENT_MP(pedID_ShopPed, COMP_TYPE_TORSO, TORSO_FMF_15_0, FALSE)
					SET_PED_COMP_ITEM_CURRENT_MP(pedID_ShopPed, COMP_TYPE_SPECIAL, SPECIAL_FMF_15_0, FALSE)
					SET_PED_COMP_ITEM_CURRENT_MP(pedID_ShopPed, COMP_TYPE_SPECIAL2, SPECIAL2_FMF_0_0, FALSE)
				ENDIF
				SET_PED_COMP_ITEM_CURRENT_MP(pedID_ShopPed, COMP_TYPE_DECL, DECL_FMF_0_0, FALSE)
				SET_PED_COMP_ITEM_CURRENT_MP(pedID_ShopPed, COMP_TYPE_HAND, HAND_FMF_0_0, FALSE)
				SET_PED_COMP_ITEM_CURRENT_MP(pedID_ShopPed, COMP_TYPE_LEGS, LEGS_FMF_15_0, FALSE)
				SET_PED_COMP_ITEM_CURRENT_MP(pedID_ShopPed, COMP_TYPE_FEET, FEET_FMF_5_0, FALSE)
				SET_PED_COMP_ITEM_CURRENT_MP(pedID_ShopPed, COMP_TYPE_TEETH, TEETH_FMF_0_0, FALSE)
				SET_PED_COMP_ITEM_CURRENT_MP(pedID_ShopPed, COMP_TYPE_BERD, BERD_FMF_0_0, FALSE)
				
				CLEAR_ALL_PED_PROPS(pedID_ShopPed)
				REMOVE_ALL_MASKS(pedID_ShopPed,fpSetPedCompItemCurrent)
				
				IF GET_MP_BOOL_CHARACTER_STAT(MP_STAT_USING_HAIR_SA_STAT)
					CPRINTLN(DEBUG_SHOPS, "PREPARE_PLAYER_FOR_TATTOO set female hair to SA stat ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA))
					SET_PED_COMP_ITEM_CURRENT_MP(pedID_ShopPed, COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA)), FALSE)
				ELSE
					CPRINTLN(DEBUG_SHOPS, "PREPARE_PLAYER_FOR_TATTOO set female hair to stat ", GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA))
					SET_PED_COMP_ITEM_CURRENT_MP(pedID_ShopPed, COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO)), FALSE)
				ENDIF
			BREAK
		ENDSWITCH
		
		IF DOES_ENTITY_EXIST(pedID_ShopPed)	
			HIDE_PED_BLOOD_DAMAGE_BY_ZONE(pedID_ShopPed, PDZ_HEAD, TRUE)
			HIDE_PED_BLOOD_DAMAGE_BY_ZONE(pedID_ShopPed, PDZ_TORSO, TRUE)
			HIDE_PED_BLOOD_DAMAGE_BY_ZONE(pedID_ShopPed, PDZ_LEFT_LEG, TRUE)
			HIDE_PED_BLOOD_DAMAGE_BY_ZONE(pedID_ShopPed, PDZ_RIGHT_LEG, TRUE)
			HIDE_PED_BLOOD_DAMAGE_BY_ZONE(pedID_ShopPed, PDZ_LEFT_ARM, TRUE)
			HIDE_PED_BLOOD_DAMAGE_BY_ZONE(pedID_ShopPed, PDZ_RIGHT_ARM, TRUE)	
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
				CLEAR_PED_DAMAGE_DECAL_BY_ZONE(pedID_ShopPed, PDZ_TORSO, "ALL")
				CLEAR_PED_DAMAGE_DECAL_BY_ZONE(pedID_ShopPed, PDZ_LEFT_LEG, "ALL")
				CLEAR_PED_DAMAGE_DECAL_BY_ZONE(pedID_ShopPed, PDZ_RIGHT_LEG, "ALL")
				CLEAR_PED_DAMAGE_DECAL_BY_ZONE(pedID_ShopPed, PDZ_LEFT_ARM, "ALL")
				CLEAR_PED_DAMAGE_DECAL_BY_ZONE(pedID_ShopPed, PDZ_RIGHT_ARM, "ALL")
				REMOVE_TATTOOS_IN_UPGRADE_GROUP_MP(GET_HASH_KEY("torsoDecal"), pedID_ShopPed)
			ENDIF
			
		ENDIF
		
	ENDIF
ENDPROC


FUNC BOOL GET_CURSOR_SELECTED_AVAILABLE_ZONE(INT &iItem )
	
	PED_DECORATION_ZONE eZone
	TORSO_DECORATION_SUBZONE eSubzone
	GET_PED_DECORATION_ZONE_FROM_INT(iItem, eZone, eSubzone)
	IF IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE(eZone, eSubzone)
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_PREVIOUS_AVAILABLE_ZONE(INT &iItem, BOOL bLoop)
	
	PED_DECORATION_ZONE eZone
	TORSO_DECORATION_SUBZONE eSubzone
	INT iTempItem = iItem
	WHILE GET_PED_DECORATION_ZONE_FROM_INT(iTempItem, eZone, eSubzone, TRUE, TRUE)
		iTempItem--
		GET_PED_DECORATION_ZONE_FROM_INT(iTempItem, eZone, eSubzone, TRUE, TRUE)
		IF IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE(eZone, eSubzone)
			iItem = iTempItem
			RETURN TRUE
		ENDIF
	ENDWHILE
	
	IF bLoop
		iTempItem = 8	//(ENUM_TO_INT(PDZ_RIGHT_LEG))
		WHILE GET_PED_DECORATION_ZONE_FROM_INT(iTempItem, eZone, eSubzone, TRUE, TRUE)
			IF IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE(eZone, eSubzone)
				iItem = iTempItem
				RETURN TRUE
			ENDIF
			iTempItem--
		ENDWHILE
	ENDIF
	
	CASSERTLN(DEBUG_SHOPS, "GET_PREVIOUS_AVAILABLE_ZONE - invalid iItem: ", iItem)
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_NEXT_AVAILABLE_ZONE(INT &iItem, BOOL bLoop)
	PED_DECORATION_ZONE eZone
	TORSO_DECORATION_SUBZONE eSubzone
	INT iTempItem = iItem
	WHILE GET_PED_DECORATION_ZONE_FROM_INT(iTempItem, eZone, eSubzone, TRUE, TRUE)
		
		iTempItem++
		GET_PED_DECORATION_ZONE_FROM_INT(iTempItem, eZone, eSubzone, TRUE, TRUE)
		IF IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE(eZone, eSubzone)
			iItem = iTempItem
			RETURN TRUE
		ENDIF
		
	ENDWHILE
	
	IF bLoop
		iTempItem = 0	//(ENUM_TO_INT(PDZ_TORSO))
		WHILE GET_PED_DECORATION_ZONE_FROM_INT(iTempItem, eZone, eSubzone, TRUE, TRUE)
			IF IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE(eZone, eSubzone)
				iItem = iTempItem
				RETURN TRUE
			ENDIF
			iTempItem++
		ENDWHILE
	ENDIF
	
	CASSERTLN(DEBUG_SHOPS, "GET_NEXT_AVAILABLE_ZONE - invalid iItem: ", iItem)
	RETURN FALSE	
ENDFUNC

FUNC BOOL GET_FIRST_AVAILABLE_ITEM(TATTOO_NAME_ENUM &eFirstTattoo, INT &iItem)
	
	iItem = -1
	
	INT iTattoo
	INT iBitsetIndex, iBitIndex
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		REPEAT MAX_DLC_TATTOOS_IN_SHOP iTattoo
			iBitsetIndex = iTattoo/32
			iBitIndex = iTattoo%32
			IF IS_BIT_SET(sData.iAvailableDLCTattoos[iBitsetIndex], iBitIndex)
				iItem++
				eFirstTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iTattoo)
				RETURN TRUE
			ENDIF
		ENDREPEAT
	ENDIF
	
	REPEAT MAX_TATTOOS_IN_SHOP iTattoo
		iBitsetIndex = iTattoo/32
		iBitIndex = iTattoo%32
		IF IS_BIT_SET(sData.iAvailableTattoos[iBitsetIndex], iBitIndex)
			iItem++
			eFirstTattoo = (GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex))
			RETURN TRUE
		ENDIF
	ENDREPEAT
	
	eFirstTattoo = INVALID_TATTOO
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_FIRST_AVAILABLE_SORTED_ITEM(TATTOO_NAME_ENUM &eFirstTattoo, INT &iItem)
	
	iItem = -1
	
	INT iTattoo
	INT iBitsetIndex, iBitIndex
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		//REPEAT MAX_DLC_TATTOOS_IN_SHOP iTattoo
		FOR iTattoo = ENUM_TO_INT(MAX_DLC_TATTOOS_IN_SHOP)-1 TO 0 STEP -1
			iBitsetIndex = iTattoo/32
			iBitIndex = iTattoo%32
			IF IS_BIT_SET(sData.iAvailableDLCTattoos[iBitsetIndex], iBitIndex)
				iItem++
				eFirstTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iTattoo)
				RETURN TRUE
			ENDIF
		ENDFOR		//ENDREPEAT
	ENDIF
	
	//REPEAT MAX_TATTOOS_IN_SHOP iTattoo
	FOR iTattoo = ENUM_TO_INT(MAX_TATTOOS_IN_SHOP)-1 TO 0 STEP -1
		iBitsetIndex = iTattoo/32
		iBitIndex = iTattoo%32
		IF IS_BIT_SET(sData.iAvailableTattoos[iBitsetIndex], iBitIndex)
			iItem++
			eFirstTattoo = (GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex))
			RETURN TRUE
		ENDIF
	ENDFOR		//ENDREPEAT
	
	eFirstTattoo = INVALID_TATTOO
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_LAST_AVAILABLE_ITEM(TATTOO_NAME_ENUM &eLastTattoo, INT &iItem)
	
	iItem = -1
	
	INT iTattoo
	INT iBitsetIndex, iBitIndex
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		REPEAT MAX_DLC_TATTOOS_IN_SHOP iTattoo
			iBitsetIndex = iTattoo/32
			iBitIndex = iTattoo%32
			IF IS_BIT_SET(sData.iAvailableDLCTattoos[iBitsetIndex], iBitIndex)
				iItem++
				eLastTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iTattoo)
			ENDIF
		ENDREPEAT
	ENDIF
	
	REPEAT MAX_TATTOOS_IN_SHOP iTattoo
		iBitsetIndex = iTattoo/32
		iBitIndex = iTattoo%32
		IF IS_BIT_SET(sData.iAvailableTattoos[iBitsetIndex], iBitIndex)
			iItem++
			eLastTattoo = (GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex))
		ENDIF
	ENDREPEAT
	
	IF iItem = -1
		eLastTattoo = INVALID_TATTOO
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL GET_LAST_AVAILABLE_SORTED_ITEM(TATTOO_NAME_ENUM &eLastTattoo, INT &iItem)
	
	iItem = -1
	
	INT iTattoo
	INT iBitsetIndex, iBitIndex
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
//		REPEAT MAX_DLC_TATTOOS_IN_SHOP iTattoo
		FOR iTattoo = ENUM_TO_INT(MAX_DLC_TATTOOS_IN_SHOP)-1 TO 0 STEP -1
			iBitsetIndex = iTattoo/32
			iBitIndex = iTattoo%32
			IF IS_BIT_SET(sData.iAvailableDLCTattoos[iBitsetIndex], iBitIndex)
				iItem++
				eLastTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iTattoo)
			ENDIF
		ENDFOR		//ENDREPEAT
	ENDIF
	
//	REPEAT MAX_TATTOOS_IN_SHOP iTattoo
	FOR iTattoo = ENUM_TO_INT(MAX_TATTOOS_IN_SHOP)-1 TO 0 STEP -1
		iBitsetIndex = iTattoo/32
		iBitIndex = iTattoo%32
		IF IS_BIT_SET(sData.iAvailableTattoos[iBitsetIndex], iBitIndex)
			iItem++
			eLastTattoo = (GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex))
		ENDIF
	ENDFOR		//ENDREPEAT
	
	IF iItem = -1
		eLastTattoo = INVALID_TATTOO
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC


/// PURPOSE:
///    Gets the tattoo associated with the cursor clicked-on menu item.
/// PARAMS:
///    eSelectedTattoo - The selected tattoo
/// RETURNS:
///    
FUNC BOOL GET_CURSOR_SELECTED_AVAILABLE_ITEM(TATTOO_NAME_ENUM &eSelectedTattoo)
	
	INT iTempItem = -1

	INT iTattoo
	INT iBitsetIndex, iBitIndex
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		REPEAT MAX_DLC_TATTOOS_IN_SHOP iTattoo
			iBitsetIndex = iTattoo/32
			iBitIndex = iTattoo%32
			IF IS_BIT_SET(sData.iAvailableDLCTattoos[iBitsetIndex], iBitIndex)
				iTempItem++
				
				IF iTempItem = g_iMenuCursorItem
					eSelectedTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iTattoo)
					RETURN TRUE
				ENDIF

			ENDIF
		ENDREPEAT
	ENDIF
		
	REPEAT MAX_TATTOOS_IN_SHOP iTattoo
		iBitsetIndex = iTattoo/32
		iBitIndex = iTattoo%32
		IF IS_BIT_SET(sData.iAvailableTattoos[iBitsetIndex], iBitIndex)
			iTempItem++
		
			IF iTempItem = g_iMenuCursorItem
				eSelectedTattoo = (GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex))
				RETURN TRUE
			ENDIF
			
		ENDIF
			
	ENDREPEAT
	
	eSelectedTattoo = INVALID_TATTOO
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Gets the tattoo associated with the cursor clicked-on menu item - Sorted version to work with the new sorted tattoos.
/// PARAMS:
///    eSelectedTattoo - The selected tattoo
/// RETURNS:
///    
FUNC BOOL GET_CURSOR_SELECTED_AVAILABLE_SORTED_ITEM(TATTOO_NAME_ENUM &eSelectedTattoo)
	
	INT iTempItem = -1

	INT iTattoo
	INT iBitsetIndex, iBitIndex
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
			FOR iTattoo = ENUM_TO_INT(MAX_DLC_TATTOOS_IN_SHOP)-1 TO 0 STEP -1
			iBitsetIndex = iTattoo/32
			iBitIndex = iTattoo%32
			IF IS_BIT_SET(sData.iAvailableDLCTattoos[iBitsetIndex], iBitIndex)
				iTempItem++
				
				IF iTempItem = g_iMenuCursorItem
					eSelectedTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iTattoo)
					RETURN TRUE
				ENDIF

			ENDIF
		ENDFOR // ENDREPEAT
	ENDIF
		
	//REPEAT MAX_TATTOOS_IN_SHOP iTattoo
	FOR iTattoo = ENUM_TO_INT(MAX_TATTOOS_IN_SHOP)-1 TO 0 STEP -1
		iBitsetIndex = iTattoo/32
		iBitIndex = iTattoo%32
		IF IS_BIT_SET(sData.iAvailableTattoos[iBitsetIndex], iBitIndex)
			iTempItem++
		
			IF iTempItem = g_iMenuCursorItem
				eSelectedTattoo = (GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex))
				RETURN TRUE
			ENDIF
			
		ENDIF
			
	//ENDREPEAT
	ENDFOR
	
	eSelectedTattoo = INVALID_TATTOO
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_PREVIOUS_AVAILABLE_ITEM(TATTOO_NAME_ENUM eCurrentTatoo, TATTOO_NAME_ENUM &ePreviousTattoo, INT &iItem, BOOL bLoop)

	INT iItempItem = -1
	TATTOO_NAME_ENUM eTempTattoo = INVALID_TATTOO
	
	//TATTOO_DATA_STRUCT sTattooData
	//TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
	
	INT iTattoo
	INT iBitsetIndex, iBitIndex
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		TATTOO_NAME_ENUM eDLCTattoo
		REPEAT MAX_DLC_TATTOOS_IN_SHOP iTattoo
			iBitsetIndex = iTattoo/32
			iBitIndex = iTattoo%32
			IF IS_BIT_SET(sData.iAvailableDLCTattoos[iBitsetIndex], iBitIndex)
				
				eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iTattoo)
				//IF GET_TATTOO_DATA(sTattooData, eDLCTattoo, eFaction, PLAYER_PED_ID())
					IF eCurrentTatoo = eDLCTattoo
					AND iItempItem != -1
						iItem = iItempItem
						ePreviousTattoo = eTempTattoo
						RETURN TRUE
					ENDIF
					iItempItem++
					eTempTattoo = eDLCTattoo
				//ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	REPEAT MAX_TATTOOS_IN_SHOP iTattoo
		iBitsetIndex = iTattoo/32
		iBitIndex = iTattoo%32
		IF IS_BIT_SET(sData.iAvailableTattoos[iBitsetIndex], iBitIndex)
			//IF GET_TATTOO_DATA(sTattooData, GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex), eFaction, PLAYER_PED_ID())
				IF eCurrentTatoo = GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex)//sTattooData.eEnum
				AND iItempItem != -1
					iItem = iItempItem
					ePreviousTattoo = eTempTattoo
					RETURN TRUE
				ENDIF
				iItempItem++
				eTempTattoo = GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex)
			//ENDIF
		ENDIF
	ENDREPEAT
	
	IF bLoop
		RETURN GET_LAST_AVAILABLE_ITEM(ePreviousTattoo, iItem)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_PREVIOUS_AVAILABLE_SORTED_ITEM(TATTOO_NAME_ENUM eCurrentTatoo, TATTOO_NAME_ENUM &ePreviousTattoo, INT &iItem, BOOL bLoop)

	INT iItempItem = -1
	TATTOO_NAME_ENUM eTempTattoo = INVALID_TATTOO
	
	//TATTOO_DATA_STRUCT sTattooData
	//TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
	
	INT iTattoo
	INT iBitsetIndex, iBitIndex
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		TATTOO_NAME_ENUM eDLCTattoo
//		REPEAT MAX_DLC_TATTOOS_IN_SHOP iTattoo
		FOR iTattoo = ENUM_TO_INT(MAX_DLC_TATTOOS_IN_SHOP)-1 TO 0 STEP -1
			iBitsetIndex = iTattoo/32
			iBitIndex = iTattoo%32
			IF IS_BIT_SET(sData.iAvailableDLCTattoos[iBitsetIndex], iBitIndex)
				
				eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iTattoo)
				//IF GET_TATTOO_DATA(sTattooData, eDLCTattoo, eFaction, PLAYER_PED_ID())
					IF eCurrentTatoo = eDLCTattoo
					AND iItempItem != -1
						iItem = iItempItem
						ePreviousTattoo = eTempTattoo
						RETURN TRUE
					ENDIF
					iItempItem++
					eTempTattoo = eDLCTattoo
				//ENDIF
			ENDIF
		ENDFOR		//ENDREPEAT
	ENDIF
	
//	REPEAT MAX_TATTOOS_IN_SHOP iTattoo
	FOR iTattoo = ENUM_TO_INT(MAX_TATTOOS_IN_SHOP)-1 TO 0 STEP -1
		iBitsetIndex = iTattoo/32
		iBitIndex = iTattoo%32
		IF IS_BIT_SET(sData.iAvailableTattoos[iBitsetIndex], iBitIndex)
			//IF GET_TATTOO_DATA(sTattooData, GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex), eFaction, PLAYER_PED_ID())
				IF eCurrentTatoo = GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex)//sTattooData.eEnum
				AND iItempItem != -1
					iItem = iItempItem
					ePreviousTattoo = eTempTattoo
					RETURN TRUE
				ENDIF
				iItempItem++
				eTempTattoo = GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex)
			//ENDIF
		ENDIF
	ENDFOR		//ENDREPEAT
	
	IF bLoop
		RETURN GET_LAST_AVAILABLE_SORTED_ITEM(ePreviousTattoo, iItem)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_NEXT_AVAILABLE_ITEM(TATTOO_NAME_ENUM eCurrentTattoo, TATTOO_NAME_ENUM &eNextTattoo, INT &iItem, BOOL bLoop)
	
	INT iItempItem = -1
	TATTOO_NAME_ENUM eTempTattoo = INVALID_TATTOO

	//TATTOO_DATA_STRUCT sTattooData
	//TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
	
	INT iTattoo
	INT iBitsetIndex, iBitIndex
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		TATTOO_NAME_ENUM eDLCTattoo
		REPEAT MAX_DLC_TATTOOS_IN_SHOP iTattoo
			iBitsetIndex = iTattoo/32
			iBitIndex = iTattoo%32
			IF IS_BIT_SET(sData.iAvailableDLCTattoos[iBitsetIndex], iBitIndex)
				eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iTattoo)
				//IF GET_TATTOO_DATA(sTattooData, eDLCTattoo, eFaction, PLAYER_PED_ID())
					IF eCurrentTattoo = eTempTattoo
						iItempItem++
						eTempTattoo = eDLCTattoo
						iItem = iItempItem
						eNextTattoo = eTempTattoo
						RETURN TRUE
					ENDIF
					iItempItem++
					eTempTattoo = eDLCTattoo
				//ENDIF
			ENDIF
		ENDREPEAT
	ENDIF
	
	REPEAT MAX_TATTOOS_IN_SHOP iTattoo
		iBitsetIndex = iTattoo/32
		iBitIndex = iTattoo%32
		IF IS_BIT_SET(sData.iAvailableTattoos[iBitsetIndex], iBitIndex)
			//IF GET_TATTOO_DATA(sTattooData, GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex), eFaction, PLAYER_PED_ID())
				IF eCurrentTattoo = eTempTattoo
					iItempItem++
					eTempTattoo = GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex)//sTattooData.eEnum
					iItem = iItempItem
					eNextTattoo = eTempTattoo
					RETURN TRUE
				ENDIF
				iItempItem++
				eTempTattoo = GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex)
			//ENDIF
		ENDIF
	ENDREPEAT
	
	IF bLoop
		RETURN GET_FIRST_AVAILABLE_ITEM(eNextTattoo, iItem)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL GET_NEXT_AVAILABLE_SORTED_ITEM(TATTOO_NAME_ENUM eCurrentTattoo, TATTOO_NAME_ENUM &eNextTattoo, INT &iItem, BOOL bLoop)
	
	INT iItempItem = -1
	TATTOO_NAME_ENUM eTempTattoo = INVALID_TATTOO
	
	INT iTattoo
	INT iBitsetIndex, iBitIndex
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		TATTOO_NAME_ENUM eDLCTattoo
//		REPEAT MAX_DLC_TATTOOS_IN_SHOP iTattoo
		FOR iTattoo = ENUM_TO_INT(MAX_DLC_TATTOOS_IN_SHOP)-1 TO 0 STEP -1
			iBitsetIndex = iTattoo/32
			iBitIndex = iTattoo%32
			IF IS_BIT_SET(sData.iAvailableDLCTattoos[iBitsetIndex], iBitIndex)
				eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iTattoo)
				IF eCurrentTattoo = eTempTattoo
					iItempItem++
					eTempTattoo = eDLCTattoo
					iItem = iItempItem
					eNextTattoo = eTempTattoo
					RETURN TRUE
				ENDIF
				iItempItem++
				eTempTattoo = eDLCTattoo
			ENDIF
		ENDFOR		//ENDREPEAT
	ENDIF
	
//	REPEAT MAX_TATTOOS_IN_SHOP iTattoo
	FOR iTattoo = ENUM_TO_INT(MAX_TATTOOS_IN_SHOP)-1 TO 0 STEP -1
		iBitsetIndex = iTattoo/32
		iBitIndex = iTattoo%32
		IF IS_BIT_SET(sData.iAvailableTattoos[iBitsetIndex], iBitIndex)
			IF eCurrentTattoo = eTempTattoo
				iItempItem++
				eTempTattoo = GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex)//sTattooData.eEnum
				iItem = iItempItem
				eNextTattoo = eTempTattoo
				RETURN TRUE
			ENDIF
			iItempItem++
			eTempTattoo = GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex)
		ENDIF
	ENDFOR		//ENDREPEAT
	
	IF bLoop
		RETURN GET_FIRST_AVAILABLE_SORTED_ITEM(eNextTattoo, iItem)
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT COUNT_AVAILABLE_ITEMS()
	INT iItem = 0
	
	INT iTattoo
	INT iBitsetIndex, iBitIndex
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		REPEAT MAX_DLC_TATTOOS_IN_SHOP iTattoo
			iBitsetIndex = iTattoo/32
			iBitIndex = iTattoo%32
			IF IS_BIT_SET(sData.iAvailableDLCTattoos[iBitsetIndex], iBitIndex)
				iItem++
			ENDIF
		ENDREPEAT
	ENDIF
	
	REPEAT MAX_TATTOOS_IN_SHOP iTattoo
		iBitsetIndex = iTattoo/32
		iBitIndex = iTattoo%32
		IF IS_BIT_SET(sData.iAvailableTattoos[iBitsetIndex], iBitIndex)
			iItem++
			IF (GET_TATTOO_NAME_FROM_BITSET_INDEX(iBitsetIndex, iBitIndex) = INVALID_TATTOO)
				RETURN iItem
			ENDIF
		ENDIF
	ENDREPEAT

	RETURN iItem
ENDFUNC

/// PURPOSE: Load model for cutscene entities
FUNC BOOL LOAD_TATTOO_CUTSCENE_ASSETS()
	
	IF g_bInMultiplayer
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
			IF NOT DOES_ENTITY_EXIST(sCutsceneData.pedID)
				
				IF iHeadBlendTimer = -1
					iHeadBlendTimer = GET_GAME_TIMER()
				ENDIF
				
				sCutsceneData.bModelRequested = TRUE
				sCutsceneData.eModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
				REQUEST_MODEL(sCutsceneData.eModel)
				IF HAS_MODEL_LOADED(sCutsceneData.eModel)
					IF (HAS_PED_HEAD_BLEND_FINISHED(PLAYER_PED_ID()) OR (GET_GAME_TIMER() - iHeadBlendTimer > 5000))
						sCutsceneData.pedID = CLONE_PED(PLAYER_PED_ID(), FALSE, FALSE, FALSE)
						g_pShopClonePed = sCutsceneData.pedID
						
						SET_ENTITY_COORDS_NO_OFFSET(sCutsceneData.pedID, sData.sLocateInfo.vPlayerBrowsePos)
						SET_ENTITY_HEADING(sCutsceneData.pedID, sData.sLocateInfo.fPlayerBrowseHead)
						SET_ENTITY_COLLISION(sCutsceneData.pedID, FALSE)
						FREEZE_ENTITY_POSITION(sCutsceneData.pedID, TRUE)
						SET_ENTITY_VISIBLE(sCutsceneData.pedID, FALSE)
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sCutsceneData.pedID)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sCutsceneData.pedID, TRUE)
						
						SET_FACIAL_IDLE_ANIM_OVERRIDE(sCutsceneData.pedID, GET_MOOD_FOR_MISSION_TYPE(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].iCurrentMissionType))
						
						MARK_PED_DECORATIONS_AS_CLONED_FROM_LOCAL_PLAYER(sCutsceneData.pedID, TRUE)
						RESET_PED_VISIBLE_DAMAGE(sCutsceneData.pedID)
						
						IF IS_DLC_CLOTHES_SHOP(sData.sShopInfo.eShop)
							SET_PED_LEG_IK_MODE(sCutsceneData.pedID, LEG_IK_OFF)
						ENDIF
						
						// Set the tattoo clothes now so we can wait for the blend to complete
						PREPARE_PLAYER_FOR_TATTOO(TRUE)
						
						SET_MP_CHARACTER_TATTOOS_AND_PATCHES(sCutsceneData.pedID)
					ELSE
						PRINTLN("LOAD_TATTOO_CUTSCENE_ASSETS - Waiting for player head blend to finalise")
					ENDIF
				ELSE
					PRINTLN("LOAD_TATTOO_CUTSCENE_ASSETS - Waiting for cutscene model to load")
				ENDIF
				
				// Wait for the head blend to complete before we return true.
				RETURN FALSE
			ENDIF
			
			IF NOT IS_PED_INJURED(sCutsceneData.pedID)
				IF NOT HAS_PED_HEAD_BLEND_FINISHED(sCutsceneData.pedID)
				OR NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sCutsceneData.pedID)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Initialises DLC shops as open for business
PROC INITIALISE_DLC_TATTOO_SHOPS(SHOP_NAME_ENUM eShop)
	IF NOT IS_DLC_TATTOO_SHOP(eShop)
		EXIT
	ENDIF
	
	SET_SHOP_BIT(eShop, SHOP_NS_FLAG_SHOP_OPEN, TRUE)
	SET_SHOP_BIT(eShop, SHOP_S_FLAG_SHOP_AVAILABLE, TRUE)
	SET_SHOP_BIT(eShop, SHOP_S_FLAG_SHOP_RUN_ENTRY_INTRO, TRUE)
	CLEAR_SHOP_BIT(eShop, SHOP_NS_FLAG_SHOP_BEING_ROBBED)
	g_sShopSettings.bFirstPassComplete = TRUE
ENDPROC

/// PURPOSE: Loads the required shop assets and sets up racks to browse
PROC DO_INITIALISE()

	SWITCH sData.sBrowseInfo.iControl
		
		// Attempt to get the shop data
		CASE 0
			// Setup the shop information based on the coords that were passed into the script
			IF GET_TATTOO_SHOP_DATA(sData.sShopInfo, eInShop)
				sData.sBrowseInfo.iControl++
			ELSE
				// Problem finding a suitable shop so cleanup script
				CLEANUP_SHOP()
			ENDIF
		BREAK
		
		// Make sure the shop is open for business before we initialise
		CASE 1
			// Make sure the shop is open for business
			IF IS_SHOP_OPEN_FOR_BUSINESS(sData.sShopInfo.eShop)
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": ", GET_STRING_FROM_TEXT_FILE(GET_SHOP_NAME(sData.sShopInfo.eShop)), " is open for business")
				sData.sBrowseInfo.iControl++
			ENDIF
		BREAK
		
		// Ready to initialise
		CASE 2
		
//			IF NOT IS_SHOP_INTERIOR_READY(sData.sShopInfo.eShop)
//				EXIT
//			ENDIF
			
			IF NOT HAS_SHOP_RUN_ENTRY_INTRO(sData.sShopInfo.eShop)
			AND NOT g_bInMultiplayer
			AND NOT ARE_VECTORS_EQUAL(sData.sShopInfo.sShopCustomer.vPosIdle, <<0,0,0>>)
				sData.sShopInfo.bRunEntryIntro = TRUE
			ELSE
				sData.sShopInfo.bRunEntryIntro = FALSE
			ENDIF
			
			IF sData.sShopInfo.bRunEntryIntro = TRUE
				// Load all the intro shop keeper anims.
				IF NOT LOAD_TATTOO_SHOP_ANIMS()
				OR NOT DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(GET_SCENE_ORIGIN(eBaseShop, sData.sShopInfo.eShop), 5.0, V_RET_TA_STOOL)		
					EXIT
				ENDIF
				GET_COORDS_AND_ROTATION_OF_CLOSEST_OBJECT_OF_TYPE(GET_SCENE_ORIGIN(eBaseShop, sData.sShopInfo.eShop), 5.0, V_RET_TA_STOOL,vChairPos,vChairRot)
			ENDIF
		
			// Clear up any previous shop keepers
			IF NOT SHOP_SCRIPT_WAS_LAUNCHED_IN_MP(eInShop)
			AND NOT NETWORK_IS_GAME_IN_PROGRESS()
				IF NOT sData.sShopInfo.sShopKeeper.bSetup
					IF DOES_ENTITY_EXIST(sData.sShopInfo.sShopKeeper.pedID)
						IF IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
						OR IS_PED_FLEEING(sData.sShopInfo.sShopKeeper.pedID)
						OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sData.sShopInfo.sShopKeeper.pedID), sData.sShopInfo.sShopKeeper.vPosIdle) > 10.0
							PRINT_SHOP_DEBUG_LINE("Marking previous shop keeper as no longer needed")
							SET_PED_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopKeeper.pedID)
						ENDIF
					ENDIF
					IF DOES_ENTITY_EXIST(sData.sShopInfo.sShopKeeper.pedID)
						DELETE_PED(sData.sShopInfo.sShopKeeper.pedID)
					ENDIF
				ENDIF
			ENDIF
			
			// Clear up any previous shop customers
			IF NOT sData.sShopInfo.sShopCustomer.bSetup
				IF DOES_ENTITY_EXIST(sData.sShopInfo.sShopCustomer.pedID)
					IF IS_PED_INJURED(sData.sShopInfo.sShopCustomer.pedID)
					OR IS_PED_FLEEING(sData.sShopInfo.sShopCustomer.pedID)
					OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(sData.sShopInfo.sShopCustomer.pedID), sData.sShopInfo.sShopCustomer.vPosIdle) > 10.0
						PRINT_SHOP_DEBUG_LINE("Marking previous shop customer as no longer needed")
						SET_PED_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopCustomer.pedID)
					ENDIF
				ENDIF
				IF DOES_ENTITY_EXIST(sData.sShopInfo.sShopCustomer.pedID)
					DELETE_PED(sData.sShopInfo.sShopCustomer.pedID)
				ENDIF
			ENDIF
			
			// Wait for all the ped models to load.
			IF sData.sShopInfo.sShopKeeper.model <> DUMMY_MODEL_FOR_SCRIPT
				REQUEST_MODEL(sData.sShopInfo.sShopKeeper.model)
				sData.sShopInfo.sShopKeeper.bModelRequested = TRUE
			ENDIF
			IF sData.sShopInfo.bRunEntryIntro
				IF sData.sShopInfo.sShopCustomer.model <> DUMMY_MODEL_FOR_SCRIPT
					REQUEST_MODEL(sData.sShopInfo.sShopCustomer.model)
					sData.sShopInfo.sShopCustomer.bModelRequested = TRUE
				ENDIF
			ENDIF
			
			IF (sData.sShopInfo.sShopKeeper.bModelRequested AND NOT HAS_MODEL_LOADED(sData.sShopInfo.sShopKeeper.model))
			OR (sData.sShopInfo.sShopCustomer.bModelRequested AND NOT HAS_MODEL_LOADED(sData.sShopInfo.sShopCustomer.model))
				EXIT
			ENDIF
			
			// Set up new shop keeper
			IF NOT DOES_ENTITY_EXIST(sData.sShopInfo.sShopKeeper.pedID)
			AND sData.sShopInfo.sShopKeeper.bModelRequested
			
				// Make sure it's safe to create net shop keeper
				BOOL bCreateKeeper
				bCreateKeeper = FALSE
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.shopkeeperID)
					AND NOT serverBD.bShopKeeperSetupDone
						bCreateKeeper = TRUE
					ENDIF
				ELSE
					bCreateKeeper = TRUE
				ENDIF
				
				IF NOT bCreateKeeper
					IF NETWORK_IS_GAME_IN_PROGRESS()
						PRINT_SHOP_DEBUG_LINE("Using net shop keeper..")
						UPDATE_SHOP_KEEPER_PED_ID(sData.sShopInfo.sShopKeeper)
						
						IF NOT sData.sShopInfo.sShopKeeper.bNetKeeperSetup
							PRINT_SHOP_DEBUG_LINE("Waiting for shop keeper to be initialised")
							EXIT
						ENDIF
					ENDIF
				ELSE
					PRINT_SHOP_DEBUG_LINE("Creating shop keeper")
					CLEAR_AREA(sData.sShopInfo.sShopKeeper.vPosIdle, 2.5, TRUE)
					
					IF NETWORK_IS_GAME_IN_PROGRESS()
						IF NOT CREATE_NET_PED(serverBD.shopkeeperID, PEDTYPE_CIVMALE, sData.sShopInfo.sShopKeeper.model, sData.sShopInfo.sShopKeeper.vPosIdle, sData.sShopInfo.sShopKeeper.fHeadIdle, TRUE, TRUE, TRUE)
							EXIT
						ENDIF
						IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
							serverBD.bShopKeeperSetupDone = TRUE
						ENDIF
						UPDATE_SHOP_KEEPER_PED_ID(sData.sShopInfo.sShopKeeper)
					ELSE
						sData.sShopInfo.sShopKeeper.pedID = CREATE_PED(PEDTYPE_CIVFEMALE, sData.sShopInfo.sShopKeeper.model, sData.sShopInfo.sShopKeeper.vPosIdle, sData.sShopInfo.sShopKeeper.fHeadIdle, FALSE, FALSE)
					ENDIF
					
					IF DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE)
						IF sData.sShopInfo.eShop = TATTOO_PARLOUR_01_HW
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,6), 0, 1, 0) //(feet)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,10), 0, 1, 0) //(decl)
						ELIF sData.sShopInfo.eShop = TATTOO_PARLOUR_02_SS	
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,6), 0, 1, 0) //(feet)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
						ELIF sData.sShopInfo.eShop = TATTOO_PARLOUR_03_PB
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair) 
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,6), 0, 1, 0) //(feet)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,10), 0, 1, 0) //(decl)
						ELIF sData.sShopInfo.eShop = TATTOO_PARLOUR_04_VC			
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,6), 0, 1, 0) //(feet)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,10), 0, 1, 0) //(decl)
						ELIF sData.sShopInfo.eShop = TATTOO_PARLOUR_05_ELS
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,6), 0, 1, 0) //(feet)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,10), 0, 1, 0) //(decl)
						#IF FEATURE_TUNER
						ELIF sData.sShopInfo.eShop = TATTOO_PARLOUR_07_CCT
						#ENDIF
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,0), 0, 0, 0) //(head)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,5), 0, 0, 0) //(hand)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,6), 0, 0, 0) //(feet)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,11), 0, 0, 0) //(jbib)
							SET_PED_PROP_INDEX(sData.sShopInfo.sShopKeeper.pedID, ANCHOR_HEAD, 0)
							SET_PED_PROP_INDEX(sData.sShopInfo.sShopKeeper.pedID, ANCHOR_EYES, 0)
							SET_PED_PROP_INDEX(sData.sShopInfo.sShopKeeper.pedID, ANCHOR_EARS, 0)
							SET_PED_PROP_INDEX(sData.sShopInfo.sShopKeeper.pedID, ANCHOR_LEFT_WRIST, 0)
							SET_PED_PROP_INDEX(sData.sShopInfo.sShopKeeper.pedID, ANCHOR_RIGHT_WRIST, 0)
						ELSE	
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,0), 0, 1, 0) //(head)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,2), 0, 0, 0) //(hair)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,3), 0, 0, 0) //(uppr)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,6), 0, 1, 0) //(feet)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,10), 0, 0, 0) //(decl)
						ENDIF
						
						IF IS_SHOP_IN_AN_INTERIOR(sData.sShopInfo.eShop)
							RETAIN_ENTITY_IN_INTERIOR(sData.sShopInfo.sShopKeeper.pedID, GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_SHOP_COORDS(sData.sShopInfo.eShop), GET_SHOP_INTERIOR_TYPE(sData.sShopInfo.eShop)))
						ENDIF
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sData.sShopInfo.sShopKeeper.pedID, TRUE)
						SET_ENTITY_LOD_DIST(sData.sShopInfo.sShopKeeper.pedID, 300)
						SET_PED_LEG_IK_MODE(sData.sShopInfo.sShopKeeper.pedID,LEG_IK_OFF)
						
						IF NOT bRelGroupSetup
							TEXT_LABEL_15 sRelGroup
							sRelGroup = "TATARTIST"
							sRelGroup += ENUM_TO_INT(sData.sShopInfo.eShop)
							ADD_RELATIONSHIP_GROUP(sRelGroup, tattooGroup)
							SET_PED_RELATIONSHIP_GROUP_HASH(sData.sShopInfo.sShopKeeper.pedID, tattooGroup)
							//SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, tattooGroup)
							//SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, tattooGroup, RELGROUPHASH_PLAYER)
							bRelGroupSetup = TRUE
						ENDIF
						
						//SET_PED_AS_ENEMY(sData.sShopInfo.sShopKeeper.pedID, FALSE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sData.sShopInfo.sShopKeeper.pedID, TRUE)
						SET_PED_CONFIG_FLAG(sData.sShopInfo.sShopKeeper.pedID,PCF_UseKinematicModeWhenStationary,TRUE)
						SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sData.sShopInfo.sShopKeeper.pedID,FALSE)
						SET_PED_CAN_EVASIVE_DIVE(sData.sShopInfo.sShopKeeper.pedID,FALSE)
						SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(sData.sShopInfo.sShopKeeper.pedID,TRUE)
						
						IF IS_DLC_TATTOO_SHOP(sData.sShopInfo.eShop)
							NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(sData.sShopInfo.sShopKeeper.pedID, TRUE)
							SET_ENTITY_CAN_BE_DAMAGED(sData.sShopInfo.sShopKeeper.pedID, FALSE)
							SET_PED_AS_ENEMY(sData.sShopInfo.sShopKeeper.pedID, FALSE)
							SET_CURRENT_PED_WEAPON(sData.sShopInfo.sShopKeeper.pedID, WEAPONTYPE_UNARMED, TRUE)
							SET_PED_CAN_RAGDOLL(sData.sShopInfo.sShopKeeper.pedID, FALSE)
							SET_PED_RESET_FLAG(sData.sShopInfo.sShopKeeper.pedID, PRF_DisablePotentialBlastReactions, TRUE)
							SET_PED_CONFIG_FLAG(sData.sShopInfo.sShopKeeper.pedID, PCF_DontActivateRagdollFromExplosions, TRUE)
							SET_PED_CONFIG_FLAG(sData.sShopInfo.sShopKeeper.pedID, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
							SET_PED_CONFIG_FLAG(sData.sShopInfo.sShopKeeper.pedID, PCF_DisableExplosionReactions, TRUE)
							FREEZE_ENTITY_POSITION(sData.sShopInfo.sShopKeeper.pedID, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(sData.sShopInfo.sShopKeeper.pedID)
				g_bTattooArtistPedCreated = TRUE
			ENDIF
			
			IF DOES_ENTITY_EXIST(sData.sShopInfo.sShopKeeper.pedID)
			AND NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
			AND DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE)	
				REMOVE_PED_FOR_DIALOGUE(sData.sDialogueInfo.cultresConversation, ConvertSingleCharacter("D"))
				ADD_PED_FOR_DIALOGUE(sData.sDialogueInfo.cultresConversation, ConvertSingleCharacter("D"), sData.sShopInfo.sShopKeeper.pedID, "ShopTattooArtist")
				
				SET_AMBIENT_VOICE_NAME(sData.sShopInfo.sShopKeeper.pedID, "U_M_Y_TATTOO_01_WHITE_MINI_01")
				
				SET_MODEL_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopKeeper.model)
				sData.sShopInfo.sShopKeeper.bModelRequested = FALSE
				sData.sShopInfo.sShopKeeper.bSetup = TRUE
			ENDIF
				
			
			// Set up new shop customer
			IF NOT DOES_ENTITY_EXIST(sData.sShopInfo.sShopCustomer.pedID)
			AND sData.sShopInfo.sShopCustomer.bModelRequested
				
				PRINT_SHOP_DEBUG_LINE("Creating shop customer")
				CLEAR_AREA(sData.sShopInfo.sShopCustomer.vPosIdle, 2.5, TRUE)
				sData.sShopInfo.sShopCustomer.pedID = CREATE_PED(PEDTYPE_CIVMALE, sData.sShopInfo.sShopCustomer.model, sData.sShopInfo.sShopCustomer.vPosIdle, sData.sShopInfo.sShopCustomer.fHeadIdle, FALSE, FALSE)
				
				SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
				SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, INT_TO_ENUM(PED_COMPONENT,3), 0, 1, 0) //(uppr)
				SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, INT_TO_ENUM(PED_COMPONENT,4), 0, 1, 0) //(lowr)
				SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, INT_TO_ENUM(PED_COMPONENT,8), 0, 1, 0) //(accs)

				CLEAR_ALL_PED_PROPS(sData.sShopInfo.sShopCustomer.pedID)
				
				IF IS_SHOP_IN_AN_INTERIOR(sData.sShopInfo.eShop)
					RETAIN_ENTITY_IN_INTERIOR(sData.sShopInfo.sShopCustomer.pedID, GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_SHOP_COORDS(sData.sShopInfo.eShop), GET_SHOP_INTERIOR_TYPE(sData.sShopInfo.eShop)))
				ENDIF
				//SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sData.sShopInfo.sShopCustomer.pedID, TRUE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopCustomer.model)
				sData.sShopInfo.sShopCustomer.bModelRequested = FALSE
				
				sData.sShopInfo.sShopCustomer.bSetup = TRUE
				
				TASK_PLAY_ANIM_ADVANCED(sData.sShopInfo.sShopCustomer.pedID, sAnimDictTattoo, "customer_loop", 
				GET_SCENE_ORIGIN(eBaseShop, sData.sShopInfo.eShop),vChairRot, 4, -1, -1, AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_IGNORE_GRAVITY)
				TASK_PLAY_ANIM_ADVANCED(sData.sShopInfo.sShopKeeper.pedID, sAnimDictTattoo, "tattooist_loop", 
				GET_SCENE_ORIGIN(eBaseShop, sData.sShopInfo.eShop),vChairRot, 4, -1, -1, AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET | AF_IGNORE_GRAVITY)
				FREEZE_ENTITY_POSITION(sData.sShopInfo.sShopCustomer.pedID,TRUE)
				IF eBaseShop <> TATTOO_PARLOUR_01_HW
					REMOVE_ANIM_DICT(sAnimDictTattoo)
					sAnimDictTattoo  = sAnimDictPlayer
				ENDIF

			ENDIF
			
			IF NOT sData.sShopInfo.bRunEntryIntro
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				AND NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)	
				AND DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE)		
					TASK_LOOK_AT_ENTITY(sData.sShopInfo.sShopKeeper.pedID, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
				ENDIF
			ENDIF
			
			IF NETWORK_IS_GAME_IN_PROGRESS()	
				IF g_sMPTunables.b_onsale_tattoo_shop
					REQUEST_STREAMED_TEXTURE_DICT("MPShops")  //Request texture dict for the sale sign
				ENDIF
			ENDIF
			
			// Unlock the shop doors
			LOCK_SHOP_DOORS(sData.sShopInfo.eShop, FALSE)
			
			// Ignore shop kicking off state
			IF IS_DLC_TATTOO_SHOP(sData.sShopInfo.eShop)
				SET_SHOP_IGNORES_KICKING_OFF_CHECKS(sData.sShopInfo.eShop, TRUE)
			ENDIF
			
			sData.sBrowseInfo.iControl = 0
			sData.sShopInfo.eStage = SHOP_STAGE_WAIT_FOR_ENTRY
			
			PRINTLN(GET_THIS_SCRIPT_NAME(), ": ", GET_STRING_FROM_TEXT_FILE(GET_SHOP_NAME(sData.sShopInfo.eShop)), " initialised")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE: Run an intro cutscene if this is the first time we have visited this shop type
PROC DO_ENTRY_INTRO()

	IF IS_REPLAY_BEING_PROCESSED()
	OR IS_PLAYER_KICKING_OFF_IN_SHOP(sData.sShopInfo.eShop)
	OR NOT IS_PLAYER_OK_FOR_SHOP_INTRO(sData.sShopInfo)	
		EXIT
	ENDIF
	
	OBJECT_INDEX tattooGun
	FLOAT fDoorAngle
	BOOL bDoorLock
	
	IF sData.sShopInfo.bRunEntryIntro
		IF IS_SHOP_PED_OK()
			
			// Common cutscene stage functionality
			IF (sData.sBrowseInfo.iControl > 0)
				
				// Remove HUD
				HIDE_HUD_AND_RADAR_THIS_FRAME()
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				
				// Whilst cutscene is active
				IF (sData.sBrowseInfo.iControl < 5)
					IF NOT IS_SCREEN_FADING_OUT()
						
						// Player has skipped this sequence
						IF SKIP_SHOP_CUTSCENE()
							sData.sBrowseInfo.sInfoLabel = ""
							bDLCLabel = FALSE
							REMOVE_SHOP_HELP()
							CLEAR_HELP()
							DO_SCREEN_FADE_OUT(500)
							sData.sBrowseInfo.iBrowseTimer = GET_GAME_TIMER()
							sData.sBrowseInfo.iControl = 4
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// Display shop help messages
			IF  sData.sBrowseInfo.iControl >= 1
			AND sData.sBrowseInfo.iControl < 4
			AND LOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
				DISPLAY_SHOP_INTRO_MESSAGE(sData.sShopInfo.eShop, sData.sBrowseInfo.sInfoLabel)
			ENDIF

			SWITCH sData.sBrowseInfo.iControl
				
				// CAMERA SHOT 1 - START OF CUTSCENE
				CASE 0
				
					REQUEST_MODEL(V_ILEV_TA_TATGUN)
					
					IF IS_PLAYER_CONTROL_ON(PLAYER_ID())
					AND HAS_MODEL_LOADED(V_ILEV_TA_TATGUN)
					
						SET_SHOP_BIT(sData.sShopInfo.eShop, SHOP_NS_FLAG_SHOP_INTRO_RUNNING, TRUE)
						
						IF eBaseShop = TATTOO_PARLOUR_01_HW
							IF NOT DOES_ENTITY_EXIST(tattooGun)
								tattooGun = CREATE_OBJECT_NO_OFFSET(V_ILEV_TA_TATGUN,GET_TATTOO_GUN_POS(eBaseShop, sData.sShopInfo.eShop))
								SET_MODEL_AS_NO_LONGER_NEEDED(V_ILEV_TA_TATGUN)
							ENDIF
						ENDIF
						
						//PRE_INTRO_CUTSCENE_START()
						REMOVE_PED_HELMET(PLAYER_PED_ID(),TRUE)
						
						sData.sEntryInfo.iStage = 0
						GET_TATTOO_SHOP_ENTRY_INTRO_DATA(eBaseShop, sData.sShopInfo.eShop, sData.sEntryInfo)
						
						GET_TATTOO_SHOP_LOCATE_DATA(sData.sShopInfo.eShop, sData.sLocateInfo)
						IF IS_ANY_VEHICLE_NEAR_POINT(sData.sLocateInfo.vEntryPos[0],8)	
						AND NOT g_bInMultiplayer		
							REPOSITION_PLAYER_LAST_VEHICLE_FOR_SHOP(sData.sShopInfo)
						ENDIF
						
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(GET_TATTOO_DOOR_POS(eBaseShop, sData.sShopInfo.eShop), 20.0, GET_TATTOO_DOOR_MODEL(eBaseShop))
							GET_STATE_OF_CLOSEST_DOOR_OF_TYPE(GET_TATTOO_DOOR_MODEL(eBaseShop), GET_TATTOO_DOOR_POS(eBaseShop, sData.sShopInfo.eShop), bDoorLock, fDoorAngle)
						ENDIF
						
						CLEAR_AREA_OF_PEDS(vChairPos, 4.0)
						
						CLEAR_AREA_OF_PROJECTILES(vChairPos, 50)
						
						// Setup player for cutscene
						CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
						CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
						
						SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
						SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
						SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
						
						// Player enters the tattoo shop and waits..
						IF eBaseShop <> TATTOO_PARLOUR_01_HW
							vChairPos.z -= 0.032
						ENDIF
						iSyncScenePlayer = CREATE_SYNCHRONIZED_SCENE(vChairPos, vChairRot)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSyncScenePlayer, FALSE)
						TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSyncScenePlayer, sAnimDictPlayer, GET_INTRO_ANIM_PLAYER(eBaseShop), INSTANT_BLEND_IN, -1)
						PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(GET_TATTOO_DOOR_POS(eBaseShop, sData.sShopInfo.eShop), 5.0, GET_TATTOO_DOOR_MODEL(eBaseShop), iSyncScenePlayer, GET_INTRO_ANIM_PLAYER_DOOR(eBaseShop), sAnimDictPlayer, INSTANT_BLEND_IN)
						CPRINTLN(DEBUG_SHOPS, "[SHOPS] playing anims on player: ", sAnimDictPlayer, "---", GET_INTRO_ANIM_PLAYER(eBaseShop))
						
						// Tattoo artist finishes up with customer
						iSyncSceneKeeper = CREATE_SYNCHRONIZED_SCENE(vChairPos, vChairRot)
						SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(iSyncSceneKeeper, FALSE)
						
						// Customer
						IF DOES_ENTITY_EXIST(sData.sShopInfo.sShopCustomer.pedID)
							IF NOT IS_ENTITY_DEAD(sData.sShopInfo.sShopCustomer.pedID)
								TASK_SYNCHRONIZED_SCENE(sData.sShopInfo.sShopCustomer.pedID, iSyncSceneKeeper, sAnimDictTattoo, GET_INTRO_ANIM_CUSTOMER(eBaseShop), INSTANT_BLEND_IN, INSTANT_BLEND_OUT)	
								FREEZE_ENTITY_POSITION(sData.sShopInfo.sShopCustomer.pedID,FALSE)
								
								SET_ENTITY_PROOFS(sData.sShopInfo.sShopCustomer.pedID, TRUE, TRUE, TRUE, TRUE, TRUE)
							ENDIF
						ENDIF
						CPRINTLN(DEBUG_SHOPS, "[SHOPS] playing anims on custimer: ", sAnimDictTattoo, "---", GET_INTRO_ANIM_CUSTOMER(eBaseShop))
						
						// Tattooist
						IF DOES_ENTITY_EXIST(sData.sShopInfo.sShopKeeper.pedID)
							IF NOT IS_ENTITY_DEAD(sData.sShopInfo.sShopKeeper.pedID)
								TASK_SYNCHRONIZED_SCENE(sData.sShopInfo.sShopKeeper.pedID, iSyncSceneKeeper, sAnimDictTattoo, GET_INTRO_ANIM_TATTOOIST(eBaseShop), INSTANT_BLEND_IN, -1)
								// Type 1 shop has tattoo gun
								IF eBaseShop = TATTOO_PARLOUR_01_HW
									IF DOES_ENTITY_EXIST(tattooGun)
										PLAY_SYNCHRONIZED_ENTITY_ANIM(tattooGun, iSyncSceneKeeper, GET_INTRO_ANIM_TATTOO_GUN(eBaseShop), sAnimDictTattoo, INSTANT_BLEND_IN, -2)
										FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(tattooGun)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						CPRINTLN(DEBUG_SHOPS, "[SHOPS] playing anims on tattooinst: ", sAnimDictTattoo, "---", GET_INTRO_ANIM_TATTOOIST(eBaseShop))
						
						// Create and initialise first camera
						IF NOT DOES_CAM_EXIST(sData.sEntryInfo.camID)
							
							sData.sEntryInfo.camID = CREATE_CAM("DEFAULT_ANIMATED_CAMERA")				
							//	sData.sEntryInfo.camID = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
						
						ENDIF
						
						SET_CAM_ACTIVE(sData.sEntryInfo.camID, TRUE)
						
						IF eBaseShop = TATTOO_PARLOUR_01_HW	
							PLAY_SYNCHRONIZED_CAM_ANIM(sData.sEntryInfo.camID,iSyncScenePlayer, "shop_ig_4_b_cam",sAnimDictPlayer)	
						ELSE
							PLAY_SYNCHRONIZED_CAM_ANIM(sData.sEntryInfo.camID,iSyncScenePlayer, "shop_ig_5_b_cam",sAnimDictPlayer)	
							//SET_CAM_PARAMS(sData.sEntryInfo.camID, sData.sEntryInfo.vCamCoord[0], sData.sEntryInfo.vCamRot[0], sData.sEntryInfo.fCamFov[0], 0)
							//SET_CAM_PARAMS(sData.sEntryInfo.camID, sData.sEntryInfo.vCamCoord[1], sData.sEntryInfo.vCamRot[1], sData.sEntryInfo.fCamFov[1], 30000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)   //6000
						ENDIF
						
						SET_ROOM_FOR_GAME_VIEWPORT_BY_KEY(sData.sShopInfo.iRoomKey)
						RENDER_SCRIPT_CAMS(TRUE, FALSE)
						
						// Cutscene safety setup
						START_SHOP_CUTSCENE(sData.sShopInfo,1)
						
						/*
						IF eBaseShop <> TATTOO_PARLOUR_01_HW
							IF fDoorAngle < 0.1	
								SET_SYNCHRONIZED_SCENE_PHASE(iSyncScenePlayer, 0.017)
							ENDIF
							IF fDoorAngle >= 0.1
							AND fDoorAngle < 0.2	
								SET_SYNCHRONIZED_SCENE_PHASE(iSyncScenePlayer, 0.066)
							ENDIF
							IF fDoorAngle >= 0.2
							AND fDoorAngle < 0.4
								SET_SYNCHRONIZED_SCENE_PHASE(iSyncScenePlayer, 0.082)
							ENDIF
							IF fDoorAngle >= 0.4
							AND fDoorAngle < 0.6
								SET_SYNCHRONIZED_SCENE_PHASE(iSyncScenePlayer, 0.100)
							ENDIF
							IF fDoorAngle >= 0.6
								SET_SYNCHRONIZED_SCENE_PHASE(iSyncScenePlayer, 0.151)
							ENDIF
						ELSE
							//IF fDoorAngle > -0.1
							//	SET_SYNCHRONIZED_SCENE_PHASE(iSyncScenePlayer, 0.035)
							//ENDIF
							IF fDoorAngle <= -0.1
							AND fDoorAngle > -0.2	
								SET_SYNCHRONIZED_SCENE_PHASE(iSyncScenePlayer, 0.060)
							ENDIF
							IF fDoorAngle <= -0.2
							AND fDoorAngle > -0.4
								SET_SYNCHRONIZED_SCENE_PHASE(iSyncScenePlayer, 0.088)
							ENDIF
							IF fDoorAngle <= -0.4
							AND fDoorAngle > -0.6
								SET_SYNCHRONIZED_SCENE_PHASE(iSyncScenePlayer, 0.111)
							ENDIF
							IF fDoorAngle <= -0.6
								SET_SYNCHRONIZED_SCENE_PHASE(iSyncScenePlayer, 0.150)
							ENDIF
						ENDIF
						*/
					
						//POST_INTRO_CUTSCENE_START()
							
						PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_TATTOO_APPLIED", "SPEECH_PARAMS_FORCE")		
							
						// Hide our buddies
						sBuddyHideData.buddyHide = TRUE

						// Setup intial help message
						sData.sBrowseInfo.sInfoLabel = "SHOP_INTRO"
						bDLCLabel = FALSE
						sData.sBrowseInfo.iBrowseTimer = GET_GAME_TIMER()
						sData.sBrowseInfo.iControl++
					ENDIF
				BREAK
		
				// CAMERA SHOT 2
				CASE 1
					IF (GET_GAME_TIMER() - sData.sBrowseInfo.iBrowseTimer) >= 5750
						
						IF NOT IS_PED_INJURED(sData.sShopInfo.sShopCustomer.pedID)	
							PLAY_PED_AMBIENT_SPEECH(sData.sShopInfo.sShopCustomer.pedID, "GENERIC_THANKS")
						ENDIF
						
						sData.sBrowseInfo.sInfoLabel = "TAT_INTRO_0"
						bDLCLabel = FALSE
						sData.sBrowseInfo.iBrowseTimer = GET_GAME_TIMER()
						sData.sBrowseInfo.iControl++
					ENDIF
				BREAK

				// CAMERA SHOT 3
				CASE 2
					IF (GET_GAME_TIMER() - sData.sBrowseInfo.iBrowseTimer) >= 5750
						
						IF NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)	
							PLAY_PED_AMBIENT_SPEECH(sData.sShopInfo.sShopKeeper.pedID, "GENERIC_BYE")
						ENDIF
						
						// Swap synced door over to customer from player
						STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(GET_TATTOO_DOOR_POS(eBaseShop, sData.sShopInfo.eShop), 5.0, GET_TATTOO_DOOR_MODEL(eBaseShop), INSTANT_BLEND_OUT)
						PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(GET_TATTOO_DOOR_POS(eBaseShop, sData.sShopInfo.eShop), 5.0, GET_TATTOO_DOOR_MODEL(eBaseShop), iSyncSceneKeeper, GET_INTRO_ANIM_CUSTOMER_DOOR(eBaseShop), sAnimDictTattoo, INSTANT_BLEND_IN)	

						sData.sBrowseInfo.sInfoLabel = "TAT_INTRO_1"
						bDLCLabel = FALSE
						sData.sBrowseInfo.iBrowseTimer = GET_GAME_TIMER()
						sData.sBrowseInfo.iControl++
					ENDIF
				BREAK
				
				// CHECK FOR END OF SCENE
				CASE 3
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenePlayer)
						//Wait until we are done with the anims.
						sData.sBrowseInfo.iControl = 5
					ENDIF
						
					IF eBaseShop = TATTOO_PARLOUR_01_HW
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenePlayer)
						AND GET_SYNCHRONIZED_SCENE_PHASE(iSyncScenePlayer) >= 0.62 //0.877
							IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK	
								TASK_LOOK_AT_ENTITY(PLAYER_PED_ID(),sData.sShopInfo.sShopKeeper.pedID,4000)
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							ENDIF
						ENDIF
					ELSE	
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenePlayer)
						AND GET_SYNCHRONIZED_SCENE_PHASE(iSyncScenePlayer) >= 0.877
							IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK	
								CLEAR_PED_TASKS(PLAYER_PED_ID())
								FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
							ENDIF
						ENDIF
					ENDIF
					IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneKeeper)
						IF GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneKeeper) >= 0.955
							STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(GET_TATTOO_DOOR_POS(eBaseShop, sData.sShopInfo.eShop), 5.0, GET_TATTOO_DOOR_MODEL(eBaseShop), INSTANT_BLEND_OUT)
							IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(GET_TATTOO_DOOR_POS(eBaseShop, sData.sShopInfo.eShop), 20.0, GET_TATTOO_DOOR_MODEL(eBaseShop))
								SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(GET_TATTOO_DOOR_MODEL(eBaseShop), GET_TATTOO_DOOR_POS(eBaseShop, sData.sShopInfo.eShop), FALSE, 0)
							ENDIF
						ENDIF
						IF GET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneKeeper) >= 0.9	
							IF NOT bDlgChk_Greet_Triggered	
								IF GET_SCRIPT_TASK_STATUS(sData.sShopInfo.sShopKeeper.pedID,SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK	
									CLEAR_PED_TASKS(sData.sShopInfo.sShopKeeper.pedID)
									FORCE_PED_AI_AND_ANIMATION_UPDATE(sData.sShopInfo.sShopKeeper.pedID)
									IF Is_Ped_Drunk(PLAYER_PED_ID())
									OR (IS_PED_IN_UNDERWEAR_SP(PLAYER_PED_ID()) AND NOT NETWORK_IS_GAME_IN_PROGRESS())
										PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_GREET_UNUSUAL", "SPEECH_PARAMS_FORCE")
									ELSE	
										IF NOT NETWORK_IS_GAME_IN_PROGRESS()
										AND GET_ENTITY_MODEL(PLAYER_PED_ID()) = PLAYER_ZERO
										AND (IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON)
										OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON_WITH_MEDAL))
											PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "KIFFLOM_GREET", "SPEECH_PARAMS_FORCE")
//											ELIF IS_SPECIAL_EDITION_GAME() 
//											OR IS_COLLECTORS_EDITION_GAME()
//												PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_GREET_SPECIAL", "SPEECH_PARAMS_FORCE")
										ELSE
											PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_GREET", "SPEECH_PARAMS_FORCE")
										ENDIF
									ENDIF
									bDlgChk_Greet_Triggered = TRUE
								ENDIF
							ENDIF
						ENDIF	
					ENDIF
				BREAK
				
				// SKIPPED CUTSCENE STAGE
				CASE 4
					IF IS_SCREEN_FADED_OUT() 
					AND NOT IS_SCREEN_FADING_OUT()
						
						STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(GET_TATTOO_DOOR_POS(eBaseShop, sData.sShopInfo.eShop), 5.0, GET_TATTOO_DOOR_MODEL(eBaseShop), INSTANT_BLEND_OUT)
						IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(GET_TATTOO_DOOR_POS(eBaseShop, sData.sShopInfo.eShop), 20.0, GET_TATTOO_DOOR_MODEL(eBaseShop))
							SET_STATE_OF_CLOSEST_DOOR_OF_TYPE(GET_TATTOO_DOOR_MODEL(eBaseShop), GET_TATTOO_DOOR_POS(eBaseShop, sData.sShopInfo.eShop), FALSE, 0)
						ENDIF
						
						// Stop synchronised scene for player
						IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScenePlayer)
							IF NOT IS_PED_INJURED(PLAYER_PED_ID())
								TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), iSyncScenePlayer, sAnimDictPlayer, GET_INTRO_ANIM_PLAYER(eBaseShop), INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
							ENDIF
							SET_SYNCHRONIZED_SCENE_PHASE(iSyncScenePlayer,1)
						ENDIF
						
						// Stop scene for tattoo artist
						IF NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)	
							IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncSceneKeeper)
								SET_SYNCHRONIZED_SCENE_PHASE(iSyncSceneKeeper,1)
							ENDIF
						ENDIF
						
						sData.sBrowseInfo.iControl++
					ENDIF
				BREAK
				
				CASE 5
				
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					
					// Return to gameplay cam
					IF DOES_CAM_EXIST(sData.sEntryInfo.camID)
						IF IS_CAM_ACTIVE(sData.sEntryInfo.camID)
							SET_CAM_ACTIVE(sData.sEntryInfo.camID, FALSE)
						ENDIF
						DESTROY_CAM(sData.sEntryInfo.camID)
					ENDIF
				
					// Set camera behind player
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
					
					//IF eBaseShop <> TATTOO_PARLOUR_01_HW		
					//	TASK_ACHIEVE_HEADING(PLAYER_PED_ID(),GET_ENTITY_HEADING(PLAYER_PED_ID()) - 19.0,0)
					//ENDIF
					
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					CLEAR_HELP()
					
					UNLOCK_PLAYER_FOR_BROWSE(sData.sBrowseInfo)
						
					// Remove customer
					IF DOES_ENTITY_EXIST(sData.sShopInfo.sShopCustomer.pedID)
					AND NOT IS_PED_INJURED(sData.sShopInfo.sShopCustomer.pedID)
						SET_PED_MIN_MOVE_BLEND_RATIO(sData.sShopInfo.sShopCustomer.pedID, PEDMOVE_WALK)
						SET_ENTITY_PROOFS(sData.sShopInfo.sShopCustomer.pedID, FALSE, FALSE, FALSE, FALSE, FALSE)
						SET_PED_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopCustomer.pedID)
					ENDIF

					// Return to gameplay settings
					END_SHOP_CUTSCENE()
	
					// Remove shop help message and assets
					REMOVE_SHOP_HELP()
					UNLOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
					REMOVE_TATTOO_SHOP_INTRO_ANIMS()
					
					// Ensure screen fades back in
					IF NOT IS_SCREEN_FADED_IN()
						IF NOT IS_SCREEN_FADING_IN()
							CPRINTLN(DEBUG_SHOPS, "END_SHOP_CUTSCENE - Fading in screen")
							DO_SCREEN_FADE_IN(500)
						ENDIF
					ENDIF
					
					// Intro complete so set all clothes shop intros complete
					INT iShop
					REPEAT NUMBER_OF_SHOPS iShop
						IF GET_SHOP_TYPE_ENUM(INT_TO_ENUM(SHOP_NAME_ENUM, iShop)) = SHOP_TYPE_TATTOO
							SET_SHOP_HAS_RUN_ENTRY_INTRO(INT_TO_ENUM(SHOP_NAME_ENUM, iShop), TRUE)
						ENDIF
					ENDREPEAT
					
					sData.sShopInfo.bRunEntryIntro = FALSE
					
					// Stop hiding our buddies
					sBuddyHideData.buddyHide = FALSE
					
				BREAK
			ENDSWITCH

		ENDIF
	ELSE
		// Shop intro scene has finished - move on to next stage
		CLEAR_SHOP_BIT(sData.sShopInfo.eShop, SHOP_NS_FLAG_SHOP_INTRO_RUNNING, TRUE)
		SET_PLAYER_HAS_VISITED_SHOP(sData.sShopInfo.eShop, TRUE)
		sData.sShopInfo.eStage = SHOP_STAGE_WAIT_FOR_TRIGGER
	ENDIF
	
	UPDATE_BUDDIES_FOR_SHOP_CUTSCENE(sBuddyHideData)
ENDPROC

/// PURPOSE: Checks for the player entering the shop
PROC CHECK_TATTOO_SHOP_ENTRY()
	IF IS_SHOP_PED_OK()
		
		UPDATE_PLAYER_IN_SHOP_STATE(sData.sShopInfo)
	
		IF sData.sShopInfo.bPlayerInRoom = TRUE
			// Increment visit count
			IF NOT g_bInMultiplayer
			AND NOT bInitialEntryComplete	
				g_savedGlobals.sShopData.iTattooShopVisits++
				bInitialEntryComplete = TRUE
			ENDIF
			
			sData.sBrowseInfo.iCurrentLocate = -1			
			sData.sShopInfo.eStage	= SHOP_STAGE_ENTRY_INTRO
			sData.sBrowseInfo.iControl = 0
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Checks to see if the player has left the shop
PROC CHECK_TATTOO_SHOP_EXIT()
	IF IS_SHOP_PED_OK()
		
		UPDATE_PLAYER_IN_SHOP_STATE(sData.sShopInfo)
		
		IF NOT sData.sShopInfo.bPlayerInRoom
			IF sData.sShopInfo.iContextID != NEW_CONTEXT_INTENTION
				RELEASE_CONTEXT_INTENTION(sData.sShopInfo.iContextID)
			ENDIF
			REMOVE_SHOP_HELP()
		ENDIF
		
		IF NOT sData.sShopInfo.bPlayerInShop
			sData.sShopInfo.eStage = SHOP_STAGE_WAIT_FOR_ENTRY
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Checks for the player entering any of the shop locates
PROC CHECK_TATTOO_LOCATE_ENTRY()

	BOOL bLoseCops
	BOOL bShopClosed
	BOOL bPlayerInTattooLocate = FALSE
	BOOL bDisplayContext = FALSE
	
	IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
		IF sData.sBrowseInfo.iCurrentLocate = -1
			sData.sBrowseInfo.iCurrentLocate = g_sTransitionSessionData.sEndReserve.iShopLocateRestore
			GET_TATTOO_SHOP_LOCATE_DATA(sData.sShopInfo.eShop, sData.sLocateInfo)
		ENDIF
	ENDIF
	
	IF IS_SHOP_PED_OK()
		IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
		OR (NOT IS_PAUSE_MENU_ACTIVE() AND NOT IS_SYSTEM_UI_BEING_DISPLAYED() AND NOT IS_WARNING_MESSAGE_ACTIVE() AND NOT g_sShopSettings.bProcessStoreAlert
		AND IS_SHOP_LOCATES_SAFE_TO_BROWSE(sData.sShopInfo.eShop)
		AND NOT IS_SHOP_LOCATES_BLOCKED(sData.sShopInfo.eShop))
		
			// Grab the locate data once
			IF sData.sBrowseInfo.iCurrentLocate = -1
				GET_TATTOO_SHOP_LOCATE_DATA(sData.sShopInfo.eShop, sData.sLocateInfo)
				sData.sBrowseInfo.iCurrentLocate = 0
			ENDIF
			
			IF SHOULD_PLAYER_LOSE_COPS_FOR_SHOP()
				bLoseCops = TRUE
			ELIF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sData.sLocateInfo.vEntryPos[0], sData.sLocateInfo.vEntryPos[1], sData.sLocateInfo.fEntrySize[0], FALSE, TRUE, TM_ON_FOOT)
			OR SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
				PRELOAD_NAKED_VARIATION() 
				bPlayerInTattooLocate = TRUE
			ENDIF
		ELSE
			IF NETWORK_IS_GAME_IN_PROGRESS()
				IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
					// Only display help if no other message is on screen
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("SHOP_CLOSED", GET_SHOP_NAME(sData.sShopInfo.eShop))
						bShopClosed = TRUE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	
	IF bLoseCops
		PRINT_SHOP_HELP("SHOP_COPS") // Lose the cops.
	ELIF bShopClosed
		IF !IS_PLAYER_AN_ANIMAL(PLAYER_ID())
		#IF FEATURE_TUNER
		AND NOT IS_DLC_TATTOO_SHOP(sData.sShopInfo.eShop)
		#ENDIF
			PRINT_SHOP_HELP_WITH_STRING("SHOP_CLOSED", GET_SHOP_NAME(sData.sShopInfo.eShop))
		ENDIF	
	ELIF IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
	AND NETWORK_IS_GAME_IN_PROGRESS()
		// Only display help if no other message is on screen
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("SHOP_NONE", GET_SHOP_NAME(sData.sShopInfo.eShop))
			PRINT_SHOP_HELP_WITH_STRING("SHOP_JUGG_NONE", GET_SHOP_NAME(sData.sShopInfo.eShop))
		ENDIF
	ELIF bPlayerInTattooLocate
		bDisplayContext = TRUE
		
		#IF FEATURE_TUNER
		IF (sData.sShopInfo.eShop = TATTOO_PARLOUR_07_CCT)
		AND NOT HAS_PLAYER_PURCHASED_CAR_CLUB_MEMBERSHIP(PLAYER_ID())
			IF NOT IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CMM_HT_TAT")
				PRINT_HELP_FOREVER("CMM_HT_TAT")
			ENDIF
		ELSE
		#ENDIF
			
			IF sData.sShopInfo.iContextID = NEW_CONTEXT_INTENTION
				REGISTER_CONTEXT_INTENTION(sData.sShopInfo.iContextID, CP_MEDIUM_PRIORITY, "TAT_TRY_TAT", FALSE)	// Press ~a~ to browse tattoos.
			ENDIF
			
			SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(),FALSE)
			
			SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CONTEXT)
			IF HAS_CONTEXT_BUTTON_TRIGGERED(sData.sShopInfo.iContextID)
			OR SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
				sData.sShopInfo.eStage = SHOP_STAGE_BROWSE_ITEMS
				sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_INIT
				
				#IF FEATURE_TUNER
				IF sData.sShopInfo.eShop = TATTOO_PARLOUR_07_CCT
					START_AUDIO_SCENE("Ls_Car_Meet_Tattoo_Shop_Scene")
				ENDIF
				#ENDIF
				
				iHeadBlendTimer = -1
				
				// Set the browsing flag straight away
				UPDATE_SHOP_BROWSE_STATE(sData.sShopInfo, sData.sBrowseInfo)
				
				// Take note of the players component and prop variations
				STORE_CURRENT_PED_COMPONENTS()
				
				sData.eCurrentTattoo = INVALID_TATTOO
				
				bDisplayContext = FALSE
			ENDIF
			
		#IF FEATURE_TUNER
		ENDIF
		#ENDIF
		
	ELIF NOT bPlayerInTattooLocate
		
		#IF FEATURE_TUNER
		IF (sData.sShopInfo.eShop = TATTOO_PARLOUR_07_CCT)
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("CMM_HT_TAT")
				CLEAR_HELP()
			ENDIF
		ENDIF
		#ENDIF
		
	ENDIF
		
	IF NOT bDisplayContext
		IF sData.sShopInfo.iContextID != NEW_CONTEXT_INTENTION
			RELEASE_CONTEXT_INTENTION(sData.sShopInfo.iContextID)
			SET_PED_CAN_PLAY_AMBIENT_ANIMS(PLAYER_PED_ID(),TRUE)
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PROCESS_TATTOO_SHOPPING_BASKET(INT iPrice)
	IF sData.sBrowseInfo.bProcessingBasket
		PRINTLN("[TATTOO_BASKET] - Processing basket [", sData.sBrowseInfo.iProcessingBasketStage, "]...")
		SWITCH sData.sBrowseInfo.iProcessingBasketStage
			// 1st attempt
			CASE 0
				IF NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
					

					// Override the players cash.
					INT iWalletAmount, iBankAmount, iTempPrice
					iWalletAmount = 0
					iBankAmount = 0
					iTempPrice = iPrice
					IF bDidPlayerHaveCoupon
						iTempPrice = 0
					ENDIF
					// Bank first
					IF (NETWORK_GET_VC_BANK_BALANCE() > 0)
						IF NETWORK_GET_VC_BANK_BALANCE() >= iTempPrice
							iBankAmount = iTempPrice
						ELSE
							iBankAmount = iTempPrice-(iTempPrice-NETWORK_GET_VC_BANK_BALANCE())
						ENDIF
						iTempPrice -= iBankAmount
					ENDIF
					IF iTempPrice > 0
						IF (NETWORK_GET_VC_WALLET_BALANCE() > 0)
							IF NETWORK_GET_VC_WALLET_BALANCE() >= iTempPrice
								iWalletAmount = iTempPrice
							ELSE
								iWalletAmount = iTempPrice-(iTempPrice-NETWORK_GET_VC_WALLET_BALANCE())
							ENDIF
							iTempPrice -= iWalletAmount
						ENDIF
					ENDIF
					#IF IS_DEBUG_BUILD
					IF iTempPrice > 0
						SCRIPT_ASSERT("PROCESSING_VEND_SHOPPING_BASKET - Player can't afford this item!")	// iTempPrice=", iTempPrice, ", iWalletAmount=", iWalletAmount, ", iBankAmount", iBankAmount)
					ENDIF
					#ENDIF
				
					PRINTLN("[TATTOO_BASKET] - CHANGE_FAKE_MP_CASH(wallet=$-", iWalletAmount, ", bank=$-", iBankAmount, "): iPrice=$", iPrice, ", bankBalance=$", NETWORK_GET_VC_BANK_BALANCE(), ", walletBalance=$", NETWORK_GET_VC_WALLET_BALANCE())
					USE_FAKE_MP_CASH(TRUE)
					CHANGE_FAKE_MP_CASH(-iWalletAmount, -iBankAmount)
				
					PRINTLN("[TATTOO_BASKET] - Basket transaction started!")
					sData.sBrowseInfo.iProcessingBasketStage = 70 // Pending
				ELSE
					PRINTLN("[TATTOO_BASKET] - Basket transaction failed to start checkout!")
					sData.sBrowseInfo.iProcessingBasketStage = 90 // Failed
				ENDIF
			BREAK
			
			// Pending
			CASE 70
				IF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
						PRINTLN("[TATTOO_BASKET] - Basket transaction finished, success!")
						sData.sBrowseInfo.iProcessingBasketStage = 80 // Success
					ELSE
						PRINTLN("[TATTOO_BASKET] - Basket transaction finished, failed!")
						sData.sBrowseInfo.iProcessingBasketStage = 90 // Failed
					ENDIF
				ENDIF
			BREAK
			
			// Success
			CASE 80
				
				USE_FAKE_MP_CASH(FALSE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
				
				sData.sBrowseInfo.bProcessingBasket = FALSE
				sData.sBrowseInfo.iProcessingBasketStage = 0
			BREAK
			
			//Failed
			CASE 90
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				
				USE_FAKE_MP_CASH(FALSE)
				HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
				
				// [TODO] Notify player
				
				sData.sBrowseInfo.bProcessingBasket = FALSE
				sData.sBrowseInfo.iProcessingBasketStage = 0
				
				RETURN FALSE
			BREAK
		ENDSWITCH
	ENDIF
	RETURN TRUE
ENDFUNC

///PURPOSE: Attempts to purchase an item for the current player character
FUNC BOOL PURCHASE_TATTOO_ITEM()

	IF IS_SHOP_PED_OK()
	
		sInvalidCatalogueKey = ""
		sData.sBrowseInfo.bPurchaseFailed = FALSE
	
		TATTOO_DATA_STRUCT sTattooData
		TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
		
		IF GET_TATTOO_DATA(sTattooData, sData.eCurrentTattoo, eFaction, PLAYER_PED_ID())
			
			BOOL bDoPayment = TRUE
			BOOL bAddDiscountModifier = FALSE
			INT iDisplayPrice = GET_TATTOO_DISPLAY_PRICE(sTattooData.iCost, sTattooData.sLabel)
			
			// Apply Luxe1 tattoo multiplier
			#IF NOT IS_NEXTGEN_BUILD
			IF iDisplayPrice != sTattooData.iCost
				INT iLabelHash = GET_HASH_KEY(GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(sTattooData.sLabel, 0, 6))
				IF (iLabelHash = HASH("TAT_LX"))
					sTattooData.iCost = ROUND((TO_FLOAT(sTattooData.iCost) * g_sMPtunables.fluxe1_all_multiplier_male_and_female_tattoos))
				ELIF (iLabelHash = HASH("TAT_L2"))
					sTattooData.iCost = ROUND((TO_FLOAT(sTattooData.iCost) * g_sMPtunables.fLuxe2_All_Tattoos))
				ENDIF
			ENDIF
			#ENDIF
			
			
			IF iDisplayPrice = -1
				sInvalidCatalogueKey = sTattooData.sLabel
				RETURN FALSE
			ENDIF
			
			IF iDisplayPrice != sTattooData.iCost
				CWARNINGLN(DEBUG_SHOPS, "PURCHASE_TATTOO_ITEM \"", GET_STRING_FROM_TEXT_FILE(sTattooData.sLabel), "\" (\"", sTattooData.sLabel, "\") - sTattooData.iCost (", sTattooData.iCost, ") != iDisplayPrice (", iDisplayPrice, ")")
				
				sTattooData.iCost = iDisplayPrice
			ENDIF
			
			APPLY_TATTOO_DISCOUNT(sTattooData.iCost, sTattooData.sLabel)
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND IS_MP_TATTOO_PURCHASED(sData.eCurrentTattoo)
			AND NOT SHOULD_TATTOO_BE_FREE_FOR_PLAYER(sTattooData.sLabel)
				sTattooData.iCost = g_sMPTunables.iPURCHASED_TATTOO_APPLICATION_FEE //#1831283 #1892437
				bDoPayment = TRUE
				bAddDiscountModifier = TRUE
			ENDIF

			IF sData.sBrowseInfo.bProcessingBasket
				IF NOT PROCESS_TATTOO_SHOPPING_BASKET(sTattooData.iCost)
					sData.sBrowseInfo.bPurchaseFailed = TRUE
					RETURN FALSE
				ELIF NOT sData.sBrowseInfo.bProcessingBasket
					//INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_TATTOOS, sTattooData.iCost)
					SET_PACKED_STAT_BOOL(PACKED_MP_BOUGHT_TATTOO,TRUE)
					INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_INGAMESTORE_MONEYSPENT, sTattooData.iCost)
					SET_PLAYER_HAS_PURCHASED_ITEM_IN_SHOP(sData.sShopInfo.eShop, TRUE, PURCHASE_TATTOOS)
					SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_TATTOO)
					//CHECK_FRANKIE_SAYS_ACHIEVEMENT()
					
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ENDIF
				
				// Always return true when we are processing (unless we failed!)
				RETURN TRUE
			ENDIF
		
			// Telemetry
			PED_DECORATION_ZONE eZone = PDZ_INVALID
			TORSO_DECORATION_SUBZONE eSubzone = TDS_INVALID
			GET_PED_DECORATION_ZONE_FROM_INT(sData.sBrowseInfo.iCurrentGroup, eZone, eSubzone)
			BOOl bOnSale = SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO(sTattooData.eEnum, sTattooData.sLabel, eZone, eSubzone)
			STORE_CURRENT_SHOP_ITEM_DATA(sTattooData.sLabel, sTattooData.iCost, DEFAULT, DEFAULT, DEFAULT, bOnSale)
		
			IF g_bInMultiplayer
				// Check if we can afford
				//IF (NETWORK_GET_VC_BANK_BALANCE()+NETWORK_GET_VC_WALLET_BALANCE()  >= sTattooData.iCost)
				IF (sTattooData.iCost = 0 OR NETWORK_CAN_SPEND_MONEY(sTattooData.iCost, FALSE, TRUE, FALSE))

				
					IF DOES_SAVE_HAVE_COUPON(COUPON_TATOO)
					OR bDidPlayerHaveCoupon
						bDidPlayerHaveCoupon = TRUE
					ENDIF
					
					IF NETWORK_IS_GAME_IN_PROGRESS()
					AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
						BOOL bStarterPackItem = FALSE
						IF SHOULD_TATTOO_BE_FREE_FOR_PLAYER(sTattooData.sLabel)
							bStarterPackItem = TRUE
						ENDIF
						
						TEXT_LABEL_63 tlCatalogueKey
						GENERATE_KEY_FOR_SHOP_CATALOGUE(tlCatalogueKey, sTattooData.sLabel, GET_ENTITY_MODEL(PLAYER_PED_ID()), SHOP_TYPE_TATTOO, 0, 0, DEFAULT, DEFAULT, DEFAULT, bStarterPackItem)
						
						IF NOT NET_GAMESERVER_CATALOG_ITEM_IS_VALID(tlCatalogueKey)
							PRINTLN("[TATTOO_BASKET] - failed to find invalid item ", tlCatalogueKey, "!")
							sInvalidCatalogueKey = tlCatalogueKey
							RETURN FALSE
						ENDIF
						
						IF bDidPlayerHaveCoupon
							PRINTLN("[TATTOO_BASKET] - player has coupon, set iCost of ", tlCatalogueKey, " from ", sTattooData.iCost, " to FREE!")
							sTattooData.iCost = 0
						ENDIF
						
						IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_TATTOO, GET_HASH_KEY(tlCatalogueKey), NET_SHOP_ACTION_SPEND, 1, sTattooData.iCost, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
						
							// If we have already purchased this then the price will be $100.
							// For the server to validate this we also need to add the price override.
							//	0: F_TA_TAT_BB_000_t0_v0, quantity=1, cost=100                          - Item you’re buying at the modified price (specifying the modified price)
							//	1: itemID = TATTOO_PRICE_OVERRIDE, extraItemKeyHash= F_TA_TAT_BB_000_t0_v0  - Modifier item 
								
							IF bAddDiscountModifier
								IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_TATTOO, HASH("PO_REAPPLY_TATTOO"), NET_SHOP_ACTION_SPEND, 1, sTattooData.iCost, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, GET_HASH_KEY(tlCatalogueKey))
									// exit below
								ELSE
									PRINTLN("[TATTOO_BASKET] - failed to reapply tattoo!")
									
									DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
									
									USE_FAKE_MP_CASH(FALSE)
									HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
									
									sData.sBrowseInfo.bPurchaseFailed = TRUE
									RETURN FALSE
								ENDIF
							ENDIF
							
							IF bDidPlayerHaveCoupon
								IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_TATTOO, HASH("PO_COUPON_TATOO"), NET_SHOP_ACTION_SPEND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, GET_HASH_KEY(tlCatalogueKey))
									// exit below
									PRINTLN("[TATTOO_BASKET] - successfully added coupon!")
									
								ELSE
									PRINTLN("[TATTOO_BASKET] - failed to add coupon!")
									
									DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
									
									USE_FAKE_MP_CASH(FALSE)
									HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
									
									sData.sBrowseInfo.bPurchaseFailed = TRUE
									RETURN FALSE
								ENDIF
							ELSE
								PRINTLN("[TATTOO_BASKET] - player doesn't have coupon!")
							ENDIF
							
							sData.sBrowseInfo.bPurchaseFailed = FALSE
							sData.sBrowseInfo.bProcessingBasket = TRUE
							sData.sBrowseInfo.iProcessingBasketStage = 0
							RETURN TRUE
						ELSE
							PRINTLN("[TATTOO_BASKET] - failed to add tattoo ", tlCatalogueKey, "!")
							
							DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				
							USE_FAKE_MP_CASH(FALSE)
							HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
							
							sData.sBrowseInfo.bPurchaseFailed = TRUE
							RETURN FALSE
						ENDIF
					ENDIF
					
					IF sTattooData.iCost > 0
						//GIVE_LOCAL_PLAYER_CASH(-sTattooData.iCost) //  Cash now deducted in SET_PLAYER_HAS_PURCHASED_ITEM_IN_SHOP
					ENDIF
					
					IF bDoPayment
						//INCREMENT_BY_MP_INT_CHARACTER_STAT(MP_STAT_MONEY_SPENT_ON_TATTOOS, sTattooData.iCost)
						SET_PACKED_STAT_BOOL(PACKED_MP_BOUGHT_TATTOO,TRUE)
						INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_INGAMESTORE_MONEYSPENT, sTattooData.iCost)
						SET_PLAYER_HAS_PURCHASED_ITEM_IN_SHOP(sData.sShopInfo.eShop, TRUE, PURCHASE_TATTOOS)
					ENDIF
					SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_TATTOO)
					//CHECK_FRANKIE_SAYS_ACHIEVEMENT()
					
					IF SHOULD_TATTOO_BE_FREE_FOR_PLAYER(sTattooData.sLabel)
						SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(GET_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_TATTOO_FROM_LABEL(sTattooData.sLabel))
					ENDIF
					
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					
					RETURN TRUE
				ENDIF
			ELSE
				// Check if we can afford
				IF (GET_TOTAL_CASH(GET_CURRENT_PLAYER_PED_ENUM()) >= sTattooData.iCost)
					IF sTattooData.iCost > 0
						DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), GET_BAAC_FROM_SHOP_ENUM(sData.sShopInfo.eShop), sTattooData.iCost)
					ENDIF
					SET_PLAYER_HAS_JUST_GOT_TATTOO()
					SET_PLAYER_HAS_PURCHASED_ITEM_IN_SHOP(sData.sShopInfo.eShop, TRUE, PURCHASE_TATTOOS)
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
		
	RETURN FALSE
ENDFUNC



PROC BUILD_TATTOO_SHOP_MENU(BOOL &bUpdatePedTattoos)
	
	INT iRow
	INT iItem
	INT iDisplayCost
	TATTOO_NAME_ENUM eTempTattoo
	TATTOO_DATA_STRUCT sTempTattooData
	
	#IF IS_DEBUG_BUILD
		ScriptCatalogItem sCatalogueData
	#ENDIF
	
	IF (sData.sBrowseInfo.iControl = 0)
		CLEAR_MENU_DATA()
		ADD_SHOP_MENU_GRAPHIC_TO_MENU(sData.sShopInfo.eShop)
		//SET_MENU_HELP_CLEAR_SPACE(80.0)
		//SET_MENU_HELP_KEYS_STACKED(TRUE)
		SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT)
		SET_MENU_TITLE("TAT_TITLE_0")
		
		IF NOT sData.sBrowseInfo.bBrowseTrigger
			sData.sBrowseInfo.iCurrentGroup = -1
		ENDIF
		
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			// Torso - Back
			iRow = 0
			IF IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE(PDZ_TORSO, TDS_TORSO_BACK)
				//CPRINTLN(DEBUG_SHOPS, "UNVIEWED TATTOOS FOR TORSO - BACK:", COUNT_UNVIEWED_TATTOOS_FOR_ZONE(PDZ_TORSO, TDS_TORSO_BACK))
				IF NOT HAS_PLAYER_VIEWED_ALL_TATTOO_ITEMS_FOR_ZONE(PDZ_TORSO, TDS_TORSO_BACK)
					ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_0a", 1)	
					ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
					
					CPRINTLN(DEBUG_SHOPS, "<TATT_STAR> menu SP has a star - has not viewed and is unlocked")
				ELIF DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_ZONE(PDZ_TORSO, TDS_TORSO_BACK)
					ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_0a", 1)	
					ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
					
					CPRINTLN(DEBUG_SHOPS, "<TATT_DISC> menu SP has a star - has not viewed and is unlocked")
				ELSE
					ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_0")
				ENDIF
				IF sData.sBrowseInfo.iCurrentGroup = -1
					sData.sBrowseInfo.iCurrentGroup = iRow
				ENDIF
			ENDIF
			
			// Torso - Back Full
			iRow++
			// Torso - Chest
			iRow++
			// Torso - Stomach
			iRow++
		ELSE
			// Torso - Back
			iRow = 0
			IF IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE(PDZ_TORSO, TDS_TORSO_BACK)
				//CPRINTLN(DEBUG_SHOPS, "UNVIEWED TATTOOS FOR TORSO - BACK:", COUNT_UNVIEWED_TATTOOS_FOR_ZONE(PDZ_TORSO, TDS_TORSO_BACK))
				IF NOT HAS_PLAYER_VIEWED_ALL_TATTOO_ITEMS_FOR_ZONE(PDZ_TORSO, TDS_TORSO_BACK)
					ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_0a", 1)	
					ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
					
					CPRINTLN(DEBUG_SHOPS, "<TATT_STAR> menu TDS_TORSO_BACK has a star - has not viewed and is unlocked")
				ELIF DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_ZONE(PDZ_TORSO, TDS_TORSO_BACK)
					ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_0a", 1)	
					ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
					
					CPRINTLN(DEBUG_SHOPS, "<TATT_DISC> menu TDS_TORSO_BACK has a star - has not viewed and is unlocked")
				ELSE
					ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_0a")
				ENDIF
				IF sData.sBrowseInfo.iCurrentGroup = -1
					sData.sBrowseInfo.iCurrentGroup = iRow
				ENDIF
			ENDIF
			
			// Torso - Back Full
			iRow++
			IF IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE(PDZ_TORSO, TDS_TORSO_BACK_FULL)
				//CPRINTLN(DEBUG_SHOPS, "UNVIEWED TATTOOS FOR TORSO - BACK FULL:", COUNT_UNVIEWED_TATTOOS_FOR_ZONE(PDZ_TORSO, TDS_TORSO_BACK_FULL))
				IF NOT HAS_PLAYER_VIEWED_ALL_TATTOO_ITEMS_FOR_ZONE(PDZ_TORSO, TDS_TORSO_BACK_FULL)
					ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_0b", 1)	
					ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
					
					CPRINTLN(DEBUG_SHOPS, "<TATT_STAR> menu TDS_TORSO_BACK_FULL has a star - has not viewed and is unlocked")
				ELIF DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_ZONE(PDZ_TORSO, TDS_TORSO_BACK_FULL)
					ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_0b", 1)	
					ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
					
					CPRINTLN(DEBUG_SHOPS, "<TATT_DISC> menu TDS_TORSO_BACK_FULL has a star - has not viewed and is unlocked")
				ELSE
					ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_0b")
				ENDIF
				IF sData.sBrowseInfo.iCurrentGroup = -1
					sData.sBrowseInfo.iCurrentGroup = iRow
				ENDIF
			ENDIF
			
			// Torso - Chest
			iRow++
			IF IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE(PDZ_TORSO, TDS_TORSO_CHEST)
				//CPRINTLN(DEBUG_SHOPS, "UNVIEWED TATTOOS FOR TORSO - CHEST:", COUNT_UNVIEWED_TATTOOS_FOR_ZONE(PDZ_TORSO, TDS_TORSO_CHEST))
				IF NOT HAS_PLAYER_VIEWED_ALL_TATTOO_ITEMS_FOR_ZONE(PDZ_TORSO, TDS_TORSO_CHEST)
					ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_0c", 1)	
					ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
					
					CPRINTLN(DEBUG_SHOPS, "<TATT_STAR> menu TDS_TORSO_CHEST has a star - has not viewed and is unlocked")
				ELIF DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_ZONE(PDZ_TORSO, TDS_TORSO_CHEST)
					ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_0c", 1)	
					ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
					
					CPRINTLN(DEBUG_SHOPS, "<TATT_DISC> menu TDS_TORSO_CHEST has a star - has not viewed and is unlocked")
				ELSE
					ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_0c")
				ENDIF
				IF sData.sBrowseInfo.iCurrentGroup = -1
					sData.sBrowseInfo.iCurrentGroup = iRow
				ENDIF
			ENDIF
			
			// Torso - Stomach
			iRow++
			IF IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE(PDZ_TORSO, TDS_TORSO_STOMACH)
				//CPRINTLN(DEBUG_SHOPS, "UNVIEWED TATTOOS FOR TORSO - STOMACH:", COUNT_UNVIEWED_TATTOOS_FOR_ZONE(PDZ_TORSO, TDS_TORSO_STOMACH))
				IF NOT HAS_PLAYER_VIEWED_ALL_TATTOO_ITEMS_FOR_ZONE(PDZ_TORSO, TDS_TORSO_STOMACH)
					ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_0d", 1)	
					ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
					
					CPRINTLN(DEBUG_SHOPS, "<TATT_STAR> menu TDS_TORSO_STOMACH has a star - has not viewed and is unlocked")
				ELIF DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_ZONE(PDZ_TORSO, TDS_TORSO_STOMACH)
					ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_0d", 1)	
					ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
					
					CPRINTLN(DEBUG_SHOPS, "<TATT_DISC> menu TDS_TORSO_STOMACH has a star - has not viewed and is unlocked")
				ELSE
					ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_0d")
				ENDIF
				IF sData.sBrowseInfo.iCurrentGroup = -1
					sData.sBrowseInfo.iCurrentGroup = iRow
				ENDIF
			ENDIF
		ENDIF
		
		// Head
		iRow++
		IF IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE(PDZ_HEAD)
			//CPRINTLN(DEBUG_SHOPS, "UNVIEWED TATTOOS FOR HEAD:", COUNT_UNVIEWED_TATTOOS_FOR_ZONE(PDZ_HEAD))
			IF NOT HAS_PLAYER_VIEWED_ALL_TATTOO_ITEMS_FOR_ZONE(PDZ_HEAD)
				ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_1", 1)	
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
				
				CPRINTLN(DEBUG_SHOPS, "<TATT_STAR> menu PDZ_HEAD has a star - has not viewed and is unlocked")
			ELIF DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_ZONE(PDZ_HEAD)
				ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_1", 1)	
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
				
				CPRINTLN(DEBUG_SHOPS, "<TATT_DISC> menu PDZ_HEAD has a star - has not viewed and is unlocked")
			ELSE
				ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_1")
			ENDIF
			IF sData.sBrowseInfo.iCurrentGroup = -1
				sData.sBrowseInfo.iCurrentGroup = iRow
			ENDIF
		ENDIF
		
		// Left Arm
		iRow++
		IF IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE(PDZ_LEFT_ARM)
			//CPRINTLN(DEBUG_SHOPS, "UNVIEWED TATTOOS FOR LEFT ARM:", COUNT_UNVIEWED_TATTOOS_FOR_ZONE(PDZ_LEFT_ARM))
			IF NOT HAS_PLAYER_VIEWED_ALL_TATTOO_ITEMS_FOR_ZONE(PDZ_LEFT_ARM)
				ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_2", 1)	
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
				
				CPRINTLN(DEBUG_SHOPS, "<TATT_STAR> menu PDZ_LEFT_ARM has a star - has not viewed and is unlocked")
			ELIF DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_ZONE(PDZ_LEFT_ARM)
				ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_2", 1)	
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
				
				CPRINTLN(DEBUG_SHOPS, "<TATT_DISC> menu PDZ_LEFT_ARM has a star - has not viewed and is unlocked")
			ELSE
				ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_2")
			ENDIF
			IF sData.sBrowseInfo.iCurrentGroup = -1
				sData.sBrowseInfo.iCurrentGroup = iRow
			ENDIF
		ENDIF
		
		// Right Arm
		iRow++
		IF IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE(PDZ_RIGHT_ARM)
			//CPRINTLN(DEBUG_SHOPS, "UNVIEWED TATTOOS FOR RIGHT ARM:", COUNT_UNVIEWED_TATTOOS_FOR_ZONE(PDZ_RIGHT_ARM))
			IF NOT HAS_PLAYER_VIEWED_ALL_TATTOO_ITEMS_FOR_ZONE(PDZ_RIGHT_ARM)
				ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_3", 1)	
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
				
				CPRINTLN(DEBUG_SHOPS, "<TATT_STAR> menu PDZ_RIGHT_ARM has a star - has not viewed and is unlocked")
			ELIF DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_ZONE(PDZ_RIGHT_ARM)
				ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_3", 1)	
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
				
				CPRINTLN(DEBUG_SHOPS, "<TATT_DISC> menu PDZ_RIGHT_ARM has a star - has not viewed and is unlocked")
			ELSE
				ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_3")
			ENDIF
			IF sData.sBrowseInfo.iCurrentGroup = -1
				sData.sBrowseInfo.iCurrentGroup = iRow
			ENDIF
		ENDIF
		
		// Left Leg
		iRow++
		IF IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE(PDZ_LEFT_LEG)
			//CPRINTLN(DEBUG_SHOPS, "UNVIEWED TATTOOS FOR LEFT LEG:", COUNT_UNVIEWED_TATTOOS_FOR_ZONE(PDZ_LEFT_LEG))
			IF NOT HAS_PLAYER_VIEWED_ALL_TATTOO_ITEMS_FOR_ZONE(PDZ_LEFT_LEG)
				ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_4", 1)	
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
				
				CPRINTLN(DEBUG_SHOPS, "<TATT_STAR> menu PDZ_LEFT_LEG has a star - has not viewed and is unlocked")
			ELIF DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_ZONE(PDZ_LEFT_LEG)
				ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_4", 1)	
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
				
				CPRINTLN(DEBUG_SHOPS, "<TATT_DISC> menu PDZ_LEFT_LEG has a star - has not viewed and is unlocked")
			ELSE
				ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_4")
			ENDIF
			IF sData.sBrowseInfo.iCurrentGroup = -1
				sData.sBrowseInfo.iCurrentGroup = iRow
			ENDIF
		ENDIF
		
		// Right Leg
		iRow++
		IF IS_TATTOO_ITEM_AVAILABLE_FOR_ZONE(PDZ_RIGHT_LEG)
			//CPRINTLN(DEBUG_SHOPS, "UNVIEWED TATTOOS FOR RIGHT LEG:", COUNT_UNVIEWED_TATTOOS_FOR_ZONE(PDZ_RIGHT_LEG))
			IF NOT HAS_PLAYER_VIEWED_ALL_TATTOO_ITEMS_FOR_ZONE(PDZ_RIGHT_LEG)
				ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_5", 1)	
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
				
				CPRINTLN(DEBUG_SHOPS, "<TATT_STAR> menu PDZ_RIGHT_LEG has a star - has not viewed and is unlocked")
			ELIF DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_ZONE(PDZ_RIGHT_LEG)
				ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_5", 1)	
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
				
				CPRINTLN(DEBUG_SHOPS, "<TATT_DISC> menu PDZ_RIGHT_LEG has a star - has not viewed and is unlocked")
			ELSE
				ADD_MENU_ITEM_TEXT(iRow, "TAT_ZONE_5")
			ENDIF
			IF sData.sBrowseInfo.iCurrentGroup = -1
				sData.sBrowseInfo.iCurrentGroup = iRow
			ENDIF
		ENDIF
		
		IF sData.sBrowseInfo.iCurrentGroup = -1
			IF HAS_PLAYER_UNLOCKED_ANY_TATTOOS()
				ADD_MENU_ITEM_TEXT(0, "TAT_NONE_OUT")
			ELSE
				ADD_MENU_ITEM_TEXT(0, "TAT_NONE")
			ENDIF
		ENDIF
		
		SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentGroup)
		
		sData.sBrowseInfo.bBrowseTrigger = FALSE
		
	ELIF (sData.sBrowseInfo.iControl = 1)
		CLEAR_MENU_DATA()
		ADD_SHOP_MENU_GRAPHIC_TO_MENU(sData.sShopInfo.eShop)
		//SET_MENU_HELP_CLEAR_SPACE(80.0)
		//SET_MENU_HELP_KEYS_STACKED(TRUE)
		SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
		SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
		
		IF g_bInMultiplayer
//			IF GET_STAT_CHARACTER_TEAM() = TEAM_CRIM_GANG_2
//				SET_MENU_TITLE("TAT_TITLE_1_1") // Select patch
//			ELSE
				SET_MENU_TITLE("TAT_TITLE_1_0") // Select tattoo
//			ENDIF
		ELSE
			SET_MENU_TITLE("TAT_TITLE_1_0")
		ENDIF
		
		TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
		PED_DECORATION_ZONE eZone = PDZ_INVALID
		TORSO_DECORATION_SUBZONE eSubzone = TDS_INVALID
		GET_PED_DECORATION_ZONE_FROM_INT(sData.sBrowseInfo.iCurrentGroup, eZone, eSubzone)
		
		BOOL bInvalidCatalogueKey = FALSE
		sInvalidCatalogueKey = ""
		BUILD_AVAILABLE_TATTOO_ITEMS(eFaction, sData.iAvailableTattoos, sData.iAvailableDLCTattoos, eZone, eSubzone)
		TEXT_LABEL_15 sCurrentTattooLabel
		TEXT_LABEL_63 tlCatalogueKey
		BOOl bTattooUnlocked
		
		IF GET_FIRST_AVAILABLE_SORTED_ITEM(eTempTattoo, iItem)
		AND GET_TATTOO_DATA(sTempTattooData, eTempTattoo, eFaction, PLAYER_PED_ID())
	
			bInvalidCatalogueKey = FALSE
			iDisplayCost = GET_TATTOO_DISPLAY_PRICE(sTempTattooData.iCost, sTempTattooData.sLabel)
			IF iDisplayCost = -1
				sInvalidCatalogueKey = sTempTattooData.sLabel
				bInvalidCatalogueKey = TRUE
			ELIF NETWORK_IS_GAME_IN_PROGRESS()
			AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
				BOOL bStarterPackItem = FALSE
				IF SHOULD_TATTOO_BE_FREE_FOR_PLAYER(sTempTattooData.sLabel)
					bStarterPackItem = TRUE
				ENDIF
				
				GENERATE_KEY_FOR_SHOP_CATALOGUE(tlCatalogueKey, sTempTattooData.sLabel, GET_ENTITY_MODEL(PLAYER_PED_ID()), SHOP_TYPE_TATTOO, 0, 0, DEFAULT, DEFAULT, DEFAULT, bStarterPackItem)
				IF NOT NET_GAMESERVER_CATALOG_ITEM_IS_VALID(tlCatalogueKey)
					sInvalidCatalogueKey = tlCatalogueKey
					bInvalidCatalogueKey = TRUE
				ENDIF
			ENDIF
			
			bTattooUnlocked = IS_THIS_TATTOO_UNLOCKED(sTempTattooData.eEnum, sTempTattooData.sLabel)
			
			IF bTattooUnlocked
			AND NOT HAS_PLAYER_PED_VIEWED_TATTOO(sTempTattooData.eEnum)
				ADD_MENU_ITEM_TEXT(iItem, sTempTattooData.sLabel, 1)	
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
				
				CPRINTLN(DEBUG_SHOPS, "<TATT_STAR> menu 2 \"", GET_STRING_FROM_TEXT_FILE(sTempTattooData.sLabel), "\" has a star - has not viewed and is unlocked")
			ELIF bTattooUnlocked
			AND SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO(sTempTattooData.eEnum, sTempTattooData.sLabel, eZone, eSubzone)
				ADD_MENU_ITEM_TEXT(iItem, sTempTattooData.sLabel, 1)	
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
				
				CPRINTLN(DEBUG_SHOPS, "<TATT_DISC> menu 2 \"", GET_STRING_FROM_TEXT_FILE(sTempTattooData.sLabel), "\" has a discount and is unlocked")
			ELSE
				ADD_MENU_ITEM_TEXT(iItem, sTempTattooData.sLabel)
			ENDIF
			
			IF NOT bTattooUnlocked
			OR bInvalidCatalogueKey
				ADD_MENU_ITEM_TEXT(iItem, "", 1)
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LOCK)
				
				CPRINTLN(DEBUG_SHOPS, "<TATT_LOCK> menu 2 \"", GET_STRING_FROM_TEXT_FILE(sTempTattooData.sLabel), "\" to be locked, rank unlock is ", GET_MP_TATTOO_UNLOCK_RANK(sTempTattooData.eEnum),
						" - IS_THIS_TATTOO_UNLOCKED: ", GET_STRING_FROM_BOOL(bTattooUnlocked),
						", sInvalidCatalogueKey: \"", sInvalidCatalogueKey, "\"")
				
			ELIF HAS_PLAYER_PED_GOT_TATTOO(sTempTattooData.eEnum)
				ADD_MENU_ITEM_TEXT(iItem, "", 1)
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_INK)
			
			ELIF NETWORK_IS_GAME_IN_PROGRESS()
			AND SHOULD_TATTOO_BE_FREE_FOR_PLAYER(sTempTattooData.sLabel)
				ADD_MENU_ITEM_TEXT(iItem, "ITEM_FREE")
						
				CPRINTLN(DEBUG_SHOPS, "<TATT_PURCH> menu 2 \"", GET_STRING_FROM_TEXT_FILE(sTempTattooData.sLabel), "\" should be free for player, tattoo enum is ", sTempTattooData.eEnum)
				
			ELIF NETWORK_IS_GAME_IN_PROGRESS()
			AND IS_MP_TATTOO_PURCHASED(sTempTattooData.eEnum)
				//ADD_MENU_ITEM_TEXT(iItem, "", 1)
				//ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_TICK)
				//#1831283
				ADD_MENU_ITEM_TEXT(iItem, "ITEM_COST", 1)
				ADD_MENU_ITEM_TEXT_COMPONENT_INT(g_sMPTunables.iPURCHASED_TATTOO_APPLICATION_FEE )
						
				CPRINTLN(DEBUG_SHOPS, "<TATT_PURCH> menu 2 \"", GET_STRING_FROM_TEXT_FILE(sTempTattooData.sLabel), "\" has been purchased, tattoo enum is ", sTempTattooData.eEnum)
				
			ELSE
				IF iDisplayCost > 0
					ADD_MENU_ITEM_TEXT(iItem, "ITEM_COST", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_INT(iDisplayCost)
				ELSE
					ADD_MENU_ITEM_TEXT(iItem, "ITEM_FREE")
				ENDIF
			ENDIF
			
			// Update selected item label for DLC description
			IF eTempTattoo = sData.eCurrentTattoo
				sCurrentTattooLabel = sTempTattooData.sLabel
			ENDIF
			
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			///    	 
			///    	 CATALOGUE SETUP
			///    	 
			#IF IS_DEBUG_BUILD
				IF (g_bAddMenuItemsToCatalogue OR g_bVerifyMenuItemPrices)
				AND NETWORK_IS_GAME_IN_PROGRESS()
					BOOL bStarterPackItem = FALSE
					IF SHOULD_TATTOO_BE_FREE_FOR_PLAYER(sTempTattooData.sLabel)
						bStarterPackItem = TRUE
					ENDIF
					
					GENERATE_KEY_FOR_SHOP_CATALOGUE(sCatalogueData.m_key, sTempTattooData.sLabel, GET_ENTITY_MODEL(PLAYER_PED_ID()), SHOP_TYPE_TATTOO, 0, 0, DEFAULT, DEFAULT, DEFAULT, bStarterPackItem)
					sCatalogueData.m_textTag = sTempTattooData.sLabel
					sCatalogueData.m_name = GET_STRING_FROM_TEXT_FILE(sCatalogueData.m_textTag)
					sCatalogueData.m_category = CATEGORY_TATTOO
					sCatalogueData.m_price = iDisplayCost
					sCatalogueData.m_storagetype = NET_SHOP_ISTORAGE_BITFIELD
					sCatalogueData.m_bitsize = 1
					sCatalogueData.m_stathash = 0
					sCatalogueData.m_stathash = ENUM_TO_INT(GET_BOOL_PACKED_STAT_KEY(GET_TATTOO_PURCHASED_PACKED_STAT_ENUM(eTempTattoo), g_iPedComponentSlot))
					sCatalogueData.m_bitshift = GET_BOOL_PACKED_STAT_BITSHIFT(GET_TATTOO_PURCHASED_PACKED_STAT_ENUM(eTempTattoo))
					
					IF sCatalogueData.m_stathash != 0
						ADD_ITEM_TO_CATALOG_WITH_CHECK(sCatalogueData)
					ENDIF
				ENDIF
			#ENDIF
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
			
			
			WHILE GET_NEXT_AVAILABLE_SORTED_ITEM(eTempTattoo, eTempTattoo, iItem, FALSE)
				IF GET_TATTOO_DATA(sTempTattooData, eTempTattoo, eFaction, PLAYER_PED_ID())
					bInvalidCatalogueKey = FALSE
					iDisplayCost = GET_TATTOO_DISPLAY_PRICE(sTempTattooData.iCost, sTempTattooData.sLabel)
					
					IF iDisplayCost = -1
						sInvalidCatalogueKey = sTempTattooData.sLabel
						bInvalidCatalogueKey = TRUE
					ELIF NETWORK_IS_GAME_IN_PROGRESS()
					AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
						BOOL bStarterPackItem = FALSE
						IF SHOULD_TATTOO_BE_FREE_FOR_PLAYER(sTempTattooData.sLabel)
							bStarterPackItem = TRUE
						ENDIF
						
						GENERATE_KEY_FOR_SHOP_CATALOGUE(tlCatalogueKey, sTempTattooData.sLabel, GET_ENTITY_MODEL(PLAYER_PED_ID()), SHOP_TYPE_TATTOO, 0, 0, DEFAULT, DEFAULT, DEFAULT, bStarterPackItem)
						IF NOT NET_GAMESERVER_CATALOG_ITEM_IS_VALID(tlCatalogueKey)
							sInvalidCatalogueKey = tlCatalogueKey
							bInvalidCatalogueKey = TRUE
						ENDIF
					ENDIF
					
					bTattooUnlocked = IS_THIS_TATTOO_UNLOCKED(sTempTattooData.eEnum, sTempTattooData.sLabel)
					
					IF bTattooUnlocked
					AND NOT HAS_PLAYER_PED_VIEWED_TATTOO(sTempTattooData.eEnum)
						ADD_MENU_ITEM_TEXT(iItem, sTempTattooData.sLabel, 1)	
						ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
						
						CPRINTLN(DEBUG_SHOPS, "<TATT_STAR> menu 1 \"", GET_STRING_FROM_TEXT_FILE(sTempTattooData.sLabel), "\" has a star - has not viewed and is unlocked")
					ELIF bTattooUnlocked
					AND SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO(sTempTattooData.eEnum, sTempTattooData.sLabel, eZone, eSubzone)
						ADD_MENU_ITEM_TEXT(iItem, sTempTattooData.sLabel, 1)	
						ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
						
						CPRINTLN(DEBUG_SHOPS, "<TATT_DISC> menu 1 \"", GET_STRING_FROM_TEXT_FILE(sTempTattooData.sLabel), "\" has a discount and is unlocked")
					ELSE
						ADD_MENU_ITEM_TEXT(iItem, sTempTattooData.sLabel)
					ENDIF
					
					IF NOT bTattooUnlocked
					OR bInvalidCatalogueKey
						ADD_MENU_ITEM_TEXT(iItem, "", 1)
						ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LOCK)
						
						CPRINTLN(DEBUG_SHOPS, "<TATT_LOCK> menu 1 \"", GET_STRING_FROM_TEXT_FILE(sTempTattooData.sLabel), "\" to be locked, rank unlock is ", GET_MP_TATTOO_UNLOCK_RANK(sTempTattooData.eEnum), " - IS_THIS_TATTOO_UNLOCKED: ", GET_STRING_FROM_BOOL(bTattooUnlocked), ", sInvalidCatalogueKey: \"", sInvalidCatalogueKey, "\"")
						
					ELIF HAS_PLAYER_PED_GOT_TATTOO(sTempTattooData.eEnum)
						ADD_MENU_ITEM_TEXT(iItem, "", 1)
						ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_INK)
					ELIF NETWORK_IS_GAME_IN_PROGRESS()
					AND SHOULD_TATTOO_BE_FREE_FOR_PLAYER(sTempTattooData.sLabel)
						ADD_MENU_ITEM_TEXT(iItem, "ITEM_FREE")
						
						CPRINTLN(DEBUG_SHOPS, "<TATT_PURCH> menu 1 \"", GET_STRING_FROM_TEXT_FILE(sTempTattooData.sLabel), "\" should be free for player, tattoo enum is ", sTempTattooData.eEnum)
						
					ELIF NETWORK_IS_GAME_IN_PROGRESS()
					AND IS_MP_TATTOO_PURCHASED(sTempTattooData.eEnum)
						//ADD_MENU_ITEM_TEXT(iItem, "", 1)
						//ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_TICK)
						//#1831283
						ADD_MENU_ITEM_TEXT(iItem, "ITEM_COST", 1)
						ADD_MENU_ITEM_TEXT_COMPONENT_INT(g_sMPTunables.iPURCHASED_TATTOO_APPLICATION_FEE )
						
						CPRINTLN(DEBUG_SHOPS, "<TATT_PURCH> menu 1 \"", GET_STRING_FROM_TEXT_FILE(sTempTattooData.sLabel), "\" has been purchased, tattoo enum is ", sTempTattooData.eEnum)
						
					ELSE
						IF iDisplayCost > 0
							ADD_MENU_ITEM_TEXT(iItem, "ITEM_COST", 1)
							ADD_MENU_ITEM_TEXT_COMPONENT_INT(iDisplayCost)
						ELSE
							ADD_MENU_ITEM_TEXT(iItem, "ITEM_FREE")
						ENDIF
					ENDIF
					
					// Update selected item label for DLC description
					IF eTempTattoo = sData.eCurrentTattoo
						sCurrentTattooLabel = sTempTattooData.sLabel
					ENDIF
					
					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					///    	 
					///    	 CATALOGUE SETUP
					///    	 
					#IF IS_DEBUG_BUILD
						IF (g_bAddMenuItemsToCatalogue OR g_bVerifyMenuItemPrices)
						AND NETWORK_IS_GAME_IN_PROGRESS()
							BOOL bStarterPackItem = FALSE
							IF SHOULD_TATTOO_BE_FREE_FOR_PLAYER(sTempTattooData.sLabel)
								bStarterPackItem = TRUE
							ENDIF
							
							GENERATE_KEY_FOR_SHOP_CATALOGUE(sCatalogueData.m_key, sTempTattooData.sLabel, GET_ENTITY_MODEL(PLAYER_PED_ID()), SHOP_TYPE_TATTOO, 0, 0, DEFAULT, DEFAULT, DEFAULT, bStarterPackItem)
							sCatalogueData.m_textTag = sTempTattooData.sLabel
							sCatalogueData.m_name = GET_STRING_FROM_TEXT_FILE(sCatalogueData.m_textTag)
							sCatalogueData.m_category = CATEGORY_TATTOO
							sCatalogueData.m_price = iDisplayCost
							sCatalogueData.m_storagetype = NET_SHOP_ISTORAGE_BITFIELD
							sCatalogueData.m_bitsize = 1
							sCatalogueData.m_stathash = 0
							sCatalogueData.m_stathash = ENUM_TO_INT(GET_BOOL_PACKED_STAT_KEY(GET_TATTOO_PURCHASED_PACKED_STAT_ENUM(eTempTattoo), g_iPedComponentSlot))
							sCatalogueData.m_bitshift = GET_BOOL_PACKED_STAT_BITSHIFT(GET_TATTOO_PURCHASED_PACKED_STAT_ENUM(eTempTattoo))
							
							IF sCatalogueData.m_stathash != 0
								ADD_ITEM_TO_CATALOG_WITH_CHECK(sCatalogueData)
							ENDIF
						ENDIF
					#ENDIF
					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				ENDIF
			ENDWHILE
		ELSE
			IF HAS_PLAYER_UNLOCKED_ANY_TATTOOS()
				ADD_MENU_ITEM_TEXT(0, "TAT_NONE_OUT")
			ELSE
				ADD_MENU_ITEM_TEXT(0, "TAT_NONE")
			ENDIF
		ENDIF
		
		// Restore menu item
		IF sData.sBrowseInfo.bBrowseTrigger
			SET_TOP_MENU_ITEM(sData.sBrowseInfo.iCurrentSubItem)
			SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem, FALSE)
		ELSE
			GET_FIRST_AVAILABLE_SORTED_ITEM(sData.eCurrentTattoo, sData.sBrowseInfo.iCurrentItem)
			SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem, TRUE)
		ENDIF
		sData.sBrowseInfo.iCurrentSubItem = GET_TOP_MENU_ITEM()
		sData.sBrowseInfo.bBrowseTrigger = FALSE
		
//		// If the cutscene ped exists, set the items on them, otherwise set it on the player
//		PED_INDEX pedID_ShopPed = PLAYER_PED_ID()
//		IF DOES_ENTITY_EXIST(sCutsceneData.pedID)
//		AND NOT IS_PED_INJURED(sCutsceneData.pedID)
//			pedID_ShopPed = sCutsceneData.pedID
//		ENDIF
//		
//		PREPARE_PLAYER_FOR_TATTOO(true, null)
//		REMOVE_TATTOOS_NOT_PURCHASED(pedID_ShopPed)
//		GIVE_PED_TEMP_TATTOO(pedID_ShopPed, sData.eCurrentTattoo)
		bUpdatePedTattoos = TRUE
		
		eBackTat = INT_TO_ENUM(TATTOO_FACING, -1)
		eStored_eFace = INT_TO_ENUM(TATTOO_FACING, -1)
		
		IF GET_HASH_KEY(sData.sBrowseInfo.sInfoLabel) != 0
			IF NOT IS_THIS_LABEL_A_CREW_TATTOO(sCurrentTattooLabel)
				
				TEXT_LABEL_15 tlCustomLabel
				IF GET_SPECIAL_LABEL_DESCRIPTION_FOR_SHOP_ITEM(sCurrentTattooLabel, tlCustomLabel)
					sData.sBrowseInfo.sInfoLabel = tlCustomLabel
					SET_CURRENT_MENU_ITEM_DESCRIPTION(sData.sBrowseInfo.sInfoLabel, 4000)
				ELSE
					IF ARE_STRINGS_EQUAL(sData.sBrowseInfo.sInfoLabel, "TAT_REMDLC")
					AND IS_STRING_NULL_OR_EMPTY(GET_SHOP_CONTENT_FOR_MENU(sCurrentTattooLabel))
						sData.sBrowseInfo.sInfoLabel = "TAT_REM"
					ENDIF
					
					SET_CURRENT_MENU_ITEM_DESCRIPTION(sData.sBrowseInfo.sInfoLabel, 4000)
					IF bDLCLabel
					AND NOT IS_STRING_NULL_OR_EMPTY(GET_SHOP_CONTENT_FOR_MENU(sCurrentTattooLabel))
						ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sCurrentTattooLabel))
					ENDIF
				ENDIF
			ELSE
				SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_LCKCREW", 4000)
			ENDIF
			sData.sBrowseInfo.sInfoLabel = ""
			bDLCLabel = FALSE
		ENDIF
	ENDIF
	
	bUpdateHelpKeys = TRUE
ENDPROC

#IF IS_DEBUG_BUILD
FUNC STRING GET_STRING_FROM_TATTOO_FACING(TATTOO_FACING eFacing)
	SWITCH eFacing
		CASE TATTOO_FRONT		RETURN "FRONT" BREAK
		CASE TATTOO_BACK		RETURN "BACK" BREAK
		CASE TATTOO_LEFT		RETURN "LEFT" BREAK
		CASE TATTOO_RIGHT		RETURN "RIGHT" BREAK
		CASE TATTOO_FRONT_LEFT	RETURN "FRONT_LEFT" BREAK
		CASE TATTOO_BACK_LEFT	RETURN "BACK_LEFT" BREAK
		CASE TATTOO_FRONT_RIGHT	RETURN "FRONT_RIGHT" BREAK
		CASE TATTOO_BACK_RIGHT	RETURN "BACK_RIGHT" BREAK
	ENDSWITCH
	
	IF eFacing = INT_TO_ENUM(TATTOO_FACING, -1)
		RETURN "null"
	ENDIF
	
	TEXT_LABEL_63 str = "UNKNOWN["
	str += ENUM_TO_INT(eFacing)
	str += "]"
	RETURN GET_STRING_FROM_STRING(str, 0, GET_LENGTH_OF_LITERAL_STRING(str))
ENDFUNC
#ENDIF

FUNC FLOAT COSINE_INTERP_TATTOO_FLOAT(FLOAT fVal1, FLOAT fVal2, FLOAT fAlpha)
	IF fVal1 > 360
		fVal1 -= 360
	ELIF fVal1 < 0
		fVal1 += 360
	ENDIF
	
	IF fVal2 > (fVal1+180.0)
		fVal2 -= 360
	ELIF fVal2 < (fVal1-180.0)
		fVal2 += 360
	ENDIF
	
	RETURN COSINE_INTERP_FLOAT(fVal1, fVal2, fAlpha)
ENDFUNC

/// PURPOSE: Keeps the browse camera pointing at the correct area
PROC UPDATE_TATTOO_BROWSE_CAM()

	// Disable camera if pause menu is active - needed for freemode.
	BOOL bAllowBrowsing  = (NOT IS_PAUSE_MENU_ACTIVE() AND sData.sBrowseInfo.eStage != SHOP_BROWSE_STAGE_OUTRO AND NOT IS_SYSTEM_UI_BEING_DISPLAYED() AND NOT IS_WARNING_MESSAGE_ACTIVE() AND NOT g_sShopSettings.bProcessStoreAlert)
	
	IF NOT bAllowBrowsing
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF NOT bDrawDebugTattooText
		IF GET_DRAW_DEBUG_COMMANDLINE_PARAM_EXISTS()
			CPRINTLN(DEBUG_FAMILY, "...tattoo_shop.sc has activated g_bDrawDebugFamilyStuff = TRUE (command line param)")
			g_bDrawDebugFamilyStuff = TRUE
		ENDIF
		IF g_bDrawDebugFamilyStuff
			bDrawDebugTattooText = TRUE
		ENDIF
	ENDIF
	#ENDIF
	
	VECTOR vBrowseCamOffset = <<-0.1,1.3129,0.5072>>//<<-0.4588,1.6264,0.5530>>  //-0.5767
	
	#IF FEATURE_TUNER
	IF (sData.sShopInfo.eShop = TATTOO_PARLOUR_07_CCT)
		vBrowseCamOffset = <<-0.1,1.4,0.506>>
	ENDIF
	#ENDIF
	
	PED_DECORATION_ZONE eCurrentZone = PDZ_INVALID
	TORSO_DECORATION_SUBZONE eCurrentSubzone = TDS_INVALID
	GET_PED_DECORATION_ZONE_FROM_INT(sData.sBrowseInfo.iCurrentGroup, eCurrentZone, eCurrentSubzone)
	
	SWITCH eCurrentZone
		CASE PDZ_LEFT_LEG	vBrowseCamOffset += <<0,0,-0.8058>> BREAK  //<<0,0,-0.8058>>
		CASE PDZ_RIGHT_LEG	vBrowseCamOffset += <<0,0,-0.8058>> BREAK
	ENDSWITCH
	
	VECTOR vBrowseCamLookAt = vStoredBrowseCamLookAt
	
	SWITCH eCurrentZone
		CASE PDZ_LEFT_LEG	vBrowseCamLookAt += <<0,0,-0.8058>> BREAK
		CASE PDZ_RIGHT_LEG	vBrowseCamLookAt += <<0,0,-0.8058>> BREAK
	ENDSWITCH
	
	CONST_FLOAT fHEADING_FRONT		00.0
	CONST_FLOAT fHEADING_BACK		190.0
	CONST_FLOAT fHEADING_LEFT		310.0		//(190.0+90)+30.0
	CONST_FLOAT fHEADING_RIGHT		70.0		//(190.0-90)-30.0

	////////////////////////////////////////////////////////////////
	///     CREATE
	IF NOT DOES_CAM_EXIST(sData.sShopInfo.sCameras[0].camID)
		sData.sShopInfo.sCameras[0].camID = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	ENDIF
	IF NOT IS_CAM_ACTIVE(sData.sShopInfo.sCameras[0].camID)
	
		sData.sShopInfo.sCameras[0].fFOV = 52.0//40.0
		
		fCamZoomAlpha = 0.0
		fCamRotAlpha = 0.0
	
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			sData.sShopInfo.sCameras[0].vRot.z = (GET_ENTITY_HEADING(PLAYER_PED_ID()) - fHEADING_FRONT)
		ENDIF
		
		IF DOES_ENTITY_EXIST(sCutsceneData.pedID)
			IF NOT IS_PED_INJURED(sCutsceneData.pedID)
				sData.sShopInfo.sCameras[0].vRot.z = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_FRONT)
			ENDIF
		ENDIF
		WHILE sData.sShopInfo.sCameras[0].vRot.z < -180.0
			sData.sShopInfo.sCameras[0].vRot.z += 360.0
		ENDWHILE
		WHILE sData.sShopInfo.sCameras[0].vRot.z > 180.0
			sData.sShopInfo.sCameras[0].vRot.z -= 360.0
		ENDWHILE
		
		fInitFrontZ = sData.sShopInfo.sCameras[0].vRot.z
		
		sData.sShopInfo.sCameras[0].vPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStoredPlayerCoords, sData.sShopInfo.sCameras[0].vRot.z, vBrowseCamOffset)
		sData.sShopInfo.sCameras[0].bSetup = FALSE
		vStoredBrowseCamLookAt = (vStoredPlayerCoords + <<0,0,0.250>>)
		vCurrentBrowseCamLookAt = vStoredBrowseCamLookAt
	ELSE
		vCurrentBrowseCamLookAt += ((vBrowseCamLookAt - vCurrentBrowseCamLookAt) * 0.25)
	ENDIF

	////////////////////////////////////////////////////////////////
	///     ROTATE
	IF fCamRotAlpha = 0.0
	OR fCamRotAlpha = 1.0	
		INT iLeftX, iLeftY, iRightX, iRightY
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS( iLeftX, iLeftY, iRightX, iRightY)
		
		// Add a deadlock but keep values from 0.
		IF ((iRightX  < 32) AND (iRightX > -32))
		AND ((iRightY  < 32) AND (iRightY > -32))
		
			IF ((iRightX  < 32) AND (iRightX > -32))
				iRightX = 0
			ELSE
				IF (iRightX  < 0)
					iRightX -= 32
				ELSE
					iRightX += 32
				ENDIF
			ENDIF
			
			IF ((iRightY  < 32) AND (iRightY > -32))
				iRightY = 0
			ELSE
				IF (iRightY  < 0)
					iRightY -= 32
				ELSE
					iRightY += 32
				ENDIF
			ENDIF
		ENDIF
		iRightX *= -1
		IF IS_LOOK_INVERTED()
			iRightY	*= -1
		ENDIF

		// ROTATION
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			IF HANDLE_MENU_CURSOR()
				sData.sShopInfo.sCameras[0].vRot.z -= g_fMenuCursorXMoveDistance * 400
			ENDIF
		ELSE
			sData.sShopInfo.sCameras[0].vRot.z += (iRightX * 0.05)
		ENDIF
		
		
	ENDIF

	WHILE sData.sShopInfo.sCameras[0].vRot.z < -180.0
		sData.sShopInfo.sCameras[0].vRot.z += 360.0
	ENDWHILE
	WHILE sData.sShopInfo.sCameras[0].vRot.z > 180.0
		sData.sShopInfo.sCameras[0].vRot.z -= 360.0
	ENDWHILE
	WHILE fStoredFrontZ < -180.0
		fStoredFrontZ += 360.0
	ENDWHILE
	WHILE fStoredFrontZ > 180.0
		fStoredFrontZ -= 360.0
	ENDWHILE
	
	TATTOO_DATA_STRUCT sTattooData
	TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
	IF eCurrentZone = PDZ_TORSO
	OR eCurrentZone = PDZ_LEFT_ARM
	OR eCurrentZone = PDZ_RIGHT_ARM
	OR NETWORK_IS_GAME_IN_PROGRESS()
		IF GET_TATTOO_DATA(sTattooData, sData.eCurrentTattoo, eFaction, PLAYER_PED_ID())
			
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				IF NOT bBackTat
					IF sTattooData.eFace = TATTOO_BACK	
						IF fCamRotAlpha = 0
							fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
							IF NOT NETWORK_IS_GAME_IN_PROGRESS()
								fStoredBackZ = (GET_ENTITY_HEADING(PLAYER_PED_ID()) - 190.0)
							ELSE
								IF DOES_ENTITY_EXIST(sCutsceneData.pedID)
									IF NOT IS_PED_INJURED(sCutsceneData.pedID)	
										fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - 190.0)
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						fCamRotAlpha += 0.05	
						IF fCamRotAlpha >= 1
							fCamRotAlpha = 1
							bBackTat = TRUE
						ELSE
							sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
						ENDIF
					ELSE
						IF fCamRotAlpha <> 0
						AND fCamRotAlpha <> 1
							fCamRotAlpha = 1
							bBackTat = TRUE
						ENDIF
					ENDIF
				ELSE
					IF sTattooData.eFace <> TATTOO_BACK	
						IF fCamRotAlpha = 1 
							fStoredFrontZ = fInitFrontZ
							fStoredBackZ = sData.sShopInfo.sCameras[0].vRot.z
						ENDIF
						fCamRotAlpha -= 0.05	
						IF fCamRotAlpha <= 0
							fCamRotAlpha = 0
							bBackTat = FALSE
						ELSE
							sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
						ENDIF
					ELSE
						IF fCamRotAlpha <> 0
						AND fCamRotAlpha <> 1	
							fCamRotAlpha = 0
							bBackTat = FALSE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF eFaction = TATTOO_MP_FM_F
					IF sTattooData.eFace = TATTOO_FRONT_LEFT
						#IF IS_DEBUG_BUILD
						IF bDrawDebugTattooText
							CDEBUG2LN(DEBUG_SHOPS, "TATTOO_MP_FM_F ", sTattooData.sLabel, " (\"", GET_STRING_FROM_TEXT_FILE(sTattooData.sLabel), "\") forced from ", GET_STRING_FROM_TATTOO_FACING(sTattooData.eFace), " to ", GET_STRING_FROM_TATTOO_FACING(TATTOO_LEFT))
						ENDIF
						#ENDIF

						sTattooData.eFace = TATTOO_LEFT
					ENDIF
					IF sTattooData.eFace = TATTOO_FRONT_RIGHT
						#IF IS_DEBUG_BUILD
						IF bDrawDebugTattooText
							CDEBUG2LN(DEBUG_SHOPS, "TATTOO_MP_FM_F ", sTattooData.sLabel, " (\"", GET_STRING_FROM_TEXT_FILE(sTattooData.sLabel), "\") forced from ", GET_STRING_FROM_TATTOO_FACING(sTattooData.eFace), " to ", GET_STRING_FROM_TATTOO_FACING(TATTOO_RIGHT))
						ENDIF
						#ENDIF

						sTattooData.eFace = TATTOO_RIGHT
					ENDIF
					
					TATTOO_FACING eTemp_eFace
					eTemp_eFace = sTattooData.eFace
					
					SWITCH GET_HASH_KEY(sTattooData.sLabel)
						//Female - left leg
						CASE HASH("TAT_BUS_F_006")	eTemp_eFace = TATTOO_BACK BREAK //"Single"	
						CASE HASH("TAT_FM_017")		eTemp_eFace = TATTOO_BACK BREAK //"Dragon and Dagger"
						CASE HASH("TAT_FM_222")		eTemp_eFace = TATTOO_BACK BREAK //"Serpent Skull"
						CASE HASH("TAT_FM_227")		eTemp_eFace = TATTOO_BACK BREAK //"Smoking Dagger"
						CASE HASH("TAT_FM_234")		eTemp_eFace = TATTOO_BACK BREAK //"Chinese Dragon"
						CASE HASH("TAT_FM_236")		eTemp_eFace = TATTOO_BACK BREAK //"Dragon"
						CASE HASH("TAT_FM_238")		eTemp_eFace = TATTOO_BACK BREAK //"Grim Reaper"
						CASE HASH("TAT_HP_009")		eTemp_eFace = TATTOO_BACK BREAK //"Squares"
						CASE HASH("TAT_HP_019")		eTemp_eFace = TATTOO_BACK BREAK //"Charm"
						CASE HASH("TAT_HP_040")		eTemp_eFace = TATTOO_BACK BREAK //"Black Anchor"
						//Female - right leg
						CASE HASH("TAT_BUS_F_010")	eTemp_eFace = TATTOO_BACK BREAK //"Diamond Crown"	
						CASE HASH("TAT_FM_014")		eTemp_eFace = TATTOO_BACK BREAK //"Skull and Sword"
						CASE HASH("TAT_FM_223")		eTemp_eFace = TATTOO_BACK BREAK //"Fiery Dragon"
						CASE HASH("TAT_FM_240")		eTemp_eFace = TATTOO_BACK BREAK //"Broken Skull"
						CASE HASH("TAT_FM_241")		eTemp_eFace = TATTOO_FRONT BREAK //"Flaming Skull"
						CASE HASH("TAT_FM_243")		eTemp_eFace = TATTOO_LEFT BREAK //"Flaming Scorpion"
						CASE HASH("TAT_FM_244")		eTemp_eFace = TATTOO_BACK BREAK //"Indian Ram"
						CASE HASH("TAT_HP_038")		eTemp_eFace = TATTOO_RIGHT BREAK //"Grub"
						CASE HASH("TAT_HP_042")		eTemp_eFace = TATTOO_BACK BREAK //"Sparkplug"
						//Female - Torso
						CASE HASH("TAT_BB_006")		eTemp_eFace = TATTOO_RIGHT BREAK //"Love Dagger"
						CASE HASH("TAT_FM_235")		eTemp_eFace = TATTOO_FRONT BREAK //"Flaming Shamrock"
						CASE HASH("TAT_HP_006")		eTemp_eFace = TATTOO_RIGHT BREAK //"Feather Birds"
						CASE HASH("TAT_HP_035")		eTemp_eFace = TATTOO_LEFT BREAK //"Sewn Heart"
						//Female - Head
						CASE HASH("TAT_BUS_F_008")	eTemp_eFace = TATTOO_LEFT BREAK //"Money Rose"
						CASE HASH("TAT_BUS_F_007")	eTemp_eFace = TATTOO_RIGHT BREAK //"Val-de-Grace Logo"
						//Female - Left Arm
						CASE HASH("TAT_HP_003")		eTemp_eFace = TATTOO_BACK BREAK //"Diamond Sparkle"
						CASE HASH("TAT_HP_026")		eTemp_eFace = TATTOO_LEFT BREAK //"Pizza"
						CASE HASH("TAT_HP_028")		eTemp_eFace = TATTOO_BACK BREAK //"Thorny Rose"
//	//	//	//	//	//	Padlock.
						CASE HASH("TAT_HP_016")		eTemp_eFace = TATTOO_FRONT BREAK //"Lightning Bolt"
						CASE HASH("TAT_HP_015")		eTemp_eFace = TATTOO_FRONT BREAK //"Mustache"
						//Female - Right Arm
						CASE HASH("TAT_FM_229")		eTemp_eFace = TATTOO_RIGHT BREAK //"Mermaid"
						CASE HASH("TAT_FM_247")		eTemp_eFace = TATTOO_RIGHT BREAK //"Lion"
						CASE HASH("TAT_HP_001")		eTemp_eFace = TATTOO_BACK BREAK //"Single Arrow"
						CASE HASH("TAT_HP_004")		eTemp_eFace = TATTOO_FRONT BREAK //"Bone"
						CASE HASH("TAT_HP_014")		eTemp_eFace = TATTOO_FRONT BREAK //"Spray Can"
						CASE HASH("TAT_HP_017")		eTemp_eFace = TATTOO_BACK BREAK //"Eye Triangle"
						CASE HASH("TAT_HP_020")		eTemp_eFace = TATTOO_BACK BREAK //"Geo Pattern"
//	//	//	//	//	//	Origami.
						
						#IF IS_DEBUG_BUILD
						DEFAULT
							IF bDrawDebugTattooText
								CDEBUG2LN(DEBUG_SHOPS, "CASE HASH(\"", sTattooData.sLabel, "\")		eTemp_eFace = TATTOO_UNKNOWN BREAK //\"", GET_STRING_FROM_TEXT_FILE(sTattooData.sLabel), "\"")
							ENDIF
						BREAK
						#ENDIF
					ENDSWITCH
					
					IF eTemp_eFace != sTattooData.eFace
						#IF IS_DEBUG_BUILD
						IF bDrawDebugTattooText
							CDEBUG2LN(DEBUG_SHOPS, "TATTOO_MP_FM_F ", sTattooData.sLabel, " (\"", GET_STRING_FROM_TEXT_FILE(sTattooData.sLabel), "\") changed from ", GET_STRING_FROM_TATTOO_FACING(sTattooData.eFace), " to ", GET_STRING_FROM_TATTOO_FACING(eTemp_eFace))
						ENDIF
						#ENDIF
						
						sTattooData.eFace = eTemp_eFace
					ENDIF
				ELIF eFaction = TATTOO_MP_FM
					IF sTattooData.eFace = TATTOO_FRONT_LEFT
					OR sTattooData.eFace = TATTOO_BACK_LEFT
						#IF IS_DEBUG_BUILD
						IF bDrawDebugTattooText
							CDEBUG2LN(DEBUG_SHOPS, "TATTOO_MP_FM ", sTattooData.sLabel, " (\"", GET_STRING_FROM_TEXT_FILE(sTattooData.sLabel), "\") forced from ", GET_STRING_FROM_TATTOO_FACING(sTattooData.eFace), " to ", GET_STRING_FROM_TATTOO_FACING(TATTOO_LEFT))
						ENDIF
						#ENDIF

						sTattooData.eFace = TATTOO_LEFT
					ENDIF
					IF sTattooData.eFace = TATTOO_FRONT_RIGHT
					OR sTattooData.eFace = TATTOO_BACK_RIGHT
						#IF IS_DEBUG_BUILD
						IF bDrawDebugTattooText
							CDEBUG2LN(DEBUG_SHOPS, "TATTOO_MP_FM ", sTattooData.sLabel, " (\"", GET_STRING_FROM_TEXT_FILE(sTattooData.sLabel), "\") forced from ", GET_STRING_FROM_TATTOO_FACING(sTattooData.eFace), " to ", GET_STRING_FROM_TATTOO_FACING(TATTOO_FRONT_RIGHT))
						ENDIF
						#ENDIF

						sTattooData.eFace = TATTOO_RIGHT
					ENDIF
					
					TATTOO_FACING eTemp_eFace
					eTemp_eFace = sTattooData.eFace
					
					SWITCH GET_HASH_KEY(sTattooData.sLabel)
						//Male - left leg
						CASE HASH("TAT_BB_027")		eTemp_eFace = TATTOO_BACK BREAK //"Tribal Star"
						CASE HASH("TAT_FM_017")		eTemp_eFace = TATTOO_BACK BREAK //"Dragon and Dagger"
						CASE HASH("TAT_FM_209")		eTemp_eFace = TATTOO_BACK BREAK //"Melting Skull"
						CASE HASH("TAT_FM_222")		eTemp_eFace = TATTOO_BACK BREAK //"Serpent Skull"
						CASE HASH("TAT_FM_224")		eTemp_eFace = TATTOO_BACK BREAK //"Hottie"
						CASE HASH("TAT_FM_227")		eTemp_eFace = TATTOO_BACK BREAK //"Smoking Dagger"
						CASE HASH("TAT_FM_233")		eTemp_eFace = TATTOO_BACK BREAK //"Faith"
						CASE HASH("TAT_FM_234")		eTemp_eFace = TATTOO_BACK BREAK //"Chinese Dragon"
						CASE HASH("TAT_FM_236")		eTemp_eFace = TATTOO_BACK BREAK //"Dragon"
						CASE HASH("TAT_FM_238")		eTemp_eFace = TATTOO_BACK BREAK //"Grim Reaper"
						CASE HASH("TAT_HP_009")		eTemp_eFace = TATTOO_BACK BREAK //"Squares"
						CASE HASH("TAT_HP_019")		eTemp_eFace = TATTOO_BACK BREAK //"Charm"
						CASE HASH("TAT_HP_040")		eTemp_eFace = TATTOO_BACK BREAK //"Black Anchor"
						//Male - Torso
						CASE HASH("TAT_HP_006")		eTemp_eFace = TATTOO_RIGHT BREAK //"Feather Birds"
						//Male - Head
						CASE HASH("TAT_BUS_006")	eTemp_eFace = TATTOO_RIGHT BREAK //"Bold Dollar Sign"
						CASE HASH("TAT_BUS_007")	eTemp_eFace = TATTOO_LEFT BREAK //"Script Dollar Sign"
						CASE HASH("TAT_BUS_008")	eTemp_eFace = TATTOO_BACK BREAK //"$100"
						//Male - right leg
						CASE HASH("TAT_BB_025")		eTemp_eFace = TATTOO_BACK BREAK //"Tribal Tiki Tower"
						CASE HASH("TAT_FM_014")		eTemp_eFace = TATTOO_BACK BREAK //"Skull and Sword"
						CASE HASH("TAT_FM_223")		eTemp_eFace = TATTOO_BACK BREAK //"Fiery Dragon"
						CASE HASH("TAT_FM_240")		eTemp_eFace = TATTOO_BACK BREAK //"Broken Skull"
						CASE HASH("TAT_FM_244")		eTemp_eFace = TATTOO_BACK BREAK //"Indian Ram"
						CASE HASH("TAT_HP_038")		eTemp_eFace = TATTOO_RIGHT BREAK //"Grub"
						CASE HASH("TAT_HP_042")		eTemp_eFace = TATTOO_BACK BREAK //"Sparkplug"
						//Male - left arm
						CASE HASH("TAT_BUS_003")	eTemp_eFace = TATTOO_FRONT BREAK //"$100 Bill"	
						CASE HASH("TAT_BUS_004")	eTemp_eFace = TATTOO_BACK BREAK //"All-Seeing Eye"	
						CASE HASH("TAT_HP_003")		eTemp_eFace = TATTOO_BACK BREAK //"Diamond Sparkle"
						CASE HASH("TAT_HP_015")		eTemp_eFace = TATTOO_FRONT BREAK //"Mustache"
						CASE HASH("TAT_HP_016")		eTemp_eFace = TATTOO_FRONT BREAK //"Lightning Bolt"
						CASE HASH("TAT_HP_026")		eTemp_eFace = TATTOO_LEFT BREAK //"Pizza"
						CASE HASH("TAT_HP_028")		eTemp_eFace = TATTOO_BACK BREAK //"Thorny Rose"
						//Male - Right Arm
						CASE HASH("TAT_FM_229")		eTemp_eFace = TATTOO_RIGHT BREAK //"Mermaid"
						CASE HASH("TAT_FM_247")		eTemp_eFace = TATTOO_RIGHT BREAK //"Lion"
						CASE HASH("TAT_HP_017")		eTemp_eFace = TATTOO_BACK BREAK //"Eye Triangle"
						CASE HASH("TAT_BUS_010")	eTemp_eFace = TATTOO_FRONT BREAK //"Green"
						
						#IF IS_DEBUG_BUILD
						DEFAULT
							IF bDrawDebugTattooText
								CPRINTLN(DEBUG_SHOPS, "CASE HASH(\"", sTattooData.sLabel, "\")		eTemp_eFace = TATTOO_UNKNOWN BREAK //\"", GET_STRING_FROM_TEXT_FILE(sTattooData.sLabel), "\"")
							ENDIF
						BREAK
						#ENDIF
					ENDSWITCH
					
					IF eTemp_eFace != sTattooData.eFace
						#IF IS_DEBUG_BUILD
						IF bDrawDebugTattooText
							CDEBUG2LN(DEBUG_SHOPS, "TATTOO_MP_FM ", sTattooData.sLabel, " (\"", GET_STRING_FROM_TEXT_FILE(sTattooData.sLabel), "\") changed from ", GET_STRING_FROM_TATTOO_FACING(sTattooData.eFace), " to ", GET_STRING_FROM_TATTOO_FACING(eTemp_eFace))
						ENDIF
						#ENDIF
						
						sTattooData.eFace = eTemp_eFace
					ENDIF
				ENDIF
				
				CONST_FLOAT fCAM_ROT_ALPHA_MULT	0.05
				
				CONST_FLOAT fCAM_ROT_ALPHA_MIN	0.0
				CONST_FLOAT fCAM_ROT_ALPHA_MAX	1.0
				
				#IF IS_DEBUG_BUILD
				PRINTLN(sTattooData.sLabel, " ", GET_STRING_FROM_TATTOO_FACING(eBackTat),
						" -> ", GET_STRING_FROM_TATTOO_FACING(sTattooData.eFace),
						" [",
						GET_STRING_FROM_TATTOO_FACING(eStored_eFace),
						"] (fCamRotAlpha: ", fCamRotAlpha,
						", fStoredFrontZ:", fStoredFrontZ,
						", fStoredBackZ:", fStoredBackZ,
						", vRot.z:", sData.sShopInfo.sCameras[0].vRot.z, ")")
				
				INT iRow = 0
				VECTOR VecCoords = GET_ENTITY_COORDS(sCutsceneData.pedID)
				TEXT_LABEL_63 str
				IF bDrawDebugTattooText
					SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
					
					VECTOR vOffset
					str  = "sLabel: "
					str  = sTattooData.sLabel
					str += " \""
					str += GET_STRING_FROM_TEXT_FILE(sTattooData.sLabel)
					str += "\", "
					str += GET_STRING_FROM_TATTOO_FACING(sTattooData.eFace)
					DRAW_DEBUG_TEXT_WITH_OFFSET(str, VecCoords,		0,iRow*10,	0,0,255)	iRow++
					
					str  = GET_STRING_FROM_TATTOO_FACING(eBackTat)
					str += " -> "
					str += GET_STRING_FROM_TATTOO_FACING(sTattooData.eFace)
					DRAW_DEBUG_TEXT_WITH_OFFSET(str, VecCoords,		0,iRow*10,	0,0,255)	iRow++
					
					str  = "["
					str += GET_STRING_FROM_TATTOO_FACING(eStored_eFace)
					str += "], group: "
					str += GET_STRING_FROM_TATTOO_GROUP(GET_TATTOO_GROUP_FROM_LABEL(GET_ENTITY_MODEL(sCutsceneData.pedID), sTattooData.sLabel, sTattooData.iUpgradeGroup))
					DRAW_DEBUG_TEXT_WITH_OFFSET(str, VecCoords,		0,iRow*10,	0,0,255)	iRow++
					
					str  = "fCamRotAlpha: "
					str += GET_STRING_FROM_FLOAT(fCamRotAlpha)
					IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
						str += " MIN"
					ELIF fCamRotAlpha = fCAM_ROT_ALPHA_MAX
						str += " MAX"
					ENDIF
					DRAW_DEBUG_TEXT_WITH_OFFSET(str, VecCoords,		0,iRow*10,	ROUND(TO_FLOAT(255)*(fCamRotAlpha)),0,ROUND(TO_FLOAT(255)*(1.0-fCamRotAlpha)))	iRow++
					
					str  = "fStoredFrontZ:"
					str += GET_STRING_FROM_FLOAT(fStoredFrontZ)
					str += " FROM..."
					DRAW_DEBUG_TEXT_WITH_OFFSET(str, VecCoords,		0,iRow*10,	255,255,0)	iRow++
					vOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(VecCoords, fStoredFrontZ, <<0,1,0>>)
					DRAW_DEBUG_LINE(VecCoords, vOffset,							0,0,255)
					DRAW_DEBUG_SPHERE(vOffset, 0.1,								0,0,255)
					DRAW_DEBUG_TEXT_WITH_OFFSET(GET_STRING_FROM_TATTOO_FACING(eBackTat),
													vOffset,		0,00,		255,255,0)
					DRAW_DEBUG_TEXT_WITH_OFFSET(str, vOffset,		0,10,		255,255,0)
					
					str  = "fStoredBackZ:"
					str += GET_STRING_FROM_FLOAT(fStoredBackZ)
					str += " ...TO"
					DRAW_DEBUG_TEXT_WITH_OFFSET(str, VecCoords,		0,iRow*10,	0,255,0)	iRow++
					vOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(VecCoords, fStoredBackZ, <<0,1,0>>)
					DRAW_DEBUG_LINE(VecCoords, vOffset,							255,0,255)
					DRAW_DEBUG_SPHERE(vOffset, 0.1,								255,0,255)
					DRAW_DEBUG_TEXT_WITH_OFFSET(GET_STRING_FROM_TATTOO_FACING(sTattooData.eFace),
													vOffset,		0,00,		0,255,0)
					DRAW_DEBUG_TEXT_WITH_OFFSET(str, vOffset,		0,10,		0,255,0)
					
					str  = "vRot.z: "
					str += GET_STRING_FROM_FLOAT(sData.sShopInfo.sCameras[0].vRot.z)
					DRAW_DEBUG_TEXT_WITH_OFFSET(str, VecCoords,		0,iRow*10,	0,0,255)	iRow++
				ENDIF
				#ENDIF
				
				IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
					#IF IS_DEBUG_BUILD
					IF bDrawDebugTattooText
					DRAW_DEBUG_TEXT_WITH_OFFSET("Settled on face.", VecCoords,			0,iRow*10,	0,0,255)	iRow++
					ENDIF
					#ENDIF
					
					bResetCamRotAlpha = FALSE
					eStored_eFace = sTattooData.eFace

				ELIF fCamRotAlpha = fCAM_ROT_ALPHA_MAX
					#IF IS_DEBUG_BUILD
					IF bDrawDebugTattooText
					DRAW_DEBUG_TEXT_WITH_OFFSET("AAA two!", VecCoords,					0,iRow*10,	0,255,0)	iRow++
					ENDIF
					#ENDIF
				ELSE
					IF eBackTat != sTattooData.eFace
						#IF IS_DEBUG_BUILD
						IF bDrawDebugTattooText
						DRAW_DEBUG_TEXT_WITH_OFFSET("INTERPING TO FACE!", VecCoords,	0,iRow*10,	0,255,0)	iRow++
						ENDIF
						#ENDIF
						
						eStored_eFace = sTattooData.eFace
					ELSE
						
						IF NOT bResetCamRotAlpha
							eBackTat = eStored_eFace
							
							fCamRotAlpha = fCAM_ROT_ALPHA_MIN
							bResetCamRotAlpha = TRUE
							
							#IF IS_DEBUG_BUILD
							IF bDrawDebugTattooText
							DRAW_DEBUG_TEXT_WITH_OFFSET("AAA four!", VecCoords,		0,iRow*10,	0,255,0)	iRow++
							ENDIF
						ELSE
							IF bDrawDebugTattooText
							DRAW_DEBUG_TEXT_WITH_OFFSET("AAA five!", VecCoords,		0,iRow*10,	0,255,0)	iRow++
							ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				SWITCH eBackTat
					CASE TATTOO_FRONT
						SWITCH sTattooData.eFace
							CASE TATTOO_FRONT		//FRONT -> FRONT
								IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
									fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
									fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_FRONT)
								ENDIF
								
								IF fCamRotAlpha <> fCAM_ROT_ALPHA_MIN
								OR bResetCamRotAlpha
									fCamRotAlpha += fCAM_ROT_ALPHA_MULT	
									IF fCamRotAlpha >= fCAM_ROT_ALPHA_MAX
										fCamRotAlpha = fCAM_ROT_ALPHA_MIN
										eBackTat = sTattooData.eFace
									ELSE
										sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_TATTOO_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
									ENDIF
								ENDIF
							BREAK
							CASE TATTOO_BACK		//FRONT -> BACK
								IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
									fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
									fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_BACK)
								ENDIF
								fCamRotAlpha += fCAM_ROT_ALPHA_MULT	
								IF fCamRotAlpha >= fCAM_ROT_ALPHA_MAX
									fCamRotAlpha = fCAM_ROT_ALPHA_MAX
									eBackTat = sTattooData.eFace
								ELSE
									sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_TATTOO_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
								ENDIF
							BREAK
							CASE TATTOO_LEFT		//FRONT -> LEFT
								IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
									fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
									fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_LEFT)
								ENDIF
								fCamRotAlpha += fCAM_ROT_ALPHA_MULT	
								IF fCamRotAlpha >= fCAM_ROT_ALPHA_MAX
									fCamRotAlpha = fCAM_ROT_ALPHA_MAX
									eBackTat = sTattooData.eFace
								ELSE
									sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_TATTOO_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
								ENDIF
							BREAK
							CASE TATTOO_RIGHT		//FRONT -> RIGHT
								IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
									fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
									fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_RIGHT)
								ENDIF
								fCamRotAlpha += fCAM_ROT_ALPHA_MULT	
								IF fCamRotAlpha >= fCAM_ROT_ALPHA_MAX
									fCamRotAlpha = fCAM_ROT_ALPHA_MAX
									eBackTat = sTattooData.eFace
								ELSE
									sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_TATTOO_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
								ENDIF
							BREAK
							DEFAULT
								CWARNINGLN(DEBUG_SHOPS, "test FRONT-UNKNOWN[", sTattooData.eFace, "]")
							BREAK
						ENDSWITCH
					BREAK
					CASE TATTOO_BACK
						SWITCH sTattooData.eFace
							CASE TATTOO_FRONT		//BACK -> FRONT
								IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
									fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
									fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_FRONT)
								ENDIF
								fCamRotAlpha += fCAM_ROT_ALPHA_MULT	
								IF fCamRotAlpha >= fCAM_ROT_ALPHA_MAX
									fCamRotAlpha = fCAM_ROT_ALPHA_MAX
									eBackTat = sTattooData.eFace
								ELSE
									sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_TATTOO_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
								ENDIF
							BREAK
							CASE TATTOO_BACK		//BACK -> BACK
								IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
									fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
									fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_BACK)
								ENDIF
								
								IF fCamRotAlpha <> fCAM_ROT_ALPHA_MIN
								OR bResetCamRotAlpha
									fCamRotAlpha += fCAM_ROT_ALPHA_MULT	
									IF fCamRotAlpha >= fCAM_ROT_ALPHA_MAX
										fCamRotAlpha = fCAM_ROT_ALPHA_MIN
										eBackTat = sTattooData.eFace
									ELSE
										sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_TATTOO_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
									ENDIF
								ENDIF
							BREAK
							CASE TATTOO_LEFT		//BACK -> LEFT
								IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
									fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
									fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_LEFT)
								ENDIF
								fCamRotAlpha += fCAM_ROT_ALPHA_MULT	
								IF fCamRotAlpha >= fCAM_ROT_ALPHA_MAX
									fCamRotAlpha = fCAM_ROT_ALPHA_MAX
									eBackTat = sTattooData.eFace
								ELSE
									sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_TATTOO_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
								ENDIF
							BREAK
							CASE TATTOO_RIGHT		//BACK -> RIGHT
								IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
									fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
									fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_RIGHT)
								ENDIF
								fCamRotAlpha += fCAM_ROT_ALPHA_MULT
								IF fCamRotAlpha >= 1
									fCamRotAlpha = 1
									eBackTat = sTattooData.eFace
								ELSE
									sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_TATTOO_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
								ENDIF
							BREAK
							DEFAULT
								CWARNINGLN(DEBUG_SHOPS, "test BACK-UNKNOWN[", sTattooData.eFace, "]")
							BREAK
						ENDSWITCH
					BREAK 
					CASE TATTOO_LEFT
						SWITCH sTattooData.eFace
							CASE TATTOO_FRONT		//LEFT -> FRONT
								IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
									fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
									fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_FRONT)
								ENDIF
								fCamRotAlpha += fCAM_ROT_ALPHA_MULT	
								IF fCamRotAlpha >= fCAM_ROT_ALPHA_MAX
									fCamRotAlpha = fCAM_ROT_ALPHA_MAX
									eBackTat = sTattooData.eFace
								ELSE
									sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_TATTOO_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
								ENDIF
							BREAK
							CASE TATTOO_BACK		//LEFT -> BACK
								IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
									fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
									fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_BACK)
								ENDIF
								fCamRotAlpha += fCAM_ROT_ALPHA_MULT	
								IF fCamRotAlpha >= fCAM_ROT_ALPHA_MAX
									fCamRotAlpha = fCAM_ROT_ALPHA_MAX
									eBackTat = sTattooData.eFace
								ELSE
									sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_TATTOO_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
								ENDIF
							BREAK
							CASE TATTOO_LEFT		//LEFT -> LEFT
								IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
									fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
									fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_LEFT)
								ENDIF
								
								IF fCamRotAlpha <> fCAM_ROT_ALPHA_MIN
								OR bResetCamRotAlpha
									fCamRotAlpha += fCAM_ROT_ALPHA_MULT	
									IF fCamRotAlpha >= fCAM_ROT_ALPHA_MAX
										fCamRotAlpha = fCAM_ROT_ALPHA_MIN
										eBackTat = sTattooData.eFace
									ELSE
										sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_TATTOO_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
									ENDIF
								ENDIF
							BREAK
							CASE TATTOO_RIGHT		//LEFT -> RIGHT
								IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
									fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
									fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_RIGHT)
								ENDIF
								fCamRotAlpha += fCAM_ROT_ALPHA_MULT	
								IF fCamRotAlpha >= fCAM_ROT_ALPHA_MAX
									fCamRotAlpha = fCAM_ROT_ALPHA_MAX
									eBackTat = sTattooData.eFace
								ELSE
									sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_TATTOO_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
								ENDIF
							BREAK
							DEFAULT
								CWARNINGLN(DEBUG_SHOPS, "test LEFT-UNKNOWN[", sTattooData.eFace, "]")
							BREAK
						ENDSWITCH
					BREAK 
					CASE TATTOO_RIGHT
						SWITCH sTattooData.eFace
							CASE TATTOO_FRONT		//RIGHT -> FRONT
								IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
									fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
									fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_FRONT)
								ENDIF
								fCamRotAlpha += fCAM_ROT_ALPHA_MULT	
								IF fCamRotAlpha >= fCAM_ROT_ALPHA_MAX
									fCamRotAlpha = fCAM_ROT_ALPHA_MAX
									eBackTat = sTattooData.eFace
								ELSE
									sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_TATTOO_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
								ENDIF
							BREAK
							CASE TATTOO_BACK		//RIGHT -> BACK
								IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
									fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
									fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_BACK)
								ENDIF
								fCamRotAlpha += fCAM_ROT_ALPHA_MULT	
								IF fCamRotAlpha >= fCAM_ROT_ALPHA_MAX
									fCamRotAlpha = fCAM_ROT_ALPHA_MAX
									eBackTat = sTattooData.eFace
								ELSE
									sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_TATTOO_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
								ENDIF
							BREAK
							CASE TATTOO_LEFT		//RIGHT -> LEFT
								IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
									fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
									fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_LEFT)
								ENDIF
								fCamRotAlpha += fCAM_ROT_ALPHA_MULT	
								IF fCamRotAlpha >= fCAM_ROT_ALPHA_MAX
									fCamRotAlpha = fCAM_ROT_ALPHA_MAX
									eBackTat = sTattooData.eFace
								ELSE
									sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_TATTOO_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
								ENDIF
							BREAK
							CASE TATTOO_RIGHT		//RIGHT -> RIGHT
								IF fCamRotAlpha = fCAM_ROT_ALPHA_MIN
									fStoredFrontZ = sData.sShopInfo.sCameras[0].vRot.z
									fStoredBackZ = (GET_ENTITY_HEADING(sCutsceneData.pedID) - fHEADING_RIGHT)
								ENDIF
								
								IF fCamRotAlpha <> fCAM_ROT_ALPHA_MIN
								OR bResetCamRotAlpha
									fCamRotAlpha += fCAM_ROT_ALPHA_MULT	
									IF fCamRotAlpha >= fCAM_ROT_ALPHA_MAX
										fCamRotAlpha = fCAM_ROT_ALPHA_MIN
										eBackTat = sTattooData.eFace
									ELSE
										sData.sShopInfo.sCameras[0].vRot.z = COSINE_INTERP_TATTOO_FLOAT(fStoredFrontZ,fStoredBackZ,fCamRotAlpha)
									ENDIF
								ENDIF
							BREAK
							DEFAULT
								CWARNINGLN(DEBUG_SHOPS, "test RIGHT-UNKNOWN[", sTattooData.eFace, "]")
							BREAK
						ENDSWITCH
					BREAK 
					DEFAULT
						IF eBackTat = INT_TO_ENUM(TATTOO_FACING, -1)
							SWITCH sTattooData.eFace
								CASE TATTOO_FRONT
									CWARNINGLN(DEBUG_SHOPS, "test NULL-FRONT")
									eBackTat = TATTOO_BACK
								BREAK
								CASE TATTOO_BACK
									CWARNINGLN(DEBUG_SHOPS, "test NULL-BACK")
									eBackTat = TATTOO_FRONT
								BREAK
								CASE TATTOO_LEFT
									CWARNINGLN(DEBUG_SHOPS, "test NULL-LEFT")
									eBackTat = TATTOO_RIGHT
								BREAK
								CASE TATTOO_RIGHT
									CWARNINGLN(DEBUG_SHOPS, "test NULL-RIGHT")
									eBackTat = TATTOO_LEFT
								BREAK
								DEFAULT
									CWARNINGLN(DEBUG_SHOPS, "test NULL-UNKNOWN[", sTattooData.eFace, "]")
								BREAK
							ENDSWITCH
						ELSE
							SWITCH sTattooData.eFace
								CASE TATTOO_FRONT
									CWARNINGLN(DEBUG_SHOPS, "test UNKNOWN[", eBackTat, "]-FRONT")
								BREAK
								CASE TATTOO_BACK
									CWARNINGLN(DEBUG_SHOPS, "test UNKNOWN[", eBackTat, "]-BACK")
								BREAK
								CASE TATTOO_LEFT
									CWARNINGLN(DEBUG_SHOPS, "test UNKNOWN[", eBackTat, "]-LEFT")
								BREAK
								CASE TATTOO_RIGHT
									CWARNINGLN(DEBUG_SHOPS, "test UNKNOWN[", eBackTat, "]-RIGHT")
								BREAK
								DEFAULT
									CWARNINGLN(DEBUG_SHOPS, "test UNKNOWN[", eBackTat, "]-UNKNOWN[", sTattooData.eFace, "]")
								BREAK
							ENDSWITCH
						ENDIF
					BREAK 
				ENDSWITCH
			ENDIF
		ENDIF
	ELSE
		bResetCamRotAlpha = FALSE
		fCamRotAlpha = 0
		bBackTat = FALSE
		eBackTat = TATTOO_FRONT
		eStored_eFace = sTattooData.eFace
	ENDIF
	
	//ZOOM
	FLOAT fZoom = 34
	
	SWITCH eCurrentZone
		CASE PDZ_LEFT_LEG	fZoom = 38 BREAK
		CASE PDZ_RIGHT_LEG	fZoom = 38 BREAK
		CASE PDZ_RIGHT_ARM	fZoom = 36 BREAK
		CASE PDZ_LEFT_ARM	fZoom = 36 BREAK
		CASE PDZ_HEAD		fZoom = 30 BREAK
	ENDSWITCH
	
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,caZoomInput)
			bZoomToggle = NOT bZoomToggle
		ENDIF
	ELSE
		bZoomToggle = FALSE
	ENDIF

	IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,caZoomInput)
	OR bZoomToggle
		IF fCamZoomAlpha < 1
			fCamZoomAlpha = fCamZoomAlpha + 0.05
		ENDIF
		IF fCamZoomAlpha > 1
			fCamZoomAlpha = 1
		ENDIF
	ELSE
		IF fCamZoomAlpha > 0
			fCamZoomAlpha = fCamZoomAlpha - 0.05
		ENDIF
		IF fCamZoomAlpha < 0
			fCamZoomAlpha = 0
		ENDIF
	ENDIF	
	
	VECTOR vCamPos = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vStoredPlayerCoords, sData.sShopInfo.sCameras[0].vRot.z, vBrowseCamOffset)
	sData.sShopInfo.sCameras[0].vPos.x = vCamPos.x
	sData.sShopInfo.sCameras[0].vPos.y = vCamPos.y
	
	sData.sShopInfo.sCameras[0].vPos.z += ((vCamPos.z - sData.sShopInfo.sCameras[0].vPos.z) * 0.25)
	/*
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF eCurrentZone <> PDZ_LEFT_LEG
		AND eCurrentZone <> PDZ_RIGHT_LEG
		AND NOT IS_PED_INJURED(sCutsceneData.pedID)
		AND IS_PED_WEARING_HIGH_HEELS(sCutsceneData.pedID)
			sData.sShopInfo.sCameras[0].vPos.z += 0.03
			vCurrentBrowseCamLookAt.z += 0.03
		ENDIF
	ENDIF
	*/
	IF fCamZoomAlpha <> 0
		IF eCurrentZone = PDZ_HEAD	
			FLOAT fHeadCamOffset = 0.07
			sData.sShopInfo.sCameras[0].vPos.z = COSINE_INTERP_FLOAT(sData.sShopInfo.sCameras[0].vPos.z,sData.sShopInfo.sCameras[0].vPos.z + fHeadCamOffset,fCamZoomAlpha)
			vCurrentBrowseCamLookAt.z = COSINE_INTERP_FLOAT(vCurrentBrowseCamLookAt.z,vCurrentBrowseCamLookAt.z + fHeadCamOffset,fCamZoomAlpha)
		ENDIF
		sData.sShopInfo.sCameras[0].fFOV = COSINE_INTERP_FLOAT(52.0,fZoom,fCamZoomAlpha)
	ENDIF
	
	////////////////////////////////////////////////////////////////
	///     UPDATE
	SET_CAM_COORD(sData.sShopInfo.sCameras[0].camID, sData.sShopInfo.sCameras[0].vPos)
	SET_CAM_FOV(sData.sShopInfo.sCameras[0].camID, sData.sShopInfo.sCameras[0].fFOV)
	POINT_CAM_AT_COORD(sData.sShopInfo.sCameras[0].camID, vCurrentBrowseCamLookAt)
	SET_CAM_NEAR_CLIP(sData.sShopInfo.sCameras[0].camID,0.7)
	
	IF NOT sData.sShopInfo.sCameras[0].bSetup
		SET_CAM_ACTIVE(sData.sShopInfo.sCameras[0].camID, TRUE)
		RENDER_SCRIPT_CAMS(TRUE, FALSE)
		sData.sShopInfo.sCameras[0].bSetup = TRUE
		
		//Set multihead blinders on
		SET_MULTIHEAD_SAFE(TRUE,TRUE)
	ENDIF
ENDPROC

CONST_INT iTATTOO_ARM_ANIM_0_none		0
CONST_INT iTATTOO_ARM_ANIM_1_leftArm	1
CONST_INT iTATTOO_ARM_ANIM_2_rightArm	2

#IF IS_DEBUG_BUILD
FUNC STRING DEBUG_Get_String_From_Tattoo_Anim(INT iArmAnim)
	SWITCH iArmAnim
		CASE iTATTOO_ARM_ANIM_0_none		RETURN "0_none" BREAK
		CASE iTATTOO_ARM_ANIM_1_leftArm		RETURN "1_leftArm" BREAK
		CASE iTATTOO_ARM_ANIM_2_rightArm	RETURN "2_rightArm" BREAK
	ENDSWITCH
	
	CASSERTLN(DEBUG_SHOPS, "DEBUG_Get_String_From_Tattoo_Anim - unknown arm anim - ", iArmAnim)
	RETURN ""
ENDFUNC
#ENDIF

INT iPreviousArmAnim = iTATTOO_ARM_ANIM_0_none
TEXT_LABEL sArmAnimClip = ""
PROC UPDATE_TATTOO_BROWSE_ANIM()
	#IF IS_DEBUG_BUILD
	IF bTestNewArmAnims
		EXIT
	ENDIF
	#ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	INT iCurrentArmAnim = iTATTOO_ARM_ANIM_0_none
	
	
	PED_DECORATION_ZONE eZone = PDZ_INVALID
	TORSO_DECORATION_SUBZONE eSubzone = TDS_INVALID
	GET_PED_DECORATION_ZONE_FROM_INT(sData.sBrowseInfo.iCurrentGroup, eZone, eSubzone)
	
	TATTOO_DATA_STRUCT sTattooData
	IF (eZone = PDZ_LEFT_ARM) OR (eZone = PDZ_RIGHT_ARM)
		TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
		IF GET_TATTOO_DATA(sTattooData, sData.eCurrentTattoo, eFaction, PLAYER_PED_ID())
			TATTOO_GROUP_ENUM eGroup = GET_TATTOO_GROUP_FROM_LABEL(GET_ENTITY_MODEL(PLAYER_PED_ID()), sTattooData.sLabel, sTattooData.iUpgradeGroup)
			
			IF eGroup = TG_ARM_LEFT_WRIST				//(Padlock)?
			OR eGroup = TG_ARM_LEFT_UPPER_BICEP			//(Mustache)?
			OR eGroup = TG_ARM_LEFT_LOWER_INNER			//($100 Bill)?
				iCurrentArmAnim = iTATTOO_ARM_ANIM_1_leftArm		//left arm
			ENDIF
			IF eGroup = TG_ARM_RIGHT_WRIST
			OR eGroup = TG_ARM_RIGHT_UPPER_BICEP
			OR eGroup = TG_ARM_RIGHT_LOWER_INNER
				iCurrentArmAnim = iTATTOO_ARM_ANIM_2_rightArm		//right arm
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL_63 str1 = "", str2 = ""
	HUD_COLOURS hudColour = HUD_COLOUR_BLACK
	#ENDIF
	
	IF iCurrentArmAnim != iPreviousArmAnim
		IF NOT DOES_ANIM_DICT_EXIST(sAnimDictArm)
			EXIT
		ENDIF
		
		REQUEST_ANIM_DICT(sAnimDictArm)
		IF NOT HAS_ANIM_DICT_LOADED(sAnimDictArm)
			CWARNINGLN(DEBUG_SHOPS, "UPDATE_TATTOO_BROWSE_ANIM: Havent loaded anim dict sAnimDictArm: \"", sAnimDictArm, "\"")
			EXIT
		ENDIF
		
		#IF IS_DEBUG_BUILD
		str1  = "iCurrent["
		str1 += DEBUG_Get_String_From_Tattoo_Anim(iCurrentArmAnim)
		str1 += "] != iPrevious["
		str1 += DEBUG_Get_String_From_Tattoo_Anim(iPreviousArmAnim)
		str1 += "]"
		#ENDIF
		
		STRING sArmTransitionAnimClip = "", sArmHoldAnimClip = ""
		SWITCH iCurrentArmAnim
			//none
			CASE iTATTOO_ARM_ANIM_0_none
				SWITCH iPreviousArmAnim
					CASE iTATTOO_ARM_ANIM_1_leftArm		sArmTransitionAnimClip = "LEFT_ARM_OUTRO" BREAK
					CASE iTATTOO_ARM_ANIM_2_rightArm	sArmTransitionAnimClip = "RIGHT_ARM_OUTRO" BREAK
				ENDSWITCH
				sArmHoldAnimClip = ""
			BREAK
			//left arm
			CASE iTATTOO_ARM_ANIM_1_leftArm
				SWITCH iPreviousArmAnim
					CASE iTATTOO_ARM_ANIM_0_none		sArmTransitionAnimClip = "LEFT_ARM_INTRO" BREAK
					CASE iTATTOO_ARM_ANIM_2_rightArm	sArmTransitionAnimClip = "LEFT_ARM_INTRO" BREAK
				ENDSWITCH
				sArmHoldAnimClip = "LEFT_ARM_BASE"
			BREAK
			//right arm
			CASE iTATTOO_ARM_ANIM_2_rightArm
				SWITCH iPreviousArmAnim
					CASE iTATTOO_ARM_ANIM_0_none		sArmTransitionAnimClip = "RIGHT_ARM_INTRO" BREAK
					CASE iTATTOO_ARM_ANIM_1_leftArm		sArmTransitionAnimClip = "RIGHT_ARM_INTRO" BREAK
				ENDSWITCH
				sArmHoldAnimClip = "RIGHT_ARM_BASE"
			BREAK
		ENDSWITCH
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sArmTransitionAnimClip)
			IF NOT ARE_STRINGS_EQUAL(sArmAnimClip, sArmTransitionAnimClip)
				#IF IS_DEBUG_BUILD
				str2  = "sArmTransitionAnimClip: \""
				str2 += sArmTransitionAnimClip
				str2 += "\""
				hudColour = HUD_COLOUR_RED
				#ENDIF
				
				FLOAT fBlendInDelta = SLOW_BLEND_IN
				FLOAT fBlendOutDelta = REALLY_SLOW_BLEND_OUT
				
				TASK_PLAY_ANIM(sCutsceneData.pedID, sAnimDictArm, sArmTransitionAnimClip,
						fBlendInDelta,		//FLOAT fBlendInDelta = NORMAL_BLEND_IN,
						fBlendOutDelta,		//FLOAT fBlendOutDelta = NORMAL_BLEND_OUT,
						DEFAULT,			//INT nTimeToPlay =-1,
						AF_HOLD_LAST_FRAME,	//ANIMATION_FLAGS AnimFlags = AF_DEFAULT,
						DEFAULT,			//FLOAT startPhase = 0.0,
						DEFAULT,			//BOOL phaseControlled = FALSE,
						DEFAULT,			//IK_CONTROL_FLAGS ikFlags = AIK_NONE,
						DEFAULT)			//BOOL bAllowOverrideCloneUpdate = FALSE)
				sArmAnimClip = sArmTransitionAnimClip
			ELSE
				IF IS_ENTITY_PLAYING_ANIM(sCutsceneData.pedID, sAnimDictArm, sArmTransitionAnimClip)
					FLOAT fAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(sCutsceneData.pedID, sAnimDictArm, sArmTransitionAnimClip)
					
					#IF IS_DEBUG_BUILD
					str2  = "playing \""
					str2 += sArmTransitionAnimClip
					str2 += "\" time: "
					str2 += GET_STRING_FROM_FLOAT(fAnimTime)
					str2 += "%"
					hudColour = HUD_COLOUR_ORANGE
					#ENDIF
					
					IF fAnimTime >= 0.95
						#IF IS_DEBUG_BUILD
						str2  = "sArmHoldAnimClip: \""
						str2 += sArmHoldAnimClip
						str2 += "\""
						hudColour = HUD_COLOUR_YELLOW
						#ENDIF
						
						IF NOT IS_STRING_NULL_OR_EMPTY(sArmHoldAnimClip)
							TASK_PLAY_ANIM(sCutsceneData.pedID, sAnimDictArm, sArmHoldAnimClip,
									DEFAULT,		//FLOAT fBlendInDelta = NORMAL_BLEND_IN,
									DEFAULT,		//FLOAT fBlendOutDelta = NORMAL_BLEND_OUT,
									DEFAULT,		//INT nTimeToPlay =-1,
									AF_LOOPING,		//ANIMATION_FLAGS AnimFlags = AF_DEFAULT,
									DEFAULT,		//FLOAT startPhase = 0.0,
									DEFAULT,		//BOOL phaseControlled = FALSE,
									DEFAULT,		//IK_CONTROL_FLAGS ikFlags = AIK_NONE,
									DEFAULT)		//BOOL bAllowOverrideCloneUpdate = FALSE)
						ELSE
							CLEAR_PED_TASKS(sCutsceneData.pedID)
							REMOVE_ANIM_DICT(sAnimDictArm)
						ENDIF
						sArmAnimClip = sArmHoldAnimClip
						iPreviousArmAnim = iCurrentArmAnim
					ENDIF
				ELSE
					#IF IS_DEBUG_BUILD
					str2  = "NOT playing \""
					str2 += sArmTransitionAnimClip
					str2 += "\""
					hudColour = HUD_COLOUR_BLACK
					#ENDIF
				ENDIF
			ENDIF
		ENDIF
		
	ELSE
		#IF IS_DEBUG_BUILD
		str1  = "iCurrent["
		str1 += DEBUG_Get_String_From_Tattoo_Anim(iCurrentArmAnim)
		str1 += "] = iPrevious["
		str1 += DEBUG_Get_String_From_Tattoo_Anim(iPreviousArmAnim)
		str1 += "]"
		#ENDIF
		
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sArmAnimClip)
			
			IF IS_ENTITY_PLAYING_ANIM(sCutsceneData.pedID, sAnimDictArm, sArmAnimClip)
				TEXT_LABEL tlArmAnimClip = sArmAnimClip
				FLOAT fAnimTime = GET_ENTITY_ANIM_CURRENT_TIME(sCutsceneData.pedID, sAnimDictArm, tlArmAnimClip)
				
				TEXT_LABEL tlArmAnimClipHead, tlArmAnimClipTail
				INT iMidpoint
				
				iMidpoint = GET_LENGTH_OF_LITERAL_STRING(tlArmAnimClip)-GET_LENGTH_OF_LITERAL_STRING("_INTRO")
				tlArmAnimClipTail = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlArmAnimClip, iMidpoint, GET_LENGTH_OF_LITERAL_STRING(tlArmAnimClip))
					
				IF ARE_STRINGS_EQUAL(tlArmAnimClipTail, "_INTRO")
					tlArmAnimClipHead = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlArmAnimClip, 0, iMidpoint)
					
					#IF IS_DEBUG_BUILD
					str2  = "intro \""
					str2 += tlArmAnimClipHead
					str2 += "\"\""
					str2 += tlArmAnimClipTail
					str2 += "\" time: "
					str2 += GET_STRING_FROM_FLOAT(fAnimTime)
					str2 += "%"
					hudColour = HUD_COLOUR_GREEN
					#ENDIF
					
					IF fAnimTime >= 0.95
					AND (iPreviousArmAnim = iTATTOO_ARM_ANIM_0_none)
						IF (eZone = PDZ_LEFT_ARM)
						OR ARE_STRINGS_EQUAL(tlArmAnimClipHead, "LEFT_ARM")
							iPreviousArmAnim = iTATTOO_ARM_ANIM_1_leftArm
						ELIF (eZone = PDZ_RIGHT_ARM)
						OR ARE_STRINGS_EQUAL(tlArmAnimClipHead, "RIGHT_ARM")
							iPreviousArmAnim = iTATTOO_ARM_ANIM_2_rightArm
						ENDIF
						
						PRINTLN("LOCKOUT TEST - set intro iPreviousArmAnim to iTATTOO_ARM_ANIM_", iPreviousArmAnim, " (eZone:PDZ_", eZone, ", tlArmAnimClipHead:\"", tlArmAnimClipHead, "\")")
						
					ENDIF
					
				ELIF ARE_STRINGS_EQUAL(tlArmAnimClipTail, "_OUTRO")
					tlArmAnimClipHead = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlArmAnimClip, 0, iMidpoint)
					
					#IF IS_DEBUG_BUILD
					str2  = "outro \""
					str2 += tlArmAnimClipHead
					str2 += "\"\""
					str2 += tlArmAnimClipTail
					str2 += "\" time: "
					str2 += GET_STRING_FROM_FLOAT(fAnimTime)
					str2 += "%"
					hudColour = HUD_COLOUR_BLUE
					#ENDIF
					
					IF fAnimTime >= 0.95
					AND (iPreviousArmAnim != iTATTOO_ARM_ANIM_0_none)
						iPreviousArmAnim = iTATTOO_ARM_ANIM_0_none
						
						PRINTLN("LOCKOUT TEST - set outro iPreviousArmAnim to iTATTOO_ARM_ANIM_", iPreviousArmAnim)
						
					ENDIF
					
				ELSE
					iMidpoint = GET_LENGTH_OF_LITERAL_STRING(tlArmAnimClip)-GET_LENGTH_OF_LITERAL_STRING("_BASE")
					tlArmAnimClipTail = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlArmAnimClip, iMidpoint, GET_LENGTH_OF_LITERAL_STRING(tlArmAnimClip))
					IF NOT ARE_STRINGS_EQUAL(tlArmAnimClipTail, "_BASE")
						iMidpoint = GET_LENGTH_OF_LITERAL_STRING(tlArmAnimClip)-GET_LENGTH_OF_LITERAL_STRING("_IDLE_A")
						tlArmAnimClipTail = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlArmAnimClip, iMidpoint, GET_LENGTH_OF_LITERAL_STRING(tlArmAnimClip))
					ENDIF
					tlArmAnimClipHead = GET_CHARACTER_FROM_AUDIO_CONVERSATION_FILENAME(tlArmAnimClip, 0, iMidpoint)
					
					#IF IS_DEBUG_BUILD
					str2  = "base \""
					str2 += tlArmAnimClipHead
					str2 += "\"\""
					str2 += tlArmAnimClipTail
					str2 += "\" time: "
					str2 += GET_STRING_FROM_FLOAT(fAnimTime)
					str2 += "%"
					hudColour = HUD_COLOUR_PURPLE
					#ENDIF
					
					IF fAnimTime >= 0.95
						TEXT_LABEL_23 tlArmLoopAnimClip = tlArmAnimClipHead
						IF NOT ARE_STRINGS_EQUAL(tlArmAnimClipTail, "_BASE")
							tlArmLoopAnimClip += "_BASE"
						ELSE
							SWITCH GET_RANDOM_INT_IN_RANGE(0, 3)
								CASE 0
									tlArmLoopAnimClip += "_IDLE_A"
								BREAK
								CASE 1
									tlArmLoopAnimClip += "_IDLE_B"
								BREAK
								CASE 2
									tlArmLoopAnimClip += "_IDLE_C"
								BREAK
							ENDSWITCH
						ENDIF
						
						sArmAnimClip = tlArmLoopAnimClip
						TASK_PLAY_ANIM(sCutsceneData.pedID, sAnimDictArm, tlArmLoopAnimClip,
								DEFAULT,		//FLOAT fBlendInDelta = NORMAL_BLEND_IN,
								DEFAULT,		//FLOAT fBlendOutDelta = NORMAL_BLEND_OUT,
								DEFAULT,		//INT nTimeToPlay =-1,
								AF_LOOPING,		//ANIMATION_FLAGS AnimFlags = AF_DEFAULT,
								DEFAULT,		//FLOAT startPhase = 0.0,
								DEFAULT,		//BOOL phaseControlled = FALSE,
								DEFAULT,		//IK_CONTROL_FLAGS ikFlags = AIK_NONE,
								DEFAULT)		//BOOL bAllowOverrideCloneUpdate = FALSE)
					ENDIF
				ENDIF
			ELSE
				#IF IS_DEBUG_BUILD
				str2  = "not playing \""
				str2 += sArmAnimClip
				str2 += "\""
				hudColour = HUD_COLOUR_BLACK
				#ENDIF
			ENDIF
		ELSE
			#IF IS_DEBUG_BUILD
			str2  = "sArmAnimClip: \""
			str2 += sArmAnimClip
			str2 += "\" - null"
			hudColour = HUD_COLOUR_BLACK
			#ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bDrawDebugTattooText
		INT ired, igreen, iblue, ialpha
		GET_HUD_COLOUR(hudColour, ired, igreen, iblue, ialpha)
		INT iRow = -2
		VECTOR VecCoords = GET_ENTITY_COORDS(sCutsceneData.pedID)
		DRAW_DEBUG_TEXT_WITH_OFFSET(str1, VecCoords,	0,iRow*10,	ired,igreen,iblue)	CDEBUG1LN(DEBUG_SHOPS, "UPDATE_TATTOO_BROWSE_ANIM: ", str1)	iRow--
		DRAW_DEBUG_TEXT_WITH_OFFSET(str2, VecCoords,	0,iRow*10,	ired,igreen,iblue)	CDEBUG1LN(DEBUG_SHOPS, "	", str2)						iRow--
	ENDIF
	#ENDIF
ENDPROC


PROC SETUP_CURRENT_TATTOO_DESCRIPTION()

	IF sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_OUTRO
		// we're leaving the menu so dont do anything
	
	ELIF (sData.sBrowseInfo.iControl = 0)
		TEXT_LABEL_15 tlDesc
		GET_CURRENT_MENU_ITEM_DESCRIPTION(tlDesc)
		IF GET_HASH_KEY(tlDesc) = 0
			IF sData.sBrowseInfo.iCurrentGroup != -1
				PED_DECORATION_ZONE eZone = PDZ_INVALID
				TORSO_DECORATION_SUBZONE eSubzone = TDS_INVALID
				GET_PED_DECORATION_ZONE_FROM_INT(sData.sBrowseInfo.iCurrentGroup, eZone, eSubzone)
				
				IF NOT HAS_PLAYER_VIEWED_ALL_TATTOO_ITEMS_FOR_ZONE(eZone, eSubzone)
					SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_UNLOCK_M")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_THIS_TATTOO_UNLOCKED(sData.eCurrentTattoo, "")
			TEXT_LABEL_15 tlDesc
			GET_CURRENT_MENU_ITEM_DESCRIPTION(tlDesc)
			IF GET_HASH_KEY(tlDesc) = 0
			
				IF NETWORK_IS_GAME_IN_PROGRESS()
				
					SWITCH sData.eCurrentTattoo
						CASE TATTOO_MP_FM_CREW_A
						CASE TATTOO_MP_FM_CREW_B 
						CASE TATTOO_MP_FM_CREW_C 
						CASE TATTOO_MP_FM_CREW_D
							
							IF HAS_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(PLAYER_ID())
								SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_CREW")
							ELSE
								SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_CREWT")
							ENDIF
							
							ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(32)
						BREAK
						
						CASE TATTOO_MP_FM_REACH_RANK_1				SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_RANK1")	BREAK
						CASE TATTOO_MP_FM_REACH_RANK_2				SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_RANK2")	BREAK
						CASE TATTOO_MP_FM_REACH_RANK_3				SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_RANK3")	BREAK
						CASE TATTOO_MP_FM_HEAD_BANGER				SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_RHB")	BREAK
						CASE TATTOO_MP_FM_SLAYER					SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_RDM")	BREAK
						CASE TATTOO_MP_FM_GANGHIDEOUT_CLEAR			SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_RGR")	BREAK
						CASE TATTOO_MP_FM_ARMOURED_VAN_TAKEDOWN		SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_RHU")	BREAK
						CASE TATTOO_MP_FM_HUSTLER					SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_RFM")	BREAK
						CASE TATTOO_MP_FM_WIN_EVER_MODE_ONCE		SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_RAL")	BREAK
						CASE TATTOO_MP_FM_BOUNTY_KILLER				SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_RBH")	BREAK
						CASE TATTOO_MP_FM_HOLD_WORLD_RECORD			SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_RWR")	BREAK
						CASE TATTOO_MP_FM_FULL_MODDED				SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_RSU")	BREAK
						CASE TATTOO_MP_FM_REVENGE_KILL				SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_RRK")	BREAK
						CASE TATTOO_MP_FM_KILL_3_RACERS				SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_RRD")	BREAK
						CASE TATTOO_MP_FM_FMKILLCHEATER				SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_RKC")	BREAK
						CASE TATTOO_MP_FM_RACES_WON					SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_RTC")	BREAK
						CASE TATTOO_MP_FM_HOLD_UP_SHOPS_1			SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_HU1")	BREAK
						CASE TATTOO_MP_FM_HOLD_UP_SHOPS_2			SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_HU2")	BREAK
						CASE TATTOO_MP_FM_HOLD_UP_SHOPS_3			SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_HU3")	BREAK
						CASE TATTOO_MP_FM_HOLD_UP_SHOPS_4			SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_HU4")	BREAK
						
						CASE TATTOO_MP_FM_24
						CASE TATTOO_MP_FM_37
						CASE TATTOO_MP_FM_17
						CASE TATTOO_MP_FM_40 
						CASE TATTOO_MP_FM_41
						CASE TATTOO_MP_FM_38
						CASE TATTOO_MP_FM_09
							SET_CURRENT_MENU_ITEM_DESCRIPTION(GET_TATTOO_DESCRIPTION(sData.eCurrentTattoo, GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())))	
						BREAK
						DEFAULT
						
							TEXT_LABEL_15 tlLabel, tlCustomLabel
							TATTOO_DATA_STRUCT sTattooData
							IF GET_TATTOO_DATA(sTattooData, sData.eCurrentTattoo, GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID()), PLAYER_PED_ID())
								tlLabel = sTattooData.sLabel
							ENDIF
							
							IF IS_THIS_LABEL_A_CREW_TATTOO(tlLabel)
								IF HAS_PLAYER_COMPLETED_INITIAL_AMBIENT_TUTORIALS(PLAYER_ID())
									SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_CREW")
								ELSE
									SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_CREWT")
								ENDIF
								
								ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(32)
							ELIF GET_SPECIAL_LABEL_DESCRIPTION_FOR_SHOP_ITEM(tlLabel, tlCustomLabel)
								SET_CURRENT_MENU_ITEM_DESCRIPTION(tlLabel)
							ELSE
								BOOL bInvalidCatalogueKeyDesc
								bInvalidCatalogueKeyDesc = FALSE
								IF NETWORK_IS_GAME_IN_PROGRESS()
								AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
									BOOL bStarterPackItem
									bStarterPackItem = FALSE
									IF SHOULD_TATTOO_BE_FREE_FOR_PLAYER(tlLabel)
										bStarterPackItem = TRUE
									ENDIF
									
									TEXT_LABEL_63 tlCatalogueKey
									GENERATE_KEY_FOR_SHOP_CATALOGUE(tlCatalogueKey, tlLabel, GET_ENTITY_MODEL(PLAYER_PED_ID()), SHOP_TYPE_TATTOO, 0, 0, DEFAULT, DEFAULT, DEFAULT, bStarterPackItem)
									IF NOT NET_GAMESERVER_CATALOG_ITEM_IS_VALID(tlCatalogueKey)
										CPRINTLN(DEBUG_SHOPS, "<TATT_LOCK> tlCatalogueKey \"", GET_STRING_FROM_TEXT_FILE(tlLabel), "\" is NOT valid  \"", tlCatalogueKey, "\", rank unlock is ", GET_MP_TATTOO_UNLOCK_RANK(sData.eCurrentTattoo))
										bInvalidCatalogueKeyDesc = TRUE
									ELSE
										CPRINTLN(DEBUG_SHOPS, "<TATT_LOCK> tlCatalogueKey \"", GET_STRING_FROM_TEXT_FILE(tlLabel), "\" is valid  \"", tlCatalogueKey, "\", rank unlock is ", GET_MP_TATTOO_UNLOCK_RANK(sData.eCurrentTattoo))
									ENDIF
								ENDIF
								
								IF bInvalidCatalogueKeyDesc
									SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_LCKPC")
								ELSE
									INT iGET_MP_TATTOO_UNLOCK_RANK
									iGET_MP_TATTOO_UNLOCK_RANK = GET_MP_TATTOO_UNLOCK_RANK(sData.eCurrentTattoo)
									IF iGET_MP_TATTOO_UNLOCK_RANK > 0
										SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_UNLOCK")	
										ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(iGET_MP_TATTOO_UNLOCK_RANK)
									ELSE
										SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_UNLOCK")	
									ENDIF
								ENDIF
							ENDIF
						BREAK
					ENDSWITCH
				ENDIF
			ENDIF
		ELSE
			TATTOO_DATA_STRUCT sTattooCurrent
			IF GET_TATTOO_DATA(sTattooCurrent, sData.eCurrentTattoo, GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID()), PLAYER_PED_ID())
				IF NOT HAS_PLAYER_PED_VIEWED_TATTOO(sData.eCurrentTattoo)
					TEXT_LABEL_15 tlDesc
					GET_CURRENT_MENU_ITEM_DESCRIPTION(tlDesc)
					IF GET_HASH_KEY(tlDesc) = 0
							
						
						IF NETWORK_IS_GAME_IN_PROGRESS()
						AND sData.eCurrentTattoo >= TATTOO_MP_FM_DLC
						AND NOT IS_THIS_LABEL_A_CREW_TATTOO(sTattooCurrent.sLabel)
						AND NOT IS_STRING_NULL_OR_EMPTY(GET_SHOP_CONTENT_FOR_MENU(sTattooCurrent.sLabel))
							TEXT_LABEL_15 tlCustomLabel
							IF SHOULD_TATTOO_BE_FREE_FOR_PLAYER(sTattooCurrent.sLabel)
								IF GET_SPECIAL_LABEL_DESCRIPTION_FOR_SHOP_ITEM(sTattooCurrent.sLabel, tlCustomLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION(tlCustomLabel, 4000)
								#IF NOT FEATURE_GEN9_EXCLUSIVE
								ELIF IS_SHOP_CONTENT_FROM_PACK(sTattooCurrent.sLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKCES", 4000)
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooCurrent.sLabel, FALSE))
								ELIF IS_SHOP_CONTENT_FROM_SPECIAL(sTattooCurrent.sLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKCES3", 4000)
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooCurrent.sLabel, FALSE))
								ELIF IS_SHOP_CONTENT_FROM_SURPRISE(sTattooCurrent.sLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKCES4", 4000)
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooCurrent.sLabel, FALSE))
								ELIF IS_SHOP_CONTENT_FROM_BASIC(sTattooCurrent.sLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKCES6", 4000)
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooCurrent.sLabel, FALSE))
								ELIF IS_SHOP_CONTENT_FROM_HALLOWEEN_SURPRISE(sTattooCurrent.sLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKCES7", 4000)
								ELSE
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKCES2", 4000)
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooCurrent.sLabel, TRUE))
								#ENDIF	
								ENDIF
							ELSE
								IF GET_SPECIAL_LABEL_DESCRIPTION_FOR_SHOP_ITEM(sTattooCurrent.sLabel, tlCustomLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION(tlCustomLabel, 4000)
								ELIF IS_SHOP_CONTENT_FROM_PACK(sTattooCurrent.sLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKDLC", 4000)
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooCurrent.sLabel, FALSE))
								ELIF IS_SHOP_CONTENT_FROM_SPECIAL(sTattooCurrent.sLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKDLC3", 4000)
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooCurrent.sLabel, FALSE))
								ELIF IS_SHOP_CONTENT_FROM_SURPRISE(sTattooCurrent.sLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKDLC4", 4000)
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooCurrent.sLabel, FALSE))
								ELIF IS_SHOP_CONTENT_FROM_BASIC(sTattooCurrent.sLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKDLC6", 4000)
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooCurrent.sLabel, FALSE))
								ELIF IS_SHOP_CONTENT_FROM_HALLOWEEN_SURPRISE(sTattooCurrent.sLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKDLC7", 4000)
								ELIF IS_SHOP_CONTENT_FROM_PLATFORM(sTattooCurrent.sLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKDLC8", 4000)
								ELSE
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKDLC2", 4000)
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooCurrent.sLabel, TRUE))
								ENDIF
							ENDIF
						ELSE
							SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_UNLOCK_N", 4000)
						ENDIF
					ENDIF
					
				ELIF GET_TATTOO_DISCOUNT() != 0
					TEXT_LABEL_15 tlDesc
					GET_CURRENT_MENU_ITEM_DESCRIPTION(tlDesc)
					IF GET_HASH_KEY(tlDesc) = 0
						SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_DISC", 0)
						ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(GET_TATTOO_DISCOUNT())
					ENDIF
				ENDIF
				
				PED_DECORATION_ZONE eZone = PDZ_INVALID
				TORSO_DECORATION_SUBZONE eSubzone = TDS_INVALID
				GET_PED_DECORATION_ZONE_FROM_INT(sData.sBrowseInfo.iCurrentGroup, eZone, eSubzone)
				
				TEXT_LABEL tlDisc = ""
				GET_CURRENT_MENU_ITEM_DISCOUNT(tlDisc)
				IF GET_HASH_KEY(tlDisc) = 0
				AND SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO(sData.eCurrentTattoo, sTattooCurrent.sLabel, eZone, eSubzone)
					SET_CURRENT_MENU_ITEM_DISCOUNT("TAT_SALE")
				ENDIF
			ENDIF
		
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Checks for user input, processes ped decorations, and displays help text for tattoo shop
PROC BROWSE_TATTOOS_AND_DRAW_HUD(BOOL &bUpdatePedTattoos)
	
	// If the cutscene ped exists, set the items on them, otherwise set it on the player
	PED_INDEX pedID_ShopPed = PLAYER_PED_ID()
	IF DOES_ENTITY_EXIST(sCutsceneData.pedID)
	AND NOT IS_PED_INJURED(sCutsceneData.pedID)
		pedID_ShopPed = sCutsceneData.pedID
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND DOES_ENTITY_EXIST(pedID_ShopPed)
	AND NOT IS_PED_INJURED(pedID_ShopPed)	
		IF NETWORK_PLAYER_HAS_HEADSET(PLAYER_ID())
	    	IF NETWORK_IS_PLAYER_TALKING(PLAYER_ID())
	    		SET_PED_RESET_FLAG(pedID_ShopPed, PRF_EnableVoiceDrivenMouthMovement, TRUE)
	   		ENDIF
		ENDIF
	ENDIF		
	
	BOOL bRebuildMenu = FALSE
	IF bRebuildMenuNextFrame
		bRebuildMenu = TRUE
		bRebuildMenuNextFrame = FALSE
	ENDIF
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	///     PROCESS INPUT
	///    
	///
	///     
	
	// PC uses a different frontend input for zooming
	IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
		caZoomInput = INPUT_FRONTEND_LS
	ELSE
		caZoomInput = INPUT_FRONTEND_LT
	ENDIF
	
	IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
		bUpdateHelpKeys = TRUE
	ENDIF
		
	
	UPDATE_SHOP_INPUT_DATA(sData.sInputData, 300)
	
	// Grab the current analogue stick positions
	INT iLeftX, iLeftY, iRightX, iRightY
	GET_CONTROL_VALUE_OF_ANALOGUE_STICKS( iLeftX, iLeftY, iRightX, iRightY)
	
	// If we have been browsing with the stick, wait for the timer to pass or stick to be
	// reset before we can use the stick again.
	IF sData.sBrowseInfo.iInputTimer != 0
		IF (TIMERA() > 200)
		OR ((iLeftY > -64) AND (iLeftY < 64) AND (iLeftX > -64) AND (iLeftX < 64))
			sData.sBrowseInfo.iInputTimer = 0
		ELSE
			iLeftX = 0
			iLeftY = 0
		ENDIF
	ENDIF
		
	// Set the input flag states
	BOOL bAccept		 = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
	BOOL bBack			 = (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)) OR SHOULD_BACK_OUT_OF_SHOP_MENU() 
	
	RESET_BACK_OUT_OF_SHOP_MENU_GLOBAL()
							
	// reset the d-pad hold if accept or back has been pressed
	IF (bAccept) OR (bBack)
		sData.sInputData.bDPADUPReset = FALSE
		sData.sInputData.bDPADDOWNReset = FALSE
	ENDIF
	
	BOOL bPrevious	     = ((iLeftY < -64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)) OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sData.sInputData.bDPADUPReset)
	BOOL bNext		     = ((iLeftY > 64) OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)) OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sData.sInputData.bDPADDOWNReset)
	BOOL bUsingLeftStick = ((iLeftY < -64) OR (iLeftY > 64)/* OR (iLeftY < -64) OR (iLeftY > 64)*/)
	BOOL bAllowBrowsing  = (NOT IS_PAUSE_MENU_ACTIVE() AND sData.sBrowseInfo.eStage != SHOP_BROWSE_STAGE_INTRO AND sData.sBrowseInfo.eStage != SHOP_BROWSE_STAGE_OUTRO AND NOT IS_SYSTEM_UI_BEING_DISPLAYED() AND NOT IS_WARNING_MESSAGE_ACTIVE() AND NOT g_sShopSettings.bProcessStoreAlert AND NOT bRebuildMenu)
	
	// B* 2302619
	IF NETWORK_TEXT_CHAT_IS_TYPING()
		bAccept			 = FALSE
		bBack 			 = FALSE
		bPrevious	     = FALSE
		bNext		     = FALSE
		bUsingLeftStick  = FALSE
	ENDIF
	
	
	// Kick off tattoo gun sound
	/*
	IF HAS_SOUND_FINISHED(iTattooGunSound)
		PLAY_SOUND_FRONTEND(iTattooGunSound,"Tattooing","TATTOOIST_SOUNDS")
	ELSE
		IF bTattooGunSound
			IF GET_GAME_TIMER() > iTimerTattooGunSound + 1000
				bTattooGunSound = FALSE
			ENDIF
		ELSE
			//Set tattoo gun sound variable to 0 - idle buzz
			SET_VARIABLE_ON_SOUND(iTattooGunSound,"doTattoo",0)
		ENDIF
	ENDIF
	*/
	//CPRINTLN(DEBUG_SHOPS, "[SHOP]:TATTOO - ALLOW BROWSING - CHECK:", bAllowBrowsing, " PAUSE MENU:", IS_PAUSE_MENU_ACTIVE(), " STAGE:", ENUM_TO_INT(sData.sBrowseInfo.eStage), " UI:", IS_SYSTEM_UI_BEING_DISPLAYED())
	
	RESET_MOUSE_POINTER_FOR_PAUSE_OR_ALERT() // Resets the mouse pointer if a pause screen or alert screen occurs.
	
	IF CHECK_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
		// Process later.
	ELIF bAllowBrowsing
	OR sData.sBrowseInfo.bProcessingBasket
	
		IF IS_PC_VERSION()
		AND bAllowBrowsing
		AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
			HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
		ENDIF
	
		SWITCH sData.sBrowseInfo.iControl
			// Select zone
			CASE 0
			
				//--------------------------------------------------------------------------------
				// MOUSE AND KEYBOARD 
				
				// Mouse select
				IF IS_PC_VERSION()
					IF IS_MENU_CURSOR_ACCEPT_PRESSED()
					
						IF g_iMenuCursorItem = sData.sBrowseInfo.iCurrentGroup
							bAccept = TRUE
						ELSE
							sData.sBrowseInfo.iCurrentGroup = g_iMenuCursorItem
							//IF GET_CURSOR_SELECTED_AVAILABLE_ZONE(sData.sBrowseInfo.iCurrentGroup)
								SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentGroup)
								PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
								sData.sInputData.bDPADUPReset = FALSE
								sData.sInputData.bDPADDOWNReset = FALSE
							//ENDIF
						ENDIF		
					
					ENDIF
					
					// Menu cursor back
					IF IS_MENU_CURSOR_CANCEL_PRESSED()
						bBack = TRUE
						sData.sInputData.bDPADUPReset = FALSE
						sData.sInputData.bDPADDOWNReset = FALSE
					ENDIF
					
					// Mouse scroll up
					IF IS_MENU_CURSOR_SCROLL_UP_PRESSED(sData.sInputData.bDPADUPReset)
						bPrevious = TRUE
					ENDIF
					
					// Mouse scroll down
					IF IS_MENU_CURSOR_SCROLL_DOWN_PRESSED(sData.sInputData.bDPADDownReset)
						bNext = TRUE
					ENDIF
					
				ENDIF
								
				//--------------------------------------------------------------------------------
				
				IF (bPrevious)
					
					IF GET_PREVIOUS_AVAILABLE_ZONE(sData.sBrowseInfo.iCurrentGroup, TRUE)
						SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentGroup)
						// Update the camera
						//GET_TATTOO_SHOP_BROWSE_CAM_DATA(sData.sShopInfo, sData.sBrowseInfo.iCurrentCam, sData.sBrowseInfo.iCurrentGroup, FALSE)
						//UPDATE_SHOP_CAMERA_WITH_INTERP(sData.sShopInfo.sCameras[sData.sBrowseInfo.iCurrentCam], 1000, GRAPH_TYPE_DECEL)					
					ENDIF
					
					// Require the analogue stick to be reset
					IF bUsingLeftStick
						SETTIMERA(0)
						sData.sBrowseInfo.iInputTimer = 1
					ENDIF
					PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
					
					sData.sInputData.bDPADUPReset = FALSE
					sData.sInputData.bLeftStickUDReset = FALSE
					sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
					ENDIF
					
				ELIF (bNext)
					
					IF GET_NEXT_AVAILABLE_ZONE(sData.sBrowseInfo.iCurrentGroup, TRUE)
						SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentGroup)
						// Update the camera
						//GET_TATTOO_SHOP_BROWSE_CAM_DATA(sData.sShopInfo, sData.sBrowseInfo.iCurrentCam, sData.sBrowseInfo.iCurrentGroup, FALSE)
						//UPDATE_SHOP_CAMERA_WITH_INTERP(sData.sShopInfo.sCameras[sData.sBrowseInfo.iCurrentCam], 1000, GRAPH_TYPE_DECEL)	
					ENDIF
					
					// Require the analogue stick to be reset
					IF bUsingLeftStick
						SETTIMERA(0)
						sData.sBrowseInfo.iInputTimer = 1
					ENDIF
					PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
					
					sData.sInputData.bDPADDOWNReset = FALSE
					sData.sInputData.bLeftStickUDReset = FALSE
					sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
					ENDIF
					
				ELIF (bAccept)
					IF sData.sBrowseInfo.iCurrentGroup != -1
						sData.sBrowseInfo.iControl++
						sData.sBrowseInfo.sInfoLabel = ""
						bDLCLabel = FALSE
						//sData.sBrowseInfo.bBrowseTrigger = FALSE // Restore menu position?
						bRebuildMenu = TRUE
						bUpdatePedTattoos = TRUE
						//bRealignCam = TRUE
					ENDIF
					PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
					
				ELIF (bBack)
					
					//SCRIPT_ASSERT("GOING TO OUTRO")
					sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_OUTRO
					bZoomToggle = FALSE
					PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
					
					//bRebuildMenu = TRUE
				ENDIF
			BREAK
			
			// Select tattoo
			CASE 1
				
				IF NOT HAS_PLAYER_PED_VIEWED_TATTOO(sData.eCurrentTattoo)
				AND IS_THIS_TATTOO_UNLOCKED(sData.eCurrentTattoo, "")
					SET_PLAYER_PED_VIEWED_TATTOO(sData.eCurrentTattoo, TRUE)
					sData.sBrowseInfo.bBrowseTrigger = TRUE // Restore menu position?
					//bRebuildMenu = TRUE
					bRebuildMenuNextTime = TRUE
				ENDIF
				
				//--------------------------------------------------------------------------------
				// MOUSE AND KEYBOARD 
				
				IF IS_PC_VERSION()
				AND bAllowBrowsing
					// Mouse select
					IF IS_MENU_CURSOR_ACCEPT_PRESSED()
					
						IF g_iMenuCursorItem = sData.sBrowseInfo.iCurrentItem
							bAccept = TRUE
						ELSE
													
							//IF GET_CURSOR_SELECTED_AVAILABLE_ITEM(sData.eCurrentTattoo )
							IF GET_CURSOR_SELECTED_AVAILABLE_SORTED_ITEM(sData.eCurrentTattoo )
								sData.sBrowseInfo.iCurrentItem = g_iMenuCursorItem
								
								bUpdatePedTattoos = TRUE
								bUpdateHelpKeys = TRUE
								SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
								sData.sBrowseInfo.iCurrentSubItem = GET_TOP_MENU_ITEM()
								
								IF (COUNT_AVAILABLE_ITEMS() > 1)
									PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
								ENDIF
							ENDIF
							
							IF bRebuildMenuNextTime = TRUE
								bRebuildMenu = TRUE
								bRebuildMenuNextTime = FALSE
							ENDIF
							
						ENDIF		
					
					ENDIF
									
					IF IS_MENU_CURSOR_CANCEL_PRESSED()
						bBack = TRUE
					ENDIF
					
					// Mouse scroll up
					IF IS_MENU_CURSOR_SCROLL_UP_PRESSED(sData.sInputData.bDPADUPReset)
						bPrevious = TRUE
					ENDIF
					
					// Mouse scroll down
					IF IS_MENU_CURSOR_SCROLL_DOWN_PRESSED(sData.sInputData.bDPADDownReset)
						bNext = TRUE
					ENDIF
					
				ENDIF
									
				//--------------------------------------------------------------------------------
					
				///////////////////
				// PURCHASE TATTOO
				IF (bAccept)
				OR (sData.sBrowseInfo.bProcessingBasket)
				
					IF sData.eCurrentTattoo != INVALID_TATTOO
						IF NOT HAS_PLAYER_PED_GOT_TATTOO(sData.eCurrentTattoo)
							IF NOT IS_THIS_TATTOO_UNLOCKED(sData.eCurrentTattoo, "")
								PLAY_SOUND_FRONTEND(-1,"ERROR","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
								SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_LOCK", 4000)
								CPRINTLN(DEBUG_SHOPS, "\n TATTOO_BROWSE: locked (NOT IS_THIS_TATTOO_UNLOCKED)!")
							ELIF PURCHASE_TATTOO_ITEM() AND NOT sData.sBrowseInfo.bProcessingBasket
								
								// Set tattoo gun sound variable to 1 - applying the tattoo
								/*
								IF NOT HAS_SOUND_FINISHED(iTattooGunSound) 
									SET_VARIABLE_ON_SOUND(iTattooGunSound,"doTattoo",1)
									bTattooGunSound = TRUE
									iTimerTattooGunSound = GET_GAME_TIMER()
								ENDIF
								*/
								PLAY_SOUND_FRONTEND(-1,"Tattooing_Oneshot","TATTOOIST_SOUNDS")
								PLAY_SOUND_FRONTEND(-1,"PURCHASE","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
								
								REMOVE_TATTOOS_IN_SAME_UPGRADE_GROUP(sData.eCurrentTattoo)
								
								IF g_bInMultiplayer
									SET_MP_TATTOO_CURRENT(sData.eCurrentTattoo, TRUE)
									SET_MP_TATTOO_PURCHASED(sData.eCurrentTattoo, TRUE)
								ELSE
									SET_PED_TATTOO_CURRENT(GET_CURRENT_PLAYER_PED_ENUM(), sData.eCurrentTattoo, TRUE)
								ENDIF
								
								IF NETWORK_IS_GAME_IN_PROGRESS()
								//	IF IS_MP_TATTOO_CURRENT(TATTOO_MP_FM_HEAD_BANGER)
									IF HAS_PLAYER_GOT_TATTOOS_FOR_HUMAN_CANVAS_AWARD()
										PRINT_SHOP_DEBUG_LINE("HUMAN CANVAS")
										SET_MP_BOOL_CHARACTER_AWARD(MP_AWARD_FMTATTOOALLBODYPARTS, TRUE)
									ENDIF
									
									REQUEST_SYSTEM_ACTIVITY_TYPE_GOT_A_TATTOO()
								ENDIF
								
								IF NETWORK_IS_GAME_IN_PROGRESS()
									ADD_TATTOOS_NO_OVERLAP(pedID_ShopPed, sData.eCurrentTattoo, TRUE)
								ELSE
									REMOVE_TATTOOS_NOT_PURCHASED(pedID_ShopPed)
								ENDIF
								
								g_bTriggerShopSave = TRUE
								bDidPlayerHaveCoupon = FALSE
								
								// Make sure we update the player aswell here
								IF pedID_ShopPed != PLAYER_PED_ID()
									IF NETWORK_IS_GAME_IN_PROGRESS()
										ADD_TATTOOS_NO_OVERLAP(PLAYER_PED_ID(), sData.eCurrentTattoo, TRUE)
									ELSE
										REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
									ENDIF
								ENDIF
								
								SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, TATT_DLG_WORK, DLG_PRIORITY_HIGH)
								
								//SETUP_SHOP_INFO_LABEL(sData.sBrowseInfo, "TAT_PURCH")
								IF NETWORK_IS_GAME_IN_PROGRESS()
								AND sData.eCurrentTattoo >= TATTOO_MP_FM_DLC
									sData.sBrowseInfo.sInfoLabel = "TAT_BUYDLC"
									bDLCLabel = TRUE
								ELSE
									sData.sBrowseInfo.sInfoLabel = "TAT_BUY"
									bDLCLabel = FALSE
								ENDIF
								sData.sBrowseInfo.bBrowseTrigger = TRUE // Restore menu position?
								//bRebuildMenu = TRUE
								bRebuildMenuNextFrame = TRUE
								bRebuildMenuNextTime = FALSE // Add to fix B*1411678 - Why this works i don't know, i'm stepping well away from this
							ELIF NOT sData.sBrowseInfo.bProcessingBasket
								IF NOT IS_STRING_NULL_OR_EMPTY(sInvalidCatalogueKey)
									PLAY_SOUND_FRONTEND(-1,"ERROR","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
									SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_LOCK", 4000)
									CPRINTLN(DEBUG_SHOPS, "\n TATTOO_BROWSE: locked (sInvalidCatalogueKey: \"", sInvalidCatalogueKey, "\")!")
									
								ELIF sData.sBrowseInfo.bPurchaseFailed
									PLAY_SOUND_FRONTEND(-1,"ERROR","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
									SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_LOCK", 4000)
									CPRINTLN(DEBUG_SHOPS, "\n TATTOO_BROWSE: transaction fail!")
									
								ELIF g_bTriggerTransactionBlockedAlert
									PLAY_SOUND_FRONTEND(-1,"ERROR","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
									SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_LCKPC", 4000)
									CPRINTLN(DEBUG_SHOPS, "\n TATTOO_BROWSE: transaction blocked!")
									
								ELSE
									// ... cannot afford
									//SETUP_SHOP_INFO_LABEL(sData.sBrowseInfo, "TAT_AFFORD")
									PLAY_SOUND_FRONTEND(-1,"ERROR","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
									SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_AFF", 4000)
									
									STORE_LAST_VIEWED_SHOP_ITEM_FOR_COMMERCE_STORE(GET_HASH_KEY(g_sShopSettings.tlCurrentItem_label), g_sShopSettings.iCurrentItem_cost, GET_HASH_KEY(GET_SHOP_NAME(sData.sShopInfo.eShop)))
									LAUNCH_STORE_CASH_ALERT(FALSE, STAT_SAVETYPE_END_SHOPPING, SPL_STORE)
									
									CPRINTLN(DEBUG_SHOPS, "\n TATTOO_BROWSE: cannot afford!")
								ENDIF
							ENDIF
						ELSE
							// Allow Lost gang to remove current patches
							//IF PURCHASE_TATTOO_ITEM()
							
								IF g_bInMultiplayer
									SET_MP_TATTOO_CURRENT(sData.eCurrentTattoo, FALSE)
								ELSE
									SET_PED_TATTOO_CURRENT(GET_CURRENT_PLAYER_PED_ENUM(), sData.eCurrentTattoo, FALSE)
								ENDIF
								
								PLAY_SOUND_FRONTEND(-1,"Tattooing_Oneshot_Remove","TATTOOIST_SOUNDS")
								//PLAY_SOUND_FRONTEND(-1,"PURCHASE","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
								
								// Refresh
								REMOVE_TATTOOS_NOT_PURCHASED(pedID_ShopPed)
								
								g_bTriggerShopSave = TRUE
								bDidPlayerHaveCoupon = FALSE
								
								// Make sure we update the player aswell here
								IF pedID_ShopPed != PLAYER_PED_ID()
									REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
								ENDIF
								
								//SETUP_SHOP_INFO_LABEL(sData.sBrowseInfo, "TAT_PURCH")
								
								IF NETWORK_IS_GAME_IN_PROGRESS()
								AND sData.eCurrentTattoo >= TATTOO_MP_FM_DLC
									sData.sBrowseInfo.sInfoLabel = "TAT_REMDLC"
									bDLCLabel = TRUE
								ELSE
									sData.sBrowseInfo.sInfoLabel = "TAT_REM"
									bDLCLabel = FALSE
								ENDIF
								sData.sBrowseInfo.bBrowseTrigger = TRUE // Restore menu position?
								bRebuildMenu = TRUE
							//ELSE
								// ... cannot afford
								//SETUP_SHOP_INFO_LABEL(sData.sBrowseInfo, "TAT_AFFORD")
							//	PLAY_SOUND_FRONTEND(-1,"ERROR","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
							//	SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_AFF_REM", 4000)
							//	CPRINTLN(DEBUG_SHOPS, "\n TATTOO_BROWSE: cannot afford to remove!")
							//ENDIF
						ENDIF
					ENDIF
				
					bZoomToggle = FALSE

				////////////////////
				// SELECT NEXT ITEM
				ELIF (bNext)
					
					//CPRINTLN(DEBUG_SHOPS, "[TATTOO]: NEXT PRESSED - AVAILABLE ITEMS:", COUNT_AVAILABLE_ITEMS())
					IF GET_NEXT_AVAILABLE_SORTED_ITEM(sData.eCurrentTattoo, sData.eCurrentTattoo, sData.sBrowseInfo.iCurrentItem, TRUE)
						bUpdatePedTattoos = TRUE
						bUpdateHelpKeys = TRUE
						SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
						sData.sBrowseInfo.iCurrentSubItem = GET_TOP_MENU_ITEM()
						
						IF (COUNT_AVAILABLE_ITEMS() > 1)
							PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
						ENDIF
						//bRealignCam = TRUE
					ENDIF
					
					// Require the analogue stick to be reset
					IF bUsingLeftStick
						SETTIMERA(0)
						sData.sBrowseInfo.iInputTimer = 1
					ENDIF
					
					IF bRebuildMenuNextTime = TRUE
						bRebuildMenu = TRUE
						bRebuildMenuNextTime = FALSE
					ENDIF
					
					sData.sInputData.bDPADDOWNReset = FALSE
					sData.sInputData.bLeftStickUDReset = FALSE
					sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
					ENDIF
			
				////////////////////////
				// SELECT PREVIOUS ITEM
				ELIF (bPrevious)
					
					//CPRINTLN(DEBUG_SHOPS, "[TATTOO]: NEXT PRESSED - AVAILABLE ITEMS:", COUNT_AVAILABLE_ITEMS())
					IF GET_PREVIOUS_AVAILABLE_SORTED_ITEM(sData.eCurrentTattoo, sData.eCurrentTattoo, sData.sBrowseInfo.iCurrentItem, TRUE)
						bUpdatePedTattoos = TRUE
						bUpdateHelpKeys = TRUE
						SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
						sData.sBrowseInfo.iCurrentSubItem = GET_TOP_MENU_ITEM()
						
						IF (COUNT_AVAILABLE_ITEMS() > 1)
							PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
						ENDIF
						//bRealignCam = TRUE
					ENDIF

					// Require the analogue stick to be reset
					IF bUsingLeftStick
						SETTIMERA(0)
						sData.sBrowseInfo.iInputTimer = 1
					ENDIF
					
					IF bRebuildMenuNextTime = TRUE
						bRebuildMenu = TRUE
						bRebuildMenuNextTime = FALSE
					ENDIF
					
					sData.sInputData.bDPADUPReset = FALSE
					sData.sInputData.bLeftStickUDReset = FALSE
					sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
					ENDIF
			
				///////////////
				// RETURN
				ELIF (bBack)
					// Remove items that we were testing out
					bRebuildMenu = TRUE
					
					sData.eCurrentTattoo = INVALID_TATTOO
					
					PREPARE_PLAYER_FOR_TATTOO(FALSE)
					REMOVE_TATTOOS_NOT_PURCHASED(pedID_ShopPed)
					
					// Make sure we update the player aswell here
					IF pedID_ShopPed != PLAYER_PED_ID()
						REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
					ENDIF
					PLAY_SOUND_FRONTEND(-1,"BACK","HUD_FRONTEND_TATTOO_SHOP_SOUNDSET")
					
					fCamRotAlpha = 0.0
					sData.sBrowseInfo.bBrowseTrigger = TRUE // Restore menu position?
					sData.sBrowseInfo.iControl--
					//CPRINTLN(DEBUG_SHOPS, "[SHOP]:TATTOO - BACK PRESSED - CONTROL:", sData.sBrowseInfo.iControl)
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	
	// If there has been a change, rebuild help and update the stored flags
	IF bRebuildMenu
#IF IS_DEBUG_BUILD	
	OR bRebuildMenuForDebug
#ENDIF
		IF NOT bRebuildMenuNextFrame
			BUILD_TATTOO_SHOP_MENU(bUpdatePedTattoos)
		ENDIF
	ENDIF
	
	SETUP_CURRENT_TATTOO_DESCRIPTION()
	
	////////////////////////////////////////////////////////////////////////////////////////////////
	///     DISPLAY HUD COMPONENTS
	///    
	SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
	SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
	SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
	SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
	
	IF NETWORK_IS_GAME_IN_PROGRESS() AND sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_OUTRO
		REMOVE_MULTIPLAYER_WALLET_CASH()  //Remove cash on hud
		REMOVE_MULTIPLAYER_BANK_CASH()
	ENDIF
	
	// Update help keys
	IF bUpdateHelpKeys
	
		REMOVE_MENU_HELP_KEYS()
		
		IF (sData.sBrowseInfo.iControl = 0)
			IF sData.sBrowseInfo.iCurrentGroup != -1
				ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
			ENDIF
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
			IF sData.sBrowseInfo.iCurrentGroup != -1
				//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD, "ITEM_SCROLL")
			ENDIF
			ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
			ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
			ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
			//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_RT, "ITEM_FSCROLL")
			
		ELIF (sData.sBrowseInfo.iControl = 1)
			IF sData.sBrowseInfo.iCurrentItem != -1
				IF HAS_PLAYER_PED_GOT_TATTOO(sData.eCurrentTattoo)
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_REM")
				ELIF NETWORK_IS_GAME_IN_PROGRESS()
				AND IS_MP_TATTOO_PURCHASED(sData.eCurrentTattoo)
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
				ELSE
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_BUY")
				ENDIF
			ENDIF
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
			IF sData.sBrowseInfo.iCurrentItem != -1
				//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD, "ITEM_SCROLL")
			ENDIF
			ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
			ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
			ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
			//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_RT, "ITEM_FSCROLL")
		ENDIF
		
		bUpdateHelpKeys = FALSE
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF (sData.sBrowseInfo.iControl != 0)
			
			TATTOO_DATA_STRUCT sTattooHeadcheck	
			GET_TATTOO_DATA(sTattooHeadcheck, sData.eCurrentTattoo, GET_TATTOO_FACTION_FOR_PED(pedID_ShopPed), pedID_ShopPed)
			
			TEXT_LABEL_15 tlDescLabel
			GET_CURRENT_MENU_ITEM_DESCRIPTION(tlDescLabel)
			IF GET_HASH_KEY(tlDescLabel) = 0
				IF sData.eCurrentTattoo >= TATTOO_MP_FM_DLC
					TEXT_LABEL_15 tlCustomLabel
					IF sTattooHeadcheck.iUpgradeGroup = HASH("HEAD_RIGHT")
					OR sTattooHeadcheck.iUpgradeGroup = HASH("HEAD_LEFT")
						SET_CURRENT_MENU_ITEM_DESCRIPTION("BB_PACK_HEADTAT")
					ELIF IS_THIS_LABEL_A_CREW_TATTOO(sTattooHeadcheck.sLabel)
						SET_CURRENT_MENU_ITEM_DESCRIPTION("TAT_LCKCREW")
					ELIF GET_SPECIAL_LABEL_DESCRIPTION_FOR_SHOP_ITEM(sTattooHeadcheck.sLabel, tlCustomLabel)
						SET_CURRENT_MENU_ITEM_DESCRIPTION(tlCustomLabel)
					ELIF NOT IS_STRING_NULL_OR_EMPTY(GET_SHOP_CONTENT_FOR_MENU(sTattooHeadcheck.sLabel))
						IF SHOULD_TATTOO_BE_FREE_FOR_PLAYER(sTattooHeadcheck.sLabel)
							#IF NOT FEATURE_GEN9_EXCLUSIVE
							IF IS_SHOP_CONTENT_FROM_PACK(sTattooHeadcheck.sLabel)
								SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_CES_PACK")
								ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooHeadcheck.sLabel))
							ELIF IS_SHOP_CONTENT_FROM_SPECIAL(sTattooHeadcheck.sLabel)
								SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_CES_PACK3")
								ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooHeadcheck.sLabel))
							ELIF IS_SHOP_CONTENT_FROM_SURPRISE(sTattooHeadcheck.sLabel)
								SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_CES_PACK4")
								ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooHeadcheck.sLabel))
							ELIF IS_SHOP_CONTENT_FROM_BASIC(sTattooHeadcheck.sLabel)
								SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_CES_PACK6")
								ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooHeadcheck.sLabel))
							ELIF IS_SHOP_CONTENT_FROM_HALLOWEEN_SURPRISE(sTattooHeadcheck.sLabel)
								SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_CES_PACK7")
							ELSE
								SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_CES_PACK2")
								ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooHeadcheck.sLabel, TRUE))
							ENDIF
							#ENDIF
						ELSE
							IF IS_SHOP_CONTENT_FROM_PACK(sTattooHeadcheck.sLabel)
								SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK")
								ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooHeadcheck.sLabel))
							ELIF IS_SHOP_CONTENT_FROM_SPECIAL(sTattooHeadcheck.sLabel)
								SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK3")
								ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooHeadcheck.sLabel))
							ELIF IS_SHOP_CONTENT_FROM_SURPRISE(sTattooHeadcheck.sLabel)
								SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK4")
								ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooHeadcheck.sLabel))
							ELIF IS_SHOP_CONTENT_FROM_BASIC(sTattooHeadcheck.sLabel)
								SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK6")
								ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooHeadcheck.sLabel))
							ELIF IS_SHOP_CONTENT_FROM_HALLOWEEN_SURPRISE(sTattooHeadcheck.sLabel)
								SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK7")
							ELIF IS_SHOP_CONTENT_FROM_PLATFORM(sTattooHeadcheck.sLabel)
								SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK8")
							ELSE
								SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK2")
								ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sTattooHeadcheck.sLabel, TRUE))
							ENDIF
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
			
			IF NOT IS_THIS_TATTOO_UNLOCKED(sData.eCurrentTattoo, "")
			ELSE
				PED_DECORATION_ZONE eZone = PDZ_INVALID
				TORSO_DECORATION_SUBZONE eSubzone = TDS_INVALID
				GET_PED_DECORATION_ZONE_FROM_INT(sData.sBrowseInfo.iCurrentGroup, eZone, eSubzone)
				
				TEXT_LABEL tlDisc = ""
				GET_CURRENT_MENU_ITEM_DISCOUNT(tlDisc)
				IF GET_HASH_KEY(tlDisc) = 0
				AND SHOULD_DISCOUNT_BE_DISPLAYED_FOR_TATTOO(sData.eCurrentTattoo, sTattooHeadcheck.sLabel, eZone, eSubzone)
					SET_CURRENT_MENU_ITEM_DISCOUNT("TAT_SALE")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF PROCESS_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
		// Processing.
	ELSE
		DRAW_MENU(TRUE, ENUM_TO_INT(sData.sShopInfo.eShop))
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()	
		IF g_sMPTunables.b_onsale_tattoo_shop
			IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPShops")
				FLOAT texW, texH
			    IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_HEADER, TRUE, TRUE, texW, texH)
			        SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP) 
			        SET_SCRIPT_GFX_ALIGN_PARAMS(0.0, 0.0, 0.0, 0.0)
			        DRAW_SPRITE("MPShops", "ShopUI_Title_Graphics_SALE", 0.112, 0.045, CUSTOM_MENU_W, texH, 0.0, 255, 255, 255, 255)
			        RESET_SCRIPT_GFX_ALIGN() 
			    ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD	
		bRebuildMenuForDebug = FALSE
	#ENDIF
ENDPROC

FUNC BOOL IS_PRELOAD_DONE()
	IF NETWORK_IS_GAME_IN_PROGRESS()
	OR HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())
		RETURN TRUE
	ENDIF
RETURN FALSE
ENDFUNC

/// PURPOSE: Sets the player into postion and displays the shop hud
PROC DO_BROWSE_ITEMS()

	IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			CLEAR_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
			SET_TRANSITION_SESSION_SHOP_STATE_INITIALISED()
		ELIF NOT HAS_TRANSITION_SESSION_SHOP_STATE_INITIALISED()
			SET_TRANSITION_SESSION_SHOP_STATE_INITIALISED()
		ENDIF
		PRINTLN("DO_BROWSE_ITEMS - waiting for player to enter game...")
		EXIT
	ENDIF
	
	VECTOR vCurrentPos
	FLOAT fCurrentHeading
	VECTOR vTempPosKeeper

	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
	ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
	
	// Check to see if it is no longer safe to browse
	UPDATE_SHOP_BROWSE_BAIL_STATE(sData.sShopInfo, sData.sBrowseInfo)

	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND DOES_ENTITY_EXIST(sData.sShopInfo.sShopKeeper.pedID)
	AND NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
		SET_ENTITY_VISIBLE_IN_CUTSCENE(sData.sShopInfo.sShopKeeper.pedID, TRUE, TRUE)
	ENDIF
	
	IF sData.sBrowseInfo.eStage > SHOP_BROWSE_STAGE_INIT
	AND sData.sBrowseInfo.eStage < SHOP_BROWSE_STAGE_OUTRO	
		HIDE_HUD_ITEMS_THIS_FRAME_FOR_SHOP_CUTSCENE(FALSE)
		THEFEED_HIDE_THIS_FRAME()
	ENDIF
	
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	BOOL bUpdatePedTattoos = FALSE
			
	SWITCH sData.sBrowseInfo.eStage
		CASE SHOP_BROWSE_STAGE_INIT
		
			sBuddyHideData.buddyHide = FALSE
			sBuddyHideData.buddyInit = FALSE
			
			IF IS_SHOP_PED_OK()
			
				sData.sBrowseInfo.bBrowsing = TRUE
				
				REQUEST_SCRIPT_AUDIO_BANK("HUD_TATTOO_SHOP")
				
				IF LOAD_MENU_ASSETS("TAT_MNU", ENUM_TO_INT(sData.sShopInfo.eShop), TRUE)
				AND LOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
				AND LOAD_TATTOO_CUTSCENE_ASSETS()
				AND IS_PRELOAD_DONE()
					
					// Take off as much clothes as we can so we can see the tattoos.
					// Note: We will need to get some special outfit that the player can wear
					// 		 when getting a tattoo.
					// In multiplayer we set the first item when we create the clone ped, so only set sp here
					IF NOT g_bInMultiplayer
						PREPARE_PLAYER_FOR_TATTOO(TRUE)
					ENDIF
					
					sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_INTRO
				ELSE
					PRINTLN("DO_BROWSE_ITEMS - waiting for tatoo assets to load")
					PRINTLN("...LOAD_MENU_ASSETS = ", LOAD_MENU_ASSETS("TAT_MNU", ENUM_TO_INT(sData.sShopInfo.eShop), TRUE))
					PRINTLN("...LOAD_SHOP_INTRO_ASSETS = ", LOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop))
					PRINTLN("...LOAD_TATTOO_CUTSCENE_ASSETS = ", LOAD_TATTOO_CUTSCENE_ASSETS())
					PRINTLN("...REQUEST_SCRIPT_AUDIO_BANK = ", REQUEST_SCRIPT_AUDIO_BANK("HUD_TATTOO_SHOP"))
					PRINTLN("...IS_PRELOAD_DONE = ", IS_PRELOAD_DONE())
				ENDIF
			ENDIF
		BREAK
		
		CASE SHOP_BROWSE_STAGE_INTRO
		
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_DISPLAY_FIRST_TATTOO_UNLOCK_HELP)
				SET_PACKED_STAT_BOOL(PACKED_MP_DISPLAY_FIRST_TATTOO_UNLOCK_HELP, TRUE)
			ENDIF
			
			SET_BIT(g_iRepeatPlayBits, ENUM_TO_INT(RPB_PLAYER_BROWSING_TATTOO_SHOP))
			
			SET_PLAYER_IS_CHANGING_CLOTHES(TRUE)
			
			// Remove player control
			LOCK_PLAYER_FOR_BROWSE(sData.sBrowseInfo)
			SETUP_PLAYER_FOR_MP_SHOP_CUTSCENE(FALSE, TRUE)
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
				SET_MULTIPLAYER_WALLET_CASH()  //Display cash on hud
				SET_MULTIPLAYER_BANK_CASH()
			ENDIF

			IF IS_ANY_VEHICLE_NEAR_POINT(sData.sLocateInfo.vPlayerBrowsePos,3)	
			AND NOT g_bInMultiplayer		
				REPOSITION_PLAYER_LAST_VEHICLE_FOR_SHOP(sData.sShopInfo)
			ENDIF
			
			IF NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
				vTempPosKeeper = GET_ENTITY_COORDS(sData.sShopInfo.sShopKeeper.pedID)
				sData.sLocateInfo.vPlayerBrowsePos.z = vTempPosKeeper.z
			ENDIF
			
			// Force the player into the required position
			IF NOT g_bInMultiplayer
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				vCurrentPos = GET_ENTITY_COORDS(PLAYER_PED_ID())     //Don't bother warping if the player is in the right place
				IF vCurrentPos.x < sData.sLocateInfo.vPlayerBrowsePos.x - 0.1
				OR vCurrentPos.x > sData.sLocateInfo.vPlayerBrowsePos.x + 0.1
				OR vCurrentPos.y < sData.sLocateInfo.vPlayerBrowsePos.y - 0.1
				OR vCurrentPos.y > sData.sLocateInfo.vPlayerBrowsePos.y + 0.1		
					SET_ENTITY_COORDS_NO_OFFSET(PLAYER_PED_ID(), sData.sLocateInfo.vPlayerBrowsePos)
					CLEAR_AREA_OF_OBJECTS(sData.sLocateInfo.vPlayerBrowsePos,1.1)
				ENDIF
				fCurrentHeading = GET_ENTITY_HEADING(PLAYER_PED_ID())
				IF fCurrentHeading < sData.sLocateInfo.fPlayerBrowseHead - 2
				OR fCurrentHeading > sData.sLocateInfo.fPlayerBrowseHead + 2
					SET_ENTITY_HEADING(PLAYER_PED_ID(), sData.sLocateInfo.fPlayerBrowseHead)
				ENDIF
				FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
				DISABLE_ON_FOOT_FIRST_PERSON_VIEW_THIS_UPDATE()
				sBuddyHideData.buddyHide = TRUE
			ENDIF
			
			// Make sure the cutscene ped is visible
			IF DOES_ENTITY_EXIST(sCutsceneData.pedID)
				SET_ENTITY_VISIBLE(sCutsceneData.pedID, TRUE)
			ENDIF
			
			vStoredPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID())
			IF DOES_ENTITY_EXIST(sCutsceneData.pedID)
				IF NOT IS_PED_INJURED(sCutsceneData.pedID)
					vStoredPlayerCoords = GET_ENTITY_COORDS(sCutsceneData.pedID)
				ENDIF
			ENDIF
			
			IF eBaseShop <> TATTOO_PARLOUR_01_HW
				// Move the shopkeeper back to the idle position to prevent them interfering with the cutscene cameras.
				IF DOES_ENTITY_EXIST(sData.sShopInfo.sShopKeeper.pedID)
					IF NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
						vCurrentPos = GET_ENTITY_COORDS(sData.sShopInfo.sShopKeeper.pedID)     //Don't bother warping if the shopkeeper is in the right place
						IF vCurrentPos.x < sData.sShopInfo.sShopKeeper.vPosIdle.x - 0.1
						OR vCurrentPos.x > sData.sShopInfo.sShopKeeper.vPosIdle.x + 0.1
						OR vCurrentPos.y < sData.sShopInfo.sShopKeeper.vPosIdle.y - 0.1
						OR vCurrentPos.y > sData.sShopInfo.sShopKeeper.vPosIdle.y + 0.1			
							CLEAR_PED_TASKS(sData.sShopInfo.sShopKeeper.pedID)
							SET_ENTITY_COORDS_NO_OFFSET(sData.sShopInfo.sShopKeeper.pedID, sData.sShopInfo.sShopKeeper.vPosIdle )
							IF eBaseShop = TATTOO_PARLOUR_01_HW	
								SET_ENTITY_HEADING(sData.sShopInfo.sShopKeeper.pedID, sData.sShopInfo.sShopKeeper.fHeadIdle)
							ELSE
								SET_ENTITY_HEADING(sData.sShopInfo.sShopKeeper.pedID, sData.sShopInfo.sShopKeeper.fHeadIdle - 180)
							ENDIF
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sData.sShopInfo.sShopKeeper.pedID)
							TASK_LOOK_AT_ENTITY(sData.sShopInfo.sShopKeeper.pedID, PLAYER_PED_ID(), -1, SLF_WHILE_NOT_IN_FOV, SLF_LOOKAT_VERY_HIGH)
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
			
			START_SHOP_CUTSCENE(sData.sShopInfo,1)
			
			// Reset some flags
			sData.sBrowseInfo.iControl = 0
			sData.sBrowseInfo.iCurrentItem = 0
			sData.sBrowseInfo.iCurrentGroup = 0
			sData.sBrowseInfo.iCurrentCam = 0
			//sData.sBrowseInfo.iTriggerTime = -1
			sData.sBrowseInfo.iInputTimer = 0
			sData.sBrowseInfo.bDisplayInfo = FALSE
			sData.sBrowseInfo.sInfoLabel = ""
			bDLCLabel = FALSE
			sData.sBrowseInfo.bBrowseTrigger = FALSE
			
			IF bTransitionRestoreMenu
				sData.sBrowseInfo.iCurrentGroup = g_sTransitionSessionData.sEndReserve.iShopMenuRestore[1]
				IF g_sTransitionSessionData.sEndReserve.bRestoreSubMenu
					sData.sBrowseInfo.iControl = 1
					sData.eCurrentTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, g_sTransitionSessionData.sEndReserve.iShopMenuRestore[0])
					sData.sBrowseInfo.iCurrentItem = g_sTransitionSessionData.sEndReserve.iShopMenuRestore[2]
					g_sTransitionSessionData.sEndReserve.bRestoreSubMenu = FALSE
					sData.sBrowseInfo.bBrowseTrigger = TRUE
					bTransitionRestoreMenu = FALSE
				ENDIF
			ENDIF
			
			BUILD_TATTOO_SHOP_MENU(bUpdatePedTattoos)
			BROWSE_TATTOOS_AND_DRAW_HUD(bUpdatePedTattoos)
			
			SET_CURSOR_POSITION_FOR_MENU()
			
			// Create the browse cam on this frame
			UPDATE_TATTOO_BROWSE_CAM()
			
			IF NOT g_bInMultiplayer
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())	
				RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
				RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
				PRELOAD_STORED_CURRENT_PED_COMPONENTS()
			ENDIF
			
			sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_BROWSING

		BREAK
		
		CASE SHOP_BROWSE_STAGE_BROWSING
			
			BROWSE_TATTOOS_AND_DRAW_HUD(bUpdatePedTattoos)
			UPDATE_TATTOO_BROWSE_CAM()
			UPDATE_TATTOO_BROWSE_ANIM()
			
			// Disable combat stance if the player is in combat.
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PED_RESET_FLAG( PLAYER_PED_ID(),PRF_DisableActionMode, true)
			ENDIF
		BREAK
		
		CASE SHOP_BROWSE_STAGE_OUTRO
			IF NOT bDoFinaliseHeadBlend
			
				g_sShopSettings.bPreviewTattooMode = FALSE
				
				// Reset variations
				IF NOT g_bInMultiplayer
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())	
					RESTORE_CURRENT_PED_COMPONENTS()
					RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
					RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
				ENDIF
				
				sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_CLEANUP
			ELSE
				
				BROWSE_TATTOOS_AND_DRAW_HUD(bUpdatePedTattoos)

			ENDIF
		BREAK
		
		CASE SHOP_BROWSE_STAGE_CLEANUP
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
				REMOVE_MULTIPLAYER_WALLET_CASH()  //Remove cash on hud
				REMOVE_MULTIPLAYER_BANK_CASH()
			ENDIF
				
			// Return to gameplay camera
			DEACTIVATE_SHOP_CAMERAS(sData.sShopInfo.sCameras)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			// Return player control
			UNLOCK_PLAYER_FOR_BROWSE(sData.sBrowseInfo)
			
			CLEANUP_PLAYER_FOR_MP_SHOP_CUTSCENE()
		
			END_SHOP_CUTSCENE()
	
			CLEANUP_MENU_ASSETS(TRUE, ENUM_TO_INT(sData.sShopInfo.eShop))
			UNLOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
			
			RELEASE_SCRIPT_AUDIO_BANK()
			
			CLEANUP_TATTOO_CUTSCENE_ASSETS()
			
			sBuddyHideData.buddyHide = FALSE
			
			IF g_bTriggerShopSave
				IF g_bInMultiplayer
					// KGM 11/12/12: Tell all players to re-Generate a player headshot after appearance has changed
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
				ENDIF
			ENDIF
			
			
			//Reapply the players shirt decal as it has been removed on initialising the tatto shop.
			IF g_bInMultiplayer
				PED_COMP_NAME_ENUM eCurrentJbib
				eCurrentJbib = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_JBIB)
				EQUIP_TORSO_DECALS_MP(PLAYER_PED_ID(), COMP_TYPE_JBIB, eCurrentJbib, FALSE)
				IF sData.eCrewLogoTattoo != INVALID_TATTOO
					SET_MP_TATTOO_CURRENT(sData.eCrewLogoTattoo, TRUE)
				ENDIF
			ENDIF
			
			//Reapply the players forced hair stats
//			SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO, iStoredHair, g_iPedComponentSlot)
//			SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_HAIR_ITEM, iForcedHairItem, g_iPedComponentSlot)
//			SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_HAIR_TYPE, iForcedHairType, g_iPedComponentSlot)

			// Remove items that we were testing out
			REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
			UNHIDE_BLOOD()
			
			SET_PLAYER_IS_CHANGING_CLOTHES(FALSE)
			
			CLEAR_BIT(g_iRepeatPlayBits, ENUM_TO_INT(RPB_PLAYER_BROWSING_TATTOO_SHOP))
			
			sData.sBrowseInfo.bBrowsing = FALSE
			sData.sShopInfo.eStage = SHOP_STAGE_WAIT_FOR_TRIGGER
			
			#IF FEATURE_TUNER
			IF eInShop = TATTOO_PARLOUR_07_CCT
				STOP_AUDIO_SCENE("Ls_Car_Meet_Tattoo_Shop_Scene")
			ENDIF
			#ENDIF
		BREAK
		
		CASE SHOP_BROWSE_STAGE_BAIL
			// Something went wrong when the player was browsing so we need to
			// bail out of the browsing process.
			
			// NOTE: Commands will be getting added to check cuffed state so we can
			// 		 tell if the player should be given control back.
			
			// Return to gameplay camera
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
				REMOVE_MULTIPLAYER_WALLET_CASH()  //Stop displaying cash on hud
				REMOVE_MULTIPLAYER_BANK_CASH()
			ENDIF
			
			DEACTIVATE_SHOP_CAMERAS(sData.sShopInfo.sCameras)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			//Set multihead blinders off
			SET_MULTIHEAD_SAFE(FALSE,TRUE)
			
			// Return player control
			UNLOCK_PLAYER_FOR_BROWSE(sData.sBrowseInfo)
			
			g_sShopSettings.bPreviewTattooMode = FALSE
			
			IF NOT g_bInMultiplayer	
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())	
				RESTORE_CURRENT_PED_COMPONENTS()
				RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
				RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
			ENDIF
			
			CLEANUP_PLAYER_FOR_MP_SHOP_CUTSCENE()
			END_SHOP_CUTSCENE()
			
			CLEANUP_MENU_ASSETS(TRUE, ENUM_TO_INT(sData.sShopInfo.eShop))
			UNLOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
			
			RELEASE_SCRIPT_AUDIO_BANK()
			
			sBuddyHideData.buddyHide = FALSE
			
			CLEANUP_TATTOO_CUTSCENE_ASSETS()

			//Reapply the players shirt decal as it has been removed on initialising the tattoo shop.
			IF g_bInMultiplayer
				PED_COMP_NAME_ENUM eBailCurrentJbib
				eBailCurrentJbib = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_JBIB)
				EQUIP_TORSO_DECALS_MP(PLAYER_PED_ID(), COMP_TYPE_JBIB, eBailCurrentJbib, FALSE)
				IF sData.eCrewLogoTattoo != INVALID_TATTOO
					SET_MP_TATTOO_CURRENT(sData.eCrewLogoTattoo, TRUE)
				ENDIF
			ENDIF
			
			//Reapply the players forced hair stats
//			SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO, iStoredHair, g_iPedComponentSlot)
//			SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_HAIR_ITEM, iForcedHairItem, g_iPedComponentSlot)
//			SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_FORCED_HAIR_TYPE, iForcedHairType, g_iPedComponentSlot)

			// Remove items that we were testing out
			REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
			
			
			SET_PLAYER_IS_CHANGING_CLOTHES(FALSE)
			
			UNHIDE_BLOOD()
			
			IF g_bTriggerShopSave
				IF g_bInMultiplayer
					// KGM 11/12/12: Tell all players to re-Generate a player headshot after appearance has changed
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
				ENDIF
			ENDIF
			
			CLEAR_BIT(g_iRepeatPlayBits, ENUM_TO_INT(RPB_PLAYER_BROWSING_TATTOO_SHOP))
			
			sData.sBrowseInfo.bBrowsing = FALSE
			sData.sShopInfo.eStage = SHOP_STAGE_WAIT_FOR_TRIGGER
		BREAK
	ENDSWITCH
	
	
	// If we changed item selection, update the player ped tattoos
	IF bUpdateTattoosThisFrame
		// If the cutscene ped exists, set the items on them, otherwise set it on the player
		PED_INDEX pedID_ShopPed = PLAYER_PED_ID()
		IF DOES_ENTITY_EXIST(sCutsceneData.pedID)
		AND NOT IS_PED_INJURED(sCutsceneData.pedID)
			pedID_ShopPed = sCutsceneData.pedID
		ENDIF
		
		PREPARE_PLAYER_FOR_TATTOO(FALSE)
		IF NETWORK_IS_GAME_IN_PROGRESS()	
			ADD_TATTOOS_NO_OVERLAP(pedID_ShopPed, sData.eCurrentTattoo)
		ELSE	
			REMOVE_TATTOOS_NOT_PURCHASED(pedID_ShopPed)
		ENDIF
		GIVE_PED_TEMP_TATTOO(pedID_ShopPed, sData.eCurrentTattoo)
		bUpdateTattoosThisFrame = FALSE
	ENDIF
	IF bUpdatePedTattoos
		bUpdateTattoosThisFrame = TRUE
	ENDIF
	
	// Keep players buddies out the way whilst we browse shop
	UPDATE_BUDDIES_FOR_SHOP_CUTSCENE(sBuddyHideData)
ENDPROC

PROC DO_PROCESS_NET_DOORS()
	// Proces door locking in MP
	IF sData.sShopInfo.bDataSet
	AND NETWORK_IS_GAME_IN_PROGRESS()
	
		// Let all the other scripts know what state we are in.
		playerBD[NATIVE_TO_INT(PLAYER_ID())].iShopProperties = g_sShopSettings.iProperties[sData.sShopInfo.eShop]
		
		// Only allow the door states to be set by the host once every 2 seconds
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			GRAB_CONTROL_OF_SHOP_DOORS(sData.sShopInfo.eShop)
			
			IF sData.bDoorTimerSet
				IF IS_TIME_MORE_THAN(GET_NETWORK_TIME(), sData.tdDoorTimer)
					// Lock the doors in MP if the shop is closed and all players have left.
					BOOL bLockDoors = TRUE
					IF NOT IS_BIT_SET(g_sShopSettings.iProperties[sData.sShopInfo.eShop], SHOP_NS_FLAG_SHOP_OPEN)
					AND NOT IS_BIT_SET(g_sShopSettings.iProperties[sData.sShopInfo.eShop], SHOP_NS_FLAG_PLAYER_IN_SHOP)
					AND (NOT DOES_ENTITY_EXIST(sData.sShopInfo.sShopKeeper.pedID) OR IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)) // if closed for host only dont close for everyone
						INT iPlayer
						REPEAT NUM_NETWORK_PLAYERS iPlayer
							IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayer))
								IF (IS_BIT_SET(playerBD[iPlayer].iShopProperties, SHOP_NS_FLAG_PLAYER_IN_SHOP))
									bLockDoors = FALSE
									iPlayer = NUM_NETWORK_PLAYERS+1// Bail
								ENDIF
							ENDIF
						ENDREPEAT
					ELSE
						bLockDoors = FALSE
					ENDIF
					
					//2400200
					IF MPGlobalsAmbience.bKeepShopDoorsOpen
						bLockDoors = FALSE
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === Vehicle in tattoo shop, unlocking doors.")
					ENDIF
					
					LOCK_SHOP_DOORS_MP(sData.sShopInfo.eShop, bLockDoors)
					sData.bDoorTimerSet = FALSE
				ENDIF
			ELSE
				sData.tdDoorTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), 2000)
				sData.bDoorTimerSet = TRUE
			ENDIF
		ELSE
			sData.tdDoorTimer = GET_NETWORK_TIME()
			sData.bDoorTimerSet = TRUE
		ENDIF
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
PROC DO_PROCESS_SHOP_MENU_DEBUG_CYCLE()
	IF g_bCycleShopMenus
		IF sData.sShopInfo.eStage = SHOP_STAGE_BROWSE_ITEMS
		AND sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_BROWSING
		
			BOOL bUpdatePedTattoos
			INT iCachedControl = sData.sBrowseInfo.iControl
			INT iCachedGroup = sData.sBrowseInfo.iCurrentGroup
			
			sData.sBrowseInfo.iControl = 1
			
			// GET_PED_DECORATION_ZONE_FROM_INT
			REPEAT 8 sData.sBrowseInfo.iCurrentGroup
				
				WAIT(0)
				
				BUILD_TATTOO_SHOP_MENU(bUpdatePedTattoos)
				BROWSE_TATTOOS_AND_DRAW_HUD(bUpdatePedTattoos)
				
			ENDREPEAT
			
			sData.sBrowseInfo.iCurrentGroup = iCachedGroup
			sData.sBrowseInfo.iControl = iCachedControl
			
			WAIT(0)
			
			BUILD_TATTOO_SHOP_MENU(bUpdatePedTattoos)
			BROWSE_TATTOOS_AND_DRAW_HUD(bUpdatePedTattoos)
			
			g_bCycleShopMenus = FALSE
		ENDIF
	ENDIF
ENDPROC
#ENDIF

PROC PROCESS_DLC_TATTOO_SHOP_KEEPER_PLACEMENT() 
	
	IF NOT IS_DLC_TATTOO_SHOP(eInShop)
		EXIT
	ENDIF
	
	IF (GET_FRAME_COUNT() % 30 != 0)
		EXIT
	ENDIF
	
	IF IS_ENTITY_ALIVE(sData.sShopInfo.sShopKeeper.pedID)
		IF NETWORK_HAS_CONTROL_OF_ENTITY(sData.sShopInfo.sShopKeeper.pedID)
			VECTOR vPedPosition = sData.sShopInfo.sShopKeeper.vPosIdle
			vPedPosition.z += 1.0
			IF NOT ARE_VECTORS_EQUAL(GET_ENTITY_COORDS(sData.sShopInfo.sShopKeeper.pedID), vPedPosition)
				SET_ENTITY_COORDS_NO_OFFSET(sData.sShopInfo.sShopKeeper.pedID, vPedPosition, TRUE)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

SCRIPT(SHOP_LAUNCHER_STRUCT sShopLauncherData)

	#IF IS_DEBUG_BUILD
		OUTPUT_SHOP_SCRIPT_LAUNCH_INFO(sShopLauncherData)
	#ENDIF
	
	eInShop = sShopLauncherData.eShop
	eBaseShop = GET_BASE_SHOP_TYPE(eInShop)
	
	SET_BIT(g_sShopSettings.iProperties[eInShop], SHOP_NS_FLAG_SHOP_SCRIPT_RUNNING)
	
	// Debug widgets
	#IF IS_DEBUG_BUILD
		SETUP_TATTOO_DEBUG_WIDGETS()
	#ENDIF
	
	// Carry out all the initial game starting duties and force cleanup checks
	PROCESS_PRE_GAME(sShopLauncherData.iNetInstanceID)
	
	REGISTER_SHOP_DOORS_MP(eInShop)

	IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
		bTransitionRestoreMenu = TRUE
	ENDIF

	sData.sShopInfo.iContextID = NEW_CONTEXT_INTENTION
	
	INITIALISE_COMMERCE_STORE_TRIGGER_DATA(sStoreTriggerData, eInShop)
	
	g_bTattooArtistPedCreated = FALSE
	
	// Main loop
	WHILE TRUE
	
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		WAIT(0)

		#IF IS_DEBUG_BUILD
			MAINTAIN_TATTOO_DEBUG_WIDGETS()
		#ENDIF
		
		UPDATE_SHOP_KEEPER_PED_ID(sData.sShopInfo.sShopKeeper)
		
		IF IS_SHOP_WITHIN_ACTIVATION_RANGE(eInShop)
		AND NOT SHOULD_SHOP_FORCE_CLEANUP(sData.sShopInfo, eInShop)
		
			IF IS_SHOP_SCRIPT_SAFE_TO_PROCESS(sData.sShopInfo)
				
				INITIALISE_DLC_TATTOO_SHOPS(eInShop)
				
				INT iDummyKickingOffPed = 0
				UPDATE_SHOP_KICKING_OFF_STATE(sData.sShopInfo, iDummyKickingOffPed, NULL, sData.sShopInfo.sShopCustomer.pedID)
				UPDATE_SHOP_IDLE_STATE(sData.sShopInfo, sData.sBrowseInfo)
				IF sData.sShopInfo.bDataSet		
					IF NOT IS_PLAYER_KICKING_OFF_IN_SHOP(sData.sShopInfo.eShop)
						UPDATE_SHOP_INPUT_BLOCKS(sData.sShopInfo, sData.sBrowseInfo)
					ENDIF
				ENDIF
				PROCESS_FINALISE_HEAD_BLEND()	
				PROCESS_TATTOO_SHOP_DIALOGUE()
				PROCESS_DLC_TATTOO_SHOP_KEEPER_PLACEMENT()
				
				SWITCH sData.sShopInfo.eStage
					
					CASE SHOP_STAGE_INITIALISE
						DO_INITIALISE()
					BREAK
					
					CASE SHOP_STAGE_WAIT_FOR_ENTRY
					
						PROCESS_END_SHOPPING_SAVE(FALSE)
					
						CHECK_TATTOO_SHOP_ENTRY()
						// Run intro stage instantly.
						IF sData.sShopInfo.eStage = SHOP_STAGE_ENTRY_INTRO
							DO_ENTRY_INTRO()
						ENDIF
					BREAK
					
					CASE SHOP_STAGE_ENTRY_INTRO
						DO_ENTRY_INTRO()
						UPDATE_SHOP_BROWSE_STATE(sData.sShopInfo, sData.sBrowseInfo)
						
						IF NETWORK_IS_GAME_IN_PROGRESS()
							UPDATE_PLAYER_IN_SHOP_STATE(sData.sShopInfo)
						ENDIF
					BREAK
					
					CASE SHOP_STAGE_WAIT_FOR_TRIGGER
						IF NOT IS_DLC_TATTOO_SHOP(sData.sShopInfo.eShop)
							UPDATE_SHOP_SHOPKEEPER_MOVED_TOOFAR(sData.sShopInfo, GET_TATTOOIST_END_POS(eBaseShop, sData.sShopInfo.eShop))
						ENDIF
						UPDATE_TATTOO_ARTIST()
						CHECK_TATTOO_LOCATE_ENTRY()
						CHECK_TATTOO_SHOP_EXIT()
					BREAK
					
					CASE SHOP_STAGE_BROWSE_ITEMS
						UPDATE_TATTOO_ARTIST()
						DO_BROWSE_ITEMS()
						UPDATE_SHOP_BROWSE_STATE(sData.sShopInfo, sData.sBrowseInfo)
					BREAK
					
					CASE SHOP_STAGE_LEAVE
						UPDATE_TATTOO_ARTIST()
						sData.sShopInfo.eStage = SHOP_STAGE_WAIT_FOR_ENTRY
					BREAK
				ENDSWITCH
			ENDIF
		ELSE
			CLEANUP_SHOP()
		ENDIF
		
		// Force a reset
		IF SHOULD_SHOP_FORCE_RESET(sData.sShopInfo)
			RESET_SHOP()
		ENDIF
		
		DO_PROCESS_NET_DOORS()
		
		#IF IS_DEBUG_BUILD
			DO_PROCESS_SHOP_MENU_DEBUG_CYCLE()
		#ENDIF
		
	ENDWHILE
ENDSCRIPT
