// This script is used for armory aricraft modification

USING "carmod_shop_core.sch"

SCRIPT(SHOP_LAUNCHER_STRUCT sShopLauncherData)

	#IF IS_DEBUG_BUILD
		OUTPUT_SHOP_SCRIPT_LAUNCH_INFO(sShopLauncherData)
	#ENDIF
	
	PERSONAL_CARMOD_MODIFICATION_SCRIPT_MAIN_INITIALISATION(sShopLauncherData)
	
	// Main loop
	WHILE TRUE
		
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		WAIT(0)
		
		CARMOD_SCRIPT_MAIN_UPDATE()
	ENDWHILE
	
ENDSCRIPT
