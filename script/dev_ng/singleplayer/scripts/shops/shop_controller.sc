//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	shop_controller.sc											//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	Maintains the shop state flags. This includes shop open, 	//
//							player kicking off, and any other flags we need to add. 	//
//							This deals with both single and multi-player data.			//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"

USING "commands_script.sch"
USING "commands_extrametadata.sch"
USING "commands_audio.sch"
USING "shop_private.sch"
USING "blip_control_public.sch"
USING "building_control_public.sch"
USING "comms_control_public.sch"
USING "screens_header.sch"
USING "Transition_Controller.sch"
USING "fm_unlocks_header.sch"
USING "flow_public_GAME.sch"
USING "code_control_public.sch"
USING "net_realty_details.sch"
USING "script_misc.sch"
USING "MPHud_CharacterAccessors.sch"
USING "net_reward_transactions.sch"
USING "casino_decoration_common.sch"
USING "am_pi_menu_const_header.sch"
USING "net_simple_interior.sch"
USING "shop_controller_reporting.sch"
#IF IS_DEBUG_BUILD
USING "mp_outfit_data.sch"
CONST_INT ENABLE_MP_OUTFIT_GENERATION 0
USING "net_realty_details.sch"
USING "net_hold_up_menu.sch"
#ENDIF

#IF FEATURE_GTAO_MEMBERSHIP
USING "net_GTAO_membership.sch"
#ENDIF

CONST_INT MAX_FRAMES_TO_PROCESS_SHOPS (NUMBER_OF_SHOPS) // Only do 1 shop per frame for now as we currently have 44 shops

ENUM SHOP_INIT_ENUM
	INITIALISED_IN_SP,
	INITIALISED_IN_MP_FREEMODE,
	
	NOT_INITIALISED
ENDENUM
SHOP_INIT_ENUM eInit = NOT_INITIALISED

ENUM SHOP_CONTROL_STAGE_ENUM
	SC_STAGE_INIT = 0,
	SC_STAGE_UPDATE,
	SC_STAGE_RESET
ENDENUM
SHOP_CONTROL_STAGE_ENUM eStage = SC_STAGE_INIT

INT iCurrentShopFrame

BOOL bHelmetVisorUp						= FALSE
BOOL bForceCleanupSetup					= FALSE
BOOL bBypassAutoEquipHelmetsInBikes		= TRUE
BOOL bTeamOutfitHasHelmet = FALSE
INT iSavedTeamOutfit = ENUM_TO_INT(OUTFIT_MP_DEFAULT)

STRUCT CAHCED_CLOTHING_STATES
	BOOL bPlayerWearingFittedHoodCache
	INT iPropHeadHashID
	INT iPropHeadVariantCount
ENDSTRUCT
CAHCED_CLOTHING_STATES sCachedClothingStates

// Shop specific flags
BOOL bCarmodProcessInit
INT iCarmodSPTimer
INT iRobbedTimer[NUMBER_OF_SHOPS]

//Help text queuing flags.
BOOL bBarberBlipHelpQueued
BOOL bBarberBlipVisibleThisFrame
BOOL bBarberBlipVisibleLastFrame

// One of checks that we can re-use
BOOL bPlayerOnMPMission
BOOL bTutorialsComplete
BOOL bPlayerOnMPActivityTurorial
BOOL bUpdatedBlipsOnMission
BOOL bUpdatedBlipsOnActivityTutorial

BOOL bPlayerOnSPMission

BOOL bInNightclubThisFrame

TIME_DATATYPE tdPlayerChangingClothes
BOOL bPlayerChangingClothesLastFrame
BOOl bUpdatePlayersTattooData = TRUE

CONST_INT ciSCUBA_BS_GEAR_FORCE_EQUIPPED	0
CONST_INT ciSCUBA_BS_USING_GEAR_PREV_FRAME	1
INT iBsScubaGear
BOOL bCheckedScubaOutfitOwnership = FALSE

BOOL bDisableMainUpdates
INT iDisableMainUpdatesStage

INT iShopEntitySetsInitialised
INT iShopEntitySetsTimer
TIME_DATATYPE tdShopEntitySetsTimer 
BOOL bInitialisedShopEntitySets

INT iForceCleanupTimer
TIME_DATATYPE tdForceCleanupTimer
BOOL bForceCleanupTimerSet

SCRIPT_TIMER removeForbiddenVehiclesTimer
INT iForbiddenVehicleStagger = 0

INT iLaunchScriptCount
SHOP_NAME_ENUM eLaunchScripts[NUMBER_OF_SHOPS]
INt iLaunchScripts[(NUMBER_OF_SHOPS/32)+1]


INT iWardrobeProperty = -1
INT iWardrobeYacht = -1
INT iIEWarehouse = -1
SHOP_NAME_ENUM eWardrobeShop = EMPTY_SHOP
WARDROBE_LAUNCHER_STRUCT sWardrobeLauncherData[3]

enumCharacterList eCurrentPed = NO_CHARACTER

BOOL bFlowStrandAtModShop

INT iHeistManagmentBitset

INT iDLCFeetHash = 0
PED_INDEX pedDLCFeetHashPed
INT iPedFeetDrawableVariation = 0

// iLocalBS
CONST_INT LOCAL_BIT_SET_AUTO_SHOP_RAND_TIME							0
CONST_INT LOCAL_BIT_SHOULD_GIVE_VEHICLE_THIS_TIME					1
CONST_INT LOCAL_BIT_FOUND_NEW_TUNER_CLIENT_VEHICLES					2
CONST_INT LOCAL_BIT_FOUND_NEW_TUNER_CLIENT_VEHICLES_FOR_SLOT_0		3
CONST_INT LOCAL_BIT_FOUND_NEW_TUNER_CLIENT_VEHICLES_FOR_SLOT_1		4
CONST_INT LOCAL_BIT_TRANSACTION_DONE_FOR_CLIENT_VEHICLES_SLOT_0		5
CONST_INT LOCAL_BIT_TRANSACTION_DONE_FOR_CLIENT_VEHICLES_SLOT_1		6
CONST_INT LOCAL_BIT_TRANSACTION_TU_DONE_FOR_CLIENT_VEHICLES_SLOT_1	7
CONST_INT LOCAL_BIT_TRANSACTION_TU_DONE_FOR_CLIENT_VEHICLES_SLOT_2	8
CONST_INT LOCAL_BIT_TRANSACTION_TU_REMOVE_CLIENT_VEHICLES_SLOT_1	9
CONST_INT LOCAL_BIT_TRANSACTION_TU_REMOVE_CLIENT_VEHICLES_SLOT_2	10
CONST_INT LOCAL_BIT_SET_BIKER_SHOP_RAND_TIME						11
CONST_INT LOCAL_BIT_BIKER_CLIENT_GIVE_VEHICLE_THIS_TIME		12
CONST_INT LOCAL_BIT_BIKER_CLIENT_FOUND_NEW_CLIENT_VEHICLE	13
CONST_INT LOCAL_BIT_BIKER_CLIENT_TRANSACTION_DONE			14

INT iLocalBS
INT iAutoShopRandomTime
SCRIPT_TIMER sTunerClientSpawnTimer
CONTRABAND_TRANSACTION_STATE clientVehicleTransResult
SC_REPORTING_DATA sExploitReportingdata

#IF IS_DEBUG_BUILD
BOOL bResetAutoShopVehiclePickTimer
BOOL bByPassAutoShopRandomTimer
BOOL bByPassAutoShopSpawnWeight
BOOL bResetAutoShopVehicleModels
INT iTunerClientVehicleID
TEXT_WIDGET_ID TunerClientVehicleName

BOOL bResetBikerShopVehiclePickTimer
BOOL bByPassBikerShopRandomTimer
BOOL bByPassBikerShopSpawnWeight
BOOL bResetBikerShopVehicleModels
INT iBikerClientVehicleID
TEXT_WIDGET_ID BikerClientVehicleName
#ENDIF

#IF FEATURE_DLC_1_2022
INT iBikerShopRandomTime
SCRIPT_TIMER sBikerClientSpawnTimer
CONTRABAND_TRANSACTION_STATE bikerClientVehicleTransResult
#ENDIF

CONST_INT HMB_REBREATHER_ON			0

MANAGE_VISUALAID_CONTENT_STRUCT sVisualAidData

SCALEFORM_LOADING_ICON sCashTransactionIconStruct
BOOL bRunCashTransactionPendingIcon = FALSE

CONST_INT iILLUMINATED_FLASH_TIME	1000
CONST_FLOAT fILLUMINATED_PULSE_FREQUENCY	0.2

CASH_TRANSACTIONS_DETAILS m_cashTransactionData[SHOPPING_TRANSACTION_MAX_NUMBER+1]	// TODO: Need to use const int that Miguel is adding for max transactions at once

#IF FEATURE_COPS_N_CROOKS
STRUCT SUI_PURCHASE_DATA
	GENERIC_TRANSACTION_STATE eTransactionState
	SCRIPT_UNLOCKABLE_ITEM eItem = SUI_UNLOCK_INVALID
	TRANSACTION_CURRENCY eCurrency
ENDSTRUCT

SUI_PURCHASE_DATA sSUIPurchase
#ENDIF

INT iRadioLockBS
CONST_INT RADIO_LOCK_BS_CACHE_ISLAND						0
CONST_INT RADIO_LOCK_BS_CACHE_TUNER							1
CONST_INT RADIO_BS_FORCE_TRACK_TUNER_INITIALISE 			2
CONST_INT RADIO_LOCK_BS_CACHE_FIXER				 			3
CONST_INT RADIO_LOCK_BS_BOOTING_TO_SP			 			4
CONST_INT RADIO_LOCK_BS_CACHE_AFTER_BOOT		 			5

#IF FEATURE_TUNER
BOOL bInitPIMRadioFavStationsSP = FALSE
BOOL bInitPIMRadioFavStationsMP = FALSE
#ENDIF

#IF FEATURE_GTAO_MEMBERSHIP
GTAO_MEMBERSHIP_DATA sGTAOMembership
#ENDIF

#IF IS_DEBUG_BUILD

USING "net_crate_drop.sch"

BOOL bRefreshTunables
BOOL bDisableFrontEndOutput

BOOL bHideShopBlipsTracker, bHideShopBlipsState
WIDGET_GROUP_ID widgetGroupID

BOOL bDrawDebugHelmetStuff

BOOL b_debug_get_weapons, b_debug_set_weapons, b_debug_remove_weapons, b_debug_output_weapon_data


CONST_INT DBG_SAVED_ITEM_TYPE_VEHICLE		0
CONST_INT DBG_SAVED_ITEM_TYPE_WEAPON		1
CONST_INT DBG_SAVED_ITEM_TYPE_COUNT			2

STRUCT DEBUG_SAVE_ITEM_DATA
	INT iSaveItemType
	TEXT_WIDGET_ID twSaveItemFilename
	BOOL bSaveItemToFile
	BOOL bSetItemFromFile
	INT iProcessSetItemBitfield
	WEAPON_INFO sWeaponInfo
	VEHICLE_SETUP_STRUCT_MP sVehicleDataMP
ENDSTRUCT
DEBUG_SAVE_ITEM_DATA sSaveItemDebugData


BOOL b_debug_save_AllClothesAcquired
BOOL b_debug_save_AllTattoosAcquired
BOOL b_debug_save_AllWeaponsAcquired

BOOL b_debug_AllSnacksAcquired

BOOL b_debug_equip_stored_parachute
INT i_debug_stored_parachute_tint = -1, i_debug_stored_parachute_bag_index = -1

BOOL b_debug_test_visual_aid_override
INT i_debug_visual_aid_override = ENUM_TO_INT(g_sFMMCVisualAid.eOverride)
INT i_debug_VISUAL_AID_SOUND = ENUM_TO_INT(g_sFMMCVisualAid.eUseSound)
TEXT_WIDGET_ID tw_debug_visual_aid_override_script

BOOL b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_END]
BOOL b_debug_clear_player_purchased_CESP_content

//INT iMP_PropertiesWidget[NUMBER_OF_SHOPS]
BOOL b_debug_force_reset
BOOL b_debug_warning_message
BOOL b_debug_launch_store
BOOL b_debug_turn_xmas_snow_on
BOOL b_debug_turn_xmas_tree_on
BOOL b_debug_check_xmas_clothing

BOOL b_debug_apply_postfx
BOOL b_debug_stop_postfx
TEXT_WIDGET_ID w_debug_post_fx
STRING strLastPostFX

TEXT_WIDGET_ID tw_debug_current_item_label

BOOL b_debug_clear_veh_txt_delay
//BOOL b_debug_unlock_reward_shirts
BOOL b_debug_reward_shirt_check
BOOL b_debug_space_monkey

BOOL b_debug_output_tattoo_data

BOOL b_debug_stats_output_1
BOOL b_debug_stats_output_2
	
BOOL b_debug_outfit_output_3
BOOL b_debug_outfit_output_4	

BOOL b_debug_output_exclusive_vehicle_items

INT	i_debug_base_property = 0
INT i_debug_base_last_valid_element = MAX_NUMBER_OF_MP_PROP_ELEMENTS
BOOL b_debug_dump_xml_property_to_log = FALSE
BOOL b_debug_dump_metadata_property_to_log = FALSE
BOOL b_debug_compare_property_metadata_with_xml = FALSE

BOOL b_debug_clothing_id_test
INT i_debug_clothing_id

BOOL b_debug_print_bitset_tattoo
TEXT_WIDGET_ID w_debug_tattoo_name
INT i_debug_tattoo_faction
INT i_debug_tattoo_namehash

BOOL b_debug_quickfix_gift_gtao_items

BOOL bWriteItemsToCatalogue[6]
BOOL bWriteInventoryToCatalogue
BOOl bWriteInventoryToCatalogue2
BOOL bDebugRunCashTransactionPendingIcon
BOOL b_debug_add_vehicle_to_garage
BOOL b_debug_process_character_appearance
BOOL b_debug_force_reset_transaction_data
BOOL b_debug_display_transaction_queue
BOOl bWriteInventoryToCatalogueCasDecoration
BOOl bWriteInventoryToCatalogueSUI
BOOL b_debug_spend
BOOL b_debug_earn
INT i_debug_earn_spend


BOOL b_debug_launch_debug_ped_script = FALSE
BOOL b_debug_launch_item_ownership_script = FALSE

BOOL b_debug_output_textTag
BOOL b_debug_add_all_pegasus_to_garage
INT i_debug_pegasus_count = 0, i_debug_pegasus_stage = 0
TEXT_WIDGET_ID tw_debug_pegasus_count

TWEAK_FLOAT fTextXScale		0.0000		// 0.0000	
TWEAK_FLOAT fTextYScale		0.258		// 0.360
										   
TWEAK_FLOAT fRectXCenter	0.500		// 0.873
TWEAK_FLOAT fRectYCenter	0.500		// 0.248
TWEAK_FLOAT fRectWidth		0.900		// 0.163
TWEAK_FLOAT fRectHeight		0.900		// 0.230
										   
TWEAK_FLOAT fTextXDisplay	0.058		// 0.8
TWEAK_FLOAT fTextYDisplay	0.074		// 0.15
										   
TWEAK_FLOAT fTextXOffset	0.200		// 0.1
TWEAK_FLOAT fTextYOffset	0.026		// 0.03
										   
TWEAK_INT iTextRowMax		32			// 25
INT i_debug_selected_owned_pegasus_vehicles = 0
BOOL b_debug_display_owned_pegasus_vehicles = FALSE

BOOL b_debug_add_mission
BOOL b_debug_add_materials
INT i_debug_material_to_add = 1
BOOL b_debug_add_product
BOOL b_debug_generate_product
BOOL b_debug_remove_product
BOOL b_debug_reset_business
BOOL b_debug_setup_business
CONTRABAND_TRANSACTION_STATE eTransResult

BOOL b_debug_output_veh_data, b_debug_output_veh_data_on_focused_veh
BOOL b_debug_output_veh_data_move
TEXT_WIDGET_ID w_debug_veh_name

BOOL b_debug_output_CURRENT_veh_mass_and_armour
BOOL b_debug_output_BUYABLE_veh_mass_and_armour

IE_VEH_EVENT_TEST_WIDGETS eIEVehTestData

#IF FEATURE_GEN9_EXCLUSIVE
INT iDebugHSWModCoupon[20]
BOOL bDisplayHSWModCouponInfo
BOOL bRedeemHSWModCoupon, bObtainHSWModCoupon
#ENDIF

BOOL b_debug_set_bigass_vehicle_rewarded
INT i_debug_set_bigass_vehicle_rewarded_bitindex
INT i_debug_set_bigass_vehicle_rewarded_bitindex_cache
TEXT_WIDGET_ID tw_debug_set_bigass_vehicle_rewarded_bitindex
INT i_debug_set_bigass_vehicle_rewarded_transaction_status
INT i_debug_set_bigass_vehicle_rewarded_result = ENUM_TO_INT(eBIGASS_VEHICLE_REWARDED_unset)+1

PROC SETUP_SHOP_CONTROL_WIDGETS()

	IF GET_COMMANDLINE_PARAM_EXISTS("sc_TransactionDebug")
		b_debug_display_transaction_queue = TRUE
	ENDIF
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_EnableClothingDebug")
		b_debug_launch_debug_ped_script = TRUE
	ENDIF
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_EnableItemOwnershipDebug")
		b_debug_launch_item_ownership_script = TRUE
	ENDIF

	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DebugUnlockDLCRewardItems")
		g_bDebugUnlockLowriderRewardItems = TRUE
		g_bDebugUnlockLuxeRewardItems = TRUE
		g_bDebugUnlockLTSRewardItems = TRUE
		g_bDebugUnlockPilotRewardItems = TRUE
		g_bDebugAllowIndependenceItems = TRUE
		g_bDebugUnlockIndependenceRewardItems = TRUE
		g_bDebugAllowValentinesItems = TRUE
		g_bDebugAllowValentines2016Items = TRUE
		g_bDebugUnlockTournamentItems = TRUE
		g_bDebugUnlockVintageRewardItems = TRUE
		g_bDebugAllowHeistItems = TRUE
		g_bDebugAllowChristmasClothes = TRUE
		g_bDebugAllowChristmasMasks = TRUE
		g_bDebugUnlockChristmasRewardItems = TRUE
		g_bDebugUnlockNaughtyHat = TRUE
		g_bDebugUnlockNiceHat = TRUE
		g_bDebugUnlockAbominableSnowmanMask = TRUE
		g_bDebugUnlockCGtoNGVehicles = TRUE
		g_bDebugUnlockHalloweenItems = TRUE
		g_bDebugUnlockExecutiveRewardItems = TRUE
		g_bDebugUnlockStuntRewardItems = TRUE
		g_bDebugUnlockBikerRewardItems = TRUE
		g_bDebugUnlockIERewardItems = TRUE
		g_bDebugUnlockGunrunningRewardItems = TRUE
		g_bDebugUnlockSmugglerRewardItems = TRUE
		g_bDebugUnlockGangOpsRewardItems = TRUE
		g_bDebugUnlockTargetRacesRewardItems = TRUE
		g_bDebugUnlockBusinessBattlesRewardItems = TRUE
		g_bDebugUnlockArenaWarsRewardItems = TRUE
		g_bDebugUnlockCasinoRewardItems = TRUE
		g_bDebugUnlockCasinoHeistRewardItems = TRUE
		#IF FEATURE_COPS_N_CROOKS
		g_bDebugUnlockCnCRewardItems = TRUE
		#ENDIF
		g_bDebugUnlockSummer20RewardItems = TRUE
		g_bDebugUnlockIslandHeistRewardItems = TRUE
		#IF FEATURE_TUNER
		g_bDebugUnlockTunerRewardItems = TRUE
		#ENDIF
		#IF FEATURE_GEN9_EXCLUSIVE
		g_bDebugUnlockGen9EXContentItems = TRUE
		#ENDIF
		#IF FEATURE_FIXER
		g_bDebugUnlockFixerRewardItems = TRUE
		#ENDIF
		#IF FEATURE_DLC_1_2022
		g_bDebugUnlockSummer22RewardItems = TRUE
		#ENDIF
		CPRINTLN(DEBUG_SHOPS, "sc_DebugUnlockDLCRewardItems = true")
	ENDIF
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DisableForbiddenVehicleRemoval")
		g_bDisableForbiddenVehicleRemoval = TRUE
		CPRINTLN(DEBUG_SHOPS, "g_bDisableForbiddenVehicleRemoval = true")
	ENDIF
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_ForceEnableForbiddenVehicleRemoval")
		g_bForceEnableForbiddenVehicleRemoval = TRUE
		CPRINTLN(DEBUG_SHOPS, "g_bForceEnableForbiddenVehicleRemoval = true")
	ENDIF
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_DebugUnlockXmasClothes")
		g_bDebugAllowChristmasClothes = TRUE
		g_bDebugAllowChristmasMasks = TRUE
	ENDIF
	
	IF GET_DRAW_DEBUG_COMMANDLINE_PARAM_EXISTS("player_helmet", TRUE)
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		bDrawDebugHelmetStuff = TRUE
	ENDIF
	
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_SkipPedCompValidation")
		g_bValidatePlayersTorsoComponent = FALSE
	ENDIF
	
	INT i
	TEXT_LABEL_31 tlName
	
	IF DOES_WIDGET_GROUP_EXIST(widgetGroupID)
		DELETE_WIDGET_GROUP(widgetGroupID)
		widgetGroupID = NULL
	ENDIF
	
	widgetGroupID = START_WIDGET_GROUP("Shop Controller")
	
		START_WIDGET_GROUP("Cash Transactions")
			ADD_WIDGET_INT_SLIDER("Transaction frame delay (Basket)", g_iTransactionFrameDelay_Basket, 0, 9999, 1)
			ADD_WIDGET_INT_SLIDER("Transaction frame delay (Service)", g_iTransactionFrameDelay_Service, 0, 9999, 1)
			ADD_WIDGET_BOOL("Display script queue", b_debug_display_transaction_queue)
			ADD_WIDGET_BOOL("Force success", g_TransactionDebug_ForceSuccess)
			ADD_WIDGET_BOOL("Block End Service", g_TransactionDebug_BlockEndService)
			ADD_WIDGET_BOOL("Block Basket End", g_TransactionDebug_BlockBasketEnd)
			ADD_WIDGET_BOOL("Display Failed Alert", g_bTriggerTransactionFailedAlert)
			ADD_WIDGET_BOOL("Display Blocked Alert", g_bTriggerTransactionBlockedAlert)
			ADD_WIDGET_BOOL("Display Blocked Consumable Alert", g_bTriggerTransactionBlockedConsumeAlert)
			ADD_WIDGET_BOOL("Block Transactions", g_bDebugBlockCashTransactions)
			ADD_WIDGET_BOOL("Add mods to basket (mod)", g_bAddModsToBasket)
			ADD_WIDGET_BOOL("Add current vehicle to garage", b_debug_add_vehicle_to_garage)
			ADD_WIDGET_BOOL("Process character appearance", b_debug_process_character_appearance)
			ADD_WIDGET_BOOL("Process reset data", b_debug_force_reset_transaction_data)
			ADD_WIDGET_BOOL("Run Cash Transaction Pending Icon", bDebugRunCashTransactionPendingIcon)
			ADD_WIDGET_INT_SLIDER("Earn/Spend value", i_debug_earn_spend, 0, 15000000, 10000)
			ADD_WIDGET_BOOL("Spend", b_debug_spend)
			ADD_WIDGET_BOOL("Earn", b_debug_earn)
			
			
			
			START_WIDGET_GROUP("Businesses")
				ADD_WIDGET_INT_SLIDER("Add/Remove value", i_debug_material_to_add, 1, 100, 1)
				ADD_WIDGET_BOOL("Add mission", b_debug_add_mission)
				ADD_WIDGET_BOOL("Add materials", b_debug_add_materials)
				ADD_WIDGET_BOOL("Add product", b_debug_add_product)
				ADD_WIDGET_BOOL("Generate product", b_debug_generate_product)
				ADD_WIDGET_BOOL("Remove product", b_debug_remove_product)
				ADD_WIDGET_BOOL("Reset business", b_debug_reset_business)
				ADD_WIDGET_BOOL("Set up business", b_debug_setup_business)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_BOOL("Add all pegasus to garage", b_debug_add_all_pegasus_to_garage)
			ADD_WIDGET_INT_SLIDER("i_debug_pegasus_count", i_debug_pegasus_count, -1, 200, 1)
			tw_debug_pegasus_count = ADD_TEXT_WIDGET("tw_debug_pegasus_count")
			SET_CONTENTS_OF_TEXT_WIDGET(tw_debug_pegasus_count, "none")
			ADD_WIDGET_INT_SLIDER("i_debug_pegasus_stage", i_debug_pegasus_stage, 0, 5, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Catalogue")
			START_WIDGET_GROUP("Price Updates")
				ADD_WIDGET_BOOL("Verify prices using tunables", g_bVerifyMenuItemPrices)
				ADD_WIDGET_BOOL("Cycle shop menus", g_bCycleShopMenus)
			STOP_WIDGET_GROUP()
			ADD_WIDGET_BOOL("Generate catalogue from menu items", g_bAddMenuItemsToCatalogue)
			ADD_WIDGET_BOOL("Remove catalogue items before adding", g_bDeleteMenuItemsBeforeAddingToCatalogue)
			ADD_WIDGET_BOOL("Generate mart items", bWriteItemsToCatalogue[0])
			ADD_WIDGET_BOOL("Generate property items", bWriteItemsToCatalogue[1])
			ADD_WIDGET_BOOL("Generate vehicle items", bWriteItemsToCatalogue[2])
			ADD_WIDGET_BOOL("Generate vehicle mod items", bWriteItemsToCatalogue[3])
			ADD_WIDGET_BOOL("Generate service items", bWriteItemsToCatalogue[4])
			ADD_WIDGET_BOOL("Generate low grip/standard tire items for all vehicles", bWriteItemsToCatalogue[5])
			ADD_WIDGET_BOOL("Generate inventory items", bWriteInventoryToCatalogue)
			ADD_WIDGET_BOOL("Generate vehicle mod inventory items", bWriteInventoryToCatalogue2)
			ADD_WIDGET_BOOL("Generate casino decoration inventory items", bWriteInventoryToCatalogueCasDecoration)
			ADD_WIDGET_BOOL("Generate Script Unlockable inventory items", bWriteInventoryToCatalogueSUI)
			ADD_WIDGET_BOOL("Output generated keys", g_bOutputCatalogKeyDebug)
			ADD_WIDGET_BOOL("Output TextTagData", b_debug_output_textTag)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Clothing")
			ADD_WIDGET_BOOL("Launch clothing debug script (Shift+C)", b_debug_launch_debug_ped_script)
			ADD_WIDGET_STRING("Widgets moved to rag/Script/Clothing Debug")
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Crate Drop")
			ADD_WIDGET_INT_SLIDER("Item hash", i_debug_clothing_id, -2147483647, 2147483647, 1)
			ADD_WIDGET_BOOL("Check if item is valid", b_debug_clothing_id_test)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Item Ownership")
			ADD_WIDGET_BOOL("Launch item ownership debug script", b_debug_launch_item_ownership_script)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Menu System")
			ADD_WIDGET_FLOAT_SLIDER("TEXT_SCALE_Y", CUSTOM_MENU_TEXT_SCALE_Y, 0.0, 100.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("CONDENSED_TEXT_SCALE_Y", CUSTOM_MENU_CONDENSED_TEXT_SCALE_Y, 0.0, 100.0, 0.001)
			ADD_WIDGET_BOOL("Use debug key pos", g_sMenuData.bUseTempKeyCoords)
			ADD_WIDGET_FLOAT_SLIDER("Help Keys X", g_sMenuData.fHelpKeysX, 0.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_SLIDER("Help Keys Y", g_sMenuData.fHelpKeysY, 0.0, 1.0, 0.001)
			
			START_WIDGET_GROUP("Menu Spacers")
				ADD_WIDGET_BOOL("Draw spacers", g_sMenuData.bDrawDebugSpacers)
				REPEAT COUNT_OF(g_sMenuData.fSpacerX) i
					tlName = "Spacer[" tlName += i tlName += "] X"
					ADD_WIDGET_FLOAT_SLIDER(tlName, g_sMenuData.fSpacerX[i], 0.0, 1.0, 0.001)
					tlName = "Spacer[" tlName += i tlName += "] Y"
					ADD_WIDGET_FLOAT_SLIDER(tlName, g_sMenuData.fSpacerY[i], 0.0, 1.0, 0.001)
					tlName = "Spacer[" tlName += i tlName += "] px"
					ADD_WIDGET_INT_SLIDER(tlName, g_sMenuData.iSpacerScale[i], 0, 1280, 1)
				ENDREPEAT
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("PostFX")
		
			w_debug_post_fx = ADD_TEXT_WIDGET("postFXName")
			SET_CONTENTS_OF_TEXT_WIDGET(w_debug_post_fx, "")
			
			ADD_WIDGET_BOOL("Apply postfx", b_debug_apply_postfx)
			ADD_WIDGET_BOOL("Stop all postfx", b_debug_stop_postfx)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Restricted Content")
			START_WIDGET_GROUP("Biker")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockBikerRewardItems)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Christmas")
				ADD_WIDGET_BOOL("Abominable Snowman", g_bDebugUnlockAbominableSnowmanMask)
				ADD_WIDGET_BOOL("Naughty Hat", g_bDebugUnlockNaughtyHat)
				ADD_WIDGET_BOOL("Nice Hat", g_bDebugUnlockNiceHat)
				ADD_WIDGET_BOOL("Clothes", g_bDebugAllowChristmasClothes)
				ADD_WIDGET_BOOL("Masks", g_bDebugAllowChristmasMasks)
				ADD_WIDGET_BOOL("Reward items", g_bDebugUnlockChristmasRewardItems)
				ADD_WIDGET_BOOL("Snow", b_debug_turn_xmas_snow_on)
				ADD_WIDGET_BOOL("Tree", b_debug_turn_xmas_tree_on)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Criminal Enterprise Starter Pack")
				ADD_WIDGET_BOOL("Force ON", g_bForce_STARTER_true)
				ADD_WIDGET_BOOL("Force OFF", g_bForce_STARTER_false)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Criminal Enterprise Premium Pack")
				ADD_WIDGET_BOOL("Force ON", g_bForce_PREMIUM_true)
				ADD_WIDGET_BOOL("Force OFF", g_bForce_PREMIUM_false)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Executive")
				ADD_WIDGET_BOOL("Reward items", g_bDebugUnlockExecutiveRewardItems)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("UpdateOne2019/Casino")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockCasinoRewardItems)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("ArenaWars")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockArenaWarsRewardItems)
				
				ADD_WIDGET_STRING("Is Vehicle Reward Unlocked")
				ADD_WIDGET_BOOL("Force unlocked - TRUE", g_bForce_IS_ARENA_WARS_VEHICLE_REWARD_UNLOCKED_true)
				ADD_WIDGET_BOOL("Force unlocked - FALSE", g_bForce_IS_ARENA_WARS_VEHICLE_REWARD_UNLOCKED_false)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("BusinessBattles")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockBusinessBattlesRewardItems)
				ADD_WIDGET_BOOL("Hacker Truck Force ON", g_bForce_HACKER_TRUCK_true)
				ADD_WIDGET_BOOL("Hacker Truck Force OFF", g_bForce_HACKER_TRUCK_false)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Casino")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockCasinoRewardItems)
				START_WIDGET_GROUP("Price Control")
					ADD_WIDGET_BOOL("Refresh Tunables", bRefreshTunables)
					ADD_WIDGET_BOOL("Casino Clothing", g_bDebugAllowCasinoClothingItems)
					ADD_WIDGET_BOOL("Casino Decoration", g_bDebugAllowCasinoDecorationItems)
				STOP_WIDGET_GROUP()
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Casino Heist")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockCasinoHeistRewardItems)
			STOP_WIDGET_GROUP()
			#IF FEATURE_COPS_N_CROOKS
			START_WIDGET_GROUP("CnC")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockCnCRewardItems)
			STOP_WIDGET_GROUP()
			#ENDIF
			#IF FEATURE_FIXER
			START_WIDGET_GROUP("Fixer")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockFixerRewardItems)
			STOP_WIDGET_GROUP()
			#ENDIF
			START_WIDGET_GROUP("GangOps")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockGangOpsRewardItems)
			STOP_WIDGET_GROUP()
			#IF FEATURE_GEN9_EXCLUSIVE
			START_WIDGET_GROUP("Gen9EC")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockGen9EXContentItems)
			STOP_WIDGET_GROUP()
			#ENDIF
			START_WIDGET_GROUP("Gunrunning")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockGunrunningRewardItems)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Halloween")
				ADD_WIDGET_BOOL("Items", g_bDebugUnlockHalloweenItems)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Heists")
				ADD_WIDGET_BOOL("Items", g_bDebugAllowHeistItems)
				ADD_WIDGET_BOOL("Reward shirt", g_bDebugUnlockDLCAwardShirts)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Import/Export")
				ADD_WIDGET_BOOL("Reward/Timed items", g_bDebugUnlockIERewardItems)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Independence Day")
				ADD_WIDGET_BOOL("Items", g_bDebugAllowIndependenceItems)
				ADD_WIDGET_BOOL("Reward items", g_bDebugUnlockIndependenceRewardItems)
				ADD_WIDGET_BOOL("Tunable: TOGGLE_ACTIVATE_INDEPENDENCE_PACK", g_sMPTunables.btoggleactivateIndependencepack)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Island Heist")
				ADD_WIDGET_BOOL("Reward items", g_bDebugUnlockIslandHeistRewardItems)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("LTS")
				ADD_WIDGET_BOOL("Reward items", g_bDebugUnlockLTSRewardItems)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Lowrider")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockLowriderRewardItems)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Lowrider 2")
				ADD_WIDGET_BOOL("High Waist Pants", g_bDebugUnlockLowrider2HighWaist)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Luxe")
				ADD_WIDGET_BOOL("Reward items", g_bDebugUnlockLuxeRewardItems)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Misc")
				START_WIDGET_GROUP("GEN9 EXCLUSIVE")
					ADD_WIDGET_BOOL("Output Exclusive Vehicle Mods", b_debug_output_exclusive_vehicle_items)
				STOP_WIDGET_GROUP()
				START_WIDGET_GROUP("FORCE BUYABLE VEHICLE LOCK")
					ADD_WIDGET_STRING("Is Last Gen Player")
					ADD_WIDGET_BOOL("Force last gen - TRUE", g_bForce_IS_LAST_GEN_PLAYER_true)
					ADD_WIDGET_BOOL("Force last gen - FALSE", g_bForce_IS_LAST_GEN_PLAYER_false)
					ADD_WIDGET_STRING("Enable Returning Content")
					ADD_WIDGET_BOOL("Enable Content - VEHICLE", g_sMPTunables.bENABLE_RETURNING_CONTENT_VEHICLE)
					ADD_WIDGET_BOOL("Enable Content - DOD", g_sMPTunables.bENABLE_RETURNING_CONTENT_DOD)
					ADD_WIDGET_BOOL("Enable Content - WEAPON", g_sMPTunables.bENABLE_RETURNING_CONTENT_WEAPON)
					ADD_WIDGET_STRING("Is Buyable Vehicle Unlocked")
					ADD_WIDGET_BOOL("Force unlocked - TRUE", g_bForce_IS_BUYABLE_VEHICLE_UNLOCKED_true)
					ADD_WIDGET_BOOL("Force unlocked - FALSE", g_bForce_IS_BUYABLE_VEHICLE_UNLOCKED_false)
					
					ADD_WIDGET_STRING("Enable gunrunning modshop upgrades")
					ADD_WIDGET_BOOL("Enable INSURGENT modshop", g_sMPTunables.bENABLE_INSURGENT_MODSHOP)
					ADD_WIDGET_BOOL("Enable TECHNICAL modshop", g_sMPTunables.bENABLE_TECHNICAL_MODSHOP)
				STOP_WIDGET_GROUP()
				
				ADD_WIDGET_BOOL("Last Gen Player", g_bLastGenPlayer)
				ADD_WIDGET_BOOL("Last Gen Collector", g_bLastGenCollectorEditionPlayer)
				ADD_WIDGET_BOOL("Tournament items", g_bDebugUnlockTournamentItems)
				ADD_WIDGET_BOOL("Vintage items", g_bDebugUnlockVintageRewardItems)
				ADD_WIDGET_BOOL("CGtoNG Stock Cars", g_bDebugUnlockCGtoNGVehicles)
				ADD_WIDGET_BOOL("Space monkey clothes", b_debug_space_monkey)
				ADD_WIDGET_BOOL("Disable forbidden vehicle removal", g_bDisableForbiddenVehicleRemoval)
				ADD_WIDGET_BOOL("Force enable forbidden vehicle removal", g_bForceEnableForbiddenVehicleRemoval)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Pilot")
				ADD_WIDGET_BOOL("Reward items", g_bDebugUnlockPilotRewardItems)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Smuggler")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockSmugglerRewardItems)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Stunt")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockStuntRewardItems)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Summer 2020")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockSummer20RewardItems)
			STOP_WIDGET_GROUP()
			#IF FEATURE_DLC_1_2022
			START_WIDGET_GROUP("Summer 2022")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockSummer22RewardItems)
				ADD_WIDGET_BOOL("eCola/Sprunk Bodysuit Purchasable", g_sMPTunables.bUNLOCK_SPRUNK_AND_ECOLA_BODYSUITS_FOR_PURCHASE)
				ADD_WIDGET_BOOL("eCola Purchasable", g_sMPTunables.bUNLOCK_ECOLA_ITEMS_FOR_PURCHASE)
				ADD_WIDGET_BOOL("Sprunk Purchasable", g_sMPTunables.bUNLOCK_SPRUNK_ITEMS_FOR_PURCHASE)
				ADD_WIDGET_BOOL("Service Carbine Purchasable", g_sMPTunables.bUNLOCK_SERVICE_CARBINE_FOR_PURCHASE)
			STOP_WIDGET_GROUP()
			#ENDIF
			START_WIDGET_GROUP("TargetRaces")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockTargetRacesRewardItems)
			STOP_WIDGET_GROUP()
			#IF FEATURE_TUNER
			START_WIDGET_GROUP("Tuner")
				ADD_WIDGET_BOOL("Reward Items", g_bDebugUnlockTunerRewardItems)
			STOP_WIDGET_GROUP()
			#ENDIF
			START_WIDGET_GROUP("Valentines")
				ADD_WIDGET_BOOL("Items", g_bDebugAllowValentinesItems)
				ADD_WIDGET_BOOL("2016 Items", g_bDebugAllowValentines2016Items)
				ADD_WIDGET_BOOL("Tunable: TURN_ON_VALENTINES_EVENT", g_sMPTunables.bturnonvalentinesevent)
				ADD_WIDGET_BOOL("Tunable: TURN_ON_VALENTINE_CLOTHING", g_sMPTunables.bTURN_ON_VALENTINE_CLOTHING)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Reward transaction")
			ADD_WIDGET_BOOL("Reward grey bulletproof helmet", g_DebugRewardHelmetViaTransaction)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Sale Tunables")
			START_WIDGET_GROUP("Web Sale Tunables")
				ADD_WIDGET_BOOL("bVehicleWebsite_sale", g_sMPTunables.bVehicleWebsite_sale)
				ADD_WIDGET_BOOL("g_bForceVehicleWebsite_sale", g_bForceVehicleWebsite_sale)
				ADD_WIDGET_BOOL("bPropertyWebsite_sale", g_sMPTunables.bPropertyWebsite_sale)
				ADD_WIDGET_BOOL("g_bForcePropertyWebsite_sale", g_bForcePropertyWebsite_sale)
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Vehicle Mod Sale Tunables")
				ADD_WIDGET_BOOL("bALL_VEHICLE_MODS_FOR_SALE", g_sMPTunables.bALL_VEHICLE_MODS_FOR_SALE)
//				ADD_WIDGET_INT_SLIDER("iSALE_CARMOD_MENU_BITSET[92]", g_sMPTunables.iSALE_CARMOD_MENU_BITSET[92])
//				ADD_WIDGET_INT_SLIDER("iINDIVIDUAL_VEHICLE_SALE_HASH_LABELS[50]", g_sMPTunables.iINDIVIDUAL_VEHICLE_SALE_HASH_LABELS[50])
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Weapon Sale Tunables")
				ADD_WIDGET_BOOL("bALL_WEAPON_MODS_FOR_SALE", g_sMPTunables.bALL_WEAPON_MODS_FOR_SALE)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_WAP_Clip", g_sMPTunables.bSALE_WEAPON_MOD_MENU_WAP_Clip)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_WAP_Flash", g_sMPTunables.bSALE_WEAPON_MOD_MENU_WAP_Flash)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_WAP_Scop", g_sMPTunables.bSALE_WEAPON_MOD_MENU_WAP_Scop)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_WAP_Rail", g_sMPTunables.bSALE_WEAPON_MOD_MENU_WAP_Rail)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_WAP_Grip", g_sMPTunables.bSALE_WEAPON_MOD_MENU_WAP_Grip)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_WAP_Supp", g_sMPTunables.bSALE_WEAPON_MOD_MENU_WAP_Supp)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_WAP_Targ", g_sMPTunables.bSALE_WEAPON_MOD_MENU_WAP_Targ)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_WAP_Engrave", g_sMPTunables.bSALE_WEAPON_MOD_MENU_WAP_Engrave)
				
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_UPGRADE", g_sMPTunables.bSALE_WEAPON_MOD_MENU_UPGRADE)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_MAGS", g_sMPTunables.bSALE_WEAPON_MOD_MENU_MAGS)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_SCOPES", g_sMPTunables.bSALE_WEAPON_MOD_MENU_SCOPES)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_MKII_GRIPS", g_sMPTunables.bSALE_WEAPON_MOD_MENU_MKII_GRIPS)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_FLASHLIGHTS", g_sMPTunables.bSALE_WEAPON_MOD_MENU_FLASHLIGHTS)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_MUZZLES", g_sMPTunables.bSALE_WEAPON_MOD_MENU_MUZZLES)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_BARRELS", g_sMPTunables.bSALE_WEAPON_MOD_MENU_BARRELS)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_LIVERY", g_sMPTunables.bSALE_WEAPON_MOD_MENU_LIVERY)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_LIVERY_COLOR", g_sMPTunables.bSALE_WEAPON_MOD_MENU_LIVERY_COLOR)
				ADD_WIDGET_BOOL("bSALE_WEAPON_MOD_MENU_TINTS", g_sMPTunables.bSALE_WEAPON_MOD_MENU_TINTS)
				
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_MG_ARMORPIERCING", g_sMPTunables.bSALE_WEAPON_AMMO_MG_ARMORPIERCING)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_MG_FMJ", g_sMPTunables.bSALE_WEAPON_AMMO_MG_FMJ)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_MG_INCENDIARY", g_sMPTunables.bSALE_WEAPON_AMMO_MG_INCENDIARY)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_MG_TRACER", g_sMPTunables.bSALE_WEAPON_AMMO_MG_TRACER)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_PISTOL_FMJ", g_sMPTunables.bSALE_WEAPON_AMMO_PISTOL_FMJ)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_PISTOL_HOLLOWPOINT", g_sMPTunables.bSALE_WEAPON_AMMO_PISTOL_HOLLOWPOINT)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_PISTOL_INCENDIARY", g_sMPTunables.bSALE_WEAPON_AMMO_PISTOL_INCENDIARY)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_PISTOL_TRACER", g_sMPTunables.bSALE_WEAPON_AMMO_PISTOL_TRACER)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_RIFLE_ARMORPIERCING", g_sMPTunables.bSALE_WEAPON_AMMO_RIFLE_ARMORPIERCING)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_RIFLE_FMJ", g_sMPTunables.bSALE_WEAPON_AMMO_RIFLE_FMJ)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_RIFLE_INCENDIARY", g_sMPTunables.bSALE_WEAPON_AMMO_RIFLE_INCENDIARY)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_RIFLE_TRACER", g_sMPTunables.bSALE_WEAPON_AMMO_RIFLE_TRACER)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_SMG_FMJ", g_sMPTunables.bSALE_WEAPON_AMMO_SMG_FMJ)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_SMG_HOLLOWPOINT", g_sMPTunables.bSALE_WEAPON_AMMO_SMG_HOLLOWPOINT)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_SMG_INCENDIARY", g_sMPTunables.bSALE_WEAPON_AMMO_SMG_INCENDIARY)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_SMG_TRACER", g_sMPTunables.bSALE_WEAPON_AMMO_SMG_TRACER)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_SNIPER_ARMORPIERCING", g_sMPTunables.bSALE_WEAPON_AMMO_SNIPER_ARMORPIERCING)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_SNIPER_EXPLOSIVE", g_sMPTunables.bSALE_WEAPON_AMMO_SNIPER_EXPLOSIVE)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_SNIPER_FMJ", g_sMPTunables.bSALE_WEAPON_AMMO_SNIPER_FMJ)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_SNIPER_INCENDIARY", g_sMPTunables.bSALE_WEAPON_AMMO_SNIPER_INCENDIARY)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_SNIPER_TRACER", g_sMPTunables.bSALE_WEAPON_AMMO_SNIPER_TRACER)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_SHOTGUN_ARMORPIERCING", g_sMPTunables.bSALE_WEAPON_AMMO_SHOTGUN_ARMORPIERCING)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_SHOTGUN_EXPLOSIVE", g_sMPTunables.bSALE_WEAPON_AMMO_SHOTGUN_EXPLOSIVE)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_SHOTGUN_HOLLOWPOINT", g_sMPTunables.bSALE_WEAPON_AMMO_SHOTGUN_HOLLOWPOINT)
				ADD_WIDGET_BOOL("bSALE_WEAPON_AMMO_SHOTGUN_INCENDIARY", g_sMPTunables.bSALE_WEAPON_AMMO_SHOTGUN_INCENDIARY)
				
//				ADD_WIDGET_INT_SLIDER("iSALE_WEAPONTYPE_BITSET[113]", g_sMPTunables.iSALE_WEAPONTYPE_BITSET[113])
//				ADD_WIDGET_INT_SLIDER("iINDIVIDUAL_WEAPON_SALE_HASH_LABELS[50]", g_sMPTunables.iINDIVIDUAL_WEAPON_SALE_HASH_LABELS[50])
			STOP_WIDGET_GROUP()
			START_WIDGET_GROUP("Clothing Sale Tunables")
				ADD_WIDGET_BOOL("bALL_CLOTHING_MODS_FOR_SALE", g_sMPTunables.bALL_CLOTHING_MODS_FOR_SALE)
				ADD_WIDGET_BOOL("bSALE_CLOTHING_MENU_LOCATE_TYPE_HEAD", g_sMPTunables.bSALE_CLOTHING_MENU_LOCATE_TYPE_HEAD)
				ADD_WIDGET_BOOL("bSALE_CLOTHING_MENU_LOCATE_TYPE_TORSO", g_sMPTunables.bSALE_CLOTHING_MENU_LOCATE_TYPE_TORSO)
				ADD_WIDGET_BOOL("bSALE_CLOTHING_MENU_LOCATE_TYPE_LEGS", g_sMPTunables.bSALE_CLOTHING_MENU_LOCATE_TYPE_LEGS)
				ADD_WIDGET_BOOL("bSALE_CLOTHING_MENU_LOCATE_TYPE_FEET", g_sMPTunables.bSALE_CLOTHING_MENU_LOCATE_TYPE_FEET)
				ADD_WIDGET_BOOL("bSALE_CLOTHING_MENU_LOCATE_TYPE_HAND", g_sMPTunables.bSALE_CLOTHING_MENU_LOCATE_TYPE_HAND)
				ADD_WIDGET_BOOL("bSALE_CLOTHING_MENU_LOCATE_TYPE_OUTFIT", g_sMPTunables.bSALE_CLOTHING_MENU_LOCATE_TYPE_OUTFIT)
				ADD_WIDGET_BOOL("bSALE_CLOTHING_MENU_LOCATE_TYPE_PROPS", g_sMPTunables.bSALE_CLOTHING_MENU_LOCATE_TYPE_PROPS)
				ADD_WIDGET_BOOL("bSALE_CLOTHING_MENU_LOCATE_TYPE_BODYARMOUR", g_sMPTunables.bSALE_CLOTHING_MENU_LOCATE_TYPE_BODYARMOUR)
//				ADD_WIDGET_INT_SLIDER("iSALE_CLOTHINGBITSET[177]", g_sMPTunables.iSALE_CLOTHINGBITSET[177])
//				ADD_WIDGET_INT_SLIDER("iINDIVIDUAL_CLOTHING_SALE_HASH_LABELS[100]", g_sMPTunables.iINDIVIDUAL_CLOTHING_SALE_HASH_LABELS[100])
			STOP_WIDGET_GROUP()
			ADD_BIT_FIELD_WIDGET("Mask Menu Sale", g_sMPTunables.iSALE_MASKMENUBITSET[0])
			TEXT_LABEL_23 lblWidgetName 
			REPEAT PACK_CLOTHES_SALE_BS_COUNT i
				lblWidgetName  = "PackClothesSale BS "
				lblWidgetName += i
				ADD_BIT_FIELD_WIDGET(lblWidgetName, g_sMPTunables.iBSPackClothesSale[i])
			ENDREPEAT
			START_WIDGET_GROUP("Hairdo Sale Tunables")
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_HAIR", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_HAIR)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_BEARD", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_BEARD)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_EYEBROWS", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_EYEBROWS)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_CHEST", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_CHEST)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_CONTACTS", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_CONTACTS)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_FACEPAINT", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_FACEPAINT)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_MAKEUP", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_MAKEUP)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_MAKEUP_BLUSHER", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_MAKEUP_BLUSHER)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_MAKEUP_EYE", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_MAKEUP_EYE)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_MAKEUP_LIPSTICK", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_MAKEUP_LIPSTICK)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Tattoo Sale Tunables")
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_ALL_TATTOOS", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_ALL_TATTOOS)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_TORSO_BACK", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_TORSO_BACK)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_TORSO_BACK_FULL", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_TORSO_BACK_FULL)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_TORSO_CHEST", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_TORSO_CHEST)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_TORSO_STOMACH", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_TORSO_STOMACH)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_HEAD", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HEAD)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_LEFT_ARM", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_LEFT_ARM)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_RIGHT_ARM", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_RIGHT_ARM)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_LEFT_LEG", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_LEFT_LEG)
				ADD_WIDGET_BOOL("BSHOULD_ITEM_BE_DISCOUNTED_RIGHT_LEG", g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_RIGHT_LEG)
			STOP_WIDGET_GROUP()
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Save Items")
		
			START_NEW_WIDGET_COMBO()
				INT iItem
				REPEAT DBG_SAVED_ITEM_TYPE_COUNT iItem
					IF iItem = DBG_SAVED_ITEM_TYPE_VEHICLE
						ADD_TO_WIDGET_COMBO("Vehicle")
					ENDIF
					IF iItem = DBG_SAVED_ITEM_TYPE_WEAPON
						ADD_TO_WIDGET_COMBO("Weapon")
					ENDIF
				ENDREPEAT
			STOP_WIDGET_COMBO("Item Type", sSaveItemDebugData.iSaveItemType)
			
			sSaveItemDebugData.twSaveItemFilename = ADD_TEXT_WIDGET("Filename") SET_CONTENTS_OF_TEXT_WIDGET(sSaveItemDebugData.twSaveItemFilename, "save_item")
			
			ADD_WIDGET_BOOL("Save item to file", sSaveItemDebugData.bSaveItemToFile)
			ADD_WIDGET_BOOL("Set item from file", sSaveItemDebugData.bSetItemFromFile)
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Script Meta Data")
			ADD_WIDGET_BOOL("Generate MP Outift data", b_debug_outfit_output_3)
			ADD_WIDGET_BOOL("Generate MP Base Loctions", b_debug_outfit_output_4)
			/// 0 - will output for all properties.
			ADD_WIDGET_INT_SLIDER("Generate MP Base Loctions - Base Property", i_debug_base_property, 0, MAX_MP_PROPERTIES, 1)
			ADD_WIDGET_INT_SLIDER("Generate MP Base Loctions - Base Property override last element ID", i_debug_base_last_valid_element, 0, MAX_NUMBER_OF_MP_PROP_ELEMENTS, 1)
			ADD_WIDGET_BOOL("Generate MP Base Loctions - Dump ScriptData to log DEBUG_SHOPS", b_debug_dump_xml_property_to_log)
			ADD_WIDGET_BOOL("Generate MP Base Loctions - Dump METADATA to log DEBUG_SHOPS", b_debug_dump_metadata_property_to_log)
			ADD_WIDGET_BOOL("Generate MP Base Loctions - Compare Metadata to ScriptData, log DEBUG_SHOPS", b_debug_compare_property_metadata_with_xml)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Shops")
			START_WIDGET_GROUP("Current Item Data")
				tw_debug_current_item_label = ADD_TEXT_WIDGET("Label")
				SET_CONTENTS_OF_TEXT_WIDGET(tw_debug_current_item_label, g_sShopSettings.tlCurrentItem_label)
				ADD_WIDGET_INT_READ_ONLY("Cost", g_sShopSettings.iCurrentItem_cost)
				ADD_WIDGET_INT_READ_ONLY("Hash", g_sShopSettings.iCurrentItem_hash)
				ADD_WIDGET_INT_READ_ONLY("Count ", g_sShopSettings.iCurrentItem_count)
				ADD_WIDGET_INT_READ_ONLY("Colour", g_sShopSettings.iCurrentItem_colour)
				ADD_WIDGET_BOOL("On Sale", g_sShopSettings.bCurrentItem_onsale)
			STOP_WIDGET_GROUP()
			ADD_WIDGET_BOOL("Prevent Shop Cutscene Cleanup", g_bPreventShopCutsceneCleanup)
			ADD_WIDGET_BOOL("Force current shop to reset", b_debug_force_reset)
			ADD_WIDGET_BOOL("Display shop debug", g_bDisplayShopDebug)
			ADD_WIDGET_BOOL("Hide all shop blips", bHideShopBlipsState)
			ADD_WIDGET_BOOL("Unlock all shop items", lw_bUnlockAllShopItems)
			ADD_WIDGET_BOOL("Rockstar dev test", g_sShopSettings.bDevTest)
//			ADD_WIDGET_BOOL("Unlock some reward t-shirts", b_debug_unlock_reward_shirts)
			ADD_WIDGET_BOOL("Check for reward shirt", b_debug_reward_shirt_check)
			
			ADD_WIDGET_BOOL("launch store cash alert", b_debug_launch_store)
			ADD_WIDGET_BOOL("Is warning message on screen?", b_debug_warning_message)
			
			ADD_WIDGET_BOOL("Main update disabled", bDisableMainUpdates)
			ADD_WIDGET_INT_READ_ONLY("Main update stage", iDisableMainUpdatesStage)
			ADD_WIDGET_BOOL("On SP mission", bPlayerOnSPMission)
			ADD_WIDGET_BOOL("On MP mission", bPlayerOnMPMission)
			ADD_WIDGET_BOOL("On MP activity tutorial", bPlayerOnMPActivityTurorial)
			ADD_WIDGET_BOOL("MP tutorial complete", bTutorialsComplete)
			
			ADD_WIDGET_BOOL("Allow mask shop on mission", g_bAllowClothesShopInMission)
			
			ADD_WIDGET_BOOL("g_bScriptsSetSafeForCutscene (READ ONLY)", g_bScriptsSetSafeForCutscene)
			ADD_WIDGET_BOOL("Process store alert (READ ONLY)", g_sShopSettings.bProcessStoreAlert)
			
			ADD_WIDGET_BOOL("Clear vehicle txt timer", b_debug_clear_veh_txt_delay)
			
			ADD_WIDGET_BOOL("Draw Debug Shop Info", bDrawDebugHelmetStuff)
			ADD_WIDGET_BOOL("GLOBAL Safe To Process MP Checks", g_bSafeToProcessMPChecks)
			ADD_WIDGET_BOOL("Force Start Car Modding For Mission Carmod", g_bForceStartMissionCarMod)
			ADD_WIDGET_BOOL("Force Ready Car Mod For Mission launch", g_bForceReadyCarModForMissionLaunch)
			ADD_WIDGET_BOOL("Force Bail personal car mod", g_bForceBailMissionCarMod)
			ADD_WIDGET_BOOL("Force Start Office personal car mod", g_bForceStartCarModScriptForOffice)
			
			ADD_WIDGET_BOOL("All clothes available", g_bAllClothesAvailable)
			ADD_WIDGET_BOOL("All clothes acquired", g_bAllClothesAcquired)
			ADD_WIDGET_BOOL("All tattoos acquired", g_bAllTattoosAcquired)
			ADD_WIDGET_BOOL("All weapons acquired", g_bAllWeaponsAcquired)

			ADD_WIDGET_BOOL("SAVE All clothes acquired", b_debug_save_AllClothesAcquired)
			ADD_WIDGET_BOOL("SAVE All tattoos acquired", b_debug_save_AllTattoosAcquired)
			ADD_WIDGET_BOOL("SAVE All weapons acquired", b_debug_save_AllWeaponsAcquired)
			
			ADD_WIDGET_BOOL("All snacks acquired", b_debug_AllSnacksAcquired)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Stats Debug")
			ADD_WIDGET_BOOL("Output vehicle display slots", b_debug_stats_output_1)
			ADD_WIDGET_BOOL("Output vehicle models", b_debug_stats_output_2)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Tattoos")
			ADD_WIDGET_BOOL("Output tattoo data", b_debug_output_tattoo_data)
			ADD_WIDGET_BOOL("Print Bitset Tattoo", b_debug_print_bitset_tattoo)
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("TATTOO_SP_MICHAEL")
				ADD_TO_WIDGET_COMBO("TATTOO_SP_FRANKLIN")
				ADD_TO_WIDGET_COMBO("TATTOO_SP_TREVOR")
				ADD_TO_WIDGET_COMBO("TATTOO_MP_FM")
				ADD_TO_WIDGET_COMBO("TATTOO_MP_FM_F")
				ADD_TO_WIDGET_COMBO("*unset*")
			STOP_WIDGET_COMBO("Component", i_debug_tattoo_faction)
			i_debug_tattoo_faction = ENUM_TO_INT(TATTOO_MP_FM_F)+1
			w_debug_tattoo_name = ADD_TEXT_WIDGET("name")
			SET_CONTENTS_OF_TEXT_WIDGET(w_debug_tattoo_name, "null")
			ADD_WIDGET_INT_READ_ONLY("tattoo name hash", i_debug_tattoo_namehash)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Vehicle Data")
			w_debug_veh_name = ADD_TEXT_WIDGET("Name")
			SET_CONTENTS_OF_TEXT_WIDGET(w_debug_veh_name, "")
			ADD_WIDGET_BOOL("Output vehicle data", b_debug_output_veh_data)
			ADD_WIDGET_BOOL("Output vehicle data for focused vehicle", b_debug_output_veh_data_on_focused_veh)
			ADD_WIDGET_BOOL("Output vehicle data + move", b_debug_output_veh_data_move)
			
			ADD_WIDGET_BOOL("Output CURRENT veh mass + armour", b_debug_output_CURRENT_veh_mass_and_armour)
			ADD_WIDGET_BOOL("Output BUYABLE veh mass + armour", b_debug_output_BUYABLE_veh_mass_and_armour)
			tw_debug_pegasus_count = ADD_TEXT_WIDGET("tw_debug_pegasus_count")
			SET_CONTENTS_OF_TEXT_WIDGET(tw_debug_pegasus_count, "none")
			ADD_WIDGET_INT_SLIDER("i_debug_pegasus_stage", i_debug_pegasus_stage, 0, 5, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Weapons")
			ADD_WIDGET_BOOL("GET_PED_WEAPONS", b_debug_get_weapons)
			ADD_WIDGET_BOOL("SET_PED_WEAPONS", b_debug_set_weapons)
			ADD_WIDGET_BOOL("REMOVE_PED_WEAPONS", b_debug_remove_weapons)
			ADD_WIDGET_BOOL("Output current weapon data", b_debug_output_weapon_data)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Stored Parachute")
			ADD_WIDGET_BOOL("Equip Stored Parachute", b_debug_equip_stored_parachute)
			ADD_WIDGET_INT_SLIDER("tint", i_debug_stored_parachute_tint, -1, 100, 1)
			ADD_WIDGET_INT_SLIDER("bag index", i_debug_stored_parachute_bag_index, -1, 100, 1)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Visual Aid Overrides")
			ADD_WIDGET_BOOL("Test Visual Aid Overrides", b_debug_test_visual_aid_override)
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO(GET_VISUAL_AID_MODE_NAME(VISUALAID_DEFAULT))
				ADD_TO_WIDGET_COMBO(GET_VISUAL_AID_MODE_NAME(VISUALAID_OFF))
				ADD_TO_WIDGET_COMBO(GET_VISUAL_AID_MODE_NAME(VISUALAID_NIGHT))
				ADD_TO_WIDGET_COMBO(GET_VISUAL_AID_MODE_NAME(VISUALAID_THERMAL))
			STOP_WIDGET_COMBO("Visual Aid Override", i_debug_visual_aid_override)
			
			ADD_WIDGET_BOOL("bMaintainOverride", g_sFMMCVisualAid.bMaintainOverride)
			
			tw_debug_visual_aid_override_script = ADD_TEXT_WIDGET("tl31OverrideScriptName")
			SET_CONTENTS_OF_TEXT_WIDGET(tw_debug_visual_aid_override_script, g_sFMMCVisualAid.tl31OverrideScriptName)
			
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO(GET_VISUAL_AID_SOUND_NAME(VISUALAID_SOUND_ON))
				ADD_TO_WIDGET_COMBO(GET_VISUAL_AID_SOUND_NAME(VISUALAID_SOUND_ALT))
				ADD_TO_WIDGET_COMBO(GET_VISUAL_AID_SOUND_NAME(VISUALAID_SOUND_OFF))
			STOP_WIDGET_COMBO("bUseSound", i_debug_VISUAL_AID_SOUND)
			
			ADD_WIDGET_BOOL("bDrawDebug", sVisualAidData.bDrawDebug)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Starter Pack - launch website")
			ADD_WIDGET_STRING("// Property")
			ADD_WIDGET_BOOL("MAZE_BANK_WEST_EXECUTIVE_OFFICE",			b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_MAZE_BANK_WEST_EXECUTIVE_OFFICE])
			ADD_WIDGET_BOOL("PALETO_FOREST_BUNKER",						b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_PALETO_FOREST_BUNKER])
			ADD_WIDGET_BOOL("GREAT_CHAPARRAL_BIKER_CLUBHOUSE",			b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_GREAT_CHAPARRAL_BIKER_CLUBHOUSE])
		//	ADD_WIDGET_BOOL("SENORA_DESERT_CONTERFEIT_CASH_FACTORY",	b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_SENORA_DESERT_CONTERFEIT_CASH_FACTORY])
			ADD_WIDGET_BOOL("1561_SAN_VITAS_STREET_APARTMENT",			b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1561_SAN_VITAS_STREET_APARTMENT])
			ADD_WIDGET_BOOL("1337_EXCEPTIONALISTS_WAY_10_CAR_GARAGE",	b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1337_EXCEPTIONALISTS_WAY_10_CAR_GARAGE])
			
			ADD_WIDGET_STRING("// Vehicles")
			ADD_WIDGET_BOOL("BF_DUNE_FAV",								b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_BF_DUNE_FAV])
			ADD_WIDGET_BOOL("PEGASSI_VORTEX",							b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_PEGASSI_VORTEX])
			ADD_WIDGET_BOOL("OBEY_OMNIS",								b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_OBEY_OMNIS])
			ADD_WIDGET_BOOL("MAIBATSU_FROGGER",							b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_MAIBATSU_FROGGER])
			ADD_WIDGET_BOOL("WESTERN_ZOMBIE_CHOPPER",					b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_WESTERN_ZOMBIE_CHOPPER])
			ADD_WIDGET_BOOL("GROTTI_TURISMO_R",							b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_GROTTI_TURISMO_R])
			ADD_WIDGET_BOOL("ENUS_WINDSOR",								b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_ENUS_WINDSOR])
			ADD_WIDGET_BOOL("BRAVADO_BANSHEE",							b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_BRAVADO_BANSHEE])
			ADD_WIDGET_BOOL("INVETERO_COQUETTE_CLASSIC",				b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_INVETERO_COQUETTE_CLASSIC])
			ADD_WIDGET_BOOL("ENUS_HUNTLEY_S",							b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_ENUS_HUNTLEY_S])
			
			ADD_WIDGET_STRING("// Clear all")
			ADD_WIDGET_BOOL("b_debug_clear_player_purchased_CESP_content",	b_debug_clear_player_purchased_CESP_content)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Tuner Client Auto Shop")
			ADD_WIDGET_BOOL("Bypass auto shop random timer",							bByPassAutoShopRandomTimer)
			ADD_WIDGET_BOOL("Bypass auto shop spawn weight",							bByPassAutoShopSpawnWeight)
			ADD_WIDGET_INT_SLIDER("Vehicle ID", iTunerClientVehicleID, 0, 40, 1)
			TunerClientVehicleName = ADD_TEXT_WIDGET("Vehicle ID Name")
			ADD_WIDGET_BOOL("Reset Auto Shop Timer Stat",								bResetAutoShopVehiclePickTimer)
			ADD_WIDGET_BOOL("Reset Auto Shop Vehicle Model",							bResetAutoShopVehicleModels)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Biker Client Auto Shop")
			ADD_WIDGET_BOOL("Bypass Biker Client random timer",							bByPassBikerShopRandomTimer)
			ADD_WIDGET_BOOL("Bypass Biker Client spawn weight",							bByPassBikerShopSpawnWeight)
			ADD_WIDGET_INT_SLIDER("Vehicle ID", iBikerClientVehicleID, 0, 19, 1)
			BikerClientVehicleName = ADD_TEXT_WIDGET("Vehicle ID Name")
			ADD_WIDGET_BOOL("Reset Biker Client Timer Stat",							bResetBikerShopVehiclePickTimer)
			ADD_WIDGET_BOOL("Reset Biker Client Vehicle Model",							bResetBikerShopVehicleModels)
		STOP_WIDGET_GROUP()
		
		IF NOT IS_PC_VERSION()
			START_WIDGET_GROUP("Quickfix gift GTAO items")
				ADD_WIDGET_BOOL("Quickfix gift GTAO items", b_debug_quickfix_gift_gtao_items)
			STOP_WIDGET_GROUP()	
		ENDIF
		
		START_WIDGET_GROUP("Test Player delivered vehicle events")
			ADD_WIDGET_STRING("To use this will require -dontCrashOnScriptValidation")
			ADD_WIDGET_BOOL("Run player delivered vehicle event test widget", eIEVehTestData.b_debugRunIeVehEventMenu)
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Vehicle website stats")
			
			ADD_WIDGET_BOOL("Display owned pegasus vehicles", b_debug_display_owned_pegasus_vehicles)
			ADD_WIDGET_INT_READ_ONLY("Selected owned pegasus vehicles", i_debug_selected_owned_pegasus_vehicles)
			
		STOP_WIDGET_GROUP()
		
		START_WIDGET_GROUP("Pegasus Vehicle Rewarded")
			ADD_WIDGET_BOOL("Set Pegasus vehicle rewarded", b_debug_set_bigass_vehicle_rewarded)
			ADD_WIDGET_INT_SLIDER("Pegasus Vehicle Index", i_debug_set_bigass_vehicle_rewarded_bitindex, -1, 999, 1)
			tw_debug_set_bigass_vehicle_rewarded_bitindex = ADD_TEXT_WIDGET("Pegasus Vehicle Name")
			START_NEW_WIDGET_COMBO()
				ADD_TO_WIDGET_COMBO("unset")
				ADD_TO_WIDGET_COMBO("SUCCESS")
				ADD_TO_WIDGET_COMBO("FAILED_INVALID")
				ADD_TO_WIDGET_COMBO("FAILED_OWNED")
				ADD_TO_WIDGET_COMBO("FAILED_TRANSACTION")
			STOP_WIDGET_COMBO("Result", i_debug_set_bigass_vehicle_rewarded_result)
		STOP_WIDGET_GROUP()
		
		#IF FEATURE_GTAO_MEMBERSHIP
		GTAO_MEMBERSHIP_CREATE_DEBUG_WIDGETS(sGTAOMembership.debug)
		#ENDIF
		
		#IF FEATURE_GEN9_EXCLUSIVE
		START_WIDGET_GROUP("HSW MOD Coupon")
			
			ADD_WIDGET_BOOL("Display HSW Mod Coupon Info", bDisplayHSWModCouponInfo)
			
			STRING sCouponName
			INT iStart = ENUM_TO_INT(COUPON_HSW_MOD1) 
			INT iEnd = iStart + (g_sMPTunables.iHSW_MOD_COUPON_LIMIT - 1)
			INT j, iIndex 
			FOR j = iStart TO iEnd
				iDebugHSWModCoupon[iIndex] = -1
				sCouponName = GET_DEBUG_HSW_MOD_COUPON_NAME(INT_TO_ENUM(COUPON_TYPE, j))
				ADD_WIDGET_INT_SLIDER(sCouponName, iDebugHSWModCoupon[iIndex], -1, HIGHEST_INT, 1)
				iIndex++
			ENDFOR	
			
			ADD_WIDGET_BOOL("Obtain HSW Mod Coupon", bObtainHSWModCoupon)
			ADD_WIDGET_BOOL("Redeem HSW Mod Coupon", bRedeemHSWModCoupon)
		STOP_WIDGET_GROUP()
		#ENDIF
	STOP_WIDGET_GROUP()
ENDPROC

PROC ADD_PED_COMP_DATA_TO_FILE(PED_COMP_TYPE_ENUM eCompType, INT iType)

	PED_COMP_NAME_ENUM ePedComp = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), eCompType)
	INT iDLCNameHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(GET_PED_COMPONENT_FROM_TYPE(eCompType)), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), GET_PED_COMPONENT_FROM_TYPE(eCompType)), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), GET_PED_COMPONENT_FROM_TYPE(eCompType)))
	
	IF iDLCNameHash = 0
		iDLCNameHash = -1
	ENDIF
	
	TEXT_LABEL_63 tlNameHash
	
	IF iType = 0
		IF iDLCNameHash = -1
			SAVE_STRING_TO_DEBUG_FILE("	sOutfitsData.iComponentDrawableID[")SAVE_STRING_TO_DEBUG_FILE(GET_COMP_VARIATION_TYPE_STRING(GET_PED_COMPONENT_FROM_TYPE(eCompType))) SAVE_STRING_TO_DEBUG_FILE("] = ")SAVE_INT_TO_DEBUG_FILE(GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), GET_PED_COMPONENT_FROM_TYPE(eCompType)))
			SAVE_STRING_TO_DEBUG_FILE("	sOutfitsData.iComponentTextureID[")SAVE_STRING_TO_DEBUG_FILE(GET_COMP_VARIATION_TYPE_STRING(GET_PED_COMPONENT_FROM_TYPE(eCompType))) SAVE_STRING_TO_DEBUG_FILE("] = ")SAVE_INT_TO_DEBUG_FILE(GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), GET_PED_COMPONENT_FROM_TYPE(eCompType)))
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ELSE
			IF GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlNameHash)
				SAVE_STRING_TO_DEBUG_FILE("	GET_SHOP_PED_COMPONENT(ENUM_TO_INT(")SAVE_STRING_TO_DEBUG_FILE(tlNameHash)SAVE_STRING_TO_DEBUG_FILE("), componentItem)")
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("	GET_SHOP_PED_COMPONENT(")SAVE_INT_TO_DEBUG_FILE(iDLCNameHash)SAVE_STRING_TO_DEBUG_FILE(", componentItem)   // MISSING DLC NAME")
			ENDIF
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	sOutfitsData.iComponentDrawableID[")SAVE_STRING_TO_DEBUG_FILE(GET_COMP_VARIATION_TYPE_STRING(GET_PED_COMPONENT_FROM_TYPE(eCompType))) SAVE_STRING_TO_DEBUG_FILE("] = componentItem.m_drawableIndex		")SAVE_STRING_TO_DEBUG_FILE("sOutfitsData.iComponentTextureID[")SAVE_STRING_TO_DEBUG_FILE(GET_COMP_VARIATION_TYPE_STRING(GET_PED_COMPONENT_FROM_TYPE(eCompType))) SAVE_STRING_TO_DEBUG_FILE("] = componentItem.m_textureIndex")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ELIF iType = 1
	
		IF iDLCNameHash != -1
			ePedComp = INT_TO_ENUM(PED_COMP_NAME_ENUM, 0)
		ELSE
			iDLCNameHash = 0
		ENDIF
		
		IF ENUM_TO_INT(ePedComp) = 0
		AND iDLCNameHash = 0
			// skip
		ELSE	
			SAVE_STRING_TO_DEBUG_FILE("			<Item>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			IF iDLCNameHash = 0
				SAVE_STRING_TO_DEBUG_FILE("				<nameHash>")SAVE_INT_TO_DEBUG_FILE(iDLCNameHash)SAVE_STRING_TO_DEBUG_FILE("</nameHash>")
			ELIF GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlNameHash)
				SAVE_STRING_TO_DEBUG_FILE("				<nameHash>")SAVE_STRING_TO_DEBUG_FILE(tlNameHash)SAVE_STRING_TO_DEBUG_FILE("</nameHash>")
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("				<nameHash>")SAVE_INT_TO_DEBUG_FILE(iDLCNameHash)SAVE_STRING_TO_DEBUG_FILE("</nameHash>   <!-- MISSING DLC NAME -->")
			ENDIF
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("				<enumValue value=\"") SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(ePedComp))SAVE_STRING_TO_DEBUG_FILE("\" />")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("				<eCompType>")SAVE_STRING_TO_DEBUG_FILE(GET_PED_COMP_DLC_STRING(eCompType))SAVE_STRING_TO_DEBUG_FILE("</eCompType>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("			</Item>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ENDIF
ENDPROC

PROC ADD_PED_PROP_DATA_TO_FILE(PED_PROP_POSITION eAnchor, INT iType)

	IF GET_PED_PROP_INDEX(PLAYER_PED_ID(), eAnchor) = -1
		EXIT
	ENDIF
	
	PED_COMP_NAME_ENUM ePedProp = GET_PROP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), GET_PED_PROP_INDEX(PLAYER_PED_ID(), eAnchor), GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), eAnchor), eAnchor)
	INT iDLCNameHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(eAnchor), GET_PED_PROP_INDEX(PLAYER_PED_ID(), eAnchor), GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), eAnchor))
	
	IF iDLCNameHash = 0
		iDLCNameHash = -1
	ENDIF
	
	TEXT_LABEL_63 tlNameHash
	
	IF iType = 0
		IF iDLCNameHash = -1
			SAVE_STRING_TO_DEBUG_FILE("	sOutfitsData.iPropDrawableID[")SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_STRING(eAnchor))SAVE_STRING_TO_DEBUG_FILE("] = ") SAVE_INT_TO_DEBUG_FILE(GET_PED_PROP_INDEX(PLAYER_PED_ID(), eAnchor))
			SAVE_STRING_TO_DEBUG_FILE("	sOutfitsData.iPropTextureID[")SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_STRING(eAnchor))SAVE_STRING_TO_DEBUG_FILE("] = ") SAVE_INT_TO_DEBUG_FILE(GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), eAnchor))
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ELSE
			IF GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlNameHash)
				SAVE_STRING_TO_DEBUG_FILE("	GET_SHOP_PED_PROP(ENUM_TO_INT(")SAVE_STRING_TO_DEBUG_FILE(tlNameHash)SAVE_STRING_TO_DEBUG_FILE("), propItem)")
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("	GET_SHOP_PED_PROP(")SAVE_INT_TO_DEBUG_FILE(iDLCNameHash)SAVE_STRING_TO_DEBUG_FILE(", propItem) // MISSING DLC NAME")
			ENDIF
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("	sOutfitsData.iPropDrawableID[")SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_STRING(eAnchor))SAVE_STRING_TO_DEBUG_FILE("] = propItem.m_propIndex")
			SAVE_STRING_TO_DEBUG_FILE("	sOutfitsData.iPropTextureID[")SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_STRING(eAnchor))SAVE_STRING_TO_DEBUG_FILE("] = propItem.m_textureIndex")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ELIF iType = 1
		IF iDLCNameHash != -1
			ePedProp = INT_TO_ENUM(PED_COMP_NAME_ENUM, 0)
		ELSE
			iDLCNameHash = 0
		ENDIF
		
		IF ENUM_TO_INT(ePedProp) = 0
		AND iDLCNameHash = 0
			// skip
		ELSE
			SAVE_STRING_TO_DEBUG_FILE("			<Item>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			IF iDLCNameHash = 0
				SAVE_STRING_TO_DEBUG_FILE("				<nameHash>")SAVE_INT_TO_DEBUG_FILE(iDLCNameHash)SAVE_STRING_TO_DEBUG_FILE("</nameHash>")
			ELIF GET_DLC_NAME_HASH_STRING(iDLCNameHash, tlNameHash)
				SAVE_STRING_TO_DEBUG_FILE("				<nameHash>")SAVE_STRING_TO_DEBUG_FILE(tlNameHash)SAVE_STRING_TO_DEBUG_FILE("</nameHash>")
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("				<nameHash>")SAVE_INT_TO_DEBUG_FILE(iDLCNameHash)SAVE_STRING_TO_DEBUG_FILE("</nameHash>   <!-- MISSING DLC NAME -->")
			ENDIF
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("				<enumValue value=\"") SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(ePedProp))SAVE_STRING_TO_DEBUG_FILE("\" />")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("				<eAnchorPoint>")SAVE_STRING_TO_DEBUG_FILE(GET_PROP_POSITION_STRING(eAnchor))SAVE_STRING_TO_DEBUG_FILE("</eAnchorPoint>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("			</Item>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ENDIF
ENDPROC

PROC EXPORT_MP_OUTFIT_DATA_TO_XML()

	INT iStart = ENUM_TO_INT(OUTFIT_MP_FREEMODE)
	INT iEnd = ENUM_TO_INT(OUTFIT_MAX_AMOUNT)-1
	INT iOutfit
	INT i
	MP_OUTFITS_DATA sOutfitsDataTemp
	
	OPEN_DEBUG_FILE()
	
	SAVE_STRING_TO_DEBUG_FILE("<?xml version=\"1.0\" encoding=\"UTF-8\"?>")
	
	SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_NEWLINE_TO_DEBUG_FILE()
	
	SAVE_STRING_TO_DEBUG_FILE("<CScriptMetadata>")SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("	<MPOutfits>")SAVE_NEWLINE_TO_DEBUG_FILE()
	
//	<MPOutfitsDataMale>
//		<MPOutfitsData>
//			<Item>
//				<OutfitEnum>OUTFIT_MP_FREEMODE</OutfitEnum>
//				<ComponentDrawables>
//					<pedComponentDrawable0 value="0"/>
//				</ComponentDrawables>
//				<ComponentTextures>
//					<pedComponentTexture0 value="0"/>
//				</ComponentTextures>
//				<PropIndices>
//					<pedPropIndex0 value="0"/>
//				</PropIndices>
//				<PropTextures>
//					<pedPropTexture8 value="0"/>
//				</PropTextures>
//				<TattooHashes>
//					<pedTattooHash0 value="0"/>
//				</TattooHashes>
//			</Item>
//		</MPOutfitsData>
//	</MPOutfitsDataMale>


	INT iPed
	REPEAT 2 iPed
	
		IF iPed = 0
		
			REQUEST_MODEL(MP_M_FREEMODE_01)
			WHILE NOT HAS_MODEL_LOADED(MP_M_FREEMODE_01)
				WAIT(0)
			ENDWHILE
			SET_PLAYER_MODEL(PLAYER_ID(), MP_M_FREEMODE_01)
			SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
			SET_MODEL_AS_NO_LONGER_NEEDED(MP_M_FREEMODE_01)
		
			SAVE_STRING_TO_DEBUG_FILE("		<MPOutfitsDataMale>")SAVE_NEWLINE_TO_DEBUG_FILE()
		ELIF iPed = 1
			
			REQUEST_MODEL(MP_F_FREEMODE_01)
			WHILE NOT HAS_MODEL_LOADED(MP_F_FREEMODE_01)
				WAIT(0)
			ENDWHILE
			SET_PLAYER_MODEL(PLAYER_ID(), MP_F_FREEMODE_01)
			SET_PED_DEFAULT_COMPONENT_VARIATION(PLAYER_PED_ID())
			SET_MODEL_AS_NO_LONGER_NEEDED(MP_F_FREEMODE_01)
			
			SAVE_STRING_TO_DEBUG_FILE("		<MPOutfitsDataFemale>")SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
		
		SAVE_STRING_TO_DEBUG_FILE("			<MPOutfitsData>")SAVE_NEWLINE_TO_DEBUG_FILE()
		
		FOR iOutfit = iStart TO iEnd
		
			if iOutfit % 20 = 0
				wait(0)
			endif
			
			IF iPed = 0
				GET_MP_OUTFIT_DATA_FOR_GENERATING_XML(MP_M_FREEMODE_01, INT_TO_ENUM(MP_OUTFIT_ENUM, iOutfit), sOutfitsDataTemp)
			ELIF iPed = 1
				GET_MP_OUTFIT_DATA_FOR_GENERATING_XML(MP_F_FREEMODE_01, INT_TO_ENUM(MP_OUTFIT_ENUM, iOutfit), sOutfitsDataTemp)
			ENDIF
			
			SAVE_STRING_TO_DEBUG_FILE("				<Item> <!-- ")SAVE_INT_TO_DEBUG_FILE(iOutfit)SAVE_STRING_TO_DEBUG_FILE(" -->")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			// Components Drawables
			SAVE_STRING_TO_DEBUG_FILE("					<ComponentDrawables>")SAVE_NEWLINE_TO_DEBUG_FILE()
				REPEAT COUNT_OF(sOutfitsDataTemp.iComponentDrawableID) i
					SAVE_STRING_TO_DEBUG_FILE("						<pedComponentDrawable")SAVE_INT_TO_DEBUG_FILE(i)SAVE_STRING_TO_DEBUG_FILE(" value=\"")SAVE_INT_TO_DEBUG_FILE(sOutfitsDataTemp.iComponentDrawableID[i])SAVE_STRING_TO_DEBUG_FILE("\"/>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDREPEAT
			SAVE_STRING_TO_DEBUG_FILE("					</ComponentDrawables>")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			// Component Textures
			SAVE_STRING_TO_DEBUG_FILE("					<ComponentTextures>")SAVE_NEWLINE_TO_DEBUG_FILE()
				REPEAT COUNT_OF(sOutfitsDataTemp.iComponentTextureID) i
					SAVE_STRING_TO_DEBUG_FILE("						<pedComponentTexture")SAVE_INT_TO_DEBUG_FILE(i)SAVE_STRING_TO_DEBUG_FILE(" value=\"")SAVE_INT_TO_DEBUG_FILE(sOutfitsDataTemp.iComponentTextureID[i])SAVE_STRING_TO_DEBUG_FILE("\"/>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDREPEAT
			SAVE_STRING_TO_DEBUG_FILE("					</ComponentTextures>")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			// Prop Index
			SAVE_STRING_TO_DEBUG_FILE("					<PropIndices>")SAVE_NEWLINE_TO_DEBUG_FILE()
				REPEAT COUNT_OF(sOutfitsDataTemp.iPropDrawableID) i
					SAVE_STRING_TO_DEBUG_FILE("						<pedPropIndex")SAVE_INT_TO_DEBUG_FILE(i)SAVE_STRING_TO_DEBUG_FILE(" value=\"")SAVE_INT_TO_DEBUG_FILE(sOutfitsDataTemp.iPropDrawableID[i])SAVE_STRING_TO_DEBUG_FILE("\"/>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDREPEAT
			SAVE_STRING_TO_DEBUG_FILE("					</PropIndices>")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			// Prop Texture
			SAVE_STRING_TO_DEBUG_FILE("					<PropTextures>")SAVE_NEWLINE_TO_DEBUG_FILE()
				REPEAT COUNT_OF(sOutfitsDataTemp.iPropTextureID) i
					SAVE_STRING_TO_DEBUG_FILE("						<pedPropTexture")SAVE_INT_TO_DEBUG_FILE(i)SAVE_STRING_TO_DEBUG_FILE(" value=\"")SAVE_INT_TO_DEBUG_FILE(sOutfitsDataTemp.iPropTextureID[i])SAVE_STRING_TO_DEBUG_FILE("\"/>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDREPEAT
			SAVE_STRING_TO_DEBUG_FILE("					</PropTextures>")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			// Tattoo Hashes
			SAVE_STRING_TO_DEBUG_FILE("					<TattooHashes>")SAVE_NEWLINE_TO_DEBUG_FILE()
				REPEAT COUNT_OF(sOutfitsDataTemp.iDLCTattooOverlay) i
					SAVE_STRING_TO_DEBUG_FILE("						<pedTattooHash")SAVE_INT_TO_DEBUG_FILE(i)SAVE_STRING_TO_DEBUG_FILE(" value=\"")SAVE_INT_TO_DEBUG_FILE(sOutfitsDataTemp.iDLCTattooOverlay[i])SAVE_STRING_TO_DEBUG_FILE("\"/>")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDREPEAT
			SAVE_STRING_TO_DEBUG_FILE("					</TattooHashes>")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("				</Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
			
		ENDFOR
		
		SAVE_STRING_TO_DEBUG_FILE("			</MPOutfitsData>")SAVE_NEWLINE_TO_DEBUG_FILE()
		
		
		IF iPed = 0
			SAVE_STRING_TO_DEBUG_FILE("		</MPOutfitsDataMale>")SAVE_NEWLINE_TO_DEBUG_FILE()
		ELIF iPed = 1
			SAVE_STRING_TO_DEBUG_FILE("		</MPOutfitsDataFemale>")SAVE_NEWLINE_TO_DEBUG_FILE()
		ENDIF
	ENDREPEAT
	
	SAVE_STRING_TO_DEBUG_FILE("	</MPOutfits>")SAVE_NEWLINE_TO_DEBUG_FILE()
	SAVE_STRING_TO_DEBUG_FILE("</CScriptMetadata>")SAVE_NEWLINE_TO_DEBUG_FILE()
	
	CLOSE_DEBUG_FILE()
ENDPROC

INT iGarageVehicleResult
VEHICLE_INDEX vehGarageVeh
INT iCharAppearanceControl

PROC OUTPUT_OUTFIT_SETUP_META(STRING sFilename = NULL)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		OPEN_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			IF IS_STRING_NULL_OR_EMPTY(sFilename)
				SAVE_STRING_TO_DEBUG_FILE("	<!-- OUTFIT -->")
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("	<!-- OUTFIT ")
				SAVE_STRING_TO_DEBUG_FILE(sFilename)
				SAVE_STRING_TO_DEBUG_FILE(" -->")
			ENDIF
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("	<Item>")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("		<lockHash />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("		<cost value=\"99\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("		<textLabel>CLO_OUTFIT_LABEL</textLabel>")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("		<uniqueNameHash>DLC_MP_OUTFIT_HASH</uniqueNameHash>")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("		<eShopEnum>CLO_SHOP_LOW</eShopEnum>")SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("		<locate value=\"999\" />")SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("		<includedPedComponents>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_BERD, 1)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_TORSO, 1)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_LEGS, 1)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_FEET, 1)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_TEETH, 1)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_SPECIAL, 1)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_SPECIAL2, 1)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_DECL, 1)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_JBIB, 1)
			
			SAVE_STRING_TO_DEBUG_FILE("		</includedPedComponents>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_STRING_TO_DEBUG_FILE("		<includedPedProps>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_HEAD, 1)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_EYES, 1)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_EARS, 1)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_MOUTH, 1)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_LEFT_HAND, 1)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_RIGHT_HAND, 1)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_LEFT_WRIST, 1)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_RIGHT_WRIST, 1)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_HIP, 1)
			
			SAVE_STRING_TO_DEBUG_FILE("		</includedPedProps>")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			SAVE_STRING_TO_DEBUG_FILE("	</Item>")
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
	ENDIF
ENDPROC

PROC OUTPUT_OUTFIT_SETUP_HEIST(STRING sFilename = NULL)
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		OPEN_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			IF IS_STRING_NULL_OR_EMPTY(sFilename)
				SAVE_STRING_TO_DEBUG_FILE("CASE MP_OUTFIT_ENUM")
			ELSE
				SAVE_STRING_TO_DEBUG_FILE("CASE ")
				SAVE_STRING_TO_DEBUG_FILE(sFilename)
			ENDIF
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_BERD, 0)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_TORSO, 0)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_LEGS, 0)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_FEET, 0)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_TEETH, 0)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_SPECIAL, 0)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_SPECIAL2, 0)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_DECL, 0)
			ADD_PED_COMP_DATA_TO_FILE(COMP_TYPE_JBIB, 0)
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_HEAD, 0)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_EYES, 0)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_EARS, 0)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_MOUTH, 0)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_LEFT_HAND, 0)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_RIGHT_HAND, 0)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_LEFT_WRIST, 0)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_RIGHT_WRIST, 0)
			ADD_PED_PROP_DATA_TO_FILE(ANCHOR_HIP, 0)
			
			SAVE_STRING_TO_DEBUG_FILE("BREAK")
			SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
	ENDIF
ENDPROC

PROC ADD_CASINO_DECORATION_INVENTORY_TO_CATALOGUE()
	ScriptCatalogItem sCatalogueData
	TEXT_LABEL_63 sItemText
	INT iIndex 
	
	FOR iIndex = 1 TO (ENUM_TO_INT(CPM_MAX_ITEMS) - 1)
		PRINTLN("ADD_CASINO_DECORATION_INVENTORY_TO_CATALOGUE iIndex: ", iIndex)
		sItemText = GET_MENU_ITEM_NAME(INT_TO_ENUM(CASINO_CPM_ITEM, iIndex))
		IF !IS_STRING_NULL_OR_EMPTY(sItemText)
			sCatalogueData.m_key = "DEC_"
			sCatalogueData.m_key += sItemText
			
			sCatalogueData.m_textTag = sItemText
			sCatalogueData.m_name = GET_STRING_FROM_TEXT_FILE(sCatalogueData.m_textTag)
			
			sCatalogueData.m_category = CATEGORY_DECORATION
			sCatalogueData.m_price = GET_CASINO_DECORATION_ITEM_PRICE(INT_TO_ENUM(CASINO_CPM_ITEM, iIndex))
			sCatalogueData.m_storagetype = NET_SHOP_ISTORAGE_BITFIELD
			sCatalogueData.m_bitsize = 1
			sCatalogueData.m_bitshift = GET_CASINO_DECORATION_BIT(INT_TO_ENUM(CASINO_CPM_ITEM, iIndex))
			sCatalogueData.m_stathash = ENUM_TO_INT(MPIntStatNamesArray[GET_CASINO_DECORATION_ITEM_STAT_NAME(INT_TO_ENUM(CASINO_CPM_ITEM, iIndex))][GET_SLOT_NUMBER(g_iPedComponentSlot)])
			
			IF sCatalogueData.m_stathash != 0
				ADD_ITEM_TO_CATALOG_WITH_CHECK(sCatalogueData)
			ENDIF	
		ELSE
			PRINTLN("ADD_CASINO_DECORATION_INVENTORY_TO_CATALOGUE iIndex: ", iIndex , " empty text label skip!")
		ENDIF
	ENDFOR	
ENDPROC

FUNC BOOL GET_SAVE_ITEM_WEAPON_DATA_FROM_ATTRIBUTE(WEAPON_INFO& sWeaponInfo, INT iAttributeHash, INT iAttribute)
	BOOL bItemFound = FALSE
	SWITCH iAttributeHash
		CASE HASH("weaponType")
			sWeaponInfo.eWeaponType = INT_TO_ENUM(WEAPON_TYPE, GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute))
			bItemFound = TRUE
		BREAK
		CASE HASH("weaponComponents")
			sWeaponInfo.iModsAsBitfield = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("weaponTint")
			sWeaponInfo.iTint = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("weaponCamo")
			sWeaponInfo.iCamo = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
	ENDSWITCH
	RETURN bItemFound
ENDFUNC

FUNC BOOL GET_SAVE_ITEM_VEHICLE_DATA_FROM_ATTRIBUTE(VEHICLE_SETUP_STRUCT_MP& sVehicleDataMP, INT iAttributeHash, INT iAttribute)
	BOOL bItemFound = FALSE
	SWITCH iAttributeHash
		CASE HASH("vehicleModel")
			sVehicleDataMP.VehicleSetup.eModel = INT_TO_ENUM(MODEL_NAMES, GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute))
			bItemFound = TRUE
		BREAK
		CASE HASH("vehiclePlateID")
			sVehicleDataMP.VehicleSetup.iPlateIndex = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehiclePlateText")
			sVehicleDataMP.VehicleSetup.tlPlateText = GET_STRING_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleCol1")
			sVehicleDataMP.VehicleSetup.iColour1 = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleCol2")
			sVehicleDataMP.VehicleSetup.iColour2 = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleColExtra1")
			sVehicleDataMP.VehicleSetup.iColourExtra1 = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleColExtra2")
			sVehicleDataMP.VehicleSetup.iColourExtra2 = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleTyreR")
			sVehicleDataMP.VehicleSetup.iTyreR = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleTyreG")
			sVehicleDataMP.VehicleSetup.iTyreG = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleTyreB")
			sVehicleDataMP.VehicleSetup.iTyreB = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleWindowTint")
			sVehicleDataMP.VehicleSetup.iWindowTintColour = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleLivery")
			sVehicleDataMP.VehicleSetup.iLivery = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleRoof")
			sVehicleDataMP.VehicleSetup.eRoofState = INT_TO_ENUM(CONVERTIBLE_ROOF_STATE, GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute))
		BREAK
		CASE HASH("vehicleWheelType")
			sVehicleDataMP.VehicleSetup.iWheelType = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleCustomR")
			sVehicleDataMP.VehicleSetup.iCustomR = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleCustomG")
			sVehicleDataMP.VehicleSetup.iCustomG = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleCustomB")
			sVehicleDataMP.VehicleSetup.iCustomB = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleNeonR")
			sVehicleDataMP.VehicleSetup.iNeonR = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleNeonG")
			sVehicleDataMP.VehicleSetup.iNeonG = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleNeonB")
			sVehicleDataMP.VehicleSetup.iNeonB = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleFlags")
			sVehicleDataMP.VehicleSetup.iFlags = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK

		CASE HASH("vehicleModIndex0")
			sVehicleDataMP.VehicleSetup.iModIndex[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex1")
			sVehicleDataMP.VehicleSetup.iModIndex[1] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex2")
			sVehicleDataMP.VehicleSetup.iModIndex[2] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex3")
			sVehicleDataMP.VehicleSetup.iModIndex[3] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex4")
			sVehicleDataMP.VehicleSetup.iModIndex[4] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex5")
			sVehicleDataMP.VehicleSetup.iModIndex[5] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex6")
			sVehicleDataMP.VehicleSetup.iModIndex[6] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex7")
			sVehicleDataMP.VehicleSetup.iModIndex[7] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex8")
			sVehicleDataMP.VehicleSetup.iModIndex[8] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex9")
			sVehicleDataMP.VehicleSetup.iModIndex[9] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex10")
			sVehicleDataMP.VehicleSetup.iModIndex[10] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex11")
			sVehicleDataMP.VehicleSetup.iModIndex[11] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex12")
			sVehicleDataMP.VehicleSetup.iModIndex[12] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex13")
			sVehicleDataMP.VehicleSetup.iModIndex[13] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex14")
			sVehicleDataMP.VehicleSetup.iModIndex[14] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex15")
			sVehicleDataMP.VehicleSetup.iModIndex[15] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex16")
			sVehicleDataMP.VehicleSetup.iModIndex[16] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex17")
			sVehicleDataMP.VehicleSetup.iModIndex[17] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex18")
			sVehicleDataMP.VehicleSetup.iModIndex[18] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex19")
			sVehicleDataMP.VehicleSetup.iModIndex[19] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex20")
			sVehicleDataMP.VehicleSetup.iModIndex[20] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex21")
			sVehicleDataMP.VehicleSetup.iModIndex[21] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex22")
			sVehicleDataMP.VehicleSetup.iModIndex[22] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex23")
			sVehicleDataMP.VehicleSetup.iModIndex[23] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex24")
			sVehicleDataMP.VehicleSetup.iModIndex[24] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex25")
			sVehicleDataMP.VehicleSetup.iModIndex[25] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex26")
			sVehicleDataMP.VehicleSetup.iModIndex[26] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex27")
			sVehicleDataMP.VehicleSetup.iModIndex[27] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex28")
			sVehicleDataMP.VehicleSetup.iModIndex[28] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex29")
			sVehicleDataMP.VehicleSetup.iModIndex[29] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex30")
			sVehicleDataMP.VehicleSetup.iModIndex[30] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex31")
			sVehicleDataMP.VehicleSetup.iModIndex[31] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex32")
			sVehicleDataMP.VehicleSetup.iModIndex[32] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex33")
			sVehicleDataMP.VehicleSetup.iModIndex[33] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex34")
			sVehicleDataMP.VehicleSetup.iModIndex[34] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex35")
			sVehicleDataMP.VehicleSetup.iModIndex[35] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex36")
			sVehicleDataMP.VehicleSetup.iModIndex[36] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex37")
			sVehicleDataMP.VehicleSetup.iModIndex[37] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex38")
			sVehicleDataMP.VehicleSetup.iModIndex[38] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex39")
			sVehicleDataMP.VehicleSetup.iModIndex[39] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex40")
			sVehicleDataMP.VehicleSetup.iModIndex[40] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex41")
			sVehicleDataMP.VehicleSetup.iModIndex[41] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex42")
			sVehicleDataMP.VehicleSetup.iModIndex[42] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex43")
			sVehicleDataMP.VehicleSetup.iModIndex[43] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex44")
			sVehicleDataMP.VehicleSetup.iModIndex[44] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex45")
			sVehicleDataMP.VehicleSetup.iModIndex[45] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex46")
			sVehicleDataMP.VehicleSetup.iModIndex[46] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex47")
			sVehicleDataMP.VehicleSetup.iModIndex[47] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModIndex48")
			sVehicleDataMP.VehicleSetup.iModIndex[48] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK

		CASE HASH("vehicleModVar0")
			sVehicleDataMP.VehicleSetup.iModVariation[0] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleModVar1")
			sVehicleDataMP.VehicleSetup.iModVariation[1] = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK

		CASE HASH("vehicleEnvEff")
			sVehicleDataMP.fEnveffScale = GET_FLOAT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleMPFlags")
			sVehicleDataMP.iFlags = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleLivery2")
			sVehicleDataMP.iLivery2 = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleCol5")
			sVehicleDataMP.iColour5 = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
		CASE HASH("vehicleCol6")
			sVehicleDataMP.iColour6 = GET_INT_FROM_XML_NODE_ATTRIBUTE(iAttribute)
		BREAK
	ENDSWITCH
	RETURN bItemFound
ENDFUNC


PROC PROCESS_SAVE_ITEM_DEBUG()

	IF sSaveItemDebugData.bSaveItemToFile
	
		TEXT_LABEL_31 tlFilename
		TEXT_LABEL_63 tlPathname = "X:/gta5/titleupdate/dev_ng/DebugSaveItemData/"
		
		tlFilename = GET_CONTENTS_OF_TEXT_WIDGET(sSaveItemDebugData.twSaveItemFilename)
		tlFilename += ".xml"
		
		CLEAR_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		
		OPEN_NAMED_DEBUG_FILE(tlPathname, tlFilename)
			
			SAVE_STRING_TO_NAMED_DEBUG_FILE("<?xml version=\"1.0\" encoding=\"UTF-8\"?>", tlPathname, tlFilename)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
			SAVE_STRING_TO_NAMED_DEBUG_FILE("<SaveItemData ", tlPathname, tlFilename)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
			
			IF sSaveItemDebugData.iSaveItemType = DBG_SAVED_ITEM_TYPE_VEHICLE
			
				VEHICLE_INDEX vehTemp = GET_PLAYERS_LAST_VEHICLE()
				GET_VEHICLE_SETUP_MP(vehTemp, sSaveItemDebugData.sVehicleDataMP)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleModel=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(ENUM_TO_INT(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.eModel), tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehiclePlateID=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iPlateIndex, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehiclePlateText=\"", tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.tlPlateText, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleCol1=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iColour1, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleCol2=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iColour2, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleColExtra1=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iColourExtra1, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleColExtra2=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iColourExtra2, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleTyreR=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iTyreR, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleTyreG=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iTyreG, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleTyreB=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iTyreB, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleWindowTint=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iWindowTintColour, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleLivery=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iLivery, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleRoof=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(ENUM_TO_INT(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.eRoofState), tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleWheelType=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iWheelType, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleCustomR=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iCustomR, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleCustomG=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iCustomG, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleCustomB=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iCustomB, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleNeonR=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iNeonR, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleNeonG=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iNeonG, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleNeonB=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iNeonB, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleFlags=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iFlags, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				
				INT iSlot
				REPEAT MAX_VEHICLE_MOD_SLOTS iSlot
					SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleModIndex", tlPathname, tlFilename)
					SAVE_INT_TO_NAMED_DEBUG_FILE(iSlot, tlPathname, tlFilename)
					SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlPathname, tlFilename)
					SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iModIndex[iSlot], tlPathname, tlFilename)
					SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
					SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				ENDREPEAT
				
				REPEAT MAX_VEHICLE_MOD_VAR_SLOTS iSlot
					SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleModVar", tlPathname, tlFilename)
					SAVE_INT_TO_NAMED_DEBUG_FILE(iSlot, tlPathname, tlFilename)
					SAVE_STRING_TO_NAMED_DEBUG_FILE("=\"", tlPathname, tlFilename)
					SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.iModVariation[iSlot], tlPathname, tlFilename)
					SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
					SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				ENDREPEAT
				
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleEnvEff=\"", tlPathname, tlFilename)
				SAVE_FLOAT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.fEnveffScale, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleMPFlags=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.iFlags, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleLivery2=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.iLivery2, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleCol5=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.iColour5, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	vehicleCol6=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(sSaveItemDebugData.sVehicleDataMP.iColour6, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				
			ELIF sSaveItemDebugData.iSaveItemType = DBG_SAVED_ITEM_TYPE_WEAPON
			
				scrShopWeaponData weaponData
				scrShopWeaponComponentData compData
				WEAPONCOMPONENT_TYPE eWeaponComp
				WEAPON_TYPE eCurrentWeapon
				GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eCurrentWeapon, FALSE)
				INT iDLCIndex = GET_DLC_WEAPON_DATA_FOR_WEAPON_TYPE(eCurrentWeapon, weaponData)
				INT iComponent
				INT iModsAsBitfield
				
				// MODEL
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	weaponType=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(ENUM_TO_INT(eCurrentWeapon), tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				
				// COMPONENTS
				iComponent = 0
				iModsAsBitfield = 0
				IF iDLCIndex = -1
					eWeaponComp = GET_PLAYER_PED_WEAPON_COMP_FROM_INT(eCurrentWeapon, iComponent)
					WHILE eWeaponComp != WEAPONCOMPONENT_INVALID
						IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), eCurrentWeapon, eWeaponComp)
							SET_BIT(iModsAsBitfield, iComponent)
						ENDIF
						iComponent++
						eWeaponComp = GET_PLAYER_PED_WEAPON_COMP_FROM_INT(eCurrentWeapon, iComponent)
					ENDWHILE
				ELSE
					REPEAT GET_NUM_DLC_WEAPON_COMPONENTS(iDLCIndex) iComponent
						IF GET_DLC_WEAPON_COMPONENT_DATA(iDLCIndex, iComponent, compData)
							IF NOT IGNORE_DLC_WEAPON_COMPONENT(INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
								IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), eCurrentWeapon, INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
									SET_BIT(iModsAsBitfield, iComponent)
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	weaponComponents=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(iModsAsBitfield, tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				
				// TINTS
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	weaponTint=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(GET_PED_WEAPON_TINT_INDEX(PLAYER_PED_ID(), eCurrentWeapon), tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("	weaponCamo=\"", tlPathname, tlFilename)
				SAVE_INT_TO_NAMED_DEBUG_FILE(GET_PED_WEAPON_CAMO_INDEX(PLAYER_PED_ID(), eCurrentWeapon), tlPathname, tlFilename)
				SAVE_STRING_TO_NAMED_DEBUG_FILE("\"", tlPathname, tlFilename)
				SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
			ENDIF
			
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
			SAVE_STRING_TO_NAMED_DEBUG_FILE(" />", tlPathname, tlFilename)
			SAVE_NEWLINE_TO_NAMED_DEBUG_FILE(tlPathname, tlFilename)
		CLOSE_DEBUG_FILE()
		
		sSaveItemDebugData.bSaveItemToFile = FALSE
	ENDIF
	
	IF sSaveItemDebugData.bSetItemFromFile
		
		TEXT_LABEL_31 tlFilename
		TEXT_LABEL_63 tlPathname = "X:/gta5/titleupdate/dev_ng/DebugSaveItemData/"
		
		tlFilename = GET_CONTENTS_OF_TEXT_WIDGET(sSaveItemDebugData.twSaveItemFilename)
		tlFilename += ".xml"
		
		TEXT_LABEL_63 tlXMLFile
		tlXMLFile = tlPathname
		tlXMLFile += tlFilename
		
		// Check that the xml exists and load
		IF NOT LOAD_XML_FILE(tlXMLFile)
		OR GET_NUMBER_OF_XML_NODES() = 0
			// bad file
			DELETE_XML_FILE()
		ELSE
			INT i
			INT iNumNodes = GET_NUMBER_OF_XML_NODES()
			INT iNumAttributes
			INT iAttribute
			TEXT_LABEL_63 txtNodeName
			INT iAttributeHash
			REPEAT iNumNodes i
				txtNodeName = GET_XML_NODE_NAME()
				SWITCH GET_HASH_KEY(txtNodeName)
					CASE HASH("SaveItemData")
						iNumAttributes = GET_NUMBER_OF_XML_NODE_ATTRIBUTES()
						REPEAT iNumAttributes iAttribute
							iAttributeHash = GET_HASH_KEY(GET_XML_NODE_ATTRIBUTE_NAME(iAttribute))
							IF sSaveItemDebugData.iSaveItemType = DBG_SAVED_ITEM_TYPE_WEAPON
								IF GET_SAVE_ITEM_WEAPON_DATA_FROM_ATTRIBUTE(sSaveItemDebugData.sWeaponInfo, iAttributeHash, iAttribute)
									SET_BIT(sSaveItemDebugData.iProcessSetItemBitfield, DBG_SAVED_ITEM_TYPE_WEAPON)
								ENDIF
							ELIF sSaveItemDebugData.iSaveItemType = DBG_SAVED_ITEM_TYPE_VEHICLE
								IF GET_SAVE_ITEM_VEHICLE_DATA_FROM_ATTRIBUTE(sSaveItemDebugData.sVehicleDataMP, iAttributeHash, iAttribute)
									SET_BIT(sSaveItemDebugData.iProcessSetItemBitfield, DBG_SAVED_ITEM_TYPE_VEHICLE)
								ENDIF
							ENDIF
						ENDREPEAT
					BREAK
				ENDSWITCH
				
				GET_NEXT_XML_NODE()
			ENDREPEAT
			
			DELETE_XML_FILE()
			
		ENDIF
		
		sSaveItemDebugData.bSetItemFromFile = FALSE
	ENDIF
	
	IF IS_BIT_SET(sSaveItemDebugData.iProcessSetItemBitfield, DBG_SAVED_ITEM_TYPE_VEHICLE)
	
		BOOL bProcessing = FALSE
		IF sSaveItemDebugData.sVehicleDataMP.VehicleSetup.eModel != DUMMY_MODEL_FOR_SCRIPT
			bProcessing = TRUE
			REQUEST_MODEL(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.eModel)
			IF HAS_MODEL_LOADED(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.eModel)
			
				VEHICLE_INDEX vehTemp = CREATE_VEHICLE(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.eModel, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, 3.5, 0.0 >>), (GET_ENTITY_HEADING(PLAYER_PED_ID()) + 90.0), FALSE)
				SET_VEHICLE_ON_GROUND_PROPERLY(vehTemp)
				SET_VEHICLE_IS_SPRAYED(vehTemp)
				SET_VEHICLE_IS_STOLEN(vehTemp, FALSE)
				SET_VEHICLE_DIRT_LEVEL(vehTemp, 0.0)
				SET_MODEL_AS_NO_LONGER_NEEDED(sSaveItemDebugData.sVehicleDataMP.VehicleSetup.eModel)
				
				SET_VEHICLE_SETUP_MP(vehTemp, sSaveItemDebugData.sVehicleDataMP, FALSE, DEFAULT, TRUE)
				
				DECOR_REMOVE_ALL(vehTemp)
				
				bProcessing = FALSE
			ENDIF
		ENDIF
		
		IF NOT bProcessing
			CLEAR_BIT(sSaveItemDebugData.iProcessSetItemBitfield, DBG_SAVED_ITEM_TYPE_VEHICLE)
		ENDIF
	ENDIF
	
	IF IS_BIT_SET(sSaveItemDebugData.iProcessSetItemBitfield, DBG_SAVED_ITEM_TYPE_WEAPON)
		IF sSaveItemDebugData.sWeaponInfo.eWeaponType != WEAPONTYPE_INVALID
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), sSaveItemDebugData.sWeaponInfo.eWeaponType, 999, TRUE, TRUE)
			SET_PED_WEAPON_TINT_INDEX(PLAYER_PED_ID(), sSaveItemDebugData.sWeaponInfo.eWeaponType, sSaveItemDebugData.sWeaponInfo.iTint)
			SET_PED_WEAPON_CAMO_INDEX(PLAYER_PED_ID(), sSaveItemDebugData.sWeaponInfo.eWeaponType, sSaveItemDebugData.sWeaponInfo.iCamo)
			
			scrShopWeaponData weaponData
			scrShopWeaponComponentData compData
			WEAPONCOMPONENT_TYPE eWeaponComp
			INT iDLCIndex = GET_DLC_WEAPON_DATA_FOR_WEAPON_TYPE(sSaveItemDebugData.sWeaponInfo.eWeaponType, weaponData)
			INT iComponent
			
			IF iDLCIndex = -1
				eWeaponComp = GET_PLAYER_PED_WEAPON_COMP_FROM_INT(sSaveItemDebugData.sWeaponInfo.eWeaponType, iComponent)
				WHILE eWeaponComp != WEAPONCOMPONENT_INVALID
					IF IS_BIT_SET(sSaveItemDebugData.sWeaponInfo.iModsAsBitfield, iComponent)
						GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), sSaveItemDebugData.sWeaponInfo.eWeaponType, eWeaponComp)
					ELIF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), sSaveItemDebugData.sWeaponInfo.eWeaponType, eWeaponComp)
						REMOVE_WEAPON_COMPONENT_FROM_PED(PLAYER_PED_ID(), sSaveItemDebugData.sWeaponInfo.eWeaponType, eWeaponComp)
					ENDIF
					iComponent++
					eWeaponComp = GET_PLAYER_PED_WEAPON_COMP_FROM_INT(sSaveItemDebugData.sWeaponInfo.eWeaponType, iComponent)
				ENDWHILE
			ELSE
				REPEAT GET_NUM_DLC_WEAPON_COMPONENTS(iDLCIndex) iComponent
					IF GET_DLC_WEAPON_COMPONENT_DATA(iDLCIndex, iComponent, compData)
						IF NOT IGNORE_DLC_WEAPON_COMPONENT(INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
							IF IS_BIT_SET(sSaveItemDebugData.sWeaponInfo.iModsAsBitfield, iComponent)
								GIVE_WEAPON_COMPONENT_TO_PED(PLAYER_PED_ID(), sSaveItemDebugData.sWeaponInfo.eWeaponType, INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
							ELIF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), sSaveItemDebugData.sWeaponInfo.eWeaponType, INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
								REMOVE_WEAPON_COMPONENT_FROM_PED(PLAYER_PED_ID(), sSaveItemDebugData.sWeaponInfo.eWeaponType, INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
							ENDIF
						ENDIF
					ENDIF
				ENDREPEAT
			ENDIF
		ENDIF
		CLEAR_BIT(sSaveItemDebugData.iProcessSetItemBitfield, DBG_SAVED_ITEM_TYPE_WEAPON)
	ENDIF
	
ENDPROC

PROC MAINTAIN_SHOP_CONTROL_WIDGETS()

	SET_CONTENTS_OF_TEXT_WIDGET(tw_debug_current_item_label, g_sShopSettings.tlCurrentItem_label)

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		IF b_debug_apply_postfx
		
			IF NOT IS_STRING_NULL_OR_EMPTY(strLastPostFX)
				IF ANIMPOSTFX_IS_RUNNING(strLastPostFX)
					ANIMPOSTFX_STOP(strLastPostFX)
				ENDIF
			ENDIF
			
			strLastPostFX = GET_CONTENTS_OF_TEXT_WIDGET(w_debug_post_fx)
			IF NOT IS_STRING_NULL_OR_EMPTY(strLastPostFX)
				IF NOT ANIMPOSTFX_IS_RUNNING(strLastPostFX)
					ANIMPOSTFX_PLAY(strLastPostFX, 0, TRUE)	
				ENDIF
			ENDIF
			
			b_debug_apply_postfx = FALSE
		ENDIF
		
		IF b_debug_stop_postfx
			ANIMPOSTFX_STOP_ALL()
			b_debug_stop_postfx = FALSE
		ENDIF
		
		IF b_debug_get_weapons
			GET_PED_WEAPONS_MP(PLAYER_PED_ID(), g_sTempPedWeaponsMPData)
			b_debug_get_weapons = FALSE
		ENDIF
		IF b_debug_set_weapons
			SET_PED_WEAPONS_MP(PLAYER_PED_ID(), g_sTempPedWeaponsMPData)
			b_debug_set_weapons = FALSE
		ENDIF
		IF b_debug_remove_weapons
			REMOVE_ALL_PED_WEAPONS(PLAYER_PED_ID(), FALSE)
			b_debug_remove_weapons = FALSE
		ENDIF
		IF b_debug_output_weapon_data
			scrShopWeaponData weaponData
			scrShopWeaponComponentData compData
			WEAPONCOMPONENT_TYPE eWeaponComp
			WEAPON_TYPE eCurrentWeapon
			GET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), eCurrentWeapon, FALSE)
			INT iDLCIndex = GET_DLC_WEAPON_DATA_FOR_WEAPON_TYPE(eCurrentWeapon, weaponData)
			INT iComponent, iAttachedComponents
			TEXT_LABEL_15 tlItemLabel
			
			OPEN_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("------------------")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				tlItemLabel = GET_WEAPON_NAME(eCurrentWeapon)
				SAVE_STRING_TO_DEBUG_FILE("Weapon Label: ")
				SAVE_STRING_TO_DEBUG_FILE(tlItemLabel)
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("Weapon Name: ")
				SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(tlItemLabel))
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("Weapon Enum: ")
				SAVE_STRING_TO_DEBUG_FILE(GET_WEAPON_TYPE_NAME_FOR_DEBUG(eCurrentWeapon))
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("Weapon Enum Hash: ")
				SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(eCurrentWeapon))
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
				// COMPONENTS
				iComponent = 0
				iAttachedComponents = 0
				IF iDLCIndex = -1
					eWeaponComp = GET_PLAYER_PED_WEAPON_COMP_FROM_INT(eCurrentWeapon, iComponent)
					WHILE eWeaponComp != WEAPONCOMPONENT_INVALID
						IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), eCurrentWeapon, eWeaponComp)
							tlItemLabel = GET_WEAPON_COMPONENT_NAME(eWeaponComp, eCurrentWeapon)
							SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("Component[")
							SAVE_INT_TO_DEBUG_FILE(iAttachedComponents)
							SAVE_STRING_TO_DEBUG_FILE("] Label: ")
							SAVE_STRING_TO_DEBUG_FILE(tlItemLabel)
							SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("Component[")
							SAVE_INT_TO_DEBUG_FILE(iAttachedComponents)
							SAVE_STRING_TO_DEBUG_FILE("] Name: ")
							SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(tlItemLabel))
							SAVE_NEWLINE_TO_DEBUG_FILE()
							SAVE_STRING_TO_DEBUG_FILE("Component[")
							SAVE_INT_TO_DEBUG_FILE(iAttachedComponents)
							SAVE_STRING_TO_DEBUG_FILE("] Enum Hash: ")
							SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(eWeaponComp))
							SAVE_NEWLINE_TO_DEBUG_FILE()
							iAttachedComponents++
						ENDIF
						iComponent++
						eWeaponComp = GET_PLAYER_PED_WEAPON_COMP_FROM_INT(eCurrentWeapon, iComponent)
					ENDWHILE
				ELSE
					REPEAT GET_NUM_DLC_WEAPON_COMPONENTS(iDLCIndex) iComponent
						IF GET_DLC_WEAPON_COMPONENT_DATA(iDLCIndex, iComponent, compData)
							IF NOT IGNORE_DLC_WEAPON_COMPONENT(INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
								IF HAS_PED_GOT_WEAPON_COMPONENT(PLAYER_PED_ID(), eCurrentWeapon, INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName))
									tlItemLabel = GET_WEAPON_COMPONENT_NAME(INT_TO_ENUM(WEAPONCOMPONENT_TYPE, compData.m_componentName), eCurrentWeapon)
									SAVE_NEWLINE_TO_DEBUG_FILE()
									SAVE_STRING_TO_DEBUG_FILE("Component[")
									SAVE_INT_TO_DEBUG_FILE(iAttachedComponents)
									SAVE_STRING_TO_DEBUG_FILE("] Label: ")
									SAVE_STRING_TO_DEBUG_FILE(tlItemLabel)
									SAVE_NEWLINE_TO_DEBUG_FILE()
									SAVE_STRING_TO_DEBUG_FILE("Component[")
									SAVE_INT_TO_DEBUG_FILE(iAttachedComponents)
									SAVE_STRING_TO_DEBUG_FILE("] Name: ")
									SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(tlItemLabel))
									SAVE_NEWLINE_TO_DEBUG_FILE()
									SAVE_STRING_TO_DEBUG_FILE("Component[")
									SAVE_INT_TO_DEBUG_FILE(iAttachedComponents)
									SAVE_STRING_TO_DEBUG_FILE("] Enum Hash: ")
									SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(compData.m_componentName))
									SAVE_NEWLINE_TO_DEBUG_FILE()
									iAttachedComponents++
								ENDIF
							ENDIF
						ENDIF
					ENDREPEAT
				ENDIF
				
				// TINTS
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("Tint: ")SAVE_INT_TO_DEBUG_FILE(GET_PED_WEAPON_TINT_INDEX(PLAYER_PED_ID(), eCurrentWeapon))SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("Camo: ")SAVE_INT_TO_DEBUG_FILE(GET_PED_WEAPON_CAMO_INDEX(PLAYER_PED_ID(), eCurrentWeapon))SAVE_NEWLINE_TO_DEBUG_FILE()
				
				SAVE_STRING_TO_DEBUG_FILE("------------------")
				SAVE_NEWLINE_TO_DEBUG_FILE()
				
			CLOSE_DEBUG_FILE()
			
			b_debug_output_weapon_data = FALSE
		ENDIF
		
		PROCESS_SAVE_ITEM_DEBUG()
		
		IF b_debug_save_AllClothesAcquired
			CONST_INT iRepeatsPerFrame		500
			CONST_INT iInstructionsPerFrame	10000000
			 
			MODEL_NAMES ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
			INT iCurrentPed
		  
		  	SWITCH ePedModel
		        CASE MP_M_FREEMODE_01
		            iCurrentPed = 3	//Male
		        BREAK
		        CASE MP_F_FREEMODE_01
		            iCurrentPed = 4	//Female
		        BREAK
				
				DEFAULT
					b_debug_save_AllClothesAcquired = FALSE
					EXIT
				BREAK
		 	ENDSWITCH
			
			scrShopPedComponent componentItem
			INIT_SHOP_PED_COMPONENT(componentItem)
			
			INT iFrameCount = 0, iProcessCount = GET_NUMBER_OF_INSTRUCTIONS_EXECUTED()
			PED_COMP_TYPE_ENUM eCompType
			REPEAT NUMBER_OF_PED_COMP_TYPES eCompType
				INT iDLCItemCount = SETUP_SHOP_PED_APPAREL_QUERY_TU(iCurrentPed, ENUM_TO_INT(CLO_SHOP_NONE), -1, ENUM_TO_INT(SHOP_PED_COMPONENT), -1, ENUM_TO_INT(eCompType))
				INT iDLCItem = 0
				REPEAT iDLCItemCount iDLCItem
					INT iProcessDifference = GET_NUMBER_OF_INSTRUCTIONS_EXECUTED()-iProcessCount
					GET_SHOP_PED_QUERY_COMPONENT(iDLCItem, componentItem)
					IF NOT IS_CONTENT_ITEM_LOCKED(componentItem.m_lockHash)
						PED_COMP_NAME_ENUM ePedComp = INT_TO_ENUM(PED_COMP_NAME_ENUM, (ENUM_TO_INT(GET_DEFAULT_DLC_PED_COMPONENT_FOR_PED_COMPONENT(ePedModel, GET_PED_COMPONENT_FROM_TYPE(eCompType)))+iDLCItem))
						IF NOT IS_PED_COMP_ITEM_ACQUIRED_MP(ePedModel, eCompType, ePedComp)
							CDEBUG1LN(DEBUG_SHOPS, "<SAVE_ACQUIRED> Clothes ", GET_PED_COMP_TYPE_STRING(eCompType), " ", ePedComp, " \"", componentItem.m_textLabel, "\" now aquired (iFrameCount:", iFrameCount, ", iProcessDifference:", iProcessDifference, ")")
							SET_PED_COMP_ITEM_ACQUIRED_MP(ePedModel, eCompType, ePedComp, TRUE, FALSE)
						ENDIF
					ENDIF
					iFrameCount++
					IF (iFrameCount > iRepeatsPerFrame)
					OR (iProcessDifference > iInstructionsPerFrame)
						WAIT(0)
						iFrameCount = 0
						iProcessCount = GET_NUMBER_OF_INSTRUCTIONS_EXECUTED()
					ENDIF
				ENDREPEAT
			ENDREPEAT
			
			scrShopPedProp propItem
			INIT_SHOP_PED_PROP(propItem)
			
			INT iDLCItem
		  	INT iDLCItemCount = SETUP_SHOP_PED_APPAREL_QUERY_TU(iCurrentPed, ENUM_TO_INT(CLO_SHOP_NONE), -1, ENUM_TO_INT(SHOP_PED_PROP), -1, -1)
			
			REPEAT iDLCItemCount iDLCItem
				INT iProcessDifference = GET_NUMBER_OF_INSTRUCTIONS_EXECUTED()-iProcessCount
				GET_SHOP_PED_QUERY_PROP(iDLCItem, propItem)
				IF NOT IS_CONTENT_ITEM_LOCKED(propItem.m_lockHash)
					IF DOES_TEXT_LABEL_EXIST(propItem.m_textLabel)
						IF NOT IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(propItem.m_nameHash, PED_COMPONENT_ACQUIRED_SLOT)
							CDEBUG1LN(DEBUG_SHOPS, "<SAVE_ACQUIRED> Props ", GET_PROP_POSITION_STRING(INT_TO_ENUM(PED_PROP_POSITION, propItem.m_eAnchorPoint)), " ", iDLCItem, " \"", propItem.m_textLabel, "\" now aquired (iFrameCount:", iFrameCount, ", iProcessDifference:", iProcessDifference, ")")
							SET_BIT_SHOP_PED_APPAREL_SCRIPT(propItem.m_nameHash, PED_COMPONENT_ACQUIRED_SLOT)
						ENDIF
					ENDIF
				ENDIF
				iFrameCount++
				IF (iFrameCount > iRepeatsPerFrame)
				OR (iProcessDifference > iInstructionsPerFrame)
					WAIT(0)
					iFrameCount = 0
					iProcessCount = GET_NUMBER_OF_INSTRUCTIONS_EXECUTED()
				ENDIF
			ENDREPEAT
			
			b_debug_save_AllClothesAcquired = FALSE
		ENDIF
		IF b_debug_save_AllTattoosAcquired
			TATTOO_DATA_STRUCT sTattooData
			TATTOO_FACTION_ENUM eFaction = GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID())
			
			INT i
			REPEAT MAX_NUMBER_OF_TATTOOS i
				IF GET_TATTOO_DATA(sTattooData, INT_TO_ENUM(TATTOO_NAME_ENUM, i), eFaction, PLAYER_PED_ID())
					IF NOT IS_MP_TATTOO_PURCHASED(sTattooData.eEnum)
						CDEBUG1LN(DEBUG_SHOPS, "<SAVE_ACQUIRED> Tattoo ", sTattooData.eEnum, " \"", sTattooData.sLabel, "\" now aquired")
						SET_MP_TATTOO_PURCHASED(sTattooData.eEnum, TRUE)
					ENDIF
				ENDIF
			ENDREPEAT
			
			//info for the exsiting tattoo
			sTattooShopItemValues 	sDLCTattooData		
			TATTOO_NAME_ENUM 		eDLCTattoo
					
			INT iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)		
			INT iDLCIndex
			REPEAT iDLCCount iDLCIndex
				IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
					IF NOT IS_CONTENT_ITEM_LOCKED(sDLCTattooData.m_lockHash)
						eDLCTattoo = INT_TO_ENUM(TATTOO_NAME_ENUM, ENUM_TO_INT(TATTOO_MP_FM_DLC)+iDLCIndex)
						IF NOT IS_MP_TATTOO_CURRENT(eDLCTattoo)
							CDEBUG1LN(DEBUG_SHOPS, "<SAVE_ACQUIRED> DLC Tattoo ", eDLCTattoo, " \"", sDLCTattooData.Label, "\" now aquired")
							SET_MP_TATTOO_PURCHASED(eDLCTattoo, TRUE)
						ENDIF
					ENDIF
				ENDIF
			ENDREPEAT
			
			b_debug_save_AllTattoosAcquired = FALSE
		ENDIF
		IF b_debug_save_AllWeaponsAcquired
			
			INT i = 0
		    WEAPON_TYPE aWeapon = GET_WEAPONTYPE_FROM_ITERATED_INDEX(i)
		    WHILE aWeapon != WEAPONTYPE_INVALID
				IF NOT IS_MP_WEAPON_PURCHASED(aWeapon)
					CDEBUG1LN(DEBUG_SHOPS, "<SAVE_ACQUIRED> Weapon ", aWeapon, " \"", GET_WEAPON_NAME(aWeapon), "\" now aquired")
					SET_BITSET_WEAPON_PURCHASED(aWeapon, TRUE)
				ENDIF
				
				i++
				aWeapon = GET_WEAPONTYPE_FROM_ITERATED_INDEX(i)
			ENDWHILE
			
			b_debug_save_AllWeaponsAcquired = FALSE
		ENDIF
		
		IF b_debug_AllSnacksAcquired
		//	SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_YUM_SNACKS, INV_SNACK_1_MAX)
		//	SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_HEALTH_SNACKS, INV_SNACK_2_MAX)
		//	SET_MP_INT_CHARACTER_STAT(MP_STAT_NO_BOUGHT_EPIC_SNACKS, INV_SNACK_3_MAX)
		//	SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_ORANGE_BOUGHT, INV_DRINK_1_MAX)
		//	SET_MP_INT_CHARACTER_STAT(MP_STAT_NUMBER_OF_BOURGE_BOUGHT, INV_DRINK_2_MAX)
		//	SET_MP_INT_CHARACTER_STAT(MP_STAT_CIGARETTES_BOUGHT, INV_SMOKES_MAX)
			
			SNACK_CATEGORIES eSnack
			REPEAT SNACK_TOTAL eSnack
				SET_MP_INT_CHARACTER_STAT(GET_SNACK_MP_STAT(ENUM_TO_INT(eSnack)), GET_SNACK_MAX(ENUM_TO_INT(eSnack)))
			ENDREPEAT 
			
			b_debug_AllSnacksAcquired = FALSE
		ENDIF
		
		IF b_debug_equip_stored_parachute
			EQUIP_STORED_MP_PARACHUTE(PLAYER_ID(), i_debug_stored_parachute_tint, i_debug_stored_parachute_bag_index)
		ELSE
			i_debug_stored_parachute_tint = -1
			i_debug_stored_parachute_bag_index = -1
		ENDIF
		
		IF b_debug_test_visual_aid_override
			IF ENUM_TO_INT(g_sFMMCVisualAid.eOverride) != i_debug_visual_aid_override
				g_sFMMCVisualAid.eOverride = INT_TO_ENUM(VISUAL_AID_MODES, i_debug_visual_aid_override)
			ENDIF
			IF ENUM_TO_INT(g_sFMMCVisualAid.eUseSound) != i_debug_VISUAL_AID_SOUND
				g_sFMMCVisualAid.eUseSound = INT_TO_ENUM(VISUAL_AID_SOUND, i_debug_VISUAL_AID_SOUND)
			ENDIF
		ELSE
			IF ENUM_TO_INT(g_sFMMCVisualAid.eOverride) != i_debug_visual_aid_override
				i_debug_visual_aid_override = ENUM_TO_INT(g_sFMMCVisualAid.eOverride)
			ENDIF
			IF ENUM_TO_INT(g_sFMMCVisualAid.eUseSound) != i_debug_VISUAL_AID_SOUND
				i_debug_VISUAL_AID_SOUND = ENUM_TO_INT(GET_PLAYER_VISUAL_AID_SOUND())
			ENDIF
		ENDIF
		
		SET_CONTENTS_OF_TEXT_WIDGET(tw_debug_visual_aid_override_script, g_sFMMCVisualAid.tl31OverrideScriptName)
		
		CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_END
		  IF b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_MAZE_BANK_WEST_EXECUTIVE_OFFICE]
			eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_MAZE_BANK_WEST_EXECUTIVE_OFFICE
		ELIF b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_PALETO_FOREST_BUNKER]
			eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_PALETO_FOREST_BUNKER
		ELIF b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_GREAT_CHAPARRAL_BIKER_CLUBHOUSE]
			eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_GREAT_CHAPARRAL_BIKER_CLUBHOUSE
	//	ELIF b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_SENORA_DESERT_CONTERFEIT_CASH_FACTORY]
	//		eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_SENORA_DESERT_CONTERFEIT_CASH_FACTORY
		ELIF b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1561_SAN_VITAS_STREET_APARTMENT]
			eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1561_SAN_VITAS_STREET_APARTMENT
		ELIF b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1337_EXCEPTIONALISTS_WAY_10_CAR_GARAGE]
			eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_PROPERTY_1337_EXCEPTIONALISTS_WAY_10_CAR_GARAGE
		
		ELIF b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_BF_DUNE_FAV]
			eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_BF_DUNE_FAV
		ELIF b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_PEGASSI_VORTEX]
			eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_PEGASSI_VORTEX
		ELIF b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_OBEY_OMNIS]
			eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_OBEY_OMNIS
		ELIF b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_MAIBATSU_FROGGER]
			eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_MAIBATSU_FROGGER
		ELIF b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_WESTERN_ZOMBIE_CHOPPER]
			eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_WESTERN_ZOMBIE_CHOPPER
		ELIF b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_GROTTI_TURISMO_R]
			eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_GROTTI_TURISMO_R
		ELIF b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_ENUS_WINDSOR]
			eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_ENUS_WINDSOR
		ELIF b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_BRAVADO_BANSHEE]
			eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_BRAVADO_BANSHEE
		ELIF b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_INVETERO_COQUETTE_CLASSIC]
			eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_INVETERO_COQUETTE_CLASSIC
		ELIF b_debug_start_browser_for_CESP_content[CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_ENUS_HUNTLEY_S]
			eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_VEHICLE_ENUS_HUNTLEY_S
		ENDIF
		IF (eDebug_start_browser_for_CESP_content != CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_END)
			START_BROWSER_FOR_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(eDebug_start_browser_for_CESP_content)
			b_debug_start_browser_for_CESP_content[eDebug_start_browser_for_CESP_content] = FALSE
			eDebug_start_browser_for_CESP_content = CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_END
		ENDIF
		
		IF b_debug_clear_player_purchased_CESP_content
			CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT eContent
			REPEAT CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT_END eContent
				IF HAS_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(eContent)
					SET_PLAYER_PURCHASED_CRIMINAL_ENTERPRISE_STARTER_PACK_CONTENT(eContent, FALSE)
				ENDIF
			ENDREPEAT
			
			b_debug_clear_player_purchased_CESP_content = FALSE
		ENDIF
		
		IF b_debug_stats_output_1
			INT i
			STATS_PACKED eDisplaySlotStat
			
			OPEN_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			REPEAT MAX_MP_SAVED_VEHICLES i
				eDisplaySlotStat = MPSV_GET_DISPLAY_SLOT_STAT_OFFSET(i)
				SAVE_STRING_TO_DEBUG_FILE("[1] Display Slot ")SAVE_INT_TO_DEBUG_FILE(i)
				SAVE_STRING_TO_DEBUG_FILE("	")SAVE_INT_TO_DEBUG_FILE(GET_INT_PACKED_STAT_BITSHIFT(eDisplaySlotStat))
				SAVE_STRING_TO_DEBUG_FILE("	")SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(GET_INT_PACKED_STAT_KEY(eDisplaySlotStat, 0)))
				SAVE_STRING_TO_DEBUG_FILE("	")SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(GET_INT_PACKED_STAT_KEY(eDisplaySlotStat, 1)))
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDREPEAT
			
			REPEAT MAX_MP_SAVED_VEHICLES i
				eDisplaySlotStat = MPSV_GET_DISPLAY_SLOT_STAT_OFFSET_PART_2(i)
				SAVE_STRING_TO_DEBUG_FILE("[2] Display Slot ")SAVE_INT_TO_DEBUG_FILE(i)
				SAVE_STRING_TO_DEBUG_FILE("	")SAVE_INT_TO_DEBUG_FILE(GET_INT_PACKED_STAT_BITSHIFT(eDisplaySlotStat))
				SAVE_STRING_TO_DEBUG_FILE("	")SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(GET_INT_PACKED_STAT_KEY(eDisplaySlotStat, 0)))
				SAVE_STRING_TO_DEBUG_FILE("	")SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(GET_INT_PACKED_STAT_KEY(eDisplaySlotStat, 1)))
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDREPEAT
			
			CLOSE_DEBUG_FILE()
			
			b_debug_stats_output_1 = FALSE
		ENDIF
		IF b_debug_stats_output_2
			INT i
			MP_INT_STATS eModelStat
			
			OPEN_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			REPEAT MAX_MP_SAVED_VEHICLES i
				eModelStat = GET_INT_STAT_FOR_VEHICLE_SLOT(MPSV_STAT_ELEMENT_MODEL, i)
				SAVE_STRING_TO_DEBUG_FILE("Vehicle Slot  ")SAVE_INT_TO_DEBUG_FILE(i)
				SAVE_STRING_TO_DEBUG_FILE("	MP{x}_MPSV_MODEL_")SAVE_INT_TO_DEBUG_FILE(i)
				SAVE_STRING_TO_DEBUG_FILE("	")SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(GET_STAT_FROM_MP_INT_CHARACTER_STAT(eModelStat, 0)))
				SAVE_STRING_TO_DEBUG_FILE("	")SAVE_INT_TO_DEBUG_FILE(ENUM_TO_INT(GET_STAT_FROM_MP_INT_CHARACTER_STAT(eModelStat, 1)))
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDREPEAT
			
			CLOSE_DEBUG_FILE()
			
			b_debug_stats_output_2 = FALSE
		ENDIF
		
		IF b_debug_output_exclusive_vehicle_items
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				VEHICLE_INDEX vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())
				IF DOES_ENTITY_EXIST(vehID)
				AND IS_VEHICLE_DRIVEABLE(vehID)
				
					SET_VEHICLE_MOD_KIT(vehID, 0)
					
					INT iModSlot
					INT iModIndex
					INT iModCount
					BOOL bLockedStatus
					MOD_TYPE eMod
					
					OPEN_DEBUG_FILE()
					SAVE_NEWLINE_TO_DEBUG_FILE()
					SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(vehID)))
					SAVE_NEWLINE_TO_DEBUG_FILE()
					
					FOR iModSlot = ENUM_TO_INT(MOD_SPOILER) TO ENUM_TO_INT(MOD_LIVERY)
					
						eMod = INT_TO_ENUM(MOD_TYPE, iModSlot)
					
						IF eMod = MOD_TOGGLE_NITROUS
						OR eMod = MOD_TOGGLE_TURBO
						OR eMod = MOD_TOGGLE_SUBWOOFER
						OR eMod = MOD_TOGGLE_TYRE_SMOKE
						OR eMod = MOD_TOGGLE_HYDRAULICS
						OR eMod = MOD_TOGGLE_XENON_LIGHTS
							// Skip
						ELSE
							iModCount = GET_NUM_VEHICLE_MODS(vehID, eMod)
							
							REPEAT iModCount iModIndex
								bLockedStatus = IS_VEHICLE_MOD_GEN9_EXCLUSIVE(vehID, eMod, iModIndex)
								IF bLockedStatus
									SAVE_STRING_TO_DEBUG_FILE("	modSlot = ")SAVE_STRING_TO_DEBUG_FILE(debug_GET_MOD_TYPE_NAME(eMod))
									SAVE_STRING_TO_DEBUG_FILE("	modIndex = ")SAVE_INT_TO_DEBUG_FILE(iModIndex)
									SAVE_STRING_TO_DEBUG_FILE("	modLabel = ")SAVE_STRING_TO_DEBUG_FILE(GET_MOD_TEXT_LABEL(vehID, eMod, iModIndex))
									SAVE_STRING_TO_DEBUG_FILE("	codeLock = ")SAVE_BOOL_TO_DEBUG_FILE(bLockedStatus)
									SAVE_NEWLINE_TO_DEBUG_FILE()
								ENDIF
							ENDREPEAT
						ENDIF
					ENDFOR
					
					CLOSE_DEBUG_FILE()
					
				ENDIF
			ENDIF
			b_debug_output_exclusive_vehicle_items = FALSE
		ENDIF
		
		IF b_debug_output_veh_data
		OR b_debug_output_veh_data_move
		OR b_debug_output_veh_data_on_focused_veh
			OPEN_DEBUG_FILE()
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			VEHICLE_INDEX tempVeh = GET_PLAYERS_LAST_USED_VEHICLE()
			
			IF b_debug_output_veh_data_on_focused_veh
				IF IS_ENTITY_A_VEHICLE(GET_FOCUS_ENTITY_INDEX())
					tempVeh = GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_FOCUS_ENTITY_INDEX())
				ENDIF	
			ENDIF
			
			IF DOES_ENTITY_EXIST(tempVeh)
			AND IS_VEHICLE_DRIVEABLE(tempVeh)
			
				IF b_debug_output_veh_data_move
					IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), tempVeh)
						IF NETWORK_IS_GAME_IN_PROGRESS()
							TASK_ENTER_VEHICLE(PLAYER_PED_ID(), tempVeh, -1, VS_DRIVER, PEDMOVE_SPRINT, ECF_WARP_PED)
						ELSE
							SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), tempVeh)
						ENDIF
						
						WAIT(0)
					ENDIF
					
					IF NEW_LOAD_SCENE_START_SPHERE(<<1463.2089, 3186.8362, 39.4141>>, 200)
						WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
							WAIT(0)
						ENDWHILE
						NEW_LOAD_SCENE_STOP()
					ENDIF
					
					DOES_ENTITY_EXIST(tempVeh)
					IS_VEHICLE_DRIVEABLE(tempVeh)
					IS_PED_INJURED(PLAYER_PED_ID())
					
					SET_ENTITY_COORDS(tempVeh, <<1463.2089, 3186.8362, 39.4141>>)
					SET_ENTITY_HEADING(tempVeh, 194.9961)
					SET_VEHICLE_ON_GROUND_PROPERLY(tempVeh)
					SET_VEHICLE_FIXED(tempVeh)
					SET_ENTITY_HEALTH(tempVeh, 1000)
					SET_VEHICLE_ENGINE_HEALTH(tempVeh, 1000)
					SET_VEHICLE_PETROL_TANK_HEALTH(tempVeh, 1000)
					SET_VEHICLE_DIRT_LEVEL(tempVeh,0)
					REMOVE_DECALS_FROM_VEHICLE(tempVeh)
					FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(tempVeh)
					
					SET_GAMEPLAY_CAM_RELATIVE_HEADING(-143.3366)
					SET_GAMEPLAY_CAM_RELATIVE_PITCH(-1.7613)
					
					IF NETWORK_IS_GAME_IN_PROGRESS()
						NETWORK_OVERRIDE_CLOCK_TIME(12, 0, 0)
					ELSE
						SET_CLOCK_TIME(12, 0, 0)
					ENDIF
					
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					
					HIDE_HUD_AND_RADAR_THIS_FRAME()
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WANTED_STARS)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_HELP_TEXT)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_1)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FLOATING_HELP_TEXT_2)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RETICLE)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SUBTITLE_TEXT)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_RADIO_STATIONS)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_SAVING_GAME)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_FEED)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL)
					HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_WHEEL_STATS)
					
					SAVE_SCREENSHOT("")
					
					WAIT(0)
				ENDIF
				
				VEHICLE_SETUP_STRUCT_MP sVehData
				GET_VEHICLE_SETUP(tempVeh, sVehData.VehicleSetup)
				
				SAVE_STRING_TO_DEBUG_FILE("CASE IE_VEH_")SAVE_STRING_TO_DEBUG_FILE(GET_CONTENTS_OF_TEXT_WIDGET(w_debug_veh_name))SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.eModel = ")SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(tempVeh)))SAVE_STRING_TO_DEBUG_FILE(" // ")SAVE_STRING_TO_DEBUG_FILE(GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_ENTITY_MODEL(tempVeh)))SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.tlPlateText = \"")SAVE_STRING_TO_DEBUG_FILE(sVehData.VehicleSetup.tlPlateText)SAVE_STRING_TO_DEBUG_FILE("\"")SAVE_NEWLINE_TO_DEBUG_FILE()
				IF sVehData.VehicleSetup.iPlateIndex != 0
					SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.iPlateIndex = ")SAVE_INT_TO_DEBUG_FILE(sVehData.VehicleSetup.iPlateIndex)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF sVehData.VehicleSetup.iColour1 != -1
					SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.iColour1 = ")SAVE_INT_TO_DEBUG_FILE(sVehData.VehicleSetup.iColour1)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF sVehData.VehicleSetup.iColour2 != -1
					SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.iColour2 = ")SAVE_INT_TO_DEBUG_FILE(sVehData.VehicleSetup.iColour2)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF sVehData.VehicleSetup.iColourExtra1 != -1
					SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.iColourExtra1 = ")SAVE_INT_TO_DEBUG_FILE(sVehData.VehicleSetup.iColourExtra1)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF sVehData.VehicleSetup.iColourExtra2 != -1
					SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.iColourExtra2 = ")SAVE_INT_TO_DEBUG_FILE(sVehData.VehicleSetup.iColourExtra2)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				IF sVehData.iColour5 != -1
					SAVE_STRING_TO_DEBUG_FILE("	sData.iColour5 = ")SAVE_INT_TO_DEBUG_FILE(sVehData.iColour5)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF sVehData.iColour6 != -1
					SAVE_STRING_TO_DEBUG_FILE("	sData.iColour6 = ")SAVE_INT_TO_DEBUG_FILE(sVehData.iColour6)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				IF sVehData.iLivery2 != -1
					SAVE_STRING_TO_DEBUG_FILE("	sData.iLivery2 = ")SAVE_INT_TO_DEBUG_FILE(sVehData.iLivery2)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF sVehData.VehicleSetup.iLivery != -1
					SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.iLivery = ")SAVE_INT_TO_DEBUG_FILE(sVehData.VehicleSetup.iLivery)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF sVehData.VehicleSetup.iWindowTintColour != 0
					SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.iWindowTintColour = ")SAVE_INT_TO_DEBUG_FILE(sVehData.VehicleSetup.iWindowTintColour)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF sVehData.VehicleSetup.iWheelType != 0
					SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.iWheelType = ")SAVE_INT_TO_DEBUG_FILE(sVehData.VehicleSetup.iWheelType)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				IF sVehData.VehicleSetup.iTyreR != 0
					SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.iTyreR = ")SAVE_INT_TO_DEBUG_FILE(sVehData.VehicleSetup.iTyreR)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF sVehData.VehicleSetup.iTyreG != 0
					SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.iTyreG = ")SAVE_INT_TO_DEBUG_FILE(sVehData.VehicleSetup.iTyreG)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF sVehData.VehicleSetup.iTyreB != 0
					SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.iTyreB = ")SAVE_INT_TO_DEBUG_FILE(sVehData.VehicleSetup.iTyreB)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				IF sVehData.VehicleSetup.iNeonR != 0
					SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.iNeonR = ")SAVE_INT_TO_DEBUG_FILE(sVehData.VehicleSetup.iNeonR)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF sVehData.VehicleSetup.iNeonG != 0
					SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.iNeonG = ")SAVE_INT_TO_DEBUG_FILE(sVehData.VehicleSetup.iNeonG)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF sVehData.VehicleSetup.iNeonB != 0
					SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.iNeonB = ")SAVE_INT_TO_DEBUG_FILE(sVehData.VehicleSetup.iNeonB)SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				IF IS_BIT_SET(sVehData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)
					SAVE_STRING_TO_DEBUG_FILE("	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_FRONT)")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF IS_BIT_SET(sVehData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)
					SAVE_STRING_TO_DEBUG_FILE("	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_BACK)")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF IS_BIT_SET(sVehData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)
					SAVE_STRING_TO_DEBUG_FILE("	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_LEFT)")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF IS_BIT_SET(sVehData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)
					SAVE_STRING_TO_DEBUG_FILE("	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_NEON_RIGHT)")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				INT i
				FOR i = 0 TO 8
					IF IS_BIT_SET(sVehData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_0+i)
						SAVE_STRING_TO_DEBUG_FILE("	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_")SAVE_INT_TO_DEBUG_FILE(i)SAVE_STRING_TO_DEBUG_FILE(")")SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				ENDFOR
				
				IF IS_BIT_SET(sVehData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)
					SAVE_STRING_TO_DEBUG_FILE("	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_9)")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF IS_BIT_SET(sVehData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_10)
					SAVE_STRING_TO_DEBUG_FILE("	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_10)")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				IF IS_BIT_SET(sVehData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_11)
					SAVE_STRING_TO_DEBUG_FILE("	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_EXTRA_11)")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				IF IS_BIT_SET(sVehData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_USE_IND_SMOKE)
					SAVE_STRING_TO_DEBUG_FILE("	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_USE_IND_SMOKE)")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF
				
				IF IS_BIT_SET(sVehData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)
					SAVE_STRING_TO_DEBUG_FILE("	SET_BIT(sData.VehicleSetup.iFlags, VEHICLE_SETUP_FLAG_BULLETPROOF_TYRES)")SAVE_NEWLINE_TO_DEBUG_FILE()
				ENDIF

				FOR i = 0 TO MAX_VEHICLE_MOD_SLOTS-1
					IF sVehData.VehicleSetup.iModIndex[i] != 0
						SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.iModIndex[")SAVE_STRING_TO_DEBUG_FILE(debug_GET_MOD_TYPE_NAME(INT_TO_ENUM(MOD_TYPE, i)))SAVE_STRING_TO_DEBUG_FILE("] = ")SAVE_INT_TO_DEBUG_FILE(sVehData.VehicleSetup.iModIndex[i])SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				ENDFOR
				
				FOR i = 0 TO MAX_VEHICLE_MOD_VAR_SLOTS-1
					IF sVehData.VehicleSetup.iModVariation[i] != 0
						SAVE_STRING_TO_DEBUG_FILE("	sData.VehicleSetup.iModVariation[")SAVE_INT_TO_DEBUG_FILE(i)SAVE_STRING_TO_DEBUG_FILE("] = ")SAVE_INT_TO_DEBUG_FILE(sVehData.VehicleSetup.iModVariation[i])SAVE_NEWLINE_TO_DEBUG_FILE()
					ENDIF
				ENDFOR
				
				SAVE_STRING_TO_DEBUG_FILE("BREAK")SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
			
			CLOSE_DEBUG_FILE()
			
		ENDIF
		b_debug_output_veh_data = FALSE
		b_debug_output_veh_data_move = FALSE
		b_debug_output_veh_data_on_focused_veh = FALSE
		
		IF b_debug_output_CURRENT_veh_mass_and_armour
			SAVE_STRING_TO_DEBUG_FILE("Output CURRENT veh mass + armour")
			SAVE_NEWLINE_TO_DEBUG_FILE()
			
			OPEN_DEBUG_FILE()
			
			VEHICLE_INDEX tempVeh = GET_PLAYERS_LAST_USED_VEHICLE()
			IF DOES_ENTITY_EXIST(tempVeh)
			AND IS_VEHICLE_DRIVEABLE(tempVeh)
				
				MODEL_NAMES eVehicleModel = GET_ENTITY_MODEL(tempVeh)
				FLOAT fVehicleMass							= GET_VEHICLE_MASS(tempVeh)
				FLOAT fVehicleWeaponDamageMult				= GET_VEHICLE_WEAPON_DAMAGE_MULT(tempVeh)
				FLOAT fVehicleCollisionDamageMult			= GET_VEHICLE_COLLISION_DAMAGE_MULT(tempVeh)
				BOOL bVehicleHasRecducedStickyBombDamage	= GET_VEHICLE_HAS_RECDUCED_STICKY_BOMB_DAMAGE(tempVeh)
				BOOL bVehicleHasCappedExplosionDamage		= GET_VEHICLE_HAS_CAPPED_EXPLOSION_DAMAGE(tempVeh)
				BOOL bVehicleHasBulletProofGlass			= GET_VEHICLE_HAS_BULLET_PROOF_GLASS(tempVeh)
				BOOL bVehicleHasBulletResistantGlass		= GET_VEHICLE_HAS_BULLET_RESISTANT_GLASS(tempVeh)
				
				SAVE_STRING_TO_DEBUG_FILE(" - ")SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(eVehicleModel))
				SAVE_STRING_TO_DEBUG_FILE("	Mass:	")SAVE_FLOAT_TO_DEBUG_FILE(fVehicleMass)
				SAVE_STRING_TO_DEBUG_FILE("	, WeaponDamageMult:	")SAVE_FLOAT_TO_DEBUG_FILE(fVehicleWeaponDamageMult)
				SAVE_STRING_TO_DEBUG_FILE("	, CollisionDamageMult:	")SAVE_FLOAT_TO_DEBUG_FILE(fVehicleCollisionDamageMult)
				SAVE_STRING_TO_DEBUG_FILE("	, HasRecducedStickyBombDamage:	")SAVE_BOOL_TO_DEBUG_FILE(bVehicleHasRecducedStickyBombDamage)
				SAVE_STRING_TO_DEBUG_FILE("	, HasCappedExplosionDamage:	")SAVE_BOOL_TO_DEBUG_FILE(bVehicleHasCappedExplosionDamage)
				SAVE_STRING_TO_DEBUG_FILE("	, HasBulletProofGlass:	")SAVE_BOOL_TO_DEBUG_FILE(bVehicleHasBulletProofGlass)
				SAVE_STRING_TO_DEBUG_FILE("	, HasBulletResistantGlass:	")SAVE_BOOL_TO_DEBUG_FILE(bVehicleHasBulletResistantGlass)
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ELSE
				SAVE_STRING_TO_DEBUG_FILE(" - unknown last used vehicle")
				SAVE_NEWLINE_TO_DEBUG_FILE()
			ENDIF
			
			SAVE_NEWLINE_TO_DEBUG_FILE()
			CLOSE_DEBUG_FILE()
			b_debug_output_CURRENT_veh_mass_and_armour = FALSE
		ENDIF
		IF b_debug_output_BUYABLE_veh_mass_and_armour
			b_debug_add_all_pegasus_to_garage = FALSE
			
			MODEL_NAMES eVehicleModel
			FLOAT fVehicleMass							 
			FLOAT fVehicleWeaponDamageMult				 
			FLOAT fVehicleCollisionDamageMult			 
			BOOL bVehicleHasRecducedStickyBombDamage	 
			BOOL bVehicleHasCappedExplosionDamage		 
			BOOL bVehicleHasBulletProofGlass			 
			BOOL bVehicleHasBulletResistantGlass		 
			
			//
			SITE_BUYABLE_VEHICLE sbv = INT_TO_ENUM(SITE_BUYABLE_VEHICLE, i_debug_pegasus_count)
			
			TEXT_LABEL_63 str = i_debug_pegasus_count
			str += " "
			str += GET_LABEL_BUYABLE_VEHICLE(sbv)
			
			IF sbv != UNSET_BUYABLE_VEHICLE
			AND sbv != TOTAL_BUYABLE_VEHICLES
				
				SET_CONTENTS_OF_TEXT_WIDGET(tw_debug_pegasus_count, str)
				
				SWITCH i_debug_pegasus_stage
					CASE 0
			
						IF i_debug_pegasus_count = 0
							SAVE_STRING_TO_DEBUG_FILE("Output BUYABLE veh mass + armour")
							SAVE_NEWLINE_TO_DEBUG_FILE()
							
							SAVE_STRING_TO_DEBUG_FILE("	Mass:	")
							SAVE_STRING_TO_DEBUG_FILE("	, WeaponDamageMult:	")
							SAVE_STRING_TO_DEBUG_FILE("	, CollisionDamageMult:	")
							SAVE_STRING_TO_DEBUG_FILE("	, HasRecducedStickyBombDamage:	")
							SAVE_STRING_TO_DEBUG_FILE("	, HasCappedExplosionDamage:	")
							SAVE_STRING_TO_DEBUG_FILE("	, HasBulletProofGlass:	")
							SAVE_STRING_TO_DEBUG_FILE("	, HasBulletResistantGlass:	")
						ENDIF
						
						//create veh
						eVehicleModel = GET_MODEL_FOR_BUYABLE_VEHICLE(sbv)
						IF (GET_MODEL_FOR_BUYABLE_VEHICLE(sbv) != DUMMY_MODEL_FOR_SCRIPT)
							IF IS_MODEL_VALID(eVehicleModel)
							AND IS_MODEL_A_VEHICLE(eVehicleModel)
								REQUEST_MODEL(eVehicleModel)
								IF HAS_MODEL_LOADED(eVehicleModel)
									vehGarageVeh = CREATE_VEHICLE(eVehicleModel,
											GET_ENTITY_COORDS(PLAYER_PED_ID())+<<0,0,100>>, 0, false, false, true)
									
									SET_MODEL_AS_NO_LONGER_NEEDED(eVehicleModel)
									i_debug_pegasus_stage++
								ENDIF
							ELSE
								i_debug_pegasus_stage = 2
							ENDIF
						ELSE
							i_debug_pegasus_stage = 2
						ENDIF
					BREAK
					CASE 1
						//
						OPEN_DEBUG_FILE()
						IF DOES_ENTITY_EXIST(vehGarageVeh)
						AND IS_VEHICLE_DRIVEABLE(vehGarageVeh)
							
							eVehicleModel = GET_ENTITY_MODEL(vehGarageVeh)
							fVehicleMass						= GET_VEHICLE_MASS(vehGarageVeh)
							fVehicleWeaponDamageMult			= GET_VEHICLE_WEAPON_DAMAGE_MULT(vehGarageVeh)
							fVehicleCollisionDamageMult			= GET_VEHICLE_COLLISION_DAMAGE_MULT(vehGarageVeh)
							bVehicleHasRecducedStickyBombDamage	= GET_VEHICLE_HAS_RECDUCED_STICKY_BOMB_DAMAGE(vehGarageVeh)
							bVehicleHasCappedExplosionDamage	= GET_VEHICLE_HAS_CAPPED_EXPLOSION_DAMAGE(vehGarageVeh)
							bVehicleHasBulletProofGlass			= GET_VEHICLE_HAS_BULLET_PROOF_GLASS(vehGarageVeh)
							bVehicleHasBulletResistantGlass		= GET_VEHICLE_HAS_BULLET_RESISTANT_GLASS(vehGarageVeh)
							
							SAVE_STRING_TO_DEBUG_FILE(" ")SAVE_INT_TO_DEBUG_FILE(i_debug_pegasus_count)SAVE_STRING_TO_DEBUG_FILE("- ")SAVE_STRING_TO_DEBUG_FILE(GET_MODEL_NAME_FOR_DEBUG(eVehicleModel))
							SAVE_STRING_TO_DEBUG_FILE("	")SAVE_FLOAT_TO_DEBUG_FILE(fVehicleMass)
							SAVE_STRING_TO_DEBUG_FILE("	:	")SAVE_FLOAT_TO_DEBUG_FILE(fVehicleWeaponDamageMult)
							SAVE_STRING_TO_DEBUG_FILE("	:	")SAVE_FLOAT_TO_DEBUG_FILE(fVehicleCollisionDamageMult)
							SAVE_STRING_TO_DEBUG_FILE("	:	")SAVE_BOOL_TO_DEBUG_FILE(bVehicleHasRecducedStickyBombDamage)
							SAVE_STRING_TO_DEBUG_FILE("	:	")SAVE_BOOL_TO_DEBUG_FILE(bVehicleHasCappedExplosionDamage)
							SAVE_STRING_TO_DEBUG_FILE("	:	")SAVE_BOOL_TO_DEBUG_FILE(bVehicleHasBulletProofGlass)
							SAVE_STRING_TO_DEBUG_FILE("	:	")SAVE_BOOL_TO_DEBUG_FILE(bVehicleHasBulletResistantGlass)
							SAVE_NEWLINE_TO_DEBUG_FILE()
							
							i_debug_pegasus_stage++
						ELSE
							SAVE_STRING_TO_DEBUG_FILE(" ")SAVE_INT_TO_DEBUG_FILE(i_debug_pegasus_count)SAVE_STRING_TO_DEBUG_FILE("- unknown buyable vehicle")
							SAVE_NEWLINE_TO_DEBUG_FILE()
							
							i_debug_pegasus_stage++
						ENDIF
						CLOSE_DEBUG_FILE()
					BREAK
					CASE 2
						//incrament count
						i_debug_pegasus_count++
						i_debug_pegasus_stage = 0
						DELETE_VEHICLE(vehGarageVeh)
					BREAK
				ENDSWITCH
				
			ELSE
				SAVE_NEWLINE_TO_DEBUG_FILE()
				b_debug_output_BUYABLE_veh_mass_and_armour = FALSE
				i_debug_pegasus_count = 0
				i_debug_pegasus_stage = 0
				SET_CONTENTS_OF_TEXT_WIDGET(tw_debug_pegasus_count, "complete")
			ENDIF
		ENDIF
	ENDIF
	
	IF b_debug_launch_debug_ped_script
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("debug_ped_data")) = 0
			REQUEST_SCRIPT_WITH_NAME_HASH(HASH("debug_ped_data"))
			IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("debug_ped_data"))
				START_NEW_SCRIPT_WITH_NAME_HASH(HASH("debug_ped_data"), MULTIPLAYER_MISSION_STACK_SIZE)
				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("debug_ped_data"))
				b_debug_launch_debug_ped_script = FALSE
			ENDIF
		ENDIF
	ENDIF
	
	IF b_debug_launch_item_ownership_script
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("item_ownership_output")) = 0
			REQUEST_SCRIPT_WITH_NAME_HASH(HASH("item_ownership_output"))
			IF HAS_SCRIPT_WITH_NAME_HASH_LOADED(HASH("item_ownership_output"))
				START_NEW_SCRIPT_WITH_NAME_HASH(HASH("item_ownership_output"), DEFAULT_STACK_SIZE)
				SET_SCRIPT_WITH_NAME_HASH_AS_NO_LONGER_NEEDED(HASH("item_ownership_output"))
				b_debug_launch_item_ownership_script = FALSE
			ENDIF
		ENDIF
	ENDIF

	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND NETWORK_IS_GAME_IN_PROGRESS()
	
		INT iSaveSlot = 0
	
		IF b_debug_add_mission
			IF GET_GOODS_CATEGORY_FROM_FACTORY_ID(GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), iSaveSlot)) = FACTORY_TYPE_INVALID
				b_debug_add_mission = FALSE
			ELIF PROCESS_TRANSACTION_FOR_CONTRABAND_MISSION(iSaveSlot, HASH("IG_BUY_0_t0_v0"), eTransResult)
				IF eTransResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					SCRIPT_ASSERT("PROCESS_TRANSACTION_FOR_CONTRABAND_MISSION - success!")
				ENDIF
				b_debug_add_mission = FALSE
			ENDIF
		ENDIF
		IF b_debug_add_materials
			IF GET_GOODS_CATEGORY_FROM_FACTORY_ID(GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), iSaveSlot)) = FACTORY_TYPE_INVALID
				b_debug_add_materials = FALSE
			ELIF PROCESS_TRANSACTION_FOR_ADD_MATERIALS(iSaveSlot, i_debug_material_to_add, eTransResult)
				IF eTransResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					SCRIPT_ASSERT("PROCESS_TRANSACTION_FOR_ADD_MATERIALS - success!")
					
					CDEBUG1LN(DEBUG_SAFEHOUSE, "MAINTAIN_BIKER_PRODUCTION_TIMESTAMP - We have some more product so clear the blocker.")
					SET_PACKED_STAT_BOOL(GET_STAT_ENUM_FOR_FACTORY_TIME_REINIT_FOR_REMAINDER(iSaveSlot), FALSE)
					
					UPDATE_FACTORY_MATERIALS_TOTAL_BD(iSaveSlot, GET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_FACTORY_MATERIALS_TOTAL_SLOT(iSaveSlot)), "Shop Controller debug 3")
					
					IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iMaterialsTotal < 0
						UPDATE_FACTORY_MATERIALS_TOTAL_BD(iSaveSlot, 0, "Shop Controller debug 1")
					ENDIF
					
				ENDIF
				b_debug_add_materials = FALSE
			ENDIF
		ENDIF
		IF b_debug_add_product
			IF GET_GOODS_CATEGORY_FROM_FACTORY_ID(GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), iSaveSlot)) = FACTORY_TYPE_INVALID
				b_debug_add_product = FALSE
			ELIF PROCESS_TRANSACTION_FOR_ADD_PRODUCT(iSaveSlot, i_debug_material_to_add, eTransResult)
				IF eTransResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					SCRIPT_ASSERT("PROCESS_TRANSACTION_FOR_ADD_PRODUCT - success!")
					
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iProductTotal = GET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_FACTORY_PRODUCT_TOTAL_SLOT(iSaveSlot))
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iTotalProductValue = GET_FACTORY_TOTAL_PRODUCT_VALUE(PLAYER_ID(), GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), iSaveSlot), GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iProductTotal)					
				ENDIF
				b_debug_add_product = FALSE
			ENDIF
		ENDIF
		IF b_debug_generate_product
			IF GET_GOODS_CATEGORY_FROM_FACTORY_ID(GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), iSaveSlot)) = FACTORY_TYPE_INVALID
				b_debug_generate_product = FALSE
			ELIF PROCESS_TRANSACTION_FOR_GENERATE_PRODUCT(iSaveSlot, i_debug_material_to_add, -i_debug_material_to_add, eTransResult)
				IF eTransResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					SCRIPT_ASSERT("PROCESS_TRANSACTION_FOR_GENERATE_PRODUCT - success!")
					
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iProductTotal = GET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_FACTORY_PRODUCT_TOTAL_SLOT(iSaveSlot))
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iMaterialsTotal = GET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_FACTORY_MATERIALS_TOTAL_SLOT(iSaveSlot))
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iTotalProductValue = GET_FACTORY_TOTAL_PRODUCT_VALUE(PLAYER_ID(), GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), iSaveSlot), GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iProductTotal)	
					
					IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iMaterialsTotal < 0
						//GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iMaterialsTotal = 0
						UPDATE_FACTORY_MATERIALS_TOTAL_BD(iSaveSlot, 0, "Shop Controller debug 2")
					ENDIF
					
					SET_FACTORY_TIME_STAGE(PLAYER_ID(), GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), iSaveSlot), 0)
					INITIALISE_BIKER_PRODUCTION_TIMESTAMP(iSaveSlot, FALSE)
				ENDIF
				b_debug_generate_product = FALSE
			ENDIF
		ENDIF
		IF b_debug_remove_product
			IF GET_GOODS_CATEGORY_FROM_FACTORY_ID(GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), iSaveSlot)) = FACTORY_TYPE_INVALID
				b_debug_remove_product = FALSE
			ELIF PROCESS_TRANSACTION_FOR_REMOVE_PRODUCT(iSaveSlot, i_debug_material_to_add, 50, REMOVE_CONTRA_MISSION_PASSED, eTransResult)
				IF eTransResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					SCRIPT_ASSERT("PROCESS_TRANSACTION_FOR_REMOVE_PRODUCT - success!")
					
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iProductTotal = GET_MP_INT_CHARACTER_STAT(GET_STAT_ENUM_FOR_FACTORY_PRODUCT_TOTAL_SLOT(iSaveSlot))
					GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iTotalProductValue = GET_FACTORY_TOTAL_PRODUCT_VALUE(PLAYER_ID(), GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), iSaveSlot), GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.bdFactoryData[iSaveSlot].iProductTotal)
				ENDIF
				b_debug_remove_product = FALSE
			ENDIF
		ENDIF
		IF b_debug_reset_business
			IF GET_GOODS_CATEGORY_FROM_FACTORY_ID(GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), iSaveSlot)) = FACTORY_TYPE_INVALID
				b_debug_reset_business = FALSE
			ELIF PROCESS_TRANSACTION_FOR_RESET_BUSINESS(iSaveSlot, eTransResult)
				IF eTransResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					SCRIPT_ASSERT("PROCESS_TRANSACTION_FOR_RESET_BUSINESS - success!")
				ENDIF
				b_debug_reset_business = FALSE
			ENDIF
		ENDIF
		IF b_debug_setup_business
			IF GET_GOODS_CATEGORY_FROM_FACTORY_ID(GET_FACTORY_ID_FROM_FACTORY_SLOT(PLAYER_ID(), iSaveSlot)) = FACTORY_TYPE_INVALID
				b_debug_setup_business = FALSE
			ELIF PROCESS_TRANSACTION_FOR_BUSINESS_SETUP(iSaveSlot, eTransResult)
				IF eTransResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					SCRIPT_ASSERT("PROCESS_TRANSACTION_FOR_BUSINESS_SETUP - success!")
				ENDIF
				b_debug_setup_business = FALSE
			ENDIF
		ENDIF
		
		IF b_debug_output_textTag
			// Check that the xml exists and load
			IF NOT LOAD_XML_FILE("X:/gta5/titleupdate/dev_ng/text_tags.xml")
			OR GET_NUMBER_OF_XML_NODES() = 0
				// bad file
				DELETE_XML_FILE()
			ELSE
			
				OPEN_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
				SAVE_NEWLINE_TO_DEBUG_FILE()
			
				INT iNumNodes = GET_NUMBER_OF_XML_NODES()
				TEXT_LABEL_63 txtNodeName
				INT i
				REPEAT iNumNodes i
					txtNodeName = GET_XML_NODE_NAME()
					SWITCH GET_HASH_KEY(txtNodeName)
						CASE HASH("textTag")
							IF DOES_TEXT_LABEL_EXIST(GET_STRING_FROM_XML_NODE())
								SAVE_STRING_TO_DEBUG_FILE("<textTag>")
								SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_TEXT_FILE(GET_STRING_FROM_XML_NODE()))
								SAVE_STRING_TO_DEBUG_FILE("</textTag>")
								SAVE_NEWLINE_TO_DEBUG_FILE()
							ELSE
								SAVE_STRING_TO_DEBUG_FILE("<textTag>")
								SAVE_STRING_TO_DEBUG_FILE(GET_STRING_FROM_XML_NODE())
								SAVE_STRING_TO_DEBUG_FILE("</textTag>")
								SAVE_NEWLINE_TO_DEBUG_FILE()
							ENDIF
							
						BREAK
					ENDSWITCH
					
					GET_NEXT_XML_NODE()
				ENDREPEAT
				
				CLOSE_DEBUG_FILE()
				
				DELETE_XML_FILE()
			ENDIF
			
			b_debug_output_textTag = FALSE
		ENDIF
		
		IF b_debug_add_all_pegasus_to_garage
			b_debug_output_BUYABLE_veh_mass_and_armour = FALSE
			SITE_BUYABLE_VEHICLE sbv = GET_BIGASS_VEHICLE_ENUM_FROM_INDICE(i_debug_pegasus_count)
			
			TEXT_LABEL_63 str = i_debug_pegasus_count
			str += " "
			str += GET_LABEL_BUYABLE_VEHICLE(sbv)
			
			IF sbv != UNSET_BUYABLE_VEHICLE
				
				SET_CONTENTS_OF_TEXT_WIDGET(tw_debug_pegasus_count, str)
				
				SWITCH i_debug_pegasus_stage
					CASE 0
						//create veh
						IF (GET_MODEL_FOR_BUYABLE_VEHICLE(sbv) != DUMMY_MODEL_FOR_SCRIPT)
							
							IF IS_THIS_MODEL_ALLOWED_IN_HANGAR(GET_MODEL_FOR_BUYABLE_VEHICLE(sbv))
								REQUEST_MODEL(GET_MODEL_FOR_BUYABLE_VEHICLE(sbv))
								IF HAS_MODEL_LOADED(GET_MODEL_FOR_BUYABLE_VEHICLE(sbv))
									vehGarageVeh = CREATE_VEHICLE(GET_MODEL_FOR_BUYABLE_VEHICLE(sbv),
											GET_ENTITY_COORDS(PLAYER_PED_ID())+<<0,0,100>>, 0, false, false, true)
									
									INT iExtraCol1, iExtraCol2
									IF IS_SBV_A_VEHICLE_WITH_COLOUR_EXTRAS(sbv, iExtraCol1, iExtraCol2)
										SET_VEHICLE_EXTRA_COLOURS(vehGarageVeh, iExtraCol1, iExtraCol2)
										CPRINTLN(DEBUG_NET_AMBIENT, "=== VEH SPAWN CLIENT === SET_VEHICLE_EXTRA_COLOURS = ",iExtraCol1,", ",iExtraCol2)
									ENDIF
									
									iGarageVehicleResult = GARAGE_VEHICLE_TRANSACTION_STATE_DEFAULT
									i_debug_pegasus_stage++
								ENDIF
							ELSE
								i_debug_pegasus_stage = 2
							ENDIF
						ELSE
							i_debug_pegasus_stage = 2
						ENDIF
					BREAK
					CASE 1
						//purchase veh
										
						IF PROCESS_TRANSACTION_FOR_NEW_GARAGE_VEHICLE(vehGarageVeh, 0, iGarageVehicleResult)
						
							SWITCH iGarageVehicleResult
								CASE GARAGE_VEHICLE_TRANSACTION_STATE_SUCCESS
									SAVE_STRING_TO_DEBUG_FILE("iGarageVehicleResult = success for ")
									SAVE_STRING_TO_DEBUG_FILE(str)
									SAVE_NEWLINE_TO_DEBUG_FILE()
								BREAK
								CASE GARAGE_VEHICLE_TRANSACTION_STATE_FAILED
									CASSERTLN(DEBUG_SHOPS, "iGarageVehicleResult = failed")
								BREAK
							ENDSWITCH
							
							i_debug_pegasus_stage++
						ENDIF
						
					BREAK
					CASE 2
						//incrament count
						i_debug_pegasus_count++
						i_debug_pegasus_stage = 0
						DELETE_VEHICLE(vehGarageVeh)
					BREAK
				ENDSWITCH
				
			ELSE
				b_debug_add_all_pegasus_to_garage = FALSE
				i_debug_pegasus_count = 0
				i_debug_pegasus_stage = 0
				SET_CONTENTS_OF_TEXT_WIDGET(tw_debug_pegasus_count, "complete")
			ENDIF
		ENDIF
		
		IF b_debug_display_owned_pegasus_vehicles
			INT iRed, iGreen, iBlue, iAlpha
			
			SET_TEXT_FONT(FONT_STANDARD)
			SET_TEXT_SCALE(fTextXScale, fTextYScale)
			GET_HUD_COLOUR(HUD_COLOUR_PURE_WHITE, iRed, iGreen, iBlue, iAlpha)
			SET_TEXT_COLOUR(iRed, iGreen, iBlue, iAlpha)
			SET_TEXT_WRAP(0.0, 1.0)
			DISPLAY_TEXT_WITH_LITERAL_STRING(fTextXDisplay, fTextYDisplay,  "STRING", "OWNED PEGASUS")
			
			TEXT_LABEL_63 tlTempText
			INT iPegasusIndex = 0
			
			SITE_BUYABLE_VEHICLE sbv = GET_BIGASS_VEHICLE_ENUM_FROM_INDICE(iPegasusIndex)
			
			WHILE sbv != UNSET_BUYABLE_VEHICLE
				
				tlTempText  = ""
				tlTempText += iPegasusIndex
				tlTempText += ":"
				
				tlTempText += GET_LABEL_BUYABLE_VEHICLE(sbv)
				IF NOT IS_STRING_NULL_OR_EMPTY(GET_VEHICLE_NAME_EXTRA(GET_MODEL_FOR_BUYABLE_VEHICLE(sbv), iPegasusIndex))
					tlTempText += " "
					tlTempText += GET_STRING_FROM_TEXT_FILE(GET_VEHICLE_NAME_EXTRA(GET_MODEL_FOR_BUYABLE_VEHICLE(sbv), iPegasusIndex))
				ENDIF
				
				IF IS_BIG_ASS_VEHICLE_BS_SET(iPegasusIndex)
					tlTempText += "[~g~OWNED~w~]"
				ELSE
					tlTempText += "[~r~UNOWNED~w~]"
				ENDIF
				IF i_debug_selected_owned_pegasus_vehicles = iPegasusIndex
					IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RETURN)
					OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_ACCEPT)
						IF IS_BIG_ASS_VEHICLE_BS_SET(iPegasusIndex)
							CLEAR_BIG_ASS_VEHICLE_BS(iPegasusIndex)
						ELSE
							SET_BIG_ASS_VEHICLE_BS(iPegasusIndex)
						ENDIF
					ENDIF
				ENDIF
				
				INT iPegasusIndexColumn	= (iPegasusIndex / iTextRowMax)
				INT iPegasusIndexRow	= (iPegasusIndex % iTextRowMax)+1
				
				SET_TEXT_FONT(FONT_STANDARD)
				SET_TEXT_SCALE(fTextXScale, fTextYScale)
				
				IF i_debug_selected_owned_pegasus_vehicles = iPegasusIndex
					GET_HUD_COLOUR(HUD_COLOUR_YELLOW, iRed, iGreen, iBlue, iAlpha)
				ELSE
					GET_HUD_COLOUR(HUD_COLOUR_PURE_WHITE, iRed, iGreen, iBlue, iAlpha)
				ENDIF
				SET_TEXT_COLOUR(iRed, iGreen, iBlue, iAlpha)
				SET_TEXT_WRAP(0.0, 1.0)
				DISPLAY_TEXT_WITH_LITERAL_STRING( fTextXDisplay+(iPegasusIndexColumn*fTextXOffset), fTextYDisplay+(iPegasusIndexRow*fTextYOffset), "STRING", tlTempText)
				
				iPegasusIndex++
				sbv = GET_BIGASS_VEHICLE_ENUM_FROM_INDICE(iPegasusIndex)
			ENDWHILE
			
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT)
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT)
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_UP)
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_UP)
				i_debug_selected_owned_pegasus_vehicles--
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_DOWN)
			OR IS_CONTROL_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_DOWN)
				i_debug_selected_owned_pegasus_vehicles++
			ENDIF
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_LEFT)
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_LEFT)
				i_debug_selected_owned_pegasus_vehicles -= iTextRowMax
			ELIF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_RIGHT)
			OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT)
			OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_RIGHT)
				i_debug_selected_owned_pegasus_vehicles += iTextRowMax
			ENDIF
			
			i_debug_selected_owned_pegasus_vehicles = IWRAP(i_debug_selected_owned_pegasus_vehicles, 0, iPegasusIndex)
			
			DRAW_RECT(fRectXCenter, fRectYCenter, fRectWidth, fRectHeight, 000,000,000,180)
		ENDIF
	ENDIF

	IF b_debug_earn
		INT iTransactionSlot
		NETWORK_REQUEST_CASH_TRANSACTION(iTransactionSlot, NET_SHOP_TTYPE_SERVICE, NET_SHOP_ACTION_EARN, CATEGORY_SERVICE_WITH_THRESHOLD, SERVICE_EARN_DEBUG, i_debug_earn_spend, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, CTPF_AUTO_PROCESS_REPLY)
		b_debug_earn = FALSE
	ENDIF
	IF b_debug_spend
		INT iTransactionSlot
		NETWORK_REQUEST_CASH_TRANSACTION(iTransactionSlot, NET_SHOP_TTYPE_SERVICE, NET_SHOP_ACTION_SPEND, CATEGORY_SERVICE_WITH_THRESHOLD, SERVICE_SPEND_TELESCOPE, i_debug_earn_spend, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, CTPF_AUTO_PROCESS_REPLY)
		b_debug_spend = FALSE
	ENDIF
	
	IF b_debug_force_reset_transaction_data
		FORCE_RESET_ALL_TRANSACTION_DATA()
		b_debug_force_reset_transaction_data = FALSE
	ENDIF
			
	IF b_debug_process_character_appearance
		IF PROCESS_CHARACTER_CREATOR_SAVE_NEW_CHARACTER_TO_INVENTORY(iCharAppearanceControl)
			b_debug_process_character_appearance = FALSE
			iCharAppearanceControl = 0
		ENDIF
	ENDIF
	IF b_debug_add_vehicle_to_garage
		
		IF iGarageVehicleResult = GARAGE_VEHICLE_TRANSACTION_STATE_DEFAULT
			vehGarageVeh = GET_PLAYERS_LAST_VEHICLE()
			IF DOES_ENTITY_EXIST(vehGarageVeh)
			AND IS_VEHICLE_DRIVEABLE(vehGarageVeh)
				// good to go
			ELSE
				SCRIPT_ASSERT("GARAGE_VEHICLE_TRANSACTION_STATE_FAILED - no vehicle")
				b_debug_add_vehicle_to_garage = FALSE
				EXIT
			ENDIF
		ENDIF
		
		IF PROCESS_TRANSACTION_FOR_NEW_GARAGE_VEHICLE(vehGarageVeh, 0, iGarageVehicleResult)
		
			SWITCH iGarageVehicleResult
				CASE GARAGE_VEHICLE_TRANSACTION_STATE_SUCCESS
					SCRIPT_ASSERT("GARAGE_VEHICLE_TRANSACTION_STATE_SUCCESS")
				BREAK
				CASE GARAGE_VEHICLE_TRANSACTION_STATE_FAILED
					SCRIPT_ASSERT("GARAGE_VEHICLE_TRANSACTION_STATE_FAILED")
				BREAK
			ENDSWITCH
			
			b_debug_add_vehicle_to_garage = FALSE
		ENDIF
	ENDIF
	
	IF b_debug_output_tattoo_data
		INT iDLCIndex
		INT iDLCCount
		sTattooShopItemValues sDLCTattooData
		TATTOO_FACTION_ENUM eFaction

		eFaction = TATTOO_MP_FM
		iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
		PRINTLN("MALE TATTOOS - ", iDLCCount)
		REPEAT iDLCCount iDLCIndex
			IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
				PRINTLN("...[", iDLCIndex, "] label = ", sDLCTattooData.label, ", collection = ", sDLCTattooData.Collection, ", preset = ", sDLCTattooData.Preset)
			ENDIF
		ENDREPEAT
		
		eFaction = TATTOO_MP_FM_F
		iDLCCount = GET_NUM_TATTOO_SHOP_DLC_ITEMS(eFaction)
		PRINTLN("FEMALE TATTOOS - ", iDLCCount)
		REPEAT iDLCCount iDLCIndex
			IF GET_TATTOO_SHOP_DLC_ITEM_DATA(eFaction, iDLCIndex, sDLCTattooData)
				PRINTLN("...[", iDLCIndex, "] label = ", sDLCTattooData.label, ", collection = ", sDLCTattooData.Collection, ", preset = ", sDLCTattooData.Preset)
			ENDIF
		ENDREPEAT
		b_debug_output_tattoo_data = FALSE
	ENDIF

	IF b_debug_space_monkey
		UNLOCK_SPACE_MONKEY_CLOTHES_FOR_TREVOR()
		b_debug_space_monkey = FALSE
	ENDIF
	
	IF bWriteItemsToCatalogue[0]
		GENERATE_MART_ENTRIES_FOR_CATALOGUE()		//	*has inventory items*
		bWriteItemsToCatalogue[0] = FALSE
	ENDIF
	IF bWriteItemsToCatalogue[1]
		GENERATE_PROPERTY_ENTRIES_FOR_CATALOGUE()		//	*has inventory items*
		bWriteItemsToCatalogue[1] = FALSE
	ENDIF
	IF bWriteItemsToCatalogue[2]
		GENERATE_VEHICLES_ENTRIES_FOR_CATALOGUE()		//	*has inventory items*
		GENERATE_PEGASUS_VEHICLES_ENTRIES_FOR_CATALOGUE()
		GENERATE_TRUCK_VEHICLE_ENTRY_FOR_CATALOGUE()
		GENERATE_ARMORY_AIRCRAFT_ENTRY_FOR_CATALOGUE()
		GENERATE_HACKING_TRUCK_VEHICLE_ENTRY_FOR_CATALOGUE()
		#IF FEATURE_HEIST_ISLAND
		GENERATE_KOSATKA_VEHICLE_ENTRY_FOR_CATALOGUE()
		#ENDIF
		bWriteItemsToCatalogue[2] = FALSE
	ENDIF
	IF bWriteItemsToCatalogue[3]
		GENERATE_VEHICLE_MOD_ENTRIES_FOR_CATALOGUE()
		bWriteItemsToCatalogue[3] = FALSE
	ENDIF
	IF bWriteItemsToCatalogue[4]
		GENERATE_SERVICES_ENTRIES_FOR_CATALOGUE()
		bWriteItemsToCatalogue[4] = FALSE
	ENDIF
	IF bWriteItemsToCatalogue[5]
		ADD_VEHICLE_LOW_GRIP_TIRES_TO_CATALOGUE()
		bWriteItemsToCatalogue[5] = FALSE
	ENDIF
	
	IF bWriteInventoryToCatalogue
		GENERATE_VEHICLE_MOD_INVENTORY_ENTRIES_FOR_CATALOGUE()
		GENERATE_BARBER_INVENTORY_ENTRIES_FOR_CATALOGUE()
		//GENERATE_MART_INVENTORY_ENTRIES_FOR_CATALOGUE()
		GENERATE_PROPERTY_INVENTORY_ENTRIES_FOR_CATALOGUE()
		GENERATE_VEHICLE_INVENTORY_ENTRIES_FOR_CATALOGUE()
		GENERATE_PEGASUS_VEHICLES_ENTRIES_FOR_CATALOGUE()
		GENERATE_TRUCK_VEHICLE_ENTRY_FOR_CATALOGUE()
		GENERATE_ARMORY_AIRCRAFT_ENTRY_FOR_CATALOGUE()
		GENERATE_HACKING_TRUCK_VEHICLE_ENTRY_FOR_CATALOGUE()
		#IF FEATURE_HEIST_ISLAND
		GENERATE_KOSATKA_VEHICLE_ENTRY_FOR_CATALOGUE()
		#ENDIF
		
		bWriteInventoryToCatalogue = FALSE
	ENDIF
	IF bWriteInventoryToCatalogue2
		GENERATE_VEHICLE_MOD_INVENTORY_ENTRIES_FOR_CATALOGUE()
		bWriteInventoryToCatalogue2 = FALSE
	ENDIF

	IF bWriteInventoryToCatalogueCasDecoration
		ADD_CASINO_DECORATION_INVENTORY_TO_CATALOGUE()
		bWriteInventoryToCatalogueCasDecoration = FALSE
	ENDIF
	
	#IF FEATURE_COPS_N_CROOKS
	IF bWriteInventoryToCatalogueSUI
		GENERATE_SCRIPT_UNLOCABLE_ITEM_CATALOGUE_ITEMS()
		bWriteInventoryToCatalogueSUI = FALSE
	ENDIF
	#ENDIF
	
	IF b_debug_outfit_output_3
		#IF ENABLE_MP_OUTFIT_GENERATION
		EXPORT_MP_OUTFIT_DATA_TO_XML()
		#ENDIF
		b_debug_outfit_output_3 = FALSE
	ENDIF
	
	IF b_debug_outfit_output_4
		EXPORT_BASE_LOCATIONS_DATA_TO_XML(i_debug_base_property, i_debug_base_last_valid_element)
		b_debug_outfit_output_4 = FALSE
	ENDIF
	
	IF b_debug_dump_xml_property_to_log
		DUMP_BASE_LOCATIONS_TO_DEBUG_FROM_XML(i_debug_base_property)
		b_debug_dump_xml_property_to_log = FALSE
	ENDIF
	
	IF b_debug_dump_metadata_property_to_log
		DUMP_BASE_LOCATIONS_TO_DEBUG_FROM_METADATA(i_debug_base_property)
		b_debug_dump_metadata_property_to_log = FALSE
	ENDIF
	
	IF b_debug_compare_property_metadata_with_xml
		BOOL bSameData = TRUE
		
		IF i_debug_base_property = 0
			INT iPropertyID
			
			FOR iPropertyID = -1 TO MAX_MP_PROPERTIES STEP 1
				IF COMPARE_BASE_ITEMS_METADATA_WITH_XML(iPropertyID) = 0
					bSameData = FALSE
				ENDIF
			ENDFOR
		ELSE
			IF COMPARE_BASE_ITEMS_METADATA_WITH_XML(i_debug_base_property, TRUE) = 0
				bSameData = FALSE
			ENDIF
		ENDIF
		
		IF bSameData
			CDEBUG1LN(DEBUG_SHOPS, " All valid apartments have the same Metadata as Scriptdata ")
		ELSE
			CDEBUG1LN(DEBUG_SHOPS, " One or more apartments have a mismatch in Metadata with Scriptdata ")
		ENDIF
		b_debug_compare_property_metadata_with_xml = FALSE
	ENDIF
	
	
//	IF b_debug_unlock_reward_shirts
//		SET_MP_TATTOO_UNLOCKED(TSHIRT_TRANS_MP_FM_LSBELLE, TRUE)
//		
//		IF IS_PLAYER_MALE(PLAYER_ID())
//			SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(DLC_SHIRT_RETRO_BITCHN, TRUE), TATTOO_MP_FM), TRUE)
//		ELSE
//			SET_MP_TATTOO_UNLOCKED(GET_TATTOO_ENUM_FROM_DLC_HASH(GET_DLC_SHIRT_PRESET_HASH(DLC_SHIRT_RETRO_BITCHN, FALSE), TATTOO_MP_FM_F), TRUE)
//		ENDIF
//
//		
//		b_debug_unlock_reward_shirts = FALSE
//	ENDIF
	
	IF b_debug_reward_shirt_check
		PRINTLN("REWARD SHIRT CHECK: JBIB = ", IS_ITEM_A_REWARD_TSHIRT(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_JBIB, GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_JBIB)), ", SPECIAL = ", IS_ITEM_A_REWARD_TSHIRT(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_JBIB, GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(PLAYER_PED_ID(), COMP_TYPE_JBIB)))
		b_debug_reward_shirt_check = FALSE
	ENDIF
	
	IF b_debug_clear_veh_txt_delay
		INT iText
		REPEAT COUNT_OF(MPGlobalsAmbience.mpVehTexts.details) iText
			MPGlobalsAmbience.mpVehTexts.details[iText].iDelay = 0
		ENDREPEAT
		b_debug_clear_veh_txt_delay = FALSE
	ENDIF
	
	IF b_debug_check_xmas_clothing
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF IS_PED_WEARING_CHRISTMAS_CLOTHES(PLAYER_PED_ID())
				PRINTLN("IS_PED_WEARING_CHRISTMAS_CLOTHES = TRUE")
				SCRIPT_ASSERT("IS_PED_WEARING_CHRISTMAS_CLOTHES = TRUE")
			ELSE
				PRINTLN("IS_PED_WEARING_CHRISTMAS_CLOTHES = FALSE")
				SCRIPT_ASSERT("IS_PED_WEARING_CHRISTMAS_CLOTHES = FALSE")
			ENDIF
		ENDIF
		b_debug_check_xmas_clothing = FALSE
	ENDIF
	
	IF b_debug_force_reset
		FORCE_SHOP_RESET(GET_SHOP_PLAYER_LAST_USED())
		b_debug_force_reset = FALSE
	ENDIF
	
	b_debug_warning_message = IS_WARNING_MESSAGE_ACTIVE()
	
	IF b_debug_launch_store
		LAUNCH_STORE_CASH_ALERT(TRUE, DEFAULT, SPL_STORE)
		b_debug_launch_store = FALSE
	ENDIF

	IF g_sMenuData.bDrawDebugSpacers
		DRAW_RECT(g_sMenuData.fSpacerX[0], g_sMenuData.fSpacerY[0], g_sMenuData.iSpacerScale[0]*CUSTOM_MENU_PIXEL_WIDTH, g_sMenuData.iSpacerScale[0]*CUSTOM_MENU_PIXEL_HEIGHT, 255, 0, 0, 255)
		DRAW_RECT(g_sMenuData.fSpacerX[1], g_sMenuData.fSpacerY[1], g_sMenuData.iSpacerScale[1]*CUSTOM_MENU_PIXEL_WIDTH, g_sMenuData.iSpacerScale[1]*CUSTOM_MENU_PIXEL_HEIGHT, 0, 255, 0, 255)
		DRAW_RECT(g_sMenuData.fSpacerX[2], g_sMenuData.fSpacerY[2], g_sMenuData.iSpacerScale[2]*CUSTOM_MENU_PIXEL_WIDTH, g_sMenuData.iSpacerScale[2]*CUSTOM_MENU_PIXEL_HEIGHT, 0, 0, 255, 255)
		DRAW_RECT(g_sMenuData.fSpacerX[3], g_sMenuData.fSpacerY[3], g_sMenuData.iSpacerScale[3]*CUSTOM_MENU_PIXEL_WIDTH, g_sMenuData.iSpacerScale[3]*CUSTOM_MENU_PIXEL_HEIGHT, 255, 255, 255, 255)
	ENDIF

	IF bHideShopBlipsTracker != g_sShopSettings.bHideAllShopBlips
		bHideShopBlipsTracker = g_sShopSettings.bHideAllShopBlips
		bHideShopBlipsState = g_sShopSettings.bHideAllShopBlips
	ENDIF
	
	IF bHideShopBlipsState != g_sShopSettings.bHideAllShopBlips
		HIDE_ALL_SHOP_BLIPS(bHideShopBlipsState)
	ENDIF

	IF b_debug_clothing_id_test
	
		PRINTLN("GET_MP_CRATE_CLOTHING_FROM_TUNABLE")
		PRINTLN("...item hash = ", i_debug_clothing_id)
		
		PED_COMP_TYPE_ENUM eContentsClothingType
		PED_COMP_NAME_ENUM eContentsClothingName
		
		IF GET_MP_REWARD_CLOTHING_FROM_TUNABLE(MP_M_FREEMODE_01, i_debug_clothing_id, eContentsClothingType, eContentsClothingName)
			PRINTLN("...found male item!")
			SCRIPT_ASSERT("Crate Drop Test - Valid item for MP_M_FREEMODE_01")
		ELIF GET_MP_REWARD_CLOTHING_FROM_TUNABLE(MP_F_FREEMODE_01, i_debug_clothing_id, eContentsClothingType, eContentsClothingName)
			PRINTLN("...found female item!")
			SCRIPT_ASSERT("Crate Drop Test - Valid item for MP_F_FREEMODE_01")
		ELSE
			PRINTLN("...failed to find an item!")
			SCRIPT_ASSERT("Crate Drop Test - Invalid item")
		ENDIF
		
		b_debug_clothing_id_test = FALSE
	ENDIF
	
	IF b_debug_print_bitset_tattoo
		IF i_debug_tattoo_faction > ENUM_TO_INT(TATTOO_MP_FM_F)
			i_debug_tattoo_faction = ENUM_TO_INT(GET_TATTOO_FACTION_FOR_PED(PLAYER_PED_ID()))
		ENDIF
		
		i_debug_tattoo_namehash = GET_HASH_KEY(GET_CONTENTS_OF_TEXT_WIDGET(w_debug_tattoo_name))
		PRINT_BITSET_TATTOO(GET_TATTOO_ENUM_FROM_DLC_HASH(i_debug_tattoo_namehash, INT_TO_ENUM(TATTOO_FACTION_ENUM, i_debug_tattoo_faction)))
		
		b_debug_print_bitset_tattoo = FALSE
	ENDIF
	
	IF NOT IS_PC_VERSION()
		IF b_debug_quickfix_gift_gtao_items
			QUICKFIX_GIFT_LIMITED_ITEMS_TO_NEW_ACCOUNTS()
			b_debug_quickfix_gift_gtao_items = FALSE
		ENDIF
	ENDIF
	
	IF i_debug_set_bigass_vehicle_rewarded_bitindex_cache != i_debug_set_bigass_vehicle_rewarded_bitindex
		SITE_BUYABLE_VEHICLE eSBV = GET_BIGASS_VEHICLE_ENUM_FROM_INDICE(i_debug_set_bigass_vehicle_rewarded_bitindex)
		SET_CONTENTS_OF_TEXT_WIDGET(tw_debug_set_bigass_vehicle_rewarded_bitindex, GET_LABEL_BUYABLE_VEHICLE(eSBV))
		i_debug_set_bigass_vehicle_rewarded_bitindex_cache = i_debug_set_bigass_vehicle_rewarded_bitindex
	ENDIF
	
	BIGASS_VEHICLE_REWARDED_ENUM eResult = INT_TO_ENUM(BIGASS_VEHICLE_REWARDED_ENUM, i_debug_set_bigass_vehicle_rewarded_result-1)
	IF b_debug_set_bigass_vehicle_rewarded
		SITE_BUYABLE_VEHICLE eSBV
		IF SET_BIGASS_VEHICLE_REWARDED(eSBV, i_debug_set_bigass_vehicle_rewarded_transaction_status, eResult)
			i_debug_set_bigass_vehicle_rewarded_transaction_status = GENERIC_TRANSACTION_STATE_DEFAULT
			b_debug_set_bigass_vehicle_rewarded = FALSE
		ENDIF
	ENDIF
	i_debug_set_bigass_vehicle_rewarded_result = ENUM_TO_INT(eResult)+1
	
	IF  bRefreshTunables
		g_sTunableLoadingStruct.bRefreshNow = TRUE
		bRefreshTunables = FALSE
	ENDIF
	
	IF iTunerClientVehicleID != 0
		TEXT_LABEL_31 tlName
		tlName = GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_TUNER_CLIENT_VEHICLE_MODEL(iTunerClientVehicleID))
		IF DOES_TEXT_WIDGET_EXIST(TunerClientVehicleName)
			SET_CONTENTS_OF_TEXT_WIDGET(TunerClientVehicleName, tlName)
		ENDIF
	ELSE
		IF DOES_TEXT_WIDGET_EXIST(TunerClientVehicleName)
			SET_CONTENTS_OF_TEXT_WIDGET(TunerClientVehicleName, "Random")
		ENDIF
	ENDIF
	
	IF iBikerClientVehicleID != 0
		TEXT_LABEL_31 tlName
		tlName = GET_DISPLAY_NAME_FROM_VEHICLE_MODEL(GET_BIKER_CLIENT_VEHICLE_MODEL_FROM_INT(iBikerClientVehicleID))
		IF DOES_TEXT_WIDGET_EXIST(BikerClientVehicleName)
			SET_CONTENTS_OF_TEXT_WIDGET(BikerClientVehicleName, tlName)
		ENDIF
	ELSE
		IF DOES_TEXT_WIDGET_EXIST(BikerClientVehicleName)
			SET_CONTENTS_OF_TEXT_WIDGET(BikerClientVehicleName, "Random")
		ENDIF
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	MP_INT_STATS statToCheck 
	COUPON_TYPE couponToCheck
	UGC_DATE dateTime
	
	FLOAT fYaxis = 0.025
	INT iStartStat = ENUM_TO_INT(COUPON_HSW_MOD1) 
	INT i 
	
	// Aspect Ratio
	FLOAT fAspectRatio = GET_SCREEN_ASPECT_RATIO()
	FLOAT fStartingAspectRatio = 1.333333
	FLOAT fAspectRatioDiff = (fStartingAspectRatio/fAspectRatio) + 0.065
	
	IF bDisplayHSWModCouponInfo 
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(50, 255, 50, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.170 * fAspectRatioDiff, 0.005, "STRING", "D")
		
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(50, 255, 50, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.200 * fAspectRatioDiff, 0.005, "STRING", "H")
		
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(50, 255, 50, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.230 * fAspectRatioDiff, 0.005, "STRING", "M")
		
		SET_TEXT_SCALE(0.3, 0.3)
		SET_TEXT_COLOUR(50, 255, 50, 200)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.260 * fAspectRatioDiff, 0.005, "STRING", "S")
	ENDIF
	
	FOR i = 0 TO g_sMPTunables.iHSW_MOD_COUPON_LIMIT - 1
		couponToCheck = INT_TO_ENUM(COUPON_TYPE, iStartStat + i)
		statToCheck = GET_HSW_MOD_DISCOUNT_STAT(couponToCheck)
		
		IF bDisplayHSWModCouponInfo 
			CONVERT_POSIX_TIME(GET_MP_INT_CHARACTER_STAT(statToCheck), dateTime)
			
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 200)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.020, fYaxis, "STRING", GET_DEBUG_HSW_MOD_COUPON_NAME(couponToCheck))
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 200)
			DISPLAY_TEXT_WITH_NUMBER(0.170 * fAspectRatioDiff, fYaxis, "NUMBER", dateTime.nDay)
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 200)
			DISPLAY_TEXT_WITH_NUMBER(0.200 * fAspectRatioDiff, fYaxis, "NUMBER", dateTime.nHour)
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 200)
			DISPLAY_TEXT_WITH_NUMBER(0.230 * fAspectRatioDiff, fYaxis, "NUMBER", dateTime.nMinute)
			SET_TEXT_SCALE(0.3, 0.3)
			SET_TEXT_COLOUR(255, 50, 50, 200)
			DISPLAY_TEXT_WITH_NUMBER(0.260 * fAspectRatioDiff, fYaxis, "NUMBER", dateTime.nSecond)
			fYaxis += 0.025
		ENDIF
		
		IF iDebugHSWModCoupon[i] != -1
			INT iCurrentPosixTime = GET_CLOUD_TIME_AS_INT()
			SET_MP_INT_CHARACTER_STAT(statToCheck, iDebugHSWModCoupon[i] + iCurrentPosixTime)
			iDebugHSWModCoupon[i] = -1
		ENDIF
	ENDFOR 
	
	IF bRedeemHSWModCoupon
		REDEEM_HSW_MOD_COUPON()
		bRedeemHSWModCoupon = FALSE
	ENDIF	
	
	IF bObtainHSWModCoupon
		OBTAIN_HSW_MOD_COUPON()
		bObtainHSWModCoupon = FALSE
	ENDIF	
	#ENDIF
ENDPROC
#ENDIF

FUNC BOOL SHOULD_SHOP_BLIP_BE_REMOVED_WHEN_INSIDE(SHOP_NAME_ENUM eShop)
	IF eShop = CLOTHES_SHOP_A_01_VB
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL SHOULD_SHOP_BLIP_BE_REMOVED_FOR_MISSION(SHOP_NAME_ENUM eShop)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// Fix for bug #584436 - Keep gunshop blip on during CnC missions
		// Fix for bug #549442 - Turn off all shop blips on FM missions
		// Fix for bug #988012 - Ran out of ammo doing a gang hideout but the weapon shop blips were gone. 
		// Fix for bug #1355929 - Can we have Ammunation blipped while in Missions. 
		IF bPlayerOnMPMission
		AND bTutorialsComplete
			IF NOT IS_SHOP_AVAILABLE_FOR_FM_MISSSION_TYPE(eShop, GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()))
				RETURN TRUE
			ENDIF
		ENDIF
	ELSE
		IF eShop = GUN_SHOP_01_DT
			IF IS_MISSION_AVAILABLE(SP_MISSION_FBI_4_PREP_5)
			AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_4_PREP_5)
				RETURN TRUE
			ENDIF
		ELIF eShop = CLOTHES_SHOP_A_01_VB
			IF IS_MISSION_AVAILABLE(SP_MISSION_FBI_4_PREP_4)
			AND NOT GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_4_PREP_4)
				RETURN TRUE
			ENDIF
		ENDIF
		
		// Fix for bug #1317077 - on missions switch off barber, clothes shop blips
		IF bPlayerOnSPMission
			IF GET_SHOP_TYPE_ENUM(eShop) = SHOP_TYPE_CLOTHES
			OR GET_SHOP_TYPE_ENUM(eShop) = SHOP_TYPE_HAIRDO
			OR GET_SHOP_TYPE_ENUM(eShop) = SHOP_TYPE_TATTOO
				RETURN TRUE
			ENDIF
		ENDIF
		
	ENDIF
	
	RETURN FALSE
ENDFUNC

#IF FEATURE_CASINO_HEIST
PROC MAINTAIN_CASINO_HEIST_MASK_SHOP_BLIP_COLOR_OVERRIDE(SHOP_NAME_ENUM eShop)
	IF NOT DOES_BLIP_EXIST(g_sShopSettings.blipID[eShop])
		EXIT
	ENDIF
	
	// Only override the mask shop color
	IF eShop != CLOTHES_SHOP_A_01_VB
		EXIT
	ENDIF
	
	BOOL bHeistMasksActive = SHOULD_SHOW_CASINO_HEIST_MASK_SETS_MENU(CLO_MENU_MASKS)
	IF bHeistMasksActive
		// Change blip to green if not already green
		IF GET_BLIP_COLOUR(g_sShopSettings.blipID[eShop]) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_GREEN)
			SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_sShopSettings.blipID[eShop], HUD_COLOUR_GREEN)
		ENDIF		
	// Heist masks shouldn't show so change blip colour back to grey
	ELIF GET_BLIP_COLOUR(g_sShopSettings.blipID[eShop]) != GET_BLIP_COLOUR_FROM_HUD_COLOUR(HUD_COLOUR_GREYLIGHT)
		SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_sShopSettings.blipID[eShop], HUD_COLOUR_GREYLIGHT)
	ENDIF
	
	SET_BLIP_AS_SHORT_RANGE(g_sShopSettings.blipID[eShop], NOT bHeistMasksActive)
	SET_BLIP_MARKER_LONG_DISTANCE(g_sShopSettings.blipID[eShop], bHeistMasksActive)
ENDPROC
#ENDIF

/// PURPOSE: Makes sure all the shop blips are in the correct state with the blip controller
PROC UPDATE_SHOP_BLIP(SHOP_NAME_ENUM eShop, BOOL bForce)

	IF g_sShopSettings.bUpdateShopBlip[eShop] OR bForce
	
		BOOL bDisplayBlip = FALSE
		STATIC_BLIP_NAME_ENUM eBlip = GET_SHOP_BLIP_ENUM(eShop)
		SHOP_TYPE_ENUM eType = GET_SHOP_TYPE_ENUM(eShop)
		
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			// Only process SP blips
			IF IS_SHOP_AVAILABLE_IN_SINGLEPLAYER(eShop)
			
				IF IS_BIT_SET(g_savedGlobals.sShopData.iProperties[eShop], SHOP_S_FLAG_SHOP_AVAILABLE)
				AND (NOT IS_BIT_SET(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_PLAYER_IN_SHOP) OR NOT SHOULD_SHOP_BLIP_BE_REMOVED_WHEN_INSIDE(eShop))
				AND NOT SHOULD_SHOP_BLIP_BE_REMOVED_FOR_MISSION(eShop)
					bDisplayBlip = TRUE
				ENDIF

				// If we have visited this type of shop before then set short range blip
				IF HAS_PLAYER_VISITED_SHOP_TYPE(eType)
					// Unless we have requested for it to be long range elsewhere in script
					IF NOT IS_SHOP_BLIP_LONG_RANGE(eShop)
						SET_STATIC_BLIP_APPEAR_EDGE_RADAR(GET_SHOP_BLIP_ENUM(eShop), FALSE)
					ENDIF
				ENDIF
				
				IF IS_SHOP_BLIP_LONG_RANGE(eShop)
					SET_STATIC_BLIP_APPEAR_EDGE_RADAR(eBlip, TRUE)
				ELSE
					SET_STATIC_BLIP_APPEAR_EDGE_RADAR(eBlip, FALSE)
				ENDIF
				
				SET_STATIC_BLIP_HIDE_IN_INTERIOR(eBlip,TRUE)
				SET_STATIC_BLIP_ICON(eBlip, GET_SHOP_BLIP_SPRITE(eShop)) 								/* Sprite		*/
				//SET_STATIC_BLIP_FLASH_ON_ACTIVE(eBlip, SHOULD_SHOP_BLIP_FLASH_WHEN_ACTIVATED(eShop))	/* Flash		*/
				SET_STATIC_BLIP_ACTIVE_STATE(eBlip, bDisplayBlip)										/* Active 		*/
				SET_STATIC_BLIP_NAME(eBlip, GET_SHOP_BLIP_NAME(eShop))									/* Name			*/
			ENDIF
		ENDIF
		
		// Multiplayer doesn't use the blip controller so maintain manually
		bDisplayBlip = FALSE
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			
			IF eType != SHOP_TYPE_PERSONAL_CARMOD
				IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
					IF NOT g_sShopSettings.bHideAllShopBlips
					AND NOT g_sShopSettings.bHideShopBlipsByType[GET_SHOP_TYPE_ENUM(eShop)]
					AND (IS_BIT_SET(GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_SHOP(eShop)), SHOP_S_FLAG_SHOP_AVAILABLE))
					AND (NOT IS_BIT_SET(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_PLAYER_IN_SHOP) OR NOT SHOULD_SHOP_BLIP_BE_REMOVED_WHEN_INSIDE(eShop))
					AND IS_SHOP_AVAILABLE_IN_MULTIPLAYER_FM(eShop)
					AND NOT bPlayerOnMPActivityTurorial
					AND NOT SHOULD_SHOP_BLIP_BE_REMOVED_FOR_MISSION(eShop)
					AND NOT g_bVSMission
					AND NOT g_b_On_Deathmatch
					#IF FEATURE_GEN9_EXCLUSIVE
					AND NOT IS_PLAYER_ON_MP_INTRO()
					#ENDIF
						bDisplayBlip = TRUE
					ENDIF
					
					IF eShop = CLOTHES_SHOP_A_01_VB
						IF SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_MASKS)
							bDisplayBlip = FALSE
						ENDIF
					ELSE
						SWITCH eType
							CASE SHOP_TYPE_CARMOD
								IF SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_MOD_SHOP)
									bDisplayBlip = FALSE
								ENDIF
							BREAK
							CASE SHOP_TYPE_GUN
								IF SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_AMMUNATION)
									bDisplayBlip = FALSE
								ENDIF
							BREAK
							CASE SHOP_TYPE_CLOTHES
								IF SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_CLOTHES)
									bDisplayBlip = FALSE
								ENDIF
							BREAK
							CASE SHOP_TYPE_HAIRDO
								IF SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_BARBER)
									bDisplayBlip = FALSE
								ENDIF
							BREAK
							CASE SHOP_TYPE_TATTOO
								IF SHOULD_HIDE_MISC_BLIP(ciPI_HIDE_MENU_ITEM_MISC_STORES_TATTOO)
									bDisplayBlip = FALSE
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
					
					// url:bugstar:2546616 - Can we disable the Benny's blip / help text on this mission?
					IF NETWORK_IS_ACTIVITY_SESSION()
						IF IS_LOADED_MISSION_LOW_FLOW_MISSION(ciLOW_FLOW_MISSION_DRIVE_BY_SHOOTOUT)
							bDisplayBlip = FALSE
							
							#IF IS_DEBUG_BUILD
								IF DOES_BLIP_EXIST(g_sShopSettings.blipID[eShop])
									PRINTLN("kr_debug: removing supermod blip as player is on ciLOW_FLOW_MISSION_DRIVE_BY_SHOOTOUT")
								ENDIF
							#ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				bDisplayBlip = FALSE
			ENDIF
		ENDIF
		
		IF bDisplayBlip
			// Turn blip on
			IF NOT DOES_BLIP_EXIST(g_sShopSettings.blipID[eShop])
			
				PRINTLN("shop_controller: Turning ON blip for shop ", GET_STRING_FROM_TEXT_FILE(GET_SHOP_NAME(eShop)))
				
				g_sShopSettings.blipID[eShop] = ADD_BLIP_FOR_COORD(GET_SHOP_COORDS(eShop))
				SET_BLIP_SPRITE(g_sShopSettings.blipID[eShop], GET_SHOP_BLIP_SPRITE(eShop))
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_sShopSettings.blipID[eShop], HUD_COLOUR_GREYLIGHT)	//#1708503
				ENDIF
				
				// url:bugstar:7233746
//				IF eShop = CARMOD_SHOP_SUPERMOD
//					SET_BLIP_COLOUR_FROM_HUD_COLOUR(g_sShopSettings.blipID[eShop], HUD_COLOUR_YELLOWLIGHT)
//				ENDIF
				
				SET_BLIP_DISPLAY(g_sShopSettings.blipID[eShop], DISPLAY_BOTH)
				SET_BLIP_NAME_FROM_TEXT_FILE(g_sShopSettings.blipID[eShop], GET_SHOP_BLIP_NAME(eShop))
				SET_BLIP_AS_MISSION_CREATOR_BLIP(g_sShopSettings.blipID[eShop], FALSE)
				SET_BLIP_PRIORITY(g_sShopSettings.blipID[eShop], BLIPPRIORITY_LOW)
				
				
				IF eShop = CARMOD_SHOP_SUPERMOD
				AND IS_SHOP_BIT_SET(eShop, SHOP_S_FLAG_FLASH_BLIP_WHEN_ACTIVATED)
					SET_BLIP_FLASH_TIMER(g_sShopSettings.blipID[eShop], ciLOW_FLOW_BLIP_FLASH_TIMER)
				ELSE
					SET_BLIP_FLASHES(g_sShopSettings.blipID[eShop], IS_SHOP_BIT_SET(eShop, SHOP_S_FLAG_FLASH_BLIP_WHEN_ACTIVATED))
				ENDIF
				
				CLEAR_SHOP_BIT(eShop, SHOP_S_FLAG_FLASH_BLIP_WHEN_ACTIVATED, FALSE)
				//SET_BLIP_AS_MISSION_CREATOR_BLIP(g_sShopSettings.blipID[eShop], TRUE)
				SET_BLIP_HIGH_DETAIL(g_sShopSettings.blipID[eShop], FALSE)
			ELSE
				IF eShop = CARMOD_SHOP_SUPERMOD
				AND IS_SHOP_BIT_SET(eShop, SHOP_S_FLAG_FLASH_BLIP_WHEN_ACTIVATED)
					SET_BLIP_FLASH_TIMER(g_sShopSettings.blipID[eShop], ciLOW_FLOW_BLIP_FLASH_TIMER)
					CLEAR_SHOP_BIT(eShop, SHOP_S_FLAG_FLASH_BLIP_WHEN_ACTIVATED, FALSE)
				ENDIF
				
				IF IS_SHOP_BIT_SET(eShop, SHOP_NS_FLAG_FLASH_BLIP, TRUE)
					SET_BLIP_FLASHES(g_sShopSettings.blipID[eShop], TRUE)
					SET_BLIP_FLASH_INTERVAL(g_sShopSettings.blipID[eShop], BLIP_FLASHING_TIME)
					SET_BLIP_FLASH_TIMER(g_sShopSettings.blipID[eShop], 7000)
					CLEAR_SHOP_BIT(eShop, SHOP_NS_FLAG_FLASH_BLIP, TRUE)
				ENDIF
			ENDIF
			
				// Override coords for supermod, casino
				IF eShop = CARMOD_SHOP_SUPERMOD
				OR eShop = CLOTHES_SHOP_CASINO
					SET_BLIP_COORDS(g_sShopSettings.blipID[eShop], GET_SHOP_COORDS(eShop, bFlowStrandAtModShop))
				ENDIF
			
			SET_BLIP_AS_SHORT_RANGE(g_sShopSettings.blipID[eShop], (NOT IS_SHOP_BLIP_LONG_RANGE(eShop)))
		ELSE
			// Turn blip off
			IF DOES_BLIP_EXIST(g_sShopSettings.blipID[eShop])
			
				PRINTLN("shop_controller: Turning OFF blip for shop ", GET_STRING_FROM_TEXT_FILE(GET_SHOP_NAME(eShop)))
				
				REMOVE_BLIP(g_sShopSettings.blipID[eShop])
			ENDIF
		ENDIF
		
		g_sShopSettings.bUpdateShopBlip[eShop] = FALSE
	ENDIF
	
	#IF FEATURE_CASINO_HEIST
	MAINTAIN_CASINO_HEIST_MASK_SHOP_BLIP_COLOR_OVERRIDE(eShop)
	#ENDIF	
ENDPROC

PROC DISPLAY_BARBER_BLIP_FIRST_SEEN_HELP_TEXT()
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		//Check for displaying of barber blip help the first time one of the barber blips are seen. #413305
		IF NOT g_savedGlobals.sShopData.bBarberBlipHelpShown
			IF NOT bBarberBlipHelpQueued
				IF IS_STATIC_BLIP_CURRENTLY_ON_MINIMAP(GET_SHOP_BLIP_ENUM(HAIRDO_SHOP_01_BH))
				OR IS_STATIC_BLIP_CURRENTLY_ON_MINIMAP(GET_SHOP_BLIP_ENUM(HAIRDO_SHOP_02_SC))
					IF NOT bBarberBlipVisibleThisFrame
						IF NOT IS_RADAR_HIDDEN()
							bBarberBlipVisibleThisFrame = TRUE
							//Make sure a blip has been visible for more than one frame.
							//Safeguards against the one frame blips return being visible
							//after leaving the pause menu.
							IF bBarberBlipVisibleLastFrame
								ADD_HELP_TO_FLOW_QUEUE("AM_H_HAIR", FHP_MEDIUM, 0, 2000)
								bBarberBlipHelpQueued = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF 
				bBarberBlipVisibleLastFrame = bBarberBlipVisibleThisFrame
			ELSE
				//Check if queued help text managed to display.
				IF GET_FLOW_HELP_MESSAGE_STATUS("AM_H_HAIR") = FHS_DISPLAYED
					g_savedGlobals.sShopData.bBarberBlipHelpShown = TRUE
				ELIF GET_FLOW_HELP_MESSAGE_STATUS("AM_H_HAIR") = FHS_EXPIRED
					bBarberBlipHelpQueued = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


PROC PRINT_SHOP_CLOSED_DEBUG(SHOP_NAME_ENUM eShop, STRING sText)
	IF eShop = EMPTY_SHOP
	OR IS_STRING_NULL_OR_EMPTY(sText)
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
		IF IS_BIT_SET(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_SHOP_OPEN)
			PRINTLN("\n shop_controller: ", GET_STRING_FROM_TEXT_FILE(GET_SHOP_NAME(eShop)), " CLOSING: ",  sText)
		ENDIF
	#ENDIF
ENDPROC

/// PURPOSE: Check game states to see if shop should lock up
FUNC BOOL SHOULD_SHOP_OPEN_FOR_BUSINESS(SHOP_NAME_ENUM eShop, INT &iGlobalSavedFlag)

	// Only close if we are not browsing
	IF NOT IS_BIT_SET(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_PLAYER_BROWSING_ITEMS_IN_SHOP)
		 
		// Close if not available yet
		IF NOT IS_BIT_SET(iGlobalSavedFlag, SHOP_S_FLAG_SHOP_AVAILABLE)
			PRINT_SHOP_CLOSED_DEBUG(eShop, "unavailable")
			RETURN FALSE
		ENDIF
		IF IS_BIT_SET(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_SHOP_TEMPORARILY_UNAVAILABLE)
			PRINT_SHOP_CLOSED_DEBUG(eShop, "temporarily unavailable")
			RETURN FALSE
		ENDIF
		
		// Close in all situations
		IF IS_BIT_SET(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_PLAYER_KICKING_OFF)
			PRINT_SHOP_CLOSED_DEBUG(eShop, "player kicking off")
			RETURN FALSE
		ENDIF
		
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			// Debug flag not set
			#IF IS_DEBUG_BUILD
				IF NOT g_savedGlobals.sFlow.isGameflowActive
				AND NOT g_bShopsAvailableInDebug
				AND NOT	IS_CURRENTLY_ON_MISSION_TO_TYPE()
				AND NOT g_bMagDemoActive
					PRINT_SHOP_CLOSED_DEBUG(eShop, "debug flag not set")
					RETURN FALSE
				ENDIF
			#ENDIF
			
			// Currently being robbed
			IF IS_BIT_SET(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_SHOP_BEING_ROBBED)
				PRINT_SHOP_CLOSED_DEBUG(eShop, "shop being robbed")
				RETURN FALSE
			ENDIF
			
			// Clothes shops blocked on mission
			IF IS_CURRENTLY_ON_MISSION_TO_TYPE()
			AND NOT CAN_PLAYER_CHANGE_CLOTHES_ON_MISSION()
			AND GET_SHOP_TYPE_ENUM(eShop) = SHOP_TYPE_CLOTHES
			// keep shop open in Lester 1a (mission script blocks locates, so player can't buy more items)
			AND MISSION_FLOW_GET_RUNNING_MISSION() != SP_MISSION_LESTER_1
				PRINT_SHOP_CLOSED_DEBUG(eShop, "not allowed to change clothes on mission")
				RETURN FALSE
			ENDIF
			
			// On rampage or minigame
			IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RAMPAGE)
			OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME)
			OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
				SET_BIT(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_CLOSED_FOR_MINIGAME)
				PRINT_SHOP_CLOSED_DEBUG(eShop, "on rampage or minigame")
				RETURN FALSE
			ENDIF
			
			
			// Not for singleplayer
			IF NOT IS_SHOP_AVAILABLE_IN_SINGLEPLAYER(eShop)
				PRINT_SHOP_CLOSED_DEBUG(eShop, "not available in singleplayer")
				RETURN FALSE
			ENDIF
			
			// Being robbed
			IF (GET_GAME_TIMER() < iRobbedTimer[eShop])
				PRINT_SHOP_CLOSED_DEBUG(eShop, "currently being robbed")
				RETURN FALSE
			ENDIF
			
			// Not a SP character
			IF NOT IS_PLAYER_PED_PLAYABLE(eCurrentPed)
				PRINT_SHOP_CLOSED_DEBUG(eShop, "not SP character")
				RETURN FALSE
			ENDIF
		ELSE
			// FM checks
			IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
				// On mission
				IF bPlayerOnMPMission
				AND bTutorialsComplete
				AND NOT IS_SHOP_AVAILABLE_FOR_FM_MISSSION_TYPE(eShop, GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID()))
					SET_BIT(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_CLOSED_FOR_MISSION)
					PRINT_SHOP_CLOSED_DEBUG(eShop, "on mission")
					RETURN FALSE
				ENDIF
				
				IF g_bVSMission
					PRINT_SHOP_CLOSED_DEBUG(eShop, "on vs mission")
					RETURN FALSE
				ENDIF
				
				IF g_b_On_Deathmatch
					PRINT_SHOP_CLOSED_DEBUG(eShop, "on deathmatch")
					RETURN FALSE
				ENDIF
				
				// Not for this mode
				IF NOT IS_SHOP_AVAILABLE_IN_MULTIPLAYER_FM(eShop)
					PRINT_SHOP_CLOSED_DEBUG(eShop, "not available in freemode")
					RETURN FALSE
				ENDIF
				
				IF  IS_PLAYER_AN_ANIMAL(PLAYER_ID())
					PRINT_SHOP_CLOSED_DEBUG(eShop, "not available when player is an animal")
					RETURN FALSE
				ENDIF
				
				IF NETWORK_IS_ACTIVITY_SESSION()
					IF IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThree, ciDISABLE_MOD_SHOP) 
						IF GET_SHOP_TYPE_ENUM(eShop) = SHOP_TYPE_CARMOD
							PRINT_SHOP_CLOSED_DEBUG(eShop, "blocked by creator")
							RETURN FALSE
						ENDIF
					ENDIF
					
					IF IS_THIS_A_SURVIVAL()
						PRINT_SHOP_CLOSED_DEBUG(eShop, "on Survival")
						RETURN FALSE
					ENDIF
				ENDIF
			
			// Invalid mp mode
			ELSE
				PRINT_SHOP_CLOSED_DEBUG(eShop, "not in valid MP mode")
				RETURN FALSE
			ENDIF
		ENDIF
		
		//
		// [!] Do wee need to setup up opening hours for each shop?
		//
	ENDIF
	
	// No issues so shop should be open
	RETURN TRUE
ENDFUNC

/// PURPOSE: Updates various states for each shop
PROC UPDATE_MISC_SHOP_FLAGS(SHOP_NAME_ENUM eShop, INT &iGlobalSavedFlag)

	// Distance to shop
	g_sShopSettings.fDistToShop[eShop] = GET_DISTANCE_BETWEEN_COORDS(GET_SHOP_COORDS(eShop), GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE))
	
	// Robbed flags
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		IF IS_SHOP_BEING_ROBBED(eShop)
			iRobbedTimer[eShop] = GET_GAME_TIMER() + 10000
		ENDIF
	ENDIF
	
	// Open for business
	IF SHOULD_SHOP_OPEN_FOR_BUSINESS(eShop, iGlobalSavedFlag)
		IF NOT IS_BIT_SET(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_SHOP_OPEN)
			PRINT_SHOP_DEBUG_LINE("Opening shop", eShop)
			SET_BIT(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_SHOP_OPEN)
			SET_SHOP_BLIP_NEEDS_UPDATED(eShop)
		ENDIF
	ELSE
		IF IS_BIT_SET(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_SHOP_OPEN)
			PRINT_SHOP_DEBUG_LINE("Closing shop", eShop)
			CLEAR_BIT(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_SHOP_OPEN)
			SET_SHOP_BLIP_NEEDS_UPDATED(eShop)
		ENDIF
	ENDIF
	
	// Open state updated so allow shops that were forced to reset to process
	CLEAR_BIT(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_FORCE_RESET_PROCESSING)
ENDPROC

/// PURPOSE: Processes any shop specific states
PROC UPDATE_SHOP_SPECIFIC_STATES(SHOP_NAME_ENUM eShop)
	SWITCH eShop
		CASE CARMOD_SHOP_01_AP
		CASE CARMOD_SHOP_05_ID2
		CASE CARMOD_SHOP_06_BT1
		CASE CARMOD_SHOP_07_CS1
		CASE CARMOD_SHOP_08_CS6
		CASE CARMOD_SHOP_SUPERMOD
		CASE CARMOD_SHOP_PERSONALMOD
		
			// Carmod vehicle repair
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
					IF g_savedGlobals.sShopData.sVehicleService[eCurrentPed].bProcessing
						IF NOT g_savedGlobals.sShopData.sVehicleService[eCurrentPed].bMessageSent
						
							// Do not process if we are sending new data to app...
							IF NOT g_savedGlobals.sSocialData.sCarAppData[eCurrentPed].bSendDataToCloud
							
								// Delay the timer if the player is still in the area when we reach 0
								IF g_savedGlobals.sShopData.sVehicleService[eCurrentPed].iHoursToComplete <= 0
									IF NOT IS_PED_INJURED(PLAYER_PED_ID())
										IF GET_DISTANCE_BETWEEN_PLAYER_AND_SHOP(eShop) < 25
											g_savedGlobals.sShopData.sVehicleService[eCurrentPed].iHoursToComplete++
											iCarmodSPTimer = GET_GAME_TIMER()
										ENDIF
									ENDIF
								ENDIF
								
								// Reset timer
								IF NOT bCarmodProcessInit
									iCarmodSPTimer = GET_GAME_TIMER()
									bCarmodProcessInit = TRUE
								
								// Reduce iHoursToComplete every hour
								ELIF g_savedGlobals.sShopData.sVehicleService[eCurrentPed].iHoursToComplete > 0
									IF (GET_GAME_TIMER() - iCarmodSPTimer) > (GET_MILLISECONDS_PER_GAME_MINUTE() * 60)
										iCarmodSPTimer = GET_GAME_TIMER()
										g_savedGlobals.sShopData.sVehicleService[eCurrentPed].iHoursToComplete--
									ENDIF
									
								// Done
								ELSE
									SWITCH g_savedGlobals.sShopData.sVehicleService[eCurrentPed].iType // we set this in carmod_shop.sc when we call START_PROCESSING_CARMOD_VEHICLE()
										CASE 2 // Recover last modded vehicle
											IF SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_MECHANIC, "MECH_COLLECT", TXTMSG_LOCKED)
												g_savedGlobals.sShopData.sVehicleService[eCurrentPed].bMessageSent = TRUE
												g_savedGlobals.sShopData.sVehicleService[eCurrentPed].bReadyForCollection = TRUE
											ENDIF
										BREAK
									ENDSWITCH
								ENDIF
							ENDIF
						ENDIF
					ELSE
						bCarmodProcessInit = FALSE
					ENDIF
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

PROC PROCESS_SHOP_SCRIPT_LAUNCHED_CLEANUP(SHOP_NAME_ENUM eShop)
	IF g_sShopSettings.bShopScriptLaunched[eShop]
		#IF IS_DEBUG_BUILD
			PRINTLN("shop_controller: Clearing launched flag for shop '", GET_SHOP_NAME(eShop), "'")
		#ENDIF
		
		// Set the robbery flag
		IF NOT IS_BIT_SET(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_CAN_ROBBERY_TAKE_PLACE_IN_SHOP)
			// Allow the player to visit the shop a few times once before allowing robbery
			SWITCH GET_SHOP_TYPE_ENUM(eShop)
				CASE SHOP_TYPE_HAIRDO
					IF g_savedGlobals.sShopData.iHairdoShopVisits >= 2
						#IF IS_DEBUG_BUILD
							PRINTLN("shop_controller: Allowing robbery to take place in shop '", GET_SHOP_NAME(eShop), "'")
						#ENDIF
						SET_BIT(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_CAN_ROBBERY_TAKE_PLACE_IN_SHOP)
					ENDIF
				BREAK
				CASE SHOP_TYPE_CLOTHES
					IF g_savedGlobals.sShopData.iClothesShopVisits >= 2
						#IF IS_DEBUG_BUILD
							PRINTLN("shop_controller: Allowing robbery to take place in shop '", GET_SHOP_NAME(eShop), "'")
						#ENDIF
						SET_BIT(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_CAN_ROBBERY_TAKE_PLACE_IN_SHOP)
					ENDIF
				BREAK
			ENDSWITCH
		ENDIF
		
		// Available when out of range
		IF IS_SHOP_BIT_SET(eShop, SHOP_S_FLAG_SHOP_AVAILABLE_WHEN_OUT_OF_RANGE)
			SET_SHOP_IS_AVAILABLE(eShop, TRUE, FALSE)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			IF IS_BIT_SET(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_SHOP_SCRIPT_RUNNING)
				PRINTLN("shop_controller: Shop script must have terminated unexpectedly for shop '", GET_SHOP_NAME(eShop), "'")
			ENDIF
		#ENDIF
		
		CLEAR_BIT(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_CLEAR_SHOP_LAUNCHED_FLAG)
		CLEAR_BIT(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_FORCE_RESET)
		CLEAR_BIT(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_FORCE_RESET_PROCESSING)
		CLEAR_BIT(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_FORCE_RESET_LINKED)
		
		// We also need to clear these flags in case the shop script terminated unexpectedly
		CLEAR_BIT(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_SHOP_SCRIPT_RUNNING)
		SET_PLAYER_IS_BROWSING_ITEMS_IN_SHOP(eShop, FALSE)
		
		CLEAR_SHOP_BIT(eShop, SHOP_NS_FLAG_PLAYER_KICKING_OFF, TRUE)
		CLEAR_SHOP_BIT(eShop, SHOP_NS_FLAG_FORCE_RESET, TRUE)
		CLEAR_SHOP_BIT(eShop, SHOP_NS_FLAG_FORCE_RESET_PROCESSING, TRUE)
		CLEAR_SHOP_BIT(eShop, SHOP_NS_FLAG_CLOSED_FOR_MISSION, TRUE)
		CLEAR_SHOP_BIT(eShop, SHOP_NS_FLAG_CLOSED_FOR_MINIGAME, TRUE)
		CLEAR_SHOP_BIT(eShop, SHOP_NS_FLAG_PLAYER_TRYING_TO_ENTER_MOD_SHOP, TRUE)
		CLEAR_SHOP_BIT(eShop, SHOP_NS_FLAG_FORCE_RESET_LINKED, TRUE)
		SET_SHOP_BLIP_NEEDS_UPDATED(eShop)
		STOP_ALL_SHOP_EVENTS(eShop)
		REMOVE_USE_OF_SHOP_KEEPER(PLAYER_ID(), eShop)
		
		g_sShopSettings.bShopScriptLaunched[eShop] = FALSE
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the shop script with a specific instance is running.
/// PARAMS:
///    sScriptName - Script file name
///    eShop - Shop enum used for check specific network instance
///    bLinkedShop - If this the main shop script or a linked one, ie clothes shop that runs as part of gunshop. This can be used to ensure we do correct instance offsets.
/// RETURNS:
///    
FUNC BOOL IS_SHOP_SCRIPT_RUNNING(STRING sScriptName, SHOP_NAME_ENUM eShop)
	INT iInstanceID = GET_SHOP_INSTANCE_ID(eShop)
	IF iInstanceID != -1
		IF NETWORK_IS_SCRIPT_ACTIVE(sScriptName, iInstanceID, TRUE)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC 

/// PURPOSE: Laucnhes the shop scripts when the player gets in range
PROC UPDATE_SHOP_LAUNCH_STATE(SHOP_NAME_ENUM eShop)
	
	TEXT_LABEL_15 sShopScript = GET_SHOP_SCRIPT(eShop)
	TEXT_LABEL_15 sLinkedShopScript = GET_LINKED_SHOP_SCRIPT(eShop)
	
	INT iThreadsRequires
	IF GET_HASH_KEY(sLinkedShopScript) = 0
		iThreadsRequires = 1
	ELSE
		iThreadsRequires = 2
	ENDIF
	
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
		// Cleanup
		IF g_sShopSettings.bShopScriptLaunched[eShop]
			// Wait for the player to get out of range
			IF IS_BIT_SET(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_CLEAR_SHOP_LAUNCHED_FLAG)
			OR (NOT IS_SHOP_WITHIN_ACTIVATION_RANGE(eShop)
			AND (NOT IS_BIT_SET(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_SHOP_SCRIPT_RUNNING) OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(GET_HASH_KEY(sShopScript)) = 0))
				PROCESS_SHOP_SCRIPT_LAUNCHED_CLEANUP(eShop)
			ENDIF
		
		// Request
		ELIF NOT g_sShopSettings.bShopScriptRequested[eShop]
			// Start requesting script when player gets in range and the script is not set as running
			IF IS_SHOP_WITHIN_ACTIVATION_RANGE(eShop)
			AND (NETWORK_IS_GAME_IN_PROGRESS() OR IS_PLAYER_PED_PLAYABLE(eCurrentPed))
			AND NOT IS_BIT_SET(g_sShopSettings.iProperties[eShop], SHOP_NS_FLAG_SHOP_SCRIPT_RUNNING)
			AND ((NETWORK_IS_GAME_IN_PROGRESS() AND IS_SHOP_AVAILABLE_IN_MULTIPLAYER_FM(eShop) AND !IS_PLAYER_IN_RACE_PLAYING_STATE(PLAYER_ID())) OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND IS_SHOP_AVAILABLE_IN_SINGLEPLAYER(eShop)))
			AND eShop != CARMOD_SHOP_PERSONALMOD	// we don't want shop_controller to start CARMOD_SHOP_PERSONALMOD
			
				REQUEST_SCRIPT(sShopScript)
				
				IF iThreadsRequires = 2
					REQUEST_SCRIPT(sLinkedShopScript)
				ENDIF
				
				PRINT_SHOP_DEBUG_LINE("Requesting shop script", eShop)
				
				g_sShopSettings.bShopScriptRequested[eShop] = TRUE
			ENDIF
		
		// Launch
		ELSE
			INT iStackCount[2] // 0: SHOP_STACK_SIZE, 1: CAR_MOD_SHOP_STACK_SIZE
			
			SWITCH GET_STACK_SIZE_FOR_SHOP_SCRIPT(sShopScript)
				CASE SHOP_STACK_SIZE
					iStackCount[0]++ // 0: SHOP_STACK_SIZE
				BREAK
				CASE CAR_MOD_SHOP_STACK_SIZE
					iStackCount[1]++ // 1: CAR_MOD_SHOP_STACK_SIZE
				BREAK
				DEFAULT
					PRINTLN("UPDATE_SHOP_LAUNCH_STATE - Missing stack Ref. Bug Default Design (Online Technical).")
					SCRIPT_ASSERT("UPDATE_SHOP_LAUNCH_STATE - Missing stack Ref. Bug Default Design (Online Technical).")
				BREAK
			ENDSWITCH
			
			REQUEST_SCRIPT(sShopScript)
			IF iThreadsRequires = 2
				REQUEST_SCRIPT(sLinkedShopScript)
				
				SWITCH GET_STACK_SIZE_FOR_SHOP_SCRIPT(sLinkedShopScript)
					CASE SHOP_STACK_SIZE
						iStackCount[0]++ // 0: SHOP_STACK_SIZE
					BREAK
					CASE CAR_MOD_SHOP_STACK_SIZE
						iStackCount[1]++ // 1: CAR_MOD_SHOP_STACK_SIZE
					BREAK
					DEFAULT
						PRINTLN("UPDATE_SHOP_LAUNCH_STATE - Missing stack Ref. Bug Default Design (Online Technical).")
						SCRIPT_ASSERT("UPDATE_SHOP_LAUNCH_STATE - Missing stack Ref. Bug Default Design (Online Technical).")
					BREAK
				ENDSWITCH
			ENDIF
			
			IF HAS_SCRIPT_LOADED(sShopScript)
			AND (iThreadsRequires = 1 OR HAS_SCRIPT_LOADED(sLinkedShopScript))
				
				// Ensure we have enough free stacks
				IF GET_NUMBER_OF_FREE_STACKS_OF_THIS_SIZE(SHOP_STACK_SIZE) >= iStackCount[0] // 0: SHOP_STACK_SIZE
				AND GET_NUMBER_OF_FREE_STACKS_OF_THIS_SIZE(CAR_MOD_SHOP_STACK_SIZE) >= iStackCount[1] // 1: CAR_MOD_SHOP_STACK_SIZE
				AND NOT IS_TRANSITION_ACTIVE() 
				AND NOT IS_PLAYER_SWITCH_IN_PROGRESS() 
				AND NOT IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY) 
				AND GET_SPAWN_ACTIVITY() = SPAWN_ACTIVITY_NOTHING
					
					BOOL bLaunchShopScript = FALSE
					BOOL bWalkingIntoSimpleInterior = IS_PLAYER_WALKING_INTO_SIMPLE_INTERIOR(PLAYER_ID())
					
					#IF FEATURE_TUNER
					BOOL bPrivateCarMeet = IS_LOCAL_PLAYER_IN_PRIVATE_CAR_MEET_OR_SANDBOX()
					#ENDIF
					
					SWITCH eShop
						CASE GUN_SHOP_ARMORY
							IF (g_iGunModInstance != -1)
								bLaunchShopScript = TRUE
							ENDIF
						BREAK
						CASE GUN_SHOP_ARMORY_AVENGER
							IF (g_iGunModInstance != -1 AND NOT bWalkingIntoSimpleInterior)
								bLaunchShopScript = TRUE
							ENDIF
						BREAK
						CASE GUN_SHOP_ARMORY_HACKER_TRUCK
							IF (g_iGunModInstance != -1 AND NOT bWalkingIntoSimpleInterior)
								bLaunchShopScript = TRUE
							ENDIF
						BREAK
						CASE GUN_SHOP_ARMORY_ARENA
							IF (g_iGunModInstance != -1 AND NOT bWalkingIntoSimpleInterior)
								bLaunchShopScript = TRUE
							ENDIF
						BREAK
						CASE HAIRDO_SHOP_CASINO_APT
							IF (g_iCasinoAptHairdoShopInstance != -1 AND NOT bWalkingIntoSimpleInterior)
								bLaunchShopScript = TRUE
							ENDIF
						BREAK
						CASE CLOTHES_SHOP_CASINO
							IF (g_iClothesShopInstance != -1 AND NOT bWalkingIntoSimpleInterior)
								bLaunchShopScript = TRUE
							ENDIF
						BREAK
						CASE GUN_SHOP_ARMORY_ARCADE
							IF (g_iGunModInstance != -1 AND NOT bWalkingIntoSimpleInterior)
								bLaunchShopScript = TRUE
							ENDIF
						BREAK
						CASE GUN_SHOP_ARMORY_SUB
							IF (g_iGunModInstance != -1 AND NOT bWalkingIntoSimpleInterior)
								bLaunchShopScript = TRUE
							ENDIF
						BREAK
						#IF FEATURE_TUNER
						CASE TATTOO_PARLOUR_07_CCT
							IF (g_iTattooShopInstance != -1 AND NOT bPrivateCarMeet AND NOT bWalkingIntoSimpleInterior AND g_bCarMeetInteriorLoaded)
								bLaunchShopScript = TRUE
							ENDIF
						BREAK
						CASE CLOTHES_SHOP_CAR_MEET
							IF (g_iClothesShopInstance != -1 AND NOT bPrivateCarMeet AND NOT bWalkingIntoSimpleInterior AND g_bCarMeetInteriorLoaded)
								bLaunchShopScript = TRUE
							ENDIF
						BREAK
						#ENDIF
						#IF FEATURE_FIXER
						CASE GUN_SHOP_ARMORY_FIXER
							IF (g_iGunModInstance != -1 AND NOT bWalkingIntoSimpleInterior)
								bLaunchShopScript = TRUE
							ENDIF
						BREAK
						CASE CLOTHES_SHOP_MUSIC_STUDIO
							IF (g_iClothesShopInstance != -1 AND NOT bWalkingIntoSimpleInterior)
								bLaunchShopScript = TRUE
							ENDIF
						BREAK
						#ENDIF
						DEFAULT
							bLaunchShopScript = TRUE
						BREAK
					ENDSWITCH
					
					IF (bLaunchShopScript)
						
						IF  NOT IS_SHOP_SCRIPT_RUNNING(sShopScript,eShop)
						AND ( (iThreadsRequires = 1) OR (NOT IS_SHOP_SCRIPT_RUNNING(sLinkedShopScript,eShop)) )
						
							// Clear any flags before we launch...
							CLEAR_SHOP_BIT(eShop, SHOP_NS_FLAG_FORCE_CLEANUP, TRUE)
							CLEAR_SHOP_BIT(eShop, SHOP_NS_FLAG_CLEAR_SHOP_LAUNCHED_FLAG, TRUE)
							
							IF NETWORK_IS_GAME_IN_PROGRESS()
								IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
									INT iStatUpdate = GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_SHOP(eShop))
									UPDATE_MISC_SHOP_FLAGS(eShop, iStatUpdate)
									SET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_SHOP(eShop), iStatUpdate)
								ENDIF
							ELSE
								UPDATE_MISC_SHOP_FLAGS(eShop, g_savedGlobals.sShopData.iProperties[eShop])
							ENDIF
													
							SHOP_LAUNCHER_STRUCT sShopLauncherData
							sShopLauncherData.bLinkedShop = FALSE
							sShopLauncherData.eShop = eShop	
							sShopLauncherData.iNetInstanceID = GET_SHOP_INSTANCE_ID(eShop)
							START_NEW_SCRIPT_WITH_ARGS(sShopScript, sShopLauncherData, SIZE_OF(sShopLauncherData), GET_STACK_SIZE_FOR_SHOP_SCRIPT(sShopScript))
							
							IF iThreadsRequires = 2
								sShopLauncherData.eShop = eShop
								sShopLauncherData.bLinkedShop = TRUE
								sShopLauncherData.iNetInstanceID = GET_SHOP_INSTANCE_ID(eShop)
								START_NEW_SCRIPT_WITH_ARGS(sLinkedShopScript, sShopLauncherData, SIZE_OF(sShopLauncherData), GET_STACK_SIZE_FOR_SHOP_SCRIPT(sLinkedShopScript))
							ENDIF
							
							PRINTLN("shop_controller: Setting launched flag for shop '", GET_SHOP_NAME(eShop), "', MP=", NETWORK_IS_GAME_IN_PROGRESS())
							g_sShopSettings.bShopScriptLaunched[eShop] = TRUE
							g_sShopSettings.bShopScriptLaunchedInMP[eShop] = NETWORK_IS_GAME_IN_PROGRESS()
							
							SET_SCRIPT_AS_NO_LONGER_NEEDED(sShopScript)
							
							IF iThreadsRequires = 2
								SET_SCRIPT_AS_NO_LONGER_NEEDED(sLinkedShopScript)
							ENDIF
							
							g_sShopSettings.bShopScriptRequested[eShop] = FALSE
						ELSE
							#IF IS_DEBUG_BUILD
								PRINTLN("shop_controller: Unable to start shop script for '", GET_SHOP_NAME(eShop), "' - last instance still active")
							#ENDIF
						ENDIF
					ELSE
						#IF IS_DEBUG_BUILD
						IF GET_COMMANDLINE_PARAM_EXISTS("sc_ShopControllerDebugPrints")
							PRINTLN("shop_controller: Unable to start shop script for '", GET_SHOP_NAME(eShop), "'")
							PRINTLN("...eShop = ", eShop)
							PRINTLN("...SHOP_STACK_SIZE required = ", iStackCount[0], ", free = ", GET_NUMBER_OF_FREE_STACKS_OF_THIS_SIZE(SHOP_STACK_SIZE))
							PRINTLN("...CAR_MOD_SHOP_STACK_SIZE required = ", iStackCount[1], ", free = ", GET_NUMBER_OF_FREE_STACKS_OF_THIS_SIZE(CAR_MOD_SHOP_STACK_SIZE))
							
							PRINTLN("...IS_TRANSITION_ACTIVE() = ", IS_TRANSITION_ACTIVE())
							PRINTLN("...IS_PLAYER_SWITCH_IN_PROGRESS() = ", IS_PLAYER_SWITCH_IN_PROGRESS())
							PRINTLN("...PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY = ", IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iBS,PROPERTY_BROADCAST_BS_SPAWNING_IN_A_PROPERTY))
							PRINTLN("...GET_SPAWN_ACTIVITY() = ", ENUM_TO_INT(GET_SPAWN_ACTIVITY()), " Expected: ", ENUM_TO_INT(SPAWN_ACTIVITY_NOTHING))
							PRINTLN("...g_iGunModInstance = ", g_iGunModInstance)
							PRINTLN("...g_iCasinoAptHairdoShopInstance = ", g_iCasinoAptHairdoShopInstance)
							PRINTLN("...g_iClothesShopInstance = ", g_iClothesShopInstance)
							PRINTLN("...g_iTattooShopInstance = ", g_iTattooShopInstance)
							PRINTLN("...bWalkingIntoSimpleInterior = ", bWalkingIntoSimpleInterior)
						ENDIF
						#ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Checks if the casino apartments master bedrooms wardrobe needs launching
///    and sets up the launcher data with the position and heading 
FUNC BOOL DOES_CASINO_APARTMENT_MASTER_BEDROOM_WARDROBE_NEED_LAUNCHING()
	IF g_sMPTunables.bVC_PENTHOUSE_DISABLE_MASTER_WARDROBE
		RETURN FALSE
	ENDIF	
	
	sWardrobeLauncherData[2].vCoords = <<976.4120, 64.7793, 116.9141>>
	sWardrobeLauncherData[2].fHeading = 53.0579
	
	IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[2].vCoords, <<0.0, 0.0, 0.0>>)
	AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[2].vCoords) < 2.5
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Checks if the casino apartments guest bedrooms wardrobe needs launching
///    and sets up the launcher data with the position and heading 
FUNC BOOL DOES_CASINO_APARTMENT_GEUST_BEDROOM_WARDROBE_NEED_LAUNCHING()
	IF g_sMPTunables.bVC_PENTHOUSE_DISABLE_GUEST_WARDROBE
		RETURN FALSE
	ENDIF	
	
	IF NOT HAS_PLAYER_PURCHASED_CASINO_APARTMENT_BEDROOM(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
		RETURN FALSE	
	ENDIF
	
	sWardrobeLauncherData[2].vCoords = <<984.6170, 60.0772, 116.9142>> 
	sWardrobeLauncherData[2].fHeading = 288.4977
		
	IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[2].vCoords, <<0.0, 0.0, 0.0>>)
	AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[2].vCoords) < 2.5
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///     Checks if any wardrobes in the casino apartment need launching
///     and makes sure the correct one launches
FUNC BOOL DOES_A_CASINO_APARTMENT_WARDROBE_NEED_LAUNCHING()
	IF g_sMPTunables.bVC_PENTHOUSE_DISABLE_WARDROBE
		RETURN FALSE
	ENDIF	
	
	IF DOES_CASINO_APARTMENT_MASTER_BEDROOM_WARDROBE_NEED_LAUNCHING()
		RETURN TRUE
	ENDIF
	
	RETURN DOES_CASINO_APARTMENT_GEUST_BEDROOM_WARDROBE_NEED_LAUNCHING()
ENDFUNC

PROC PROCESS_MP_WARDROBE_LAUNCHER()
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("wardrobe_mp")) = 0
		
			INT iLaunchType = -1
		
			// CLOTHES SHOPS
			IF IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CLOTHES)
				IF eWardrobeShop != GET_SHOP_PLAYER_LAST_USED()
					eWardrobeShop = GET_SHOP_PLAYER_LAST_USED()
					
					SWITCH eWardrobeShop
						CASE CLOTHES_SHOP_L_01_SC
						CASE CLOTHES_SHOP_L_02_GS
						CASE CLOTHES_SHOP_L_03_DT
						CASE CLOTHES_SHOP_L_04_CS
						CASE CLOTHES_SHOP_L_05_GSD
						CASE CLOTHES_SHOP_L_06_VC
						CASE CLOTHES_SHOP_L_07_PB
							sWardrobeLauncherData[0].vCoords = <<71.2800, -1387.9392, 28.3761>>
							sWardrobeLauncherData[0].vCoords += <<0,0,0.9948>>
							sWardrobeLauncherData[0].fHeading = 186.8302
							ALIGN_SHOP_COORD(CLOTHES_SHOP_L_01_SC, eWardrobeShop, sWardrobeLauncherData[0].vCoords)
							ALIGN_SHOP_HEADING(CLOTHES_SHOP_L_01_SC, eWardrobeShop, sWardrobeLauncherData[0].fHeading)
						BREAK
						CASE CLOTHES_SHOP_CASINO
							IF !NETWORK_IS_ACTIVITY_SESSION()
								sWardrobeLauncherData[0].vCoords = <<1096.2449, 201.5049, -50.4402>>
								sWardrobeLauncherData[0].fHeading =  238.5800
						
								IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[0].vCoords, <<0.0, 0.0, 0.0>>)
								AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[0].vCoords) < 7.5
									iLaunchType = 2
								ENDIF
							ENDIF
						BREAK
						DEFAULT
							sWardrobeLauncherData[0].vCoords = <<0,0,0>>
						BREAK
					ENDSWITCH
				ENDIF
				
				IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[0].vCoords, <<0,0,0>>)
				AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[0].vCoords) < 2.5
					iLaunchType = 0
				ENDIF
				
			
			ELSE
				IF IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
					// APARTMENTS
					IF GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty > 0
						IF iWardrobeProperty != GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
						OR iWardrobeYacht != GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtIAmOn
							iWardrobeProperty = GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.iCurrentlyInsideProperty
							iWardrobeYacht = GlobalplayerBD[NATIVE_TO_INT(PLAYER_ID())].PrivateYachtDetails.iYachtIAmOn
							GET_PROPERTY_WARDROBE_LOCATION(iWardrobeProperty, sWardrobeLauncherData[1].vCoords, sWardrobeLauncherData[1].fHeading  ,iWardrobeYacht )
							sWardrobeLauncherData[1].vCoords += <<0,0,0.9948>>
							CPRINTLN(DEBUG_SCRIPT_LAUNCH, "[WARD] Player is now in property ", iWardrobeProperty)
						ENDIF
						IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[1].vCoords, <<0,0,0>>)
						AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[1].vCoords) < 7.5
							iLaunchType = 1
						ENDIF
					
					// WAREHOUSE
					ELIF IS_PLAYER_IN_IE_GARAGE(PLAYER_ID())
						IF iIEWarehouse != ENUM_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior)
							iIEWarehouse = ENUM_TO_INT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior)
							sWardrobeLauncherData[2].vCoords = <<958.9901, -3005.0342, -39.6430>>//-40.6349>>
							sWardrobeLauncherData[2].fHeading = 263.0107
						ENDIF
						
						IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[2].vCoords, <<0,0,0>>)
						AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[2].vCoords) < 7.5
							iLaunchType = 2
						ENDIF
					
					ELIF IS_PLAYER_IN_ARMORY_TRUCK(PLAYER_ID())
						IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
						AND NOT IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
							IF g_ownerOfArmoryTruckPropertyIAmIn = PLAYER_ID()
								ARMORY_TRUCK_SECTIONS_ENUM iSection
								
								IF GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, g_ownerOfArmoryTruckPropertyIAmIn) = AT_ST_COMMAND_CENTER
								OR GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_FIRST_SECTION, g_ownerOfArmoryTruckPropertyIAmIn) = AT_ST_LIVING_ROOM
								OR GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_SECOND_SECTION, g_ownerOfArmoryTruckPropertyIAmIn) = AT_ST_LIVING_ROOM
								OR GET_PLAYER_TRUCK_PURCHASED_SECTIONS(ATS_THIRD_SECTION, g_ownerOfArmoryTruckPropertyIAmIn) = AT_ST_LIVING_ROOM
									MP_PROP_OFFSET_STRUCT tempOffset
									GET_ARMORY_TRUCK_PROP_TRANSFORM(ATP_SECTION_WARDROBE, iSection, tempOffset)
									
									sWardrobeLauncherData[2].vCoords = tempOffset.vLoc
									PRINTLN("GET_ARMORY_TRUCK_PROP_TRANSFORM - tempOffset.vLoc: ", tempOffset.vLoc)
									sWardrobeLauncherData[2].fHeading = 270.0
									
									IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[2].vCoords, <<0,0,0>>)
										iLaunchType = 2
									ENDIF
								ENDIF
							ENDIF
						ENDIF
						
					ELIF IS_PLAYER_IN_BUNKER(PLAYER_ID())
						IF g_ownerOfBunkerPropertyIAmIn != INVALID_PLAYER_INDEX()
							IF g_ownerOfBunkerPropertyIAmIn = PLAYER_ID()
								sWardrobeLauncherData[2].vCoords = <<903.4413, -3199.4375, -98.1878>>
								sWardrobeLauncherData[2].fHeading = 227.9133 
								IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[2].vCoords, <<0,0,0>>)
								AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[2].vCoords) < 7.5
									iLaunchType = 2
								ENDIF
							ENDIF
						ENDIF
					
					ELIF IS_PLAYER_IN_HANGAR(PLAYER_ID())
						IF g_OwnerOfHangarPropertyIAmIn != INVALID_PLAYER_INDEX()
						AND (IS_HANGAR_SAVEBED_TRADITIONAL_PURCHASED() OR IS_HANGAR_SAVEBED_MODERN_PURCHASED())
							IF g_OwnerOfHangarPropertyIAmIn = PLAYER_ID()
								sWardrobeLauncherData[2].vCoords = <<-1234.5508, -2981.0730, -42.2636>>
								sWardrobeLauncherData[2].fHeading = 180
								IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[2].vCoords, <<0,0,0>>)
								AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[2].vCoords) < 7.5
									iLaunchType = 2
								ENDIF
							ENDIF
						ENDIF
					
					ELIF IS_PLAYER_IN_DEFUNCT_BASE(PLAYER_ID())
						IF g_ownerOfBasePropertyIAmIn != INVALID_PLAYER_INDEX()
						AND (IS_DEFUNCT_BASE_PERSONAL_QUARTERS_3_PURCHASED() OR IS_DEFUNCT_BASE_PERSONAL_QUARTERS_2_PURCHASED() OR IS_DEFUNCT_BASE_PERSONAL_QUARTERS_1_PURCHASED())
							IF g_OwnerOfBasePropertyIAmIn= PLAYER_ID()
								sWardrobeLauncherData[2].vCoords = <<371.1484, 4819.4702, -59.9884>>
								sWardrobeLauncherData[2].fHeading = 0
								IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[2].vCoords, <<0,0,0>>)
								AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[2].vCoords) < 7.5
									iLaunchType = 2
								ELSE
									FLOAT iDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[2].vCoords)
									CDEBUG1LN(DEBUG_NET_ACTIVITY_CREATOR, "PROCESS_MP_WARDROBE_LAUNCHER: iDistance from coord = ", iDistance)
								ENDIF
							ENDIF
						ENDIF
					
					ELIF IS_PLAYER_IN_ARMORY_AIRCRAFT(PLAYER_ID())
						IF g_OwnerOfArmoryAircraftPropertyIAmIn = PLAYER_ID()
							sWardrobeLauncherData[2].vCoords = <<509.317108,4749.101563,-69.996033>>
							sWardrobeLauncherData[2].fHeading = 0
							IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[2].vCoords, <<0,0,0>>)
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[2].vCoords) < 7.5
								iLaunchType = 2
							ENDIF
						ENDIF
					
					ELIF IS_PLAYER_IN_HACKER_TRUCK(PLAYER_ID())
						IF g_OwnerOfHackerTruckPropertyIAmIn = PLAYER_ID()
							sWardrobeLauncherData[2].vCoords = <<-1419.9304, -3014.3147, -79.9999>>
							sWardrobeLauncherData[2].fHeading = 0
							IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[2].vCoords, <<0,0,0>>)
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[2].vCoords) < 7.5
								iLaunchType = 2
							ENDIF
						ENDIF
					
					ELIF IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
						IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID()
							sWardrobeLauncherData[2].vCoords = <<-1619.6835, -3020.2756, -76.2050>>
							sWardrobeLauncherData[2].fHeading = 0
							IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[2].vCoords, <<0,0,0>>)
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[2].vCoords) < 7.5
								iLaunchType = 2
							ENDIF
						ENDIF
					
					ELIF IS_PLAYER_IN_ARENA_GARAGE(PLAYER_ID())
						IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID()
							sWardrobeLauncherData[2].vCoords = <<210.9503, 5162.0801, -90.1981>>
							sWardrobeLauncherData[2].fHeading = 90
							
							IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[2].vCoords, <<0.0, 0.0, 0.0>>)
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[2].vCoords) < 7.5
								iLaunchType = 2
							ENDIF
						ENDIF
					
					ELIF IS_PLAYER_IN_CASINO_APARTMENT(PLAYER_ID())
						IF DOES_CASINO_APARTMENT_GEUST_BEDROOM_WARDROBE_NEED_LAUNCHING()
							iLaunchType = 2
						
						ELIF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner  = PLAYER_ID()
						AND DOES_A_CASINO_APARTMENT_WARDROBE_NEED_LAUNCHING()
							iLaunchType = 2
						ENDIF
					
					ELIF IS_PLAYER_IN_CASINO(PLAYER_ID())
						IF NOT NETWORK_IS_ACTIVITY_SESSION()
							sWardrobeLauncherData[2].vCoords = <<1096.2449, 201.5049, -50.4402>>
							sWardrobeLauncherData[2].fHeading =  238.5800
							
							IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[2].vCoords, <<0.0, 0.0, 0.0>>)
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[2].vCoords) < 7.5
								iLaunchType = 2
							ENDIF
						ENDIF
					
					ELIF IS_PLAYER_IN_ARCADE_PROPERTY(PLAYER_ID())
						IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID()
						AND HAS_PLAYER_COMPLETED_ARCADE_PROPERTY_CABINETS_SETUP(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
						AND HAS_PLAYER_PURCHASED_ARCADE_PERSONAL_QUARTERS(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner)
							sWardrobeLauncherData[2].vCoords = <<2721.5591, -371.3648, -56.3809>>
							sWardrobeLauncherData[2].fHeading = 0.0
							IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[2].vCoords, <<0,0,0>>)
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[2].vCoords) < 7.5
								iLaunchType = 2
							ENDIF
						ENDIF
					
					ELIF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
						IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID()
						AND HAS_PLAYER_PURCHASED_AUTO_SHOP_PERSONAL_QUARTERS(PLAYER_ID())
						AND HAS_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION(PLAYER_ID())
							sWardrobeLauncherData[2].vCoords = <<-1358.1615, 145.0377, -95.1259>>
							sWardrobeLauncherData[2].fHeading = 26.3923
							
							IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[2].vCoords, <<0.0, 0.0, 0.0>>)
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[2].vCoords) < 7.5
								iLaunchType = 2
							ENDIF
						ENDIF
					
					#IF FEATURE_FIXER
					ELIF IS_PLAYER_IN_FIXER_HQ(PLAYER_ID())
						IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID()
						AND HAS_PLAYER_PURCHASED_FIXER_HQ_UPGRADE_PERSONAL_QUARTERS(PLAYER_ID())
							SIMPLE_INTERIORS eSimpleInterior = GET_SIMPLE_INTERIOR_PLAYER_IS_IN(PLAYER_ID())
						
							sWardrobeLauncherData[2].vCoords = TRANSFORM_SIMPLE_INTERIOR_COORDS_TO_WORLD_COORDS_GIVEN_ID(<< 7.22816, 10.8279, 8.6 >>, eSimpleInterior) // Original value at Hawick location <<392.5167, -71.2226, 110.9630>>
							sWardrobeLauncherData[2].fHeading = TRANSFORM_SIMPLE_INTERIOR_HEADING_TO_WORLD_HEADING_GIVEN_ID(80.000000, eSimpleInterior)	// Original value at Hawick location -30.0
																				
							IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[2].vCoords, <<0.0, 0.0, 0.0>>)
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[2].vCoords) < 7.5
								iLaunchType = 2
							ENDIF
						ENDIF
					#ENDIF
					
					ELIF IS_PLAYER_IN_SUBMARINE(PLAYER_ID())
						IF GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner = PLAYER_ID()
						OR (GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE) AND GB_ARE_PLAYERS_IN_SAME_GANG(PLAYER_ID(), GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.propertyOwner))
							sWardrobeLauncherData[2].vCoords = <<1557.1438, 380.7705, -53.0888>>
							sWardrobeLauncherData[2].fHeading = 334.9014
							IF NOT ARE_VECTORS_EQUAL(sWardrobeLauncherData[2].vCoords, <<0,0,0>>)
							AND GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), sWardrobeLauncherData[2].vCoords) < 7.5
								iLaunchType = 2
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			IF iLaunchType != -1
				REQUEST_SCRIPT("wardrobe_mp")
				IF HAS_SCRIPT_LOADED("wardrobe_mp")
					IF GET_NUMBER_OF_FREE_STACKS_OF_THIS_SIZE(SHOP_STACK_SIZE) > 1
						CPRINTLN(DEBUG_SCRIPT_LAUNCH, "[WARD] Launching Freemode wardrobe script for property ", iWardrobeProperty)
						CDEBUG1LN(DEBUG_SAFEHOUSE, "launch wardrobe script")
						sWardrobeLauncherData[iLaunchType].eWardrobe = PW_FREEMODE
						START_NEW_SCRIPT_WITH_ARGS("wardrobe_mp", sWardrobeLauncherData[iLaunchType], SIZE_OF(sWardrobeLauncherData[iLaunchType]), SHOP_STACK_SIZE)
						SET_SCRIPT_AS_NO_LONGER_NEEDED("wardrobe_mp")
					ELSE
						CPRINTLN(DEBUG_SCRIPT_LAUNCH, "[WARD] Launching Freemode wardrobe script - no free stacks")
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC



/// PURPOSE: Checks to see if the shop controller should be re-initialised
PROC CHECK_CONTROLLER_RESET()
	
	IF eStage != SC_STAGE_INIT
	AND eInit != NOT_INITIALISED
		// Reset if we have changed game modes
		IF eInit = INITIALISED_IN_SP
			IF NETWORK_IS_GAME_IN_PROGRESS()
				PRINTLN("...shop_controller.sc controller reset for SP.")
				eInit = NOT_INITIALISED
			ENDIF
		ELIF eInit = INITIALISED_IN_MP_FREEMODE
			IF (NOT NETWORK_IS_GAME_IN_PROGRESS() AND NOT IS_TRANSITION_SESSION_LAUNCHING() AND NOT IS_TRANSITION_SESSION_KILLING_SESSION())
			OR NOT IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
			OR GET_CURRENT_GAMEMODE() != GAMEMODE_FM
				PRINTLN("...shop_controller.sc controller reset for MP.")
				eInit = NOT_INITIALISED
			ENDIF
		ENDIF
		
		// Jump back to the initialisation stage if we have switched game modes
		IF eInit = NOT_INITIALISED
		OR NOT g_sShopSettings.bControllerRunning
			PRINTLN("...shop_controller.sc controller reset forced.")
			eStage = SC_STAGE_RESET
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Resets all the runtime globals used to handle shop states
PROC RESET_SHOP_RUNTIME_FLAGS()
	// Reset the local flags
	bCarmodProcessInit = FALSE
	bUpdatedBlipsOnMission = FALSE
	bDisableMainUpdates = FALSE
	
	// Reset the global flags
	INT i
	REPEAT NUMBER_OF_SHOPS i
		g_sShopSettings.iProperties[i] = 0
		
		// Force blip updates
		SET_SHOP_BLIP_NEEDS_UPDATED(INT_TO_ENUM(SHOP_NAME_ENUM, i))
		
		// Allow the script to launch straight away
		PROCESS_SHOP_SCRIPT_LAUNCHED_CLEANUP(INT_TO_ENUM(SHOP_NAME_ENUM, i))
	ENDREPEAT
	
	g_sShopSettings.bDisplayingShopHelp = FALSE
	g_sShopSettings.iShopHelpFrameDelay = 0
	g_sShopSettings.bHideAllShopBlips = FALSE
	g_sShopSettings.bHideShopBlipsByType[0] = FALSE
	g_sShopSettings.bHideShopBlipsByType[1] = FALSE
	g_sShopSettings.bHideShopBlipsByType[2] = FALSE
	g_sShopSettings.bHideShopBlipsByType[3] = FALSE
	g_sShopSettings.bHideShopBlipsByType[4] = FALSE
	
	g_sShopSettings.iCount_PlayerInShop = 0
	g_sShopSettings.iCount_BrowsingInShop = 0
	
	g_sShopSettings.bAssignPVBackOnCleanup = TRUE
ENDPROC

/// PURPOSE: Sets up each shop blip
PROC INITIALISE_SHOP_BLIPS()
	
	INT i
	STATIC_BLIP_NAME_ENUM eBlip
	BLIP_SPRITE eSprite
	
	FOR i = 0 TO NUMBER_OF_SHOPS-1
	
		IF DOES_BLIP_EXIST(g_sShopSettings.blipID[i])
			REMOVE_BLIP(g_sShopSettings.blipID[i])
		ENDIF
	
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			// Only initialise SP blips
			IF IS_SHOP_AVAILABLE_IN_SINGLEPLAYER((INT_TO_ENUM(SHOP_NAME_ENUM, i)))	
				eBlip = GET_SHOP_BLIP_ENUM((INT_TO_ENUM(SHOP_NAME_ENUM, i)))
				eSprite = GET_SHOP_BLIP_SPRITE((INT_TO_ENUM(SHOP_NAME_ENUM, i)))
				
				SET_STATIC_BLIP_ICON(eBlip, eSprite)																			/* Sprite		*/
				SET_STATIC_BLIP_NAME(eBlip, GET_SHOP_BLIP_NAME((INT_TO_ENUM(SHOP_NAME_ENUM, i))))								/* Name			*/
				//SET_STATIC_BLIP_FLASH_ON_ACTIVE(eBlip, SHOULD_SHOP_BLIP_FLASH_WHEN_ACTIVATED(INT_TO_ENUM(SHOP_NAME_ENUM, i)))	/* Flash		*/
				SET_STATIC_BLIP_APPEAR_ON_MAP(eBlip, TRUE)																		/* Map			*/
				SET_STATIC_BLIP_APPEAR_IN_RADAR(eBlip, TRUE)																	/* Radar		*/
				SET_STATIC_BLIP_APPEAR_EDGE_RADAR(eBlip, IS_SHOP_BLIP_LONG_RANGE(INT_TO_ENUM(SHOP_NAME_ENUM, i)))				/* Rader edge	*/
				//SET_STATIC_BLIP_HIDDEN_IN_MISSION(eBlip, FALSE)																	/* Mission		*/
				//SET_STATIC_BLIP_IS_DISCOVERABLE(eBlip, FALSE)																	/* Discoverable */
				SET_STATIC_BLIP_MISSION_LAUNCH_LEVEL_LOCKED(eBlip,MISSION_TYPE_SHOP_BLIPS_AVAILABILITY)
				SET_STATIC_BLIP_CATEGORY(eBlip, STATIC_BLIP_CATEGORY_SHOP)														/* Category		*/
				SET_STATIC_BLIP_POSITION(eBlip, GET_SHOP_COORDS(INT_TO_ENUM(SHOP_NAME_ENUM, i))) 								/* Position		*/
				SET_STATIC_BLIP_ACTIVE_STATE(eBlip, IS_BIT_SET(g_savedGlobals.sShopData.iProperties[INT_TO_ENUM(SHOP_NAME_ENUM, i)], SHOP_S_FLAG_SHOP_AVAILABLE))/* Active 		*/
				SET_STATIC_BLIP_SHOWS_MAP_HOVER_INFO(eBlip, TRUE)
				
			ENDIF
		ENDIF
	ENDFOR
	
//	DEBLIP_MY_PAY_N_SPRAY()
ENDPROC


PROC DO_ENTITY_SET_UPDATE(INT iBit, SHOP_NAME_ENUM eShop, STRING entitySetNameDeactivate, STRING entitySetNameActivate)
	IF NOT IS_BIT_SET(iShopEntitySetsInitialised, iBit)
		INTERIOR_INSTANCE_INDEX interiorID = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_SHOP_COORDS(eShop), GET_SHOP_INTERIOR_TYPE(eShop))
		IF interiorID != NULL
			IF GET_HASH_KEY(entitySetNameDeactivate) != 0
				IF IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, entitySetNameDeactivate)
					DEACTIVATE_INTERIOR_ENTITY_SET(interiorID, entitySetNameDeactivate)
				ENDIF
			ENDIF
			IF GET_HASH_KEY(entitySetNameActivate) != 0
				IF NOT IS_INTERIOR_ENTITY_SET_ACTIVE(interiorID, entitySetNameActivate)
					ACTIVATE_INTERIOR_ENTITY_SET(interiorID, entitySetNameActivate)
				ENDIF
			ENDIF
			SET_BIT(iShopEntitySetsInitialised, iBit)
		ELSE
			bInitialisedShopEntitySets = FALSE
		ENDIF
	ENDIF	
ENDPROC

PROC INITIALISE_SHOP_ENTITY_SETS()

	IF NOT bInitialisedShopEntitySets
		bInitialisedShopEntitySets = TRUE
		DO_ENTITY_SET_UPDATE(0, CLOTHES_SHOP_L_01_SC, "ClothesLowCHEAP", "ClothesLowHipster")
		DO_ENTITY_SET_UPDATE(1, CLOTHES_SHOP_L_02_GS, "ClothesLowHipster", "ClothesLowCHEAP")
		DO_ENTITY_SET_UPDATE(2, CLOTHES_SHOP_L_03_DT, "ClothesLowHipster", "ClothesLowCHEAP")
		DO_ENTITY_SET_UPDATE(3, CLOTHES_SHOP_L_04_CS, "ClothesLowHipster", "ClothesLowCHEAP")
		DO_ENTITY_SET_UPDATE(4, CLOTHES_SHOP_L_05_GSD, "ClothesLowHipster", "ClothesLowCHEAP")
		DO_ENTITY_SET_UPDATE(5, CLOTHES_SHOP_L_06_VC, "ClothesLowHipster", "ClothesLowCHEAP")
		DO_ENTITY_SET_UPDATE(6, CLOTHES_SHOP_L_07_PB, "ClothesLowHipster", "ClothesLowCHEAP")
		
		DO_ENTITY_SET_UPDATE(7, GUN_SHOP_01_DT, "", "GunClubWallHooks")
		DO_ENTITY_SET_UPDATE(8, GUN_SHOP_02_SS, "", "GunStoreHooks")
		DO_ENTITY_SET_UPDATE(9, GUN_SHOP_03_HW, "", "GunStoreHooks")
		DO_ENTITY_SET_UPDATE(9, GUN_SHOP_04_ELS, "", "GunStoreHooks")
		DO_ENTITY_SET_UPDATE(10, GUN_SHOP_05_PB, "", "GunStoreHooks")
		DO_ENTITY_SET_UPDATE(11, GUN_SHOP_06_LS, "", "GunStoreHooks")
		DO_ENTITY_SET_UPDATE(12, GUN_SHOP_07_MW, "", "GunStoreHooks")
		DO_ENTITY_SET_UPDATE(13, GUN_SHOP_08_CS, "", "GunStoreHooks")
		DO_ENTITY_SET_UPDATE(14, GUN_SHOP_09_GOH, "", "GunStoreHooks")
		DO_ENTITY_SET_UPDATE(15, GUN_SHOP_10_VWH, "", "GunStoreHooks")
		DO_ENTITY_SET_UPDATE(16, GUN_SHOP_11_ID1, "", "GunClubWallHooks")
		
		IF bInitialisedShopEntitySets
			PRINTLN("shop_controller: Shop entity sets initialised")
		ELSE
			// Waiting too long?
			IF (NETWORK_IS_GAME_IN_PROGRESS() AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(tdShopEntitySetsTimer, 30000)))
			OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - iShopEntitySetsTimer) > 30000)
				
				PRINTLN("INITIALISE_SHOP_ENTITY_SETS() has taken more than 30 seconds. Pass logs to Kenneth R.")
				INT iBit
				REPEAT 32 iBit
					PRINTLN("...bit[", iBit, "] = ", IS_BIT_SET(iShopEntitySetsInitialised, iBit))
				ENDREPEAT
			
				#IF IS_DEBUG_BUILD
					SCRIPT_ASSERT("INITIALISE_SHOP_ENTITY_SETS() has taken more than 30 seconds. Pass logs to Kenneth R.")
				#ENDIF
				bInitialisedShopEntitySets = TRUE
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Ensure all the correct shops are set to available
///    NOTE: This is a fix for bug 1689124 - shops broken after visiting GTA Online
PROC PATCH_SP_SHOP_STATES()

	IF NETWORK_IS_GAME_IN_PROGRESS()
	OR IS_REPEAT_PLAY_ACTIVE()
		EXIT
	ENDIF

	PRINTSTRING("\n shop_controller: start PATCH_SP_SHOP_AVAILABLE_STATE")PRINTNL()
	
	// Clothes
	// - Mask shop - Before FBI4 prep 4.
	IF IS_MISSION_AVAILABLE(SP_MISSION_FBI_4_PREP_4)
	OR GET_MISSION_COMPLETE_STATE(SP_MISSION_FBI_4_PREP_4)
		SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_A_01_VB, TRUE)
	ENDIF
	// - Clothes shop - End of Lester 1
	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_LESTER_1)
		SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_L_01_SC, TRUE)
		SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_L_02_GS, TRUE)
		SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_L_03_DT, TRUE)
		SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_L_04_CS, TRUE)
		SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_L_05_GSD, TRUE)
		SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_L_06_VC, TRUE)
		SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_L_07_PB, TRUE)
		SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_M_01_SM, TRUE)
		SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_M_03_H, TRUE)
		SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_M_04_HW, TRUE)
		SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_M_05_GOH, TRUE)
		SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_H_01_BH, TRUE)
		SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_H_02_B, TRUE)
		SET_SHOP_IS_AVAILABLE(CLOTHES_SHOP_H_03_MW, TRUE)
	ENDIF
	
	// Tattoos
	// - End of Trevor1
	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_TREVOR_1)
		SET_SHOP_IS_AVAILABLE(TATTOO_PARLOUR_01_HW, TRUE)
		SET_SHOP_IS_AVAILABLE(TATTOO_PARLOUR_02_SS, TRUE)
		SET_SHOP_IS_AVAILABLE(TATTOO_PARLOUR_03_PB, TRUE)
		SET_SHOP_IS_AVAILABLE(TATTOO_PARLOUR_04_VC, TRUE)
		SET_SHOP_IS_AVAILABLE(TATTOO_PARLOUR_05_ELS, TRUE)
		SET_SHOP_IS_AVAILABLE(TATTOO_PARLOUR_06_GOH, TRUE)
	ENDIF
	
	// Mod shop
	// Hairdo
	// Weapons
	// - End of Armenian1
	IF GET_MISSION_COMPLETE_STATE(SP_MISSION_ARMENIAN_1)
		SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_01_AP, TRUE)
		SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_05_ID2, TRUE)
		SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_06_BT1, TRUE)
		SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_07_CS1, TRUE)
		SET_SHOP_IS_AVAILABLE(CARMOD_SHOP_08_CS6, TRUE)
		
		SET_SHOP_IS_AVAILABLE(HAIRDO_SHOP_01_BH, TRUE)
		SET_SHOP_IS_AVAILABLE(HAIRDO_SHOP_02_SC, TRUE)
		SET_SHOP_IS_AVAILABLE(HAIRDO_SHOP_03_V, TRUE)
		SET_SHOP_IS_AVAILABLE(HAIRDO_SHOP_04_SS, TRUE)
		SET_SHOP_IS_AVAILABLE(HAIRDO_SHOP_05_MP, TRUE)
		SET_SHOP_IS_AVAILABLE(HAIRDO_SHOP_06_HW, TRUE)
		SET_SHOP_IS_AVAILABLE(HAIRDO_SHOP_07_PB, TRUE)
		
		SET_SHOP_IS_AVAILABLE(GUN_SHOP_01_DT, TRUE)
		SET_SHOP_IS_AVAILABLE(GUN_SHOP_02_SS, TRUE)
		SET_SHOP_IS_AVAILABLE(GUN_SHOP_03_HW, TRUE)
		SET_SHOP_IS_AVAILABLE(GUN_SHOP_04_ELS, TRUE)
		SET_SHOP_IS_AVAILABLE(GUN_SHOP_05_PB, TRUE)
		SET_SHOP_IS_AVAILABLE(GUN_SHOP_06_LS, TRUE)
		SET_SHOP_IS_AVAILABLE(GUN_SHOP_07_MW, TRUE)
		SET_SHOP_IS_AVAILABLE(GUN_SHOP_08_CS, TRUE)
		SET_SHOP_IS_AVAILABLE(GUN_SHOP_09_GOH, TRUE)
		SET_SHOP_IS_AVAILABLE(GUN_SHOP_10_VWH, TRUE)
		SET_SHOP_IS_AVAILABLE(GUN_SHOP_11_ID1, TRUE)
	ENDIF
	
	// Fix for 1995161 - Data lookup was wrong for some clothing items so we need to patch
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_JBIB, JBIB_P0_5_4, TRUE)
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_JBIB, JBIB_P0_5_5, TRUE)
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_JBIB, JBIB_P0_5_6, TRUE)
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_JBIB, JBIB_P0_5_7, TRUE)
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_JBIB, JBIB_P0_5_8, TRUE)
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_JBIB, JBIB_P0_5_9, TRUE)
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_JBIB, JBIB_P0_5_10, TRUE)
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_JBIB, JBIB_P0_5_11, TRUE)
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_JBIB, JBIB_P0_5_12, TRUE)
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_JBIB, JBIB_P0_5_13, TRUE)
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_JBIB, JBIB_P0_5_14, TRUE)
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_JBIB, JBIB_P0_5_15, TRUE)
	SET_PED_COMP_ITEM_AVAILABLE_SP(PLAYER_ZERO, COMP_TYPE_JBIB, JBIB_P0_BARE_CHEST, TRUE)
	
	PRINTSTRING("\n shop_controller: end PATCH_SP_SHOP_AVAILABLE_STATE")PRINTNL()
ENDPROC

PROC INITALISE_HELMET_VISOR()
	bHelmetVisorUp 	= DOES_PLAYER_PREFER_VISOR_UP()
	CPRINTLN(DEBUG_PED_COMP, "INITALISE_HELMET_VISOR - bHelmetVisorUp initalised to: ", bHelmetVisorUp)
	
	IF GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_HasHelmet)
		INT iItemHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(ANCHOR_HEAD), GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD), GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD))
		
		sCachedClothingStates.iPropHeadHashID = iItemHash
		sCachedClothingStates.iPropHeadVariantCount = GET_SHOP_PED_APPAREL_VARIANT_PROP_COUNT(iItemHash)
		
		IF sCachedClothingStates.iPropHeadVariantCount > 0
			
			IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iItemHash, DLC_RESTRICTION_TAG_ALT_HELMET, ENUM_TO_INT(SHOP_PED_PROP))
				IF bHelmetVisorUp = FALSE
					bHelmetVisorUp = TRUE
					
					CPRINTLN(DEBUG_AMBIENT, "[ShopController] INITALISE_HELMET_VISOR(): force helmet visor to UP based on the current helmet equipped")
				ENDIF
			ELSE
				IF bHelmetVisorUp = TRUE
					bHelmetVisorUp = FALSE
					
					CPRINTLN(DEBUG_AMBIENT, "[ShopController] INITALISE_HELMET_VISOR(): force helmet visor to DOWN based on the current helmet equipped")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF GET_MP_INT_CHARACTER_STAT(MP_STAT_AUTO_EQUIP_HELMET) = 0
		IF NOT GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableAutoEquipHelmetsInBikes)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableAutoEquipHelmetsInBikes, TRUE)
			CPRINTLN(DEBUG_AMBIENT, "[ShopController] INITALISE_HELMET_VISOR - AUTO HELMET - PCF_DisableAutoEquipHelmetsInBikes = TRUE")
		ENDIF
	ELSE
		IF GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableAutoEquipHelmetsInBikes)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableAutoEquipHelmetsInBikes, FALSE)
			CPRINTLN(DEBUG_AMBIENT, "[ShopController] INITALISE_HELMET_VISOR - AUTO HELMET - PCF_DisableAutoEquipHelmetsInBikes = FALSE")
		ENDIF
	ENDIF

	
ENDPROC

#IF FEATURE_GEN9_EXCLUSIVE
PROC INITALISE_GEN9_EXCLUSIVE_LIVERIES()
	IF g_sMPTunables.bENABLE_DEVESTE_MEMBER_LIVERY1
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DEVESTE_MEMBER_LIVERY1, TRUE)
	ENDIF
	
	IF g_sMPTunables.bENABLE_DEVESTE_MEMBER_LIVERY2
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_DEVESTE_MEMBER_LIVERY2, TRUE)
	ENDIF
	
	IF g_sMPTunables.bENABLE_BRIOSO_MEMBER_LIVERY1
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BRIOSO_MEMBER_LIVERY1, TRUE)
	ENDIF
	
	IF g_sMPTunables.bENABLE_BRIOSO_MEMBER_LIVERY2
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_BRIOSO_MEMBER_LIVERY2, TRUE)
	ENDIF
	
	#IF FEATURE_DLC_1_2022
	IF g_sMPTunables.bENABLE_VIGERO2_MEMBER_LIVERY1
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_VIGERO2_MEMBER_LIVERY1, TRUE)
	ENDIF
	
	IF g_sMPTunables.bENABLE_CORSITA_MEMBER_LIVERY1
		SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CORSITA_MEMBER_LIVERY1, TRUE)
	ENDIF
	#ENDIF
ENDPROC
#ENDIF

/// PURPOSE: Setup default shop data and re initialise flags
PROC DO_INITIALISE()

	IF GET_INDEX_OF_CURRENT_LEVEL() != LEVEL_GTA5
		#IF IS_DEBUG_BUILD
			IF g_bDisplayShopDebug
				PRINTSTRING("\n shop_controller: Init - not in LEVEL_GTA5.")PRINTNL()
			ENDIF
		#ENDIF
		EXIT
	ENDIF
	
	// Not required for the Creator
	IF GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR
		IF NOT g_sShopSettings.bRunControllerInCreator
			#IF IS_DEBUG_BUILD
				IF g_bDisplayShopDebug
					PRINTSTRING("\n shop_controller: Init - player is in Creator.")PRINTNL()
				ENDIF
			#ENDIF
			EXIT
		ENDIF
	ENDIF
	
	// Wait for MP player to be in main game
	IF NETWORK_IS_GAME_IN_PROGRESS()
	
		IF NOT HAS_IMPORTANT_STATS_LOADED()
			#IF IS_DEBUG_BUILD
				IF g_bDisplayShopDebug
					PRINTSTRING("\n shop_controller: Init - important stats not loaded.")PRINTNL()
				ENDIF
			#ENDIF
			EXIT
		ENDIF
	
		IF NOT IS_PLAYER_ACTIVE_IN_MP_LAUNCH_SCRIPT(PLAYER_ID())
			#IF IS_DEBUG_BUILD
				IF g_bDisplayShopDebug
					PRINTSTRING("\n shop_controller: Init - player not active in MP.")PRINTNL()
				ENDIF
			#ENDIF
			EXIT
		ENDIF
		
		IF GET_CURRENT_GAMEMODE() != GAMEMODE_FM
			#IF IS_DEBUG_BUILD
				IF g_bDisplayShopDebug
					PRINTSTRING("\n shop_controller: Init - player not in FM.")PRINTNL()
				ENDIF
			#ENDIF
			EXIT
		ENDIF
		
		INITALISE_HELMET_VISOR()
		
		#IF FEATURE_GEN9_EXCLUSIVE
		INITALISE_GEN9_EXCLUSIVE_LIVERIES()
		#ENDIF
	ENDIF
	
	
	// Wait for all shop script to terminate.
	INT iShop
	BOOL bForceCleanup = FALSE
	BOOL bForceRecover = FALSE
	
	IF bForceCleanupTimerSet
		IF (NOT NETWORK_IS_GAME_IN_PROGRESS() AND (GET_GAME_TIMER() - iForceCleanupTimer) > 10000)
		OR (NETWORK_IS_GAME_IN_PROGRESS() AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(tdForceCleanupTimer, 10000)))
			PRINTLN(" shop_controller: Init - Force reset taking too long to process so we need to recover!")
			bForceRecover = TRUE
		ENDIF
	ENDIF
	
	REPEAT NUMBER_OF_SHOPS iShop
		IF IS_BIT_SET(g_sShopSettings.iProperties[iShop], SHOP_NS_FLAG_SHOP_SCRIPT_RUNNING)
		
			IF bForceRecover
				#IF IS_DEBUG_BUILD
					PRINTSTRING("\n shop_controller: Init - Fixing shop ")PRINTSTRING(GET_SHOP_NAME(INT_TO_ENUM(SHOP_NAME_ENUM, iShop)))PRINTNL()
				#ENDIF
				
				CLEAR_BIT(g_sShopSettings.iProperties[iShop], SHOP_NS_FLAG_SHOP_SCRIPT_RUNNING)
				SET_PLAYER_IS_BROWSING_ITEMS_IN_SHOP(INT_TO_ENUM(SHOP_NAME_ENUM, iShop), FALSE)
				
				CLEAR_SHOP_BIT(INT_TO_ENUM(SHOP_NAME_ENUM, iShop), SHOP_NS_FLAG_PLAYER_KICKING_OFF, TRUE)
				CLEAR_SHOP_BIT(INT_TO_ENUM(SHOP_NAME_ENUM, iShop), SHOP_NS_FLAG_FORCE_RESET, TRUE)
				CLEAR_SHOP_BIT(INT_TO_ENUM(SHOP_NAME_ENUM, iShop), SHOP_NS_FLAG_FORCE_RESET_PROCESSING, TRUE)
				CLEAR_SHOP_BIT(INT_TO_ENUM(SHOP_NAME_ENUM, iShop), SHOP_NS_FLAG_CLOSED_FOR_MISSION, TRUE)
				CLEAR_SHOP_BIT(INT_TO_ENUM(SHOP_NAME_ENUM, iShop), SHOP_NS_FLAG_CLOSED_FOR_MINIGAME, TRUE)
				CLEAR_SHOP_BIT(INT_TO_ENUM(SHOP_NAME_ENUM, iShop), SHOP_NS_FLAG_PLAYER_TRYING_TO_ENTER_MOD_SHOP, TRUE)
				CLEAR_SHOP_BIT(INT_TO_ENUM(SHOP_NAME_ENUM, iShop), SHOP_NS_FLAG_FORCE_RESET_LINKED, TRUE)
				
				SET_SHOP_BIT(INT_TO_ENUM(SHOP_NAME_ENUM, iShop), SHOP_NS_FLAG_CLEAR_SHOP_LAUNCHED_FLAG, TRUE)
				SET_SHOP_BLIP_NEEDS_UPDATED(INT_TO_ENUM(SHOP_NAME_ENUM, iShop))
				
				STOP_ALL_SHOP_EVENTS(INT_TO_ENUM(SHOP_NAME_ENUM, iShop))
				REMOVE_USE_OF_SHOP_KEEPER(PLAYER_ID(), INT_TO_ENUM(SHOP_NAME_ENUM, iShop))
			ELSE
				#IF IS_DEBUG_BUILD
					IF g_bDisplayShopDebug
						PRINTSTRING("\n shop_controller: Init - waiting for shop script to terminate, ")PRINTSTRING(GET_SHOP_NAME(INT_TO_ENUM(SHOP_NAME_ENUM, iShop)))PRINTNL()
					ENDIF
				#ENDIF
				
				// Fix for bug #942065 - Send the cleanup message incase the script initialised as we started new SP/MP game.
				FORCE_SHOP_CLEANUP(INT_TO_ENUM(SHOP_NAME_ENUM, iShop))
				bForceCleanup = TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	IF bForceCleanup
		IF NOT bForceCleanupTimerSet
			IF NETWORK_IS_GAME_IN_PROGRESS()
				tdForceCleanupTimer = GET_NETWORK_TIME()
			ELSE
				iForceCleanupTimer = GET_GAME_TIMER()
			ENDIF
			bForceCleanupTimerSet = TRUE
		ENDIF
		EXIT
	ENDIF
	
	bForceCleanupTimerSet = FALSE
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
			eInit = INITIALISED_IN_MP_FREEMODE
			PRINTSTRING("\n shop_controller: INITIALISING in FM")PRINTNL()
		ENDIF
	ELSE
		eInit = INITIALISED_IN_SP
		PRINTSTRING("\n shop_controller: INITIALISING in SP")PRINTNL()
	ENDIF
	
	RESET_SHOP_RUNTIME_FLAGS()
	
	IF NOT HAS_DEFAULT_SHOP_DATA_BEEN_SET()
		SETUP_DEFAULT_SHOP_DATA()
	ENDIF
	
	PATCH_SP_SHOP_STATES()
	
	INITIALISE_SHOP_BLIPS()
	
	// Clean up all menu data incase MP scripts never cleaned it up correctly.
	INT iID
	REPEAT MAX_MENU_IDS iID
		g_sMenuData.bMenuAssetsRequested[iID] = FALSE
		g_sMenuData.bShopDiscountAssetsRequested[iID] = FALSE
		g_sMenuData.bMenuTextRequested[iID] = FALSE
		g_sMenuData.tlTextBlockName[iID] = ""
		g_sMenuData.iScriptHash[iID] = 0
		g_sMenuData.sMenuHelp[iID].stage = SF_REQUEST
		g_sMenuData.sMenuHelp[iID].movieID = NULL
	ENDREPEAT
	
	iShopEntitySetsInitialised = 0
	iShopEntitySetsTimer = GET_GAME_TIMER()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		tdShopEntitySetsTimer = GET_NETWORK_TIME()
	ENDIF
	bInitialisedShopEntitySets = FALSE
	
	// Turn rebreather off if required
	IF IS_BIT_SET(iHeistManagmentBitset, HMB_REBREATHER_ON)
		CPRINTLN(DEBUG_PED_COMP, "DO_INITIALISE - REBREATHER turned off")
		SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), TRUE)
		SET_PED_DIES_IN_SINKING_VEHICLE(PLAYER_PED_ID(), TRUE)
		SET_AUDIO_FLAG("SuppressPlayerScubaBreathing", TRUE)
		CLEAR_BIT(iHeistManagmentBitset, HMB_REBREATHER_ON)
	ELSE
		CDEBUG1LN(DEBUG_PED_COMP, "DO_INITIALISE - REBREATHER already off")
	ENDIF
	
	g_sShopSettings.bControllerRunning = TRUE
	g_sShopSettings.bFirstPassComplete = FALSE
	eStage = SC_STAGE_UPDATE
ENDPROC

FUNC BOOL DO_CHECK_TO_TERMINATE_WHILE_LOOPS()
	IF g_bInMultiplayer
	AND MP_FORCE_TERMINATE_INTERNET_ACTIVE()
		CPRINTLN(DEBUG_INTERNET, "DO_CHECK_TO_TERMINATE_WHILE_LOOPS - MP force terminate internet active")
		RETURN FALSE
	ENDIF
	IF NOT g_bInMultiplayer
	AND SP_FORCE_TERMINATE_INTERNET_ACTIVE()
		CPRINTLN(DEBUG_INTERNET, "DO_CHECK_TO_TERMINATE_WHILE_LOOPS - SP force terminate internet active")
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		CPRINTLN(DEBUG_INTERNET, "DO_CHECK_TO_TERMINATE_WHILE_LOOPS - player switch is in progress")
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

BOOL bDeleteExploitMOC
MODEL_NAMES eCabModel
VEHICLE_INDEX mocVeh
BOOL bPurchaseMOC
PROC FIX_MOC_SAVE_SLOT()
	
	IF USE_SERVER_TRANSACTIONS()
	OR NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR !IS_SKYSWOOP_AT_GROUND()
		EXIT
	ENDIF
	
	IF IS_GUNRUNNING_TRUCK_PURCHASED()
	AND NOT bDeleteExploitMOC
		IF DOES_ENTITY_EXIST(MPGlobalsAmbience.vehTruckVehicle[0])
		AND NOT IS_ENTITY_DEAD(MPGlobalsAmbience.vehTruckVehicle[0])
			INT iSaveSlot
			MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_MODDED_ARMOURY_TRUCK  , iSaveSlot)
			IF iSaveSlot >= 0 
			AND iSaveSlot != DISPLAY_SLOT_MODDED_ARMOURY_TRUCK
				eCabModel = INT_TO_ENUM(MODEL_NAMES,GET_MP_INT_CHARACTER_STAT(GET_INT_STAT_FOR_VEHICLE_SLOT(MPSV_STAT_ELEMENT_MODEL,iSaveSlot), GET_ACTIVE_CHARACTER_SLOT()))
				PRINTLN("FIX_MOC_SAVE_SLOT  - PHANTOM3 OR HAULER2 save slot: ", iSaveSlot)
				IF NETWORK_HAS_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehTruckVehicle[0])		
					IF NOT IS_ENTITY_A_MISSION_ENTITY(MPGlobalsAmbience.vehTruckVehicle[0])
						SET_ENTITY_AS_MISSION_ENTITY(MPGlobalsAmbience.vehTruckVehicle[0], FALSE, TRUE)
					ELSE
						CLEAR_MP_SAVED_VEHICLE_SLOT(iSaveSlot)
						bDeleteExploitMOC = TRUE
						DELETE_VEHICLE(MPGlobalsAmbience.vehTruckVehicle[0])
						PRINTLN("FIX_MOC_SAVE_SLOT  - DELETE PHANTOM3 OR HAULER2")
					ENDIF	
				ELSE
					NETWORK_REQUEST_CONTROL_OF_ENTITY(MPGlobalsAmbience.vehTruckVehicle[0])
				ENDIF
			ENDIF
		ENDIF	
	ENDIF
	
	IF bDeleteExploitMOC
	AND NOT bPurchaseMOC

		INT   iSaveSlot
		
		INT iAltVersion = g_sTruckDataStruct.iCab
		BOOL bAltVersion = (iAltVersion = 0)
		CONFIGURE_STRUCT_FOR_BUYABLE_VEHICLE(BV_DLC_BIG_TRUCK, g_sConfVssMP, TRUE, DEFAULT, iAltVersion)
		
		MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(DISPLAY_SLOT_MODDED_ARMOURY_TRUCK,iSaveSlot)
		
		PRINTLN("FIX_MOC_SAVE_SLOT bPurchaseMOC iSaveSlot: ", DISPLAY_SLOT_MODDED_ARMOURY_TRUCK)
		MPSV_SET_DISPLAY_SLOT(DISPLAY_SLOT_MODDED_ARMOURY_TRUCK, DISPLAY_SLOT_MODDED_ARMOURY_TRUCK)
		
		g_sConfVssMP.VehicleSetup.eModel = eCabModel
		IF CREATE_AND_POPULATE_VEHICLE_DETAILS_FOR_MP_WEBSITE_VEHICLE_BUY(BV_DLC_BIG_TRUCK, WWW_WARSTOCK_D_CACHE_D_AND_D_CARRY_COM, g_sConfVssMP, mocVeh, bAltVersion)

			MP_SAVE_VEHICLE_STORE_CAR_DETAILS_IN_SLOT(mocVeh ,DISPLAY_SLOT_MODDED_ARMOURY_TRUCK,TRUE,TRUE,FALSE,TRUE)
			bPurchaseMOC = TRUE
			PRINTLN("FIX_MOC_SAVE_SLOT bPurchaseMOC = TRUE")
		ENDIF
	
	ENDIF
ENDPROC

BOOL bFixVehSlot
PROC FIX_FOR_RC_VEH_SLOTS()
	IF IS_SCREEN_FADED_IN()
	AND IS_SKYSWOOP_AT_GROUND()
	AND NETWORK_IS_GAME_IN_PROGRESS()
	AND NOT IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		IF !bFixVehSlot
			MODEL_NAMES eRCVehicle
			INT iDisplaySlot, iSaveSlot
			FOR iDisplaySlot = 0 TO MAX_MP_SAVED_VEHICLES-1
				MPSV_GET_SAVE_SLOT_FROM_DISPLAY_SLOT(iDisplaySlot  , iSaveSlot)
				IF iSaveSlot >= 0 
					eRCVehicle = INT_TO_ENUM(MODEL_NAMES,GET_MP_INT_CHARACTER_STAT(GET_INT_STAT_FOR_VEHICLE_SLOT(MPSV_STAT_ELEMENT_MODEL,iSaveSlot), GET_ACTIVE_CHARACTER_SLOT()))
					IF eRCVehicle != DUMMY_MODEL_FOR_SCRIPT
						PRINTLN("FIX_FOR_RC_VEH_SLOTS model name: ", GET_MODEL_NAME_FOR_DEBUG(eRCVehicle), " is in save slot: ", iSaveSlot)
						IF eRCVehicle = RCBANDITO
						AND iDisplaySlot != DISPLAY_SLOT_RCCAR 
							CLEAR_MP_SAVED_VEHICLE_SLOT(iSaveSlot)
							PRINTLN("FIX_FOR_RC_VEH_SLOTS RCCAR IN SLOT clearing iSaveSlot: ", iSaveSlot)
						ENDIF
						IF eRCVehicle = MINITANK
						AND iDisplaySlot != DISPLAY_SLOT_RC_TANK
							CLEAR_MP_SAVED_VEHICLE_SLOT(iSaveSlot)
							PRINTLN("FIX_FOR_RC_VEH_SLOTS MINITANK IN SLOT clearing iSaveSlot: ", iSaveSlot)
						ENDIF
					ENDIF	
				ENDIF
			ENDFOR
			PRINTLN("FIX_FOR_RC_VEH_SLOTS clearing bFixVehSlot TRUE")
			bFixVehSlot = TRUE
		ENDIF	
	ELSE
		IF bFixVehSlot
			bFixVehSlot = FALSE
			PRINTLN("FIX_FOR_RC_VEH_SLOTS clearing bFixVehSlot FALSE")
		ENDIF	
	ENDIF
ENDPROC

FUNC BOOL SHOULD_BLOCK_PLAYER_FROM_ENTERING_ON_FOOT(MODEL_NAMES vehToCheck)
	SWITCH vehToCheck
		CASE RCBANDITO
		CASE MINITANK
		#IF FEATURE_HEIST_ISLAND
		CASE KOSATKA
		#ENDIF
			RETURN TRUE
		BREAK	
	ENDSWITCH
	
	RETURN FALSE
ENDFUNC

PROC PERVENT_PLAYER_FROM_ENTERING_RC_VEHS_ON_FOOT()
	IF IS_LOCAL_PLAYER_INITIALISING_RC_VEHICLE()
		EXIT
	ENDIF	
	
	IF IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(tempVeh)
			MODEL_NAMES playerVehModel = GET_ENTITY_MODEL(tempVeh)
			IF SHOULD_BLOCK_PLAYER_FROM_ENTERING_ON_FOOT(playerVehModel)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				PRINTLN("PERVENT_PLAYER_FROM_ENTERING_RC_VEHS_ON_FOOT CLEAR_PED_TASKS")
			ENDIF
		ENDIF	
	ENDIF	
	
	IF GET_VEHICLE_PED_IS_TRYING_TO_ENTER(PLAYER_PED_ID()) != 0
		VEHICLE_INDEX tempVeh = INT_TO_NATIVE(VEHICLE_INDEX, GET_VEHICLE_PED_IS_TRYING_TO_ENTER(PLAYER_PED_ID()))
		IF NOT IS_ENTITY_DEAD(tempVeh)
			MODEL_NAMES playerVehModel = GET_ENTITY_MODEL(tempVeh)
			IF SHOULD_BLOCK_PLAYER_FROM_ENTERING_ON_FOOT(playerVehModel)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				PRINTLN("PERVENT_PLAYER_FROM_ENTERING_RC_VEHS_ON_FOOT CLEAR_PED_TASKS 2")
			ENDIF
		ENDIF	
	ENDIF
	
	IF GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID()) != NULL
		VEHICLE_INDEX tempVeh = GET_VEHICLE_PED_IS_ENTERING(PLAYER_PED_ID())
		IF NOT IS_ENTITY_DEAD(tempVeh)
			MODEL_NAMES playerVehModel = GET_ENTITY_MODEL(tempVeh)
			IF SHOULD_BLOCK_PLAYER_FROM_ENTERING_ON_FOOT(playerVehModel)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
				PRINTLN("PERVENT_PLAYER_FROM_ENTERING_RC_VEHS_ON_FOOT CLEAR_PED_TASKS 3")
			ENDIF
		ENDIF	
	ENDIF
ENDPROC


PROC DO_UPDATE()
	IF NOT bDisableMainUpdates
	OR iDisableMainUpdatesStage < NUMBER_OF_SHOPS
	
		// Update the frame that we are processing
		iCurrentShopFrame = (iCurrentShopFrame+1) % MAX_FRAMES_TO_PROCESS_SHOPS
		
		INT i, j
		INT iStatValue, iStatUpdate
		SHOP_NAME_ENUM eShop
		
		REPEAT (NUMBER_OF_SHOPS / MAX_FRAMES_TO_PROCESS_SHOPS)+1 j
		
			i = iCurrentShopFrame+(j*MAX_FRAMES_TO_PROCESS_SHOPS)
			
			// Only perform an update if this shop is within the range
			IF i < NUMBER_OF_SHOPS
			
				eShop = INT_TO_ENUM(SHOP_NAME_ENUM, i)
				
				IF bDisableMainUpdates
					iDisableMainUpdatesStage++
					PRINTLN("shop_controller DO_UPDATE iDisableMainUpdatesStage: ", iDisableMainUpdatesStage)
				ENDIF
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF GET_CURRENT_GAMEMODE() = GAMEMODE_FM
						iStatValue = GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_SHOP(eShop))
						iStatUpdate = iStatValue
						UPDATE_MISC_SHOP_FLAGS(eShop, iStatUpdate)
						// Only update when it changes
						IF iStatValue != iStatUpdate
							SET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_SHOP(eShop), iStatUpdate)
						ENDIF
					ENDIF
				ELSE
					UPDATE_MISC_SHOP_FLAGS(eShop, g_savedGlobals.sShopData.iProperties[i])
				ENDIF
				
				UPDATE_SHOP_SPECIFIC_STATES(eShop)
				IF NOT IS_BIT_SET(iLaunchScripts[ENUM_TO_INT(eShop)/32], ENUM_TO_INT(eShop)%32)
					SET_BIT(iLaunchScripts[ENUM_TO_INT(eShop)/32], ENUM_TO_INT(eShop)%32)
					eLaunchScripts[iLaunchScriptCount] = eShop
					iLaunchScriptCount++
				ENDIF
			ENDIF
		ENDREPEAT
		
		// Check what scripts need to be launched
		INT iLaunchScriptCount_Temp = iLaunchScriptCount
		iLaunchScriptCount = 0
		REPEAT (COUNT_OF(iLaunchScripts)) i
			iLaunchScripts[i] = 0
		ENDREPEAT
		REPEAT iLaunchScriptCount_Temp i
			IF eShop != CARMOD_SHOP_PERSONALMOD
//			#IF FEATURE_GUNRUNNING
//			AND eShop != GUN_SHOP_ARMORY
//			#ENDIF
			UPDATE_SHOP_LAUNCH_STATE(eLaunchScripts[i])
//			#IF FEATURE_GUNRUNNING
//			ELIF eShop = GUN_SHOP_ARMORY
//				IF g_ownerOfArmoryTruckPropertyIAmIn != INVALID_PLAYER_INDEX()
//					IF PLAYER_ID() = g_ownerOfArmoryTruckPropertyIAmIn
//						UPDATE_SHOP_LAUNCH_STATE(eLaunchScripts[i])
//					ENDIF
//				ENDIF
//			#ENDIF
			ENDIF
			// If the shop is still in range add it to the new list
			IF IS_SHOP_WITHIN_ACTIVATION_RANGE(eLaunchScripts[i])
			AND (NETWORK_IS_GAME_IN_PROGRESS() OR IS_PLAYER_PED_PLAYABLE(eCurrentPed))
				IF NOT IS_BIT_SET(iLaunchScripts[ENUM_TO_INT(eLaunchScripts[i])/32], ENUM_TO_INT(eLaunchScripts[i])%32)
					SET_BIT(iLaunchScripts[ENUM_TO_INT(eLaunchScripts[i])/32], ENUM_TO_INT(eLaunchScripts[i])%32)
					eLaunchScripts[iLaunchScriptCount] = eLaunchScripts[i]
					iLaunchScriptCount++
				ENDIF
			ENDIF
		ENDREPEAT
		
		UPDATE_SHOP_HELP_DELAY()
		DISPLAY_BARBER_BLIP_FIRST_SEEN_HELP_TEXT()
		
		// First process complete
		IF iCurrentShopFrame >= NUMBER_OF_SHOPS-1
			g_sShopSettings.bFirstPassComplete = TRUE
		ENDIF
	ENDIF
	
	// Remove/add blips for mission change?
	BOOL bForceBlipUpdate = FALSE
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF (bPlayerOnMPMission != bUpdatedBlipsOnMission)
		OR (bPlayerOnMPActivityTurorial != bUpdatedBlipsOnActivityTutorial)
			bUpdatedBlipsOnMission = bPlayerOnMPMission
			bPlayerOnMPActivityTurorial = bUpdatedBlipsOnActivityTutorial
			bForceBlipUpdate = TRUE
			bDisableMainUpdates = FALSE
			
			IF bPlayerOnMPMission
				INT iMissionType = GET_FM_MISSION_TYPE_PLAYER_IS_ON(PLAYER_ID())
				IF iMissionType = FMMC_TYPE_RACE
				OR iMissionType = FMMC_TYPE_DEATHMATCH
				OR (g_sShopSettings.bDisableMainUpdatesForAllAdversaryModes AND g_FMMC_STRUCT.iAdversaryModeType > 0)
					bDisableMainUpdates = TRUE
					PRINTLN("shop_controller - DO_UPDATE bDisableMainUpdates = TRUE")
					iDisableMainUpdatesStage = 0
					
					// We also need to force cleanup any running shops.
					IF g_sShopSettings.bForceCleanupShopScriptsWhenMainUpdateDisabled
						FORCE_SHOP_CLEANUP_ALL()
					ENDIF
				ENDIF
			ENDIF
		ENDIF
		
			// Flow changes
			IF IS_FLOW_STRAND_MISSION_LOCATED_AT_MODSHOP() != bFlowStrandAtModShop
				bFlowStrandAtModShop = !bFlowStrandAtModShop
				bForceBlipUpdate = TRUE
			ENDIF
	ELSE
		IF (bPlayerOnSPMission != bUpdatedBlipsOnMission)
			bUpdatedBlipsOnMission = bPlayerOnSPMission
			bForceBlipUpdate = TRUE
			bDisableMainUpdates = FALSE
		ENDIF
	ENDIF
	
	IF g_sShopSettings.bUpdateShopBlips OR bForceBlipUpdate
		INT i
		REPEAT NUMBER_OF_SHOPS i
			UPDATE_SHOP_BLIP(INT_TO_ENUM(SHOP_NAME_ENUM, i), bForceBlipUpdate)
		ENDREPEAT
		g_sShopSettings.bUpdateShopBlips = FALSE
	ENDIF
ENDPROC

/// PURPOSE: Reset shop runtime flags and jump back to init stage
PROC DO_RESET()
	eStage = SC_STAGE_INIT
	g_sShopSettings.bControllerRunning = FALSE
	g_sShopSettings.bFirstPassComplete = FALSE
	PRINTSTRING("\n shop_controller: RESET")PRINTNL()
ENDPROC

PROC ADJUST_FIRST_PERSON_AIMING()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
			IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
				IF IS_PED_FEMALE(PLAYER_PED_ID())
					// Check if player has bare feet 
					IF GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET) = 35
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_HasBareFeet,TRUE)
					ELSE
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_HasBareFeet,FALSE)
					ENDIF	
				ELSE
					// Check if player has bare feet 
					IF GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET) = 34
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_HasBareFeet,TRUE)
					ELSE
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_HasBareFeet,FALSE)
					ENDIF	
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

INT iStoreAlertStage
BOOL bStoreAcceptReleased
BOOL bStoreCancelReleased
INT iStoreTimer
TIME_DATATYPE tdStoreTimer

PROC PROCESS_STORE_ALERT()

	// Fix for 1677574 - We need to prevent pause menu access whenever a store transition is active.
	IF SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
	OR g_sShopSettings.bProcessStoreAlert
		
		DISABLE_FRONTEND_THIS_FRAME()
		
		#IF IS_DEBUG_BUILD
			IF NOT bDisableFrontEndOutput
				PRINTLN("PROCESS_STORE_ALERT - Frontend menu will now be blocked each frame until the next PROCESS_STORE_ALERT output.")
				bDisableFrontEndOutput = TRUE
			ENDIF
		#ENDIF
	ELSE
		#IF IS_DEBUG_BUILD
			IF bDisableFrontEndOutput
				PRINTLN("PROCESS_STORE_ALERT - No longer blocking the frontend menu.")
				bDisableFrontEndOutput = FALSE
			ENDIF
		#ENDIF
	ENDIF

	// Disable blur after first part of sky swoop up.
	IF g_bStoreTransitionForCashRenderPaused
		IF IS_PLAYER_SWITCH_IN_PROGRESS()
		AND GET_PLAYER_SWITCH_STATE() = SWITCH_STATE_JUMPCUT_ASCENT
		    SET_SKYFREEZE_CLEAR(TRUE)
			SET_SKYBLUR_CLEAR()
//			TOGGLE_RENDERPHASES(TRUE)
//			IF ANIMPOSTFX_IS_RUNNING("MinigameTransitionIn")
//		    	ANIMPOSTFX_STOP("MinigameTransitionIn")
//		    ENDIF
			g_bStoreTransitionForCashRenderPaused = FALSE
			PRINTLN("PROCESS_STORE_ALERT - unpausing render phases")
		ENDIF
	ENDIF
	
	IF TIMERA() < 1000
	OR (iStoreAlertStage > 0)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	ENDIF
	
	IF g_sShopSettings.bProcessStoreAlert
	
		IF (iStoreAlertStage < 3)
			FE_WARNING_FLAGS feWarningFlags = FE_WARNING_OKCANCEL
			SET_WARNING_MESSAGE_WITH_HEADER("BRSCRWTEX", "BRDISTEX", feWarningFlags, "BRSHETEX")
		ENDIF
		
		IF (iStoreAlertStage = 0)
			bStoreAcceptReleased = FALSE
			bStoreCancelReleased = FALSE
			iStoreTimer = GET_GAME_TIMER()
			IF NETWORK_IS_GAME_IN_PROGRESS()
				tdStoreTimer = GET_NETWORK_TIME()
			ENDIF
			iStoreAlertStage++
		ELIF (iStoreAlertStage = 1)
			IF (NETWORK_IS_GAME_IN_PROGRESS() AND IS_TIME_MORE_THAN(GET_NETWORK_TIME(), GET_TIME_OFFSET(tdStoreTimer, 500)))
			OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND GET_GAME_TIMER() - iStoreTimer > 500)
				IF (NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
					bStoreAcceptReleased = TRUE
				ENDIF
				IF (NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL))
					bStoreCancelReleased = TRUE
				ENDIF
			ENDIF
			
			IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) AND bStoreAcceptReleased)
				PLAY_SOUND_FRONTEND(-1,"OK","HUD_FRONTEND_DEFAULT_SOUNDSET")
				iStoreAlertStage++
			ELIF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) AND bStoreCancelReleased)
				PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET")
				g_sShopSettings.bProcessStoreAlert = FALSE
			ENDIF
			
			IF NOT IS_NET_PLAYER_OK(PLAYER_ID())
				g_sShopSettings.bProcessStoreAlert = FALSE
				PRINTLN("PROCESS_STORE_ALERT - EXIT Warning Screen due to Player Death")
			ENDIF
			
			IF bStoreAcceptReleased OR bStoreCancelReleased
			ENDIF
		ELIF (iStoreAlertStage = 2)
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND INT_TO_ENUM(SAVE_TYPE, g_iSaveTypeForStore) != STAT_SAVETYPE_MAX_NUMBER
				REQUEST_SAVE(SSR_REASON_SHARK_CARD_STORE, INT_TO_ENUM(SAVE_TYPE, g_iSaveTypeForStore))
				SETTIMERB(0)
				iStoreAlertStage++
			ELSE
				IF NETWORK_IS_GAME_IN_PROGRESS()
//					ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
//					TOGGLE_RENDERPHASES(FALSE)
					g_bStoreTransitionForCash = TRUE
					g_bStoreTransitionForCashRenderPaused = TRUE
					PRINTLN("PROCESS_STORE_ALERT - pausing render phases")
				ENDIF
				
				IF g_sShopSettings.ePurchaseLocation = SPL_STORE
					SET_LAST_VIEWED_SHOP_ITEM(g_sShopSettings.iLastViewedItemHash, g_sShopSettings.iLastViewedItemCost, g_sShopSettings.iLastViewedItemShop)
				ELSE
					SET_LAST_VIEWED_SHOP_ITEM(0, 0, 0)
				ENDIF
				
				OPEN_COMMERCE_STORE("", "", g_sShopSettings.ePurchaseLocation)
				iStoreAlertStage = 99
				SETTIMERB(0)
			ENDIF
		ELIF (iStoreAlertStage = 3)
//			IF TIMERB() > 500 // Give the save script some time to init.
//				IF NOT STAT_SAVE_PENDING_OR_REQUESTED()
					IF NETWORK_IS_GAME_IN_PROGRESS()
//						ANIMPOSTFX_PLAY("MinigameTransitionIn", 0, TRUE)
//						TOGGLE_RENDERPHASES(FALSE)	
						g_bStoreTransitionForCash = TRUE
						g_bStoreTransitionForCashRenderPaused = TRUE
						PRINTLN("PROCESS_STORE_ALERT - pausing render phases")
					ENDIF
					IF g_sShopSettings.ePurchaseLocation = SPL_STORE
						SET_LAST_VIEWED_SHOP_ITEM(g_sShopSettings.iLastViewedItemHash, g_sShopSettings.iLastViewedItemCost, g_sShopSettings.iLastViewedItemShop)
					ELSE
						SET_LAST_VIEWED_SHOP_ITEM(0, 0, 0)
					ENDIF
					OPEN_COMMERCE_STORE("", "", g_sShopSettings.ePurchaseLocation)
					iStoreAlertStage = 99
					SETTIMERB(0)
//				ELSE
//					PRINTLN("KR STORE SAVE - waiting for save to complete")
//				ENDIF
//			ELSE
//				PRINTLN("KR STORE SAVE - waiting for initial delay on save request")
//			ENDIF
		ELIF (iStoreAlertStage = 99)
			IF TIMERB() > 500
				g_sShopSettings.bProcessStoreAlert = FALSE
			ENDIF
		ELSE
			g_sShopSettings.bProcessStoreAlert = FALSE
		ENDIF
	
	ELIF (iStoreAlertStage > 0)
		iStoreAlertStage = 0
		SETTIMERA(0)
	ENDIF
ENDPROC

OBJECT_INDEX objXmasTree
BOOL bXmasTreeRequested
BOOL bLoadSnowVFX
BOOL bSnowVFXSet
BOOL bSnowSounds
BOOL bLoadFireworkVFX
BOOL bFireworkVFXSet
SCENARIO_BLOCKING_INDEX sbTreeBlock

BOOL bSafeToTurnOnMpXmasContent = FALSE

PROC MANAGE_XMAS_CONTENT()

	BOOL bSafeToTurnOnMpContent_local = FALSE
	IF NOT IS_FAKE_MULTIPLAYER_MODE_SET()
		IF (GET_CURRENT_GAMEMODE() != GAMEMODE_SP AND GET_CURRENT_GAMEMODE() != GAMEMODE_EMPTY AND GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR)
			bSafeToTurnOnMpContent_local = TRUE
		ELIF (GET_JOINING_GAMEMODE() = GAMEMODE_FM)
			bSafeToTurnOnMpContent_local = TRUE
		ELIF IS_THIS_PLAYER_LEAVING_THE_CORONA(PLAYER_ID())
			bSafeToTurnOnMpContent_local = TRUE
		ELIF GET_CONFIRM_INVITE_INTO_GAME_STATE()
			bSafeToTurnOnMpContent_local = TRUE
		ENDIF
	ENDIF
	
	IF bSafeToTurnOnMpXmasContent != bSafeToTurnOnMpContent_local
		bSafeToTurnOnMpXmasContent = bSafeToTurnOnMpContent_local
		PRINTLN("MANAGE_XMAS_CONTENT - Updating bSafeToTurnOnMpXmasContent = ", bSafeToTurnOnMpXmasContent)
	ENDIF
	
	
	BOOL bTurnXmasSnowOn = FALSE
	
	IF (g_sMPTunables.bTurn_snow_on_off AND bSafeToTurnOnMpXmasContent)
	#IF IS_DEBUG_BUILD
	OR b_debug_turn_xmas_snow_on
	#ENDIF
		bTurnXmasSnowOn = TRUE
	ENDIF
	
	#IF FEATURE_HEIST_ISLAND
	IF IS_PLAYER_ON_HEIST_ISLAND(PLAYER_ID())
	OR g_bDisableXmasSnowForIsland  //second global to kill snow earlier than "on island" is set
		bTurnXmasSnowOn = FALSE
	ENDIF
	#ENDIF
	
	IF PLAYER_USING_ORBITAL(PLAYER_ID())
		bTurnXmasSnowOn = FALSE
	ENDIF
	
	//////////////////////////////////////////////////////////////////////////////
	/// Bug 1701043 - Timed switch for Xmas Release Ped Content: Hats & Masks
	///     
	IF bTurnXmasSnowOn != g_bTurnXmasSnowOn
		g_bTurnXmasSnowOn = bTurnXmasSnowOn
		PRINTLN("MANAGE_XMAS_CONTENT - Updating xmas snow content unlock to ", g_bTurnXmasSnowOn)
		
		/*#IF FEATURE_XMAS_2014
		IF bTurnXmasSnowOn
		AND NETWORK_IS_GAME_IN_PROGRESS()
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID() , WEAPONTYPE_DLC_SNOWBALL, 3, FALSE, FALSE)
			PRINTLN("MANAGE_XMAS_CONTENT - Give the player three snowballs when it snows")
		ENDIF
		#ENDIF*/
	ENDIF
	
	//////////////////////////////////////////////////////////////////////////////
	/// Bug 1696407 - Set it so we can make it snow on xmas day
	///     
	IF g_bTurnXmasSnowOn
		IF GET_NEXT_WEATHER_TYPE_HASH_NAME() != HASH("XMAS")
			IF NOT SHOULD_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH()

				PRINTLN("MANAGE_XMAS_CONTENT - Turning on the snow!")
				SHOULD_TRANSITION_SESSIONS_RUN_FREEMODE_RELAUNCH()
				
				SET_OVERRIDE_WEATHER("XMAS")
				
				bLoadSnowVFX = TRUE
				bLoadFireworkVFX = TRUE
			ENDIF
		ENDIF
	ELSE
		IF GET_NEXT_WEATHER_TYPE_HASH_NAME() = HASH("XMAS")
			PRINTLN("MANAGE_XMAS_CONTENT - Turning off the snow!")
			CLEAR_OVERRIDE_WEATHER()
		ENDIF
		bLoadSnowVFX = FALSE
		bLoadFireworkVFX = FALSE
	ENDIF
	
	IF bLoadSnowVFX
	AND (NOT NETWORK_IS_GAME_IN_PROGRESS() OR GET_YACHT_PLAYER_IS_ON(PLAYER_ID()) = -1)
		IF NOT bSnowVFXSet
			REQUEST_NAMED_PTFX_ASSET("core_snow")
			//Load the snow sounds
			IF REQUEST_SCRIPT_AUDIO_BANK("SNOW_FOOTSTEPS") 
			//and has that PTFX loaded
			AND HAS_NAMED_PTFX_ASSET_LOADED("core_snow")
				USE_SNOW_FOOT_VFX_WHEN_UNSHELTERED(TRUE)
				USE_SNOW_WHEEL_VFX_WHEN_UNSHELTERED(TRUE)
				bSnowVFXSet = TRUE
			ENDIF
		ENDIF
		//If the player is OK
		IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			//And they are out side
			IF GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID()) = 0
			AND NOT ((g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_STUNT //Don't load snow footsteps if we are on a transform race
			OR g_FMMC_STRUCT.iRaceType = FMMC_RACE_TYPE_STUNT_P2P)
			AND g_FMMC_STRUCT.iMissionSubType = FMMC_RACE_SUBTYPE_TRANSFORM_RACE)
				//Then request the snow sounds
				IF NOT bSnowSounds
					HINT_SCRIPT_AUDIO_BANK("SNOW_FOOTSTEPS") 
					bSnowSounds = TRUE
				ENDIF
			//If they are out side then clear the snow sounds
			ELSE
				IF bSnowSounds
					RELEASE_NAMED_SCRIPT_AUDIO_BANK("SNOW_FOOTSTEPS") 
					bSnowSounds = FALSE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		//If the snow sounds have been loaded then clear them. 
		IF bSnowSounds
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("SNOW_FOOTSTEPS") 
			bSnowSounds = FALSE
		ENDIF
		IF bSnowVFXSet
			USE_SNOW_FOOT_VFX_WHEN_UNSHELTERED(FALSE)
			USE_SNOW_WHEEL_VFX_WHEN_UNSHELTERED(FALSE)
			REMOVE_NAMED_PTFX_ASSET("core_snow")
			//Release the snow sounds
			RELEASE_NAMED_SCRIPT_AUDIO_BANK("SNOW_FOOTSTEPS") 
			bSnowVFXSet = FALSE
		ENDIF
	ENDIF
	
	IF bLoadFireworkVFX
		IF NOT bFireworkVFXSet
			REQUEST_NAMED_PTFX_ASSET("proj_xmas_firework")
			IF HAS_NAMED_PTFX_ASSET_LOADED("proj_xmas_firework")
				SET_PARTICLE_FX_OVERRIDE("scr_firework_indep_burst_rwb", "scr_firework_xmas_burst_rgw")
				SET_PARTICLE_FX_OVERRIDE("scr_firework_indep_spiral_burst_rwb", "scr_firework_xmas_spiral_burst_rgw")
				SET_PARTICLE_FX_OVERRIDE("scr_firework_indep_repeat_burst_rwb", "scr_firework_xmas_repeat_burst_rgw")
				SET_PARTICLE_FX_OVERRIDE("scr_firework_indep_ring_burst_rwb", "scr_firework_xmas_ring_burst_rgw")
				bFireworkVFXSet = TRUE
				PRINTLN("MANAGE_XMAS_CONTENT - switched to xmas firework ptfx")
			ENDIF
		ENDIF
	ELSE
		IF bFireworkVFXSet
			RESET_PARTICLE_FX_OVERRIDE("scr_firework_indep_burst_rwb")
			RESET_PARTICLE_FX_OVERRIDE("scr_firework_indep_spiral_burst_rwb")
			RESET_PARTICLE_FX_OVERRIDE("scr_firework_indep_repeat_burst_rwb")
			RESET_PARTICLE_FX_OVERRIDE("scr_firework_indep_ring_burst_rwb")
			REMOVE_NAMED_PTFX_ASSET("proj_xmas_firework")
			bFireworkVFXSet = FALSE
			PRINTLN("MANAGE_XMAS_CONTENT - switched back to standard firework ptfx")
		ENDIF
	ENDIF
	
	//////////////////////////////////////////////////////////////////////////////
	/// Bug 2152404 - Set up the Pershing Square Christmas tree removal / switch via the tunables.
	///   
	IF (NOT g_sMPTunables.bDisable_Christmas_Tree_Perishing_Square AND bSafeToTurnOnMpXmasContent AND NOT IS_PLAYER_ON_ANY_FM_MISSION_THAT_IS_NOT_GANG_ATTACK(PLAYER_ID()) AND GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR)
	#IF IS_DEBUG_BUILD
	OR b_debug_turn_xmas_tree_on
	#ENDIF
		// Create xmas tree
		IF NOT DOES_ENTITY_EXIST(objXmasTree)
			
			VECTOR vTreeCoords = <<0.0, 0.0, -87.66>>
			
			REQUEST_MODEL(PROP_XMAS_EXT)
			bXmasTreeRequested = TRUE
			IF HAS_MODEL_LOADED(PROP_XMAS_EXT)
			AND (GET_DISTANCE_BETWEEN_COORDS(vTreeCoords, GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)) > 150 OR NOT IS_SKYSWOOP_AT_GROUND())
				objXmasTree = CREATE_OBJECT_NO_OFFSET(PROP_XMAS_EXT, << 237.4236, -880.7832, 29.4971>>, FALSE, FALSE, FALSE)
				FREEZE_ENTITY_POSITION(objXmasTree, TRUE)
				SET_ENTITY_ROTATION(objXmasTree, vTreeCoords)
				SET_ENTITY_LOD_DIST(objXmasTree, 5000)
				SET_MODEL_AS_NO_LONGER_NEEDED(PROP_XMAS_EXT)
				sbTreeBlock = ADD_SCENARIO_BLOCKING_AREA(<< 237.4236, -880.7832, 29.4971>> + <<-6,-6,-6>>, << 237.4236, -880.7832, 29.4971>> + <<6,6,20>>)
				bXmasTreeRequested = FALSE
				
				PRINTLN("MANAGE_XMAS_CONTENT - Creating Christmas tree!")
			ENDIF
		ENDIF
	ELSE
		// Remove xmas tree
		IF bXmasTreeRequested
			SET_MODEL_AS_NO_LONGER_NEEDED(PROP_XMAS_EXT)
			bXmasTreeRequested = FALSE
		ENDIF
		IF DOES_ENTITY_EXIST(objXmasTree)
			DELETE_OBJECT(objXmasTree)
			REMOVE_SCENARIO_BLOCKING_AREA(sbTreeBlock)
			PRINTLN("MANAGE_XMAS_CONTENT - Deleting Christmas tree!")
		ENDIF
	ENDIF
ENDPROC

INT iTransactionAlertStage
INT iTransactionTimer
TIME_DATATYPE tdTransactionTimer

FUNC STRING GET_TRANSACTION_FAIL_ALERT_TEXT_LABEL(GAMESERVERRESULTS eResult)

	SWITCH eResult
		CASE GAMESERVER_ERROR_UNDEFINED
		CASE GAMESERVER_ERROR_NOT_IMPLEMENTED
		CASE GAMESERVER_ERROR_SYSTEM_ERROR
		CASE GAMESERVER_ERROR_STALE_DATA
		CASE GAMESERVER_ERROR_INVALID_REQUEST
		CASE GAMESERVER_ERROR_INVALID_ACCOUNT
		CASE GAMESERVER_ERROR_INVALID_LIMITER_DEFINITION
		CASE GAMESERVER_ERROR_GAME_TRANSACTION_EXCEPTION
		CASE GAMESERVER_ERROR_AUTH_FAILED
		CASE GAMESERVER_ERROR_BAD_SIGNATURE
		CASE GAMESERVER_ERROR_INVALID_CONFIGURATION
			RETURN "CTALERT_F_1"
		BREAK
		
		CASE GAMESERVER_ERROR_EXCEED_LIMIT
			RETURN "CTALERT_F_2"
		BREAK
		
		CASE GAMESERVER_ERROR_INVALID_PERMISSIONS
			RETURN "CTALERT_F_3"
		BREAK
		
		CASE GAMESERVER_ERROR_INVALID_DATA
		CASE GAMESERVER_ERROR_INVALID_PRICE
		CASE GAMESERVER_ERROR_INSUFFICIENT_FUNDS
		CASE GAMESERVER_ERROR_INSUFFICIENT_INVENTORY
		CASE GAMESERVER_ERROR_INCORRECT_FUNDS
		CASE GAMESERVER_ERROR_INVALID_ITEM
			RETURN "CTALERT_F_4"
		BREAK
		
		DEFAULT
			IF ENUM_TO_INT(eResult) >= 400 AND ENUM_TO_INT(eResult) <= 600
				RETURN "CTALERT_F"
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN "CTALERT_F_1"
ENDFUNC

PROC PROCESS_CASH_TRANSACTION_ALERT()
	
	IF g_bTriggerTransactionFailedAlert
	
		
		IF GET_TRANSACTION_ALERT_TYPE(g_eTriggerTransactionFailed_Category, g_eTriggerTransactionFailed_Service, g_iTriggerTransactionFailed_Cost) = TRANSACTION_ALERT_FULLSCREEN
		
			IF (iTransactionAlertStage = 0)
				iTransactionAlertStage++
			ELSE
				FE_WARNING_FLAGS feWarningFlags = FE_WARNING_CONTINUE
				
				#IF IS_DEBUG_BUILD
					SET_WARNING_MESSAGE_WITH_HEADER("CTALERT_A", GET_TRANSACTION_FAIL_ALERT_TEXT_LABEL(g_eTriggerTransactionFailed_Error), feWarningFlags, DEFAULT, DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE)
				#ENDIF
				
				#IF NOT IS_DEBUG_BUILD
					SET_WARNING_MESSAGE_WITH_HEADER("CTALERT_A", GET_TRANSACTION_FAIL_ALERT_TEXT_LABEL(g_eTriggerTransactionFailed_Error), feWarningFlags)
				#ENDIF
				
				SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
				
				
				#IF IS_DEBUG_BUILD
					// Add additional debug text so we can quickly debug failed transactions.
					
					DRAW_RECT(0.5, 0.5, 1.0, 1.0, 0, 0, 0, 255)
					
					INT iR, iG, iB, iA
					GET_HUD_COLOUR(HUD_COLOUR_WHITE, iR, iG, iB, iA)
					
					SET_TEXT_FONT(FONT_STANDARD)
					SET_TEXT_SCALE(0.0000, CUSTOM_MENU_TEXT_SCALE_Y)
				    SET_TEXT_COLOUR(iR, iG, iB, iA)
					SET_TEXT_CENTRE(FALSE)
				    SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
				    SET_TEXT_EDGE(0, 0, 0, 0, 0)
					
					
					BEGIN_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT("DBG_CASH_FAIL")
						/*id*/		ADD_TEXT_COMPONENT_INTEGER(g_TransactionDebug_id)
						/*error*/	ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING( g_TransactionDebug_error )
						/*frame*/	ADD_TEXT_COMPONENT_INTEGER(g_TransactionDebug_frame)
						/*type*/	ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_TransactionDebug_type)
						/*action*/	ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_TransactionDebug_action)
						/*flags*/	ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_TransactionDebug_flags)
					FLOAT fTextWidth = END_TEXT_COMMAND_GET_SCREEN_WIDTH_OF_DISPLAY_TEXT(TRUE)
					
					FLOAT fTextX = 0.5-(fTextWidth*0.5)
					
					SET_TEXT_FONT(FONT_STANDARD)
					SET_TEXT_SCALE(0.0000, CUSTOM_MENU_TEXT_SCALE_Y)
				    SET_TEXT_COLOUR(iR, iG, iB, iA)
					SET_TEXT_CENTRE(FALSE)
				    SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
				    SET_TEXT_EDGE(0, 0, 0, 0, 0)
					
					BEGIN_TEXT_COMMAND_DISPLAY_TEXT("DBG_CASH_FAIL")
						/*id*/		ADD_TEXT_COMPONENT_INTEGER(g_TransactionDebug_id)
						/*error*/	ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING( g_TransactionDebug_error )
						/*frame*/	ADD_TEXT_COMPONENT_INTEGER(g_TransactionDebug_frame)
						/*type*/	ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_TransactionDebug_type)
						/*action*/	ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_TransactionDebug_action)
						/*flags*/	ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_TransactionDebug_flags)
					END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(fTextX, 0.65)
					
					SET_TEXT_FONT(FONT_STANDARD)
					SET_TEXT_SCALE(0.0000, CUSTOM_MENU_TEXT_SCALE_Y)
				    SET_TEXT_COLOUR(iR, iG, iB, iA)
					SET_TEXT_CENTRE(FALSE)
				    SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
				    SET_TEXT_EDGE(0, 0, 0, 0, 0)
					
					BEGIN_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING("DBG_CASH_FAIL")
						/*id*/		ADD_TEXT_COMPONENT_INTEGER(g_TransactionDebug_id)
						/*error*/	ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING( g_TransactionDebug_error )
						/*frame*/	ADD_TEXT_COMPONENT_INTEGER(g_TransactionDebug_frame)
						/*type*/	ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_TransactionDebug_type)
						/*action*/	ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_TransactionDebug_action)
						/*flags*/	ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_TransactionDebug_flags)
					FLOAT fTextHeight = (GET_RENDERED_CHARACTER_HEIGHT(CUSTOM_MENU_TEXT_SCALE_Y) * END_TEXT_COMMAND_GET_NUMBER_OF_LINES_FOR_STRING(0.5-(fTextWidth*0.5), 0.65))
					
					SET_TEXT_FONT(FONT_STANDARD)
					SET_TEXT_SCALE(0.0000, CUSTOM_MENU_TEXT_SCALE_Y)
				    SET_TEXT_COLOUR(iR, iG, iB, iA)
					SET_TEXT_CENTRE(FALSE)
				    SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
				    SET_TEXT_EDGE(0, 0, 0, 0, 0)
					
					BEGIN_TEXT_COMMAND_DISPLAY_TEXT("DBG_CASH_FAIL2")
						/*service*/		ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_TransactionDebug_service)
						/*category*/	ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING(g_TransactionDebug_category)
						/*cost*/		ADD_TEXT_COMPONENT_INTEGER(g_TransactionDebug_cost)
						/*itemID*/		ADD_TEXT_COMPONENT_INTEGER(g_TransactionDebug_itemID)
						/*extraItemID*/	ADD_TEXT_COMPONENT_INTEGER(g_TransactionDebug_extraItemID)
					END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(fTextX, 0.65 + (fTextHeight) + 0.125)
				#ENDIF
				
				IF (iTransactionAlertStage = 1)
					iTransactionTimer = GET_GAME_TIMER()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						tdTransactionTimer = GET_NETWORK_TIME()
					ENDIF
					iTransactionAlertStage++
				ELIF (iTransactionAlertStage = 2)
					IF (NETWORK_IS_GAME_IN_PROGRESS() AND ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdTransactionTimer)) > 500)
					OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND GET_GAME_TIMER() - iTransactionTimer > 500)
						IF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
						OR (IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT))
						OR NOT IS_NET_PLAYER_OK(PLAYER_ID())
							g_bTriggerTransactionFailedAlert = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
		ELIF GET_TRANSACTION_ALERT_TYPE(g_eTriggerTransactionFailed_Category, g_eTriggerTransactionFailed_Service, g_iTriggerTransactionFailed_Cost) = TRANSACTION_ALERT_FEED
			
			IF (iTransactionAlertStage = 0)
				iTransactionAlertStage++
			ELSE
				BEGIN_TEXT_COMMAND_THEFEED_POST(GET_TRANSACTION_FAIL_ALERT_TEXT_LABEL(g_eTriggerTransactionFailed_Error))
				END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)
				g_bTriggerTransactionFailedAlert = FALSE
			ENDIF
			
		ELSE
			g_bTriggerTransactionFailedAlert = FALSE
		ENDIF
	ELSE
		iTransactionAlertStage = 0
	ENDIF
	
	IF g_bTriggerTransactionBlockedAlert
		BEGIN_TEXT_COMMAND_THEFEED_POST("CTALERT_F_1")
		END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)
		g_bTriggerTransactionBlockedAlert = FALSE
		g_bTriggerTransactionBlockedConsumeAlert = FALSE
	ELIF g_bTriggerTransactionBlockedConsumeAlert
		BEGIN_TEXT_COMMAND_THEFEED_POST("CTALERT_F_1")
		END_TEXT_COMMAND_THEFEED_POST_TICKER(TRUE)
		g_bTriggerTransactionBlockedAlert = FALSE
		g_bTriggerTransactionBlockedConsumeAlert = FALSE
	ENDIF
	
	
	#IF IS_DEBUG_BUILD
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND IS_NET_PLAYER_OK(PLAYER_ID())
	AND IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		INT currentState
		INT refreshSessionRequested
		NET_GAMESERVER_GET_SESSION_STATE_AND_STATUS(currentState, refreshSessionRequested)
		
		IF currentState = ENUM_TO_INT(GAMESERVER_PENDING_CATALOG)
			
			INT iR, iG, iB, iA
			GET_HUD_COLOUR(HUD_COLOUR_RED, iR, iG, iB, iA)
			
			SET_TEXT_FONT(FONT_STANDARD)
			SET_TEXT_SCALE(0.0000, 1.15)
		    SET_TEXT_COLOUR(iR, iG, iB, iA)
			SET_TEXT_CENTRE(FALSE)
			SET_TEXT_RIGHT_JUSTIFY(TRUE)
			SET_TEXT_DROPSHADOW(0, 0, 0, 0, 0)
		    SET_TEXT_EDGE(0, 0, 0, 0, 0)
			
			SET_SCRIPT_GFX_ALIGN(UI_ALIGN_RIGHT, UI_ALIGN_TOP)
			SET_SCRIPT_GFX_ALIGN_PARAMS(0.0, 0.0, 0.0, 0.0)
			
			BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
				ADD_TEXT_COMPONENT_SUBSTRING_LITERAL_STRING("Refreshing Catalog")
			END_TEXT_COMMAND_DISPLAY_DEBUG_TEXT(0.0, 0.835)
			
			RESET_SCRIPT_GFX_ALIGN()
		ENDIF
	ENDIF
	#ENDIF
ENDPROC
BOOL bRequestIndiPTFX = FALSE
PROC MANAGE_INDEPENDENCE_CONTENT()
	//Request all the assets for the independence smoke
	IF IS_MP_INDEPENDENCE_PACK_PRESENT()
		
		IF NOT bRequestIndiPTFX
			REQUEST_NAMED_PTFX_ASSET("scr_indep_wheelsmoke")
			REQUEST_NAMED_PTFX_ASSET("scr_indep_parachute")
			bRequestIndiPTFX = TRUE
		ENDIF
		
		IF g_sMPTunables.btoggleactivateIndependencepack
			SET_INDEPENDENCE_CONTENT_AVAILABLE_IN_SP()
		ENDIF
	ENDIF
ENDPROC

INT iBrowseStage
TIME_DATATYPE tdBrowseTimer
PROC MANAGE_SHOP_BROWSE_TIME()
	SWITCH iBrowseStage
		CASE 0
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
				PRINTLN("MANAGE_SHOP_BROWSE_TIME - Player started browsing in shops")
				tdBrowseTimer = GET_NETWORK_TIME()
				iBrowseStage++
			ENDIF
		BREAK
		CASE 1
			IF NOT IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					INT iBrowseTime, iCurrentBrowseTime
					
					iBrowseTime = (ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdBrowseTimer))/1000)
					PRINTLN("MANAGE_SHOP_BROWSE_TIME - Player has just been browsing for ", iBrowseTime, " seconds.")
					
					IF STAT_GET_INT(TOTAL_SHOP_TIME, iCurrentBrowseTime)
						iCurrentBrowseTime += iBrowseTime
						PRINTLN("MANAGE_SHOP_BROWSE_TIME - Total browse time is ", iCurrentBrowseTime, " seconds.")
						STAT_SET_INT(TOTAL_SHOP_TIME, iCurrentBrowseTime)
					ENDIF
				ENDIF
				PRINTLN("MANAGE_SHOP_BROWSE_TIME - Player finished browsing in shops")
				iBrowseStage = 0
			ENDIF
		BREAK
	ENDSWITCH
	
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		REPLAY_PREVENT_RECORDING_AND_UI_THIS_FRAME()
	ENDIF
ENDPROC


FUNC BOOL IS_PED_IN_A_WATER_VEHICLE(PED_INDEX pedID)
	IF IS_PED_IN_ANY_SUB(pedID)
		RETURN TRUE
	ENDIF
	
	IF IS_PED_IN_ANY_VEHICLE(pedID)
		VEHICLE_INDEX vehID = GET_VEHICLE_PED_IS_IN(pedID)
		IF DOES_ENTITY_EXIST(vehID)
		//AND IS_VEHICLE_DRIVEABLE(vehID)
			SWITCH GET_ENTITY_MODEL(vehID)
				CASE TECHNICAL2
				CASE BLAZER5
				CASE STROMBERG
					RETURN TRUE
				BREAK
			ENDSWITCH
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

SCRIPT_TIMER stRebreatherTimer
INT iUsedAir = 0
BOOL bInVehWater = FALSE

//PURPOSE: Controls the rebreather HUD display and How much air is left in each filter
PROC MAINTAIN_REBREATHER_AIR()
	IF IS_BIT_SET(iHeistManagmentBitset, HMB_REBREATHER_ON)
	
		HUDORDER hOrder = HUDORDER_DONTCARE
		
		// 2718266
		IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_GB_SALVAGE
		#IF FEATURE_DLC_1_2022
		OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_SPECIAL_CARGO
		#ENDIF
			hOrder = HUDORDER_BOTTOM
		ENDIF
		
		IF IS_THIS_ROCKSTAR_MISSION_WVM_APC(g_FMMC_STRUCT.iRootContentIDHash)
		OR GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(PLAYER_ID()) = FMMC_TYPE_SMUGGLER_BUY
		OR (NETWORK_IS_ACTIVITY_SESSION() AND IS_BIT_SET(g_FMMC_STRUCT.iOptionsMenuBitSetThirteen, ciALLOW_NON_HEIST_REBREATHERS))
			IF g_bResetRebreatherAir
				RESET_NET_TIMER(stRebreatherTimer)
				iUsedAir = 0
				g_bResetRebreatherAir = FALSE
			ENDIF
		ENDIF
		
		IF (NOT IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_ENTITY_UNDERWATER(PLAYER_PED_ID()))
		OR (IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID()) AND IS_ENTITY_UNDERWATER(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID())) AND NOT IS_PED_IN_A_WATER_VEHICLE(PLAYER_PED_ID()))
		OR bInVehWater
			INT iRebreathersRemaining = GET_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT)
			//Display Air
			IF iRebreathersRemaining > 0
								
				//IF HAS_NET_TIMER_EXPIRED(stRebreatherTimer, (g_sMPTunables.itimeofrebreather*1000))
				IF HAS_NET_TIMER_STARTED(stRebreatherTimer)
					IF ((g_sMPTunables.itimeofrebreather*1000)-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stRebreatherTimer)-iUsedAir) < 0
						DECREMENT_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT)
						iRebreathersRemaining = GET_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT)
						CPRINTLN(DEBUG_PED_COMP, "MAINTAIN_REBREATHER_AIR - ONE REBREATHER USED UP - REBREATHERS REMAINING = ", iRebreathersRemaining)
						iUsedAir = 0
						CPRINTLN(DEBUG_PED_COMP, "MAINTAIN_REBREATHER_AIR - USED AIR RESET = ", iUsedAir)
						RESET_NET_TIMER(stRebreatherTimer)
					ENDIF
				ELSE
					//RESET_NET_TIMER(stRebreatherTimer)
					START_NET_TIMER(stRebreatherTimer)
					CPRINTLN(DEBUG_PED_COMP, "MAINTAIN_REBREATHER_AIR - TIMER STARTED - USED AIR = ", iUsedAir)
				ENDIF
				IF GB_GET_PLAYER_CURRENT_GANG_BOSS_MISSION(player_id()) =FMMC_TYPE_GB_SALVAGE
					iRebreathersRemaining -=1
					PRINTLN("MAINTAIN_REBREATHER_AIR player is on salvage mission. Make the rebreathers one less = ", iRebreathersRemaining, ", bug 2822380 KW 5 may 2016")
				ENDIF
				DRAW_GENERIC_SCORE(iRebreathersRemaining, "NUM_REBR", DEFAULT, DEFAULT, hOrder)	
				DRAW_GENERIC_METER(((g_sMPTunables.itimeofrebreather*1000)-GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stRebreatherTimer)-iUsedAir), 
									(g_sMPTunables.itimeofrebreather*1000), "AIR_REBR", HUD_COLOUR_BLUELIGHT, DEFAULT, hOrder)
				
			//Display Empty
			ELSE
				DRAW_GENERIC_SCORE(0, "NUM_REBR")							//REBREATHERS
				DRAW_GENERIC_METER(0, 1, "AIR_REBR", HUD_COLOUR_BLUELIGHT)	//AIR
			ENDIF
			
			bInVehWater = FALSE
			IF (IS_PED_SITTING_IN_ANY_VEHICLE(PLAYER_PED_ID())
			AND IS_ENTITY_IN_WATER(GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID()))
			AND NOT IS_PED_IN_A_WATER_VEHICLE(PLAYER_PED_ID()))
				bInVehWater = TRUE
			ENDIF
		ELSE
			IF HAS_NET_TIMER_STARTED(stRebreatherTimer)
				iUsedAir += GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stRebreatherTimer)
				RESET_NET_TIMER(stRebreatherTimer)
				CPRINTLN(DEBUG_PED_COMP, "MAINTAIN_REBREATHER_AIR - OUT OF WATER - USED AIR = ", iUsedAir)
			ENDIF
		ENDIF
	ELSE
		IF HAS_NET_TIMER_STARTED(stRebreatherTimer)
			iUsedAir += GET_NET_TIMER_DIFFERENCE_WITH_CURRENT_TIME(stRebreatherTimer)
			RESET_NET_TIMER(stRebreatherTimer)
			CPRINTLN(DEBUG_PED_COMP, "MAINTAIN_REBREATHER_AIR - REBREATHER TURNED OFF - USED AIR = ", iUsedAir)
		ENDIF
	ENDIF
ENDPROC

BOOL	bFinalize = FALSE
INT		iEarID = -1000, iHeadID = -1000
INT		iBerdID = 0, iJbibID = -1000, iSpecialID = -1000
INT		iCharacterSlotID = -1
BOOL	bInVehicle = FALSE

FUNC BOOL IS_SAFE_TO_TURN_ON_REBREATHER()
	VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
	IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_DEACTIVATE_REBREATHER)
	OR GET_MP_INT_CHARACTER_STAT(MP_STAT_BREATHING_APPAR_BOUGHT) = 0
	OR g_bCelebrationScreenIsActive
	OR vPlayerCoords.z < -140	//Depth check for Bug 1949575 AND 2210743
	OR (g_b_On_Race AND NOT g_b_IsGTARace) //Disable for non GTA races B* 3308874
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

PROC MANAGE_HEIST_CONTENT()

	BOOL bDisableRebreather = FALSE
	BOOL bSafeToProcessMPChecks = FALSE
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND NOT IS_PLAYER_SPECTATING(PLAYER_ID())
	AND g_bSafeToProcessMPChecks
	AND GET_CURRENT_GAMEMODE() = GAMEMODE_FM
		bSafeToProcessMPChecks = TRUE
	ENDIF
	
	IF bSafeToProcessMPChecks
		
		INT iTempIDCheck, iTempIDCheck2
		INT iDLCNameHash
		INT iDLCNameHashJBIB
		INT iDLCNameHashBERD
		INT iDLCNameHashHEAD
		INT iDLCNameHashEYES
		scrShopPedComponent componentItem
		
		// Process ear prop changes
		iTempIDCheck = ((GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_EARS) * 1000)+(GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_EARS)))
		IF iEarID != iTempIDCheck
			//BLUETOOTH
			//control SET_PLAYER_BLUETOOTH_STATE based on if the player is wearing an earpiece
			IF DOES_CURRENT_PED_PROP_HAVE_RESTRICTION_TAG(PLAYER_PED_ID(), ENUM_TO_INT(ANCHOR_EARS), DLC_RESTRICTION_TAG_EAR_PIECE)
				IF NOT IS_PLAYER_BLUETOOTH_ENABLE(PLAYER_ID())
					CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Setting PROP//ANCHOR_EARS varition with restriction tag EAR_PIECE turned on - SET_PLAYER_BLUETOOTH_STATE(PLAYER_ID(), TRUE)")
					SET_PLAYER_BLUETOOTH_STATE(PLAYER_ID(), TRUE)
				ENDIF
			ELSE
				IF IS_PLAYER_BLUETOOTH_ENABLE(PLAYER_ID())
					CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Setting PROP//ANCHOR_EARS varition with restriction tag EAR_PIECE turned off - SET_PLAYER_BLUETOOTH_STATE(PLAYER_ID(), FALSE)")
					SET_PLAYER_BLUETOOTH_STATE(PLAYER_ID(), FALSE)
				ENDIF
			ENDIF
			iEarID = iTempIDCheck
		ENDIF
				
		iTempIDCheck = ((GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD) * 1000)+(GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD)))
		IF iHeadID != iTempIDCheck
			CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Head prop swapped")

			iDLCNameHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(ANCHOR_HEAD), GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD), GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD))
			iDLCNameHashJBIB = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(PED_COMP_JBIB), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB))
			iDLCNameHashBERD = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(PED_COMP_BERD), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD))
			IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_PILOT_SUIT, ENUM_TO_INT(SHOP_PED_PROP))
			AND DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashJBIB, DLC_RESTRICTION_TAG_PILOT_SUIT, ENUM_TO_INT(SHOP_PED_COMPONENT))
				IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
					GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_M_TEETH_4_0), componentItem)
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, componentItem.m_drawableIndex, componentItem.m_textureIndex)
				ELIF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
					GET_SHOP_PED_COMPONENT(ENUM_TO_INT(DLC_MP_HEIST_F_TEETH_5_0), componentItem)
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, componentItem.m_drawableIndex, componentItem.m_textureIndex)
				ENDIF
				CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Pilot helmet on add hose")
				bFinalize = TRUE
			ELSE
				IF DOES_CURRENT_PED_COMPONENT_HAVE_RESTRICTION_TAG(PLAYER_PED_ID(), PED_COMP_TEETH, DLC_RESTRICTION_TAG_PILOT_SUIT)
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH, 0, 0)
					CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Pilot helmet off remove hose")
					bFinalize = TRUE
				ENDIF
			ENDIF
			
			
			IF g_bValidatePlayersHelmetBerd
				// Make sure we have a hat that needs to shrink the hair
				IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_SHRINK_HAIR, ENUM_TO_INT(SHOP_PED_PROP))
					IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01
						GET_SHOP_PED_COMPONENT(HASH("DLC_MP_SMUG_M_BERD_10_0"), componentItem)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, componentItem.m_drawableIndex, componentItem.m_textureIndex)
						iDLCNameHashBERD = HASH("DLC_MP_SMUG_M_BERD_10_0")
					ELIF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
						GET_SHOP_PED_COMPONENT(HASH("DLC_MP_SMUG_F_BERD_10_0"), componentItem)
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, componentItem.m_drawableIndex, componentItem.m_textureIndex)
						iDLCNameHashBERD = HASH("DLC_MP_SMUG_F_BERD_10_0")
					ENDIF
					CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Hair shrink 1")
					bFinalize = TRUE
				ENDIF
				
				// Check for hair shrink.
				IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashBERD, DLC_RESTRICTION_TAG_HAIR_SHRINK, ENUM_TO_INT(SHOP_PED_COMPONENT))
					// Make sure we have a hat that needs to shrink the hair
					IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_SHRINK_HAIR, ENUM_TO_INT(SHOP_PED_PROP))
						// Safe
					ELSE
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
						RESET_PLAYER_HEAD_BLEND_TO_NORM(PLAYER_PED_ID())
						CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Shrink hair hat off remove hair shrink 1")
						bFinalize = TRUE
					ENDIF
					
				// Check for mask.
				ELIF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashBERD, DLC_RESTRICTION_TAG_BIKER_MASK, ENUM_TO_INT(SHOP_PED_COMPONENT))
					// Make sure we have the dome helmet
					PED_COMP_NAME_ENUM ePropItem = GET_PROP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD), GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD), ANCHOR_HEAD)
					IF IS_PED_WEARING_A_HELMET(PLAYER_PED_ID())
					AND NOT IS_ITEM_A_FULL_FACE_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, ePropItem, iDLCNameHash)
					AND NOT IS_ITEM_A_BULLETPROOF_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, ePropItem, iDLCNameHash)
					AND NOT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_BIKER_SKULL, ENUM_TO_INT(SHOP_PED_PROP))
						// Safe
					ELIF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashJBIB, DLC_RESTRICTION_TAG_HOOD_UP, ENUM_TO_INT(SHOP_PED_COMPONENT))
						// Safe
					ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
						// Safe
					ELIF GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
					OR (GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID()) AND GB_OUTFITS_BD_GET_BOSS_STYLE() != GB_BOSS_STYLE_NONE)
					OR (GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE) AND GB_OUTFITS_BD_GET_BOSS_STYLE() != GB_BOSS_STYLE_NONE)
						// Safe
					ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
					AND (GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) = ciGANGOPS_FLOW_MISSION_BCCUSTOM // barrage
					OR GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) = ciGANGOPS_FLOW_MISSION_STEALTHTANKS // khanjali
					OR GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) = ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE) // sub finale
						// Safe
					ELIF g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iroot_id_HASH_The_Flecca_Job
						// Safe
					ELSE
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
						RESET_PLAYER_HEAD_BLEND_TO_NORM(PLAYER_PED_ID())
						CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Dome helmet off remove mask 1")
						bFinalize = TRUE
					ENDIF
				
				ELIF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashBERD, DLC_RESTRICTION_TAG_FORCE_PROP, ENUM_TO_INT(SHOP_PED_COMPONENT))
					PED_COMP_NAME_ENUM ePropItem = GET_PROP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD), GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD), ANCHOR_HEAD)
					IF IS_ITEM_A_DOME_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, ePropItem, iDLCNameHash)
					OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_FITTED_CAP, ENUM_TO_INT(SHOP_PED_COMPONENT))
					OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_HOOD_HAT, ENUM_TO_INT(SHOP_PED_COMPONENT))
					OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_BOWLER_HAT, ENUM_TO_INT(SHOP_PED_PROP))
					OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_TOP_HAT, ENUM_TO_INT(SHOP_PED_PROP))
					OR (ePropItem >= PROPS_FMM_HAT_4_0 AND ePropItem <= PROPS_FMM_HAT_4_7)
					OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
					OR GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
					OR (GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID()) AND GB_OUTFITS_BD_GET_BOSS_STYLE() != GB_BOSS_STYLE_NONE)
					OR (GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE) AND GB_OUTFITS_BD_GET_BOSS_STYLE() != GB_BOSS_STYLE_NONE)
						// Safe
					ELIF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashBERD, DLC_RESTRICTION_TAG_HEADSCARF, ENUM_TO_INT(SHOP_PED_COMPONENT))
					AND (DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_HOOD_HAT, ENUM_TO_INT(SHOP_PED_PROP))
					OR iDLCNameHash = HASH("DLC_MP_APA_M_PHEAD_1_0")
					OR iDLCNameHash = HASH("DLC_MP_APA_M_PHEAD_1_1")
					OR iDLCNameHash = HASH("DLC_MP_APA_M_PHEAD_1_2")
					OR iDLCNameHash = HASH("DLC_MP_APA_F_PHEAD_1_0")
					OR iDLCNameHash = HASH("DLC_MP_APA_F_PHEAD_1_1")
					OR iDLCNameHash = HASH("DLC_MP_APA_F_PHEAD_1_2"))
						// Safe
					ELSE
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
						RESET_PLAYER_HEAD_BLEND_TO_NORM(PLAYER_PED_ID())
						CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Hat off remove face bandana 1")
						bFinalize = TRUE
					ENDIF
				ENDIF
				
				IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashBERD, DLC_RESTRICTION_TAG_HAZ_MASK, ENUM_TO_INT(SHOP_PED_COMPONENT))
					
					BOOL bHoodIsUp = FALSE
					BOOL bHoodIsTucked = FALSE
					IS_PED_WEARING_HOODED_JACKET(PLAYER_PED_ID(), bHoodIsUp, bHoodIsTucked)
					
					// Make sure we have a suitable top or helmet
					IF (IS_PED_WEARING_HAZ_TOP(PLAYER_PED_ID(), iDLCNameHashJBIB)
					OR bHoodIsUp
					OR (IS_PED_WEARING_A_HELMET(PLAYER_PED_ID()) AND NOT IS_PED_WEARING_A_FULL_FACE_HELMET(PLAYER_PED_ID())))
					AND NOT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashJBIB, DLC_RESTRICTION_TAG_FITTED_HOOD, ENUM_TO_INT(SHOP_PED_COMPONENT))
						// Safe
					ELSE
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
						RESET_PLAYER_HEAD_BLEND_TO_NORM(PLAYER_PED_ID())
						CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Helmet or hazmat suit off remove mask")
						bFinalize = TRUE
					ENDIF
				ENDIF
				
				// If the hat was removed make sure we pop the hood down.
				IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashJBIB, DLC_RESTRICTION_TAG_FITTED_HOOD, ENUM_TO_INT(SHOP_PED_COMPONENT))
					IF GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
						// skip
					ELSE
						CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Helmet or hat changed so update hood")
						UPDATE_PED_HOOD_STATE(PLAYER_PED_ID())
						bFinalize = TRUE
					ENDIF
				ENDIF
			
			ENDIF
			
			iHeadID = iTempIDCheck
		ENDIF
		
		// Process berd changes
		iTempIDCheck = ((GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD) * 1000)+(GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD)))
		IF iBerdID != iTempIDCheck
		OR IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_DO_GEAR_BERD_CHECK)
			
			IF IS_BIT_SET(MPGlobalsAmbience.iAmbBitSet, iABI_DO_GEAR_BERD_CHECK)
				IF NOT IS_PLAYER_SWITCH_IN_PROGRESS()
					CLEAR_BIT(MPGlobalsAmbience.iAmbBitSet, iABI_DO_GEAR_BERD_CHECK)
					CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - iABI_DO_GEAR_BERD_CHECK CLEARED")
				ELSE
					EXIT
				ENDIF
			ENDIF
			
			//REBREATHER
			//control SET_PED_DIES_IN_WATER based on if the player is wearing a rebreather
			bDisableRebreather = TRUE
			iDLCNameHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(PED_COMP_BERD), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD))
			IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_REBREATHER, ENUM_TO_INT(SHOP_PED_COMPONENT))
			AND IS_SAFE_TO_TURN_ON_REBREATHER()
				VECTOR vPlayerCoords = GET_PLAYER_COORDS(PLAYER_ID())
				IF vPlayerCoords.z > -140	//Depth check for Bug 1949575
					IF NOT IS_BIT_SET(iHeistManagmentBitset, HMB_REBREATHER_ON)
						CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Setting BEARD varition with restriction tag REBREATHER turned on - SET_PED_DIES_IN_WATER(PLAYER_ID(), FALSE)")
						SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), FALSE)
						SET_PED_DIES_IN_SINKING_VEHICLE(PLAYER_PED_ID(), FALSE) // url:bugstar:2345317
						SET_AUDIO_FLAG("SuppressPlayerScubaBreathing", FALSE)
						SET_BIT(iHeistManagmentBitset, HMB_REBREATHER_ON)
					ENDIF
					bDisableRebreather = FALSE
				ENDIF
			ENDIF	
			
			
			IF g_bValidatePlayersHelmetBerd
				
				iDLCNameHashHEAD = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(ANCHOR_HEAD), GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD), GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD))
				iDLCNameHashEYES = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(ANCHOR_EYES), GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_EYES), GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_EYES))
				iDLCNameHashJBIB = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(PED_COMP_JBIB), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB))
				// Check for hair shrink.
				IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_HAIR_SHRINK, ENUM_TO_INT(SHOP_PED_COMPONENT))
					// Make sure we have a hat that needs to shrink the hair
					IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashHEAD, DLC_RESTRICTION_TAG_SHRINK_HAIR, ENUM_TO_INT(SHOP_PED_PROP))
						// Safe
					ELSE
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
						RESET_PLAYER_HEAD_BLEND_TO_NORM(PLAYER_PED_ID())
						CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Shrink hair hat off remove hair shrink 2")
						bFinalize = TRUE
					ENDIF
				// Check for mask.
				ELIF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_BIKER_MASK, ENUM_TO_INT(SHOP_PED_COMPONENT))
					// Make sure we have the dome helmet
					PED_COMP_NAME_ENUM ePropItem = GET_PROP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD), GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD), ANCHOR_HEAD)
					IF IS_PED_WEARING_A_HELMET(PLAYER_PED_ID())
					AND NOT IS_ITEM_A_FULL_FACE_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, ePropItem, iDLCNameHashHEAD)
					AND NOT IS_ITEM_A_BULLETPROOF_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, ePropItem, iDLCNameHashHEAD)
					AND NOT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashHEAD, DLC_RESTRICTION_TAG_BIKER_SKULL, ENUM_TO_INT(SHOP_PED_PROP))
						// Safe
					ELIF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashJBIB, DLC_RESTRICTION_TAG_HOOD_UP, ENUM_TO_INT(SHOP_PED_COMPONENT))
						// Safe
					ELIF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
						// Safe
					ELIF GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
					OR (GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID()) AND GB_OUTFITS_BD_GET_BOSS_STYLE() != GB_BOSS_STYLE_NONE)
					OR (GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE) AND GB_OUTFITS_BD_GET_BOSS_STYLE() != GB_BOSS_STYLE_NONE)
						// Safe
					ELIF GANGOPS_FLOW_IS_CURRENT_MISSION_GANGOPS_FLOW()
					AND (GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) = ciGANGOPS_FLOW_MISSION_BCCUSTOM // barrage
					OR GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) = ciGANGOPS_FLOW_MISSION_STEALTHTANKS // khanjali
					OR GANGOPS_FLOW_GET_GANG_OPS_MISSION_CONST_FROM_ROOT_ID(g_FMMC_STRUCT.iRootContentIDHash) = ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE) // sub finale
						// Safe
					ELIF g_FMMC_STRUCT.iRootContentIDHash = g_sMPTunables.iroot_id_HASH_The_Flecca_Job
						// Safe
					ELSE
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
						RESET_PLAYER_HEAD_BLEND_TO_NORM(PLAYER_PED_ID())
						CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Dome helmet off remove mask 2")
						bFinalize = TRUE
					ENDIF
				ELIF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_FORCE_PROP, ENUM_TO_INT(SHOP_PED_COMPONENT))
					PED_COMP_NAME_ENUM ePropItem = GET_PROP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD), GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD), ANCHOR_HEAD)
					IF IS_ITEM_A_DOME_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, ePropItem, iDLCNameHashHEAD)
					OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashHEAD, DLC_RESTRICTION_TAG_FITTED_CAP, ENUM_TO_INT(SHOP_PED_COMPONENT))
					OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashHEAD, DLC_RESTRICTION_TAG_HOOD_HAT, ENUM_TO_INT(SHOP_PED_COMPONENT))
					OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashHEAD, DLC_RESTRICTION_TAG_BOWLER_HAT, ENUM_TO_INT(SHOP_PED_PROP))
					OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashHEAD, DLC_RESTRICTION_TAG_TOP_HAT, ENUM_TO_INT(SHOP_PED_PROP))
					OR (ePropItem >= PROPS_FMM_HAT_4_0 AND ePropItem <= PROPS_FMM_HAT_4_7)
					OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
					OR GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
					OR (GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID()) AND GB_OUTFITS_BD_GET_BOSS_STYLE() != GB_BOSS_STYLE_NONE)
					OR (GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE) AND GB_OUTFITS_BD_GET_BOSS_STYLE() != GB_BOSS_STYLE_NONE)
					OR DOES_DLC_ITEM_FORCE_THIS_ITEM(iDLCNameHash, COMP_TYPE_PROPS, ePropItem, iDLCNameHashHEAD)
						// Safe
					ELIF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_HEADSCARF, ENUM_TO_INT(SHOP_PED_COMPONENT))
					AND (DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashHEAD, DLC_RESTRICTION_TAG_HOOD_HAT, ENUM_TO_INT(SHOP_PED_PROP))
					OR iDLCNameHashHEAD = HASH("DLC_MP_APA_M_PHEAD_1_0")
					OR iDLCNameHashHEAD = HASH("DLC_MP_APA_M_PHEAD_1_1")
					OR iDLCNameHashHEAD = HASH("DLC_MP_APA_M_PHEAD_1_2")
					OR iDLCNameHashHEAD = HASH("DLC_MP_APA_F_PHEAD_1_0")
					OR iDLCNameHashHEAD = HASH("DLC_MP_APA_F_PHEAD_1_1")
					OR iDLCNameHashHEAD = HASH("DLC_MP_APA_F_PHEAD_1_2"))
						// Safe
					ELSE
						SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
						RESET_PLAYER_HEAD_BLEND_TO_NORM(PLAYER_PED_ID())
						CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Hat off remove face bandana 2")
						bFinalize = TRUE
					ENDIF
				ENDIF
				
				// If the mask was removed make sure we pop the hood down.
				IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashJBIB, DLC_RESTRICTION_TAG_FITTED_HOOD, ENUM_TO_INT(SHOP_PED_COMPONENT))
					IF GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
						// skip
					ELSE
						CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Mask changed so update hood")
						UPDATE_PED_HOOD_STATE(PLAYER_PED_ID())
						bFinalize = TRUE
					ENDIF
				ENDIF
				
				// Pogo glasses need to be tied to the pogo mask.
				IF iDLCNameHash = HASH("DLC_MP_ARENA_M_BERD_13_0")
				OR iDLCNameHash = HASH("DLC_MP_ARENA_F_BERD_13_0")
					// Force glasses on
					IF NOT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashEYES, DLC_RESTRICTION_TAG_ARENA_DRAW_0, ENUM_TO_INT(SHOP_PED_PROP))
						scrShopPedProp propItem
						INIT_SHOP_PED_PROP(propItem)
						IF iDLCNameHash = HASH("DLC_MP_ARENA_M_BERD_13_0")
							GET_SHOP_PED_QUERY_PROP(HASH("DLC_MP_ARENA_M_PEYES_0_0"), propItem)
						ELSE
							GET_SHOP_PED_QUERY_PROP(HASH("DLC_MP_ARENA_F_PEYES_0_0"), propItem)
						ENDIF
						SET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_EYES, propItem.m_propIndex, propItem.m_textureIndex)
						bFinalize = TRUE
					ENDIF
				ELSE
					// Force glasses off
					IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHashEYES, DLC_RESTRICTION_TAG_ARENA_DRAW_0, ENUM_TO_INT(SHOP_PED_PROP))
						CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_EYES)
						bFinalize = TRUE
					ENDIF
				ENDIF
				
				IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_FITTED_HOOD, ENUM_TO_INT(SHOP_PED_COMPONENT))
					IF GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
						// skip
					ELSE
						CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Mask changed so update hood")
						UPDATE_PED_HOOD_STATE(PLAYER_PED_ID())
						bFinalize = TRUE
					ENDIF
				ENDIF
				
			ENDIF
			
			iBerdID = iTempIDCheck
		ENDIF
		
		IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), FALSE) != bInVehicle
			IF GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
				// Only do stuff if you're a boss wearing the gangmaster outfit
				IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
					IF GB_OUTFITS_GET_BOSS_OUTFIT_FROM_STYLE(GB_OUTFITS_BD_GET_BOSS_STYLE(), PLAYER_ID()) = OUTFIT_GUNR_CEO_GANGMASTER_0
						bInVehicle = !bInVehicle
						IF bInVehicle
							CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Vehicle state changed so update hood - CEO Gangmaster")
							UPDATE_PED_HOOD_STATE(PLAYER_PED_ID())
							g_sMagnateOutfitStruct.bPerformVehicleRefresh = TRUE
							bFinalize = TRUE
						ENDIF
					ENDIF
				ENDIF
			ELSE
				bInVehicle = !bInVehicle
				IF bInVehicle
					CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Vehicle state changed so update hood")
					UPDATE_PED_HOOD_STATE(PLAYER_PED_ID())
					bFinalize = TRUE
				ENDIF
			ENDIF
		ENDIF
		
		// Process top changes
		// Using the wardrobe?
		IF IS_PLAYER_CHANGING_CLOTHES()
		AND NOT IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
			iTempIDCheck = ((g_sLastStoredClothesInWardrobe.iDrawableVariation[PED_COMP_JBIB] * 1000)+(g_sLastStoredClothesInWardrobe.iTextureVariation[PED_COMP_JBIB]))
			iTempIDCheck2 = ((g_sLastStoredClothesInWardrobe.iDrawableVariation[PED_COMP_SPECIAL] * 1000)+(g_sLastStoredClothesInWardrobe.iTextureVariation[PED_COMP_SPECIAL]))
		ELSE
			iTempIDCheck = ((GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB) * 1000)+(GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB)))
			iTempIDCheck2 = ((GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL) * 1000)+(GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL)))
		ENDIF
		
		IF iJbibID != iTempIDCheck
		OR iSpecialID != iTempIDCheck2
			
			IF GB_OUTFITS_BD_GET_BOSS_STYLE() = GB_BOSS_STYLE_NONE
				// Track last top player wears so we can re-apply it when we go from a closed to open jacket.
				INT iDLCJbibHash = GET_HASH_NAME_FOR_COMPONENT( NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(PED_COMP_JBIB), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB))
				
				PED_COMP_NAME_ENUM eCurrentJBIB = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_JBIB)
				PED_COMP_NAME_ENUM eCurrentSpecial = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL)
				
				IF IS_JBIB_COMPONENT_A_JACKET(GET_ENTITY_MODEL(PLAYER_PED_ID()), eCurrentJBIB, iDLCJbibHash)
					IF NOT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCJbibHash, DLC_RESTRICTION_TAG_JACKET_ONLY, ENUM_TO_INT(SHOP_PED_COMPONENT))
					OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCJbibHash, DLC_RESTRICTION_TAG_BIKER_VEST, ENUM_TO_INT(SHOP_PED_COMPONENT))
						IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01
						AND (eCurrentSpecial = SPECIAL_FMF_2_0 OR eCurrentSpecial = SPECIAL_FMF_3_0 OR eCurrentSpecial = SPECIAL_FMF_14_0)
							//Avoid saving the dummy items.
						ELSE
							SET_LAST_UNDER_JACKET_ITEM(COMP_TYPE_SPECIAL, eCurrentSpecial, GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL), GET_MP_INT_CHARACTER_STAT(MP_STAT_TSHIRT_DECAL_HASH))
						ENDIF
					ENDIF
					
				ELIF NOT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCJbibHash, DLC_RESTRICTION_TAG_SWEAT_VEST, ENUM_TO_INT(SHOP_PED_COMPONENT))
				AND NOT DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCJbibHash, DLC_RESTRICTION_TAG_BASIC_VEST, ENUM_TO_INT(SHOP_PED_COMPONENT))
					SET_LAST_UNDER_JACKET_ITEM(COMP_TYPE_JBIB, eCurrentJBIB, GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB), GET_MP_INT_CHARACTER_STAT(MP_STAT_TSHIRT_DECAL_HASH))
				ELSE
					SET_LAST_UNDER_JACKET_ITEM(COMP_TYPE_JBIB, DUMMY_PED_COMP, 0, 0)
				ENDIF
				
				// Fix for url:bugstar:3506519 - Female Clothing - Hairstyles - Multiple Hairstyles are clipping through the local player's hood when the hood option is set to 'Up'
				// Reapply berd whenever we switch jbib to fix hair.
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD))
			ENDIF
			
			sCachedClothingStates.bPlayerWearingFittedHoodCache = IS_PED_WEARING_FITTED_HOOD(PLAYER_PED_ID())
			
			iJbibID = iTempIDCheck
			iSpecialID = iTempIDCheck2
		ENDIF
			
		
		IF bFinalize
		AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(PLAYER_PED_ID())
		AND HAS_PED_HEAD_BLEND_FINISHED(PLAYER_PED_ID())
			FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
			CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - Finalize head blend for outfit change.")
			bFinalize = FALSE
		ENDIF
		
		//NIGHTVISION
		//control SET_NIGHTVISION based on if the player is wearing the night vision goggles
		//only process when the state changes!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
		MANAGE_VISUALAID_CONTENT(sVisualAidData, iBerdID, iHeadID)
		
		IF (!g_bDisplayed_Night_Vision_Controller_HELP_CACT)
		AND (GET_PLAYER_VISUAL_AID_OVERRIDE() = VISUALAID_DEFAULT)
			INT iHELP_CACT_display_count = GET_PACKED_STAT_INT(PACKED_MP_INT_HELP_CACT_DISPLAYED)
			
			IF iHELP_CACT_display_count >= 3
				g_bDisplayed_Night_Vision_Controller_HELP_CACT = TRUE
			ELSE
				BOOL bSafe_to_show_help = TRUE
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				OR IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				OR IS_SCREEN_FADED_OUT()
				OR IS_CUSTOM_MENU_ON_SCREEN()
				OR IS_FM_MISSION_LAUNCH_IN_PROGRESS()
				OR IS_PAUSE_MENU_ACTIVE()
				OR IS_POST_MISSION_CLEANUP_REQUIRED_OR_ACTIVE(PLAYER_ID())
				OR NOT IS_SKYSWOOP_AT_GROUND()	
				OR SHOULD_TRANSITION_SESSIONS_HIDE_RADAR_UNTILL_LAUNCHED()
				OR IS_TRANSITION_SESSION_LAUNCHING()
				OR IS_PLAYER_IN_CORONA()
				OR NETWORK_IS_IN_MP_CUTSCENE()
				OR FM_EVENT_IS_PLAYER_ON_ANY_FM_EVENT(PLAYER_ID())
				OR NOT IS_NET_PLAYER_OK(PLAYER_ID())
				OR IS_ANY_BIG_MESSAGE_BEING_DISPLAYED()
				OR NETWORK_IS_ACTIVITY_SESSION()
				OR IS_PHONE_ONSCREEN()
				OR IS_POST_MISSION_SCENE_ACTIVE()
					bSafe_to_show_help = FALSE
				ENDIF
			
				IF bSafe_to_show_help
					iDLCNameHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(PED_COMP_BERD), GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD))
					
					IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_NIGHT_VISION, ENUM_TO_INT(SHOP_PED_COMPONENT))
					OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_THERMAL_VISION, ENUM_TO_INT(SHOP_PED_COMPONENT))
						iDLCNameHashHEAD = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(ANCHOR_HEAD), GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD), GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD))
						IF GET_SHOP_PED_APPAREL_VARIANT_PROP_COUNT(iDLCNameHashHEAD) <= 0
							//
							
							IF IS_NIGHTVISION_DEACTIVATED_VIA_PIM()
								PRINT_HELP("HELP_CACT")		//"Go to the accessories section of the Interaction Menu to activate the nightvision mode of your mask."
							ELSE
								PRINT_HELP("HELP_CDAC")		//"Go to the accessories section of the Interaction Menu to deactivate the nightvision mode of your mask."
							ENDIF
							SET_PACKED_STAT_INT(PACKED_MP_INT_HELP_CACT_DISPLAYED, iHELP_CACT_display_count+1)
							g_bDisplayed_Night_Vision_Controller_HELP_CACT = TRUE
						ENDIF
					ENDIF
					
				ENDIF
			ENDIF
		ENDIF
		
		// Prevent mouth from moving when wearing gas mask.
		IF DOES_CURRENT_PED_COMPONENT_HAVE_RESTRICTION_TAG(PLAYER_PED_ID(), PED_COMP_BERD, DLC_RESTRICTION_TAG_GAS_MASK)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableVoiceDrivenMouthMovement, TRUE)
		ENDIF
		
		// Turn rebreather off if required
		IF IS_BIT_SET(iHeistManagmentBitset, HMB_REBREATHER_ON)
			IF bDisableRebreather
			OR NOT IS_SAFE_TO_TURN_ON_REBREATHER()
				CPRINTLN(DEBUG_PED_COMP, "MANAGE_HEIST_CONTENT - REBREATHER turned off")
				SET_PED_DIES_IN_WATER(PLAYER_PED_ID(), TRUE)
				SET_PED_DIES_IN_SINKING_VEHICLE(PLAYER_PED_ID(), TRUE)
				SET_AUDIO_FLAG("SuppressPlayerScubaBreathing", TRUE)
				CLEAR_BIT(iHeistManagmentBitset, HMB_REBREATHER_ON)
			ENDIF
		ENDIF
		
		IF bSafeToProcessMPChecks
			MAINTAIN_REBREATHER_AIR()
		ENDIF
	ENDIF
	
	// If we change slot we need to clear some flags
	IF NETWORK_IS_GAME_IN_PROGRESS()
		INT iSlot = GET_ACTIVE_CHARACTER_SLOT()
		IF iCharacterSlotID	!= iSlot
			iCharacterSlotID = iSlot
			SET_LAST_UNDER_JACKET_ITEM(COMP_TYPE_JBIB, DUMMY_PED_COMP, 0, 0)
		ENDIF
	ENDIF
	
ENDPROC

INT iTankPTFXStage
INT iPlayerTankCheck
INT iPlayersInTanks
TIME_DATATYPE tdTankTimer
BOOL bTankTimerSet
BOOL bTankPTFXSet

PROC MANAGE_TANK_PTFX()

	IF iTankPTFXStage > 0
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			iTankPTFXStage = 99
		ENDIF
	ENDIF

	SWITCH iTankPTFXStage
		CASE 0
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
				PRINTLN("MANAGE_TANK_PTFX - Player is in freemode, start checking tanks.")
				iTankPTFXStage++
			ENDIF
		BREAK
		
		CASE 1
			PED_INDEX pedID
			VEHICLE_INDEX vehID
			PLAYER_INDEX playerID
			
			playerID = INT_TO_PLAYERINDEX(iPlayerTankCheck)
			IF IS_NET_PLAYER_OK(playerID)
				pedID = GET_PLAYER_PED(playerID)
				IF NOT IS_PED_INJURED(pedID)
				AND IS_PED_IN_ANY_VEHICLE(pedID)
					vehID = GET_VEHICLE_PED_IS_IN(pedID)
					IF DOES_ENTITY_EXIST(vehID)
					AND IS_VEHICLE_DRIVEABLE(vehID)
					AND GET_PED_IN_VEHICLE_SEAT(vehID, VS_DRIVER) = pedID
					AND GET_ENTITY_MODEL(vehID) = RHINO
						iPlayersInTanks++
					ENDIF
				ENDIF
			ENDIF
			
			iPlayerTankCheck++
			
			// Check if 4 or more players are in tanks.
			IF iPlayerTankCheck >= NUM_NETWORK_PLAYERS
				IF iPlayersInTanks >= 4
				
					IF NOT bTankTimerSet
						PRINTLN("MANAGE_TANK_PTFX - ", iPlayersInTanks, " players are driving tanks - switch to cheap ptfx")
					ENDIF
					
					bTankTimerSet = TRUE
					tdTankTimer = GET_NETWORK_TIME()
				ENDIF
				
				// Reset counters
				iPlayerTankCheck = 0
				iPlayersInTanks = 0
			ENDIF
			
			IF (bTankTimerSet AND ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdTankTimer)) < 300000) // 5 minutes
				IF NOT bTankPTFXSet
					REQUEST_NAMED_PTFX_ASSET("scr_mp_tankbattle")
					IF HAS_NAMED_PTFX_ASSET_LOADED("scr_mp_tankbattle")
						PRINTLN("MANAGE_TANK_PTFX - switched to chearp ptfx - exp_grd_tankshell_mp")
						SET_PARTICLE_FX_OVERRIDE("exp_grd_tankshell", "exp_grd_tankshell_mp")
						bTankPTFXSet = TRUE
					ENDIF
				ENDIF
				
			ELIF bTankTimerSet
				PRINTLN("MANAGE_TANK_PTFX - switching back to standard ptfx - exp_grd_tankshell")
				iTankPTFXStage = 99 // Cleanup
			ENDIF
		BREAK
		
		CASE 99
			// Cleanup
			IF bTankPTFXSet
				RESET_PARTICLE_FX_OVERRIDE("exp_grd_tankshell")
			ENDIF
			REMOVE_NAMED_PTFX_ASSET("scr_mp_tankbattle")
			iPlayerTankCheck = 0
			iPlayersInTanks = 0
			bTankTimerSet = FALSE
			bTankPTFXSet = FALSE
			iTankPTFXStage = 0
		BREAK
		
	ENDSWITCH
ENDPROC


INT iAirBombPTFXStage
INT iPlayerAirBombCheck
TIME_DATATYPE tdAirBombTimer
BOOL bAirBombTimerSet
BOOL bAirBombPTFXSet
BOOL bPlayerInAirBombVeh

PROC MANAGE_AIR_BOMB_PTFX()
	IF iAirBombPTFXStage > 0
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			iAirBombPTFXStage = 99
		ENDIF
	ENDIF
	
	SWITCH iAirBombPTFXStage
		CASE 0
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
				PRINTLN("MANAGE_AIR_BOMB_PTFX - Player is in freemode, start checking for air bombs.")
				iAirBombPTFXStage++
			ENDIF
		BREAK
		
		CASE 1
			PED_INDEX pedID
			VEHICLE_INDEX vehID
			PLAYER_INDEX playerID
			
			playerID = INT_TO_PLAYERINDEX(iPlayerAirBombCheck)
			
			IF IS_NET_PLAYER_OK(playerID)
				pedID = GET_PLAYER_PED(playerID)
				
				IF NOT IS_PED_INJURED(pedID)
				AND IS_PED_IN_ANY_VEHICLE(pedID)
					vehID = GET_VEHICLE_PED_IS_IN(pedID)
					
					IF DOES_ENTITY_EXIST(vehID)
					AND IS_VEHICLE_DRIVEABLE(vehID)
					AND CAN_VEHICLE_HAVE_AIR_BOMBS(vehID)
						bPlayerInAirBombVeh = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			iPlayerAirBombCheck++
			
			IF iPlayerAirBombCheck >= NUM_NETWORK_PLAYERS
				iPlayerAirBombCheck = 0
			ENDIF
			
			IF bPlayerInAirBombVeh
				IF NOT bAirBombTimerSet
					PRINTLN("MANAGE_AIR_BOMB_PTFX - a player is in an Air Bomb vehicle - load ptfx - scr_weap_bombs")
				ENDIF
				
				bAirBombTimerSet = TRUE
				tdAirBombTimer = GET_NETWORK_TIME()
			ENDIF
			
			IF (bAirBombTimerSet AND ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdAirBombTimer)) < 60000)
				IF NOT bAirBombPTFXSet
					REQUEST_NAMED_PTFX_ASSET("scr_weap_bombs")
					
					IF HAS_NAMED_PTFX_ASSET_LOADED("scr_weap_bombs")
						bAirBombPTFXSet = TRUE
					ENDIF
				ENDIF
			ELIF bAirBombTimerSet
				PRINTLN("MANAGE_AIR_BOMB_PTFX - releasing ptfx - scr_weap_bombs")
				
				iAirBombPTFXStage = 99
			ENDIF
		BREAK
		
		CASE 99
			REMOVE_NAMED_PTFX_ASSET("scr_weap_bombs")
			iPlayerAirBombCheck = 0
			bPlayerInAirBombVeh = FALSE
			bAirBombTimerSet = FALSE
			bAirBombPTFXSet = FALSE
			iAirBombPTFXStage = 0
		BREAK
	ENDSWITCH
ENDPROC


INT iOrbitalCannonPTFXStage
INT iPlayerOrbitalCannonCheck
TIME_DATATYPE tdOrbitalCannonTimer
BOOL bOrbitalCannonTimerSet
BOOL bOrbitalCannonPTFXSet
BOOL bPlayerUsingOrbitalCannon

PROC MANAGE_ORBITAL_CANNON_PTFX()
	IF NOT NETWORK_IS_SIGNED_ONLINE()
	AND g_iOrbitalCannonRefundBS != 0
		g_iOrbitalCannonRefundBS = 0
		
		PRINTLN("[ORBITAL_CANNON] - Not connected, resetting g_iOrbitalCannonRefundBS")
	ENDIF
	
	IF iOrbitalCannonPTFXStage > 0
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			iOrbitalCannonPTFXStage = 99
		ENDIF
	ENDIF
	
	SWITCH iOrbitalCannonPTFXStage
		CASE 0
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
				PRINTLN("MANAGE_ORBITAL_CANNON_PTFX - Player is in freemode, start checking for Orbital Cannon.")
				iOrbitalCannonPTFXStage++
			ENDIF
		BREAK
		
		CASE 1
			PLAYER_INDEX playerID
			
			playerID = INT_TO_PLAYERINDEX(iPlayerOrbitalCannonCheck)
			
			IF IS_NET_PLAYER_OK(playerID)
				IF IS_BIT_SET(GlobalPlayerBD[NATIVE_TO_INT(playerID)].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_USING_CANNON)
					bPlayerUsingOrbitalCannon = TRUE
				ENDIF
			ENDIF
			
			iPlayerOrbitalCannonCheck++
			
			IF iPlayerOrbitalCannonCheck >= NUM_NETWORK_PLAYERS
				iPlayerOrbitalCannonCheck = 0
			ENDIF
			
			IF bPlayerUsingOrbitalCannon
				IF NOT bOrbitalCannonTimerSet
					PRINTLN("MANAGE_ORBITAL_CANNON_PTFX - a player is using the Orbital Cannon - load ptfx - scr_xm_orbital")
				ENDIF
				
				bOrbitalCannonTimerSet = TRUE
				tdOrbitalCannonTimer = GET_NETWORK_TIME()
			ENDIF
			
			IF (bOrbitalCannonTimerSet AND ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdOrbitalCannonTimer)) < 60000)
				IF NOT bOrbitalCannonPTFXSet
					REQUEST_NAMED_PTFX_ASSET("scr_xm_orbital")
					
					IF HAS_NAMED_PTFX_ASSET_LOADED("scr_xm_orbital")
						bOrbitalCannonPTFXSet = TRUE
					ENDIF
				ENDIF
			ELIF bOrbitalCannonTimerSet
				PRINTLN("MANAGE_ORBITAL_CANNON_PTFX - releasing ptfx - scr_xm_orbital")
				
				iOrbitalCannonPTFXStage = 99
			ENDIF
		BREAK
		
		CASE 99
			REMOVE_NAMED_PTFX_ASSET("scr_xm_orbital")
			iPlayerOrbitalCannonCheck = 0
			bPlayerUsingOrbitalCannon = FALSE
			bOrbitalCannonTimerSet = FALSE
			bOrbitalCannonPTFXSet = FALSE
			iOrbitalCannonPTFXStage = 0
		BREAK
	ENDSWITCH
ENDPROC

#IF NOT IS_NEXTGEN_BUILD
	
	BOOL bRPGTrails_Loading
	BOOL bRPGTrails_Applied

	PROC MANAGE_RPG_TRAIL_PTFX()
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			
			IF NOT bRPGTrails_Applied
				
				IF NOT bRPGTrails_Loading
					
					bRPGTrails_Loading = TRUE
					
					PRINTLN("MANAGE_RPG_TRAIL_PTFX - Player has entered freemode, start loading scr_biolab_lazer asset for RPG trail replacement")
				ENDIF
				
				REQUEST_NAMED_PTFX_ASSET("scr_biolab_lazer")
				
				IF HAS_NAMED_PTFX_ASSET_LOADED("scr_biolab_lazer")
					
					bRPGTrails_Applied = TRUE
					
					PRINTLN("MANAGE_RPG_TRAIL_PTFX - applying particle effect override for proj_rpg_trail, override with proj_rpg_lazer_trail")
					SET_PARTICLE_FX_OVERRIDE("proj_rpg_trail", "proj_rpg_lazer_trail")
				ENDIF
				
			ENDIF
		ELSE
			IF bRPGTrails_Loading
				
				bRPGTrails_Loading = FALSE
				
				PRINTLN("MANAGE_RPG_TRAIL_PTFX - Player has left freemode, remove scr_biolab_lazer asset")
				REMOVE_NAMED_PTFX_ASSET("scr_biolab_lazer")
				
				IF bRPGTrails_Applied
					
					bRPGTrails_Applied = FALSE
					
					PRINTLN("MANAGE_RPG_TRAIL_PTFX - Player has left freemode, reset particle effect override for proj_rpg_trail")
					RESET_PARTICLE_FX_OVERRIDE("proj_rpg_trail")
				ENDIF
			ENDIF
		ENDIF
		
	ENDPROC
	
#ENDIF


PROC PROCESS_CACHED_DLC_DATA()
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	
	g_bPedComponentGenderIsMale = (GET_PACKED_STAT_INT(PACKED_CHAR_PICTURE, g_iPedComponentSlot) = 0)
	
ENDPROC

enumCharacterList eReserveChutePed = NO_CHARACTER
BOOL bReserveChuteState // Cache the reserve chute state that code holds

INT iChuteCheck_iCurrentCheckPoint = 0
BOOL bChuteCheck_OnMission = FALSE
BOOL bHashReserveChuteAtCheckpoint[3]
BOOL bHashReserveChuteAtMissionStart[3]


PROC MANAGE_PURCHASED_PARACHUTES()
	IF NOT IS_PLAYER_PED_PLAYABLE(eCurrentPed)
		EXIT
	ENDIF
	
	// Ped has changed so cache the new chute states
	IF eReserveChutePed != eCurrentPed
		eReserveChutePed = eCurrentPed
		bReserveChuteState = GET_PLAYER_HAS_RESERVE_PARACHUTE(PLAYER_ID())
	ENDIF
	
	IF GET_PLAYER_HAS_RESERVE_PARACHUTE(PLAYER_ID())
		// Player has a chute and never used to have one...
		IF NOT bReserveChuteState
			IF NOT IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(eReserveChutePed))
				PRINTLN("MANAGE_PURCHASED_PARACHUTES - Player now has a reserve chute, eReserveChutePed = ", GET_PLAYER_PED_STRING(eReserveChutePed))
				SET_BIT(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(eReserveChutePed))
			ENDIF
			bReserveChuteState = TRUE
		ENDIF
	
	// If the player doesn't have a reserve chute give him one or clear the save data
	ELSE
		// Player doesn't have a chute but used to have one...
		IF bReserveChuteState
			IF IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(eReserveChutePed))
				PRINTLN("MANAGE_PURCHASED_PARACHUTES - Player no longer has a reserve chute, eReserveChutePed = ", GET_PLAYER_PED_STRING(eReserveChutePed))
				CLEAR_BIT(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(eReserveChutePed))
			ENDIF
			
		// Player doesn't have a chute and has never had one...
		ELSE
			IF IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(eReserveChutePed))
				PRINTLN("MANAGE_PURCHASED_PARACHUTES - Giving reserve chute to player, eReserveChutePed = ", GET_PLAYER_PED_STRING(eReserveChutePed))
				SET_PLAYER_HAS_RESERVE_PARACHUTE(PLAYER_ID())
			ENDIF
		ENDIF
	ENDIF
	
	// Checkpoint restore
	IF g_replay.replayStageID = RS_ACCEPTING
		IF bHashReserveChuteAtCheckpoint[CHAR_MICHAEL]
		AND NOT IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_MICHAEL))
			PRINTLN("MANAGE_PURCHASED_PARACHUTES - Setting reserve chute flag for checkpoint, eReserveChutePed = ", GET_PLAYER_PED_STRING(CHAR_MICHAEL))
			SET_BIT(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_MICHAEL))
			bReserveChuteState = FALSE
		ENDIF
		
		IF bHashReserveChuteAtCheckpoint[CHAR_FRANKLIN]
		AND NOT IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_FRANKLIN))
			PRINTLN("MANAGE_PURCHASED_PARACHUTES - Setting reserve chute flag for checkpoint, eReserveChutePed = ", GET_PLAYER_PED_STRING(CHAR_FRANKLIN))
			SET_BIT(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_FRANKLIN))
			bReserveChuteState = FALSE
		ENDIF
		
		IF bHashReserveChuteAtCheckpoint[CHAR_TREVOR]
		AND NOT IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_TREVOR))
			PRINTLN("MANAGE_PURCHASED_PARACHUTES - Setting reserve chute flag for checkpoint, eReserveChutePed = ", GET_PLAYER_PED_STRING(CHAR_TREVOR))
			SET_BIT(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_TREVOR))
			bReserveChuteState = FALSE
		ENDIF
	ELIF g_replay.replayStageID = RS_REJECTING
		IF bHashReserveChuteAtMissionStart[CHAR_MICHAEL]
		AND NOT IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_MICHAEL))
			PRINTLN("MANAGE_PURCHASED_PARACHUTES - Setting reserve chute flag for mission reject, eReserveChutePed = ", GET_PLAYER_PED_STRING(CHAR_MICHAEL))
			SET_BIT(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_MICHAEL))
			bReserveChuteState = FALSE
		ENDIF
		
		IF bHashReserveChuteAtMissionStart[CHAR_FRANKLIN]
		AND NOT IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_FRANKLIN))
			PRINTLN("MANAGE_PURCHASED_PARACHUTES - Setting reserve chute flag for mission reject, eReserveChutePed = ", GET_PLAYER_PED_STRING(CHAR_FRANKLIN))
			SET_BIT(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_FRANKLIN))
			bReserveChuteState = FALSE
		ENDIF
		
		IF bHashReserveChuteAtMissionStart[CHAR_TREVOR]
		AND NOT IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_TREVOR))
			PRINTLN("MANAGE_PURCHASED_PARACHUTES - Setting reserve chute flag for mission reject, eReserveChutePed = ", GET_PLAYER_PED_STRING(CHAR_TREVOR))
			SET_BIT(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_TREVOR))
			bReserveChuteState = FALSE
		ENDIF
	ELSE
		// Store reserve chute state when checkpoint changes
		IF g_replayMissionStage > iChuteCheck_iCurrentCheckPoint
		OR g_replayMissionStage = 0
			bHashReserveChuteAtCheckpoint[CHAR_MICHAEL] = IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_MICHAEL))
			bHashReserveChuteAtCheckpoint[CHAR_FRANKLIN] = IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_FRANKLIN))
			bHashReserveChuteAtCheckpoint[CHAR_TREVOR] = IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_TREVOR))
			iChuteCheck_iCurrentCheckPoint = g_replayMissionStage
		ENDIF
		
		// Store reserve chute state when mission starts
		IF bPlayerOnSPMission != bChuteCheck_OnMission
			bHashReserveChuteAtMissionStart[CHAR_MICHAEL] = IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_MICHAEL))
			bHashReserveChuteAtMissionStart[CHAR_FRANKLIN] = IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_FRANKLIN))
			bHashReserveChuteAtMissionStart[CHAR_TREVOR] = IS_BIT_SET(g_savedGlobals.sPlayerData.sInfo.iUnlockedCarMods[4], 28+ENUM_TO_INT(CHAR_TREVOR))
			bChuteCheck_OnMission = bPlayerOnSPMission
		ENDIF
	ENDIF
	
	
	//-	When mission starts cache if we have a reserve shoot in g_bLastMissionStartHadReserve.
	//-	When checkpoint is hit (g_replayMissionStage gets bigger) cache if we have a reserve shoot in g_bLastMissionCheckpointHadReserve.
	//-	When g_replay.replayStageID = RS_ACCEPTING restore based on bLastMissionCheckpointHadReserve.
	//-	When g_replay.replayStageID = RS_REJECTING restore based on bLastMissionStartHadReserve.

	
	
	
ENDPROC

INT iCrewEmblemStage
TIME_DATATYPE tdEmblemTime
INT iCrewID
NETWORK_CLAN_DESC memberInfo
GAMER_HANDLE gamerHandle
INT iCharacterSlot = -1
PROC MANAGE_CREW_EMBLEM_CHECKS()
	IF NOT g_bPlayerHasValidCrewEmblem
	
		// Bail checks
		IF iCrewEmblemStage > 0
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			OR NOT IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
			OR NOT NETWORK_CLAN_SERVICE_IS_VALID()
			OR iCharacterSlot != GET_ACTIVE_CHARACTER_SLOT()
				IF iCrewEmblemStage = 2
					NETWORK_CLAN_RELEASE_EMBLEM(iCrewID)
				ENDIF
				iCrewEmblemStage = 0
				PRINTLN("MANAGE_CREW_EMBLEM_CHECKS - Changed player or clan - reset")
			ENDIF
		ENDIF
		
		SWITCH iCrewEmblemStage
			CASE 0
				IF NETWORK_IS_GAME_IN_PROGRESS()
				AND IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
				AND IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
				AND NETWORK_CLAN_SERVICE_IS_VALID()
					iCharacterSlot = GET_ACTIVE_CHARACTER_SLOT()
					gamerHandle = GET_GAMER_HANDLE_PLAYER(PLAYER_ID())
					IF NETWORK_CLAN_PLAYER_IS_ACTIVE(gamerHandle)
					AND NETWORK_CLAN_PLAYER_GET_DESC(memberInfo, SIZE_OF(memberInfo), gamerHandle)
						iCrewID = memberInfo.Id
						iCrewEmblemStage++
						PRINTLN("MANAGE_CREW_EMBLEM_CHECKS - Got crew ID: ", iCrewID)
					ELSE
						iCrewEmblemStage = 98
						PRINTLN("MANAGE_CREW_EMBLEM_CHECKS - Failed to obtain crew ID")
					ENDIF
				ENDIF
			BREAK
			CASE 1
				IF NETWORK_CLAN_REQUEST_EMBLEM(iCrewID)
					iCrewEmblemStage++
					tdEmblemTime = GET_NETWORK_TIME()
					PRINTLN("MANAGE_CREW_EMBLEM_CHECKS - Request for emblem started")
				ELSE
					iCrewEmblemStage = 98
					PRINTLN("MANAGE_CREW_EMBLEM_CHECKS - Request for emblem failed")
				ENDIF					
			BREAK
			CASE 2
				TEXT_LABEL_63 tlCrewTxd
				IF NETWORK_CLAN_IS_EMBLEM_READY(iCrewID, tlCrewTxd)
					NETWORK_CLAN_RELEASE_EMBLEM(iCrewID)
					g_bPlayerHasValidCrewEmblem = TRUE
					iCrewEmblemStage = 0
					PRINTLN("MANAGE_CREW_EMBLEM_CHECKS - Player has a valid crew emblem")
				ELIF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdEmblemTime)) > 10000
					NETWORK_CLAN_RELEASE_EMBLEM(iCrewID)
					iCrewEmblemStage = 98
					PRINTLN("MANAGE_CREW_EMBLEM_CHECKS - Failed to get emblem, timeout")
				ENDIF
			BREAK
			CASE 98
				tdEmblemTime = GET_NETWORK_TIME()
				iCrewEmblemStage = 99
			BREAK
			CASE 99
				IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdEmblemTime)) > 120000 // 2 minutes
					iCrewEmblemStage = 0
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
		OR NOT IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
			g_bPlayerHasValidCrewEmblem = FALSE
		ENDIF
	ENDIF
	
	PED_INDEX pedID = PLAYER_PED_ID()
	IF IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
	AND DOES_ENTITY_EXIST(g_pShopClonePed)
	AND IS_ENTITY_A_PED(g_pShopClonePed)
	AND NOT IS_PED_INJURED(g_pShopClonePed)
		pedID = g_pShopClonePed
	ENDIF
	IF NOT IS_PED_INJURED(pedID)
	AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedID)
		SET_PED_ENABLE_CREW_EMBLEM(pedID, DOES_CURRENT_PED_COMPONENT_HAVE_RESTRICTION_TAG(pedID, PED_COMP_DECL, DLC_RESTRICTION_TAG_CREW_LOGO))
	ENDIF
ENDPROC

FUNC BOOL IS_PENDING_CASH_TRANSACTION_VALID_FOR_SPINNER(CASH_TRANSACTIONS_DETAILS &sCashTransactionDetail)
	IF sCashTransactionDetail.eEssentialData.eTransactionService = SERVICE_SPEND_CASH_DROP
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

#IF IS_DEBUG_BUILD
PROC DISPLAY_CASH_TRANSACTION_DEBUG()
	IF b_debug_display_transaction_queue
		SET_TEXT_FONT(FONT_STANDARD)
		SET_TEXT_SCALE(0.0000, 0.360)
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_WRAP(0.0, 1.0)
		DISPLAY_TEXT_WITH_LITERAL_STRING(0.8, 0.15,  "STRING", "TRANSACTION QUEUE")
		
		TEXT_LABEL_63 tlTempText
		INT iScriptTransactionIndex
		
		REPEAT SHOPPING_TRANSACTION_MAX_NUMBER iScriptTransactionIndex
			
			tlTempText = ""
			tlTempText += iScriptTransactionIndex
			tlTempText += ":"
			
			SET_TEXT_FONT(FONT_STANDARD)
			SET_TEXT_SCALE(0.0000, 0.360)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			SET_TEXT_WRAP(0.0, 1.0)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.8, 0.15+0.03+(iScriptTransactionIndex*0.03),  "STRING", tlTempText)
			
			tlTempText = ""
			
			IF g_cashTransactionData[iScriptTransactionIndex].eEssentialData.eTransactionStatus != CASH_TRANSACTION_STATUS_NULL
			
				IF g_cashTransactionData[iScriptTransactionIndex].eEssentialData.eCashTransactionType = CASH_TRANSACTION_TYPE_BASKET
					tlTempText += "[BASKET]"
				ELSE
					tlTempText += "[SINGLE]"
				ENDIF
			
				IF g_cashTransactionData[iScriptTransactionIndex].eEssentialData.iEventFrameCount = -1
					tlTempText += "[~p~Stale~w~]"
				ELIF NOT g_cashTransactionData[iScriptTransactionIndex].eEssentialData.bTransactionStarted
					IF NOT g_cashTransactionData[iScriptTransactionIndex].eEssentialData.bTransactionReady
						tlTempText += "[~o~Open~w~]"
					ELSE
						tlTempText += "[~o~Queued~w~]"
					ENDIF
				ELIF g_cashTransactionData[iScriptTransactionIndex].eEssentialData.eTransactionStatus = CASH_TRANSACTION_STATUS_PENDING
					tlTempText += "[~b~Pending~w~]"
				ELIF g_cashTransactionData[iScriptTransactionIndex].eEssentialData.eTransactionStatus = CASH_TRANSACTION_STATUS_SUCCESS
					tlTempText += "[~g~Success~w~]"
				ELIF g_cashTransactionData[iScriptTransactionIndex].eEssentialData.eTransactionStatus = CASH_TRANSACTION_STATUS_FAIL
					tlTempText += "[~r~Failed~w~]"
				ENDIF
				tlTempText += "[Attempt="
				tlTempText += g_cashTransactionData[iScriptTransactionIndex].eEssentialData.iRetryAttempts
				tlTempText += "]"
			ENDIF
			
			SET_TEXT_FONT(FONT_STANDARD)
			SET_TEXT_SCALE(0.0000, 0.360)
			SET_TEXT_COLOUR(255, 255, 255, 255)
			SET_TEXT_WRAP(0.0, 1.0)
			DISPLAY_TEXT_WITH_LITERAL_STRING(0.815, 0.15+0.03+(iScriptTransactionIndex*0.03),  "STRING", tlTempText)
		ENDREPEAT
		
		DRAW_RECT(0.873, 0.248, 0.163, 0.230, 0, 0, 0, 180)
	ENDIF
ENDPROC
#ENDIF

/// PURPOSE:
///    Delete the transaction data stored on this script
PROC DELETE_SC_STORED_CASH_TRANSACTION(INT iScriptTransactionIndex)
		
	DELETE_CASH_TRANSACTION(iScriptTransactionIndex)
	// Now reset the transaction data
	RESEST_CASH_TRANSACTION_STRUCT(m_cashTransactionData[iScriptTransactionIndex])
	
ENDPROC

/// PURPOSE:
///    Converted from relying on global data to relying on events and locally stored datas
PROC MANAGE_CASH_TRANSACTION_EVENTS()
	
	BOOL bIsCashTransactionCurrentlyPending = FALSE
	
	INT iTransactionError
	INT iScriptTransactionIndex
	
	REPEAT SHOPPING_TRANSACTION_MAX_NUMBER iScriptTransactionIndex
		
		// Validate
		IF g_bValidateCashTransactionEvent
			IF sTransactionEventData[iScriptTransactionIndex].bValidate
				VALIDATE_CASH_TRANSACTION_SUCCESS_RESULT(iScriptTransactionIndex)
				sTransactionEventData[iScriptTransactionIndex].bValidate = FALSE
			ENDIF
		ENDIF
		
		// Fake events
		IF m_cashTransactionData[iScriptTransactionIndex].eEssentialData.bTriggerFakeEvent
			PROCESS_EVENT_NETWORK_SHOP_TRANSACTION(m_cashTransactionData[iScriptTransactionIndex].eEssentialData.iTransactionId)
		ENDIF
		
		//Check if we're done with this transaction
		IF g_cashTransactionData[iScriptTransactionIndex].eEssentialData.iTransactionId = NET_SHOP_INVALID_ID
		AND m_cashTransactionData[iScriptTransactionIndex].eEssentialData.bTransactionStarted
			PRINTLN("[CASH] MANAGE_CASH_TRANSACTION_EVENTS - Cleaning up transaction data")
			RESEST_CASH_TRANSACTION_STRUCT(m_cashTransactionData[iScriptTransactionIndex])
		ENDIF
		
		// Spinner
		IF m_cashTransactionData[iScriptTransactionIndex].eEssentialData.eTransactionStatus = CASH_TRANSACTION_STATUS_PENDING
		AND g_bPendingCashTransactionCommunicatingWithServer
		AND IS_PENDING_CASH_TRANSACTION_VALID_FOR_SPINNER(m_cashTransactionData[iScriptTransactionIndex])
			bIsCashTransactionCurrentlyPending = TRUE
		ENDIF
		
		// Flag stale transactions
		IF m_cashTransactionData[iScriptTransactionIndex].eEssentialData.iEventFrameCount > 0
		AND NOT IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
			INT iFramesPerSec = 1000/ROUND(GET_FRAME_TIME()*1000.0)
			IF (GET_FRAME_COUNT() - m_cashTransactionData[iScriptTransactionIndex].eEssentialData.iEventFrameCount) > iFramesPerSec * 10 // Time out after 10s
				PRINTLN("[CASH] agh ### MANAGE_CASH_TRANSACTION_EVENTS - Transaction has not been cleaned up by script. Add bug for Kenneth R.")
				PRINTLN("...iScriptTransactionIndex = ", iScriptTransactionIndex)
				PRINTLN("...eTransactionType = ", GET_CASH_TRANSACTION_TYPE_DEBUG_STRING(m_cashTransactionData[iScriptTransactionIndex].eEssentialData.eTransactionType))
				PRINTLN("...eTransactionService = ", GET_CASH_TRANSACTION_SERVICE_NAME(m_cashTransactionData[iScriptTransactionIndex].eEssentialData.eTransactionService))
				PRINTLN("...eActionType = ", GET_CASH_TRANSACTION_ACTION_TYPE_DEBUG_STRING(m_cashTransactionData[iScriptTransactionIndex].eEssentialData.eActionType))
				
				SCRIPT_ASSERT("MANAGE_CASH_TRANSACTION_EVENTS - Transaction has not been cleaned up by script. Send logs to Kenneth R.")
				m_cashTransactionData[iScriptTransactionIndex].eEssentialData.iEventFrameCount = -1
			ENDIF
		ENDIF
		
		// Process transactions passed to this script
		IF USE_SERVER_TRANSACTIONS()
		AND m_cashTransactionData[iScriptTransactionIndex].eEssentialData.eTransactionStatus = CASH_TRANSACTION_STATUS_PENDING
		AND m_cashTransactionData[iScriptTransactionIndex].eEssentialData.bTransactionReady
		AND NOT m_cashTransactionData[iScriptTransactionIndex].eEssentialData.bTransactionStarted
		
			IF (GET_FRAME_COUNT() - m_cashTransactionData[iScriptTransactionIndex].eEssentialData.iTransactionDelay) >= GET_TRANSACTION_FRAME_DELAY_FOR_TYPE(m_cashTransactionData[iScriptTransactionIndex].eEssentialData.eCashTransactionType)
				
				iTransactionError = 0
				
				IF NOT NET_GAMESERVER_IS_SESSION_VALID(GET_ACTIVE_CHARACTER_SLOT())
				OR NET_GAMESERVER_IS_SESSION_REFRESH_PENDING()
					iTransactionError = 1
				ELIF m_cashTransactionData[iScriptTransactionIndex].eEssentialData.eTransactionStatus != CASH_TRANSACTION_STATUS_PENDING
					iTransactionError = 2
				ELIF m_cashTransactionData[iScriptTransactionIndex].eEssentialData.eCashTransactionType != CASH_TRANSACTION_TYPE_BASKET
				AND NOT NET_GAMESERVER_BEGIN_SERVICE(m_cashTransactionData[iScriptTransactionIndex].eEssentialData.iTransactionId, m_cashTransactionData[iScriptTransactionIndex].eEssentialData.eTransactionCategory, m_cashTransactionData[iScriptTransactionIndex].eEssentialData.eTransactionService, m_cashTransactionData[iScriptTransactionIndex].eEssentialData.eActionType, m_cashTransactionData[iScriptTransactionIndex].eEssentialData.iCost, m_cashTransactionData[iScriptTransactionIndex].eEssentialData.eFlags)
					iTransactionError = 3
				ELIF NOT NET_GAMESERVER_CHECKOUT_START(m_cashTransactionData[iScriptTransactionIndex].eEssentialData.iTransactionId)
					iTransactionError = 4
				ELSE
					PRINTLN("[CASH] MANAGE_CASH_TRANSACTION_EVENTS - Transaction started [", m_cashTransactionData[iScriptTransactionIndex].eEssentialData.iTransactionId, "]")
					m_cashTransactionData[iScriptTransactionIndex].eEssentialData.bTransactionStarted = TRUE
					g_cashTransactionData[iScriptTransactionIndex].eEssentialData.bTransactionStarted = TRUE
					g_cashTransactionData[iScriptTransactionIndex].eEssentialData.iTransactionId = m_cashTransactionData[iScriptTransactionIndex].eEssentialData.iTransactionId
				ENDIF
				
				IF iTransactionError != 0
					PRINTLN("[CASH] MANAGE_CASH_TRANSACTION_EVENTS - Transaction aborted [", iScriptTransactionIndex, "], code ", iTransactionError)
					DELETE_SC_STORED_CASH_TRANSACTION(iScriptTransactionIndex)
				ENDIF
			ELSE
				PRINTLN("[CASH] MANAGE_CASH_TRANSACTION_EVENTS - Transaction queued [", iScriptTransactionIndex, "]")
			ENDIF
		ENDIF
	ENDREPEAT
	
	
	//#2053093 - Add some kind of visual showing the transaction is in process
	IF NOT bRunCashTransactionPendingIcon
		IF bIsCashTransactionCurrentlyPending
		#IF IS_DEBUG_BUILD
		OR bDebugRunCashTransactionPendingIcon
		#ENDIF
			REFRESH_SCALEFORM_LOADING_ICON(sCashTransactionIconStruct)
			bRunCashTransactionPendingIcon = TRUE
		ENDIF
	ELSE
		IF bIsCashTransactionCurrentlyPending
		#IF IS_DEBUG_BUILD
		OR bDebugRunCashTransactionPendingIcon
		#ENDIF
			sCashTransactionIconStruct.sMainStringSlot = "HUD_TRANSP"	//Transaction Pending
			RUN_SCALEFORM_LOADING_ICON(sCashTransactionIconStruct, SHOULD_REFRESH_SCALEFORM_LOADING_ICON(sCashTransactionIconStruct))
		ELSE
			bRunCashTransactionPendingIcon = FALSE
			SET_LOADING_ICON_INACTIVE()
		ENDIF
	ENDIF
	
ENDPROC

/// PURPOSE:
///    This will periodically run a staggered loop over list of vehicles from mp DLCs that
///    should never exist in SP. If any of such vehicles is found it will be instantly deleted.
///    This should stop people from trying to spawn unreleased vehicles in SP.
PROC MAINTAIN_REMOVAL_OF_FORBIDDEN_SP_VEHICLES()

	IF NOT CAN_SAFELY_REMOVE_FORBIDDEN_MP_VEHICLE_FROM_SP(-1)
		IF HAS_NET_TIMER_STARTED(removeForbiddenVehiclesTimer)
			RESET_NET_TIMER(removeForbiddenVehiclesTimer)
		ENDIF
		EXIT
	ENDIF
	
	IF NOT HAS_NET_TIMER_STARTED(removeForbiddenVehiclesTimer)
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SHOPS, "MAINTAIN_REMOVAL_OF_FORBIDDEN_SP_VEHICLES - First time we are OK to delete vehicle, but let's wait till next interval to actually do anything.")
		#ENDIF
		REINIT_NET_TIMER(removeForbiddenVehiclesTimer, TRUE)
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SHOPS, "MAINTAIN_REMOVAL_OF_FORBIDDEN_SP_VEHICLES - Current removal timer after init is: ", NATIVE_TO_INT(removeForbiddenVehiclesTimer.Timer))
		#ENDIF
		
		// Set the timer to some future date so we have more padding before deleting kicks in
		// (this is to stop this mechanism from kicking in on quiting the game or during the transition,
		// even though CAN_SAFELY_REMOVE_FORBIDDEN_MP_VEHICLE_FROM_SP should stop it sometimes it slips throguh that check...)
		removeForbiddenVehiclesTimer.Timer = GET_TIME_OFFSET(removeForbiddenVehiclesTimer.Timer, 1000 * 10)
		iForbiddenVehicleStagger = NUMBER_OF_VEHICLE_MODELS_FORBIDDEN_IN_SP // iForbiddenVehicleStagger is 0 by default so next frame the check would still go, have to set it to max number so it waits until next turn
		
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SHOPS, "MAINTAIN_REMOVAL_OF_FORBIDDEN_SP_VEHICLES - Setting removal timer to a future date: ", NATIVE_TO_INT(removeForbiddenVehiclesTimer.Timer))
		#ENDIF
		EXIT
	ENDIF
	
	//IF HAS_NET_TIMER_EXPIRED(removeForbiddenVehiclesTimer, 6000)
	// (we can't use HAS_NET_TIMER_EXPIRED because it does a ABSI on the time difference so a timer set to the future will be treated as if it expired...
//	INT iTimeDiff = GET_TIME_DIFFERENCE(INT_TO_NATIVE(TIME_DATATYPE, GET_GAME_TIMER()), removeForbiddenVehiclesTimer.Timer)
//	
//	IF iTimeDiff >= 10000
//		#IF IS_DEBUG_BUILD
//			CDEBUG1LN(DEBUG_SHOPS, "MAINTAIN_REMOVAL_OF_FORBIDDEN_SP_VEHICLES - Resetting iForbiddenVehicleStagger since ", GET_GAME_TIMER(), " - ", NATIVE_TO_INT(removeForbiddenVehiclesTimer.Timer), " = ", iTimeDiff, " > 6000")
//		#ENDIF
//		// Every 6 seconds reset the stagger which will cause this function to go through all forbidden vehicles, one per frame
//		iForbiddenVehicleStagger = 0
//		REINIT_NET_TIMER(removeForbiddenVehiclesTimer, TRUE)
//	ENDIF
	
	IF iForbiddenVehicleStagger < NUMBER_OF_VEHICLE_MODELS_FORBIDDEN_IN_SP
		#IF IS_DEBUG_BUILD
			CDEBUG1LN(DEBUG_SHOPS, "MAINTAIN_REMOVAL_OF_FORBIDDEN_SP_VEHICLES - Running REMOVE_FORBIDDEN_MP_VEHICLE_FROM_SP")
		#ENDIF
		REMOVE_FORBIDDEN_MP_VEHICLE_FROM_SP(iForbiddenVehicleStagger)
		iForbiddenVehicleStagger = iForbiddenVehicleStagger + 1
	ELSE
		iForbiddenVehicleStagger = 0
		REINIT_NET_TIMER(removeForbiddenVehiclesTimer, TRUE)
	ENDIF
ENDPROC


INT iHelmetID 		= 100
PROC PROCESS_HELMET_VISOR()
	
	BOOL bSafeToProcessMPChecks = FALSE
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND NOT IS_PLAYER_SPECTATING(PLAYER_ID())
	AND NOT (IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE) AND NOT IS_PED_ON_ANY_BIKE(PLAYER_PED_ID()))
	AND NOT GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_EXIT_VEHICLE)
	AND NOT GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_PuttingOnHelmet)
	AND g_bSafeToProcessMPChecks
	AND NOT g_bForcePlayersHelmetOn
	AND (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01 OR GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01)
		bSafeToProcessMPChecks = TRUE
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF bDrawDebugHelmetStuff
	AND NOT g_db_bDrawDisplaySlotData
		SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
		
		INT iStrOffset, iColumn = 0
		INT iRed, iGreen, iBlue, iAlpha
		TEXT_LABEL_63 str = ""
		HUD_COLOURS eHudColour = HUD_COLOUR_RED
		FLOAT fSafeAlpha
		
		//bSafeToProcessMPChecks
		IF NOT bSafeToProcessMPChecks
			str  = "NOT bSafeToProcessMPChecks"
			
			BOOL bSafeReason = FALSE
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				str += ", network game not in progress"
				bSafeReason = TRUE
			ENDIF
			IF IS_PED_INJURED(PLAYER_PED_ID())
				str += ", injured"
				bSafeReason = TRUE
			ENDIF

			IF IS_PLAYER_SPECTATING(PLAYER_ID())
				str += ", spectating"
				bSafeReason = TRUE
			ENDIF
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE) AND NOT IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
				str += ", in vehicle"
				bSafeReason = TRUE
			ENDIF
			IF NOT g_bSafeToProcessMPChecks
				str += ", NOT g_bSafeToProcessMPChecks"
				bSafeReason = TRUE
			ENDIF
			IF GET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_PuttingOnHelmet)
				str += ", PRF_PuttingOnHelmet"
				bSafeReason = TRUE
			ENDIF
			IF NOT (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01 OR GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01)
				str += ", model:"
				str += GET_MODEL_NAME_FOR_DEBUG(GET_ENTITY_MODEL(PLAYER_PED_ID()))
				bSafeReason = TRUE
			ENDIF
			IF NOT bSafeReason
				str += ", NOT bSafeReason"
			ENDIF
			
			eHudColour = HUD_COLOUR_RED
			fSafeAlpha = 0.5
		ELSE
			str  = "bSafeToProcessMPChecks"
			eHudColour = HUD_COLOUR_BLUE
			fSafeAlpha = 1.0
		ENDIF
		GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
		DRAW_DEBUG_TEXT_2D(str, <<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, iAlpha)	iStrOffset++
		
		//PCF_HasHelmet
		IF NOT GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_HasHelmet)
			str  = "NOT PCF_HasHelmet"
			eHudColour = HUD_COLOUR_RED
		ELSE
			str  = "PCF_HasHelmet"
			eHudColour = HUD_COLOUR_BLUE
		ENDIF
		IF HAVE_STATS_LOADED()
			PED_COMP_ITEM_DATA_STRUCT ItemData = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_PLAYER_MODEL(), COMP_TYPE_PROPS, INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_MP_INT_CHARACTER_STAT(MP_STAT_HELMET_CURRENT_COLOR)))
			str += " [colour:"
			str += ItemData.iDrawable
			str += ", "
			str += ItemData.iTexture
			str += "]"
		ENDIF
		str += " [iHelmetID:"
		str += iHelmetID
		str += "]"
		GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
		DRAW_DEBUG_TEXT_2D(str, <<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, ROUND(TO_FLOAT(iAlpha)*fSafeAlpha))	iStrOffset++
		
		//PCF_DontTakeOffHelmet
		IF NOT GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet)
			str  = "NOT PCF_DontTakeOffHelmet"
			eHudColour = HUD_COLOUR_RED
		ELSE
			str  = "PCF_DontTakeOffHelmet"
			eHudColour = HUD_COLOUR_BLUE
		ENDIF
		GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
		DRAW_DEBUG_TEXT_2D(str, <<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, ROUND(TO_FLOAT(iAlpha)*fSafeAlpha))	iStrOffset++
		
		//PCF_IsSwitchingHelmetVisor
		IF NOT GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_IsSwitchingHelmetVisor)
			str  = "NOT PCF_IsSwitchingHelmetVisor"
			eHudColour = HUD_COLOUR_RED
		ELSE
			str  = "PCF_IsSwitchingHelmetVisor"
			eHudColour = HUD_COLOUR_BLUE
		ENDIF
		GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
		DRAW_DEBUG_TEXT_2D(str, <<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, ROUND(TO_FLOAT(iAlpha)*fSafeAlpha))	iStrOffset++

		IF NOT GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceHelmetVisorSwitch)
			str  = "NOT PCF_ForceHelmetVisorSwitch"
			eHudColour = HUD_COLOUR_RED
		ELSE
			str  = "PCF_ForceHelmetVisorSwitch"
			eHudColour = HUD_COLOUR_BLUE
		ENDIF
		GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
		DRAW_DEBUG_TEXT_2D(str, <<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, ROUND(TO_FLOAT(iAlpha)*fSafeAlpha))	iStrOffset++
		
		IF HAVE_STATS_LOADED()		
		AND DOES_PLAYER_PREFER_VISOR_UP() != bHelmetVisorUp
			iStrOffset++
			GET_HUD_COLOUR(HUD_COLOUR_BLUE, iRed, iGreen, iBlue, iAlpha)
			DRAW_DEBUG_TEXT_2D("Stat & visor state mismatch", <<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, iAlpha) iStrOffset++
		ENDIF
		
		IF NOT GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableAutoEquipHelmetsInBikes)
			str  = "NOT PCF_DisableAutoEquipHelmetsInBikes"
			eHudColour = HUD_COLOUR_RED
		ELSE
			str  = "PCF_DisableAutoEquipHelmetsInBikes"
			eHudColour = HUD_COLOUR_BLUE
		ENDIF
		IF bBypassAutoEquipHelmetsInBikes
			str += ", bypassed"
		ELSE
			str += ", not bypassed"
		ENDIF
		
		GET_HUD_COLOUR(eHudColour, iRed, iGreen, iBlue, iAlpha)
		DRAW_DEBUG_TEXT_2D(str, <<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, ROUND(TO_FLOAT(iAlpha)*fSafeAlpha))	iStrOffset++
		
		IF HAVE_STATS_LOADED()
			iStrOffset++
			IF DOES_PLAYER_PREFER_VISOR_UP()
				GET_HUD_COLOUR(HUD_COLOUR_MENU_GREEN, iRed, iGreen, iBlue, iAlpha)
				DRAW_DEBUG_TEXT_2D("Stat set: Visor Up", <<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, iAlpha)
			ELSE
				GET_HUD_COLOUR(HUD_COLOUR_BLUE, iRed, iGreen, iBlue, iAlpha)
				DRAW_DEBUG_TEXT_2D("Stat set: Visor Down", <<0.3,0.1,0>>+(<<0.0,0.01,0>>*TO_FLOAT(iStrOffset))+(<<0.2,0.0,0>>*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, iAlpha)
			ENDIF
		ENDIF
	ENDIF
	#ENDIF
	
	IF bSafeToProcessMPChecks
		// Process Helmet prop changes
		INT iPlayerHeadPropIndex = GET_PED_PROP_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD)
		INT iPlayerHeadPropTexture = GET_PED_PROP_TEXTURE_INDEX(PLAYER_PED_ID(), ANCHOR_HEAD)
		INT iTempIDCheck = ((iPlayerHeadPropIndex * 1000)+(iPlayerHeadPropTexture))
		IF iHelmetID != iTempIDCheck
		
			INT iDLCNameHash = GET_HASH_NAME_FOR_PROP(NATIVE_TO_INT(PLAYER_PED_ID()), ENUM_TO_INT(ANCHOR_HEAD), iPlayerHeadPropIndex, iPlayerHeadPropTexture)
			
			sCachedClothingStates.iPropHeadHashID = iDLCNameHash
			sCachedClothingStates.iPropHeadVariantCount = GET_SHOP_PED_APPAREL_VARIANT_PROP_COUNT(iDLCNameHash)
			
			IF IS_ITEM_A_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS, GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD)), iDLCNameHash)
				IF iDLCNameHash != 0
					IF NOT GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_HasHelmet)
						CPRINTLN(DEBUG_PED_COMP, "PROCESS_HELMET_VISOR - Setting PROP//ANCHOR_HEAD varition with restriction tag HELMET turned on - SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_HasHelmet, TRUE)")
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_HasHelmet, TRUE)
					ENDIF
					
					CPRINTLN(DEBUG_PED_COMP, "SET_PED_HELMET_INDEX prop:", iPlayerHeadPropIndex, " text:", iPlayerHeadPropTexture, " iDLCNameHash:", iDLCNameHash)
					
					SET_PED_HELMET_PROP_INDEX(PLAYER_PED_ID(), iPlayerHeadPropIndex, FALSE)
					SET_PED_HELMET_TEXTURE_INDEX(PLAYER_PED_ID(), iPlayerHeadPropTexture)
					
					IF sCachedClothingStates.iPropHeadVariantCount > 0
						scrShopPedProp propItem
						GET_SHOP_PED_PROP(iDLCNameHash, propItem)
						
						bHelmetVisorUp = DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCNameHash, DLC_RESTRICTION_TAG_ALT_HELMET, ENUM_TO_INT(SHOP_PED_PROP))
						
						STORE_HELMET_VISOR_PROP_INDICES(PLAYER_PED_ID(), propItem.m_propIndex, propItem.m_textureIndex, FALSE)
						
						//Set our helmet visor stat
						//url:bugstar:3036512
						//PED_COMP_NAME_ENUM eAltPropName = GET_PROP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), propItem.m_propIndex, propItem.m_textureIndex, ANCHOR_HEAD)
						//SET_MP_INT_CHARACTER_STAT(MP_STAT_HELMET_CURRENT_COLOR, ENUM_TO_INT(eAltPropName))
					ENDIF
				ENDIF
			ELSE			
				PED_COMP_ITEM_DATA_STRUCT ItemData = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_PLAYER_MODEL(), COMP_TYPE_PROPS, INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_MP_INT_CHARACTER_STAT(MP_STAT_HELMET_CURRENT_COLOR)))
				IF ItemData.iDrawable != -1
				AND ItemData.iTexture != -1
					CPRINTLN(DEBUG_PED_COMP, "SET_PED_HELMET_INDEX prop:", ItemData.iDrawable, " text:", ItemData.iTexture, " enum:", GET_MP_INT_CHARACTER_STAT(MP_STAT_HELMET_CURRENT_COLOR), " sLabel:", ItemData.sLabel)
					
					SET_PED_HELMET_PROP_INDEX(PLAYER_PED_ID(), ItemData.iDrawable, FALSE)
					SET_PED_HELMET_TEXTURE_INDEX(PLAYER_PED_ID(), ItemData.iTexture)
					
					STORE_HELMET_VISOR_PROP_INDICES(PLAYER_PED_ID(), ItemData.iDrawable, ItemData.iTexture)
				ELSE
					CPRINTLN(DEBUG_PED_COMP, "PROCESS_HELMET_VISOR - dont set helmet prop and texture indexes, MP_STAT_HELMET_CURRENT_COLOR:", GET_MP_INT_CHARACTER_STAT(MP_STAT_HELMET_CURRENT_COLOR))
				ENDIF

				IF GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_HasHelmet)
					CPRINTLN(DEBUG_PED_COMP, "PROCESS_HELMET_VISOR - Setting PROP//ANCHOR_HEAD varition with restriction tag HELMET turned off - SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_HasHelmet, FALSE)")
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_HasHelmet, FALSE)
				ENDIF
				IF GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet)
					CPRINTLN(DEBUG_PED_COMP, "PROCESS_HELMET_VISOR - Setting PROP//ANCHOR_HEAD varition with restriction tag HELMET turned off - SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, FALSE)")
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, FALSE)
				ENDIF
			ENDIF
			iHelmetID = iTempIDCheck
		ENDIF
		
		IF iPlayerHeadPropIndex != -1
		AND GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_HasHelmet, FALSE)
			IF g_iPIM_SubMenu = ciPI_SUB_MENU_ACCESSORIES
			OR g_iPIM_SubMenu = ciPI_SUB_MENU_MC_PRES_STYLE
				bHelmetVisorUp = DOES_PLAYER_PREFER_VISOR_UP()
			ELIF GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_IsSwitchingHelmetVisor)
				INITALISE_HELMET_VISOR()
				
				IF DOES_PLAYER_PREFER_VISOR_UP() != bHelmetVisorUp
					CPRINTLN(DEBUG_PED_COMP, "PROCESS_HELMET_VISOR - Setting visor preference to:", bHelmetVisorUp)
					SET_PLAYER_VISOR_PREFERENCE(bHelmetVisorUp)
				ENDIF
			ELSE
				IF sCachedClothingStates.iPropHeadVariantCount > 0
				AND DOES_PLAYER_PREFER_VISOR_UP() != bHelmetVisorUp
					MP_OUTFIT_ENUM thisOutfit = GB_OUTFITS_BD_GET_THIS_GOON_OUTFIT(PLAYER_ID())
					IF thisOutfit >= OUTFIT_HIDDEN_DEADLINE_PURPLE_0
					AND thisOutfit <= OUTFIT_HIDDEN_DEADLINE_GREEN_0
						//bypass
						CDEBUG1LN(DEBUG_PED_COMP, "PROCESS_HELMET_VISOR - Bypass shopcontroller flag bHelmetVisorUp, outfit is ", GB_OUTFITS_GET_GOON_OUTFIT_STRING_FOR_HELP(thisOutfit))
					ELSE
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceHelmetVisorSwitch, TRUE)
						
						IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(sCachedClothingStates.iPropHeadHashID, DLC_RESTRICTION_TAG_ALT_HELMET, ENUM_TO_INT(SHOP_PED_PROP))
							CPRINTLN(DEBUG_PED_COMP, "PROCESS_HELMET_VISOR - Setting shopcontroller flag bHelmetVisorUp to TRUE \"", GB_OUTFITS_GET_GOON_OUTFIT_STRING_FOR_HELP(thisOutfit), "\"")
							bHelmetVisorUp = TRUE
						ELSE
							CPRINTLN(DEBUG_PED_COMP, "PROCESS_HELMET_VISOR - Setting shopcontroller flag bHelmetVisorUp to FALSE \"", GB_OUTFITS_GET_GOON_OUTFIT_STRING_FOR_HELP(thisOutfit), "\"")
							bHelmetVisorUp = FALSE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
	AND (GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_M_FREEMODE_01 OR GET_ENTITY_MODEL(PLAYER_PED_ID()) = MP_F_FREEMODE_01)
	AND NOT b_is_player_an_animal
	
		GB_MAGNATE_OUTFIT_BOSS_STYLE thisStyle = GB_OUTFITS_BD_GET_BOSS_STYLE()
		
		INT iTeamOutfit = ENUM_TO_INT(OUTFIT_MP_DEFAULT)
		IF NETWORK_IS_ACTIVITY_SESSION()
			INT iTeam = GET_PLAYER_HEIST_TEAM()
			
			IF iTeam <= INVALID_HEIST_DATA
			OR iTeam >= MAX_MISS_CONTROLER_TEAMS
				//
			ELSE
				iTeamOutfit = GET_LOCAL_DEFAULT_OUTFIT(iTeam)
			ENDIF
		ENDIF
		IF (iSavedTeamOutfit != iTeamOutfit)
			bTeamOutfitHasHelmet = FALSE
			
			MP_OUTFITS_DATA sOutfitsData
			MP_OUTFITS_APPLY_DATA sApplyOutfitdata
			
			sApplyOutfitdata.pedID = PLAYER_PED_ID()
			sApplyOutfitdata.eOutfit = INT_TO_ENUM(MP_OUTFIT_ENUM, iTeamOutfit)
			
			IF GET_MP_OUTFIT_DATA(sOutfitsData, sApplyOutfitdata, FALSE)
				PED_PROP_POSITION eProp = ANCHOR_HEAD
		    	IF (sOutfitsData.iPropDrawableID[eProp] != -1)
				AND (sOutfitsData.iPropTextureID[eProp] != -1)
					//
					IF IS_ITEM_A_HELMET(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_PROPS,
							GET_PROP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(),
							sOutfitsData.iPropDrawableID[eProp],
							sOutfitsData.iPropTextureID[eProp] , eProp))
						bTeamOutfitHasHelmet = TRUE
						CPRINTLN(DEBUG_AMBIENT, "PROCESS_HELMET_VISOR bTeamOutfitHasHelmet = [", GET_STRING_FROM_BOOL(bTeamOutfitHasHelmet), ", ", GB_OUTFITS_GET_GOON_OUTFIT_STRING_FOR_HELP(sApplyOutfitdata.eOutfit), "] IS_ITEM_A_HELMET is true")
					ELSE
					//	bTeamOutfitHasHelmet = FALSE
						CDEBUG1LN(DEBUG_AMBIENT, "PROCESS_HELMET_VISOR bTeamOutfitHasHelmet = [", GET_STRING_FROM_BOOL(bTeamOutfitHasHelmet), ", ", GB_OUTFITS_GET_GOON_OUTFIT_STRING_FOR_HELP(sApplyOutfitdata.eOutfit), "] IS_ITEM_A_HELMET is false")
					ENDIF
				ELSE
				//	bTeamOutfitHasHelmet = FALSE
					CDEBUG1LN(DEBUG_AMBIENT, "PROCESS_HELMET_VISOR bTeamOutfitHasHelmet = [", GET_STRING_FROM_BOOL(bTeamOutfitHasHelmet), ", ", GB_OUTFITS_GET_GOON_OUTFIT_STRING_FOR_HELP(sApplyOutfitdata.eOutfit), "] iPropDrawableID and iPropTextureID are -1")
				ENDIF
			ELSE
			//	bTeamOutfitHasHelmet = FALSE
				CDEBUG1LN(DEBUG_AMBIENT, "PROCESS_HELMET_VISOR bTeamOutfitHasHelmet = [", GET_STRING_FROM_BOOL(bTeamOutfitHasHelmet), ", ", GB_OUTFITS_GET_GOON_OUTFIT_STRING_FOR_HELP(sApplyOutfitdata.eOutfit), "] GET_MP_OUTFIT_DATA is false???")
			ENDIF
			
			iSavedTeamOutfit = iTeamOutfit
		ENDIF
		
		IF thisStyle >= GB_BOSS_STYLE_BIKER_PRESIDENT_0
		AND thisStyle <= GB_BOSS_STYLE_BIKER_PRESIDENT_7
			IF NOT bBypassAutoEquipHelmetsInBikes
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableAutoEquipHelmetsInBikes, TRUE)
				CPRINTLN(DEBUG_AMBIENT, "PROCESS_HELMET_VISOR - bypass PCF_DisableAutoEquipHelmetsInBikes, wearing biker style \"", GB_OUTFITS_GET_BOSS_STYLE_STRING_FOR_HELP(thisStyle), "\"")
				bBypassAutoEquipHelmetsInBikes = TRUE
			ENDIF
		ELIF bTeamOutfitHasHelmet
			IF NOT bBypassAutoEquipHelmetsInBikes
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableAutoEquipHelmetsInBikes, TRUE)
				CPRINTLN(DEBUG_AMBIENT, "PROCESS_HELMET_VISOR - bypass PCF_DisableAutoEquipHelmetsInBikes, wearing team outfit \"", GET_MP_OUTFIT_NAME_FROM_ENUM(INT_TO_ENUM(MP_OUTFIT_ENUM, iTeamOutfit)), "\"")
				bBypassAutoEquipHelmetsInBikes = TRUE
			ENDIF
		ELIF sCachedClothingStates.bPlayerWearingFittedHoodCache
			IF NOT bBypassAutoEquipHelmetsInBikes
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableAutoEquipHelmetsInBikes, TRUE)
				CPRINTLN(DEBUG_AMBIENT, "PROCESS_HELMET_VISOR - bypass PCF_DisableAutoEquipHelmetsInBikes,
						wearing hooded jacket with hood up \"",
						GET_MP_OUTFIT_NAME_FROM_ENUM(INT_TO_ENUM(MP_OUTFIT_ENUM, iTeamOutfit)),
						"\"")
				bBypassAutoEquipHelmetsInBikes = TRUE
			ENDIF
		ELSE
			IF bBypassAutoEquipHelmetsInBikes
				IF HAS_IMPORTANT_STATS_LOADED()
					INT iAutoEquipHelmetsInBikes = GET_MP_INT_CHARACTER_STAT(MP_STAT_AUTO_EQUIP_HELMET)
					IF (iAutoEquipHelmetsInBikes = 0)
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableAutoEquipHelmetsInBikes, TRUE)
						CPRINTLN(DEBUG_AMBIENT, "PROCESS_HELMET_VISOR - restore PCF_DisableAutoEquipHelmetsInBikes")
					ELSE
						SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DisableAutoEquipHelmetsInBikes, FALSE)
						CPRINTLN(DEBUG_AMBIENT, "PROCESS_HELMET_VISOR - restore NOT PCF_DisableAutoEquipHelmetsInBikes")
					ENDIF
					bBypassAutoEquipHelmetsInBikes = FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC


INT iPedValidationStage
PROC PROCESS_INVALID_PED_COMPONENTS()

	IF (GET_FRAME_COUNT() % 160) != 0
		EXIT
	ENDIF

	IF NOT g_bValidatePlayersTorsoComponent
	OR bInNightclubThisFrame
	OR IS_PED_INJURED(PLAYER_PED_ID())
	OR NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR NETWORK_IS_ACTIVITY_SESSION()
	OR IS_PLAYER_IN_CORONA()
	OR IS_PLAYER_PLAYING_OR_PLANNING_HEIST(PLAYER_ID())
	OR Is_Player_Currently_On_MP_Versus_Mission(PLAYER_ID())
	OR FM_EVENT_GET_PLAYER_CURRENT_FM_EVENT(PLAYER_ID()) = FMMC_TYPE_HUNT_THE_BEAST
	OR IS_LOCAL_PLAYER_ON_A_NEW_AVERSAY()
	OR GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
	OR (GB_IS_PLAYER_BOSS_OF_A_GANG(PLAYER_ID()) AND GB_OUTFITS_BD_GET_BOSS_STYLE() != GB_BOSS_STYLE_NONE)
	OR (GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(FALSE) AND GB_OUTFITS_BD_GET_BOSS_STYLE() != GB_BOSS_STYLE_NONE)
	OR GET_CURRENT_GAMEMODE() != GAMEMODE_FM
		EXIT
	ENDIF
	
	VALIDATE_INVISIBLE_PED_COMPONENTS(iPedValidationStage)
	
ENDPROC

INT iTransactionState = GENERIC_TRANSACTION_STATE_DEFAULT

BOOL bPedHairTimerSet, bFailedPedHairTimerSet
TIME_DATATYPE tdPedHairTimer, tdFailedPedHairTimer

PROC VALIDATE_PLAYER_PURCHASED_HAIR()
	MODEL_NAMES ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
	STRING sPedModelName = ""	//GET_MODEL_NAME_FOR_DEBUG(ePedModel)
	
	PED_COMP_NAME_ENUM eCurrent_haircut, eEquipped_haircut, eUpgrade_haircut
	IF (ePedModel = MP_M_FREEMODE_01)
		sPedModelName = "MP_M_FREEMODE_01"
		eCurrent_haircut = INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA, g_iPedComponentSlot))
		eEquipped_haircut = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_HAIR)
		eUpgrade_haircut = GET_MALE_HAIR(eCurrent_haircut)
	ELIF (ePedModel = MP_F_FREEMODE_01)
		sPedModelName = "mp_f_freemode_01"
		eCurrent_haircut = INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA, g_iPedComponentSlot))
		eEquipped_haircut = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_HAIR)
		eUpgrade_haircut = GET_FEMALE_HAIR(eCurrent_haircut)
	ELSE
		UNUSED_PARAMETER(sPedModelName)
		UNUSED_PARAMETER(eEquipped_haircut)
		CPRINTLN(DEBUG_SHOPS, "bail gr_hair: VALIDATE_PLAYER_PURCHASED_HAIR ", sPedModelName)
		EXIT
	ENDIF
	
	IF (eCurrent_haircut != eUpgrade_haircut)
	OR (iTransactionState != GENERIC_TRANSACTION_STATE_DEFAULT)
		
		IF (eUpgrade_haircut != DUMMY_PED_COMP)
			
			IF (iTransactionState = GENERIC_TRANSACTION_STATE_DEFAULT)
				CPRINTLN(DEBUG_SHOPS, "begin gr_hair: VALIDATE_PLAYER_PURCHASED_HAIR ", sPedModelName, " haircut:", eCurrent_haircut, " / ", eUpgrade_haircut, ")")
			ELSE
				CPRINTLN(DEBUG_SHOPS, "performing gr_hair: VALIDATE_PLAYER_PURCHASED_HAIR ", sPedModelName, " haircut:", eCurrent_haircut, " / ", eUpgrade_haircut, ", state: ", iTransactionState, ")")
			ENDIF
			
			IF USE_SERVER_TRANSACTIONS()
				
				IF NOT NET_GAMESERVER_IS_SESSION_VALID(GET_ACTIVE_CHARACTER_SLOT())
				OR NET_GAMESERVER_IS_SESSION_REFRESH_PENDING()
					CASSERTLN(DEBUG_SHOPS, "quitting gr_hair: VALIDATE_PLAYER_PURCHASED_HAIR ", sPedModelName, " haircut:", eCurrent_haircut, " / ", eUpgrade_haircut, "), result: session invalid or refreshing")
					tdFailedPedHairTimer = GET_NETWORK_TIME()
					bFailedPedHairTimerSet = TRUE
					
					IF (iTransactionState != GENERIC_TRANSACTION_STATE_DEFAULT)
						DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					ENDIF
					
					iTransactionState = GENERIC_TRANSACTION_STATE_DEFAULT
					g_bValidatedPlayerPurchasedHair = TRUE
					
					EXIT
				ENDIF
				
				TEXT_LABEL_15 tlLabel
				
				INT iOption
				iOption = 0
				NGMP_MENU_OPTION_DATA sOptionData
				
				WHILE GET_NGMP_MENU_OPTION_DATA(ePedModel, HME_NGMP_HAIR, iOption, sOptionData)
					iOption++
					IF INT_TO_ENUM(PED_COMP_NAME_ENUM, eUpgrade_haircut) = sOptionData.eHairItem
						tlLabel = GET_HAIR_LABEL_FROM_LABEL(sOptionData.tlLabel)
						iOption = 99
					ENDIF
				ENDWHILE
				
				TEXT_LABEL_63 tlCategoryKey
				GENERATE_HAIRDO_KEY_FOR_CATALOGUE(tlCategoryKey, HME_NGMP_HAIR, tlLabel, ePedModel, 1)
				
				IF (iTransactionState = GENERIC_TRANSACTION_STATE_DEFAULT)
					IF NOT NET_GAMESERVER_CATALOG_ITEM_IS_VALID(tlCategoryKey)
						CASSERTLN(DEBUG_SHOPS, "start gr_hair: VALIDATE_PLAYER_PURCHASED_HAIR process \"", tlCategoryKey, "\" keyhash:", GET_HASH_KEY(tlCategoryKey), " statValue:", ENUM_TO_INT(eUpgrade_haircut), " not valid")
					ELSE
						CPRINTLN(DEBUG_SHOPS, "start gr_hair: VALIDATE_PLAYER_PURCHASED_HAIR process \"", tlCategoryKey, "\" ", GET_HASH_KEY(tlCategoryKey), " valid")
					ENDIF
				ENDIF
				
				IF PROCESS_GENERIC_ITEM_TRANSACTION(CATEGORY_INVENTORY_HAIR, NET_SHOP_ACTION_BUY_ITEM,
						GET_HASH_KEY(tlCategoryKey),					//INT iItemKey,
						1,												//INT iQuantity,
						0,												//INT iCost,
						iTransactionState,								//INT &iResult,
						-427320424,										//INT iInventoryKey = HASH("MP_STAT_CHAR_FM_STORED_HAIRDO_SA_v0"),
						DEFAULT,										//INT iProcessFlags = DEFAULT_CASH_TRANSACTION_FLAGS,
						DEFAULT,										//INT iItemHash = 0,
						DEFAULT)										//INT iExtraItemHash = 0)
					//
					IF iTransactionState = GENERIC_TRANSACTION_STATE_SUCCESS
						CPRINTLN(DEBUG_SHOPS, "succeeded gr_hair: VALIDATE_PLAYER_PURCHASED_HAIR ", sPedModelName, " haircut:", eCurrent_haircut, " / ", eUpgrade_haircut, ") tlCategoryKey:", tlCategoryKey, ", iCategoryHashkey:", GET_HASH_KEY(tlCategoryKey), ", result: SUCCESS")
					ELIF iTransactionState = GENERIC_TRANSACTION_STATE_FAILED
						CASSERTLN(DEBUG_SHOPS, "failed gr_hair: VALIDATE_PLAYER_PURCHASED_HAIR ", sPedModelName, " haircut:", eCurrent_haircut, " / ", eUpgrade_haircut, ") tlCategoryKey:", tlCategoryKey, ", iCategoryHashkey:", GET_HASH_KEY(tlCategoryKey), ", result: FAILED")
						tdFailedPedHairTimer = GET_NETWORK_TIME()
						bFailedPedHairTimerSet = TRUE
					ELSE
						CPRINTLN(DEBUG_SHOPS, "completed gr_hair: VALIDATE_PLAYER_PURCHASED_HAIR ", sPedModelName, " haircut:", eCurrent_haircut, " / ", eUpgrade_haircut, ") tlCategoryKey:", tlCategoryKey, ", iCategoryHashkey:", GET_HASH_KEY(tlCategoryKey), ", result: ", iTransactionState)
					ENDIF
					
					SET_PED_COMP_ITEM_AVAILABLE_MP(ePedModel, COMP_TYPE_HAIR, eUpgrade_haircut, TRUE)
					SET_PED_COMP_ITEM_ACQUIRED_MP(ePedModel, COMP_TYPE_HAIR, eUpgrade_haircut, TRUE) 
					
					SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_HAIR, eUpgrade_haircut, FALSE) 
					
					SET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT, ENUM_TO_INT(eUpgrade_haircut),g_iPedComponentSlot)
					
					SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA, ENUM_TO_INT(eUpgrade_haircut),g_iPedComponentSlot)
					SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO, ENUM_TO_INT(eUpgrade_haircut),g_iPedComponentSlot)
					
					iTransactionState = GENERIC_TRANSACTION_STATE_DEFAULT
					g_bValidatedPlayerPurchasedHair = TRUE
				ENDIF
			ELSE
				SET_PED_COMP_ITEM_AVAILABLE_MP(ePedModel, COMP_TYPE_HAIR, eUpgrade_haircut, TRUE)
				SET_PED_COMP_ITEM_ACQUIRED_MP(ePedModel, COMP_TYPE_HAIR, eUpgrade_haircut, TRUE) 
				
				SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_HAIR, eUpgrade_haircut, FALSE) 
				
				SET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT, ENUM_TO_INT(eUpgrade_haircut),g_iPedComponentSlot)
				
				SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA, ENUM_TO_INT(eUpgrade_haircut),g_iPedComponentSlot)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO, ENUM_TO_INT(eUpgrade_haircut),g_iPedComponentSlot)
				
				CPRINTLN(DEBUG_SHOPS, "performed gr_hair: VALIDATE_PLAYER_PURCHASED_HAIR ", sPedModelName, " haircut:", eCurrent_haircut, " / ", eUpgrade_haircut, ")")
				iTransactionState = GENERIC_TRANSACTION_STATE_DEFAULT
				g_bValidatedPlayerPurchasedHair = TRUE
			ENDIF
		ELSE
			CPRINTLN(DEBUG_SHOPS, "bypass gr_hair: VALIDATE_PLAYER_PURCHASED_HAIR ", sPedModelName, " haircut:", eCurrent_haircut, ", dummy ", eUpgrade_haircut, ")")
			iTransactionState = GENERIC_TRANSACTION_STATE_DEFAULT
			g_bValidatedPlayerPurchasedHair = TRUE
		ENDIF
	ELSE
		//CDEBUG1LN(DEBUG_SHOPS, "gr_hair: VALIDATE_PLAYER_PURCHASED_HAIR ", sPedModelName, " haircut:", eCurrent_haircut, " = ", eUpgrade_haircut, ", ", eEquipped_haircut)
		iTransactionState = GENERIC_TRANSACTION_STATE_DEFAULT
		g_bValidatedPlayerPurchasedHair = TRUE
	ENDIF
ENDPROC

INT iPedHairValidationPrintChecks = -1
PROC PROCESS_INVALID_PLAYER_PURCHASED_HAIR()

	IF (iTransactionState = GENERIC_TRANSACTION_STATE_DEFAULT)
		IF NOT g_bValidatePlayersHairStats
		OR IS_PED_INJURED(PLAYER_PED_ID())
		OR NOT NETWORK_IS_GAME_IN_PROGRESS()
		OR NOT IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)
		OR NOT HAS_IMPORTANT_STATS_LOADED()
		OR IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()
		OR IS_BROWSER_OPEN()
		OR (GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR)
		OR (GET_JOINING_GAMEMODE() = GAMEMODE_CREATOR)
			IF (iPedHairValidationPrintChecks != 0)
				iPedHairValidationPrintChecks = 0
				CDEBUG1LN(DEBUG_SHOPS, "gr_hair: PROCESS_INVALID_PLAYER_PURCHASED_HAIR - safeguarded (",
						PICK_STRING(NOT g_bValidatePlayersHairStats,				" NOT g_bValidatePlayersHairStats",					""),
						PICK_STRING(IS_PED_INJURED(PLAYER_PED_ID()),				" IS_PED_INJURED(PLAYER_PED_ID())",					""),
						PICK_STRING(NOT NETWORK_IS_GAME_IN_PROGRESS(),				" NOT NETWORK_IS_GAME_IN_PROGRESS()",				""),
						PICK_STRING(NOT IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE),	" NOT IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)",	""),
						PICK_STRING(NOT HAS_IMPORTANT_STATS_LOADED(),				" NOT HAS_IMPORTANT_STATS_LOADED()",				""),
						PICK_STRING(IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP(),			" IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()",			""),
						PICK_STRING(IS_BROWSER_OPEN(),								" IS_BROWSER_OPEN()",								""),
						PICK_STRING((GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR),	" (GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR)",		""),
						PICK_STRING((GET_JOINING_GAMEMODE() = GAMEMODE_CREATOR),	" (GET_JOINING_GAMEMODE() = GAMEMODE_CREATOR)",		""),
						" )")
			ENDIF
			bPedHairTimerSet = FALSE
			EXIT
		ENDIF
		
		IF USE_SERVER_TRANSACTIONS()
			IF NET_GAMESERVER_IS_SESSION_REFRESH_PENDING()
				IF (iPedHairValidationPrintChecks != 1)
					iPedHairValidationPrintChecks = 1
					CDEBUG1LN(DEBUG_SHOPS, "gr_hair: PROCESS_INVALID_PLAYER_PURCHASED_HAIR - session refresh pending")
				ENDIF
				bPedHairTimerSet = FALSE
				EXIT
			ELIF (NET_GAMESERVER_TRANSACTION_IN_PROGRESS())
				IF (iPedHairValidationPrintChecks != 2)
					iPedHairValidationPrintChecks = 2
					CDEBUG1LN(DEBUG_SHOPS, "gr_hair: PROCESS_INVALID_PLAYER_PURCHASED_HAIR - transaction in progress")
				ENDIF
				bPedHairTimerSet = FALSE
				EXIT
			ENDIF
			
			INT iScriptTransactionIndex = GET_BASKET_TRANSACTION_SCRIPT_INDEX()
			IF iScriptTransactionIndex != -1
				IF (iPedHairValidationPrintChecks != 6)
					iPedHairValidationPrintChecks = 6
					CDEBUG1LN(DEBUG_SHOPS, "gr_hair: PROCESS_INVALID_PLAYER_PURCHASED_HAIR - basket transaction index: ", iScriptTransactionIndex)
				ENDIF
				bPedHairTimerSet = FALSE
				EXIT
			ENDIF
		ENDIF
		
		IF NOT bPedHairTimerSet
			tdPedHairTimer = GET_NETWORK_TIME()
			bPedHairTimerSet = TRUE
		ENDIF
		
		IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdPedHairTimer)) < 60000
			IF (iPedHairValidationPrintChecks != 3)
				iPedHairValidationPrintChecks = 3
				//CDEBUG3LN(DEBUG_SHOPS, "gr_hair: PROCESS_INVALID_PLAYER_PURCHASED_HAIR - waiting on timer")
			ENDIF
			EXIT
		ENDIF
		
		IF bFailedPedHairTimerSet
			IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdFailedPedHairTimer)) < 180000
				IF (iPedHairValidationPrintChecks != 4)
					iPedHairValidationPrintChecks = 4
					CDEBUG1LN(DEBUG_SHOPS, "gr_hair: PROCESS_INVALID_PLAYER_PURCHASED_HAIR - waiting on failed timer")
				ENDIF
				EXIT
			ENDIF
		ENDIF
		
		IF g_bValidatedPlayerPurchasedHair
		AND bPedHairTimerSet
			IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdPedHairTimer)) < 360000
				IF (iPedHairValidationPrintChecks != 5)
					iPedHairValidationPrintChecks = 5
					CDEBUG1LN(DEBUG_SHOPS, "gr_hair: PROCESS_INVALID_PLAYER_PURCHASED_HAIR - waiting on validated timer")
				ENDIF
				EXIT
			ENDIF
		ENDIF
	ENDIF
	
	bPedHairTimerSet = FALSE
	bFailedPedHairTimerSet = FALSE
	
	VALIDATE_PLAYER_PURCHASED_HAIR()
	iPedHairValidationPrintChecks = -1
	
	
ENDPROC

BOOL bCustomLogoTXTLoaded = FALSE
PROC PROCESS_CUSTOM_CREW_LOGO()

	BOOL bLoadCustomCrewLogoTXD = FALSE
	TEXT_LABEL_31 tlClubEmblem = ""

	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND HAS_IMPORTANT_STATS_LOADED()
	
		// Other players may need to get hold our own emblem selection so broadcast.
		GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iClubhouseEmblem = GET_MP_INT_CHARACTER_STAT(MP_STAT_CLBHOS_EMBLEM)
//		PRINTLN("[CREW_EMBLEM] [0] PROCESS_CUSTOM_CREW_LOGO - Setting emblem to: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iClubhouseEmblem)
		
		// If we have it set to crew but we're no longer in a crew then we need to switch it to none.
		IF GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iClubhouseEmblem = 0
		AND NOT IS_LOCAL_PLAYER_IN_ACTIVE_CLAN()
			GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iClubhouseEmblem = -1
//			PRINTLN("[CREW_EMBLEM] [1] PROCESS_CUSTOM_CREW_LOGO - Setting emblem to: ", GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iClubhouseEmblem)
		ENDIF
		
		// The custom logo assets that appears on clothing and vehicles need to be managed by script so we just
		// load in a single TXD with all the emblems in it. These are light on memory so we get away with loading in all under
		// the one TXD. We have single TXDs for higher res versions but it's up to the script using them to manage.
		bLoadCustomCrewLogoTXD = TRUE
		REQUEST_STREAMED_TEXTURE_DICT("MPClubEmblemSmall")
		IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPClubEmblemSmall")
			bCustomLogoTXTLoaded = TRUE
		ENDIF
		
		// Update global that tells us what custom logo each player should use.
		INT iPlayer
		REPEAT NUM_NETWORK_PLAYERS iPlayer
			IF GlobalPlayerBD_FM_3[iPlayer].iClubhouseEmblem > 0
			
				tlClubEmblem = "MPClubPreset"
				tlClubEmblem += GlobalPlayerBD_FM_3[iPlayer].iClubhouseEmblem
				
				IF IS_STRING_NULL_OR_EMPTY(g_tlCustomCrewLogoTXDTexture[iPlayer])
				OR NOT ARE_STRINGS_EQUAL(g_tlCustomCrewLogoTXDTexture[iPlayer], tlClubEmblem)
					PRINTLN("PROCESS_CUSTOM_CREW_LOGO - changing custom logo for player ", iPlayer, " from '", g_tlCustomCrewLogoTXDTexture[iPlayer], "' to '", tlClubEmblem, "'")
					g_tlCustomCrewLogoTXDTexture[iPlayer] = tlClubEmblem
					
					// Has my crew logo changed?
					IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_BIKER_GANG()
					AND GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
						IF iPlayer = NATIVE_TO_INT(PLAYER_ID())
						OR iPlayer = NATIVE_TO_INT(GB_GET_LOCAL_PLAYER_GANG_BOSS())
							g_sMagnateOutfitStruct.bPerformGeneralRefresh = TRUE
							PRINTLN("PROCESS_CUSTOM_CREW_LOGO - My logo, or my bosses logo, has changed whilst we're wearing an outfit. I should update it.")
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT IS_BIT_SET(g_iCustomCrewLogoUsePresetBitset, iPlayer)
					PRINTLN("PROCESS_CUSTOM_CREW_LOGO - setting override for player ", iPlayer)
					SET_BIT(g_iCustomCrewLogoUsePresetBitset, iPlayer)
				ENDIF
				
			ELIF IS_BIT_SET(g_iCustomCrewLogoUsePresetBitset, iPlayer)
				PRINTLN("PROCESS_CUSTOM_CREW_LOGO - clearing override for player ", iPlayer)
				CLEAR_BIT(g_iCustomCrewLogoUsePresetBitset, iPlayer)
			ENDIF
		ENDREPEAT
	ENDIF
	
	IF bCustomLogoTXTLoaded
	AND NOT bLoadCustomCrewLogoTXD
		SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPClubEmblemSmall")
		bCustomLogoTXTLoaded = FALSE
	ENDIF
ENDPROC

INT iIlluminateClothingGameTimes[NUM_NETWORK_PLAYERS]
CONST_INT ILLUMINATE_CLOTHING_ON 		0
CONST_INT ILLUMINATE_CLOTHING_FLASH		1
CONST_INT ILLUMINATE_CLOTHING_PULSE		2
CONST_INT ILLUMINATE_CLOTHING_OFF		3

#IF IS_DEBUG_BUILD
INT iCachedIlluminatedClothingState = -1
#ENDIF

PROC PROCESS_ILLUMINATED_CLOTHING()
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR NOT HAVE_STATS_LOADED()
		EXIT
	ENDIF
	
	// Visual state for local player
	INT iLocalPlayerIlluminatedClothingState = GET_MP_INT_CHARACTER_STAT(MP_STAT_ILLUMINATED_CLOTHING)
	
	// Force the 'on' state if we are not wearing the appropriate gear or if the ability to control has been been disabled.
	IF iLocalPlayerIlluminatedClothingState != ILLUMINATE_CLOTHING_ON
		PED_INDEX pedID = PLAYER_PED_ID()
		INT iDLCJbibHash = GET_HASH_NAME_FOR_COMPONENT(NATIVE_TO_INT(pedID), ENUM_TO_INT(PED_COMP_JBIB), GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_JBIB), GET_PED_TEXTURE_VARIATION(pedID, PED_COMP_JBIB))
		
		IF NOT (DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCJbibHash, DLC_RESTRICTION_TAG_DEADLINE_OUTFIT, ENUM_TO_INT(SHOP_PED_COMPONENT))
		OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCJbibHash, DLC_RESTRICTION_TAG_MORPH_SUIT, ENUM_TO_INT(SHOP_PED_COMPONENT))
		OR DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCJbibHash, DLC_RESTRICTION_TAG_LIGHT_UP, ENUM_TO_INT(SHOP_PED_COMPONENT))
		OR DOES_CURRENT_PED_COMPONENT_HAVE_RESTRICTION_TAG(pedID, PED_COMP_SPECIAL, DLC_RESTRICTION_TAG_LIGHT_UP)
		OR DOES_CURRENT_PED_PROP_HAVE_RESTRICTION_TAG(pedID, ENUM_TO_INT(ANCHOR_HEAD), DLC_RESTRICTION_TAG_LIGHT_UP)
		OR DOES_CURRENT_PED_PROP_HAVE_RESTRICTION_TAG(pedID, ENUM_TO_INT(ANCHOR_LEFT_WRIST), DLC_RESTRICTION_TAG_LIGHT_UP)
		OR DOES_CURRENT_PED_PROP_HAVE_RESTRICTION_TAG(pedID, ENUM_TO_INT(ANCHOR_RIGHT_WRIST), DLC_RESTRICTION_TAG_LIGHT_UP)
		OR DOES_CURRENT_PED_COMPONENT_HAVE_RESTRICTION_TAG(pedID, PED_COMP_FEET, DLC_RESTRICTION_TAG_LIGHT_UP)
		OR DOES_CURRENT_PED_COMPONENT_HAVE_RESTRICTION_TAG(pedID, PED_COMP_BERD, DLC_RESTRICTION_TAG_LIGHT_UP))
			iLocalPlayerIlluminatedClothingState = ILLUMINATE_CLOTHING_ON
			#IF IS_DEBUG_BUILD
			IF iCachedIlluminatedClothingState != iLocalPlayerIlluminatedClothingState
				PRINTLN("PROCESS_ILLUMINATED_CLOTHING: Overriding state to ON - not wearing applicable gear")
				iCachedIlluminatedClothingState = iLocalPlayerIlluminatedClothingState
			ENDIF
			#ENDIF
		ENDIF
		
		IF NOT PLAYER_CAN_CHANGE_ILLUMINATED_CLOTHING()
			iLocalPlayerIlluminatedClothingState = ILLUMINATE_CLOTHING_ON
			#IF IS_DEBUG_BUILD
			IF iCachedIlluminatedClothingState != iLocalPlayerIlluminatedClothingState
				PRINTLN("PROCESS_ILLUMINATED_CLOTHING: Overriding state to ON - not allowed to change state")
				iCachedIlluminatedClothingState = iLocalPlayerIlluminatedClothingState
			ENDIF
			#ENDIF
		ENDIF
		
		#IF IS_DEBUG_BUILD
		IF iLocalPlayerIlluminatedClothingState != ILLUMINATE_CLOTHING_ON
			PRINTLN("PROCESS_ILLUMINATED_CLOTHING: Clearing override")
			iCachedIlluminatedClothingState = iLocalPlayerIlluminatedClothingState
		ENDIF
		#ENDIF
	ENDIF
	GlobalPlayerBD_FM_3[NATIVE_TO_INT(PLAYER_ID())].iIlluminatedClothing = iLocalPlayerIlluminatedClothingState
	
	// Visual state on all players
	INT iGameTime = GET_GAME_TIMER()
	FLOAT fEmissive
	FLOAT fDesiredEmissive, fDesiredFlash, fDesiredPulse
	INT iPlayer
	REPEAT NUM_NETWORK_PLAYERS iPlayer
		PED_INDEX pedId = GET_PLAYER_PED(INT_TO_PLAYERINDEX(iPlayer))
		
		IF NOT IS_PED_INJURED(pedId)
		AND DOES_ENTITY_HAVE_DRAWABLE(pedId)
		AND DOES_ENTITY_HAVE_PHYSICS(pedId)
		AND HAVE_ALL_STREAMING_REQUESTS_COMPLETED(pedId)
		AND IS_PED_SHADER_READY(pedId)
		
			fEmissive = GET_PED_EMISSIVE_SCALE(pedId)
			fDesiredEmissive = -1.0
			fDesiredFlash = 0.0
			fDesiredPulse = 0.0
			
			SWITCH GlobalPlayerBD_FM_3[iPlayer].iIlluminatedClothing
				CASE ILLUMINATE_CLOTHING_ON
					CONST_FLOAT fILLU_0_ON		1.0
					IF fEmissive != fILLU_0_ON
						fDesiredEmissive = (fILLU_0_ON)
						iIlluminateClothingGameTimes[iPlayer] = iGameTime
					ENDIF
				BREAK
				CASE ILLUMINATE_CLOTHING_FLASH
					IF iIlluminateClothingGameTimes[iPlayer] = -1
						iIlluminateClothingGameTimes[iPlayer] = iGameTime
					ENDIF
					fDesiredFlash = fEmissive
					IF fDesiredFlash < 0.5
						fDesiredFlash = 0.0
					ELSE
						fDesiredFlash = 1.0
					ENDIF
					
					IF iGameTime >= (iIlluminateClothingGameTimes[iPlayer]+iILLUMINATED_FLASH_TIME)
						IF fDesiredFlash < 0.5
							fDesiredFlash = 1.0
						ELSE
							fDesiredFlash = 0.0
						ENDIF
					ENDIF
					
					IF fEmissive != fDesiredFlash
						fDesiredEmissive = (fDesiredFlash)
						iIlluminateClothingGameTimes[iPlayer] = iGameTime
					ENDIF
				BREAK
				CASE ILLUMINATE_CLOTHING_PULSE
					IF iIlluminateClothingGameTimes[iPlayer] = -1
						iIlluminateClothingGameTimes[iPlayer] = iGameTime
					ENDIF
					
					fDesiredPulse = SIN(TO_FLOAT(iGameTime-iIlluminateClothingGameTimes[iPlayer]) * fILLUMINATED_PULSE_FREQUENCY)
					fDesiredPulse = (fDesiredPulse+1.0)*0.5
					IF fEmissive != fDesiredPulse
						fDesiredEmissive = (fDesiredPulse)
					ENDIF
				BREAK
				CASE ILLUMINATE_CLOTHING_OFF
					CONST_FLOAT fILLU_3_OFF		0.0
					IF fEmissive != fILLU_3_OFF
						fDesiredEmissive = (fILLU_3_OFF)
						iIlluminateClothingGameTimes[iPlayer] = iGameTime
					ENDIF
				BREAK
				DEFAULT
					ASSERTLN("PROCESS_ILLUMINATED_CLOTHING ", iPlayer, ": unknown iIlluminatedClothing state: ", GlobalPlayerBD_FM_3[iPlayer].iIlluminatedClothing)
					fDesiredEmissive = 1.0
					iIlluminateClothingGameTimes[iPlayer] = iGameTime
				BREAK
			ENDSWITCH
			
			IF (fDesiredEmissive != -1.0)
				IF (fDesiredEmissive < 0.0)	fDesiredEmissive = 0.0	ENDIF
				IF (fDesiredEmissive > 1.0)	fDesiredEmissive = 1.0	ENDIF
				IF fEmissive != fDesiredEmissive
					SET_PED_EMISSIVE_SCALE(pedId, fDesiredEmissive)
					PRINTLN("PROCESS_ILLUMINATED_CLOTHING ", iPlayer, ": SET_PED_EMISSIVE_SCALE(", fDesiredEmissive, ")")
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

BOOL bTriggerUnlock = FALSE
PROC PROCESS_RESEARCH_UNLOCK()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF g_bTriggerResearchUnlockTicker
		AND IS_SAFE_TO_DISPLAY_BUNKER_RESEARCH_UNLOCK_TICKER()
			IF NOT IS_STRING_NULL_OR_EMPTY(g_tlResearchUnlockItem)
			AND DOES_TEXT_LABEL_EXIST(g_tlResearchUnlockItem)
				bTriggerUnlock = TRUE
			ENDIF
		ENDIF
		
		IF bTriggerUnlock
			IF g_bResearchTriggerTxt
				IF Request_MP_Comms_Txtmsg_With_Components(CHAR_MP_AGENT_14, g_tlResearchUnlockTxt, g_tlResearchUnlockItem)
					IF g_bResearchTriggerFeed
						PRINT_TICKER_WITH_STRING("RSRCH_UNLCK_TKR", g_tlResearchUnlockItem)
					ENDIF
					g_bTriggerResearchUnlockTicker = FALSE
					g_tlResearchUnlockItem = ""
					bTriggerUnlock = FALSE
				ELSE
					PRINTLN("[BUNKER_RESEARCH] - PROCESS_RESEARCH_UNLOCK - Holding up the messaging for Request_MP_Comms_Txtmsg_With_Components")
				ENDIF
			ELSE
				IF g_bResearchTriggerFeed
					PRINT_TICKER_WITH_STRING("RSRCH_UNLCK_TKR", g_tlResearchUnlockItem)
					g_bTriggerResearchUnlockTicker = FALSE
					g_tlResearchUnlockItem = ""
					bTriggerUnlock = FALSE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		g_bTriggerResearchUnlockTicker = FALSE
		g_tlResearchUnlockItem = ""
		bTriggerUnlock = FALSE
	ENDIF
ENDPROC

FUNC BOOL IS_PLAYER_IN_ANY_INTERIOR()
	RETURN GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].SimpleInteriorBD.eCurrentSimpleInterior <> SIMPLE_INTERIOR_INVALID
			OR IS_LOCAL_PLAYER_WALKING_IN_OR_OUT_OF_SIMPLE_INTERIOR()
			OR IS_PLAYER_IN_PROPERTY(PLAYER_ID(), TRUE, TRUE)
ENDFUNC


PROC PROCESS_SCUBA_GEAR()

	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR NOT g_bValidatePlayersScubaGear
	OR NETWORK_IS_ACTIVITY_SESSION() AND IS_THIS_ROCKSTAR_MISSION_GANGOPS_MISSION_OF_TYPE(ciGANGOPS_FLOW_MISSION_SUBMARINE_FINALE)
	OR g_bBlockScubaGear
		EXIT
	ENDIF
	
	#IF FEATURE_CASINO
	IF NOT bCheckedScubaOutfitOwnership
		MODEL_NAMES ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
		IF (ePedModel = MP_M_FREEMODE_01)
			IF IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_0"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_1"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_2"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_3"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_4"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_5"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_6"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_7"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_8"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_9"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_10"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_11"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_12"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_13"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_14"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_15"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_16"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_17"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_18"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_19"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_20"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_M_OUTFIT_SCUBA_21"), PED_COMPONENT_ACQUIRED_SLOT)
				IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SCUBA_OUTFIT_PURCH)
					CPRINTLN(DEBUG_PED_COMP, "Already owns a male scuba outfit")
				ELSE
					CPRINTLN(DEBUG_PED_COMP, "Owned a male scuba outfit, setting packstat")
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SCUBA_OUTFIT_PURCH, TRUE)
				ENDIF
			ENDIF
			bCheckedScubaOutfitOwnership = TRUE
		ELIF (ePedModel = MP_F_FREEMODE_01)
			IF IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_0"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_1"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_2"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_3"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_4"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_5"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_6"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_7"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_8"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_9"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_10"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_11"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_12"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_13"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_14"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_15"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_16"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_17"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_18"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_19"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_20"), PED_COMPONENT_ACQUIRED_SLOT)
			OR IS_BIT_SET_SHOP_PED_APPAREL_SCRIPT(HASH("DLC_MP_X17_F_OUTFIT_SCUBA_21"), PED_COMPONENT_ACQUIRED_SLOT)
				IF GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SCUBA_OUTFIT_PURCH)
					CPRINTLN(DEBUG_PED_COMP, "Already owns a female scuba outfit")
				ELSE
					CPRINTLN(DEBUG_PED_COMP, "Owned a female scuba outfit, setting packstat")
					SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SCUBA_OUTFIT_PURCH, TRUE)
				ENDIF
			ENDIF
			bCheckedScubaOutfitOwnership = TRUE
		ENDIF
	ENDIF
	#ENDIF
	
	BOOL bIsPlayerInThruster = FALSE
	IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
		VEHICLE_INDEX vehID = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(), TRUE)
		IF DOES_ENTITY_EXIST(vehID)
		AND IS_VEHICLE_DRIVEABLE(vehID)
		AND GET_ENTITY_MODEL(vehID) = THRUSTER
			// Safe to process
			// url:bugstar:4199387 record that we are in a thruster so we can take off scuba gear if necessary
			bIsPlayerInThruster = TRUE
		ELSE
			EXIT
		ENDIF
	ENDIF
	
	
	
	
	IF pedDLCFeetHashPed != PLAYER_PED_ID()
	OR iPedFeetDrawableVariation != GET_PED_DRAWABLE_VARIATION(pedDLCFeetHashPed, PED_COMP_FEET)
		pedDLCFeetHashPed = PLAYER_PED_ID()
		iPedFeetDrawableVariation = GET_PED_DRAWABLE_VARIATION(pedDLCFeetHashPed, PED_COMP_FEET)
		iDLCFeetHash = GET_HASH_NAME_FOR_COMPONENT( NATIVE_TO_INT(pedDLCFeetHashPed), ENUM_TO_INT(PED_COMP_FEET), iPedFeetDrawableVariation, GET_PED_TEXTURE_VARIATION(pedDLCFeetHashPed, PED_COMP_FEET))	
	ENDIF
	
	IF iDLCFeetHash != 0
	AND DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCFeetHash, DLC_RESTRICTION_TAG_SCUBA_GEAR, ENUM_TO_INT(SHOP_PED_COMPONENT))
		
		IF IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
		AND NOT IS_PLAYER_IN_ANY_INTERIOR()
		AND NOT GET_MP_LIGHT_ENABLED(PLAYER_PED_ID())
			PRINTLN("[scuba] scuba light off, turning on")
			ENABLE_MP_LIGHT(PLAYER_PED_ID(), TRUE)
		ENDIF

//		FLOAT fHeight
//		VECTOR vPlayerCoords = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
		BOOL bUseFlippers
		
		IF DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCFeetHash, DLC_RESTRICTION_TAG_SCUBA_FLIPPERS, ENUM_TO_INT(SHOP_PED_COMPONENT))
			bUseFlippers = TRUE
			
			IF IS_BIT_SET(iBsScubaGear, ciSCUBA_BS_GEAR_FORCE_EQUIPPED)
				// Toggle off when leave water after having force equipped
				/*** CODE NOW REMOVE THE ACCS WHEN WE GET OUT SO WE QUERY IS_USING_PED_SCUBA_GEAR_VARIATION INSTEAD ***/
				/*IF GET_WATER_HEIGHT(vPlayerCoords, fHeight)
					IF fHeight-vPlayerCoords.z < -0.3
					AND NOT IS_PED_SWIMMING(PLAYER_PED_ID())
					AND NOT IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
						bUseFlippers = FALSE
					ENDIF
				ELSE
					bUseFlippers = FALSE
				ENDIF*/
				
				IF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
					PRINTLN("[scuba] IS_PED_ON_ANY_BIKE == true. Setting bUseFlippers = false")					
					bUseFlippers = FALSE
				ELIF IS_PED_RAGDOLL(PLAYER_PED_ID())
					PRINTLN("[scuba] IS_PED_RAGDOLL == true. Setting bUseFlippers = false")					
					bUseFlippers = FALSE
				ELIF NOT IS_USING_PED_SCUBA_GEAR_VARIATION(PLAYER_PED_ID())
					PRINTLN("[scuba] IS_USING_PED_SCUBA_GEAR_VARIATION == false. Setting bUseFlippers = false")					
					bUseFlippers = FALSE
				ENDIF
			ELSE
				PRINTLN("[scuba] IS_BIT_SET(iBsScubaGear, ciSCUBA_BS_GEAR_FORCE_EQUIPPED) == false")
				// Set force equipped flag when we enter water with them already on
//				IF GET_WATER_HEIGHT(vPlayerCoords, fHeight)
//					IF fHeight-vPlayerCoords.z < -0.3
					IF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
						bUseFlippers = FALSE
					ELIF IS_PED_RAGDOLL(PLAYER_PED_ID())
						//
					ELIF NOT IS_PED_SWIMMING(PLAYER_PED_ID())
					AND NOT IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
						PRINTLN("[scuba] Player is not swimming")
					ELSE
						PRINTLN("[scuba] PROCESS_SCUBA_GEAR() - Entered water with flippers")
						SET_BIT(iBsScubaGear, ciSCUBA_BS_GEAR_FORCE_EQUIPPED)
					ENDIF
//				ENDIF
			ENDIF
		ELSE
			PRINTLN("[scuba] DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(DLC_RESTRICTION_TAG_SCUBA_FLIPPERS) == false")
			bUseFlippers = FALSE
//			IF GET_WATER_HEIGHT(vPlayerCoords, fHeight)
//				IF fHeight-vPlayerCoords.z >= 0.4
				IF IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
					// Skip
				ELIF IS_PED_RAGDOLL(PLAYER_PED_ID())
					// Skip
				ELIF IS_PED_SWIMMING(PLAYER_PED_ID())
				OR IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())
					IF IS_PLAYER_IN_ANY_INTERIOR()
					OR bIsPlayerInThruster
						// Skip
						PRINTLN("[scuba] Setting bUseFlippers = FALSE")
					ELSE
						PRINTLN("[scuba] Setting bUseFlippers = TRUE")
						bUseFlippers = TRUE
					ENDIF
				ENDIF
//			ENDIF
		ENDIF
		
		IF (DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(iDLCFeetHash, DLC_RESTRICTION_TAG_SCUBA_FLIPPERS, ENUM_TO_INT(SHOP_PED_COMPONENT)) != bUseFlippers)			
			INT iAltFeetHash
			scrShopPedComponent componentItem
			MODEL_NAMES ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
			IF GET_ALT_DLC_PED_COMP_OF_TYPE(iDLCFeetHash, PED_COMP_FEET, iAltFeetHash)
				PRINTLN("[scuba] GET_ALT_DLC_PED_COMP_OF_TYPE() == true")
				GET_SHOP_PED_COMPONENT(iAltFeetHash, componentItem)
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET, componentItem.m_drawableIndex, componentItem.m_textureIndex)
			ENDIF
			
			IF bUseFlippers
			
				// Remove the parachute
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND, 0, 0)
				
				// Forced components all set up in the meta data
				PED_COMP_NAME_ENUM eFlipperFeet = GET_PED_COMP_ITEM_FROM_VARIATIONS(PLAYER_PED_ID(), componentItem.m_drawableIndex, componentItem.m_textureIndex, COMP_TYPE_FEET)
				IF eFlipperFeet != DUMMY_PED_COMP
					SET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_FEET, eFlipperFeet, FALSE) 
				ENDIF
				CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_HEAD)
				
				IF ePedModel = MP_M_FREEMODE_01
					SET_PLAYER_PREVIOUS_VARIATION_DATA(PLAYER_ID(), PED_COMP_SPECIAL, 15, 0, 0, 0)
				ELIF ePedModel = MP_F_FREEMODE_01
					SET_PLAYER_PREVIOUS_VARIATION_DATA(PLAYER_ID(), PED_COMP_SPECIAL, 3, 0, 0, 0)
				ENDIF
				
				PRINTLN("PROCESS_SCUBA_GEAR() - Entered water with scuba boots")
				
				SET_BIT(iBsScubaGear, ciSCUBA_BS_GEAR_FORCE_EQUIPPED)
				SET_ENABLE_SCUBA(PLAYER_PED_ID(), TRUE)
			ELSE
				PRINTLN("[scuba] bUseFlippers == false")
				
				// Remove mask
				SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_BERD, 0, 0)
				RESET_PLAYER_HEAD_BLEND_TO_NORM(PLAYER_PED_ID())
				
				// Remove tank
				/*** CODE NOW REMOVE THE ACCS BUT WE STILL NEED TO SEAT DEFAULT ***/
				/*IF ePedModel = MP_M_FREEMODE_01
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 15, 0)
				ELIF ePedModel = MP_F_FREEMODE_01
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 3, 0)
				ENDIF*/
				
				// Remove goggles
				/*** CODE NOW REMOVE THE MASK SO NO NEED TO CLEAR ***/
				//CLEAR_PED_PROP(PLAYER_PED_ID(), ANCHOR_EYES)
				
				IF IS_BIT_SET(iBsScubaGear, ciSCUBA_BS_GEAR_FORCE_EQUIPPED)
					PRINTLN("[scuba] PROCESS_SCUBA_GEAR() - Left the water with flippers")
					CLEAR_BIT(iBsScubaGear, ciSCUBA_BS_GEAR_FORCE_EQUIPPED)
				ENDIF
				SET_ENABLE_SCUBA(PLAYER_PED_ID(), FALSE)
			ENDIF		
		ENDIF
		SET_BIT(iBsScubaGear, ciSCUBA_BS_USING_GEAR_PREV_FRAME)
	ELSE
		IF IS_BIT_SET(iBsScubaGear, ciSCUBA_BS_USING_GEAR_PREV_FRAME)
		AND GET_MP_LIGHT_ENABLED(PLAYER_PED_ID())
			PRINTLN("[scuba] scuba light on, turning off")
			ENABLE_MP_LIGHT(PLAYER_PED_ID(), FALSE)
		ENDIF
		CLEAR_BIT(iBsScubaGear, ciSCUBA_BS_USING_GEAR_PREV_FRAME)
	ENDIF
ENDPROC

#IF FEATURE_FIXER
PROC MAINTAIN_SECURITY_CONTRACT_LAUNCING_WITH_DELAY()
	IF IS_BIT_SET(g_sFixerFlow.Bitset3, ciFIXER_FLOW_BITSET3__PROMPTED_REQUEST_START_SECURITY_CONTRACT)
		IF NOT HAS_NET_TIMER_STARTED(SecurityContractLaunchDelayTimer)
			START_NET_TIMER(SecurityContractLaunchDelayTimer)
		ELIF HAS_NET_TIMER_EXPIRED(SecurityContractLaunchDelayTimer, 10000)
			REQUEST_LAUNCH_GB_MISSION(FMMC_TYPE_FIXER_SECURITY, ENUM_TO_INT(GET_PLAYER_FIXER_SECURITY_CONTRACT_IN_SLOT(PLAYER_ID(), GET_RANDOM_INT_IN_RANGE(0, 3))))
			RESET_NET_TIMER(SecurityContractLaunchDelayTimer)
			CLEAR_BIT(g_sFixerFlow.Bitset3, ciFIXER_FLOW_BITSET3__PROMPTED_REQUEST_START_SECURITY_CONTRACT)
		ENDIF
	ENDIF
ENDPROC
#ENDIF

INT iRadioUnlockBitset = 0
BOOL bInitialRadioUpdateDone = FALSE
INT iBattleMixLockState = -1
BOOL bCached_music_mixdisplay = FALSE
PROC MAINTAIN_RADIO_UNLOCKS()
	IF NOT IS_SKYSWOOP_AT_GROUND()
		bInitialRadioUpdateDone = FALSE
		EXIT
	ENDIF

	IF !(GET_CURRENT_GAMEMODE() = GAMEMODE_CREATOR)
	AND !(GET_CURRENT_GAMEMODE() = GAMEMODE_FM)
	AND !(GET_CURRENT_GAMEMODE() = GAMEMODE_SP)
		IF iBattleMixLockState != 0
			// Fix for url:bugstar:5040430 - Can we block LSUR in single player please
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_RADIO_UNLOCKS - Locking RADIO_22_DLC_BATTLE_MIX1_RADIO for SP (GET_CURRENT_GAMEMODE() = ", GET_GAMEMODE_STRING(GET_CURRENT_GAMEMODE()), ")")
			LOCK_RADIO_STATION("RADIO_22_DLC_BATTLE_MIX1_RADIO", TRUE)
			
//			#IF FEATURE_HEIST_ISLAND
//			LOCK_RADIO_STATION("RADIO_27_DLC_PRHEI4", TRUE)
//			LOCK_RADIO_STATION("RADIO_34_DLC_HEI4_KULT", TRUE)
//			LOCK_RADIO_STATION("RADIO_35_DLC_HEI4_MLR", TRUE)
//			#ENDIF
			
			iBattleMixLockState = 0
		ENDIF
		
		iRadioUnlockBitset = -1
		
		// Bail out.
		EXIT
	ENDIF
	
	IF NOT g_bHasCopiedAllTunableValuesToScriptGlobals 
		EXIT
	ENDIF
	
	IF (GET_FRAME_COUNT() % 120) = 0
	OR bCached_music_mixdisplay != g_bmusic_mixdisplay
	
		INT iRadioUnlockBitset_Temp
	
		IF g_sMPTunables.bENABLE_LSUR_SOL
			SET_BIT(iRadioUnlockBitset_Temp, 0)
		ENDIF
		IF g_sMPTunables.bENABLE_LSUR_TOS
			SET_BIT(iRadioUnlockBitset_Temp, 1)
		ENDIF
		IF g_sMPTunables.bENABLE_LSUR_DIX
			SET_BIT(iRadioUnlockBitset_Temp, 2)
		ENDIF
		IF g_sMPTunables.bENABLE_LSUR_BM
			SET_BIT(iRadioUnlockBitset_Temp, 3)
		ENDIF

		IF iRadioUnlockBitset_Temp = 0
			IF iBattleMixLockState != 0
				IF IS_BIT_SET(iRadioUnlockBitset_Temp, 0)
				OR IS_BIT_SET(iRadioUnlockBitset_Temp, 1)
				OR IS_BIT_SET(iRadioUnlockBitset_Temp, 2)
				OR IS_BIT_SET(iRadioUnlockBitset_Temp, 3)
					LOCK_RADIO_STATION("RADIO_22_DLC_BATTLE_MIX1_RADIO", TRUE)
					CPRINTLN(DEBUG_SHOPS, "MAINTAIN_RADIO_UNLOCKS - Locking RADIO_22_DLC_BATTLE_MIX1_RADIO for MP")
				ELSE
					LOCK_RADIO_STATION("RADIO_22_DLC_BATTLE_MIX1_RADIO", FALSE)
					CPRINTLN(DEBUG_SHOPS, "MAINTAIN_RADIO_UNLOCKS - unLocking RADIO_22_DLC_BATTLE_MIX1_RADIO for MP")
				ENDIF

				iBattleMixLockState = 0
			ENDIF
			
			iRadioUnlockBitset = -1
		ELSE
			IF NOT bInitialRadioUpdateDone
				CPRINTLN(DEBUG_SHOPS, "MAINTAIN_RADIO_UNLOCKS - Calling UPDATE_UNLOCKABLE_DJ_RADIO_TRACKS(TRUE)")
				UPDATE_UNLOCKABLE_DJ_RADIO_TRACKS(TRUE)
			ELIF iRadioUnlockBitset != iRadioUnlockBitset_Temp
				CPRINTLN(DEBUG_SHOPS, "MAINTAIN_RADIO_UNLOCKS - Calling UPDATE_UNLOCKABLE_DJ_RADIO_TRACKS(FALSE) for iRadioUnlockBitset = ", iRadioUnlockBitset)
				UPDATE_UNLOCKABLE_DJ_RADIO_TRACKS(FALSE)
			ELSE
				CDEBUG3LN(DEBUG_SHOPS, "MAINTAIN_RADIO_UNLOCKS - No reason to call UPDATE_UNLOCKABLE_DJ_RADIO_TRACKS as iRadioUnlockBitset = ", iRadioUnlockBitset)
			ENDIF
			
			bInitialRadioUpdateDone = TRUE
			iRadioUnlockBitset = iRadioUnlockBitset_Temp
			iBattleMixLockState = -1
		ENDIF
		
		bCached_music_mixdisplay = g_bmusic_mixdisplay
	ENDIF
ENDPROC

PROC MAINTAIN_ISLAND_HEIST_RADIO_UNLOCK()

	IF NOT IS_SKYSWOOP_AT_GROUND()
		CLEAR_BIT(iRadioLockBS, RADIO_LOCK_BS_CACHE_ISLAND)
		EXIT
	ENDIF	
	
	IF IS_BIT_SET(iRadioLockBS, RADIO_LOCK_BS_CACHE_ISLAND)
		EXIT
	ENDIF	
	
	IF NOT g_bHasCopiedAllTunableValuesToScriptGlobals 
		EXIT
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_UnlockAllHeistIslandRadios")
		LOCK_RADIO_STATION("RADIO_27_DLC_PRHEI4", FALSE)
		LOCK_RADIO_STATION("RADIO_34_DLC_HEI4_KULT", FALSE)
		LOCK_RADIO_STATION("RADIO_35_DLC_HEI4_MLR", FALSE)
		SET_BIT(iRadioLockBS, RADIO_LOCK_BS_CACHE_ISLAND)
		CPRINTLN(DEBUG_SHOPS, "MAINTAIN_ISLAND_HEIST_RADIO_UNLOCK - using sc_UnlockAllHeistIslandRadios to unlock all radios")
		EXIT
	ENDIF
	#ENDIF
	
	BOOL bLockAll = FALSE
	IF (GET_CURRENT_GAMEMODE() != GAMEMODE_FM AND GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR)
	OR IS_DIRECTOR_MODE_RUNNING() 
		bLockAll = TRUE
	ENDIF

	INT iRadioUnlockBitset_Temp
	
	IF !g_sMPTunables.IH_RADIO_ENABLE_STILL_SLIPPIN_LIMITED
	AND !g_sMPTunables.IH_RADIO_ENABLE_STILL_SLIPPIN_FULL
		SET_BIT(iRadioUnlockBitset_Temp, 0)
	ENDIF
	IF !g_sMPTunables.IH_RADIO_ENABLE_STILL_SLIPPIN_FULL	
		SET_BIT(iRadioUnlockBitset_Temp, 1)
	ENDIF
	IF !g_sMPTunables.IH_RADIO_ENABLE_MUSIC_LOCKER
		SET_BIT(iRadioUnlockBitset_Temp, 2)
	ENDIF
	IF !g_sMPTunables.IH_RADIO_ENABLE_KULT_FM
		SET_BIT(iRadioUnlockBitset_Temp, 3)
	ENDIF
	
	IF !bLockAll
		IF IS_BIT_SET(iRadioUnlockBitset_Temp, 0)	 
		OR IS_BIT_SET(iRadioUnlockBitset_Temp, 1)	
			LOCK_RADIO_STATION("RADIO_27_DLC_PRHEI4", TRUE)
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_ISLAND_HEIST_RADIO_UNLOCK - Locking RADIO_27_DLC_PRHEI4 for MP")
			CLEAR_BIT(collectables_missiondata_main.icollectablesbitset ,  28 ) //BITSET_PIRATE_RADIO_STATION_UNLOCKED)
		ELSE
			LOCK_RADIO_STATION("RADIO_27_DLC_PRHEI4", FALSE)
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_ISLAND_HEIST_RADIO_UNLOCK - unLocking RADIO_27_DLC_PRHEI4 for MP")
			SET_BIT(collectables_missiondata_main.icollectablesbitset , 28 ) //BITSET_PIRATE_RADIO_STATION_UNLOCKED)
		ENDIF
		
		IF IS_BIT_SET(iRadioUnlockBitset_Temp, 2)	 
			LOCK_RADIO_STATION("RADIO_35_DLC_HEI4_MLR", TRUE)
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_ISLAND_HEIST_RADIO_UNLOCK - Locking RADIO_35_DLC_HEI4_MLR for MP")
		ELSE
			LOCK_RADIO_STATION("RADIO_35_DLC_HEI4_MLR", FALSE)
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_ISLAND_HEIST_RADIO_UNLOCK - unLocking RADIO_35_DLC_HEI4_MLR for MP")
		ENDIF	
		
		IF IS_BIT_SET(iRadioUnlockBitset_Temp, 3)	 
			LOCK_RADIO_STATION("RADIO_34_DLC_HEI4_KULT", TRUE)
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_ISLAND_HEIST_RADIO_UNLOCK - locking RADIO_34_DLC_HEI4_KULT for MP")
		ELSE
			LOCK_RADIO_STATION("RADIO_34_DLC_HEI4_KULT", FALSE)
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_ISLAND_HEIST_RADIO_UNLOCK - unLocking RADIO_34_DLC_HEI4_KULT for MP")
		ENDIF
	ELSE			
		LOCK_RADIO_STATION("RADIO_27_DLC_PRHEI4", TRUE)
		LOCK_RADIO_STATION("RADIO_34_DLC_HEI4_KULT", TRUE)
		LOCK_RADIO_STATION("RADIO_35_DLC_HEI4_MLR", TRUE)
		CPRINTLN(DEBUG_SHOPS, "MAINTAIN_ISLAND_HEIST_RADIO_UNLOCK - lock all")
	ENDIF
	
	SET_BIT(iRadioLockBS, RADIO_LOCK_BS_CACHE_ISLAND)
ENDPROC

#IF FEATURE_TUNER
PROC MAINTAIN_TUNER_RADIO_UNLOCK()

	IF NOT IS_SKYSWOOP_AT_GROUND()
		CLEAR_BIT(iRadioLockBS, RADIO_LOCK_BS_CACHE_TUNER)
		EXIT
	ENDIF	
	
	IF IS_BIT_SET(iRadioLockBS, RADIO_LOCK_BS_CACHE_TUNER)
		EXIT
	ENDIF	
	
	IF NOT g_bHasCopiedAllTunableValuesToScriptGlobals 
		EXIT
	ENDIF
	
	IF IS_PLAYER_IN_CORONA()
		IF CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_STREET_RACE_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
		OR CV2_IS_THIS_ROOT_CONTENT_A_CORONA_FLOW_PURSUIT_SERIES(g_FMMC_STRUCT.iRootContentIDHash)
			EXIT
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_UnlockAllTunerRadios")
		LOCK_RADIO_STATION("RADIO_36_AUDIOPLAYER", FALSE)
		CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - using sc_UnlockAllTunerRadios to unlock all radios")
		SET_BIT(iRadioLockBS, RADIO_LOCK_BS_CACHE_TUNER)
	ENDIF
	#ENDIF
	
	BOOL bLockAll = FALSE
	IF (GET_CURRENT_GAMEMODE() != GAMEMODE_FM AND GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR)
	OR IS_DIRECTOR_MODE_RUNNING() 
		bLockAll = TRUE
	ENDIF

	IF NOT bLockAll
	#IF IS_DEBUG_BUILD
	OR GET_COMMANDLINE_PARAM_EXISTS("sc_UnlockAllTunerRadios") 
	#ENDIF 
		BOOL BIs_Fav_usb_station_set_to_off = FALSE 
		INT icurrent_usb_station_stat = GET_MP_INT_CHARACTER_STAT(MP_STAT_CURRENT_USB_MUSIC_MIX)
		
		IF icurrent_usb_station_stat = ENUM_TO_INT( USB_MUSIC_MIX_NONE)
		OR icurrent_usb_station_stat = ENUM_TO_INT( USB_MUSIC_MIX_OFF)
			BIs_Fav_usb_station_set_to_off = TRUE
		ENDIF
		
		
		
		LOCK_RADIO_STATION("RADIO_36_AUDIOPLAYER", FALSE)
		
		IF NOT HAS_LOCAL_PLAYER_COLLECTED_ANY_USB_MIXES()
			//USB_MUSIC_MIX_NONE
			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_SILENCE_MUSIC") 
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - unlock ", "TUNER_AP_SILENCE_MUSIC")
			IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CURRENT_USB_MUSIC_MIX) != ENUM_TO_INT( USB_MUSIC_MIX_OFF)
				SET_MP_INT_CHARACTER_STAT(MP_STAT_CURRENT_USB_MUSIC_MIX , ENUM_TO_INT( USB_MUSIC_MIX_OFF))
				PRINTLN("MAINTAIN_TUNER_RADIO_UNLOCK MP_STAT_CURRENT_USB_MUSIC_MIX setting to USB_MUSIC_MIX_OFF")
				BIs_Fav_usb_station_set_to_off = TRUE
			ENDIF
		ELSE
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_SILENCE_MUSIC")
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - lock ", "TUNER_AP_SILENCE_MUSIC")
			
		ENDIF
		
		IF HAS_LOCAL_PLAYER_COLLECTED_USB_MIX(USB_MUSIC_MIX_0)
		AND NOT BIs_Fav_usb_station_set_to_off
			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_0)) 
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - unlock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_0))
		ELSE
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_0))
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - lock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_0))
		ENDIF
		
		IF HAS_LOCAL_PLAYER_COLLECTED_USB_MIX(USB_MUSIC_MIX_1)
		AND NOT BIs_Fav_usb_station_set_to_off
			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_1)) 
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - unlock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_1))
		ELSE	
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_1)) 	
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - lock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_1))
		ENDIF
		
		IF HAS_LOCAL_PLAYER_COLLECTED_USB_MIX(USB_MUSIC_MIX_2)
		AND NOT BIs_Fav_usb_station_set_to_off
			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_2))
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - unlock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_2))
		ELSE
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_2))
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - lock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_2))
		ENDIF
		
		IF HAS_LOCAL_PLAYER_COLLECTED_USB_MIX(USB_MUSIC_MIX_3)
		AND NOT BIs_Fav_usb_station_set_to_off
			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_3))  
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - unlock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_3))
		ELSE
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_3))
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - lock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_3))
		ENDIF
		
		IF HAS_LOCAL_PLAYER_UNLOCKED_SPECIAL_LOCATIONS_PLAYLIST()
		AND NOT BIs_Fav_usb_station_set_to_off
			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_0_1_2_3))  
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - unlock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_0_1_2_3))
		ELSE
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_0_1_2_3))
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - lock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_0_1_2_3))
		ENDIF
		
		IF HAS_LOCAL_PLAYER_COLLECTED_USB_MIX(USB_MUSIC_MIX_4_MOODY_MAN)
		AND NOT BIs_Fav_usb_station_set_to_off
			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_4_MOODY_MAN))  
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - unlock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_4_MOODY_MAN))
		ELSE
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_4_MOODY_MAN))
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - lock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_4_MOODY_MAN))
		ENDIF
		
		IF HAS_LOCAL_PLAYER_COLLECTED_USB_MIX(USB_MUSIC_MIX_7_FIXER_DRE)
		AND NOT BIs_Fav_usb_station_set_to_off
			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_7_FIXER_DRE))  
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - unlock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_7_FIXER_DRE))
		ELSE
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_7_FIXER_DRE))
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - lock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_7_FIXER_DRE))
		ENDIF
	
		IF HAS_LOCAL_PLAYER_COLLECTED_USB_MIX(USB_MUSIC_MIX_8_FIXER_LOCATION_MIX)
		AND NOT BIs_Fav_usb_station_set_to_off
			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_8_FIXER_LOCATION_MIX))  
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - unlock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_8_FIXER_LOCATION_MIX))
		ELSE
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_8_FIXER_LOCATION_MIX))
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - lock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_8_FIXER_LOCATION_MIX))
		ENDIF
		
		IF HAS_LOCAL_PLAYER_COLLECTED_USB_MIX(USB_MUSIC_MIX_9_FIXER_LOCATION_MIX_2)
		AND NOT BIs_Fav_usb_station_set_to_off
			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_9_FIXER_LOCATION_MIX_2))  
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - unlock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_9_FIXER_LOCATION_MIX_2))
		ELSE
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_9_FIXER_LOCATION_MIX_2))
			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - lock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_9_FIXER_LOCATION_MIX_2))
		ENDIF
		
		
		STRING sTrackName = GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(INT_TO_ENUM(USB_MUSIC_MIX_COLLECTABLES, icurrent_usb_station_stat))

		
		IF NOT IS_BIT_SET(iRadioLockBS, RADIO_BS_FORCE_TRACK_TUNER_INITIALISE)
			IF BIs_Fav_usb_station_set_to_off
				FORCE_MUSIC_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_SILENCE_MUSIC", 0)			
				PRINTLN("MAINTAIN_TUNER_RADIO_UNLOCK forcing radio station off")
			ELSE
				FORCE_MUSIC_TRACK_LIST("RADIO_36_AUDIOPLAYER", sTrackName, GET_RANDOM_INT_IN_RANGE(0, 13) * 60000)
				PRINTLN("MAINTAIN_TUNER_RADIO_UNLOCK forcing radio station to ", sTrackName)
			ENDIF
			SET_BIT(iRadioLockBS, RADIO_BS_FORCE_TRACK_TUNER_INITIALISE)
		ENDIF	
		
//		// FINALE
//		IF HAS_LOCAL_PLAYER_COLLECTED_USB_MIX(USB_MUSIC_MIX_5)
//			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_5))  
//			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - unlock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_5))
//		ELSE
//			LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_5))
//			CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - lock ", GET_USB_COLLECTABLES_RADIO_PLAYLIST_NAME(USB_MUSIC_MIX_5))
//		ENDIF
		
		
	ELSE
		LOCK_RADIO_STATION("RADIO_36_AUDIOPLAYER", TRUE)
		LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_MIX1") 
		LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_MIX2") 
		LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_MIX3") 
		LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_MIX3_PARTA")
		LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_MIX3_PARTB")  
		LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_MIX3_PARTC")
		LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_MIX3_PARTD")  
		LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "TUNER_AP_SILENCE_MUSIC")
		LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "FIXER_AP_MIX")
		LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "FIXER_AP_DSCVRBl_MIX_1")
		LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "FIXER_AP_DSCVRBl_MIX_2")
		LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "RADIO_AP_DRE")
		LOCK_RADIO_STATION_TRACK_LIST("RADIO_36_AUDIOPLAYER", "FIXER_AP_LOWRIDERS_MIX")
		CPRINTLN(DEBUG_SHOPS, "MAINTAIN_TUNER_RADIO_UNLOCK - lock all track lists")
	ENDIF
	
	SET_BIT(iRadioLockBS, RADIO_LOCK_BS_CACHE_TUNER)
ENDPROC

PROC MAINTAIN_PIM_FAVOURITE_RADIO_STATIONS()
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		EXIT
	ENDIF
	
	INT iRadioStation = 0
	STRING sRadioStation = ""
	
	IF (GET_CURRENT_GAMEMODE() != GAMEMODE_FM AND GET_CURRENT_GAMEMODE() != GAMEMODE_CREATOR)
	OR IS_DIRECTOR_MODE_RUNNING() 
		// Restore SP Radio Stations
		IF (NOT bInitPIMRadioFavStationsSP)
			bInitPIMRadioFavStationsSP = TRUE
			bInitPIMRadioFavStationsMP = FALSE
			PRINTLN("[FAV_RADIO_STATIONS] MAINTAIN_PIM_FAVOURITE_RADIO_STATIONS - Restoring SP Radio Stations")
			
			SET_RADIO_TO_STATION_NAME("OFF")
			REPEAT ciPI_TYPE_M2_FAVOURITE_RADIO_STATIONS_MAX iRadioStation
				sRadioStation = GET_FAVOURITE_RADIO_STATION_NAME(iRadioStation)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sRadioStation)
					IF IS_SP_RADIO_STATION(iRadioStation)
						SET_RADIO_STATION_AS_FAVOURITE(sRadioStation, TRUE)
						PRINTLN("[FAV_RADIO_STATIONS] MAINTAIN_PIM_FAVOURITE_RADIO_STATIONS - SP - Favouriting station: ", sRadioStation)
					ELSE
						SET_RADIO_STATION_AS_FAVOURITE(sRadioStation, FALSE)
						PRINTLN("[FAV_RADIO_STATIONS] MAINTAIN_PIM_FAVOURITE_RADIO_STATIONS - MP -Unfavouriting station: ", sRadioStation)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ELSE
		// MP Favourite Radio Stations
		IF (NOT bInitPIMRadioFavStationsMP)
			bInitPIMRadioFavStationsMP = TRUE
			bInitPIMRadioFavStationsSP = FALSE
			PRINTLN("[FAV_RADIO_STATIONS] MAINTAIN_PIM_FAVOURITE_RADIO_STATIONS - Setting MP Favourite Radio Stations")
			INT iStatValue = GET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_FAV_RADIO_STATIONS)
			
			SET_RADIO_TO_STATION_NAME("OFF")
			REPEAT ciPI_TYPE_M2_FAVOURITE_RADIO_STATIONS_MAX iRadioStation
				sRadioStation = GET_FAVOURITE_RADIO_STATION_NAME(iRadioStation)
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sRadioStation)
				AND SHOULD_ADD_RADIO_STATION_TO_PIM_FAVOURITES(iRadioStation)
					IF IS_BIT_SET(iStatValue, iRadioStation)
						SET_RADIO_STATION_AS_FAVOURITE(sRadioStation, FALSE)
						PRINTLN("[FAV_RADIO_STATIONS] MAINTAIN_PIM_FAVOURITE_RADIO_STATIONS - MP - Unfavouriting station: ", sRadioStation)
					ELSE
						SET_RADIO_STATION_AS_FAVOURITE(sRadioStation, TRUE)
						PRINTLN("[FAV_RADIO_STATIONS] MAINTAIN_PIM_FAVOURITE_RADIO_STATIONS - MP - Favouriting station: ", sRadioStation)
					ENDIF
				ENDIF
			ENDREPEAT
		ENDIF
	ENDIF
	
ENDPROC
#ENDIF

PROC MAINTAIN_FIXER_RADIO_UNLOCK()
	MP_GAMEMODE gameModeToCheck = GAMEMODE_EMPTY

	IF GET_JOINING_GAMEMODE() != GAMEMODE_EMPTY
	AND GET_JOINING_GAMEMODE() != GET_CURRENT_GAMEMODE()
		gameModeToCheck = GET_JOINING_GAMEMODE()
	ENDIF

	IF IS_GAME_BOOTED_TO_SP()
	AND NOT IS_BIT_SET(iRadioLockBS, RADIO_LOCK_BS_BOOTING_TO_SP)
		SET_BIT(iRadioLockBS, RADIO_LOCK_BS_BOOTING_TO_SP)
		gameModeToCheck = GAMEMODE_SP
		PRINTLN("MAINTAIN_FIXER_RADIO_UNLOCK RADIO_LOCK_BS_BOOTING_TO_SP = TRUE")
	ENDIF	

	IF IS_SKYSWOOP_AT_GROUND()
	AND IS_BIT_SET(iRadioLockBS, RADIO_LOCK_BS_CACHE_AFTER_BOOT)
		IF IS_BIT_SET(iRadioLockBS, RADIO_LOCK_BS_CACHE_FIXER)
			CLEAR_BIT(iRadioLockBS, RADIO_LOCK_BS_CACHE_FIXER)
		ENDIF	
		EXIT
	ENDIF
	
	IF NOT g_bHasCopiedAllTunableValuesToScriptGlobals 
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iRadioLockBS, RADIO_LOCK_BS_CACHE_FIXER)
		EXIT
	ENDIF	
	
	IF gameModeToCheck = GAMEMODE_EMPTY
		EXIT
	ENDIF

	SWITCH gameModeToCheck
		CASE GAMEMODE_FM
		CASE GAMEMODE_CREATOR
		CASE GAMEMODE_MPTESTBED
			IF g_sMPTunables.bFIXER_ENABLE_RADIO_03_HIPHOP_LAUNCH_EVENT 	
				UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_03_HIPHOP_NEW", "RADIO_03_HIPHOP_NEW_DD_DJSOLO_LAUNCH")
				UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_03_HIPHOP_NEW", "RADIO_03_HIPHOP_NEW_DD_MUSIC_LAUNCH")
				
				LOCK_RADIO_STATION_TRACK_LIST("RADIO_03_HIPHOP_NEW", "RADIO_03_HIPHOP_NEW_DD_DJSOLO_POST_LAUNCH")
				LOCK_RADIO_STATION_TRACK_LIST("RADIO_03_HIPHOP_NEW", "RADIO_03_HIPHOP_NEW_DD_MUSIC_POST_LAUNCH_UPDATE")
			ELSE
				LOCK_RADIO_STATION_TRACK_LIST("RADIO_03_HIPHOP_NEW", "RADIO_03_HIPHOP_NEW_DD_DJSOLO_LAUNCH")
				LOCK_RADIO_STATION_TRACK_LIST("RADIO_03_HIPHOP_NEW", "RADIO_03_HIPHOP_NEW_DD_MUSIC_LAUNCH")
				
				UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_03_HIPHOP_NEW", "RADIO_03_HIPHOP_NEW_DD_DJSOLO_POST_LAUNCH")
				UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_03_HIPHOP_NEW", "RADIO_03_HIPHOP_NEW_DD_MUSIC_POST_LAUNCH_UPDATE")
			ENDIF
			
			IF g_sMPTunables.bFIXER_ENABLE_RADIO_09_HIPHOP_LAUNCH_EVENT 			
				LOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DJSOLO")
				LOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_IDENTS")
				LOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_MUSIC")
				LOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_MUSIC_NEW")
				LOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DD_MUSIC_POST_LAUNCH")
				LOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_CORE_MUSIC")
				
				UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DD_DJSOLO_POST_LAUNCH")
				UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DD_MUSIC_LAUNCH")
				UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DD_IDENTS_LAUNCH")
				UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DD_DJSOLO_LAUNCH")
			ELSE
				UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DJSOLO")
				UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_IDENTS")
				UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_MUSIC")
				UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_MUSIC_NEW")
				UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DD_DJSOLO_POST_LAUNCH")
				UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DD_MUSIC_POST_LAUNCH")
				UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_CORE_MUSIC")
				
				LOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DD_MUSIC_LAUNCH")		
				LOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DD_IDENTS_LAUNCH")
				LOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DD_DJSOLO_LAUNCH")
			ENDIF
			
			IF g_sMPTunables.bFIXER_ENABLE_RADIO_37_MOTOMAMI
				LOCK_RADIO_STATION("RADIO_37_MOTOMAMI", FALSE)
			ELSE
				LOCK_RADIO_STATION("RADIO_37_MOTOMAMI", TRUE)
			ENDIF
		BREAK
		DEFAULT
			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DJSOLO")
			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_IDENTS")
			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_MUSIC")
			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_MUSIC_NEW")
			UNLOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_CORE_MUSIC")
			
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_03_HIPHOP_NEW", "RADIO_03_HIPHOP_NEW_DD_DJSOLO_LAUNCH")
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_03_HIPHOP_NEW", "RADIO_03_HIPHOP_NEW_DD_DJSOLO_POST_LAUNCH")
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_03_HIPHOP_NEW", "RADIO_03_HIPHOP_NEW_DD_MUSIC_POST_LAUNCH_UPDATE")
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_03_HIPHOP_NEW", "RADIO_03_HIPHOP_NEW_DD_MUSIC_LAUNCH")
			
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DD_MUSIC_LAUNCH")
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DD_MUSIC_POST_LAUNCH")
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DD_IDENTS_LAUNCH")
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DD_DJSOLO_LAUNCH")
			LOCK_RADIO_STATION_TRACK_LIST("RADIO_09_HIPHOP_OLD", "RADIO_09_HIPHOP_OLD_DD_DJSOLO_POST_LAUNCH")
			LOCK_RADIO_STATION("RADIO_37_MOTOMAMI", TRUE)
		BREAK
	ENDSWITCH

	PRINTLN("MAINTAIN_FIXER_RADIO_UNLOCK gameModeToCheck: ", GET_GAMEMODE_STRING(gameModeToCheck), " is host: ", GET_STRING_FROM_BOOL(NETWORK_IS_HOST()))
	SET_BIT(iRadioLockBS, RADIO_LOCK_BS_CACHE_AFTER_BOOT)
	SET_BIT(iRadioLockBS, RADIO_LOCK_BS_CACHE_FIXER)
ENDPROC

PROC PROCESS_RECEIVED_TRANSACTION_REQUEST(INT iEventID)
	PRINTLN("PROCESS_RECEIVED_TRANSACTION_REQUEST - called...")
	SCRIPT_EVENT_DATA_REQUEST_CASH_TRANSACTION Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		// Is this transaction slot free?
		IF Event.transaction.iSlot > -1
		AND Event.transaction.iSlot < (SHOPPING_TRANSACTION_MAX_NUMBER+1)
			IF VALIDATE_CASH_TRANSACTION_EVENT_DATA(Event.transaction.eDetails, Event.transaction.iSlot)				
				//Save the details of this transaction
				PRINTLN("[CASH] PROCESS_RECEIVED_TRANSACTION_REQUEST - Request saved in slot ", Event.transaction.iSlot, " transaction ID: ", Event.transaction.eDetails.iTransactionId)
				m_cashTransactionData[Event.transaction.iSlot].eEssentialData 	= Event.transaction.eDetails
				m_cashTransactionData[Event.transaction.iSlot].cashInfo			= Event.transaction.cashInfo
				//Grab the last piece of extra data as it is too big for the event
				m_cashTransactionData[Event.transaction.iSlot].eExtraData		= g_cashTransactionData[Event.transaction.iSlot].eExtraData
				
				IF m_cashTransactionData[Event.transaction.iSlot].eEssentialData.eTransactionStatus != CASH_TRANSACTION_STATUS_NULL
					PRINTLN("[CASH] PROCESS_RECEIVED_TRANSACTION_REQUEST - Transaction assigned to slot in use: ", Event.transaction.iSlot)
					m_cashTransactionData[Event.transaction.iSlot].eEssentialData.eTransactionStatus = CASH_TRANSACTION_STATUS_PENDING
				ENDIF
				
				EXIT
			ELSE
				DELETE_CASH_TRANSACTION(Event.transaction.iSlot)
				PRINTLN("[CASH] PROCESS_RECEIVED_TRANSACTION_REQUEST - Given invalid transaction data!")
				SCRIPT_ASSERT("PROCESS_RECEIVED_TRANSACTION_REQUEST - Given invalid transaction data!")
			ENDIF
		ELSE
			PRINTLN("[CASH] PROCESS_RECEIVED_TRANSACTION_REQUEST - Given invalid transaction slot!")
			SCRIPT_ASSERT("PROCESS_RECEIVED_TRANSACTION_REQUEST - Given invalid transaction slot!")
		ENDIF
	ELSE
		PRINTLN("[CASH] PROCESS_RECEIVED_TRANSACTION_REQUEST - could not retreive any data.")
		SCRIPT_ASSERT("PROCESS_RECEIVED_TRANSACTION_REQUEST - could not retreive any data.")
	ENDIF
ENDPROC

#IF FEATURE_COPS_N_CROOKS
PROC MAINTAIN_SUI_PURCHASE()
	IF sSUIPurchase.eItem != SUI_UNLOCK_INVALID
		IF PURCHASE_SCRIPT_UNLOCKABLE_ITEM(sSUIPurchase.eCurrency, sSUIPurchase.eItem, sSUIPurchase.eTransactionState)			
			//Set the global for other scripts to pick up on
			g_TransitionSessionNonResetVars.SUIPurchase.eTransactionState = sSUIPurchase.eTransactionState
			
			PRINTLN("MAINTAIN_SUI_PURCHASE - Finished. Result: ", sSUIPurchase.eTransactionState)
			sSUIPurchase.eTransactionState = TRANSACTION_STATE_DEFAULT
			sSUIPurchase.eItem = SUI_UNLOCK_INVALID
		ENDIF
	ENDIF
ENDPROC

PROC PROCESS_RECEIVED_SUI_REQUEST_PURCHASE(INT iEventID)
	PRINTLN("PROCESS_RECEIVED_TRANSACTION_REQUEST - called...")
	SCRIPT_EVENT_DATA_REQUEST_SUI_PURCHASE Event
	
	IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iEventID, Event, SIZE_OF(Event))
		IF IS_SCRIPT_UNLOCKABLE_ITEM_ID_VALID(Event.eItem)
			IF sSUIPurchase.eItem = SUI_UNLOCK_INVALID
				sSUIPurchase.eItem 		= Event.eItem
				sSUIPurchase.eCurrency 	= Event.eCurrency
				sSUIPurchase.eTransactionState = TRANSACTION_STATE_DEFAULT
				PRINTLN("PROCESS_RECEIVED_SUI_REQUEST_PURCHASE - Starting transaction for item: ", Event.eItem)
			ELSE
				g_TransitionSessionNonResetVars.SUIPurchase.eTransactionState = TRANSACTION_STATE_FAILED
				PRINTLN("PROCESS_RECEIVED_SUI_REQUEST_PURCHASE - Attempting to overwrite running transaction!")
				SCRIPT_ASSERT("PROCESS_RECEIVED_SUI_REQUEST_PURCHASE - Attempting to overwrite running transaction!")
			ENDIF
		ELSE
			g_TransitionSessionNonResetVars.SUIPurchase.eTransactionState = TRANSACTION_STATE_FAILED
			PRINTLN("PROCESS_RECEIVED_SUI_REQUEST_PURCHASE - Given invalid item!")
			SCRIPT_ASSERT("PROCESS_RECEIVED_SUI_REQUEST_PURCHASE - Given invalid item!")
		ENDIF
	ELSE
		g_TransitionSessionNonResetVars.SUIPurchase.eTransactionState = TRANSACTION_STATE_FAILED
		PRINTLN("PROCESS_RECEIVED_SUI_REQUEST_PURCHASE - could not retreive any data.")
		SCRIPT_ASSERT("PROCESS_RECEIVED_SUI_REQUEST_PURCHASE - could not retreive any data.")
	ENDIF
ENDPROC
#ENDIF

PROC PROCESS_NET_TRANSACTION_EVENTS()
	INT iCount
	EVENT_NAMES ThisScriptEvent
	STRUCT_EVENT_COMMON_DETAILS Details
	
	//process the events
	REPEAT GET_NUMBER_OF_EVENTS(SCRIPT_EVENT_QUEUE_NETWORK) iCount
		ThisScriptEvent = GET_EVENT_AT_INDEX(SCRIPT_EVENT_QUEUE_NETWORK, iCount)
		
		IF NOT ShouldBGScriptInterceptCodeEvent(ThisScriptEvent)
			IF ThisScriptEvent = EVENT_NETWORK_SCRIPT_EVENT					
	
				IF GET_EVENT_DATA(SCRIPT_EVENT_QUEUE_NETWORK, iCount, Details, SIZE_OF(Details))
					IF Details.FromPlayerIndex = PLAYER_ID()
						IF Details.Type = SCRIPT_EVENT_REQUEST_CASH_TRANSACTION
							IF NOT g_sMPTunables.bBLOCK_NS_TRANS
							OR NOT g_sMPTunables.bSC_RUN_TRANS
								EXIT
							ENDIF
							
							PRINTLN("PROCESS_NET_TRANSACTION_EVENTS - Received new transaction request event")
							PROCESS_RECEIVED_TRANSACTION_REQUEST(iCount)
							
						#IF FEATURE_COPS_N_CROOKS
						ELIF Details.Type = SCRIPT_EVENT_REQUEST_SUI_PURCHASE
							PRINTLN("PROCESS_NET_TRANSACTION_EVENTS - Received new transaction request event")
							PROCESS_RECEIVED_SUI_REQUEST_PURCHASE(iCount)
						#ENDIF
						
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDREPEAT
ENDPROC

#IF FEATURE_CASINO
GENERIC_TRANSACTION_STATE ePodiumTransactionResult
PROC MAINTAIN_CASINO_LUCKY_WHEEL_PODIUM_USAGE_TRANSACTION()
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF g_bTriggerPodiumVehicleUsageTransaction = TRUE
			IF USE_SERVER_TRANSACTIONS()
				INT iInventoryItem[1], iInventoryValue[1]
				iInventoryItem[0] =  HASH("VW_LUCKY_WHEEL_VEH_PRIZE") 
				iInventoryValue[0] = g_iCurrentPosixPodiumReward + LUCKY_WHEEL_PODIUM_VEHICLE_POSIX
				IF PROCESS_UPDATE_STORAGE_DATA_TRANSACTION(ePodiumTransactionResult, iInventoryItem, iInventoryValue)
					IF ePodiumTransactionResult = TRANSACTION_STATE_SUCCESS
						SET_MP_INT_PLAYER_STAT(MPPLY_LW_PODIUM_VEH_MODEL,  g_sMPTunables.iCASINO_PRIZE_VEHICLE_MODEL_HASH) 
						SET_LUCKY_WHEEL_PODIUM_VEHICLE_WON_TIME()
						g_bTriggerPodiumVehicleUsageTransaction = FALSE
						PRINTLN("[LUCKY_WHEEL] MAINTAIN_CASINO_LUCKY_WHEEL_PODIUM_USAGE_TRANSACTION - PROCESS_UPDATE_STORAGE_DATA_TRANSACTION true!")
					ELSE
						PRINTLN("[LUCKY_WHEEL] MAINTAIN_CASINO_LUCKY_WHEEL_PODIUM_USAGE_TRANSACTION - waiting for transaction results: ", ePodiumTransactionResult)
					ENDIF
					
					ePodiumTransactionResult = TRANSACTION_STATE_DEFAULT
				ELSE
					PRINTLN("[LUCKY_WHEEL] MAINTAIN_CASINO_LUCKY_WHEEL_PODIUM_USAGE_TRANSACTION - PROCESS_UPDATE_STORAGE_DATA_TRANSACTION false!")
				ENDIF
			ENDIF				
		ENDIF	
	ENDIF	
ENDPROC
#ENDIF

#IF FEATURE_TUNER
PROC CLEANUP_TUNER_AUTO_SHOP_CLIENT_SPAWN_TIMER()
	RESET_NET_TIMER(sTunerClientSpawnTimer)
	iAutoShopRandomTime = 0
	CLEAR_BIT(iLocalBS, LOCAL_BIT_SET_AUTO_SHOP_RAND_TIME)
	CLEAR_BIT(iLocalBS, LOCAL_BIT_FOUND_NEW_TUNER_CLIENT_VEHICLES)
	CLEAR_BIT(iLocalBS, LOCAL_BIT_FOUND_NEW_TUNER_CLIENT_VEHICLES_FOR_SLOT_0)
	CLEAR_BIT(iLocalBS, LOCAL_BIT_FOUND_NEW_TUNER_CLIENT_VEHICLES_FOR_SLOT_1)
	CLEAR_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_DONE_FOR_CLIENT_VEHICLES_SLOT_0)
	CLEAR_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_DONE_FOR_CLIENT_VEHICLES_SLOT_1)
ENDPROC

#IF IS_DEBUG_BUILD
PROC UPDATE_TUNER_AUTO_SHOP_DEBUG_WIDGET()
	IF bResetAutoShopVehiclePickTimer
		CLEANUP_TUNER_AUTO_SHOP_CLIENT_SPAWN_TIMER()
		SET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_CLIENT_VEHICLE_POSSIX, 0)
		bResetAutoShopVehiclePickTimer = FALSE
	ENDIF
	
	IF bResetAutoShopVehicleModels
		SET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CLIENT_VEHICLE_MODIFICATION(0), 0)
		SET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CLIENT_VEHICLE(0), 0)
		CLEANUP_TUNER_CLIENT_VEHICLE_MODIFICATION_TASKS(0)
		CLEAR_AUTO_SHOP_CLIENT_VEHICLE_SETUP(0)
		SET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CLIENT_VEHICLE_MODIFICATION(1), 0)
		SET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CLIENT_VEHICLE(1), 0)
		CLEANUP_TUNER_CLIENT_VEHICLE_MODIFICATION_TASKS(1)
		CLEAR_AUTO_SHOP_CLIENT_VEHICLE_SETUP(1)
		SET_AUTO_SHOP_CLIENT_VEHICLE_DELIVERY_SLOT(-1)
		SET_PLAYER_SELECTED_TO_DELIVER_AUTO_SHOP_CLIENT_VEHICLE(FALSE)
		CLEANUP_TUNER_AUTO_SHOP_CLIENT_SPAWN_TIMER()
		SET_PLAYER_SELECTED_TO_DELIVER_AUTO_SHOP_CLIENT_VEHICLE(FALSE)
		SET_AUTO_SHOP_CLIENT_VEHICLE_MODEL(DUMMY_MODEL_FOR_SCRIPT, 0)
		SET_AUTO_SHOP_CLIENT_VEHICLE_MODEL(DUMMY_MODEL_FOR_SCRIPT, 1)
		bResetAutoShopVehicleModels = FALSE
	ENDIF
ENDPROC
#ENDIF

FUNC BOOL SHOULD_GIVE_TUNER_CLIENT_VEHICLE_THIS_TIME()
	
	IF g_sMPTunables.iTUNER_AUTO_SHOP_CLIENT_VEH_SPAWN_WEIGHT >= 100
		RETURN TRUE
	ENDIF	
	
	IF g_sMPTunables.iTUNER_AUTO_SHOP_CLIENT_VEH_SPAWN_WEIGHT <= 0
		RETURN FALSE
	ENDIF	
	
	INT iSumOfWeight = 101
	INT iRandom = GET_RANDOM_INT_IN_RANGE(0, (iSumOfWeight))

	IF iRandom <= g_sMPTunables.iTUNER_AUTO_SHOP_CLIENT_VEH_SPAWN_WEIGHT
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC 

FUNC BOOL CAN_START_TUNER_AUTO_SHOP_TIMER()
	IF PLAYER_ID() = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()	
		RETURN FALSE
	ENDIF
	
	IF NETWORK_IS_ACTIVITY_SESSION()
		RETURN FALSE
	ENDIF	
	
	IF NOT IS_FREEMODE()
		RETURN FALSE
	ENDIF	
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
		RETURN FALSE
	ENDIF	
	
	IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_DANCING(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT IS_SKYSWOOP_AT_GROUND()
		RETURN FALSE
	ENDIF
	
	IF IS_ANY_TRANSITION_SESSION_INVITE_BEING_ACTIONED()
		RETURN FALSE
	ENDIF
	
	IF AM_I_TRANSITION_SESSIONS_STARTING_ON_CALL()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_ANY_SHOP()
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_PLAYER_OWN_A_AUTO_SHOP(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT HAS_PLAYER_COMPLETED_AUTO_SHOP_SETUP_MISSION(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_AUTO_SHOP(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT IS_BIT_SET(GET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_GEN_BS), ciTUNERGENERALBITSET_DONE_CUSTOMER_CAR_INTRO_CALL)
		RETURN FALSE
	ENDIF
	
	IF HAS_TUNER_CLIENT_VEHICLE_SPAWNED_IN_PAST_DAY()
		RETURN FALSE
	ENDIF
	
	IF GET_AUTO_SHOP_CLIENT_VEHICLE_MODEL(0) != DUMMY_MODEL_FOR_SCRIPT
	AND GET_AUTO_SHOP_CLIENT_VEHICLE_MODEL(1) != DUMMY_MODEL_FOR_SCRIPT
	AND NOT IS_BIT_SET(iLocalBS, LOCAL_BIT_FOUND_NEW_TUNER_CLIENT_VEHICLES)
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC BOOL PROCESS_TRANSACTION_FOR_ADDING_TUNER_CLIENT_VEHICLE(CONTRABAND_TRANSACTION_STATE &eResult, INT iVehicleID, INT iSlot)

	// Cleanup previous attempts
	IF eResult != CONTRABAND_TRANSACTION_STATE_DEFAULT
	AND eResult != CONTRABAND_TRANSACTION_STATE_PENDING
		eResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
	ENDIF
	
	SWITCH eResult
		// Start the transaction
		CASE CONTRABAND_TRANSACTION_STATE_DEFAULT
			
			IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() != -1
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADDING_TUNER_CLIENT_VEHICLE - Transaction waiting")
				RETURN FALSE
			ELSE
			
				INT iVehicleKey
				iVehicleKey	= GET_TUNER_CLIENT_VEHICLE_INVENTORY_KEY_FOR_CATALOGUE(iSlot)

				IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CONTRABAND_QNTY, iVehicleKey, NET_SHOP_ACTION_UPDATE_BUSINESS_GOODS, iVehicleID, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
					eResult = CONTRABAND_TRANSACTION_STATE_PENDING
				ELSE
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADDING_TUNER_CLIENT_VEHICLE - Failed to add vehicle slot: ", iSlot)
				ENDIF
				
				IF eResult = CONTRABAND_TRANSACTION_STATE_PENDING
				AND NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADDING_TUNER_CLIENT_VEHICLE - Starting basket checkout")
				ELSE
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADDING_TUNER_CLIENT_VEHICLE - Failed to start checkout")
				ENDIF
			ENDIF
		BREAK
		
		CASE CONTRABAND_TRANSACTION_STATE_PENDING
			IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() = -1
				eResult = CONTRABAND_TRANSACTION_STATE_FAILED
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADDING_TUNER_CLIENT_VEHICLE - GET_BASKET_TRANSACTION_SCRIPT_INDEX() = -1 FAILED ")
			ELIF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADDING_TUNER_CLIENT_VEHICLE - Transaction finished - SUCCESS")
					eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ELSE
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADDING_TUNER_CLIENT_VEHICLE - Transaction finished - FAILED")
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN (eResult != CONTRABAND_TRANSACTION_STATE_PENDING AND eResult != CONTRABAND_TRANSACTION_STATE_DEFAULT)
ENDFUNC

/// PURPOSE:
///    Runs a transaction to add a tuner client vehicle
///    iVehicleOneID: -1 ignore this vehicle
///    iVehicleTwoID: -1 ignore this vehicle
FUNC BOOL ADD_TUNER_CLIENT_VEHICLE(INT iVehicleID, INT iSlot, CONTRABAND_TRANSACTION_STATE &eResult)
	IF iVehicleID < 0
		PRINTLN("ADD_TUNER_CLIENT_VEHICLE - Attempting to add iVehicleID: ", iVehicleID, " iSlot: ", iSlot)
		eResult = CONTRABAND_TRANSACTION_STATE_FAILED
		RETURN TRUE
	ENDIF

	IF USE_SERVER_TRANSACTIONS()
		IF PROCESS_TRANSACTION_FOR_ADDING_TUNER_CLIENT_VEHICLE(eResult, iVehicleID, iSlot)			
			IF eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS				
				PRINTLN("ADD_TUNER_CLIENT_VEHICLE - PC: Attempting to add iVehicleID: ", iVehicleID, " iSlot: ", iSlot)
				SET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CLIENT_VEHICLE(iSlot), iVehicleID)
			ENDIF
		ENDIF
		
		RETURN (eResult != CONTRABAND_TRANSACTION_STATE_PENDING AND eResult != CONTRABAND_TRANSACTION_STATE_DEFAULT)
	ELSE
		IF iVehicleID != -1
			PRINTLN("ADD_TUNER_CLIENT_VEHICLE - Console: Attempting to add iVehicleID: ", iVehicleID, " iSlot: ", iSlot)
			SET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CLIENT_VEHICLE(iSlot), iVehicleID)
		ENDIF	

		eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS

		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC INT GET_CLIENT_VEHICLE_COLOUR_FROM_RANDOM_INT(INT iRandomInt)
	SWITCH iRandomInt	
		CASE 0		RETURN 15	 //- BLACK
		CASE 1		RETURN 16	 //- BLACK POLY
		CASE 2		RETURN 18	 //- SILVER
		CASE 3		RETURN 19	 //- GUNMETAL
		CASE 4		RETURN 14	 //- GREY
		CASE 5		RETURN 111	 //- WHITE
		CASE 6		RETURN 27	 //- RED
		CASE 7		RETURN 40	 //- MATTE DARK RED
		CASE 8		RETURN 41	 //- MATTE ORANGE
		CASE 9		RETURN 42	 //- MATTE YELLOW
		CASE 10		RETURN 49	 //- DARK GREEN
		CASE 11		RETURN 50	 //- GREEN
		CASE 12		RETURN 54	 //- AQUA BLUE
		CASE 13		RETURN 80	 //- BLUE POLY
		CASE 14		RETURN 62	 //- DARK BLUE
		CASE 15		RETURN 64	 //- BLUE
		CASE 16		RETURN 88	 //- YELLOW
		CASE 17		RETURN 89	 //- OFF YELLOW
		CASE 18		RETURN 105	 //- DESSERT SAND
		CASE 19		RETURN 37	 //- GOLD
		CASE 20		RETURN 90	 //- BRONZE
		CASE 21		RETURN 135	 //- HOT PINK
		CASE 22		RETURN 137	 //- DARK SALMON
		CASE 23		RETURN 136	 //- SALMON PINK
		CASE 24		RETURN 145	 //- PURPLE
		CASE 25		RETURN 71	 //- DARK PURPLE
		CASE 26		RETURN 92	 //- LIME GREEN
	ENDSWITCH
	
	RETURN 15
ENDFUNC

PROC MAINTAIN_TUNER_AUTO_SHOP_CLIENT_VEHICLE_SPAWN()
	#IF IS_DEBUG_BUILD
	UPDATE_TUNER_AUTO_SHOP_DEBUG_WIDGET()
	#ENDIF

	IF NOT CAN_START_TUNER_AUTO_SHOP_TIMER()
	#IF IS_DEBUG_BUILD
	AND NOT bByPassAutoShopSpawnWeight
	#ENDIF
		CLEANUP_TUNER_AUTO_SHOP_CLIENT_SPAWN_TIMER()
		EXIT
	ENDIF

	IF NOT IS_BIT_SET(iLocalBS, LOCAL_BIT_SET_AUTO_SHOP_RAND_TIME)
		// Random time in seconds
		iAutoShopRandomTime = GET_RANDOM_INT_IN_RANGE(0, 600)
		
		IF SHOULD_GIVE_TUNER_CLIENT_VEHICLE_THIS_TIME()
		#IF IS_DEBUG_BUILD
		OR bByPassAutoShopSpawnWeight
		#ENDIF
			SET_BIT(iLocalBS, LOCAL_BIT_SHOULD_GIVE_VEHICLE_THIS_TIME)
			PRINTLN("[AUTO_SHOP_CLIENT_VEH] MAINTAIN_TUNER_AUTO_SHOP_CLIENT_VEHICLE_SPAWN LOCAL_BIT_SHOULD_GIVE_VEHICLE_THIS_TIME TRUE")
		ELSE
			CLEAR_BIT(iLocalBS, LOCAL_BIT_SHOULD_GIVE_VEHICLE_THIS_TIME)
			PRINTLN("[AUTO_SHOP_CLIENT_VEH] MAINTAIN_TUNER_AUTO_SHOP_CLIENT_VEHICLE_SPAWN LOCAL_BIT_SHOULD_GIVE_VEHICLE_THIS_TIME FALSE")
		ENDIF
		
		SET_BIT(iLocalBS, LOCAL_BIT_SET_AUTO_SHOP_RAND_TIME)
		PRINTLN("[AUTO_SHOP_CLIENT_VEH] MAINTAIN_TUNER_AUTO_SHOP_CLIENT_VEHICLE_SPAWN LOCAL_BIT_SET_AUTO_SHOP_RAND_TIME TRUE iAutoShopRandomTime ", iAutoShopRandomTime)
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(sTunerClientSpawnTimer, (iAutoShopRandomTime * 1000))
	#IF IS_DEBUG_BUILD
	OR bByPassAutoShopRandomTimer
	#ENDIF
		BOOL bGivenVehicle = FALSE
		BOOL bHasCarLift = HAS_PLAYER_PURCHASED_AUTO_SHOP_CAR_LIFT(PLAYER_ID())
		INT iRandomVeh, iRandomColour
		IF IS_BIT_SET(iLocalBS, LOCAL_BIT_SHOULD_GIVE_VEHICLE_THIS_TIME)
			IF NOT IS_BIT_SET(iLocalBS, LOCAL_BIT_FOUND_NEW_TUNER_CLIENT_VEHICLES)
				IF GET_AUTO_SHOP_CLIENT_VEHICLE_MODEL(0) = DUMMY_MODEL_FOR_SCRIPT
				AND GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CLIENT_VEHICLE(0)) = 0
					bGivenVehicle = TRUE
					
					RESET_VEHICLE_SETUP_STRUCT_MP(g_sClientVehicleSetupStruct[0].vehicleSetupMP)
					
					iRandomVeh = GET_RANDOM_INT_IN_RANGE(1, MAX_NUM_TUNER_CLIENT_VEHICLE + 1)
					
					#IF IS_DEBUG_BUILD
					IF iTunerClientVehicleID != 0
						iRandomVeh = iTunerClientVehicleID
					ENDIF	
					#ENDIF
					
					iRandomColour = GET_RANDOM_INT_IN_RANGE(0, 27)
					iRandomColour = GET_CLIENT_VEHICLE_COLOUR_FROM_RANDOM_INT(iRandomColour)
					
					g_sClientVehicleSetupStruct[0].vehicleSetupMP.VehicleSetup.iColour1 = iRandomColour
					g_sClientVehicleSetupStruct[0].vehicleSetupMP.VehicleSetup.iColour2 = iRandomColour
					
					g_sClientVehicleSetupStruct[0].vehicleSetupMP.VehicleSetup.iColourExtra1 = iRandomColour
					g_sClientVehicleSetupStruct[0].vehicleSetupMP.VehicleSetup.iColourExtra2 = iRandomColour
					
					SET_AUTO_SHOP_CLIENT_VEHICLE_MODEL(GET_TUNER_CLIENT_VEHICLE_MODEL(iRandomVeh), 0)
					MP_SAVE_TUNER_CLIENT_VEHICLE_SLOT_STATS_FROM_SAVEGAME(0, g_sClientVehicleSetupStruct[0])
					SET_BIT(iLocalBS, LOCAL_BIT_FOUND_NEW_TUNER_CLIENT_VEHICLES_FOR_SLOT_0)
					PRINTLN("[AUTO_SHOP_CLIENT_VEH] MAINTAIN_TUNER_AUTO_SHOP_CLIENT_VEHICLE_SPAWN LOCAL_BIT_FOUND_NEW_TUNER_CLIENT_VEHICLES_FOR_SLOT_0 TRUE")
				ENDIF

				IF bHasCarLift
				AND NOT bGivenVehicle
					IF GET_AUTO_SHOP_CLIENT_VEHICLE_MODEL(1) = DUMMY_MODEL_FOR_SCRIPT
					AND GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CLIENT_VEHICLE(1)) = 0
						RESET_VEHICLE_SETUP_STRUCT_MP(g_sClientVehicleSetupStruct[1].vehicleSetupMP)
						
						iRandomVeh = GET_RANDOM_INT_IN_RANGE(1, MAX_NUM_TUNER_CLIENT_VEHICLE + 1)
						
						#IF IS_DEBUG_BUILD
						IF iTunerClientVehicleID != 0
							iRandomVeh = iTunerClientVehicleID
						ENDIF	
						#ENDIF
						
						iRandomColour = GET_RANDOM_INT_IN_RANGE(0, 27)
						iRandomColour = GET_CLIENT_VEHICLE_COLOUR_FROM_RANDOM_INT(iRandomColour)
						
						g_sClientVehicleSetupStruct[1].vehicleSetupMP.VehicleSetup.iColour1 = iRandomColour
						g_sClientVehicleSetupStruct[1].vehicleSetupMP.VehicleSetup.iColour2 = iRandomColour
						
						g_sClientVehicleSetupStruct[1].vehicleSetupMP.VehicleSetup.iColourExtra1 = iRandomColour
						g_sClientVehicleSetupStruct[1].vehicleSetupMP.VehicleSetup.iColourExtra2 = iRandomColour
						
						SET_AUTO_SHOP_CLIENT_VEHICLE_MODEL(GET_TUNER_CLIENT_VEHICLE_MODEL(iRandomVeh), 1)
						MP_SAVE_TUNER_CLIENT_VEHICLE_SLOT_STATS_FROM_SAVEGAME(1, g_sClientVehicleSetupStruct[1])
						SET_BIT(iLocalBS, LOCAL_BIT_FOUND_NEW_TUNER_CLIENT_VEHICLES_FOR_SLOT_1)
						PRINTLN("[AUTO_SHOP_CLIENT_VEH] MAINTAIN_TUNER_AUTO_SHOP_CLIENT_VEHICLE_SPAWN LOCAL_BIT_FOUND_NEW_TUNER_CLIENT_VEHICLES_FOR_SLOT_1 TRUE")
					ENDIF
				ENDIF
				
				SET_BIT(iLocalBS, LOCAL_BIT_FOUND_NEW_TUNER_CLIENT_VEHICLES)
				EXIT	
			ELSE
				IF NOT IS_BIT_SET(iLocalBS, LOCAL_BIT_TRANSACTION_DONE_FOR_CLIENT_VEHICLES_SLOT_0)
					IF IS_BIT_SET(iLocalBS, LOCAL_BIT_FOUND_NEW_TUNER_CLIENT_VEHICLES_FOR_SLOT_0)
						IF NOT ADD_TUNER_CLIENT_VEHICLE(GET_TUNER_CLIENT_VEHICLE_STAT_INDEX(GET_AUTO_SHOP_CLIENT_VEHICLE_MODEL(0)), 0, clientVehicleTransResult)
							EXIT	
						ENDIF
						
						IF clientVehicleTransResult != CONTRABAND_TRANSACTION_STATE_SUCCESS
							SET_AUTO_SHOP_CLIENT_VEHICLE_MODEL(DUMMY_MODEL_FOR_SCRIPT, 0)
						ELSE
							// Only clear transaction result if we are going to do another transaction for second slot
							IF IS_BIT_SET(iLocalBS, LOCAL_BIT_FOUND_NEW_TUNER_CLIENT_VEHICLES_FOR_SLOT_1)
								clientVehicleTransResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
								PRINTLN("[AUTO_SHOP_CLIENT_VEH] MAINTAIN_TUNER_AUTO_SHOP_CLIENT_VEHICLE_SPAWN first transaction SUCCESS clear clientVehicleTransResult")
							ELSE
								PRINTLN("[AUTO_SHOP_CLIENT_VEH] MAINTAIN_TUNER_AUTO_SHOP_CLIENT_VEHICLE_SPAWN first transaction SUCCESS move to adding the second vehicle")
							ENDIF	
						ENDIF
						
						SET_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_DONE_FOR_CLIENT_VEHICLES_SLOT_0)
						PRINTLN("[AUTO_SHOP_CLIENT_VEH] MAINTAIN_TUNER_AUTO_SHOP_CLIENT_VEHICLE_SPAWN LOCAL_BIT_TRANSACTION_DONE_FOR_CLIENT_VEHICLES_SLOT_0 TRUE")
					ELSE
						SET_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_DONE_FOR_CLIENT_VEHICLES_SLOT_0)
						PRINTLN("[AUTO_SHOP_CLIENT_VEH] LOCAL_BIT_TRANSACTION_DONE_FOR_CLIENT_VEHICLES_SLOT_0 TRUE")
					ENDIF
					
					EXIT
				ENDIF	

				IF NOT IS_BIT_SET(iLocalBS, LOCAL_BIT_TRANSACTION_DONE_FOR_CLIENT_VEHICLES_SLOT_1)
					IF IS_BIT_SET(iLocalBS, LOCAL_BIT_FOUND_NEW_TUNER_CLIENT_VEHICLES_FOR_SLOT_1)
						IF NOT ADD_TUNER_CLIENT_VEHICLE(GET_TUNER_CLIENT_VEHICLE_STAT_INDEX(GET_AUTO_SHOP_CLIENT_VEHICLE_MODEL(1)), 1, clientVehicleTransResult)
							EXIT	
						ENDIF
						
						IF clientVehicleTransResult != CONTRABAND_TRANSACTION_STATE_SUCCESS
							SET_AUTO_SHOP_CLIENT_VEHICLE_MODEL(DUMMY_MODEL_FOR_SCRIPT, 1)
						ENDIF
						
						SET_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_DONE_FOR_CLIENT_VEHICLES_SLOT_1)
						PRINTLN("[AUTO_SHOP_CLIENT_VEH] MAINTAIN_TUNER_AUTO_SHOP_CLIENT_VEHICLE_SPAWN LOCAL_BIT_TRANSACTION_DONE_FOR_CLIENT_VEHICLES_SLOT_1 TRUE")
					ELSE
						SET_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_DONE_FOR_CLIENT_VEHICLES_SLOT_1)
						PRINTLN("[AUTO_SHOP_CLIENT_VEH] MAINTAIN_TUNER_AUTO_SHOP_CLIENT_VEHICLE_SPAWN LOCAL_BIT_TRANSACTION_DONE_FOR_CLIENT_VEHICLES_SLOT_1 TRUE")
					ENDIF
					
					EXIT
				ENDIF
				
				IF clientVehicleTransResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					IF NOT IS_BIT_SET(GET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_GEN_BS), ciTUNERGENERALBITSET_DONE_CUSTOMER_CAR_ARRIVED_CALL)
					AND NOT IS_BIT_SET(g_sTunerFlow.BitSet, ciTUNER_FLOW_BITSET__DO_CUSTOMER_CARS_ARRIVED_CALL)
						SET_BIT(g_sTunerFlow.BitSet, ciTUNER_FLOW_BITSET__DO_CUSTOMER_CARS_ARRIVED_CALL)
						PRINTLN("[AUTO_SHOP_CLIENT_VEH][TUNER] | MAINTAIN_TUNER_AUTO_SHOP_CLIENT_VEHICLE_SPAWN - set ciTUNER_FLOW_BITSET__DO_CUSTOMER_CARS_ARRIVED_CALL")
					ELSE
						IF NOT IS_BIT_SET(g_SimpleInteriorData.iNinthBS, BS9_SIMPLE_INTERIOR_AUTO_SHOP_CLIENT_VEHICLE_TEXT_MSG)
							SEND_TEXT_MESSAGE_TO_CURRENT_PLAYER(CHAR_SESSANTA, "SESSANTA_VEH_AR", TXTMSG_UNLOCKED, TXTMSG_NOT_CRITICAL, TXTMSG_AUTO_UNLOCK_AFTER_READ, NO_REPLY_REQUIRED)
							SET_BIT(g_SimpleInteriorData.iNinthBS, BS9_SIMPLE_INTERIOR_AUTO_SHOP_CLIENT_VEHICLE_TEXT_MSG)
							PRINTLN("[AUTO_SHOP_CLIENT_VEH][TUNER] | MAINTAIN_TUNER_AUTO_SHOP_CLIENT_VEHICLE_SPAWN - set BS9_SIMPLE_INTERIOR_AUTO_SHOP_CLIENT_VEHICLE_TEXT_MSG")
						ELSE
							PRINT_TICKER("SESSANTA_VEH_ART")
						ENDIF	
					ENDIF	
				ELSE
					CLEANUP_TUNER_AUTO_SHOP_CLIENT_SPAWN_TIMER()
				ENDIF	
			ENDIF
		ENDIF
		
		INT iCoolDownTime = g_sMPTunables.iTUNER_AUTO_SHOP_CLIENT_VEH_SPAWN_COOLDOWN_POSIX_TIME
		IF bHasCarLift
			iCoolDownTime = ROUND(iCoolDownTime * g_sMPTunables.fTUNER_AUTO_SHOP_CLIENT_VEH_SPAWN_COOLDOWN_POSIX_TIME_MULTIPLIER)
		ENDIF
		
		iCoolDownTime = iCoolDownTime + GET_CLOUD_TIME_AS_INT()
		
		SET_MP_INT_CHARACTER_STAT(MP_STAT_TUNER_CLIENT_VEHICLE_POSSIX, iCoolDownTime)
		
		#IF IS_DEBUG_BUILD
		bByPassAutoShopRandomTimer = FALSE
		bByPassAutoShopSpawnWeight = FALSE
		iTunerClientVehicleID = 0
		#ENDIF
		
		CLEANUP_TUNER_AUTO_SHOP_CLIENT_SPAWN_TIMER()
		
		PRINTLN("[AUTO_SHOP_CLIENT_VEH] MAINTAIN_TUNER_AUTO_SHOP_CLIENT_VEHICLE_SPAWN MP_STAT_TUNER_CLIENT_VEHICLE_POSSIX: ", iCoolDownTime)
	ENDIF
ENDPROC
#ENDIF

#IF FEATURE_DLC_1_2022
PROC CLEANUP_BIKER_CLIENT_SPAWN_TIMER()
	iBikerShopRandomTime = 0
	RESET_NET_TIMER(sBikerClientSpawnTimer)
	CLEAR_BIT(iLocalBS, LOCAL_BIT_SET_BIKER_SHOP_RAND_TIME)
	CLEAR_BIT(iLocalBS, LOCAL_BIT_BIKER_CLIENT_FOUND_NEW_CLIENT_VEHICLE)
	CLEAR_BIT(iLocalBS, LOCAL_BIT_BIKER_CLIENT_TRANSACTION_DONE)
ENDPROC

FUNC BOOL CAN_START_BIKER_CLIENT_TIMER()
	IF PLAYER_ID() = INVALID_PLAYER_INDEX()
		RETURN FALSE
	ENDIF
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()	
		RETURN FALSE
	ENDIF	
	
	IF NOT IS_FREEMODE()
		RETURN FALSE
	ENDIF	
	
	IF NOT IS_CLUBHOUSE_GARAGE_PURCHASED()
		RETURN FALSE
	ENDIF
	
	IF GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID())
		RETURN FALSE
	ENDIF	
	
	IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	IF NOT DOES_PLAYER_OWN_CLUBHOUSE(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	#IF FEATURE_GEN9_EXCLUSIVE
	IF IS_PLAYER_ON_MP_INTRO()
		RETURN FALSE
	ENDIF
	#ENDIF
	
	IF GET_BIKER_CLIENT_VEHICLE_MODEL() != DUMMY_MODEL_FOR_SCRIPT
	AND NOT IS_BIT_SET(iLocalBS, LOCAL_BIT_BIKER_CLIENT_FOUND_NEW_CLIENT_VEHICLE)
		RETURN FALSE
	ENDIF
	
	IF HAS_BIKER_CLIENT_VEHICLE_SPAWNED_IN_PAST_DAY()
		RETURN FALSE
	ENDIF
	
	IF IS_PLAYER_IN_CLUBHOUSE_PROPERTY(PLAYER_ID())
		RETURN FALSE
	ENDIF
	
	RETURN TRUE
ENDFUNC

FUNC INT GET_BIKER_CLIENT_VEHICLE_STAT_INDEX(MODEL_NAMES vehicleModel)
	SWITCH vehicleModel
		CASE REEVER		RETURN 1
		CASE SHINOBI	RETURN 2
		CASE STRYDER	RETURN 3
		CASE FCR2		RETURN 4
		CASE DIABLOUS2	RETURN 5
		CASE ESSKEY		RETURN 6
		CASE VORTEX		RETURN 7
		CASE DAEMON2	RETURN 8
		CASE ZOMBIEA	RETURN 9
		CASE ZOMBIEB	RETURN 10
		CASE WOLFSBANE	RETURN 11
		CASE NIGHTBLADE	RETURN 12
		CASE MANCHEZ	RETURN 13
		CASE HAKUCHOU2	RETURN 14
		CASE FAGGIO3	RETURN 15
		CASE DEFILER	RETURN 16
		CASE CHIMERA	RETURN 17
		CASE AVARUS		RETURN 18
		CASE BLAZER4	RETURN 19
	ENDSWITCH	
	RETURN 0
ENDFUNC

FUNC BOOL PROCESS_TRANSACTION_FOR_ADDING_BIKER_CLIENT_VEHICLE(CONTRABAND_TRANSACTION_STATE &eResult, INT iVehicleID)

	// Cleanup previous attempts
	IF eResult != CONTRABAND_TRANSACTION_STATE_DEFAULT
	AND eResult != CONTRABAND_TRANSACTION_STATE_PENDING
		eResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
	ENDIF
	
	SWITCH eResult
		// Start the transaction
		CASE CONTRABAND_TRANSACTION_STATE_DEFAULT
			
			IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() != -1
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADDING_BIKER_CLIENT_VEHICLE - Transaction waiting")
				RETURN FALSE
			ELSE
			
				INT iVehicleKey
				iVehicleKey	= GET_BIKER_CLIENT_VEHICLE_INVENTORY_KEY_FOR_CATALOGUE()

				IF NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, CATEGORY_CONTRABAND_QNTY, iVehicleKey, NET_SHOP_ACTION_UPDATE_BUSINESS_GOODS, iVehicleID, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET)
					eResult = CONTRABAND_TRANSACTION_STATE_PENDING
				ELSE
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADDING_BIKER_CLIENT_VEHICLE - Failed to add vehicle")
				ENDIF
				
				IF eResult = CONTRABAND_TRANSACTION_STATE_PENDING
				AND NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADDING_BIKER_CLIENT_VEHICLE - Starting basket checkout")
				ELSE
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADDING_BIKER_CLIENT_VEHICLE - Failed to start checkout")
				ENDIF
			ENDIF
		BREAK
		
		CASE CONTRABAND_TRANSACTION_STATE_PENDING
			IF GET_BASKET_TRANSACTION_SCRIPT_INDEX() = -1
				eResult = CONTRABAND_TRANSACTION_STATE_FAILED
				CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADDING_BIKER_CLIENT_VEHICLE - GET_BASKET_TRANSACTION_SCRIPT_INDEX() = -1 FAILED ")
			ELIF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADDING_BIKER_CLIENT_VEHICLE - Transaction finished - SUCCESS")
					eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ELSE
					CPRINTLN(DEBUG_SHOPS, "PROCESS_TRANSACTION_FOR_ADDING_BIKER_CLIENT_VEHICLE - Transaction finished - FAILED")
					eResult = CONTRABAND_TRANSACTION_STATE_FAILED
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				ENDIF
			ENDIF
		BREAK
	ENDSWITCH
	
	RETURN (eResult != CONTRABAND_TRANSACTION_STATE_PENDING AND eResult != CONTRABAND_TRANSACTION_STATE_DEFAULT)
ENDFUNC

FUNC BOOL ADD_BIKER_CLIENT_VEHICLE(INT iVehicleID, CONTRABAND_TRANSACTION_STATE &eResult)
	IF iVehicleID < 0
		PRINTLN("ADD_BIKER_CLIENT_VEHICLE - Attempting to add iVehicleID: ", iVehicleID)
		eResult = CONTRABAND_TRANSACTION_STATE_FAILED
		RETURN TRUE
	ENDIF

	IF USE_SERVER_TRANSACTIONS()
		IF PROCESS_TRANSACTION_FOR_ADDING_BIKER_CLIENT_VEHICLE(eResult, iVehicleID)			
			IF eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS				
				PRINTLN("ADD_BIKER_CLIENT_VEHICLE - PC: Attempting to add iVehicleID: ", iVehicleID)
				SET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_BIKER_CLIENT_VEHICLE(), iVehicleID)
			ENDIF
		ENDIF
		
		RETURN (eResult != CONTRABAND_TRANSACTION_STATE_PENDING AND eResult != CONTRABAND_TRANSACTION_STATE_DEFAULT)
	ELSE
		IF iVehicleID != -1
			PRINTLN("ADD_BIKER_CLIENT_VEHICLE - Console: Attempting to add iVehicleID: ", iVehicleID)
			SET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_BIKER_CLIENT_VEHICLE(), iVehicleID)
		ENDIF	

		eResult = CONTRABAND_TRANSACTION_STATE_SUCCESS

		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL SHOULD_GIVE_BIKER_CLIENT_VEHICLE_THIS_TIME()
	
	IF g_sMPTunables.iTUNER_AUTO_SHOP_CLIENT_VEH_SPAWN_WEIGHT >= 100
		RETURN TRUE
	ENDIF	
	
	IF g_sMPTunables.iTUNER_AUTO_SHOP_CLIENT_VEH_SPAWN_WEIGHT <= 0
		RETURN FALSE
	ENDIF	
	
	INT iSumOfWeight = 101
	INT iRandom = GET_RANDOM_INT_IN_RANGE(0, (iSumOfWeight))

	IF iRandom <= g_sMPTunables.iTUNER_AUTO_SHOP_CLIENT_VEH_SPAWN_WEIGHT
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC 

#IF IS_DEBUG_BUILD
PROC UPDATE_BIKER_AUTO_SHOP_DEBUG_WIDGET()
	IF bResetBikerShopVehiclePickTimer
		CLEANUP_BIKER_CLIENT_SPAWN_TIMER()
		SET_MP_INT_CHARACTER_STAT(MP_STAT_BIKER_CLIENT_VEHICLE_POSSIX, 0)
		bResetBikerShopVehiclePickTimer = FALSE
	ENDIF
	
	IF bResetBikerShopVehicleModels
		SET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_BIKER_CLIENT_VEHICLE_MODIFICATION(), 0)
		SET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_BIKER_CLIENT_VEHICLE(), 0)
		CLEANUP_BIKER_CLIENT_VEHICLE_MODIFICATION_TASKS()
		CLEAR_BIKER_CLIENT_VEHICLE_SETUP()
		SET_PLAYER_SELECTED_TO_DELIVER_BIKER_CLIENT_VEHICLE(FALSE)
		CLEANUP_BIKER_CLIENT_SPAWN_TIMER()
		SET_BIKER_CLIENT_VEHICLE_MODEL(DUMMY_MODEL_FOR_SCRIPT)
		bResetBikerShopVehicleModels = FALSE
	ENDIF
ENDPROC
#ENDIF

PROC MAINTAIN_BIKER_CLIENT_VEHICLE_SPAWN()
	#IF IS_DEBUG_BUILD
	UPDATE_BIKER_AUTO_SHOP_DEBUG_WIDGET()
	#ENDIF

	IF NOT CAN_START_BIKER_CLIENT_TIMER()
	#IF IS_DEBUG_BUILD
	AND NOT bByPassBikerShopSpawnWeight
	#ENDIF
		CLEANUP_BIKER_CLIENT_SPAWN_TIMER()
		EXIT
	ENDIF

	IF NOT IS_BIT_SET(iLocalBS, LOCAL_BIT_SET_BIKER_SHOP_RAND_TIME)
		// Random time in seconds
		iBikerShopRandomTime = GET_RANDOM_INT_IN_RANGE(0, 600)
		
		IF SHOULD_GIVE_BIKER_CLIENT_VEHICLE_THIS_TIME()
		#IF IS_DEBUG_BUILD
		OR bByPassBikerShopSpawnWeight
		#ENDIF
			SET_BIT(iLocalBS, LOCAL_BIT_BIKER_CLIENT_GIVE_VEHICLE_THIS_TIME)
			PRINTLN("[BIKER_CLIENT_VEH] MAINTAIN_BIKER_CLIENT_VEHICLE_SPAWN LOCAL_BIT_SHOULD_GIVE_VEHICLE_THIS_TIME TRUE")
		ELSE
			CLEAR_BIT(iLocalBS, LOCAL_BIT_BIKER_CLIENT_GIVE_VEHICLE_THIS_TIME)
			PRINTLN("[BIKER_CLIENT_VEH] MAINTAIN_BIKER_CLIENT_VEHICLE_SPAWN LOCAL_BIT_SHOULD_GIVE_VEHICLE_THIS_TIME FALSE")
		ENDIF
		
		SET_BIT(iLocalBS, LOCAL_BIT_SET_BIKER_SHOP_RAND_TIME)
		PRINTLN("[BIKER_CLIENT_VEH] MAINTAIN_BIKER_CLIENT_VEHICLE_SPAWN LOCAL_BIT_SET_BIKER_SHOP_RAND_TIME TRUE iBikerShopRandomTime ", iBikerShopRandomTime)
	ENDIF
	
	IF HAS_NET_TIMER_EXPIRED(sBikerClientSpawnTimer, (iBikerShopRandomTime * 1000))
	#IF IS_DEBUG_BUILD
	OR bByPassBikerShopRandomTimer
	#ENDIF
		INT iRandomColour
		IF IS_BIT_SET(iLocalBS, LOCAL_BIT_BIKER_CLIENT_GIVE_VEHICLE_THIS_TIME)
			IF NOT IS_BIT_SET(iLocalBS, LOCAL_BIT_BIKER_CLIENT_FOUND_NEW_CLIENT_VEHICLE)
				IF GET_BIKER_CLIENT_VEHICLE_MODEL() = DUMMY_MODEL_FOR_SCRIPT
				AND GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_BIKER_CLIENT_VEHICLE()) = 0
					RESET_VEHICLE_SETUP_STRUCT_MP(g_sBikerClientVehicleSetupStruct.vehicleSetupMP)
					iRandomColour = GET_RANDOM_INT_IN_RANGE(0, 27)
					iRandomColour = GET_CLIENT_VEHICLE_COLOUR_FROM_RANDOM_INT(iRandomColour)
					
					g_sBikerClientVehicleSetupStruct.vehicleSetupMP.VehicleSetup.iColour1 = iRandomColour
					g_sBikerClientVehicleSetupStruct.vehicleSetupMP.VehicleSetup.iColour2 = iRandomColour
					
					INT iRandomModel = GET_RANDOM_INT_IN_RANGE(1, 20)
					
					#IF IS_DEBUG_BUILD
					IF iBikerClientVehicleID != 0
						iRandomModel = iBikerClientVehicleID
					ENDIF	
					#ENDIF
					
					MODEL_NAMES vehicleModel = GET_BIKER_CLIENT_VEHICLE_MODEL_FROM_INT(iRandomModel)
					
					SET_BIKER_CLIENT_VEHICLE_MODEL(vehicleModel)
					PRINTLN("[BIKER_CLIENT_VEH] MAINTAIN_BIKER_CLIENT_VEHICLE_SPAWN - LOCAL_BIT_BIKER_CLIENT_FOUND_NEW_CLIENT_VEHICLE TRUE")
					SET_BIT(iLocalBS, LOCAL_BIT_BIKER_CLIENT_FOUND_NEW_CLIENT_VEHICLE)
					MP_SAVE_BIKER_CLIENT_VEHICLE_SLOT_STATS_FROM_SAVEGAME(g_sBikerClientVehicleSetupStruct)
				ENDIF
				
				EXIT
			ELSE
				IF NOT IS_BIT_SET(iLocalBS, LOCAL_BIT_BIKER_CLIENT_TRANSACTION_DONE)
					IF NOT ADD_BIKER_CLIENT_VEHICLE(GET_BIKER_CLIENT_VEHICLE_STAT_INDEX(GET_BIKER_CLIENT_VEHICLE_MODEL()), bikerClientVehicleTransResult)
						EXIT	
					ENDIF
					
					IF bikerClientVehicleTransResult != CONTRABAND_TRANSACTION_STATE_SUCCESS
						SET_BIKER_CLIENT_VEHICLE_MODEL(DUMMY_MODEL_FOR_SCRIPT)
					ENDIF
					
					SET_BIT(iLocalBS, LOCAL_BIT_BIKER_CLIENT_TRANSACTION_DONE)
					PRINTLN("[BIKER_CLIENT_VEH] MAINTAIN_BIKER_CLIENT_VEHICLE_SPAWN - LOCAL_BIT_BIKER_CLIENT_TRANSACTION_DONE TRUE")
					EXIT
				ENDIF
				
				IF bikerClientVehicleTransResult = CONTRABAND_TRANSACTION_STATE_SUCCESS
					IF NOT IS_BIT_SET(g_sSummer2022Flow.iBitSet, ciSUM22_FLOW_BITSET_MALC_CALL_FOR_FIRST_CLIENT_BIKE_DELIVERY)
					AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_SUM22_DONE_FIRST_BIKE_DELIVERY_CALL_MALC)
						SET_BIT(g_sSummer2022Flow.iBitSet, ciSUM22_FLOW_BITSET_MALC_CALL_FOR_FIRST_CLIENT_BIKE_DELIVERY)
						PRINTLN("[BIKER_CLIENT_VEH] MAINTAIN_BIKER_CLIENT_VEHICLE_SPAWN - set ciSUM22_FLOW_BITSET_MALC_CALL_FOR_FIRST_CLIENT_BIKE_DELIVERY")
					ELSE
						PRINT_TICKER("BIKER_CLIENT_TIK")	
					ENDIF	
				ELSE
					CLEANUP_BIKER_CLIENT_SPAWN_TIMER()
				ENDIF	
			ENDIF
		ENDIF	
		
		INT iCoolDownTime = g_sMPTunables.iBIKER_CLIENT_VEH_SPAWN_COOLDOWN_POSIX_TIME

		iCoolDownTime = iCoolDownTime + GET_CLOUD_TIME_AS_INT()
		
		SET_MP_INT_CHARACTER_STAT(MP_STAT_BIKER_CLIENT_VEHICLE_POSSIX, iCoolDownTime)
		
		#IF IS_DEBUG_BUILD
		bByPassBikerShopRandomTimer = FALSE
		bByPassBikerShopSpawnWeight = FALSE
		iBikerClientVehicleID = 0
		#ENDIF
		
		CLEANUP_BIKER_CLIENT_SPAWN_TIMER()
	ENDIF
ENDPROC
#ENDIF

// url:bugstar:7272016 - [PUBLIC][REPORTED] Auto Shop - Repair Business - Players receive a transaction error when delivering client vehicles from the auto shop
CONTRABAND_TRANSACTION_STATE clientVehicleTransResultTU
PROC FIX_FOR_7272016()
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR NOT IS_SKYSWOOP_AT_GROUND()
	OR NOT IS_PC_VERSION()
		CLEAR_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_TU_DONE_FOR_CLIENT_VEHICLES_SLOT_1)
		CLEAR_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_TU_DONE_FOR_CLIENT_VEHICLES_SLOT_2)
		clientVehicleTransResultTU = CONTRABAND_TRANSACTION_STATE_DEFAULT
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBS, LOCAL_BIT_TRANSACTION_TU_DONE_FOR_CLIENT_VEHICLES_SLOT_1)
	AND IS_BIT_SET(iLocalBS, LOCAL_BIT_TRANSACTION_TU_DONE_FOR_CLIENT_VEHICLES_SLOT_2)
		EXIT
	ENDIF	
	
	IF NOT IS_BIT_SET(iLocalBS, LOCAL_BIT_TRANSACTION_TU_DONE_FOR_CLIENT_VEHICLES_SLOT_1)
	AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CLIENT_VEHICLE_SLOT_1_FIX)
		IF (GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CLIENT_VEHICLE(0))	= 0	
		AND GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CLIENT_VEHICLE_MODIFICATION(0))	!= 0
		AND INT_TO_ENUM(MODEL_NAMES, GET_MP_INT_CHARACTER_STAT(MP_STAT_MPSV_MODEL_TUNER_VEH1, GET_ACTIVE_CHARACTER_SLOT())) != DUMMY_MODEL_FOR_SCRIPT)
		OR clientVehicleTransResultTU = CONTRABAND_TRANSACTION_STATE_PENDING
		
			MODEL_NAMES vehicleModel = INT_TO_ENUM(MODEL_NAMES, GET_MP_INT_CHARACTER_STAT(MP_STAT_MPSV_MODEL_TUNER_VEH1, GET_ACTIVE_CHARACTER_SLOT()))
			IF NOT ADD_TUNER_CLIENT_VEHICLE(GET_TUNER_CLIENT_VEHICLE_STAT_INDEX(vehicleModel), 0, clientVehicleTransResultTU)
				EXIT
			ENDIF

			IF clientVehicleTransResultTU = CONTRABAND_TRANSACTION_STATE_SUCCESS
				PRINTLN("[FIX_FOR_7272016] clientVehicleTransResultTU CONTRABAND_TRANSACTION_STATE_SUCCESS slot 0 ")	
			ENDIF
			
			clientVehicleTransResultTU = CONTRABAND_TRANSACTION_STATE_DEFAULT
			SET_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_TU_DONE_FOR_CLIENT_VEHICLES_SLOT_1)
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CLIENT_VEHICLE_SLOT_1_FIX, TRUE)
		ELSE
			SET_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_TU_DONE_FOR_CLIENT_VEHICLES_SLOT_1)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBS, LOCAL_BIT_TRANSACTION_TU_DONE_FOR_CLIENT_VEHICLES_SLOT_2)
	AND IS_BIT_SET(iLocalBS, LOCAL_BIT_TRANSACTION_TU_DONE_FOR_CLIENT_VEHICLES_SLOT_1)
	AND NOT GET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CLIENT_VEHICLE_SLOT_2_FIX)
		IF (GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CLIENT_VEHICLE(1))	= 0	
		AND GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CLIENT_VEHICLE_MODIFICATION(1))	!= 0
		AND INT_TO_ENUM(MODEL_NAMES, GET_MP_INT_CHARACTER_STAT(MP_STAT_MPSV_MODEL_TUNER_VEH2, GET_ACTIVE_CHARACTER_SLOT())) != DUMMY_MODEL_FOR_SCRIPT)
		OR clientVehicleTransResultTU = CONTRABAND_TRANSACTION_STATE_PENDING
			MODEL_NAMES vehicleModel = INT_TO_ENUM(MODEL_NAMES, GET_MP_INT_CHARACTER_STAT(MP_STAT_MPSV_MODEL_TUNER_VEH2, GET_ACTIVE_CHARACTER_SLOT()))
			IF NOT ADD_TUNER_CLIENT_VEHICLE(GET_TUNER_CLIENT_VEHICLE_STAT_INDEX(vehicleModel), 1, clientVehicleTransResultTU)
				EXIT
			ENDIF

			IF clientVehicleTransResultTU = CONTRABAND_TRANSACTION_STATE_SUCCESS
				PRINTLN("[FIX_FOR_7272016] clientVehicleTransResultTU CONTRABAND_TRANSACTION_STATE_SUCCESS slot 1 ")	
			ENDIF
			
			clientVehicleTransResultTU = CONTRABAND_TRANSACTION_STATE_DEFAULT
			SET_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_TU_DONE_FOR_CLIENT_VEHICLES_SLOT_2)
			SET_PACKED_STAT_BOOL(PACKED_MP_BOOL_CLIENT_VEHICLE_SLOT_2_FIX, TRUE)
		ELSE
			SET_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_TU_DONE_FOR_CLIENT_VEHICLES_SLOT_2)
		ENDIF
	ENDIF
ENDPROC

CONTRABAND_TRANSACTION_STATE clientVehicleRemoveTransResult
PROC FIX_FOR_AUTO_SHOP_MODIFICATION_MISSMATCH()
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR NOT IS_SKYSWOOP_AT_GROUND()
	OR NOT IS_PC_VERSION()
		CLEAR_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_TU_REMOVE_CLIENT_VEHICLES_SLOT_1)
		CLEAR_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_TU_REMOVE_CLIENT_VEHICLES_SLOT_2)
		clientVehicleRemoveTransResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
		EXIT
	ENDIF
	
	IF IS_BIT_SET(iLocalBS, LOCAL_BIT_TRANSACTION_TU_REMOVE_CLIENT_VEHICLES_SLOT_1)
	AND IS_BIT_SET(iLocalBS, LOCAL_BIT_TRANSACTION_TU_REMOVE_CLIENT_VEHICLES_SLOT_2)
		EXIT
	ENDIF	
	
	IF NOT IS_BIT_SET(iLocalBS, LOCAL_BIT_TRANSACTION_TU_REMOVE_CLIENT_VEHICLES_SLOT_1)
		IF GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CLIENT_VEHICLE_MODIFICATION(0)) != 0
			MODEL_NAMES vehicleModel = INT_TO_ENUM(MODEL_NAMES, GET_MP_INT_CHARACTER_STAT(MP_STAT_MPSV_MODEL_TUNER_VEH1, GET_ACTIVE_CHARACTER_SLOT()))
		
			IF (GET_TUNER_CLIENT_VEHICLE_PRICE_GROUP(vehicleModel) != 0
			AND GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CLIENT_VEHICLE_MODIFICATION(0)) != GET_TUNER_CLIENT_VEHICLE_PRICE_GROUP(vehicleModel))
			OR clientVehicleRemoveTransResult = CONTRABAND_TRANSACTION_STATE_PENDING
			
				SET_AUTO_SHOP_CLIENT_VEHICLE_DELIVERY_SLOT(0)
				REMOVE_TUNER_CLIENT_VEHICLE(GET_TUNER_CLIENT_VEHICLE_PRICE(vehicleModel), REMOVE_CONTRA_MISSION_PASSED, clientVehicleRemoveTransResult, TRUE)
				IF clientVehicleRemoveTransResult = CONTRABAND_TRANSACTION_STATE_SUCCESS OR clientVehicleRemoveTransResult = CONTRABAND_TRANSACTION_STATE_FAILED
					SET_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_TU_REMOVE_CLIENT_VEHICLES_SLOT_1)
					clientVehicleRemoveTransResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
					PRINTLN("[FIX_FOR_AUTO_SHOP_MODIFICATION_MISSMATCH] LOCAL_BIT_TRANSACTION_TU_REMOVE_CLIENT_VEHICLES_SLOT_1 TRUE")
				ELSE
					EXIT
				ENDIF
			ELSE
				SET_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_TU_REMOVE_CLIENT_VEHICLES_SLOT_1)
			ENDIF
		ELSE
			SET_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_TU_REMOVE_CLIENT_VEHICLES_SLOT_1)
		ENDIF
	ENDIF
	
	IF NOT IS_BIT_SET(iLocalBS, LOCAL_BIT_TRANSACTION_TU_REMOVE_CLIENT_VEHICLES_SLOT_2)
		IF GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CLIENT_VEHICLE_MODIFICATION(1)) != 0
			MODEL_NAMES vehicleModel = INT_TO_ENUM(MODEL_NAMES, GET_MP_INT_CHARACTER_STAT(MP_STAT_MPSV_MODEL_TUNER_VEH2, GET_ACTIVE_CHARACTER_SLOT()))
		
			IF (GET_TUNER_CLIENT_VEHICLE_PRICE_GROUP(vehicleModel) != 0
			AND GET_MP_INT_CHARACTER_STAT(GET_FM_INT_STAT_FOR_CLIENT_VEHICLE_MODIFICATION(1)) != GET_TUNER_CLIENT_VEHICLE_PRICE_GROUP(vehicleModel))
			OR clientVehicleRemoveTransResult = CONTRABAND_TRANSACTION_STATE_PENDING
			
				SET_AUTO_SHOP_CLIENT_VEHICLE_DELIVERY_SLOT(1)
				REMOVE_TUNER_CLIENT_VEHICLE(GET_TUNER_CLIENT_VEHICLE_PRICE(vehicleModel), REMOVE_CONTRA_MISSION_PASSED, clientVehicleRemoveTransResult, TRUE)
				IF clientVehicleRemoveTransResult = CONTRABAND_TRANSACTION_STATE_SUCCESS OR clientVehicleRemoveTransResult = CONTRABAND_TRANSACTION_STATE_FAILED
					SET_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_TU_REMOVE_CLIENT_VEHICLES_SLOT_2)
					clientVehicleRemoveTransResult = CONTRABAND_TRANSACTION_STATE_DEFAULT
					PRINTLN("[FIX_FOR_AUTO_SHOP_MODIFICATION_MISSMATCH] LOCAL_BIT_TRANSACTION_TU_REMOVE_CLIENT_VEHICLES_SLOT_2 TRUE")
				ELSE
					EXIT
				ENDIF
			ELSE
				SET_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_TU_REMOVE_CLIENT_VEHICLES_SLOT_2)
			ENDIF
		ELSE
			SET_BIT(iLocalBS, LOCAL_BIT_TRANSACTION_TU_REMOVE_CLIENT_VEHICLES_SLOT_2)
		ENDIF
	ENDIF
	
ENDPROC

PROC MAINTAIN_PLAYER_TATTOO_DATA()

	IF g_bBlockTattooDataMaintain
	OR NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR NOT HAS_IMPORTANT_STATS_LOADED()
	OR NOT IS_FREEMODE()
		bUpdatePlayersTattooData = TRUE
		bPlayerChangingClothesLastFrame = TRUE
		EXIT
	ENDIF
	
	// Update whenever we stop changing clothes, this includes changing tattoos.
	IF IS_PLAYER_CHANGING_CLOTHES()
		bPlayerChangingClothesLastFrame = TRUE
		EXIT
	ENDIF
	
	IF bPlayerChangingClothesLastFrame
		tdPlayerChangingClothes = GET_NETWORK_TIME()
		bUpdatePlayersTattooData = TRUE
		bPlayerChangingClothesLastFrame = FALSE
		EXIT
	ENDIF
	
	IF bUpdatePlayersTattooData
	AND ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), tdPlayerChangingClothes)) > 500
		bUpdatePlayersTattooData = FALSE
		SET_PLAYER_TATTOO_DATA()
	ENDIF
ENDPROC

#IF IS_DEBUG_BUILD
DEBUGONLY PROC MAINTAIN_PHONE_TOGGLE()
	IF GET_COMMANDLINE_PARAM_EXISTS("sc_EnablePhoneDisabling")
		IF NOT NETWORK_IS_ACTIVITY_SESSION()
		AND GET_CURRENT_GAMEMODE() = GAMEMODE_FM
			IF GET_COMMANDLINE_PARAM_EXISTS("sc_PhoneDisabledByDefault")
			AND NOT g_bScriptPhoneBlockerDefaultSet
				g_bScriptPhoneBlockerActive = TRUE
				g_bScriptPhoneBlockerDefaultSet = TRUE
				PRINTLN("DISABLE_CELLPHONE_THIS_FRAME_ONLY - sc_PhoneDisabledByDefault - Phone disable = TRUE")
			ENDIF
			
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_NUMPAD7, KEYBOARD_MODIFIER_ALT, "Disable Phone - Script block")
				g_bScriptPhoneBlockerActive = !g_bScriptPhoneBlockerActive
				PRINTLN("DISABLE_CELLPHONE_THIS_FRAME_ONLY - sc_EnablePhoneDisabling - Toggled phone disable: ", g_bScriptPhoneBlockerActive)
			ENDIF
			
			IF g_bScriptPhoneBlockerActive
			AND NOT (GB_IS_PLAYER_ON_ANY_GANG_BOSS_MISSION(PLAYER_ID()) AND GB_IS_PLAYER_CRITICAL_TO_JOB(PLAYER_ID()))
				DISABLE_CELLPHONE_THIS_FRAME_ONLY()
				
				SET_TEXT_SCALE(0.6, 0.6)
				SET_TEXT_COLOUR(51, 102, 255, 255)
				SET_TEXT_OUTLINE()
				DISPLAY_TEXT_WITH_LITERAL_STRING(0.55, 0.85, "STRING", "PHONE BLOCKED")
				
				IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL, INPUT_CELLPHONE_UP)
				AND NOT IS_CUSTOM_MENU_ON_SCREEN()
				AND NOT IS_INTERACTION_MENU_OPEN()
					START_BROWSER()
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC
#ENDIF

PROC MAINTAIN_LASTGEN_STATUS_OVERRIDE()
	// url:bugstar:7201377 - Players do not appear to be receiving the 'Returning Player Benefits'
	IF g_bCheckAndForceLastGenPlayer
		IF NOT platformUpgradeLBCheck.bComplete
			platformUpgradeLBCheck.bComplete = TRUE
			PRINTLN("[LASTGEN] MAINTAIN_LASTGEN_STATUS_OVERRIDE Forcing platformUpgradeLBCheck.bComplete = TRUE")
		ENDIF
		IF NOT platformUpgradeLBCheckSP.bComplete
			platformUpgradeLBCheckSP.bComplete = TRUE
			PRINTLN("[LASTGEN] MAINTAIN_LASTGEN_STATUS_OVERRIDE Forcing platformUpgradeLBCheckSP.bComplete = TRUE")
		ENDIF
		IF g_i_Private_AreCharactersLastGen != LAST_GEN_STATUS_IS_LAST_GEN
			g_i_Private_AreCharactersLastGen = LAST_GEN_STATUS_IS_LAST_GEN
			PRINTLN("[LASTGEN] MAINTAIN_LASTGEN_STATUS_OVERRIDE Forcing g_i_Private_AreCharactersLastGen = LAST_GEN_STATUS_IS_LAST_GEN")
		ENDIF
		IF g_i_Private_IsPlayerFromLastGen != LAST_GEN_STATUS_IS_LAST_GEN
			g_i_Private_IsPlayerFromLastGen = LAST_GEN_STATUS_IS_LAST_GEN
			PRINTLN("[LASTGEN] MAINTAIN_LASTGEN_STATUS_OVERRIDE Forcing g_i_Private_IsPlayerFromLastGen = LAST_GEN_STATUS_IS_LAST_GEN")
		ENDIF
	ENDIF
ENDPROC

SCRIPT
	
	PRINTSTRING("\nStarting shop controller")PRINTNL()
	
	NETWORK_SET_SCRIPT_IS_SAFE_FOR_NETWORK_GAME()

	// Setup some debug widgets
	#IF IS_DEBUG_BUILD
		SETUP_SHOP_CONTROL_WIDGETS()
	#ENDIF
	
	// Make sure snow feet are turned off by default.
	USE_SNOW_FOOT_VFX_WHEN_UNSHELTERED(FALSE)
	
	USE_SNOW_WHEEL_VFX_WHEN_UNSHELTERED(FALSE)
	
	#IF FEATURE_COPS_N_CROOKS
	_SCRIPT_UNLOCKABLE_VALIDATE_METADATA()
	#ENDIF
	
	#IF IS_DEBUG_BUILD
	INT iPackedStatEnd = (ENUM_TO_INT(PACKED_STAT_END) - 1)
	STATS_CHECK_PACKED_STATS_END_VALUE(iPackedStatEnd)
	#ENDIF
	
	// Main loop
	WHILE (TRUE)
	
		MAINTAIN_LASTGEN_STATUS_OVERRIDE()
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			bInNightclubThisFrame = IS_PLAYER_IN_NIGHTCLUB(PLAYER_ID())
		ENDIF
		
		#IF IS_DEBUG_BUILD
		MAINTAIN_PHONE_TOGGLE()
		#ENDIF
		
		// This script needs to cleanup only when the game runs the magdemo
		IF NOT bForceCleanupSetup
			PRINTLN("...shop_controller.sc setting up a new force cleanup.")
			bForceCleanupSetup = TRUE
			IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_MAGDEMO|FORCE_CLEANUP_FLAG_REPEAT_PLAY))
				PRINTLN("...shop_controller.sc has been forced to cleanup.")

				//Only clean up with a REPEAT_PLAY flag if we know it is for a recording playback.
				IF GET_CAUSE_OF_MOST_RECENT_FORCE_CLEANUP() = FORCE_CLEANUP_FLAG_REPEAT_PLAY
					IF NOT g_bForceCleaupShopsForRecordingPlayback
						PRINTLN("...shop_controller.sc has found a repeat play cleanup without the recoring playback flag.")
						PRINTLN("...shop_controller.sc skipping force cleanup.")
						bForceCleanupSetup = FALSE //Set up a new force cleanup next frame.
					ELSE
						PRINTLN("...shop_controller.sc cleaning up for recording playback.")
						TERMINATE_THIS_THREAD()
					ENDIF
					g_bForceCleaupShopsForRecordingPlayback = FALSE
				ELSE
					PRINTLN("...shop_controller.sc cleaning up for magdemo.")
					TERMINATE_THIS_THREAD()
				ENDIF
			ENDIF
		ENDIF
	
		eCurrentPed = GET_CURRENT_PLAYER_PED_ENUM()
	
		// Maintain the debug widgets
		#IF IS_DEBUG_BUILD
			MAINTAIN_SHOP_CONTROL_WIDGETS()
			DISPLAY_CASH_TRANSACTION_DEBUG()
		#ENDIF
		
		PROCESS_MP_WARDROBE_LAUNCHER()
		
		CHECK_CONTROLLER_RESET()
		
		// One off checks that we can re-use
		IF eStage > SC_STAGE_INIT
			IF NETWORK_IS_GAME_IN_PROGRESS()
				bPlayerOnMPMission = IS_PLAYER_ON_ANY_FM_MISSION(PLAYER_ID())
				bPlayerOnMPActivityTurorial = IS_LOCAL_PLAYER_RUNNING_ACTIVITY_TUTORIAL()
				bTutorialsComplete = HAS_FM_INTRO_MISSION_BEEN_DONE()
			ELSE
				bPlayerOnSPMission = (IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY) OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_STORY_FRIENDS))
			ENDIF
		ENDIF
		
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			g_sShopSettings.playerInterior = GET_INTERIOR_FROM_ENTITY(PLAYER_PED_ID())
			g_sShopSettings.playerRoom = GET_ROOM_KEY_FROM_ENTITY(PLAYER_PED_ID())
			
			IF IS_VALID_INTERIOR(g_sShopSettings.playerInterior)
			OR IS_ANY_TYPE_OF_CUTSCENE_PLAYING()
				SET_BIT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_INSIDE_INTERIOR)
			ELSE
				CLEAR_BIT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_INSIDE_INTERIOR)
			ENDIF
			
			IF USE_SERVER_TRANSACTIONS()
				IF NET_GAMESERVER_TRANSACTION_IN_PROGRESS()
					SET_BIT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_RUNNING_TRANSACTION)
				ELSE
					CLEAR_BIT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_RUNNING_TRANSACTION)
				ENDIF
			ELSE
				CLEAR_BIT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_RUNNING_TRANSACTION)
			ENDIF
		ELSE
			g_sShopSettings.playerInterior = NULL
			g_sShopSettings.playerRoom = 0
			
			CLEAR_BIT(GlobalPlayerBD[NATIVE_TO_INT(PLAYER_ID())].iOrbitalCannonBS, ORBITAL_CANNON_GLOBAL_BS_INSIDE_INTERIOR)
		ENDIF
		
		INITIALISE_SHOP_ENTITY_SETS()
		
		PROCESS_STORE_ALERT()
		
		PROCESS_RESEARCH_UNLOCK()
		
		// PC TRANSACTION SPECIFIC PROCESSING.
		IF USE_SERVER_TRANSACTIONS()
			PROCESS_CASH_TRANSACTION_ALERT()
			MAINTAIN_SHOP_TRANSACTION_RETRY_SYSTEM()
			PROCESS_NET_TRANSACTION_EVENTS()
			MANAGE_CASH_TRANSACTION_EVENTS()
			PROCESS_REWARD_TRANSACTIONS_QUEUE()
			RUN_TRANSACTION_FOR_FACTORY_RESET()
			RUN_TRANSACTION_FOR_DEFEND_MISSION_PRODUCT_REMOVAL()
			RUN_TRANSACTION_TO_UPDATE_IE_VEH_SLOT()
			MAINTAIN_CASINO_LUCKY_WHEEL_PODIUM_USAGE_TRANSACTION()
			MAINTAIN_EXPLOIT_TRANSACTIONS(sExploitReportingdata)
		ENDIF
		
		MANAGE_XMAS_CONTENT()
		
		MANAGE_INDEPENDENCE_CONTENT()
		
		MANAGE_SHOP_BROWSE_TIME()
		
		MANAGE_PURCHASED_PARACHUTES()
		
		MANAGE_CREW_EMBLEM_CHECKS()
		
		ADJUST_FIRST_PERSON_AIMING()
		
		MAINTAIN_REMOVAL_OF_FORBIDDEN_SP_VEHICLES()
		
		MAINTAIN_PLAYER_TATTOO_DATA()
		
			
			#IF NOT IS_NEXTGEN_BUILD
				MANAGE_RPG_TRAIL_PTFX()
			#ENDIF
			
			MANAGE_TANK_PTFX()
			MANAGE_HEIST_CONTENT()
		
		MANAGE_AIR_BOMB_PTFX()
		
		MANAGE_ORBITAL_CANNON_PTFX()
		
		MAINTAIN_RADIO_UNLOCKS()
		
		#IF FEATURE_FIXER
		MAINTAIN_SECURITY_CONTRACT_LAUNCING_WITH_DELAY()
		#ENDIF
		
		MAINTAIN_ISLAND_HEIST_RADIO_UNLOCK()
		
		#IF FEATURE_TUNER
		MAINTAIN_TUNER_RADIO_UNLOCK()
		#ENDIF
		
		MAINTAIN_FIXER_RADIO_UNLOCK()
		
		FIX_FOR_7272016()
		
		FIX_FOR_AUTO_SHOP_MODIFICATION_MISSMATCH()
		
		PROCESS_CACHED_DLC_DATA()
		
		PROCESS_HELMET_VISOR()
		
		PROCESS_CUSTOM_CREW_LOGO()
		
		PROCESS_SCUBA_GEAR()
		
		RUN_TRANSACTION_TO_ADD_PAID_SUPPLIES()

		#IF IS_DEBUG_BUILD
			RUN_DELIVERED_IE_VEH_EVENT_TEST_WIDGETS(eIEVehTestData)
			MAINTAIN_TUN_ASD_EXT_TEST()
		#ENDIF
		
		
		PROCESS_ILLUMINATED_CLOTHING()
		
		PROCESS_INVALID_PED_COMPONENTS()
		PROCESS_INVALID_PLAYER_PURCHASED_HAIR()
		
		MANAGE_CASINO_CHIPS_HUD()
		MAINTAIN_ADD_SPECIAL_ITEM_FROM_LW_TO_WAREHOUSE()
		
		#IF FEATURE_TUNER
		MAINTAIN_TUNER_AUTO_SHOP_CLIENT_VEHICLE_SPAWN()
		MAINTAIN_PIM_FAVOURITE_RADIO_STATIONS()
		#ENDIF
		
		#IF FEATURE_DLC_1_2022
		MAINTAIN_BIKER_CLIENT_VEHICLE_SPAWN()
		MAINTAIN_SEND_PLAYSTATS_INVENTORY_TELEMETRY_DATA_WITH_DELAY()
		#ENDIF
		
		#IF FEATURE_COPS_N_CROOKS
		MAINTAIN_ARCADE_TOKENS()
		MAINTAIN_SUI_PURCHASE()
		#ENDIF
		
		#IF FEATURE_GTAO_MEMBERSHIP
		MAINTAIN_GTAO_MEMBERSHIP(sGTAOMembership)
		MAINTAIN_TELEMETRY_SENDING_DELAY_TIMER()
		#ENDIF
		
		FIX_MOC_SAVE_SLOT()
		FIX_FOR_RC_VEH_SLOTS()
		PERVENT_PLAYER_FROM_ENTERING_RC_VEHS_ON_FOOT()
		
		SWITCH eStage
			CASE SC_STAGE_INIT
				DO_INITIALISE()
			BREAK
			
			CASE SC_STAGE_UPDATE
				DO_UPDATE()
			BREAK
			
			CASE SC_STAGE_RESET
				DO_RESET()
			BREAK
		ENDSWITCH
		
		//PRINTLN("PACKED_MP_CHANGE_APPERANCE = ", PACKED_MP_CHANGE_APPERANCE, ", ", GET_INT_PACKED_STAT_KEY(PACKED_MP_CHANGE_APPERANCE), ", ", GET_INT_PACKED_STAT_BITSHIFT(PACKED_MP_CHANGE_APPERANCE))
		//PRINTLN("PACKED_MP_CHANGE_APPERANCE = ", PACKED_MP_BOOL_MP_BATTLE_MSG_CLOTHING_036_DJ_BLACK_MADONNA, ", ", GET_BOOL_PACKED_STAT_KEY(PACKED_MP_BOOL_MP_BATTLE_MSG_CLOTHING_036_DJ_BLACK_MADONNA), ", ", GET_BOOL_PACKED_STAT_BITSHIFT(PACKED_MP_BOOL_MP_BATTLE_MSG_CLOTHING_036_DJ_BLACK_MADONNA))
		
//		PRINTLN("PACKED_MP_SIGNAL_JAMMER_0 = ",PACKED_MP_SIGNAL_JAMMER_0," , ", GET_BOOL_PACKED_STAT_KEY(PACKED_MP_SIGNAL_JAMMER_0), ", Bitshift ", GET_BOOL_PACKED_STAT_BITSHIFT(PACKED_MP_SIGNAL_JAMMER_0))
//		INT iOffset
		//STATS_PACKED packedStat
//		REPEAT 4 iOffset
//			packedStat = INT_TO_ENUM(STATS_PACKED,(ENUM_TO_INT(PACKED_INT_GETAWAY_VEH0_MOD) + iOffset))
//			PRINTLN("Getaway Veh Upgrade slot ",iOffset,"  stat value = ",packedStat , ", ", GET_INT_PACKED_STAT_KEY(packedStat), ", Bitshift ", GET_INT_PACKED_STAT_BITSHIFT(packedStat))
//			packedStat = INT_TO_ENUM(STATS_PACKED,(ENUM_TO_INT(PACKED_INT_GETAWAY_VEH0_COLOUR) + iOffset))
//			PRINTLN("Getaway Veh Colour slot ",iOffset,"  stat value = ",packedStat , ", ", GET_INT_PACKED_STAT_KEY(packedStat), ", Bitshift ", GET_INT_PACKED_STAT_BITSHIFT(packedStat))
//		ENDREPEAT

//		FOR iOffset = DISPLAY_SLOT_START_AUTO_SHOP TO MAX_MP_SAVED_VEHICLES-1
//			PRINTLN("DISPLAY_SLOT ",iOffset,"  stat value = ", MPSV_GET_DISPLAY_SLOT_STAT_OFFSET(iOffset), ", ", GET_INT_PACKED_STAT_KEY(MPSV_GET_DISPLAY_SLOT_STAT_OFFSET(iOffset)), ", Bitshift ", GET_INT_PACKED_STAT_BITSHIFT(MPSV_GET_DISPLAY_SLOT_STAT_OFFSET(iOffset)))
//			PRINTLN("DISPLAY_SLOT: part 2 ",iOffset,"  stat value = ", MPSV_GET_DISPLAY_SLOT_STAT_OFFSET_PART_2(iOffset), ", ", GET_INT_PACKED_STAT_KEY(MPSV_GET_DISPLAY_SLOT_STAT_OFFSET_PART_2(iOffset)), ", Bitshift ", GET_INT_PACKED_STAT_BITSHIFT(MPSV_GET_DISPLAY_SLOT_STAT_OFFSET_PART_2(iOffset)))
//		ENDFOR
//		
//		REPEAT MAX_MP_SAVED_VEHICLES u
//			iOffset = u
//			PRINTLN("All vehicles: DISPLAY_SLOT: part 2 ",iOffset,"  stat value = ", MPSV_GET_DISPLAY_SLOT_STAT_OFFSET_PART_2(iOffset), ", ", GET_INT_PACKED_STAT_KEY(MPSV_GET_DISPLAY_SLOT_STAT_OFFSET_PART_2(iOffset)), ", Bitshift ", GET_INT_PACKED_STAT_BITSHIFT(MPSV_GET_DISPLAY_SLOT_STAT_OFFSET_PART_2(iOffset)))
//		ENDREPEAT
		
		IF g_bForcePlayersHelmetOn
		AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		AND NOT GET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet)
			SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_DontTakeOffHelmet, TRUE)
			CPRINTLN(DEBUG_PED_COMP, "PROCESS_HELMET_VISOR - forcing helmet to stay on for debug widget")
		ENDIF
		
		WAIT(0)
		
	ENDWHILE
	
ENDSCRIPT
