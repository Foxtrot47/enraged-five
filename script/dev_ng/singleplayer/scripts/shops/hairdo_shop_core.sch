//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	hairdo_shop.sc												//
//		AUTHOR			:	Kenneth Ross												//
//		DESCRIPTION		:	A hairdo shop that gives the player the chance to change 	//
//							their current hair style.									//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "brains.sch"
USING "script_player.sch"
USING "commands_script.sch"
USING "commands_task.sch"
USING "commands_pad.sch"
USING "commands_graphics.sch"
USING "commands_camera.sch"
USING "commands_interiors.sch"
USING "hairdo_shop_private.sch"
USING "cellphone_public.sch"
USING "finance_control_public.sch"
USING "net_scoring_common.sch"
USING "LineActivation.sch"
USING "flow_public_core_override.sch"
USING "menu_public.sch"
USING "screens_header.sch"
USING "script_ped.sch"
USING "rc_helper_functions.sch"
USING "tattoo_shop_private.sch"
USING "cheat_handler.sch"
USING "achievement_public.sch"
USING "store_trigger.sch"

// ****************
// BROADCAST DATA.
// ****************

// The server broadcast data.
// Everyone can read this data, only the server can update it. 
STRUCT ServerBroadcastData

	NETWORK_INDEX shopkeeperID
	NETWORK_INDEX scissorsID
	
	INT iPlayersAssignedToShopBitset
	BOOL bShopKeeperSetupDone
	INT iPlayerAssignedToShop[NUMBER_OF_SHOPS]
ENDSTRUCT
ServerBroadcastData serverBD


// The client broadcast data.
// Everyone can read this data, only the local player only the server can update it. 
STRUCT PlayerBroadcastData
	INT iShopProperties
	INT iShopRquestedByPlayer
	BOOL bPlayerRequestedShop
ENDSTRUCT
PlayerBroadcastData playerBD[NUM_NETWORK_PLAYERS]

COMMERCE_STORE_TRIGGER_DATA sStoreTriggerData

CONTROL_ACTION	caZoomInput	= INPUT_FRONTEND_LT // Input used for zooming - different on PC.
BOOL			bZoomToggle	= FALSE

#IF USE_TU_CHANGES 
BOOL bDisplayingCash
#ENDIF

INT iMouseSliderSoundID = GET_SOUND_ID() 
FLOAT fMouseSliderPrevValue = -1
BOOL bMouseRolloverActive = FALSE

TWEAK_FLOAT mpng_centreX_01		0.113	//CUSTOM_MENU_TEXT_INDENT_X
TWEAK_FLOAT mpng_centreY_01		0.255	//CUSTOM_MENU_SPACER_H
TWEAK_FLOAT mpng_height_01		0.5972 	//CUSTOM_MENU_TEXT_SCALE_Y

TWEAK_FLOAT mpng_centreY_02		0.255	//0.363	//mpng_centreY_01

TWEAK_FLOAT fAspectOffset_16x10	0.013
TWEAK_FLOAT fAspectOffset_16x9		0.000
TWEAK_FLOAT fAspectOffset_4x3		0.038

// ------------------- functions ------------------------------------------------------

/// PURPOSE:
///    Checks if the hair colour highlight scaleform is active
/// RETURNS:
///    
FUNC BOOL IS_HIGHLIGHTS_UI_ACTIVE( HAIRDO_SHOP_STRUCT &sData )

	IF sData.bToggleChangedItem 
	AND sData.eCurrentMenu = HME_NGMP_HAIR  

		RETURN TRUE
	
	ENDIF

	RETURN FALSE

ENDFUNC


FUNC BOOL IS_COLOUR_UI_ACTIVE(HAIRDO_SHOP_STRUCT &sData, MODEL_NAMES ePedModel)
	
	IF NOT (sData.eCurrentMenu = HME_NGMP_FACEPAINT OR sData.eCurrentMenu = HME_NGMP_MAKEUP_EYE)
	
		IF CAN_EDIT_NGMP_TINTS_OF_CURRENT_ITEM(sData, ePedModel)
			RETURN TRUE
		ENDIF
	
	ENDIF
	
	RETURN FALSE

ENDFUNC


FUNC BOOL IS_OPACITY_SLIDER_ACTIVE(HAIRDO_SHOP_STRUCT &sData, MODEL_NAMES ePedModel)

	IF sData.eCurrentMenu = HME_NGMP_MAKEUP_BLUSHER
	OR sData.eCurrentMenu = HME_NGMP_MAKEUP_EYE
	OR sData.eCurrentMenu = HME_NGMP_MAKEUP_LIPSTICK
	OR sData.eCurrentMenu = HME_NGMP_BEARD
	OR sData.eCurrentMenu = HME_NGMP_EYEBROWS
	OR sData.eCurrentMenu = HME_NGMP_CHEST
	OR sData.eCurrentMenu = HME_NGMP_FACEPAINT

		IF CAN_EDIT_NGMP_TINTS_OF_CURRENT_ITEM(sData, ePedModel)
			RETURN TRUE
		ENDIF
		
	ENDIF

	RETURN FALSE
		
ENDFUNC

/// PURPOSE:
///    Sets the peds head overlay. with optional param to use in SP. (as most are MP only)
/// PARAMS:
///    mPed - the ped we are setting the overlay for
///    iSlot - the head overlay slot
///    iTexture - the texutre to use
///    fBlend - the blend to use
///    bMultiplayerOnly - if TRUE we ignore this call in SP
PROC SAFE_SET_PED_HEAD_OVERLAY(PED_INDEX mPed, HEAD_OVERLAY_SLOT eHeadSlot, INT iTexture, INT tint1, INT tint2, RAMP_TYPE rampType, FLOAT fBlend, BOOL bMultiplayerOnly = TRUE)
	IF bMultiplayerOnly = TRUE
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			EXIT
		ENDIF
	ENDIF
	
	#IF IS_DEBUG_BUILD
	TEXT_LABEL t_mPed, t_eHeadSlot, t_iTexture
	IF (mPed = PLAYER_PED_ID())
		t_mPed = "PLAYER_PED_ID()"
	ELIF (mPed = g_pShopClonePed)
		t_mPed = "g_pShopClonePed"
	ELSE
		t_mPed  = "ped:"
		t_mPed += NATIVE_TO_INT(mPed)
	ENDIF
	SWITCH eHeadSlot
		CASE HOS_BLEMISHES		t_eHeadSlot = "BLEMISHES" BREAK
		CASE HOS_FACIAL_HAIR	t_eHeadSlot = "FACIAL_HAIR" BREAK
		CASE HOS_EYEBROW		t_eHeadSlot = "EYEBROW" BREAK
		CASE HOS_AGING			t_eHeadSlot = "AGING" BREAK
		CASE HOS_MAKEUP			t_eHeadSlot = "MAKEUP" BREAK
		CASE HOS_BLUSHER		t_eHeadSlot = "BLUSHER" BREAK
		CASE HOS_DAMAGE			t_eHeadSlot = "DAMAGE" BREAK
		CASE HOS_BASE_DETAIL	t_eHeadSlot = "BASE_DETAIL" BREAK
		CASE HOS_SKIN_DETAIL_1	t_eHeadSlot = "SKIN_DETAIL_1" BREAK
		CASE HOS_SKIN_DETAIL_2	t_eHeadSlot = "SKIN_DETAIL_2" BREAK
		CASE HOS_BODY_1			t_eHeadSlot = "BODY_1" BREAK
		CASE HOS_BODY_2			t_eHeadSlot = "BODY_2" BREAK
		CASE HOS_BODY_3			t_eHeadSlot = "BODY_3" BREAK
		
		DEFAULT
			t_eHeadSlot  = "UNKNOWN_"
			t_eHeadSlot += ENUM_TO_INT(eHeadSlot)
		BREAK
	ENDSWITCH
	t_iTexture = iTexture
	#ENDIF
	
	IF (eHeadSlot = HOS_BASE_DETAIL)
		CPRINTLN(DEBUG_SHOPS, "SAFE_SET_HEAD_BLEND_EYE_COLOR(", t_mPed, ", ", t_iTexture, ")	//max: ", GET_NUM_EYE_COLORS())
		SET_HEAD_BLEND_EYE_COLOR(mPed, iTexture)
	ELSE
		#IF IS_DEBUG_BUILD
		TEXT_LABEL t_iTint1 = tint1
		TEXT_LABEL t_fBlend = GET_STRING_FROM_FLOAT(fBlend)
		#ENDIF
		
		IF (eHeadSlot = HOS_BLUSHER)
		AND (rampType != RT_MAKEUP)
			CPRINTLN(DEBUG_SHOPS, "    SAFE_SET_PED_HEAD_OVERLAY - override invalid blusher ramp to RT_MAKEUP")
			rampType = RT_MAKEUP
		ELIF (eHeadSlot = HOS_MAKEUP)
		AND (rampType != RT_NONE)
			CPRINTLN(DEBUG_SHOPS, "    SAFE_SET_PED_HEAD_OVERLAY - override invalid makeup ramp to RT_NONE")
			rampType = RT_NONE
		ENDIF
		
		CHECK_RAMP_IS_VALID(eHeadSlot, rampType)
		IF (rampType = RT_HAIR)
			CPRINTLN(DEBUG_SHOPS, "SAFE_SET_PED_HEAD_OVERLAY(", t_mPed, ", HOS_", t_eHeadSlot, ", texture: ", t_iTexture, ", tint1: ", t_iTint1, ", blend: ", t_fBlend, ") //max: ", GET_NUM_PED_HAIR_TINTS())
		ELSE
			CPRINTLN(DEBUG_SHOPS, "SAFE_SET_PED_HEAD_OVERLAY(", t_mPed, ", HOS_", t_eHeadSlot, ", texture: ", t_iTexture, ", tint1: ", t_iTint1, ", blend: ", t_fBlend, ") //max: ", GET_NUM_PED_MAKEUP_TINTS())
		ENDIF
		
		SET_PED_HEAD_OVERLAY(mPed, eHeadSlot, iTexture, fBlend)
		IF (rampType != RT_NONE)
			IF (tint1 = -1)
				tint1 = GET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY(eHeadSlot, rampType)
				
				IF (eHeadSlot = HOS_BLUSHER)
				AND (iTexture != -1)
				AND (tint1 = -1)
					CPRINTLN(DEBUG_SHOPS, "    SAFE_SET_PED_HEAD_OVERLAY - override invalid blusher tint to zero")
					tint1 = 0
				ENDIF
			ENDIF
			IF (tint2 = -1)
				tint2 = GET_MP_CHARACTER_SECONDARY_COLOUR_FROM_HEAD_OVERLAY(eHeadSlot)
			ENDIF
			
			CPRINTLN(DEBUG_SHOPS, "    SAFE_SET_PED_HEAD_OVERLAY_TINT(", GET_RAMP_TYPE_TEXT(rampType), ", tint1: ", tint1, ", tint2: ", tint2, ")")
			SET_PED_HEAD_OVERLAY_TINT(mPed, eHeadSlot, rampType, tint1, tint2)
		ENDIF
		
		IF (eHeadSlot = HOS_MAKEUP)
			CPRINTLN(DEBUG_SHOPS, "    SAFE_SET_PED_HEAD_OVERLAY(", t_mPed, ", HOS_BLUSHER, -1) //reset for cleared makeup")
			SET_PED_HEAD_OVERLAY(mPed, HOS_BLUSHER, -1, -1)
		ELIF (eHeadSlot = HOS_BLUSHER)
			CPRINTLN(DEBUG_SHOPS, "    SAFE_SET_PED_HEAD_OVERLAY(", t_mPed, ", HOS_MAKEUP, -1) //reset for cleared blusher")
			SET_PED_HEAD_OVERLAY(mPed, HOS_MAKEUP, -1, -1)
		ENDIF
	ENDIF
ENDPROC

FUNC INT GET_MENU_OVERLAY_INDEX(HAIRDO_SHOP_STRUCT &sData)
	IF sData.eCurrentMenu= HME_BEARD
	OR sData.eCurrentMenu= HME_MAKEUP
		RETURN sData.sBrowseInfo.iCurrentItem-2
	ELSE
		RETURN sData.sBrowseInfo.iCurrentItem-1
	ENDIF
ENDFUNC

// -------------------debug functions ------------------------------------------------------

#IF IS_DEBUG_BUILD
	BOOL bKillScript
	BOOL bResetScene
	BOOL bResetIntro
	INT iForcedMakeup = -2
	VECTOR vCamPos = <<139.4213, -1710.1654, 29.6279>>
	VECTOR vCamRotDEBUG = <<7.7786, 0.0099, 50.3051>>
	VECTOR vCamLookAt = <<138.4104, -1709.3263, 29.8074>>
	
	BOOL bUpdateCam = FALSE
	
	INT iFakeChestHairStat = -1
	
	BOOL bGenerateFreeTransitionHair
	
	BOOL bTestShopInteriorOrientation
	
	PROC SETUP_HAIRDO_DEBUG_WIDGETS(HAIRDO_SHOP_STRUCT &sData)
		
		IF GET_DRAW_DEBUG_COMMANDLINE_PARAM_EXISTS()
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
			sData.bDrawDebugStuff = TRUE
		ENDIF
		
		START_WIDGET_GROUP("Shop Debug - Hairdo")
			ADD_WIDGET_BOOL("Draw Debug Stuff", sData.bDrawDebugStuff)
			
			ADD_WIDGET_BOOL("Kill script", bKillScript)
			ADD_WIDGET_BOOL("PTFX loaded", sData.bLoadPTFX)
			ADD_WIDGET_BOOL("Reset intro", bResetIntro)
			ADD_WIDGET_BOOL("reset cutscene", bResetScene)
			ADD_WIDGET_FLOAT_SLIDER("Hairshop Multiplier", g_sMPTunables.fHairDoShopMultiplier, -1000.0, 1000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Valentines Multiplier", g_sMPTunables.fValentine_Haircuts_Multiplier, -1000.0, 1000.0, 0.1)
			ADD_WIDGET_FLOAT_SLIDER("Haircuts Group Modifier", g_sMPTunableGroups.fhaircuts_group_modifier, -1000.0, 1000.0, 0.1)
			
			ADD_WIDGET_INT_SLIDER("Force make-up", iForcedMakeup, -2, 20, 1)
			
			ADD_WIDGET_FLOAT_SLIDER("vCamLookAt.x", vCamLookAt.x, -1000, 1000, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("vCamLookAt.y", vCamLookAt.y, -2000, 2000, 0.05)
			ADD_WIDGET_FLOAT_SLIDER("vCamLookAt.z", vCamLookAt.z, 0, 40, 0.05)
			ADD_WIDGET_BOOL("bUpdateCam", bUpdateCam)
			
			ADD_WIDGET_BOOL("bAllowZoomControl", sData.bAllowZoomControl)
			
			START_WIDGET_GROUP("Beards")
				ADD_WIDGET_INT_READ_ONLY("Current", sData.iCurrentBeard)
				ADD_WIDGET_INT_READ_ONLY("Original", sData.iOriginalBeard)
			STOP_WIDGET_GROUP()
			
			START_WIDGET_GROUP("Makeup")
				ADD_WIDGET_INT_READ_ONLY("Current", sData.iPlayerMakeup)
				ADD_WIDGET_INT_READ_ONLY("Original", sData.iOriginalMakeup)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_STRING("colourTint_movieID_01")
			ADD_WIDGET_FLOAT_SLIDER("mpng_centreX_01", mpng_centreX_01, -1.0, 1.0, 0.001)
			IF IS_PC_VERSION()
				IF ABSF(GET_ASPECT_RATIO(FALSE) - AR16_10) <0.1 //16:10, most UI errors
					ADD_WIDGET_FLOAT_SLIDER("fAspectOffset_16x10", fAspectOffset_16x10, -0.1, 0.1, 0.001)
				ELIF ABSF(GET_ASPECT_RATIO(FALSE) - AR16_9) <0.1
					ADD_WIDGET_FLOAT_SLIDER("fAspectOffset_16x9", fAspectOffset_16x9, -0.1, 0.1, 0.001)
				ELIF ABSF(GET_ASPECT_RATIO(FALSE) - AR4_3) <0.1
					ADD_WIDGET_FLOAT_SLIDER("fAspectOffset_4x3", fAspectOffset_4x3, -0.1, 0.1, 0.001)
	 			ELSE
					ADD_WIDGET_FLOAT_SLIDER("fAspectOffset_16x10", fAspectOffset_16x10, -0.1, 0.1, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("fAspectOffset_16x9", fAspectOffset_16x9, -0.1, 0.1, 0.001)
					ADD_WIDGET_FLOAT_SLIDER("fAspectOffset_4x3", fAspectOffset_4x3, -0.1, 0.1, 0.001)
	 			ENDIF
			ENDIF
			ADD_WIDGET_FLOAT_SLIDER("mpng_centreY_01", mpng_centreY_01, -1.0, 1.0, 0.01)
			ADD_WIDGET_FLOAT_READ_ONLY("CUSTOM_MENU_W", CUSTOM_MENU_W)
			ADD_WIDGET_FLOAT_SLIDER("mpng_height_01", mpng_height_01, -1.0, 1.0, 0.01)
			ADD_WIDGET_STRING("colourTint_movieID_01")
			ADD_WIDGET_FLOAT_READ_ONLY("mpng_centreX_01", mpng_centreX_01)
			ADD_WIDGET_FLOAT_SLIDER("mpng_centreY_02", mpng_centreY_02, -1.0, 1.0, 0.001)
			ADD_WIDGET_FLOAT_READ_ONLY("CUSTOM_MENU_W", CUSTOM_MENU_W)
			ADD_WIDGET_FLOAT_READ_ONLY("mpng_height_01", mpng_height_01)
			
			ADD_WIDGET_INT_SLIDER("iFakeChestHairStat", iFakeChestHairStat, -1, GET_NUMBER_OF_NGMP_MENU_OPTIONS(GET_ENTITY_MODEL(PLAYER_PED_ID()), HME_NGMP_CHEST), 1)
			
			IF USE_SERVER_TRANSACTIONS()
				ADD_WIDGET_BOOL("bGenerateFreeTransitionHair", bGenerateFreeTransitionHair)
			ENDIF
			
			IF (sData.eInShop = HAIRDO_SHOP_CASINO_APT)
				START_WIDGET_GROUP("Casino Apt")
					ADD_WIDGET_BOOL("Test Shop Interior Orientation", bTestShopInteriorOrientation)
					
					ADD_WIDGET_VECTOR_SLIDER("g_vHairdoShopCasinoAptIntCoord", g_vHairdoShopCasinoAptIntCoord, -1000, 1000, 0.001)
					ADD_WIDGET_VECTOR_SLIDER("g_vHairdoShopCasinoAptIntRot", g_vHairdoShopCasinoAptIntRot, -360, 360, 1.0)
				STOP_WIDGET_GROUP()
			ENDIF
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC MAINTAIN_HAIRDO_DEBUG_WIDGETS(HAIRDO_SHOP_STRUCT &sData)
		IF bKillScript
			FORCE_SHOP_CLEANUP(sData.eInShop)
		ENDIF
		
		IF bResetScene
			bResetScene = FALSE
			SET_SHOP_HAS_RUN_ENTRY_INTRO(sData.sShopInfo.eShop, FALSE)
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<-150.4498, -1529.4912, 33.3215>>)
				ENDIF
			ENDIF
			FORCE_SHOP_CLEANUP(sData.eInShop)
		ENDIF
		
		IF bResetIntro
			SET_SHOP_HAS_RUN_ENTRY_INTRO(sData.sShopInfo.eShop, FALSE)
			bResetIntro = FALSE
		ENDIF
		
		IF iForcedMakeup > -2
			IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SAFE_SET_PED_HEAD_OVERLAY(PLAYER_PED_ID(), HOS_MAKEUP, iForcedMakeup,sData.iTint1, 0, RT_NONE, 1.0)
					CPRINTLN(DEBUG_SHOPS, "Forced make up to be ", iForcedMakeup)
				ENDIF
			ENDIF
			iForcedMakeup = -2
		ENDIF
		
		IF bUpdateCam = TRUE
			SET_CAM_COORD(sData.sShopInfo.sCameras[1].camID, vCamPos)
			SET_CAM_ROT(sData.sShopInfo.sCameras[1].camID, vCamRotDEBUG)
			POINT_CAM_AT_COORD(sData.sShopInfo.sCameras[1].camID, vCamLookAt)
			CPRINTLN(DEBUG_SHOPS, "Hairdo Camera: updated camera")	
			bUpdateCam = FALSE
		ENDIF
		
		IF iFakeChestHairStat > -1
			SET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_BODY_1), iFakeChestHairStat)
			iFakeChestHairStat = -1
		ENDIF
		
		IF bGenerateFreeTransitionHair
			g_bAddMenuItemsToCatalogue = TRUE
		//	g_bVerifyMenuItemPrices = TRUE
			
			INT iOption = 1
			NGMP_MENU_OPTION_DATA sOptionData
			ScriptCatalogItem sCatalogueData
			
			WHILE GET_NGMP_MENU_OPTION_DATA(MP_M_FREEMODE_01, HME_NGMP_HAIR, iOption, sOptionData)
				GENERATE_HAIRDO_KEY_FOR_CATALOGUE(sCatalogueData.m_key, HME_NGMP_HAIR, sOptionData.tlLabel, MP_M_FREEMODE_01, 2)
				sCatalogueData.m_textTag = sOptionData.tlLabel
				sCatalogueData.m_name = GET_STRING_FROM_TEXT_FILE(sCatalogueData.m_textTag)
				sCatalogueData.m_category = GET_CATALOGUE_CATEGORY_FOR_HAIRDO_MENU(HME_NGMP_HAIR, INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1))
				sCatalogueData.m_price = sOptionData.iCost
				sCatalogueData.m_storagetype = NET_SHOP_ISTORAGE_INT
				sCatalogueData.m_statvalue = sOptionData.iTexture
				sCatalogueData.m_bitsize = 0
				sCatalogueData.m_stathash = ENUM_TO_INT(MPIntStatNamesArray[MP_STAT_MPSV_VEHICLE_BS_0][GET_SLOT_NUMBER(g_iPedComponentSlot)]) // Dummy
				ADD_ITEM_TO_CATALOG_WITH_CHECK(sCatalogueData)
				
				GENERATE_HAIRDO_KEY_FOR_CATALOGUE(sCatalogueData.m_key, HME_NGMP_HAIR, sOptionData.tlLabel, MP_M_FREEMODE_01, 1)
				sCatalogueData.m_textTag = sOptionData.tlLabel
				sCatalogueData.m_name = GET_STRING_FROM_TEXT_FILE(sCatalogueData.m_textTag)
				sCatalogueData.m_category = GET_CATALOGUE_CATEGORY_FOR_HAIRDO_MENU(HME_NGMP_HAIR, INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1))
				sCatalogueData.m_price = 0		//paramCost
				sCatalogueData.m_storagetype = NET_SHOP_ISTORAGE_INT
				sCatalogueData.m_statvalue = sOptionData.iTexture
				sCatalogueData.m_bitsize = 0
				sCatalogueData.m_stathash = ENUM_TO_INT(MPIntStatNamesArray[MP_STAT_MPSV_VEHICLE_BS_0][GET_SLOT_NUMBER(g_iPedComponentSlot)]) // Dummy
				ADD_ITEM_TO_CATALOG_WITH_CHECK(sCatalogueData)
				
				iOption++
			ENDWHILE
			
			iOption = 1
			WHILE GET_NGMP_MENU_OPTION_DATA(MP_F_FREEMODE_01, HME_NGMP_HAIR, iOption, sOptionData)
				GENERATE_HAIRDO_KEY_FOR_CATALOGUE(sCatalogueData.m_key, HME_NGMP_HAIR, sOptionData.tlLabel, MP_F_FREEMODE_01, 2)
				sCatalogueData.m_textTag = sOptionData.tlLabel
				sCatalogueData.m_name = GET_STRING_FROM_TEXT_FILE(sCatalogueData.m_textTag)
				sCatalogueData.m_category = GET_CATALOGUE_CATEGORY_FOR_HAIRDO_MENU(HME_NGMP_HAIR, INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1))
				sCatalogueData.m_price = sOptionData.iCost
				sCatalogueData.m_storagetype = NET_SHOP_ISTORAGE_INT
				sCatalogueData.m_statvalue = sOptionData.iTexture
				sCatalogueData.m_bitsize = 0
				sCatalogueData.m_stathash = ENUM_TO_INT(MPIntStatNamesArray[MP_STAT_MPSV_VEHICLE_BS_0][GET_SLOT_NUMBER(g_iPedComponentSlot)]) // Dummy
				ADD_ITEM_TO_CATALOG_WITH_CHECK(sCatalogueData)
				
				GENERATE_HAIRDO_KEY_FOR_CATALOGUE(sCatalogueData.m_key, HME_NGMP_HAIR, sOptionData.tlLabel, MP_F_FREEMODE_01, 1)
				sCatalogueData.m_textTag = sOptionData.tlLabel
				sCatalogueData.m_name = GET_STRING_FROM_TEXT_FILE(sCatalogueData.m_textTag)
				sCatalogueData.m_category = GET_CATALOGUE_CATEGORY_FOR_HAIRDO_MENU(HME_NGMP_HAIR, INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1))
				sCatalogueData.m_price = 0		//paramCost
				sCatalogueData.m_storagetype = NET_SHOP_ISTORAGE_INT
				sCatalogueData.m_statvalue = sOptionData.iTexture
				sCatalogueData.m_bitsize = 0
				sCatalogueData.m_stathash = ENUM_TO_INT(MPIntStatNamesArray[MP_STAT_MPSV_VEHICLE_BS_0][GET_SLOT_NUMBER(g_iPedComponentSlot)]) // Dummy
				ADD_ITEM_TO_CATALOG_WITH_CHECK(sCatalogueData)
				
				iOption++
			ENDWHILE
			
			bGenerateFreeTransitionHair = FALSE
		ENDIF
		
		IF bTestShopInteriorOrientation
			VECTOR vAnimPos, vAnimRot
			GET_HAIRDO_SHOP_DATA(sData.sShopInfo, sData.sShopReceptionist, sData.eInShop)
			GET_HAIRDO_SHOP_ANIM_POSITION(sData.sShopInfo.eShop, vAnimPos, vAnimRot)
			GET_HAIRDO_SHOP_PLAYER_GO_TO_POS(sData.sShopInfo.eShop, sData.sLocateInfo.vPlayerGoToPos)
			GET_HAIRDO_SHOP_LOCATE_DATA(sData.fpSetupClothingItemForShop, sData.sShopInfo.eShop, sData.sLocateInfo, g_sShopCompItemData, sData.iHairdoGroupsAvailable, sData.iHairdoGroupsWithNewItems)
			GET_HAIRDO_SHOP_ENTRY_INTRO_DATA(sData.sShopInfo.eShop, sData.sEntryInfo)
			
			VECTOR vAnimHeadingOffset
			vAnimHeadingOffset = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(
					vAnimPos,
					vAnimRot.z,
					<<0.0,1.0,0.0>>)
			DRAW_DEBUG_SPHERE(vAnimPos, 0.1,
					255, 000, 000, 127)
			DRAW_DEBUG_LINE_WITH_TWO_COLOURS(vAnimPos, vAnimHeadingOffset,
					000, 000, 255, 255,
					000, 255, 000, 255)
			
			TEXT_LABEL_63 tl63 = ""
			sData.iSyncScenePlayer = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sData.iNETSyncScenePlayer)
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncScenePlayer)
				tl63  = "phase "
				tl63 += (sData.iSyncScenePlayer)
				tl63 += ":"
				tl63 += "NULL"
				DRAW_DEBUG_TEXT_WITH_OFFSET(tl63, vAnimPos, 000, 000, 255, 000, 000, 255)
			ELSE
				tl63  = "phase "
				tl63 += (sData.iSyncScenePlayer)
				tl63 += ":"
				tl63 += GET_STRING_FROM_FLOAT(GET_SYNCHRONIZED_SCENE_PHASE(sData.iSyncScenePlayer))
				DRAW_DEBUG_TEXT_WITH_OFFSET(tl63, vAnimPos, 000, 000, 255, 000, 000, 255)
				
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				SET_ENTITY_COLLISION(PLAYER_PED_ID(), FALSE)
				SET_SYNCHRONIZED_SCENE_ORIGIN(sData.iSyncScenePlayer, vAnimPos, vAnimRot)
			ENDIF

			
			
		ENDIF
	ENDPROC
#ENDIF

PROC UPDATE_SHOP_KEEPER_PED_ID(SHOP_KEEPER_STRUCT &sShopKeeper)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.shopkeeperID)
			sShopKeeper.pedID = NET_TO_PED(serverBD.shopkeeperID)
			sShopKeeper.bSetup = TRUE
		ELSE
			sShopKeeper.pedID = NULL
		ENDIF
		sShopKeeper.bNetKeeperSetup = serverBD.bShopKeeperSetupDone
	ENDIF
ENDPROC

FUNC BOOL DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(BOOL bRequestUse)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.shopkeeperID)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.shopkeeperID)
				RETURN TRUE
			ELSE
				IF bRequestUse
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.shopkeeperID)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL DO_WE_HAVE_CONTROL_OF_SCISSORS(BOOL bRequestUse)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.scissorsID)
			IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.scissorsID)
				RETURN TRUE
			ELSE
				IF bRequestUse
					NETWORK_REQUEST_CONTROL_OF_NETWORK_ID(serverBD.scissorsID)
				ENDIF
			ENDIF
		ENDIF
	ELSE
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC MANAGE_NET_SCISSORS(HAIRDO_SHOP_STRUCT &sData)
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND NETWORK_IS_HOST_OF_THIS_SCRIPT()
	AND sData.sShopInfo.eStage > SHOP_STAGE_INITIALISE
	AND sData.sBrowseInfo.eStage < SHOP_BROWSE_STAGE_OUTRO
		IF NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.scissorsID)
			REQUEST_MODEL(P_CS_SCISSORS_S)
			IF HAS_MODEL_LOADED(P_CS_SCISSORS_S)
				IF CAN_RESERVE_NETWORK_OBJECTS_FOR_THIS_SCRIPT(1, FALSE, TRUE)
					RESERVE_NETWORK_MISSION_OBJECTS(1)
					IF CREATE_NET_OBJ(serverBD.scissorsID, P_CS_SCISSORS_S, sData.sShopInfo.sShopKeeper.vPosIdle, TRUE, TRUE)
						CPRINTLN(DEBUG_SHOPS, "Created scissors")
						SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.scissorsID), FALSE)
						SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.scissorsID), FALSE)
						IF sData.eInShop = HAIRDO_SHOP_CASINO_APT
							NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(NET_TO_ENT(serverBD.scissorsID), TRUE)
						ENDIF
						FREEZE_ENTITY_POSITION(NET_TO_ENT(serverBD.scissorsID), TRUE)
						SET_MODEL_AS_NO_LONGER_NEEDED(P_CS_SCISSORS_S)
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC HIDE_NET_SCISSORS()
	IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.scissorsID)
	AND DO_WE_HAVE_CONTROL_OF_SCISSORS(TRUE)
		SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.scissorsID), FALSE)
		SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.scissorsID), FALSE)
		FREEZE_ENTITY_POSITION(NET_TO_ENT(serverBD.scissorsID), TRUE)
	ENDIF
ENDPROC

// ---------------------------functions -----------------------------------------------

/// PURPOSE:
///    Gets the name of the camera anim needed.
/// PARAMS:
///    sData - hairdo shop data struct
///    eCamera - the type of camera shot we are doing
///    sShopKeeperCutAnim - name of the shopkeeper's haircut anim, so we can pick the matching camera anim
/// RETURNS:
///    STRING name of the camera anim needed
FUNC STRING GET_CAMERA_ANIM(HAIRDO_SHOP_STRUCT &sData, HAIRDO_SHOP_CAMERAS eCamera, STRING sShopKeeperCutAnim)

	STRING sCameraAnim
	
	IF IS_SALON_ACTIVE(sData)
	
		// Salon cameras
		SWITCH  eCamera
			CASE HSC_ENTER_CHAIR
				sCameraAnim = "cam_enterchair"
			BREAK
			CASE HSC_EXIT_CHAIR
				sCameraAnim = "cam_exitchair"
			BREAK
			CASE HSC_TUTORIAL
				sCameraAnim = "cam_intro_-_customer_-_keeper_-_player_-_assistant"
			BREAK
			CASE HSC_CUT
				IF ARE_STRINGS_EQUAL(sShopKeeperCutAnim, "keeper_hair_cut_a")
					sCameraAnim = "cam_hair_cut_a"
				ELSE
					sCameraAnim = "cam_hair_cut_b"
				ENDIF
			BREAK
		ENDSWITCH
	ELSE
		// barber shop cameras
		SWITCH  eCamera
			CASE HSC_ENTER_CHAIR
				sCameraAnim = "enterchair_cam"
			BREAK
			CASE HSC_EXIT_CHAIR
				sCameraAnim = "exitchair_cam"
			BREAK
			CASE HSC_TUTORIAL
				sCameraAnim = "tutorial_cam"
			BREAK
			CASE HSC_CUT
				IF ARE_STRINGS_EQUAL(sShopKeeperCutAnim, "keeper_idle_a")
					sCameraAnim = "idle_a_cam"
				ELSE
					sCameraAnim = "idle_b_cam"
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
	RETURN sCameraAnim
ENDFUNC

/// PURPOSE:
///    Sets the current camera animation.
///    Creates camera if it doesn't exist, sets up new anim to play and activates the camera
PROC UPDATE_CAMERA_ANIM(HAIRDO_SHOP_STRUCT &sData, STRING sNewCameraAnim)
	VECTOR vAnimPos, vAnimRot
	GET_HAIRDO_SHOP_ANIM_POSITION(sData.sShopInfo.eShop, vAnimPos, vAnimRot)
	IF DOES_CAM_EXIST(sData.sShopInfo.sCameras[1].camID)
		DESTROY_CAM(sData.sShopInfo.sCameras[1].camID)
	ENDIF
	IF NOT DOES_CAM_EXIST(sData.sShopInfo.sCameras[0].camID)
		sData.sShopInfo.sCameras[0].camID = CREATE_CAM("DEFAULT_ANIMATED_CAMERA")
	ENDIF
	PLAY_CAM_ANIM(sData.sShopInfo.sCameras[0].camID, sNewCameraAnim, sData.sHairdoShopAnimDict, vAnimPos, vAnimRot)
	SET_CAM_ACTIVE(sData.sShopInfo.sCameras[0].camID, TRUE)
	RENDER_SCRIPT_CAMS(TRUE, FALSE)
	CPRINTLN(DEBUG_SHOPS, "Hairdo Camera: ", sNewCameraAnim)	
ENDPROC

/// PURPOSE:
///    Returns the phase of the synced scene passed in
/// RETURNS:
///    Phase of synced scene. returns 1.0 if it isn't running.
FUNC FLOAT GET_SYNC_SCENE_PHASE(INT &iSyncScene, INT &iNetSyncScnee)
	FLOAT fPhase = 0.0
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		iSyncScene = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(iNetSyncScnee)
		IF iSyncScene != -1
			IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScene)
				fPhase = GET_SYNCHRONIZED_SCENE_PHASE(iSyncScene)
			ELSE
				fPhase = 1.0
			ENDIF
		ELSE
			fPhase = 1.0
		ENDIF
	ELSE
		IF IS_SYNCHRONIZED_SCENE_RUNNING(iSyncScene)
			fPhase = GET_SYNCHRONIZED_SCENE_PHASE(iSyncScene)
		ELSE
			fPhase = 1.0
		ENDIF
	ENDIF
	
	RETURN fPhase
ENDFUNC

/// PURPOSE:
///    Creates or deletes the scissors
/// PARAMS:
///    bCreate - if true the scissors get created. If false they get deleted
PROC CREATE_SCISSORS(HAIRDO_SHOP_STRUCT &sData, BOOL bCreate)
	IF bCreate = TRUE
		// Create scissors for shop keeper
		IF NOT DOES_ENTITY_EXIST(sData.mScissors)
			sData.mScissors = CREATE_OBJECT(P_CS_SCISSORS_S, sData.sShopInfo.sShopKeeper.vPosIdle, FALSE, FALSE)
			CPRINTLN(DEBUG_SHOPS, "Created scissors")
		ENDIF
	ELSE
		// remove scissors
		IF DOES_ENTITY_EXIST(sData.mScissors)
			DELETE_OBJECT(sData.mScissors)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Picks a random idle anim for player to play whilst sat in the chair
/// RETURNS:
///    STRING anim name
FUNC STRING GET_RANDOM_IN_CHAIR_IDLE_ANIM(HAIRDO_SHOP_STRUCT &sData)
	SWITCH GET_RANDOM_INT_IN_RANGE(0, 4)
		CASE 0 RETURN  "player_idle_a"	BREAK
		CASE 1 RETURN "player_idle_b"	BREAK
		CASE 2 RETURN "player_idle_c"	BREAK
		DEFAULT 
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND IS_SALON_ACTIVE(sData)
				RETURN "player_idle_c"
			ELSE
				RETURN "player_idle_d"	
			ENDIF
		BREAK
	ENDSWITCH
	RETURN "player_idle_a"
ENDFUNC

/// PURPOSE:
///    Checks if the anim passed in is a haircut anim
/// RETURNS:
///    TRUE if it is a haricutting anim, FALSE otherwise
FUNC BOOL IS_HAIRCUT_ANIM(HAIRDO_SHOP_STRUCT &sData, STRING sAnim)

	IF NOT IS_STRING_NULL_OR_EMPTY(sAnim)
		IF IS_SALON_ACTIVE(sData)
			IF ARE_STRINGS_EQUAL(sAnim, "keeper_hair_cut_a")
			OR ARE_STRINGS_EQUAL(sAnim, "keeper_hair_cut_b")
				RETURN TRUE
			ENDIF
		ELSE
			IF ARE_STRINGS_EQUAL(sAnim, "keeper_idle_a")
			OR ARE_STRINGS_EQUAL(sAnim, "keeper_idle_b")
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Picks a random haircutting anim for shopkeeper to play
/// RETURNS:
///    STRING anim name
FUNC STRING GET_RANDOM_HAIRCUT_ANIM(HAIRDO_SHOP_STRUCT &sData)

	IF IS_SALON_ACTIVE(sData)
		IF GET_RANDOM_INT_IN_RANGE(0, 2) = 1
			RETURN "keeper_hair_cut_a"
		ELSE
			RETURN "keeper_hair_cut_b"
		ENDIF
	ELSE
		IF GET_RANDOM_INT_IN_RANGE(0, 2) = 1
			RETURN "keeper_idle_a"
		ELSE
			RETURN "keeper_idle_b"
		ENDIF
	ENDIF
	
	RETURN "keeper_idle_a"
ENDFUNC

FUNC BOOL IS_PLAYER_IN_SOFA_LOCATE(HAIRDO_SHOP_STRUCT &sData)
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR IS_SALON_ACTIVE(sData)		
	OR (sData.eInShop = HAIRDO_SHOP_CASINO_APT)
		RETURN FALSE
	ENDIF
	
	VECTOR vSofaLocate1
	VECTOR vSofaLocate2
	FLOAT fSofaLocateWidth
	
	GET_HAIRDO_SHOP_SOFA_LOCATE_DATA(sData.sShopInfo.eShop, vSofaLocate1, vSofaLocate2, fSofaLocateWidth)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())	
		IF IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), vSofaLocate1, vSofaLocate2, fSofaLocateWidth)
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Handles player sitting on the sofas in the barber shops (MP only)
PROC DO_SOFA(HAIRDO_SHOP_STRUCT &sData)
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR IS_SALON_ACTIVE(sData)	
		EXIT
	ENDIF
	
	VECTOR vSofaSeat1
	VECTOR vSofaSeat2
	FLOAT fSofaHeading
	
	GET_HAIRDO_SHOP_SOFA_ANIM_DATA(sData.sShopInfo.eShop, vSofaSeat1, vSofaSeat2, fSofaHeading)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())
		IF IS_PLAYER_IN_SOFA_LOCATE(sData)
			IF NOT IS_PED_ACTIVE_IN_SCENARIO(PLAYER_PED_ID())
				IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HAIR_SOFA_STAND")
				OR IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HAIR_FTRIG_BUSY")	
					CLEAR_HELP()
				ENDIF
			ENDIF
			IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_START_SCENARIO_AT_POSITION) <> PERFORMING_TASK	
				IF NOT IS_ANY_PED_NEAR_POINT(vSofaSeat1,0.1)
				OR NOT IS_ANY_PED_NEAR_POINT(vSofaSeat2,0.1)
					IF NOT IS_BIT_SET(MPGlobalsAmbience.iThiefBitSet,THIEF_MUGGED_PLAYER)
						IF sData.sShopInfo.iContextID = NEW_CONTEXT_INTENTION					
							STRING sHelpText = "HAIR_SOFA_SIT"						
							REGISTER_CONTEXT_INTENTION(sData.sShopInfo.iContextID, CP_MEDIUM_PRIORITY, sHelpText, FALSE)
						ENDIF
						SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CONTEXT)
						IF HAS_CONTEXT_BUTTON_TRIGGERED(sData.sShopInfo.iContextID)
							VECTOR vPlayer = GET_ENTITY_COORDS(PLAYER_PED_ID(),FALSE)
							IF VDIST(vPlayer,vSofaSeat1) < VDIST(vPlayer,vSofaSeat2)
								IF NOT IS_ANY_PED_NEAR_POINT(vSofaSeat1,0.1)
									TASK_START_SCENARIO_AT_POSITION(PLAYER_PED_ID(),"PROP_HUMAN_SEAT_CHAIR_MP_PLAYER",vSofaSeat1,fSofaHeading,0,TRUE,FALSE)
								ELSE
									TASK_START_SCENARIO_AT_POSITION(PLAYER_PED_ID(),"PROP_HUMAN_SEAT_CHAIR_MP_PLAYER",vSofaSeat2,fSofaHeading,0,TRUE,FALSE)
								ENDIF
							ELSE
								IF NOT IS_ANY_PED_NEAR_POINT(vSofaSeat2,0.1)
									TASK_START_SCENARIO_AT_POSITION(PLAYER_PED_ID(),"PROP_HUMAN_SEAT_CHAIR_MP_PLAYER",vSofaSeat2,fSofaHeading,0,TRUE,FALSE)
								ELSE
									TASK_START_SCENARIO_AT_POSITION(PLAYER_PED_ID(),"PROP_HUMAN_SEAT_CHAIR_MP_PLAYER",vSofaSeat1,fSofaHeading,0,TRUE,FALSE)
								ENDIF
							ENDIF
							IF sData.sShopInfo.iContextID <> NEW_CONTEXT_INTENTION		
								RELEASE_CONTEXT_INTENTION(sData.sShopInfo.iContextID)
							ENDIF
						ENDIF
					ELSE
						CPRINTLN(DEBUG_SHOPS, "Player has been mugged - prevent context \"HAIR_SOFA_SIT\"")
					ENDIF
				ENDIF
			ELSE
				IF IS_PED_ACTIVE_IN_SCENARIO(PLAYER_PED_ID())
					DISABLE_CELLPHONE_CAMERA_APP_THIS_FRAME_ONLY()
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
						PRINT_HELP_FOREVER("HAIR_SOFA_STAND")
					ENDIF
					IF NOT IS_CUSTOM_MENU_ON_SCREEN()
						IF IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_FRONTEND_CANCEL)
							CLEAR_PED_TASKS(PLAYER_PED_ID())
						ENDIF
					ENDIF
					IF IS_BIT_SET(MPGlobalsAmbience.iThiefBitSet,THIEF_MUGGED_PLAYER)
						CPRINTLN(DEBUG_SHOPS, "Player has been mugged - clear tasks from using sofa ")
						
						CLEAR_PED_TASKS(PLAYER_PED_ID())
						CLEAR_BIT(MPGlobalsAmbience.iThiefBitSet,THIEF_MUGGED_PLAYER)
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF IS_BIT_SET(MPGlobalsAmbience.iThiefBitSet,THIEF_MUGGED_PLAYER)
				CPRINTLN(DEBUG_SHOPS, "Player has been mugged - clear bit even if not using sofa ")
				CLEAR_BIT(MPGlobalsAmbience.iThiefBitSet,THIEF_MUGGED_PLAYER)
			ENDIF
		ENDIF
	ENDIF
	
ENDPROC

PROC CLEANUP_SOFA(HAIRDO_SHOP_STRUCT &sData)
	
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
	OR IS_SALON_ACTIVE(sData)	
		EXIT
	ENDIF

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
	AND NOT IS_PED_INJURED(PLAYER_PED_ID())	
		IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_START_SCENARIO_AT_POSITION) = PERFORMING_TASK	
		OR IS_PED_ACTIVE_IN_SCENARIO(PLAYER_PED_ID())	
			IF IS_THIS_HELP_MESSAGE_BEING_DISPLAYED("HAIR_SOFA_STAND")
				CLEAR_HELP()
			ENDIF
			CLEAR_PED_TASKS(PLAYER_PED_ID())
		ENDIF
	ENDIF
	
ENDPROC

FUNC STRING GET_SCISSOR_ANIM_FOR_KEEPER_ANIM(STRING sKeeperAnim)
	
	STRING sScissorsAnim
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sKeeperAnim)
	
		IF ARE_STRINGS_EQUAL(sKeeperAnim, "keeper_base") 
			sScissorsAnim = "scissors_base"
			
		ELIF ARE_STRINGS_EQUAL(sKeeperAnim, "keeper_enterchair") 
			sScissorsAnim = "scissors_enterchair"
			
		ELIF ARE_STRINGS_EQUAL(sKeeperAnim, "keeper_exitchair") 
			sScissorsAnim = "scissors_exitchair"
			
		ELIF ARE_STRINGS_EQUAL(sKeeperAnim, "keeper_idle_a") 
			sScissorsAnim = "scissors_idle_a"
			
		ELIF ARE_STRINGS_EQUAL(sKeeperAnim, "keeper_idle_b") 
			sScissorsAnim = "scissors_idle_b"
			
		ELIF ARE_STRINGS_EQUAL(sKeeperAnim, "keeper_idle_c") 
			sScissorsAnim = "scissors_idle_c"
			
		ELIF ARE_STRINGS_EQUAL(sKeeperAnim, "keeper_hair_cut_a") 
			sScissorsAnim = "scissors_hair_cut_a"
			
		ELIF ARE_STRINGS_EQUAL(sKeeperAnim, "keeper_hair_cut_b") 
			sScissorsAnim = "scissors_hair_cut_b"
			
		ELIF ARE_STRINGS_EQUAL(sKeeperAnim, "keeper_intro") 
			sScissorsAnim = "scissors_intro"
			
		ELIF ARE_STRINGS_EQUAL(sKeeperAnim, "keeper_tutorial") 
			sScissorsAnim = "scissors_tutorial"
			
		ELIF ARE_STRINGS_EQUAL(sKeeperAnim, "keeper_tutorial_base") 
			sScissorsAnim = "scissors_tutorial_base"
		ENDIF
	ENDIF
	
	RETURN sScissorsAnim
ENDFUNC

/// PURPOSE:
///    Returns the ped index of the specified shopworker (shopkeeper or receptionist)
/// PARAMS:
///    bReceptionist - if true returns the receptionist. if false- the shopkeeper.
/// RETURNS:
///    PED_INDEX of ped specified
FUNC PED_INDEX SHOPWORKER_GET_PED(HAIRDO_SHOP_STRUCT &sData, BOOL bReceptionist)
	IF bReceptionist = TRUE
		RETURN sData.sShopReceptionist.pedID
	ELSE
		// shopkeeper
		RETURN sData.sShopInfo.sShopKeeper.pedID
	ENDIF
ENDFUNC

/// PURPOSE:
///    Returns the idle pos of the specified shopworker (shopkeeper or receptionist)
/// PARAMS:
///    bReceptionist - if true returns the receptionist's idle pos. if false- the shopkeeper's.
/// RETURNS:
///    VECTOR of idle pos
FUNC VECTOR SHOPWORKER_GET_IDLE_POS(HAIRDO_SHOP_STRUCT &sData, BOOL bReceptionist)
	IF bReceptionist = TRUE
		RETURN sData.sShopReceptionist.vPosIdle
	ELSE
		// shopkeeper
		RETURN sData.sShopInfo.sShopKeeper.vPosIdle
	ENDIF
ENDFUNC

/// PURPOSE:
///    Returns the idle pos heading of the specified shopworker (shopkeeper or receptionist)
/// PARAMS:
///    bReceptionist - if true returns the receptionist's idle pos heading. if false- the shopkeeper's.
/// RETURNS:
///    FLOAT of idle pos heading
FUNC FLOAT SHOPWORKER_GET_IDLE_HEADING(HAIRDO_SHOP_STRUCT &sData, BOOL bReceptionist)
	IF bReceptionist = TRUE
		RETURN sData.sShopReceptionist.fHeadIdle
	ELSE
		// shopkeeper
		RETURN sData.sShopInfo.sShopKeeper.fHeadIdle
	ENDIF
ENDFUNC

/// PURPOSE:
///    Gets whether the receptionist / shopkeeper is looking at the player
/// PARAMS:
///    bReceptionist - true = recepionist. false = shopkeeper
/// RETURNS:
///    TRUE if they are looking at the player. FALSE otherwise
FUNC BOOL SHOPWORKER_GET_LOOKING_AT_PLAYER(HAIRDO_SHOP_STRUCT &sData, BOOL bReceptionist)
	IF bReceptionist = TRUE
		RETURN sData.bReceptionistLookingAtPlayer
	ELSE
		// shopkeeper
		RETURN sData.bHairdresserLookingAtPlayer
	ENDIF
ENDFUNC

/// PURPOSE:
///    Sets whether the receptionist / shopkeeper is looking at the player
/// PARAMS:
///    bReceptionist - true = recepionist. false = shopkeeper
///    bLooking - are they looking at the player
PROC SHOPWORKER_SET_LOOKING_AT_PLAYER(HAIRDO_SHOP_STRUCT &sData, BOOL bReceptionist, BOOL bLooking)
	IF bReceptionist = TRUE
		sData.bReceptionistLookingAtPlayer = bLooking
	ELSE
		// shopkeeper
		sData.bHairdresserLookingAtPlayer = bLooking
	ENDIF
ENDPROC

/// PURPOSE:
///    Stops shopkeeper / receptionist doing look ot, and clears flag
PROC SHOPWORKER_CLEAR_LOOK_AT(HAIRDO_SHOP_STRUCT &sData, BOOL bReceptionist)
	IF DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE)
		SHOPWORKER_SET_LOOKING_AT_PLAYER(sData, bReceptionist, FALSE)
		TASK_CLEAR_LOOK_AT(SHOPWORKER_GET_PED(sData, bReceptionist))
	ENDIF
ENDPROC

/// PURPOSE:
///    Tells the shopkeeper / receptionist to go to their idle position
PROC SHOPWORKER_GO_TO_IDLE_POS(HAIRDO_SHOP_STRUCT &sData, BOOL bReceptionist)
	IF DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE) OR bReceptionist
		IF GET_SCRIPT_TASK_STATUS(SHOPWORKER_GET_PED(sData, bReceptionist), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK		
			TASK_GO_STRAIGHT_TO_COORD(SHOPWORKER_GET_PED(sData, bReceptionist), SHOPWORKER_GET_IDLE_POS(sData, bReceptionist), PEDMOVEBLENDRATIO_WALK, DEFAULT_TIME_BEFORE_WARP, SHOPWORKER_GET_IDLE_HEADING(sData, bReceptionist))
		ENDIF
		//SHOPWORKER_CLEAR_LOOK_AT(sData, bReceptionist)
		
		// if shopkeeper, delete the scissors
		IF bReceptionist = FALSE
			CREATE_SCISSORS(sData, FALSE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Tells the shopkeeper / receptionist to turn to face the player
PROC SHOPWORKER_FACE_PLAYER(HAIRDO_SHOP_STRUCT &sData, BOOL bReceptionist)
	IF DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE) OR bReceptionist
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()		
			IF GET_SCRIPT_TASK_STATUS(SHOPWORKER_GET_PED(sData, bReceptionist), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
				TASK_TURN_PED_TO_FACE_ENTITY(SHOPWORKER_GET_PED(sData, bReceptionist), PLAYER_PED_ID(), -1)
			ENDIF
		ELSE
			IF GET_SCRIPT_TASK_STATUS(SHOPWORKER_GET_PED(sData, bReceptionist), SCRIPT_TASK_TURN_PED_TO_FACE_ENTITY) <> PERFORMING_TASK
			AND	GET_SCRIPT_TASK_STATUS(SHOPWORKER_GET_PED(sData, bReceptionist), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> PERFORMING_TASK
			AND NOT IS_PED_FACING_PED(SHOPWORKER_GET_PED(sData, bReceptionist), PLAYER_PED_ID(), 90)		
			AND sData.sShopInfo.eStage = SHOP_STAGE_WAIT_FOR_TRIGGER	
				TASK_TURN_PED_TO_FACE_ENTITY(SHOPWORKER_GET_PED(sData, bReceptionist), PLAYER_PED_ID(), -1)
			ENDIF
		ENDIF

		//SHOPWORKER_CLEAR_LOOK_AT(sData, bReceptionist)
		
		// if shopkeeper, delete the scissors
		//IF bReceptionist = FALSE
		//	CREATE_SCISSORS(sData,FALSE)
		//ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Handles the shop worker headtracking, turning to face player and returning to idle pos
/// PARAMS:
///    bReceptionist - if true this is the receptionist, if false this is the shopkeeper
PROC UPDATE_SHOPWORKER(HAIRDO_SHOP_STRUCT &sData, BOOL bReceptionist)
	PED_INDEX mPed = SHOPWORKER_GET_PED(sData, bReceptionist)
	
	IF NOT IS_PED_INJURED(mPed)
		//IF GET_DISTANCE_BETWEEN_ENTITIES(PLAYER_PED_ID(), mPed) < 10.0
			IF NOT SHOPWORKER_GET_LOOKING_AT_PLAYER(sData, bReceptionist)
				TASK_LOOK_AT_ENTITY(mPed, PLAYER_PED_ID(), -1)
				SHOPWORKER_SET_LOOKING_AT_PLAYER(sData, bReceptionist, TRUE)
			ENDIF
		//ELSE
			//IF SHOPWORKER_GET_LOOKING_AT_PLAYER(sData, bReceptionist)
				//SHOPWORKER_CLEAR_LOOK_AT(sData, bReceptionist)
			//ENDIF
		//ENDIF
		
		// Make the worker return to their spot if the player has pushed them off.
		FLOAT fIdlePosRadius
		IF IS_SALON_ACTIVE(sData)
			fIdlePosRadius = 3.0
		ELSE
			fIdlePosRadius = 1.5
		ENDIF
		IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(mPed, SHOPWORKER_GET_IDLE_POS(sData, bReceptionist)) > fIdlePosRadius
			SHOPWORKER_GO_TO_IDLE_POS(sData, bReceptionist)
		ELSE
			IF NOT IS_PED_FACING_PED(mPed, PLAYER_PED_ID(), 45)  //30
				SHOPWORKER_FACE_PLAYER(sData, bReceptionist)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Makes shop keeper and receptionist look at the player when they get within a certain range
PROC DO_PEDS_LOOK_AT_PLAYER(HAIRDO_SHOP_STRUCT &sData)
	IF IS_SHOP_PED_OK()
	
		// --------shopkeeper--------
		IF sData.sShopInfo.sShopKeeper.bSetup
			IF DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE)
				UPDATE_SHOPWORKER(sData, FALSE)
			ENDIF
		ENDIF
		
		// -------receptionist---------
		IF sData.sShopReceptionist.bSetup
			UPDATE_SHOPWORKER(sData, TRUE)
		ENDIF
	ENDIF
ENDPROC


PROC SETUP_NETWORK_SYNC_SCENE_FOR_SHOP(HAIRDO_SHOP_STRUCT &sData, BOOL bHoldLastFrame, BOOL bLooped)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		VECTOR vAnimPos, vAnimRot
		GET_HAIRDO_SHOP_ANIM_POSITION(sData.sShopInfo.eShop, vAnimPos, vAnimRot)
		sData.iNETSyncSceneShopkeeper = NETWORK_CREATE_SYNCHRONISED_SCENE(vAnimPos, vAnimRot, EULER_YXZ, bHoldLastFrame, bLooped)
		sData.bNETSyncSceneLooped = bLooped
	ENDIF
ENDPROC

PROC ADD_PED_TO_SHOP_SYNC_SCENE(HAIRDO_SHOP_STRUCT &sData, PED_INDEX pedID, STRING sNewAnim, SYNCED_SCENE_PLAYBACK_FLAGS eFlags = SYNCED_SCENE_NONE, FLOAT fBlendin = INSTANT_BLEND_IN, FLOAT fBlendout = INSTANT_BLEND_OUT)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF DOES_ENTITY_EXIST(pedID)
			NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(pedID, sData.iNETSyncSceneShopkeeper, sData.sHairdoShopAnimDict, sNewAnim, fBlendin, fBlendout, eFlags)		
		ENDIF
	ENDIF
ENDPROC

PROC ADD_ENTITY_TO_SHOP_SYNC_SCENE(HAIRDO_SHOP_STRUCT &sData, ENTITY_INDEX entityID, STRING sNewAnim, SYNCED_SCENE_PLAYBACK_FLAGS eFlags = SYNCED_SCENE_NONE, FLOAT fBlendin = INSTANT_BLEND_IN, FLOAT fBlendout = INSTANT_BLEND_OUT)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF DOES_ENTITY_EXIST(entityID)
			NETWORK_ADD_ENTITY_TO_SYNCHRONISED_SCENE(entityID, sData.iNETSyncSceneShopkeeper, sData.sHairdoShopAnimDict, sNewAnim, fBlendin, fBlendout, eFlags)		
			SET_ENTITY_VISIBLE(entityID, TRUE)
			FREEZE_ENTITY_POSITION(entityID, FALSE)
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(entityID)
		ENDIF
	ENDIF
ENDPROC

PROC START_NETWORK_SYNC_SCENE_FOR_SHOP(HAIRDO_SHOP_STRUCT &sData)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		NETWORK_START_SYNCHRONISED_SCENE(sData.iNETSyncSceneShopkeeper)
	ENDIF
ENDPROC

PROC STOP_NETWORK_SYNC_SCENE_FOR_SHOP(HAIRDO_SHOP_STRUCT &sData)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		sData.iSyncSceneShopkeeper = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sData.iNETSyncSceneShopkeeper)
		IF sData.iSyncSceneShopkeeper != -1
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncSceneShopkeeper)
				NETWORK_STOP_SYNCHRONISED_SCENE(sData.iNETSyncSceneShopkeeper)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_NETWORK_SYNC_SCENE_LOOPED_STATE(HAIRDO_SHOP_STRUCT &sData)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		sData.iSyncSceneShopkeeper = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sData.iNETSyncSceneShopkeeper)
		IF sData.iSyncSceneShopkeeper != -1
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncSceneShopkeeper)
				SET_SYNCHRONIZED_SCENE_LOOPED(sData.iSyncSceneShopkeeper, sData.bNETSyncSceneLooped)
			ENDIF
		ENDIF
	ENDIF
ENDPROC

PROC PRELOAD_PROPS(HAIRDO_SHOP_STRUCT &sData)

	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		PRELOAD_PED_COMP(PLAYER_PED_ID(),COMP_TYPE_PROPS,sData.eHeadProp)
		PRELOAD_PED_COMP(PLAYER_PED_ID(),COMP_TYPE_PROPS,sData.eEyeProp)
		PRELOAD_PED_COMP(PLAYER_PED_ID(),COMP_TYPE_PROPS,sData.eSpecialMask)
		PRELOAD_PED_COMP(PLAYER_PED_ID(),COMP_TYPE_PROPS,sData.eSpecial2Mask)
		PRELOAD_PED_COMP(PLAYER_PED_ID(),COMP_TYPE_PROPS,sData.eHairMask)
	ENDIF
	
ENDPROC

/// PURPOSE:
///    Starts the shopkeeper playing the new anim.
///    Also updates scissors to play correct anim
/// PARAMS:
///    sNewAnim - animation shopkeeper should play
///    bClearTasks- clear shopkeeper tasks first?
///    bLoop - should the scene loop?
PROC UPDATE_SHOPKEEPER_ANIM(HAIRDO_SHOP_STRUCT &sData, STRING sNewAnim, BOOL bClearTasks,  BOOL bLoop = FALSE, BOOL bClearTaskImmediate = FALSE, FLOAT fBlendout = NORMAL_BLEND_OUT)
	VECTOR vAnimPos, vAnimRot
	GET_HAIRDO_SHOP_ANIM_POSITION(sData.sShopInfo.eShop, vAnimPos, vAnimRot)
	
	IF NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
	AND DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE)
	
		IF bClearTasks = TRUE
			CLEAR_PED_TASKS(sData.sShopInfo.sShopKeeper.pedID)
		ENDIF
		
		IF bClearTaskImmediate = TRUE
			SET_PED_SHOULD_PLAY_IMMEDIATE_SCENARIO_EXIT(sData.sShopInfo.sShopKeeper.pedID)
			CLEAR_PED_TASKS_IMMEDIATELY(sData.sShopInfo.sShopKeeper.pedID)
			//FORCE_PED_AI_AND_ANIMATION_UPDATE(sData.sShopInfo.sShopKeeper.pedID)
		ENDIF
		
		sData.sCurrentKeeperAnim = sNewAnim
		CPRINTLN(DEBUG_SHOPS, "UPDATE_SHOPKEEPER_ANIM: ", sData.sCurrentKeeperAnim)
		
		STRING sScissorsAnim = GET_SCISSOR_ANIM_FOR_KEEPER_ANIM(sNewAnim)
		
		IF IS_STRING_NULL_OR_EMPTY(sScissorsAnim)
			CREATE_SCISSORS(sData, FALSE)
		ELSE
			IF NOT DOES_ENTITY_EXIST(sData.mScissors)
				CREATE_SCISSORS(sData, TRUE)
			ENDIF
		ENDIF
		
		sData.iSyncSceneShopkeeper = CREATE_SYNCHRONIZED_SCENE(vAnimPos, vAnimRot)
		SET_FORCE_FOOTSTEP_UPDATE(sData.sShopInfo.sShopKeeper.pedID,TRUE)
		TASK_SYNCHRONIZED_SCENE(sData.sShopInfo.sShopKeeper.pedID, sData.iSyncSceneShopkeeper, sData.sHairdoShopAnimDict, sData.sCurrentKeeperAnim, INSTANT_BLEND_IN, fBlendout, SYNCED_SCENE_NONE)
		SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sData.iSyncSceneShopkeeper, !bLoop)
		SET_SYNCHRONIZED_SCENE_LOOPED(sData.iSyncSceneShopkeeper, bLoop)
		FORCE_PED_AI_AND_ANIMATION_UPDATE(sData.sShopInfo.sShopKeeper.pedID)
		IF DOES_ENTITY_EXIST(sData.mScissors)
		AND DOES_ENTITY_HAVE_DRAWABLE(sData.mScissors)	
			PLAY_SYNCHRONIZED_ENTITY_ANIM(sData.mScissors, sData.iSyncSceneShopkeeper, sScissorsAnim, sData.sHairdoShopAnimDict,  INSTANT_BLEND_IN, INSTANT_BLEND_OUT)
			FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(sData.mScissors)
		ENDIF
		ENDIF
ENDPROC

/// PURPOSE:
///    Requests the anim dictionary used by this shop
/// RETURNS:
///    TRUE if all anims needed have loaded. FALSE otherwise.
FUNC BOOL LOAD_HAIRDO_SHOP_ANIMS(HAIRDO_SHOP_STRUCT &sData)
	IF IS_STRING_NULL_OR_EMPTY(sData.sHairdoShopAnimDict)
		//CPRINTLN(DEBUG_SHOPS, "LOAD_HAIRDO_SHOP_ANIMS: sData.sHairdoShopAnimDict not set up yet.")
		RETURN FALSE
	ENDIF
	
	REQUEST_ANIM_DICT(sData.sHairdoShopAnimDict)
	
//	IF IS_HAIRDO_SHOP_INTRO_NEEDED(sData)
//		REQUEST_MODEL(V_ILEV_HD_DOOR_L)
//		REQUEST_MODEL(V_ILEV_HD_DOOR_R)
//		
//		IF NOT HAS_MODEL_LOADED(V_ILEV_HD_DOOR_L)
//		OR NOT HAS_MODEL_LOADED(V_ILEV_HD_DOOR_R)
//			RETURN FALSE
//		ENDIF
//	ENDIF
	
	IF NOT HAS_ANIM_DICT_LOADED(sData.sHairdoShopAnimDict)
		RETURN FALSE
	ENDIF

	RETURN TRUE
ENDFUNC

/// PURPOSE:
///    Unloads the anim dictionary used by this shop
PROC REMOVE_HAIRDO_SHOP_ANIMS(HAIRDO_SHOP_STRUCT &sData)
	IF NOT IS_STRING_NULL_OR_EMPTY(sData.sHairdoShopAnimDict)
		REMOVE_ANIM_DICT(sData.sHairdoShopAnimDict)
	ENDIF
ENDPROC

/// PURPOSE:
///  Tries to restore the head prop that we stored for the player when he entered chair  
/// PARAMS:
///    sData - hairdo shop struct
PROC RESTORE_HEAD_PROP(HAIRDO_SHOP_STRUCT &sData)
	IF sData.eHeadProp <> DUMMY_PED_COMP
		IF NETWORK_IS_GAME_IN_PROGRESS()
		
			PED_INDEX mPedToCheck = PLAYER_PED_ID()
		
			IF DOES_ENTITY_EXIST(sData.sCutsceneData.pedID)
			AND NOT IS_PED_INJURED(sData.sCutsceneData.pedID)
				CPRINTLN(DEBUG_PED_COMP, "RESTORE_HEAD_PROP cutscene ped exists, use them instead.")
				mPedToCheck = sData.sCutsceneData.pedID
			ENDIF

			// in MP some hats are blocked for some haircuts
			IF DO_HAT_AND_HAIR_CHECK_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), sData.eHeadProp, GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(mPedToCheck, COMP_TYPE_HAIR))
				CPRINTLN(DEBUG_PED_COMP, "Restoring head prop.")
				CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_PROPS, sData.eHeadProp, FALSE)
			ELSE
				CPRINTLN(DEBUG_PED_COMP, "Not restoring head prop: doesnt work with current hair.")
				CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_HEAD_NONE, FALSE)
			ENDIF
		ELSE
			CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_PROPS, sData.eHeadProp, FALSE)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    Restores the head and eye props the player had when he entered the shop
///    Resets values to -1 so we don't get masks randomly appearing
PROC ResetProps(HAIRDO_SHOP_STRUCT &sData)
	IF DOES_ENTITY_EXIST(PLAYER_PED_ID())
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
		
			RESTORE_HEAD_PROP(sData)
			
			IF sData.eEyeProp <> DUMMY_PED_COMP
				CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_PROPS, sData.eEyeProp, FALSE)
			ENDIF
			IF sData.eSpecialMask <> DUMMY_PED_COMP
				CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_SPECIAL, sData.eSpecialMask, FALSE)
			ENDIF
			IF sData.eSpecial2Mask <> DUMMY_PED_COMP
				CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, sData.eSpecial2Mask, FALSE)
			ENDIF
			IF sData.eHairMask <> DUMMY_PED_COMP
				CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_HAIR, sData.eHairMask, FALSE)
			ENDIF
			IF sData.eJacketState <> INT_TO_ENUM(HOODED_JACKET_STATE_ENUM, -1)
				SET_HOODED_JACKET_STATE(PLAYER_PED_ID(), sData.eJacketState)
			ENDIF
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
				// MP masks are on PED_COMP_BERD
				IF sData.eBerdMask <> DUMMY_PED_COMP
					CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_BERD, sData.eBerdMask, FALSE)
				ENDIF
				
				// MP masks are on PED_COMP_TEETH
				IF sData.eTeethMask <> DUMMY_PED_COMP
					CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH, sData.eTeethMask, FALSE)
				ENDIF
				
				// MP parachutes are on COMP_TYPE_HAND
				IF sData.iHandDrawable <> -1
				AND sData.iHandTexture <> -1
					#IF USE_TU_CHANGES
					PRINTLN("ResetProps - Restoring varition ", sData.iHandDrawable, ",", sData.iHandTexture, " for comp type ", GET_PED_COMP_TYPE_STRING(COMP_TYPE_HAND), " - ped comp: ", GET_COMP_VARIATION_TYPE_STRING(GET_PED_COMPONENT_FROM_TYPE(COMP_TYPE_HAND)))
					#ENDIF
					
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), GET_PED_COMPONENT_FROM_TYPE(COMP_TYPE_HAND), sData.iHandDrawable, sData.iHandTexture)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	sData.eHeadProp = DUMMY_PED_COMP
	sData.eEyeProp = DUMMY_PED_COMP
	sData.eBerdMask = DUMMY_PED_COMP
	sData.eTeethMask = DUMMY_PED_COMP
	sData.iHandDrawable = -1
	sData.iHandTexture = -1
	sData.eSpecialMask = DUMMY_PED_COMP
	sData.eSpecial2Mask = DUMMY_PED_COMP
	sData.eHairMask = DUMMY_PED_COMP
	sData.eJacketState = INT_TO_ENUM(HOODED_JACKET_STATE_ENUM, -1)
ENDPROC

PROC RESTORE_COMPS(HAIRDO_SHOP_STRUCT &sData)

	// If the cutscene ped exists, set the items on them, otherwise set it on the player
	PED_INDEX pedID_ShopPed = PLAYER_PED_ID()
	IF DOES_ENTITY_EXIST(sData.sCutsceneData.pedID)
	AND NOT IS_PED_INJURED(sData.sCutsceneData.pedID)
		pedID_ShopPed = sData.sCutsceneData.pedID
	ENDIF
	
	IF NOT g_bInMultiplayer
		SET_PED_CONFIG_FLAG(pedID_ShopPed, PCF_ForceSkinCharacterCloth, FALSE)
	ENDIF

	IF IS_SHOP_PED_OK()
	AND NOT IS_PED_INJURED(pedID_ShopPed)
	AND NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
			
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()	
			// Reset the head and eye props
			ResetProps(sData)	
			RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
			RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
		ENDIF
		
		// Reset Hair
		IF sData.eCurrentMenu = HME_HAIR
			CPRINTLN(DEBUG_SHOPS, "Resetting hairdo now. sData.iCurrentHairdo = ", sData.iCurrentHairdo)
			CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, sData.iCurrentHairdo), FALSE)
		
		// Reset beard 
		ELIF sData.eCurrentMenu = HME_BEARD
			IF NETWORK_IS_GAME_IN_PROGRESS()
				IF DOES_PLAYER_HAVE_BEARD_OPTIONS()
					SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_FACIAL_HAIR, sData.iCurrentBeard-1, sData.iTint1, 0, RT_NONE, sData.fCurrentBeardFade)
				ENDIF
			ELSE
				CPRINTLN(DEBUG_SHOPS, "Resetting SP beard now. sData.iCurrentBeard = ", sData.iCurrentBeard)
				CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, sData.iCurrentBeard), FALSE)
			ENDIF
			
		// Reset makeup MP
		ELIF sData.eCurrentMenu = HME_MAKEUP
			IF DOES_PLAYER_HAVE_MAKEUP_OPTIONS()
				SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_MAKEUP, sData.iPlayerMakeup-1, sData.iTint1, 0, RT_NONE, 1.0)
				//Hide or unhide eyebrows because some makeup contains eyebrows for some reason
				IF sData.iPlayerMakeup-1 = -1 
				OR sData.iPlayerMakeup-1 > 15
					SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_EYEBROW, GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_ACTIVE_CHARACTER_SLOT()), sData.iTint1, 0, RT_NONE, 1.0)
				ELSE
					SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_EYEBROW, GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_ACTIVE_CHARACTER_SLOT()), sData.iTint1, 0, RT_NONE, 0.0)
				ENDIF
			ENDIF
		ELSE
			CPRINTLN(DEBUG_SHOPS, "Not resetting hairdo / beard / makeup sData.eCurrentMenu= ", sData.eCurrentMenu)
		ENDIF
		
		// Update stored variation
		IF NOT g_bInMultiplayer
			enumCharacterList eCurrentPed
			eCurrentPed = GET_CURRENT_PLAYER_PED_ENUM()
			IF IS_PLAYER_PED_PLAYABLE(eCurrentPed)
				g_sLastStoredPlayerPedClothes[eCurrentPed].iDrawableVariation[PED_COMP_HAIR] = GET_PED_DRAWABLE_VARIATION(pedID_ShopPed, PED_COMP_HAIR)
				g_sLastStoredPlayerPedClothes[eCurrentPed].iTextureVariation[PED_COMP_HAIR] = GET_PED_TEXTURE_VARIATION(pedID_ShopPed, PED_COMP_HAIR)
				
				g_sLastStoredPlayerPedClothes[eCurrentPed].iDrawableVariation[PED_COMP_HEAD] = GET_PED_DRAWABLE_VARIATION(pedID_ShopPed, PED_COMP_HEAD)
				g_sLastStoredPlayerPedClothes[eCurrentPed].iTextureVariation[PED_COMP_HEAD] = GET_PED_TEXTURE_VARIATION(pedID_ShopPed, PED_COMP_HEAD)
				
				g_sLastStoredPlayerPedClothes[eCurrentPed].iDrawableVariation[PED_COMP_BERD] = GET_PED_DRAWABLE_VARIATION(pedID_ShopPed, PED_COMP_BERD)
				g_sLastStoredPlayerPedClothes[eCurrentPed].iTextureVariation[PED_COMP_BERD] = GET_PED_TEXTURE_VARIATION(pedID_ShopPed, PED_COMP_BERD)
			ENDIF
		ENDIF
		
		sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_OUTRO
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			// see if we need to remove the player's hat (some hair block some hats)
			// needs to be done here so we don't see it get removed
			RESTORE_HEAD_PROP(sData)
		ENDIF
	ENDIF
ENDPROC

PROC STORE_SHOP_GB_BOSS_STYLE(HAIRDO_SHOP_STRUCT &sData)
	sData.sCutsceneData.sGBStyle = GB_OUTFITS_GD_GET_BOSS_STYLE()
	CPRINTLN(DEBUG_SHOPS, "STORE_SHOP_GB_BOSS_STYLE \"", GET_SHOP_NAME(sData.sShopInfo.eShop), "\" GB outfit ", GB_OUTFITS_GET_BOSS_STYLE_STRING_FOR_HELP(sData.sCutsceneData.sGBStyle))
ENDPROC
FUNC BOOL RESTORE_SHOP_GB_BOSS_STYLE(HAIRDO_SHOP_STRUCT &sData)
	GB_MAGNATE_OUTFIT_BOSS_STYLE sThisGBStyle = GB_OUTFITS_GD_GET_BOSS_STYLE()
	IF sData.sCutsceneData.sGBStyle != sThisGBStyle
		//
		CPRINTLN(DEBUG_SHOPS, "RESTORE_SHOP_GB_BOSS_STYLE \"", GET_SHOP_NAME(sData.sShopInfo.eShop), "\" GB outfit ",
				GB_OUTFITS_GET_BOSS_STYLE_STRING_FOR_HELP(sData.sCutsceneData.sGBStyle), " != ",
				GB_OUTFITS_GET_BOSS_STYLE_STRING_FOR_HELP(sThisGBStyle))
		
		IF sThisGBStyle = GB_BOSS_STYLE_NONE
			SET_PLAYER_CLOTHES_FOR_RETURN_TO_FREEMODE()
			STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID(), TRUE)
			GB_OUTFITS_SET_LOCAL_PLAYER_IS_WEARING_GANG_OUTFIT(FALSE)				
			
			sData.sCutsceneData.sGBStyle = GB_BOSS_STYLE_NONE
		ENDIF
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Cleanup model for cutscene entities
PROC CLEANUP_HAIRDO_CUTSCENE_ASSETS(HAIRDO_SHOP_STRUCT &sData)
	IF sData.sCutsceneData.bModelRequested
		SET_MODEL_AS_NO_LONGER_NEEDED(sData.sCutsceneData.eModel)
		sData.sCutsceneData.bModelRequested = FALSE
	ENDIF
	
	IF DOES_ENTITY_EXIST(sData.sCutsceneData.pedID)
		DELETE_PED(sData.sCutsceneData.pedID)
	ENDIF
	IF DOES_ENTITY_EXIST(g_pShopClonePed)
		DELETE_PED(g_pShopClonePed)
	ENDIF
	RESTORE_SHOP_GB_BOSS_STYLE(sData)
ENDPROC

/// PURPOSE: Load model for cutscene entities
FUNC BOOL LOAD_HAIRDO_CUTSCENE_ASSETS(HAIRDO_SHOP_STRUCT &sData, BOOL bCreatePed)
	IF g_bInMultiplayer
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			
			IF NOT DOES_ENTITY_EXIST(sData.sCutsceneData.pedID)
			
				IF sData.iHeadBlendTimer = -1
					sData.iHeadBlendTimer = GET_GAME_TIMER()
				ENDIF
				
				sData.sCutsceneData.bModelRequested = TRUE
				sData.sCutsceneData.eModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
				REQUEST_MODEL(sData.sCutsceneData.eModel)
				
				IF HAS_MODEL_LOADED(sData.sCutsceneData.eModel)
					IF (HAS_PED_HEAD_BLEND_FINISHED(PLAYER_PED_ID()) OR (GET_GAME_TIMER() - sData.iHeadBlendTimer > 5000))
				
						IF NOT bCreatePed
							RETURN TRUE
						ELSE
							sData.sCutsceneData.pedID = CLONE_PED(PLAYER_PED_ID(), FALSE, FALSE, FALSE)
							g_pShopClonePed = sData.sCutsceneData.pedID
							
							SET_ENTITY_COORDS_NO_OFFSET(sData.sCutsceneData.pedID, sData.sLocateInfo.vPlayerBrowsePos)
							SET_ENTITY_HEADING(sData.sCutsceneData.pedID, sData.sLocateInfo.fPlayerBrowseHead)
							SET_ENTITY_COLLISION(sData.sCutsceneData.pedID, FALSE)
							FREEZE_ENTITY_POSITION(sData.sCutsceneData.pedID, TRUE)
							SET_ENTITY_VISIBLE(sData.sCutsceneData.pedID, FALSE)
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sData.sCutsceneData.pedID, TRUE)
							
							MARK_PED_DECORATIONS_AS_CLONED_FROM_LOCAL_PLAYER(sData.sCutsceneData.pedID, TRUE)
							RESET_PED_VISIBLE_DAMAGE(sData.sCutsceneData.pedID)
							
							STORE_SHOP_GB_BOSS_STYLE(sData)
							
							IF GB_IS_LOCAL_PLAYER_BOSS_OF_A_GANG()
								INT iEmblemToDisplay = GB_OUTFITS_BD_GET_BIKER_EMBLEM_TO_DISPLAY()
								IF iEmblemToDisplay = 0
									PRINTLN("LOAD_HAIRDO_CUTSCENE_ASSETS - CREW_LOGO=NONE")
								ELIF iEmblemToDisplay = 1
									PRINTLN("LOAD_HAIRDO_CUTSCENE_ASSETS - CREW_LOGO=CREW")
								ELIF iEmblemToDisplay = 2
									PRINTLN("LOAD_HAIRDO_CUTSCENE_ASSETS - CREW_LOGO=PRESET")
									REQUEST_STREAMED_TEXTURE_DICT(GET_CUSTOM_CREW_LOGO_TXD_NAME_FOR_PLAYER(PLAYER_ID()))
									OVERRIDE_PED_CREW_LOGO_TEXTURE(sData.sCutsceneData.pedID, GET_CUSTOM_CREW_LOGO_TXD_NAME_FOR_PLAYER(PLAYER_ID()), GET_CUSTOM_CREW_LOGO_TXD_TEXTURE_FOR_PLAYER(PLAYER_ID(), CUSTOM_CREW_LOGO_PRESET))
								ENDIF
							ENDIF
							
							CPRINTLN(DEBUG_SHOPS, "LOAD_HAIRDO_CUTSCENE_ASSETS Setting cutscene ped invisible")
						ENDIF
					ELSE
						PRINTLN("LOAD_HAIRDO_CUTSCENE_ASSETS - Waiting for player head blend to finalise")
					ENDIF
				ELSE
					PRINTLN("LOAD_HAIRDO_CUTSCENE_ASSETS - Waiting for cutscene model to load")
				ENDIF
				
				// Wait for the head blend to complete before we return true.
				RETURN FALSE
			ENDIF
			
			IF NOT IS_PED_INJURED(sData.sCutsceneData.pedID)
				IF NOT HAS_PED_HEAD_BLEND_FINISHED(sData.sCutsceneData.pedID)
				OR NOT HAVE_ALL_STREAMING_REQUESTS_COMPLETED(sData.sCutsceneData.pedID)
					RETURN FALSE
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	RETURN TRUE
ENDFUNC

PROC REQUEST_USE_OF_SHOP_KEEPER_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_INDEX PlayerID, SHOP_NAME_ENUM eShop)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT playerBD[NATIVE_TO_INT(PlayerID)].bPlayerRequestedShop
			PRINTLN("shop_keeper_usage Casino - local player is requesting use of shop keeper, shop=", GET_SHOP_NAME(eShop))
			playerBD[NATIVE_TO_INT(PlayerID)].iShopRquestedByPlayer = ENUM_TO_INT(eShop)
			playerBD[NATIVE_TO_INT(PlayerID)].bPlayerRequestedShop = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC REMOVE_USE_OF_SHOP_KEEPER_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_INDEX PlayerID, SHOP_NAME_ENUM eShop)

	// Not using this flag for now.
	IF eShop = EMPTY_SHOP
	ENDIF

	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF playerBD[NATIVE_TO_INT(PlayerID)].bPlayerRequestedShop
			PRINTLN("shop_keeper_usage - local player is no longer requesting use of shop keeper, shop=", GET_SHOP_NAME(eShop))
			playerBD[NATIVE_TO_INT(PlayerID)].bPlayerRequestedShop = FALSE
			playerBD[NATIVE_TO_INT(PlayerID)].iShopRquestedByPlayer = 0
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL IS_SHOP_KEEPER_SAFE_TO_USE_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_INDEX PlayerID, SHOP_NAME_ENUM eShop, BOOL bMustBeAssignedToPlayer = FALSE)

	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN TRUE
	ENDIF
	
	INT iPlayerAssignedToSlot = serverBD.iPlayerAssignedToShop[ENUM_TO_INT(eShop)]-1

	// Player already assigned a shop
	IF iPlayerAssignedToSlot = NATIVE_TO_INT(PlayerID)
		RETURN TRUE
	ENDIF
	
	// Free slot available
	IF NOT bMustBeAssignedToPlayer
		IF iPlayerAssignedToSlot = -1
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Cleans up any assets that were created
PROC CLEANUP_SHOP(HAIRDO_SHOP_STRUCT &sData, BOOL bResetOnly = FALSE)

	PROCESS_END_SHOPPING_SAVE(TRUE)
	
	IF sData.sBrowseInfo.bBrowsing
		IF SHOULD_TRANSITION_SESSIONS_RESTART_FM_AFTER_END_RESERVING_SLOT()
			PRINTLN("KR STORE TEST = restore hairdo")
			SET_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
			g_sTransitionSessionData.sEndReserve.iShopLocateRestore = sData.sBrowseInfo.iCurrentLocate
		ENDIF
	ENDIF

	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())		
			RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
			RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
		ENDIF
	ENDIF

	IF sData.sShopInfo.iContextID != NEW_CONTEXT_INTENTION
		RELEASE_CONTEXT_INTENTION(sData.sShopInfo.iContextID)
	ENDIF
	
	IF IS_SHOP_KEEPER_SAFE_TO_USE(PLAYER_ID(), sData.eInShop, TRUE)
	AND sData.sShopInfo.eShop != HAIRDO_SHOP_CASINO_APT
		REMOVE_USE_OF_SHOP_KEEPER(PLAYER_ID(), sData.eInShop)
	ENDIF
	
	IF IS_SHOP_KEEPER_SAFE_TO_USE_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_ID(), sData.eInShop, FALSE)
	AND sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT
		REMOVE_USE_OF_SHOP_KEEPER_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_ID(), sData.eInShop)
	ENDIF

	PERFORM_DEFAULT_SHOP_CLEANUP(sData.sShopInfo, sData.eInShop)

	// Destroy any assets we may have loaded
	DESTROY_SHOP_CAMERAS(sData.sShopInfo.sCameras)
	
	// Cleanup shop keeper
	IF NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
		IF sData.sShopInfo.bForceCleanup
			IF SHOP_SCRIPT_WAS_LAUNCHED_IN_MP(sData.eInShop)
			OR NETWORK_IS_GAME_IN_PROGRESS()
				// Do not cleanup in MP - other scripts may be using them.
			ELSE
				DELETE_PED(sData.sShopInfo.sShopKeeper.pedID)
			ENDIF
		ELIF bResetOnly
			// remove the ped incase we need to flush memory
			IF SHOP_SCRIPT_WAS_LAUNCHED_IN_MP(sData.eInShop)
			OR NETWORK_IS_GAME_IN_PROGRESS()
				// Do not cleanup in MP - other scripts may be using them.
			ELSE
				DELETE_PED(sData.sShopInfo.sShopKeeper.pedID)
			ENDIF
		ELSE
			IF SHOP_SCRIPT_WAS_LAUNCHED_IN_MP(sData.eInShop)
			OR NETWORK_IS_GAME_IN_PROGRESS()
				// Do not cleanup in MP - other scripts may be using them.
			ELSE
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sData.sShopInfo.sShopKeeper.pedID,TRUE)
				SET_PED_KEEP_TASK(sData.sShopInfo.sShopKeeper.pedID, TRUE)
				SET_PED_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopKeeper.pedID)
			ENDIF
		ENDIF
	ELSE
		sData.sShopInfo.sShopKeeper.pedID = NULL
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(P_CS_SCISSORS_S)
	IF DOES_ENTITY_EXIST(sData.mScissors)
		SET_OBJECT_AS_NO_LONGER_NEEDED(sData.mScissors)
	ENDIF
	
	IF sData.sShopInfo.sShopKeeper.bModelRequested
		SET_MODEL_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopKeeper.model)
		sData.sShopInfo.sShopKeeper.bModelRequested = FALSE
	ENDIF
	
	// Cleanup shop receptionist
	IF NOT IS_PED_INJURED(sData.sShopReceptionist.pedID)
		IF sData.sShopInfo.bForceCleanup
			DELETE_PED(sData.sShopReceptionist.pedID)
		ELIF bResetOnly
			// remove the ped incase we need to flush memory
			DELETE_PED(sData.sShopReceptionist.pedID)
		ELSE
			IF DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE)	
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sData.sShopReceptionist.pedID,TRUE)
			ENDIF
			SET_PED_KEEP_TASK(sData.sShopReceptionist.pedID, TRUE)
			SET_PED_AS_NO_LONGER_NEEDED(sData.sShopReceptionist.pedID)
		ENDIF
	ELSE
		sData.sShopReceptionist.pedID = NULL
	ENDIF
	
	IF sData.sShopReceptionist.bModelRequested
		SET_MODEL_AS_NO_LONGER_NEEDED(sData.sShopReceptionist.model)
		sData.sShopReceptionist.bModelRequested = FALSE
	ENDIF
	
	// Cleanup shop customer
	IF NOT IS_PED_INJURED(sData.sShopInfo.sShopCustomer.pedID)
		IF sData.sShopInfo.bForceCleanup
			DELETE_PED(sData.sShopInfo.sShopCustomer.pedID)
		ELIF bResetOnly
			// remove the ped incase we need to flush memory
			DELETE_PED(sData.sShopInfo.sShopCustomer.pedID)
		ELSE
			SET_PED_KEEP_TASK(sData.sShopInfo.sShopCustomer.pedID, TRUE)
			SET_PED_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopCustomer.pedID)
		ENDIF
	ELSE
		sData.sShopInfo.sShopCustomer.pedID = NULL
	ENDIF
	
	IF sData.sShopInfo.sShopCustomer.bModelRequested
		SET_MODEL_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopCustomer.model)
		sData.sShopInfo.sShopCustomer.bModelRequested = FALSE
	ENDIF
	
	REMOVE_HAIRDO_SHOP_ANIMS(sData)
	
	IF sData.bLoadPTFX
		REMOVE_PTFX_ASSET()
		sData.bLoadPTFX = FALSE
	ENDIF
	
	IF sData.bAudioBankLoaded
		RELEASE_AMBIENT_AUDIO_BANK()
		sData.bAudioBankLoaded = FALSE
	ENDIF
	
	IF sData.bRelGroupSetup
		REMOVE_RELATIONSHIP_GROUP(sData.hairdoGroup)
		sData.bRelGroupSetup = FALSE
	ENDIF
	
	REMOVE_ANIM_DICT("amb@prop_human_seat_chair@male@generic@react_cowering")

	// If we were browing then make sure player has control etc
	BOOL bDoFullCleanup = FALSE

	IF IS_PLAYER_BROWSING_ITEMS_IN_SHOP(sData.eInShop)
		bDoFullCleanup = TRUE
	ENDIF
	
	IF g_bShopCutsceneActive = TRUE
		IF sData.sShopInfo.eStage = SHOP_STAGE_ENTRY_INTRO
		OR sData.sShopInfo.eStage = SHOP_STAGE_BROWSE_ITEMS
			bDoFullCleanup = TRUE
		ENDIF
	ENDIF
	
	CLEANUP_SOFA(sData)
	
	IF sData.eInShop = HAIRDO_SHOP_CASINO_APT
		g_iCasinoAptHairdoShopInstance = -1
	ENDIF
	
	IF bDoFullCleanup = TRUE
		CPRINTLN(DEBUG_SHOPS, "Shop cleanup: was browsing so release cut-scene assets.")
		// Reset the head and eye props
		IF IS_SHOP_PED_OK()
			ResetProps(sData)
		ENDIF

		UNLOCK_PLAYER_FOR_BROWSE(sData.sBrowseInfo)
		SET_WIDESCREEN_BORDERS(FALSE, 0)
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
		
		CLEANUP_PLAYER_FOR_MP_SHOP_CUTSCENE()
		END_SHOP_CUTSCENE()
		
		#IF USE_TU_CHANGES
		IF NETWORK_IS_GAME_IN_PROGRESS()	
			IF g_sMPTunables.b_onsale_hair_dresser_shop
				SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED("MPShops")
			ENDIF
		ENDIF
		#ENDIF
		
		CLEANUP_MENU_ASSETS(TRUE, ENUM_TO_INT(sData.sShopInfo.eShop))
		CLEANUP_NGMP_MENU_ASSETS(sData)
		UNLOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
		
		CLEANUP_HAIRDO_CUTSCENE_ASSETS(sData)
		
		//Reset hair or player can get free haircuts B*1566840
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, sData.iCurrentHairdo), FALSE)
			CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, sData.iCurrentBeard), FALSE)
		ENDIF
		
		sData.sBuddyHideData.buddyHide = FALSE
		UPDATE_BUDDIES_FOR_SHOP_CUTSCENE(sData.sBuddyHideData)
		
		SET_PLAYER_IS_CHANGING_CLOTHES(FALSE)
		
		IF g_bShopCutsceneActive = TRUE
			CLEAR_ROOM_FOR_GAME_VIEWPORT()
		ENDIF
		
		g_bShopCutsceneActive= FALSE
		
		IF sData.bLeaveRemotePlayerBehindDisabled
			CPRINTLN(DEBUG_SHOPS, "Shop cleanup: NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(FALSE)")
			NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(FALSE)
			sData.bLeaveRemotePlayerBehindDisabled = FALSE
		ENDIF
			
	ELSE
		CPRINTLN(DEBUG_SHOPS, "Shop cleanup: was NOT browsing.")
	ENDIF

	IF bResetOnly
		PRINT_SHOP_DEBUG_LINE("Resetting script")
		SET_BIT(g_sShopSettings.iProperties[sData.eInShop], SHOP_NS_FLAG_FORCE_RESET_PROCESSING)
	ELSE
		PRINT_SHOP_DEBUG_LINE("Terminating script")
		CLEAR_BIT(g_sShopSettings.iProperties[sData.eInShop], SHOP_NS_FLAG_SHOP_SCRIPT_RUNNING)
		TERMINATE_THIS_THREAD()
	ENDIF
ENDPROC

/// PURPOSE: Cleans up any assets that were created and resets script
PROC RESET_SHOP(HAIRDO_SHOP_STRUCT &sData)
	CLEANUP_SHOP(sData, TRUE)
	RESET_HAIRDO_SHOP_DATA(sData)
ENDPROC

/// PURPOSE: Make a call to finalise head blend when ready
PROC PROCESS_FINALISE_HEAD_BLEND(HAIRDO_SHOP_STRUCT &sData)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			IF sData.bDoFinaliseHeadBlend
				IF sData.sShopInfo.eStage != SHOP_STAGE_BROWSE_ITEMS
				OR sData.sBrowseInfo.eStage > SHOP_BROWSE_STAGE_OUTRO
					IF HAS_PED_HEAD_BLEND_FINISHED(PLAYER_PED_ID())
						PRINTLN("HAS_PED_HEAD_BLEND_FINISHED = TRUE")
						FINALIZE_HEAD_BLEND(PLAYER_PED_ID())
						sData.bDoFinaliseHeadBlend = FALSE
					ENDIF
				ENDIF
			ELSE
				IF sData.sShopInfo.eStage = SHOP_STAGE_BROWSE_ITEMS
				AND sData.sBrowseInfo.eStage <= SHOP_BROWSE_STAGE_OUTRO
					sData.bDoFinaliseHeadBlend = TRUE
				ENDIF
			ENDIF
		ENDIF
	ELSE
		sData.bDoFinaliseHeadBlend = FALSE
	ENDIF
ENDPROC

/// PURPOSE: Plays lines of dialogue when required
PROC PROCESS_HAIRDO_SHOP_DIALOGUE(HAIRDO_SHOP_STRUCT &sData)
	PED_COMP_NAME_ENUM current_haircut
	
	IF NOT sData.sShopInfo.bDataSet
		SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, HAIR_DLG_NONE, DLG_PRIORITY_MID)
		EXIT
	ENDIF
	
	IF IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
		EXIT
	ENDIF
	
	//-------------------Setup Conversation---------------------------
	// Player enters shop
	IF sData.sShopInfo.bPlayerInShop
		IF sData.sShopInfo.eStage = SHOP_STAGE_WAIT_FOR_TRIGGER
			IF NOT sData.bDlgChk_Greet_Triggered
				IF (VDIST(GET_ENTITY_COORDS(sData.sShopInfo.sShopKeeper.pedID),GET_ENTITY_COORDS(PLAYER_PED_ID())) < 5.5 OR NOT NETWORK_IS_GAME_IN_PROGRESS())
					IF NOT sData.bDlgChk_Greet_TimerSet
						IF NETWORK_IS_GAME_IN_PROGRESS()
							sData.tdDlgChk_Greet = GET_TIME_OFFSET(GET_NETWORK_TIME(), GET_RANDOM_INT_IN_RANGE(300, 600))
						ELSE
							 sData.iDlgChk_Greet = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(300, 600)
						ENDIF
						sData.bDlgChk_Greet_TimerSet = TRUE
					ELSE
						IF NETWORK_IS_GAME_IN_PROGRESS()
							IF IS_TIME_MORE_THAN(GET_NETWORK_TIME(), sData.tdDlgChk_Greet)
								SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, HAIR_DLG_GREET, DLG_PRIORITY_HIGH)
								sData.bDlgChk_Greet_Triggered = TRUE
							ENDIF
						ELSE
							IF (GET_GAME_TIMER() - sData.iDlgChk_Greet) > 0
								SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, HAIR_DLG_GREET, DLG_PRIORITY_HIGH)
								sData.bDlgChk_Greet_Triggered = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF sData.sShopInfo.eStage = SHOP_STAGE_BROWSE_ITEMS
			sData.bDlgChk_Greet_Triggered = TRUE
		ENDIF
	ELSE
		// Reset the greet dialogue when player leaves shop
		IF GET_DISTANCE_BETWEEN_PLAYER_AND_SHOP(sData.sShopInfo.eShop) > 30.0
			sData.bDlgChk_Greet_Triggered = FALSE
			sData.bDlgChk_Greet_TimerSet = FALSE
		ENDIF
	ENDIF
	
	// Browsing dialogue
	IF sData.sBrowseInfo.bBrowsing
	
		// No longer required to greet the player
		sData.bDlgChk_Greet_Triggered = TRUE
	
		IF sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_INTRO
			SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, HAIR_DLG_NONE, DLG_PRIORITY_MID)

			// Ask what player wants
			IF NOT sData.bDlgChk_Ask_Triggered
				IF NOT sData.bDlgChk_Ask_TimerSet
					IF NETWORK_IS_GAME_IN_PROGRESS()
						sData.tdDlgChk_Ask = GET_TIME_OFFSET(GET_NETWORK_TIME(), 100)  //6000
					ELSE
						sData.iDlgChk_Ask = GET_GAME_TIMER() + 100//6000
					ENDIF
					sData.bDlgChk_Ask_TimerSet = TRUE
				ELSE
					IF NETWORK_IS_GAME_IN_PROGRESS()
						IF IS_TIME_MORE_THAN(GET_NETWORK_TIME(), sData.tdDlgChk_Ask)
							SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, HAIR_DLG_ASK, DLG_PRIORITY_HIGH)
							sData.bDlgChk_Ask_Triggered = TRUE
						ENDIF
					ELSE
						IF (GET_GAME_TIMER() - sData.iDlgChk_Ask) > 0
							SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, HAIR_DLG_ASK, DLG_PRIORITY_HIGH)
							sData.bDlgChk_Ask_Triggered = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELIF sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_BROWSING
			// Reset exit dialogue
			sData.bDlgChk_Exit_Trigger = TRUE
			sData.bDlgChk_Exit_TimerSet = FALSE
			
			// Cutting hair
			IF sData.sBrowseInfo.bItemSelected
			AND NOT sData.sBrowseInfo.bPurchasingItem
				IF NOT sData.bDlgChk_Cut_Triggered
					IF NOT sData.bDlgChk_Cut_TimerSet
						IF NETWORK_IS_GAME_IN_PROGRESS()
							sData.tdDlgChk_Cut = GET_TIME_OFFSET(GET_NETWORK_TIME(), GET_RANDOM_INT_IN_RANGE(100, 400))
						ELSE
							sData.iDlgChk_Cut = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(100, 400)
						ENDIF
						sData.bDlgChk_Cut_TimerSet = TRUE
					ELSE
						IF NETWORK_IS_GAME_IN_PROGRESS()
							IF IS_TIME_MORE_THAN(GET_NETWORK_TIME(), sData.tdDlgChk_Cut)
								SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, HAIR_DLG_CUT, DLG_PRIORITY_HIGH)
								sData.bDlgChk_Cut_Triggered = TRUE
							ENDIF
						ELSE
							IF (GET_GAME_TIMER() - sData.iDlgChk_Cut) > 0
								SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, HAIR_DLG_CUT, DLG_PRIORITY_HIGH)
								sData.bDlgChk_Cut_Triggered = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ELSE
		// Reset browse dialogue
		sData.bDlgChk_Ask_Triggered = FALSE
		sData.bDlgChk_Ask_TimerSet = FALSE
		sData.bDlgChk_Cut_Triggered = FALSE
		sData.bDlgChk_Cut_TimerSet = FALSE
	ENDIF
	
	// Finished browsing
	IF sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_OUTRO
	
		IF sData.bDlgChk_Exit_Trigger
			IF NOT sData.bDlgChk_Exit_TimerSet
				IF NETWORK_IS_GAME_IN_PROGRESS()
					sData.tdDlgChk_Exit = GET_TIME_OFFSET(GET_NETWORK_TIME(), GET_RANDOM_INT_IN_RANGE(300, 600))
				ELSE
					sData.iDlgChk_Exit = GET_GAME_TIMER() + GET_RANDOM_INT_IN_RANGE(300, 600)
				ENDIF
				sData.bDlgChk_Exit_TimerSet = TRUE
			ELSE
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF IS_TIME_MORE_THAN(GET_NETWORK_TIME(), sData.tdDlgChk_Exit)
						SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, HAIR_DLG_EXIT, DLG_PRIORITY_HIGH)
						sData.bDlgChk_Exit_Trigger = FALSE
					ENDIF
				ELSE
					IF (GET_GAME_TIMER() - sData.iDlgChk_Exit) > 0
						SETUP_SHOP_DIALOGUE_TO_PLAY(sData.sDialogueInfo, HAIR_DLG_EXIT, DLG_PRIORITY_HIGH)
						sData.bDlgChk_Exit_Trigger = FALSE
					ENDIF
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
// ------------Trigger Conversation ------------------------------------------------
	// Safe to play?
	IF sData.sDialogueInfo.iDialogueToPlay != -1
		IF NOT IS_SHOP_DIALOGUE_SAFE_TO_PLAY(sData.sDialogueInfo, sData.sShopInfo.eShop)
			IF NETWORK_IS_GAME_IN_PROGRESS()
				// Allow a little time before we clear
				IF NOT sData.bDlgChk_Reset_TimerSet
					sData.tdDlgChk_Reset = GET_TIME_OFFSET(GET_NETWORK_TIME(), 3000)
					sData.bDlgChk_Reset_TimerSet = TRUE
				ELIF IS_TIME_MORE_THAN(GET_NETWORK_TIME(), sData.tdDlgChk_Reset)
					RESET_SHOP_DIALOGUE_STRUCT(sData.sDialogueInfo)
					sData.bDlgChk_Reset_TimerSet = FALSE
				ENDIF
			ELSE
				// Allow a little time before we clear
				IF NOT sData.bDlgChk_Reset_TimerSet
					sData.iDlgChk_Reset = GET_GAME_TIMER()
					sData.bDlgChk_Reset_TimerSet = TRUE
				ELIF (GET_GAME_TIMER() - sData.iDlgChk_Reset) > 3000
					RESET_SHOP_DIALOGUE_STRUCT(sData.sDialogueInfo)
					sData.bDlgChk_Reset_TimerSet = FALSE
				ENDIF
			ENDIF
		ELSE
			sData.bDlgChk_Reset_TimerSet = FALSE
			
			BOOL SyncOverNetwork = TRUE
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND NOT NETWORK_HAS_CONTROL_OF_ENTITY(sData.sShopInfo.sShopKeeper.pedID)
				SyncOverNetwork = FALSE
			ENDIF
	
			SWITCH INT_TO_ENUM(HAIRDO_DIALOGUE_ENUM, sData.sDialogueInfo.iDialogueToPlay)
				CASE HAIR_DLG_GREET
					IF SHOULD_PLAYER_LOSE_COPS_FOR_SHOP()
						PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_NO_COPS", "SPEECH_PARAMS_FORCE", SyncOverNetwork)
					ELSE
						IF Is_Ped_Drunk(PLAYER_PED_ID())
						OR (IS_PED_IN_UNDERWEAR_SP(PLAYER_PED_ID()) AND NOT NETWORK_IS_GAME_IN_PROGRESS())
							PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_GREET_UNUSUAL", "SPEECH_PARAMS_FORCE", SyncOverNetwork)
						ELSE		
							IF NOT NETWORK_IS_GAME_IN_PROGRESS()
							AND GET_ENTITY_MODEL(PLAYER_PED_ID()) = PLAYER_ZERO
							AND (IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON)
							OR IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_OUTFIT, OUTFIT_P0_EPSILON_WITH_MEDAL))
								PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "KIFFLOM_GREET", "SPEECH_PARAMS_FORCE", SyncOverNetwork)
//							ELIF IS_SPECIAL_EDITION_GAME() 
//							OR IS_COLLECTORS_EDITION_GAME()
//								PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_GREET_SPECIAL", "SPEECH_PARAMS_FORCE", SyncOverNetwork)
							ELSE
								PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_GREET", "SPEECH_PARAMS_FORCE", SyncOverNetwork)
							ENDIF
						ENDIF
					ENDIF
				BREAK
				CASE HAIR_DLG_ASK
					SWITCH sData.sShopInfo.eShop
						CASE HAIRDO_SHOP_01_BH
							IF g_sShopSettings.eHairConvState = HAIR_CUT_BY_LOW_END //Was our last haircut by the low end barber? If so, pour disdain on your cut. 
								PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_HAIR_WHAT_WANT", "SPEECH_PARAMS_FORCE", SyncOverNetwork)
							ELSE
								//If we are bald, don't play anything.
								current_haircut = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_HAIR)
								IF current_haircut <> HAIR_P0_1_0 AND GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
									PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_HAIR_WHAT_WANT", "SPEECH_PARAMS_FORCE", SyncOverNetwork)
								ENDIF
							ENDIF
							g_sShopSettings.eHairConvState = TALKED_TO_BY_HIGH_END
						BREAK
						DEFAULT
							PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_HAIR_WHAT_WANT", "SPEECH_PARAMS_FORCE", SyncOverNetwork)
							g_sShopSettings.eHairConvState = TALKED_TO_LOW_END
						BREAK
					ENDSWITCH
				BREAK
				CASE HAIR_DLG_CUT
					SWITCH sData.sShopInfo.eShop
						CASE HAIRDO_SHOP_01_BH
							PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_CUTTING_HAIR", "SPEECH_PARAMS_FORCE", SyncOverNetwork)
							g_sShopSettings.eHairConvState = HAIR_CUT_BY_HIGH_END
						BREAK
						DEFAULT
							PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_CUTTING_HAIR", "SPEECH_PARAMS_FORCE", SyncOverNetwork)
							g_sShopSettings.eHairConvState = HAIR_CUT_BY_LOW_END
						BREAK
					ENDSWITCH
				BREAK
				CASE HAIR_DLG_EXIT
					PLAY_PED_AMBIENT_SPEECH_NATIVE(sData.sShopInfo.sShopKeeper.pedID, "SHOP_GOODBYE", "SPEECH_PARAMS_FORCE", SyncOverNetwork)
				BREAK
				DEFAULT
					#IF IS_DEBUG_BUILD
						SCRIPT_ASSERT("PROCESS_HAIRDO_SHOP_DIALOGUE() - missing dialogue case. Tell Kenneth R.")
						PRINTSTRING("\nPROCESS_HAIRDO_SHOP_DIALOGUE() - missing dialogue case. Tell Kenneth R.")PRINTNL()
					#ENDIF
				BREAK
			ENDSWITCH
			
			SET_SHOP_DIALOGUE_HAS_JUST_BEEN_TRIGGERED(sData.sDialogueInfo)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE:
///    If we have a previous version of this ped it gets set as no longer needed or deleted
/// PARAMS:
///    mPed - the ped we are checking.
///    vIdlePos - the idle position this ped should be in.
PROC CLEANUP_PREVIOUS_SHOP_PED(PED_INDEX &mPed, VECTOR vIdlePos)
	IF DOES_ENTITY_EXIST(mPed)
		IF IS_PED_INJURED(mPed)
		OR IS_PED_FLEEING(mPed)
		OR GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(mPed), vIdlePos) > 10.0
			PRINT_SHOP_DEBUG_LINE("Marking previous shop ped as no longer needed")
			SET_PED_AS_NO_LONGER_NEEDED(mPed)
			EXIT // set as no longer needed and exit
		ENDIF
	ENDIF
	IF DOES_ENTITY_EXIST(mPed)
		PRINT_SHOP_DEBUG_LINE("Deleting previous shop ped.")
		DELETE_PED(mPed)
	ENDIF
ENDPROC

/// PURPOSE: Loads the required shop assets and sets up items to browse
PROC DO_INITIALISE(HAIRDO_SHOP_STRUCT &sData)
	INT iVariation
	VECTOR vAnimPos, vAnimRot
	
	SWITCH sData.sBrowseInfo.iControl
		// Attempt to get the shop data
		CASE 0
			// init the prop stuff to DUMMY_PED_COMP so it doesnt think we've got masks etc
			sData.eHeadProp = DUMMY_PED_COMP
			sData.eEyeProp = DUMMY_PED_COMP
			sData.eBerdMask = DUMMY_PED_COMP
			sData.eTeethMask = DUMMY_PED_COMP
			sData.iHandDrawable = -1
			sData.iHandTexture = -1
			sData.eSpecialMask = DUMMY_PED_COMP
			sData.eSpecial2Mask = DUMMY_PED_COMP
			sData.eHairMask = DUMMY_PED_COMP
			sData.eJacketState = INT_TO_ENUM(HOODED_JACKET_STATE_ENUM, -1)
			
			sData.sAudioBank = "SCRIPT\\Hair_Cut"
			sData.iNumMakeupItems = GET_PED_HEAD_OVERLAY_NUM(4) +1
			
			#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
			#IF IS_DEBUG_BUILD
			IF sData.iNumMakeupItems < COUNT_OF(sData.sMakeup)
				CWARNINGLN(DEBUG_SHOPS, "DO_INITIALISE - iNumMakeupItems[", sData.iNumMakeupItems, "] < COUNT_OF(sMakeup)[", COUNT_OF(sData.sMakeup), "], increase size of sMakeup??")
				sData.iNumMakeupItems = COUNT_OF(sData.sMakeup)
			ENDIF
			#ENDIF
			#ENDIF
			
			// Setup the shop information based on the coords that were passed into the script
			IF GET_HAIRDO_SHOP_DATA(sData.sShopInfo, sData.sShopReceptionist, sData.eInShop)
				sData.sBrowseInfo.iControl++
			ELSE
				// Problem finding a suitable shop so cleanup script
				CLEANUP_SHOP(sData)
			ENDIF
		BREAK
		
		// Make sure the shop is open for business before we initialise
		CASE 1
			// Make sure the shop is open for business
			IF IS_SHOP_OPEN_FOR_BUSINESS(sData.sShopInfo.eShop)
				PRINTLN(GET_THIS_SCRIPT_NAME(), ": ", GET_STRING_FROM_TEXT_FILE(GET_SHOP_NAME(sData.sShopInfo.eShop)), " is open for business")
				sData.sBrowseInfo.iControl++
			ENDIF
		BREAK
		
		// Ready to initialise
		CASE 2
//			IF NOT IS_SHOP_INTERIOR_READY(sData.sShopInfo.eShop)
//				EXIT
//			ENDIF
			
			REQUEST_MODEL(P_CS_SCISSORS_S)
			
			IF NOT HAS_SHOP_RUN_ENTRY_INTRO(sData.sShopInfo.eShop)
			AND NOT g_bInMultiplayer
			AND NOT ARE_VECTORS_EQUAL(sData.sShopInfo.sShopCustomer.vPosIdle, <<0,0,0>>)
				sData.sShopInfo.bRunEntryIntro = TRUE
			ELSE
				sData.sShopInfo.bRunEntryIntro = FALSE
			ENDIF
			
			// Clear up any previous shop keepers
			IF NOT sData.sShopInfo.sShopKeeper.bSetup
				CLEANUP_PREVIOUS_SHOP_PED(sData.sShopInfo.sShopKeeper.pedID, sData.sShopInfo.sShopKeeper.vPosIdle)
				
				// Start requesting the model now
				IF sData.sShopInfo.sShopKeeper.model <> DUMMY_MODEL_FOR_SCRIPT
					REQUEST_MODEL(sData.sShopInfo.sShopKeeper.model)
					sData.sShopInfo.sShopKeeper.bModelRequested = TRUE
				ENDIF
			ENDIF
			
			// Clear up any previous shop assistants
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()		
				IF NOT sData.sShopReceptionist.bSetup
					CLEANUP_PREVIOUS_SHOP_PED(sData.sShopReceptionist.pedID, sData.sShopReceptionist.vPosIdle)
					
					// Start requesting the model now
					IF sData.sShopReceptionist.model <> DUMMY_MODEL_FOR_SCRIPT
						REQUEST_MODEL(sData.sShopReceptionist.model)
						sData.sShopReceptionist.bModelRequested = TRUE
					ENDIF
				ENDIF
			ENDIF
			
			// Clear up any previous shop customers
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()			
				IF NOT sData.sShopInfo.sShopCustomer.bSetup
					CLEANUP_PREVIOUS_SHOP_PED(sData.sShopInfo.sShopCustomer.pedID, sData.sShopInfo.sShopCustomer.vPosIdle)
					
					// Start requesting the model now
					IF IS_HAIRDO_SHOP_INTRO_NEEDED(sData)
						IF sData.sShopInfo.sShopCustomer.model <> DUMMY_MODEL_FOR_SCRIPT
							REQUEST_MODEL(sData.sShopInfo.sShopCustomer.model)
							sData.sShopInfo.sShopCustomer.bModelRequested = TRUE
						ENDIF
					ENDIF
				ENDIF
			ENDIF
			
			// Exit if any of the models we need haven't loaded yet!
			IF (sData.sShopInfo.sShopKeeper.bModelRequested AND NOT HAS_MODEL_LOADED(sData.sShopInfo.sShopKeeper.model))
			OR (sData.sShopReceptionist.bModelRequested AND NOT HAS_MODEL_LOADED(sData.sShopReceptionist.model))
			OR (sData.sShopInfo.sShopCustomer.bModelRequested AND NOT HAS_MODEL_LOADED(sData.sShopInfo.sShopCustomer.model))
			OR NOT HAS_MODEL_LOADED(P_CS_SCISSORS_S)
				EXIT
			ENDIF
			
			IF IS_HAIRDO_SHOP_INTRO_NEEDED(sData)
				// set up which anim dictionary we are using + load the anims
				IF IS_SALON_ACTIVE(sData)
					// SALON
					sData.sHairdoShopAnimDict = "misshair_shop@hair_dressers"
				ELIF sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT
					// CASINO_APT
					sData.sHairdoShopAnimDict = "anim@amb@luxury_suite@spa@barbers"
				ELSE
					// BARBER SHOPS
					sData.sHairdoShopAnimDict = "misshair_shop@barbers"
				ENDIF

				IF NOT LOAD_HAIRDO_SHOP_ANIMS(sData)
					//CPRINTLN(DEBUG_SHOPS, "waiting for anims to load")
					EXIT
				ENDIF
			ENDIF
			
			// Set up new shop keeper
			IF NOT DOES_ENTITY_EXIST(sData.sShopInfo.sShopKeeper.pedID)
			AND sData.sShopInfo.sShopKeeper.bModelRequested
				
				// Make sure it's safe to create net shop keeper
				BOOL bCreateBarber
				bCreateBarber = FALSE
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
					AND NOT NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.shopkeeperID)
					AND NOT serverBD.bShopKeeperSetupDone
						bCreateBarber = TRUE
					ENDIF
					
					IF bCreateBarber
					AND (sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT)
						INTERIOR_INSTANCE_INDEX hairdoInterior
						hairdoInterior = GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_SHOP_COORDS(sData.sShopInfo.eShop), GET_SHOP_INTERIOR_TYPE(sData.sShopInfo.eShop))
						
						IF (hairdoInterior = NULL)
							PRINT_SHOP_DEBUG_LINE("Hairdo interior is null..")
							bCreateBarber = FALSE
						ENDIF
					ENDIF
				ELSE
					bCreateBarber = TRUE
				ENDIF
				
				
				IF NOT bCreateBarber
					IF NETWORK_IS_GAME_IN_PROGRESS()
						PRINT_SHOP_DEBUG_LINE("Using net shop keeper..")
						UPDATE_SHOP_KEEPER_PED_ID(sData.sShopInfo.sShopKeeper)
						
						IF NOT sData.sShopInfo.sShopKeeper.bNetKeeperSetup
							PRINT_SHOP_DEBUG_LINE("Waiting for shop keeper to be initialised")
							EXIT
						ENDIF
					ENDIF
				ELSE
					PRINT_SHOP_DEBUG_LINE("Creating shop keeper..")
					CLEAR_AREA(sData.sShopInfo.sShopKeeper.vPosIdle, 2.5, TRUE)
					
					// spawn at idle pos, or at the chair if we're doing the barber shop intro
					VECTOR vSpawnPos 
					vSpawnPos = sData.sShopInfo.sShopKeeper.vPosIdle
					IF IS_HAIRDO_SHOP_INTRO_NEEDED(sData)
						IF NOT IS_SALON_ACTIVE(sData)
							IF GET_HAIRDO_SHOP_ANIM_POSITION(sData.sShopInfo.eShop, vAnimPos, vAnimRot)
								vSpawnPos = vAnimPos
							ENDIF
						ENDIF
					ENDIF
					
					IF NETWORK_IS_GAME_IN_PROGRESS()
						IF NOT CREATE_NET_PED(serverBD.shopkeeperID, PEDTYPE_CIVMALE, sData.sShopInfo.sShopKeeper.model, vSpawnPos, sData.sShopInfo.sShopKeeper.fHeadIdle, TRUE, TRUE, TRUE)
							EXIT
						ENDIF
						IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
							serverBD.bShopKeeperSetupDone = TRUE
						ENDIF
						UPDATE_SHOP_KEEPER_PED_ID(sData.sShopInfo.sShopKeeper)
					ELSE
						sData.sShopInfo.sShopKeeper.pedID = CREATE_PED(PEDTYPE_CIVMALE, sData.sShopInfo.sShopKeeper.model, vSpawnPos, sData.sShopInfo.sShopKeeper.fHeadIdle, FALSE, FALSE)	
					ENDIF
					
					IF DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE)	
						IF sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT
							SET_PED_DEFAULT_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID)
							
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_HAIR, 1, 0)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_HEAD, 1, 0)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_TORSO, 1, 0)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_LEG, 0, 1)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_HAND, 0, 1)
							SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_SPECIAL, 0, 0)
							
							SET_ENTITY_CAN_BE_DAMAGED(sData.sShopInfo.sShopKeeper.PedID, FALSE)
							SET_PED_AS_ENEMY(sData.sShopInfo.sShopKeeper.PedID, FALSE)
							SET_CURRENT_PED_WEAPON(sData.sShopInfo.sShopKeeper.PedID, WEAPONTYPE_UNARMED, TRUE)
							
							SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sData.sShopInfo.sShopKeeper.PedID, TRUE)
							SET_PED_RESET_FLAG(sData.sShopInfo.sShopKeeper.PedID, PRF_DisablePotentialBlastReactions, TRUE)
							SET_PED_CONFIG_FLAG(sData.sShopInfo.sShopKeeper.PedID, PCF_UseKinematicModeWhenStationary, TRUE)
							SET_PED_CONFIG_FLAG(sData.sShopInfo.sShopKeeper.PedID, PCF_DontActivateRagdollFromExplosions, TRUE)
							SET_PED_CONFIG_FLAG(sData.sShopInfo.sShopKeeper.PedID, PCF_DontActivateRagdollFromVehicleImpact, TRUE)
							
							SET_PED_CAN_EVASIVE_DIVE(sData.sShopInfo.sShopKeeper.PedID, FALSE)
							SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(sData.sShopInfo.sShopKeeper.PedID, TRUE)
							SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sData.sShopInfo.sShopKeeper.PedID, FALSE)
							SET_PED_CAN_RAGDOLL(sData.sShopInfo.sShopKeeper.PedID, FALSE)
							SET_PED_CONFIG_FLAG(sData.sShopInfo.sShopKeeper.PedID, PCF_DisableExplosionReactions, TRUE)
							
							NETWORK_SET_ENTITY_ONLY_EXISTS_FOR_PARTICIPANTS(sData.sShopInfo.sShopKeeper.PedID, TRUE)
						ELIF IS_SALON_ACTIVE(sData)
							SET_PED_DEFAULT_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID)
						ELSE
							// use the shop enum to set how the ped looks. 
							// take 1 off as the low end barbers begins at 1
							iVariation = ENUM_TO_INT(sData.sShopInfo.eShop)-1 
						
							/*
							ped has these variations:
							2 heads with 2 textures each.
							2 hairs with 2 and 3 textures each.
							2 uppers with 1 texture each.
							2 lowers with 1 texture each.
							*/
							
							SWITCH iVariation
								CASE 0
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_HEAD, 0, 0)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_HAIR, 1, 0)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_TORSO, 0, 0)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_LEG, 1, 0)
								BREAK
								CASE 1
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_HEAD, 1, 0)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_HAIR, 0, 1)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_TORSO, 1, 0)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_LEG, 0, 0)
								BREAK
								CASE 2
									//SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_HEAD, 0, 1)
									//SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_HAIR, 1, 0)
									//SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_TORSO, 0, 0)
									//SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_LEG, 1, 0)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,0), 1, 0, 0) //(head)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,2), 1, 0, 0) //(hair)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,3), 1, 0, 0) //(uppr)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,4), 1, 0, 0) //(lowr)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, INT_TO_ENUM(PED_COMPONENT,8), 0, 0, 0) //(accs)
								BREAK
								CASE 3
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_HEAD, 1, 1)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_HAIR, 1, 1)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_TORSO, 1, 0)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_LEG, 0, 0)
								BREAK
								CASE 4
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_HEAD, 1, 0)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_HAIR, 1, 0)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_TORSO, 0, 0)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_LEG, 1, 0)
								BREAK
								CASE 5
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_HEAD, 1, 0)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_HAIR, 1, 1)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_TORSO, 1, 0)
									SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopKeeper.pedID, PED_COMP_LEG, 1, 0)
								BREAK
							ENDSWITCH
						ENDIF
						
						IF IS_SHOP_IN_AN_INTERIOR(sData.sShopInfo.eShop)
							RETAIN_ENTITY_IN_INTERIOR(sData.sShopInfo.sShopKeeper.pedID, GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_SHOP_COORDS(sData.sShopInfo.eShop), GET_SHOP_INTERIOR_TYPE(sData.sShopInfo.eShop)))
						ENDIF
						SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sData.sShopInfo.sShopKeeper.pedID, TRUE)
						SET_ENTITY_LOD_DIST(sData.sShopInfo.sShopKeeper.pedID, 300)
						IF IS_SALON_ACTIVE(sData)
							SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sData.sShopInfo.sShopKeeper.pedID,FALSE)
							SET_PED_CONFIG_FLAG(sData.sShopInfo.sShopKeeper.pedID, PCF_UseKinematicModeWhenStationary, TRUE) // prevent shopkeeper from being pushed about by the player.
						ENDIF
						SET_PED_CAN_EVASIVE_DIVE(sData.sShopInfo.sShopKeeper.pedID,FALSE)
						SET_TREAT_AS_AMBIENT_PED_FOR_DRIVER_LOCKON(sData.sShopInfo.sShopKeeper.pedID,TRUE)
						SET_PED_AS_ENEMY(sData.sShopInfo.sShopKeeper.pedID, FALSE)
						SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sData.sShopInfo.sShopKeeper.pedID, TRUE)
					ENDIF
				ENDIF
				
				IF DOES_ENTITY_EXIST(sData.sShopInfo.sShopKeeper.pedID)
				AND NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
					IF IS_SALON_ACTIVE(sData)
						REMOVE_PED_FOR_DIALOGUE(sData.sDialogueInfo.cultresConversation, ConvertSingleCharacter("E"))
						ADD_PED_FOR_DIALOGUE(sData.sDialogueInfo.cultresConversation, ConvertSingleCharacter("E"), sData.sShopInfo.sShopKeeper.pedID, "ShopHairStylist")
					ELSE
						REMOVE_PED_FOR_DIALOGUE(sData.sDialogueInfo.cultresConversation, ConvertSingleCharacter("D"))
						ADD_PED_FOR_DIALOGUE(sData.sDialogueInfo.cultresConversation, ConvertSingleCharacter("D"), sData.sShopInfo.sShopKeeper.pedID, "ShopBarber")
					ENDIF
					SET_MODEL_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopKeeper.model)
					sData.sShopInfo.sShopKeeper.bModelRequested = FALSE
					sData.sShopInfo.sShopKeeper.bSetup = TRUE
				ENDIF
			ENDIF

			// Create shop assistant
			IF NOT DOES_ENTITY_EXIST(sData.sShopReceptionist.pedID)
			AND sData.sShopReceptionist.bModelRequested
				PRINT_SHOP_DEBUG_LINE("Creating shop receptionist")
				CLEAR_AREA(sData.sShopReceptionist.vPosIdle, 2.5, TRUE)
				
				sData.sShopReceptionist.pedID = CREATE_PED(PEDTYPE_CIVFEMALE, sData.sShopReceptionist.model, sData.sShopReceptionist.vPosIdle, sData.sShopReceptionist.fHeadIdle, FALSE, FALSE)
				SET_PED_DEFAULT_COMPONENT_VARIATION(sData.sShopReceptionist.pedID)
				IF IS_SHOP_IN_AN_INTERIOR(sData.sShopInfo.eShop)
					RETAIN_ENTITY_IN_INTERIOR(sData.sShopReceptionist.pedID, GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_SHOP_COORDS(sData.sShopInfo.eShop), GET_SHOP_INTERIOR_TYPE(sData.sShopInfo.eShop)))
				ENDIF
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sData.sShopReceptionist.pedID, TRUE)
				SET_PED_CONFIG_FLAG(sData.sShopReceptionist.pedID, PCF_UseKinematicModeWhenStationary, TRUE) // Stops them being pushed about by the player.
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(sData.sShopInfo.sShopKeeper.pedID,FALSE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(sData.sShopReceptionist.model)
				sData.sShopReceptionist.bModelRequested = FALSE
				SET_PED_AS_ENEMY(sData.sShopReceptionist.pedID, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sData.sShopReceptionist.pedID, TRUE)
				sData.sShopReceptionist.bSetup = TRUE 
				
				REMOVE_PED_FOR_DIALOGUE(sData.sDialogueInfo.cultresConversation, ConvertSingleCharacter("D"))
				ADD_PED_FOR_DIALOGUE(sData.sDialogueInfo.cultresConversation, ConvertSingleCharacter("D"), sData.sShopInfo.sShopKeeper.pedID, "ShopBarberAssistant")
				
				IF IS_SALON_ACTIVE(sData)
					SET_AMBIENT_VOICE_NAME(sData.sShopInfo.sShopKeeper.pedID, "S_M_M_HAIRDRESSER_01_BLACK_MINI_01")
				ELSE
					SET_AMBIENT_VOICE_NAME(sData.sShopInfo.sShopKeeper.pedID, "S_F_M_FEMBARBER_BLACK_MINI_01")
				ENDIF
			ENDIF
		
			// Set up new shop customer
			IF NOT DOES_ENTITY_EXIST(sData.sShopInfo.sShopCustomer.pedID)
			AND sData.sShopInfo.sShopCustomer.bModelRequested
				PRINT_SHOP_DEBUG_LINE("Creating shop customer")
				CLEAR_AREA(sData.sShopInfo.sShopCustomer.vPosIdle, 2.5, TRUE)
				sData.sShopInfo.sShopCustomer.pedID = CREATE_PED(PEDTYPE_CIVMALE, sData.sShopInfo.sShopCustomer.model, sData.sShopInfo.sShopCustomer.vPosIdle, sData.sShopInfo.sShopCustomer.fHeadIdle, FALSE, FALSE)
				// set customer variations
				IF IS_SALON_ACTIVE(sData)
					SET_PED_DEFAULT_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID)
				ELSE
					/*
					SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, PED_COMP_BERD, 1, 0)
					SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, PED_COMP_DECL, 1, 0)
					SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, PED_COMP_FEET, 1, 0)
					SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, PED_COMP_HAIR, 1, 0)
					SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, PED_COMP_HAND, 1, 0)
					SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, PED_COMP_HEAD, 1, 0)
					SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, PED_COMP_JBIB, 1, 0)
					SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, PED_COMP_LEG, 1, 0)
					SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, PED_COMP_SPECIAL, 1, 0)
					SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, PED_COMP_SPECIAL2, 1, 0)
					SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, PED_COMP_TEETH, 1, 0)
					SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, PED_COMP_TORSO, 1, 0)
					*/
					SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, INT_TO_ENUM(PED_COMPONENT,0), 1, 1, 0) //(head)
					SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, INT_TO_ENUM(PED_COMPONENT,2), 0, 1, 0) //(hair)
					SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, INT_TO_ENUM(PED_COMPONENT,3), 1, 2, 0) //(uppr)
					SET_PED_COMPONENT_VARIATION(sData.sShopInfo.sShopCustomer.pedID, INT_TO_ENUM(PED_COMPONENT,4), 0, 0, 0) //(lowr)
				ENDIF
						
				SET_ENTITY_COLLISION(sData.sShopInfo.sShopCustomer.pedID, FALSE)
				IF IS_SHOP_IN_AN_INTERIOR(sData.sShopInfo.eShop)
					RETAIN_ENTITY_IN_INTERIOR(sData.sShopInfo.sShopCustomer.pedID, GET_INTERIOR_AT_COORDS_WITH_TYPE(GET_SHOP_COORDS(sData.sShopInfo.eShop), GET_SHOP_INTERIOR_TYPE(sData.sShopInfo.eShop)))
				ENDIF
				SET_ENTITY_SHOULD_FREEZE_WAITING_ON_COLLISION(sData.sShopInfo.sShopCustomer.pedID, TRUE)
				
				SET_MODEL_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopCustomer.model)
				sData.sShopInfo.sShopCustomer.bModelRequested = FALSE
				
				REQUEST_ANIM_DICT("amb@prop_human_seat_chair@male@generic@react_cowering")
				
				sData.sShopInfo.sShopCustomer.bSetup = TRUE
			ENDIF
		
			// set up relationship group for shopkeeper and assistant
			IF NOT sData.bRelGroupSetup
			
				BOOL bDoRelGroup  // are we ready to set up the relationship group?
				bDoRelGroup = TRUE
				
				// check if all peds needed have been created
				IF NOT DOES_ENTITY_EXIST(sData.sShopInfo.sShopKeeper.pedID)
				OR IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
					bDoRelGroup = FALSE
				ENDIF
				IF IS_SALON_ACTIVE(sData)
				AND NOT NETWORK_IS_GAME_IN_PROGRESS()	
					IF NOT DOES_ENTITY_EXIST(sData.sShopReceptionist.pedID)
						bDoRelGroup = FALSE
					ENDIF
				ENDIF
			
				// if peds are ready, set up the relationship group
				IF bDoRelGroup = TRUE
					TEXT_LABEL_15 sRelGroup
					sRelGroup = "HAIRDRESSER"
					sRelGroup += ENUM_TO_INT(sData.sShopInfo.eShop)
					ADD_RELATIONSHIP_GROUP(sRelGroup, sData.hairdoGroup)

					IF DOES_ENTITY_EXIST(sData.sShopReceptionist.pedID)
						SET_PED_RELATIONSHIP_GROUP_HASH(sData.sShopReceptionist.pedID, sData.hairdoGroup)
					ENDIF
					IF DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE)
						SET_PED_RELATIONSHIP_GROUP_HASH(sData.sShopInfo.sShopKeeper.pedID, sData.hairdoGroup)
						SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, sData.hairdoGroup, RELGROUPHASH_PLAYER)
						IF NETWORK_IS_GAME_IN_PROGRESS()
							SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_LIKE, RELGROUPHASH_PLAYER, sData.hairdoGroup)
							SET_PED_CAN_BE_TARGETTED(sData.sShopInfo.sShopKeeper.pedID,FALSE)
						ENDIF
					ENDIF
					sData.bRelGroupSetup = TRUE
				ENDIF
			ENDIF
			
			IF NOT IS_HAIRDO_SHOP_INTRO_NEEDED(sData)
				// set up which anim dictionary we are using + load the anims
				IF IS_SALON_ACTIVE(sData)
					// SALON
					sData.sHairdoShopAnimDict = "misshair_shop@hair_dressers"
				ELIF sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT
					// CASINO_APT
					sData.sHairdoShopAnimDict = "anim@amb@luxury_suite@spa@barbers"
				ELSE
					// BARBER SHOPS
					sData.sHairdoShopAnimDict = "misshair_shop@barbers"
				ENDIF

				IF NOT LOAD_HAIRDO_SHOP_ANIMS(sData)
					//CPRINTLN(DEBUG_SHOPS, "waiting for anims to load")
					EXIT
				ENDIF
			ENDIF
			
			// Prepare the peds for the intro
			IF IS_HAIRDO_SHOP_INTRO_NEEDED(sData)
				STRING sShopkeeperStartAnim, sCustomerStartAnim
				
				IF IS_SALON_ACTIVE(sData)
					sShopkeeperStartAnim = "keeper_base"
					sCustomerStartAnim = "player_base"
				ELSE
					sShopkeeperStartAnim = "keeper_tutorial_base"
					sCustomerStartAnim = "customer_tutorial_base"
				ENDIF
				
				IF NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
					UPDATE_SHOPKEEPER_ANIM(sData, sShopkeeperStartAnim, FALSE)
				ENDIF
				IF NOT IS_PED_INJURED(sData.sShopInfo.sShopCustomer.pedID)
					IF GET_HAIRDO_SHOP_ANIM_POSITION(sData.sShopInfo.eShop, vAnimPos, vAnimRot)
						TASK_PLAY_ANIM_ADVANCED(sData.sShopInfo.sShopCustomer.pedID, sData.sHairdoShopAnimDict, sCustomerStartAnim, vAnimPos, vAnimRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION | AF_NOT_INTERRUPTABLE | AF_LOOPING | AF_EXTRACT_INITIAL_OFFSET)
					ENDIF
				ENDIF
			ENDIF
			
			IF DOES_ENTITY_EXIST(sData.sShopReceptionist.pedID)
				IF NOT IS_PED_INJURED(sData.sShopReceptionist.pedID)
					TASK_START_SCENARIO_IN_PLACE(sData.sShopReceptionist.pedID,"WORLD_HUMAN_STAND_IMPATIENT_UPRIGHT")
				ENDIF
			ENDIF
			
			#IF USE_TU_CHANGES
			IF NETWORK_IS_GAME_IN_PROGRESS()	
				IF g_sMPTunables.b_onsale_hair_dresser_shop
					REQUEST_STREAMED_TEXTURE_DICT("MPShops")  //Request texture dict for the sale sign
				ENDIF
			ENDIF
			#ENDIF
			
			// Unlock the shop doors
			LOCK_SHOP_DOORS(sData.sShopInfo.eShop, FALSE)
					
			sData.sBrowseInfo.iControl = 0
			sData.sShopInfo.eStage = SHOP_STAGE_WAIT_FOR_ENTRY
			
			CPRINTLN(DEBUG_SHOPS, GET_THIS_SCRIPT_NAME(), ": ", GET_STRING_FROM_TEXT_FILE(GET_SHOP_NAME(sData.sShopInfo.eShop)), " initialised")
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Make receptionist cower and the customer play cower in chair anims
PROC DO_HAIRDRESSER_KICK_OFF(HAIRDO_SHOP_STRUCT &sData)
	
	IF DOES_ENTITY_EXIST(SHOPWORKER_GET_PED(sData, TRUE))	
		PED_INDEX ped = SHOPWORKER_GET_PED(sData, TRUE)
		IF NOT IS_PED_INJURED(ped)	
			IF GET_SCRIPT_TASK_STATUS(ped,SCRIPT_TASK_COWER) <> PERFORMING_TASK	
				SET_PED_CONFIG_FLAG(ped,PCF_UseKinematicModeWhenStationary,FALSE)
				SET_PED_CONFIG_FLAG(ped,PCF_CowerInsteadOfFlee,TRUE)
				SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(ped,TRUE)
				TASK_COWER(ped)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(ped, FALSE)
				SET_PED_KEEP_TASK(ped,TRUE)	
				SET_PED_AS_NO_LONGER_NEEDED(ped)
			ENDIF
		ENDIF
	ENDIF
	
	IF sData.sShopInfo.bRunEntryIntro
		IF DOES_ENTITY_EXIST(SHOPWORKER_GET_PED(sData, FALSE))	
		AND DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE)	
			PED_INDEX pedKeeper = SHOPWORKER_GET_PED(sData, FALSE)
			IF NOT IS_PED_INJURED(pedKeeper)	
				IF GET_SCRIPT_TASK_STATUS(pedKeeper,SCRIPT_TASK_REACT_AND_FLEE_PED) <> PERFORMING_TASK	
				AND GET_SCRIPT_TASK_STATUS(pedKeeper,SCRIPT_TASK_COWER) <> PERFORMING_TASK	
					SET_PED_CONFIG_FLAG(pedKeeper,PCF_UseKinematicModeWhenStationary,FALSE)
					SET_PED_CONFIG_FLAG(pedKeeper,PCF_CowerInsteadOfFlee,TRUE)
					SET_PED_CAN_RAGDOLL_FROM_PLAYER_IMPACT(pedKeeper,TRUE)
					//TASK_REACT_AND_FLEE_PED(pedKeeper,PLAYER_PED_ID())
					TASK_COWER(pedKeeper,-1)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedKeeper, FALSE)
					SET_PED_KEEP_TASK(pedKeeper,TRUE)	
					SET_PED_AS_NO_LONGER_NEEDED(pedKeeper)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	IF DOES_ENTITY_EXIST(sData.sShopInfo.sShopCustomer.pedID)	
		IF NOT IS_PED_INJURED(sData.sShopInfo.sShopCustomer.pedID)
			IF NOT IS_ENTITY_PLAYING_ANIM(sData.sShopInfo.sShopCustomer.pedID,"amb@prop_human_seat_chair@male@generic@react_cowering","idle_front")	
				IF HAS_ANIM_DICT_LOADED("amb@prop_human_seat_chair@male@generic@react_cowering")	
					VECTOR vAnimPos = GET_ENTITY_COORDS(sData.sShopInfo.sShopCustomer.pedID)
					FLOAT fZOffset
					IF IS_SALON_ACTIVE(sData)
						fZOffset = 0.32
					ELSE
						fZOffset = 0.2
					ENDIF
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sData.sShopInfo.sShopCustomer.pedID,TRUE)
					TASK_LOOK_AT_ENTITY(sData.sShopInfo.sShopCustomer.pedID,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV)
					TASK_PLAY_ANIM_ADVANCED(sData.sShopInfo.sShopCustomer.pedID,"amb@prop_human_seat_chair@male@generic@react_cowering","idle_front",
					<<vAnimPos.x,vAnimPos.y,vAnimPos.z - fZOffset>>, 
					<<0,0,GET_ENTITY_HEADING(sData.sShopInfo.sShopCustomer.pedID)>>, 4,-2,-1, AF_LOOPING)
					SET_PED_KEEP_TASK(sData.sShopInfo.sShopCustomer.pedID,TRUE)	
					SET_PED_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopCustomer.pedID)
				ENDIF
			ENDIF
		ENDIF
	ENDIF

ENDPROC

// -----------------------INTROS ----------------------------------------------------------

/// PURPOSE:
///    Checks if the intro has been skipped.
///    If so, starts the screen fading and updates skipped bool 
/// RETURNS:
///    TRUE if intro skipped, FALSE otherwise
FUNC BOOL HANDLE_INTRO_BEING_SKIPPED(HAIRDO_SHOP_STRUCT &sData)
	IF sData.bIntroSkipped = FALSE
		IF SKIP_SHOP_CUTSCENE()
			DO_SCREEN_FADE_OUT(500)
			sData.bIntroSkipped = TRUE
			RETURN TRUE
		ENDIF
	ENDIF
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Called by both entry intros
///    Sets up intro data for 1st shot
///    Gets player and camera ready
PROC DO_COMMON_ENTRY_INTRO_SETUP(HAIRDO_SHOP_STRUCT &sData)
	//PRE_INTRO_CUTSCENE_START()
	REMOVE_PED_HELMET(PLAYER_PED_ID(),TRUE)
	
	IF IS_ANY_VEHICLE_NEAR_POINT(sData.sLocateInfo.vPlayerBrowsePos,3)	
	AND NOT g_bInMultiplayer	
		REPOSITION_PLAYER_LAST_VEHICLE_FOR_SHOP(sData.sShopInfo)
	ENDIF
	
	START_SHOP_CUTSCENE(sData.sShopInfo)
				
	// set up intro data for 1st shot: (overview of shop)
	GET_HAIRDO_SHOP_ENTRY_INTRO_DATA(sData.sShopInfo.eShop, sData.sEntryInfo)
		
	// load the logo for this shop
	LOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
	
	// Hide our buddies
	sData.sBuddyHideData.buddyHide = TRUE
	sData.sBuddyHideData.buddyInit = FALSE
	
	UPDATE_BUDDIES_FOR_SHOP_CUTSCENE(sData.sBuddyHideData)
	
	// get the player ready for the cut-scene
	CLEAR_PLAYER_WANTED_LEVEL(PLAYER_ID())
	SET_PLAYER_CONTROL(PLAYER_ID(), FALSE)
	CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
	SET_ENTITY_COORDS(PLAYER_PED_ID(), sData.sEntryInfo.vPlayerCoords[0])
	SET_ENTITY_HEADING(PLAYER_PED_ID(), sData.sEntryInfo.fPlayerHead[0])
	SET_CURRENT_PED_WEAPON(PLAYER_PED_ID(), WEAPONTYPE_UNARMED, TRUE)
	FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK)
	
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	// Did we have a helmet? If so reset it. 
	//POST_INTRO_CUTSCENE_START()
	
	// camera
	UPDATE_CAMERA_ANIM(sData, GET_CAMERA_ANIM(sData, HSC_TUTORIAL, ""))
ENDPROC

/// PURPOSE:
///    Called at end of both entry intros
///    Cleans up camera, puts player in position
///    removes help and intro assets, fades back in etc
PROC DO_COMMON_ENTRY_INTRO_CLEANUP(HAIRDO_SHOP_STRUCT &sData)

	// put player in position
	CLEAR_PED_TASKS(PLAYER_PED_ID())
	SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(),FALSE)
	FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
	IF IS_SCREEN_FADED_OUT() 
		// only move the player if cut-scene was skipped (just set the synced scene phase to 1 instead)
		//SET_ENTITY_COORDS(PLAYER_PED_ID(), sData.sEntryInfo.vPlayerCoords[1])
		//SET_ENTITY_HEADING(PLAYER_PED_ID(), sData.sEntryInfo.fPlayerHead[1])
	ENDIF

	// clean up the camera
	IF DOES_CAM_EXIST(sData.sEntryInfo.camID)
		IF IS_CAM_ACTIVE(sData.sEntryInfo.camID)
			SET_CAM_ACTIVE(sData.sEntryInfo.camID, FALSE)
		ENDIF
		DESTROY_CAM(sData.sEntryInfo.camID)
	ENDIF

	// barbers intro ends with a catch up camera
	IF NOT IS_SCREEN_FADED_OUT() 
		CPRINTLN(DEBUG_SHOPS, "Doing catch up camera for end of intro.")
		STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
	ELSE
		RENDER_SCRIPT_CAMS(FALSE, FALSE)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0)
	ENDIF
	
	// Return to gameplay settings
	END_SHOP_CUTSCENE()
	
	// Remove shop help message and assets
	CLEAR_HELP()
	REMOVE_SHOP_HELP()
	UNLOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
	
	// Ensure screen fades back in
	IF NOT IS_SCREEN_FADED_IN()
		WAIT(0) // wait a frame to avoid popping
		IF NOT IS_SCREEN_FADING_IN()
			PRINT_SHOP_DEBUG_LINE("END_SHOP_CUTSCENE - fading in screen")
			DO_SCREEN_FADE_IN(500)
		ENDIF
	ENDIF

	// Intro complete so set all hairdo shop intros complete
	INT iShop
	REPEAT NUMBER_OF_SHOPS iShop
		IF GET_SHOP_TYPE_ENUM(INT_TO_ENUM(SHOP_NAME_ENUM, iShop)) = SHOP_TYPE_HAIRDO
			SET_SHOP_HAS_RUN_ENTRY_INTRO(INT_TO_ENUM(SHOP_NAME_ENUM, iShop), TRUE)
		ENDIF
	ENDREPEAT
	sData.sShopInfo.bRunEntryIntro = FALSE
	
	// Stop hiding our buddies
	sData.sBuddyHideData.buddyHide = FALSE

	// Dont do auto trigger this time.
	sData.bDoInstantLocateTrigger = FALSE
	
	//  reset intro skipped variable
	sData.bIntroSkipped = FALSE
	
	// return player control
	SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
ENDPROC

/// PURPOSE:
///    Skips part of the door opening sync scene based on how far open the door already is
/// PARAMS:
///    fDorrFullyOpenPhase - the phase at which the door is fully open in the synced scene
///    bSalon - is this shop the salon?
///    sData- hairdo shop struct
PROC SET_DOOR_ANIM_PHASE(FLOAT fDoorFullyOpenPhase, BOOL bSalon, HAIRDO_SHOP_STRUCT &sData)

	SHOP_NAME_ENUM eBaseShop
	MODEL_NAMES eDoorModel
	FLOAT fDoorAngle
	BOOL bDoorLock
	
	IF bSalon = TRUE
		// salon
		eBaseShop = HAIRDO_SHOP_01_BH
		eDoorModel = V_ILEV_HD_DOOR_L
	ELSE
		//barbers
		eBaseShop = HAIRDO_SHOP_02_SC
		eDoorModel = V_ILEV_BS_DOOR
	ENDIF

	IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(GET_SHOP_COORDS(sData.sShopInfo.eShop), 20.0, eDoorModel)
		GET_STATE_OF_CLOSEST_DOOR_OF_TYPE(eDoorModel, GET_HAIRDO_SHOP_DOOR_POS(eBaseShop,  sData.sShopInfo.eShop), bDoorLock, fDoorAngle)
		CPRINTLN(DEBUG_SHOPS, "DO_BARBERS_ENTRY_INTRO: door found at co-ords: angle = ", fDoorAngle)
	ENDIF
	
	// Salon has 2 doors. use whichever is most open
	IF bSalon = TRUE
		FLOAT fDoorAngle2
		eDoorModel = V_ILEV_HD_DOOR_R
		IF DOES_OBJECT_OF_TYPE_EXIST_AT_COORDS(GET_SHOP_COORDS(sData.sShopInfo.eShop), 20.0, eDoorModel)
			GET_STATE_OF_CLOSEST_DOOR_OF_TYPE(eDoorModel, GET_HAIRDO_SHOP_DOOR_POS(eBaseShop,  sData.sShopInfo.eShop), bDoorLock, fDoorAngle2)
			CPRINTLN(DEBUG_SHOPS, "DO_BARBERS_ENTRY_INTRO: door found at co-ords: angle = ", fDoorAngle2)
		ENDIF
		IF fDoorAngle2 > fDoorAngle
			fDoorAngle = fDoorAngle2
		ENDIF
	ENDIF
	
	// skip part of sync scene based on door angle
	CPRINTLN(DEBUG_SHOPS, "DO_BARBERS_ENTRY_INTRO: fDoorAngle = ", fDoorAngle)
	FLOAT fSyncScenePhase
	fSyncScenePhase = fDoorFullyOpenPhase// door fully open at this point
	fSyncScenePhase = fSyncScenePhase * fDoorAngle
	SET_SYNCHRONIZED_SCENE_PHASE(sData.iSyncScenePlayer, fSyncScenePhase)
ENDPROC

/// PURPOSE:
///    Handles the shop intro for the salon
PROC DO_SALON_ENTRY_INTRO(HAIRDO_SHOP_STRUCT &sData)
	VECTOR vAnimPos, vAnimRot
	INT iCleanupStage = 4 // if extra camera shots get added, update this so skipping works
	SEQUENCE_INDEX mSequence
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		EXIT
	ENDIF
	
	IF IS_REPLAY_BEING_PROCESSED()
	OR IS_PLAYER_KICKING_OFF_IN_SHOP(sData.sShopInfo.eShop)
	//OR NOT IS_PLAYER_OK_FOR_SHOP_INTRO(sData.sShopInfo)	
	OR VDIST(sData.sShopInfo.sShopKeeper.vPosIdle,GET_ENTITY_COORDS(PLAYER_PED_ID())) > 40.0	//For 912326 (and other bugs like it)
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		IF DOES_CAM_EXIST(sData.sShopInfo.sCameras[0].camID)
			DESTROY_CAM(sData.sShopInfo.sCameras[0].camID)
		ENDIF
		IF DOES_CAM_EXIST(sData.sShopInfo.sCameras[1].camID)
			DESTROY_CAM(sData.sShopInfo.sCameras[1].camID)
		ENDIF
		EXIT
	ENDIF
	
	SWITCH sData.sEntryInfo.iStage
	
		// -----------Set up initial cams and anims-----------------------------
		CASE 0
			// do common set up
			DO_COMMON_ENTRY_INTRO_SETUP(sData)
			
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			
//			IF HAS_MODEL_LOADED(V_ILEV_HD_DOOR_L)
//				CREATE_MODEL_HIDE(GET_SHOP_COORDS(sData.sShopInfo.eShop), 20.0, V_ILEV_HD_DOOR_L, FALSE)
//				sData.mDoorL = CREATE_OBJECT(V_ILEV_HD_DOOR_L, GET_SHOP_COORDS(sData.sShopInfo.eShop))
//			ENDIF
//			IF HAS_MODEL_LOADED(V_ILEV_HD_DOOR_R)
//				CREATE_MODEL_HIDE(GET_SHOP_COORDS(sData.sShopInfo.eShop), 20.0, V_ILEV_HD_DOOR_R, FALSE)
//				sData.mDoorR = CREATE_OBJECT(V_ILEV_HD_DOOR_R, GET_SHOP_COORDS(sData.sShopInfo.eShop))
//			ENDIF
			
			// -----give everyone their anims------
			// Set up the player and door sync scene
			GET_HAIRDO_SHOP_ANIM_POSITION(sData.sShopInfo.eShop, vAnimPos, vAnimRot)
			sData.iSyncScenePlayer = CREATE_SYNCHRONIZED_SCENE(vAnimPos, vAnimRot)
			SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(),TRUE)
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sData.iSyncScenePlayer, sData.sHairdoShopAnimDict, "player_intro", INSTANT_BLEND_IN, -1, SYNCED_SCENE_NONE)
//			IF DOES_ENTITY_EXIST(sData.mDoorL)
//				PLAY_SYNCHRONIZED_ENTITY_ANIM(sData.mDoorL, sData.iSyncScenePlayer, "left_door_playerintro", sData.sHairdoShopAnimDict, NORMAL_BLEND_IN)
//			ELSE
//				PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(GET_SHOP_COORDS(sData.sShopInfo.eShop), 20.0, V_ILEV_HD_DOOR_L, sData.iSyncScenePlayer, "left_door_playerintro", sData.sHairdoShopAnimDict, NORMAL_BLEND_IN)
//			ENDIF
//			IF DOES_ENTITY_EXIST(sData.mDoorR)
//				PLAY_SYNCHRONIZED_ENTITY_ANIM(sData.mDoorR, sData.iSyncScenePlayer, "left_door_playerintro", sData.sHairdoShopAnimDict, NORMAL_BLEND_IN)
//			ELSE
//				PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(GET_SHOP_COORDS(sData.sShopInfo.eShop), 20.0, V_ILEV_HD_DOOR_R, sData.iSyncScenePlayer, "right_door_playerintro", sData.sHairdoShopAnimDict, NORMAL_BLEND_IN)
//			ENDIF
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sData.iSyncScenePlayer,FALSE)
			// skip part of sync scene based on door angle
			// not needed at the moment as the door isn't in 1st shot
			//SET_DOOR_ANIM_PHASE(0.6, TRUE, sData)
			
			// get receptionist to play idle anim (not needed she's playing scenario)
			IF DOES_ENTITY_EXIST(sData.sShopReceptionist.pedID)						
				IF NOT IS_PED_INJURED(sData.sShopReceptionist.pedID)
					// TODO!
				ENDIF
			ENDIF

			// Set up the shop keeper to play his anims
			UPDATE_SHOPKEEPER_ANIM(sData, "keeper_intro", TRUE)
				
			// Set up the shop customer
			IF NOT IS_PED_INJURED(sData.sShopInfo.sShopCustomer.pedID)
				// get the go to pos for the customer so he can walk out of shop
				GET_HAIRDO_SHOP_PLAYER_GO_TO_POS(sData.sShopInfo.eShop, sData.sLocateInfo.vPlayerGoToPos)
				
				SET_FORCE_FOOTSTEP_UPDATE(sData.sShopInfo.sShopCustomer.pedID,TRUE)
				CLEAR_PED_TASKS(sData.sShopInfo.sShopCustomer.pedID)
				OPEN_SEQUENCE_TASK(mSequence)
					sData.sCurrentCustomerAnim = "customer_intro"
					TASK_PLAY_ANIM_ADVANCED(NULL, sData.sHairdoShopAnimDict, sData.sCurrentCustomerAnim, vAnimPos, vAnimRot, INSTANT_BLEND_IN, SLOW_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS|AF_TURN_OFF_COLLISION)
				CLOSE_SEQUENCE_TASK(mSequence)
				TASK_PERFORM_SEQUENCE(sData.sShopInfo.sShopCustomer.pedID, mSequence)
				
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sData.sShopInfo.sShopCustomer.pedID)
				CLEAR_SEQUENCE_TASK(mSequence)
				
				ADD_PED_FOR_DIALOGUE(sData.sDialogueInfo.cultresConversation, ConvertSingleCharacter("F"), sData.sShopInfo.sShopCustomer.pedID, "HairCustomer")

			ENDIF
			
			// update cut-scene data
			sData.sBrowseInfo.sInfoLabel = "SHOP_INTRO"
			sData.sBrowseInfo.iBrowseTimer = GET_GAME_TIMER()
			sData.sEntryInfo.iStage++
		BREAK

		//------------------overview of shop----------------------------------
		CASE 1
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			IF HANDLE_INTRO_BEING_SKIPPED(sData)
				sData.sEntryInfo.iStage = iCleanupStage
			ENDIF
			
			// Display the help
			IF LOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
				DISPLAY_SHOP_INTRO_MESSAGE(sData.sShopInfo.eShop, sData.sBrowseInfo.sInfoLabel)
			ENDIF
		
			#if USE_CLF_DLC
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (GET_GAME_TIMER() - sData.sBrowseInfo.iBrowseTimer) >= 500
					CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(sData.sDialogueInfo.cultresConversation, "hairdau", "haird_hbye", CONV_PRIORITY_AMBIENT_HIGH, g_eShopSubtitleState)
				ENDIF
			#ENDIF
			#if not USE_SP_DLC
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (GET_GAME_TIMER() - sData.sBrowseInfo.iBrowseTimer) >= 500
					CREATE_CONVERSATION(sData.sDialogueInfo.cultresConversation, "hairdau", "haird_hbye", CONV_PRIORITY_AMBIENT_HIGH, g_eShopSubtitleState)
				ENDIF
			#ENDIF
			
			IF (GET_GAME_TIMER() - sData.sBrowseInfo.iBrowseTimer) >= 4700 //5000	
				// once shot is done, move to next one
				
				// update cut-scene data
				sData.sBrowseInfo.sInfoLabel = "HAIR_INTRO_0"
				sData.sBrowseInfo.iBrowseTimer = GET_GAME_TIMER()
				sData.sEntryInfo.iStage++
			ENDIF
		BREAK
		
		///	----------------- close up of chair----------------------------------------
		CASE 2
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			IF HANDLE_INTRO_BEING_SKIPPED(sData)
				sData.sEntryInfo.iStage = iCleanupStage
			ENDIF
			
			// Display the help
			IF LOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
				DISPLAY_SHOP_INTRO_MESSAGE(sData.sShopInfo.eShop, sData.sBrowseInfo.sInfoLabel)
			ENDIF
			
			IF (GET_GAME_TIMER() - sData.sBrowseInfo.iBrowseTimer) >= 1100	
				// once shot is done, move to next one
				
				// update cut-scene data
				sData.sBrowseInfo.iBrowseTimer = GET_GAME_TIMER()
				sData.sEntryInfo.iStage++
			ENDIF
		BREAK
		
		///	---------------close up of hairdresser-------------------------------
		CASE 3
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			IF HANDLE_INTRO_BEING_SKIPPED(sData)
				sData.sEntryInfo.iStage = iCleanupStage
			ENDIF
			
			// Display the help
			IF LOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
				DISPLAY_SHOP_INTRO_MESSAGE(sData.sShopInfo.eShop, sData.sBrowseInfo.sInfoLabel)
				
				IF (GET_GAME_TIMER() - sData.sBrowseInfo.iBrowseTimer) >= 4000
					// wait before updating the label, so player has time to read
					sData.sBrowseInfo.sInfoLabel = "HAIR_INTRO_1"
				ENDIF
			ENDIF
		
			IF GET_CAM_ANIM_CURRENT_PHASE(sData.sShopInfo.sCameras[0].camID) >= 0.883
				// once shot is done, cut-scene is finished, clean-up
				sData.sEntryInfo.iStage = iCleanupStage
			ENDIF
			
			IF GET_CAM_ANIM_CURRENT_PHASE(sData.sShopInfo.sCameras[0].camID) >= 0.734
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncScenePlayer)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_SYNCHRONIZED_SCENE) = PERFORMING_TASK		
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF
				ENDIF
			ENDIF
			
		BREAK
		
		//-------------------- cleanup-------------------------------------
		CASE 4
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
		
			// if intro was skipped, wait for screen to fade out
			IF sData.bIntroSkipped = TRUE
				sData.sBrowseInfo.sInfoLabel = ""
				REMOVE_SHOP_HELP()
				IF IS_HELP_MESSAGE_BEING_DISPLAYED()
				CLEAR_HELP()
				ENDIF
				IF NOT IS_SCREEN_FADED_OUT()
					IF NOT IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_OUT(500)
					ENDIF
					EXIT
				ENDIF
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncScenePlayer)
					//SET_SYNCHRONIZED_SCENE_PHASE(sData.iSyncScenePlayer,1)
				ENDIF
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncSceneShopkeeper)
					//SET_SYNCHRONIZED_SCENE_PHASE(sData.iSyncSceneShopkeeper,1)
				ENDIF
				SAFE_TELEPORT_ENTITY(PLAYER_PED_ID(),<<-820.9124, -187.2931, 36.5689>>, 319.5678)
				CLEAR_PED_TASKS(PLAYER_PED_ID())
			ENDIF
			
			// Stop player and door anims
//			IF DOES_ENTITY_EXIST(sData.mDoorL)
//				STOP_SYNCHRONIZED_ENTITY_ANIM(sData.mDoorL, INSTANT_BLEND_OUT, TRUE)
//				DELETE_OBJECT(sData.mDoorL)
//				REMOVE_MODEL_HIDE(GET_SHOP_COORDS(sData.sShopInfo.eShop), 20.0, V_ILEV_HD_DOOR_L)
//				SET_MODEL_AS_NO_LONGER_NEEDED(V_ILEV_HD_DOOR_L)
//			ELSE
//				STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(GET_SHOP_COORDS(sData.sShopInfo.eShop), 20.0, V_ILEV_HD_DOOR_L, INSTANT_BLEND_OUT)
//			ENDIF
//			IF DOES_ENTITY_EXIST(sData.mDoorR)
//				STOP_SYNCHRONIZED_ENTITY_ANIM(sData.mDoorR, INSTANT_BLEND_OUT, TRUE)
//				DELETE_OBJECT(sData.mDoorR)
//				REMOVE_MODEL_HIDE(GET_SHOP_COORDS(sData.sShopInfo.eShop), 20.0, V_ILEV_HD_DOOR_R)
//				SET_MODEL_AS_NO_LONGER_NEEDED(V_ILEV_HD_DOOR_R)
//			ELSE
//				STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(GET_SHOP_COORDS(sData.sShopInfo.eShop), 20.0, V_ILEV_HD_DOOR_R, INSTANT_BLEND_OUT)
//			ENDIF
			
			// sort out the shopkeeper
			IF NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID) 
			
				IF IS_SCREEN_FADED_OUT() 
					// If we skipped, teleport him into position
					//SET_ENTITY_COORDS(sData.sShopInfo.sShopKeeper.pedID, sData.sShopInfo.sShopKeeper.vPosIdle)
					//SET_ENTITY_HEADING(sData.sShopInfo.sShopKeeper.pedID, sData.sShopInfo.sShopKeeper.fHeadIdle)
					CREATE_SCISSORS(sData,FALSE) // delete the scissors
				ENDIF
				CLEAR_PED_TASKS(sData.sShopInfo.sShopKeeper.pedID)
				SHOPWORKER_FACE_PLAYER(sData, FALSE)
				sData.sCurrentKeeperAnim = ""
			ENDIF
	
			// Fix up the shop customer
			IF DOES_ENTITY_EXIST(sData.sShopInfo.sShopCustomer.pedID)
			AND NOT IS_PED_INJURED(sData.sShopInfo.sShopCustomer.pedID)
				IF IS_SCREEN_FADED_OUT() 
					// if we skip, get rid of the ped. 
					DELETE_PED(sData.sShopInfo.sShopCustomer.pedID)
				ELSE
				
					CLEAR_PED_TASKS(sData.sShopInfo.sShopCustomer.pedID)
					FORCE_PED_MOTION_STATE(sData.sShopInfo.sShopCustomer.pedID, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					SET_PED_MIN_MOVE_BLEND_RATIO(sData.sShopInfo.sShopCustomer.pedID, PEDMOVE_WALK)
					
					SET_ENTITY_COLLISION(sData.sShopInfo.sShopCustomer.pedID, TRUE)
					
					OPEN_SEQUENCE_TASK(mSequence)
						TASK_GO_STRAIGHT_TO_COORD(NULL, sData.sLocateInfo.vPlayerGoToPos, PEDMOVE_WALK, -1)
						TASK_WANDER_STANDARD(NULL)
					CLOSE_SEQUENCE_TASK(mSequence)
					TASK_PERFORM_SEQUENCE(sData.sShopInfo.sShopCustomer.pedID, mSequence)
					
					CLEAR_SEQUENCE_TASK(mSequence)
				
					SET_PED_KEEP_TASK(sData.sShopInfo.sShopCustomer.pedID, TRUE)
					SET_PED_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopCustomer.pedID)
				ENDIF
				sData.sCurrentCustomerAnim = ""
			ENDIF
			
			DO_COMMON_ENTRY_INTRO_CLEANUP(sData)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Handles the shop intro for the barber shop
PROC DO_BARBERS_ENTRY_INTRO(HAIRDO_SHOP_STRUCT &sData)
	
	VECTOR vAnimPos, vAnimRot
	INT iCleanupStage = 6 // if extra camera shots get added, update this so skipping works
	SEQUENCE_INDEX mSequence
	
	IF IS_PLAYER_SWITCH_IN_PROGRESS()
		EXIT
	ENDIF
	
	IF IS_REPLAY_BEING_PROCESSED()
	OR IS_PLAYER_KICKING_OFF_IN_SHOP(sData.sShopInfo.eShop)
	//OR NOT IS_PLAYER_OK_FOR_SHOP_INTRO(sData.sShopInfo)	
	OR VDIST(sData.sShopInfo.sShopKeeper.vPosIdle,GET_ENTITY_COORDS(PLAYER_PED_ID())) > 30.0   //For 912326 (and other bugs like it)
		SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
		IF DOES_CAM_EXIST(sData.sShopInfo.sCameras[0].camID)
			DESTROY_CAM(sData.sShopInfo.sCameras[0].camID)
		ENDIF
		IF DOES_CAM_EXIST(sData.sShopInfo.sCameras[1].camID)
			DESTROY_CAM(sData.sShopInfo.sCameras[1].camID)
		ENDIF
		EXIT
	ENDIF

	SWITCH sData.sEntryInfo.iStage
	
		// -----------Set up initial cams and anims-----------------------------
		CASE 0
			// do common set up
			DO_COMMON_ENTRY_INTRO_SETUP(sData)
			
			SET_ALL_RANDOM_PEDS_FLEE_THIS_FRAME(PLAYER_ID())
			
			// -----give everyone their anims------		
			GET_HAIRDO_SHOP_ANIM_POSITION(sData.sShopInfo.eShop, vAnimPos, vAnimRot)
			
			// Set up the shop keeper to play anims
			UPDATE_SHOPKEEPER_ANIM(sData, "keeper_tutorial", TRUE,FALSE,FALSE,-1)
		
			GET_HAIRDO_SHOP_ANIM_POSITION(sData.sShopInfo.eShop, vAnimPos, vAnimRot)
			sData.iSyncScenePlayer = CREATE_SYNCHRONIZED_SCENE(vAnimPos, vAnimRot)
			SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(),TRUE)
			TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sData.iSyncScenePlayer, sData.sHairdoShopAnimDict, "player_intro", INSTANT_BLEND_IN, -1, SYNCED_SCENE_NONE, RBF_NONE)
			SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sData.iSyncScenePlayer, FALSE)

			// Set up the shop customer
			IF NOT IS_PED_INJURED(sData.sShopInfo.sShopCustomer.pedID)
				CLEAR_PED_TASKS(sData.sShopInfo.sShopCustomer.pedID)
				SET_FORCE_FOOTSTEP_UPDATE(sData.sShopInfo.sShopCustomer.pedID,TRUE)   //Needs to be synced scene so it's properly in sync with door anim
				TASK_SYNCHRONIZED_SCENE(sData.sShopInfo.sShopCustomer.pedID, sData.iSyncSceneShopkeeper, sData.sHairdoShopAnimDict, "customer_tutorial", INSTANT_BLEND_IN, -2, SYNCED_SCENE_NONE, RBF_NONE)
				//TASK_PLAY_ANIM_ADVANCED(sData.sShopInfo.sShopCustomer.pedID, sData.sHairdoShopAnimDict, "customer_tutorial", vAnimPos, vAnimRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS|AF_TURN_OFF_COLLISION)
				sData.sCurrentCustomerAnim = "customer_tutorial"
				
				ADD_PED_FOR_DIALOGUE(sData.sDialogueInfo.cultresConversation, ConvertSingleCharacter("F"), sData.sShopInfo.sShopCustomer.pedID, "HairCustomer")
			ENDIF
			
			PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(GET_SHOP_COORDS(sData.sShopInfo.eShop), 20.0, V_ILEV_BS_DOOR, sData.iSyncSceneShopkeeper, "DOOR_TUTORIAL", sData.sHairdoShopAnimDict, NORMAL_BLEND_IN)	
			
			// update cut-scene data
			sData.sBrowseInfo.sInfoLabel = "SHOP_INTRO"
			sData.sBrowseInfo.iBrowseTimer = GET_GAME_TIMER()
			sData.sEntryInfo.iStage++
			CPRINTLN(DEBUG_SHOPS, "DO_BARBERS_ENTRY_INTRO 1")
		BREAK
		
		
		//----------------- Shot of customer getting hair-cut----------------------------------
		CASE 1
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			IF HANDLE_INTRO_BEING_SKIPPED(sData)
				sData.sEntryInfo.iStage = iCleanupStage
			ENDIF
			
			// Display the help
			IF LOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
				DISPLAY_SHOP_INTRO_MESSAGE(sData.sShopInfo.eShop, sData.sBrowseInfo.sInfoLabel)
			ENDIF
					
			#if USE_CLF_DLC
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (GET_GAME_TIMER() - sData.sBrowseInfo.iBrowseTimer) >= 1000
					CREATE_CONVERSATION_USING_V_CONTENT_IN_DLC(sData.sDialogueInfo.cultresConversation, "hairdau", "haird_bbye", CONV_PRIORITY_AMBIENT_HIGH, g_eShopSubtitleState)
				ENDIF
			#ENDIF
			#if not USE_SP_DLC
				IF NOT IS_ANY_CONVERSATION_ONGOING_OR_QUEUED()
				AND (GET_GAME_TIMER() - sData.sBrowseInfo.iBrowseTimer) >= 1000
					CREATE_CONVERSATION(sData.sDialogueInfo.cultresConversation, "hairdau", "haird_bbye", CONV_PRIORITY_AMBIENT_HIGH, g_eShopSubtitleState)
				ENDIF
			#ENDIF
			
			IF (GET_GAME_TIMER() - sData.sBrowseInfo.iBrowseTimer) >= 4200//4500	
				// once shot is done, move to next one
				
				// Set up the player and door sync scene
				/*
				GET_HAIRDO_SHOP_ANIM_POSITION(sData.sShopInfo.eShop, vAnimPos, vAnimRot)
				sData.iSyncScenePlayer = CREATE_SYNCHRONIZED_SCENE(vAnimPos, vAnimRot)
				SET_FORCE_FOOTSTEP_UPDATE(PLAYER_PED_ID(),TRUE)
				TASK_SYNCHRONIZED_SCENE(PLAYER_PED_ID(), sData.iSyncScenePlayer, sData.sHairdoShopAnimDict, "player_intro", NORMAL_BLEND_IN, NORMAL_BLEND_OUT, SYNCED_SCENE_NONE, RBF_NONE)
				PLAY_SYNCHRONIZED_MAP_ENTITY_ANIM(GET_SHOP_COORDS(sData.sShopInfo.eShop), 20.0, V_ILEV_BS_DOOR, sData.iSyncScenePlayer, "DOOR_TUTORIAL", sData.sHairdoShopAnimDict, NORMAL_BLEND_IN)	
				SET_SYNCHRONIZED_SCENE_HOLD_LAST_FRAME(sData.iSyncScenePlayer, FALSE)
				*/
				
				// skip part of sync scene based on door angle
				//SET_DOOR_ANIM_PHASE(0.6, FALSE, sData)

				// update cut-scene data
				sData.sBrowseInfo.sInfoLabel = "HAIR_INTRO_0"
				sData.sBrowseInfo.iBrowseTimer = GET_GAME_TIMER()
				sData.sEntryInfo.iStage++
				
				CPRINTLN(DEBUG_SHOPS, "DO_BARBERS_ENTRY_INTRO 2")
			ENDIF
		BREAK
		
		///	-----------------  Close up of barber / customer----------------------------------------
		CASE 2
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			IF HANDLE_INTRO_BEING_SKIPPED(sData)
				sData.sEntryInfo.iStage = iCleanupStage
			ENDIF
			
			// Display the help
			IF LOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
				DISPLAY_SHOP_INTRO_MESSAGE(sData.sShopInfo.eShop, sData.sBrowseInfo.sInfoLabel)
			ENDIF
			
			// once shot is done, move to next one
			IF (GET_GAME_TIMER() - sData.sBrowseInfo.iBrowseTimer) >= 1100	
				
				// update cut-scene data
				sData.sBrowseInfo.iBrowseTimer = GET_GAME_TIMER()
				sData.sEntryInfo.iStage++
				
				CPRINTLN(DEBUG_SHOPS, "DO_BARBERS_ENTRY_INTRO 3")
			ENDIF
		BREAK

		///	---------------Behind barber, customer checks hair-------------------------------
		CASE 3
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			IF HANDLE_INTRO_BEING_SKIPPED(sData)
				sData.sEntryInfo.iStage = iCleanupStage
			ENDIF
			
			// Display the help
			IF LOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
				DISPLAY_SHOP_INTRO_MESSAGE(sData.sShopInfo.eShop, sData.sBrowseInfo.sInfoLabel)
				
				IF (GET_GAME_TIMER() - sData.sBrowseInfo.iBrowseTimer) >= 4000
					// wait before updating the label, so player has time to read
					sData.sBrowseInfo.sInfoLabel = "HAIR_INTRO_1"
				ENDIF
			ENDIF
			
			IF GET_CAM_ANIM_CURRENT_PHASE(sData.sShopInfo.sCameras[0].camID) >= 0.752500//0.752531//0.911//0.7 //0.74//0.87//0.83
			//IF GET_SYNCHRONIZED_SCENE_PHASE(sData.iSyncScenePlayer) >= 0.99//0.83
				//SET_CAM_ANIM_CURRENT_PHASE(sData.sShopInfo.sCameras[0].camID,1)
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncScenePlayer)
					//SET_SYNCHRONIZED_SCENE_PHASE(sData.iSyncScenePlayer,1)
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
						CLEAR_PED_TASKS(PLAYER_PED_ID())
					ENDIF
					//SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				ENDIF
				sData.sEntryInfo.iStage++
				CPRINTLN(DEBUG_SHOPS, "DO_BARBERS_ENTRY_INTRO 4")
			ENDIF
		BREAK
		
		///	---------------Last shot-------------------------------
		CASE 4
			
			GET_HAIRDO_SHOP_ENTRY_INTRO_DATA(sData.sShopInfo.eShop, sData.sEntryInfo)
			/*
			IF NOT DOES_CAM_EXIST(sData.sShopInfo.sCameras[1].camID)
				sData.sShopInfo.sCameras[1].camID = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
				SET_CAM_COORD(sData.sShopInfo.sCameras[1].camID,sData.sEntryInfo.vCamCoord[0])
				SET_CAM_ROT(sData.sShopInfo.sCameras[1].camID,sData.sEntryInfo.vCamRot[0])
				SET_CAM_FOV(sData.sShopInfo.sCameras[1].camID,sData.sEntryInfo.fCamFov[0])
				SET_CAM_ACTIVE(sData.sShopInfo.sCameras[1].camID,TRUE)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
			ENDIF
			
			IF DOES_CAM_EXIST(sData.sShopInfo.sCameras[0].camID)	
				DESTROY_CAM(sData.sShopInfo.sCameras[0].camID)
			ENDIF
			*/
			/*
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncScenePlayer)
				SET_SYNCHRONIZED_SCENE_PHASE(sData.iSyncScenePlayer,1)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
					CLEAR_PED_TASKS(PLAYER_PED_ID())
				ENDIF
				//SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
			ENDIF
			*/
			sData.sEntryInfo.iStage++
			
		BREAK
		
		///	---------------Leadout-------------------------------
		CASE 5
			/*
			IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncSceneShopkeeper)	
			OR GET_SYNCHRONIZED_SCENE_PHASE(sData.iSyncSceneShopkeeper) >= 0.8
				IF IS_CAM_ACTIVE(sData.sShopInfo.sCameras[1].camID)
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
					//IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					//	SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
					//ENDIF
				ENDIF
			ENDIF
			*/
			//IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncSceneShopkeeper)	
			//OR GET_SYNCHRONIZED_SCENE_PHASE(sData.iSyncSceneShopkeeper) >= 0.87
			IF GET_CAM_ANIM_CURRENT_PHASE(sData.sShopInfo.sCameras[0].camID) >= 0.792
				IF NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
				AND NOT IS_PED_HEADTRACKING_PED(sData.sShopInfo.sShopKeeper.pedID,PLAYER_PED_ID())
					TASK_LOOK_AT_ENTITY(sData.sShopInfo.sShopKeeper.pedID,PLAYER_PED_ID(),-1,SLF_WHILE_NOT_IN_FOV | SLF_WIDEST_PITCH_LIMIT | SLF_WIDEST_YAW_LIMIT,SLF_LOOKAT_VERY_HIGH)
				ENDIF
			ENDIF
			
			IF GET_CAM_ANIM_CURRENT_PHASE(sData.sShopInfo.sCameras[0].camID) >= 0.911//0.7 //0.74//0.87//0.83	
				STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP()
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())
					SET_PLAYER_CONTROL(PLAYER_ID(),TRUE)
				ENDIF
				sData.sEntryInfo.iStage = iCleanupStage
			ENDIF
			
		BREAK
		
		//-------------------- cleanup-------------------------------------
		CASE 6
			HIDE_HUD_AND_RADAR_THIS_FRAME()
			DISABLE_CELLPHONE_THIS_FRAME_ONLY()
			
			// if intro was skipped, wait for screen to fade out
			IF sData.bIntroSkipped = TRUE
				sData.sBrowseInfo.sInfoLabel = ""
				IF g_sShopSettings.bDisplayingShopHelp
				AND g_sShopSettings.iShopDisplayingHelp = GET_HASH_KEY(GET_THIS_SCRIPT_NAME())
					CLEAR_HELP()
					g_sShopSettings.bDisplayingShopHelp = FALSE
				ELIF IS_HELP_MESSAGE_BEING_DISPLAYED()
					CLEAR_HELP()
				ENDIF
				IF NOT IS_SCREEN_FADED_OUT()
					IF NOT IS_SCREEN_FADING_OUT()
						DO_SCREEN_FADE_OUT(500)
					ENDIF
					EXIT
				ENDIF
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncSceneShopkeeper)
				STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(GET_SHOP_COORDS(sData.sShopInfo.eShop), 20.0, V_ILEV_BS_DOOR, INSTANT_BLEND_OUT)	
			ENDIF
			
			IF IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncScenePlayer)
				SET_SYNCHRONIZED_SCENE_PHASE(sData.iSyncScenePlayer,1)
			ENDIF
		
			IF IS_SCREEN_FADED_OUT() 
				GET_HAIRDO_SHOP_ENTRY_INTRO_DATA(sData.sShopInfo.eShop, sData.sEntryInfo)
				IF NOT IS_PED_INJURED(PLAYER_PED_ID())	
					SET_ENTITY_COORDS(PLAYER_PED_ID(), sData.sEntryInfo.vPlayerCoords[1])
					SET_ENTITY_HEADING(PLAYER_PED_ID(), sData.sEntryInfo.fPlayerHead[1])
				ENDIF
				IF IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncSceneShopkeeper)
					STOP_SYNCHRONIZED_MAP_ENTITY_ANIM(GET_SHOP_COORDS(sData.sShopInfo.eShop), 20.0, V_ILEV_BS_DOOR, INSTANT_BLEND_OUT)	
					SET_SYNCHRONIZED_SCENE_PHASE(sData.iSyncSceneShopkeeper,1)
				ENDIF
				IF DOES_CAM_EXIST(sData.sShopInfo.sCameras[1].camID)	
					DESTROY_CAM(sData.sShopInfo.sCameras[1].camID)
				ENDIF
				sData.sShopInfo.sCameras[1].camID = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
				SET_CAM_COORD(sData.sShopInfo.sCameras[1].camID,sData.sEntryInfo.vCamCoord[0])
				SET_CAM_ROT(sData.sShopInfo.sCameras[1].camID,sData.sEntryInfo.vCamRot[0])
				SET_CAM_FOV(sData.sShopInfo.sCameras[1].camID,sData.sEntryInfo.fCamFov[0])
				SET_CAM_ACTIVE(sData.sShopInfo.sCameras[1].camID,TRUE)
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
			ENDIF
			
			// sort out the shopkeeper
			IF NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID) 
				CLEAR_PED_TASKS(sData.sShopInfo.sShopKeeper.pedID)
				FORCE_PED_AI_AND_ANIMATION_UPDATE(sData.sShopInfo.sShopKeeper.pedID)
				TASK_TURN_PED_TO_FACE_ENTITY(sData.sShopInfo.sShopKeeper.pedID,PLAYER_PED_ID())
				SET_FORCE_FOOTSTEP_UPDATE(sData.sShopInfo.sShopKeeper.pedID,FALSE)
				//CLEAR_PED_TASKS(sData.sShopInfo.sShopKeeper.pedID)
				IF IS_SCREEN_FADED_OUT() 
					// If we skipped, teleport her into position
					//SET_ENTITY_COORDS(sData.sShopInfo.sShopKeeper.pedID, sData.sShopInfo.sShopKeeper.vPosIdle)
					//SET_ENTITY_HEADING(sData.sShopInfo.sShopKeeper.pedID, sData.sShopInfo.sShopKeeper.fHeadIdle)
					CREATE_SCISSORS(sData,FALSE) // delete the scissors
				ELSE
					// if not skipped, turn to face player
					SHOPWORKER_FACE_PLAYER(sData, FALSE)
				ENDIF
				
				sData.sCurrentKeeperAnim = ""
			ENDIF
	
			// Fix up the shop customer
			IF DOES_ENTITY_EXIST(sData.sShopInfo.sShopCustomer.pedID)
			AND NOT IS_PED_INJURED(sData.sShopInfo.sShopCustomer.pedID)
				IF IS_SCREEN_FADED_OUT() 
					// if we skip, get rid of the ped. 
					DELETE_PED(sData.sShopInfo.sShopCustomer.pedID)
				ELSE
					// get the go to pos for the customer so he can walk out of shop
					GET_HAIRDO_SHOP_PLAYER_GO_TO_POS(sData.sShopInfo.eShop, sData.sLocateInfo.vPlayerGoToPos)
					SET_FORCE_FOOTSTEP_UPDATE(sData.sShopInfo.sShopCustomer.pedID,FALSE)
					CLEAR_PED_TASKS(sData.sShopInfo.sShopCustomer.pedID)
					FORCE_PED_MOTION_STATE(sData.sShopInfo.sShopCustomer.pedID, MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					SET_PED_MIN_MOVE_BLEND_RATIO(sData.sShopInfo.sShopCustomer.pedID, PEDMOVE_WALK)
					
					SET_ENTITY_COLLISION(sData.sShopInfo.sShopCustomer.pedID, TRUE)
					
					OPEN_SEQUENCE_TASK(mSequence)
						TASK_GO_STRAIGHT_TO_COORD(NULL, sData.sLocateInfo.vPlayerGoToPos, PEDMOVE_WALK, -1)
						TASK_WANDER_STANDARD(NULL)
					CLOSE_SEQUENCE_TASK(mSequence)
					TASK_PERFORM_SEQUENCE(sData.sShopInfo.sShopCustomer.pedID, mSequence)
					CLEAR_SEQUENCE_TASK(mSequence)
				
					SET_PED_KEEP_TASK(sData.sShopInfo.sShopCustomer.pedID, TRUE)
					SET_PED_AS_NO_LONGER_NEEDED(sData.sShopInfo.sShopCustomer.pedID)
				ENDIF
				sData.sCurrentCustomerAnim = ""
			ENDIF
			
			DO_COMMON_ENTRY_INTRO_CLEANUP(sData)
		BREAK
	ENDSWITCH
ENDPROC

/// PURPOSE:
///    Calls the appropriate shop intro, and handles some common functionality
PROC DO_ENTRY_INTRO(HAIRDO_SHOP_STRUCT &sData)
			
	IF IS_HAIRDO_SHOP_INTRO_NEEDED(sData)
		IF IS_SHOP_PED_OK()
			
			SET_SHOP_BIT(sData.sShopInfo.eShop, SHOP_NS_FLAG_SHOP_INTRO_RUNNING, TRUE)
		
			// do individual shop entry intro
			IF IS_SALON_ACTIVE(sData)
				DO_SALON_ENTRY_INTRO(sData)
			ELSE
				DO_BARBERS_ENTRY_INTRO(sData)
			ENDIF
			
			// hide shop buddies for this cut-scene
			// buddyHide flag set to false at end of intro, this call then shows the buddies again
			UPDATE_BUDDIES_FOR_SHOP_CUTSCENE(sData.sBuddyHideData)
		ENDIF
	ELSE
		// intro not needed (or has now finished)
		// move on to next shop stage
		CLEAR_SHOP_BIT(sData.sShopInfo.eShop, SHOP_NS_FLAG_SHOP_INTRO_RUNNING, TRUE)
		SET_PLAYER_HAS_VISITED_SHOP(sData.sShopInfo.eShop, TRUE)
		sData.sShopInfo.eStage = SHOP_STAGE_WAIT_FOR_TRIGGER
	ENDIF
ENDPROC

/// PURPOSE: Checks for the player entering the shop
PROC CHECK_HAIRDO_SHOP_ENTRY(HAIRDO_SHOP_STRUCT &sData)
	//If the ped is ok, and no replay and the player is on control. This is needed in case the player enters the shop with the replay screen on. 
	//The CONTROL test is there as there seems to be a gap between the replay screen being finished and the player being moved out of the way. 
	IF (IS_SHOP_PED_OK() AND NOT IS_REPLAY_BEING_PROCESSED() AND IS_PLAYER_CONTROL_ON(PLAYER_ID()))
	OR SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()

		UPDATE_PLAYER_IN_SHOP_STATE(sData.sShopInfo)
	
		IF sData.sShopInfo.bPlayerInRoom = TRUE
			
			//#url:bugstar:1954710 - As I parked the Stromberg near the hairdressers in Hawick, I was warped out of the vehicle and in to the shop
			IF IS_HAIRDO_SHOP_INTRO_NEEDED(sData)
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
			AND IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
				CPRINTLN(DEBUG_SHOPS, "CHECK_HAIRDO_SHOP_ENTRY - halt entry stage as player is in a vehicle")
				EXIT
			ENDIF
			
			
			// Increment visit count
			IF NOT g_bInMultiplayer
			AND NOT sData.bInitialEntryComplete	
				#if USE_CLF_DLC
					g_savedGlobalsClifford.sShopData.iHairdoShopVisits++
				#endif
				#if not USE_CLF_DLC
					g_savedGlobals.sShopData.iHairdoShopVisits++
				#endif
				sData.bInitialEntryComplete = TRUE
			ENDIF
			
			// Allow auto trigger
			sData.sBrowseInfo.bBrowseTrigger = TRUE
			
			sData.sBrowseInfo.iCurrentLocate = -1
			
			sData.sShopInfo.eStage	= SHOP_STAGE_ENTRY_INTRO
			sData.sBrowseInfo.iControl = 0
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Checks to see if the player has left the shop
PROC CHECK_HAIRDO_SHOP_EXIT(HAIRDO_SHOP_STRUCT &sData)
	IF IS_SHOP_PED_OK()
		
		UPDATE_PLAYER_IN_SHOP_STATE(sData.sShopInfo)
		
		IF NOT g_bInMultiplayer	
			SET_PED_CAN_ARM_IK(PLAYER_PED_ID(),TRUE)
			SET_PED_RESET_FLAG(PLAYER_PED_ID(),PRF_SearchForClosestDoor,TRUE) 
		ENDIF
		
		IF NOT sData.sShopInfo.bPlayerInRoom
			IF sData.sShopInfo.iContextID != NEW_CONTEXT_INTENTION
				RELEASE_CONTEXT_INTENTION(sData.sShopInfo.iContextID)
			ENDIF
			IF IS_SHOP_KEEPER_SAFE_TO_USE(PLAYER_ID(), sData.eInShop, TRUE)
			AND sData.sShopInfo.eShop != HAIRDO_SHOP_CASINO_APT
				REMOVE_USE_OF_SHOP_KEEPER(PLAYER_ID(), sData.eInShop)
			ENDIF
			IF IS_SHOP_KEEPER_SAFE_TO_USE_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_ID(), sData.eInShop, FALSE)
			AND sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT
				REMOVE_USE_OF_SHOP_KEEPER_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_ID(), sData.eInShop)
			ENDIF	
			REMOVE_SHOP_HELP()
		ENDIF
		
		IF NOT sData.sShopInfo.bPlayerInShop
			sData.sShopInfo.eStage = SHOP_STAGE_WAIT_FOR_ENTRY
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Loads any assets required in the browse process
///    NOTE: Player should not be allowed to browse any locates until this returns TRUE
FUNC BOOL LOAD_SHOP_BROWSE_ASSETS(HAIRDO_SHOP_STRUCT &sData, BOOL bSkipAudioBankInCasino)
	//Particle effects
	IF NOT sData.bLoadPTFX
		REQUEST_PTFX_ASSET()
		IF NOT HAS_PTFX_ASSET_LOADED()
			RETURN FALSE
		ENDIF
		sData.bLoadPTFX = TRUE
	ENDIF
	
	// Audio
	IF NOT sData.bAudioBankLoaded
		
		IF bSkipAudioBankInCasino
		AND sData.eInShop = HAIRDO_SHOP_CASINO_APT
			RETURN TRUE
		ENDIF
		
		IF NOT REQUEST_AMBIENT_AUDIO_BANK(sData.sAudioBank)
			RETURN FALSE
		ENDIF
		sData.bAudioBankLoaded = TRUE
		sData.iHairCutSoundID = GET_SOUND_ID()
		sData.iClippersSoundID = GET_SOUND_ID()
		sData.iMakeupSoundID = GET_SOUND_ID()
	ENDIF
	
	RETURN TRUE
ENDFUNC

/// PURPOSE: Unloads any assets required in the browse process
PROC UNLOAD_SHOP_BROWSE_ASSETS(HAIRDO_SHOP_STRUCT &sData)

	IF sData.bLoadPTFX
		REMOVE_PTFX_ASSET()
		sData.bLoadPTFX = FALSE
	ENDIF
	
	IF sData.bAudioBankLoaded
		RELEASE_AMBIENT_AUDIO_BANK()
		sData.bAudioBankLoaded = FALSE
	ENDIF
ENDPROC

INT iStaggeredShopUser
/// PURPOSE: Decide who gets to use the shop keeper
PROC SERVER_MAINTAIN_SHOP_KEEPER_USAGE_FOR_PARTICIPANT_OF_THIS_SCRIPT()
	IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
		INT iPlayerAssignedToSlot
		PARTICIPANT_INDEX ParticipantID = INT_TO_PARTICIPANTINDEX(iStaggeredShopUser)
		
		IF NETWORK_IS_PARTICIPANT_ACTIVE(ParticipantID)
		AND IS_NET_PLAYER_OK(NETWORK_GET_PLAYER_INDEX(ParticipantID), TRUE) // It's actually a staggered participant index.
		AND playerBD[iStaggeredShopUser].bPlayerRequestedShop
		AND NETWORK_IS_PLAYER_A_PARTICIPANT(NETWORK_GET_PLAYER_INDEX(ParticipantID))
		
			iPlayerAssignedToSlot = serverBD.iPlayerAssignedToShop[playerBD[iStaggeredShopUser].iShopRquestedByPlayer]-1
			
			// Player already assigned a shop
			IF iPlayerAssignedToSlot = iStaggeredShopUser
				// ! do nothing
				EXIT
				
				
			// Assign free slot
			ELIF iPlayerAssignedToSlot = -1
				PRINTLN("shop_keeper_usage Casino - free slot: shop=", GET_SHOP_NAME(INT_TO_ENUM(SHOP_NAME_ENUM, (playerBD[iStaggeredShopUser].iShopRquestedByPlayer))), ", player=", iStaggeredShopUser)
				serverBD.iPlayerAssignedToShop[playerBD[iStaggeredShopUser].iShopRquestedByPlayer] = (iStaggeredShopUser+1)
				SET_BIT(serverBD.iPlayersAssignedToShopBitset, iStaggeredShopUser)
				EXIT
				
				
			// Clear up occupied slots that are no longer valid
			ELSE
				IF NOT IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayerAssignedToSlot), TRUE)
				OR NOT playerBD[iPlayerAssignedToSlot].bPlayerRequestedShop
				OR playerBD[iPlayerAssignedToSlot].iShopRquestedByPlayer != playerBD[iStaggeredShopUser].iShopRquestedByPlayer
					PRINTLN("shop_keeper_usage Casino - dirty slot: shop=", GET_SHOP_NAME(INT_TO_ENUM(SHOP_NAME_ENUM, (playerBD[iStaggeredShopUser].iShopRquestedByPlayer))), ", player=", iStaggeredShopUser)
					serverBD.iPlayerAssignedToShop[playerBD[iStaggeredShopUser].iShopRquestedByPlayer] = (iStaggeredShopUser+1)
					SET_BIT(serverBD.iPlayersAssignedToShopBitset, iStaggeredShopUser)
					EXIT
				ENDIF
				
				// ! unable to assign id
			ENDIF
		
		// Clear out assigned slot
		ELIF IS_BIT_SET(serverBD.iPlayersAssignedToShopBitset, iStaggeredShopUser)
		
			PRINTLN("shop_keeper_usage Casino - clearing assigned slots: player=", iStaggeredShopUser)
		
			INT iShop
			REPEAT NUMBER_OF_SHOPS iShop
				IF serverBD.iPlayerAssignedToShop[iShop] = (iStaggeredShopUser+1)
					PRINTLN("shop_keeper_usage Casino - clear slot: shop=", GET_SHOP_NAME(INT_TO_ENUM(SHOP_NAME_ENUM, (playerBD[iStaggeredShopUser].iShopRquestedByPlayer))), ", player=", iStaggeredShopUser)
					serverBD.iPlayerAssignedToShop[iShop] = 0
				ENDIF
			ENDREPEAT
			CLEAR_BIT(serverBD.iPlayersAssignedToShopBitset, iStaggeredShopUser)
		ENDIF
	ENDIF	
	
	iStaggeredShopUser++
	IF (iStaggeredShopUser >= NUM_NETWORK_PLAYERS)
		iStaggeredShopUser = 0
	ENDIF
ENDPROC

/// PURPOSE: Checks for the player entering any of the shop locates
PROC CHECK_HAIRDO_LOCATE_ENTRY(HAIRDO_SHOP_STRUCT &sData)

	BOOL bLoseCops
	BOOL bShopClosed
	BOOL bKeeperBusy
	BOOL bPlayerInHairdoLocate = FALSE
	BOOL bDisplayContext = FALSE
	
	BOOL bIsEntityInAngledArea = FALSE
	
	INT iClosestLocate = -1
	
	IF IS_SHOP_PED_OK()
	AND NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
	
		IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
		OR (NOT IS_PAUSE_MENU_ACTIVE()
			AND IS_SHOP_LOCATES_SAFE_TO_BROWSE(sData.sShopInfo.eShop)
			AND NOT IS_SHOP_LOCATES_BLOCKED(sData.sShopInfo.eShop))
			
			IF SHOULD_PLAYER_LOSE_COPS_FOR_SHOP()
				bLoseCops = TRUE
				
			ELIF NOT IS_SHOP_KEEPER_SAFE_TO_USE(PLAYER_ID(), sData.eInShop, FALSE)
			AND sData.sShopInfo.eShop != HAIRDO_SHOP_CASINO_APT
				
				PRINTLN("DEAL_WITH_TRANSITION_SESSIONS_RESTORE_VEHICLE_AND_WARP - shop keeper now safe to use...")
				bKeeperBusy = TRUE
			ELIF NOT IS_SHOP_KEEPER_SAFE_TO_USE_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_ID(), sData.eInShop, FALSE)
			AND sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT
				PRINTLN("DEAL_WITH_TRANSITION_SESSIONS_RESTORE_VEHICLE_AND_WARP - shop keeper casino not safe to use...")
				bKeeperBusy = TRUE
			ELSE
				// Grab the locate data once
				IF sData.sBrowseInfo.iCurrentLocate = -1
				OR GET_ENTITY_MODEL(PLAYER_PED_ID()) != sData.eCurrentPedModel
				
					CPRINTLN(DEBUG_SHOPS, "Grabbing the locate data for hairdo shop. Includes the available hairdos.")
					sData.eCurrentPedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
					IF GET_HAIRDO_SHOP_LOCATE_DATA(sData.fpSetupClothingItemForShop, sData.sShopInfo.eShop, sData.sLocateInfo, g_sShopCompItemData, sData.iHairdoGroupsAvailable, sData.iHairdoGroupsWithNewItems)
						PRINT_SHOP_DEBUG_LINE("GRABBED LOCATE DATA")
						sData.sBrowseInfo.iCurrentLocate = 0
					ELSE
						PRINT_SHOP_DEBUG_LINE("FAILED TO GRAB LOCATE DATA")
						//CLEAR_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
						sData.sBrowseInfo.iCurrentLocate = 99
					ENDIF
				ENDIF
				
				IF sData.sBrowseInfo.iCurrentLocate = 0
					IF NOT bPlayerInHairdoLocate
					
						IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
						OR IS_ENTITY_IN_ANGLED_AREA(PLAYER_PED_ID(), sData.sLocateInfo.vEntryPos[0], sData.sLocateInfo.vEntryPos[1], sData.sLocateInfo.fEntrySize[0])
							bIsEntityInAngledArea = TRUE
							IF LOAD_SHOP_BROWSE_ASSETS(sData, FALSE)
								IF sData.sShopInfo.iContextID = NEW_CONTEXT_INTENTION
									
									STRING sHelpText
									
									// pick correct help text message
									IF DOES_PLAYER_HAVE_BEARD_OPTIONS()
										IF DOES_PLAYER_HAVE_MAKEUP_OPTIONS()
											// hair, beard and makeup
											sHelpText = "HAIR_BROWSE_MB"
										ELSE
											// hair and beard
											sHelpText = "HAIR_BROWSE_B"
										ENDIF
									ELSE
										IF DOES_PLAYER_HAVE_MAKEUP_OPTIONS()
											// hair and makeup
											sHelpText = "HAIR_BROWSE_M"
										ELSE
											// just haircuts
											sHelpText = "HAIR_BROWSE"
										ENDIF
									ENDIF
									
									
									///////////////////////////////////////////////////////////////////////////////////////////////////
									///    	 
									///    	 NG MP MENU UPDATE
									#IF USE_NEW_HAIRDOS_SHOP_MENUS
										IF NETWORK_IS_GAME_IN_PROGRESS()
											IF IS_PLAYSTATION_PLATFORM()
											OR IS_XBOX_PLATFORM()
											OR IS_PC_VERSION()
												sHelpText = "HAIR_BROWSE_NG"
											ENDIF
										ENDIF
									#ENDIF
									
									REGISTER_CONTEXT_INTENTION(sData.sShopInfo.iContextID, CP_MEDIUM_PRIORITY, sHelpText, FALSE)
								ENDIF
								bDisplayContext = TRUE
								SET_INPUT_EXCLUSIVE(PLAYER_CONTROL, INPUT_CONTEXT)
								IF HAS_CONTEXT_BUTTON_TRIGGERED(sData.sShopInfo.iContextID)
								OR SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
									// Player has presed the try button so jump to the next stage
									bDisplayContext = FALSE
									bPlayerInHairdoLocate = TRUE
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ENDIF
		ELSE
			IF NETWORK_IS_GAME_IN_PROGRESS()
				IF IS_PLAYER_CRITICAL_TO_ANY_EVENT(PLAYER_ID())
					// Only display help if no other message is on screen
					IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
					OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("SHOP_CLOSED", GET_SHOP_NAME(sData.sShopInfo.eShop))
						bShopClosed = TRUE
					ENDIF
				ENDIF
			ENDIF
		
			//Don't do instant trigger if we are not safe to browse. 
			//i.e. jumping/ragdoll etc. 
			sData.bDoInstantLocateTrigger = FALSE
			//TODO - take this out?
			sData.bDoInstantLocateTrigger = sData.bDoInstantLocateTrigger
		ENDIF
	ENDIF
	
	IF bLoseCops
		PRINT_SHOP_HELP("SHOP_COPS") // Lose the cops.
	ELIF bKeeperBusy
		IF NOT IS_PLAYER_IN_SOFA_LOCATE(sData)	
		AND sData.sShopInfo.eShop != HAIRDO_SHOP_CASINO_APT
			PRINT_SHOP_HELP("HAIR_FTRIG_BUSY") // The hair dresser is busy, please come back later.
		ENDIF
	ELIF bShopClosed
		IF sData.sShopInfo.eShop != HAIRDO_SHOP_CASINO_APT
		AND !IS_PLAYER_AN_ANIMAL(PLAYER_ID())
			PRINT_SHOP_HELP_WITH_STRING("SHOP_CLOSED", GET_SHOP_NAME(sData.sShopInfo.eShop))
		ENDIF
	ELIF IS_PED_WEARING_JUGGERNAUT_SUIT(PLAYER_PED_ID())
	AND NETWORK_IS_GAME_IN_PROGRESS()
		// Only display help if no other message is on screen
		IF NOT IS_HELP_MESSAGE_BEING_DISPLAYED()
		OR IS_THIS_HELP_MESSAGE_WITH_STRING_BEING_DISPLAYED("SHOP_NONE", GET_SHOP_NAME(sData.sShopInfo.eShop))
			PRINT_SHOP_HELP_WITH_STRING("SHOP_JUGG_NONE", GET_SHOP_NAME(sData.sShopInfo.eShop))
		ENDIF
	ELSE
		// Once we have found the closest locate, and we are atleast 8m from it, display the floating help
		IF bPlayerInHairdoLocate
			IF sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT
				REQUEST_USE_OF_SHOP_KEEPER_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_ID(), sData.eInShop)
			ELSE
				REQUEST_USE_OF_SHOP_KEEPER(PLAYER_ID(), sData.eInShop)
			ENDIF
			
			REMOVE_SHOP_HELP()
			sData.sShopInfo.eStage = SHOP_STAGE_BROWSE_ITEMS
			sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_INIT
			
			sData.iEntryTimer = GET_GAME_TIMER()
			IF NETWORK_IS_GAME_IN_PROGRESS()
				sData.tdEntryTimer = GET_NETWORK_TIME()
			ENDIF
			
			sData.iHeadBlendTimer = -1
			
			// Set the browsing flag straight away
			UPDATE_SHOP_BROWSE_STATE(sData.sShopInfo, sData.sBrowseInfo)
			
		ELIF iClosestLocate = 0
		AND NOT g_bInMultiplayer
		AND NOT bDisplayContext
			PRINT_SHOP_HELP(GET_SHOP_LABEL_FOR_LOCATE_ENTRY(sData.sLocateInfo.eType))
		ELSE
			REMOVE_SHOP_HELP()
			sData.sBrowseInfo.bBrowsing = FALSE
		ENDIF
	ENDIF
	
	IF NOT bDisplayContext
	AND NOT IS_PLAYER_IN_SOFA_LOCATE(sData)	
		IF sData.sShopInfo.iContextID != NEW_CONTEXT_INTENTION
			RELEASE_CONTEXT_INTENTION(sData.sShopInfo.iContextID)
		ENDIF
	ENDIF
	
	IF NOT bPlayerInHairdoLocate
		IF sData.sShopInfo.eShop != HAIRDO_SHOP_CASINO_APT 
		AND IS_SHOP_KEEPER_SAFE_TO_USE(PLAYER_ID(), sData.eInShop, TRUE)
			REMOVE_USE_OF_SHOP_KEEPER(PLAYER_ID(), sData.eInShop)
		ENDIF
		
		IF sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT 
		AND IS_SHOP_KEEPER_SAFE_TO_USE_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_ID(), sData.eInShop, FALSE)
			REMOVE_USE_OF_SHOP_KEEPER_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_ID(), sData.eInShop)
		ENDIF
		
		IF sData.eInShop = HAIRDO_SHOP_CASINO_APT
		AND NOT	bIsEntityInAngledArea
			UNLOAD_SHOP_BROWSE_ASSETS(sData)
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Determines the current hairdo in the menu list
PROC GET_CURRENT_HAIRDO_ITEM(HAIRDO_SHOP_STRUCT &sData, BOOL bSetCurrentItem)

	MODEL_NAMES ePedModel
	INT iMenuItem
	PED_INDEX pedID_ShopPed
	
	IF IS_SHOP_PED_OK()
	
		// If the cutscene ped exists, set the items on them, otherwise set it on the player
		pedID_ShopPed = PLAYER_PED_ID()
		IF DOES_ENTITY_EXIST(sData.sCutsceneData.pedID)
		AND NOT IS_PED_INJURED(sData.sCutsceneData.pedID)
			pedID_ShopPed = sData.sCutsceneData.pedID
		ENDIF
		
		//Grab the item details
		ePedModel = GET_ENTITY_MODEL(pedID_ShopPed)
		
		//hair
		IF sData.eCurrentMenu = HME_HAIR
			IF sData.iCurrentHairdo = -1
				CPRINTLN(DEBUG_SHOPS, "GET_CURRENT_HAIRDO_ITEM: HAIR")
				sData.iCurrentHairdo = ENUM_TO_INT(CALL sData.fpGetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_HAIR))
			ENDIF
		//beard
		ELIF sData.eCurrentMenu = HME_BEARD
			IF sData.iCurrentBeard = -1
				CPRINTLN(DEBUG_SHOPS, "GET_CURRENT_HAIRDO_ITEM: BEARD")
				sData.iCurrentBeard = ENUM_TO_INT(CALL sData.fpGetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_BERD))
			ENDIF
		ELIF IS_NGMP_MENU(sData.eCurrentMenu)
							
			NGMP_MENU_OPTION_DATA sOptionData
			IF GET_NGMP_MENU_OPTION_DATA(GET_ENTITY_MODEL(pedID_ShopPed), sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
				
				CDEBUG1LN(DEBUG_SHOPS, "try item.GET_NGMP_MENU_OPTION_DATA(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ", iCurrentItem: ", sData.sBrowseInfo.iCurrentItem, "): ",
						sOptionData.tlLabel, ", iTexture: ",
						sOptionData.iTexture, ", eHairItem: ",
						sOptionData.eHairItem, ", iType: ",
						sOptionData.iType)
				
				// NG MP Ped Comp
				IF sOptionData.iType = 0
					sData.iCurrentHairdo = GET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT)
					
					IF bSetCurrentItem
						sData.iTint1 = SAFE_GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
						sData.iTint2 = SAFE_GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
						sData.iSavedTint2 = sData.iTint2
					ENDIF
					
					CPRINTLN(DEBUG_SHOPS, "try item.GET_NGMP_MENU_OPTION_DATA: NGMP COMP (", GET_CHARACTER_OVERLAY_TEXT(sOptionData.eHeadSlot),
							", sData.iCurrentHairdo: ", sData.iCurrentHairdo,
							") bSetCurrentItem: ", bSetCurrentItem,
							", iTint1: ", sData.iTint1,
							", iTint2 ", sData.iTint2,
							", fBlend ", sData.fBlend)
					
				// NG MP Overlay
				ELIF sOptionData.iType = 1
				OR sOptionData.iType = 2
					sData.iCurrentBeard = GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(sOptionData.eHeadSlot))
					IF bSetCurrentItem
						sData.iTint1 = GET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY((sOptionData.eHeadSlot), sData.eRampType)
						sData.iTint2 = GET_MP_CHARACTER_SECONDARY_COLOUR_FROM_HEAD_OVERLAY((sOptionData.eHeadSlot))
						sData.iSavedTint2 = sData.iTint2
						
						sData.fBlend = GET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(sOptionData.eHeadSlot))
					ENDIF
					
					CPRINTLN(DEBUG_SHOPS, "try item.GET_NGMP_MENU_OPTION_DATA: NGMP OVERLAY (", GET_CHARACTER_OVERLAY_TEXT(sOptionData.eHeadSlot),
							", sData.iCurrentBeard: ", sData.iCurrentBeard,
							") bSetCurrentItem: ", bSetCurrentItem,
							", iTint1: ", sData.iTint1,
							", iTint2 ", sData.iTint2,
							", fBlend ", sData.fBlend)
				ENDIF
			ENDIF
			
			IF bSetCurrentItem
				
			ENDIF
		//misc
		ELSE
			CPRINTLN(DEBUG_SHOPS, "GET_CURRENT_HAIRDO_ITEM: UNKNOWN_", sData.eCurrentMenu)
		ENDIF
		
		IF bSetCurrentItem
			REPEAT g_sShopCompItemData.iItemCount iMenuItem
				IF CALL sData.fpIsPedCompItemAvailable(ePedModel, g_sShopCompItemData.eTypes[iMenuItem], g_sShopCompItemData.eItems[iMenuItem])
					CPRINTLN(DEBUG_SHOPS, "CALL sData.fpIsPedCompItemAvailable(ePedModel, g_sShopCompItemData.eTypes[", iMenuItem, "], g_sShopCompItemData.eItems[iMenuItem])")
					
					sData.sBrowseInfo.iCurrentItem = iMenuItem
					EXIT
				ENDIF
			ENDREPEAT
			
			// Hairdo not in list so just get the first item
			CDEBUG1LN(DEBUG_SHOPS, "Hairdo not in list so just get the first item")
			sData.sBrowseInfo.iCurrentItem = 0
		ENDIF
	ENDIF
ENDPROC

/// PURPOSE: Renders a temp cam which is a copy of the cam anim. This allows us to update the final rotation.
PROC UPDATE_HAIRDO_SHOP_BROWSE_CAM(HAIRDO_SHOP_STRUCT &sData, BOOL bAllowRStickInput)

	FLOAT fFOV

	IF NOT DOES_CAM_EXIST(sData.sShopInfo.sCameras[1].camID)
		CPRINTLN(DEBUG_SHOPS, "HAIRDO_SHOP_BROWSE_CAM cam 1 doesnt exist: creating")
		sData.sShopInfo.sCameras[1].camID = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA")
	ENDIF
	
	IF NOT IS_CAM_ACTIVE(sData.sShopInfo.sCameras[1].camID)
		// set up initial camera values
		IF IS_SALON_ACTIVE(sData)					
			sData.vBrowseCamOffset = <<0.586, -1.1006, -0.1042>>
			sData.vBrowseCamLookAtCoord = <<-816.3060, -182.9140, 37.8927>>
			sData.fBrowseCamHeading = 180.0
			
		ELIF sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT
			sData.vBrowseCamOffset = <<0.586, -1.1006, -0.1042>>+<<0.0,0.0,0.2>>
			sData.vBrowseCamLookAtCoord = <<-816.3060, -182.9140, 37.8927>>+<<0.0,0.0,0.2>>
			sData.fBrowseCamHeading = 180.0
			
			// Align vec data to shop
			ALIGN_SHOP_COORD(HAIRDO_SHOP_01_BH, sData.eInShop, sData.vBrowseCamLookAtCoord)
			ALIGN_SHOP_HEADING(HAIRDO_SHOP_01_BH, sData.eInShop, sData.fBrowseCamHeading)
		ELSE
			sData.vBrowseCamOffset = <<1.0109, -0.8391, -0.1795>>
			sData.vBrowseCamLookAtCoord = <<138.4104, -1709.3263, 29.8074>>
			sData.fBrowseCamHeading = 0.0
			
			// Align vec data to shop
			ALIGN_SHOP_COORD(HAIRDO_SHOP_02_SC, sData.eInShop, sData.vBrowseCamLookAtCoord)
			ALIGN_SHOP_HEADING(HAIRDO_SHOP_02_SC, sData.eInShop, sData.fBrowseCamHeading)
		ENDIF
		sData.fBrowseCamHeightOffset = 0.0
		SET_CAM_COORD(sData.sShopInfo.sCameras[1].camID, GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sData.vBrowseCamLookAtCoord, sData.fBrowseCamHeading, sData.vBrowseCamOffset))
		POINT_CAM_AT_COORD(sData.sShopInfo.sCameras[1].camID, sData.vBrowseCamLookAtCoord)
		SET_CAM_FOV(sData.sShopInfo.sCameras[1].camID, 47.0)
		SET_CAM_ACTIVE(sData.sShopInfo.sCameras[1].camID, TRUE)
		SET_CAM_ACTIVE(sData.sShopInfo.sCameras[0].camID, FALSE)
		
//		IF sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT
//			SET_ROOM_FOR_GAME_VIEWPORT_BY_NAME()
//		ENDIF
		
		// Limit rotation angle to prevent clipping into the hairdresser
		sData.fUpperCamLimit = sData.fBrowseCamHeading + 120.0//100.0 
		sData.fLowerCamLimit = sData.fBrowseCamHeading - 150.0//100.0
		
		// Reset zoom
		sData.fCamZoomAlpha = 0
		sData.bAllowZoomControl = TRUE
		fFOV = 47.0
		
		IF IS_NGMP_MENU(sData.eCurrentMenu)
			IF sData.eCurrentMenu = HME_NGMP_MAIN
			OR sData.eCurrentMenu = HME_NGMP_MAKEUP
			OR sData.eCurrentMenu = HME_NGMP_CHEST
				sData.bAllowZoomControl = TRUE
			ELSE
				sData.bAllowZoomControl = FALSE
	//			sData.fCamZoomAlpha = 1
			ENDIF
		ENDIF
		
		CPRINTLN(DEBUG_SHOPS, "Hairdo Camera: browse camera")	
	ENDIF

	IF bAllowRStickInput
		INT iLeftX, iLeftY, iRightX, iRightY
		
		GET_CONTROL_VALUE_OF_ANALOGUE_STICKS( iLeftX, iLeftY, iRightX, iRightY)
		
		// Add a deadlock but keep values from 0.
		IF ((iRightX  < 32) AND (iRightX > -32))
		AND ((iRightY  < 32) AND (iRightY > -32))
		
			IF ((iRightX  < 32) AND (iRightX > -32))
				iRightX = 0
			ELSE
				IF (iRightX  < 0)
					iRightX -= 32
				ELSE
					iRightX += 32
				ENDIF
			ENDIF
			
			IF ((iRightY  < 32) AND (iRightY > -32))
				iRightY = 0
			ELSE
				IF (iRightY  < 0)
					iRightY -= 32
				ELSE
					iRightY += 32
				ENDIF
			ENDIF
		ENDIF
		
		iRightX *= -1
		IF IS_LOOK_INVERTED()
			iRightY	*= -1
		ENDIF
		
		// Use mouse movement if necessary.
		IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
			IF HANDLE_MENU_CURSOR(TRUE, sData.sBrowseInfo.iCurrentItem )
				sData.fBrowseCamHeading -= g_fMenuCursorXMoveDistance * 200
				sData.fBrowseCamHeightOffset += g_fMenuCursorYMoveDistance * 2
				//sData.fBrowseCamHeading -= GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_X_RELATIVE) * 200
				//sData.fBrowseCamHeightOffset += GET_CONTROL_UNBOUND_NORMAL(FRONTEND_CONTROL, INPUT_CURSOR_Y_RELATIVE) * 2
			ELSE
				IF bMouseRolloverActive
					SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
				ENDIF
			ENDIF
		ELSE
		sData.fBrowseCamHeading += (iRightX * 0.05)
		sData.fBrowseCamHeightOffset += (iRightY * 0.0005)
		ENDIF
		
		// make sure we're not letting the camera go out of bounds before adjusting its position.
		IF sData.fBrowseCamHeading < sData.fLowerCamLimit
			sData.fBrowseCamHeading = sData.fLowerCamLimit
		ENDIF
		
		IF sData.fBrowseCamHeading > sData.fUpperCamLimit 
			sData.fBrowseCamHeading = sData.fUpperCamLimit
		ENDIF
		
		// Cap height offset
		IF sData.fBrowseCamHeightOffset > 0.8
			sData.fBrowseCamHeightOffset = 0.8
		ELIF sData.fBrowseCamHeightOffset < -0.1
			sData.fBrowseCamHeightOffset = -0.1
		ENDIF
		
		//ZOOM
		FLOAT fZoom = 33
		
		IF sData.bAllowZoomControl
			IF NOT IS_NGMP_MENU(sData.eCurrentMenu)
					
				IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
					IF IS_CONTROL_JUST_RELEASED(FRONTEND_CONTROL,caZoomInput)
						bZoomToggle = NOT bZoomToggle
					ENDIF
				ELSE
					bZoomToggle = FALSE
				ENDIF
	
				IF IS_CONTROL_PRESSED(FRONTEND_CONTROL,caZoomInput)
				OR bZoomToggle

					IF sData.fCamZoomAlpha < 1
						sData.fCamZoomAlpha = sData.fCamZoomAlpha + 0.05
					ENDIF
					IF sData.fCamZoomAlpha > 1
						sData.fCamZoomAlpha = 1
					ENDIF
				ELSE
					IF sData.fCamZoomAlpha > 0
						sData.fCamZoomAlpha = sData.fCamZoomAlpha - 0.05
					ENDIF
					IF sData.fCamZoomAlpha < 0
						sData.fCamZoomAlpha = 0
					ENDIF
				ENDIF
			ELSE
				IF sData.fCamZoomAlpha > 0
					sData.fCamZoomAlpha = sData.fCamZoomAlpha - 0.05
				ENDIF
				IF sData.fCamZoomAlpha < 0
					sData.fCamZoomAlpha = 0
				ENDIF
			ENDIF
		ELSE
			IF sData.fCamZoomAlpha < 1
				sData.fCamZoomAlpha = sData.fCamZoomAlpha + 0.05
			ENDIF
			IF sData.fCamZoomAlpha > 1
				sData.fCamZoomAlpha = 1
			ENDIF
		ENDIF
		
		IF sData.fCamZoomAlpha <> 0	
		AND sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_BROWSING
			fFOV = COSINE_INTERP_FLOAT(47.0,fZoom,sData.fCamZoomAlpha)
		ELSE
			fFOV = 47.0
		ENDIF
		
		SET_CAM_FOV(sData.sShopInfo.sCameras[1].camID, fFOV)
		SET_CAM_COORD(sData.sShopInfo.sCameras[1].camID, << 0.0, 0.0, sData.fBrowseCamHeightOffset>> + GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(sData.vBrowseCamLookAtCoord, sData.fBrowseCamHeading, sData.vBrowseCamOffset))
		
	ELSE
		fFOV = 47.0
		SET_CAM_FOV(sData.sShopInfo.sCameras[1].camID, fFOV)
	ENDIF
ENDPROC

PROC RUN_HAIRDO_INTRO(HAIRDO_SHOP_STRUCT &sData)

	INT iIndex, iTexture

	// If the cutscene ped exists, set the items on them, otherwise set it on the player
	PED_INDEX pedID_ShopPed = PLAYER_PED_ID()
	IF DOES_ENTITY_EXIST(sData.sCutsceneData.pedID)
	AND NOT IS_PED_INJURED(sData.sCutsceneData.pedID)
		pedID_ShopPed = sData.sCutsceneData.pedID
	ENDIF
	
	IF IS_SHOP_PED_OK()
	AND NOT IS_PED_INJURED(pedID_ShopPed)
	AND NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
			
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF NETWORK_PLAYER_HAS_HEADSET(PLAYER_ID())
		    	IF NETWORK_IS_PLAYER_TALKING(PLAYER_ID())
		    		SET_PED_RESET_FLAG(pedID_ShopPed, PRF_EnableVoiceDrivenMouthMovement, TRUE)
		   		ENDIF
			ENDIF
		ENDIF
			
		SWITCH sData.sBrowseInfo.iControl
		
			CASE 0
				// had to wrap this in multiplayer check to stop an assert in sp
				//IF g_bInMultiplayer
					//SETUP_PLAYER_FOR_MP_SHOP_CUTSCENE(FALSE, TRUE, sData.sShopInfo.sShopKeeper.pedID, NET_TO_ENT(serverBD.scissorsID), TRUE, TRUE)
				//ENDIF
				REMOVE_TATTOOS_NOT_PURCHASED(pedID_ShopPed)
				sData.sBrowseInfo.iControl++
			BREAK
			
			CASE 1
				
				IF NOT g_bInMultiplayer
					SET_PED_CONFIG_FLAG(pedID_ShopPed, PCF_ForceSkinCharacterCloth, TRUE)
				ENDIF
				
				IF IS_ANY_VEHICLE_NEAR_POINT(sData.sLocateInfo.vEntryPos[0],3)	
				AND NOT g_bInMultiplayer		
					REPOSITION_PLAYER_LAST_VEHICLE_FOR_SHOP(sData.sShopInfo)
				ENDIF

				// Make sure the cutscene ped is visible
				IF DOES_ENTITY_EXIST(sData.sCutsceneData.pedID)
				AND NOT IS_PED_INJURED(sData.sCutsceneData.pedID)
					IF NOT IS_ENTITY_VISIBLE(sData.sCutsceneData.pedID)
						SET_ENTITY_VISIBLE(sData.sCutsceneData.pedID, TRUE)
						CLEAR_PED_BLOOD_DAMAGE_BY_ZONE(sData.sCutsceneData.pedID, PDZ_HEAD)
						CPRINTLN(DEBUG_SHOPS, "RUN_HAIRDO_INTRO: Setting cutscene ped visible")
					ENDIF
				ENDIF

				// player
				VECTOR vAnimPos, vAnimRot
				CLEAR_PED_TASKS_IMMEDIATELY(pedID_ShopPed)
				GET_HAIRDO_SHOP_ANIM_POSITION(sData.sShopInfo.eShop, vAnimPos, vAnimRot) 
				CLEAR_AREA_OF_OBJECTS(GET_ENTITY_COORDS(pedID_ShopPed), 2.75) 
				TASK_PLAY_ANIM_ADVANCED(pedID_ShopPed, sData.sHairdoShopAnimDict, "player_enterchair", vAnimPos, vAnimRot, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK )
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF IS_SALON_ACTIVE(sData)
						TASK_PLAY_ANIM_ADVANCED(sData.sShopInfo.sShopKeeper.pedID, sData.sHairdoShopAnimDict, "keeper_enterchair", vAnimPos, vAnimRot, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK )
						FORCE_PED_AI_AND_ANIMATION_UPDATE(sData.sShopInfo.sShopKeeper.pedID)
					ENDIF
					STOP_NETWORK_SYNC_SCENE_FOR_SHOP(sData)
					SETUP_NETWORK_SYNC_SCENE_FOR_SHOP(sData, TRUE, FALSE)
					ADD_PED_TO_SHOP_SYNC_SCENE(sData, sData.sShopInfo.sShopKeeper.pedID, "keeper_enterchair",SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
					ADD_PED_TO_SHOP_SYNC_SCENE(sData, PLAYER_PED_ID(), "player_enterchair",SYNCED_SCENE_LOOP_WITHIN_SCENE)
					ADD_ENTITY_TO_SHOP_SYNC_SCENE(sData, NET_TO_ENT(serverBD.scissorsID), GET_SCISSOR_ANIM_FOR_KEEPER_ANIM("keeper_enterchair"),SYNCED_SCENE_LOOP_WITHIN_SCENE)
					START_NETWORK_SYNC_SCENE_FOR_SHOP(sData)
				ELSE
					// shopkeeper
					UPDATE_SHOPKEEPER_ANIM(sData, "keeper_enterchair", FALSE, FALSE, TRUE)
				ENDIF
				
				sData.sBuddyHideData.buddyHide = TRUE
				
				IF NOT NETWORK_IS_GAME_IN_PROGRESS()	
					GET_PROP_VARIATION_TO_STORE(PLAYER_PED_ID(),ANCHOR_HEAD,iIndex, iTexture)
				ENDIF
				
				IF iIndex <> -1
					//Grab the current item for head and eye props
					sData.eHeadProp =  CALL sData.fpGetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_HEAD))
				ELSE
					sData.eHeadProp =  PROPS_HEAD_NONE
				ENDIF
				sData.eEyeProp =  CALL sData.fpGetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_PROPS, ENUM_TO_INT(ANCHOR_EYES))
				
				// Grab item info for any other masks
				MODEL_NAMES ePedModel
				PED_COMP_NAME_ENUM eCurrentItem
				ePedModel = GET_ENTITY_MODEL(pedID_ShopPed)
				eCurrentItem =  CALL sData.fpGetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_SPECIAL)
				IF IS_ITEM_A_MASK(ePedModel, COMP_TYPE_SPECIAL, eCurrentItem)
					sData.eSpecialMask = eCurrentItem
				ENDIF
				eCurrentItem =  CALL sData.fpGetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_SPECIAL2)
				IF IS_ITEM_A_MASK(ePedModel, COMP_TYPE_SPECIAL2, eCurrentItem)
					sData.eSpecial2Mask = eCurrentItem
				ENDIF
				eCurrentItem =  CALL sData.fpGetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_HAIR)
				IF IS_ITEM_A_MASK(ePedModel, COMP_TYPE_HAIR, eCurrentItem)
					sData.eHairMask = eCurrentItem
				ENDIF
				
				RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
				RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
				PRELOAD_PROPS(sData)
				
				// remove head and eye props
				CPRINTLN(DEBUG_SHOPS, "Removing clone head and eye props now.")
				CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_PROPS, PROPS_HEAD_NONE, FALSE)
				CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_PROPS, PROPS_EYES_NONE, FALSE)
				
				// remove any other masks
				REMOVE_ALL_MASKS(pedID_ShopPed, sData.fpSetPedCompItemCurrent)
				
				// set hood state down
				BOOL bHoodIsUp, bHoodIsTucked
				IF IS_PED_WEARING_HOODED_JACKET(PLAYER_PED_ID(), bHoodIsUp, bHoodIsTucked)
					SET_HOODED_JACKET_STATE(pedID_ShopPed, JACKET_HOOD_DOWN)
				ENDIF
				
				BOOL bClearedPilotSuitTeeth
				bClearedPilotSuitTeeth = FALSE
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					// MP masks use the berd component, so need to be stored and removed too
					sData.eBerdMask =  CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_BERD)
					CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0), FALSE)
					
					// MP pilot school outfit uses the teeth component, so need to be stored and removed too
					sData.eTeethMask =  CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH)
					IF (ePedModel = MP_M_FREEMODE_01 AND DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(GET_NAME_HASH_FROM_PED_COMP_ITEM(ePedModel, sData.eTeethMask, COMP_TYPE_TEETH, 3), DLC_RESTRICTION_TAG_PILOT_SUIT, ENUM_TO_INT(SHOP_PED_COMPONENT)))
					OR (ePedModel = MP_F_FREEMODE_01 AND DOES_SHOP_PED_APPAREL_HAVE_RESTRICTION_TAG(GET_NAME_HASH_FROM_PED_COMP_ITEM(ePedModel, sData.eTeethMask, COMP_TYPE_TEETH, 4), DLC_RESTRICTION_TAG_PILOT_SUIT, ENUM_TO_INT(SHOP_PED_COMPONENT)))
						bClearedPilotSuitTeeth = TRUE
						CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_TEETH, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0), FALSE)
					ENDIF
					
					// MP parachutes use the hand component, so need to be stored and removed too
					sData.iHandDrawable = GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), GET_PED_COMPONENT_FROM_TYPE(COMP_TYPE_HAND))
					sData.iHandTexture = GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), GET_PED_COMPONENT_FROM_TYPE(COMP_TYPE_HAND))
					CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_HAND, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0), FALSE)
				ENDIF
				
				// We need to remove the maska and hats on the player in MP, not just the clone.
				IF NETWORK_IS_GAME_IN_PROGRESS()
					// remove head and eye props
					CPRINTLN(DEBUG_SHOPS, "Removing player head and eye props now.")
					CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_HEAD_NONE, FALSE)
					CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_PROPS, PROPS_EYES_NONE, FALSE)
					
					// remove any other masks
					REMOVE_ALL_MASKS(PLAYER_PED_ID(), sData.fpSetPedCompItemCurrent)
					// MP masks use the berd component, so need to be stored and removed too
					CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0), FALSE)
					
					// set hood state down
					IF IS_PED_WEARING_HOODED_JACKET(PLAYER_PED_ID(), bHoodIsUp, bHoodIsTucked)
						IF bHoodIsTucked
							sData.eJacketState = JACKET_HOOD_TUCKED
						ELIF bHoodIsUp
							sData.eJacketState = JACKET_HOOD_UP
						ELSE
							sData.eJacketState = JACKET_HOOD_DOWN
						ENDIF
						SET_HOODED_JACKET_STATE(PLAYER_PED_ID(), JACKET_HOOD_DOWN)
					ELSE
						sData.eJacketState = INT_TO_ENUM(HOODED_JACKET_STATE_ENUM, -1)
					ENDIF
					
					// MP pilot school outfit uses the teeth component, so need to be stored and removed too
					IF bClearedPilotSuitTeeth
						CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_TEETH, INT_TO_ENUM(PED_COMP_NAME_ENUM, 0), FALSE)
					ENDIF
					
					sData.eCrewLogoTattoo = GET_CURRENT_CREW_LOGO_TATTOO()
				ENDIF
				
				START_SHOP_CUTSCENE(sData.sShopInfo)
				
				// camera
				UPDATE_CAMERA_ANIM(sData, GET_CAMERA_ANIM(sData, HSC_ENTER_CHAIR, ""))
				SET_FORCE_FOOTSTEP_UPDATE (PLAYER_PED_ID(), true)
				sData.sBrowseInfo.iControl++
			BREAK
			
			// Wait for the set to finish
			CASE 2
				IF HAS_ENTITY_ANIM_FINISHED(pedID_ShopPed, sData.sHairdoShopAnimDict, "player_enterchair")
				AND GET_SYNC_SCENE_PHASE(sData.iSyncSceneShopkeeper, sData.iNETSyncSceneShopkeeper) = 1.0
				AND GET_CAM_ANIM_CURRENT_PHASE(sData.sShopInfo.sCameras[0].camID) >= 1

					GET_HAIRDO_SHOP_ANIM_POSITION(sData.sShopInfo.eShop, vAnimPos, vAnimRot)
					
					STRING sPlayerIdle
					sPlayerIdle = GET_RANDOM_IN_CHAIR_IDLE_ANIM(sData)
					
					// player anim in chair
					IF IS_SALON_ACTIVE(sData)
						TASK_PLAY_ANIM_ADVANCED(pedID_ShopPed, sData.sHairdoShopAnimDict, sPlayerIdle, vAnimPos, vAnimRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING |   AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK )  //AF_HOLD_LAST_FRAME
					ELSE
						TASK_PLAY_ANIM_ADVANCED(pedID_ShopPed, sData.sHairdoShopAnimDict, "player_base", vAnimPos, vAnimRot, INSTANT_BLEND_IN, NORMAL_BLEND_OUT, -1, AF_LOOPING |   AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK )  //AF_HOLD_LAST_FRAME
					ENDIF
					IF NETWORK_IS_GAME_IN_PROGRESS()	
						sData.iNETSyncScenePlayer = NETWORK_CREATE_SYNCHRONISED_SCENE(vAnimPos, vAnimRot, EULER_YXZ, FALSE, TRUE)	
						IF IS_SALON_ACTIVE(sData)
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sData.iNETSyncScenePlayer, sData.sHairdoShopAnimDict, sPlayerIdle, INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						ELSE
							NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sData.iNETSyncScenePlayer, sData.sHairdoShopAnimDict, "player_base", INSTANT_BLEND_IN, NORMAL_BLEND_OUT)
						ENDIF
						NETWORK_START_SYNCHRONISED_SCENE(sData.iNETSyncScenePlayer)
					ENDIF
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedID_ShopPed, TRUE)
					
					IF NETWORK_IS_GAME_IN_PROGRESS()
						STOP_NETWORK_SYNC_SCENE_FOR_SHOP(sData)
						SETUP_NETWORK_SYNC_SCENE_FOR_SHOP(sData, FALSE, TRUE)
						ADD_PED_TO_SHOP_SYNC_SCENE(sData, sData.sShopInfo.sShopKeeper.pedID, "keeper_base",SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
						//ADD_PED_TO_SHOP_SYNC_SCENE(sData, PLAYER_PED_ID(), sPlayerIdle,SYNCED_SCENE_LOOP_WITHIN_SCENE)  //Adding player messes up the sync scene
						ADD_ENTITY_TO_SHOP_SYNC_SCENE(sData, NET_TO_ENT(serverBD.scissorsID), GET_SCISSOR_ANIM_FOR_KEEPER_ANIM("keeper_base"),SYNCED_SCENE_LOOP_WITHIN_SCENE)
						START_NETWORK_SYNC_SCENE_FOR_SHOP(sData)
					ELSE
						// shopkeeper anim in chair
						UPDATE_SHOPKEEPER_ANIM(sData, "keeper_base", FALSE, TRUE)
					ENDIF
					
					// camera
					UPDATE_HAIRDO_SHOP_BROWSE_CAM(sData, TRUE)
					
					GET_CURRENT_HAIRDO_ITEM(sData, FALSE)
					GET_CURRENT_BEARD_ITEM(sData, FALSE)
					
					// beard + makeup
					IF DOES_PLAYER_HAVE_BEARD_OPTIONS() 
					OR DOES_PLAYER_HAVE_MAKEUP_OPTIONS()
						sData.eCurrentMenu = HME_MAIN_MENU
					ELSE
						IF NETWORK_IS_GAME_IN_PROGRESS()
							sData.eCurrentMenu = HME_HAIR_GROUP
						ELSE
							sData.eCurrentMenu = HME_HAIR
						ENDIF
					ENDIF
					
					///////////////////////////////////////////////////////////////////////////////////////////////////
					///    	 
					///    	 NG MP MENU UPDATE
					#IF USE_NEW_HAIRDOS_SHOP_MENUS
					IF NETWORK_IS_GAME_IN_PROGRESS()
						IF IS_PLAYSTATION_PLATFORM()
						OR IS_XBOX_PLATFORM()
						OR IS_PC_VERSION()
							sData.eCurrentMenu = HME_NGMP_MAIN
							sData.sBrowseInfo.iCurrentItem=0
							
							WHILE sData.sBrowseInfo.iCurrentItem <= NUMBER_OF_NGMP_MAIN_OPTIONS-1
							AND NOT IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, GET_NGMP_MENU_FOR_MAIN_MENU_OPTION(sData.sBrowseInfo.iCurrentItem), sData.eInShop)
								sData.sBrowseInfo.iCurrentItem++
								
								CDEBUG1LN(DEBUG_SHOPS, "RUN_HAIRDO_INTRO: sData.sBrowseInfo.iCurrentItem = ", sData.sBrowseInfo.iCurrentItem, "...")
							ENDWHILE
							
							IF sData.sBrowseInfo.iCurrentItem >= NUMBER_OF_NGMP_MAIN_OPTIONS
								
								CWARNINGLN(DEBUG_SHOPS, "RUN_HAIRDO_INTRO: sData.sBrowseInfo.iCurrentItem = ", sData.sBrowseInfo.iCurrentItem, ", set to zero!")
								sData.sBrowseInfo.iCurrentItem = 0
							ENDIF
						ENDIF
						
//						IF GET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT) = 0
//							PED_COMP_NAME_ENUM eHairEnum
//							eHairEnum = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_HAIR)
//							SET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT, ENUM_TO_INT(eHairEnum))
//							
//							IF eHairEnum = DUMMY_PED_COMP
//							OR eHairEnum = INT_TO_ENUM(PED_COMP_NAME_ENUM, 0)
//								ASSERTLN("[KW SPAWN] SET_PLAYER_INITIAL_SHOP_ITEMS 1 - Saving players hair PACKED_CHAR_CURRENT_HAIRCUT = ", ENUM_TO_INT(eHairEnum),
//										" draw:", GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR),
//										" text:", GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR))
//							ELSE
//								PRINTLN("[KW SPAWN] SET_PLAYER_INITIAL_SHOP_ITEMS 1 - Saving players hair PACKED_CHAR_CURRENT_HAIRCUT = ", ENUM_TO_INT(eHairEnum))
//							ENDIF
//						ENDIF
//						
//						IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA) = 0
//							PED_COMP_NAME_ENUM eHairEnum
//							eHairEnum = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_HAIR)
//							SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA, ENUM_TO_INT(eHairEnum))
//							
//							IF eHairEnum = DUMMY_PED_COMP
//							OR eHairEnum = INT_TO_ENUM(PED_COMP_NAME_ENUM, 0)
//								ASSERTLN("[KW SPAWN] SET_PLAYER_INITIAL_SHOP_ITEMS 2 - Saving players hair MP_STAT_CHAR_FM_STORED_HAIRDO_SA = ", ENUM_TO_INT(eHairEnum),
//										" draw:", GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR),
//										" text:", GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR))
//							ELSE
//								PRINTLN("[KW SPAWN] SET_PLAYER_INITIAL_SHOP_ITEMS 2 - Saving players hair MP_STAT_CHAR_FM_STORED_HAIRDO_SA = ", ENUM_TO_INT(eHairEnum))
//							ENDIF
//						ENDIF
//						
//						IF GET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO) = 0
//							PED_COMP_NAME_ENUM eHairEnum
//							eHairEnum = GET_PED_COMP_ITEM_CURRENT_MP(PLAYER_PED_ID(), COMP_TYPE_HAIR)
//							SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO,ENUM_TO_INT(eHairEnum))
//							
//							IF eHairEnum = DUMMY_PED_COMP
//							OR eHairEnum = INT_TO_ENUM(PED_COMP_NAME_ENUM, 0)
//								ASSERTLN("[KW SPAWN] SET_PLAYER_INITIAL_SHOP_ITEMS 3 - Saving players hair MP_STAT_CHAR_FM_STORED_HAIRDO = ", ENUM_TO_INT(eHairEnum),
//										" draw:", GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR),
//										" text:", GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_HAIR))
//							ELSE
//								PRINTLN("[KW SPAWN] SET_PLAYER_INITIAL_SHOP_ITEMS 3 - Saving players hair MP_STAT_CHAR_FM_STORED_HAIRDO = ", ENUM_TO_INT(eHairEnum))
//							ENDIF
//						ENDIF
					
						IF GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_FEATURE_20) = 255.0
							FLOAT fHead_blend_eye_color
							fHead_blend_eye_color = TO_FLOAT(GET_HEAD_BLEND_EYE_COLOR(PLAYER_PED_ID()))
							SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_FEATURE_20,fHead_blend_eye_color)
							PRINTLN("[KW SPAWN] SET_PLAYER_INITIAL_SHOP_ITEMS 4 - Saving players eye MP_STAT_FEATURE_20 = ", fHead_blend_eye_color)
						ENDIF
					ENDIF
					#ENDIF
					
					IF NETWORK_IS_GAME_IN_PROGRESS()
						CPRINTLN(DEBUG_SHOPS, "Run Hairdo Intro: NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)")
						NETWORK_DISABLE_LEAVE_REMOTE_PED_BEHIND(TRUE)
						sData.bLeaveRemotePlayerBehindDisabled = TRUE
					ENDIF
					
					SET_CURSOR_POSITION_FOR_MENU()
					
					SET_FORCE_FOOTSTEP_UPDATE (PLAYER_PED_ID(), FALSE)
					sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_BROWSING
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

PROC RUN_HAIRDO_OUTRO(HAIRDO_SHOP_STRUCT &sData)
		
	// If the cutscene ped exists, set the items on them, otherwise set it on the player
	PED_INDEX pedID_ShopPed = PLAYER_PED_ID()
	IF DOES_ENTITY_EXIST(sData.sCutsceneData.pedID)
	AND NOT IS_PED_INJURED(sData.sCutsceneData.pedID)
		pedID_ShopPed = sData.sCutsceneData.pedID
	ENDIF
	
	IF NOT g_bInMultiplayer
		SET_PED_CONFIG_FLAG(pedID_ShopPed, PCF_ForceSkinCharacterCloth, FALSE)
	ENDIF

	VECTOR vAnimPos, vAnimRot
	FLOAT fCamBreakoutPhase, fAnimBlendoutPhase
	
	IF IS_SHOP_PED_OK()
	AND NOT IS_PED_INJURED(pedID_ShopPed)
	AND NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
			IF NETWORK_PLAYER_HAS_HEADSET(PLAYER_ID())
		    	IF NETWORK_IS_PLAYER_TALKING(PLAYER_ID())
		    		SET_PED_RESET_FLAG(pedID_ShopPed, PRF_EnableVoiceDrivenMouthMovement, TRUE)
		   		ENDIF
			ENDIF
		ENDIF	
			
		SWITCH sData.sBrowseInfo.iControl
			// Activate the camera and start the anims
			CASE 0
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					CLEANUP_HAIRDO_CUTSCENE_ASSETS(sData)
					CLEANUP_PLAYER_FOR_MP_SHOP_CUTSCENE()
					pedID_ShopPed = PLAYER_PED_ID()
				ENDIF
				
				REMOVE_SHOP_HELP()
				
				#IF USE_TU_CHANGES
				IF NETWORK_IS_GAME_IN_PROGRESS()
					REMOVE_MULTIPLAYER_WALLET_CASH()  //Remove cash on hud
					REMOVE_MULTIPLAYER_BANK_CASH()
				ENDIF
				#ENDIF	
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					// Reset the head and eye props
					ResetProps(sData)
				ENDIF
				
				// player anim
				GET_HAIRDO_SHOP_ANIM_POSITION(sData.sShopInfo.eShop, vAnimPos, vAnimRot)
				TASK_PLAY_ANIM_ADVANCED(pedID_ShopPed, sData.sHairdoShopAnimDict, "player_exitchair", vAnimPos, vAnimRot, INSTANT_BLEND_IN, REALLY_SLOW_BLEND_OUT, -1,  AF_OVERRIDE_PHYSICS |  AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_TAG_SYNC_OUT, 0, EULER_YXZ, AIK_DISABLE_LEG_IK )  //0.2
				FORCE_PED_AI_AND_ANIMATION_UPDATE(pedID_ShopPed)
				
				IF NETWORK_IS_GAME_IN_PROGRESS()	
					sData.iNETSyncScenePlayer = NETWORK_CREATE_SYNCHRONISED_SCENE(vAnimPos, vAnimRot, EULER_YXZ, FALSE, FALSE)	
					NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sData.iNETSyncScenePlayer, sData.sHairdoShopAnimDict, "player_exitchair", INSTANT_BLEND_IN, -1, SYNCED_SCENE_TAG_SYNC_OUT | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_EXPAND_PED_CAPSULE_FROM_SKELETON | SYNCED_SCENE_DONT_INTERRUPT)
					NETWORK_START_SYNCHRONISED_SCENE(sData.iNETSyncScenePlayer)
					TASK_PLAY_ANIM_ADVANCED(sData.sShopInfo.sShopKeeper.pedID, sData.sHairdoShopAnimDict, "keeper_exitchair", vAnimPos, vAnimRot, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_HOLD_LAST_FRAME, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK )
					FORCE_PED_AI_AND_ANIMATION_UPDATE(sData.sShopInfo.sShopKeeper.pedID)
					STOP_NETWORK_SYNC_SCENE_FOR_SHOP(sData)
					SETUP_NETWORK_SYNC_SCENE_FOR_SHOP(sData, TRUE, FALSE)
					ADD_PED_TO_SHOP_SYNC_SCENE(sData, sData.sShopInfo.sShopKeeper.pedID, "keeper_exitchair", SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, SLOW_BLEND_IN, REALLY_SLOW_BLEND_OUT)
					//ADD_PED_TO_SHOP_SYNC_SCENE(sData, PLAYER_PED_ID(), "player_exitchair")
					ADD_ENTITY_TO_SHOP_SYNC_SCENE(sData, NET_TO_ENT(serverBD.scissorsID), GET_SCISSOR_ANIM_FOR_KEEPER_ANIM("keeper_exitchair"))
					START_NETWORK_SYNC_SCENE_FOR_SHOP(sData)
				ELSE
					// shopkeeper anim in chair
					UPDATE_SHOPKEEPER_ANIM(sData, "keeper_exitchair", FALSE, FALSE, TRUE)
				ENDIF
				
				// camera
				UPDATE_CAMERA_ANIM(sData, GET_CAMERA_ANIM(sData, HSC_EXIT_CHAIR, ""))
				SET_FORCE_FOOTSTEP_UPDATE (PLAYER_PED_ID(), true)
				
				IF NOT g_bInMultiplayer
					SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceSkinCharacterCloth, FALSE)
				ENDIF
				
				sData.sBuddyHideData.buddyHide = FALSE
				
				sData.sBrowseInfo.iControl++
			BREAK
			
			CASE 1
				IF IS_SALON_ACTIVE(sData)
					
					fCamBreakoutPhase = 0.807
					
					IF NETWORK_IS_GAME_IN_PROGRESS()	
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_GO_STRAIGHT_TO_COORD)	<> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> WAITING_TO_START_TASK
							sData.iSyncScenePlayer = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sData.iNETSyncScenePlayer)
							IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncScenePlayer)
							OR GET_SYNCHRONIZED_SCENE_PHASE(sData.iSyncScenePlayer) >= 0.7181		
								TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), sData.sLocateInfo.vPlayerGoToPos, PEDMOVE_WALK, -1)
								FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
							ENDIF
						ENDIF
					ENDIF
						
				ELSE	
					fCamBreakoutPhase = 0.92//0.765
				ENDIF
				IF GET_CAM_ANIM_CURRENT_PHASE(sData.sShopInfo.sCameras[0].camID) >= fCamBreakoutPhase
					STOP_RENDERING_SCRIPT_CAMS_USING_CATCH_UP(TRUE, 0)
					sData.sBrowseInfo.iControl++
				ENDIF
			BREAK

			// Wait for the set to finish
			CASE 2
				IF IS_SALON_ACTIVE(sData)
					IF NETWORK_IS_GAME_IN_PROGRESS()	
						fAnimBlendoutPhase = 0.7181
					ELSE
						fAnimBlendoutPhase = 0.9151
					ENDIF
				ELSE
					fAnimBlendoutPhase = 0.8779//0.9624
				ENDIF
				IF NOT NETWORK_IS_GAME_IN_PROGRESS()	
					IF HAS_ENTITY_ANIM_FINISHED(pedID_ShopPed, sData.sHairdoShopAnimDict, "player_exitchair")
					OR GET_ENTITY_ANIM_CURRENT_TIME(pedID_ShopPed, sData.sHairdoShopAnimDict, "player_exitchair") >= fAnimBlendoutPhase	
						CPRINTLN(DEBUG_SHOPS, "Hairdo: Exit animation finished.")
						sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_CLEANUP
						
						SET_FORCE_FOOTSTEP_UPDATE (PLAYER_PED_ID(), FALSE)
						TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), sData.sLocateInfo.vPlayerGoToPos, PEDMOVE_WALK, -1)
						FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
					ENDIF
				ELSE
					sData.iSyncScenePlayer = NETWORK_GET_LOCAL_SCENE_FROM_NETWORK_ID(sData.iNETSyncScenePlayer)
					IF NOT IS_SYNCHRONIZED_SCENE_RUNNING(sData.iSyncScenePlayer)
					OR GET_SYNCHRONIZED_SCENE_PHASE(sData.iSyncScenePlayer) >= fAnimBlendoutPhase		
						CPRINTLN(DEBUG_SHOPS, "Hairdo: Exit animation finished.")
						sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_CLEANUP
						
						SET_FORCE_FOOTSTEP_UPDATE (PLAYER_PED_ID(), FALSE)
			
						HIDE_NET_SCISSORS()
						STOP_NETWORK_SYNC_SCENE_FOR_SHOP(sData)
							
						IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_GO_STRAIGHT_TO_COORD)	<> PERFORMING_TASK
						AND GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(),SCRIPT_TASK_GO_STRAIGHT_TO_COORD) <> WAITING_TO_START_TASK	
							IF sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT
							//	TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), sData.sLocateInfo.vPlayerGoToPos, PEDMOVE_WALK, -1)
							//	FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
							ELSE
								TASK_GO_STRAIGHT_TO_COORD(PLAYER_PED_ID(), sData.sLocateInfo.vPlayerGoToPos, PEDMOVE_WALK, -1)
								FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_WALK, FALSE, FAUS_CUTSCENE_EXIT)
							ENDIF
						ENDIF

					ENDIF
				ENDIF
			BREAK
		ENDSWITCH
	ENDIF
ENDPROC

/// PURPOSE:
///    Applies any discounts the player has earned. e.g for stopping shop robbery
/// PARAMS:
///    sData - hairdo shop struct
///    iCost - returns the adjusted cost of the item
PROC APPLY_HAIRDO_DISCOUNT(HAIRDO_SHOP_STRUCT &sData, INT &iCost)

	INT iDiscount = 0
	// - 5% for 95% positive reports
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF SHOULD_GIVE_SHOP_DISCOUNT()
			iDiscount += g_sMPTunables.iShopDiscountPercentValue
		ENDIF
	ENDIF
//	// 20% discount for SE
//	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
//		IF IS_SPECIAL_EDITION_GAME() OR IS_COLLECTORS_EDITION_GAME()
//			iDiscount += 20
//		ENDIF
//	ENDIF
	IF (HAS_PLAYER_STOPPED_SHOP_ROBBERY(sData.sShopInfo.eShop, GET_CURRENT_PLAYER_PED_ENUM()))
		iDiscount += 100
	ENDIF
	iDiscount = CLAMP_INT(iDiscount, 0, 100)
	
	iCost = FLOOR((TO_FLOAT(iCost) * (1.0 - (TO_FLOAT(iDiscount) / 100))))
ENDPROC

FUNC INT GET_HAIRDO_DISPLAY_PRICE(INT iCost)
	IF NETWORK_IS_GAME_IN_PROGRESS()
		RETURN ROUND((TO_FLOAT(iCost) * g_sMPTunables.fHairDoShopMultiplier))
	ELSE
		RETURN iCost
	ENDIF
ENDFUNC

/// PURPOSE:
///    Gets the cost of a hairdo, applies any discounts. Stores cost in sData.sBrowseInfo.iCurrentSubItem
///    sData.sBrowseInfo.iCurrentSubItem was unused in the hairdo shop so is only used for storing cost
/// PARAMS:
///    sItemData - item data struct of the item we want the price of
///    ePedModel - the model of the ped in the shop
///    eItem - the item enum we want the price of
FUNC INT GET_HAIRDO_ITEM_COST(HAIRDO_SHOP_STRUCT &sData, PED_COMP_ITEM_DATA_STRUCT &sItemData, MODEL_NAMES ePedModel, PED_COMP_NAME_ENUM eItem, BOOL bApplyDiscount = TRUE)
	INT iCost
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		// MP hairdo prices are stored in ped comp data
		iCost = sItemData.iCost
		
		IF sData.eCurrentMenu = HME_HAIR
			iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunableGroups.fhaircuts_group_modifier))
		ELIF sData.eCurrentMenu = HME_BEARD
			iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunableGroups.fbeards_group_modifier))
		ELIF sData.eCurrentMenu = HME_BEARD
			iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunableGroups.ffacepaint_make_up_group_modifier))
		ENDIF
		
		SWITCH GET_HASH_KEY(sItemData.sLabel)
			CASE HASH("CLO_BBF_H_05")				iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Messy_Bun_Chestnut                 BREAK // 990                    // "DLC_FEMALE_HAIR_MESSY_BUN_CHESTNUT"                 
			CASE HASH("CLO_BBF_H_06")				iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Messy_Bun_Blonde                   BREAK // 985                   // "DLC_FEMALE_HAIR_MESSY_BUN_BLONDE"                
			CASE HASH("CLO_BBF_H_07")				iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Messy_Bun_Auburn                   BREAK // 980                   // "DLC_FEMALE_HAIR_MESSY_BUN_AUBURN"                
			CASE HASH("CLO_BBF_H_08")				iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Messy_Bun_Black                    BREAK // 975                   // "DLC_FEMALE_HAIR_MESSY_BUN_BLACK"                 
			CASE HASH("CLO_BBF_H_09")				iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Messy_Bun_Brown                    BREAK // 970                   // "DLC_FEMALE_HAIR_MESSY_BUN_BROWN"                 
			CASE HASH("CLO_BBF_H_00")				iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Pin_Up_Girl_Chestnut               BREAK // 590                   // "DLC_FEMALE_HAIR_PIN_UP_GIRL_CHESTNUT"            
			CASE HASH("CLO_BBF_H_01")				iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Pin_Up_Girl_Blonde                 BREAK // 585                   // "DLC_FEMALE_HAIR_PIN_UP_GIRL_BLONDE"              
			CASE HASH("CLO_BBF_H_02")				iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Pin_Up_Girl_Auburn                 BREAK // 580                   // "DLC_FEMALE_HAIR_PIN_UP_GIRL_AUBURN"              
			CASE HASH("CLO_BBF_H_03")				iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Pin_Up_Girl_Black                  BREAK // 575                   // "DLC_FEMALE_HAIR_PIN_UP_GIRL_BLACK"               
			CASE HASH("CLO_BBF_H_04")				iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Pin_Up_Girl_Brown                  BREAK // 570                   // "DLC_FEMALE_HAIR_PIN_UP_GIRL_BROWN"               
			CASE HASH("CLO_BUS_F_H_1_0")			iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Twisted_Bob_Chestnut               BREAK // 1190                  // "DLC_FEMALE_HAIR_TWISTED_BOB_CHESTNUT"            
			CASE HASH("CLO_BUS_F_H_1_1")			iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Twisted_Bob_Black                  BREAK // 1185                  // "DLC_FEMALE_HAIR_TWISTED_BOB_BLACK"               
			CASE HASH("CLO_BUS_F_H_1_2")			iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Twisted_Bob_Auburn                 BREAK // 1180                  // "DLC_FEMALE_HAIR_TWISTED_BOB_AUBURN"              
			CASE HASH("CLO_BUS_F_H_1_3")			iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Twisted_Bob_Brown                  BREAK // 1175                  // "DLC_FEMALE_HAIR_TWISTED_BOB_BROWN"               
			CASE HASH("CLO_BUS_F_H_1_4")			iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Twisted_Bob_Blonde                 BREAK // 1170                  // "DLC_FEMALE_HAIR_TWISTED_BOB_BLONDE"              
			CASE HASH("CLO_BUS_F_H_0_3")			iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Tight_Bun_Chestnut                 BREAK // 865                   // "DLC_FEMALE_HAIR_TIGHT_BUN_CHESTNUT"              
			CASE HASH("CLO_BUS_F_H_0_4")			iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Tight_Bun_Blonde                   BREAK // 860                   // "DLC_FEMALE_HAIR_TIGHT_BUN_BLONDE"                
			CASE HASH("CLO_BUS_F_H_0_2")			iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Tight_Bun_Auburn                   BREAK // 855                   // "DLC_FEMALE_HAIR_TIGHT_BUN_AUBURN"                
			CASE HASH("CLO_BUS_F_H_0_0")			iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Tight_Bun_Black                    BREAK // 850                   // "DLC_FEMALE_HAIR_TIGHT_BUN_BLACK"                 
			CASE HASH("CLO_BUS_F_H_0_1")			iCost = g_sMPTunables.iDLC_FEMALE_HAIR_Tight_Bun_Brown                    BREAK // 845                   // "DLC_FEMALE_HAIR_TIGHT_BUN_BROWN"                 
			CASE HASH("CLO_BBM_H_05")				iCost = g_sMPTunables.iDLC_MALE_HAIR_Surfer_Dude_Dark_Brown               BREAK // 990                   // "DLC_MALE_HAIR_SURFER_DUDE_DARK_BROWN"            
			CASE HASH("CLO_BBM_H_06")				iCost = g_sMPTunables.iDLC_MALE_HAIR_Surfer_Dude_Light_Brown              BREAK // 985                   // "DLC_MALE_HAIR_SURFER_DUDE_LIGHT_BROWN"           
			CASE HASH("CLO_BBM_H_07")				iCost = g_sMPTunables.iDLC_MALE_HAIR_Surfer_Dude_Auburn                   BREAK // 980                   // "DLC_MALE_HAIR_SURFER_DUDE_AUBURN"                
			CASE HASH("CLO_BBM_H_08")				iCost = g_sMPTunables.iDLC_MALE_HAIR_Surfer_Dude_Blonde                   BREAK // 975                   // "DLC_MALE_HAIR_SURFER_DUDE_BLONDE"                
			CASE HASH("CLO_BBM_H_09")				iCost = g_sMPTunables.iDLC_MALE_HAIR_Surfer_Dude_Black                    BREAK // 970                   // "DLC_MALE_HAIR_SURFER_DUDE_BLACK"                 
			CASE HASH("CLO_BBM_H_00")				iCost = g_sMPTunables.iDLC_MALE_HAIR_Shaggy_Curls_Dark_Brown              BREAK // 590                   // "DLC_MALE_HAIR_SHAGGY_CURLS_DARK_BROWN"           
			CASE HASH("CLO_BBM_H_01")				iCost = g_sMPTunables.iDLC_MALE_HAIR_Shaggy_Curls_Light_Brown             BREAK // 585                   // "DLC_MALE_HAIR_SHAGGY_CURLS_LIGHT_BROWN"          
			CASE HASH("CLO_BBM_H_02")				iCost = g_sMPTunables.iDLC_MALE_HAIR_Shaggy_Curls_Auburn                  BREAK // 580                   // "DLC_MALE_HAIR_SHAGGY_CURLS_AUBURN"               
			CASE HASH("CLO_BBM_H_03")				iCost = g_sMPTunables.iDLC_MALE_HAIR_Shaggy_Curls_Blonde                  BREAK // 575                   // "DLC_MALE_HAIR_SHAGGY_CURLS_BLONDE"               
			CASE HASH("CLO_BBM_H_04")				iCost = g_sMPTunables.iDLC_MALE_HAIR_Shaggy_Curls_Black                   BREAK // 570                   // "DLC_MALE_HAIR_SHAGGY_CURLS_BLACK"                
			CASE HASH("CLO_BUS_H_1_0")				iCost = g_sMPTunables.iDLC_MALE_HAIR_High_Slicked_Sides_Dark_Brown        BREAK // 1190                  // "DLC_MALE_HAIR_HIGH_SLICKED_SIDES_DARK_BROWN"     
			CASE HASH("CLO_BUS_H_1_1")				iCost = g_sMPTunables.iDLC_MALE_HAIR_High_Slicked_Sides_Light_Brown       BREAK // 1185                  // "DLC_MALE_HAIR_HIGH_SLICKED_SIDES_LIGHT_BROWN"    
			CASE HASH("CLO_BUS_H_1_2")				iCost = g_sMPTunables.iDLC_MALE_HAIR_High_Slicked_Sides_Auburn            BREAK // 1180                  // "DLC_MALE_HAIR_HIGH_SLICKED_SIDES_AUBURN"         
			CASE HASH("CLO_BUS_H_1_3")				iCost = g_sMPTunables.iDLC_MALE_HAIR_High_Slicked_Sides_Blonde            BREAK // 1175                  // "DLC_MALE_HAIR_HIGH_SLICKED_SIDES_BLONDE"         
			CASE HASH("CLO_BUS_H_1_4")				iCost = g_sMPTunables.iDLC_MALE_HAIR_High_Slicked_Sides_Black             BREAK // 1170                  // "DLC_MALE_HAIR_HIGH_SLICKED_SIDES_BLACK"          
			CASE HASH("CLO_BUS_H_0_0")				iCost = g_sMPTunables.iDLC_MALE_HAIR_Short_Side_Part_Dark_Brown           BREAK // 865                   // "DLC_MALE_HAIR_SHORT_SIDE_PART_DARK_BROWN"        
			CASE HASH("CLO_BUS_H_0_1")				iCost = g_sMPTunables.iDLC_MALE_HAIR_Short_Side_Part_Light_Brown          BREAK // 860                   // "DLC_MALE_HAIR_SHORT_SIDE_PART_LIGHT_BROWN"       
			CASE HASH("CLO_BUS_H_0_2")				iCost = g_sMPTunables.iDLC_MALE_HAIR_Short_Side_Part_Auburn               BREAK // 855                   // "DLC_MALE_HAIR_SHORT_SIDE_PART_AUBURN"            
			CASE HASH("CLO_BUS_H_0_3")				iCost = g_sMPTunables.iDLC_MALE_HAIR_Short_Side_Part_Blonde               BREAK // 850                   // "DLC_MALE_HAIR_SHORT_SIDE_PART_BLONDE"            
			CASE HASH("CLO_BUS_H_0_4")				iCost = g_sMPTunables.iDLC_MALE_HAIR_Short_Side_Part_Black                BREAK // 845                   // "DLC_MALE_HAIR_SHORT_SIDE_PART_BLACK"
			CASE HASH("CLO_HP_F_HR_0_0")			iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[0]		  BREAK // 1400                  // "BIG_BANGS_1"
			CASE HASH("CLO_HP_F_HR_0_1")			iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[1]		  BREAK // 1445                  // "BIG_BANGS_2"
			CASE HASH("CLO_HP_F_HR_0_2")			iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[2]		  BREAK // 1455                  // "BIG_BANGS_3"
			CASE HASH("CLO_HP_F_HR_0_3")			iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[3]		  BREAK // 1465                  // "BIG_BANGS_4"
			CASE HASH("CLO_HP_F_HR_0_4")			iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[4]		  BREAK // 1460                  // "BIG_BANGS_5"
			CASE HASH("CLO_HP_F_HR_1_0")			iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[5]		  BREAK // 1225                  // "BRAIDED_TOP_KNOT_1"
			CASE HASH("CLO_HP_F_HR_1_1")			iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[6]		  BREAK // 1215                  // "BRAIDED_TOP_KNOT_2"
			CASE HASH("CLO_HP_F_HR_1_2")			iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[7]		  BREAK // 1205                  // "BRAIDED_TOP_KNOT_3"
			CASE HASH("CLO_HP_F_HR_1_3")			iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[8]		  BREAK // 1210                  // "BRAIDED_TOP_KNOT_4"
			CASE HASH("CLO_HP_F_HR_1_4")			iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[9]		  BREAK // 1220                  // "BRAIDED_TOP_KNOT_5"
			CASE HASH("CLO_HP_HR_0_0")				iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[10]		  BREAK // 1225                  // "LONG_SLICKED_DARK_BROWN"
			CASE HASH("CLO_HP_HR_0_1")				iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[11]		  BREAK // 1215                  // "LONG_SLICKED_LIGHT_BROWN"
			CASE HASH("CLO_HP_HR_0_2")				iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[12]		  BREAK // 1205                  // "LONG_SLICKED_AUBURN"
			CASE HASH("CLO_HP_HR_0_3")				iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[13]		  BREAK // 1210                  // "LONG_SLICKED_BLONDE"
			CASE HASH("CLO_HP_HR_0_4")				iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[14]		  BREAK // 1220                  // "LONG_SLICKED_BLACK"
			CASE HASH("CLO_HP_HR_1_0")				iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[15]		  BREAK // 1400                  // "HIPSTER_YOUTH_DARK_BROWN"
			CASE HASH("CLO_HP_HR_1_1")				iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[16]		  BREAK // 1445                  // "HIPSTER_YOUTH_BLONDE"
			CASE HASH("CLO_HP_HR_1_2")				iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[17]		  BREAK // 1455                  // "HIPSTER_YOUTH_AUBURN"
			CASE HASH("CLO_HP_HR_1_3")				iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[18]		  BREAK // 1465                  // "HIPSTER_YOUTH_LIGHT_BROWN"
			CASE HASH("CLO_HP_HR_1_4")				iCost = g_sMPTunables.iDLC_hipster_hair_makeup_price_modifiers[19]		  BREAK // 1460                  // "HIPSTER_YOUTH_BLACK"
		ENDSWITCH
		
		// apply shop tuneable
		iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fHairDoShopMultiplier))
		
		// apply valentines tuneable
		IF IS_BIT_SET(sItemData.iProperties, PED_COMPONENT_IS_DLC_BIT)
			SWITCH g_iLastDLCItemLockHash
				CASE HASH("CU_VAL_CLOTHES")
					iCost = ROUND(TO_FLOAT(iCost) * g_sMPTunables.fValentine_Haircuts_Multiplier)
				BREAK
				CASE HASH("CU_INDI_CLOTHES")
					iCost = ROUND(TO_FLOAT(iCost) * g_sMPTunables.fHair_Makeup_IndependenceDay_Group)
				BREAK
			ENDSWITCH
		ENDIF
	ELSE
		// SP hairdo prices are in a look up function as they vary greatly from barbers to salon
		IF sData.eCurrentMenu = HME_HAIR
			iCost = GET_SP_HAIRDO_PRICE(sData, ePedModel, eItem, TRUE)
			
		ELIF sData.eCurrentMenu = HME_BEARD
			iCost = GET_SP_HAIRDO_PRICE(sData, ePedModel, eItem, FALSE)
		ELSE
			CPRINTLN(DEBUG_SHOPS, "GET_HAIRDO_ITEM_COST passed invalid eCurrentMenu: ", sData.eCurrentMenu)
			SCRIPT_ASSERT("GET_HAIRDO_ITEM_COST passed invalid eCurrentMenu. Bug for Andy Minghella")
		ENDIF
	ENDIF
	
	IF (bApplyDiscount)
		APPLY_HAIRDO_DISCOUNT(sData, iCost)
	ENDIF
	
	// Price override for basket system
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
		iCost = NET_GAMESERVER_GET_PRICE(GET_HAIRDO_KEY_FOR_CATALOGUE(sData.eCurrentMenu, sItemData.sLabel), CATEGORY_HAIR, 1)
	ENDIF
	
	RETURN iCost
ENDFUNC

FUNC INT GET_BEARD_ITEM_COST(HAIRDO_SHOP_STRUCT &sData, INT iBeard, TEXT_LABEL_15 &sLabel, BOOL bApplyDiscount = TRUE)
	
	INT iCost = 5
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		SWITCH iBeard
			CASE 0 iCost =  500  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_cleanshaven_expenditure_tunable)) BREAK
			CASE 1 iCost =  150  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_lightstubble_expenditure_tunable)) BREAK
			CASE 2 iCost =  430  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_balbo_expenditure_tunable)) BREAK
			CASE 3 iCost =  325  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_circlebeard_expenditure_tunable))  BREAK
			CASE 4 iCost =  465  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_goatee_expenditure_tunable))  BREAK
			CASE 5 iCost =  500  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_chin_expenditure_tunable))  BREAK
			CASE 6 iCost =  350  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_soulpatch_expenditure_tunable))  BREAK
			CASE 7 iCost =  600  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_pencilchinstrap_expenditure_tunable))  BREAK
			CASE 8 iCost =  220  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_lightbeard_expenditure_tunable))  BREAK
			CASE 9 iCost =  1425  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_musketeer_expenditure_tunable))  BREAK
			CASE 10 iCost = 1700  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_moustache_expenditure_tunable))  BREAK
			CASE 11 iCost = 290  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_heavybeard_expenditure_tunable))  BREAK
			CASE 12 iCost = 185  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_stubble_expenditure_tunable))  BREAK
			CASE 13 iCost = 255  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_circlebeard2_expenditure_tunable))  BREAK
			CASE 14 iCost = 1150  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_horseshoeandsideburns_expenditure_tunable))  BREAK
			CASE 15 iCost = 1975  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_pencilmoustacheandmuttonchops_expenditure_tunable))  BREAK
			CASE 16 iCost = 875  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_pencilmoustacheandchinstrap_expenditure_tunable))  BREAK
			CASE 17 iCost = 2000  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_balboanddesignsideburns_expenditure_tunable))  BREAK
			CASE 18 iCost = 725  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_muttonchops_expenditure_tunable))  BREAK
			CASE 19 iCost = 395  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_fullbeard_expenditure_tunable))  BREAK
			CASE 20 iCost = 395  iCost = ROUND((TO_FLOAT(iCost) * g_sMPTunables.fbeard_unlocks_fullbeard_expenditure_tunable))  BREAK  //Don't think this is a thing
			
			CASE 27 iCost = g_sMPTunables.iDlc_BeardPrice_Curly  BREAK  //3400	Curly
			CASE 28 iCost = g_sMPTunables.iDlc_BeardPrice_CurlyDeepStranger  BREAK  //3850	Curly & Deep Stranger
			CASE 29 iCost = g_sMPTunables.iDlc_BeardPrice_Handlebar  BREAK  //5000	Handlebar
			CASE 30 iCost = g_sMPTunables.iDlc_BeardPrice_Faustic  BREAK  //4550	Faustic
			CASE 31 iCost = g_sMPTunables.iDlc_BeardPrice_OttoPatch  BREAK  //4300	Otto & Patch
			CASE 32 iCost = g_sMPTunables.iDlc_BeardPrice_OttoFullStranger  BREAK  //3650	Otto & Full Stranger
			CASE 33 iCost = g_sMPTunables.iDlc_BeardPrice_LightFranz  BREAK  //2150	Light Franz
			CASE 36 iCost = g_sMPTunables.iDlc_BeardPrice_LincolnCurtain  BREAK  //3100	Lincoln Curtain
			CASE 34 iCost = g_sMPTunables.iDlc_BeardPrice_Hampstead  BREAK  //2750	The Hampstead
			CASE 35 iCost = g_sMPTunables.iDlc_BeardPrice_Ambrose  BREAK  //2900	The Ambrose
			
		ENDSWITCH
	ELSE
		//
	ENDIF
	
	IF (bApplyDiscount)
		APPLY_HAIRDO_DISCOUNT(sData, iCost)
	ENDIF
	
	// Price override for basket system
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
		iCost = NET_GAMESERVER_GET_PRICE(GET_HAIRDO_KEY_FOR_CATALOGUE(sData.eCurrentMenu, sLabel), CATEGORY_BEARD, 1)
	ENDIF
	
	RETURN iCost
ENDFUNC

/// PURPOSE:
///    Gets Makeup Cost and Applies Discout
FUNC INT GET_MAKEUP_ITEM_COST(HAIRDO_SHOP_STRUCT &sData, MAKEUP_DATA_STRUCT &makeup, BOOL bApplyDiscount = TRUE)
	INT iCost = makeup.iCost
	
	IF GET_HASH_KEY(makeup.sLabel) = HASH("MKUP_IND_0")
		iCost = ROUND(TO_FLOAT(iCost) * g_sMPTunables.fHair_Makeup_IndependenceDay_Group)
	ENDIF
	
	IF (bApplyDiscount)
		APPLY_HAIRDO_DISCOUNT(sData, iCost)
	ENDIF
	
	// Price override for basket system
	IF NETWORK_IS_GAME_IN_PROGRESS()
	AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
		iCost = NET_GAMESERVER_GET_PRICE(GET_HAIRDO_KEY_FOR_CATALOGUE(sData.eCurrentMenu, makeup.sLabel), CATEGORY_MKUP, 1)
	ENDIF
	RETURN iCost
ENDFUNC

FUNC BOOL SET_DLC_BEARD_LABEL(TEXT_LABEL_15 &tlLabel, INT iBeardIndex)
	IF iBeardIndex >= ENUM_TO_INT(MP_BEARDS_MAX)+1 // +1 for clean shaven
		//Hipster Beards
		IF iBeardIndex >= 27 AND iBeardIndex <= DLC_MP_BEARDS_MAX
			tlLabel = "BRD_HP_"
			tlLabel += (iBeardIndex-27)
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

/// PURPOSE: Determines the current beard in the menu list
PROC GET_CURRENT_BEARD_COST(HAIRDO_SHOP_STRUCT &sData)
	IF IS_SHOP_PED_OK()
		
		TEXT_LABEL_15 tlBeardLabel = "HAIR_BEARD"
		tlBeardLabel += GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)
		
		sData.bSelectedItemIsDLC = SET_DLC_BEARD_LABEL(tlBeardLabel, GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem))
		sData.sBrowseInfo.iCurrentSubItem = GET_BEARD_ITEM_COST(sData, GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem), tlBeardLabel)
		
		// Telemetry
		STORE_CURRENT_SHOP_ITEM_DATA(tlBeardLabel, sData.sBrowseInfo.iCurrentSubItem)
	ENDIF
ENDPROC

/// PURPOSE: Determines the hairdo cost and stores in in sData.sBrowseInfo.iCurrentSubItem 
///    sData.sBrowseInfo.iCurrentSubItem was unused in the hairdo shop so is only used for storing cost
PROC GET_CURRENT_HAIRDO_COST(HAIRDO_SHOP_STRUCT &sData)
	MODEL_NAMES ePedModel
	
	IF IS_SHOP_PED_OK()
		//Grab the item details
		ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
		PED_COMP_ITEM_DATA_STRUCT sItemData = CALL sData.fpGetPedCompDataForItem(ePedModel, g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem])
		
		// get the cost of the item (applies discounts etc)
		sData.sBrowseInfo.iCurrentSubItem = GET_HAIRDO_ITEM_COST(sData, sItemData, ePedModel, g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem])
		
		// Telemetry
		STORE_CURRENT_SHOP_ITEM_DATA(sItemData.sLabel, sData.sBrowseInfo.iCurrentSubItem)
	ENDIF
ENDPROC

///PURPOSE: Checks which menu we are in, then checks if the item is current
FUNC BOOL IS_HAIR_OR_BEARD_ITEM_CURRENT(HAIRDO_SHOP_STRUCT &sData, INT iItem)
	
	IF sData.eCurrentMenu = HME_HAIR
	OR NETWORK_IS_GAME_IN_PROGRESS()			
		IF g_sShopCompItemData.eItems[iItem] = INT_TO_ENUM(PED_COMP_NAME_ENUM, sData.iCurrentHairdo)	
			RETURN TRUE
		ENDIF
	ELSE				
		IF g_sShopCompItemData.eItems[iItem] = INT_TO_ENUM(PED_COMP_NAME_ENUM, sData.iCurrentBeard)	
			RETURN TRUE
		ENDIF
	ENDIF

	RETURN FALSE
ENDFUNC

///PURPOSE: Deducts the cost of the hairdo from the players score
PROC PURCHASE_HAIRDO_ITEM(HAIRDO_SHOP_STRUCT &sData)
	CDEBUG1LN(DEBUG_SHOPS, "PURCHASE_HAIRDO_ITEM(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ")")
	
	IF IS_SHOP_PED_OK()
		IF g_bInMultiplayer
			// Make payment
			//IF sData.sBrowseInfo.iCurrentSubItem > 0
				//GIVE_LOCAL_PLAYER_CASH(-sData.sBrowseInfo.iCurrentSubItem) //  Cash now deducted in SET_PLAYER_HAS_PURCHASED_ITEM_IN_SHOP
			//ENDIF
			
			IF sData.eCurrentMenu = HME_BEARD 
				
			ELIF sData.eCurrentMenu = HME_HAIR 
				INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_NO_HAIRCUTS, 1)
				
				IF sData.sBrowseInfo.iCurrentSubItem > 0
					INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_INGAMESTORE_MONEYSPENT, sData.sBrowseInfo.iCurrentSubItem)
				ENDIF
				
				// update saved hairdo details
				SET_STORED_FM_HAIRDO(g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem])
				CPRINTLN(DEBUG_PED_COMP, "Saved hair updated to: ", g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem])
				
				CALL sData.fpSetPedCompItemAcquired(GET_ENTITY_MODEL(PLAYER_PED_ID()), g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem], TRUE)
				
				INT iCurrentLabelHash = GET_HASH_KEY(sData.sCurrentItemLabel)
				IF iCurrentLabelHash = HASH("CLO_IND_H_0_0")
				OR iCurrentLabelHash = HASH("CLO_IND_H_0_1")
				OR iCurrentLabelHash = HASH("CLO_IND_H_0_2")
				OR iCurrentLabelHash = HASH("CLO_IND_H_0_3")
				OR iCurrentLabelHash = HASH("CLO_IND_H_0_4")
				
				OR iCurrentLabelHash = HASH("CLO_INDF_H_0_0")
				OR iCurrentLabelHash = HASH("CLO_INDF_H_0_1")
				OR iCurrentLabelHash = HASH("CLO_INDF_H_0_2")
				OR iCurrentLabelHash = HASH("CLO_INDF_H_0_3")
				OR iCurrentLabelHash = HASH("CLO_INDF_H_0_4")
					SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PURCHASED_MULLET, TRUE)
				ELIF iCurrentLabelHash = HASH("CLO_VALF_H_0_0")
				OR iCurrentLabelHash = HASH("CLO_VALF_H_0_1")
				OR iCurrentLabelHash = HASH("CLO_VALF_H_0_2")
				OR iCurrentLabelHash = HASH("CLO_VALF_H_0_3")
				OR iCurrentLabelHash = HASH("CLO_VALF_H_0_4")
					SET_PACKED_STAT_BOOL(PACKED_MP_STATS_PURCHASED_FLAPPER_BOB, TRUE)
				ENDIF
				
				sData.bPurchasedHairOrMakeup = TRUE
			ELIF sData.eCurrentMenu = HME_MAKEUP
				#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
				IF GET_HASH_KEY(sData.sMakeup[sData.sBrowseInfo.iCurrentItem].sLabel) = HASH("MKUP_IND_0")
					SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PURCHASED_FACEPAINT, TRUE)
				ENDIF
				#ENDIF
				
				sData.bPurchasedHairOrMakeup = TRUE
			ELIF IS_NGMP_MENU(sData.eCurrentMenu)
			
				NGMP_MENU_OPTION_DATA sOptionData
				IF GET_NGMP_MENU_OPTION_DATA(GET_ENTITY_MODEL(PLAYER_PED_ID()), sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
					// NG MP Ped Comp
					IF sOptionData.iType = 0
						CPRINTLN(DEBUG_SHOPS, "	Purchased NGMP comp!!!")
						INCREMENT_BY_MP_INT_CHARACTER_AWARD(MP_AWARD_NO_HAIRCUTS, 1)
						
						IF sData.sBrowseInfo.iCurrentSubItem > 0
							INCREMENT_BY_MP_INT_PLAYER_STAT(MPPLY_INGAMESTORE_MONEYSPENT, sData.sBrowseInfo.iCurrentSubItem)
						ENDIF
						
						// update saved hairdo details
						SET_STORED_FM_HAIRDO(g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem])
						CPRINTLN(DEBUG_PED_COMP, "Saved hair updated to: ", g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem])
						
						CALL sData.fpSetPedCompItemAcquired(GET_ENTITY_MODEL(PLAYER_PED_ID()), g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem], TRUE)
						
						IF sOptionData.iUniqueHash = HASH("NGMP_HAIR_M_22")		// Mullet
						OR sOptionData.iUniqueHash = HASH("NGMP_HAIR_F_23")		// Mullet
							SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PURCHASED_MULLET, TRUE)
						ELIF sOptionData.iUniqueHash = HASH("NGMP_HAIR_F_20")	// Flapper Bob
							SET_PACKED_STAT_BOOL(PACKED_MP_STATS_PURCHASED_FLAPPER_BOB, TRUE)
						ENDIF
						
						sData.bPurchasedHairOrMakeup = TRUE
						
					// NG MP Overlay
					ELIF sOptionData.iType = 1
					OR sOptionData.iType = 2
						CPRINTLN(DEBUG_SHOPS, "Purchased NGMP overlay ", GET_CHARACTER_OVERLAY_TEXT(sOptionData.eHeadSlot), " is now ", sData.iCurrentBeard, " [iCurrentItem: ", sData.sBrowseInfo.iCurrentItem, "]")
						IF sData.eCurrentMenu = HME_NGMP_FACEPAINT
						OR sData.eCurrentMenu = HME_NGMP_MAKEUP_BLUSHER
						OR sData.eCurrentMenu = HME_NGMP_MAKEUP_EYE
						OR sData.eCurrentMenu = HME_NGMP_MAKEUP_LIPSTICK
							IF sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_13")	 // Stars n Stripes Face Paint
							OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_13")	 // Stars n Stripes Face Paint
								SET_PACKED_STAT_BOOL(PACKED_MP_STAT_PURCHASED_FACEPAINT, TRUE)
							ENDIF
							IF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_14") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_14"))		SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SHADOW_DEMON, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_15") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_15"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FLESHY_DEMON, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_16") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_16"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FLAYED_DEMON, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_17") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_17"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SORROW_DEMON, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_18") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_18"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SMILER_DEMON, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_19") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_19"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_CRACKED_DEMON, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_20") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_20"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_DANGER_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_21") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_21"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_WICKED_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_22") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_22"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_MENACE_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_23") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_23"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_BONE_JAW_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_24") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_24"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FLESH_JAW_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_25") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_25"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SPIRIT_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_26") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_26"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_GHOUL_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_27") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_27"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_PHANTOM_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_28") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_28"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_GNASHER_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_29") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_29"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_EXPOSED_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_30") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_30"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_GHOSTLY_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_31") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_31"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FURY_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_32") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_32"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_DEMI_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_33") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_33"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_INBRED_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_34") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_34"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SPOOKY_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_35") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_35"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SLASHED_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_36") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_36"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_WEB_SUGAR_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_37") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_37"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SENOR_SUGAR_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_38") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_38"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SWIRL_SUGAR_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_39") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_39"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FLORAL_SUGAR_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_40") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_40"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_MONO_SUGAR_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_41") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_41"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_FEMME_SUGAR_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_42") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_42"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_DEMI_SUGAR_SKULL, TRUE)
							ELIF (sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_M_43") OR sOptionData.iUniqueHash = HASH("NGMP_FACEPAINT_F_43"))	SET_PACKED_STAT_BOOL(PACKED_STAT_FACEPAINT_HALLOWEEN_2015_SCARRED_SUGAR_SKULL, TRUE)
							ENDIF
						ENDIF
						
						IF sData.eCurrentMenu = HME_NGMP_FACEPAINT
						AND sData.sBrowseInfo.iCurrentItem = 0
							STATS_PACKED sp_blusher_headslot
							sp_blusher_headslot = GET_CHARACTER_OVERLAY_STAT(HOS_BLUSHER)
							
							IF (sp_blusher_headslot != INT_TO_ENUM(STATS_PACKED, -1))
								IF (GET_PACKED_STAT_INT(sp_blusher_headslot) >= 7)
								AND (GET_PACKED_STAT_INT(sp_blusher_headslot) < 33)
									SET_PACKED_STAT_INT(sp_blusher_headslot, 255)
								ENDIF
							ENDIF
						ENDIF
						
						sData.bPurchasedHairOrMakeup = TRUE
					ENDIF
					
					IF sOptionData.eUnlockItem != DUMMY_CLOTHES_UNLOCK
						CPRINTLN(DEBUG_SHOPS, "Set clothes owned stat")
						SET_BITSET_CLOTHES_OWNED(sOptionData.eUnlockItem, TRUE)
					ENDIF
				ENDIF
				
				sData.bPurchasedHairOrMakeup = TRUE
			ENDIF
		ELSE
			// Make payment
			IF sData.sBrowseInfo.iCurrentSubItem > 0
				DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), GET_BAAC_FROM_SHOP_ENUM(sData.sShopInfo.eShop), sData.sBrowseInfo.iCurrentSubItem)
			ENDIF
			
			enumCharacterList eChar = GET_CURRENT_PLAYER_PED_ENUM()
			
			IF sData.eCurrentMenu = HME_HAIR
				SET_PLAYER_HAS_JUST_CHANGED_HAIRDO()
				#if USE_CLF_DLC							
				// update saved hairdo details
				g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle = g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]
				CPRINTLN(DEBUG_PED_COMP, "Saved hair updated to: ", g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle)				
				#endif
				#if USE_NRM_DLC							
				// update saved hairdo details
				g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle = g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]
				CPRINTLN(DEBUG_PED_COMP, "Saved hair updated to: ", g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle)				
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					//Special case check for Franklin been told to get his haircut at the end of Armenian 1.
					//Check if "get your haircut" request is active.
					IF Get_Mission_Flow_Bitset_Bit_State(FLOWBITSET_FRANKLIN_CHANGED_HAIRCUT, BITS_CHARACTER_REQUEST_ACTIVE)
						//Are we playing as Franklin?
						IF eChar = CHAR_FRANKLIN
							//Franklin just bought a new haircut, flag request as completed.
							Set_Mission_Flow_Bitset_Bit_State(FLOWBITSET_FRANKLIN_CHANGED_HAIRCUT, BITS_CHARACTER_REQUEST_COMPLETE, TRUE)
						ENDIF
					ENDIF			
					// update saved hairdo details
					g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle = g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]
					CPRINTLN(DEBUG_PED_COMP, "Saved hair updated to: ", g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eStoredHairstyle)
				#endif
				#endif
			ELIF sData.eCurrentMenu = HME_BEARD 
				#if USE_CLF_DLC							
					// update saved beard details
					g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eStoredBeard = g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]
					CPRINTLN(DEBUG_PED_COMP, "Saved Beard updated to: ", g_savedGlobalsClifford.sPlayerData.sInfo.sVariations[eChar].eStoredBeard)
				#endif
				#if USE_NRM_DLC							
					// update saved beard details
					g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eStoredBeard = g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]
					CPRINTLN(DEBUG_PED_COMP, "Saved Beard updated to: ", g_savedGlobalsnorman.sPlayerData.sInfo.sVariations[eChar].eStoredBeard)
				#endif
				#if not USE_CLF_DLC
				#if not USE_NRM_DLC
					// update saved beard details
					g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eStoredBeard = g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]
					CPRINTLN(DEBUG_PED_COMP, "Saved Beard updated to: ", g_savedGlobals.sPlayerData.sInfo.sVariations[eChar].eStoredBeard)
				#endif
				#endif
			ENDIF
		ENDIF
		
		g_bTriggerShopSave = TRUE
		
		SET_PLAYER_HAS_PURCHASED_ITEM_IN_SHOP(sData.sShopInfo.eShop, TRUE, PURCHASE_BARBERS)
		
		IF USE_SERVER_TRANSACTIONS()
			DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
		ENDIF
		
		// Hairstyles
		IF sData.eCurrentMenu = HME_HAIR 
			sData.iCurrentHairdo = ENUM_TO_INT(g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem])
			CPRINTLN(DEBUG_SHOPS, "Current hairdo is now ", sData.iCurrentHairdo)
		
		// Beards
		ELIF sData.eCurrentMenu = HME_BEARD 
			IF NETWORK_IS_GAME_IN_PROGRESS()
				sData.iCurrentBeard = GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)
			ELSE
				sData.iCurrentBeard = ENUM_TO_INT(g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]) 
			ENDIF
			sData.fCurrentBeardFade = 1.0
			CPRINTLN(DEBUG_SHOPS, "Current beard is now ", sData.iCurrentBeard)
			
		// makeup
		ELIF sData.eCurrentMenu = HME_MAKEUP
			sData.iPlayerMakeup = sData.sBrowseInfo.iCurrentItem
			CPRINTLN(DEBUG_SHOPS, "Current makeup is now ", sData.iPlayerMakeup)
		ELIF IS_NGMP_MENU(sData.eCurrentMenu)
			
			NGMP_MENU_OPTION_DATA sOptionData
			IF GET_NGMP_MENU_OPTION_DATA(GET_ENTITY_MODEL(PLAYER_PED_ID()), sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
				// NG MP Ped Comp
				IF sOptionData.iType = 0
					sData.iCurrentHairdo = sData.sBrowseInfo.iCurrentItem
					CPRINTLN(DEBUG_SHOPS, "Current NGMP comp is now ", sData.iCurrentHairdo, " [iCurrentItem: ", sData.sBrowseInfo.iCurrentItem, "]")
					
				// NG MP Overlay
				ELIF sOptionData.iType = 1
				OR sOptionData.iType = 2
					sData.iCurrentBeard = sData.sBrowseInfo.iCurrentItem
					CPRINTLN(DEBUG_SHOPS, "Current NGMP overlay is now ", sData.iCurrentBeard, " [iCurrentItem: ", sData.sBrowseInfo.iCurrentItem, "]")
					
					IF sOptionData.iType = 2
						CPRINTLN(DEBUG_SHOPS, "Clone the clone to player to copy eye blends")
					
						CLONE_PED_TO_TARGET(sData.sCutsceneData.pedID, PLAYER_PED_ID())
					ENDIF
					
				ENDIF
			ENDIF
			
		ELSE
			sData.iCurrentHairdo = sData.sBrowseInfo.iCurrentItem
			sData.iCurrentBeard = sData.sBrowseInfo.iCurrentItem
			CPRINTLN(DEBUG_SHOPS, "Current ITEM_", sData.eCurrentMenu, " is now ", sData.iCurrentHairdo, " and/or ", sData.iCurrentBeard)
		ENDIF
		sData.sBrowseInfo.bItemPurchased = TRUE
		sData.bRebuildMenu = TRUE
		
		BAWSAQ_INCREMENT_MODIFIER(BSMF_SM_HAIRCUTS)		//1514495 - stockmarket
	ENDIF
ENDPROC

PROC INITIALISE_DLC_STYLIST(SHOP_NAME_ENUM eShop)
	BOOL bSetFlags = FALSE
	
	IF eShop = HAIRDO_SHOP_CASINO_APT
		bSetFlags = TRUE
	ENDIF
	
	IF bSetFlags
		SET_SHOP_BIT(eShop, SHOP_NS_FLAG_SHOP_OPEN, TRUE)
		SET_SHOP_BIT(eShop, SHOP_S_FLAG_SHOP_AVAILABLE, TRUE)
		SET_SHOP_BIT(eShop, SHOP_S_FLAG_SHOP_RUN_ENTRY_INTRO, TRUE)
		CLEAR_SHOP_BIT(eShop, SHOP_NS_FLAG_SHOP_BEING_ROBBED)
		g_sShopSettings.bFirstPassComplete = TRUE
	ENDIF
ENDPROC

PROC BUILD_HAIR_GROUP_MENU(HAIRDO_SHOP_STRUCT &sData)
	
	INT iFirstAvailableItem = -1
	TEXT_LABEL_15 tlTempLabel
	
	// clear and rebuild the menu
	CLEAR_MENU_DATA()
	ADD_SHOP_MENU_GRAPHIC_TO_MENU(sData.sShopInfo.eShop)
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_TITLE("HAIR_TITLE_2") // Hairstyles
	sData.bSelectedItemIsLocked = FALSE
	sData.bSelectedItemIsNew = FALSE
	sData.bSelectedItemIsDLC = FALSE
	sData.bUnlockMessageDisplayed = FALSE
	
	// Add all the types
	INT iHairdoGroup
	REPEAT NUMBER_OF_HAIRDO_GROUPS iHairdoGroup
		IF IS_BIT_SET(sData.iHairdoGroupsAvailable[iHairdoGroup/32], iHairdoGroup%32)
			
			GET_HAIRDO_GROUP_LABEL(INT_TO_ENUM(HAIRDO_GROUP_ENUM, iHairdoGroup), tlTempLabel)
			
			IF IS_BIT_SET(sData.iHairdoGroupsWithNewItems[iHairdoGroup/32], iHairdoGroup%32)
				ADD_MENU_ITEM_TEXT(iHairdoGroup, tlTempLabel, 1)
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
			ELSE
				ADD_MENU_ITEM_TEXT(iHairdoGroup, tlTempLabel)
			ENDIF
			
			IF iFirstAvailableItem = -1
				iFirstAvailableItem = iHairdoGroup
			ENDIF
		ENDIF
	ENDREPEAT
	
	// highlight current item
	IF sData.bBlockAutoItemSelect
		sData.bBlockAutoItemSelect = FALSE
		SET_TOP_MENU_ITEM(sData.iTopMenuItem)
	ELSE
		sData.eCurrentHairGroup = INT_TO_ENUM(HAIRDO_GROUP_ENUM, iFirstAvailableItem)
	ENDIF
	SET_CURRENT_MENU_ITEM(ENUM_TO_INT(sData.eCurrentHairGroup))
	
	// set up button labels
	IF NOT IS_NGMP_MENU(sData.eCurrentMenu)
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ELSE
		IF sData.eCurrentMenu = HME_NGMP_MAIN
		OR sData.eCurrentMenu = HME_NGMP_MAKEUP
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
		ELSE
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_BUY")
		ENDIF
	ENDIF
	IF sData.bAllowZoomControl
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
		ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
		//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD, "ITEM_SCROLL")
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
		IF NOT IS_NGMP_MENU(sData.eCurrentMenu)
			ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
		ENDIF
		//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_RT, "ITEM_FSCROLL")
	ELSE
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
		ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
		//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD, "ITEM_SCROLL")
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
		//ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
		//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_RT, "ITEM_FSCROLL")
	ENDIF
ENDPROC

/// PURPOSE:
///    Builds the menu for the hair options.
///    This is also used in SP for beards as the functionality is the same
PROC BUILD_HAIR_MENU(HAIRDO_SHOP_STRUCT &sData, PED_INDEX pedID_ShopPed, BOOL bAddStarToSelectedItem)
	MODEL_NAMES ePedModel = GET_ENTITY_MODEL(pedID_ShopPed)
	
	TEXT_LABEL_15 tlTempLabel
	INT iMenuItem
	INT iCurrentItem
	INT iDisplayCost
	PED_COMP_NAME_ENUM eCompName
	PED_COMP_TYPE_ENUM eCompType
	PED_COMP_ITEM_DATA_STRUCT sItemData
	
	#IF IS_DEBUG_BUILD
		INT iBit
		MP_INT_STATS eStat
		ScriptCatalogItem sCatalogueData
	#ENDIF
	
				
	// Get the current selection and cost
	GET_CURRENT_HAIRDO_ITEM(sData, !sData.bBlockAutoItemSelect)
	
	// clear and rebuild the menu
	CLEAR_MENU_DATA()
	ADD_SHOP_MENU_GRAPHIC_TO_MENU(sData.sShopInfo.eShop)
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	IF sData.eCurrentMenu = HME_BEARD
		SET_MENU_TITLE("HAIR_TITLE_1")
	ELSE
		SET_MENU_TITLE("HAIR_TITLE_2")
	ENDIF
	sData.bSelectedItemIsLocked = FALSE
	sData.bSelectedItemIsNew = FALSE
	sData.bSelectedItemIsDLC = FALSE
	sData.bUnlockMessageDisplayed = FALSE
	
	IF g_sShopCompItemData.iItemCount = 0
		ADD_MENU_ITEM_TEXT(0, "HAIR_NONE")
	ELSE
		REPEAT g_sShopCompItemData.iItemCount iMenuItem
		
			eCompName = g_sShopCompItemData.eItems[iMenuItem]
			eCompType = g_sShopCompItemData.eTypes[iMenuItem]
			sItemData = CALL sData.fpGetPedCompDataForItem(ePedModel, eCompType, eCompName)
			
			IF GET_CLOTHES_SHOP_MENU_LABEL_OVERRIDE(INT_TO_ENUM(CLOTHES_MENU_LABEL_OVERRIDE_ENUM, g_sShopCompItemData.iItemLabelOverride[iMenuItem]), tlTempLabel)
				sItemData.sLabel = tlTempLabel
			ENDIF
			
			IF sData.sBrowseInfo.iCurrentItem = iMenuItem
				sData.sCurrentItemLabel = sItemData.sLabel
			ENDIF
			
			IF NOT (IS_BIT_SET(sItemData.iProperties, PED_COMPONENT_AVAILABLE_BIT))
				ADD_MENU_ITEM_TEXT(iMenuItem, sItemData.sLabel)
				ADD_MENU_ITEM_TEXT(iMenuItem, "", 1)
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LOCK)
				
				// FLAG: LOCKED
				IF iMenuItem = sData.sBrowseInfo.iCurrentItem
					sData.bSelectedItemIsLocked = TRUE
				ENDIF
			ELSE
			
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				///    	 
				///    	 CATALOGUE SETUP
				///    	 
				#IF IS_DEBUG_BUILD
					IF (g_bAddMenuItemsToCatalogue OR g_bVerifyMenuItemPrices)
					AND NETWORK_IS_GAME_IN_PROGRESS()
						GENERATE_KEY_FOR_SHOP_CATALOGUE(sCatalogueData.m_key, sItemData.sLabel, ePedModel, SHOP_TYPE_HAIRDO, 0, 0)
						sCatalogueData.m_textTag = sItemData.sLabel
						sCatalogueData.m_name = GET_STRING_FROM_TEXT_FILE(sCatalogueData.m_textTag)
						sCatalogueData.m_category = CATEGORY_HAIR
						sCatalogueData.m_price = sItemData.iCost
						sCatalogueData.m_storagetype = NET_SHOP_ISTORAGE_BITFIELD
						sCatalogueData.m_bitsize = 1
						sCatalogueData.m_stathash = 0
						
						IF IS_BIT_SET(sItemData.iProperties, PED_COMPONENT_IS_DLC_BIT)
							IF GET_SHOP_PED_APPAREL_SAVE_DATA(g_iLastDLCItemNameHash, PED_COMPONENT_ACQUIRED_SLOT, eStat, iBit)
								sCatalogueData.m_stathash = ENUM_TO_INT(MPIntStatNamesArray[eStat][GET_SLOT_NUMBER(g_iPedComponentSlot)])
								sCatalogueData.m_bitshift = iBit
							ENDIF
						ELSE
							sCatalogueData.m_stathash = ENUM_TO_INT(MPIntStatNamesArray[GET_MP_PED_COMPONENT_ACQUIRED_STAT(eCompType, ENUM_TO_INT(eCompName)/32)][GET_SLOT_NUMBER(g_iPedComponentSlot)])
							sCatalogueData.m_bitshift = ENUM_TO_INT(eCompName)%32
						ENDIF
						
						IF sCatalogueData.m_stathash != 0
							ADD_ITEM_TO_CATALOG_WITH_CHECK(sCatalogueData)
						ENDIF
					ENDIF
				#ENDIF
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
			
				// available
				//IF CALL sData.fpIsPedCompItemCurrent(PLAYER_PED_ID(), eCompType, eCompName)
				//IF SHOULD_WE_DISPLAY_SCISSORS_ICON(sData,iMenuItem)
				IF IS_HAIR_OR_BEARD_ITEM_CURRENT(sData,iMenuItem)	
					// current
					ADD_MENU_ITEM_TEXT(iMenuItem, sItemData.sLabel)
					
					ADD_MENU_ITEM_TEXT(iMenuItem, "", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_SCISSORS)
					
					iCurrentItem = iMenuItem

					#IF IS_DEBUG_BUILD
						CPRINTLN(DEBUG_SHOPS, "BUILD_HAIR_MENU - '", GET_STRING_FROM_TEXT_FILE(sItemData.sLabel), "' is current")
					#ENDIF
					
				ELSE
					// available, not current

					// name and star if new
					IF (IS_BIT_SET(sItemData.iProperties, PED_COMPONENT_IS_NEW_BIT))
					OR (bAddStarToSelectedItem AND iMenuItem = sData.sBrowseInfo.iCurrentItem)
						ADD_MENU_ITEM_TEXT(iMenuItem, sItemData.sLabel, 1)
						ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
						
						// FLAG: NEW
						IF iMenuItem = sData.sBrowseInfo.iCurrentItem
							sData.bSelectedItemIsNew = TRUE
						ENDIF
					ELSE
						ADD_MENU_ITEM_TEXT(iMenuItem, sItemData.sLabel)
					ENDIF
					
					// cost
					iDisplayCost = GET_HAIRDO_ITEM_COST(sData, sItemData, ePedModel, eCompName, FALSE)
					IF iDisplayCost > 0
						ADD_MENU_ITEM_TEXT(iMenuItem, "ITEM_COST", 1)
						ADD_MENU_ITEM_TEXT_COMPONENT_INT(iDisplayCost)
					ELSE
						ADD_MENU_ITEM_TEXT(iMenuItem, "ITEM_FREE")
					ENDIF
				ENDIF
			ENDIF
		ENDREPEAT
	ENDIF

	// highlight current item
	IF sData.bBlockAutoItemSelect
		sData.bBlockAutoItemSelect = FALSE
		SET_TOP_MENU_ITEM(sData.iTopMenuItem)
	ELSE
		sData.sBrowseInfo.iCurrentItem = iCurrentItem
	ENDIF
	SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
	
	// Try this on.
	CPRINTLN(DEBUG_SHOPS, "BUILD_HAIR_MENU. Try this on.: sData.sBrowseInfo.iCurrentItem = ", sData.sBrowseInfo.iCurrentItem)
	CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem], FALSE)
	
	// FLAG: DLC
	sData.bSelectedItemIsDLC = IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT)
	
	GET_CURRENT_HAIRDO_COST(sData)
	
	// set up button labels
	IF sData.bAllowZoomControl
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_BUY")
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
		//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD, "ITEM_SCROLL")
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
		IF NOT IS_NGMP_MENU(sData.eCurrentMenu)
			ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
		ENDIF
		//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_RT, "ITEM_FSCROLL")
	ELSE
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_BUY")
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
		//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD, "ITEM_SCROLL")
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
		//ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
		//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_RT, "ITEM_FSCROLL")
	ENDIF
ENDPROC

FUNC INT GET_THIS_PED_HEAD_OVERLAY_NUM()
	MODEL_NAMES eModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
	IF eModel = MP_M_FREEMODE_01
		RETURN 30
	ENDIF
	
	RETURN ENUM_TO_INT(MP_BEARDS_MAX)	//GET_PED_HEAD_OVERLAY_NUM(1)
ENDFUNC

FUNC BOOL IS_THIS_MAKEUP_PART_OF_SPORTS_FANATIC_DLC(INT iMakeup)
	
	CLOTHES_UNLOCK_ITEMS eMakeup = GET_MAKEUP_UNLOCK_ENUM_FOR_SHOP(iMakeup)
	
	IF eMakeup = MAKEUP_UNLOCKS_THEJOCK
	OR eMakeup = MAKEUP_UNLOCKS_LOSSANTOSCORKERS
	OR eMakeup = MAKEUP_UNLOCKS_LOSSANTOSPANIC
	OR eMakeup = MAKEUP_UNLOCKS_LIBERTYCITYSWINGERS
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC


FUNC BOOL IS_THIS_MAKEUP_PART_OF_INDEPENDENCE_DLC(INT iMakeup)
	
	CLOTHES_UNLOCK_ITEMS eMakeup = GET_MAKEUP_UNLOCK_ENUM_FOR_SHOP(iMakeup)
	
	IF eMakeup = MAKEUP_UNLOCKS_STARS_N_STRIPES
		RETURN TRUE
	ENDIF

	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_MAKEUP_PART_OF_ARMY_DLC(INT iMakeup)
	#IF NOT FEATURE_ARMY
	UNUSED_PARAMETER(iMakeup)
	#ENDIF
	#IF FEATURE_ARMY
	
	CLOTHES_UNLOCK_ITEMS eMakeup = GET_MAKEUP_UNLOCK_ENUM_FOR_SHOP(iMakeup)
	
	IF eMakeup = MAKEUP_UNLOCKS_ARMY_A
	OR eMakeup = MAKEUP_UNLOCKS_ARMY_B
	OR eMakeup = MAKEUP_UNLOCKS_ARMY_C
	OR eMakeup = MAKEUP_UNLOCKS_ARMY_D
	OR eMakeup = MAKEUP_UNLOCKS_ARMY_E
	OR eMakeup = MAKEUP_UNLOCKS_ARMY_F
	OR eMakeup = MAKEUP_UNLOCKS_ARMY_G
	OR eMakeup = MAKEUP_UNLOCKS_ARMY_H
	OR eMakeup = MAKEUP_UNLOCKS_ARMY_I
		RETURN TRUE
	ENDIF
	#ENDIF

	RETURN FALSE
ENDFUNC


FUNC BOOL CHECK_THIS_MAKEUP_PART_FOR_DLC_VALIDITY(INT iMakeup)
	/*
	IF NOT IS_THIS_MAKEUP_PART_OF_SPORTS_FANATIC_DLC(iMakeup)
	OR IS_SPORTS_FANATIC_DLC_INSTALLED_FOR_MAKEUP()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
	*/
	
	IF IS_THIS_MAKEUP_PART_OF_SPORTS_FANATIC_DLC(iMakeup)
		
		IF NOT IS_SPORTS_FANATIC_DLC_INSTALLED_FOR_MAKEUP()
			
			PRINTLN("CHECK_THIS_MAKEUP_PART_FOR_DLC_VALIDITY(", iMakeup, ":SPORTS) - not installed")
			
			RETURN FALSE
		ENDIF
		
		PRINTLN("CHECK_THIS_MAKEUP_PART_FOR_DLC_VALIDITY(", iMakeup, ":SPORTS) - installed")
		
	ELIF IS_THIS_MAKEUP_PART_OF_INDEPENDENCE_DLC(iMakeup)
		IF NOT IS_INDEPENDENCE_DLC_INSTALLED_FOR_MAKEUP()
			
			PRINTLN("CHECK_THIS_MAKEUP_PART_FOR_DLC_VALIDITY(", iMakeup, ":INDEPENDENCE) - not installed")
			
			RETURN FALSE
		ENDIF
		
		PRINTLN("CHECK_THIS_MAKEUP_PART_FOR_DLC_VALIDITY(", iMakeup, ":INDEPENDENCE) - installed")
		
	ELIF IS_THIS_MAKEUP_PART_OF_ARMY_DLC(iMakeup)
#IF FEATURE_ARMY
		IF NOT IS_ARMY_DLC_INSTALLED_FOR_MAKEUP()
			
			PRINTLN("CHECK_THIS_MAKEUP_PART_FOR_DLC_VALIDITY(", iMakeup, ":ARMY) - not installed")
			
			RETURN FALSE
		ENDIF
		PRINTLN("CHECK_THIS_MAKEUP_PART_FOR_DLC_VALIDITY(", iMakeup, ":ARMY) - installed")
#ENDIF
#IF NOT FEATURE_ARMY
		PRINTLN("CHECK_THIS_MAKEUP_PART_FOR_DLC_VALIDITY(", iMakeup, ":ARMY) - not installed/not featured")
		RETURN FALSE
#ENDIF
	ENDIF
	
	RETURN TRUE
ENDFUNC




FUNC BOOL IS_NGMP_UNLOCKED_FOR_SHOP(HAIRDO_SHOP_STRUCT &sData, MODEL_NAMES ePedModel, HAIRDO_MENU_ENUM eMenu, INT iTempItem)
	NGMP_MENU_OPTION_DATA sOptionData
	IF GET_NGMP_MENU_OPTION_DATA(ePedModel, eMenu, iTempItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
		
		IF NOT IS_DLC_INSTALLED_FOR_NGMP_ITEM(ePedModel, eMenu, iTempItem, sData.eInShop)
			CDEBUG1LN(DEBUG_SHOPS, "locked DLC item.GET_NGMP_MENU_OPTION_DATA(", GET_LABEL_FOR_NGMP_MENU(eMenu), "): tlLabel: ", sOptionData.tlLabel, "], eHairItem: ", sOptionData.eHairItem)
			RETURN FALSE
		ENDIF
		
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
			TEXT_LABEL_63 tlCatalogueKey
			GENERATE_HAIRDO_KEY_FOR_CATALOGUE(tlCatalogueKey, eMenu, sOptionData.tlLabel, ePedModel)
			IF NOT NET_GAMESERVER_CATALOG_ITEM_IS_VALID(tlCatalogueKey)
				CDEBUG1LN(DEBUG_SHOPS, "invalid key.GET_NGMP_MENU_OPTION_DATA(", GET_LABEL_FOR_NGMP_MENU(eMenu),
						"): tlLabel: ", sOptionData.tlLabel,
						" \"", GET_STRING_FROM_TEXT_FILE(sOptionData.tlLabel),
						"\", eHairItem: ", sOptionData.eHairItem,
						"\", tlCatalogueKey: ", tlCatalogueKey)
				RETURN FALSE
			ENDIF
		ENDIF
		
		// NG MP Ped Comp
		IF sOptionData.iType = 0
			PED_COMP_ITEM_DATA_STRUCT sItemData
			sItemData = CALL sData.fpGetPedCompDataForItem(ePedModel, COMP_TYPE_HAIR, sOptionData.eHairItem)
			
			IF NOT (IS_BIT_SET(sItemData.iProperties, PED_COMPONENT_AVAILABLE_BIT))
				CDEBUG1LN(DEBUG_SHOPS, "locked comp item.GET_NGMP_MENU_OPTION_DATA(", GET_LABEL_FOR_NGMP_MENU(eMenu),
						"): tlLabel: ", sOptionData.tlLabel,
						" \"", GET_STRING_FROM_TEXT_FILE(sOptionData.tlLabel),
						"\", eHairItem: ", sOptionData.eHairItem)
				RETURN FALSE
			ENDIF
			
			CDEBUG1LN(DEBUG_SHOPS, "unlocked comp item.GET_NGMP_MENU_OPTION_DATA(", GET_LABEL_FOR_NGMP_MENU(eMenu),
					"): tlLabel: ", sOptionData.tlLabel,
					" \"", GET_STRING_FROM_TEXT_FILE(sOptionData.tlLabel),
					"\", eHairItem: ", sOptionData.eHairItem)
			
			RETURN TRUE
			
		// NG MP Overlay
		ELIF sOptionData.iType = 1
		OR sOptionData.iType = 2
		
			IF (sOptionData.eUnlockItem = DUMMY_CLOTHES_UNLOCK)
				CDEBUG1LN(DEBUG_SHOPS, "unlocked dummy item.GET_NGMP_MENU_OPTION_DATA(", GET_LABEL_FOR_NGMP_MENU(eMenu), "): tlLabel: ", sOptionData.tlLabel, "], eHairItem: ", sOptionData.eHairItem)
				RETURN TRUE
			ENDIF
			
			IF sOptionData.eHeadSlot = HOS_FACIAL_HAIR
				IF NOT IS_MP_CLOTHES_UNLOCKED(sOptionData.eUnlockItem)
					CDEBUG1LN(DEBUG_SHOPS, "locked facial hair item.GET_NGMP_MENU_OPTION_DATA(\"", GET_STRING_FROM_TEXT_FILE(sOptionData.tlLabel), "\", ", sOptionData.tlLabel,
							"), iTexture: ", sOptionData.iTexture,
							", locked[", sOptionData.eUnlockItem,
							"]: ", GET_FM_MAKEUP_UNLOCK_RANK(sOptionData.eUnlockItem))
					RETURN FALSE
				ENDIF
				
				CDEBUG1LN(DEBUG_SHOPS, "unlocked facial hair item.GET_NGMP_MENU_OPTION_DATA(\"", GET_STRING_FROM_TEXT_FILE(sOptionData.tlLabel), "\", ", sOptionData.tlLabel,
						"), iTexture: ", sOptionData.iTexture,
						", unlocked[", sOptionData.eUnlockItem,
						"]: ", GET_FM_MAKEUP_UNLOCK_RANK(sOptionData.eUnlockItem))
				RETURN TRUE
			ELIF sOptionData.eHeadSlot = HOS_MAKEUP
				IF NOT IS_MP_CLOTHES_UNLOCKED(sOptionData.eUnlockItem)
					CDEBUG1LN(DEBUG_SHOPS, "locked makeup item.GET_NGMP_MENU_OPTION_DATA(", GET_LABEL_FOR_NGMP_MENU(eMenu), "): tlLabel: ", sOptionData.tlLabel, "], eHairItem: ", sOptionData.eHairItem, ", unlocked[", sOptionData.eUnlockItem, "]: ", IS_MP_CLOTHES_UNLOCKED(sOptionData.eUnlockItem))
					RETURN FALSE
				ENDIF
				
				CDEBUG1LN(DEBUG_SHOPS, "unlocked makeup item.GET_NGMP_MENU_OPTION_DATA(", GET_LABEL_FOR_NGMP_MENU(eMenu), "): tlLabel: ", sOptionData.tlLabel, "], eHairItem: ", sOptionData.eHairItem, ", unlocked[", sOptionData.eUnlockItem, "]: ", IS_MP_CLOTHES_UNLOCKED(sOptionData.eUnlockItem))
				RETURN TRUE
			ELSE
				CDEBUG1LN(DEBUG_SHOPS, "unlocked/locked overlay item.GET_NGMP_MENU_OPTION_DATA(", GET_LABEL_FOR_NGMP_MENU(eMenu), "): tlLabel: ", sOptionData.tlLabel, "], eHairItem: ", sOptionData.eHairItem, ", iType: overlay ", sOptionData.iType)
			
				RETURN TRUE
			ENDIF
		ENDIF
	ENDIF
	
	CPRINTLN(DEBUG_SHOPS, "	IS_NGMP_UNLOCKED_FOR_SHOP(HME_OTHER): iTempItem: ", iTempItem, " RETURN TRUE")
	RETURN TRUE
ENDFUNC
FUNC BOOL HAS_NGMP_BEEN_VIEWED_IN_SHOP(HAIRDO_SHOP_STRUCT &sData, MODEL_NAMES ePedModel, HAIRDO_MENU_ENUM eMenu, INT iTempItem)
	NGMP_MENU_OPTION_DATA sOptionData
	IF GET_NGMP_MENU_OPTION_DATA(ePedModel, eMenu, iTempItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
		IF NOT IS_BIT_SET_HAIRDO_NGMP_SCRIPT(sOptionData.iUniqueHash, PED_COMPONENT_USED_SLOT)
			CDEBUG1LN(DEBUG_SHOPS, "unviewed item item.GET_NGMP_MENU_OPTION_DATA( ", GET_LABEL_FOR_NGMP_MENU(eMenu),
					"): hash:", sOptionData.iUniqueHash, "): \"", sOptionData.tlLabel, "\", iTexture: ", sOptionData.iTexture)
			RETURN FALSE
		ENDIF
		
		CDEBUG1LN(DEBUG_SHOPS, "viewed iUniqueHash item.GET_NGMP_MENU_OPTION_DATA(", GET_LABEL_FOR_NGMP_MENU(eMenu),
				"): hash:", sOptionData.iUniqueHash, "): \"", sOptionData.tlLabel, "\", iTexture: ", sOptionData.iTexture)
		RETURN TRUE
	ENDIF
	
	UNUSED_PARAMETER(sData)
	CPRINTLN(DEBUG_SHOPS, "	HAS_NGMP_BEEN_VIEWED_IN_SHOP(HME_OTHER): iTempItem: ", iTempItem, " RETURN TRUE")
	RETURN TRUE
ENDFUNC

FUNC BOOL HAS_MENU_GOT_NEW_ITEMS(HAIRDO_SHOP_STRUCT &sData, MODEL_NAMES ePedModel, HAIRDO_MENU_ENUM eMenu)

	IF eMenu = HME_HAIR
		REPOPULATE_HAIRDO_MENU(sData.fpSetupClothingItemForShop, g_sShopCompItemData, TRUE, IS_SALON_ACTIVE(sData), HAIR_GROUP_ALL, sData.iHairdoGroupsAvailable, sData.iHairdoGroupsWithNewItems)
		INT iTempItem
		REPEAT g_sShopCompItemData.iItemCount iTempItem
			IF (CALL sData.fpIsPedCompItemNew(ePedModel, g_sShopCompItemData.eTypes[iTempItem], g_sShopCompItemData.eItems[iTempItem]))
			AND (CALL sData.fpIsPedCompItemAvailable(ePedModel, g_sShopCompItemData.eTypes[iTempItem], g_sShopCompItemData.eItems[iTempItem]))
				RETURN TRUE
			ENDIF
		ENDREPEAT
		
	ELIF eMenu = HME_BEARD	
		IF NOT NETWORK_IS_GAME_IN_PROGRESS()
			REPOPULATE_HAIRDO_MENU(sData.fpSetupClothingItemForShop, g_sShopCompItemData, FALSE, IS_SALON_ACTIVE(sData), HAIR_GROUP_ALL, sData.iHairdoGroupsAvailable, sData.iHairdoGroupsWithNewItems)
			INT iTempItem
			REPEAT g_sShopCompItemData.iItemCount iTempItem
				IF (CALL sData.fpIsPedCompItemNew(ePedModel, g_sShopCompItemData.eTypes[iTempItem], g_sShopCompItemData.eItems[iTempItem]))
				AND (CALL sData.fpIsPedCompItemAvailable(ePedModel, g_sShopCompItemData.eTypes[iTempItem], g_sShopCompItemData.eItems[iTempItem]))
					RETURN TRUE
				ENDIF
			ENDREPEAT
		ELSE
			INT iBeard
			INT iBeardCount = GET_THIS_PED_HEAD_OVERLAY_NUM()
			
			REPEAT iBeardCount iBeard
				IF IS_BEARD_UNLOCKED_FOR_SHOP(iBeard)
				AND NOT HAS_BEARD_BEEN_VIEWED_IN_SHOP(iBeard)
					RETURN TRUE
				ENDIF
			ENDREPEAT
		ENDIF
	ELIF eMenu = HME_MAKEUP
		INT iTempItem = 0
		WHILE iTempItem < sData.iNumMakeupItems
			IF CHECK_THIS_MAKEUP_PART_FOR_DLC_VALIDITY(iTempItem)
				IF IS_MAKEUP_UNLOCKED_FOR_SHOP(iTempItem)
				AND NOT HAS_MAKEUP_BEEN_VIEWED_IN_SHOP(iTempItem)
					RETURN TRUE
				ENDIF
			ENDIF
			++ iTempItem
		ENDWHILE
	
	ELIF IS_NGMP_MENU(eMenu)
		INT iTempItem
		REPEAT GET_NUMBER_OF_NGMP_MENU_OPTIONS(ePedModel, eMenu) iTempItem
			IF IS_NGMP_UNLOCKED_FOR_SHOP(sData, ePedModel, eMenu, iTempItem)
			AND NOT HAS_NGMP_BEEN_VIEWED_IN_SHOP(sData, ePedModel, eMenu, iTempItem)
				
				CPRINTLN(DEBUG_SHOPS, "HAS_MENU_GOT_NEW_ITEMS - item ", iTempItem, " of menu ", GET_LABEL_FOR_NGMP_MENU(eMenu), " is unlocked and unviewed")
				
				RETURN TRUE
			ENDIF
		ENDREPEAT
		
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
	
ENDFUNC



FUNC BOOL SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP(STRING paramLabel, HAIRDO_MENU_ENUM eMenu, INT iCost)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		RETURN FALSE
	ENDIF
	
	IF (iCost = 0)
		CDEBUG1LN(DEBUG_SHOPS, "should_discount_be_displayed_for_ngmp FREE ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
		RETURN FALSE
	ENDIF
	
	SWITCH eMenu
		CASE HME_NGMP_HAIR
			IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_HAIR
				CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP_HAIR ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
				RETURN TRUE
			ENDIF
		BREAK
		CASE HME_NGMP_BEARD
			IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_BEARD
				CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP_BEARD ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
				RETURN TRUE
			ENDIF
		BREAK
		CASE HME_NGMP_EYEBROWS
			IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_EYEBROWS
				CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP_EYEBROWS ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
				RETURN TRUE
			ENDIF
		BREAK
		CASE HME_NGMP_CHEST
			IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_CHEST
				CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP_CHEST ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
				RETURN TRUE
			ENDIF
		BREAK
		CASE HME_NGMP_CONTACTS
			IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_CONTACTS
				CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP_CONTACTS ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
				RETURN TRUE
			ENDIF
		BREAK
		CASE HME_NGMP_FACEPAINT
			IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_FACEPAINT
				CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP_FACEPAINT ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
				RETURN TRUE
			ENDIF
		BREAK
		CASE HME_NGMP_MAKEUP_BLUSHER
			IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_MAKEUP
				CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP_MAKEUP ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
				RETURN TRUE
			ENDIF
			IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_MAKEUP_BLUSHER
				CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP_MAKEUP_BLUSHER ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
				RETURN TRUE
			ENDIF
		BREAK
		CASE HME_NGMP_MAKEUP_EYE
			IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_MAKEUP
				CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP_MAKEUP ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
				RETURN TRUE
			ENDIF
			IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_MAKEUP_EYE
				CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP_MAKEUP_EYE ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
				RETURN TRUE
			ENDIF
		BREAK
		CASE HME_NGMP_MAKEUP_LIPSTICK
			IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_MAKEUP
				CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP_MAKEUP ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
				RETURN TRUE
			ENDIF
			IF g_sMPTunables.BSHOULD_ITEM_BE_DISCOUNTED_HME_NGMP_MAKEUP_LIPSTICK
				CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP_MAKEUP_LIPSTICK ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\"")
				RETURN TRUE
			ENDIF
		BREAK
	ENDSWITCH
	
	INT i, iLabelHash = GET_HASH_KEY(paramLabel)
	REPEAT COUNT_OF(g_sMPTunables.iINDIVIDUAL_HAIRCUT_SALE_HASH_LABELS) i
		IF (g_sMPTunables.iINDIVIDUAL_HAIRCUT_SALE_HASH_LABELS[i] != 0)
			IF (iLabelHash = g_sMPTunables.iINDIVIDUAL_HAIRCUT_SALE_HASH_LABELS[i])
				CPRINTLN(DEBUG_SHOPS, "SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP_HASH_", i, " ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\" ", iLabelHash)
				RETURN TRUE
			ENDIF
		ENDIF
	ENDREPEAT
	
	CDEBUG1LN(DEBUG_SHOPS, "should_discount_be_displayed_FOR_NGMP ", paramLabel, " \"", GET_STRING_FROM_TEXT_FILE(paramLabel), "\" ", iLabelHash)
	RETURN FALSE
ENDFUNC

FUNC BOOL DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_NGMP(HAIRDO_SHOP_STRUCT &sData, MODEL_NAMES ePedModel, HAIRDO_MENU_ENUM eMenu)
	
	IF IS_NGMP_MENU(eMenu)
		NGMP_MENU_OPTION_DATA sOptionData
		INT iTempItem
		REPEAT GET_NUMBER_OF_NGMP_MENU_OPTIONS(ePedModel, eMenu) iTempItem
			IF GET_NGMP_MENU_OPTION_DATA(ePedModel, eMenu, iTempItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
				IF IS_NGMP_UNLOCKED_FOR_SHOP(sData, ePedModel, eMenu, iTempItem)
				AND SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP(sOptionData.tlLabel, eMenu, sOptionData.iCost)
				AND NOT IS_NGMP_ITEM_AND_TINTS_CURRENT(ePedModel, sOptionData, sData)
					CPRINTLN(DEBUG_SHOPS, "DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_NGMP - item ", iTempItem, " of menu ", GET_LABEL_FOR_NGMP_MENU(eMenu), " is unlocked and discounted")
					
					RETURN TRUE
				ENDIF
			ENDIF
		ENDREPEAT
		
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_THIS_BEARD_DLC(INT iBeardIndex)
	
	CLOTHES_UNLOCK_ITEMS eBeard = GET_BEARD_UNLOCK_ENUM_FOR_SHOP(iBeardIndex)
	
	IF eBeard = BEARD_UNLOCKS_MUTTONCHOPS
		RETURN TRUE
	ENDIF
	
	IF iBeardIndex >= ENUM_TO_INT(MP_BEARDS_MAX)+1 // +1 for clean shaven
		RETURN TRUE
	ENDIF
	
	RETURN FALSE
ENDFUNC

FUNC BOOL IS_DLC_INSTALLED_FOR_BEARD(INT iBeardIndex)
	//TODO Fill this in when dlc is setup and we get a command to check it
	
	IF iBeardIndex >= ENUM_TO_INT(MP_BEARDS_MAX)+1 // +1 for clean shaven
		//Hipster Beards
		IF iBeardIndex >= 27 AND iBeardIndex <= DLC_MP_BEARDS_MAX
			RETURN TRUE
		ENDIF
		
		//non-hipster beards
		RETURN FALSE
	ENDIF
	
	RETURN FALSE
ENDFUNC

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////    NG MP PROCS   ////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
PED_COMP_NAME_ENUM eCurrentJbib = DUMMY_PED_COMP

PROC REMOVE_PED_CHEST_CLOTHES(PED_INDEX pedID, HAIRDO_MENU_ENUM eMenu)
	CDEBUG1LN(DEBUG_SHOPS, "REMOVE_PED_CHEST_CLOTHES(", GET_LABEL_FOR_NGMP_MENU(eMenu), ")")
	
	IF (pedID != PLAYER_PED_ID())
		BOOL bUpdate_tatoos_mp = FALSE
		
		IF (GET_ENTITY_MODEL(pedID) = MP_M_FREEMODE_01)
			IF eMenu = HME_NGMP_CHEST
				// remove chest jbjb and accs
				IF GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_JBIB) != 15
					eCurrentJbib = GET_PED_COMPONENT_ITEM_CURRENT_FROM_LOOKUP(pedID, COMP_TYPE_JBIB)
					SET_PED_COMP_ITEM_CURRENT_MP(pedID, COMP_TYPE_JBIB, GET_DUMMY_COMPONENT_FOR_SLOT(GET_ENTITY_MODEL(pedID), COMP_TYPE_JBIB, -1), FALSE)
					
					CPRINTLN(DEBUG_SHOPS, "remove clone peds jbib.")
					SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_JBIB, 15, 0)
					bUpdate_tatoos_mp = TRUE
				ENDIF
				IF GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_TORSO) != 15
					CPRINTLN(DEBUG_SHOPS, "remove clone peds torso.")
					SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO, 15, 0)
					bUpdate_tatoos_mp = TRUE
				ENDIF
				IF GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_SPECIAL) != 15
					CPRINTLN(DEBUG_SHOPS, "remove clone peds special.")
					SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_SPECIAL, 15, 0)
					bUpdate_tatoos_mp = TRUE
				ENDIF
				IF GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_SPECIAL2) != 0
					CPRINTLN(DEBUG_SHOPS, "remove clone peds special2.")
					SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_SPECIAL2, 0, 0)
					bUpdate_tatoos_mp = TRUE
				ENDIF
				IF GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_DECL) != 0
					CPRINTLN(DEBUG_SHOPS, "remove clone peds decl.")
					SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_DECL, 0, 0)
					bUpdate_tatoos_mp = TRUE
				ENDIF
				IF GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_HAND) != 0
					CPRINTLN(DEBUG_SHOPS, "remove clone peds hand.")
					SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HAND, 0, 0)
					bUpdate_tatoos_mp = TRUE
				ENDIF
	//			IF GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_FEET) != 5
	//				CPRINTLN(DEBUG_SHOPS, "remove clone peds feet.")
	//				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_FEET, 5, 0)
	//				bUpdate_tatoos_mp = TRUE
	//			ENDIF
	//			IF GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_LEG) != 14 //12	
	//				CPRINTLN(DEBUG_SHOPS, "remove clone peds leg.")
	//				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, 14, 0)
	//				bUpdate_tatoos_mp = TRUE
	//			ENDIF
				IF GET_PED_DRAWABLE_VARIATION(pedID,  PED_COMP_TEETH) != 0	
					CPRINTLN(DEBUG_SHOPS, "remove clone peds teeth.")
					SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TEETH, 0, 0)
					bUpdate_tatoos_mp = TRUE
				ENDIF
				
				IF bUpdate_tatoos_mp
					UPDATE_TATOOS_MP(pedID)
					REMOVE_TATTOOS_IN_UPGRADE_GROUP_MP(GET_HASH_KEY("torsoDecal"), pedID)
				ENDIF
			ELSE
				// restore chest jbjb and accs
				IF GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_JBIB) != GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB)
					CPRINTLN(DEBUG_SHOPS, "restore clone peds jbib.")
					SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_JBIB, GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_JBIB))
					bUpdate_tatoos_mp = TRUE
				ENDIF
				IF GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_TORSO) != GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO)
					CPRINTLN(DEBUG_SHOPS, "restore clone peds torso.")
					SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TORSO, GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_TORSO))
					bUpdate_tatoos_mp = TRUE
				ENDIF
				IF GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_SPECIAL) != GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL)
					
					INT iDrawable = GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL)
					IF iDrawable = GET_CHEMSUIT_HOOD_VARIATION(pedID,TRUE)
						CPRINTLN(DEBUG_SHOPS, "restore clone peds special [", iDrawable, ", ", GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL), "] - hood down.")
						SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_SPECIAL, GET_CHEMSUIT_HOOD_VARIATION(PLAYER_PED_ID(), FALSE), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL))
						bUpdate_tatoos_mp = TRUE
					ELSE
						CPRINTLN(DEBUG_SHOPS, "restore clone peds special [", iDrawable, ", ", GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL), "].")
						SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_SPECIAL, GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL))
						bUpdate_tatoos_mp = TRUE
					ENDIF
					
				ENDIF
				IF GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_SPECIAL2) != GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2)
					CPRINTLN(DEBUG_SHOPS, "restore clone peds special2.")
					SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_SPECIAL2, GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2))
					bUpdate_tatoos_mp = TRUE
				ENDIF
				IF GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_DECL) != GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL)
					CPRINTLN(DEBUG_SHOPS, "restore clone peds decl.")
					SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_DECL, GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_DECL))
					bUpdate_tatoos_mp = TRUE
				ENDIF
				IF GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_HAND) != GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND)
					CPRINTLN(DEBUG_SHOPS, "restore clone peds hand.")
					SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_HAND, GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_HAND))
					bUpdate_tatoos_mp = TRUE
				ENDIF
	//			IF GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_FEET) != GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET)
	//				CPRINTLN(DEBUG_SHOPS, "restore clone peds feet.")
	//				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_FEET, GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_FEET))
	//				bUpdate_tatoos_mp = TRUE
	//			ENDIF
	//			IF GET_PED_DRAWABLE_VARIATION(pedID, PED_COMP_LEG) != GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG)
	//				CPRINTLN(DEBUG_SHOPS, "restore clone peds leg.")
	//				SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_LEG, GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_LEG))
	//				bUpdate_tatoos_mp = TRUE
	//			ENDIF
				IF GET_PED_DRAWABLE_VARIATION(pedID,  PED_COMP_TEETH) != GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH)
					CPRINTLN(DEBUG_SHOPS, "restore clone peds teeth.")
					SET_PED_COMPONENT_VARIATION(pedID, PED_COMP_TEETH, GET_PED_DRAWABLE_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH), GET_PED_TEXTURE_VARIATION(PLAYER_PED_ID(), PED_COMP_TEETH))
					bUpdate_tatoos_mp = TRUE
				ENDIF
				
				IF bUpdate_tatoos_mp
					IF eCurrentJbib != DUMMY_PED_COMP
						SET_PED_COMP_ITEM_CURRENT_MP(pedID, COMP_TYPE_JBIB, eCurrentJbib, FALSE)
					ENDIF
					eCurrentJbib = DUMMY_PED_COMP
					UPDATE_TATOOS_MP(pedID)
				ENDIF
			ENDIF
		ELSE
			//no need for female
		ENDIF
	ELSE
		//dont perform on player
	ENDIF
ENDPROC

PROC TRY_ON_NGMP_MENU_OPTION(HAIRDO_SHOP_STRUCT &sData, PED_INDEX pedID, HAIRDO_MENU_ENUM eMenu, INT iOption)
	CDEBUG1LN(DEBUG_SHOPS, "TRY_ON_NGMP_MENU_OPTION(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ")")
	
	NGMP_MENU_OPTION_DATA sOptionData
	GET_NGMP_MENU_OPTION_DATA(GET_ENTITY_MODEL(pedID), eMenu, iOption, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
	SWITCH sOptionData.iType
		// Ped comp
		CASE 0
			CALL sData.fpSetPedCompItemCurrent(pedID, COMP_TYPE_HAIR, sOptionData.eHairItem, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sData.iTint1, sData.iTint2)
		BREAK
		// Overlay
		CASE 1
			IF (eMenu = HME_NGMP_FACEPAINT)
				IF (sOptionData.eHeadSlot = HOS_BLUSHER)
				AND (sData.eRampType = RT_NONE)
					CPRINTLN(DEBUG_SHOPS, "TRY_ON_NGMP_MENU_OPTION: update facepaint ramp type [RT_", sData.eRampType, " to RT_MAKEUP] for ", GET_CHARACTER_OVERLAY_TEXT(sOptionData.eHeadSlot))
					sData.eRampType = RT_MAKEUP
					
					SAFE_SET_PED_HEAD_OVERLAY(pedID,
							HOS_MAKEUP, -1, -1, -1, RT_NONE, 1.0)
				ELIF (sOptionData.eHeadSlot = HOS_MAKEUP)
					CPRINTLN(DEBUG_SHOPS, "TRY_ON_NGMP_MENU_OPTION: update facepaint ramp type [RT_", sData.eRampType, " to RT_NONE] for ", GET_CHARACTER_OVERLAY_TEXT(sOptionData.eHeadSlot))
					sData.eRampType = RT_NONE
					
					IF (GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_BLUSHER)) >= 7)
						SAFE_SET_PED_HEAD_OVERLAY(pedID,
								HOS_BLUSHER, -1, -1, -1, RT_MAKEUP, 1.0)
					ENDIF
				ENDIF
			ENDIF
			
			CHECK_RAMP_IS_VALID(sOptionData.eHeadSlot, sData.eRampType)
			SAFE_SET_PED_HEAD_OVERLAY(pedID, sOptionData.eHeadSlot, sOptionData.iTexture, sData.iTint1, sData.iTint2, sData.eRampType, sData.fBlend)
		BREAK
		// Eyes
		CASE 2
			CHECK_RAMP_IS_VALID(sOptionData.eHeadSlot, sData.eRampType)
			SAFE_SET_PED_HEAD_OVERLAY(pedID, sOptionData.eHeadSlot, sOptionData.iTexture, sData.iTint1, sData.iTint2, sData.eRampType, sData.fBlend)
		BREAK
	ENDSWITCH
	
	REMOVE_PED_CHEST_CLOTHES(pedID, eMenu)
ENDPROC

PROC RESET_PED_FOR_NGMP_MENU(PED_INDEX pedID, HAIRDO_SHOP_STRUCT &sData)
	IF (pedID = PLAYER_PED_ID())
		CDEBUG1LN(DEBUG_SHOPS, "RESET_PED_FOR_NGMP_MENU(PLAYER_PED_ID())")
	ELIF (pedID = g_pShopClonePed)
		CDEBUG1LN(DEBUG_SHOPS, "RESET_PED_FOR_NGMP_MENU(g_pShopClonePed)")
	ELSE
		CDEBUG1LN(DEBUG_SHOPS, "RESET_PED_FOR_NGMP_MENU(pedID_", NATIVE_TO_INT(pedID), ")")
	ENDIF
	
	// Reset Hair
	PED_COMP_NAME_ENUM eWhichHair = INT_TO_ENUM(PED_COMP_NAME_ENUM,  GET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT))
	IF (eWhichHair != DUMMY_PED_COMP)
		PED_COMP_NAME_ENUM eGRHairItem
		eGRHairItem = DUMMY_PED_COMP
		IF (GET_ENTITY_MODEL(pedID) = MP_M_FREEMODE_01)
			eGRHairItem = GET_MALE_HAIR(eWhichHair)
		ELIF (GET_ENTITY_MODEL(pedID) = MP_F_FREEMODE_01)
			eGRHairItem = GET_FEMALE_HAIR(eWhichHair)
		ENDIF
		
		IF (eGRHairItem != DUMMY_PED_COMP)
		AND (eWhichHair != eGRHairItem)
			CPRINTLN(DEBUG_SHOPS,"[RESET_PED_FOR_NGMP_MENU] gr_hair: replacing hair enum ", eWhichHair, " with gunrunning hair enum ", eGRHairItem)
			eWhichHair = eGRHairItem
		ENDIF
	ENDIF
	CALL sData.fpSetPedCompItemCurrent(pedID, COMP_TYPE_HAIR, eWhichHair, FALSE)
	
//	// Reset beard
//	SAFE_SET_PED_HEAD_OVERLAY(pedID, HOS_FACIAL_HAIR,	GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_FACIAL_HAIR)), sData.iTint1, GET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(HOS_FACIAL_HAIR)))
//	// Reset chest hair
//	SAFE_SET_PED_HEAD_OVERLAY(pedID, HOS_BODY_1, 0, sData.iTint1, 0, RT_NONE, 1.0)	//GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_BODY_1)), sData.iTint1, GET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(HOS_BODY_1)))
//	// Reset eyebrows
//	SAFE_SET_PED_HEAD_OVERLAY(pedID, HOS_EYEBROW,		GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_EYEBROW)), sData.iTint1, GET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(HOS_EYEBROW)))
//	// Reset contacts
//	SAFE_SET_PED_HEAD_OVERLAY(pedID, HOS_BASE_DETAIL,	GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_BASE_DETAIL)), sData.iTint1, GET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(HOS_BASE_DETAIL)))
//	// Reset facepaint
//	SAFE_SET_PED_HEAD_OVERLAY(pedID, HOS_MAKEUP,		GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_MAKEUP)), sData.iTint1, GET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(HOS_MAKEUP)))
//	// Reset makeup - blusher
//	SAFE_SET_PED_HEAD_OVERLAY(pedID, HOS_BLUSHER,		GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_BLUSHER)), sData.iTint1, GET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(HOS_BLUSHER)))
//	// Reset makeup - eye makeup
//	SAFE_SET_PED_HEAD_OVERLAY(pedID, HOS_MAKEUP,		GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_MAKEUP)), sData.iTint1, GET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(HOS_MAKEUP)))
//	// Reset makeup - lipstick
//	SAFE_SET_PED_HEAD_OVERLAY(pedID, HOS_SKIN_DETAIL_1,	GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_SKIN_DETAIL_1)), sData.iTint1, GET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(HOS_SKIN_DETAIL_1)))
	
	CONST_INT iSlot -1
	APPLY_CHARACTER_CREATOR_FEATURES_FROM_STATS(pedID, iSlot, FALSE)
ENDPROC

PROC CLEAR_MENU_ITEM_NGMP_COMPONENT(HAIRDO_SHOP_STRUCT &sData)

	IF IS_PC_VERSION()
		BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_02.movieID, "SET_IS_PC")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF

//	SET_DATA_SLOT_EMPTY() // This clears the colours
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_01.movieID, "SET_DATA_SLOT_EMPTY")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_01.movieID, "SET_TITLE")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	IF IS_PC_VERSION()
		BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_02.movieID, "SET_IS_PC")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
		
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_02.movieID, "SET_DATA_SLOT_EMPTY")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
	
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_02.movieID, "SET_TITLE")
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC NGMP_STORE_CURRENT_SHOP_ITEM_DATA(MODEL_NAMES ePedModel, HAIRDO_MENU_ENUM eMenu, INT iOption, INT iVariant = 0)
	CDEBUG1LN(DEBUG_SHOPS, "NGMP_STORE_CURRENT_SHOP_ITEM_DATA(", GET_LABEL_FOR_NGMP_MENU(eMenu), ")")
	
	NGMP_MENU_OPTION_DATA sOptionData
	IF GET_NGMP_MENU_OPTION_DATA(ePedModel, eMenu, iOption, sOptionData, iVariant)
		STORE_CURRENT_SHOP_ITEM_DATA(sOptionData.tlLabel, sOptionData.iCost, DEFAULT, DEFAULT, DEFAULT, SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP(sOptionData.tlLabel, eMenu, sOptionData.iCost))
	ELSE
		CASSERTLN(DEBUG_SHOPS, "NGMP_STORE_CURRENT_SHOP_ITEM_DATA(", GET_LABEL_FOR_NGMP_MENU(eMenu), ", ", GET_MODEL_NAME_FOR_DEBUG(ePedModel), ", ", iOption, ")")
	ENDIF
ENDPROC

PROC BUILD_NGMP_TINT_MENU(HAIRDO_SHOP_STRUCT &sData, MODEL_NAMES ePedModel, BOOL bRebuildHelpKeys)
	CDEBUG1LN(DEBUG_SHOPS, "BUILD_NGMP_TINT_MENU(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ", ", GET_MODEL_NAME_FOR_DEBUG(ePedModel), ", bRebuildHelpKeys: ", bRebuildHelpKeys, "): iTint[", sData.iTint1, ", ", sData.iTint2, "]")
	
	LOAD_NGMP_MENU_ASSETS(sData)
	
	STRING sOpacity = "FACE_OPAC"
	STRING sColour01 = "FACE_COLOUR"
	
	FLOAT fOpacity =(sData.fBlend-fMIN_OPACITY_BLEND)*(1/(fMAX_OPACITY_BLEND-fMIN_OPACITY_BLEND))
	IF fOpacity >= 1.0
		fOpacity = 1.0
	ELIF fOpacity <= 0.0
		fOpacity = 0.0
	ENDIF
	fOpacity *= 100.0
	
	INT iTintIndex1 = GET_NGMP_TINTINDEX_FROM_TINT(ePedModel, sData.eCurrentMenu, sData.iTint1, sData.eInShop)
	sData.iMAX_TINT_COUNT = 0
	
	INT i
	REPEAT GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu) i
		IF IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, i, sData.eInShop)
			sData.iMAX_TINT_COUNT++
		ENDIF
	ENDREPEAT
	
	STRING sColour02 = ""
	INT iTintIndex2 = -1
	NGMP_MENU_OPTION_DATA sOptionData
	
	SWITCH sData.eCurrentMenu
		CASE HME_NGMP_HAIR
			sOpacity = ""				fOpacity = -1
			sColour02 = "FACE_COLHILI"
			
			IF CAN_EDIT_NGMP_TINTS_OF_CURRENT_ITEM(sData, ePedModel)
				IF sData.iTint1 = -1
					sData.iTint1 = SAFE_GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
				ENDIF
				IF sData.iTint2 = -1
					sData.iTint2 = SAFE_GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
					sData.iSavedTint2 = sData.iTint2
				ENDIF
				sData.fBlend = 1.0
				
				sColour02 = "FACE_COLHILI"
				iTintIndex1 = GET_NGMP_TINTINDEX_FROM_TINT(ePedModel, sData.eCurrentMenu, sData.iTint1, sData.eInShop)
				iTintIndex2 = GET_NGMP_TINTINDEX_FROM_TINT(ePedModel, sData.eCurrentMenu, sData.iTint2, sData.eInShop)
//				iMAX_TINTS = appearanceData_iColourMax
			ELSE
				sOpacity = ""				fOpacity = -1
				sColour01 = ""				iTintIndex1 = -1
				sColour02 = ""				iTintIndex2 = -1
				sData.iMAX_TINT_COUNT = -1
			ENDIF
		BREAK
		CASE HME_NGMP_CONTACTS
			sOpacity = ""				fOpacity = -1
			sColour01 = ""				iTintIndex1 = -1				sData.iMAX_TINT_COUNT = -1
			
			sData.iTint1 = GET_NGMP_TINT_FROM_TINTINDEX(ePedModel, sData.eCurrentMenu, iTintIndex1, sData.eInShop)
			sData.iTint2 = GET_NGMP_TINT_FROM_TINTINDEX(ePedModel, sData.eCurrentMenu, iTintIndex2, sData.eInShop)
			sData.iSavedTint2 = sData.iTint2
			sData.fBlend = 1.0
		BREAK
		CASE HME_NGMP_MAKEUP_EYE
			sColour01 = ""				iTintIndex1 = -1				sData.iMAX_TINT_COUNT = -1
			
			IF sData.sBrowseInfo.iCurrentItem = 0
				sOpacity = ""				fOpacity = -1
			ENDIF
			
			sData.iTint1 = GET_NGMP_TINT_FROM_TINTINDEX(ePedModel, sData.eCurrentMenu, iTintIndex1, sData.eInShop)
			sData.iTint2 = GET_NGMP_TINT_FROM_TINTINDEX(ePedModel, sData.eCurrentMenu, iTintIndex2, sData.eInShop)
			sData.iSavedTint2 = sData.iTint2
//			sData.fBlend = 1.0
		BREAK
		CASE HME_NGMP_FACEPAINT
			IF GET_NGMP_MENU_OPTION_DATA(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
				IF NOT CAN_EDIT_NGMP_TINTS_OF_CURRENT_ITEM(sData, ePedModel)
				OR sOptionData.eHeadSlot != HOS_BLUSHER
					sColour01 = ""				iTintIndex1 = -1				sData.iMAX_TINT_COUNT = -1
					
					IF sData.sBrowseInfo.iCurrentItem = 0
						sOpacity = ""				fOpacity = -1
					ENDIF
					
					sData.iTint1 = GET_NGMP_TINT_FROM_TINTINDEX(ePedModel, sData.eCurrentMenu, iTintIndex1, sData.eInShop)
					sData.iTint2 = GET_NGMP_TINT_FROM_TINTINDEX(ePedModel, sData.eCurrentMenu, iTintIndex2, sData.eInShop)
					sData.iSavedTint2 = sData.iTint2
		//			sData.fBlend = 1.0
				ELSE
					sData.iTint1 = GET_NGMP_TINT_FROM_TINTINDEX(ePedModel, sData.eCurrentMenu, iTintIndex1, sData.eInShop)
					IF sData.iTint1 = -1
						sData.iTint1 = GET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY(sOptionData.eHeadSlot, sData.eRampType)
						iTintIndex1 = GET_NGMP_TINTINDEX_FROM_TINT(ePedModel, sData.eCurrentMenu, sData.iTint1, sData.eInShop)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		CASE HME_NGMP_MAKEUP_BLUSHER
		CASE HME_NGMP_MAKEUP_LIPSTICK
		CASE HME_NGMP_BEARD
		CASE HME_NGMP_EYEBROWS
		CASE HME_NGMP_CHEST
			IF GET_NGMP_MENU_OPTION_DATA(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
				IF NOT CAN_EDIT_NGMP_TINTS_OF_CURRENT_ITEM(sData, ePedModel)
					sOpacity = ""				fOpacity = -1
					sColour01 = ""				iTintIndex1 = -1				sData.iMAX_TINT_COUNT = -1
				ELSE
					sData.iTint1 = GET_NGMP_TINT_FROM_TINTINDEX(ePedModel, sData.eCurrentMenu, iTintIndex1, sData.eInShop)
					IF sData.iTint1 = -1
						sData.iTint1 = GET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY(sOptionData.eHeadSlot, sData.eRampType)
						iTintIndex1 = GET_NGMP_TINTINDEX_FROM_TINT(ePedModel, sData.eCurrentMenu, sData.iTint1, sData.eInShop)
					ENDIF
				ENDIF
			ENDIF
		BREAK
		
		DEFAULT
			CASSERTLN(DEBUG_SHOPS, "BUILD_NGMP_TINT_MENU - unknown sData.eCurrentMenu")
		BREAK
	ENDSWITCH
	
	CLEAR_MENU_ITEM_NGMP_COMPONENT(sData)
	
	// Add PC arrows and support for mouse events
	IF IS_PC_VERSION()
		BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_01.movieID, "SET_IS_PC")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
		
	IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_01.movieID, "SET_TITLE")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sOpacity)
		
		IF iTintIndex1 != -1
		AND sData.iMAX_TINT_COUNT != -1
			BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sColour01)
				ADD_TEXT_COMPONENT_INTEGER(iTintIndex1+1)
				ADD_TEXT_COMPONENT_INTEGER(sData.iMAX_TINT_COUNT)
			END_TEXT_COMMAND_SCALEFORM_STRING()
		ELSE
			CPRINTLN(DEBUG_SHOPS, "BUILD_NGMP_TINT_MENU invalid tint colours - ", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ", iTintIndex1:", iTintIndex1, ", iMAX_TINT_COUNT:", sData.iMAX_TINT_COUNT)
		ENDIF
		
		IF fOpacity != -1
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fOpacity)
		ENDIF
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
	
////	SET_DATA_SLOT_EMPTY() // This clears the colours
//	IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_01.movieID, "SET_DATA_SLOT_EMPTY")
//		END_SCALEFORM_MOVIE_METHOD()
//	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sColour01)
	//	SET_DATA_SLOT(index:int, r:int, g:int, b:int ) // set as many colours as you like
		INT iTintIndex
		INT iR, iG, iB
		REPEAT sData.iMAX_TINT_COUNT iTintIndex
			INT iTint = GET_NGMP_TINT_FROM_TINTINDEX(ePedModel, sData.eCurrentMenu, iTintIndex, sData.eInShop)
			
			// Get the RGB values for our paletter
			IF sData.eCurrentMenu = HME_NGMP_HAIR
			OR sData.eCurrentMenu = HME_NGMP_BEARD
			OR sData.eCurrentMenu = HME_NGMP_CHEST
			OR sData.eCurrentMenu = HME_NGMP_EYEBROWS
				GET_PED_HAIR_TINT_COLOR(iTint, iR, iG, iB)
			ELIF sData.eCurrentMenu = HME_NGMP_MAKEUP_BLUSHER
			OR sData.eCurrentMenu = HME_NGMP_MAKEUP_LIPSTICK
			OR sData.eCurrentMenu = HME_NGMP_FACEPAINT
				GET_PED_MAKEUP_TINT_COLOR(iTint, iR, iG, iB)
			ELSE
				CASSERTLN(DEBUG_SHOPS, "unknown menu choice for GET_PED_XXX_TINT_COLOR - ", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu))
			ENDIF
			
			CDEBUG3LN(DEBUG_SHOPS, "SET_DATA_SLOT(index:", iTintIndex, ", r:", iR, ", g:", iG, ", b:", iB, ")")
			IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_01.movieID, "SET_DATA_SLOT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTintIndex)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
		ENDREPEAT

	//	DISPLAY_VIEW() // draws the colours to the screen (call once)
		IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_01.movieID, "DISPLAY_VIEW")
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF

	//	SET_HIGHLIGHT(index:int) // only highlight once its drawn, call again on dpad to update the highlight
		CPRINTLN(DEBUG_SHOPS, "SET_HIGHLIGHT(iTintIndex1: ", iTintIndex1, ")")
		IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_01.movieID, "SET_HIGHLIGHT")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTintIndex1)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF
	
	/*
//	[COLOUR]
//	[]			- false, true
//	
//	[tint]
//	[COLOUR]	- false, false
//	
//	[OPACITY]
//	[COLOUR]	- true, false
	*/
	
	BOOL bOne, bTwo
//	bOne = NOT sData.bToggleChangedItem					//is opacity on?
//	bTwo = NOT IS_STRING_NULL_OR_EMPTY(sColour02)		//is highlight not on
//	IF bOne AND bTwo
//		bOne = FALSE
//	ENDIF
	IF (fOpacity = -1)
		bOne = FALSE
		IF NOT sData.bToggleChangedItem
		//	[COLOUR]
		//	[]			- false, true
			bTwo = true
		ELSE
		//	[tint]
		//	[COLOUR]	- false, false
			bTwo = FALSE
		ENDIF
	ELSE
	//	[OPACITY]
	//	[COLOUR]	- true, false
		bOne = TRUE
		bTwo = FALSE
	ENDIF
	IF (fOpacity = -1)
		CDEBUG2LN(DEBUG_SHOPS, "SHOW_OPACITY(opacity: ", bOne, ", highlight: ", bTwo, ")")
		IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_01.movieID, "SHOW_OPACITY")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bOne)
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(bTwo)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF
	
	IF IS_STRING_NULL_OR_EMPTY(sColour02)
	OR NOT sData.bToggleChangedItem
//		IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_02.movieID, "SET_TITLE")
//			END_SCALEFORM_MOVIE_METHOD()
//		ENDIF
//		IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_02.movieID, "SET_DATA_SLOT_EMPTY")
//			END_SCALEFORM_MOVIE_METHOD()
//		ENDIF
	ELSE
		
		IF IS_PC_VERSION()
			BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_02.movieID, "SET_IS_PC")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_BOOL(TRUE)		
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF

		IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_02.movieID, "SET_TITLE")
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(sOpacity)
			
			IF iTintIndex2 != -1
			AND sData.iMAX_TINT_COUNT != -1
				BEGIN_TEXT_COMMAND_SCALEFORM_STRING(sColour02)
					ADD_TEXT_COMPONENT_INTEGER(iTintIndex2+1)
					ADD_TEXT_COMPONENT_INTEGER(sData.iMAX_TINT_COUNT)
				END_TEXT_COMMAND_SCALEFORM_STRING()
			ELSE
				CPRINTLN(DEBUG_SHOPS, "BUILD_NGMP_TINT_MENU invalid tint2 colours - ", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ", iTintIndex2:", iTintIndex2, ", iMAX_TINT_COUNT:", sData.iMAX_TINT_COUNT)
			ENDIF
			
			IF fOpacity != -1
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fOpacity)
			ENDIF
			
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		
	//	SET_DATA_SLOT_EMPTY() // This clears the colours
		IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_02.movieID, "SET_DATA_SLOT_EMPTY")
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sColour02)
		//	SET_DATA_SLOT(index:int, r:int, g:int, b:int ) // set as many colours as you like
			INT iTintIndex
			INT iR, iG, iB
			REPEAT sData.iMAX_TINT_COUNT iTintIndex
				INT iTint = GET_NGMP_TINT_FROM_TINTINDEX(ePedModel, sData.eCurrentMenu, iTintIndex, sData.eInShop)
				
				// Get the RGB values for our paletter
				IF sData.eCurrentMenu = HME_NGMP_HAIR
				OR sData.eCurrentMenu = HME_NGMP_BEARD
				OR sData.eCurrentMenu = HME_NGMP_CHEST
				OR sData.eCurrentMenu = HME_NGMP_EYEBROWS
					GET_PED_HAIR_TINT_COLOR(iTint, iR, iG, iB)
				ELIF sData.eCurrentMenu = HME_NGMP_MAKEUP_BLUSHER
				OR sData.eCurrentMenu = HME_NGMP_MAKEUP_LIPSTICK
				OR sData.eCurrentMenu = HME_NGMP_FACEPAINT
					GET_PED_MAKEUP_TINT_COLOR(iTint, iR, iG, iB)
				ELSE
					CASSERTLN(DEBUG_SHOPS, "unknown menu choice for GET_PED_XXX_TINT_COLOR - ", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu))
				ENDIF
			
				CDEBUG2LN(DEBUG_SHOPS, "SET_DATA_SLOT(index:", iTintIndex, ", r:", iR, ", g:", iG, ", b:", iB, ")")
				IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_02.movieID, "SET_DATA_SLOT")
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTintIndex)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iR)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iG)
					SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iB)
					END_SCALEFORM_MOVIE_METHOD()
				ENDIF
			ENDREPEAT

		//	DISPLAY_VIEW() // draws the colours to the screen (call once)
			IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_02.movieID, "DISPLAY_VIEW")
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF

		//	SET_HIGHLIGHT(index:int) // only highlight once its drawn, call again on dpad to update the highlight
			CDEBUG2LN(DEBUG_SHOPS, "SET_HIGHLIGHT(iTintIndex2: ", iTintIndex2, ")")
			IF BEGIN_SCALEFORM_MOVIE_METHOD(sData.colourTint_movieID_02.movieID, "SET_HIGHLIGHT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iTintIndex2)
				END_SCALEFORM_MOVIE_METHOD()
			ENDIF
		ENDIF
	ENDIF
	
	 
	IF bRebuildHelpKeys
		REMOVE_MENU_HELP_KEYS()
	ENDIF
	
	IF NOT IS_NGMP_MENU(sData.eCurrentMenu)
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ELSE
		IF sData.eCurrentMenu = HME_NGMP_MAIN
		OR sData.eCurrentMenu = HME_NGMP_MAKEUP
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
		ELSE
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_BUY")
		ENDIF
	ENDIF
	IF sData.bAllowZoomControl
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
		ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
		IF NOT IS_NGMP_MENU(sData.eCurrentMenu)
			ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
		ENDIF
	ELSE
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
		ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
		//ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
	ENDIF
	
	IF NOT IS_STRING_NULL_OR_EMPTY(sColour01)
		IF NOT IS_STRING_NULL_OR_EMPTY(sOpacity)
		OR NOT IS_STRING_NULL_OR_EMPTY(sColour02)
			IF sData.bToggleChangedItem
				IF NOT IS_STRING_NULL_OR_EMPTY(sOpacity)
					ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_BUMPERS, "ITEM_B_OPACITY")
				ELIF NOT IS_STRING_NULL_OR_EMPTY(sColour02)
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "ITEM_X_TINT")
					ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_BUMPERS, "ITEM_B_HILI")
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sColour02)
					ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_TRIGGERS, "ITEM_T_HCOL")
				ELSE
					ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_TRIGGERS, "ITEM_T_COL")
				ENDIF
			ELSE
				IF NOT IS_STRING_NULL_OR_EMPTY(sOpacity)
					ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_BUMPERS, "ITEM_B_OPACITY")
				ELIF NOT IS_STRING_NULL_OR_EMPTY(sColour02)
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_X, "ITEM_X_HILI")
				ENDIF
				
				IF NOT IS_STRING_NULL_OR_EMPTY(sColour02)
					ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_TRIGGERS, "ITEM_T_HCOL")
				ELSE
					ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_TRIGGERS, "ITEM_T_COL")
				ENDIF
			ENDIF
		ENDIF
	ELSE
		IF NOT IS_STRING_NULL_OR_EMPTY(sOpacity)
			ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_BUMPERS, "ITEM_B_OPACITY")
		ELIF NOT IS_STRING_NULL_OR_EMPTY(sColour02)
			//
		ENDIF
		
		IF NOT IS_STRING_NULL_OR_EMPTY(sColour02)
			//
		ELSE
			//
		ENDIF
	ENDIF
ENDPROC

PROC BUILD_NGMP_MAIN_MENU(HAIRDO_SHOP_STRUCT &sData, PED_INDEX pedID_ShopPed)
	CDEBUG1LN(DEBUG_SHOPS, "BUILD_NGMP_MAIN_MENU(iCurrentItem: ", sData.sBrowseInfo.iCurrentItem, ")")
	
	MODEL_NAMES ePedModel = GET_ENTITY_MODEL(pedID_ShopPed)
	
//	IF NOT sData.bRebuildMenu
//		sData.sBrowseInfo.iCurrentItem = 0
//	ENDIF
	
	// Get the current selection and cost
	GET_CURRENT_HAIRDO_ITEM(sData, NOT sData.bRebuildMenu)
	
	CLEAR_MENU_DATA()
	ADD_SHOP_MENU_GRAPHIC_TO_MENU(sData.sShopInfo.eShop)
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT)
	SET_MENU_TITLE(GET_LABEL_FOR_NGMP_MENU(HME_NGMP_MAIN, TRUE))
	sData.iTopItem = 0
	sData.bSelectedItemIsLocked = FALSE
	sData.bSelectedItemIsNew = FALSE
	sData.bUnlockMessageDisplayed = FALSE
//	sData.iValidTintCount = 0
	
	INT iMenu
	HAIRDO_MENU_ENUM eMenu
	REPEAT NUMBER_OF_NGMP_MAIN_OPTIONS iMenu
		eMenu = GET_NGMP_MENU_FOR_MAIN_MENU_OPTION(iMenu)
		IF IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, eMenu, sData.eInShop)
			
			BOOL bHAS_MENU_GOT_NEW_ITEMS = FALSE, bDOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_NGMP = FALSE
			IF eMenu = HME_NGMP_MAKEUP
				IF NOT bHAS_MENU_GOT_NEW_ITEMS
					IF IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, HME_NGMP_MAKEUP_BLUSHER, sData.eInShop)
						IF HAS_MENU_GOT_NEW_ITEMS(sData, ePedModel, HME_NGMP_MAKEUP_BLUSHER)
							bHAS_MENU_GOT_NEW_ITEMS = TRUE
						ENDIF
					ENDIF
				ENDIF
				IF NOT bHAS_MENU_GOT_NEW_ITEMS
					IF IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, HME_NGMP_MAKEUP_EYE, sData.eInShop)
						IF HAS_MENU_GOT_NEW_ITEMS(sData, ePedModel, HME_NGMP_MAKEUP_EYE)
							bHAS_MENU_GOT_NEW_ITEMS = TRUE
						ENDIF
					ENDIF
				ENDIF
				IF NOT bHAS_MENU_GOT_NEW_ITEMS
					IF IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, HME_NGMP_MAKEUP_LIPSTICK, sData.eInShop)
						IF HAS_MENU_GOT_NEW_ITEMS(sData, ePedModel, HME_NGMP_MAKEUP_LIPSTICK)
							bHAS_MENU_GOT_NEW_ITEMS = TRUE
						ENDIF
					ENDIF
				ENDIF
				
				IF NOT bHAS_MENU_GOT_NEW_ITEMS
					IF NOT bDOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_NGMP
						IF IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, HME_NGMP_MAKEUP_BLUSHER, sData.eInShop)
							IF DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_NGMP(sData, ePedModel, HME_NGMP_MAKEUP_BLUSHER)
								bDOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_NGMP = TRUE
							ENDIF
						ENDIF
					ENDIF
					IF NOT bDOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_NGMP
						IF IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, HME_NGMP_MAKEUP_EYE, sData.eInShop)
							IF DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_NGMP(sData, ePedModel, HME_NGMP_MAKEUP_EYE)
								bDOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_NGMP = TRUE
							ENDIF
						ENDIF
					ENDIF
					IF NOT bDOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_NGMP
						IF IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, HME_NGMP_MAKEUP_LIPSTICK, sData.eInShop)
							IF DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_NGMP(sData, ePedModel, HME_NGMP_MAKEUP_LIPSTICK)
								bDOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_NGMP = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
			ELSE
				IF HAS_MENU_GOT_NEW_ITEMS(sData, ePedModel, eMenu)
					bHAS_MENU_GOT_NEW_ITEMS = TRUE
				ELIF DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_NGMP(sData, ePedModel, eMenu)
					bDOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_NGMP = TRUE
				ENDIF
			ENDIF
			
			IF bHAS_MENU_GOT_NEW_ITEMS
				ADD_MENU_ITEM_TEXT(iMenu, GET_LABEL_FOR_NGMP_MENU(eMenu), 1)
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
			ELIF bDOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_NGMP
				ADD_MENU_ITEM_TEXT(iMenu, GET_LABEL_FOR_NGMP_MENU(eMenu), 1)
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
			ELSE
				ADD_MENU_ITEM_TEXT(iMenu, GET_LABEL_FOR_NGMP_MENU(eMenu))
			ENDIF
		ENDIF
	ENDREPEAT
	
	SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
	
	IF NOT IS_NGMP_MENU(sData.eCurrentMenu)
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ELSE
		IF sData.eCurrentMenu = HME_NGMP_MAIN
		OR sData.eCurrentMenu = HME_NGMP_MAKEUP
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
		ELSE
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_BUY")
		ENDIF
	ENDIF
	IF sData.bAllowZoomControl
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
		ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
		IF NOT IS_NGMP_MENU(sData.eCurrentMenu)
			ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
		ENDIF
	ELSE
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
		ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
		//ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
	ENDIF
	
	CLEAR_MENU_ITEM_NGMP_COMPONENT(sData)
ENDPROC
PROC BUILD_NGMP_MAKEUP_MENU(HAIRDO_SHOP_STRUCT &sData, PED_INDEX pedID_ShopPed)
	CDEBUG1LN(DEBUG_SHOPS, "BUILD_NGMP_MAKEUP_MENU(iCurrentItem: ", sData.sBrowseInfo.iCurrentItem, ")")
	
	MODEL_NAMES ePedModel = GET_ENTITY_MODEL(pedID_ShopPed)
	
//	IF NOT sData.bRebuildMenu
//		sData.sBrowseInfo.iCurrentItem = 0
//	ENDIF
	
	// Get the current selection and cost
	GET_CURRENT_HAIRDO_ITEM(sData, NOT sData.bRebuildMenu)
	
	CLEAR_MENU_DATA()
	ADD_SHOP_MENU_GRAPHIC_TO_MENU(sData.sShopInfo.eShop)
	sData.iTopItem = 0
	sData.bSelectedItemIsLocked = FALSE
	sData.bSelectedItemIsNew = FALSE
	sData.bUnlockMessageDisplayed = FALSE
//	sData.iValidTintCount = 0
	
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT)
	SET_MENU_TITLE(GET_LABEL_FOR_NGMP_MENU(HME_NGMP_MAKEUP, TRUE))
	
	INT iMenu
	HAIRDO_MENU_ENUM eMenu
	REPEAT NUMBER_OF_NGMP_MAKEUP_OPTIONS iMenu
		eMenu = GET_NGMP_MENU_FOR_MAKEUP_MENU_OPTION(iMenu)
		IF IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, eMenu, sData.eInShop)
			IF HAS_MENU_GOT_NEW_ITEMS(sData, ePedModel, eMenu)
				ADD_MENU_ITEM_TEXT(iMenu, GET_LABEL_FOR_NGMP_MENU(eMenu), 1)
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
			ELIF DOES_PLAYER_HAVE_DISCOUNTED_OPTIONS_IN_NGMP(sData, ePedModel, eMenu)
				ADD_MENU_ITEM_TEXT(iMenu, GET_LABEL_FOR_NGMP_MENU(eMenu), 1)
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
			ELSE
				ADD_MENU_ITEM_TEXT(iMenu, GET_LABEL_FOR_NGMP_MENU(eMenu))
			ENDIF
		ENDIF
	ENDREPEAT
	
	SET_TOP_MENU_ITEM(sData.iTopItem)
	SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
	
	IF NOT IS_NGMP_MENU(sData.eCurrentMenu)
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ELSE
		IF sData.eCurrentMenu = HME_NGMP_MAIN
		OR sData.eCurrentMenu = HME_NGMP_MAKEUP
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
		ELSE
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_BUY")
		ENDIF
	ENDIF
	IF sData.bAllowZoomControl
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
		ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
		IF NOT IS_NGMP_MENU(sData.eCurrentMenu)
			ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
		ENDIF
	ELSE
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
		ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
		//ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
	ENDIF
	
	CLEAR_MENU_ITEM_NGMP_COMPONENT(sData)
ENDPROC

PROC BUILD_NGMP_GENERIC_MENU(HAIRDO_SHOP_STRUCT &sData, PED_INDEX pedID_ShopPed, BOOL bAllowSetCurrentItem = TRUE, BOOL bAllowSetCurrentTint = TRUE)
	CDEBUG1LN(DEBUG_SHOPS, "BUILD_NGMP_GENERIC_MENU(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ", ", sData.sBrowseInfo.iCurrentItem, ")")
	
	// Get the current selection and cost
	GET_CURRENT_HAIRDO_ITEM(sData, FALSE)
	
	CLEAR_MENU_DATA()
	ADD_SHOP_MENU_GRAPHIC_TO_MENU(sData.sShopInfo.eShop)
	SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
	SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
	SET_MENU_TITLE(GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu, TRUE))
	
	sData.bSelectedItemIsLocked = FALSE
	sData.bSelectedItemIsNew = FALSE
	sData.bUnlockMessageDisplayed = FALSE
	
	INT iOption
	NGMP_MENU_OPTION_DATA sOptionData
	MODEL_NAMES ePedModel = GET_ENTITY_MODEL(pedID_ShopPed)
	
	
	IF sData.bRebuildTintMenu
	AND bAllowSetCurrentTint
		IF NOT (NOT sData.sBrowseInfo.bPurchasingItem
		AND sData.sBrowseInfo.bItemSelected)
			NGMP_MENU_OPTION_DATA sTintOptionData
			IF GET_NGMP_MENU_OPTION_DATA(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sTintOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
				
				IF sOptionData.iUniqueHash != 0
				AND IS_BIT_SET_HAIRDO_NGMP_SCRIPT(sOptionData.iUniqueHash, PED_COMPONENT_USED_SLOT)
					CWARNINGLN(DEBUG_SHOPS, "bypass tint setting (not set) GET_NGMP_MENU_OPTION_DATA: NGMP OVERLAY (", GET_CHARACTER_OVERLAY_TEXT(sTintOptionData.eHeadSlot),
							", sData.iCurrentBeard: ", sData.iCurrentBeard,
							", iTintIndex1: ", sData.iTint1,
							", iTintIndex2 ", sData.iTint2,
							", fBlend ", sData.fBlend)
							
				// NG MP Ped Comp
				ELIF sTintOptionData.iType = 0
				
					sData.iTint1 = SAFE_GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
					sData.iTint2 = SAFE_GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
					sData.iSavedTint2 = sData.iTint2
					sData.fBlend = 1.0
					
					CPRINTLN(DEBUG_SHOPS, "GET_CURRENT_HAIRDO_ITEM: NGMP HAIR",
							", sData.iCurrentHairdo: ", sData.iCurrentHairdo,
							", iTint1: ", sData.iTint1,
							", iTint2 ", sData.iTint2,
							", fBlend ", sData.fBlend)
					
				// NG MP Overlay
				ELIF sTintOptionData.iType = 1
					IF sTintOptionData.eHeadSlot = HOS_MAKEUP
						sData.iTint1 = -1
						sData.eRampType = RT_NONE
					ELIF sTintOptionData.eHeadSlot = HOS_BLUSHER
						sData.iTint1 = GET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY(sTintOptionData.eHeadSlot, sData.eRampType)
						sData.eRampType = RT_MAKEUP
					ELSE
						sData.iTint1 = GET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY(sTintOptionData.eHeadSlot, sData.eRampType)
					ENDIF
					
					MP_INT_STATS mpiTintIndex2 = GET_CHARACTER_OVERLAY_SECONDARY_COLOUR_STAT(sTintOptionData.eHeadSlot)
					MP_FLOAT_STATS mpfBlend = GET_CHARACTER_OVERLAY_BLEND_STAT(sTintOptionData.eHeadSlot)
					IF (mpiTintIndex2 != INT_TO_ENUM(MP_INT_STATS, -1))
						sData.iTint2 = SAFE_GET_MP_INT_CHARACTER_STAT(mpiTintIndex2)
					ELSE
						sData.iTint2 = 0
					ENDIF
					sData.iSavedTint2 = sData.iTint2
					IF (mpfBlend != INT_TO_ENUM(MP_FLOAT_STATS, -1))
						sData.fBlend = GET_MP_FLOAT_CHARACTER_STAT(mpfBlend)
						
						IF sData.sBrowseInfo.iCurrentItem = 0
						AND sData.fBlend = 0
							CPRINTLN(DEBUG_SHOPS, "build item.GET_NGMP_MENU_OPTION_DATA: NGMP OVERLAY (", GET_CHARACTER_OVERLAY_TEXT(sTintOptionData.eHeadSlot), ") - force blend to 100%")
							sData.fBlend = sTintOptionData.fBlend
						ENDIF
					ELSE
						sData.fBlend = sTintOptionData.fBlend
					ENDIF
					
					CPRINTLN(DEBUG_SHOPS, "build item.GET_NGMP_MENU_OPTION_DATA: NGMP OVERLAY (", GET_CHARACTER_OVERLAY_TEXT(sTintOptionData.eHeadSlot),
							", iCurrentItem: ", sData.sBrowseInfo.iCurrentItem,
							", iTintIndex1: ", sData.iTint1,
							", iTintIndex2 ", sData.iTint2,
							", fBlend ", sData.fBlend)
					
				// NG MP Eye
				ELIF sTintOptionData.iType = 2
					sData.iTint1 = -1
					sData.eRampType = RT_NONE
					sData.iTint2 = -1
					sData.iSavedTint2 = sData.iTint2
					sData.fBlend = sTintOptionData.fBlend
					
					CPRINTLN(DEBUG_SHOPS, "build item.GET_NGMP_MENU_OPTION_DATA: NGMP OVERLAY (", GET_CHARACTER_OVERLAY_TEXT(sTintOptionData.eHeadSlot),
							", sData.iCurrentBeard: ", sData.iCurrentBeard,
							", iTintIndex1: ", sData.iTint1,
							", iTintIndex2 ", sData.iTint2,
							", fBlend ", sData.fBlend)
				ELSE
					CASSERTLN(DEBUG_SHOPS, "build item.GET_NGMP_MENU_OPTION_DATA(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ", ", sData.sBrowseInfo.iCurrentItem, "): ",
							sTintOptionData.tlLabel, ", iTexture: ",
							sTintOptionData.iTexture, ", eHairItem: ",
							sTintOptionData.eHairItem, ", iType: ",
							sTintOptionData.iType)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	//FLAPPER BOB
	IF ePedModel = MP_F_FREEMODE_01
	AND sData.eCurrentMenu = HME_NGMP_HAIR
		IF NOT GET_PACKED_STAT_BOOL(PACKED_MP_STATS_PURCHASED_FLAPPER_BOB)
			PED_COMP_NAME_ENUM eFemaleValHair = GET_PED_COMP_ITEM_FROM_NAME_HASH(ePedModel, HASH("DLC_MP_VAL_F_HAIR0_0"), COMP_TYPE_HAIR, 4)
			
			
			IF ENUM_TO_INT(eFemaleValHair) = GET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT)
			OR ENUM_TO_INT(GET_FEMALE_HAIR(eFemaleValHair)) = GET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT)
				IF IS_MP_VALENTINES_PACK_PRESENT()
					CERRORLN(DEBUG_SHOPS, "force valentines day pack")
					SET_PACKED_STAT_BOOL(PACKED_MP_STATS_PURCHASED_FLAPPER_BOB, TRUE)
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	BOOL bCurrentFound = FALSE
	WHILE GET_NGMP_MENU_OPTION_DATA(ePedModel, sData.eCurrentMenu, iOption, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
		
		IF IS_DLC_INSTALLED_FOR_NGMP_ITEM(ePedModel, sData.eCurrentMenu, iOption, sData.eInShop)
			IF IS_NGMP_UNLOCKED_FOR_SHOP(sData, ePedModel, sData.eCurrentMenu, iOption)
			AND NOT HAS_NGMP_BEEN_VIEWED_IN_SHOP(sData, ePedModel, sData.eCurrentMenu, iOption)
				ADD_MENU_ITEM_TEXT(iOption, sOptionData.tlLabel, 1)
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
				
				// FLAG: NEW
				IF iOption = sData.sBrowseInfo.iCurrentItem
					sData.bSelectedItemIsNew = TRUE
				ENDIF
			ELIF IS_NGMP_UNLOCKED_FOR_SHOP(sData, ePedModel, sData.eCurrentMenu, iOption)
			AND SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP(sOptionData.tlLabel, sData.eCurrentMenu, sOptionData.iCost)
			AND NOT IS_NGMP_ITEM_AND_TINTS_CURRENT(ePedModel, sOptionData, sData)
				ADD_MENU_ITEM_TEXT(iOption, sOptionData.tlLabel, 1)
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_DISCOUNT)
				
//				// FLAG: NEW
//				IF iOption = sData.sBrowseInfo.iCurrentItem
//					bShowDiscountInfo = TRUE
//				ENDIF
			ELSE
				ADD_MENU_ITEM_TEXT(iOption, sOptionData.tlLabel)
			ENDIF
			
			// Label update
			IF iOption = sData.sBrowseInfo.iCurrentItem
				sData.sCurrentItemLabel = sOptionData.tlLabel
			ENDIF
			
			IF IS_NGMP_ITEM_AND_TINTS_CURRENT(ePedModel, sOptionData, sData)	//TRUE)
				bCurrentFound = TRUE
				
				IF bAllowSetCurrentItem
					IF NOT (NOT sData.sBrowseInfo.bPurchasingItem
					AND sData.sBrowseInfo.bItemSelected)
						CPRINTLN(DEBUG_SHOPS, "BUILD_NGMP_GENERIC_MENU-IS_NGMP_ITEM_AND_TINTS_CURRENT(iOption:", iOption, ") - update iCurrentItem [", sData.sBrowseInfo.iCurrentItem, "]")
						sData.sBrowseInfo.iCurrentItem = iOption
					ELSE
						CPRINTLN(DEBUG_SHOPS, "BUILD_NGMP_GENERIC_MENU-IS_NGMP_ITEM_AND_TINTS_CURRENT(iOption:", iOption, ") - ignore iCurrentItem [bPurchasingItem: ", sData.sBrowseInfo.bPurchasingItem, ", bItemSelected: ", sData.sBrowseInfo.bItemSelected, "]")
					ENDIF
					
					sData.bSelectedItemIsDLC = IS_NGMP_ITEM_DLC(ePedModel, sData.eCurrentMenu, sOptionData)
				ENDIF
				
				ADD_MENU_ITEM_TEXT(iOption, "", 1)
				
				IF sData.eCurrentMenu = HME_NGMP_FACEPAINT
				OR sData.eCurrentMenu = HME_NGMP_MAKEUP_BLUSHER
				OR sData.eCurrentMenu = HME_NGMP_MAKEUP_EYE
				OR sData.eCurrentMenu = HME_NGMP_MAKEUP_LIPSTICK
					ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_MAKEUP)
				ELSE
					ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_SCISSORS)
				ENDIF
			ELIF NOT IS_NGMP_UNLOCKED_FOR_SHOP(sData, ePedModel, sData.eCurrentMenu, iOption)
				ADD_MENU_ITEM_TEXT(iOption, "", 1)
				ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LOCK)
				
				// FLAG: LOCKED
				IF iOption = sData.sBrowseInfo.iCurrentItem
					sData.bSelectedItemIsLocked = TRUE
				ENDIF
				
			ELSE
				IF sOptionData.iCost > 0
					ADD_MENU_ITEM_TEXT(iOption, "ITEM_COST", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_INT(sOptionData.iCost)
				ELSE
					ADD_MENU_ITEM_TEXT(iOption, "ITEM_FREE")
				ENDIF
			ENDIF
			
			// Write data to the catalogue
			#IF IS_DEBUG_BUILD
				IF (g_bAddMenuItemsToCatalogue OR g_bVerifyMenuItemPrices)
					SET_TEMP_SHOP_MENU_ITEM_DATA(sOptionData.tlLabel, sOptionData.iCost, 0, ENUM_TO_INT(sData.eCurrentMenu), 0, sOptionData.iTexture)
				ENDIF
			#ENDIF
		ENDIF
		
		iOption++
	ENDWHILE
	IF iOption = 0
		ADD_MENU_ITEM_TEXT(0, "HAIR_NONE")
	ELSE
		IF NOT bCurrentFound
			IF bAllowSetCurrentItem
				IF NOT (NOT sData.sBrowseInfo.bPurchasingItem
				AND sData.sBrowseInfo.bItemSelected)
					IF sData.eCurrentMenu = HME_NGMP_FACEPAINT
					OR sData.eCurrentMenu = HME_NGMP_MAKEUP_EYE
					OR sData.eCurrentMenu = HME_NGMP_MAKEUP_BLUSHER
						CPRINTLN(DEBUG_SHOPS, "BUILD_NGMP_GENERIC_MENU-NOT bCurrentFound(\"", GET_STRING_FROM_TEXT_FILE(GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu)), "\") not found but might be shared with another HOS_MAKEUP, update iCurrentItem [", sData.sBrowseInfo.iCurrentItem, "]")
						
						sData.fBlend = 1.0
						sData.sBrowseInfo.iCurrentItem = 0
						
						TRY_ON_NGMP_MENU_OPTION(sData, pedID_ShopPed, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem)
					ELSE
						sData.fBlend = 1.0
						sData.sBrowseInfo.iCurrentItem = 0
						
						IF GET_NGMP_MENU_OPTION_DATA(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
							// NG MP Ped Comp
							IF sOptionData.iType = 0
								PED_COMP_NAME_ENUM eCurrentHaircut
								eCurrentHaircut = INT_TO_ENUM(PED_COMP_NAME_ENUM, GET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT))
								g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(ePedModel, COMP_TYPE_HAIR, eCurrentHaircut)
								
								IF g_bValidatedPlayerPurchasedHair
							
									PED_COMP_NAME_ENUM eGRHairItem
									eGRHairItem = DUMMY_PED_COMP
									IF (ePedModel = MP_M_FREEMODE_01)
										eGRHairItem = GET_MALE_HAIR(eCurrentHaircut)
									ELIF (ePedModel = MP_F_FREEMODE_01)
										eGRHairItem = GET_FEMALE_HAIR(eCurrentHaircut)
									ENDIF
									
									IF (eGRHairItem != DUMMY_PED_COMP)
									AND (eCurrentHaircut != eGRHairItem)
										CWARNINGLN(DEBUG_SHOPS, "NGMP model ",
												GET_MODEL_NAME_FOR_DEBUG(ePedModel),
												" hair not found, current stat [", eCurrentHaircut, ",", eGRHairItem,
												": dra:", g_sTempCompData[1].iDrawable,
												", tex:", g_sTempCompData[1].iTexture,
												", \"", g_sTempCompData[1].sLabel, "\"]")
									ELSE
										CWARNINGLN(DEBUG_SHOPS, "NGMP model ",
												GET_MODEL_NAME_FOR_DEBUG(ePedModel),
												" hair not found, current stat [", eCurrentHaircut,
												": dra:", g_sTempCompData[1].iDrawable,
												", tex:", g_sTempCompData[1].iTexture,
												", \"", g_sTempCompData[1].sLabel, "\"]")
									ENDIF
								ELSE
									CWARNINGLN(DEBUG_SHOPS, "NGMP model ",
											GET_MODEL_NAME_FOR_DEBUG(ePedModel),
											" unvalidated hair not found, current stat [", eCurrentHaircut,
											": dra:", g_sTempCompData[1].iDrawable,
											", tex:", g_sTempCompData[1].iTexture,
											", \"", g_sTempCompData[1].sLabel, "\"]")
								ENDIF
								
								sData.sBrowseInfo.iCurrentItem = g_sTempCompData[1].iDrawable
							
							// NG MP Overlay
							ELIF sOptionData.iType = 1
								CASSERTLN(DEBUG_SHOPS, "NGMP model ",
										GET_MODEL_NAME_FOR_DEBUG(ePedModel),
										" overlay \"",
										GET_CHARACTER_OVERLAY_TEXT(sOptionData.eHeadSlot),
										"\" not found, current stat [",
										GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(sOptionData.eHeadSlot)),
										"]")
							
							// NG MP Eye
							ELIF sOptionData.iType = 2
								CASSERTLN(DEBUG_SHOPS, "NGMP model ",
										GET_MODEL_NAME_FOR_DEBUG(ePedModel),
										" eyes not found, current stat [",
										ROUND(GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_FEATURE_20)),
										"]")
							ENDIF
						ENDIF
					ENDIF
				ELSE
					CPRINTLN(DEBUG_SHOPS, "BUILD_NGMP_GENERIC_MENU-NOT bCurrentFound(\"", GET_STRING_FROM_TEXT_FILE(GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu)), "\") not found, ignore iCurrentItem [bPurchasingItem: ", sData.sBrowseInfo.bPurchasingItem, ", bItemSelected: ", sData.sBrowseInfo.bItemSelected, "]")
				ENDIF
			ENDIF
		ENDIF
	ENDIF
	
	SET_TOP_MENU_ITEM(sData.iTopItem)
	SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
	
	// Telemetry
	NGMP_STORE_CURRENT_SHOP_ITEM_DATA(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
	
	IF NOT IS_NGMP_MENU(sData.eCurrentMenu)
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
	ELSE
		IF sData.eCurrentMenu = HME_NGMP_MAIN
		OR sData.eCurrentMenu = HME_NGMP_MAKEUP
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
		ELSE
			ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_BUY")
		ENDIF
	ENDIF
	IF sData.bAllowZoomControl
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
		ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
		IF NOT IS_NGMP_MENU(sData.eCurrentMenu)
			ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
		ENDIF
	ELSE
		ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
		ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
		ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
		//ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
	ENDIF
	
	IF sData.bRebuildTintMenu
		BUILD_NGMP_TINT_MENU(sData, ePedModel, TRUE)
		sData.bRebuildTintMenu = FALSE
	ENDIF
ENDPROC

PROC GO_BACK_TO_PREVIOUS_NGMP_MENU(HAIRDO_SHOP_STRUCT &sData)
	CDEBUG1LN(DEBUG_SHOPS, "GO_BACK_TO_PREVIOUS_NGMP_MENU(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ")")
	
	PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET")
	
	sData.sBrowseInfo.bItemSelected = FALSE
	sData.sBrowseInfo.bItemPurchased = FALSE
	sData.sBrowseInfo.bPurchasingItem = FALSE
	sData.bRebuildMenu = TRUE
	
	SWITCH sData.eCurrentMenu
		CASE HME_NGMP_MAIN
			sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_RESTORE_PED
			sData.sBrowseInfo.iControl = 0
		BREAK
		CASE HME_NGMP_HAIR
		CASE HME_NGMP_BEARD
		CASE HME_NGMP_CHEST
		CASE HME_NGMP_EYEBROWS
		CASE HME_NGMP_CONTACTS
		CASE HME_NGMP_FACEPAINT
		CASE HME_NGMP_MAKEUP
			sData.sBrowseInfo.iCurrentItem = GET_MENU_ITEM_FOR_NGMP_MENU(sData.eCurrentMenu)
			sData.eCurrentMenu = HME_NGMP_MAIN
		BREAK
		CASE HME_NGMP_MAKEUP_EYE
		CASE HME_NGMP_MAKEUP_BLUSHER
		CASE HME_NGMP_MAKEUP_LIPSTICK
			sData.sBrowseInfo.iCurrentItem = GET_MENU_ITEM_FOR_NGMP_MENU(sData.eCurrentMenu)
			sData.eCurrentMenu = HME_NGMP_MAKEUP
		BREAK
	ENDSWITCH
	
	CDEBUG1LN(DEBUG_SHOPS, "		gone back to (", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ", ", sData.sBrowseInfo.iCurrentItem, ")")
	
ENDPROC

PROC UPDATE_NGMP_SELECTION(HAIRDO_SHOP_STRUCT &sData, PED_INDEX pedID_ShopPed)
	
	IF IS_PED_INJURED(pedID_ShopPed)
		EXIT
	ENDIF
	
	// Backup
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
	OR IS_MENU_CURSOR_CANCEL_PRESSED()
		RESET_PED_FOR_NGMP_MENU(pedID_ShopPed, sData)
		GO_BACK_TO_PREVIOUS_NGMP_MENU(sData)
		
		IF sData.eCurrentMenu = HME_NGMP_MAIN
		OR sData.eCurrentMenu = HME_NGMP_MAKEUP
		OR sData.eCurrentMenu = HME_NGMP_CHEST
			sData.bAllowZoomControl = TRUE
		ELSE
			sData.bAllowZoomControl = FALSE
//			sData.fCamZoomAlpha = 1
		ENDIF
		
		REMOVE_PED_CHEST_CLOTHES(pedID_ShopPed, sData.eCurrentMenu)
		EXIT
	ENDIF
	
	// Mouse vars
	BOOL bMouseSelect = FALSE
	FLOAT fSliderValue = -1.0
	FLOAT fMouseSliderX
	FLOAT fMouseSliderY
	eMOUSE_EVT eMouseEvent
	INT iUID
	INT iContext
				
	
	MODEL_NAMES ePedModel = GET_ENTITY_MODEL(pedID_ShopPed)
		
	UPDATE_SHOP_INPUT_DATA(sData.sInputData, 300)
	
	SWITCH sData.eCurrentMenu
		CASE HME_NGMP_MAIN
		
			sData.sBrowseInfo.bItemSelected = FALSE
			
			HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
			
			IF IS_MENU_CURSOR_ACCEPT_PRESSED()
				IF g_iMenuCursorItem = sData.sBrowseInfo.iCurrentItem
					bMouseSelect = TRUE
				ELSE
					sData.sBrowseInfo.iCurrentItem = g_iMenuCursorItem
					CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(HME_NGMP_MAIN) - menu cursor accept pressed")
					
					PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
					SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
				ENDIF
			ENDIF
			
			// Change selection to previous item
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			OR (sData.sInputData.leftStickUD < -100 AND sData.sInputData.bLeftStickUDReset)
			OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sData.sInputData.bDPADUPReset)
			OR  IS_MENU_CURSOR_SCROLL_UP_PRESSED(sData.sInputData.bDPADUPReset)
				CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(HME_NGMP_MAIN) - Change selection to previous item")
				
				PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
				sData.sInputData.bDPADUPReset = FALSE
				sData.sInputData.bLeftStickUDReset = FALSE
				sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
				ENDIF
				
				sData.sBrowseInfo.iCurrentItem--
				WHILE sData.sBrowseInfo.iCurrentItem >= 0
				AND NOT IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, GET_NGMP_MENU_FOR_MAIN_MENU_OPTION(sData.sBrowseInfo.iCurrentItem), sData.eInShop)
					sData.sBrowseInfo.iCurrentItem--
				ENDWHILE
				IF NOT IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, GET_NGMP_MENU_FOR_MAIN_MENU_OPTION(sData.sBrowseInfo.iCurrentItem), sData.eInShop)
					sData.sBrowseInfo.iCurrentItem = NUMBER_OF_NGMP_MAIN_OPTIONS-1
				ENDIF
				WHILE sData.sBrowseInfo.iCurrentItem >= 0
				AND NOT IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, GET_NGMP_MENU_FOR_MAIN_MENU_OPTION(sData.sBrowseInfo.iCurrentItem), sData.eInShop)
					sData.sBrowseInfo.iCurrentItem--
				ENDWHILE
				
				SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
			
			// Change selection to next item
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			OR (sData.sInputData.leftStickUD >  100 AND sData.sInputData.bLeftStickUDReset)
			OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sData.sInputData.bDPADDOWNReset)
			OR IS_MENU_CURSOR_SCROLL_DOWN_PRESSED(sData.sInputData.bDPADDOWNReset)
				CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(HME_NGMP_MAIN) - Change selection to next item")
				
				PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
				sData.sInputData.bDPADDOWNReset = FALSE
				sData.sInputData.bLeftStickUDReset = FALSE
				sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
				ENDIF
				
				sData.sBrowseInfo.iCurrentItem++
				WHILE sData.sBrowseInfo.iCurrentItem <= NUMBER_OF_NGMP_MAIN_OPTIONS-1
				AND NOT IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, GET_NGMP_MENU_FOR_MAIN_MENU_OPTION(sData.sBrowseInfo.iCurrentItem), sData.eInShop)
					sData.sBrowseInfo.iCurrentItem++
				ENDWHILE
				IF NOT IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, GET_NGMP_MENU_FOR_MAIN_MENU_OPTION(sData.sBrowseInfo.iCurrentItem), sData.eInShop)
					sData.sBrowseInfo.iCurrentItem = 0
				ENDIF
				WHILE sData.sBrowseInfo.iCurrentItem <= NUMBER_OF_NGMP_MAIN_OPTIONS-1
				AND NOT IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, GET_NGMP_MENU_FOR_MAIN_MENU_OPTION(sData.sBrowseInfo.iCurrentItem), sData.eInShop)
					sData.sBrowseInfo.iCurrentItem++
				ENDWHILE
				
				SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
			
			// Make selection
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR bMouseSelect = TRUE
				bMouseSelect = FALSE
				CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(HME_NGMP_MAIN) - Make selection")
				
				PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
				
				sData.eCurrentHairGroup = HAIR_GROUP_ALL
				sData.eCurrentMenu = GET_NGMP_MENU_FOR_MAIN_MENU_OPTION(sData.sBrowseInfo.iCurrentItem)
				sData.bRebuildMenu = TRUE
				sData.sBrowseInfo.iCurrentItem = 0
				
				sData.bToggleChangedItem = FALSE
				
				IF sData.eCurrentMenu = HME_NGMP_CHEST
					REMOVE_PED_CHEST_CLOTHES(pedID_ShopPed, sData.eCurrentMenu)
				ENDIF
				IF sData.eCurrentMenu = HME_NGMP_MAIN
				OR sData.eCurrentMenu = HME_NGMP_MAKEUP
				OR sData.eCurrentMenu = HME_NGMP_CHEST
					sData.bAllowZoomControl = TRUE
				ELSE
					sData.bAllowZoomControl = FALSE
//					sData.fCamZoomAlpha = 1
				ENDIF
				
				IF sData.eCurrentMenu = HME_NGMP_MAKEUP
					WHILE sData.sBrowseInfo.iCurrentItem <= NUMBER_OF_NGMP_MAKEUP_OPTIONS-1
					AND NOT IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, GET_NGMP_MENU_FOR_MAKEUP_MENU_OPTION(sData.sBrowseInfo.iCurrentItem), sData.eInShop)
						sData.sBrowseInfo.iCurrentItem++
					ENDWHILE
				ENDIF
			ENDIF
		BREAK
		CASE HME_NGMP_MAKEUP
			sData.sBrowseInfo.bItemSelected = FALSE
			
			HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
			
			IF IS_MENU_CURSOR_ACCEPT_PRESSED()
				IF g_iMenuCursorItem = sData.sBrowseInfo.iCurrentItem
					bMouseSelect = TRUE
				ELSE
					sData.sBrowseInfo.iCurrentItem = g_iMenuCursorItem
					CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(HME_NGMP_MAKEUP) - menu cursor accept pressed")
					
					PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
					SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
					sData.iTopItem = GET_TOP_MENU_ITEM()
				ENDIF
			ENDIF
			
			// Change selection to previous item
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			OR (sData.sInputData.leftStickUD < -100 AND sData.sInputData.bLeftStickUDReset)
			OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sData.sInputData.bDPADUPReset)
			OR  IS_MENU_CURSOR_SCROLL_UP_PRESSED(sData.sInputData.bDPADUPReset)
				CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(HME_NGMP_MAKEUP) - Change selection to previous item")
				
				PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
				sData.sInputData.bDPADUPReset = FALSE
				sData.sInputData.bLeftStickUDReset = FALSE
				sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
				ENDIF
				
				sData.sBrowseInfo.iCurrentItem--
				WHILE sData.sBrowseInfo.iCurrentItem >= 0
				AND NOT IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, GET_NGMP_MENU_FOR_MAKEUP_MENU_OPTION(sData.sBrowseInfo.iCurrentItem), sData.eInShop)
					sData.sBrowseInfo.iCurrentItem--
				ENDWHILE
				IF NOT IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, GET_NGMP_MENU_FOR_MAKEUP_MENU_OPTION(sData.sBrowseInfo.iCurrentItem), sData.eInShop)
					sData.sBrowseInfo.iCurrentItem = NUMBER_OF_NGMP_MAKEUP_OPTIONS-1
				ENDIF
				WHILE sData.sBrowseInfo.iCurrentItem >= 0
				AND NOT IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, GET_NGMP_MENU_FOR_MAKEUP_MENU_OPTION(sData.sBrowseInfo.iCurrentItem), sData.eInShop)
					sData.sBrowseInfo.iCurrentItem--
				ENDWHILE
				
				SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
				sData.iTopItem = GET_TOP_MENU_ITEM()
			
			// Change selection to next item
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			OR (sData.sInputData.leftStickUD >  100 AND sData.sInputData.bLeftStickUDReset)
			OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sData.sInputData.bDPADDOWNReset)
			OR IS_MENU_CURSOR_SCROLL_DOWN_PRESSED(sData.sInputData.bDPADDOWNReset)
				CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(HME_NGMP_MAKEUP) - Change selection to next item")
				
				PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
				sData.sInputData.bDPADDOWNReset = FALSE
				sData.sInputData.bLeftStickUDReset = FALSE
				sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
				ENDIF
				
				sData.sBrowseInfo.iCurrentItem++
				WHILE sData.sBrowseInfo.iCurrentItem <= NUMBER_OF_NGMP_MAKEUP_OPTIONS-1
				AND NOT IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, GET_NGMP_MENU_FOR_MAKEUP_MENU_OPTION(sData.sBrowseInfo.iCurrentItem), sData.eInShop)
					sData.sBrowseInfo.iCurrentItem++
				ENDWHILE
				IF NOT IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, GET_NGMP_MENU_FOR_MAKEUP_MENU_OPTION(sData.sBrowseInfo.iCurrentItem), sData.eInShop)
					sData.sBrowseInfo.iCurrentItem = 0
				ENDIF
				WHILE sData.sBrowseInfo.iCurrentItem <= NUMBER_OF_NGMP_MAKEUP_OPTIONS-1
				AND NOT IS_NGMP_MENU_AVAILABLE_FOR_PED_MODEL(ePedModel, GET_NGMP_MENU_FOR_MAKEUP_MENU_OPTION(sData.sBrowseInfo.iCurrentItem), sData.eInShop)
					sData.sBrowseInfo.iCurrentItem++
				ENDWHILE
				
				SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
				sData.iTopItem = GET_TOP_MENU_ITEM()
			
			// Make selection
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR bMouseSelect = TRUE
				bMouseSelect = FALSE
				CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(HME_NGMP_MAKEUP) - Make selection")
				
				PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
				sData.eCurrentMenu = GET_NGMP_MENU_FOR_MAKEUP_MENU_OPTION(sData.sBrowseInfo.iCurrentItem)
				
				IF sData.eCurrentMenu = HME_NGMP_MAIN
				OR sData.eCurrentMenu = HME_NGMP_MAKEUP
				OR sData.eCurrentMenu = HME_NGMP_CHEST
					sData.bAllowZoomControl = TRUE
				ELSE
					sData.bAllowZoomControl = FALSE
		//			sData.fCamZoomAlpha = 1
				ENDIF
				
				sData.bRebuildMenu = TRUE
				sData.sBrowseInfo.iCurrentItem = 0
			ENDIF
		BREAK
		DEFAULT

			IF sData.sBrowseInfo.iCurrentItem != -1
			AND sData.bSelectedItemIsNew
			AND NOT sData.bSelectedItemIsLocked
			
				NGMP_MENU_OPTION_DATA sOptionData
				IF GET_NGMP_MENU_OPTION_DATA(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
					CPRINTLN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION: set hash[", sOptionData.iUniqueHash, "] \"", sOptionData.tlLabel, "\" been viewed in shop")
					SET_BIT_HAIRDO_NGMP_SCRIPT(sOptionData.iUniqueHash, PED_COMPONENT_USED_SLOT)
				ENDIF
			ENDIF
			
			sData.sBrowseInfo.bItemSelected = FALSE
			
			HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
			
			IF IS_MENU_CURSOR_ACCEPT_PRESSED()
				IF g_iMenuCursorItem = sData.sBrowseInfo.iCurrentItem
					bMouseSelect = TRUE
				ELSE
					NGMP_MENU_OPTION_DATA sOptionData
					sData.sBrowseInfo.iCurrentItem = g_iMenuCursorItem
					
					PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
					
					// Refresh if last selected item was new.
					IF sData.bSelectedItemIsNew
						sData.bRebuildMenu = TRUE
						sData.bBlockAutoItemSelect = TRUE
						sData.iTopMenuItem = GET_TOP_MENU_ITEM()
					ENDIF
				
					// FLAG: LOCKED
					sData.bSelectedItemIsLocked = (NOT IS_NGMP_UNLOCKED_FOR_SHOP(sData, ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem))
					// FLAG: NEW
					sData.bSelectedItemIsNew = (NOT sData.bSelectedItemIsLocked AND NOT HAS_NGMP_BEEN_VIEWED_IN_SHOP(sData, ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem))
					// FLAG: DLC
					IF GET_NGMP_MENU_OPTION_DATA(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
						sData.bSelectedItemIsDLC = IS_NGMP_ITEM_DLC(ePedModel, sData.eCurrentMenu, sOptionData)
					ELSE
						sData.bSelectedItemIsDLC = FALSE
					ENDIF
					
					// Try before you buy
					TRY_ON_NGMP_MENU_OPTION(sData, pedID_ShopPed, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem)
					IF sData.bRebuildMenu
						sData.bRebuildTintMenu = TRUE
						BUILD_NGMP_GENERIC_MENU(sData, pedID_ShopPed, FALSE, FALSE)
						sData.bRebuildMenu = FALSE
					ELSE
						BUILD_NGMP_TINT_MENU(sData, ePedModel, TRUE)
					ENDIF
		
					SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
					sData.iTopItem = GET_TOP_MENU_ITEM()
					
					IF GET_NGMP_MENU_OPTION_DATA(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
						sData.sCurrentItemLabel = sOptionData.tlLabel
					ENDIF
					
					// Telemetry
					NGMP_STORE_CURRENT_SHOP_ITEM_DATA(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
					
					CDEBUG1LN(DEBUG_SHOPS, "		MOUSE SELECT - Changed selection item to ", sData.sBrowseInfo.iCurrentItem)
					
				ENDIF
			ENDIF
			
			// Handle mouse highlghting of the colour bars and arrow  // REMOVED AT THE REQUEST OF SAM G.
//			// Main hair menu with colour and highlights
//			IF IS_HIGHLIGHTS_UI_ACTIVE(sData)
//				
//				//DISPLAY_TEXT_WITH_NUMBER( 0.8, 0.5, "NUMBER", 1 )
//				
//				IF IS_CURSOR_IN_AREA(mpng_centreX_01 - (CUSTOM_MENU_W / 2) + 0.02,( mpng_centreY_02 - (mpng_height_01 / 2) + 0.059) + GET_CUSTOM_MENU_FINAL_Y_COORD(), CUSTOM_MENU_W - 0.017, 0.043 )
//					SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
//				ENDIF
//				
//				IF IS_CURSOR_IN_AREA(mpng_centreX_01 - (CUSTOM_MENU_W / 2) + 0.02,( mpng_centreY_01 - (mpng_height_01 / 2) + 0.162) + GET_CUSTOM_MENU_FINAL_Y_COORD(), CUSTOM_MENU_W - 0.017, 0.043 )
//					SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
//				ENDIF
//			
//			ENDIF
//			
//			// Colours active, not on the hair menu.
//			IF IS_COLOUR_UI_ACTIVE(sData, ePedModel)
//			
//				IF sData.eCurrentMenu != HME_NGMP_HAIR
//				
//					//DISPLAY_TEXT_WITH_NUMBER( 0.8, 0.6, "NUMBER", 2 )
//								
//					IF IS_CURSOR_IN_AREA(mpng_centreX_01 - (CUSTOM_MENU_W / 2) + 0.02,( mpng_centreY_01 - (mpng_height_01 / 2) + 0.128) + GET_CUSTOM_MENU_FINAL_Y_COORD(), CUSTOM_MENU_W - 0.017, 0.043 )
//						SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
//					ENDIF
//					
//				ELSE
//				
//					// On the hair menu but no highlights active.
//					
//					IF NOT IS_HIGHLIGHTS_UI_ACTIVE(sData)
//					
//						//DISPLAY_TEXT_WITH_NUMBER( 0.8, 0.7, "NUMBER", 3 )
//				
//						IF IS_CURSOR_IN_AREA(mpng_centreX_01 - (CUSTOM_MENU_W / 2) + 0.02,( mpng_centreY_01 - (mpng_height_01 / 2) + 0.059) + GET_CUSTOM_MENU_FINAL_Y_COORD(), CUSTOM_MENU_W - 0.017, 0.043 )
//							SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
//						ENDIF
//						
//					ENDIF
//				
//				ENDIF
//		
//			ENDIF
						
			// Handle mouse support for tints and opacity.
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
						
				IF IS_HIGHLIGHTS_UI_ACTIVE(sData)
								
					IF GET_MOUSE_EVENT(sData.colourTint_movieID_02.movieID, eMouseEvent, iUID, iContext)
					
						IF eMouseEvent = EVENT_TYPE_MOUSE_PRESS
							
							//DISPLAY_TEXT_WITH_NUMBER( 0.5, 0.5, "NUMBER", iUID )
							//DISPLAY_TEXT_WITH_NUMBER( 0.6, 0.5, "NUMBER", iContext )

							// Valid ID, so set the colour.
							IF iUID > -1
						
								sData.iTint2 = 	GET_NGMP_TINT_FROM_TINTINDEX(ePedModel, sData.eCurrentMenu, iUID, sData.eInShop)
								
								sData.bRebuildTintMenu = TRUE
								
								// Try before you buy
								TRY_ON_NGMP_MENU_OPTION(sData, pedID_ShopPed, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem)
								IF sData.bRebuildTintMenu
									sData.bRebuildTintMenu = TRUE
									BUILD_NGMP_GENERIC_MENU(sData, pedID_ShopPed, FALSE, FALSE)
									sData.bRebuildMenu = FALSE
								ENDIF					
									
							ELIF iUID = -1
								// < arrow click - Scroll left
								SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, INPUT_FRONTEND_LB, 1)
							
								// > arrow click - Scroll right
							ELIF iUID = -2
								SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, INPUT_FRONTEND_RB, 1)										
							ENDIF
							
						ENDIF
						
					ENDIF
							
				ENDIF
				
				// Mouse selection of items
				IF IS_COLOUR_UI_ACTIVE(sData, ePedModel)
													
					IF GET_MOUSE_EVENT(sData.colourTint_movieID_01.movieID, eMouseEvent, iUID, iContext)
				
						// Check for mouse click
						IF eMouseEvent = EVENT_TYPE_MOUSE_PRESS
							
							//DISPLAY_TEXT_WITH_NUMBER( 0.7, 0.7, "NUMBER", iUID )
							//DISPLAY_TEXT_WITH_NUMBER( 0.8, 0.7, "NUMBER", iContext )

							// Valid ID, so set the colour.
							IF iUID > -1
						
								sData.iTint1 = 	GET_NGMP_TINT_FROM_TINTINDEX(ePedModel, sData.eCurrentMenu, iUID, sData.eInShop)
								
								sData.bRebuildTintMenu = TRUE
								
								// Try before you buy
								TRY_ON_NGMP_MENU_OPTION(sData, pedID_ShopPed, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem)
								IF sData.bRebuildTintMenu
									sData.bRebuildTintMenu = TRUE
									BUILD_NGMP_GENERIC_MENU(sData, pedID_ShopPed, FALSE, FALSE)
									sData.bRebuildMenu = FALSE
								ENDIF					
									
							ELIF iUID = -1
								// < arrow click - Scroll left
								SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, INPUT_FRONTEND_LT, 1)
							
								// > arrow click - Scroll right
							ELIF iUID = -2
								SET_CONTROL_VALUE_NEXT_FRAME(FRONTEND_CONTROL, INPUT_FRONTEND_RT, 1)
							
							ENDIF
							
						ENDIF
						
					ENDIF
		
				ENDIF
				
				
				// Mouse slider support
				IF IS_OPACITY_SLIDER_ACTIVE( sData, ePedModel )
				
					fMouseSliderX = mpng_centreX_01 - CUSTOM_MENU_W / 2
					fMouseSliderY = GET_CUSTOM_MENU_FINAL_Y_COORD() + (mpng_centreY_02 - (mpng_height_01 / 2))
				
					fSliderValue = HANDLE_MOUSE_SLIDER(MOUSE_SLIDER_TYPE_HAIRDO_SHOP_OPACITY, fMouseSliderX, fMouseSliderY, CUSTOM_MENU_W, 0.15, 1.0 )
					
					//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.5, "NUMBER", fSliderValue, 2 )
					//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.6, "NUMBER", fMouseSliderPrevValue, 2 )
					//DISPLAY_TEXT_WITH_FLOAT( 0.5, 0.7, "NUMBER", sData.fBlend, 2 )
			
					IF fSliderValue >= 0.0
					AND fSliderValue != fMouseSliderPrevValue
										
						CDEBUG1LN(DEBUG_SHOPS, "		MOUSE SLIDER VALUE - ", fSliderValue)
						IF (sData.eCurrentMenu != HME_NGMP_HAIR)
							
							CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ") - Set colour highlight/opacity (MOUSE SLIDER)")
					
							//PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
						
							sData.fBlend = fSliderValue
					
							CDEBUG1LN(DEBUG_SHOPS, "		sData.fBlend mouse set to ", sData.fBlend)
							sData.bRebuildTintMenu = TRUE
							
							// Try before you buy
							TRY_ON_NGMP_MENU_OPTION(sData, pedID_ShopPed, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem)
							
							
							BUILD_NGMP_TINT_MENU(sData, ePedModel, TRUE)
			
						ENDIF
						
						PLAY_SOUND_FRONTEND(iMouseSliderSoundID, "CONTINUOUS_SLIDER", "HUD_FRONTEND_DEFAULT_SOUNDSET")
					
					ELSE
					
						// Stopped moving the slider so now we can re-build the tint menu safely.
						IF sData.bRebuildTintMenu
							sData.bRebuildTintMenu = TRUE
							BUILD_NGMP_GENERIC_MENU(sData, pedID_ShopPed, FALSE, FALSE)
							sData.bRebuildMenu = FALSE
						ENDIF
					
						STOP_SOUND(iMouseSliderSoundID)
					ENDIF
					
					fMouseSliderPrevValue = fSliderValue
					
				ENDIF
	
			ENDIF
			
			FLOAT fCONST_BLEND_INCRAMENT
			fCONST_BLEND_INCRAMENT = (fMAX_OPACITY_BLEND-fMIN_OPACITY_BLEND)/20.0
			
			PRINTLN("fCONST_BLEND_INCRAMENT - ", fCONST_BLEND_INCRAMENT)
			
			// Change selection to previous item
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			OR (sData.sInputData.leftStickUD < -100 AND sData.sInputData.bLeftStickUDReset)
			OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sData.sInputData.bDPADUPReset)
			OR  IS_MENU_CURSOR_SCROLL_UP_PRESSED(sData.sInputData.bDPADUPReset)
				CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ") - Change selection to previous item [iPrev:", sData.sBrowseInfo.iCurrentItem, "]")
				
				PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
				sData.sInputData.bDPADUPReset = FALSE
				sData.sInputData.bLeftStickUDReset = FALSE
				sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
				ENDIF
				
				sData.sBrowseInfo.iCurrentItem--
				WHILE sData.sBrowseInfo.iCurrentItem >= 0
				AND NOT IS_DLC_INSTALLED_FOR_NGMP_ITEM(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sData.eInShop)
					sData.sBrowseInfo.iCurrentItem--
					CDEBUG1LN(DEBUG_SHOPS, "		decrease iCurrentItem(one) to ", sData.sBrowseInfo.iCurrentItem)
				ENDWHILE
				IF NOT IS_DLC_INSTALLED_FOR_NGMP_ITEM(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sData.eInShop)
					sData.sBrowseInfo.iCurrentItem = GET_NUMBER_OF_NGMP_MENU_OPTIONS(ePedModel, sData.eCurrentMenu)-1
					CDEBUG1LN(DEBUG_SHOPS, "		decrease iCurrentItem(two) to ", sData.sBrowseInfo.iCurrentItem)
				ENDIF
				WHILE sData.sBrowseInfo.iCurrentItem >= 0
				AND NOT IS_DLC_INSTALLED_FOR_NGMP_ITEM(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sData.eInShop)
					sData.sBrowseInfo.iCurrentItem--
					CDEBUG1LN(DEBUG_SHOPS, "		decrease iCurrentItem(three) to ", sData.sBrowseInfo.iCurrentItem)
				ENDWHILE
				IF sData.sBrowseInfo.iCurrentItem < 0
					sData.sBrowseInfo.iCurrentItem = GET_NUMBER_OF_NGMP_MENU_OPTIONS(ePedModel, sData.eCurrentMenu)-1
					CDEBUG1LN(DEBUG_SHOPS, "		decrease iCurrentItem(four) to ", sData.sBrowseInfo.iCurrentItem)
				ENDIF
				WHILE sData.sBrowseInfo.iCurrentItem >= 0
				AND NOT IS_DLC_INSTALLED_FOR_NGMP_ITEM(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sData.eInShop)
					sData.sBrowseInfo.iCurrentItem--
					CDEBUG1LN(DEBUG_SHOPS, "		decrease iCurrentItem(five) to ", sData.sBrowseInfo.iCurrentItem)
				ENDWHILE
				/* */
				CDEBUG1LN(DEBUG_SHOPS, "		Changed selection item to ", sData.sBrowseInfo.iCurrentItem)
				
				// Refresh if last selected item was new.
				IF sData.bSelectedItemIsNew
					sData.bRebuildMenu = TRUE
					sData.bBlockAutoItemSelect = TRUE
					sData.iTopMenuItem = GET_TOP_MENU_ITEM()
				ENDIF
				
				NGMP_MENU_OPTION_DATA sOptionData
				
				// FLAG: LOCKED
				sData.bSelectedItemIsLocked = (NOT IS_NGMP_UNLOCKED_FOR_SHOP(sData, ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem))
				// FLAG: NEW
				sData.bSelectedItemIsNew = (NOT sData.bSelectedItemIsLocked AND NOT HAS_NGMP_BEEN_VIEWED_IN_SHOP(sData, ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem))
				// FLAG: DLC
				IF GET_NGMP_MENU_OPTION_DATA(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
					sData.bSelectedItemIsDLC = IS_NGMP_ITEM_DLC(ePedModel, sData.eCurrentMenu, sOptionData)
				ELSE
					sData.bSelectedItemIsDLC = FALSE
				ENDIF
				
				// Try before you buy
				TRY_ON_NGMP_MENU_OPTION(sData, pedID_ShopPed, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem)
				IF sData.bRebuildMenu
					sData.bRebuildTintMenu = TRUE
					BUILD_NGMP_GENERIC_MENU(sData, pedID_ShopPed, FALSE, FALSE)
					sData.bRebuildMenu = FALSE
				ELSE
					BUILD_NGMP_TINT_MENU(sData, ePedModel, TRUE)
				ENDIF
				SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
				sData.iTopItem = GET_TOP_MENU_ITEM()
				
				IF GET_NGMP_MENU_OPTION_DATA(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
					sData.sCurrentItemLabel = sOptionData.tlLabel
				ENDIF
				
				// Telemetry
				NGMP_STORE_CURRENT_SHOP_ITEM_DATA(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
				
			// Change selection to next item
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			OR (sData.sInputData.leftStickUD >  100 AND sData.sInputData.bLeftStickUDReset)
			OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sData.sInputData.bDPADDOWNReset)
			OR IS_MENU_CURSOR_SCROLL_DOWN_PRESSED(sData.sInputData.bDPADDOWNReset)
				CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ") - Change selection to next item [iPrev:", sData.sBrowseInfo.iCurrentItem, "]")
				
				PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
				sData.sInputData.bDPADDOWNReset = FALSE
				sData.sInputData.bLeftStickUDReset = FALSE
				sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
				ENDIF
				
				sData.sBrowseInfo.iCurrentItem++
			/*	IF sData.sBrowseInfo.iCurrentItem >= GET_NUMBER_OF_NGMP_MENU_OPTIONS(ePedModel, sData.eCurrentMenu)
					sData.sBrowseInfo.iCurrentItem = 0
				ENDIF
				*/
				WHILE sData.sBrowseInfo.iCurrentItem <= GET_NUMBER_OF_NGMP_MENU_OPTIONS(ePedModel, sData.eCurrentMenu)-1
				AND NOT IS_DLC_INSTALLED_FOR_NGMP_ITEM(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sData.eInShop)
					sData.sBrowseInfo.iCurrentItem++
					CDEBUG1LN(DEBUG_SHOPS, "		increase iCurrentItem(one) to ", sData.sBrowseInfo.iCurrentItem)
				ENDWHILE
				IF NOT IS_DLC_INSTALLED_FOR_NGMP_ITEM(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sData.eInShop)
					sData.sBrowseInfo.iCurrentItem = 0
					CDEBUG1LN(DEBUG_SHOPS, "		increase iCurrentItem(two) to ", sData.sBrowseInfo.iCurrentItem)
				ENDIF
				WHILE sData.sBrowseInfo.iCurrentItem <= GET_NUMBER_OF_NGMP_MENU_OPTIONS(ePedModel, sData.eCurrentMenu)-1
				AND NOT IS_DLC_INSTALLED_FOR_NGMP_ITEM(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sData.eInShop)
					sData.sBrowseInfo.iCurrentItem++
					CDEBUG1LN(DEBUG_SHOPS, "		increase iCurrentItem(three) to ", sData.sBrowseInfo.iCurrentItem)
				ENDWHILE
				IF sData.sBrowseInfo.iCurrentItem >= GET_NUMBER_OF_NGMP_MENU_OPTIONS(ePedModel, sData.eCurrentMenu)
					sData.sBrowseInfo.iCurrentItem = 0
					CDEBUG1LN(DEBUG_SHOPS, "		increase iCurrentItem(four) to ", sData.sBrowseInfo.iCurrentItem)
				ENDIF
				WHILE sData.sBrowseInfo.iCurrentItem <= GET_NUMBER_OF_NGMP_MENU_OPTIONS(ePedModel, sData.eCurrentMenu)-1
				AND NOT IS_DLC_INSTALLED_FOR_NGMP_ITEM(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sData.eInShop)
					sData.sBrowseInfo.iCurrentItem++
					CDEBUG1LN(DEBUG_SHOPS, "		increase iCurrentItem(five) to ", sData.sBrowseInfo.iCurrentItem)
				ENDWHILE
				/* */
				CDEBUG1LN(DEBUG_SHOPS, "		Changed selection item to ", sData.sBrowseInfo.iCurrentItem)
				
				// Refresh if last selected item was new.
				IF sData.bSelectedItemIsNew
					sData.bRebuildMenu = TRUE
					sData.bBlockAutoItemSelect = TRUE
					sData.iTopMenuItem = GET_TOP_MENU_ITEM()
				ENDIF
				
				NGMP_MENU_OPTION_DATA sOptionData
				
				// FLAG: LOCKED
				sData.bSelectedItemIsLocked = (NOT IS_NGMP_UNLOCKED_FOR_SHOP(sData, ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem))
				// FLAG: NEW
				sData.bSelectedItemIsNew = (NOT sData.bSelectedItemIsLocked AND NOT HAS_NGMP_BEEN_VIEWED_IN_SHOP(sData, ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem))
				// FLAG: DLC
				IF GET_NGMP_MENU_OPTION_DATA(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
					sData.bSelectedItemIsDLC = IS_NGMP_ITEM_DLC(ePedModel, sData.eCurrentMenu, sOptionData)
				ELSE
					sData.bSelectedItemIsDLC = FALSE
				ENDIF
				
				// Try before you buy
				TRY_ON_NGMP_MENU_OPTION(sData, pedID_ShopPed, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem)
				IF sData.bRebuildMenu
					sData.bRebuildTintMenu = TRUE
					BUILD_NGMP_GENERIC_MENU(sData, pedID_ShopPed, FALSE, FALSE)
					sData.bRebuildMenu = FALSE
				ELSE
					BUILD_NGMP_TINT_MENU(sData, ePedModel, TRUE)
				ENDIF
				SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
				sData.iTopItem = GET_TOP_MENU_ITEM()
				
				IF GET_NGMP_MENU_OPTION_DATA(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
					sData.sCurrentItemLabel = sOptionData.tlLabel
				ENDIF
				
				// Telemetry
				NGMP_STORE_CURRENT_SHOP_ITEM_DATA(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
	
			// Change colour tint (L2)
			ELIF (CAN_EDIT_NGMP_TINTS_OF_CURRENT_ITEM(sData, ePedModel)
			AND NOT (sData.eCurrentMenu = HME_NGMP_MAKEUP_EYE))
			AND (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LT)
			OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LT) AND sData.sInputData.bLTriggerReset))
				CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ") - Change colour tint (L2) from ", sData.iTint1)
				
				PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
				sData.sInputData.bLTriggerReset = FALSE
				//sData.sInputData.bXXXXUDReset = FALSE
				sData.sInputData.iBumperTriggerTimer = GET_GAME_TIMER()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					sData.sInputData.iMP_BumperTriggerTimer = GET_NETWORK_TIME()
				ENDIF
			
				sData.iTint1--
			/*	IF sData.iTint1 < 0
					sData.iTint1 = (GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu)-1)
				ENDIF
				*/
				WHILE sData.iTint1 >= 0
				AND NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, sData.iTint1, sData.eInShop)
					sData.iTint1--
					CDEBUG1LN(DEBUG_SHOPS, "		decrease iTint1(one) to ", sData.iTint1)
				ENDWHILE
				IF NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, sData.iTint1, sData.eInShop)
					sData.iTint1 = (GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu)-1)
					CDEBUG1LN(DEBUG_SHOPS, "		decrease iTint1(two) to ", sData.iTint1)
				ENDIF
				WHILE sData.iTint1 >= 0
				AND NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, sData.iTint1, sData.eInShop)
					sData.iTint1--
					CDEBUG1LN(DEBUG_SHOPS, "		decrease iTint1(three) to ", sData.iTint1)
				ENDWHILE
				IF sData.iTint1 < 0
					sData.iTint1 = (GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu)-1)
					CDEBUG1LN(DEBUG_SHOPS, "		decrease iTint1(four) to ", sData.iTint1)
				ENDIF
				WHILE sData.iTint1 >= 0
				AND NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, sData.iTint1, sData.eInShop)
					sData.iTint1--
					CDEBUG1LN(DEBUG_SHOPS, "		decrease iTint1(five) to ", sData.iTint1)
				ENDWHILE
				/* */
				CDEBUG1LN(DEBUG_SHOPS, "		Changed selection item to ", sData.iTint1)
				
				IF NOT sData.bToggleChangedItem
					sData.iTint2 = GET_DEFAULT_SECONDARY_TINT_FOR_BARBER(sData.iTint1)
				ELSE
					sData.iSavedTint2 = sData.iTint2
				ENDIF
				
				sData.bRebuildTintMenu = TRUE
				
				// Try before you buy
				TRY_ON_NGMP_MENU_OPTION(sData, pedID_ShopPed, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem)
				IF sData.bRebuildTintMenu
					sData.bRebuildTintMenu = TRUE
					BUILD_NGMP_GENERIC_MENU(sData, pedID_ShopPed, FALSE, FALSE)
					sData.bRebuildMenu = FALSE
				ENDIF
				
			// Change colour tint (R2)
			ELIF (CAN_EDIT_NGMP_TINTS_OF_CURRENT_ITEM(sData, ePedModel)
			AND NOT (sData.eCurrentMenu = HME_NGMP_MAKEUP_EYE))
			AND (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT)
			OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RT) AND sData.sInputData.bRTriggerReset))
				CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ") - Change colour tint (R2) from ", sData.iTint1)
				
				PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
				sData.sInputData.bRTriggerReset = FALSE
				//sData.sInputData.bXXXXUDReset = FALSE
				sData.sInputData.iBumperTriggerTimer = GET_GAME_TIMER()
				IF NETWORK_IS_GAME_IN_PROGRESS()
					sData.sInputData.iMP_BumperTriggerTimer = GET_NETWORK_TIME()
				ENDIF
			
				sData.iTint1++
				WHILE sData.iTint1 <= (GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu)-1)
				AND NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, sData.iTint1, sData.eInShop)
					sData.iTint1++
					CDEBUG1LN(DEBUG_SHOPS, "		increase iTint1(one) to ", sData.iTint1)
				ENDWHILE
				IF NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, sData.iTint1, sData.eInShop)
					sData.iTint1 = 0
					CDEBUG1LN(DEBUG_SHOPS, "		increase iTint1(two) to ", sData.iTint1)
				ENDIF
				WHILE sData.iTint1 <= (GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu)-1)
				AND NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, sData.iTint1, sData.eInShop)
					sData.iTint1++
					CDEBUG1LN(DEBUG_SHOPS, "		increase iTint1(three) to ", sData.iTint1)
				ENDWHILE
				IF sData.iTint1 >= GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu)
					sData.iTint1 = 0
					CDEBUG1LN(DEBUG_SHOPS, "		increase iTint1(four) to ", sData.iTint1)
				ENDIF
				WHILE sData.iTint1 <= (GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu)-1)
				AND NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, sData.iTint1, sData.eInShop)
					sData.iTint1++
					CDEBUG1LN(DEBUG_SHOPS, "		increase iTint1(five) to ", sData.iTint1)
				ENDWHILE
				/* */
				CDEBUG1LN(DEBUG_SHOPS, "		Changed selection item to ", sData.iTint1)
				
				
				IF NOT sData.bToggleChangedItem
					sData.iTint2 = GET_DEFAULT_SECONDARY_TINT_FOR_BARBER(sData.iTint1)
				ELSE
					sData.iSavedTint2 = sData.iTint2
				ENDIF
				
				sData.bRebuildTintMenu = TRUE
				
				// Try before you buy
				TRY_ON_NGMP_MENU_OPTION(sData, pedID_ShopPed, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem)
				IF sData.bRebuildTintMenu
					sData.bRebuildTintMenu = TRUE
					BUILD_NGMP_GENERIC_MENU(sData, pedID_ShopPed, FALSE, FALSE)
					sData.bRebuildMenu = FALSE
				ENDIF
				
			// Decrease colour highlight/opacity (L1)
			ELIF CAN_EDIT_NGMP_TINTS_OF_CURRENT_ITEM(sData, ePedModel)
			AND (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
			OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB) AND sData.sInputData.bLBumperReset))
				
				IF (sData.eCurrentMenu = HME_NGMP_HAIR)
					IF sData.bToggleChangedItem
						CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ") - Decrease colour highlight/opacity (L1)")
				
						PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
						sData.sInputData.bLBumperReset = FALSE
						//sData.sInputData.bXXXXUDReset = FALSE
						sData.sInputData.iBumperTriggerTimer = GET_GAME_TIMER()
						IF NETWORK_IS_GAME_IN_PROGRESS()
							sData.sInputData.iMP_BumperTriggerTimer = GET_NETWORK_TIME()
						ENDIF
						
						sData.iTint2--
						WHILE sData.iTint2 >= 0
						AND NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, sData.iTint2, sData.eInShop)
							sData.iTint2--
							CDEBUG1LN(DEBUG_SHOPS, "		decrease iTint2(one) to ", sData.iTint2)
						ENDWHILE
						IF NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, sData.iTint2, sData.eInShop)
							sData.iTint2 = (GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu)-1)
							CDEBUG1LN(DEBUG_SHOPS, "		decrease iTint2(two) to ", sData.iTint2)
						ENDIF
						WHILE sData.iTint2 >= 0
						AND NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, sData.iTint2, sData.eInShop)
							sData.iTint2--
							CDEBUG1LN(DEBUG_SHOPS, "		decrease iTint2(three) to ", sData.iTint2)
						ENDWHILE
						IF sData.iTint2 < 0
							sData.iTint2 = (GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu)-1)
							CDEBUG1LN(DEBUG_SHOPS, "		decrease iTint2(four) to ", sData.iTint2)
						ENDIF
						WHILE sData.iTint2 >= 0
						AND NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, sData.iTint2, sData.eInShop)
							sData.iTint2--
							CDEBUG1LN(DEBUG_SHOPS, "		decrease iTint2(five) to ", sData.iTint2)
						ENDWHILE
						/* */
						CDEBUG1LN(DEBUG_SHOPS, "		Changed selection item to ", sData.iTint2)
						
						CDEBUG1LN(DEBUG_SHOPS, "		sData.iTint2 decreased to ", sData.iTint2)
						sData.bRebuildTintMenu = TRUE
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ") - Decrease colour highlight/opacity (L1)")
					
					PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
					sData.sInputData.bLBumperReset = FALSE
					//sData.sInputData.bXXXXUDReset = FALSE
					sData.sInputData.iBumperTriggerTimer = GET_GAME_TIMER()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						sData.sInputData.iMP_BumperTriggerTimer = GET_NETWORK_TIME()
					ENDIF
				
					sData.fBlend -= fCONST_BLEND_INCRAMENT
					IF sData.fBlend < fMIN_OPACITY_BLEND
						sData.fBlend = fMIN_OPACITY_BLEND
					ENDIF
					CDEBUG1LN(DEBUG_SHOPS, "		sData.fBlend decreased to ", sData.fBlend)
					sData.bRebuildTintMenu = TRUE
				ENDIF
				
				// Try before you buy
				TRY_ON_NGMP_MENU_OPTION(sData, pedID_ShopPed, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem)
				IF sData.bRebuildTintMenu
					sData.bRebuildTintMenu = TRUE
					BUILD_NGMP_GENERIC_MENU(sData, pedID_ShopPed, FALSE, FALSE)
					sData.bRebuildMenu = FALSE
				ENDIF
				
			// Increase colour highlight/opacity (R1)
			ELIF CAN_EDIT_NGMP_TINTS_OF_CURRENT_ITEM(sData, ePedModel)
			AND (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
			OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB) AND sData.sInputData.bRBumperReset))					


				IF (sData.eCurrentMenu = HME_NGMP_HAIR)
					IF sData.bToggleChangedItem
						CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ") - Increase colour highlight/opacity (R1)")
						
						PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
						sData.sInputData.bRBumperReset = FALSE
						//sData.sInputData.bXXXXUDReset = FALSE
						sData.sInputData.iBumperTriggerTimer = GET_GAME_TIMER()
						IF NETWORK_IS_GAME_IN_PROGRESS()
							sData.sInputData.iMP_BumperTriggerTimer = GET_NETWORK_TIME()
						ENDIF
						
						sData.iTint2++
						WHILE sData.iTint2 <= (GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu)-1)
						AND NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, sData.iTint2, sData.eInShop)
							sData.iTint2++
							CDEBUG1LN(DEBUG_SHOPS, "		increase iTint2(one) to ", sData.iTint2)
						ENDWHILE
						IF NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, sData.iTint2, sData.eInShop)
							sData.iTint2 = 0
							CDEBUG1LN(DEBUG_SHOPS, "		increase iTint2(two) to ", sData.iTint2)
						ENDIF
						WHILE sData.iTint2 <= (GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu)-1)
						AND NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, sData.iTint2, sData.eInShop)
							sData.iTint2++
							CDEBUG1LN(DEBUG_SHOPS, "		increase iTint2(three) to ", sData.iTint2)
						ENDWHILE
						IF sData.iTint2 >= GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu)
							sData.iTint2 = 0
							CDEBUG1LN(DEBUG_SHOPS, "		increase iTint2(four) to ", sData.iTint2)
						ENDIF
						WHILE sData.iTint2 <= (GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu)-1)
						AND NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, sData.iTint2, sData.eInShop)
							sData.iTint2++
							CDEBUG1LN(DEBUG_SHOPS, "		increase iTint2(five) to ", sData.iTint2)
						ENDWHILE
						/* */
						CDEBUG1LN(DEBUG_SHOPS, "		Changed selection item to ", sData.iTint2)
						CDEBUG1LN(DEBUG_SHOPS, "		sData.iTint2 increased to ", sData.iTint2)
						sData.bRebuildTintMenu = TRUE
					ENDIF
				ELSE
					CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ") - Increase colour highlight/opacity (R1)")
					
					PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
					sData.sInputData.bRBumperReset = FALSE
					//sData.sInputData.bXXXXUDReset = FALSE
					sData.sInputData.iBumperTriggerTimer = GET_GAME_TIMER()
					IF NETWORK_IS_GAME_IN_PROGRESS()
						sData.sInputData.iMP_BumperTriggerTimer = GET_NETWORK_TIME()
					ENDIF
					
					sData.fBlend += fCONST_BLEND_INCRAMENT
					IF sData.fBlend > fMAX_OPACITY_BLEND
						sData.fBlend = fMAX_OPACITY_BLEND
					ENDIF
					CDEBUG1LN(DEBUG_SHOPS, "		sData.fBlend increased to ", sData.fBlend)
					sData.bRebuildTintMenu = TRUE
				ENDIF
				
				// Try before you buy
				TRY_ON_NGMP_MENU_OPTION(sData, pedID_ShopPed, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem)
				IF sData.bRebuildTintMenu
					sData.bRebuildTintMenu = TRUE
					BUILD_NGMP_GENERIC_MENU(sData, pedID_ShopPed, FALSE, FALSE)
					sData.bRebuildMenu = FALSE
				ENDIF
				
			// toggle changed item (opacity/colour)
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
//			OR (sData.sInputData.leftStickUD >  100 AND sData.sInputData.bLeftStickUDReset)
//			OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, eINPUT_FRONTEND_RB) AND sData.sInputData.bDPADxxxxReset)
//			OR IS_MENU_CURSOR_SCROLL_xxxx_PRESSED(sData.sInputData.bDPADxxxxReset)
				CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ") - toggle changed item (opacity/colour)")
				
				PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
				
				sData.bToggleChangedItem = NOT sData.bToggleChangedItem
				IF (sData.eCurrentMenu = HME_NGMP_HAIR)
					IF NOT sData.bToggleChangedItem
						sData.iSavedTint2 = sData.iTint2
						sData.iTint2 = GET_DEFAULT_SECONDARY_TINT_FOR_BARBER(sData.iTint1)
					ELSE
						sData.iTint2 = sData.iSavedTint2
					ENDIF
				ENDIF
				
				sData.bRebuildTintMenu = TRUE
				
				// Try before you buy
				TRY_ON_NGMP_MENU_OPTION(sData, pedID_ShopPed, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem)
				IF sData.bRebuildTintMenu
					BUILD_NGMP_TINT_MENU(sData, ePedModel, TRUE)
					sData.bRebuildTintMenu = FALSE
				ENDIF
			// Make selection
			ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR bMouseSelect = TRUE
				bMouseSelect = FALSE
			
				CDEBUG1LN(DEBUG_SHOPS, "UPDATE_NGMP_SELECTION(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ") - Make selection \"", sData.sCurrentItemLabel, "\"")
				
				PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
				
				NGMP_MENU_OPTION_DATA sOptionData
				GET_NGMP_MENU_OPTION_DATA(ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
				AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
					IF DOES_SAVE_HAVE_COUPON(COUPON_HAIRCUT)
						CPRINTLN(DEBUG_SHOPS, "[COUPON_HAIRCUT] override haircut price from $", sOptionData.iCost, " to FREE.")
						sOptionData.iCost = 0
					ELSE
						CPRINTLN(DEBUG_SHOPS, "[COUPON_HAIRCUT] player doesn't have coupon.")
					ENDIF
				ENDIF
				
				BOOL bForceFacepaintItem
				HAIRDO_MENU_ENUM eForceFacepaintMenu
				HEAD_OVERLAY_SLOT eForceFacepaintHeadshot
				TEXT_LABEL_15 sForceFacepaintLabel
				
				bForceFacepaintItem = FALSE
				eForceFacepaintMenu = INT_TO_ENUM(HAIRDO_MENU_ENUM, -1)
				eForceFacepaintHeadshot = INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1)
				sForceFacepaintLabel = ""
				IF (sData.eCurrentMenu = HME_NGMP_FACEPAINT)
					IF (sOptionData.eHeadSlot = HOS_MAKEUP)
						bForceFacepaintItem = TRUE
						eForceFacepaintMenu = HME_NGMP_MAKEUP_BLUSHER
						eForceFacepaintHeadshot = HOS_BLUSHER
						sForceFacepaintLabel = "NONE"
					ELIF (sOptionData.eHeadSlot = HOS_BLUSHER)
						bForceFacepaintItem = TRUE
						eForceFacepaintMenu = HME_NGMP_FACEPAINT
						eForceFacepaintHeadshot = HOS_MAKEUP
						sForceFacepaintLabel = "NONE"
					ENDIF
				ENDIF
				
				// Already have
				IF IS_NGMP_ITEM_AND_TINTS_CURRENT(ePedModel, sOptionData, sData)
					SWITCH sData.eCurrentMenu
						CASE HME_NGMP_BEARD
							SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_CUR_1", 4000)	//Sorry - you already have this beard.
						BREAK
						CASE HME_NGMP_HAIR
							SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_CUR_2", 4000)	//Sorry - you already have this hairstyle.
						BREAK
						CASE HME_NGMP_FACEPAINT
						CASE HME_NGMP_MAKEUP_BLUSHER
						CASE HME_NGMP_MAKEUP_EYE
						CASE HME_NGMP_MAKEUP_LIPSTICK
							SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_CUR_3", 4000)	//Sorry - you already have this makeup.
						BREAK
						DEFAULT
							SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_CUR_9", 4000)	//Sorry - you already have this item.
						BREAK
					ENDSWITCH
					
					sData.iOriginalBeard = GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)
				// Locked
				ELIF NOT IS_NGMP_UNLOCKED_FOR_SHOP(sData, ePedModel, sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem)
					SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_LOCK", 4000)
				
				// Can't afford
				ELIF NOT CAN_PLAYER_AFFORD_ITEM_COST(sOptionData.iCost)
					CPRINTLN(DEBUG_SHOPS, "Can't afford item.")
					SWITCH sData.eCurrentMenu
						CASE HME_NGMP_BEARD
							SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_AFF_1", 4000)	//Sorry - you cannot afford this beard.
						BREAK
						CASE HME_NGMP_HAIR
							SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_AFF_2", 4000)	//Sorry - you cannot afford this hairstyle.
						BREAK
						CASE HME_NGMP_FACEPAINT
						CASE HME_NGMP_MAKEUP_BLUSHER
						CASE HME_NGMP_MAKEUP_EYE
						CASE HME_NGMP_MAKEUP_LIPSTICK
							SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_AFF_3", 4000)	//Sorry - you cannot afford this makeup.
						BREAK
						DEFAULT
							SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_AFF_9", 4000)	//Sorry - you cannot afford this item.
						BREAK
					ENDSWITCH
					STORE_LAST_VIEWED_SHOP_ITEM_FOR_COMMERCE_STORE(GET_HASH_KEY(g_sShopSettings.tlCurrentItem_label), g_sShopSettings.iCurrentItem_cost, GET_HASH_KEY(GET_SHOP_NAME(sData.sShopInfo.eShop)))
					LAUNCH_STORE_CASH_ALERT(FALSE, STAT_SAVETYPE_END_SHOPPING, SPL_STORE)
					
				// Failed to add to basket
				ELIF NETWORK_IS_GAME_IN_PROGRESS()
				AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
				AND NOT NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, GET_CATALOGUE_INV_CATEGORY_FOR_HAIRDO_MENU(sData.eCurrentMenu, sOptionData.eHeadSlot), GET_HAIRDO_KEY_FOR_CATALOGUE(sData.eCurrentMenu, sData.sCurrentItemLabel, DEFAULT, GET_HAIRDO_KEY_VARIANT(sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sData.eInShop)), NET_SHOP_ACTION_BUY_ITEM, 1, sOptionData.iCost, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, GET_CATALOGUE_INVENTORY_FOR_HAIRDO_MENU(sData.eCurrentMenu, sOptionData.eHeadSlot))
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					CPRINTLN(DEBUG_SHOPS, "Can't add NGMP item to basket. Menu = ", sData.eCurrentMenu, ", Label = ", sData.sCurrentItemLabel)
					SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_FAIL", 4000)
			
				// Failed to add to basket
				ELIF NETWORK_IS_GAME_IN_PROGRESS()
				AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
				AND bForceFacepaintItem
				AND NOT NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET,
						GET_CATALOGUE_INV_CATEGORY_FOR_HAIRDO_MENU(eForceFacepaintMenu, eForceFacepaintHeadshot),
						GET_HAIRDO_KEY_FOR_CATALOGUE(eForceFacepaintMenu, sForceFacepaintLabel, DEFAULT, 1),
						NET_SHOP_ACTION_BUY_ITEM, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET,
						GET_CATALOGUE_INVENTORY_FOR_HAIRDO_MENU(eForceFacepaintMenu, eForceFacepaintHeadshot))
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					CPRINTLN(DEBUG_SHOPS, "Can't add forced free NGMP facepaint item to basket. Menu = ", eForceFacepaintMenu, ", Label = ", sForceFacepaintLabel)
					SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_FAIL", 4000)
			
				ELIF NETWORK_IS_GAME_IN_PROGRESS()
				AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
				AND DOES_SAVE_HAVE_COUPON(COUPON_HAIRCUT)
				AND NOT NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, GET_CATALOGUE_INV_CATEGORY_FOR_HAIRDO_MENU(sData.eCurrentMenu, sOptionData.eHeadSlot), GET_CATALOGUE_INVENTORY_FOR_HAIRDO_MENU(sData.eCurrentMenu, sOptionData.eHeadSlot), NET_SHOP_ACTION_SPEND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, HASH("PO_COUPON_HAIRCUT"))
					DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					CPRINTLN(DEBUG_SHOPS, "Can't add coupon discount to basket. Menu = ", sData.eCurrentMenu, ", Label = ", sData.sCurrentItemLabel)
					SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_FAIL", 4000)
					
				// Start cut process
				ELSE
					IF NETWORK_IS_GAME_IN_PROGRESS()
					AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
						sData.sBrowseInfo.bProcessingBasket = TRUE
						sData.sBrowseInfo.iProcessingBasketStage = 0
					ENDIF
				
					// Reset to actual beard and buy
//					SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_FACIAL_HAIR, sData.iCurrentBeard-1, sData.iTint1, 0, RT_NONE, sData.fCurrentBeardFade)
					sData.sBrowseInfo.bItemSelected = TRUE
//					sData.iOriginalBeard = GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem) //sData.iCurrentBeard


					/* *old* * */
					// Reset to actual beard and buy
					RESET_PED_FOR_NGMP_MENU(pedID_ShopPed, sData)
					/* * * * * */
				ENDIF


			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



/// PURPOSE:
///    Builds the menu for the current selection (main menu, hair, beards or makeup)
/// PARAMS:
///    sData - main hairdo shop struct
///    pedID_ShopPed - the shop ped (player or clone)
PROC BUILD_HAIRDO_SHOP_MENU(HAIRDO_SHOP_STRUCT &sData, PED_INDEX pedID_ShopPed)

	INT iTempItem
	#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
	INT iCost
	#ENDIF
	MODEL_NAMES ePedModel = GET_ENTITY_MODEL(pedID_ShopPed)
	
	IF NOT sData.bMenuInitialised OR sData.bRebuildMenu
		SWITCH sData.eCurrentMenu 
		
			//----------- Main menu---------------
			CASE HME_MAIN_MENU 
				IF NOT sData.bRebuildMenu
					sData.sBrowseInfo.iCurrentItem = 0
				ENDIF
								
				CLEAR_MENU_DATA()
				ADD_SHOP_MENU_GRAPHIC_TO_MENU(sData.sShopInfo.eShop)
				SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT)
				SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT)
				SET_MENU_TITLE("HAIR_TITLE_0")
				
				sData.bSelectedItemIsLocked = FALSE
				sData.bSelectedItemIsNew = FALSE
				sData.bUnlockMessageDisplayed = FALSE
				
				// Always add hair as the first item - makes the menu ordering more consistent.
				IF HAS_MENU_GOT_NEW_ITEMS(sData, ePedModel, HME_HAIR)
					ADD_MENU_ITEM_TEXT(0, "HAIR_OPTION_1", 1)
					ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
				ELSE
					ADD_MENU_ITEM_TEXT(0, "HAIR_OPTION_1")
				ENDIF
				
				// Add beard option as the second menu item if present, if not and the player has makeup available, then add that as the second item instead.
				IF DOES_PLAYER_HAVE_BEARD_OPTIONS()
					IF HAS_MENU_GOT_NEW_ITEMS(sData, ePedModel, HME_BEARD)
						ADD_MENU_ITEM_TEXT(1, "HAIR_OPTION_0", 1)
						ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)	
					ELSE
						ADD_MENU_ITEM_TEXT(1, "HAIR_OPTION_0")
					ENDIF
					
					IF DOES_PLAYER_HAVE_MAKEUP_OPTIONS()
						IF HAS_MENU_GOT_NEW_ITEMS(sData, ePedModel, HME_MAKEUP)
							ADD_MENU_ITEM_TEXT(2, "HAIR_OPTION_2", 1)
							ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)	
						ELSE
							ADD_MENU_ITEM_TEXT(2, "HAIR_OPTION_2")
						ENDIF
					ENDIF
				// If the player doesn't have beard options (i.e. female) then we add the makeup as the second item.
				ELIF DOES_PLAYER_HAVE_MAKEUP_OPTIONS()
					IF HAS_MENU_GOT_NEW_ITEMS(sData, ePedModel, HME_MAKEUP)
						ADD_MENU_ITEM_TEXT(1, "HAIR_OPTION_2", 1)
						ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)	
					ELSE
						ADD_MENU_ITEM_TEXT(1, "HAIR_OPTION_2")
					ENDIF
				ENDIF
				
				SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
				
				
				IF NOT IS_NGMP_MENU(sData.eCurrentMenu)
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
				ELSE
					IF sData.eCurrentMenu = HME_NGMP_MAIN
					OR sData.eCurrentMenu = HME_NGMP_MAKEUP
						ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_SELECT")
					ELSE
						ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_BUY")
					ENDIF
				ENDIF
				IF sData.bAllowZoomControl
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
					ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
					//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD, "ITEM_SCROLL")
					ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
					IF NOT IS_NGMP_MENU(sData.eCurrentMenu)
						ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
					ENDIF
					//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_RT, "ITEM_FSCROLL")
				ELSE
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
					ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
					//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD, "ITEM_SCROLL")
					ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
					//ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
					//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_RT, "ITEM_FSCROLL")
				ENDIF
			BREAK
			
			// --------------Hairstyles----------------------
			CASE HME_HAIR_GROUP
				BUILD_HAIR_GROUP_MENU(sData)
			BREAK
			
			CASE HME_HAIR
				BUILD_HAIR_MENU(sData, pedID_ShopPed, FALSE)
			BREAK
			
			// -----------------------Beards--------------------------------
			CASE HME_BEARD
				IF NOT NETWORK_IS_GAME_IN_PROGRESS()
					BUILD_HAIR_MENU(sData, pedID_ShopPed, FALSE)
				ELSE
					// Get the current selection and cost
					GET_CURRENT_BEARD_ITEM(sData, !sData.bBlockAutoItemSelect)
					GET_CURRENT_BEARD_COST(sData)
					
					CLEAR_MENU_DATA()
					ADD_SHOP_MENU_GRAPHIC_TO_MENU(sData.sShopInfo.eShop)
					SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
					SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
					SET_MENU_TITLE("HAIR_TITLE_1")
					
					sData.bSelectedItemIsLocked = FALSE
					sData.bSelectedItemIsNew = FALSE
					sData.bSelectedItemIsDLC = FALSE
					sData.bUnlockMessageDisplayed = FALSE
					
					INT iBeardIndex
					INT iMenuIndex
					INT iBeardCount
					TEXT_LABEL_15 tlBeardLabel
					
					#IF IS_DEBUG_BUILD
						ScriptCatalogItem sCatalogueData
					#ENDIF
					
					iBeardCount = GET_THIS_PED_HEAD_OVERLAY_NUM()
					
					REPEAT iBeardCount iMenuIndex
						iBeardIndex = GET_BEARD_INDEX_FROM_MENU_INDEX(iMenuIndex)
						
						tlBeardLabel = "HAIR_BEARD"
						tlBeardLabel += iBeardIndex
						
						// DLC beard override.
						IF SET_DLC_BEARD_LABEL(tlBeardLabel, iBeardIndex)
							IF sData.sBrowseInfo.iCurrentItem = iMenuIndex
								sData.bSelectedItemIsDLC = TRUE
							ENDIF
						ENDIF
						
						IF sData.sBrowseInfo.iCurrentItem = iMenuIndex
							sData.sCurrentItemLabel = tlBeardLabel
						ENDIF
						
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						///    	 
						///    	 CATALOGUE SETUP
						///    	 
						#IF IS_DEBUG_BUILD
							IF (g_bAddMenuItemsToCatalogue OR g_bVerifyMenuItemPrices)
							AND NETWORK_IS_GAME_IN_PROGRESS()
								GENERATE_KEY_FOR_SHOP_CATALOGUE(sCatalogueData.m_key, tlBeardLabel, ePedModel, SHOP_TYPE_HAIRDO, 1, 0)
								sCatalogueData.m_textTag = tlBeardLabel
								sCatalogueData.m_name = GET_STRING_FROM_TEXT_FILE(sCatalogueData.m_textTag)
								sCatalogueData.m_category = CATEGORY_BEARD
								sCatalogueData.m_price = GET_BEARD_ITEM_COST(sData, iBeardIndex, tlBeardLabel, TRUE)
								sCatalogueData.m_storagetype = NET_SHOP_ISTORAGE_INT
								sCatalogueData.m_bitsize = 8
								sCatalogueData.m_stathash = ENUM_TO_INT(GET_INT_PACKED_STAT_KEY(PACKED_MP_HEADBLEND_OVERLAY_BEARD, g_iPedComponentSlot))
								sCatalogueData.m_bitshift = GET_INT_PACKED_STAT_BITSHIFT(PACKED_MP_HEADBLEND_OVERLAY_BEARD)
								
								IF sCatalogueData.m_stathash != 0
									ADD_ITEM_TO_CATALOG_WITH_CHECK(sCatalogueData)
								ENDIF
							ENDIF
						#ENDIF
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
						
						IF NOT IS_THIS_BEARD_DLC(iBeardIndex)
						OR IS_DLC_INSTALLED_FOR_BEARD(iBeardIndex)
							IF IS_BEARD_UNLOCKED_FOR_SHOP(iBeardIndex)
							AND NOT HAS_BEARD_BEEN_VIEWED_IN_SHOP(iBeardIndex)
								ADD_MENU_ITEM_TEXT(iMenuIndex, tlBeardLabel, 1)
								ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
								
								// FLAG: NEW
								IF iMenuIndex = sData.sBrowseInfo.iCurrentItem
									sData.bSelectedItemIsNew = TRUE
								ENDIF
							ELSE
								ADD_MENU_ITEM_TEXT(iMenuIndex, tlBeardLabel)
							ENDIF
							
							IF iBeardIndex = sData.iCurrentBeard
								ADD_MENU_ITEM_TEXT(iMenuIndex, "", 1)
								ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_SCISSORS)
							ELIF NOT IS_BEARD_UNLOCKED_FOR_SHOP(iBeardIndex)
								ADD_MENU_ITEM_TEXT(iMenuIndex, "", 1)
								ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LOCK)
								
								// FLAG: LOCKED
								IF iMenuIndex = sData.sBrowseInfo.iCurrentItem
									sData.bSelectedItemIsLocked = TRUE
								ENDIF
								
							ELSE
								ADD_MENU_ITEM_TEXT(iMenuIndex, "ITEM_COST", 1)
								ADD_MENU_ITEM_TEXT_COMPONENT_INT(GET_BEARD_ITEM_COST(sData, iBeardIndex, tlBeardLabel, TRUE))
							ENDIF
						ENDIF
					ENDREPEAT
					
					IF sData.sBrowseInfo.iCurrentItem > (iBeardCount)
					OR sData.sBrowseInfo.iCurrentItem < 0
						sData.sBrowseInfo.iCurrentItem = 0
					ENDIF
					// Try this on.
					SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_FACIAL_HAIR, GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)-1, sData.iTint1, 0, RT_NONE, 1.0)
					
					IF sData.bAllowZoomControl
						ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_BUY")
						ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
						ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
						//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD, "ITEM_SCROLL")
						ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
						IF NOT IS_NGMP_MENU(sData.eCurrentMenu)
							ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
						ENDIF
						//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_RT, "ITEM_FSCROLL")
					ELSE
						ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_BUY")
						ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
						ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
						//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD, "ITEM_SCROLL")
						ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
						//ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
						//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_RT, "ITEM_FSCROLL")
					ENDIF
					
					IF sData.bBlockAutoItemSelect
						sData.bBlockAutoItemSelect = FALSE
						SET_TOP_MENU_ITEM(sData.iTopMenuItem)
					ELSE
						// sData.sBrowseInfo.iCurrentItem already updated via GET_CURRENT_BEARD_ITEM
						//sData.sBrowseInfo.iCurrentItem = iCurrentItem
					ENDIF
					SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
				ENDIF
			BREAK
			
			// --------------------Makeup----------------------------------
			CASE HME_MAKEUP
		
				CLEAR_MENU_DATA()
				ADD_SHOP_MENU_GRAPHIC_TO_MENU(sData.sShopInfo.eShop)
				SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
				SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
				SET_MENU_TITLE("HAIR_TITLE_3")
				
				#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
				// Get the current selection and cost
				FILL_MAKEUP_DATA(sData.sMakeup)
				#ENDIF
				
				// Cap the makeup value so it's in bounds.
				GET_CURRENT_MAKEUP_ITEM(sData, !sData.bBlockAutoItemSelect)
				IF sData.iPlayerMakeup < 0 OR sData.iPlayerMakeup >= sData.iNumMakeupItems
					SCRIPT_ASSERT("MAKEUP HAS GONE OVER BOUNDS")
					sData.iPlayerMakeup = 0
				ENDIF
				
				sData.bSelectedItemIsDLC = FALSE
				
				iTempItem = 0
				
				#IF IS_DEBUG_BUILD
				#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
					ScriptCatalogItem sCatalogueData
				#ENDIF
				#ENDIF
				
				WHILE iTempItem < sData.iNumMakeupItems
				
				#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
				AND iTempItem < COUNT_OF(sData.sMakeup)
				#ENDIF
				
					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					///    	 
					///    	 CATALOGUE SETUP
					///    	 
					#IF IS_DEBUG_BUILD
					#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
						IF (g_bAddMenuItemsToCatalogue OR g_bVerifyMenuItemPrices)
						AND NETWORK_IS_GAME_IN_PROGRESS()
							GENERATE_KEY_FOR_SHOP_CATALOGUE(sCatalogueData.m_key, sData.sMakeup[iTempItem].sLabel, ePedModel, SHOP_TYPE_HAIRDO, 2, 0)
							sCatalogueData.m_textTag = sData.sMakeup[iTempItem].sLabel
							sCatalogueData.m_name = GET_STRING_FROM_TEXT_FILE(sCatalogueData.m_textTag)
							sCatalogueData.m_category = CATEGORY_MKUP
							sCatalogueData.m_price = GET_MAKEUP_ITEM_COST(sData, sData.sMakeup[iTempItem])
							sCatalogueData.m_storagetype = NET_SHOP_ISTORAGE_INT
							sCatalogueData.m_bitsize = 8
							sCatalogueData.m_stathash = ENUM_TO_INT(GET_INT_PACKED_STAT_KEY(PACKED_MP_HEADBLEND_OVERLAY_MAKEUP, g_iPedComponentSlot))
							sCatalogueData.m_bitshift = GET_INT_PACKED_STAT_BITSHIFT(PACKED_MP_HEADBLEND_OVERLAY_MAKEUP)
							
							IF sCatalogueData.m_stathash != 0
								ADD_ITEM_TO_CATALOG_WITH_CHECK(sCatalogueData)
							ENDIF
						ENDIF
					#ENDIF
					#ENDIF
					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
					/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
				
					#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
					iCost = GET_MAKEUP_ITEM_COST(sData, sData.sMakeup[iTempItem], TRUE)
					IF iCost != -1
						IF CHECK_THIS_MAKEUP_PART_FOR_DLC_VALIDITY(iTempItem)

							// Current label
							IF iTempItem = sData.sBrowseInfo.iCurrentItem
								sData.sCurrentItemLabel = sData.sMakeup[iTempItem].sLabel
							ENDIF
							
							IF IS_MAKEUP_UNLOCKED_FOR_SHOP(iTempItem)
							AND NOT HAS_MAKEUP_BEEN_VIEWED_IN_SHOP(iTempItem)
								ADD_MENU_ITEM_TEXT(iTempItem, sData.sMakeup[iTempItem].sLabel, 1)
								ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LEFT_STAR)
								
								// FLAG: NEW
								IF iTempItem = sData.sBrowseInfo.iCurrentItem
									sData.bSelectedItemIsNew = TRUE
								ENDIF
							ELSE
								ADD_MENU_ITEM_TEXT(iTempItem, sData.sMakeup[iTempItem].sLabel)
							ENDIF
							
							// Show currently applied item
							IF iTempItem = sData.iPlayerMakeup
								ADD_MENU_ITEM_TEXT(iTempItem, "", 1)
								ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_MAKEUP)
									
							ELIF NOT IS_MAKEUP_UNLOCKED_FOR_SHOP(iTempItem)
								ADD_MENU_ITEM_TEXT(iTempItem, "", 1)
								ADD_MENU_ITEM_TEXT_COMPONENT_ICON(MENU_ICON_LOCK)
								
								// FLAG: LOCKED
								IF iTempItem = sData.sBrowseInfo.iCurrentItem
									sData.bSelectedItemIsLocked = TRUE
								ENDIF
							
							ELSE
								ADD_MENU_ITEM_TEXT(iTempItem, "ITEM_COST", 1)
								ADD_MENU_ITEM_TEXT_COMPONENT_INT(iCost)
							ENDIF
							
							// FLAG: DLC
							IF iTempItem = sData.sBrowseInfo.iCurrentItem
							AND GET_HASH_KEY(sData.sMakeup[iTempItem].sLabel) = HASH("MKUP_IND_0")
								sData.bSelectedItemIsDLC = TRUE
							ENDIF
						ENDIF
					ELSE
//						sData.iNumMakeupItems--
//						INT iLoop
//						FOR iLoop = iTempItem TO (COUNT_OF(sData.sMakeup) - 2)
//							sData.sMakeup[iLoop] = sData.sMakeup[iLoop + 1]
//						ENDFOR 
					ENDIF
					#ENDIF
					++ iTempItem
				ENDWHILE
				
				
				// highlight current item
				IF sData.bBlockAutoItemSelect
					sData.bBlockAutoItemSelect = FALSE
					SET_TOP_MENU_ITEM(sData.iTopMenuItem)
				ELSE
					sData.sBrowseInfo.iCurrentItem = sData.iPlayerMakeup
				ENDIF
				#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
				IF sData.sBrowseInfo.iCurrentItem >= COUNT_OF(sData.sMakeup)
					sData.sBrowseInfo.iCurrentItem = 0
				ENDIF
				#ENDIF
				
				SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
				
				// Telemetry
				#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
				STORE_CURRENT_SHOP_ITEM_DATA(sData.sMakeup[sData.sBrowseInfo.iCurrentItem].sLabel, GET_MAKEUP_ITEM_COST(sData, sData.sMakeup[sData.sBrowseInfo.iCurrentItem]))
				#ENDIF

				IF sData.bAllowZoomControl
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_BUY")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
					ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
					//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD, "ITEM_SCROLL")
					ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
					
					IF NOT IS_NGMP_MENU(sData.eCurrentMenu)
						ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
					ENDIF
					//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_RT, "ITEM_FSCROLL")
				ELSE
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_ACCEPT, "ITEM_BUY")
					ADD_MENU_HELP_KEY_CLICKABLE(INPUT_FRONTEND_CANCEL, "ITEM_BACK")
					ADD_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
					//ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_FRONTEND_GENERIC_UD, "ITEM_SCROLL")
					ADD_MENU_HELP_KEY_GROUP( INPUTGROUP_SCRIPT_RSTICK_ALL, "ITEM_MOV_CAM")
					//ADD_MENU_HELP_KEY_CLICKABLE(caZoomInput, "ITEM_ZOOM")
					//ADD_MENU_HELP_KEY_INPUT( INPUT_FRONTEND_RT, "ITEM_FSCROLL")
				ENDIF
			BREAK
			
			CASE HME_NGMP_MAIN
				BUILD_NGMP_MAIN_MENU(sData, pedID_ShopPed)
			BREAK
			CASE HME_NGMP_MAKEUP
				BUILD_NGMP_MAKEUP_MENU(sData, pedID_ShopPed)
			BREAK
			DEFAULT
				IF IS_NGMP_MENU(sData.eCurrentMenu)
					sData.bRebuildTintMenu = TRUE
					BUILD_NGMP_GENERIC_MENU(sData, pedID_ShopPed)
				ENDIF
			BREAK
		ENDSWITCH
		
		sData.bMenuInitialised = TRUE
		sData.bRebuildMenu = FALSE
		sData.bRebuildTintMenu = FALSE
	ENDIF
ENDPROC

///PURPOSE: Cycles through the available menu options
PROC UPDATE_MAIN_SELECTION(HAIRDO_SHOP_STRUCT &sData, PED_INDEX pedID_ShopPed)
	
	BOOL bMouseSelect = FALSE
	
	UPDATE_SHOP_INPUT_DATA(sData.sInputData, 300)
	
	// Beards and makeup add menu options,(makeup only in multiplayer)
	INT iMaxMenuItem = 0
	
	IF DOES_PLAYER_HAVE_BEARD_OPTIONS()
		++ iMaxMenuItem
	ENDIF
	
	IF DOES_PLAYER_HAVE_MAKEUP_OPTIONS()
		++ iMaxMenuItem
	ENDIF
	

	IF NOT IS_PED_INJURED(pedID_ShopPed)
	
		sData.sBrowseInfo.bItemSelected = FALSE
		
		HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
		
		IF IS_MENU_CURSOR_ACCEPT_PRESSED()
			IF g_iMenuCursorItem = sData.sBrowseInfo.iCurrentItem
				bMouseSelect = TRUE
			ELSE
				sData.sBrowseInfo.iCurrentItem = g_iMenuCursorItem
				PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
				SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
			ENDIF
		
		ENDIF
		
		// Change selection to previous item
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		OR (sData.sInputData.leftStickUD < -100 AND sData.sInputData.bLeftStickUDReset)
		OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sData.sInputData.bDPADUPReset)
		OR  IS_MENU_CURSOR_SCROLL_UP_PRESSED(sData.sInputData.bDPADUPReset)
			PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
			sData.sInputData.bDPADUPReset = FALSE
			sData.sInputData.bLeftStickUDReset = FALSE
			sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
			IF NETWORK_IS_GAME_IN_PROGRESS()
				sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
			ENDIF
			
			IF sData.sBrowseInfo.iCurrentItem > 0
				sData.sBrowseInfo.iCurrentItem--
			ELSE
				sData.sBrowseInfo.iCurrentItem = iMaxMenuItem
			ENDIF
			
			SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
		
		// Change selection to next item
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		OR (sData.sInputData.leftStickUD >  100 AND sData.sInputData.bLeftStickUDReset)
		OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sData.sInputData.bDPADDOWNReset)
		OR IS_MENU_CURSOR_SCROLL_DOWN_PRESSED(sData.sInputData.bDPADDOWNReset)
			PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
			sData.sInputData.bDPADDOWNReset = FALSE
			sData.sInputData.bLeftStickUDReset = FALSE
			sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
			IF NETWORK_IS_GAME_IN_PROGRESS()
				sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
			ENDIF
			
			IF sData.sBrowseInfo.iCurrentItem < iMaxMenuItem
				sData.sBrowseInfo.iCurrentItem++
			ELSE
				sData.sBrowseInfo.iCurrentItem = 0
			ENDIF
			
			SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
		
		// Make selection
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR bMouseSelect = TRUE
			bMouseSelect = FALSE
			PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
			// Move the the next menu
			IF sData.sBrowseInfo.iCurrentItem = 0
				sData.eCurrentMenu = HME_HAIR
				sData.bRebuildMenu = TRUE
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					sData.eCurrentMenu = HME_HAIR_GROUP
					sData.eCurrentHairGroup = HAIR_GROUP_ALL
				ENDIF

				// Populate the hairdo / beard list
				REPOPULATE_HAIRDO_MENU(sData.fpSetupClothingItemForShop, g_sShopCompItemData, TRUE, IS_SALON_ACTIVE(sData), sData.eCurrentHairGroup, sData.iHairdoGroupsAvailable, sData.iHairdoGroupsWithNewItems)
				CPRINTLN(DEBUG_SHOPS, "Grabbing hairdos list.")
				
			ELIF sData.sBrowseInfo.iCurrentItem = 1
				
				// If the player doesn't have beard options, but they do have makeup then
				// we need to switch the beard menu out for the makeup menu.
				IF DOES_PLAYER_HAVE_BEARD_OPTIONS()
					sData.eCurrentMenu = HME_BEARD
					
					// Populate the hairdo / beard list	
					IF NOT NETWORK_IS_GAME_IN_PROGRESS()
						REPOPULATE_HAIRDO_MENU(sData.fpSetupClothingItemForShop, g_sShopCompItemData, FALSE, IS_SALON_ACTIVE(sData), sData.eCurrentHairGroup, sData.iHairdoGroupsAvailable, sData.iHairdoGroupsWithNewItems)
						CPRINTLN(DEBUG_SHOPS, "Grabbing beards list.")
					ENDIF
					
				ELIF DOES_PLAYER_HAVE_MAKEUP_OPTIONS()
					sData.eCurrentMenu = HME_MAKEUP
				ENDIF
				sData.bRebuildMenu = TRUE
				
			ELIF sData.sBrowseInfo.iCurrentItem = 2
				sData.eCurrentMenu = HME_MAKEUP
				sData.bRebuildMenu = TRUE
			ENDIF
			
		// Exit
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		OR IS_MENU_CURSOR_CANCEL_PRESSED()
			PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET")
			sData.sBrowseInfo.bItemSelected = FALSE
			sData.sBrowseInfo.bItemPurchased = FALSE
			sData.sBrowseInfo.bPurchasingItem = FALSE
			sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_RESTORE_PED
			sData.sBrowseInfo.iControl = 0
		ENDIF
	ENDIF
ENDPROC

///PURPOSE: Cycles through the available beards
PROC UPDATE_BEARD_SELECTION(HAIRDO_SHOP_STRUCT &sData, PED_INDEX pedID_ShopPed)
	
	BOOL bMouseSelect = FALSE
		
	UPDATE_SHOP_INPUT_DATA(sData.sInputData, 300)
	
	IF sData.sBrowseInfo.iCurrentItem != -1
	AND sData.bSelectedItemIsNew
	AND NOT sData.bSelectedItemIsLocked
		SET_BEARD_BEEN_VIEWED_IN_SHOP(GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem))
	ENDIF
	
	IF NOT IS_PED_INJURED(pedID_ShopPed)
	
		sData.sBrowseInfo.bItemSelected = FALSE
		
		HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
			
		IF IS_MENU_CURSOR_ACCEPT_PRESSED()
			IF g_iMenuCursorItem = sData.sBrowseInfo.iCurrentItem
				bMouseSelect = TRUE
			ELSE
				sData.sBrowseInfo.iCurrentItem = g_iMenuCursorItem
				PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
				
				// Refresh if last selected item was new.
				
				// FLAG: LOCKED
				sData.bSelectedItemIsLocked = (NOT IS_BEARD_UNLOCKED_FOR_SHOP(sData.sBrowseInfo.iCurrentItem))
				// FLAG: NEW
				sData.bSelectedItemIsNew = (NOT sData.bSelectedItemIsLocked AND NOT HAS_BEARD_BEEN_VIEWED_IN_SHOP(sData.sBrowseInfo.iCurrentItem))
				// FLAG: DLC
				sData.bSelectedItemIsDLC = FALSE
				
				// Try before you buy
				SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_FACIAL_HAIR, GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem), sData.iTint1, 0, RT_NONE, sData.fCurrentBeardFade)
				GET_CURRENT_BEARD_COST(sData)
				
				IF sData.bRebuildMenu
					BUILD_HAIRDO_SHOP_MENU(sData, pedID_ShopPed)
					sData.bRebuildMenu = FALSE
				ELSE
					SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
				ENDIF
			ENDIF
	
		ENDIF
		
		// Change selection to previous item
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		OR IS_MENU_CURSOR_SCROLL_UP_PRESSED(sData.sInputData.bDPADUPReset)
		OR (sData.sInputData.leftStickUD < -100 AND sData.sInputData.bLeftStickUDReset)
		OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sData.sInputData.bDPADUPReset)
			PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
			sData.sInputData.bDPADUPReset = FALSE
			sData.sInputData.bLeftStickUDReset = FALSE
			sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
			IF NETWORK_IS_GAME_IN_PROGRESS()
				sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
			ENDIF
			
			IF sData.sBrowseInfo.iCurrentItem > 0
				sData.sBrowseInfo.iCurrentItem--
				WHILE IS_THIS_BEARD_DLC(GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem))
				AND NOT IS_DLC_INSTALLED_FOR_BEARD(GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem))
					sData.sBrowseInfo.iCurrentItem--
				ENDWHILE
			ELSE
				sData.sBrowseInfo.iCurrentItem = GET_THIS_PED_HEAD_OVERLAY_NUM()-1
			ENDIF
			
			// Refresh if last selected item was new.
			IF sData.bSelectedItemIsNew
				sData.bRebuildMenu = TRUE
				sData.bBlockAutoItemSelect = TRUE
				sData.iTopMenuItem = GET_TOP_MENU_ITEM()
			ENDIF
			
			// FLAG: LOCKED
			sData.bSelectedItemIsLocked = (NOT IS_BEARD_UNLOCKED_FOR_SHOP(GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)))
			// FLAG: NEW
			sData.bSelectedItemIsNew = (NOT sData.bSelectedItemIsLocked AND NOT HAS_BEARD_BEEN_VIEWED_IN_SHOP(GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)))
			// FLAG: DLC
			TEXT_LABEL_15 tlTempLabel
			tlTempLabel = "HAIR_BEARD"
			tlTempLabel += GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)
			sData.bSelectedItemIsDLC = SET_DLC_BEARD_LABEL(tlTempLabel, GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem))
			sData.sCurrentItemLabel = tlTempLabel
			
			// Try before you buy
			SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_FACIAL_HAIR, GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)-1, sData.iTint1, 0, RT_NONE, sData.fCurrentBeardFade)
			GET_CURRENT_BEARD_COST(sData)
			
			IF sData.bRebuildMenu
				BUILD_HAIRDO_SHOP_MENU(sData, pedID_ShopPed)
				sData.bRebuildMenu = FALSE
			ELSE
				SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
			ENDIF

		
		// Change selection to next item
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		OR IS_MENU_CURSOR_SCROLL_DOWN_PRESSED(sData.sInputData.bDPADDOWNReset)
		OR (sData.sInputData.leftStickUD >  100 AND sData.sInputData.bLeftStickUDReset)
		OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sData.sInputData.bDPADDOWNReset)
			PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
			sData.sInputData.bDPADDOWNReset = FALSE
			sData.sInputData.bLeftStickUDReset = FALSE
			sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
			IF NETWORK_IS_GAME_IN_PROGRESS()
				sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
			ENDIF
			IF sData.sBrowseInfo.iCurrentItem < GET_THIS_PED_HEAD_OVERLAY_NUM()-1
				sData.sBrowseInfo.iCurrentItem++
				WHILE IS_THIS_BEARD_DLC(GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem))
				AND NOT IS_DLC_INSTALLED_FOR_BEARD(GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem))
					sData.sBrowseInfo.iCurrentItem++
				ENDWHILE
			ELSE
				sData.sBrowseInfo.iCurrentItem = 0
			ENDIF
			
			// Refresh if last selected item was new.
			IF sData.bSelectedItemIsNew
				sData.bRebuildMenu = TRUE
				sData.bBlockAutoItemSelect = TRUE
				sData.iTopMenuItem = GET_TOP_MENU_ITEM()
			ENDIF
			
			// FLAG: LOCKED
			sData.bSelectedItemIsLocked = (NOT IS_BEARD_UNLOCKED_FOR_SHOP(GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)))
			// FLAG: NEW
			sData.bSelectedItemIsNew = (NOT sData.bSelectedItemIsLocked AND NOT HAS_BEARD_BEEN_VIEWED_IN_SHOP(GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)))
			// FLAG: DLC
			TEXT_LABEL_15 tlTempLabel
			tlTempLabel = "HAIR_BEARD"
			tlTempLabel += GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)
			sData.bSelectedItemIsDLC = SET_DLC_BEARD_LABEL(tlTempLabel, GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem))
			sData.sCurrentItemLabel = tlTempLabel
			
			// Try before you buy
			SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_FACIAL_HAIR, GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)-1, sData.iTint1, 0, RT_NONE, sData.fCurrentBeardFade)
			GET_CURRENT_BEARD_COST(sData)
			
			IF sData.bRebuildMenu
				BUILD_HAIRDO_SHOP_MENU(sData, pedID_ShopPed)
				sData.bRebuildMenu = FALSE
			ELSE
				SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
			ENDIF
		
		// Make selection
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR bMouseSelect = TRUE
			bMouseSelect = FALSE
			bZoomToggle = FALSE
			
			PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
			// Already have
			IF GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem) = sData.iCurrentBeard
				SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_CUR_1", 4000)
				sData.iOriginalBeard = GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)
			// Locked
			ELIF NOT IS_BEARD_UNLOCKED_FOR_SHOP(GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem))
				SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_LOCK", 4000)
			
			// Can't afford
			ELIF NOT CAN_PLAYER_AFFORD_ITEM_COST(sData.sBrowseInfo.iCurrentSubItem)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_AFF_1", 4000)
				STORE_LAST_VIEWED_SHOP_ITEM_FOR_COMMERCE_STORE(GET_HASH_KEY(g_sShopSettings.tlCurrentItem_label), g_sShopSettings.iCurrentItem_cost, GET_HASH_KEY(GET_SHOP_NAME(sData.sShopInfo.eShop)))
				LAUNCH_STORE_CASH_ALERT(FALSE, STAT_SAVETYPE_END_SHOPPING, SPL_STORE)
				
			// Failed to add to basket
			ELIF NETWORK_IS_GAME_IN_PROGRESS()
			AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
			AND NOT NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, GET_CATALOGUE_INV_CATEGORY_FOR_HAIRDO_MENU(sData.eCurrentMenu, INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1)), GET_HAIRDO_KEY_FOR_CATALOGUE(sData.eCurrentMenu, sData.sCurrentItemLabel), NET_SHOP_ACTION_BUY_ITEM, 1, sData.sBrowseInfo.iCurrentSubItem, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, GET_CATALOGUE_INVENTORY_FOR_HAIRDO_MENU(sData.eCurrentMenu, INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1)))
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				CPRINTLN(DEBUG_SHOPS, "Can't add item to basket. Menu = ", sData.eCurrentMenu, ", Label = ", sData.sCurrentItemLabel)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_FAIL", 4000)
			
			ELIF NETWORK_IS_GAME_IN_PROGRESS()
			AND DOES_SAVE_HAVE_COUPON(COUPON_HAIRCUT)
			AND NOT NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, GET_CATALOGUE_INV_CATEGORY_FOR_HAIRDO_MENU(sData.eCurrentMenu, INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1)), HASH("PO_COUPON_HAIRCUT"), NET_SHOP_ACTION_SPEND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, GET_CATALOGUE_INVENTORY_FOR_HAIRDO_MENU(sData.eCurrentMenu, INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1)))
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				CPRINTLN(DEBUG_SHOPS, "Can't add coupon discount to basket. Menu = ", sData.eCurrentMenu, ", Label = ", sData.sCurrentItemLabel)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_FAIL", 4000)
				
			// Start cut process
			ELSE
				IF NETWORK_IS_GAME_IN_PROGRESS()
				AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
					sData.sBrowseInfo.bProcessingBasket = TRUE
					sData.sBrowseInfo.iProcessingBasketStage = 0
				ENDIF
			
				// Reset to actual beard and buy
				SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_FACIAL_HAIR, sData.iCurrentBeard-1, sData.iTint1, 0, RT_NONE, sData.fCurrentBeardFade)
				sData.sBrowseInfo.bItemSelected = TRUE
				sData.iOriginalBeard = GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem) //sData.iCurrentBeard
			ENDIF
			
		// Back
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		OR IS_MENU_CURSOR_CANCEL_PRESSED()
			PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET")
			// Reset beard
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
				SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_FACIAL_HAIR, sData.iOriginalBeard-1, sData.iTint1, 0, RT_NONE, sData.fCurrentBeardFade)
			ELSE
				SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_FACIAL_HAIR, sData.iCurrentBeard+1, sData.iTint1, 0, RT_NONE, sData.fCurrentBeardFade)
			ENDIF
			
			sData.sBrowseInfo.bItemSelected = FALSE
			sData.sBrowseInfo.bItemPurchased = FALSE
			sData.sBrowseInfo.bPurchasingItem = FALSE
			sData.sBrowseInfo.iCurrentItem = 1 // Beards
			sData.eCurrentMenu = HME_MAIN_MENU
			sData.bRebuildMenu = TRUE
		ENDIF
	ENDIF
ENDPROC

PROC UPDATE_HAIRDO_GROUP_SELECTION(HAIRDO_SHOP_STRUCT &sData, PED_INDEX pedID_ShopPed)

	BOOL bMouseSelect = FALSE
	
	
	
	UPDATE_SHOP_INPUT_DATA(sData.sInputData, 300)
	
	IF NOT IS_PED_INJURED(pedID_ShopPed)
		
		sData.sBrowseInfo.bItemSelected = FALSE
		
		HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
			
		// Check for mouse selection of item
		IF IS_MENU_CURSOR_ACCEPT_PRESSED()
			IF g_iMenuCursorItem = ENUM_TO_INT(sData.eCurrentHairGroup)
				bMouseSelect = TRUE
			
			ELIF IS_BIT_SET(sData.iHairdoGroupsAvailable[g_iMenuCursorItem/32], g_iMenuCursorItem%32)

				sData.eCurrentHairGroup = INT_TO_ENUM(HAIRDO_GROUP_ENUM, g_iMenuCursorItem)
				SET_CURRENT_MENU_ITEM(g_iMenuCursorItem)
			
				PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			ENDIF
		ENDIF

		// Change selection to previous item
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sData.sInputData.bDPADUPReset)
		OR (sData.sInputData.leftStickUD < -100 AND sData.sInputData.bLeftStickUDReset)
		OR IS_MENU_CURSOR_SCROLL_UP_PRESSED( sData.sInputData.bDPADUPReset )
			
			PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
			sData.sInputData.bDPADUPReset = FALSE
			sData.sInputData.bLeftStickUDReset = FALSE
			sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
				sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
			ENDIF
			
			BOOL bFoundNextGroup = FALSE
			INT i
			FOR i = ENUM_TO_INT(sData.eCurrentHairGroup)-1 TO 0 STEP -1
				IF IS_BIT_SET(sData.iHairdoGroupsAvailable[i/32], i%32)
					bFoundNextGroup = TRUE
					sData.eCurrentHairGroup = INT_TO_ENUM(HAIRDO_GROUP_ENUM, i)
					i = -1// Bail
				ENDIF
			ENDFOR
			IF NOT bFoundNextGroup
				FOR i = NUMBER_OF_HAIRDO_GROUPS-1 TO ENUM_TO_INT(sData.eCurrentHairGroup)+1 STEP -1
					IF IS_BIT_SET(sData.iHairdoGroupsAvailable[i/32], i%32)
						bFoundNextGroup = TRUE
						sData.eCurrentHairGroup = INT_TO_ENUM(HAIRDO_GROUP_ENUM, i)
						i = -1// Bail
					ENDIF
				ENDFOR
			ENDIF
			
			IF bFoundNextGroup
				SET_CURRENT_MENU_ITEM(ENUM_TO_INT(sData.eCurrentHairGroup))
			ENDIF
		
		// Change selection to next item
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		OR   (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sData.sInputData.bDPADDOWNReset)
		OR (sData.sInputData.leftStickUD >  100 AND sData.sInputData.bLeftStickUDReset)
		OR   IS_MENU_CURSOR_SCROLL_DOWN_PRESSED( sData.sInputData.bDPADDOWNReset )
			
			PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
			sData.sInputData.bDPADDOWNReset = FALSE
			sData.sInputData.bLeftStickUDReset = FALSE
			sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
			IF NETWORK_IS_GAME_IN_PROGRESS()
				sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
			ENDIF
			
			BOOL bFoundNextGroup = FALSE
			INT i
			FOR i = ENUM_TO_INT(sData.eCurrentHairGroup)+1 TO NUMBER_OF_HAIRDO_GROUPS-1
				IF IS_BIT_SET(sData.iHairdoGroupsAvailable[i/32], i%32)
					bFoundNextGroup = TRUE
					sData.eCurrentHairGroup = INT_TO_ENUM(HAIRDO_GROUP_ENUM, i)
					i = NUMBER_OF_HAIRDO_GROUPS+1// Bail
				ENDIF
			ENDFOR
			IF NOT bFoundNextGroup
				FOR i = 0 TO ENUM_TO_INT(sData.eCurrentHairGroup)-1
					IF IS_BIT_SET(sData.iHairdoGroupsAvailable[i/32], i%32)
						bFoundNextGroup = TRUE
						sData.eCurrentHairGroup = INT_TO_ENUM(HAIRDO_GROUP_ENUM, i)
						i = NUMBER_OF_HAIRDO_GROUPS+1// Bail
					ENDIF
				ENDFOR
			ENDIF
			
			IF bFoundNextGroup
				SET_CURRENT_MENU_ITEM(ENUM_TO_INT(sData.eCurrentHairGroup))
			ENDIF
		
		// Accept selection
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR   bMouseSelect = TRUE
		
			PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			sData.eCurrentMenu = HME_HAIR
			sData.bRebuildMenu = TRUE
			
			// Populate the hairdo / beard list
			REPOPULATE_HAIRDO_MENU(sData.fpSetupClothingItemForShop, g_sShopCompItemData, TRUE, IS_SALON_ACTIVE(sData), sData.eCurrentHairGroup, sData.iHairdoGroupsAvailable, sData.iHairdoGroupsWithNewItems)
			CPRINTLN(DEBUG_SHOPS, "Grabbing hairdos list.")
			
		// Back
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		OR   IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		OR   IS_MENU_CURSOR_CANCEL_PRESSED()
	
			PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET")
			sData.sBrowseInfo.bItemSelected = FALSE
			sData.sBrowseInfo.bItemPurchased = FALSE
			sData.sBrowseInfo.bPurchasingItem = FALSE
			
			IF DOES_PLAYER_HAVE_BEARD_OPTIONS() 
			OR DOES_PLAYER_HAVE_MAKEUP_OPTIONS()
				sData.eCurrentMenu = HME_MAIN_MENU
				sData.sBrowseInfo.iCurrentItem = 0 
				sData.bRebuildMenu = TRUE
				BUILD_HAIRDO_SHOP_MENU(sData, pedID_ShopPed)
			ELSE
				sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_OUTRO
				sData.sBrowseInfo.iControl = 0
			ENDIF
			
		ENDIF	
	ENDIF
ENDPROC

///PURPOSE: Cycles through the available hairdos
PROC UPDATE_HAIRDO_SELECTION(HAIRDO_SHOP_STRUCT &sData, PED_INDEX pedID_ShopPed)
	MODEL_NAMES ePedModel
	
	BOOL bMouseSelect = FALSE
			
	UPDATE_SHOP_INPUT_DATA(sData.sInputData, 300)
	
	IF NOT IS_PED_INJURED(pedID_ShopPed)
	
		sData.sBrowseInfo.bItemSelected = FALSE
		ePedModel = GET_ENTITY_MODEL(pedID_ShopPed)
		
		////////////////////////////
		///      Mouse control
		////////////////////////////
		///      
		///      

		IF IS_PC_VERSION()
			// Get the menu item index that the mouse is over. -1 means the mouse is outside the menu area.
			HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS() 
			
			// We know the mouse is inside the menu now...
			IF IS_MENU_CURSOR_ACCEPT_PRESSED()
					
				IF g_iMenuCursorItem = sData.sBrowseInfo.iCurrentItem
					bMouseSelect = TRUE
				ELSE
				
					sData.sBrowseInfo.iCurrentItem = g_iMenuCursorItem
				
					IF GET_CURRENT_SHOP_MENU_ITEM(g_sShopCompItemData, CLO_MENU_NONE, sData.sBrowseInfo.iCurrentItem)

						// Refresh if last selected item was new.
						IF sData.bSelectedItemIsNew
							sData.bRebuildMenu = TRUE
							sData.bBlockAutoItemSelect = TRUE
							sData.iTopMenuItem = GET_TOP_MENU_ITEM()
						ENDIF
						
						// FLAG: LOCKED
						sData.bSelectedItemIsLocked = (NOT CALL sData.fpIsPedCompItemAvailable(ePedModel, g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]))
						// FLAG: NEW
						sData.bSelectedItemIsNew = (NOT sData.bSelectedItemIsLocked AND CALL sData.fpIsPedCompItemNew(ePedModel, g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]))
						// FLAG: DLC
						sData.bSelectedItemIsDLC = IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT)
						
						sData.sCurrentItemLabel = g_sTempCompData[1].sLabel
						
						CPRINTLN(DEBUG_SHOPS, "UPDATE_HAIRDO_SELECTION. Change selection to previous item: sData.sBrowseInfo.iCurrentItem = ", sData.sBrowseInfo.iCurrentItem, ", NEW=", sData.bSelectedItemIsNew, ", LOCKED=", sData.bSelectedItemIsLocked)
						
						CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem], FALSE)
						GET_CURRENT_HAIRDO_COST(sData)
						
						IF sData.bRebuildMenu
							BUILD_HAIR_MENU(sData, pedID_ShopPed, sData.bSelectedItemIsNew)
							sData.bRebuildMenu = FALSE
						ELSE
							SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
						ENDIF

					ENDIF
															
					PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
				
				ENDIF
		
			ENDIF
			
		ENDIF
				
		// Change selection to previous item
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		OR (sData.sInputData.leftStickUD < -100 AND sData.sInputData.bLeftStickUDReset)
		OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sData.sInputData.bDPADUPReset)
		OR IS_MENU_CURSOR_SCROLL_UP_PRESSED( sData.sInputData.bDPADUPReset )
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
			PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
			sData.sInputData.bDPADUPReset = FALSE
			sData.sInputData.bLeftStickUDReset = FALSE
			sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
				sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
			ENDIF
			
			IF GET_PREV_SHOP_MENU_ITEM(g_sShopCompItemData, CLO_MENU_NONE, sData.sBrowseInfo.iCurrentItem, sData.sBrowseInfo.iCurrentItem, TRUE)
				
				// Refresh if last selected item was new.
				IF sData.bSelectedItemIsNew
					sData.bRebuildMenu = TRUE
					sData.bBlockAutoItemSelect = TRUE
					sData.iTopMenuItem = GET_TOP_MENU_ITEM()
				ENDIF
				
				// FLAG: LOCKED
				sData.bSelectedItemIsLocked = (NOT CALL sData.fpIsPedCompItemAvailable(ePedModel, g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]))
				// FLAG: NEW
				sData.bSelectedItemIsNew = (NOT sData.bSelectedItemIsLocked AND CALL sData.fpIsPedCompItemNew(ePedModel, g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]))
				// FLAG: DLC
				sData.bSelectedItemIsDLC = IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT)
				
				sData.sCurrentItemLabel = g_sTempCompData[1].sLabel
				
				CPRINTLN(DEBUG_SHOPS, "UPDATE_HAIRDO_SELECTION. Change selection to previous item: sData.sBrowseInfo.iCurrentItem = ", sData.sBrowseInfo.iCurrentItem, ", NEW=", sData.bSelectedItemIsNew, ", LOCKED=", sData.bSelectedItemIsLocked)
				
				CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem], FALSE)
				GET_CURRENT_HAIRDO_COST(sData)
				
				IF sData.bRebuildMenu
					BUILD_HAIR_MENU(sData, pedID_ShopPed, sData.bSelectedItemIsNew)
					sData.bRebuildMenu = FALSE
				ELSE
					SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
				ENDIF
			ENDIF
		
		
		// Change selection to next item
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		OR (sData.sInputData.leftStickUD >  100 AND sData.sInputData.bLeftStickUDReset)
		OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sData.sInputData.bDPADDOWNReset)
		OR IS_MENU_CURSOR_SCROLL_DOWN_PRESSED( sData.sInputData.bDPADDOWNReset )
		OR IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
			PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
			sData.sInputData.bDPADDOWNReset = FALSE
			sData.sInputData.bLeftStickUDReset = FALSE
			sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
			IF NETWORK_IS_GAME_IN_PROGRESS()
				sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
			ENDIF
			
			IF GET_NEXT_SHOP_MENU_ITEM(g_sShopCompItemData, CLO_MENU_NONE, sData.sBrowseInfo.iCurrentItem, sData.sBrowseInfo.iCurrentItem, TRUE)
				
				// Refresh if last selected item was new.
				IF sData.bSelectedItemIsNew
					sData.bRebuildMenu = TRUE
					sData.bBlockAutoItemSelect = TRUE
					sData.iTopMenuItem = GET_TOP_MENU_ITEM()
				ENDIF
				
				// FLAG: LOCKED
				sData.bSelectedItemIsLocked = (NOT CALL sData.fpIsPedCompItemAvailable(ePedModel, g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]))
				// FLAG: NEW
				sData.bSelectedItemIsNew = (NOT sData.bSelectedItemIsLocked AND CALL sData.fpIsPedCompItemNew(ePedModel, g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]))
				// FLAG: DLC
				sData.bSelectedItemIsDLC = IS_BIT_SET(g_sTempCompData[1].iProperties, PED_COMPONENT_IS_DLC_BIT)
				
				sData.sCurrentItemLabel = g_sTempCompData[1].sLabel
				
				CPRINTLN(DEBUG_SHOPS, "UPDATE_HAIRDO_SELECTION. Change selection to previous item: sData.sBrowseInfo.iCurrentItem = ", sData.sBrowseInfo.iCurrentItem, ", NEW=", sData.bSelectedItemIsNew, ", LOCKED=", sData.bSelectedItemIsLocked)
				
				CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem], FALSE)
				GET_CURRENT_HAIRDO_COST(sData)
				
				IF sData.bRebuildMenu
					BUILD_HAIR_MENU(sData, pedID_ShopPed, sData.bSelectedItemIsNew)
					sData.bRebuildMenu = FALSE
				ELSE
					SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
				ENDIF
			ENDIF
		
		
		// Make selection
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT) OR bMouseSelect = TRUE
			bMouseSelect = FALSE
			bZoomToggle = FALSE
			PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
			
			// Locked
			IF NOT CALL sData.fpIsPedCompItemAvailable(ePedModel, g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem])
				SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_LOCK", 4000)
				
			// Already have
			ELIF IS_HAIR_OR_BEARD_ITEM_CURRENT(sData,sData.sBrowseInfo.iCurrentItem)
				// We just queried this item so we can use the global struct to check dlc flag.
				IF sData.bSelectedItemIsDLC
				AND NOT IS_STRING_NULL_OR_EMPTY(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel))
					SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_CUR_2_DLC", 4000)
					ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel))
				ELSE
					SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_CUR_2", 4000)
				ENDIF
				
			// Can't afford
			ELIF NOT CAN_PLAYER_AFFORD_ITEM_COST(sData.sBrowseInfo.iCurrentSubItem)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_AFF_2", 4000)
				STORE_LAST_VIEWED_SHOP_ITEM_FOR_COMMERCE_STORE(GET_HASH_KEY(g_sShopSettings.tlCurrentItem_label), g_sShopSettings.iCurrentItem_cost, GET_HASH_KEY(GET_SHOP_NAME(sData.sShopInfo.eShop)))
				LAUNCH_STORE_CASH_ALERT(FALSE, STAT_SAVETYPE_END_SHOPPING, SPL_STORE)
				
			// Failed to add to basket
			ELIF NETWORK_IS_GAME_IN_PROGRESS()
			AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
			AND NOT NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, GET_CATALOGUE_INV_CATEGORY_FOR_HAIRDO_MENU(sData.eCurrentMenu, INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1)), GET_HAIRDO_KEY_FOR_CATALOGUE(sData.eCurrentMenu, sData.sCurrentItemLabel), NET_SHOP_ACTION_BUY_ITEM, 1, sData.sBrowseInfo.iCurrentSubItem, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, GET_CATALOGUE_INVENTORY_FOR_HAIRDO_MENU(sData.eCurrentMenu, INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1)))
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				CPRINTLN(DEBUG_SHOPS, "Can't add item to basket. Menu = ", sData.eCurrentMenu, ", Label = ", sData.sCurrentItemLabel)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_FAIL", 4000)
			
			ELIF NETWORK_IS_GAME_IN_PROGRESS()
			AND DOES_SAVE_HAVE_COUPON(COUPON_HAIRCUT)
			AND NOT NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, GET_CATALOGUE_INV_CATEGORY_FOR_HAIRDO_MENU(sData.eCurrentMenu, INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1)), HASH("PO_COUPON_HAIRCUT"), NET_SHOP_ACTION_SPEND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, GET_CATALOGUE_INVENTORY_FOR_HAIRDO_MENU(sData.eCurrentMenu, INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1)))
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				CPRINTLN(DEBUG_SHOPS, "Can't add coupon discount to basket. Menu = ", sData.eCurrentMenu, ", Label = ", sData.sCurrentItemLabel)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_FAIL", 4000)
				
			// Start cut process
			ELSE
			
				IF NETWORK_IS_GAME_IN_PROGRESS()
				AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
					sData.sBrowseInfo.bProcessingBasket = TRUE
					sData.sBrowseInfo.iProcessingBasketStage = 0
				ENDIF
			
				IF sData.eCurrentMenu = HME_HAIR
					// Reset to actual do and buy
					CPRINTLN(DEBUG_SHOPS, "UPDATE_HAIRDO_SELECTION. (Hair) Reset to actual do and buy: sData.iCurrentHairdo = ", sData.iCurrentHairdo)
					CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, sData.iCurrentHairdo), FALSE)
					sData.sBrowseInfo.bItemSelected = TRUE
					
				ELIF sData.eCurrentMenu = HME_BEARD
					// Reset to actual do and buy
					CPRINTLN(DEBUG_SHOPS, "UPDATE_HAIRDO_SELECTION. (Beard) Reset to actual do and buy: sData.iCurrentBeard = ", sData.iCurrentBeard)
					CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, sData.iCurrentBeard), FALSE)
					sData.sBrowseInfo.bItemSelected = TRUE
				ENDIF
			ENDIF
			
		// Back
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_MENU_CURSOR_CANCEL_PRESSED()
			PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET")
			sData.sBrowseInfo.bItemSelected = FALSE
			sData.sBrowseInfo.bItemPurchased = FALSE
			sData.sBrowseInfo.bPurchasingItem = FALSE
			
			IF DOES_PLAYER_HAVE_BEARD_OPTIONS() 
			OR DOES_PLAYER_HAVE_MAKEUP_OPTIONS()
			
				IF sData.eCurrentMenu = HME_HAIR
					// Reset hairdo
					CPRINTLN(DEBUG_SHOPS, "UPDATE_HAIRDO_SELECTION. Back,  Reset hairdo: sData.iCurrentHairdo = ", sData.iCurrentHairdo)
					CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_HAIR, INT_TO_ENUM(PED_COMP_NAME_ENUM, sData.iCurrentHairdo), FALSE)
				
				ELIF sData.eCurrentMenu = HME_BEARD
					// Reset beard
					CPRINTLN(DEBUG_SHOPS, "UPDATE_HAIRDO_SELECTION. Back,  Reset Beard: sData.iCurrentBeard = ", sData.iCurrentBeard)
					CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_BERD, INT_TO_ENUM(PED_COMP_NAME_ENUM, sData.iCurrentBeard), FALSE)
				ENDIF
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					sData.eCurrentMenu = HME_HAIR_GROUP
					sData.bBlockAutoItemSelect = TRUE
					// Populate the hairdo list
					REPOPULATE_HAIRDO_MENU(sData.fpSetupClothingItemForShop, g_sShopCompItemData, TRUE, IS_SALON_ACTIVE(sData), HAIR_GROUP_ALL, sData.iHairdoGroupsAvailable, sData.iHairdoGroupsWithNewItems)
				ELSE
					sData.eCurrentMenu = HME_MAIN_MENU
				ENDIF
				sData.sBrowseInfo.iCurrentItem = 0 
				sData.bRebuildMenu = TRUE
			ELSE
				sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_OUTRO
				sData.sBrowseInfo.iControl = 0
			ENDIF
		ENDIF
	ENDIF
ENDPROC

///PURPOSE: Cycles through the available makeup
PROC UPDATE_MAKEUP_SELECTION(HAIRDO_SHOP_STRUCT &sData, PED_INDEX pedID_ShopPed)
	
	BOOL bMouseSelect = FALSE
		
	UPDATE_SHOP_INPUT_DATA(sData.sInputData, 300)
	
	IF sData.sBrowseInfo.iCurrentItem != -1
	AND sData.bSelectedItemIsNew
	AND NOT sData.bSelectedItemIsLocked
		SET_MAKEUP_BEEN_VIEWED_IN_SHOP(sData.sBrowseInfo.iCurrentItem)
	ENDIF
	
	IF NOT IS_PED_INJURED(pedID_ShopPed)
	
		sData.sBrowseInfo.bItemSelected = FALSE
		

		HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
			
		IF IS_MENU_CURSOR_ACCEPT_PRESSED()
			
			IF g_iMenuCursorItem = sData.sBrowseInfo.iCurrentItem
				bMouseSelect = TRUE
			ELSE
				sData.sBrowseInfo.iCurrentItem = g_iMenuCursorItem
				
				PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
				
				SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_MAKEUP, sData.sBrowseInfo.iCurrentItem, sData.iTint1, 0, RT_NONE, 1.0)
				//Hide or unhide eyebrows because some makeup contains eyebrows for some reason 
				IF sData.sBrowseInfo.iCurrentItem-1 = -1 
				OR sData.sBrowseInfo.iCurrentItem-1 > 15
					SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_EYEBROW, GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_ACTIVE_CHARACTER_SLOT()), sData.iTint1, 0, RT_NONE, 1.0)
				ELSE
					SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_EYEBROW, GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_ACTIVE_CHARACTER_SLOT()), sData.iTint1, 0, RT_NONE, 0.0)
				ENDIF
				
				IF sData.bRebuildMenu
					BUILD_HAIRDO_SHOP_MENU(sData, pedID_ShopPed)
					sData.bRebuildMenu = FALSE
				ELSE
					SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
				ENDIF
				
				// Telemtry
				#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
				STORE_CURRENT_SHOP_ITEM_DATA(sData.sMakeup[sData.sBrowseInfo.iCurrentItem].sLabel, GET_MAKEUP_ITEM_COST(sData, sData.sMakeup[sData.sBrowseInfo.iCurrentItem]))
				#ENDIF
			ENDIF
	
		ENDIF
		
		// Change selection to previous item
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		OR IS_MENU_CURSOR_SCROLL_UP_PRESSED(sData.sInputData.bDPADUPReset)
		OR (sData.sInputData.leftStickUD < -100 AND sData.sInputData.bLeftStickUDReset)
		OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP) AND sData.sInputData.bDPADUPReset)
			PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
			sData.sInputData.bDPADUPReset = FALSE
			sData.sInputData.bLeftStickUDReset = FALSE
			sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
				sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
			ENDIF
			
			IF sData.sBrowseInfo.iCurrentItem > 0
				sData.sBrowseInfo.iCurrentItem--
				WHILE NOT CHECK_THIS_MAKEUP_PART_FOR_DLC_VALIDITY(sData.sBrowseInfo.iCurrentItem)
					sData.sBrowseInfo.iCurrentItem--
				ENDWHILE
			ELSE
				sData.sBrowseInfo.iCurrentItem = (sData.iNumMakeupItems -1)
				WHILE NOT CHECK_THIS_MAKEUP_PART_FOR_DLC_VALIDITY(sData.sBrowseInfo.iCurrentItem)
					sData.sBrowseInfo.iCurrentItem--
				ENDWHILE
			ENDIF
			
			// Refresh if last selected item was new.
			IF sData.bSelectedItemIsNew
				sData.bRebuildMenu = TRUE
				sData.bBlockAutoItemSelect = TRUE
				sData.iTopMenuItem = GET_TOP_MENU_ITEM()
			ENDIF
			
			// FLAG: LOCKED
			sData.bSelectedItemIsLocked = (NOT IS_MAKEUP_UNLOCKED_FOR_SHOP(sData.sBrowseInfo.iCurrentItem))
			// FLAG: NEW
			sData.bSelectedItemIsNew = (NOT sData.bSelectedItemIsLocked AND NOT HAS_MAKEUP_BEEN_VIEWED_IN_SHOP(sData.sBrowseInfo.iCurrentItem))
			// FLAG: DLC
			sData.bSelectedItemIsDLC = FALSE
			
			// Try before you buy			
			IF	sData.sBrowseInfo.iCurrentItem != -1
			
			#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
			AND	sData.sBrowseInfo.iCurrentItem < COUNT_OF(sData.sMakeup)
			#ENDIF
			
				SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_MAKEUP, sData.sBrowseInfo.iCurrentItem-1, sData.iTint1, 0, RT_NONE, 1.0)
				//Hide or unhide eyebrows because some makeup contains eyebrows for some reason 
				IF sData.sBrowseInfo.iCurrentItem-1 = -1 
				OR sData.sBrowseInfo.iCurrentItem-1 > 15
					SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_EYEBROW, GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_ACTIVE_CHARACTER_SLOT()), sData.iTint1, 0, RT_NONE, 1.0)
				ELSE
					SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_EYEBROW, GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_ACTIVE_CHARACTER_SLOT()), sData.iTint1, 0, RT_NONE, 0.0)
				ENDIF
				
				IF sData.bRebuildMenu
					BUILD_HAIRDO_SHOP_MENU(sData, pedID_ShopPed)
					sData.bRebuildMenu = FALSE
				ELSE
					SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
				ENDIF

				// Telemtry
				
				#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
				STORE_CURRENT_SHOP_ITEM_DATA(sData.sMakeup[sData.sBrowseInfo.iCurrentItem].sLabel, GET_MAKEUP_ITEM_COST(sData, sData.sMakeup[sData.sBrowseInfo.iCurrentItem]))
				#ENDIF
			ENDIF
		
		// Change selection to next item
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		OR IS_MENU_CURSOR_SCROLL_DOWN_PRESSED(sData.sInputData.bDPADDOWNReset)
		OR (sData.sInputData.leftStickUD >  100 AND sData.sInputData.bDPADDOWNReset)
		OR (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN) AND sData.sInputData.bDPADDOWNReset)
			PLAY_SOUND_FRONTEND(-1,"NAV_UP_DOWN","HUD_FRONTEND_DEFAULT_SOUNDSET")
			sData.sInputData.bDPADDOWNReset = FALSE
			sData.sInputData.bLeftStickUDReset = FALSE
			sData.sInputData.iLeftStickUDTimer = GET_GAME_TIMER()
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
				sData.sInputData.iMP_LeftStickUDTimer = GET_NETWORK_TIME()
			ENDIF
			IF sData.sBrowseInfo.iCurrentItem < (sData.iNumMakeupItems -1)
				sData.sBrowseInfo.iCurrentItem++
				WHILE NOT CHECK_THIS_MAKEUP_PART_FOR_DLC_VALIDITY(sData.sBrowseInfo.iCurrentItem)
				
				#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
				AND sData.sBrowseInfo.iCurrentItem < COUNT_OF(sData.sMakeup)
				#ENDIF
					sData.sBrowseInfo.iCurrentItem++
				ENDWHILE
				
				IF NOT CHECK_THIS_MAKEUP_PART_FOR_DLC_VALIDITY(sData.sBrowseInfo.iCurrentItem)
				OR sData.sBrowseInfo.iCurrentItem >= (sData.iNumMakeupItems)
					sData.sBrowseInfo.iCurrentItem = 0
					WHILE NOT CHECK_THIS_MAKEUP_PART_FOR_DLC_VALIDITY(sData.sBrowseInfo.iCurrentItem)
					
					#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
					AND sData.sBrowseInfo.iCurrentItem < COUNT_OF(sData.sMakeup)
					#ENDIF
					
						sData.sBrowseInfo.iCurrentItem++
					ENDWHILE
				ENDIF
			ELSE
				sData.sBrowseInfo.iCurrentItem = 0
				WHILE NOT CHECK_THIS_MAKEUP_PART_FOR_DLC_VALIDITY(sData.sBrowseInfo.iCurrentItem)
				
				#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
				AND sData.sBrowseInfo.iCurrentItem < COUNT_OF(sData.sMakeup)
				#ENDIF
				
					sData.sBrowseInfo.iCurrentItem++
				ENDWHILE
			ENDIF
			
			// Refresh if last selected item was new.
			IF sData.bSelectedItemIsNew
				sData.bRebuildMenu = TRUE
				sData.bBlockAutoItemSelect = TRUE
				sData.iTopMenuItem = GET_TOP_MENU_ITEM()
			ENDIF
			
			// FLAG: LOCKED
			sData.bSelectedItemIsLocked = (NOT IS_MAKEUP_UNLOCKED_FOR_SHOP(sData.sBrowseInfo.iCurrentItem))
			// FLAG: NEW
			sData.bSelectedItemIsNew = (NOT sData.bSelectedItemIsLocked AND NOT HAS_MAKEUP_BEEN_VIEWED_IN_SHOP(sData.sBrowseInfo.iCurrentItem))
			// FLAG: DLC
			sData.bSelectedItemIsDLC = FALSE
			
			// Try before you buy			
			IF	sData.sBrowseInfo.iCurrentItem != -1
			
			#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
			AND	sData.sBrowseInfo.iCurrentItem < COUNT_OF(sData.sMakeup)
			#ENDIF
			
				SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_MAKEUP, sData.sBrowseInfo.iCurrentItem-1, sData.iTint1, 0, RT_NONE, 1.0)
				//Hide or unhide eyebrows because some makeup contains eyebrows for some reason 
				IF sData.sBrowseInfo.iCurrentItem-1 = -1 
				OR sData.sBrowseInfo.iCurrentItem-1 > 15
					SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_EYEBROW, GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_ACTIVE_CHARACTER_SLOT()), sData.iTint1, 0, RT_NONE, 1.0)
				ELSE
					SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_EYEBROW, GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_ACTIVE_CHARACTER_SLOT()), sData.iTint1, 0, RT_NONE, 0.0)
				ENDIF
				
				IF sData.bRebuildMenu
					BUILD_HAIRDO_SHOP_MENU(sData, pedID_ShopPed)
					sData.bRebuildMenu = FALSE
				ELSE
					SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
				ENDIF
				
				// Telemtry
				
				#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
				STORE_CURRENT_SHOP_ITEM_DATA(sData.sMakeup[sData.sBrowseInfo.iCurrentItem].sLabel, GET_MAKEUP_ITEM_COST(sData, sData.sMakeup[sData.sBrowseInfo.iCurrentItem]))
				#ENDIF
				
			ENDIF
		
		// Make selection
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR bMouseSelect = TRUE
			
			bMouseSelect = FALSE
			bZoomToggle = FALSE
			
			PLAY_SOUND_FRONTEND(-1,"SELECT","HUD_FRONTEND_DEFAULT_SOUNDSET")
			// Already have
			IF sData.sBrowseInfo.iCurrentItem = sData.iPlayerMakeup
				SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_CUR_3", 4000)
				sData.iOriginalMakeup = sData.sBrowseInfo.iCurrentItem
				
			ELIF NOT IS_MAKEUP_UNLOCKED_FOR_SHOP(sData.sBrowseInfo.iCurrentItem)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_LOCK", 4000)
				
			#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
			// Can't afford
			ELIF NOT CAN_PLAYER_AFFORD_ITEM_COST(sData.sMakeup[sData.sBrowseInfo.iCurrentItem].iCost)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_AFF_3", 4000)
				STORE_LAST_VIEWED_SHOP_ITEM_FOR_COMMERCE_STORE(GET_HASH_KEY(g_sShopSettings.tlCurrentItem_label), g_sShopSettings.iCurrentItem_cost, GET_HASH_KEY(GET_SHOP_NAME(sData.sShopInfo.eShop)))
				LAUNCH_STORE_CASH_ALERT(FALSE, STAT_SAVETYPE_END_SHOPPING, SPL_STORE)
			#ENDIF
				
			// Failed to add to basket
			ELIF NETWORK_IS_GAME_IN_PROGRESS()
			AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
			
			#IF NOT USE_NEW_HAIRDOS_SHOP_MENUS
			AND NOT NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, GET_CATALOGUE_INV_CATEGORY_FOR_HAIRDO_MENU(sData.eCurrentMenu, INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1)), GET_HAIRDO_KEY_FOR_CATALOGUE(sData.eCurrentMenu, sData.sCurrentItemLabel), NET_SHOP_ACTION_BUY_ITEM, 1, sData.sMakeup[sData.sBrowseInfo.iCurrentItem].iCost, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, GET_CATALOGUE_INVENTORY_FOR_HAIRDO_MENU(sData.eCurrentMenu, INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1)))
			#ENDIF
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				CPRINTLN(DEBUG_SHOPS, "Can't add item to basket. Menu = ", sData.eCurrentMenu, ", Label = ", sData.sCurrentItemLabel)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_FAIL", 4000)
			
			ELIF NETWORK_IS_GAME_IN_PROGRESS()
			AND DOES_SAVE_HAVE_COUPON(COUPON_HAIRCUT)
			AND NOT NETWORK_REQUEST_BASKET_TRANSACTION(NET_SHOP_TTYPE_BASKET, GET_CATALOGUE_INV_CATEGORY_FOR_HAIRDO_MENU(sData.eCurrentMenu, INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1)), GET_CATALOGUE_INVENTORY_FOR_HAIRDO_MENU(sData.eCurrentMenu, INT_TO_ENUM(HEAD_OVERLAY_SLOT, -1)), NET_SHOP_ACTION_SPEND, 1, 0, 1, CATALOG_ITEM_FLAG_BANK_THEN_WALLET, HASH("PO_COUPON_HAIRCUT"))
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				CPRINTLN(DEBUG_SHOPS, "Can't add coupon discount to basket. Menu = ", sData.eCurrentMenu, ", Label = ", sData.sCurrentItemLabel)
				SET_CURRENT_MENU_ITEM_DESCRIPTION("CSHOP_FAIL", 4000)
				
			// Start cut process
			ELSE
			
				IF NETWORK_IS_GAME_IN_PROGRESS()
				AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
					sData.sBrowseInfo.bProcessingBasket = TRUE
					sData.sBrowseInfo.iProcessingBasketStage = 0
				ENDIF
				
				// Update player's make-up			
				SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_MAKEUP, sData.iPlayerMakeup-1, sData.iTint1, 0, RT_NONE, 1.0)
				//Hide or unhide eyebrows because some makeup contains eyebrows for some reason 
				IF sData.iPlayerMakeup-1 = -1 
				OR sData.iPlayerMakeup-1 > 15
					SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_EYEBROW, GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_ACTIVE_CHARACTER_SLOT()), sData.iTint1, 0, RT_NONE, 1.0)
				ELSE
					SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_EYEBROW, GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_ACTIVE_CHARACTER_SLOT()), sData.iTint1, 0, RT_NONE, 0.0)
				ENDIF
				
				sData.sBrowseInfo.bItemSelected = TRUE
				sData.iOriginalMakeup = sData.sBrowseInfo.iCurrentItem
			ENDIF
			
		// Back
		ELIF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL) OR IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		OR IS_MENU_CURSOR_CANCEL_PRESSED()
			PLAY_SOUND_FRONTEND(-1,"CANCEL","HUD_FRONTEND_DEFAULT_SOUNDSET")
			// Reset makeup
			//SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_MAKEUP, sData.iPlayerMakeup-1, sData.iTint1, 0, RT_NONE, 1.0)
			SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_MAKEUP, sData.iOriginalMakeup-1, sData.iTint1, 0, RT_NONE, 1.0)
			//Hide or unhide eyebrows because some makeup contains eyebrows for some reason 
			IF sData.iOriginalMakeup-1 = -1 
			OR sData.iOriginalMakeup-1 > 15
				SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_EYEBROW, GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_ACTIVE_CHARACTER_SLOT()), sData.iTint1, 0, RT_NONE, 1.0)
			ELSE
				SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_EYEBROW, GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_ACTIVE_CHARACTER_SLOT()), sData.iTint1, 0, RT_NONE, 0.0)
			ENDIF
			
			sData.sBrowseInfo.bItemSelected = FALSE
			sData.sBrowseInfo.bItemPurchased = FALSE
			sData.sBrowseInfo.bPurchasingItem = FALSE
			
			// Need to change the selected menu item if we have beard options or not.
			IF DOES_PLAYER_HAVE_BEARD_OPTIONS()
				sData.sBrowseInfo.iCurrentItem = 2
			ELSE
				sData.sBrowseInfo.iCurrentItem = 1
			ENDIF
			sData.eCurrentMenu = HME_MAIN_MENU
			sData.bRebuildMenu = TRUE
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL PROCESS_HAIRDO_SHOPPING_BASKET(HAIRDO_SHOP_STRUCT &sData)
	IF sData.sBrowseInfo.bProcessingBasket
		CONST_INT iSTAGE_0	0
		CONST_INT iSTAGE_1	1
		CONST_INT iSTAGE_2	2
		CONST_INT iSTAGE_70	70
		CONST_INT iSTAGE_80	80
		CONST_INT iSTAGE_90	90
		
		PRINTLN("[HAIRDO_BASKET] - Processing basket, stage ", sData.sBrowseInfo.iProcessingBasketStage, "...")
		SWITCH sData.sBrowseInfo.iProcessingBasketStage
			// 1st attempt
			CASE iSTAGE_0
				IF NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
					sData.sBrowseInfo.iProcessingBasketStage = iSTAGE_70 // Pending
				ELSE
					PRINTLN("[HAIRDO_BASKET] - Basket transaction failed to start checkout! 1")
					sData.sBrowseInfo.tdBasketRetryTimer = GET_NETWORK_TIME()
					sData.sBrowseInfo.iProcessingBasketStage = iSTAGE_1
				ENDIF
			BREAK
			// 2nd attempt
			CASE iSTAGE_1
				IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sData.sBrowseInfo.tdBasketRetryTimer)) > 1000
					IF NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
						sData.sBrowseInfo.iProcessingBasketStage = iSTAGE_70 // Pending
					ELSE
						PRINTLN("[HAIRDO_BASKET] - Basket transaction failed to start checkout! 2")
						sData.sBrowseInfo.tdBasketRetryTimer = GET_NETWORK_TIME()
						sData.sBrowseInfo.iProcessingBasketStage = iSTAGE_2
					ENDIF
				ENDIF
			BREAK
			// 3rd attempt
			CASE iSTAGE_2
				IF ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sData.sBrowseInfo.tdBasketRetryTimer)) > 1000
					IF NETWORK_START_BASKET_TRANSACTION_CHECKOUT()
						sData.sBrowseInfo.iProcessingBasketStage = iSTAGE_70 // Pending
					ELSE
						PRINTLN("[HAIRDO_BASKET] - Basket transaction failed to start checkout! 3")
						sData.sBrowseInfo.iProcessingBasketStage = iSTAGE_90 // Failed
					ENDIF
				ENDIF
			BREAK
			
			// Pending
			CASE iSTAGE_70
				IF IS_CASH_TRANSACTION_COMPLETE(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
					IF GET_CASH_TRANSACTION_STATUS(GET_BASKET_TRANSACTION_SCRIPT_INDEX()) = CASH_TRANSACTION_STATUS_SUCCESS
						PRINTLN("[HAIRDO_BASKET] - Basket transaction finished, success!")
						sData.sBrowseInfo.iProcessingBasketStage = iSTAGE_80 // Success
					ELSE
						PRINTLN("[HAIRDO_BASKET] - Basket transaction finished, failed!")
						sData.sBrowseInfo.iProcessingBasketStage = iSTAGE_90 // Failed
					ENDIF
				ENDIF
			BREAK
			
			// Success
			CASE iSTAGE_80
				sData.sBrowseInfo.bProcessingBasket = FALSE
				sData.sBrowseInfo.iProcessingBasketStage = iSTAGE_0
			BREAK
			
			//Failed
			CASE iSTAGE_90
				DELETE_CASH_TRANSACTION(GET_BASKET_TRANSACTION_SCRIPT_INDEX())
				
				// [TODO] Notify player
				sData.sBrowseInfo.bItemSelected = FALSE
				
				sData.sBrowseInfo.bProcessingBasket = FALSE
				sData.sBrowseInfo.iProcessingBasketStage = iSTAGE_0
			BREAK
		ENDSWITCH
		
		// Always return true when we are processing
		RETURN TRUE
	ENDIF
	RETURN (sData.sBrowseInfo.bProcessingBasket)
ENDFUNC

/// PURPOSE: Sets the player into postion and displays the shop hud
PROC DO_BROWSE_ITEMS(HAIRDO_SHOP_STRUCT &sData)
	
	DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(TRUE)
	DO_WE_HAVE_CONTROL_OF_SCISSORS(TRUE)
	
	IF SHOULD_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
		IF IS_NET_PLAYER_OK(PLAYER_ID(), FALSE, TRUE)
			CLEAR_TRANSITION_SESSION_RESTORE_SHOP_STATE_ON_RESPAWN()
			SET_TRANSITION_SESSION_SHOP_STATE_INITIALISED()
		ELIF NOT HAS_TRANSITION_SESSION_SHOP_STATE_INITIALISED()
			SET_TRANSITION_SESSION_SHOP_STATE_INITIALISED()
		ENDIF
		PRINTLN("DO_BROWSE_ITEMS - waiting for player to enter game...")
		EXIT
	ENDIF
	
	IF SHOULD_BACK_OUT_OF_SHOP_MENU()
		RESET_BACK_OUT_OF_SHOP_MENU_GLOBAL()
		GO_BACK_TO_PREVIOUS_NGMP_MENU(sData)
	ENDIF
	
	// Check to see if it is no longer safe to browse
	UPDATE_SHOP_BROWSE_BAIL_STATE(sData.sShopInfo, sData.sBrowseInfo)
	
	IF sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT AND NOT IS_SHOP_KEEPER_SAFE_TO_USE_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_ID(), sData.eInShop, FALSE)
		sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_BAIL
		PRINTLN("DO_BROWSE_ITEMS - IS_SHOP_KEEPER_SAFE_TO_USE_FOR_PARTICIPANT_OF_THIS_SCRIPT false bail")
	ENDIF
	
	g_bAllowHairdoBail = TRUE
	
	HIDE_HUD_ITEMS_THIS_FRAME_FOR_SHOP_CUTSCENE(FALSE)
	DISABLE_CELLPHONE_THIS_FRAME_ONLY()
	
	// Gets the cursor position and also works out distances moved this frame.
	
	SEQUENCE_INDEX seqCheckoutHairAndIdle
	
	FLOAT fPhaseStartSound
	FLOAT fPhaseStopSound
	FLOAT fHighlightScaleformOffset = 0.0
	
	IF NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
		IF IS_ENTITY_PLAYING_ANIM(sData.sShopInfo.sShopKeeper.pedID,sData.sHairdoShopAnimDict,"keeper_idle_a")
			fPhaseStartSound = 0.29
			fPhaseStopSound = 0.474
		ELIF IS_ENTITY_PLAYING_ANIM(sData.sShopInfo.sShopKeeper.pedID,sData.sHairdoShopAnimDict,"keeper_idle_b")
			fPhaseStartSound = 0.33
			fPhaseStopSound = 0.59
		ELSE
			fPhaseStartSound = 0.3
			fPhaseStopSound = 0.5
		ENDIF
	ENDIF

	SWITCH sData.sBrowseInfo.eStage
	
		// -------------Init-----------------------------------------
		CASE SHOP_BROWSE_STAGE_INIT
		
			sData.sBuddyHideData.buddyHide = FALSE
			sData.sBuddyHideData.buddyInit = FALSE
			
			IF sData.sShopInfo.eShop != HAIRDO_SHOP_CASINO_APT
				REQUEST_USE_OF_SHOP_KEEPER(PLAYER_ID(), sData.eInShop)
			ELSE	
				REQUEST_USE_OF_SHOP_KEEPER_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_ID(), sData.eInShop)
			ENDIF	
			IF IS_SHOP_PED_OK()
				IF LOAD_SHOP_BROWSE_ASSETS(sData, TRUE)
				AND LOAD_HAIRDO_SHOP_ANIMS(sData)
				AND LOAD_MENU_ASSETS("HAR_MNU", ENUM_TO_INT(sData.sShopInfo.eShop), TRUE)
				AND LOAD_NGMP_MENU_ASSETS(sData)
				AND LOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
				AND LOAD_HAIRDO_CUTSCENE_ASSETS(sData, TRUE)
				AND ((IS_SHOP_KEEPER_SAFE_TO_USE(PLAYER_ID(), sData.eInShop, TRUE) AND sData.sShopInfo.eShop != HAIRDO_SHOP_CASINO_APT) OR ((IS_SHOP_KEEPER_SAFE_TO_USE_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_ID(), sData.eInShop, TRUE) AND sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT)))
				AND DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(TRUE)
				AND DO_WE_HAVE_CONTROL_OF_SCISSORS(TRUE)
				
					SET_PLAYER_IS_CHANGING_CLOTHES(TRUE)

					IF NOT g_bInMultiplayer
						PED_INDEX pedID_ShopPed
						pedID_ShopPed = PLAYER_PED_ID()
						IF DOES_ENTITY_EXIST(sData.sCutsceneData.pedID)
						AND NOT IS_PED_INJURED(sData.sCutsceneData.pedID)
							pedID_ShopPed = sData.sCutsceneData.pedID
						ENDIF				
						pedID_ShopPed = pedID_ShopPed				
						//SET_PED_CONFIG_FLAG(pedID_ShopPed, PCF_ForceSkinCharacterCloth, TRUE)
					ENDIF
					
					IF NETWORK_IS_GAME_IN_PROGRESS()
					AND NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
						SET_ENTITY_INVINCIBLE(sData.sShopInfo.sShopKeeper.pedID,TRUE)
						SET_PED_CAN_RAGDOLL(sData.sShopInfo.sShopKeeper.pedID,FALSE)
						SET_PED_RELATIONSHIP_GROUP_HASH(sData.sShopInfo.sShopKeeper.pedID, rgFM_AiLikePlyrLikeCops)
					ENDIF
					
					// Remove player control
					LOCK_PLAYER_FOR_BROWSE(sData.sBrowseInfo)
					CLEAR_PED_TASKS(PLAYER_PED_ID())
					
					// Reset the browse info
					sData.bSelectedItemIsLocked = FALSE
					sData.bSelectedItemIsNew = FALSE
					sData.bSelectedItemIsDLC = FALSE
					sData.bUnlockMessageDisplayed = FALSE
					sData.bMenuInitialised = FALSE
					sData.bRebuildMenu = TRUE
					sData.bBlockAutoItemSelect = FALSE
					sData.iTopMenuItem = 0
					sData.iCurrentHairdo = -1
					sData.iCurrentBeard = -1
					sData.iPlayerMakeup = -1
					sData.fCurrentBeardFade = 1.0
					sData.sBrowseInfo.bBrowsing = TRUE
					sData.sBrowseInfo.bExitTrigger = FALSE
					sData.sBrowseInfo.bItemSelected = FALSE
					sData.sBrowseInfo.bItemPurchased = FALSE
					sData.sBrowseInfo.bPurchasingItem = FALSE
					sData.sBrowseInfo.sInfoLabel = ""
					sData.sBrowseInfo.iControl = 0
					sData.iTopItem = 0
					sData.eCurrentMenu = HME_MAIN_MENU
					sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_INTRO
					
					sData.bPlayingSound = FALSE
					
				ELIF (NETWORK_IS_GAME_IN_PROGRESS() AND ABSI(GET_TIME_DIFFERENCE(GET_NETWORK_TIME(), sData.tdEntryTimer)) > 8000)
				OR (NOT NETWORK_IS_GAME_IN_PROGRESS() AND GET_GAME_TIMER() - sData.iEntryTimer > 8000)
					sData.sBrowseInfo.eStage = SHOP_BROWSE_STAGE_BAIL
				ELSE
					PRINTLN("DO_BROWSE_ITEMS - Waiting for assets to load")
					PRINTLN("...LOAD_SHOP_BROWSE_ASSETS = ", LOAD_SHOP_BROWSE_ASSETS(sData, TRUE))
					PRINTLN("...LOAD_HAIRDO_SHOP_ANIMS = ", LOAD_HAIRDO_SHOP_ANIMS(sData))
					PRINTLN("...LOAD_MENU_ASSETS = ", LOAD_MENU_ASSETS("HAR_MNU", ENUM_TO_INT(sData.sShopInfo.eShop), TRUE))
					PRINTLN("...LOAD_NGMP_MENU_ASSETS = ", LOAD_NGMP_MENU_ASSETS(sData))
					PRINTLN("...LOAD_SHOP_INTRO_ASSETS = ", LOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop))
					PRINTLN("...LOAD_HAIRDO_CUTSCENE_ASSETS = ", LOAD_HAIRDO_CUTSCENE_ASSETS(sData, TRUE))
					PRINTLN("...IS_SHOP_KEEPER_SAFE_TO_USE = ", IS_SHOP_KEEPER_SAFE_TO_USE(PLAYER_ID(), sData.eInShop, TRUE))
					PRINTLN("...DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER = ", DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(TRUE))
					PRINTLN("...DO_WE_HAVE_CONTROL_OF_SCISSORS = ", DO_WE_HAVE_CONTROL_OF_SCISSORS(TRUE))
				ENDIF
			ENDIF
		BREAK
		
		// -------browse intro----------------------------------------
		CASE SHOP_BROWSE_STAGE_INTRO
			RUN_HAIRDO_INTRO(sData)
		BREAK
		
		// -------Browse Outro -----------------------------------   
		CASE SHOP_BROWSE_STAGE_OUTRO
			RUN_HAIRDO_OUTRO(sData)
		BREAK
		
		CASE SHOP_BROWSE_STAGE_RESTORE_PED	
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()		
				IF HAS_PED_PRELOAD_PROP_DATA_FINISHED(PLAYER_PED_ID())	
				AND HAS_PED_PRELOAD_VARIATION_DATA_FINISHED(PLAYER_PED_ID())	
					RESTORE_COMPS(sData)
				ENDIF
			ELSE
				RESTORE_COMPS(sData)
			ENDIF
		BREAK
		
		// --------browsing -----------------------------------------
		CASE SHOP_BROWSE_STAGE_BROWSING
		
			PED_INDEX pedID_ShopPed
			VECTOR vAnimPos, vAnimRot
			
			RESET_MOUSE_POINTER_FOR_PAUSE_OR_ALERT() // Resets the mouse pointer if a pause screen or alert screen occurs.
						
			// PC uses a different frontend input for zooming
			IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
				caZoomInput = INPUT_FRONTEND_LS
			ELSE
				caZoomInput = INPUT_FRONTEND_LT
			ENDIF
			
			// If the cutscene ped exists, set the items on them, otherwise set it on the player
			pedID_ShopPed = PLAYER_PED_ID()
			IF DOES_ENTITY_EXIST(sData.sCutsceneData.pedID)
			AND NOT IS_PED_INJURED(sData.sCutsceneData.pedID)
				pedID_ShopPed = sData.sCutsceneData.pedID
			ENDIF
			
			IF NOT NETWORK_IS_GAME_IN_PROGRESS()
				SET_PED_CONFIG_FLAG(pedID_ShopPed, PCF_ForceSkinCharacterCloth, TRUE)
			ENDIF
			
			IF IS_SHOP_PED_OK()
			AND NOT IS_PED_INJURED(pedID_ShopPed)
			AND NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
				IF NETWORK_IS_GAME_IN_PROGRESS()
					IF NETWORK_PLAYER_HAS_HEADSET(PLAYER_ID())
				    	IF NETWORK_IS_PLAYER_TALKING(PLAYER_ID())
				    		SET_PED_RESET_FLAG(pedID_ShopPed, PRF_EnableVoiceDrivenMouthMovement, TRUE)
				   		ENDIF
					ENDIF
				ENDIF
				
				// Get hair cut
				IF NOT sData.sBrowseInfo.bPurchasingItem
				AND sData.sBrowseInfo.bItemSelected
					g_bAllowHairdoBail = FALSE
					
					// Process basket before we go ahead with the cut.
					IF NETWORK_IS_GAME_IN_PROGRESS()
					AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
					AND PROCESS_HAIRDO_SHOPPING_BASKET(sData)
						// Processing...
						
					// Trigger the next cutscene if the shopkeeper isn't already playing a haircut anim
					ELIF NOT IS_HAIRCUT_ANIM(sData, sData.sCurrentKeeperAnim)
						// player
						
						GET_HAIRDO_SHOP_ANIM_POSITION(sData.sShopInfo.eShop, vAnimPos, vAnimRot)
						IF IS_SALON_ACTIVE(sData)
							// TODO! replace with base anim when it is less camp
							sData.sCurrentCustomerAnim = GET_RANDOM_IN_CHAIR_IDLE_ANIM(sData)
						ELSE
							sData.sCurrentCustomerAnim = "player_base"
						ENDIF
						TASK_PLAY_ANIM_ADVANCED(pedID_ShopPed, sData.sHairdoShopAnimDict, sData.sCurrentCustomerAnim, vAnimPos, vAnimRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,  AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK )  //AF_HOLD_LAST_FRAME
						FORCE_PED_AI_AND_ANIMATION_UPDATE(pedID_ShopPed)
						
						// shopkeeper
						CPRINTLN(DEBUG_SHOPS, "Picked haircut anim")
						sData.sCurrentKeeperAnim = GET_RANDOM_HAIRCUT_ANIM(sData)
						
						IF NETWORK_IS_GAME_IN_PROGRESS()
							TASK_PLAY_ANIM_ADVANCED(sData.sShopInfo.sShopKeeper.pedID, sData.sHairdoShopAnimDict, sData.sCurrentKeeperAnim, vAnimPos, vAnimRot, INSTANT_BLEND_IN, INSTANT_BLEND_OUT, -1, AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK )
							FORCE_PED_AI_AND_ANIMATION_UPDATE(sData.sShopInfo.sShopKeeper.pedID)
							PLAY_ENTITY_ANIM(NET_TO_ENT(serverBD.scissorsID), GET_SCISSOR_ANIM_FOR_KEEPER_ANIM(sData.sCurrentKeeperAnim), sData.sHairdoShopAnimDict, INSTANT_BLEND_IN, FALSE, TRUE, TRUE, 0.0, 0)
							FORCE_ENTITY_AI_AND_ANIMATION_UPDATE(NET_TO_ENT(serverBD.scissorsID))
							STOP_NETWORK_SYNC_SCENE_FOR_SHOP(sData)
							SETUP_NETWORK_SYNC_SCENE_FOR_SHOP(sData, TRUE, FALSE)
							ADD_PED_TO_SHOP_SYNC_SCENE(sData, sData.sShopInfo.sShopKeeper.pedID, sData.sCurrentKeeperAnim,SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT, SLOW_BLEND_IN)
							ADD_PED_TO_SHOP_SYNC_SCENE(sData, PLAYER_PED_ID(), sData.sCurrentCustomerAnim,SYNCED_SCENE_LOOP_WITHIN_SCENE, SLOW_BLEND_IN)
							ADD_ENTITY_TO_SHOP_SYNC_SCENE(sData, NET_TO_ENT(serverBD.scissorsID), GET_SCISSOR_ANIM_FOR_KEEPER_ANIM(sData.sCurrentKeeperAnim),SYNCED_SCENE_LOOP_WITHIN_SCENE)
							START_NETWORK_SYNC_SCENE_FOR_SHOP(sData)
							IF DO_WE_HAVE_CONTROL_OF_SCISSORS(TRUE)
								IF sData.eCurrentMenu = HME_MAKEUP 
								
								OR sData.eCurrentMenu = HME_NGMP_EYEBROWS
								OR sData.eCurrentMenu = HME_NGMP_CONTACTS
								OR sData.eCurrentMenu = HME_NGMP_FACEPAINT
								OR sData.eCurrentMenu = HME_NGMP_MAKEUP_BLUSHER
								OR sData.eCurrentMenu = HME_NGMP_MAKEUP_EYE
								OR sData.eCurrentMenu = HME_NGMP_MAKEUP_LIPSTICK
									SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.scissorsID), FALSE)
								ELSE
									SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.scissorsID), TRUE)
								ENDIF
							ENDIF
						ELSE
							// shopkeeper anim in chair
							UPDATE_SHOPKEEPER_ANIM(sData, sData.sCurrentKeeperAnim, FALSE, FALSE, TRUE)
						ENDIF

						// camera
						UPDATE_CAMERA_ANIM(sData, GET_CAMERA_ANIM(sData, HSC_CUT, sData.sCurrentKeeperAnim))
				
						// Get rid of the browse cam, we will reinit when this set is done.
						IF DOES_CAM_EXIST(sData.sShopInfo.sCameras[1].camID)
							SET_CAM_ACTIVE(sData.sShopInfo.sCameras[1].camID, FALSE)
							DESTROY_CAM(sData.sShopInfo.sCameras[1].camID)
						ENDIF
					ELSE
						SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
						SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
						SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
						SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
						
						// Wait for the set to finish
						IF GET_SYNC_SCENE_PHASE(sData.iSyncSceneShopkeeper, sData.iNETSyncSceneShopkeeper) = 1.0
						
							// Done...
							sData.sBrowseInfo.bItemSelected = FALSE
							sData.sBrowseInfo.bItemPurchased = FALSE
							sData.sBrowseInfo.bPurchasingItem = FALSE
							sData.sCurrentKeeperAnim = ""
							sData.bRebuildMenu = TRUE
							sData.bDoFinaliseHeadBlend = TRUE
							
							// Back to main menu
							IF DOES_PLAYER_HAVE_BEARD_OPTIONS() OR DOES_PLAYER_HAVE_MAKEUP_OPTIONS()
							
								 // Hairstyles
								IF sData.eCurrentMenu = HME_HAIR 
									sData.eCurrentMenu = HME_MAIN_MENU 
									sData.sBrowseInfo.iCurrentItem = 0
								
								// Beards
								ELIF sData.eCurrentMenu = HME_BEARD 
									sData.eCurrentMenu = HME_MAIN_MENU
									sData.sBrowseInfo.iCurrentItem = 1
								
								// Makeup
								ELIF sData.eCurrentMenu = HME_MAKEUP
									
									// Need to change which menu item is selected depending on if we have beards as well as makeup.
									sData.eCurrentMenu = HME_MAKEUP
									IF DOES_PLAYER_HAVE_BEARD_OPTIONS()
										sData.sBrowseInfo.iCurrentItem = 2
									ELSE
										sData.sBrowseInfo.iCurrentItem = 1
									ENDIF
									
								// NG MP Menus
								ELIF IS_NGMP_MENU(sData.eCurrentMenu)
									//GO_BACK_TO_PREVIOUS_NGMP_MENU(sData)
									
								ENDIF
							ENDIF
							BUILD_HAIRDO_SHOP_MENU(sData, pedID_ShopPed)
							
							// Back to the first idle...
							IF NETWORK_IS_GAME_IN_PROGRESS()
								STOP_NETWORK_SYNC_SCENE_FOR_SHOP(sData)
								SETUP_NETWORK_SYNC_SCENE_FOR_SHOP(sData, FALSE, TRUE)
								ADD_PED_TO_SHOP_SYNC_SCENE(sData, sData.sShopInfo.sShopKeeper.pedID, "keeper_base",SYNCED_SCENE_LOOP_WITHIN_SCENE | SYNCED_SCENE_USE_PHYSICS | SYNCED_SCENE_DONT_INTERRUPT)
								//ADD_PED_TO_SHOP_SYNC_SCENE(sData, PLAYER_PED_ID(), sData.sCurrentCustomerAnim,SYNCED_SCENE_LOOP_WITHIN_SCENE)  //Adding player messes up the sync scene
								ADD_ENTITY_TO_SHOP_SYNC_SCENE(sData, NET_TO_ENT(serverBD.scissorsID), GET_SCISSOR_ANIM_FOR_KEEPER_ANIM("keeper_base"),SYNCED_SCENE_LOOP_WITHIN_SCENE)
								START_NETWORK_SYNC_SCENE_FOR_SHOP(sData)
							ELSE
								// shopkeeper anim in chair
								UPDATE_SHOPKEEPER_ANIM(sData, "keeper_base", FALSE, TRUE)
							ENDIF
							
							UPDATE_HAIRDO_SHOP_BROWSE_CAM(sData, FALSE)
							
						// Make purchase
						ELIF GET_SYNC_SCENE_PHASE(sData.iSyncSceneShopkeeper, sData.iNETSyncSceneShopkeeper) > 0.6
							IF NOT sData.sBrowseInfo.bItemPurchased
								PURCHASE_HAIRDO_ITEM(sData)
							ENDIF
		
						// Stop the particle effect when the barber stops cutting
						ELIF GET_SYNC_SCENE_PHASE(sData.iSyncSceneShopkeeper, sData.iNETSyncSceneShopkeeper) > fPhaseStopSound
							IF sData.ptfxHair <> NULL
								STOP_PARTICLE_FX_LOOPED(sData.ptfxHair)
								sData.ptfxHair = NULL
							ENDIF
							IF sData.bPlayingSound
								// pick an idle for the player
								sData.sCurrentCustomerAnim = GET_RANDOM_IN_CHAIR_IDLE_ANIM(sData)
								GET_HAIRDO_SHOP_ANIM_POSITION(sData.sShopInfo.eShop, vAnimPos, vAnimRot)
								IF NETWORK_IS_GAME_IN_PROGRESS()	
									sData.iNETSyncScenePlayer = NETWORK_CREATE_SYNCHRONISED_SCENE(vAnimPos, vAnimRot, EULER_YXZ, FALSE, TRUE)	
									NETWORK_ADD_PED_TO_SYNCHRONISED_SCENE(PLAYER_PED_ID(), sData.iNETSyncScenePlayer, sData.sHairdoShopAnimDict, sData.sCurrentCustomerAnim, NORMAL_BLEND_IN, NORMAL_BLEND_OUT)
									NETWORK_START_SYNCHRONISED_SCENE(sData.iNETSyncScenePlayer)
								ELSE
									OPEN_SEQUENCE_TASK(seqCheckoutHairAndIdle)	
										TASK_PLAY_ANIM_ADVANCED(NULL, sData.sHairdoShopAnimDict, sData.sCurrentCustomerAnim, vAnimPos, vAnimRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,  AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK )
										TASK_PLAY_ANIM_ADVANCED(NULL, sData.sHairdoShopAnimDict, "player_base", vAnimPos, vAnimRot, NORMAL_BLEND_IN, NORMAL_BLEND_OUT, -1,  AF_OVERRIDE_PHYSICS | AF_TURN_OFF_COLLISION | AF_EXTRACT_INITIAL_OFFSET | AF_NOT_INTERRUPTABLE | AF_LOOPING, 0.0, EULER_YXZ, AIK_DISABLE_LEG_IK )
									CLOSE_SEQUENCE_TASK(seqCheckoutHairAndIdle)
									TASK_PERFORM_SEQUENCE(pedID_ShopPed,seqCheckoutHairAndIdle)
									CLEAR_SEQUENCE_TASK(seqCheckoutHairAndIdle)
								ENDIF
								STOP_SOUND(sData.iHairCutSoundID)
								STOP_SOUND(sData.iClippersSoundID)
								STOP_SOUND(sData.iMakeupSoundID)
								sData.bPlayingSound = FALSE
							ENDIF
							
						// Switch out the hairdos when the head is blocked
						ELIF GET_SYNC_SCENE_PHASE(sData.iSyncSceneShopkeeper, sData.iNETSyncSceneShopkeeper) > 0.4					
							IF NOT sData.sBrowseInfo.bItemPurchased
								PURCHASE_HAIRDO_ITEM(sData)
							ENDIF
							// Hairstyles
							IF sData.eCurrentMenu = HME_HAIR 
								// Make sure we now have the hair...
								IF NOT CALL sData.fpIsPedCompItemCurrent(PLAYER_PED_ID(), g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem])
									
									CPRINTLN(DEBUG_SHOPS, "DO_BROWSE_ITEMS. Make sure we now have the hair....: sData.sBrowseInfo.iCurrentItem = ", sData.sBrowseInfo.iCurrentItem)
									CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem], FALSE)
									
									IF g_bInMultiplayer
										SET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT, ENUM_TO_INT(g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]))
										SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_HAIRCUT)
										SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO,ENUM_TO_INT(g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem])) 
										SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA,ENUM_TO_INT(g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]))
									ENDIF
								ENDIF
								// Update cutscene ped too...
								IF PLAYER_PED_ID() != pedID_ShopPed
									CPRINTLN(DEBUG_SHOPS, "DO_BROWSE_ITEMS. Update cutscene ped too...: sData.sBrowseInfo.iCurrentItem = ", sData.sBrowseInfo.iCurrentItem)
									CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem], FALSE)
								ENDIF
						
							// Beards
							ELIF sData.eCurrentMenu = HME_BEARD
								
								IF NETWORK_IS_GAME_IN_PROGRESS()
									// Make sure we now have the beard...
									SAFE_SET_PED_HEAD_OVERLAY(PLAYER_PED_ID(), HOS_FACIAL_HAIR, GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)-1, sData.iTint1, 0, RT_NONE, sData.fCurrentBeardFade)
								
									// Update cutscene ped too...
									IF PLAYER_PED_ID() != pedID_ShopPed
										SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_FACIAL_HAIR, GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)-1, sData.iTint1, 0, RT_NONE, sData.fCurrentBeardFade)
									ENDIF
								
									SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_HAIRCUT)
									SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_BEARD, GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)-1)
									SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_BEARD_PC, 1.0)
									sData.iCurrentBeard = GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem)
									sData.fCurrentBeardFade = 1.0
									CPRINTLN(DEBUG_SHOPS, "DO_BROWSE_ITEMS. Make sure we now have the beard....: sData.iCurrentBeard = ", sData.iCurrentBeard)
								ELSE
									// SP beards
									// Make sure we now have the beard...
									IF NOT CALL sData.fpIsPedCompItemCurrent(PLAYER_PED_ID(), g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem])
										CPRINTLN(DEBUG_SHOPS, "DO_BROWSE_ITEMS. Make sure we now have the beard: iCurrentItem = ", sData.sBrowseInfo.iCurrentItem)
										CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem], FALSE)
										sData.iCurrentBeard = ENUM_TO_INT(g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]) 
									ENDIF
									// Update cutscene ped too...
									IF PLAYER_PED_ID() != pedID_ShopPed
										CPRINTLN(DEBUG_SHOPS, "DO_BROWSE_ITEMS. Update cutscene ped too (Beard): iCurrentItem = ", sData.sBrowseInfo.iCurrentItem)
										CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, g_sShopCompItemData.eTypes[sData.sBrowseInfo.iCurrentItem], g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem], FALSE)
									ENDIF
								ENDIF

							// Makeup
							ELIF sData.eCurrentMenu = HME_MAKEUP
							
								// We'll need to properly support makeup for unlocking purposes like the beards and hairdos at some point, but the support isn't currently there,
								// so this is temporarily implemented for now. LDS SR 19/FEB/13
								IF g_bInMultiplayer
									sData.iPlayerMakeup = sData.sBrowseInfo.iCurrentItem
									CPRINTLN(DEBUG_SHOPS, "Updating make up for MP ped. sData.iPlayerMakeup= ", sData.iPlayerMakeup-1)
									SAFE_SET_PED_HEAD_OVERLAY(PLAYER_PED_ID(), HOS_MAKEUP, sData.iPlayerMakeup-1, sData.iTint1, 0, RT_NONE, 1.0)
									SET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_MAKEUP, sData.iPlayerMakeup-1)
									SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_MAKEUP_PC, 1.0)
																		
									//Hide or unhide eyebrows because some makeup contains eyebrows for some reason 
									IF sData.iPlayerMakeup-1 = -1 
									OR sData.iPlayerMakeup-1 > 15
										SAFE_SET_PED_HEAD_OVERLAY(PLAYER_PED_ID(), HOS_EYEBROW, GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_ACTIVE_CHARACTER_SLOT()), sData.iTint1, 0, RT_NONE, 1.0)
										SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_EYEBRW_PC, 1.0)
										
									ELSE
										SAFE_SET_PED_HEAD_OVERLAY(PLAYER_PED_ID(), HOS_EYEBROW, GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_ACTIVE_CHARACTER_SLOT()), sData.iTint1, 0, RT_NONE, 0.0)
										SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_HEADBLEND_OVERLAY_EYEBRW_PC, 0.0)
										
									ENDIF
									SET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(HOS_MAKEUP), 1.0)
									SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_HAIRCUT)
									
									// Update cutscene ped too...
									IF PLAYER_PED_ID() != pedID_ShopPed
										CPRINTLN(DEBUG_SHOPS, "Updating make up for cutscene ped. sData.iPlayerMakeup= ", sData.iPlayerMakeup)
										SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_MAKEUP, sData.iPlayerMakeup-1, sData.iTint1, 0, RT_NONE, 1.0)
										//Hide or unhide eyebrows because some makeup contains eyebrows for some reason 
										IF sData.iPlayerMakeup-1 = -1 
										OR  sData.iPlayerMakeup-1 > 15
											SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_EYEBROW, GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_ACTIVE_CHARACTER_SLOT()), sData.iTint1, 0, RT_NONE, 1.0)
										ELSE
											SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, HOS_EYEBROW, GET_PACKED_STAT_INT(PACKED_MP_HEADBLEND_OVERLAY_EYEBRW, GET_ACTIVE_CHARACTER_SLOT()), sData.iTint1, 0, RT_NONE, 0.0)
										ENDIF
									ENDIF
								ENDIF
								
							ELIF IS_NGMP_MENU(sData.eCurrentMenu)
							
								NGMP_MENU_OPTION_DATA sOptionData
								IF GET_NGMP_MENU_OPTION_DATA(GET_ENTITY_MODEL(PLAYER_PED_ID()), sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
									// NG MP Ped Comp
									IF sOptionData.iType = 0
										CDEBUG1LN(DEBUG_SHOPS, "buying ped comp.GET_NGMP_MENU_OPTION_DATA(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ", ", sData.sBrowseInfo.iCurrentItem, "): ",
												sOptionData.tlLabel, ", iTexture: ",
												sOptionData.iTexture, ", eHairItem: ",
												sOptionData.eHairItem, ", iType: ",
												sOptionData.iType)
									
										IF NOT IS_NGMP_ITEM_AND_TINTS_CURRENT(GET_ENTITY_MODEL(PLAYER_PED_ID()), sOptionData, sData)
											CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_HAIR, sOptionData.eHairItem, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sData.iTint1, sData.iTint2)
											
											IF g_bInMultiplayer
												SET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT, ENUM_TO_INT(sOptionData.eHairItem))
												SET_BIT_FOR_FRANKIE_SAYS_ACHIEVEMENT(ACH45_HAIRCUT)
												SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO,ENUM_TO_INT(sOptionData.eHairItem)) 
												SET_MP_INT_CHARACTER_STAT(MP_STAT_CHAR_FM_STORED_HAIRDO_SA,ENUM_TO_INT(sOptionData.eHairItem)) 
												
												SET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT, sData.iTint1)
												SET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT, sData.iTint2)
												
												g_sTempCompData[1] = GET_PED_COMP_DATA_FOR_ITEM_MP(GET_ENTITY_MODEL(PLAYER_PED_ID()), COMP_TYPE_HAIR, sOptionData.eHairItem)
												SET_PACKED_STAT_INT(PACKED_MP_CLTH_TEXTVAR_HAIR, g_sTempCompData[1].iTexture)
												SET_PACKED_STAT_INT(PACKED_MP_CLTH_DRAWVAR_HAIR, g_sTempCompData[1].iDrawable)
											ENDIF
										ENDIF
										// Update cutscene ped too...
										IF PLAYER_PED_ID() != pedID_ShopPed
											CALL sData.fpSetPedCompItemCurrent(pedID_ShopPed, COMP_TYPE_HAIR, sOptionData.eHairItem, FALSE, DEFAULT, DEFAULT, DEFAULT, DEFAULT, sData.iTint1, sData.iTint2)
										ENDIF
										
									// NG MP Overlay
									ELIF sOptionData.iType = 1
									OR sOptionData.iType = 2
										CDEBUG1LN(DEBUG_SHOPS, "buying ped overlay.GET_NGMP_MENU_OPTION_DATA(", GET_LABEL_FOR_NGMP_MENU(sData.eCurrentMenu), ", ", sData.sBrowseInfo.iCurrentItem, "): ",
												sOptionData.tlLabel, ", iTexture: ",
												sOptionData.iTexture, ", eHairItem: ",
												sOptionData.eHairItem, ", iType: ",
												sOptionData.iType)
									
										SAFE_SET_PED_HEAD_OVERLAY(PLAYER_PED_ID(), sOptionData.eHeadSlot, sOptionData.iTexture, sData.iTint1, sData.iTint2, sData.eRampType, sData.fBlend)
										IF g_bInMultiplayer
											//contact lenses
											IF sOptionData.eHeadSlot = HOS_BASE_DETAIL
												SET_MP_FLOAT_CHARACTER_STAT(MP_STAT_FEATURE_20, TO_FLOAT(sOptionData.iTexture))
												
												sData.iTint1 = -1
												sData.iTint2 = -1
												sData.iSavedTint2 = sData.iTint2
											ELSE
												SET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(sOptionData.eHeadSlot), sOptionData.iTexture)
												
												IF sData.iTint1 >= 0
													IF (sOptionData.eHeadSlot = HOS_BLUSHER)
													AND (sData.eRampType = RT_NONE)
														CDEBUG1LN(DEBUG_SHOPS, "DO_BROWSE_ITEMS: update facepaint ramp type [RT_", sData.eRampType, " to RT_MAKEUP] for ", GET_CHARACTER_OVERLAY_TEXT(sOptionData.eHeadSlot))
														sData.eRampType = RT_MAKEUP
													ELIF (sOptionData.eHeadSlot = HOS_MAKEUP)
													AND (sData.eRampType = RT_MAKEUP)
														CDEBUG1LN(DEBUG_SHOPS, "DO_BROWSE_ITEMS: update facepaint ramp type [RT_", sData.eRampType, " to RT_NONE] for ", GET_CHARACTER_OVERLAY_TEXT(sOptionData.eHeadSlot))
														sData.eRampType = RT_NONE
													ENDIF
													
													SET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY(sOptionData.eHeadSlot, sData.iTint1, sData.eRampType)
												ENDIF
												IF sData.iTint2 >= 0
													SET_MP_INT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_SECONDARY_COLOUR_STAT(sOptionData.eHeadSlot), sData.iTint2)
												ENDIF
												
												SET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(sOptionData.eHeadSlot), sData.fBlend)
												
												IF (sData.eCurrentMenu = HME_NGMP_FACEPAINT)
													IF (sOptionData.eHeadSlot = HOS_MAKEUP)
														SET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_BLUSHER), 255)
														
													ELIF (sOptionData.eHeadSlot = HOS_BLUSHER)
														SET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_MAKEUP), 255)
														
													ENDIF
												ENDIF
											ENDIF
										ENDIF
											
										// Update cutscene ped too...
										IF PLAYER_PED_ID() != pedID_ShopPed
											SAFE_SET_PED_HEAD_OVERLAY(pedID_ShopPed, sOptionData.eHeadSlot, sOptionData.iTexture,sData.iTint1, sData.iTint2, sData.eRampType, sData.fBlend)
										ENDIF
									ENDIF
								ENDIF
							ENDIF

						// Trigger particle effects + sounds when the barber starts cutting
						ELIF GET_SYNC_SCENE_PHASE(sData.iSyncSceneShopkeeper, sData.iNETSyncSceneShopkeeper)> fPhaseStartSound
							
							// particle effects
							IF sData.bLoadPTFX
							AND sData.ptfxHair = NULL
								// TODO! don't have particle effects if the player is bald
								IF sData.eCurrentMenu = HME_HAIR 
									sData.ptfxHair = START_PARTICLE_FX_LOOPED_ON_PED_BONE("scr_barbers_haircut", pedID_ShopPed,<<0.0, 0.0, 0.1>>, <<0.0, 0.0, 0.0>>, BONETAG_HEAD)
								ENDIF
							ENDIF
							
							IF DOES_PARTICLE_FX_LOOPED_EXIST(sData.ptfxHair)
								SET_PARTICLE_FX_LOOPED_COLOUR(sData.ptfxHair,0,0,0)
							ENDIF
							
							// Sounds
							IF NOT sData.bPlayingSound
								// Hair	
								IF sData.eCurrentMenu = HME_HAIR 
								OR sData.eCurrentMenu = HME_NGMP_HAIR
									IF HAS_SOUND_FINISHED(sData.iHairCutSoundID)
										PLAY_SOUND_FROM_ENTITY(sData.iHairCutSoundID, "Scissors", pedID_ShopPed,"Barber_Sounds")
									ENDIF
								// Beards
								ELIF sData.eCurrentMenu = HME_BEARD  
								OR sData.eCurrentMenu = HME_NGMP_BEARD
								OR sData.eCurrentMenu = HME_NGMP_CHEST
								OR sData.eCurrentMenu = HME_NGMP_EYEBROWS
									IF HAS_SOUND_FINISHED(sData.iClippersSoundID)
										PLAY_SOUND_FROM_ENTITY(sData.iHairCutSoundID, "Scissors", pedID_ShopPed,"Barber_Sounds")
										//PLAY_SOUND_FROM_ENTITY(sData.iClippersSoundID, "Clippers", pedID_ShopPed,"Barber_Sounds")
									ENDIF
								// Contacts
								ELIF sData.eCurrentMenu = HME_NGMP_CONTACTS	
									// nothing
								ELSE
								// Makeup
									IF HAS_SOUND_FINISHED(sData.iMakeupSoundID)
										PLAY_SOUND_FROM_ENTITY(sData.iMakeupSoundID, "Makeup", pedID_ShopPed,"Barber_Sounds")
									ENDIF
								ENDIF
								sData.bPlayingSound = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// Build the menu
				IF sData.bRebuildMenu
					REMOVE_TATTOOS_NOT_PURCHASED(pedID_ShopPed)
					BUILD_HAIRDO_SHOP_MENU(sData, pedID_ShopPed)
				ENDIF
				
				IF CHECK_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
					// Process later.
				
				// Make selection 	
				ELIF NOT sData.sBrowseInfo.bPurchasingItem
				AND NOT sData.sBrowseInfo.bItemSelected
					
					// Rebuild menu if control method has changed
					IF HAVE_CONTROLS_CHANGED(FRONTEND_CONTROL)
					
						// Rebuild menu (Backup selection, rebuild menu, then restore selection)
						INT iBackupItem
						iBackupItem = sData.sBrowseInfo.iCurrentItem
						sData.bRebuildMenu = TRUE
						REMOVE_TATTOOS_NOT_PURCHASED(pedID_ShopPed)
						BUILD_HAIRDO_SHOP_MENU(sData, pedID_ShopPed)
						sData.sBrowseInfo.iCurrentItem = iBackupItem
						SET_CURRENT_MENU_ITEM(sData.sBrowseInfo.iCurrentItem)
						sData.iTopItem = GET_TOP_MENU_ITEM()

					ELIF NOT IS_PAUSE_MENU_ACTIVE()
					AND NOT IS_SYSTEM_UI_BEING_DISPLAYED()
					AND NOT IS_WARNING_MESSAGE_ACTIVE()
					AND NOT g_sShopSettings.bProcessStoreAlert
					AND NOT NETWORK_TEXT_CHAT_IS_TYPING()
						
						UPDATE_HAIRDO_SHOP_BROWSE_CAM(sData, TRUE)
						
						SWITCH sData.eCurrentMenu
							// Main menu
							CASE HME_MAIN_MENU
								UPDATE_MAIN_SELECTION(sData, pedID_ShopPed)
							BREAK
							
							// Hairstyles
							CASE HME_HAIR_GROUP
								UPDATE_HAIRDO_GROUP_SELECTION(sData, pedID_ShopPed)
							BREAK
							
							CASE HME_HAIR
								UPDATE_HAIRDO_SELECTION(sData, pedID_ShopPed)
							BREAK
							
							// Beards
							CASE HME_BEARD
								IF NETWORK_IS_GAME_IN_PROGRESS()
									UPDATE_BEARD_SELECTION(sData, pedID_ShopPed)
								ELSE
									// SP uses hair function as functionality is the same for beards (use ped comp data)
									UPDATE_HAIRDO_SELECTION(sData, pedID_ShopPed)
								ENDIF
							BREAK
							
							// Makeup
							CASE HME_MAKEUP
								UPDATE_MAKEUP_SELECTION(sData, pedID_ShopPed)
							BREAK
							
							// NG MP
							DEFAULT
								IF IS_NGMP_MENU(sData.eCurrentMenu)
									UPDATE_NGMP_SELECTION(sData, pedID_ShopPed)
								ENDIF
							BREAK
						ENDSWITCH
					ENDIF
					
					NGMP_MENU_OPTION_DATA sOptionData
					IF GET_NGMP_MENU_OPTION_DATA(GET_ENTITY_MODEL(PLAYER_PED_ID()), sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
						IF SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP(sOptionData.tlLabel, sData.eCurrentMenu, sOptionData.iCost)
						AND NOT IS_NGMP_ITEM_AND_TINTS_CURRENT(GET_ENTITY_MODEL(PLAYER_PED_ID()), sOptionData, sData)
							TEXT_LABEL tlDisc
							tlDisc = ""
							GET_CURRENT_MENU_ITEM_DISCOUNT(tlDisc)
							IF GET_HASH_KEY(tlDisc) = 0
		//					AND bShowDiscountInfo = TRUE
								SET_CURRENT_MENU_ITEM_DISCOUNT("HAIR_SALE")
							ENDIF
		//					bShowDiscountInfo = FALSE
						ENDIF
					ENDIF
				ENDIF
				
				//UPDATE_NETWORK_SYNC_SCENE_LOOPED_STATE(sData) // we now deal with this when we setup the network sync scene

				TEXT_LABEL_15 tlDesc
				
				// Handle locked items
				IF sData.bSelectedItemIsLocked
					IF sData.sBrowseInfo.iCurrentItem >= 0
						IF NETWORK_IS_GAME_IN_PROGRESS()
							GET_CURRENT_MENU_ITEM_DESCRIPTION(tlDesc)
							IF GET_HASH_KEY(tlDesc) = 0
								IF sData.eCurrentMenu = HME_HAIR
									SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_UNLOCK", 0)
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(GET_FM_HAIRCUT_UNLOCK_RANK(g_sShopCompItemData.eItems[sData.sBrowseInfo.iCurrentItem]))
								ELIF sData.eCurrentMenu = HME_BEARD
									SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_UNLOCK", 0)
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(GET_FM_MAKEUP_UNLOCK_RANK(GET_BEARD_UNLOCK_ENUM_FOR_SHOP(GET_BEARD_INDEX_FROM_MENU_INDEX(sData.sBrowseInfo.iCurrentItem))))
								ELIF sData.eCurrentMenu = HME_MAKEUP
									SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_UNLOCK", 0)
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(GET_FM_MAKEUP_UNLOCK_RANK(GET_MAKEUP_UNLOCK_ENUM_FOR_SHOP(sData.sBrowseInfo.iCurrentItem)))
								ELIF IS_NGMP_MENU(sData.eCurrentMenu)
									// NG TODO - Setup unlocks for menu and use here
									
									NGMP_MENU_OPTION_DATA sOptionData
									IF GET_NGMP_MENU_OPTION_DATA(GET_ENTITY_MODEL(PLAYER_PED_ID()), sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
										// NG MP Ped Comp
										
										BOOL bRankLocked
										bRankLocked = TRUE
										
										IF NETWORK_IS_GAME_IN_PROGRESS()
										AND SHOULD_SHOP_SCRIPT_USE_BASKET_SYSTEM()
										AND NOT IS_STRING_NULL_OR_EMPTY(sOptionData.tlLabel)
											TEXT_LABEL_63 tlCategoryKey
											GENERATE_HAIRDO_KEY_FOR_CATALOGUE(tlCategoryKey, sData.eCurrentMenu, sOptionData.tlLabel)
											
											IF NOT NET_GAMESERVER_CATALOG_ITEM_IS_VALID(tlCategoryKey)
												CERRORLN(DEBUG_SHOPS, "PC RANK CHECK GET_NGMP_MENU_OPTION_DATA: \"", tlCategoryKey, "\" (keyhash:", GET_HASH_KEY(tlCategoryKey), ") has no valid catalog item, add price:$", sOptionData.iCost, " for \"", GET_STRING_FROM_TEXT_FILE(sOptionData.tlLabel), "\".")
												bRankLocked = FALSE
											ENDIF
										ENDIF
										IF bRankLocked
											IF sOptionData.iType = 0
												IF sOptionData.eHairItem = DUMMY_PED_COMP
													CERRORLN(DEBUG_SHOPS, "HAIR RANK CHECK sOptionData.eHairItem = DUMMY_PED_COMP.")
													bRankLocked = FALSE
												ELIF GET_FM_HAIRCUT_UNLOCK_RANK(sOptionData.eHairItem) = 0
													CERRORLN(DEBUG_SHOPS, "HAIR RANK CHECK GET_FM_HAIRCUT_UNLOCK_RANK(", sOptionData.eHairItem, ") = 0")
													bRankLocked = FALSE
												ENDIF
											ELIF sOptionData.iType = 1
											OR sOptionData.iType = 2
												IF sOptionData.eUnlockItem = DUMMY_CLOTHES_UNLOCK
													CERRORLN(DEBUG_SHOPS, "MAKEUP RANK CHECK sOptionData.eHairItem = DUMMY_PED_COMP.")
													bRankLocked = FALSE
												ELIF GET_FM_MAKEUP_UNLOCK_RANK(sOptionData.eUnlockItem) = 0
													CERRORLN(DEBUG_SHOPS, "MAKEUP RANK CHECK GET_FM_MAKEUP_UNLOCK_RANK(", sOptionData.eUnlockItem, ") = 0")
													bRankLocked = FALSE
												ENDIF
											ENDIF
										ENDIF
										
										IF NOT bRankLocked
											IF sOptionData.iType = 0
												SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_LCKPC", 0)
											// NG MP Overlay
											ELIF sOptionData.iType = 1
											OR sOptionData.iType = 2
												IF sOptionData.eHeadSlot = HOS_FACIAL_HAIR
													SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_LCKPC", 0)
												ELIF sOptionData.eHeadSlot = HOS_MAKEUP
													SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_LCKPC", 0)
												ELSE
													CASSERTLN(DEBUG_SHOPS, "MISSING LOCKED PC ITEM MESSAGE!!!")
												ENDIF
											ENDIF
										ELSE
											IF sOptionData.iType = 0
												SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_UNLOCK", 0)
												ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(GET_FM_HAIRCUT_UNLOCK_RANK(sOptionData.eHairItem))
											// NG MP Overlay
											ELIF sOptionData.iType = 1
											OR sOptionData.iType = 2
												IF sOptionData.eHeadSlot = HOS_FACIAL_HAIR
													SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_UNLOCK", 0)
													ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(GET_FM_MAKEUP_UNLOCK_RANK(sOptionData.eUnlockItem))
												ELIF sOptionData.eHeadSlot = HOS_MAKEUP
													SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_UNLOCK", 0)
													ADD_CURRENT_MENU_ITEM_DESCRIPTION_INT(GET_FM_MAKEUP_UNLOCK_RANK(sOptionData.eUnlockItem))
												ELSE
													CASSERTLN(DEBUG_SHOPS, "MISSING LOCKED ITEM MESSAGE!!!")
												ENDIF
											ENDIF
										ENDIF
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
				ELIF sData.eCurrentMenu = HME_MAIN_MENU
					IF sData.sBrowseInfo.iCurrentItem >= 0
						IF NETWORK_IS_GAME_IN_PROGRESS()
							GET_CURRENT_MENU_ITEM_DESCRIPTION(tlDesc)
							IF GET_HASH_KEY(tlDesc) = 0
								IF sData.sBrowseInfo.iCurrentItem = 0 // Hair
									IF HAS_MENU_GOT_NEW_ITEMS(sData, GET_ENTITY_MODEL(pedID_ShopPed), HME_HAIR)
										SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_UNLOCK_M")	
									ENDIF
								ELIF sData.sBrowseInfo.iCurrentItem = 1 // Beard
									IF HAS_MENU_GOT_NEW_ITEMS(sData, GET_ENTITY_MODEL(pedID_ShopPed), HME_BEARD)
										SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_UNLOCK_M")	
									ENDIF
								ELIF sData.sBrowseInfo.iCurrentItem = 2 // Makeup
									IF HAS_MENU_GOT_NEW_ITEMS(sData, GET_ENTITY_MODEL(pedID_ShopPed), HME_MAKEUP)
										SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_UNLOCK_M")	
									ENDIF
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				// NG MP Main
				ELIF sData.eCurrentMenu = HME_NGMP_MAIN
					// NG TODO - Add new item unlock text
				ELIF sData.eCurrentMenu = HME_HAIR_GROUP
					IF sData.eCurrentHairGroup > HAIR_GROUP_ALL
						IF NETWORK_IS_GAME_IN_PROGRESS()
							GET_CURRENT_MENU_ITEM_DESCRIPTION(tlDesc)
							IF GET_HASH_KEY(tlDesc) = 0
								IF IS_BIT_SET(sData.iHairdoGroupsWithNewItems[ENUM_TO_INT(sData.eCurrentHairGroup)/32], ENUM_TO_INT(sData.eCurrentHairGroup)%32)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_UNLOCK_M")
								ENDIF
							ENDIF
						ENDIF
					ENDIF
				ELIF sData.eCurrentMenu != HME_MAIN_MENU AND sData.bSelectedItemIsNew AND NOT sData.bUnlockMessageDisplayed
					IF sData.sBrowseInfo.iCurrentItem >= 0
						IF NETWORK_IS_GAME_IN_PROGRESS()
							GET_CURRENT_MENU_ITEM_DESCRIPTION(tlDesc)
							IF GET_HASH_KEY(tlDesc) = 0
								IF sData.bSelectedItemIsDLC
									IF IS_SHOP_CONTENT_FROM_PACK(sData.sCurrentItemLabel)
										SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKDLC", 4000)
										ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel))
									ELIF IS_SHOP_CONTENT_FROM_SPECIAL(sData.sCurrentItemLabel)
										SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKDLC3", 4000)
										ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel))
									ELIF IS_SHOP_CONTENT_FROM_SURPRISE(sData.sCurrentItemLabel)
										SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKDLC4", 4000)
										ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel))
									ELIF IS_SHOP_CONTENT_FROM_BASIC(sData.sCurrentItemLabel)
										SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKDLC6", 4000)
										ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel))
									ELIF IS_SHOP_CONTENT_FROM_HALLOWEEN_SURPRISE(sData.sCurrentItemLabel)
										SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKDLC7", 4000)
									ELIF IS_SHOP_CONTENT_FROM_PLATFORM(sData.sCurrentItemLabel)
										SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKDLC8", 4000)
									ELSE
										SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_UNLOCKDLC2", 4000)
										ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel, TRUE))
									ENDIF
								ELSE
									SET_CURRENT_MENU_ITEM_DESCRIPTION("HAIR_UNLOCK_N", 4000)
								ENDIF
								sData.bUnlockMessageDisplayed = TRUE
							ENDIF
						ENDIF
					ENDIF
				ENDIF
				
				// Draw the Menu  	
				IF PROCESS_COMMERCE_STORE_TRIGGER(sStoreTriggerData)
					// Processing.
				ELIF (NOT sData.sBrowseInfo.bItemSelected) OR (sData.sBrowseInfo.bPurchasingItem)
					
					#IF USE_TU_CHANGES
					IF NETWORK_IS_GAME_IN_PROGRESS() AND NOT bDisplayingCash
						SET_MULTIPLAYER_WALLET_CASH()  //Display cash on hud
						SET_MULTIPLAYER_BANK_CASH()
						bDisplayingCash = TRUE
					ENDIF
					#ENDIF
					
					TEXT_LABEL_15 tlDescLabel
					IF sData.eCurrentMenu = HME_HAIR
					OR sData.eCurrentMenu = HME_BEARD
					OR sData.eCurrentMenu = HME_MAKEUP
					OR sData.eCurrentMenu = HME_NGMP_HAIR
					OR sData.eCurrentMenu = HME_NGMP_FACEPAINT
						GET_CURRENT_MENU_ITEM_DESCRIPTION(tlDescLabel)
						IF GET_HASH_KEY(tlDescLabel) = 0
							IF sData.bSelectedItemIsDLC
							AND NOT IS_STRING_NULL_OR_EMPTY(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel))
								IF IS_SHOP_CONTENT_FROM_PACK(sData.sCurrentItemLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK")
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel))
								ELIF IS_SHOP_CONTENT_FROM_SPECIAL(sData.sCurrentItemLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK3")
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel))
								ELIF IS_SHOP_CONTENT_FROM_SURPRISE(sData.sCurrentItemLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK4")
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel))
								ELIF IS_SHOP_CONTENT_FROM_BASIC(sData.sCurrentItemLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK6")
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel))
								ELIF IS_SHOP_CONTENT_FROM_HALLOWEEN_SURPRISE(sData.sCurrentItemLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK7")
								ELIF IS_SHOP_CONTENT_FROM_PLATFORM(sData.sCurrentItemLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK8")
								ELSE
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK2")
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel, TRUE))
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH)
					SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_CASH)
					SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_MP_MESSAGE)
					SHOW_HUD_COMPONENT_THIS_FRAME(NEW_HUD_CASH_CHANGE)
					DRAW_MENU(TRUE, ENUM_TO_INT(sData.sShopInfo.eShop))
					
					// We need to move the highlights UI out of the way when it's not visible
					// on PC, as the two movies for hair colour and highlights overlap and
					// interfere with each-other when checking mouse events.
					
					FLOAT fCentreX_01_aspectOffset
					fHighlightScaleformOffset = 0.0
					IF IS_PC_VERSION()
						IF NOT IS_HIGHLIGHTS_UI_ACTIVE(sData)
							fHighlightScaleformOffset = 0.4
						ENDIF
						
						IF ABSF(GET_ASPECT_RATIO(FALSE) - AR16_10) <0.1 //16:10, most UI errors
							fCentreX_01_aspectOffset =  fAspectOffset_16x10
							CDEBUG3LN(DEBUG_SHOPS, "aspect ratio: ", GET_ASPECT_RATIO(FALSE), ", AR16_10, fCentreX_01_aspectOffset: ", fCentreX_01_aspectOffset)
						ELIF ABSF(GET_ASPECT_RATIO(FALSE) - AR16_9) <0.1
							fCentreX_01_aspectOffset = fAspectOffset_16x9
							CDEBUG3LN(DEBUG_SHOPS, "aspect ratio: ", GET_ASPECT_RATIO(FALSE), ", AR16_9, fCentreX_01_aspectOffset: ", fCentreX_01_aspectOffset)
						ELIF ABSF(GET_ASPECT_RATIO(FALSE) - AR4_3) <0.1
							fCentreX_01_aspectOffset = fAspectOffset_4x3
							CDEBUG3LN(DEBUG_SHOPS, "aspect ratio: ", GET_ASPECT_RATIO(FALSE), ", AR4_3, fCentreX_01_aspectOffset: ", fCentreX_01_aspectOffset)
						ELSE
							fCentreX_01_aspectOffset = 0.0
							CDEBUG3LN(DEBUG_SHOPS, "aspect ratio: ", GET_ASPECT_RATIO(FALSE), ", fCentreX_01_aspectOffset: ", fCentreX_01_aspectOffset)
						ENDIF
						
					ENDIF
					
					SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
					SET_SCRIPT_GFX_ALIGN_PARAMS(0.0, 0.0, 0.0, 0.0)
					
					DRAW_SCALEFORM_MOVIE(sData.colourTint_movieID_01.movieID,
							mpng_centreX_01 + fCentreX_01_aspectOffset,
							mpng_centreY_01+GET_CUSTOM_MENU_FINAL_Y_COORD(),
							CUSTOM_MENU_W,
							mpng_height_01,
							255,255,255,255)
					RESET_SCRIPT_GFX_ALIGN()
					
					SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
					SET_SCRIPT_GFX_ALIGN_PARAMS(0.0, 0.0, 0.0, 0.0)
						DRAW_SCALEFORM_MOVIE(sData.colourTint_movieID_02.movieID,
								mpng_centreX_01 + fCentreX_01_aspectOffset + fHighlightScaleformOffset,
								mpng_centreY_02+GET_CUSTOM_MENU_FINAL_Y_COORD(),
								CUSTOM_MENU_W,
								mpng_height_01,
								255,255,255,255)
					RESET_SCRIPT_GFX_ALIGN()
										
					IF sData.eCurrentMenu = HME_HAIR
					OR sData.eCurrentMenu = HME_BEARD
					OR sData.eCurrentMenu = HME_NGMP_HAIR
					OR sData.eCurrentMenu = HME_NGMP_FACEPAINT
						GET_CURRENT_MENU_ITEM_DESCRIPTION(tlDescLabel)
						IF GET_HASH_KEY(tlDescLabel) = 0
							IF sData.bSelectedItemIsDLC
							AND NOT IS_STRING_NULL_OR_EMPTY(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel))
								IF IS_SHOP_CONTENT_FROM_PACK(sData.sCurrentItemLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK")
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel))
								ELIF IS_SHOP_CONTENT_FROM_SPECIAL(sData.sCurrentItemLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK3")
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel))
								ELIF IS_SHOP_CONTENT_FROM_SURPRISE(sData.sCurrentItemLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK4")
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel))
								ELIF IS_SHOP_CONTENT_FROM_BASIC(sData.sCurrentItemLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK6")
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel))
								ELIF IS_SHOP_CONTENT_FROM_HALLOWEEN_SURPRISE(sData.sCurrentItemLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK7")
								ELIF IS_SHOP_CONTENT_FROM_PLATFORM(sData.sCurrentItemLabel)
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK8")
								ELSE
									SET_CURRENT_MENU_ITEM_DESCRIPTION("SHOP_DLC_PACK2")
									ADD_CURRENT_MENU_ITEM_DESCRIPTION_STRING(GET_SHOP_CONTENT_FOR_MENU(sData.sCurrentItemLabel, TRUE))
								ENDIF
							ENDIF
						ENDIF
					ENDIF
					
					NGMP_MENU_OPTION_DATA sOptionData
					IF GET_NGMP_MENU_OPTION_DATA(GET_ENTITY_MODEL(PLAYER_PED_ID()), sData.eCurrentMenu, sData.sBrowseInfo.iCurrentItem, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
						IF SHOULD_DISCOUNT_BE_DISPLAYED_FOR_NGMP(sOptionData.tlLabel, sData.eCurrentMenu, sOptionData.iCost)
						AND NOT IS_NGMP_ITEM_AND_TINTS_CURRENT(GET_ENTITY_MODEL(PLAYER_PED_ID()), sOptionData, sData)
							TEXT_LABEL tlDisc
							tlDisc = ""
							GET_CURRENT_MENU_ITEM_DISCOUNT(tlDisc)
							IF GET_HASH_KEY(tlDisc) = 0
		//					AND bShowDiscountInfo = TRUE
								SET_CURRENT_MENU_ITEM_DISCOUNT("HAIR_SALE")
							ENDIF
		//					bShowDiscountInfo = FALSE
						ENDIF
					ENDIF
					
					#IF USE_TU_CHANGES
					IF NETWORK_IS_GAME_IN_PROGRESS()
					
						//#1657908
						SET_MULTIPLAYER_WALLET_CASH()
						SET_MULTIPLAYER_BANK_CASH()
						
						IF g_sMPTunables.b_onsale_hair_dresser_shop
							IF HAS_STREAMED_TEXTURE_DICT_LOADED("MPShops")
								FLOAT texW, texH
								IF GET_MENU_ICON_SCREEN_RESOLUTION(MENU_ICON_HEADER, TRUE, TRUE, texW, texH)
									SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP) 
									SET_SCRIPT_GFX_ALIGN_PARAMS(0.0, 0.0, 0.0, 0.0)
									DRAW_SPRITE("MPShops", "ShopUI_Title_Graphics_SALE", 0.112, 0.045, CUSTOM_MENU_W, texH, 0.0, 255, 255, 255, 255)
									RESET_SCRIPT_GFX_ALIGN() 
							    ENDIF
							ENDIF
						ENDIF
					ENDIF
					#ENDIF
					
				ELSE
					#IF USE_TU_CHANGES
					IF NETWORK_IS_GAME_IN_PROGRESS() AND bDisplayingCash
						REMOVE_MULTIPLAYER_WALLET_CASH()  //Remove cash on hud
						REMOVE_MULTIPLAYER_BANK_CASH()
						bDisplayingCash = FALSE
					ENDIF
					#ENDIF	
				ENDIF
			ENDIF	
		BREAK
		
		// -------Clean up -------------------------------
		CASE SHOP_BROWSE_STAGE_CLEANUP
			
			//IF NOT g_bInMultiplayer	
				SET_PED_CAN_ARM_IK(PLAYER_PED_ID(),TRUE)
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(),PCF_OpenDoorArmIK,TRUE)
				SET_PED_RESET_FLAG(PLAYER_PED_ID(),PRF_SearchForClosestDoor,TRUE) 
			//ENDIF
			
			IF sData.sBrowseInfo.bBrowsing
				
				IF NETWORK_IS_GAME_IN_PROGRESS()	
					// Reset the head and eye props
					ResetProps(sData)
				ELSE
					RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
					RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
				ENDIF
				
				// Tell the shop keeper to to get in position
				IF NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
					CLEAR_PED_TASKS(sData.sShopInfo.sShopKeeper.pedID)
					SHOPWORKER_GO_TO_IDLE_POS(sData, FALSE)
					SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sData.sShopInfo.sShopKeeper.pedID, TRUE)
				ENDIF
				/*
				IF NETWORK_IS_GAME_IN_PROGRESS()
					// Return to gameplay camera
					DEACTIVATE_SHOP_CAMERAS(sData.sShopInfo.sCameras)
					RENDER_SCRIPT_CAMS(FALSE, FALSE)
				ENDIF
				*/
				IF NETWORK_IS_GAME_IN_PROGRESS()
					HIDE_NET_SCISSORS()
					STOP_NETWORK_SYNC_SCENE_FOR_SHOP(sData)
				ENDIF
				
				// Assets cleanup
				CLEANUP_MENU_ASSETS(TRUE, ENUM_TO_INT(sData.sShopInfo.eShop))
				CLEANUP_NGMP_MENU_ASSETS(sData)
				UNLOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
				CLEANUP_HAIRDO_CUTSCENE_ASSETS(sData)
				CLEANUP_PLAYER_FOR_MP_SHOP_CUTSCENE()
				sData.sBuddyHideData.buddyHide = FALSE
				
				IF NETWORK_IS_GAME_IN_PROGRESS()
					// set hair on again so we get the overlay
					// only if not wearing a mask
					IF NOT IS_PED_INJURED(PLAYER_PED_ID())
						IF NOT IS_PED_WEARING_A_MASK(PLAYER_PED_ID())
							CPRINTLN(DEBUG_SHOPS, "Setting hair on again so we get the overlay.")
							PED_COMP_NAME_ENUM eCurrentHair 
							eCurrentHair = CALL sData.fpGetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_HAIR)
							CALL sData.fpSetPedCompItemCurrent(PLAYER_PED_ID(), COMP_TYPE_HAIR, eCurrentHair, FALSE)
							
							IF NETWORK_IS_GAME_IN_PROGRESS()
								INT iTintIndex1, iTintIndex2
								iTintIndex1 = SAFE_GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
								iTintIndex2 = SAFE_GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
								SET_PED_HAIR_TINT(PLAYER_PED_ID(), iTintIndex1, iTintIndex2)
							ENDIF
							
							IF sData.eCrewLogoTattoo != INVALID_TATTOO
								SET_MP_TATTOO_CURRENT(sData.eCrewLogoTattoo, TRUE)
								sData.eCrewLogoTattoo = INVALID_TATTOO
							ENDIF
							REMOVE_TATTOOS_NOT_PURCHASED(PLAYER_PED_ID())
						ENDIF
					ENDIF
				ENDIF
				
				#IF USE_TU_CHANGES
				IF NETWORK_IS_GAME_IN_PROGRESS()
					REMOVE_MULTIPLAYER_WALLET_CASH()  //Remove cash on hud
					REMOVE_MULTIPLAYER_BANK_CASH()
				ENDIF
				#ENDIF	
				
				// Return player control
				//IF NETWORK_IS_GAME_IN_PROGRESS()
					//UNLOCK_PLAYER_FOR_BROWSE(sData.sBrowseInfo)
				//ELSE	
					UNLOCK_PLAYER_FOR_BROWSE(sData.sBrowseInfo,FALSE,FALSE)
				//ENDIF
				END_SHOP_CUTSCENE()
				SET_PLAYER_IS_CHANGING_CLOTHES(FALSE)
				
				// Update stats incase we changed clothes
				IF NETWORK_IS_GAME_IN_PROGRESS()
				AND NOT IS_PED_INJURED(PLAYER_PED_ID())
					IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(TRUE)
					AND GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
						// block
						PRINTLN("kr_debug: blocking call to SAVE_PLAYERS_CLOTHES as player is wearing goon outfit")
					ELSE
						SAVE_PLAYERS_CLOTHES(PLAYER_PED_ID())
					ENDIF
				ENDIF
				
				IF sData.sShopInfo.eShop != HAIRDO_SHOP_CASINO_APT 
				AND IS_SHOP_KEEPER_SAFE_TO_USE(PLAYER_ID(), sData.eInShop, TRUE)
					REMOVE_USE_OF_SHOP_KEEPER(PLAYER_ID(), sData.eInShop)
				ENDIF
				
				IF sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT 
				AND IS_SHOP_KEEPER_SAFE_TO_USE_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_ID(), sData.eInShop, FALSE)
					REMOVE_USE_OF_SHOP_KEEPER_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_ID(), sData.eInShop)
				ENDIF
				
				// Update stored hairdo
				IF NOT g_bInMultiplayer
					STORE_PLAYER_PED_HAIRDO(PLAYER_PED_ID(), TRUE)
				ENDIF
				
				IF g_bTriggerShopSave
					IF g_bInMultiplayer
						// KGM 11/12/12: Tell all players to re-Generate a player headshot after appearance has changed
						BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
					ENDIF
				ENDIF
				
				SETTIMERB(0)
				sData.sBrowseInfo.iBrowseTimer = GET_GAME_TIMER()
				sData.sBrowseInfo.bBrowsing = FALSE
				sData.sBrowseInfo.bExitTrigger = TRUE // Use this flag for the walk out transition
				sData.sBrowseInfo.bItemPurchased = FALSE
				sData.sBrowseInfo.bPurchasingItem = FALSE
			ELSE
				
				// Let the go to coord task complete before we finish up
				IF sData.sBrowseInfo.bExitTrigger
				
					// Update input and task status
					UPDATE_SHOP_INPUT_DATA(sData.sInputData, 300)
					IF GET_SCRIPT_TASK_STATUS(PLAYER_PED_ID(), SCRIPT_TASK_GO_STRAIGHT_TO_COORD) = FINISHED_TASK
					OR sData.sInputData.leftStickLR < -75 OR sData.sInputData.leftStickLR > 75 // Left/Right
					OR sData.sInputData.leftStickUD < -75 OR sData.sInputData.leftStickUD > 75 // Forwards/Backwards
					OR IS_POSITION_OCCUPIED(GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(),<<0,1.5,0>>),0.2,FALSE,TRUE,TRUE,FALSE,FALSE,PLAYER_PED_ID())	
						END_SHOP_CUTSCENE() // fail safe
						CLEAR_PED_TASKS(PLAYER_PED_ID())

						IF NETWORK_IS_GAME_IN_PROGRESS()
						AND NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)	
						AND DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE)
							SET_ENTITY_INVINCIBLE(sData.sShopInfo.sShopKeeper.pedID,FALSE)
							SET_PED_CAN_RAGDOLL(sData.sShopInfo.sShopKeeper.pedID,TRUE)
							IF sData.bRelGroupSetup
								SET_PED_RELATIONSHIP_GROUP_HASH(sData.sShopInfo.sShopKeeper.pedID, sData.hairdoGroup)
							ENDIF
						ENDIF
						
						IF sData.bPurchasedHairOrMakeup
							REQUEST_SYSTEM_ACTIVITY_TYPE_BOUGHT_HAIRCUT_OR_MAKEUP()
						ENDIF
						
						// Dont do auto trigger until we leave shop.
						sData.bDoInstantLocateTrigger = FALSE
						sData.sBrowseInfo.bBrowseTrigger = FALSE	
						sData.bPurchasedHairOrMakeup = FALSE
						sData.sShopInfo.eStage = SHOP_STAGE_LEAVE
					ENDIF
				ELSE

					IF NETWORK_IS_GAME_IN_PROGRESS()
					AND NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)	
					AND DO_WE_HAVE_CONTROL_OF_SHOP_KEEPER(FALSE)	
						SET_ENTITY_INVINCIBLE(sData.sShopInfo.sShopKeeper.pedID,FALSE)
						SET_PED_CAN_RAGDOLL(sData.sShopInfo.sShopKeeper.pedID,TRUE)
						IF sData.bRelGroupSetup
							SET_PED_RELATIONSHIP_GROUP_HASH(sData.sShopInfo.sShopKeeper.pedID, sData.hairdoGroup)
						ENDIF
					ENDIF
						
					IF sData.bPurchasedHairOrMakeup
						REQUEST_SYSTEM_ACTIVITY_TYPE_BOUGHT_HAIRCUT_OR_MAKEUP()
					ENDIF
					
					// Dont do auto trigger until we leave shop.
					sData.bDoInstantLocateTrigger = FALSE
					sData.sBrowseInfo.bBrowseTrigger = FALSE
					sData.bPurchasedHairOrMakeup = FALSE
					sData.sShopInfo.eStage = SHOP_STAGE_LEAVE
				ENDIF
			ENDIF
		BREAK
		
		// ---------------Bail ---------------------------------------------
		CASE SHOP_BROWSE_STAGE_BAIL
			CPRINTLN(DEBUG_SHOPS, "DO_BROWSE_ITEMS: SHOP_BROWSE_STAGE_BAIL")
		
			// Something went wrong when the player was browsing so we need to
			// bail out of the browsing process.
			
			// NOTE: Commands will be getting added to check cuffed state so we can
			// 		 tell if the player should be given control back.
						
			// Return to gameplay camera
			
			#IF USE_TU_CHANGES
			IF NETWORK_IS_GAME_IN_PROGRESS()
				REMOVE_MULTIPLAYER_WALLET_CASH()  //Remove cash on hud
				REMOVE_MULTIPLAYER_BANK_CASH()
			ENDIF
			#ENDIF	
			
			DEACTIVATE_SHOP_CAMERAS(sData.sShopInfo.sCameras)
			RENDER_SCRIPT_CAMS(FALSE, FALSE)
			
			// Assets cleanup
			CLEANUP_MENU_ASSETS(TRUE, ENUM_TO_INT(sData.sShopInfo.eShop))
			CLEANUP_NGMP_MENU_ASSETS(sData)
			UNLOAD_SHOP_INTRO_ASSETS(sData.sShopInfo.eShop)
			CLEANUP_HAIRDO_CUTSCENE_ASSETS(sData)
			sData.sBuddyHideData.buddyHide = FALSE
			
			ResetProps(sData)
			
			// Return player control
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
			ENDIF
			UNLOCK_PLAYER_FOR_BROWSE(sData.sBrowseInfo)
			CLEANUP_PLAYER_FOR_MP_SHOP_CUTSCENE()
			END_SHOP_CUTSCENE()
			
			IF NETWORK_IS_GAME_IN_PROGRESS()
				HIDE_NET_SCISSORS()
				STOP_NETWORK_SYNC_SCENE_FOR_SHOP(sData)
				sData.sCurrentKeeperAnim = ""
				
				IF NETWORK_DOES_ENTITY_EXIST_WITH_NETWORK_ID(serverBD.scissorsID)
					IF NETWORK_HAS_CONTROL_OF_NETWORK_ID(serverBD.scissorsID)
						SET_ENTITY_VISIBLE(NET_TO_ENT(serverBD.scissorsID), FALSE)
						SET_ENTITY_COLLISION(NET_TO_ENT(serverBD.scissorsID), FALSE)
						FREEZE_ENTITY_POSITION(NET_TO_ENT(serverBD.scissorsID), TRUE)
					ENDIF	
				ENDIF
			ELSE
				RELEASE_PED_PRELOAD_PROP_DATA(PLAYER_PED_ID())
				RELEASE_PED_PRELOAD_VARIATION_DATA(PLAYER_PED_ID())
			ENDIF
			
			// Tell the shop keeper to to get in position
			IF NOT IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)
				CLEAR_PED_TASKS(sData.sShopInfo.sShopKeeper.pedID)
				SHOPWORKER_GO_TO_IDLE_POS(sData, FALSE)
				SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(sData.sShopInfo.sShopKeeper.pedID, TRUE)
			ENDIF
			
			IF g_bTriggerShopSave
				IF g_bInMultiplayer
					// KGM 11/12/12: Tell all players to re-Generate a player headshot after appearance has changed
					BROADCAST_GENERAL_EVENT(GENERAL_EVENT_TYPE_PLAYER_CHANGED_APPEARANCE, ALL_PLAYERS())
				ENDIF
			ENDIF
			
			SET_PLAYER_IS_CHANGING_CLOTHES(FALSE)
			
			// Update stats incase we changed clothes
			IF NETWORK_IS_GAME_IN_PROGRESS()
			AND NOT IS_PED_INJURED(PLAYER_PED_ID())
				IF GB_IS_LOCAL_PLAYER_MEMBER_OF_A_GANG(TRUE)
				AND GB_OUTFITS_IS_PLAYER_WEARING_GANG_OUTFIT(PLAYER_ID())
					// block
					PRINTLN("kr_debug: blocking call to SAVE_PLAYERS_CLOTHES as player is wearing goon outfit")
				ELSE
					SAVE_PLAYERS_CLOTHES(PLAYER_PED_ID())
				ENDIF
			ENDIF
			
			IF sData.sShopInfo.eShop != HAIRDO_SHOP_CASINO_APT
			AND IS_SHOP_KEEPER_SAFE_TO_USE(PLAYER_ID(), sData.eInShop, TRUE)
				REMOVE_USE_OF_SHOP_KEEPER(PLAYER_ID(), sData.eInShop)
			ENDIF
			
			IF sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT
			AND IS_SHOP_KEEPER_SAFE_TO_USE_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_ID(), sData.eInShop, FALSE)
				REMOVE_USE_OF_SHOP_KEEPER_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_ID(), sData.eInShop)
			ENDIF
			
			IF NOT g_bInMultiplayer
				SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceSkinCharacterCloth, FALSE)
			ENDIF
			
			// Dont do auto trigger until we leave shop.
			sData.bDoInstantLocateTrigger = FALSE
			sData.sBrowseInfo.bBrowseTrigger = FALSE
			sData.sBrowseInfo.bBrowsing = FALSE
			sData.sShopInfo.eStage = SHOP_STAGE_WAIT_FOR_TRIGGER
		BREAK
	ENDSWITCH
	
	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF DOES_ENTITY_EXIST(sData.sCutsceneData.pedID)
		AND NOT IS_PED_INJURED(sData.sCutsceneData.pedID)
		AND IS_ENTITY_VISIBLE_TO_SCRIPT(sData.sCutsceneData.pedID)
			SET_LOCAL_PLAYER_INVISIBLE_LOCALLY()
		ENDIF
	ENDIF

	// Keep players buddies out the way whilst we browse shop
	UPDATE_BUDDIES_FOR_SHOP_CUTSCENE(sData.sBuddyHideData)
ENDPROC

FUNC BOOL IS_HAIRDRESSER_SERVING_SOMEONE(HAIRDO_SHOP_STRUCT &sData)

	IF NETWORK_IS_GAME_IN_PROGRESS()
		IF sData.sShopInfo.eStage <> SHOP_STAGE_WAIT_FOR_TRIGGER
		OR (sData.sShopInfo.eShop != HAIRDO_SHOP_CASINO_APT AND NOT IS_SHOP_KEEPER_SAFE_TO_USE(PLAYER_ID(), sData.eInShop, FALSE))
		OR (sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT AND NOT IS_SHOP_KEEPER_SAFE_TO_USE_FOR_PARTICIPANT_OF_THIS_SCRIPT(PLAYER_ID(), sData.eInShop, FALSE))
			RETURN TRUE
		ENDIF
	ENDIF
	
	RETURN FALSE
ENDFUNC

PROC DO_PROCESS_NET_DOORS(HAIRDO_SHOP_STRUCT &sData)
	// Proces door locking in MP
	IF sData.sShopInfo.bDataSet
	AND NETWORK_IS_GAME_IN_PROGRESS()
	
		// Let all the other scripts know what state we are in.
		playerBD[NATIVE_TO_INT(PLAYER_ID())].iShopProperties = g_sShopSettings.iProperties[sData.sShopInfo.eShop]
		
		// Only allow the door states to be set by the host once every 2 seconds
		IF NETWORK_IS_HOST_OF_THIS_SCRIPT()
		
			GRAB_CONTROL_OF_SHOP_DOORS(sData.sShopInfo.eShop)
			
			IF sData.bDoorTimerSet
				IF IS_TIME_MORE_THAN(GET_NETWORK_TIME(), sData.tdDoorTimer)
					// Lock the doors in MP if the shop is closed and all players have left.
					BOOL bLockDoors = TRUE
					IF NOT IS_BIT_SET(g_sShopSettings.iProperties[sData.sShopInfo.eShop], SHOP_NS_FLAG_SHOP_OPEN)
					AND NOT IS_BIT_SET(g_sShopSettings.iProperties[sData.sShopInfo.eShop], SHOP_NS_FLAG_PLAYER_IN_SHOP)
					AND (NOT DOES_ENTITY_EXIST(sData.sShopInfo.sShopKeeper.pedID) OR IS_PED_INJURED(sData.sShopInfo.sShopKeeper.pedID)) // if closed for host only dont close for everyone
						INT iPlayer
						REPEAT NUM_NETWORK_PLAYERS iPlayer
							IF IS_NET_PLAYER_OK(INT_TO_PLAYERINDEX(iPlayer))
								IF (IS_BIT_SET(playerBD[iPlayer].iShopProperties, SHOP_NS_FLAG_PLAYER_IN_SHOP))
									bLockDoors = FALSE
									iPlayer = NUM_NETWORK_PLAYERS+1// Bail
								ENDIF
							ENDIF
						ENDREPEAT
					ELSE
						bLockDoors = FALSE
					ENDIF
					
					//2400200
					IF MPGlobalsAmbience.bKeepShopDoorsOpen
						bLockDoors = FALSE
						CPRINTLN(DEBUG_NET_AMBIENT, "=== AM_PASS_THE_PARCEL === Vehicle in hairdo shop, unlocking doors.")
					ENDIF
					
					LOCK_SHOP_DOORS_MP(sData.sShopInfo.eShop, bLockDoors)
					sData.bDoorTimerSet = FALSE
				ENDIF
			ELSE
				sData.tdDoorTimer = GET_TIME_OFFSET(GET_NETWORK_TIME(), 2000)
				sData.bDoorTimerSet = TRUE
			ENDIF
		ELSE
			sData.tdDoorTimer = GET_NETWORK_TIME()
			sData.bDoorTimerSet = TRUE
		ENDIF
	ENDIF
ENDPROC


#IF IS_DEBUG_BUILD
PROC DRAW_DEBUG_HAIRDO_TEXT(HAIRDO_SHOP_STRUCT &sData, TEXT_LABEL_63 str, INT &iStrOffset, HUD_COLOURS hudColour = HUD_COLOUR_SIMPLEBLIP_DEFAULT, INT iColumn = 0)
	IF NOT sData.bDrawDebugStuff
		EXIT
	ENDIF
	IF sData.sShopInfo.eStage = SHOP_STAGE_INITIALISE
		EXIT
	ENDIF
	
	INT iRed, iGreen, iBlue, iAlpha
	IF (hudColour = HUD_COLOUR_SIMPLEBLIP_DEFAULT)
		GET_HUD_COLOUR(HUD_COLOUR_BLUE, iRed, iGreen, iBlue, iAlpha)
	ELSE
		GET_HUD_COLOUR(hudColour, iRed, iGreen, iBlue, iAlpha)
	ENDIF
	
	IF sData.sShopInfo.eStage != SHOP_STAGE_BROWSE_ITEMS
		FLOAT fCONST_MIN		= 10.0
		FLOAT fCONST_MAX		= 50.0
		IF (sData.eInShop = HAIRDO_SHOP_CASINO_APT)
			fCONST_MIN			= 5.0
			fCONST_MAX			= 15.0
		ENDIF
		
		FLOAT fDist = GET_DISTANCE_BETWEEN_PLAYER_AND_SHOP(sData.sShopInfo.eShop)
		FLOAT fAlphaMult = 1.0 -((fDist-fCONST_MIN)/(fCONST_MAX-fCONST_MIN))
		
		IF (fAlphaMult > 1.0)
			fAlphaMult = 1.0
		ENDIF
		IF (fAlphaMult <= 0.0)
			fAlphaMult = 0.0
			EXIT
		ENDIF
		iAlpha = ROUND(TO_FLOAT(iAlpha) * fAlphaMult)
		
	ENDIF
	
	VECTOR vOffsetBase = <<0.3,0.1,0>>, vOffsetMult = <<0.0,0.010,0>>, vColumnMult = <<0.2,0.0,0>>
	IF USE_SERVER_TRANSACTIONS()
		vOffsetMult = <<0.0,0.015,0>>
	ENDIF
	
	DRAW_DEBUG_TEXT_2D(str, vOffsetBase+(vOffsetMult*TO_FLOAT(iStrOffset))+(vColumnMult*TO_FLOAT(iColumn)), iRed, iGreen, iBlue, iAlpha)
	
	iStrOffset += 1
ENDPROC

PROC DRAW_DEBUG_MP_HAIRDO_INFO(HAIRDO_SHOP_STRUCT &sData, INT &iStrOffset)
	IF NOT NETWORK_IS_GAME_IN_PROGRESS()
		EXIT
	ENDIF
	IF NOT sData.bDrawDebugStuff
		EXIT
	ENDIF
	
	IF IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCasinoPropertyData.iBS2Casino, PROPERTY_BROADCAST_BS2_CASINO_LUX_SUITE_A_ARCADE_REQUEST_TO_PLAY)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCasinoPropertyData.iBS2Casino, PROPERTY_BROADCAST_BS2_CASINO_LUX_SUITE_B_ARCADE_REQUEST_TO_PLAY)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCasinoPropertyData.iBS2Casino, PROPERTY_BROADCAST_BS2_CASINO_LUX_SUITE_C_ARCADE_REQUEST_TO_PLAY)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCasinoPropertyData.iBS2Casino, PROPERTY_BROADCAST_BS2_CASINO_LUX_SUITE_D_ARCADE_REQUEST_TO_PLAY)
	OR IS_BIT_SET(GlobalplayerBD_FM[NATIVE_TO_INT(PLAYER_ID())].propertyDetails.sCasinoPropertyData.iBS2Casino, PROPERTY_BROADCAST_BS2_CASINO_LUX_SUITE_E_ARCADE_REQUEST_TO_PLAY)
		EXIT
	ENDIF

	MODEL_NAMES ePedModel = GET_ENTITY_MODEL(PLAYER_PED_ID())
	INT i
	NGMP_MENU_OPTION_DATA sOptionData
	RAMP_TYPE eDebugRampType
	TEXT_LABEL_63 str
	str  = "eStage: "
	SWITCH sData.sShopInfo.eStage
		CASE SHOP_STAGE_INITIALISE			str += "INITIALISE" BREAK
		CASE SHOP_STAGE_WAIT_FOR_ENTRY		str += "WAIT_FOR_ENTRY" BREAK
		CASE SHOP_STAGE_ENTRY_INTRO			str += "ENTRY_INTRO" BREAK
		CASE SHOP_STAGE_WAIT_FOR_TRIGGER	str += "WAIT_FOR_TRIGGER" BREAK
		CASE SHOP_STAGE_BROWSE_ITEMS		str += "BROWSE_ITEMS" BREAK
		CASE SHOP_STAGE_LEAVE				str += "LEAVE" BREAK
		CASE SHOP_STAGE_IDLE				str += "IDLE" BREAK
		
		DEFAULT
			str += "SHOP_STAGE_"
			str += ENUM_TO_INT(sData.sShopInfo.eStage)
		BREAK
	ENDSWITCH
	DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, HUD_COLOUR_RED)
	
	str  = "iCurrentItem: "
	str += sData.sBrowseInfo.iCurrentItem
	str += ", iTint["
	str += sData.iTint1
	str += ", "
	str += sData.iTint2
	str += "], fBlend: "
	str += ROUND(sData.fBlend*100.0)
	str += ", eRamp: "
	str += GET_RAMP_TYPE_TEXT(sData.eRampType)
	DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, HUD_COLOUR_RED)
	
	str  = "iCurrentHairdo: "
	str += sData.iCurrentHairdo
	str += ", iCurrentBeard: "
	str += sData.iCurrentBeard
	str += ", iSavedTint2: "
	str += sData.iSavedTint2
	DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, HUD_COLOUR_RED)
	
	//saved items
	str  = "saved hair: "
	str += GET_PACKED_STAT_INT(PACKED_CHAR_CURRENT_HAIRCUT)
	str += ", tint1: "
	str += SAFE_GET_MP_INT_CHARACTER_STAT(MP_STAT_HAIR_TINT)
	str += ", tint2: "
	str += SAFE_GET_MP_INT_CHARACTER_STAT(MP_STAT_SEC_HAIR_TINT)
	DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, HUD_COLOUR_ORANGE)
	
	IF ePedModel = MP_M_FREEMODE_01
		str  = "saved beard: "
		str += GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_FACIAL_HAIR))
		str += ", tint1: "
		str += GET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY((HOS_FACIAL_HAIR), eDebugRampType)
		str += " ["
		str += GET_RAMP_TYPE_TEXT(eDebugRampType)
		str += "]"
//		str += ", tint2: "
//		str += GET_MP_CHARACTER_SECONDARY_COLOUR_FROM_HEAD_OVERLAY(HOS_FACIAL_HAIR)
		str += ", blend: "
		str += ROUND(GET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(HOS_FACIAL_HAIR))*100.0)
		DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, HUD_COLOUR_ORANGE)
	ENDIF
	
	str  = "saved eyebrows: "
	str += GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_EYEBROW))
	str += ", tint1: "
	str += GET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY((HOS_EYEBROW), eDebugRampType)
	str += " ["
	str += GET_RAMP_TYPE_TEXT(eDebugRampType)
	str += "]"
//	str += ", tint2: "
//	str += GET_MP_CHARACTER_SECONDARY_COLOUR_FROM_HEAD_OVERLAY(HOS_EYEBROW))
	str += ", blend: "
	str += ROUND(GET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(HOS_EYEBROW))*100.0)
	DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, HUD_COLOUR_ORANGE)
	
	str  = "saved makeup: "
	str += GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_MAKEUP))
//	str += ", tint1: "
//	str += GET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY((HOS_MAKEUP))
//	str += ", tint2: "
//	str += GET_MP_CHARACTER_SECONDARY_COLOUR_FROM_HEAD_OVERLAY(HOS_MAKEUP))
	str += ", blend: "
	str += ROUND(100.0*GET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(HOS_MAKEUP)))
	DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, HUD_COLOUR_ORANGE)
	
	str  = "saved lipstick: "
	str += GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_SKIN_DETAIL_1))
	str += ", tint1: "
	str += GET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY((HOS_SKIN_DETAIL_1), eDebugRampType)
	str += ", tint2: "
	str += GET_MP_CHARACTER_SECONDARY_COLOUR_FROM_HEAD_OVERLAY(HOS_SKIN_DETAIL_1)
	str += ", blend: "
	str += ROUND(100.0*GET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(HOS_SKIN_DETAIL_1)))
	DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, HUD_COLOUR_ORANGE)

	str  = "saved blusher: "
	str += GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_BLUSHER))
	str += ", tint1: "
	str += GET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY((HOS_BLUSHER), eDebugRampType)
	str += " ["
	str += GET_RAMP_TYPE_TEXT(eDebugRampType)
	str += "]"
//	str += ", tint2: "
//	str += GET_MP_CHARACTER_SECONDARY_COLOUR_FROM_HEAD_OVERLAY(HOS_BLUSHER))
	str += ", blend: "
	str += ROUND(100.0*GET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(HOS_BLUSHER)))
	DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, HUD_COLOUR_ORANGE)
	
	IF ePedModel = MP_M_FREEMODE_01
		str  = "saved chest hair: "
		str += GET_PACKED_STAT_INT(GET_CHARACTER_OVERLAY_STAT(HOS_BODY_1))
		str += ", tint1: "
		str += GET_MP_CHARACTER_COLOUR_FROM_HEAD_OVERLAY((HOS_BODY_1), eDebugRampType)
		str += " ["
		str += GET_RAMP_TYPE_TEXT(eDebugRampType)
		str += "]"
		str += ", tint2: "
		str += GET_MP_CHARACTER_SECONDARY_COLOUR_FROM_HEAD_OVERLAY(HOS_BODY_1)
	//	str += ", blend: "
	//	str += ROUND(100.0*GET_MP_FLOAT_CHARACTER_STAT(GET_CHARACTER_OVERLAY_BLEND_STAT(HOS_BODY_1)))
		DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, HUD_COLOUR_ORANGE)
	ENDIF
	
	str  = "saved eyes: "
	str += GET_STRING_FROM_FLOAT(GET_MP_FLOAT_CHARACTER_STAT(MP_STAT_FEATURE_20))
	DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, HUD_COLOUR_ORANGE)
	
	//available tints
	str  = "iTints["
	str += sData.iMAX_TINT_COUNT
	str += "]"
	REPEAT GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu) i
		IF IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, i, sData.eInShop)
			INT iStrLength = GET_LENGTH_OF_LITERAL_STRING(str)+4
			IF (iStrLength >= 64)
				DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, HUD_COLOUR_YELLOW)
				str  = "    "
			ELSE
				IF i = 0
					str += ": "
				ELSE
					str += ", "
				ENDIF
			ENDIF
			
			str += i
		ENDIF
	ENDREPEAT
	DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, HUD_COLOUR_YELLOW)
	
	//unavailable tints
	str  = "iTints["
	str += GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu) - sData.iMAX_TINT_COUNT
	str += "]"
	REPEAT GET_NUMBER_OF_NGMP_TINT_OPTIONS(ePedModel, sData.eCurrentMenu) i
		IF NOT IS_TINT_AVAILABLE_FOR_NGMP_ITEM(sData.eCurrentMenu, i, sData.eInShop)
			INT iStrLength = GET_LENGTH_OF_LITERAL_STRING(str)+4
			IF (iStrLength >= 64)
				DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, HUD_COLOUR_YELLOW)
				str  = "    "
			ELSE
				IF i = 0
					str += ": "
				ELSE
					str += ", "
				ENDIF
			ENDIF
			
			str += i
		ENDIF
	ENDREPEAT
	DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, HUD_COLOUR_YELLOW)
	
	//zoom stuff
	str  = ""
	IF sData.bAllowZoomControl
		str += "allow zoom"
	ELSE
		str += "DON'T allow zoom"
	ENDIF
	str += ", fCamZoomAlpha: "
	str += GET_STRING_FROM_FLOAT(sData.fCamZoomAlpha)
	DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, HUD_COLOUR_GREEN)
	
	//clothes unlock stats
	REPEAT GET_NUMBER_OF_NGMP_MENU_OPTIONS(ePedModel, sData.eCurrentMenu) i
		IF GET_NGMP_MENU_OPTION_DATA(ePedModel, sData.eCurrentMenu, i, sOptionData, GET_HAIRDO_SHOP_KEY_VARIANT(sData.eInShop))
			HUD_COLOURS eHudColour = HUD_COLOUR_BLUE
			str  = "  "
			IF sOptionData.eUnlockItem != DUMMY_CLOTHES_UNLOCK
				IF IS_MP_CLOTHES_OWNED(sOptionData.eUnlockItem)
					str += "\""
					str += GET_STRING_FROM_TEXT_FILE(sOptionData.tlLabel)
					str += "\" owned"
					eHudColour = HUD_COLOUR_BLUELIGHT
				ELSE
					str += "\""
					str += GET_STRING_FROM_TEXT_FILE(sOptionData.tlLabel)
					str += "\" NOT owned"
					eHudColour = HUD_COLOUR_BLUEDARK
				ENDIF
			ELSE
				str += "\""
				str += GET_STRING_FROM_TEXT_FILE(sOptionData.tlLabel)
				str += "\" doesn't have owned stat"
				eHudColour = HUD_COLOUR_BLUE
			ENDIF
			DRAW_DEBUG_HAIRDO_TEXT(sData, str, iStrOffset, eHudColour)
		ENDIF
	ENDREPEAT
	
ENDPROC
#ENDIF

PROC HAIRDO_SHOP_MAIN_UPDATE(HAIRDO_SHOP_STRUCT &sData)

	UPDATE_SHOP_KEEPER_PED_ID(sData.sShopInfo.sShopKeeper)

	//B*2293786: Disable the interior occulsion this frame if the player is in the shop
	IF sData.sShopInfo.bPlayerInShop
		DISABLE_OCCLUSION_THIS_FRAME()
	ENDIF

	// ----------------------Main Shop Loop------------------------
	IF IS_SHOP_WITHIN_ACTIVATION_RANGE(sData.eInShop)
	AND NOT SHOULD_SHOP_FORCE_CLEANUP(sData.sShopInfo, sData.eInShop)
	
		IF IS_SHOP_SCRIPT_SAFE_TO_PROCESS(sData.sShopInfo)
				
			ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(FRONTEND_CONTROL)
			ALLOW_ALTERNATIVE_SCRIPT_CONTROLS_LAYOUT(PLAYER_CONTROL)
			
			// Set penthouse stylist as open
			INITIALISE_DLC_STYLIST(sData.eInShop)
		
			// common updates
			IF NOT (sData.sShopInfo.eStage = SHOP_STAGE_ENTRY_INTRO)
			AND NOT IS_HAIRDRESSER_SERVING_SOMEONE(sData)
				INT iDummyKickingOffPed = 0
				UPDATE_SHOP_KICKING_OFF_STATE(sData.sShopInfo, iDummyKickingOffPed)
			ENDIF
			UPDATE_SHOP_IDLE_STATE(sData.sShopInfo, sData.sBrowseInfo)
			IF sData.sShopInfo.bDataSet	
				IF NOT IS_PLAYER_KICKING_OFF_IN_SHOP(sData.sShopInfo.eShop)
					UPDATE_SHOP_INPUT_BLOCKS(sData.sShopInfo, sData.sBrowseInfo, sData.bDontBlockCutsceneSkipButton)
				ELSE
					CLEANUP_SOFA(sData)
					IF NOT (sData.sShopInfo.eStage = SHOP_STAGE_ENTRY_INTRO)
					AND NOT IS_HAIRDRESSER_SERVING_SOMEONE(sData)
						DO_HAIRDRESSER_KICK_OFF(sData)
					ENDIF
				ENDIF
			ENDIF
			PROCESS_FINALISE_HEAD_BLEND(sData)
			PROCESS_HAIRDO_SHOP_DIALOGUE(sData)
			IF sData.sShopInfo.eShop = HAIRDO_SHOP_CASINO_APT
				SERVER_MAINTAIN_SHOP_KEEPER_USAGE_FOR_PARTICIPANT_OF_THIS_SCRIPT()			
			ENDIF	
			MANAGE_NET_SCISSORS(sData)
			// Perform the main processing
			SWITCH sData.sShopInfo.eStage
				CASE SHOP_STAGE_INITIALISE
					DO_INITIALISE(sData)
				BREAK
				
				CASE SHOP_STAGE_WAIT_FOR_ENTRY
				
					PROCESS_END_SHOPPING_SAVE(FALSE)
					
					UNLOAD_SHOP_BROWSE_ASSETS(sData)
					CHECK_HAIRDO_SHOP_ENTRY(sData)

					IF IS_HAIRDO_SHOP_INTRO_NEEDED(sData)
						// Run intro stage instantly.
						IF sData.sShopInfo.eStage = SHOP_STAGE_ENTRY_INTRO
							sData.bDoInstantLocateTrigger = TRUE
							DO_ENTRY_INTRO(sData)
						ENDIF
					ELSE
						// only do the peds look at player stuff if we're not doing the intro
						DO_PEDS_LOOK_AT_PLAYER(sData)
					ENDIF
				BREAK
				
				CASE SHOP_STAGE_ENTRY_INTRO
					DO_ENTRY_INTRO(sData)
					UPDATE_SHOP_BROWSE_STATE(sData.sShopInfo, sData.sBrowseInfo)
				BREAK
				
				CASE SHOP_STAGE_WAIT_FOR_TRIGGER
					LOAD_SHOP_BROWSE_ASSETS(sData, TRUE)
					DO_PEDS_LOOK_AT_PLAYER(sData)
					CHECK_HAIRDO_LOCATE_ENTRY(sData)
					CHECK_HAIRDO_SHOP_EXIT(sData)
					DO_SOFA(sData)
				BREAK
				
				CASE SHOP_STAGE_BROWSE_ITEMS
					DO_BROWSE_ITEMS(sData)
					UPDATE_SHOP_BROWSE_STATE(sData.sShopInfo, sData.sBrowseInfo)
				BREAK
				
				CASE SHOP_STAGE_LEAVE
					//Go straight to the trigger stage now. 
					sData.sShopInfo.eStage = SHOP_STAGE_WAIT_FOR_TRIGGER
				BREAK
			ENDSWITCH
			
			#IF IS_DEBUG_BUILD
			INT iStrOffset = 0
			DRAW_DEBUG_MP_HAIRDO_INFO(sData, iStrOffset)
			#ENDIF
		ENDIF
	ELSE
		// cleanup shop when the player goes out of range
		// or the shop cleanup flag gets set
		CLEANUP_SHOP(sData)
	ENDIF
	
	// Force a reset
	IF SHOULD_SHOP_FORCE_RESET(sData.sShopInfo)
		RESET_SHOP(sData)
	ENDIF
	
	DO_PROCESS_NET_DOORS(sData)
	
ENDPROC
