//////////////////////////////////////////////////////////////////////////////////////////
//																						//
//		SCRIPT NAME		:	hairdo_shop_mp.sc											//
//		AUTHOR			:	Kenneth Ross / Andrew Minghella								//
//		DESCRIPTION		:	An example shop that should be used as a guide for setting	//
//							up a new shop.												//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

USING "rage_builtins.sch"
USING "globals.sch"
USING "hairdo_shop_core.sch"

HAIRDO_SHOP_STRUCT sData

SCRIPT(SHOP_LAUNCHER_STRUCT sShopLauncherData)

	#IF IS_DEBUG_BUILD
		OUTPUT_SHOP_SCRIPT_LAUNCH_INFO(sShopLauncherData)
	#ENDIF
	 
	sData.sShopInfo.bLinkedShop = sShopLauncherData.bLinkedShop
	sData.sShopInfo.iContextID = NEW_CONTEXT_INTENTION
	sData.eInShop = sShopLauncherData.eShop
	 
	#IF IS_DEBUG_BUILD SETUP_HAIRDO_DEBUG_WIDGETS(sData) #ENDIF
	
	IF NOT sData.sShopInfo.bLinkedShop
		SET_BIT(g_sShopSettings.iProperties[sShopLauncherData.eShop], SHOP_NS_FLAG_SHOP_SCRIPT_RUNNING)
	ENDIF
	
	// MP setup
	sData.fpIsPedCompItemCurrent = &IS_PED_COMP_ITEM_CURRENT_MP
	sData.fpGetPedCompItemCurrent = &GET_PED_COMP_ITEM_CURRENT_MP
	sData.fpSetPedCompItemCurrent = &SET_PED_COMP_ITEM_CURRENT_MP
	sData.fpIsPedCompItemAvailable = &IS_PED_COMP_ITEM_AVAILABLE_MP
	sData.fpIsPedCompItemAcquired = &IS_PED_COMP_ITEM_ACQUIRED_MP
	sData.fpSetPedCompItemAcquired = &SET_PED_COMP_ITEM_ACQUIRED_MP
	sData.fpIsPedCompItemNew = &IS_PED_COMPONENT_ITEM_NEW_MP
	sData.fpGetPedCompDataForItem = &GET_PED_COMP_DATA_FOR_ITEM_MP
	sData.fpSetupClothingItemForShop = &SETUP_CLOTHING_ITEM_FOR_SHOP_MP
	
	NETWORK_SET_THIS_SCRIPT_IS_NETWORK_SCRIPT(NUM_NETWORK_PLAYERS, FALSE, sShopLauncherData.iNetInstanceID)
	
	// This makes sure the net script is active, waits untull it is.
	IF NOT HANDLE_NET_SCRIPT_INITIALISATION(FALSE, -1, TRUE)
		CLEANUP_SHOP(sData)
	ENDIF
	
	NETWORK_REGISTER_HOST_BROADCAST_VARIABLES(serverBD, SIZE_OF(serverBD))
	NETWORK_REGISTER_PLAYER_BROADCAST_VARIABLES(playerBD, SIZE_OF(playerBD))
	
	RESERVE_NETWORK_MISSION_PEDS(2)
	
	SET_THIS_SCRIPT_CAN_BE_PAUSED(FALSE)
	
	// KGM: Wait for the first network broadcast before moving on - call this as the last instruction in pre-game
	// (after Initialisation and Broadcast Variable Registration - Hosts can do this too)
	IF NOT Wait_For_First_Network_Broadcast()
		CPRINTLN(DEBUG_SHOPS, GET_THIS_SCRIPT_NAME(), ": Failed to receive initial network broadcast for ", GET_SHOP_NAME(sShopLauncherData.eShop), ". Cleaning up.")
		CLEANUP_SHOP(sData)
	ENDIF
	
	REGISTER_SHOP_DOORS_MP(sData.eInShop)
	
	INITIALISE_COMMERCE_STORE_TRIGGER_DATA(sStoreTriggerData, sData.eInShop)
	
	// Main loop
	WHILE TRUE
	
		// One wait to rule them all. This can be the ONLY wait from here on in.....
		WAIT(0)
		
		#IF IS_DEBUG_BUILD MAINTAIN_HAIRDO_DEBUG_WIDGETS(sData) #ENDIF
		
		HAIRDO_SHOP_MAIN_UPDATE(sData)
		
	ENDWHILE
	
ENDSCRIPT

