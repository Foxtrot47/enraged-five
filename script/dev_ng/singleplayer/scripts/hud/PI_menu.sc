//////////////////////////////////////////////////////////////////////////////////////////
// Name:        PI_MENU                                                                 //
// Description: Runs the Player Interaction Menu                                        //
// Written by:  Craig Vincent                                                           //
//                                                                                      //
//////////////////////////////////////////////////////////////////////////////////////////

USING "globals.sch"

// Game Headers
USING "net_race_to_point.sch"
USING "net_friend_request.sch"
using "mission_titles_private.sch"
using "cellphone_public.sch"

//////////////////////////////////////////////////////////////////////////////////////////
//                                      ENUMS                                           //  
//////////////////////////////////////////////////////////////////////////////////////////
 
ENUM ENUM_PI_MENU_STATE
    PI_MENU_INACTIVE,
    PI_MENU_ACTIVE,
    PI_MENU_CLEANUP
ENDENUM
ENUM ENUM_FACE_CAM_STAGE
    FCS_WAIT,
    FCS_INIT,
    FCS_RUNNING,
    FCS_CLEANUP
ENDENUM
ENUM ENUM_FACE_SHAPE_TEST_STAGE
    FCST_INIT,
    FCST_WAITING_RESULT,
    FCST_RESULT_READY
ENDENUM
ENUM ENUM_GPS_LIST
    eGPS_NONE,  
    eGPS_HOME,  
    eGPS_MISSION1,
    eGPS_MISSION2,
    eGPS_MISSION3,
    eGPS_MISSION4,
    eGPS_MISSION5,
    eGPS_MISSION6,
    eGPS_MISSION7,  
    eGPS_MISSION8,
    eGPS_ATM,       
    eGPS_GUN,               
    eGPS_MOD,               
    eGPS_LowCLOTHES,
    eGPS_medCLOTHES,
    eGPS_highCLOTHES,
    eGPS_BARBER,    
    eGPS_TATTOO,
    eGPS_MASK,
    eGPS_GARAGE,
    eGPS_MAX
ENDENUM
ENUM_GPS_LIST eCurrentGPS

ENUM ENUM_SELFIE
    //Michael
    eself_finger = 0    
    ,eself_neck
    ,eSelf_MAX_MIKE
    
    //Franklin
    ,eself_bump  = 0
    ,eself_WestCoast
    ,eSelf_MAX_FRANK
        
    //Trevor
    ,eself_ProudFinger = 0  
    ,eself_SlitThroat
    ,eSelf_MAX_TREV
        
ENDENUM


//////////////////////////////////////////////////////////////////////////////////////////
//                                      CONSTANTS                                       //  
//////////////////////////////////////////////////////////////////////////////////////////
CONST_INT       PI_MENU_DELAY               350
CONST_INT       MENU_UPDATE_TIME            2000
CONST_INT       iPI_MOVE_DELAY              175
CONST_INT       MAX_MENUS                   10


CONST_INT       ciPI_SUB_MENU_NONE          0
CONST_INT       ciPI_SUB_MENU_INVENTORY     1
CONST_INT       ciPI_SUB_MENU_OBJECTIVE     2
CONST_INT       ciPI_SUB_MENU_HELPTEXT      3
CONST_INT       MAX_SUBMENUS                4

CONST_INT       ciPI_TYPE_QUICK_GPS         0
CONST_INT       ciPI_TYPE_INVENTORY         1
CONST_INT       ciPI_TYPE_OBJECTIVE         2
CONST_INT       ciPI_TYPE_HELPTEXT          3
#IF FEATURE_SP_DLC_DIRECTOR_MODE
CONST_INT       ciPI_TYPE_DIRECTOR          4
CONST_INT       ciPI_TYPE_SELFIE            5
CONST_INT       ciPI_TYPE_MAX               6 
#ENDIF
#IF not FEATURE_SP_DLC_DIRECTOR_MODE
CONST_INT       ciPI_TYPE_SELFIE            4
CONST_INT       ciPI_TYPE_MAX               5 
#ENDIF
  

CONST_INT       ciPI_TYPE_M2_HATS           0
CONST_INT       ciPI_TYPE_M2_GLASSES        1
CONST_INT       ciPI_TYPE_M2_MASKS          2  
CONST_INT       MAX_MENU_SELECTIONS         6

#IF FEATURE_SP_DLC_DIRECTOR_MODE
CONST_INT       ciPI_MENU_MOVE_MAX          5
#ENDIF
#IF not FEATURE_SP_DLC_DIRECTOR_MODE
CONST_INT       ciPI_MENU_MOVE_MAX          4
#ENDIF

CONST_INT       ciPI_INVENTORY_TYPE_MAX     3
CONST_INT       ciPI_BRIEF_TYPE_MAX         1

CONST_INT       MAX_MASKS   26
CONST_INT       MAX_GLASS   76
CONST_INT       MAX_HATS    40

//////////////////////////////////////////////////////////////////////////////////////////
//                                      STRUCTS                                         //  
//////////////////////////////////////////////////////////////////////////////////////////

STRUCT PI_LAUNCHER_MENU_STRUCT
    INT                         iSelection[MAX_MENUS][MAX_SUBMENUS]
    TEXT_LABEL_15               tl15
    INT                         iCurrentSelection
ENDSTRUCT
STRUCT PI_SELECTED_ITEMS
    INT iSelection[MAX_MENUS][MAX_MENU_SELECTIONS]
    INT iSelectionChangeID
ENDSTRUCT
STRUCT PI_MENU_HEADER_DATA
    TEXT_STYLE      PlayerNameStyle
    TEXT_PLACEMENT  PlayerNamePlace 
    RECT            Background  
ENDSTRUCT
//STRUCT COMP_DATA
//    PED_COMP_TYPE_ENUM eType = COMP_TYPE_PROPS  
//    PED_COMP_NAME_ENUM eName = DUMMY_PED_COMP
//ENDSTRUCT


//////////////////////////////////////////////////////////////////////////////////////////
//                                      BITSETS                                         //  
//////////////////////////////////////////////////////////////////////////////////////////
INT             iBoolsBitSet    =   0   
CONST_INT       biSwitchButtonCheck         0
CONST_INT       biM_MenuSetup               1
CONST_INT       biEnableFaceCam             2

//////////////////////////////////////////////////////////////////////////////////////////
//                                      VARIABLES                                       //  
//////////////////////////////////////////////////////////////////////////////////////////
    
ENUM_PI_MENU_STATE              ePI_MENU_STATE          = PI_MENU_INACTIVE

PI_LAUNCHER_MENU_STRUCT         sPI_MenuData
PI_MENU_HEADER_DATA             sPIM_HeaderData 
PI_SELECTED_ITEMS               sSelection

COMP_DATA                       MaskData[MAX_MASKS]
COMP_DATA                       GlassesData[MAX_GLASS]  
COMP_DATA                       HatsData[MAX_HATS]      

//menu scale
float       fMenuExtension      =  CUSTOM_MENU_W
//resolution
INT iOldScrX
INT iOldScrY

//menu header
string  sGraphicHeader
bool    bHeaderGraphicRequested

//timers
INT         iPi_menu_timer
INT         iPi_submenu_timer
INT         iPI_MoveTimer 
INT         iMenuUpdateTimer
INT         ijustClosedTimer
//INT 		iScrollTimer

//menu positions
INT         iStoredSelection
INT         iSubMenu                = ciPI_SUB_MENU_NONE
    
//bools
bool        bResetMenuNow           = TRUE
BOOL        bUseSelectArrows        = FALSE
BOOL        bSelectAvailable        = FALSE
bool        bSwitchMenuState        = FALSE
BOOL		bFirstTimeHit			= TRUE

FLOAT 		fPreviousAspectRatio

#IF FEATURE_SP_DLC_DIRECTOR_MODE
bool		bDirectorWarningScreen			= FALSE
#endif

//Face cam  
ENUM_FACE_CAM_STAGE             eFaceCamStage           = FCS_WAIT
ENUM_FACE_SHAPE_TEST_STAGE      eFaceCamShapeTestStage  = FCST_INIT 
SHAPETEST_INDEX                 sti_FaceCam
Bool                            b_FaceCamShapeTestHitSomething
CAMERA_INDEX                    FaceCam

TWEAK_FLOAT                     FC_FOV      50.0
FLOAT                           fXpos 
FLOAT                           fYpos
VECTOR                          vFCAttachOffset         = <<0, 0.7, 0>>
VECTOR                          vFCPointOffset
float                           fCamMoveSpeed =  0.0006//0.0002


//Selfie 
INT     iSelfieStage
int     iCurrentSelfie              = 0
int     iFastSelectSelfie           = 0
bool    bdoAnim                     = FALSE
BOOL    bChangeAnim                 = FALSE

//brief scaleform 
TWEAK_FLOAT                     sf_x        	0.113	//0.120//0.196
FLOAT                    		sf_y        = 	0.754
TWEAK_FLOAT                     sf_w        	0.225	//0.240//0.225
TWEAK_FLOAT                     sf_h        	1.000	//0.225

FLOAT							sf_y_adj	=	8.00	//	B* 2147964 Adjustment to the y depending on the aspect ratio (Magic number)

TWEAK_FLOAT                     wl_x       		0.1125	//.188
FLOAT                     		wl_y      	=	0.253
TWEAK_FLOAT                     wl_w       		0.225
TWEAK_FLOAT                     wl_h       		0.003	//0.225

TWEAK_FLOAT                     txt_x      		0.182
TWEAK_FLOAT                     txt_y      		0.107

#if IS_DEBUG_BUILD
WIDGET_GROUP_ID     wGroup
int                 iwidgetMenuState
#endif

SCALEFORM_INDEX         sfMenu

 //Items
INT             iMaskTotalCount
INT             iGlassesTotalCount
INT             iHatTotalCount

INT             ciPI_MAX_TYPE_MASKS         = 0 // Vary depending on how many items are unlocked.
INT             ciPI_MAX_TYPE_GLASSES       = 0 
INT             ciPI_MAX_TYPE_HATS          = 0 
INT             ciPI_MAX_GPS                = 2 // will always include <none>, ATM's and savehouse from start


//char test
enumCharacterList echarPrev
enumCharacterList echarCurr

enumCharacterList eSelfiecharPrev
enumCharacterList eSelfiecharCurr

// PC mouse
BOOL bCursorAccept
BOOL bCursorCancel
BOOL bCursorScrollUp
BOOL bCursorScrollDown
BOOL bCursorQuitMenu
INT iCursorValueChange
BOOL bCursorMenuFirstTime
BOOL bCursorCamMove

#IF FEATURE_SP_DLC_DIRECTOR_MODE
//Director Mode blocking
FEATURE_BLOCK_REASON lastBlockReason = FBR_NONE
#ENDIF

//////////////////////////////////////////////////////////////////////////////////////////
//                              PROCEDURES AND FUNCTIONS                                //  
//////////////////////////////////////////////////////////////////////////////////////////
///    
///
///    
///    

// request the sf movie
FUNC BOOL HAVE_SF_ASSETS_LOADED()
    sfMenu = REQUEST_SCALEFORM_MOVIE_WITH_IGNORE_SUPER_WIDESCREEN("TEXTFIELD")       
    IF HAS_SCALEFORM_MOVIE_LOADED(sfMenu)
        RETURN TRUE
    ENDIF
    RETURN FALSE    
ENDFUNC










//Added by Steve T. 26.0.1.15
//Dan made a director mode fix for the selector wheel that has impacted how GET_CURRENT_PLAYER_PED_ENUM() works in Director Mode. It doesn't return the correct player ped enum.
//It's hard to achieve the effect of his fix another way, so we're modifying the scripts impacted to get the model before returning an enum.  
//Primarily updating this script to get the Selfie Snapmatic actions working in Director Mode.
FUNC EnumCharacterList Get_Player_Director_Mode_Compatible()

    IF IS_CURRENTLY_ON_MISSION_OF_TYPE (MISSION_TYPE_DIRECTOR) //the player enum isn't returned correctly in Director mode, so we go for an explicit model check.


        IF NOT IS_ENTITY_DEAD (PLAYER_PED_ID())

            IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = g_savedGlobals.sCharSheetData.g_CharacterSheet[CHAR_MICHAEL].game_model //Check the explicit model from the character sheet.
                RETURN CHAR_MICHAEL
            ELSE

                IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = g_savedGlobals.sCharSheetData.g_CharacterSheet[CHAR_FRANKLIN].game_model
                    RETURN CHAR_FRANKLIN
                ELSE

                    IF GET_ENTITY_MODEL(PLAYER_PED_ID()) = g_savedGlobals.sCharSheetData.g_CharacterSheet[CHAR_TREVOR].game_model
                        RETURN CHAR_TREVOR
                    ELSE
                    
                        /*
                        #if IS_DEBUG_BUILD
                            cdPrintstring("PI_menu.sc - Get_Player_Director_Mode_Compatible() is returning an abnormal player model in Director Mode. Returning CHAR_MULTIPLAYER for safety.")
                            cdPrintnl()
                        #endif
                        */

                        RETURN CHAR_MULTIPLAYER //Default to Multiplayer phone as a failsafe. PI_Menu doesn't look to be checking for anyone outwith the standard three SP playable flow characters.
                        
                    ENDIF

                ENDIF

            ENDIF

        ELSE

            RETURN CHAR_MULTIPLAYER

        ENDIF


    ELSE

        //Not in Director Mode, safe to just go ahead with the standard player enum retrieval process.
        RETURN GET_CURRENT_PLAYER_PED_ENUM()

    ENDIF

ENDFUNC















FUNC VECTOR RETURN_ATM_COORDS_BY_INDEX(INT iATM)
    SWITCH iATM
        CASE 0
            RETURN <<2558.324 , 350.988  ,  107.5975>>
        CASE 1
            RETURN <<-301.6573, -829.5886,  31.41977>>
        CASE 2
            RETURN <<-303.2257, -829.3121,  31.41977>>
        CASE 3
            RETURN <<-204.0193, -861.0091,  29.27133>>
        CASE 4
            RETURN <<118.6416 , -883.5695,  30.13945>>
        CASE 5
            RETURN <<24.5933  , -945.543 ,  28.33305>>
        CASE 6
            RETURN <<5.686035 , -919.9551,  28.48088>>
        CASE 7
            RETURN <<296.1756 , -896.2318,  28.29015>>
        CASE 8
            RETURN <<296.8775 , -894.3196,  28.26148>>
        CASE 9
            RETURN <<147.4731 , -1036.218,  28.38707>>
        
        CASE 10
            RETURN<<145.8392    ,-1035.625, 28.38707>>
        CASE 11
            RETURN <<-256.6386  ,-715.8899, 32.7883 >>
        CASE 12
            RETURN <<112.4762   ,-819.8081, 30.33955>>
        CASE 13
            RETURN <<-259.2767  ,-723.2651, 32.70155>>
        CASE 14
            RETURN <<-254.5219  ,-692.8869, 32.57825>>
        CASE 15
            RETURN <<-27.89034  ,-724.1089, 43.22287>>
        CASE 16
            RETURN <<-30.09956  ,-723.2863, 43.22287>>
        CASE 17
            RETURN <<111.3886   ,-774.8402, 30.43766>>
        CASE 18
            RETURN <<114.5474   ,-775.972,  30.41737>>
        CASE 19
            RETURN <<289.53     ,-1256.788, 28.44057>>
            
        CASE 20
            RETURN <<289.2679   ,-1282.32   ,28.65519>>
        CASE 21
            RETURN <<158.7965   ,234.7452   ,105.6433>>
        CASE 22
            RETURN <<228.0324   ,337.8501   ,104.5013>>
        CASE 23
            RETURN <<527.7776   ,-160.6609  ,56.13671>>
        CASE 24
            RETURN <<-57.17029  ,-92.37918  ,56.75069>>
        CASE 25
            RETURN <<285.3485   ,142.9751   ,103.1623>>
        CASE 26
            RETURN <<357.1284   ,174.0836   ,102.0597>>
        CASE 27
            RETURN <<89.81339   ,2.880329   ,67.35216>>
        CASE 28
            RETURN <<1077.779   ,-776.9664  ,57.25652>>
        CASE 29
            RETURN <<1137.811   ,-468.8625  ,65.69865>>
            
        CASE 30 
            RETURN <<1167.06    ,-455.6541  ,65.81857>>
        CASE 31
            RETURN <<-165.5844  ,234.7659   ,93.92897>>
        CASE 32
            RETURN <<-165.5844  ,232.6955   ,93.92897>>
        CASE 33
            RETURN <<-1205.378  ,-326.5286  ,36.85104>>
        CASE 34
            RETURN <<-1206.142  ,-325.0316  ,36.85104>>
        CASE 35
            RETURN <<-846.6537  ,-341.509   ,37.6685 >>
        CASE 36
            RETURN <<-847.204   ,-340.4291  ,37.6793 >>
        CASE 37
            RETURN <<-720.6288  ,-415.5243  ,33.97996>>
        CASE 38
            RETURN <<-867.013   ,-187.9928  ,36.88218>>
        CASE 39
            RETURN <<-867.9745  ,-186.3419  ,36.88218>>
            
        CASE 40 
            RETURN <<-1415.48   ,-212.3324  ,45.49542>>
        CASE 41
            RETURN <<-1430.663  ,-211.3587  ,45.47162>>
        CASE 42
            RETURN <<-1410.736  ,-98.92789  ,51.39701>>
        CASE 43
            RETURN <<-1410.183  ,-100.6454  ,51.39652>>
        CASE 44
            RETURN <<-1282.098  ,-210.5599  ,41.43031>>
        CASE 45
            RETURN <<-1286.704  ,-213.7827  ,41.43031>>
        CASE 46
            RETURN <<-1289.742  ,-227.165   ,41.43031>>
        CASE 47
            RETURN <<-1285.136  ,-223.9422  ,41.43031>>
        CASE 48
            RETURN <<-1569.84   ,-547.0309  ,33.93216>>
        CASE 49
            RETURN <<-1570.765  ,-547.7035  ,33.93216>>
        
        //CASE 50
        //  RETURN <<-1305.708  ,-706.6881  ,24.31447>>
        CASE 50
            RETURN <<-1315.416  ,-834.431   ,15.95233>>
        CASE 51
            RETURN <<-1314.466  ,-835.6913  ,15.95233>>
        CASE 52
            RETURN <<-2071.928  ,-317.2862  ,12.31808>>
        CASE 53
            RETURN <<-821.8936  ,-1081.555  ,10.13664>>
        CASE 54
            RETURN <<-1110.228  ,-1691.154  ,3.378483>>
        //CASE 56
        //  RETURN <<-1044.466  ,-2739.641  ,8.12406 >>
        CASE 55
            RETURN <<-712.9357  ,-818.4827  ,22.74066>>
        CASE 56
            RETURN <<-710.0828  ,-818.4756  ,22.73634>>
        CASE 57
            RETURN <<-617.8035  ,-708.8591  ,29.04321>>
        CASE 58 
            RETURN <<-617.8035  ,-706.8521  ,29.04321>>
        CASE 59
            RETURN <<2564       ,2584.553   ,37.06807>>
        
        CASE 60
            RETURN <<-3241.455  ,997.9085   ,11.66582>> 
        CASE 61
            RETURN <<-614.5187  ,-705.5981  ,30.224  >>
        CASE 62
            RETURN <<-611.8581  ,-705.5981  ,30.224  >>
        CASE 63
            RETURN <<-660.6763  ,-854.4882  ,23.45663>>
        CASE 64
            RETURN <<-537.8052  ,-854.9357  ,28.27543>>
        CASE 65
            RETURN <<-594.6144  ,-1160.852  ,21.33351>>
        CASE 66
            RETURN <<-596.1251  ,-1160.85   ,21.3336 >>
        CASE 67
            RETURN <<-526.7791  ,-1223.374  ,17.45272>>
        CASE 68
            RETURN <<156.1886   ,6643.2     ,30.59372>>
        CASE 69
            RETURN <<173.8246   ,6638.217   ,30.59372>>
            
        CASE 70 
            RETURN <<-386.4596  ,6046.411   ,30.47399>>
        CASE 71
            RETURN <<-282.7141  ,6226.43    ,30.49648>>
        CASE 72
            RETURN <<-132.6663  ,6366.876   ,30.47258>>
        CASE 73
            RETURN <<-95.87029  ,6457.462   ,30.47394>>
        CASE 74
            RETURN <<-97.63721  ,6455.732   ,30.46793>>
        CASE 75
            RETURN <<1687.395   ,4815.9     ,41.00647>>
        CASE 76
            RETURN <<1700.694   ,6426.762   ,31.63297>>
        CASE 77
            RETURN <<1822.971   ,3682.577   ,33.26745>>
        CASE 78
            RETURN <<1171.523   ,2703.139   ,37.1477 >>
        CASE 79
            RETURN <<1172.457   ,2703.139   ,37.1477 >>
        
        CASE 80 
            RETURN <<-2956.848  ,487.2158   ,14.478 >>
        CASE 81
            RETURN <<-2958.977  ,487.3071   ,14.478 >>
        CASE 82
            RETURN <<-2974.586  ,380.1269   ,14.32212>>
        CASE 83
            RETURN <<-1091.887  ,2709.053   ,17.91941>>
        CASE 84
            RETURN <<-2295.853  ,357.9348   ,173.6014>>
        CASE 85
            RETURN <<-2295.069  ,356.2556   ,173.6014>>
        CASE 86
            RETURN <<-2294.3    ,354.6056   ,173.6014>>
        CASE 87
            RETURN <<-3043.835  ,594.1639   ,6.732796>>
        CASE 88
            RETURN <<-3144.887  ,1127.811   ,19.83804>>
            
    ENDSWITCH
    RETURN <<158.7965   ,234.7452   ,105.6433>> //<<0,0,0>>
ENDFUNC
FUNC string GET_STRAND_LABEL(SP_MISSIONS emission)

    switch emission
        case SP_HEIST_JEWELRY_PREP_1A
            return "M_JHP1A"
        break
        case SP_HEIST_JEWELRY_PREP_1B
            return "M_JHP1B"
        break
        case SP_HEIST_JEWELRY_PREP_2A
            return "M_JHP2A"
        break
        case SP_HEIST_DOCKS_PREP_1
            return "M_DHP1"
        break
        case SP_HEIST_DOCKS_PREP_2B
            return "M_DHP2b"
        break
        case SP_HEIST_FINALE_PREP_A
            return "M_FHPRA"
        break
        case SP_HEIST_FINALE_PREP_B
            return "M_FHPRB"
        break
        case SP_HEIST_FINALE_PREP_D
            return "PIM_TRAIN"
        break
        case SP_MISSION_FBI_4_PREP_1
            return "M_FB4P1"
        break
        case SP_MISSION_FBI_4_PREP_2
            return "M_FB4P2"
        break
        case SP_MISSION_FBI_4_PREP_3
            return "M_FB4P3"
        break
        case SP_MISSION_FBI_4_PREP_4
            return "M_FB4P4"
        break
        case SP_MISSION_FBI_4_PREP_5
            return "M_FB4P5"
        break
        case SP_MISSION_MICHAEL_2
            return "PIM_FRANK"// michael mission actually start with F blip.
        break
        case SP_MISSION_TREVOR_1
            return "PIM_MIKE"
        break
        case SP_MISSION_FAMILY_3
            if GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
                return "PIM_MIKE"
            endif
        break
    endswitch
    
    STRANDS eStrand = GET_STORY_MISSION_STRAND(emission)
    
    switch eStrand
        case STRAND_AGENCY_HEIST
            return "PIM_AGEN"
        break
        case STRAND_ARMENIAN
            return "PIM_ARM"
        break
        case STRAND_ASSASSINATIONS
            return "PIM_ASSA"
        break
        case STRAND_CAR_STEAL
            return "PIM_CARS" 
        break
        case STRAND_CHINESE
            return "PIM_CHIN"
        break
        case STRAND_DOCKS_HEIST
            return "PIM_DOCKS"
        break
        case STRAND_DOCKS_HEIST_2
            return "PIM_DOCKS"
        break
        case STRAND_EXILE
            return "PIM_EXIL"
        break
        case STRAND_FAMILY
            return "PIM_FAM"
        break
        case STRAND_FBI_OFFICERS
            return "PIM_FIB"
        break
        case STRAND_FBI_OFFICERS_2
            return "PIM_FIB"
        break
        case STRAND_FBI_OFFICERS_3
            return "PIM_FIB"
        break
        case STRAND_FBI_OFFICERS_4
            return "PIM_FIB"
        break
        case STRAND_FBI_OFFICERS_5
            return "PIM_FIB"
        break
        case STRAND_FINALE
            return "PIM_FINAL"
        break
        case STRAND_FINALE_HEIST
            return "PIM_BIGS"
        break
        case STRAND_FINALE_HEIST_2
            return "PIM_BIGS"
        break
        case STRAND_FINALE_HEIST_3
            return "PIM_BIGS"
        break
        case STRAND_FINALE_HEIST_4
            return "PIM_BIGS"
        break
        case STRAND_FRANKLIN
            return "PIM_FRANK"
        break
        case STRAND_JEWEL_HEIST
            return "PIM_JEWEL"
        break
        case STRAND_JEWEL_HEIST_2
            return "PIM_JEWEL"
        break
        case STRAND_LAMAR
            return "PIM_LAM"
        break
        case STRAND_LESTER
            return "PIM_LEST"
        break
        case STRAND_MARTIN
            return "PIM_MART"
        break
        case STRAND_MICHAEL
            return "PIM_MIKE"
        break
        case STRAND_MICHAEL_EVENTS
            return ""
        break
        case STRAND_PROLOGUE
            return ""
        break
        case STRAND_RURAL_BANK_HEIST
            return "PIM_RURAL"
        break
        case STRAND_SHRINK
            return "PIM_SHRINK"
        break
        case STRAND_SOLOMON
            return "PIM_SOL"
        break
        case STRAND_TREVOR
            return "PIM_TREV"
        break
    endswitch
    
    return ""
ENDFUNC
FUNC string GET_SHORT_LABEL(string sCurrentLabel)
    
    if  ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_H2")
        return "PIM_P0_H2"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E4_6")
        return "PIM_P0_E4_6"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E5")
        return  "PIM_P0_E5"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E5_1")           
        return  "PIM_P0_E5_1"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E5_2")
        return "PIM_P0_E5_2"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E5_4")
        return "PIM_P0_E5_4"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E5_5")
        return "PIM_P0_E5_5"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E5_6")
        return "PIM_P0_E5_6"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E5_7")
        return "PIM_P0_E5_7"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E5_8")
        return "PIM_P0_E5_8"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E5_9")
        return "PIM_P0_E5_9"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E6")
        return "PIM_P0_E6"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E6_1")
        return "PIM_P0_E6_1"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E6_5")
        return"PIM_P0_E6_5" 
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E6_8")
        return"PIM_P0_E6_8" 
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E8_6")
        return "PIM_P0_E8_6"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E8_7")
        return  "PIM_P0_E8_7"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E9_1")
        return  "PIM_P0_E9_1"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P0_E9_4")
        return  "PIM_P0_E9_4"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_E1_5")
        return  "PIM_P1_E1_5"   
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_E4_0")
        return  "PIM_P1_E4_0"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_E4_1")
        return  "PIM_P1_E4_1"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_E7_0")
        return  "PIM_P1_E7_0"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_E7_1")
        return  "PIM_P1_E7_1"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_E7_3")
        return  "PIM_P1_E7_3"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_E8_5")
        return  "PIM_P1_E8_5"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_E8_7")
        return  "PIM_P1_E8_7"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_E9_2")
        return  "PIM_P1_E9_2"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_E9_7")
        return  "PIM_P1_E9_7"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_E10_3")
        return  "PIM_P1_E10_3"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_E10_4")
        return  "PIM_P1_E10_4"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_H26_4")
        return  "PIM_P1_H26_4"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_H19_0")
        return  "PIM_P1_H19_0"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_H19_3")
        return  "PIM_P1_H19_3"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_H19_5")
        return  "PIM_P1_H19_5"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_H19_10")
        return  "PIM_P1_H19_10"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_H19_12")
        return  "PIM_P1_H19_12"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_H19_13")
        return  "PIM_P1_H19_13"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P1_H19_14")
        return  "PIM_P1_H19_14"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E1")
        return  "PIM_P2_E1"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E3")
        return  "PIM_P2_E3"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E3_6")
        return  "PIM_P2_E3_6"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E3_7")
        return  "PIM_P2_E3_7"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E3_8")
        return  "PIM_P2_E3_8"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E8_4")
        return  "PIM_P2_E8_4"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E8_5")
        return  "PIM_P2_E8_5"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E8_6")
        return  "PIM_P2_E8_6"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E9_1")
        return  "PIM_P2_E9_1"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E9_2")
        return  "PIM_P2_E9_2"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E9_3")
        return  "PIM_P2_E9_3"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E9_5")
        return  "PIM_P2_E9_5"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E9_6")
        return  "PIM_P2_E9_6"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E9_7")
        return  "PIM_P2_E9_7"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E9_8")
        return  "PIM_P2_E9_8"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E9_9")
        return  "PIM_P2_E9_9"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E10_3")
        return  "PIM_P2_E10_3"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E10_7")
        return  "PIM_P2_E10_7"
    elif ARE_STRINGS_EQUAL(sCurrentLabel,"PROPS_P2_E10_8")
        return  "PIM_P2_E10_8"      
    endif

    
CPRINTLN(DEBUG_MISSION, "GET_SHORT_LABEL: Using orginal text label: ",sCurrentLabel )   
return sCurrentLabel

ENDFUNC
FUNC VECTOR GET_CLOSEST_ATM_COORDS()
    
        //**TWH - CMcM - #1383555 - Added all supplied ATM coords to quick GPS.
    CONST_INT MAX_NUMBER_OF_ATMs 89
    
    FLOAT fNewDistance
    FLOAT fShortestDistance = 9999.9
    VECTOR vNearest = <<-717.7181, -915.6580, 18.2156>>
    INT i
    REPEAT MAX_NUMBER_OF_ATMs i
        fNewDistance = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(PLAYER_PED_ID()), RETURN_ATM_COORDS_BY_INDEX(i))
        
        IF fNewDistance < fShortestDistance
            fShortestDistance = fNewDistance
            vNearest = RETURN_ATM_COORDS_BY_INDEX(i)
        ENDIF 
    ENDREPEAT
    
    RETURN vNearest
ENDFUNC

//PURPOSE: Returns the Comp Name based on the players gender
FUNC PED_COMP_NAME_ENUM GET_PROP_NAME_OF_TYPE(INT iType,int iPropNum,PED_COMP_TYPE_ENUM CompType = COMP_TYPE_PROPS)
    // Glasses are listed seperately 
    IF iType = ciPI_TYPE_M2_GLASSES
        INT iNewPropnum
        
        IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL     
            iNewPropnum = enum_to_int(PROPS_P0_GLASSES)+ iPropNum
            
            if iNewPropnum = enum_to_int(PROPS_P0_EXTERMINATOR_MASK) //If scuba mask go one extra
            or iNewPropnum = enum_to_int(PROPS_P0_SCUBA_MASK)
                RETURN DUMMY_PED_COMP
            endif
            
            if iNewPropnum > enum_to_int(PROPS_P0_GLASSES_THICK_RIM_7)          
                RETURN DUMMY_PED_COMP
            else
                RETURN int_to_enum(PED_COMP_NAME_ENUM,iNewPropnum)
            endif
        elif GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
            iNewPropnum = enum_to_int(PROPS_P1_SCUBA_MASK)+ iPropNum            
            if iNewPropnum > enum_to_int(PROPS_P1_SUNGLASSES_I_6)
                RETURN DUMMY_PED_COMP
            else
                RETURN int_to_enum(PED_COMP_NAME_ENUM,iNewPropnum)
            endif
        elif GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
            iNewPropnum = enum_to_int(PROPS_P2_SCUBA_MASK)+ iPropNum
            if iNewPropnum > enum_to_int(PROPS_P2_SQUARE_GLASSES_9)
                RETURN DUMMY_PED_COMP
            else
                RETURN int_to_enum(PED_COMP_NAME_ENUM,iNewPropnum)
            endif
        endif
                
    //Hats and Masks    
    ELSE        
        
        PED_COMP_NAME_ENUM  ePropListEnd
        INT                 iNewPropnum
        int                 ICurrentProp    = 0
        enumCharacterList   eCurrentchar    = GET_CURRENT_PLAYER_PED_ENUM()
        MODEL_NAMES         charModel       = GET_PLAYER_PED_MODEL(eCurrentchar)
        
        IF eCurrentchar = CHAR_MICHAEL          
            if compType = COMP_TYPE_PROPS
                iNewPropnum     = enum_to_int(PROPS_P0_FIREMAN_HAT)     //start of head props for mike  
                ePropListEnd    = PROPS_P0_FLIGHT_CAP                   //end of head props for mike                
            
            elif compType = COMP_TYPE_SPECIAL2
                iNewPropnum     = enum_to_int(SPECIAL2_P0_WRESTLER_MASK_0)      //start of SPECIAL2 for mike    
                ePropListEnd    = SPECIAL2_P0_WRESTLER_MASK_5                   //end of SPECIAL2 for mike              
            endif       
        ELIF eCurrentchar = CHAR_FRANKLIN   
            if compType = COMP_TYPE_PROPS
                iNewPropnum     = enum_to_int(PROPS_P1_HOCKEY_MASK)     //start of head props for frank
                ePropListEnd    = PROPS_P1_FLIGHT_CAP                   //end of head props for frank               
            
            elif compType = COMP_TYPE_SPECIAL2
                iNewPropnum     = enum_to_int(SPECIAL2_P1_WRESTLER_MASK_0)      //start of SPECIAL2 for frank
                ePropListEnd    = SPECIAL2_P1_WRESTLER_MASK_5                   //end of SPECIAL2 for frank             
            endif
        ELIF eCurrentchar = CHAR_TREVOR
            if compType = COMP_TYPE_PROPS
                iNewPropnum     = enum_to_int(PROPS_P2_FIREMAN_HAT)     //start of head props for trev
                ePropListEnd    = PROPS_P2_CAP_15                       //end of head props for trev                
            
            elif compType = COMP_TYPE_SPECIAL2              
                iNewPropnum     = enum_to_int(SPECIAL2_P2_WRESTLER_MASK_0)      //start of SPECIAL2 for trev
                ePropListEnd    = SPECIAL2_P2_MASK_MONSTER_RED                  //end of SPECIAL2 for trev              
            endif
        endif
        
            //Cycle through list of head props          
            WHILE iNewPropnum <= enum_to_int(ePropListEnd)
                // If looking for a hat
                IF iType = ciPI_TYPE_M2_HATS                    
                    if IS_ITEM_A_HAT(charModel,compType,int_to_enum(PED_COMP_NAME_ENUM,iNewPropnum))                    
                        if eCurrentchar = CHAR_TREVOR AND ( iNewPropnum != enum_to_int(PROPS_P2_BEANIE_HAT_1))// block this no name hat
                            if ICurrentProp = iPropNum
                                //This is the hat we are looking for
                                RETURN int_to_enum(PED_COMP_NAME_ENUM,iNewPropnum)                      
                            else
                                //Increase amount of hats cycled through
                                ICurrentProp++  
                            endif
                        elif eCurrentchar = CHAR_FRANKLIN AND ( iNewPropnum != enum_to_int(PROPS_P1_BUGSTAR_CAP)) // block this no name hat
                            if ICurrentProp = iPropNum
                                //This is the hat we are looking for
                                RETURN int_to_enum(PED_COMP_NAME_ENUM,iNewPropnum)                      
                            else
                                //Increase amount of hats cycled through
                                ICurrentProp++  
                            endif
                        elif eCurrentchar = CHAR_MICHAEL
                            if ICurrentProp = iPropNum
                                //This is the hat we are looking for
                                RETURN int_to_enum(PED_COMP_NAME_ENUM,iNewPropnum)                      
                            else
                                //Increase amount of hats cycled through
                                ICurrentProp++  
                            endif
                        endif
                    endif   
                elif iType = ciPI_TYPE_M2_MASKS             
                    if IS_ITEM_A_MASK(charModel,compType,int_to_enum(PED_COMP_NAME_ENUM,iNewPropnum))
                        if ICurrentProp = iPropNum
                            //This is the hat we are looking for
                            RETURN int_to_enum(PED_COMP_NAME_ENUM,iNewPropnum)                      
                        else
                            //Increase amount of hats cycled through
                            ICurrentProp++  
                        endif
                    endif                       
                endif
                iNewPropnum++
            ENDWHILE    
            
        //No current hats
        if iNewPropnum > enum_to_int(ePropListEnd)
            RETURN DUMMY_PED_COMP
        endif   
    ENDIF
    
    RETURN DUMMY_PED_COMP
ENDFUNC

PROC GET_COMP_DATA()
    INT i
    MODEL_NAMES PlayerModel = GET_PLAYER_PED_MODEL(GET_CURRENT_PLAYER_PED_ENUM())
    
    //Masks
    REPEAT MAX_MASKS i
                
        IF IS_PED_COMP_ITEM_ACQUIRED_SP(PlayerModel, COMP_TYPE_SPECIAL2, GET_PROP_NAME_OF_TYPE(ciPI_TYPE_M2_MASKS,(i-1),COMP_TYPE_SPECIAL2))
            MaskData[iMaskTotalCount].eType = COMP_TYPE_SPECIAL2
            MaskData[iMaskTotalCount].eName = GET_PROP_NAME_OF_TYPE(ciPI_TYPE_M2_MASKS,(i-1),COMP_TYPE_SPECIAL2)
            CPRINTLN(DEBUG_MISSION, "MASK2 COMP_TYPE_SPECIAL2---CV:GET_COMP_DATA: IS_PED_COMP_ITEM_ACQUIRED_SP: ",i)
            
            IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL2, GET_PROP_NAME_OF_TYPE(ciPI_TYPE_M2_MASKS,(i-1),COMP_TYPE_SPECIAL2))
                CPRINTLN(DEBUG_MISSION, "MASK2 COMP_TYPE_SPECIAL2---CV:GET_COMP_DATA: IS_PED_COMP_ITEM_CURRENT_SP: ",i) 
                sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_MASKS] = iMaskTotalCount
            ENDIF
            
            iMaskTotalCount++
            ciPI_MAX_TYPE_MASKS = (iMaskTotalCount-1)
        ENDIF   
        IF IS_PED_COMP_ITEM_ACQUIRED_SP(PlayerModel, COMP_TYPE_PROPS, GET_PROP_NAME_OF_TYPE(ciPI_TYPE_M2_MASKS,(i-1),COMP_TYPE_PROPS))
            MaskData[iMaskTotalCount].eType = COMP_TYPE_PROPS
            MaskData[iMaskTotalCount].eName = GET_PROP_NAME_OF_TYPE(ciPI_TYPE_M2_MASKS,(i-1),COMP_TYPE_PROPS)
            CPRINTLN(DEBUG_MISSION, "MASK COMP_TYPE_PROPS---CV:GET_COMP_DATA: IS_PED_COMP_ITEM_ACQUIRED_SP: ",i)
            
            IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, GET_PROP_NAME_OF_TYPE(ciPI_TYPE_M2_MASKS,(i-1),COMP_TYPE_PROPS))
                CPRINTLN(DEBUG_MISSION, "MASK COMP_TYPE_PROPS---CV:GET_COMP_DATA: IS_PED_COMP_ITEM_CURRENT_SP: ",i) 
                sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_MASKS] = iMaskTotalCount
            ENDIF
            
        
            iMaskTotalCount++
            ciPI_MAX_TYPE_MASKS = (iMaskTotalCount-1)
        ENDIF   
        
        IF i = 0
            MaskData[iMaskTotalCount].eType = COMP_TYPE_PROPS
            MaskData[iMaskTotalCount].eName = PROPS_HEAD_NONE
            CPRINTLN(DEBUG_MISSION, "masks---CV:GET_COMP_DATA:PROPS_HEAD_NONE")
            iMaskTotalCount++
            ciPI_MAX_TYPE_MASKS = (iMaskTotalCount-1)
        endif
    ENDREPEAT
    
    //balaclava check for franklin
    IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN 
        IF IS_PED_COMP_ITEM_ACQUIRED_SP(PlayerModel, COMP_TYPE_SPECIAL, SPECIAL_P1_MASK)
            MaskData[iMaskTotalCount].eType = COMP_TYPE_SPECIAL
            MaskData[iMaskTotalCount].eName = SPECIAL_P1_MASK
            CPRINTLN(DEBUG_MISSION, "MASK2 COMP_TYPE_SPECIAL---CV:GET_COMP_DATA: SPECIAL_P1_MASK ACQUIRED")
            
            IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL, SPECIAL_P1_MASK)
                CPRINTLN(DEBUG_MISSION, "MASK2 COMP_TYPE_SPECIAL2---CV:GET_COMP_DATA: SPECIAL_P1_MASK EQUIPPED ")   
                sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_MASKS] = iMaskTotalCount
            ENDIF
            
            iMaskTotalCount++
            ciPI_MAX_TYPE_MASKS = (iMaskTotalCount-1)
        ENDIF
    ENDIF
    
    CPRINTLN(DEBUG_MISSION, "MASKS max : ",iMaskTotalCount)
    //Glasses
    REPEAT MAX_GLASS i
        IF i > 0
            IF IS_PED_COMP_ITEM_ACQUIRED_SP(PlayerModel, COMP_TYPE_PROPS, GET_PROP_NAME_OF_TYPE(ciPI_TYPE_M2_GLASSES,(i-1)))
                GlassesData[iGlassesTotalCount].eType = COMP_TYPE_PROPS
                GlassesData[iGlassesTotalCount].eName = GET_PROP_NAME_OF_TYPE(ciPI_TYPE_M2_GLASSES,(i-1))
                CPRINTLN(DEBUG_MISSION, "GLASS---CV:GET_COMP_DATA: ACQUIRED_SP: ",i)
                
                IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, GET_PROP_NAME_OF_TYPE(ciPI_TYPE_M2_GLASSES,(i-1)))
                    CPRINTLN(DEBUG_MISSION, "GLASS---CV:GET_COMP_DATA: CURRENT_SP: ",i)
                    sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_GLASSES] = iGlassesTotalCount
                ENDIF
                iGlassesTotalCount++
                ciPI_MAX_TYPE_GLASSES = (iGlassesTotalCount-1)
            ENDIF
        ELSE
            GlassesData[iGlassesTotalCount].eType = COMP_TYPE_PROPS
            GlassesData[iGlassesTotalCount].eName = PROPS_EYES_NONE
            CPRINTLN(DEBUG_MISSION, "Glasses---CV:GET_COMP_DATA:PROPS_HEAD_NONE")
            iGlassesTotalCount++
            ciPI_MAX_TYPE_GLASSES = (iGlassesTotalCount-1)
        ENDIF
    ENDREPEAT
    
    //Hats
    REPEAT MAX_HATS i
        IF i > 0
            IF IS_PED_COMP_ITEM_ACQUIRED_SP(PlayerModel, COMP_TYPE_PROPS, GET_PROP_NAME_OF_TYPE(ciPI_TYPE_M2_HATS,(i-1)))
                HatsData[iHatTotalCount].eType = COMP_TYPE_PROPS
                HatsData[iHatTotalCount].eName = GET_PROP_NAME_OF_TYPE(ciPI_TYPE_M2_HATS,(i-1))
                CPRINTLN(DEBUG_MISSION, "Hats---CV:GET_COMP_DATA: ACQUIRED_SP: ",i)
                
                IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_PROPS, GET_PROP_NAME_OF_TYPE(ciPI_TYPE_M2_HATS,(i-1)))
                    CPRINTLN(DEBUG_MISSION, "Hats---CV:GET_COMP_DATA: CURRENT_SP: ",i)
                    sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_HATS] = iHatTotalCount
                ENDIF
                iHatTotalCount++
                ciPI_MAX_TYPE_HATS = (iHatTotalCount-1)
            ENDIF
        ELSE
            HatsData[iHatTotalCount].eType = COMP_TYPE_PROPS
            HatsData[iHatTotalCount].eName = PROPS_HEAD_NONE
            CPRINTLN(DEBUG_MISSION, "Hats---CV:GET_COMP_DATA:PROPS_HEAD_NONE")  
            iHatTotalCount++
            ciPI_MAX_TYPE_HATS = (iHatTotalCount-1)
        ENDIF
    ENDREPEAT
    
ENDPROC
//PURPOSE: Returns the Max size of the current menu
FUNC INT GET_MENU_MAX()

    SWITCH iSubMenu
        CASE    ciPI_SUB_MENU_NONE                  RETURN ciPI_MENU_MOVE_MAX
        CASE    ciPI_SUB_MENU_INVENTORY             RETURN ciPI_INVENTORY_TYPE_MAX
        CASE    ciPI_SUB_MENU_OBJECTIVE             RETURN ciPI_BRIEF_TYPE_MAX
        CASE    ciPI_SUB_MENU_HELPTEXT              RETURN ciPI_BRIEF_TYPE_MAX
    ENDSWITCH
    
    RETURN ciPI_TYPE_MAX
ENDFUNC
//PURPOSE: Returns the max options for the current selection
FUNC INT GET_SELECTION_MAX_PI(INT iSelection)
    IF iSubMenu = ciPI_SUB_MENU_NONE
        SWITCH iSelection   
            CASE ciPI_TYPE_QUICK_GPS        RETURN ciPI_MAX_GPS
            CASE ciPI_TYPE_SELFIE       
                if Get_Player_Director_Mode_Compatible() = CHAR_MICHAEL
                    RETURN enum_to_int(eSelf_MAX_MIKE)-1
                    
                elif Get_Player_Director_Mode_Compatible() = CHAR_FRANKLIN   
                    RETURN enum_to_int(eSelf_MAX_FRANK)-1
                    
                elif Get_Player_Director_Mode_Compatible() = CHAR_TREVOR    
                    RETURN enum_to_int(eSelf_MAX_TREV)-1
                    
                endif   
            BREAK   
        ENDSWITCH   
    ELIF iSubMenu = ciPI_SUB_MENU_INVENTORY
        SWITCH iSelection
            CASE ciPI_TYPE_M2_HATS          RETURN ciPI_MAX_TYPE_HATS
            CASE ciPI_TYPE_M2_GLASSES       RETURN ciPI_MAX_TYPE_GLASSES
            CASE ciPI_TYPE_M2_MASKS         RETURN ciPI_MAX_TYPE_MASKS
        ENDSWITCH
		
    ENDIF
    RETURN 0
ENDFUNC
FUNC SHOP_NAME_ENUM GET_CLOSEST_LOWENDSHOP()
    
    FLOAT fCurrentDist
    FLOAT fShortDist = 999999.99    
    vector vCoords = GET_ENTITY_COORDS(player_ped_id())
    SHOP_NAME_ENUM eshop =CLOTHES_SHOP_L_01_SC
    
    fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, GET_SHOP_COORDS(CLOTHES_SHOP_L_01_SC))
    IF fCurrentDist < fShortDist    
        fShortDist = fCurrentDist
        eShop = CLOTHES_SHOP_L_01_SC
    ENDIF
    fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, GET_SHOP_COORDS(CLOTHES_SHOP_L_02_GS))
    IF fCurrentDist < fShortDist    
        fShortDist = fCurrentDist
        eShop = CLOTHES_SHOP_L_02_GS
    ENDIF
    fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, GET_SHOP_COORDS(CLOTHES_SHOP_L_03_DT))
    IF fCurrentDist < fShortDist    
        fShortDist = fCurrentDist
        eShop = CLOTHES_SHOP_L_03_DT
    ENDIF
    fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, GET_SHOP_COORDS(CLOTHES_SHOP_L_04_CS))
    IF fCurrentDist < fShortDist    
        fShortDist = fCurrentDist
        eShop = CLOTHES_SHOP_L_04_CS
    ENDIF
    fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, GET_SHOP_COORDS(CLOTHES_SHOP_L_05_GSD))
    IF fCurrentDist < fShortDist    
        fShortDist = fCurrentDist
        eShop = CLOTHES_SHOP_L_05_GSD
    ENDIF
    fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, GET_SHOP_COORDS(CLOTHES_SHOP_L_06_VC))
    IF fCurrentDist < fShortDist    
        fShortDist = fCurrentDist
        eShop = CLOTHES_SHOP_L_06_VC
    ENDIF
    fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, GET_SHOP_COORDS(CLOTHES_SHOP_L_07_PB))
    IF fCurrentDist < fShortDist    
        fShortDist = fCurrentDist
        eShop = CLOTHES_SHOP_L_07_PB
    ENDIF
            
    RETURN eShop
ENDFUNC
FUNC SHOP_NAME_ENUM GET_CLOSEST_MEDENDSHOP()
    
    FLOAT fCurrentDist
    FLOAT fShortDist = 999999.99    
    vector vCoords = GET_ENTITY_COORDS(player_ped_id())
    SHOP_NAME_ENUM eshop =CLOTHES_SHOP_M_01_SM
    
    fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, GET_SHOP_COORDS(CLOTHES_SHOP_M_01_SM))
    IF fCurrentDist < fShortDist    
        fShortDist = fCurrentDist
        eShop = CLOTHES_SHOP_M_01_SM
    ENDIF
    fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, GET_SHOP_COORDS(CLOTHES_SHOP_M_03_H))
    IF fCurrentDist < fShortDist    
        fShortDist = fCurrentDist
        eShop = CLOTHES_SHOP_M_03_H
    ENDIF
    fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, GET_SHOP_COORDS(CLOTHES_SHOP_M_04_HW))
    IF fCurrentDist < fShortDist    
        fShortDist = fCurrentDist
        eShop = CLOTHES_SHOP_M_04_HW
    ENDIF
    fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, GET_SHOP_COORDS(CLOTHES_SHOP_M_05_GOH))
    IF fCurrentDist < fShortDist    
        fShortDist = fCurrentDist
        eShop = CLOTHES_SHOP_M_05_GOH
    ENDIF
            
    RETURN eShop
    
ENDFUNC
FUNC SHOP_NAME_ENUM GET_CLOSEST_HIGHENDSHOP()
    
    FLOAT fCurrentDist
    FLOAT fShortDist = 999999.99    
    vector vCoords = GET_ENTITY_COORDS(player_ped_id())
    SHOP_NAME_ENUM eshop = CLOTHES_SHOP_H_01_BH
    
    fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, GET_SHOP_COORDS(CLOTHES_SHOP_H_01_BH))
    IF fCurrentDist < fShortDist    
        fShortDist = fCurrentDist
        eShop = CLOTHES_SHOP_H_01_BH
    ENDIF
    fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, GET_SHOP_COORDS(CLOTHES_SHOP_H_02_B))
    IF fCurrentDist < fShortDist    
        fShortDist = fCurrentDist
        eShop = CLOTHES_SHOP_H_02_B
    ENDIF
    fCurrentDist = GET_DISTANCE_BETWEEN_COORDS(vCoords, GET_SHOP_COORDS(CLOTHES_SHOP_H_03_MW))
    IF fCurrentDist < fShortDist    
        fShortDist = fCurrentDist
        eShop = CLOTHES_SHOP_H_03_MW
    ENDIF
            
    RETURN eShop
ENDFUNC
FUNC STRING GET_HEADER_GRAPHIC_FOR_PI()
      SWITCH GET_CURRENT_PLAYER_PED_ENUM()
            CASE CHAR_MICHAEL   RETURN "ShopUI_Title_Graphics_Michael"    BREAK
            CASE CHAR_FRANKLIN  RETURN "ShopUI_Title_Graphics_Franklin"   BREAK
            CASE CHAR_TREVOR    RETURN "ShopUI_Title_Graphics_Trevor"     BREAK
      ENDSWITCH
      RETURN ""
ENDFUNC

FUNC BOOL REQUEST_HEADER_GRAPHIC_FOR_PI()
    IF NOT IS_STRING_NULL_OR_EMPTY(sGraphicHeader)
        REQUEST_STREAMED_TEXTURE_DICT(sGraphicHeader)
        bHeaderGraphicRequested = TRUE
        RETURN HAS_STREAMED_TEXTURE_DICT_LOADED(sGraphicHeader)
    ENDIF
  
  RETURN TRUE
ENDFUNC

PROC CLEANUP_HEADER_GRAPHIC_FOR_PI()
      IF bHeaderGraphicRequested
            IF NOT IS_STRING_NULL_OR_EMPTY(sGraphicHeader)
                  SET_STREAMED_TEXTURE_DICT_AS_NO_LONGER_NEEDED(sGraphicHeader)
            ENDIF
            bHeaderGraphicRequested = FALSE
      ENDIF
ENDPROC

FUNC BOOL GET_CHARACTER_COLOUR_PI(INT &iHeadRed, INT &iHeadGreen, INT &iHeadBlue, INT &iFootRed, INT &iFootGreen, INT &iFootBlue)

    BOOL bColourSet = FALSE
    INT iAlphsDummy
    SWITCH GET_CURRENT_PLAYER_PED_ENUM()
        CASE CHAR_MICHAEL
            GET_HUD_COLOUR(HUD_COLOUR_MICHAEL, iFootRed, iFootGreen, iFootBlue, iAlphsDummy)
            iHeadRed = 15
            iHeadGreen = 27
            iHeadBlue = 32
            bColourSet = TRUE
        BREAK
        CASE CHAR_FRANKLIN
            GET_HUD_COLOUR(HUD_COLOUR_FRANKLIN, iFootRed, iFootGreen, iFootBlue, iAlphsDummy)
            iHeadRed = 25
            iHeadGreen = 35
            iHeadBlue = 25
            bColourSet = TRUE
        BREAK
        CASE CHAR_TREVOR
            GET_HUD_COLOUR(HUD_COLOUR_TREVOR, iFootRed, iFootGreen, iFootBlue, iAlphsDummy)
            iHeadRed = 38
            iHeadGreen = 24
            iHeadBlue = 13
            bColourSet = TRUE
        BREAK
    ENDSWITCH
    
    RETURN bColourSet
ENDFUNC
FUNC VECTOR GET_STATIC_MISSION_BLIP_COORD(STATIC_BLIP_NAME_ENUM eblip)
    vector vTemp = <<0,0,0>>
                
    if eBlip = STATIC_BLIP_MISSION_DOCKS_P2B //overide static blip location to place for better waypoint location 
        vTemp = <<-1581.04492, 2791.69409, 15.85525>>
    else
        vTemp = GET_STATIC_BLIP_POSITION(eBlip,GET_CURRENT_PLAYER_PED_INT())
        if IS_VECTOR_ZERO(vTemp)
            vTemp = GET_STATIC_BLIP_POSITION(eBlip)
        endif
    endif
    
    return vTemp
ENDFUNC
PROC SET_QUICK_GPS_WAYPOINT()
    if not IS_PED_INJURED(player_ped_id())
        SHOP_NAME_ENUM thisShop
        VECTOR vTemp
        
        SWITCH eCurrentGPS
            CASE eGPS_NONE
                DELETE_WAYPOINTS_FROM_THIS_PLAYER()
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_HOME
                SAVEHOUSE_NAME_ENUM eSavehouse
                eSavehouse = GET_CLOSEST_SAVEHOUSE(GET_ENTITY_COORDS(player_ped_id()),GET_CURRENT_PLAYER_PED_ENUM(),true)           
                vTemp = GET_STATIC_BLIP_POSITION(g_sSavehouses[eSavehouse].eBlip)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()              
                EXIT
            BREAK           
            CASE eGPS_MISSION1                          
                vtemp = GET_STATIC_MISSION_BLIP_COORD(g_TriggerableMissions[0].eBlip)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_MISSION2
                vtemp = GET_STATIC_MISSION_BLIP_COORD(g_TriggerableMissions[1].eBlip)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_MISSION3
                vtemp = GET_STATIC_MISSION_BLIP_COORD(g_TriggerableMissions[2].eBlip)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_MISSION4
                vtemp = GET_STATIC_MISSION_BLIP_COORD(g_TriggerableMissions[3].eBlip)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_MISSION5
                vtemp = GET_STATIC_MISSION_BLIP_COORD(g_TriggerableMissions[4].eBlip)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_MISSION6
                vtemp = GET_STATIC_MISSION_BLIP_COORD(g_TriggerableMissions[5].eBlip)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_MISSION7
                vtemp = GET_STATIC_MISSION_BLIP_COORD(g_TriggerableMissions[6].eBlip)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_MISSION8
                vtemp = GET_STATIC_MISSION_BLIP_COORD(g_TriggerableMissions[7].eBlip)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_ATM
                vTemp = GET_CLOSEST_ATM_COORDS()
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_GUN
                thisShop = GET_CLOSEST_SHOP_OF_TYPE(GET_ENTITY_COORDS(player_ped_id()), SHOP_TYPE_GUN)
                vTemp = GET_SHOP_COORDS(thisShop)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK           
            CASE eGPS_MOD
                thisShop = GET_CLOSEST_SHOP_OF_TYPE(GET_ENTITY_COORDS(player_ped_id()), SHOP_TYPE_CARMOD, DEFAULT, DEFAULT  , DEFAULT, CARMOD_SHOP_SUPERMOD  )
                vTemp = GET_SHOP_COORDS(thisShop)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_lowCLOTHES
                thisShop = GET_CLOSEST_LOWENDSHOP()             
                vTemp = GET_SHOP_COORDS(thisShop)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_medCLOTHES
                thisShop = GET_CLOSEST_MEDENDSHOP()             
                vTemp = GET_SHOP_COORDS(thisShop)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_highCLOTHES
                thisShop = GET_CLOSEST_HIGHENDSHOP()                
                vTemp = GET_SHOP_COORDS(thisShop)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_BARBER
                thisShop = GET_CLOSEST_SHOP_OF_TYPE(GET_ENTITY_COORDS(player_ped_id()), SHOP_TYPE_HAIRDO)
                vTemp = GET_SHOP_COORDS(thisShop)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_TATTOO
                thisShop = GET_CLOSEST_SHOP_OF_TYPE(GET_ENTITY_COORDS(player_ped_id()), SHOP_TYPE_TATTOO)
                vTemp = GET_SHOP_COORDS(thisShop)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_MASK
                thisShop = CLOTHES_SHOP_A_01_VB
                vTemp = GET_SHOP_COORDS(thisShop)
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
            CASE eGPS_GARAGE
            
                VEHICLE_GEN_NAME_ENUM ename
                if GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
                    eName = VEHGEN_WEB_CAR_MICHAEL
                elif GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
                    eName = VEHGEN_WEB_CAR_FRANKLIN
                elif GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
                    eName = VEHGEN_WEB_CAR_TREVOR 
                ENDIF
                
                if DOES_BLIP_EXIST(g_sVehicleGenNSData.blipID[eName])
                    vTemp = GET_BLIP_COORDS(g_sVehicleGenNSData.blipID[eName])                  
                ENDIF
                SET_NEW_WAYPOINT(vTemp.x, vTemp.y)
                REFRESH_WAYPOINT()
                EXIT
            BREAK
        ENDSWITCH   
    endif   
ENDPROC

//	B* 2147964 - Gets the multiplier for the fake vs physical aspect ratio
//	(If not in triple head)
FUNC FLOAT GET_ASPECT_RATIO_MULTIPLIER()
	INT xScreen, yScreen
	GET_ACTUAL_SCREEN_RESOLUTION(xScreen, yScreen)

	IF IS_PHYSICAL_RES_CONSIDERED_TRIPLE_HEAD(xScreen, yScreen)
		RETURN 1.0
	ENDIF
		
	FLOAT fFakeAspect = GET_ASPECT_RATIO(FALSE)
	FLOAT fActualAspect = TO_FLOAT(xScreen) / TO_FLOAT(yScreen)
	RETURN fActualAspect / fFakeAspect
ENDFUNC

//PURPOSE: Initialises the Menu Header - Shows Player Headshot, Name + CrewTag, Crew Name
PROC INIT_PI_MENU_HEADER(PI_MENU_HEADER_DATA &HeaderData)
    //Move Main Menu Down
    CUSTOM_MENU_Y = (((CUSTOM_MENU_ITEM_BAR_H / GET_ASPECT_RATIO_MULTIPLIER() ) * (3.0 / GET_ASPECT_RATIO_MULTIPLIER() ))+0.05) 
    
    //Background
    HeaderData.Background.x = 0.188
    HeaderData.Background.y = 0.100 + CUSTOM_MENU_ITEM_BAR_H    //0.111
    HeaderData.Background.w = CUSTOM_MENU_W
    HeaderData.Background.h = 0.090     //(CUSTOM_MENU_ITEM_BAR_H*2)
    INT iHudR, iHudG, iHudB, iHudA
    HUD_COLOURS hudColour
    if GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
        hudColour = HUD_COLOUR_MICHAEL
    elif GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
        hudColour = HUD_COLOUR_FRANKLIN
    else //trev
        hudColour = HUD_COLOUR_TREVOR
    endif
    
    echarPrev = GET_CURRENT_PLAYER_PED_ENUM()
    
    GET_HUD_COLOUR(hudColour, iHudR, iHudG, iHudB, iHudA)
    HeaderData.Background.r = iHudR //68    //200
    HeaderData.Background.g = iHudG //158   //66
    HeaderData.Background.b = iHudB //208   //66
    HeaderData.Background.a = iHudA //255
        
    //Player Name
    HeaderData.PlayerNameStyle.aFont  = FONT_CURSIVE
    HeaderData.PlayerNameStyle.XScale = 0.8//0.500 
    HeaderData.PlayerNameStyle.YScale = 1.05//0.750 
    HeaderData.PlayerNameStyle.r = 255
    HeaderData.PlayerNameStyle.g = 255
    HeaderData.PlayerNameStyle.b = 255
    HeaderData.PlayerNameStyle.a = 255
    HeaderData.PlayerNameStyle.drop = DROPSTYLE_NONE
    HeaderData.PlayerNameStyle.wrapEndX = 0
    HeaderData.PlayerNameStyle.wrapStartX = 0
    
    HeaderData.PlayerNamePlace.x = txt_x        
    HeaderData.PlayerNamePlace.y = txt_y  
//    
//    sGraphicHeader  = GET_HEADER_GRAPHIC_FOR_PI()
//    REQUEST_HEADER_GRAPHIC_FOR_PI()
    CPRINTLN(DEBUG_MISSION, "header init---CV:sGraphicHeader: ",sGraphicHeader) 
    SET_MENU_USES_HEADER_GRAPHIC((NOT IS_STRING_NULL_OR_EMPTY(sGraphicHeader)), sGraphicHeader, sGraphicHeader)
    
    
ENDPROC  
PROC DRAW_TEXT_WITH_CHAR_NAME(TEXT_PLACEMENT &inText, TEXT_STYLE &Style, HUD_COLOURS aColour = HUD_COLOUR_WHITE, eTextJustification Justifyied = FONT_LEFT)
    
    TEXT_LABEL_15 inCharName
    if GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
        inCharName = "BLIP_78"//"CHAR_SEL_M"
    elif GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
        inCharName = "BLIP_88"//"CHAR_SEL_F"
    else //trev
        inCharName = "BLIP_79"//"CHAR_SEL_T"
    endif
    
    IF NOT IS_STRING_EMPTY_HUD(inCharName)          
        IF IS_SAFE_TO_DRAW_ON_SCREEN()
        
            SET_TEXT_STYLE(Style)   
            SET_TEXT_JUSTIFICATION(Justifyied)      
            
            BEGIN_TEXT_COMMAND_DISPLAY_TEXT("STRING")
                SET_COLOUR_OF_NEXT_TEXT_COMPONENT(aColour)
                ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL(inCharName)
            END_TEXT_COMMAND_DISPLAY_TEXT(ADJUST_SCREEN_X_POSITION(inText.x), ADJUST_SCREEN_Y_POSITION(inText.y))
        
        ENDIF
    ENDIF
ENDPROC
PROC DRAW_PI_Menu_HEADER(PI_MENU_HEADER_DATA &HeaderData)
    
    SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
    SET_SCRIPT_GFX_ALIGN_PARAMS(fAlignX, fAlignY, 0.0, 0.0)
        
    //Move Main Menu Down
    CUSTOM_MENU_Y = ( ( ( CUSTOM_MENU_ITEM_BAR_H / GET_ASPECT_RATIO_MULTIPLIER() ) * ( 3.0 / GET_ASPECT_RATIO_MULTIPLIER() ) ) + 0.05 )       
    //Background
    DRAW_RECTANGLE(HeaderData.Background)
    //char name
    DRAW_TEXT_WITH_CHAR_NAME(HeaderData.PlayerNamePlace,HeaderData.PlayerNameStyle,HUD_COLOUR_BLACK,FONT_CENTRE)
    
    RESET_SCRIPT_GFX_ALIGN()
ENDPROC
PROC SPECIAL1_PED_RESET(bool bAddingMask = FALSE)
    //if applying a mask then remove all special conflicts
    if GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
        if GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL) = SPECIAL_P0_FIREMAN_ACCS
        or GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL) = SPECIAL_P0_BALACLAVA
        or GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL) = SPECIAL_P0_GAS_MASK
        or GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL) = SPECIAL_P0_CLOWN_ACCS
        or GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL) = SPECIAL_P0_FIREMAN_ACCS_1
        or GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL) = SPECIAL_P0_BALACLAVA_B
            SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL2 ,SPECIAL_P0_NONE)
        endif                       
    elif GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN                      
        if GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL) = SPECIAL_P1_BALLISTICS
        or GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL) = SPECIAL_P1_BALACLAVA
        or GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL) = SPECIAL_P1_EXTERMINATOR
        or GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL) = SPECIAL_P1_FIREMAN
        or( GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL)= SPECIAL_P1_MASK and bAddingMask)
        or GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL) = SPECIAL_P1_FIREMAN_1                       
            SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL ,SPECIAL_P1_DUMMY)
        endif       
        if GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_HAIR) = HAIR_P1_MASK
            SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_HAIR ,HAIR_P1_SHORT_1)
        endif
    elif GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR                        
        if GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL) = SPECIAL_P2_FIREMAN_ACCS
        or GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL) = SPECIAL_P2_BALACLAVA
        or GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL) = SPECIAL_P2_NCHEIFMASK
        or GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL) = SPECIAL_P2_MASK
        or GET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL) = SPECIAL_P2_FIREMAN_ACCS_1                      
            SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL ,SPECIAL_P2_DUMMY)
        endif
    endif
ENDPROC
//PURPOSE: Sets the currently selected item
PROC SET_CURRENT_PED_COMPONENT()

    IF iSubMenu = ciPI_SUB_MENU_INVENTORY
        //Masks
        IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_M2_MASKS
            IF iMaskTotalCount > 1
                CPRINTLN(DEBUG_MISSION, "Masks---CV:SET_CURRENT_PED_COMPONENT:SET name: ",ENUM_TO_INT( MaskData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eName))    
                
                if GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
                    if MaskData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eName != SPECIAL_P1_MASK
                        //Remove Conflicts
                        sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_HATS]       = 0
                        sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_GLASSES]    = 0
                    endif
                else
                    //Remove Conflicts
                    sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_HATS]       = 0
                    sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_GLASSES]    = 0
                endif
                
                //Remove Mask
                if sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_MASKS] != 0              
                    if GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
                        SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL2 ,SPECIAL2_P0_NONE)
                    elif GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
                        SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL2 ,SPECIAL2_P1_NONE)  
                        IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL, SPECIAL_P1_MASK)
                            SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL ,SPECIAL_P1_WATCH_AND_BRACELET)  //Reset to watch and bracelet
                        endif
                    else 
                        SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL2 ,SPECIAL2_P2_NONE)  
                    endif
                    SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_PROPS ,PROPS_HEAD_NONE)  
                endif       
                
                SPECIAL1_PED_RESET(true)    
                //only set hats and glasses here if not franklin or not wearing bandana as franklin
                if GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
                    if MaskData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eName != SPECIAL_P1_MASK
                        SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),HatsData[sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_HATS]].eType,HatsData[sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_HATS]].eName)
                        SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),GlassesData[sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_GLASSES]].eType,GlassesData[sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_GLASSES]].eName)
                    endif
                else                    
                    SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),HatsData[sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_HATS]].eType,HatsData[sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_HATS]].eName)
                    SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),GlassesData[sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_GLASSES]].eType,GlassesData[sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_GLASSES]].eName)
                endif
                REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
                                
                //Add Mask
                REMOVE_INVALID_PROP_ITEMS_FOR_ITEM(PLAYER_PED_ID(), MaskData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eType, MaskData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eName)
                SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), MaskData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eType, MaskData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eName)
                if MaskData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eName = PROPS_HEAD_NONE
                    if GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
                        SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL2 ,SPECIAL2_P0_NONE)  
                    elif GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
                        SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL2 ,SPECIAL2_P1_NONE)  
                        IF IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL, SPECIAL_P1_MASK)
                            SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL ,SPECIAL_P1_WATCH_AND_BRACELET)  //Reset to watch and bracelet
                        endif
                    else 
                        SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL2 ,SPECIAL2_P2_NONE)  
                    endif
                endif
                
                SET_BIT(iBoolsBitSet, biEnableFaceCam)
            ENDIF
        //Glasses
        ELIF sPI_MenuData.iCurrentSelection = ciPI_TYPE_M2_GLASSES
            IF iGlassesTotalCount > 1
                CPRINTLN(DEBUG_MISSION, "Glasses---CV:SET_CURRENT_PED_COMPONENT:SET name: ",ENUM_TO_INT( GlassesData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eName))   
                
                //Remove Mask
                if sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_MASKS] != 0
                    if GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
                        IF not IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL, SPECIAL_P1_MASK)
                            sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_MASKS] = 0
                            SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL2 ,SPECIAL2_P0_NONE)
                            SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), MaskData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_MASKS]].eType, MaskData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_MASKS]].eName)
                        endif
                    else
                        sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_MASKS] = 0
                        SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL2 ,SPECIAL2_P0_NONE)
                        SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), MaskData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_MASKS]].eType, MaskData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_MASKS]].eName)
                    endif
                endif
                SPECIAL1_PED_RESET()
                //Remove Helmet
                REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
                //add glasses
                REMOVE_INVALID_PROP_ITEMS_FOR_ITEM(PLAYER_PED_ID(), GlassesData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eType, GlassesData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eName)
//              FORCE_VALID_PED_COMPONENT_COMBO_FOR_ITEM_SP(PLAYER_PED_ID(), GlassesData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eType, GlassesData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eName)
                SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), GlassesData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eType, GlassesData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eName)
                SET_BIT(iBoolsBitSet, biEnableFaceCam)  
            ENDIF
            
        //Hats
        ELIF sPI_MenuData.iCurrentSelection = ciPI_TYPE_M2_HATS
            IF iHatTotalCount > 1
            AND NOT IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
                CPRINTLN(DEBUG_MISSION, "HATS---CV:SET_CURRENT_PED_COMPONENT:SET name: ",ENUM_TO_INT( HatsData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eName)) 
                    
               //Remove Mask
                if sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_MASKS] != 0
                    if GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
                        IF not IS_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), COMP_TYPE_SPECIAL, SPECIAL_P1_MASK)
                            sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_MASKS] = 0
                            SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL2 ,SPECIAL2_P0_NONE)
                            SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), MaskData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_MASKS]].eType, MaskData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_MASKS]].eName)
                        endif
                    else
                        sSelection.iSelection[ciPI_SUB_MENU_INVENTORY][ciPI_TYPE_M2_MASKS] = 0
                        SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(),COMP_TYPE_SPECIAL2 ,SPECIAL2_P0_NONE)
                        SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), MaskData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_MASKS]].eType, MaskData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_MASKS]].eName)
                    endif
                endif
                SPECIAL1_PED_RESET()
                //Remove Helmet
                REMOVE_PED_HELMET(PLAYER_PED_ID(), TRUE)
                //Add hat
                REMOVE_INVALID_PROP_ITEMS_FOR_ITEM(PLAYER_PED_ID(), HatsData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eType, HatsData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eName)
//              FORCE_VALID_PED_COMPONENT_COMBO_FOR_ITEM_SP(PLAYER_PED_ID(), HatsData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eType, HatsData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eName)
                SET_PED_COMP_ITEM_CURRENT_SP(PLAYER_PED_ID(), HatsData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eType, HatsData[sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]].eName)
//              ALTER_WANTED_LEVEL_FOR_CHANGED_LOOK()
                SET_BIT(iBoolsBitSet, biEnableFaceCam)
            ENDIF           
        ENDIF
           
    ENDIF
    
    STORE_PLAYER_PED_VARIATIONS(PLAYER_PED_ID())
ENDPROC

//PURPOSE: Controls what should happen when selecting an option
PROC CONTROL_PI_MENU_INPUT()
	#IF FEATURE_SP_DLC_DIRECTOR_MODE
	IF bDirectorWarningScreen
		SET_GAME_PAUSED(TRUE)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		SET_WARNING_MESSAGE_WITH_HEADER("VEUI_HDR_ALERT", "VE_DIR_MODE_SURE", FE_WARNING_OKCANCEL)	
		if 	IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			g_bLaunchDirectorMode = true
            ePI_MENU_STATE = PI_MENU_CLEANUP
            DO_SCREEN_FADE_OUT(DEFAULT_FADE_TIME_SHORT)      
			bDirectorWarningScreen = false
			SET_PLAYER_CONTROL(player_id(),true)
			SET_GAME_PAUSED(FALSE)
		elif IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
		or (IS_PC_VERSION() AND IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL))
			bDirectorWarningScreen = false
			SET_PLAYER_CONTROL(player_id(),true)
			SET_GAME_PAUSED(FALSE)
		endif

	else
	#endif
	    if bSelectAvailable
	        IF IS_CELLPHONE_ACCEPT_ONLY_JUST_RELEASED()
	            OR bCursorAccept = TRUE
	            PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
	                    
	            IF iSubMenu = ciPI_SUB_MENU_NONE
	                SWITCH sPI_MenuData.iCurrentSelection
	                    case ciPI_TYPE_QUICK_GPS
	                        SET_QUICK_GPS_WAYPOINT()
	                        ePI_MENU_STATE = PI_MENU_CLEANUP
	                    break
	                    case ciPI_TYPE_INVENTORY
	                        iStoredSelection = sPI_MenuData.iCurrentSelection 
	                        sPI_MenuData.iCurrentSelection = 0
	                        iSubMenu = ciPI_SUB_MENU_INVENTORY
	                        bResetMenuNow = TRUE
	                    break
	                    case ciPI_TYPE_OBJECTIVE
	                        iStoredSelection = sPI_MenuData.iCurrentSelection
	                        sPI_MenuData.iCurrentSelection = 0
	                        iSubMenu = ciPI_SUB_MENU_OBJECTIVE
	                        bResetMenuNow = TRUE
	                    break
	                    case ciPI_TYPE_HELPTEXT
	                        iStoredSelection = sPI_MenuData.iCurrentSelection
	                        sPI_MenuData.iCurrentSelection = 0
	                        iSubMenu = ciPI_SUB_MENU_HELPTEXT
	                        bResetMenuNow = TRUE
	                    break
	                    #IF FEATURE_SP_DLC_DIRECTOR_MODE
	                    case ciPI_TYPE_DIRECTOR          
							SET_PLAYER_CONTROL(player_id(),false)
	                        bDirectorWarningScreen = true
	                    break
	                    #ENDIF
						
	                endswitch           
	            elif iSubMenu = ciPI_SUB_MENU_INVENTORY
	                sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]++
	                IF sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection] > GET_SELECTION_MAX_PI(sPI_MenuData.iCurrentSelection)
	                    sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection] = 0
	                ENDIF
	                bResetMenuNow = TRUE
	                SET_CURRENT_PED_COMPONENT()
					
				
	            endif
				
	        endif           
	    endif
	#IF FEATURE_SP_DLC_DIRECTOR_MODE	
	Endif
	#endif
ENDPROC 
//purpose: checks the missions and other situations to stop inventory working
FUNC BOOL SAFE_TO_USE_INVENTORY()

    // Are these missions running
    if GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Family2")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Lester1")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Michael2")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Michael4")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Franklin0")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Franklin2")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("carsteal3")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FBI1")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FBI2")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FBI4")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("FBI5A")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("agency_heist2")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("agency_heist3A")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("agency_heist3B")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("docks_setup")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("docks_heistA")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("docks_heistB")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("finale_heist2a")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("finale_heist2b")) != 0
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Jewelry_setup1")) != 0 
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Jewelry_heist")) != 0  
    or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("rural_bank_heist")) != 0   
        return false
    endif
    
    //Blocking the use of the inventory in any vehicle that isn't a car or a boat
    if not IS_PED_INJURED(player_ped_id())
        IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
            VEHICLE_INDEX thisVeh = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())           
            IF not IS_THIS_MODEL_A_CAR(GET_ENTITY_MODEL(thisVeh))
            and not IS_THIS_MODEL_A_BOAT(GET_ENTITY_MODEL(thisVeh))
                RETURN FALSE
            Endif
        endif   
    endif
        
    RETURN TRUE
ENDFUNC


PROC CONTROL_PI_MENU_PC_MOUSE_INPUT()

    bCursorAccept = FALSE
    bCursorCancel = FALSE
    bCursorQuitMenu = FALSE
    bCursorScrollUp = FALSE
    bCursorScrollDown = FALSE
    iCursorValueChange = 0
    bCursorCamMove = FALSE
    
	INT iTempCursorChange = 0
    CONST_FLOAT LEFT_SIDE_ACCEPT_SIZE 0.045
    
    IF ePI_MENU_STATE != PI_MENU_ACTIVE
        IF NOT HAVE_SF_ASSETS_LOADED()
            EXIT
        ENDIF
    ENDIF
        
    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
                                    
        IF eFaceCamStage != FCS_RUNNING
            HANDLE_PI_MENU_MOUSE_CAMERA(FALSE, bCursorMenuFirstTime)
        ELSE
            bCursorCamMove = HANDLE_PI_MENU_MOUSE_CAMERA(TRUE, bCursorMenuFirstTime)
        ENDIF
        
        // Cursor is inside the menu
        IF g_iMenuCursorItem > MENU_CURSOR_NO_ITEM
        
            // Clicking on an item
			IF IS_CURSOR_ACCEPT_HELD()
			
                // Clicking on currently selected item
                IF g_iMenuCursorItem = sPI_MenuData.iCurrentSelection
                    
                    // Item is a < item > style with value change, so handle that.
                    IF (GET_SELECTION_MAX_PI(sPI_MenuData.iCurrentSelection) > 0) AND (bUseSelectArrows = TRUE)
					
                        // Some options can be confirmed with a click to the left hand side of the menu.
						iTempCursorChange = GET_CURSOR_MENU_ITEM_VALUE_CHANGE(LEFT_SIDE_ACCEPT_SIZE)
                       	IF (iTempCursorChange = -1) OR (iTempCursorChange = 1)
							iCursorValueChange = iTempCursorChange
                        ENDIF
					ENDIF
				ENDIF
			ENDIF
			
            IF IS_CURSOR_ACCEPT_ONLY_JUST_RELEASED()
            
                // Clicking on currently selected item
                IF g_iMenuCursorItem = sPI_MenuData.iCurrentSelection
                    
                    // Item is a < item > style with value change, so handle that.
                    IF GET_SELECTION_MAX_PI(sPI_MenuData.iCurrentSelection) > 0
                    AND bUseSelectArrows = TRUE
                        
                        // Some options can be confirmed with a click to the left hand side of the menu.
                        IF iSubMenu = ciPI_SUB_MENU_NONE
                        AND sPI_MenuData.iCurrentSelection = ciPI_TYPE_QUICK_GPS
                            iCursorValueChange = GET_CURSOR_MENU_ITEM_VALUE_CHANGE(LEFT_SIDE_ACCEPT_SIZE)
                            IF iCursorValueChange = -999
                                bCursorAccept = TRUE
                            ENDIF
                        ELSE
                            iCursorValueChange = GET_CURSOR_MENU_ITEM_VALUE_CHANGE()
                        ENDIF
                        
                    ELSE
                    // Item is a normal item, so just confirm
                        bCursorAccept = TRUE
                    ENDIF
                        
                ELSE
                    // Select new item. 
                    
                    sPI_MenuData.iCurrentSelection = g_iMenuCursorItem
                    SET_CURRENT_MENU_ITEM(sPI_MenuData.iCurrentSelection)
                    bResetMenuNow = TRUE
                    PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                    
                ENDIF
                    
            ENDIF
            
            // Different cursor if value can be confirmed by a click to the left of the menu.
            IF g_iMenuCursorItem = sPI_MenuData.iCurrentSelection
                IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_QUICK_GPS
                AND iSubMenu = ciPI_SUB_MENU_NONE 
                AND GET_CURSOR_MENU_ITEM_VALUE_CHANGE(LEFT_SIDE_ACCEPT_SIZE) = -999
                    SET_MOUSE_CURSOR_STYLE(MOUSE_CURSOR_STYLE_HAND_MIDDLE_FINGER)
                ENDIF
            ENDIF
            
        ELSE
            
            // Cancel if clicked outside the menu, but not if face-cam is running.
            IF eFaceCamStage != FCS_RUNNING
			AND iMenuUpdateTimer != GET_GAME_TIMER()		// B*2280267: Also needed to make sure that menu has not been rebuilt this frame (as that means clicking anywhere counts as off-menu). The menu sets this timer when it rebuilds, so I've added a check to it.

                IF IS_CURSOR_ACCEPT_ONLY_JUST_RELEASED()
                    bCursorQuitMenu = TRUE
                ENDIF

            ENDIF
        
        ENDIF
        
        // Cancel menu
        IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
            bCursorCancel = TRUE
        ENDIF
        
        // Scroll up
        IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
        OR (IS_CURSOR_ACCEPT_ONLY_JUST_RELEASED() AND g_iMenuCursorItem = MENU_CURSOR_SCROLL_UP)
            bCursorScrollUp = TRUE
        ENDIF
        
        // Scroll down
        IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
        OR (IS_CURSOR_ACCEPT_ONLY_JUST_RELEASED() AND g_iMenuCursorItem = MENU_CURSOR_SCROLL_DOWN)
            bCursorScrollDown = TRUE
        ENDIF
        
		/*
		IF IS_CURSOR_ACCEPT_HELD()
			IF (GET_GAME_TIMER() > iScrollTimer)			
				IF (g_iMenuCursorItem = MENU_CURSOR_SCROLL_UP)
					iScrollTimer = GET_GAME_TIMER() + 250
					bCursorScrollUp = TRUE
					EXIT
				ENDIF
				
				IF (g_iMenuCursorItem = MENU_CURSOR_SCROLL_DOWN)
					iScrollTimer = GET_GAME_TIMER() + 250
					bCursorScrollDown = TRUE
					EXIT
				ENDIF				
			ENDIF
		ENDIF
		*/
    ENDIF
    
ENDPROC



//PURPOSE: Controls movement of the menu
FUNC BOOL CONTROL_PI_MENU_MOVEMENT()
    //FLOAT fTemp
	#IF FEATURE_SP_DLC_DIRECTOR_MODE
		IF !bDirectorWarningScreen  
	#endif
	
	    //Move Up and Down
	    IF (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN) OR bCursorScrollDown)
	        IF get_Game_timer() - iPI_MoveTimer > iPI_MOVE_DELAY OR bCursorScrollDown
	            iPI_MoveTimer = get_Game_timer()            
	            PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
	            sPI_MenuData.iCurrentSelection++
	            IF sPI_MenuData.iCurrentSelection > (GET_MENU_MAX() -1) 
	                sPI_MenuData.iCurrentSelection = 0
	            ENDIF
	                        
	            SET_CURRENT_MENU_ITEM(sPI_MenuData.iCurrentSelection)
	            bResetMenuNow = TRUE
	            
	            RETURN TRUE
	        ENDIF
	        
	    ELIF (IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_UP) OR bCursorScrollUp)
	        IF get_Game_timer() - iPI_MoveTimer > iPI_MOVE_DELAY OR bCursorScrollUp
	            iPI_MoveTimer = get_Game_timer()
	            PLAY_SOUND_FRONTEND(-1, "NAV_UP_DOWN", "HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
	            sPI_MenuData.iCurrentSelection--
	            IF sPI_MenuData.iCurrentSelection < 0
	                sPI_MenuData.iCurrentSelection = (GET_MENU_MAX() -1)
	            ENDIF
	            
	            SET_CURRENT_MENU_ITEM(sPI_MenuData.iCurrentSelection)
	            bResetMenuNow = TRUE
	            
	            RETURN TRUE
	        ENDIF
	        
	    ELIF (IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT) OR iCursorValueChange = -1)
	        IF get_Game_timer() - iPI_MoveTimer > iPI_MOVE_DELAY
	            iPI_MoveTimer = get_Game_timer()
	            IF GET_SELECTION_MAX_PI(sPI_MenuData.iCurrentSelection) > 0
	            AND bUseSelectArrows = TRUE
	                sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]--
	                IF sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection] < 0
	                    sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection] = GET_SELECTION_MAX_PI(sPI_MenuData.iCurrentSelection)
	                ENDIF
	                SET_CURRENT_PED_COMPONENT()
	                PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)                          
	                bResetMenuNow = TRUE                
	                RETURN TRUE
	            ENDIF           
	        ENDIF
	    ELIF ( IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT) OR iCursorValueChange = 1)
	        IF get_Game_timer() - iPI_MoveTimer > iPI_MOVE_DELAY
	            iPI_MoveTimer = get_Game_timer()
	            IF GET_SELECTION_MAX_PI(sPI_MenuData.iCurrentSelection) > 0
	            AND bUseSelectArrows = TRUE
	                sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection]++
	                IF sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection] > GET_SELECTION_MAX_PI(sPI_MenuData.iCurrentSelection)
	                    sSelection.iSelection[iSubMenu][sPI_MenuData.iCurrentSelection] = 0
	                ENDIF
	                SET_CURRENT_PED_COMPONENT()
	                PLAY_SOUND_FRONTEND(-1, "NAV_LEFT_RIGHT", "HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)                                 
	                bResetMenuNow = TRUE                
	                RETURN TRUE
	            ENDIF
	        ENDIF       
	    ENDIF
			
    #IF FEATURE_SP_DLC_DIRECTOR_MODE
		endif  
	#endif
    
    RETURN TRUE
ENDFUNC
//PURPOSE: Stops and clears the Face Cam
PROC CLEANUP_FACE_CAM()
    IF IS_BIT_SET(iBoolsBitSet, biEnableFaceCam)        
        SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
        SET_CAM_ACTIVE(FaceCam, FALSE)
        RENDER_SCRIPT_CAMS(FALSE, FALSE)    //TRUE)
        DESTROY_CAM(FaceCam)
        CLEAR_BIT(iBoolsBitSet, biEnableFaceCam)
        eFaceCamStage = FCS_WAIT
        fXpos = 0 
        fYpos = 0
        NET_PRINT_TIME() NET_PRINT("     ---------->     PIM - PROCESS_FACE_CAM - CLEANUP_FACE_CAM - iFaceCamStage = FCS_WAIT") NET_NL()        
    ENDIF
ENDPROC
FUNC BOOL MENU_OFF_CHECK()
    enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM() 
	
	// Don't allow the menu to be closed when we are displaying a warning screen.
	// Player control is off and we have to let the warning screen clear before
	// killing the menu.
	#IF FEATURE_SP_DLC_DIRECTOR_MODE
		IF !bDirectorWarningScreen //dont apply this check when on the warning screen
	#endif
		
	
	    IF NOT IS_PED_INJURED(player_ped_id())                                                              // ped alive    
	        if IS_PAUSE_MENU_ACTIVE()                                                                       // is pause menu active
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: is pause menu active")
	            return true
	        endif   
	        if GET_PAUSE_MENU_STATE()!= PM_INACTIVE                                                         // is pause menu active
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: is pause menu activate state")               
	            return true
	        endif
	        if IS_RESULT_SCREEN_DISPLAYING()                                                                // passed screen on
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: passed screen on")
	            return true
	        endif
	        if IS_PHONE_ONSCREEN()                                                                          // phone is on screen
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: phone is on screen")
	            return true
	        endif
	        if IS_HUD_HIDDEN()                                                                              // hud is hidden
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: hud is hidden")
	            return true
	        endif
	        if IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME)                                       // Any minigame block
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: Any minigame block")
	            return true
	        endif
	        if IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME_FRIENDS)                               // any friends block
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: MINIGAME_FRIENDS")
	            return true
	        endif
	        if IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)                                // any friends block
	        or IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: any FRIEND_ACTIVITY")
	            return true
	        endif
			if( not IS_PLAYER_CONTROL_ON(player_id()) and not IS_BIT_SET(iBoolsBitSet, biEnableFaceCam) )   // make sure menu close if control off and not in cam mode
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: make sure menu close if control off and not in cam mode")
	            return true
	        endif
	        if( not IS_GAMEPLAY_CAM_RENDERING() 
	            and not IS_CINEMATIC_FIRST_PERSON_VEHICLE_INTERIOR_CAM_RENDERING()
	            and not IS_BIT_SET(iBoolsBitSet, biEnableFaceCam))          // if a scripted cam is running block menu
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: if a scripted cam is running block menu")
	            return true
	        endif 
			if IS_HELP_MESSAGE_BEING_DISPLAYED() 
	        and not IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_OFF_MISSION)                               // help message displayed
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: help message displayed on mission")
	            return true
	        endif
	        if g_iCurrentlyDisplayingContextINDEX != -1                                                     // context displayed
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: context displayed")
	            return true
	        endif
	        if not IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	            if IS_PED_SWIMMING(PLAYER_PED_ID())                                                             //is ped swimming
	            or IS_PED_SWIMMING_UNDER_WATER(PLAYER_PED_ID())     
	                CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: is ped swimming")
	                return true
	            endif
	        endif       
	        
	        if IS_CUTSCENE_PLAYING()                                                                        // Cutscene playing 
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: Cutscene playing")
	            return true
	        endif
	        if IS_PED_BEING_ARRESTED(player_ped_id())                                                       // Being arrested
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: Being arrested")
	            return true
	        endif
	        if IS_FIRST_PERSON_AIM_CAM_ACTIVE()                                                             // player aiming
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: player aiming")
	            return true
	        endif
	        if IS_PED_FALLING(player_ped_id())                                                              // player falling
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK:  player falling")
	            return true
	        endif
	        if IS_PED_RAGDOLL(player_ped_id())                                                              // ped ragdolling
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: ped ragdolling")
	            return true
	        endif
	        if GET_PED_PARACHUTE_STATE(player_ped_id()) = PPS_SKYDIVING                                     // skydiving should block   
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: skydiving should block")
	            return true
	        endif
	        if GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Prologue1")) > 0               // Prologue block menu
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: Prologue block menu")
	            return true
	        endif
	        if GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Armenian1")) > 0               // arm1 block menu  
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: arm1 block menu")
	            return true
	        endif
	        if GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Appinternet")) > 0             // internet check   
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: internet check")
	            return true
	        endif
	        if GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("hao1")) > 0                    // hao race check   
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: street race mini game RC version")
	            return true
	        endif
	        if GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Fanatic1")) > 0                // fanatic race check   
	        or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Fanatic2")) > 0                // fanatic race check   
	        or GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Fanatic3")) > 0                // fanatic race check
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: fanatic race check")
	            return true
	        endif
	        
	        if (GET_PLAYER_CHAR_COMMUNICATION_PRIORITY_LEVEL(ePed) = CPR_VERY_HIGH)                         // end of mission call disable menu 
	        and IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_OFF_MISSION)   
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: end of mission call disable menu")
	            return true
	        endif
	        if g_bInATM                                                                                     // ATM check
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: ATM check")
	            return true
	        endif
	        if IS_PLAYER_BROWSING_ITEMS_IN_ANY_SHOP()                                                       // browsing in a shop
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: browsing in a shop")
	            return true
	        ENDIF   
	        if IS_PLAYER_IN_SHOP_OF_TYPE(SHOP_TYPE_CARMOD)
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: in SHOP_TYPE_CARMOD")                        // In mod shop
	            return true
	        endif

	        if g_bPlayerLockedInToTrigger                                                               // Triggering mission
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: g_bPlayerLockedInToTrigger")                         
	            return true
	        endif
	                
	        echarCurr = GET_CURRENT_PLAYER_PED_ENUM()
	        if echarCurr != echarPrev
	            echarPrev = echarCurr
	            iCurrentSelfie = 0
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: switched to a different Char")                       
	            return true         
	        endif
	        
	        if IS_PLAYER_SWITCH_IN_PROGRESS()                                                               // switching 
	            CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: IS_PLAYER_SWITCH_IN_PROGRESS")                           
	            return true
	        endif
	        
	        if IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_ANIMAL)                                         //peyote mission block.
	        or IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
	            return true
	        endif
			
			if ARE_STRINGS_EQUAL(g_sMenuData.tl15Title,"IMPOUND_TITLE")
			and IS_CUSTOM_MENU_ON_SCREEN()
				CPRINTLN(DEBUG_MISSION, "---CV MENU_OFF_CHECK: Multiple vehcile impound menu") 
				return true
			endif
			
			// ---- resolution update check ----		
			INT scrX,scrY
			GET_ACTUAL_SCREEN_RESOLUTION(scrX,scrY)
			IF scrX != iOldScrX
			OR scrY != iOldScrY
				iOldScrX = scrX
				iOldScrY = scrY
				RETURN FALSE	//TRUE
			ENDIF
	        
	    ELSE        
	        RETURN TRUE
	    ENDIF
		
	#IF FEATURE_SP_DLC_DIRECTOR_MODE
		endif
	#endif
    
    RETURN FALSE
ENDFUNC
FUNC BOOL CONTROL_MENU_CLOSE()
    if not IS_BIT_SET(iBoolsBitSet, biSwitchButtonCheck)
        IF IS_DISABLED_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_CANCEL)//IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CELLPHONE_CANCEL)
        OR bCursorCancel OR bCursorQuitMenu
            
            IF bCursorQuitMenu
                iPi_menu_timer = get_Game_timer()
                return true
            ENDIF
        
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
            if iSubMenu = ciPI_SUB_MENU_INVENTORY
            or   iSubMenu = ciPI_SUB_MENU_OBJECTIVE
            or   iSubMenu = ciPI_SUB_MENU_HELPTEXT

                CLEANUP_FACE_CAM()
                PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
                sPI_MenuData.iCurrentSelection = iStoredSelection
                iSubMenu = ciPI_SUB_MENU_NONE     
                bResetMenuNow = TRUE
                iPi_submenu_timer = get_Game_timer()
                RETURN FALSE
            elif iSubMenu = ciPI_SUB_MENU_NONE 
            and get_Game_timer() - iPi_submenu_timer >= PI_MENU_DELAY
                iPi_menu_timer = get_Game_timer()
                return true
            endif
        ENDIF   
    endif
    
    if MENU_OFF_CHECK()
#IF FEATURE_SP_DLC_DIRECTOR_MODE
		IF bDirectorWarningScreen
			bDirectorWarningScreen = FALSE
		ENDIF

#ENDIF
		 return true
    endif
    
    if iSubMenu = ciPI_SUB_MENU_INVENTORY
    and IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())
        CLEANUP_FACE_CAM()
        PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
        sPI_MenuData.iCurrentSelection = iStoredSelection
        iSubMenu = ciPI_SUB_MENU_NONE           
        bResetMenuNow = TRUE
        iPi_submenu_timer = get_Game_timer()
    endif
    
    if iSubMenu = ciPI_SUB_MENU_OBJECTIVE
    and not IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
        PLAY_SOUND_FRONTEND(-1, "BACK", "HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)
        sPI_MenuData.iCurrentSelection = iStoredSelection
        iSubMenu = ciPI_SUB_MENU_NONE           
        bResetMenuNow = TRUE
        iPi_submenu_timer = get_Game_timer()
    endif
	
	
    
RETURN FALSE

ENDFUNC

PROC PRE_SETUP_PI_MENU()
    //set menu start values
    sPI_MenuData.iCurrentSelection = ciPI_TYPE_QUICK_GPS   
    iFastSelectSelfie =  iCurrentSelfie 
	
   	GET_ACTUAL_SCREEN_RESOLUTION(iOldScrX,iOldScrY)     
    GET_COMP_DATA()
    HAVE_SF_ASSETS_LOADED() 
    INIT_PI_MENU_HEADER(sPIM_HeaderData)
    bResetMenuNow = TRUE
    iPI_MoveTimer = get_Game_timer()
        
ENDPROC

FUNC BOOL SWITCH_MENU_STATE()
    if not IS_BIT_SET(iBoolsBitSet, biSwitchButtonCheck)    
        //Is PI menu button down for PI_MENU_DELAY(350ms)
        bool bMenubuttonPressed = False
        if not IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
            IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_INTERACTION_MENU)
                bMenubuttonPressed = true
            endif
        else
            IF IS_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_INTERACTION_MENU)
                bMenubuttonPressed = true
            endif
        endif
        
        IF bMenubuttonPressed
        and not MENU_OFF_CHECK()    
        and not IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_FRONTEND_PAUSE)//Stat button cant be pressed at the same time 
            IF not IS_USING_KEYBOARD_AND_MOUSE(PLAYER_CONTROL)
                IF get_Game_timer() - iPi_menu_timer >= PI_MENU_DELAY
                    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
                    CPRINTLN(DEBUG_MISSION, "---CV biSwitchButtonCheck: SET PAD")
                    SET_BIT(iBoolsBitSet, biSwitchButtonCheck)
                    iPi_menu_timer = get_Game_timer()
                    PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)  
                    RETURN TRUE
                ENDIF       
            else
                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
                CPRINTLN(DEBUG_MISSION, "---CV biSwitchButtonCheck: SET keyboard & mouse")
                SET_BIT(iBoolsBitSet, biSwitchButtonCheck)
                iPi_menu_timer = get_Game_timer()
                PLAY_SOUND_FRONTEND(-1, "SELECT", "HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE) 
                RETURN TRUE
            endif
        ELSE
            iPi_menu_timer = get_Game_timer()
            RETURN FALSE
        ENDIF
    else
        IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_INTERACTION_MENU)      
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
        ELSE    
            CPRINTLN(DEBUG_MISSION, "---CV biSwitchButtonCheck: CLEAR")
            CLEAR_BIT(iBoolsBitSet, biSwitchButtonCheck)
        ENDIF
    endif
    RETURN FALSE    
ENDFUNC

PROC PROCESS_LOCAL_PLAYER_MENU()
    //TITLE
    
    SET_MENU_TITLE("PIM_TITLE1")    //HIDE")    //PIM_TITLE0")  //PIM_TITLE1")
    SET_MENU_USES_HEADER_GRAPHIC((NOT IS_STRING_NULL_OR_EMPTY(sGraphicHeader)), sGraphicHeader, sGraphicHeader)
    INT iRed, iGreen, iBlue, iRedF, iGreenF, iBlueF
    IF GET_CHARACTER_COLOUR_PI(iRed, iGreen, iBlue, iRedF, iGreenF, iBlueF)
        //SET_MENU_HEADER_COLOUR(iRed, iGreen, iBlue, 255, TRUE)
        SET_MENU_HEADER_TEXT_COLOUR(iRedF, iGreenF, iBlueF, 255, TRUE)
        SET_MENU_FOOTER_COLOUR(iRedF, iGreenF, iBlueF, 255, TRUE)
        SET_MENU_USES_INVERTED_SCROLL_ICON(TRUE)
    ELSE
        //SET_MENU_HEADER_COLOUR(iRed, iGreen, iBlue, 255, FALSE)
        SET_MENU_HEADER_TEXT_COLOUR(iRedF, iGreenF, iBlueF, 255, FALSE)
        SET_MENU_FOOTER_COLOUR(iRedF, iGreenF, iBlueF, 255, FALSE)
        SET_MENU_USES_INVERTED_SCROLL_ICON(FALSE)
    ENDIF
    sPIM_HeaderData.Background.w = CUSTOM_MENU_W
    fMenuExtension = CUSTOM_MENU_W
    
    BOOL bUseArrows = FALSE
    BOOL bAvailable = FALSE
        
    //QUICK GPS
    ADD_MENU_ITEM_TEXT(ciPI_TYPE_QUICK_GPS, "PIM_TQGPS", 0,  TRUE)
    sPI_MenuData.tl15 = "PIM_QGPS0"  
    eCurrentGPS = eGPS_NONE // set values to minimum then check for unlocked shops
     
    
    int iMissionsAvailable[MAX_MISSION_TRIGGERS]
    int triggerLoop
    enumCharacterList eCurrentPlayer = GET_CURRENT_PLAYER_PED_ENUM()
    
    //NONE
    ciPI_MAX_GPS = 0
    
    //HOME
    ciPI_MAX_GPS++
    
    //missions 
    repeat MAX_MISSION_TRIGGERS triggerLoop
        if g_TriggerableMissions[triggerLoop].eMissionID != SP_MISSION_NONE 
        and g_TriggerableMissions[triggerLoop].eMissionID != SP_HEIST_FINALE_PREP_C1    
        and g_TriggerableMissions[triggerLoop].eMissionID != SP_HEIST_FINALE_PREP_C2
        and g_TriggerableMissions[triggerLoop].eMissionID != SP_HEIST_FINALE_PREP_C3
            IF Is_Mission_Triggerable_By_Character(g_sMissionStaticData[g_TriggerableMissions[triggerLoop].eMissionID].triggerCharBitset, eCurrentPlayer)
            and IS_STATIC_BLIP_CURRENTLY_VISIBLE(g_TriggerableMissions[triggerLoop].eBlip)          
                if IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_OFF_MISSION)
                or IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_EVENT)
                    ciPI_MAX_GPS++
                    iMissionsAvailable[triggerLoop] = ciPI_MAX_GPS
                endif
            endif
        endif
    endrepeat
    
    //ATM   
    ciPI_MAX_GPS++
    int iAtmAvailable = ciPI_MAX_GPS
    
    //Are gun shops open
    int iGunshopAvailable = -1
    if IS_SHOP_AVAILABLE(GUN_SHOP_01_DT) or IS_SHOP_AVAILABLE(GUN_SHOP_02_SS)or IS_SHOP_AVAILABLE(GUN_SHOP_03_HW)
    or IS_SHOP_AVAILABLE(GUN_SHOP_04_ELS)or IS_SHOP_AVAILABLE(GUN_SHOP_05_PB)or IS_SHOP_AVAILABLE(GUN_SHOP_06_LS) 
    or IS_SHOP_AVAILABLE(GUN_SHOP_07_MW) or IS_SHOP_AVAILABLE(GUN_SHOP_08_CS)or IS_SHOP_AVAILABLE(GUN_SHOP_09_GOH)
    or IS_SHOP_AVAILABLE(GUN_SHOP_10_VWH) or IS_SHOP_AVAILABLE(GUN_SHOP_11_ID1)     
        ciPI_MAX_GPS++
        iGunshopAvailable = ciPI_MAX_GPS
    endif
    
    //mod shops
    int iCarShopsAvailable = -1
    if IS_SHOP_AVAILABLE(CARMOD_SHOP_01_AP) or IS_SHOP_AVAILABLE(CARMOD_SHOP_05_ID2)or IS_SHOP_AVAILABLE(CARMOD_SHOP_06_BT1)
    or IS_SHOP_AVAILABLE(CARMOD_SHOP_07_CS1)or IS_SHOP_AVAILABLE(CARMOD_SHOP_08_CS6)
	or IS_SHOP_AVAILABLE(CARMOD_SHOP_SUPERMOD)
        ciPI_MAX_GPS++
        iCarShopsAvailable = ciPI_MAX_GPS
    endif
    //clothes shop
    int ilowClothesAvailable = -1
    if IS_SHOP_AVAILABLE(CLOTHES_SHOP_L_01_SC) or IS_SHOP_AVAILABLE(CLOTHES_SHOP_L_02_GS)or IS_SHOP_AVAILABLE(CLOTHES_SHOP_L_03_DT)
    or IS_SHOP_AVAILABLE(CLOTHES_SHOP_L_04_CS)or IS_SHOP_AVAILABLE(CLOTHES_SHOP_L_05_GSD)or IS_SHOP_AVAILABLE(CLOTHES_SHOP_L_06_VC) 
    or IS_SHOP_AVAILABLE(CLOTHES_SHOP_L_07_PB)
        ciPI_MAX_GPS++
        ilowClothesAvailable = ciPI_MAX_GPS
    endif
    
    int imedClothesAvailable = -1
    if IS_SHOP_AVAILABLE(CLOTHES_SHOP_M_01_SM)or IS_SHOP_AVAILABLE(CLOTHES_SHOP_M_03_H)
    or IS_SHOP_AVAILABLE(CLOTHES_SHOP_M_04_HW)or IS_SHOP_AVAILABLE(CLOTHES_SHOP_M_05_GOH)   
        ciPI_MAX_GPS++
        imedClothesAvailable = ciPI_MAX_GPS
    endif
    
    int ihighClothesAvailable = -1
    if IS_SHOP_AVAILABLE(CLOTHES_SHOP_H_01_BH)
    or IS_SHOP_AVAILABLE(CLOTHES_SHOP_H_02_B)or IS_SHOP_AVAILABLE(CLOTHES_SHOP_H_03_MW)     
        ciPI_MAX_GPS++
        ihighClothesAvailable = ciPI_MAX_GPS
    endif
    
    //mask shop
    int iMasksAvailable = -1
    if IS_SHOP_AVAILABLE(CLOTHES_SHOP_A_01_VB)
        ciPI_MAX_GPS++
        iMasksAvailable = ciPI_MAX_GPS
    endif
    //hair shop
    int ihairAvailable = -1
    if IS_SHOP_AVAILABLE(HAIRDO_SHOP_01_BH) or IS_SHOP_AVAILABLE(HAIRDO_SHOP_02_SC)or IS_SHOP_AVAILABLE(HAIRDO_SHOP_03_V)
    or IS_SHOP_AVAILABLE(HAIRDO_SHOP_04_SS)or IS_SHOP_AVAILABLE(HAIRDO_SHOP_05_MP)or IS_SHOP_AVAILABLE(HAIRDO_SHOP_06_HW)   
    or IS_SHOP_AVAILABLE(HAIRDO_SHOP_07_PB)
        ciPI_MAX_GPS++
        ihairAvailable = ciPI_MAX_GPS
    endif
    //tattoo shop
    int iTattooAvailable = -1
    if IS_SHOP_AVAILABLE(TATTOO_PARLOUR_01_HW) or IS_SHOP_AVAILABLE(TATTOO_PARLOUR_02_SS)or IS_SHOP_AVAILABLE(TATTOO_PARLOUR_03_PB)
    or IS_SHOP_AVAILABLE(TATTOO_PARLOUR_04_VC)or IS_SHOP_AVAILABLE(TATTOO_PARLOUR_05_ELS)or IS_SHOP_AVAILABLE(TATTOO_PARLOUR_06_GOH)
        ciPI_MAX_GPS++
        iTattooAvailable = ciPI_MAX_GPS
    endif
     
    //Garage
    int iGarageAvailable = -1
    VEHICLE_GEN_NAME_ENUM ename
    if GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
        eName = VEHGEN_WEB_CAR_MICHAEL
    elif GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
        eName = VEHGEN_WEB_CAR_FRANKLIN
    elif GET_CURRENT_PLAYER_PED_ENUM() = CHAR_TREVOR
        eName = VEHGEN_WEB_CAR_TREVOR 
    ENDIF
    
    if DOES_BLIP_EXIST(g_sVehicleGenNSData.blipID[eName])
        ciPI_MAX_GPS++
        iGarageAvailable =  ciPI_MAX_GPS    
    ENDIF   
        
    int iGPS    
    triggerLoop = 0 
    for iGPS = 0 to ciPI_MAX_GPS            
        if sSelection.iSelection[ciPI_SUB_MENU_NONE][ciPI_TYPE_QUICK_GPS] = iGPS
            if   iGPS = enum_to_int(eGPS_NONE)
                eCurrentGPS = eGPS_NONE
            	sPI_MenuData.tl15 = "PIM_QGPS0"     //sPI_MenuData.tl15 += sSelection.iSelection[ciPI_SUB_MENU_NONE][ciPI_TYPE_QUICK_GPS]   
            elif iGPS = enum_to_int(eGPS_HOME)
                eCurrentGPS = eGPS_HOME 
                sPI_MenuData.tl15 = "BLIP_40"//safehouse                            
            elif iGPS = iAtmAvailable
                eCurrentGPS = eGPS_ATM
                sPI_MenuData.tl15 = "SPPIM_ATM"//atm
            elif iGPS = iGunshopAvailable
                eCurrentGPS = eGPS_GUN
                sPI_MenuData.tl15 = "BLIP_110"//gun
            elif iGPS = iCarShopsAvailable
                eCurrentGPS = eGPS_MOD
                sPI_MenuData.tl15 = "BLIP_72"//mod
            elif iGPS = ilowClothesAvailable
                eCurrentGPS = eGPS_lowCLOTHES
                sPI_MenuData.tl15 = "PIM_LECLTH"//clothes
            elif iGPS = imedClothesAvailable
                eCurrentGPS = eGPS_medCLOTHES
                sPI_MenuData.tl15 = "PIM_MECLTH"//clothes
            elif iGPS = ihighClothesAvailable
                eCurrentGPS = eGPS_highCLOTHES
                sPI_MenuData.tl15 = "PIM_HECLTH"//clothes   
            elif iGPS = iMasksAvailable
                eCurrentGPS = eGPS_MASK
                sPI_MenuData.tl15 = "BLIP_362"//mask
            elif iGPS = ihairAvailable
                eCurrentGPS = eGPS_BARBER
                sPI_MenuData.tl15 = "BLIP_71"//barber       
            elif iGPS = iTattooAvailable
                eCurrentGPS = eGPS_TATTOO
                sPI_MenuData.tl15 = "BLIP_75"//tattoo  
            elif iGPS = iGarageAvailable
                eCurrentGPS = eGPS_GARAGE
                sPI_MenuData.tl15 = "BLIP_357"//Garage  
            else                
                repeat MAX_MISSION_TRIGGERS triggerLoop
                    if iGPS = iMissionsAvailable[triggerLoop]
                        eCurrentGPS = INT_TO_ENUM(ENUM_GPS_LIST, ENUM_TO_INT(eGPS_MISSION1) + triggerLoop)
                        TEXT_LABEL_15 txtTemp
                        txtTemp = GET_STRAND_LABEL(g_TriggerableMissions[triggerLoop].eMissionID)
                        sPI_MenuData.tl15 = txtTemp                     
                    endif
                endrepeat   
            endif
        endif       
    endfor
    
    bAvailable = TRUE
    
    //GPS    
    ADD_MENU_ITEM_TEXT(ciPI_TYPE_QUICK_GPS, sPI_MenuData.tl15, 0, bAvailable)
    if GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Michael1")) > 0 
        bAvailable = false
    endif   
    IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_QUICK_GPS
        IF bAvailable = TRUE
            bSelectAvailable = TRUE
        ELSE
            bSelectAvailable = FALSE
        ENDIF
    ENDIF
        
    //INVENTORY
    if SAFE_TO_USE_INVENTORY()
        ADD_MENU_ITEM_TEXT(ciPI_TYPE_INVENTORY, "PIM_TINVE", 0, TRUE)
        IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_INVENTORY
            bSelectAvailable = TRUE
        ENDIF
    else
        ADD_MENU_ITEM_TEXT(ciPI_TYPE_INVENTORY, "PIM_TINVE", 0, FALSE)
        IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_INVENTORY
            bSelectAvailable = FALSE
        ENDIF
    endif
    
        
    //Objective
    if IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
    and not IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
    and not IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
    and not IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME_FRIENDS)
    and not IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME)
    and not IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_EVENT)
    and DOES_LATEST_BRIEF_STRING_EXIST(PREV_MESSAGE_TYPE_MISSION)
        ADD_MENU_ITEM_TEXT(ciPI_TYPE_OBJECTIVE, "PIM_TOBJ", 0, TRUE)
        IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_OBJECTIVE
            bSelectAvailable = TRUE
        ENDIF
    else
        ADD_MENU_ITEM_TEXT(ciPI_TYPE_OBJECTIVE, "PIM_TOBJ", 0, FALSE)
        IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_OBJECTIVE
            bSelectAvailable = FALSE
        ENDIF
    endif
    
    //Help Text
    if  DOES_LATEST_BRIEF_STRING_EXIST(PREV_MESSAGE_TYPE_HELP)
        ADD_MENU_ITEM_TEXT(ciPI_TYPE_HELPTEXT, "PIM_THELP", 0, TRUE)
        IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_HELPTEXT      
            bSelectAvailable = TRUE     
        ENDIF
    else
        ADD_MENU_ITEM_TEXT(ciPI_TYPE_HELPTEXT, "PIM_THELP", 0, FALSE)
        IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_HELPTEXT      
            bSelectAvailable = FALSE        
        ENDIF
    endif
    
    //Director Mode
    #IF FEATURE_SP_DLC_DIRECTOR_MODE
		FEATURE_BLOCK_REASON newBlockReason = GET_FEATURE_BLOCKED_REASON()
		IF lastBlockReason != newBlockReason
		//block reason updated, force update PI menu
			bResetMenuNow = TRUE
			lastBlockReason = newBlockReason
		ENDIF
        IF newBlockReason = FBR_NONE
            ADD_MENU_ITEM_TEXT(ciPI_TYPE_DIRECTOR, "CM_IN_TIT", 0, TRUE)
            IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_DIRECTOR  
                bSelectAvailable = TRUE
            ENDIF
        ELSE
            ADD_MENU_ITEM_TEXT(ciPI_TYPE_DIRECTOR, "CM_IN_TIT", 0, FALSE)
            IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_DIRECTOR  
                bSelectAvailable = FALSE
            ENDIF
        ENDIF
    #ENDIF
	
	
	
    
    IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_QUICK_GPS
        bUseArrows = TRUE
    ENDIF
    
    //Control Arrows
    IF bUseArrows
    and bAvailable
        SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
        bUseSelectArrows = true
    ELSE
        SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)
        bUseSelectArrows = false
    ENDIF
    
ENDPROC
PROC PROCESS_LOCAL_INVENTORY_MENU()

    PED_COMP_ITEM_DATA_STRUCT ItemData  
    MODEL_NAMES PlayerModel = GET_PLAYER_PED_MODEL(GET_CURRENT_PLAYER_PED_ENUM())
    TEXT_LABEL_15 sShortVersionLabel
    
//  is_player
    BOOL bAvailable
//  BOOL bPlayerInVehicle   = IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
    BOOL bPlayerOnBike      = IS_PED_ON_ANY_BIKE(PLAYER_PED_ID())

    SET_MENU_TITLE("PIM_TITLE2")
    SET_MENU_USES_HEADER_GRAPHIC((NOT IS_STRING_NULL_OR_EMPTY(sGraphicHeader)), sGraphicHeader, sGraphicHeader)
    INT iRed, iGreen, iBlue, iRedF, iGreenF, iBlueF
    IF GET_CHARACTER_COLOUR_PI(iRed, iGreen, iBlue, iRedF, iGreenF, iBlueF)
        //SET_MENU_HEADER_COLOUR(iRed, iGreen, iBlue, 255, TRUE)
        SET_MENU_HEADER_TEXT_COLOUR(iRedF, iGreenF, iBlueF, 255, TRUE)
        SET_MENU_FOOTER_COLOUR(iRedF, iGreenF, iBlueF, 255, TRUE)
        SET_MENU_USES_INVERTED_SCROLL_ICON(TRUE)
    ELSE
        //SET_MENU_HEADER_COLOUR(iRed, iGreen, iBlue, 255, FALSE)
        SET_MENU_HEADER_TEXT_COLOUR(iRedF, iGreenF, iBlueF, 255, FALSE)
        SET_MENU_FOOTER_COLOUR(iRedF, iGreenF, iBlueF, 255, FALSE)
        SET_MENU_USES_INVERTED_SCROLL_ICON(FALSE)
    ENDIF
    
    
    sPIM_HeaderData.Background.w    = CUSTOM_MENU_W //+ 0.05
    fMenuExtension                  = CUSTOM_MENU_W //+ 0.05
    
//HATS  
    bAvailable = TRUE
    IF iHatTotalCount <= 1 
    OR bPlayerOnBike
        bAvailable = TRUE//FALSE #1879097 - none barely visible with new gradient texture
    ENDIF
    ADD_MENU_ITEM_TEXT(ciPI_TYPE_M2_HATS, "PIM_THELM", 0, TRUE)
    IF sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_HATS] > 0
        ItemData = GET_PED_COMP_DATA_FOR_ITEM_SP(PlayerModel,HatsData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_HATS]].eType,HatsData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_HATS]].eName)
    else
        ItemData.sLabel = "PIM_NHELM"
    endif
    
    sShortVersionLabel = GET_SHORT_LABEL(ItemData.sLabel)
    
    ADD_MENU_ITEM_TEXT(ciPI_TYPE_M2_HATS, sShortVersionLabel, 0, bAvailable)
    IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_M2_HATS
    AND bAvailable = TRUE
        bSelectAvailable = TRUE
    ENDIF
    
//GLASSES
    ADD_MENU_ITEM_TEXT(ciPI_TYPE_M2_GLASSES, "PIM_TGLAS", 0, TRUE)
    IF sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_GLASSES] > 0
        ItemData = GET_PED_COMP_DATA_FOR_ITEM_SP(PlayerModel,GlassesData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_GLASSES]].eType,GlassesData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_GLASSES]].eName)
        //Names that are too long for glasses shorterned
        if GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL     
            if GlassesData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_GLASSES]].eName = PROPS_P0_GLASSES_THICK_RIM_1
                ItemData.sLabel = "PIM_SHDE1"//Hawaiian snow charcoal
            elif GlassesData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_GLASSES]].eName = PROPS_P0_GLASSES_THICK_RIM_5
                ItemData.sLabel = "PIM_SHDE2"//Hawaiian snow Tortoiseshell                          
            elif GlassesData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_GLASSES]].eName = PROPS_P0_GLASSES_THICK_RIM_7
                ItemData.sLabel = "PIM_SHDE3"//Hawaiian snow marbled
            elif GlassesData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_GLASSES]].eName = PROPS_P0_GLASSES_THICK_RIM_6
                ItemData.sLabel = "PIM_SHDE4"//Hawaiian snow walnut 
            endif   
        elif GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN  
            if GlassesData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_GLASSES]].eName = PROPS_P1_SUNGLASSES_F_2
                ItemData.sLabel = "PIM_SHDE5"//Suburban Tortoiseshell
            endif   
        endif
    else
        ItemData.sLabel = "PIM_NGLAS"
    endif
    sShortVersionLabel = GET_SHORT_LABEL(ItemData.sLabel)
    ADD_MENU_ITEM_TEXT(ciPI_TYPE_M2_GLASSES, sShortVersionLabel, 0, true)//(iGlassesTotalCount > 1))#1879097 - none barely visible with new gradient texture
    IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_M2_GLASSES
        bSelectAvailable = TRUE
    ENDIF
    
//MASK  
    bAvailable = TRUE
    IF iMaskTotalCount <= 1 
    OR bPlayerOnBike
        bAvailable = TRUE//FALSE #1879097 - none barely visible with new gradient texture
    ENDIF
    ADD_MENU_ITEM_TEXT(ciPI_TYPE_M2_MASKS, "PIM_TMASK", 0, TRUE)
    IF sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_MASKS] > 0
        ItemData = GET_PED_COMP_DATA_FOR_ITEM_SP(PlayerModel, MaskData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_MASKS]].eType, MaskData[sSelection.iSelection[iSubMenu][ciPI_TYPE_M2_MASKS]].eName)
    else
        ItemData.sLabel = "PIM_NHELM1"
    endif
    
    sShortVersionLabel = GET_SHORT_LABEL(ItemData.sLabel)
    ADD_MENU_ITEM_TEXT(ciPI_TYPE_M2_MASKS, sShortVersionLabel, 0, bAvailable)
    IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_M2_MASKS
    AND bAvailable = TRUE
        bSelectAvailable = TRUE
    ENDIF
    
    
//Control Arrows
    IF (sPI_MenuData.iCurrentSelection = ciPI_TYPE_M2_MASKS         AND bSelectAvailable = TRUE)    
    OR (sPI_MenuData.iCurrentSelection = ciPI_TYPE_M2_HATS          AND bSelectAvailable = TRUE)
        SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
        bUseSelectArrows = TRUE
    Elif (sPI_MenuData.iCurrentSelection = ciPI_TYPE_M2_GLASSES         AND iGlassesTotalCount > 1)
        SET_MENU_ITEM_TOGGLEABLE(FALSE, TRUE)
        bUseSelectArrows = TRUE
    else    
        SET_MENU_ITEM_TOGGLEABLE(FALSE, FALSE)
    ENDIF
    
ENDPROC


// draw the sf movie
PROC SET_BRIEF_TEXT_MENU(bool bObjective)       
    if HAVE_SF_ASSETS_LOADED()          
        BEGIN_SCALEFORM_MOVIE_METHOD(sfMenu,"SET_TEXT")     
            if bObjective
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_LATEST_BRIEF_STRING(PREV_MESSAGE_TYPE_MISSION)
            else
                SCALEFORM_MOVIE_METHOD_ADD_PARAM_LATEST_BRIEF_STRING(PREV_MESSAGE_TYPE_HELP)
            endif
        END_SCALEFORM_MOVIE_METHOD()    
        bResetMenuNow = true
    endif
ENDPROC
PROC PROCESS_LOCAL_OBJECTIVE_MENU()
    SET_MENU_TITLE("PIM_TITLE1")
    SET_MENU_USES_HEADER_GRAPHIC((NOT IS_STRING_NULL_OR_EMPTY(sGraphicHeader)), sGraphicHeader, sGraphicHeader)
    INT iRed, iGreen, iBlue, iRedF, iGreenF, iBlueF
    IF GET_CHARACTER_COLOUR_PI(iRed, iGreen, iBlue, iRedF, iGreenF, iBlueF)
        //SET_MENU_HEADER_COLOUR(iRed, iGreen, iBlue, 255, TRUE)
        SET_MENU_HEADER_TEXT_COLOUR(iRedF, iGreenF, iBlueF, 255, TRUE)
        SET_MENU_FOOTER_COLOUR(iRedF, iGreenF, iBlueF, 255, TRUE)
        SET_MENU_USES_INVERTED_SCROLL_ICON(TRUE)
    ELSE
        //SET_MENU_HEADER_COLOUR(iRed, iGreen, iBlue, 255, FALSE)
        SET_MENU_HEADER_TEXT_COLOUR(iRedF, iGreenF, iBlueF, 255, FALSE)
        SET_MENU_FOOTER_COLOUR(iRedF, iGreenF, iBlueF, 255, FALSE)
        SET_MENU_USES_INVERTED_SCROLL_ICON(FALSE)
    ENDIF
    ADD_MENU_ITEM_TEXT(0, "PIM_TOBJ", 0)    
    SET_BRIEF_TEXT_MENU(true)
ENDPROC
PROC PROCESS_LOCAL_HELPTEXT_MENU()
    SET_MENU_TITLE("PIM_TITLE1")    
    SET_MENU_USES_HEADER_GRAPHIC((NOT IS_STRING_NULL_OR_EMPTY(sGraphicHeader)), sGraphicHeader, sGraphicHeader)
    INT iRed, iGreen, iBlue, iRedF, iGreenF, iBlueF
    IF GET_CHARACTER_COLOUR_PI(iRed, iGreen, iBlue, iRedF, iGreenF, iBlueF)
        //SET_MENU_HEADER_COLOUR(iRed, iGreen, iBlue, 255, TRUE)
        SET_MENU_HEADER_TEXT_COLOUR(iRedF, iGreenF, iBlueF, 255, TRUE)
        SET_MENU_FOOTER_COLOUR(iRedF, iGreenF, iBlueF, 255, TRUE)
        SET_MENU_USES_INVERTED_SCROLL_ICON(TRUE)
    ELSE
        //SET_MENU_HEADER_COLOUR(iRed, iGreen, iBlue, 255, FALSE)
        SET_MENU_HEADER_TEXT_COLOUR(iRedF, iGreenF, iBlueF, 255, FALSE)
        SET_MENU_FOOTER_COLOUR(iRedF, iGreenF, iBlueF, 255, FALSE)
        SET_MENU_USES_INVERTED_SCROLL_ICON(FALSE)
    ENDIF

	
    ADD_MENU_ITEM_TEXT(0, "PIM_THELP", 0)
    SET_BRIEF_TEXT_MENU(false)  
ENDPROC



PROC PROCESS_BELOW_MENU_HELP()
    SWITCH iSubMenu
        CASE ciPI_SUB_MENU_NONE
            IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_QUICK_GPS
                if GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Michael1")) > 0 
                    SET_CURRENT_MENU_ITEM_DESCRIPTION("PIM_GPSOFF")
                else
                    SET_CURRENT_MENU_ITEM_DESCRIPTION("PIM_HQGPS")
                endif               
            ELIF sPI_MenuData.iCurrentSelection = ciPI_TYPE_INVENTORY
                if SAFE_TO_USE_INVENTORY()
                    SET_CURRENT_MENU_ITEM_DESCRIPTION("PIM_INV")
                ELSE
                    SET_CURRENT_MENU_ITEM_DESCRIPTION("PIM_INVOFF")
                ENDIF
            ELIF sPI_MenuData.iCurrentSelection = ciPI_TYPE_OBJECTIVE
                if IS_CURRENTLY_ON_MISSION_OF_ANY_TYPE()
                and not IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY)
                and not IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_FRIEND_ACTIVITY_WITH_MG)
                and not IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME_FRIENDS)
                and not IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME)
                and not IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_RANDOM_EVENT)
                and DOES_LATEST_BRIEF_STRING_EXIST(PREV_MESSAGE_TYPE_MISSION)
                    SET_CURRENT_MENU_ITEM_DESCRIPTION("PIM_THOBJ")
                else
                    SET_CURRENT_MENU_ITEM_DESCRIPTION("PIM_OBJOFF")
                endif
            ELIF sPI_MenuData.iCurrentSelection = ciPI_TYPE_HELPTEXT
                if DOES_LATEST_BRIEF_STRING_EXIST(PREV_MESSAGE_TYPE_HELP)
                    SET_CURRENT_MENU_ITEM_DESCRIPTION("PIM_THHELP") 
                else
                    SET_CURRENT_MENU_ITEM_DESCRIPTION("PIM_HELPOF")
                endif

            #IF FEATURE_SP_DLC_DIRECTOR_MODE

            ELIF sPI_MenuData.iCurrentSelection = ciPI_TYPE_DIRECTOR
                SWITCH GET_FEATURE_BLOCKED_REASON(default,default,DEBUG_DIRECTOR)
					CASE FBR_NONE              	SET_CURRENT_MENU_ITEM_DESCRIPTION( "PIM_DIRECT" )     	BREAK
					CASE FBR_FORCE_CLEANUP		SET_CURRENT_MENU_ITEM_DESCRIPTION( "FBR_BLK_CLEAN" )	BREAK
					CASE FBR_RUNNING_CURR		SET_CURRENT_MENU_ITEM_DESCRIPTION( "DI_BLK_RNNNG" )		BREAK
					CASE FBR_DEAD				SET_CURRENT_MENU_ITEM_DESCRIPTION( "DI_BLK_DEAD" )		BREAK
					CASE FBR_ON_MISSION			SET_CURRENT_MENU_ITEM_DESCRIPTION( "DI_BLK_MISS" ) 		BREAK
					CASE FBR_IN_SHOP			SET_CURRENT_MENU_ITEM_DESCRIPTION( "DI_BLK_SHOP" )		BREAK
					CASE FBR_IN_CUTSCENE		SET_CURRENT_MENU_ITEM_DESCRIPTION( "DI_BLK_CUTS" )		BREAK
					CASE FBR_WANTED				SET_CURRENT_MENU_ITEM_DESCRIPTION( "DI_BLK_WANT" )		BREAK
					CASE FBR_ONLINE				SET_CURRENT_MENU_ITEM_DESCRIPTION( "DI_BLK_ONLI" )		BREAK
					CASE FBR_UNSAFE_ACTION		SET_CURRENT_MENU_ITEM_DESCRIPTION( "DI_BLK_ACT" )		BREAK
					CASE FBR_UNSAFE_LOCATION	SET_CURRENT_MENU_ITEM_DESCRIPTION( "DI_BLK_LOC" )		BREAK
					CASE FBR_IN_VEHICLE			SET_CURRENT_MENU_ITEM_DESCRIPTION( "DI_BLK_VEH" )		BREAK
					CASE FBR_WEARING_PARA		SET_CURRENT_MENU_ITEM_DESCRIPTION( "DI_BLK_PARA" )		BREAK
					CASE FBR_FALLING			SET_CURRENT_MENU_ITEM_DESCRIPTION( "DI_BLK_FALL" )		BREAK
					DEFAULT		
						CASSERTLN(DEBUG_DIRECTOR, " - pi_menu - PROCESS_BELOW_MENU_HELP: Feature is blocked for a reason that is not being handled. RowanJ")		
					BREAK				
                ENDSWITCH

            #ENDIF
			
			

            ENDIF
        BREAK
        CASE ciPI_SUB_MENU_INVENTORY            
            IF sPI_MenuData.iCurrentSelection = ciPI_TYPE_M2_HATS
                SET_CURRENT_MENU_ITEM_DESCRIPTION("PIM_HATS")
            elIF sPI_MenuData.iCurrentSelection = ciPI_TYPE_M2_GLASSES
                SET_CURRENT_MENU_ITEM_DESCRIPTION("PIM_HGLAS")
            elIF sPI_MenuData.iCurrentSelection = ciPI_TYPE_M2_MASKS
                SET_CURRENT_MENU_ITEM_DESCRIPTION("PIM_HMASK")
            endif               
        BREAK
		
    ENDSWITCH
ENDPROC
PROC SETUP_PI_MENU()

	//	B* 2147964 - very hacky fix using magic numbers. 
	//	Positions the brief text depending on the aspect ratio 
	//	(which changes the height a pos of the menu)
	FLOAT fAspectMulti = GET_ASPECT_RATIO_MULTIPLIER()
	FLOAT fTempMulti = ( ( ( fAspectMulti - 1.0 ) / sf_y_adj ) + 1.0 )	//	Magic number ask Rowan Jones/ Ben Rollinson if required edit
	sf_y = 0.754 * fTempMulti
	wl_y = sf_y - 0.501	//	White line const distance from breif box

    bSelectAvailable = FALSE
    bUseSelectArrows = FALSE
    CLEAR_MENU_DATA( TRUE )
    
    //STRUCTURE
    SET_MENU_ITEM_LAYOUT(MENU_ITEM_TEXT, MENU_ITEM_TEXT)
    SET_MENU_ITEM_JUSTIFICATION(FONT_LEFT, FONT_RIGHT)
    
    #IF FEATURE_SP_DLC_DIRECTOR_MODE
		 SET_MAX_MENU_ROWS_TO_DISPLAY(5)
    #ENDIF
    #IF not FEATURE_SP_DLC_DIRECTOR_MODE
        SET_MAX_MENU_ROWS_TO_DISPLAY(4)
    #ENDIF
    
    
    INT iHudR, iHudG, iHudB, iHudA
    HUD_COLOURS hudColour
    if GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
        hudColour = HUD_COLOUR_MICHAEL
    elif GET_CURRENT_PLAYER_PED_ENUM() = CHAR_FRANKLIN
        hudColour = HUD_COLOUR_FRANKLIN
    else 
        hudColour = HUD_COLOUR_TREVOR
    endif
    GET_HUD_COLOUR(hudColour, iHudR, iHudG, iHudB, iHudA)
    SET_MENU_FOOTER_COLOUR(iHudR, iHudG, iHudB, iHudA, TRUE)
//  g_sMenuData.bForceFooter = TRUE
    
    SWITCH iSubMenu
        //setup main menu
        CASE ciPI_SUB_MENU_NONE 
            PROCESS_LOCAL_PLAYER_MENU()      
            IF bSelectAvailable = TRUE
                ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CELLPHONE_SELECT, "PIM_CSEL")           //SELECT
            endif
            ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CELLPHONE_CANCEL, "PIM_CEXI")           //EXIT
        BREAK
        //Setup Inventory Menu
        CASE ciPI_SUB_MENU_INVENTORY
//          if SAFE_TO_USE_INVENTORY()
                PROCESS_LOCAL_INVENTORY_MENU()
                IF bSelectAvailable = TRUE
                    IF (sPI_MenuData.iCurrentSelection = ciPI_TYPE_M2_HATS AND iHatTotalCount > 1)
                    OR (sPI_MenuData.iCurrentSelection = ciPI_TYPE_M2_GLASSES AND iGlassesTotalCount > 1)
                    OR (sPI_MenuData.iCurrentSelection = ciPI_TYPE_M2_MASKS AND iMaskTotalCount > 1)
						ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CELLPHONE_SELECT, "PIM_CNEX")       //NEXT
                    endif
                endif
//          endif
            ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CELLPHONE_CANCEL, "PIM_CEXI")           //EXIT
        BREAK
        //Setup Objective Menu
        CASE ciPI_SUB_MENU_OBJECTIVE
            PROCESS_LOCAL_OBJECTIVE_MENU()
            ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CELLPHONE_CANCEL, "PIM_CEXI")           //EXIT
        BREAK
        //Setup Help Menu
        CASE ciPI_SUB_MENU_HELPTEXT
            PROCESS_LOCAL_HELPTEXT_MENU()
            ADD_MENU_HELP_KEY_CLICKABLE(INPUT_CELLPHONE_CANCEL, "PIM_CEXI")           //EXIT
        BREAK
		
		
    ENDSWITCH
        
    SET_CURRENT_MENU_ITEM(sPI_MenuData.iCurrentSelection)
    SET_CURRENT_MENU_ITEM_DESCRIPTION("") // Pass in "" to clear
    
    
ENDPROC
PROC Maintain_Active_PI_Menu()
    IF LOAD_MENU_ASSETS()
    AND NOT IS_WARNING_MESSAGE_ACTIVE()
    
        IF NOT IS_BIT_SET(iBoolsBitSet, biM_MenuSetup)
        or bResetMenuNow        
            SETUP_PI_MENU()
            bResetMenuNow = FALSE
            iMenuUpdateTimer = get_Game_timer()
            
        ELSE
            if Get_Game_timer() - iMenuUpdateTimer > MENU_UPDATE_TIME
                bResetMenuNow = TRUE
            endif
        ENDIF
        
        IF NOT IS_BIT_SET(iBoolsBitSet, biM_MenuSetup)          
            SET_USER_RADIO_CONTROL_ENABLED(FALSE)
            SET_BIT(iBoolsBitSet, biM_MenuSetup)
        ENDIF
        if iSubMenu = ciPI_SUB_MENU_OBJECTIVE
        or iSubMenu = ciPI_SUB_MENU_HELPTEXT
            if HAVE_SF_ASSETS_LOADED()
                SET_SCRIPT_GFX_ALIGN(UI_ALIGN_LEFT, UI_ALIGN_TOP)
                SET_SCRIPT_GFX_ALIGN_PARAMS(fAlignX, fAlignY, 0.0, 0.0)
				ADJUST_NEXT_POS_SIZE_AS_NORMALIZED_16_9() //b* 2147964
                DRAW_SCALEFORM_MOVIE(sfMenu,sf_x,sf_y,sf_w,sf_h, 255, 255, 255, 255)
				ADJUST_NEXT_POS_SIZE_AS_NORMALIZED_16_9() //b* 2147964
                DRAW_RECT(wl_x,wl_y,wl_w,wl_h,255,255,255,255)
                RESET_SCRIPT_GFX_ALIGN()
            endif
        endif
        
        PROCESS_BELOW_MENU_HELP()    
        HIDE_HELP_TEXT_THIS_FRAME()         
        HANDLE_CURSOR_HIGHLIGHTED_MENU_ITEMS()
        
        DRAW_MENU(DEFAULT, DEFAULT, DEFAULT, DEFAULT, FALSE,fMenuExtension, DEFAULT, TRUE)
    endif   
ENDPROC
PROC Maintain_Menu_Controls()
    
    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
        
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_ACCEPT)
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_CANCEL)
            
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_UP)
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_SCROLL_DOWN)
        
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_X)
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CURSOR_Y)
        
        ENABLE_CONTROL_ACTION( FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
                
    ELSE
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_SELECT)
        SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_CANCEL)
    ENDIF
    
    SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_UP)
    SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_DOWN)
    SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_LEFT)
    SET_INPUT_EXCLUSIVE(FRONTEND_CONTROL, INPUT_CELLPHONE_RIGHT)    

    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_UNARMED)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_MELEE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HANDGUN)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SHOTGUN)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SMG)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_AUTO_RIFLE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SNIPER)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_HEAVY)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_WEAPON_SPECIAL) 
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_NEXT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_WHEEL_PREV)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_WEAPON_SPECIAL_TWO)    
    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_LIGHT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_ATTACK_HEAVY)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_MELEE_BLOCK)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DETONATE)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)   
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_RELOAD)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_DIVE)  
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_HEADLIGHT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_RADIO_WHEEL)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PUSHBIKE_SPRINT)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_PUSHBIKE_PEDAL)
    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_FRANKLIN)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_MICHAEL)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_TREVOR)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SELECT_CHARACTER_MULTIPLAYER)
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CHARACTER_WHEEL)

    CONTROL_PI_MENU_PC_MOUSE_INPUT()
    CONTROL_PI_MENU_MOVEMENT()
    CONTROL_PI_MENU_INPUT()
    
ENDPROC
//PURPOSE: Return TRUE if the currently highlighted option can trigger the FaceCam
FUNC BOOL DOES_CURRENT_SELECTION_USE_FACECAM()
    IF iSubMenu = ciPI_SUB_MENU_INVENTORY
    and SAFE_TO_USE_INVENTORY()
        RETURN TRUE
    ENDIF
    
    RETURN FALSE
ENDFUNC
PROC PROCESS_FACE_CAM_SHAPETEST()

    // 2nd frame of being ready, we need to restart the test
    IF eFaceCamShapeTestStage = FCST_RESULT_READY
        eFaceCamShapeTestStage = FCST_INIT
    ENDIF

    SWITCH eFaceCamShapeTestStage
        CASE FCST_INIT
            IF DOES_CURRENT_SELECTION_USE_FACECAM()
                IF not IS_PED_INJURED(PLAYER_PED_ID())
                    VECTOR vHeadPos
                    vHeadPos = GET_WORLD_POSITION_OF_ENTITY_BONE(PLAYER_PED_ID(), GET_PED_BONE_INDEX(PLAYER_PED_ID(), BONETAG_HEAD))
                
                    sti_FaceCam = START_SHAPE_TEST_SWEPT_SPHERE(
                        GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vHeadPos, GET_ENTITY_HEADING(PLAYER_PED_ID()), vFCPointOffset),
                        GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vHeadPos, GET_ENTITY_HEADING(PLAYER_PED_ID()), vFCAttachOffset),
                        0.5, SCRIPT_INCLUDE_ALL, PLAYER_PED_ID(), SCRIPT_SHAPETEST_OPTION_IGNORE_NO_COLLISION)
                    
                    b_FaceCamShapeTestHitSomething  = FALSE
                    eFaceCamShapeTestStage          = FCST_WAITING_RESULT
                ENDIF
            ENDIF
        BREAK
        CASE FCST_WAITING_RESULT
        
            INT iHitSomething
            VECTOR vPosHit, vNormalAtPosHit
            ENTITY_INDEX entityHit
        
            SHAPETEST_STATUS sResult
            sResult = GET_SHAPE_TEST_RESULT(sti_FaceCam, iHitSomething, vPosHit, vNormalAtPosHit, entityHit)
            // result is ready
            IF sResult = SHAPETEST_STATUS_RESULTS_READY
                IF iHitSomething = 0
                    b_FaceCamShapeTestHitSomething = FALSE
                ELSE
                    b_FaceCamShapeTestHitSomething = TRUE
                ENDIF
                eFaceCamShapeTestStage = FCST_RESULT_READY
            
            // test not running, jump back to init stage
            ELIF sResult = SHAPETEST_STATUS_NONEXISTENT
                eFaceCamShapeTestStage = FCST_INIT              
            ENDIF
            
        BREAK
    ENDSWITCH

ENDPROC
FUNC BOOL HAS_FACE_CAM_PASSED_SHAPE_TEST()

    IF eFaceCamShapeTestStage = FCST_RESULT_READY
    AND NOT b_FaceCamShapeTestHitSomething
        RETURN TRUE
    ENDIF

    RETURN FALSE
ENDFUNC
//PURPOSE: Returns TRUE if its okay to use the Face Cam
FUNC BOOL SAFE_TO_USE_FACE_CAM()
    IF not IS_PED_INJURED(PLAYER_PED_ID())
        
        IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
            RETURN FALSE            
        ELSE
            IF NOT IS_PLAYER_FREE_FOR_AMBIENT_TASK(PLAYER_ID())
                RETURN FALSE
            ENDIF
            
            IF GET_ENTITY_SPEED(PLAYER_PED_ID()) > 3
                RETURN FALSE
            ENDIF
            
            IF NOT HAS_FACE_CAM_PASSED_SHAPE_TEST()
                RETURN FALSE
            ENDIF
            
        ENDIF
        
        if GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("chop")) > 0
            RETURN FALSE
        endif
        
        IF IS_ENTITY_IN_AIR(PLAYER_PED_ID())
            RETURN FALSE
        ENDIF
        
        IF IS_ENTITY_IN_WATER(PLAYER_PED_ID())
            RETURN FALSE
        ENDIF
        
        IF IS_PED_RAGDOLL(PLAYER_PED_ID())
            RETURN FALSE
        ENDIF
        
        if IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
            return false
        endif
        
        IF IS_PED_IN_MELEE_COMBAT(PLAYER_PED_ID())
            RETURN FALSE
        ENDIF
        
        IF IS_ANY_INTERACTION_ANIM_PLAYING()
            RETURN FALSE
        ENDIF
        
        IF IS_STRIPCLUB_USING_BAR()
            RETURN FALSE
        ENDIF
        
        IF GET_IS_TASK_ACTIVE(PLAYER_PED_ID(), CODE_TASK_CLIMB_LADDER)
            RETURN FALSE
        ENDIF 
        
        if GET_PLAYER_WANTED_LEVEL(player_ID()) > 0
            RETURN FALSE
        endif
        
    ELSE
        RETURN FALSE
    ENDIF
    
    RETURN TRUE
ENDFUNC

//PURPOSE: Listen for user control of camera
PROC MAINTAIN_FACE_CAM_CONTROL()
    
    // Allow player to move the camera
    INT iLeftX, iLeftY, iRightX, iRightY
    
    BOOL bUpdateCamera
    INT iLimit = 8
    
    GET_CONTROL_VALUE_OF_ANALOGUE_STICKS_UNBOUND( iLeftX, iLeftY, iRightX, iRightY, bCursorCamMove)
    //iRightX *= -1
    IF IS_LOOK_INVERTED()
        iRightY *= -1
    ENDIF
    
    
    // We allow them full cycle of the vehicle
    IF ((iRightX < -iLimit) OR (iRightX > iLimit))
        bUpdateCamera = TRUE
        fXpos += (TO_FLOAT(iRightX)*fCamMoveSpeed)//0.0006) //0.025)
    ENDIF
    
    IF ((iRightY < -iLimit) OR (iRightY > iLimit))
        bUpdateCamera = TRUE
        fYpos += (TO_FLOAT(iRightY)*fCamMoveSpeed)//0.0006) //0.003)
    ENDIF
    
    FLOAT fXmin = -0.4
    FLOAT fXmax = 0.4
    FLOAT fYmin = -0.2
    FLOAT fYmax = 0.3
    
    IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID(), TRUE)
        fXmin = -0.1
        fXmax = 0.1
        fYmin = -0.05
        fYmax = 0.075
    ENDIF
        
    //Cap X
    IF fXpos < fXmin
        fXpos = fXmin
    ELIF fXpos > fXmax
        fXpos = fXmax
    ENDIF
    //Cap Y
    IF fYpos < fYmin
        fYpos = fYmin
    ELIF fYpos > fYmax
        fYpos = fYmax
    ENDIF
        
    IF bUpdateCamera    
        ATTACH_CAM_TO_PED_BONE(FaceCam, PLAYER_PED_ID(), BONETAG_HEAD, <<fXpos, 0.7, fYpos>>)
        POINT_CAM_AT_PED_BONE(FaceCam, PLAYER_PED_ID(), BONETAG_HEAD, <<0, 0, 0>>)//vFCPointOffset)
    ENDIF
    
    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_JUMP)
    
ENDPROC

//PURPOSE: Returns TRUE if the Face Cam should be cleaned up.
FUNC BOOL SHOULD_CLEANUP_FACE_CAM()

    IF NOT DOES_CURRENT_SELECTION_USE_FACECAM()
        RETURN TRUE
    ENDIF
    
    IF NOT SAFE_TO_USE_FACE_CAM()
        RETURN TRUE
    ENDIF
    
    RETURN FALSE
ENDFUNC

PROC CLEANUP_PI_MENU()
    //Clean up 
    bUseSelectArrows    = FALSE
    bSelectAvailable    = FALSE
    #IF FEATURE_SP_DLC_DIRECTOR_MODE
		bDirectorWarningScreen  = false
	#endif
	
	
    iSubMenu            = ciPI_SUB_MENU_NONE
    iStoredSelection    = 0 
    
    //Items
    iMaskTotalCount     = 0
    iGlassesTotalCount  = 0
    iHatTotalCount      = 0
//  iHelmetTotalCount   = 0

    bCursorMenuFirstTime = FALSE
    
    INT k
    INT i
    REPEAT MAX_MENUS i
        REPEAT MAX_MENU_SELECTIONS k
            if  i = ciPI_SUB_MENU_NONE 
                //...dont refresh selfie
            ELSE
                sSelection.iSelection[i][k] = 0
            ENDIF
        ENDREPEAT
    ENDREPEAT
    
    SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(sfMenu) 
    SET_USER_RADIO_CONTROL_ENABLED(TRUE)
    CLEANUP_FACE_CAM()
    CLEANUP_HEADER_GRAPHIC_FOR_PI()
    CLEANUP_MENU_ASSETS()
    BLOCK_MISSION_TITLE(false)
    
ENDPROC
//PURPOSE: Controls the Face Cam. Enabled when player swaps face props like glasses.
PROC Maintain_Face_Cam()
    
    IF eFaceCamStage = FCS_WAIT
        PROCESS_FACE_CAM_SHAPETEST()
    ENDIF

    SWITCH eFaceCamStage
        CASE FCS_WAIT
            IF IS_BIT_SET(iBoolsBitSet, biEnableFaceCam)
                IF SAFE_TO_USE_FACE_CAM()
                    eFaceCamStage = FCS_INIT
                    NET_PRINT_TIME() NET_PRINT("     ---------->     PIM - PROCESS_FACE_CAM - iFaceCamStage = FCS_INIT") NET_NL() 
                ELSE
                    IF eFaceCamShapeTestStage = FCST_RESULT_READY
                        CLEAR_BIT(iBoolsBitSet, biEnableFaceCam)
                        NET_PRINT_TIME() NET_PRINT("     ---------->     PIM - PROCESS_FACE_CAM - Not safe to start cam") NET_NL() 
                    ENDIF
                ENDIF
            ENDIF
        BREAK
        
        CASE FCS_INIT
            IF NOT DOES_CAM_EXIST(FaceCam)
                if not IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
                    CLEAR_PED_TASKS(PLAYER_PED_ID())
                endif               
                SET_PED_STEALTH_MOVEMENT(PLAYER_PED_ID(), FALSE)
                SET_PLAYER_CONTROL(PLAYER_ID(), FALSE, SPC_ALLOW_PLAYER_DAMAGE|SPC_PREVENT_EVERYBODY_BACKOFF)
                FORCE_PED_MOTION_STATE(PLAYER_PED_ID(), MS_ON_FOOT_IDLE)
                FORCE_PED_AI_AND_ANIMATION_UPDATE(PLAYER_PED_ID())
                FaceCam = CREATE_CAM_WITH_PARAMS("DEFAULT_SCRIPTED_CAMERA", GET_ENTITY_COORDS(PLAYER_PED_ID()), <<0, 0, 0>>, FC_FOV, FALSE)
                //SET_CAM_COORD(FaceCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), vFCOffset))
                ATTACH_CAM_TO_PED_BONE(FaceCam, PLAYER_PED_ID(), BONETAG_HEAD, vFCAttachOffset) //vFCAttachOffsetPre)   //
                POINT_CAM_AT_PED_BONE(FaceCam, PLAYER_PED_ID(), BONETAG_HEAD, vFCPointOffset)
                SET_CAM_FOV(FaceCam, FC_FOV)
                SET_CAM_ACTIVE(FaceCam, TRUE)
                RENDER_SCRIPT_CAMS(TRUE, FALSE) //TRUE)
            ENDIF
            eFaceCamStage = FCS_RUNNING
            NET_PRINT_TIME() NET_PRINT("     ---------->     PIM - PROCESS_FACE_CAM - iFaceCamStage = FCS_RUNNING") NET_NL() 
        BREAK
        
        CASE FCS_RUNNING

            MAINTAIN_FACE_CAM_CONTROL()
            
            //Cleanup Checks
            IF SHOULD_CLEANUP_FACE_CAM()
                eFaceCamStage = FCS_CLEANUP
                NET_PRINT_TIME() NET_PRINT("     ---------->     PIM - PROCESS_FACE_CAM - iFaceCamStage = FCS_CLEANUP - A") NET_NL() 
            ENDIF
        BREAK
        
        CASE FCS_CLEANUP
            CLEANUP_FACE_CAM()
        BREAK
    ENDSWITCH
ENDPROC











FUNC STRING GET_SELFIE_DICT_NAME()
    if Get_Player_Director_Mode_Compatible() = CHAR_MICHAEL
        return "cellphone@self@michael@"
    elif Get_Player_Director_Mode_Compatible() = CHAR_FRANKLIN
        return "cellphone@self@franklin@"
    elif Get_Player_Director_Mode_Compatible() = CHAR_TREVOR
        return "cellphone@self@trevor@"
    endif
return ""   
ENDFUNC
FUNC STRING GET_SELFIE_ANIM_NAME()
    if Get_Player_Director_Mode_Compatible() = CHAR_MICHAEL 
        switch INT_TO_ENUM(ENUM_SELFIE,iCurrentSelfie)
            case eself_finger   
                return "FINGER_POINT"               
            break               
//          case eself_Chin
//              return "RUN_CHIN"
//          break
            case eself_neck
                return "STRETCH_NECK"
            break
        endswitch
        
    elif Get_Player_Director_Mode_Compatible() = CHAR_FRANKLIN
        switch INT_TO_ENUM(ENUM_SELFIE,iCurrentSelfie)
            case eself_bump 
                return "CHEST_BUMP"             
            break               
//          case eself_peace
//              return "PEACE"
//          break
            case eself_WestCoast
                return "WEST_COAST"
            break
        endswitch
        
    elif Get_Player_Director_Mode_Compatible() = CHAR_TREVOR
        switch INT_TO_ENUM(ENUM_SELFIE,iCurrentSelfie)
//          case eself_AgroFinger   
//              return "AGGRESSIVE_FINGER"              
//          break               
            case eself_ProudFinger
                return "PROUD_FINGER"
            break
            case eself_SlitThroat
                return "THROAT_SLIT"
            break
        endswitch
    endif
return ""   
ENDFUNC
FUNC STRING GET_SELFIE_DISPLAY_NAME()
    if Get_Player_Director_Mode_Compatible() = CHAR_MICHAEL 
        switch INT_TO_ENUM(ENUM_SELFIE,iCurrentSelfie)
            case eself_finger   
                return "SELF_M1"                
            break               
//          case eself_Chin
//              return "SELF_M2"
//          break
            case eself_neck
                return "SELF_M3"
            break
        endswitch
        
    elif Get_Player_Director_Mode_Compatible() = CHAR_FRANKLIN
        switch INT_TO_ENUM(ENUM_SELFIE,iCurrentSelfie)
            case eself_bump 
                return "SELF_F1"                
            break               
//          case eself_peace
//              return "SELF_F2"
//          break
            case eself_WestCoast
                return "SELF_F3"
            break
        endswitch
        
    elif Get_Player_Director_Mode_Compatible() = CHAR_TREVOR
        switch INT_TO_ENUM(ENUM_SELFIE,iCurrentSelfie)
//          case eself_AgroFinger   
//              return "SELF_T1"                
//          break               
            case eself_ProudFinger
                return "SELF_T2"
            break
            case eself_SlitThroat
                return "SELF_T3"
            break
        endswitch
    endif
return ""
ENDFUNC
PROC manage_selfie_anims()  

    // *********************************************************************************************
    //              SELECT FILTER
    // *********************************************************************************************
    string  animName
    string  dictName        =   GET_SELFIE_DICT_NAME()          
    STRING  sFilter         =   "IgnoreMoverBlend_filter"
    
    IF IS_CELLPHONE_CAMERA_IN_SELFIE_MODE()     
    AND (IS_SNAPMATIC_NOT_IN_VIEWFINDER_MODE() = FALSE) //Inverted by Steve T.
        
        if IS_CONTROL_PRESSED(PLAYER_CONTROL,INPUT_CELLPHONE_LEFT)
            bdoAnim = TRUE
        ELSE
            bdoAnim = false
        ENDIF
        
        bool bCharSwapped = false
        eSelfiecharCurr = GET_CURRENT_PLAYER_PED_ENUM()
        if eSelfiecharCurr != eSelfiecharPrev
            eSelfiecharPrev = eSelfiecharCurr
            bCharSwapped = true
        endif
        
        if (IS_CONTROL_JUST_PRESSED(PLAYER_CONTROL,INPUT_CELLPHONE_RIGHT)
        and get_Game_timer() - iPI_MoveTimer > iPI_MOVE_DELAY 
        and not IS_BIT_SET(BitSet_CellphoneTU,g_BSTU_APPCAMERA_NEEDS_TO_UPDATE_ACTION))
        OR  bCharSwapped// update the anim if the character has switched
            
            iPI_MoveTimer = get_Game_timer()    
            CPRINTLN(DEBUG_MISSION, "---CV manage_selfie_anims: iFastSelectSelfie: ",iFastSelectSelfie," MAX: ",GET_SELECTION_MAX_PI(ciPI_TYPE_SELFIE)) 
            iFastSelectSelfie++
            IF iFastSelectSelfie > GET_SELECTION_MAX_PI(ciPI_TYPE_SELFIE)
                iFastSelectSelfie = 0
            ENDIF  
            g_txtSP_CurrentSelfie_anim = GET_SELFIE_DISPLAY_NAME()
            bChangeAnim = TRUE
            SET_BIT (BitSet_CellphoneTU, g_BSTU_APPCAMERA_NEEDS_TO_UPDATE_ACTION)
            CPRINTLN(DEBUG_MISSION, "---CV manage_selfie_anims: next selfie: ",iFastSelectSelfie," name: ",g_txtSP_CurrentSelfie_anim)              
        ENDIF
        
    ELSE
        bdoAnim = FALSE
    ENDIF
        
        switch iSelfieStage             
            CASE 0
                IF IS_CELLPHONE_CAMERA_IN_SELFIE_MODE()         
                AND (IS_SNAPMATIC_NOT_IN_VIEWFINDER_MODE() = FALSE) //Inverted by Steve T
                    if bChangeAnim      
                        iCurrentSelfie = iFastSelectSelfie
                        g_txtSP_CurrentSelfie_anim = GET_SELFIE_DISPLAY_NAME()                        
                        CPRINTLN(DEBUG_MISSION, "---CV manage_selfie_anims: iCurrentSelfie = iFastSelectSelfie: ",iFastSelectSelfie)        
                        bChangeAnim = FALSE
                    ELSE        
                        IF not IS_STRING_NULL_OR_EMPTY(dictName)
                            REQUEST_ANIM_DICT(dictName)
                        ENDIF
                        if bdoAnim      
                        and not IS_STRING_NULL_OR_EMPTY(dictName)
                             IF HAS_ANIM_DICT_LOADED(dictName)                      
                                animName = GET_SELFIE_ANIM_NAME()   
                                if not IS_PLAYING_PHONE_GESTURE_ANIM(PLAYER_PED_ID())   
                                and not IS_STRING_NULL_OR_EMPTY(dictName)
                                and not IS_STRING_NULL_OR_EMPTY(animName)
                                    CPRINTLN(DEBUG_MISSION, "---CV manage_selfie_anims: iCurrentSelfie name: ",animName," Global label: ",g_txtSP_CurrentSelfie_anim)                                
                                    //anim trigger
                                    TASK_PLAY_PHONE_GESTURE_ANIMATION(PLAYER_PED_ID(),dictName ,animName, sFilter,SLOW_BLEND_DURATION,SLOW_BLEND_DURATION)                               
                                    iSelfieStage =  1  
                                ENDIF
                            ENDIF
                        ENDIF
                    ENDIF
                ENDIF
            BREAK            
            CASE 1
                IF IS_CELLPHONE_CAMERA_IN_SELFIE_MODE()         
                AND (IS_SNAPMATIC_NOT_IN_VIEWFINDER_MODE() = FALSE) //Inverted by Steve T
                    if not IS_PLAYING_PHONE_GESTURE_ANIM(PLAYER_PED_ID()) 
                        iSelfieStage = 0      
                    ENDIF
                else
                    if IS_PLAYING_PHONE_GESTURE_ANIM(PLAYER_PED_ID()) 
                        TASK_STOP_PHONE_GESTURE_ANIMATION(PLAYER_PED_ID(),INSTANT_BLEND_OUT)
                    endif
                    iSelfieStage = 0
                endif
            BREAK
        ENDSWITCH   
    
    
ENDPROC


FUNC BOOL CHECK_ASPECT_RATIO_FOR_FRAME()

	FLOAT fCurrAspectRatio = GET_ASPECT_RATIO( FALSE )
	IF bFirstTimeHit
		fPreviousAspectRatio = fCurrAspectRatio
		bFirstTimeHit = FALSE
		RETURN FALSE
	ELSE
		IF fPreviousAspectRatio != fCurrAspectRatio
			fPreviousAspectRatio = fCurrAspectRatio
			PRINTLN("[PI_MENU] - PI_menu.sc - CHECK_ASPECT_RATIO_FOR_FRAME returning TRUE")
			RETURN TRUE
		ELSE
			fPreviousAspectRatio = fCurrAspectRatio
			RETURN FALSE
		ENDIF	
	ENDIF

ENDFUNC


//////////////////////////////////////////////////////////////////////////////
//                                  Main Loop                               //
//////////////////////////////////////////////////////////////////////////////

SCRIPT  
    PRINTLN("...Starting PI_MENU.sc")
    
    IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_DIRECTOR)
        CLEANUP_PI_MENU()
        ePI_MENU_STATE = PI_MENU_INACTIVE
        TERMINATE_THIS_THREAD()
    ENDIF   
    
#if IS_DEBUG_BUILD  
    wGroup = START_WIDGET_GROUP("PI MENU") 
        ADD_WIDGET_INT_READ_ONLY("g_iCurrentlyDisplayingContextINDEX", g_iCurrentlyDisplayingContextINDEX)
        ADD_WIDGET_FLOAT_SLIDER("PosX", sf_X, -1.0, 1.0, 0.001)
        ADD_WIDGET_FLOAT_SLIDER("PosY", sf_y, -1.0, 1.0, 0.001)
        ADD_WIDGET_FLOAT_SLIDER("Width",sf_w, -10.0, 10.0, 0.001)
        ADD_WIDGET_FLOAT_SLIDER("Height",sf_h, -10.0, 10.0, 0.001)
		ADD_WIDGET_FLOAT_SLIDER("PosYAdj", sf_y_adj, 0.0, 100.0, 0.0001)
        ADD_WIDGET_FLOAT_SLIDER("WLPosX", wl_x, -1.0, 1.0, 0.001)
        ADD_WIDGET_FLOAT_SLIDER("WLPosY", wl_y, -1.0, 1.0, 0.001)
        ADD_WIDGET_FLOAT_SLIDER("WLWidth",wl_w, -10.0, 10.0, 0.001)
        ADD_WIDGET_FLOAT_SLIDER("WLHeight",wl_h, -10.0, 10.0, 0.001)
        ADD_WIDGET_FLOAT_SLIDER("TitleX", txt_X, -1.0, 1.0, 0.001)
        ADD_WIDGET_FLOAT_SLIDER("TitleY", txt_y, -1.0, 1.0, 0.001)
        ADD_WIDGET_INT_READ_ONLY("Menu stage",iwidgetMenuState)
        START_WIDGET_GROUP("face cam")
            ADD_WIDGET_FLOAT_SLIDER("fCamMoveSpeed",fCamMoveSpeed,0.000000001,1,0.000000001)
        STOP_WIDGET_GROUP()
    STOP_WIDGET_GROUP()
    SET_LOCATES_HEADER_WIDGET_GROUP(wGroup)
#endif

//set default selfie

    iCurrentSelfie              = 0 
    iSelfieStage                = 0
    eSelfiecharPrev             = GET_CURRENT_PLAYER_PED_ENUM()
    g_txtSP_CurrentSelfie_anim  = GET_SELFIE_DISPLAY_NAME()
        
    // Main loop
    WHILE TRUE
    	
		IF CHECK_ASPECT_RATIO_FOR_FRAME()
			PRINTLN("[PI_MENU] - PI_menu.sc - Setting ePI_MENU_STATE to PI_MENU_CLEANUP as aspect ratio changed whilst menu was up")
			ePI_MENU_STATE = PI_MENU_CLEANUP
		ENDIF
		
        // Disable Pause menu so it doesn't trigger if Esc is pressed when you quit.
        IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
            IF ePI_MENU_STATE != PI_MENU_INACTIVE
                IF NOT IS_PAUSE_MENU_ACTIVE()
                    DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
                ENDIF
            ENDIF
        ENDIF
    
        IF IS_PED_INJURED(player_ped_id())
            IF IS_BIT_SET(iBoolsBitSet, biSwitchButtonCheck)
                IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_INTERACTION_MENU)
                    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
                ELSE    
                    CPRINTLN(DEBUG_MISSION, "---CV biSwitchButtonCheck: PI_MENU_CLEANUP CLEAR")
                    CLEAR_BIT(iBoolsBitSet, biSwitchButtonCheck)
                ENDIF
            endif
            
            ijustClosedTimer = get_game_timer()
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
            DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
            
            CLEANUP_PI_MENU()
            ePI_MENU_STATE = PI_MENU_INACTIVE
        ELSE
        
            manage_selfie_anims()
        endif
        
        // switch menu state ready requested to switch
        if SWITCH_MENU_STATE()  
            bSwitchMenuState = true
        endif   
        
        SWITCH ePI_MENU_STATE
            
            CASE PI_MENU_INACTIVE // check if menu is to be turned on
                #IF FEATURE_SP_DLC_DIRECTOR_MODE
                IF NOT g_bLaunchDirectorMode
				AND NOT IS_DIRECTOR_MODE_RUNNING()
                    g_bDirectorPIMenuUp = FALSE
                ENDIF
                #ENDIF
                
                if bSwitchMenuState             
                    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)// just to stop conflicting menus                                      
                    iPi_submenu_timer = get_Game_timer()
                    bCursorMenuFirstTime = TRUE
                    
                    sGraphicHeader = GET_HEADER_GRAPHIC_FOR_PI()
                    REQUEST_HEADER_GRAPHIC_FOR_PI() 
                    if REQUEST_HEADER_GRAPHIC_FOR_PI()
                        PRE_SETUP_PI_MENU()   
                        bSwitchMenuState = false
                        CLEAR_HELP()    
                        SET_CURSOR_POSITION_FOR_MENU()
                        ePI_MENU_STATE = PI_MENU_ACTIVE                     
                    endif
                endif
                if GET_GAME_TIMER() - ijustClosedTimer < 500
                    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
                    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
                    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_CONTEXT)
                    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
                    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
                    DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
                    
                    // Disable Pause menu so it doesn't trigger if Esc is pressed when you quit.
                    IF IS_USING_KEYBOARD_AND_MOUSE(FRONTEND_CONTROL)
                        IF NOT IS_PAUSE_MENU_ACTIVE()
                            DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_FRONTEND_PAUSE_ALTERNATE)
                        ENDIF
                    ENDIF
                    
                    iPi_menu_timer = get_Game_timer()
                endif           
            BREAK
            
            CASE PI_MENU_ACTIVE
                #IF FEATURE_SP_DLC_DIRECTOR_MODE
                g_bDirectorPIMenuUp = TRUE  //Mark as active
                #ENDIF

                Maintain_Active_PI_Menu()   //  draw
                Maintain_Menu_Controls()    //  Controls
                Maintain_Face_Cam()         //  Cam
                PAUSE_FLOW_HELP_QUEUE(TRUE) 
                
                if bSwitchMenuState      // Check if menu is to be turned off            
                or CONTROL_MENU_CLOSE()             
                    PLAY_SOUND_FRONTEND(-1, "CANCEL", "HUD_FRONTEND_DEFAULT_SOUNDSET",FALSE)  
                    bSwitchMenuState = false
                    ePI_MENU_STATE = PI_MENU_CLEANUP
                endif
                
                //stop interferance with jet weapons
                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)        
                HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_AREA_NAME)
                HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_VEHICLE_NAME)
                HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_DISTRICT_NAME)
                HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_STREET_NAME)
                BLOCK_MISSION_TITLE()
                g_sMenuData.bKeepPhoneForNextDrawMenuCall = TRUE //allow phone to draw and stop menu 
            BREAK
            
            CASE PI_MENU_CLEANUP
                IF IS_BIT_SET(iBoolsBitSet, biSwitchButtonCheck)
                    IF IS_DISABLED_CONTROL_PRESSED(PLAYER_CONTROL, INPUT_INTERACTION_MENU)
                        DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
                    ELSE    
                        CPRINTLN(DEBUG_MISSION, "---CV biSwitchButtonCheck: PI_MENU_CLEANUP CLEAR")
                        CLEAR_BIT(iBoolsBitSet, biSwitchButtonCheck)
                    ENDIF
                endif
                
                ijustClosedTimer = get_game_timer()
                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_FLY_ATTACK)
                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_NEXT_CAMERA)
                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_SPRINT)
                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_CIN_CAM)
                DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_VEH_DUCK)
                PAUSE_FLOW_HELP_QUEUE(false)    
                CLEANUP_PI_MENU()
				bFirstTimeHit = TRUE
                ePI_MENU_STATE = PI_MENU_INACTIVE
            BREAK
        ENDSWITCH
        
        WAIT(0) 
    ENDWHILE
    
ENDSCRIPT
