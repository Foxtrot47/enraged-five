
USING "commands_script.sch"
USING "commands_graphics.sch"
USING "mp_icons.sch"


// MUST BE KEPT IN SYNC FROM CODE!
ENUM eOPERATION_TYPES
	// Fire-and-die types
	// These types should have NO WAITS; just instantly fall to the end of the block and die.
	
						//	Args filled out:		MenuScreenId	PreviousId	UniqueIdentifier
	kFill = 0, 			// Fill up the entire menu		-				-			-
	kLayoutChange = 1,	// Layout was changed			X				X			X
	kTriggerEvent = 2,	// Button was pressed			X				-			X
	
	// Continual update type
	// Just run the script normally, full control
	// including WAIT(0) and stuff
	kUpdate = 3			//								-				-			-
ENDENUM

// MUST BE KEPT IN SYNC FROM CODE!
STRUCT PAUSE_MENU_LAUNCH_DATA
	eOPERATION_TYPES	operation
	INT					MenuScreenId
	INT					PreviousId
	INT					UniqueIdentifier
ENDSTRUCT

PROC PM_SET_DATA_SLOT(INT iColumn, INT iMenuIndex, INT iMenuId, INT iUniqueId, BOOL bActive, STRING pszLabel )
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("SET_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuIndex) // imenuindex
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iMenuId) // menu id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iUniqueId) // unique id
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // menu type
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // initial index
		IF bActive
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(1) // active or not
		ELSE
			SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(0) // active or not
		ENDIF
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_STRING(pszLabel) // text label
		
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC

PROC PM_DISPLAY_DATA_SLOT(INT iColumn)
	IF BEGIN_SCALEFORM_MOVIE_METHOD_ON_FRONTEND("DISPLAY_DATA_SLOT")
		SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT(iColumn) // depth
		END_SCALEFORM_MOVIE_METHOD()
	ENDIF
ENDPROC


CONST_INT MIDDLE_COLUMN 1
CONST_INT MY_MENU HASH("PM_REPLAY")
CONST_INT SUBMENU HASH("PM_REPLAY_MISSIONS")
CONST_INT SILLY_OFFSET 1000


SCRIPT( PAUSE_MENU_LAUNCH_DATA args ) 

	PRINTLN("SCRIPT LAUNCHED WITH OP: ", args.operation, ", Menu: ", args.MenuScreenId, ", Prev:", args.PreviousId, ", Unique: ", args.UniqueIdentifier)
	PRINTLN("MY MENU HASH: ", 	MY_MENU)
	PRINTLN("MY SUBMENU HASH: ", SUBMENU)
	
	SWITCH args.operation
	
		CASE kUpdate
			WHILE TRUE
				PRINTLN("Just Chillin' in Update()")
				// perform any/all querying stuff here.
				// not shown because I don't like it as much as the below stuff
				WAIT(5000)
			ENDWHILE
		BREAK
		
		/// ----------------------------------------------------------------
		
		CASE kTriggerEvent
			PRINTLN("Event Triggered for ", args.MenuScreenId, " and ", args.UniqueIdentifier)
			IF args.MenuScreenId = SUBMENU
				PRINTLN("Launching Mission: ", args.UniqueIdentifier)
			ENDIF
		BREAK
		
		CASE kFill
		CASE kLayoutChange
			IF args.MenuScreenId = MY_MENU
				PRINTLN("Acknowledged layout Changed!")
				PM_SET_DATA_SLOT(MIDDLE_COLUMN,0,SUBMENU+SILLY_OFFSET,0,TRUE, "PM_PANE_AUD")
				PM_SET_DATA_SLOT(MIDDLE_COLUMN,1,SUBMENU+SILLY_OFFSET,1,FALSE, "PM_PANE_DIS")
				PM_DISPLAY_DATA_SLOT(MIDDLE_COLUMN)
			ENDIF
			
			IF args.MenuScreenId = SUBMENU
				// FILL OUT CRAP IN NEXT COLUMN (once it exists)
			ENDIF
		BREAK
	ENDSWITCH
	
	
	


	TERMINATE_THIS_THREAD()

ENDSCRIPT

