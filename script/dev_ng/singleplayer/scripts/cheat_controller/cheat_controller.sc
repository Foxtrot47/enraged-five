//╒═════════════════════════════════════════════════════════════════════════════╕
//│				Author:  Stephen Robertson					Date: 12/01/13		│
//╞═════════════════════════════════════════════════════════════════════════════╡
//│																				│
//│					  			Cheat Controller								│
//│																				│
//│		DESCRIPTION: A basic controller script used to activate and disable 	│
//│		cheats.			                                                        │
//│		                  														│
//╘═════════════════════════════════════════════════════════════════════════════╛

// Includes
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_streaming.sch"
USING "context_control_public.sch"
USING "drunk_public.sch"
USING "cheat_controller_public.sch"
USING "replay_public.sch"
USING "website_public.sch"

/// PURPOSE: Status for cheat activation. Not all cheats will use all statuses.
///    
ENUM CHEAT_STATUS

	CHEAT_DISABLED = 0,
	CHEAT_INACTIVE,
	CHEAT_INITIALISE,
	CHEAT_INITIALSE_PT2,
	CHEAT_ACTIVATE,
	CHEAT_RUNNING,
	CHEAT_RUNNING_SLOWMO_LEVEL1,
	CHEAT_RUNNING_SLOWMO_LEVEL2,
	CHEAT_RUNNING_SLOWMO_LEVEL3,
	CHEAT_DEACTIVATE,
	CHEAT_CLEANUP
	
ENDENUM

/// PURPOSE: Overall cheat system status.
ENUM CHEAT_SYSTEM_STATUS

	CHEAT_SYSTEM_ACTIVE = 0,
	CHEAT_SYSTEM_PLAYER_DEAD,
	CHEAT_SYSTEM_CUTSCENE_ACTIVE

ENDENUM

ENUM CHEAT_WEATHER

	CHEAT_WEATHER_OFF,
	CHEAT_WEATHER_EXTRASUNNY,
	CHEAT_WEATHER_CLEAR,
	CHEAT_WEATHER_CLOUDS,
	CHEAT_WEATHER_SMOG,
	CHEAT_WEATHER_CLOUDY,
	CHEAT_WEATHER_OVERCAST,
	CHEAT_WEATHER_RAIN,
	CHEAT_WEATHER_THUNDER, 
	CHEAT_WEATHER_CLEARING,
	CHEAT_WEATHER_NEUTRAL,
	CHEAT_WEATHER_SNOW

ENDENUM


CHEAT_STATUS eCheatStatusSpawnVehicle				= CHEAT_INACTIVE

// Vehicle cheat status

// General cheats status
CHEAT_STATUS eCheatStatusSuperJump					= CHEAT_INACTIVE
CHEAT_STATUS eCheatStatusSlideyCars					= CHEAT_INACTIVE
CHEAT_STATUS eCheatStatusFastRun					= CHEAT_INACTIVE
CHEAT_STATUS eCheatStatusFastSwim					= CHEAT_INACTIVE
CHEAT_STATUS eCheatStatusWeapons					= CHEAT_INACTIVE
CHEAT_STATUS eCheatStatusAdvanceWeather				= CHEAT_INACTIVE
CHEAT_STATUS eCheatStatusHealthArmor				= CHEAT_INACTIVE
CHEAT_STATUS eCheatStatusSpecialAbilityRecharge		= CHEAT_INACTIVE
CHEAT_STATUS eCheatStatusWantedLevelUp				= CHEAT_INACTIVE
CHEAT_STATUS eCheatStatusWantedLevelDown			= CHEAT_INACTIVE
CHEAT_STATUS eCheatStatusGiveParachute				= CHEAT_INACTIVE



// Off Mission Cheat status
CHEAT_STATUS	eCheatStatusBangBang				= CHEAT_INACTIVE
CHEAT_STATUS	eCheatStatusFlamingBullets			= CHEAT_INACTIVE
CHEAT_STATUS	eCheatStatusExplosiveMelee			= CHEAT_INACTIVE
CHEAT_STATUS	eCheatStatus0Gravity				= CHEAT_INACTIVE
CHEAT_STATUS	eCheatStatusInvincibility			= CHEAT_INACTIVE
CHEAT_STATUS	eCheatStatusSlowMo					= CHEAT_INACTIVE
CHEAT_STATUS	eCheatStatusSkyfall					= CHEAT_INACTIVE
CHEAT_STATUS	eCheatStatusDrunk					= CHEAT_INACTIVE
CHEAT_STATUS	eCheatStatusAimSlowMo				= CHEAT_INACTIVE


MODEL_NAMES		eCheatVehicleModel 					= BMX	// Default model.
VEHICLE_INDEX	vCheatVehicle
VEHICLE_INDEX	vCheatSlideyVehicle					= NULL
STRING			sCheatVehicleText
CHEAT_WEATHER	eCheatWeather						= CHEAT_WEATHER_OFF // Default weather cheat
INT				iSkyfallTimer
INT				iInvincibilityTimeLimit 			= 5 * 60 * 1000 // 5 mins
INT				iInvincibilityStartTime
INT				iInvincibilityTimer
INT				iSlowMoLevel						= 0
INT				iAimSlowMoLevel						= 0
FLOAT			fAimSlowMoValue						= 1.0
INT				iGravityLevel						= 0


CONST_INT		MAX_FEED_MESSAGE_IDS				10
CONST_INT		MAX_COLLISION_POS					9

INT				iFeedMessageIds[MAX_FEED_MESSAGE_IDS]
INT				iCurrentFeedMessage					= 0
BOOL			bFeedCleared 						= FALSE

BOOL			bIsPhoneCheat						= FALSE

INT				directorModeTimer					= 0

#IF IS_DEBUG_BUILD
BOOL			bWidgetsCreated						= FALSE
#ENDIF

/// PURPOSE:
///    Checks if player is on a mission
/// RETURNS:
///    TRUE if the player is on any of the restricted mission types
FUNC BOOL IS_ANY_MISSION_ACTIVE()
	
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE( MISSION_TYPE_STORY )
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE( MISSION_TYPE_RAMPAGE )
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE( MISSION_TYPE_RANDOM_CHAR )
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE( MISSION_TYPE_MINIGAME )
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE( MISSION_TYPE_MINIGAME_FRIENDS )
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE( MISSION_TYPE_STORY_FRIENDS )
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE( MISSION_TYPE_STORY_PREP )
	OR GET_MISSION_FLOW_FLAG_STATE(FLOWFLAG_HEIST_FINALE_PREPE_PLACING_CAR)
		RETURN TRUE
	ENDIF

	RETURN FALSE

ENDFUNC


/// PURPOSE:
///    Initialises the cheat feed message ID array
PROC INIT_CHEAT_FEED_MESSAGES()

	INT i = 0
	WHILE i < MAX_FEED_MESSAGE_IDS
		iFeedMessageIds[i] = -1	
		++ i
	ENDWHILE
	
	bFeedCleared = FALSE
	iCurrentFeedMessage = 0
	
ENDPROC


/// PURPOSE:
///    Stores feed message IDs so we can clear them if needed.
/// PARAMS:
///    id - The id of the feed message to be cleared.
PROC STORE_FEED_MESSAGE_ID( INT id )
	
	iFeedMessageIds[iCurrentFeedMessage] = id
	
	++ iCurrentFeedMessage
	
	IF iCurrentFeedMessage >= MAX_FEED_MESSAGE_IDS
		iCurrentFeedMessage = 0
	ENDIF

ENDPROC

/// PURPOSE:
///    Clears any cheat feed messages.
PROC CLEAR_FEED_MESSAGES()

	INT i = 0
	WHILE i < MAX_FEED_MESSAGE_IDS
		
		IF iFeedMessageIds[i]  != -1
			THEFEED_REMOVE_ITEM(iFeedMessageIds[i])
			iFeedMessageIds[i] = -1
		ENDIF
		
		++i
	
	ENDWHILE

ENDPROC


/// PURPOSE:
///    Increments the times cheated stat.
PROC INCREMENT_CHEAT_CODES_ACTIVATED_STAT()

	enumCharacterList ePed = GET_CURRENT_PLAYER_PED_ENUM()
	 
	SWITCH ePed
	
		CASE CHAR_MICHAEL
            QUICK_INCREMENT_INT_STAT(SP0_TIMES_CHEATED, 1)
			CPRINTLN(DEBUG_AMBIENT, "CHEAT CONTROLLER: INCREMENTED MICHAEL TIMES CHEATED STAT")
			BREAK
		CASE CHAR_FRANKLIN
			QUICK_INCREMENT_INT_STAT(SP1_TIMES_CHEATED, 1)
			CPRINTLN(DEBUG_AMBIENT, "CHEAT CONTROLLER: INCREMENTED FRANKLIN TIMES CHEATED STAT")
        	BREAK
        CASE CHAR_TREVOR
			QUICK_INCREMENT_INT_STAT(SP2_TIMES_CHEATED, 1)
			CPRINTLN(DEBUG_AMBIENT, "CHEAT CONTROLLER: INCREMENTED TREVOR TIMES CHEATED STAT")
        	BREAK
			
	ENDSWITCH

ENDPROC

FUNC BOOL IS_CHEAT_FROM_DIRECTOR_MODE(STRING sText)

	IF directorModeTimer != 0			
		IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR) AND IS_SCREEN_FADED_IN()
			IF ARE_STRINGS_EQUAL(sText,"CHEAT_SUPER_JUMP") AND IS_INTERIOR_SCENE()
				CPRINTLN(DEBUG_DIRECTOR, "CHEAT CONTROLLER: IS_CHEAT_FROM_DIRECTOR_MODE CHEAT_SUPER_JUMP RETURN FALSE")
				RETURN FALSE
			ENDIF
			IF ARE_STRINGS_EQUAL(sText,"CHEAT_GRAVITY_MOON") AND NOT IS_ENTITY_DEAD(PLAYER_PED_ID()) AND IS_ENTITY_IN_WATER(PLAYER_PED_ID())
				CPRINTLN(DEBUG_DIRECTOR, "CHEAT CONTROLLER: IS_CHEAT_FROM_DIRECTOR_MODE CHEAT_GRAVITY_MOON RETURN FALSE")
				RETURN FALSE
			ENDIF
		ENDIF
		
		CPRINTLN(DEBUG_DIRECTOR, "CHEAT CONTROLLER: IS_CHEAT_FROM_DIRECTOR_MODE RETURN TRUE")
		RETURN TRUE
	ENDIF
	
	CPRINTLN(DEBUG_DIRECTOR, "CHEAT CONTROLLER: IS_CHEAT_FROM_DIRECTOR_MODE RETURN FALSE")
	RETURN FALSE
ENDFUNC

/// PURPOSE:
///    Registers the specified cheat as being used this session
/// PARAMS:
///    eCheatEnum - 
PROC REGISTER_CHEAT_USED( CHEATS_BITSET_ENUM eCheatEnum )

	//In director mode, gameplay is very restricted.  
	//Any cheats activated while in this mode, we do not want affecting SP or MP.
	//Do not register that a cheat has been activate while in director mode.
	IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
	AND NOT IS_CHEAT_FROM_DIRECTOR_MODE("")

		// This gets set, but not reset, as it lets achievements know if a cheat has been activated this session
		SET_BIT( g_iBitsetCheatsUsedThisSession, ENUM_TO_INT(eCheatEnum) )
		
		// This is used by the stats guys to keep track of if any cheats have been used.
		g_bHasAnyCheatBeenUsed = TRUE 
		
		INCREMENT_CHEAT_CODES_ACTIVATED_STAT()
		
	ELSE	
		CPRINTLN(DEBUG_AMBIENT, "CHEAT CONTROLLER: DO NOT REGISTER CHEAT (DIRECTOR MODE ACTIVE), eCheatEnum = ",eCheatEnum)	
	ENDIF

ENDPROC

/// PURPOSE:
///    Registers if a specific cheat is active or inactive
/// PARAMS:
///    eCheatEnum - 
///    bActive - 
PROC REGISTER_CHEAT_ACTIVE( CHEATS_BITSET_ENUM eCheatEnum, BOOL bActive )

	IF bActive
		SET_BIT( g_iBitsetCheatsCurrentlyActive, ENUM_TO_INT(eCheatEnum) )		
		REGISTER_CHEAT_USED(eCheatEnum)
	ELSE
		CLEAR_BIT( g_iBitsetCheatsCurrentlyActive, ENUM_TO_INT(eCheatEnum) )
	ENDIF

ENDPROC

/// =================================================================
///    NOTIFICATIONS
/// =================================================================
///    
/// PURPOSE: Displays the cheat activation message
///    
/// PARAMS:
///    sText - The cheat text
PROC DISPLAY_CHEAT_ACTIVATION_NOTIFICATION(STRING sText)
	IF IS_CHEAT_FROM_DIRECTOR_MODE("")
	//If in director mode, do not display this message
		CPRINTLN(DEBUG_DIRECTOR, "Cheat Controller: DISPLAY_CHEAT_ACTIVATION_NOTIFICATION - blocked (Director Mode)")
	ELSE
		BEGIN_TEXT_COMMAND_THEFEED_POST( "CHEAT_ACTIVATED" )
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL( sText )
	    STORE_FEED_MESSAGE_ID(END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE))
		
		PLAYSTATS_CHEAT_APPLIED(sText)
	ENDIF
ENDPROC

/// PURPOSE: Displays the cheat de-activation message
///    
/// PARAMS:
///    sText - The cheat text 
PROC DISPLAY_CHEAT_DEACTIVATION_NOTIFICATION(STRING sText)
	IF IS_CHEAT_FROM_DIRECTOR_MODE(sText)
	//If in director mode, do not display this message
		CPRINTLN(DEBUG_DIRECTOR, "Cheat Controller: DISPLAY_CHEAT_DEACTIVATION_NOTIFICATION - blocked (Director Mode)")
	ELSE
		BEGIN_TEXT_COMMAND_THEFEED_POST( "CHEAT_DEACTIVATED" )
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL( sText )
	    STORE_FEED_MESSAGE_ID(END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE))		
	ENDIF	
ENDPROC


/// PURPOSE: Displays the cheat de-activation message
///    
/// PARAMS:
///    sText - The cheat text 
PROC DISPLAY_CHEAT_DENIED_NOTIFICATION(STRING sText)
	
	//Don't display a help message if in Director mode.
	IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
		BEGIN_TEXT_COMMAND_THEFEED_POST( "CHEAT_DENIED" )
			ADD_TEXT_COMPONENT_SUBSTRING_TEXT_LABEL( sText )
    	STORE_FEED_MESSAGE_ID(END_TEXT_COMMAND_THEFEED_POST_TICKER(FALSE))
	ELSE
		CPRINTLN(DEBUG_DIRECTOR, "Cheat Controller: DISPLAY_CHEAT_DENIED_NOTIFICATION - blocked (Director Mode)")
	ENDIF

ENDPROC


/// PURPOSE:
///    Checks if phone is allowed on screen when a cheat is activated
/// RETURNS:
///    TRUE if the phone is onscreen and the cheat hasn't been triggered from a phone
FUNC BOOL IS_PHONE_FORBIDDEN()

	IF bIsPhoneCheat
		bIsPhoneCheat = FALSE
		RETURN FALSE
	ENDIF

	IF IS_PHONE_ONSCREEN()
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC

FUNC BOOL HAS_TIME_PASSED(INT iTimeAmount, INT iTimer)

	INT iCurrentTime
	INT iTimeDifference
	
	iCurrentTime = GET_GAME_TIMER()
	
	iTimeDifference = iCurrentTime - iTimeAmount
	
	IF iTimeDifference > iTimer
		RETURN TRUE
	ENDIF
	
	RETURN FALSE

ENDFUNC



FUNC BOOL IS_CHEAT_VEHICLE_SPAWN_POS_SAFE( MODEL_NAMES eVehicleModel, VECTOR vPos, FLOAT fHeading )

	VECTOR vCollCheck[MAX_COLLISION_POS]
	VECTOR vehicleSizeMin
	VECTOR vehicleSizeMax
	VECTOR vVehicleDimensions
	
	VECTOR vResult
	VECTOR vNormal
	INT	   iResult
		
	SHAPETEST_INDEX	iCollisionBottom1
	SHAPETEST_INDEX	iCollisionBottom2
	SHAPETEST_INDEX	iCollisionTop1
	SHAPETEST_INDEX	iCollisionTop2
	SHAPETEST_INDEX	iCollisionLeft
	SHAPETEST_INDEX	iCollisionRight
	SHAPETEST_INDEX	iCollisionLOS
	
	ENTITY_INDEX entityResult
		
	GET_MODEL_DIMENSIONS(eVehicleModel, vehicleSizeMin, vehicleSizeMax )
			
	vVehicleDimensions.x = ABSF(vehicleSizeMax.x - vehicleSizeMin.x) / 2
	vVehicleDimensions.y = ABSF(vehicleSizeMax.y - vehicleSizeMin.y) / 2
	vVehicleDimensions.z = ABSF(vehicleSizeMax.z - vehicleSizeMin.z) / 2
	
	vCollCheck[0] = vPos

	vCollCheck[1] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, << -vVehicleDimensions.x, -vVehicleDimensions.y, -vVehicleDimensions.z >>  )	
	vCollCheck[2] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, << vVehicleDimensions.x, -vVehicleDimensions.y, -vVehicleDimensions.z >>  )	
	vCollCheck[3] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, << vVehicleDimensions.x, vVehicleDimensions.y, -vVehicleDimensions.z >>  )	
	vCollCheck[4] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, << -vVehicleDimensions.x, vVehicleDimensions.y, -vVehicleDimensions.z >>  )

	vCollCheck[5] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, << -vVehicleDimensions.x, -vVehicleDimensions.y, vVehicleDimensions.z >>  )	
	vCollCheck[6] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, << vVehicleDimensions.x, -vVehicleDimensions.y, vVehicleDimensions.z >>  )	
	vCollCheck[7] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, << vVehicleDimensions.x, vVehicleDimensions.y, vVehicleDimensions.z >>  )	
	vCollCheck[8] = GET_OFFSET_FROM_COORD_AND_HEADING_IN_WORLD_COORDS(vPos, fHeading, << -vVehicleDimensions.x, vVehicleDimensions.y, vVehicleDimensions.z >>  )
		
	/*
	INT i = 0
	WHILE i < 15
		
		DRAW_DEBUG_LINE( vCollCheck[1], vCollCheck[2])
		DRAW_DEBUG_LINE( vCollCheck[2],vCollCheck[3])
		DRAW_DEBUG_LINE( vCollCheck[3],vCollCheck[4])
		DRAW_DEBUG_LINE( vCollCheck[4], vCollCheck[1])
				
		DRAW_DEBUG_LINE( vCollCheck[5], vCollCheck[6])
		DRAW_DEBUG_LINE( vCollCheck[6],vCollCheck[7])
		DRAW_DEBUG_LINE( vCollCheck[7],vCollCheck[8])
		DRAW_DEBUG_LINE( vCollCheck[8], vCollCheck[5])
				
		DRAW_DEBUG_LINE( vCollCheck[1], vCollCheck[5])
		DRAW_DEBUG_LINE( vCollCheck[2],vCollCheck[6])
		DRAW_DEBUG_LINE( vCollCheck[3],vCollCheck[7])
		DRAW_DEBUG_LINE( vCollCheck[4], vCollCheck[8])
		
		DRAW_DEBUG_LINE( vCollCheck[1], vCollCheck[3])
		DRAW_DEBUG_LINE( vCollCheck[2], vCollCheck[4])
		
		DRAW_DEBUG_LINE( vCollCheck[5], vCollCheck[7])
		DRAW_DEBUG_LINE( vCollCheck[6], vCollCheck[8])
		
		DRAW_DEBUG_LINE( vCollCheck[5], vCollCheck[1])
		DRAW_DEBUG_LINE( vCollCheck[6], vCollCheck[8])
		
		DRAW_DEBUG_LINE( vCollCheck[5], vCollCheck[2])
		DRAW_DEBUG_LINE( vCollCheck[6], vCollCheck[7])
				
		++i
		
		WAIT (0)
		
	ENDWHILE
	*/
	
	IF NOT IS_COLLISION_MARKED_OUTSIDE(vPos)
		CPRINTLN(DEBUG_AMBIENT, "Cheat Controller: IS_CHEAT_VEHICLE_SPAWN_POS_SAFE - FALSE- Inside interior")
		RETURN FALSE
	ENDIF
	
	iCollisionLOS = START_EXPENSIVE_SYNCHRONOUS_SHAPE_TEST_LOS_PROBE(GET_PLAYER_COORDS(PLAYER_ID()) + <<0.0,0.0,1.0>>, vPos, SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_GLASS|SCRIPT_INCLUDE_PED, PLAYER_PED_ID(), 0)
	GET_SHAPE_TEST_RESULT(iCollisionLOS, iResult, vResult, vNormal, EntityResult)
	
	IF iResult != 0
		CPRINTLN(DEBUG_AMBIENT, "Cheat Controller: IS_CHEAT_VEHICLE_SPAWN_POS_SAFE - FALSE- iCollisionLOS ray test failed.")
		RETURN FALSE
	ENDIF

	iCollisionBottom1 = START_EXPENSIVE_SYNCHRONOUS_SHAPE_TEST_LOS_PROBE(vCollCheck[1], vCollCheck[3], SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_GLASS|SCRIPT_INCLUDE_PED, PLAYER_PED_ID(), 0)
	GET_SHAPE_TEST_RESULT(iCollisionBottom1, iResult, vResult, vNormal, EntityResult)
	
	IF iResult != 0
		CPRINTLN(DEBUG_AMBIENT, "Cheat Controller: IS_CHEAT_VEHICLE_SPAWN_POS_SAFE - FALSE- iCollisionBottom1 ray test failed.")
		RETURN FALSE
	ENDIF
		
	iCollisionBottom2 = START_EXPENSIVE_SYNCHRONOUS_SHAPE_TEST_LOS_PROBE(vCollCheck[2], vCollCheck[4], SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_GLASS|SCRIPT_INCLUDE_PED, PLAYER_PED_ID(), 0)
	GET_SHAPE_TEST_RESULT(iCollisionBottom2, iResult, vResult, vNormal, EntityResult)
	
	IF iResult != 0
		CPRINTLN(DEBUG_AMBIENT, "Cheat Controller: IS_CHEAT_VEHICLE_SPAWN_POS_SAFE - FALSE- iCollisionBottom2 ray test failed.")
		RETURN FALSE
	ENDIF
		
		
	iCollisionTop1 = START_EXPENSIVE_SYNCHRONOUS_SHAPE_TEST_LOS_PROBE(vCollCheck[5], vCollCheck[7], SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_GLASS|SCRIPT_INCLUDE_PED, PLAYER_PED_ID(), 0)
	GET_SHAPE_TEST_RESULT(iCollisionTop1, iResult, vResult, vNormal, EntityResult)
	
	IF iResult != 0
		CPRINTLN(DEBUG_AMBIENT, "Cheat Controller: IS_CHEAT_VEHICLE_SPAWN_POS_SAFE - FALSE- iCollisionTop1 ray test failed.")
		RETURN FALSE
	ENDIF
	
	
	iCollisionTop2 = START_EXPENSIVE_SYNCHRONOUS_SHAPE_TEST_LOS_PROBE(vCollCheck[6], vCollCheck[8], SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_GLASS|SCRIPT_INCLUDE_PED, PLAYER_PED_ID(), 0)
	GET_SHAPE_TEST_RESULT(iCollisionTop2, iResult, vResult, vNormal, EntityResult)
	
	IF iResult != 0
		CPRINTLN(DEBUG_AMBIENT, "Cheat Controller: IS_CHEAT_VEHICLE_SPAWN_POS_SAFE - FALSE- iCollisionTop2 ray test failed.")
		RETURN FALSE
	ENDIF

	iCollisionLeft = START_EXPENSIVE_SYNCHRONOUS_SHAPE_TEST_LOS_PROBE(vCollCheck[1], vCollCheck[8], SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_GLASS|SCRIPT_INCLUDE_PED, PLAYER_PED_ID(), 0)
	GET_SHAPE_TEST_RESULT(iCollisionLeft, iResult, vResult, vNormal, EntityResult)
	
	IF iResult != 0
		CPRINTLN(DEBUG_AMBIENT, "Cheat Controller: IS_CHEAT_VEHICLE_SPAWN_POS_SAFE - FALSE- iCollisionLeft ray test failed.")
		RETURN FALSE
	ENDIF
	
	iCollisionRight = START_EXPENSIVE_SYNCHRONOUS_SHAPE_TEST_LOS_PROBE(vCollCheck[2], vCollCheck[7], SCRIPT_INCLUDE_MOVER|SCRIPT_INCLUDE_VEHICLE|SCRIPT_INCLUDE_OBJECT|SCRIPT_INCLUDE_GLASS|SCRIPT_INCLUDE_PED, PLAYER_PED_ID(), 0)
	GET_SHAPE_TEST_RESULT(iCollisionRight, iResult, vResult, vNormal, EntityResult)
	
	IF iResult != 0
		CPRINTLN(DEBUG_AMBIENT, "Cheat Controller: IS_CHEAT_VEHICLE_SPAWN_POS_SAFE - FALSE- iCollisionRight ray test failed.")
		RETURN FALSE
	ENDIF
		
	CPRINTLN(DEBUG_AMBIENT, "Cheat Controller: IS_CHEAT_VEHICLE_SPAWN_POS_SAFE - TRUE")
	
	RETURN TRUE
	
ENDFUNC

/// PURPOSE:
///    Verifies if a cheat vehicle is unlocked (car spawn available)
/// PARAMS:
///    eVehicleModel - Model of vehicle to check
/// RETURNS:
///    TRUE if the vehicle is available.
///    
FUNC BOOL IS_CHEAT_VEHICLE_UNLOCKED(MODEL_NAMES eVehicleModel)

	SWITCH eVehicleModel
	
		CASE DODO
		
			IF GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_RE_SEAPLANE_CITY, VEHGEN_S_FLAG_AVAILABLE) = FALSE
				RETURN FALSE
			ENDIF
		
		BREAK
		
		CASE DUKES2
		
			IF GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_RE_DUEL_CITY, VEHGEN_S_FLAG_AVAILABLE) = FALSE
				RETURN FALSE
			ENDIF
		
		BREAK
		
		CASE SUBMERSIBLE2
		
			IF GET_VEHICLE_GEN_SAVED_FLAG_STATE(VEHGEN_WILDPHOTO_SUB, VEHGEN_S_FLAG_AVAILABLE) = FALSE
				RETURN FALSE
			ENDIF

		BREAK
		
	ENDSWITCH
		
	// Assume vehicles are unlocked by default
	RETURN TRUE

ENDFUNC


/// =================================================================
///   TRIGGERS
/// =================================================================

/// PURPOSE: Set the vehicle cheat active.
///    
/// PARAMS:
///    eVehicleModel -	Vehicle model to spawn
///    sText - 			Vehicle cheat text
PROC TRIGGER_VEHICLE_CHEAT( MODEL_NAMES eVehicleModel, STRING sText)
	
	IF eCheatStatusSpawnVehicle = CHEAT_INACTIVE
		IF IS_CURRENTLY_ON_MISSION_OF_TYPE( MISSION_TYPE_MINIGAME )
		OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Barry1")) > 0) // Force off for Barry1
		OR (GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("Tennis")) > 0) // Force off for Tennis
		OR IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
		OR IS_CHEAT_DISABLED( CHEAT_TYPE_GENERAL )
		OR IS_CHEAT_DISABLED( CHEAT_TYPE_SPAWN_VEHICLE )
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
			EXIT
		ENDIF
		
		IF IS_PHONE_FORBIDDEN()
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
			EXIT
		ENDIF
		
		// Mainly for CG to NG
		IF NOT IS_CHEAT_VEHICLE_UNLOCKED( eVehicleModel)
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_VEHICLE_LOCKED_DENIED")
			EXIT
		ENDIF
		
		eCheatVehicleModel = eVehicleModel
		sCheatVehicleText = sText
		eCheatStatusSpawnVehicle = CHEAT_INITIALISE
		
	ENDIF

ENDPROC

/// 
/// PURPOSE: Set the slidey cars cheat active (off mission)
///    
PROC TRIGGER_SLIDEY_CARS_CHEAT()

	IF IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_GENERAL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_SLIDEY_CARS )
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
		EXIT
	ENDIF
	
	IF IS_PHONE_FORBIDDEN()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
		EXIT
	ENDIF
		
	IF eCheatStatusSlideyCars = CHEAT_INACTIVE
	
		eCheatStatusSlideyCars = CHEAT_ACTIVATE

	ELIF eCheatStatusSlideyCars = CHEAT_RUNNING
	
		eCheatStatusSlideyCars = CHEAT_DEACTIVATE
	
	ENDIF
		
ENDPROC

/// 
/// PURPOSE: Set the slowmo cheat active (off mission)
///    
PROC TRIGGER_SLOWMO_CHEAT()

	IF IS_ANY_MISSION_ACTIVE()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_MISSION_DENIED")
		EXIT
	ENDIF
	
	IF IS_PHONE_FORBIDDEN()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
		EXIT
	ENDIF
		
	IF IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_SLOWMO )
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
		EXIT
	ENDIF
		
	IF eCheatStatusAimSlowMo <> CHEAT_INACTIVE
	
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
	
	ELSE
		
		eCheatStatusSlowMo = CHEAT_ACTIVATE

	ENDIF
		

ENDPROC

/// 
/// PURPOSE: Set the aim slowmo cheat active (off mission)
///    
PROC TRIGGER_AIM_SLOWMO_CHEAT()

	IF IS_ANY_MISSION_ACTIVE()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_MISSION_DENIED")
		EXIT
	ENDIF
	
	IF IS_PHONE_FORBIDDEN()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
		EXIT
	ENDIF
		
	IF IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_SLOWMO )
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
		EXIT
	ENDIF
	
	IF eCheatStatusSlowMo <> CHEAT_INACTIVE
	
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")

	ELSE
		
		eCheatStatusAimSlowMo = CHEAT_ACTIVATE

	ENDIF
		

ENDPROC

/// 
/// PURPOSE: Set the gravity cheat active (off mission)
///    
PROC TRIGGER_0GRAVITY_CHEAT()

	
	IF IS_ANY_MISSION_ACTIVE()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_MISSION_DENIED")
		EXIT
	ENDIF
	
	IF IS_PHONE_FORBIDDEN()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
		EXIT
	ENDIF
		
	IF IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_0_GRAVITY )
	OR IS_CHEAT_ACTIVE(CHEAT_TYPE_SKYFALL)
	OR IS_ENTITY_IN_WATER(PLAYER_PED_ID())
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
		EXIT
	ENDIF
	
	eCheatStatus0Gravity = CHEAT_ACTIVATE

		

ENDPROC


/// PURPOSE: Toggle the drunk cheat on/off (Off mission)
///    
PROC TRIGGER_DRUNK_CHEAT()

	IF IS_ANY_MISSION_ACTIVE()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_MISSION_DENIED")
		eCheatStatusDrunk = CHEAT_INACTIVE
		EXIT
	ENDIF
	
	IF IS_PHONE_FORBIDDEN()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
		eCheatStatusDrunk = CHEAT_INACTIVE
		EXIT
	ENDIF
		
	IF IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_DRUNK )
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
		eCheatStatusDrunk = CHEAT_INACTIVE
		EXIT
	ENDIF

	// Toggle cheat off if already running.
	
	IF eCheatStatusDrunk = CHEAT_INACTIVE
	
		eCheatStatusDrunk = CHEAT_ACTIVATE

	ELIF eCheatStatusDrunk = CHEAT_RUNNING
	
		eCheatStatusDrunk = CHEAT_DEACTIVATE
	
	ENDIF

ENDPROC



PROC TRIGGER_FLAMING_BULLETS_CHEAT()

	
	IF IS_ANY_MISSION_ACTIVE()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_MISSION_DENIED")
		eCheatStatusFlamingBullets = CHEAT_INACTIVE
		EXIT
	ENDIF
	
	IF IS_PHONE_FORBIDDEN()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
		eCheatStatusFlamingBullets = CHEAT_INACTIVE
		EXIT
	ENDIF
		
	IF IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_FLAMING_BULLETS )
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
		eCheatStatusFlamingBullets = CHEAT_INACTIVE
		EXIT
	ENDIF
	
	// Toggle cheat off if already running.
	
	IF eCheatStatusFlamingBullets = CHEAT_INACTIVE
	
		eCheatStatusFlamingBullets = CHEAT_ACTIVATE

	ELIF eCheatStatusFlamingBullets = CHEAT_RUNNING
	
		eCheatStatusFlamingBullets = CHEAT_DEACTIVATE
	
	ENDIF

ENDPROC

PROC TRIGGER_BANG_BANG_CHEAT()

	IF IS_ANY_MISSION_ACTIVE()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_MISSION_DENIED")
		eCheatStatusBangBang = CHEAT_INACTIVE
		EXIT
	ENDIF
	
	IF IS_PHONE_FORBIDDEN()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
		eCheatStatusBangBang = CHEAT_INACTIVE
		EXIT
	ENDIF
		
	IF IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_BANG_BANG )
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
		eCheatStatusBangBang = CHEAT_INACTIVE
		EXIT
	ENDIF
	
	// Toggle cheat off if already running.
	
	IF eCheatStatusBangBang = CHEAT_INACTIVE
	
		eCheatStatusBangBang = CHEAT_ACTIVATE

	ELIF eCheatStatusBangBang = CHEAT_RUNNING
	
		eCheatStatusBangBang = CHEAT_DEACTIVATE
	
	ENDIF

ENDPROC

PROC TRIGGER_EXPLOSIVE_MELEE_CHEAT()

	IF IS_ANY_MISSION_ACTIVE()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_MISSION_DENIED")
		eCheatStatusExplosiveMelee = CHEAT_INACTIVE
		EXIT
	ENDIF
	
	IF IS_PHONE_FORBIDDEN()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
		eCheatStatusExplosiveMelee = CHEAT_INACTIVE
		EXIT
	ENDIF
		
	IF IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_EXPLOSIVE_MELEE )
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
		eCheatStatusExplosiveMelee = CHEAT_INACTIVE
		EXIT
	ENDIF

	// Toggle cheat off if already running.
	
	IF eCheatStatusExplosiveMelee = CHEAT_INACTIVE
	
		eCheatStatusExplosiveMelee = CHEAT_ACTIVATE

	ELIF eCheatStatusExplosiveMelee = CHEAT_RUNNING
	
		eCheatStatusExplosiveMelee = CHEAT_DEACTIVATE
		
	ENDIF

ENDPROC


PROC TRIGGER_SUPER_JUMP_CHEAT()

	IF IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_GENERAL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_SUPER_JUMP )
	OR IS_INTERIOR_SCENE()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
		EXIT
	ENDIF
	
	IF IS_PHONE_FORBIDDEN()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
		EXIT
	ENDIF

	// Toggle cheat off if already running.
	
	IF eCheatStatusSuperJump = CHEAT_INACTIVE
	
		eCheatStatusSuperJump = CHEAT_ACTIVATE

	ELIF eCheatStatusSuperJump = CHEAT_RUNNING
	
		eCheatStatusSuperJump = CHEAT_DEACTIVATE
	
	ENDIF
		

ENDPROC


PROC TRIGGER_INVINCIBILITY_CHEAT()

	VECTOR vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
		
	IF IS_ANY_MISSION_ACTIVE()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_MISSION_DENIED")
		EXIT
	ENDIF
	
	IF IS_PHONE_FORBIDDEN()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
		EXIT
	ENDIF
		
	IF IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_INVINCIBILITY )
	OR vPlayerPos.z <= -170.0	// For submersible crush depth.
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
		EXIT
	ENDIF
	
	// Toggle cheat off if already running.
	
	IF eCheatStatusInvincibility = CHEAT_INACTIVE
	
		eCheatStatusInvincibility = CHEAT_ACTIVATE

	ELIF eCheatStatusInvincibility = CHEAT_RUNNING
	
		eCheatStatusInvincibility = CHEAT_DEACTIVATE
	
	ENDIF
		
ENDPROC


PROC TRIGGER_SKYFALL_CHEAT()

	IF IS_ANY_MISSION_ACTIVE()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_MISSION_DENIED")
		EXIT
	ENDIF
	
	IF IS_PHONE_FORBIDDEN()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
		EXIT
	ENDIF
	
	IF IS_INTERIOR_SCENE()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_HERE")
		EXIT
	ENDIF
	
	IF eCheatStatusSkyfall != CHEAT_INACTIVE
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_ALREADY_ACTIVE")
		EXIT
	ENDIF
		
	IF IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_SKYFALL)
	OR IS_CHEAT_ACTIVE( CHEAT_TYPE_SUPER_JUMP )
	OR IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
	OR NOT IS_GAMEPLAY_CAM_RENDERING()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
		EXIT
	ENDIF
		
	IF IS_PED_INJURED(PLAYER_PED_ID()) OR IS_PLAYER_BEING_ARRESTED(PLAYER_ID(), FALSE)
	OR GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("respawn_controller")) > 0
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
		EXIT
	ENDIF
	
	// Ok to start
	eCheatStatusSkyfall = CHEAT_INITIALISE

	
ENDPROC


PROC TRIGGER_FAST_RUN_CHEAT

	IF IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_GENERAL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_FAST_RUN )
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME_FRIENDS)
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
		EXIT
	ENDIF
	
	IF IS_PHONE_FORBIDDEN()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
		EXIT
	ENDIF

	IF eCheatStatusFastRun = CHEAT_INACTIVE
	
		eCheatStatusFastRun = CHEAT_ACTIVATE

	ELIF eCheatStatusFastRun = CHEAT_RUNNING
	
		eCheatStatusFastRun = CHEAT_DEACTIVATE
	
	ENDIF

ENDPROC

PROC TRIGGER_FAST_SWIM_CHEAT

	IF IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_GENERAL )
	OR IS_CHEAT_DISABLED( CHEAT_TYPE_FAST_SWIM )
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME)
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME_FRIENDS)
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
		EXIT
	ENDIF
	
	IF IS_PHONE_FORBIDDEN()
		DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
		EXIT
	ENDIF

	IF eCheatStatusFastSwim = CHEAT_INACTIVE
	
		eCheatStatusFastSwim = CHEAT_ACTIVATE

	ELIF eCheatStatusFastSwim = CHEAT_RUNNING
	
		eCheatStatusFastSwim = CHEAT_DEACTIVATE
	
	ENDIF

ENDPROC

/// PURPOSE:
///    Checks if the phone has triggered a cheat
/// PARAMS:
///    eCheatEnum - Enum of cheat.
/// RETURNS:
///    TRUE if cheat has been triggered, false if not.
FUNC BOOL HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEATS_BITSET_ENUM eCheatEnum, MODEL_NAMES eVehicle = DUMMY_MODEL_FOR_SCRIPT)
	
	IF IS_BIT_SET( g_iCheatTriggerBitset, ENUM_TO_INT(eCheatEnum))
	
		// Vehicle has been passed in
		IF eVehicle != DUMMY_MODEL_FOR_SCRIPT
		
			// Vehicle matches, trigger cheat
			IF eVehicle = g_eCheatVehicleModelName
				//g_iCheatTriggerBitset = 0
				CLEAR_BIT(g_iCheatTriggerBitset, ENUM_TO_INT(eCheatEnum))
				bIsPhoneCheat = TRUE
				RETURN TRUE
			ELSE
				//Vehicle doesn't match
				RETURN FALSE	
			ENDIF
	
		ELSE	
			// No Vehicle passed in, trigger cheat
			//g_iCheatTriggerBitset = 0
			CLEAR_BIT(g_iCheatTriggerBitset, ENUM_TO_INT(eCheatEnum))
			bIsPhoneCheat = TRUE
			RETURN TRUE
		ENDIF
		
	ENDIF
	
	RETURN FALSE

ENDFUNC

PROC CONTROL_DIRECTOR_MODE_CHEAT_TIMER()
	//keep track of director mode timer
	IF IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR)
	OR (NOT IS_SCREEN_FADED_IN()) AND directorModeTimer != 0
		directorModeTimer = GET_GAME_TIMER()
	ENDIF
	
	//If director mode is currently running, or cleaning up.
	IF directorModeTimer != 0
		IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR) 
			IF GET_GAME_TIMER() - directorModeTimer > 1000
			//reset directorModeTimer
				CPRINTLN(DEBUG_DIRECTOR,"RESET directorModeTimer")
				directorModeTimer = 0
			ENDIF
		ENDIF
	ENDIF
ENDPROC

FUNC BOOL HAS_CHEAT_CODE_BEEN_ACTIVATED(INT hashOfCheatString, INT lengthOfCheatString)
	IF NOT IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR) 	
		RETURN HAS_CHEAT_WITH_HASH_BEEN_ACTIVATED(hashOfCheatString, lengthOfCheatString)
	ENDIF
	RETURN FALSE
ENDFUNC

PROC CHECK_CHEAT_CODES()

	// Disable the PC cheat code window during the end of mission results screen
	// If director mode is running, or just cleaned up, disable cheats
	IF IS_RESULT_SCREEN_DISPLAYING() 
	OR directorModeTimer != 0 
	OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_DIRECTOR) 	
		DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_ENTER_CHEAT_CODE)		
	ENDIF
	
	CONTROL_DIRECTOR_MODE_CHEAT_TIMER()
	
	// Don't allow player to trigger cheats if they are dead
	IF IS_PED_INJURED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	// Don't allow player to trigger cheats if they are being arrested
	IF IS_PED_BEING_ARRESTED(PLAYER_PED_ID())
		EXIT
	ENDIF
	
	// Donm
	IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
		EXIT
	ENDIF
	
	// Don't allow player to trigger cheats if player controls are disabled.
	IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
		EXIT
	ENDIF
	
	// Don't bother doing anything if switching.
	IF IS_PLAYER_PED_SWITCH_IN_PROGRESS() OR IS_PLAYER_SWITCH_IN_PROGRESS()
		EXIT
	ENDIF
	
	// Don't allow player to trigger cheats if a replay is being processed.
	IF IS_REPLAY_BEING_PROCESSED()
		IF NOT bFeedCleared
			CLEAR_FEED_MESSAGES()
			bFeedCleared = TRUE
		ENDIF
		EXIT
	ENDIF
	
	bFeedCleared = FALSE
	bIsPhoneCheat = FALSE
	
	// Vehicle spawn cheats


	// Buzzard -	Circle, Circle, L1, Circle, Circle, Circle, L1, L2, R1, Triangle, Circle, Triangle
	//					B, B, L1, B, B, B, L1, L2, R1, Y, B, Y
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("BB1BBB123YBY"), 12)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("BUZZOFF"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED( CHEAT_TYPE_SPAWN_VEHICLE, BUZZARD)
		TRIGGER_VEHICLE_CHEAT( BUZZARD, "CHEAT_SPAWN_VEH1" )
	ENDIF
	
	// BMX  -	Left, Left, Right, Right, Left, Right, Square, Circle, Triangle, R1, R2
	//			Left, Left, Right, Right, Left, Right, X, B, Y, R1, R2
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("LLRRLRXBY34"), 11)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("BANDIT"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED( CHEAT_TYPE_SPAWN_VEHICLE, BMX)
		TRIGGER_VEHICLE_CHEAT( BMX, "CHEAT_SPAWN_VEH2" )
	ENDIF

	// Caddy  - Circle, L1, Left, R1, L2, X, R1, L1, Circle, X
	// 			B, L1, Left, R1, L2, A, R1, L1, B, A
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("B1L32A31BA"), 10)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("HOLEIN1"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED( CHEAT_TYPE_SPAWN_VEHICLE, CADDY)
		TRIGGER_VEHICLE_CHEAT( CADDY, "CHEAT_SPAWN_VEH3" )
	ENDIF

	// Comet  - R1, Circle, R2, Right, L1, L2, X, X, Square, R1
	//			R1, B, R2, Right, L1, L2, A, A, X, R1
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("3B4R12AAX3"), 10)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("COMET"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED( CHEAT_TYPE_SPAWN_VEHICLE, COMET2)
		TRIGGER_VEHICLE_CHEAT( COMET2, "CHEAT_SPAWN_VEH4" )
	ENDIF

	//Crop duster - Right, Left, R1, R1, R1, Left, Triangle, Triangle, X, Circle, L1, L1
	//				Right, Left, R1, R1, R1, Left, Y, Y, A, B, L1, L1
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("RL333LYYAB11"), 12)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("FLYSPRAY"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED( CHEAT_TYPE_SPAWN_VEHICLE, DUSTER)
		TRIGGER_VEHICLE_CHEAT( DUSTER, "CHEAT_SPAWN_VEH5" )
	ENDIF

	// PCJ -	R1, Right, Left, Right, R2, Left, Right, Square, Right, L2, L1, L1
	// 			R1, Right, Left, Right, R2, Left, Right, X, Right, L2, L1, L1
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("3RLR4LRXR211"), 12)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("ROCKET"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED( CHEAT_TYPE_SPAWN_VEHICLE, PCJ)
		TRIGGER_VEHICLE_CHEAT( PCJ, "CHEAT_SPAWN_VEH6" )
	ENDIF

	// Rapid GT  - 	R2, L1, Circle, Right, L1, R1, Right, Left, Circle, R2
	//				R2, L1, B, Right, L1, R1, Right, Left, B, R2
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("41BR13RLB4"), 10)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("RAPIDGT"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED( CHEAT_TYPE_SPAWN_VEHICLE, RAPIDGT)
		TRIGGER_VEHICLE_CHEAT( RAPIDGT, "CHEAT_SPAWN_VEH7" )
	ENDIF

	// Sanchez  - Circle, X, L1, Circle, Circle, L1, Circle, R1, R2, L2, L1, L1
	//			- B, A, L1, B, B, L1, B, R1, R2, L2, L1, L1
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("BA1BB1B34211"), 12)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("OFFROAD"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED( CHEAT_TYPE_SPAWN_VEHICLE, SANCHEZ )
		TRIGGER_VEHICLE_CHEAT( SANCHEZ, "CHEAT_SPAWN_VEH8" )
	ENDIF

	// Stretch Limo -	R2, Right, L2, Left, Left, R1, L1, Circle, Right
	//				-	R2, Right, L2, Left, Left, R1, L1,  B, Right
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("4R2LL31BR"), 9)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("VINEWOOD"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED( CHEAT_TYPE_SPAWN_VEHICLE, STRETCH )
		TRIGGER_VEHICLE_CHEAT( STRETCH, "CHEAT_SPAWN_VEH9" )
	ENDIF

	// Stunt Plane -	Circle, Right, L1, L2, Left, R1, L1, L1, Left, Left, X, Triangle
	//					B, Right, L1, L2, Left, R1, L1, L1, Left, Left, A, Y
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("BR12L311LLAY"), 12)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("BARNSTORM"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED( CHEAT_TYPE_SPAWN_VEHICLE, STUNT )
		TRIGGER_VEHICLE_CHEAT( STUNT, "CHEAT_SPAWN_VEH10" )
	ENDIF

	// Trashmaster  -	Circle, R1, Circle, R1, Left, Left, R1, L1, Circle, Right
	// 					B, R1, B, R1, Left, Left, R1, L1, B, Right
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("B3B3LL31BR"), 10)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("TRASHED"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED( CHEAT_TYPE_SPAWN_VEHICLE, TRASH )
		TRIGGER_VEHICLE_CHEAT( TRASH, "CHEAT_SPAWN_VEH11" )
	ENDIF
	
	// New vehicle cheats for CG-->NG
	
	IF IS_LAST_GEN_PLAYER()
		
		// Sea Plane  -	Square, R1, Square, R1, Left, Left, R1, L1, Square, Right
		// 					X, R1, X, R1, Left, Left, R1, L1, X, Right
		IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("X3X3LL31XR"), 10)
		OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("EXTINCT"))
		OR HAS_CHEAT_BEEN_PHONE_TRIGGERED( CHEAT_TYPE_SPAWN_VEHICLE,DODO)
			TRIGGER_VEHICLE_CHEAT( DODO, "CHEAT_SPAWN_VEH12" )
		ENDIF
		
		
		// Duel car  -	Triangle, R1, Triangle, R1, Right, Left, R1, L1, Triange, Right
		// 					Y, R1, Y, R1, Right, Left, R1, L1, Y, Right
		IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("Y3Y3RL31YR"), 10)
		OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("DEATHCAR"))
		OR HAS_CHEAT_BEEN_PHONE_TRIGGERED( CHEAT_TYPE_SPAWN_VEHICLE, DUKES2 )
			TRIGGER_VEHICLE_CHEAT( DUKES2, "CHEAT_SPAWN_VEH13" )
		ENDIF
		
		// Bubble Sub -	R2, Left, L2, Right, Right, R1, L1, Square, Right
		//				-	R2, Left, L2, Right, Right, R1, L1,  X, Right
		IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("4L2RR31XR"), 9)
		OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("BUBBLES"))
		OR HAS_CHEAT_BEEN_PHONE_TRIGGERED( CHEAT_TYPE_SPAWN_VEHICLE, SUBMERSIBLE2 )
			TRIGGER_VEHICLE_CHEAT( SUBMERSIBLE2, "CHEAT_SPAWN_VEH14" )
		ENDIF
		
	ENDIF
	

//-----------------------------------------------------------------

	// General cheats

	// Super Jump - Left, Left, Triangle, Triangle, Right, Right, Left, Right, Square, R1, R2
	//				Left, Left, Y, Y, Right, Right, Left, Right, X, R1, R2 
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("LLYYRRLRX34"), 11)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("HOPTOIT"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_SUPER_JUMP)
		TRIGGER_SUPER_JUMP_CHEAT()
	ENDIF

	// Slidey Car Mode - 	Triangle, R1, R1, LEFT, R1, L1, R2, L1
	//						Y, R1, R1, Left, R1, L1, R2, L1
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("Y33L3141"), 8)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("SNOWDAY"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_SLIDEY_CARS)
		TRIGGER_SLIDEY_CARS_CHEAT()
	ENDIF

	// Fast Run - 	Triangle, Left, Right, Right, L2, L1, Square
	//				Y, Left, Right, Right, L2, L1, X				
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("YLRR21X"), 7)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("CATCHME"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_FAST_RUN)
		TRIGGER_FAST_RUN_CHEAT()
	ENDIF

	// Fast Swim -	Left, Left, L1, Right, Right, R2, Left, L2, Right
	//				Left, Left, L1, Right, Right, R2, Left, L2, Right	
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("LL1RR4L2R"), 9)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("GOTGILLS"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_FAST_SWIM)
		TRIGGER_FAST_SWIM_CHEAT()
	ENDIF

	// Give Weapons - 	R1, R2, L1, R2, Left, Left, Square, Right, Left, Right, Square, 
	//					R1, R2, L1, R2, Left, Left, X, Right, Left, Right, Right, X 
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("3414LLXRLRRX"), 12)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("TOOLUP"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_GIVE_WEAPONS)
		eCheatStatusWeapons = CHEAT_ACTIVATE
	ENDIF

	// Advance Weather -	R2, X, L1, L1, L2, L2, L2, Square
	//						R2, A, L1, L1, L2, L2, L2, X
	// UNKNOWN WEATHER TYPE
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("4A11222X"), 8)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("MAKEITRAIN"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_ADVANCE_WEATHER)
		eCheatStatusAdvanceWeather = CHEAT_ACTIVATE
	ENDIF

	// Health and armour -	R1, R2, L1, X, Left, Left, Right, Right, Left, Square, Right, Square
	//						R1, R2, L1, A, Left, Left, Right, Right, Left, X, Right, X
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("341ALLRRLXRX"), 12)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("TURTLE"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_GIVE_HEALTH_ARMOR)
		eCheatStatusHealthArmor = CHEAT_ACTIVATE
	ENDIF
	

	// Special Ability recharge -	X, X, Square, R1, L1, X, Right, Left, X
	//								A, A, X, R1, L1, A, Right, Left, A 
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("AAX31ARLA"), 9)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("POWERUP"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_SPECIAL_ABILITY_RECHARGE)
		eCheatStatusSpecialAbilityRecharge = CHEAT_ACTIVATE
	ENDIF

	// Wanted Level Up -	R1, R1, Circle, R2, Left, Right, Left, Right, Left, Right
	//						R1, R1, B, R2, Left, Right, Left, Right, Left, Right
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("33B4LRLRLR"), 10)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("FUGITIVE"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_WANTED_LEVEL_UP)
		eCheatStatusWantedLevelUp = CHEAT_ACTIVATE
	ENDIF

	// Wanted Level Down - R1, R1, Circle R2, Right, Left, Right, Left,Right, Left
	//						R1, R1, B, R2, Right, Left, Right, Left, Right, Left
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("33B4RLRLRL"), 10)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("LAWYERUP"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_WANTED_LEVEL_DOWN)
		eCheatStatusWantedLevelDown = CHEAT_ACTIVATE
	ENDIF

	// Give Parachute -	Left, Right, L1, L2, R1, R2, R2, Left, Left, Right, L1
	// 					Left, Right, L1, L2, R1, R2, R2, Left, Left, Right, L1
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("LR12344LLR1"), 11)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("SKYDIVE"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_GIVE_PARACHUTE)
		eCheatStatusGiveParachute = CHEAT_ACTIVATE
	ENDIF

//-----------------------------------------------------------------

	// Off mission cheats
	
	// Bang Bang -	Right, Square, X, Left, R1, R2, Left, Right, Right, L1, L1, L1
	//				Right, X, A, Left R1, R2, Left, Right, Right, L1, L1, L1					 
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("RXAL34LRR111"), 12)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("HIGHEX"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_BANG_BANG)
		TRIGGER_BANG_BANG_CHEAT()
	ENDIF

	//  Flaming Bullets -	L1, R1, Square, R1, Left, R2, R1, Left, Square, Right, L1, L1
	// 						L1, R1, X, R1, Left, R2, R1, Left, X, Right, L1, L1
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("13X3L43LXR11"), 12)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("INCENDIARY"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_FLAMING_BULLETS)
		TRIGGER_FLAMING_BULLETS_CHEAT()
	ENDIF

	// Explosive Melee Attacks -	Right, Left, X, Triangle, R1, Circle, Circle, Circle, L2
	//								Right, Left, A, Y, R1, B, B, B, L2
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("RLAY3BBB2"), 9)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("HOTHANDS"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_EXPLOSIVE_MELEE)
		TRIGGER_EXPLOSIVE_MELEE_CHEAT()
	ENDIF


	// 0 Gravity - 	Left, Left, L1, R1, L1, Right, Left, L1, Left
	//				Left, Left, L1, R1, L1, Right, Left, L1, Left
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("LL131RL1L"), 9)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("FLOATER"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_0_GRAVITY)
		TRIGGER_0GRAVITY_CHEAT()
	ENDIF
	
	// Invincibility -	Right, X, Right, Left, Right, R1, Right, Left, X, Triangle
	//					Right, A, Right, Left, Right, R1, Right, Left, A, Y
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("RARLR3RLAY"), 10)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("PAINKILLER"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_INVINCIBILITY)
		TRIGGER_INVINCIBILITY_CHEAT()
	ENDIF

	//  Slow-mo mode -	Triangle, Left, Right, Right, Square, R2, R1
	//					Y, Left, Right, Right, X, R2, R1
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("YLRRX43"), 7)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("SLOWMO"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_SLOWMO)
		TRIGGER_SLOWMO_CHEAT()
	ENDIF

	// Skyfall -	L1, L2, R1, R2, Left, Right, Left, Right, L1, L2, R1, R2, Left, Right, Left, Right
	//				L1, L2, R1, R2, Left, Right, Left, Right, L1, L2, R1, R2, Left, Right, Left, Right
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("1234LRLR1234LRLR"), 16)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("SKYFALL"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_SKYFALL)
		TRIGGER_SKYFALL_CHEAT()
	ENDIF
	
	// Drunk Mode - Triangle, Right, Right, Left, Right, Square, Circle, Left
	//				Y, Right, Right, Left, Right, X, B, Left
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("YRRLRXBL"), 8)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("LIQUOR"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_DRUNK)
		TRIGGER_DRUNK_CHEAT()
	ENDIF

	// Aim Slow Down Time - 	Square, L2, R1, Triangle, Left, Square, L2, Right, X 
	//							X, L2, R1, Y, Left, X, L2, Right, A
	IF HAS_CHEAT_CODE_BEEN_ACTIVATED( HASH("X23YLX2RA"), 9)
	OR HAS_PC_CHEAT_WITH_HASH_BEEN_ACTIVATED( HASH("DEADEYE"))
	OR HAS_CHEAT_BEEN_PHONE_TRIGGERED(CHEAT_TYPE_AIM_SLOWMO)
		TRIGGER_AIM_SLOWMO_CHEAT()
	ENDIF
	
ENDPROC

#IF IS_DEBUG_BUILD

	// Basic widget struct
	STRUCT sCheatWidget
		WIDGET_GROUP_ID groupID
		BOOL			bToggle
	ENDSTRUCT

	// Debug widget vars
	WIDGET_GROUP_ID mainWidget
	WIDGET_GROUP_ID	generalCheatWidget
	WIDGET_GROUP_ID vehicleCheatWidget
	WIDGET_GROUP_ID	offMissionCheatWidget
	WIDGET_GROUP_ID disableCheatsWidget

	sCheatWidget	cheatWidgetEnableDebugWidgets
	sCheatWidget	cheatWidgetEnableDebugDisplay

	// Vehicle Spawn cheats
	sCheatWidget	cheatWidgetSpawnBuzzard
	sCheatWidget	cheatWidgetSpawnBMX
	sCheatWidget	cheatWidgetSpawnCaddy
	sCheatWidget	cheatWidgetSpawnComet
	sCheatWidget	cheatWidgetSpawnDuster
	sCheatWidget	cheatWidgetSpawnPCJ
	sCheatWidget	cheatWidgetSpawnRapidGT
	sCheatWidget	cheatWidgetSpawnSanchez
	sCheatWidget	cheatWidgetSpawnStretchLimo
	sCheatWidget	cheatWidgetSpawnStuntPlane
	sCheatWidget	cheatWidgetSpawnTrashmaster
	sCheatWidget	cheatWidgetSpawnSeaPlane
	sCheatWidget	cheatWidgetSpawnDuel
	sCheatWidget	cheatWidgetSpawnBubbleSub

	// General cheats
	sCheatWidget	cheatWidgetSuperJump
	sCheatWidget	cheatWidgetSlideyCars
	sCheatWidget	cheatWidgetFastRun
	sCheatWidget	cheatWidgetFastSwim
	sCheatWidget	cheatWidgetWeapons
	sCheatWidget	cheatWidgetAdvanceWeather
	sCheatWidget	cheatWidgetHealthArmor
	sCheatWidget	cheatWidgetSpecialAbilityRecharge
	sCheatWidget	cheatWidgetWantedLevelUp
	sCheatWidget	cheatWidgetWantedLevelDown
	sCheatWidget	cheatWidgetGiveParachute

	// Off mission only cheats
	sCheatWidget	cheatWidgetBangBang
	sCheatWidget	cheatWidgetFlamingBullets
	sCheatWidget	cheatWidgetExplosiveMelee
	sCheatWidget	cheatWidget0Gravity
	sCheatWidget	cheatWidgetInvincibility
	sCheatWidget	cheatWidgetSlowMo
	sCheatWidget	cheatWidgetSkyfall
	sCheatWidget	cheatWidgetDrunk
	sCheatWidget	cheatWidgetAimSloMo
	
	// Disable cheat widgets
	sCheatWidget	cheatWidgetDisableSuperJump
	sCheatWidget	cheatWidgetDisableSlideyCars
	sCheatWidget	cheatWidgetDisableFastRun
	sCheatWidget	cheatWidgetDisableFastSwim
	sCheatWidget	cheatWidgetDisableWeapons
	sCheatWidget	cheatWidgetDisableAdvanceWeather
	sCheatWidget	cheatWidgetDisableHealthArmor
	sCheatWidget	cheatWidgetDisableSpecialAbilityRecharge
	sCheatWidget	cheatWidgetDisableWantedLevelUp
	sCheatWidget	cheatWidgetDisableWantedLevelDown
	sCheatWidget	cheatWidgetDisableGiveParachute
	sCheatWidget	cheatWidgetDisableBangBang
	sCheatWidget	cheatWidgetDisableFlamingBullets
	sCheatWidget	cheatWidgetDisableExplosiveMelee
	sCheatWidget	cheatWidgetDisable0Gravity
	sCheatWidget	cheatWidgetDisableInvincibility
	sCheatWidget	cheatWidgetDisableSlowMo
	sCheatWidget	cheatWidgetDisableSkyfall
	sCheatWidget	cheatWidgetDisableDrunk
	sCheatWidget	cheatWidgetDisableAimSloMo
	sCheatWidget	cheatWidgetDisableVehicleSpawns
	sCheatWidget	cheatWidgetDisableOffMission
	sCheatWidget	cheatWidgetDisableGeneral
	sCheatWidget	cheatWidgetDisableAll
	
	PROC SETUP_BASIC_CHEAT_WIDGETS()
	
		mainWidget = START_WIDGET_GROUP("Cheat Controller")
		ADD_WIDGET_BOOL("Toggle cheat widgets", cheatWidgetEnableDebugWidgets.bToggle)
		STOP_WIDGET_GROUP()
	
	ENDPROC
	
	/// PURPOSE: Create  widget for cheats
	PROC SETUP_CHEAT_WIDGETS()

		mainWidget = START_WIDGET_GROUP("Cheat Controller")
		ADD_WIDGET_BOOL("Toggle cheat widgets", cheatWidgetEnableDebugWidgets.bToggle)
		ADD_WIDGET_BOOL("Display cheat debug info", cheatWidgetEnableDebugDisplay.bToggle)

		disableCheatsWidget = START_WIDGET_GROUP("Disable cheats")
		ADD_WIDGET_BOOL("Disable/Enable ALL cheats ", cheatWidgetDisableAll.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Off Mission cheats ", cheatWidgetDisableOffMission.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable General cheats ", cheatWidgetDisableGeneral.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Spawn Vehicle cheats ", cheatWidgetDisableVehicleSpawns.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Advance weather", cheatWidgetDisableAdvanceWeather.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Fast run", cheatWidgetDisableFastRun.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Fast swim", cheatWidgetDisableFastSwim.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Slidey cars", cheatWidgetDisableSlideyCars.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Super jump", cheatWidgetDisableSuperJump.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Give max health and armor", cheatWidgetDisableHealthArmor.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Give parachute", cheatWidgetDisableGiveParachute.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Give weapons", cheatWidgetDisableWeapons.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Recharge special ability", cheatWidgetDisableSpecialAbilityRecharge.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Wanted level down", cheatWidgetDisableWantedLevelDown.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Wanted level up", cheatWidgetDisableWantedLevelUp.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Enable 0 Gravity", cheatWidgetDisable0Gravity.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Enable aim slo-mo mode", cheatWidgetDisableAimSloMo.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Enable drunk mode", cheatWidgetDisableDrunk.bToggle)
		ADD_WIDGET_BOOL("Disable Enable explosive melee", cheatWidgetDisableExplosiveMelee.bToggle)
		ADD_WIDGET_BOOL("Disable Enable explosive shots", cheatWidgetDisableBangBang.bToggle)
		ADD_WIDGET_BOOL("Disable Enable flaming bullets", cheatWidgetDisableFlamingBullets.bToggle)
		ADD_WIDGET_BOOL("Disable Enable Invincibility", cheatWidgetDisableInvincibility.bToggle)
		ADD_WIDGET_BOOL("Disable Enable skyfall mode", cheatWidgetDisableSkyfall.bToggle)
		ADD_WIDGET_BOOL("Disable/Enable Cycle slow-mo", cheatWidgetDisableSlowMo.bToggle)
		STOP_WIDGET_GROUP()
		
		// Vehicle spawn cheats
		vehicleCheatWidget = START_WIDGET_GROUP("Vehicle Spawn Cheats")
		ADD_WIDGET_BOOL("Spawn Buzzard", cheatWidgetSpawnBuzzard.bToggle)
		ADD_WIDGET_BOOL("Spawn BMX", cheatWidgetSpawnBMX.bToggle)
		ADD_WIDGET_BOOL("Spawn Caddy", cheatWidgetSpawnCaddy.bToggle)
		ADD_WIDGET_BOOL("Spawn Comet", cheatWidgetSpawnComet.bToggle)
		ADD_WIDGET_BOOL("Spawn Duster", cheatWidgetSpawnDuster.bToggle)
		ADD_WIDGET_BOOL("Spawn PCJ", cheatWidgetSpawnPCJ.bToggle)
		ADD_WIDGET_BOOL("Spawn Rapid GT", cheatWidgetSpawnRapidGT.bToggle)
		ADD_WIDGET_BOOL("Spawn Sanchez", cheatWidgetSpawnSanchez.bToggle)
		ADD_WIDGET_BOOL("Spawn Stretch Limo", cheatWidgetSpawnStretchLimo.bToggle)
		ADD_WIDGET_BOOL("Spawn Stunt Plane", cheatWidgetSpawnStuntPlane.bToggle)
		ADD_WIDGET_BOOL("Spawn Trashmaster", cheatWidgetSpawnTrashmaster.bToggle)
		ADD_WIDGET_BOOL("Spawn Dodo Sea Plane: CG to NG only. Must be unlocked in-game.", cheatWidgetSpawnSeaPlane.bToggle)
		ADD_WIDGET_BOOL("Spawn Dukes O'Death: CG to NG only. Must be unlocked in-game.", cheatWidgetSpawnDuel.bToggle)
		ADD_WIDGET_BOOL("Spawn Kraken Sub: CG to NG only. Must be unlocked in-game.", cheatWidgetSpawnBubbleSub.bToggle)
		STOP_WIDGET_GROUP()

		// General Cheats
		generalCheatWidget = START_WIDGET_GROUP("General Cheats")
		ADD_WIDGET_BOOL("Advance weather", cheatWidgetAdvanceWeather.bToggle)
		ADD_WIDGET_BOOL("Activate fast run", cheatWidgetFastRun.bToggle)
		ADD_WIDGET_BOOL("Activate fast swim", cheatWidgetFastSwim.bToggle)
		ADD_WIDGET_BOOL("Activate slidey cars", cheatWidgetSlideyCars.bToggle)
		ADD_WIDGET_BOOL("Activate super jump", cheatWidgetSuperJump.bToggle)
		ADD_WIDGET_BOOL("Give max health and armor", cheatWidgetHealthArmor.bToggle)
		ADD_WIDGET_BOOL("Give parachute", cheatWidgetGiveParachute.bToggle)
		ADD_WIDGET_BOOL("Give weapons", cheatWidgetWeapons.bToggle)
		ADD_WIDGET_BOOL("Recharge special ability", cheatWidgetSpecialAbilityRecharge.bToggle)
		ADD_WIDGET_BOOL("Wanted level down", cheatWidgetWantedLevelDown.bToggle)
		ADD_WIDGET_BOOL("Wanted level up", cheatWidgetWantedLevelUp.bToggle)
		STOP_WIDGET_GROUP()

		// Off mission cheats
		offMissionCheatWidget = START_WIDGET_GROUP("Off mission Cheats")
		ADD_WIDGET_BOOL("Activate 0 Gravity", cheatWidget0Gravity.bToggle)
		ADD_WIDGET_BOOL("Activate aim slo-mo mode", cheatWidgetAimSloMo.bToggle)
		ADD_WIDGET_BOOL("Activate drunk mode", cheatWidgetDrunk.bToggle)
		ADD_WIDGET_BOOL("Activate explosive melee", cheatWidgetExplosiveMelee.bToggle)
		ADD_WIDGET_BOOL("Activate explosive shots", cheatWidgetBangBang.bToggle)
		ADD_WIDGET_BOOL("Activate flaming bullets", cheatWidgetFlamingBullets.bToggle)
		ADD_WIDGET_BOOL("Activate Invincibility", cheatWidgetInvincibility.bToggle)
		ADD_WIDGET_BOOL("Activate skyfall mode", cheatWidgetSkyfall.bToggle)
		ADD_WIDGET_BOOL("Activate slow-mo (3 levels)", cheatWidgetSlowMo.bToggle)
		STOP_WIDGET_GROUP()

		// Stop main widget group
		STOP_WIDGET_GROUP()
	ENDPROC
	
	PROC CLEANUP_WIDGETS()
	
		IF DOES_WIDGET_GROUP_EXIST(mainWidget)
			DELETE_WIDGET_GROUP(mainWidget)
		ENDIF

		IF DOES_WIDGET_GROUP_EXIST(generalCheatWidget)
			DELETE_WIDGET_GROUP(generalCheatWidget)
		ENDIF

		IF DOES_WIDGET_GROUP_EXIST(offMissionCheatWidget)
			DELETE_WIDGET_GROUP(offMissionCheatWidget)
		ENDIF

		IF DOES_WIDGET_GROUP_EXIST(vehicleCheatWidget)
			DELETE_WIDGET_GROUP(vehicleCheatWidget)
		ENDIF
		
		IF DOES_WIDGET_GROUP_EXIST(disableCheatsWidget)
			DELETE_WIDGET_GROUP(disableCheatsWidget)
		ENDIF

	ENDPROC
	
	PROC PROCESS_DISABLE_CHEAT_WIDGETS()
	
		IF cheatWidgetDisableAll.bToggle 
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_ALL)
				DISABLE_CHEAT( CHEAT_TYPE_ALL, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_ALL, TRUE )
			ENDIF
			cheatWidgetDisableAll.bToggle = FALSE
		ENDIF
		
		IF cheatWidgetDisableVehicleSpawns.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_SPAWN_VEHICLE)
				DISABLE_CHEAT( CHEAT_TYPE_SPAWN_VEHICLE, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_SPAWN_VEHICLE, TRUE )
			ENDIF
			cheatWidgetDisableVehicleSpawns.bToggle = FALSE
		ENDIF
		
		IF cheatWidgetDisableOffMission.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_OFF_MISSION)
				DISABLE_CHEAT( CHEAT_TYPE_OFF_MISSION, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_OFF_MISSION, TRUE )
			ENDIF
			cheatWidgetDisableOffMission.bToggle = FALSE
		ENDIF
		
		IF cheatWidgetDisableGeneral.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_GENERAL)
				DISABLE_CHEAT( CHEAT_TYPE_GENERAL, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_GENERAL, TRUE )
			ENDIF
			cheatWidgetDisableGeneral.bToggle = FALSE
		ENDIF
		
		IF cheatWidgetDisableSuperJump.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_SUPER_JUMP)
				DISABLE_CHEAT( CHEAT_TYPE_SUPER_JUMP, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_SUPER_JUMP, TRUE )
			ENDIF
			cheatWidgetDisableSuperJump.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableSlideyCars.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_SLIDEY_CARS)
				DISABLE_CHEAT( CHEAT_TYPE_SLIDEY_CARS, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_SLIDEY_CARS, TRUE )
			ENDIF
			cheatWidgetDisableSlideyCars.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableFastRun.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_FAST_RUN)
				DISABLE_CHEAT( CHEAT_TYPE_FAST_RUN, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_FAST_RUN, TRUE )
			ENDIF
			cheatWidgetDisableFastRun.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableFastSwim.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_FAST_SWIM)
				DISABLE_CHEAT( CHEAT_TYPE_FAST_SWIM, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_FAST_SWIM, TRUE )
			ENDIF
			cheatWidgetDisableFastSwim.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableWeapons.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_GIVE_WEAPONS)
				DISABLE_CHEAT( CHEAT_TYPE_GIVE_WEAPONS, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_GIVE_WEAPONS, TRUE )
			ENDIF
			cheatWidgetDisableWeapons.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableAdvanceWeather.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_ADVANCE_WEATHER)
				DISABLE_CHEAT( CHEAT_TYPE_ADVANCE_WEATHER, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_ADVANCE_WEATHER, TRUE )
			ENDIF
			cheatWidgetDisableAdvanceWeather.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableHealthArmor.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_GIVE_HEALTH_ARMOR)
				DISABLE_CHEAT( CHEAT_TYPE_GIVE_HEALTH_ARMOR, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_GIVE_HEALTH_ARMOR, TRUE )
			ENDIF
			cheatWidgetDisableHealthArmor.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableSpecialAbilityRecharge.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_SPECIAL_ABILITY_RECHARGE)
				DISABLE_CHEAT( CHEAT_TYPE_SPECIAL_ABILITY_RECHARGE, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_SPECIAL_ABILITY_RECHARGE, TRUE )
			ENDIF
			cheatWidgetDisableSpecialAbilityRecharge.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableWantedLevelUp.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_WANTED_LEVEL_UP)
				DISABLE_CHEAT( CHEAT_TYPE_WANTED_LEVEL_UP, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_WANTED_LEVEL_UP, TRUE )
			ENDIF
			cheatWidgetDisableWantedLevelUp.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableWantedLevelDown.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_WANTED_LEVEL_DOWN)
				DISABLE_CHEAT( CHEAT_TYPE_WANTED_LEVEL_DOWN, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_WANTED_LEVEL_DOWN, TRUE )
			ENDIF
			cheatWidgetDisableWantedLevelDown.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableGiveParachute.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_GIVE_PARACHUTE)
				DISABLE_CHEAT( CHEAT_TYPE_GIVE_PARACHUTE, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_GIVE_PARACHUTE, TRUE )
			ENDIF
			cheatWidgetDisableGiveParachute.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableBangBang.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_BANG_BANG)
				DISABLE_CHEAT( CHEAT_TYPE_BANG_BANG, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_BANG_BANG, TRUE )
			ENDIF
			cheatWidgetDisableBangBang.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableFlamingBullets.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_FLAMING_BULLETS)
				DISABLE_CHEAT( CHEAT_TYPE_FLAMING_BULLETS, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_FLAMING_BULLETS, TRUE )
			ENDIF
			cheatWidgetDisableFlamingBullets.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableExplosiveMelee.bToggle = TRUE
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_EXPLOSIVE_MELEE)
				DISABLE_CHEAT( CHEAT_TYPE_EXPLOSIVE_MELEE, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_EXPLOSIVE_MELEE, TRUE )
			ENDIF
			cheatWidgetDisableExplosiveMelee.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisable0Gravity.bToggle = TRUE
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_0_GRAVITY)
				DISABLE_CHEAT( CHEAT_TYPE_0_GRAVITY, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_0_GRAVITY, TRUE )
			ENDIF
			cheatWidgetDisable0Gravity.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableInvincibility.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_INVINCIBILITY)
				DISABLE_CHEAT( CHEAT_TYPE_INVINCIBILITY, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_INVINCIBILITY, TRUE )
			ENDIF
			cheatWidgetDisableInvincibility.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableSlowMo.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_SLOWMO)
				DISABLE_CHEAT( CHEAT_TYPE_SLOWMO, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_SLOWMO, TRUE )
			ENDIF
			cheatWidgetDisableSlowMo.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableSkyfall.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_SKYFALL)
				DISABLE_CHEAT( CHEAT_TYPE_SKYFALL, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_SKYFALL, TRUE )
			ENDIF
			cheatWidgetDisableSkyfall.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableDrunk.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_DRUNK)
				DISABLE_CHEAT( CHEAT_TYPE_DRUNK, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_DRUNK, TRUE )
			ENDIF
			cheatWidgetDisableDrunk.bToggle = FALSE
		ENDIF

		IF cheatWidgetDisableAimSloMo.bToggle
			IF IS_CHEAT_DISABLED(CHEAT_TYPE_AIM_SLOWMO)
				DISABLE_CHEAT( CHEAT_TYPE_AIM_SLOWMO, FALSE )
			ELSE
				DISABLE_CHEAT( CHEAT_TYPE_AIM_SLOWMO, TRUE )
			ENDIF
			cheatWidgetDisableAimSloMo.bToggle = FALSE
		ENDIF	
	
	ENDPROC
	

/// PURPOSE: Checks the cheat widget statuses and enables them if the checkboxes are ticked.
///    
	PROC PROCESS_CHEAT_WIDGETS
	
		// Don't allow player to trigger cheats if they are dead
		IF IS_PED_INJURED(PLAYER_PED_ID())
			EXIT
		ENDIF
		
		// Don't allow player to trigger cheats if player controls are disabled.
		IF NOT IS_PLAYER_CONTROL_ON(PLAYER_ID())
			EXIT
		ENDIF
		
		IF cheatWidgetEnableDebugWidgets.bToggle = TRUE
			
			IF bWidgetsCreated = FALSE
				CLEANUP_WIDGETS()
				SETUP_CHEAT_WIDGETS()
				bWidgetsCreated = TRUE
			ENDIF
			
		ELSE
		
			IF bWidgetsCreated
				CLEANUP_WIDGETS()
				SETUP_BASIC_CHEAT_WIDGETS()
				bWidgetsCreated = FALSE
			ENDIF
			
			EXIT
		
		ENDIF

		// Vehicle spawn cheats

		IF cheatWidgetSpawnBuzzard.bToggle = TRUE
			cheatWidgetSpawnBuzzard.bToggle = FALSE

			//SET_BIT( g_iCheatTriggerBitset, ENUM_TO_INT(CHEAT_TYPE_SPAWN_VEHICLE) )
			//g_eCheatVehicleModelName = BUZZARD
			TRIGGER_VEHICLE_CHEAT( BUZZARD, "CHEAT_SPAWN_VEH1" )

		ENDIF

		IF cheatWidgetSpawnBMX.bToggle = TRUE
			cheatWidgetSpawnBMX.bToggle = FALSE
			
			//SET_BIT( g_iCheatTriggerBitset, ENUM_TO_INT(CHEAT_TYPE_SPAWN_VEHICLE) )
			//g_eCheatVehicleModelName = BMX

			TRIGGER_VEHICLE_CHEAT( BMX, "CHEAT_SPAWN_VEH2" )

		ENDIF

		IF cheatWidgetSpawnCaddy.bToggle = TRUE
			cheatWidgetSpawnCaddy.bToggle = FALSE
			
			SET_BIT( g_iCheatTriggerBitset,  ENUM_TO_INT(CHEAT_TYPE_SPAWN_VEHICLE) )
			g_eCheatVehicleModelName = CADDY

			//TRIGGER_VEHICLE_CHEAT( CADDY, "CHEAT_SPAWN_VEH3" )

		ENDIF

		IF cheatWidgetSpawnComet.bToggle = TRUE
			cheatWidgetSpawnComet.bToggle = FALSE

			TRIGGER_VEHICLE_CHEAT( COMET2, "CHEAT_SPAWN_VEH4" )

		ENDIF

		IF cheatWidgetSpawnDuster.bToggle = TRUE
			cheatWidgetSpawnDuster.bToggle = FALSE

			TRIGGER_VEHICLE_CHEAT( DUSTER, "CHEAT_SPAWN_VEH5" )

		ENDIF

		IF cheatWidgetSpawnPCJ.bToggle = TRUE
			cheatWidgetSpawnPCJ.bToggle = FALSE

			TRIGGER_VEHICLE_CHEAT( PCJ, "CHEAT_SPAWN_VEH6" )

		ENDIF

		IF cheatWidgetSpawnRapidGT.bToggle = TRUE
			cheatWidgetSpawnRapidGT.bToggle = FALSE

			TRIGGER_VEHICLE_CHEAT( RAPIDGT, "CHEAT_SPAWN_VEH7" )

		ENDIF

		IF cheatWidgetSpawnSanchez.bToggle = TRUE
			cheatWidgetSpawnSanchez.bToggle = FALSE

			TRIGGER_VEHICLE_CHEAT( SANCHEZ, "CHEAT_SPAWN_VEH8" )

		ENDIF

		IF cheatWidgetSpawnStretchLimo.bToggle = TRUE
			cheatWidgetSpawnStretchLimo.bToggle = FALSE

			TRIGGER_VEHICLE_CHEAT( STRETCH, "CHEAT_SPAWN_VEH9" )

		ENDIF

		IF cheatWidgetSpawnStuntPlane.bToggle = TRUE
			cheatWidgetSpawnStuntPlane.bToggle = FALSE

			TRIGGER_VEHICLE_CHEAT( STUNT, "CHEAT_SPAWN_VEH10" )

		ENDIF

		IF cheatWidgetSpawnTrashmaster.bToggle = TRUE
			cheatWidgetSpawnTrashmaster.bToggle = FALSE

			TRIGGER_VEHICLE_CHEAT( TRASH, "CHEAT_SPAWN_VEH11" )

		ENDIF
		
		
		// New NG vehicle cheats
		
		IF cheatWidgetSpawnSeaPlane.bToggle = TRUE
			cheatWidgetSpawnSeaPlane.bToggle = FALSE

			IF IS_LAST_GEN_PLAYER()
				TRIGGER_VEHICLE_CHEAT( DODO, "CHEAT_SPAWN_VEH12" )
			ENDIF
			
		ENDIF
		
		IF cheatWidgetSpawnDuel.bToggle = TRUE
			cheatWidgetSpawnDuel.bToggle = FALSE

			IF IS_LAST_GEN_PLAYER()
				TRIGGER_VEHICLE_CHEAT( DUKES2, "CHEAT_SPAWN_VEH13" )
			ENDIF


		ENDIF
		
		IF cheatWidgetSpawnBubbleSub.bToggle = TRUE
			cheatWidgetSpawnBubbleSub.bToggle = FALSE
			
			IF IS_LAST_GEN_PLAYER()
				TRIGGER_VEHICLE_CHEAT( SUBMERSIBLE2, "CHEAT_SPAWN_VEH14" )
			ENDIF

		ENDIF

//-----------------------------------------------------------------

		// General cheats

		IF cheatWidgetSuperJump.bToggle = TRUE
			cheatWidgetSuperJump.bToggle = FALSE
			TRIGGER_SUPER_JUMP_CHEAT()
			
		ENDIF

		IF cheatWidgetSlideyCars.bToggle = TRUE
			cheatWidgetSlideyCars.bToggle = FALSE
			TRIGGER_SLIDEY_CARS_CHEAT()
		ENDIF

		IF cheatWidgetFastRun.bToggle = TRUE
			cheatWidgetFastRun.bToggle = FALSE
			TRIGGER_FAST_RUN_CHEAT()
		ENDIF

		IF cheatWidgetFastSwim.bToggle = TRUE
			cheatWidgetFastSwim.bToggle = FALSE
			TRIGGER_FAST_SWIM_CHEAT()
		ENDIF

		IF cheatWidgetWeapons.bToggle = TRUE
			cheatWidgetWeapons.bToggle = FALSE
			eCheatStatusWeapons = CHEAT_ACTIVATE
		ENDIF

		IF cheatWidgetAdvanceWeather.bToggle = TRUE
			cheatWidgetAdvanceWeather.bToggle = FALSE
			eCheatStatusAdvanceWeather = CHEAT_ACTIVATE
		ENDIF

		IF cheatWidgetHealthArmor.bToggle = TRUE
			cheatWidgetHealthArmor.bToggle = FALSE
			eCheatStatusHealthArmor = CHEAT_ACTIVATE
		ENDIF

		IF cheatWidgetSpecialAbilityRecharge.bToggle = TRUE
			cheatWidgetSpecialAbilityRecharge.bToggle = FALSE
			eCheatStatusSpecialAbilityRecharge = CHEAT_ACTIVATE
		ENDIF

		IF cheatWidgetWantedLevelUp.bToggle = TRUE
			cheatWidgetWantedLevelUp.bToggle = FALSE
			eCheatStatusWantedLevelUp = CHEAT_ACTIVATE
		ENDIF

		IF cheatWidgetWantedLevelDown.bToggle = TRUE
			cheatWidgetWantedLevelDown.bToggle = FALSE
			eCheatStatusWantedLevelDown = CHEAT_ACTIVATE
		ENDIF

		IF cheatWidgetGiveParachute.bToggle = TRUE
			cheatWidgetGiveParachute.bToggle = FALSE
			eCheatStatusGiveParachute = CHEAT_ACTIVATE
		ENDIF

//-----------------------------------------------------------------

		// Off mission cheats
		IF cheatWidgetBangBang.bToggle = TRUE
			cheatWidgetBangBang.bToggle = FALSE
			TRIGGER_BANG_BANG_CHEAT()
		ENDIF

		IF cheatWidgetFlamingBullets.bToggle = TRUE
			cheatWidgetFlamingBullets.bToggle = FALSE
			TRIGGER_FLAMING_BULLETS_CHEAT()
		ENDIF

		IF cheatWidgetExplosiveMelee.bToggle = TRUE
			cheatWidgetExplosiveMelee.bToggle = FALSE
			TRIGGER_EXPLOSIVE_MELEE_CHEAT()
		ENDIF

		IF cheatWidget0Gravity.bToggle = TRUE
			cheatWidget0Gravity.bToggle = FALSE
			TRIGGER_0GRAVITY_CHEAT()
		ENDIF

		IF cheatWidgetInvincibility.bToggle = TRUE
			cheatWidgetInvincibility.bToggle = FALSE
			TRIGGER_INVINCIBILITY_CHEAT()
		ENDIF

		IF cheatWidgetSlowMo.bToggle = TRUE
			cheatWidgetSlowMo.bToggle = FALSE
			TRIGGER_SLOWMO_CHEAT()
		ENDIF

		IF cheatWidgetSkyfall.bToggle = TRUE
			cheatWidgetSkyfall.bToggle = FALSE
			TRIGGER_SKYFALL_CHEAT()
		ENDIF

		IF cheatWidgetDrunk.bToggle = TRUE
			cheatWidgetDrunk.bToggle = FALSE
			TRIGGER_DRUNK_CHEAT()
		ENDIF

		IF cheatWidgetAimSloMo.bToggle = TRUE
			cheatWidgetAimSloMo.bToggle = FALSE
			TRIGGER_AIM_SLOWMO_CHEAT()
		ENDIF
		
		PROCESS_DISABLE_CHEAT_WIDGETS()
		
	ENDPROC
	
	
	
	
	/// PURPOSE: Constants 
	CONST_FLOAT	DEBUG_TEXT_SCALE_X				0.25
	CONST_FLOAT	DEBUG_TEXT_SCALE_Y				0.3
	CONST_FLOAT	DEBUG_TEXT_WRAP_XPOS_START		0.0
	CONST_FLOAT	DEBUG_TEXT_WRAP_XPOS_END		1.0
	CONST_FLOAT DEBUG_START_XPOS				0.075
	CONST_FLOAT	DEBUG_START_YPOS				0.075
	CONST_FLOAT	DEBUG_ADD_Y						0.02

	CONST_FLOAT DEBUG_NAME_XSPACE				0.11
	CONST_FLOAT DEBUG_STRING_XSPACE				0.065
	CONST_FLOAT DEBUG_INT_XSPACE				0.040
	CONST_FLOAT DEBUG_GAP_XSPACE				0.08
	
	FLOAT fX = DEBUG_START_XPOS
	FLOAT fY = DEBUG_START_YPOS
	
	PROC SETUP_DEBUG_TEXT_DISPLAY(HUD_COLOURS paramColour)
	
      INT theR, theG, theB, theA
      GET_HUD_COLOUR(paramColour, theR, theG, theB, theA)
      SET_TEXT_COLOUR(theR, theG, theB, theA)
      
      SET_TEXT_SCALE(DEBUG_TEXT_SCALE_X, DEBUG_TEXT_SCALE_Y)
      SET_TEXT_WRAP(DEBUG_TEXT_WRAP_XPOS_START, DEBUG_TEXT_WRAP_XPOS_END)
      SET_TEXT_DROPSHADOW(0, 0, 0, 0, 255)

	ENDPROC

	PROC DISPLAY_DEBUG_LITERAL_TEXT(HUD_COLOURS paramColour, FLOAT paramXPos, FLOAT paramYPos, STRING paramText)

	      SETUP_DEBUG_TEXT_DISPLAY(paramColour)
	      DISPLAY_TEXT_WITH_LITERAL_STRING(paramXPos, paramYPos, "STRING", paramText)

	ENDPROC
	
	PROC DISPLAY_DEBUG_BACKGROUND()

		// Calculate the background rectangle height
		FLOAT	textBoxYSize	= DEBUG_ADD_Y * 20
		FLOAT	boxYBorder		= DEBUG_START_YPOS + (DEBUG_ADD_Y * 0.5)
		FLOAT	totalBoxYSize	= textBoxYSize + boxYBorder
		
		FLOAT	boxYCentre		= (totalBoxYSize * 0.5) + 0.06
		
		DRAW_RECT(0.235, boxYCentre, 0.35, totalBoxYSize, 0, 0, 0, 175)
	
	ENDPROC


	PROC DISPLAY_DEBUG_LINE( CHEATS_BITSET_ENUM eCheat, CHEATS_BITSET_ENUM eCheatCategory )
				
		fX = DEBUG_START_XPOS
		DISPLAY_DEBUG_LITERAL_TEXT( HUD_COLOUR_WHITE, fX, fY, GET_CHEAT_NAME_FROM_ENUM(eCheat) )
		
		fX += DEBUG_NAME_XSPACE
		
		IF IS_CHEAT_ACTIVE(eCheat)
			DISPLAY_DEBUG_LITERAL_TEXT( HUD_COLOUR_RED, fX, fY, "ACTIVE" )
		ELSE
			DISPLAY_DEBUG_LITERAL_TEXT( HUD_COLOUR_GREEN, fX, fY, "INACTIVE" )
		ENDIF
		
		fX += DEBUG_STRING_XSPACE
		
		IF IS_CHEAT_DISABLED(eCheat)
		OR IS_CHEAT_DISABLED(eCheatCategory)
		OR IS_CHEAT_DISABLED(CHEAT_TYPE_ALL)
			DISPLAY_DEBUG_LITERAL_TEXT( HUD_COLOUR_RED, fX, fY, "DISABLED" )
		ELSE
			DISPLAY_DEBUG_LITERAL_TEXT( HUD_COLOUR_GREEN, fX, fY, "ENABLED" )
		ENDIF
		
		fX += DEBUG_STRING_XSPACE
		
		IF HAS_CHEAT_BEEN_USED_THIS_SESSION(eCheat)
			DISPLAY_DEBUG_LITERAL_TEXT( HUD_COLOUR_RED, fX, fY, "USED" )
		ELSE
			DISPLAY_DEBUG_LITERAL_TEXT( HUD_COLOUR_GREEN, fX, fY, "NOT USED" )
		ENDIF
		
		fY += DEBUG_ADD_Y
		
	ENDPROC

	
	PROC DISPLAY_DEBUG_INFO()
	
		IF cheatWidgetEnableDebugDisplay.bToggle = FALSE
			EXIT
		ENDIF
		
		DISPLAY_DEBUG_BACKGROUND()
		
		fX = DEBUG_START_XPOS
		fY = DEBUG_START_YPOS
		
		// Title
		DISPLAY_DEBUG_LITERAL_TEXT( HUD_COLOUR_WHITE, fX, fY, "CHEAT" )
		fX += DEBUG_NAME_XSPACE
		DISPLAY_DEBUG_LITERAL_TEXT( HUD_COLOUR_WHITE, fX, fY, "ACTIVE" )
		fX += DEBUG_STRING_XSPACE
		DISPLAY_DEBUG_LITERAL_TEXT( HUD_COLOUR_WHITE, fX, fY, "DISABLED" )
		fX += DEBUG_STRING_XSPACE
		DISPLAY_DEBUG_LITERAL_TEXT( HUD_COLOUR_WHITE, fX, fY, "USED THIS SESSION" )
		
		fY += DEBUG_ADD_Y
		
		//DISPLAY_DEBUG_LINE(CHEAT_TYPE_ALL, CHEAT_TYPE_ALL )
		//DISPLAY_DEBUG_LINE(CHEAT_TYPE_GENERAL, CHEAT_TYPE_GENERAL )
		//DISPLAY_DEBUG_LINE(CHEAT_TYPE_OFF_MISSION, CHEAT_TYPE_OFF_MISSION )
		
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_SPAWN_VEHICLE, CHEAT_TYPE_SPAWN_VEHICLE )
		
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_ADVANCE_WEATHER, CHEAT_TYPE_GENERAL )
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_FAST_RUN, CHEAT_TYPE_GENERAL)			
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_FAST_SWIM, CHEAT_TYPE_GENERAL)
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_SLIDEY_CARS, CHEAT_TYPE_GENERAL)
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_SUPER_JUMP, CHEAT_TYPE_GENERAL)		
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_GIVE_HEALTH_ARMOR, CHEAT_TYPE_GENERAL)
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_GIVE_PARACHUTE, CHEAT_TYPE_GENERAL)		
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_GIVE_WEAPONS, CHEAT_TYPE_GENERAL)		
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_SPECIAL_ABILITY_RECHARGE, CHEAT_TYPE_GENERAL)
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_WANTED_LEVEL_DOWN, CHEAT_TYPE_GENERAL)
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_WANTED_LEVEL_UP, CHEAT_TYPE_GENERAL)
		
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_0_GRAVITY, CHEAT_TYPE_OFF_MISSION)
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_AIM_SLOWMO, CHEAT_TYPE_OFF_MISSION)
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_DRUNK, CHEAT_TYPE_OFF_MISSION)	
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_EXPLOSIVE_MELEE, CHEAT_TYPE_OFF_MISSION)
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_BANG_BANG, CHEAT_TYPE_OFF_MISSION)	
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_FLAMING_BULLETS, CHEAT_TYPE_OFF_MISSION)	
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_INVINCIBILITY, CHEAT_TYPE_OFF_MISSION)	
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_SKYFALL, CHEAT_TYPE_OFF_MISSION)	
		DISPLAY_DEBUG_LINE(CHEAT_TYPE_SLOWMO, CHEAT_TYPE_OFF_MISSION)	
				
	ENDPROC
#ENDIF




//////////////////////////////////////////////////////////////
/// CAR SPAWN CHEATS
///


/// PURPOSE: Pre-loads the cheat vehicle model
///
/// PARAMS:
///    eVehicleModel - Vehicle model name
PROC LOAD_CHEAT_VEHICLE( MODEL_NAMES eVehicleModel )

	IF IS_MODEL_IN_CDIMAGE(eVehicleModel)

		REQUEST_MODEL(eVehicleModel)
		
		IF HAS_MODEL_LOADED(eVehicleModel)
			eCheatStatusSpawnVehicle = CHEAT_ACTIVATE
		ENDIF
	ENDIF

ENDPROC


/// PURPOSE: Creates the cheat vehicle
///
/// PARAMS:
///    eVehicleModel - Vehicle model name
PROC CREATE_CHEAT_VEHICLE( MODEL_NAMES eVehicleModel )

    VECTOR vehiclePosition
    FLOAT vehicleHeading
    BOOL playerWasInCar = FALSE

    IF HAS_MODEL_LOADED( eVehicleModel )

		IF IS_PLAYER_PLAYING(PLAYER_ID())

			IF DOES_ENTITY_EXIST(vCheatVehicle)
	            IF NOT IS_ENTITY_DEAD(vCheatVehicle)
	                IF IS_PED_IN_VEHICLE(PLAYER_PED_ID(), vCheatVehicle)
	                    vehiclePosition = GET_ENTITY_COORDS(vCheatVehicle)
	                    vehicleHeading  = GET_ENTITY_HEADING(vCheatVehicle)
	                    SET_ENTITY_COORDS(PLAYER_PED_ID(), <<vehiclePosition.x, vehiclePosition.y, vehiclePosition.z+4.0>>)
	                    playerWasInCar = TRUE
	                ENDIF
	            ENDIF

	            DELETE_VEHICLE(vCheatVehicle)
	        ENDIF

	        IF NOT playerWasInCar
				
				// Spawn duster slightly off to one side to prevent the player being clipped by the wing.
				IF eVehicleModel = DUSTER
				OR eVehicleModel = STUNT
					vehiclePosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << -1.5, 3.5, 1.0 >>)
				ELIF eVehicleModel = BUZZARD
					vehiclePosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, 6.0, 2.0 >>)
				ELSE
					vehiclePosition = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), << 0.0, 3.5, 1.0 >>)
				ENDIF
	            vehicleHeading  = (GET_ENTITY_HEADING(PLAYER_PED_ID()) + 90.0)
	        ENDIF
			
			IF IS_CHEAT_VEHICLE_SPAWN_POS_SAFE(eVehicleModel, vehiclePosition, vehicleHeading)

				vCheatVehicle = CREATE_VEHICLE(eVehicleModel, vehiclePosition, vehicleHeading, FALSE)
	           	SET_VEHICLE_ON_GROUND_PROPERLY(vCheatVehicle)
				SET_MODEL_AS_NO_LONGER_NEEDED(eVehicleModel)
				SET_VEHICLE_AS_NO_LONGER_NEEDED(vCheatVehicle)

				DISPLAY_CHEAT_ACTIVATION_NOTIFICATION(sCheatVehicleText)
				REGISTER_CHEAT_USED( CHEAT_TYPE_SPAWN_VEHICLE )
				
			ELSE
				SET_MODEL_AS_NO_LONGER_NEEDED(eVehicleModel)
				DISPLAY_CHEAT_DENIED_NOTIFICATION( "CHEAT_VEHICLE_SPAWN_DENIED" )
			ENDIF

			eCheatStatusSpawnVehicle = CHEAT_INACTIVE

	    ENDIF
	ENDIF

ENDPROC

/// PURPOSE: Runs the vehicle spawn cheat.
///
/// PARAMS:
///    eVehicleModel -
PROC PROCESS_VEHICLE_SPAWN_CHEATS( MODEL_NAMES eVehicleModel )
	
	SWITCH eCheatStatusSpawnVehicle

		CASE CHEAT_DISABLED

		BREAK

		CASE CHEAT_INACTIVE

		BREAK

		CASE CHEAT_INITIALISE
		
			// Can't spawn a vehicle in an interior.
			IF IS_INTERIOR_SCENE()
				DISPLAY_CHEAT_DENIED_NOTIFICATION( "CHEAT_VEHICLE_SPAWN_DENIED" )
				eCheatStatusSpawnVehicle = CHEAT_INACTIVE
			ELSE
				LOAD_CHEAT_VEHICLE(eVehicleModel)
			ENDIF

		BREAK

		CASE CHEAT_ACTIVATE
			CREATE_CHEAT_VEHICLE(eVehicleModel)
		BREAK

		DEFAULT

		// Should not get here 
		SCRIPT_ASSERT( "Cheat controller: Reached an invalid case in the vehicle spawn cheat.")
		BREAK

	ENDSWITCH

ENDPROC

//////////////////////////////////////////////////////////////
/// GENERAL CHEATS
///
///

PROC CHEAT_SUPER_JUMP()

	SWITCH eCheatStatusSuperJump
	
	CASE CHEAT_INACTIVE
			
			// Do nothing
		
			BREAK
		
		CASE CHEAT_ACTIVATE
		
			DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_SUPER_JUMP")
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_SUPER_JUMP, TRUE )
			eCheatStatusSuperJump = CHEAT_RUNNING
			BREAK
			
		CASE CHEAT_RUNNING
		
			// Deactivate if the player dies/is arrested/cutscene, etc.
			
			IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_GENERAL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_SUPER_JUMP )
			OR IS_INTERIOR_SCENE()
				eCheatStatusSuperJump = CHEAT_DEACTIVATE
				EXIT
			ENDIF
			
			SET_SUPER_JUMP_THIS_FRAME(PLAYER_ID())
			SET_PED_RESET_FLAG(PLAYER_PED_ID(), PRF_DisableParachuting, TRUE) // Fix for B* 1948122
			
			BREAK
			
		CASE CHEAT_DEACTIVATE
		
			DISPLAY_CHEAT_DEACTIVATION_NOTIFICATION("CHEAT_SUPER_JUMP")
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_SUPER_JUMP, FALSE )
			eCheatStatusSuperJump = CHEAT_INACTIVE
		
		BREAK
			
		DEFAULT
		
		// Should never get here
		SCRIPT_ASSERT("cheat controller: Super jump cheat has entered a default case, not allowed!")
		break
	ENDSWITCH

ENDPROC

//-----------------------------------------------------------------------------

PROC CHEAT_SLIDEY_CARS()

	VEHICLE_INDEX tempVehicle

	SWITCH eCheatStatusSlideyCars
	
		CASE CHEAT_INACTIVE
		
			// Do Nothing
		
		BREAK
		
		CASE CHEAT_ACTIVATE
	
			DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_SLIDEY_CARS")
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_SLIDEY_CARS, TRUE )
			eCheatStatusSlideyCars = CHEAT_RUNNING
						
		BREAK
			
		CASE CHEAT_RUNNING
		
			IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_GENERAL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_SLIDEY_CARS )
				eCheatStatusSlideyCars = CHEAT_DEACTIVATE
			ENDIF

		
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
				tempVehicle = GET_VEHICLE_PED_IS_IN(PLAYER_PED_ID(),FALSE)
				
				IF NOT IS_ENTITY_DEAD(tempVehicle)
				
					IF GET_PED_IN_VEHICLE_SEAT(tempVehicle) = PLAYER_PED_ID()
			
						IF vCheatSlideyVehicle = NULL
					
							vCheatSlideyVehicle = tempVehicle
						
							IF NOT IS_ENTITY_DEAD(vCheatSlideyVehicle)
								IF IS_PLAYERS_VEHICLE_OF_TYPE( PV_CAR )
									SET_VEHICLE_REDUCE_GRIP( vCheatSlideyVehicle, TRUE )
								ELSE
									vCheatSlideyVehicle = NULL
								ENDIF
							ENDIF
							
						ELSE
							//If player changed vehicles, disable slidey cars on 
							IF vCheatSlideyVehicle != tempVehicle
								IF NOT IS_ENTITY_DEAD(vCheatSlideyVehicle) 
									SET_VEHICLE_REDUCE_GRIP( vCheatSlideyVehicle, FALSE )
								ENDIF
								vCheatSlideyVehicle = NULL
							ENDIF
							
						ENDIF
						
					ENDIF
					
				ENDIF
				
			ENDIF
			
		BREAK
			
		CASE CHEAT_DEACTIVATE
			
			DISPLAY_CHEAT_DEACTIVATION_NOTIFICATION("CHEAT_SLIDEY_CARS")
			eCheatStatusSlideyCars = CHEAT_CLEANUP
		BREAK
			
		CASE CHEAT_CLEANUP
		
			IF NOT IS_ENTITY_DEAD(vCheatSlideyVehicle)
				SET_VEHICLE_REDUCE_GRIP( vCheatSlideyVehicle, FALSE )
				vCheatSlideyVehicle = NULL
			ENDIF
			
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_SLIDEY_CARS, FALSE )
			
			eCheatStatusSlideyCars = CHEAT_INACTIVE
		BREAK
		
		DEFAULT // Shouldn't get here

			SCRIPT_ASSERT("cheat controller: Cheat slidey cars has entered a default case, not allowed!")
		break
	ENDSWITCH
	

ENDPROC

//-----------------------------------------------------------------------------

PROC CHEAT_FAST_RUN()

	
	SWITCH eCheatStatusFastRun
	
		CASE CHEAT_INACTIVE
		
			// Do Nothing
		
		BREAK
		
		CASE CHEAT_ACTIVATE
	
			DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_FAST_RUN")	
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_FAST_RUN, TRUE )
			eCheatStatusFastRun = CHEAT_RUNNING
			
		BREAK
			
		CASE CHEAT_RUNNING
		
			IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
			OR IS_PED_INJURED(PLAYER_PED_ID())
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_GENERAL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_FAST_RUN )
				eCheatStatusFastRun = CHEAT_DEACTIVATE
			ELSE
				
				IF IS_PED_RUNNING(PLAYER_PED_ID())
				OR IS_PED_SPRINTING(PLAYER_PED_ID())
				AND NOT IS_PED_GETTING_INTO_A_VEHICLE(PLAYER_PED_ID())
					SET_RUN_SPRINT_MULTIPLIER_FOR_PLAYER(PLAYER_ID(), 1.49)
				ELSE
					SET_RUN_SPRINT_MULTIPLIER_FOR_PLAYER(PLAYER_ID(), 1.0)
				ENDIF
				
				STAT_SET_CHEAT_IS_ACTIVE()
			
			ENDIF
		
		BREAK
			
		CASE CHEAT_DEACTIVATE
			SET_RUN_SPRINT_MULTIPLIER_FOR_PLAYER(PLAYER_ID(), 1.0)
			DISPLAY_CHEAT_DEACTIVATION_NOTIFICATION("CHEAT_FAST_RUN")
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_FAST_RUN, FALSE )
			eCheatStatusFastRun = CHEAT_INACTIVE
		BREAK
		
		DEFAULT // Shouldn't get here

			SCRIPT_ASSERT("cheat controller: Cheat fast run has entered a default case, not allowed!")
		BREAK
	ENDSWITCH
	
	

ENDPROC

//-----------------------------------------------------------------------------

PROC CHEAT_FAST_SWIM()
	
		SWITCH eCheatStatusFastSwim
	
		CASE CHEAT_INACTIVE
		
			// Do Nothing
		
			BREAK
		
		CASE CHEAT_ACTIVATE
	
			DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_FAST_SWIM")
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_FAST_SWIM, TRUE )
			eCheatStatusFastSwim = CHEAT_RUNNING
			
		BREAK
			
		CASE CHEAT_RUNNING
		
			IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_GENERAL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_FAST_SWIM )
				eCheatStatusFastSwim = CHEAT_DEACTIVATE
			ELSE

				SET_SWIM_MULTIPLIER_FOR_PLAYER(PLAYER_ID(),1.49) 
				STAT_SET_CHEAT_IS_ACTIVE()
				
			ENDIF
			
		BREAK
	
		CASE CHEAT_DEACTIVATE
		
			SET_SWIM_MULTIPLIER_FOR_PLAYER(PLAYER_ID(),1.0) 
			DISPLAY_CHEAT_DEACTIVATION_NOTIFICATION("CHEAT_FAST_SWIM")
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_FAST_SWIM, FALSE )
			eCheatStatusFastSwim = CHEAT_INACTIVE
		BREAK
		
		DEFAULT // Shouldn't get here

			SCRIPT_ASSERT("cheat controller: Cheat fast swim has entered a default case, not allowed!")
		BREAK
	ENDSWITCH
	

ENDPROC

//-----------------------------------------------------------------------------

PROC CHEAT_GIVE_WEAPONS()
	
	IF eCheatStatusWeapons = CHEAT_ACTIVATE
	
		eCheatStatusWeapons = CHEAT_INACTIVE
	
		IF IS_CHEAT_DISABLED(CHEAT_TYPE_ALL)
		OR IS_CHEAT_DISABLED(CHEAT_TYPE_GENERAL)
		OR IS_CHEAT_DISABLED(CHEAT_TYPE_GIVE_WEAPONS)
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
			EXIT
		ENDIF
		
		IF IS_PHONE_FORBIDDEN()
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
			EXIT
		ENDIF
	
		DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_GIVE_WEAPONS")
		REGISTER_CHEAT_USED( CHEAT_TYPE_GIVE_WEAPONS )
	
		
		IF IS_PLAYER_PLAYING( PLAYER_ID())
			GIVE_DELAYED_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_KNIFE, INFINITE_AMMO ,FALSE )
			GIVE_DELAYED_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SMG, 300, FALSE )
			GIVE_DELAYED_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_ASSAULTRIFLE, 300, TRUE )
			GIVE_DELAYED_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_PUMPSHOTGUN, 150, FALSE )
			GIVE_DELAYED_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_SNIPERRIFLE, 30, FALSE )
			GIVE_DELAYED_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_GRENADE, 5, FALSE )
			GIVE_DELAYED_WEAPON_TO_PED(PLAYER_PED_ID(), WEAPONTYPE_RPG, 5, FALSE )
		ENDIF
	ENDIF

ENDPROC

//-----------------------------------------------------------------------------

PROC CHEAT_ADVANCE_WEATHER()

	IF eCheatStatusAdvanceWeather = CHEAT_ACTIVATE
	
		eCheatStatusAdvanceWeather = CHEAT_INACTIVE
		
		IF IS_CHEAT_DISABLED(CHEAT_TYPE_ALL)
		OR IS_CHEAT_DISABLED(CHEAT_TYPE_GENERAL)
		OR IS_CHEAT_DISABLED(CHEAT_TYPE_ADVANCE_WEATHER)
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
			EXIT
		ENDIF
		
		IF IS_PHONE_FORBIDDEN()
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
			EXIT
		ENDIF
		
		REGISTER_CHEAT_USED( CHEAT_TYPE_ADVANCE_WEATHER )
										
		SWITCH eCheatWeather
		
			CASE 	CHEAT_WEATHER_OFF
					
					eCheatWeather = CHEAT_WEATHER_EXTRASUNNY
			
					FALLTHRU
								
			CASE	CHEAT_WEATHER_EXTRASUNNY
					SET_WEATHER_TYPE_NOW_PERSIST("EXTRASUNNY")
					CLEAR_WEATHER_TYPE_PERSIST()
					DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_ADVANCE_WEATHER_EXTRA_SUNNY")
					eCheatWeather = CHEAT_WEATHER_CLEAR
					BREAK
					
			CASE	CHEAT_WEATHER_CLEAR
					SET_WEATHER_TYPE_NOW_PERSIST("CLEAR")
					CLEAR_WEATHER_TYPE_PERSIST()
					DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_ADVANCE_WEATHER_CLEAR")
					eCheatWeather = CHEAT_WEATHER_CLOUDS
					BREAK
				
			CASE	CHEAT_WEATHER_CLOUDS
					SET_WEATHER_TYPE_NOW_PERSIST("CLOUDS")
					CLEAR_WEATHER_TYPE_PERSIST()
					DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_ADVANCE_WEATHER_CLOUDY")
					eCheatWeather = CHEAT_WEATHER_SMOG
					BREAK
					
			CASE	CHEAT_WEATHER_SMOG
					SET_WEATHER_TYPE_NOW_PERSIST("SMOG")
					CLEAR_WEATHER_TYPE_PERSIST()
					DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_ADVANCE_WEATHER_SMOGGY")
					//eCheatWeather = CHEAT_WEATHER_CLOUDY
					eCheatWeather = CHEAT_WEATHER_OVERCAST
					BREAK
/*					
	Commented out because it's asserting. Will check later.
			CASE	CHEAT_WEATHER_CLOUDY
					SET_WEATHER_TYPE_NOW_PERSIST("CLOUDY")
					CLEAR_WEATHER_TYPE_PERSIST()
					DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_ADVANCE_WEATHER_CLOUDY")
					eCheatWeather = CHEAT_WEATHER_OVERCAST
					BREAK
*/					
			CASE	CHEAT_WEATHER_OVERCAST
					SET_WEATHER_TYPE_NOW_PERSIST("OVERCAST")
					CLEAR_WEATHER_TYPE_PERSIST()
					DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_ADVANCE_WEATHER_OVERCAST")
					eCheatWeather = CHEAT_WEATHER_RAIN
					BREAK
					
			CASE	CHEAT_WEATHER_RAIN
					SET_WEATHER_TYPE_NOW_PERSIST("RAIN")
					CLEAR_WEATHER_TYPE_PERSIST()
					DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_ADVANCE_WEATHER_RAIN")
					REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_ADVANCE_WEATHER, TRUE )
					eCheatWeather = CHEAT_WEATHER_THUNDER
					BREAK
					
			CASE	CHEAT_WEATHER_THUNDER
					SET_WEATHER_TYPE_NOW_PERSIST("THUNDER")
					CLEAR_WEATHER_TYPE_PERSIST()
					DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_ADVANCE_WEATHER_THUNDER")
					eCheatWeather = CHEAT_WEATHER_CLEARING
					BREAK
					
			CASE	CHEAT_WEATHER_CLEARING
					SET_WEATHER_TYPE_NOW_PERSIST("CLEARING")
					CLEAR_WEATHER_TYPE_PERSIST()
					DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_ADVANCE_WEATHER_CLEARING")
					eCheatWeather = CHEAT_WEATHER_SNOW
				BREAK
							
			CASE	CHEAT_WEATHER_SNOW
					SET_WEATHER_TYPE_NOW_PERSIST("XMAS")
					CLEAR_WEATHER_TYPE_PERSIST()
					DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_ADVANCE_WEATHER_SNOW")
					eCheatWeather = CHEAT_WEATHER_OFF
				BREAK

			DEFAULT
			
					// Should not get here
					SCRIPT_ASSERT( "cheat controller: unknown weather type in weather cheat")
			BREAK		
		ENDSWITCH
		

	ENDIF

ENDPROC
//-----------------------------------------------------------------------------


PROC CHEAT_GIVE_HEALTH_ARMOR()

	IF eCheatStatusHealthArmor = CHEAT_ACTIVATE
	
		eCheatStatusHealthArmor = CHEAT_INACTIVE
		
		IF IS_CHEAT_DISABLED(CHEAT_TYPE_ALL)
		OR IS_CHEAT_DISABLED(CHEAT_TYPE_GENERAL)
		OR IS_CHEAT_DISABLED(CHEAT_TYPE_GIVE_HEALTH_ARMOR)
		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME)
		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME_FRIENDS)
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
			EXIT
		ENDIF
		
		IF IS_PHONE_FORBIDDEN()
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
			EXIT
		ENDIF
	
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_HEALTH_ARMOR")
			REGISTER_CHEAT_USED( CHEAT_TYPE_GIVE_HEALTH_ARMOR )
			SET_ENTITY_HEALTH(PLAYER_PED_ID(), GET_ENTITY_MAX_HEALTH(PLAYER_PED_ID()))
			ADD_ARMOUR_TO_PED(PLAYER_PED_ID(), (GET_PLAYER_MAX_ARMOUR(PLAYER_ID())-GET_PED_ARMOUR(PLAYER_PED_ID())))
			
			// Repair vehicle if necessary.
			IF IS_PED_IN_ANY_VEHICLE(PLAYER_PED_ID())
			
				VEHICLE_INDEX playerVehicle = GET_VEHICLE_PED_IS_USING(PLAYER_PED_ID())
				
				IF DOES_ENTITY_EXIST(playerVehicle)
					IF NOT IS_ENTITY_DEAD(playerVehicle)
						SET_VEHICLE_FIXED(playerVehicle)
					ENDIF
				ENDIF		
			
			ENDIF
		ENDIF
		
	ENDIF

ENDPROC
//-----------------------------------------------------------------------------

PROC CHEAT_SPECIAL_ABILITY_RECHARGE()

	IF eCheatStatusSpecialAbilityRecharge = CHEAT_ACTIVATE
		
		eCheatStatusSpecialAbilityRecharge = CHEAT_INACTIVE
		
		IF IS_CHEAT_DISABLED(CHEAT_TYPE_ALL)
		OR IS_CHEAT_DISABLED(CHEAT_TYPE_GENERAL)
		OR IS_CHEAT_DISABLED(CHEAT_TYPE_SPECIAL_ABILITY_RECHARGE)
		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME)
		OR IS_CURRENTLY_ON_MISSION_OF_TYPE(MISSION_TYPE_MINIGAME_FRIENDS)
		OR NOT IS_SPECIAL_ABILITY_UNLOCKED(GET_PLAYER_MODEL())
		OR NOT IS_SPECIAL_ABILITY_ENABLED(PLAYER_ID())
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
			EXIT
		ENDIF
		
		IF IS_PHONE_FORBIDDEN()
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
			EXIT
		ENDIF
		
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			SPECIAL_ABILITY_FILL_METER(PLAYER_ID(), TRUE)
			DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_SPECIAL_ABILITY")
			REGISTER_CHEAT_USED( CHEAT_TYPE_SPECIAL_ABILITY_RECHARGE )
		ENDIF
	ENDIF

ENDPROC
//-----------------------------------------------------------------------------

PROC CHEAT_WANTED_LEVEL_UP()

	INT iWantedLevel
	
	IF eCheatStatusWantedLevelUp = CHEAT_ACTIVATE
		
		eCheatStatusWantedLevelUp = CHEAT_INACTIVE
		
		IF IS_CHEAT_DISABLED(CHEAT_TYPE_ALL)
		OR IS_CHEAT_DISABLED(CHEAT_TYPE_GENERAL)
		OR IS_CHEAT_DISABLED(CHEAT_TYPE_WANTED_LEVEL_UP)
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
			EXIT
		ENDIF
		
		IF IS_PHONE_FORBIDDEN()
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
			EXIT
		ENDIF
		
		IF IS_PLAYER_PLAYING(PLAYER_ID())
		
			// If the max wanted level is 0, then they're probably faking the wanted level, so we can't activate the cheat.	
			IF GET_MAX_WANTED_LEVEL() = 0
				DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
				EXIT
			ENDIF
			
			iWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
				
			IF iWantedLevel < GET_MAX_WANTED_LEVEL()
							
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), iWantedLevel + 1 )
				SET_PLAYER_WANTED_LEVEL_NOW(PLAYER_ID())
				
				DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_WANTED_UP")
				REGISTER_CHEAT_USED( CHEAT_TYPE_WANTED_LEVEL_UP )
				
			ELSE
				DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_WANTED_UP_DENIED")
			ENDIF
			
		ENDIF
		
	ENDIF

ENDPROC
//-----------------------------------------------------------------------------

PROC CHEAT_WANTED_LEVEL_DOWN()

	INT iWantedLevel
	
	IF eCheatStatusWantedLevelDown = CHEAT_ACTIVATE
		eCheatStatusWantedLevelDown = CHEAT_INACTIVE
		
		IF IS_CHEAT_DISABLED(CHEAT_TYPE_ALL)
		OR IS_CHEAT_DISABLED(CHEAT_TYPE_GENERAL)
		OR IS_CHEAT_DISABLED(CHEAT_TYPE_WANTED_LEVEL_DOWN)
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
			EXIT
		ENDIF
		
		IF IS_PHONE_FORBIDDEN()
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
			EXIT
		ENDIF
		
		IF IS_PLAYER_PLAYING(PLAYER_ID()) 
		
			// If the max wanted level is 0, then they're probably faking the wanted level, so we can't activate the cheat.	
			IF GET_MAX_WANTED_LEVEL() = 0
				DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
				EXIT
			ENDIF

			iWantedLevel = GET_PLAYER_WANTED_LEVEL(PLAYER_ID())
			
			IF iWantedLevel > 0
				DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_WANTED_DOWN")
				SET_PLAYER_WANTED_LEVEL(PLAYER_ID(), iWantedLevel - 1)
				REGISTER_CHEAT_USED( CHEAT_TYPE_WANTED_LEVEL_DOWN )
			ELSE
				DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_WANTED_DOWN_DENIED")
			ENDIF
			
		ENDIF
	ENDIF

ENDPROC
//-----------------------------------------------------------------------------

PROC CHEAT_GIVE_PARACHUTE()

	IF eCheatStatusGiveParachute = CHEAT_ACTIVATE
	
		eCheatStatusGiveParachute = CHEAT_INACTIVE
		
		IF IS_CHEAT_DISABLED(CHEAT_TYPE_ALL)
		OR IS_CHEAT_DISABLED(CHEAT_TYPE_GENERAL)
		OR IS_CHEAT_DISABLED(CHEAT_TYPE_GIVE_PARACHUTE)
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_NOT_NOW")
			EXIT
		ENDIF
		
		IF IS_PHONE_FORBIDDEN()
			DISPLAY_CHEAT_DENIED_NOTIFICATION("CHEAT_PHONE_DENIED")
			EXIT
		ENDIF
		
		IF IS_PLAYER_PLAYING(PLAYER_ID())
			DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_GIVE_PARACHUTE")
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1,TRUE,TRUE)
			REGISTER_CHEAT_USED( CHEAT_TYPE_GIVE_PARACHUTE )
		ENDIF
	
	ENDIF

ENDPROC

//-----------------------------------------------------------------------------
/// PURPOSE: Processes all the general cheats
///    
PROC PROCESS_GENERAL_CHEATS()

	CHEAT_SUPER_JUMP()
	CHEAT_SLIDEY_CARS()
	CHEAT_FAST_RUN()
	CHEAT_FAST_SWIM()
	CHEAT_GIVE_WEAPONS()
	CHEAT_ADVANCE_WEATHER()
	CHEAT_GIVE_HEALTH_ARMOR()
	CHEAT_SPECIAL_ABILITY_RECHARGE()
	CHEAT_WANTED_LEVEL_UP()
	CHEAT_WANTED_LEVEL_DOWN()
	CHEAT_GIVE_PARACHUTE()

ENDPROC

//////////////////////////////////////////////////////////////////
///  OFF MISSION CHEATS
///
///
PROC CHEAT_BANG_BANG()

	SWITCH eCheatStatusBangBang
	
	CASE CHEAT_INACTIVE
			
			// Do nothing
		
			BREAK
		
		CASE CHEAT_ACTIVATE
		
				DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_BANG_BANG")
				REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_BANG_BANG, TRUE )
				eCheatStatusBangBang = CHEAT_RUNNING
			BREAK
			
		CASE CHEAT_RUNNING
		
			// Deactivate if a mission activates, or the player dies/is arrested/cutscene, etc.
			
			IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_BANG_BANG )
				eCheatStatusBangBang = CHEAT_DEACTIVATE
				EXIT
			ENDIF
			
			IF IS_ANY_MISSION_ACTIVE()
				eCheatStatusBangBang = CHEAT_DEACTIVATE
				EXIT
			ENDIF
			
			SET_EXPLOSIVE_AMMO_THIS_FRAME(PLAYER_ID())

			BREAK
			
		CASE CHEAT_DEACTIVATE
		
			DISPLAY_CHEAT_DEACTIVATION_NOTIFICATION("CHEAT_BANG_BANG")
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_BANG_BANG, FALSE )
			eCheatStatusBangBang = CHEAT_INACTIVE
		
		BREAK
			
		DEFAULT
		
		// Should never get here
		SCRIPT_ASSERT("cheat controller: Bang Bang cheat has entered a default case, not allowed!")
		BREAK
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Does the flaming bullets cheat
PROC CHEAT_FLAMING_BULLETS()
	
	SWITCH eCheatStatusFlamingBullets
	
	CASE CHEAT_INACTIVE
			
			// Do nothing
		
			BREAK
		
		CASE CHEAT_ACTIVATE
		
				DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_FLAMING_BULLETS")
				REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_FLAMING_BULLETS, TRUE )
				eCheatStatusFlamingBullets = CHEAT_RUNNING
			BREAK
			
		CASE CHEAT_RUNNING
		
			// Deactivate if a mission activates, or the player dies/is arrested/cutscene, etc.
			
			IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_FLAMING_BULLETS )
				eCheatStatusFlamingBullets = CHEAT_DEACTIVATE
				EXIT
			ENDIF
			
			IF IS_ANY_MISSION_ACTIVE()
				eCheatStatusFlamingBullets = CHEAT_DEACTIVATE
				EXIT
			ENDIF
			
			SET_FIRE_AMMO_THIS_FRAME(PLAYER_ID())
				
			BREAK
			
		CASE CHEAT_DEACTIVATE
		
			DISPLAY_CHEAT_DEACTIVATION_NOTIFICATION("CHEAT_FLAMING_BULLETS")
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_FLAMING_BULLETS, FALSE )
			eCheatStatusFlamingBullets = CHEAT_INACTIVE
		
		BREAK
			
		DEFAULT
		
		// Should never get here
		SCRIPT_ASSERT("cheat controller: Explosive melee cheat has entered a default case, not allowed!")
		
		BREAK
	ENDSWITCH

ENDPROC


/// PURPOSE:
///    Does the explosive melee cheat
PROC CHEAT_EXPLOSIVE_MELEE()

	SWITCH eCheatStatusExplosiveMelee
	
	CASE CHEAT_INACTIVE
			
			// Do nothing
		
			BREAK
		
		CASE CHEAT_ACTIVATE
		
				DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_EXPLOSIVE_MELEE")
				REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_EXPLOSIVE_MELEE, TRUE )
				eCheatStatusExplosiveMelee = CHEAT_RUNNING
			BREAK
			
		CASE CHEAT_RUNNING
		
			// Deactivate if a mission activates, or the player dies/is arrested/cutscene, etc.
			
			IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_EXPLOSIVE_MELEE )
			OR IS_ANY_MISSION_ACTIVE()
				eCheatStatusExplosiveMelee = CHEAT_DEACTIVATE
				EXIT
			ENDIF
			
			SET_EXPLOSIVE_MELEE_THIS_FRAME(PLAYER_ID())

			BREAK
			
		CASE CHEAT_DEACTIVATE
		
			DISPLAY_CHEAT_DEACTIVATION_NOTIFICATION("CHEAT_EXPLOSIVE_MELEE")
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_EXPLOSIVE_MELEE, FALSE )
			eCheatStatusExplosiveMelee = CHEAT_INACTIVE
		
		BREAK
			
		DEFAULT
		
		// Should never get here
		SCRIPT_ASSERT("cheat controller: Explosive melee cheat has entered a default case, not allowed!")
		BREAK
	ENDSWITCH

ENDPROC

/// PURPOSE:
///    Activates the 0gravity cheat
PROC ACTIVATE_0GRAVITY()
	
	++iGravityLevel
	
	IF iGravityLevel > 1 // Originally 3 - Only moon gravity allowed now due to B* 1354692
		
		eCheatStatus0Gravity = CHEAT_DEACTIVATE
		
		EXIT
	
	ENDIF
	
	SWITCH iGravityLevel
	
		CASE 1
		
			DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_GRAVITY_MOON")
			SET_GRAVITY_LEVEL(GRAV_MOON)
	
		BREAK

/*
	CASE 2
	
		DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_GRAVITY_MICRO")
		SET_GRAVITY_LEVEL(GRAV_LOW)
		
		BREAK
	
	CASE 3
	
		DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_GRAVITY_ZERO")
		SET_GRAVITY_LEVEL(GRAV_ZERO)
		
		BREAK
*/	
	
		DEFAULT
			SCRIPT_ASSERT("cheat controller: Gravity cheat has out of bounds iGravityLevel value")
		BREAK	
	ENDSWITCH
	
	eCheatStatus0Gravity = CHEAT_RUNNING
		
ENDPROC

/// PURPOSE:
///    Does the 0 gravity cheat - 3 levels.
PROC CHEAT_0GRAVITY()

	SWITCH eCheatStatus0Gravity
	
		CASE CHEAT_INACTIVE
				
			// Do nothing
			
		BREAK
		
		CASE CHEAT_ACTIVATE
		
			REGISTER_CHEAT_USED( CHEAT_TYPE_0_GRAVITY )
			REGISTER_CHEAT_ACTIVE(CHEAT_TYPE_0_GRAVITY,TRUE)
			
			ACTIVATE_0GRAVITY()
				
		BREAK
	
		CASE CHEAT_RUNNING
			if not  IS_PED_INJURED(PLAYER_PED_ID())
				IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
				OR IS_ANY_MISSION_ACTIVE()
				OR IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
				OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
				OR IS_CHEAT_DISABLED( CHEAT_TYPE_0_GRAVITY )
				OR IS_CHEAT_ACTIVE(CHEAT_TYPE_SKYFALL)
				OR IS_ENTITY_IN_WATER(PLAYER_PED_ID())
					eCheatStatus0Gravity = CHEAT_DEACTIVATE
				ENDIF
			else
				eCheatStatus0Gravity = CHEAT_DEACTIVATE
			endif
		BREAK
		
		CASE CHEAT_DEACTIVATE
		
			//DISPLAY_CHEAT_DEACTIVATION_NOTIFICATION("CHEAT_GRAVITY_NORM")
			DISPLAY_CHEAT_DEACTIVATION_NOTIFICATION("CHEAT_GRAVITY_MOON")
			REGISTER_CHEAT_ACTIVE(CHEAT_TYPE_0_GRAVITY,FALSE)
			SET_GRAVITY_LEVEL(GRAV_EARTH)
			iGravityLevel = 0
			eCheatStatus0Gravity = CHEAT_INACTIVE
		
		BREAK
		
		DEFAULT
			// Should never get here
			SCRIPT_ASSERT("cheat controller: Gravity cheat has entered a default case, not allowed!")
		break	
	ENDSWITCH
	

ENDPROC


/// PURPOSE:
///    Does the invincibility cheat
PROC CHEAT_INVINCIBILITY()

	VECTOR vPlayerPos

	SWITCH eCheatStatusInvincibility
	
		CASE CHEAT_INACTIVE
			
			// Do nothing
		
		BREAK
		
		CASE CHEAT_ACTIVATE
		

				DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_INVINCIBILITY")
				REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_INVINCIBILITY, TRUE )
				eCheatStatusInvincibility = CHEAT_RUNNING
				iInvincibilityStartTime = GET_GAME_TIMER()
				
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
				SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
			
			BREAK
			
		CASE CHEAT_RUNNING
		
			// Deactivate if a mission activates, or the player dies/is arrested/cutscene, etc.
			
			IF NOT IS_PLAYER_PLAYING(PLAYER_ID()) OR IS_PED_INJURED(PLAYER_PED_ID())
					CPRINTLN(DEBUG_AMBIENT, "CHEAT_INVINCIBILITY: CHEAT_RUNNING disabling cheat")
					eCheatStatusInvincibility = CHEAT_DEACTIVATE
				EXIT
			ENDIF
			
			vPlayerPos = GET_PLAYER_COORDS(PLAYER_ID())
			
			IF IS_ANY_MISSION_ACTIVE()
			OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_INVINCIBILITY )
			OR vPlayerPos.z <= -170.0	// For submersible crush depth.
				eCheatStatusInvincibility = CHEAT_DEACTIVATE
				EXIT
			ENDIF
			
			iInvincibilityTimer = GET_GAME_TIMER() - iInvincibilityStartTime
			
			IF GET_NUMBER_OF_THREADS_RUNNING_THE_SCRIPT_WITH_THIS_HASH(HASH("appInternet")) = 0
				DRAW_GENERIC_TIMER(	iInvincibilityTimeLimit - iInvincibilityTimer  , "CHEAT_INV", 0, TIMER_STYLE_DONTUSEMILLISECONDS, 1000, PODIUMPOS_NONE, HUDORDER_DONTCARE, FALSE, HUD_COLOUR_RED) 
			ENDIF
			
			IF  iInvincibilityTimer >= (iInvincibilityTimeLimit - 1000)
				eCheatStatusInvincibility = CHEAT_DEACTIVATE
				EXIT
			ENDIF
			
			SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
			
			STAT_SET_CHEAT_IS_ACTIVE()
			
			BREAK
			
		CASE CHEAT_DEACTIVATE
		
			DISPLAY_CHEAT_DEACTIVATION_NOTIFICATION("CHEAT_INVINCIBILITY_OFF")
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_INVINCIBILITY, FALSE )
			eCheatStatusInvincibility = CHEAT_INACTIVE
			
			IF NOT IS_PED_INJURED(PLAYER_PED_ID())
				SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
				SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
			ENDIF
		
		BREAK
			
		DEFAULT
		
			// Should never get here
			SCRIPT_ASSERT("cheat controller: Invincibility cheat has entered a default case, not allowed!")
		BREAK
	ENDSWITCH
	

ENDPROC

/// PURPOSE:
///    Activates the slowmo cheat
PROC ACTIVATE_SLOWMO()

	// This cheat cycles through three levels.
	++iSlowMoLevel
	
	IF iSlowMoLevel > 3
		
		eCheatStatusSlowMo = CHEAT_DEACTIVATE
		
		EXIT
	
	ENDIF
	
	SWITCH iSlowMoLevel
	
		CASE 1
		
			DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_SLOW_MO1")
			SET_TIME_SCALE( 0.6 )
	
		BREAK
		
		CASE 2
		
			DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_SLOW_MO2")
			SET_TIME_SCALE( 0.4 )
			
		BREAK
	
		CASE 3
		
			DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_SLOW_MO3")
			SET_TIME_SCALE( 0.2 )
			
		BREAK
	
	
		DEFAULT
			SCRIPT_ASSERT("cheat controller: Slowmo cheat has out of bounds iSlowMoLevel value")
		BREAK
	
	ENDSWITCH
	
	eCheatStatusSlowMo = CHEAT_RUNNING
		
ENDPROC


/// PURPOSE:
///    Runs the slowmo cheat
PROC CHEAT_SLOWMO()

	SWITCH eCheatStatusSlowMo
	
		CASE CHEAT_INACTIVE
				
			// Do nothing
			
		BREAK
		
		CASE CHEAT_ACTIVATE
			
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_SLOWMO, TRUE )
			ACTIVATE_SLOWMO()
				
		BREAK
	
		CASE CHEAT_RUNNING
		
			// Suppress actual inputs for weapon wheel and character wheel
			// as they play with the time step which causes issues with the cheat
			// Instead we'll check the disabled controls and quit the cheats
			// when the buttons are pressed, so we can deactivate the cheats safely.
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SELECT_WEAPON )
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CHARACTER_WHEEL )
		
			IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
			OR IS_ANY_MISSION_ACTIVE()
			OR IS_PLAYER_PED_SWITCH_IN_PROGRESS()
			OR IS_SELECTOR_ONSCREEN()
			OR IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_SELECT_WEAPON )
			OR IS_DISABLED_CONTROL_JUST_PRESSED( FRONTEND_CONTROL, INPUT_CHARACTER_WHEEL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_SLOWMO )
				eCheatStatusSlowMo = CHEAT_DEACTIVATE
			ENDIF
		
		BREAK
		
		CASE CHEAT_DEACTIVATE
		
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_SELECT_WEAPON )
			DISABLE_CONTROL_ACTION(FRONTEND_CONTROL, INPUT_CHARACTER_WHEEL )
			
			DISPLAY_CHEAT_DEACTIVATION_NOTIFICATION("CHEAT_SLOW_MO")
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_SLOWMO, FALSE )
			
			iSlowMoLevel = 0
			SET_TIME_SCALE(1.0)
			
			eCheatStatusSlowMo = CHEAT_INACTIVE
		
		BREAK
		
		DEFAULT
			// Should never get here
			SCRIPT_ASSERT("cheat controller: Slowmo cheat has entered a default case, not allowed!")
		BREAK	
	ENDSWITCH
	
	
ENDPROC


/// PURPOSE:Cleans up the skyfall cheat
///    
PROC CLEANUP_SKYFALL_CHEAT()

		IF NOT IS_PED_INJURED(PLAYER_PED_ID())
			REMOVE_WEAPON_FROM_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE)
		ENDIF
			
		// Don't turn off invincibility if the invincible cheat is running
		IF eCheatStatusInvincibility != CHEAT_RUNNING
			SET_PLAYER_INVINCIBLE(PLAYER_ID(), FALSE)
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
		ENDIF
		
		SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_NONE)

ENDPROC


/// PURPOSE: Runs the skyfall cheat
///    
PROC CHEAT_SKYFALL()

	VECTOR vPos
	FLOAT	fRot
	FLOAT	fGroundHeight
	
	SWITCH eCheatStatusSkyfall
			
		CASE CHEAT_INACTIVE
		
			BREAK
		
		CASE CHEAT_INITIALISE
		
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_SKYFALL, TRUE )

			FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), TRUE)
			SET_PLAYER_INVINCIBLE(PLAYER_ID(), TRUE)
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), TRUE, TRUE, TRUE, TRUE, TRUE)
			GIVE_WEAPON_TO_PED(PLAYER_PED_ID(), GADGETTYPE_PARACHUTE, 1)
			
			vPos = GET_ENTITY_COORDS(PLAYER_PED_ID())
			fRot = GET_ENTITY_HEADING(PLAYER_PED_ID())
			GET_GROUND_Z_FOR_3D_COORD(vPos, fGroundHeight )
			
			fGroundHeight = fGroundHeight + 500
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << vPos.x, vPos.y, fGroundHeight >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), fRot)
			
			iSkyfallTimer = GET_GAME_TIMER()
			
			DO_SCREEN_FADE_OUT(0)
			
			eCheatStatusSkyfall = CHEAT_INITIALSE_PT2
			
			BREAK
			
		CASE CHEAT_INITIALSE_PT2
		
			IF HAS_TIME_PASSED(1000, iSkyfallTimer)
				FREEZE_ENTITY_POSITION(PLAYER_PED_ID(), FALSE)
				SET_PLAYER_CONTROL(PLAYER_ID(), TRUE)
				iSkyfallTimer = GET_GAME_TIMER()
				eCheatStatusSkyfall = CHEAT_ACTIVATE
			ENDIF
			
			BREAK
		
		CASE CHEAT_ACTIVATE
		
			IF HAS_TIME_PASSED(1000, iSkyfallTimer)
			
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
				DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_PITCH_UD)
				
				// Disable parachute. For some reason it's in a different slot for Michael.
				IF GET_CURRENT_PLAYER_PED_ENUM() = CHAR_MICHAEL
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL2, 0, 0, 0)
				ELSE
					SET_PED_COMPONENT_VARIATION(PLAYER_PED_ID(), PED_COMP_SPECIAL, 0, 0, 0)
				ENDIF
			
				DO_SCREEN_FADE_IN(DEFAULT_FADE_TIME_SHORT)
				iSkyfallTimer = GET_GAME_TIMER()
				
				DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_SKYFALL")
				
				SET_INSTANCE_PRIORITY_HINT(INSTANCE_HINT_DRIVING)
				
				eCheatStatusSkyfall = CHEAT_RUNNING
			ENDIF
		
			BREAK
				
				
		CASE CHEAT_RUNNING
		
			IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
			OR IS_ANY_MISSION_ACTIVE()
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_SKYFALL )
				eCheatStatusSkyfall = CHEAT_DEACTIVATE
				BREAK
			ENDIF
		
			DISABLE_CONTROL_ACTION(PLAYER_CONTROL, INPUT_PARACHUTE_DEPLOY)
			
			HIDE_HUD_COMPONENT_THIS_FRAME(NEW_HUD_WEAPON_ICON)
		
			IF IS_PED_IN_PARACHUTE_FREE_FALL(PLAYER_PED_ID())
			
				APPLY_FORCE_TO_ENTITY(PLAYER_PED_ID(), APPLY_TYPE_IMPULSE, << 0.0, 200.0, 2.5 >>, << 0.0, 0.0, 0.0 >>, 0, TRUE, FALSE, FALSE)
					
				IF HAS_ENTITY_COLLIDED_WITH_ANYTHING(PLAYER_PED_ID())
					eCheatStatusSkyfall = CHEAT_DEACTIVATE
				ENDIF
				
			ELSE
				eCheatStatusSkyfall = CHEAT_DEACTIVATE
			ENDIF
		
			BREAK
		
		CASE CHEAT_DEACTIVATE
		
			CLEANUP_SKYFALL_CHEAT()
			
			DISPLAY_CHEAT_DEACTIVATION_NOTIFICATION("CHEAT_SKYFALL")
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_SKYFALL, FALSE )
			
			eCheatStatusSkyfall = CHEAT_INACTIVE
			
			BREAK
			
	
		DEFAULT
		BREAK
	ENDSWITCH

ENDPROC

PROC CHEAT_DRUNK()
	
	SWITCH eCheatStatusDrunk
	
		CASE CHEAT_INACTIVE
		
			// Do Nothing
		
			BREAK
		
		CASE CHEAT_ACTIVATE
	
			DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_DRUNK")
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_DRUNK, TRUE )
			Make_Ped_Drunk_Constant(PLAYER_PED_ID())
			Activate_Drunk_Camera_Constant()
			eCheatStatusDrunk = CHEAT_RUNNING
						
			BREAK
			
		CASE CHEAT_RUNNING
		
			IF IS_ANY_MISSION_ACTIVE()
			OR NOT IS_PLAYER_PLAYING(PLAYER_ID())
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_DRUNK )
				eCheatStatusDrunk = CHEAT_DEACTIVATE
			ENDIF
			
			BREAK
			
		CASE CHEAT_DEACTIVATE
			
			DISPLAY_CHEAT_DEACTIVATION_NOTIFICATION("CHEAT_DRUNK")
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_DRUNK, FALSE )
			Make_Ped_Sober(PLAYER_PED_ID())
			Quit_Drunk_Camera_Gradually(1000)
			eCheatStatusDrunk = CHEAT_INACTIVE
			BREAK
			
		CASE CHEAT_CLEANUP
			Quit_Drunk_Camera_Immediately()
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_DRUNK, FALSE )
			eCheatStatusDrunk = CHEAT_INACTIVE
			BREAK
		
		DEFAULT // Shouldn't get here

			SCRIPT_ASSERT("cheat controller: Cheat drunk has entered a default case, not allowed!")
		BREAK
	ENDSWITCH
	
ENDPROC


PROC ACTIVATE_AIM_SLOWMO()

		
	++iAimSlowMoLevel
	
	IF iAimSlowMoLevel > 3
		
		iAimSlowMoLevel = 0
		eCheatStatusAimSlowMo = CHEAT_DEACTIVATE
		
		EXIT
	
	ENDIF
	
	SWITCH iAimSlowMoLevel
	
	CASE 1
	
		DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_AIM_SLOW_MO1")
		fAimSlowMoValue = 0.6
	
		BREAK
		
	CASE 2
	
		DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_AIM_SLOW_MO2")
		fAimSlowMoValue = 0.4
		
		BREAK
	
	CASE 3
	
		DISPLAY_CHEAT_ACTIVATION_NOTIFICATION("CHEAT_AIM_SLOW_MO3")
		fAimSlowMoValue = 0.2
		
		BREAK
	
	
	DEFAULT
		SCRIPT_ASSERT("cheat controller: Aim slowmo cheat has out of bounds iAimSlowMoLevel value")
	BREAK	
	ENDSWITCH
	
	eCheatStatusAimSlowMo = CHEAT_RUNNING
		
ENDPROC


PROC SET_SLOW_MO_AIM()
			
	// Check the player is on foot
	IF IS_PED_ON_FOOT(PLAYER_PED_ID())
		//IF IS_PLAYER_TARGETTING_ANYTHING(PLAYER_ID())
		//OR IS_PLAYER_FREE_AIMING(PLAYER_ID())
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_AIM )
			
			IF GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_UNARMED
            AND GET_PEDS_CURRENT_WEAPON(PLAYER_PED_ID()) <> WEAPONTYPE_OBJECT //CHECK FOR MOBILE PHONE AS THIS WILL TRIGGER
          		SET_TIME_SCALE( fAimSlowMoValue )
            ELSE
				SET_TIME_SCALE( 1.0 )
			ENDIF
			
		ELSE
			SET_TIME_SCALE( 1.0 )
		ENDIF
	
	ENDIF

ENDPROC

PROC CHEAT_AIM_SLOWMO()

	
		SWITCH eCheatStatusAimSlowMo
	
		CASE CHEAT_INACTIVE
				
			// Do nothing
			
		BREAK
		
		CASE CHEAT_ACTIVATE
			
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_AIM_SLOWMO, TRUE )
			ACTIVATE_AIM_SLOWMO()
				
		BREAK
	
		CASE CHEAT_RUNNING
		
			IF NOT IS_PLAYER_PLAYING(PLAYER_ID())
			OR IS_ANY_MISSION_ACTIVE()
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_ALL )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_OFF_MISSION )
			OR IS_CHEAT_DISABLED( CHEAT_TYPE_AIM_SLOWMO )
				eCheatStatusAimSlowMo = CHEAT_DEACTIVATE
				EXIT
			ENDIF
			
			SET_SLOW_MO_AIM()
		
		BREAK
		
		CASE CHEAT_DEACTIVATE
		
			DISPLAY_CHEAT_DEACTIVATION_NOTIFICATION("CHEAT_AIM_SLOW_MO")
			REGISTER_CHEAT_ACTIVE( CHEAT_TYPE_AIM_SLOWMO, FALSE )
			SET_TIME_SCALE(1.0)
			iAimSlowMoLevel = 0
			eCheatStatusAimSlowMo = CHEAT_INACTIVE
		
		BREAK
		
		DEFAULT
			// Should never get here
			SCRIPT_ASSERT("cheat controller: Aim slowmo cheat has entered a default case, not allowed!")
		BREAK	
	ENDSWITCH

ENDPROC

/// PURPOSE: Processes all the off-mission cheats
///    
PROC PROCESS_OFF_MISSION_CHEATS()

	CHEAT_0GRAVITY()
	CHEAT_BANG_BANG()
	CHEAT_DRUNK()
	CHEAT_EXPLOSIVE_MELEE()
	CHEAT_FLAMING_BULLETS()
	CHEAT_INVINCIBILITY()
	CHEAT_SKYFALL()
	CHEAT_SLOWMO()
	CHEAT_AIM_SLOWMO()
	
ENDPROC


/// PURPOSE: Processes all the cheats
///    
PROC PROCESS_CHEATS()

	#IF IS_DEBUG_BUILD
		PROCESS_CHEAT_WIDGETS()
		DISPLAY_DEBUG_INFO()
	#ENDIF
	
	CHECK_CHEAT_CODES()
	PROCESS_GENERAL_CHEATS()
	PROCESS_OFF_MISSION_CHEATS()
	PROCESS_VEHICLE_SPAWN_CHEATS( eCheatVehicleModel )

ENDPROC


/// PURPOSE: Cleanup cheats and terminate script
PROC CLEANUP_SCRIPT()

	#IF IS_DEBUG_BUILD
		// Cleanup widgets
		CPRINTLN(DEBUG_AMBIENT, "Cheat Controller: Deleting debug widgets...")
		
		CLEANUP_WIDGETS()
		
	#ENDIF
	
	SET_RIOT_MODE_ENABLED(FALSE)
	
	IF eCheatStatusSkyfall = CHEAT_RUNNING
		CLEANUP_SKYFALL_CHEAT()
	ENDIF
	
	IF eCheatStatusDrunk = CHEAT_RUNNING
		Quit_Drunk_Camera_Immediately()
	ENDIF
	
	IF eCheatStatusSlowMo = CHEAT_RUNNING
	OR eCheatStatusAimSlowMo = CHEAT_RUNNING
		SET_TIME_SCALE(1.0)
	ENDIF
	
	IF NOT IS_PED_INJURED(PLAYER_PED_ID())
	
		IF eCheatStatusInvincibility = CHEAT_RUNNING			
			SET_PLAYER_INVINCIBLE( PLAYER_ID(), FALSE )
			SET_ENTITY_PROOFS(PLAYER_PED_ID(), FALSE, FALSE, FALSE, FALSE, FALSE)
		ENDIF
		
		SET_RUN_SPRINT_MULTIPLIER_FOR_PLAYER(PLAYER_ID(), 1.0)
		SET_SWIM_MULTIPLIER_FOR_PLAYER(PLAYER_ID(),1.0) 
	ENDIF
	
	SET_GRAVITY_LEVEL(GRAV_EARTH)
	
	// Reset globals
	g_iBitsetCheatsCurrentlyActive = 0
	g_iBitsetCheatsCurrentlyDisabled = 0
	g_iBitsetCheatsUsedThisSession = 0
	g_iCheatTriggerBitset = 0
	g_bHasAnyCheatBeenUsed = FALSE
	g_eCheatVehicleModelName = DUMMY_MODEL_FOR_SCRIPT
		
	CPRINTLN(DEBUG_AMBIENT, "Cheat Controller: Terminated")
	
	// End script
	TERMINATE_THIS_THREAD()
ENDPROC

//////////////////////////////////
/// MAIN LOOP
//////////////////////////////////
///    
SCRIPT

	CPRINTLN(DEBUG_AMBIENT, "Cheat Controller: Starting...")

	// This script needs to cleanup only when the game moves from SP to MP
	IF (HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|FORCE_CLEANUP_FLAG_MAGDEMO))
		CPRINTLN(DEBUG_AMBIENT, "Cheat Controller: Forced to cleanup (SP to MP)")
		CLEANUP_SCRIPT()
	ENDIF


	// Setup debug widgets.

	#IF IS_DEBUG_BUILD
		SETUP_BASIC_CHEAT_WIDGETS()
		bWidgetsCreated = FALSE
	#ENDIF
	
	// Initialise globals
	g_iBitsetCheatsCurrentlyActive = 0
	g_iBitsetCheatsCurrentlyDisabled = 0
	g_iBitsetCheatsUsedThisSession = 0
	g_iCheatTriggerBitset = 0
	g_bHasAnyCheatBeenUsed = FALSE
	g_eCheatVehicleModelName = DUMMY_MODEL_FOR_SCRIPT
	
	INIT_CHEAT_FEED_MESSAGES()
	
	// Main loop
	WHILE TRUE

		PROCESS_CHEATS()
		
		IF NETWORK_IS_GAME_IN_PROGRESS()
		AND IS_NET_PLAYER_OK(PLAYER_ID(), TRUE, TRUE)
		AND NOT g_b_TransitionActive
		AND g_Private_Gamemode_Current = GAMEMODE_FM
			CPRINTLN(DEBUG_AMBIENT, "Cheat Controller active in MP, cleaning up")
			CLEANUP_SCRIPT()
		ENDIF
		
		WAIT(0)
		
	ENDWHILE

ENDSCRIPT
