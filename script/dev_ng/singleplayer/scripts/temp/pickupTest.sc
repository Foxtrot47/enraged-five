// Includes
USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_misc.sch"
USING "commands_script.sch"
USING "commands_streaming.sch"
USING "script_player.sch"

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	pickupTest.sc
//		AUTHOR			:	
//		DESCRIPTION		:	
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

#IF IS_DEBUG_BUILD

// -----------------------------------------------------------------------------------------------------------
//		Variable declaration
// -----------------------------------------------------------------------------------------------------------
	
	// Pickup widgets
	WIDGET_GROUP_ID widgetGroup
	PICKUP_INDEX    piPickup
	INT				iPickupType = 0
	
	FLOAT			fPos[3]
	FLOAT			fRot[3]
	
	INT				iFlags       = 0
	BOOL			bSnapFlag    = FALSE
	BOOL			bOrientFlag  = FALSE
	BOOL			bUprightFlag = FALSE
	
	BOOL			bApplyRotate = FALSE
	BOOL			bApplyFlags  = FALSE
	BOOL			bLiveUpdate  = FALSE
	BOOL			bUpdateOnce  = FALSE
	BOOL			bMouseMode	 = FALSE
	BOOL			bGetPedCoords = FALSE
	BOOL			bDumpToDebug = FALSE

// -----------------------------------------------------------------------------------------------------------
//		Script Cleanup
// -----------------------------------------------------------------------------------------------------------

	// Unload all pickup models
	PROC UNLOAD_MODELS
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_LD_SCRAP)
		SET_MODEL_AS_NO_LONGER_NEEDED(PROP_POWER_CELL)
	ENDPROC

	// Load all pickup models
	PROC LOAD_MODELS

		REQUEST_MODEL(PROP_LD_SCRAP)
		REQUEST_MODEL(PROP_POWER_CELL)
		
		WHILE NOT HAS_MODEL_LOADED(PROP_LD_SCRAP)
		AND NOT HAS_MODEL_LOADED(PROP_POWER_CELL)
			REQUEST_MODEL(PROP_LD_SCRAP)
			REQUEST_MODEL(PROP_POWER_CELL)
			WAIT(0)
		ENDWHILE
	ENDPROC

	/// PURPOSE: Deletes widget box
	PROC CLEANUP_WIDGET()

		IF DOES_PICKUP_EXIST(piPickup)
			REMOVE_PICKUP(piPickup)
		ENDIF

		IF DOES_WIDGET_GROUP_EXIST(widgetGroup)
			DELETE_WIDGET_GROUP(widgetGroup)
		ENDIF
	ENDPROC
	
	PROC DUMP_VECTOR_TEXT(FLOAT pkx, FLOAT pky, FLOAT pkz)
		SAVE_STRING_TO_DEBUG_FILE("<< ")
		SAVE_FLOAT_TO_DEBUG_FILE(pkx)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(pky)
		SAVE_STRING_TO_DEBUG_FILE(", ")
		SAVE_FLOAT_TO_DEBUG_FILE(pkz)
		SAVE_STRING_TO_DEBUG_FILE(" >>")
	ENDPROC
	
	PROC DUMP_PICKUP_TEXT()
		OPEN_DEBUG_FILE()
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("------------PICKUP-------------")
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Coords: ")
		DUMP_VECTOR_TEXT(fPos[0],fPos[1],fPos[2])
		SAVE_NEWLINE_TO_DEBUG_FILE()
		SAVE_STRING_TO_DEBUG_FILE("Angles: ")
		DUMP_VECTOR_TEXT(fRot[0],fRot[1],fRot[2])
		SAVE_NEWLINE_TO_DEBUG_FILE()
		CLOSE_DEBUG_FILE()
	ENDPROC
#ENDIF

/// PURPOSE: Removes any active models, anims and resets camera
PROC SCRIPT_CLEANUP()
	CPRINTLN(DEBUG_AMBIENT, "PickupTest: Cleanup")
	#IF IS_DEBUG_BUILD 
		UNLOAD_MODELS()
		CLEANUP_WIDGET() 
	#ENDIF
	TERMINATE_THIS_THREAD()
ENDPROC

#IF IS_DEBUG_BUILD
	
	/// PURPOSE: Checks for player pressing debug key to cancel script
	PROC CHECK_DEBUG_CLEANUP()
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
		OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
		OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			SCRIPT_CLEANUP()
		ENDIF
	ENDPROC
	
	
	/// PURPOSE: Create pickup instance at specified coords and heading
	FUNC PICKUP_TYPE GET_PICKUP_TYPE()
	
		SWITCH iPickupType
			CASE 0  RETURN PICKUP_ARMOUR_STANDARD        BREAK
			CASE 1  RETURN PICKUP_HEALTH_STANDARD        BREAK
			CASE 2  RETURN PICKUP_CUSTOM_SCRIPT          BREAK
			CASE 3  RETURN PICKUP_CUSTOM_SCRIPT          BREAK
			CASE 4  RETURN PICKUP_WEAPON_ASSAULTRIFLE    BREAK
			CASE 5  RETURN PICKUP_WEAPON_GRENADE         BREAK
			CASE 6  RETURN PICKUP_WEAPON_GRENADELAUNCHER BREAK
			CASE 7  RETURN PICKUP_WEAPON_MG 			 BREAK
			CASE 8  RETURN PICKUP_WEAPON_MINIGUN	     BREAK
			CASE 9  RETURN PICKUP_WEAPON_PISTOL 		 BREAK
			CASE 10 RETURN PICKUP_WEAPON_RPG 			 BREAK
			CASE 11 RETURN PICKUP_WEAPON_PUMPSHOTGUN	 BREAK
			CASE 12 RETURN PICKUP_WEAPON_SMG 		     BREAK
			CASE 13 RETURN PICKUP_WEAPON_SNIPERRIFLE	 BREAK
			CASE 14 RETURN PICKUP_WEAPON_STICKYBOMB	 	 BREAK
			CASE 15 RETURN PICKUP_WEAPON_ASSAULTSHOTGUN	 BREAK
			CASE 16 RETURN PICKUP_WEAPON_SAWNOFFSHOTGUN	 BREAK
			CASE 17 RETURN PICKUP_WEAPON_CROWBAR		 BREAK
			CASE 18 RETURN PICKUP_WEAPON_BAT			 BREAK
			// Exile
			CASE 19 RETURN PICKUP_WEAPON_APPISTOL		 BREAK
			CASE 20 RETURN PICKUP_WEAPON_COMBATMG		 BREAK
		ENDSWITCH
	
		RETURN PICKUP_CUSTOM_SCRIPT
	ENDFUNC

	/// PURPOSE: Returns custom model type for selected pickup (if applicable)
	FUNC MODEL_NAMES GET_PICKUP_MODEL()
			
		SWITCH iPickupType
			CASE 2 RETURN PROP_LD_SCRAP   BREAK
			CASE 3 RETURN PROP_POWER_CELL BREAK
		ENDSWITCH

		RETURN DUMMY_MODEL_FOR_SCRIPT
	ENDFUNC

	/// PURPOSE: Create pickup instance at specified coords and heading
	PROC UPDATE_PICKUP()

		IF DOES_PICKUP_EXIST(piPickup)
			REMOVE_PICKUP(piPickup)
		ENDIF

		IF bLiveUpdate
			iFlags       = 0
			bSnapFlag    = FALSE
			bOrientFlag  = FALSE
			bUprightFlag = FALSE
			bApplyFlags  = FALSE
		ENDIF
		
		CLEAR_BIT(iFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))
		CLEAR_BIT(iFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND))
		CLEAR_BIT(iFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))
		
		IF bApplyFlags
			IF bSnapFlag    SET_BIT(iFlags, ENUM_TO_INT(PLACEMENT_FLAG_SNAP_TO_GROUND))   ENDIF
			IF bOrientFlag  SET_BIT(iFlags, ENUM_TO_INT(PLACEMENT_FLAG_ORIENT_TO_GROUND)) ENDIF		
			IF bUprightFlag SET_BIT(iFlags, ENUM_TO_INT(PLACEMENT_FLAG_UPRIGHT))          ENDIF	
		ENDIF
		
		IF bApplyRotate
			IF GET_PICKUP_MODEL() = DUMMY_MODEL_FOR_SCRIPT
				piPickup = CREATE_PICKUP_ROTATE(GET_PICKUP_TYPE(), <<fPos[0],fPos[1],fPos[2]>>, <<fRot[0],fRot[1],fRot[2]>>, iFlags, -1, EULER_YXZ, TRUE)
			ELSE
				piPickup = CREATE_PICKUP_ROTATE(GET_PICKUP_TYPE(), <<fPos[0],fPos[1],fPos[2]>>, <<fRot[0],fRot[1],fRot[2]>>, iFlags, -1, EULER_YXZ, TRUE, GET_PICKUP_MODEL())
			ENDIF
		ELSE
			IF GET_PICKUP_MODEL() = DUMMY_MODEL_FOR_SCRIPT
				piPickup = CREATE_PICKUP(GET_PICKUP_TYPE(), <<fPos[0],fPos[1],fPos[2]>>, iFlags, -1, TRUE)
			ELSE
				piPickup = CREATE_PICKUP(GET_PICKUP_TYPE(), <<fPos[0],fPos[1],fPos[2]>>, iFlags, -1, TRUE, GET_PICKUP_MODEL())
			ENDIF
		ENDIF	
	ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		Initialisation
// -----------------------------------------------------------------------------------------------------------

	/// PURPOSE: Initialise widgets with initial values
	PROC SETUP_WIDGETS()
	
		// Setup widget
		widgetGroup = START_WIDGET_GROUP("Pickup Placement Helper")

			START_NEW_WIDGET_COMBO()
			ADD_TO_WIDGET_COMBO("Ambient - Body Armour")
			ADD_TO_WIDGET_COMBO("Ambient - Health Pack")
			ADD_TO_WIDGET_COMBO("Ambient - Letter Scrap")
			ADD_TO_WIDGET_COMBO("Ambient - Spaceship Part")
			ADD_TO_WIDGET_COMBO("Weapon - Assault Rifle")
			ADD_TO_WIDGET_COMBO("Weapon - Grenade")
			ADD_TO_WIDGET_COMBO("Weapon - Grenade Launcher")
			ADD_TO_WIDGET_COMBO("Weapon - Machine Gun")
			ADD_TO_WIDGET_COMBO("Weapon - Minigun")
			ADD_TO_WIDGET_COMBO("Weapon - Pistol")
			ADD_TO_WIDGET_COMBO("Weapon - RPG")
			ADD_TO_WIDGET_COMBO("Weapon - Shotgun")
			ADD_TO_WIDGET_COMBO("Weapon - SMG")
			ADD_TO_WIDGET_COMBO("Weapon - Sniper Rifle")
			ADD_TO_WIDGET_COMBO("Weapon - Sticky Bomb")
			ADD_TO_WIDGET_COMBO("Weapon - Assault Shotgun")
			ADD_TO_WIDGET_COMBO("Weapon - Sawn-Off Shotgun")
			ADD_TO_WIDGET_COMBO("Weapon - Crowbar")
			ADD_TO_WIDGET_COMBO("Weapon - Bat")
			ADD_TO_WIDGET_COMBO("EXILE - AP Pistol")
			ADD_TO_WIDGET_COMBO("EXILE - Combat MG")
			STOP_WIDGET_COMBO("Pickup Type", iPickupType)

			START_WIDGET_GROUP("Pickup Location")
				ADD_WIDGET_FLOAT_SLIDER("Pos X", fPos[0], -7000, 7000, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Pos Y", fPos[1], -7000, 7000, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Pos Z", fPos[2], -7000, 7000, 0.01)
				ADD_WIDGET_FLOAT_SLIDER("Rot X", fRot[0], -360, 360, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Rot Y", fRot[1], -360, 360, 1.0)
				ADD_WIDGET_FLOAT_SLIDER("Rot Z", fRot[2], -360, 360, 1.0)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_STRING("Set coordinates - OVERWRITES EXISTING VALUES!")
			ADD_WIDGET_BOOL("Set to player coordinates", bGetPedCoords)
			ADD_WIDGET_BOOL("Fuzzy mode (Experimental, dodgy. Need to enable debug in script tools)", bMouseMode)
			
			START_WIDGET_GROUP("Placement Flags")
				ADD_WIDGET_BOOL("Snap to Ground",   bSnapFlag)
				ADD_WIDGET_BOOL("Orient to Ground", bOrientFlag)
				ADD_WIDGET_BOOL("Set as Upright",   bUprightFlag)
			STOP_WIDGET_GROUP()
			
			ADD_WIDGET_STRING("Create Pickup")
			ADD_WIDGET_BOOL("Apply Rotate", bApplyRotate)
			ADD_WIDGET_BOOL("Apply Flags",  bApplyFlags)
			ADD_WIDGET_BOOL("Live Update (flags have no effect)", bLiveUpdate)
			ADD_WIDGET_BOOL("Update Once (flags should work)", bUpdateOnce)

			ADD_WIDGET_STRING("Output")
			ADD_WIDGET_BOOL("Dump to temp_debug.txt", bDumpToDebug)
			
		STOP_WIDGET_GROUP()
	ENDPROC
	
	/// PURPOSE: Update pickup widgets
	PROC UPDATE_WIDGETS()
	
		IF bMouseMode
			bApplyRotate = FALSE
			bSnapFlag = TRUE
			IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)
				VECTOR vTmp
				vTmp = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
				fPos[0] = vTmp.x
				fPos[1] = vTmp.y
				fPos[2] = vTmp.z + 0.4
			ENDIF
		ENDIF
		IF bGetPedCoords
			VECTOR vTmp
			vTmp = GET_ENTITY_COORDS(PLAYER_PED_ID(), FALSE)
			fPos[0] = vTmp.x
			fPos[1] = vTmp.y
			fPos[2] = vTmp.z
			bGetPedCoords = FALSE
		ENDIF
		IF bLiveUpdate
			UPDATE_PICKUP()
		ELIF bUpdateOnce
			UPDATE_PICKUP()
			bUpdateOnce = FALSE
		ENDIF
		IF bDumpToDebug
			DUMP_PICKUP_TEXT()
			bDumpToDebug = FALSE
		ENDIF
	ENDPROC
#ENDIF

/// Main script loop
SCRIPT

	// Fade in screen if necessary
	CPRINTLN(DEBUG_AMBIENT, "PickupTest: Launched")
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(500)
	ENDIF

	// Check for force cleanup of script
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP)
		CPRINTLN(DEBUG_AMBIENT, "PickupTest: Force Cleanup")
		SCRIPT_CLEANUP()
	ENDIF
	
	// Create widgets and load pickup models
	#IF IS_DEBUG_BUILD 
		SETUP_WIDGETS()
		LOAD_MODELS()
	#ENDIF
	
	// Main loop
	WHILE (TRUE)
		
		#IF IS_DEBUG_BUILD
			UPDATE_WIDGETS()
			CHECK_DEBUG_CLEANUP()
		#ENDIF
	
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
