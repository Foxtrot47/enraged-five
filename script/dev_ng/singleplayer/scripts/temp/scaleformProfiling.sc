

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "cellphone_public.sch"



// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	scaleformProfiling.sc
//		AUTHOR			:	Gareth Evans / Taylor Wright
//		DESCRIPTION		:	A simple holding script to profile scaleform in game.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

SCALEFORM_INDEX mov
// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Cleanup()

	PRINTSTRING("...Placeholder Scaleform Test Cleanup")
	PRINTNL()
	DISABLE_CELLPHONE(FALSE)
	SET_GAME_PAUSED(FALSE)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(mov)
	TERMINATE_THIS_THREAD()
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Passed()

	PRINTSTRING("...Placeholder Scaleform Test Passed")
	PRINTNL()
	

	Mission_Cleanup()
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Failed()

	PRINTSTRING("...Placeholder Scaleform Test Failed")
	PRINTNL()
	

	Mission_Cleanup()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		pad events
// -----------------------------------------------------------------------------------------------------------

PROC PASS_INPUTS_TO_SCALEFORM(SCALEFORM_INDEX movieID)

	 IF dpad_pause_cued = FALSE
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			dpad_pause_cued = TRUE
			SETTIMERA(0)
		
		ENDIF
	ELSE
		IF TIMERA() > 50

			dpad_pause_cued = FALSE

		ENDIF
	ENDIF
	
	
	IF dpad_pause_cued = FALSE
        IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(SCALEFORM_INPUT_EVENT_LEFT)))
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(SCALEFORM_INPUT_EVENT_RIGHT)))
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LB)
			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(SCALEFORM_INPUT_EVENT_LEFTSHOULDER1)))
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RB)
			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(SCALEFORM_INPUT_EVENT_RIGHTSHOULDER1)))
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(SCALEFORM_INPUT_EVENT_UP)))
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(SCALEFORM_INPUT_EVENT_DOWN)))
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CROSS)))
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CIRCLE)))
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	ENDIF
	
ENDPROC


// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	PRINTSTRING("...Placeholder Scaleform Test Launched")
	PRINTNL()
	
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(500)
	ENDIF

	IF (HAS_FORCE_CLEANUP_OCCURRED())
		PRINTSTRING("...Placeholder Scaleform Test Force Cleanup")
		PRINTNL()

		Mission_Cleanup()
	ENDIF
	
	mov = REQUEST_SCALEFORM_MOVIE("scaleform_profiling")
	WHILE (NOT HAS_SCALEFORM_MOVIE_LOADED(mov))
	WAIT(0)
	ENDWHILE
	WHILE (TRUE)
	
		
		#IF IS_DEBUG_BUILD
			IF NOT IS_CELLPHONE_DISABLED()
				DISABLE_CELLPHONE(TRUE)
				//SET_GAME_PAUSED(TRUE)
			ENDIF
		#ENDIF
		DRAW_SCALEFORM_MOVIE(mov, 0.5,0.5,1.0,1.0,255,255,255,0)
	    PASS_INPUTS_TO_SCALEFORM(mov)
		
		
		
#IF IS_DEBUG_BUILD
		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			Mission_Passed()
		ENDIF

		
		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			Mission_Failed()
		ENDIF
#ENDIF	//	IS_DEBUG_BUILD
	
		WAIT(0)
	ENDWHILE


// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
