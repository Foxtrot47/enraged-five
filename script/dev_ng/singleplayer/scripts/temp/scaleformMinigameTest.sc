

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "cellphone_public.sch"



// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	placeholderMission.sc
//		AUTHOR			:	Keith
//		DESCRIPTION		:	A placeholder scaleform test script.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

SCALEFORM_INDEX mov
CAMERA_INDEX camVisualFlow
BLIP_INDEX testBlip = ADD_BLIP_FOR_COORD(<< 100.0, 100.0, 30.0 >>)
INT iSingleFrame = 0 


BOOL bFirstRunFlag = TRUE

BOOL bLeftButtonDown = FALSE
BOOL bPreviousLeftButton = FALSE
FLOAT fLeftButtonStatus = 0
BOOL bRightButtonDown = FALSE
BOOL bPreviousRightButton = FALSE
FLOAT fRightButtonStatus = 0
BOOL bUpButtonDown = FALSE
BOOL bPreviousUpButton = FALSE
FLOAT fUpButtonStatus = 0
BOOL bDownButtonDown = FALSE
BOOL bPreviousDownButton = FALSE
FLOAT fDownButtonStatus = 0
BOOL bCircleButtonDown = FALSE
BOOL bPreviousCircleButton = FALSE
FLOAT fCircleButtonStatus = 0
BOOL bTriangleButtonDown = FALSE
BOOL bPreviousTriangleButton = FALSE
FLOAT fTriangleButtonStatus = 0
BOOL bCrossButtonDown = FALSE
BOOL bPreviousCrossButton = FALSE
FLOAT fCrossButtonStatus = 0
BOOL bSquareButtonDown = FALSE
BOOL bPreviousSquareButton = FALSE
FLOAT fSquareButtonStatus = 0
	
	
// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Cleanup()

	PRINTSTRING("...Placeholder Scaleform Test Cleanup")
	PRINTNL()
	IF DOES_CAM_EXIST(camVisualFlow)
		DESTROY_CAM(camVisualFlow)
	ENDIF
	RENDER_SCRIPT_CAMS(FALSE, FALSE)
	DISABLE_CELLPHONE(FALSE)
	SET_GAME_PAUSED(FALSE)
	CLEAR_HELP(TRUE)
	SET_SCALEFORM_MOVIE_AS_NO_LONGER_NEEDED(mov)
	TERMINATE_THIS_THREAD()
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Passed()

	PRINTSTRING("...Placeholder Scaleform Test Passed")
	PRINTNL()
	

	Mission_Cleanup()
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Failed()

	PRINTSTRING("...Placeholder Scaleform Test Failed")
	PRINTNL()
	

	Mission_Cleanup()
	
ENDPROC

// -----------------------------------------------------------------------------------------------------------
//		pad events
// -----------------------------------------------------------------------------------------------------------

PROC PASS_INPUTS_TO_SCALEFORM(SCALEFORM_INDEX movieID)

	 IF dpad_pause_cued = FALSE
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
		OR IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			dpad_pause_cued = TRUE
			SETTIMERA(0)
		
		ENDIF
	ELSE
		IF TIMERA() > 50

			dpad_pause_cued = FALSE

		ENDIF
	ENDIF
	
	
	IF dpad_pause_cued = FALSE

		//Check for left button press and release
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			bLeftButtonDown = TRUE
			fLeftButtonStatus = 1
		ENDIF
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			bLeftButtonDown = TRUE
			fLeftButtonStatus = 1
		ENDIF
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_LEFT)
			bLeftButtonDown = FALSE
			fLeftButtonStatus = 0
		ENDIF
		IF NOT bLeftButtonDown = bPreviousLeftButton
			bPreviousLeftButton = bLeftButtonDown
			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(SCALEFORM_INPUT_EVENT_LEFT)))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fLeftButtonStatus)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		
		
		//Check for right button press and release
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			bRightButtonDown = TRUE
			fRightButtonStatus = 1
		ENDIF
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			bRightButtonDown = TRUE
			fRightButtonStatus = 1
		ENDIF
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_RIGHT)
			bRightButtonDown = FALSE
			fRightButtonStatus = 0
		ENDIF
		IF NOT bRightButtonDown = bPreviousRightButton
			bPreviousRightButton = bRightButtonDown
			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(SCALEFORM_INPUT_EVENT_RIGHT)))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fRightButtonStatus)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		
		
		//Check for Up button press and release
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			bUpButtonDown = TRUE
			fUpButtonStatus = 1
		ENDIF
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			bUpButtonDown = TRUE
			fUpButtonStatus = 1
		ENDIF
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_UP)
			bUpButtonDown = FALSE
			fUpButtonStatus = 0
		ENDIF
		IF NOT bUpButtonDown = bPreviousUpButton
			bPreviousUpButton = bUpButtonDown
			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(SCALEFORM_INPUT_EVENT_UP)))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fUpButtonStatus)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		
		
		//Check for Down button press and release
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			bDownButtonDown = TRUE
			fDownButtonStatus = 1
		ENDIF
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			bDownButtonDown = TRUE
			fDownButtonStatus = 1
		ENDIF
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_DOWN)
			bDownButtonDown = FALSE
			fDownButtonStatus = 0
		ENDIF
		IF NOT bDownButtonDown = bPreviousDownButton
			bPreviousDownButton = bDownButtonDown
			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(SCALEFORM_INPUT_EVENT_DOWN)))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fDownButtonStatus)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		
		
		//Check for CIRCLE button press and release
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			bCircleButtonDown = TRUE
			fCircleButtonStatus = 1
		ENDIF
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			bCircleButtonDown = TRUE
			fCircleButtonStatus = 1
		ENDIF
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_CANCEL)
			bCircleButtonDown = FALSE
			fCircleButtonStatus = 0
		ENDIF
		IF NOT bCircleButtonDown = bPreviousCircleButton
			bPreviousCircleButton = bCircleButtonDown
			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CIRCLE)))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCircleButtonStatus)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		
		
		
		//Check for TRIANGLE button press and release
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
			bTriangleButtonDown = TRUE
			fTriangleButtonStatus = 1
		ENDIF
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
			bTriangleButtonDown = TRUE
			fTriangleButtonStatus = 1
		ENDIF
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_Y)
			bTriangleButtonDown = FALSE
			fTriangleButtonStatus = 0
		ENDIF
		IF NOT bTriangleButtonDown = bPreviousTriangleButton
			bPreviousTriangleButton = bTriangleButtonDown
			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(SCALEFORM_INPUT_EVENT_TRIANGLE)))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fTriangleButtonStatus)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		
		//Check for CROSS button press and release
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			bCrossButtonDown = TRUE
			fCrossButtonStatus = 1
		ENDIF
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			bCrossButtonDown = TRUE
			fCrossButtonStatus = 1
		ENDIF
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_ACCEPT)
			bCrossButtonDown = FALSE
			fCrossButtonStatus = 0
		ENDIF
		IF NOT bCrossButtonDown = bPreviousCrossButton
			bPreviousCrossButton = bCrossButtonDown
			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(SCALEFORM_INPUT_EVENT_CROSS)))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fCrossButtonStatus)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
		
		//Check for SQUARE button press and release
		IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
			bSquareButtonDown = TRUE
			fSquareButtonStatus = 1
		ENDIF
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
			bSquareButtonDown = TRUE
			fSquareButtonStatus = 1
		ENDIF
		IF NOT IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_FRONTEND_X)
			bSquareButtonDown = FALSE
			fSquareButtonStatus = 0
		ENDIF
		IF NOT bSquareButtonDown = bPreviousSquareButton
			bPreviousSquareButton = bSquareButtonDown
			BEGIN_SCALEFORM_MOVIE_METHOD(movieID, "SET_INPUT_EVENT")
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_INT((ENUM_TO_INT(SCALEFORM_INPUT_EVENT_SQUARE)))
				SCALEFORM_MOVIE_METHOD_ADD_PARAM_FLOAT(fSquareButtonStatus)
			END_SCALEFORM_MOVIE_METHOD()
		ENDIF
	
	ENDIF
	
	
	
	
ENDPROC


// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	PRINTSTRING("...Placeholder Scaleform Test Launched")
	PRINTNL()
	
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(500)
	ENDIF

	IF (HAS_FORCE_CLEANUP_OCCURRED())
		PRINTSTRING("...Placeholder Scaleform Test Force Cleanup")
		PRINTNL()

		Mission_Cleanup()
	ENDIF
	
	mov = REQUEST_SCALEFORM_MOVIE("p_bubblegum")
	WHILE (NOT HAS_SCALEFORM_MOVIE_LOADED(mov))
	WAIT(0)
	ENDWHILE
	WHILE (TRUE)
		
		IF iSingleFrame = 1
		
		
			#IF IS_DEBUG_BUILD
				IF NOT IS_CELLPHONE_DISABLED()
					DISABLE_CELLPHONE(TRUE)
					//SET_GAME_PAUSED(TRUE)
				ENDIF
			#ENDIF
			
		ENDIF	
		IF bFirstRunFlag = TRUE
			bFirstRunFlag = FALSE
			//SET_HELP_MESSAGE_STYLE(HELP_MESSAGE_STYLE_TAGGABLE, HUD_COLOUR_BLUE)
			
				
			camVisualFlow = CREATE_CAM("DEFAULT_SCRIPTED_CAMERA", TRUE)
			IF DOES_CAM_EXIST(camVisualFlow)
				SET_CAM_PARAMS(camVisualFlow, << -160.6632, -1072.1438, -1615.4709 >>, << -89.4999, -0.2863, 58.1189 >>, 45.0000)
				SET_CAM_NEAR_CLIP(camVisualFlow,0.01)
				SET_CAM_FAR_CLIP(camVisualFlow,0.02)	
				RENDER_SCRIPT_CAMS(TRUE,FALSE)
			ENDIF
			
			SET_BLIP_SPRITE(testBlip, RADAR_TRACE_RANDOM_CHARACTER)
			
			
		ENDIF
		
		iSingleFrame = 1
		DRAW_SCALEFORM_MOVIE(mov, 0.5,0.5,1.0,1.0,255,255,255,0)
	    PASS_INPUTS_TO_SCALEFORM(mov)
		
#IF IS_DEBUG_BUILD
		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
			Mission_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			Mission_Failed()
		ENDIF
#ENDIF	//	IS_DEBUG_BUILD
	
		WAIT(0)
	ENDWHILE


// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
