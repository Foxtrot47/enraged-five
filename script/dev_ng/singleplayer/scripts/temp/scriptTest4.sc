USING "rage_builtins.sch"
USING "globals.sch"
USING "beast_secret_unlock.sch"

// Compiled in to randomchar_controller.sc when FEATURE_SP_DLC_BEAST_SECRET is set to 1 
// and FEATURE_SP_DLC_BEAST_SECRET_DEBUG to 0.

#IF FEATURE_SP_DLC_BEAST_SECRET
#IF FEATURE_SP_DLC_BEAST_SECRET_DEBUG
	BeastUnlockVars sBeastUnlockVars
#ENDIF
#ENDIF

SCRIPT
	#IF FEATURE_SP_DLC_BEAST_SECRET
	#IF FEATURE_SP_DLC_BEAST_SECRET_DEBUG
	
		CPRINTLN(DEBUG_HUNTING, "Beast thread 4 launched.")
		
		IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
			CPRINTLN(DEBUG_HUNTING, "Beast thread 4 terminating.")
			Cleanup_Beast_Unlock(sBeastUnlockVars)
			TERMINATE_THIS_THREAD()
		ENDIF
		
		Reset_Beast_Unlock_Variables(sBeastUnlockVars)
		
		#IF IS_DEBUG_BUILD
			Create_Beast_Unlock_Widgets(sBeastUnlockVars, NULL)
		#ENDIF

		WHILE TRUE
			Maintain_Beast_Unlock(sBeastUnlockVars)
		
			#IF IS_DEBUG_BUILD
				Update_Beast_Unlock_Widgets(sBeastUnlockVars)
			#ENDIF
		
			WAIT(0)
		ENDWHILE
		
	#ENDIF //FEATURE_SP_DLC_BEAST_SECRET_DEBUG
	#ENDIF //FEATURE_SP_DLC_BEAST_SECRET
ENDSCRIPT
