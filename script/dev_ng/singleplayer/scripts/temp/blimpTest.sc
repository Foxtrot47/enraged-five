

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "RC_Helper_Functions.sch"
USING "rgeneral_include.sch"
#IF IS_DEBUG_BUILD
	USING "script_debug.sch"
	USING "select_mission_stage.sch"
#ENDIF

// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	blimpTest.sc
//		AUTHOR			:	
//		DESCRIPTION		:	Test script which pilots blimp around Los Santos and allows
//							the player to shoot it down.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************

CONST_INT Z_SKIP_RESET_RELEASE_WHEN_DESTROYED 0
CONST_INT Z_SKIP_RESET_DELETE_WHEN_DESTROYED 1
CONST_INT Z_SKIP_WARP_INTO_BLIMP 2

VEHICLE_INDEX vehBlimp
MODEL_NAMES modelBlimp = BLIMP
BLIP_INDEX blipBlimp

PED_INDEX pedBlimpPilot
MODEL_NAMES modelBlimpPilot = S_M_M_Pilot_02

VEHICLE_INDEX vehPlayerHelicopter
MODEL_NAMES modelPlayerHelicopter = BUZZARD

BOOL bDeleteBlimpWhenDestroyed = FALSE

#IF IS_DEBUG_BUILD
	CONST_INT MAX_SKIP_MENU_LENGTH 3
	MissionStageMenuTextStruct mSkipMenu[MAX_SKIP_MENU_LENGTH]
	WIDGET_GROUP_ID widgetGroup
	BOOL bDebug_ToggleBlip = TRUE
#ENDIF

PROC CREATE_BLIMP_BLIP()
	SAFE_REMOVE_BLIP(blipBlimp)
	IF IS_VEHICLE_OK(vehBlimp)
		blipBlimp = CREATE_VEHICLE_BLIP(vehBlimp)
		#IF IS_DEBUG_BUILD bDebug_ToggleBlip = TRUE #ENDIF
	ENDIF
ENDPROC

PROC RESET_BLIMP()
	IF IS_VEHICLE_OK(vehBlimp)
	AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBlimp)
		STOP_PLAYBACK_RECORDED_VEHICLE(vehBlimp)
	ENDIF
	SAFE_REMOVE_BLIP(blipBlimp)
	SAFE_DELETE_PED(pedBlimpPilot)
	SAFE_DELETE_VEHICLE(vehBlimp)
	REQUEST_MODEL(modelBlimp)
	REQUEST_VEHICLE_RECORDING(1, "Blimp_City")
	WHILE NOT HAS_MODEL_LOADED(modelBlimp)
	OR NOT HAS_VEHICLE_RECORDING_BEEN_LOADED(1, "Blimp_City")
		WAIT(0)
	ENDWHILE
	VECTOR v_blimp_spawn_rotation = GET_ROTATION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(1, "Blimp_City"), 0)
	vehBlimp = CREATE_VEHICLE(modelBlimp, GET_POSITION_OF_VEHICLE_RECORDING_ID_AT_TIME(GET_VEHICLE_RECORDING_ID(1, "Blimp_City"), 0), v_blimp_spawn_rotation.z)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelBlimp)
ENDPROC

PROC CREATE_PILOT()
	REQUEST_MODEL(modelBlimpPilot)
	WHILE NOT HAS_MODEL_LOADED(modelBlimpPilot)
		WAIT(0)
	ENDWHILE
	IF IS_VEHICLE_OK(vehBlimp)
		pedBlimpPilot = CREATE_PED_INSIDE_VEHICLE(vehBlimp, PEDTYPE_MISSION, modelBlimpPilot)
		IF IS_ENTITY_ALIVE(pedBlimpPilot)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(pedBlimpPilot, TRUE)
			SET_PED_CONFIG_FLAG(pedBlimpPilot, PCF_RunFromFiresAndExplosions, FALSE)
			SET_PED_CONFIG_FLAG(pedBlimpPilot, PCF_GetOutUndriveableVehicle, FALSE)
			SET_PED_CONFIG_FLAG(pedBlimpPilot, PCF_GetOutBurningVehicle, FALSE)
		ENDIF
	ENDIF
	IF IS_VEHICLE_OK(vehBlimp)
		START_PLAYBACK_RECORDED_VEHICLE(vehBlimp, 1, "Blimp_City")
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(modelBlimpPilot)
ENDPROC

PROC RESET_PLAYER_INTO_HELICOPTER()
	SAFE_RELEASE_VEHICLE(vehPlayerHelicopter)
	REQUEST_MODEL(modelPlayerHelicopter)
	WHILE NOT HAS_MODEL_LOADED(modelPlayerHelicopter)
		WAIT(0)
	ENDWHILE
	IF IS_VEHICLE_OK(vehBlimp)
		vehPlayerHelicopter = CREATE_VEHICLE(modelPlayerHelicopter, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(vehBlimp, <<50,0,15>>), 0)
		SET_HELI_BLADES_FULL_SPEED(vehPlayerHelicopter)
		SET_ENTITY_HEADING_FACE_ENTITY(vehPlayerHelicopter, vehBlimp)
		IF IS_ENTITY_ALIVE(PLAYER_PED_ID())
			SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlayerHelicopter)
			SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
			SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
			CLEAR_AREA(GET_ENTITY_COORDS(PLAYER_PED_ID()), 500, TRUE)
		ENDIF
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(modelPlayerHelicopter)
ENDPROC

PROC RESET_PLAYER_INTO_BLIMP()
	IF IS_VEHICLE_OK(vehBlimp)
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehBlimp)
		SET_GAMEPLAY_CAM_RELATIVE_PITCH(0.0)
		SET_GAMEPLAY_CAM_RELATIVE_HEADING(0.0)
	ENDIF
ENDPROC

PROC DO_BLIMP_EXPLOSION(VECTOR vPos)

	INT    iIndex 
	INT    iNumExplosions = 12
	VECTOR vOffset[12]
	
	vOffset[0]  = <<5,0,0>>
	vOffset[1]  = <<0,5,0>>
	vOffset[2]  = <<0,0,5>>
	vOffset[3]  = <<5,5,0>>
	vOffset[4]  = <<5,0,5>>
	vOffset[5]  = <<0,5,5>>
	vOffset[6]  = <<20,0,0>>
	vOffset[7]  = <<0,20,0>>
	vOffset[8]  = <<0,0,20>>
	vOffset[9]  = <<20,20,0>>
	vOffset[10] = <<20,0,20>>
	vOffset[11] = <<0,20,20>>
	
	ADD_EXPLOSION(vPos, EXP_TAG_PLANE, 1.0)
	FOR iIndex=0 TO iNumExplosions-1
		ADD_EXPLOSION(vPos + vOffset[iIndex], EXP_TAG_PLANE, 1.0)
	ENDFOR
ENDPROC

PROC UPDATE_BLIMP()
	
	IF DOES_ENTITY_EXIST(vehBlimp)
	
		IF IS_ENTITY_DEAD(vehBlimp)
		OR GET_ENTITY_HEALTH(vehBlimp) <= 0
		OR GET_VEHICLE_ENGINE_HEALTH(vehBlimp) <= 0
			
			SAFE_REMOVE_BLIP(blipBlimp)

			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBlimp)
				STOP_PLAYBACK_RECORDED_VEHICLE(vehBlimp)
			ENDIF
	
			// Only create explosion when blimp is deleted as otherwise
			// the explosion can affect the physics.
			IF bDeleteBlimpWhenDestroyed = TRUE
				VECTOR vBlimpPos = GET_ENTITY_COORDS(vehBlimp, FALSE)
				DO_BLIMP_EXPLOSION(vBlimpPos)
			ENDIF

			IF bDeleteBlimpWhenDestroyed = TRUE
				SAFE_DELETE_PED(pedBlimpPilot)
				SAFE_DELETE_VEHICLE(vehBlimp)
			ELSE
				SAFE_RELEASE_PED(pedBlimpPilot)
				SAFE_RELEASE_VEHICLE(vehBlimp)
			ENDIF

		ELIF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBlimp)
			SET_PLAYBACK_SPEED(vehBlimp, 0.25)
		ENDIF
	ENDIF
ENDPROC

PROC Script_Cleanup()
	#IF IS_DEBUG_BUILD PRINTLN("blimpTest: Cleanup") #ENDIF
	IF IS_VEHICLE_OK(vehBlimp)
	AND IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehBlimp)
		STOP_PLAYBACK_RECORDED_VEHICLE(vehBlimp)
	ENDIF
	REMOVE_VEHICLE_RECORDING(1, "Blimp_city")
	SAFE_REMOVE_BLIP(blipBlimp)
	SAFE_RELEASE_PED(pedBlimpPilot)
	SAFE_RELEASE_VEHICLE(vehBlimp)
	SAFE_RELEASE_VEHICLE(vehPlayerHelicopter)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelBlimpPilot)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelBlimp)
	SET_MODEL_AS_NO_LONGER_NEEDED(modelPlayerHelicopter)
	SET_MAX_WANTED_LEVEL(6)
	TERMINATE_THIS_THREAD()
ENDPROC

#IF IS_DEBUG_BUILD
	PROC CHECK_FOR_BLIMP_BLIP_TOGGLE()
		IF bDebug_ToggleBlip = TRUE
			IF NOT DOES_BLIP_EXIST(blipBlimp)
			AND IS_VEHICLE_OK(vehBlimp)
				blipBlimp = CREATE_VEHICLE_BLIP(vehBlimp)
			ENDIF
		ELSE
			IF DOES_BLIP_EXIST(blipBlimp)
				SAFE_REMOVE_BLIP(blipBlimp)
			ENDIF
		ENDIF
	ENDPROC

	/// PURPOSE:
	///    Checks for debug keys being pressed.
	PROC DEBUG_Check_Debug_Keys()
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
		OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
		OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
		OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P))
			Script_Cleanup()
		ENDIF
		INT i_new_state
		IF LAUNCH_MISSION_STAGE_MENU(mSkipMenu, i_new_state)
			SWITCH i_new_state
				CASE Z_SKIP_RESET_RELEASE_WHEN_DESTROYED
					bDeleteBlimpWhenDestroyed = FALSE
					RESET_BLIMP()
					CREATE_PILOT()
					CREATE_BLIMP_BLIP()
					RESET_PLAYER_INTO_HELICOPTER()
				BREAK
				CASE Z_SKIP_RESET_DELETE_WHEN_DESTROYED
					bDeleteBlimpWhenDestroyed = TRUE
					RESET_BLIMP()
					CREATE_PILOT()
					CREATE_BLIMP_BLIP()
					RESET_PLAYER_INTO_HELICOPTER()
				BREAK
				CASE Z_SKIP_WARP_INTO_BLIMP
					RESET_BLIMP()
					RESET_PLAYER_INTO_BLIMP()
				BREAK
			ENDSWITCH
		ENDIF
	ENDPROC
#ENDIF

SCRIPT

	#IF IS_DEBUG_BUILD PRINTLN("blimpTest: Launched") #ENDIF
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(500)
	ENDIF

	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_DEBUG_MENU|FORCE_CLEANUP_FLAG_SP_TO_MP)
		#IF IS_DEBUG_BUILD PRINTLN("blimpTest: Force Cleanup") #ENDIF
		Script_Cleanup()
	ENDIF
	
	#IF IS_DEBUG_BUILD
		mSkipMenu[Z_SKIP_RESET_RELEASE_WHEN_DESTROYED].sTxtLabel = "Reset, warp into heli, blimp released when destroyed"
		mSkipMenu[Z_SKIP_RESET_DELETE_WHEN_DESTROYED].sTxtLabel = "Reset, warp into heli, blimp deleted when destroyed"
		mSkipMenu[Z_SKIP_WARP_INTO_BLIMP].sTxtLabel = "Reset, warp into blimp (for carrec purposes)"
		IF NOT DOES_WIDGET_GROUP_EXIST(widgetGroup)
			widgetGroup = START_WIDGET_GROUP("Blimp widgets")
			ADD_WIDGET_BOOL("Toggle blimp's blip on/off", bDebug_ToggleBlip)	
			STOP_WIDGET_GROUP()
		ENDIF
	#ENDIF
	
	SET_MAX_WANTED_LEVEL(0)
	
	RESET_BLIMP()
	CREATE_PILOT()
	CREATE_BLIMP_BLIP()
	RESET_PLAYER_INTO_HELICOPTER()
	
	WHILE (TRUE)
		
		// Display 'Placeholder Mission'
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_SCALE(0.75, 0.9)
		SET_TEXT_WRAP(0.0, 1.0)
		DISPLAY_TEXT(0.05, 0.63, "PLCHLD_MISS")
		
		// Display Pass and Fail instructions
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_SCALE(0.4, 0.45)
		SET_TEXT_WRAP(0.0, 1.0)
		DISPLAY_TEXT(0.05, 0.7, "PLCHLD_PASS")

		#IF IS_DEBUG_BUILD
			CHECK_FOR_BLIMP_BLIP_TOGGLE()
			DEBUG_Check_Debug_Keys()
		#ENDIF
		
		UPDATE_BLIMP()
	
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
