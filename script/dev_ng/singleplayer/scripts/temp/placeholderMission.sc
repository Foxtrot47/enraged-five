

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "flow_public_core_override.sch"




// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	placeholderMission.sc
//		AUTHOR			:	Keith
//		DESCRIPTION		:	A placeholder for a mission within the mission flow.
//
// *****************************************************************************************
// *****************************************************************************************
// *****************************************************************************************




// ===========================================================================================================
//		Termination
// ===========================================================================================================

// -----------------------------------------------------------------------------------------------------------
//		Mission Cleanup
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Cleanup()

	//Test

	PRINTSTRING("...Placeholder Mission Cleanup")
	PRINTNL()
	
	TERMINATE_THIS_THREAD()
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//		Mission Pass
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Passed()

	PRINTSTRING("...Placeholder Mission Passed")
	PRINTNL()
	
	Mission_Flow_Mission_Passed()
	Mission_Cleanup()
	
ENDPROC




// -----------------------------------------------------------------------------------------------------------
//		Mission Fail
// -----------------------------------------------------------------------------------------------------------

PROC Mission_Failed()

	PRINTSTRING("...Placeholder Mission Failed")
	PRINTNL()
	
	Mission_Flow_Mission_Failed()
	Mission_Cleanup()
	
ENDPROC





// ===========================================================================================================
//		Script Loop
// ===========================================================================================================

SCRIPT

	PRINTSTRING("...Placeholder Mission Launched")
	PRINTNL()
	
	IF IS_SCREEN_FADED_OUT()
		DO_SCREEN_FADE_IN(500)
	ENDIF

	IF (HAS_FORCE_CLEANUP_OCCURRED())
		PRINTSTRING("...Placeholder Mission Force Cleanup")
		PRINTNL()
		Mission_Flow_Mission_Force_Cleanup()
		Mission_Cleanup()
	ENDIF
	
	SET_MISSION_FLAG(TRUE)
	
	
	WHILE (TRUE)
		
		// Display 'Placeholder Mission'
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_SCALE(0.75, 0.9)
		SET_TEXT_WRAP(0.0, 1.0)
		DISPLAY_TEXT(0.05, 0.63, "PLCHLD_MISS")
		
		// Display Pass and Fail instructions
		SET_TEXT_COLOUR(255, 255, 255, 255)
		SET_TEXT_SCALE(0.4, 0.45)
		SET_TEXT_WRAP(0.0, 1.0)
		DISPLAY_TEXT(0.05, 0.7, "PLCHLD_PASS")
		
#IF IS_DEBUG_BUILD
		// Check for Pass
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S))
		OR (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J))
			Mission_Passed()
		ENDIF

		// Check for Fail
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			Mission_Failed()
		ENDIF
#ENDIF	//	IS_DEBUG_BUILD
	
		WAIT(0)
	ENDWHILE

// Script should never reach here. Always terminate with cleanup function.
ENDSCRIPT
