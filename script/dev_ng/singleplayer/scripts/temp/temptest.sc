
//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.

USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "commands_vehicle.sch"
USING "commands_path.sch"
USING "flow_public_core_override.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "selector_public.sch"
using "dialogue_public.sch"
USING "commands_audio.sch"
USING "drunk_public.sch"
USING "beam_effect.sch"
USING "CompletionPercentage_public.sch"
USING "commands_graphics.sch"

//using "cutscene_builder.sch"
USING "replay_public.sch"
using "ped_component_public.sch"
USING "script_blips.sch"
USING "script_ped.sch"
USING "locates_public.sch"
USING "player_ped_public.sch"
#if IS_DEBUG_BUILD

float fLastDesiredSpeed
//vector vLastCoord
float fZOffset
float fRecSpeed[3]
vector fixHeight
vector vTakeoff
float fTargetHeight[9]
vector vFormationTarget[15]

int checkPointStage

/*

//ped_index pedLamar,pedDrunk
WIDGET_GROUP_ID thisWidget
vector vLamar,vDrunkOffset
float hDrunkOffset,hLamar
bool bStart
*/
/*
bool bPlay, bplaying,bsetDrunkStartToPlayer
	vector vDrunk,vLamar,vLamarTarget,vWalkTo,vShift,vNudgeThisFrame,vnew
	float hDrunk,hLamar,fGap, fLastGap
	WIDGET_GROUP_ID thisWidget
PED_INDEX pedLamar,peddrunk
int flag,iNudgeSteps
SEQUENCE_INDEX seq
*/

vehicle_index dummyVehicle

VEHICLE_INDEX vehLazer[3]
vehicle_INdex vehPlayer
int iStartTime

enum enumProgress
	COUNTDOWN,	
	FLYING,
	ENDED
endenum

enumProgress eProgress



//debug
bool wBredoVeh
int wIveh
float wFRecTime
float lastRecTime
bool bWidgetMade

WIDGET_GROUP_ID wFormation, wRecord

enum edebugState
	eDebugPending,
	eDebugWaitForVeh,
	eDebugWaitForPlayerOverride,
	eDebugOverridingRecording
endenum

edebugState debugState

proc cleanup()
	int i
	repeat count_of(vehLazer) i
		if does_entity_exist(vehLazer[i]) 
			IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer[i])
				STOP_PLAYBACK_RECORDED_VEHICLE(vehLazer[i])
			ENDIF
			IF IS_RECORDING_GOING_ON_FOR_VEHICLE(vehLazer[i])
				STOP_RECORDING_ALL_VEHICLES()
			ENDIF
			wait(0)
			DELETE_vehicle(vehLazer[i]) 
		endif
	endrepeat
	if does_entity_exist(vehPlayer)
		SET_ENTITY_COORDS(player_ped_id(),get_entity_coords(player_ped_id()))
		IF IS_RECORDING_GOING_ON_FOR_VEHICLE(vehPlayer)
			STOP_RECORDING_ALL_VEHICLES()
		ENDIF
		DELETE_vehicle(vehPlayer)
	ENDIF
	
	if DOES_WIDGET_GROUP_EXIST(wFormation)
		DELETE_WIDGET_GROUP(wFormation)
	endif
endproc



PROC resetMission()
	int i
	SET_MAX_WANTED_LEVEL(0)
	SET_WANTED_LEVEL_MULTIPLIER(0.0)
	SET_PLAYER_WANTED_LEVEL(player_id(),0)
	SET_PLAYER_WANTED_LEVEL_NOW(player_id())
	SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE,true)
	SET_VEHICLE_POPULATION_BUDGET(0)
	
	
	REQUEST_VEHICLE_RECORDING(120,"pilotSchool")
	REQUEST_VEHICLE_RECORDING(121,"pilotSchool")
	REQUEST_VEHICLE_RECORDING(122,"pilotSchool")
	REQUEST_MODEL(LAZER)
	while not HAS_VEHICLE_RECORDING_BEEN_LOADED(120,"pilotSchool")
	or not HAS_VEHICLE_RECORDING_BEEN_LOADED(121,"pilotSchool")
	or not HAS_VEHICLE_RECORDING_BEEN_LOADED(122,"pilotSchool")
	or not HAS_MODEL_LOADED(LAZER)
		WAIT(0)
	ENDWHILE
			
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(true)
	
	repeat count_of(vehLazer) i
		IF DOES_ENTITY_EXIST(vehLazer[i])
			DELETE_VEHICLE(vehLazer[i])
		ENDIF
	endrepeat
	
	IF DOES_ENTITY_EXIST(vehPlayer)	
		SET_ENTITY_COORDS(player_ped_id(),GET_ENTITY_COORDS(player_ped_id()))
		delete_vehicle(vehPlayer)
	ENDIF
	
	vehLazer[0] = CREATE_VEHICLE(LAZER,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(120,0,"pilotSchool"))
	vehLazer[1] = CREATE_VEHICLE(LAZER,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(121,0,"pilotSchool"))
	vehLazer[2] = CREATE_VEHICLE(LAZER,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(122,0,"pilotSchool"))
	
	dummyVehicle = CREATE_VEHICLE(LAZER,GET_POSITION_OF_VEHICLE_RECORDING_AT_TIME(120,0,"pilotSchool"))
	SET_ENTITY_VISIBLE(dummyVehicle,false)
	SET_ENTITY_COLLISION(dummyVehicle,false)
	SET_ENTITY_DYNAMIC(dummyVehicle,false)
	
	vehPlayer = CREATE_VEHICLE(LAZER,<<-960.0587, -3360.9028, 12.9444>>,60.4)



	SET_VEHICLE_USED_FOR_PILOT_SCHOOL(vehPlayer,true) 
	SET_VEHICLE_USED_FOR_PILOT_SCHOOL(vehLazer[0],true) 
	SET_VEHICLE_USED_FOR_PILOT_SCHOOL(vehLazer[1],true) 
	SET_VEHICLE_USED_FOR_PILOT_SCHOOL(vehLazer[2],true) 
	SET_PLANE_TURBULENCE_MULTIPLIER(vehPlayer,0.0)
	SET_PLANE_TURBULENCE_MULTIPLIER(vehLazer[0],0.0)
	SET_PLANE_TURBULENCE_MULTIPLIER(vehLazer[1],0.0)
	SET_PLANE_TURBULENCE_MULTIPLIER(vehLazer[2],0.0)
	
	SET_ENTITY_ROTATION(vehLazer[0],GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(120,0,"pilotSchool"))
	SET_ENTITY_ROTATION(vehLazer[1],GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(121,0,"pilotSchool"))
	SET_ENTITY_ROTATION(vehLazer[2],GET_ROTATION_OF_VEHICLE_RECORDING_AT_TIME(122,0,"pilotSchool"))
	SET_VEHICLE_ON_GROUND_PROPERLY(vehPlayer)
	SET_ENTITY_ROTATION(vehPlayer,<<0,0,60.4>>)
	SET_GAMEPLAY_CAM_RELATIVE_HEADING(0)
	
	iStartTime = GET_GAME_TIMER() + 3000
				
	if not IS_PED_INJURED(PLAYER_PED_ID())
		SET_PED_INTO_VEHICLE(player_ped_id(),vehPlayer)
	ENDIF
	
	eProgress = COUNTDOWN
	
	IF DOES_WIDGET_GROUP_EXIST(wRecord)
		DELETE_WIDGET_GROUP(wRecord)
	ENDIF
	
	IF NOT DOES_WIDGET_GROUP_EXIST(wFormation)
		wFormation = START_WIDGET_GROUP("FORMATION")
		STOP_WIDGET_GROUP()
	ENDIF
	
	SET_CURRENT_WIDGET_GROUP(wFormation)
	wRecord = START_WIDGET_GROUP("Record")
		add_widget_Bool("Redo vehicle",wBredoVeh)
		ADD_WIDGET_INT_SLIDER("veh",wIveh,0,2,1)		
	STOP_WIDGET_GROUP()
	

	
	
	checkPointStage = 0
	
	START_WIDGET_GROUP("Take Off")
		ADD_WIDGET_VECTOR_SLIDER("Take off point",vTakeoff,-4000,3000,1.0)
		ADD_WIDGET_FLOAT_SLIDER("Target Height",fTargetHeight[0],0,200,0.5)
		ADD_WIDGET_VECTOR_SLIDER("Target Coord",vFormationTarget[0],-4000,3000,1.0)
	STOP_WIDGET_GROUP()
	
	START_WIDGET_GROUP("Turn to")
		ADD_WIDGET_FLOAT_SLIDER("Target Height",fTargetHeight[1],0,200,0.5)
		ADD_WIDGET_VECTOR_SLIDER("Target Coord",vFormationTarget[1],-4000,3000,1.0)
	STOP_WIDGET_GROUP()
	
	CLEAR_CURRENT_WIDGET_GROUP(wFormation)
	
ENDPROC

FUNC FLOAT SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehicle_index leadVehicle, vehicle_index vehChasing, vehicle_index vehToAlter, vector vOffset, float fLeaderRecSpeed=1.0, float fMinSpeed=0.8, float fMaxSpeed=1.2)
	float fNewSpeed = 1.0
	IF IS_VEHICLE_DRIVEABLE(leadVehicle)
	AND IS_VEHICLE_DRIVEABLE(vehChasing)
	AND IS_VEHICLE_DRIVEABLE(vehToAlter)
	//	IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(leadVehicle)	
	//		IF GET_TIME_POSITION_IN_RECORDING(leadVehicle) < 24000
	//		OR GET_TIME_POSITION_IN_RECORDING(leadVehicle) > 44000			
				vector vChasing
				
				vector vTarget
			//	vector vNormal
				//vector vResult
				float fHeading
				float fLeadHeading
				float fHeadingVariance
				float fDistanceToLead
				float fOffsetToLead
				float fMultiplyer
		
			
				vChasing = GET_ENTITY_COORDS(vehChasing)
			
				vTarget = GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(leadVehicle,vOffset)
				
				fLeadHeading = GET_ENTITY_HEADING(leadVehicle)
				fHeading = GET_HEADING_FROM_COORDS(vChasing,vTarget)
				fHeadingVariance = fHeading - fLeadHeading
				fDistanceToLead = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehChasing,vTarget)
				fOffsetToLead = COS(fHeadingVariance) * fDistanceToLead
				
				float pitch = GET_ENTITY_PITCH(leadVehicle)
				
				if pitch > 90.0 
				OR pitch < -90.0
					fOffsetToLead *= -1.0
				endif
				
				
				fMultiplyer = 1.0
				if vehChasing = vehToAlter fMultiplyer*=-1.0 endif
				
				fNewSpeed = fLeaderRecSpeed + ((-fOffsetToLead * fMultiplyer) / 100.0)
				
				//cprintln(debug_trevor3,fNewSpeed," ",fOffsetToLead," ",fMultiplyer," ",fLeaderRecSpeed)
				
				if fNewSpeed < fMinSpeed fNewSpeed = fMinSpeed endif				
				if fNewSpeed > fMaxSpeed fNewSpeed = fMaxSpeed endif
				
				if ABSF(fNewSpeed-1.0) < 0.02
					fNewSpeed = 1.0
				endif
				
				SET_PLAYBACK_SPEED(vehToAlter,fNewSpeed)
				
				cprintln(debug_trevor3,fNewSpeed," ",fLeadHeading," ",fHeading," ",fHeadingVariance," ",fDistanceToLead," ",fOffsetToLead)
				
				
				DRAW_DEBUG_SPHERE(vTarget,0.5,255,0,0,120)
				
				
				fMaxSpeed = fMaxSpeed
				
				fMinSpeed=fMinSpeed
				vOffset=vOffset
					
				/*
				
				float fLeadHeading
				
				fLeadHeading = GET_ENTITY_HEADING(leadVehicle) * -1.0
										
				vNormal = <<SIN(fLeadHeading),cos(fLeadHeading),0.0>>
				
				vNormal = NORMALISE_VECTOR(vNormal)
				
				
				GET_LINE_PLANE_INTERSECT(vResult,vChasing,vNormal*500.0,vNormal,vFlyTo)
				
				float fRange = GET_DISTANCE_BETWEEN_COORDS(vResult,vChasing)
				
				
						
				float relHeading = GET_ENTITY_HEADING(vehChasing) - fHeading
				
				if vehChasing = vehplayer
					cprintln(debug_trevor3,fHeading," ",relHeading)
				endif
				
				if absf(relHeading) > 180
					if relHeading < 0 relHeading += 360 else relHeading -=360 endif
				endif
				
				float fDist,fMultiplyer
				fDist = GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehChasing,vFlyTo,false) * absf(cos(relHeading))
				
				if absf(relHeading) > 90 fMultiplyer = -1.0 else fMultiplyer = 1.0 endif
							
				if vehChasing = vehToAlter fMultiplyer*=-1.0 endif
						
				fNewSpeed = fLeaderRecSpeed + ((-fdist * fMultiplyer) / 100.0)
				
				//intercept speed
				
				//cprintln(debug_trevor3,"heading= ",fHeading," ent heading = ",GET_ENTITY_HEADING(vehChasing)," fDist= ",fDist," relHeading= ", relHeading , " fMultiplyer = " , fMultiplyer , " (-fdist * fMultiplyer) = " , (-fdist * fMultiplyer), " fNewSpeed = " , fNewSpeed)
						
				if fNewSpeed < fMinSpeed fNewSpeed = fMinSpeed endif				
				if fNewSpeed > fMaxSpeed fNewSpeed = fMaxSpeed endif
			//	cprintln(debug_trevor3,GET_GAME_TIMER())
			//	IF GET_DISTANCE_BETWEEN_ENTITIES(leadVehicle,vehToAlter) < 100		
				SET_PLAYBACK_SPEED(vehToAlter,fNewSpeed)
			//	cprintln(debug_trevor3,"veh = ",native_to_int(vehToAlter)," fNewSpeed = ",fNewSpeed)
			//	ELSE
			//		fNewSpeed = 1.0
			//		SET_PLAYBACK_SPEED(vehToAlter,fNewSpeed)
			//	ENDIF
				*/
					
//		ENDIF
	ENDIF
	RETURN fNewSpeed
ENDFUNC

PROC GET_PLANE_TO_HEIGHT(vehicle_index vehToAlter, float fGetToHeight, bool withRotation, bool withVelocity)
	IF IS_VEHICLE_DRIVEABLE(vehToAlter)
		vector vRot
		vector VCoord
		float vel
		float currentPitch
		float interceptTime
		float heightOffset
		float targetPitch
		float interceptZspeed
		float targetZspeed
		
		
		
		vCoord = GET_ENTITY_COORDS(vehToAlter)
		heightOffset = (fGetToHeight - VCoord.z)
		
		IF withRotation
			vrot = GET_ENTITY_ROTATION(vehToAlter)
			currentPitch = vRot.x
			vel = GET_ENTITY_SPEED(vehToAlter)
			
			interceptZspeed = sin(currentPitch) * vel
			
			interceptTime = heightOffset / interceptZspeed
			
			IF interceptTime < 2.0			
				
			
				targetZspeed = heightOffset / 2
				targetPitch = ASIN(targetZspeed / vel)		
				
				if absf(targetPitch - vRot.x) > 30.0 * timestep()
					if targetPitch > vRot.x 
						targetPitch = vRot.x + 30.0 * timestep()
					else
						targetPitch = vRot.x - 30.0 * timestep()
					endif
				endif
				
				SET_ENTITY_ROTATION(vehToAlter,<<targetPitch+0.2,vrot.y,vrot.z>>)
				
			//	cprintln(debug_trevor3,"target pitch = ",targetPitch," actual pitch = ",currentPitch," vel = ",vel," interceptZspeed = ",interceptZspeed," height diff= ",heightOffset," interceptTime = ",interceptTime," targetZspeed = ",targetZspeed)
			ENDIF
		ENDIF
		
		IF withVelocity
			vector vPlayerSpeed
			vPlayerspeed = GET_ENTITY_VELOCITY(vehToAlter)
			SET_ENTITY_VELOCITY(vehToAlter,<<vPlayerspeed.x,vPlayerspeed.y,heightOffset>>)
		ENDIF
	ENDIF
ENDPROC

PROC FORCE_PLANE_VERT_ROT(vehicle_index vehToAlter)
	vector vRot
	
	IF IS_VEHICLE_DRIVEABLE(vehToAlter)
		vRot = GET_ENTITY_ROTATION(vehToAlter)
		IF GET_ENTITY_PITCH(vehToAlter) > 90.0
			vRot.x = 90.0
			SET_ENTITY_ROTATION(vehToAlter,vRot)
		ENDIF
		cprintln(debug_trevor3,"Pitch = ",vRot.x," ",GET_ENTITY_PITCH(vehToAlter))
	ENDIF
ENDPROC

PROC FORCE_PLAYER_VEH_HEIGHT(vehicle_index vehToAlter,vehicle_index vehToMatch, float offset = 0.0)

	IF IS_VEHICLE_DRIVEABLE(vehToAlter)
	AND IS_VEHICLE_DRIVEABLE(vehToMatch)

		//force player's vehicle to be correct height.
					
		vector vRot
		vector vPLayer,vLead
		float fVDist
		
		vRot = GET_ENTITY_ROTATION(vehToAlter)			
		vRot=vRot
		vLead = GET_ENTITY_COORDS(vehToMatch)
		vPlayer = GET_ENTITY_COORDS(vehToAlter)
			
		if vehToAlter = vehToMatch
			fVDist = offset-vLead.z
			cprintln(debug_trevor3,"fix height = ",fixHeight.z," fVDist = ",fVDist)
		else
			fVDist = vLead.z-vPlayer.z+offset
		endif
					
		vector vPlayerSpeed
		vPlayerspeed = GET_ENTITY_VELOCITY(vehToAlter)
					
		IF absi(GET_CONTROL_VALUE(player_control,INPUT_VEH_FLY_PITCH_UD) - 128) < 5
		and absf(vRot.x) < 5.0
		and absf(vRot.y) < 5.0
		and absf(fVDist) < 6.0
		and IS_ENTITY_IN_AIR(vehToAlter)
		and GET_ENTITY_SPEED(vehToAlter) > 15.0
		and GET_DISTANCE_BETWEEN_COORDS(vLead,vPlayer) < 150.0					
			
			//check if player isn't doing much to the 										
						
			float fRequiredZSpeed
						
			if absf(fVDist) < 3.0 fRequiredZSpeed = absf(fVdist) / 1.0 endif
			if absf(fVdist) >= 3.0 fRequiredZSpeed = absf(fVdist) / 1.0 endif
						
			if fVDist < 0.0 
				fRequiredZSpeed *= -1.0
			endif
						
						
			fLastDesiredSpeed = fRequiredZSpeed

			//vector vCoord
			//vCoord = GET_ENTITY_COORDS(vehToAlter)
					
			//vector vDiff = vCoord - vLastCoord
			//vLastCoord = vCoord
			//float zSpeed
			//zSpeed = vDiff.z / timestep()
											
			//cprintln(debug_trevor3,"Required Speed = ",fRequiredZSpeed," offset = ",fZOffset)
					
						
			IF fLastDesiredSpeed > vPlayerspeed.z fZOffset += 2.5*timestep() endif
			IF fLastDesiredSpeed < vPlayerspeed.z fZOffset -= 2.5*timestep() endif
						
			fRequiredZSpeed += fZOffset
		//	cprintln(debug_trevor3,"2) Required Speed = ",	fRequiredZSpeed," vPlayerspeed.z = ",vPlayerspeed.z)	
			
			IF fRequiredZSpeed - vPlayerspeed.z < -1.0 fRequiredZSpeed = vPlayerspeed.z - 1.0 endif
			IF fRequiredZSpeed - vPlayerspeed.z > 1.0 fRequiredZSpeed = vPlayerspeed.z + 1.0 endif	
		//	cprintln(debug_trevor3,"3) Required Speed = ",	fRequiredZSpeed)		
				//SET_ENTITY_HAS_GRAVITY(vehToAlter,false)
						
			vPlayerspeed = <<vPlayerspeed.x,vPlayerspeed.y,fRequiredZSpeed>>
						
				//	IF fVDist < -1.0 vPlayerspeed -= <<0,0,absf(fRequiredZSpeed)>> ENDIF
				//	IF fVDist > 1.0 vPlayerspeed += <<0,0,absf(fRequiredZSpeed)>> ENDIF
	
			SET_ENTITY_VELOCITY(vehToAlter,vPlayerSpeed)
		endif		
	endif
ENDPROC

FUNC VECTOR GET_PLANE_OFFSET(vector vCoord,float heading, vector offset)
	float fXa,fYa,fxb,fyb
	
	heading	*= -1.0
	
	fxa = cos(heading) * offset.x
	fya = sin(heading) * offset.x * -1.0
	fxb = sin(heading) * offset.y
	fyb = cos(heading) * offset.y
	

	
	RETURN vCoord+<<fxa+fxb,fya+fyb,offset.z>>
	
ENDFUNC

PROC doDebug()

	

	IF wBredoVeh
		
		IF wIveh = 0
			DRAW_DEBUG_SPHERE(vTakeoff,3.0,255,0,0,100)
			vTakeoff=vTakeoff
			DRAW_DEBUG_SPHERE(<<vFormationTarget[0].x,vFormationTarget[0].y,fTargetHeight[0]>>,5.0,255,0,0,100)
			vTakeoff=vTakeoff
			DRAW_DEBUG_LINE(<<vTakeoff.x,vTakeoff.y,fTargetHeight[0]>>,vFormationTarget[0],0,255,0,255)
			vTakeoff=vTakeoff
			DRAW_DEBUG_LINE(vFormationTarget[0],vFormationTarget[1],0,255,0,255)
			
			DRAW_DEBUG_SPHERE(vFormationTarget[1],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[2],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[3],5.0,255,0,0,100)
			DRAW_DEBUG_LINE(vFormationTarget[2],vFormationTarget[3],0,255,0,255)
			
			DRAW_DEBUG_SPHERE(vFormationTarget[4],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[5],5.0,255,0,0,100)
			DRAW_DEBUG_LINE(vFormationTarget[4],vFormationTarget[5],0,255,0,255)
			
			DRAW_DEBUG_SPHERE(vFormationTarget[6],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[7],5.0,255,0,0,100)
			DRAW_DEBUG_LINE(vFormationTarget[6],vFormationTarget[7],0,255,0,255)
			
			DRAW_DEBUG_LINE(vFormationTarget[2],vFormationTarget[5],0,255,0,255)
		ENDIF
		
		IF wIveh = 1
			DRAW_DEBUG_SPHERE(GET_PLANE_OFFSET(<<-1228.9534, -3205.4641, 12.9444>>,59.9,<<-10,-20,0>>),3.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(GET_PLANE_OFFSET(<<-1228.9534, -3205.4641, 85.0>>,59.9,<<-20,-20,0>>),3.0,255,0,0,100)
			
			vTakeoff=vTakeoff
			DRAW_DEBUG_SPHERE(<<vFormationTarget[0].x,vFormationTarget[0].y,fTargetHeight[0]>>,5.0,255,0,0,100)
			vTakeoff=vTakeoff
			DRAW_DEBUG_LINE(GET_PLANE_OFFSET(<<-1228.9534, -3205.4641, 85.0>>,59.9,<<-20,-20,0>>),vFormationTarget[0],0,255,0,255)
			vTakeoff=vTakeoff
			DRAW_DEBUG_LINE(vFormationTarget[1],vFormationTarget[2],0,255,0,255)
			
			DRAW_DEBUG_SPHERE(vFormationTarget[1],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[2],5.0,255,0,0,100)
			
			DRAW_DEBUG_SPHERE(vFormationTarget[3],5.0,255,0,0,100)
			
		
			DRAW_DEBUG_SPHERE(vFormationTarget[4],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[5],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[6],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[7],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[8],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[9],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[10],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[11],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[12],5.0,255,0,0,100)
			DRAW_DEBUG_LINE(vFormationTarget[3],vFormationTarget[4],0,255,0,255)
			DRAW_DEBUG_LINE(vFormationTarget[4],vFormationTarget[5],0,255,0,255)
			DRAW_DEBUG_LINE(vFormationTarget[5],vFormationTarget[6],0,255,0,255)
			DRAW_DEBUG_LINE(vFormationTarget[6],vFormationTarget[7],0,255,0,255)
			DRAW_DEBUG_LINE(vFormationTarget[7],vFormationTarget[8],0,255,0,255)
			DRAW_DEBUG_LINE(vFormationTarget[9],vFormationTarget[10],0,255,0,255)
			DRAW_DEBUG_LINE(vFormationTarget[11],vFormationTarget[12],0,255,0,255)
		ELIF wIveh = 2
		
		
			DRAW_DEBUG_SPHERE(GET_PLANE_OFFSET(<<-1228.9534, -3205.4641, 12.9444>>,59.9,<<10,-20,0>>),3.0,255,0,0,100)
			vTakeoff=vTakeoff
			
			DRAW_DEBUG_SPHERE(GET_PLANE_OFFSET(<<-1228.9534, -3205.4641, 85.0>>,59.9,<<20,-20,0>>),3.0,255,0,0,100)
			
			DRAW_DEBUG_SPHERE(<<vFormationTarget[0].x,vFormationTarget[0].y,fTargetHeight[0]>>,5.0,255,0,0,100)
			vTakeoff=vTakeoff
			DRAW_DEBUG_LINE(<<vTakeoff.x,vTakeoff.y,fTargetHeight[0]>>,vFormationTarget[0],0,255,0,255)
			vTakeoff=vTakeoff
			DRAW_DEBUG_LINE(vFormationTarget[1],vFormationTarget[2],0,255,0,255)
			
			DRAW_DEBUG_SPHERE(vFormationTarget[1],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[2],5.0,255,0,0,100)
			
			DRAW_DEBUG_SPHERE(vFormationTarget[3],5.0,255,0,0,100)
			
		
			DRAW_DEBUG_SPHERE(vFormationTarget[4],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[5],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[6],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[7],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[8],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[9],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[10],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[11],5.0,255,0,0,100)
			DRAW_DEBUG_SPHERE(vFormationTarget[12],5.0,255,0,0,100)
			DRAW_DEBUG_LINE(vFormationTarget[3],vFormationTarget[4],0,255,0,255)
			DRAW_DEBUG_LINE(vFormationTarget[4],vFormationTarget[5],0,255,0,255)
			DRAW_DEBUG_LINE(vFormationTarget[5],vFormationTarget[6],0,255,0,255)
			DRAW_DEBUG_LINE(vFormationTarget[6],vFormationTarget[7],0,255,0,255)
			DRAW_DEBUG_LINE(vFormationTarget[7],vFormationTarget[8],0,255,0,255)
			DRAW_DEBUG_LINE(vFormationTarget[9],vFormationTarget[10],0,255,0,255)
			DRAW_DEBUG_LINE(vFormationTarget[11],vFormationTarget[12],0,255,0,255)
		ENDIF
	
		SWITCH debugState
			CASE eDebugPending
				cprintln(debug_trevor3,"Start bollox")
				iF wIveh = 0
					cprintln(debug_trevor3,"iVeh = 0????")
					vTakeoff = <<-1228.9534, -3205.4641, 12.9444>>
					fTargetHeight[0] = 85.0
					vFormationTarget[0] = <<-2700.6768, -2358.4080, 85.0>>
					vFormationTarget[1] = <<-3352.5100, -1980.7301, 85>>
					vFormationTarget[2] = <<-3196.3120, -1198.5851, 85>>  //-3759.4558, -1192.6437, 75
					vFormationTarget[3] = <<226.0905, -1215.6815, 85>> //start loop
					vFormationTarget[4] = <<279.6717, -1216.0490, 60>> //end loop
					vFormationTarget[5] = <<923.0837, -1219.3074, 60>> //start vertical
					vFormationTarget[6] = <<915.7158, -1222.3358, 194.0>> 
					vFormationTarget[7] = <<87.5748, -1214.8563, 194.0>>
				ELIF wIveh = 1 
				
					vTakeoff = <<-1173.690430,-3250.503418,12.9444>>
					fTargetHeight[0] = 85.0
					vFormationTarget[0] = <<-2378.7422, -2543.9849, 85.0>>
					vFormationTarget[1] = <<-2378.7422, -2543.9849, 85.0>>
					vFormationTarget[2] = <<-3352.5100, -1980.7301, 85>>
			
					vFormationTarget[3] = <<-3196.3120, -1198.5851, 85>>  //-3759.4558, -1192.6437, 75
					vFormationTarget[4] = <<-3029.6733, -1199.4188, 85>>
					vFormationTarget[5] = <<-2770.8884, -1200.6942, 100>>
					
					vFormationTarget[6] = <<-1364.1387, -1207.7056, 100>> //change to square formation
					vFormationTarget[7] = <<-1168.3983,	-1208.6884, 85>>
					vFormationTarget[8] = <<226.0905, -1215.6815, 85>>
					
					vFormationTarget[9] = <<279.6717, -1216.0490, 60>> //end loop
					vFormationTarget[10] = <<923.0837, -1219.3074, 60>> //start vertical
					vFormationTarget[11] = <<915.7158, -1222.3358, 194.0>> 
					vFormationTarget[12] = <<87.5748, -1214.8563, 194.0>>
					
					float fHeading
				
					fHeading = GET_HEADING_FROM_COORDS(vFormationTarget[1],vFormationTarget[2])
					
					vFormationTarget[0] = GET_PLANE_OFFSET(vFormationTarget[0],fHeading,<<-20,-20,0>>)
					vTakeoff = GET_PLANE_OFFSET(vTakeoff,fHeading,<<-20,-20,0>>)
					
					fHeading = GET_HEADING_FROM_COORDS(vFormationTarget[7],vFormationTarget[8])
					
					vFormationTarget[7] = GET_PLANE_OFFSET(vFormationTarget[7],fHeading-90.0,<<0,-15,0>>)								
					vFormationTarget[8] = GET_PLANE_OFFSET(vFormationTarget[8],fHeading-90.0,<<0,-15,0>>)
					vFormationTarget[9] = GET_PLANE_OFFSET(vFormationTarget[9],fHeading-90.0,<<0,-15,0>>)
					vFormationTarget[10] = GET_PLANE_OFFSET(vFormationTarget[10],fHeading-90.0,<<0,-15,0>>)
					vFormationTarget[11] = GET_PLANE_OFFSET(vFormationTarget[11],fHeading-90.0,<<0,-15,0>>)
					vFormationTarget[12] = GET_PLANE_OFFSET(vFormationTarget[12],fHeading-90.0,<<0,-15,0>>)
				ELSE
					vTakeoff = <<-1228.9534, -3205.4641, 12.9444>>
					fTargetHeight[0] = 85.0
					vFormationTarget[0] = <<-2378.7422, -2543.9849, 85.0>>
					vFormationTarget[1] = <<-2378.7422, -2543.9849, 85.0>>
					vFormationTarget[2] = <<-3352.5100, -1980.7301, 85>>
			
					vFormationTarget[3] = <<-3196.3120, -1198.5851, 85>>  //-3759.4558, -1192.6437, 75
					vFormationTarget[4] = <<-3029.6733, -1199.4188, 85>>
					vFormationTarget[5] = <<-2770.8884, -1200.6942, 130>>
					
					vFormationTarget[6] = <<-1364.1387, -1207.7056, 130>> //change to square formation
					vFormationTarget[7] = <<-1168.3983,	-1208.6884, 85>>
					vFormationTarget[8] = <<226.0905, -1215.6815, 85>>
					
					vFormationTarget[9] = <<279.6717, -1216.0490, 60>> //end loop
					vFormationTarget[10] = <<923.0837, -1219.3074, 60>> //start vertical
					vFormationTarget[11] = <<915.7158, -1222.3358, 194.0>> 
					vFormationTarget[12] = <<87.5748, -1214.8563, 194.0>>
					
					float fHeading
				
					fHeading = GET_HEADING_FROM_COORDS(vFormationTarget[1],vFormationTarget[2])
					
					vFormationTarget[0] = GET_PLANE_OFFSET(vFormationTarget[0],fHeading,<<20,-20,0>>)
					vTakeoff = GET_PLANE_OFFSET(vTakeoff,fHeading,<<20,-20,0>>)
					
					fHeading = GET_HEADING_FROM_COORDS(vFormationTarget[7],vFormationTarget[8])
					
				//	vFormationTarget[7] = GET_PLANE_OFFSET(vFormationTarget[7],fHeading,<<0,0,0>>)								
				//	vFormationTarget[8] = GET_PLANE_OFFSET(vFormationTarget[8],fHeading,<<-7.5,0,15>>)
				//	vFormationTarget[9] = GET_PLANE_OFFSET(vFormationTarget[9],fHeading,<<-7.5,0,15>>)
				//	vFormationTarget[10] = GET_PLANE_OFFSET(vFormationTarget[10],fHeading,<<-7.5,0,15>>)
				//	vFormationTarget[11] = GET_PLANE_OFFSET(vFormationTarget[11],fHeading,<<-7.5,0,15>>)
				//	vFormationTarget[12] = GET_PLANE_OFFSET(vFormationTarget[12],fHeading,<<-7.5,0,15>>)
				endif
	
				resetMission()
				
				debugState = eDebugWaitForVeh
			BREAK
			CASE eDebugWaitForVeh
				IF DOES_ENTITY_EXIST(vehLazer[wIveh])
					IF IS_VEHICLE_DRIVEABLE(vehLazer[wIveh])
						SET_PED_INTO_VEHICLE(player_ped_id(),vehLazer[wIveh])
						debugState = eDebugWaitForPlayerOverride
					ENDIF
				ENDIF
			BREAK
			CASE eDebugWaitForPlayerOverride
		
				IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer[wIveh])
					IF NOT bWidgetMade
						SET_CURRENT_WIDGET_GROUP(wRecord)
						float fDuration								
						int iRec
						iRec = 120+wIveh
						cprintln(debug_trevor3,"rec: ",wIveh)
						fDuration = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(iRec,"pilotSchool")
						//SWITCH aVeh
							//CASE aVeh
							//iDuration = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(121,"pilotSchool")
							//BREAK
						//	CASE vehLazer[1] iDuration = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(122,"pilotSchool") BREAK
						//	CASE vehLazer[2] iDuration = GET_TOTAL_DURATION_OF_VEHICLE_RECORDING(123,"pilotSchool") BREAK
						//ENDSWITCH
						bWidgetMade = true
						
						ADD_WIDGET_FLOAT_SLIDER("Rec Time",wFRecTime,0,fDuration,0.1)
						lastRecTime = 0
						CLEAR_CURRENT_WIDGET_GROUP(wRecord)							
					ENDIF
					
					IF wFRecTime != lastRecTime
						lastRecTime = GET_TIME_POSITION_IN_RECORDING(vehLazer[wIveh])
						SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehLazer[wIveh],wFRecTime-lastRecTime)						
						IF wIveh != 0 SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehLazer[0],wFRecTime-lastRecTime) ENDIF
						IF wIveh != 1 SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehLazer[1],wFRecTime-lastRecTime) ENDIF
						IF wIveh != 2 SKIP_TIME_IN_PLAYBACK_RECORDED_VEHICLE(vehLazer[2],wFRecTime-lastRecTime) ENDIF
						lastRecTime = wFRecTime
					ELSE
						wFRecTime = GET_TIME_POSITION_IN_RECORDING(vehLazer[wIveh])
						lastRecTime = wFRecTime
					ENDIF										
				ENDIF													
							
				IF absi(GET_CONTROL_VALUE(player_control,INPUT_VEH_FLY_PITCH_UD) - 128) > 5
				OR absi(GET_CONTROL_VALUE(player_control,INPUT_VEH_FLY_ROLL_LR) - 128) > 5
				OR IS_CONTROL_PRESSED(player_control,INPUT_VEH_FLY_YAW_RIGHT)
				OR IS_CONTROL_PRESSED(player_control,INPUT_VEH_FLY_YAW_LEFT)
				OR IS_CONTROL_PRESSED(player_control,INPUT_VEH_FLY_THROTTLE_UP)
					IF IS_VEHICLE_DRIVEABLE(vehLazer[wIveh])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer[wIveh])
							START_RECORDING_VEHICLE_TRANSITION_FROM_PLAYBACK(vehLazer[wIveh],999,"pilotSchool",true)
							debugState = eDebugOverridingRecording
						ENDIF
					ENDIF										
				ENDIF
			BREAK		
			
			CASE eDebugOverridingRecording
				IF IS_VEHICLE_DRIVEABLE(vehLazer[wIveh])															
					IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL,INPUT_FRONTEND_X)
						IF IS_RECORDING_GOING_ON_FOR_VEHICLE(vehLazer[wIveh])
							STOP_RECORDING_ALL_VEHICLES()
							debugState = eDebugPending
							wBredoVeh = false
						ENDIF
					ENDIF
				ENDIF
			BREAK
		ENDSWITCH		
	ELSE
		IF debugState != eDebugPending
			IF IS_RECORDING_GOING_ON_FOR_VEHICLE(vehLazer[wIveh])
				STOP_RECORDING_ALL_VEHICLES()
			ENDIF
			debugState = eDebugPending
			bWidgetMade = false
		ENDIF
	ENDIF
ENDPROC

SCRIPT
	SET_MISSION_FLAG(TRUE)
	ADD_SCENARIO_BLOCKING_AREA( -<<10000,100000,10000>>,<<10000,100000,10000>>)
	resetMission()
	
	cprintln(debug_Trevor3,"Launch B")
	
	
	
		
	/*	
	REQUEST_ANIM_DICT("missarmenian2")
	REQUEST_MODEL(A_M_M_BEACH_01)
	REQUEST_MODEL(A_M_Y_GENSTREET_02)
	REQUEST_WAYPOINT_RECORDING("arm2_20")
	while not HAS_ANIM_DICT_LOADED("missarmenian2")
	or not HAS_MODEL_LOADED(A_M_M_BEACH_01)
	or not HAS_MODEL_LOADED(A_M_Y_GENSTREET_02)
	or not GET_IS_WAYPOINT_RECORDING_LOADED("arm2_20")
		WAIT(0)
	ENDWHILE
	
	if not IS_PED_INJURED(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -1122.7813, -1598.0078, 3.3921 >>)
		SET_ENTITY_HEADING(PLAYER_PED_ID(),217)
	ENDIF
	
	//pedLamar = CREATE_PED(PEDTYPE_MISSION,A_M_M_BEACH_01,<< -1124.9785, -1597.2457, 3.3594 >>,212)
	//pedDrunk = CREATE_PED(PEDTYPE_MISSION,A_M_M_BEACH_01,<< -1105.9553, -1600.7543, 3.6621 >>, 148.1510)
	

	
	
	vDrunk = << -1113.1440, -1606.1331, 3.5971 >>
	hDrunk = -150
	vLamar = << -1126.9425, -1595.3152, 3.3120 >>
	vLamarTarget = << -1116.4960, -1602.7670, 3.3594 >>
	hLamar = 221.5007
	
	thisWidget = START_WIDGET_GROUP("ARM2 anim test")
		ADD_WIDGET_BOOL("Start",bPlay)		
		ADD_WIDGET_BOOL("set to player pos",bsetDrunkStartToPlayer)
		ADD_WIDGET_VECTOR_SLIDER("Lamar start",vLamar,-1700.0,5.0,0.01)
		ADD_WIDGET_FLOAT_SLIDER("Lamar heading",hLamar,-360.0,360.0,0.01)
		ADD_WIDGET_VECTOR_SLIDER("Drunk start",vDrunk,-1700.0,5.0,0.01)
		ADD_WIDGET_FLOAT_SLIDER("Drunk heading",hDrunk,-360.0,360.0,0.01)		
	STOP_WIDGET_GROUP()
*/
	WHILE (TRUE)
	
		TEXT_LABEL_7 txtLbl
		float fBlah
		
		SET_MAX_WANTED_LEVEL(0)
		SET_WANTED_LEVEL_MULTIPLIER(0.0)
		SET_PLAYER_WANTED_LEVEL(player_id(),0)
		SET_PLAYER_WANTED_LEVEL_NOW(player_id())
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SET_RANDOM_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		
		
	//	SUPPRESS_RESTRICTED_AREA_WANTED_LEVEL(AC_AIRPORT_AIRSIDE,true)
		
		IF (IS_KEYBOARD_KEY_JUST_PRESSED(KEY_F))
			cleanup()
			TERMINATE_THIS_THREAD()
		ENDIF
		
		doDebug()

			SWITCH eProgress
				CASE COUNTDOWN							
					txtLbl = ""								
					fBlah = ((iStartTime - GET_GAME_TIMER()) / 1000.0)
					txtLbl += ceil(fBlah)
					fixHeight.z = 0
				//	PRINT_STRING_WITH_LITERAL_STRING("STRING",txtLbl,10,1)
					DRAW_DEBUG_TEXT_2D(txtLbl,<<0.5,0.8,0>>)
					IF GET_GAME_TIMER() > iStartTime
						iStartTime = 0
						IF IS_VEHICLE_DRIVEABLE(vehLazer[0])
							START_PLAYBACK_RECORDED_VEHICLE(vehLazer[0],120,"pilotSchool")
						ENDIF
						IF IS_VEHICLE_DRIVEABLE(vehLazer[1])
							START_PLAYBACK_RECORDED_VEHICLE(vehLazer[1],121,"pilotSchool")						
						ENDIf
						IF IS_VEHICLE_DRIVEABLE(vehLazer[2])
							START_PLAYBACK_RECORDED_VEHICLE(vehLazer[2],122,"pilotSchool")
						ENDIf
						//START_RECORDING_VEHICLE(vehLazer[1],121,"pilotSchool",true)
						eProgress = FLYING
					ENDIF
				BREAK
				CASE FLYING
					IF IS_VEHICLE_DRIVEABLE(vehLazer[0])
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer[0])
							txtLbl = ""
							fBlah = GET_TIME_POSITION_IN_RECORDING(vehLazer[0]) / 1000.0
							txtlbl += ceil(fBlah)
							DRAW_DEBUG_TEXT_2D(txtLbl,<<0.5,0.8,0>>)							
							
							txtLbl = ""
							fBlah = fRecSpeed[0]
							txtlbl += floor(fBlah)
							txtlbl += "."
							txtlbl += floor((fBlah - floor(fBlah)) * 100.0)
							DRAW_DEBUG_TEXT_2D(txtLbl,<<0.3,0.9,0>>)
							
						else
							IF IS_RECORDING_GOING_ON_FOR_VEHICLE(vehplayer)
								cprintln(debug_Trevor3,"Recording finished")
								STOP_RECORDING_ALL_VEHICLES()
								eProgress = ENDED
							ENDIF
						ENDIF
						
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer[1])
							txtLbl = ""
							fBlah = fRecSpeed[1]
							txtlbl += floor(fBlah)
							txtlbl += "."
							txtlbl += floor((fBlah - floor(fBlah)) * 100.0)
							DRAW_DEBUG_TEXT_2D(txtLbl,<<0.5,0.9,0>>)
						ENDIF
						IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer[2])
							txtLbl = ""
							fBlah = fRecSpeed[2]
							txtlbl += floor(fBlah)
							txtlbl += "."
							txtlbl += floor((fBlah - floor(fBlah)) * 100.0)
							DRAW_DEBUG_TEXT_2D(txtLbl,<<0.7,0.9,0>>)
						ENDIF
					ELSE
						IF IS_RECORDING_GOING_ON_FOR_VEHICLE(vehplayer)
							cprintln(debug_Trevor3,"Recording finished b")
							STOP_RECORDING_ALL_VEHICLES()
							eProgress = ENDED
						ENDIF
					ENDIF
				BREAK
			ENDSWITCH
			
			
			//make sure second lazer is in line with first
			
			
			
			
		
		
			if wBredoVeh
			//	switch wIveh
			//		CASE 0
						fRecSpeed[0] = 1.0
					//	SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehPlayer,vehLazer[0],<<0,-60,0>>)
					//	fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-20,-30,0>>,fRecSpeed[0])
					//	fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<20,-30,0>>,fRecSpeed[0])
						switch wIveh
							CASE 0
								SWITCH checkPointStage
									CASE 0			
									/*
										IF IS_VEHICLE_DRIVEABLE(vehLazer[0])
											IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer[0])
												STOP_PLAYBACK_RECORDED_VEHICLE(vehLazer[0])
												CPRINTLN(Debug_TREVOR3,"SKIP")
												checkPointStage =2
												SET_ENTITY_COORDS(vehLazer[0],<<-246.5569, -1209.7578, 85.0048>>)
												SET_ENTITY_HEADING(vehLazer[0],270)
												SET_VEHICLE_FORWARD_SPEED(vehLazer[0],50.0)
												SET_GAMEPLAY_CAM_FOLLOW_PED_THIS_UPDATE(player_ped_id())
											ENDIF																
										ENDIF*/
								
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],fTargetHeight[0],TRUE,FALSE)	
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[2]) < 5.0
											checkPointStage++
										ENDIF
									BREAK		
									CASE 1
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],fTargetHeight[0],FALSE,TRUE)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[3]) < 15.0
											checkPointStage++
										ENDIF
									BREAK
									CASE 2
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],fTargetHeight[0],TRUE,FALSE)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[3]) < 5.0
											checkPointStage++
										ENDIF
									BREAK
									CASE 3
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[4]) < 10.0
											checkPointStage++
										ENDIF
									BREAK
									CASE 4
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],vFormationTarget[4].z,TRUE,FALSE)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[5]) < 10.0
											checkPointStage++
										ENDIF
									BREAK
									CASE 5
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[6]) < 10.0 //in vertical
											checkPointStage++
										ENDIF
									BREAK
									CASE 6
										//FORCE_PLANE_VERT_ROT(vehLazer[wIveh])
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],vFormationTarget[7].z,TRUE,FALSE)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[7]) < 10.0 //in vertical
											checkPointStage++
										ENDIF
									BREAK
								ENDSWITCH	
							BREAK
							CASE 1
								IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer[2])
									SET_PLAYBACK_SPEED(vehLazer[2],0.01)
								ENDIF
								cprintln(debug_trevor3,"checkPointStage = ",checkPointStage)
								SWITCH checkPointStage
									CASE 0									
								
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],fTargetHeight[0],TRUE,FALSE)	
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[2]) < 5.0
											checkPointStage++
										ENDIF
									BREAK		
									CASE 1
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],fTargetHeight[0],FALSE,TRUE)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[3]) < 15.0
											checkPointStage++
										ENDIF
									BREAK
									CASE 2
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],fTargetHeight[0],TRUE,FALSE)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[4]) < 15.0
											checkPointStage++
										ENDIF
									BREAK
									CASE 3
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],100.0,TRUE,FALSE)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[6]) < 10.0
											checkPointStage++
										ENDIF
									BREAK
									CASE 4
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],vFormationTarget[7].z,TRUE,FALSE)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[8]) < 10.0
											checkPointStage++
										ENDIF
									BREAK
									CASE 5
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[9]) < 10.0
											checkPointStage++
										ENDIF
									BREAK
									CASE 6
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],vFormationTarget[10].z,TRUE,FALSE)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[10]) < 10.0
											checkPointStage++
										ENDIF
									BREAK
									CASE 7
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[11]) < 10.0 
											checkPointStage++
										ENDIF
									BREAK
									CASE 8
										//FORCE_PLANE_VERT_ROT(vehLazer[wIveh])
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],vFormationTarget[12].z,TRUE,FALSE)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[12]) < 10.0 
											checkPointStage++
										ENDIF
									BREAK
									
								ENDSWITCH
							BREAK
							CASE 2
								
								
								SWITCH checkPointStage
									CASE 0									
								
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],fTargetHeight[0],TRUE,FALSE)	
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[2]) < 5.0
											checkPointStage++
										ENDIF
									BREAK		
									CASE 1
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],fTargetHeight[0],FALSE,TRUE)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[3]) < 15.0
											checkPointStage++
										ENDIF
									BREAK
									CASE 2
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],fTargetHeight[0],TRUE,FALSE)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[4]) < 15.0
											checkPointStage++
										ENDIF
									BREAK
									CASE 3
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],130.0,TRUE,FALSE)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[6]) < 10.0
											checkPointStage++
										ENDIF
									BREAK
									CASE 4
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],vFormationTarget[7].z,TRUE,FALSE)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[8]) < 10.0
											checkPointStage++
										ENDIF
									BREAK
									CASE 5
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[9]) < 10.0
											checkPointStage++
										ENDIF
									BREAK
									CASE 6
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],vFormationTarget[10].z,TRUE,FALSE)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[10]) < 10.0
											checkPointStage++
										ENDIF
									BREAK
									CASE 7
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[11]) < 10.0 
											checkPointStage++
										ENDIF
									BREAK
									CASE 8
										//FORCE_PLANE_VERT_ROT(vehLazer[wIveh])
										GET_PLANE_TO_HEIGHT(vehLazer[wIveh],vFormationTarget[12].z,TRUE,FALSE)
										IF GET_DISTANCE_BETWEEN_ENTITY_AND_COORD(vehLazer[wIveh],vFormationTarget[12]) < 10.0 
											checkPointStage++
										ENDIF
									BREAK
									
								ENDSWITCH
							
								IF IS_VEHICLE_DRIVEABLE(vehLazer[0])
									IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer[0])	
										TEXT_LABEL_15 txtlblB
										txtlblB = ""
										txtlblB += FLOOR(GET_TIME_POSITION_IN_RECORDING(vehLazer[0]))
										DRAW_DEBUG_TEXT(txtlblB,GET_ENTITY_COORDS(vehLazer[0]) + <<0,0,2>>)
										fBlah = GET_TIME_POSITION_IN_RECORDING(vehLazer[0])
										if fBlah < 31780																				
											fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-20,-30,0>>,fRecSpeed[0])										
										elif fBlah < 61000 																				
											fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<0,-30,0>>,fRecSpeed[0],1.0)
										elif fBlah < 84000
											fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<0,0,20>>,fRecSpeed[0],1.0)
										elif fBlah < 84000 																				
											fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-20,0,0>>,fRecSpeed[0],1.0)
										elif fBlah < 113000
											fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[0],vehLazer[0],<<-20,0,0>>,1.0,1.0,1.0)
											fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-20,0,0>>,1.0,1.0,1.0)
										elif fBlah < 123000 																				
											fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-20,0,0>>,fRecSpeed[0],1.0)
										elif fBlah < 127000
											fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[0],vehLazer[0],<<-20,0,0>>,1.0,1.0,1.0)
											fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-20,0,0>>,1.0,1.0,1.0)
										else
											fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<20,0,0>>,fRecSpeed[0],1.0)
										endif
									endif
								endif
							BREAK
						ENDSWITCH
				/*	BREAK
					CASE 1
						IF IS_VEHICLE_DRIVEABLE(vehLazer[0])
							IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer[0])							
					/*			fBlah = GET_TIME_POSITION_IN_RECORDING(vehLazer[0])
								if fBlah < 25000
									fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[0],<<-20,-30,0>>)
									//SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-20,-30,0>>,fRecSpeed[0])
									fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<20,-30,0>>,fRecSpeed[0])
									FORCE_PLAYER_VEH_HEIGHT(vehLazer[1],vehLazer[0],0)
								elIf fBlah < 40000
									fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[0],<<20,0,-15>>,1,1.0,1.0)
									//SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-20,-30,0>>,fRecSpeed[0])
									fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<20,-30,0>>,fRecSpeed[0])
									FORCE_PLAYER_VEH_HEIGHT(vehLazer[1],vehLazer[0],-15)
								
								elif fBlah < 68000
									fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[0],<<20,0,-15>>)
									//SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-20,-30,0>>,fRecSpeed[0])
									fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<20,-30,0>>,fRecSpeed[0])
									FORCE_PLAYER_VEH_HEIGHT(vehLazer[1],vehLazer[0],-15)
								else
									fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[0],<<0,-30,0>>,1,0.8,1.3)
									//SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-20,-30,0>>,fRecSpeed[0])
									fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<0,-60,0>>,fRecSpeed[0])
									FORCE_PLAYER_VEH_HEIGHT(vehLazer[1],vehLazer[0],0)
								endif
							else
								if fixHeight.z = 0 
									
									FORCE_PLAYER_VEH_HEIGHT(vehLazer[1],vehLazer[0],fixHeight.z)
								endif
							endif
						endif
							
							
						
					BREAK
					
				endswitch*/
			else
				
				IF IS_VEHICLE_DRIVEABLE(vehLazer[0])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer[0])	
						TEXT_LABEL_15 txtlblB
						txtlblB = ""
						txtlblB += FLOOR(GET_TIME_POSITION_IN_RECORDING(vehLazer[0]))
						DRAW_DEBUG_TEXT(txtlblB,GET_ENTITY_COORDS(vehLazer[0]) + <<0,0,2>>)
						fBlah = GET_TIME_POSITION_IN_RECORDING(vehLazer[0])
						if fBlah < 29000				
							fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehPlayer,vehLazer[0],<<0,-60,0>>)
							fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-20,-30,0>>,fRecSpeed[0])										
							fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<20,-30,0>>,fRecSpeed[0])	
							FORCE_PLAYER_VEH_HEIGHT(vehPlayer,vehLazer[0])
						elif fBlah < 34000				
							fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehPlayer,vehLazer[0],<<0,-120,0>>)
							fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-20,-90,0>>,fRecSpeed[0])										
							fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<20,-90,0>>,fRecSpeed[0])	
							FORCE_PLAYER_VEH_HEIGHT(vehPlayer,vehLazer[0])
						elif fBlah < 61000 			
							fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehPlayer,vehLazer[0],<<0,-90,0>>)
							fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<0,-30,0>>,fRecSpeed[0])
							fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<0,-60,0>>,fRecSpeed[0])
							FORCE_PLAYER_VEH_HEIGHT(vehPlayer,vehLazer[0])
						elif fBlah < 84000
							fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehPlayer,vehLazer[0],<<0,-25,30>>)
							fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<0,-10,15>>,fRecSpeed[0])
							fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<0,-30,45>>,fRecSpeed[0])						
							FORCE_PLAYER_VEH_HEIGHT(vehPlayer,vehLazer[0],30.0)
						elif fBlah < 87000
							fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehPlayer,vehLazer[0],<<15,-30,00>>,1.15,1.15,1.15)
							fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-15,-30,0>>,1.15,1.15,1.15)
							fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<0,-20,0>>,1.15,1.15,1.15)
						elif fBlah < 107000
							fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehPlayer,vehLazer[0],<<15,-30,0>>)
							fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-15,-30,0>>,fRecSpeed[0])
							fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<0,-20,0>>,fRecSpeed[0])
							FORCE_PLAYER_VEH_HEIGHT(vehPlayer,vehLazer[0])
						elif fBlah < 113000
							fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehPlayer,vehLazer[0],<<15,-30,0>>,1.0,1.0,1.0)
							fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-15,-30,0>>,1.0,1.0,1.0)
							fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<0,-20,0>>,1.0,1.0,1.0)
						elif fBlah < 123000
							fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehPlayer,vehLazer[0],<<15,-30,0>>)
							fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-15,-30,0>>,fRecSpeed[0])
							fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<0,-20,0>>,fRecSpeed[0])
							FORCE_PLAYER_VEH_HEIGHT(vehPlayer,vehLazer[0])
						elif fBlah < 127000
							fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehPlayer,vehLazer[0],<<15,-30,0>>,1.0,1.0,1.0)
							fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-15,-30,0>>,1.0,1.0,1.0)
							fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<0,-20,0>>,1.0,1.0,1.0)
						else
							fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehPlayer,vehLazer[0],<<15,-30,0>>)
							fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-15,-30,0>>,fRecSpeed[0])
							fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<0,-20,0>>,fRecSpeed[0])
							FORCE_PLAYER_VEH_HEIGHT(vehPlayer,vehLazer[0])
						endif
					endif
				endif
			/*
				IF IS_VEHICLE_DRIVEABLE(vehLazer[0])
					IF IS_PLAYBACK_GOING_ON_FOR_VEHICLE(vehLazer[0])							
						fBlah = GET_TIME_POSITION_IN_RECORDING(vehLazer[0])
						If fBlah < 25000
					//		cprintln(debug_trevor3,"STARTIT")
							fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehPlayer,vehLazer[0],<<0,-60,0>>)
							fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-20,-30,0>>,fRecSpeed[0])
							fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<20,-30,0>>,fRecSpeed[0])
							FORCE_PLAYER_VEH_HEIGHT(vehPlayer,vehLazer[0])
						elif fBlah < 34000
						//	cprintln(debug_trevor3,"STARTIT 2")
							fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehPlayer,vehLazer[0],<<0,-60,0>>,1,1,1)
							fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<-20,-30,0>>,fRecSpeed[0],1,1)
							fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<20,-30,0>>,fRecSpeed[0],1,1)
						
						elif fBlah < 68000
						//	cprintln(debug_trevor3,"STARTIT 3")
							fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehPlayer,vehLazer[0],<<0,-20,-15>>,1,0.7)
							fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<20,0,-15>>,fRecSpeed[0],0.6,1.5)
							fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<-20,0,-15>>,fRecSpeed[0],0.6,1.5)
							FORCE_PLAYER_VEH_HEIGHT(vehPlayer,vehLazer[0],-15)
						elif fBlah < 74000
							fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehPlayer,vehLazer[0],<<0,-90,0>>,1,1.2,1.3)
							fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<0,-60,0>>,fRecSpeed[0],1,1.1)
							fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<0,-30,0>>,fRecSpeed[0],1,1.1)
							FORCE_PLAYER_VEH_HEIGHT(vehPlayer,vehLazer[0],0)
						else
							fRecSpeed[0] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehPlayer,vehLazer[0],<<0,-110,0>>,1,0.8,1.4)
							fRecSpeed[1] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[1],vehLazer[1],<<0,-60,0>>,fRecSpeed[0],0.8,1.4)
							fRecSpeed[2] = SET_VEHICLE_RECORDING_ADJUSTED_BY_ENTITY(vehLazer[0],vehLazer[2],vehLazer[2],<<0,-30,0>>,fRecSpeed[0],0.8,1.4)
							FORCE_PLAYER_VEH_HEIGHT(vehPlayer,vehLazer[0],0)
						endif
					endif
				endif*/
			endif
			
		
	
	
	
		WAIT(0)
	endwhile

ENDSCRIPT

#ENDIF

#IF IS_FINAL_BUILD
	SCRIPT 
	ENDSCRIPT
#ENDIF
