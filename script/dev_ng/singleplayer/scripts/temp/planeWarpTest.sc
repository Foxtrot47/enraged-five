

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_streaming.sch"
USING "player_ped_public.sch"

VEHICLE_INDEX vehPlane
VECTOR vCoords[2]
FLOAT fHeading[2]

#IF IS_DEBUG_BUILD
	INT iCurrentPos = 0
#ENDIF


PROC Cleanup()
	PRINTLN("PlaneWarpTest cleaning up.")
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		CLEAR_PED_TASKS_IMMEDIATELY(PLAYER_PED_ID())
	ENDIF
	IF DOES_ENTITY_EXIST(vehPlane)
		DELETE_VEHICLE(vehPlane)
	ENDIF
	SET_MODEL_AS_NO_LONGER_NEEDED(CUBAN800)
	
	TERMINATE_THIS_THREAD()
ENDPROC


SCRIPT
	PRINTLN("PlaneWarpTest script started.")
	
	IF HAS_FORCE_CLEANUP_OCCURRED()
		PRINTLN("PlaneWarpTest forced to clean up.")
		Cleanup()
	ENDIF

	SET_MISSION_FLAG(TRUE)

	REQUEST_MODEL(CUBAN800)
	WHILE NOT HAS_MODEL_LOADED(CUBAN800)
		WAIT(0)
	ENDWHILE
	
	vCoords[0] = <<1169.9758, 3592.5715, 32.6481>>
	vCoords[1] = <<1215.7380, 3586.6077, 33.5131>>
	fHeading[0] = 277.7043
	fHeading[1] = 77.1113

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_ENTITY_COORDS(PLAYER_PED_ID(), <<1220.2024, 3596.2805, 33.2590>>)
	ENDIF
	
	LOAD_SCENE(<<1220.2024, 3596.2805, 33.2590>>)

	vehPlane = CREATE_VEHICLE(CUBAN800, vCoords[0], fHeading[0])
	SET_VEHICLE_ON_GROUND_PROPERLY(vehPlane)
	SET_MODEL_AS_NO_LONGER_NEEDED(CUBAN800)
	
	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
		SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), vehPlane, VS_DRIVER)
	ENDIF
	
	WHILE TRUE
	
		#IF IS_DEBUG_BUILD
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_S, KEYBOARD_MODIFIER_NONE, "Exit test")
			OR IS_DEBUG_KEY_JUST_PRESSED(KEY_F, KEYBOARD_MODIFIER_NONE, "Exit test")
				PRINTLN("PlaneWarpTest ending due to key press.")
				Cleanup()
			ENDIF
			
			IF IS_DEBUG_KEY_JUST_PRESSED(KEY_PERIOD, KEYBOARD_MODIFIER_NONE, "Warp Plane")
				PRINTLN("PlaneWarpTest warping plane position.")
				IF DOES_ENTITY_EXIST(vehPlane)
					IF IS_VEHICLE_DRIVEABLE(vehPlane)
						iCurrentPos++
						IF iCurrentPos > 1
							iCurrentPos = 0
						ENDIF
						PRINTLN("Using position # ", iCurrentPos, ".")
						SET_ENTITY_COORDS(vehPlane, vCoords[iCurrentPos])
						SET_ENTITY_HEADING(vehPlane, fHeading[iCurrentPos])
						SET_VEHICLE_ON_GROUND_PROPERLY(vehPlane)
					ENDIF
				ENDIF
			ENDIF
		#ENDIF
		#IF IS_FINAL_BUILD
			Cleanup()
		#ENDIF
	
		WAIT(0)
	ENDWHILE
	
ENDSCRIPT
