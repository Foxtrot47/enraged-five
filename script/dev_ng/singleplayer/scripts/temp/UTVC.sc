

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


USING "rage_builtins.sch"
USING "globals.sch"
USING "commands_camera.sch"
USING "commands_pad.sch"
USING "commands_script.sch"
USING "commands_vehicle.sch"
USING "commands_path.sch"
USING "flow_public_core_override.sch"
USING "commands_ped.sch"
USING "commands_task.sch"
USING "selector_public.sch"
using "dialogue_public.sch"
USING "commands_audio.sch"
USING "drunk_public.sch"
USING "beam_effect.sch"
USING "CompletionPercentage_public.sch"
USING "commands_graphics.sch"

//using "cutscene_builder.sch"
USING "replay_public.sch"
using "ped_component_public.sch"
USING "script_blips.sch"
USING "script_ped.sch"
USING "locates_public.sch"
USING "player_ped_public.sch"

#if IS_DEBUG_BUILD

WIDGET_GROUP_ID mainWidget

// Basic widget struct
STRUCT sWidget
	WIDGET_GROUP_ID groupID
	BOOL			bToggle
ENDSTRUCT

sWidget	widget_UNDER_INT_1_P2
sWidget	widget_UNDER_INT_2
sWidget	widget_UNDER_INT_2_p2
sWidget	widget_UNDER_INT_3
sWidget	widget_UNDER_INT_4_p2
sWidget	widget_UNDER_INT_4_p3
sWidget	widget_UNDER_INT_5
sWidget	widget_UNDER_INT_5_P2
sWidget	widget_UNDER_INT_5_P2_a
sWidget	widget_UNDER_INT_6
sWidget	widget_UNDER_INT_6_P2
sWidget	widget_UNDER_INT_7

PROC SETUP_WIDGETS()

	mainWidget = START_WIDGET_GROUP("Underbelly TV show capture")
	ADD_WIDGET_BOOL("Play UNDER_INT_1_P2", widget_UNDER_INT_1_P2.bToggle)
	ADD_WIDGET_BOOL("Play UNDER_INT_2", widget_UNDER_INT_2.bToggle)
	ADD_WIDGET_BOOL("Play UNDER_INT_2_p2", widget_UNDER_INT_2_p2.bToggle)
	ADD_WIDGET_BOOL("Play UNDER_INT_3", widget_UNDER_INT_3.bToggle)
	ADD_WIDGET_BOOL("Play UNDER_INT_4_p2", widget_UNDER_INT_4_p2.bToggle)
	ADD_WIDGET_BOOL("Play UNDER_INT_4_p3", widget_UNDER_INT_4_p3.bToggle)
	ADD_WIDGET_BOOL("Play UNDER_INT_5", widget_UNDER_INT_5.bToggle)
	ADD_WIDGET_BOOL("Play UNDER_INT_5_P2", widget_UNDER_INT_5_P2.bToggle)
	ADD_WIDGET_BOOL("Play UNDER_INT_5_P2 1pm time", widget_UNDER_INT_5_P2_a.bToggle)
	ADD_WIDGET_BOOL("Play UNDER_INT_6", widget_UNDER_INT_6.bToggle)
	ADD_WIDGET_BOOL("Play UNDER_INT_6_P2", widget_UNDER_INT_6_P2.bToggle)
	ADD_WIDGET_BOOL("Play UNDER_INT_7", widget_UNDER_INT_7.bToggle)
	STOP_WIDGET_GROUP()
	
ENDPROC


PROC MISSION_CLEANUP

	IF IS_CUTSCENE_ACTIVE()
		STOP_CUTSCENE_IMMEDIATELY()
	ENDIF
	
	IF DOES_WIDGET_GROUP_EXIST(mainWidget)
		DELETE_WIDGET_GROUP(mainWidget)
	ENDIF
	
	TERMINATE_THIS_THREAD()

ENDPROC

PROC PLAY_CUTSCENE( STRING sCutscene, VECTOR vPlayerPos, VECTOR vLoadScenePos, VECTOR vLoadSceneRot, INT iTime, STRING sWeather)

	IF NOT IS_ENTITY_DEAD(PLAYER_PED_ID())
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vPlayerPos)
	ENDIF
	
	SET_WEATHER_TYPE_NOW_PERSIST( sWeather )

	IF NOT IS_NEW_LOAD_SCENE_ACTIVE()
		NEW_LOAD_SCENE_START( vLoadScenePos, vLoadSceneRot, 100, NEWLOADSCENE_FLAG_LONGSWITCH_CUTSCENE )
	ENDIF
	
	WHILE NOT IS_NEW_LOAD_SCENE_LOADED()
		WAIT (0)
	ENDWHILE
	
	REQUEST_CUTSCENE( sCutscene )
	
	WHILE NOT HAS_CUTSCENE_LOADED()
		WAIT (0)
	ENDWHILE
		
	SET_CLOCK_TIME( iTime, 00, 00 )
	
	START_CUTSCENE()
	SET_CUTSCENE_CAN_BE_SKIPPED(TRUE)
	WHILE IS_CUTSCENE_PLAYING()
		WAIT(0)
	ENDWHILE
	
	WAIT(0)

ENDPROC

SCRIPT
	
	SET_MISSION_FLAG(TRUE)
	
	IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP|DEFAULT_FORCE_CLEANUP_FLAGS | FORCE_CLEANUP_FLAG_RANDOM_EVENTS)
		MISSION_CLEANUP()
	ENDIF
	
	SETUP_WIDGETS()
		
	WHILE TRUE
	
		IF widget_UNDER_INT_1_P2.bToggle
			PLAY_CUTSCENE("UNDER_INT_1_p2", <<-347.0, 1149.1,324.9>>, <<-343.9469, 1145.3226, 326.3110>>,  <<-5.0877, -0.0018, -160.8381>>, 09, "SMOG" )
		ENDIF
	
		IF widget_UNDER_INT_2.bToggle
			PLAY_CUTSCENE("UNDER_INT_2", <<-141.8, -2493.2, 47.8>>, <<-144.9430, -2490.4485, 48.9068>>, <<-15.1803, -0.0030, -11.9018>>, 22, "CLEAR" )
		ENDIF
	
		IF widget_UNDER_INT_2_p2.bToggle
			PLAY_CUTSCENE("UNDER_INT_2_p2",<<-78.1, -1462.3,32.3>>, <<-82.8457, -1467.0221, 33.0389>>, <<-8.6343, -0.0014, -47.3615>>, 22, "SMOG" )
		ENDIF
	
		IF widget_UNDER_INT_3.bToggle
			PLAY_CUTSCENE("UNDER_INT_3",<<-78.1, -1462.3,32.3>>, <<-80.8528, -1467.4266, 32.8937>>, <<-3.8217, -0.0016, 141.9845>>, 20, "EXTRASUNNY" )
		ENDIF
	
		IF widget_UNDER_INT_4_p2.bToggle
			PLAY_CUTSCENE("UNDER_INT_4_p2",<<-83.0, -1458.5,33.0>>, <<-81.0232, -1464.0978, 32.9889>>, <<-2.6401, -0.0009, 153.4561>>, 20, "SMOG" )
		ENDIF
		
		IF widget_UNDER_INT_4_p3.bToggle
			PLAY_CUTSCENE("UNDER_INT_4_p3",<<-80.0, -1477.5,32.0>>, <<-75.2977, -1471.6405, 32.1720>>, <<-3.2578, 0.0016, -115.0573>>, 08, "SMOG" )
		ENDIF

		IF widget_UNDER_INT_5.bToggle
			PLAY_CUTSCENE("UNDER_INT_5", <<77.7, 3631.1,39.9>>, <<77.1149, 3634.0852, 40.3101>>, <<-3.5369, -0.0011, 1.1504>>, 08, "EXTRASUNNY" )
		ENDIF
		
		IF widget_UNDER_INT_5_P2.bToggle
			PLAY_CUTSCENE("UNDER_INT_5_P2", <<1543.0,3616.1,35.7>>, <<1534.6456, 3602.7878, 35.9456>>, <<-6.7477, 0.0008, 146.3904>>, 12, "EXTRASUNNY" )
		ENDIF
		
		IF widget_UNDER_INT_5_P2_a.bToggle
			PLAY_CUTSCENE("UNDER_INT_5_P2", <<1543.0,3616.1,35.7>>, <<1534.6456, 3602.7878, 35.9456>>, <<-6.7477, 0.0008, 146.3904>>, 13, "EXTRASUNNY" )
		ENDIF
		
		IF widget_UNDER_INT_6.bToggle
			PLAY_CUTSCENE("UNDER_INT_6", <<310.6,178.0,103.8>>, <<307.7991, 177.2771, 104.6467>>, <<-4.6824, 0.0039, 43.8832>>, 14, "EXTRASUNNY" )
		ENDIF
		
		IF widget_UNDER_INT_6_P2.bToggle
			PLAY_CUTSCENE("UNDER_INT_6_P2", <<259.2, 195.2,104.8>>, <<261.6538, 194.3267, 105.4799>>, <<-5.8766, -0.0004, -111.4145>>, 17, "EXTRASUNNY" )
		ENDIF
		
		IF widget_UNDER_INT_7.bToggle
			PLAY_CUTSCENE("UNDER_INT_7", <<-81.5,-1463.0,32.0>>, <<-81.4466, -1465.7937, 32.8899>>, <<-5.7438, -0.0038, -177.8971>>, 13, "EXTRASUNNY" )
		ENDIF
		
		WAIT(0)
		
	endwhile

ENDSCRIPT

#ENDIF

#IF IS_FINAL_BUILD
	SCRIPT 
	ENDSCRIPT
#ENDIF
