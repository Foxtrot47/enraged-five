

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// *****************************************************************************************
// *****************************************************************************************
//
//		MISSION NAME	:	Barry1.sc
//		AUTHOR			:	Aaron Gandaa
//		DESCRIPTION		:	First Barry Mission
//
// *****************************************************************************************
// *****************************************************************************************

//----------------------
//	INCLUDES
//----------------------
USING "rgeneral_include.sch"
USING "rc_helper_functions.sch"
USING "RC_launcher_public.sch"
USING "RC_asset_public.sch"
USING "RC_Setup_public.sch"
USING "net_fps_cam.sch"

#IF IS_FINAL_BUILD
	SCRIPT
	ENDSCRIPT
#ENDIF

#IF IS_DEBUG_BUILD

//----------------------
//	ENUM
//----------------------
ENUM eCableCarState
	CABLECAR_NULLSTATE,
	CABLECAR_WAITING,
	CABLECAR_DEPARTING, 	// close the doors
	CABLECAR_SPEEDUP,
	CABLECAR_MOVING,
	CABLECAR_SLOWDOWN,
	CABLECAR_ARRIVING,		// open the doors
	CABLECAR_CLEANUP
ENDENUM

//----------------------
//	CONSTANTS
//----------------------
CONST_INT MAX_CABLEWIRE_POINTS 		15
CONST_INT MAX_CABLEWIRES 			2
CONST_INT COST_CABLECAR				10

CONST_INT CABLE_WIRE_LEFT			0
CONST_INT CABLE_WIRE_RIGHT			1

CONST_INT CABLE_DOCK_LOW			0
CONST_INT CABLE_DOCK_HIGH			1

CONST_FLOAT CABLE_CAR_SLOW_TIME		10.0

//----------------------
//	STRUCTS
//----------------------
STRUCT CABLECAR_WIRE
	VECTOR vPoints[MAX_CABLEWIRE_POINTS]
	INT iPointsUsed
	INT iDebugPoint
	BOOL bShowWire = FALSE
	FLOAT fRopeLength
ENDSTRUCT

STRUCT CABLE_CAR
	eCableCarState carState
	BOOL bIsMoving = FALSE
	BOOL bIsPlayerIn = FALSE
	BOOL bHelpShown = FALSE
	
	INT iWireIndex = 0
	INT iWireSegment = 0		
	
	FLOAT fSpeed = 0.0
	
	FLOAT fInterpolateValue = 0.0
	FLOAT fDistanceTravelled = 0.0
	INT iDepartTime = 0
	INT iDiedTime = 0
	
	VECTOR vPosition
	VEHICLE_INDEX vehicleID
	
	FLOAT fTDelta = 0.0 
	FLOAT fSpeedSgn = 0.0
	
	VEHICLE_INDEX carryVehicleID
	INT iFrameCounter = 0
	
	#IF IS_DEBUG_BUILD
		BOOL bReAttach = FALSE
		INT iState
	#ENDIF
ENDSTRUCT

//----------------------
//	VARIABLES
//----------------------
FLOAT fDebugWireVertexSize = 0.0625
FLOAT fDebugWireVertexHeight = 10.0
FLOAT fCableCarMaxSpeed = 10.0
FLOAT fCableCarAcceleration = 0.65

FLOAT fCableCarAttachHeightOffset = -7.0
VECTOR vCableCarPlayerAttachOffset = <<0, 2.750, -5.850>>
VECTOR vCableCarBikeAttachOffset = <<-1.5, 2.5, -5.850>>

VECTOR vLowDockPosition = <<-740.3, 5594.5, 41.7>>
VECTOR vHighDockPosition = <<446.8, 5571.1, 781.2>>

CABLECAR_WIRE cableWire[MAX_CABLEWIRES]		// this assumes there are only 2 cable car wires in the entire game
CABLE_CAR cableCarLeft
CABLE_CAR cableCarRight
MODEL_NAMES cableCarModel = CABLECAR

FIRST_PERSON_CAM_STRUCT fpsCam
BOOL bFPSMode = FALSE
BOOL bIsOnMission = FALSE
BOOL bSkipped = FALSE
BOOL bNoWait = FALSE

#IF IS_DEBUG_BUILD
	BOOL bExitScript = FALSE 
	BOOL bWarpToLowDock = FALSE 
	BOOL bWarpToHighDock = FALSE 
#ENDIF

//----------------------
//	CAMERA FUNCTIONS
//----------------------

/// PURPOSE:
/// 	Enable First Person Camera
PROC ENABLE_CABLE_CAR_FIRST_PERSON()
	VECTOR v = GET_PED_BONE_COORDS(PLAYER_PED_ID(), BONETAG_HEAD, <<0, -0.25, 0>>)
	
	fpsCam.iLookXLimit = 60
	fpsCam.iLookYLimit = 20
	
	INIT_FIRST_PERSON_CAMERA(fpsCam, v, GET_ENTITY_ROTATION(PLAYER_PED_ID()), 50.0, fpsCam.iLookXLimit, fpsCam.iLookYLimit)
	ATTACH_CAM_TO_ENTITY(fpsCam.theCam, PLAYER_PED_ID(), <<0, 0, 0.5>>)	
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
	bFPSMode = TRUE
ENDPROC

/// PURPOSE:
/// 	Disable First Person Camera
PROC DISABLE_CABLE_CAR_FIRST_PERSON()
	CLEAR_FIRST_PERSON_CAMERA(fpsCam)
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	bFPSMode = FALSE
ENDPROC
			
//----------------------
//	WIRE FUNCTIONS
//----------------------

/// PURPOSE:
///    Adds a point to the spline list
/// PARAMS:
///    x - xcoord
///    y - ycoord
///    z - zcoord
PROC ADD_CABLE_CAR_WIRE_POINT(CABLECAR_WIRE &wire, FLOAT x, FLOAT y, FLOAT z)
	IF (wire.iPointsUsed >= COUNT_OF(wire.vPoints))
		SCRIPT_ASSERT("INCREASE ARRAY SIZE OF CABLECAR_WIRE::vPoints")
		EXIT
	ENDIF
	
	wire.vPoints[wire.iPointsUsed].x = x
	wire.vPoints[wire.iPointsUsed].y = y 
	wire.vPoints[wire.iPointsUsed].z = z 
	wire.iPointsUsed ++
ENDPROC

/// PURPOSE:
///    Fills the points for the cable
/// PARAMS:
///    wire - wire reference
PROC CALCULATE_CABLE_CAR_WIRE_LENGTH(CABLECAR_WIRE &wire)
	INT i
	
	wire.fRopeLength = 0
	
	REPEAT wire.iPointsUsed - 2 i
		wire.fRopeLength += GET_DISTANCE_BETWEEN_COORDS(wire.vPoints[i], wire.vPoints[i + 1])
	ENDREPEAT
	
	CPRINTLN(DEBUG_AMBIENT, "CALCULATE_CABLE_CAR_WIRE_LENGTH() - Wire Length:", wire.fRopeLength)
ENDPROC

/// PURPOSE:
///    Fills the points for the cable
/// PARAMS:
///    wire - wire reference
PROC SETUP_CABLE_CAR_WIRE_LEFT(CABLECAR_WIRE &wire)
	wire.iPointsUsed = 0
		
	ADD_CABLE_CAR_WIRE_POINT(wire, -742.064, 5599.458, 47.419)
	ADD_CABLE_CAR_WIRE_POINT(wire, -739.557, 5599.467, 47.424)
	ADD_CABLE_CAR_WIRE_POINT(wire, -581.128, 5596.517, 77.151)
	ADD_CABLE_CAR_WIRE_POINT(wire, -575.717, 5596.388, 79.157)
	ADD_CABLE_CAR_WIRE_POINT(wire, -273.275, 5590.844, 240.828)
	ADD_CABLE_CAR_WIRE_POINT(wire, -268.111, 5590.745, 243.45)
	ADD_CABLE_CAR_WIRE_POINT(wire, 6.483, 5585.668, 414.761)
	ADD_CABLE_CAR_WIRE_POINT(wire, 11.361, 5585.591, 417.735)
	ADD_CABLE_CAR_WIRE_POINT(wire, 236.82, 5581.446, 599.642)
	ADD_CABLE_CAR_WIRE_POINT(wire, 241.365, 5581.370, 603.183)
	ADD_CABLE_CAR_WIRE_POINT(wire, 412.855, 5578.216, 774.401)
	ADD_CABLE_CAR_WIRE_POINT(wire, 417.541, 5578.124, 777.688)
	ADD_CABLE_CAR_WIRE_POINT(wire, 444.953, 5577.573, 786.893)
	ADD_CABLE_CAR_WIRE_POINT(wire, 447.458, 5577.576, 786.917)
	
	CPRINTLN(DEBUG_AMBIENT, "SETUP_CABLE_CAR_WIRE_LEFT() - MCable Car Left Wire Setup")
	CALCULATE_CABLE_CAR_WIRE_LENGTH(wire)
ENDPROC

/// PURPOSE:
///    Fills the points for the cable
/// PARAMS:
///    wire - wire reference
PROC SETUP_CABLE_CAR_WIRE_RIGHT(CABLECAR_WIRE &wire)
	wire.iPointsUsed = 0 

	ADD_CABLE_CAR_WIRE_POINT(wire, -742.156, 5590.682, 47.419)
	ADD_CABLE_CAR_WIRE_POINT(wire, -739.646, 5590.677, 47.433)
	ADD_CABLE_CAR_WIRE_POINT(wire, -581.321, 5587.400, 77.318)
	ADD_CABLE_CAR_WIRE_POINT(wire, -575.898, 5587.286, 79.285)
	ADD_CABLE_CAR_WIRE_POINT(wire, -273.600, 5581.125, 240.85)
	ADD_CABLE_CAR_WIRE_POINT(wire, -268.571, 5580.997, 243.428)
	ADD_CABLE_CAR_WIRE_POINT(wire, 6.575, 5575.391, 414.584)
	ADD_CABLE_CAR_WIRE_POINT(wire, 11.350, 5575.298, 417.618)
	ADD_CABLE_CAR_WIRE_POINT(wire, 236.82, 5570.664, 599.561)
	ADD_CABLE_CAR_WIRE_POINT(wire, 241.31, 5570.594, 603.137)
	ADD_CABLE_CAR_WIRE_POINT(wire, 412.661, 5567.086, 774.439)
	ADD_CABLE_CAR_WIRE_POINT(wire, 417.371, 5567.001, 777.708)
	ADD_CABLE_CAR_WIRE_POINT(wire, 444.937, 5566.384, 786.879)
	ADD_CABLE_CAR_WIRE_POINT(wire, 447.445, 5566.373, 786.924)
	
	CPRINTLN(DEBUG_AMBIENT, "SETUP_CABLE_CAR_WIRE_RIGHT() - Cable Car Right Wire Setup")
	CALCULATE_CABLE_CAR_WIRE_LENGTH(wire)
ENDPROC

/// PURPOSE:
///    Fills the points for the cable
/// PARAMS:
///    wire - wire reference
PROC SETUP_CABLE_CAR_WIRE_RIGHT_FLIPPED(CABLECAR_WIRE &wire)
	wire.iPointsUsed = 0 
	
	ADD_CABLE_CAR_WIRE_POINT(wire, 447.445, 5566.373, 786.924)
	ADD_CABLE_CAR_WIRE_POINT(wire, 444.937, 5566.384, 786.879)
	ADD_CABLE_CAR_WIRE_POINT(wire, 417.371, 5567.001, 777.708)
	ADD_CABLE_CAR_WIRE_POINT(wire, 412.661, 5567.086, 774.439)
	ADD_CABLE_CAR_WIRE_POINT(wire, 241.31, 5570.594, 603.137)
	ADD_CABLE_CAR_WIRE_POINT(wire, 236.82, 5570.664, 599.561)
	ADD_CABLE_CAR_WIRE_POINT(wire, 11.350, 5575.298, 417.618)
	ADD_CABLE_CAR_WIRE_POINT(wire, 6.575, 5575.391, 414.584)
	ADD_CABLE_CAR_WIRE_POINT(wire, -268.571, 5580.997, 243.428)
	ADD_CABLE_CAR_WIRE_POINT(wire, -273.600, 5581.125, 240.85)
	ADD_CABLE_CAR_WIRE_POINT(wire, -575.898, 5587.286, 79.285)
	ADD_CABLE_CAR_WIRE_POINT(wire, -581.321, 5587.400, 77.318)
	ADD_CABLE_CAR_WIRE_POINT(wire, -739.646, 5590.677, 47.433)
	ADD_CABLE_CAR_WIRE_POINT(wire, -742.156, 5590.682, 47.419)
	
	CPRINTLN(DEBUG_AMBIENT, "SETUP_CABLE_CAR_WIRE_RIGHT_FLIPPED() - Cable Car Right Wire Flipped Setup")
	CALCULATE_CABLE_CAR_WIRE_LENGTH(wire)
ENDPROC

/// PURPOSE:
///    Draw Wire with debug
/// PARAMS:
///    wire - wire reference
PROC DRAW_DEBUG_CABLE_CAR_WIRE(CABLECAR_WIRE &wire, INT r = 0, INT g = 0, INT b = 255, INT a = 255) 
	INT i 
	
	IF (wire.bShowWire = FALSE)
		EXIT
	ENDIF
	
	IF (wire.iDebugPoint > (wire.iPointsUsed - 1))
		wire.iDebugPoint = (wire.iPointsUsed - 1)
	ENDIF
	
	REPEAT wire.iPointsUsed i
		IF (i > 0)
			IF ((i % 2) = 0)
				DRAW_DEBUG_LINE(wire.vPoints[i], wire.vPoints[i - 1], r, g, b, a)
			ELSE
				DRAW_DEBUG_LINE(wire.vPoints[i], wire.vPoints[i - 1], r, 255, b, a)
			ENDIF
			
		ENDIF
	ENDREPEAT
	
	DRAW_DEBUG_LINE(wire.vPoints[wire.iDebugPoint] + <<0, 0, fDebugWireVertexHeight>>, wire.vPoints[wire.iDebugPoint] - <<0, 0, fDebugWireVertexHeight>>, 0, 255, 0, 255)
	DRAW_DEBUG_SPHERE(wire.vPoints[wire.iDebugPoint], fDebugWireVertexSize)
	DRAW_DEBUG_CIRCLE(wire.vPoints[wire.iDebugPoint], 1.0)
ENDPROC

//----------------------
//	CAR FUNCTIONS
//----------------------

/// PURPOSE:
///    Zeros out variables
/// PARAMS:
///    car - 
PROC ZERO_CABLE_CAR(CABLE_CAR &car)
	car.iDepartTime = GET_GAME_TIMER() + 20000
	car.iDiedTime = 0
	car.fDistanceTravelled = 0
	
	car.bIsMoving = FALSE
	car.bIsPlayerIn = FALSE
	car.bHelpShown = FALSE
	car.carState = CABLECAR_NULLSTATE
	
	car.fTDelta = 0.0
	car.fSpeed = 0.0
ENDPROC

/// PURPOSE:
///    Creates a cable car
/// PARAMS:
///    car - car reference
///    wire - index of wire to be on
///    seg - wire segment to start on - 0 if going down to up
///    f - how far along the wire segment to be placed
PROC CREATE_CABLE_CAR(CABLE_CAR &car, INT wire, INT seg = 0, FLOAT f = 0.0, FLOAT fSpdSgn = 0.0)

	ZERO_CABLE_CAR(car)
	car.iWireIndex = wire
	car.iWireSegment = seg
	car.fInterpolateValue = f
	
	car.vPosition = INTERPOLATE_VECTOR(cableWire[car.iWireIndex].vPoints[car.iWireSegment], 
									   cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1], car.fInterpolateValue)

	car.vehicleID = CREATE_VEHICLE(cableCarModel, car.vPosition)
	car.fSpeedSgn = fSpdSgn
	
	FREEZE_ENTITY_POSITION(car.vehicleID, TRUE)	
	
	// break the doors for the time being
	SET_VEHICLE_DOOR_BROKEN(car.vehicleID, SC_DOOR_FRONT_LEFT, TRUE)
	SET_VEHICLE_DOOR_BROKEN(car.vehicleID, SC_DOOR_FRONT_RIGHT, TRUE)

	IF IS_ENTITY_OK(car.vehicleID)
		SET_ENTITY_COORDS(car.vehicleID, car.vPosition + <<0, 0, fCableCarAttachHeightOffset>>)
		SET_ENTITY_HEADING(car.vehicleID, GET_HEADING_FROM_COORDS(cableWire[car.iWireIndex].vPoints[car.iWireSegment], cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1]))
	ENDIF
	
	car.iFrameCounter = car.iWireIndex
	CPRINTLN(DEBUG_AMBIENT, "CREATE_CABLE_CAR() - Cable Car Created On Wire:", wire, " Segment:", seg)
ENDPROC

/// PURPOSE:
///    Check if ped is in cable car
/// PARAMS:
///    ped - ped index
///    car - cable car index
/// RETURNS:
///    cable car
FUNC BOOL IS_PED_IN_CABLE_CAR(PED_INDEX ped, CABLE_CAR car, FLOAT ex = 0.0)	
	//VECTOR vmin, vmax
	ANGLED_AREA area
	ANGLED_AREA areaex
	
	SET_ANGLED_AREA(area,
		GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(car.vehicleID, <<0, 2.750, -5.850>>) + <<0, 0, 1>>,
		GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(car.vehicleID, <<0, -2.750, -5.850>>) - <<0, 0, 1>>,
		3.0)
		
	IF (ex > 0.0)
		areaex = EXPAND_ANGLED_AREA(area, ex)
		//DEBUG_DRAW_ANGLED_AREA_EX(areaex)
		RETURN IS_ENTITY_IN_ANGLED_AREA(ped, areaex.vPosition[0], areaex.vPosition[1], areaex.fWidth)
	ENDIF
	
	//DEBUG_DRAW_ANGLED_AREA_EX(area)
	RETURN IS_ENTITY_IN_ANGLED_AREA(ped, area.vPosition[0], area.vPosition[1], area.fWidth)
ENDFUNC

/// PURPOSE:
///    Check if ped is in cable car
/// PARAMS:
///    ped - ped index
///    car - cable car index
/// RETURNS:
///    cable car
FUNC BOOL IS_PED_IN_CABLE_CAR_VEHICLE_INDEX(PED_INDEX ped, VEHICLE_INDEX car)	
	//VECTOR vmin, vmax
	ANGLED_AREA area
	
	SET_ANGLED_AREA(area,
		GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(car, <<0, 2.750, -5.850>>) + <<0, 0, 1>>,
		GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(car, <<0, -2.750, -5.850>>) - <<0, 0, 1>>,
		3.0)
		
	RETURN IS_ENTITY_IN_ANGLED_AREA(ped, area.vPosition[0], area.vPosition[1], area.fWidth)
ENDFUNC

/// PURPOSE:
///    Cleans up a cable car
/// PARAMS:
///    car - car reference
PROC CLEANUP_CABLE_CAR(CABLE_CAR &car)
	IF IS_ENTITY_OK(car.vehicleID)
		FREEZE_ENTITY_POSITION(car.vehicleID, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(car.vehicleID)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(car.vehicleID)
		car.vehicleID = NULL
	ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "CLEANUP_CABLE_CAR() - Cable Car Cleaned Up")
ENDPROC 

/// PURPOSE:
///    Cleans up a cable car
/// PARAMS:
///    car - car reference
PROC DELETE_CABLE_CAR(CABLE_CAR &car)
	IF IS_ENTITY_OK(car.vehicleID)
		FREEZE_ENTITY_POSITION(car.vehicleID, TRUE)
	ENDIF
	
	IF DOES_ENTITY_EXIST(car.vehicleID)
		DELETE_VEHICLE(car.vehicleID)
		car.vehicleID = NULL
	ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "DELETE_CABLE_CAR() - Cable Car Deleted")
ENDPROC 

PROC FORCE_BREAK_CABLE_CAR_DOORS(CABLE_CAR &car)
	IF IS_ENTITY_OK(car.vehicleID)
		SET_VEHICLE_DOOR_BROKEN(car.vehicleID, SC_DOOR_FRONT_LEFT, TRUE)
		SET_VEHICLE_DOOR_BROKEN(car.vehicleID, SC_DOOR_FRONT_RIGHT, TRUE)
	ENDIF
ENDPROC

PROC REPAIR_CABLE_CAR_DOORS(CABLE_CAR &car)
	IF IS_ENTITY_OK(car.vehicleID)
		SET_VEHICLE_FIXED(car.vehicleID)
	ENDIF
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    car - 
PROC ATTACH_PED_TO_CABLE_CAR(PED_INDEX ped, CABLE_CAR &car)
	IF NOT IS_ENTITY_OK(ped)
		EXIT
	ENDIF
	
	VECTOR v = vCableCarPlayerAttachOffset
	IF IS_ENTITY_ATTACHED(ped)
		DETACH_ENTITY(ped)
	ENDIF
	
	v.y *= car.fSpeedSgn
	IF (car.fSpeedSgn = -1.0)
		ATTACH_ENTITY_TO_ENTITY(ped, car.vehicleID, 0, v, <<0, 0, 180.0>>)
	ELSE
		ATTACH_ENTITY_TO_ENTITY(ped, car.vehicleID, 0, v, <<0, 0, 0>>)
	ENDIF
	
	CPRINTLN(DEBUG_AMBIENT, "ATTACH_PED_TO_CABLE_CAR() - Ped Attached")
ENDPROC

/// PURPOSE:
///    
/// PARAMS:
///    car - 
PROC ATTACH_VEHICLE_TO_CABLE_CAR(VEHICLE_INDEX ped, CABLE_CAR &car)
	IF NOT IS_ENTITY_OK(ped)
		EXIT
	ENDIF
	
	VECTOR v = vCableCarBikeAttachOffset
	IF IS_ENTITY_ATTACHED(ped)
		DETACH_ENTITY(ped)
	ENDIF
	
	v.y *= car.fSpeedSgn
	ATTACH_ENTITY_TO_ENTITY(ped, car.vehicleID, 0, v, <<0, 0, 0>>)
	CPRINTLN(DEBUG_AMBIENT, "ATTACH_VEHICLE_TO_CABLE_CAR() - Vehicle Attached")
ENDPROC


/// PURPOSE:
///    Updates a cable car moving forward
/// PARAMS:
///    car - cable car reference
/// RETURNS:
///    We return true if the ride is finished 
FUNC BOOL UPDATE_CABLE_CAR_FORWARD_FORCES(CABLE_CAR &car)
	BOOL bFinished = FALSE
	VECTOR vNorm = NORMALISE_VECTOR(cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1] - cableWire[car.iWireIndex].vPoints[car.iWireSegment])

	IF (car.bIsMoving)
		FREEZE_ENTITY_POSITION(car.vehicleID, FALSE)
		SET_ENTITY_VELOCITY(car.vehicleID, vNorm * car.fSpeed)
		
		IF GET_PLANE_SIDE(vNorm, GET_ENTITY_COORDS(car.vehicleID), cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1]) = BEHIND_PLANE
			car.iWireSegment ++
			
			IF (car.iWireSegment >= (cableWire[car.iWireIndex].iPointsUsed - 1))
				car.iWireSegment = (cableWire[car.iWireIndex].iPointsUsed - 1)
				car.fSpeed = 0.0
				car.bIsMoving = FALSE
				bFinished = TRUE
				SET_ENTITY_VELOCITY(car.vehicleID, vNorm * car.fSpeed)
				CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR_FORWARD() - Cable Car:", car.iWireIndex, " Finished")	
			ELSE
				CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR_FORWARD() - Cable Car:", car.iWireIndex, " At Segment:", car.iWireSegment)
				SET_ENTITY_COORDS(car.vehicleID, cableWire[car.iWireIndex].vPoints[car.iWireSegment] + <<0, 0, fCableCarAttachHeightOffset>>)
				SET_ENTITY_HEADING(car.vehicleID, GET_HEADING_FROM_COORDS(cableWire[car.iWireIndex].vPoints[car.iWireSegment], cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1]))
			ENDIF
		ENDIF
	ENDIF
	
	RETURN bFinished
ENDFUNC

PROC UPDATE_CABLE_CAR_POSITION(CABLE_CAR &car)
	
	IF IS_SPHERE_VISIBLE(car.vPosition + <<0, 0, fCableCarAttachHeightOffset>>, 5.0) OR (car.bIsPlayerIn)
	   OR (VDIST2(car.vPosition + <<0, 0, fCableCarAttachHeightOffset>>, GET_ENTITY_COORDS(PLAYER_PED_ID())) < (60 * 60))
			SET_ENTITY_COORDS(car.vehicleID, car.vPosition + <<0, 0, fCableCarAttachHeightOffset>>)
			//SET_ENTITY_HEADING(car.vehicleID, GET_HEADING_FROM_COORDS(cableWire[car.iWireIndex].vPoints[car.iWireSegment], cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1]))
	ENDIF
ENDPROC

/// PURPOSE:
///    Updates a cable car moving forward
/// PARAMS:
///    car - cable car reference
/// RETURNS:
///    We return true if the ride is finished 
FUNC BOOL UPDATE_CABLE_CAR_FORWARD(CABLE_CAR &car)
	BOOL bFinished = FALSE
	FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(cableWire[car.iWireIndex].vPoints[car.iWireSegment], cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1])
	
	// zero divide defense
	IF (fDist != 0.0)
		car.fTDelta = (car.fSpeedSgn * car.fSpeed) / fDist
	ELSE
		car.fTDelta = 0.0
	ENDIF
	
	// only bother to update if we have told car to move
	IF (car.bIsMoving) AND (car.fTDelta != 0.0) 
		//car.fDistanceTravelled += (car.fSpeed * TIMESTEP())
		car.fInterpolateValue += (car.fTDelta * TIMESTEP()) 		
		
		IF (car.fInterpolateValue >= 1.0)
			car.fInterpolateValue = 0.0
	
			car.iWireSegment ++
			IF (car.iWireSegment >= (cableWire[car.iWireIndex].iPointsUsed - 1))
				car.iWireSegment = (cableWire[car.iWireIndex].iPointsUsed - 1)
				car.fSpeed = 0.0
				car.bIsMoving = FALSE
				bFinished = TRUE
				
				CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR_FORWARD() - Cable Car:", car.iWireIndex, " Finished")
			ENDIF	
		ENDIF
				
		// check no bounds are broken
		IF (car.iWireSegment < (cableWire[car.iWireIndex].iPointsUsed - 1))
			car.vPosition = INTERPOLATE_VECTOR(cableWire[car.iWireIndex].vPoints[car.iWireSegment], 
											   cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1], car.fInterpolateValue)
											   
			car.fDistanceTravelled += GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(car.vehicleID) - <<0, 0, fCableCarAttachHeightOffset>>, car.vPosition)
			UPDATE_CABLE_CAR_POSITION(car)
		ENDIF
	ENDIF
	
	RETURN bFinished
ENDFUNC

/// PURPOSE:
///    Updates a cable car moving backward
/// PARAMS:
///    car - cable car reference
/// RETURNS:
///    We return true if the ride is finished 
FUNC BOOL UPDATE_CABLE_CAR_BACKWARD(CABLE_CAR &car)
	BOOL bFinished = FALSE
	FLOAT fDist = GET_DISTANCE_BETWEEN_COORDS(cableWire[car.iWireIndex].vPoints[car.iWireSegment], cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1])
	
	// zero divide defense
	IF (fDist != 0.0)
		car.fTDelta = (car.fSpeedSgn * car.fSpeed) / fDist
	ELSE
		car.fTDelta = 0.0
	ENDIF
	
	// only bother to update if we have told car to move
	IF (car.bIsMoving) AND (car.fTDelta != 0.0) 
		//car.fDistanceTravelled += (car.fSpeed * TIMESTEP())
		car.fInterpolateValue += (car.fTDelta * TIMESTEP())		
		
		IF (car.fInterpolateValue < 0.0)
			car.fInterpolateValue = 1.0
		
			car.iWireSegment --
			IF (car.iWireSegment < 0)
				car.iWireSegment = 0
				car.fInterpolateValue = 0.0
				car.bIsMoving = FALSE
				car.fSpeed = 0.0
				bFinished = TRUE
				CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR_BACKWARD() - Cable Car:", car.iWireIndex, " Finished")
			ENDIF	
		ENDIF
		
		IF (car.iWireSegment < (cableWire[car.iWireIndex].iPointsUsed - 1))
			car.vPosition = INTERPOLATE_VECTOR(cableWire[car.iWireIndex].vPoints[car.iWireSegment], 
											   cableWire[car.iWireIndex].vPoints[car.iWireSegment + 1], car.fInterpolateValue)
			car.fDistanceTravelled += GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(car.vehicleID) - <<0, 0, fCableCarAttachHeightOffset>>, car.vPosition)
			UPDATE_CABLE_CAR_POSITION(car)
		ENDIF
	ENDIF

	RETURN bFinished
ENDFUNC

PROC UPDATE_CABLE_CAR_WAITING(CABLE_CAR &car)	
	// but force the cable to wait if the player is around the area (as in trying to load stuff)
	IF (car.bIsPlayerIn = FALSE) AND IS_PED_IN_CABLE_CAR(PLAYER_PED_ID(), car, 1.5)
		car.iDepartTime = GET_GAME_TIMER() + 5000
	ENDIF
	
	//CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR_WAITING() - Cable Car:", car.iWireIndex, " Player In Area:", car.bIsPlayerIn, " Help:", car.bHelpShown)
	
	// wait for the player to be in the area before going
	IF (car.bIsPlayerIn = TRUE)
		IF (GET_CURRENT_PLAYER_PED_ACCOUNT_BALANCE() < COST_CABLECAR)
			IF (car.bHelpShown = FALSE)
				PRINT_HELP_FOREVER("PLAY_CABLE_NO_CASH")			
				CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR_WAITING() - Cable Car:", car.iWireIndex, " Player Can't Afford It")
				car.bHelpShown = TRUE
			ENDIF

		ELSE
			IF (car.bHelpShown = FALSE)
				PRINT_HELP_FOREVER("PLAY_CABLE")			
				CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR_WAITING() - Cable Car:", car.iWireIndex, " Player Ride Help Shown")
				car.bHelpShown = TRUE
			ENDIF
			
			
			IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_CONTEXT)
				CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR_WAITING() - Cable Car:", car.iWireIndex, " Is Ready To Depart With Player")
				CLEAR_THIS_FLOATING_HELP("PLAY_CABLE")
				car.iDepartTime = 0
			ENDIF
		ENDIF
	ELSE
		IF (car.bHelpShown = TRUE)
			CLEAR_HELP()
			car.bHelpShown = FALSE
		ENDIF
	ENDIF
	
	// set the cable car going
	IF (GET_GAME_TIMER() > car.iDepartTime) AND (car.bIsMoving = FALSE)
		CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR_WAITING() - Cable Car:", car.iWireIndex, " Departing")
		car.carState = CABLECAR_DEPARTING
		car.fDistanceTravelled = 0
		
		IF (car.bHelpShown = TRUE)
			car.bHelpShown = FALSE
			CLEAR_HELP()
		ENDIF
		
		IF (car.bIsPlayerIn = TRUE)
			RC_START_CUTSCENE_MODE(<<0, 0, 0>>)
			DISABLE_CELLPHONE(TRUE)
		ENDIF
	ENDIF
			
ENDPROC

PROC PLAYER_END_CABLE_CAR(CABLE_CAR &car, BOOL skip = FALSE)
	IS_ENTITY_OK(PLAYER_PED_ID()) 
	IF IS_ENTITY_ATTACHED(PLAYER_PED_ID()) 
		DETACH_ENTITY(PLAYER_PED_ID()) 
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("CABLE_CAR_RIDE_UP_SCENE")
		CPRINTLN(DEBUG_AMBIENT, "PLAYER_END_CABLE_CAR() - Cable Car:", car.iWireIndex, " Stop Audio Scene: CABLE_CAR_RIDE_UP_SCENE")
		STOP_AUDIO_SCENE("CABLE_CAR_RIDE_UP_SCENE")
	ENDIF
	
	IF IS_AUDIO_SCENE_ACTIVE("CABLE_CAR_RIDE_DOWN_SCENE")
		CPRINTLN(DEBUG_AMBIENT, "PLAYER_END_CABLE_CAR() - Cable Car:", car.iWireIndex, " Stop Audio Scene: CABLE_CAR_RIDE_DOWN_SCENE")
		STOP_AUDIO_SCENE("CABLE_CAR_RIDE_DOWN_SCENE")
	ENDIF
	
	DISABLE_CABLE_CAR_FIRST_PERSON()
	RC_END_CUTSCENE_MODE()
	DISABLE_CELLPHONE(FALSE)
	
	
	IF (skip)
		CPRINTLN(DEBUG_AMBIENT, "PLAYER_END_CABLE_CAR() - Cable Car:", car.iWireIndex, " Skipped Re-Spotting Player")
		IF ((car.iWireIndex = CABLE_WIRE_LEFT) AND (car.fSpeedSgn = 1.0)) OR ((car.iWireIndex = CABLE_WIRE_RIGHT) AND (car.fSpeedSgn = -1.0))
			WAIT_FOR_WORLD_TO_LOAD(vHighDockPosition)
			IF IS_ENTITY_OK(PLAYER_PED_ID())
				SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vHighDockPosition)
			ENDIF
		ENDIF

		IF ((car.iWireIndex = CABLE_WIRE_LEFT) AND (car.fSpeedSgn = -1.0)) OR ((car.iWireIndex = CABLE_WIRE_RIGHT) AND (car.fSpeedSgn = 1.0))
			WAIT_FOR_WORLD_TO_LOAD(vHighDockPosition)
			IF IS_ENTITY_OK(PLAYER_PED_ID())
				SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vLowDockPosition)
			ENDIF
		ENDIF
		
		car.carState = CABLECAR_CLEANUP
		DELETE_CABLE_CAR(car)
		ZERO_CABLE_CAR(car) 
		
		// regenerate cable car at end
		IF (car.fSpeedSgn = 1.0)
			CREATE_CABLE_CAR(car, car.iWireIndex, cableWire[car.iWireIndex].iPointsUsed - 1, 1.0, -car.fSpeedSgn)
		ELSE
			CREATE_CABLE_CAR(car, car.iWireIndex, 0, 0.0, -car.fSpeedSgn)
		ENDIF
		
		WAIT(250)
		bSkipped = FALSE
	ENDIF
		
	DO_SCREEN_FADE_IN(500)
	DEBIT_BANK_ACCOUNT(GET_CURRENT_PLAYER_PED_ENUM(), BAAC_UNLOGGED_SMALL_ACTION, COST_CABLECAR)
ENDPROC

/// PURPOSE:
///    Handles controls while in cable car - skipping and camera switch
/// PARAMS:
///    car - 
PROC UPDATE_CABLE_CAR_PLAYER_CONTROLS(CABLE_CAR &car)
	INT iEnum = ENUM_TO_INT(car.carState)
	
	IF (iEnum < ENUM_TO_INT(CABLECAR_SPEEDUP)) OR (iEnum > ENUM_TO_INT(CABLECAR_SLOWDOWN))
		EXIT 
	ENDIF
	
	// camera switching
	IF IS_CONTROL_JUST_PRESSED(FRONTEND_CONTROL, INPUT_SCRIPT_RRIGHT)	
		IF NOT bFPSMode
			ENABLE_CABLE_CAR_FIRST_PERSON()
		ENDIF
		
		/*
		IF NOT bFPSMode
			ENABLE_CABLE_CAR_FIRST_PERSON()
		ELSE
			DISABLE_CABLE_CAR_FIRST_PERSON()
		ENDIF
		*/
	ENDIF
	
	// skipping
	IF (bSkipped = FALSE)
		IF IS_CONTROL_PRESSED(FRONTEND_CONTROL, INPUT_SKIP_CUTSCENE)
			SAFE_FADE_SCREEN_OUT_TO_BLACK(500, FALSE)
			bSkipped = TRUE
		ENDIF
	ELIF IS_SCREEN_FADED_OUT()
		PLAYER_END_CABLE_CAR(car, TRUE)
	ENDIF	
ENDPROC

/// PURPOSE:
///    Update the cable car
/// PARAMS:
///    car - car reference
/// RETURNS:
///    We return true if the ride is finished 
FUNC BOOL UPDATE_CABLE_CAR(CABLE_CAR &car)
	FLOAT s 

	// if vehicle doesn't exist quit
	car.iFrameCounter ++
	IF NOT DOES_ENTITY_EXIST(car.vehicleID)
		RETURN FALSE
	ENDIF
	
	// make vehicle drop when dead
	IF IS_ENTITY_DEAD(car.vehicleID)
		SET_ENTITY_HAS_GRAVITY(car.vehicleID, TRUE)
		EXPLODE_VEHICLE(car.vehicleID)
		IF IS_ENTITY_ATTACHED_TO_ENTITY(PLAYER_PED_ID(), car.vehicleID)
			DETACH_ENTITY(PLAYER_PED_ID()) 
		ENDIF
			
		FREEZE_ENTITY_POSITION(car.vehicleID, FALSE)
		SET_VEHICLE_AS_NO_LONGER_NEEDED(car.vehicleID)
		car.vehicleID = NULL
		car.iDiedTime = GET_GAME_TIMER()
		RETURN FALSE
	ENDIF
	
	// disable switching and weapons if player is attached
	car.bIsPlayerIn = IS_PED_IN_CABLE_CAR(PLAYER_PED_ID(), car)
	
	IF (car.bIsPlayerIn)
		RC_DISABLE_CONTROL_ACTIONS_FOR_LEAD_IN()
		DISABLE_SELECTOR_THIS_FRAME()
		UPDATE_CABLE_CAR_PLAYER_CONTROLS(car)
	ENDIF
	
	SWITCH (car.carState)
		CASE CABLECAR_NULLSTATE
		CASE CABLECAR_WAITING
			UPDATE_CABLE_CAR_WAITING(car)
		BREAK
		CASE CABLECAR_DEPARTING
			IF (car.bIsPlayerIn)
				bSkipped = FALSE
				ATTACH_PED_TO_CABLE_CAR(PLAYER_PED_ID(), car)
				ENABLE_CABLE_CAR_FIRST_PERSON()
			ENDIF
			
			CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR() - Cable Car:", car.iWireIndex, " Accelerating")
			//SET_ENTITY_COLLISION(car.vehicleID, FALSE)
			SET_ENTITY_HAS_GRAVITY(car.vehicleID, FALSE)	
			car.carState = CABLECAR_SPEEDUP
			car.bIsMoving = TRUE
		BREAK
		CASE CABLECAR_SPEEDUP		
			car.fSpeed += (fCableCarAcceleration * TIMESTEP())
			IF (car.fSpeed > fCableCarMaxSpeed)
				car.fSpeed = fCableCarMaxSpeed
				car.carState = CABLECAR_MOVING
				CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR() - Cable Car:", car.iWireIndex, " Acceleration Complete")
			ENDIF
		BREAK
		CASE CABLECAR_MOVING
			s = (fCableCarMaxSpeed * CABLE_CAR_SLOW_TIME) + (0.5 * (fCableCarAcceleration * (CABLE_CAR_SLOW_TIME * CABLE_CAR_SLOW_TIME)))
			
			IF (car.fDistanceTravelled >= (cableWire[car.iWireIndex].fRopeLength - s))
				CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR() - Cable Car:", car.iWireIndex, " Slowdown Start")
				car.carState = CABLECAR_SLOWDOWN
			ENDIF	
		BREAK
		CASE CABLECAR_SLOWDOWN		
			car.fSpeed -= (fCableCarAcceleration * TIMESTEP())
			IF (car.fSpeed < 0.0)
				car.fSpeed = 0.0
				CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR() - Cable Car:", car.iWireIndex, " Slowdown Complete")
				car.carState = CABLECAR_ARRIVING
			ENDIF
		BREAK		
		CASE CABLECAR_ARRIVING
			//SET_ENTITY_COLLISION(car.vehicleID, TRUE)
			
			// turn off 1st person camera and get ready to detach the player
			IF IS_ENTITY_ATTACHED_TO_ENTITY(PLAYER_PED_ID(), car.vehicleID)
				DETACH_ENTITY(PLAYER_PED_ID()) 
				
				/*
				IF ((car.iWireIndex = CABLE_WIRE_LEFT) AND (car.fSpeedSgn = 1.0)) OR ((car.iWireIndex = CABLE_WIRE_RIGHT) AND (car.fSpeedSgn = -1.0))
					CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR() - Cable Car:", car.iWireIndex, " Finished - Collision bust - Warp Player")
					SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vHighDockPosition)
				ENDIF
				*/
				PLAYER_END_CABLE_CAR(car)
			ENDIF
			
			// wait for the player to be at least a couple of meters away 
			IF NOT (bNoWait)
				IF IS_PED_IN_CABLE_CAR(PLAYER_PED_ID(), car, 1.5)
					RETURN TRUE
				ENDIF
			ENDIF
			
			CPRINTLN(DEBUG_AMBIENT, "UPDATE_CABLE_CAR() - Cable Car:", car.iWireIndex, " Waiting 20 Seconds to Depart")
			car.carState = CABLECAR_WAITING
			car.iDepartTime = GET_GAME_TIMER() + 20000
			car.fSpeedSgn *= -1.0
			car.fDistanceTravelled = 0
		BREAK
	ENDSWITCH
	
	#IF IS_DEBUG_BUILD
		IF (car.bReAttach)
			IF IS_ENTITY_ATTACHED(PLAYER_PED_ID()) 
				DETACH_ENTITY(PLAYER_PED_ID()) 
			ENDIF
			
			ATTACH_ENTITY_TO_ENTITY(PLAYER_PED_ID(), car.vehicleID, 0, vCableCarPlayerAttachOffset, <<0, 0, 0>>)
			car.bReAttach = FALSE
		ENDIF
		
		car.iState = ENUM_TO_INT(car.carState)
	#ENDIF
	
	IF (car.fSpeedSgn > 0.0)
		RETURN UPDATE_CABLE_CAR_FORWARD(car)
	ELSE
		RETURN UPDATE_CABLE_CAR_BACKWARD(car)
	ENDIF
	
	RETURN FALSE
ENDFUNC

//----------------------
//	DEBUG VARIABLES
//----------------------
#IF IS_DEBUG_BUILD
	WIDGET_GROUP_ID m_WidgetGroup
#ENDIF

#IF IS_DEBUG_BUILD
/// PURPOSE:
/// 	Initializes Debug Widgets
PROC SETUP_CABLECAR_DEBUG_WIDGETS(STRING str, CABLE_CAR &car)
	START_WIDGET_GROUP(str)	
		ADD_WIDGET_BOOL("Is Moving", car.bIsMoving)
		ADD_WIDGET_BOOL("Is Player In", car.bIsPlayerIn)
		ADD_WIDGET_INT_SLIDER("Current Index", car.iWireSegment, 0, MAX_CABLEWIRE_POINTS - 1, 1)
		ADD_WIDGET_FLOAT_SLIDER("Interpolate Value", car.fInterpolateValue, -1, 1, 0.000625)
		
		ADD_WIDGET_INT_READ_ONLY("State", car.iState)
		ADD_WIDGET_INT_READ_ONLY("Depart Time", car.iDepartTime)
		ADD_WIDGET_FLOAT_READ_ONLY("T Delta", car.fTDelta)
		ADD_WIDGET_FLOAT_READ_ONLY("Speed", car.fSpeed)
		ADD_WIDGET_FLOAT_READ_ONLY("Speed Sign", car.fSpeedSgn)
		ADD_WIDGET_FLOAT_READ_ONLY("Distance Travelled", car.fDistanceTravelled)
		ADD_WIDGET_BOOL("Attach Player", car.bReAttach)
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
/// 	Initializes Wire Debug Widgets
PROC SETUP_CABLECAR_WIRE_DEBUG_WIDGETS(STRING str, CABLECAR_WIRE &wire)
	START_WIDGET_GROUP(str)	
		ADD_WIDGET_BOOL("Show Wire", wire.bShowWire)
		ADD_WIDGET_INT_SLIDER("Show Point", wire.iDebugPoint, 0, COUNT_OF(wire.vPoints) - 1, 1)
		ADD_WIDGET_INT_READ_ONLY("Points Used", wire.iPointsUsed)
		ADD_WIDGET_FLOAT_READ_ONLY("Wire Length", wire.fRopeLength)
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
/// 	Initializes Wire Debug Widgets
PROC SETUP_CABLECAR_SYSTEM_DEBUG_WIDGETS()
	START_WIDGET_GROUP("Cable Car System")	
		ADD_WIDGET_BOOL("Quit Script", bExitScript)
		ADD_WIDGET_BOOL("Don't Wait", bNoWait)
		ADD_WIDGET_BOOL("Warp To Low Dock", bWarpToLowDock)
		ADD_WIDGET_BOOL("Warp To High Dock", bWarpToHighDock)
		ADD_WIDGET_FLOAT_SLIDER("Max Speed", fCableCarMaxSpeed, 0, 30, 0.25)
		ADD_WIDGET_FLOAT_SLIDER("Acceleration", fCableCarAcceleration, 0, 5, 0.005)

		ADD_WIDGET_VECTOR_SLIDER("Player Attach", vCableCarPlayerAttachOffset, -15, 15, 0.25)
		ADD_WIDGET_FLOAT_SLIDER("Wire Attach Offset", fCableCarAttachHeightOffset, -10, 10, 0.25)
		ADD_WIDGET_FLOAT_SLIDER("Vert Size", fDebugWireVertexSize, 0, 1, 0.005)
		ADD_WIDGET_FLOAT_SLIDER("Vert Height", fDebugWireVertexHeight, 0, 30, 0.25)
		ADD_WIDGET_BOOL("On Mission", bIsOnMission)
		ADD_WIDGET_BOOL("FPS Mode", bFPSMode)
		ADD_WIDGET_BOOL("Skipped", bSkipped)
	STOP_WIDGET_GROUP()
ENDPROC

/// PURPOSE:
/// 	Initializes Debug Widgets
PROC SETUP_DEBUG_WIDGETS()
	SET_PROFILING_OF_THIS_SCRIPT(TRUE)
	
	m_WidgetGroup = START_WIDGET_GROUP("Cable Car Test")	
		SETUP_CABLECAR_SYSTEM_DEBUG_WIDGETS()
		SETUP_CABLECAR_DEBUG_WIDGETS("Left Car", cableCarLeft)
		SETUP_CABLECAR_DEBUG_WIDGETS("Right Car", cableCarRight)
		SETUP_CABLECAR_WIRE_DEBUG_WIDGETS("Left Wire", cableWire[CABLE_WIRE_LEFT])
		SETUP_CABLECAR_WIRE_DEBUG_WIDGETS("Right Wire", cableWire[CABLE_WIRE_RIGHT])
	STOP_WIDGET_GROUP()
ENDPROC

PROC UPDATE_DEBUG_WIDGETS()
	IF (bWarpToLowDock) 
		IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
			DETACH_ENTITY(PLAYER_PED_ID())
		ENDIF
		
		RC_END_CUTSCENE_MODE()
		CLEAR_FIRST_PERSON_CAMERA(fpsCam)
		SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vLowDockPosition)
		bWarpToLowDock = FALSE 
	ENDIF
	
	IF (bWarpToHighDock) 
		IF IS_ENTITY_ATTACHED(PLAYER_PED_ID())
			DETACH_ENTITY(PLAYER_PED_ID())
		ENDIF
		
		RC_END_CUTSCENE_MODE()
		CLEAR_FIRST_PERSON_CAMERA(fpsCam)
		SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), vHighDockPosition)
		bWarpToHighDock = FALSE 
	ENDIF
ENDPROC	

PROC CLEANUP_DEBUG_WIDGETS()
	IF DOES_WIDGET_GROUP_EXIST(m_WidgetGroup)
		DELETE_WIDGET_GROUP(m_WidgetGroup)
	ENDIF
ENDPROC	
#ENDIF
 
/// PURPOSE: 
///  	Cleanups Script and terminates thread - this should be the last function called 
PROC SCRIPT_CLEANUP()
	#IF IS_DEBUG_BUILD
		CLEANUP_DEBUG_WIDGETS()
	#ENDIF
	
	CLEAR_FIRST_PERSON_CAMERA(fpsCam)
	IS_ENTITY_OK(PLAYER_PED_ID()) 
	IF IS_ENTITY_ATTACHED(PLAYER_PED_ID()) 
		DETACH_ENTITY(PLAYER_PED_ID()) 
	ENDIF
	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)
	
	DELETE_CABLE_CAR(cableCarLeft)
	DELETE_CABLE_CAR(cableCarRight)
	SET_MODEL_AS_NO_LONGER_NEEDED(cableCarModel)
	
	REACTIVATE_ALL_WORLD_BRAINS_THAT_ARE_WAITING_TILL_OUT_OF_RANGE()
	CPRINTLN(DEBUG_AMBIENT, "New Cable Car - Cleanup")
	TERMINATE_THIS_THREAD()
ENDPROC

//----------------------
//	MAIN SCRIPT
//----------------------
SCRIPT		
	VECTOR v
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS))
		SCRIPT_CLEANUP()
	ENDIF
	 
	CPRINTLN(DEBUG_AMBIENT, "New Cable Car - Initializing - Version:", 1109)
	//SET_ENTITY_COORDS_GROUNDED(PLAYER_PED_ID(), <<-741.5, 5595.5, 41.7>>)
	
	#IF IS_DEBUG_BUILD
		SETUP_DEBUG_WIDGETS()
	#ENDIF
	
	IS_ENTITY_OK(PLAYER_PED_ID())
	//SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	CPRINTLN(DEBUG_AMBIENT, "New Cable Car - Fade Done")
	
	SECURE_REQUEST_AND_LOAD_MODEL(cableCarModel)
	SETUP_CABLE_CAR_WIRE_LEFT(cableWire[CABLE_WIRE_LEFT])
	SETUP_CABLE_CAR_WIRE_RIGHT_FLIPPED(cableWire[CABLE_WIRE_RIGHT])

	CREATE_CABLE_CAR(cableCarLeft, CABLE_WIRE_LEFT, 0, 0.460, 1)
	UPDATE_CABLE_CAR(cableCarLeft)
	
	CREATE_CABLE_CAR(cableCarRight, CABLE_WIRE_RIGHT, 0, 0.460, 1)
	UPDATE_CABLE_CAR(cableCarRight)
	
	WHILE (TRUE)
		IS_ENTITY_OK(PLAYER_PED_ID())
		
		#IF IS_DEBUG_BUILD
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) OR (bExitScript)
				RC_END_CUTSCENE_MODE()
				SCRIPT_CLEANUP()
			ENDIF	
		#ENDIF
		
		#IF IS_DEBUG_BUILD
			DRAW_DEBUG_CABLE_CAR_WIRE(cableWire[CABLE_WIRE_LEFT], 255, 0, 0, 255)
			DRAW_DEBUG_CABLE_CAR_WIRE(cableWire[CABLE_WIRE_RIGHT], 0, 0, 255, 255)
		#ENDIF
		
		v = GET_ENTITY_COORDS(PLAYER_PED_ID()) 
		IF (v.y > 4500.0)
			UPDATE_CABLE_CAR(cableCarLeft)
			UPDATE_CABLE_CAR(cableCarRight)
		ENDIF
		
		IF (bIsOnMission = FALSE) AND GET_MISSION_FLAG()
			CPRINTLN(DEBUG_AMBIENT, "On Mission - Seal up the Cable Car")
			REPAIR_CABLE_CAR_DOORS(cableCarLeft)
			REPAIR_CABLE_CAR_DOORS(cableCarRight)
			bIsOnMission = TRUE
		ENDIF
		
		IF (bIsOnMission = TRUE) AND NOT GET_MISSION_FLAG()
			CPRINTLN(DEBUG_AMBIENT, "On Mission - Reopen up the Cable Car")
			FORCE_BREAK_CABLE_CAR_DOORS(cableCarLeft)
			FORCE_BREAK_CABLE_CAR_DOORS(cableCarRight)
			bIsOnMission = FALSE
		ENDIF
		
		IF bFPSMode = TRUE
			UPDATE_FIRST_PERSON_CAMERA(fpsCam)
		ENDIF
		
		#IF IS_DEBUG_BUILD
			UPDATE_DEBUG_WIDGETS()
		#ENDIF
		
		WAIT(0)
	ENDWHILE
ENDSCRIPT

#ENDIF



