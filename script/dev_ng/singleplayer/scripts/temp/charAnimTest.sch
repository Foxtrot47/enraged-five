// Includes
USING "model_enums.sch"

// Constants
CONST_INT MAX_NUM_CHARACTERS(266)
CONST_INT MAX_NUM_PROPS(7)

// Ambient helpers
CONST_INT A_F_M_START 0		
CONST_INT A_F_M_END   20
CONST_INT A_F_O_START 21	
CONST_INT A_F_O_END   26
CONST_INT A_F_Y_START 27	
CONST_INT A_F_Y_END   64
CONST_INT A_M_M_START 65	
CONST_INT A_M_M_END   107
CONST_INT A_M_O_START 108	
CONST_INT A_M_O_END   117
CONST_INT A_M_Y_START 118	
CONST_INT A_M_Y_END   182

// Gang helpers
CONST_INT G_F_Y_START 183	
CONST_INT G_F_Y_END   183
CONST_INT G_M_M_START 184	
CONST_INT G_M_M_END   194
CONST_INT G_M_Y_START 195	
CONST_INT G_M_Y_END   218

// Unique helpers
CONST_INT U_F_M_START 219	
CONST_INT U_F_M_END   221
CONST_INT U_F_O_START 222	
CONST_INT U_F_O_END   223
CONST_INT U_F_Y_START 224	
CONST_INT U_F_Y_END   231
CONST_INT U_M_M_START 232	
CONST_INT U_M_M_END   248
CONST_INT U_M_O_START 249	
CONST_INT U_M_O_END   249
CONST_INT U_M_Y_START 250	
CONST_INT U_M_Y_END   266

// Model name array
MODEL_NAMES eModelName[MAX_NUM_CHARACTERS]
MODEL_NAMES ePropName[MAX_NUM_PROPS]

// Functions
PROC INIT_MODEL_ENUMS()

	// Ambient Peds
	eModelName[0]=A_F_M_BEACH_01		// A_F_M_START                                                                           
	eModelName[1]=A_F_M_BEVHILLS_01                                                                    
	eModelName[2]=A_F_M_BEVHILLS_02                                                                      
	eModelName[3]=A_F_M_BODYBUILD_01                                                                     
	eModelName[4]=A_F_M_BUSINESS_02                                                                       
	eModelName[5]=A_F_M_DOWNTOWN_01                                                                       
	eModelName[6]=A_F_M_EASTSA_01                                                                      
	eModelName[7]=A_F_M_EASTSA_02                                                                        
	eModelName[8]=A_F_M_FATBLA_01                                                                         
	eModelName[9]=A_F_M_FATWHITE_01                                                                        
	eModelName[10]=A_F_M_KTOWN_01                                                                        
	eModelName[11]=A_F_M_KTOWN_02                                                                         
	eModelName[12]=A_F_M_PROLHOST_01                                                                        
	eModelName[13]=A_F_M_SALTON_01                                                                      
	eModelName[14]=A_F_M_SKIDROW_01                                                                     
	eModelName[15]=A_F_M_SOUCENTMC_01                                                                     
	eModelName[16]=A_F_M_SOUCENT_01                                                                        
	eModelName[17]=A_F_M_SOUCENT_02                                                                       
	eModelName[18]=A_F_M_TOURIST_01                                                                      
	eModelName[19]=A_F_M_TRAMPBEAC_01                                                                    
	eModelName[20]=A_F_M_TRAMP_01                                                                         
	eModelName[21]=A_F_O_GENSTREET_01	// A_F_O_START                                     
	eModelName[22]=A_F_O_INDIAN_01                                                                    
	eModelName[23]=A_F_O_KTOWN_01                                                                    
	eModelName[24]=A_F_O_SALTON_01                                                                       
	eModelName[25]=A_F_O_SOUCENT_01                                                                      
	eModelName[26]=A_F_O_SOUCENT_02                                                                      
	eModelName[27]=A_F_Y_BEACH_01		// A_F_Y_START                                                                    
	eModelName[28]=A_F_Y_BEVHILLS_01                                                                     
	eModelName[29]=A_F_Y_BEVHILLS_02                                                                     
	eModelName[30]=A_F_Y_BEVHILLS_03                                                                       
	eModelName[31]=A_F_Y_BEVHILLS_04                                                                       
	eModelName[32]=A_F_Y_BUSINESS_01                                                                       
	eModelName[33]=A_F_Y_BUSINESS_02                                                                       
	eModelName[34]=A_F_Y_BUSINESS_03                                                                      
	eModelName[35]=A_F_Y_BUSINESS_04                                                                     
	eModelName[36]=A_F_Y_EASTSA_01                                                                       
	eModelName[37]=A_F_Y_EASTSA_02                                                                          
	eModelName[38]=A_F_Y_EASTSA_03                                                                       
	eModelName[39]=A_F_Y_EPSILON_01                                                                      
	eModelName[40]=A_F_Y_FITNESS_01                                                                        
	eModelName[41]=A_F_Y_FITNESS_02                                                                        
	eModelName[42]=A_F_Y_GENHOT_01                                                                       
	eModelName[43]=A_F_Y_GOLFER_01                                                                        
	eModelName[44]=A_F_Y_HIKER_01                                                                          
	eModelName[45]=A_F_Y_HIPPIE_01                                                                       
	eModelName[46]=A_F_Y_HIPSTER_01                                                                      
	eModelName[47]=A_F_Y_HIPSTER_02                                                                  
	eModelName[48]=A_F_Y_HIPSTER_03                                                                     
	eModelName[49]=A_F_Y_HIPSTER_04                                                                       
	eModelName[50]=A_F_Y_INDIAN_01                                                                       
	eModelName[51]=A_F_Y_RUNNER_01                                                                       
	eModelName[52]=A_F_Y_SCDRESSY_01                                                                       
	eModelName[53]=A_F_Y_SKATER_01                                                                        
	eModelName[54]=A_F_Y_SOUCENT_01                                                                        
	eModelName[55]=A_F_Y_SOUCENT_02                                                                     
	eModelName[56]=A_F_Y_SOUCENT_03                                                                    
	eModelName[57]=A_F_Y_TENNIS_01                                                                   
	eModelName[58]=A_F_Y_TOURIST_01                                                                 
	eModelName[59]=A_F_Y_TOURIST_02                                                                    
	eModelName[60]=A_F_Y_VINEWOOD_01                                                                     
	eModelName[61]=A_F_Y_VINEWOOD_02                                                                       
	eModelName[62]=A_F_Y_VINEWOOD_03                                                                       
	eModelName[63]=A_F_Y_VINEWOOD_04                                                                      
	eModelName[64]=A_F_Y_YOGA_01                                                                     
	eModelName[65]=A_M_M_AFRIAMER_01	// A_M_M_START                                                                   
	eModelName[66]=A_M_M_BEACH_01                                                                         
	eModelName[67]=A_M_M_BEACH_02                                                                       
	eModelName[68]=A_M_M_BEVHILLS_01                                                                      
	eModelName[69]=A_M_M_BEVHILLS_02                                                                     
	eModelName[70]=A_M_M_BUSINESS_01                                                                      
	eModelName[71]=A_M_M_EASTSA_01                                                                       
	eModelName[72]=A_M_M_EASTSA_02                                                                         
	eModelName[73]=A_M_M_FARMER_01                                                                        
	eModelName[74]=A_M_M_FATLATIN_01                                                                      
	eModelName[75]=A_M_M_GENFAT_01                                                                      
	eModelName[76]=A_M_M_GENFAT_02                                                                          
	eModelName[77]=A_M_M_GOLFER_01                                                                       
	eModelName[78]=A_M_M_HASJEW_01                                                                         
	eModelName[79]=A_M_M_HILLBILLY_01                                                                      
	eModelName[80]=A_M_M_HILLBILLY_02                                                                     
	eModelName[81]=A_M_M_INDIAN_01                                                                        
	eModelName[82]=A_M_M_KTOWN_01                                                                          
	eModelName[83]=A_M_M_MALIBU_01                                                                         
	eModelName[84]=A_M_M_MEXCNTRY_01                                                                      
	eModelName[85]=A_M_M_MEXLABOR_01                                                                      
	eModelName[86]=A_M_M_OG_BOSS_01                                                                     
	eModelName[87]=A_M_M_PAPARAZZI_01                                                                  
	eModelName[88]=A_M_M_POLYNESIAN_01                                                                   
	eModelName[89]=A_M_M_PROLHOST_01                                                                    
	eModelName[90]=A_M_M_RURMETH_01                                                                      
	eModelName[91]=A_M_M_SALTON_01                                                                       
	eModelName[92]=A_M_M_SALTON_02                                                                        
	eModelName[93]=A_M_M_SALTON_03                                                                        
	eModelName[94]=A_M_M_SKATER_01                                                                       
	eModelName[95]=A_M_M_SKIDROW_01                                                                        
	eModelName[96]=A_M_M_SOCENLAT_01                                                                      
	eModelName[97]=A_M_M_SOUCENT_01                                                                       
	eModelName[98]=A_M_M_SOUCENT_02                                                                      
	eModelName[99]=A_M_M_SOUCENT_03                                                                     
	eModelName[100]=A_M_M_SOUCENT_04                                                                      
	eModelName[101]=A_M_M_STLAT_02                                                                       
	eModelName[102]=A_M_M_TENNIS_01                                                                        
	eModelName[103]=A_M_M_TOURIST_01                                                                       
	eModelName[104]=A_M_M_TRAMPBEAC_01                                                                     
	eModelName[105]=A_M_M_TRAMP_01                                                                      
	eModelName[106]=A_M_M_TRANVEST_01                                                                       
	eModelName[107]=A_M_M_TRANVEST_02 
	eModelName[108]=A_M_O_ACULT_01		// A_M_O_START                                                                   
	eModelName[109]=A_M_O_ACULT_02                                                               
	eModelName[110]=A_M_O_BEACH_01                                                                    
	eModelName[111]=A_M_O_GENSTREET_01                                                                 
	eModelName[112]=A_M_O_KTOWN_01                                                                       
	eModelName[113]=A_M_O_SALTON_01                                                                       
	eModelName[114]=A_M_O_SOUCENT_01                                                                      
	eModelName[115]=A_M_O_SOUCENT_02                                                                    
	eModelName[116]=A_M_O_SOUCENT_03                                                                      
	eModelName[117]=A_M_O_TRAMP_01                                                                          
	eModelName[118]=A_M_Y_ACULT_01		// A_M_Y_START                                                                       
	eModelName[119]=A_M_Y_ACULT_02                                                                        
	eModelName[120]=A_M_Y_BEACHVESP_01                                                                     
	eModelName[121]=A_M_Y_BEACHVESP_02                                                                     
	eModelName[122]=A_M_Y_BEACH_01                                                                       
	eModelName[123]=A_M_Y_BEACH_02                                                                       
	eModelName[124]=A_M_Y_BEACH_03                                                                        
	eModelName[125]=A_M_Y_BEVHILLS_01                                                                     
	eModelName[126]=A_M_Y_BEVHILLS_02                                                                      
	eModelName[127]=A_M_Y_BREAKDANCE_01                                                                      
	eModelName[128]=A_M_Y_BUSICAS_01                                                                      
	eModelName[129]=A_M_Y_BUSINESS_01                                                                    
	eModelName[130]=A_M_Y_BUSINESS_02                                                                   
	eModelName[131]=A_M_Y_BUSINESS_03                                                                    
	eModelName[132]=A_M_Y_CYCLIST_01                                                                         
	eModelName[133]=A_M_Y_DOWNTOWN_01                                                                       
	eModelName[134]=A_M_Y_EASTSA_01                                                                    
	eModelName[135]=A_M_Y_EASTSA_02                                                                         
	eModelName[136]=A_M_Y_EPSILON_01                                                                      
	eModelName[137]=A_M_Y_EPSILON_02                                                                     
	eModelName[138]=A_M_Y_GAY_01                                                                         
	eModelName[139]=A_M_Y_GAY_02                                                                      
	eModelName[140]=A_M_Y_GENSTREET_01                                                                   
	eModelName[141]=A_M_Y_GENSTREET_02                                                                      
	eModelName[142]=A_M_Y_GOLFER_01                                                                         
	eModelName[143]=A_M_Y_HASJEW_01                                                                         
	eModelName[144]=A_M_Y_HIKER_01                                                                       
	eModelName[145]=A_M_Y_HIPPY_01                                                                       
	eModelName[146]=A_M_Y_HIPSTER_01                                                                     
	eModelName[147]=A_M_Y_HIPSTER_02                                                                 
	eModelName[148]=A_M_Y_HIPSTER_03                                                                     
	eModelName[149]=A_M_Y_INDIAN_01                                                                     
	eModelName[150]=A_M_Y_JETSKI_01                                                                         
	eModelName[151]=A_M_Y_KTOWN_01                                                                        
	eModelName[152]=A_M_Y_KTOWN_02                                                                          
	eModelName[153]=A_M_Y_METHHEAD_01                                                                       
	eModelName[154]=A_M_Y_MEXTHUG_01                                                                         
	eModelName[155]=A_M_Y_MOTOX_01                                                                        
	eModelName[156]=A_M_Y_MOTOX_02                                                                      
	eModelName[157]=A_M_Y_MUSCLBEAC_01                                                                     
	eModelName[158]=A_M_Y_MUSCLBEAC_02                                                                    
	eModelName[159]=A_M_Y_POLYNESIAN_01                                                                   
	eModelName[160]=A_M_Y_ROADCYC_01                                                                       
	eModelName[161]=A_M_Y_RUNNER_01                                                                        
	eModelName[162]=A_M_Y_RUNNER_02                                                                     
	eModelName[163]=A_M_Y_SALTON_01                                                                         
	eModelName[164]=A_M_Y_SKATER_01                                                                       
	eModelName[165]=A_M_Y_SKATER_02                                                                      
	eModelName[166]=A_M_Y_SOUCENT_01                                                                       
	eModelName[167]=A_M_Y_SOUCENT_02                                                                       
	eModelName[168]=A_M_Y_SOUCENT_03                                                                      
	eModelName[169]=A_M_Y_SOUCENT_04                                                                       
	eModelName[170]=A_M_Y_STBLA_01                                                                         
	eModelName[171]=A_M_Y_STBLA_02                                                                        
	eModelName[172]=A_M_Y_STLAT_01                                                                        
	eModelName[173]=A_M_Y_STWHI_01                                                                           
	eModelName[174]=A_M_Y_STWHI_02                                                                          
	eModelName[175]=A_M_Y_SUNBATHE_01                                                                     
	eModelName[176]=A_M_Y_SURFER_01                                                                        
	eModelName[177]=A_M_Y_VINDOUCHE_01                                                                    
	eModelName[178]=A_M_Y_VINEWOOD_01                                                                      
	eModelName[179]=A_M_Y_VINEWOOD_02                                                                     
	eModelName[180]=A_M_Y_VINEWOOD_03                                                                      
	eModelName[181]=A_M_Y_VINEWOOD_04                                                                       
	eModelName[182]=A_M_Y_YOGA_01
	
	// Gang Peds
	eModelName[183]=G_F_Y_VAGOS_01		// G_F_Y_START                                                                          
	eModelName[184]=G_M_M_ARMBOSS_01 	// G_F_M_START                                                                      
	eModelName[185]=G_M_M_ARMGOON_01                                                                        
	eModelName[186]=G_M_M_ARMLIEUT_01                                                                                                                                            
	eModelName[187]=G_M_M_CHEMWORK_01                                                                       
	eModelName[188]=G_M_M_CHIBOSS_01                                                                       
	eModelName[189]=G_M_M_CHICOLD_01                                                                         
	eModelName[190]=G_M_M_CHIGOON_01                                                                       
	eModelName[191]=G_M_M_CHIGOON_02                                                                         
	eModelName[192]=G_M_M_KORBOSS_01                                                                       
	eModelName[193]=G_M_M_MEXBOSS_01                                                                        
	eModelName[194]=G_M_M_MEXBOSS_02                                                                        
	eModelName[195]=G_M_Y_ARMGOON_02	// G_M_Y_START                                                                        
	eModelName[196]=G_M_Y_AZTECA_01                                                                         
	eModelName[197]=G_M_Y_BALLAEAST_01                                                                      
	eModelName[198]=G_M_Y_BALLAORIG_01                                                                       
	eModelName[199]=G_M_Y_BALLASOUT_01                                                                       
	eModelName[200]=G_M_Y_FAMCA_01                                                                         
	eModelName[201]=G_M_Y_FAMDNF_01                                                                        
	eModelName[202]=G_M_Y_FAMFOR_01                                                                       
	eModelName[203]=G_M_Y_KOREAN_01                                                                        
	eModelName[204]=G_M_Y_KOREAN_02                                                                       
	eModelName[205]=G_M_Y_KORLIEUT_01                                                                                                                                                
	eModelName[206]=G_M_Y_LOST_01                                                                         
	eModelName[207]=G_M_Y_LOST_02                                                                         
	eModelName[208]=G_M_Y_LOST_03                                                                          
	eModelName[209]=G_M_Y_MEXGOON_01                                                                        
	eModelName[210]=G_M_Y_MEXGOON_02                                                                       
	eModelName[211]=G_M_Y_MEXGOON_03                                                                      
	eModelName[212]=G_M_Y_POLOGOON_01                                                                      
	eModelName[213]=G_M_Y_SALVABOSS_01                                                                     
	eModelName[214]=G_M_Y_SALVAGOON_01                                                                      
	eModelName[215]=G_M_Y_SALVAGOON_02                                                                      
	eModelName[216]=G_M_Y_SALVAGOON_03                                                                        
	eModelName[217]=G_M_Y_STRPUNK_01                                                                        
	eModelName[218]=G_M_Y_STRPUNK_02                                                                       
	
	// Unique peds
	eModelName[219]=U_F_M_CORPSE_01		// U_F_M_START                                                                       
	eModelName[220]=U_F_M_MIRANDA                                                                      
	eModelName[221]=U_F_M_PROMOURN_01                                                                      
	eModelName[222]=U_F_O_MOVIESTAR 	// U_F_O_START                                                                        
	eModelName[223]=U_F_O_PROLHOST_01                                                                       
	eModelName[224]=U_F_Y_BIKERCHIC		// U_F_Y_START                                                                          
	eModelName[225]=U_F_Y_COMJANE                                                                         
	eModelName[226]=U_F_Y_HOTPOSH_01                                                                       
	eModelName[227]=U_F_Y_JEWELASS_01                                                                      
	eModelName[228]=U_F_Y_MISTRESS                                                                          
	eModelName[229]=U_F_Y_POPPYMICH                                                                         
	eModelName[230]=U_F_Y_PRINCESS                                                                         
	eModelName[231]=U_F_Y_SPYACTRESS                                                                       
	eModelName[232]=U_M_M_ALDINAPOLI	// U_M_M_START                                                                        
	eModelName[233]=U_M_M_BANKMAN                                                                        
	eModelName[234]=U_M_M_BIKEHIRE_01                                                                                                                                       
	eModelName[235]=U_M_M_FIBARCHITECT                                                                      
	eModelName[236]=U_M_M_FILMDIRECTOR                                                                       
	eModelName[237]=U_M_M_GLENSTANK_01                                                                     
	eModelName[238]=U_M_M_GRIFF_01                                                                                                                                           
	eModelName[239]=U_M_M_JESUS_01                                                                         
	eModelName[240]=U_M_M_JEWELSEC_01                                                                      
	eModelName[241]=U_M_M_JEWELTHIEF                                                                                                                                             
	eModelName[242]=U_M_M_PARTYTARGET                                                                     
	eModelName[243]=U_M_M_PROLSEC_01                                                                       
	eModelName[244]=U_M_M_PROMOURN_01                                                                       
	eModelName[245]=U_M_M_RIVALPAP                                                                                                                                
	eModelName[246]=U_M_M_SPYACTOR                                                                        
	eModelName[247]=U_M_M_SPYACTOR
	eModelName[248]=U_M_M_WILLYFIST
	eModelName[249]=U_M_O_FINGURU_01	// U_M_O_START                                                                        
	eModelName[250]=U_M_Y_BABYD         // U_M_Y_START                                                           
	eModelName[251]=U_M_Y_BAYGOR                                                                                                                                             
	eModelName[252]=U_M_Y_BURGERDRUG_01                                                                   
	eModelName[253]=U_M_Y_FIBMUGGER_01                                                                     
	eModelName[254]=U_M_Y_GUNVEND_01                                                                       
	eModelName[255]=U_M_Y_HIPPIE_01                                                                         
	eModelName[256]=U_M_Y_IMPORAGE                                                                          
	eModelName[257]=U_M_Y_MANI                                                                            
	eModelName[258]=U_M_Y_MILITARYBUM                                                                       
	eModelName[259]=U_M_Y_PAPARAZZI                                                                        
	eModelName[260]=U_M_Y_POGO_01                                                                          
	eModelName[261]=U_M_Y_PRISONER_01                                                                     
	eModelName[262]=U_M_Y_PROLDRIVER_01                                                                   
	eModelName[263]=U_M_Y_RSRANGER_01                                                                      
	eModelName[264]=U_M_Y_STAGGRM_01                                                                     
	eModelName[265]=U_M_Y_ZOMBIE_01
ENDPROC

PROC INIT_PROP_ENUMS()

	ePropName[0]=P_AMB_COFFEECUP_01
	ePropName[1]=P_CS_JOINT_01                                                                                                                    
	ePropName[2]=PROP_CS_BEER_BOT_01 
	ePropName[3]=PROP_CS_BEER_BOT_40OZ                                                                                                                                   
	ePropName[4]=PROP_CS_CIGGY_01
	ePropName[5]=PROP_PHONE_ING 
	ePropName[6]=PROP_PLASTIC_CUP_02
ENDPROC
