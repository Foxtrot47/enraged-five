

//Compile out Title Update changes to header functions.
//Must be before includes.
//CONST_INT 	USE_TU_CHANGES	0 // Removed by Kenneth R.


// *****************************************************************************************
// *****************************************************************************************
//
//		FILE NAME		:	selling_short_1.sch
//		AUTHOR			:	Aaron Gandaa
//		DESCRIPTION		: 	TEMP SCRIPT TO BE USED BY NYC FOR PRERENDER
//
//							This is the Life Invader presentation from Lester 1B. This is
//							the whole presentation that plays out should the player fail to
//							make the call to the prototype cell phone in time.
//
// *****************************************************************************************
// *****************************************************************************************

//----------------------
//	INCLUDES
//----------------------
USING "selling_short_include.sch"
 
//----------------------
//	CONSTS
//----------------------
CONST_INT XVERSION_NUMBER	171
 
//----------------------
//	VARIABLES
//----------------------

//----------------------
//	FUNCTIONS
//----------------------
PROC SCRIPT_CLEANUP()
	#IF IS_DEBUG_BUILD
		CLEANUP_SS_DEBUG_WIDGETS()
	#ENDIF

	UNLOAD_SS_ASSETS()
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(FALSE)
	TERMINATE_THIS_THREAD()
ENDPROC

SCRIPT
	SET_MISSION_FLAG(TRUE) // - comment this out so we can see blip behavior
	
	// Setup callback when player is killed, arrested or goes to multiplayer
	IF (HAS_FORCE_CLEANUP_OCCURRED(DEFAULT_FORCE_CLEANUP_FLAGS))
		SCRIPT_CLEANUP()
	ENDIF
	
	CPRINTLN(DEBUG_MISSION, "MISSION VERSION:", XVERSION_NUMBER)
	
	DO_SCREEN_FADE_OUT(0)
	SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
	DISABLE_CELLPHONE(TRUE)
	
	// load cutscene stuff
	REQUEST_CUTSCENE("les_1b_mcs_2")
	LOAD_SS_ASSETS()
	
	#IF IS_DEBUG_BUILD
		SETUP_SS_DEBUG_WIDGETS()
	#ENDIF
	
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(TRUE)
	WHILE (TRUE)
		SET_VEHICLE_DENSITY_MULTIPLIER_THIS_FRAME(0.0)
		SET_PED_DENSITY_MULTIPLIER_THIS_FRAME(0.0)

		IS_ENTITY_OK(PLAYER_PED_ID())
		IF UPDATE_LF_CUTSCENE_DONE()
			SCRIPT_CLEANUP()
		ENDIF
		
		#IF IS_DEBUG_BUILD
			UPDATE_SS_DEBUG_WIDGETS()
			
			// use shift-s to shut this down
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_S) 
				SCRIPT_CLEANUP()
			ENDIF	
		#ENDIF
		
		IF (bShowVersionNumber)
			SET_TEXT_SCALE(0.5, 0.5)
			DISPLAY_TEXT_WITH_NUMBER(0.1, 0.1, "NUMBER", XVERSION_NUMBER)
		ENDIF
		
		WAIT(0)
	ENDWHILE
ENDSCRIPT


