USING "rage_builtins.sch"
USING "globals.sch"
USING "beast_secret_peyote.sch"

// Compiled in to main_persistent.sc when FEATURE_SP_DLC_BEAST_SECRET is set to 1 
// and FEATURE_SP_DLC_BEAST_SECRET_DEBUG to 0.

#IF FEATURE_SP_DLC_BEAST_SECRET
#IF FEATURE_SP_DLC_BEAST_SECRET_DEBUG
	BeastPeyoteVars sBeastPeyoteVars
#ENDIF
#ENDIF

SCRIPT
	#IF FEATURE_SP_DLC_BEAST_SECRET
	#IF FEATURE_SP_DLC_BEAST_SECRET_DEBUG
	
		CPRINTLN(DEBUG_HUNTING, "Beast thread 1 launched.")
		
		IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
			CPRINTLN(DEBUG_HUNTING, "Beast thread 1 terminating.")
			TERMINATE_THIS_THREAD()
		ENDIF

		Reset_Beast_Peyote_Variables(sBeastPeyoteVars)
		
		#IF IS_DEBUG_BUILD
			Create_Beast_Peyote_Widgets(sBeastPeyoteVars, NULL)
		#ENDIF

		WHILE TRUE
			Maintain_Beast_Peyote_Progression(sBeastPeyoteVars)
			Maintain_Play_Beast_Peyote_Sound(sBeastPeyoteVars)
			
			#IF IS_DEBUG_BUILD
				Update_Beast_Peyote_Widgets(sBeastPeyoteVars)
			#ENDIF

			WAIT(0)
		ENDWHILE
		
	#ENDIF //FEATURE_SP_DLC_BEAST_SECRET_DEBUG
	#ENDIF //FEATURE_SP_DLC_BEAST_SECRET
ENDSCRIPT
