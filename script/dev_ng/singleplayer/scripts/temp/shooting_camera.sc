//Solomon5.sc
//Ross Wallace 20/06/2012

#IF IS_DEBUG_BUILD

USING "rage_builtins.sch"
USING "commands_clock.sch"
USING "globals.sch"
USING "player_ped_public.sch"
USING "dialogue_public.sch"
USING "blip_control_public.sch"
// ____________________________________ INCLUDES ___________________________________________
USING "commands_script.sch"
USING "CompletionPercentage_public.sch"
using "commands_pad.sch"
using "commands_misc.sch"
using "commands_player.sch"
USING "Commands_streaming.sch"
USING "script_player.sch"
USING "commands_camera.sch"
USING "commands_interiors.sch"
USING "script_MISC.sch"
USING "commands_object.sch"
USING "script_ped.sch"
USING "chase_hint_cam.sch"
USING "script_blips.sch" 
USING "locates_public.sch"
USING "replay_public.sch"
USING "commands_cutscene.sch"
USING "script_maths.sch"
USING "mission_stat_public.sch"
USING "cutscene_public.sch"
using "selector_public.sch"
USING "taxi_functions.sch"
USING "timeLapse.sch"
USING "kill_cam_public.sch"


  
	USING "select_mission_stage.sch"
	USING "shared_debug.sch"
	USING "script_debug.sch"
// ____________________________________ VARIABLES __________________________________________

//STAGE_TIMELAPSE

//CAMERA_INDEX cutsceneCamera
CAMERA_INDEX initialCam
CAMERA_INDEX destinationCam



	WIDGET_GROUP_ID sol5WidgetGroup
	
	INT iDebugMissionStage
	BOOL bDebugInitialised
	BOOL bIsSuperDebugEnabled
	BOOL bDebugOn = TRUE
	
	CONST_INT MAX_SKIP_MENU_LENGTH 9                      // number of stages in mission + 2 (for menu )
	
	INT iReturnStage                                       // mission stage to jump to
	MissionStageMenuTextStruct SkipMenuStruct[MAX_SKIP_MENU_LENGTH]      // struct containing the debug menu 

	
KILL_CAM_DATA sKillCamDataTest

REL_GROUP_HASH relGroupPlayer
REL_GROUP_HASH relGroupCops

CONST_INT NUMBER_OF_VEHICLES 6
CONST_INT NUMBER_OF_PEDS	 12

VEHICLE_INDEX vehiclesEnemy[NUMBER_OF_VEHICLES]

PED_INDEX pedsEnemy[NUMBER_OF_PEDS]

//MODEL_NAMES mnGoonModel = S_M_Y_BLACKOPS_01

INT iCurrentPlayerCam = 0
INT iCurrentTargetCam = 0

BOOL bUseSequentialCameras = FALSE

// ____________________________________ FUNCTIONS __________________________________________

ENUM MISSION_STAGE_FLAG 

	STAGE_INITIALISE,						//0
	STAGE_CHOOSE_STAGE,						//1
	STAGE_DO_HOMING,						//2
	STAGE_DO_SHOOTOUT,						//3
	STAGE_DO_CORRIDOOR,						//4
	STAGE_DO_SPIN_KILL,						//5
	STAGE_DO_KILL_HEADER,					//6
	STAGE_DO_CAMERA_ATTACH,					//7
	STAGE_DO_POSITIONER,					//8
	STAGE_DEBUG								//9

ENDENUM


MISSION_STAGE_FLAG mission_stage =  STAGE_INITIALISE //STAGE_INITIALISE //STAGE_TIME_LAPSE

INT i_current_event

//PURPOSE: Starts or stops a cutscene. If TRUE is passed TIMERA() is set to Zero
PROC SET_CUTSCENE_RUNNING(BOOL isRunning, BOOL doGameCamInterp = FALSE, INT durationFromInterp = 2000, BOOL turnOffGadgets = TRUE)

//	SET_USE_HIGHDOF(isRunning)removed
	SET_WIDESCREEN_BORDERS(isRunning,0)
	IF turnOffGadgets = TRUE
		SET_PLAYER_CONTROL(PLAYER_ID(), (NOT isRunning),  SPC_DEACTIVATE_GADGETS)
	ELSE
		SET_PLAYER_CONTROL(PLAYER_ID(), (NOT isRunning))
	ENDIF
	RENDER_SCRIPT_CAMS(isRunning, doGameCamInterp, durationFromInterp)
	
	CLEAR_HELP(TRUE)
	SET_SCRIPTS_SAFE_FOR_CUTSCENE(isRunning)
		
	DISABLE_CELLPHONE(isRunning)
	DISPLAY_HUD(NOT isRunning)
	DISPLAY_RADAR(NOT isRunning)
ENDPROC

//Cleanup
PROC MISSION_CLEANUP()

	SET_ENTITY_VISIBLE(PLAYER_PED_ID(), TRUE)

	DISABLE_CELLPHONE(FALSE)
	DELETE_WIDGET_GROUP(sol5WidgetGroup)
	 
	SET_ENTITY_INVINCIBLE(player_ped_id(), FALSE)
	SET_CUTSCENE_RUNNING(FALSE)
	
	HANG_UP_AND_PUT_AWAY_PHONE() 
	
	SET_AIR_DRAG_MULTIPLIER_FOR_PLAYERS_VEHICLE(PLAYER_ID(), 1.0)
	SET_GPS_MULTI_ROUTE_RENDER(FALSE)
		
	SET_PED_CONFIG_FLAG(PLAYER_PED_ID(), PCF_ForceDirectEntry, FALSE)

	SET_REDUCE_VEHICLE_MODEL_BUDGET(FALSE)
	SET_REDUCE_PED_MODEL_BUDGET(FALSE)
	
	SET_VEHICLE_POPULATION_BUDGET(3)
	SET_MAX_WANTED_LEVEL(5)
	

	CLEAR_TIMECYCLE_MODIFIER()
	SET_TIME_SCALE(1.0)		
	
	IF IS_SCREEN_FADED_OUT()
	
		//DO_SCREEN_FADE_IN(500)
	ENDIF
	
ENDPROC


PROC Mission_Passed()	
		
	SET_CUTSCENE_RUNNING(FALSE)
	
	CLEAR_PRINTS () 	
	Mission_Cleanup()
	TERMINATE_THIS_THREAD()
		
ENDPROC

//Mission Failed
PROC Mission_Failed()	
	

	IF IS_PLAYER_PLAYING(PLAYER_ID())
		SPECIAL_ABILITY_DEACTIVATE(PLAYER_ID())
	ENDIF

	Mission_Cleanup()
	TERMINATE_THIS_THREAD()
		
ENDPROC


//MISSION_STAGE_FLAG skip_mission_stage	//For debug and replays.

PROC initialiseMission()

	 
		g_b_CellDialDebugTextToggle = FALSE
	 
	DISABLE_CELLPHONE(TRUE)
	REQUEST_MODEL(POLICE)
	REQUEST_MODEL(S_M_Y_COP_01)
	REQUEST_MODEL(S_M_Y_SWAT_01)
	
	WHILE NOT HAS_MODEL_LOADED(POLICE)
	OR NOT HAS_MODEL_LOADED(S_M_Y_COP_01)
	OR NOT HAS_MODEL_LOADED(S_M_Y_SWAT_01)
		WAIT(0)
	ENDWHILE

	ADD_RELATIONSHIP_GROUP("PLAYERGROUP", relGroupPlayer)
	ADD_RELATIONSHIP_GROUP("COPS", relGroupCops)

	SET_PED_RELATIONSHIP_GROUP_HASH(player_ped_id(), relGroupPlayer)

	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupPlayer, relGroupCops)
	SET_RELATIONSHIP_BETWEEN_GROUPS(ACQUAINTANCE_TYPE_PED_HATE, relGroupCops, relGroupPlayer)
	
	SET_MAX_WANTED_LEVEL(0)
	
	SET_ENTITY_COORDS(PLAYER_PED_ID(), << 990.6710, -3084.4187, 4.9006 >>)
	SET_ENTITY_HEADING(PLAYER_PED_ID(), 25.3050)
	CLEAR_AREA(<< 964.5986, -3074.5789, 13.3323 >>, 1000.0, TRUE)
	LOAD_SCENE(<< 964.5986, -3074.5789, 13.3323 >>)
	
	DISABLE_TAXI_HAILING(TRUE)
	
	bDrawMenu = TRUE
	
	mission_stage = STAGE_CHOOSE_STAGE
		
ENDPROC


PROC RESET_GAME_CAMERA(FLOAT fHeading = 0.0)

	SET_GAMEPLAY_CAM_RELATIVE_HEADING(fHeading)
	SET_GAMEPLAY_CAM_RELATIVE_PITCH(fHeading)

ENDPROC


PROC DELETE_ARRAY_OF_VEHICLES(VEHICLE_INDEX &Vehicles[])

	INT i
	
	FOR i = 0 TO COUNT_OF(Vehicles) -1
		IF DOES_ENTITY_EXIST(vehicles[i])
			SET_ENTITY_AS_MISSION_ENTITY(vehicles[i])
			DELETE_VEHICLE(vehicles[i])
			PRINTLN("deleted vehicle in array at: ", i)
		ENDIF
	ENDFOR
	
ENDPROC

PROC DELETE_ARRAY_OF_PEDS(PED_INDEX &thesePeds[], BOOL bSetAsNoLongerNeeded = FALSE, INT iMaxIndex = -1)

	INT i
	
	INT iArrayLength 
	
	IF iMaxIndex <> -1
		iArrayLength = iMaxIndex
	ELSE
		iArrayLength = COUNT_OF(thesePeds) -1
	ENDIF
	
	FOR i = 0 TO iArrayLength
		IF DOES_ENTITY_EXIST(thesePeds[i])
			IF NOT bSetAsNoLongerNeeded
				DELETE_PED(thesePeds[i])
				PRINTLN("deleted Ped in array at: ", i)
			ELSE
				SET_PED_AS_NO_LONGER_NEEDED(thesePeds[i])
				PRINTLN("set Ped as no longer needed in array at: ", i)
			ENDIF
			
		ENDIF
	ENDFOR
	
ENDPROC

PROC CAM_SET_PED_AS_COP(PED_INDEX &thisPed)

	SET_PED_RELATIONSHIP_GROUP_HASH(thisPed, relGroupCops)
	GIVE_WEAPON_TO_PED(thisPed, WEAPONTYPE_CARBINERIFLE, INFINITE_AMMO, TRUE, TRUE)
	SET_ENTITY_ONLY_DAMAGED_BY_PLAYER(thisPed, TRUE)
	SET_PED_CAN_COWER_IN_COVER(thisPed, FALSE)
	SET_PED_CAN_PEEK_IN_COVER(thisPed, TRUE)
	SET_PED_COMBAT_MOVEMENT(thisPed, CM_DEFENSIVE)
	
	SET_PED_COMBAT_ATTRIBUTES(thisPed, CA_USE_VEHICLE, FALSE)
	SET_PED_AS_ENEMY(thisPed, TRUE)
			
	SET_COMBAT_FLOAT(thisPed, CCF_TIME_BETWEEN_BURSTS_IN_COVER, 2.5)
	//SET_COMBAT_FLOAT(thisCop, CCF_TIME_BETWEEN_PEEKS, 10.00)
	SET_COMBAT_FLOAT(thisPed, CCF_BURST_DURATION_IN_COVER, 5.00)
	SET_COMBAT_FLOAT(thisPed, CCF_MAX_SHOOTING_DISTANCE, 150.00)	
	SET_COMBAT_FLOAT(thisPed, CCF_STRAFE_WHEN_MOVING_CHANCE, 0.75)
	SET_PED_COMBAT_ATTRIBUTES(thisPed,CA_MOVE_TO_LOCATION_BEFORE_COVER_SEARCH,TRUE)

ENDPROC

PROC CREATE_ENEMIES_KILL_CAM()

	vehiclesEnemy[0] = CREATE_VEHICLE(POLICE, << 966.8484, -3069.0615, 4.9006 >>, 151.9361)
	vehiclesEnemy[1] = CREATE_VEHICLE(POLICE, << 968.8089, -3057.7603, 4.9006 >>, 164.3634)
	vehiclesEnemy[2] = CREATE_VEHICLE(POLICE, << 981.6879, -3060.0669, 4.9006 >>, 121.5130)
	vehiclesEnemy[3] = CREATE_VEHICLE(POLICE, << 978.7624, -3069.4978, 4.9006 >>, 150.7781)
	vehiclesEnemy[4] = CREATE_VEHICLE(POLICE, << 955.2049, -3059.1777, 4.9006 >>, 358.4798 )
	vehiclesEnemy[5] = CREATE_VEHICLE(POLICE, << 975.9450, -3044.5044, 4.9006 >>, 74.9391)
	
						
	pedsEnemy[0] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01 , << 978.5275, -3067.2932, 4.9006 >>, 287.6422)
	pedsEnemy[1] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01 , << 976.4548, -3071.2263, 4.9006 >>, 200.6026)
	pedsEnemy[2] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01 , << 966.6886, -3066.7339, 4.9006 >>, 245.7643)
	pedsEnemy[3] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01 , << 972.4304, -3062.3967, 4.9006 >>, 251.6285)
	pedsEnemy[4] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01 , << 979.9864, -3059.5664, 4.9006 >>, 190.4261)
	pedsEnemy[5] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01 , << 969.6686, -3054.8042, 4.9007 >>, 200.7037)
	pedsEnemy[6] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 , << 955.5161, -3056.1052, 4.9006 >>, 222.0927)
	pedsEnemy[7] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01 , << 967.0250, -3059.5339, 4.9006 >>, 241.9995)
	pedsEnemy[8] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 , << 953.9085, -3061.3762, 4.9006 >>, 283.5547)
	pedsEnemy[9] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01 , << 964.5879, -3073.1560, 4.9006 >>, 242.5852)
	pedsEnemy[10] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 , << 989.2443, -3047.2817, 4.9006 >>, 151.4118)
	pedsEnemy[11] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 , << 964.5986, -3074.5789, 13.3323 >>, 269.5620)
	
	INT i
	
	i = 0
	
	FOR i = 0 TO NUMBER_OF_VEHICLES - 1
		IF DOES_ENTITY_EXIST(vehiclesEnemy[i])
			SET_VEHICLE_SIREN(vehiclesEnemy[i], TRUE)
			SET_SIREN_WITH_NO_DRIVER(vehiclesEnemy[i], TRUE)
					
			TEXT_LABEL_63 debugCarName = "CopCar "
			debugCarName += i
			SET_VEHICLE_NAME_DEBUG(vehiclesEnemy[i], debugCarName)
		ENDIF
	ENDFOR
	
	FOR i = 0 TO NUMBER_OF_PEDS - 1
		CAM_SET_PED_AS_COP(pedsEnemy[i])
	ENDFOR

ENDPROC

PROC CREATE_ENEMIES_CORRIDOOR()

	
						
	pedsEnemy[0] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 , << 994.8319, -3031.7495, 7.6909 >>, 144.0577)
	pedsEnemy[1] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 , << 1002.0558, -3029.6760, 10.5116 >>, 118.8338)
	pedsEnemy[2] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 , << 1007.7225, -3038.7188, 10.5116 >>, 139.3695)
	pedsEnemy[3] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 , << 1008.1252, -3029.8955, 13.3323 >>, 97.5118)
	pedsEnemy[4] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 , << 1022.9753, -3032.3149, 13.3323 >>, 90.0578)
	pedsEnemy[5] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 , << 1023.9942, -3035.6318, 7.6909 >>, 80.9927 )
	pedsEnemy[6] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 ,<< 1019.6062, -3032.5198, 4.9006 >>, 178.9412)
	pedsEnemy[7] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 , << 1019.8748, -3035.9895, 4.9006 >>, 358.9412)
	pedsEnemy[8] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 ,<< 1013.1638, -3031.6289, 7.6909 >>, 92.1116)
	pedsEnemy[9] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 , << 1003.9309, -3032.3086, 7.6909 >>, 96.0002)
	
	INT i
		
	FOR i = 0 TO NUMBER_OF_PEDS - 1
		IF DOES_ENTITY_EXIST(pedsEnemy[i])
			CAM_SET_PED_AS_COP(pedsEnemy[i])
		ENDIF
	ENDFOR

ENDPROC

PROC CREATE_ENEMIES_SPIN_KILL()

	vehiclesEnemy[0] = CREATE_VEHICLE(POLICE, << 966.8484, -3069.0615, 4.9006 >>, 151.9361)
	vehiclesEnemy[1] = CREATE_VEHICLE(POLICE, << 968.8089, -3057.7603, 4.9006 >>, 164.3634)
	vehiclesEnemy[2] = CREATE_VEHICLE(POLICE, << 981.6879, -3060.0669, 4.9006 >>, 121.5130)
	vehiclesEnemy[3] = CREATE_VEHICLE(POLICE, << 978.7624, -3069.4978, 4.9006 >>, 150.7781)
	vehiclesEnemy[4] = CREATE_VEHICLE(POLICE, << 955.2049, -3059.1777, 4.9006 >>, 358.4798 )
	vehiclesEnemy[5] = CREATE_VEHICLE(POLICE, << 975.9450, -3044.5044, 4.9006 >>, 74.9391)
	
						
	pedsEnemy[0] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01 , << 978.5275, -3067.2932, 4.9006 >>, 287.6422)
	pedsEnemy[1] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01 , << 976.4548, -3071.2263, 4.9006 >>, 200.6026)
	pedsEnemy[2] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01 , << 966.6886, -3066.7339, 4.9006 >>, 245.7643)
	pedsEnemy[3] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01 , << 972.4304, -3062.3967, 4.9006 >>, 251.6285)
	pedsEnemy[4] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01 , << 979.9864, -3059.5664, 4.9006 >>, 190.4261)
	pedsEnemy[5] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01 , << 969.6686, -3054.8042, 4.9007 >>, 200.7037)
	pedsEnemy[6] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 , << 955.5161, -3056.1052, 4.9006 >>, 222.0927)
	pedsEnemy[7] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01 , << 967.0250, -3059.5339, 4.9006 >>, 241.9995)
	pedsEnemy[8] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 , << 953.9085, -3061.3762, 4.9006 >>, 283.5547)
	pedsEnemy[9] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_COP_01 , << 964.5879, -3073.1560, 4.9006 >>, 242.5852)
	pedsEnemy[10] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 , << 989.2443, -3047.2817, 4.9006 >>, 151.4118)
	pedsEnemy[11] = CREATE_PED(PEDTYPE_MISSION, S_M_Y_SWAT_01 , << 964.5986, -3074.5789, 13.3323 >>, 269.5620)
	
	INT i
	
	i = 0
	
	FOR i = 0 TO NUMBER_OF_VEHICLES - 1
		IF DOES_ENTITY_EXIST(vehiclesEnemy[i])
			SET_VEHICLE_SIREN(vehiclesEnemy[i], TRUE)
			SET_SIREN_WITH_NO_DRIVER(vehiclesEnemy[i], TRUE)
					
			TEXT_LABEL_63 debugCarName = "CopCar "
			debugCarName += i
			SET_VEHICLE_NAME_DEBUG(vehiclesEnemy[i], debugCarName)
		ENDIF
	ENDFOR
	
	FOR i = 0 TO NUMBER_OF_PEDS - 1
		CAM_SET_PED_AS_COP(pedsEnemy[i])
	ENDFOR

ENDPROC


FUNC INT GET_ENEMY_IN_ARRAY_BEING_AIMED_AT()
	
	INT i
	
	FOR i = 0 TO NUMBER_OF_PEDS - 1
		IF NOT IS_ENTITY_DEAD(pedsEnemy[i])
			IF IS_PLAYER_TARGETTING_ENTITY(PLAYER_ID(), pedsEnemy[i] )
			OR IS_PLAYER_FREE_AIMING_AT_ENTITY(PLAYER_ID(), pedsEnemy[i] )
				RETURN i 	
			ENDIF
		ENDIF
	ENDFOR

	RETURN -1

ENDFUNC

FLOAT fPreviousClosest = 999.99
INT iClosestPed

FUNC INT GET_CLOSEST_ENEMY_TO_CAM_IN_ARRAY()
	
	INT i
	
	FOR i = 0 TO NUMBER_OF_PEDS - 1
		IF NOT IS_ENTITY_DEAD(pedsEnemy[i])
			IF GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedsEnemy[i]), GET_FINAL_RENDERED_CAM_COORD()) < fPreviousClosest
				fPreviousClosest = GET_DISTANCE_BETWEEN_COORDS(GET_ENTITY_COORDS(pedsEnemy[i]), GET_FINAL_RENDERED_CAM_COORD())
				iClosestPed = i	
			ENDIF
		ENDIF
	ENDFOR

	fPreviousClosest = 999.99
	RETURN iClosestPed

ENDFUNC



INT iKillCamStage
INT iEnemyArrayIndex
INT iCameraChoice

PROC UPDATE_KILL_CAM()

	SWITCH iKillCamStage

		CASE 0
		
			iEnemyArrayIndex = GET_ENEMY_IN_ARRAY_BEING_AIMED_AT()
		
			//Camera swings around Michael a bit...
			IF iEnemyArrayIndex <> -1
			AND IS_PED_SHOOTING(PLAYER_PED_ID())
				

				DESTROY_ALL_CAMS()
				
				initialCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				destinationCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				
				
							
				IF mission_stage = STAGE_DO_HOMING
					iCameraChoice = 5
				ELSE
					iCameraChoice = GET_RANDOM_INT_IN_RANGE(0, 7)
					
					IF bUseSequentialCameras
						iCurrentPlayerCam++
						IF iCurrentPlayerCam > 6
							iCurrentPlayerCam = 0
						ENDIF
						
						iCameraChoice = iCurrentPlayerCam
					ENDIF
				ENDIF
				
				
				
				SWITCH iCameraChoice
				
					CASE 0
						ATTACH_CAM_TO_ENTITY(initialCam, PLAYER_PED_ID(), <<-0.3919, 1.6401, 0.2750>>)
						POINT_CAM_AT_ENTITY(initialCam, PLAYER_PED_ID(), <<0.6206, -1.1318, 0.8149>>)
						SET_CAM_FOV(initialCam, 34.2176)
		
						ATTACH_CAM_TO_ENTITY(destinationCam, PLAYER_PED_ID(), <<-0.2344, 1.2086, 0.3590>>)
						POINT_CAM_AT_ENTITY(destinationCam, PLAYER_PED_ID(), <<0.7781, -1.5633, 0.8989>>)
						SET_CAM_FOV(destinationCam, 34.2176)
						
						SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 2000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
					BREAK
					
					CASE 1
				
						ATTACH_CAM_TO_ENTITY(initialCam, PLAYER_PED_ID(), <<0.2195, -0.1664, 0.5592>>)
						POINT_CAM_AT_ENTITY(initialCam, PLAYER_PED_ID(), <<-0.2152, 2.8012, 0.4948>>)
						SET_CAM_FOV(initialCam, 22.9217)

						ATTACH_CAM_TO_ENTITY(destinationCam, PLAYER_PED_ID(), <<0.2086, -0.0227, 0.6016>>)
						POINT_CAM_AT_ENTITY(destinationCam, PLAYER_PED_ID(), <<0.0509, 2.9689, 0.4409>>)
						SET_CAM_FOV(destinationCam, 22.9217)
						
						SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 2000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
					BREAK
					
					CASE 2
						ATTACH_CAM_TO_ENTITY(initialCam, PLAYER_PED_ID(), <<0.1181, 1.6466, 0.5102>>)
						POINT_CAM_AT_ENTITY(initialCam, PLAYER_PED_ID(), <<0.1214, -1.3524, 0.5898>>)
						SET_CAM_FOV(initialCam, 25.2608)

						ATTACH_CAM_TO_ENTITY(destinationCam, PLAYER_PED_ID(), <<0.1163, 3.8481, -0.3137>>)
						POINT_CAM_AT_ENTITY(destinationCam, PLAYER_PED_ID(), <<0.1204, 0.9066, 0.2762>>)
						SET_CAM_FOV(destinationCam, 25.2608)
						
						SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 2000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
					BREAK
					
					CASE 3
						ATTACH_CAM_TO_ENTITY(initialCam, PLAYER_PED_ID(), <<1.8749, -1.1823, 0.2832>>)
						POINT_CAM_AT_ENTITY(initialCam, PLAYER_PED_ID(), <<-0.4155, 0.7377, 0.5433>>)
						SET_CAM_FOV(initialCam, 14.6981)

						ATTACH_CAM_TO_ENTITY(destinationCam, PLAYER_PED_ID(), <<0.6086, -1.9726, 0.2832>>)
						POINT_CAM_AT_ENTITY(destinationCam, PLAYER_PED_ID(), <<0.0622, 0.9657, 0.5433>>)
						SET_CAM_FOV(destinationCam, 14.6981)
						
						SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 2000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
					BREAK
					
					CASE 4
						SET_CAM_COORD(initialCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<1.8541, -0.3141, 0.4045>>))
						POINT_CAM_AT_ENTITY(initialCam, PLAYER_PED_ID(), <<-0.9772, 0.6750, 0.3321>>)
						SET_CAM_FOV(initialCam, 30.6492)
						
						SET_CAM_COORD(destinationCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<1.8877, -0.2179, 0.4045>>))
						POINT_CAM_AT_ENTITY(destinationCam, PLAYER_PED_ID(), <<-0.9436, 0.7712, 0.3321>>)
						SET_CAM_FOV(destinationCam, 30.6492)						
						
						SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 2000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
					BREAK
					
					CASE 5
						SET_CAM_COORD(initialCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<0.1545, 0.3264, 0.5799>>))
						POINT_CAM_AT_ENTITY(initialCam, PLAYER_PED_ID(), <<0.2061, 3.3210, 0.4089>>)
						SET_CAM_FOV(initialCam, 55.0249)
						SET_CAM_MOTION_BLUR_STRENGTH(initialCam, 0.05)
						
						ATTACH_CAM_TO_ENTITY(destinationCam, pedsEnemy[iEnemyArrayIndex], <<0.0797, 0.3037, 0.5934>>)
						SET_CAM_ROT(destinationCam, <<0.0, 0.0, GET_ENTITY_HEADING(PLAYER_PED_ID())>>)
						SET_CAM_FOV(destinationCam, 55.0249)	
						SET_CAM_MOTION_BLUR_STRENGTH(destinationCam, 0.05)
						
						SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 1100, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
					BREAK
					
					CASE 6
						SET_CAM_COORD(initialCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<-0.7419, 4.1227, 0.4054>>))
						POINT_CAM_AT_ENTITY(initialCam, PLAYER_PED_ID(), <<-0.1724, 1.1775, 0.4413>>)
						SET_CAM_FOV(initialCam, 12.2175)
						SHAKE_CAM(initialCam, "HAND_SHAKE", 0.3)
						SET_CAM_MOTION_BLUR_STRENGTH(initialCam, 0.05)
						
						SET_CAM_COORD(destinationCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(PLAYER_PED_ID(), <<2.2875, 3.6625, 0.4056>>))
						POINT_CAM_AT_ENTITY(destinationCam, PLAYER_PED_ID(), <<0.6709, 1.1355, 0.4416>>)
						SET_CAM_FOV(destinationCam, 12.2175)	
						SHAKE_CAM(destinationCam, "HAND_SHAKE", 0.3)
						SET_CAM_MOTION_BLUR_STRENGTH(destinationCam, 0.05)
						
						SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 2000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
					BREAK
					
				ENDSWITCH
							
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				SET_CUTSCENE_RUNNING(TRUE)
				
				SET_PED_ACCURACY(PLAYER_PED_ID(), 100)
				IF NOT IS_ENTITY_DEAD(pedsEnemy[iEnemyArrayIndex])
	//				TASK_SHOOT_AT_COORD(PLAYER_PED_ID(), GET_PED_BONE_COORDS(pedsEnemy[iEnemyArrayIndex], BONETAG_HEAD, <<0.0, 0.0, 0.0>>), 4000, FIRING_TYPE_CONTINUOUS)
					TASK_SHOOT_AT_ENTITY(PLAYER_PED_ID(), pedsEnemy[iEnemyArrayIndex], 2000, FIRING_TYPE_CONTINUOUS)
				ENDIF
				SETTIMERA(0)
				SET_TIME_SCALE(0.15)
				iKillCamStage++
			ENDIF
		BREAK
		
		CASE 1
			IF iCameraChoice != 6
				STOP_CAM_POINTING(initialCam)
				STOP_CAM_POINTING(destinationCam)
			ENDIF
			
			iKillCamStage++
		BREAK
	
		CASE 2
			//Cut to bad guy falling down, maybe slow mo...
			IF TIMERA() > 160
				iKillCamStage++
			ENDIF
		BREAK
		
		CASE 3
			
			DESTROY_ALL_CAMS()
				
			initialCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			destinationCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			
			//Pick a random death cam, unless the cam is intended to play after a specific kill cam.
			IF iCameraChoice = 4
				iCameraChoice = 2
			ELSE
				iCameraChoice = GET_RANDOM_INT_IN_RANGE(0, 3)
				
				IF bUseSequentialCameras
					iCurrentTargetCam++
					IF iCurrentTargetCam > 2
						iCurrentTargetCam = 0
					ENDIF
					
					iCameraChoice = iCurrentTargetCam
				ENDIF
			ENDIF
				
			SWITCH iCameraChoice
				
				CASE 0
					SET_CAM_COORD(initialCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedsEnemy[iEnemyArrayIndex], <<5.0468, -2.8877, -0.6471>>))
					POINT_CAM_AT_ENTITY(initialCam, pedsEnemy[iEnemyArrayIndex], <<2.5357, -1.2658, -0.3946>>)
					SET_CAM_FOV(initialCam, 26.0325)
					
					SET_CAM_COORD(destinationCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedsEnemy[iEnemyArrayIndex], <<6.1844, 1.0106, 0.0197>>))
					POINT_CAM_AT_ENTITY(destinationCam, pedsEnemy[iEnemyArrayIndex], <<3.2274, 0.5045, 0.0150>>)
					SET_CAM_FOV(destinationCam, 23.8634)

					SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 5000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				BREAK
				
				CASE 1
					SET_CAM_COORD(initialCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedsEnemy[iEnemyArrayIndex], <<6.1844, 1.0106, 0.0197>>))
					POINT_CAM_AT_ENTITY(initialCam, pedsEnemy[iEnemyArrayIndex], <<3.2274, 0.5045, 0.0150>>)
					SET_CAM_FOV(initialCam, 23.8634)
					
					SET_CAM_COORD(destinationCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedsEnemy[iEnemyArrayIndex], <<5.0468, -2.8877, -0.6471>>))
					POINT_CAM_AT_ENTITY(destinationCam, pedsEnemy[iEnemyArrayIndex], <<2.5357, -1.2658, -0.3946>>)
					SET_CAM_FOV(destinationCam, 26.0325)
					
					SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 5000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				BREAK
				
				CASE 2
					SET_CAM_COORD(initialCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedsEnemy[iEnemyArrayIndex], <<-2.8838, -0.4106, 0.1995>>))
					POINT_CAM_AT_ENTITY(initialCam, pedsEnemy[iEnemyArrayIndex], <<0.0107, 0.3743, 0.1270>>)
					SET_CAM_FOV(initialCam, 30.6492)
					
					SET_CAM_COORD(destinationCam, GET_OFFSET_FROM_ENTITY_IN_WORLD_COORDS(pedsEnemy[iEnemyArrayIndex], <<-2.7120, -1.0260, 0.1995>>))
					POINT_CAM_AT_ENTITY(destinationCam, pedsEnemy[iEnemyArrayIndex], <<0.0045, 0.2449, 0.1270>>)
					SET_CAM_FOV(destinationCam, 30.6492)
					
					SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 2000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				BREAK
				
			ENDSWITCH

			SETTIMERA(0)
			iKillCamStage++	
		BREAK
		
		CASE 4
			STOP_CAM_POINTING(initialCam)
			STOP_CAM_POINTING(destinationCam)
			
			iKillCamStage++	
		BREAK
		
		CASE 5
			//Cut to bad guy falling down, maybe slow mo...
			IF TIMERA() > 250
				iKillCamStage++
			ENDIF
		BREAK
		
		CASE 6
			SET_TIME_SCALE(1.0)
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SET_CUTSCENE_RUNNING(FALSE)
			iKillCamStage = 0
		BREAK
	
	ENDSWITCH

ENDPROC


FLOAT fTimeScale = 1.0

PROC UPDATE_SPIN_CAM()
	
	PED_INDEX pedThisPed

	SWITCH iKillCamStage

		CASE 0
		
			iEnemyArrayIndex = GET_ENEMY_IN_ARRAY_BEING_AIMED_AT()
		
			//Camera swings around Michael a bit...
			IF iEnemyArrayIndex <> -1
			AND IS_PED_SHOOTING(PLAYER_PED_ID())

				DESTROY_ALL_CAMS()
				
				initialCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				destinationCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				
				ATTACH_CAM_TO_ENTITY(initialCam, PLAYER_PED_ID(), <<1.2777, 0.7717, 0.4916>>)
				POINT_CAM_AT_ENTITY(initialCam, PLAYER_PED_ID(), <<-1.4069, -0.5664, 0.5338>>)
				SET_CAM_FOV(initialCam, 26.6584)
				SHAKE_CAM(initialCam, "HAND_SHAKE", 0.1)

				ATTACH_CAM_TO_ENTITY(destinationCam, PLAYER_PED_ID(), <<0.0924, 1.5221, 0.4916>>)
				POINT_CAM_AT_ENTITY(destinationCam, PLAYER_PED_ID(), <<0.0559, -1.4777, 0.4995>>)
				SET_CAM_FOV(destinationCam, 26.6584)
				SHAKE_CAM(destinationCam, "HAND_SHAKE", 0.1)
				
				SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 2500)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				SET_CUTSCENE_RUNNING(TRUE)
				
				SET_PED_ACCURACY(PLAYER_PED_ID(), 100)
				IF NOT IS_ENTITY_DEAD(pedsEnemy[iEnemyArrayIndex])
	//				TASK_SHOOT_AT_COORD(PLAYER_PED_ID(), GET_PED_BONE_COORDS(pedsEnemy[iEnemyArrayIndex], BONETAG_HEAD, <<0.0, 0.0, 0.0>>), 4000, FIRING_TYPE_CONTINUOUS)
					TASK_SHOOT_AT_ENTITY(PLAYER_PED_ID(), pedsEnemy[iEnemyArrayIndex], 2000, FIRING_TYPE_CONTINUOUS)
				
				
				ENDIF
				SETTIMERA(0)
				fTimeScale = 1.0
				SET_TIME_SCALE(fTimeScale)
				iKillCamStage++
			ENDIF
		BREAK

		CASE 1
		
			IF fTimeScale > 0.15
				fTimeScale = fTimeScale -@ 0.75
				PRINTLN("fTimeScale: ", fTimeScale)
			ENDIF
			
			SET_TIME_SCALE(fTimeScale)
		
			IF TIMERA() > 1000
				iKillCamStage++
			ENDIF
		BREAK
	
		CASE 2
			DESTROY_ALL_CAMS()
				
			initialCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			destinationCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			
			iCameraChoice = GET_RANDOM_INT_IN_RANGE(0, 2)
			
			IF bUseSequentialCameras
				iCurrentTargetCam++
				IF iCurrentTargetCam > 1
					iCurrentTargetCam = 0
				ENDIF
				
				iCameraChoice = iCurrentTargetCam
			ENDIF
				
			SWITCH iCameraChoice
				
				CASE 0
			
//					SET_CAM_COORD(initialCam,<<942.233154,-3051.665771,6.927951>>)
//					SET_CAM_ROT(initialCam,<<-1.753416,0.000000,-103.492142>>)
//					SET_CAM_FOV(initialCam,8.322383)
//
//					SET_CAM_COORD(destinationCam,<<940.782532,-3073.242920,6.927951>>)
//					SET_CAM_ROT(destinationCam,<<-1.753416,0.000000,-83.092056>>)
//					SET_CAM_FOV(destinationCam,8.322383)
					
					SET_CAM_COORD(initialCam,<<976.505737,-3076.698242,5.880080>>)
					SET_CAM_ROT(initialCam,<<-0.820553,-0.000000,-0.333209>>)
					SET_CAM_FOV(initialCam,26.327719)
					SHAKE_CAM(initialCam, "HAND_SHAKE", 0.1)


					SET_CAM_COORD(destinationCam,<<971.502441,-3076.665771,5.880080>>)
					SET_CAM_ROT(destinationCam,<<-0.820553,-0.000000,-0.333209>>)
					SET_CAM_FOV(destinationCam,26.327719)
					SHAKE_CAM(destinationCam, "HAND_SHAKE", 0.1)

					
					SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 7000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				BREAK
				
				CASE 1
					
					SET_ENTITY_COORDS(PLAYER_PED_ID(), <<990.5257, -3063.8633, 4.9806>>)
					SET_ENTITY_HEADING(PLAYER_PED_ID(), 106.7876)
					
					SET_CAM_COORD(initialCam,<<990.692505,-3063.817139,5.012168>>)
					SET_CAM_ROT(initialCam,<<5.759845,-0.000001,92.532021>>)
					SET_CAM_FOV(initialCam,45.000000)
					SHAKE_CAM(initialCam, "HAND_SHAKE", 0.1)

					SET_CAM_COORD(destinationCam,<<974.165344,-3063.804932,4.984015>>)
					SET_CAM_ROT(destinationCam,<<5.759886,0.000000,92.532402>>)
					SET_CAM_FOV(destinationCam,45.000000)
					SHAKE_CAM(destinationCam, "HAND_SHAKE", 0.1)
					
					SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 10000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)		
				BREAK
			
									
			ENDSWITCH
			
			
			
			SETTIMERA(0)
			iKillCamStage++
				
		BREAK
		
		CASE 3
		
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
						
			DRAW_DEBUG_SPHERE(GET_FINAL_RENDERED_CAM_COORD(), 0.35, 255, 0, 0, 65)
		
			pedThisPed = pedsEnemy[GET_CLOSEST_ENEMY_TO_CAM_IN_ARRAY()]
		
			PRINTLN("closest ped: ", GET_CLOSEST_ENEMY_TO_CAM_IN_ARRAY())
		
//			IF NOT IS_ENTITY_DEAD(pedThisPed)
//				
//            	DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(pedThisPed), 0.35)
//			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedThisPed)
			AND TIMERB() > 50
				SET_PED_SHOOTS_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(pedThisPed) )
				TASK_SHOOT_AT_ENTITY(PLAYER_PED_ID(), pedThisPed, 2000, FIRING_TYPE_CONTINUOUS)
			
				EXPLODE_PED_HEAD(pedThisPed)
				APPLY_DAMAGE_TO_PED(pedThisPed, 200, TRUE)
				SETTIMERB(0)
			ENDIF
		
			
			IF TIMERA() > 700
				IF fTimeScale < 1.0
					fTimeScale = fTimeScale +@ 0.85
					
					PRINTLN("fTimeScale: ", fTimeScale)
				ENDIF
					
				SET_TIME_SCALE(fTimeScale)
			ENDIF
			
			IF TIMERA() > 2000
				iKillCamStage++
			ENDIF
			
		BREAK
		
		CASE 4
			SET_TIME_SCALE(1.0)
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SET_CUTSCENE_RUNNING(FALSE)
			iKillCamStage = 0
		BREAK
	
	ENDSWITCH

ENDPROC


PROC UPDATE_CORRIDOOR_CAM()
	
	PED_INDEX pedThisPed
	INT i
	SWITCH iKillCamStage

		CASE 0
		
			iEnemyArrayIndex = GET_ENEMY_IN_ARRAY_BEING_AIMED_AT()
		
			//Camera swings around Michael a bit...
			IF iEnemyArrayIndex <> -1
			AND IS_PED_SHOOTING(PLAYER_PED_ID())
				
				FOR i = 0 TO NUMBER_OF_PEDS - 1
					IF NOT IS_ENTITY_DEAD(pedsEnemy[i])
						SET_ENTITY_INVINCIBLE(pedsEnemy[i], TRUE)
					ENDIF
				ENDFOR
				
				SET_ENTITY_COORDS(PLAYER_PED_ID(), << 985.6133, -3035.2703, 4.9006 >>, TRUE, TRUE)
				SET_ENTITY_HEADING(PLAYER_PED_ID(), 261.0529)

				IF NOT IS_ENTITY_DEAD(pedsEnemy[iEnemyArrayIndex])	
					TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(PLAYER_PED_ID(), << 995.4260, -3035.5215, 4.9006 >>, pedsEnemy[iEnemyArrayIndex], PEDMOVE_SPRINT, TRUE )
					FORCE_PED_AI_AND_ANIMATION_UPDATE(pedsEnemy[iEnemyArrayIndex])
				ENDIF

				DESTROY_ALL_CAMS()
				
				initialCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				destinationCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
				
				ATTACH_CAM_TO_ENTITY(initialCam, PLAYER_PED_ID(), <<1.2777, 0.7717, 0.4916>>)
				POINT_CAM_AT_ENTITY(initialCam, PLAYER_PED_ID(), <<-1.4069, -0.5664, 0.5338>>)
				SET_CAM_FOV(initialCam, 26.6584)

				ATTACH_CAM_TO_ENTITY(destinationCam, PLAYER_PED_ID(), <<0.0924, 1.5221, 0.4916>>)
				POINT_CAM_AT_ENTITY(destinationCam, PLAYER_PED_ID(), <<0.0559, -1.4777, 0.4995>>)
				SET_CAM_FOV(destinationCam, 26.6584)
				
				SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 2500, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				RENDER_SCRIPT_CAMS(TRUE, FALSE)
				
				SET_CUTSCENE_RUNNING(TRUE)
				
				SET_PED_ACCURACY(PLAYER_PED_ID(), 100)
				
				SETTIMERA(0)
				//SET_TIME_SCALE(0.1)
				fTimeScale = 1.0
				SET_TIME_SCALE(fTimeScale)
				iKillCamStage++
			ENDIF
		BREAK

		CASE 1
				
			IF fTimeScale > 0.25
				fTimeScale = fTimeScale -@ 0.75
				PRINTLN("fTimeScale: ", fTimeScale)
			ENDIF
			
			SET_TIME_SCALE(fTimeScale)
			
			IF TIMERA() > 1000
				iKillCamStage++
			ENDIF
		BREAK
	
		CASE 2
			
			FOR i = 0 TO NUMBER_OF_PEDS - 1
				IF NOT IS_ENTITY_DEAD(pedsEnemy[i])
					SET_ENTITY_INVINCIBLE(pedsEnemy[i], FALSE)
				ENDIF
			ENDFOR
			
			SET_TIME_SCALE(0.25)
						
			DESTROY_ALL_CAMS()
			
			
			initialCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			destinationCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			
			iCameraChoice = GET_RANDOM_INT_IN_RANGE(0, 2)
			
			IF bUseSequentialCameras
				iCurrentTargetCam++
				IF iCurrentTargetCam > 1
					iCurrentTargetCam = 0
				ENDIF
				
				iCameraChoice = iCurrentTargetCam
			ENDIF
			
			SWITCH iCameraChoice
				
				CASE 0
			
					SET_CAM_COORD(initialCam,<<1032.033569,-3034.421875,10.432465>>)
					SET_CAM_ROT(initialCam,<<0.611577,-0.000000,90.044098>>)
					SET_CAM_FOV(initialCam,45.000000)

					SET_CAM_COORD(destinationCam,<<1014.923340,-3034.411377,10.432465>>)
					SET_CAM_ROT(destinationCam,<<-14.518593,-0.000000,90.044098>>)
					SET_CAM_FOV(destinationCam,45.000000)
					SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 10000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)		
				BREAK
				
				CASE 1
					
							
					
					SET_CAM_COORD(initialCam,<<982.060242,-3035.990967,5.236570>>)
					SET_CAM_ROT(initialCam,<<13.490773,0.000000,-72.398155>>)
					SET_CAM_FOV(initialCam,52.704018)

					SET_CAM_COORD(destinationCam,<<1000.046387,-3035.990967,5.236570>>)
					SET_CAM_ROT(destinationCam,<<13.490773,0.000000,-72.398155>>)
					SET_CAM_FOV(destinationCam,52.704018)
					SET_CAM_ACTIVE_WITH_INTERP(destinationCam, initialCam, 25000, GRAPH_TYPE_LINEAR, GRAPH_TYPE_LINEAR)
				BREAK
			
									
			ENDSWITCH
			
			
			
			SETTIMERA(0)
			iKillCamStage++
				
		BREAK
		
		CASE 3
		
			SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)
						
			DRAW_DEBUG_SPHERE(GET_FINAL_RENDERED_CAM_COORD(), 0.35, 255, 0, 0, 65)
		
			pedThisPed = pedsEnemy[GET_CLOSEST_ENEMY_TO_CAM_IN_ARRAY()]
		
			PRINTLN("closest ped: ", GET_CLOSEST_ENEMY_TO_CAM_IN_ARRAY())
		
//			IF NOT IS_ENTITY_DEAD(pedThisPed)
//				
//            	DRAW_DEBUG_SPHERE(GET_ENTITY_COORDS(pedThisPed), 0.35)
//			ENDIF
			
			IF NOT IS_ENTITY_DEAD(pedThisPed)
			AND TIMERB() > 50
				SET_PED_SHOOTS_AT_COORD(PLAYER_PED_ID(), GET_ENTITY_COORDS(pedThisPed) )
				//TASK_SHOOT_AT_ENTITY(PLAYER_PED_ID(), pedThisPed, 2000, FIRING_TYPE_CONTINUOUS)
				TASK_GO_TO_COORD_WHILE_AIMING_AT_ENTITY(PLAYER_PED_ID(), << 995.4260, -3035.5215, 4.9006 >>, pedThisPed, PEDMOVE_SPRINT, TRUE  )
				
				EXPLODE_PED_HEAD(pedThisPed)
				APPLY_DAMAGE_TO_PED(pedThisPed, 200, TRUE)
				SETTIMERB(0)
			ENDIF
		
			IF TIMERA() > 1000
				IF fTimeScale < 1.0
					fTimeScale = fTimeScale +@ 0.75
					
					PRINTLN("fTimeScale: ", fTimeScale)
				ENDIF
					
				SET_TIME_SCALE(fTimeScale)
			ENDIF
			
			IF TIMERA() > 3000
				iKillCamStage++
			ENDIF
			
		BREAK
		
		CASE 4
			SET_TIME_SCALE(1.0)
			CLEAR_PED_TASKS(PLAYER_PED_ID())
			SET_CUTSCENE_RUNNING(FALSE)
			iKillCamStage = 0
		BREAK
	
	ENDSWITCH

ENDPROC


PROC stageDoShootout()

	SWITCH i_current_event

		CASE 0
			//init
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << 990.6710, -3084.4187, 4.9006 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 25.3050)
			GIVE_WEAPON_TO_PED(player_ped_id(), WEAPONTYPE_CARBINERIFLE, INFINITE_AMMO, TRUE, TRUE)
			CREATE_ENEMIES_KILL_CAM()
			SET_ENTITY_INVINCIBLE(player_ped_id(), TRUE)
			SET_ENTITY_PROOFS(player_ped_id(), TRUE, TRUE, TRUE, TRUE, TRUE)
			RESET_GAME_CAMERA()		
			i_current_event++
		BREAK
				
		CASE 1
			//Update
			UPDATE_KILL_CAM()
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC stageDoCorridoor()

	SWITCH i_current_event

		CASE 0
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << 985.6133, -3035.2703, 4.9006 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 261.0529)
			GIVE_WEAPON_TO_PED(player_ped_id(), WEAPONTYPE_CARBINERIFLE, INFINITE_AMMO, TRUE, TRUE)
			CREATE_ENEMIES_CORRIDOOR()
			SET_ENTITY_INVINCIBLE(player_ped_id(), TRUE)
			SET_ENTITY_PROOFS(player_ped_id(), TRUE, TRUE, TRUE, TRUE, TRUE)
			RESET_GAME_CAMERA()		
			i_current_event++
		BREAK
				
		CASE 1
			//Update
			UPDATE_CORRIDOOR_CAM()
		BREAK
		
	ENDSWITCH
		
ENDPROC

PROC stageDoSpinkill()

	SWITCH i_current_event

		CASE 0
			//init
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << 990.5258, -3063.8633, 4.9006 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 94.3487)
			GIVE_WEAPON_TO_PED(player_ped_id(), WEAPONTYPE_CARBINERIFLE, INFINITE_AMMO, TRUE, TRUE)
			CREATE_ENEMIES_KILL_CAM()
			SET_ENTITY_INVINCIBLE(player_ped_id(), TRUE)
			SET_ENTITY_PROOFS(player_ped_id(), TRUE, TRUE, TRUE, TRUE, TRUE)
			RESET_GAME_CAMERA()		
			i_current_event++
		BREAK
				
		CASE 1
			//Update
			UPDATE_SPIN_CAM()
		BREAK
		
	ENDSWITCH
	
ENDPROC




PROC stageDoKillHeader()
	SWITCH i_current_event
		CASE 0
			//init
			SET_ENTITY_COORDS(PLAYER_PED_ID(), << 990.5258, -3063.8633, 4.9006 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 94.3487)
			GIVE_WEAPON_TO_PED(player_ped_id(), WEAPONTYPE_CARBINERIFLE, INFINITE_AMMO, TRUE, TRUE)
			CREATE_ENEMIES_KILL_CAM()
			SET_ENTITY_INVINCIBLE(player_ped_id(), TRUE)
			SET_ENTITY_PROOFS(player_ped_id(), TRUE, TRUE, TRUE, TRUE, TRUE)
			RESET_GAME_CAMERA()		
			i_current_event++
		BREAK
				
		CASE 1
			iEnemyArrayIndex = GET_ENEMY_IN_ARRAY_BEING_AIMED_AT()

			IF iEnemyArrayIndex <> -1
			AND IS_PED_SHOOTING(PLAYER_PED_ID())
				i_current_event++
			ENDIF
		BREAK
		
		CASE 2
			IF RUN_KILL_CAM(sKillCamDataTest, <<976.505737,-3076.698242,5.880080>>, <<-0.820553,-0.000000,-0.333209>>, 26.327719,
							<<971.502441,-3076.665771,5.880080>>, <<-0.820553,-0.000000,-0.333209>>, 26.327719)
				i_current_event = 1
			ENDIF
		BREAK
	ENDSWITCH
ENDPROC

 PROC GET_DEBUG_CAM_RELATIVE_TO_VEHICLE(VEHICLE_INDEX veh, VECTOR &vOffset, VECTOR &vPoint, FLOAT &fFov)
        CAMERA_INDEX cam = GET_DEBUG_CAM()
        
        IF DOES_CAM_EXIST(cam)
        
            IF IS_VEHICLE_DRIVEABLE(veh)
                VECTOR v_cam_pos = GET_CAM_COORD(cam)
                VECTOR v_cam_rot = GET_CAM_ROT(cam)             
                
				VECTOR v_veh_rot = GET_ENTITY_ROTATION(veh)
				                
				
				
                //Source data
               	vOffset = GET_OFFSET_FROM_ENTITY_GIVEN_WORLD_COORDS(veh, v_cam_pos)
				vPoint = (v_cam_rot - v_veh_rot)
				fFov = GET_CAM_FOV(cam) 
               
            ENDIF   
        ENDIF
    ENDPROC

VECTOR vCameraOffset, vCameraPointVec
FLOAT fFov

PROC stageDoCameraAttach()

	VECTOR tempRotation

	SWITCH i_current_event

		CASE 0
			
			SET_ENTITY_COORDS(PLAYER_PED_ID(),<< -2424.7180, 3139.6670, 31.8254 >>)
			SET_ENTITY_HEADING(PLAYER_PED_ID(), 239.9460)
		
			
		
			SET_ENTITY_INVINCIBLE(player_ped_id(), TRUE)
			SET_ENTITY_PROOFS(player_ped_id(), TRUE, TRUE, TRUE, TRUE, TRUE)
			RESET_GAME_CAMERA()		
			
						
			i_current_event++
		BREAK
				
		CASE 1
			IF DOES_ENTITY_EXIST(GET_FOCUS_ENTITY_INDEX())
				IF IS_ENTITY_A_VEHICLE(GET_FOCUS_ENTITY_INDEX())
					i_current_event++
				ENDIF
			ENDIF
		BREAK
		
		CASE 2
			
			IF IS_ENTITY_A_VEHICLE(GET_FOCUS_ENTITY_INDEX())
				
				IF NOT IS_PED_IN_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_FOCUS_ENTITY_INDEX()))
					IF NOT IS_ENTITY_DEAD(GET_FOCUS_ENTITY_INDEX())
						SET_ENTITY_INVINCIBLE(GET_FOCUS_ENTITY_INDEX(), TRUE)
						SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_FOCUS_ENTITY_INDEX()))
					ENDIF
				ENDIF
				
				
				IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_N)
					
					DESTROY_ALL_CAMS()
					initialCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
						
					GET_DEBUG_CAM_RELATIVE_TO_VEHICLE(GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_FOCUS_ENTITY_INDEX()), vCameraOffset, vCameraPointVec, fFov)	
											
					PRINTLN("boom indeed:", vCameraOffset, vCameraPointVec, fFov)
						
					ATTACH_CAM_TO_ENTITY(initialCam, GET_FOCUS_ENTITY_INDEX(), vCameraOffset)
						
					tempRotation = GET_ENTITY_ROTATION(GET_FOCUS_ENTITY_INDEX())
					tempRotation = tempRotation + vCameraPointVec
						
					SET_CAM_ROT(initialCam, tempRotation) 
						
					SET_CAM_FOV(initialCam, fFov)
				
					//SET_CAM_INHERIT_ROLL_VEHICLE(initialCam, GET_VEHICLE_INDEX_FROM_ENTITY_INDEX(GET_FOCUS_ENTITY_INDEX()))
				
					SET_CAM_ACTIVE(initialCam, TRUE)
					RENDER_SCRIPT_CAMS(TRUE, FALSE)
					
					SCRIPT_ASSERT("boom")
					
				ENDIF				
					
				tempRotation = GET_ENTITY_ROTATION(GET_FOCUS_ENTITY_INDEX())
				tempRotation = tempRotation + vCameraPointVec
						
				SET_CAM_ROT(initialCam, tempRotation) 
					
				
			ENDIF
		
		BREAK
				
	ENDSWITCH
	
ENDPROC

VEHICLE_INDEX viPlane
VEHICLE_INDEX viHeli1
VEHICLE_INDEX viHeli2

PED_INDEX piPLanePilot
PED_INDEX piHeli1Pilot
PED_INDEX piHeli2Pilot

VECTOR vVehicleSpawn = << -257.2034, -45.7409, 48.5418 >>

CAMERA_INDEX dummyCam

VECTOR vPlanePos =  <<-324.5,-35.125,163.250>>
VECTOR vPlaneRot = <<-8.875, -64.150, 202.125>>

VECTOR vHeli1Pos = <<-328.3,-78.225, 180.075>>
VECTOR vHeli1Rot = <<0.0, 0.0, 0.0>>

VECTOR vHeli2Pos = <<-308.450,-103.250, 176.150>>
VECTOR vHeli2Rot = <<-16.550, -16.750, 0.0>>

BOOL bDrawDebugSphere = TRUE

PROC stageDoPositioner()

	SWITCH 	i_current_event			
		CASE 0
			REQUEST_MODEL(LAZER)
			REQUEST_MODEL(POLMAV)
			REQUEST_MODEL(S_M_Y_PILOT_01)
			i_current_event++
		BREAK
		
		CASE 1
			IF HAS_MODEL_LOADED(LAZER)
			AND HAS_MODEL_LOADED(POLMAV)
			AND HAS_MODEL_LOADED(S_M_Y_PILOT_01)
				i_current_event++
			ENDIF
		BREAK
		
		CASE 2
		
			viPlane = CREATE_VEHICLE(LAZER, vPlanePos)
			
			CONTROL_LANDING_GEAR(viPlane, LGC_RETRACT_INSTANT)
			
			viHeli1 = CREATE_VEHICLE(POLMAV, vHeli1Pos)
			viHeli2 = CREATE_VEHICLE(POLMAV, vHeli2Pos)
			
			//SET_PED_INTO_VEHICLE(PLAYER_PED_ID(), viPlane)
			
			piPLanePilot = CREATE_PED_INSIDE_VEHICLE(viPlane, PEDTYPE_MISSION, S_M_Y_PILOT_01)
			piHeli1Pilot = CREATE_PED_INSIDE_VEHICLE(viHeli1, PEDTYPE_MISSION, S_M_Y_PILOT_01)
			piHeli2Pilot = CREATE_PED_INSIDE_VEHICLE(viHeli2, PEDTYPE_MISSION, S_M_Y_PILOT_01)
			
			SET_HELI_BLADES_FULL_SPEED(viHeli1)
			SET_HELI_BLADES_FULL_SPEED(viHeli2)
			
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piPLanePilot, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piHeli1Pilot, TRUE)
			SET_BLOCKING_OF_NON_TEMPORARY_EVENTS(piHeli2Pilot, TRUE)
			
			DESTROY_ALL_CAMS()
			dummyCam = CREATE_CAMERA(CAMTYPE_SCRIPTED, TRUE)
			SET_CAM_ACTIVE(dummyCam, TRUE)
			i_current_event++
		
		BREAK
		
		CASE 3
		
			IF NOT IS_ENTITY_DEAD(viPlane)
				SET_ENTITY_COORDS(viPlane, vPlanePos)
				SET_ENTITY_ROTATION(viPlane, vPlaneRot)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(viHeli1)
				SET_ENTITY_COORDS(viHeli1, vHeli1Pos)
				SET_ENTITY_ROTATION(viHeli1, vHeli1Rot)
			ENDIF
			
			IF NOT IS_ENTITY_DEAD(viHeli2)
				SET_ENTITY_COORDS(viHeli2, vHeli2Pos)
				SET_ENTITY_ROTATION(viHeli2, vHeli2Rot)
			ENDIF
			
			IF IS_MOUSE_BUTTON_PRESSED(MB_LEFT_BTN)
				SCRIPT_ASSERT("boom")
				vVehicleSpawn = GET_SCRIPT_MOUSE_POINTER_IN_WORLD_COORDS()
			ENDIF
			
			SET_CAM_COORD(dummyCam, vVehicleSpawn)
			
			IF IS_MOUSE_BUTTON_JUST_PRESSED(MB_RIGHT_BTN)
				IF bDrawDebugSphere = FALSE	
					bDrawDebugSphere = TRUE
				ELSE
					bDrawDebugSphere = FALSE
				ENDIF
			ENDIF
			
			IF bDrawDebugSphere
				DRAW_DEBUG_SPHERE(vVehicleSpawn, 3.0, 0, 255, 255)
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(TRUE)			
			ELSE
				SET_DEBUG_LINES_AND_SPHERES_DRAWING_ACTIVE(FALSE)			
			ENDIF
			SET_ENTITY_COORDS(PLAYER_PED_ID(), vVehicleSpawn)
			SET_ENTITY_VISIBLE(PLAYER_PED_ID(), FALSE)
			USE_SCRIPT_CAM_FOR_AMBIENT_POPULATION_ORIGIN_THIS_FRAME(TRUE, TRUE)
			
		BREAK
		
	ENDSWITCH
	
ENDPROC

PROC debugRoutines()

	
		

//***************************************************************
//****************** MISSION STAGES *****************************
//***************************************************************

ENDPROC
		
	PROC jSkip()
	
		KILL_ANY_CONVERSATION()
			
		SWITCH mission_stage
	
			CASE STAGE_INITIALISE //keep compiler happy
			BREAK
			
			CASE STAGE_DO_SHOOTOUT
							
			BREAK
						
			
		ENDSWITCH
		
	ENDPROC
	
	PROC pSkip()
	
//		SWITCH mission_stage
//			
//
//			
//		ENDSWITCH
		
	ENDPROC
	
	
	
PROC debugProcedures()

		iDebugMissionStage = ENUM_TO_INT(mission_stage)

		IF bDebugInitialised = FALSE
			//Init debug stuff.
					
			sol5WidgetGroup = START_WIDGET_GROUP("Shooting Camera")
				
				ADD_WIDGET_BOOL("bIsSuperDebugEnabled", bIsSuperDebugEnabled)
				ADD_WIDGET_BOOL("Enable Debug Processing", bDebugOn)
				
				ADD_WIDGET_INT_READ_ONLY("iKillCamStage", iKillCamStage)
				ADD_WIDGET_INT_READ_ONLY("i_current_event", i_current_event)
				ADD_WIDGET_INT_READ_ONLY("mission_stage", iDebugMissionStage)

				START_WIDGET_GROUP("Positioner")
					ADD_WIDGET_STRING("Plane")
					ADD_WIDGET_VECTOR_SLIDER("Plane Pos", vPlanePos, -3000.0, 3000.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("Plane Rot", vPlaneRot, -3000.0, 3000.0, 0.1)
					ADD_WIDGET_STRING("heli 1")
					ADD_WIDGET_VECTOR_SLIDER("Heli1 Pos", vHeli1Pos, -3000.0, 3000.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("Heli1 Rot", vHeli1Rot, -3000.0, 3000.0, 0.1)
					ADD_WIDGET_STRING("heli 2")
					ADD_WIDGET_VECTOR_SLIDER("Heli2 Pos", vHeli2Pos, -3000.0, 3000.0, 0.1)
					ADD_WIDGET_VECTOR_SLIDER("Heli2 Rot", vHeli2Rot, -3000.0, 3000.0, 0.1)
					ADD_WIDGET_STRING("Traffic position")
					ADD_WIDGET_VECTOR_SLIDER("Traffic Pos", vVehicleSpawn, -3000.0, 3000.0, 0.1)
					
				STOP_WIDGET_GROUP()			 
									
			STOP_WIDGET_GROUP()		
			
			SET_LOCATES_HEADER_WIDGET_GROUP(sol5WidgetGroup)

			SkipMenuStruct[ENUM_TO_INT(STAGE_INITIALISE)].bSelectable = FALSE
			SkipMenuStruct[ENUM_TO_INT(STAGE_CHOOSE_STAGE)].bSelectable = FALSE
			
			SkipMenuStruct[ENUM_TO_INT(STAGE_DO_HOMING)].sTxtLabel 			= "Homing bullet"
			SkipMenuStruct[ENUM_TO_INT(STAGE_DO_SHOOTOUT)].sTxtLabel 		= "One hit kill"
			SkipMenuStruct[ENUM_TO_INT(STAGE_DO_CORRIDOOR)].sTxtLabel 		= "Kill all - corridoor"              
			SkipMenuStruct[ENUM_TO_INT(STAGE_DO_SPIN_KILL)].sTxtLabel 		= "Kill all - open area"
			SkipMenuStruct[ENUM_TO_INT(STAGE_DO_KILL_HEADER)].sTxtLabel 	= "Kill all - using header file"   
			SkipMenuStruct[ENUM_TO_INT(STAGE_DO_CAMERA_ATTACH)].sTxtLabel 	= "Camera Attachment - for Aaron"   
			SkipMenuStruct[ENUM_TO_INT(STAGE_DO_POSITIONER)].sTxtLabel 		= "Entity Positioner - for Aaron"   
			
			
			bDebugInitialised = TRUE
		ENDIF

	
 	IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, ENUM_TO_INT(mission_stage), TRUE)
		DELETE_ARRAY_OF_VEHICLES(vehiclesEnemy)
		DELETE_ARRAY_OF_PEDS(pedsEnemy)
		i_current_event = 0	
		mission_stage =  INT_TO_ENUM(MISSION_STAGE_FLAG, iReturnStage)
	ENDIF
			
	IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_C)
	
	
		FLOAT fX, fY, fZ, fW
		VECTOR vCoords
		
		ENTITY_INDEX thisCar = GET_FOCUS_ENTITY_INDEX()
		IF DOES_ENTITY_EXIST(thisCar)
			GET_ENTITY_QUATERNION(thisCar, fX, fY, fZ, fW)
			vCoords = GET_ENTITY_COORDS(thiscar)
		
			SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_QUATERNION(thisCar,") SAVE_FLOAT_TO_DEBUG_FILE(fX)
			SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fY)
			SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fZ)
			SAVE_STRING_TO_DEBUG_FILE(", ") SAVE_FLOAT_TO_DEBUG_FILE(fW)
			SAVE_STRING_TO_DEBUG_FILE(")")
			
			
			SAVE_STRING_TO_DEBUG_FILE("SET_ENTITY_COORDS(thisCar,") SAVE_VECTOR_TO_DEBUG_FILE(vCoords) 
			SAVE_STRING_TO_DEBUG_FILE(")")
		ENDIF
		
	ENDIF
			
		
		IF bDebugOn
								
			IF IS_KEYBOARD_KEY_PRESSED(KEY_S)		
				MISSION_PASSED()
			ENDIF
		
			IF IS_KEYBOARD_KEY_PRESSED(KEY_f)	
				Mission_Failed()
			ENDIF
		
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_J) 
				jSkip()	
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_P) 
				pSkip()	
			ENDIF
			
			IF IS_KEYBOARD_KEY_JUST_PRESSED(KEY_NUMPAD1) 
				bUseSequentialCameras = NOT bUseSequentialCameras
			ENDIF
			
		ENDIF
	
	ENDPROC

 	
#ENDIF

// ____________________________________ MISSION SCRIPT _____________________________________

SCRIPT

	#IF IS_DEBUG_BUILD

	IF HAS_FORCE_CLEANUP_OCCURRED()	      
		MISSION_CLEANUP()  
		TERMINATE_THIS_THREAD()
	ENDIF
	
	// ____________________________________ MISSION LOOP _______________________________________
	
	SET_MISSION_FLAG(TRUE)
	
	WHILE TRUE	
				
		 
			debugProcedures()
			//mission_stage = STAGE_DEBUG	//Comment out...
					
		SWITCH mission_stage 
					 
			CASE STAGE_DEBUG
				debugRoutines()
			BREAK			
			 			
			CASE STAGE_INITIALISE	
				initialiseMission()		
			BREAK		
			
			CASE STAGE_CHOOSE_STAGE
			
				IF LAUNCH_MISSION_STAGE_MENU(SkipMenuStruct, iReturnStage, ENUM_TO_INT(mission_stage), TRUE)
					DELETE_ARRAY_OF_VEHICLES(vehiclesEnemy)
					DELETE_ARRAY_OF_PEDS(pedsEnemy)
					i_current_event = 0	
					mission_stage =  INT_TO_ENUM(MISSION_STAGE_FLAG, iReturnStage)
				ENDIF
				
			BREAK
			
			CASE STAGE_DO_SHOOTOUT
				stageDoShootout()		
			BREAK		
	
			CASE STAGE_DO_HOMING
				stageDoShootout()		
			BREAK		
			
			CASE STAGE_DO_CORRIDOOR
				stageDoCorridoor()		
			BREAK		
			
			CASE STAGE_DO_SPIN_KILL
				stageDoSpinKill()		
			BREAK	
			
			CASE STAGE_DO_KILL_HEADER
				stageDoKillHeader()
			BREAK

			CASE STAGE_DO_CAMERA_ATTACH
				stageDoCameraAttach()
			BREAK		
			
			CASE STAGE_DO_POSITIONER
				stageDoPositioner()
			BREAK		
	
			DEFAULT
				SCRIPT_ASSERT("Unrecognised Mission Stage!")
			BREAK
			
		ENDSWITCH	
		
		WAIT(0)

		 
		
	ENDWHILE
	
	//	should never reach here - always ends by going through the cleanup function

	#ENDIF

ENDSCRIPT			





