USING "rage_builtins.sch"
USING "globals.sch"
USING "beast_secret_fight.sch"

// Compiled in to vehicle_gen_controller.sc when FEATURE_SP_DLC_BEAST_SECRET is set to 1 
// and FEATURE_SP_DLC_BEAST_SECRET_DEBUG to 0.

#IF FEATURE_SP_DLC_BEAST_SECRET
#IF FEATURE_SP_DLC_BEAST_SECRET_DEBUG
	BeastFinaleVars sBeastFinaleVars
#ENDIF
#ENDIF

SCRIPT
	#IF FEATURE_SP_DLC_BEAST_SECRET
	#IF FEATURE_SP_DLC_BEAST_SECRET_DEBUG
	
		CPRINTLN(DEBUG_HUNTING, "Beast thread 3 launched.")
		
		IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
			CPRINTLN(DEBUG_HUNTING, "Beast thread 3 terminating.")
			Cleanup_Beast_Fight(sBeastFinaleVars)
			Release_Beast_Finale_Assets(sBeastFinaleVars)
			TERMINATE_THIS_THREAD()
		ENDIF
		
		Reset_Beast_Finale_Variables(sBeastFinaleVars)
		
		#IF IS_DEBUG_BUILD
			Create_Beast_Finale_Widgets(sBeastFinaleVars, NULL)
		#ENDIF

		WHILE TRUE
			Maintain_Beast_Finale(sBeastFinaleVars)
		
			#IF IS_DEBUG_BUILD
				Update_Beast_Finale_Widgets(sBeastFinaleVars)
			#ENDIF
		
			WAIT(0)
		ENDWHILE
		
	#ENDIF //FEATURE_SP_DLC_BEAST_SECRET_DEBUG
	#ENDIF //FEATURE_SP_DLC_BEAST_SECRET
ENDSCRIPT
