USING "rage_builtins.sch"
USING "globals.sch"
USING "beast_secret_hunt.sch"

// Compiled in to player_controller.sc when FEATURE_SP_DLC_BEAST_SECRET is set to 1 
// and FEATURE_SP_DLC_BEAST_SECRET_DEBUG to 0.

#IF FEATURE_SP_DLC_BEAST_SECRET
#IF FEATURE_SP_DLC_BEAST_SECRET_DEBUG
	BeastHuntVars sBeastHuntVars
#ENDIF
#ENDIF

SCRIPT
	#IF FEATURE_SP_DLC_BEAST_SECRET
	#IF FEATURE_SP_DLC_BEAST_SECRET_DEBUG

		CPRINTLN(DEBUG_HUNTING, "Beast thread 2 launched.")
		
		IF HAS_FORCE_CLEANUP_OCCURRED(FORCE_CLEANUP_FLAG_SP_TO_MP)
			CPRINTLN(DEBUG_HUNTING, "Beast thread 2 terminating.")
			TERMINATE_THIS_THREAD()
		ENDIF

		Reset_Beast_Hunt_Variables(sBeastHuntVars)

		#IF IS_DEBUG_BUILD
			Create_Beast_Hunt_Widgets(sBeastHuntVars, NULL)
			sBeastHuntVars.bDebugDisplayCheckpoints = TRUE
			sBeastHuntVars.bDebugDisplayPaths = TRUE
			sBeastHuntVars.bDebugDisplayCurrentPath = TRUE
		#ENDIF

		WHILE TRUE
			Maintain_Beast_Hunt(sBeastHuntVars)
			
			#IF IS_DEBUG_BUILD
				Update_Beast_Hunt_Widgets(sBeastHuntVars)
			#ENDIF
			
			WAIT(0)
		ENDWHILE

	#ENDIF //FEATURE_SP_DLC_BEAST_SECRET_DEBUG
	#ENDIF //FEATURE_SP_DLC_BEAST_SECRET
ENDSCRIPT
